/* Formatted on 2009/05/12  17:16 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Operations\GET_PARTNUMBER_PO_PEND_QTY.fnc";
CREATE OR REPLACE FUNCTION get_partnumber_po_pend_qty (
	p_partnum	IN	 t402_work_order.c205_part_number_id%TYPE
)
	RETURN VARCHAR2
IS
--
/********************************************************************************************
 * Description	:This function returns the Pending Qty for a Part
 * Change Details:
 *	 James Jun23,2006 : Changed Formatting according to Standards
	 James Jun23,2006 : Changes for including CloseOut Qty
 *********************************************************************************************/
--
	v_pend_cnt	   NUMBER;
	v_comp_id      t1900_company.c1900_company_id%TYPE;
--
BEGIN
  SELECT get_compid_frm_cntx() INTO  v_comp_id FROM DUAL;
--
  SELECT NVL (SUM (get_wo_pend_qty (c402_work_order_id, c402_qty_ordered)), 0)
    INTO v_pend_cnt
    FROM t402_work_order
   WHERE c205_part_number_id = p_partnum
     AND c402_void_fl       IS NULL
     AND c402_status_fl      < 3
     AND c1900_company_id=v_comp_id;
	--
	RETURN v_pend_cnt;
--
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN ' ';
END get_partnumber_po_pend_qty;
/
