/* Formatted on 2008/06/17 11:00 (Formatter Plus v4.8.0) */
--@"c:\database\functions\operations\get_qty_in_sales_bo.fnc";
--select get_qty_in_sales_bo('608.111') from dual;

CREATE OR REPLACE FUNCTION get_qty_in_sales_bo (
	p_pnum_id	t205_part_number.c205_part_number_id%TYPE
)
	RETURN NUMBER
IS
--
/********************************************************************************************
 * Description	:This function returns the sales back order count for the selected part.
 *********************************************************************************************/
--
	v_sales_cnt    NUMBER;
--
BEGIN
	SELECT NVL (SUM (t502.c502_item_qty), 0)
	  INTO v_sales_cnt
	  FROM t501_order t501, t502_item_order t502
	 WHERE t501.c501_order_id = t502.c501_order_id
	   AND t501.c901_order_type = 2525
	   AND t501.c501_delete_fl IS NULL
	   AND t501.c501_void_fl IS NULL
	   AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
	   AND t501.c501_status_fl = 0
	   AND t502.c205_part_number_id = p_pnum_id;

	RETURN v_sales_cnt;
--
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN 0;
--
END get_qty_in_sales_bo;
/
