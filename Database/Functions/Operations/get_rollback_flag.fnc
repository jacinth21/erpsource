/* Formatted on 2008/06/26 19:50 (Formatter Plus v4.8.0) */
--@"c:\database\functions\operations\get_rollback_flag.fnc";

CREATE OR REPLACE FUNCTION get_rollback_flag (
	  p_cn_id	 t504_consignment.c504_consignment_id%TYPE
	, p_txn_id 	 t412_inhouse_transactions.c504a_loaner_transaction_id%TYPE
)
	RETURN NUMBER
IS
--
/********************************************************************************************
 * Description	:This function returns a flag that determines loaner rollback  
 *********************************************************************************************/
--
	v_status   		NUMBER;
	v_open          NUMBER;
	v_controlled	NUMBER;
	v_verified		NUMBER;
	v_total_txn		NUMBER;
	v_missing_qty	NUMBER;
	v_recon_qty		NUMBER;
	v_msg_status_fl NUMBER;
	v_company_id    t1900_company.c1900_company_id%TYPE;
	v_flag          VARCHAR2(2);
	v_type          t504_consignment.c504_type%TYPE;
--
BEGIN 
																									   
SELECT COUNT(1) total_txn
     , NVL(SUM(CASE WHEN c412_status_fl = 3 THEN 1 ELSE 0 END),0) controlled
	 , NVL(SUM(CASE WHEN c412_status_fl = 4 THEN 1 ELSE 0 END),0) verified 
	 , NVL(SUM(CASE WHEN c412_status_fl <= 2 THEN 1 ELSE 0 END),0) open
  INTO v_total_txn, v_controlled, v_verified, v_open
  FROM t412_inhouse_transactions t412 
 WHERE t412.c412_ref_id = p_cn_id 
   AND t412.c504a_loaner_transaction_id = NVL(p_txn_id, t412.c504a_loaner_transaction_id)
   AND t412.c412_void_fl IS NULL
   AND t412.c412_type NOT IN (50159 , 1006571); -- Excluding Product Loaner Missing Parts, IH Loaner Missing Parts txn

 SELECT NVL(SUM(missing_qty),0), NVL(SUM(reconciled_qty),0) , NVL(MAX(sfl),0)
   INTO v_missing_qty, v_recon_qty, v_msg_status_fl
   FROM
	(SELECT gm_pkg_op_fetch_recon.get_missing_qty( t413.c205_part_number_id, t412.c412_inhouse_trans_id ) missing_qty
	    ,  gm_pkg_op_fetch_recon.get_reconciled_qty( t413.c205_part_number_id, t412.c412_inhouse_trans_id ) reconciled_qty
	    ,  t412.c412_status_fl sfl
	  FROM t413_inhouse_trans_items t413, t412_inhouse_transactions t412
	 WHERE t413.c412_inhouse_trans_id = t412.c412_inhouse_trans_id
	   AND t412.c412_ref_id = p_cn_id 
	   AND t412.c504a_loaner_transaction_id = NVL(p_txn_id, t412.c504a_loaner_transaction_id)
	   AND t413.c413_status_fl  = 'Q'
	   AND t412.c412_type IN (50159 , 1006571)  --  Product Loaner Missing Parts, IH Loaner Missing Parts txn
	   AND t412.c412_void_fl IS NULL 
	   AND t413.c413_void_fl IS NULL);
	   
	SELECT get_compid_frm_cntx() 
      INTO v_company_id  
      FROM dual;
      
     -- v_type --4127 - Product loaner and GMNA only - added in in PC-4980
    BEGIN
	    SELECT C504_TYPE  
	      INTO v_type 
	      FROM t504_consignment 
	     WHERE c504_consignment_id = p_cn_id 
	       AND C504_VOID_FL IS NULL; 
	EXCEPTION WHEN OTHERS
	     THEN
	   	v_type := NULL;
	END;
    ----v_Flag used in PC-4980-Prevent Rollback for the loaner set has open inhouse transactions
    v_flag := NVL(get_rule_value_by_company(v_type,'ROLLBACKLN',v_company_id),'N');
	IF v_flag ='N' AND v_open = v_total_txn AND v_recon_qty = 0 
	THEN 
	 v_status := 0; -- Rollback set and void all inhouse transactions
	ELSIF  v_verified = v_total_txn
	THEN
	 v_status := 1; -- Rollback set
	ELSE
	 v_status := 2; -- Do not rollback set
	END IF;
	RETURN v_status;
--
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN 0;
--
END get_rollback_flag;
/
