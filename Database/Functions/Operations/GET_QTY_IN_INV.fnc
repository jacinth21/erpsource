/* Formatted on 2013/03/26 14:54 (Formatter Plus v4.8.8) */
--@"C:\Database\Functions\Operations\GET_QTY_IN_INV.FNC";
CREATE OR REPLACE FUNCTION GET_QTY_IN_INV (
    p_partnum       IN   t205c_part_qty.c205_part_number_id%TYPE,  
    p_wh_type       IN   t901_code_lookup.c901_code_id%TYPE
)
    RETURN NUMBER
IS
/*  Description     : This Function returns the Availability Qty for given Inventory
*/
    v_avail_qty          NUMBER;
    v_plant_id     		 T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
BEGIN
	SELECT  get_plantid_frm_cntx() INTO v_plant_id FROM DUAL;
    BEGIN       
          SELECT (SUM (rw_avail_qty) - (SUM (rw_alloc_qty))) avail_qty INTO v_avail_qty
            FROM (
                 SELECT T205c.C205_Part_Number_Id pnum, NVL (SUM (c205_available_qty), 0) rw_avail_qty, 0 rw_alloc_qty
                   FROM T205c_Part_Qty T205c
                  WHERE C901_Transaction_Type     IN (p_wh_type)
                    AND T205c.C205_Part_Number_Id =  p_partnum
                    AND t205c.C5040_PLANT_ID 	  =  v_plant_id
                 GROUP BY T205c.C205_Part_Number_Id
               UNION ALL 
			   /*Existing GET_QTY_IN_INV function query used warehouse type( p_wh_type ) as parameter, so we cannot use v205c_part_allocated_qty to get allocated qty. 
  				Instead of that using v205_inhouse_alloc_inv and v205_consignment_alloc_inv and passing warehouse type( p_wh_type ) as parameter to get allocated qty*/
                 SELECT v205_rw.c205_part_number_id, 0 rw_avail_qty, SUM (NVL (c413_item_qty, 0)) qty
                       FROM v205_inhouse_alloc_inv v205_rw, t906_rules t906a, t906_rules t906B
                      WHERE c412_status_fl        IN (2, 3)
                        AND v205_rw.c412_type      = t906b.c906_rule_grp_id
                        AND t906a.c906_rule_grp_id = t906b.c906_rule_grp_id
                        AND t906a.c906_rule_id     = 'MINUS'
                        AND t906a.c906_rule_value  = 'QTY_IN_INV'
                        AND t906b.c906_rule_id     = 'TRANS_TABLE'
                        AND t906b.c906_rule_value  = 'IN_HOUSE'
                        AND v205_rw.C205_Part_Number_Id = p_partnum
                        AND v205_rw.C5040_PLANT_ID 	    = v_plant_id
                   GROUP BY v205_rw.c205_part_number_id
                  UNION ALL
                     SELECT v205_cons.c205_part_number_id, 0 rw_avail_qty, NVL (SUM (v205_cons.c505_item_qty), 0) qty
                       FROM v205_consignment_alloc_inv v205_cons
                      WHERE v205_cons.c504_type IN (4110, 4112)
                        AND v205_cons.c504_status_fl      IN (2, 3)
                        AND v205_cons.c901_warehouse_type = p_wh_type -- For RW only 56001
                    	AND v205_cons.C205_Part_Number_Id = p_partnum
                    	AND v205_cons.C5040_PLANT_ID 	  = v_plant_id
                   GROUP BY v205_cons.c205_part_number_id
          )
GROUP BY pnum;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            v_avail_qty := 0;
    END;
    
    IF v_avail_qty < 0
    THEN
    	v_avail_qty := 0;
    END IF;
    
    RETURN v_avail_qty;
END GET_QTY_IN_INV;
/