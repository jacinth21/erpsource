--@"c:\database\functions\operations\GET_ALPHATEC_UDI.fnc";
create or replace
FUNCTION GET_ALPHATEC_UDI (
  p_udi VARCHAR
)
RETURN VARCHAR 
IS 
  v_gtin   varchar2(50);
  v_lot    varchar2(30);
  v_udi    varchar2(50);
BEGIN
  IF SUBSTR(p_udi,17,2) = '11' THEN
     v_gtin := SUBSTR(p_udi,1,16);
     v_lot := SUBSTR(p_udi,25);
     v_udi :=  CONCAT(v_gtin, v_lot ); --v_gtin + v_lot;
  ELSE
     v_udi := p_udi;
  END IF;   
 
  RETURN v_udi;
END GET_ALPHATEC_UDI;
/
