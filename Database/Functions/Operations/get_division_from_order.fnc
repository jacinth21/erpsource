
--@"c:\Database\Functions\Operations\get_division_from_order.fnc";

/*PC-577- Fedex/UPS  account split-up for shipping transactions
 *  Description    : THIS FUNCTION RETURNS DIVISION ID for Orders
 Parameters       : p_txn_id
*/
CREATE OR REPLACE FUNCTION get_division_from_order(
   p_txn_id      t501_order.c501_order_id%TYPE
)
   RETURN VARCHAR2
IS
   v_division_id   VARCHAR2(1000);
BEGIN
   SELECT COMPID  INTO v_division_id
     FROM v700_territory_mapping_detail v700, t501_order t501
    WHERE t501.c704_account_id = v700.ac_id 
      AND v700.rep_id = t501.c703_sales_rep_id 
      AND t501.c501_order_id= p_txn_id
      AND t501.c501_void_fl IS NULL
      AND ROWNUM = 1;

   RETURN v_division_id;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN '';
END get_division_from_order;
/


