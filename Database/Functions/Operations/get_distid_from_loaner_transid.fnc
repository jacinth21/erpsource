
--@"C:\Database\Functions\Operations\get_distid_from_loaner_transid.fnc";

/*********************************************************************
* Description : To get dist id from inhouse transction id
* Author      : Velu
* param		  : p_ref_id - Inhouse trans id
********************************************************************/

CREATE OR REPLACE FUNCTION get_distid_from_loaner_transid (
	p_ref_id	IN	 t412_inhouse_transactions.c412_inhouse_trans_id%TYPE
)
	RETURN VARCHAR2
IS
	v_dist_id	t504_consignment.c701_distributor_id%TYPE;
	v_ref_id	t504_consignment.c504_consignment_id%TYPE;
BEGIN
	BEGIN
		BEGIN
			SELECT c412_ref_id INTO v_ref_id FROM t412_inhouse_transactions
			WHERE c412_inhouse_trans_id = p_ref_id AND c412_void_fl IS NULL;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
			 v_ref_id := null;
		END;
		
		SELECT C701_DISTRIBUTOR_ID INTO v_dist_id FROM t504_consignment
		WHERE c504_consignment_id = v_ref_id AND c504_void_fl  IS NULL;
	EXCEPTION
			WHEN NO_DATA_FOUND
				THEN
			 v_dist_id := null;
	END;	
 RETURN v_dist_id;
END get_distid_from_loaner_transid;
/
