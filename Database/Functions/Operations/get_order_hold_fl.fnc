/*******************************************************************************
	  * Description : This function used to get the hold flag for the order id
	  * Author		: Suganthi Sharmila
********************************************************************************/
CREATE OR REPLACE FUNCTION GET_ORDER_HOLD_FL(
			P_ORDER_ID IN T501_ORDER.C501_ORDER_ID%TYPE,
      		P_SOURCE  IN VARCHAR2
)
RETURN VARCHAR2
IS
V_HOLD_FL T501_ORDER.C501_HOLD_FL%TYPE;
BEGIN
	IF (P_SOURCE = '50180') THEN
	BEGIN
		SELECT C501_HOLD_FL
	 	INTO V_HOLD_FL
		FROM T501_ORDER
		WHERE C501_ORDER_ID = P_ORDER_ID 
		AND C501_VOID_FL  IS NULL;
	EXCEPTION
	WHEN NO_DATA_FOUND THEN
    RETURN NULL;
 	END;
END IF;
IF V_HOLD_FL IS NULL THEN
RETURN 'N';
END IF;
RETURN V_HOLD_FL;
END GET_ORDER_HOLD_FL;
/

