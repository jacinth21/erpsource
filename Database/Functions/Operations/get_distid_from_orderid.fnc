--@"C:\Database\Functions\Operations\get_distid_from_orderid.fnc";

/*******************************************************
* Purpose: To get distid from order id for T907 table
* Author: Velu
********************************************************/

CREATE OR REPLACE FUNCTION get_distid_from_orderid (
	p_order_id	IN	 t501_order.C501_ORDER_ID%TYPE
)
	RETURN VARCHAR2
IS
	v_dist_id	  t504_consignment.c701_distributor_id%TYPE;
BEGIN
	BEGIN
		SELECT c501_distributor_id INTO v_dist_id 
		FROM t501_order WHERE c501_order_id = p_order_id AND c501_void_fl IS NULL;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_dist_id := null;
	END;
	RETURN v_dist_id;
END get_distid_from_orderid;
/
