/* Formatted on 2011/11/14 13:01 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Operations\get_consign_request_for.fnc";

/*	Description 	: THIS FUNCTION RETURNS t520_request_for for the consignment id
 Parameters 		: p_consign_id
*/

CREATE OR REPLACE FUNCTION get_consign_request_for (
	p_consign_id	IN	 t504_consignment.c504_consignment_id%TYPE
)
	RETURN VARCHAR2
IS
	v_request_for	   t520_request.c520_request_for%TYPE;
BEGIN
	BEGIN
		 SELECT c520_request_for INTO v_request_for
			 FROM t520_request t520, t504_consignment t504
			  WHERE t520.c520_request_id     = t504.c520_request_id
			    AND t504.c504_consignment_id = p_consign_id
			    AND t504.c504_void_fl       IS NULL
			    AND t520.C520_VOID_FL       IS NULL;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END;

	RETURN v_request_for;
END get_consign_request_for;
/
