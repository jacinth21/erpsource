CREATE OR REPLACE FUNCTION GET_QTY_IN_RETURNS_HOLD
(p_part_num T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE)
RETURN NUMBER
IS
/*  Description     : THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters 		: p_code_id
*/
v_hold_qty NUMBER;
v_plant_id     		 T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;

BEGIN
	SELECT  get_plantid_frm_cntx() INTO v_plant_id FROM DUAL;
     BEGIN

       SELECT	 NVL (t205c.c205_available_qty, 0) INTO v_hold_qty
       FROM t205c_part_qty t205c
	   WHERE t205c.c205_part_number_id  = p_part_num
	   AND t205c.C901_Transaction_Type  = 90812 --90812 - RETURNS_HOLD Qty
	   AND t205c.C5040_PLANT_ID 	    = v_plant_id;
	   
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN null;
      END;
     RETURN v_hold_qty;
END GET_QTY_IN_RETURNS_HOLD;
/

