-- @"C:\Database\Functions\Operations\get_ticket_id.fnc";
CREATE OR REPLACE FUNCTION get_ticket_id (
	p_txn_id	t412_inhouse_transactions.c412_inhouse_trans_id%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: This function returns Ticket id for the given transaction id
 Parameters 		: p_txn_id
*/
	v_ticket	   VARCHAR2(40);
BEGIN
	--
	BEGIN
	SELECT DECODE (t412.c412_type, 50159, t9800.c9800_ticket_id, '') ticketd INTO v_ticket
   FROM t412_inhouse_transactions t412, t504a_loaner_transaction t504a, t526_product_request_detail t526
  , t9800_jira_ticket t9800
  WHERE t9800.c9800_ref_id                  = t526.c526_product_request_detail_id
    AND t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id
    AND t504a.c504a_loaner_transaction_id   = t412.c504a_loaner_transaction_id
    AND t412.c412_inhouse_trans_id          = p_txn_id
	AND t9800.c9800_ref_type = 10056 -- REQUEST DETAIL ID PC-918
    AND t412.c412_void_fl                  IS NULL
    AND t504a.c504a_void_fl                IS NULL
    AND t526.c526_void_fl                  IS NULL
    AND t9800.c9800_void_fl                IS NULL;

    EXCEPTION
		WHEN NO_DATA_FOUND THEN
			BEGIN
				SELECT DECODE (t412.c412_type, 50159, t9800.c9800_ticket_id, '') ticketd INTO v_ticket
			   FROM t412_inhouse_transactions t412, t504a_loaner_transaction t504a, t526_product_request_detail t526
			  , t9800_jira_ticket t9800
			  WHERE t9800.c9800_ref_id                  = t526.c525_product_request_id
				AND t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id
				AND t504a.c504a_loaner_transaction_id   = t412.c504a_loaner_transaction_id
				AND t412.c412_inhouse_trans_id          = p_txn_id
				AND t9800.c9800_ref_type = 110521 -- REQUEST ID PC-918
				AND t412.c412_void_fl                  IS NULL
				AND t504a.c504a_void_fl                IS NULL
				AND t526.c526_void_fl                  IS NULL
				AND t9800.c9800_void_fl                IS NULL;

				EXCEPTION
					WHEN NO_DATA_FOUND THEN
						RETURN NULL;
				END;
	END;
	--
	RETURN v_ticket;

END get_ticket_id;
/
