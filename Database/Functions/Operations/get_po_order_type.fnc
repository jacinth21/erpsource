CREATE OR REPLACE FUNCTION get_po_order_type (
	p_po_num   IN   t501_order.c501_customer_po%type,
	p_acc_id	in  t501_order.C704_ACCOUNT_ID%type
)
	RETURN NUMBER
IS
	v_po_type	   t501_order.c901_order_type%TYPE;
BEGIN
	BEGIN
	   SELECT c901_order_type 
		   INTO v_po_type
		   FROM t501_order
		   WHERE c501_customer_po = p_po_num 
		   AND c501_ref_id IS NOT NULL
		   AND c704_account_id = p_acc_id	
		   AND c501_void_fl IS NULL
		   AND c501_delete_fl IS NULL 
		   --AND c503_invoice_id is null 
		   AND NVL (C901_ORDER_TYPE, -9999) NOT IN ( 
        SELECT t906.c906_rule_value 
        FROM t906_rules t906 
        WHERE T906.C906_RULE_GRP_ID = 'ORDERTYPE' 
        AND c906_rule_id = 'EXCLUDE');  
		  
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RETURN NULL;
	END;

	RETURN v_po_type;
	
END get_po_order_type;
/

