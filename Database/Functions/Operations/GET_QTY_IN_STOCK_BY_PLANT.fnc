--@"c:\database\Functions\Operations\get_qty_in_stock_by_plant.fnc"

/* Refer GET_QTY_IN_INVENTORY.fnc*/

CREATE OR REPLACE FUNCTION get_qty_in_stock_by_plant (
 	p_orderid      IN t501_order.c501_order_id%TYPE   
  ,	p_part_num	   IN t205_part_number.c205_part_number_id%TYPE
  , p_plant_id     IN t5040_plant_master.c5040_plant_id%TYPE
)
	RETURN NUMBER
IS
/* Description	  : This returns the Qty in shelf for a particular part
					Its a sql copied from v205_qty_in_stock view both sql should math all the time 
					  For easy maintenance all types are included in the view 	
   Parameters	  : p_part_num , p_plant_id
   Author         : Karthik
*/
	v_shelf_qty    NUMBER;
	v_plant_id     T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
    v_alloc_qty    NUMBER;
--
BEGIN
	BEGIN
		
		SELECT	 NVL (t205c.c205_available_qty, 0)  -  NVL (v205c.c205_allocated_qty, 0)			 
		 	 INTO v_shelf_qty
		  FROM t205c_part_qty t205c
			 , v205c_part_allocated_qty v205c
		 WHERE t205c.c205_part_number_id = p_part_num
		   AND t205c.C901_Transaction_Type  = 90800 --90800 - Inventory Qty
		   AND t205c.c205_part_number_id = v205c.c205_part_number_id(+)
		   AND v205c.c901_transaction_type(+) = '60001' -- FG Inventory
		   AND t205c.C5040_PLANT_ID 	= 	p_plant_id
		   AND t205c.C5040_PLANT_ID     =   v205c.C5040_PLANT_ID(+);
	
		 EXCEPTION
	        WHEN NO_DATA_FOUND
	        THEN
	            RETURN v_shelf_qty;
	END;
	BEGIN 
	      SELECT NVL (SUM (t502.c502_item_qty), 0) alloc_qty
		   			 INTO v_alloc_qty
		           FROM T502_ITEM_ORDER T502, T501_ORDER T501
		          WHERE T502.C501_ORDER_ID    = T501.C501_ORDER_ID
		            AND T502.C501_ORDER_ID    = p_orderid
		            AND C205_PART_NUMBER_ID   = p_part_num
		            AND T502.C901_TYPE        = 50300
		            AND T501.C901_ORDER_TYPE IS NULL
		            AND T502.C502_VOID_FL    IS NULL
		            AND T501.C501_VOID_FL    IS NULL;
		EXCEPTION
	   		 WHEN NO_DATA_FOUND
	   		 THEN
	    		v_alloc_qty := 0;
 	END;
    
v_shelf_qty := v_shelf_qty - v_alloc_qty;
RETURN v_shelf_qty;

END get_qty_in_stock_by_plant;
/
