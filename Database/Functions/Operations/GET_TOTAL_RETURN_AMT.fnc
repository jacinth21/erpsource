/* Formatted on 2007/07/13 15:58 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_total_return_amt (
	p_ra_id   t507_returns_item.c506_rma_id%TYPE
)
	RETURN NUMBER
IS
/*	Description 	: For Selected RA returns the total return value
 Parameters 		: p_ra_id	return id
*/
	v_total 	   NUMBER (15, 2);
BEGIN
	--
	SELECT SUM (c507_item_price * c507_item_qty)
	  INTO v_total
	  FROM t507_returns_item
	 WHERE c506_rma_id = p_ra_id AND c507_status_fl <> 'Q';

	--
	RETURN v_total;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN 0;
END get_total_return_amt;
/
