--@"C:\Database\Functions\Operations\GET_LN_REQ_DET_ID.fnc";
CREATE OR REPLACE
    FUNCTION GET_LN_REQ_DET_ID (
            p_consign_id t504a_loaner_transaction.c504_consignment_id%TYPE)
        RETURN VARCHAR2
    IS
        /*  Description     : THIS FUNCTION RETURNS PRODUCT REQUEST DETAIL ID FOR THE ENTERED CONSIGN ID
        Parameters   : CONSIGNMENT ID
        */
        v_request_id VARCHAR2 (20) ;
    BEGIN
        BEGIN
             SELECT c526_product_request_detail_id
               INTO v_request_id
               FROM t504a_loaner_transaction t504a
              WHERE t504a.c504_consignment_id = p_consign_id
                AND t504a.c504a_void_fl      IS NULL
                AND t504a.c504a_return_dt    IS NULL;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN NULL;
        END;
        RETURN v_request_id;
    END GET_LN_REQ_DET_ID;
    /