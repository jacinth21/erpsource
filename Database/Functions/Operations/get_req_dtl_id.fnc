--@"c:\database\functions\operations\get_req_dtl_id.fnc";
CREATE OR REPLACE FUNCTION get_req_dtl_id 
	(
       p_reqid T525_Product_request.c525_product_request_id%TYPE,
       p_pnum t526_product_request_detail.c205_part_number_id%TYPE
    )
        RETURN NUMBER
    IS
        /* Description  : THIS FUNCTION RETURNS request detail id based on request and partnumber
        Parameters   : p_code_id
        */
        v_reqdtlid t526_product_request_detail.c526_product_request_detail_id%TYPE;
    BEGIN
        BEGIN
            SELECT     t526.c526_product_request_detail_id
                INTO v_reqdtlid
                FROM t526_product_request_detail t526
                WHERE t526.c525_product_request_id = p_reqid
                    AND t526.c205_part_number_id = p_pnum
					AND ROWNUM = 1;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN NULL;
        END;
        RETURN v_reqdtlid;
    END get_req_dtl_id;
/