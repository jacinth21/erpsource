--@"C:\Database\Functions\Operations\GET_REQUEST_SHIP_EMAIL.fnc";

CREATE OR REPLACE FUNCTION get_request_ship_email(
	p_request_id  t907_shipping_info.c907_ref_id%TYPE,
	p_action      VARCHAR2
)
	RETURN VARCHAR2

IS
v_ship_to  		t907_shipping_info.c901_ship_to%TYPE;
v_ship_to_id 	t907_shipping_info.c907_ship_to_id%TYPE;
v_ship_email    VARCHAR2 (100);

BEGIN
	BEGIN
	 SELECT t907.c901_ship_to, t907.c907_ship_to_id
	   INTO v_ship_to, v_ship_to_id
	   FROM t907_shipping_info t907
	  WHERE t907.c907_ref_id   = p_request_id
	    AND t907.c901_source   = 50184
	    AND t907.c907_void_fl IS NULL;
	EXCEPTION
	   WHEN NO_DATA_FOUND THEN
			v_ship_to:=NULL;
	   		v_ship_to_id:=NULL;
	WHEN TOO_MANY_ROWS
        THEN
     SELECT MAX(t907.c901_ship_to), MAX(t907.c907_ship_to_id)
	   INTO v_ship_to, v_ship_to_id
	   FROM t907_shipping_info t907
	  WHERE t907.c907_ref_id   = p_request_id
	    AND t907.c901_source   = 50184
	    AND t907.c907_void_fl IS NULL;
	END;
	IF p_action ='MAIL' 
	THEN
	 v_ship_email:= get_cs_ship_email(v_ship_to,v_ship_to_id);
	ELSIF  p_action ='ADD'
	THEN
	v_ship_email:= GET_USER_NAME(v_ship_to_id) ||'<BR>'||GET_RULE_VALUE('GM-ADD', 'GM-ADD');
	ELSE
	v_ship_email:= NULL;
    END IF;
	RETURN v_ship_email;
	
END get_request_ship_email;
/