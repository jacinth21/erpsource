/******************************************************************
* Author      :  Xun 
* Description : Fetch the associated transaction ID by passing
* the ref_id for any transaction which is  on hold
*****************************************************************/
--@"C:\Database\Functions\Operations\get_associate_txn_id.fnc";

CREATE OR REPLACE FUNCTION get_associate_txn_id (
        p_id IN t504_consignment.c504_consignment_id%TYPE)
    RETURN VARCHAR2
IS
    v_txn t412_inhouse_transactions.c412_inhouse_trans_id%TYPE;
BEGIN
    -- if there's open PSFG txn, then count as hold.
     SELECT c412_inhouse_trans_id
       INTO v_txn
       FROM t412_inhouse_transactions t412 
      WHERE c412_ref_id    = p_id
        AND c412_void_fl  IS NULL
        AND c412_type      = '100406' --Pending shippping to Shelf
        AND c412_status_fl < 4
        AND ROWNUM=1;
    RETURN v_txn;
EXCEPTION
WHEN NO_DATA_FOUND THEN
    RETURN ' ';
END get_associate_txn_id ;
/ 