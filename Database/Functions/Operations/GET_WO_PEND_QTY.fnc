/* Formatted on 2009/05/13 14:15 (Formatter Plus v4.8.0) */
-- @ "C:\Database\Functions\Operations\GET_WO_PEND_QTY.fnc";

CREATE OR REPLACE FUNCTION get_wo_pend_qty (
	p_wo_id 	t408_dhr.c402_work_order_id%TYPE
  , p_qty_ord	NUMBER
)
	RETURN NUMBER
IS
--
/********************************************************************************************
 * Description	:This function returns the Pending Qty in a Work Order
 * Change Details:
 *	 James Jun3,2006 Changed Formatting according to Standards
   James Jun3,2006 Changes for UOM
 *********************************************************************************************/
--
	v_cnt		   NUMBER;
	v_rec_cnt	   NUMBER;
	v_rej_cnt	   NUMBER;
	v_clo_cnt	   NUMBER;
	v_act_cnt	   NUMBER;
	v_uom_cnt	   NUMBER;
	v_wo_uom_str   VARCHAR2 (20);
--
BEGIN
--

	---- if the function returns null, UOM will be 1
	SELECT NVL (get_wo_uom_type_qty (p_wo_id), 'BX/1')
	  INTO v_wo_uom_str
	  FROM DUAL;

	v_uom_cnt	:= TO_NUMBER (NVL (SUBSTR (v_wo_uom_str, INSTR (v_wo_uom_str, '/') + 1), 1));	-- Function return Type/Qty. So stripping Type/ from that

	--Calculating the  sum of the rejections aggregation
	SELECT NVL (SUM (CEIL (c408_qty_rejected / v_uom_cnt)), 0)
	  INTO v_rej_cnt
	  FROM t408_dhr t408
	 WHERE t408.c402_work_order_id = p_wo_id AND t408.c408_void_fl IS NULL AND t408.c408_dhr_id NOT LIKE 'GM-RC%'; 
	 

	SELECT NVL (SUM (NVL(c408_shipped_qty, c408_qty_received)), 0)
	  INTO v_rec_cnt
	  FROM t408_dhr t408
	 WHERE t408.c402_work_order_id = p_wo_id AND t408.c408_void_fl IS NULL AND t408.c408_dhr_id NOT LIKE 'GM-RC%'; 

--
	SELECT NVL (SUM (c409_closeout_qty), 0)
	  INTO v_clo_cnt
	  FROM t409_ncmr
	 WHERE c402_work_order_id = p_wo_id AND c409_void_fl IS NULL;

	IF v_rec_cnt IS NULL
	THEN
		v_rec_cnt	:= 0;
	END IF;

	v_rec_cnt	:= v_rec_cnt * v_uom_cnt;
	v_rej_cnt	:= v_rej_cnt * v_uom_cnt;
--
	v_act_cnt	:= FLOOR (((v_rec_cnt - NVL (v_rej_cnt, 0)) + NVL (v_clo_cnt, 0)) / v_uom_cnt);
--
	v_cnt		:= p_qty_ord - v_act_cnt;

	IF (v_cnt < 0)
	THEN
		RETURN 0;
	END IF;

--
	RETURN v_cnt;
--
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN 0;
--
END get_wo_pend_qty;
/
