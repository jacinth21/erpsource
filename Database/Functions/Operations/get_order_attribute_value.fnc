	/****************************************************************
	  * Description : This function used to get attribut values for DO
	  * Author		: Elango
	 *************************************************************/
	CREATE OR REPLACE  FUNCTION get_order_attribute_value (
		p_orderid	    IN	 t501_order.c501_order_id%TYPE,
		p_attribtype	IN	 t501a_order_attribute.c901_attribute_type%TYPE		
	)
		RETURN VARCHAR2
	IS
		v_order_attr_value VARCHAR2(50);
	BEGIN
		BEGIN
			SELECT c501a_attribute_value
			  INTO v_order_attr_value
			  FROM t501a_order_attribute
			 WHERE c901_attribute_type = p_attribtype 
			   AND c501_order_id = p_orderid 
			   AND c501a_void_fl IS NULL
			   AND ROWNUM = 1
			   ORDER BY C501A_CREATED_DATE DESC;
			RETURN v_order_attr_value;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN NULL;
		END;
	END get_order_attribute_value;
	/