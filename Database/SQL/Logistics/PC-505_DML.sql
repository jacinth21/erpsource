/*
 * PC-505 - Ability to Consign Multiple Sets
 * Insert record to create left link
 */  
INSERT INTO T1520_FUNCTION_LIST
  (
    C1520_FUNCTION_ID,
    C1520_FUNCTION_NM,
    C1520_FUNCTION_DESC,
    C1520_INHERITED_FROM_ID,
    C901_TYPE,
    C1520_VOID_FL,
    C1520_CREATED_BY,
    C1520_CREATED_DATE,
    C1520_LAST_UPDATED_BY,
    C1520_LAST_UPDATED_DATE,
    C1520_REQUEST_URI,
    C1520_REQUEST_STROPT,
    C1520_SEQ_NO,
    C901_DEPARTMENT_ID,
    C901_LEFT_MENU_TYPE
  )
  VALUES
  (
    'CONSIGN_MULTI_SETS',
    'Request - Multi Select Sets',
    'To Ability to Consign Multiple Sets.',
    NULL,
    92262,
    NULL,
    '2193221',
    SYSDATE,
    '2193221',
    NULL,
    '/gmIncShipDetails.do?screenType=Consign&RE_FORWARD=gmMultiProcConsign',  
    NULL,
    NULL,
    '2014',
    '101921'  
  );
  
 
-- create synonym for new package gm_pkg_op_multi_process_consign
CREATE OR REPLACE public synonym gm_pkg_op_multi_process_consign FOR GLOBUS_APP.gm_pkg_op_multi_process_consign;