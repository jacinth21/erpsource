  --create synonym for new table
   create or replace public synonym t922_inventory_transaction_map for GLOBUS_APP.t922_inventory_transaction_map; 
   
  --create synonym for new packages, trigger and view
   create or replace public synonym TRG_5081_LOT_TRACK_INV_LOG for GLOBUS_APP.TRG_5081_LOT_TRACK_INV_LOG;
   create or replace public synonym V205_STERILE_PARTS for GLOBUS_APP.V205_STERILE_PARTS;
   create or replace public synonym GM_PKG_OP_LOT_TRACK_DATA for GLOBUS_APP.GM_PKG_OP_LOT_TRACK_DATA;
   create or replace public synonym GM_PKG_OP_LOT_TRACK_JOB for GLOBUS_APP.GM_PKG_OP_LOT_TRACK_JOB;