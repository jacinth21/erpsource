DELETE FROM t906_rules where c906_rule_id = '1006504' and c906_rule_grp_id = 'CHK_DIST_COMPANY';

---Insert a rule value for the Rule_grp_id to get the distributor id
INSERT INTO T906_RULES(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_RULE_GRP_ID,C906_CREATED_DATE,C906_CREATED_BY)
VALUES (S906_RULE.NEXTVAL,'1006504','To get the distributor id for the sales rep- Event setup','Y','CHK_DIST_COMPANY',current_date,'2765071');