 /*TO Enable/Disable Left Link Access-PMT-38197 - Lot Track Transactions Log */
 
 INSERT 
   INTO t1520_function_list 
				(C1520_FUNCTION_ID,
				C1520_FUNCTION_NM,
				C1520_FUNCTION_DESC,
				C901_TYPE,
				C1520_CREATED_BY,
				C1520_CREATED_DATE,
				C1520_REQUEST_URI,
				C1520_REQUEST_STROPT,
				C901_DEPARTMENT_ID,
				C901_LEFT_MENU_TYPE) 
 VALUES 
				('LOT-TRACK-TRANS-LOG',
				'Lot Track Transaction Log Report',
				'This reports is used to display the Lot Track Transactions Log',
				92262,
				'303013',
				SYSDATE,
				'/gmLotTrackTxnLogRpt.do?method=loadLotTrackTxnLogDetails',
				'Load',
				2014,
				101920);
				
				
