INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY)
VALUES (S906_RULE.NEXTVAL,'gminc\sw10bld01','null','TRUE',CURRENT_DATE,'WHITELISTSERVER','2765071');
commit;

INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY)
VALUES (S906_RULE.NEXTVAL,'sw10bld01','null','TRUE',CURRENT_DATE,'WHITELISTSERVER','2765071');
commit;

UPDATE t202_project
SET c202_lot_track_fl    = 'Y',
  c202_last_updated_by   = '2765071',
  c202_last_updated_date = CURRENT_DATE
WHERE c202_project_id IN ('GM18-225','GM18-224','GM17-213','GM17-212','GM17-211','GM17-210','GM17-209','GM17-208','GM17-205','GM17-202','GM16-190','GM16-183','GM15-182','GM15-176','GM15-173','GM15-168');
commit;

update t502b_item_order_usage set  c502b_job_run_fl='Y' ;
update t214_transactions set c214_job_run_fl = 'Y';
update t907_shipping_info set c907_job_run_fl = 'Y';
commit;
update t502b_item_order_usage set  c502b_job_run_fl=NULL WHERE c205_part_number_id IN (SELECT c205_part_number_id FROM v205_sterile_parts);
update t214_transactions set c214_job_run_fl=NULL WHERE c205_part_number_id IN (SELECT c205_part_number_id FROM v205_sterile_parts);
commit;
execute gm_pkg_op_lot_track_job.gm_initiate_job();    
commit;
execute gm_pkg_op_lot_track_job.gm_set_consign_job();
commit; 
execute gm_pkg_op_lot_track_job.gm_order_usage_job();  
commit;

