-- Insert Script for RFID Status 

DELETE from T901_CODE_LOOKUP where C901_CODE_GRP = 'RFIDST' and C901_CODE_ID IN (30004,30006,30007,30008);

INSERT into T901_CODE_LOOKUP (C901_CODE_ID,C901_CODE_NM,C901_CODE_GRP,C901_ACTIVE_FL,C901_CODE_SEQ_NO,C902_CODE_NM_ALT,C901_CONTROL_TYPE,C901_CREATED_DATE,C901_CREATED_BY,C901_LAST_UPDATED_DATE,C901_LAST_UPDATED_BY,C901_VOID_FL,C901_LOCK_FL,C901_LAST_UPDATED_DATE_UTC) 
values (30004,'TAG NOT AVAILABLE','RFIDST',1,null,null,null,SYSDATE,'3178209',null,null,null,null,null);

INSERT into T901_CODE_LOOKUP (C901_CODE_ID,C901_CODE_NM,C901_CODE_GRP,C901_ACTIVE_FL,C901_CODE_SEQ_NO,C902_CODE_NM_ALT,C901_CONTROL_TYPE,C901_CREATED_DATE,C901_CREATED_BY,C901_LAST_UPDATED_DATE,C901_LAST_UPDATED_BY,C901_VOID_FL,C901_LOCK_FL,C901_LAST_UPDATED_DATE_UTC) 
values (30006,'Tag is valid and Status of the tag in Pending Return','RFIDST',1,null,null,null,SYSDATE,'3178209',null,null,null,null,null);

INSERT into T901_CODE_LOOKUP (C901_CODE_ID,C901_CODE_NM,C901_CODE_GRP,C901_ACTIVE_FL,C901_CODE_SEQ_NO,C902_CODE_NM_ALT,C901_CONTROL_TYPE,C901_CREATED_DATE,C901_CREATED_BY,C901_LAST_UPDATED_DATE,C901_LAST_UPDATED_BY,C901_VOID_FL,C901_LOCK_FL,C901_LAST_UPDATED_DATE_UTC) 
values (30007,'Tag is valid and Status of the tag not in Pending Return','RFIDST',1,null,null,null,SYSDATE,'3178209',null,null,null,null,null);

INSERT into T901_CODE_LOOKUP (C901_CODE_ID,C901_CODE_NM,C901_CODE_GRP,C901_ACTIVE_FL,C901_CODE_SEQ_NO,C902_CODE_NM_ALT,C901_CONTROL_TYPE,C901_CREATED_DATE,C901_CREATED_BY,C901_LAST_UPDATED_DATE,C901_LAST_UPDATED_BY,C901_VOID_FL,C901_LOCK_FL,C901_LAST_UPDATED_DATE_UTC) 
values (30008,'Tag is valid and Status of tag not in Pending Return-Tag already updated in previous batch-Duplicate','RFIDST',1,null,null,null,SYSDATE,'3178209',null,null,null,null,null);

--Insert script for RFID status into Rule table

DELETE from T906_Rules where C906_Rule_Grp_Id = 'RFIDSTATUS' and C906_Rule_Id IN ('SUCCESS','FAILURE','INVALID');

INSERT Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,C906_Last_Updated_Date,C906_Last_Updated_By,C901_Rule_Type,C906_Active_Fl,C906_Void_Fl,C1900_Company_Id,C906_Last_Updated_Date_Utc) Values (S906_Rule.Nextval,'SUCCESS',null,'30001/Critical',Sysdate,'RFIDSTATUS','839135',null,null,null,'Y',null,1000,null);

INSERT Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,C906_Last_Updated_Date,C906_Last_Updated_By,C901_Rule_Type,C906_Active_Fl,C906_Void_Fl,C1900_Company_Id,C906_Last_Updated_Date_Utc) Values (S906_Rule.Nextval,'FAILURE',null,'30002/No Critical',Sysdate,'RFIDSTATUS','839135',null,null,null,'Y',null,1000,null);

INSERT Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,C906_Last_Updated_Date,C906_Last_Updated_By,C901_Rule_Type,C906_Active_Fl,C906_Void_Fl,C1900_Company_Id,C906_Last_Updated_Date_Utc) Values (S906_Rule.Nextval,'INVALID',null,'30003/Invalid Token',Sysdate,'RFIDSTATUS','839135',null,null,null,'Y',null,1000,null);

CREATE OR REPLACE PUBLIC SYNONYM gm_pkg_op_rfid_process FOR GLOBUS_APP.gm_pkg_op_rfid_process;    

DELETE from T906_Rules where C906_Rule_Grp_Id = 'RFIDCOMP' and C906_Rule_Id IN ('1000');
INSERT Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,C906_Last_Updated_Date,C906_Last_Updated_By,C901_Rule_Type,C906_Active_Fl,C906_Void_Fl,C1900_Company_Id,C906_Last_Updated_Date_Utc) Values (S906_Rule.Nextval,'1000',null,'Y',Sysdate,'RFIDCOMP','706328',null,null,null,'Y',null,1000,null);

UPDATE t504a_consignment_loaner SET c504a_status_updated_date=c504a_last_updated_date WHERE c5040_plant_id=3000 AND c504a_void_fl is null AND c504a_status_updated_date IS  NULL;
