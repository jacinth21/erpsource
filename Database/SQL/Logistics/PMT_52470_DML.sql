--insert the type(WIP set) in code lookup table
INSERT INTO t901_code_lookup(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES(26240846,'WIP Set','LOCTP','1','','','','2598779',CURRENT_DATE);

DELETE from t901a_lookup_access where c901_access_code_id = 2014 and c901_code_id IN (26240846);
--insert the type(WIP set) in acccess table 
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2014, 26240846, 1000);

DELETE from t901a_lookup_access where c901_access_code_id = 2024 and c901_code_id IN (93320,93336,93501,93502,93503,4000338,56005,93354,93355);
DELETE from t901a_lookup_access where c901_access_code_id = 2001 and c901_code_id IN (93320,93336,93501,93502,93503,4000338,56005,93354,93355);
DELETE from t901a_lookup_access where c901_access_code_id = 2003 and c901_code_id IN (93320,93336,93501,93502,93503,4000338,56005,93354,93355);

DELETE from t901a_lookup_access where c901_access_code_id = 2010 and c901_code_id IN (93320,93336,93501,93502,93503,4000338,56005,93354,93355);
DELETE from t901a_lookup_access where c901_access_code_id = 2009 and c901_code_id IN (93320,93336,93501,93502,93503,4000338,56005,93354,93355);
DELETE from t901a_lookup_access where c901_access_code_id = 2005 and c901_code_id IN (93320,93336,93501,93502,93503,4000338,56005,93354,93355);
DELETE from t901a_lookup_access where c901_access_code_id = 2006 and c901_code_id IN (93320,93336,93501,93502,93503,4000338,56005,93354,93355);
DELETE from t901a_lookup_access where c901_access_code_id = 2022 and c901_code_id IN (93320,93336,93501,93502,93503,4000338,56005,93354,93355);
DELETE from t901a_lookup_access where c901_access_code_id = 2015 and c901_code_id IN (93320,93336,93501,93502,93503,4000338,56005,93354,93355);
DELETE from t901a_lookup_access where c901_access_code_id = 2004 and c901_code_id IN (93320,93336,93501,93502,93503,4000338,56005,93354,93355);
DELETE from t901a_lookup_access where c901_access_code_id = 2020 and c901_code_id IN (93320,93336,93501,93502,93503,4000338,56005,93354,93355);
DELETE from t901a_lookup_access where c901_access_code_id = 2026 and c901_code_id IN (93320,93336,93501,93502,93503,4000338,56005,93354,93355);
DELETE from t901a_lookup_access where c901_access_code_id = 2012 and c901_code_id IN (93320,93336,93501,93502,93503,4000338,56005,93354,93355);

--insert the types(Dropdown values) for existing in access table
--2024 :=Inventory department
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2024, 93320, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2024, 93336, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2024, 93501, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2024, 93502, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2024, 93503, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2024, 4000338, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2024, 56005, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2024, 93354, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2024, 93355, 1000);

--2001 :=C/S department
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2001, 93320, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2001, 93336, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2001, 93501, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2001, 93502, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2001, 93503, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2001, 4000338, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2001, 56005, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2001, 93354, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2001, 93355, 1000);

--2003 :=Operations department
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2003, 93320, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2003, 93336, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2003, 93501, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2003, 93502, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2003, 93503, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2003, 4000338, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2003, 56005, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2003, 93354, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2003, 93355, 1000);

--2010 :=Purchasing department
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2010, 93320, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2010, 93336, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2010, 93501, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2010, 93502, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2010, 93503, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2010, 4000338, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2010, 56005, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2010, 93354, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2010, 93355, 1000);

--2009 :=Quality department
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2009, 93320, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2009, 93336, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2009, 93501, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2009, 93502, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2009, 93503, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2009, 4000338, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2009, 56005, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2009, 93354, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2009, 93355, 1000);

--2005 :=Sales department
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2005, 93320, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2005, 93336, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2005, 93501, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2005, 93502, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2005, 93503, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2005, 4000338, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2005, 56005, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2005, 93354, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2005, 93355, 1000);

--2006 :=Admin department
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2006, 93320, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2006, 93336, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2006, 93501, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2006, 93502, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2006, 93503, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2006, 4000338, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2006, 56005, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2006, 93354, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2006, 93355, 1000);

--2022 :=Costing department
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2022, 93320, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2022, 93336, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2022, 93501, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2022, 93502, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2022, 93503, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2022, 4000338, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2022, 56005, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2022, 93354, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2022, 93355, 1000);

--2015 :=Executive department
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2015, 93320, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2015, 93336, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2015, 93501, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2015, 93502, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2015, 93503, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2015, 4000338, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2015, 56005, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2015, 93354, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2015, 93355, 1000);

--2004 :=Product Development department
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2004, 93320, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2004, 93336, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2004, 93501, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2004, 93502, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2004, 93503, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2004, 4000338, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2004, 56005, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2004, 93354, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2004, 93355, 1000);

--2020 :=A/R department
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2020, 93320, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2020, 93336, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2020, 93501, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2020, 93502, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2020, 93503, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2020, 4000338, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2020, 56005, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2020, 93354, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2020, 93355, 1000);

--2026 :=Pricing department
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2026, 93320, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2026, 93336, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2026, 93501, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2026, 93502, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2026, 93503, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2026, 4000338, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2026, 56005, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2026, 93354, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2026, 93355, 1000);

--2012 :=Shipping department
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2012, 93320, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2012, 93336, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2012, 93501, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2012, 93502, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2012, 93503, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2012, 4000338, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2012, 56005, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2012, 93354, 1000);
      
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2012, 93355, 1000);