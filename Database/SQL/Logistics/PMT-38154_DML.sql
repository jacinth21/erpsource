/*TO Enable/Disable Left Link Access-PMT-38154 - Lot Track Report(Non Tissue) */
 
 INSERT INTO t1520_function_list 
 			 (C1520_FUNCTION_ID, 
 			  C1520_FUNCTION_NM,
 			  C1520_FUNCTION_DESC,
 			  C901_TYPE,
 			  C1520_CREATED_BY,
			  C1520_CREATED_DATE,
			  C1520_REQUEST_URI,
			  C1520_REQUEST_STROPT,
			  C901_DEPARTMENT_ID,
			  C901_LEFT_MENU_TYPE) 
      VALUES ('LOTTRACKNONTISSUERPT',
      		  'Lot Track Report (Non Tissue)',
      		  'This reports is used to display the Lot Track Report (Non Tissue)',
			  92262,
			  '303013',
			  CURRENT_DATE,
			  '/gmLotTrackNonTissueRpt.do?method=fetchLotTrackNonTissueDetails',
			  'Load',
			  2014,
			  101920);