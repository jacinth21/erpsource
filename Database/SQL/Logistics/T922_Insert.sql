--Insert values from t214_transactions to t922_inventory_transaction_map table 

	 DELETE FROM t922_inventory_transaction_map;

     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'4312','Return','t506','90812','','','','T214');
     
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'4313','Quarantine','t504','90800','90813','','','T214');

     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'120602','DHR to Finished Goods','t412','90800','','','','T214');   

     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'120618','Inv Adjustment to Quarantine','t412','90813','','','','T214');  
     
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'120454','Bulk to Quarantine','t504','90813','90814','','','T214');  
     
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'56044','RW Inventory to Inv Adj','t412','','56001','','','T214'); 
     
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'56025','Raw Mat to RW Inventory','t412','56001','','','','T214'); 
     
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'4317','Mfg Req/Rel','t303','','90802','','','T214'); 
     
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'4324','Inhouse Inventory','t412','90816','90816','','','T214'); 
     
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'400078','Shelf to Loaner Extension w/Replenish','t412','','90800','','','T214');
     
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'120606','MW to Finished Goods','t412','90800','','','','T214');
     
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'120615','Repack To Inv Adjustment','t412','','90815','','','T214');
     
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'1006614','InHouse Inventory to Qurantine','t412','90813','','','','T214');
     
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'120624','Rework DHR to Repack','t412','90815','','','','T214');
     
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'1006470','BULK to InHouse Inventory','t412','','90814','','','T214');
     
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'56027','DHR to RW Inventory','t412','56001','','','','T214'); 
     
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'26240383','In-Transit to PN','t412','90815','','','','T214');  
     
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'4319','Work Order','t402','90801','90801','','','T214');  
     
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'4119','In-House Loaner','t412','90813','90800','','','T214');  
     
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'400075','Shelf to Inhouse Set','t412','','90800','','','T214'); 
     
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'120603','DHR to Quarantine','t412','90813','','','','T214');  
     
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'120620','Quarantine to Repack','t412','90815','90813','','','T214');  
          
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'40052','Raw Material to Quarantine','t412','90813','','','','T214');  
               
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'1006472','Quarantine to InHouse Inventory','t412','','90813','','','T214');  
                    
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'26240381','In-Transit to FG','t412','','90800','','','T214');  
                         
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'4310','Sales','t501','','90800','4000339','','T214');  
                              
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'120621','Quarantine to Inv Adjustment','t412','','90813','','','T214'); 
                                   
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'120614','Quarantine to Bulk','t412','90814','90813','','','T214'); 
                                        
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'120432','Returns To Repackage','t504','90815','90812','','','T214'); 
                                             
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'120629','Quarantine to Returns','t504','','90813','','','T214'); 
                                                  
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'1006617','Request IH Inv from Shelf','t412','','90800','','','T214'); 
                                                       
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'30002','Return','t303','90802','','90812','','T214'); 
                                                            
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'1006613','InHouse Inventory to Bulk','t412','90814','','','','T214'); 
                                                                 
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'1006474','Returns Hold to InHouse Inventory','t412','','90812','','','T214'); 
                                                                      
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'4118','Bulk to Shelf','t412','90800','90814','','','T214'); 
                                                                           
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'120488','Repackage to Shelf','t504,t412','90800','90815','','','T214'); 
                                                                                
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'4322','Raw Material Inv','t402,t215,t412','90802','90802','','','T214'); 
                                                                                     
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'56024','Inv Adj to RW Inventory','t412','56001','','','','T214'); 
                                                                                          
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'120623','Rework DHR to Bulk','t412','90814','','','','T214'); 
                                                                                               
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'56022','Quarantine to RW Inventory','t412','56001','90813','','','T214'); 
                                                                                                    
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'56020','FG to RW Inventory','t412','56001','90800','','','T214'); 
                                                                                                             
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'56023','Packaging to RW Inventory','t412','56001','90815','','','T214'); 
                                                                                                                  
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'120607','MW to Bulk','t412','90814','','','','T214'); 
                                                                                                                       
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'56021','Bulk to RW Inventory','t412','56001','90814','','','T214'); 
                                                                                                                            
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'4311','Consignment','t504','90814','90812','4000339','26240819','T214'); 
                                                                                                                    
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'400066','Shelf to Inventory Adjustment','t412','','90800','','','T214'); 
                                                                                                                         
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'120468','Bulk to Inv Adjustment','t504','','90814','','','T214'); 
                                                                                                                              
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'120622','Rework DHR to Finished goods','t412','','90800','','','T214');
                                                                                                                                   
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'400072','Shelf to Built Set','t412','','90800','','','T214'); 
                                                                                                                                        
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'56040','RW Inventory to FG','t412','90800','56001','','','T214'); 
                                                                                                                                             
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'56041','RW Inventory to Bulk','t412','90814','56001','','','T214'); 
                                                                                                                                                  
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'56045','RW Inventory to Raw Mat','t412','','56001','','','T214'); 
                                                                                                                                                       
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'120433','Returns To Quarantine','t504','90813','90812','','','T214'); 
                                                                                                                                                            
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'100064','Loaner Reconciliation to Repack','t412','90815','','','','T214'); 
                                                                                                                                                                     
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'120600','DHR to Bulk','t412','90814','','','','T214'); 
                                                                                                                                                                          
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'120628','Shelf to Returns','t412','','90800','','','T214'); 
                                                                                                                                                                               
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'400069','Inventory Adjustment to Shelf','t412','90800','','','','T214'); 
                                                                                                                                                                                    
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'4000645','Part Re-Designation','t412','90800','90815','','','T214'); 
                                                                                                                                                                                         
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'40054','Shelf to Raw Material','t412','','90800','','','T214'); 
                                                                                                                                                                                              
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'1006616','InHouse Inventory to Shelf','t412','90800','','','','T214'); 
                                                                                                                                                                                                  
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'400071','Built Set to Shelf','t412','90800','','','','T214');  
                                                                                                                                                                                                       
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'40053','Raw Material to Shelf','t412','90800','','','','T214');  
                                                                                                                                                                                                            
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'56043','RW Inventory to Packaging','t412','90815','56001','','','T214');  
                                                                                                                                                                                                                 
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'120461','Bulk to Repackage','t504','90815','90814','','','T214'); 
                                                                                                                                                                                                                      
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'4117','Shelf to Bulk','t412','90814','90800','','','T214'); 
                                                                                                                                                                                                                           
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'120616','Repack To Quarantine','t412','90813','90815','','','T214'); 
                                                                                                                                                                                                                                
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'120601','DHR to Repack','t412','90815','','','','T214'); 
                                                                                                                                                                                                                                     
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'120617','Inv Adjustment to Bulk','t412','90814','','','','T214'); 
                                                                                                                                                                                                                                          
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'120619','Inv Adjustment to Repack','t412','90815','','','','T214'); 
                                                                                                                                                                                                                                               
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'4314','Purchase','t408,t401','90813','90813','','','T214'); 
                                                                                                                                                                                                                                                    
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'4114','Shelf to Repack','t504','90815','90800','','','T214'); 
                                                                                                                                                                                                                                                         
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'56042','RW Inventory to Quarantine','t412','90813','56001','','','T214'); 
                                                                                                                                                                                                                                                                  
     INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'26240382','In-Transit to QN','t412','90813','','','','T214'); 
	 
	 INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'400074','Shelf to Loaner','t412','','90800','','','Loaner'); 
	 
	 INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'4127','Product Loaner','t412','90813','90814','','','Loaner'); 
	 
	 INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY)VALUES(s922_inventory_transaction_map.NEXTVAL,'400073','Loaner to Shelf','t412','90800','','','','Loaner'); 
	 
     UPDATE t922_inventory_transaction_map set c922_addl_minus_warehouse=null,c922_addl_plus_warehouse=null,c922_minus_warehouse=null,c922_plus_warehouse=null  where c901_type=4311; 
     
	 INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_VALUE,C922_CATEGORY,C922_VOID_FL,C922_CREATED_BY,C922_CREATED_DATE,C922_LAST_UPDATED_BY,C922_LAST_UPDATED_DATE) 
	 VALUES (GLOBUS_APP.S922_inventory_transaction_map.NEXTVAL,400088,'Consignment',4000339,90800,null,null,'t504','T214',null,null,null,null,null);
	 
	 INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_VALUE,C922_CATEGORY,C922_VOID_FL,C922_CREATED_BY,C922_CREATED_DATE,C922_LAST_UPDATED_BY,C922_LAST_UPDATED_DATE) 
	 VALUES (GLOBUS_APP.S922_inventory_transaction_map.NEXTVAL,40057,'ICT',4000339,90800,null,null,'t504','T214',null,null,null,null,null);

--	 INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_VALUE,C922_CATEGORY,C922_VOID_FL,C922_CREATED_BY,C922_CREATED_DATE,C922_LAST_UPDATED_BY,C922_LAST_UPDATED_DATE,C922_LAST_UPDATED_DATE_UTC) 
--	 VALUES (GLOBUS_APP.S922_inventory_transaction_map.NEXTVAL,4110,'Consignment',26240819,90814,null,null,'t504','T214',null,null,null,null,null,null);

	 UPDATE t922_inventory_transaction_map set c901_type=4116,c922_lot_transaction_name='Quarantine TO FG'  where c901_type=4313;   

	 INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_VALUE,C922_CATEGORY,C922_VOID_FL,C922_CREATED_BY,C922_CREATED_DATE,C922_LAST_UPDATED_BY,C922_LAST_UPDATED_DATE) 
	 VALUES (GLOBUS_APP.S922_inventory_transaction_map.NEXTVAL,4113,'FG to Quarantine',90813,90800,null,null,'t504','T214',null,null,null,null,null);

	 UPDATE t922_inventory_transaction_map set c922_addl_plus_warehouse=null,c922_plus_warehouse=4000339  where c901_type=4310;
	 
	 INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_VALUE,C922_CATEGORY,C922_VOID_FL,C922_CREATED_BY,C922_CREATED_DATE,C922_LAST_UPDATED_BY,C922_LAST_UPDATED_DATE,c922_LOCATION_FROM) 
	 VALUES (GLOBUS_APP.S922_inventory_transaction_map.NEXTVAL,4110,'Consignment',null,90800,null,null,'t504','T214',null,null,null,null,null,'LOCATION');
	 
	 INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_VALUE,C922_CATEGORY,C922_VOID_FL,C922_CREATED_BY,C922_CREATED_DATE,C922_LAST_UPDATED_BY,C922_LAST_UPDATED_DATE,c922_LOCATION_FROM) 
	 VALUES (GLOBUS_APP.S922_inventory_transaction_map.NEXTVAL,4110,'Consignment',4000339,null,null,null,'t504','T214',null,null,null,null,null,'FS');
	 
	 INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_VALUE,C922_CATEGORY,C922_VOID_FL,C922_CREATED_BY,C922_CREATED_DATE,C922_LAST_UPDATED_BY,C922_LAST_UPDATED_DATE,c922_LOCATION_FROM) 
	 VALUES (GLOBUS_APP.S922_inventory_transaction_map.NEXTVAL,3301,'Consignment-set',26240819,null,null,null,'t504','T214',null,null,null,null,null,'FS');
	 
 	 INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_VALUE,C922_CATEGORY,C922_VOID_FL,C922_CREATED_BY,C922_CREATED_DATE,C922_LAST_UPDATED_BY,C922_LAST_UPDATED_DATE,c922_LOCATION_FROM) 
	 VALUES (GLOBUS_APP.S922_inventory_transaction_map.NEXTVAL,3301,'Consignment-set',null,90814,null,null,'t504','T214',null,null,null,null,null,'LOCATION');
	 
	 UPDATE t922_inventory_transaction_map set c922_minus_warehouse = 4000339 where c901_type = '4312'; 
	 
	 update t922_inventory_transaction_map set c922_void_fl='Y'  where c901_type=4319;
	 update t922_inventory_transaction_map set c922_location_from='LOCATION'  where c901_type=120468;