 /*TO Enable/Disable Left Link Access-PMT-38197 - Lot Track Transactions Log */
 
 INSERT 
   INTO t1520_function_list 
				(C1520_FUNCTION_ID,
				C1520_FUNCTION_NM,
				C1520_FUNCTION_DESC,
				C901_TYPE,
				C1520_CREATED_BY,
				C1520_CREATED_DATE,
				C1520_REQUEST_URI,
				C1520_REQUEST_STROPT,
				C901_DEPARTMENT_ID,
				C901_LEFT_MENU_TYPE) 
 VALUES 
				('LOT_DETAIL_REPORT',
				'Lot Track Detail Report',
				'This reports is used to display the Lot Track Detail',
				92262,
				'303013',
				SYSDATE,
				'/gmLotDetailRpt.do?method=loadLotDetailsReport',
				'Load',
				2014,
				101920);
				
--Create synonym for new package
CREATE OR REPLACE  public synonym gm_pkg_op_lot_detail_report FOR GLOBUS_APP.gm_pkg_op_lot_detail_report;
	
				
--Update the left link for Lot Track Report for Sterile
UPDATE  t1520_function_list set c1520_request_uri = '/gmLotTrackNonTissueRpt.do?method=loadLotTrackNonTissueDetails' where C1520_FUNCTION_ID = 'LOTTRACKNONTISSUERPT';
				
				
