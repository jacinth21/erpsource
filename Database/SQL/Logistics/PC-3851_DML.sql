--Synonym for new package
CREATE OR REPLACE  public synonym gm_pkg_ls_bo_item_req_txn FOR GLOBUS_APP.gm_pkg_ls_bo_item_req_txn;

--Synonyms for New View
create or replace public synonym v521_bo_fulfill_list FOR GLOBUS_APP.v521_bo_fulfill_list;

--DML For left Link
INSERT
   INTO t1520_function_list
    (
        C1520_FUNCTION_ID, C1520_FUNCTION_NM, C1520_FUNCTION_DESC
      , C1520_INHERITED_FROM_ID, C901_TYPE, C1520_VOID_FL
      , C1520_CREATED_BY, C1520_CREATED_DATE, C1520_LAST_UPDATED_BY
      , C1520_LAST_UPDATED_DATE, C1520_REQUEST_URI, C1520_REQUEST_STROPT
      , C1520_SEQ_NO, C901_DEPARTMENT_ID
    )
    VALUES
    (
        'BO_ITEM_REQ_REL', 'Release Item BO', 'BO Item Request Release'
      , NULL, 92262, NULL
      , '706322', SYSDATE, NULL
      , NULL,  '/gmBOItemReqReleaseAction.do?method=loadBOItemReqDetails'||'&'||'initialFullchk=yes', null
      , NULL, 2000
    ) ;
    
    