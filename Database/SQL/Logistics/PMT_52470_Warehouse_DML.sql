--code id for warehouse in t901_code_lookup
INSERT INTO t901_code_lookup(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES(107366,'Bulk Warehouse','LOCWT','1','1','107366','','2277820',CURRENT_DATE);
--warehouse table insert for blk warehuse
INSERT INTO t5051_inv_warehouse (c5051_inv_warehouse_id,c5051_warehouse_sh_nm,c5051_inv_warehouse_nm,c901_warehouse_type,c901_status_id,c5051_created_by,c5051_created_date) 
VALUES (7,'BK','Bulk Warehouse','107366','1','505012',sysdate);
DELETE from t901a_lookup_access where c901_access_code_id = 2014 and c901_code_id IN (107366);		
--Access table entry for ware house(BLK warehouse) US
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2014, 107366, 1000);
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2014, 107366, 1002);
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2014, 107366, 1018);
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2014, 107366, 1010);
INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id, c1900_company_id)
VALUES (s901a_lookup_access.NEXTVAL, 2014, 107366, 1012);
