--INSERT IN CODE LOOKUP FOR TCS

INSERT INTO t901_code_lookup (
    c901_code_id,
    c901_code_nm,
    c901_code_grp,
    c901_active_fl,
    c901_code_seq_no,
    c902_code_nm_alt,
    c901_control_type,
    c901_created_by,
    c901_created_date
) VALUES (
    112020,
    'TCS %',
    'ACCATB',
    '1',
    '12',
    'TCS',
    '',
    '2765071',
    current_date
);

--UPDATE IN CODE LOOKUP 

UPDATE t901_code_lookup
SET
    c901_code_nm = 'Local Drug Lic # 2',
    c901_last_updated_date = sysdate,
    c901_last_updated_by = '2765071'
WHERE
    c901_code_id = 101189;