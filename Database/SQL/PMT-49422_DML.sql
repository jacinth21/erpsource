 -- New Rule grp  
delete from t906_rules where c906_rule_id in ('1026_2000_50202_REGULAR','1026_2000_50203_REGULAR'); 
 
INSERT INTO T906_RULES(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID, C906_CREATED_BY,
c906_last_updated_date,c906_last_updated_by,c901_rule_type,c906_active_fl,c906_void_fl,c1900_company_id,c906_last_updated_date_utc)
VALUES (S906_RULE.NEXTVAL,'1026_2000_50203_REGULAR','Invoice template for Japan - Division as Spine','/GmJPCrDbInvoicePrint.jasper',CURRENT_DATE,'INVOICE_JASPER','2173387',NULL,NULL,NULL,NULL, NULL,NULL,NULL);

INSERT INTO T906_RULES(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID, C906_CREATED_BY,
C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID,C906_LAST_UPDATED_DATE_UTC)
values (s906_rule.nextval,'1026_2000_50202_REGULAR','Invoice template for Japan - Division as Spine','/GmJPCrDbInvoicePrint.jasper',current_date,'INVOICE_JASPER','2173387',null,null,null,null, null,null,null);

INSERT INTO T906_RULES(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID, C906_CREATED_BY,
c906_last_updated_date,c906_last_updated_by,c901_rule_type,c906_active_fl,c906_void_fl,c1900_company_id,c906_last_updated_date_utc)
VALUES (S906_RULE.NEXTVAL,'1026_2006_50203_REGULAR','Invoice template for Japan - Division as JOM','/GmJPCrDbInvoicePrint.jasper',CURRENT_DATE,'INVOICE_JASPER','2173387',NULL,NULL,NULL,NULL, NULL,NULL,NULL);

INSERT INTO T906_RULES(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID, C906_CREATED_BY,
C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID,C906_LAST_UPDATED_DATE_UTC)
values (s906_rule.nextval,'1026_2006_50202_REGULAR','Invoice template for Japan - Division as JOM','/GmJPCrDbInvoicePrint.jasper',current_date,'INVOICE_JASPER','2173387',null,null,null,null, null,null,null);

INSERT INTO T906_RULES(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID, C906_CREATED_BY,
c906_last_updated_date,c906_last_updated_by,c901_rule_type,c906_active_fl,c906_void_fl,c1900_company_id,c906_last_updated_date_utc)
VALUES (S906_RULE.NEXTVAL,'1026_2004_50203_REGULAR','Invoice template for Japan - Division as Robotics','/GmJPCrDbInvoicePrint.jasper',CURRENT_DATE,'INVOICE_JASPER','2173387',NULL,NULL,NULL,NULL, NULL,NULL,NULL);

INSERT INTO T906_RULES(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID, C906_CREATED_BY,
C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID,C906_LAST_UPDATED_DATE_UTC)
values (s906_rule.nextval,'1026_2004_50202_REGULAR','Invoice template for Japan - Division as Robotics','/GmJPCrDbInvoicePrint.jasper',current_date,'INVOICE_JASPER','2173387',null,null,null,null, null,null,null);

INSERT INTO T906_RULES(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID, C906_CREATED_BY,
C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID,C906_LAST_UPDATED_DATE_UTC)
VALUES (S906_RULE.NEXTVAL,'REGULAR','Template for CR/DR','Y',CURRENT_DATE,'DEBIT_CREDIT_TEMP','2173387',NULL,NULL,NULL,NULL, NULL,1026,NULL);
