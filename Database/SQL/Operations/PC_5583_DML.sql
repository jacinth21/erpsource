UPDATE t901_code_lookup
SET
    c902_code_nm_alt = 'Active',
    c901_last_updated_date = current_date,
    c901_last_updated_by = '303043'
WHERE
    c901_code_id = 93310;
    
UPDATE t901_code_lookup
SET
    c902_code_nm_alt = 'InActive',
    c901_last_updated_date = current_date,
    c901_last_updated_by = '303043'
WHERE
    c901_code_id = 93311;
    

--PC-3316 Cycle Count - Scanned Lots Report

INSERT 
INTO t1520_function_list (
    c1520_function_id,
    c1520_function_nm,
    c1520_function_desc,
    c1520_inherited_from_id,
    c901_type,
    c1520_void_fl,
    c1520_created_by,
    c1520_created_date,
    c1520_last_updated_by,
    c1520_last_updated_date,
    c1520_request_uri,
    c1520_request_stropt,
    c1520_seq_no,
    c901_department_id
) VALUES (
    'ACT_INACT_LOC',
    'Activate/Inactivate Locations',
    'Active and Inactive location in warehouse.',
    NULL,
    92262,
    NULL,
    '706322',
    current_date,
    NULL,
    NULL,
    '/gmActivateLocation.do?method=fetchWareHouseDtl',
    NULL,
    NULL,
    NULL);    