 INSERT INTO t906_rules(
        c906_rule_seq_id
        ,c906_rule_id
        ,c906_rule_value
        ,c906_rule_grp_id
        ,c906_created_date
        ,c906_created_by)
VALUES(
        s906_rule.nextval,
        'CHARGE_AMT_DETAIL',
        'gm_pkg_op_loaner_charges_log.gm_fch_charge_amount_history_log',
        'DYNAMIC_AUDIT_TRAIL',
        CURRENT_DATE,
        '303043');