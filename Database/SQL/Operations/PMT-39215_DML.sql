 INSERT
INTO t100_error_msg
  (
    c100_error_msg_id ,
    c100_error_code,
    c100_ora_error_code,
    c100_error_msg,
    c901_language_id,
    c100_error_void_fl,
    c100_created_by,
    c100_created_date,
    c100_last_updated_by,
    c100_last_updated_date,
    c100_last_updated_date_utc
  )
  VALUES
  (
    '778',
    '778',
    '-20999',
    'Picked started for this transaction, cannot void',
    103097,
    NULL,
    NULL,
    sysdate,
    NULL,
    NULL,
    NULL
  );
 
  -- New Rule grp update for Inventory Status  
INSERT
INTO
  T906_RULES
  (
    C906_RULE_SEQ_ID,
    C906_RULE_ID,
    C906_RULE_DESC,
    C906_RULE_VALUE,
    C906_CREATED_DATE,
    C906_RULE_GRP_ID,
    C906_CREATED_BY
  )
  VALUES
  (
    S906_RULE.NEXTVAL,
    '93003',
    'Inventory Pick Status',
    'Assigned',
    CURRENT_DATE,
    'INVPICKSTATUS',
    '303510'
  );
INSERT
INTO
  T906_RULES
  (
    C906_RULE_SEQ_ID,
    C906_RULE_ID,
    C906_RULE_DESC,
    C906_RULE_VALUE,
    C906_CREATED_DATE,
    C906_RULE_GRP_ID,
    C906_CREATED_BY
  )
  VALUES
  (
    S906_RULE.NEXTVAL,
    '93004',
    'Inventory Pick Status',
    'Initiated',
    CURRENT_DATE,
    'INVPICKSTATUS',
    '303510'
  );
INSERT
INTO
  T906_RULES
  (
    C906_RULE_SEQ_ID,
    C906_RULE_ID,
    C906_RULE_DESC,
    C906_RULE_VALUE,
    C906_CREATED_DATE,
    C906_RULE_GRP_ID,
    C906_CREATED_BY
  )
  VALUES
  (
    S906_RULE.NEXTVAL,
    '93005',
    'Inventory Pick Status',
    'WIP',
    CURRENT_DATE,
    'INVPICKSTATUS',
    '303510'
  );
  
  --UPDATE 
  
   UPDATE t100_error_msg
SET c100_error_code          ='778',
  c100_ora_error_code        = '-20999',
  c100_error_msg             = 'Picked started for this transaction, Please contact Inventory team',
  c901_language_id           = 103097,
  c100_error_void_fl         = NULL,
  c100_created_by            = NULL,
  c100_created_date          = NULL,
  c100_last_updated_by       = NULL,
  c100_last_updated_date     = sysdate,
  c100_last_updated_date_utc = NULL
WHERE c100_error_msg_id      = '778';

--DELETE
--DELETE FROM t906_rules WHERE c906_rule_id = 93003 AND c906_rule_grp_id = 'INVPICKSTATUS'; 
 