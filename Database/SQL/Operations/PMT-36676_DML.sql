--PMT-36676   : Rerun Transaction Split
--Insert record to create Left link
INSERT INTO t1520_function_list (C1520_FUNCTION_ID,C1520_FUNCTION_NM,C1520_FUNCTION_DESC,C1520_INHERITED_FROM_ID,C901_TYPE,C1520_VOID_FL,C1520_CREATED_BY,C1520_CREATED_DATE,C1520_LAST_UPDATED_BY,C1520_LAST_UPDATED_DATE,C1520_REQUEST_URI,C1520_REQUEST_STROPT,C1520_SEQ_NO,C901_DEPARTMENT_ID,C901_LEFT_MENU_TYPE,C1520_LAST_UPDATED_DATE_UTC)
VALUES ('RERUN_TXN_SPLIT','Rerun Transaction Split','Rerun the transaction split',null,92262,null,'303013',to_date('22-11-18','DD-MM-RR'),null,null,'\gmRerunTxnSplitAction.do?method=fetchRerunTxnDetails',null,null,2010,null,to_timestamp_tz('05-12-18 08:39:20.672371000 PM +05:30','DD-MM-RR HH12:MI:SS.FF AM TZR'));

UPDATE t1520_function_list SET c1520_function_nm = 'Split Transaction - By Location' WHERE c1520_function_id = 'RERUN_TXN_SPLIT';


--Type of transactions to be splitted

DELETE FROM  t901b_code_group_master 
      WHERE C901B_CODE_GRP = 'SPLIT';   
      
INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'SPLIT','Rerun Transaction Split','','2598779',CURRENT_DATE);

--scripts for Code lookup table
DELETE FROM t901_code_lookup WHERE c901_code_grp = 'SPLIT' AND c901_code_id IN (108500, 108501, 108502, 108503);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108500,'Order','SPLIT','1','1','','','2598779',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108501,'Consignment','SPLIT','1','2','','','2598779',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108502,'Inhouse','SPLIT','1','3','','','2598779',CURRENT_DATE);


--Access to Inventory Group
DELETE FROM t1530_access WHERE c1520_function_id = 'RERUN_TXN_SPLIT' AND c1500_group_id = '100093';
INSERT INTO t1530_access (c1530_access_id, c1500_group_id, c1520_function_id, c101_party_id, c1530_update_access_fl, c1530_read_access_fl,c1530_void_access_fl, c1530_void_fl, c1530_created_by,c1530_created_date) VALUES (s1530_access.nextval, '100093', 'RERUN_TXN_SPLIT',NULL, 'Y', 'Y','Y', NULL, '2193221',SYSDATE);


--Rule values for calling dynamic procedures based on the transaction type

DELETE FROM T906_RULES WHERE C906_RULE_GRP_ID = 'CMN_SPLIT_BY_LOC';

INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'108500','procedure for order type','gm_pkg_op_storage_building.gm_order_process_transaction',CURRENT_DATE,'CMN_SPLIT_BY_LOC','303510',NULL,NULL,NULL,NULL,NULL,NULL);

INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'108501','procedure for consignment type','gm_pkg_op_storage_building_cn.gm_cn_process_transaction',CURRENT_DATE,'CMN_SPLIT_BY_LOC','303510',NULL,NULL,NULL,NULL,NULL,NULL);

INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'108502','procedure for inhouse type','gm_pkg_op_storage_building_ih.gm_inhouset504_process_transaction',CURRENT_DATE,'CMN_SPLIT_BY_LOC','303510',NULL,NULL,NULL,NULL,NULL,NULL);

INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'INHOUSE_OTHERS','procedure for inhouse others type','gm_pkg_op_storage_building_ih.gm_inhouset412_process_transaction',CURRENT_DATE,'CMN_SPLIT_BY_LOC','303510',NULL,NULL,NULL,NULL,NULL,NULL);