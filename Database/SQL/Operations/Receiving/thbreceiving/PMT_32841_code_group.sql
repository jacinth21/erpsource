-- Code Group for THb Analysis List Report Code group values
--THBACT:Code group for THB Analysis list report action eve
INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'THBACT','Code group for THB Analysis list report action eve','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108120,'Schedule','THBACT','0','1','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108121,'Edit Processing Dates','THBACT','0','2','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108122,'Void Analysis','THBACT','0','3','','','2277820',CURRENT_DATE);

--THBVOI: THB analysis void
INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'THBVOI','Code Group for THB Void Analyis action','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108105,'No Longer Valid','THBVOI','0','1','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108106,'Combining Loads','THBVOI','0','2','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108107,'Others','THBVOI','0','3','','','2277820',CURRENT_DATE);

--THBFLR: THB Column Filter
INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'THBFLR','Code Group for THB Column Filter','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108108,'B.O. Qty','THBFLR','0','4','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108109,'Pending < 2 Wks','THBFLR','0','5','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108110,'Pending < 2 Wks','THBFLR','0','6','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108111,'Past 3 Mo. Sale','THBFLR','0','7','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108112,'Shelf Qty','THBFLR','0','8','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108113,'DHR Qty','THBFLR','0','9','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108114,'Repack Qty','THBFLR','0','10','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108123,'PTRD Qty','THBFLR','0','11','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108124,'Need','THBFLR','0','12','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108125,'Rad Run Qty','THBFLR','0','13','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108115,'Alloc. in Rad Run','THBFLR','0','14','','','2277820',current_date);

-- To UPdate active flag in production
UPDATE T901_CODE_LOOKUP
SET C901_ACTIVE_FL       = 1,
  C901_LAST_UPDATED_BY   = '2277820',
  C901_LAST_UPDATED_DATE = sysdate
WHERE c901_code_id in (108120,108121);

-- TO update the code group in prod
UPDATE T901_CODE_LOOKUP
SET c901_code_grp        = 'THBFLR',
  C901_LAST_UPDATED_BY   = '2277820',
  C901_LAST_UPDATED_DATE = sysdate
WHERE c901_code_id      IN (108108,108109,108110,108111,108112,108113,108114,108123,108124,108125,108115);

-- THB Analysis Status
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
values 
(108040,'Initiated','THBANA','0','1','','','2277820',current_date);
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
values 
(108041,'In Progress','THBANA','0','1','','','2277820',current_date);
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
values 
(108042,'Completed','THBANA','0','1','','','2277820',current_date);
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
values 
(108043,'Void','THBANA','0','1','','','2277820',current_date);

-- To UPdate active flag in production
UPDATE T901_CODE_LOOKUP
SET C901_ACTIVE_FL       = 1,
  C901_LAST_UPDATED_BY   = '2277820',
  C901_LAST_UPDATED_DATE = sysdate
WHERE c901_code_id in (108040,108041,108042);