INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'LOCKVO','Lock Void for the THB Analysis Lock Void','','2277820',CURRENT_DATE);

 
  
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(109600,'Void Lock and Generate','LOCKVO','1','1','LOCKVO','','2277820',CURRENT_DATE); 


INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID,C906_LAST_UPDATED_DATE_UTC)
VALUES (S906_RULE.NEXTVAL,'109600','Void Lock and Generate','gm_pkg_op_thb_analysis_rpt.gm_rollback_lock_genereate',current_date,'CMNCNCL','2277820',null,null,null,null,null,null,null);
    