 --Update Re creation to emaild ids
 UPDATE t906_rules 
    SET C906_RULE_VALUE ='#everyone_bba_dataentry@globusmedical.com, gpalani@globusmedical.com' ,
        C906_LAST_UPDATED_DATE=CURRENT_DATE,
        C906_LAST_UPDATED_BY ='2277820'
  WHERE C906_RULE_GRP_ID='RS_CREATION' 
    AND C906_RULE_ID='RS_EMAIL_TO';