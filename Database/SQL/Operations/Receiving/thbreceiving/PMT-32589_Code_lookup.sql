INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES
(s901b_code_group_master.NEXTVAL,'THBFLR','Code Group for THB Void Analyis action','','2277820',CURRENT_DATE);


INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(108108,'B.O. Qty','THBFLR','0','4','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(108109,'Pending < 2 Wks','THBFLR','0','5','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(108110,'Pending < 2 Wks','THBFLR','0','6','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(108111,'Past 3 Mo. Sale','THBFLR','0','7','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(108112,'Shelf Qty','THBFLR','0','8','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(108113,'DHR Qty','THBFLR','0','9','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(108114,'Repack Qty','THBFLR','0','10','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(108123,'PTRD Qty','THBFLR','0','11','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(108124,'Need','THBFLR','0','12','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(108125,'Rad Run Qty','THBFLR','0','13','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(108115,'Alloc. in Rad Run','THBFLR','0','14','','','2277820',CURRENT_DATE);

-- To Update Active Flag value equal to 1
UPDATE t901_code_lookup SET C901_ACTIVE_FL='1',c901_last_updated_date=SYSDATE,c901_last_updated_by='2193221' WHERE c901_code_grp ='THBFLR' AND c901_code_id ='108108';
UPDATE t901_code_lookup SET C901_ACTIVE_FL='1',c901_last_updated_date=SYSDATE,c901_last_updated_by='2193221' WHERE c901_code_grp ='THBFLR' AND c901_code_id ='108109';
UPDATE t901_code_lookup SET C901_ACTIVE_FL='1',c901_last_updated_date=SYSDATE,c901_last_updated_by='2193221' WHERE c901_code_grp ='THBFLR' AND c901_code_id ='108110';
UPDATE t901_code_lookup SET C901_ACTIVE_FL='1',c901_last_updated_date=SYSDATE,c901_last_updated_by='2193221' WHERE c901_code_grp ='THBFLR' AND c901_code_id ='108111';
UPDATE t901_code_lookup SET C901_ACTIVE_FL='1',c901_last_updated_date=SYSDATE,c901_last_updated_by='2193221' WHERE c901_code_grp ='THBFLR' AND c901_code_id ='108112';
UPDATE t901_code_lookup SET C901_ACTIVE_FL='1',c901_last_updated_date=SYSDATE,c901_last_updated_by='2193221' WHERE c901_code_grp ='THBFLR' AND c901_code_id ='108113';
UPDATE t901_code_lookup SET C901_ACTIVE_FL='1',c901_last_updated_date=SYSDATE,c901_last_updated_by='2193221' WHERE c901_code_grp ='THBFLR' AND c901_code_id ='108114';
UPDATE t901_code_lookup SET C901_ACTIVE_FL='1',c901_last_updated_date=SYSDATE,c901_last_updated_by='2193221' WHERE c901_code_grp ='THBFLR' AND c901_code_id ='108123';
UPDATE t901_code_lookup SET C901_ACTIVE_FL='1',c901_last_updated_date=SYSDATE,c901_last_updated_by='2193221' WHERE c901_code_grp ='THBFLR' AND c901_code_id ='108124';
UPDATE t901_code_lookup SET C901_ACTIVE_FL='1',c901_last_updated_date=SYSDATE,c901_last_updated_by='2193221' WHERE c901_code_grp ='THBFLR' AND c901_code_id ='108125';
UPDATE t901_code_lookup SET C901_ACTIVE_FL='1',c901_last_updated_date=SYSDATE,c901_last_updated_by='2193221' WHERE c901_code_grp ='THBFLR' AND c901_code_id ='108115';
