INSERT INTO T1520_FUNCTION_LIST
  (
    C1520_FUNCTION_ID,
    C1520_FUNCTION_NM,
    C1520_FUNCTION_DESC,
    C1520_INHERITED_FROM_ID,
    C901_TYPE,
    C1520_VOID_FL,
    C1520_CREATED_BY,
    C1520_CREATED_DATE,
    C1520_LAST_UPDATED_BY,
    C1520_LAST_UPDATED_DATE,
    C1520_REQUEST_URI,
    C1520_REQUEST_STROPT,
    C1520_SEQ_NO,
    C901_DEPARTMENT_ID,
    C901_LEFT_MENU_TYPE
  )
  VALUES
  (
    'THBDonorSch',
    'THB Donor Stage Scheduling',
    'This screen is used to Schedule Donor Date',
    NULL,
    92262,
    NULL,
    '303510',
    SYSDATE,
    '303510',
    NULL,
    '\gmTHBDonorScheduling.do?method=loadTHBDonorScheduling',
    NULL,
    NULL,
    null,
    NULL
  );
  
  insert into t100_error_msg (c100_error_msg_id ,c100_error_code, c100_ora_error_code, c100_error_msg, c901_language_id, c100_error_void_fl, c100_created_by, c100_created_date, c100_last_updated_by, c100_last_updated_date, c100_last_updated_date_utc )
values (S100_ERROR_MSG.nextval ,'448', '-20999', 'The Selected date is not between the Processed Start and Processed End Date', 103097, null, null, sysdate, null, null, null );