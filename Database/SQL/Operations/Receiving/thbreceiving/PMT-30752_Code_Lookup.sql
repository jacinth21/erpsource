INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'THBLDL','THB Load Group Status','','2277820',CURRENT_DATE);
  
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(107880,'THB Load Initiated','THBLDL','1','1','','','2277820',CURRENT_DATE);
 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(107881,'THB Load Processed','THBLDL','1','2','','','2277820',CURRENT_DATE); 