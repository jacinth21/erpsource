--Inventory Type code group for rad Run Report 
INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'BBAINV','BBA Inventory','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108314,'Finished Goods','BBAINV','1','1','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108315,'Re-Pack Inventory','BBAINV','1','2','','','2277820',CURRENT_DATE);

CREATE OR REPLACE PUBLIC SYNONYM gm_pkg_op_donor_inv_allocation FOR globus_app.gm_pkg_op_donor_inv_allocation;
CREATE OR REPLACE PUBLIC SYNONYM gm_pkg_op_thb_detail_rpt FOR globus_app.gm_pkg_op_thb_detail_rpt;