--Insert left link for fileupload
DELETE FROM t1520_function_list WHERE C1520_FUNCTION_ID = 'LOT_OVERIDE_BULK';
INSERT
   INTO T1520_FUNCTION_LIST
    (
        C1520_FUNCTION_ID, C1520_FUNCTION_NM, C1520_FUNCTION_DESC
      , C1520_INHERITED_FROM_ID, C901_TYPE, C1520_VOID_FL
      , C1520_CREATED_BY, C1520_CREATED_DATE, C1520_LAST_UPDATED_BY
      , C1520_LAST_UPDATED_DATE, C1520_REQUEST_URI, C1520_REQUEST_STROPT
      , C1520_SEQ_NO, C901_DEPARTMENT_ID
    )
    VALUES
    (
        'LOT_OVERIDE_BULK', 'Lot Number Override - Bulk', 'This screen is used to upload multiple Lots'
      , NULL, 92262, NULL
      , '2277820', sysdate, null
      , null, '/gmLotOverideBulk.do?method=loadLotUpload', null
      , NULL, '2024'
    ) ; 