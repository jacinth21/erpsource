--Create left link for Lot Override by Transaction

INSERT INTO t1520_function_list 
(C1520_FUNCTION_ID,
C1520_FUNCTION_NM,
C1520_FUNCTION_DESC,
C901_TYPE,
C1520_CREATED_BY,
C1520_CREATED_DATE,
C1520_REQUEST_URI,
C1520_REQUEST_STROPT,
C901_DEPARTMENT_ID,
C901_LEFT_MENU_TYPE) 
VALUES 
('LOT_OVERRIDE_BY_TXN',
'Lot Override by Transaction',
'Override the LOT number - based on transaction id',
92262,
'706322',
current_date,
'/gmPartControlSetup.do?method=loadLotOverrideByTxn',
'',
2009,101921);