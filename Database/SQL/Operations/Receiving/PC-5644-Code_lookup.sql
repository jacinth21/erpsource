 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(112007,'MS.2008','STOCK','0','8','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(112000,'MS.2001','STOCK','1','1','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(112001,'MS.2002','STOCK','1','2','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(112002,'MS.2003','STOCK','1','3','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(112003,'MS.2004','STOCK','1','4','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(112004,'MS.2005','STOCK','1','5','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(112005,'MS.2006','STOCK','1','6','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(112006,'MS.2007','STOCK','1','7','','','2277820',CURRENT_DATE);

insert into t205l_printer_stock(C205L_PRINTER_STOCK_ID,C205L_STOCK_NAME,C205L_STOCK_DESC,C205L_STOCK_LENGTH,C205L_STOCK_WIDTH,C205L_STOCK_HEIGHT,C901_STOCK_TYPE)
values(S205L_PRINTER_STOCK.nextval,'MS.2001','Box_Label',4,2,2,112000);
insert into t205l_printer_stock(C205L_PRINTER_STOCK_ID,C205L_STOCK_NAME,C205L_STOCK_DESC,C205L_STOCK_LENGTH,C205L_STOCK_WIDTH,C205L_STOCK_HEIGHT,C901_STOCK_TYPE)
values(S205L_PRINTER_STOCK.nextval,'MS.2002','Box_Label_Record',4,2,2,112001);
insert into t205l_printer_stock(C205L_PRINTER_STOCK_ID,C205L_STOCK_NAME,C205L_STOCK_DESC,C205L_STOCK_LENGTH,C205L_STOCK_WIDTH,C205L_STOCK_HEIGHT,C901_STOCK_TYPE)
values(S205L_PRINTER_STOCK.nextval,'MS.2003','Barcode_Label_Set_of_6',1,3,1,112002);
insert into t205l_printer_stock(C205L_PRINTER_STOCK_ID,C205L_STOCK_NAME,C205L_STOCK_DESC,C205L_STOCK_LENGTH,C205L_STOCK_WIDTH,C205L_STOCK_HEIGHT,C901_STOCK_TYPE)
values(S205L_PRINTER_STOCK.nextval,'MS.2004','Barcode_Label_Set_of_2',1,3,1,112003);
insert into t205l_printer_stock(C205L_PRINTER_STOCK_ID,C205L_STOCK_NAME,C205L_STOCK_DESC,C205L_STOCK_LENGTH,C205L_STOCK_WIDTH,C205L_STOCK_HEIGHT,C901_STOCK_TYPE)
values(S205L_PRINTER_STOCK.nextval,'MS.2005','Barcode_Label_Set_of_1',1,3,1,112004);
insert into t205l_printer_stock(C205L_PRINTER_STOCK_ID,C205L_STOCK_NAME,C205L_STOCK_DESC,C205L_STOCK_LENGTH,C205L_STOCK_WIDTH,C205L_STOCK_HEIGHT,C901_STOCK_TYPE)
values(S205L_PRINTER_STOCK.nextval,'MS.2006','Barcode_Label_Record',1,3,1,112005);
insert into t205l_printer_stock(C205L_PRINTER_STOCK_ID,C205L_STOCK_NAME,C205L_STOCK_DESC,C205L_STOCK_LENGTH,C205L_STOCK_WIDTH,C205L_STOCK_HEIGHT,C901_STOCK_TYPE)
values(S205L_PRINTER_STOCK.nextval,'MS.2007','Final_Package_Label',2,3.8125,2,112006);