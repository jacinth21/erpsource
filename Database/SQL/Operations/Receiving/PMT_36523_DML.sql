--Staging Dashboard to display both RS and PTRD Transaction type
INSERT
   INTO T1520_FUNCTION_LIST
    (
        C1520_FUNCTION_ID, C1520_FUNCTION_NM, C1520_FUNCTION_DESC
      , C1520_INHERITED_FROM_ID, C901_TYPE, C1520_VOID_FL
      , C1520_CREATED_BY, C1520_CREATED_DATE, C1520_LAST_UPDATED_BY
      , C1520_LAST_UPDATED_DATE, C1520_REQUEST_URI, C1520_REQUEST_STROPT
      , C1520_SEQ_NO, C901_DEPARTMENT_ID, C901_LEFT_MENU_TYPE
    )
    VALUES
    (
        'STAGING DASHBOARD', 'Stage DashBoard', 'Staging Dashboard to display RS and PTRD Transactions'
      , NULL, 92262, NULL
      , '2277820', sysdate, null
      , null, '/gmBBAStageDash.do?method=fetchStagingDashboard', '26230710'
      , NULL, '2024','101920'
    ) ;



