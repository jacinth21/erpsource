INSERT INTO t906_rules (
        c906_rule_seq_id,
        c906_rule_id,
        c906_rule_desc,
        c906_rule_value,
        c906_created_date,
        c906_rule_grp_id,
        c906_created_by,
        c906_last_updated_date,
        c906_last_updated_by,
        c901_rule_type,
        c906_active_fl,
        c906_void_fl
    ) VALUES (
        s906_rule.NEXTVAL,
        'FGRT',
        'FG Return Process',
        '400086',
        current_date,
        'SKIP_EXP_DATE_VALID',
        '303150',
        NULL,
        NULL,
        NULL,
        NULL,
        NULL
    );
	
 INSERT INTO t906_rules (
        c906_rule_seq_id,
        c906_rule_id,
        c906_rule_desc,
        c906_rule_value,
        c906_created_date,
        c906_rule_grp_id,
        c906_created_by,
        c906_last_updated_date,
        c906_last_updated_by,
        c901_rule_type,
        c906_active_fl,
        c906_void_fl
    ) VALUES (
        s906_rule.NEXTVAL,
        'QNRT',
        'QN Return Process',
        '400085',
        current_date,
        'SKIP_EXP_DATE_VALID',
        '303150',
        NULL,
        NULL,
        NULL,
        NULL,
        NULL
    );