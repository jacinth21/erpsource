
-- Left link for THB Dashboard
DELETE FROM t1530_access WHERE C1520_FUNCTION_ID = 'THB_LOAD';
DELETE FROM T1520_FUNCTION_LIST WHERE C1520_FUNCTION_ID = 'THB_LOAD';

INSERT INTO T1520_FUNCTION_LIST(
    C1520_FUNCTION_ID, C1520_FUNCTION_NM, C1520_FUNCTION_DESC, C1520_INHERITED_FROM_ID, C901_TYPE, C1520_VOID_FL
    , C1520_CREATED_BY, C1520_CREATED_DATE, C1520_REQUEST_URI
    , C1520_REQUEST_STROPT, C1520_SEQ_NO, C901_DEPARTMENT_ID, C901_LEFT_MENU_TYPE, C1520_LAST_UPDATED_DATE_UTC
)
VALUES(
    'THB_LOAD', 'THB Receiving Dashboard', 'This screen displays THB ReceivING Dashboard', NULL, 92262, NULL
    , '2193221', CURRENT_DATE, '\gmTHBReceivingDashBoard.do?method=fetchTHBReceivingDashBoard'
    , NULL, NULL, NULL, NULL, NULL
);

INSERT INTO t1530_access
            (c1530_access_id, c1500_group_id, c1520_function_id,
             c101_party_id, c1530_update_access_fl, c1530_read_access_fl,
             C1530_VOID_ACCESS_FL, C1530_VOID_FL, C1530_CREATED_BY,
             c1530_created_date, C1520_PARENT_FUNCTION_ID, C1530_SEQ_NO
            )
     VALUES (s1530_access.nextval, '100107', 'THB_LOAD',
             NULL, 'Y', 'Y',
             'Y', NULL, '2193221',
             SYSDATE, 12273900, 204621706
            );
            
INSERT INTO t1530_access
            (c1530_access_id, c1500_group_id, c1520_function_id,
             c101_party_id, c1530_update_access_fl, c1530_read_access_fl,
             C1530_VOID_ACCESS_FL, C1530_VOID_FL, C1530_CREATED_BY,
             c1530_created_date
            )
     VALUES (s1530_access.nextval, '100106', 'THB_LOAD',
             NULL, 'Y', 'Y',
             'Y', NULL, '2193221',
             SYSDATE
            );
            
--Left link for THB Transaction
DELETE FROM t1530_access WHERE C1520_FUNCTION_ID = 'THB_Receive_Load';
DELETE FROM T1520_FUNCTION_LIST WHERE C1520_FUNCTION_ID = 'THB_Receive_Load';

INSERT
   INTO T1520_FUNCTION_LIST
    (
        C1520_FUNCTION_ID, C1520_FUNCTION_NM, C1520_FUNCTION_DESC
      , C1520_INHERITED_FROM_ID, C901_TYPE, C1520_VOID_FL
      , C1520_CREATED_BY, C1520_CREATED_DATE, C1520_LAST_UPDATED_BY
      , C1520_LAST_UPDATED_DATE, C1520_REQUEST_URI, C1520_REQUEST_STROPT
      , C1520_SEQ_NO, C901_DEPARTMENT_ID
    )
    VALUES
    (
        'THB_Receive_Load', 'THB Receiving Transaction', 'This screen loads THBLoad Receiving Load Transaction'
      , NULL, 92262, NULL
      , '2277820', sysdate, null
      , null, '\gmTHBReceivingLoadTran.do?method=fetchTHBHeadLoadDetails', null
      , NULL, '2024'
    ) ;

INSERT INTO t1530_access
            (c1530_access_id, c1500_group_id, c1520_function_id,
             c101_party_id, c1530_update_access_fl, c1530_read_access_fl,
             C1530_VOID_ACCESS_FL, C1530_VOID_FL, C1530_CREATED_BY,
             c1530_created_date, C1520_PARENT_FUNCTION_ID, C1530_SEQ_NO
            )
     VALUES (s1530_access.nextval, '100107', 'THB_Receive_Load',
             NULL, 'Y', 'Y',
             'Y', NULL, '2193221',
             SYSDATE, 12273900, 204621707
             );
            
INSERT INTO t1530_access
            (c1530_access_id, c1500_group_id, c1520_function_id,
             c101_party_id, c1530_update_access_fl, c1530_read_access_fl,
             C1530_VOID_ACCESS_FL, C1530_VOID_FL, C1530_CREATED_BY,
             c1530_created_date
            )
     VALUES (s1530_access.nextval, '100106', 'THB_Receive_Load',
             NULL, 'Y', 'Y',
             'Y', NULL, '2193221',
             SYSDATE
            );
            