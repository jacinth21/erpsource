--- PMT-34026   : Users building mapping
--  Author      : N RAJA

INSERT INTO T1520_FUNCTION_LIST
  (
    C1520_FUNCTION_ID,
    C1520_FUNCTION_NM,
    C1520_FUNCTION_DESC,
    C1520_INHERITED_FROM_ID,
    C901_TYPE,
    C1520_VOID_FL,
    C1520_CREATED_BY,
    C1520_CREATED_DATE,
    C1520_LAST_UPDATED_BY,
    C1520_LAST_UPDATED_DATE,
    C1520_REQUEST_URI,
    C1520_REQUEST_STROPT,
    C1520_SEQ_NO,
    C901_DEPARTMENT_ID,
    C901_LEFT_MENU_TYPE
  )
  VALUES
  (
    'BUILDING_USER_MAP',
    'Building User Mapping',
    'This is used to Mapping Building to the users',
    NULL,
    92262,
    NULL,
    '303510',
    SYSDATE,
    '303510',
    NULL,
    '\gmUserBuildingMappingAction.do?&strOpt=SECUSR',
    'SECUSR',
    NULL,
    '2024',
    '101920'
  );

DELETE FROM  t901b_code_group_master 
      WHERE C901B_CODE_GRP = 'BLDGRP';   
       
INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'BLDGRP','Opr. User Building Mapping','','303149',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(198716,'Inventory User Group','BLDGRP','1','1','100000113','','303149',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(199164,'Shipping User Group','BLDGRP','1','2','100025','','303149',CURRENT_DATE);

ALTER TABLE t1501_group_mapping ADD c5057_building_id NUMBER;

CREATE OR REPLACE PUBLIC SYNONYM gm_pkg_op_user_bldg_mapping FOR globus_app.gm_pkg_op_user_bldg_mapping;
