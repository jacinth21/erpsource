--Update to pick PNFG transactions on lot track job
update t922_inventory_transaction_map set C922_VALUE = 't504', C922_LAST_UPDATED_BY = '2765071', C922_LAST_UPDATED_DATE=current_date where C901_TYPE = '120488';
update t922_inventory_transaction_map set c922_minus_warehouse = null, c922_plus_warehouse = null , C922_LAST_UPDATED_BY = '2765071', C922_LAST_UPDATED_DATE = current_date where c901_type = '4127';

--Disable constraint for location id
ALTER TABLE T5081_LOT_TRACK_INV_LOG DISABLE CONSTRAINT SYS_C00116233;
DELETE FROM t922_inventory_transaction_map WHERE c901_type IN (100063,50158);
INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY,c922_created_by,c922_created_date)
     VALUES(s922_inventory_transaction_map.NEXTVAL,'100063','Bulk to Loaner','t412','','90814','','','Loaner','2765071',current_date);
INSERT INTO t922_inventory_transaction_map (C922_LOT_TRANSACTION_MAPPING_ID,C901_TYPE,C922_LOT_TRANSACTION_NAME,C922_VALUE,C922_PLUS_WAREHOUSE,C922_MINUS_WAREHOUSE,
     C922_ADDL_PLUS_WAREHOUSE,C922_ADDL_MINUS_WAREHOUSE,C922_CATEGORY,c922_created_by,c922_created_date)
     VALUES(s922_inventory_transaction_map.NEXTVAL,'50158','Loaner to Quarantine','t412','90813','','','','Loaner','2765071',current_date);
