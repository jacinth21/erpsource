-- create synonym for new package
create or replace public synonym gm_pkg_oppr_ld_ttp_vendor_po_dashboard_rpt for GLOBUS_APP.gm_pkg_oppr_ld_ttp_vendor_po_dashboard_rpt;

--Add new code id
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
values
(26240822,'PO Generated','TTPVST','1','','','','2598779',CURRENT_DATE);


INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
values
(26240823,'PO Generation Failed','TTPVST','1','','','','2598779',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
values
(26240821,'PO Generation In-Progress','TTPVST','1','','','','2598779',CURRENT_DATE);

--Insert record to create left link
INSERT INTO T1520_FUNCTION_LIST
  (
    C1520_FUNCTION_ID,
    C1520_FUNCTION_NM,
    C1520_FUNCTION_DESC,
    C1520_INHERITED_FROM_ID,
    C901_TYPE,
    C1520_VOID_FL,
    C1520_CREATED_BY,
    C1520_CREATED_DATE,
    C1520_LAST_UPDATED_BY,
    C1520_LAST_UPDATED_DATE,
    C1520_REQUEST_URI,
    C1520_REQUEST_STROPT,
    C1520_SEQ_NO,
    C901_DEPARTMENT_ID,
    C901_LEFT_MENU_TYPE
  )
  VALUES
  (
    'VENDOR_PO_DASHBOARD',
    'Create PO By Vendor',
    'This is used to save and generate PO by vendor dashboard',
    NULL,
    92262,
    NULL,
    '303510',
    SYSDATE,
    '303510',
    NULL,
    '\gmTTPVendorPODashboard.do?method=loadVendorPODashboard',
    NULL,
    NULL,
    '2003',
    '101920'
  );
 --Button access for Vendor PO Dashboard #VEN_PO_SAV_AC - Save
   INSERT
   INTO t1520_function_list
    (
        C1520_FUNCTION_ID, C1520_FUNCTION_NM, C1520_FUNCTION_DESC,
        C1520_INHERITED_FROM_ID, C901_TYPE, C1520_VOID_FL,
        C1520_CREATED_BY, C1520_CREATED_DATE, C1520_LAST_UPDATED_BY,
        C1520_LAST_UPDATED_DATE, C1520_REQUEST_URI, C1520_REQUEST_STROPT,
        C1520_SEQ_NO, C901_DEPARTMENT_ID
    )    VALUES
    (
        'TTP_VEND_PO_SAVE_ACC', 'Vendor PO DashBoard Button Access', 
        'Enable/disable the save button',
        NULL, 92267, NULL,
        '303510', SYSDATE, NULL,
        NULL, NULL, NULL,
        null, null
    ) ;
    
   -- Button access for Vendor PO Dashboard #VEN_PO_GEN_AC - Generate
   INSERT
   INTO t1520_function_list
    (
        C1520_FUNCTION_ID, C1520_FUNCTION_NM, C1520_FUNCTION_DESC,
        C1520_INHERITED_FROM_ID, C901_TYPE, C1520_VOID_FL,
        C1520_CREATED_BY, C1520_CREATED_DATE, C1520_LAST_UPDATED_BY,
        C1520_LAST_UPDATED_DATE, C1520_REQUEST_URI, C1520_REQUEST_STROPT,
        C1520_SEQ_NO, C901_DEPARTMENT_ID
    )    VALUES
    (
        'TTP_VEND_PO_GEN_ACC', 'Vendor PO DashBoard Button Access', 
        'Enable/disable the Generate PO button',
        NULL, 92267, NULL,
        '303510', SYSDATE, NULL,
        NULL, NULL, NULL,
        null, null
    ) ;
    
   
