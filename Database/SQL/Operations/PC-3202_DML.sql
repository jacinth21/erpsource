--synonym for new package and table
create or replace public synonym GM_PKG_WO_LEAD_TIME_TXN for GLOBUS_APP.GM_PKG_WO_LEAD_TIME_TXN;
create or replace public synonym T3011_VENDOR_LEAD_TIME for GLOBUS_APP.T3011_VENDOR_LEAD_TIME;