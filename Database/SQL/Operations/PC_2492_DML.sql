delete from t906_rules where C906_Rule_Grp_Id = 'REQBRWEMAIL';

INSERT INTO t906_rules (
c906_rule_seq_id,
c906_rule_id,
c906_rule_desc,
c906_rule_value,
c906_created_date,
c906_rule_grp_id,
c906_created_by,
c906_last_updated_date,
c906_last_updated_by,
c901_rule_type,
c906_active_fl,
c906_void_fl,
c1900_company_id,
c906_last_updated_date_utc
) VALUES (
s906_rule.NEXTVAL,
'EMAIL_BORROW_BODY',
'Email Body for Set Request Borrow Emails',
'Request to borrow these consignment sets from you for a case on ',
current_date,
'REQBRWEMAIL',
'3178209',
NULL,
NULL,
NULL,
NULL,
Null,
1000,
Null
);

INSERT INTO t906_rules (
c906_rule_seq_id,
c906_rule_id,
c906_rule_desc,
c906_rule_value,
c906_created_date,
c906_rule_grp_id,
c906_created_by,
c906_last_updated_date,
c906_last_updated_by,
c901_rule_type,
c906_active_fl,
c906_void_fl,
c1900_company_id,
c906_last_updated_date_utc
) VALUES (
S906_Rule.Nextval,
'NO_OF_EMAIL_REQ',
'can send this no of emails from the borrow report',
'100',
current_date,
'REQBRWEMAIL',
'3178209',
NULL,
NULL,
NULL,
NULL,
Null,
1000,
Null
);