INSERT
INTO t901_code_lookup
  (
    c901_code_id,
    c901_code_nm,
    c901_code_grp,
    c901_active_fl,
    c901_code_seq_no,
    c902_code_nm_alt,
    c901_control_type,
    c901_created_by,
    c901_created_date
  )
  VALUES
  (
    4001001,
    'Rollback Sets',
    'CNCLT',
    '1',
    '',
    'RLBPIC',
    '',
    '2192360',
    CURRENT_DATE
  );
  
INSERT
INTO t901_code_lookup
  (
    c901_code_id,
    c901_code_nm,
    c901_code_grp,
    c901_active_fl,
    c901_code_seq_no,
    c902_code_nm_alt ,
    c901_control_type,
    c901_created_by,
    c901_created_date
  )
  VALUES
  (
    '400040',
    'Reason 1',
    'RLBPIC',
    1,
    1,
    NULL ,
    NULL,
    '303431',
    sysdate
  );
  INSERT
INTO t901_code_lookup
  (
    c901_code_id,
    c901_code_nm,
    c901_code_grp,
    c901_active_fl,
    c901_code_seq_no,
    c902_code_nm_alt ,
    c901_control_type,
    c901_created_by,
    c901_created_date
  )
  VALUES
  (
    '400041',
    'Reason 2',
    'RLBPIC',
    1,
    1,
    NULL ,
    NULL,
    '303431',
    sysdate
  );
  INSERT
INTO t901_code_lookup
  (
    c901_code_id,
    c901_code_nm,
    c901_code_grp,
    c901_active_fl,
    c901_code_seq_no,
    c902_code_nm_alt ,
    c901_control_type,
    c901_created_by,
    c901_created_date
  )
  VALUES
  (
    '400042',
    'Reason 3',
    'RLBPIC',
    1,
    1,
    NULL ,
    NULL,
    '303431',
    sysdate
  );
   INSERT
INTO t901_code_lookup
  (
    c901_code_id,
    c901_code_nm,
    c901_code_grp,
    c901_active_fl,
    c901_code_seq_no,
    c902_code_nm_alt ,
    c901_control_type,
    c901_created_by,
    c901_created_date
  )
  VALUES
  (
    '400043',
    'Others',
    'RLBPIC',
    1,
    1,
    NULL ,
    NULL,
    '303431',
    sysdate
  );
  
  
INSERT INTO t1520_function_list ( c1520_function_id, c1520_function_nm, c1520_function_desc, c1520_inherited_from_id, c901_type, c1520_void_fl,
                c1520_created_by, c1520_created_date, c1520_last_updated_by, c1520_last_updated_date, c1520_request_uri, c1520_request_stropt,
                c1520_seq_no,c901_department_id) 
       VALUES ('SET_BUILD_ROLLBACK','Set build rollback', 'Set build rollback',NULL, 92267, NULL,'2173387', current_date,
               NULL, NULL, NULL, NULL, NULL, NULL);