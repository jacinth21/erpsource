
DECLARE

CURSOR cur_incident IS
		SELECT t9200.c9200_incident_id incident_id,
		  t9200.c9200_incident_date incident_dt
		FROM t9200_incident t9200,
		  t9201_charge_details t9201
		WHERE t9200.c9200_incident_id      = t9201.c9200_incident_id
		AND t9201.c9201_charge_end_date   IS NULL
		AND t9201.c9201_charge_start_date IS NULL
		AND t9200.c9200_ref_type           = '92068'; --loaner late fee
            
BEGIN
		FOR  inc_cur IN  cur_incident
	    LOOP
	      BEGIN
          UPDATE t9201_charge_details
          SET c9201_charge_start_date = Last_Day(ADD_MONTHS(inc_cur.incident_dt,-1))+1 ,
            c9201_charge_end_date      =inc_cur.incident_dt
          WHERE c9200_incident_id      =inc_cur.incident_id
          AND c9201_charge_end_date   IS NULL
          AND c9201_charge_start_date IS NULL;
        EXCEPTION
          WHEN OTHERS THEN
	          DBMS_OUTPUT.PUT_LINE('No need to update charge start date and end date for ----------------------------- ' || inc_cur.incident_dt); 
        END;

    END LOOP;

END;