CREATE OR REPLACE PUBLIC SYNONYM gm_pkg_op_loaner_charges_rpt FOR GLOBUS_APP.gm_pkg_op_loaner_charges_rpt; 
CREATE OR REPLACE PUBLIC SYNONYM gm_pkg_op_loaner_charges_txn FOR GLOBUS_APP.gm_pkg_op_loaner_charges_txn; 
CREATE OR REPLACE PUBLIC SYNONYM gm_pkg_op_loaner_charges_log FOR GLOBUS_APP.gm_pkg_op_loaner_charges_log; 
CREATE OR REPLACE PUBLIC SYNONYM v_loaner_late_fee FOR GLOBUS_APP.v_loaner_late_fee;

 INSERT INTO t1520_function_list
                     (c1520_function_id, c1520_function_nm,c901_type)
                      VALUES ('EDIT_LATE_FEE_AMOUNT','EDIT_LATE_FEE_AMOUNT',92267);
                       
 INSERT INTO t1520_function_list
                     (c1520_function_id, c1520_function_nm,c901_type)
                      VALUES ('EDIT_LATE_FEE_STATUS','EDIT_LATE_FEE_STATUS',92267);                                      
 INSERT INTO t1520_function_list
                     (c1520_function_id, c1520_function_nm,c901_type)
                      VALUES ('HOLD_LATE_FEE_STATUS','HOLD_LATE_FEE_STATUS',92267);  
                      
                      
 INSERT INTO t1520_function_list
                     (c1520_function_id, c1520_function_nm,c901_type)
                      VALUES ('NOTE_LATE_FEE','NOTE_LATE_FEE',92267);
                      
 INSERT INTO t1520_function_list
                     (c1520_function_id, c1520_function_nm,c901_type)
                      VALUES ('CREDIT_LATE_FEE','CREDIT_LATE_FEE',92267);

                      
UPDATE T901_CODE_LOOKUP SET C902_CODE_NM_ALT = '10',C901_LAST_UPDATED_BY='303012',C901_LAST_UPDATED_DATE=SYSDATE WHERE C901_CODE_ID = '110501';
UPDATE T901_CODE_LOOKUP SET C902_CODE_NM_ALT = '15',C901_LAST_UPDATED_BY='303012',C901_LAST_UPDATED_DATE=SYSDATE WHERE C901_CODE_ID = '110502';
UPDATE T901_CODE_LOOKUP SET C902_CODE_NM_ALT = '20',C901_LAST_UPDATED_BY='303012',C901_LAST_UPDATED_DATE=SYSDATE WHERE C901_CODE_ID = '110503';
UPDATE T901_CODE_LOOKUP SET C902_CODE_NM_ALT = '25',C901_LAST_UPDATED_BY='303012',C901_LAST_UPDATED_DATE=SYSDATE WHERE C901_CODE_ID = '110504';
UPDATE T901_CODE_LOOKUP SET C902_CODE_NM_ALT = '40',C901_LAST_UPDATED_BY='303012',C901_LAST_UPDATED_DATE=SYSDATE WHERE C901_CODE_ID = '110505';

 
INSERT INTO t1520_function_list (
    c1520_function_id,
    c1520_function_nm,
    c1520_function_desc,
    c1520_inherited_from_id,
    c901_type,
    c1520_void_fl,
    c1520_created_by,
    c1520_created_date,
    c1520_last_updated_by,
    c1520_last_updated_date,
    c1520_request_uri,
    c1520_request_stropt,
    c1520_seq_no,
    c901_department_id,
    c901_left_menu_type
) VALUES (  
    'LoanerLateFeeCharge',
    'Loaner Late Fee Charges',
    'This is used to display the late fee charges of loaners',
    NULL,
    92262,
    NULL,
    '303013',
    sysdate,
    NULL,
    NULL,
    '\gmLoanerLateFeeAction.do?method=loadLateFeeDashboard',
    '26230710',
    NULL,
    '2000',
    '101921'
);

INSERT INTO t906_rules(
        c906_rule_seq_id
        ,c906_rule_id
        ,c906_rule_value
        ,c906_rule_grp_id
        ,c906_created_date
        ,c906_created_by)
VALUES(
        s906_rule.nextval,
        'CHARGE_DETAIL',
        'gm_pkg_op_loaner_charges_log.gm_fch_charge_history_log',
        'DYNAMIC_AUDIT_TRAIL',
        CURRENT_DATE,
        '303043');
        
update t901_code_lookup set c901_active_fl='0',c901_last_updated_by='303043',c901_last_updated_date=sysdate where c901_code_id='92068';

update t1520_function_list set C1520_REQUEST_URI='\gmChargesApproval.do?FORMNAME=frmChargesApproval',c1520_last_updated_by='303043',c1520_last_updated_date=sysdate where c1520_function_id='2850000';