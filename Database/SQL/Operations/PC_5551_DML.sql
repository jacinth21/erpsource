INSERT INTO t1520_function_list (
    c1520_function_id,
    c1520_function_nm,
    c1520_function_desc,
    c1520_inherited_from_id,
    c901_type,
    c1520_void_fl,
    c1520_created_by,
    c1520_created_date,
    c1520_last_updated_by,
    c1520_last_updated_date,
    c1520_request_uri,
    c1520_request_stropt,
    c1520_seq_no,
    c901_department_id
) VALUES (
    'BULK_LOC_TRANS',
    'Bulk Locations Transfer',
    'Bulk location Transfer in warehouse.',
    NULL,
    92262,
    NULL,
    '706322',
    current_date,
    NULL,
    NULL,
    '/gmLocationTransfer.do?method=fetchBuildingInfo',
    NULL,
    NULL,
    NULL
);

 UPDATE t1520_function_list
SET
    c1520_function_nm = 'Bulk Location Transfer'
WHERE
    c1520_function_id = 'BULK_LOC_TRANS';