---Insert a rule value for the Rule_grp_id �EMAIL�---

insert into t906_rules (c906_rule_seq_id, c906_rule_id, c906_rule_desc, c906_rule_value, c906_created_date, c906_rule_grp_id, c906_created_by,c1900_company_id)
values (s906_rule.nextval, 'INVOICE_EMAIL_OUS_CC', 'email CC for OUS invoice', 'ous-invoices@globusmedical.com', sysdate, 'EMAIL', 303510,1000);

-- update a previous rule value to invoices@globusmedical.com --
update t906_rules set c906_rule_value = 'invoices@globusmedical.com',c906_last_updated_date=sysdate where c906_rule_id = 'INVOICE_EMAIL_CC' and c1900_company_id='1000';