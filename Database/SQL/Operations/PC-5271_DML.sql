UPDATE T501C_ORDER_TAG_DO_CLASSIFICATION
SET c501c_created_date  = C501D_TAG_CREATED_DATE
WHERE C5010_TAG_ID     IS NOT NULL
AND c501c_created_date IS NULL;

DELETE
FROM t501c_order_tag_do_classification t501c
WHERE t501c.rowid IN
  (SELECT t501c.rowid
  FROM t501c_order_tag_do_classification t501c,
    ( SELECT DISTINCT c501_order_id FROM t501c_order_tag_do_classification_temp
    ) t501c_tmp
  WHERE t501c.c501_order_id = t501c_tmp.c501_order_id
  AND t501c.rowid NOT      IN
    (SELECT MAX(t501c.rowid)
    FROM t501c_order_tag_do_classification t501c,
      ( SELECT DISTINCT c501_order_id FROM t501c_order_tag_do_classification_temp
      ) t501c_tmp
    WHERE t501c.c501_order_id = t501c_tmp.c501_order_id
    GROUP BY t501c.c501_order_id,
      t501c.c207_set_id,
      t501c.c207_system_id,
      t501c.c5010_tag_id
    )
  );