
-- to create the dummy distributor id

DECLARE
    v_dist_id NUMBER;
BEGIN
    -- dist seq
     SELECT s701_distributor.NEXTVAL INTO v_dist_id FROM DUAL;
    -- to create the dummy dist.
     INSERT
       INTO T701_DISTRIBUTOR
        (
            C701_DISTRIBUTOR_ID, C701_DISTRIBUTOR_NAME, C701_REGION
          , C701_BILL_ADD1, C701_BILL_NAME, C701_BILL_ADD2
          , C701_BILL_STATE, C701_BILL_COUNTRY, C701_BILL_CITY
          , C701_BILL_ZIP_CODE, C701_CONTACT_PERSON, C701_CONTRACT_START_DATE
          , C701_ACTIVE_FL, C701_CREATED_DATE, C701_CREATED_BY
          , C701_VOID_FL, C901_DISTRIBUTOR_TYPE, C1900_COMPANY_ID
          , C701_DISTRIBUTOR_NAME_EN
        )
        VALUES
        (
            v_dist_id, 'Dummy distributor (Agent type)', 100860
          , 'TEB', 'TEB', NULL
          , 1011, 1101, 'Atlanta'
          , '30307', 'TBE', SYSDATE
          , 'N', SYSDATE, 706322
          , NULL, 70100, 1022
          , 'Dummy distributor (Agent type)'
        ) ;
    -- insert the rules
     INSERT
       INTO T906_RULES
        (
            C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_VALUE, C906_RULE_DESC
          , C906_RULE_GRP_ID, C906_CREATED_DATE, C906_CREATED_BY
        )
        VALUES
        (
            s906_rule.NEXTVAL, 'AGENT_DIST_ID', v_dist_id, 'To get the default distributor id (dummy dist id)'
          , 'AGENT_DIST', CURRENT_DATE, '706322'
        ) ;
END;
/