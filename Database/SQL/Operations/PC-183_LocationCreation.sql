INSERT INTO t901_code_lookup(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES(26241098,'Manf Lot','LOCTP','1','','','','3178209',CURRENT_DATE);

INSERT INTO t901_code_lookup(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES(26241099,'Manf Warehouse','LOCWT','1','1','26241099','','3178209',CURRENT_DATE);

INSERT INTO t5051_inv_warehouse (c5051_inv_warehouse_id,c5051_warehouse_sh_nm,c5051_inv_warehouse_nm,c901_warehouse_type,c901_status_id,c5051_created_by,c5051_created_date)
VALUES (8,'MF','Manf Warehouse','26241099','1','3178209',sysdate);

--Location Creation
BEGIN
  GM_PKG_OP_INV_LOCATION_MODULE.GM_SAV_INV_LOCATION('Initiator Wrkspc^^$','110060','A','3178209','001','93310','26241098','8','5002');
  GM_PKG_OP_INV_LOCATION_MODULE.GM_SAV_INV_LOCATION('QC Sply Insp Wrkspc^^$','110060','A','3178209','001','93310','26241098','8','5002');
  GM_PKG_OP_INV_LOCATION_MODULE.GM_SAV_INV_LOCATION('Clean Rooms^^$','110060','A','3178209','001','93310','26241098','8','5002');
  GM_PKG_OP_INV_LOCATION_MODULE.GM_SAV_INV_LOCATION('Burst/Peel Testing^^$','110060','A','3178209','001','93310','26241098','8','5002');
  GM_PKG_OP_INV_LOCATION_MODULE.GM_SAV_INV_LOCATION('Boxing area^^$','110060','A','3178209','001','93310','26241098','8','5002');
  GM_PKG_OP_INV_LOCATION_MODULE.GM_SAV_INV_LOCATION('Out to sterztn^^$','110060','A','3178209','001','93310','26241098','8','5002');
  GM_PKG_OP_INV_LOCATION_MODULE.GM_SAV_INV_LOCATION('Back from sterztn^^$','110060','A','3178209','001','93310','26241098','8','5002');
  GM_PKG_OP_INV_LOCATION_MODULE.GM_SAV_INV_LOCATION('QC Final Insp^^$','110060','A','3178209','001','93310','26241098','8','5002');
  GM_PKG_OP_INV_LOCATION_MODULE.GM_SAV_INV_LOCATION('QA Review^^$','110060','A','3178209','001','93310','26241098','8','5002');
END;
/