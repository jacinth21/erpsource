CREATE OR REPLACE  public synonym T4025_CUT_PLAN_LOCK FOR GLOBUS_APP.T4025_CUT_PLAN_LOCK;
CREATE OR REPLACE  public synonym T4026_CUT_PLAN_LOCK_DETAIL FOR GLOBUS_APP.T4026_CUT_PLAN_LOCK_DETAIL;
CREATE OR REPLACE  public synonym T2562_THB_WIP_INVENTORY_STATUS FOR GLOBUS_APP.T2562_THB_WIP_INVENTORY_STATUS;
CREATE OR REPLACE  public synonym T2563_THB_WEEKLY_CAPACITY FOR GLOBUS_APP.T2563_THB_WEEKLY_CAPACITY;
CREATE OR REPLACE  public synonym gm_pkg_op_cut_plan_forecast FOR GLOBUS_APP.gm_pkg_op_cut_plan_forecast;

/*
-- Code Group
INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'THBWIP','THBWIP Part Inventory','','2277820',CURRENT_DATE);

 
 
  
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(109700,'Pending Lyo','THBWIP','1','1','','','2277820',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(109701,'Pending Rad Run','THBWIP','1','2','','','2277820',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(109702,'QA Hold','THBWIP','1','3','','','2277820',CURRENT_DATE);
 

 
  
 
INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'CUTPLA','BBA Cut Plan Group','','2277820',CURRENT_DATE);

 
 
  
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(109580,'Cut Plan Started','CUTPLA','1','1','','','2277820',CURRENT_DATE);


INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(109581,'Cut Plan Ended','CUTPLA','1','2','','','2277820',CURRENT_DATE); 


INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'CUTCAT','Cut Plan Category','','2277820',CURRENT_DATE);


 
  
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(109800,'DBM','CUTCAT','1','1','','','2277820',CURRENT_DATE);


INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(109801,'Amnion','CUTCAT','1','2','','','2277820',CURRENT_DATE);


INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(109802,'Base','CUTCAT','1','3','','','2277820',CURRENT_DATE);

*/


