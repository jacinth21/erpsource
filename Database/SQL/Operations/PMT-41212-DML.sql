Delete from T1520_FUNCTION_LIST where C1520_REQUEST_URI = '\gmLoanerExtReqRpt.do?method=loadLoanerExtnReqRpt';

insert into T1520_FUNCTION_LIST 
(C1520_FUNCTION_ID,C1520_FUNCTION_NM,C1520_FUNCTION_DESC,C1520_INHERITED_FROM_ID,C901_TYPE,C1520_VOID_FL,C1520_CREATED_BY,C1520_CREATED_DATE,C1520_LAST_UPDATED_BY,C1520_LAST_UPDATED_DATE,C1520_REQUEST_URI,C1520_REQUEST_STROPT,C1520_SEQ_NO,C901_DEPARTMENT_ID,C901_LEFT_MENU_TYPE,C1520_LAST_UPDATED_DATE_UTC) 
values 
(S1520_FUNCTION_LIST_TASK.nextval,'Loaner Extn Request Report','Loaner Extension Request Report.',null,92262,null,'2765071',CURRENT_DATE,null,null,'/gmLoanerExtReqRpt.do?method=loadLoanerExtnReqRpt',null,null,2005,101921,null);

--Synonynm for new package files
create or replace public synonym gm_pkg_op_loaner_ext_rpt for GLOBUS_APP.gm_pkg_op_loaner_ext_rpt;
create or replace public synonym gm_pkg_op_loaner_extn_txn for GLOBUS_APP.gm_pkg_op_loaner_extn_txn;