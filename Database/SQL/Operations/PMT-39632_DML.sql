             
 --create left link for PMT-39632
 INSERT INTO t1520_function_list 
              (C1520_FUNCTION_ID,
               C1520_FUNCTION_NM,
               C1520_FUNCTION_DESC,
               C901_TYPE,
               C1520_CREATED_BY,
               C1520_CREATED_DATE,
			   C1520_REQUEST_URI,
			   C901_DEPARTMENT_ID,
               C901_LEFT_MENU_TYPE) 
        VALUES 
          ('FINISHED_GOODS_DASH',
            'Finished Goods Dashboard',
            'This reports is used to display the count of Finished Goods Dashboard',
            92262,
            '303013',
            current_date,
            '/gmFGDashboard.do?method=loadFGDashBoard',   
            2009,101921);
  
            
  --create synonymn for PMT-39632
  CREATE OR REPLACE  public synonym gm_pkg_op_FG_dashboard FOR GLOBUS_APP.gm_pkg_op_FG_dashboard;
  
