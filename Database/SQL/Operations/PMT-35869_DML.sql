INSERT INTO t1520_function_list  (
        C1520_FUNCTION_ID, 
        C1520_FUNCTION_NM,
        C1520_FUNCTION_DESC,
        C1520_INHERITED_FROM_ID,
        C901_TYPE, C1520_CREATED_BY, 
        C1520_CREATED_DATE
        )VALUES( 'VENDOR_PORTAL_MAP_AC',
        'Vendor Portal Mapping Access',
        'Enable/disable Vendor Portal Mapping button in vendor setup screen', 
        NULL, 
        92267, 
        '303013', 
        current_date 
        );                    
