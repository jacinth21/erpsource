UPDATE T906_Rules
SET C906_rule_value = 'div-do-tag-id' ,
c906_void_fl = null,
C906_Last_Updated_By = 3178209,
C906_Last_Updated_Date = CURRENT_DATE
WHERE C906_Rule_Id = '1000'
AND C906_Rule_Grp_Id = 'MODULEACCESS';


UPDATE T906_RULES
SET C906_rule_value      = 'btn-catalog,btn-collateral,btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-NPI-info,div-do-record-tags',
  C906_Last_Updated_By   = 3178209,
  C906_Last_Updated_Date = CURRENT_DATE
WHERE C906_Rule_Id       IN ('1025','1026') 
AND C906_Rule_Grp_Id     ='MODULEACCESS';