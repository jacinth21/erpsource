ALTER TABLE t2551_part_control_number_log RENAME TO TEMP_part_control_number_log;

CREATE TABLE t2551_part_control_number_log AS SELECT * FROM TEMP_part_control_number_log WHERE 1 =2;

INSERT INTO t2551_part_control_number_log
 ( C2551_LOG_ID                             
, C205_PART_NUMBER_ID                
, C2550_PART_CONTROL_NUMBER_ID          
, C2551_CREATED_BY                           
, C2551_CREATED_DATE                                  
, C2550_LAST_UPDATE_TRANS_ID                  
, C1900_COMPANY_ID                                  
, C5040_PLANT_ID                                    
, C2550_CONTROL_NUMBER                        
, C2550_LOT_STATUS                                  
, C901_LAST_UPDATED_WAREHOUSE_TYPE                  
, C901_LAST_UPDATED_TRANS_TYPE                      
, C2550_LAST_UPDATED_TRANS_DATE                       
, C704_ACCOUNT_NM                            
, C701_DISTRIBUTOR_NAME                      
, C901_REF_TYPE                                     
, C2550_REF_ID  )
(
  SELECT S5061_control_number_inv_log.NEXTVAL, C205_PART_NUMBER_ID ,0, C5061_CREATED_BY,  c5061_created_date
       , C5061_LAST_UPDATE_TRANS_ID, C1900_COMPANY_ID, C5040_PLANT_ID, c5060_control_number, NULL
	   , C901_WAREHOUSE_TYPE, C901_LAST_TRANSACTION_TYPE, c5061_created_date, NULL, NULL, C901_REF_TYPE, C5060_REF_ID
  from (
  select row_number() over(partition by c5060_control_number, C5061_LAST_UPDATE_TRANS_ID order by c5060_control_number, C5061_LAST_UPDATE_TRANS_ID) RNUM, temp.* from ( 
  select *
  from t5061_control_number_inv_log t5061
   ORDER BY  c5060_control_number, C5061_LAST_UPDATE_TRANS_ID,c5061_txn_qty desc, c5061_created_date desc
    ) temp)
  where RNUM = 1  );
  
  ALTER TRIGGER trg_t2550_part_control_number DISABLE;

 --moving lot controlled status from c2550_lot_status to c901_lot_controlled_status
update t2550_part_control_number set c901_lot_controlled_status='105008',c2550_lot_status=null,c2550_last_updated_by=706328,c2550_last_updated_date=sysdate  where c2550_lot_status ='105008';
--updating old usage lots status to implanted
update t2550_part_control_number set c2550_lot_status='105000',c2550_last_updated_by=706328,c2550_last_updated_date=sysdate  where c2550_lot_status ='105006';
--existing records status update
UPDATE t2550_part_control_number t2550a
SET t2550a.c2550_last_updated_by=706328,t2550a.c2550_last_updated_date=sysdate, t2550a.c2550_lot_status =
  (SELECT c906_rule_value
  FROM T906_RULES t906,
    t2550_part_control_number t2550
  WHERE t906.c906_rule_grp_id                 = 'LOTSTATUSPOS'
  AND t906.c906_rule_id                       = t2550.C901_LAST_UPDATED_WAREHOUSE_TYPE
  AND t906.c901_rule_type                     =t2550.C901_LAST_UPDATED_TRANS_TYPE
  AND t2550.c901_last_updated_warehouse_type IS NOT NULL
  AND t2550.c2550_lot_status                 IS NULL
  AND t2550.c2550_control_number              = t2550a.c2550_control_number
  AND t2550.c205_part_number_id               = t2550a.c205_part_number_id
  AND t906.c906_void_fl                      IS NULL
  GROUP BY t2550.c2550_control_number,
    c906_rule_value
  )
WHERE t2550a.c901_last_updated_warehouse_type IS NOT NULL
AND t2550a.c2550_lot_status                   IS NULL;

ALTER TRIGGER trg_t2550_part_control_number ENABLE;