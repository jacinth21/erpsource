
INSERT INTO t901b_code_group_master
           (C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
     VALUES
(s901b_code_group_master.NEXTVAL,'UPBLR','Update Bill Return','','2765071',CURRENT_DATE);


INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(109940,'Reason 1','UPBLR','0','1','','','2765071',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(109941,'Reason 2','UPBLR','0','2','','','2765071',CURRENT_DATE);
 

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(109942,'Reason 3','UPBLR','0','3','','','2765071',CURRENT_DATE);


INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(109943,'Update Bill Only Return','UPBLR','0','4','UPBLR','','2765071',CURRENT_DATE);


DELETE FROM T906_RULES where C906_RULE_GRP_ID = 'CMNCNCL' and C906_RULE_ID = '109743';
DELETE FROM T906_RULES where C906_RULE_GRP_ID = 'CMNCNCOMMENTS' and C906_RULE_ID = '109743';


DELETE FROM T906_RULES where C906_RULE_GRP_ID = 'CMNCNCL' and C906_RULE_ID = '109943';
DELETE FROM T906_RULES where C906_RULE_GRP_ID = 'CMNCNCOMMENTS' and C906_RULE_ID = '109943';

INSERT INTO t906_rules (c906_rule_seq_id, c906_rule_id ,C906_RULE_DESC,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by)
       VALUES 
                  (s906_rule.nextval,'109943','Update Bill Only Return','gm_pkg_op_backorder_txn.gm_upd_bo_to_bill_only_return','CMNCNCL',current_date,'2765071'); 
                  
                  
INSERT INTO t906_rules (c906_rule_seq_id, c906_rule_id ,C906_RULE_DESC,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by)
       VALUES 
                  (s906_rule.nextval,'109943','Update Bill Only Return','Y','CMNCNCOMMENTS',current_date,'2765071'); 
     
DELETE FROM T906_RULES where C906_RULE_GRP_ID = 'SALES-RETURN' and C906_RULE_ID = '109940';
DELETE FROM T906_RULES where C906_RULE_GRP_ID = 'SALES-RETURN' and C906_RULE_ID = '109941';
DELETE FROM T906_RULES where C906_RULE_GRP_ID = 'SALES-RETURN' and C906_RULE_ID = '109942';

                  
 INSERT INTO t906_rules (c906_rule_seq_id, c906_rule_id ,C906_RULE_DESC,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by)
       VALUES 
                  (s906_rule.nextval,'109940','Voiding Prior Month backorders','4818','SALES-RETURN',current_date,'2765071'); 
                  
  INSERT INTO t906_rules (c906_rule_seq_id, c906_rule_id ,C906_RULE_DESC,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by)
       VALUES 
                  (s906_rule.nextval,'109941','Voiding Prior Month backorders','4818','SALES-RETURN',current_date,'2765071'); 

                  
   INSERT INTO t906_rules (c906_rule_seq_id, c906_rule_id ,C906_RULE_DESC,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by)
       VALUES 
                  (s906_rule.nextval,'109942','Voiding Prior Month backorders','4818','SALES-RETURN',current_date,'2765071'); 

DELETE FROM T906_RULES where C906_RULE_GRP_ID = 'SHOW-ICT-CREDIT-SALE' and C906_RULE_ID = '109940';
DELETE FROM T906_RULES where C906_RULE_GRP_ID = 'SHOW-ICT-CREDIT-SALE' and C906_RULE_ID = '109941';
DELETE FROM T906_RULES where C906_RULE_GRP_ID = 'SHOW-ICT-CREDIT-SALE' and C906_RULE_ID = '109942';   


INSERT INTO t906_rules (c906_rule_seq_id, c906_rule_id ,C906_RULE_DESC,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by)
       VALUES 
                  (s906_rule.nextval,'109940','To allow retrun pending credit on Voiding Prior Month backorders','Y','SHOW-ICT-CREDIT-SALE',current_date,'2765071'); 
                  
  INSERT INTO t906_rules (c906_rule_seq_id, c906_rule_id ,C906_RULE_DESC,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by)
       VALUES 
                  (s906_rule.nextval,'109941','To allow retrun pending credit on Voiding Prior Month backorders','Y','SHOW-ICT-CREDIT-SALE',current_date,'2765071'); 

                  
   INSERT INTO t906_rules (c906_rule_seq_id, c906_rule_id ,C906_RULE_DESC,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by)
       VALUES 
                  (s906_rule.nextval,'109942','To allow retrun pending credit on Voiding Prior Month backorders','Y','SHOW-ICT-CREDIT-SALE',current_date,'2765071'); 

                  
  


                  

                  

 