CREATE OR REPLACE PUBLIC SYNONYM gm_pkg_inv_tag_share_option_txn FOR GLOBUS_APP.gm_pkg_inv_tag_share_option_txn;

INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES
(s901b_code_group_master.NEXTVAL,'TGSHRE','Tag Share Options','','3178209',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(111140,'Promote Share','TGSHRE','1','1','','','3178209',CURRENT_DATE);
 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(111141,'Share','TGSHRE','1','2','','','3178209',CURRENT_DATE);
 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(111142,'Do not Share','TGSHRE','1','3','','','3178209',CURRENT_DATE);
--

--update share Options for all the tag
UPDATE T5010g_Tag_System_Locator
SET C901_Tag_Share_Options = 111141,
  C5010g_Updated_By        = 3178209,
  C5010g_Updated_Date      = CURRENT_DATE
WHERE C5010g_Void_Fl      IS NULL;
--

--Job entry for Tag Share option--
DELETE FROM T9300_Job WHERE C9300_Job_Id =1061;

INSERT
INTO T9300_Job
  (
    C9300_Job_Id,
    C9300_Job_Nm,
    C9300_What,
    C9300_Start_Date ,
    C9300_Created_By ,
    C9300_Created_Date,
    C9300_Inactive_Fl
  )
  VALUES
  (
    1061,
    'INV_SYS_LOC_TAG_SHARE_OPT_MAIN',
    'gm_pkg_inv_tag_system_locator_job.gm_process_tag_share_option_main()',
    NULL,
    '3178209' ,
    CURRENT_DATE,
    NULL
  );
  --