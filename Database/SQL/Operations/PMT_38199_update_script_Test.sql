update t2550_part_control_number set c2550_expiry_date=TO_DATE('06/09/2020', 'MM/DD/YYYY'),c2550_last_updated_by='303044',c2550_last_updated_date=SYSDATE where c2550_part_control_number_id='789866'; --28 days
update t2550_part_control_number set c2550_expiry_date=TO_DATE('06/09/2020', 'MM/DD/YYYY'),c2550_last_updated_by='303044',c2550_last_updated_date=SYSDATE where c2550_part_control_number_id='828765'; --28
update t2550_part_control_number set c2550_expiry_date=TO_DATE('06/08/2020', 'MM/DD/YYYY'),c2550_last_updated_by='303044',c2550_last_updated_date=SYSDATE where c2550_part_control_number_id='791269'; --27 days
update t2550_part_control_number set c2550_expiry_date=TO_DATE('06/10/2020', 'MM/DD/YYYY'),c2550_last_updated_by='303044',c2550_last_updated_date=SYSDATE where c2550_part_control_number_id='791530'; --29 days
update t2550_part_control_number set c2550_expiry_date=TO_DATE('06/11/2020', 'MM/DD/YYYY'),c2550_last_updated_by='303044',c2550_last_updated_date=SYSDATE where c2550_part_control_number_id='794515'; --30 days
update t2550_part_control_number set c2550_expiry_date=TO_DATE('05/28/2020', 'MM/DD/YYYY'),c2550_last_updated_by='303044',c2550_last_updated_date=SYSDATE where c2550_part_control_number_id='794664'; --16 days
update t2550_part_control_number set c2550_expiry_date=TO_DATE('06/02/2020', 'MM/DD/YYYY'),c2550_last_updated_by='303044',c2550_last_updated_date=SYSDATE where c2550_part_control_number_id='791337 '; --22 days
update t2550_part_control_number set c2550_expiry_date=TO_DATE('05/29/2020', 'MM/DD/YYYY'),c2550_last_updated_by='303044',c2550_last_updated_date=SYSDATE where c2550_part_control_number_id='828303'; --17 days
update t2550_part_control_number set c2550_expiry_date=TO_DATE('05/30/2020', 'MM/DD/YYYY'),c2550_last_updated_by='303044',c2550_last_updated_date=SYSDATE where c2550_part_control_number_id='828765'; --18 days
update t2550_part_control_number set c2550_expiry_date=TO_DATE('06/01/2020', 'MM/DD/YYYY'),c2550_last_updated_by='303044',c2550_last_updated_date=SYSDATE where c2550_part_control_number_id='793599'; --20 days

-- 15 days
update t2550_part_control_number set c2550_expiry_date=TO_DATE('05/27/2020', 'MM/DD/YYYY'),c2550_last_updated_by='303044',c2550_last_updated_date=SYSDATE where c2550_part_control_number_id='871670'; --15 days, inventory
update t2550_part_control_number set c2550_expiry_date=TO_DATE('05/26/2020', 'MM/DD/YYYY'),c2550_last_updated_by='303044',c2550_last_updated_date=SYSDATE where c2550_part_control_number_id='871694'; --14 days, returns hold
update t2550_part_control_number set c2550_expiry_date=TO_DATE('05/25/2020', 'MM/DD/YYYY'),c2550_last_updated_by='303044',c2550_last_updated_date=SYSDATE where c2550_part_control_number_id='790042'; --13 days, quarantine
update t2550_part_control_number set c2550_expiry_date=TO_DATE('05/22/2020', 'MM/DD/YYYY'),c2550_last_updated_by='303044',c2550_last_updated_date=SYSDATE where c2550_part_control_number_id='877316'; --10 days, bulk
update t2550_part_control_number set c2550_expiry_date=TO_DATE('05/21/2020', 'MM/DD/YYYY'),c2550_last_updated_by='303044',c2550_last_updated_date=SYSDATE where c2550_part_control_number_id='935048'; --9 days, consignmed
update t2550_part_control_number set c2550_expiry_date=TO_DATE('05/14/2020', 'MM/DD/YYYY'),c2550_last_updated_by='303044',c2550_last_updated_date=SYSDATE where c2550_part_control_number_id='794023'; --2 days, repackage
update t2550_part_control_number set c2550_expiry_date=TO_DATE('05/13/2020', 'MM/DD/YYYY'),c2550_last_updated_by='303044',c2550_last_updated_date=SYSDATE where c2550_part_control_number_id='827258'; --1 days, inhouse

-- 40days
update t2550_part_control_number set c2550_expiry_date=TO_DATE('07/27/2020', 'MM/DD/YYYY'),c2550_last_updated_by='303044',c2550_last_updated_date=SYSDATE where c2550_part_control_number_id='791992'; --above 30 days