INSERT
   INTO T1520_FUNCTION_LIST
    (
        C1520_FUNCTION_ID, C1520_FUNCTION_NM, C1520_FUNCTION_DESC
      , C1520_INHERITED_FROM_ID, C901_TYPE, C1520_VOID_FL
      , C1520_CREATED_BY, C1520_CREATED_DATE, C1520_LAST_UPDATED_BY
      , C1520_LAST_UPDATED_DATE, C1520_REQUEST_URI, C1520_REQUEST_STROPT
      , C1520_SEQ_NO, C901_DEPARTMENT_ID
    )
    VALUES
    (
        'THB_Receive_Load', 'THB Receiving Transaction', 'This screen loads THBLoad Receiving Load Transaction'
      , NULL, 92262, NULL
      , '2277820', sysdate, null
      , null, '\gmTHBReceivingLoadTran.do?method=fetchTHBHeadLoadDetails', null
      , NULL, '2024'
    ) ; 