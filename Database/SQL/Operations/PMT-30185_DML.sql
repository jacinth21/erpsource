Delete From T906_Rules Where C906_Rule_Grp_Id='EXCL_APP_MBL_VIEW';

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Value,C906_Rule_Desc, C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,C1900_Company_Id)Values ( S906_Rule.Nextval,'MOBILE_VIEW_APP','btn-crm-mob','Exclude CRM module for the Globus App in mobile',Sysdate,'EXCL_APP_MBL_VIEW',3178209,1000 );
INSERT INTO T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Value,C906_Rule_Desc,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,C1900_Company_Id)Values ( S906_Rule.Nextval,'MOBILE_VIEW_APP','btn-pricing-mob','Exclude Pricing module for the Globus App in mobile',Sysdate,'EXCL_APP_MBL_VIEW',3178209,1000);
