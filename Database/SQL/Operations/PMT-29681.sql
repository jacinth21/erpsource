INSERT INTO t1520_function_list 
(C1520_FUNCTION_ID,
C1520_FUNCTION_NM,
C1520_FUNCTION_DESC,
C901_TYPE,
C1520_CREATED_BY,
C1520_CREATED_DATE,
C1520_REQUEST_URI,
C901_DEPARTMENT_ID,
C901_LEFT_MENU_TYPE) 
VALUES 
('LOT_ERROR_REPORT',
'Lot Error Report',
'Create a new Lot Error Report',
92262,
'303013',
current_date,
'/gmLotErrorReport.do?method=loadLotReport', 
2009,101921);

--Access to Prod Development Group
DELETE FROM t1530_access WHERE c1520_function_id = 'LOT_ERROR_REPORT' AND c1500_group_id = '100000015';
INSERT INTO t1530_access (c1530_access_id, c1500_group_id, c1520_function_id, c101_party_id, c1530_update_access_fl, c1530_read_access_fl,c1530_void_access_fl, c1530_void_fl, c1530_created_by,c1530_created_date) VALUES (s1530_access.nextval, '100000015', 'LOT_ERROR_REPORT',NULL, 'Y', 'Y','Y', NULL, '2193221',SYSDATE);

DELETE FROM t906_rules WHERE c906_rule_grp_id in ('LOTSTATUSPOS','LOTSTATUSNEG') AND C901_RULE_TYPE='2525';
INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C901_RULE_TYPE,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY) VALUES (S906_RULE.NEXTVAL,'4000339','2525','Back Order','105007',CURRENT_DATE,'LOTSTATUSPOS','706328');
INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C901_RULE_TYPE,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY) VALUES (S906_RULE.NEXTVAL,'4000339','2525','Back Order','105000',CURRENT_DATE,'LOTSTATUSNEG','706328');

--Insert script for t100_error_msg 
--DELETE  FROM  t100_error_msg WHERE c100_error_code in('501','502');
--INSERT INTO t100_error_msg (c100_error_msg_id ,C100_ERROR_CODE, c100_ora_error_code, c100_error_msg, c901_language_id, c100_error_void_fl, c100_created_by, c100_created_date, c100_last_updated_by, c100_last_updated_date, c100_last_updated_date_utc )VALUES (467 ,'501', '-20999', 'Record is already voided', 103097, null, null, sysdate, null, null, null );
--INSERT INTO t100_error_msg (c100_error_msg_id ,C100_ERROR_CODE, c100_ora_error_code, c100_error_msg, c901_language_id, c100_error_void_fl, c100_created_by, c100_created_date, c100_last_updated_by, c100_last_updated_date, c100_last_updated_date_utc )VALUES (469 ,'502', '-20999', 'Record is not in valid status', 103097, null, null, sysdate, null, null, null );
