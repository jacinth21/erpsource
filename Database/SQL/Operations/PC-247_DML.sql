delete from T211_Document_History where c211_doc_seq_id = 19;

INSERT
INTO t211_document_history
  (
    c211_active_fl,
    c211_rev,
    c210_document_id,
    c211_activated_date,
    c211_doc_dco_id,
    c211_created_by,
    c211_created_date,
    c211_doc_file_name,
    c211_doc_seq_id
  )
  VALUES
  (
    'Y',
    'J',
    'GM-G003',
    sysdate,
    '20-001510',
    3178209,
    sysdate,
    'GmDHRPrintProductTravelerJ.jsp',
    19
  );
  