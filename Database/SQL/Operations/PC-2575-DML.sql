--insert new rule vaule for skip sterile
DELETE FROM T906_RULES WHERE C906_RULE_ID = 'GM11-103' AND C906_RULE_GRP_ID = 'STERILEORDTYPBYPLANT' AND C1900_COMPANY_ID = '1017';

INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'GM11-103','Rule to update order type','Y',CURRENT_DATE,'STERILEORDTYPBYPLANT','2765071',1017);

--Create synonynm for new package and function
create or replace public synonym gm_pkg_op_split_process FOR GLOBUS_APP.gm_pkg_op_split_process;
create or replace public synonym get_quantity_in_stock_byplant FOR GLOBUS_APP.get_quantity_in_stock_byplant;