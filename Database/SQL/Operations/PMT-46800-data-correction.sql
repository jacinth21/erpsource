/*
 This is used create lot details for all the sterile parts used loaner sets - PMT-46800
*/
SET SERVEROUTPUT ON;
DECLARE
  v_trans_id VARCHAR2 (50);
  v_loc_part_id t5053_location_part_mapping.c5053_location_part_map_id%TYPE;
  v_set_lot_master_id t5070_set_lot_master.c5070_set_lot_master_id%TYPE;
  v_set_part_qty_id t5071_set_part_qty.c5071_set_part_qty_id%TYPE;
  v_cnt     NUMBER;
  v_lcn_cnt NUMBER;
  v_lcn_id t5052_location_master.c5052_location_id%TYPE;
  v_setlot_id NUMBER;
  v_slot_cnt  NUMBER;
  CURSOR consignment_details
  IS
    SELECT t504.c504_consignment_id
    FROM t504a_consignment_loaner t504a ,
      t504_consignment t504 ,
      t207_set_master t207
    WHERE t504a.c504_consignment_id = t504.c504_consignment_id
    AND c504_type                   = '4127'
    AND t504.c1900_company_id       = '1000'
    AND c504a_status_fl            !=60
    AND t504.c504_void_fl          IS NULL
    AND t504a.c504a_void_fl        IS NULL
    AND t207.c207_set_id            = t504.c207_set_id
	AND t504.c504_consignment_id = v_trans_id --'GM-CN-376128'
    AND c207_lot_track_fl           ='Y';
  CURSOR part_details
  IS
    SELECT v_trans_id consignment_id,
      t205.c205_part_number_id pnum,
      'NOC#' cnum,
      NVL(cons.qty, 0) - NVL (miss.qty, 0) qty,
      (SELECT c5070_set_lot_master_id
      FROM t5070_set_lot_master
      WHERE c901_txn_id     = v_trans_id
      AND c901_txn_type     = 4127
      AND c5070_void_fl    IS NULL
      AND c901_location_type='93502'
      ) setlotmasterid,
    4127 txntype,
    4127 type,
    706328 userid
  FROM t205_part_number t205,
    (SELECT t505.c205_part_number_id,
      SUM (t505.c505_item_qty) qty
    FROM t505_item_consignment t505
    WHERE t505.c504_consignment_id = v_trans_id
    AND t505.c505_void_fl         IS NULL
    GROUP BY t505.c205_part_number_id
    ) cons,
    (SELECT c205_part_number_id,
      SUM (qty) qty,
      SUM (qtyrecon) qtyrecon,
      SUM (missing_qty) missing_qty
    FROM
      (SELECT t413.c205_part_number_id,
        DECODE (c412_type, 50151, 0, DECODE (t413.c413_status_fl, 'Q', NVL (t413.c413_item_qty, 0 ), 0 ) ) qty,
        DECODE (c412_type, 50151, DECODE (t413.c413_status_fl, 'Q', NVL (t413.c413_item_qty, 0 ), 0 ), 0 ) missing_qty,
        DECODE (c412_type, 50151, NVL (t413.c413_item_qty, 0 ), DECODE (NVL (t413.c413_status_fl, 'Q' ), 'Q', 0, NVL (t413.c413_item_qty, 0) ) ) qtyrecon
      FROM t412_inhouse_transactions t412,
        t413_inhouse_trans_items t413
      WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
      AND t412.c412_ref_id             = v_trans_id
      AND t412.c412_void_fl           IS NULL
      AND t413.c413_void_fl           IS NULL
      )
    GROUP BY c205_part_number_id
    ) miss,
    (SELECT t413.c205_part_number_id,
      SUM(t413.c413_item_qty) qty
    FROM t412_inhouse_transactions t412,
      t413_inhouse_trans_items t413
    WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
    AND c412_status_fl               < 4
    AND t412.c412_type               = 100062
    AND t413.c413_void_fl           IS NULL
    AND c412_void_fl                IS NULL
    AND t412.c412_ref_id             = v_trans_id
    GROUP BY t413.c205_part_number_id
    )loanerboqty,
    (SELECT t504.c504_consignment_id,
      t208.c205_part_number_id,
      t208.c208_set_qty qty
    FROM t208_set_details t208,
      t504_consignment t504
    WHERE t208.c208_void_fl     IS NULL
    AND t208.c208_inset_fl       = 'Y'
    AND t504.c207_set_id         = t208.c207_set_id
    AND t504.c504_void_fl       IS NULL
    AND t504.c504_consignment_id = v_trans_id
    ) setdetails
  WHERE t205.c205_part_number_id = cons.c205_part_number_id
  AND t205.c205_part_number_id   = miss.c205_part_number_id(+)
  AND t205.c205_part_number_id   = setdetails.c205_part_number_id(+)
  AND t205.c205_part_number_id   = loanerboqty.c205_part_number_id(+) ;
BEGIN
  FOR cn_details IN consignment_details
  LOOP
    BEGIN
      v_trans_id := cn_details.c504_consignment_id;
      DBMS_OUTPUT.PUT_LINE('Consignment id ------------------------------ ' || v_trans_id );
      BEGIN
        SELECT COUNT(1),
          c5052_location_id
        INTO v_cnt,
          v_lcn_id
        FROM t5052_location_master
        WHERE c5052_ref_id         = v_trans_id
        AND c5051_inv_warehouse_id = 6
        AND c5052_void_fl         IS NULL
        GROUP BY c5052_location_id;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_cnt := 0;
      END;
      IF v_cnt > 0 THEN
        -- t5052 void
        UPDATE t5052_location_master
        SET c5052_void_fl       = 'Y'
        WHERE c5052_location_id = v_lcn_id;
        --UPDATE t5053_location_part_mapping
        --SET c5053_void_fl = 'Y'
        --WHERE c5052_location_id = v_lcn_id;
        DELETE
        FROM t5053_location_part_mapping
        WHERE c5052_location_id = v_lcn_id;
      END IF;
      BEGIN
        SELECT COUNT(1),
          C5070_SET_LOT_MASTER_ID
        INTO v_slot_cnt,
          v_setlot_id
        FROM t5070_SET_LOT_master
        WHERE c901_txn_id  = v_trans_id
        AND C901_TXN_TYPE  = 4127
        AND C5070_VOID_FL IS NULL
        GROUP BY C5070_SET_LOT_MASTER_ID;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_slot_cnt := 0;
      END;
      IF v_slot_cnt > 0 THEN
        -- t5070 void
        DELETE
        FROM t5072a_set_part_lot_qty_log
        WHERE c5070_set_lot_master_id=v_setlot_id;
        DELETE FROM t5071a_set_part_qty_log WHERE c5070_set_lot_master_id=v_setlot_id;
        DELETE FROM t5072_set_part_lot_qty WHERE c5070_set_lot_master_id=v_setlot_id;
        DELETE FROM t5071_set_part_qty WHERE c5070_set_lot_master_id=v_setlot_id;
        DELETE FROM t5070_SET_LOT_master WHERE c5070_set_lot_master_id=v_setlot_id;
        
        /*UPDATE t5071_set_part_qty
        SET c5071_void_fl = 'Y'
        WHERE C5070_SET_LOT_MASTER_ID = v_setlot_id;
        UPDATE t5072_set_part_lot_qty
        SET c5072_void_fl = 'Y'
        WHERE C5070_SET_LOT_MASTER_ID = v_setlot_id; */
      END IF;
	  UPDATE t5070_SET_LOT_master
        SET c5070_void_fl = 'Y'
        WHERE c901_txn_id = v_trans_id;
      -- t5052 insert
      SELECT s5053_location_part_mapping.NEXTVAL
      INTO v_loc_part_id
      FROM DUAL;
      INSERT
      INTO t5052_location_master
        (
          c5052_location_id,
          c5052_ref_id,
          c901_status ,
          c5052_created_by,
          c5052_created_date,
          c901_zone,
          c5052_shelf,
          c901_location_type ,
          c5051_inv_warehouse_id,
          c5052_location_cd,
          c1900_company_id,
          c5040_plant_id,
          c5057_building_id -- added for PMT-33507
        )
        VALUES
        (
          TO_CHAR(v_loc_part_id),
          v_trans_id,
          93310 ,
          30301,
          CURRENT_DATE,
          NULL,
          NULL,
          93502 ,
          6,
          NULL,
          1000,3000,
          NULL
        );
      -- -- t5070 insert
      SELECT TO_CHAR (S5070_SET_LOT_MASTER.NEXTVAL)
      INTO v_set_lot_master_id
      FROM DUAL;
      INSERT
      INTO t5070_SET_LOT_master
        (
          C5070_SET_LOT_MASTER_ID,
          C901_TXN_ID,
          C901_TXN_TYPE ,
          C5052_LOCATION_ID,
          C901_LOCATION_TYPE,
          C5070_VOID_FL ,
          C5070_LAST_UPDATED_BY,
          C5070_LAST_UPDATED_DATE
        )
        VALUES
        (
          v_set_lot_master_id,
          v_trans_id,
          4127 ,
          TO_CHAR(v_loc_part_id),
          93502,
          NULL ,
          30301,
          CURRENT_DATE
        ) ;
      SELECT COUNT(t5071.c205_part_number_id)
      INTO v_lcn_cnt
      FROM t5070_set_lot_master t5070,
        t5071_set_part_qty t5071,
        t5072_set_part_lot_qty t5072
      WHERE t5070.C5070_SET_LOT_MASTER_ID = t5071.C5070_SET_LOT_MASTER_ID
      AND t5071.C5071_SET_PART_QTY_ID     = t5072.C5071_SET_PART_QTY_ID
      AND t5070.C901_TXN_ID               = v_trans_id
      AND t5070.c901_txn_type             =4127
      AND t5070.c901_location_type        = 93502
      AND t5070.c5070_void_fl            IS NULL
      AND t5071.c5071_void_fl            IS NULL
      AND t5072.c5072_void_fl            IS NULL
      AND t5072.c5072_curr_qty            >0 ;
      FOR tran_details                   IN part_details
      LOOP
        BEGIN
          BEGIN
            IF tran_details.qty > 0 THEN
              -- t5053 insert
              INSERT
              INTO t5053_location_part_mapping
                (
                  c5053_location_part_map_id,
                  c5052_location_id ,
                  c205_part_number_id ,
                  c5053_curr_qty,
                  c901_type ,
                  c5053_last_update_trans_id,
                  c901_last_transaction_type ,
                  c5053_last_txn_control_number,
                  c5053_created_by,
                  c5053_created_date
                )
                VALUES
                (
                  s5053_location_part_mapping.NEXTVAL,
                  TO_CHAR(v_loc_part_id),
                  tran_details.pnum ,
                  tran_details.qty,
                  4127,
                  v_trans_id,
                  4127 ,
                  tran_details.cnum,
                  30301 ,
                  SYSDATE
                );
              IF v_lcn_cnt = 0 THEN
                SELECT TO_CHAR (S5071_SET_PART_QTY_ID.NEXTVAL)
                INTO V_SET_PART_QTY_ID
                FROM DUAL;
                INSERT
                INTO T5071_SET_PART_QTY
                  (
                    C5071_SET_PART_QTY_ID,
                    C5070_SET_LOT_MASTER_ID,
                    C205_PART_NUMBER_ID ,
                    C5071_CURR_QTY,
                    C5053_LAST_TXN_CONTROL_NUMBER,
                    C5071_LAST_UPDATED_TXN_ID ,
                    C5053_LAST_UPDATED_BY,
                    C5053_LAST_UPDATED_DATE,
                    C901_TXN_TYPE,
                    C901_TYPE
                  )
                  VALUES
                  (
                    v_set_part_qty_id,
                    v_set_lot_master_id,
                    tran_details.pnum ,
                    tran_details.qty,
                    tran_details.cnum,
                    v_trans_id ,
                    30301,
                    CURRENT_DATE,
                    4127,4127
                  ) ;
                -- gm_pkg_op_loaner_lot_process.gm_sav_loaner_lot_update(tran_details.consignment_id, tran_details.pnum, tran_details.cnum, tran_details.qty, v_set_lot_master_id,tran_details.txntype, tran_details.type,tran_details.userid );
               -- DBMS_OUTPUT.PUT_LINE('v_trans_id ' || tran_details.consignment_id || ' v_pnum '||tran_details.pnum|| ' v_qty '||tran_details.qty|| ' 4127');
              END IF;
            END IF;
          EXCEPTION
          WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('v_trans_id ' || tran_details.consignment_id || ' v_pnum '||tran_details.pnum|| ' v_qty '||tran_details.qty|| ' 4127'|| SQLERRM);
          END;
        END;
      END LOOP;
    EXCEPTION
          WHEN OTHERS THEN
           DBMS_OUTPUT.PUT_LINE('Consignment id ------------------------------ ' || v_trans_id ||' Error'|| SQLERRM);            
    END;
  END LOOP;
END;
/ 