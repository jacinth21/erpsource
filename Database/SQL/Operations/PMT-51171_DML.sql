--Create left link for Uploaded Files - Report
INSERT INTO t1520_function_list 
(C1520_FUNCTION_ID,
C1520_FUNCTION_NM,
C1520_FUNCTION_DESC,
C901_TYPE,
C1520_CREATED_BY,
C1520_CREATED_DATE,
C1520_REQUEST_URI,
C1520_REQUEST_STROPT,
C901_DEPARTMENT_ID,
C901_LEFT_MENU_TYPE) 
VALUES 
('FILE_UPLOAD_REPORT',
'Uploaded Files Report',
'This screen is used to show the upload files',
92262,
'706322',
current_date,
'/gmPDFileUploadReport.do?method=loadUploadedFilesReport',
'',
2009,101921);