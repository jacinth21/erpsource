  --create sysnonymn for PMT-36895
   create or replace public synonym t526a_product_request_log for GLOBUS_APP.t526a_product_request_log;
   create or replace public synonym trg_t526a_utc_date for GLOBUS_APP.trg_t526a_utc_date;
   create or replace public synonym get_loaner_log_flag for GLOBUS_APP.get_loaner_log_flag;
   create or replace public synonym gm_pkg_op_loaner_request for GLOBUS_APP.gm_pkg_op_loaner_request;
   create or replace public synonym gm_pkg_op_loaner_request_rpt for GLOBUS_APP.gm_pkg_op_loaner_request_rpt;