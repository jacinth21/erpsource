--Pacakge file synonym
create or replace public synonym gm_pkg_cos_sync FOR GLOBUS_APP.gm_pkg_cos_sync;
create or replace public synonym gm_pkg_cos_report FOR GLOBUS_APP.gm_pkg_cos_report;
create or replace public synonym gm_pkg_cos_lot_track FOR GLOBUS_APP.gm_pkg_cos_lot_track;
create or replace public synonym trg_t5066_cos_part_lot_log FOR GLOBUS_APP.trg_t5066_cos_part_lot_log;
--public synonym for table
create or replace public synonym T5066_COS_PART_LOT_QTY for GLOBUS_APP.T5066_COS_PART_LOT_QTY;
create or replace public synonym T5066A_COS_PART_LOT_QTY_LOG for GLOBUS_APP.T5066A_COS_PART_LOT_QTY_LOG;
create or replace public synonym t5067_cos_order  for GLOBUS_APP.t5067_cos_order;
create or replace public synonym t5067a_cos_order_item for GLOBUS_APP.t5067a_cos_order_item;
create or replace public synonym t5068_cos_shipping for GLOBUS_APP.t5068_cos_shipping;
create or replace public synonym t5069_cos_return for GLOBUS_APP.t5069_cos_return;
create or replace public synonym t5069a_cos_return_detail for GLOBUS_APP.t5069a_cos_return_detail; 

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc, c906_rule_value,
               c906_created_date,c906_rule_grp_id,c906_created_by,c906_active_fl ) 
VALUES (s906_rule.NEXTVAL,'COS_INV_SUM_EMAIL_SUB','Rule group for COS','COS - Inventory Summary Report',
        current_date,'COS_RULE_GRP','2277820','Y');

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc, c906_rule_value,
               c906_created_date,c906_rule_grp_id,c906_created_by,c906_active_fl ) 
VALUES (s906_rule.NEXTVAL,'COS_INV_SUM_EMAIL_TO','Rule group for COS','nstrassner@bonebank.com,ccline@bonebank.com,TMajors@bonebank.com,gpalani@bonebank.com',
        current_date,'COS_RULE_GRP','2277820','Y');

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc, c906_rule_value,
               c906_created_date,c906_rule_grp_id,c906_created_by,c906_active_fl ) 
VALUES (s906_rule.NEXTVAL,'COS_SHELF_EMAIL_SUB','Rule group for Ack Order','COS - Inventory Shelf Detail Report',
        current_date,'COS_RULE_GRP','2277820','Y');
        
INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc, c906_rule_value,
               c906_created_date,c906_rule_grp_id,c906_created_by,c906_active_fl ) 
VALUES (s906_rule.NEXTVAL,'COS_SALES_EMAIL_SUB','Rule group for Ack Order','COS Account Name is updated with SpineIt Order ID',
        current_date,'COS_RULE_GRP','2277820','Y');