--Courier Mode
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(110621,'Two Day Ecomony','CCARMO','1','1','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(110623,'Standard Overnight','CCARMO','1','3','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(110622,'First Overnight','CCARMO','1','2','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(110624,'2Day AM','CCARMO','1','4','','','2277820',CURRENT_DATE); 

--Courier Service 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(110603,'Fedex','COSCAR','1','1','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(110604,'UPS','COSCAR','1','2','','','2277820',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(110605,'DHL','COSCAR','1','3','','','2277820',CURRENT_DATE);

--COS order Status 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(110613,'COS Order Initiated','COSORD','1','1','','','303013',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(110614,'COS Order Controlled','COSORD','1','2','','','303013',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(110615,'COS Order Shipped','COSORD','1','3','','','303013',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(110660,'COS Order Draft','COSORD','1','4','','','303013',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26241145,'COS Order Voided','COSORD','1','','','','303013',CURRENT_DATE);

--Return Order Status
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(110760,'COS Return Initiated','COSRTN','1','1','','','2277820',CURRENT_DATE);
 

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(110761,'COS Return Processed','COSRTN','1','2','','','2277820',CURRENT_DATE);
-------------------------------------------------------------

--Return Inventory Type
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(110740,'Return to COS','COSRET','1','1','','','2277820',CURRENT_DATE);
 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(110741,'Quarantine','COSRET','1','2','','','2277820',CURRENT_DATE);
------------------------------------------------------------------------------
--Shipment Box Dimension

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(110720,'Small','COSSHI','1','1','','','2277820',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(110721,'Medium','COSSHI','1','2','','','2277820',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(110722,'Large','COSSHI','1','3','','','2277820',CURRENT_DATE);
---------------------------------------------------------
--COS Lot Inventory Type or status

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(110600,'COS-BILLED','COSSTA','1','1','','','2598779',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(110601,'COS-PENDING SHIPPING','COSSTA','1','2','','','2598779',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(110602,'COS-QUARANTINE','COSSTA','1','3','','','2598779',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(110620,'COS-SHIPPED','COSSTA','1','4','','','2598779',CURRENT_DATE);
