INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc, c906_rule_value, c906_created_date,c906_rule_grp_id,c906_created_by,c906_active_fl ) 
VALUES (s906_rule.NEXTVAL,'COS_LOT_EXPIRY_DAY','Rule group for Ack Order','30', current_date,'COS_RULE_GRP','2277820','Y');
        
UPDATE t907_shipping_info SET c907_cos_sync_fl='Y', c907_last_updated_by='2277820', c907_last_updated_date=current_date
 WHERE c907_ship_to_id  =  '16116'
   AND c907_status_fl = 40
   AND c907_void_fl IS NULL;