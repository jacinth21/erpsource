create or replace public synonym gm_pkg_cos_order FOR GLOBUS_APP.gm_pkg_cos_order;
create or replace public synonym gm_pkg_cos_shipping FOR GLOBUS_APP.gm_pkg_cos_shipping;
create or replace public synonym gm_pkg_cos_returns FOR GLOBUS_APP.gm_pkg_cos_returns;
create or replace public synonym gm_pkg_cos_lot_track FOR GLOBUS_APP.gm_pkg_cos_lot_track;
create or replace public synonym trg_t5066_cos_part_lot_log FOR GLOBUS_APP.trg_t5066_cos_part_lot_log;
create or replace public synonym T5066A_COS_PART_LOT_QTY_LOG for GLOBUS_APP.T5066A_COS_PART_LOT_QTY_LOG;