--For Left link
INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'IHTYPE','Inhouse transaction Type','','3178209',CURRENT_DATE);
  
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(111800,'Quarantine - Inhouse','IHTYPE','1','1','QNIN','','3178209',CURRENT_DATE);
 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(111801,'Bulk - Inhouse','IHTYPE','1','2','BLIN','','3178209',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(111802,'Finished Goods - Inhouse','IHTYPE','1','3','FGIN','','3178209',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(111803,'Repackaging - Inhouse','IHTYPE','1','4','PNIN','','3178209',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(111804,'IH Inventory - Inhouse','IHTYPE','1','5','IHIN','','3178209',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(111805,'Restricted Warehouse - Inhouse','IHTYPE','1','6','RWIN','','3178209',CURRENT_DATE);

UPDATE T901_CODE_LOOKUP
   SET C901_CODE_GRP        = 'CONTY',
       c901_code_seq_no = '128',
       C901_LAST_UPDATED_BY   = 3178209,
       C901_LAST_UPDATED_DATE = CURRENT_DATE
 WHERE C901_CODE_ID       = '111800'; 

UPDATE T901_CODE_LOOKUP
   SET C901_CODE_GRP        = 'CONTY',
       c901_code_seq_no = '129',
       C901_LAST_UPDATED_BY   = 3178209,
       C901_LAST_UPDATED_DATE = CURRENT_DATE
 WHERE C901_CODE_ID       = '111801'; 

UPDATE T901_CODE_LOOKUP
   SET C901_CODE_GRP        = 'CONTY',
       c901_code_seq_no = '130',
       C901_LAST_UPDATED_BY   = 3178209,
       C901_LAST_UPDATED_DATE = CURRENT_DATE
 WHERE C901_CODE_ID       = '111802'; 

UPDATE T901_CODE_LOOKUP
   SET C901_CODE_GRP        = 'CONTY',
       c901_code_seq_no = '131',
       C901_LAST_UPDATED_BY   = 3178209,
       C901_LAST_UPDATED_DATE = CURRENT_DATE
 WHERE C901_CODE_ID       = '111803'; 

UPDATE T901_CODE_LOOKUP
   SET C901_CODE_GRP        = 'CONTY',
       c901_code_seq_no = '132',
       C901_LAST_UPDATED_BY   = 3178209,
       C901_LAST_UPDATED_DATE = CURRENT_DATE
 WHERE C901_CODE_ID       = '111804'; 

UPDATE T901_CODE_LOOKUP
   SET C901_CODE_GRP        = 'CONTY',
       c901_code_seq_no = '133',
       C901_LAST_UPDATED_BY   = 3178209,
       C901_LAST_UPDATED_DATE = CURRENT_DATE
 WHERE C901_CODE_ID       = '111805'; 

--Create left link for Quarantine � Inhouse
SET DEFINE OFF;
INSERT INTO t1520_function_list (C1520_FUNCTION_ID,C1520_FUNCTION_NM,C1520_FUNCTION_DESC,C901_TYPE,C1520_CREATED_BY,C1520_CREATED_DATE,C1520_REQUEST_URI,C1520_REQUEST_STROPT,C901_DEPARTMENT_ID,C901_LEFT_MENU_TYPE) 
VALUES ('INHOUSE_QN_TXN','Quarantine - Inhouse','This screen is used for item initiate from Quarantine Inventory',92262,'3178209',current_date,'\gmInHouseTransactionSetup.do?method=loadInHouseTxnInfo&IHTransType=111800','',2013,101921);

--Create left link for Finished Goods � Inhouse
INSERT INTO t1520_function_list (C1520_FUNCTION_ID,C1520_FUNCTION_NM,C1520_FUNCTION_DESC,C901_TYPE,C1520_CREATED_BY,C1520_CREATED_DATE,C1520_REQUEST_URI,C1520_REQUEST_STROPT,C901_DEPARTMENT_ID,C901_LEFT_MENU_TYPE) 
VALUES ('INHOUSE_FG_TXN','Finished Goods - Inhouse','This screen is used for item initiate from Finished Goods Inventory',92262,'3178209',current_date,'\gmInHouseTransactionSetup.do?method=loadInHouseTxnInfo&IHTransType=111802','',2013,101921);

--Create left link for Bulk � Inhouse
INSERT INTO t1520_function_list (C1520_FUNCTION_ID,C1520_FUNCTION_NM,C1520_FUNCTION_DESC,C901_TYPE,C1520_CREATED_BY,C1520_CREATED_DATE,C1520_REQUEST_URI,C1520_REQUEST_STROPT,C901_DEPARTMENT_ID,C901_LEFT_MENU_TYPE) 
VALUES ('INHOUSE_BL_TXN','Bulk - Inhouse','This screen is used for item initiate from Bulk Inventory',92262,'3178209',current_date,'\gmInHouseTransactionSetup.do?method=loadInHouseTxnInfo&IHTransType=111801','',2013,101921);

--Create left link for Repackaging � Inhouse
INSERT INTO t1520_function_list (C1520_FUNCTION_ID,C1520_FUNCTION_NM,C1520_FUNCTION_DESC,C901_TYPE,C1520_CREATED_BY,C1520_CREATED_DATE,C1520_REQUEST_URI,C1520_REQUEST_STROPT,C901_DEPARTMENT_ID,C901_LEFT_MENU_TYPE) 
VALUES ('INHOUSE_PN_TXN','Repackaging - Inhouse','This screen is used for item initiate from Repackage Inventory',92262,'3178209',current_date,'\gmInHouseTransactionSetup.do?method=loadInHouseTxnInfo&IHTransType=111803','',2013,101921);

--Create left link for IH Inventory � Inhouse
INSERT INTO t1520_function_list (C1520_FUNCTION_ID,C1520_FUNCTION_NM,C1520_FUNCTION_DESC,C901_TYPE,C1520_CREATED_BY,C1520_CREATED_DATE,C1520_REQUEST_URI,C1520_REQUEST_STROPT,C901_DEPARTMENT_ID,C901_LEFT_MENU_TYPE) 
VALUES ('INHOUSE_IH_TXN','IH Inventory - Inhouse','This screen is used for item initiate from Inhouse Inventory',92262,'3178209',current_date,'\gmInHouseTransactionSetup.do?method=loadInHouseTxnInfo&IHTransType=111804','',2013,101921);

--Create left link for  Restricted Warehouse � Inhouse.
INSERT INTO t1520_function_list (C1520_FUNCTION_ID,C1520_FUNCTION_NM,C1520_FUNCTION_DESC,C901_TYPE,C1520_CREATED_BY,C1520_CREATED_DATE,C1520_REQUEST_URI,C1520_REQUEST_STROPT,C901_DEPARTMENT_ID,C901_LEFT_MENU_TYPE) 
VALUES ('INHOUSE_RW_TXN','Restricted Warehouse - Inhouse','This screen is used for item initiate from Restricted Warehouse Inventory',92262,'3178209',current_date,'\gmInHouseTransactionSetup.do?method=loadInHouseTxnInfo&IHTransType=111805','',2013,101921);

--Posting code lookup's
	--s901_code_lookup.NEXTVAL
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26241334,'BL to Customs','TXNTP','1','','','','3178209',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26241333,'BL to In-House','TXNTP','1','','','','3178209',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26241332,'BL to PD expense','TXNTP','1','','','','3178209',CURRENT_DATE);

 --
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26241325,'IH to Customs','TXNTP','1','','','','3178209',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26241324,'IH to In-House','TXNTP','1','','','','3178209',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26241323,'IH to PD expense','TXNTP','1','','','','3178209',CURRENT_DATE);
--
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26241328,'PN to Customs','TXNTP','1','','','','3178209',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26241327,'PN to In-House','TXNTP','1','','','','3178209',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26241326,'PN to PD expense','TXNTP','1','','','','3178209',CURRENT_DATE);
 --
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26241322,'QN to Customs','TXNTP','1','','','','3178209',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26241321,'QN to In-House','TXNTP','1','','','','3178209',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26241320,'QN to PD expense','TXNTP','1','','','','3178209',CURRENT_DATE);
--
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26241331,'RW to Customs','TXNTP','1','','','','3178209',CURRENT_DATE);
 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26241330,'RW to In-House','TXNTP','1','','','','3178209',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26241329,'RW to PD expense','TXNTP','1','','','','3178209',CURRENT_DATE);

UPDATE T901_CODE_LOOKUP
   SET c901_code_nm        = 'Restricted Warehouse to Customs',
       C901_LAST_UPDATED_BY   = 3178209,
       C901_LAST_UPDATED_DATE = CURRENT_DATE
 WHERE C901_CODE_ID       = '26241331'; 
 
 UPDATE T901_CODE_LOOKUP
   SET c901_code_nm        = 'Restricted Warehouse to In-House',
       C901_LAST_UPDATED_BY   = 3178209,
       C901_LAST_UPDATED_DATE = CURRENT_DATE
 WHERE C901_CODE_ID       = '26241330'; 
 
 UPDATE T901_CODE_LOOKUP
   SET c901_code_nm        = 'Restricted Warehouse to PD expense',
       C901_LAST_UPDATED_BY   = 3178209,
       C901_LAST_UPDATED_DATE = CURRENT_DATE
 WHERE C901_CODE_ID       = '26241329'; 

