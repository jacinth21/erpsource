INSERT INTO T906_RULES  (
    C906_RULE_SEQ_ID,
    C906_RULE_ID,
    C906_RULE_DESC,
    C906_RULE_VALUE,
    C906_CREATED_DATE,
    C906_RULE_GRP_ID,
    C906_CREATED_BY,
    C906_LAST_UPDATED_DATE,
    C906_LAST_UPDATED_BY,
    C901_RULE_TYPE,
    C906_ACTIVE_FL,
    C906_VOID_FL,
    C1900_COMPANY_ID,
    C906_LAST_UPDATED_DATE_UTC 
)
VALUES  ( 
    S906_RULE.NEXTVAL,
    'ITEM_REQ_FAV_FL',
    'Favourite Flag',
    'Y',
    CURRENT_DATE,
    'CONTAB',
    '303510',
    NULL,
    NULL,
    NULL,
    Null,
    Null,
    '1026',
    Null  
 );

INSERT INTO T906_Rules (
    C906_Rule_Seq_Id,
    C906_Rule_Id,
    c906_Rule_Desc,
    C906_Rule_Value,
    C906_Created_Date,
    C906_Rule_Grp_Id,
    C906_Created_By,
    C906_Active_Fl,
    C1900_Company_Id
)
Values  (
    S906_Rule.Nextval,
    'CASE_APP_ACCESS',
    Null,
    'btn-case',
    Current_Date,
    'MOBILE_APP_ACCESS',
    '303510',
    'Y',
    1026
  );

Insert Into T906_Rules  (
    C906_Rule_Seq_Id, C906_Rule_Id,
    C906_Rule_Desc,
    C906_Rule_Value,
    C906_Created_Date,
    C906_Rule_Grp_Id,
    C906_Created_By,
    C906_Last_Updated_Date,
    C906_Last_Updated_By,
    C901_Rule_Type,
    C906_Active_Fl,
    C906_Void_Fl,
    C1900_Company_Id,
    C906_Last_Updated_Date_Utc 
)
Values  (
    S906_Rule.Nextval,
    'EXC_KIT_SET',
    'Favourite Flag',
    'Y',
    Current_Date,
    'CONTAB',
    '303510',
    Null,
    Null,
    Null,
    Null,
    Null,
    '1026',
    Null  
);
