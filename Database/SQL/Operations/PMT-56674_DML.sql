INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(26240887,'Reload Consignment','LOANOP','1','','RLDCONS','','2598779',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(26240888,'Reload Sterile Consignment','CNCLT','1','','RLDCNS','','2598779',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(109820,'Reload Lot Qty','RLDCNS','1','1','','','2598779',CURRENT_DATE);


--Common Cancel procedure call for 26240887(Reload Consignment
INSERT
INTO T906_RULES
  (
    C906_RULE_SEQ_ID,
    C906_RULE_ID,
    C906_RULE_DESC,
    C906_RULE_VALUE,
    C906_CREATED_DATE,
    C906_RULE_GRP_ID,
    C906_CREATED_BY,
    C1900_COMPANY_ID
  )
  VALUES
  (
    S906_RULE.NEXTVAL,
    '26240888',
    'Reload Consignment',
    'gm_pkg_op_loaner_lot_process.gm_sav_loaner_lot_track',
    CURRENT_DATE,
    'CMNCNCL',
    '706328',
    NULL
  );