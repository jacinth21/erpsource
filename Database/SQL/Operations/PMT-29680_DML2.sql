--- Insert a Warehouse dropdown value for Sold Qty 
--- PMT-29680
DELETE FROM t901_code_lookup where c901_code_id='108000';

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108000,'Sold Qty','LOTSLD','1','1','','','706328',CURRENT_DATE);