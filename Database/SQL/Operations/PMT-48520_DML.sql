--Synonynm for new package files
--PMT-48520 - Remove Insert Part Number in Shipping Job
create or replace public synonym gm_pkg_get_inserts_exception for GLOBUS_APP.gm_pkg_get_inserts_exception;