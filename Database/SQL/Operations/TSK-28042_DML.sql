--Enable Lot scanning for Netherland in process check screen
INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,c1900_company_id,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY)
VALUES (S906_RULE.NEXTVAL,'1010','New Process Check screen for Lot Tracking flag for Netherland','Y','1010',CURRENT_DATE,'LOT_TRK','2765071');