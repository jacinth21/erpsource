-- PMT-40241 - Mobile Device - Tissue parts shipping address changes in order confirmation
-- Adding new address type "Hospital" in sales rep setup screen
INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'ADDTP','','','706328',CURRENT_DATE); 


INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26240675,'Hospital','ADDTP','1','','','','706328',CURRENT_DATE);

--Datacorrection
UPDATE t501a_order_attribute
SET c501a_void_fl               ='Y',
  c501a_last_updated_by         =706328,
  c501a_last_updated_date       =sysdate
WHERE c501a_order_attribute_id IN
  (SELECT c501a_order_attribute_id
  FROM t501_order t501,
    t501a_order_attribute t501a
  WHERE t501.c901_order_type   =2525
  AND t501.c501_void_fl       IS NULL
  AND t501.c1900_company_id    =1000
  AND t501.c501_created_date  >= sysdate-10
  AND t501.c501_status_fl      = 0
  AND t501a.c901_attribute_type='11241'
  AND t501a.c501_order_id      = t501.c501_order_id
  AND t501a.c501a_void_fl     IS NULL
  );
