update t207_set_master set c207_tissue_fl='Y' where c207_set_id in (
        select t207.c207_set_id from t207_set_master t207, t208_set_details t208 where  t207.c207_set_id=t208.c207_set_id and t207.c202_project_id in (
        SELECT  c202_project_id       FROM t205_part_number t205, t906_rules t906
      WHERE t205.c202_project_id           = t906.c906_rule_id
        AND t906.c906_rule_grp_id     = 'EXVND'
        AND t906.c906_void_fl        IS NULL
        AND t205.c205_product_material=100845 --tissue
        AND NVL(t205.c205_product_family,-999) <> 4053 --Demo-Material
        AND t205.c205_active_fl='Y' group by c202_project_id) and c901_status_id=20367 and c207_void_fl is null and c207_type<> 4078
        and t208.c205_part_number_id in (
          SELECT  c205_part_number_id       FROM t205_part_number t205, t906_rules t906
      WHERE t205.c202_project_id           = t906.c906_rule_id
        AND t906.c906_rule_grp_id     = 'EXVND'
        AND t906.c906_void_fl        IS NULL
        AND t205.c205_product_material=100845 --tissue
        AND NVL(t205.c205_product_family,-999) <> 4053 --Demo-Material
        AND t205.c205_active_fl='Y' group by c205_part_number_id
        ) and t208.c208_inset_fl='Y' and t208.c208_void_fl is null group by t207.c207_set_id);

INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY) VALUES (S906_RULE.NEXTVAL,'1000','Tissue Validation','Y',CURRENT_DATE,'TISSUE_VALIDATION','706328');
