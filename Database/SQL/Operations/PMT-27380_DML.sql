insert into t100_error_msg (c100_error_msg_id ,C100_ERROR_CODE, c100_ora_error_code, c100_error_msg, c901_language_id, c100_error_void_fl, c100_created_by, c100_created_date, c100_last_updated_by, c100_last_updated_date, c100_last_updated_date_utc )
values (450 ,'417', '-20999', 'The following Parts are missing ICT Pricing: [1]', 103097, null, null, sysdate, null, null, null );

DELETE from T906_Rules where C906_Rule_Grp_Id = 'ICSPRICECHECK' and C906_Rule_Id IN ('427');
INSERT into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,C906_Last_Updated_Date,C906_Last_Updated_By,C901_Rule_Type,C906_Active_Fl,C906_Void_Fl,C1900_Company_Id,C906_Last_Updated_Date_Utc)
values (S906_Rule.Nextval,'427',null,'Y',Sysdate,'ICSPRICECHECK','706328',null,null,null,'Y',null,1000,null);

INSERT into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,C906_Last_Updated_Date,C906_Last_Updated_By,C901_Rule_Type,C906_Active_Fl,C906_Void_Fl,C1900_Company_Id,C906_Last_Updated_Date_Utc)
values (S906_Rule.Nextval,'OUSDIST_PRICE',null,'vramanathan@globusmedical.com,aprasath@globusmedical.com',Sysdate,'EMAIL','706328',null,null,null,'Y',null,1000,null);

INSERT into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,C906_Last_Updated_Date,C906_Last_Updated_By,C901_Rule_Type,C906_Active_Fl,C906_Void_Fl,C1900_Company_Id,C906_Last_Updated_Date_Utc)
values (S906_Rule.Nextval,'OUSDIST_SUBJECT',null,'OUS Distributor Price not available for the order ',Sysdate,'EMAILSUBJECT','706328',null,null,null,'Y',null,1000,null);