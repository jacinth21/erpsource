-- Create new rule 
  INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID,C906_RULE_DESC, C906_RULE_GRP_ID, C906_RULE_VALUE, C901_RULE_TYPE, C1900_COMPANY_ID, C906_CREATED_DATE, 
       C906_CREATED_BY
    )
    VALUES
    (
        s906_rule.nextval, 'TRANTYPE','Lot Track - Rollback loaner changes','SKIPLOTTRACK','Y','50183','1000', CURRENT_DATE,
        '303223'
    ) ;