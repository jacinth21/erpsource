INSERT INTO t1520_function_list 
(C1520_FUNCTION_ID,
C1520_FUNCTION_NM,
C1520_FUNCTION_DESC,
C901_TYPE,
C1520_CREATED_BY,
C1520_CREATED_DATE,
C1520_REQUEST_URI,
C901_DEPARTMENT_ID,
C901_LEFT_MENU_TYPE) 
VALUES 
('CHANGE_LOT_NUMBER',
'Change Lot Number',
'This is used to change old lot number with new lot number',
92262,
'303013',
current_date,
'/gmChangeLotNumber.do?method=changeLotNumber', 
2009,101921);


INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,
C906_CREATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID) VALUES 
(S906_RULE.nextval,'INHT_LOT','In-house Lot Change','gm_pkg_op_change_lot.gm_sav_inht_chng_lot',CURRENT_DATE,'CHNGLOT','303223',null,null,null,null);

INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,
C906_CREATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID) VALUES 
(S906_RULE.nextval,'CNSGN_LOT','Consignment Lot Change','gm_pkg_op_change_lot.gm_sav_cn_chng_lot',CURRENT_DATE,'CHNGLOT','303223',null,null,null,null);

INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,
C906_CREATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID) VALUES 
(S906_RULE.nextval,'ORD_LOT',' Order Lot Change','gm_pkg_op_change_lot.gm_sav_ord_chng_lot',CURRENT_DATE,'CHNGLOT','303223',null,null,null,null);


INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108820,'Change Lot','CHNGLT','0','1','','','2598779',CURRENT_DATE);


CREATE OR REPLACE public synonym gm_pkg_op_change_lot for GLOBUS_APP.gm_pkg_op_change_lot;