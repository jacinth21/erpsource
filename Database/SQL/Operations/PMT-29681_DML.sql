--Insert scripts for Lot Error Transaction type into code lookup table


INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'LOTTRN','Lot Error Transaction Type','','706328',CURRENT_DATE);


INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(107980,'Order','LOTTRN','1','1','','','706328',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(107981,'Return','LOTTRN','1','2','','','706328',CURRENT_DATE);

--Insert and update script for Lot error Resolution type into code lookup table

INSERT INTO t901b_code_group_master (C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE) VALUES (s901b_code_group_master.NEXTVAL,'RESTYP','Viacell Error Resoultion Type','','706328',CURRENT_DATE);  
INSERT INTO t901_code_lookup (c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date) VALUES (107962,'Mark as resolved','RESTYP','1','3','','','706328',CURRENT_DATE); 
INSERT INTO t901_code_lookup (c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date) VALUES (107960,'Proceed with same lot','RESTYP','1','1','','','706328',CURRENT_DATE); 
INSERT INTO t901_code_lookup (c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date) VALUES (107961,'Proceed with new lot','RESTYP','1','2','','','706328',CURRENT_DATE);
UPDATE t901_code_lookup SET c901_void_fl=NULL,c901_code_nm='Mark as resolved',c901_active_fl='1',c901_last_updated_by=706328,c901_last_updated_date=SYSDATE  WHERE c901_code_id='107962';
UPDATE t901_code_lookup SET c901_void_fl=NULL,c901_code_nm='Proceed with same lot',c901_active_fl='1',c901_last_updated_by=706328,c901_last_updated_date=SYSDATE  WHERE c901_code_id='107960';
UPDATE t901_code_lookup SET c901_void_fl=NULL,c901_code_nm='Proceed with new lot',c901_active_fl='1',c901_last_updated_by=706328,c901_last_updated_date=SYSDATE  WHERE c901_code_id='107961';
UPDATE t901b_code_group_master SET c901b_void_fl=NULL,c901b_last_updated_by=706328,c901b_last_updated_date=SYSDATE  WHERE c901B_code_grp='RESTYP';
  

--Insert script for Lot Error Type and error message into code lookup table
INSERT INTO t901b_code_group_master (C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE) VALUES (s901b_code_group_master.NEXTVAL,'ERRTYP','Viacell Error Type','','706328',CURRENT_DATE);
INSERT INTO t901_code_lookup (c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date) VALUES (107972,'Invalid Lot','ERRTYP','1','1','','','706328',CURRENT_DATE);
INSERT INTO t901_code_lookup (c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date) VALUES (107973,'Billed Lot','ERRTYP','1','2','','','706328',CURRENT_DATE);
INSERT INTO t901_code_lookup (c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date) VALUES (107974,'Invalid Inventory','ERRTYP','1','3','','','706328',CURRENT_DATE);
INSERT INTO t901_code_lookup (c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date) VALUES (107975,'Field Sales Mismatch','ERRTYP','1','4','','','706328',CURRENT_DATE);


--Synonym
CREATE OR REPLACE PUBLIC SYNONYM t5063_lot_error_info FOR GLOBUS_APP.t5063_lot_error_info;  

CREATE OR REPLACE PUBLIC SYNONYM gm_pkg_op_lot_error_rpt FOR GLOBUS_APP.gm_pkg_op_lot_error_rpt; 

CREATE OR REPLACE PUBLIC SYNONYM gm_pkg_op_lot_validation FOR GLOBUS_APP.gm_pkg_op_lot_validation; 

CREATE OR REPLACE PUBLIC SYNONYM gm_pkg_op_lot_error_txn FOR GLOBUS_APP.gm_pkg_op_lot_error_txn; 