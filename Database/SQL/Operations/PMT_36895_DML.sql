
  insert into t1520_function_list(c1520_function_id, c1520_function_nm, c1520_function_desc,c901_type, c1520_created_date, c1520_created_by) 
VALUES ('ESCALATION_ACCESS','ESCALATION_ACCESS','Users in this security event will restrict access escalation flag ',92267,sysdate,'303012');

  insert into t1520_function_list(c1520_function_id, c1520_function_nm, c1520_function_desc, c901_type, c1520_created_date, c1520_created_by) 
VALUES ('ESC_REMOVE_ACCESS','ESC_REMOVE_ACCESS','Users in this security access for escalation flag ',92267,sysdate,'303012');

INSERT INTO T1500_GROUP 
(C1500_GROUP_ID , C1500_GROUP_NM,  C1500_GROUP_DESC  ,C1500_VOID_FL   ,  C1500_CREATED_BY, C1500_CREATED_DATE ,C901_GROUP_TYPE  )
VALUES 
('ESCALATION_ACCESS','ESCALATION_ACCESS','Users in this security event will restrict access escalation flag ',null,303012,CURRENT_DATE,92264);

INSERT INTO T1500_GROUP 
(C1500_GROUP_ID , C1500_GROUP_NM,  C1500_GROUP_DESC  ,C1500_VOID_FL   ,  C1500_CREATED_BY, C1500_CREATED_DATE ,C901_GROUP_TYPE  )
VALUES 
('ESC_REMOVE_ACCESS','ESC_REMOVE_ACCESS','Users in this security access for escalation flag ',null,303012,CURRENT_DATE,92264);

 
   UPDATE T1520_FUNCTION_LIST
SET c1520_request_uri    ='/gmOprLoanerReqRpt.do?method=loadLoanerRequest',
  c1520_last_updated_by  ='303013',
  c1520_last_updated_date=sysdate
WHERE c1520_function_id  ='6212400'
AND c1520_function_nm    ='Loaners (Process Request)';