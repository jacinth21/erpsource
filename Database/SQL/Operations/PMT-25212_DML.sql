delete from  t906_rules where c906_rule_grp_id in ('LOT_EXPIRY','LOTEXT')  ;
insert into t906_rules (c906_rule_seq_id, c906_rule_id ,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) values 
(s906_rule.nextval,'PART_NUM_CHK','The entered Part number doesnt Exist','LOT_EXPIRY',current_date,'2277820');
insert into t906_rules (c906_rule_seq_id, c906_rule_id ,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) values 
(s906_rule.nextval,'LOT_NUM_CHK','The entered Lot number doesnt Exist','LOT_EXPIRY',current_date,'2277820');
insert into t906_rules (c906_rule_seq_id, c906_rule_id ,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) values 
(s906_rule.NEXTVAL,'LOTEXT','107365','LOTEXT',CURRENT_DATE,'2277820');
insert into t906_rules (c906_rule_seq_id, c906_rule_id ,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) values 
(s906_rule.nextval,'LOT_EMAIL_SUB','Allograft Expiry Extension failed','LOT_EXPIRY',current_date,'2277820');

insert into t906_rules (c906_rule_seq_id, c906_rule_id ,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) values 
(s906_rule.nextval,'LOT_EMAIL_SUB_SUCCESS','Allograft Expiry Extension Completed','LOT_EXPIRY',current_date,'2277820');

insert into t906_rules (c906_rule_seq_id, c906_rule_id ,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) values 
(s906_rule.nextval,'LOT_EMAIL_MSG','Expiration extended for all the Allograft uploaded','LOT_EXPIRY',current_date,'2277820');

insert into t906_rules (c906_rule_seq_id, c906_rule_id ,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) values 
(s906_rule.nextval,'LOT_EMAIL_NULL_VAL','There is no data entered for one of the column in the Allograft expiration excel upload.','LOT_EXPIRY',current_date,'2277820');

CREATE OR REPLACE PUBLIC SYNONYM gm_pkg_op_lot_expiry FOR globus_app.gm_pkg_op_lot_expiry;
