--PC-1262 - Ack order Req date changes rule value
INSERT INTO t906_rules (
    c906_rule_seq_id, 
    c906_rule_id,c906_rule_desc, 
    c906_rule_value,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c906_active_fl
) 
VALUES (
    s906_rule.NEXTVAL,
    'ACK_ORDER_EMAIL_TO',
    'Rule group for Ack Order',
    'sales@bonebank.com,amuzumdar@bonebank.com,gpalani@globusmedical.com',
    current_date,
    'ACK_ORDER_DATE_VAL',
    '2277820',
    'Y'
);


INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_desc,
    c906_rule_value,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c906_active_fl
) 
VALUES (
    s906_rule.NEXTVAL,
    'ACK_ORDER_EMAIL_SUB',
    'Rule group for Ack Order',
    'Ack Order - Required Date is Lessthan 42 Business Days',
    current_date,
    'ACK_ORDER_DATE_VAL',
    '2277820',
    'Y'
);

INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_desc,
    c906_rule_value,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c906_active_fl
) VALUES (
    s906_rule.NEXTVAL,
    'ACK_ORDER_EMAIL_MSG',
    'Rule group for Ack Order',
    'Ack Order - Required Date is Lessthan 42 Business Days',
    current_date,
    'ACK_ORDER_DATE_VAL',
    '2277820',
    'Y'
);
INSERT INTO t906_rules (
    c906_rule_seq_id, 
    c906_rule_id,c906_rule_desc, 
    c906_rule_value,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c906_active_fl
) 
VALUES (
    s906_rule.NEXTVAL,
    '1001',
    'Rule group for Ack Order',
    'Y',
    current_date,
    'ACK_ORDER_DATE_VAL',
    '2277820',
    'Y'
);      


INSERT INTO t906_rules (
    c906_rule_seq_id, 
    c906_rule_id,c906_rule_desc, 
    c906_rule_value,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c906_active_fl
) 
VALUES (
    s906_rule.NEXTVAL,
    'ACK_BUSINESS_DAYS',
    'Rule group for Ack Order',
    '41',
    current_date,
    'ACK_ORDER_DATE_VAL',
    '2277820',
    'Y'
);      





