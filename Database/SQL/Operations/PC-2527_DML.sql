--Create left link for Safety stock upload
INSERT INTO t1520_function_list 
(C1520_FUNCTION_ID,
C1520_FUNCTION_NM,
C1520_FUNCTION_DESC,
C901_TYPE,
C1520_CREATED_BY,
C1520_CREATED_DATE,
C1520_REQUEST_URI,
C1520_REQUEST_STROPT,
C901_DEPARTMENT_ID,
C901_LEFT_MENU_TYPE) 
VALUES 
('SAFETY_STOCK_UPLOAD',
'Safety Stock Upload',
'This screen is used to upload safety stock data',
92262,
'2765071',
current_date,
'/gmSafetyStockUpload.do?method=loadTTPbyVendorStockBulkUpload',
'',
2009,101921);

--create synonynm for new package
create or replace public synonym gm_pkg_oppr_ld_safety_stock_txn FOR GLOBUS_DM_OPERATION.gm_pkg_oppr_ld_safety_stock_txn;