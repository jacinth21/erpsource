create or replace public synonym gm_pkg_common_history for GLOBUS_APP.gm_pkg_common_history;

INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_value,
    c906_rule_desc,
    c906_rule_grp_id,
    c906_created_date,
    c906_created_by
) VALUES (
    s906_rule.NEXTVAL,
    'SOP_ACTION',
    'gm_pkg_sop_setoutofpolicy_rpt.gm_fch_log_history_dtls',
    'Set Out of Policy Status Log Details',
    'CMNHL',
    current_date,
    '303012'
);