INSERT
INTO t1520_function_list
  (
    c1520_function_id,
    c1520_function_nm,
    c1520_function_desc ,
    c1520_request_uri,
    c1520_inherited_from_id,
    c901_type,
    c1520_void_fl,
    c1520_created_by,
    c1520_created_date,
    c1520_last_updated_by,
    c1520_last_updated_date,
    c1520_request_stropt ,
    c1520_seq_no,
    c901_department_id,
    c901_left_menu_type,
    c1520_last_updated_date_utc
  )
  VALUES
  (
    'LOANERSET_ACCEPTANCE',
    'RFID Overall Received Tags',
    'This screen loads the report for Loaner Set Acceptance Status ',
    '\gmLoanerSetAcceptanceRpt.do?method=loadLoanerSetAcceptanceDetails',
    NULL,
    92262 ,
    NULL,
    '303149',
    sysdate,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    sysdate
  ) ;
  
  
  CREATE OR REPLACE  public synonym gm_pkg_op_rfid_process FOR GLOBUS_APP.gm_pkg_op_rfid_process;