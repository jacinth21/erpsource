/*TO Enable/Disable Left Link Access - PMT#54256 - Add/Edit Lot Track Details */
 
 INSERT INTO t1520_function_list 
 			 (C1520_FUNCTION_ID, 
 			  C1520_FUNCTION_NM,
 			  C1520_FUNCTION_DESC,
 			  C901_TYPE,
 			  C1520_CREATED_BY,
			  C1520_CREATED_DATE,
			  C1520_REQUEST_URI,
			  C1520_REQUEST_STROPT,
			  C901_DEPARTMENT_ID,
			  C901_LEFT_MENU_TYPE) 
      VALUES ('ADDOREDITLOTDETAILS',
      		  'Add/Edit Lot Track Details',
      		  'This reports is used Add or Edit the Lot Track Details',
			  92262,
			  '303013',
			  CURRENT_DATE,
			  '/gmLotTrackNonTissueRpt.do?method=addOrEditLotTrackDetails',
			  NULL,
			  2014,
			  101920);

			  
-- PMT#54256 - Security event for Lot Tracking Details 
-- To create the Security event to Submit Button  and Edit Icon
INSERT
INTO T1520_FUNCTION_LIST
    (C1520_FUNCTION_ID, 
     C1520_FUNCTION_NM, 
     C1520_FUNCTION_DESC, 
     C1520_INHERITED_FROM_ID, 
     C901_TYPE, 
     C1520_VOID_FL, 
     C1520_CREATED_BY, 
     C1520_CREATED_DATE, 
     C1520_LAST_UPDATED_BY, 
     C1520_LAST_UPDATED_DATE, 
     C1520_REQUEST_URI, 
     C1520_REQUEST_STROPT, 
     C1520_SEQ_NO, 
     C901_DEPARTMENT_ID, 
     C901_LEFT_MENU_TYPE, 
     C1520_LAST_UPDATED_DATE_UTC )
VALUES
    ('EDIT_LOT_DATA',
     'EDIT_LOT_DATA',
     'Users in this security event will have the access to modify the Lot Track Details',
      null, 
      92267,
      null,
      '303510',
      CURRENT_DATE,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      CURRENT_DATE) ;
      