-- Script to create group record in T1500_Group table

insert into t1500_group(c1500_group_id,c1500_group_nm,c1500_group_desc,c1500_void_fl,c1500_created_by,c1500_created_date,c1500_last_updated_by,c1500_last_updated_date,c901_group_type,c1500_last_updated_date_utc) 
VALUES ('IDN_ACCESS','IDN_ACCESS','DO APP ACCESS',null,'2163127',sysdate,null,null,92264,sysdate);


-- Script to Create record in T1520_FUNCTION_LIST

insert into t1520_function_list(c1520_function_id, c1520_function_nm, c1520_function_desc, c1520_created_date, c1520_created_by) 
VALUES ('IDN_ACCESS','IDN_ACCESS','Users in this security event will restrict access over Price approval ',sysdate,'2163127');

--Script to create record in T1530_ACCESS 

insert into t1530_access (c1530_access_id,c1500_group_id,c1520_function_id,c101_party_id,c1530_update_access_fl,c1530_read_access_fl,c1530_void_access_fl,c1530_void_fl,c1530_created_by,c1530_created_date,c1530_last_updated_by,c1530_last_updated_date,c1520_parent_function_id,c1530_function_nm,c1530_seq_no,c1530_last_updated_date_utc)
VALUES (s1530_access.nextval, 'IDN_ACCESS', 'IDN_ACCESS',null,'Y','Y','Y',null,'2163127',SYSDATE,null,null,null,null,null,SYSDATE);

------- Create rule for Set of IDN


INSERT Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,C906_Last_Updated_Date,C906_Last_Updated_By,C901_Rule_Type,C906_Active_Fl,C906_Void_Fl,C1900_Company_Id,C906_Last_Updated_Date_Utc) Values
(S906_Rule.Nextval,'1920770',null,'3067',Sysdate,'PRTIDN','839135',null,null,null,'Y',null,null,null);   --- Ascension


INSERT Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,C906_Last_Updated_Date,C906_Last_Updated_By,C901_Rule_Type,C906_Active_Fl,C906_Void_Fl,C1900_Company_Id,C906_Last_Updated_Date_Utc) Values
(S906_Rule.Nextval,'1920770',null,'3212',Sysdate,'PRTIDN','839135',null,null,null,'Y',null,null,null);  ---  Franciscan Alliance

CREATE OR REPLACE PUBLIC SYNONYM get_idn_no FOR GLOBUS_APP.get_idn_no;

