INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(26241169,'Vendor Setup Active-InActive','GPLOG','0','','','','2765071',CURRENT_DATE);

--synonym for new trigger
create or replace public synonym TRG_T301_VENDOR_INACTIVE for GLOBUS_APP.TRG_T301_VENDOR_INACTIVE;

--insert new record for audit trail id to store vendor active/inactive history 
insert into t940_audit_trail (C940_AUDIT_TRAIL_ID,C940_TABLE_NAME,C940_COLUMN_NAME,C940_CREATED_BY,C940_CREATED_DATE)values (4000765,'t301_vendor','c301_active_fl',2765071,current_date);