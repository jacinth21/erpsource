--PC-4551-Skip lot validation for specific stelkast parts in rework po process 
--Delete the Rule entry and insert it again to avoid duplicate entries
DELETE FROM t906_rules where c906_rule_id ='2957-2' AND c906_rule_grp_id ='SKIP_REWORK_VALIDATE';
DELETE FROM t906_rules where c906_rule_id ='SC1401-54L' AND c906_rule_grp_id ='SKIP_REWORK_VALIDATE';
DELETE FROM t906_rules where c906_rule_id ='SC1439-0800' AND c906_rule_grp_id ='SKIP_REWORK_VALIDATE';
DELETE FROM t906_rules where c906_rule_id ='SC1439-0700' AND c906_rule_grp_id ='SKIP_REWORK_VALIDATE';

INSERT INTO t906_rules(
        c906_rule_seq_id
        ,c906_rule_id
        ,c906_rule_value
        ,c906_rule_grp_id
        ,c906_created_date
        ,c906_created_by)
VALUES(
        s906_rule.nextval,
        'SC1401-54L',
        'Skip lot validation for specific stelkast parts in rework po process',
        'SKIP_REWORK_VALIDATE',
        CURRENT_DATE,
        '706328');
        
 INSERT INTO t906_rules(
        c906_rule_seq_id
        ,c906_rule_id
        ,c906_rule_value
        ,c906_rule_grp_id
        ,c906_created_date
        ,c906_created_by)
VALUES(
        s906_rule.nextval,
        'SC1439-0800',
        'Skip lot validation for specific stelkast parts in rework po process',
        'SKIP_REWORK_VALIDATE',
        CURRENT_DATE,
        '706328');
        
 INSERT INTO t906_rules(
        c906_rule_seq_id
        ,c906_rule_id
        ,c906_rule_value
        ,c906_rule_grp_id
        ,c906_created_date
        ,c906_created_by)
VALUES(
        s906_rule.nextval,
        'SC1439-0700',
        'Skip lot validation for specific stelkast parts in rework po process',
        'SKIP_REWORK_VALIDATE',
        CURRENT_DATE,
        '706328');