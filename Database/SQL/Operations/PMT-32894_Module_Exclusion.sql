--JAPAN
UPDATE T906_RULES
SET c906_rule_value      = 'btn-catalog,btn-collateral,btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-NPI-info,div-do-record-tags',
  c906_last_updated_by   = '2423914',
  C906_LAST_UPDATED_DATE = SYSDATE
WHERE c906_rule_id       = '1025'
AND c906_rule_grp_id     ='MODULEACCESS';

-- Exclude JAPAN
UPDATE T906_RULES
SET c906_rule_value      = 'btn-catalog,btn-collateral,btn-case,btn-inv,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags',
  c906_last_updated_by   = '2423914',
  C906_LAST_UPDATED_DATE = SYSDATE
WHERE c906_rule_id NOT  IN ('1025','1022','1026')
AND c906_rule_grp_id     ='MODULEACCESS';
--US 
INSERT INTO T906_RULES (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,C906_Last_Updated_Date,C906_Last_Updated_By,C901_Rule_Type,C906_Active_Fl,C906_Void_Fl,C1900_Company_Id,C906_Last_Updated_Date_Utc)
VALUES (S906_Rule.Nextval,'1000','Ipad Module Access Exclusion list For US','div-do-record-tags',Sysdate,'MODULEACCESS','3178209',Null,Null,Null,Null,Null,1000,Null);