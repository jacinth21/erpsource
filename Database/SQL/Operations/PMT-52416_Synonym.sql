-- Synonyms
CREATE OR REPLACE  public synonym gm_pkg_op_wip_set_loc FOR GLOBUS_APP.gm_pkg_op_wip_set_loc;
CREATE OR REPLACE  public synonym TRG_T504_WIP_Set_Loc_Update FOR GLOBUS_APP.TRG_T504_WIP_Set_Loc_Update;