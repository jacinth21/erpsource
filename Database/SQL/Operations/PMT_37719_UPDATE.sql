--PMT-37719 Pre Staging 
UPDATE t901_code_lookup
set c901_code_nm = 'Pre Staging' , c901_active_fl = '1', c901_last_updated_by = '2193221' , c901_last_updated_date = SYSDATE
where c901_code_id = '104523';

--Staging
UPDATE t901_code_lookup
set c901_code_nm = 'Staging' , c901_active_fl = '1', c901_last_updated_by = '2193221' , c901_last_updated_date = SYSDATE
where c901_code_id = '104524';
