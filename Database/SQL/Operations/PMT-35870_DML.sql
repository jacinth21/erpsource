  -- PMT-35870 - Mapping Vendor Portal User to Vendor
  -- Insert the new Code id for Vendor Portal Map
INSERT
  INTO t901_code_lookup
  (
    c901_code_id,
    c901_code_nm,
    c901_code_grp,
    c901_active_fl,
    c901_code_seq_no,
    c902_code_nm_alt,
    c901_control_type,
    c901_created_by,
    c901_created_date
  )
  VALUES
  (
    26240605,
    'Vendor Portal Map',
    'PTTYP',
    '1',
    '',
    '',
    '',
    '2765071',
    CURRENT_DATE
  );
        
-- create synonym for new package

CREATE OR REPLACE PUBLIC SYNONYM gm_pkg_vendor_portal_map FOR GLOBUS_APP.gm_pkg_vendor_portal_map; 

