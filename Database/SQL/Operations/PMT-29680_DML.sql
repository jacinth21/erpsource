-- --Data migration
INSERT
INTO t205m_part_label_parameter
 (SELECT s205m_part_label_parameter.NEXTVAL,
     c205_part_number_id,
     c205d_general_spec,
     c205d_label_size ,
     c205d_size_format,
     c205d_box_label,
     c205d_patient_label,
     c205d_package_label,
     c205d_sample,
     c205d_material_spec,
     c205d_part_drawing,
     c205d_processing_spec_type,
     c205d_contract_process_client,
     null,
     null,
     null,
     1001,
     3001
     FROM
     (SELECT t205d.c205_part_number_id,
       t205d.c205d_general_spec,
       t205d.c205d_label_size ,
       t205d.c205d_size_format,
       t205d.c205d_box_label,
       t205d.c205d_patient_label,
       t205d.c205d_package_label,
       t205d.c205d_sample,
       t205d.c205d_material_spec,
       t205d.c205d_part_drawing,
       t205d.c205d_processing_spec_type,
       t205d.c205d_contract_process_client          
     FROM t205_part_number t205,
       (SELECT c205_part_number_id,
         -- Label Parameter
         MAX (DECODE (c901_attribute_type, 103737, c205d_attribute_value)) c205d_general_spec,
         MAX (DECODE (c901_attribute_type, 103738, c205d_attribute_value)) c205d_label_size,
         MAX (DECODE (c901_attribute_type, 103739, c205d_attribute_value)) c205d_size_format,
         MAX (DECODE (c901_attribute_type, 103740, c205d_attribute_value)) c205d_box_label,
         MAX (DECODE (c901_attribute_type, 103741, c205d_attribute_value)) c205d_patient_label,
         MAX (DECODE (c901_attribute_type, 103742, c205d_attribute_value)) c205d_package_label,
         MAX (DECODE (c901_attribute_type, 103745, c205d_attribute_value)) c205d_sample,
         MAX (DECODE (c901_attribute_type, 105880, c205d_attribute_value)) c205d_material_spec,
         MAX (DECODE (c901_attribute_type, 105881, c205d_attribute_value)) c205d_part_drawing,
         MAX (DECODE (c901_attribute_type, 103729, c205d_attribute_value)) c205d_processing_spec_type,
         MAX (DECODE (c901_attribute_type, 103731, c205d_attribute_value)) c205d_contract_process_client
       FROM MV205G_PART_LABEL_PARAMETER
       WHERE C205d_VOID_FL IS NULL
       GROUP BY c205_part_number_id
       )t205d
     WHERE t205.c205_part_number_id = t205d.c205_part_number_id
     )
   WHERE c205d_general_spec        IS NOT NULL
   OR c205d_label_size             IS NOT NULL
   OR c205d_size_format            IS NOT NULL
   OR c205d_box_label              IS NOT NULL
   OR c205d_patient_label          IS NOT NULL
   OR c205d_package_label          IS NOT NULL
   OR c205d_sample                 IS NOT NULL
   OR c205d_material_spec          IS NOT NULL
   OR c205d_part_drawing           IS NOT NULL
   OR c205d_part_drawing           IS NOT NULL
   OR c205d_processing_spec_type   IS NOT NULL
   OR c205d_contract_process_client IS NOT NULL
 ) ;
