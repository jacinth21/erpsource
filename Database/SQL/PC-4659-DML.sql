---
DELETE FROM t906_rules where c906_rule_grp_id ='ACC_DIV_EGPS' and c906_rule_id ='100800';

INSERT INTO t906_rules 
				   	( c906_rule_seq_id, c906_rule_id, c906_rule_value
				   	, c906_rule_grp_id, c906_created_date, c906_created_by,c1900_company_id
				) 
				VALUES (s906_rule.NEXTVAL, '100800', 'Y'
					, 'ACC_DIV_EGPS', CURRENT_DATE, '30301', '1000'
					);

					
					
--attribute type
DELETE FROM t901_code_lookup where c901_code_id ='26241315';

INSERT 
  INTO t901_code_lookup 
      (c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES (26241315,'Surgery EGPS Usage','SURINF','1','','','','2598779',CURRENT_DATE);

--
UPDATE t906_rules SET c906_rule_value='N' where c906_rule_grp_id ='ACC_DIV_EGPS' and c906_rule_id ='100800';