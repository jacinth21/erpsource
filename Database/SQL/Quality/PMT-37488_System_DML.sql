
-- create synonym for new package gm_pkg_qa_system_publish_trans
create or replace public synonym gm_pkg_qa_system_publish_trans for GLOBUS_APP.gm_pkg_qa_system_publish_trans;
-- create synonym for new package gm_pkg_qa_system_publish_rpt
create or replace public synonym gm_pkg_qa_system_publish_rpt for GLOBUS_APP.gm_pkg_qa_system_publish_rpt;
--------------------------------------------------------- System Release and Un Release ------------------------------------------------------------------- 
-- select * from T901_code_lookup where c901_code_grp in ('MAPTYP');

--------------------------------------------------------- Code Lookup for System Release Regions Name List -------------------------------------------------------------------  
 
INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'QUAREG','Quality System Release Regions','','2423914',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108640,'US','QUAREG','1','1','','','2423914',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108641,'APAC','QUAREG','1','2','','','2423914',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108642,'EU','QUAREG','1','3','','','2423914',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108643,'Japan','QUAREG','1','4','','','2423914',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108644,'-','QUAREG','0','5','','','2423914',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108645,'-','QUAREG','0','6','','','2423914',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108646,'-','QUAREG','0','7','','','2423914',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108647,'-','QUAREG','0','8','','','2423914',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108648,'-','QUAREG','0','9','','','2423914',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108649,'-','QUAREG','0','10','','','2423914',CURRENT_DATE);




--------------------------------------------------------- SYSTEM PUBLISHED and UNPUBLISHED ------------------------------------------------------------------- 
 
INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'SYSPUB','Quality System Publish and UnPublished','','2423914',CURRENT_DATE);
 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108660,'Published','SYSPUB','1','1','','','2423914',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108661,'Unpublished','SYSPUB','1','2','','','2423914',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108662,'-','SYSPUB','0','3','','','2423914',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108663,'-','SYSPUB','0','4','','','2423914',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108664,'-','SYSPUB','0','5','','','2423914',CURRENT_DATE);



--------------------------------------------------------- SYSTEM Release Log Table Entry ------------------------------------------------------------------- 

INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'GPLOG','Quality System/Set Release to Regions Log','','2423914',CURRENT_DATE);
 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108650,'Quality System/Set Release to Regions Log','GPLOG','1','102','','','2423914',CURRENT_DATE); 


------New code ids for region-----------

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108720,'US','COMREG','1','1','','','2598779',CURRENT_DATE);
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108721,'Japan','COMREG','1','2','','','2598779',CURRENT_DATE);
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108722,'APAC','COMREG','1','3','','','2598779',CURRENT_DATE);
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108723,'EU','COMREG','1','4','','','2598779',CURRENT_DATE);

----------new code id for system release comment log in existing group-----------------

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26240694,'System Release Log','GPLOG','1','','','','2598779',CURRENT_DATE);


-------insert new record for audit trail id ------------

insert into T940_AUDIT_TRAIL (C940_AUDIT_TRAIL_ID,C940_TABLE_NAME,C940_COLUMN_NAME,C940_CREATED_BY,C940_CREATED_DATE) values('313','T2082_SET_RELEASE_MAPPING','C901_STATUS_ID','303510',current_date);
 
/*
 * 
 * Based on SYSTEM_PUBLISH_ACC security event access for publish/unpublish button
 */  
 
INSERT
INTO T1520_FUNCTION_LIST
  (
    C1520_FUNCTION_ID,
    C1520_FUNCTION_NM,
    C1520_FUNCTION_DESC ,
    C1520_INHERITED_FROM_ID,
    C901_TYPE,
    C1520_VOID_FL,
    C1520_CREATED_BY,
    C1520_CREATED_DATE,
    C1520_LAST_UPDATED_BY,
    C1520_LAST_UPDATED_DATE,
    C1520_REQUEST_URI,
    C1520_REQUEST_STROPT ,
    C1520_SEQ_NO,
    C901_DEPARTMENT_ID,
    C901_LEFT_MENU_TYPE,
    C1520_LAST_UPDATED_DATE_UTC
  )
  VALUES
  (
    'SYSTEM_PUBLISH_ACC', 'SYSTEM_PUBLISH_ACC',
    'Users in this security event will have the access to publish the system',
    NULL,
    92267 ,
    NULL,
    '303510',
    CURRENT_DATE,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    CURRENT_DATE
  );
  

/*
 * 
 * Based on SYS_MAP_SUB_BTN_ACC security event access user can release and unrelease system
 */  
 
 
INSERT
INTO T1520_FUNCTION_LIST
  (
    C1520_FUNCTION_ID,
    C1520_FUNCTION_NM,
    C1520_FUNCTION_DESC ,
    C1520_INHERITED_FROM_ID,
    C901_TYPE,
    C1520_VOID_FL,
    C1520_CREATED_BY,
    C1520_CREATED_DATE,
    C1520_LAST_UPDATED_BY,
    C1520_LAST_UPDATED_DATE,
    C1520_REQUEST_URI,
    C1520_REQUEST_STROPT ,
    C1520_SEQ_NO,
    C901_DEPARTMENT_ID,
    C901_LEFT_MENU_TYPE,
    C1520_LAST_UPDATED_DATE_UTC
  )
  VALUES
  (
    'SYS_MAP_SUB_BTN_ACC', 'SYSTEM MAPPING SUBMIT BUTTON ACCESS',
    'Users in this security event will have the access to release and unrelease the system',
    NULL,
    92267 ,
    NULL,
    '303510',
    CURRENT_DATE,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    current_date
  );
  
 
--New T906 entry for mail Id
INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY)
VALUES (S906_RULE.NEXTVAL,'SYS_RELEASE_NOTIFY','JMS Exception Email Notification To','jgurunathan@globusmedical.com',CURRENT_DATE,'JMS_EXCEP_NOTIFY','303510');
