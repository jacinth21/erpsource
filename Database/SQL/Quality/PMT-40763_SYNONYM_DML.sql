-- create synonym for new package gm_pkg_device_group_master_sync
create or replace public synonym gm_pkg_device_group_master_sync_rpt for GLOBUS_APP.gm_pkg_device_group_master_sync_rpt;
-- create synonym for new package gm_pkg_device_group_sync_rpt
create or replace public synonym gm_pkg_device_group_sync_rpt for GLOBUS_APP.gm_pkg_device_group_sync_rpt;