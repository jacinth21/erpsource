--Insert Query to create group and records

insert into T906_Rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
values (s906_rule.NEXTVAL,'2000','Spine','100800',current_date,'DIV_COMP_MAPPING','303510');

insert into T906_Rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
values (s906_rule.NEXTVAL,'2001','Algea Therapies','100801',current_date,'DIV_COMP_MAPPING','303510');

insert into T906_Rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
values (s906_rule.NEXTVAL,'2002','TTOT','4000643',current_date,'DIV_COMP_MAPPING','303510');

insert into T906_Rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
values (s906_rule.NEXTVAL,'2004','I-N-Robotics','100800',current_date,'DIV_COMP_MAPPING','303510');

insert into T906_Rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
values (s906_rule.NEXTVAL,'2005','Trauma','100800',current_date,'DIV_COMP_MAPPING','303510');

insert into T906_Rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
values (s906_rule.NEXTVAL,'2006','JOM','100800',current_date,'DIV_COMP_MAPPING','303510');

insert into T906_Rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
values (s906_rule.NEXTVAL,'2007','Arthroplasty','100800',current_date,'DIV_COMP_MAPPING','303510');