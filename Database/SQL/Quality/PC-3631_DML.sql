--PC-3631 Corporate Files Upload on SpineIT - Globus App - Corporate Files
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(26241209,'Corporate Files','SMSRG','1','','','','2423914',CURRENT_DATE);

--Product Literature

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(26241210,'Product Literature','SMSRG','1','','','','2423914',CURRENT_DATE);

