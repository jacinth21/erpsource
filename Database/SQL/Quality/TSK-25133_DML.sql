-- Left Links are Robotics Quote, CRM Lead, ERP AR for Micro Applications from SpineIT

INSERT INTO t1520_function_list ( c1520_function_id, c1520_function_nm, c1520_inherited_from_id, c1520_function_desc, c1520_request_uri, c1520_request_stropt, c901_type, c1520_created_date, c1520_created_by )
VALUES ( 'Micro_Robotics_Quote', 'Robotics Quote', NULL, 'INR Quote Tool Web Application - Micro App', '/inrquote/', NULL, 92262, current_date, '4241740');
 
INSERT INTO t1520_function_list ( c1520_function_id, c1520_function_nm, c1520_inherited_from_id, c1520_function_desc, c1520_request_uri, c1520_request_stropt, c901_type, c1520_created_date, c1520_created_by )
VALUES ( 'Micro_CRM_Lead', 'CRM Lead', NULL, 'CRM Lead Web Application - Micro App', '/lead/', NULL, 92262, current_date, '4241740');

INSERT INTO t1520_function_list ( c1520_function_id, c1520_function_nm, c1520_inherited_from_id, c1520_function_desc, c1520_request_uri, c1520_request_stropt, c901_type, c1520_created_date, c1520_created_by )
VALUES ( 'Micro_ERP_AR', 'ERP AR', NULL, 'ERP AR Web Application - Micro App', '/erpar/', NULL, 92262, current_date, '4241740');