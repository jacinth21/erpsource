--PC-2368 BOM mapping - Remove void access get the rule values 


INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID, C906_CREATED_BY, C906_LAST_UPDATED_DATE, C906_LAST_UPDATED_BY
    )
    VALUES
    (
        s906_rule.nextval,4050, 'BOM creation by part family - Implants'
      , 'Y'
      ,CURRENT_DATE, 'BOM_CRE_BY_PART_FAM'
      , '303510', NULL, NULL
    ) ;
   
INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY, C906_LAST_UPDATED_DATE, C906_LAST_UPDATED_BY
    )
    VALUES
    (
        s906_rule.nextval,4051, 'BOM creation by part family - Instruments'
      , 'Y'
      ,CURRENT_DATE, 'BOM_CRE_BY_PART_FAM'
      , '303510', NULL, NULL
    ) ;

INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY, C906_LAST_UPDATED_DATE, C906_LAST_UPDATED_BY
    )
    VALUES
    (
        s906_rule.nextval,4052, 'BOM creation by part family - Graphic Case'
      , 'Y'
      ,CURRENT_DATE, 'BOM_CRE_BY_PART_FAM'
      , '303510', NULL, NULL
    ) ;
	
INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY, C906_LAST_UPDATED_DATE, C906_LAST_UPDATED_BY
    )
    VALUES
    (
        s906_rule.nextval,4053, 'BOM creation by part family - Demo-Material'
      , 'Y'
      ,CURRENT_DATE, 'BOM_CRE_BY_PART_FAM'
      , '303510', NULL, NULL
    ) ;
	
INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY, C906_LAST_UPDATED_DATE, C906_LAST_UPDATED_BY
    )
    VALUES
    (
        s906_rule.nextval,4054, 'BOM creation by part family - Mfg-Inventory'
      , 'Y'
      ,CURRENT_DATE, 'BOM_CRE_BY_PART_FAM'
      , '303510', NULL, NULL
    ) ;
	
INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY, C906_LAST_UPDATED_DATE, C906_LAST_UPDATED_BY
    )
    VALUES
    (
        s906_rule.nextval,4055, 'BOM creation by part family - Mfg-Supply'
      , 'Y'
      ,CURRENT_DATE, 'BOM_CRE_BY_PART_FAM'
      , '303510', NULL, NULL
    ) ;
	
INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY, C906_LAST_UPDATED_DATE, C906_LAST_UPDATED_BY
    )
    VALUES
    (
        s906_rule.nextval,26240095, 'BOM creation by part family - Frequently Sold'
      , 'Y'
      ,CURRENT_DATE, 'BOM_CRE_BY_PART_FAM'
      , '303510', NULL, NULL
    ) ;
    