/*
 * 
 * Based on SET_MAP_SUB_BTN_ACC security event access user can release and unrelease set
 */ 
  
INSERT
INTO T1520_FUNCTION_LIST
  (
    C1520_FUNCTION_ID,
    C1520_FUNCTION_NM,
    C1520_FUNCTION_DESC ,
    C1520_INHERITED_FROM_ID,
    C901_TYPE,
    C1520_VOID_FL,
    C1520_CREATED_BY,
    C1520_CREATED_DATE,
    C1520_LAST_UPDATED_BY,
    C1520_LAST_UPDATED_DATE,
    C1520_REQUEST_URI,
    C1520_REQUEST_STROPT ,
    C1520_SEQ_NO,
    C901_DEPARTMENT_ID,
    C901_LEFT_MENU_TYPE,
    C1520_LAST_UPDATED_DATE_UTC
  )
  VALUES
  (
    'SET_MAP_SUB_BTN_ACC', 'SET MAPPING SUBMIT BUTTON ACCESS',
    'Users in this security event will have the access to release and unrelease the set',
    NULL,
    92267 ,
    NULL,
    '303510',
    CURRENT_DATE,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    current_date
  );