-- Update the IPAD sync flag and synced date as NULL in part tables and execute the job to insert or update the master ipad sync tables to sync in IPAD
    -- to update Part number ipad flag based on Demo Material Product Family
     UPDATE T205_PART_NUMBER
        SET C205_IPAD_SYNCED_FLAG    = NULL, C205_IPAD_SYNCED_DATE = NULL
      WHERE C205_PART_NUMBER_ID IN
        (
             SELECT c205_part_number_id
               FROM t208_set_details t208, t207_set_master t207
              WHERE t208.c207_set_id       = t207.c207_set_id
                AND t207.c901_set_grp_type = 1600
        )
        AND C205_ACTIVE_FL      = 'Y'
        AND C205_RELEASE_FOR_SALE = 'Y'
        AND C205_PRODUCT_FAMILY  = '4053';  
      

	-- to update part attribute ipad flag based on Demo Material Product Family
      UPDATE T205D_PART_ATTRIBUTE
         SET C205D_IPAD_SYNCED_FLAG   = NULL, C205D_IPAD_SYNCED_DATE = NULL
       WHERE C205_PART_NUMBER_ID IN
        (
             SELECT t205.c205_part_number_id
               FROM t208_set_details t208, t207_set_master t207, t205_part_number t205
              WHERE t208.c207_set_id       = t207.c207_set_id
                AND t205.c205_part_number_id = t208.c205_part_number_id
                AND t205.C205_PRODUCT_FAMILY  = '4053'
                AND t205.C205_ACTIVE_FL      = 'Y'
                AND t205.C205_RELEASE_FOR_SALE = 'Y'
                AND t207.c901_set_grp_type = 1600
        )
        AND C205D_VOID_FL      IS NULL
        AND C901_ATTRIBUTE_TYPE = 80180;
        
     -- to update part company map ipad flag based on Demo Material Product Family
     UPDATE T2023_PART_COMPANY_MAPPING
        SET C2023_IPAD_SYNCED_FLAG   = NULL, C2023_IPAD_SYNCED_DATE = NULL
      WHERE C205_PART_NUMBER_ID IN
        (
             SELECT t205.c205_part_number_id
               FROM t208_set_details t208, t207_set_master t207, t205_part_number t205
              WHERE t208.c207_set_id       = t207.c207_set_id
                AND t205.c205_part_number_id = t208.c205_part_number_id
                AND t205.C205_PRODUCT_FAMILY  = '4053'
                AND t205.C205_ACTIVE_FL      = 'Y'
                AND t205.C205_RELEASE_FOR_SALE = 'Y'
                AND t207.c901_set_grp_type = 1600
        )
        AND C2023_VOID_FL IS NULL;
        
    -- to update part meta ipad flag based on Demo Material Product Family
     UPDATE T2000_MASTER_METADATA
        SET C2000_IPAD_SYNCED_FLAG = NULL, C2000_IPAD_SYNCED_DATE = NULL
      WHERE C2000_REF_ID      IN
        (
             SELECT  t205.c205_part_number_id
               FROM t208_set_details t208, t207_set_master t207, t205_part_number t205
              WHERE t208.c207_set_id       = t207.c207_set_id
                AND t205.c205_part_number_id = t208.c205_part_number_id
                AND t205.C205_PRODUCT_FAMILY  = '4053'
                AND t205.C205_ACTIVE_FL      = 'Y'
                AND t205.C205_RELEASE_FOR_SALE = 'Y'
                AND t207.c901_set_grp_type = 1600
        )
        AND C2000_VOID_FL IS NULL;