-- Add the demo material type in PART_PROD_FAMILY in RULE Table, 4053 is a demo material type
INSERT INTO t906_rules (
C906_RULE_SEQ_ID,
c906_rule_id,
c906_rule_desc,
c906_rule_value,
c906_created_date,
c906_rule_grp_id,
c906_created_by
) VALUES (
S906_RULE.nextval,
'PART_PROD_FAMILY',
'Inserts Demo Material to Part Sync',
'4053',
CURRENT_DATE,
'PROD_CAT_SYNC',
'303011'
);