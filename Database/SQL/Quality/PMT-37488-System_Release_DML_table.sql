-- create synonym for new table t1902_entity_company_map
create or replace public synonym t1902_entity_company_map for GLOBUS_APP.t1902_entity_company_map;
-- create synonym for new table t2082_set_release_mapping
create or replace public synonym t2082_set_release_mapping for GLOBUS_APP.t2082_set_release_mapping;
-- create synonym for new table t207s_set_master_sync
create or replace public synonym t207s_set_master_sync for GLOBUS_APP.t207s_set_master_sync;
-- create synonym for new table t207s_set_attribute_sync
create or replace public synonym t207s_set_attribute_sync for GLOBUS_APP.t207s_set_attribute_sync;
-- create synonym for new table t207s_set_details_sync
create or replace public synonym t207s_set_details_sync for GLOBUS_APP.t207s_set_details_sync;
-- create synonym for new table my_temp_system_sync_update
create or replace public synonym my_temp_system_sync_update for GLOBUS_APP.my_temp_system_sync_update;
-- create synonym for new table my_temp_set_sync_update
create or replace public synonym my_temp_set_sync_update for GLOBUS_APP.my_temp_set_sync_update;
-- create synonym for new table my_temp_set_attribute_sync_update
create or replace public synonym my_temp_set_attribute_sync_update for GLOBUS_APP.my_temp_set_attribute_sync_update;
-- create synonym for new table my_temp_set_part_dtls_sync_update
create or replace public synonym my_temp_set_part_dtls_sync_update for GLOBUS_APP.my_temp_set_part_dtls_sync_update;

-- create synonym for new procedure gm_pkg_cm_entity_rpt
create or replace public synonym gm_pkg_cm_entity_rpt for GLOBUS_APP.gm_pkg_cm_entity_rpt;
-- create synonym for new trigger trg_2082_system_release_history
create or replace public synonym trg_2082_system_release_history for GLOBUS_APP.trg_2082_system_release_history;
