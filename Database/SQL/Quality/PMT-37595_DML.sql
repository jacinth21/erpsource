-- create synonym for new procedure gm_pkg_device_system_master_sync
create or replace public synonym gm_pkg_device_system_master_sync_rpt for GLOBUS_APP.gm_pkg_device_system_master_sync_rpt;
-- create synonym for new procedure gm_pkg_device_system_sync_rpt
create or replace public synonym gm_pkg_device_system_sync_rpt for GLOBUS_APP.gm_pkg_device_system_sync_rpt;
-- create synonym for new procedure gm_pkg_device_set_master_sync
create or replace public synonym gm_pkg_device_set_master_sync_rpt for GLOBUS_APP.gm_pkg_device_set_master_sync_rpt;
-- create synonym for new procedure gm_pkg_device_set_sync_rpt
create or replace public synonym gm_pkg_device_set_sync_rpt for GLOBUS_APP.gm_pkg_device_set_sync_rpt;
-- create synonym for new procedure gm_pkg_device_sync_update_dtls
create or replace public synonym gm_pkg_device_sync_update_dtls for GLOBUS_APP.gm_pkg_device_sync_update_dtls;

