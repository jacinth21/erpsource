/*
 * PMT-50777 - Create BOM and sub-component Mapping
 * Left link Part Number - Bulk Sub Component Mapping 
 */  
  
  --Insert record to create left link
INSERT INTO T1520_FUNCTION_LIST
  (
    C1520_FUNCTION_ID,
    C1520_FUNCTION_NM,
    C1520_FUNCTION_DESC,
    C1520_INHERITED_FROM_ID,
    C901_TYPE,
    C1520_VOID_FL,
    C1520_CREATED_BY,
    C1520_CREATED_DATE,
    C1520_LAST_UPDATED_BY,
    C1520_LAST_UPDATED_DATE,
    C1520_REQUEST_URI,
    C1520_REQUEST_STROPT,
    C1520_SEQ_NO,
    C901_DEPARTMENT_ID,
    C901_LEFT_MENU_TYPE
  )
  VALUES
  (
    'BULK_SUB_COMPONENT',
    'Part Number-Bulk SubComponent Map',
    'To create the new BOM/Update the BOM details and sub-component Mapping.',
    NULL,
    92262,
    NULL,
    '303510',
    SYSDATE,
    '303510',
    NULL,
    '/gmPartNumberAssembly.do?method=loadPartNumberAssemblyUploadDtls',
    NULL,
    NULL,
    '2003',
    '101920'
  );
  
     
INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'SUBUPL','BOM Sub component Action','','2598779',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(109740,'Part Number','SUBUPL','1','1','','','2598779',CURRENT_DATE);
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(109741,'Sub-component','SUBUPL','1','2','','','2598779',CURRENT_DATE);


--Comments section
INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES
(s901b_code_group_master.NEXTVAL,'GPLOG','Quality Set List Copy','','2598779',CURRENT_DATE);
 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(26240883,'BOM bulk upload','GPLOG','1','','','','2598779',CURRENT_DATE);


-- based on access to enable/disable the button (BOM - Mpping).

INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, '1602_SET_MAPPING_BUTTON', 'Based on flag to disable the BOM mapping button'
      , 'Y', CURRENT_DATE, 'DISABLE'
      , '706322'
    ) ;


    create or replace public synonym gm_pkg_qa_part_num_assembly for GLOBUS_APP.gm_pkg_qa_part_num_assembly;