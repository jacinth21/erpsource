-- TO inactive the duplicate parts

set serveroutput on;
DECLARE
  v_part_count  number := 0;
  v_log_out     varchar2(4000);
  
  CURSOR c_partnum_list
	IS
    SELECT c205_part_number_id pnum
      FROM t205_part_number 
     WHERE c205_part_number_id IN (
                               '32601x','80720x','dt.601.03','dt.601.04','dt.601.05','Gc.803.06','Gm10300214S.3','gm10301108S.3',
                               'Gm10301108S.3','Gm10301111S.2','Gm31101041.01','Gm31101042.01','gm31104509.01','Gm81120102S.3',
                               'Gm81140010S.3','Gm81140110S.1','Gm81140202S.3','Gm81140210S.3','Gm81145010S.3','Gm81145102S.1',
                               'Gm81145102S.3','Gm81145202S.1','ms.0656','Nu.V222','sc2025-3','sc2476-1','sc2633-3','sc2861-46',
                               'sc2861-60','sc2880-2','sc2880-3','sc2880-4','sc2880-5','sc2995','sc2996','sc3427-26','sc3427-29',
                               'sc3427-32','sc3427-35','sc3427-38','sc3427-41'
                                 );
BEGIN
	     
    	--
	FOR v_partnum IN c_partnum_list
	LOOP    
	    
         v_part_count := v_part_count +1;
         
         UPDATE t205_part_number
            SET c205_active_fl = 'N', -- Inactive
                c205_last_updated_by = '303011', -- dspaul
                c205_last_updated_date = current_date
          WHERE c205_part_number_id = v_partnum.pnum;
          
         GM_UPDATE_LOG(v_partnum.pnum,'Inactivated the duplicate parts.','1218','303011',v_log_out);
         dbms_output.put_line(v_log_out); 

    END LOOP;      

   dbms_output.put_line(v_part_count || ' parts are inactivated... ' ); 
END;
