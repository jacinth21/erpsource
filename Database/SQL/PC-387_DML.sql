--insert keycloak in ldap Master
  insert into t906c_ldap_master (C906C_LDAP_MASTER_ID, C906C_LDAP_NAME, C906C_CREATED_BY, C906C_CREATED_DATE, C906C_ACTIVE_FL) 
  values(4,'Keycloak',706322,current_date,'Y');

--insertion of security group
  INSERT INTO t1500_group (C1500_GROUP_ID,c901_group_type,c1500_group_nm,c1500_group_desc,c1500_created_by,c1500_created_date)  
  values('Keycloak_Access',92264,'Keycloak_Access','Keycloak_Access','303013',current_date);
  
--insertion of Keyclock in Code Look up
  INSERT INTO t901_code_lookup ( C901_CODE_ID, C901_CODE_NM, C901_CODE_GRP, C901_CREATED_DATE, C901_CREATED_BY ) 
  values(109300, 'Keycloak', 'AUTHT', current_date, 706310);
  