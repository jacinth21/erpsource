-- Update Attendee type as surgeon for all records in case request table
UPDATE t6660_case_request
SET
    c901_attendee_type = '109000',
    c6660_updated_by = '706310',
    c6660_updated_dt = current_date
WHERE
    c6660_void_fl IS NULL
    AND c901_attendee_type IS NULL;