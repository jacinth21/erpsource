/*To show the Company Dropdown with all Company Options by security event on Create Surgeon */
INSERT INTO t1520_function_list (
    c1520_function_id,
    c1520_function_nm,
    c1520_function_desc,
    c901_type,
    c1520_created_by,
    c1520_created_date,
    c1520_request_uri,
    c901_department_id,
    c901_left_menu_type
) VALUES (
    'CRM-SURGEON-ADMIN',
    'CRM-SURGEON-ADMIN',
    'Surgeon Admin Access',
    '92262',
    '4241740',
    current_date,
    '/#crm/surgeon',
    NULL,
    NULL
);

 