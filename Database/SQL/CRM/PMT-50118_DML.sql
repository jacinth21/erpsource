/*To show the Tradeshow Widget by security group*/
INSERT INTO t1520_function_list (
    c1520_function_id,
    c1520_function_nm,
    c1520_function_desc,
    c901_type,
    c1520_created_by,
    c1520_created_date,
    c1520_request_uri,
    c901_department_id,
    c901_left_menu_type
) VALUES (
    'CRM-TRADESHOW',
    'Tradeshow',
    'Tradeshow Dashboard',
    '92262',
    '706310',
    current_date,
    '/#crm/tradeshow',
    NULL,
    NULL
);

 

 
/*To show the tradeshow event by status in t906_rules table*/
 
INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_desc,
    c906_rule_value,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by
) VALUES (
    s906_rule.NEXTVAL,
    '109041',
    'To display Tradeshow Event in CRM dashboard',
    'Approved',
    current_date,
    'TSSTAS',
    '706310'
);

 

INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_desc,
    c906_rule_value,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by
) VALUES (
    s906_rule.NEXTVAL,
    '109042',
    'To display Tradeshow Event in CRM dashboard',
    'In Progress',
    current_date,
    'TSSTAS',
    '706310'
);

 

