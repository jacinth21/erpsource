 --US ZONES
 --Inactive '100822','108651','26240440','26240580'
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100823', '94102');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100823', '94103');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100823', '94104');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100823', '94105');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100823', '100822');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100823', '101420');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100823', '108651');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100823', '4000396');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100823', '26230601');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100823', '26230607');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100823', '26230711');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100823', '26230712');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100823', '26240196');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100823', '26240197');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100823', '26240276');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100823', '26240405');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100823', '26240406');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100823', '26240410');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100823', '26240411');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100823', '26240440');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100823', '26240580');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100823', '26240630');
 
 --OUS ZONES
 --Inactive '100361','100829','101962','26230751'
 --OUS --INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100824', '94106');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100824', '100361');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100824', '100826');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100824', '100829');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100824', '101962');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100824', '101964');
--Luxemborg -- INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100824', '102180');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100824', '10306115');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100824', '26230751');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100824', '26230752');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100824', '26240290');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100824', '26240520');
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '100824', '26240538');