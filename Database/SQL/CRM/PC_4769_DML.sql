----------------------------Physician Visit---------------------------------
--SPINE--
INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_desc,
    c906_rule_value,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c906_last_updated_date,
    c906_last_updated_by,
    C1900_COMPANY_ID
) VALUES (
    s906_rule.NEXTVAL,
    '2000',
    'Recipient email id for VIP Notification',
    'SPINE_VIP_INVITE',
    sysdate,
    'CRM_INVT_GRP_105266',
    '706310',
    NULL,
    NULL,
    1000
);

INSERT
INTO t1520_function_list
  (
    c1520_function_id,
    c1520_function_nm,
    c1520_inherited_from_id,
    c1520_function_desc,
    c901_type,
    c1520_created_date,
    c1520_created_by
  )
  VALUES
  (
    'SPINE_VIP_INVITE',
    'SPINE_VIP_INVITE',
    'SPINE_VIP_INVITE',
    'Users in this security event can receive VIP visit notification',
    92267,
    sysdate,
    '706322'
  );
  
  --INR--
  INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_desc,
    c906_rule_value,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c906_last_updated_date,
    c906_last_updated_by,
    C1900_COMPANY_ID
) VALUES (
    s906_rule.NEXTVAL,
    '2004',
    'Recipient email id for VIP Notification',
    'INR_VIP_INVITE',
    sysdate,
    'CRM_INVT_GRP_105266',
    '706310',
    NULL,
    NULL,
    1000
);
  
INSERT
INTO t1520_function_list
  (
    c1520_function_id,
    c1520_function_nm,
    c1520_inherited_from_id,
    c1520_function_desc,
    c901_type,
    c1520_created_date,
    c1520_created_by
  )
  VALUES
  (
    'INR_VIP_INVITE',
    'INR_VIP_INVITE',
    'INR_VIP_INVITE',
    'Users in this security event can receive VIP visit notification',
    92267,
    sysdate,
    '706322'
  );
  
  --TRUMA--
  INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_desc,
    c906_rule_value,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c906_last_updated_date,
    c906_last_updated_by,
    C1900_COMPANY_ID
) VALUES (
    s906_rule.NEXTVAL,
    '2005',
    'Recipient email id for VIP Notification',
    'TRAUMA_VIP_INVITE',
    sysdate,
    'CRM_INVT_GRP_105266',
    '706310',
    NULL,
    NULL,
    1000
);
  
INSERT
INTO t1520_function_list
  (
    c1520_function_id,
    c1520_function_nm,
    c1520_inherited_from_id,
    c1520_function_desc,
    c901_type,
    c1520_created_date,
    c1520_created_by
  )
  VALUES
  (
    'TRAUMA_VIP_INVITE',
    'TRAUMA_VIP_INVITE',
    'TRAUMA_VIP_INVITE',
    'Users in this security event can receive VIP visit notification',
    92267,
    sysdate,
    '706322'
  );
  
  --JOINT--
  
   INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_desc,
    c906_rule_value,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c906_last_updated_date,
    c906_last_updated_by,
    C1900_COMPANY_ID
) VALUES (
    s906_rule.NEXTVAL,
    '2007',
    'Recipient email id for VIP Notification',
    'JOINT_VIP_INVITE',
    sysdate,
    'CRM_INVT_GRP_105266',
    '706310',
    NULL,
    NULL,
    1000
);
  
INSERT
INTO t1520_function_list
  (
    c1520_function_id,
    c1520_function_nm,
    c1520_inherited_from_id,
    c1520_function_desc,
    c901_type,
    c1520_created_date,
    c1520_created_by
  )
  VALUES
  (
    'JOINT_VIP_INVITE',
    'JOINT_VIP_INVITE',
    'JOINT_VIP_INVITE',
    'Users in this security event can receive VIP visit notification',
    92267,
    sysdate,
    '706322'
  );
  
  
  ----Preceptorship---
  
  --SPINE--
  INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_desc,
    c906_rule_value,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c906_last_updated_date,
    c906_last_updated_by,
    C1900_COMPANY_ID
) VALUES (
    s906_rule.NEXTVAL,
    '2000',
    'Recipient email id for Preceptorship Notification',
    'SPINE_PTRSHIP_INVITE',
    sysdate,
    'CRM_INVT_GRP_105272',
    '706310',
    NULL,
    NULL,
    1000
);
  
INSERT
INTO t1520_function_list
  (
    c1520_function_id,
    c1520_function_nm,
    c1520_inherited_from_id,
    c1520_function_desc,
    c901_type,
    c1520_created_date,
    c1520_created_by
  )
  VALUES
  (
    'SPINE_PTRSHIP_INVITE',
    'SPINE_PTRSHIP_INVITE',
    'SPINE_PTRSHIP_INVITE',
    'Users in this security event can receive Preceptorship visit notification',
    92267,
    sysdate,
    '706322'
  );
  
  --INR--
  
  INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_desc,
    c906_rule_value,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c906_last_updated_date,
    c906_last_updated_by,
    C1900_COMPANY_ID
) VALUES (
    s906_rule.NEXTVAL,
    '2004',
    'Recipient email id for Preceptorship Notification',
    'INR_PTRSHIP_INVITE',
    sysdate,
    'CRM_INVT_GRP_105272',
    '706310',
    NULL,
    NULL,
    1000
);
  
INSERT
INTO t1520_function_list
  (
    c1520_function_id,
    c1520_function_nm,
    c1520_inherited_from_id,
    c1520_function_desc,
    c901_type,
    c1520_created_date,
    c1520_created_by
  )
  VALUES
  (
    'INR_PTRSHIP_INVITE',
    'INR_PTRSHIP_INVITE',
    'INR_PTRSHIP_INVITE',
    'Users in this security event can receive Preceptorship visit notification',
    92267,
    sysdate,
    '706322'
  );
  
  ------------------------------------------------------------