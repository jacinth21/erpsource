-- To Display Status in Filter in the Physician Visit Calendar (105266) - Physician Visit Type

 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '105266', '105760');
 
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '105266', '105761');
 
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '105266', '105762');
 
 -- To Display Division List in Host and case Create in the Preceptorship Module (105272) - Preceptorship Type

 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '105272', '107200');
 
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '105272', '107201');
 
 INSERT INTO t901a_lookup_access(c901a_code_access_id, c901_access_code_id, c901_code_id)VALUES (s901a_lookup_access.NEXTVAL, '105272', '107202');