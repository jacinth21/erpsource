DELETE FROM T906_Rules WHERE C906_Rule_Id ='MODE_EXCLUSION' AND C906_Rule_Grp_Id='MODEEXCLUDE';

-- To exclude Next Day Air Early A.M., Saturday-Next Day Air Early A.M, First Overnight, Saturday-First Over, Early AM, Same Day from shipping address mode dropdown
INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by,c906_last_updated_date,c906_last_updated_by,c901_rule_type,c906_active_fl,c906_void_fl,c1900_company_id,c906_last_updated_date_utc) 
VALUES (S906_Rule.Nextval,'MODE_EXCLUSION','Mode Exclude','4000612,4000608,4000606,5043,5003,5038',current_date,'MODEEXCLUDE','3178209',NULL,NULL,NULL,NULL,Null,1000,NULL);    