--Disable PO Generation Checkbox Rule For No Vendor 
DELETE FROM t906_rules WHERE c906_rule_id in ('690','86') AND c906_rule_grp_id = 'GEN_VEN_PO_MANUAL';

INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_value,
    c906_rule_desc,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c1900_company_id
) VALUES (
    s906_rule.NEXTVAL,
    690,
    'Y',
    'Disable Checkbox Po Generation Screen',
    sysdate,
    'GEN_VEN_PO_MANUAL',
    303012,
    NULL
);

INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_value,
    c906_rule_desc,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c1900_company_id
) VALUES (
    s906_rule.NEXTVAL,
    86,
    'Y',
    'Disable Checkbox Po Generation Screen',
    sysdate,
    'GEN_VEN_PO_MANUAL',
    303012,
    NULL
);