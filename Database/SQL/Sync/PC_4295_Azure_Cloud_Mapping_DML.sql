
INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'CLOUD','Cloud Type','','706322',CURRENT_DATE);

 
 
  
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(111380,'Azure','CLOUD','1','','','','706322',CURRENT_DATE); 


-- select * From T906_RULES where C906_RULE_GRP_ID like '%MARKETINGCOLL%' ;

 INSERT
   INTO t906_rules
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'SYNC_RULE_GRP', 'Rule for expiry token days'
      , 'MARKETINGCOLL', CURRENT_DATE, 'PROD_CAT_SYNC'
      , '706322'
    ) ;

set scan off;

 INSERT
   INTO t906_rules
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'CLOUD_FILE_SAS_TOKEN', 'Azure cloud file - SAS token'
      , '?sv=2020-02-10&ss=bfqt&srt=sco&sp=rwdlacupx&se=2022-01-01T03:40:57Z&st=2021-04-01T18:40:57Z&spr=https&sig=9NbEjcPBnJ3uEzU3U5EdoVM6iAKdULZUS6iUkH5N55g%3D', CURRENT_DATE, 'MARKETINGCOLL'
      , '706322'
    ) ;

INSERT
   INTO t906_rules
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY, C1900_COMPANY_ID
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'GLOBUS_CONTACT_NO', 'Globus contact number'
      , '601-930-1800', CURRENT_DATE, 'MARKETINGCOLL'
      , '706322', 1000
    ) ;
    
  

update t906_rules set C906_RULE_VALUE = C906_RULE_VALUE, C906_LAST_UPDATED_BY = 706322, C906_LAST_UPDATED_DATE = sysdate
where C906_RULE_GRP_ID = 'MARKETINGCOLL';



INSERT
   INTO t906_rules
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY, C1900_COMPANY_ID
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'MESSAGE_MKT_COLL_EMAIL', 'Marketing Collateral - Share Email Message'
      , 'Thank you for your interest in Globus Medical. Below are the link(s) to the collateral you requested.These links will expire on', CURRENT_DATE, 'MARKETINGCOLL'
      , '706322', 1000
    ) ;

INSERT
   INTO t906_rules
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'DEFAULT_MESSAGE_MKT_COLL_EMAIL', 'Marketing Collateral - Share Email Message'
      , 'Thank you for your interest in Globus Medical. Below are the link(s) to the collateral you requested.These links will expire on', CURRENT_DATE, 'MARKETINGCOLL'
      , '706322'
    ) ;


INSERT
   INTO t906_rules
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY, C1900_COMPANY_ID
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'SUBJECT_MKT_COLL_EMAIL', 'Marketing Collateral - Share Subject'
      , 'Globus Product Information', CURRENT_DATE, 'MARKETINGCOLL'
      , '706322', 1000
    ) ;


INSERT
   INTO t906_rules
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'DEFAULT_SUBJECT_MKT_COLL_EMAIL', 'Marketing Collateral - Share Subject'
      , 'Globus Product Information', CURRENT_DATE, 'MARKETINGCOLL'
      , '706322'
    ) ;



INSERT
   INTO t906_rules
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY, C1900_COMPANY_ID
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'COMPANY_ADDRESS', 'Marketing Collateral - Share Company Address- GMNA'
      , 'Globus Medical North America, Inc.<br>Valley Forge Business Center<br>2560 General Armistead Avenue<br>Audubon, PA 19403<br>Phone:610-930-1800', CURRENT_DATE, 'MARKETINGCOLL'
      , '706322', 1000
    ) ;

INSERT
   INTO t906_rules
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'DEFAULT_COMPANY_ADDRESS', 'Marketing Collateral - Share Company Address'
      , 'Globus Medical North America, Inc.<br>Valley Forge Business Center<br>2560 General Armistead Avenue<br>Audubon, PA 19403<br>Phone:610-930-1800', CURRENT_DATE, 'MARKETINGCOLL'
      , '706322'
    ) ;

INSERT
   INTO t906_rules
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'FUNCTION_AUTH_KEY', 'Azure cloud Function key'
      , 'Z8tECH3GkAK02ilKFJAHlQ1SesmTJmaqFCo7hQE868wiiQSSTzIwaw==', CURRENT_DATE, 'MARKETINGCOLL'
      , '706322'
    ) ;

    
create or replace public synonym T9170_CLOUD_WEB_SERVICE_LOG FOR GLOBUS_APP.T9170_CLOUD_WEB_SERVICE_LOG;
create or replace public synonym gm_pkg_cm_cloud_ws_log FOR GLOBUS_APP.gm_pkg_cm_cloud_ws_log;
