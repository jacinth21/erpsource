-- PC-4236 List RFS parts based on the logged in user Region
INSERT INTO t906_rules ( c906_rule_seq_id, c906_rule_id, c906_rule_grp_id, c906_rule_value, c906_rule_desc, c906_created_date, c906_created_by )
VALUES ( s906_rule.NEXTVAL, 108640,  'RFS_REGION', 80120, 'Release for sales region mapping', current_date, '303510' );

INSERT INTO t906_rules ( c906_rule_seq_id, c906_rule_id, c906_rule_grp_id, c906_rule_value, c906_rule_desc, c906_created_date, c906_created_by ) 
VALUES ( s906_rule.NEXTVAL, 108641,  'RFS_REGION', 100940, 'Release for sales region mapping', current_date, '303510' );


INSERT INTO t906_rules ( c906_rule_seq_id, c906_rule_id, c906_rule_grp_id, c906_rule_value, c906_rule_desc, c906_created_date, c906_created_by ) 
VALUES ( s906_rule.NEXTVAL, 108641,  'RFS_REGION', 80122, 'Release for sales region mapping', current_date, '303510' );


INSERT INTO t906_rules ( c906_rule_seq_id, c906_rule_id, c906_rule_grp_id, c906_rule_value, c906_rule_desc, c906_created_date, c906_created_by ) 
VALUES ( s906_rule.NEXTVAL, 108642,  'RFS_REGION', 80121, 'Release for sales region mapping', current_date, '303510' );


INSERT INTO t906_rules ( c906_rule_seq_id, c906_rule_id, c906_rule_grp_id, c906_rule_value, c906_rule_desc, c906_created_date, c906_created_by ) 
VALUES ( s906_rule.NEXTVAL, 108642,  'RFS_REGION', 101620, 'Release for sales region mapping', current_date, '303510' );

INSERT INTO t906_rules ( c906_rule_seq_id, c906_rule_id, c906_rule_grp_id, c906_rule_value, c906_rule_desc, c906_created_date, c906_created_by ) 
VALUES ( s906_rule.NEXTVAL, 108643,  'RFS_REGION', 10306128, 'Release for sales region mapping', current_date, '303510' );



INSERT INTO t906_rules ( c906_rule_seq_id, c906_rule_id, c906_rule_grp_id, c906_rule_value, c906_rule_desc, c906_created_date, c906_created_by ) 
VALUES ( s906_rule.NEXTVAL, 'UDI_LOAD_TXN',  'UDI_LOAD_TXN_TYPE', 50154, 'UDI Load Shippment Transaction', current_date, '303510' );

INSERT INTO t906_rules ( c906_rule_seq_id, c906_rule_id, c906_rule_grp_id, c906_rule_value, c906_rule_desc, c906_created_date, c906_created_by ) 
VALUES ( s906_rule.NEXTVAL, 'UDI_LOAD_TXN',  'UDI_LOAD_TXN_TYPE', 4112, 'UDI Load Shippment Transaction', current_date, '303510' );

INSERT INTO t906_rules ( c906_rule_seq_id, c906_rule_id, c906_rule_grp_id, c906_rule_value, c906_rule_desc, c906_created_date, c906_created_by ) 
VALUES ( s906_rule.NEXTVAL, 'UDI_LOAD_TXN',  'UDI_LOAD_TXN_TYPE', 4119, 'UDI Load Shippment Transaction', current_date, '303510' );

INSERT INTO t906_rules ( c906_rule_seq_id, c906_rule_id, c906_rule_grp_id, c906_rule_value, c906_rule_desc, c906_created_date, c906_created_by ) 
VALUES ( s906_rule.NEXTVAL, 'UDI_LOAD_TXN',  'UDI_LOAD_TXN_TYPE', 4127, 'UDI Load Shippment Transaction', current_date, '303510' );

INSERT INTO t906_rules ( c906_rule_seq_id, c906_rule_id, c906_rule_grp_id, c906_rule_value, c906_rule_desc, c906_created_date, c906_created_by ) 
VALUES ( s906_rule.NEXTVAL, 'UDI_LOAD_TXN',  'UDI_LOAD_TXN_TYPE', 4129, 'UDI Load Shippment Transaction', current_date, '303510' );

INSERT INTO t906_rules ( c906_rule_seq_id, c906_rule_id, c906_rule_grp_id, c906_rule_value, c906_rule_desc, c906_created_date, c906_created_by ) 
VALUES ( s906_rule.NEXTVAL, 'UDI_LOAD_TXN',  'UDI_LOAD_TXN_TYPE', 9110, 'UDI Load Shippment Transaction', current_date, '303510' );

INSERT INTO t906_rules ( c906_rule_seq_id, c906_rule_id, c906_rule_grp_id, c906_rule_value, c906_rule_desc, c906_created_date, c906_created_by ) 
VALUES ( s906_rule.NEXTVAL, 'UDI_LOAD_TXN',  'UDI_LOAD_TXN_TYPE', 40021, 'UDI Load Shippment Transaction', current_date, '303510' );

INSERT INTO t906_rules ( c906_rule_seq_id, c906_rule_id, c906_rule_grp_id, c906_rule_value, c906_rule_desc, c906_created_date, c906_created_by ) 
VALUES ( s906_rule.NEXTVAL, 'UDI_LOAD_TXN',  'UDI_LOAD_TXN_TYPE', 40057, 'UDI Load Shippment Transaction', current_date, '303510' );