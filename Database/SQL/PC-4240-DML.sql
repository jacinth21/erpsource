-- PC-4240 Display only specific family types of part in the UDI Barcode generate part list

INSERT INTO t906_rules (c906_rule_seq_id, c906_rule_id, c906_rule_grp_id, c906_rule_desc, c906_rule_value, c906_created_date,
    c906_created_by, c901_rule_type, c906_active_fl, c906_void_fl ) 
 VALUES ( s906_rule.NEXTVAL, 'UDI_PART_PROD_FAMILY', 'UDI_SHOW_PARTS', 'UDI_PART_PROD_FAMILY', '26240095', current_date, '303510',
    NULL, NULL, NULL );
    
INSERT INTO t906_rules (c906_rule_seq_id, c906_rule_id, c906_rule_grp_id, c906_rule_desc, c906_rule_value, c906_created_date,
    c906_created_by, c901_rule_type, c906_active_fl, c906_void_fl ) 
 VALUES ( s906_rule.NEXTVAL, 'UDI_PART_PROD_FAMILY', 'UDI_SHOW_PARTS', 'UDI_PART_PROD_FAMILY', '4052', current_date, '303510',
    NULL, NULL, NULL );

INSERT INTO t906_rules (c906_rule_seq_id, c906_rule_id, c906_rule_grp_id, c906_rule_desc, c906_rule_value, c906_created_date,
    c906_created_by, c901_rule_type, c906_active_fl, c906_void_fl ) 
 VALUES ( s906_rule.NEXTVAL, 'UDI_PART_PROD_FAMILY', 'UDI_SHOW_PARTS', 'UDI_PART_PROD_FAMILY', '4051', current_date, '303510',
    NULL, NULL, NULL );

INSERT INTO t906_rules (c906_rule_seq_id, c906_rule_id, c906_rule_grp_id, c906_rule_desc, c906_rule_value, c906_created_date,
    c906_created_by, c901_rule_type, c906_active_fl, c906_void_fl ) 
 VALUES ( s906_rule.NEXTVAL, 'UDI_PART_PROD_FAMILY', 'UDI_SHOW_PARTS', 'UDI_PART_PROD_FAMILY', '4050', current_date, '303510',
    NULL, NULL, NULL );