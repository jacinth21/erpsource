--Create left link for Vendor Work Order Lead Time Report
INSERT INTO t1520_function_list 
(C1520_FUNCTION_ID,
C1520_FUNCTION_NM,
C1520_FUNCTION_DESC,
C901_TYPE,
C1520_CREATED_BY,
C1520_CREATED_DATE,
C1520_REQUEST_URI,
C1520_REQUEST_STROPT,
C901_DEPARTMENT_ID,
C901_LEFT_MENU_TYPE) 
VALUES 
('VENDOR_LEADTIME_RPT',
'Vendor Lead Time Report',
'This screen is used to show the vendor lead time based on part',
92262,
'2765071',
current_date,
'/gmVendorWOLaedTimeRpt.do?method=loadVendorWOLeadTimeRpt',
'',
2009,101921);

--Create synonym for new package
create or replace public synonym gm_pkg_wo_lead_time_rpt for GLOBUS_APP.gm_pkg_wo_lead_time_rpt;