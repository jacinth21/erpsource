
DELETE FROM T906_Rules WHERE C906_Rule_Grp_Id='CUT_PLAN_BBA';
INSERT INTO T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By, C906_Void_Fl)
VALUES (S906_Rule.Nextval,'EMAIL_TO','Cut Plan Email to','amuzumdar@bonebank.com,rblanquiz@bonebank.com,kodare@bonebank.com,jmcdougall@bonebank.com,tdavis@bonebank.com,dmcknight@bonebank.com,ccline@bonebank.com,gpalani@bonebank.com'
,CURRENT_DATE,'CUT_PLAN_BBA','2277820',Null);

INSERT INTO T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By, C906_Void_Fl,c906_active_fl)
VALUES (S906_Rule.Nextval,'EMAIL_SUCCESS_SUB','Cut Plan Email Subject','Cut Plan Lock and generate : Success'
,CURRENT_DATE,'CUT_PLAN_BBA','2277820',Null,'Y');

INSERT INTO T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By, C906_Void_Fl,c906_active_fl)
VALUES (S906_Rule.Nextval,'EMAIL_ERROR_SUB','Cut Plan Email Subject','Cut Plan Lock and generate:  Failure'
,CURRENT_DATE,'CUT_PLAN_BBA','2277820',Null,'Y');

INSERT INTO T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By, C906_Void_Fl,c906_active_fl)
VALUES (S906_Rule.Nextval,'EMAIL_SUCCESS_MSG','Cut Plan Email Message','Cut Plan Lock Generated Successfully for the current week. Visit Cut Plan report to view details.'
,CURRENT_DATE,'CUT_PLAN_BBA','2277820',Null,'Y');

INSERT INTO T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By, C906_Void_Fl,c906_active_fl)
VALUES (S906_Rule.Nextval,'EMAIL_ERROR_MSG','Cut Plan Email Message','Cut Plan Lock failed for the current week. Contact IT to find out the cause '
,CURRENT_DATE,'CUT_PLAN_BBA','2277820',Null,'Y');