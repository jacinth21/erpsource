--Create left link for Vendor Work Order Lead Time Report
INSERT INTO t1520_function_list 
(C1520_FUNCTION_ID,
C1520_FUNCTION_NM,
C1520_FUNCTION_DESC,
C901_TYPE,
C1520_CREATED_BY,
C1520_CREATED_DATE,
C1520_REQUEST_URI,
C1520_REQUEST_STROPT,
C901_DEPARTMENT_ID,
C901_LEFT_MENU_TYPE) 
VALUES 
('WO_ACKDATE_UPLOAD',
'WO Ack Date Bulk Upload',
'This screen is used to upload Acknowledge date of work order',
92262,
'2765071',
current_date,
'/gmWOAckDateBulkUpload.do?method=loadWOAckDateUpload',
'',
2009,101921);