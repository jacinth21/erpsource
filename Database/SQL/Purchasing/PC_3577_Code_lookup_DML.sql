--insert code id for WO Due date
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(26241178,'WO Due','VPRMAI','0','','','','2765071',CURRENT_DATE);
    
  --insert rule for WO Due date update notification
INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY)
VALUES (S906_RULE.NEXTVAL,'26241178','Work Order Due','GmVendorWODueEmail',CURRENT_DATE,'VENDOR_EXT','2765071');