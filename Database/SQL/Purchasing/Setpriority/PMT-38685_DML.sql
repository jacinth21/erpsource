--Button acces for TTP View By Vendor DashBoard Screen
   INSERT
   INTO t1520_function_list
    (
        C1520_FUNCTION_ID, C1520_FUNCTION_NM, C1520_FUNCTION_DESC,
        C1520_INHERITED_FROM_ID, C901_TYPE, C1520_VOID_FL,
        C1520_CREATED_BY, C1520_CREATED_DATE, C1520_LAST_UPDATED_BY,
        C1520_LAST_UPDATED_DATE, C1520_REQUEST_URI, C1520_REQUEST_STROPT,
        C1520_SEQ_NO, C901_DEPARTMENT_ID
    )    VALUES
    (
        'TTP_BY_VENDOR_LCK_AC', 'TTP Vendor DashBoard Access', 
        'Enable/disable the lock button',
        NULL, 92267, NULL,
        '706322', SYSDATE, NULL,
        NULL, NULL, NULL,
        NULL, NULL
    ) ;

--create left link for Dashboard screen
INSERT
   INTO t1520_function_list
    (
        C1520_FUNCTION_ID, C1520_FUNCTION_NM, C1520_FUNCTION_DESC
      , C1520_INHERITED_FROM_ID, C901_TYPE, C1520_VOID_FL
      , C1520_CREATED_BY, C1520_CREATED_DATE, C1520_LAST_UPDATED_BY
      , C1520_LAST_UPDATED_DATE, C1520_REQUEST_URI, C1520_REQUEST_STROPT
      , C1520_SEQ_NO, C901_DEPARTMENT_ID
    )
    VALUES
    (
        'TTP_VENDOR_DASH', 'Dashboard', ' view the TTP By Vendor Dashboard to see which Sheets are ready to have the Vendor Split reviewed'
      , NULL, 92262, NULL
      , '706322', SYSDATE, NULL
      , NULL, '/gmTTPbyVendorDashboard.do?method=loadTTPbyVendorDashboard', null
      , NULL, 2000
    ) ;

--
INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'TTPCST','TTP Capacity Vendor - Status','','706322',CURRENT_DATE);

--  
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108860,'Open','TTPCST','1','1','','','706322',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108861,'Approve In-Progress','TTPCST','1','2','','','706322',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108862,'Approved','TTPCST','1','3','','','706322',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108863,'Lock In-Progress','TTPCST','1','4','','','706322',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108864,'Lock Failed','TTPCST','1','5','','','706322',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108865,'Locked','TTPCST','1','6','','','706322',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108866,'TBE','TTPCST','0','7','','','706322',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108867,'TBE','TTPCST','0','8','','','706322',CURRENT_DATE);
 
-- Capacity Status
UPDATE T901_CODE_LOOKUP SET C901_CODE_GRP = 'TTPCST',C901_CODE_NM ='--',C901_ACTIVE_FL = '1',C901_CODE_SEQ_NO='1', C901_LAST_UPDATED_BY= 706322, C901_LAST_UPDATED_DATE = SYSDATE WHERE c901_code_id = 108866;
UPDATE T901_CODE_LOOKUP SET C901_CODE_GRP = 'TTPCST',C901_CODE_NM ='Open', C901_ACTIVE_FL ='1',C901_CODE_SEQ_NO='2',C901_LAST_UPDATED_BY= 706322, C901_LAST_UPDATED_DATE = SYSDATE WHERE c901_code_id = 108867;
UPDATE T901_CODE_LOOKUP SET C901_CODE_GRP = 'TTPCST', C901_CODE_SEQ_NO='3',C901_LAST_UPDATED_BY= 706322, C901_LAST_UPDATED_DATE = SYSDATE WHERE c901_code_id = 108863;
UPDATE T901_CODE_LOOKUP SET C901_CODE_GRP = 'TTPCST', C901_CODE_SEQ_NO='4',C901_LAST_UPDATED_BY= 706322, C901_LAST_UPDATED_DATE = SYSDATE WHERE c901_code_id = 108864;
UPDATE T901_CODE_LOOKUP SET C901_CODE_GRP = 'TTPCST',C901_CODE_SEQ_NO='5' ,C901_LAST_UPDATED_BY= 706322, C901_LAST_UPDATED_DATE = SYSDATE WHERE c901_code_id = 108865;

--
INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'TTPVST','TTP Vendor - Status','','706322',CURRENT_DATE);
-- Vendor Status
Update T901_CODE_LOOKUP set C901_CODE_GRP = 'TTPVST', C901_LAST_UPDATED_BY= 706322, C901_LAST_UPDATED_DATE = SYSDATE where c901_code_id = 108860;
Update T901_CODE_LOOKUP set C901_CODE_GRP = 'TTPVST', C901_ACTIVE_FL =0, C901_LAST_UPDATED_BY= 706322, C901_LAST_UPDATED_DATE = SYSDATE where c901_code_id = 108861;
Update T901_CODE_LOOKUP set C901_CODE_GRP = 'TTPVST', C901_LAST_UPDATED_BY= 706322, C901_LAST_UPDATED_DATE = SYSDATE where c901_code_id = 108862;