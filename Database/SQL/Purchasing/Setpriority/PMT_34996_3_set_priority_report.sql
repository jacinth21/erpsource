-- Insert Left Link for Set Priority Report
INSERT INTO T1520_FUNCTION_LIST
  (
    C1520_FUNCTION_ID,
    C1520_FUNCTION_NM,
    C1520_FUNCTION_DESC,
    C1520_INHERITED_FROM_ID,
    C901_TYPE,
    C1520_VOID_FL,
    C1520_CREATED_BY,
    C1520_CREATED_DATE,
    C1520_LAST_UPDATED_BY,
    C1520_LAST_UPDATED_DATE,
    C1520_REQUEST_URI,
    C1520_REQUEST_STROPT,
    C1520_SEQ_NO,
    C901_DEPARTMENT_ID,
    C901_LEFT_MENU_TYPE
  )
  VALUES
  (
    'SET_PRIORITY_REPORT',
    'Set Priority Report',
    'This is used to to view and update the Set Priority details',
    NULL,
    92262,
    NULL,
    '303510',
    SYSDATE,
    '303510',
    NULL,
    '/gmSetPriorityReport.do?method=loadSetPriorityReports',
    NULL,
    NULL,
    '2024',
    '101920'
  );