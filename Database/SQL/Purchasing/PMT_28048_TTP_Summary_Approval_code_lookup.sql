--Create Code Group For Category DropDown
INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'TTPCAT','TTP Category','','706322',CURRENT_DATE);
 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(107820,'Maintenance','TTPCAT','1','1','','','706322',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(107821,'Launch','TTPCAT','1','2','','','706322',CURRENT_DATE); 

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(107822,'Multipart','TTPCAT','1','3','','','2598779',CURRENT_DATE);



--Update CodeGroup TTPST
     UPDATE T901_CODE_LOOKUP
     SET C901_CODE_NM='Open',
     C901_LAST_UPDATED_BY=303012,
     C901_LAST_UPDATED_DATE=CURRENT_DATE
     WHERE C901_CODE_GRP='TTPST'
     AND C901_CODE_ID=50580;
     
        UPDATE T901_CODE_LOOKUP
     SET C901_CODE_NM='InProgress',
     C901_LAST_UPDATED_BY=303012,
     C901_LAST_UPDATED_DATE=CURRENT_DATE
     WHERE C901_CODE_GRP='TTPST'
     AND C901_CODE_ID=50581;
     
     UPDATE T901_CODE_LOOKUP
     SET C901_CODE_NM='Approved',
     C901_LAST_UPDATED_BY=303012,
     C901_LAST_UPDATED_DATE=CURRENT_DATE
     WHERE C901_CODE_GRP='TTPST'
     AND C901_CODE_ID=50582;
--Inset New Failed Dropdown in TTPST Code Group   
 INSERT INTO T901_CODE_LOOKUP
(C901_CODE_ID, C901_CODE_NM, C901_CODE_GRP,C901_ACTIVE_FL, C901_CODE_SEQ_NO, C902_CODE_NM_ALT, C901_CONTROL_TYPE, C901_CREATED_BY, C901_CREATED_DATE)
VALUES 
(50583,'Failed','TTPST','1','','','','706322',CURRENT_DATE);

