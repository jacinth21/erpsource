 --Inset New Lock and Generate Dropdown in TTPST Code Group   
INSERT INTO t901_code_lookup (
  c901_code_id, c901_code_nm, c901_code_grp, 
  c901_active_fl, c901_code_seq_no, 
  c902_code_nm_alt, c901_control_type, 
  c901_created_by, c901_created_date
) 
VALUES 
  (
    26240573, 'Lock In-Progress', 'TTPST', 
    '1', '', '', '', '706322', CURRENT_DATE
  );
  
  INSERT INTO t901_code_lookup (
  c901_code_id, c901_code_nm, c901_code_grp, 
  c901_active_fl, c901_code_seq_no, 
  c902_code_nm_alt, c901_control_type, 
  c901_created_by, c901_created_date
) 
VALUES 
  (
    26240574, 'Locked', 'TTPST', '1', '', 
    '', '', '706322', CURRENT_DATE
  );
 
 INSERT INTO t901_code_lookup (
  c901_code_id, c901_code_nm, c901_code_grp, 
  c901_active_fl, c901_code_seq_no, 
  c902_code_nm_alt, c901_control_type, 
  c901_created_by, c901_created_date
) 
VALUES 
  (
    26240575, 'Lock Failed', 'TTPST', 
    '1', '', '', '', '706322', CURRENT_DATE
  );
--Update c901_code_nm values for c901_code_id 50581,50582
 --50581
update 
  t901_code_lookup 
set 
  c901_code_nm = 'Approve In-Progress', 
  c901_last_updated_by = 303012, 
  c901_last_updated_date = current_date 
where 
  c901_code_id = 50581;
  
 --50583 
update 
  t901_code_lookup 
set 
  c901_code_nm = 'Approve Failed', 
  c901_last_updated_by = 303012, 
  c901_last_updated_date = current_date 
where 
  c901_code_id = 50583;