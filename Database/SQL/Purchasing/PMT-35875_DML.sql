/*
--In Vendor PO Report Screen Choose Action Dropdown Values
-- Code Group VPOAVT Insert for Vendor PO Action
Delete from t901b_code_group_master WHERE C901B_CODE_GRP='VPOAVT';
INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
values
(s901b_code_group_master.NEXTVAL,'VPOAVT','Vendor PO Action','','2765071',CURRENT_DATE);

-- Code Lookup Values for VPOAVT code group
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
values
(108562,'Remove from Hold Status','VPOAVT','0','3','','','2765071',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
values
(108561,'Move to Hold Status','VPOAVT','0','2','','','2765071',current_date);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
values
(108560,'Publish PO','VPOAVT','0','1','','','2765071',CURRENT_DATE);


-- Code Lookup Insert for Cancel type Code Group CNCLT and code name alt VDMVHS
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(26240634,'Move Purchase Order to Hold Status','CNCLT','0','','VDMVHS','','2765071',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(26240635,'Remove Purchase Order from Hold Status','CNCLT','0','','VDRFHS','','2765071',CURRENT_DATE);

-- Common cancel screen list cancel reason for the type Vendor Portal move to hold status
-- Create code group VDMVHS 
Delete from t901b_code_group_master WHERE C901B_CODE_GRP='VDMVHS';
INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES
(s901b_code_group_master.NEXTVAL,'VDMVHS','Vendor Portal move to hold status','','2765071',CURRENT_DATE);

-- Code Lookup values for the code group VDMVHS
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(108540,'Reviewing Pricing','VDMVHS','0','1','','','2765071',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(108541,'Reviewing Rev Level','VDMVHS','0','2','','','2765071',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(108542,'Other','VDMVHS','0','3','','','2765071',CURRENT_DATE);


-- Common cancel screen list cancel reason for the type Vendor Portal remove from  hold status
-- Create code group VDRFHS 
Delete from t901b_code_group_master WHERE C901B_CODE_GRP='VDRFHS';
INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES
(s901b_code_group_master.NEXTVAL,'VDRFHS','vendor portal remove from hold status','','2765071',CURRENT_DATE);

-- Code Lookup values for the code group VDRFHS
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(108548,'Other','VDRFHS','0','4','','','2765071',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(108547,'Globus Cancelled','VDRFHS','0','3','','','2765071',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(108546,'Vendor Rejected','VDRFHS','0','2','','','2765071',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(108545,'Vendor Accepted','VDRFHS','0','1','','','2765071',CURRENT_DATE);
*/

Delete from  T906_RULES Where c906_rule_id IN ('26240634','26240635');
--Rule Table Insert Values

--Move PO to Hold Status
INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by,c906_last_updated_date,c906_last_updated_by,c901_rule_type,c906_active_fl,c906_void_fl,c1900_company_id,c906_last_updated_date_utc)
VALUES (S906_RULE.NEXTVAL,'26240634','Move PO to Hold Status','gm_pkg_vendor_po_report.gm_sav_vendor_po_hold',sysdate,'CMNCNCL','2765071',null,null,null,null,null,null,null);
    
INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by,c906_last_updated_date,c906_last_updated_by,c901_rule_type,c906_active_fl,c906_void_fl,c1900_company_id,c906_last_updated_date_utc)
VALUES (S906_RULE.NEXTVAL,'26240634','Move PO to Hold Status','6',sysdate,'CMNCLPM','2765071',null,null,null,null,null,null,null); -- for cancel parameter

--Remove PO From Hold Status
INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by,c906_last_updated_date,c906_last_updated_by,c901_rule_type,c906_active_fl,c906_void_fl,c1900_company_id,c906_last_updated_date_utc)
VALUES (S906_RULE.NEXTVAL,'26240635','Remove PO From Hold Status','gm_pkg_vendor_po_report.gm_sav_vendor_po_unhold',sysdate,'CMNCNCL','2765071',null,null,null,null,null,null,null);
    
INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by,c906_last_updated_date,c906_last_updated_by,c901_rule_type,c906_active_fl,c906_void_fl,c1900_company_id,c906_last_updated_date_utc)
VALUES (S906_RULE.NEXTVAL,'26240635','Remove PO From Hold Status','6',sysdate,'CMNCLPM','2765071',null,null,null,null,null,null,null); -- for cancel parameter

Delete from T1520_FUNCTION_LIST WHERE C1520_FUNCTION_NM='Vendor Portal PO Report';
-- Create new left link as Vendor PO Report
insert into T1520_FUNCTION_LIST 
(C1520_FUNCTION_ID,C1520_FUNCTION_NM,C1520_FUNCTION_DESC,C1520_INHERITED_FROM_ID,C901_TYPE,C1520_VOID_FL,C1520_CREATED_BY,C1520_CREATED_DATE,C1520_LAST_UPDATED_BY,C1520_LAST_UPDATED_DATE,C1520_REQUEST_URI,C1520_REQUEST_STROPT,C1520_SEQ_NO,C901_DEPARTMENT_ID,C901_LEFT_MENU_TYPE,C1520_LAST_UPDATED_DATE_UTC) 
values 
(S1520_FUNCTION_LIST_TASK.nextval,'Vendor Portal PO Report','Summary of all raised Vendor POs with ability to edit or void POs if necessary.',null,92262,null,'2765071',CURRENT_DATE,null,null,'/gmVendorPOReport.do?method=loadVendorPOReport','Vendor',null,2010,101921,null);

--Create security event for Hold Access
INSERT INTO t1520_function_list 
( C1520_FUNCTION_ID, C1520_FUNCTION_NM, C1520_FUNCTION_DESC, C1520_INHERITED_FROM_ID,  C901_TYPE, C1520_CREATED_BY, C1520_CREATED_DATE )
VALUES
( 'PO_HOLD_GRP', 'PO_HOLD_ACCESS', 'Users in this security event will have access to Hold/Unhold PO.', NULL, 92267, '2765071', current_date );
