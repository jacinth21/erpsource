-- create synonym for new package gm_pkg_purchase_activity
create or replace public synonym gm_pkg_purchase_activity for GLOBUS_APP.gm_pkg_purchase_activity;

-- create synonym for new package trg_t401_po_activity_log 
create or replace public synonym trg_t401_po_activity_log for GLOBUS_APP.trg_t401_po_activity_log;

-- create synonym for new package trg_t402_wo_activity_log 
create or replace public synonym trg_t402_wo_activity_log for GLOBUS_APP.trg_t402_wo_activity_log;
