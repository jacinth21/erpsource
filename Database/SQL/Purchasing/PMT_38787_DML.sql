--To create folder for TTP By Vendor Under GOP module report
INSERT INTO T1520_FUNCTION_LIST
  (C1520_FUNCTION_ID,
    C1520_FUNCTION_NM,
    C1520_FUNCTION_DESC ,
    C1520_INHERITED_FROM_ID,
    C901_TYPE,
    C1520_VOID_FL ,
    C1520_CREATED_BY,
    C1520_CREATED_DATE,
    C1520_LAST_UPDATED_BY ,
    C1520_LAST_UPDATED_DATE,
    C1520_REQUEST_URI,
    C1520_REQUEST_STROPT ,
    C1520_SEQ_NO,
    C901_DEPARTMENT_ID
  )
  VALUES
  (
    'TTP_BY_VENDOR_RPT',
    'TTP By Vendor',
    'TTP By Vendor rpt folder' ,
    NULL,
    92262,
    NULL ,
    '706322',
    SYSDATE,
    NULL ,
    NULL,
    NULL,
    NULL ,
    NULL,
    2000
  );
  
  INSERT INTO T1530_ACCESS
  (
    C1530_ACCESS_ID,
    C1500_GROUP_ID,
    C1520_FUNCTION_ID ,
    C101_PARTY_ID,
    C1530_UPDATE_ACCESS_FL,
    C1530_READ_ACCESS_FL ,
    C1530_VOID_ACCESS_FL,
    C1530_VOID_FL,
    C1530_CREATED_BY ,
    C1530_CREATED_DATE,
    C1530_LAST_UPDATED_BY,
    C1530_LAST_UPDATED_DATE ,
    C1520_PARENT_FUNCTION_ID,
    C1530_FUNCTION_NM,
    C1530_SEQ_NO
  )
  VALUES
  (
    s1530_access.nextval,
    '100030',
    'TTP_BY_VENDOR_RPT' ,
    NULL,
    'Y',
    'Y' ,
    NULL,
    NULL,
    NULL ,
    NULL,
    NULL,
    NULL ,
    '12274000',
    NULL,
    2002471000008974
  );
  
--Left link query for Vendor TTP Period Summary Report screen
INSERT
   INTO t1520_function_list
    (
        C1520_FUNCTION_ID, C1520_FUNCTION_NM, C1520_FUNCTION_DESC
      , C1520_INHERITED_FROM_ID, C901_TYPE, C1520_VOID_FL
      , C1520_CREATED_BY, C1520_CREATED_DATE, C1520_LAST_UPDATED_BY
      , C1520_LAST_UPDATED_DATE, C1520_REQUEST_URI, C1520_REQUEST_STROPT
      , C1520_SEQ_NO, C901_DEPARTMENT_ID
    )
    VALUES
    (
        'TTP_VENDOR_PERIODRPT', 'Vendor TTP Period Summary Report', 'load Vendor TTP Period Summary Report'
      , NULL, 92262, NULL
      , '706322', SYSDATE, NULL
      , NULL, '/gmTTPbyVendorReport.do?method=loadTTPbyVendorSummary', null
      , NULL, 2000
    ) ;