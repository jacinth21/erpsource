--Create left link for Vendor Work Order Lead Time Report
INSERT INTO t1520_function_list 
(C1520_FUNCTION_ID,
C1520_FUNCTION_NM,
C1520_FUNCTION_DESC,
C901_TYPE,
C1520_CREATED_BY,
C1520_CREATED_DATE,
C1520_REQUEST_URI,
C1520_REQUEST_STROPT,
C901_DEPARTMENT_ID,
C901_LEFT_MENU_TYPE) 
VALUES 
('VENDOR_ACK_REPORT',
'Vendor Acknowledge Report',
'This screen is used to show vendor acknowledge report',
92262,
'2765071',
current_date,
'/gmVendorAckReport.do?method=loadVendorAckReport',
'',
2009,101920);