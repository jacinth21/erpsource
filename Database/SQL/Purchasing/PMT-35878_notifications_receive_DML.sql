--Get Email template name and vm file name from t906_rule table
--Receive shipment email template
INSERT
INTO T906_RULES
  (
    C906_RULE_SEQ_ID,
    C906_RULE_ID,
    C906_RULE_DESC,
    C906_RULE_VALUE,
    C906_CREATED_DATE,
    C906_RULE_GRP_ID,
    C906_CREATED_BY
  )
  VALUES
  (
    S906_RULE.NEXTVAL,
    '108407',
    'Shipment Accepted',
    'GmVendorDHREmailShipment',
    sysdate,
    'VENDOR_EXT',
    '2765071'
  );
  --Crossed Inspection Email Template
INSERT
INTO T906_RULES
  (
    C906_RULE_SEQ_ID,
    C906_RULE_ID,
    C906_RULE_DESC,
    C906_RULE_VALUE,
    C906_CREATED_DATE,
    C906_RULE_GRP_ID,
    C906_CREATED_BY
  )
  VALUES
  (
    S906_RULE.NEXTVAL,
    '108408',
    'DHR Crossed Inspection',
    'GmVendorDHREmailInspect',
    sysdate,
    'VENDOR_EXT',
    '2765071'
  );
  --Closed purchase order email template
INSERT
INTO T906_RULES
  (
    C906_RULE_SEQ_ID,
    C906_RULE_ID,
    C906_RULE_DESC,
    C906_RULE_VALUE,
    C906_CREATED_DATE,
    C906_RULE_GRP_ID,
    C906_CREATED_BY
  )
  VALUES
  (
    S906_RULE.NEXTVAL,
    '108409',
    'Closed Purchase Order',
    'GmVendorDHREmailClosed',
    sysdate,
    'VENDOR_EXT',
    '2765071'
  );

--VM file name
INSERT
INTO T906_RULES
  (
    C906_RULE_SEQ_ID,
    C906_RULE_ID,
    C906_RULE_DESC,
    C906_RULE_VALUE,
    C906_CREATED_DATE,
    C906_RULE_GRP_ID,
    C906_CREATED_BY
  )
  VALUES
  (
    S906_RULE.NEXTVAL,
    '108442',
    'DHR',
    'GmVendorDHRReport.vm',
    sysdate,
    'VENDOR_EXT',
    '2765071'
  );