--Left Link query for TTP By Vendor Part Details
INSERT INTO T1520_FUNCTION_LIST
  (
    C1520_FUNCTION_ID,
    C1520_FUNCTION_NM,
    C1520_FUNCTION_DESC,
    C1520_INHERITED_FROM_ID,
    C901_TYPE,
    C1520_VOID_FL,
    C1520_CREATED_BY,
    C1520_CREATED_DATE,
    C1520_LAST_UPDATED_BY,
    C1520_LAST_UPDATED_DATE,
    C1520_REQUEST_URI,
    C1520_REQUEST_STROPT,
    C1520_SEQ_NO,
    C901_DEPARTMENT_ID,
    C901_LEFT_MENU_TYPE,
    C1520_LAST_UPDATED_DATE_UTC
  )
  VALUES
  (
    'TTP_VENDOR_PART_DTL',
    'Vendor TTP Period Detail Report',
    'Viewing the TTP By Vendor Part Details',
    NULL,
    92262,
    NULL,
    '303510',
    CURRENT_DATE,
    '303510',
    CURRENT_DATE,
    '/gmTTPByVendorPartDetails.do?method=loadTTPbyVendorPartDetails',
    NULL,
    NULL,
    2000,
    101920,
    NULL
  );