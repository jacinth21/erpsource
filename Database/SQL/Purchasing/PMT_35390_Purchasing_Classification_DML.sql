--Create Left Link For A/B/C Classification Report
INSERT
   INTO t1520_function_list
    (
        C1520_FUNCTION_ID, C1520_FUNCTION_NM, C1520_FUNCTION_DESC
      , C1520_INHERITED_FROM_ID, C901_TYPE, C1520_VOID_FL
      , C1520_CREATED_BY, C1520_CREATED_DATE, C1520_LAST_UPDATED_BY
      , C1520_LAST_UPDATED_DATE, C1520_REQUEST_URI, C1520_REQUEST_STROPT
      , C1520_SEQ_NO, C901_DEPARTMENT_ID
    )
    VALUES
    (
        'PUR_CLASS_RPT', 'Purchasing Classification Report', 'To see Purchasing Classification Report'
      , NULL, 92262, NULL
      , '706322', SYSDATE, NULL
      , NULL, '/gmPurchaseClassificationRpt.do?method=loadPartClassificationRpt', NULL
      , NULL, 2000
    ) ;
    
    
