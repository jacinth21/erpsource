--Create Left Link For Vendor MOQ Bulk Upload Screen
INSERT
   INTO t1520_function_list
    (
        C1520_FUNCTION_ID, C1520_FUNCTION_NM, C1520_FUNCTION_DESC
      , C1520_INHERITED_FROM_ID, C901_TYPE, C1520_VOID_FL
      , C1520_CREATED_BY, C1520_CREATED_DATE, C1520_LAST_UPDATED_BY
      , C1520_LAST_UPDATED_DATE, C1520_REQUEST_URI, C1520_REQUEST_STROPT
      , C1520_SEQ_NO, C901_DEPARTMENT_ID
    )
    VALUES
    (
        'VEN_MOQ_BULK_UPL', 'Vendor MOQ Upload', 'load the MOQ Bulk Upload screen for Vendor.'
      , NULL, 92262, NULL
      , '706322', SYSDATE, NULL
      , NULL, '/gmTTPbyVendorMoq.do?method=loadTTPbyVendorMOQBulk', null
      , NULL, 2000
    ) ;
    
--

--Create Code Id for Save Log Comments
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(26240845,'Vendor MOQ Upload','GPLOG','1','','','','706322',CURRENT_DATE);
    
    
    
---- Create New Security Event For Submit Button Access for vendor MOQ Upload save Button
    
INSERT INTO t1520_function_list (
    c1520_function_id,
    c1520_function_nm,
    c1520_function_desc,
    c1520_inherited_from_id,
    c901_type,
    c1520_void_fl,
    c1520_created_by,
    c1520_created_date,
    c1520_last_updated_by,
    c1520_last_updated_date,
    c1520_request_uri,
    c1520_request_stropt,
    c1520_seq_no,
    c901_department_id
) VALUES (
    'VEN_MOQ_SAV_BTN_ACC',
    'Vendor Cost Bulk Upload Screen save Button Access',
    'Vendor Cost Bulk Upload Screen save Button Access',
    NULL,
    92267,
    NULL,
    '706322',
    sysdate,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
);  

--
INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        s906_rule.nextval, 'MAX_DATA', 'To get the max. record to process the vendor moq bulk upload'
      , '1000', SYSDATE, 'MOQ_DATA_SETUP'
      , 706322
    ) ;