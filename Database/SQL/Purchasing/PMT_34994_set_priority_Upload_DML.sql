
-- create synonym for new package gm_pkg_op_set_priority_trans
create or replace public synonym gm_pkg_op_set_priority_trans for GLOBUS_APP.gm_pkg_op_set_priority_trans;
-- create synonym for new package  gm_pkg_op_set_priority_report
create or replace public synonym gm_pkg_op_set_priority_report for GLOBUS_APP.gm_pkg_op_set_priority_report;


--SET_PRIORITY_RPT_FLD - Report
INSERT
INTO T1520_FUNCTION_LIST
  (
    C1520_FUNCTION_ID,
    C1520_FUNCTION_NM,
    C1520_FUNCTION_DESC ,
    C1520_INHERITED_FROM_ID,
    C901_TYPE,
    C1520_VOID_FL ,
    C1520_CREATED_BY,
    C1520_CREATED_DATE,
    C1520_LAST_UPDATED_BY ,
    C1520_LAST_UPDATED_DATE,
    C1520_REQUEST_URI,
    C1520_REQUEST_STROPT ,
    C1520_SEQ_NO,
    C901_DEPARTMENT_ID
  )
  VALUES
  (
    'SET_PRIORITY_RPT_FLD',
    'Set Priority',
    'Set Priority folder' ,
    NULL,
    92262,
    NULL ,
    '706322',
    SYSDATE,
    NULL ,
    NULL,
    NULL,
    NULL ,
    NULL,
    2000
  );
INSERT
INTO T1530_ACCESS
  (
    C1530_ACCESS_ID,
    C1500_GROUP_ID,
    C1520_FUNCTION_ID ,
    C101_PARTY_ID,
    C1530_UPDATE_ACCESS_FL,
    C1530_READ_ACCESS_FL ,
    C1530_VOID_ACCESS_FL,
    C1530_VOID_FL,
    C1530_CREATED_BY ,
    C1530_CREATED_DATE,
    C1530_LAST_UPDATED_BY,
    C1530_LAST_UPDATED_DATE ,
    C1520_PARENT_FUNCTION_ID,
    C1530_FUNCTION_NM,
    C1530_SEQ_NO
  )
  VALUES
  (
    s1530_access.nextval,
    '100030',
    'SET_PRIORITY_RPT_FLD' ,
    NULL,
    'Y',
    'Y' ,
    NULL,
    NULL,
    NULL ,
    NULL,
    NULL,
    NULL ,
    '12274000',
    NULL,
    2002471000008972
  );
--SET_PRIORITY_FLD - transactions
INSERT
INTO T1520_FUNCTION_LIST
  (
    C1520_FUNCTION_ID,
    C1520_FUNCTION_NM,
    C1520_FUNCTION_DESC ,
    C1520_INHERITED_FROM_ID,
    C901_TYPE,
    C1520_VOID_FL ,
    C1520_CREATED_BY,
    C1520_CREATED_DATE,
    C1520_LAST_UPDATED_BY ,
    C1520_LAST_UPDATED_DATE,
    C1520_REQUEST_URI,
    C1520_REQUEST_STROPT ,
    C1520_SEQ_NO,
    C901_DEPARTMENT_ID
  )
  VALUES
  (
    'SET_PRIORITY_FLD',
    'Set Priority',
    'Set Priority folder' ,
    NULL,
    92262,
    NULL ,
    '706322',
    SYSDATE,
    NULL ,
    NULL,
    NULL,
    NULL ,
    NULL,
    2000
  );
  
  
INSERT
INTO T1530_ACCESS
  (
    C1530_ACCESS_ID,
    C1500_GROUP_ID,
    C1520_FUNCTION_ID ,
    C101_PARTY_ID,
    C1530_UPDATE_ACCESS_FL,
    C1530_READ_ACCESS_FL ,
    C1530_VOID_ACCESS_FL,
    C1530_VOID_FL,
    C1530_CREATED_BY ,
    C1530_CREATED_DATE,
    C1530_LAST_UPDATED_BY,
    C1530_LAST_UPDATED_DATE ,
    C1520_PARENT_FUNCTION_ID,
    C1530_FUNCTION_NM,
    C1530_SEQ_NO
  )
  VALUES
  (
    s1530_access.nextval,
    '100030',
    'SET_PRIORITY_FLD' ,
    NULL,
    'Y',
    'Y' ,
    NULL,
    NULL,
    NULL ,
    NULL,
    NULL,
    NULL ,
    '12273900',
    NULL,
    2002471000008879
  );
  


--Create Left Link For Set Priority upload screen
INSERT
   INTO t1520_function_list
    (
        C1520_FUNCTION_ID, C1520_FUNCTION_NM, C1520_FUNCTION_DESC
      , C1520_INHERITED_FROM_ID, C901_TYPE, C1520_VOID_FL
      , C1520_CREATED_BY, C1520_CREATED_DATE, C1520_LAST_UPDATED_BY
      , C1520_LAST_UPDATED_DATE, C1520_REQUEST_URI, C1520_REQUEST_STROPT
      , C1520_SEQ_NO, C901_DEPARTMENT_ID
    )
    VALUES
    (
        'SET_PRIOR_UPLOAD', 'Set Priority Upload', 'load the Set Priority Upload screen for a Set Priority transaction'
      , NULL, 92262, NULL
      , '706322', SYSDATE, NULL
      , NULL, '/gmSetPriorityUpload.do?method=loadSetPriorityUpload', null
      , NULL, 2000
    ) ;
 

    
    
--Void Button Functionality    
    
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(107602,'Void Set Priority Upload','CNCLT','1','163','VSPUPL','','706322',CURRENT_DATE);

INSERT INTO T906_RULES(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY) values (S906_RULE.NEXTVAL,'107602','Void Set Priority Upload','2',sysdate,'CMNCLPM','706322');
INSERT INTO T906_RULES(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY) values (S906_RULE.NEXTVAL,'107602','Void Set Priority Upload','gm_pkg_op_set_priority_trans.gm_void_set_priority',sysdate,'CMNCNCL','706322');



--Button acces for SetPriorityUploadScreen
   INSERT
   INTO t1520_function_list
    (
        C1520_FUNCTION_ID, C1520_FUNCTION_NM, C1520_FUNCTION_DESC,
        C1520_INHERITED_FROM_ID, C901_TYPE, C1520_VOID_FL,
        C1520_CREATED_BY, C1520_CREATED_DATE, C1520_LAST_UPDATED_BY,
        C1520_LAST_UPDATED_DATE, C1520_REQUEST_URI, C1520_REQUEST_STROPT,
        C1520_SEQ_NO, C901_DEPARTMENT_ID
    )    VALUES
    (
        'SET_UPL_BTN_ACCESS', 'SetPriority Button Access', 
        'Enable/disable the void/save/allocate buttons',
        NULL, 92267, NULL,
        '706322', SYSDATE, NULL,
        NULL, NULL, NULL,
        NULL, NULL
    ) ;