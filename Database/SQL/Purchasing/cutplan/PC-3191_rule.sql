DELETE FROM T906_Rules WHERE C906_Rule_Grp_Id='CUT_PLAN_PART_BBA';

INSERT INTO T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By, C906_Void_Fl,c906_active_fl)
VALUES (S906_Rule.Nextval,'EMAIL_SUCCESS_SUB','Cut Plan Add new part number Email Subject','Part Number Added to current CutPlan Report'
,CURRENT_DATE,'CUT_PLAN_PART_BBA','2277820',Null,'Y');

INSERT INTO T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By, C906_Void_Fl,c906_active_fl)
VALUES (S906_Rule.Nextval,'EMAIL_SUCCESS_MSG','Cut Plan Email Message','Part Number Added  Successfully for the current week CutPlan Report'
,CURRENT_DATE,'CUT_PLAN_PART_BBA','2277820',Null,'Y');
