-- create synonym for new package gm_pkg_cutplan_txn
create or replace public synonym gm_pkg_cutplan_txn for GLOBUS_APP.gm_pkg_cutplan_txn;

DELETE FROM T906_Rules WHERE C906_Rule_Grp_Id='CUT_PLAN_ANALY_EMAIL';
--PC-2742 - cut plan analysis summary email details rule
INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY)
VALUES (S906_RULE.NEXTVAL,'CUT_PLAN_ANA_EMAIL_TO','Rule group for Cut plan Analysis Summary Mail','gpalani@globusmedical.com,tramasamy@globusmedical.com',CURRENT_DATE,'CUT_PLAN_ANALY_EMAIL','2277820');

INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY)
VALUES (S906_RULE.NEXTVAL,'CUT_PLAN_ANA_SUB','Rule group for Cut plan Analysis Summary Mail','Cut Plan Analysis Summary Report',CURRENT_DATE,'CUT_PLAN_ANALY_EMAIL','2277820');