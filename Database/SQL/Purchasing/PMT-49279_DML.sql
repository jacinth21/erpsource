-- Insert script for mail  during NCMR creation in  code lookup table
-- 26240816 � NCMR creation mail id type

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
('26240816','PO Rejected Qty','VPRMAI','0','','','','2765071',CURRENT_DATE);