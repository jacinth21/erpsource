DECLARE
   CURSOR active_date_cur is
      SELECT c301_vendor_id vid,c301_created_date cdt FROM t301_vendor;
	  
   CURSOR inactive_date_cur is
     select distinct t301.c301_vendor_id vid, t902.C902_CREATED_DATE cdt from t902_log t902,t301_vendor t301 
     where c902_ref_id in (select c101_party_id from t301_vendor where c301_active_fl = 'N') 
     and c902_type=1217 and t902.c902_ref_id =t301.c101_party_id and upper(C902_COMMENTS) like upper('%inactive%') and c301_active_fl = 'N';

BEGIN
  FOR vendor_active_dtl IN active_date_cur
  LOOP
  	update t301_vendor set c301_active_date = vendor_active_dtl.cdt,C301_LAST_UPDATED_BY='2765071',C301_LAST_UPDATED_DATE=current_date where c301_vendor_id = vendor_active_dtl.vid;  
   END LOOP;
   
   FOR vendor_inactive_dtl IN inactive_date_cur
  LOOP
    	update t301_vendor set c301_inactive_date = vendor_inactive_dtl.cdt,C301_LAST_UPDATED_BY='2765071',C301_LAST_UPDATED_DATE=current_date where c301_vendor_id = vendor_inactive_dtl.vid;
   END LOOP;
END;
/