--Create left link for Vendor Work Order Lead Time Report
INSERT INTO t1520_function_list 
(C1520_FUNCTION_ID,
C1520_FUNCTION_NM,
C1520_FUNCTION_DESC,
C901_TYPE,
C1520_CREATED_BY,
C1520_CREATED_DATE,
C1520_REQUEST_URI,
C1520_REQUEST_STROPT,
C901_DEPARTMENT_ID,
C901_LEFT_MENU_TYPE) 
VALUES 
('WO_DUEDATE_UPLOAD',
'WO Due Date Bulk Upload',
'This screen is used to upload due date of work order',
92262,
'2765071',
current_date,
'/gmWODueDateBulkUpload.do?method=loadWODueDateUpload',
'',
2009,101921);

--Create synonym for new package
create or replace public synonym GM_PKG_WO_LEAD_TIME_TXN for GLOBUS_APP.GM_PKG_WO_LEAD_TIME_TXN;