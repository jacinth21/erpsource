--select * From T906_RULES where C906_RULE_GRP_ID = 'MARKETINGCOLL';

 UPDATE t906_rules
SET C906_RULE_VALUE      = 'Globus Medical Inc.<br>Valley Forge Business Center<br>2560 General Armistead Avenue<br>Audubon, PA 19403<br>Phone:610-930-1800', C906_LAST_UPDATED_BY = 706322, C906_LAST_UPDATED_DATE = sysdate
  WHERE C906_RULE_ID     = 'COMPANY_ADDRESS'
    AND C906_RULE_GRP_ID = 'MARKETINGCOLL'
    AND C1900_COMPANY_ID = 1000;


UPDATE t906_rules
SET C906_RULE_VALUE      = 'Globus Medical Inc.<br>Valley Forge Business Center<br>2560 General Armistead Avenue<br>Audubon, PA 19403<br>Phone:610-930-1800', C906_LAST_UPDATED_BY = 706322, C906_LAST_UPDATED_DATE = sysdate
  WHERE C906_RULE_ID     = 'DEFAULT_COMPANY_ADDRESS'
    AND C906_RULE_GRP_ID = 'MARKETINGCOLL';