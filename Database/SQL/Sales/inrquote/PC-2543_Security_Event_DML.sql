INSERT INTO t1520_function_list (c1520_function_id, c1520_function_nm, c1520_inherited_from_id, c1520_function_desc, c901_type, c1520_created_date, c1520_created_by)
  VALUES ('INR_QUOTE_USER','INR_QUOTE_USER','INR_QUOTE_USER','Users in this security event will be used as robotics quotation users', 92267, current_date,'4241740' );
  
INSERT INTO t1520_function_list (c1520_function_id, c1520_function_nm, c1520_inherited_from_id, c1520_function_desc, c901_type, c1520_created_date, c1520_created_by)
  VALUES ('INR_QUOTE_ADMIN','INR_QUOTE_ADMIN','INR_QUOTE_ADMIN','Users in this security event will be used as robotics quotation admins', 92267, current_date,'4241740' );
  
INSERT INTO t1520_function_list (c1520_function_id, c1520_function_nm, c1520_inherited_from_id, c1520_function_desc, c901_type, c1520_created_date, c1520_created_by)
  VALUES ('INR_QUOTE_APPROVER','INR_QUOTE_APPROVER','INR_QUOTE_APPROVER','Users in this security event will be used as robotics quotation approvers', 92267, current_date,'4241740' );
  
--delete from t1520_function_list where c1520_function_id IN ('INR_QUOTE_USER','INR_QUOTE_ADMIN','INR_QUOTE_APPROVER');

Insert into T1500_GROUP (C1500_GROUP_ID,C1500_GROUP_NM,C1500_GROUP_DESC,C1500_VOID_FL,C1500_CREATED_BY,C1500_CREATED_DATE,C901_GROUP_TYPE) 
values ('INR_QUOTE_USER','INR_QUOTE_USER','Users in this security event will be used as robotics quotation users',null,'4241740',current_date,92264);

Insert into T1500_GROUP (C1500_GROUP_ID,C1500_GROUP_NM,C1500_GROUP_DESC,C1500_VOID_FL,C1500_CREATED_BY,C1500_CREATED_DATE,C901_GROUP_TYPE) 
values ('INR_QUOTE_ADMIN','INR_QUOTE_ADMIN','Users in this security event will be used as robotics quotation admins',null,'4241740',current_date,92264);

Insert into T1500_GROUP (C1500_GROUP_ID,C1500_GROUP_NM,C1500_GROUP_DESC,C1500_VOID_FL,C1500_CREATED_BY,C1500_CREATED_DATE,C901_GROUP_TYPE) 
values ('INR_QUOTE_APPROVER','INR_QUOTE_APPROVER','Users in this security event will be used as robotics quotation approvers',null,'4241740',current_date,92264);