----
SET SERVEROUTPUT ON;

DECLARE
    v_strcatmasterid       NUMBER;
    v_strreftype           NUMBER;
    v_strquoteid           VARCHAR2(20);
    v_quote_account_type   VARCHAR2(20);
    v_order_type           VARCHAR2(100);
    v_plant_id             NUMBER;
    v_plant_name           VARCHAR2(20);
    v_ship_to              VARCHAR2(20);
    v_cat_status           VARCHAR2(100) := NULL;
    v_order_cat_seq        NUMBER := 0;
    v_strAccType		   VARCHAR2(20);
    CURSOR cur_ref_catagories IS
    SELECT
        c5502_robot_quotation_category_master_id   strcatmasterid,
        c901_ref_type                              strreftype,
		c5503_em_default_account_type              acctype
    FROM
        t5503_robot_quotation_category_template
    GROUP BY
        c5502_robot_quotation_category_master_id,
        c901_ref_type,
		c5503_em_default_account_type
    ORDER BY
        c5502_robot_quotation_category_master_id;

BEGIN
   
    FOR cat IN cur_ref_catagories
    LOOP
        v_strcatmasterid := cat.strcatmasterid;
        v_strreftype := cat.strreftype;
		v_strAccType := cat.acctype;
        v_order_cat_seq := v_order_cat_seq + 1;
     
        dbms_output.put_line(v_strcatmasterid || ', ' || v_strreftype || '-' || v_order_cat_seq);
        
		v_quote_account_type := '';
		v_order_type := '';
        v_plant_id := 0;
        v_plant_name := '';
        v_ship_to :=  '';
        v_cat_status := '';
		
    -- 111120 = Part, 111121 = Set
	
        IF v_strreftype = 111120 THEN
            v_order_type := 'Direct';
            v_plant_id := 3014;
            v_plant_name := 'Methuen - Globus';
            v_ship_to :=  'Hospital';
            v_cat_status := 'Ready';
        END IF;

        IF v_strreftype = 111121 THEN
            v_order_type := 'Direct';
            v_plant_id := 3000;
            v_plant_name := 'Audubon';
            v_ship_to := 'Hospital';
            v_cat_status := 'Open';
        END IF;

        IF ((v_strcatmasterid = 1 OR v_strcatmasterid = 12 OR v_strcatmasterid = 3 OR v_strcatmasterid = 14)  AND v_strreftype = 111120) THEN
            v_quote_account_type := 'Capital';
            v_order_type := 'Direct';
            v_plant_id := 3014;
            v_plant_name := 'Methuen - Globus';
        END IF;

        IF ((v_strcatmasterid = 1 OR v_strcatmasterid = 12 OR v_strcatmasterid = 3 OR v_strcatmasterid = 14)  AND v_strreftype = 111121) THEN
            v_quote_account_type := 'Capital';
            v_order_type := 'Direct';
            v_plant_id := 3000;
            v_plant_name := 'Audubon';
        END IF;

        IF v_strcatmasterid = 2 OR v_strcatmasterid = 8 OR v_strcatmasterid = 11 OR v_strcatmasterid = 13 OR v_strcatmasterid = 19 OR v_strcatmasterid = 22  THEN
            v_quote_account_type := 'Capital';
            v_order_type := 'Bill Only - From Sales Consignments';
            v_ship_to := 'N/A';
            v_plant_id := 3014;
            v_plant_name := 'Methuen - Globus';
        END IF;

        IF v_strcatmasterid = 4 OR v_strcatmasterid = 5 OR v_strcatmasterid = 7 OR v_strcatmasterid = 15 OR v_strcatmasterid = 16 OR v_strcatmasterid = 18 THEN
            v_quote_account_type := 'Metal';
            v_order_type := 'Direct';
            v_plant_id := 3000;
            v_plant_name := 'Audubon';
        END IF;
        
        IF v_strcatmasterid = 6 OR v_strcatmasterid = 17 THEN
            v_quote_account_type := 'Metal';
            v_order_type := 'Direct';
            v_plant_id := 3014;
            v_plant_name := 'Methuen - Globus';
        END IF;
		
		IF v_strcatmasterid = 9 OR v_strcatmasterid = 20 THEN
			v_quote_account_type := 'Capital';
		END IF;
		
		IF v_strcatmasterid > 11 AND v_order_type != 'Bill Only - From Sales Consignments' THEN
			v_order_type := 'OUS Distributor';
		END IF;

		IF ((v_strcatmasterid = 1 OR v_strcatmasterid = 12 OR v_strcatmasterid = 2 OR v_strcatmasterid = 13 
		   OR v_strcatmasterid = 9 OR v_strcatmasterid = 20) AND v_strreftype = 111120) THEN
		   	v_plant_id := 3014;
            v_plant_name := 'Methuen - Globus';
        ELSIF ((v_strcatmasterid != 1 OR v_strcatmasterid != 12 OR v_strcatmasterid != 2 OR v_strcatmasterid != 13 
		   OR v_strcatmasterid != 9 OR v_strcatmasterid != 20) AND v_strreftype = 111120) THEN
        	v_plant_id := 3000;
            v_plant_name := 'Audubon';
		END IF;
		
		IF (v_strcatmasterid = 7 OR  v_strcatmasterid = 18) THEN
			v_quote_account_type := v_strAccType;
		END IF;
			
        UPDATE t5503_robot_quotation_category_template
        SET
            c5503_em_default_account_type = v_quote_account_type,
            c5503_em_default_order_type = v_order_type,
            c5503_em_default_plant_name = v_plant_name,
            c5503_em_default_plant_id = v_plant_id,
            c5503_em_default_ship_to_type_name = v_ship_to,
            c5503_em_default_category_status = v_cat_status,
            c5503_order_category_seq = v_order_cat_seq
        WHERE
            c5502_robot_quotation_category_master_id = v_strcatmasterid
            AND c901_ref_type = v_strreftype
			AND c5503_em_default_account_type = v_strAccType;

    END LOOP;
END;
/