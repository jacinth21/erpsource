-- Robototics Quote Group Email
DELETE FROM T906_RULES WHERE C906_RULE_GRP_ID = 'ROQT' AND C906_RULE_ID = 'ROQTEG';
INSERT INTO t906_rules (C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC, C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID, C906_CREATED_BY, C901_RULE_TYPE, C906_ACTIVE_FL, C906_VOID_FL )
  VALUES  ( S906_RULE.NEXTVAL, 'ROQTEG', 'Robotics Quote Team Email Group', 'notification-nonprod@globusmedical.com', CURRENT_DATE, 'ROQT', '4241740', NULL, NULL, NULL );
  
-- Robotics Quote Discount Request Service Section Approver User ID - jay Martin
DELETE FROM T906_RULES WHERE C906_RULE_GRP_ID = 'ROQT' AND C906_RULE_ID = 'ROQTSA';
INSERT INTO t906_rules (C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC, C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID, C906_CREATED_BY, C901_RULE_TYPE, C906_ACTIVE_FL, C906_VOID_FL )
  VALUES  ( S906_RULE.NEXTVAL, 'ROQTSA', 'Robotics Quote Service Discount Approver', '2031775', CURRENT_DATE, 'ROQT', '4241740', NULL, NULL, NULL );
  
-- Robototics Quote rule insert for cancel quote
DELETE FROM T906_RULES WHERE C906_RULE_GRP_ID = 'CMNCNCL' AND C906_RULE_ID = '108260';
INSERT INTO t906_rules (C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC, C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID, C906_CREATED_BY, C901_RULE_TYPE, C906_ACTIVE_FL, C906_VOID_FL )
  VALUES  ( S906_RULE.NEXTVAL, '108260', 'Cancel Robotics Quote', 'gm_pkg_sm_robotics_quote_trans.gm_sav_quote_cancel', CURRENT_DATE, 'CMNCNCL', '4241740', NULL, NULL, NULL );
  
-- Robototics Quote parameter rule insert for reject quote
DELETE FROM T906_RULES WHERE C906_RULE_GRP_ID = 'CMNCNCL' AND C906_RULE_ID = '108261';  
INSERT INTO t906_rules (C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC, C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID, C906_CREATED_BY, C901_RULE_TYPE, C906_ACTIVE_FL, C906_VOID_FL )
  VALUES  ( S906_RULE.NEXTVAL, '108261', 'Reject Discount Request in Robotics Quote', 'gm_pkg_sm_robotics_quote_trans.gm_sav_quote_cancel', CURRENT_DATE, 'CMNCNCL', '4241740', NULL, NULL, NULL );

-- Robototics Quote parameter rule insert for cancel quote
DELETE FROM T906_RULES WHERE C906_RULE_GRP_ID = 'CMNCLPM' AND C906_RULE_ID = '108261';  
INSERT INTO t906_rules (c906_rule_seq_id, c906_rule_id, c906_rule_desc, c906_rule_value, c906_created_date, c906_rule_grp_id, c906_created_by, c901_rule_type, c906_active_fl, c906_void_fl) 
 VALUES ( s906_rule.NEXTVAL, '108260', 'Cancel Robotics Quote', '4', current_date, 'CMNCLPM', '4241740', NULL, NULL, NULL);
 
-- Robototics Quote parameter rule insert for reject quote
DELETE FROM T906_RULES WHERE C906_RULE_GRP_ID = 'CMNCLPM' AND C906_RULE_ID = '108261';  
INSERT INTO t906_rules ( c906_rule_seq_id, c906_rule_id, c906_rule_desc, c906_rule_value, c906_created_date, c906_rule_grp_id, c906_created_by, c901_rule_type, c906_active_fl, c906_void_fl) 
 VALUES ( s906_rule.NEXTVAL, '108261', 'Reject Discount Request in Robotics Quote', '4', current_date, 'CMNCLPM', '4241740', NULL, NULL, NULL);
