-- INR Quote Set Allocation Confirm Access
INSERT INTO t1520_function_list (c1520_function_id, c1520_function_nm, c1520_inherited_from_id, c1520_function_desc, c901_type, c1520_created_date, c1520_created_by)
  VALUES ('INR_QUOTE_SET_AL_CON','INR_QUOTE_SET_AL_CON','INR_QUOTE_SET_AL_CON','Users in this security event will be used as Quote Set Allocation Confirm Access', 92267, current_date,'4241740' );
  

-- INR Quote Consignment Access
INSERT INTO t1520_function_list (c1520_function_id, c1520_function_nm, c1520_inherited_from_id, c1520_function_desc, c901_type, c1520_created_date, c1520_created_by)
  VALUES ('INR_QUOTE_CN_ASSIGN','INR_QUOTE_CN_ASSIGN','INR_QUOTE_CN_ASSIGN','Users in this security event will be used as Quote Consignment Access', 92267, current_date,'4241740' );