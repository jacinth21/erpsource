  -- INR Quote Place order button Access
INSERT INTO t1520_function_list (c1520_function_id, c1520_function_nm, c1520_inherited_from_id, c1520_function_desc, c901_type, c1520_created_date, c1520_created_by)
  VALUES ('INR_QUOTE_SAV_ORDER','INR_QUOTE_SAV_ORDER','INR_QUOTE_SAV_ORDER','Place order button access', 92267, current_date,'4241740' );
  

-- INR Quote edit order button access
INSERT INTO t1520_function_list (c1520_function_id, c1520_function_nm, c1520_inherited_from_id, c1520_function_desc, c901_type, c1520_created_date, c1520_created_by)
  VALUES ('INR_QUOTE_ORDER_EDIT','INR_QUOTE_ORDER_EDIT','INR_QUOTE_ORDER_EDIT','Edit order access', 92267, current_date,'4241740' );
