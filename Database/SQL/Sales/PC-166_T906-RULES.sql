INSERT INTO T906_RULES( C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC, C906_RULE_VALUE, C906_RULE_GRP_ID,
     C906_CREATED_DATE, C906_CREATED_BY, C906_LAST_UPDATED_DATE, C906_LAST_UPDATED_BY, C901_RULE_TYPE ) 
VALUES ( S906_RULE.nextval, 'LOANERS', 'LOANERS - LATEFEEACCRSTMT', '/resources/latefee/accrlatefeespastmon', 'LATEFEEACCRSTMT',  
     current_date, '706310', NULL, NULL, NULL);
     
INSERT INTO T906_RULES( C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC, C906_RULE_VALUE, C906_RULE_GRP_ID,
     C906_CREATED_DATE, C906_CREATED_BY, C906_LAST_UPDATED_DATE, C906_LAST_UPDATED_BY, C901_RULE_TYPE ) 
VALUES ( S906_RULE.nextval, 'LOANERS', 'LOANERS - RECENTLATEFEEACCR', '/resources/latefee/accrlatefeescurr', 'RECENTLATEFEEACCR',  
     current_date, '706310', NULL, NULL, NULL);
     
update t906_rules set c906_rule_value='Late Fee Accr Stmt',c906_last_updated_by='303150',c906_last_updated_date=sysdate where c906_rule_id='LateFeeAccrStmt';
update t906_rules set c906_rule_value='Recent Late Fee Accr',c906_last_updated_by='303150',c906_last_updated_date=sysdate where c906_rule_id='RecentLateFeeAccr';