/*Enable Pricing For Mobile*/
UPDATE t906_rules
SET c906_void_fl         = null,
  c906_last_updated_date = sysdate ,
  c906_last_updated_by   ='303012'
WHERE c906_rule_id       ='PRICING_APP_ACCESS';

UPDATE t906_rules
SET c906_void_fl         = 'Y',
  c906_last_updated_date = sysdate ,
  c906_last_updated_by   ='303012'
WHERE c906_rule_value    ='btn-pricing-mob';


