--- Insert Code Lookup for Tag Sync ---

INSERT
INTO t901_code_lookup
  (
    C901_CODE_ID,
    C901_CODE_NM,
    C901_CODE_GRP,
    C901_ACTIVE_FL,
    C901_CODE_SEQ_NO,
    C901_CREATED_DATE,
    C901_CREATED_BY
  )
  VALUES
  (
    4001234,
    'Tag Details',
    'DESYTY',
    1,
    28,
    sysdate,
    '1056244'
  );
