INSERT INTO T9152_SD_DRILLDOWN( C9152_SD_ID, C901_DRILLDOWN_GRP_ID, C9152_SD_COLUMN_NM, C9152_SEQ, C9152_SD_COLUMN_SHRT_NM,C9152_LAST_UPDATED_DATE, C9152_LAST_UPDATED_BY ) VALUES ( S9152_SD_DRILLDOWN.nextval, '103482', 'Act. Return Date', NULL, 'actreturndt', current_date, '706310');

INSERT INTO T9152_SD_DRILLDOWN( C9152_SD_ID, C901_DRILLDOWN_GRP_ID, C9152_SD_COLUMN_NM, C9152_SEQ, C9152_SD_COLUMN_SHRT_NM,C9152_LAST_UPDATED_DATE, C9152_LAST_UPDATED_BY ) VALUES ( S9152_SD_DRILLDOWN.nextval, '103482', 'Incident Date', NULL, 'incidentdt', current_date, '706310');

INSERT INTO T9152_SD_DRILLDOWN( C9152_SD_ID, C901_DRILLDOWN_GRP_ID, C9152_SD_COLUMN_NM, C9152_SEQ, C9152_SD_COLUMN_SHRT_NM,C9152_LAST_UPDATED_DATE, C9152_LAST_UPDATED_BY ) VALUES ( S9152_SD_DRILLDOWN.nextval, '103482', 'Charge Amount', NULL, 'chargeamt', current_date, '706310');

INSERT INTO T9152_SD_DRILLDOWN( C9152_SD_ID, C901_DRILLDOWN_GRP_ID, C9152_SD_COLUMN_NM, C9152_SEQ, C9152_SD_COLUMN_SHRT_NM,C9152_LAST_UPDATED_DATE, C9152_LAST_UPDATED_BY ) VALUES ( S9152_SD_DRILLDOWN.nextval, '103482', 'Deduction Date', NULL, 'deductiondt', current_date, '706310');

INSERT INTO T9152_SD_DRILLDOWN( C9152_SD_ID, C901_DRILLDOWN_GRP_ID, C9152_SD_COLUMN_NM, C9152_SEQ, C9152_SD_COLUMN_SHRT_NM,C9152_LAST_UPDATED_DATE, C9152_LAST_UPDATED_BY ) VALUES ( S9152_SD_DRILLDOWN.nextval, '103482', 'Charge Status', NULL, 'chargests', current_date, '706310');    
