CREATE OR REPLACE PUBLIC SYNONYM mv9161_email_tracking_master FOR GLOBUS_APP.mv9161_email_tracking_master;
CREATE OR REPLACE PUBLIC SYNONYM mv9161_email_tracking_details FOR GLOBUS_APP.mv9161_email_tracking_details;
CREATE OR REPLACE PUBLIC SYNONYM t1016_share FOR GLOBUS_APP.t1016_share;
CREATE OR REPLACE PUBLIC SYNONYM t1017_share_detail FOR GLOBUS_APP.t1017_share_detail;
CREATE OR REPLACE PUBLIC SYNONYM t1018_share_file FOR GLOBUS_APP.t1018_share_file;
CREATE OR REPLACE PUBLIC SYNONYM get_email_track_event_count FOR GLOBUS_APP.get_email_track_event_count;
CREATE OR REPLACE PUBLIC SYNONYM get_email_track_click_count FOR GLOBUS_APP.get_email_track_click_count;

