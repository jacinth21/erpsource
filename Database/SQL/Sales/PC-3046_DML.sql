--Jobs 
 Delete From  T9300_Job Where C9300_Job_Id In ( '1055','1056','1057' );

  Insert Into T9300_Job( C9300_Job_Id,C9300_Job_Nm,C9300_What,C9300_Start_Date ,C9300_Created_By ,C9300_Created_Date,C9300_Inactive_Fl)
   VALUES (1055,'INV_SYS_LOC_RPT_TAG_MAIN', 'gm_pkg_inv_tag_system_locator_job.gm_process_tag_main()', NULL,'3178209' , CURRENT_DATE,NULL);
  
  Insert Into T9300_Job( C9300_Job_Id,C9300_Job_Nm,C9300_What,C9300_Start_Date ,C9300_Created_By ,C9300_Created_Date,C9300_Inactive_Fl)
  VALUES (1056,'INV_SYS_LOC_RPT_REP_CONTACT_MAIN', 'gm_pkg_inv_tag_system_locator_job.gm_process_rep_contact_dtl_main()', NULL,'3178209' , CURRENT_DATE,NULL);

  Insert Into T9300_Job( C9300_Job_Id,C9300_Job_Nm,C9300_What,C9300_Start_Date ,C9300_Created_By ,C9300_Created_Date,C9300_Inactive_Fl)
  Values (1057,'INV_SYS_LOC_RPT_DETAIL', 'gm_pkg_inv_tag_system_locator_job.gm_process_sys_loc_report_dtl()', Null,'3178209' , Current_Date,Null);

--codelookup
INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'SYSLOC','Code group for system locator report job','','3178209',CURRENT_DATE);
  
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(110700,'DO Classification','SYSLOC','1','1','','','3178209',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(110701,'Tag Recorded from Order','SYSLOC','1','2','','','3178209',CURRENT_DATE);

--Packages synonyms
CREATE OR REPLACE PUBLIC SYNONYM gm_pkg_inv_tag_system_locator_job FOR GLOBUS_APP.gm_pkg_inv_tag_system_locator_job;
CREATE OR REPLACE PUBLIC SYNONYM gm_pkg_inv_system_locator_txn FOR GLOBUS_APP.gm_pkg_inv_system_locator_txn;
CREATE OR REPLACE PUBLIC SYNONYM MV5010g_tag_system_locator FOR GLOBUS_APP.MV5010g_tag_system_locator;

--table synonyms
CREATE OR REPLACE PUBLIC SYNONYM T5010g_Tag_System_Locator FOR GLOBUS_APP.T5010g_Tag_System_Locator;
CREATE OR REPLACE PUBLIC SYNONYM T5010h_System_Locator_Dtls FOR GLOBUS_APP.T5010h_System_Locator_Dtls;

