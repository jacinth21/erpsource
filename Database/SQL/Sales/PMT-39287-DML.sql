UPDATE T906_Rules
SET C906_Rule_Value      = 'btn-catalog,btn-collateral,btn-case,btn-inv,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags,div-do-NPI-info',
  C906_Last_Updated_By   = '706328',
  C906_Last_Updated_Date = CURRENT_DATE
WHERE C906_Rule_Id      IN ( '1017','1004')
AND c906_rule_grp_id     ='MODULEACCESS';
