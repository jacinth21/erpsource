DELETE FROM t906_rules where c906_rule_grp_id = 'ESTREAMAPP' AND c906_rule_id in ('ACCESSID','ACCESS');

INSERT
INTO t906_rules
  (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_desc,
    c906_rule_value,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c906_void_fl
  )
  VALUES
  (
    S906_Rule.Nextval,
    'ACCESSID',
    'Fleet Name to validate',
    '48d9af1672b0fee54a88b1a22477d729b5505d8c0a6d44729f0e571bfe4404a0',
    CURRENT_DATE,
    'ESTREAMAPP',
    '2765071',
    NULL
  );
  
  INSERT
INTO t906_rules
  (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_desc,
    c906_rule_value,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c906_void_fl
  )
  VALUES
  (
    S906_Rule.Nextval,
    'ACCESS',
    'Fleet Name to validate',
    'Y',
    CURRENT_DATE,
    'ESTREAMAPP',
    '2765071',
    NULL
  );