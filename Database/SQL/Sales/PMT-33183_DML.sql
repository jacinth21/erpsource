update t1520_function_list set c1520_request_uri='\GmSaleDashBoardServlet?hReportType=Historical_HIDECOMPANY',c1520_last_updated_by='303510',c1520_last_updated_date=current_date 
where c1520_function_id='19000800'; 

--By AD
update t1520_function_list set c1520_request_uri='\GmSalesGrowthServlet?hReportType=Historical_HIDECOMPANY',c1520_last_updated_by='303510',c1520_last_updated_date=current_date
where c1520_function_id='26926200';

--By VP
update t1520_function_list set c1520_request_uri='\GmSalesGrowthServlet?hReportType=Historical_HIDECOMPANY',c1520_last_updated_by='303510',c1520_last_updated_date=current_date
where c1520_function_id='26926100';

--By Field Sales (Historical)
update t1520_function_list set C1520_FUNCTION_NM='By Field Sales (Historical)',c1520_last_updated_by='303510',c1520_last_updated_date=current_date
where c1520_function_id='QUOTA_HIST_BY_DIST';

--By Rep (Historical)
update t1520_function_list set C1520_FUNCTION_NM='By Rep (Historical)',c1520_last_updated_by='303510',c1520_last_updated_date=current_date
where c1520_function_id='QUOTA_HIST_BY_REP';


