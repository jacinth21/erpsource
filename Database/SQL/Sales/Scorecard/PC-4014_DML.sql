-- PC-4014 Scorecard and Field Inventory Report full access (mschmitt)
-- Sales more filter full access for Micah Schmitt
INSERT INTO T906_RULES (C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID , C906_CREATED_BY)
VALUES ( s906_rule.nextval, '1120062', 'Micah Schmitt access' , 'Y', SYSDATE, 'SALES_FILTER_FULL_AC' , 706322);
