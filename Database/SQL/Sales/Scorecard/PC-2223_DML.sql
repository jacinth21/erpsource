/*
 * PC-2223 - Scorecard - Exp. Consignment usage report
 * Insert record to create left link
 */  
-- By Consignment
INSERT INTO T1520_FUNCTION_LIST
  (
    C1520_FUNCTION_ID,
    C1520_FUNCTION_NM,
    C1520_FUNCTION_DESC,
    C1520_INHERITED_FROM_ID,
    C901_TYPE,
    C1520_VOID_FL,
    C1520_CREATED_BY,
    C1520_CREATED_DATE,
    C1520_LAST_UPDATED_BY,
    C1520_LAST_UPDATED_DATE,
    C1520_REQUEST_URI,
    C1520_REQUEST_STROPT,
    C1520_SEQ_NO,
    C901_DEPARTMENT_ID,
    C901_LEFT_MENU_TYPE
  )
  VALUES
  (
    'SCORECARD_BY_CON',
    'Exp. Consignment Usage',
    'To Scorecard - Exp. Consignment usage report.',
    NULL,
    92262,
    NULL,
    '706322',
    SYSDATE,
    '706322',
    NULL,
    '/gmSalesEfficiencyScoreCardByCon.do?method=loadConUsageScoreCardDtls&hideCompany=Y',                                                                                       
    NULL,
    NULL,
    '2005',
    '101920'  
  );
  

