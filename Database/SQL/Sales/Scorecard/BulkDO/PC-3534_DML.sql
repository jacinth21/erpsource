--Create Code Lookup for Bulk DO
DELETE t901b_code_group_master WHERE C901B_CODE_GRP='ORDCTG';
INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'ORDCTG','Order Category as Bulk DO, DO, etc.,','','941711',CURRENT_DATE);
  
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(111160,'DO','ORDCTG','0','1','','','941711',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(111161,'Bulk DO','ORDCTG','0','2','','','941711',CURRENT_DATE);
 
  
  -- Update existing tag recording data
UPDATE t501d_order_tag_usage_details SET c501d_system_usage=1 WHERE c501d_void_fl IS NULL; 
UPDATE t501d_order_tag_usage_details SET c901_order_category=111160 WHERE c501d_void_fl IS NULL; 

--Update system id in existing data based on Set Id
UPDATE t501d_order_tag_usage_details t501d
SET t501d.c207_System_id =  GM_PKG_SM_TAG_ORDER_HISTORY.get_system_id(t501d.c207_set_id) 
WHERE t501d.c207_System_id IS NULL
AND c501d_void_fl IS NULL;
  
--Account atribute
delete  T704a_account_attribute where C901_ATTRIBUTE_TYPE = 111161;

Insert into T704a_account_attribute (C704A_ACCOUNT_ATTRIBUTE_ID,C704_ACCOUNT_ID,C704A_ATTRIBUTE_VALUE,C704A_CREATED_BY,C704A_CREATED_DATE,
C901_ATTRIBUTE_TYPE) 
values (S704A_ACCOUNT_ATTRIBUTE.nextval,'5553','Y','941711',SYSDATE,111161);

Insert into T704a_account_attribute (C704A_ACCOUNT_ATTRIBUTE_ID,C704_ACCOUNT_ID,C704A_ATTRIBUTE_VALUE,C704A_CREATED_BY,C704A_CREATED_DATE,
C901_ATTRIBUTE_TYPE) 
values (S704A_ACCOUNT_ATTRIBUTE.nextval,'6240','Y','941711',SYSDATE,111161);

Insert into T704a_account_attribute (C704A_ACCOUNT_ATTRIBUTE_ID,C704_ACCOUNT_ID,C704A_ATTRIBUTE_VALUE,C704A_CREATED_BY,C704A_CREATED_DATE,
C901_ATTRIBUTE_TYPE) 
values (S704A_ACCOUNT_ATTRIBUTE.nextval,'6499','Y','941711',SYSDATE,111161);

Insert into T704a_account_attribute (C704A_ACCOUNT_ATTRIBUTE_ID,C704_ACCOUNT_ID,C704A_ATTRIBUTE_VALUE,C704A_CREATED_BY,C704A_CREATED_DATE,
C901_ATTRIBUTE_TYPE) 
values (S704A_ACCOUNT_ATTRIBUTE.nextval,'9713','Y','941711',SYSDATE,111161);

Insert into T704a_account_attribute (C704A_ACCOUNT_ATTRIBUTE_ID,C704_ACCOUNT_ID,C704A_ATTRIBUTE_VALUE,C704A_CREATED_BY,C704A_CREATED_DATE,
C901_ATTRIBUTE_TYPE) 
values (S704A_ACCOUNT_ATTRIBUTE.nextval,'15381','Y','941711',SYSDATE,111161);

Insert into T704a_account_attribute (C704A_ACCOUNT_ATTRIBUTE_ID,C704_ACCOUNT_ID,C704A_ATTRIBUTE_VALUE,C704A_CREATED_BY,C704A_CREATED_DATE,
C901_ATTRIBUTE_TYPE) 
values (S704A_ACCOUNT_ATTRIBUTE.nextval,'15380','Y','941711',SYSDATE,111161);

