/*
 * PC-1927 - Scorecard - Sales report
 * Insert record to create left link
 */  
-- By System
INSERT INTO T1520_FUNCTION_LIST
  (
    C1520_FUNCTION_ID,
    C1520_FUNCTION_NM,
    C1520_FUNCTION_DESC,
    C1520_INHERITED_FROM_ID,
    C901_TYPE,
    C1520_VOID_FL,
    C1520_CREATED_BY,
    C1520_CREATED_DATE,
    C1520_LAST_UPDATED_BY,
    C1520_LAST_UPDATED_DATE,
    C1520_REQUEST_URI,
    C1520_REQUEST_STROPT,
    C1520_SEQ_NO,
    C901_DEPARTMENT_ID,
    C901_LEFT_MENU_TYPE
  )
  VALUES
  (
    'SCORECARD_BY_SYSTEM',
    'By System',
    'To Report Scorecard - Sales report By System.',
    NULL,
    92262,
    NULL,
    '706322',
    SYSDATE,
    '706322',
    NULL,
    '/gmSalesEfficiencyScoreCard.do?method=loadSystemScoreCardDtls&hideCompany=Y',                                                                                       
    NULL,
    NULL,
    '2005',
    '101920'  
  );
  
-- By Field Sales 
INSERT INTO T1520_FUNCTION_LIST
  (
    C1520_FUNCTION_ID,
    C1520_FUNCTION_NM,
    C1520_FUNCTION_DESC,
    C1520_INHERITED_FROM_ID,
    C901_TYPE,
    C1520_VOID_FL,
    C1520_CREATED_BY,
    C1520_CREATED_DATE,
    C1520_LAST_UPDATED_BY,
    C1520_LAST_UPDATED_DATE,
    C1520_REQUEST_URI,
    C1520_REQUEST_STROPT,
    C1520_SEQ_NO,
    C901_DEPARTMENT_ID,
    C901_LEFT_MENU_TYPE
  )
  VALUES
  (
    'SCORECARD_BY_FS',
    'By Field Sales',
    'To Report Scorecard - Sales report By Field Sales.',
    NULL,
    92262,
    NULL,
    '706322',
    SYSDATE,
    '706322',
    NULL,
    '/gmSalesEfficiencyScoreCard.do?method=loadFieldSalesScoreCardDtls&hideCompany=Y',                                                                                                    
    NULL,
    NULL,
    '2005',
    '101920'  
  );

-- By AD
INSERT INTO T1520_FUNCTION_LIST
  (
    C1520_FUNCTION_ID,
    C1520_FUNCTION_NM,
    C1520_FUNCTION_DESC,
    C1520_INHERITED_FROM_ID,
    C901_TYPE,
    C1520_VOID_FL,
    C1520_CREATED_BY,
    C1520_CREATED_DATE,
    C1520_LAST_UPDATED_BY,
    C1520_LAST_UPDATED_DATE,
    C1520_REQUEST_URI,
    C1520_REQUEST_STROPT,
    C1520_SEQ_NO,
    C901_DEPARTMENT_ID,
    C901_LEFT_MENU_TYPE
  )
  VALUES
  (
    'SCORECARD_BY_AD',
    'By AD',
    'To Report Scorecard - Sales report By Area Director.',
    NULL,
    92262,
    NULL,
    '706322',
    SYSDATE,
    '706322',
    NULL,
    '/gmSalesEfficiencyScoreCard.do?method=loadADScoreCardDtls&hideCompany=Y',                                                                                                    
    NULL,
    NULL,
    '2005',
    '101920'  
  );
  
 -- Left link folder script
INSERT
   INTO T1520_FUNCTION_LIST
    (
        C1520_FUNCTION_ID, C1520_FUNCTION_NM, C1520_FUNCTION_DESC
      , C1520_INHERITED_FROM_ID, C901_TYPE, C1520_VOID_FL
      , C1520_CREATED_BY, C1520_CREATED_DATE, C1520_LAST_UPDATED_BY
      , C901_DEPARTMENT_ID
    )
    VALUES
    (
        'SCORECARD_RPT_FLD', 'Scorecard', 'Scorecard folder'
      , NULL, 92262, NULL
      , '706322', SYSDATE, NULL
      , 2005
    ) ;

--100000000 Sales

INSERT
   INTO T1530_ACCESS
    (
        C1530_ACCESS_ID, C1500_GROUP_ID, C1520_FUNCTION_ID
      , C101_PARTY_ID, C1530_UPDATE_ACCESS_FL, C1530_READ_ACCESS_FL
      , C1530_CREATED_BY
      , C1530_CREATED_DATE
      , C1520_PARENT_FUNCTION_ID, C1530_FUNCTION_NM, C1530_SEQ_NO
    )
    VALUES
    (
        s1530_access.nextval, '100000000', 'SCORECARD_RPT_FLD'
      , NULL, 'Y', 'Y'
      , 706322
      , SYSDATE
      , '12274000', NULL, 2002471000001200
    ) ;


