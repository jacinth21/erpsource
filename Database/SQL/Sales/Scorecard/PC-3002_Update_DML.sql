UPDATE t901_code_lookup
SET
    c901_void_fl = NULL,
    c901_last_updated_by = '2598779',
    c901_last_updated_date = sysdate
WHERE
    c901_code_id = '103164';