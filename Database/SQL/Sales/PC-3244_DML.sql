--select * from t906_rules where c906_rule_grp_id = 'MODULEACCESS';
-- To Show 

--Ipad Module Access Exclusion list For BBA
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags,div-do-report-edit', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1001' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For AUSTRALIA
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags,div-do-report-edit', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1002' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For AUSTRIA
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags,div-do-report-edit', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1003' AND C906_VOID_FL IS NULL;
 --Ipad Module Access Exclusion list For BELGIUM
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags,div-do-report-edit', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1004' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For DENMARK
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags,div-do-report-edit', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1006' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For FRANCE
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags,div-do-report-edit', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1007' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For GERMANY
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags,div-do-report-edit', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1008' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For CHENNAI
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags,div-do-report-edit', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1009' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For NETHERLANDS
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags,div-do-report-edit', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1010' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For DELHI
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags,div-do-report-edit', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1011' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For SOUTH AFRICA
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags,div-do-report-edit', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1012' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For POLAND
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags,div-do-report-edit', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1014' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For SWEDEN
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags,div-do-report-edit', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1015' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For SWITZERLAND
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags,div-do-report-edit', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1016' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For UK
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags,div-do-report-edit', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1017' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For HQ
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags,div-do-report-edit', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1018' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For BRAZIL
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags,div-do-report-edit', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1019' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For ITALY
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags,div-do-report-edit', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1020' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For IRELAND
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags,div-do-report-edit', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1023' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For JAPAN
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-catalog,btn-collateral,btn-guide,btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags,div-do-report-edit', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1026' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For Finland
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags,div-do-report-edit', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1027' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For Norway
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags,div-do-report-edit', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1028' AND C906_VOID_FL IS NULL;


-------- Non - Prod------
/*INSERT
INTO t906_rules
  (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_desc,
    c906_rule_value,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c906_last_updated_date,
    c906_last_updated_by,
    c901_rule_type,
    c906_active_fl,
    c906_void_fl,
    c1900_company_id,
    c906_last_updated_date_utc
  )
  VALUES
  (
    S906_Rule.Nextval,
    'AMZ_ENDPOINT',
    'Amazon Read Image endpoint',
    'https://globusonereadimage.cognitiveservices.azure.com/vision/v3.1/read/analyze',
    CURRENT_DATE,
    'AMAZON_CREDENTIAL',
    '3178209',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    1000,
    Null
  );
  
INSERT
INTO t906_rules
  (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_desc,
    c906_rule_value,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c906_last_updated_date,
    c906_last_updated_by,
    c901_rule_type,
    c906_active_fl,
    c906_void_fl,
    c1900_company_id,
    c906_last_updated_date_utc
  )
  VALUES
  (
    S906_Rule.Nextval,
    'AMZ_SUBKEY',
    'Amazon Read Image subscripton key',
    'dc22bb3d3b844b6a895cf8ec0cf386af',
    CURRENT_DATE,
    'AMAZON_CREDENTIAL',
    '3178209',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    1000,
    Null
  );
  */
  

delete From T906_Rules Where C906_Rule_Grp_Id='AMAZON_CREDENTIAL';

-------- Prod --------
INSERT
INTO t906_rules
  (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_desc,
    c906_rule_value,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c906_last_updated_date,
    c906_last_updated_by,
    c901_rule_type,
    c906_active_fl,
    c906_void_fl,
    c1900_company_id,
    c906_last_updated_date_utc
  )
  VALUES
  (
    S906_Rule.Nextval,
    'AMZ_ENDPOINT',
    'Amazon Read Image computer vision endpoint',
    'https://globuscomputervision.cognitiveservices.azure.com/vision/v3.1/read/analyze',
    CURRENT_DATE,
    'AMAZON_CREDENTIAL',
    '3178209',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    1000,
    Null
  );
  
INSERT
INTO t906_rules
  (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_desc,
    c906_rule_value,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c906_last_updated_date,
    c906_last_updated_by,
    c901_rule_type,
    c906_active_fl,
    c906_void_fl,
    c1900_company_id,
    c906_last_updated_date_utc
  )
  VALUES
  (
    S906_Rule.Nextval,
    'AMZ_SUBKEY',
    'Amazon Read Image computer vision subscripton key',
    '6c077ec8fc6442148c39578c46421026',
    CURRENT_DATE,
    'AMAZON_CREDENTIAL',
    '3178209',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    1000,
    Null
  );  
