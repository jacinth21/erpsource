delete From T906_Rules Where C906_Rule_Id ='FIREBASE_CONFIG' And C906_Rule_Grp_Id='FIREBASE_CREDENTIAL';

INSERT
INTO t906_rules
  (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_desc,
    c906_rule_value,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c906_last_updated_date,
    c906_last_updated_by,
    c901_rule_type,
    c906_active_fl,
    c906_void_fl,
    c1900_company_id,
    c906_last_updated_date_utc
  )
  VALUES
  (
    S906_Rule.Nextval,
    'FIREBASE_CONFIG',
    'Ftrebase Configuration details',
    '{        
      "apiKey": "AIzaSyBK05kRgzYYMECYgyFjv4W7VRjsaBae0Js",
      "authDomain": "svcfireprod-b1ad9.firebaseapp.com",
      "projectId": "svcfireprod-b1ad9",
      "storageBucket": "svcfireprod-b1ad9.appspot.com",
      "messagingSenderId": "997130310022",
      "appId": "1:997130310022:web:466d228af64830d2de9c07",
      "measurementId": "G-E176PV1Z95"     
    }',
    CURRENT_DATE,
    'FIREBASE_CREDENTIAL',
    '3178209',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    1000,
    Null
  );
  
  --committing for reference
  /*
--globusmedical.com.2020@gmail.com
INSERT
INTO t906_rules
  (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_desc,
    c906_rule_value,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c906_last_updated_date,
    c906_last_updated_by,
    c901_rule_type,
    c906_active_fl,
    c906_void_fl,
    c1900_company_id,
    c906_last_updated_date_utc
  )
  VALUES
  (
    S906_Rule.Nextval,
    'FIREBASE_CONFIG',
    'Ftrebase Configuration details',
    '{        
      "apiKey": "AIzaSyDMYdW9yf-wWnX8odCKWllJ7SWiUMbC8fY",        
      "authDomain": "fir-webapp-1cda7.firebaseapp.com",        
      "databaseURL": "https://fir-webapp-1cda7.firebaseio.com",        
      "projectId": "fir-webapp-1cda7",        
      "storageBucket": "fir-webapp-1cda7.appspot.com",        
      "messagingSenderId": "1065009534937",        
      "appId": "1:1065009534937:web:b5d56df1b8534100d5abc8",        
      "measurementId": "G-01LE7FG0KM"      
}',
    CURRENT_DATE,
    'FIREBASE_CREDENTIAL',
    '3178209',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    1000,
    NULL
  );
  
  --svcfiredev@gmail.com
  INSERT
INTO t906_rules
  (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_desc,
    c906_rule_value,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c906_last_updated_date,
    c906_last_updated_by,
    c901_rule_type,
    c906_active_fl,
    c906_void_fl,
    c1900_company_id,
    c906_last_updated_date_utc
  )
  VALUES
  (
    S906_Rule.Nextval,
    'FIREBASE_CONFIG',
    'Ftrebase Configuration details',
    '{        
      "apiKey": "AIzaSyBektzeHPb9JdDqcA4WqkdcXVTrUUNuJlU",
      "authDomain": "svcfiredev-cdccc.firebaseapp.com",
      "projectId": "svcfiredev-cdccc",
      "storageBucket": "svcfiredev-cdccc.appspot.com",
      "messagingSenderId": "638853643248",
      "appId": "1:638853643248:web:669fac561490f524237b35",
      "measurementId": "G-Q9FBT6YQ3B"      
    }',
    CURRENT_DATE,
    'FIREBASE_CREDENTIAL',
    '3178209',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    1000,
    Null
  );
  
 --svcfireprod@gmail.com 
  INSERT
INTO t906_rules
  (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_desc,
    c906_rule_value,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c906_last_updated_date,
    c906_last_updated_by,
    c901_rule_type,
    c906_active_fl,
    c906_void_fl,
    c1900_company_id,
    c906_last_updated_date_utc
  )
  VALUES
  (
    S906_Rule.Nextval,
    'FIREBASE_CONFIG',
    'Ftrebase Configuration details',
    '{        
      "apiKey": "AIzaSyBK05kRgzYYMECYgyFjv4W7VRjsaBae0Js",
      "authDomain": "svcfireprod-b1ad9.firebaseapp.com",
      "projectId": "svcfireprod-b1ad9",
      "storageBucket": "svcfireprod-b1ad9.appspot.com",
      "messagingSenderId": "997130310022",
      "appId": "1:997130310022:web:466d228af64830d2de9c07",
      "measurementId": "G-E176PV1Z95"     
    }',
    CURRENT_DATE,
    'FIREBASE_CREDENTIAL',
    '3178209',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    1000,
    Null
  );
  */