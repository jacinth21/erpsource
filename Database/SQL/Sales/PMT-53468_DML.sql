--select * from t906_rules where c906_rule_grp_id = 'MODULEACCESS';
-- To Show NPI Tab in All OUS Countries

--Ipad Module Access Exclusion list For BBA
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1001' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For AUSTRALIA
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1002' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For AUSTRIA
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1003' AND C906_VOID_FL IS NULL;
 --Ipad Module Access Exclusion list For BELGIUM
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1004' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For DENMARK
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1006' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For FRANCE
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1007' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For GERMANY
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1008' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For CHENNAI
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1009' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For NETHERLANDS
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1010' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For DELHI
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1011' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For SOUTH AFRICA
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1012' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For POLAND
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1014' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For SWEDEN
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1015' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For SWITZERLAND
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1016' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For UK
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1017' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For HQ
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1018' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For BRAZIL
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1019' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For ITALY
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1020' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For IRELAND
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1023' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For JAPAN
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-catalog,btn-collateral,btn-guide,btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1026' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For Finland
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1027' AND C906_VOID_FL IS NULL;
--Ipad Module Access Exclusion list For Norway
UPDATE t906_rules SET C906_RULE_VALUE = 'btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-record-tags', C906_LAST_UPDATED_BY = '706310',  C906_LAST_UPDATED_DATE = current_date WHERE C906_RULE_GRP_ID = 'MODULEACCESS' AND C906_RULE_ID ='1028' AND C906_VOID_FL IS NULL;
