--by Field sales Report screen
INSERT 
INTO t1520_function_list
  (
    C1520_FUNCTION_ID,
    C1520_FUNCTION_NM,
    C1520_FUNCTION_DESC,
    C1520_INHERITED_FROM_ID,
    C901_TYPE,
    C1520_VOID_FL,
    C1520_CREATED_BY,
    C1520_CREATED_DATE,
    C1520_LAST_UPDATED_BY,
    C1520_LAST_UPDATED_DATE,
    C1520_REQUEST_URI,
    C1520_REQUEST_STROPT,
    C1520_SEQ_NO,
    C901_DEPARTMENT_ID,
    C901_LEFT_MENU_TYPE
  )
  VALUES
  (
    'QUOTA_HIST_BY_DIST',
    'By Field Sales',
    'Report which shows the details of Quota Performance by each Distributor in Month, quarter and year.',
    NULL,
    92262,
    NULL,
    '303510',
    CURRENT_DATE,
    NULL,
    NULL,
    '\GmSalesGrowthServlet?hReportType=Historical_HIDECOMPANY',
    'DIST',
    NULL,
    2005,
    101920
  );

--by reps

INSERT 
INTO t1520_function_list
  (
    C1520_FUNCTION_ID,
    C1520_FUNCTION_NM,
    C1520_FUNCTION_DESC,
    C1520_INHERITED_FROM_ID,
    C901_TYPE,
    C1520_VOID_FL,
    C1520_CREATED_BY,
    C1520_CREATED_DATE,
    C1520_LAST_UPDATED_BY,
    C1520_LAST_UPDATED_DATE,
    C1520_REQUEST_URI,
    C1520_REQUEST_STROPT,
    C1520_SEQ_NO,
    C901_DEPARTMENT_ID,
    C901_LEFT_MENU_TYPE
  )
  VALUES
  (
    'QUOTA_HIST_BY_REP',
    'By Rep',
    'Report which shows the details of Quota Performance by each Rep in Month, quarter and year.',
    NULL,
    92262,
    NULL,
    '303510',
    CURRENT_DATE,
    NULL,
    NULL,
    '\GmSalesGrowthServlet?hReportType=Historical_HIDECOMPANY',
    'TERR',
    NULL,
    2005,
    101920
  );
