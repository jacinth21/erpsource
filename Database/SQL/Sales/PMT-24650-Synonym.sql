create or replace public synonym T2079A_KIT_PART_MAP_DTL for GLOBUS_APP.T2079A_KIT_PART_MAP_DTL;
create or replace public synonym T2079B_KIT_REP_MAP for GLOBUS_APP.T2079B_KIT_REP_MAP;
create or replace public synonym gm_pkg_kit_mapping_rpt for GLOBUS_APP.gm_pkg_kit_mapping_rpt;
create or replace public synonym gm_pkg_kit_mapping_wrap for GLOBUS_APP.gm_pkg_kit_mapping_wrap;