UPDATE t906_rules SET c906_rule_value='Y', c906_last_updated_date = current_date, c906_last_updated_by = '706328' where c906_rule_grp_id ='ACC_DIV_EGPS' and c906_rule_id ='100800';


UPDATE t901_code_lookup SET c901_code_nm='eGPS without CSR', c901_last_updated_date =current_date, C901_LAST_UPDATED_BY='706328' where c901_code_id='111623';

UPDATE t901_code_lookup SET c901_code_nm='No eGPS',  c901_last_updated_date =current_date, C901_LAST_UPDATED_BY='706328' where c901_code_id='111624';

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(26241352,'eGPS with CSR support','EGPUSE','1','','','','2598779',CURRENT_DATE);
