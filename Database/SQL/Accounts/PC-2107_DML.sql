--Create synonynm for new package
CREATE OR REPLACE PUBLIC SYNONYM gm_pkg_temp_vat_upd FOR GLOBUS_APP.gm_pkg_temp_vat_upd; 

  --Update existing vat rate to new vat rate in code lookup table
  UPDATE T901_CODE_LOOKUP 
     SET C902_CODE_NM_ALT ='21',
     	 C901_CODE_NM = 'VAT Rate for Ireland (21%)',
     	 C901_LAST_UPDATED_BY = '2765071',
     	 C901_LAST_UPDATED_DATE = CURRENT_DATE
   WHERE C901_CODE_ID = '26230677'
     AND C901_VOID_FL IS NULL;
     
 DELETE FROM T906_RULES WHERE C906_RULE_GRP_ID IN ('TEMP_VAT_PER','TEMP_VAT','TEMP_VAT_ST_DT','TEMP_VAT_ED_DT')
 AND C1900_COMPANY_ID = '1023';
 
 INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID,C1900_COMPANY_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, '1023', 'Temp VAT rate - for Globus Medical Ireland'
      , 'VAT 21%', CURRENT_DATE, 'TEMP_VAT_PER','1023'
      , '2765071'
    ) ;
     
 
 INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID,C1900_COMPANY_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, '1023', 'Temp VAT rate - for Globus Medical Ireland'
      , 'Y', CURRENT_DATE, 'TEMP_VAT','1023'
      , '2765071'
    ) ;
   
  INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID,C1900_COMPANY_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, '1023', 'Temp VAT rate - start date for Globus Medical Ireland'
      , '01/09/2020', CURRENT_DATE, 'TEMP_VAT_ST_DT','1023'
      , '2765071'
    ) ; 
    
  INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID,C1900_COMPANY_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, '1023', 'Temp VAT rate - end date for Globus Medical Ireland'
      , '28/02/2021', CURRENT_DATE, 'TEMP_VAT_ED_DT','1023'
      , '2765071'
    ) ;   
    