  --create synonymn for PMT-42635
  CREATE OR REPLACE  public synonym gm_pkg_ac_ar_split_batch_trans FOR GLOBUS_APP.gm_pkg_ac_ar_split_batch_trans;

 
  INSERT
INTO T906_RULES
  (
    C906_RULE_SEQ_ID,
    C906_RULE_ID,
    C906_RULE_DESC,
    C906_RULE_VALUE,
    C906_CREATED_DATE,
    C906_RULE_GRP_ID,
    C906_CREATED_BY,
    C1900_COMPANY_ID
  )
  VALUES
  (
    S906_RULE.NEXTVAL,
    '1026',
    'Dealer count for spliting the batch',
    '20',
    CURRENT_DATE,
    'BATCH_DEALER_SLT_CNT',
    '303510',
    '1026'
  );
  
   INSERT
INTO T906_RULES
  (
    C906_RULE_SEQ_ID,
    C906_RULE_ID,
    C906_RULE_DESC,
    C906_RULE_VALUE,
    C906_CREATED_DATE,
    C906_RULE_GRP_ID,
    C906_CREATED_BY,
    C1900_COMPANY_ID
  )
  VALUES
  (
    S906_RULE.NEXTVAL,
    '1026',
    'MIC-Dealer having bulk transactions',
    '21901',
    CURRENT_DATE,
    'JPN_BULK_DEALERS',
    '303510',
    '1026'
  ); 
  
   INSERT
INTO T906_RULES
  (
    C906_RULE_SEQ_ID,
    C906_RULE_ID,
    C906_RULE_DESC,
    C906_RULE_VALUE,
    C906_CREATED_DATE,
    C906_RULE_GRP_ID,
    C906_CREATED_BY,
    C1900_COMPANY_ID
  )
  VALUES
  (
    S906_RULE.NEXTVAL,
    '1026',
    'Senko medical - Dealer having bulk transactions',
    '22029',
    CURRENT_DATE,
    'JPN_BULK_DEALERS',
    '303510',
    '1026'
  );
