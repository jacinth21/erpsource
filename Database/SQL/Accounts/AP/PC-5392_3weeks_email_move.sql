-- 111724	In-Complete	RPAEMS	1
-- 111720	Not Processed	RPAEMS	1
INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_value,
    c906_rule_grp_id,
    c906_created_date,
    c906_created_by
) VALUES (
    s906_rule.NEXTVAL,
    'EMAIL_DAYS_MOVE',
    '21',
    'RPA_EXTRACTOR',
    SYSDATE,
    941711
);


UPDATE t901_code_lookup
SET
    c901_code_nm = 'In-Complete',
    c901_active_fl = 1,
    c901_last_updated_by = 941711,
    c901_last_updated_date = current_date
WHERE
    c901_code_id = 111724;
    
INSERT INTO t4306_rpa_add_charge_mapping (
    c4306_additional_charge_name,
    c4306_additional_charge_code,
    c4306_last_updated_by,
    c4306_last_updated_date
) VALUES (
    'Shipped',
    '5002 Freight Implants',
    30301,
    current_date
);
    