-- grant for RPA tables
GRANT SELECT,INSERT,UPDATE ON globus_app.t4300_rpa_email_hdr TO globus_power_app ;
GRANT SELECT,INSERT,UPDATE ON globus_app.t4301_rpa_email_attachment_hdr TO globus_power_app ;
GRANT SELECT,INSERT,UPDATE ON globus_app.t4302_rpa_invoice_hdr TO globus_power_app ;
GRANT SELECT,INSERT,UPDATE ON globus_app.t4303_rpa_invoice_details TO globus_power_app ;
GRANT SELECT,INSERT,UPDATE ON globus_app.t4303a_rpa_invoice_details_temp TO globus_power_app ;
GRANT SELECT,INSERT,UPDATE ON globus_app.t4304_rpa_tolerance TO globus_power_app ;
GRANT SELECT,INSERT,UPDATE ON globus_app.t4305_rpa_additional_charges TO globus_power_app ;
GRANT SELECT,INSERT,UPDATE ON globus_app.t4306_rpa_add_charge_mapping TO globus_power_app ;

GRANT SELECT,INSERT,UPDATE ON globus_app.t4303b_rpa_invoice_details_log TO globus_power_app ;
GRANT SELECT,INSERT,UPDATE ON globus_app.t4307_rpa_invoice_hdr_log  TO globus_power_app ;
GRANT SELECT,INSERT,UPDATE ON globus_app.t4308_rpa_email_hdr_log TO globus_power_app ;

GRANT SELECT ON globus_app.v4301_rpa_email_attachement_hdr TO globus_power_app ;

GRANT SELECT ON globus_app.t406_payment TO globus_power_app ;
GRANT SELECT ON globus_app.t401_purchase_order TO globus_power_app ;
GRANT SELECT ON globus_app.t402_work_order TO globus_power_app ;
GRANT SELECT ON globus_app.t301_vendor TO globus_power_app ;

GRANT EXECUTE ON globus_app.gm_rpa_invoice_updater TO globus_power_app ;


-- Synonym for RPA Tables
CREATE OR REPLACE PUBLIC SYNONYM t4300_rpa_email_hdr FOR globus_app.t4300_rpa_email_hdr;
CREATE OR REPLACE PUBLIC SYNONYM t4301_rpa_email_attachment_hdr FOR globus_app.t4301_rpa_email_attachment_hdr;
CREATE OR REPLACE PUBLIC SYNONYM t4302_rpa_invoice_hdr FOR globus_app.t4302_rpa_invoice_hdr;
CREATE OR REPLACE PUBLIC SYNONYM t4303_rpa_invoice_details FOR globus_app.t4303_rpa_invoice_details;
CREATE OR REPLACE PUBLIC SYNONYM t4303a_rpa_invoice_details_temp FOR globus_app.t4303a_rpa_invoice_details_temp;
CREATE OR REPLACE PUBLIC SYNONYM t4304_rpa_tolerance FOR globus_app.t4304_rpa_tolerance;
CREATE OR REPLACE PUBLIC SYNONYM t4305_rpa_additional_charges FOR globus_app.t4305_rpa_additional_charges;
CREATE OR REPLACE PUBLIC SYNONYM t4306_rpa_add_charge_mapping FOR globus_app.t4306_rpa_add_charge_mapping;

CREATE OR REPLACE PUBLIC SYNONYM t4303b_rpa_invoice_details_log FOR globus_app.t4303b_rpa_invoice_details_log;
CREATE OR REPLACE PUBLIC SYNONYM t4307_rpa_invoice_hdr_log FOR globus_app.t4307_rpa_invoice_hdr_log;
CREATE OR REPLACE PUBLIC SYNONYM t4308_rpa_email_hdr_log FOR globus_app.t4308_rpa_email_hdr_log;

-- Synonym for RPA pkg/proc/view
CREATE OR REPLACE PUBLIC SYNONYM gm_pkg_rpa_ap_invoice_validator FOR globus_app.gm_pkg_rpa_ap_invoice_validator;   
CREATE OR REPLACE PUBLIC SYNONYM gm_rpa_invoice_updater FOR globus_app.gm_rpa_invoice_updater;
CREATE OR REPLACE PUBLIC SYNONYM v4301_rpa_email_attachement_hdr FOR globus_app.v4301_rpa_email_attachement_hdr;