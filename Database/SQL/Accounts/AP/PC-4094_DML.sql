-- Set how many days old emails to be processed. By default take emails 7 days older from today
INSERT
INTO t906_rules
  (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_value,
    C906_RULE_GRP_ID,
    c906_created_date,
    c906_created_by
  )
  VALUES
  (
    s906_rule.NEXTVAL,
    'EMAIL_DAYS_SINCE',
    '7',
    'RPA_INVOICER',
    SYSDATE,
    941711
  );