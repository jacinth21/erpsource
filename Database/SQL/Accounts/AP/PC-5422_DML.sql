DELETE t906_rules where c906_rule_grp_id in ('RPA_VENDOR_GROUP');

set scan off;
INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) 
VALUES (s906_rule.NEXTVAL,'334','AP_Invoice_Automation_Model','Axial-Medical.com','RPA_VENDOR_GROUP',SYSDATE,941711);

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) 
VALUES (s906_rule.NEXTVAL,'362','AP_Invoice_Automation_Model','ferrymachine.com','RPA_VENDOR_GROUP',SYSDATE,941711);

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) 
VALUES (s906_rule.NEXTVAL,'290','AP_Invoice_Automation_Model','jandbprecision.com','RPA_VENDOR_GROUP',SYSDATE,941711);

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) 
VALUES (s906_rule.NEXTVAL,'145','AP_Invoice_Automation_Model','jjprecisiontech.com','RPA_VENDOR_GROUP',SYSDATE,941711);

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) 
VALUES (s906_rule.NEXTVAL,'122','AP_Invoice_Group0_Model','flex-cellinc.com','RPA_VENDOR_GROUP',SYSDATE,941711);

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) 
VALUES (s906_rule.NEXTVAL,'61','AP_Invoice_Group0_Model','firstarticleinc.com','RPA_VENDOR_GROUP',SYSDATE,941711);

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) 
VALUES (s906_rule.NEXTVAL,'150','AP_Invoice_Group0_Model','Tecomet.com','RPA_VENDOR_GROUP',SYSDATE,941711);

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) 
VALUES (s906_rule.NEXTVAL,'77','AP_Invoice_Group0_Model','archgp.com','RPA_VENDOR_GROUP',SYSDATE,941711);

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) 
VALUES (s906_rule.NEXTVAL,'114','AP_Invoice_Group0_Model','carolinaprecision.com','RPA_VENDOR_GROUP',SYSDATE,941711);

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) 
VALUES (s906_rule.NEXTVAL,'454','AP_Invoice_Group0_Model','culvertool.com','RPA_VENDOR_GROUP',SYSDATE,941711);

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) 
VALUES (s906_rule.NEXTVAL,'68','AP_Invoice_Group0_Model','vicpre.com','RPA_VENDOR_GROUP',SYSDATE,941711);

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) 
VALUES (s906_rule.NEXTVAL,'137','AP_Invoice_Group1_Model','westwoodprecisionllc.com','RPA_VENDOR_GROUP',SYSDATE,941711);

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) 
VALUES (s906_rule.NEXTVAL,'353','AP_Invoice_Group1_Model','elitemedical.healthcare','RPA_VENDOR_GROUP',SYSDATE,941711);

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) 
VALUES (s906_rule.NEXTVAL,'170','AP_Invoice_Group1_Model','cwswiss.com','RPA_VENDOR_GROUP',SYSDATE,941711);

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) 
VALUES (s906_rule.NEXTVAL,'277','AP_Invoice_Group1_Model','efgroup.com','RPA_VENDOR_GROUP',SYSDATE,941711);

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) 
VALUES (s906_rule.NEXTVAL,'377','AP_Invoice_Group1_Model','engmedsys.net','RPA_VENDOR_GROUP',SYSDATE,941711);

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) 
VALUES (s906_rule.NEXTVAL,'597','AP_Invoice_Group1_Model','aziyo.com','RPA_VENDOR_GROUP',SYSDATE,941711);

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) 
VALUES (s906_rule.NEXTVAL,'249','AP_Invoice_Group1_Model','wilseytool.com','RPA_VENDOR_GROUP',SYSDATE,941711);

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) 
VALUES (s906_rule.NEXTVAL,'477','AP_Invoice_Group1_Model','titanmedicalmfg.com','RPA_VENDOR_GROUP',SYSDATE,941711);

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_rule_grp_id,c906_created_date,c906_created_by) 
VALUES (s906_rule.NEXTVAL,'191','AP_Invoice_Group1_Model','edwardsprinting.com','RPA_VENDOR_GROUP',SYSDATE,941711);

