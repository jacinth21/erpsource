--- PMT-32391 : Secuirty Event Needed for spineIT Screen
--  Author    : N RAJA

INSERT 
  INTO T1520_FUNCTION_LIST
       (C1520_FUNCTION_ID, 
        C1520_FUNCTION_NM, 
        C1520_FUNCTION_DESC, 
        C1520_INHERITED_FROM_ID, 
        C901_TYPE, 
        C1520_VOID_FL, 
        C1520_CREATED_BY, 
        C1520_CREATED_DATE, 
        C1520_LAST_UPDATED_BY, 
        C1520_LAST_UPDATED_DATE, 
        C1520_REQUEST_URI, 
        C1520_REQUEST_STROPT, 
        C1520_SEQ_NO, 
        C901_DEPARTMENT_ID, 
        C901_LEFT_MENU_TYPE, 
        C1520_LAST_UPDATED_DATE_UTC )
  VALUES
       ('CURR_CONV_EDIT_ACC', 
        'CURR_CONV_EDIT_ACC',
        'Users in this security event will have the edit access for currency conversion report',
         null,
         92267,
         null,
         '303510',
         CURRENT_DATE,
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         CURRENT_DATE
        );
