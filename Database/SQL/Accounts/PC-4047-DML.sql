--PC-4047 Display Belgium Luxembourg invoice 
INSERT INTO t9101_print_template (
    c9101_print_template_id,
    c1900_company_id,
    c9101_portal_template_name,
    c9101_pdf_template_name,
    c9101_last_updated_by,
    c9101_last_updated_date
) VALUES (
    S9101_PRINT_TEMPLATE.nextval,
    1004,
    '/GmBelgiumLuxInvoicePrint.jasper',
    '/GmBelgiumLuxInvoicePrint.jasper',
    706310,
    current_date
);

INSERT INTO t9102_print_template_map (
    c9102_print_template_map_id,
    c101_party_id,
    c9101_print_template_id,
    c1900_company_id,
    c901_currency,
    c9102_last_updated_by,
    c9102_last_updated_date
)  select S9102_PRINT_TEMPLATE_MAP.nextval,9212,c9101_print_template_id,
          c1900_company_id,null,c9101_last_updated_by,c9101_last_updated_date
     from t9101_print_template 
    where c9101_portal_template_name ='/GmBelgiumLuxInvoicePrint.jasper' 
      and  c1900_company_id = 1004 
      and  c9101_void_fl is null; 
      
INSERT INTO t9102_print_template_map (
    c9102_print_template_map_id,
    c101_party_id,
    c9101_print_template_id,
    c1900_company_id,
    c901_currency,
    c9102_last_updated_by,
    c9102_last_updated_date
)  select S9102_PRINT_TEMPLATE_MAP.nextval,17517,c9101_print_template_id, 
          c1900_company_id,null,c9101_last_updated_by,c9101_last_updated_date
     from t9101_print_template 
    where c9101_portal_template_name ='/GmBelgiumLuxInvoicePrint.jasper' 
      and  c1900_company_id = 1004 
      and  c9101_void_fl is null;

INSERT INTO t9102_print_template_map (
    c9102_print_template_map_id,
    c101_party_id,
    c9101_print_template_id,
    c1900_company_id,
    c901_currency,
    c9102_last_updated_by,
    c9102_last_updated_date
)  select S9102_PRINT_TEMPLATE_MAP.nextval,27094,c9101_print_template_id,   
          c1900_company_id,null,c9101_last_updated_by,c9101_last_updated_date
     from t9101_print_template 
    where c9101_portal_template_name ='/GmBelgiumLuxInvoicePrint.jasper' 
      and  c1900_company_id = 1004 
      and  c9101_void_fl is null;
      
INSERT INTO t9102_print_template_map (
    c9102_print_template_map_id,
    c101_party_id,
    c9101_print_template_id,
    c1900_company_id,
    c901_currency,
    c9102_last_updated_by,
    c9102_last_updated_date
)  select S9102_PRINT_TEMPLATE_MAP.nextval,34685,c9101_print_template_id,
          c1900_company_id,null,c9101_last_updated_by,c9101_last_updated_date
     from t9101_print_template 
    where c9101_portal_template_name ='/GmBelgiumLuxInvoicePrint.jasper' 
      and  c1900_company_id = 1004 
      and  c9101_void_fl is null;