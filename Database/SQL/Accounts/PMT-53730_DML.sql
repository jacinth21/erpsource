INSERT INTO t901b_code_group_master (
    c901b_code_grp_id,
    c901b_code_grp,
    c901b_code_description,
    c901b_lookup_needed,
    c901b_created_by,
    c901b_created_date
) VALUES (
    s901b_code_group_master.NEXTVAL,
    'DOPOST',
    'Sales Order - PO Status',
    '',
    '706322',
    current_date
);

INSERT INTO t901_code_lookup (
    c901_code_id,
    c901_code_nm,
    c901_code_grp,
    c901_active_fl,
    c901_code_seq_no,
    c902_code_nm_alt,
    c901_control_type,
    c901_created_by,
    c901_created_date
) VALUES (
    109540,
    'Draft',
    'DOPOST',
    '1',
    '1',
    '',
    '',
    '706322',
    current_date
);

INSERT INTO t901_code_lookup (
    c901_code_id,
    c901_code_nm,
    c901_code_grp,
    c901_active_fl,
    c901_code_seq_no,
    c902_code_nm_alt,
    c901_control_type,
    c901_created_by,
    c901_created_date
) VALUES (
    109541,
    'Pending Approval',
    'DOPOST',
    '1',
    '2',
    '',
    '',
    '706322',
    current_date
);

INSERT INTO t901_code_lookup (
    c901_code_id,
    c901_code_nm,
    c901_code_grp,
    c901_active_fl,
    c901_code_seq_no,
    c902_code_nm_alt,
    c901_control_type,
    c901_created_by,
    c901_created_date
) VALUES (
    109542,
    'Ready for Invoice',
    'DOPOST',
    '1',
    '3',
    '',
    '',
    '706322',
    current_date
);

INSERT INTO t901_code_lookup (
    c901_code_id,
    c901_code_nm,
    c901_code_grp,
    c901_active_fl,
    c901_code_seq_no,
    c902_code_nm_alt,
    c901_control_type,
    c901_created_by,
    c901_created_date
) VALUES (
    109543,
    'Reject',
    'DOPOST',
    '1',
    '4',
    '',
    '',
    '706322',
    current_date
);

INSERT INTO t901_code_lookup (
    c901_code_id,
    c901_code_nm,
    c901_code_grp,
    c901_active_fl,
    c901_code_seq_no,
    c902_code_nm_alt,
    c901_control_type,
    c901_created_by,
    c901_created_date
) VALUES (
    109544,
    'Move to discrepancy',
    'DOPOST',
    '1',
    '5',
    '',
    '',
    '706322',
    current_date
);

INSERT INTO t901_code_lookup (
    c901_code_id,
    c901_code_nm,
    c901_code_grp,
    c901_active_fl,
    c901_code_seq_no,
    c902_code_nm_alt,
    c901_control_type,
    c901_created_by,
    c901_created_date
) VALUES (
    109545,
    'Invoice Gen. inprogress',
    'DOPOST',
    '1',
    '6',
    '',
    '',
    '706322',
    current_date
);

INSERT INTO t901_code_lookup (
    c901_code_id,
    c901_code_nm,
    c901_code_grp,
    c901_active_fl,
    c901_code_seq_no,
    c902_code_nm_alt,
    c901_control_type,
    c901_created_by,
    c901_created_date
) VALUES (
    109546,
    'Invoiced',
    'DOPOST',
    '1',
    '7',
    '',
    '',
    '706322',
    current_date
);

INSERT INTO t901_code_lookup (
    c901_code_id,
    c901_code_nm,
    c901_code_grp,
    c901_active_fl,
    c901_code_seq_no,
    c902_code_nm_alt,
    c901_control_type,
    c901_created_by,
    c901_created_date
) VALUES (
    109547,
    'TBE',
    'DOPOST',
    '0',
    '8',
    '',
    '',
    '706322',
    current_date
);

INSERT INTO t901_code_lookup (
    c901_code_id,
    c901_code_nm,
    c901_code_grp,
    c901_active_fl,
    c901_code_seq_no,
    c902_code_nm_alt,
    c901_control_type,
    c901_created_by,
    c901_created_date
) VALUES (
    109548,
    'TBE',
    'DOPOST',
    '0',
    '9',
    '',
    '',
    '706322',
    current_date
);

INSERT INTO t901_code_lookup (
    c901_code_id,
    c901_code_nm,
    c901_code_grp,
    c901_active_fl,
    c901_code_seq_no,
    c902_code_nm_alt,
    c901_control_type,
    c901_created_by,
    c901_created_date
) VALUES (
    109549,
    'TBE',
    'DOPOST',
    '0',
    '10',
    '',
    '',
    '706322',
    current_date
);

INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_desc,
    c906_rule_value,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c906_last_updated_date,
    c906_last_updated_by,
    c901_rule_type,
    c906_active_fl,
    c906_void_fl,
    c1900_company_id,
    c906_last_updated_date_utc
) VALUES (
    s906_rule.NEXTVAL,
    '107604',
    'Reject Sales Order PO',
    'gm_pkg_ar_po_trans.gm_sav_po_reject',
    current_date,
    'CMNCNCL',
    '706322',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
);

INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_desc,
    c906_rule_value,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c906_last_updated_date,
    c906_last_updated_by,
    c901_rule_type,
    c906_active_fl,
    c906_void_fl,
    c1900_company_id,
    c906_last_updated_date_utc
) VALUES (
    s906_rule.NEXTVAL,
    '107604',
    'Reject Sales Order PO',
    '5',
    current_date,
    'CMNCLPM',
    '706322',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
);

 INSERT INTO t901_code_lookup (
    c901_code_id,
    c901_code_nm,
    c901_code_grp,
    c901_active_fl,
    c901_code_seq_no,
    c902_code_nm_alt,
    c901_control_type,
    c901_created_by,
    c901_created_date
) VALUES (
    109550,
    'Reason 1',
    'REDOPO',
    '1',
    '1',
    '',
    '',
    '706322',
    current_date
);


INSERT INTO t901_code_lookup (
    c901_code_id,
    c901_code_nm,
    c901_code_grp,
    c901_active_fl,
    c901_code_seq_no,
    c902_code_nm_alt,
    c901_control_type,
    c901_created_by,
    c901_created_date
) VALUES (
    109551,
    'Others',
    'REDOPO',
    '1',
    '1',
    '',
    '',
    '706322',
    current_date
);