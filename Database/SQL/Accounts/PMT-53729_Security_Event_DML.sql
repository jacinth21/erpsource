INSERT
INTO t1520_function_list
  (
    c1520_function_id,
    c1520_function_nm,
    c1520_inherited_from_id,
    c1520_function_desc,
    c901_type,
    c1520_created_date,
    c1520_created_by
  )
  VALUES
  (
    'ORD_PO_ENTRY_ACC',
    'ORD_PO_ENTRY_ACC',
    'ORD_PO_ENTRY_ACC',
    'Users in this security event will restrict access right panel ',
    92267,
    sysdate,
    '706322'
  );
--delete from t1520_function_list where c1520_function_id='ORD_PO_ENTRY_ACC';
INSERT
INTO T1500_GROUP
  (
    C1500_GROUP_ID ,
    C1500_GROUP_NM,
    C1500_GROUP_DESC ,
    C1500_VOID_FL ,
    C1500_CREATED_BY,
    C1500_CREATED_DATE ,
    C901_GROUP_TYPE
  )
  VALUES
  (
    'ORD_PO_ENTRY_ACC',
    'ORD_PO_ENTRY_ACC',
    'Users in this security event will restrict access right panel ',
    NULL,
    '706322',
    CURRENT_DATE,
    92264
  );
