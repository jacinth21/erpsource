
	/*
select * From T901_CODE_LOOKUP where C901_CODE_NM like 'Vatican%';
select * From T704_ACCOUNT where C704_SHIP_CITY = '26240549' or C704_BILL_CITY = '26240549';

select * From T704_ACCOUNT where C704_SHIP_STATE = '26240549' or C704_BILL_STATE = '26240549';

select * From T704_ACCOUNT t704, T704A_ACCOUNT_ATTRIBUTE t704a
where t704.C704_ACCOUNT_ID = t704a.C704_ACCOUNT_ID
and t704.C1900_COMPANY_ID = 1020
and t704a.C901_ATTRIBUTE_TYPE = 101183;
*/




-- Vatican city template name updated
 INSERT
   INTO t9101_print_template
    (
        c9101_print_template_id, c1900_company_id, c9101_portal_template_name
      , c9101_pdf_template_name, c901_print_group, c901_print_layout
      , c9101_default_template_fl, c9101_last_updated_by, c9101_last_updated_date
    )
    VALUES
    (
        1, 1020, '/GmITInvoiceVaticanPrint.jasper'
      , '/pdf/GmITInvoiceVaticanPDFPrint.jasper', NULL, NULL
      , NULL, 706322, SYSDATE
    ) ;
        

	
-- Vatican city party map
 INSERT
   INTO t9102_print_template_map
    (
        c9102_print_template_map_id, c101_party_id, c9101_print_template_id
      , c1900_company_id, c9102_last_updated_by, c9102_last_updated_date
    )
    VALUES
    (
        1, NULL, 1
      , 1020, 706322, SYSDATE
    ) ;