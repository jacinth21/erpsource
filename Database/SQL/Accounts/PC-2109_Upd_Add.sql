--Update Address to t5040_plant_master
UPDATE t5040_plant_master 
   SET c5040_plant_name = 'Heathrow-UK',
       c5040_address_1 = 'Heathrow Boulervard 2',
       c5040_address_2 = '284 Bath road',
       c5040_city = 'West Drayton',
       c5040_zipcode = 'UB7 0DQ', 
       c5040_last_updated_date = current_date,
       c5040_last_updated_by = 2765071 
 WHERE c5040_plant_id = 3004
   AND c5040_void_fl is null;  
