INSERT INTO t1520_function_list
  (
    C1520_FUNCTION_ID,
    C1520_FUNCTION_NM,
    C1520_FUNCTION_DESC,
    C1520_INHERITED_FROM_ID,
    C901_TYPE,
    C1520_VOID_FL,
    C1520_CREATED_BY,
    C1520_CREATED_DATE,
    C1520_REQUEST_URI,
    C1520_REQUEST_STROPT,
    C1520_SEQ_NO,
    C901_DEPARTMENT_ID,
    C901_LEFT_MENU_TYPE,
    c1520_left_link_path
  )
  VALUES
  (
    s1520_function_list_task.nextval,
    'A/R Amount Report By Dealers',
    'A/R Amount Report by Dealers',
    NULL,
    92262,
    NULL,
    '303013',
    SYSDATE,
    '/gmARDealerRpt.do?method=loadFilters',
    NULL,
     NULL,
    '2000',
    '101920',
    'A/R >Reports >A/R Amount Report By Dealers'
  );