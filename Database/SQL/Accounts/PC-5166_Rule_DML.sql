--Enable Lot scanning for Netherland in process check screen
INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,c1900_company_id,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY)
VALUES (S906_RULE.NEXTVAL,'1010','New Process Check screen for Lot Tracking flag for Netherland','Y','1010',CURRENT_DATE,'LOT_TRK','2765071');


--- warehouse type - FG- Pick, FG- Bulk, Bulk Loose 
 DELETE FROM T906_RULES WHERE C906_RULE_ID = '3003' AND C906_RULE_GRP_ID = 'CC_SPLIT_WAREHOUSE' AND C906_VOID_FL IS NULL;

-- FG- Pick -- 106800   
 INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, '3003', 'Cycle count Warehouse Type FG- Pick'
      , '106800', SYSDATE, 'CC_SPLIT_WAREHOUSE'
      , '706328'
    ) ;
    
  --FG- Bulk - 106801
    INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, '3003', 'Cycle count Warehouse Type FG- Bulk'
      , '106801', SYSDATE, 'CC_SPLIT_WAREHOUSE'
      , '706328'
    ) ;
    
    --Bulk Loose -106808
     INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, '3003', 'Cycle count Warehouse Type Bulk Loose'
      , '106808', SYSDATE, 'CC_SPLIT_WAREHOUSE'
      , '706328'
    ) ;