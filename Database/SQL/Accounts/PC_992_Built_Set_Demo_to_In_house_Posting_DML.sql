-- Demo CN type

--select * From T906_RULES where C906_RULE_GRP_ID = 'DEMO_CN_TYPE';

 INSERT
   INTO T906_Rules
    (
        C906_Rule_Seq_Id, C906_Rule_Id, C906_Rule_Desc
      , C906_Rule_Value, C906_Created_Date, C906_Rule_Grp_Id
      , C906_Created_By
    )
    VALUES
    (
        S906_Rule.Nextval, '4119', 'Demo CN to In-house CN posting'
      , 'Y', CURRENT_DATE, 'DEMO_CN_TYPE'
      , 706322
    ) ;
    
    INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26241311,'Built Set Demo to Inhouse','TXNTP','1','','','','706322',CURRENT_DATE);

-- T804 entry

--select * From T804_POSTING_REF where C901_TRANSACTION_TYPE IN (26241311,  26240364);

 INSERT
   INTO T804_POSTING_REF
    (
        C804_POSTING_REF_ID, C901_TRANSACTION_TYPE, C901_ACCOUNT_TYPE_CR
      , C901_ACCOUNT_TYPE_DR, C901_POSTING_TYPE, C901_FROM_COSTING_TYPE
      , C901_TO_COSTING_TYPE, C804_DELETE_FL, C804_CREATED_BY
      , C804_CREATED_DATE
    )
    VALUES
    (
        S804_POSTING_REF.NEXTVAL, 26241311, 26240105
      , 4857, 4807, 26240103
      , 4903, 'N', 706322
      , SYSDATE
    ) ;

    
--select * From T906_RULES where C906_RULE_GRP_ID = 'INHOUSE-LOANER';

 INSERT
   INTO T906_Rules
    (
        C906_Rule_Seq_Id, C906_Rule_Id, C906_Rule_Desc
      , C906_Rule_Value, C906_Created_Date, C906_Rule_Grp_Id
      , C906_Created_By
    )
    VALUES
    (
        S906_Rule.Nextval, '4078', 'Built set Demo CN to In-house CN posting'
      , '26241311', CURRENT_DATE, 'INHOUSE-LOANER'
      , 706322
    ) ;