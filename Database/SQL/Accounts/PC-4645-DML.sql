SET serveroutput ON;
				
DECLARE
 v_count     NUMBER := 0;
 		
 CURSOR cur_pnum IS
  SELECT MY_TEMP_TXN_ID pnum , MY_TEMP_TXN_KEY adminref, MY_TEMP_TXN_VALUE mgmtdata  FROM MY_TEMP_KEY_VALUE ;
   				
BEGIN

-- Looping ipad orders
    
	FOR xml_val IN  cur_pnum
    LOOP
    BEGIN  
      Update t2053_part_elements  set c2053_admin_ref = xml_val.adminref,c2053_other_mgmt_data = xml_val.mgmtdata,
             c2053_updated_by = '30301', c2053_updated_date= current_date 
       where c205_part_number_id = xml_val.pnum 
         and c2053_void_fl is null;
      
    IF (SQL%ROWCOUNT = 0)
		THEN
        
        insert into t2053_part_elements (C205_PART_NUMBER_ID,C2053_ADMIN_REF,C2053_OTHER_MGMT_DATA,C1900_COMPANY_ID,C2053_UPDATED_BY,C2053_UPDATED_DATE)
        Values (xml_val.pnum,xml_val.adminref,xml_val.mgmtdata,1020,30301,current_date);
        
    END IF;    
    	
      v_count:=v_count+1;   
     EXCEPTION
          WHEN OTHERS THEN
           DBMS_OUTPUT.PUT_LINE('Part number------------------------------ ' || xml_val.pnum  ||' Error'|| SQLERRM);  
     END;
    END LOOP;
      delete from MY_TEMP_KEY_VALUE;
      DBMS_OUTPUT.PUT_LINE('Total rows updated------------------------------ ' || v_count );     

END;
/
