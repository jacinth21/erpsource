 DELETE FROM T906_RULES WHERE C906_RULE_ID = 'CYC_CNT_ID_PREFIX' AND C906_RULE_GRP_ID = 'CYC_CNT_LT_LOCK' AND C906_VOID_FL IS NULL;
 -- 
INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID,C1900_COMPANY_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'CYC_CNT_ID_PREFIX', 'Cycle count lot Track'
      , 'CC-LT-', SYSDATE, 'CYC_CNT_LT_LOCK',1000
      , '706328'
    ) ;

    
----    NEDC 
 INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID,C1900_COMPANY_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'CYC_CNT_ID_PREFIX', 'NEDC plant Cycle Count Lot Track prefix'
      , 'CC-LT-', SYSDATE, 'CYC_CNT_LT_LOCK', 1010
      , '706328'
    ) ;
    
  ---
  
 INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID,C1900_COMPANY_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'CYC_CNT_ID_PREFIX', 'FG Pick Inventory- Plus Cycle Count Lot Track prefix'
      , 'CC-LT-', SYSDATE, 'CYC_CNT_LT_LOCK', 1018
      , '706328'
    ) ;
    
    
 -- Japan   
   INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID,C1900_COMPANY_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'CYC_CNT_ID_PREFIX', 'Japan plant Cycle Count Lot Track prefix'
      , 'CC-LT-', SYSDATE, 'CYC_CNT_LT_LOCK', 1026
      , '706328'
    ) ;
    
   INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID,C1900_COMPANY_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'CYC_CNT_ID_PREFIX', 'Cycle count lot Track'
      , 'CC-LT-', SYSDATE, 'CYC_CNT_LT_LOCK',1001
      , '706328'
    ) ;
    
    
    
--- warehouse type - FG- Pick, FG- Bulk, Bulk Loose 
 DELETE FROM T906_RULES WHERE C906_RULE_ID = '3000' AND C906_RULE_GRP_ID = 'CC_SPLIT_WAREHOUSE' AND C906_VOID_FL IS NULL;

 -- FG- Pick -- 106800   
 INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, '3000', 'Cycle count Warehouse Type FG- Pick'
      , '106800', SYSDATE, 'CC_SPLIT_WAREHOUSE'
      , '706328'
    ) ;


    
  --create synonymn for PC-3494
CREATE OR REPLACE  public synonym gm_pkg_ac_cycle_cnt_lot FOR GLOBUS_APP.gm_pkg_ac_cycle_cnt_lot;
  
  --create synonymn
CREATE OR REPLACE public synonym t2708_cycle_count_scanned_lots for GLOBUS_APP.t2708_cycle_count_scanned_lots;
  
