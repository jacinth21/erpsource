UPDATE t503_invoice 
   SET c701_distributor_id = null,
       c503_last_updated_by = '30301',
       c503_last_updated_date =  current_date
 WHERE c503_invoice_id in 
 	   		(SELECT t503.c503_invoice_id
               FROM T501_ORDER t501 ,T503_INVOICE t503
              WHERE c501_order_id not in 
              		(SELECT c501_order_id 
              		   FROM T504C_CONSIGNMENT_ICT_DETAIL 
              		  WHERE c504c_void_fl IS NULL)
            	AND t503.c503_invoice_id = t501.c503_invoice_id  
           		AND c501_parent_order_id IS NULL
         		AND t503.c503_invoice_id = t501.c503_invoice_id  
         		AND t501.c501_parent_order_id IS  NULL
         		AND t503.C901_INVOICE_SOURCE = 50253 --ICS
      			AND t503.c701_distributor_id IS NOT NULL
        		AND t501.c503_invoice_id  IS NOT NULL
           		AND c501_void_fl IS NULL               
           		AND c503_void_fl IS NULL   
	UNION
             SELECT t503.c503_invoice_id
          	   FROM T501_ORDER t501 ,T503_INVOICE t503
              WHERE c501_parent_order_id not in 
              		(SELECT c501_order_id 
              		   FROM T504C_CONSIGNMENT_ICT_DETAIL 
              		  WHERE c504c_void_fl IS NULL)  
         		AND t503.c503_invoice_id = t501.c503_invoice_id  
         		AND t501.c501_parent_order_id IS NOT NULL
         		AND t503.C901_INVOICE_SOURCE = 50253 --ICS
         		AND t503.c701_distributor_id IS NOT NULL
        		AND t501.c503_invoice_id  IS NOT NULL
           		AND c501_void_fl IS NULL               
           		AND c503_void_fl IS NULL);