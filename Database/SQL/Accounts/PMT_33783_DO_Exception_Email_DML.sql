 INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        s906_rule.nextval, 'DO_SUMMARY_EXCEPTION', 'DO Summary batch exception'
      , 'SShea@globusmedical.com', SYSDATE, 'JMS_EXCEP_NOTIFY'
      , 706322
    ) ;
