 --create sysnonymn for PMT-53710
create or replace public synonym t5003_order_revenue_sample_dtls for GLOBUS_APP.t5003_order_revenue_sample_dtls;
create or replace public synonym gm_pkg_ac_ar_revenue_sampling for GLOBUS_APP.gm_pkg_ac_ar_revenue_sampling;
create or replace public synonym trg_t5003_order_revenue_log for GLOBUS_APP.trg_t5003_order_revenue_log;

