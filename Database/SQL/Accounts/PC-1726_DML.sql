-- Patient sticker keyword
INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID,c1900_company_id
      , C906_CREATED_BY, C906_LAST_UPDATED_DATE, C906_LAST_UPDATED_BY
    )
    VALUES
    (
        s906_rule.nextval, 'PAT_STICK_KEYWORDS', 'Patient sticker keywords for GMNA company'
      , '(?:.*MRN.*|.*DOB.*|.*HAR.*|.*CSN.*|.*ATT.*|.*ATD.*|.*ADMIT.*|.*ADM.*|.*FIN.*|.*DOS.*|.*MR.*|.*SUR.*|.*URN.*|.*SEX.*|.*FEMALE.*|.*MALE.*|.*YRS.*|.*ACT.*|M|F|AC.*)'
      ,CURRENT_DATE, 'AR_AUTO_REV_TRACK',1000
      , '303510', NULL, NULL
    ) ;
   
   -- Auto revenue job, not able to access directory email notifiaction
   INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID,c1900_company_id
      , C906_CREATED_BY, C906_LAST_UPDATED_DATE, C906_LAST_UPDATED_BY
    )
    VALUES
    (
        s906_rule.nextval, 'AR_AUTO_REV_FROM', 'Auto revenue track file access notify email FROM'
      , 'spineitcomms@spineit.net'
      ,CURRENT_DATE, 'AR_AUTO_REV_EMAIL',1000
      , '303510', NULL, NULL
    ) ;
    
    
     INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID,c1900_company_id
      , C906_CREATED_BY, C906_LAST_UPDATED_DATE, C906_LAST_UPDATED_BY
    )
    VALUES
    (
        s906_rule.nextval, 'AR_AUTO_REV_TO', 'Auto revenue track file access notify email TO'
      , 'pakurathi@globusmedical.com'
      ,CURRENT_DATE, 'AR_AUTO_REV_EMAIL',1000
      , '303510', NULL, NULL
    ) ;
    
     INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID,c1900_company_id
      , C906_CREATED_BY, C906_LAST_UPDATED_DATE, C906_LAST_UPDATED_BY
    )
    VALUES
    (
        s906_rule.nextval, 'AR_AUTO_REV_CC', 'Auto revenue track file access notify email CC'
      , 'mahavishnu@globusmedical.com'
      ,CURRENT_DATE, 'AR_AUTO_REV_EMAIL',1000
      , '303510', NULL, NULL
    ) ;
    
        
       INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID,c1900_company_id
      , C906_CREATED_BY, C906_LAST_UPDATED_DATE, C906_LAST_UPDATED_BY
    )
    VALUES
    (
        s906_rule.nextval, 'AR_AUTO_REV_SUBJECT', 'Auto revenue track file access notify email SUBJECT'
      , 'Globus Medical North America - AutoRevenueForiPadOrderJob(obsidian) not able to access DO pdf directory'
      ,CURRENT_DATE, 'AR_AUTO_REV_EMAIL',1000
      , '303510', NULL, NULL
    ) ;
    
    INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID,c1900_company_id
      , C906_CREATED_BY, C906_LAST_UPDATED_DATE, C906_LAST_UPDATED_BY
    )
    VALUES
    (
        s906_rule.nextval, 'AR_AUTO_REV_CONTYP', 'Auto revenue track file access notify email TYPE'
      , 'text/html'
      ,CURRENT_DATE, 'AR_AUTO_REV_EMAIL',1000
      , '303510', NULL, NULL
    ) ;
    
     INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID,c1900_company_id
      , C906_CREATED_BY, C906_LAST_UPDATED_DATE, C906_LAST_UPDATED_BY
    )
    VALUES
    (
        s906_rule.nextval, 'AR_AUTO_REV_BODY', 'Auto revenue track file access notify email BODY'
      , 'AutoRevenueForiPadOrderJob not able to access DO pdf directly , Please contact system administration'
      ,CURRENT_DATE, 'AR_AUTO_REV_EMAIL',1000
      , '303510', NULL, NULL
    ) ;
    
 -- Revenue category code lookup   
INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES
(s901b_code_group_master.NEXTVAL,'REVCA','Revenue Tracking Category','','2598779',CURRENT_DATE);
 


INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(110120,'Recognized','REVCA','1','1','','','2598779',CURRENT_DATE);
 

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(110121,'UnRecognized','REVCA','1','2','','','2598779',CURRENT_DATE);
 

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(110122,'UnRecognized- no PDF','REVCA','1','3','','','2598779',CURRENT_DATE);

 -- Revenue Aging code lookup

INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES
(s901b_code_group_master.NEXTVAL,'REVAG','Revenue Tracking Aging','','2598779',CURRENT_DATE);
 


INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(110123,'Today','REVAG','1','1','0','','2598779',CURRENT_DATE);
 

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(110124,'1 Day','REVAG','1','2','1','','2598779',CURRENT_DATE);
 

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(110125,'2 Days','REVAG','1','3','2','','2598779',CURRENT_DATE);
 

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(110126,'> 2 Days','REVAG','1','4','3','','2598779',CURRENT_DATE);


CREATE OR REPLACE PUBLIC SYNONYM gm_pkg_ar_auto_revenue_track FOR GLOBUS_APP.gm_pkg_ar_auto_revenue_track;
CREATE OR REPLACE PUBLIC SYNONYM T5005_AR_REVENUE_SCAN_DTLS FOR globus_app.T5005_AR_REVENUE_SCAN_DTLS;