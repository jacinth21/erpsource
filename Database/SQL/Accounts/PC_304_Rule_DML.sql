--NORWAY - Invoice Jasper file entry for division globus
INSERT INTO T906_RULES(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID, C906_CREATED_BY,
c906_last_updated_date,c906_last_updated_by,c901_rule_type,c906_active_fl,c906_void_fl,c1900_company_id,c906_last_updated_date_utc)
VALUES (S906_RULE.NEXTVAL,'1028_2000_REGULAR','Invoice template for Norway - Division as Globus','/GmNOInvoicePrint.jasper',CURRENT_DATE,'INVOICE_JASPER','303150',NULL,NULL,NULL,NULL, NULL,NULL,NULL);

--NORWAY - Invoice Jasper file entry for division algea
INSERT INTO T906_RULES(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID, C906_CREATED_BY,
c906_last_updated_date,c906_last_updated_by,c901_rule_type,c906_active_fl,c906_void_fl,c1900_company_id,c906_last_updated_date_utc)
VALUES (S906_RULE.NEXTVAL,'1028_2001_REGULAR','Invoice template for Norway - Division as Algea','/GmNOInvoicePrint.jasper',CURRENT_DATE,'INVOICE_JASPER','303150',NULL,NULL,NULL,NULL, NULL,NULL,NULL);

--INSERT script for invoice id prefix and sequence number
INSERT INTO globus_app.t503c_invoice_sequence (C503C_INVOICE_SEQUENCE_ID,C503C_INVOICE_PREFIX,C901_INVOICE_SOURCE,C503C_INVOICE_CURRVAL,C1900_COMPANY_ID,C503C_VOID_FL,C503C_LAST_UPDATED_BY,C503C_LAST_UPDATED_DATE,C503C_LAST_UPDATED_DATE_UTC) 
 values (24,'47-',108200,0,1028,null,'303150',current_date,null);
