INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_value,
    c906_rule_desc,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c1900_company_id
) VALUES (
    s906_rule.NEXTVAL,
    'PO_ORDTYPE',
    '2527',
    'Exclude Replacement Order in PO report',
    sysdate,
    'EXC_PO_ORD_TYP',
    303012,
    NULL
);

INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_value,
    c906_rule_desc,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c1900_company_id
) VALUES (
    s906_rule.NEXTVAL,
    'PO_ORDTYPE',
    '2531',
    'Exclude Loan-Consign Swap in PO report',
    sysdate,
    'EXC_PO_ORD_TYP',
    303012,
    NULL
);
INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_value,
    c906_rule_desc,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c1900_company_id
) VALUES (
    s906_rule.NEXTVAL,
    'PO_ORDTYPE',
    '2528',
    'Exclude Credit Memo in PO report',
    sysdate,
    'EXC_PO_ORD_TYP',
    303012,
    NULL
);
INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_value,
    c906_rule_desc,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c1900_company_id
) VALUES (
    s906_rule.NEXTVAL,
    'PO_ORDTYPE',
    '2524',
    'Exclude Price Adjustment Order in PO report',
    sysdate,
    'EXC_PO_ORD_TYP',
    303012,
    NULL
);
INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_value,
    c906_rule_desc,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c1900_company_id
) VALUES (
    s906_rule.NEXTVAL,
    'PO_ORDTYPE',
    '2523',
    'Exclude Return Order in PO report',
    sysdate,
    'EXC_PO_ORD_TYP',
    303012,
    NULL
);
INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_value,
    c906_rule_desc,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c1900_company_id
) VALUES (
    s906_rule.NEXTVAL,
    'PO_ORDTYPE',
    '2534',
    'Exclude ICT returns Order in PO report',
    sysdate,
    'EXC_PO_ORD_TYP',
    303012,
    NULL
);
INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_value,
    c906_rule_desc,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c1900_company_id
) VALUES (
    s906_rule.NEXTVAL,
    'PO_ORDTYPE',
    '102080',
    'Exclude OUS Distributor in PO report',
    sysdate,
    'EXC_PO_ORD_TYP',
    303012,
    NULL
);
INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_value,
    c906_rule_desc,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c1900_company_id
) VALUES (
    s906_rule.NEXTVAL,
    'PO_ORDTYPE',
    '102364',
    'Exclude Stock Transfer in PO report',
    sysdate,
    'EXC_PO_ORD_TYP',
    303012,
    NULL
);
INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_value,
    c906_rule_desc,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c1900_company_id
) VALUES (
    s906_rule.NEXTVAL,
    'PO_ORDTYPE',
    '26240234',
    'Exclude Install Order in PO report',
    sysdate,
    'EXC_PO_ORD_TYP',
    303012,
    NULL
);
INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_value,
    c906_rule_desc,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c1900_company_id
) VALUES (
    s906_rule.NEXTVAL,
    'PO_ORDTYPE',
    '2535',
    'Exclude Sales Discount in PO report',
    sysdate,
    'EXC_PO_ORD_TYP',
    303012,
    NULL
);
INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_value,
    c906_rule_desc,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c1900_company_id
) VALUES (
    s906_rule.NEXTVAL,
    'PO_ORDTYPE',
    '101260',
    'Exclude Acknowledgement Order in PO report',
    sysdate,
    'EXC_PO_ORD_TYP',
    303012,
    NULL
);
