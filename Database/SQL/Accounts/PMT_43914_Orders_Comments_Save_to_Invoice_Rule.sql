
INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY, C1900_COMPANY_ID
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'ORDERS_COMMENTS',
        'Italy Company - Invoice generated time to store the all the Orders comments to Invoice table', 'Y', SYSDATE
      , 'INVOICE', '706322', 1020
    ) ;
    
    