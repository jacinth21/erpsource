 INSERT
INTO T906_RULES
  (
    C906_RULE_SEQ_ID,
    C906_RULE_ID,
    C906_RULE_DESC,
    C906_RULE_VALUE,
    C906_CREATED_DATE,
    C906_RULE_GRP_ID,
    C906_CREATED_BY,
    C1900_COMPANY_ID
  )
  VALUES
  (
    S906_RULE.NEXTVAL,
    '1000',
    'count for spliting the batch',
    '50',
    CURRENT_DATE,
    'BATCH_SLT_CNT',
    '303510',
    '1000'
  );