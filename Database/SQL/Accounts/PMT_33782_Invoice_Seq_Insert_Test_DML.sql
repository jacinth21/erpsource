INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'INVSEQ','Invoice Sequence','','706322',CURRENT_DATE);

 
 
  
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108200,'Customer Invoice','INVSEQ','1','1','','','706322',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(108201,'Distributor Invoice','INVSEQ','1','2','','','706322',CURRENT_DATE);
 


set serveroutput on;
DECLARE
    v_invoice_seq     NUMBER := 1;
    v_inv_prefix      VARCHAR2 (20) ;
    v_inv_dist_seq_id NUMBER;
    v_msg VARCHAR2 (400);
    --
    CURSOR inv_seq_cur
    IS
         SELECT MY_TEMP_TXN_ID seq_name, MY_TEMP_TXN_KEY ref_id, MY_TEMP_TXN_VALUE seq_number
           FROM MY_TEMP_KEY_VALUE;
           
BEGIN
    -- delete the temp
     DELETE FROM MY_TEMP_KEY_VALUE;
     
    -- Insert the seq to temp table
     INSERT
       INTO MY_TEMP_KEY_VALUE
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT SEQUENCE_NAME, SUBSTR (SEQUENCE_NAME, 14, LENGTH (SEQUENCE_NAME)), LAST_NUMBER - 1
       FROM all_sequences
      WHERE SEQUENCE_NAME LIKE upper ('s503_invoice%')
        AND SEQUENCE_OWNER = 'GLOBUS_APP'
        -- to avoid the dist seq
        AND SEQUENCE_NAME <> 'S503_INVOICE_1000DIST';
    
    
    -- Loop the cursor
    
    FOR inv_seq IN inv_seq_cur
    LOOP
    
        -- to get the invoice prefix
         SELECT get_rule_value_by_company (inv_seq.ref_id, 'INVOICE_PREFIX_MAP', inv_seq.ref_id)
           INTO v_inv_prefix
           FROM DUAL;
           
        --   
        
        DBMS_OUTPUT.PUT_LINE ('Sequence Name :'|| inv_seq.seq_name || ' Sequence number :' || inv_seq.seq_number ||
        ' Invoice prefix :'||v_inv_prefix) ;
        
        -- 108200 Customer invoice
        
         INSERT
           INTO T503C_INVOICE_SEQUENCE
            (
                C503C_INVOICE_SEQUENCE_ID, C901_INVOICE_SOURCE, C503C_INVOICE_PREFIX
              , C503C_INVOICE_CURRVAL, C1900_COMPANY_ID, C503C_LAST_UPDATED_BY
              , C503C_LAST_UPDATED_DATE
            )
            VALUES
            (
                v_invoice_seq, 108200, v_inv_prefix
              , inv_seq.seq_number, inv_seq.ref_id, 706322
              , CURRENT_DATE
            ) ;
            
        --
        GM_UPDATE_LOG (inv_seq.ref_id, 'Invoice Sequence get from Oracle Seq and updated to new table :: Sequence Name :'|| inv_seq.seq_name || ' Sequence number :' || inv_seq.seq_number ||
        ' Invoice prefix :'||v_inv_prefix, 1201, 70633, v_msg );
        
        v_invoice_seq := v_invoice_seq + 1;
        
    END LOOP;
    
    -- Insert the DIST sequence
    
     SELECT LAST_NUMBER - 1
       INTO v_inv_dist_seq_id
       FROM all_sequences
      WHERE SEQUENCE_OWNER = 'GLOBUS_APP'
        AND SEQUENCE_NAME  = 'S503_INVOICE_1000DIST';
    --
     SELECT get_rule_value_by_company ('1000DIST', 'INVOICE_PREFIX_MAP', 1000)
       INTO v_inv_prefix
       FROM DUAL;
       
    --108200 Distributor invoice
    
      DBMS_OUTPUT.PUT_LINE ('Sequence Name :S503_INVOICE_1000DIST Sequence number :' || v_inv_dist_seq_id ||
        ' Invoice prefix :'||v_inv_prefix) ;
        
     INSERT
       INTO T503C_INVOICE_SEQUENCE
        (
            C503C_INVOICE_SEQUENCE_ID, C901_INVOICE_SOURCE, C503C_INVOICE_PREFIX
          , C503C_INVOICE_CURRVAL, C1900_COMPANY_ID, C503C_LAST_UPDATED_BY
          , C503C_LAST_UPDATED_DATE
        )
        VALUES
        (
            v_invoice_seq, 108201, v_inv_prefix
          , v_inv_dist_seq_id, 1000, 706322
          , CURRENT_DATE
        ) ;
        
       --
       GM_UPDATE_LOG ( '1000DIST', 'Invoice Sequence get from Oracle Seq and updated to new table :: Sequence Name :S503_INVOICE_1000DIST Sequence number :' || v_inv_dist_seq_id ||
        ' Invoice prefix :'||v_inv_prefix, 1201, 70633, v_msg );
        
END;
/