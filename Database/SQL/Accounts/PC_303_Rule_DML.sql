
--Rule Entry for Finland invoice paper work  - Division as Globus
INSERT INTO T906_RULES(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID, C906_CREATED_BY,
c906_last_updated_date,c906_last_updated_by,c901_rule_type,c906_active_fl,c906_void_fl,c1900_company_id,c906_last_updated_date_utc)
VALUES (S906_RULE.NEXTVAL,'1027_2000_REGULAR','Invoice template for Finland - Division as Globus','/GmFIInvoicePrint.jasper',CURRENT_DATE,'INVOICE_JASPER','303150',NULL,NULL,NULL,NULL, NULL,NULL,NULL);

--Rule Entry for Finland invoice paper work  - Division as Algea
INSERT INTO T906_RULES(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID, C906_CREATED_BY,
c906_last_updated_date,c906_last_updated_by,c901_rule_type,c906_active_fl,c906_void_fl,c1900_company_id,c906_last_updated_date_utc)
VALUES (S906_RULE.NEXTVAL,'1027_2001_REGULAR','Invoice template for Finland - Division as Algea','/GmFIInvoicePrint.jasper',CURRENT_DATE,'INVOICE_JASPER','303150',NULL,NULL,NULL,NULL, NULL,NULL,NULL);

-- Insertscript for Invoice id prefix and sequence number
INSERT INTO globus_app.t503c_invoice_sequence (C503C_INVOICE_SEQUENCE_ID,C503C_INVOICE_PREFIX,C901_INVOICE_SOURCE,C503C_INVOICE_CURRVAL,C1900_COMPANY_ID,C503C_VOID_FL,C503C_LAST_UPDATED_BY,C503C_LAST_UPDATED_DATE,C503C_LAST_UPDATED_DATE_UTC) 
 values (23,'358-',108200,0,1027,null,'303150',current_date,null);