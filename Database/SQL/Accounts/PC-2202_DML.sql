--Update company vat rate for germany
UPDATE t906_rules 
   SET c906_rule_value = '16',
       c906_last_updated_by = '2765071',
       c906_last_updated_date = current_date 
 WHERE c906_rule_grp_id = 'VAT_RATE' 
   AND c906_rule_id = '1008' 
   AND c1900_company_id = '1008'
   AND c906_void_fl is null;