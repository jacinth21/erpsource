--Update script for Euro symbloe to EUR for PC-3597
UPDATE t901_code_lookup
SET
    c901_code_nm = 'EUR',
    c901_last_updated_by = '303150',
    c901_last_updated_date = current_date
WHERE
    c901_code_id = '2'
and C901_VOID_FL is null;