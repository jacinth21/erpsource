SET serveroutput ON;
DECLARE
    v_company_id NUMBER;
    v_cnt        NUMBER;
    --
    CURSOR company_cur
    IS
         SELECT c1900_company_id comp_id
           FROM T1900_COMPANY
          WHERE C1900_COMPANY_ID <> 1026
            AND C1900_VOID_FL    IS NULL
       ORDER BY comp_id;
    --
    CURSOR account_dtls_cur
    IS
         SELECT t503.C704_ACCOUNT_ID acc_id, MAX (t508.C508_RECEIVED_DATE) last_payment_received_date
           FROM t508_receivables t508, t503_invoice t503
          WHERE t503.C503_INVOICE_ID         = t508.C503_INVOICE_ID
            AND t508.c508_received_mode NOT IN ('5013', '5014', '5019')
            AND t503.c1900_company_id        = v_company_id
            AND t503.C503_VOID_FL           IS NULL
       GROUP BY t503.C704_ACCOUNT_ID;
    --
    CURSOR dealer_dtls_cur
    IS
         SELECT t503.c101_dealer_id dealer_id, MAX (t508.C508_RECEIVED_DATE) last_payment_received_date
           FROM t508_receivables t508, t503_invoice t503
          WHERE t503.C503_INVOICE_ID         = t508.C503_INVOICE_ID
            AND t508.c508_received_mode NOT IN ('5013', '5014', '5019')
            AND t503.c1900_company_id        = 1026
            AND t503.C503_VOID_FL           IS NULL
       GROUP BY t503.c101_dealer_id;
    --
    CURSOR invoice_dtls_cur
    IS
         SELECT t503.C503_INVOICE_ID inv_id, MAX (t508.C508_RECEIVED_DATE) last_payment_received_date
           FROM t508_receivables t508, t503_invoice t503
          WHERE t503.C503_INVOICE_ID         = t508.C503_INVOICE_ID
            AND t508.c508_received_mode NOT IN ('5013', '5014', '5019')
            AND t503.c1900_company_id        = v_company_id
            AND t503.C503_VOID_FL           IS NULL
       GROUP BY t503.C503_INVOICE_ID;
BEGIN
    --
    FOR comp IN company_cur
    LOOP
        v_company_id := comp.comp_id;
        --
        v_cnt := 0;
        --
        DBMS_OUTPUT.put_line ('started the company id ' || v_company_id) ;
        --
        FOR acc_dtls IN account_dtls_cur
        LOOP
            --
            v_cnt := v_cnt + 1;
            -- to update the account
             UPDATE t704_account
            SET c704_last_payment_received_date = acc_dtls.last_payment_received_date, c704_last_updated_by = 706322,
                c704_last_updated_date          = CURRENT_DATE
              WHERE c704_account_id             = acc_dtls.acc_id ;
            --
        END LOOP;
        --
        DBMS_OUTPUT.put_line ('Total Account updated ===> ' || v_cnt) ;
        -- Invoice update
        v_cnt := 0;
        --
        FOR invoice IN invoice_dtls_cur
        LOOP
            --
            v_cnt := v_cnt + 1;
            --
             UPDATE t503_invoice
            SET c503_last_payment_received_date = invoice.last_payment_received_date, c503_last_updated_by = 706322,
                c503_last_updated_date          = CURRENT_DATE
              WHERE c503_invoice_id             = invoice.inv_id ;
        END LOOP;
        --
        DBMS_OUTPUT.put_line ('Total Invoice updated ===> ' || v_cnt) ;
    END LOOP;
    --
    -- Dealer
    v_cnt := 0;
    --
    FOR dealer IN dealer_dtls_cur
    LOOP
        --
        v_cnt := v_cnt + 1;
        --
         UPDATE t101_party_invoice_details
        SET c101_last_payment_received_date = dealer.last_payment_received_date, c101_last_updated_by = 706322,
            c101_last_updated_date          = CURRENT_DATE
          WHERE c101_party_id               = dealer.dealer_id ;
    END LOOP;
    DBMS_OUTPUT.put_line ('Total Dealer account updated ===> ' || v_cnt) ;
    --
    v_company_id := 1026;
    -- Dealer invoice
    --
    v_cnt := 0;
    --
    FOR invoice IN invoice_dtls_cur
    LOOP
        --
        v_cnt := v_cnt + 1;
        --
         UPDATE t503_invoice
        SET c503_last_payment_received_date = invoice.last_payment_received_date, c503_last_updated_by = 706322,
            c503_last_updated_date          = CURRENT_DATE
          WHERE c503_invoice_id             = invoice.inv_id ;
    END LOOP;
    --
    DBMS_OUTPUT.put_line ('Total Dealer invoice updated ===> ' || v_cnt) ;
END;
/ 