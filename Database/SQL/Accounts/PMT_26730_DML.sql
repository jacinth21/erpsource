
--Button acces for user Profile
   INSERT
   INTO t1520_function_list
    (
        C1520_FUNCTION_ID, C1520_FUNCTION_NM, C1520_FUNCTION_DESC,
        C1520_INHERITED_FROM_ID, C901_TYPE, C1520_VOID_FL,
        C1520_CREATED_BY, C1520_CREATED_DATE, C1520_LAST_UPDATED_BY,
        C1520_LAST_UPDATED_DATE, C1520_REQUEST_URI, C1520_REQUEST_STROPT,
        C1520_SEQ_NO, C901_DEPARTMENT_ID
    )    VALUES
    (
        'USER_PROF_BTN_ACCESS', 'UserProfile Button Access', 
        'Enable/disable userprofile buttons',
        NULL, 92267, NULL,
        '303013', SYSDATE, NULL,
        NULL, NULL, NULL,
        NULL, NULL
    ) ;
        
  --Button acces for vendor button access
   INSERT
   INTO t1520_function_list
    (
        C1520_FUNCTION_ID, C1520_FUNCTION_NM, C1520_FUNCTION_DESC,
        C1520_INHERITED_FROM_ID, C901_TYPE, C1520_VOID_FL,
        C1520_CREATED_BY, C1520_CREATED_DATE, C1520_LAST_UPDATED_BY,
        C1520_LAST_UPDATED_DATE, C1520_REQUEST_URI, C1520_REQUEST_STROPT,
        C1520_SEQ_NO, C901_DEPARTMENT_ID
    )    VALUES
    (
        'VENDOR_SETUP_ACS', 'Vendor SetUp Access', 
        'Enable/disable vendorsetup buttons',
        NULL, 92267, NULL,
        '303013', SYSDATE, NULL,
        NULL, NULL, NULL,
        NULL, NULL
    ) ;
    
    
    
   /*TO Enable/Disable Button Access-PMT 26730 -System Manager Changes*/
   INSERT
   INTO t1520_function_list
    (
        C1520_FUNCTION_ID, C1520_FUNCTION_NM, C1520_FUNCTION_DESC,
        C1520_INHERITED_FROM_ID, C901_TYPE, C1520_VOID_FL,
        C1520_CREATED_BY, C1520_CREATED_DATE, C1520_LAST_UPDATED_BY,
        C1520_LAST_UPDATED_DATE, C1520_REQUEST_URI, C1520_REQUEST_STROPT,
        C1520_SEQ_NO, C901_DEPARTMENT_ID
    )    VALUES
    (
        'SYS_ADMIN_BTN_ACS', 'System Admin Button Access', 
        'Enable/disable system Admin button access',
        NULL, 92267, NULL,
        '303013', SYSDATE, NULL,
        NULL, NULL, NULL,
        NULL, NULL
    ) ;