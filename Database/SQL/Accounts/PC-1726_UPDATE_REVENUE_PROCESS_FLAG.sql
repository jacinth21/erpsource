SET serveroutput ON;
				
DECLARE
 v_count     NUMBER := 0;
 		
 CURSOR cur_ord IS
  SELECT c501_order_id ord_id   FROM t501_order 
  		WHERE 	 c501_receive_mode = 26230683 
  			AND  c501_void_fl IS NULL
  		    AND	 trunc(c501_created_date) <= trunc(to_date('27-08-2020','dd-mm-YYYY')) 
  		     order by c501_created_date;	 -- 26230683 iPad	
  		        -- to avoid 28 Aug 2020 orders revenue flag update
 				
BEGIN

-- Looping ipad orders
	FOR  ord_cur IN  cur_ord
    LOOP
    
    -- Updating revenue track process flag as Y
    	UPDATE t501_order SET c501_revenue_track_process_fl='Y' 
    		 WHERE c501_order_id=ord_cur.ord_id
    		 	AND c501_void_fl is null;
    	
      v_count:=v_count+1;
      
      -- commit group by 1000 records
	  IF (v_count=1000) THEN
      commit;
      v_count:=0;
	  END IF;
    
    END LOOP;

	commit;

END;
/