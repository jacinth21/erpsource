--A/P New Invoice Access
INSERT INTO t1520_function_list (
    c1520_function_id,
    c1520_function_nm,
    c1520_function_desc,
    c1520_inherited_from_id,
    c901_type,
    c1520_void_fl,
    c1520_created_by,
    c1520_created_date,
    c1520_last_updated_by,
    c1520_last_updated_date,
    c1520_request_uri,
    c1520_request_stropt,
    c1520_seq_no,
    c901_department_id
) VALUES (
    'AP_NEW_INVOICE',
    'A/P New Invoice Access',
    'A/P New Invoice Access',
    NULL,
    92267,
    NULL,
    '706322',
    sysdate,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
);
--A/P Save Invoice Access
INSERT INTO t1520_function_list (
    c1520_function_id,
    c1520_function_nm,
    c1520_function_desc,
    c1520_inherited_from_id,
    c901_type,
    c1520_void_fl,
    c1520_created_by,
    c1520_created_date,
    c1520_last_updated_by,
    c1520_last_updated_date,
    c1520_request_uri,
    c1520_request_stropt,
    c1520_seq_no,
    c901_department_id
) VALUES (
    'AP_SAVE_INVOICE',
    'A/P Save Invoice Access',
    'A/P Save Invoice Access',
    NULL,
    92267,
    NULL,
    '706322',
    sysdate,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
);
--A/P void Invoice Access
INSERT INTO t1520_function_list (
    c1520_function_id,
    c1520_function_nm,
    c1520_function_desc,
    c1520_inherited_from_id,
    c901_type,
    c1520_void_fl,
    c1520_created_by,
    c1520_created_date,
    c1520_last_updated_by,
    c1520_last_updated_date,
    c1520_request_uri,
    c1520_request_stropt,
    c1520_seq_no,
    c901_department_id
) VALUES (
    'AP_VOID_INVOICE',
    'A/P void Invoice Access',
    'A/P void Invoice Access',
    NULL,
    92267,
    NULL,
    '706322',
    sysdate,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
);
--A/P Release Download Access
INSERT INTO t1520_function_list (
    c1520_function_id,
    c1520_function_nm,
    c1520_function_desc,
    c1520_inherited_from_id,
    c901_type,
    c1520_void_fl,
    c1520_created_by,
    c1520_created_date,
    c1520_last_updated_by,
    c1520_last_updated_date,
    c1520_request_uri,
    c1520_request_stropt,
    c1520_seq_no,
    c901_department_id
) VALUES (
    'AP_RELEASE_DOWNLOAD',
    'A/P Release Download Access',
    'A/P Release Download Access',
    NULL,
    92267,
    NULL,
    '706322',
    sysdate,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
);
--A/P Release Payment Access
INSERT INTO t1520_function_list (
    c1520_function_id,
    c1520_function_nm,
    c1520_function_desc,
    c1520_inherited_from_id,
    c901_type,
    c1520_void_fl,
    c1520_created_by,
    c1520_created_date,
    c1520_last_updated_by,
    c1520_last_updated_date,
    c1520_request_uri,
    c1520_request_stropt,
    c1520_seq_no,
    c901_department_id
) VALUES (
    'AP_RELEASE_PAYMENT',
    'A/P Release Payment Access',
    'A/P Release Payment Access',
    NULL,
    92267,
    NULL,
    '706322',
    sysdate,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
);
--A/P Roll Back Access
INSERT INTO t1520_function_list (
    c1520_function_id,
    c1520_function_nm,
    c1520_function_desc,
    c1520_inherited_from_id,
    c901_type,
    c1520_void_fl,
    c1520_created_by,
    c1520_created_date,
    c1520_last_updated_by,
    c1520_last_updated_date,
    c1520_request_uri,
    c1520_request_stropt,
    c1520_seq_no,
    c901_department_id
) VALUES (
    'AP_ROLLBACK_RELEASE',
    'A/P Roll Back Access',
    'A/P Roll Back Access',
    NULL,
    92267,
    NULL,
    '706322',
    sysdate,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
);


--PD Create Rule Access

INSERT INTO t1520_function_list (
    c1520_function_id,
    c1520_function_nm,
    c1520_function_desc,
    c1520_inherited_from_id,
    c901_type,
    c1520_void_fl,
    c1520_created_by,
    c1520_created_date,
    c1520_last_updated_by,
    c1520_last_updated_date,
    c1520_request_uri,
    c1520_request_stropt,
    c1520_seq_no,
    c901_department_id
) VALUES (
    'PD_CREATE_RULE',
    'PD Create Rule Access',
    'PD Create Rule Access',
    NULL,
    92267,
    NULL,
    '706322',
    sysdate,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
);

--PD Void Rule Access
INSERT INTO t1520_function_list (
    c1520_function_id,
    c1520_function_nm,
    c1520_function_desc,
    c1520_inherited_from_id,
    c901_type,
    c1520_void_fl,
    c1520_created_by,
    c1520_created_date,
    c1520_last_updated_by,
    c1520_last_updated_date,
    c1520_request_uri,
    c1520_request_stropt,
    c1520_seq_no,
    c901_department_id
) VALUES (
    'PD_VOID_RULE',
    'PD Void Rule Access',
    'PD Void Rule Access',
    NULL,
    92267,
    NULL,
    '706322',
    sysdate,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
);
--PD Override Rule Access
INSERT INTO t1520_function_list (
    c1520_function_id,
    c1520_function_nm,
    c1520_function_desc,
    c1520_inherited_from_id,
    c901_type,
    c1520_void_fl,
    c1520_created_by,
    c1520_created_date,
    c1520_last_updated_by,
    c1520_last_updated_date,
    c1520_request_uri,
    c1520_request_stropt,
    c1520_seq_no,
    c901_department_id
) VALUES (
    'PD_OVERRIDE_RULE',
    'PD Override Rule Access',
    'PD Override Rule Access',
    NULL,
    92267,
    NULL,
    '706322',
    sysdate,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
);


--SELECT *  FROM T1500_GROUP WHERE C1500_GROUP_ID IN ('5', '7', '8', '6', '9', '10') ;
-- to update the group type - security group

  UPDATE T1500_GROUP
SET C901_GROUP_TYPE      = 92264, C1500_LAST_UPDATED_BY = 706322, C1500_LAST_UPDATED_DATE = SYSDATE
  WHERE C1500_GROUP_ID  IN ('5', '8', '9', '10')
    AND C901_GROUP_TYPE IS NULL;
