--Create Left Link For Standard Cost Bulk Upload Screen
INSERT
   INTO t1520_function_list
    (
        C1520_FUNCTION_ID, C1520_FUNCTION_NM, C1520_FUNCTION_DESC
      , C1520_INHERITED_FROM_ID, C901_TYPE, C1520_VOID_FL
      , C1520_CREATED_BY, C1520_CREATED_DATE, C1520_LAST_UPDATED_BY
      , C1520_LAST_UPDATED_DATE, C1520_REQUEST_URI, C1520_REQUEST_STROPT
      , C1520_SEQ_NO, C901_DEPARTMENT_ID
    )
    VALUES
    (
        'STD_COST_BULK_UPL', 'Standard Cost Bulk Upload', 'load the Standard Cost Bulk Upload  screen for a Standard cost.'
      , NULL, 92262, NULL
      , '706322', SYSDATE, NULL
      , NULL, '/gmVendorStandardCost.do?method=loadStandardCostBulkUpload', null
      , NULL, 2000
    ) ;
    
    
-- Create New Security Event For Submit Button Access for Standard Cost Bulk Upload Button
    
INSERT INTO t1520_function_list (
    c1520_function_id,
    c1520_function_nm,
    c1520_function_desc,
    c1520_inherited_from_id,
    c901_type,
    c1520_void_fl,
    c1520_created_by,
    c1520_created_date,
    c1520_last_updated_by,
    c1520_last_updated_date,
    c1520_request_uri,
    c1520_request_stropt,
    c1520_seq_no,
    c901_department_id
) VALUES (
    'VEN_COST_BULK_UPL',
    'Vendor Cost Bulk Upload Screen Button Access',
    'Vendor Cost Bulk Upload Screen Button Access',
    NULL,
    92267,
    NULL,
    '706322',
    sysdate,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
);    