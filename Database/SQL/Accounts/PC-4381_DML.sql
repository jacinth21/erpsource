--To update shipper tax id for ICS distributor
update t906_rules set c906_rule_value='NL823991246B01',c906_last_updated_by=30301,c906_last_updated_date=current_date 
where c906_rule_grp_id in (select c101_party_intra_map_id from t701_distributor 
where c901_distributor_type=70105 and c701_void_fl is null) 
and c906_rule_id ='50210';
       	
--To update ICS Bank details
update t906_rules set c906_rule_value='383011362195',c906_last_updated_by=30301,c906_last_updated_date=current_date 
where c906_rule_grp_id ='IBAN' and c906_rule_id in ('569','16731');
update t906_rules set c906_rule_value='BOFAUS3N',c906_last_updated_by=30301,c906_last_updated_date=current_date 
where c906_rule_grp_id ='SWIFTCODE' and c906_rule_id in ('569','16731');
update t906_rules set c906_rule_value='Bank of America/ 222 Broadway/ New York, NY 10035/USA',c906_last_updated_by=30301,c906_last_updated_date=current_date 
where c906_rule_grp_id ='BANK' and c906_rule_id in ('569','16731');

INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,
C906_RULE_GRP_ID,C906_CREATED_BY) 
values (S906_RULE.NEXTVAL,'50253','Globus Tax/VAT ID','NL823991246B01',current_date,'ICS_SHIPPERTAX',
'30301');
