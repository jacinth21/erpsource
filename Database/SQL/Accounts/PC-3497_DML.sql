INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
VALUES (S906_Rule.Nextval,'3000','To get lot update procedure','gm_pkg_op_lot_track_data.gm_upd_lot_inventory',current_date,'CYC_CNT_LOT_PRC','706328');

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
VALUES (S906_Rule.Nextval,'3001','To get lot update procedure','gm_pkg_op_ld_lot_track.gm_sav_control_number_inv',current_date,'CYC_CNT_LOT_PRC','706328');

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
VALUES (S906_Rule.Nextval,'3002','To get lot update procedure','gm_pkg_op_lot_track_data.gm_upd_lot_inventory',current_date,'CYC_CNT_LOT_PRC','706328');

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
VALUES (S906_Rule.Nextval,'3003','To get lot update procedure','gm_pkg_op_lot_track_data.gm_upd_lot_inventory',current_date,'CYC_CNT_LOT_PRC','706328');

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
VALUES (S906_Rule.Nextval,'3004','To get lot update procedure','gm_pkg_op_lot_track_data.gm_upd_lot_inventory',current_date,'CYC_CNT_LOT_PRC','706328');

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
VALUES (S906_Rule.Nextval,'3005','To get lot update procedure','gm_pkg_op_lot_track_data.gm_upd_lot_inventory',current_date,'CYC_CNT_LOT_PRC','706328');

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
VALUES (S906_Rule.Nextval,'3006','To get lot update procedure','gm_pkg_op_lot_track_data.gm_upd_lot_inventory',current_date,'CYC_CNT_LOT_PRC','706328');

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
VALUES (S906_Rule.Nextval,'3007','To get lot update procedure','gm_pkg_op_lot_track_data.gm_upd_lot_inventory',current_date,'CYC_CNT_LOT_PRC','706328');

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
VALUES (S906_Rule.Nextval,'3008','To get lot update procedure','gm_pkg_op_lot_track_data.gm_upd_lot_inventory',current_date,'CYC_CNT_LOT_PRC','706328');

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
VALUES (S906_Rule.Nextval,'3011','To get lot update procedure','gm_pkg_op_lot_track_data.gm_upd_lot_inventory',current_date,'CYC_CNT_LOT_PRC','706328');

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
VALUES (S906_Rule.Nextval,'3013','To get lot update procedure','gm_pkg_op_lot_track_data.gm_upd_lot_inventory',current_date,'CYC_CNT_LOT_PRC','706328');

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
VALUES (S906_Rule.Nextval,'3014','To get lot update procedure','gm_pkg_op_lot_track_data.gm_upd_lot_inventory',current_date,'CYC_CNT_LOT_PRC','706328');

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
VALUES (S906_Rule.Nextval,'3017','To get lot update procedure','gm_pkg_op_lot_track_data.gm_upd_lot_inventory',current_date,'CYC_CNT_LOT_PRC','706328');

--Rule entry to get warehouse type based on warehouse location
INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
VALUES (S906_Rule.Nextval,'106808','Bulk Loose','90814',current_date,'CYC_CNT_LOT','706328');

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
VALUES (S906_Rule.Nextval,'106801','FG- Bulk','90800',current_date,'CYC_CNT_LOT','706328');

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
VALUES (S906_Rule.Nextval,'106800','FG- Pick','90800',current_date,'CYC_CNT_LOT','706328');

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
VALUES (S906_Rule.Nextval,'106809','In-house Loose','90816',current_date,'CYC_CNT_LOT','706328');

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
VALUES (S906_Rule.Nextval,'106812','In-house Set','90816',current_date,'CYC_CNT_LOT','706328');

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
VALUES (S906_Rule.Nextval,'106805','Quarantine','90813',current_date,'CYC_CNT_LOT','706328');

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
VALUES (S906_Rule.Nextval,'106806','Quarantine Sterile','90813',current_date,'CYC_CNT_LOT','706328');

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by) 
VALUES (S906_Rule.Nextval,'106804','Repack','90815',current_date,'CYC_CNT_LOT','706328');

--create Synonynm for new package
CREATE OR REPLACE PUBLIC SYNONYM gm_pkg_ac_cc_lot_txn FOR GLOBUS_APP.gm_pkg_ac_cc_lot_txn;