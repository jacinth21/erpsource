-- For Folder
INSERT 
     INTO T1520_FUNCTION_LIST 
                       ( C1520_FUNCTION_ID, C1520_FUNCTION_NM, C1520_FUNCTION_DESC
                       , C1520_INHERITED_FROM_ID, C901_TYPE, C1520_VOID_FL
                       , C1520_CREATED_BY, C1520_CREATED_DATE, C1520_LAST_UPDATED_BY
                       , C1520_LAST_UPDATED_DATE, C1520_REQUEST_URI, C1520_REQUEST_STROPT
                       , C1520_SEQ_NO, C901_DEPARTMENT_ID
                       ) 
                    VALUES 
                       (
                        'REBATE_MGMNT_RPT_FLD', 'Rebate Management Report', 'Rebate Management Report folder'
                        , null, 92262, null
                        ,'706322', SYSDATE, null
                        , null, null, null
                        , null, 2000
                       );  
           
 INSERT INTO T1530_ACCESS 
                  ( C1530_ACCESS_ID, C1500_GROUP_ID, C1520_FUNCTION_ID
                  , C101_PARTY_ID,C1530_UPDATE_ACCESS_FL,C1530_READ_ACCESS_FL
                  , C1530_VOID_ACCESS_FL,C1530_VOID_FL,C1530_CREATED_BY
                  , C1530_CREATED_DATE,C1530_LAST_UPDATED_BY,C1530_LAST_UPDATED_DATE
                  , C1520_PARENT_FUNCTION_ID,C1530_FUNCTION_NM,C1530_SEQ_NO
                  ) 
      VALUES
                  ( 
                    s1530_access.nextval, '100004', 'REBATE_MGMNT_RPT_FLD'
                  , null, 'Y', 'Y'
                  , null, null, null
                  , null, null, null
                  , '12274000', null, 204539510006
                  );    





--Left Link Script For Rebate Mangemnet Report
INSERT
INTO t1520_function_list
  (
    C1520_FUNCTION_ID,
    C1520_FUNCTION_NM,
    C1520_FUNCTION_DESC ,
    C1520_INHERITED_FROM_ID,
    C901_TYPE,
    C1520_VOID_FL ,
    C1520_CREATED_BY,
    C1520_CREATED_DATE,
    C1520_LAST_UPDATED_BY ,
    C1520_LAST_UPDATED_DATE,
    C1520_REQUEST_URI,
    C1520_REQUEST_STROPT ,
    C1520_SEQ_NO,
    C901_DEPARTMENT_ID
  )
  VALUES
  (
    s1520_function_list_task.nextval,
    'Rebate Management Report',
    'Rebate Management Report' ,
    NULL,
    92262,
    NULL ,
    '706322',
    SYSDATE,
    NULL ,
    NULL,
    '/gmRebateReport.do?method=loadRebateManagmentReport',
    NULL ,
    NULL,
    2000
  ) ;
  INSERT
INTO t901b_code_group_master
  (
    C901B_CODE_GRP_ID,
    C901B_CODE_GRP ,
    C901B_CODE_DESCRIPTION ,
    C901B_LOOKUP_NEEDED,
    C901B_CREATED_BY ,
    C901B_CREATED_DATE
  )
  VALUES
  (
    s901b_code_group_master.NEXTVAL,
    'RBDTTY',
    'Rebate Date Type',
    '',
    '3058724',
    CURRENT_DATE
  );
INSERT
INTO t901_code_lookup
  (
    c901_code_id,
    c901_code_nm,
    c901_code_grp,
    c901_active_fl,
    c901_code_seq_no,
    c902_code_nm_alt,
    c901_control_type,
    c901_created_by,
    c901_created_date
  )
  VALUES
  (
    107720,
    'Contract Date',
    'RBDTTY',
    '1',
    '1',
    '',
    '',
    '3058724',
    CURRENT_DATE
  );
INSERT
INTO t901_code_lookup
  (
    c901_code_id,
    c901_code_nm,
    c901_code_grp,
    c901_active_fl,
    c901_code_seq_no,
    c902_code_nm_alt,
    c901_control_type,
    c901_created_by,
    c901_created_date
  )
  VALUES
  (
    107721,
    'Effective Date',
    'RBDTTY',
    '1',
    '2',
    '',
    '',
    '3058724',
    CURRENT_DATE
  );