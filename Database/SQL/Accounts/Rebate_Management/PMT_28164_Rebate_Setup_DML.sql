
create or replace public synonym gm_pkg_sm_rebate_setup for GLOBUS_APP.gm_pkg_sm_rebate_setup;
create or replace public synonym gm_pkg_sm_rebate_report for GLOBUS_APP.gm_pkg_sm_rebate_report;

-- For Folder
INSERT 
     INTO T1520_FUNCTION_LIST 
                       ( C1520_FUNCTION_ID, C1520_FUNCTION_NM, C1520_FUNCTION_DESC
                       , C1520_INHERITED_FROM_ID, C901_TYPE, C1520_VOID_FL
                       , C1520_CREATED_BY, C1520_CREATED_DATE, C1520_LAST_UPDATED_BY
                       , C1520_LAST_UPDATED_DATE, C1520_REQUEST_URI, C1520_REQUEST_STROPT
                       , C1520_SEQ_NO, C901_DEPARTMENT_ID
                       ) 
                    VALUES 
                       (
                        'REBATE_MGMNT_FLD', 'Rebate Management', 'Rebate Management folder'
                        , null, 92262, null
                        ,'706322', SYSDATE, null
                        , null, null, null
                        , null, 2000
                       );  
           
 INSERT INTO T1530_ACCESS 
                  ( C1530_ACCESS_ID, C1500_GROUP_ID, C1520_FUNCTION_ID
                  , C101_PARTY_ID,C1530_UPDATE_ACCESS_FL,C1530_READ_ACCESS_FL
                  , C1530_VOID_ACCESS_FL,C1530_VOID_FL,C1530_CREATED_BY
                  , C1530_CREATED_DATE,C1530_LAST_UPDATED_BY,C1530_LAST_UPDATED_DATE
                  , C1520_PARENT_FUNCTION_ID,C1530_FUNCTION_NM,C1530_SEQ_NO
                  ) 
      VALUES
                  ( 
                    s1530_access.nextval, '100004', 'REBATE_MGMNT_FLD'
                  , null, 'Y', 'Y'
                  , null, null, null
                  , null, null, null
                  , '12273900', null, 20452903
                  );    



INSERT
   INTO t1520_function_list
    (
        C1520_FUNCTION_ID, C1520_FUNCTION_NM, C1520_FUNCTION_DESC
      , C1520_INHERITED_FROM_ID, C901_TYPE, C1520_VOID_FL
      , C1520_CREATED_BY, C1520_CREATED_DATE, C1520_LAST_UPDATED_BY
      , C1520_LAST_UPDATED_DATE, C1520_REQUEST_URI, C1520_REQUEST_STROPT
      , C1520_SEQ_NO, C901_DEPARTMENT_ID
    )
    VALUES
    (
        s1520_function_list_task.nextval, 'Create Rebate', 'Rebate Management'
      , NULL, 92262, NULL
      , '706322', SYSDATE, NULL
      , NULL, '/gmRebateSetup.do?method=loadRebateManagmentDtls', null
      , NULL, 2000
    ) ;
    --Access For Submit 
  INSERT
   INTO t1520_function_list
    (
        C1520_FUNCTION_ID, C1520_FUNCTION_NM, C1520_FUNCTION_DESC
      , C1520_INHERITED_FROM_ID, C901_TYPE, C1520_VOID_FL
      , C1520_CREATED_BY, C1520_CREATED_DATE, C1520_LAST_UPDATED_BY
      , C1520_LAST_UPDATED_DATE, C1520_REQUEST_URI, C1520_REQUEST_STROPT
      , C1520_SEQ_NO, C901_DEPARTMENT_ID
    )
    VALUES
    (
        'REBATE_SAVE_ACCESS', 'Rebate Setup Access', 'Rebate Setup Access'
      , NULL, 92267, NULL
      , '706322', SYSDATE, NULL
      , NULL, NULL,NULL
      , NULL, NULL
    ) ;
   
   
              
    -- create New rule for 107485 ,107484      
	INSERT 
	   INTO t906_rules
	   (
	   C906_RULE_SEQ_ID,C906_RULE_ID, C906_RULE_DESC,C906_RULE_VALUE, C906_CREATED_DATE,
	   C906_RULE_GRP_ID,C906_CREATED_BY, C906_LAST_UPDATED_DATE, C906_LAST_UPDATED_BY,
	   C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL
	   )
	VALUES
	(
	s906_rule.nextval,'107485', NULL,'999.500',SYSDATE,
	'REBATE_PARTS','30301',NULL, NULL,
	NULL,NULL,NULL
	);	

	INSERT
	  INTO t906_rules
	    (
	     C906_RULE_SEQ_ID,C906_RULE_ID, C906_RULE_DESC,C906_RULE_VALUE, C906_CREATED_DATE,
	     C906_RULE_GRP_ID,C906_CREATED_BY, C906_LAST_UPDATED_DATE, C906_LAST_UPDATED_BY,
	     C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL)
	VALUES
	(
	 s906_rule.nextval,'107484', NULL,'999.501',SYSDATE,
	 'REBATE_PARTS','30301',NULL, NULL,
	  NULL,NULL,NULL
	);
-- create new rule for Sales discount
	INSERT 
	   INTO t906_rules
	   (
	   C906_RULE_SEQ_ID,C906_RULE_ID, C906_RULE_DESC,C906_RULE_VALUE, C906_CREATED_DATE,
	   C906_RULE_GRP_ID,C906_CREATED_BY, C906_LAST_UPDATED_DATE, C906_LAST_UPDATED_BY,
	   C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL
	   )
	VALUES
	(
	s906_rule.nextval,'26240506', NULL,'999.502',SYSDATE,
	'REBATE_PARTS','30301',NULL, NULL,
	NULL,NULL,NULL
	);	
	
	
-- Create Code Id for Save Log Comments
 INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(26240503,'Rebate Management Setup','GPLOG','0','','','','706322',CURRENT_DATE);    

-- Rule for rebate id Prefix
INSERT
INTO t906_rules
  (
    C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,
    C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,
    C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,
    C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL
  )
  VALUES
  (
    s906_rule.nextval,'REBATE_ID_PREFIX',NULL,
    'RB-',SYSDATE,'REBATE',
    '30301',NULL,NULL,
    NULL,NULL,NULL
  );
  --Code group for Sales Discount
INSERT 
    INTO t901_code_lookup 
      (
      c901_code_id, c901_code_nm, c901_code_grp,
      c901_active_fl, c901_code_seq_no, c902_code_nm_alt, 
      c901_control_type, c901_created_by, c901_created_date
      ) 
      VALUES 
      (
      '26240506','Sales Discount','RBTYPE',
      '1','','','',
      '706322',CURRENT_DATE
      );