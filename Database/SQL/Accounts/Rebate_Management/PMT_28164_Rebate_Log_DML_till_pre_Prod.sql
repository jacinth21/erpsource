
INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'REBLOG','Rebate Audit Trail Log','','706322',CURRENT_DATE);
 
  
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(107702,'Rebate Part Type','REBLOG','1','1','','','706322',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(107703,'Rebate Category','REBLOG','1','2','','','706322',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(107704,'Rebate Rate','REBLOG','1','3','','','706322',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(107705,'Rebate effective Date','REBLOG','1','4','','','706322',CURRENT_DATE); 
