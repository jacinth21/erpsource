-- new rules


INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        s906_rule.nextval, 'REBATE_MASTER', 'To fetch the Rebate audit trail information'
      , 'gm_pkg_sm_rebate_report.gm_fch_rebate_log_dtls', SYSDATE, 'DYNAMIC_AUDIT_TRAIL'
      , 706322
    ) ;


    create or replace public synonym trg_t7200_rebate_master_log for GLOBUS_APP.trg_t7200_rebate_master_log;