--Addding default posting type for OUS Direct Order Sales Return
INSERT INTO T906_RULES(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID, C906_CREATED_BY,
c906_last_updated_date,c906_last_updated_by,c901_rule_type,c906_active_fl,c906_void_fl,c1900_company_id,c906_last_updated_date_utc)
VALUES (S906_RULE.NEXTVAL,'OUS_ORDER_RETURN','OUS Direct Order Sales Return Default Posting Type','26240474',CURRENT_DATE,'DEFAULT_POSTING_TYPE','2423914',NULL,NULL,NULL,NULL, NULL,NULL,NULL);

--Addding default posting type for OUS Direct Order Sales 
INSERT INTO T906_RULES(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID, C906_CREATED_BY,
c906_last_updated_date,c906_last_updated_by,c901_rule_type,c906_active_fl,c906_void_fl,c1900_company_id,c906_last_updated_date_utc)
VALUES (S906_RULE.NEXTVAL,'OUS_ORDER_SALES','OUS Direct Order Sales Default Posting Type','26240464',CURRENT_DATE,'DEFAULT_POSTING_TYPE','2423914',NULL,NULL,NULL,NULL, NULL,NULL,NULL);
