--Update the rule value of rep account type in AR_ACCOUNT_SOURCE rule group
--Norway
UPDATE t906_rules
   SET c906_rule_value = '70110,70111',
       c906_last_updated_by = '303150',
       c906_last_updated_date = current_date
WHERE c1900_company_id = 1028
  AND c906_rule_id = '50255';
    
--Finland
UPDATE t906_rules
   SET c906_rule_value = '70110,70111',
       c906_last_updated_by = '303150',
      c906_last_updated_date = current_date
WHERE c1900_company_id = 1027
  AND c906_rule_id = '50255';