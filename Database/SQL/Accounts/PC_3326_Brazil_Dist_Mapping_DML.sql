--1880	Globus Medical Brasil Ltda (ATEC)
--2737	Globus Medical Brasil Ltda (GMED)

 INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        s906_rule.nextval, '1019_1018', 'ICT Dist for HQ company - Brazil'
      , '1880', SYSDATE, 'OUS_ACCOUNT_DIST_MAP'
      , '706322'
    ) ;
    
    
INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        s906_rule.nextval, '1019_1000', 'ICT Dist for GMNA company - Brazil'
      , '2737', SYSDATE, 'OUS_ACCOUNT_DIST_MAP'
      , '706322'
    ) ;