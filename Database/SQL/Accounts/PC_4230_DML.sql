--PC-4230 Display GST number on NZ invoice 

INSERT INTO t9101_print_template (
    c9101_print_template_id,
    c1900_company_id,
    c9101_portal_template_name,
    c9101_pdf_template_name,
    c9101_last_updated_by,
    c9101_last_updated_date
) VALUES (
    S9101_PRINT_TEMPLATE.nextval,
    1002,
    '/GmNZDInvoicePrint.jasper',
    '/GmNZDInvoicePrint.jasper',
    706310,
    current_date
);
 
INSERT INTO t9102_print_template_map (
    c9102_print_template_map_id,
    c101_party_id,
    c9101_print_template_id,
    c1900_company_id,
    c901_currency,
    c9102_last_updated_by,
    c9102_last_updated_date
)  select S9102_PRINT_TEMPLATE_MAP.nextval,NULL,c9101_print_template_id,
          c1900_company_id,26240461,c9101_last_updated_by,c9101_last_updated_date
     from t9101_print_template 
    where c9101_portal_template_name ='/GmNZDInvoicePrint.jasper' 
      and  c1900_company_id = 1002  
      and  c9101_void_fl is null; 


