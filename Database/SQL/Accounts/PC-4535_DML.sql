DELETE FROM t906_rules
WHERE
    c906_rule_id = 1114
    AND c906_rule_grp_id = 'PAPER_WORK_BY_CNTRY'

INSERT INTO t906_rules (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_value,
    c906_rule_grp_id,
    c906_created_date,
    c906_created_by,
    c1900_company_id
) VALUES (
    s906_rule.NEXTVAL,
    100985,
    'Y',
    'PAPER_WORK_BY_CNTRY',
    current_date,
    '303012',
    1002
);