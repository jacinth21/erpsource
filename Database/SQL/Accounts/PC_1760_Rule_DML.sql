-- insert Rule for po reject email notify
Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'AR_REJECT_PO_CC','Reject PO email notification cc','notification-nonprod@globusmedical.com',Current_Date,'AR_REJECT_PO_EMAIL','303510',1000);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'AR_REJECT_PO_FROM','Reject PO email notification from','spineitcomms@spineit.net',Current_Date,'AR_REJECT_PO_EMAIL','303510',1000);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'AR_REJECT_PO_SUBJECT','Reject PO email notification subject','PO Request Rejected',Current_Date,'AR_REJECT_PO_EMAIL','303510',1000);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'AR_REJECT_PO_CONTYP','Reject PO email notification content type','text/html',Current_Date,'AR_REJECT_PO_EMAIL','303510',1000);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'AR_REJECT_PO_TITLE','Reject PO email notification title','PO Request Rejected',Current_Date,'AR_REJECT_PO_EMAIL','303510',1000);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'AR_REJECT_PO_BODY','Reject PO email notification body','<meta http-equiv="Content-Type" content="text/html"  charset="UTF-8" /><style>TD{ FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: verdana, arial, sans-serif;}.headercol{width : 120px;text-align: right;vertical-align: top;}.headerval{padding-left : 10px;vertical-align: top;text-align: left;}</style><font face=arial size="2">#<HEADER_LOGO><table style="border: 1px solid rgb(0, 54, 110); border-image: none; min-width: 850px; max-width: 850px;" cellspacing="0" cellpadding="0"><tbody><tr style="height:50px"><td colspan="4" style="padding: 10px 10px;"><b>The following PO Request(s) are Rejected </b></td></tr><tr style="height:30px"><td class="headercol"><b>Rejected By :</b></td><td class="headerval">#<REC_BY></td></tr><tr style="height:30px"><td class="headercol"><b>Rejected Date :</b></td><td class="headerval">#<REJ_DT></td></tr><tr style="height:30px"><td class="headercol"><b>PO# :</b></td><td class="headerval">#<PO_ID></td></tr><tr style="height:30px"><td class="headercol"><b>DO# :</b></td><td class="headerval">#<DO_ID></td></tr><tr style="height:30px"><td class="headercol"><b>Account# :</b></td><td class="headerval">#<ACC_ID></td></tr><tr style="height:30px"><td class="headercol"><b>Reason :</b></td><td class="headerval">#<PO_REASON></td></tr><tr style="height:60px"><td class="headercol"><b>Comments :</b></td><td class="headerval"><p>#<PO_COM></p></td></tr></tbody></table></font></html>',Current_Date,'AR_REJECT_PO_EMAIL','303510',1000);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By)
Values (S906_Rule.Nextval,'AR_REJECT_PO_HEADER_FL','Reject PO email notification header flag','Y',Current_Date,'AR_REJECT_PO_EMAIL','303510');
   