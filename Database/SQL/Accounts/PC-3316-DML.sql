--PC-3316 Cycle Count - Scanned Lots Report

-- Scanned lots view report Value added in Function list table 
INSERT 
INTO t1520_function_list (
    c1520_function_id,
    c1520_function_nm,
    c1520_function_desc,
    c1520_inherited_from_id,
    c901_type,
    c1520_void_fl,
    c1520_created_by,
    c1520_created_date,
    c1520_last_updated_by,
    c1520_last_updated_date,
    c1520_request_uri,
    c1520_request_stropt,
    c1520_seq_no,
    c901_department_id
) VALUES (
    'CYCLE_COUNT_LOT_RPT',
    'Scanned lots view report',
    'Cycle Count Scanned Lots Report screen.',
    NULL,
    92262,
    NULL,
    '706322',
    current_date,
    NULL,
    NULL,
    '/gmCycleCountLotRpt.do?method=loadScannedLots',
    NULL,
    NULL,
    2000);
    