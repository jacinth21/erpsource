

SET define OFF;
INSERT
INTO T906_RULES
  (
    C906_RULE_SEQ_ID,
    C906_RULE_ID,
    C906_RULE_DESC,
    C906_RULE_VALUE,
    C906_CREATED_DATE,
    C906_RULE_GRP_ID,
    C906_CREATED_BY
  )
  VALUES
  (
    S906_RULE.NEXTVAL,
    'SHIP_SUMMARY',
    'RULE FOR Python Url',
    'https://api.powerbi.com/beta/493165ce-0076-480f-9f43-e1ef480c02a2/datasets/17376911-6b57-46c4-b180-3daff38352f3/rows?q=shi&key=VN5fhyoNNWpfkGd9w7OCubOjB6M5tyx1zv4hDfJKi3aH7srWNVRY4t58pldWVYVAa9cPp%2BLt2S32dQ7nJbkamw%3D%3D',
    CURRENT_DATE,
    'PBI_SHIP_SUMMARY',
    '303510'
  );

