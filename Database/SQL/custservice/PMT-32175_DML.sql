--New Insert
INSERT
INTO t1520_function_list
  (
    C1520_FUNCTION_ID,
    C1520_FUNCTION_NM,
    C1520_FUNCTION_DESC,
    C1520_INHERITED_FROM_ID,
    C901_TYPE,
    C1520_VOID_FL,
    C1520_CREATED_BY,
    C1520_CREATED_DATE,
    c1520_last_updated_by,
    c1520_last_updated_date,
    c1520_request_uri,
    c1520_request_stropt,
    c1520_seq_no,
    c901_department_id,
    c901_left_menu_type
  )
  VALUES
  (
    'ADDRESS_ADD_EDIT',
    'ADDRESS_ADD_EDIT',
    'Users in this security event having an access to edit/create the address',
    NULL,
    92267,
    NULL,
    '303510',
    CURRENT_DATE,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    2006,
    NULL
  );
--New Insert
INSERT
INTO t1520_function_list
  (
    C1520_FUNCTION_ID,
    C1520_FUNCTION_NM,
    C1520_FUNCTION_DESC,
    C1520_INHERITED_FROM_ID,
    C901_TYPE,
    C1520_VOID_FL,
    C1520_CREATED_BY,
    C1520_CREATED_DATE,
    c1520_last_updated_by,
    c1520_last_updated_date,
    c1520_request_uri,
    c1520_request_stropt,
    c1520_seq_no,
    c901_department_id,
    c901_left_menu_type
  )
  VALUES
  (
    'CONTACT_ADD_EDIT',
    'CONTACT_ADD_EDIT',
    'Users in this security event having an access to edit/create the contact',
    NULL,
    92267,
    NULL,
    '303510',
    CURRENT_DATE,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    2006,
    NULL
  );

