--Loaner Status Details 
INSERT
INTO T906_RULES
  (
    C906_RULE_SEQ_ID,
    C906_RULE_ID,
    C906_RULE_DESC,
    C906_RULE_VALUE,
    C906_CREATED_DATE,
    C906_RULE_GRP_ID,
    C906_CREATED_BY,
    C906_LAST_UPDATED_DATE,
    C906_LAST_UPDATED_BY,
    C901_RULE_TYPE,
    C906_ACTIVE_FL,
    C906_VOID_FL,
    C1900_COMPANY_ID,
    C906_LAST_UPDATED_DATE_UTC
  )
  VALUES
  (
    S906_RULE.NEXTVAL,
    '21',
    'Disputed',
    'Disputed',
    current_date,
    'LOANS',
    '706310',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
  ) ;
  
--Common Cancel procedure call for 107603(update loaner as Disputed
INSERT
INTO T906_RULES
  (
    C906_RULE_SEQ_ID,
    C906_RULE_ID,
    C906_RULE_DESC,
    C906_RULE_VALUE,
    C906_CREATED_DATE,
    C906_RULE_GRP_ID,
    C906_CREATED_BY,
    C1900_COMPANY_ID
  )
  VALUES
  (
    S906_RULE.NEXTVAL,
    '107603',
    'Loaner as Disputed',
    'gm_pkg_set_dispute.gm_sav_dispute_loaner_set',
    CURRENT_DATE,
    'CMNCNCL',
    '706310',
    NULL
  );