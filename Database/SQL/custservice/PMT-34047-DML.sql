--Insert code lookup for showing status
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26240614,'Missing','TAGST','0','','','','2765071',CURRENT_DATE);

--Insert code lookup for showing tranfer type
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26240616,'Missing to Distributor','TSFTP','0','','','','2765071',CURRENT_DATE);

--Insert code lookup for showing tranfer type
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26240615,'Distributor to Missing','TSFTP','0','','','','2765071',CURRENT_DATE); 

--Insert code lookup for showing tranfer resson
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26240617,'Missing','TSFRN','0','','','','2765071',CURRENT_DATE);

DELETE FROM T906_RULES where C906_RULE_GRP_ID = 'TAGST' and C906_RULE_ID = 'DISTRIBUTOR';
INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY)
VALUES (S906_RULE.NEXTVAL,'DISTRIBUTOR','RULE TO DISPLAY DISTRIBUTOR AS MISSING','3102',CURRENT_DATE,'TAGST','2765071');

DELETE FROM T906_RULES where C906_RULE_GRP_ID = 'TAGINVTYPE' and C906_RULE_ID = '26240615';
-- New Rule grp to update inventory type when tag transfer
INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by)
VALUES (s906_rule.nextval,'26240615','Transfer-Distributor to Missing','10003',current_date,'TAGINVTYPE','2765071');

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(26240618,'Missing Distributor','GPLOG','0','','','','2765071',CURRENT_DATE);