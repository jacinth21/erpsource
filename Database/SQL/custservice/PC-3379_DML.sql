--PC-3379 - Shipping device printer name changes
 
--Update existing code grp -- ZEBPRN in code lookup table
  UPDATE T901_CODE_LOOKUP 
     SET C902_CODE_NM_ALT = C901_CODE_NM, 
     	 C901_LAST_UPDATED_BY = '706328',
     	 C901_LAST_UPDATED_DATE = CURRENT_DATE
   WHERE C901_CODE_GRP = 'ZEBPRN'
     AND C901_VOID_FL IS NULL;

 --Update existing code grp -- REGPRN in code lookup table
  UPDATE T901_CODE_LOOKUP 
     SET C902_CODE_NM_ALT = C901_CODE_NM,
     	 C901_LAST_UPDATED_BY = '706328',
     	 C901_LAST_UPDATED_DATE = CURRENT_DATE
   WHERE  C901_CODE_GRP = 'REGPRN'
     AND C901_VOID_FL IS NULL;
     
 
