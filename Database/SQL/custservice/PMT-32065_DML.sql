
-- create synonym for new package
create or replace public synonym gm_pkg_cs_consignment_transfer for GLOBUS_APP.gm_pkg_cs_consignment_transfer;

--�	To create the new left link � T1520 � Function id (�ACCOUNT_CN_TRANSFER�)
INSERT
   INTO t1520_function_list
    (
        C1520_FUNCTION_ID, C1520_FUNCTION_NM, C1520_FUNCTION_DESC
      , C1520_INHERITED_FROM_ID, C901_TYPE, C1520_VOID_FL
      , C1520_CREATED_BY, C1520_CREATED_DATE, C1520_LAST_UPDATED_BY
      , C1520_LAST_UPDATED_DATE, C1520_REQUEST_URI, C1520_REQUEST_STROPT
      , C1520_SEQ_NO, C901_DEPARTMENT_ID
    )
    VALUES
    (
        'ACCOUNT_CN_TRANSFER', 'Account Item Transfer', 'Account Item Consignment Transfer'
      , NULL, 92262, NULL
      , '706322', SYSDATE, NULL
      , NULL, '/gmConsignmentTransfer.do?method=loadAccountConsignmentScreen', null
      , NULL, 2000
    ) ;
