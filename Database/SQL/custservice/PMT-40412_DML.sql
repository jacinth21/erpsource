/*synonyms for tables*/
CREATE OR REPLACE  public synonym MY_TEMP_GROUP_SYNC_UPDATE FOR GLOBUS_APP.MY_TEMP_GROUP_SYNC_UPDATE;

CREATE OR REPLACE  public synonym T4010S_GROUP_MASTER_SYNC FOR GLOBUS_APP.T4010S_GROUP_MASTER_SYNC;

CREATE OR REPLACE  public synonym T4011S_GROUP_DETAILS_SYNC FOR GLOBUS_APP.T4011S_GROUP_DETAILS_SYNC;

/*synonyms for procedures*/
CREATE OR REPLACE  public synonym gm_pkg_device_group_sync_temp FOR GLOBUS_APP.gm_pkg_device_group_sync_temp;

CREATE OR REPLACE  public synonym gm_pkg_device_group_sync_json_master FOR GLOBUS_APP.gm_pkg_device_group_sync_json_master;

/*synonyms for triggers*/
CREATE OR REPLACE  public synonym trg_4010_group_ipad_update FOR GLOBUS_APP.trg_4010_group_ipad_update;

CREATE OR REPLACE  public synonym trg_4011_group_detail_ipad_update FOR GLOBUS_APP.trg_4011_group_detail_ipad_update;

CREATE OR REPLACE  public synonym trg_4012_group_company_mapping_ipad_update FOR GLOBUS_APP.trg_4012_group_company_mapping_ipad_update;




