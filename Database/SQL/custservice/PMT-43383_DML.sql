
DELETE FROM t906_rules WHERE c906_rule_id = 'DO_APP_REQ_TYPE' AND c906_rule_grp_id ='DO_ORDER';
--iPad Loaner in Type field disable in Parts Used tab
INSERT INTO T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By, C906_Void_Fl,C1900_Company_Id)
VALUES (S906_Rule.Nextval,'DO_APP_REQ_TYPE','iPad Loaner in Type field disable in Parts Used tab','Y',CURRENT_DATE,'DO_ORDER','303510',Null,1009);
--iPad Loaner in Type field disable in Parts Used tab
INSERT INTO T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By, C906_Void_Fl,C1900_Company_Id)
VALUES (S906_Rule.Nextval,'DO_APP_REQ_TYPE','iPad Loaner in Type field disable in Parts Used tab','Y',CURRENT_DATE,'DO_ORDER','303510',Null,1011);