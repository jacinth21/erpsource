--to create synonym for new procedures

CREATE OR REPLACE  public synonym gm_pkg_device_file_sync_json_master FOR GLOBUS_APP.gm_pkg_device_file_sync_json_master;

CREATE OR REPLACE  public synonym gm_pkg_device_file_sync_temp FOR GLOBUS_APP.gm_pkg_device_file_sync_temp;

CREATE OR REPLACE public synonym trg_903_upload_file_list_ipad_update for GLOBUS_APP.trg_903_upload_file_list_ipad_update;

--CREATE OR REPLACE public synonym MY_TEMP_FILE_SYNC_UPDATE for GLOBUS_APP.MY_TEMP_FILE_SYNC_UPDATE;

CREATE OR REPLACE public synonym T903S_FILE_MASTER_SYNC for GLOBUS_APP.T903S_FILE_MASTER_SYNC;