-- Update rule_value to hide NPI tab for japanese users in DO module
UPDATE t906_rules
SET c906_rule_value      = 'btn-catalog,btn-collateral,btn-case,btn-crm,btn-pricing,btn-case-udi,btn-guide,div-do-NPI-info',
  c906_last_updated_by   = '2423914',
  c906_last_updated_date = sysdate
WHERE c906_rule_id       = '1026'
AND c906_rule_grp_id     ='MODULEACCESS';
