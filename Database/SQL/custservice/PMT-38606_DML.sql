--Synonym for newly created procedure to update the price for all orders 
CREATE OR REPLACE PUBLIC SYNONYM gm_update_order_price_for_all_orders FOR GLOBUS_APP.gm_update_order_price_for_all_orders;

-- Insert script to exclude orders that should not be allowed to change the price (t906_rules table)

DELETE FROM T906_RULES WHERE C906_RULE_GRP_ID = 'EXCHANGEPRICINGORD' AND C906_RULE_ID = 'CHANGEPRICEORD' ;
DELETE FROM T906_RULES WHERE C906_RULE_GRP_ID = 'DISBLCHPRICINGORD';

--2527  Replacement
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'CHANGEPRICEORD','Exclude the order type','2527',CURRENT_DATE,'EXCHANGEPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);

--2528 Credit Memo
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'CHANGEPRICEORD','Exclude the order type','2528',CURRENT_DATE,'EXCHANGEPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);
 
--2529  Sales Adjustment
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'CHANGEPRICEORD','Exclude the order type','2529',CURRENT_DATE,'EXCHANGEPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);
  
--2524 Price Adjustment
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'CHANGEPRICEORD','Exclude the order type','2524',CURRENT_DATE,'EXCHANGEPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);

--2523  Return Order
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'CHANGEPRICEORD','Exclude the order type','2523',CURRENT_DATE,'EXCHANGEPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);
 
--2534  ICT returns order
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'CHANGEPRICEORD','Exclude the order type','2534',CURRENT_DATE,'EXCHANGEPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);
 
--2533   ICS
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'CHANGEPRICEORD','Exclude the order type','2533',CURRENT_DATE,'EXCHANGEPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);

 --2518  Draft
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'CHANGEPRICEORD','Exclude the order type','2518',CURRENT_DATE,'EXCHANGEPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);
 
--2519   Pending CS Confirmation
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'CHANGEPRICEORD','Exclude the order type','2519',CURRENT_DATE,'EXCHANGEPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);

--102080  OUS Distributor
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'CHANGEPRICEORD','Exclude the order type','102080',CURRENT_DATE,'EXCHANGEPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);
  
--102364  Stock Transfer
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'CHANGEPRICEORD','Exclude the order type','102364',CURRENT_DATE,'EXCHANGEPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);

--2520    Quote
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'CHANGEPRICEORD','Exclude the order type','2520',CURRENT_DATE,'EXCHANGEPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);
 
--26240232    Direct
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'CHANGEPRICEORD','Exclude the order type','26240232',CURRENT_DATE,'EXCHANGEPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);

--26240233    Intercompany
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'CHANGEPRICEORD','Exclude the order type','26240233',CURRENT_DATE,'EXCHANGEPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);

--26240236  Bill & Hold
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'CHANGEPRICEORD','Exclude the order type','26240236',CURRENT_DATE,'EXCHANGEPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);

--106348   Generate-Intercompany
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'CHANGEPRICEORD','Exclude the order type','106348',CURRENT_DATE,'EXCHANGEPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);

-- 106347 Generate � Direct
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'CHANGEPRICEORD','Exclude the order type','106347',CURRENT_DATE,'EXCHANGEPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);

--2522  Duplicate
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'CHANGEPRICEORD','Exclude the order type','2522',CURRENT_DATE,'EXCHANGEPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);
  

-- Insert script to Disable Net Unit Price field and Adj code field in Change pricing screen for the below orders

--2527  Replacement
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'2527','Disable the order type','Y',CURRENT_DATE,'DISBLCHPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);

--2528 Credit Memo
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'2528','Disable the order type','Y',CURRENT_DATE,'DISBLCHPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);
 
--2529  Sales Adjustment
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'2529','Disable the order type','Y',CURRENT_DATE,'DISBLCHPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);
  
--2524 Price Adjustment
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'2524','Disable the order type','Y',CURRENT_DATE,'DISBLCHPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);

--2523  Return Order
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'2523','Disable the order type','Y',CURRENT_DATE,'DISBLCHPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);
 
--2534  ICT returns order
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'2534','Disable the order type','Y',CURRENT_DATE,'DISBLCHPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);
 
--2533   ICS
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'2533','Disable the order type','Y',CURRENT_DATE,'DISBLCHPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);

 --2518  Draft
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'2518','Disable the order type','Y',CURRENT_DATE,'DISBLCHPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);
 
--2519   Pending CS Confirmation
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'2519','Disable the order type','Y',CURRENT_DATE,'DISBLCHPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);

--102080  OUS Distributor
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'102080','Disable the order type','Y',CURRENT_DATE,'DISBLCHPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);
  
--102364  Stock Transfer
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'102364','Disable the order type','Y',CURRENT_DATE,'DISBLCHPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);

--2520    Quote
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'2520','Disable the order type','Y',CURRENT_DATE,'DISBLCHPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);
 
--26240232    Direct
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'26240232','Disable the order type','Y',CURRENT_DATE,'DISBLCHPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);

--26240233    Intercompany
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'26240233','Disable the order type','Y',CURRENT_DATE,'DISBLCHPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);

--26240236  Bill & Hold
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'26240236','Disable the order type','Y',CURRENT_DATE,'DISBLCHPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);

--106348   Generate-Intercompany
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'106348','Disable the order type','Y',CURRENT_DATE,'DISBLCHPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);


INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'106347','Disable the order type','Y',CURRENT_DATE,'DISBLCHPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);

--2522  Duplicate
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_LAST_UPDATED_DATE,C906_LAST_UPDATED_BY,C901_RULE_TYPE,C906_ACTIVE_FL,C906_VOID_FL,C1900_COMPANY_ID)
VALUES (S906_RULE.NEXTVAL,'2522','Disable the order type','Y',CURRENT_DATE,'DISBLCHPRICINGORD','303510',NULL,NULL,NULL,NULL,NULL,NULL);
  












   
