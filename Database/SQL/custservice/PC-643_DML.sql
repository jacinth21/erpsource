----PC-643 To call different jasper


INSERT INTO t906_rules
( c906_rule_seq_id, c906_rule_id, c906_rule_value, c906_rule_grp_id, c906_created_date, c906_created_by) 
VALUES 
(s906_rule.NEXTVAL, '10304532', 'LISTPRICE', 'INVRULEID', CURRENT_DATE, '2423914');

INSERT INTO t906_rules
( c906_rule_seq_id, c906_rule_id, c906_rule_desc, c906_rule_value, c906_created_date, c906_rule_grp_id, c906_created_by, c1900_company_id) 
VALUES 
(s906_rule.NEXTVAL, 'LISTPRICE_REGULAR', 'Invoice for PDF Jasper -List Price', '/pdf/GmUSInvPDFPrintWithListPriceLayout.jasper', CURRENT_DATE ,'INVOICE_PDF_JASPER','2423914', '1000');

INSERT INTO t906_rules
( c906_rule_seq_id, c906_rule_id, c906_rule_value, c906_created_date, c906_rule_grp_id, c906_created_by) 
VALUES 
(s906_rule.NEXTVAL, '1000_2000_LISTPRICE_REGULAR', '/GmUSInvPrintWithListPriceLayout.jasper', CURRENT_DATE ,'INVOICE_JASPER','2423914');

INSERT INTO t906_rules
( c906_rule_seq_id, c906_rule_id, c906_rule_value, c906_created_date, c906_rule_grp_id, c906_created_by) 
VALUES 
(s906_rule.NEXTVAL, '1000_2001_LISTPRICE_REGULAR', '/GmUSInvPrintWithListPriceLayout.jasper', CURRENT_DATE ,'INVOICE_JASPER','2423914');

INSERT INTO t906_rules
( c906_rule_seq_id, c906_rule_id, c906_rule_value, c906_created_date, c906_rule_grp_id, c906_created_by) 
VALUES 
(s906_rule.NEXTVAL, '1000_2004_LISTPRICE_REGULAR', '/GmUSInvPrintWithListPriceLayout.jasper', CURRENT_DATE ,'INVOICE_JASPER','2423914');

INSERT INTO t906_rules
( c906_rule_seq_id, c906_rule_id, c906_rule_value, c906_created_date, c906_rule_grp_id, c906_created_by) 
VALUES 
(s906_rule.NEXTVAL, '1000_2005_LISTPRICE_REGULAR', '/GmUSInvPrintWithListPriceLayout.jasper', CURRENT_DATE ,'INVOICE_JASPER','2423914');






