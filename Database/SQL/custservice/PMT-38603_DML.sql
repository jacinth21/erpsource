/*2521 BILL&SHIP order type*/
INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY) 
values (S906_RULE.NEXTVAL,'STATUS_LOG',NULL,'Y',SYSDATE,'2521','706328');

/*2531 Loan-Consign Swap order type*/
INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY) 
values (S906_RULE.NEXTVAL,'STATUS_LOG',NULL,'Y',SYSDATE,'2531','706328');

/*2525 Backorder type*/
INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY) 
values (S906_RULE.NEXTVAL,'STATUS_LOG',NULL,'Y',SYSDATE,'2525','706328');

/*2522 Duplicate order type*/
INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY) 
values (S906_RULE.NEXTVAL,'STATUS_LOG',NULL,'Y',SYSDATE,'2522','706328');

/*102080 OUS Distributor order type*/
INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY) 
values (S906_RULE.NEXTVAL,'STATUS_LOG',NULL,'Y',SYSDATE,'102080','706328');

/*26240232 Direct order type*/
INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY) 
values (S906_RULE.NEXTVAL,'STATUS_LOG',NULL,'Y',SYSDATE,'26240232','706328');