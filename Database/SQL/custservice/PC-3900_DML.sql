INSERT INTO t1520_function_list (C1520_FUNCTION_ID,C1520_FUNCTION_NM,C1520_FUNCTION_DESC,C901_TYPE,C1520_CREATED_BY,C1520_CREATED_DATE,C1520_REQUEST_URI,C1520_REQUEST_STROPT,C901_DEPARTMENT_ID,C901_LEFT_MENU_TYPE) 
VALUES ('MASTER_BARCODE','Master Barcode','This screen is used to for generate master barcode for order/ consignment',92262,'2765071',current_date,'/gmMasterBarcode.do?method=loadMasterBarcode','',2009,101921);

CREATE OR REPLACE PUBLIC SYNONYM gm_pkg_cs_master_barcode_rpt FOR GLOBUS_APP.gm_pkg_cs_master_barcode_rpt;