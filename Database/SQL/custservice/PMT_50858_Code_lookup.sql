-- For Reason drop in disputed screen(Common Cancel)
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(109280,'FedEx Issue','LNDSPT','1','1','','','2598779',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(109281,'Rep says Set not received; possible re-route/transfer issue','LNDSPT','1','2','','','2598779',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(109282,'Theft � pending police report','LNDSPT','1','3','','','2598779',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(109283,'Hospital has Lost/Misplaced set','LNDSPT','1','4','','','2598779',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(109284,'Shipping Issue to rep','LNDSPT','1','5','','','2598779',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(109285,'Rep state set returned; but still in ''Pending Return''','LNDSPT','1','6','','','2598779',CURRENT_DATE);



--For Common Cancel
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(107603,'Loaner as Disputed','CNCLT','1','164','LNDSPT','','2598779',CURRENT_DATE);



--For Loaner Option dropdown
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(26240839,'Updated as Disputed','LOANOP','1','19','DISPUTE','','2598779',CURRENT_DATE);


--For Tag Status
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(26240842,'Disputed','TAGST','1','','','','2598779',CURRENT_DATE);


--For Loaner Status
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES
(26240843,'Disputed','LOANS','1','21','21','','2598779',CURRENT_DATE);

