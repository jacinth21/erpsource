--Insert scripts for t5057_building table
DELETE FROM t5057_building WHERE C5057_BUILDING_ID IN (5001, 5002);

INSERT INTO t5057_building (C5057_BUILDING_ID,C1900_COMPANY_ID,C5040_PLANT_ID,C5057_BUILDING_NAME,C5057_BUILDING_SHORT_NAME,C5057_ADDRESS_1,C5057_ADDRESS_2,C5057_CITY,C901_COUNTRY_ID,C901_STATE_ID,C5057_ZIPCODE,C5057_ACTIVE_FL,C5057_VOID_FL,C5057_LAST_UPDATED_BY,C5057_LAST_UPDATED_DATE,C5057_PRIMARY_FL) 
VALUES (5001,1000,3000,'GA Avenue','2550','Valley Forge Business Center,','2550 General Armistead Avenue','Audubon',1101,1001,'19403','Y',null,null,null,'Y');
     
INSERT INTO t5057_building (C5057_BUILDING_ID,C1900_COMPANY_ID,C5040_PLANT_ID,C5057_BUILDING_NAME,C5057_BUILDING_SHORT_NAME,C5057_ADDRESS_1,C5057_ADDRESS_2,C5057_CITY,C901_COUNTRY_ID,C901_STATE_ID,C5057_ZIPCODE,C5057_ACTIVE_FL,C5057_VOID_FL,C5057_LAST_UPDATED_BY,C5057_LAST_UPDATED_DATE,C5057_PRIMARY_FL) 
VALUES (5002,1000,3000,'Boulevard Generals','2435',null,'Boulevard of the Generals','Audubon',1101,1001,'19403',null,null,null,null,null);

