INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26240644,'Add/Edit the Recorded tags','MODACT','0','','','','2598779',CURRENT_DATE);


CREATE OR REPLACE  public synonym gm_pkg_order_tag_record FOR GLOBUS_APP.gm_pkg_order_tag_record;

-- SYNONYM for New functions
CREATE OR REPLACE PUBLIC SYNONYM IS_NUMBER FOR GLOBUS_APP.IS_NUMBER;


CREATE SEQUENCE s501d_temp_orderid
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;