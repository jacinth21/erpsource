--Transfer MOde    
INSERT INTO t901_code_lookup (
  c901_code_id, c901_code_nm, c901_code_grp, 
  c901_active_fl, c901_code_seq_no, 
  c902_code_nm_alt, c901_control_type, 
  c901_created_by, c901_created_date
) 
VALUES 
  (
    26240576, 'Account Item Transfer', 
    'TSMOD', '1', '', '', '', '706322', 
    CURRENT_DATE
  );
  --Type
INSERT INTO t901_code_lookup (
  c901_code_id, c901_code_nm, c901_code_grp, 
  c901_active_fl, c901_code_seq_no, 
  c902_code_nm_alt, c901_control_type, 
  c901_created_by, c901_created_date
) 
VALUES 
  (
    26240577, 'Account Consignment to Account', 
    'TSFTP', '1', '', '', '', '706322', 
    CURRENT_DATE
  );
  
  --Reason
INSERT INTO t901_code_lookup (
  c901_code_id, c901_code_nm, c901_code_grp, 
  c901_active_fl, c901_code_seq_no, 
  c902_code_nm_alt, c901_control_type, 
  c901_created_by, c901_created_date
) 
VALUES 
  (
    108100, 'Lack of Inventory', 'ACCTSF', 
    '1', '1', '', '', '706322', CURRENT_DATE
  );
INSERT INTO t901_code_lookup (
  c901_code_id, c901_code_nm, c901_code_grp, 
  c901_active_fl, c901_code_seq_no, 
  c902_code_nm_alt, c901_control_type, 
  c901_created_by, c901_created_date
) 
VALUES 
  (
    108101, 'Urgent Surgery', 'ACCTSF', 
    '1', '2', '', '', '706322', CURRENT_DATE
  );
INSERT INTO t901_code_lookup (
  c901_code_id, c901_code_nm, c901_code_grp, 
  c901_active_fl, c901_code_seq_no, 
  c902_code_nm_alt, c901_control_type, 
  c901_created_by, c901_created_date
) 
VALUES 
  (
    108102, 'Customer Request', 'ACCTSF', 
    '1', '3', '', '', '706322', CURRENT_DATE
  );
INSERT INTO t901_code_lookup (
  c901_code_id, c901_code_nm, c901_code_grp, 
  c901_active_fl, c901_code_seq_no, 
  c902_code_nm_alt, c901_control_type, 
  c901_created_by, c901_created_date
) 
VALUES 
  (
    108103, 'Others', 'ACCTSF', '1', '4', 
    '', '', '706322', CURRENT_DATE
  );
  