 INSERT
INTO t906_rules
  (
    c906_rule_seq_id,
    c906_rule_id,
    c906_rule_desc,
    c906_rule_value,
    c906_created_date,
    c906_rule_grp_id,
    c906_created_by,
    c1900_company_id
  )
  VALUES
  (
    S906_Rule.Nextval,
    'NPIAPI',
    'NPI not found in API',
    'smariyappan@globusmedical.com',
    CURRENT_DATE,
    'EMAIL',
    '303510',
    ''
  );