-- PMT-33781 - Security event for Modify Shipping Details 
-- To create the Security event to Submit Button  and Disable Button
INSERT
INTO T1520_FUNCTION_LIST
    (C1520_FUNCTION_ID, 
     C1520_FUNCTION_NM, 
     C1520_FUNCTION_DESC, 
     C1520_INHERITED_FROM_ID, 
     C901_TYPE, 
     C1520_VOID_FL, 
     C1520_CREATED_BY, 
     C1520_CREATED_DATE, 
     C1520_LAST_UPDATED_BY, 
     C1520_LAST_UPDATED_DATE, 
     C1520_REQUEST_URI, 
     C1520_REQUEST_STROPT, 
     C1520_SEQ_NO, 
     C901_DEPARTMENT_ID, 
     C901_LEFT_MENU_TYPE, 
     C1520_LAST_UPDATED_DATE_UTC )
VALUES
    ('MODIFY_SHIP_EDIT_ACC',
     'MODIFY_SHIP_EDIT_ACC',
     'Users in this security event will have the access to modify the shipping details',
      null, 
      92267,
      null,
      '303510',
      CURRENT_DATE,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      CURRENT_DATE) ;
      
  -- To create the Security event to Disable button 
 INSERT 
   INTO T1520_FUNCTION_LIST
        (C1520_FUNCTION_ID,
         C1520_FUNCTION_NM,
         C1520_FUNCTION_DESC, 
         C1520_INHERITED_FROM_ID,
         C901_TYPE,
         C1520_VOID_FL,
         C1520_CREATED_BY,
         C1520_CREATED_DATE,
         C1520_LAST_UPDATED_BY,
         C1520_LAST_UPDATED_DATE,
         C1520_REQUEST_URI,
         C1520_REQUEST_STROPT ,
         C1520_SEQ_NO,
         C901_DEPARTMENT_ID,
         C901_LEFT_MENU_TYPE,
         C1520_LAST_UPDATED_DATE_UTC )
 VALUES
     ('MODIFY_SHIP_DIS_ACC',
      'MODIFY_SHIP_DIS_ACC',
      'Users in this security event will have the access to modify the shipping details',
       null, 
       92267,
       null,
       '303510',
       CURRENT_DATE,
       null,
       null,
       null,
       null,
       null,
       null,
       null,
       CURRENT_DATE) ;

