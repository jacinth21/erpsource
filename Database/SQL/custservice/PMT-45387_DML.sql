--to create synonym for new procedures
-- PMT-45387 - Provision to record DDT and Usage Lot when processing Credit or Debit Notes

CREATE OR REPLACE public synonym gm_pkg_cs_ord_usage_lot_rpt for GLOBUS_APP.gm_pkg_cs_ord_usage_lot_rpt;