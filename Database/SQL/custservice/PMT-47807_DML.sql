--Insert into Rule table for displaying the transaction based on the priority 
INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_CREATED_DATE,C906_ACTIVE_FL) 
VALUES (S906_RULE.NEXTVAL,'5038','Transaction based on the priority','1','INVTXNPRIORITY','303510',CURRENT_DATE,NULL);

INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_CREATED_DATE,C906_ACTIVE_FL) 
VALUES (S906_RULE.NEXTVAL,'5003','Transaction based on the priority','2','INVTXNPRIORITY','303510',CURRENT_DATE,NULL);

INSERT INTO T906_RULES 
(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_RULE_GRP_ID,C906_CREATED_BY,C906_CREATED_DATE,C906_ACTIVE_FL) 
VALUES (S906_RULE.NEXTVAL,'4000612','Transaction based on the priority','3','INVTXNPRIORITY','303510',CURRENT_DATE,NULL);
