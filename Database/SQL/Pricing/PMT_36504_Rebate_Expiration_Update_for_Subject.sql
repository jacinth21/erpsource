-- Update Subject info

update T906_Rules set c906_rule_value='Globus Medical North America - Rebate expiry in #<RANGE> days', c906_last_updated_date=current_date, c906_last_updated_by='303510' where c906_rule_id='REBATE_EMAIL_SUBJECT' and c1900_company_id=1000;

update T906_Rules set c906_rule_value='Globus Medical Australia - Rebate expiry in #<RANGE> days', c906_last_updated_date=current_date, c906_last_updated_by='303510' where c906_rule_id='REBATE_EMAIL_SUBJECT' and c1900_company_id=1003;

update T906_Rules set c906_rule_value='Globus Medical Sweden - Rebate expiry in #<RANGE> days', c906_last_updated_date=current_date, c906_last_updated_by='303510' where c906_rule_id='REBATE_EMAIL_SUBJECT' and c1900_company_id=1015;

update T906_Rules set c906_rule_value='Globus Medical Belgium - Rebate expiry in #<RANGE> days', c906_last_updated_date=current_date, c906_last_updated_by='303510' where c906_rule_id='REBATE_EMAIL_SUBJECT' and c1900_company_id=1004;

update T906_Rules set c906_rule_value='Globus Medical Denmark - Rebate expiry in #<RANGE> days', c906_last_updated_date=current_date, c906_last_updated_by='303510' where c906_rule_id='REBATE_EMAIL_SUBJECT' and c1900_company_id=1006;

update T906_Rules set c906_rule_value='Globus Medical Germany - Rebate expiry in #<RANGE> days', c906_last_updated_date=current_date, c906_last_updated_by='303510' where c906_rule_id='REBATE_EMAIL_SUBJECT' and c1900_company_id=1008;

update T906_Rules set c906_rule_value='Globus Medical Ireland - Rebate expiry in #<RANGE> days', c906_last_updated_date=current_date, c906_last_updated_by='303510' where c906_rule_id='REBATE_EMAIL_SUBJECT' and c1900_company_id=1023;