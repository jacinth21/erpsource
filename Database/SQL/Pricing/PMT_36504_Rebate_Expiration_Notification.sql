--Insert rule for rebate expiry range by company for companyid 1000
Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id) 
Values (S906_Rule.Nextval,'REBATE_EXP_EMAIL_RANGE','Rebate expiry email notification range','30',Current_Date,'REBATE_EXP_EMAIL_RG','303510',1000);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id) 
Values (S906_Rule.Nextval,'REBATE_EXP_EMAIL_RANGE','Rebate expiry email notification range','15',Current_Date,'REBATE_EXP_EMAIL_ RG','303510',1000);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EXP_EMAIL_RANGE','Rebate expiry email notification range','6',Current_Date,'REBATE_EXP_EMAIL_RG','303510',1000);


--Insert rule for rebate expiry range by company for companyid 1003
Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EXP_EMAIL_RANGE','Rebate expiry email notification range','30',Current_Date,'REBATE_EXP_EMAIL_RG','303510',1003);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EXP_EMAIL_RANGE','Rebate expiry email notification range','15',Current_Date,'REBATE_EXP_EMAIL_ RG','303510',1003);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EXP_EMAIL_RANGE','Rebate expiry email notification range','6',Current_Date,'REBATE_EXP_EMAIL_RG','303510',1003);


--Insert rule for rebate expiry range by company for companyid 1015
Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EXP_EMAIL_RANGE','Rebate expiry email notification range','30',Current_Date,'REBATE_EXP_EMAIL_RG','303510',1015);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EXP_EMAIL_RANGE','Rebate expiry email notification range','15',Current_Date,'REBATE_EXP_EMAIL_ RG','303510',1015);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EXP_EMAIL_RANGE','Rebate expiry email notification range','6',Current_Date,'REBATE_EXP_EMAIL_RG','303510',1015);


--Insert rule for rebate expiry range by company for companyid 1004
Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EXP_EMAIL_RANGE','Rebate expiry email notification range','30',Current_Date,'REBATE_EXP_EMAIL_RG','303510',1004);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EXP_EMAIL_RANGE','Rebate expiry email notification range','15',Current_Date,'REBATE_EXP_EMAIL_ RG','303510',1004);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EXP_EMAIL_RANGE','Rebate expiry email notification range','6',Current_Date,'REBATE_EXP_EMAIL_RG','303510',1004);


--Insert rule for rebate expiry range by company for companyid 1006
Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EXP_EMAIL_RANGE','Rebate expiry email notification range','30',Current_Date,'REBATE_EXP_EMAIL_RG','303510',1006);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EXP_EMAIL_RANGE','Rebate expiry email notification range','15',Current_Date,'REBATE_EXP_EMAIL_ RG','303510',1006);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EXP_EMAIL_RANGE','Rebate expiry email notification range','6',Current_Date,'REBATE_EXP_EMAIL_RG','303510',1006);


--Insert rule for rebate expiry range by company for companyid 1008
Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EXP_EMAIL_RANGE','Rebate expiry email notification range','30',Current_Date,'REBATE_EXP_EMAIL_RG','303510',1008);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EXP_EMAIL_RANGE','Rebate expiry email notification range','15',Current_Date,'REBATE_EXP_EMAIL_ RG','303510',1008);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EXP_EMAIL_RANGE','Rebate expiry email notification range','6',Current_Date,'REBATE_EXP_EMAIL_RG','303510',1008);


--Insert rule for rebate expiry range by company for companyid 1023
Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EXP_EMAIL_RANGE','Rebate expiry email notification range','30',Current_Date,'REBATE_EXP_EMAIL_RG','303510',1023);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EXP_EMAIL_RANGE','Rebate expiry email notification range','15',Current_Date,'REBATE_EXP_EMAIL_ RG','303510',1023);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EXP_EMAIL_RANGE','Rebate expiry email notification range','6',Current_Date,'REBATE_EXP_EMAIL_RG','303510',1023);



--Insert rule for rebate expiry email notification details for companyid 1000
Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_TO','Rebate expiry email notification to','mmuthusamy@globusmedical.com',Current_Date,'REBATE_EXP_EMAIL','303510',1000);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_CC','Rebate expiry email notification cc','RSelvaraj@globusmedical.com',Current_Date,'REBATE_EXP_EMAIL','303510',1000);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_FROM','Rebate expiry email notification from','mmuthusamy@globusmedical.com',Current_Date,'REBATE_EXP_EMAIL','303510',1000);


Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_SUBJECT','Rebate expiry email notification subject','Rebate expiry in #<RANGE> days',Current_Date,'REBATE_EXP_EMAIL','303510',1000);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_CONTYP','Rebate expiry email notification content type','text/html',Current_Date,'REBATE_EXP_EMAIL','303510',1000);


--Insert rule for rebate expiry email notification details for companyid 1003
Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_TO','Rebate expiry email notification to','mmuthusamy@globusmedical.com',Current_Date,'REBATE_EXP_EMAIL','303510',1003);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_CC','Rebate expiry email notification cc','RSelvaraj@globusmedical.com',Current_Date,'REBATE_EXP_EMAIL','303510',1003);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_FROM','Rebate expiry email notification from','mmuthusamy@globusmedical.com',Current_Date,'REBATE_EXP_EMAIL','303510',1003);


Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_SUBJECT','Rebate expiry email notification subject','Rebate expiry in #<RANGE> days',Current_Date,'REBATE_EXP_EMAIL','303510',1003);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_CONTYP','Rebate expiry email notification content type','text/html',Current_Date,'REBATE_EXP_EMAIL','303510',1003);


--Insert rule for rebate expiry email notification details for companyid 1015
Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_TO','Rebate expiry email notification to','mmuthusamy@globusmedical.com',Current_Date,'REBATE_EXP_EMAIL','303510',1015);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_CC','Rebate expiry email notification cc','RSelvaraj@globusmedical.com',Current_Date,'REBATE_EXP_EMAIL','303510',1015);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_FROM','Rebate expiry email notification from','mmuthusamy@globusmedical.com',Current_Date,'REBATE_EXP_EMAIL','303510',1015);


Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_SUBJECT','Rebate expiry email notification subject','Rebate expiry in #<RANGE> days',Current_Date,'REBATE_EXP_EMAIL','303510',1015);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_CONTYP','Rebate expiry email notification content type','text/html',Current_Date,'REBATE_EXP_EMAIL','303510',1015);


--Insert rule for rebate expiry email notification details for companyid 1004
Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_TO','Rebate expiry email notification to','mmuthusamy@globusmedical.com',Current_Date,'REBATE_EXP_EMAIL','303510',1004);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_CC','Rebate expiry email notification cc','RSelvaraj@globusmedical.com',Current_Date,'REBATE_EXP_EMAIL','303510',1004);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_FROM','Rebate expiry email notification from','mmuthusamy@globusmedical.com',Current_Date,'REBATE_EXP_EMAIL','303510',1004);


Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_SUBJECT','Rebate expiry email notification subject','Rebate expiry in #<RANGE> days',Current_Date,'REBATE_EXP_EMAIL','303510',1004);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_CONTYP','Rebate expiry email notification content type','text/html',Current_Date,'REBATE_EXP_EMAIL','303510',1004);


--Insert rule for rebate expiry email notification details for companyid 1006
Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_TO','Rebate expiry email notification to','mmuthusamy@globusmedical.com',Current_Date,'REBATE_EXP_EMAIL','303510',1006);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_CC','Rebate expiry email notification cc','RSelvaraj@globusmedical.com',Current_Date,'REBATE_EXP_EMAIL','303510',1006);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_FROM','Rebate expiry email notification from','mmuthusamy@globusmedical.com',Current_Date,'REBATE_EXP_EMAIL','303510',1006);


Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_SUBJECT','Rebate expiry email notification subject','Rebate expiry in #<RANGE> days',Current_Date,'REBATE_EXP_EMAIL','303510',1006);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_CONTYP','Rebate expiry email notification content type','text/html',Current_Date,'REBATE_EXP_EMAIL','303510',1006);


--Insert rule for rebate expiry email notification details for companyid 1008
Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_TO','Rebate expiry email notification to','mmuthusamy@globusmedical.com',Current_Date,'REBATE_EXP_EMAIL','303510',1008);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_CC','Rebate expiry email notification cc','RSelvaraj@globusmedical.com',Current_Date,'REBATE_EXP_EMAIL','303510',1008);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_FROM','Rebate expiry email notification from','mmuthusamy@globusmedical.com',Current_Date,'REBATE_EXP_EMAIL','303510',1008);


Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_SUBJECT','Rebate expiry email notification subject','Rebate expiry in #<RANGE> days',Current_Date,'REBATE_EXP_EMAIL','303510',1008);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_CONTYP','Rebate expiry email notification content type','text/html',Current_Date,'REBATE_EXP_EMAIL','303510',1008);


--Insert rule for rebate expiry email notification details for companyid 1023
Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_TO','Rebate expiry email notification to','mmuthusamy@globusmedical.com',Current_Date,'REBATE_EXP_EMAIL','303510',1023);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_CC','Rebate expiry email notification cc','RSelvaraj@globusmedical.com',Current_Date,'REBATE_EXP_EMAIL','303510',1023);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_FROM','Rebate expiry email notification from','mmuthusamy@globusmedical.com',Current_Date,'REBATE_EXP_EMAIL','303510',1023);


Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_SUBJECT','Rebate expiry email notification subject','Rebate expiry in #<RANGE> days',Current_Date,'REBATE_EXP_EMAIL','303510',1023);

Insert Into T906_Rules (C906_Rule_Seq_Id,C906_Rule_Id,C906_Rule_Desc,C906_Rule_Value,C906_Created_Date,C906_Rule_Grp_Id,C906_Created_By,c1900_company_id)
Values (S906_Rule.Nextval,'REBATE_EMAIL_CONTYP','Rebate expiry email notification content type','text/html',Current_Date,'REBATE_EXP_EMAIL','303510',1023);
