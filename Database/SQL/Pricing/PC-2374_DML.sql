--7010:Group Pricing Aff, 7011:Healthcare Aff, 7008:Account Group, 26241117:RPC Setup, 26241118:Health System Setup

INSERT INTO t901_code_lookup                                                  
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26241118,'Health System Name','PTTYP','1','','','','2598779',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26241117,'RPC Name','PTTYP','1','','','','2598779',CURRENT_DATE);



INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY)
VALUES (S906_RULE.NEXTVAL,'26241117','Rule for RPC Setup','Y',CURRENT_DATE,'DISPLAY_NAME','2598779');

INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY)
VALUES (S906_RULE.NEXTVAL,'26241118','Rule for RPC Setup','Y',CURRENT_DATE,'DISPLAY_NAME','2598779');

INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY)
VALUES (S906_RULE.NEXTVAL,'7010','Rule to void a party','Y',CURRENT_DATE,'EXCLUDE_PARTY_TYPE','2598779');

INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY)
VALUES (S906_RULE.NEXTVAL,'7011','Rule to void a party','Y',CURRENT_DATE,'EXCLUDE_PARTY_TYPE','2598779');

INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY)
VALUES (S906_RULE.NEXTVAL,'7008','Rule to void a party','Y',CURRENT_DATE,'EXCLUDE_PARTY_TYPE','2598779');

INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY)
VALUES (S906_RULE.NEXTVAL,'26241117','Rule to void a party','Y',CURRENT_DATE,'EXCLUDE_PARTY_TYPE','2598779');

INSERT INTO T906_RULES (C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,C906_CREATED_DATE,C906_RULE_GRP_ID,C906_CREATED_BY)
VALUES (S906_RULE.NEXTVAL,'26241118','Rule to void a party','Y',CURRENT_DATE,'EXCLUDE_PARTY_TYPE','2598779');

UPDATE t901_code_lookup 
SET c901_code_nm = 'RPC', c901_last_updated_by = '2598779', c901_last_updated_date =CURRENT_DATE WHERE c901_code_id = '26241117';

UPDATE t901_code_lookup 
SET c901_code_nm = 'Health System', c901_last_updated_by = '2598779', c901_last_updated_date =CURRENT_DATE WHERE c901_code_id = '26241118';

--for left link issue
delete from t1530_access where c1520_function_id = 'RPC Setup';
delete from T1520_FUNCTION_LIST where c1520_function_id in ('RPC Setup','Health System Setup');
INSERT INTO T1520_FUNCTION_LIST
(C1520_FUNCTION_ID,C1520_FUNCTION_NM,C1520_FUNCTION_DESC,C1520_INHERITED_FROM_ID,C901_TYPE,C1520_VOID_FL,C1520_CREATED_BY,C1520_CREATED_DATE,
C1520_REQUEST_URI, C1520_REQUEST_STROPT, C1520_SEQ_NO,C901_DEPARTMENT_ID, C901_LEFT_MENU_TYPE, C1520_LEFT_LINK_PATH)
VALUES( 'RPC_SETUP','RPC Setup', 'RPC Setup Screen', NULL, 92262, NULL, '2193221',
SYSDATE, '\gmPartySetup.do?partyType=26241117',NULL, NULL,2026, 101921,'Pricing >Transactions >RPC Setup');

 

INSERT INTO t1530_access
            (c1530_access_id, c1500_group_id, c1520_function_id,
             c101_party_id, c1530_update_access_fl, c1530_read_access_fl,
             c1530_void_access_fl, c1530_void_fl, c1530_created_by,
             c1530_created_date, c1520_parent_function_id,
             c1530_seq_no
            )
     VALUES (s1530_access.nextval, 100004, 'RPC_SETUP',
             NULL, 'Y', 'Y',
             'Y', NULL, '2193221',
             SYSDATE, 12273900,
             20452897
            );
            
INSERT INTO T1520_FUNCTION_LIST
(C1520_FUNCTION_ID,C1520_FUNCTION_NM,C1520_FUNCTION_DESC,C1520_INHERITED_FROM_ID,C901_TYPE,C1520_VOID_FL,C1520_CREATED_BY,C1520_CREATED_DATE,
C1520_REQUEST_URI, C1520_REQUEST_STROPT, C1520_SEQ_NO,C901_DEPARTMENT_ID, C901_LEFT_MENU_TYPE, C1520_LEFT_LINK_PATH)
VALUES( 'HEALTH_SETUP','Health System Setup', 'Health system Setup Screen', NULL, 92262, NULL, '2193221',
SYSDATE, '\gmPartySetup.do?partyType=26241117',NULL, NULL,2026, 101921,'Pricing >Transactions >Health System Setup');

 

INSERT INTO t1530_access
            (c1530_access_id, c1500_group_id, c1520_function_id,
             c101_party_id, c1530_update_access_fl, c1530_read_access_fl,
             c1530_void_access_fl, c1530_void_fl, c1530_created_by,
             c1530_created_date, c1520_parent_function_id,
             c1530_seq_no
            )
     VALUES (s1530_access.nextval, 100004, 'HEALTH_SETUP',
             NULL, 'Y', 'Y',
             'Y', NULL, '2193221',
             SYSDATE, 12273900,
             20452897
            ); 
update T1520_FUNCTION_LIST set C1520_REQUEST_URI='\gmPartySetup.do?partyType=26241118',C1520_LAST_UPDATED_BY = '2193221',C1520_LAST_UPDATED_DATE = SYSDATE where C1520_FUNCTION_ID='HEALTH_SETUP';