--1. to create the new code id CAPS group
	-- Code name - question id 26701
	-- alt name - Form id 267
	
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(26240829,'26701','CAPS','1','4','267','','706322',CURRENT_DATE);
	
--2. get the new code id and update to below rule value (amnios study)

INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'GPR009', 'Amnios study - Patient Satisfaction map'
      , '26240829', SYSDATE, 'CAPS_GRP'
      , '706322'
    ) ;