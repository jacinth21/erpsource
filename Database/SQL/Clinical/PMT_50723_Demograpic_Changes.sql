-- to update the current rule values (Demograhic)
-- select * From T906_RULES where C906_RULE_GRP_ID = 'DEMOPATIENT' and c906_rule_id ='GPR009';

 UPDATE t906_rules
SET c906_rule_value      = '26202', c906_last_updated_by = 706322, c906_last_updated_date = sysdate
  WHERE c906_rule_grp_id = 'STUDY_LIST_ID_GRP'
    AND c906_rule_id     = 'AMNIOS_S_FCH_PATINFO';
    
--select * From T613_STUDY_PERIOD  where c612_study_list_id   = 26202 ;

 UPDATE t906_rules
SET c906_rule_value      = '20', c906_last_updated_by = 706322, c906_last_updated_date = sysdate
  WHERE c906_rule_grp_id = 'STUDY_PRD_ID_IN_GRP'
    AND c906_rule_id     = 'AMNIOS_S_FCH_PATINFO';


-- Gender 26203
-- Age 26202 (Avg)
-- Height 26204 (Avg)
-- Weight 26205
-- BMI 26206
-- Race 


 INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'GPR009', 'Amnios study - Date of Birth'
      , '26201', SYSDATE, 'DEMOPATIENT'
      , '706322'
    ) ;

    
 INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'GPR009', 'Amnios study - Age'
      , '26202', SYSDATE, 'DEMOPATIENT'
      , '706322'
    ) ;
    
 INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'GPR009', 'Amnios study - Gender'
      , '26203', SYSDATE, 'DEMOPATIENT'
      , '706322'
    ) ;



    

 INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'GPR009', 'Amnios study - Height'
      , '26204', SYSDATE, 'DEMOPATIENT'
      , '706322'
    ) ;

 INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'GPR009', 'Amnios study - Weight'
      , '26205', SYSDATE, 'DEMOPATIENT'
      , '706322'
    ) ; 
 
 -- BMI
 
 INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'GPR009', 'Amnios study - BMI'
      , '26206', SYSDATE, 'DEMOPATIENT'
      , '706322'
    ) ; 

    -- Tobacco
    
 INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'GPR009', 'Amnios study - Tobacco'
      , '26207', SYSDATE, 'DEMOPATIENT'
      , '706322'
    ) ;     
    

    
 INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'GPR009', 'Amnios study - Race Asian'
      , '26209', SYSDATE, 'DEMOPATIENT'
      , '706322'
    ) ;     

    
 INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'GPR009', 'Amnios study - Race Black'
      , '26210', SYSDATE, 'DEMOPATIENT'
      , '706322'
    ) ;     

    
 INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'GPR009', 'Amnios study - Race Caucasian'
      , '26211', SYSDATE, 'DEMOPATIENT'
      , '706322'
    ) ;     

    
 INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'GPR009', 'Amnios study - Race Hispanic'
      , '26212', SYSDATE, 'DEMOPATIENT'
      , '706322'
    ) ;     

        
 INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'GPR009', 'Amnios study - Race Other'
      , '26213', SYSDATE, 'DEMOPATIENT'
      , '706322'
    ) ;     