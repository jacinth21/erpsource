

INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        s906_rule.nextval, 'GPR010', 'Reflect study - FFI type drop down'
      , 'REFFFI', SYSDATE, 'SURGERY_GRP'
      , '706322'
    ) ;
	
	-- Answer group id
 INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        s906_rule.nextval, '110587', 'Reflect study - Ans group id details'
      , '27608', SYSDATE, 'FCH_OUT_SCR'
      , '706322'
    ) ;	
	
	
	INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        s906_rule.nextval, '110588', 'Reflect study - Ans group id details'
      , '27616', SYSDATE, 'FCH_OUT_SCR'
      , '706322'
    ) ;
	
	INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        s906_rule.nextval, '110589', 'Reflect study - Ans group id details'
      , '27624', SYSDATE, 'FCH_OUT_SCR'
      , '706322'
    ) ;
	
	INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        s906_rule.nextval, '110590', 'Reflect study - Ans group id details'
      , '27632', SYSDATE, 'FCH_OUT_SCR'
      , '706322'
    ) ;
	
	INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        s906_rule.nextval, '110591', 'Reflect study - Ans group id details'
      , '27635', SYSDATE, 'FCH_OUT_SCR'
      , '706322'
    ) ;
	
	INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        s906_rule.nextval, '110592', 'Reflect study - Ans group id details'
      , '27640', SYSDATE, 'FCH_OUT_SCR'
      , '706322'
    ) ;
	
	
	INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        s906_rule.nextval, '110593', 'Reflect study - Ans group id details'
      , '27643', SYSDATE, 'FCH_OUT_SCR'
      , '706322'
    ) ;
    
    -- dynamic procedure
    
   INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        s906_rule.nextval, 'GPR002', 'Other outcome dynamci procedure - SECURE-C IDE '
      , 'gm_pkg_cr_outcome_rpt.gm_cr_fch_outcome_score', SYSDATE, 'CLI_DYN_OUTCOME_RPT'
      , '706322'
    ) ;
    
       INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        s906_rule.nextval, 'GPR003', 'Other outcome dynamci procedure - FLEXUS IDE Interspinous Spacer '
      , 'gm_pkg_cr_outcome_rpt.gm_cr_fch_outcome_score', SYSDATE, 'CLI_DYN_OUTCOME_RPT'
      , '706322'
    ) ;
    
       INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        s906_rule.nextval, 'GPR004', 'Other outcome dynamci procedure - TRIUMPH Lumbar Disc Pilot Study'
      , 'gm_pkg_cr_outcome_rpt.gm_cr_fch_outcome_score', SYSDATE, 'CLI_DYN_OUTCOME_RPT'
      , '706322'
    ) ;
    
       INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        s906_rule.nextval, 'GPR006', 'Other outcome dynamci procedure - ACADIA Pilot'
      , 'gm_pkg_cr_outcome_rpt.gm_cr_fch_outcome_score', SYSDATE, 'CLI_DYN_OUTCOME_RPT'
      , '706322'
    ) ;
    
       INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        s906_rule.nextval, 'GPR007', 'Other outcome dynamci procedure - TRANSITION-Postmarket Surveillance'
      , 'gm_pkg_cr_outcome_rpt.gm_cr_fch_outcome_score', SYSDATE, 'CLI_DYN_OUTCOME_RPT'
      , '706322'
    ) ;
    
       INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        s906_rule.nextval, 'GPR005', 'Other outcome dynamci procedure - ACADIA IDE Facet Replacement'
      , 'gm_pkg_cr_outcome_rpt.gm_cr_fch_outcome_score', SYSDATE, 'CLI_DYN_OUTCOME_RPT'
      , '706322'
    ) ;
    
       INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        s906_rule.nextval, 'GPR008', 'Other outcome dynamci procedure - REVLOK'
      , 'gm_pkg_cr_outcome_rpt.gm_cr_fch_outcome_score', SYSDATE, 'CLI_DYN_OUTCOME_RPT'
      , '706322'
    ) ;
    
       INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        s906_rule.nextval, 'GPR009', 'Other outcome dynamci procedure - Amnios RT'
      , 'gm_pkg_cr_outcome_rpt.gm_cr_fch_amnios_outcome_score', SYSDATE, 'CLI_DYN_OUTCOME_RPT'
      , '706322'
    ) ;
    
       INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        s906_rule.nextval, 'GPR010', 'Other outcome dynamci procedure - REFLECT'
      , 'gm_pkg_cr_outcome_rpt.gm_cr_fch_reflect_outcome_score', SYSDATE, 'CLI_DYN_OUTCOME_RPT'
      , '706322'
    ) ;