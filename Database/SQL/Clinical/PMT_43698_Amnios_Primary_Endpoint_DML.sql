
-- insert rules (to define the code group - end point dropdown)

 INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'GPR009', 'Amnios study - Primary endpoint Code group'
      , 'PEPAMQ', SYSDATE, 'PEP_QUESTIONNAIRE'
      , '706322'
    ) ;

-- new code group ( End point)

INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'PEPAMQ','Primary End poin - Amnios','','706322',CURRENT_DATE);

   
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(109380,'VAS Right Foot Pain','PEPAMQ','1','1','AMNRFP','','706322',CURRENT_DATE);

 
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(109381,'VAS Left Foot Pain','PEPAMQ','1','2','AMNLFP','','706322',CURRENT_DATE);
 
 -- VAS right foot pain
 
 INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'AMNRFP','VAS - Right foot pain','','706322',CURRENT_DATE);

   
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(109382,'26501','AMNRFP','1','1','265','','706322',CURRENT_DATE); 

 -- VAS left foot pain
 
 INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'AMNLFP','VAS - Left foot pain','','706322',CURRENT_DATE);

  
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(109383,'26502','AMNLFP','1','1','265','','706322',CURRENT_DATE); 



-- insert rules (to define the code group - Right foot pain score)

 INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, '109380', 'Amnios study - Right foot score code group'
      , 'AMNRRP', SYSDATE, 'PEP_SCORE'
      , '706322'
    ) ; 

-- insert rules (to define the code group - Left foot pain score)

INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, '109381', 'Amnios study - Left foot score code group'
      , 'AMNLRP', SYSDATE, 'PEP_SCORE'
      , '706322'
    ) ; 


-- VAS right foot score

INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'AMNRRP','Amnios - VAS Right score drop down','','706322',CURRENT_DATE);

  
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(109384,'Right VAS','AMNRRP','1','1','109382','','706322',CURRENT_DATE); 

-- VAS left foot score

INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'AMNLRP','VAS - Left foot score drop down','','706322',CURRENT_DATE);

INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(109385,'Left VAS','AMNLRP','1','1','109383','','706322',CURRENT_DATE); 

