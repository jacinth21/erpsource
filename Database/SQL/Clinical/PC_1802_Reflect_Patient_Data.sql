--1 - to update the rule values
 UPDATE T906_RULES
SET C906_RULE_VALUE      = 'REFLECT_S_FCH_PATINFO', C906_LAST_UPDATED_BY = 706322, C906_LAST_UPDATED_DATE = SYSDATE
  WHERE C906_RULE_ID     = 'GPR010'
    AND C906_RULE_GRP_ID = 'CR_FCH_PATINFO'
    AND C906_RULE_VALUE  = 'CR_S_FCH_PATINFO';
    
-- 2 insert new rules
-- T612 Demographic form

 INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'REFLECT_S_FCH_PATINFO', 'Reflect Study - Demograhics form T612 mapping'
      , '27202', SYSDATE, 'STUDY_LIST_ID_GRP'
      , '706322'
    ) ;

-- T613 - 6300
--select * From T613_STUDY_PERIOD where C612_STUDY_LIST_ID = 27202;
INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'REFLECT_S_FCH_PATINFO', 'Reflect Study - Demograhics form T613 mapping'
      , '1045', SYSDATE, 'STUDY_PRD_ID_IN_GRP'
      , '706322'
    ) ;    

    
    
    
    
    -- Surgical Data fix
    
    INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'GPR010', 'Reflect Study - Surgical report mappping'
      , 'CRSREF', SYSDATE, 'CR_FCH_SURGRES'
      , '706322'
    ) ;       
    
-- create the code lookup    

INSERT INTO t901b_code_group_master
(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
VALUES 
(s901b_code_group_master.NEXTVAL,'CRSREF','Reflect Study - Surgical data mapping','','706322',CURRENT_DATE);

 
 
  
INSERT INTO t901_code_lookup
(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)
VALUES 
(110140,'27804,27805','CRSREF','1','1','278','','706322',CURRENT_DATE); 


INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'ANS_HRS', 'Reflect Study - Surgical Data Operative time (hours)'
      , '27804', SYSDATE, 'CRSREF'
      , '706322'
    ) ;       
    
    
    INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'ANS_MIN', 'Reflect Study - Surgical Data Operative time (minutes)'
      , '27844', SYSDATE, 'CRSREF'
      , '706322'
    ) ;       
    
    
    INSERT
   INTO T906_RULES
    (
        C906_RULE_SEQ_ID, C906_RULE_ID, C906_RULE_DESC
      , C906_RULE_VALUE, C906_CREATED_DATE, C906_RULE_GRP_ID
      , C906_CREATED_BY
    )
    VALUES
    (
        S906_RULE.NEXTVAL, 'ANS_MLS', 'Reflect Study - Surgical Data est. blood loss'
      , '27805', SYSDATE, 'CRSREF'
      , '706322'
    ) ;       
    