INSERT INTO t1520_function_list
(
   C1520_FUNCTION_ID,
   C1520_FUNCTION_NM,
   C1520_FUNCTION_DESC,
   C901_TYPE,
   C1520_CREATED_BY,
   C1520_CREATED_DATE,
   C1520_REQUEST_URI,
   C1520_REQUEST_STROPT,
   C901_DEPARTMENT_ID,
   C901_LEFT_MENU_TYPE
)
VALUES
(
   'SET_OUT_POLICY_APP',
   'Set Out Of Policy App',
   'This link used to Load Set Out Of Policy screen in New tab when click on Left link',
   92262,
   '706310',
   current_date,
   '/sop/index.html',
   'NEWTAB',
   2009,
   101921
);