--PC-4980-Prevent Rollback for the loaner set has open inhouse transactions -4127 - Product Loaner
DELETE FROM t906_rules WHERE c906_rule_id ='1000' AND c906_rule_grp_id='ROLLBACKLN';
DELETE FROM t906_rules WHERE c906_rule_id ='4127' AND c906_rule_grp_id='ROLLBACKLN';
INSERT 
  INTO t906_rules (
            c906_rule_seq_id,
            c906_rule_id,
            c906_rule_desc,
			c906_rule_value,
			c1900_company_id,
			c906_created_date,
			c906_rule_grp_id,
			c906_created_by ) 
 VALUES     (S906_RULE.nextval,
			'4127',
			'Prevent Rollback for the loaner set has open inhouse transactions',
			'Y',
			1000,
			CURRENT_DATE,
			'ROLLBACKLN',
			'706328'
			);