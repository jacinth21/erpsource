/* Formatted on 2010/07/21 17:45 (Formatter Plus v4.8.0) */
-- @"c:\Database\Triggers\trg_t102_update.trg"
CREATE OR REPLACE TRIGGER trg_t102_update
   AFTER UPDATE
   ON t102_user_login
   FOR EACH ROW
DECLARE
   v_old_updated_date   t102_user_login.c102_last_updated_date%TYPE;
   v_new_updated_date   t102_user_login.c102_last_updated_date%TYPE;
BEGIN
   v_old_updated_date := :OLD.c102_last_updated_date;
   v_new_updated_date := :NEW.c102_last_updated_date;   
   
   IF ( v_new_updated_date IS NULL OR :NEW.c102_last_updated_by IS NULL
      )
   THEN
      raise_application_error
                   (-20299,'');
   END IF;
END trg_t102_update;
/
