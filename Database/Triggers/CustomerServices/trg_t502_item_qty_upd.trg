--@"C:\Database\Triggers\trg_t502_item_qty_upd.trg";
CREATE OR REPLACE TRIGGER trg_t502_item_qty_upd
	BEFORE UPDATE OF c502_item_qty
	ON t502_item_order
	FOR EACH ROW
	
	DECLARE
		--
    	v_old_qty	t502_item_order.c502_item_qty%TYPE;
    	v_new_qty	t502_item_order.c502_item_qty%TYPE;
    	--
    	v_order_type	t501_order.c901_order_type%TYPE;
    BEGIN
        --
        /*
        * This tirgger used to update the rebate process flag details.
        * 
        * 1. Once Qty updated - rebate process flag update to Null - so, that next time job will pickup and
        calcualte the Rebate
        * 
        * 2. To void the existing Discount orders (DS)
        *
        */
	    
        v_old_qty := :OLD.c502_item_qty;
        v_new_qty := :NEW.c502_item_qty;
        --
        
        IF (v_old_qty <> v_new_qty AND :OLD.c502_rebate_process_fl IS NOT NULL AND NVL(:OLD.C7200_REBATE_RATE, 0 ) <> 0)
            THEN
            
		            :NEW.c502_rebate_process_fl := NULL;
		            :NEW.c7200_rebate_rate      := NULL;
		            -- to void the DS orders
		            gm_pkg_ac_order_rebate_txn.gm_void_discount_order (:OLD.c501_order_id, 30301) ;
		            --
		            

        END IF; -- end of Qty check
        
        --
    END trg_t502_item_qty_upd;
    /