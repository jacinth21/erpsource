
--@"C:\DATABASE\Triggers\trg_t2060_di_part_mapping.trg";
/*********************************************************************
* Purpose: Need to add an update to the table t2001, when we map/unmap the di number to a part number
* Author: arajan
********************************************************************/

CREATE OR REPLACE TRIGGER trg_t2060_di_part_mapping
	BEFORE INSERT OR UPDATE ON t2060_di_part_mapping
	FOR EACH ROW
	DECLARE
		v_action			VARCHAR2(20) := NULL;
		v_system_id			T207_SET_MASTER.C207_SET_ID%TYPE;
		v_cmp_cnt			NUMBER;
		v_rfs_cnt			NUMBER;
   BEGIN
   		
        IF ((:OLD.c2060_di_number <> :NEW.c2060_di_number) OR (:OLD.C205_PART_NUMBER_ID <> :NEW.C205_PART_NUMBER_ID)) THEN
            v_action := '103123';  -- Updated
        END IF;
        
        v_system_id := get_set_id_from_part(:NEW.C205_PART_NUMBER_ID);

        
        	SELECT COUNT(1) 
        	  INTO v_rfs_cnt 
        	  FROM T205D_PART_ATTRIBUTE 
        	 WHERE C205_PART_NUMBER_ID=:NEW.C205_PART_NUMBER_ID
	           AND C901_ATTRIBUTE_TYPE='80180' 
	           AND C205D_VOID_FL IS NULL;
        
            --check part is available in GMNA company
        	SELECT count(1) INTO v_cmp_cnt
			  FROM t2023_part_company_mapping t2023, t205_part_number t205
			 WHERE t205.c205_part_number_id = :NEW.C205_PART_NUMBER_ID
               AND t205.c205_part_number_id = t2023.c205_part_number_id
               AND T205.C205_PRODUCT_FAMILY   IN (SELECT C906_RULE_VALUE
												   FROM T906_RULES
												  WHERE C906_RULE_ID     = 'PART_PROD_FAMILY'
													AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
													AND c906_void_fl    IS NULL)
        	   AND T205.c205_active_fl        IS NOT NULL
			   AND c1900_company_id = 1000
			   AND c2023_void_fl IS NULL;
        -- Check if any of these column data is modified. If it is, then we should treat this as an update to part number.
        -- 4000411 Part Number , 103097 English, 103100 Product Catalog
		IF ( v_action IS NOT NULL AND v_cmp_cnt > 0 AND v_rfs_cnt >0 )
		THEN
	        gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd(:NEW.C205_PART_NUMBER_ID
								     , '4000411', v_action 
								     ,'103097','103100'
								     ,:NEW.C2060_LAST_UPDATED_BY
								     ,v_system_id
								     ,'Forceupdate'
								     ,'1000'
								     );
		END IF;
        
END trg_t2060_di_part_mapping;
/
