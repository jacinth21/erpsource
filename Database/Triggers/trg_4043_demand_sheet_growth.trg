/* Formatted on 2008/03/06 16:30 (Formatter Plus v4.8.0) */
CREATE OR REPLACE TRIGGER trg_4043_demand_sheet_growth
	BEFORE INSERT OR UPDATE OF c4043_value
	ON t4043_demand_sheet_growth
	FOR EACH ROW
DECLARE
	v_old		   t4043_demand_sheet_growth.c4043_value%TYPE;
	v_new		   t4043_demand_sheet_growth.c4043_value%TYPE;
BEGIN
--
	v_old		:= :OLD.c4043_value;
	v_new		:= :NEW.c4043_value;

-- If type equals to forecast and the value then load the value
	IF (v_old <> v_new)
	THEN
		-- For first time initial load
		IF (:OLD.c4043_history_fl IS NULL)
		THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.c4043_demand_sheet_growth_id
													 , v_old
													 , 1004
													 , :OLD.c4043_updated_by
													 , :OLD.c4043_updated_date
													  );
		END IF;

		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.c4043_demand_sheet_growth_id
												 , v_new
												 , 1004
												 , :NEW.c4043_updated_by
												 , :NEW.c4043_updated_date
												  );
		:NEW.c4043_history_fl := 'Y';
--
	END IF;
--
END trg_4043_demand_sheet_growth;
/
