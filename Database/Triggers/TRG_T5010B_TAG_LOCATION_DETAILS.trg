/* Formatted on 2012/02/09 18:05 (Formatter Plus v4.8.0) */
--@"C:\DATABASE\Triggers\TRG_T5010B_TAG_LOCATION_DETAILS.trg";

CREATE OR REPLACE TRIGGER TRG_T5010B_TAG_LOCATION_DETAILS
	BEFORE UPDATE
	ON T5010B_TAG_LOCATION_DETAILS
	FOR EACH ROW
DECLARE

BEGIN

	IF (:OLD.C5010B_HISTORY_FL IS NULL) THEN
		gm_Pkg_Op_Order_Txn.gm_save_tag_location_log (:OLD.c5010_tag_id
										 , :OLD.c207_set_id
										 , :OLD.C704_Account_Id
										 , :OLD.C703_SALES_REP_ID
										 , :OLD.C5010B_TAG_LATITUDE
										 , :OLD.C5010B_TAG_LONGTITUDE
										 , :OLD.C5010B_TAG_GEO_LOCATION
										 , :OLD.C1900_COMPANY_ID
										 , :OLD.C5040_Plant_Id
										 , :OLD.C5010B_CREATED_BY
										  );
		:NEW.C5010B_HISTORY_FL := 'Y';
	END IF;

	gm_Pkg_Op_Order_Txn.gm_save_tag_location_log (:NEW.c5010_tag_id
									 , :NEW.c207_set_id
									 , :NEW.C704_Account_Id
									 , :NEW.C703_SALES_REP_ID
									 , :NEW.C5010B_TAG_LATITUDE
									 , :NEW.C5010B_TAG_LONGTITUDE
									 , :NEW.C5010B_TAG_GEO_LOCATION
									 , :NEW.C1900_COMPANY_ID
									 , :NEW.C5040_Plant_Id
									 , :NEW.C5010B_CREATED_BY							 
									  );
--
END trg_t5010_tag;
/
