/* Formatted on 2008/10/09 12:56 (Formatter Plus v4.8.0) */
--@"c:\database\triggers\trg_part_lead_time_upd.trg"

CREATE OR REPLACE TRIGGER trg_part_lead_time_upd
	BEFORE UPDATE OF c205d_attribute_value
	ON t205d_part_attribute
	FOR EACH ROW
DECLARE
	v_old		   t205d_part_attribute.c205d_attribute_value%TYPE;
	v_new		   t205d_part_attribute.c205d_attribute_value%TYPE;
	--
	v_old_val		   t205d_part_attribute.c205d_attribute_value%TYPE;
	v_new_val		   t205d_part_attribute.c205d_attribute_value%TYPE;
	--
	v_attribute_val t941_audit_trail_log.c941_value%TYPE;
	v_audit_trail_id t940_audit_trail.c940_audit_trail_id%TYPE;
	v_save_code_name VARCHAR2(20);
	--
	v_issuing_ag_nm	VARCHAR2(4000);
BEGIN
--
	v_old		:= NVL(:OLD.c205d_attribute_value,'-999');
	v_new		:= NVL(:NEW.c205d_attribute_value,'-999');

--
	IF (v_old <> v_new)
	THEN
		--
		BEGIN
			SELECT get_rule_value (:OLD.c901_attribute_type, 'AUDIT_TRAIL_ID')
   				INTO v_audit_trail_id
   			FROM dual;
		EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_audit_trail_id := NULL;
		END;
		--
		IF (v_audit_trail_id IS NOT NULL)
		THEN
			--
			v_old_val := :OLD.c205d_attribute_value;
			v_new_val := :NEW.c205d_attribute_value;
			--
			BEGIN
				SELECT get_rule_value (:OLD.c901_attribute_type, 'SAVE_CODE_NAME')
	   				INTO v_save_code_name
	   			FROM dual;
			EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_save_code_name := NULL;
			END;
			--
			IF (:OLD.c901_attribute_type = 4000539) -- Issuing Agency
		    THEN
		    BEGIN
		         SELECT c1610_issue_agency_config_nm
		           INTO v_issuing_ag_nm
		           FROM v1610_issuing_agency_config
		          WHERE c1610_issue_agency_config_id = v_new_val;
		    EXCEPTION
		    WHEN NO_DATA_FOUND THEN
		        v_issuing_ag_nm := NULL;
		    END;
		    --
		    v_new_val := v_issuing_ag_nm;
		    --
		    BEGIN
		         SELECT c1610_issue_agency_config_nm
		           INTO v_issuing_ag_nm
		           FROM v1610_issuing_agency_config
		          WHERE c1610_issue_agency_config_id = v_old_val;
		    EXCEPTION
		    WHEN NO_DATA_FOUND THEN
		        v_issuing_ag_nm := NULL;
		    END;
		    --
		    v_old_val := v_issuing_ag_nm;    
			ELSIF (v_save_code_name IS NOT NULL AND v_save_code_name ='Y')
			THEN
				BEGIN
					SELECT get_code_name (:OLD.c205d_attribute_value), get_code_name (:NEW.c205d_attribute_value)
					  INTO v_old_val, v_new_val
					FROM dual;
				EXCEPTION
				WHEN NO_DATA_FOUND
	 		    THEN
	 		      v_old_val := NULL;
	 		      v_new_val := NULL;
	 		    END;
			END IF;
			--
			IF (:OLD.c205d_history_fl IS NULL)
			THEN
				gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.c205_part_number_id
														 , v_old_val
														 , v_audit_trail_id
														 , :OLD.C205D_CREATED_BY
														 , :OLD.C205D_CREATED_DATE
														  );
			END IF;-- end history check
	
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.c205_part_number_id
													 , v_new_val
													 , v_audit_trail_id
													 , :NEW.c205d_last_updated_by
													 , :NEW.c205d_last_updated_date
													  );
			--
			:NEW.c205d_history_fl := 'Y';
		END IF; -- end v_audit_trail_id check
--
	END IF; -- end old and new value check
--
END trg_part_lead_time_upd;
/