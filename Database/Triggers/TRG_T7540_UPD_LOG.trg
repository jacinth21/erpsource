/* Formatted on 2017/03/31 */
--Trigger to update T7540A_ACCOUNT_GRP_PRC_LOG (PRT - 288)
-- Author: Matt B
--@"C:\Database\Triggers\TRG_T7540_UPD_LOG.trg"
CREATE OR REPLACE TRIGGER TRG_T7540_UPD_LOG AFTER
  INSERT OR  UPDATE OF C7540_UNIT_PRICE,
           C7540_VOID_FL ON T7540_Account_Group_Pricing 
    FOR EACH ROW 
    DECLARE
         v_action VARCHAR2(20) ;
  BEGIN
  
  
    IF INSERTING THEN
      v_action := '103122';        -- record inserted
    ELSIF UPDATING THEN
      IF :NEW.C7540_VOID_FL IS NOT NULL THEN
        v_action            := '4000412';    --Recorder voided
      ELSE
        v_action := '103123';      -- record updated
      END IF;
    END IF;
    
    INSERT
    INTO T7540A_ACCOUNT_GRP_PRC_LOG
      (
        C7540_ACCOUNT_GRP_PRC_LOG_ID ,
        C7540_ACCOUNT_GROUP_PRICING_ID ,
        C101_PARTY_ID ,
        C207_SET_ID,
        C4010_GROUP_ID,
        C901_PRICED_TYPE ,
        C205_PART_NUMBER_ID ,
        C7540_UNIT_PRICE ,
        C1900_COMPANY_ID ,
        C7540_VOID_FL ,
        C7540_LAST_UPDATED_BY ,
        C7540_LAST_UPDATED_DATE ,
        C7540_LAST_UPDATE_TRANS_ID
      )
      VALUES
      (
        S7540A_ACCT_GRP_LOG.Nextval ,
        :NEW.C7540_ACCOUNT_GROUP_PRICING_ID ,
        :OLD.C101_PARTY_ID ,
        :OLD.C207_SET_ID,
        :OLD.C4010_GROUP_ID,
        :OLD.C901_PRICED_TYPE ,
        :OLD.C205_PART_NUMBER_ID ,
        :OLD.C7540_UNIT_PRICE ,
        :OLD.C1900_COMPANY_ID ,
        :OLD.C7540_VOID_FL ,
        :OLD.C7540_LAST_UPDATED_BY ,
        :OLD.C7540_LAST_UPDATED_ON ,
        v_action
      );
      
    INSERT
    INTO T7540A_ACCOUNT_GRP_PRC_LOG
      (
        C7540_ACCOUNT_GRP_PRC_LOG_ID ,
        C7540_ACCOUNT_GROUP_PRICING_ID ,
        C101_PARTY_ID ,
        C207_SET_ID ,
        C4010_GROUP_ID ,
        C901_PRICED_TYPE ,
        C205_PART_NUMBER_ID ,
        C7540_UNIT_PRICE ,
        C1900_COMPANY_ID ,
        C7540_VOID_FL ,
        C7540_LAST_UPDATED_BY ,
        C7540_LAST_UPDATED_DATE ,
        C7540_LAST_UPDATE_TRANS_ID
      )
      VALUES
      (
        S7540A_ACCT_GRP_LOG.Nextval ,
        :NEW.C7540_ACCOUNT_GROUP_PRICING_ID ,
        :NEW.C101_PARTY_ID ,
        :NEW.C207_SET_ID ,
        :NEW.C4010_GROUP_ID ,
        :NEW.C901_PRICED_TYPE ,
        :NEW.C205_PART_NUMBER_ID ,
        :NEW.C7540_UNIT_PRICE ,
        :NEW.C1900_COMPANY_ID ,
        :NEW.C7540_VOID_FL ,
        :NEW.C7540_LAST_UPDATED_BY ,
        :NEW.C7540_LAST_UPDATED_ON ,
        v_action
      );
  END TRG_T7540_UPD_LOG;
/