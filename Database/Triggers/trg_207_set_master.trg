--@"c:\database\triggers\trg_207_set_master.trg";
CREATE OR REPLACE TRIGGER trg_207_set_master 
   BEFORE  
   	INSERT OR UPDATE ON T207_SET_MASTER FOR EACH ROW 
   DECLARE
	v_old_nm			t207_set_master.c207_set_nm%TYPE;
	v_new_nm			t207_set_master.c207_set_nm%TYPE;
	v_old_desc			t207_set_master.c207_set_desc%TYPE;
	v_new_desc			t207_set_master.c207_set_desc%TYPE;
	v_old_dtl_desc		t207_set_master.c207_set_dtl_desc%TYPE;
	v_new_dtl_desc		t207_set_master.c207_set_dtl_desc%TYPE;
	v_old_hrchy			t207_set_master.c901_hierarchy%TYPE;
	v_new_hrchy			t207_set_master.c901_hierarchy%TYPE;
	v_old_ctgry			t207_set_master.c207_category%TYPE;
	v_new_ctgry			t207_set_master.c207_category%TYPE;
	v_old_type			t207_set_master.c207_type%TYPE;
	v_new_type			t207_set_master.c207_type%TYPE;
	v_old_void_fl		t207_set_master.c207_void_fl%TYPE;
	v_new_void_fl		t207_set_master.c207_void_fl%TYPE;
	v_set_grp_type		t207_set_master.c901_set_grp_type%TYPE;
	v_new_set_grp_type	t207_set_master.c901_set_grp_type%TYPE;
	v_ref_type			VARCHAR2 (20) ;
	v_action			VARCHAR2 (20) ;
	v_system_id			T207_SET_MASTER.C207_SET_ID%TYPE;
	v_old_system		t207_set_master.C207_SET_SALES_SYSTEM_ID%TYPE;
	v_new_system		t207_set_master.C207_SET_SALES_SYSTEM_ID%TYPE;
	v_cnt_pub_pc		NUMBER;
	v_force_upd			VARCHAR2(1) := 'Y';
	v_old_status_id	    t207_set_master.c901_status_id%TYPE;	
    v_new_status_id	    t207_set_master.c901_status_id%TYPE;
    v_company_id        t207_set_master.c1900_company_id%TYPE;
    v_cmp_cnt  			NUMBER;
	
	-- below cursor used for get iPad releasd companies from the RULE , Updates should go only for iPad used conmapanies
	CURSOR setcomp_cur
		IS
		SELECT T2080.C1900_COMPANY_ID COMPID
			FROM T2080_SET_COMPANY_MAPPING T2080 
			WHERE T2080.C207_SET_ID = :NEW.c207_set_id
			AND T2080.C1900_COMPANY_ID IN (SELECT token FROM v_in_list WHERE token IS NOT NULL)
			AND T2080.C2080_VOID_FL IS NULL;
		
   BEGIN

        v_old_nm           := NVL(:old.c207_set_nm,'-9999');
        v_new_nm           := NVL(:new.c207_set_nm,'-9999');
        v_old_desc         := NVL(:old.c207_set_desc,'-9999');
        v_new_desc         := NVL(:new.c207_set_desc,'-9999');
        v_old_dtl_desc     := NVL(:old.c207_set_dtl_desc,'-9999');
        v_new_dtl_desc     := NVL(:new.c207_set_dtl_desc,'-9999');
        v_old_hrchy        := NVL(:old.c901_hierarchy,'-9999');
        v_new_hrchy        := NVL(:new.c901_hierarchy,'-9999');
        v_old_ctgry        := NVL(:old.c207_category,'-9999');
        v_new_ctgry        := NVL(:new.c207_category,'-9999');
        v_old_type         := NVL(:old.c207_type,'-9999');
        v_new_type         := NVL(:new.c207_type,'-9999');
        v_old_void_fl      := :old.c207_void_fl;
        v_new_void_fl      := :new.c207_void_fl;
        v_set_grp_type     := NVL(:new.c901_set_grp_type,'-9999');
        v_old_system	   := NVL(:old.C207_SET_SALES_SYSTEM_ID,'-9999');	
        v_new_system	   := NVL(:new.C207_SET_SALES_SYSTEM_ID,'-9999');
        v_old_status_id	   := NVL(:old.c901_status_id,'-9999');	
        v_new_status_id	   := NVL(:new.c901_status_id,'-9999');
        v_company_id       := :new.C1900_COMPANY_ID;
		SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual; 
		my_context.set_my_inlist_ctx (get_rule_value('IPAD_RELEASED_COMP', 'ONEPORTAL'));

        
        -- 1600 - System type, 1601 - SET type
        IF (v_set_grp_type IN ('1600', '1601')) THEN
            IF INSERTING THEN
                v_action := '103122'; -- New
            ELSIF UPDATING THEN
                IF ( (v_old_nm <> v_new_nm) OR (v_old_desc <> v_new_desc) OR (v_old_dtl_desc <> v_new_dtl_desc) 
                	OR ( v_old_hrchy <> v_new_hrchy) OR (v_old_ctgry <> v_new_ctgry) OR (v_old_type <> v_new_type)
                	OR (v_old_system <> v_new_system) OR (v_old_status_id <> v_new_status_id)
                	) THEN
                    v_action        := '103123'; -- Updated
                END IF;
            END IF;
            
            -- When a set is voided, the If condition will satisfy.
            IF (v_old_void_fl IS NULL AND v_new_void_fl IS NOT NULL) THEN
                v_action      := '4000412'; -- Voided
                -- When a set is made active from voided, this condition will satisfy.
            ELSIF (v_old_void_fl IS NOT NULL AND v_new_void_fl IS NULL) THEN
                v_action         := '103123'; -- Updated
            END IF;
            
            -- 1600 - System type, 1601 - SET type
            IF v_set_grp_type    = '1600' THEN
                v_ref_type      := '103108'; -- System ref type
                v_system_id := :NEW.c207_set_id; 
                
                --Check if the system is released for PC. If not, no updates should be fired.
               SELECT COUNT(1) INTO v_cnt_pub_pc
				 FROM	t207c_set_attribute t207c 
				WHERE	t207c.c207_set_id = v_system_id
				  AND	t207c.c207c_void_fl IS NULL 
				  AND	t207c.c901_attribute_type = '103119'    --Publish to 
				  AND	t207c.c207c_attribute_value = '103085';  -- Product Catalog
               
			   IF(v_cnt_pub_pc = 0)
	            THEN
	            	v_force_upd :='';
	            	v_action := '';
	            END IF;
	            
					IF (v_action IS NOT NULL AND v_ref_type IS NOT NULL) 
						THEN
						-- 103097 English, 103100 Product Catalog
						gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (:NEW.c207_set_id, v_ref_type, v_action, '103097',
												  '103100', NVL (:NEW.c207_created_by, :NEW.c207_last_updated_by)
												  , v_system_id,v_force_upd,v_company_id) ;
					END IF;	
            ELSIF v_set_grp_type = '1601' THEN
                v_ref_type      := '103111'; -- SET ref type
                v_system_id := :new.C207_SET_SALES_SYSTEM_ID;
                
                --When the set status is changed the respective action should be assigned.
                IF v_old_status_id = '20367' AND v_new_status_id <> '20367' 
	            THEN
	           		v_action      := '4000412';   --from approved to others that action should be voided. 
	           	ELSIF v_new_status_id = '20367' 
	           	THEN
	           	    v_action      := '103123';    --from others to approved that action should be updated. 
	           	ELSIF v_old_status_id <> '20367' AND v_new_status_id <> '20367'  
	           	THEN
	           		v_action      := NULL;      --Added set group type because system updates are not triggering. Only for set and status not 20367 the update should not happen. 
	            END IF;
	            
 
                -- Check SET is mapped to GMNA company
         	    SELECT COUNT(1) INTO v_cmp_cnt
				  FROM T2080_SET_COMPANY_MAPPING T2080 
				 WHERE T2080.C207_SET_ID = :NEW.c207_set_id
				   AND T2080.C2080_VOID_FL IS NULL
                   AND T2080.C1900_COMPANY_ID IN (v_company_id);

				IF v_cmp_cnt = 0 THEN
	            	v_force_upd :='';
	            	v_action := '';	            
	            END IF;
	            
			  FOR v_setcomp IN setcomp_cur
				LOOP
				
            -- Check if any of these column data is modified. If it is, then we should treat this as an update to Set
            		IF (v_action IS NOT NULL AND v_ref_type IS NOT NULL) 
						THEN
                -- 103097 English, 103100 Product Catalog
                			gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (:NEW.c207_set_id, v_ref_type, v_action, '103097',
                                                  '103100', NVL (:NEW.c207_created_by, :NEW.c207_last_updated_by)
                                                  , v_system_id,v_force_upd,v_setcomp.COMPID) ;
                -- send update for SET part when set status is approved
                		IF v_set_grp_type = '1601' AND v_old_status_id <> '20367' AND v_new_status_id = '20367' 
                
	            			THEN                                
                				gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (:NEW.c207_set_id, '4000409', v_action, '103097',
                                                  '103100', NVL (:NEW.c207_created_by, :NEW.c207_last_updated_by)
                                                  , v_system_id,v_force_upd,v_setcomp.COMPID) ; 
                		END IF;
					END IF;
				END LOOP;
            END IF;		
        END IF; 
    END trg_207_set_master;
    /