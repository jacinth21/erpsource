
--pc-407 -record the inventory change of transactions
CREATE OR REPLACE TRIGGER trg_5050a_invpick_status_log AFTER
  INSERT OR
  UPDATE ON t5050_invpick_assign_detail 
  FOR EACH ROW 
  DECLARE
	v_old_allocated_user	t5050_invpick_assign_detail.c101_allocated_user_id%type;
	v_new_allocated_user	t5050_invpick_assign_detail.c101_allocated_user_id%type;
	v_transacted_by			t5050a_invpick_status_log.c5050a_transacted_by%type;
	v_transacted_date_time	t5050a_invpick_status_log.c5050a_transacted_date_time%type;
	v_action				t5050a_invpick_status_log.c901_action%type;
	v_set_cnt				number;
    v_type                  t5050a_invpick_status_log.c5050a_type%type;
  BEGIN

	IF  NVL(:old.c901_status,0) <> NVL(:new.c901_status,0) 
       OR (v_new_allocated_user  IS NOT NULL AND v_old_allocated_user <> v_new_allocated_user) 
	THEN

		v_old_allocated_user := nvl(:old.c101_allocated_user_id,0);
		v_new_allocated_user := nvl(:new.c101_allocated_user_id,0);

		-- if the user is assiged for picking the transaction, then allocated details need to consider instead of created details
		IF v_old_allocated_user <> v_new_allocated_user
		THEN
			v_transacted_by := v_new_allocated_user;
			v_transacted_date_time := :new.c5050_allocated_dt;
		ELSE
			v_transacted_by := nvl(:new.c5050_last_updated_by,:new.c5050_created_by);
			v_transacted_date_time := nvl(:new.c5050_last_updated_date,:new.c5050_created_date);
		END IF;

		IF :new.c901_ref_type = 93604 --  Pending Putaway
		THEN
			v_action := 93327; --putaway
			v_type   := 18561;
		ELSE
			v_action := 93326; --pick

			SELECT count(1) 
			INTO v_set_cnt
			FROM t504_consignment t504
			WHERE t504.c504_consignment_id = :new.c5050_ref_id
			AND t504.c207_set_id is not null
			AND t504.c504_void_fl is null;
		END IF;

		-- set or part
		--v_type := v_set_cnt > 0 ? 18561 :18562;
        
        SELECT decode(v_set_cnt,0,18562,18561) INTO v_type FROM dual;

			  INSERT
			  INTO t5050a_invpick_status_log
				(
				  c5050a_ref_id,
				  c5050a_status,
				  c901_ref_type,
				  c901_action,
				  c5050a_type,
				  c5050a_transacted_by,
				  c5050a_transacted_date,
				  c5050a_transacted_date_time,
				  c1900_company_id,
				  c5040_plant_id
				)
				VALUES
				(
				  :new.c5050_ref_id,
				  nvl(:new.c901_status,93004), -- initiated
				  :new.c901_ref_type,
				  v_action, --put or pick
				  v_type, -- set or part
				  v_transacted_by,
				  trunc(v_transacted_date_time),
				  v_transacted_date_time,
				  :new.c1900_company_id,
				  :new.c5040_plant_id
				);
				
	END IF;
END trg_5050a_invpick_status_log;
/