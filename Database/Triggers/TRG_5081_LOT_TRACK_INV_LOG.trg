--@"c:\database\triggers\TRG_5081_LOT_TRACK_INV_LOG.trg"
CREATE OR REPLACE TRIGGER TRG_5081_LOT_TRACK_INV_LOG
	BEFORE INSERT OR UPDATE OF c5080_qty
	ON T5080_LOT_TRACK_INV
	FOR EACH ROW
DECLARE

	v_set_id   		t207_set_master.c207_set_id%type;
	v_set_name		t207_set_master.c207_set_nm%type;
	v_tag_id		t5010_tag.c5010_tag_id%type;

BEGIN
IF (	UPDATING
		AND (	:NEW.C5080_LAST_UPDATED_BY IS NULL
			 OR :NEW.C5080_LAST_UPDATE_TRANS_ID IS NULL
			 		 
			)
	   )
	THEN
		raise_application_error
			('-20501'
		   , 'Either of the following is not correctly entered : Transaction by, Trasaction id,' 
			);
	END IF;
	
	BEGIN 
		SELECT t504.c207_set_id, t207.c207_set_nm
		  INTO v_set_id,v_set_name
		  FROM t207_set_master t207,
    		   t504_consignment t504
		 WHERE t504.c207_set_id = t207.c207_set_id
    	   AND c504_consignment_id = :NEW.c5080_last_update_trans_id
    	   AND c504_void_fl IS NULL;
     EXCEPTION WHEN OTHERS THEN
		v_set_id := NULL;
		v_set_name := NULL;
	 END;
	 
     BEGIN
		SELECT t5010.c5010_tag_id
		  INTO v_tag_id
          FROM t5010_tag t5010
         WHERE t5010.c5010_last_updated_trans_id = :NEW.c5080_last_update_trans_id
	       AND t5010.c205_part_number_id = :NEW.C205_PART_NUMBER_ID
		   AND c5010_void_fl IS NULL;
	 EXCEPTION WHEN OTHERS THEN
		v_tag_id := NULL;
	 END;
  
	IF (UPDATING AND NVL (:OLD.c5080_qty, 0) != NVL (:NEW.c5080_qty, 0))
	THEN
	gm_pkg_op_lot_track_data.gm_sav_lot_track_inv_log (	
														  :NEW.c205_part_number_id
  							     			            , :NEW.c2550_control_number
														, :NEW.c901_warehouse_type
													    , :OLD.c5080_qty
														, (:NEW.c5080_qty-:OLD.c5080_qty)
   													    , :NEW.c5080_qty
														, :NEW.c5080_last_update_trans_id
														, :NEW.c5080_control_number_inv_id
														, :NEW.c901_last_transaction_type
														, :NEW.c5080_location_id
														, :NEW.c901_location_type
														, :NEW.c5080_location_name
														, :NEW.c1900_company_id
														, :NEW.c5040_plant_id
														, v_set_id
														, v_set_name
														, v_tag_id
														, :NEW.c5080_created_date
														 ); 								 
														 

	ELSIF (INSERTING AND NVL (:NEW.c5080_qty, 0) <> 0)
	THEN
		IF :NEW.c5080_last_update_trans_id IS NULL 
		THEN
        -- Need to validate this
			raise_application_error
				('-20501'
		   			, 'The following is not correctly entered : Trasaction id'
				);
		END IF;
	gm_pkg_op_lot_track_data.gm_sav_lot_track_inv_log (
	  												      :NEW.c205_part_number_id
  							     				        , :NEW.c2550_control_number
														, :NEW.c901_warehouse_type
													    ,  0
   													    , :NEW.c5080_qty
														, :NEW.c5080_qty
													    , :NEW.c5080_last_update_trans_id
														, :NEW.c5080_control_number_inv_id
														, :NEW.c901_last_transaction_type
														, :NEW.c5080_location_id
														, :NEW.c901_location_type
														, :NEW.c5080_location_name
														, :NEW.c1900_company_id
														, :NEW.c5040_plant_id
														, v_set_id
														, v_set_name
														, v_tag_id
														, :NEW.c5080_created_date
														 );
										 
	END IF;
	
	--added for PMT#54256, trigger when warehouse or location changed
	IF (UPDATING AND NVL (:OLD.c5080_qty, 0) = NVL (:NEW.c5080_qty, 0) AND ((:OLD.C901_WAREHOUSE_TYPE <> :NEW.C901_WAREHOUSE_TYPE)
        OR (:OLD.C5080_LOCATION_ID <> :NEW.C5080_LOCATION_ID)))
	THEN
	
	gm_pkg_op_lot_track_data.gm_sav_lot_track_inv_log (	
														  :NEW.c205_part_number_id
  							     			            , :NEW.c2550_control_number
														, :NEW.c901_warehouse_type
													    , :OLD.c5080_qty
														, (:NEW.c5080_qty-:OLD.c5080_qty)
   													    , :NEW.c5080_qty
														, :NEW.c5080_last_update_trans_id
														, :NEW.c5080_control_number_inv_id
														, :NEW.c901_last_transaction_type
														, :NEW.c5080_location_id
														, :NEW.c901_location_type
														, :NEW.c5080_location_name
														, :NEW.c1900_company_id
														, :NEW.c5040_plant_id
														, v_set_id
														, v_set_name
														, v_tag_id
														, :NEW.c5080_created_date
														 ); 

	END IF;
	
--- Updating the below values as Null to ensure any manual updates in the table need txn id.
/*
	:NEW.c5053_last_updated_by := NULL;
	:NEW.c5071_last_updated_txn_id := NULL;*/
END TRG_5081_LOT_TRACK_INV_LOG;
/
