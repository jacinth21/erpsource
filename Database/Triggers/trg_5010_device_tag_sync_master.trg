CREATE OR REPLACE
TRIGGER trg_5010_device_tag_sync_master
   BEFORE INSERT OR UPDATE OF C5010_TAG_ID,C207_SET_ID,C901_STATUS,C1900_COMPANY_ID,C5010_VOID_FL
   ON T5010_TAG
   FOR EACH ROW

DECLARE
	v_action  VARCHAR2(20) ;
	v_company_id T5010_TAG.C1900_COMPANY_ID%TYPE;
	v_old_company_id     T5010_TAG.C1900_COMPANY_ID%TYPE;
	v_new_company_id     T5010_TAG.C1900_COMPANY_ID%TYPE;
BEGIN
	v_company_id :=:NEW.C1900_COMPANY_ID;
	v_old_company_id     := :OLD.C1900_COMPANY_ID;
    v_new_company_id     := :NEW.C1900_COMPANY_ID;

   	  IF v_company_id IS NULL THEN
      		 v_company_id := v_old_company_id;
       ELSE
      		 v_company_id := v_new_company_id;
       END IF;
       
  IF INSERTING THEN
        v_action := '103122'; -- New
  ELSIF UPDATING THEN
    IF (( NVL(:OLD.C207_SET_ID,'-999') <> :NEW.C207_SET_ID)
    	  OR (:OLD.C901_STATUS <> :NEW.C901_STATUS) 
  		  OR (:OLD.C5010_TAG_ID <> :NEW.C5010_TAG_ID)
  		  OR (:OLD.C1900_COMPANY_ID <> :NEW.C1900_COMPANY_ID) 
  		  OR (NVL(:OLD.C5010_VOID_FL,'N') <> NVL(:NEW.C5010_VOID_FL,'N') AND NVL(:NEW.C5010_VOID_FL,'N') = 'N')
  		 )
  	THEN
  		v_action := '103123'; -- Updated
  	END IF;
  END IF;
  
 -- When a Tag is voided, the If condition will satisfy.
  IF (NVL(:OLD.C5010_VOID_FL,'N') <> NVL(:NEW.C5010_VOID_FL,'N') AND NVL(:NEW.C5010_VOID_FL,'N') = 'Y') 
  THEN
    v_action  := '4000412'; -- Voided
  END IF;

  IF v_action IS NOT NULL THEN
    -- 103097 English, 103100 Product Catalog , 4001234 Ref Type
    gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (:NEW.C5010_TAG_ID, '4001234', v_action, '103097', '103100', NVL (:NEW.C5010_CREATED_BY, :NEW.C5010_LAST_UPDATED_BY ), NULL,'Y',v_company_id) ;
 END IF;
      
END; 
/