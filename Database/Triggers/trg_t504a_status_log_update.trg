/* Formatted on 2011/07/14 19:16 (Formatter Plus v4.8.0) */
--@"c:\Database\Triggers\trg_t504a_status_log_update.trg"
CREATE OR REPLACE TRIGGER trg_t504a_status_log_update
   AFTER INSERT OR UPDATE
   ON t504a_consignment_loaner
   FOR EACH ROW
DECLARE
   
   v_type             NUMBER := 0;
   v_loaner_turn_no   NUMBER;
   v_status           NUMBER;
   v_set_id  t207_set_master.c207_set_id%TYPE;
   v_cnt  			  NUMBER;
   v_company_id      t504a_consignment_loaner.c1900_company_id%TYPE;
   v_plant_id        t504a_consignment_loaner.c5040_plant_id%TYPE;
   v_tag_id          t5010_tag.c5010_tag_id%TYPE;
BEGIN
   /**
    *  The status details are tracked as follows
    * 100 --> Accepted
    * 150 --> WIP
    * 200 --> Checked
    * 250 --> Processed
    * 300 --> Picture Taken
    * 350 --> Available
    * 400 --> Allocated
    * 450 --> Shipped
    * 750 --> InActivated
    * 800 --> Deployed
    * 850 --> Missed
    * 900 --> Disputed
    */
     v_company_id       := :new.C1900_COMPANY_ID; 
     v_plant_id         := :new.C5040_PLANT_ID;
   IF (NVL (:NEW.c504a_status_fl, -9999) != NVL (:OLD.c504a_status_fl, -9999))
   THEN
      SELECT c901_code_id
        INTO v_type
        FROM t901_code_lookup t901
       WHERE c901_code_grp = 'LOANS'
             AND c902_code_nm_alt = :NEW.c504a_status_fl;

      SELECT MAX (c504a_loaner_transaction_id)
        INTO v_loaner_turn_no
        FROM t504a_loaner_transaction t504a
       WHERE t504a.c504_consignment_id = :NEW.c504_consignment_id
         AND t504a.c504a_void_fl IS NULL;

      IF :OLD.c504a_status_fl = 58 AND :NEW.c504a_status_fl = 0
      THEN
         gm_save_status_details (:NEW.c504_consignment_id,
                                 300,                         -- Picture Taken
                                 :NEW.c504a_last_updated_by,
                                 NULL,
                                 SYSDATE,
                                 v_type,
                                 91107,
                                 v_loaner_turn_no
                                );
         v_status := 350;                                         -- Available
      ELSIF :OLD.c504a_status_fl = 58 AND :NEW.c504a_status_fl IN (5, 10)
      THEN
         gm_save_status_details (:NEW.c504_consignment_id,
                                 300,                         -- Picture Taken
                                 :NEW.c504a_last_updated_by,
                                 NULL,
                                 SYSDATE,
                                 v_type,
                                 91107,
                                 v_loaner_turn_no
                                );
      v_status := 400;   
      ELSIF :NEW.c504a_status_fl = 0 
      THEN
         v_status := 350;                                       -- Available
      ELSIF :NEW.c504a_status_fl IN (5, 7, 10)
      THEN
         v_status := 400;                                        -- Allocated
      ELSIF :NEW.c504a_status_fl = 13
      THEN
         v_status := 420;					-- Packing in Progress
      ELSIF :NEW.c504a_status_fl = 16
      THEN
         v_status := 430;					-- Ready for Pickup
      ELSIF :NEW.c504a_status_fl IN (20, 24)
      THEN
         v_status := 450;                                          -- Shipped
      ELSIF :NEW.c504a_status_fl = 25
      THEN
         v_status := 100;                                         -- Accepted
      ELSIF :NEW.c504a_status_fl IN (30, 40)
      THEN
         v_status := 150;                                              -- WIP
      ELSIF :NEW.c504a_status_fl = 50
      THEN
         v_status := 200;                                          -- Checked
      ELSIF :NEW.c504a_status_fl = 55
      THEN
         v_status := 250;                                        -- Processed
      ELSIF :NEW.c504a_status_fl = 58
      THEN
         v_status := 255;   			-- Pending FG
      ELSIF :NEW.c504a_status_fl = 60
      THEN
         v_status := 750;                                      -- InActivated
      ELSIF :NEW.c504a_status_fl = 23                            
      THEN                                                        -- Deployed
         v_status := 800;
      ELSIF :NEW.c504a_status_fl = 22                            
      THEN                                                        -- Missed
         v_status := 850;  
      ELSIF :NEW.c504a_status_fl = 21                            
      THEN                                                        -- Disputed
         v_status := 900;           
     ELSIF :NEW.c504a_status_fl IS NOT NULL
      THEN
         raise_application_error
            ('-20999',
                'Need to Update trg_t504a_status_log_update.trg for the new status'
             || :NEW.c504a_status_fl
            );
      END IF;
      
     /* IF :NEW.c504a_status_fl = 24  --Pending Acceptance 
       THEN
         UPDATE T504A_CONSIGNMENT_LOANER T504A SET T504A.C504A_COMMISSION_FL ='Y' , T504A.C504A_STATUS_UPDATED_DATE = SYSDATE WHERE T504A.C504_CONSIGNMENT_ID = :NEW.c504_consignment_id  AND T504A.C504A_VOID_FL IS NULL;
      END IF;*/
      IF :NEW.c504a_status_fl = 25 --Pending check
        THEN
         IF :OLD.C5010_TAG_ID IS NULL
            THEN 
            BEGIN
            	SELECT c5010_tag_id INTO v_tag_id FROM t5010_tag WHERE c5010_last_updated_trans_id  = :NEW.C504_CONSIGNMENT_ID AND c5010_void_fl IS NULL AND rownum=1;
            EXCEPTION WHEN NO_DATA_FOUND THEN
            	v_tag_id:= NULL;
            END;
           END IF;
           
         INSERT INTO T504A_LOANER_STATUS_LOG(C504A_LOANER_STATUS_LOG_ID,C504_CONSIGNMENT_ID,C5010_TAG_ID,C504A_FROM_STATUS,C504A_FROM_STATUS_DATE,
          C504A_TO_STATUS,C504A_TO_STATUS_DATE,C504A_COMMISSION_FL,C504A_CREATED_BY,C504A_CREATED_DATE,C504A_VOID_FL,C1900_COMPANY_ID,C5040_PLANT_ID)
         VALUES(S504A_LOANER_STATUS_LOG.NEXTVAL,:NEW.C504_CONSIGNMENT_ID,v_tag_id,:OLD.c504a_status_fl,:OLD.C504A_STATUS_UPDATED_DATE,:NEW.c504a_status_fl,
          current_date,:OLD.C504A_COMMISSION_FL,:NEW.c504a_last_updated_by,current_date,NULL,v_company_id,v_plant_id);
      END IF;

      gm_save_status_details (:NEW.c504_consignment_id,
                              v_status,
                              :NEW.c504a_last_updated_by,
                              NULL,
                              SYSDATE,
                              v_type,
                              91107,
                              v_loaner_turn_no
                             );
   END IF;
   --The Below code is added for PMT-5969 when Consignment is Swaped to Loaner It needs to trigger an update to device.
   --Commented for this changes has been handled in PMT-12455 with checking available loaner sets 
  /* 
  IF INSERTING THEN   
	-- 103097 English, 103100 Product Catalog , 4000520 Ref Type
	v_set_id := get_set_id_from_cn(:NEW.c504_consignment_id);
      gm_pkg_op_set_master.gm_sav_set_attribute(v_set_id,'104740', 'Y', NULL,:NEW.C504A_LAST_UPDATED_BY);
      
     SELECT COUNT(t207.C207_SET_ID) INTO v_cnt
	   FROM t207_set_master t207, T2080_SET_COMPANY_MAPPING T2080
	  WHERE T207.c901_set_grp_type  = 1601
	    AND T207.c901_status_id     = '20367' --Approved status
	    AND T207.C207_SET_ID        = T2080.C207_SET_ID
	    AND T207.C207_SET_ID        = v_set_id
	    AND T2080.C2080_VOID_FL    IS NULL
	    AND T2080.C1900_COMPANY_ID = v_company_id
	    AND T207.C207_EXCLUDE_IPAD_FL IS NULL
	    AND t207.c207_void_fl      IS NULL;
      IF v_cnt > 0 THEN
      	gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (v_set_id, 4000736,  '103122', '103097', '103100',:NEW.C504A_LOANER_CREATE_BY, NULL,'Y',v_company_id) ;
      END IF;
    END IF;
    */
END trg_t504a_status_log_update;
/