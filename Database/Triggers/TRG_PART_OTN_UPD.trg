/* Formatted on 2013/07/03  */
--@"c:\database\triggers\TRG_PART_OTN_UPD.trg";

CREATE OR REPLACE TRIGGER TRG_PART_OTN_UPD
	BEFORE UPDATE OF C4023_QTY
	ON T4023_DEMAND_OTN_DETAIL
	FOR EACH ROW
DECLARE
	v_old		   T4023_DEMAND_OTN_DETAIL.C4023_QTY%TYPE;
	v_new		   T4023_DEMAND_OTN_DETAIL.C4023_QTY%TYPE;
BEGIN
--
	v_old		:= :OLD.C4023_QTY;
	v_new		:= :NEW.C4023_QTY;

--
	IF (NVL(v_old,-9999) <> NVL(v_new,-9999))
	THEN
		--
		IF (:OLD.C4023_HISTORY_FL IS NULL)
		THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.c4023_demand_otn_detail_id
													 , v_old
													 , 1053
													 , :OLD.C4023_LAST_UPDATED_BY
													 , :OLD.C4023_LAST_UPDATED_DATE
													  );
		END IF;

		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.c4023_demand_otn_detail_id
												 , v_new
												 , 1053
												 , :NEW.C4023_LAST_UPDATED_BY
												 , :NEW.C4023_LAST_UPDATED_DATE
												  );
		--
		:NEW.C4023_HISTORY_FL := 'Y';
--
	END IF;
--
END TRG_PART_OTN_UPD;
/