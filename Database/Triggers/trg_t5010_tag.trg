/* Formatted on 2012/02/09 18:05 (Formatter Plus v4.8.0) */
--@"C:\DATABASE\Triggers\trg_t5010_tag.trg";

CREATE OR REPLACE TRIGGER trg_t5010_tag
	BEFORE UPDATE
	ON t5010_tag
	FOR EACH ROW
DECLARE
	v_txntype	   t901_code_lookup.c901_code_id%TYPE;
	v_invtype	   t5010_tag.c901_inventory_type%TYPE;
	v_tag_inv_type t5010_tag.c901_inventory_type%TYPE;
BEGIN
--
	v_txntype	:= NULL;
	v_invtype	:= NULL;
	v_tag_inv_type := NULL;

	--Below code added	so that the transaction can be distinguished in one of the categories In-House Loaner/Consignment	/Scrap /Sales Consignment /Shelf /Product Loaner
	SELECT get_tag_inv_type (:NEW.c5010_last_updated_trans_id)
	  INTO v_txntype
	  FROM DUAL;

	SELECT get_rule_value (v_txntype, 'TAGINVTYPE')
	  INTO v_invtype
	  FROM DUAL;

--
--If new Type of Transaction is added and no rule value present.
	IF v_txntype IS NOT NULL AND v_invtype IS NULL THEN
		raise_application_error (-20231, 'Tag Inventory Type should not be NULL.');
	END IF;

	IF v_invtype IS NOT NULL THEN
		:NEW.c901_inventory_type := v_invtype;
	ELSIF	  v_invtype IS NULL
		  AND :NEW.c5010_last_updated_trans_id IS NULL
		  AND :NEW.c5010_location_id IS NOT NULL
		  AND :NEW.c901_location_type = 4120
		  AND :NEW.c5010_location_id <> 52098
		  AND :NEW.c5010_location_id <> 52099 THEN
		:NEW.c901_inventory_type := 10003;	 --Distributor Consignment
	ELSIF v_invtype IS NULL AND :NEW.c5010_last_updated_trans_id IS NOT NULL AND :NEW.c5010_location_id = 52098 THEN
		:NEW.c901_inventory_type := 10003;	 -- BuiltSet without Type
	ELSIF v_invtype IS NULL AND (:NEW.c901_status = 51010 OR :NEW.c901_status = 51011 OR :NEW.c901_status = 51013) THEN	
		:NEW.c901_inventory_type := NULL;	 -- status is released.
	END IF;

--
	IF (:OLD.c5010_history_fl IS NULL) THEN
		gm_pkg_ac_tag_info.gm_sav_tag_log (:OLD.c5010_tag_id
										 , :OLD.c5010_control_number
										 , :OLD.c205_part_number_id
										 , :OLD.c207_set_id
										 , :OLD.c5010_last_updated_trans_id
										 , :OLD.c901_trans_type
										 , :OLD.c5010_location_id
										 , :OLD.c901_location_type
										 , :OLD.c901_status
										 , :OLD.c5010_lock_fl
										 , :OLD.c5010_void_fl
										 , :OLD.c5010_created_by
										 , :OLD.c5010_created_date
										 , :OLD.c704_account_id
										 , :OLD.c901_sub_location_type
										 , :OLD.c5010_sub_location_id
										 , :OLD.c5010_sub_location_comments
										 , :OLD.c106_address_id
										 , :OLD.c703_sales_rep_id
										 , :OLD.c901_inventory_type
										 , :OLD.c5010_retag_id
										 , :OLD.c1900_company_id
										 , :OLD.c5040_plant_id
										 , :OLD.c5010_missing_since
										  );
		:NEW.c5010_history_fl := 'Y';
	END IF;

	gm_pkg_ac_tag_info.gm_sav_tag_log (:NEW.c5010_tag_id
									 , :NEW.c5010_control_number
									 , :NEW.c205_part_number_id
									 , :NEW.c207_set_id
									 , :NEW.c5010_last_updated_trans_id
									 , :NEW.c901_trans_type
									 , :NEW.c5010_location_id
									 , :NEW.c901_location_type
									 , :NEW.c901_status
									 , :NEW.c5010_lock_fl
									 , :NEW.c5010_void_fl
									 , :NEW.c5010_last_updated_by
									 , SYSDATE
									 , :NEW.c704_account_id
									 , :NEW.c901_sub_location_type
									 , :NEW.c5010_sub_location_id
									 , :NEW.c5010_sub_location_comments
									 , :NEW.c106_address_id
									 , :NEW.c703_sales_rep_id
									 , :NEW.c901_inventory_type
									 , :NEW.c5010_retag_id
									 , :NEW.c1900_company_id
									 , :NEW.c5040_plant_id
									 , :NEW.c5010_missing_since									 
									  );
--
END trg_t5010_tag;
/
