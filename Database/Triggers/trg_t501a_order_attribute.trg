/* Formatted on 2012/05/03 17:32 (Formatter Plus v4.8.0) */
CREATE OR REPLACE TRIGGER trg_t501a_order_attribute
	BEFORE UPDATE OF c501a_attribute_value
	ON t501a_order_attribute
	FOR EACH ROW
DECLARE
	v_old_attr_value t501a_order_attribute.c501a_attribute_value%TYPE;
	v_new_attr_value t501a_order_attribute.c501a_attribute_value%TYPE;
	v_old_attr_type t501a_order_attribute.c901_attribute_type%TYPE;
	v_trail_id		t941_audit_trail_log.c940_audit_trail_id%TYPE;
	v_old_attribute_id  VARCHAR2(50);
	v_new_attribute_id  VARCHAR2(50);
BEGIN
	v_old_attr_value := :OLD.c501a_attribute_value;
	v_new_attr_value := :NEW.c501a_attribute_value;
	v_old_attr_type := :OLD.c901_attribute_type;

	IF v_old_attr_type != 53060
	THEN
		IF (NVL (v_old_attr_value, 0) <> NVL (v_new_attr_value, 0))
		THEN
			IF (:OLD.c901_attribute_type = 53017 OR :OLD.c901_attribute_type = 53018)
			THEN
				--
				SELECT get_code_name (v_old_attr_value), get_code_name (v_new_attr_value)
				  INTO v_old_attr_value, v_new_attr_value
				  FROM DUAL;
			--
			END IF;

			-- 4000764: Sales BackOrder Email Time; 53028: Revenue Recognition attribute value updated
			IF(v_old_attr_type = 4000764)
			THEN
				v_trail_id := 4000764;
				v_old_attribute_id := :OLD.c501_order_id;
				v_new_attribute_id := :NEW.c501_order_id;
			ELSE
				v_trail_id := 53028;
				v_old_attribute_id := :OLD.c501a_order_attribute_id;
				v_new_attribute_id := :NEW.c501a_order_attribute_id;
			END IF;

			IF (:OLD.c501a_history_fl IS NULL)
			THEN
				gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (v_old_attribute_id
														 , v_old_attr_value
														 , v_trail_id
														 , :OLD.c501a_created_by
														 , :OLD.c501a_created_date
														  );
			END IF;

			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (v_new_attribute_id
													 , v_new_attr_value
													 , v_trail_id
													 , :NEW.c501a_last_updated_by
													 , :NEW.c501a_last_updated_date
													  );
			:NEW.c501a_history_fl := 'Y';
		END IF;
	END IF;
END trg_t501a_order_attribute;
/
