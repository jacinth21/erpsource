/* Formatted on 2010/03/02 11:36 (Formatter Plus v4.8.0) */
--@"C:\Database\Triggers\trg_t2656_update.trg";

CREATE OR REPLACE TRIGGER trg_t2656_update
	BEFORE UPDATE
	ON t2656_audit_entry
	FOR EACH ROW
DECLARE
	v_old_update_dt   t2656_audit_entry.c2656_last_updated_date%TYPE;
	v_new_update_dt	   t2656_audit_entry.c2656_last_updated_date%TYPE;
BEGIN
--
	v_old_update_dt		:= :OLD.c2656_last_updated_date;
	v_new_update_dt		:= :NEW.c2656_last_updated_date;

--
	IF (NVL(v_old_update_dt,SYSDATE-1) <> v_new_update_dt)
	THEN
	IF (:OLD.c2656_history_fl IS NULL)
	THEN
		gm_pkg_ac_tag_info.gm_ac_sav_audit_entry_log (:OLD.c2656_audit_entry_id
													, :OLD.c2651_field_sales_details_id
													, :OLD.c5010_tag_id
													, :OLD.c205_part_number_id
													, :OLD.c2656_control_number
													, :OLD.c207_set_id
													, :OLD.c2656_qty
													, :OLD.c901_status
													, :OLD.c2656_comments
													, :OLD.c901_location_type
													, :OLD.c2656_location_id
													, :OLD.c901_ref_type
													, :OLD.c2656_counted_by
													, :OLD.c2656_counted_date
													, :OLD.c2656_void_fl
													, :OLD.c2656_created_by
													, :OLD.c2656_created_date
													, :OLD.c903_upload_file_list
													, :OLD.c2656_ref_id
													, :OLD.c2656_ref_details
													, :OLD.c901_entry_type
													, :OLD.c901_reconciliation_status
													, :OLD.c2659_field_sales_ref_lock_id
													, :OLD.c901_posting_option
													, :OLD.c2656_posting_comment
													, :OLD.c5010_retag_id
													, :OLD.c2656_set_count_fl
													, :OLD.c2656_retag_fl
													, :OLD.c2656_part_count_fl
													, :OLD.c901_tag_status
													, :OLD.c106_address_id
													, :OLD.c2656_address_others
													, :OLD.c2656_flagged_by
													, :OLD.c2656_flagged_dt
													, :OLD.c2656_approved_by
													, :OLD.c2656_approved_dt
													, :OLD.c2656_reconciled_by
													, :OLD.c2656_reconciled_dt);
	END IF;

	gm_pkg_ac_tag_info.gm_ac_sav_audit_entry_log (:NEW.c2656_audit_entry_id
												, :NEW.c2651_field_sales_details_id
												, :NEW.c5010_tag_id
												, :NEW.c205_part_number_id
												, :NEW.c2656_control_number
												, :NEW.c207_set_id
												, :NEW.c2656_qty
												, :NEW.c901_status
												, :NEW.c2656_comments
												, :NEW.c901_location_type
												, :NEW.c2656_location_id
												, :NEW.c901_ref_type
												, :NEW.c2656_counted_by
												, :NEW.c2656_counted_date
												, :NEW.c2656_void_fl
												, :NEW.c2656_last_updated_by
												, SYSDATE
												, :NEW.c903_upload_file_list
												, :NEW.c2656_ref_id
												, :NEW.c2656_ref_details
												, :NEW.c901_entry_type
												, :NEW.c901_reconciliation_status
												, :NEW.c2659_field_sales_ref_lock_id
												, :NEW.c901_posting_option
												, :NEW.c2656_posting_comment
												, :NEW.c5010_retag_id
												, :NEW.c2656_set_count_fl
												, :NEW.c2656_retag_fl
												, :NEW.c2656_part_count_fl
												, :NEW.c901_tag_status
												, :NEW.c106_address_id
												, :NEW.c2656_address_others
												, :NEW.c2656_flagged_by
												, :NEW.c2656_flagged_dt
												, :NEW.c2656_approved_by
												, :NEW.c2656_approved_dt
												, :NEW.c2656_reconciled_by
												, :NEW.c2656_reconciled_dt
												 );
	--
	:NEW.c2656_history_fl := 'Y';
END IF;
END trg_t2656_update;
/
