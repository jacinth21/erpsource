CREATE OR REPLACE TRIGGER TRG_PATIENT_ANS_LIST_HIST_INS
AFTER INSERT ON T623_PATIENT_ANSWER_LIST
FOR EACH ROW
BEGIN
 --
 INSERT INTO T623A_PATIENT_ANSWER_HISTORY
 ( c623a_patient_ans_his_id
    ,c601_form_id
    ,c602_question_id
    ,c603_answer_grp_id
    ,c605_answer_list_id
    ,c904_answer_ds
    ,c622_patient_list_id
    ,c623_delete_fl
    ,c623_created_by
    ,c623_created_date
    ,c623_patient_ans_list_id
    ,c621_patient_id
    ,c901_status 
  )
 VALUES (
  s623a_patient_answer_history.NEXTVAL
    ,:new.c601_form_id
    ,:new.c602_question_id
    ,:new.c603_answer_grp_id
    ,:new.c605_answer_list_id
    ,:new.c904_answer_ds
    ,:new.c622_patient_list_id
    ,:new.c623_delete_fl
    ,:new.c623_created_by
    ,:new.c623_created_date
    ,:new.c623_patient_ans_list_id
    ,:new.c621_patient_id
    ,92470
 );
--
END TRG_PATIENT_ANS_LIST_HIST_INS;
/

