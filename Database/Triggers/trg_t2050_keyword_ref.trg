CREATE OR REPLACE TRIGGER trg_t2050_keyword_ref
BEFORE INSERT OR UPDATE ON t2050_keyword_ref
FOR EACH ROW
DECLARE
	v_old_keyword_ref_name t2050_keyword_ref.c2050_keyword_ref_name%TYPE;
    v_new_keyword_ref_name t2050_keyword_ref.c2050_keyword_ref_name%TYPE;
    v_old_void_fl t2050_keyword_ref.c2050_void_fl%TYPE;
    v_new_void_fl t2050_keyword_ref.c2050_void_fl%TYPE;
    v_system_id t207_set_master.c207_set_id%TYPE;
    v_action VARCHAR2 (20) ;
    BEGIN
        -- to get the old values.
        v_old_keyword_ref_name := NVL (:OLD.c2050_keyword_ref_name, '-999') ;
        v_new_keyword_ref_name := NVL (:NEW.c2050_keyword_ref_name, '-999') ;
        v_old_void_fl          := :OLD.c2050_void_fl;
        v_new_void_fl          := :NEW.c2050_void_fl;
        --
        --the trigger is enabled when do insert/update/void a keyword in t2050_keyword_ref table.
        IF INSERTING THEN-- whenever we are entering a new keyword in keyword details screen trigger is fired.
            v_action := '103122'; -- New
        ELSIF UPDATING THEN -- when we are updating existing keyword nmae with new keyword trigger is enabled.
            IF (v_old_keyword_ref_name <> v_new_keyword_ref_name) THEN
                v_action               := '103123'; -- Updated
                --1055 - audit trail id.whenever we are updating keyword , history need to be saved for older data and record should be fetched.
                gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.c2050_keyword_ref_id, v_old_keyword_ref_name, 1055, NVL
                (:OLD.c2050_last_updated_by, :OLD.c2050_created_by), NVL (:OLD.c2050_last_updated_date,
                :OLD.c2050_created_date)) ;
            END IF;
        END IF; -- end if updating
        --when we void a record trigger is enabled.
        IF (v_old_void_fl IS NULL AND v_new_void_fl IS NOT NULL) THEN
            v_action      := '4000412'; -- Voided
            -- When a File is Made active from Inactive, this condition will satisfy.
        ELSIF (v_old_void_fl IS NOT NULL AND v_new_void_fl IS NULL) THEN
            v_action         := '103123'; -- Updated
        END IF;
        --Ref type is file we need to get corresponding system id for file id and we are assigning to variable.
        IF (:NEW.c901_ref_type = 4000410) -- File
            THEN
            BEGIN
                 SELECT c903_ref_id
                   INTO v_system_id
                   FROM t903_upload_file_list
                  WHERE c903_upload_file_list = :NEW.c2050_ref_id--file ID.
                    AND C903_DELETE_FL       IS NULL;
            EXCEPTION
            WHEN OTHERS THEN
                RETURN;
            END;
        ELSE
            v_system_id := :NEW.c2050_ref_id;--if Ref Type is System we are assigning system id to local variable.
        END IF; -- end if c901_ref_type
        
        -- 103100 Product Catalog
        -- 4000410  Keyword Ref , 103097 English
        
            gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (:NEW.c2050_keyword_ref_id, '4000442', v_action, '103097',
            '103100', NVL (:NEW.c2050_last_updated_by, :NEW.c2050_created_by), v_system_id,NULL,1000) ;
        
    END;
    /
    
    