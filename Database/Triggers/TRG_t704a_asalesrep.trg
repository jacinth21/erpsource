--@"c:\database\triggers\TRG_t704a_asalesrep.trg";
CREATE OR REPLACE TRIGGER TRG_t704a_assocrep BEFORE
     INSERT OR
     UPDATE ON t704a_account_rep_mapping FOR EACH ROW 
     DECLARE 
    v_old_account_id t704a_account_rep_mapping.c704_account_id%TYPE;
    v_new_account_id t704a_account_rep_mapping.c704_account_id%TYPE;
    v_old_asrep_id t704a_account_rep_mapping.c703_sales_rep_id%TYPE;
    v_new_asrep_id t704a_account_rep_mapping.c703_sales_rep_id%TYPE;
    v_old_activefl t704a_account_rep_mapping.c704a_active_fl%TYPE;
    v_new_activefl t704a_account_rep_mapping.c704a_active_fl%TYPE;
    v_old_voidfl t704a_account_rep_mapping.c704a_void_fl%TYPE;
    v_new_voidfl t704a_account_rep_mapping.c704a_void_fl%TYPE;
    v_action VARCHAR2 (20) ;
    v_cmp_cnt  NUMBER;
    v_company_id  t704_account.c1900_company_id%TYPE;    
    BEGIN
        v_old_account_id := NVL (:OLD.c704_account_id, '-9999') ;
        v_new_account_id := NVL (:NEW.c704_account_id, '-9999') ;
        v_old_asrep_id   := NVL (:OLD.c703_sales_rep_id, '-9999') ;
        v_new_asrep_id   := NVL (:NEW.c703_sales_rep_id, '-9999') ;
        v_old_activefl   := :OLD.c704a_active_fl;
        v_new_activefl   := :NEW.c704a_active_fl;
        v_old_voidfl     := :OLD.c704a_void_fl;
        v_new_voidfl     := :NEW.c704a_void_fl;
        
       select t704.c1900_company_id
		into v_company_id
 		from  t704_account t704 
 		where t704.c704_account_id = v_new_account_id;
   		--and t704a.c704a_account_rep_map_id = :NEW.c704a_account_rep_map_id;
        
        IF INSERTING THEN
            v_action := '103122'; -- New
        ELSIF UPDATING THEN
            IF ( (v_old_account_id <> v_new_account_id) OR (v_old_asrep_id <> v_new_asrep_id) OR 
                 (NVL (v_old_activefl,'N') <> NVL (v_new_activefl, 'N')) OR (NVL (v_old_voidfl, 'N') <> NVL (v_new_voidfl, 'N')
                  AND NVL (v_new_voidfl, 'N') = 'N')) 
            THEN
                v_action                       := '103123'; -- Updated
            END IF;
        END IF;
		/* When a AccountGPOMap is voided, the If condition will satisfy.
         Below condition is for When a AccountGPOMap is 'newly voided/already voided' then voidfl is Y, but if it's not voided then voidfl is 'null' instead of 'N'
         so we are using NVL function here. 
        **/
        IF (NVL (v_old_voidfl, 'N') <> NVL (v_new_voidfl, 'N') AND NVL (v_new_voidfl, 'N') = 'Y') THEN
            v_action                := '4000412'; -- Voided
        END IF;
        
        --Check the sales rep is in GMNA company.
         SELECT COUNT (1) INTO v_cmp_cnt
		   FROM T703_SALES_REP
		  WHERE (c703_sales_rep_id = v_old_asrep_id  OR c703_sales_rep_id = v_new_asrep_id )
		    AND C703_VOID_FL     IS NULL
		    AND C1900_COMPANY_ID  = v_company_id;
        
        IF (v_action IS NOT NULL AND v_cmp_cnt > 0 ) THEN
            -- 103097 English, 103100 Product Catalog , 4000530 Ref Type
            gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (:NEW.C704A_ACCOUNT_REP_MAP_ID, '4000629', v_action, '103097'
            , '103100', NVL (:NEW.c704a_created_by, :NEW.c704a_last_updated_by), NULL, 'Y',v_company_id) ;
        END IF;
    END; 
   /