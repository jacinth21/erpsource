/*
 * Trigger will update the status updated date
 */
CREATE OR REPLACE TRIGGER trg_t504a_status_update
   BEFORE INSERT OR UPDATE
   ON T504A_CONSIGNMENT_LOANER    
   FOR EACH ROW
DECLARE
BEGIN
	IF :NEW.c504a_status_fl <> :OLD.c504a_status_fl
	 THEN
	 :NEW.C504A_STATUS_UPDATED_DATE :=current_date;
	END IF;
	
	IF :NEW.c504a_status_fl = 24  --Pending Acceptance
       THEN
         :NEW.C504A_COMMISSION_FL :='Y'; 
    END IF;

	
END trg_t504a_status_update;
/