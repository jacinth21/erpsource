--@"c:\database\triggers\trg_2080_set_company_mapping.trg";
CREATE OR REPLACE TRIGGER trg_2080_set_company_mapping
   BEFORE  
   	INSERT OR UPDATE ON t2080_set_company_mapping FOR EACH ROW 
   DECLARE
   	  v_action			   VARCHAR2(20);
   	  v_old_void_fl        	t2080_set_company_mapping.c2080_void_fl%TYPE;
   	  v_new_void_fl        	t2080_set_company_mapping.c2080_void_fl%TYPE;
      v_old_company_id     	t2080_set_company_mapping.c1900_company_id%TYPE;
      v_new_company_id     	t2080_set_company_mapping.c1900_company_id%TYPE;
      v_set_grp_type		t207_set_master.c901_set_grp_type%TYPE;
      v_system_id			t2080_set_company_mapping.C207_SET_ID%TYPE;
      v_ref_type			VARCHAR2 (20);
   BEGIN
        v_old_void_fl        := :OLD.c2080_void_fl;
        v_new_void_fl        := :NEW.c2080_void_fl;        
        v_old_company_id     := :OLD.c1900_company_id;
        v_new_company_id     := :NEW.c1900_company_id; 
        v_system_id		    := :NEW.c207_set_id; 
        
        IF INSERTING THEN
            v_action := '103122'; -- New
        ELSIF UPDATING THEN
        	v_action := '103123'; -- Updated
        END IF;
       
        -- PMT#37595/For get set group type by set id
        BEGIN   
        	SELECT C901_SET_GRP_TYPE 
        		INTO v_set_grp_type
        	FROM T207_SET_MASTER
        	WHERE C207_SET_ID = v_system_id
          		AND C207_VOID_FL IS NULL; 
     		EXCEPTION WHEN OTHERS THEN
  		   		RETURN;
  			END;
        
        -- 1600 - System type, 1601 - SET type
       	IF v_set_grp_type    = '1600' THEN
       		v_ref_type      := '103108'; 
       	ELSE
       		v_ref_type      := '103111'; 
        END IF;
            
        -- When a set is voided, the If condition will satisfy.
        IF (v_old_void_fl IS NULL AND v_new_void_fl IS NOT NULL) THEN
        	v_action      := '4000412'; -- Voided
        -- When a set is made active from voided, this condition will satisfy.
        ELSIF (v_old_void_fl IS NOT NULL AND v_new_void_fl IS NULL) THEN
        	v_action         := '103123'; -- Updated
        END IF;
            
        IF v_action IS NOT NULL THEN
            -- 103097 English, 103100 Product Catalog
        	gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (:NEW.c207_set_id, v_ref_type, v_action, '103097',
                                                  '103100',:NEW.c2080_last_updated_by
                                                  , NULL,'Y',v_new_company_id) ;
                                                  
            IF v_set_grp_type    = '1601' THEN
	            -- send update for SET part
	            gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (:NEW.c207_set_id, '4000409', v_action, '103097',
	                                                  '103100', :NEW.c2080_last_updated_by
	                                                  , NULL,'Y',v_new_company_id) ; 
				-- send update for SET Attr
	            gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (:NEW.c207_set_id, '4000736', v_action, '103097',
	                                                  '103100', :NEW.c2080_last_updated_by
	                                                  , NULL,'Y',v_new_company_id) ; 
	        END IF;
        END IF;
    END trg_2080_set_company_mapping;
    /
