CREATE OR REPLACE TRIGGER TRG_PATIENT_ANS_LIST_HIST_UPD
BEFORE UPDATE OF c605_answer_list_id, c904_answer_ds ON T623_PATIENT_ANSWER_LIST
FOR EACH ROW
DECLARE
 v_old t623_patient_answer_list.c904_answer_ds%TYPE;
 v_new t623_patient_answer_list.c904_answer_ds%TYPE;
BEGIN
--
v_old := :old.c904_answer_ds || '1'; -- Since checked is a key value we are appening 1
v_new := :new.c904_answer_ds || '1'; -- Since checked is a key value we are appening 1  to pass the value
--
IF (v_old <> v_new OR
 :old.c605_answer_list_id <> :new.c605_answer_list_id )
THEN
 --
 INSERT INTO T623A_PATIENT_ANSWER_HISTORY
 ( c623a_patient_ans_his_id
    ,c601_form_id
    ,c602_question_id
    ,c603_answer_grp_id
    ,c605_answer_list_id
    ,c904_answer_ds
    ,c622_patient_list_id
    ,c623_delete_fl
    ,c623_created_by
    ,c623_created_date
    ,c623_patient_ans_list_id
    ,c621_patient_id
    ,c901_status
  )
 VALUES (
  s623a_patient_answer_history.NEXTVAL
    ,:new.c601_form_id
    ,:new.c602_question_id
    ,:new.c603_answer_grp_id
    ,:new.c605_answer_list_id
    ,:new.c904_answer_ds
    ,:new.c622_patient_list_id
    ,:new.c623_delete_fl
    ,:new.c623_last_updated_by
    ,:new.c623_last_updated_date
    ,:new.c623_patient_ans_list_id
    ,:new.c621_patient_id
    , 92471
 );
 --
 :new.c623_history_flag := 'Y';
 --
 UPDATE T622_PATIENT_LIST
 SET  C622_VERIFY_FL = ''
 WHERE c622_patient_list_id = :new.c622_patient_list_id;
--
END IF;
--
END TRG_PATIENT_ANS_LIST_HIST_UPD;
/

