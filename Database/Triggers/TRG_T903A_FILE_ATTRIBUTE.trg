/* Formatted on 2011/06/08 09:49 (Formatter Plus v4.8.0) */
--@"C:\Database\Triggers\TRG_T903A_FILE_ATTRIBUTE.trg"
CREATE OR REPLACE TRIGGER TRG_T903A_FILE_ATTRIBUTE AFTER
  INSERT OR
  UPDATE ON T903A_FILE_ATTRIBUTE FOR EACH ROW 
  DECLARE 
  
    v_old_attribute_value 	t903a_file_attribute.c903a_attribute_value%TYPE;
    v_new_attribute_value 	t903a_file_attribute.c903a_attribute_value%TYPE;
    v_old_void_fl 			t903a_file_attribute.c903a_void_fl%TYPE;
    v_new_void_fl 			t903a_file_attribute.c903a_void_fl%TYPE;
    v_action 				VARCHAR2(20);
    v_system_id 			t207_set_master.c207_set_id%TYPE;
    v_ref_grp 				t903_upload_file_list.c901_ref_grp%TYPE;
    v_ref_id 				t903_upload_file_list.c903_ref_id%TYPE;
  BEGIN
	  
    v_old_attribute_value  := NVL(:OLD.c903a_attribute_value,'-9999');
    v_new_attribute_value  := NVL(:NEW.c903a_attribute_value,'-9999');
    v_old_void_fl          := NVL(:OLD.c903a_void_fl,'N');
    v_new_void_fl          := NVL(:NEW.c903a_void_fl,'N');
    
    IF (:NEW.c901_attribute_type in ('103216', '103217')) THEN  --'103216' - Title, '103217' - Shareable
	    IF INSERTING THEN
	      v_action  := '103122'; -- New
	    ELSIF UPDATING THEN
	      IF (v_old_attribute_value <> v_new_attribute_value AND v_old_void_fl <> v_new_void_fl ) THEN
	        v_action  := '103123'; -- Updated
	      END IF;
	    END IF;
	    
	    BEGIN
	      SELECT NVL(c901_ref_grp,'-9999'), c903_ref_id
	      INTO v_ref_grp, v_ref_id
	      FROM t903_upload_file_list
	      WHERE c903_upload_file_list = :NEW.c903_upload_file_list
	      AND c903_delete_fl         IS NULL;
	    EXCEPTION
	    WHEN NO_DATA_FOUND THEN
	      RETURN;
	    END;
	    
	    BEGIN
	      --Part Number
	      IF (v_ref_grp= '103090') THEN
	        v_system_id        := TRIM(get_set_id_from_part(v_ref_id));
	        --Group
	      ELSIF(v_ref_grp= '103094') THEN
	        SELECT c207_set_id
	        INTO v_system_id
	        FROM t4010_group
	        WHERE c4010_void_fl IS NULL
	        AND c4010_group_id   =v_ref_id;
	        --Set
	      ELSIF(v_ref_grp = '103091') THEN
	        SELECT c207_set_sales_system_id
	        INTO v_system_id
	        FROM t207_set_master
	        WHERE c207_set_id     =v_ref_id
	        AND c901_set_grp_type = '1601' -- Set
	        AND c207_void_fl     IS NULL;
	        --System
	      ELSIF(v_ref_grp = '103092') THEN
	        v_system_id          := v_ref_id;
	      END IF;
	    EXCEPTION
	    WHEN OTHERS THEN
	      RETURN;
	    END;
	    
	    IF(v_system_id IS NOT NULL) THEN
	      gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd(:NEW.c903_upload_file_list , '4000410' ,
	                                                     v_action ,'103097' ,'103100' ,
	                                                     NVL(:NEW.c903a_created_by,:NEW.c903a_last_updated_by) ,v_system_id,null );
	    END IF;
    END IF;
  END TRG_T903A_FILE_ATTRIBUTE;
  /