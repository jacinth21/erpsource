  CREATE OR REPLACE TRIGGER "GLOBUS_APP"."TRG_T202_UPDT" 
   BEFORE INSERT OR UPDATE OF C202_PROJECT_ID, C202_PROJECT_NM, C202_VOID_FL
   ON T202_PROJECT
   FOR EACH ROW
   DECLARE
  V_ACTION 				    VARCHAR2(20) ;
  BEGIN

	IF INSERTING THEN
        V_ACTION := '103122'; -- New
    ELSIF UPDATING THEN
  						IF ((:OLD.C202_PROJECT_ID <> :NEW.C202_PROJECT_ID) 
                OR (:OLD.C202_PROJECT_NM <> :NEW.C202_PROJECT_NM)
                OR (NVL(:OLD.C202_VOID_FL,'N') <> NVL(:new.C202_VOID_FL,'N') AND NVL(:new.C202_VOID_FL,'N') = 'N'))
                
  						THEN
  						 V_ACTION := '103123'; -- Updated
  						END IF;
      END IF;
 -- When activity is voided, the If condition will satisfy.
      IF (NVL(:OLD.C202_VOID_FL,'N') <> NVL(:new.C202_VOID_FL,'N') AND NVL(:new.C202_VOID_FL,'N') = 'Y')
      THEN
        V_ACTION  := '4000412'; -- Voided
      END IF;

      IF V_ACTION IS NOT NULL THEN
        -- 103097 English, 103100 Product Catalog , 103109 Ref Type*/
        GM_PKG_PDPC_PRODCATTXN.GM_SAV_MASTER_DATA_UPD (:new.C202_PROJECT_ID, '103109', V_ACTION, '103097', '103100', :NEW.C202_LAST_UPDATED_BY, NULL,'Y');
      END IF;
end;
/