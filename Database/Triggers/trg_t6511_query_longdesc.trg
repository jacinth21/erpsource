/* Formatted on 2010/04/11 20:30 (Formatter Plus v4.8.0) */
--@"C:\database\Triggers\trg_t6511_query_longdesc.trg";
CREATE OR REPLACE TRIGGER trg_t6511_query_longdesc
	AFTER INSERT
	ON t6511_query_longdesc
	FOR EACH ROW
BEGIN
	--
	INSERT INTO t623a_patient_answer_history
				(c623a_patient_ans_his_id, c601_form_id, c602_question_id, c603_answer_grp_id, c605_answer_list_id
			   , c904_answer_ds, c622_patient_list_id, c623_delete_fl, c623_created_by, c623_created_date
			   , c623_patient_ans_list_id, c621_patient_id, c6510_query_id, c6511_query_text, c901_status)
		(SELECT s623a_patient_answer_history.NEXTVAL, t6510.c601_form_id, t623.c602_question_id
			  , t623.c603_answer_grp_id, t623.c605_answer_list_id, t623.c904_answer_ds, t622.c622_patient_list_id
			  , t623.c623_delete_fl, :NEW.c6511_last_updated_by, SYSDATE, t623.c623_patient_ans_list_id
			  , t623.c621_patient_id, :NEW.c6510_query_id
			  , 'Query ID = ' || :NEW.c6510_query_id || ', ' || :NEW.c6511_query_text, :NEW.c901_status
		   FROM t6510_query t6510, t622_patient_list t622, t623_patient_answer_list t623
		  WHERE t6510.c622_patient_list_id = t622.c622_patient_list_id
			AND t6510.c623_patient_ans_list_id = t623.c623_patient_ans_list_id
			AND t6510.c6510_query_id = :NEW.c6510_query_id);
END trg_t6511_query_longdesc;
--
/
