/*
 * this trg is used when the loaner is extented, the data gets inserted   in t504F_loaner_transaction_log table whenever any insert or update happens in T504A_LOANER_TRANSACTION table
 */
CREATE OR REPLACE TRIGGER TRG_T504A_LOANER_TRANSACTION_LOG
   BEFORE UPDATE
   ON T504A_LOANER_TRANSACTION 
   FOR EACH ROW
DECLARE
v_old_expected_rt_dt       T504A_LOANER_TRANSACTION.C504A_EXPECTED_RETURN_DT%TYPE;
v_new_expected_rt_dt       T504A_LOANER_TRANSACTION.C504A_EXPECTED_RETURN_DT%TYPE;     
v_ext_fl                 T504A_LOANER_TRANSACTION.C504A_IS_LOANER_EXTENDED%TYPE;
v_status                 T504A_LOANER_TRANSACTION.C504A_STATUS_CHANGED_BY%TYPE;
v_old_surg_dt            T504A_LOANER_TRANSACTION.C504A_SURGERY_DATE%TYPE;
v_new_surg_dt            T504A_LOANER_TRANSACTION.C504A_SURGERY_DATE%TYPE;
v_old_expc_ret_dt        T504A_LOANER_TRANSACTION.C504A_EXPECTED_RETURN_DT%TYPE;
v_new_expc_ret_dt        T504A_LOANER_TRANSACTION.C504A_EXPECTED_RETURN_DT%TYPE;
v_comp_id                T504A_LOANER_TRANSACTION.C1900_COMPANY_ID%TYPE;

BEGIN
 
--
v_old_expected_rt_dt                      := :OLD.C504A_EXPECTED_RETURN_DT;
v_new_expected_rt_dt                      := :NEW.C504A_EXPECTED_RETURN_DT;
v_ext_fl                                := :NEW.C504A_IS_LOANER_EXTENDED;
v_status                                := :OLD.C504A_STATUS_CHANGED_BY;
v_old_surg_dt                           := :OLD.C504A_SURGERY_DATE;
v_new_surg_dt                           := :NEW.C504A_SURGERY_DATE;
v_old_expc_ret_dt                       := :OLD.C504A_EXPECTED_RETURN_DT;
v_new_expc_ret_dt                       := :NEW.C504A_EXPECTED_RETURN_DT;
v_comp_id                               := :NEW.C1900_COMPANY_ID;

--

IF  (v_old_expected_rt_dt <> v_new_expected_rt_dt AND v_status IS NULL) OR (v_old_surg_dt <> v_new_surg_dt  AND  v_comp_id != 1000)
                 
                THEN
                
         
                                
INSERT
  INTO T504F_LOANER_TRANSACTION_LOG
    (
        C504F_LOANER_TRANSACTION_LOG_ID,                      
                                C504A_LOANER_TRANSACTION_ID,                           
                                C504_CONSIGNMENT_ID,                                   
                                C504F_LOANER_DT,                                                  
                                C504F_EXPECTED_RETURN_DT,                                   
                                C504F_RETURN_DT,                                                  
                                C504F_VOID_FL,                                            
                                C901_CONSIGNED_TO,                                        
                                C504F_CONSIGNED_TO_ID,                                 
                                C504F_CREATED_BY,                                        
                                C504F_CREATED_DATE,                                            
                                C504F_LAST_UPDATED_DATE,                                    
                                C504F_LAST_UPDATED_BY,                                  
                                C703_SALES_REP_ID,                                             
                                C704_ACCOUNT_ID,                                          
                                C504F_IS_LOANER_EXTENDED,                                  
                                C504F_IS_REPLENISHED,                                      
                                C504F_PARENT_LOANER_TXN_ID,                            
                                C526_PRODUCT_REQUEST_DETAIL_ID,                                
                                C504F_PROCESSED_DATE,                                            
                                C504F_TRANSF_REF_ID,                                      
                                C504F_TRANSF_DATE,                                               
                                C504F_TRANSF_BY,                                        
                                C504F_TRANSF_EMAIL_FLG,                                        
                                C703_ASS_REP_ID,                                               
                                C5040_PLANT_ID,                                                
                                C504F_LAST_UPDATED_DATE_UTC,               
                                C1900_COMPANY_ID,                                           
                                C504F_APPROVE_DATE,                                             
                                C504F_EXT_REQUESTED_DATE,                                  
                                C504F_EXT_REQUIRED_DATE,                                       
                                C504F_EXT_APPR_REJ_DT,                                          
                                C901_STATUS,                                                 
                                C504F_EXTENSION_COMMENTS,                               
                                C504F_APPR_REJ_COMMENTS,                              
                                C504F_EXT_REQUESTED_BY,                                   
                                C504F_STATUS_CHANGED_BY,                                  
                                C504F_SURGERY_DATE,                                               
                                C504F_OLD_EXPECTED_RETDT,                                       
                                C504F_APPR_REJ_EMAIL_FL,                                     
                                C901_REASON_TYPE,                                            
                                C504F_ROLLBACK_FL
    )
    VALUES
    (
       s504F_loaner_transaction_log.nextval,
       :OLD.C504A_LOANER_TRANSACTION_ID,                                                      
                                :OLD.C504_CONSIGNMENT_ID,                                                                              
                                :OLD.C504A_LOANER_DT,                                                                                       
                                :OLD.C504A_EXPECTED_RETURN_DT,                                                                                 
                                :OLD.C504A_RETURN_DT,                                                                                       
                                :OLD.C504A_VOID_FL,                                                                               
                                :OLD.C901_CONSIGNED_TO,                                                                                   
                                :OLD.C504A_CONSIGNED_TO_ID,                                                                          
                                :OLD.C504A_CREATED_BY,                                                                       
                                :OLD.C504A_CREATED_DATE,                                                                                
                                :OLD.C504A_LAST_UPDATED_DATE,                                                                                    
                                :OLD.C504A_LAST_UPDATED_BY,                                                                          
                                :OLD.C703_SALES_REP_ID,                                                                                      
                                :OLD.C704_ACCOUNT_ID,                                                                                        
                                :OLD.C504A_IS_LOANER_EXTENDED,                                                                   
                                :OLD.C504A_IS_REPLENISHED,                                                                                
                                :OLD.C504A_PARENT_LOANER_TXN_ID,                                                              
                                :OLD.C526_PRODUCT_REQUEST_DETAIL_ID,                                                                     
                                :OLD.C504A_PROCESSED_DATE,                                                                            
                                :OLD.C504A_TRANSF_REF_ID,                                                                 
                                :OLD.C504A_TRANSF_DATE,                                                                                   
                                :OLD.C504A_TRANSF_BY,                                                                         
                                :OLD.C504A_TRANSF_EMAIL_FLG,                                                                        
                                :OLD.C703_ASS_REP_ID,                                                                                           
                                :OLD.C5040_PLANT_ID,                                                                                            
                                :OLD.C504A_LAST_UPDATED_DATE_UTC,                                                            
                                :OLD.C1900_COMPANY_ID,                                                                                     
                                :OLD.C504A_EXT_APPR_REJ_DT,                                                                                
                                :OLD.C504A_EXT_REQUESTED_DATE,                                                                                  
                                :OLD.C504A_EXT_REQUIRED_DATE,                                                                                     
                                :OLD.C504A_EXT_APPR_REJ_DT,                                                                           
                                :OLD.C901_STATUS,                                                                                                   
                                :OLD.C504A_EXTENSION_COMMENTS,                                                                
                                :OLD.C504A_APPR_REJ_COMMENTS,                                                                   
                                :OLD.C504A_EXT_REQUESTED_BY,                                                                        
                                :OLD.C504A_STATUS_CHANGED_BY,                                                                    
                                :OLD.C504A_SURGERY_DATE,                                                                                
                                :OLD.C504A_OLD_EXPECTED_RETDT,                                                                                  
                                :OLD.C504A_APPR_REJ_EMAIL_FL,                                                                       
                                :OLD.C901_REASON_TYPE,                                                                                      
                                :OLD.C504A_ROLLBACK_FL                                                        
    );
    
    
    
    
END IF;
END TRG_T504A_LOANER_TRANSACTION_LOG;
/


