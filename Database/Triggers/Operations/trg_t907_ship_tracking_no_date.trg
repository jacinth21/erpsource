/*
 * Trigger will update Tracking number Date UTC when first update in C907_TRACKING_NUMBER
 */
CREATE OR REPLACE TRIGGER trg_t907_ship_tracking_no_date
   BEFORE UPDATE OF C907_TRACKING_NUMBER
   ON t907_shipping_info    
   FOR EACH ROW
DECLARE
BEGIN
  
  	IF (:NEW.C907_TRACKING_NUMBER IS NOT NULL AND (NVL(:NEW.C907_TRACKING_NUMBER, '-9999') <> NVL(:OLD.C907_TRACKING_NUMBER, '-9999')))
		THEN
			:NEW.C907_TRACK_NUM_DT_UTC := SYSTIMESTAMP;
	END IF;
	
END trg_t907_ship_tracking_no_date;
/
