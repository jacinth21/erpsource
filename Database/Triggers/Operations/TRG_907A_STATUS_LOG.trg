/*to add records in T907A_STATUS_LOG table whenever any insert or update happens in T907_SHIPPING_INFO table*/
CREATE OR REPLACE TRIGGER TRG_907A_SHIPPING_STATUS_LOG AFTER
  INSERT OR
  UPDATE ON T907_SHIPPING_INFO 
  FOR EACH row DECLARE
  BEGIN

	  IF  NVL(:OLD.c907_status_fl,0) <> NVL(:NEW.c907_status_fl,0)
	  THEN
		  INSERT
		  INTO t907a_shipping_status_log
			(
			  c907_shipping_id,
			  c901_source,
			  c907a_ref_id,
			  c907a_status,
			  c907a_transacted_by,
			  c907a_transacted_date,
			  c907a_transacted_date_time,
			  c1900_company_id
			)
			VALUES
			(
			  :NEW.c907_shipping_id,
			  :NEW.c901_source,
			  :NEW.c907_ref_id,
			  NVL(:OLD.c907_status_fl,0),
			  NVL(:NEW.c907_last_updated_by,:NEW.c907_created_by),
			  TRUNC(:NEW.c907_last_updated_date),
			  :NEW.c907_last_updated_date,
			  :NEW.c1900_company_id
			);
		END IF;
END TRG_907A_SHIPPING_STATUS_LOG;
/
