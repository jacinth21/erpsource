/*
 * Trigger will last updated UTC everytime insert/update.
 */
CREATE OR REPLACE TRIGGER TRG_T413_UTC_DATE
   BEFORE UPDATE
   ON t413_inhouse_trans_items 
   FOR EACH ROW
DECLARE
begin
	:NEW.C413_LAST_UPDATED_DATE_UTC := SYSTIMESTAMP;
	
END TRG_T413_UTC_DATE;
/