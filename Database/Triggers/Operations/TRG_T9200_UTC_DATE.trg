/*
 * Trigger will last updated UTC everytime insert/update.
 */
CREATE OR REPLACE TRIGGER TRG_T9200_UTC_DATE
   BEFORE UPDATE
   ON t9200_incident 
   FOR EACH ROW
DECLARE
begin
	:NEW.C9200_UPDATED_DATE_UTC := SYSTIMESTAMP;
	
end TRG_T9200_UTC_DATE;
/