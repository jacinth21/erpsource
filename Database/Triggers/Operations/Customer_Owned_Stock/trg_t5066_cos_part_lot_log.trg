/* Formatted on 2020/08/24 14:22 (Formatter Plus v4.8.0) */
--@"c:\database\triggers\Customer_Owned_Stock\trg_t5066_cos_part_lot_log.trg"
CREATE OR REPLACE TRIGGER trg_t5066_cos_part_lot_log
	AFTER INSERT OR UPDATE OF C901_CURRENT_INVENTORY_TYPE
	ON t5066_cos_part_lot_qty 
	FOR EACH ROW
DECLARE
BEGIN

	
	gm_pkg_cos_lot_track.gm_pkg_cos_part_lot_qty_log (
														:NEW.C5066_COS_PART_LOT_QTY_ID
													   ,:NEW.C704_ACCOUNT_ID
													   ,:NEW.C205_PART_NUMBER_ID
													   ,:NEW.C2550_CONTROL_NUMBER
													   ,:NEW.C5066_LAST_UPDATED_TXN_ID
													   ,:NEW.C901_CURRENT_INVENTORY_TYPE
													   ,:NEW.C5066_LAST_UPDATED_BY
													   ,CURRENT_DATE
													 );

END trg_t5066_cos_part_lot_log;
/