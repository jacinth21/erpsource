/* Formatted on 2018/01/16 11:26 (Formatter Plus v4.8.0) */
-- @"c:\Database\Triggers\TRG_T504_WIP_Set_Loc_Update.trg"

/*
 * Trigger will save the log of the Locations at everytime of Consignment table UPDATE 
 */

CREATE OR REPLACE TRIGGER TRG_T504_WIP_Set_Loc_Update
	AFTER UPDATE OF C5052_LOCATION_ID
	ON T504_CONSIGNMENT
	FOR EACH ROW
DECLARE
	v_old_Location_id		   T504_CONSIGNMENT.C5052_LOCATION_ID%TYPE;
	v_new_Location_id		   T504_CONSIGNMENT.C5052_LOCATION_ID%TYPE;
BEGIN
--
	v_old_Location_id := :OLD.C5052_LOCATION_ID;
	v_new_Location_id := :NEW.C5052_LOCATION_ID;

--
IF UPDATING AND (NVL(v_old_Location_id,'-9999') <> NVL(v_new_Location_id,'-9999'))
	THEN
		gm_pkg_op_wip_set_loc.gm_sav_wip_set_loc_log 
		(
			:OLD.c504_consignment_id
			,v_old_Location_id
			,:OLD.C504_STATUS_FL
			,:OLD.C504_VERIFY_FL
			,:OLD.C504_TYPE
			,:NEW.C504_LAST_UPDATED_BY
		 );

	
END IF;

END TRG_T504_WIP_Set_Loc_Update;
/