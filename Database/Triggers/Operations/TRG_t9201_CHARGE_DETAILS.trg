--@"c:/Database/Triggers/Operations/TRG_t9201_CHARGE_DETAILS.trg";
CREATE OR REPLACE TRIGGER TRG_t9201_CHARGE_DETAILS
			
		BEFORE INSERT OR UPDATE OF C9201_COMMENT, C9201_AMOUNT ON T9201_CHARGE_DETAILS
		
	FOR EACH ROW
	
	DECLARE 
	
    --comment 
    v_charge_detail_id    t9201_charge_details.C9201_CHARGE_DETAILS_ID%TYPE;
    v_old_notes           t9201_charge_details.C9201_COMMENT%TYPE;
    v_new_notes           t9201_charge_details.C9201_COMMENT%TYPE;
    v_user_id             t9201_charge_details.C9201_CREATED_BY%TYPE;
              
    --amount
      v_incident_id         t9201_charge_details.C9200_INCIDENT_ID%TYPE;
   	  v_txn_amount          t9201_charge_details.C9201_AMOUNT%TYPE;
      v_open_amount         t9201_charge_details.C9201_AMOUNT%TYPE;
      v_closed_amount       t9201_charge_details.C9201_AMOUNT%TYPE;
      v_charged_by          t9201_charge_details.C9201_CREATED_BY%TYPE;
      v_charged_dt          t9201_charge_details.C9201_CHARGE_END_DATE%TYPE;
      v_updated_dt          t9201_charge_details.C9201_UPDATED_DATE%TYPE;

    BEGIN
    v_charge_detail_id    := :NEW.C9201_CHARGE_DETAILS_ID;
    v_old_notes           := :OLD.C9201_COMMENT;
    v_new_notes           := :NEW.C9201_COMMENT;
    v_user_id             := NVL(:NEW.C9201_UPDATED_BY,:NEW.C9201_CREATED_BY);
    v_charged_by		  := NVL(:NEW.C9201_UPDATED_BY,:NEW.C9201_CREATED_BY);
    v_charged_dt          := :NEW.C9201_CHARGE_END_DATE;
    
    v_txn_amount          :=  NVL(:NEW.c9201_amount,0) - NVL(:OLD.c9201_amount,0);
	v_open_amount         :=  NVL(:OLD.c9201_amount,'0'); 
	v_closed_amount       :=  NVL(:NEW.c9201_amount,'0');
    v_incident_id         :=  :NEW.c9200_incident_id;
	

    --110500 comment code id
    
    --1. if comment updated then call 
    
     IF NVL(v_old_notes,0) <> NVL(v_new_notes,0) THEN
            
         gm_pkg_op_loaner_charges_log.gm_sav_charge_history_log(v_charge_detail_id,110500,v_new_notes,v_user_id,CURRENT_DATE);
                
     END IF;
            
     --2.IF AMOUTN IS UPDATED THEN CALL
     -- if amount  updated then call
     IF NVL(v_open_amount,0) <> NVL(v_closed_amount,0) THEN
     
     	 gm_pkg_op_loaner_charges_log.gm_sav_charges_log(v_charge_detail_id,v_incident_id,v_txn_amount,v_open_amount,v_closed_amount,v_user_id,v_charged_by,v_charged_dt,CURRENT_DATE);
     			
     END IF;

    END TRG_t9201_CHARGE_DETAILS;
    /
   