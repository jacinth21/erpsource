/*
 * Trigger will last updated UTC everytime insert/update.
 */
CREATE OR REPLACE TRIGGER TRG_T5052_UTC_DATE
   BEFORE UPDATE
   ON t5052_location_master 
   FOR EACH ROW
DECLARE
begin
	:NEW.C5052_LAST_UPDATED_DATE_UTC := SYSTIMESTAMP;
	     
end TRG_T5052_UTC_DATE;
/