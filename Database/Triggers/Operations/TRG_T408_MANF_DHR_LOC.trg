/* Formatted on 2018/01/16 11:26 (Formatter Plus v4.8.0) */
-- @"c:\Database\Triggers\TRG_T408_MANF_DHR_LOC.trg"

/*
 * Trigger will save the log of the Locations at everytime of DHR table UPDATE 
 */

CREATE OR REPLACE TRIGGER TRG_T408_MANF_DHR_LOC
	AFTER UPDATE OF C5052_LOCATION_ID
	ON T408_DHR
	FOR EACH ROW
DECLARE
	v_old_Location_id		   T408_DHR.C5052_LOCATION_ID%TYPE;
	v_new_Location_id		   T408_DHR.C5052_LOCATION_ID%TYPE;
BEGIN
--
	v_old_Location_id := :OLD.C5052_LOCATION_ID;
	v_new_Location_id := :NEW.C5052_LOCATION_ID;

--
IF UPDATING AND (NVL(v_old_Location_id,'-9999') <> NVL(v_new_Location_id,'-9999'))
	THEN
		gm_pkg_op_dhr_loc.gm_sav_manf_dhr_loc_log 
		(
			:OLD.C408_DHR_ID
			,v_old_Location_id
			,:OLD.C408_LOCATION_SCAN_DT
			,:OLD.C408_CONTROL_NUMBER
			,:OLD.C402_WORK_ORDER_ID
			,:OLD.C205_PART_NUMBER_ID
			,:NEW.C408_LAST_UPDATED_BY
		 );

	
END IF;

END TRG_T408_MANF_DHR_LOC;
/