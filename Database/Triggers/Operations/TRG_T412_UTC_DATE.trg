/*
 * Trigger will last updated UTC everytime insert/update.
 */
CREATE OR REPLACE TRIGGER TRG_T412_UTC_DATE
   BEFORE UPDATE
   ON t412_inhouse_transactions 
   FOR EACH ROW
DECLARE
begin
	:NEW.C412_LAST_UPDATED_DATE_UTC := SYSTIMESTAMP;
	
end TRG_T412_UTC_DATE;
/