/*
 * Trigger will last updated UTC everytime insert/update.
 */
CREATE OR REPLACE TRIGGER TRG_T205C_UTC_DATE
   BEFORE UPDATE
   ON t205c_part_qty 
   FOR EACH ROW
DECLARE
begin
	:NEW.C205C_LAST_UPDATED_DATE_UTC := SYSTIMESTAMP;
	
end TRG_T205C_UTC_DATE;
/