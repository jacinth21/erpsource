/*
 * Trigger will last updated UTC everytime insert/update.
 */
CREATE OR REPLACE TRIGGER TRG_T905_UTC_DATE
   BEFORE INSERT OR UPDATE
   ON t905_status_details 
   FOR EACH ROW
DECLARE
BEGIN
	:NEW.c905_last_updated_date_utc := SYSTIMESTAMP;
	
END TRG_T905_UTC_DATE;
/