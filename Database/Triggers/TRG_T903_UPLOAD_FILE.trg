create or replace
TRIGGER "GLOBUS_APP"."TRG_T903_UPLOAD_FILE" 
	BEFORE INSERT OR UPDATE ON T903_UPLOAD_FILE_LIST
	FOR EACH ROW
DECLARE
	v_old_nm		T903_UPLOAD_FILE_LIST.C903_FILE_NAME%TYPE;
	v_new_nm		T903_UPLOAD_FILE_LIST.C903_FILE_NAME%TYPE;
	v_old_ref_typ	T903_UPLOAD_FILE_LIST.C901_REF_TYPE%TYPE;
	v_new_ref_typ	T903_UPLOAD_FILE_LIST.C901_REF_TYPE%TYPE;
	v_old_titl		T903_UPLOAD_FILE_LIST.C901_FILE_TITLE%TYPE;
	v_new_titl		T903_UPLOAD_FILE_LIST.C901_FILE_TITLE%TYPE;
	v_old_size		T903_UPLOAD_FILE_LIST.C903_FILE_SIZE%TYPE;
	v_new_size		T903_UPLOAD_FILE_LIST.C903_FILE_SIZE%TYPE;
	v_old_seq		T903_UPLOAD_FILE_LIST.C903_FILE_SEQ_NO%TYPE;
	v_new_seq		T903_UPLOAD_FILE_LIST.C903_FILE_SEQ_NO%TYPE;
	v_old_del_fl	T903_UPLOAD_FILE_LIST.C903_DELETE_FL%TYPE;
	v_new_del_fl	T903_UPLOAD_FILE_LIST.C903_DELETE_FL%TYPE;
	v_old_sub_type	T903_UPLOAD_FILE_LIST.C901_TYPE%TYPE;
	v_new_sub_type	T903_UPLOAD_FILE_LIST.C901_TYPE%TYPE;
	v_action		VARCHAR2(20);
	V_SYSTEM_ID		T207_SET_MASTER.C207_SET_ID%type;
  v_force_upd 		VARCHAR2(5);
  v_type_cnt 		NUMBER;
BEGIN

	v_old_nm		:= NVL(:OLD.C903_FILE_NAME,'-9999');
	v_new_nm		:= NVL(:NEW.C903_FILE_NAME,'-9999');
	v_old_ref_typ	:= NVL(:OLD.C901_REF_TYPE,'-9999');
	v_new_ref_typ	:= NVL(:NEW.C901_REF_TYPE,'-9999');
	v_old_titl		:= NVL(:OLD.C901_FILE_TITLE,'-9999');
	v_new_titl		:= NVL(:NEW.C901_FILE_TITLE,'-9999');
	v_old_size		:= NVL(:OLD.C903_FILE_SIZE,'-9999');
	v_new_size		:= NVL(:NEW.C903_FILE_SIZE,'-9999');
	v_old_seq 		:= NVL(:OLD.C903_FILE_SEQ_NO,'-9999');
	v_new_seq		:= NVL(:NEW.C903_FILE_SEQ_NO,'-9999');
	v_old_sub_type  := NVL(:OLD.C901_TYPE,'-9999');
	v_new_sub_type  := NVL(:NEW.C901_TYPE,'-9999');
	v_old_del_fl 	:= :OLD.C903_DELETE_FL;
	v_new_del_fl 	:= :NEW.C903_DELETE_FL;

	IF INSERTING THEN
		v_action := '103122';  -- New
	ELSIF UPDATING THEN

		IF ((v_old_nm <> v_new_nm) OR (v_old_ref_typ <> v_new_ref_typ) OR (v_old_titl <> v_new_titl)
		      OR (v_old_size <> v_new_size)OR (v_old_seq <> v_new_seq) OR (v_old_sub_type <>v_new_sub_type))
		THEN
			v_action := '103123';  -- Updated
		END IF;
	END IF;

	-- When a file is voided, the If condition will satisfy.
	IF (v_old_del_fl IS  NULL AND v_new_del_fl IS NOT NULL)
	THEN
		v_action := '4000412';  -- Voided

	-- When a File is Made active from Inactive, this condition will satisfy.
	ELSIF (v_old_del_fl IS NOT NULL AND v_new_del_fl IS  NULL)
	THEN
		v_action := '103123';  -- Updated
	END IF;

	BEGIN
  
    -- CRM File Upload
    IF ((:new.C901_REF_GRP = '103112') or (:new.C901_REF_GRP = '107993') or (:new.C901_REF_GRP = '107994'))
    THEN
      v_force_upd := 'Y';
    ELSE
      v_force_upd := null;
    END IF;
  
		--Part Number
			IF (:NEW.C901_REF_GRP = '103090')
			THEN
				v_system_id := TRIM(get_set_id_from_part(:NEW.C903_REF_ID));
			--Group
			ELSIF(:NEW.C901_REF_GRP = '103094')
			THEN
				SELECT C207_SET_ID INTO v_system_id
				FROM T4010_GROUP
				WHERE c4010_VOID_FL IS NULL
				AND   C4010_GROUP_ID=:NEW.C903_REF_ID;
			--Set
			ELSIF(:NEW.C901_REF_GRP = '103091')
			THEN
				SELECT C207_SET_SALES_SYSTEM_ID INTO v_system_id
				FROM T207_SET_MASTER
				WHERE C207_SET_ID=:NEW.C903_REF_ID
				AND C901_SET_GRP_TYPE = '1601' -- Set
				AND C207_VOID_FL IS NULL;
			--System
			ELSIF(:NEW.C901_REF_GRP = '103092')
			THEN
				v_system_id := :NEW.C903_REF_ID;
			--Project
			--To Handle it later

			--ELSIF(:NEW.C901_REF_GRP = '103093')
			END IF;
		EXCEPTION WHEN OTHERS
		THEN
			RETURN;
		END;
		
    SELECT COUNT(1) INTO v_type_cnt
    FROM t906_rules
    WHERE c906_rule_id   = 'FILE_TYPE'
    AND c906_rule_value  = TO_CHAR(:NEW.c901_ref_type)
    AND c906_rule_grp_id ='PROD_CAT_SYNC'
    AND c906_void_fl is null;
    
	--103100 Product Catalog
	-- 4000410  Images , 103097 English
	IF(v_system_id IS NOT NULL OR v_force_upd = 'Y' OR v_type_cnt >1) 
		THEN
			gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd(:NEW.C903_UPLOAD_FILE_LIST
													     , '4000410'
													     ,v_action
													     ,'103097'
													     ,'103100'
													     ,NVL(:new.C903_CREATED_BY,:new.C903_LAST_UPDATED_BY)
													     ,V_SYSTEM_ID
                               							 ,'Y'
														);
		END IF;

END;
/