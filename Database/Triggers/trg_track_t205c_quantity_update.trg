/* Formatted on 2010/08/04 14:29 (Formatter Plus v4.8.0) */
-- @"c:\database\triggers\trg_track_t205c_qty_update.trg"
CREATE OR REPLACE TRIGGER trg_track_t205c_qty_update
   BEFORE INSERT OR UPDATE OF c205_available_qty
   ON t205c_part_qty
   FOR EACH ROW
DECLARE
BEGIN	
     
   -- Negative qty validation
   IF (NVL (:NEW.c205_available_qty, 0) < 0)
   THEN
      raise_application_error
           ('-20500',
               'Requested quantity cannot exceed the available quantity for Part:'
            || :NEW.c205_part_number_id || ', Current Qty:' || :OLD.c205_available_qty || ', New Qty:' || :NEW.c205_available_qty  || ', Inventory Bucket:' || get_code_name(:NEW.c901_transaction_type) || ', Transaction:' || :NEW.c205_last_update_trans_id || ', Action:' || get_code_name(:NEW.c901_action) || ', Plant:' || NVL(get_plant_name(:NEW.c5040_plant_id),'Empty')
           );
   END IF;

   IF (UPDATING AND (:NEW.c205_last_updated_by IS NULL
       OR :NEW.c205_last_update_trans_id IS NULL
       OR :NEW.c901_action IS NULL
       OR :NEW.c901_type IS NULL)
      )
   THEN
      raise_application_error
         ('-20501',
          'Either of the following is not correctly entered : Trasaction id, Transaction type, Action taken'
         );
   END IF;

   --
   IF (UPDATING AND NVL(:OLD.c205_available_qty,0) != NVL(:NEW.c205_available_qty,0))
   THEN
      --
      INSERT INTO t214_transactions
                  (c214_transactions_id, c214_created_dt,
                   c214_created_by, c205_part_number_id,
                   c214_orig_qty, c214_id,
                   c214_trans_qty,
                   c214_new_qty, c901_action,
                   c901_type, c901_transaction_type, c5040_plant_id
                  )
           VALUES (s214_transactions.NEXTVAL, CURRENT_DATE,
                   :NEW.c205_last_updated_by, :NEW.c205_part_number_id,
                   NVL(:OLD.c205_available_qty,0), :NEW.c205_last_update_trans_id,
                   (NVL(:NEW.c205_available_qty,0) - NVL(:OLD.c205_available_qty,0)),
                   NVL(:NEW.c205_available_qty,0), :NEW.c901_action,
                   :NEW.c901_type, :NEW.c901_transaction_type, :NEW.c5040_plant_id
                  );
   --
   ELSIF (INSERTING AND NVL(:NEW.c205_available_qty,0) <> 0)
   THEN
      --
      INSERT INTO t214_transactions
                  (c214_transactions_id, c214_created_dt,
                   c214_created_by, c205_part_number_id, c214_orig_qty,
                   c214_id, c214_trans_qty,
                   c214_new_qty, c901_action,
                   c901_type, c901_transaction_type, c5040_plant_id
                  )
           VALUES (s214_transactions.NEXTVAL, CURRENT_DATE,
                   :NEW.c205_last_updated_by, :NEW.c205_part_number_id, 0,
                   :NEW.c205_last_update_trans_id, NVL(:NEW.c205_available_qty,0),
                   NVL(:NEW.c205_available_qty,0), :NEW.c901_action,
                   :NEW.c901_type, :NEW.c901_transaction_type, :NEW.c5040_plant_id
                  );
   --
   END IF;

--
   :NEW.c205_last_updated_by := NULL;
   :NEW.c205_last_update_trans_id := NULL;
   :NEW.c901_action := NULL;
   :NEW.c901_type := NULL;

-- PC-997: to update the transaction date (every time update/insert the record)

   :NEW.c205c_transaction_date := CURRENT_DATE;
   
END;
/
