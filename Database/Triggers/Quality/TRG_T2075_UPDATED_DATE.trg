CREATE OR REPLACE TRIGGER TRG_T2075_UPDATED_DATE 
BEFORE UPDATE ON T2075_SET_BUNDLE
FOR EACH ROW
BEGIN
	IF ( :NEW.C2075_Last_updated_by IS NULL  OR 
	     :NEW.C2075_Last_updated_date IS NULL )
    THEN
         raise_application_error ('-20501', 'Updated_Date and Updated_By should not be NULL.' );
	END IF;
 END TRG_T2075_UPDATED_DATE;
/