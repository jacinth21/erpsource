--PC-4400 To avoid to create duplicate part numbers
-- PC-3823 Issue on Update T205_PART_NUMBER Record 
/*
 * Trigger will avoid to create duplicate part numbers.
 */

			CREATE OR REPLACE TRIGGER TRG_T205_NAME_UPD
				BEFORE INSERT OR UPDATE ON T205_PART_NUMBER
				FOR EACH ROW

			BEGIN
			 IF INSERTING THEN
			 
				:NEW.C205_PART_NUMBER_ID := UPPER(:NEW.C205_PART_NUMBER_ID);
				
			 END IF;
			
			 IF UPDATING  AND (:OLD.C205_PART_NUMBER_ID != :NEW.C205_PART_NUMBER_ID) THEN
			 
                :NEW.C205_PART_NUMBER_ID := UPPER(:NEW.C205_PART_NUMBER_ID);
                
 			 END IF;

			END TRG_T205_NAME_UPD;
			/