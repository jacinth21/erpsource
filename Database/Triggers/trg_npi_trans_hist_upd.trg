/*
 * Trigger to add history for npi details
 */
CREATE OR REPLACE TRIGGER trg_npi_trans_hist_upd
	BEFORE INSERT OR UPDATE OF c6600_surgeon_npi_id, c6640_surgeon_name 
	ON t6640_npi_transaction
	FOR EACH ROW
DECLARE
	v_old_npi_id 		t6640_npi_transaction.c6600_surgeon_npi_id%TYPE;
  	v_new_npi_id 		t6640_npi_transaction.c6600_surgeon_npi_id%TYPE;
  	v_old_surgeon_nm 	t6640_npi_transaction.c6640_surgeon_name%TYPE;
  	v_new_surgeon_nm 	t6640_npi_transaction.c6640_surgeon_name%TYPE;
BEGIN
--

	v_old_npi_id     := :old.c6600_surgeon_npi_id;
    v_new_npi_id     := :new.c6600_surgeon_npi_id;
    v_old_surgeon_nm := :old.c6640_surgeon_name;
    v_new_surgeon_nm := :new.c6640_surgeon_name;

	-- If npi id or surgeon name is changed
	--IF ((v_old_npi_id IS NULL AND v_new_npi_id IS NOT NULL) OR (v_old_surgeon_nm IS NULL AND v_new_surgeon_nm IS NOT NULL ) OR (v_old_npi_id <> v_new_npi_id) OR (v_old_surgeon_nm <> v_new_surgeon_nm) ) THEN
	IF (NVL(v_old_npi_id,-999) <> NVL(v_new_npi_id,-999) OR NVL(v_old_surgeon_nm,-999) <> NVL(v_new_surgeon_nm,-999) ) THEN
      --
      INSERT
      INTO t6640a_npi_transaction_history
        (
          c6640a_npi_hist_id ,
          c6640a_ref_id ,
          c6640a_surgeon_npi_id ,
          c6640a_surgeon_name ,
          c6640a_created_by ,
          c6640a_created_date ,
          c6640a_reason
        )
        VALUES
        (
          s6640a_npi_transaction_history.NEXTVAL ,
          :new.c6640_ref_id ,
          :new.c6600_surgeon_npi_id ,
          :new.c6640_surgeon_name ,
          :new.c6640_last_updated_by ,
          :new.c6640_last_updated_date ,
          :new.c6640_reason
        );
    END IF;
--
END trg_npi_trans_hist_upd;
/
