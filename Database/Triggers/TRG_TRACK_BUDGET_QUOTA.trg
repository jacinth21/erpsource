/* Formatted on 2009/07/07 12:51 (Formatter Plus v4.8.0) */
-- @"C:\database\Triggers\trg_track_budget_quota.trg"
CREATE OR REPLACE TRIGGER trg_track_budget_quota
	AFTER INSERT OR UPDATE OF c732_qty
	ON t732_ai_quota
	FOR EACH ROW
DECLARE
BEGIN
	IF (UPDATING AND NVL (:OLD.c732_qty, 0) != NVL (:NEW.c732_qty, 0))
	THEN
		gm_pkg_sm_lad_ai_quota.gm_sm_sav_daily_txn (20211
												  , :NEW.c701_distributor_id
												  , :NEW.c207_set_id
												  , :NEW.c205_part_number_id
												  , :OLD.c732_qty
												  , :NEW.c732_qty
												  , :NEW.c732_price
												  , :NEW.c732_COGS_COST
												   );
		gm_pkg_sm_lad_ai_quota.gm_sm_sav_daily_txn (20212
												  , :NEW.c701_distributor_id
												  , :NEW.c207_set_id
												  , :NEW.c205_part_number_id
												  , :OLD.c732_qty
												  , :NEW.c732_qty
												  , :NEW.c732_price
												  , :NEW.c732_COGS_COST
												   );
	ELSIF (INSERTING AND NVL (:NEW.c732_qty, 0) <> 0)
	THEN
		gm_pkg_sm_lad_ai_quota.gm_sm_sav_daily_txn (20211
												  , :NEW.c701_distributor_id
												  , :NEW.c207_set_id
												  , :NEW.c205_part_number_id
												  , 0
												  , :NEW.c732_qty
												  , :NEW.c732_price
												  , :NEW.c732_COGS_COST
												   );
	END IF;
END trg_track_budget_quota;
/
