--@"c:\database\triggers\trg_4010_group.trg";
CREATE OR REPLACE TRIGGER trg_4010_group 
   BEFORE  
   	INSERT OR UPDATE ON t4010_group FOR EACH ROW 
   DECLARE
   
	v_old_nm 			t4010_group.c4010_group_nm%TYPE;
	v_new_nm 			t4010_group.c4010_group_nm%TYPE;
	v_old_desc 			t4010_group.c4010_group_desc%TYPE;
	v_new_desc 			t4010_group.c4010_group_desc%TYPE;
	v_old_dtl_desc 		t4010_group.c4010_group_dtl_desc%TYPE;
	v_new_dtl_desc 		t4010_group.c4010_group_dtl_desc%TYPE;
	v_old_set_id 		t4010_group.c207_set_id%TYPE;
	v_new_set_id 		t4010_group.c207_set_id%TYPE;
	v_old_grp_type 		t4010_group.c901_type%TYPE;
	v_new_grp_type 		t4010_group.c901_type%TYPE;
	v_old_void_fl 		t4010_group.c4010_void_fl%TYPE;
	v_new_void_fl 		t4010_group.c4010_void_fl%TYPE;
	v_action   			VARCHAR2 (20);
	v_publsh_cnt		NUMBER;
	v_old_publ_fl		T4010_GROUP.C4010_PUBLISH_FL%TYPE;
	v_new_publ_fl 		T4010_GROUP.C4010_PUBLISH_FL%TYPE;
	v_cur_sys_pub_pc 	NUMBER;
	v_new_sys_pub_pc 	NUMBER;
	v_force_upd  		VARCHAR2(1):='N';
	v_old_priced_type	t4010_group.C901_PRICED_TYPE%TYPE;  
	v_new_priced_type	t4010_group.C901_PRICED_TYPE%TYPE;
	v_old_inactive_fl 	t4010_group.c4010_inactive_fl%TYPE;
	v_new_inactive_fl 	t4010_group.c4010_inactive_fl%TYPE;
	v_company_id        t4010_group.C1900_COMPANY_ID%TYPE;
	v_old_company_id     t4010_group.C1900_COMPANY_ID%TYPE;
    v_new_company_id     t4010_group.C1900_COMPANY_ID%TYPE;
    BEGIN
        v_old_nm           := NVL(:OLD.c4010_group_nm,'-9999');
        v_new_nm           := NVL(:NEW.c4010_group_nm,'-9999');
        v_old_desc         := NVL(:OLD.c4010_group_desc,'-9999');
        v_new_desc         := NVL(:NEW.c4010_group_desc,'-9999');
        v_old_dtl_desc     := NVL(:OLD.c4010_group_dtl_desc,'-9999');
        v_new_dtl_desc     := NVL(:NEW.c4010_group_dtl_desc,'-9999');
        v_old_set_id       := NVL(:OLD.c207_set_id,'-9999');
        v_new_set_id       := NVL(:NEW.c207_set_id,'-9999');
        v_old_grp_type     := NVL(:OLD.C901_GROUP_TYPE,'-9999');
        v_new_grp_type     := NVL(:NEW.C901_GROUP_TYPE,'-9999');
        v_old_void_fl      := :OLD.c4010_void_fl;
        v_new_void_fl      := :NEW.c4010_void_fl;
        v_old_publ_fl      := NVL(:OLD.C4010_PUBLISH_FL,'N');
        v_new_publ_fl      := NVL(:NEW.C4010_PUBLISH_FL,'N');
        v_old_inactive_fl  := NVL(:OLD.c4010_inactive_fl,'N');
        v_new_inactive_fl  := NVL(:NEW.c4010_inactive_fl,'N');
        v_old_priced_type     := NVL(:OLD.C901_PRICED_TYPE,'-9999');
        v_new_priced_type     := NVL(:NEW.C901_PRICED_TYPE,'-9999');
        v_company_id	:= :NEW.C1900_COMPANY_ID;
        v_old_company_id     := :OLD.C1900_COMPANY_ID;
        v_new_company_id     := :NEW.C1900_COMPANY_ID;
        
        
        --Check if Group is published/unpulished to product catalog
        
       IF v_company_id IS NULL THEN
      		 v_company_id := v_old_company_id  ;
       ELSE
      		v_company_id :=  get_rule_value('DEFAULT_COMPANY', 'ONEPORTAL')  ;
       END IF; 
        
        
        IF INSERTING THEN
            v_action := '103122'; -- New
        ELSIF UPDATING THEN
            IF ( (v_old_nm <> v_new_nm) OR (v_old_desc <> v_new_desc) OR (v_old_dtl_desc <> v_new_dtl_desc) 
            	OR ( v_old_set_id <> v_new_set_id) OR (v_old_grp_type <> v_new_grp_type)
            	OR (v_old_publ_fl <> v_new_publ_fl) OR (v_old_priced_type <> v_new_priced_type)
            	) THEN
                v_action        := '103123'; -- Updated
            END IF;
        END IF;
        
        -- When a Group is voided / unpublished , the If condition will satisfy.
        IF (v_old_void_fl IS NULL AND v_new_void_fl IS NOT NULL
        	OR (v_old_publ_fl = 'Y' AND v_new_publ_fl ='N')
        	OR (v_old_inactive_fl = 'N' AND v_new_inactive_fl ='Y')) THEN
        	
            v_action      := '4000412'; -- Voided
            -- When a Group is made active from voided, this condition will satisfy.
        ELSIF (v_old_void_fl IS NOT NULL AND v_new_void_fl IS NULL
        OR (v_old_publ_fl = 'N' AND v_new_publ_fl ='Y')
        OR (v_old_inactive_fl = 'Y' AND v_new_inactive_fl ='N')) THEN
            v_action         := '103123'; -- Updated
        END IF;
        
        --	Group sync is not checking system publish to PC.
		--	So,group update no need to check Group�s system published to PC.

        /*
        	SELECT COUNT(1) into v_cur_sys_pub_pc
           FROM t207_set_master T207, t207c_set_attribute t207c
          WHERE T207.c207_set_id       = t207c.c207_set_id
            AND T207.c207_set_id = v_old_set_id
            AND T207.c901_set_grp_type = '1600' --System Report Grp
            AND T207.c207_void_fl     IS NULL
            AND t207c.c207c_void_fl   IS NULL
            AND c901_attribute_type    = '103119' --Publish to
            AND c207c_attribute_value  = '103085'; --Product Catalog

	        SELECT COUNT(1) into v_new_sys_pub_pc
	       FROM t207_set_master T207, t207c_set_attribute t207c
	      WHERE T207.c207_set_id       = t207c.c207_set_id
	        AND T207.c207_set_id = v_new_set_id
	        AND T207.c901_set_grp_type = '1600' --System Report Grp
	        AND T207.c207_void_fl     IS NULL
	        AND t207c.c207c_void_fl   IS NULL
	        AND c901_attribute_type    = '103119' --Publish to
	        AND c207c_attribute_value  = '103085'; --Product Cat
	        
	         -- If the Curent System is released for PC and the new System is not , then the part should be removed from Product Catl.
	        IF (v_cur_sys_pub_pc >0 AND v_new_sys_pub_pc =0 )
	        THEN
	        	v_force_upd := 'Y';
	        	v_action := '4000412 '; --Void
	        END IF;
	      */  	
        -- Check if any of these column data is modified. If it is, then we should treat this as an update to Group
        IF (v_action IS NOT NULL OR (v_old_set_id <> v_new_set_id) ) 
        AND (v_old_publ_fl = 'Y' OR v_new_publ_fl ='Y')
        AND (v_old_inactive_fl = 'N' OR v_new_inactive_fl ='N')
        AND :NEW.c901_type = '40045' THEN --40045 -Forecast/Demand Group
            -- 103110 Group ref type, 103097 English, 103100 Product Catalog
            gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (:NEW.c4010_group_id,'103110', v_action, '103097',
                                              '103100', NVL (:NEW.c4010_created_by, :NEW.c4010_last_updated_by)
					      ,:NEW.c207_set_id,v_force_upd,v_company_id) ;
					      
			IF v_old_publ_fl = 'N' AND v_new_publ_fl ='Y' THEN
			-- Send update for Group part when group published
			gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (:NEW.c4010_group_id,'4000408', v_action, '103097',
                                              '103100', NVL (:NEW.c4010_created_by, :NEW.c4010_last_updated_by)
					      ,:NEW.c207_set_id,v_force_upd,v_company_id) ;
			END IF;
        END IF;
        
       
    END trg_4010_group;
    /