CREATE OR REPLACE
TRIGGER trg_t2023_part_company_mapping
   BEFORE INSERT OR UPDATE
   ON t2023_part_company_mapping
   FOR EACH ROW
DECLARE 
BEGIN
	-- This trigger is used to enable or disable lot tracking for BBA parts whenever part is released or unreleased or created 
	--105360	Created
	--10304547	UnReleased
	--105361	Released
    IF (INSERTING OR  UPDATING) AND :NEW.c1900_company_id = '1001'
    THEN
		   UPDATE t205d_part_attribute
		       SET c205d_attribute_value = DECODE(:NEW.c901_part_mapping_type,'10304547','N','Y') 
		       , c205d_last_updated_date = CURRENT_DATE
		       , c205d_last_updated_by = :NEW.c2023_updated_by
		     WHERE c205_part_number_id = :NEW.c205_part_number_id AND c901_attribute_type =  104480 
		       AND c1900_company_id = 1001
		       AND c205d_void_fl IS NULL;  
  
	    IF (SQL%ROWCOUNT = 0)
	    THEN	      
	  	   INSERT
               INTO t205d_part_attribute
                (
                    c2051_part_attribute_id, c205d_attribute_value, c205d_void_fl
                  , c205d_created_by, c205d_created_date, c205_part_number_id
                  , c901_attribute_type, c1900_company_id
                )
                VALUES
                (
                    S205D_PART_ATTRIBUTE.nextval, 'Y', NULL
                  , :NEW.c2023_updated_by, CURRENT_DATE, :NEW.c205_part_number_id
                  , 104480, :NEW.c1900_company_id
                ) ;	      
	    END IF;
  	END IF;
END trg_t2550_part_control_number;
/
 