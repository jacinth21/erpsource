/* Formatted on 2010/07/22 11:13 (Formatter Plus v4.8.0) */
-- @"c:\Database\Triggers\TRG_T704_CREDIT_RATING_UPDATE.trg"
CREATE OR REPLACE TRIGGER TRG_T704_CREDIT_RATING_UPDATE
	BEFORE UPDATE OF c704_credit_rating
	ON T704_ACCOUNT
	FOR EACH ROW
DECLARE
	v_old		   T704_ACCOUNT.C704_credit_rating%TYPE;
	v_new		   T704_ACCOUNT.C704_credit_rating%TYPE;
BEGIN
	v_old		:= :OLD.C704_credit_rating;
	v_new		:= :NEW.C704_credit_rating;


		IF (NVL(:NEW.C704_credit_rating,'0') != NVL(:OLD.C704_credit_rating,'0') )
		THEN
		
		
		IF (:OLD.c704_rating_history_fl IS NULL)
		THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.C704_ACCOUNT_ID
													 , v_old
													 , 53029
													 , :OLD.c704_created_by
													 , :OLD.c704_created_date
													  );
		END IF;

		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.C704_ACCOUNT_ID
												 , v_new
												 , 53029
												 , :NEW.c704_last_updated_by
												 , :NEW.c704_last_updated_date
												  );
		:NEW.c704_rating_history_fl := 'Y';
		:NEW.C704_credit_rating_date := SYSDATE;
    
    	END IF;
	
END TRG_T704_CREDIT_RATING_UPDATE;
/