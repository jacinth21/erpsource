--@"c:\Database\Triggers\trg_t412_status_log_update.trg"
	CREATE OR REPLACE TRIGGER trg_t412_status_log_update
	   AFTER INSERT OR UPDATE
	   ON t412_inhouse_transactions
	   FOR EACH ROW
	DECLARE
		v_status		t901_code_lookup.c901_code_id%TYPE;
	    v_userid 		t412_inhouse_transactions.c412_last_updated_by%TYPE;
	    v_status_log_fl t906_rules.c906_rule_value%TYPE;
	    v_avoid_status  t906_rules.c906_rule_value%TYPE;
    BEGIN
	    /*Get the status log flag for the transaction type*/
        v_status_log_fl        := get_rule_value ('STATUS_LOG', :NEW.c412_type);
        
        IF NVL(:OLD.c412_status_fl,'N') <> NVL(:NEW.c412_status_fl,'N') AND NVL (v_status_log_fl, 'N') = 'Y' THEN
        	 /* Get status log code id from rule table based on txn type and txn status.
        	  * Because,some txns may have exception status.The exceptional status txns are defined in the rule table.
        	  * Other txns will get the below default status logs.
        	  * Get the code id for the appropriate status
        	  * 93004	Initiated
        	  * 93010	Controlled
        	  * 93006	Completed */
             SELECT NVL(get_rule_value (:NEW.c412_status_fl, :NEW.c412_type),DECODE (:NEW.c412_status_fl, '2', 93004, '3', 93111, '4', 93006,NULL))
                  , NVL (:NEW.c412_last_updated_by, :NEW.c412_created_by)
                  , NVL(get_rule_value ('AVOID'||:NEW.c412_status_fl, :NEW.c412_type),'N')
               INTO v_status, v_userid, v_avoid_status
               FROM DUAL;
               
            /* ISQN and ISIA txns are created with the status of 3 and then update to 2.
 			*  avoid to store status log while inserting these txns with the status of 3.*/   
            IF (v_status IS NOT NULL) AND (NOT(INSERTING AND v_avoid_status = 'Y' ))  THEN
                gm_save_status_details (:NEW.c412_inhouse_trans_id, '', v_userid, NULL, SYSDATE, v_status, 91106) ; --91106	Allocation status log type
            END IF;
        END IF;
    END trg_t412_status_log_update;
/    