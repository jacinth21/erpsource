/* Formatted on 2011/06/08 09:49 (Formatter Plus v4.8.0) */
--@"C:\Database\Triggers\TRG_T1520_FUNCTION_LIST.trg"
CREATE OR REPLACE TRIGGER TRG_T1520_FUNCTION_LIST
   AFTER INSERT OR UPDATE
   ON T1520_FUNCTION_LIST 
   FOR EACH ROW
DECLARE
  BEGIN
		INSERT INTO T1520_FUNCTION_LOG(
			C1520L_LOG_ID,
			C1520L_FUNCTION_ID,
			C1520L_TYPE,
			C1520L_REQUEST_URI,
			C1520L_REQUEST_STROPT,
			C1520L_DEPARTMENT_ID,
			C1520L_CREATED_BY,
			C1520L_CREATED_DATE			
		) VALUES (
			S1520L_SEQ_NO.nextval,
			:NEW.C1520_FUNCTION_ID,
			:NEW.C901_TYPE,
			:NEW.C1520_REQUEST_URI,
			:NEW.C1520_REQUEST_STROPT,
			:NEW.C901_DEPARTMENT_ID,
			:NEW.C1520_CREATED_BY,
			SYSDATE
		);
END TRG_T1520_FUNCTION_LIST;
/