/* Formatted on 2008/08/21 17:27 (Formatter Plus v4.8.0) */
-- @"c:\Database\Trigger\trg_growth_detail_upd.trg"

CREATE OR REPLACE TRIGGER trg_required_date_upd
	BEFORE UPDATE OF c520_required_date
	ON t520_request
	FOR EACH ROW
DECLARE
	v_old		   t520_request.c520_required_date%TYPE;
	v_new		   t520_request.c520_required_date%TYPE;
BEGIN
--
	v_old		:= :OLD.c520_required_date;
	v_new		:= :NEW.c520_required_date;

--
	IF (v_old <> v_new)
	THEN
		--
		IF (:OLD.c520_req_dt_history_fl IS NULL)
		THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.c520_request_id
													 , TO_CHAR (v_old, 'MM/DD/YYYY')
													 , 1006
													 , :OLD.c520_created_by
													 , :OLD.c520_created_date
													  );
		END IF;

		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.c520_request_id
												 , TO_CHAR (v_new, 'MM/DD/YYYY')
												 , 1006
												 , :NEW.c520_last_updated_by
												 , :NEW.c520_last_updated_date
												  );
		--
		:NEW.c520_req_dt_history_fl := 'Y';
--
	END IF;
--
END trg_required_date_upd;
/
