/* Formatted on 2007/08/30 11:26 (Formatter Plus v4.8.0) */
-- @"c:\Database\Trigger\trg_ttp_part_detail_upd.trg"

CREATE OR REPLACE TRIGGER trg_ttp_part_detail_upd
	BEFORE UPDATE OF c4053_qty
	ON t4053_ttp_part_detail
	FOR EACH ROW
DECLARE
	v_old		   t4053_ttp_part_detail.c4053_qty%TYPE;
	v_new		   t4053_ttp_part_detail.c4053_qty%TYPE;
BEGIN
--
	v_old		:= :OLD.c4053_qty;
	v_new		:= :NEW.c4053_qty;

--
	IF (v_old <> v_new)
	THEN
		--
		IF (:OLD.c4053_history_fl IS NULL)
		THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.c4053_ttp_part_id
													 , v_old
													 , 1003
													 , :OLD.c4053_created_by
													 , :OLD.c4053_created_date
													  );
		END IF;

		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.c4053_ttp_part_id
												 , v_new
												 , 1003
												 , :NEW.c4053_last_updated_by
												 , :NEW.c4053_last_updated_date
												  );
		--
		:NEW.c4053_history_fl := 'Y';
--
	END IF;
--
END trg_ttp_part_detail_upd;
/
