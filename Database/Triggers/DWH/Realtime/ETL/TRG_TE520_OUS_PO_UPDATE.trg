-- C:\Projects\Branches\PMT\db\ETL\Triggers\TRG_TE520_OUS_PO_UPDATE.trg
create or replace
TRIGGER GLOBUS_ETL.TRG_TE520_OUS_PO_UPDATE
   BEFORE  INSERT
   ON GLOBUS_ETL.TE520_OUS_RESTOCK_PO_REQUEST
   FOR EACH ROW
/*****************************************************************************************
  When OUS order contains ATEC part, creating CN should have distributor as Atec distributor. As a quick solution, 
  this trigger will update the US Distributor (689) as ATEC distributor (1899)
************************************************************************************************/
BEGIN

  IF (:NEW.C1900_PART_COMPANY_ID =1018)
   THEN
     SELECT '1899' INTO :NEW.C520_REQUEST_TO FROM DUAL;
     SELECT '1899' INTO :NEW.C520_SHIP_TO_ID FROM DUAL;
   END IF;

end;
