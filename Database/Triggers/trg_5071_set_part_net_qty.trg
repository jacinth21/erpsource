/* Formatted on 2011/04/20 14:22 (Formatter Plus v4.8.0) */
--@"c:\database\triggers\trg_5053_location_part_mapping.trg"
CREATE OR REPLACE TRIGGER trg_5071_SET_PART_QTY
	BEFORE INSERT OR UPDATE OF c5071_curr_qty
	ON t5071_SET_PART_QTY
	FOR EACH ROW
DECLARE
    v_count        NUMBER;
	v_set_part_qty_id    t5071_SET_PART_QTY.c5070_set_lot_master_id%TYPE;
	V_TRANS_TYPE T5070_SET_LOT_MASTER.C901_TXN_TYPE%TYPE;


BEGIN
IF (	UPDATING
		AND (	:NEW.c5053_last_updated_by IS NULL
			 OR :NEW.c5071_last_updated_txn_id IS NULL
			 		 
			)
	   )
	THEN
		raise_application_error
			('-20501'
		   , 'Either of the following is not correctly entered : Transaction by, Trasaction id,' 
			);
	END IF;


	IF NVL (:OLD.c5071_curr_qty, 0) > 0 AND :NEW.c5071_curr_qty IS NULL
	THEN
		raise_application_error ('-20500'
							   ,	'Inventory Location Quantity Cannot be empty for Part: '
								 || :NEW.c205_part_number_id
								 || ' and Transaction: '
								 || :NEW.c5071_last_updated_txn_id
								);
	END IF;	



  
	IF (UPDATING AND NVL (:OLD.c5071_curr_qty, 0) != NVL (:NEW.c5071_curr_qty, 0))
	THEN
	gm_pkg_set_lot_track.gm_sav_set_part_qty_log (	
														  :NEW.C5071_SET_PART_QTY_ID
  							     			            , :NEW.c5070_set_lot_master_id
														, :NEW.C205_PART_NUMBER_ID
														, :NEW.C5071_LAST_UPDATED_TXN_ID
													    , :OLD.C5071_CURR_QTY
														, (:NEW.C5071_CURR_QTY-:OLD.C5071_CURR_QTY)
   													    , :NEW.C5071_CURR_QTY
														, :NEW.C5053_LAST_TXN_CONTROL_NUMBER
														, CURRENT_DATE
														, :NEW.c5053_last_updated_by
														, :NEW.c901_txn_type
														, :NEW.c901_type
														 );
	
														 
	
	gm_pkg_set_lot_track.gm_sav_set_part_lot_qty (
	  													   :NEW.C5071_SET_PART_QTY_ID
  							     				         , :NEW.C5070_SET_LOT_MASTER_ID
														 , :NEW.C205_PART_NUMBER_ID
														 , (:NEW.C5071_CURR_QTY-:OLD.C5071_CURR_QTY)
														 , :NEW.C5071_LAST_UPDATED_TXN_ID
													     , :NEW.C5053_LAST_TXN_CONTROL_NUMBER
													     , CURRENT_DATE
														 , :NEW.c5053_last_updated_by
														 , :NEW.c901_txn_type
														 , :NEW.c901_type
													     );
 								 
														 

	ELSIF (INSERTING AND NVL (:NEW.c5071_curr_qty, 0) <> 0)
	THEN
		IF :NEW.c5071_last_updated_txn_id IS NULL 
		THEN
        -- Need to validate this
			raise_application_error
				('-20501'
		   			, 'The following is not correctly entered : Trasaction id'
				);
		END IF;
	gm_pkg_set_lot_track.gm_sav_set_part_qty_log (
	  												      :NEW.C5071_SET_PART_QTY_ID
  							     				        , :NEW.C5070_SET_LOT_MASTER_ID
														, :NEW.C205_PART_NUMBER_ID
														, :NEW.C5071_LAST_UPDATED_TXN_ID
													    ,  0
   													    , :NEW.C5071_CURR_QTY
														, :NEW.C5071_CURR_QTY
													    , :NEW.C5053_LAST_TXN_CONTROL_NUMBER
													    ,  CURRENT_DATE
														, :NEW.c5053_last_updated_by
														, :NEW.c901_txn_type
														, :NEW.c901_type
														 );
	gm_pkg_set_lot_track.GM_SAV_SET_PART_LOT_QTY (
	  												      :NEW.C5071_SET_PART_QTY_ID
  							     				        , :NEW.C5070_SET_LOT_MASTER_ID
														, :NEW.C205_PART_NUMBER_ID
														, :NEW.C5071_CURR_QTY
													    , :NEW.C5071_LAST_UPDATED_TXN_ID
													    , :NEW.C5053_LAST_TXN_CONTROL_NUMBER
													    , CURRENT_DATE
														, :NEW.c5053_LAST_UPDATED_BY
														, :NEW.c901_txn_type
														, :NEW.c901_type
													    );
														 							 
														 
														 
	END IF;

	
--- Updating the below values as Null to ensure any manual updates in the table need txn id.
/*
	:NEW.c5053_last_updated_by := NULL;
	:NEW.c5071_last_updated_txn_id := NULL;*/
END trg_5071_SET_PART_QTY;
/
