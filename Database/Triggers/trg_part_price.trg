/* Formatted on 2010/05/17 17:11 (Formatter Plus v4.8.0) */

--@"C:\DATABASE\Triggers\trg_part_price.trg";
CREATE OR REPLACE TRIGGER trg_part_price
	AFTER DELETE OR INSERT
	ON t4011_group_detail
	FOR EACH ROW
DECLARE
BEGIN
	IF (DELETING)
	THEN
		gm_pkg_pd_group_pricing.gm_pd_sav_part_price_log (:OLD.c4011_group_detail_id
														, :OLD.c205_part_number_id
														, :OLD.c4010_group_id
														, :OLD.c901_part_pricing_type
														, 52090
														, :OLD.c4011_created_by
														, :OLD.c4011_created_date
														, :OLD.c4011_last_updated_by
														 );
	ELSIF (INSERTING)
	THEN	 		
		gm_pkg_pd_group_pricing.gm_pd_sav_part_price_log (:NEW.c4011_group_detail_id
														, :NEW.c205_part_number_id
														, :NEW.c4010_group_id
														, :NEW.c901_part_pricing_type
														, 52091
														, :NEW.c4011_created_by
														, :NEW.c4011_created_date
														, :NEW.c4011_last_updated_by
														 );
	END IF;
--
END trg_part_price;
/
