/* Formatted on 2011/04/20 14:22 (Formatter Plus v4.8.0) */
--@"c:\database\triggers\trg_5053_location_part_mapping.trg"
CREATE OR REPLACE TRIGGER trg_5072_SET_PART_LOT_QTY
	BEFORE INSERT OR UPDATE OF c5072_curr_qty
	ON t5072_SET_PART_LOT_QTY
	FOR EACH ROW
DECLARE
    v_count        NUMBER;
--	v_set_part_qty_id    t5071_SET_PART_QTY.c5071_set_part_qty_id%TYPE;
--	v_set_lot_master_id    t5070_SET_lot_master.c5070_set_lot_master_id%TYPE;
	V_TRANS_TYPE T5070_SET_LOT_MASTER.C901_TXN_TYPE%TYPE;
 --   v_set_part_lot_qty_id    t5072_SET_PART_LOT_QTY.c5072_set_lot_qty_id%TYPE;



BEGIN
IF (	UPDATING
		AND (	:NEW.c5072_last_updated_by IS NULL
			 OR :NEW.c5072_last_updated_txn_id IS NULL
			 		 
			)
	   )
	THEN
		raise_application_error
			('-20501'
		   , 'Either of the following is not correctly entered : Transaction by, Trasaction id,' 
			);
	END IF;
	
	
	

	IF NVL (:OLD.c5072_curr_qty, 0) > 0 AND :NEW.c5072_curr_qty IS NULL
	THEN
		raise_application_error ('-20500'
							   ,	'Inventory Location Quantity Cannot be empty for Part: '
								 || :NEW.c205_part_number_id
							
								 || ' and Transaction: '
								 || :NEW.c5072_last_updated_txn_id
								);
	END IF;	
--

   
	IF (UPDATING AND NVL (:OLD.c5072_curr_qty, 0) != NVL (:NEW.c5072_curr_qty, 0))
	THEN
		gm_pkg_set_lot_track.gm_sav_set_part_lot_qty_log (
														  :NEW.C5072_set_part_lot_qty_id
  							     					     ,:NEW.c5070_set_lot_master_id
  							     					     ,:NEW.C5071_set_part_qty_id
														 ,:NEW.c205_part_number_id
    												     ,:NEW.C5072_CONTROL_NUMBER
    													 ,(:NEW.c5072_curr_qty-:OLD.c5072_curr_qty)
													     ,:NEW.C5072_curr_QTY
    												     ,:OLD.c5072_curr_qty
    												     ,CURRENT_DATE
														 ,:NEW.c5072_last_updated_by
														 ,:NEW.c5072_last_updated_txn_id
														 ,:NEW.c901_txn_type
													   	 ,:NEW.c901_type
													     );
										 
									 								 
														 

	ELSIF (INSERTING AND NVL (:NEW.c5072_curr_qty, 0) <> 0)
	THEN
		IF :NEW.c5072_last_updated_txn_id IS NULL 
		THEN
			raise_application_error
				('-20501'
		   			, 'The following is not correctly entered : Trasaction id'
				);
		END IF;
	
		gm_pkg_set_lot_track.gm_sav_set_part_lot_qty_log (		  												
														  :NEW.C5072_set_part_lot_qty_id
  							     					     ,:NEW.c5070_set_lot_master_id
  							     					     ,:NEW.C5071_set_part_qty_id
														 ,:NEW.c205_part_number_id
    												     ,:NEW.C5072_CONTROL_NUMBER
    												     ,:NEW.C5072_curr_QTY
    												     ,:NEW.C5072_curr_QTY
													   	 ,0
													   	 , CURRENT_DATE
														 ,:NEW.c5072_last_updated_by
														 ,:NEW.c5072_last_updated_txn_id
														 ,:NEW.c901_txn_type
													   	 ,:NEW.c901_type
   													     );
										 
														 
	END IF;

	
--- Updating the below values as Null to ensure any manual updates in the table need txn id.
/*
    :NEW.c5072_last_updated_by := NULL;
	:NEW.c5072_last_updated_txn_id := NULL;*/
	
END trg_5072_SET_PART_LOT_QTY;
/ 
