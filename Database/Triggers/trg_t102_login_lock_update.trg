/* Formatted on 2010/07/22 11:13 (Formatter Plus v4.8.0) */
-- @"c:\Database\Triggers\trg_t102_login_lock_update.trg"
CREATE OR REPLACE TRIGGER trg_t102_login_lock_update
   BEFORE UPDATE OF c102_login_lock_fl
   ON t102_user_login
   FOR EACH ROW
DECLARE
   v_old_fl   t102_user_login.c102_login_lock_fl%TYPE;
   v_new_fl   t102_user_login.c102_login_lock_fl%TYPE;
BEGIN
   v_old_fl := :OLD.c102_login_lock_fl;
   v_new_fl := :NEW.c102_login_lock_fl;

   IF (NVL (v_old_fl, 0) != NVL (v_new_fl, 0))
   THEN
      gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.c102_login_username,
                                                 v_new_fl,
                                                 1035,
                                                 :NEW.c102_last_updated_by,
                                                 SYSDATE
                                                );
   END IF;
END trg_ttp_part_detail_upd;
/
