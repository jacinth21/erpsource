create or replace
TRIGGER "GLOBUS_APP"."TRG_T1012_PARTY_OTHER_INFO"
   BEFORE INSERT OR UPDATE OF C1012_PARTY_OTHER_INFO_ID, C901_TYPE, C1012_DETAIL, C101_PARTY_ID, C1012_VOID_FL
   ON T1012_PARTY_OTHER_INFO
   FOR EACH ROW
   DECLARE
  V_ACTION 				    VARCHAR2(20) ;
  BEGIN

	IF INSERTING THEN
        V_ACTION := '103122'; -- New
    ELSIF UPDATING THEN
  						IF ((:OLD.C1012_PARTY_OTHER_INFO_ID <> :NEW.C1012_PARTY_OTHER_INFO_ID) 
                OR (:OLD.C901_TYPE <> :NEW.C901_TYPE)
                OR (:OLD.C1012_DETAIL <> :NEW.C1012_DETAIL)  
                OR (:OLD.C101_PARTY_ID <> :NEW.C101_PARTY_ID)
                OR (NVL(:OLD.C1012_VOID_FL,'N') <> NVL(:new.C1012_VOID_FL,'N') AND NVL(:new.C1012_VOID_FL,'N') = 'N'))
                
  						THEN
  						 V_ACTION := '103123'; -- Updated
  						END IF;
      END IF;
 -- When activity is voided, the If condition will satisfy.
      IF (NVL(:OLD.C1012_VOID_FL,'N') <> NVL(:new.C1012_VOID_FL,'N') AND NVL(:new.C1012_VOID_FL,'N') = 'Y')
      THEN
        V_ACTION  := '4000412'; -- Voided
      END IF;

      IF V_ACTION IS NOT NULL THEN
        -- 103097 English, 103100 Product Catalog , 10306161 Ref Type*/
        GM_PKG_PDPC_PRODCATTXN.GM_SAV_MASTER_DATA_UPD (:new.C1012_PARTY_OTHER_INFO_ID, '10306162', V_ACTION, '103097', '103100', :NEW.C1012_LAST_UPDATED_BY, NULL,'Y');
      END IF;
      
END;
/