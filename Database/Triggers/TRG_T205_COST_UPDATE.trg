--@"C:\Database\Triggers\TRG_T205_COST_UPDATE.trg";
CREATE OR REPLACE TRIGGER trg_t205_cost_update BEFORE
     UPDATE OF c205_cost ON t205_part_number FOR EACH ROW 
    DECLARE 
    BEGIN 
    
    IF (:OLD.c205_cost_history_fl IS NULL) THEN
        gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.c205_part_number_id, :NEW.c205_cost, 1045,
        :NEW.c205_last_updated_by, :NEW.c205_last_updated_date) ;
    :NEW.c205_cost_history_fl := 'Y';
ELSE
    gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.c205_part_number_id, :NEW.c205_cost, 1045,
    :NEW.c205_last_updated_by, :NEW.c205_last_updated_date) ;
END IF;
--
END trg_t205_cost_update;
/