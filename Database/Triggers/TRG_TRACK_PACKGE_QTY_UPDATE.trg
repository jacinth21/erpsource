/* Formatted on 2010/08/04 14:25 (Formatter Plus v4.8.0) */
-- @"c:\database\triggers\TRG_TRACK_PACKGE_QTY_UPDATE.trg"
CREATE OR REPLACE TRIGGER trg_track_packge_qty_update
   BEFORE INSERT OR UPDATE OF C205_QTY_IN_PACKAGING
   ON t205_part_number
   FOR EACH ROW
BEGIN
   -- Negative qty validation
   IF (NVL (:NEW.C205_QTY_IN_PACKAGING, 0) < 0)
   THEN
      raise_application_error
           ('-20500',
               'Requested quantity cannot exceed the available quantity for '
            || :NEW.c205_part_number_id
           );
   END IF;

   IF (UPDATING AND (:NEW.c205_last_updated_by IS NULL
       OR :NEW.c205_last_update_trans_id IS NULL
       OR :NEW.c901_action IS NULL
       OR :NEW.c901_type IS NULL)
      )
   THEN
      raise_application_error
         ('-20501',
          'Either of the following is not correctly entered : Trasaction id, Transaction type, Action taken'
         );
   END IF;

   --
   IF (UPDATING AND NVL(:OLD.C205_QTY_IN_PACKAGING,0) != NVL(:NEW.C205_QTY_IN_PACKAGING,0))
   THEN
      --
      INSERT INTO t214_transactions
                  (c214_transactions_id, c214_created_dt,
                   c214_created_by, c205_part_number_id,
                   c214_orig_qty, c214_id,
                   c214_trans_qty,
                   c214_new_qty, c901_action, c901_type,
                   c901_transaction_type
                  )
           VALUES (s214_transactions.NEXTVAL, SYSDATE,
                   :NEW.c205_last_updated_by, :NEW.c205_part_number_id,
                   NVL (:OLD.C205_QTY_IN_PACKAGING,0), :NEW.c205_last_update_trans_id,
                   (NVL(:NEW.C205_QTY_IN_PACKAGING,0) - NVL(:OLD.C205_QTY_IN_PACKAGING,0)
                   ),
                   NVL (:NEW.C205_QTY_IN_PACKAGING,0), :NEW.c901_action, :NEW.c901_type,
                   90815
                  );
   --
   ELSIF (INSERTING AND NVL(:NEW.C205_QTY_IN_PACKAGING,0) <> 0)
   THEN
      --
      INSERT INTO t214_transactions
                  (c214_transactions_id, c214_created_dt,
                   c214_created_by, c205_part_number_id, c214_orig_qty,
                   c214_id, c214_trans_qty,
                   c214_new_qty, c901_action, c901_type,
                   c901_transaction_type
                  )
           VALUES (s214_transactions.NEXTVAL, SYSDATE,
                   :NEW.c205_last_updated_by, :NEW.c205_part_number_id, 0,
                   :NEW.c205_last_update_trans_id, NVL (:NEW.C205_QTY_IN_PACKAGING,0),
                   NVL (:NEW.C205_QTY_IN_PACKAGING,0), :NEW.c901_action, :NEW.c901_type,
                   90815
                  );
   --
   END IF;

--
   :NEW.c205_last_updated_by := NULL;
   :NEW.c205_last_update_trans_id := NULL;
   :NEW.c901_action := NULL;
   :NEW.c901_type := NULL;
END;
/
