--@"c:\database\triggers\trg_t2072_di_gudid_status.trg";

CREATE OR REPLACE TRIGGER trg_t2072_di_gudid_status
	BEFORE INSERT OR UPDATE ON T2072_DI_GUDID_STATUS
	FOR EACH ROW
DECLARE
BEGIN
	IF ( UPDATING OR INSERTING) 
	 THEN

	         --   raise_application_error('-20500','  :OLD.C2072_DI_GUDID_STATUS_ID: '||:OLD.C2072_DI_GUDID_STATUS_ID);

		gm_pkg_pd_udi_submission_trans.gm_sav_gudid_stat_log(  :NEW.C2072_DI_GUDID_STATUS_ID,
									:NEW.C2060_DI_NUMBER,
									    :NEW.C901_INTERNAL_SUBMIT_STATUS,
									    :NEW.C901_EXTERNAL_SUBMIT_STATUS,
									    :NEW.C2072_DOCUMENT_ID,
									    :NEW.C2072_REVIEWED_BY,
									    :NEW.C2072_REVIEWED_DATE,
									    :NEW.C2072_GDSN_ITEM_UPLOAD_FL ,
									    :NEW.C2072_GDSN_ITEM_UPLOAD_DATE,
									    :NEW.C2072_GDSN_PUBLISH_UPLOAD_FL,
									    :NEW.C2072_GDSN_PUBLISH_UPLOAD_DATE,
									    :NEW.C2072_GDSN_CIC_FL,
									    :NEW.C2072_GDSN_CIC_DATE,
									    :NEW.C2072_GUDID_PUBLISH_FL,
									    :NEW.C2072_GUDID_PUBLISH_DATE,
									    :NEW.C2072_LAST_UPDATED_BY,
									    :NEW.C2072_LAST_UPDATED_DATE,
									    :NEW.C9030_JOB_ACTIVITY_ID,
									    :NEW.C2072_VOID_FL,
                                        :NEW.C2072_FDA_PUBLISH_DATE,
										:NEW.C9030_SOURCE_ACTIVITY_ID);
	 END IF;
END trg_t2072_di_gudid_status;
/