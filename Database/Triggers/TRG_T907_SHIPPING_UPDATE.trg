-- @"c:\Database\Triggers\trg_t907_shipping_update.trg";

/*********************************************************************
* Purpose: To Insert or Update company information for T907 table.
* Author: Velu
********************************************************************/

CREATE OR REPLACE TRIGGER trg_t907_shipping_update
   BEFORE INSERT OR UPDATE
   ON t907_shipping_info
   FOR EACH ROW
DECLARE
   v_consign_id		 t504_consignment.c504_consignment_id%TYPE;
   v_dist_id 		 t501_order.c501_distributor_id%TYPE;
   v_comp_id 		 t901_code_lookup.C901_CODE_ID%TYPE := 100800; -- Globus Medical India Inc
   v_div_id 		 t901_code_lookup.C901_CODE_ID%TYPE := 100823; -- US
   v_area_id 		 t901_code_lookup.C901_CODE_ID%TYPE;
   v_zone_id 		 t901_code_lookup.C901_CODE_ID%TYPE;
   v_ref_id 		 t412_inhouse_transactions.c412_ref_id%TYPE;

   BEGIN
	   
	    IF(:NEW.C901_SOURCE = '50184' OR :NEW.C901_SOURCE = '50181') THEN-- Consignment  
	    	v_dist_id := get_distid_from_requestid(:NEW.C907_REF_ID);
		ELSIF(:NEW.C901_SOURCE = '50180') THEN-- order
	   		v_dist_id := get_distid_from_orderid(:NEW.C907_REF_ID);
	    ELSIF(:NEW.C901_SOURCE = '50183') THEN-- Loaner ext
	   		v_dist_id := get_distid_from_loaner_transid(:NEW.C907_REF_ID);
		ELSIF(:NEW.C901_SOURCE = '50182' OR :NEW.C901_SOURCE = '50186') THEN -- Loaner, InHouse Loaner
			v_dist_id := get_distid_from_cons_loanerid(:NEW.C907_REF_ID);
		END IF;
		
		-- To get company information from distribute id
		IF(v_dist_id IS NOT NULL) THEN
			gm_pkg_cm_shipping_info.gm_cs_fch_company_info(v_dist_id,v_comp_id,v_div_id,v_area_id,v_zone_id);
		END IF;
		
		IF  :NEW.C907_VOID_Fl IS NULL THEN
			:NEW.c701_distributor_id:= v_dist_id;
	   		:NEW.C901_COMPANY_ID	:= v_comp_id;
			:NEW.C901_DIVISION_ID 	:= v_div_id; 
	   		:NEW.C901_AREA_ID 		:= v_area_id; 
	   		:NEW.C901_ZONE_ID 		:= v_zone_id;
  		END IF;
END trg_t907_shipping_update;
/
