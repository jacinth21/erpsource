/* Formatted on 2011/10/25 10:11 (Formatter Plus v4.8.0) */
-- @"c:\Database\Triggers\trg_inhouse_trans_void.trg";

CREATE OR REPLACE TRIGGER trg_inhouse_trans_void
	AFTER UPDATE OF c412_void_fl
	ON t412_inhouse_transactions
	FOR EACH ROW
DECLARE
BEGIN
	IF :NEW.c412_void_fl = 'Y'
	THEN
		gm_pkg_allocation.gm_inv_void_bytxn (:NEW.c412_inhouse_trans_id, :NEW.c412_type, :NEW.c412_last_updated_by);
		gm_pkg_op_item_control_txn.gm_void_control_number_txn (:NEW.c412_inhouse_trans_id, :NEW.c412_last_updated_by);
	END IF;
END trg_inhouse_trans_void;
/
