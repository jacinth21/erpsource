/* Formatted on 2008/10/09 12:56 (Formatter Plus v4.8.0) */
--@"c:\database\triggers\trg_qty_upd.trg"

CREATE OR REPLACE TRIGGER "GLOBUS_TEST_APP".trg_qty_upd
	BEFORE UPDATE OF c2605_qty
	ON t2605_audit_tag_detail
	FOR EACH ROW
DECLARE
	v_old		   t2605_audit_tag_detail.c2605_qty%TYPE;
	v_new		   t2605_audit_tag_detail.c2605_qty%TYPE;
BEGIN
--
	v_old		:= :OLD.c2605_qty;
	v_new		:= :NEW.c2605_qty;

--
	IF (v_old <> v_new)
	THEN
		--
		IF (:OLD.c2605_tag_history_fl IS NULL)
		THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.c2605_audit_tag_id
													 , v_old
													 , 1008
													 , :OLD.c2605_last_updated_by
													 , :OLD.c2605_last_updated_date
													  );
		END IF;

		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.c2605_audit_tag_id
												 , v_new
												 , 1008
												 , :NEW.c2605_last_updated_by
												 , :NEW.c2605_last_updated_date
												  );
		--
		:NEW.c2605_tag_history_fl := 'Y';
--
	END IF;
--
END trg_qty_upd;
/