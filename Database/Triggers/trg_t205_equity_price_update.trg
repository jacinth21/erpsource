--@"C:\Database\Triggers\trg_t2052_equity_price_update.trg";

CREATE OR REPLACE TRIGGER trg_t2052_equity_price_update
	BEFORE UPDATE OF C2052_EQUITY_PRICE
	ON T2052_PART_PRICE_MAPPING
	FOR EACH ROW
DECLARE 
	v_old		   	  T2052_PART_PRICE_MAPPING.C2052_EQUITY_PRICE%TYPE;
	v_new		      T2052_PART_PRICE_MAPPING.C2052_EQUITY_PRICE%TYPE;
	v_old_lprice	  T2052_PART_PRICE_MAPPING.C2052_LIST_PRICE%TYPE;
	v_new_lprice	  T2052_PART_PRICE_MAPPING.C2052_LIST_PRICE%TYPE;
	v_company_id      T2052_PART_PRICE_MAPPING.c1900_company_id%TYPE;
BEGIN 

	v_old				:= :OLD.C2052_EQUITY_PRICE;
	v_new				:= :NEW.C2052_EQUITY_PRICE;
	v_old_lprice		:= :OLD.C2052_LIST_PRICE;
	v_new_lprice		:= :NEW.C2052_LIST_PRICE;
	
	IF ( :NEW.C2052_Last_updated_by IS NULL  OR 
	     :NEW.C2052_Last_updated_date IS NULL )
    THEN
         raise_application_error ('-20501', 'Updated_Date and Updated_By should not be NULL.' );
	END IF;
 
    --Getting company id from context.   
    SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM dual;

	IF ( NVL(v_old,'-9999') <> NVL(v_new,'-9999') ) 
	THEN
	  --During update if history flag is null, need to track the previous price value if it is NOT NULL. 
	   IF :OLD.C2052_EQT_PRICE_HISTORY_FL IS NULL AND v_old IS NOT NULL
       THEN 
	    gm_pkg_pd_group_pricing.gm_sav_grp_part_price_log (
													       :NEW.c205_part_number_id
													     , 103020
														 , ' ' 
													     , v_old
													     , 1030
													     , NULL   --for OLD price value we dont who set this. so for OLD price this will be NULL always.
													     , NULL
													     , v_company_id
													   );      

	   END IF;
       
		gm_pkg_pd_group_pricing.gm_sav_grp_part_price_log (
													      :NEW.c205_part_number_id
													     , 103020
														 , NVL(:NEW.C2052_PRICE_COMMENTS,' ') 
													     , v_new
													     , 1030
													     , :NEW.C2052_Last_updated_by
													     , :NEW.C2052_Last_updated_date
													     , v_company_id
													   );
													   
		:NEW.C2052_EQT_PRICE_HISTORY_FL  := 'Y';
		
	END IF;
		
END trg_t2052_equity_price_update;
/