/* Formatted on 2012/03/01 17:56 (Formatter Plus v4.8.0) */
--@"C:\Database\Triggers\trg_t507_returns_item_delete.trg"
CREATE OR REPLACE TRIGGER trg_t507_returns_item_delete
	BEFORE DELETE
	ON t507_returns_item
	FOR EACH ROW
BEGIN
	-- Cannot delete a credited Part Number
	IF (:OLD.c507_status_fl = 'C') THEN
		raise_application_error ('-20500', 'Cannot delete a credited Part Number.');
	END IF;
END;
/