create or replace
TRIGGER trg_4012_group_company_mapping_ipad_update
BEFORE UPDATE OF C901_GROUP_COM_MAP_TYP, C4012_VOID_FL ON t4012_group_company_mapping
FOR EACH ROW 

DECLARE	

 v_old_grp_maptype    	t4012_group_company_mapping.C901_GROUP_COM_MAP_TYP%TYPE;
 v_new_grp_maptype    	t4012_group_company_mapping.C901_GROUP_COM_MAP_TYP%TYPE;
 v_old_void_fl 			t4012_group_company_mapping.C4012_VOID_FL%TYPE;
 v_new_void_fl 			t4012_group_company_mapping.C4012_VOID_FL%TYPE;
 
BEGIN 
-- 
  v_old_grp_maptype		:= NVL(:OLD.C901_GROUP_COM_MAP_TYP,'-9999'); 
  v_new_grp_maptype		:= NVL(:NEW.C901_GROUP_COM_MAP_TYP,'-9999');
  v_old_void_fl      	:= NVL(:OLD.C4012_VOID_FL, 'N');
  v_new_void_fl      	:= NVL(:NEW.C4012_VOID_FL, 'N');
  
--if any changes in t4012_group_company_mapping , then Update ipad sync flag as null and ipad sync date as current date in t4012_group_company_mapping table
 
IF (v_old_grp_maptype <> v_new_grp_maptype OR (v_old_void_fl <> v_new_void_fl))
	THEN 
					:NEW.C4012_IPAD_SYNCED_FLAG := NULL;
   					:NEW.C4012_IPAD_SYNCED_DATE := NULL;
   					
END IF;
 
END trg_4012_group_company_mapping_ipad_update;
/