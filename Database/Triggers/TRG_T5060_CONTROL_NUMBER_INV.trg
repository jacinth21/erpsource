--@"c:\database\triggers\trg_t5060_control_number_inv.trg";
CREATE OR REPLACE
TRIGGER trg_t5060_control_number_inv
  BEFORE INSERT OR UPDATE OF c5060_qty
  ON t5060_control_number_inv
  FOR EACH ROW
DECLARE
  v_new_qty NUMBER;
BEGIN
	v_new_qty :=:NEW.c5060_qty;
	
	
	-- Negative qty validation. If Negative save as 0 so the txn will have a record
	IF (NVL (:NEW.c5060_qty, 0) < 0)
	THEN
		v_new_qty :=0;
		
	END IF;	
	
	-- If there is a change in Qty, save in Log    
	IF (UPDATING AND NVL (:OLD.c5060_qty, 0) != NVL (:NEW.c5060_qty, 0))
	THEN
		gm_pkg_op_lot_track.gm_sav_control_num_inv_log(:NEW.c5060_control_number_inv_id,
			:NEW.c205_part_number_id,
			:NEW.c5060_control_number,
			:NEW.c901_warehouse_type,
			--(NVL(v_new_qty,0) - NVL(:OLD.c5060_qty,0)),
			(NVL(:NEW.c5060_qty,0) - NVL(:OLD.c5060_qty,0)),
			:OLD.c5060_qty,
			NVL(v_new_qty,0),
			:NEW.c5060_last_update_trans_id,
			:NEW.c5060_created_by,
			:NEW.c901_ref_type,
			:NEW.c901_last_transaction_type,
			:NEW.c5060_ref_id,
			:NEW.c1900_company_id,
			:NEW.c5040_plant_id);
			
		--Update the lot information into t2550 while inserting t5060_control_number_inv based on part number, control number, warehouse, Ref id and Ref type
		   gm_pkg_op_lot_track_txn.gm_sav_lot_inventory(:NEW.c205_part_number_id,
		   :NEW.c5060_control_number,
		   :NEW.c901_warehouse_type,
		   (NVL(:NEW.c5060_qty,0) - NVL(:OLD.c5060_qty,0)),
		   :NEW.c5060_last_update_trans_id,
		   :NEW.c901_last_transaction_type,
		   :NEW.c901_ref_type,
		   :NEW.c5060_ref_id,
		   :NEW.c5060_created_by,
		   :NEW.c1900_company_id,
		   :NEW.c5040_plant_id,                                            
		   :NEW.C901_SHIP_TO_TYPE, 
		   :NEW.C5060_SHIP_TO_ID, 
		   :NEW.C5060_SHIPPED_DT, 
		   :NEW.C5060_CUSTOMER_PO);
			

	ELSIF (INSERTING AND NVL (:NEW.c5060_qty, 0) <> 0)
	THEN	  
		gm_pkg_op_lot_track.gm_sav_control_num_inv_log(:NEW.c5060_control_number_inv_id,
			:NEW.c205_part_number_id,
			:NEW.c5060_control_number,
			:NEW.c901_warehouse_type,
			NVL(:NEW.c5060_qty,0),
			0, -- Initially original qty should be zero
			NVL(v_new_qty,0),
			:NEW.c5060_last_update_trans_id,
			:NEW.c5060_created_by,
			:NEW.c901_ref_type,
			:NEW.c901_last_transaction_type,
			:NEW.c5060_ref_id,
			:NEW.c1900_company_id,
			:NEW.c5040_plant_id);
			
		--Update the lot information into t2550 while inserting t5060_control_number_inv based on part number, control number, warehouse, Ref id and Ref type 
		   gm_pkg_op_lot_track_txn.gm_sav_lot_inventory(:NEW.c205_part_number_id,
		   :NEW.c5060_control_number,
		   :NEW.c901_warehouse_type,
		    NVL(:NEW.c5060_qty,0),
		   :NEW.c5060_last_update_trans_id,
		   :NEW.c901_last_transaction_type,
		   :NEW.c901_ref_type,
		   :NEW.c5060_ref_id,
		   :NEW.c5060_created_by,
		   :NEW.c1900_company_id,
		   :NEW.c5040_plant_id,                                              
		   :NEW.C901_SHIP_TO_TYPE, 
		   :NEW.C5060_SHIP_TO_ID, 
		   :NEW.C5060_SHIPPED_DT, 
		   :NEW.C5060_CUSTOMER_PO);
	END IF;

	--Because Negative quantity should not be available, Update as that as zero
	IF (:NEW.c5060_qty<0) THEN
		:NEW.c5060_qty :=0;
	END IF;
	:NEW.c5060_last_updated_by := NULL;
	:NEW.c5060_last_update_trans_id:=NULL;
   
END trg_t5060_control_number_inv;
/