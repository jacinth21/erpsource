--@"C:\database\Triggers\ProdDevelopment\trg_2082_system_release_history.trg";

CREATE OR REPLACE TRIGGER trg_2082_system_release_history
	BEFORE UPDATE
	ON T2082_SET_RELEASE_MAPPING
	FOR EACH ROW
DECLARE

BEGIN
	--
	IF (:OLD.C901_STATUS_ID <> :NEW.C901_STATUS_ID) THEN
	     IF (:OLD.C2082_STATUS_HISTORY_FL IS NULL) THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.C207_SET_ID||'-'||get_code_name(:OLD.C901_ENTITY_ID)
													 , get_code_name(:OLD.C901_STATUS_ID)
													 , 313
													 , :OLD.C2082_UPDATED_BY
													 , :OLD.C2082_UPDATED_DATE
													  );
													  
			
			
		 END IF;
   
   
             gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.C207_SET_ID||'-'||get_code_name(:NEW.C901_ENTITY_ID)
													 , get_code_name(:NEW.C901_STATUS_ID)
													 , 313
													 , :NEW.C2082_UPDATED_BY
													 , :NEW.C2082_UPDATED_DATE
													  );  
			:NEW.C2082_STATUS_HISTORY_FL := 'Y';										  
	END IF;
	--
END trg_2082_system_release_history;
/