/*
 * Trigger will last updated UTC everytime insert/update.
 */
CREATE OR REPLACE TRIGGER TRG_T4010_UTC_DATE
   BEFORE INSERT OR UPDATE
   ON T4010_GROUP
   FOR EACH ROW
DECLARE
BEGIN
	:NEW.C4010_UPDATED_DATE_UTC := SYSTIMESTAMP;
	
END TRG_T4010_UTC_DATE;
/