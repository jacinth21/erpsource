/*
 * Trigger will call  bulk DO by system procedure.
 */
 
CREATE OR REPLACE TRIGGER trg_t240_baseline_value_update BEFORE
     INSERT OR
     UPDATE OF C240_VALUE ON t240_baseline_value
	 FOR EACH ROW DECLARE BEGIN
        
		--20175 Baseline case
		
        IF (NVL (:OLD.c240_value, - 999) <> NVL (:NEW.c240_value, - 999)
        AND :NEW.c901_type                = 20175
        AND :NEW.c240_void_fl              IS NULL)
		THEN
			gm_pkg_sm_tag_order_history.gm_process_order_bulk_do_by_system (
			:OLD.c207_set_id, NVL (:NEW.c240_last_updated_by, :NEW.c240_created_by)) ;
		END IF;
		--
END trg_t240_baseline_value_update;
/