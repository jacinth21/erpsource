-- @"c:\Database\Triggers\TRG_T704_ACCOUNT_UPDATE.trg";

create or replace
TRIGGER TRG_T704_ACCOUNT_UPDATE
   BEFORE INSERT OR UPDATE OF C704_SHIP_ADD1,C704_SHIP_ADD2,C704_SHIP_CITY,C704_SHIP_STATE,C704_SHIP_COUNTRY,C704_SHIP_ZIP_CODE,
   					C704_BILL_ADD1,C704_BILL_ADD2,C704_BILL_CITY,C704_BILL_STATE,C704_BILL_COUNTRY,C704_BILL_ZIP_CODE,
   					C704_ACCOUNT_NM,C703_SALES_REP_ID,C704_ACTIVE_FL,C704_VOID_FL,C704_ACCOUNT_ID,C901_ACCOUNT_TYPE,C101_PARTY_ID
   ON T704_ACCOUNT
   FOR EACH ROW

DECLARE
	v_action  VARCHAR2(20) ;
	v_accttype t704_account.c901_account_type%TYPE ;
	v_old_dist_id  VARCHAR2(20) ;
	v_new_dist_id  VARCHAR2(20) ;
	v_gpo_cnt NUMBER;
	v_price_upd_fl  VARCHAR2(1);
	v_company_id T704_ACCOUNT.C1900_COMPANY_ID%TYPE;
	v_old_company_id     T704_ACCOUNT.C1900_COMPANY_ID%TYPE;
	v_new_company_id     T704_ACCOUNT.C1900_COMPANY_ID%TYPE;

BEGIN
	v_old_dist_id := GET_DISTRIBUTOR_ID(:OLD.C703_SALES_REP_ID);
	v_new_dist_id := GET_DISTRIBUTOR_ID(:NEW.C703_SALES_REP_ID);
	v_company_id :=:NEW.C1900_COMPANY_ID;
	v_old_company_id     := :OLD.C1900_COMPANY_ID;
    v_new_company_id     := :NEW.C1900_COMPANY_ID;

    IF v_company_id IS NULL THEN
      		 v_company_id := v_old_company_id  ;
       ELSE
      		 v_company_id := v_new_company_id  ;
       END IF;
    
   IF (UPDATING AND(:NEW.C704_BILL_ADD1 IS NULL
              OR :NEW.C704_BILL_CITY IS NULL
              OR :NEW.C704_BILL_STATE IS NULL
              OR :NEW.C704_BILL_COUNTRY IS NULL
              OR :NEW.C704_BILL_ZIP_CODE IS NULL
              OR :NEW.C704_SHIP_ADD1 IS NULL
              OR :NEW.C704_SHIP_CITY IS NULL
              OR :NEW.C704_SHIP_STATE IS NULL
              OR :NEW.C704_SHIP_COUNTRY IS NULL
              OR :NEW.C704_SHIP_ZIP_CODE IS NULL              
              )
      )
   THEN
      raise_application_error('-20535','');
   END IF;
   
   
  IF (UPDATING AND (:OLD.C704_BILL_ADD1 <> :NEW.C704_BILL_ADD1)
                OR (:OLD.C704_BILL_CITY <> :NEW.C704_BILL_CITY)
                OR (:OLD.C704_BILL_STATE <> :NEW.C704_BILL_STATE)
                OR (:OLD.C704_BILL_COUNTRY <> :NEW.C704_BILL_COUNTRY)
                OR (:OLD.C704_BILL_ZIP_CODE <> :NEW.C704_BILL_ZIP_CODE)
      )
  THEN
		        :NEW.c704_BILL_LATITUDE := NULL;
		        :NEW.c704_BILL_LONGITUDE := NULL;
		        :NEW.c704_BILL_GEO_LOCATION := NULL;
		        
		         v_action := '103123'; -- Updated
  END IF;
  IF (UPDATING AND (:OLD.C704_SHIP_ADD1 <> :NEW.C704_SHIP_ADD1)
  				OR (:OLD.C704_SHIP_ADD2 <> :NEW.C704_SHIP_ADD2)
                OR (:OLD.C704_SHIP_CITY <> :NEW.C704_SHIP_CITY)
                OR (:OLD.C704_SHIP_STATE <> :NEW.C704_SHIP_STATE)
                OR (:OLD.C704_SHIP_COUNTRY <> :NEW.C704_SHIP_COUNTRY)
                OR (:OLD.C704_SHIP_ZIP_CODE <> :NEW.C704_SHIP_ZIP_CODE)
	  )
   THEN
   				:NEW.c704_SHIP_LATITUDE := NULL;
		        :NEW.c704_SHIP_LONGITUDE := NULL;
		        :NEW.c704_SHIP_GEO_LOCATION := NULL;
		        
		        v_action := '103123'; -- Updated
  END IF;
  
  --IF (:NEW.C901_ACCOUNT_TYPE ='70110')
 -- THEN
  		IF INSERTING THEN
        	v_action := '103122'; -- New
        
			
        --When existing parent or new parent is selected from during account setup
        IF(NVL(:OLD.C101_PARTY_ID,-999)<>NVL(:NEW.C101_PARTY_ID,-999)) 
        THEN
             	v_price_upd_fl:='Y';
        END IF;
        --PMT-9127 No Update to IPAD should be fired if Rep Account is mapped to New Parent
    ELSIF UPDATING THEN
  						IF ( (:OLD.C704_ACCOUNT_NM <> :NEW.C704_ACCOUNT_NM) OR (:OLD.C703_SALES_REP_ID <> :NEW.C703_SALES_REP_ID) 
  								OR (:OLD.C704_ACCOUNT_ID <> :NEW.C704_ACCOUNT_ID) OR (:OLD.C901_ACCOUNT_TYPE <> :NEW.C901_ACCOUNT_TYPE) OR (v_old_dist_id <> v_new_dist_id) 
  								OR (NVL(:OLD.C704_VOID_FL,'N') <> NVL(:NEW.C704_VOID_FL,'N') AND NVL(:NEW.C704_VOID_FL,'N') = 'N') 
  								  OR (:OLD.C704_SHIP_NAME <> :NEW.C704_SHIP_NAME) OR (NVL(:OLD.C704_BILL_ADD1,'-9999')) <> (:NEW.C704_BILL_ADD1)
  								  OR (NVL(:OLD.C704_SHIP_ADD1,'-9999')) <> (:NEW.C704_SHIP_ADD1) OR (NVL(:OLD.C704_SHIP_ADD2,'-9999')) <> (:NEW.C704_SHIP_ADD2)
     							OR (NVL(:OLD.C704_ACTIVE_FL,'N') <> NVL(:NEW.C704_ACTIVE_FL,'N') AND NVL(:NEW.C704_ACTIVE_FL,'N') = 'Y'))
  						THEN
  							 v_action := '103123'; -- Updated
  							 
  							 --When existing parent or new parent is selected from during account setup
						        IF(NVL(:OLD.C101_PARTY_ID,-999)<>NVL(:NEW.C101_PARTY_ID,-999)) 
						        THEN
						        	v_price_upd_fl:='Y';
						        END IF;
  						END IF;
      END IF;
 -- When a account is voided, the If condition will satisfy.
  IF (NVL(:OLD.C704_VOID_FL,'N') <> NVL(:NEW.C704_VOID_FL,'N') AND NVL(:NEW.C704_VOID_FL,'N') = 'Y') 
     	 OR (NVL(:OLD.C704_ACTIVE_FL,'N') <> NVL(:NEW.C704_ACTIVE_FL,'N') AND NVL(:NEW.C704_ACTIVE_FL,'N') = 'N')
       THEN
        v_action  := '4000412'; -- Voided
      END IF;
      
--      raise_application_error('-20999',':NEW.c901_ext_country_id:' || :NEW.c901_ext_country_id );
    
      
	      IF v_action IS NOT NULL AND :NEW.c901_ext_country_id IS NULL AND :NEW.C1900_COMPANY_ID = v_company_id THEN  --Companyid is fetched dynamically from table
	      
	        -- 103097 English, 103100 Product Catalog , 4000519 Ref Type
	        gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (:NEW.c704_account_id, '4000519', v_action, '103097', '103100', NVL (:NEW.c704_created_by, :NEW.c704_last_updated_by ), NULL,'Y',v_company_id) ;
	        
	        --Update only if the Patent account is change and not other parameter from account set up sereen
	        IF v_price_upd_fl='Y'
	        THEN
	        --Account Pricing Update'4000526' 
	         gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (:NEW.c704_account_id ,'4000526', v_action,'103097', '103100', NVL (:NEW.c704_created_by, :NEW.c704_last_updated_by ), NULL,'Forceupdate',v_company_id) ;
	        END IF;
	     END IF;
     --  END IF;
      
END; 
/   