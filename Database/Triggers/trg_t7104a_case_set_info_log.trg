/* Formatted on 2011/11/08 12:36 (Formatter Plus v4.8.0) */
-- @"c:\Database\Triggers\trg_t7104a_case_set_info_log.trg"

CREATE OR REPLACE TRIGGER trg_t7104a_case_set_info_log
	BEFORE UPDATE OF c5010_tag_id, c526_product_request_detail_id
	ON t7104_case_set_information
	FOR EACH ROW
DECLARE
	v_old_tag	   t7104_case_set_information.c5010_tag_id%TYPE;
	v_new_tag	   t7104_case_set_information.c5010_tag_id%TYPE;
	v_old_reqdtlid t7104_case_set_information.c526_product_request_detail_id%TYPE;
	v_new_reqdtlid t7104_case_set_information.c526_product_request_detail_id%TYPE;
	v_case_set_type t7104_case_set_information.c901_system_type%TYPE;
	v_settype	   t7104_case_set_information.c901_set_location_type%TYPE;
	v_type		   t7104a_case_set_info_log.c901_type%TYPE;
	v_attrval	   t7104a_case_set_info_log.C7104A_VALUE%TYPE;
BEGIN
--
	v_old_tag	:= :OLD.c5010_tag_id;
	v_new_tag	:= :NEW.c5010_tag_id;
	v_old_reqdtlid := :OLD.c526_product_request_detail_id;
	v_new_reqdtlid := :NEW.c526_product_request_detail_id;
	v_settype	:= NVL (:NEW.c901_set_location_type, 0);
	v_case_set_type	:= NVL (:NEW.c901_system_type, 0);
    
--
	IF ((NVL(v_old_tag,0) <> NVL(v_new_tag,0)) OR (NVL(v_old_reqdtlid,0) <> NVL(v_new_reqdtlid,0)))
	THEN
	    --For MIM, device request, no need to update the v_attrval, v_type values.
		IF(v_case_set_type <> '103642' AND  v_case_set_type <> '103643' AND v_case_set_type <> '103644')
		THEN	
			SELECT DECODE (v_settype, 11375, 'N/A', 11378, 'N', 'N'), DECODE (v_settype, 11378, 11392, 11381, 11392, 0, 11396, NULL)
			  INTO v_attrval, v_type
			  FROM DUAL;
			--
			--If set type is loaner(11381) and new request detailId is null, it consider as a loaner deallocation and attrvalue is also 'N/A',
			--otherwise it will affect tag allocation email component.
	         IF (NVL(v_old_reqdtlid,0) <> NVL(v_new_reqdtlid,0) AND v_new_reqdtlid IS NULL)
	         THEN
	                  SELECT DECODE (v_settype, 11381, 'N/A', 'N'), DECODE (v_settype, 11381,11396, NULL)
	              INTO v_attrval, v_type
	              FROM DUAL;
	
	        END IF;
	     END IF;
	    --For MIM, request from device , v_type should be c901_system_type.
		IF(v_case_set_type = '103642' OR  v_case_set_type = '103643' OR v_case_set_type = '103644')
		THEN
			v_type := v_case_set_type;
		END IF;
		
		gm_pkg_sm_setmgmt_txn.gm_cm_sav_tag_log (NULL
											   ,:OLD.c7104_case_set_id
											   , v_new_tag
											   , v_new_reqdtlid
											   , v_type
											   , v_attrval
											   , :NEW.c7104_last_updated_by
												);
	--
	END IF;
--
END trg_t7104a_case_set_info_log;
/
