create or replace
TRIGGER "GLOBUS_APP"."TRG_T6605_CRF_UPDT" 
   BEFORE INSERT OR UPDATE OF c6605_crf_id, c101_party_id, c6630_activity_id, c901_request_type, c6605_request_date, c901_status,
   c6605_service_type, c6605_reason, c6605_service_description, c6605_hcp_qualification, c6605_prep_time, c6605_work_hours, c6605_time_fl ,
   c6605_agreement_fl, c6605_travel_from, c6605_travel_to, c6605_travel_time, c6605_travel_miles, c6605_hour_rate, c6605_honoraria_status,
   c6605_honoraria_work_hrs, c6605_honoraria_prep_hrs, c6605_honoraria_comment, c6605_created_by, C6605_PRIORITY_FL
   ON T6605_CRF
   FOR EACH ROW
   DECLARE
  V_ACTION 				    VARCHAR2(20) ;
  BEGIN

	IF INSERTING THEN
        V_ACTION := '103122'; -- New
    ELSIF UPDATING THEN
  						IF ((:OLD.c6605_crf_id <> :NEW.c6605_crf_id) 
                OR (:OLD.c101_party_id <> :NEW.c101_party_id)
                OR (:OLD.c6630_activity_id <> :NEW.c6630_activity_id)  
                OR (:OLD.c901_request_type <> :NEW.c901_request_type)
                OR (:OLD.c6605_request_date <> :NEW.c6605_request_date)
                OR (:OLD.c901_status <> :NEW.c901_status)
                OR (:OLD.c6605_service_type <> :NEW.c6605_service_type)
                OR (:OLD.c6605_reason <> :NEW.c6605_reason)
                OR (:OLD.c6605_service_description <> :NEW.c6605_service_description)
                OR (:OLD.c6605_hcp_qualification <> :NEW.c6605_hcp_qualification)
                OR (:OLD.c6605_prep_time <> :NEW.c6605_prep_time)
                OR (:OLD.c6605_work_hours <> :NEW.c6605_work_hours)
                OR (:OLD.c6605_time_fl <> :NEW.c6605_time_fl)
                OR (:OLD.c6605_agreement_fl <> :NEW.c6605_agreement_fl) OR (:OLD.c6605_travel_from <> :NEW.c6605_travel_from)
                OR (:OLD.c6605_travel_to <> :NEW.c6605_travel_to) OR (:OLD.c6605_travel_time <> :NEW.c6605_travel_time)
                OR (:OLD.c6605_travel_miles <> :NEW.c6605_travel_miles) OR (:OLD.c6605_hour_rate <> :NEW.c6605_hour_rate)
                OR (:OLD.c6605_honoraria_status <> :NEW.c6605_honoraria_status) OR (:OLD.c6605_honoraria_work_hrs <> :NEW.c6605_honoraria_work_hrs)
                OR (:OLD.c6605_honoraria_prep_hrs <> :NEW.c6605_honoraria_prep_hrs) OR (:OLD.c6605_honoraria_comment <> :NEW.c6605_honoraria_comment)
                OR (:OLD.c6605_created_by <> :NEW.c6605_created_by) 
                OR (:OLD.C6605_PRIORITY_FL <> :NEW.C6605_PRIORITY_FL) 
                OR (NVL(:OLD.c6605_void_fl,'N') <> NVL(:new.c6605_void_fl,'N') AND NVL(:new.c6605_void_fl,'N') = 'N'))
                
  						THEN
  						 V_ACTION := '103123'; -- Updated
  						END IF;
      END IF;
 -- When activity is voided, the If condition will satisfy.
      IF (NVL(:OLD.c6605_void_fl,'N') <> NVL(:new.c6605_void_fl,'N') AND NVL(:new.c6605_void_fl,'N') = 'Y')
      THEN
        V_ACTION  := '4000412'; -- Voided
      END IF;

      IF V_ACTION IS NOT NULL THEN
        -- 103097 English, 103100 Product Catalog , 10306176 Ref Type*/
        GM_PKG_PDPC_PRODCATTXN.GM_SAV_MASTER_DATA_UPD (:new.c6605_crf_id, '10306176', V_ACTION, '103097', '103100', :NEW.C6605_LAST_UPDATED_BY, NULL,'Y');
      END IF;
      
END;
/