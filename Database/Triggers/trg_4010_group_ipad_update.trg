
CREATE OR REPLACE TRIGGER trg_4010_group_ipad_update 
   BEFORE  
   	 UPDATE OF C4010_INACTIVE_FL, C4010_VOID_FL, C901_TYPE, C207_SET_ID, C901_GROUP_TYPE, C4010_PUBLISH_FL, C901_PRICED_TYPE
   	 ON t4010_group FOR EACH ROW
   DECLARE
   
	v_old_system_id 	t4010_group.C207_SET_ID%TYPE;
	v_new_system_id 	t4010_group.C207_SET_ID%TYPE;
	v_old_type 		    t4010_group.C901_TYPE%TYPE;
	v_new_type 		    t4010_group.C901_TYPE%TYPE;
	v_old_grp_type 		t4010_group.C901_GROUP_TYPE%TYPE;
	v_new_grp_type 		t4010_group.C901_GROUP_TYPE%TYPE;
	v_old_void_fl 		t4010_group.C4010_VOID_FL%TYPE;
	v_new_void_fl 		t4010_group.C4010_VOID_FL%TYPE;
	v_old_inactive_fl 	t4010_group.C4010_INACTIVE_FL%TYPE;
	v_new_inactive_fl 	t4010_group.C4010_INACTIVE_FL%TYPE;
	v_old_publ_fl		T4010_GROUP.C4010_PUBLISH_FL%TYPE;
	v_new_publ_fl 		T4010_GROUP.C4010_PUBLISH_FL%TYPE;
	v_old_priced_type	t4010_group.C901_PRICED_TYPE%TYPE;  
	v_new_priced_type	t4010_group.C901_PRICED_TYPE%TYPE;

    BEGIN

        v_old_system_id    := NVL(:OLD.C207_SET_ID,'-9999');
        v_new_system_id    := NVL(:NEW.C207_SET_ID,'-9999');
        v_old_grp_type     := NVL(:OLD.C901_GROUP_TYPE,'-9999');
        v_new_grp_type     := NVL(:NEW.C901_GROUP_TYPE,'-9999');
        v_old_type         := NVL(:OLD.C901_TYPE,'-9999');
        v_new_type         := NVL(:NEW.C901_TYPE,'-9999');
        v_old_void_fl      := NVL(:OLD.C4010_VOID_FL, 'N');
        v_new_void_fl      := NVL(:NEW.C4010_VOID_FL, 'N');
        v_old_inactive_fl  := NVL(:OLD.C4010_INACTIVE_FL,'N');
        v_new_inactive_fl  := NVL(:NEW.C4010_INACTIVE_FL,'N');
        v_old_publ_fl      := NVL(:OLD.C4010_PUBLISH_FL,'N');
        v_new_publ_fl      := NVL(:NEW.C4010_PUBLISH_FL,'N');
        v_old_priced_type  := NVL(:OLD.C901_PRICED_TYPE,'-9999');
        v_new_priced_type  := NVL(:NEW.C901_PRICED_TYPE,'-9999');
        
        
    IF ((v_old_system_id <> v_new_system_id) OR (v_old_type <> v_new_type) OR (v_old_grp_type <> v_new_grp_type) OR (v_old_void_fl <> v_new_void_fl) OR 
			(v_old_inactive_fl <> v_new_inactive_fl) OR (v_old_publ_fl <> v_new_publ_fl) OR (v_old_priced_type <> v_new_priced_type))
	THEN 
					:NEW.C4010_IPAD_SYNCED_FLAG := NULL;
   					:NEW.C4010_IPAD_SYNCED_DATE := NULL;
   					
	END IF;
       
    END trg_4010_group_ipad_update;
    /