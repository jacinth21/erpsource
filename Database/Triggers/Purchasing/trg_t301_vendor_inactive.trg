/*
 * this trg is used update history details when vendor is make an active or inactive
 */
CREATE OR REPLACE TRIGGER TRG_T301_vendor_inactive
   BEFORE UPDATE
   ON T301_VENDOR
   FOR EACH ROW
DECLARE
v_old_fl	         T301_VENDOR.C301_ACTIVE_FL%TYPE;
v_new_fl		     T301_VENDOR.C301_ACTIVE_FL%TYPE;
v_comments			 VARCHAR2(40);
v_comments_old		 VARCHAR2(40);    
BEGIN
 
--
v_old_fl                      := :OLD.C301_ACTIVE_FL;
v_new_fl                      := :NEW.C301_ACTIVE_FL;

--
IF v_new_fl = 'N' THEN
	v_comments := 'Inactive';
	v_comments_old := 'Active';
ELSE
	v_comments := 'Active';
	v_comments_old := 'Inactive';
END IF;

	--
	IF (nvl(v_old_fl,-999) <> nvl(v_new_fl,-999)) THEN
	     IF (:OLD.C301_INACTIVE_DATE_HISTORY_FL  IS NULL) THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:old.c301_vendor_id
													 , v_comments_old
													 , 4000765
													 , :old.c301_created_by
													 , :old.c301_created_date
													  );
		  END IF;
 
             gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:new.c301_vendor_id
													 , v_comments
													 , 4000765
													 , :new.c301_last_updated_by
													 , :new.c301_last_updated_date
													  );  
			:NEW.C301_INACTIVE_DATE_HISTORY_FL  := 'Y';										  
	END IF;
	--

END TRG_T301_vendor_inactive;
/