/* Formatted on 2019/01/16 11:26 (Formatter Plus v4.8.0) */
/* @"\Database\Triggers\Purchasing\trg_t402_wo_activity_log.trg"*/
/*
 * Trigger will update the log of the vendor status and status flag at everytime of t402_work_order UPDATE 
 */
 
 CREATE OR REPLACE TRIGGER trg_t402_wo_activity_log 
BEFORE  UPDATE OF C901_VENDOR_STATUS, C402_COST_PRICE, C402_QTY_ORDERED, C402_CRITICAL_FL, C402_FAR_FL, C402_STATUS_FL
ON t402_work_order 
FOR EACH ROW 
DECLARE	

 v_old_status  	   T402_WORK_ORDER.c901_vendor_status%TYPE;
 v_new_status      T402_WORK_ORDER.c901_vendor_status%TYPE;
 v_old_qty         T402_WORK_ORDER.C402_QTY_ORDERED%TYPE;
 v_new_qty         T402_WORK_ORDER.C402_QTY_ORDERED%TYPE;
 v_old_price       T402_WORK_ORDER.C402_COST_PRICE%TYPE;
 v_new_price       T402_WORK_ORDER.C402_COST_PRICE%TYPE;
 v_ref_id          T402_WORK_ORDER.C402_WORK_ORDER_ID%TYPE;
 v_ref_type        T4203_PURCHASE_ACTIVITY_LOG.C901_REF_TYPE%TYPE;
 v_old_tmp_str     T4203_PURCHASE_ACTIVITY_LOG.C4203_OLD_STRING%TYPE;
 v_new_tmp_str     T4203_PURCHASE_ACTIVITY_LOG.C4203_NEW_STRING%TYPE;
 v_vendor_id       T402_WORK_ORDER.C301_VENDOR_ID%TYPE;
 v_user_id         T402C_WORK_ORDER_REVISION.C402C_LAST_UPDATED_BY%TYPE;
 v_comp_id         T402_WORK_ORDER.C1900_COMPANY_ID%TYPE;
 v_old_critfl	   T402_WORK_ORDER.C402_CRITICAL_FL%TYPE;
 v_new_critfl	   T402_WORK_ORDER.C402_CRITICAL_FL%TYPE;
 v_old_farfl	   T402_WORK_ORDER.C402_FAR_FL%TYPE;
 v_new_farfl	   T402_WORK_ORDER.C402_FAR_FL%TYPE;
 v_po_id	       T402_WORK_ORDER.C401_PURCHASE_ORD_ID%TYPE;
 v_old_stsfl	   T402_WORK_ORDER.C402_STATUS_FL%TYPE;
 v_new_stsfl	   T402_WORK_ORDER.C402_STATUS_FL%TYPE;
 
BEGIN 
-- 

  v_ref_id          := :OLD.C402_WORK_ORDER_ID;
  v_ref_type        := 108441;
  v_user_id         := :NEW.C402_LAST_UPDATED_BY; 
  v_old_status      := :OLD.c901_vendor_status;
  v_new_status      := :NEW.c901_vendor_status;
  v_old_qty 	    := :OLD.C402_QTY_ORDERED;
  v_new_qty     := :NEW.C402_QTY_ORDERED;
  v_old_price   := :OLD.C402_COST_PRICE;
  v_new_price   := :NEW.C402_COST_PRICE;

  v_vendor_id   := :OLD.C301_VENDOR_ID;
  v_comp_id     := :OLD.C1900_COMPANY_ID;	
  
  v_old_critfl  := :OLD.C402_CRITICAL_FL;
  v_new_critfl  := :NEW.C402_CRITICAL_FL;
  v_old_farfl   := :OLD.C402_FAR_FL;
  v_new_farfl   := :NEW.C402_FAR_FL;
  v_po_id       := :NEW.C401_PURCHASE_ORD_ID;
  v_old_stsfl   := :OLD.C402_STATUS_FL;
  v_new_stsfl   := :NEW.C402_STATUS_FL;

--if any changes in vendor status , then insert changes in T4203_PURCHASE_ACTIVITY_LOG table
IF (v_old_status <> v_new_status)
THEN 
  
  v_old_tmp_str  := 'Updated Vendor Status from '||get_code_name(:OLD.c901_vendor_status);
  v_new_tmp_str  := get_rule_value('UPDVENDSTS','POACTLOG') || ' ' ||get_code_name(:NEW.c901_vendor_status);
  
  
           gm_pkg_purchase_activity.gm_sav_po_activity( 
                             v_ref_id, 
                             v_ref_type, 
                             v_old_status, 
                             v_new_status,
                             v_vendor_id,
                             v_old_tmp_str,
                             v_new_tmp_str,
                             v_user_id,
                             v_comp_id);	
   
END IF; 

----if any changes in work order qty , then insert changes in T4203_PURCHASE_ACTIVITY_LOG table
IF (v_old_qty <> v_new_qty)
THEN 
  
  v_old_tmp_str  := 'Updated Qty from '||v_old_qty;
  v_new_tmp_str  := get_rule_value('UPDQTY','POACTLOG') || ' ' ||v_new_qty;
  
           gm_pkg_purchase_activity.gm_sav_po_activity( 
                             v_ref_id, 
                             v_ref_type, 
                             null, 
                             null,
                             v_vendor_id,
                             v_old_tmp_str,
                             v_new_tmp_str,
                             v_user_id,
                             v_comp_id);	
   
END IF; 

----if any changes in work order price , then insert changes in T4203_PURCHASE_ACTIVITY_LOG table
IF (v_old_price <> v_new_price)
THEN 

  v_old_tmp_str  := 'Updated Price from '||v_old_price;
  v_new_tmp_str  := get_rule_value('UPDPRICE','POACTLOG') || ' ' ||v_new_price;
  
  
           gm_pkg_purchase_activity.gm_sav_po_activity( 
                             v_ref_id, 
                             v_ref_type, 
                             null, 
                             null,
                             v_vendor_id,
                             v_old_tmp_str,
                             v_new_tmp_str,
                             v_user_id,
                             v_comp_id);	
   
END IF; 

----if work order is reopened, then insert changes in T4203_PURCHASE_ACTIVITY_LOG table
IF ((v_old_stsfl = 3) AND (v_new_stsfl = 2))
THEN
  v_old_tmp_str  := 'Reopened Work Orders from '||v_old_stsfl;
  v_new_tmp_str  := get_rule_value('REOPENWO','POACTLOG');  
  
           gm_pkg_purchase_activity.gm_sav_po_activity( 
                             v_po_id, 
                             108440, 
                             null, 
                             null,
                             v_vendor_id,
                             v_old_tmp_str,
                             v_new_tmp_str,
                             v_user_id,
                             v_comp_id);	

END IF;


----if any changes in work order, then insert changes in T4203_PURCHASE_ACTIVITY_LOG table
IF ((v_old_qty <> v_new_qty) OR (v_old_price <> v_new_price) OR (v_old_critfl <> v_new_critfl) OR (v_old_farfl <> v_new_farfl))
THEN 

  v_new_tmp_str  := get_rule_value('UPDATEPO','POACTLOG');  
  
           gm_pkg_purchase_activity.gm_sav_po_activity( 
                             v_po_id, 
                             108440, 
                             null, 
                             null,
                             v_vendor_id,
                             null,
                             v_new_tmp_str,
                             v_user_id,
                             v_comp_id);	
   
END IF; 
 
END trg_t402_wo_activity_log;
/ 