/* Formatted on 11/13/2009 13:24 (Formatter Plus v4.8.0) */
--@"C:\database\Triggers\trg_t402b_wo_vendor_commit_history.trg";

CREATE OR REPLACE TRIGGER trg_t402b_wo_vendor_commit_history
   BEFORE UPDATE
   ON T402B_WO_VENDOR_COMMIT 
   FOR EACH ROW
DECLARE
	v_old_commit_dt		   T402B_WO_VENDOR_COMMIT.C402B_COMMIT_DATE%TYPE;
	v_new_commit_dt		   T402B_WO_VENDOR_COMMIT.C402B_COMMIT_DATE%TYPE;
	v_old_commit_qty	   T402B_WO_VENDOR_COMMIT.C402B_COMMIT_QTY%TYPE;
	v_new_commit_qty	   T402B_WO_VENDOR_COMMIT.C402B_COMMIT_QTY%TYPE;

BEGIN
--
	v_old_commit_dt		   := :OLD.C402B_COMMIT_DATE;
	v_new_commit_dt		   := :NEW.C402B_COMMIT_DATE;
	
--
	IF (v_old_commit_dt <> v_new_commit_dt)
	THEN
		IF (:OLD.C402B_COMMIT_DATE_HISTORY_FL IS NULL)   --if first time insert of record
		THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.C402B_VENDOR_COMMIT_ID
													 , v_old_commit_dt
													 , 307
													 , :OLD.C402B_LAST_UPDATED_BY
													 , :OLD.C402B_LAST_UPDATED_DATE
													  );
		END IF;
		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.C402B_VENDOR_COMMIT_ID
												 , v_new_commit_dt
												 , 307
												 , :NEW.C402B_LAST_UPDATED_BY
												 , :NEW.C402B_LAST_UPDATED_DATE
												  );
		:NEW.C402B_COMMIT_DATE_HISTORY_FL := 'Y';

	END IF;
	
	v_old_commit_qty	   := :OLD.C402B_COMMIT_QTY;
	v_new_commit_qty	   := :NEW.C402B_COMMIT_QTY;
--
	
		IF (v_old_commit_qty <> v_new_commit_qty)
	THEN
		IF (:OLD.C402B_COMMIT_QTY_HISTORY_FL IS NULL)   --if first time insert of record
		THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.C402B_VENDOR_COMMIT_ID
													 , v_old_commit_qty
													 , 308
													 , :OLD.C402B_LAST_UPDATED_BY
													 , :OLD.C402B_LAST_UPDATED_DATE
													  );
		END IF;

		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.C402B_VENDOR_COMMIT_ID
												 , v_new_commit_qty
												 , 308
												 , :NEW.C402B_LAST_UPDATED_BY
												 , :NEW.C402B_LAST_UPDATED_DATE
												  );
		:NEW.C402B_COMMIT_QTY_HISTORY_FL := 'Y';

	END IF;
			
--
END trg_t402b_wo_vendor_commit_history;
/
