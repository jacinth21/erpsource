/*
* Trigger will last updated UTC everytime insert/update.
*/
CREATE OR REPLACE TRIGGER trg_t302a_std_cost_log_utc BEFORE
     INSERT OR
     UPDATE ON t302a_vendor_standard_cost_log FOR EACH ROW
     
     DECLARE
     BEGIN
	     :NEW.c302a_last_updated_date_utc := SYSTIMESTAMP;
END trg_t302a_std_cost_log_utc;
/