/* Formatted on 2012/02/09 18:05 (Formatter Plus v4.8.0) */
--@"C:\DATABASE\Triggers\trg_t302_standard_cost_history.trg";

CREATE OR REPLACE TRIGGER trg_t302_standard_cost_history 
	BEFORE UPDATE
	ON T302_VENDOR_STANDARD_COST
	FOR EACH ROW
DECLARE
	
BEGIN
--
IF (NVL(:OLD.c302_standard_cost,-999) <> :NEW.c302_standard_cost) THEN
	IF (:OLD.C302_HISTORY_FL IS NULL) THEN
		gm_pkg_pur_vendor_cost.gm_sav_std_cost_history (:OLD.c301_vendor_id
										 , :OLD.c302_vendor_std_cost_id
										 , :OLD.c205_part_number_id
										 , :OLD.c302_standard_cost
										 , :OLD.c302_last_updated_by
										  );
		:NEW.C302_HISTORY_FL := 'Y';
	END IF;

	gm_pkg_pur_vendor_cost.gm_sav_std_cost_history (:NEW.c301_vendor_id
									 , :NEW.c302_vendor_std_cost_id
									 , :NEW.c205_part_number_id
									 , :NEW.c302_standard_cost
									 , :NEW.c302_last_updated_by
									 );
									 
END IF;									 
--
END trg_t302_standard_cost_history;
/
