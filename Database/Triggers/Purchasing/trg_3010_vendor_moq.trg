
--"@Database\Triggers\Purchasing\trg_3010_vendor_moq.trg";
   /*************************************************************************
    * Description : This trigger is used to insert the pnum,qty,void flag,vendorid,
					moqid in t3010a_vendor_moq_log table when insert or update happen
					in t3010_vendor_moq table
    * Author      : Prabhu vigneshwaran M D 
    *************************************************************************/
CREATE OR REPLACE TRIGGER TRG_3010_VENDOR_MOQ 
BEFORE UPDATE OF c3010_moq ON t3010_vendor_moq
FOR EACH ROW
DECLARE
	v_old		   t3010_vendor_moq.c3010_moq%TYPE;
	v_new		   t3010_vendor_moq.c3010_moq%TYPE;
		
BEGIN
	--
	v_old		:= :OLD.c3010_moq;
	v_new		:= :NEW.c3010_moq;
	
	--if old moq not equal to new moq then insert the value
    IF (nvl(v_old,-999) <> nvl(v_new,-999)) 
    THEN
		IF (:OLD.c3010_vendor_moq_hist_fl IS NULL)
	    THEN
		
           INSERT INTO t3010a_vendor_moq_log
                       (c3010_vendor_moq_id, 
                       c301_vendor_id, 
                       c205_part_number_id,
                       c3010_moq,
                       c3010_void_fl,
                       c3010_updated_by, 
                       c3010_updated_date,
                       c3010a_created_date
                       )
               VALUES
                       (
                       :old.c3010_vendor_moq_id, 
                       :old.c301_vendor_id, 
                       :old.c205_part_number_id, 
                       :old.c3010_moq, 
                       :old.c3010_void_fl,
                       :old.c3010_updated_by,
                       :old.c3010_updated_date,
                       CURRENT_DATE
                       ); 
			 :NEW.c3010_vendor_moq_hist_fl := 'Y';  
					   
		END IF;
          INSERT INTO t3010a_vendor_moq_log
                       (c3010_vendor_moq_id, 
                       c301_vendor_id, 
                       c205_part_number_id,
                       c3010_moq,
                       c3010_void_fl,
                       c3010_updated_by, 
                       c3010_updated_date,
                       c3010a_created_date
                       )
               VALUES
                       (
                       :old.c3010_vendor_moq_id, 
                       :old.c301_vendor_id, 
                       :new.c205_part_number_id, 
                       :new.c3010_moq, 
                       :new.c3010_void_fl,
                       :new.c3010_updated_by,
                       :new.c3010_updated_date,
                       CURRENT_DATE
                       );              
	     

	END IF;	                                         
  
END;