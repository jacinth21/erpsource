/* Formatted on 2019/01/16 11:26 (Formatter Plus v4.8.0) */
/* @"\Database\Triggers\Purchasing\trg_t401_po_activity_log.trg"*/

/*
 * Trigger will update the log of the status and publish flag at everytime of t401_purchase_order UPDATE 
 */

CREATE OR REPLACE TRIGGER trg_t401_po_activity_log 
BEFORE  UPDATE OF C901_STATUS,c401_publish_fl 
ON t401_purchase_order 
FOR EACH ROW 


DECLARE	

 v_old_type       t401_purchase_order.C901_STATUS%TYPE;
 v_new_type       t401_purchase_order.C901_STATUS%TYPE;
 v_ref_id 	      T4203_PURCHASE_ACTIVITY_LOG.C4203_REF_ID%TYPE;
 v_ref_type       T4203_PURCHASE_ACTIVITY_LOG.C901_REF_TYPE%TYPE;
 v_vendor_id      T4203_PURCHASE_ACTIVITY_LOG.C301_VENDOR_ID%TYPE;
 v_user_id        T401_PURCHASE_ORDER.C401_LAST_UPDATED_BY%TYPE;
 v_comp_id        T4203_PURCHASE_ACTIVITY_LOG.C1900_COMPANY_ID%TYPE;
 v_old_string     T4203_PURCHASE_ACTIVITY_LOG.C4203_OLD_STRING%TYPE;
 v_new_string     T4203_PURCHASE_ACTIVITY_LOG.C4203_NEW_STRING%TYPE;
 v_old_publish_fl T401_PURCHASE_ORDER.C401_PUBLISH_FL%TYPE;
 v_new_publish_fl T401_PURCHASE_ORDER.C401_PUBLISH_FL%TYPE;
 
 
BEGIN 
-- 
  v_old_type		:= :OLD.C901_STATUS; 
  v_new_type		:= :NEW.C901_STATUS; 
  v_ref_id 			:= :OLD.C401_PURCHASE_ORD_ID;
  v_ref_type 		:= 108440; 				--Purchase Order
  v_vendor_id 		:= :OLD.C301_VENDOR_ID; 
  v_user_id 		:= :NEW.C401_LAST_UPDATED_BY; 
  v_comp_id 		:= :OLD.C1900_COMPANY_ID; 
  v_old_string  	:= get_code_name(:OLD.C901_STATUS);
  v_new_string  	:= get_code_name(:NEW.C901_STATUS);
  v_old_publish_fl  := NVL(:OLD.C401_PUBLISH_FL,'N');
  v_new_publish_fl  := NVL(:NEW.C401_PUBLISH_FL,'N');


--if any changes in status or publish flag , then insert changes in T4203_PURCHASE_ACTIVITY_LOG table
 
IF ((v_old_type <> v_new_type) OR (v_old_publish_fl <> v_new_publish_fl)) 
THEN 
			IF (v_old_type = '108364')  
			THEN
			-- IF Status is changed from Hold then activity log get from Rule value by Rule id UNHOLD
				v_new_string  	:= get_rule_value('UNHOLD','POACTLOG');
			ELSE
				v_new_string  	:= NVL(get_rule_value(v_new_type,'POACTLOG'),get_code_name(v_new_type));
			END IF;

           gm_pkg_purchase_activity.gm_sav_po_activity( 
                             v_ref_id, 
                             v_ref_type, 
                             v_old_type, 
                             v_new_type,
                             v_vendor_id,
                             v_old_string,
                             v_new_string,
                             v_user_id,
                             v_comp_id);	
   
END IF; 
 
END trg_t401_po_activity_log;
/