/*
* Trigger will last updated UTC everytime insert/update.
*/
CREATE OR REPLACE TRIGGER trg_t302_std_cost_utc_date BEFORE
     INSERT OR
     UPDATE ON T302_VENDOR_STANDARD_COST FOR EACH ROW
     
     DECLARE
     BEGIN
	     :NEW.C302_LAST_UPDATED_DATE_UTC := SYSTIMESTAMP;
END trg_t302_std_cost_utc_date;
/