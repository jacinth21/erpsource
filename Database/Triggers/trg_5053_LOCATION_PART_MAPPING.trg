/* Formatted on 2011/04/20 14:22 (Formatter Plus v4.8.0) */
--@"c:\database\triggers\trg_5053_location_part_mapping.trg"
CREATE OR REPLACE TRIGGER trg_5053_location_part_mapping
	BEFORE INSERT OR UPDATE OF c5053_curr_qty
	ON t5053_location_part_mapping
	FOR EACH ROW
DECLARE
    v_count        NUMBER;
	v_old		   t5053_location_part_mapping.c5053_curr_qty%TYPE;
	v_new		   t5053_location_part_mapping.c5053_curr_qty%TYPE;
	v_action	   t5054_inv_location_log.c901_action%TYPE;
	v_txn_typ	   t5054_inv_location_log.c901_txn_type%TYPE;
	v_warehouse_id  t5051_inv_warehouse.c5051_inv_warehouse_id%TYPE;
	v_company_id   t1900_company.c1900_company_id%TYPE;
	v_plant_id     	t5040_plant_master.c5040_plant_id%TYPE;
BEGIN
IF (	UPDATING
		AND (	:NEW.c5053_last_updated_by IS NULL
			 OR :NEW.c5053_last_update_trans_id IS NULL
			 OR :NEW.c901_last_transaction_type IS NULL			 
			)
	   )
	THEN
		raise_application_error
			('-20501'
		   , 'Either of the following is not correctly entered : Transaction by, Trasaction id, Transaction type'
			);
	END IF;
	BEGIN
	SELECT c5051_inv_warehouse_id
		INTO v_warehouse_id
	   FROM t5052_location_master
	  WHERE c5052_location_id = :NEW.c5052_location_id ;
   EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_warehouse_id := NULL;
    END;
  --validating warehouse id not in FS or Account  
    SELECT COUNT(1) INTO v_count
	  FROM t906_rules
		WHERE c906_rule_id     = 'WHID'
		   AND c906_rule_grp_id = 'RULEWH'
		   AND c906_rule_value = v_warehouse_id 
		   AND c906_void_fl    IS NULL;
		   
  IF v_count = 0
    THEN
     SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;	
  		
	SELECT DECODE (:NEW.c901_last_transaction_type, '93327', '4301', '93326', '4302')
	  INTO v_action
	  FROM DUAL;

	SELECT '90800'
	  INTO v_txn_typ
	  FROM DUAL;

	-- Negative qty validation
	IF (NVL (:NEW.c5053_curr_qty, 0) < 0)
	THEN
		raise_application_error ('-20500'
							   ,	'Inventory Location Quantity Cannot be Negative for Part: '
								 || :NEW.c205_part_number_id
								 || '	Location: '
								 || :NEW.c5052_location_id
								);
	END IF;

	

	IF NVL (:OLD.c5053_curr_qty, 0) > 0 AND :NEW.c5053_curr_qty IS NULL
	THEN
		raise_application_error ('-20500'
							   ,	'Inventory Location Quantity Cannot be empty for Part: '
								 || :NEW.c205_part_number_id
								 || '	Location: '
								 || :NEW.c5052_location_id
								 || ' and Transaction: '
								 || :NEW.c5053_last_update_trans_id
								);
	END IF;	
--
	IF (UPDATING AND NVL (:OLD.c5053_curr_qty, 0) != NVL (:NEW.c5053_curr_qty, 0))
	THEN
		gm_pkg_op_inv_location_module.gm_sav_inv_loc_log (SYSDATE
														, :NEW.c5053_last_updated_by
														, :NEW.c205_part_number_id
														, :NEW.c5052_location_id
														, :OLD.c5053_curr_qty
														, :NEW.c5053_last_update_trans_id
														, (:NEW.c5053_curr_qty - :OLD.c5053_curr_qty)
														, :NEW.c5053_curr_qty
														, :NEW.c901_last_transaction_type
														, v_action
														, v_txn_typ														
														, v_company_id
														, v_plant_id
														, NULL
														, :NEW.C5053_LAST_TXN_CONTROL_NUMBER
														 );
--
	ELSIF (INSERTING AND NVL (:NEW.c5053_curr_qty, 0) <> 0)
	THEN
		IF :NEW.c5053_last_update_trans_id IS NULL 
		THEN
			raise_application_error
				('-20501'
		   			, 'The following is not correctly entered : Trasaction id'
				);
		END IF;
		gm_pkg_op_inv_location_module.gm_sav_inv_loc_log (SYSDATE
														, :NEW.c5053_created_by
														, :NEW.c205_part_number_id
														, :NEW.c5052_location_id
														, 0
														, :NEW.c5053_last_update_trans_id
														, :NEW.c5053_curr_qty
														, :NEW.c5053_curr_qty
														, :NEW.c901_last_transaction_type
														, v_action
														, v_txn_typ
														, v_company_id
														, v_plant_id
														, NULL 
														, :NEW.C5053_LAST_TXN_CONTROL_NUMBER
														 );
	END IF;

	ELSE --Field Sales Warehouse
		-- Negative qty validation
		
		SELECT  NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO  v_company_id FROM DUAL;

	IF (UPDATING)	
	THEN
		gm_pkg_op_inv_location_module.gm_sav_inv_loc_log ( NVL(:NEW.c5053_last_updated_date, :NEW.c5053_created_date)
														, :NEW.c5053_last_updated_by
														, :NEW.c205_part_number_id
														, :NEW.c5052_location_id
														, :OLD.c5053_curr_qty
														, :NEW.c5053_last_update_trans_id
														, (:NEW.c5053_curr_qty - :OLD.c5053_curr_qty)
														, :NEW.c5053_curr_qty
													        , :NEW.c901_action
														, :NEW.c901_type
														, :NEW.c901_last_transaction_type
														, v_company_id
														, v_plant_id
														, NULL 
														, :NEW.C5053_LAST_TXN_CONTROL_NUMBER
														 );
--
	ELSIF (INSERTING AND NVL (:NEW.c5053_curr_qty, 0) <> 0)
	THEN
		IF :NEW.c5053_last_update_trans_id IS NULL 
		THEN
			raise_application_error
				('-20501'
		   			, 'The following is not correctly entered : Trasaction id'
				);
		END IF;

		gm_pkg_op_inv_location_module.gm_sav_inv_loc_log (NVL(:NEW.c5053_last_updated_date, :NEW.c5053_created_date)
														, :NEW.c5053_created_by
														, :NEW.c205_part_number_id
														, :NEW.c5052_location_id
														, 0
														, :NEW.c5053_last_update_trans_id
														, :NEW.c5053_curr_qty
														, :NEW.c5053_curr_qty
														, :NEW.c901_action
														, :NEW.c901_type
														, :NEW.c901_last_transaction_type
														, v_company_id
														, v_plant_id
														, NULL 
														, :NEW.C5053_LAST_TXN_CONTROL_NUMBER
														 );
		END IF;		-- UPDATING												 
	END IF; -- v_warehouse_id

--
/*	:NEW.c5053_last_updated_by := NULL;
	:NEW.c5053_last_update_trans_id := NULL;
	:NEW.c901_last_transaction_type := NULL;*/
END trg_5053_location_part_mapping;
/
