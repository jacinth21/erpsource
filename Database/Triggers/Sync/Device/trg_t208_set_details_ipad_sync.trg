--@"c:\database\triggers\Sync\Device\trg_t208_set_details_ipad_sync.trg";
CREATE OR REPLACE TRIGGER trg_t208_set_details_ipad_sync 
   BEFORE  
   	 UPDATE ON T208_SET_DETAILS FOR EACH ROW 
   DECLARE
	v_old_part_nm  		t208_set_details.c205_part_number_id%TYPE;
	v_new_part_nm 		t208_set_details.c205_part_number_id%TYPE;
	v_old_set_qty	    t208_set_details.c208_set_qty%TYPE;	
    v_new_set_qty	    t208_set_details.c208_set_qty%TYPE;
    v_old_ins_fl	    VARCHAR2(10);	
    v_new_ins_fl	    VARCHAR2(10);
	v_old_void_fl		VARCHAR2(10);
	v_new_void_fl		VARCHAR2(10);
	v_system_id			t208_set_details.C207_SET_ID%TYPE;
	v_set_grp_type		t207_set_master.c901_set_grp_type%TYPE;

		
   BEGIN
  
   
        v_old_part_nm	   := NVL(:old.c205_part_number_id,'-9999');
        v_new_part_nm      := NVL(:new.c205_part_number_id,'-9999');
        v_old_set_qty	   := NVL(:old.c208_set_qty,'-9999');	
        v_new_set_qty	   := NVL(:new.c208_set_qty,'-9999');
        v_old_ins_fl       := NVL(:old.c208_inset_fl,'-9999');
        v_new_ins_fl       := NVL(:new.c208_inset_fl,'-9999');
        v_old_void_fl      := NVL(:old.c208_void_fl,'-9999');
        v_new_void_fl      := NVL(:new.c208_void_fl,'-9999');
        v_system_id		   := :NEW.c207_set_id; 
     
  --   BEGIN   
  --     SELECT C901_SET_GRP_TYPE 
  --      INTO v_set_grp_type
  --      FROM T207_SET_MASTER
  --      WHERE C207_SET_ID = v_system_id
  --        AND C901_PUBLISHED_STATUS='108660'
  --        AND C207_VOID_FL IS NULL; 
  --   EXCEPTION WHEN OTHERS THEN
  --		   RETURN;
  --	END;
        -- 1600 - System type, 1601 - SET type
  --      IF (v_set_grp_type IN ('1600', '1601')) THEN

        	IF (INSERTING OR (UPDATING AND ( (v_old_part_nm <> v_new_part_nm) OR (v_old_set_qty <> v_new_set_qty) 
                		OR (v_old_ins_fl <> v_new_ins_fl) OR (v_old_void_fl <> v_new_void_fl))  ))           
                THEN

						   :NEW.C208_IPAD_SYNCED_FLAG     := NULL;
						   :NEW.C208_IPAD_SYNCED_DATE 	  := NULL; 
						       
                END IF;

    --    END IF;		
     
    END trg_t208_set_details_ipad_sync;
    /