--@"c:\database\triggers\Sync\Device\trg_205_part_number_ipad_update.trg";

CREATE OR REPLACE TRIGGER TRG_205_PART_NUMBER_IPAD_UPDATE
	BEFORE UPDATE OF C205_PRODUCT_FAMILY, C205_PRODUCT_MATERIAL, C205_PRODUCT_CLASS, C205_ACTIVE_FL, C901_TYPE, C901_STATUS_ID 
	ON T205_PART_NUMBER
	FOR EACH ROW
DECLARE
 
	v_old_pfmly		T205_PART_NUMBER.C205_PRODUCT_FAMILY%TYPE;
	v_new_pfmly		T205_PART_NUMBER.C205_PRODUCT_FAMILY%TYPE;
	v_old_type 		T205_PART_NUMBER.C901_TYPE%TYPE;
	v_new_type 		T205_PART_NUMBER.C901_TYPE%TYPE;
	v_old_status 	T205_PART_NUMBER.C901_STATUS_ID%TYPE;
	v_new_status 	T205_PART_NUMBER.C901_STATUS_ID%TYPE;
	v_old_active_fl	VARCHAR2(10);
	v_new_active_fl	VARCHAR2(10);
	v_old_pmat		T205_PART_NUMBER.C205_PRODUCT_MATERIAL%TYPE;
	v_new_pmat		T205_PART_NUMBER.C205_PRODUCT_MATERIAL%TYPE;
	v_old_pclass	T205_PART_NUMBER.C205_PRODUCT_CLASS%TYPE;
	v_new_pclass	T205_PART_NUMBER.C205_PRODUCT_CLASS%TYPE;

BEGIN

	v_old_pfmly		:= NVL(:OLD.C205_PRODUCT_FAMILY,'-9999');
	v_new_pfmly		:= NVL(:NEW.C205_PRODUCT_FAMILY,'-9999');
	v_old_status	:= NVL(:OLD.C901_STATUS_ID,'-9999');
	v_new_status	:= NVL(:NEW.C901_STATUS_ID,'-9999'); 
	v_old_type		:= NVL(:OLD.C901_TYPE,'-9999');
	v_new_type		:= NVL(:NEW.C901_TYPE,'-9999');
	v_old_active_fl := NVL(:OLD.C205_ACTIVE_FL,'N');
	v_new_active_fl := NVL(:NEW.C205_ACTIVE_FL,'N');
	v_old_pmat	    := :OLD.C205_PRODUCT_MATERIAL;
	v_new_pmat	    := :NEW.C205_PRODUCT_MATERIAL;
	v_old_pclass	:= :OLD.C205_PRODUCT_CLASS;
	v_new_pclass	:= :NEW.C205_PRODUCT_CLASS;


	        	IF ((v_old_pfmly <> v_new_pfmly) OR (v_old_status <> v_new_status) OR (v_old_type <> v_new_type) 
	        		OR (v_old_active_fl <> v_new_active_fl) OR (v_old_pmat <> v_new_pmat)OR (v_old_pclass <> v_new_pclass))
                		         
                THEN
					 	:NEW.C205_IPAD_SYNCED_FLAG  := NULL;
						:NEW.C205_IPAD_SYNCED_DATE 	:= NULL; 
                END IF;
	
END TRG_205_PART_NUMBER_IPAD_UPDATE;
/