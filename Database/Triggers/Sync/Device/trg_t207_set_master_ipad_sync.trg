--@"c:\database\triggers\trg_207_set_master.trg";
CREATE OR REPLACE TRIGGER trg_t207_set_master_ipad_sync 
   BEFORE  
   	 UPDATE ON T207_SET_MASTER FOR EACH ROW 
   DECLARE
	v_old_publish_sts	t207_set_master.c901_published_status%TYPE;
	v_new_publish_sts	t207_set_master.c901_published_status%TYPE;
	v_old_status_id	    t207_set_master.c901_status_id%TYPE;	
    v_new_status_id	    t207_set_master.c901_status_id%TYPE;
	v_old_void_fl		VARCHAR2(10);
	v_new_void_fl		VARCHAR2(10);
	v_system_id			T207_SET_MASTER.C207_SET_ID%TYPE;
	v_set_grp_type		t207_set_master.c901_set_grp_type%TYPE;
	v_old_system		t207_set_master.C207_SET_SALES_SYSTEM_ID%TYPE;
	v_new_system		t207_set_master.C207_SET_SALES_SYSTEM_ID%TYPE;
	v_old_hrchy			t207_set_master.c901_hierarchy%TYPE;
	v_new_hrchy			t207_set_master.c901_hierarchy%TYPE;
	v_old_ctgry			t207_set_master.c207_category%TYPE;
	v_new_ctgry			t207_set_master.c207_category%TYPE;
	v_old_type			t207_set_master.c207_type%TYPE;
	v_new_type			t207_set_master.c207_type%TYPE;

	
   BEGIN

        v_old_publish_sts  := NVL(:old.c901_published_status,'-9999');
        v_new_publish_sts  := NVL(:new.c901_published_status,'-9999');
        v_old_void_fl      := NVL(:old.c207_void_fl,'-9999');
        v_new_void_fl      := NVL(:new.c207_void_fl,'-9999');
        v_old_status_id	   := NVL(:old.c901_status_id,'-9999');	
        v_new_status_id	   := NVL(:new.c901_status_id,'-9999');
        v_set_grp_type     := NVL(:new.c901_set_grp_type,'-9999');
        v_system_id		   := :NEW.c207_set_id; 
        v_old_hrchy        := NVL(:old.c901_hierarchy,'-9999');
        v_new_hrchy        := NVL(:new.c901_hierarchy,'-9999');
        v_old_ctgry        := NVL(:old.c207_category,'-9999');
        v_new_ctgry        := NVL(:new.c207_category,'-9999');
        v_old_type         := NVL(:old.c207_type,'-9999');
        v_new_type         := NVL(:new.c207_type,'-9999');
        v_old_system	   := NVL(:old.C207_SET_SALES_SYSTEM_ID,'-9999');	
        v_new_system	   := NVL(:new.C207_SET_SALES_SYSTEM_ID,'-9999');
        
        -- 1600 - System type, 1601 - SET type
        IF (v_set_grp_type IN (1600,1601)) THEN

        	IF (UPDATING AND ( (v_old_publish_sts <> v_new_publish_sts) OR (v_old_void_fl <> v_new_void_fl) 
                		OR (v_old_status_id <> v_new_status_id) OR ( v_old_hrchy <> v_new_hrchy) OR (v_old_ctgry <> v_new_ctgry) OR (v_old_type <> v_new_type)
                	OR (v_old_system <> v_new_system) )  )           
                THEN

						      		:NEW.C207_IPAD_SYNCED_FLAG     := NULL;
									:NEW.C207_IPAD_SYNCED_DATE 	   := NULL; 
						       
                END IF;

        END IF;		
     
     
    END trg_t207_set_master_ipad_sync;
    /