--@"c:\database\triggers\Sync\Device\trg_t207c_set_attribute_ipad_sync.trg";
CREATE OR REPLACE TRIGGER trg_t207c_set_attribute_ipad_sync 
   BEFORE  
   	 UPDATE ON T207C_SET_ATTRIBUTE FOR EACH ROW 
   DECLARE
	v_old_attr_type  	t207c_set_attribute.c901_attribute_type%TYPE;
	v_new_attr_type 	t207c_set_attribute.c901_attribute_type%TYPE;
	v_old_attr_val	    t207c_set_attribute.c207c_attribute_value%TYPE;	
    v_new_attr_val	    t207c_set_attribute.c207c_attribute_value%TYPE;
	v_old_void_fl		VARCHAR2(10);
	v_new_void_fl		VARCHAR2(10);
	v_system_id			t207c_set_attribute.C207_SET_ID%TYPE;
	v_set_grp_type		t207_set_master.c901_set_grp_type%TYPE;

		
   BEGIN

       
        v_old_attr_type	   := NVL(:old.c901_attribute_type,'-9999');
        v_new_attr_type    := NVL(:new.c901_attribute_type,'-9999');
        v_old_void_fl      := NVL(:old.c207c_void_fl,'-9999');
        v_new_void_fl      := NVL(:new.c207c_void_fl,'-9999');
        v_old_attr_val	   := NVL(:old.c207c_attribute_value,'-9999');	
        v_new_attr_val	   := NVL(:new.c207c_attribute_value,'-9999');
        v_system_id		   := :NEW.c207_set_id; 
     
 --    BEGIN   
 --       SELECT C901_SET_GRP_TYPE 
 --       INTO v_set_grp_type
 --       FROM T207_SET_MASTER
 --       WHERE C207_SET_ID = v_system_id
 --         AND C901_PUBLISHED_STATUS='108660'
 --         AND C207_VOID_FL IS NULL; 
 --    EXCEPTION WHEN OTHERS THEN
 --		   RETURN;
 --	END;
        -- 1600 - System type, 1601 - SET type
 --       IF (v_set_grp_type IN ('1600', '1601')) THEN

        	IF (INSERTING OR (UPDATING AND ( (v_old_attr_type <> v_new_attr_type) OR (v_old_void_fl <> v_new_void_fl) 
                		OR (v_old_attr_val <> v_new_attr_val))  ))           
                THEN
						   		:NEW.C207C_IPAD_SYNCED_FLAG     := NULL;
								:NEW.C207C_IPAD_SYNCED_DATE 	:= NULL; 
						       
                END IF;

  --      END IF;		
     
    END trg_t207c_set_attribute_ipad_sync;
    /