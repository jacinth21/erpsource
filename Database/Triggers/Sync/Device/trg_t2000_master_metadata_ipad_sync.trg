--@"c:\database\triggers\Sync\Device\trg_t2000_master_metadata_ipad_sync.trg";
CREATE OR REPLACE TRIGGER trg_t2000_master_metadata_ipad_sync 
   BEFORE  
   	 UPDATE ON T2000_MASTER_METADATA FOR EACH ROW 
   DECLARE
	v_old_ref_nm		t2000_master_metadata.c2000_ref_name%TYPE;	
    v_new_ref_nm		t2000_master_metadata.c2000_ref_name%TYPE;
    v_old_ref_desc		t2000_master_metadata.c2000_ref_desc%TYPE;	
    v_new_ref_desc		t2000_master_metadata.c2000_ref_desc%TYPE;
    v_old_ref_dtl_dsc	t2000_master_metadata.c2000_ref_dtl_desc%TYPE;	
    v_new_ref_dtl_dsc	t2000_master_metadata.c2000_ref_dtl_desc%TYPE;
	v_old_void_fl		VARCHAR2(10);
	v_new_void_fl		VARCHAR2(10);
	v_system_id			t2000_master_metadata.c2000_ref_id%TYPE;
	v_lang_id        	t2000_master_metadata.c901_language_id%TYPE;
	v_ref_type			t2000_master_metadata.c901_ref_type%TYPE;
	
	
   BEGIN

       
        v_old_ref_nm 		 := NVL(:old.c2000_ref_name,'-9999');
        v_new_ref_nm  		 := NVL(:new.c2000_ref_name,'-9999');
        v_old_ref_desc 		 := NVL(:old.c2000_ref_desc,'-9999');
        v_new_ref_desc		 := NVL(:new.c2000_ref_desc,'-9999');
        v_old_ref_dtl_dsc    := NVL(:old.c2000_ref_dtl_desc,'-9999');
        v_new_ref_dtl_dsc 	 := NVL(:new.c2000_ref_dtl_desc,'-9999');
        v_old_void_fl        := NVL(:old.c2000_void_fl,'-9999');
        v_new_void_fl        := NVL(:new.c2000_void_fl,'-9999');
        v_lang_id       	 := NVL(:new.c901_language_id,'-9999');
        v_system_id		     := :NEW.c2000_ref_id; 
        v_ref_type			 := :NEW.c901_ref_type;

        
        -- 103091 - Set, 103092 - System, 103090 - Part Number, 103094 - Group

        IF (v_ref_type IN ('103091','103092','103090','103094')) THEN

        	IF (UPDATING AND ( (v_old_ref_nm <> v_new_ref_nm) OR (v_old_ref_desc <> v_new_ref_desc)
        	 OR (v_old_ref_dtl_dsc <> v_new_ref_dtl_dsc) OR (v_old_void_fl <> v_new_void_fl))) 
                		         
                THEN

						   	  	:NEW.C2000_IPAD_SYNCED_FLAG     := NULL;
								:NEW.C2000_IPAD_SYNCED_DATE 	:= NULL; 
                END IF;

        END IF;		
     
    END trg_t2000_master_metadata_ipad_sync;
    /