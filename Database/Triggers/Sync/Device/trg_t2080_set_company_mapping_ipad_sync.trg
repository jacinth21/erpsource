--@"c:\database\triggers\Sync\Device\trg_t2080_set_company_mapping_ipad_sync.trg";
CREATE OR REPLACE TRIGGER trg_t2080_set_company_mapping_ipad_sync 
   BEFORE  
   	 UPDATE ON T2080_SET_COMPANY_MAPPING FOR EACH ROW 
   DECLARE
	v_old_ipad_rel_sts	t2080_set_company_mapping.c901_ipad_released_status%TYPE;	
    v_new_ipad_rel_sts	t2080_set_company_mapping.c901_ipad_released_status%TYPE;
	v_old_void_fl		VARCHAR2(10);
	v_new_void_fl		VARCHAR2(10);
	v_system_id			t2080_set_company_mapping.C207_SET_ID%TYPE;
	v_company_id        t2080_set_company_mapping.c1900_company_id%TYPE;
	v_set_grp_type		t207_set_master.c901_set_grp_type%TYPE;
	
   BEGIN

       
        v_old_ipad_rel_sts  := NVL(:old.c901_ipad_released_status,'-9999');
        v_new_ipad_rel_sts  := NVL(:new.c901_ipad_released_status,'-9999');
        v_old_void_fl       := NVL(:old.c2080_void_fl,'-9999');
        v_new_void_fl       := NVL(:new.c2080_void_fl,'-9999');
        v_company_id        := NVL(:new.c1900_company_id,'-9999');
        v_system_id		    := :NEW.c207_set_id; 
        
             
  --   BEGIN   
  --      SELECT C901_SET_GRP_TYPE 
  --      INTO v_set_grp_type
  --      FROM T207_SET_MASTER
  --      WHERE C207_SET_ID = v_system_id
  --        AND C901_PUBLISHED_STATUS='108660'
  --        AND C207_VOID_FL IS NULL; 
  --   EXCEPTION WHEN OTHERS THEN
  --		   RETURN;
  --	END;
        -- 1600 - System type, 1601 - SET type
  --      IF (v_set_grp_type IN ('1600', '1601')) THEN

        	IF (INSERTING OR (UPDATING AND ( (v_old_ipad_rel_sts <> v_new_ipad_rel_sts) OR (v_old_void_fl <> v_new_void_fl)))) 
                		         
                THEN
						   	 	:NEW.C2080_IPAD_SYNCED_FLAG     := NULL;
								:NEW.C2080_IPAD_SYNCED_DATE 	:= NULL; 
                END IF;

   --     END IF;		
     
    END trg_t2080_set_company_mapping_ipad_sync;
    /