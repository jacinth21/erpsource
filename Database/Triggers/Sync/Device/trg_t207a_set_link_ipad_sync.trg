--@"c:\database\triggers\Sync\Device\trg_t207a_set_link_ipad_sync.trg";
CREATE OR REPLACE TRIGGER trg_t207a_set_link_ipad_sync 
   BEFORE  
   	 UPDATE ON t207a_set_link FOR EACH ROW 
   DECLARE

    v_old_type			t207a_set_link.c901_type%TYPE;	
    v_new_type			t207a_set_link.c901_type%TYPE;
	v_old_shared_sts	t207a_set_link.c901_shared_status%TYPE;
	v_new_shared_sts	t207a_set_link.c901_shared_status%TYPE;
	v_system_id			t207a_set_link.c207_main_set_id%TYPE;
	v_set_grp_type		t207_set_master.c901_set_grp_type%TYPE;
	
   BEGIN
                

        v_old_type 			 := NVL(:old.c901_type,'-9999');
        v_new_type			 := NVL(:new.c901_type,'-9999');
        v_old_shared_sts     := NVL(:old.c901_shared_status,'-9999');
        v_new_shared_sts 	 := NVL(:new.c901_shared_status,'-9999');
        v_system_id		     := :NEW.c207_main_set_id; 

        
   --  BEGIN   
   --    SELECT C901_SET_GRP_TYPE 
   --     INTO v_set_grp_type
   --     FROM T207_SET_MASTER
   --     WHERE C207_SET_ID = v_system_id
   --       AND C901_PUBLISHED_STATUS='108660'
   --		  AND C207_VOID_FL IS NULL; 
   --  EXCEPTION WHEN OTHERS THEN
		   RETURN;
   --	END;
        -- 1600 - System type, 1601 - SET type
   --     IF (v_set_grp_type IN ('1600', '1601')) THEN

        	IF (INSERTING OR (UPDATING AND ((v_old_type <> v_new_type)
        		OR (v_old_shared_sts <> v_new_shared_sts)))) 
                		         
                THEN
						   
						   :NEW.C207A_IPAD_SYNCED_FLAG     := NULL;
						   :NEW.C207A_IPAD_SYNCED_DATE 	   := NULL; 
                END IF;

   --     END IF;		
     
    END trg_t207a_set_link_ipad_sync;
    /