--@"c:\database\triggers\Sync\Device\trg_205_part_number_ipad_update.trg";

CREATE OR REPLACE TRIGGER TRG_2023_PART_COMPANY_MAPPING_IPAD_UPDATE
	BEFORE UPDATE OF C901_PART_MAPPING_TYPE,
					 C1900_COMPANY_ID,
					 C2023_VOID_FL
	ON T2023_PART_COMPANY_MAPPING
	FOR EACH ROW
DECLARE
	v_old_part_map_type		T2023_PART_COMPANY_MAPPING.C901_PART_MAPPING_TYPE%TYPE;
	v_new_part_map_type		T2023_PART_COMPANY_MAPPING.C901_PART_MAPPING_TYPE%TYPE;
	v_old_comp_id			T2023_PART_COMPANY_MAPPING.C1900_COMPANY_ID%TYPE;
	v_new_comp_id			T2023_PART_COMPANY_MAPPING.C1900_COMPANY_ID%TYPE;
	v_old_void_fl			VARCHAR2(10);
	v_new_void_fl			VARCHAR2(10);
	v_rfs_cnt				NUMBER;

	
BEGIN

   
	v_old_part_map_type		:= NVL(:OLD.C901_PART_MAPPING_TYPE,'-9999');
	v_new_part_map_type		:= NVL(:NEW.C901_PART_MAPPING_TYPE,'-9999');
	v_old_comp_id			:= NVL(:OLD.C1900_COMPANY_ID,'-9999');
	v_new_comp_id			:= NVL(:NEW.C1900_COMPANY_ID,'-9999');
	v_old_void_fl			:= NVL(:OLD.C2023_VOID_FL,'N');
	v_new_void_fl			:= NVL(:NEW.C2023_VOID_FL,'N');

	
		--RFS Check
	SELECT COUNT(1) 
		INTO v_rfs_cnt 
	FROM T205D_PART_ATTRIBUTE 
		WHERE C205_PART_NUMBER_ID=:NEW.C205_PART_NUMBER_ID
		AND C901_ATTRIBUTE_TYPE = 80180 
		AND C205D_VOID_FL is null;
		
			IF v_rfs_cnt <> 0 THEN
	        	IF ((v_old_part_map_type <> v_new_part_map_type) OR (v_old_comp_id <> v_new_comp_id) OR (v_old_void_fl <> v_new_void_fl))
                		         
                THEN
						   	 	:NEW.C2023_IPAD_SYNCED_FLAG        := NULL;
								:NEW.C2023_IPAD_SYNCED_DATE 	   := NULL; 
                END IF;
			END IF;
       
	
END TRG_205_PART_NUMBER_IPAD_UPDATE;
/