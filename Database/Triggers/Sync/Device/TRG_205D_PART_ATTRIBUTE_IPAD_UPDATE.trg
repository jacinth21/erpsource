--@"c:\database\triggers\Sync\Device\TRG_205D_PART_ATTRIBUTE_IPAD_UPDATE.trg";

CREATE OR REPLACE TRIGGER TRG_205D_PART_ATTRIBUTE_IPAD_UPDATE
	BEFORE UPDATE OF C205D_ATTRIBUTE_VALUE,
					 C205D_VOID_FL,
					 C901_ATTRIBUTE_TYPE,
					 C1900_COMPANY_ID,
					 C205D_HISTORY_FL
	ON T205D_PART_ATTRIBUTE 
	FOR EACH ROW
DECLARE
	v_old_attr_val		T205D_PART_ATTRIBUTE.C205D_ATTRIBUTE_VALUE%TYPE;
	v_new_attr_val		T205D_PART_ATTRIBUTE.C205D_ATTRIBUTE_VALUE%TYPE;
	v_old_void_fl		T205D_PART_ATTRIBUTE.C205D_VOID_FL%TYPE;
	v_new_void_fl		T205D_PART_ATTRIBUTE.C205D_VOID_FL%TYPE;
	v_old_attr_typ		T205D_PART_ATTRIBUTE.C901_ATTRIBUTE_TYPE%TYPE;
	v_new_attr_typ		T205D_PART_ATTRIBUTE.C901_ATTRIBUTE_TYPE%TYPE;
	v_old_comp_id		T205D_PART_ATTRIBUTE.C1900_COMPANY_ID%TYPE;
	v_new_comp_id		T205D_PART_ATTRIBUTE.C1900_COMPANY_ID%TYPE;
	
BEGIN

	v_old_attr_val		:= NVL(:OLD.C205D_ATTRIBUTE_VALUE,'-9999');
	v_new_attr_val		:= NVL(:NEW.C205D_ATTRIBUTE_VALUE,'-9999');
	v_old_void_fl		:= NVL(:OLD.C205D_VOID_FL,'N');
	v_new_void_fl		:= NVL(:NEW.C205D_VOID_FL,'N');
	v_old_attr_typ		:= NVL(:OLD.C901_ATTRIBUTE_TYPE,'-9999');
	v_new_attr_typ		:= NVL(:NEW.C901_ATTRIBUTE_TYPE,'-9999');
	v_old_comp_id		:= NVL(:OLD.C1900_COMPANY_ID,'-9999');
	v_new_comp_id		:= NVL(:NEW.C1900_COMPANY_ID,'-9999');

	-- Only trigger when, RFS type modified

	        	IF v_new_attr_typ = 80180 AND ((v_old_attr_val <> v_new_attr_val) OR (v_old_void_fl <> v_new_void_fl) OR (v_old_attr_typ <> v_new_attr_typ)
	        		OR (v_old_comp_id <> v_new_comp_id))
                		         
                THEN
				   	 	:NEW.C205D_IPAD_SYNCED_FLAG  := NULL;
						:NEW.C205D_IPAD_SYNCED_DATE  := NULL; 
                END IF;

END TRG_205D_PART_ATTRIBUTE_IPAD_UPDATE;
/