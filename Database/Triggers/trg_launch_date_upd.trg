/* Formatted on 2009/10/06 19:07 (Formatter Plus v4.8.0) */

CREATE OR REPLACE TRIGGER trg_launch_date_upd
	BEFORE INSERT OR UPDATE OF c4021_launch_date
	ON t4021_demand_mapping
	FOR EACH ROW
DECLARE
	v_old		   t4021_demand_mapping.c4021_launch_date%TYPE;
	v_new		   t4021_demand_mapping.c4021_launch_date%TYPE;
BEGIN
--
	v_old		:= :OLD.c4021_launch_date;
	v_new		:= :NEW.c4021_launch_date;

--
	IF (:OLD.c4021_demand_mapping_id IS NULL)	--if first time insert of record
	THEN   -- if insert
		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.c4021_demand_mapping_id
												 , v_new
												 , 1025
												 , :NEW.c4021_created_by
												 , :NEW.c4021_created_date
												  );
	ELSIF (v_old <> v_new OR :OLD.c4021_launch_date IS NULL)   -- if update date
	THEN
		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.c4021_demand_mapping_id
												 , v_new
												 , 1025
												 , :NEW.c4021_last_updated_by
												 , :NEW.c4021_last_updated_date
												  );
	END IF;

	IF (:OLD.c4021_launch_date IS NOT NULL)
	THEN
		:NEW.c4021_history_fl := 'Y';
	END IF;
--
END trg_launch_date_upd;
/
