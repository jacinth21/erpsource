/*
 * Trigger will last updated UTC everytime insert/update.
 */
CREATE OR REPLACE TRIGGER TRG_T9500_UTC_DATE
   BEFORE INSERT OR UPDATE
   ON t9500_data_download_log    
   FOR EACH ROW
DECLARE
BEGIN
	:NEW.C9500_UPDATED_DATE_UTC := SYSTIMESTAMP;
	
END TRG_T9500_UTC_DATE;
/