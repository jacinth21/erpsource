--@"c:\database\triggers\TRG_205_PART_NUMBER.trg";

CREATE OR REPLACE TRIGGER TRG_205_PART_NUMBER
	BEFORE INSERT OR UPDATE ON T205_PART_NUMBER
	FOR EACH ROW
DECLARE
	v_old_nm	T205_PART_NUMBER.C205_PART_NUM_NAME%TYPE;
	v_new_nm	T205_PART_NUMBER.C205_PART_NUM_NAME%TYPE;
	v_old_desc	T205_PART_NUMBER.C205_PART_NUM_DESC%TYPE;
	v_new_desc	T205_PART_NUMBER.C205_PART_NUM_DESC%TYPE;
	v_old_dtl_desc	T205_PART_NUMBER.C205_PART_NUM_DTL_DESC%TYPE;
	v_new_dtl_desc	T205_PART_NUMBER.C205_PART_NUM_DTL_DESC%TYPE;
	v_old_pfmly	T205_PART_NUMBER.C205_PRODUCT_FAMILY%TYPE;
	v_new_pfmly	T205_PART_NUMBER.C205_PRODUCT_FAMILY%TYPE;
	v_old_status T205_PART_NUMBER.C901_STATUS_ID%TYPE;
	v_new_status T205_PART_NUMBER.C901_STATUS_ID%TYPE;
	v_action	VARCHAR2(20);
	v_active_fl	T205_PART_NUMBER.C205_ACTIVE_FL%TYPE;
	v_system_id	T207_SET_MASTER.C207_SET_ID%TYPE;
	v_old_pmat	T205_PART_NUMBER.C205_PRODUCT_MATERIAL%TYPE;
	v_new_pmat	T205_PART_NUMBER.C205_PRODUCT_MATERIAL%TYPE;
	v_old_pclass	T205_PART_NUMBER.C205_PRODUCT_CLASS%TYPE;
	v_new_pclass	T205_PART_NUMBER.C205_PRODUCT_CLASS%TYPE;
	v_rfs_cnt	NUMBER;
	v_cmp_cnt	NUMBER;
	v_fmly_cnt  NUMBER;
	v_new_fmly_cnt NUMBER;
	v_company_id        VARCHAR2 (50) ;
	--
	v_old_track_implant VARCHAR2 (1);
    v_new_track_implant VARCHAR2 (1);
	
BEGIN

    SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual; 
	v_old_nm		:= NVL(:OLD.C205_PART_NUM_NAME,'-9999');
	v_new_nm		:= NVL(:NEW.C205_PART_NUM_NAME,'-9999');
	v_old_desc		:= NVL(:OLD.C205_PART_NUM_DESC,'-9999');
	v_new_desc		:= NVL(:NEW.C205_PART_NUM_DESC,'-9999');
	v_old_dtl_desc	:= NVL(:OLD.C205_PART_NUM_DTL_DESC,'-9999');
	v_new_dtl_desc	:= NVL(:NEW.C205_PART_NUM_DTL_DESC,'-9999');
	v_old_pfmly		:= NVL(:OLD.C205_PRODUCT_FAMILY,'-9999');
	v_new_pfmly		:= NVL(:NEW.C205_PRODUCT_FAMILY,'-9999');
	v_old_status	:= NVL(:OLD.C901_STATUS_ID,'-9999');
	v_new_status	:= NVL(:NEW.C901_STATUS_ID,'-9999'); 
	v_old_pmat	    := :OLD.C205_PRODUCT_MATERIAL;
	v_new_pmat	    := :NEW.C205_PRODUCT_MATERIAL;
	v_old_pclass	:= :OLD.C205_PRODUCT_CLASS;
	v_new_pclass	:= :NEW.C205_PRODUCT_CLASS;
	--
	v_old_track_implant := NVL(:OLD.C205_TRACKING_IMPLANT_FL, 'N');
    v_new_track_implant := NVL(:NEW.C205_TRACKING_IMPLANT_FL, 'N');	

	IF INSERTING THEN
		v_action := '103122';  -- New
	ELSIF UPDATING THEN

		IF ((v_old_nm <> v_new_nm) OR (v_old_desc <> v_new_desc) OR (v_old_dtl_desc <> v_new_dtl_desc) OR (v_old_status <> v_new_status)
		      OR(v_old_pmat <> v_new_pmat) OR (v_old_pclass <> v_new_pclass))
		THEN
			v_action := '103123';  -- Updated
		END IF;
	END IF;
	
	-- When a Part is Made Inactive, the If condition will satisfy.
	IF (:OLD.C205_ACTIVE_FL IS NOT NULL AND :NEW.C205_ACTIVE_FL IS NULL) OR
	 (:OLD.c205_release_for_sale IS NOT NULL AND :NEW.c205_release_for_sale IS NULL)--For Part Updates this condition needs to be checked and setting action
	THEN
		v_action := '4000412';  -- Voided	
		
	-- When a Part is Made active from Inactive, this condition will satisfy.
	ELSIF (:OLD.C205_ACTIVE_FL IS NULL AND :NEW.C205_ACTIVE_FL IS NOT NULL) OR
	(:OLD.c205_release_for_sale IS NULL AND :NEW.c205_release_for_sale IS NOT NULL)--For Part Updates this condition needs to be checked and setting action
	THEN
		v_action := '103123';  -- Updated
	END IF;
	
	v_system_id := get_set_id_from_part(:NEW.C205_PART_NUMBER_ID);
	
	--RFS Check
	SELECT COUNT(1) INTO v_rfs_cnt FROM T205D_PART_ATTRIBUTE WHERE C205_PART_NUMBER_ID=:NEW.C205_PART_NUMBER_ID
	and C901_ATTRIBUTE_TYPE='80180' and C205D_VOID_FL is null;
	--check part is available in GMNA company
	SELECT count(1) INTO v_cmp_cnt
	  FROM t2023_part_company_mapping 
	 WHERE c205_part_number_id = :NEW.C205_PART_NUMBER_ID
	   AND c1900_company_id = v_company_id
	   AND c2023_void_fl IS NULL;
	   
	IF (v_old_pfmly <> v_new_pfmly) THEN  
	
		SELECT count(1) INTO v_fmly_cnt
		  FROM T906_RULES
		 WHERE C906_RULE_ID     = 'PART_PROD_FAMILY'
		   AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
		   AND C906_RULE_VALUE = :OLD.c205_product_family
		   AND c906_void_fl    IS NULL;
		  
		SELECT count(1) INTO v_new_fmly_cnt
		  FROM T906_RULES
		 WHERE C906_RULE_ID     = 'PART_PROD_FAMILY'
		   AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
		   AND C906_RULE_VALUE = :NEW.c205_product_family
		   AND c906_void_fl    IS NULL;
	   
	   IF v_fmly_cnt > 0 AND v_new_fmly_cnt = 0  THEN
	   	v_action := '4000412';  -- Voided
	   ELSIF v_fmly_cnt = 0 AND v_new_fmly_cnt > 0  THEN
	    v_action := '103123';  -- Updated
	   END IF;
	   
	END IF;
	
	--When a part is not RFS to any country, then it should not be treated as an update for Product Catalog.
	IF ((v_rfs_cnt=0 and NVL(:OLD.c205_release_for_sale,'N') = NVL(:NEW.c205_release_for_sale,'N')  )OR v_cmp_cnt = 0)
	THEN
		v_action := NULL;
	END IF;
	
	
	-- Check if any of these column data is modified. If it is, then we should treat this as an update to part number.
	IF ( v_action IS NOT NULL)
	THEN
		-- 4000411 Part Number , 103097 English, 103100 Product Catalog
			gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd(:NEW.C205_PART_NUMBER_ID
							     , '4000411', v_action 
							     ,'103097','103100'
							     ,NVL(:NEW.C205_CREATED_BY,:NEW.C205_LAST_UPDATED_BY)
							     ,v_system_id
							     ,'Forceupdate');

	END IF;
	
	-- PC-3845: Based on Tracking implant to trigger and update the Bulk DO flag
	IF v_old_track_implant <> v_new_track_implant
	THEN
		-- to update the t501.c501_bulk_do_process_fl (based on part and accounts )
		gm_pkg_sm_tag_order_history.gm_process_order_bulk_do_by_part (:NEW.C205_PART_NUMBER_ID,NVL(:NEW.C205_LAST_UPDATED_BY, :NEW.C205_CREATED_BY));
	
	END IF;
	
END TRG_205_PART_NUMBER;
/