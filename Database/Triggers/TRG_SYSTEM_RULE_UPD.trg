-- @"c:\Database\Triggers\trg_system_rule_upd.trg"
CREATE OR REPLACE TRIGGER TRG_SYSTEM_RULE_UPD
	BEFORE UPDATE OF c901_status
	ON t9720_SYSTEM_RULE
	FOR EACH ROW
DECLARE
	v_old		   t9720_SYSTEM_RULE.C901_STATUS%TYPE;
	v_new		   t9720_SYSTEM_RULE.C901_STATUS%TYPE;
	v_sys_rule_clnup_log_id t9729_SYSTEM_RULE_CLEANUP_LOG.C9729_SYSTEM_RULE_CLEANUP_ID%TYPE;
	v_old_system_id		    t9720_SYSTEM_RULE.C207_SYSTEM_ID%TYPE;
	v_count		   NUMBER;
BEGIN
--
	v_old		:= :OLD.C901_STATUS;
	v_new		:= :NEW.C901_STATUS;
	v_old_system_id :=:OLD.C207_SYSTEM_ID;

--
	IF (NVL(v_old,-999) <> NVL(v_new,-999))
	THEN
		--
		IF (:OLD.c9720_history_fl IS NULL)
		THEN
		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.C207_SYSTEM_ID 
					, get_code_name(v_old)
					, 1041
					, :OLD.C9720_CREATED_BY
					, :OLD.C9720_CREATED_DATE
					 );
		END IF;
		
		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.C207_SYSTEM_ID
					, get_code_name(v_new)
					, 1041
					, :NEW.C9720_LAST_UPDATED_BY
					, :NEW.C9720_LAST_UPDATED_DATE
					 );
		--
		:NEW.c9720_history_fl := 'Y';
--
	END IF;
	 
    -- Code numbers for DRAFT and PUBLISHED
	-- Draft:     11500
	-- Published: 11501
	IF (:OLD.c901_status = 11500 AND :NEW.c901_status = 11501)
	THEN
	    -- Insert into T9729_SYSTEM_RULE_CLEANUP_LOG
	    
	    SELECT COUNT (1)
		  INTO v_count
		  FROM t9729_system_rule_cleanup_log
		 WHERE c207_system_id = v_old_system_id AND c9729_is_rule_applied_fl IS NULL AND c9729_void_fl IS NULL;

		IF (v_count = 0)
		THEN
	    
            gm_pkg_pd_proc_order_master.gm_sav_sys_rule_cleanup_log(
			:OLD.C901_STATUS,
			:OLD.C207_SYSTEM_ID,
			null, -- Part Number is null for cleanup log
			:OLD.C9720_CREATED_BY,
			SYSDATE,
		    NULL,
			NULL,
			v_sys_rule_clnup_log_id
	    );
	    END IF;

	END IF;
	 
--
END TRG_SYSTEM_RULE_UPD;
/