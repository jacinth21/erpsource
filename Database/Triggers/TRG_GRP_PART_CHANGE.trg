-- @"c:\Database\Triggers\trg_grp_part_change.trg"

CREATE OR REPLACE TRIGGER TRG_GRP_PART_CHANGE
	BEFORE DELETE OR INSERT -- OF c205_PART_NUMBER_ID
	ON T4011_GROUP_DETAIL
	FOR EACH ROW
DECLARE
	v_old		T4011_GROUP_DETAIL.c205_PART_NUMBER_ID%TYPE;
	v_new		T4011_GROUP_DETAIL.c205_PART_NUMBER_ID%TYPE;
	v_sys_rule_clnup_log_id t9729_SYSTEM_RULE_CLEANUP_LOG.C9729_SYSTEM_RULE_CLEANUP_ID%TYPE;
	v_count		   NUMBER;
BEGIN
	v_old		:= :OLD.c205_PART_NUMBER_ID;
	v_new		:= :NEW.c205_PART_NUMBER_ID;

  -- Call Package from this trigger
     IF (INSERTING)
     THEN
     
     	 SELECT COUNT (1)
		  INTO v_count
		  FROM t9729_system_rule_cleanup_log
		 WHERE C205_PART_NUMBER_ID = v_new AND c9729_is_rule_applied_fl IS NULL AND c9729_void_fl IS NULL;

		IF (v_count = 0)
		THEN
         gm_pkg_pd_proc_order_master.gm_sav_sys_rule_cleanup_log (
	     53076,--add part to group
         null, -- System ID null for group part change
	    :NEW.C205_PART_NUMBER_ID,
	    :NEW.C4011_CREATED_BY,
	     SYSDATE,
	     NULL,
	     NULL,
	     v_sys_rule_clnup_log_id
         );
         END IF;
     ELSE
     		
     	SELECT COUNT (1)
		  INTO v_count
		  FROM t9729_system_rule_cleanup_log
		 WHERE C205_PART_NUMBER_ID = v_old AND c9729_is_rule_applied_fl IS NULL AND c9729_void_fl IS NULL;

		IF (v_count = 0)
		THEN
         gm_pkg_pd_proc_order_master.gm_sav_sys_rule_cleanup_log (
         53075,-- delete part from group	
	     null, -- System ID null for group part change
	    :OLD.C205_PART_NUMBER_ID,
	    :OLD.C4011_CREATED_BY,
	     SYSDATE,
	     NULL,
	     NULL,
         v_sys_rule_clnup_log_id
         );
         END IF;
     END IF;
END trg_grp_part_change;
/