/* Formatted on 2010/08/13 11:39 (Formatter Plus v4.8.0) */

--@"C:\Database\Triggers\trg_5010_update_tag.trg";
CREATE OR REPLACE TRIGGER trg_5010_update_tag
   AFTER UPDATE
   ON t5010_tag
   FOR EACH ROW
/*****************************************************************************************
   When an update C5010_LAST_UPDATED_BY and C5010_LAST_UPDATED_DATE should not be null.
   Whenever C901_TRANS_TYPE is not null, then C901_LOCATION_TYPE in-house or distributor.
   Whenever C901_TRANS_TYPE is not null, C205_PART_NUMBER_ID or C5010_CONTROL_NUMBER should not be null.
   Whenever C901_TRANS_TYPE is not null and C5010_LAST_UPDATED_TRANS_ID should not be null.
   If C205_PART_NUMBER_ID is not null, C901_TRANS_TYPE should not be null.
   If C901_STATUS  > 51011 foloowing columns values should not be null.
      C5010_CONTROL_NUMBER
      C5010_LAST_UPDATED_TRANS_ID
      C5010_LOCATION_ID
      C205_PART_NUMBER_ID
      C901_TRANS_TYPE
      C901_LOCATION_TYPE
      C901_STATUS
************************************************************************************************/
DECLARE
	v_count		NUMBER;
	
BEGIN
	SELECT COUNT(c504_consignment_id) 
	  INTO v_count 
	  FROM t504e_consignment_in_trans 
	 WHERE c504_consignment_id = :OLD.c5010_last_updated_trans_id
	   AND c504e_void_fl is null;	   			
		
-- IF (   TO_DATE (:NEW.c5010_last_updated_date, 'DD.MM.YYYY:HH24:MI:SS') =
   --                                      TO_DATE (:OLD.c5010_last_updated_date, 'DD.MM.YYYY:HH24:MI:SS')
   IF (   :NEW.c5010_last_updated_date IS NULL
       OR :NEW.c5010_last_updated_by IS NULL
      )
   THEN
      raise_application_error
                   (-20227,
                    'Last updated date or last updated by should not be null'
                   );
   END IF;

   IF (:NEW.c205_part_number_id IS NOT NULL AND :NEW.c901_trans_type IS NULL)
   THEN
      raise_application_error (-20231, 'transcation type can not be null ');
   END IF;

   IF (:NEW.c901_status > 51011)
   THEN
      IF (   :NEW.c5010_control_number IS NULL
          OR :NEW.c5010_location_id IS NULL
          OR :NEW.c205_part_number_id IS NULL
          OR :NEW.c901_trans_type IS NULL
          OR :NEW.c901_location_type IS NULL
         )
      THEN
         raise_application_error
              (-20232,
               'control no or location id or part number should not be null '
              );
      END IF;
   END IF;

   IF (:NEW.c901_trans_type IS NOT NULL)
   THEN
      IF (    :NEW.c901_location_type <> 40033
          AND :NEW.c901_location_type <> 4120
          AND :NEW.c901_location_type <> 4123
         )
      THEN
         raise_application_error
               (-20228,
                'location type should be in-house or distributor or employee'
               );
      END IF;

      IF (:NEW.c205_part_number_id IS NULL
          OR :NEW.c5010_control_number IS NULL
         )
      THEN
         raise_application_error
                          (-20229,
                           'part number or control number should not be null'
                          );
      END IF;

      IF (    :NEW.c5010_last_updated_trans_id IS NULL
          AND :OLD.c5010_last_updated_trans_id IS NOT NULL
          AND :NEW.c901_location_type <> 4120
          AND v_count = 0 -- If the consignment intrasnit then allow to update the c5010_last_updated_trans_id is null(PMT-21804) 
         )
      THEN
         raise_application_error
                               (-20230,
                                'last updated transcation should not be null'
                               );
      END IF;
   END IF;
END;
/