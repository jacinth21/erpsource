/* This Trigger will update the part primary type log for group's,
   when changing the part primary designation from one group to another group.
   So when clicking on the Part History Icon in Group Part Mapping, 
   The history of the part Primary and Secondary type on the group's will show. 
 */

--@"C:\DATABASE\Triggers\trg_t4011_part_primary_update.trg";
CREATE OR REPLACE TRIGGER trg_t4011_part_primary_update
	AFTER UPDATE OF C901_PART_PRICING_TYPE ON 
		T4011_GROUP_DETAIL FOR EACH ROW 
DECLARE
BEGIN
	
 		-- 52092 - update
		gm_pkg_pd_group_pricing.gm_pd_sav_part_price_log (:NEW.c4011_group_detail_id
														, :NEW.c205_part_number_id
														, :NEW.c4010_group_id
														, :NEW.c901_part_pricing_type
														, 52092
														, :NEW.c4011_created_by
														, :NEW.c4011_created_date
														, :NEW.c4011_last_updated_by
														 );
--
END trg_t4011_part_primary_update;
/
