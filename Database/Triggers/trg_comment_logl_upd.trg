-- @"c:\Database\Triggers\trg_comment_logl_upd.trg"

CREATE OR REPLACE TRIGGER trg_comment_logl_upd
	BEFORE UPDATE OF C902_COMMENTS
	ON T902_LOG
	FOR EACH ROW
DECLARE
	v_old		   T902_LOG.C902_COMMENTS%TYPE;
	v_new		   T902_LOG.C902_COMMENTS%TYPE;
BEGIN
--
	v_old		:= :OLD.C902_COMMENTS;
	v_new		:= :NEW.C902_COMMENTS;

--
	IF (NVL(v_old,-999) <> NVL(v_new,-999))
	THEN
		--
		/*IF (:OLD.c4031_history_fl IS NULL)
		THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.C902_LOG_ID
													 , v_old
													 , 902
													 , :OLD.C902_LAST_UPDATED_BY
													 , :OLD.C902_LAST_UPDATED_DATE
													  );
		END IF;*/

		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.C902_LOG_ID
												 , v_old
												 , 902
												 , :NEW.C902_LAST_UPDATED_BY
												 , :NEW.C902_LAST_UPDATED_DATE
												  );
--
	END IF;
--
END trg_comment_logl_upd;
/
