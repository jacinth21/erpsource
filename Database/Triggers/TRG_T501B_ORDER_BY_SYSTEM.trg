-- Trigger for T501B_ORDER_BY_SYSTEM
-- @"c:\Database\Triggers\trg_t501b_order_by_system.trg"

CREATE OR REPLACE TRIGGER  TRG_T501B_ORDER_BY_SYSTEM  
        BEFORE UPDATE OF c501B_VOID_FL
	ON T501B_ORDER_BY_SYSTEM
	FOR EACH ROW
DECLARE
	v_old		T501B_ORDER_BY_SYSTEM.c501B_VOID_FL%TYPE;
	v_new		T501B_ORDER_BY_SYSTEM.c501B_VOID_FL%TYPE;
	v_system_log_id T501B_ORDER_BY_SYSTEM_LOG.C501B_ORDER_BY_SYSTEM_LOG_ID%TYPE;
BEGIN
	v_old		:= :OLD.c501B_VOID_FL;
	v_new		:= :NEW.c501B_VOID_FL;
v_system_log_id := 0;
  -- Call Package from this trigger
     gm_pkg_pd_proc_order_master.gm_sav_order_by_sys_log (
        :OLD.C501B_ORDER_BY_SYSTEM_ID,
	:OLD.C501_ORDER_ID,
	:OLD.C207_SYSTEM_ID,
	:OLD.C501B_QTY,
	:OLD.C501B_VOID_FL,
	:OLD.C501B_LAST_UPDATED_BY,
	:OLD.C501B_LAST_UPDATED_DATE,
        v_system_log_id
     );
END trg_order_by_system;
/