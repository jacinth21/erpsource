/* Formatted on 2009/11/15 13:10 (Formatter Plus v4.8.0) */

--@"C:\DATABASE\Triggers\trg_t7009_effective_date_upd.trg";

CREATE OR REPLACE TRIGGER trg_t7009_effective_date_upd
	BEFORE UPDATE OF c7009_effective_from_date
	ON t7009_group_price_master
	FOR EACH ROW
DECLARE
	v_old		   t7009_group_price_master.c7009_effective_from_date%TYPE;
	v_new		   t7009_group_price_master.c7009_effective_from_date%TYPE;
BEGIN
--
	v_old		:= :OLD.c7009_effective_from_date;
	v_new		:= :NEW.c7009_effective_from_date;

--
	IF (v_old <> v_new)
	THEN
		--
		IF (:OLD.c7009_effective_date_historyfl IS NULL)
		THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.c7009_group_price_master_id
													 , TO_CHAR (v_old, 'MM/DD/YYYY')
													 , 1031
													 , :OLD.c7009_created_by
													 , :OLD.c7009_created_date
													  );
			:NEW.c7009_effective_date_historyfl := 'Y'; 
		END IF;

		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.c7009_group_price_master_id
												 , TO_CHAR (v_new, 'MM/DD/YYYY')
												 , 1031
												 , :NEW.c7009_last_updated_by
												 , :NEW.c7009_last_updated_date
												  );
 
	END IF;
--
END trg_t7009_effective_date_upd;
/