--@"c:\database\triggers\T906_RULES.trg";
CREATE OR REPLACE TRIGGER trg_t906_rules BEFORE
  INSERT OR
  UPDATE ON T906_RULES FOR EACH ROW 
  DECLARE 
  
  v_old_rule_id 		t906_rules.c906_rule_id%TYPE;
  v_new_rule_id 		t906_rules.c906_rule_id%TYPE;
  v_old_rule_value  	t906_rules.c906_rule_value%TYPE;
  v_new_rule_value 		t906_rules.c906_rule_value%TYPE;
  v_old_rule_group_id 	t906_rules.c906_rule_grp_id%TYPE;
  v_new_rule_group_id 	t906_rules.c906_rule_grp_id%TYPE;
  v_old_void_fl 		t906_rules.c906_void_fl%TYPE;
  v_new_void_fl 		t906_rules.c906_void_fl%TYPE;
  v_action				VARCHAR2(20) ;
  v_cnt 				NUMBER;
  v_company_id          t906_rules.c1900_company_id%TYPE;
  BEGIN
    v_old_rule_id       := NVL(:old.c906_rule_id,'-9999');
    v_new_rule_id       := NVL(:new.c906_rule_id,'-9999');
    v_old_rule_value    := NVL(:old.c906_rule_value,'-9999');
    v_new_rule_value    := NVL(:new.c906_rule_value,'-9999');
    v_old_rule_group_id := NVL(:old.c906_rule_grp_id,'-9999');
    v_new_rule_group_id := NVL(:new.c906_rule_grp_id,'-9999');
    v_old_void_fl       := :old.c906_void_fl;
    v_new_void_fl       := :new.c906_void_fl;
     v_company_id    := :new.c1900_company_id;        
    
    /*we cannot use select statement in a table where trigger is applied. so the rule_group_id from t906_rule table
	is inserted into code lookup table.
	trigger is enabled whenever we update or insert in t906_rules table where rule_group_id is SHAREGRP*/	       
    SELECT COUNT(1) INTO v_cnt
	  FROM t901_code_lookup
     WHERE c901_code_grp = 'RULES'
       AND c901_active_fl  =1
       AND c901_code_nm    = v_new_rule_group_id;
    IF (v_cnt > 0) THEN
      IF INSERTING THEN
        v_action := '103122'; -- New
      ELSIF UPDATING THEN
        IF ( (v_old_rule_id <> v_new_rule_id) ) OR (v_old_rule_value <> v_new_rule_value) THEN
          v_action          := '103123'; -- Updated
      	END IF;
      END IF;
     
      -- When a set is voided, the If condition will satisfy.
      IF (v_old_void_fl IS NULL AND v_new_void_fl IS NOT NULL) THEN
        v_action        := '4000412'; -- Voided
        -- When a set is made active from voided, this condition will satisfy.
      ELSIF (v_old_void_fl IS NOT NULL AND v_new_void_fl IS NULL) THEN
        v_action           := '103123'; -- Updated
      END IF;
     
      IF (v_action IS NOT NULL ) THEN
        -- 103097 English, 103100 Product Catalog , 4000443 Ref Type
        gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (:NEW.c906_rule_grp_id, '4000443', v_action,
                                                       '103097', '103100',
                                                       NVL (:NEW.c906_created_by, :NEW.c906_last_updated_by),
                                                       NULL,'Y',v_company_id) ;
      END IF;
    END IF;
  END trg_t906_rules;
  /