/* Formatted on 2010/07/21 10:28 (Formatter Plus v4.8.0) */
-- @"c:\Database\Triggers\trg_t102_password_update.trg"
CREATE OR REPLACE TRIGGER trg_t102_password_update
   BEFORE UPDATE OF c102_user_password
   ON t102_user_login
   FOR EACH ROW
DECLARE
   v_old_password   t102_user_login.c102_user_password%TYPE;
   v_new_password   t102_user_login.c102_user_password%TYPE;
BEGIN
   v_old_password := :OLD.c102_user_password;
   v_new_password := :NEW.c102_user_password;

   IF (NVL (v_old_password, 0) != NVL (v_new_password, 0))
   THEN
      gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.c102_login_username,
                                                 'Y',
                                                 1036,
                                                 :NEW.c102_last_updated_by,
                                                 SYSDATE
                                                );
   END IF;
END trg_t102_password_update;
/
