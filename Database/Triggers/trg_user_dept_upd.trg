/* Formatted on 2010/07/22 11:13 (Formatter Plus v4.8.0) */
-- @"c:\Database\Triggers\trg_user_dept_upd.trg" 
CREATE OR REPLACE TRIGGER trg_user_dept_upd
	BEFORE INSERT OR UPDATE --OF c901_dept_id,c101_access_level_id
	ON T101_USER
	FOR EACH ROW
DECLARE
	-- for user edit department
	v_old_dept		   T101_USER.c901_dept_id%TYPE;
	v_new_dept		   T101_USER.c901_dept_id%TYPE;
	
	-- for user edit access level  
	v_old_acclvl		   T101_USER.c101_access_level_id%TYPE;
	v_new_acclvl		   T101_USER.c101_access_level_id%TYPE;
	v_action   VARCHAR2(20) ;
	v_dept_cnt	   NUMBER;
	v_status_cnt   NUMBER;
	v_cmp_cnt  NUMBER;
	v_old_cnt  NUMBER;
    v_new_cnt  NUMBER;
BEGIN
	v_old_dept		:= :OLD.c901_dept_id;
	v_new_dept		:= :NEW.c901_dept_id;
	
	-- for user edit access level  
	v_old_acclvl		:= :OLD.c101_access_level_id;
	v_new_acclvl		:= :NEW.c101_access_level_id;

	IF UPDATING AND (NVL(v_old_dept,-999) <> NVL(v_new_dept,-999)) AND (NVL(v_old_dept,-999) <> 2027)
	THEN
	gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.C101_USER_ID
	 					, GET_CODE_NAME_ALT(v_old_dept)
	 					, 901
						, :NEW.C101_LAST_UPDATED_BY
						, :NEW.C101_LAST_UPDATED_DATE );
	END IF;
	
	-- for user edit access level  
	IF UPDATING AND (NVL(v_old_acclvl,-999) <> NVL(v_new_acclvl,-999))
	THEN
	gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.C101_USER_ID
	 								, v_old_acclvl
	 								, 900
									, :NEW.C101_LAST_UPDATED_BY
									,:NEW.C101_LAST_UPDATED_DATE);
		IF (v_old_acclvl = 7)THEN	
			gm_pkg_sm_mapping.gm_sav_rem_acct_map(NULL,NULL,:OLD.C101_PARTY_ID,:NEW.C101_LAST_UPDATED_BY);
		END IF;
		
	END IF;

	
	SELECT COUNT(1) INTO v_dept_cnt
    FROM t906_rules
    WHERE c906_rule_id   = 'USER_DEPT'
    AND (c906_rule_value  = TO_CHAR(v_new_dept)
    OR c906_rule_value  = TO_CHAR(v_old_dept))
    AND c906_rule_grp_id ='PROD_CAT_SYNC'
    AND c906_void_fl IS NULL;
    
    SELECT COUNT(1) INTO v_status_cnt
    FROM t906_rules
    WHERE c906_rule_id   = 'USER_STATUS'
    AND (c906_rule_value = TO_CHAR(NVL(:NEW.C901_USER_STATUS,-999))
    OR c906_rule_value = TO_CHAR(NVL(:OLD.C901_USER_STATUS,-999)))
    AND c906_rule_grp_id ='PROD_CAT_SYNC'
    AND c906_void_fl IS NULL;
	
	--If sales user is updated then it will be treated as an update for MCS device app.
	IF v_dept_cnt > 0 AND v_status_cnt > 0
	THEN
	
		IF INSERTING THEN
	        v_action := '103122'; -- New
	    ELSIF UPDATING THEN
	  				IF ((:OLD.C101_USER_F_NAME <> :NEW.C101_USER_F_NAME) 
	  				        OR (:OLD.C101_USER_L_NAME <> :NEW.C101_USER_L_NAME) 
	  						OR (NVL(:OLD.C101_EMAIL_ID,'-999') <> NVL(:NEW.C101_EMAIL_ID,'-999'))
	  						OR (NVL(:OLD.C101_ACCESS_LEVEL_ID,-999) <> NVL(:NEW.C101_ACCESS_LEVEL_ID,-999))
	  						OR (NVL(:OLD.C701_DISTRIBUTOR_ID,-999) <> NVL(:NEW.C701_DISTRIBUTOR_ID,-999))
	  						OR (NVL(:OLD.C101_PARTY_ID,-999) <> NVL(:NEW.C101_PARTY_ID,-999))
	  						OR (NVL(:OLD.C901_USER_STATUS,-999) <> NVL(:NEW.C901_USER_STATUS,-999))
	  						OR (NVL(:OLD.C901_USER_TYPE,-999) <> NVL(:NEW.C901_USER_TYPE,-999))
	  						OR (NVL(v_old_dept,-999) <> NVL(v_new_dept,-999))
	  						)
	  				THEN
	  				v_action := '103123'; -- Updated
	  				END IF;
	    END IF;
	END IF;
	/*
	 * If user department removed form sales or status updated inactive then
	 * It is consider as updated as user voided to iPad.
	 */
	                      
		 SELECT COUNT (1) INTO v_old_cnt
		   FROM t906_rules
		  WHERE c906_rule_id    IN ('USER_STATUS', 'USER_DEPT')
		    AND c906_rule_grp_id = 'PROD_CAT_SYNC'
		    AND (
			    c906_rule_value = TO_CHAR(NVL(:OLD.C901_USER_STATUS,-999))
			    OR c906_rule_value   = TO_CHAR(NVL(v_old_dept,-999))
		    	)
		    AND c906_void_fl    IS NULL;
		
		 SELECT COUNT (1) INTO v_new_cnt
		   FROM t906_rules
		  WHERE c906_rule_id    IN ('USER_STATUS', 'USER_DEPT')
		    AND c906_rule_grp_id = 'PROD_CAT_SYNC'
		    AND (
			    c906_rule_value = TO_CHAR(NVL(:NEW.C901_USER_STATUS,-999))
			    OR c906_rule_value   = TO_CHAR(NVL(v_new_dept,-999))
		    	)
		    AND c906_void_fl    IS NULL;
		
		IF 	v_new_cnt <	v_old_cnt THEN
			v_action := '4000412'; -- Voided
		END IF;
		    
	    SELECT COUNT(1) INTO v_cmp_cnt
	     FROM t101_party
	     WHERE (c101_party_id = :OLD.C101_PARTY_ID OR c101_party_id = :NEW.C101_PARTY_ID)
	       AND C1900_COMPANY_ID  = 1000
	       AND C101_VOID_FL  IS NULL;
 		
	 	IF v_action IS NOT NULL AND v_cmp_cnt > 0 THEN
	        -- 103097 English, 103100 Product Catalog , 4000535 Ref Type
	        gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (:NEW.C101_USER_ID, '4000535', v_action, '103097', '103100', NVL (:NEW.c101_created_by, :NEW.c101_last_updated_by ), NULL,'Y') ;
	    END IF;
	

	
END trg_user_dept_upd;
/
