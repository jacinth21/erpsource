/* Formatted on 2009/12/15 13:20 (Formatter Plus v4.8.0) */

 --@"C:\DATABASE\Triggers\trg_7001_adjusted_price_upd.trg";
CREATE OR REPLACE TRIGGER trg_7001_adjusted_price_upd
	BEFORE UPDATE OF c7001_adjusted_price
	ON t7001_group_part_pricing
	FOR EACH ROW
DECLARE
	v_old		   t7001_group_part_pricing.c7001_adjusted_price%TYPE;
	v_new		   t7001_group_part_pricing.c7001_adjusted_price%TYPE;
BEGIN
--
	v_old		:= :OLD.c7001_adjusted_price;
	v_new		:= :NEW.c7001_adjusted_price;

--
	IF (v_old <> v_new)
	THEN
		--
		IF (:OLD.c7001_adjusted_price_his_fl IS NULL)
		THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.c7001_group_part_pricing_id
													 , v_old
													 , 1033
													 , :OLD.c7001_created_by
													 , :OLD.c7001_created_date
													  );
			:NEW.c7001_adjusted_price_his_fl := 'Y';
		END IF;

		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.c7001_group_part_pricing_id
												 , v_new
												 , 1033
												 , :NEW.c7001_last_updated_by
												 , :NEW.c7001_last_updated_date
												  );
	--

	--
--
	END IF;
--
END trg_7001_adjusted_price_upd;
/
