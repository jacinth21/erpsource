/* Formatted on 2010/04/21 16:52 (Formatter Plus v4.8.0) */
--@"C:\database\Triggers\trg_t6514_ec_incident.trg";
CREATE OR REPLACE TRIGGER trg_t6514_ec_incident
	AFTER INSERT OR UPDATE
	ON t6514_ec_incident
	FOR EACH ROW
BEGIN
	--
	INSERT INTO t6515_ec_incident_history
				(C6514_EC_INCIDENT_HISTORY_ID, c6515_form_value, c6515_linked_form_value
			   , c6515_update_required_fl, c6515_status, c6515_created_by, c6515_created_date
			   , c6514_ec_incident_id, c6513_edit_check_details_id, c621_patient_id
			   , c613_linked_study_period_id, c613_study_period_id, c623_patient_ans_list_id
			   , c901_status_reason, c6515_incident_description, c6515_void_fl
			   , c623_linked_pt_ans_list_id
				)
		 VALUES (s6515_ec_incident_history.NEXTVAL, :NEW.c6514_form_value, :NEW.c6514_linked_form_value
			   , :NEW.c6514_update_required_fl, :NEW.c6514_status, :NEW.c6514_created_by, :NEW.c6514_created_date
			   , :NEW.c6514_ec_incident_id, :NEW.c6513_edit_check_details_id, :NEW.c621_patient_id
			   , :NEW.c613_linked_study_period_id, :NEW.c613_study_period_id, :NEW.c623_patient_ans_list_id
			   , :NEW.c901_status_reason, :NEW.c6514_incident_description, :NEW.c6514_void_fl
			   , :NEW.c623_linked_pt_ans_list_id
				);
END trg_t6514_ec_incident;
--
/
