
-- @"c:\Database\Triggers\TRG_T106_ADDRESS_UPDATE.trg";

create or replace
TRIGGER TRG_T106_ADDRESS_UPDATE 
   BEFORE INSERT OR UPDATE OF C106_ADD1,C106_ADD2,C106_CITY,C901_STATE,C901_COUNTRY,C106_ZIP_CODE,
   							  C901_ADDRESS_TYPE,C106_SEQ_NO,C106_VOID_FL,C106_INACTIVE_FL,C106_PRIMARY_FL,C106_ADDRESS_ID,C901_CARRIER
   ON t106_address
   FOR EACH ROW
   
DECLARE
	v_action   VARCHAR2(20) ;
	v_cnt  NUMBER;
	v_company_id        t101_party.c1900_company_id%TYPE;

BEGIN
	BEGIN 
 	   SELECT t101.c1900_company_id 
       INTO v_company_id
       FROM t101_party t101
       WHERE t101.c101_party_id = :NEW.c101_party_id;
    EXCEPTION WHEN NO_DATA_FOUND THEN 
    v_company_id :=  get_rule_value('DEFAULT_COMPANY', 'ONEPORTAL')  ;
    END;
  
   IF (UPDATING AND( :NEW.c106_add1 IS NULL
              OR :NEW.c106_city IS NULL
              OR (:NEW.c901_state IS NULL AND v_company_id <> 1000)
              OR :NEW.c901_country IS NULL
              OR (:NEW.c106_zip_code IS NULL AND v_company_id <> 1000))
      )
   THEN
	 raise_application_error('-20535','');
   END IF;
 --  
        		:NEW.c106_GEO_LOCATION := NULL;
		        :NEW.c106_LATITUDE := NULL;
		        :NEW.c106_LONGITUDE := NULL;
		        
	IF INSERTING THEN
        v_action := '103122'; -- New
    ELSIF UPDATING THEN
  						IF (  (:OLD.C901_ADDRESS_TYPE <> :NEW.C901_ADDRESS_TYPE) OR (:OLD.C106_SEQ_NO <> :NEW.C106_SEQ_NO)
  							OR (:OLD.c106_city <> :NEW.c106_city) OR (:OLD.c901_state <> :NEW.c901_state)
  							OR (:OLD.c901_country <> :NEW.c901_country) OR (:OLD.c106_zip_code <> :NEW.c106_zip_code)
  							OR (:OLD.C106_ADD1 <> :NEW.C106_ADD1) OR (:OLD.C106_ADD2 <> :NEW.C106_ADD2)
  							OR (NVL(:OLD.C106_VOID_FL,'N') <> NVL(:NEW.C106_VOID_FL,'N') AND NVL(:NEW.C106_VOID_FL,'N') = 'N') 
     						OR (NVL(:OLD.C106_PRIMARY_FL,'N') <> NVL(:NEW.C106_PRIMARY_FL,'N') )
      						OR (NVL(:OLD.C106_INACTIVE_FL,'N') <> NVL(:NEW.C106_INACTIVE_FL,'N') AND NVL(:NEW.C106_INACTIVE_FL,'N') = 'N')
      						OR (:OLD.C901_CARRIER <> :NEW.C901_CARRIER) )
  						THEN
  						 v_action := '103123'; -- Updated
  						END IF;
      END IF;
      
 -- When address is voided, the If condition will satisfy.
      IF (NVL(:OLD.C106_VOID_FL,'N') <> NVL(:NEW.C106_VOID_FL,'N') AND NVL(:NEW.C106_VOID_FL,'N') = 'Y') 
	      OR (NVL(:OLD.C106_INACTIVE_FL,'N') <> NVL(:NEW.C106_INACTIVE_FL,'N') AND NVL(:NEW.C106_INACTIVE_FL,'N') = 'Y')
      THEN
        v_action  := '4000412'; -- Voided
      END IF;
      
      --check the address is linked with sales rep
      	   SELECT COUNT(1) INTO v_cnt
			FROM t703_sales_rep t703 
			WHERE t703.c101_party_id = :NEW.c101_party_id
			AND t703.c703_void_fl IS NULL
            AND t703.c1900_company_id = v_company_id;
      
     --If address is not linking with rep then check the address is linked with surgeon.       
     IF  v_cnt = 0  THEN    
     
		 SELECT COUNT(1) INTO v_cnt
		  FROM T101_Party T101
		  WHERE T101.C901_Party_Type = '7000' --Surgeon
		  AND T101.C101_Void_Fl     IS NULL
		  AND t101.c101_party_id = :NEW.c101_party_id;
		  
	 END IF;
	--If address is not linking with rep or surgeon then check the address is linked with activity. 
     IF  v_cnt = 0  THEN    
     	
		  SELECT COUNT(1) INTO v_cnt
			FROM t6630_activity t6630 ,
		  	t6632_activity_attribute t6632
			WHERE t6632.c6630_activity_id   = t6630.c6630_activity_id
			AND t6632.c901_attribute_type   = '7777'
			AND t6632.c6632_attribute_value = TO_CHAR(:NEW.C106_ADDRESS_ID)
			AND t6630.c6630_void_fl        IS NULL
			AND t6632.c6632_void_fl        IS NULL;
		
	 END IF; 
	 
	 --iPad update will trigger when address is linked with rep/surgeon/activity.
      IF v_action IS NOT NULL AND v_cnt > 0 THEN
        -- 103097 English, 103100 Product Catalog , 4000522 Ref Type*/
        gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (:NEW.C106_ADDRESS_ID, '4000522', v_action, '103097', '103100', NVL (:NEW.c106_created_by, :NEW.c106_last_updated_by ), NULL,'Y',v_company_id); 
      END IF;
END;    
/    