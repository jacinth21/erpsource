--@"c:\database\triggers\T107_CONTACT.trg";
CREATE OR REPLACE TRIGGER trg_t107_contact BEFORE
  INSERT OR
  UPDATE ON T107_CONTACT FOR EACH ROW DECLARE
  v_old_party_id 		t107_contact.c101_party_id%TYPE;
  v_new_party_id 		t107_contact.c101_party_id%TYPE;
  v_old_contact_id 		t107_contact.c107_contact_id %TYPE;
  v_new_contact_id 		t107_contact.c107_contact_id %TYPE;
  v_old_contact_type 	t107_contact.c107_contact_type%TYPE;
  v_new_contact_type 	t107_contact.c107_contact_type%TYPE;
  v_old_contact_value 	t107_contact.c107_contact_value%TYPE;
  v_new_contact_value 	t107_contact.c107_contact_value%TYPE;
  v_old_contact_mode 	t107_contact.c901_mode%TYPE;
  v_new_contact_mode 	t107_contact.c901_mode%TYPE;
  v_old_void_fl 		t107_contact.c107_void_fl%TYPE;
  v_new_void_fl 		t107_contact.c107_void_fl%TYPE;
  v_old_inactive_fl 	t107_contact.c107_inactive_fl%TYPE;
  v_new_inactive_fl 	t107_contact.c107_inactive_fl%TYPE;
  v_old_primary_fl 		t107_contact.c107_primary_fl%TYPE;
  v_new_primary_fl 		t107_contact.c107_primary_fl %TYPE;
  v_action              VARCHAR2(20) ;
  v_old_cnt             NUMBER;
  v_new_cnt             NUMBER;
  V_PARTY_CNT           NUMBER;
  v_company_id        	t101_party.c1900_company_id%TYPE;
  BEGIN
    v_old_party_id      := NVL(:old.c101_party_id,'-9999');
    v_new_party_id      := NVL(:new.c101_party_id,'-9999');
   	v_old_contact_id    := NVL(:old.c107_contact_id,'-9999');
    v_new_contact_id    := NVL(:new.c107_contact_id,'-9999');
    v_old_contact_type  := NVL(:old.c107_contact_type,'-9999');
    v_new_contact_type  := NVL(:new.c107_contact_type,'-9999');
    v_old_contact_value := NVL(:old.c107_contact_value ,'-9999');
    v_new_contact_value := NVL(:new.c107_contact_value,'-9999');
    v_old_contact_mode  := NVL(:old.c901_mode ,'-9999');
    v_new_contact_mode  := NVL(:new.c901_mode,'-9999');
    v_old_void_fl       := :old.c107_void_fl;
    v_new_void_fl       := :new.c107_void_fl;
    v_old_inactive_fl   := :old.c107_inactive_fl;
    v_new_inactive_fl   := :new.c107_inactive_fl;
    v_old_primary_fl    := NVL(:old.c107_primary_fl,'N');
    v_new_primary_fl    := NVL(:new.c107_primary_fl,'N');
    
     SELECT t101.c1900_company_id 
     INTO v_company_id 
     FROM  t101_party t101
     WHERE T101.c101_party_id  = v_new_party_id;

    /* v_cnt and v_party_cnt --> The trigger is enabled only if we are inserting or updating record in Contact table for Contact mode -90452(E-mail)and
      in  party table the respective party type for party id sholud be -7000(Surgeon)*/
    SELECT COUNT(1) INTO v_old_cnt
    FROM t906_rules
    WHERE c906_rule_id   = 'CONTACT_MODE'
    AND c906_rule_value  = TO_CHAR(v_old_contact_mode)
    AND c906_rule_grp_id ='PROD_CAT_SYNC'
    AND c906_void_fl is null;
    
    SELECT COUNT(1) INTO v_new_cnt
    FROM t906_rules
    WHERE c906_rule_id   = 'CONTACT_MODE'
    AND c906_rule_value  = TO_CHAR(v_new_contact_mode)
    AND c906_rule_grp_id ='PROD_CAT_SYNC'
    AND c906_void_fl is null;
    
    SELECT COUNT(1)INTO V_PARTY_CNT
    FROM T101_PARTY
    WHERE c101_party_id  = v_new_party_id
    AND c901_party_type IN
      (SELECT c906_rule_value
      FROM t906_rules
      WHERE c906_rule_id   = 'PARTY_TYPE'
      AND c906_rule_grp_id ='PROD_CAT_SYNC'
      AND c906_void_fl is null)
    and NVL(c101_active_fl,'Y') = 'Y'
    AND c101_void_fl is null;
      
    IF (V_PARTY_CNT >0) AND (v_new_cnt > 0 OR v_old_cnt > 0) THEN--v_cnt-->90452 v_party_cnt-->7000
      IF INSERTING THEN
        v_action := '103122'; -- New
      ELSIF UPDATING THEN
        IF (v_new_inactive_fl IS NULL AND v_old_inactive_fl IS NULL) AND ((v_old_party_id <> v_new_party_id) OR (v_old_contact_id <> v_new_contact_id) OR (v_old_contact_type <> v_new_contact_type)
        	OR (v_old_contact_value <> v_new_contact_value) OR (v_old_primary_fl <> v_new_primary_fl))THEN
          v_action           := '103123'; -- Updated
        END IF;
      END IF;
    
      IF (v_old_void_fl IS NULL AND v_new_void_fl IS NOT NULL)  
      OR (v_old_inactive_fl IS NULL AND v_new_inactive_fl IS NOT NULL)
      OR (v_new_cnt = 0 AND v_old_cnt > 0 AND v_new_inactive_fl IS NULL)THEN
        v_action        := '4000412'; -- Voided
        
      ELSIF (v_old_void_fl IS NOT NULL AND v_new_void_fl IS NULL) 
      OR (v_old_inactive_fl IS NOT NULL AND v_new_inactive_fl IS NULL)
      OR (v_new_cnt > 0 AND v_old_cnt = 0 AND v_new_inactive_fl IS NULL)THEN
        v_action           := '103123'; -- Updated
      END IF;
      IF (v_action IS NOT NULL ) THEN
        -- 103097 English, 103100 Product Catalog , 4000448 Ref Type
        gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (:NEW.c107_contact_id, '4000448', 
        												v_action, '103097', '103100', 
        												NVL (:NEW.c107_created_by, :NEW.c107_last_updated_by),
        												NULL,'Y',v_company_id) ;
      END IF;
    END IF;
  END trg_t107_contact;
  /