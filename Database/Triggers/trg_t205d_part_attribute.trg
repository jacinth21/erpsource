
--@"C:\DATABASE\Triggers\trg_t205d_part_attribute.trg";
/*********************************************************************
* Purpose: To add an update to the T2001_MASTER_DATA_UPDATES, if the attribute value of type 'Issuing Agency Configuration'
* of a part is modified, or if the record record is voided/unvoided. To make the changes available in IPAD
* Author: arajan
********************************************************************/

CREATE OR REPLACE TRIGGER trg_t205d_part_attribute 
	BEFORE INSERT OR UPDATE ON t205d_part_attribute 
	FOR EACH ROW
	DECLARE
		v_old_attr_value	t205d_part_attribute.c205d_attribute_value%TYPE;
		v_new_attr_value	t205d_part_attribute.c205d_attribute_value%TYPE;
		v_new_attr_type		t205d_part_attribute.c901_attribute_type%TYPE;
		v_action			VARCHAR2(20) := NULL;
		v_system_id			T207_SET_MASTER.C207_SET_ID%TYPE;
		v_cmp_cnt				NUMBER;
   BEGIN
   		v_old_attr_value		:= NVL(:OLD.C205D_ATTRIBUTE_VALUE,'-9999');
		v_new_attr_value		:= NVL(:NEW.C205D_ATTRIBUTE_VALUE,'-9999');
		v_new_attr_type			:= NVL(:NEW.C901_ATTRIBUTE_TYPE,'-9999');
        --
        /*4000539:Issuing Agency Configuration
         * If the attribute value is modified or if the record is voided/unvoided then add an update to the T2001_MASTER_DATA_UPDATES
         */
        IF 	((
				(v_old_attr_value <> v_new_attr_value) 
				OR (:OLD.C205D_VOID_FL IS NULL AND :NEW.C205D_VOID_FL IS NOT NULL)  
				OR (:OLD.C205D_VOID_FL IS NOT NULL AND :NEW.C205D_VOID_FL IS NULL)
			 )AND v_new_attr_type = 4000539)
        THEN
            v_action := '103123';  -- Updated
        END IF;
        
        v_system_id := get_set_id_from_part(:NEW.C205_PART_NUMBER_ID);
	
         --check part is available in GMNA company
			SELECT count(1) INTO v_cmp_cnt
			  FROM t2023_part_company_mapping 
			 WHERE c205_part_number_id = :NEW.C205_PART_NUMBER_ID
			   AND c1900_company_id = 1000
			   AND c2023_void_fl IS NULL;
        
        -- Check if any of these column data is modified. If it is, then we should treat this as an update to part number.
        -- 4000411 Part Number , 103097 English, 103100 Product Catalog
		IF ( v_action IS NOT NULL AND v_cmp_cnt > 0)
		THEN
	        gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd(:NEW.C205_PART_NUMBER_ID
								     , '4000411', v_action 
								     ,'103097','103100'
								     ,NVL(:NEW.C205D_CREATED_BY,:NEW.C205D_LAST_UPDATED_BY)
								     ,v_system_id
								     ,'Forceupdate'
								     );
		END IF;
        
END trg_t205d_part_attribute;
/
