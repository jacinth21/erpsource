--@"c:\database\triggers\T703_SALES_REP.trg";
CREATE OR REPLACE TRIGGER TRG_t740_gpo_account_mapping BEFORE
  INSERT OR
  UPDATE ON t740_gpo_account_mapping FOR EACH ROW 
  
  DECLARE 
  	v_old_account_id 		t740_gpo_account_mapping.c704_account_id%TYPE;
  	v_new_account_id 		t740_gpo_account_mapping.c704_account_id%TYPE;
  	v_old_partyid			t740_gpo_account_mapping.c101_party_id%TYPE;
  	v_new_partyid 			t740_gpo_account_mapping.C101_PARTY_ID%TYPE;
  	v_old_gpocatalogid 		t740_gpo_account_mapping.C740_GPO_CATALOG_ID%TYPE;
  	v_new_gpocatalogid 		t740_gpo_account_mapping.C740_GPO_CATALOG_ID%TYPE;
 	v_old_voidfl 			t740_gpo_account_mapping.c740_void_fl%TYPE;
  	v_new_voidfl 			t740_gpo_account_mapping.c740_void_fl%TYPE;
  	v_action VARCHAR2(20) ;
  	v_cmp_cnt NUMBER;
  	v_company_id        t704_account.c1900_company_id%TYPE;
  BEGIN
    v_old_account_id   := NVL(:old.c704_account_id,'-9999');
    v_new_account_id   := NVL(:new.c704_account_id,'-9999');
    v_old_partyid      := NVL(:old.c101_party_id,'-9999');
    v_new_partyid      := NVL(:new.c101_party_id,'-9999');
    v_old_gpocatalogid := NVL(:old.C740_GPO_CATALOG_ID,'-9999');
    v_new_gpocatalogid := NVL(:new.C740_GPO_CATALOG_ID,'-9999');
    v_old_voidfl       := :old.c740_void_fl;
    v_new_voidfl       := :new.c740_void_fl;
    
    select  t704.c1900_company_id 
    into v_company_id
    from t704_account t704
    where t704.c704_account_id = :NEW.c704_account_id;

    IF INSERTING THEN
      v_action := '103122'; -- New
    ELSIF UPDATING THEN
      				IF ( (v_old_account_id <> v_new_account_id) 
	      				OR (v_old_partyid <> v_new_partyid)
	      				OR (v_old_gpocatalogid <> v_new_gpocatalogid) 
	      				OR (NVL(v_old_voidfl,'N') <> NVL(v_new_voidfl,'N') AND NVL(v_new_voidfl,'N') = 'N')	)
      				THEN
      					v_action := '103123'; -- Updated
					END IF;	
    END IF;
    
    -- When a AccountGPOMap is voided, the If condition will satisfy.
    IF (NVL(v_old_voidfl,'N') <> NVL(v_new_voidfl,'N') AND NVL(v_new_voidfl,'N') = 'Y') 
    THEN
      v_action := '4000412'; -- Voided
    END IF;
    
     SELECT COUNT(1) INTO v_cmp_cnt 
	   FROM t704_account t704 
	  WHERE t704.c704_void_fl IS NULL
	    AND (c704_account_id = v_old_account_id OR c704_account_id = v_new_account_id)
        AND t704.c901_ext_country_id IS NULL  
		AND t704.c1900_company_id = v_company_id 
        AND t704.c704_active_fl = 'Y';
    
    
    
    IF (v_action IS NOT NULL AND v_cmp_cnt > 0) THEN
      -- 103097 English, 103100 Product Catalog , 4000530 Ref Type
      gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (:NEW.C740_GPO_ACCOUNT_MAP_ID, '4000530', v_action, '103097', '103100', NVL (:NEW.c740_created_by, :NEW.c740_last_updated_by ), NULL,'Y',v_company_id) ;
    END IF;
    -- the below conditon will satisfy only when Account is mapped to GPO that time account id will be null
    -- PMT-9127 replaced to :new.c101_party_id IS NOT NULL because when an account is removed(choose one) from GBP(Group price book) then AccountGpoMap update (4000530) should fire  
    IF (v_old_partyid <> v_new_partyid) AND :new.c101_party_id IS NOT NULL AND v_cmp_cnt > 0 THEN 
    
    gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd(NVL(:NEW.C101_PARTY_ID, :NEW.C704_ACCOUNT_ID), '4000524', v_action,'103097','103100'  
									,NVL(:NEW.c740_created_by, :NEW.c740_last_updated_by ), null, 'Forceupdate',v_company_id);
  
   END IF;

    
  END TRG_t740_gpo_account_mapping;
  / 