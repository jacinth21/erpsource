/* Formatted on 2009/07/22 09:51 (Formatter Plus v4.8.0) */
--@"C:\DATABASE\Triggers\trg_t9700_update.trg";

CREATE OR REPLACE TRIGGER trg_t9700_update
	BEFORE UPDATE OF c9700_expiry_date, c9700_active_fl, c9700_email_notify_fl
	ON t9700_rule
	FOR EACH ROW
DECLARE
	v_old_expiry_date t9700_rule.c9700_expiry_date%TYPE;
	v_new_expiry_date t9700_rule.c9700_expiry_date%TYPE;
	v_old_active_fl t9700_rule.c9700_active_fl%TYPE;
	v_new_active_fl t9700_rule.c9700_active_fl%TYPE;
	v_old_email_notify_fl t9700_rule.c9700_email_notify_fl%TYPE;
	v_new_email_notify_fl t9700_rule.c9700_email_notify_fl%TYPE;
BEGIN
--
	v_old_expiry_date := :OLD.c9700_expiry_date;
	v_new_expiry_date := :NEW.c9700_expiry_date;

--
	IF (v_old_expiry_date <> v_new_expiry_date)
	THEN
		--
		IF (:OLD.c9700_expiry_history_fl IS NULL)
		THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.c9700_rule_id
													 , TO_CHAR (v_old_expiry_date, 'MM/DD/YYYY')
													 , 1011
													 , :OLD.c9700_created_by
													 , :OLD.c9700_created_date
													  );
		END IF;

		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.c9700_rule_id
												 , TO_CHAR (v_new_expiry_date, 'MM/DD/YYYY')
												 , 1011
												 , :NEW.c9700_last_updated_by
												 , :NEW.c9700_last_updated_date
												  );
		--
		:NEW.c9700_expiry_history_fl := 'Y';
--
	END IF;

--
	v_old_active_fl := :OLD.c9700_active_fl;
	v_new_active_fl := :NEW.c9700_active_fl;

	IF (NVL (v_old_active_fl, 'N') <> NVL (v_new_active_fl, 'N'))
	THEN
		--
		IF (:OLD.c9700_active_history_fl IS NULL)
		THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.c9700_rule_id
													 , NVL(v_old_active_fl, 'N')
													 , 1012
													 , :OLD.c9700_created_by
													 , :OLD.c9700_created_date
													  );
		END IF;

		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.c9700_rule_id
												 , NVL(v_new_active_fl, 'N')
												 , 1012
												 , :NEW.c9700_last_updated_by
												 , :NEW.c9700_last_updated_date
												  );
		--
		:NEW.c9700_active_history_fl := 'Y';
--
	END IF;

--
	v_old_email_notify_fl := :OLD.c9700_email_notify_fl;
	v_new_email_notify_fl := :NEW.c9700_email_notify_fl;

	IF (NVL (v_old_email_notify_fl, 'N') <> NVL (v_new_email_notify_fl, 'N'))
	THEN
		--
		IF (:OLD.c9700_email_notify_history_fl IS NULL)
		THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.c9700_rule_id
													 , NVL(v_old_email_notify_fl, 'N')
													 , 1013
													 , :OLD.c9700_created_by
													 , :OLD.c9700_created_date
													  );
		END IF;

		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.c9700_rule_id
												 , NVL(v_new_email_notify_fl, 'N')
												 , 1013
												 , :NEW.c9700_last_updated_by
												 , :NEW.c9700_last_updated_date
												  );
		--
		:NEW.c9700_email_notify_history_fl := 'Y';
--
	END IF;
--
END trg_t9700_update;
