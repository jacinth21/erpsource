create or replace
TRIGGER TRG_2078_KIT_MAPPING_LOG
   BEFORE INSERT OR UPDATE
   ON T2078_KIT_MAPPING
   FOR EACH ROW
DECLARE 
BEGIN

    IF INSERTING OR  UPDATING  
    THEN
           INSERT INTO T2078A_KIT_MAPPING_LOG
                       (C2078A_KIT_MAPPING_LOG_ID, 
                       C2078_KIT_MAP_ID, 
                       C901_STATUS,C2078A_LAST_UPDATED_TRANS_ID, 
                       C2078A_LAST_TRANS_UPDATED_BY , 
                       C2078A_LAST_TRANS_UPDATED_DATE                           
                       )
               VALUES
                       (
                       S2078A_KIT_MAPPING_LOG.nextval, 
                       :new.C2078_KIT_MAP_ID, 
                       :new.C901_KIT_IN_OUT_STATUS, 
                       :new.C2078_LAST_UPDATED_TRANS_ID, 
                       :new.C2078_LAST_TRANS_UPDATED_BY, 
                       :new.C2078_LAST_TRANS_UPDATED_DATE               
                       );                                            
     END IF;
END TRG_2078_KIT_MAPPING_LOG;
/
