/* Formatted on 2018/01/16 11:26 (Formatter Plus v4.8.0) */
-- @"c:\Database\Triggers\trg_501_order_date_upd.trg"

/*
 * Trigger will save the log of the order date at everytime of order table UPDATE 
 */

CREATE OR REPLACE TRIGGER trg_501_order_date_upd
	BEFORE UPDATE OF C501_ORDER_DATE
	ON T501_ORDER
	FOR EACH ROW
DECLARE
	v_old_order_date		   T501_ORDER.C501_ORDER_DATE%TYPE;
	v_new_order_date		   T501_ORDER.C501_ORDER_DATE%TYPE;
BEGIN
--
	v_old_order_date := :OLD.C501_ORDER_DATE;
	v_new_order_date := :NEW.C501_ORDER_DATE;

--
IF UPDATING AND (NVL(v_old_order_date,CURRENT_DATE) <> NVL(v_new_order_date,CURRENT_DATE))
	THEN
		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.C501_ORDER_ID
					, to_char(v_old_order_date)
					, 304
					, :NEW.C1900_COMPANY_ID
					, :NEW.C501_LAST_UPDATED_BY
					, :NEW.C501_LAST_UPDATED_DATE );
--					
	:NEW.C501_ORDER_DATE_HIS_FL := 'Y';
	:NEW.C501_ORDER_DATE_UPDATED_DATE := CURRENT_DATE;
	:NEW.C501_ORDER_DATE_UPDATED_BY := :NEW.C501_LAST_UPDATED_BY;
	
END IF;



END trg_501_order_date_upd;
/