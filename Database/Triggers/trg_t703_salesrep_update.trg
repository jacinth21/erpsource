--@"c:\database\triggers\T703_SALES_REP.trg";
CREATE OR REPLACE TRIGGER trg_t703_salesrep_update BEFORE
  INSERT OR
  UPDATE ON T703_SALES_REP FOR EACH ROW DECLARE 
  	v_old_repid t703_sales_rep.c703_sales_rep_id%TYPE;
  	v_new_repid t703_sales_rep.c703_sales_rep_id%TYPE;
  	v_old_repname t703_sales_rep.C703_SALES_REP_NAME%TYPE;
  	v_new_repname t703_sales_rep.C703_SALES_REP_NAME%TYPE;
  	v_old_did t703_sales_rep.c701_distributor_id%TYPE;
  	v_new_did t703_sales_rep.c701_distributor_id%TYPE;
  	v_old_actvfl t703_sales_rep.c703_active_fl%TYPE;
  	v_new_actvfl t703_sales_rep.c703_active_fl%TYPE;
  	v_old_voidfl t703_sales_rep.c703_void_fl%TYPE;
  	v_new_voidfl t703_sales_rep.c703_void_fl%TYPE;
  	v_action VARCHAR2(20) ;
  	v_cnt    NUMBER;
  	v_company_id t703_sales_rep.C1900_COMPANY_ID%TYPE;
  	v_old_company_id     t703_sales_rep.C1900_COMPANY_ID%TYPE;
    v_new_company_id     t703_sales_rep.C1900_COMPANY_ID%TYPE;
  BEGIN
    v_old_repid   := NVL(:old.c703_sales_rep_id,'-9999');
    v_new_repid   := NVL(:new.c703_sales_rep_id,'-9999');
    v_old_did     := NVL(:old.c701_distributor_id,'-9999');
    v_new_did     := NVL(:new.c701_distributor_id,'-9999');
    v_old_repname := NVL(:old.C703_SALES_REP_NAME,'-9999');
    v_new_repname := NVL(:new.C703_SALES_REP_NAME,'-9999');
    v_old_actvfl  := :old.c703_active_fl;
    v_new_actvfl  := :new.c703_active_fl;
    v_old_voidfl  := :old.c703_void_fl;
    v_new_voidfl  := :new.c703_void_fl;
    v_company_id  := :new.C1900_COMPANY_ID;
    v_old_company_id     := :OLD.C1900_COMPANY_ID;
    v_new_company_id     := :NEW.C1900_COMPANY_ID;
      
       IF v_company_id IS NULL THEN
      		 v_company_id := v_old_company_id  ;
       ELSE
      		 v_company_id := v_new_company_id  ;
       END IF; 
        
    IF INSERTING THEN
      v_action := '103122'; -- New
    ELSIF UPDATING THEN
   					   IF ( (v_old_repid <> v_new_repid) OR (v_old_did <> v_new_did) OR (v_old_repname <> v_new_repname) 
   					   	OR (NVL(v_old_voidfl,'N') <> NVL(v_new_voidfl,'N') AND NVL(v_new_voidfl,'N') = 'N')
  						OR (v_old_actvfl <> v_new_actvfl AND v_new_actvfl ='Y') )
   				   THEN
       				 v_action := '103123'; -- Updated
      			   END IF;
    END IF;
       
    -- When a salesrep is voided, the If condition will satisfy.
    IF (NVL(v_old_voidfl,'N') <> NVL(v_new_voidfl,'N') AND NVL(v_new_voidfl,'N') = 'Y') 
     	 OR (v_old_actvfl <> v_new_actvfl AND v_new_actvfl ='N')
       THEN
        v_action  := '4000412'; -- Voided
      END IF;
      
    IF (v_action IS NOT NULL AND :NEW.c901_ext_country_id IS NULL AND :NEW.C1900_COMPANY_ID = v_company_id ) THEN --COMPANY ID VALUE TAKEN DYNAMICALLY FROM TABLE
      -- 103097 English, 103100 Product Catalog , 4000520 Ref Type
      gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (:NEW.c703_sales_rep_id, '4000520', v_action, '103097', '103100', NVL (:NEW.c703_created_by, :NEW.c703_last_updated_by ), NULL,'Y',v_company_id) ;
    END IF;
  END trg_t703_salesrep_update;
  /