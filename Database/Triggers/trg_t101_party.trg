--@"c:\database\triggers\T101_PARTY.trg";
CREATE OR REPLACE TRIGGER trg_t101_party BEFORE
  INSERT OR
  UPDATE ON T101_PARTY FOR EACH ROW DECLARE 
  v_old_party_id 		t101_party.c101_party_id%TYPE;
  v_new_party_id 		t101_party.c101_party_id%TYPE;
  v_old_party_type 		t101_party.c901_party_type%TYPE;
  v_new_party_type 		t101_party.c901_party_type%TYPE;
  v_old_first_name 		t101_party.c101_first_nm%TYPE;
  v_new_first_name 		t101_party.c101_first_nm%TYPE;
  v_old_last_name 		t101_party.c101_last_nm%TYPE;
  v_new_last_name 		t101_party.c101_last_nm%TYPE;
  v_old_mid_initial 	t101_party.c101_middle_initial%TYPE;
  v_new_mid_initial 	t101_party.c101_middle_initial%TYPE;
  v_old_void_fl 		t101_party.c101_void_fl%TYPE;
  v_new_void_fl 		t101_party.c101_void_fl%TYPE;
  v_old_delete_fl 		t101_party.c101_delete_fl%TYPE;
  v_new_delete_fl 		t101_party.c101_delete_fl%TYPE;
  v_old_active_fl 		t101_party.c101_active_fl%TYPE;
  v_new_active_fl 		t101_party.c101_active_fl%TYPE;
  v_company_id          t101_party.C1900_COMPANY_ID%TYPE;
  v_old_company_id     t4010_group.C1900_COMPANY_ID%TYPE;
  v_new_company_id     t4010_group.C1900_COMPANY_ID%TYPE;
  v_action 				VARCHAR2(20) ;
  v_cnt    				NUMBER;
  BEGIN
    v_old_party_id    := NVL(:old.c101_party_id,'-9999');
    v_new_party_id    := NVL(:new.c101_party_id,'-9999');
    v_old_party_type  := NVL(:old.c901_party_type,'-9999');
    v_new_party_type  := NVL(:new.c901_party_type,'-9999');
    v_old_first_name  := NVL(:old.c101_first_nm,'-9999');
    v_new_first_name  := NVL(:new.c101_first_nm,'-9999');
    v_old_last_name   := NVL(:old.c101_last_nm,'-9999');
    v_new_last_name   := NVL(:new.c101_last_nm,'-9999');
    v_old_mid_initial := :old.c101_middle_initial;
    v_new_mid_initial := :new.c101_middle_initial;
    v_old_void_fl     := :old.c101_void_fl;
    v_new_void_fl     := :new.c101_void_fl;
    v_old_delete_fl   := :old.c101_delete_fl;
    v_new_delete_fl   := :new.c101_delete_fl;
    v_old_active_fl   := NVL(:old.c101_active_fl,'Y');
    v_new_active_fl   := NVL(:new.c101_active_fl,'Y');
    v_company_id  := :new.C1900_COMPANY_ID; 
    v_old_company_id     := :OLD.C1900_COMPANY_ID;
    v_new_company_id     := :NEW.C1900_COMPANY_ID;
   
     IF v_company_id IS NULL THEN
      		 v_company_id := v_old_company_id  ;
       ELSE
      		 v_company_id := v_new_company_id  ;
       END IF; 
    /* v_cnt --> The trigger is enabled only if we are inserting or updating record in party table for party type -7000(Surgeon)*/
    SELECT COUNT(1)INTO v_cnt
    FROM t906_rules
    WHERE c906_rule_id   = 'PARTY_TYPE'
    AND c906_rule_value  = :new.c901_party_type
    AND c906_rule_grp_id ='PROD_CAT_SYNC'
    AND c906_void_fl is NULL;
    IF (v_cnt > 0) THEN
      IF INSERTING THEN
        v_action := '103122'; -- New
      ELSIF UPDATING THEN
      -- PC-3197 Receive iPad Auto Update When Party Company Changes added (v_old_company_id <> v_new_company_id)
        IF ((v_old_active_fl = 'Y' AND v_new_active_fl ='Y') AND ((v_old_party_id <> v_new_party_id) OR (v_old_party_type <> v_new_party_type) OR 
        	 (v_old_first_name <> v_new_first_name) OR (v_old_last_name <> v_new_last_name) OR 
        	  (v_old_mid_initial <> v_new_mid_initial) OR (v_old_company_id <> v_new_company_id)))THEN
          v_action := '103123'; -- Updated
        END IF;
      END IF;
      
      -- When a set is voided, the If condition will satisfy.
      IF (v_old_void_fl IS NULL AND v_new_void_fl IS NOT NULL) OR (v_old_delete_fl IS NULL AND v_new_delete_fl IS NOT NULL)
        OR (v_old_active_fl ='Y' AND v_new_active_fl = 'N')THEN
        v_action  := '4000412'; -- Voided
        -- When a set is made active from voided, this condition will satisfy.
      ELSIF (v_old_void_fl IS NOT NULL AND v_new_void_fl IS NULL) OR (v_old_delete_fl IS NOT NULL AND v_new_delete_fl IS NULL)
       OR (v_old_active_fl ='N' AND v_new_active_fl = 'Y')THEN
        v_action  := '103123'; -- Updated
      END IF;
      
      IF (v_action IS NOT NULL ) THEN
        -- 103097 English, 103100 Product Catalog , 4000445 Ref Type
        gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (:NEW.c101_party_id, '4000445', v_action, '103097', '103100', NVL (:NEW.c101_created_by, :NEW.c101_last_updated_by), NULL,'Y',v_company_id) ;
      END IF;
    END IF;
  END trg_t101_party;
  /