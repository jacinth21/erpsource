/* Formatted on 2010/02/25 16:29 (Formatter Plus v4.8.0) */

--@"C:\DATABASE\Triggers\trg_t7010_group_price_detail.trg";
CREATE OR REPLACE TRIGGER trg_t7010_group_price_detail
	AFTER INSERT
	ON t7010_group_price_detail
	FOR EACH ROW
DECLARE
	v_new_created_date t7010_group_price_detail.c7010_created_date%TYPE;
	v_group_id	   NUMBER;
	v_log_id	   NUMBER;
	v_seq_id	   NUMBER;
	v_count 	   NUMBER := 0;
	v_counter	   NUMBER := 0;
BEGIN
--
	v_new_created_date := :NEW.c7010_created_date;

/*	SELECT s7012_part_price_log_detail.NEXTVAL
	  INTO v_seq_id
	  FROM DUAL;
*/
	SELECT c4010_group_id
	  INTO v_group_id
	  FROM t7010_group_price_detail t7010
	 WHERE t7010.c7010_group_price_detail_id = :NEW.c7010_group_price_detail_id;

	SELECT MAX (c7011_part_price_log_id)
	  INTO v_log_id
	  FROM t7011_part_price_log
	 WHERE c4010_group_id = v_group_id;

	DBMS_OUTPUT.put_line ('v_log_id :  ' || v_log_id);

	SELECT COUNT (1)
	  INTO v_count
	  FROM t7011_part_price_log t7011
	 WHERE TO_CHAR (t7011.c7011_created_date, 'MM/DD/YYYY HH12:MI:SS AM') =
																TO_CHAR (v_new_created_date, 'MM/DD/YYYY HH12:MI:SS AM')
	   AND t7011.c4010_group_id = v_group_id;

	WHILE v_counter < v_count
	LOOP
		SELECT s7012_part_price_log_detail.NEXTVAL
		  INTO v_seq_id
		  FROM DUAL;

		INSERT INTO t7012_part_price_log_detail
					(c7012_part_price_log_detail_id, c7011_part_price_log_id, c901_price_type, c7012_price
					)
			 VALUES (v_seq_id, v_log_id - v_counter, :NEW.c901_price_type, :NEW.c7010_price
					);

		v_counter	:= v_counter + 1;
	END LOOP;
--
END trg_t7010_group_price_detail;
/
