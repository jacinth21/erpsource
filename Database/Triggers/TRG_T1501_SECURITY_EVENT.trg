/* Formatted on 2007/08/30 11:26 (Formatter Plus v4.8.0) */
-- @"C:\Database\Triggers\TRG_T1501_SECURITY_EVENT"

CREATE OR REPLACE TRIGGER trg_t1501_security_event
	BEFORE INSERT OR UPDATE ON t1501_group_mapping
	FOR EACH ROW
DECLARE
	v_group_id              t1501_group_mapping.c1500_group_id%TYPE;
	v_count                 number := 0;
BEGIN
        v_group_id	:= NVL(:NEW.c1500_group_id,'-9999');
	
	BEGIN				    
                  SELECT count(*) 
		    INTO v_count
		    FROM T1530_access T1530,t906_rules T906 
		   WHERE T1530.c1500_group_id = v_group_id
                     AND t1530.c1520_function_id = t906.c906_rule_id 
                     AND t906.c906_rule_grp_id = 'MOBILESECURITYEVENTS'  
		     AND c906_void_fl is null;

		  EXCEPTION WHEN NO_DATA_FOUND THEN
		    v_count := 0;
         END;

	

	     IF v_count > 0 THEN
		gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd(v_group_id
							      ,'7000443'
							      , null 
							      ,'103097'
							      ,'103100'
							      ,NVL(:NEW.C1501_CREATED_BY,:NEW.C1501_LAST_UPDATED_BY)
							      ,null
							      ,'Forceupdate'
							      ,null
							     );
	     END IF;


END trg_t1501_security_event;
/
