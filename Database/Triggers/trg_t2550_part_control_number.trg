--@"C:\Database\Triggers\trg_t2550_part_control_number.trg";
/********************************************************
	* Author      : APrasath
	* Description : Insert t2551_part_control_number_log while updating the lot status and transaction information from  trg_t2550_part_control_number.trg
*******************************************************/  
CREATE OR REPLACE
TRIGGER trg_t2550_part_control_number
   AFTER UPDATE OF c2550_lot_status,c901_last_updated_warehouse_type
   ON t2550_part_control_number
   FOR EACH ROW   
DECLARE 
BEGIN	 
		IF (:NEW.c2550_lot_status IS NOT NULL AND (NVL(:OLD.c2550_lot_status,'-999') <> :NEW.c2550_lot_status) AND :NEW.c2550_lot_status <> '105008') --105008 controlled
		  OR  (:OLD.c901_last_updated_warehouse_type <> :NEW.c901_last_updated_warehouse_type) THEN
          gm_pkg_op_lot_track_txn.gm_sav_lot_inventory_log(:NEW.c2550_part_control_number_id,
			:NEW.c205_part_number_id,
			:NEW.c2550_control_number,
			:NEW.c2550_lot_status,
			:NEW.c901_last_updated_warehouse_type,
			:NEW.c2550_last_updated_trans_id,
			:NEW.c2550_last_updated_by,			
			:NEW.c901_last_updated_trans_type,	
			:NEW.c2550_last_updated_trans_date,
			:NEW.c2550_ref_id,
			:NEW.c901_ref_type,
			:NEW.c704_account_nm,
			:NEW.c701_distributor_name,
			:NEW.c1900_company_id,
			:NEW.c5040_plant_id);	
  		END IF;
END trg_t2550_part_control_number;
/
 