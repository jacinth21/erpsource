--@"c:\Database\Triggers\trg_t504_status_log_update.trg"
	CREATE OR REPLACE TRIGGER trg_t504_status_log_update
	   AFTER INSERT OR UPDATE
	   ON t504_consignment
	   FOR EACH ROW
	DECLARE
		v_status		t901_code_lookup.c901_code_id%TYPE;
	    v_userid 		t504_consignment.c504_last_updated_by%TYPE;
	    v_status_log_fl t906_rules.c906_rule_value%TYPE;
    BEGIN
	    /*Get the status log flag for the transaction type*/
        v_status_log_fl        := get_rule_value ('STATUS_LOG', :NEW.c504_type);
        
        IF NVL(:OLD.c504_status_fl,'-9') <> NVL(:NEW.c504_status_fl,'-9') AND NVL (v_status_log_fl, 'N') = 'Y' THEN
        	 /* Get status log code id from rule table based on txn type and txn status.
        	  * Because,some txns may have exception status.The exceptional status txns are defined in the rule table.
        	  * Other txns will get the below default status logs.
        	  * Get the code id for the appropriate status
        	  * 93004	Initiated
        	  * 93010	Controlled
        	  * 93006	Completed */
             SELECT NVL(get_rule_value (:NEW.c504_status_fl, :NEW.c504_type),DECODE (:NEW.c504_status_fl, '2', 93004, '3', 93111, '4', 93006,NULL))
                  , NVL (:NEW.c504_last_updated_by, :NEW.c504_created_by)
               INTO v_status, v_userid
               FROM DUAL;
            IF (v_status IS NOT NULL) THEN
                gm_save_status_details (:NEW.c504_consignment_id, '', v_userid, NULL, SYSDATE, v_status, 91108) ; --91108	Inhouse Loaner Items type
            END IF;
        END IF;
    END trg_t504_status_log_update;
/    