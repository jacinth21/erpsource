/* Formatted on 2012/02/09 18:05 (Formatter Plus v4.8.0) */
--@"C:\DATABASE\Triggers\trg_t2060_di_part_mapping_upd.trg";
CREATE OR REPLACE TRIGGER trg_t2060_di_part_mapping_upd
	BEFORE UPDATE ON t2060_di_part_mapping
	FOR EACH ROW
	DECLARE

   BEGIN
        --
        IF (:OLD.c2060_history_fl IS NULL) THEN
            gm_pkg_pd_sav_udi.gm_sav_di_part_map_log (:OLD.C2060_DI_PART_MAPPING_ID, :OLD.C2060_DI_NUMBER,
            :OLD.C205_PART_NUMBER_ID, :OLD.C901_STATUS, :OLD.c2060_set_uuid, :OLD.c2060_model_uuid,
            :OLD.c2060_last_updated_by, :OLD.c2060_last_updated_date) ;
        END IF;
        --
        gm_pkg_pd_sav_udi.gm_sav_di_part_map_log (:NEW.C2060_DI_PART_MAPPING_ID, :NEW.C2060_DI_NUMBER,
        :NEW.C205_PART_NUMBER_ID, :NEW.C901_STATUS, :NEW.c2060_set_uuid, :NEW.c2060_model_uuid,
        :NEW.c2060_last_updated_by, :NEW.c2060_last_updated_date) ;
        --
        :NEW.c2060_history_fl := 'Y';
        --
END trg_t2060_di_part_mapping_upd;
/
