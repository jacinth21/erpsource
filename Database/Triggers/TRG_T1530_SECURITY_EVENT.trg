/* Formatted on 2007/08/30 11:26 (Formatter Plus v4.8.0) */
-- @"C:\Database\Triggers\TRG_T1530_SECURITY_EVENT.trg"

CREATE OR REPLACE
TRIGGER trg_t1530_security_event
	BEFORE INSERT OR UPDATE ON T1530_ACCESS
	FOR EACH ROW
DECLARE
	v_sec_event_name	t1530_access.c1520_function_id%TYPE;
	v_group_id   	        t1530_access.c1500_group_id%TYPE;
	v_count                 number :=0;
BEGIN
        v_sec_event_name	:= NVL(:NEW.c1520_function_id,'-9999');
        v_group_id              := NVL(:NEW.c1500_group_id,'-9999');
    BEGIN
         SELECT count(*) 
           INTO v_count
          FROM  t906_rules  
         WHERE c906_rule_grp_id = 'MOBILESECURITYEVENTS' 
           AND c906_rule_id = v_sec_event_name
           AND c906_void_fl is null;
         EXCEPTION WHEN NO_DATA_FOUND THEN
	  v_count := 0;
     END;

     IF v_count > 0 THEN
        gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd(v_group_id
						      , '7000443'
						      , null 
						      ,'103097'
						      ,'103100'
						      ,NVL(:NEW.C1530_CREATED_BY,:NEW.C1530_LAST_UPDATED_BY)
						      ,null
						      ,'Forceupdate'
						      ,null
						      );
     END IF;

END trg_t1530_security_event;
/
