--@"C:\Database\Triggers\TRG_T205F_PPPQ_TYPE_UPD.trg";
CREATE OR REPLACE 
TRIGGER TRG_T205F_PPPQ_TYPE_UPD BEFORE
     UPDATE OF C901_TYPE ON T205F_PART_PROCESS_VALIDATION FOR EACH ROW 
 DECLARE 
    v_old T205F_PART_PROCESS_VALIDATION.C901_TYPE%TYPE;
    v_new T205F_PART_PROCESS_VALIDATION.C901_TYPE%TYPE;
    BEGIN
        --
        v_old := :OLD.C901_TYPE;
        v_new := :NEW.C901_TYPE;
        --
        IF (v_old <> v_new) THEN
            --
            IF (:OLD.C205F_HISTORY_FL IS NULL) THEN
                gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.C205F_PROCESS_VALIDATION_ID, get_code_name(:OLD.C901_TYPE), 1050,
                :OLD.C205F_LAST_UPDATED_BY, :OLD.C205F_LAST_UPDATED_DATE) ;
                
            :NEW.C205F_HISTORY_FL := 'Y';
            --
            END IF;
            gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.C205F_PROCESS_VALIDATION_ID, get_code_name(:NEW.C901_TYPE), 1050,
            :NEW.C205F_LAST_UPDATED_BY, :NEW.C205F_LAST_UPDATED_DATE) ;
            --
            
        END IF;
        --
END TRG_T205F_PPPQ_TYPE_UPD;
/