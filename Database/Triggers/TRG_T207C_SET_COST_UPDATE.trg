--@"C:\Database\Triggers\TRG_T207C_SET_COST_UPDATE.trg";
/*
---------------------------------------------------------------------------
Purpose: To log updates from the Set Attribute Table
Logic Structure: Everytime the Cost is updated, then a new row is inserted
-----------------------------------------------------------------------------
*/ 
CREATE OR REPLACE TRIGGER TRG_T207C_SET_COST_UPDATE BEFORE 
    INSERT OR UPDATE OF c207c_attribute_value ON t207c_set_attribute FOR EACH ROW 
    DECLARE 
    BEGIN 
    
    IF (:OLD.c207c_set_history_fl IS NULL AND :OLD.c901_attribute_type = 11210) 
    THEN
        gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (	:OLD.c207_set_id,:NEW.C207C_ATTRIBUTE_VALUE ,1048,:NEW.c207c_last_updated_by,:NEW.c207c_last_updated_date);
    	:NEW.C207C_SET_HISTORY_FL := 'Y' ;    											  
    ELSIF (:NEW.c901_attribute_type = 11210) 
    THEN
    	gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (	:NEW.c207_set_id,:NEW.C207C_ATTRIBUTE_VALUE ,1048,:NEW.c207c_last_updated_by,:NEW.c207c_last_updated_date);
	END IF;
	
END TRG_T207C_SET_COST_UPDATE;
/
