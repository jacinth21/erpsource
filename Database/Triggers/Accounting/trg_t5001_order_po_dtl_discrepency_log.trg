--@"c:\database\triggers\accounting\trg_t5001_order_po_dtl_discrepency_log.trg";
--This trigger is  used to save updated log history for Category, Collector and Resolution Details PC-1823
CREATE OR REPLACE TRIGGER trg_t5001_order_po_dtl_discrepency_log BEFORE
    UPDATE OF c901_collector_id, c901_po_disc_category_type, c901_po_disc_resolution_type ON t5001_order_po_dtls
    FOR EACH ROW
DECLARE
    v_po_dtl_id             t5001_order_po_dtls.c5001_order_po_dtls_id%TYPE;
    v_old_collector_id      t5001_order_po_dtls.c901_collector_id%TYPE;
    v_new_collector_id      t5001_order_po_dtls.c901_collector_id%TYPE;
    v_old_category_id       t5001_order_po_dtls.c901_po_disc_category_type%TYPE;
    v_new_category_id       t5001_order_po_dtls.c901_po_disc_category_type%TYPE;
    v_old_resolution_id     t5001_order_po_dtls.c901_po_disc_resolution_type%TYPE;
    v_new_resolution_id     t5001_order_po_dtls.c901_po_disc_resolution_type%TYPE;
    v_old_collector_name    t901_code_lookup.c901_code_nm%TYPE;
    v_new_collector_name    t901_code_lookup.c901_code_nm%TYPE;
    v_old_category_name     t901_code_lookup.c901_code_nm%TYPE;
    v_new_category_name     t901_code_lookup.c901_code_nm%TYPE;
    v_old_resolution_name   t901_code_lookup.c901_code_nm%TYPE;
    v_new_resolution_name   t901_code_lookup.c901_code_nm%TYPE;
    v_user_id               t5001_order_po_dtls.c5001_last_updated_by%TYPE;
BEGIN
    v_po_dtl_id := :old.c5001_order_po_dtls_id;
    v_old_collector_id := nvl(:old.c901_collector_id, -999);
    v_new_collector_id := nvl(:new.c901_collector_id, -999);
    v_old_category_id := nvl(:old.c901_po_disc_category_type, -999);
    v_new_category_id := nvl(:new.c901_po_disc_category_type, -999);
    v_old_resolution_id := nvl(:old.c901_po_disc_resolution_type, -999);
    v_new_resolution_id := nvl(:new.c901_po_disc_resolution_type, -999);
    v_user_id := :new.c5001_last_updated_by;
	
	
	
	-- Check the old collector name and new collector name(Collector History Details Save)
    IF v_old_collector_id <> v_new_collector_id THEN
		 SELECT
                get_code_name(v_old_collector_id),
                get_code_name(v_new_collector_id)
            INTO
                v_old_collector_name,
                v_new_collector_name
            FROM
                dual;
		-- Check the old collector name is not null and old collector history flag is null
        IF v_old_collector_id <> -999 AND :old.c5001_collecoter_history_fl IS NULL THEN
           
			-- used to save the old log detail for collector  
			-- 109661-- Collector History type

            gm_pkg_ar_po_trans.gm_sav_po_details_log(v_po_dtl_id, 109661, v_old_collector_id, v_old_collector_name, v_user_id); 
			-- used to save the new log detail for collector	
            gm_pkg_ar_po_trans.gm_sav_po_details_log(v_po_dtl_id, 109661, v_new_collector_id, v_new_collector_name, v_user_id);
			-- update history Flag
            :new.c5001_collecoter_history_fl := 'Y'; 
			
		-- Check the old collector history flag is equat to 'Y'	
        ELSIF :old.c5001_collecoter_history_fl = 'Y' THEN
		    -- used to save the new log detail for collector
            gm_pkg_ar_po_trans.gm_sav_po_details_log(v_po_dtl_id, 109661, v_new_collector_id, v_new_collector_name, v_user_id);
        END IF;
    END IF;
	
	-- Check the old Category name and new Category name(Category History Details Save)

    IF v_old_category_id <> v_new_category_id THEN
        SELECT
            get_code_name(v_old_category_id),
            get_code_name(v_new_category_id)
        INTO
            v_old_category_name,
            v_new_category_name
        FROM
            dual;
		
	     -- Check the old category name is not null and old category history flag is null

        IF v_old_category_id <> -999 AND :old.c5001_po_disc_category_history_fl IS NULL THEN
			-- used to save the old log detail for category	
			-- 109662-- Category History type	
            gm_pkg_ar_po_trans.gm_sav_po_details_log(v_po_dtl_id, 109662, v_old_category_id, v_old_category_name, v_user_id);
				-- used to save the new log detail for category	
            gm_pkg_ar_po_trans.gm_sav_po_details_log(v_po_dtl_id, 109662, v_new_category_id, v_new_category_name, v_user_id);
			-- update history Flag
            :new.c5001_po_disc_category_history_fl := 'Y';
			
			-- Check the old category history flag is equat to 'Y'	
        ELSIF :old.c5001_po_disc_category_history_fl = 'Y' THEN
		 -- used to save the new log detail for category
            gm_pkg_ar_po_trans.gm_sav_po_details_log(v_po_dtl_id, 109662, v_new_category_id, v_new_category_name, v_user_id);
        END IF;

    END IF;
	
	-- Check the old resolution name and new resolution name(Resolution History Details Save)

    IF v_old_resolution_id <> v_new_resolution_id THEN
		 SELECT
                get_code_name(v_old_resolution_id),
                get_code_name(v_new_resolution_id)
            INTO
                v_old_resolution_name,
                v_new_resolution_name
            FROM
                dual;
		-- Check the old resolution name is not null and old resolution history flag is null
        IF v_old_resolution_id <> -999 AND :old.c5001_po_disc_resolution_history_fl IS NULL THEN
           
			-- used to save the old log detail for resolution	
			-- 109663-- Resolution History type		

            gm_pkg_ar_po_trans.gm_sav_po_details_log(v_po_dtl_id, 109663, v_old_resolution_id, v_old_resolution_name, v_user_id);
			-- used to save the new log detail for resolution	
            gm_pkg_ar_po_trans.gm_sav_po_details_log(v_po_dtl_id, 109663, v_new_resolution_id, v_new_resolution_name, v_user_id);
			-- update history Flag
            :new.c5001_po_disc_resolution_history_fl := 'Y';
			-- Check the old resolution history flag is equat to 'Y'
        ELSIF :old.c5001_po_disc_resolution_history_fl = 'Y' THEN
			-- used to save the new log detail for resolution
            gm_pkg_ar_po_trans.gm_sav_po_details_log(v_po_dtl_id, 109663, v_new_resolution_id, v_new_resolution_name, v_user_id);
        END IF;
    END IF;

END trg_t5001_order_po_dtl_discrepency_log;
/