-- @"c:\database\triggers\Accounting\trg_t501_cust_po_dtls_update.trg";
CREATE OR REPLACE TRIGGER trg_t501_cust_po_dtls_update BEFORE
     INSERT OR UPDATE OF c501_customer_po, c501_po_amount ON t501_order
	   FOR EACH ROW
     DECLARE BEGIN
        /*if po is entered the first time than customer po entered date should get updated.
        if the po is edited or revenue sampling information is changed then po entered date should not be updated.
        if the po is removed then the customer po entered date should be removed.
        */
        IF (NVL(:old.c501_customer_po, '-999') <> NVL(:new.c501_customer_po, '-999') OR NVL(:old.c501_po_amount, '-999') <> NVL(:new.c501_po_amount, '-999'))
        THEN
		-- Save/Update the PO Status if PO Amount is entered from A/R PO Entry Screen
        gm_pkg_ar_po_trans.gm_sav_order_po_process_dtsl (:new.c501_order_id, :new.c704_account_id, :new.c501_parent_order_id, :old.c501_customer_po , :new.c501_customer_po, :new.c501_po_amount, NVL(:new.c501_last_updated_by,:new.c501_created_by));
        
		END IF;
		
END trg_t501_cust_po_dtls_update;
/
