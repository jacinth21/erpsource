--@"c:\database\triggers\accounting\trg_t2706_counted_by_update.trg";
CREATE OR REPLACE TRIGGER trg_t2706_counted_by_update before
     UPDATE OF c101_counted_by ON t2706_cycle_count_lock_dtls FOR EACH row
     DECLARE
     --
     v_old_counted_by  t2706_cycle_count_lock_dtls.c101_counted_by%type;
     v_new_counted_by t2706_cycle_count_lock_dtls.c101_counted_by%type;
     --
     v_lock_id t2705_cycle_count_lock.C2705_CYC_CNT_LOCK_ID%TYPE;
     v_lock_status NUMBER;
    BEGIN
        --
        v_old_counted_by := :OLD.c101_counted_by;
        v_new_counted_by := :NEW.c101_counted_by;
        --
        v_lock_id := :OLD.C2705_CYC_CNT_LOCK_ID;
        --
        select c901_lock_status INTO v_lock_status FROM t2705_cycle_count_lock WHERE c2705_cyc_cnt_lock_id = v_lock_id;
        --
        -- AND v_old_counted_by <> v_new_counted_by
        IF (v_lock_status = 106830 AND v_old_counted_by          IS NOT NULL) THEN
        	-- to update the history flag
        	IF (v_old_counted_by = v_new_counted_by)
        	THEN
        		raise_application_error (-20999, 'Recount cannot be done by the same person');
        	END IF;
        	--
            IF (:OLD.c2706_history_fl IS NULL) THEN
                gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.c2706_cyc_cnt_lock_dtls_id, :OLD.c2706_physical_qty,
                1181, :OLD.c101_counted_by, :OLD.c2706_counted_date) ;
                :NEW.c2706_history_fl := 'Y';
            END IF; -- end if history flag
            --
            gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.c2706_cyc_cnt_lock_dtls_id, :NEW.c2706_physical_qty,
            1181, :NEW.c101_counted_by, :NEW.c2706_counted_date) ;
            --
            :NEW.c2706_recount_fl := NULL;
         END IF; -- end if counted by check   
            --
        END trg_t2706_counted_by_update;
        /