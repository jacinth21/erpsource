--@"c:\database\triggers\accounting\trg_t5003_order_revenue_log.trg";
--This trigger is  used to save updated log history for PO and DO Details PMT-53710
CREATE OR REPLACE TRIGGER trg_t5003_order_revenue_log BEFORE
	  UPDATE OF c901_po_dtls,
	  c901_do_dtls ON t5003_order_revenue_sample_dtls FOR EACH ROW DECLARE v_old_po_dtls  T901_CODE_LOOKUP.C901_CODE_NM%TYPE;
	  v_old_do_dtls                                                                       T901_CODE_LOOKUP.C901_CODE_NM%TYPE;
	  v_new_po_dtls                                                                       T901_CODE_LOOKUP.C901_CODE_NM%TYPE;
	  v_new_do_dtls                                                                       T901_CODE_LOOKUP.C901_CODE_NM%TYPE;
	  v_po_trail_id t941_audit_trail_log.c940_audit_trail_id%TYPE;
	  v_do_trail_id t941_audit_trail_log.c940_audit_trail_id%TYPE;
	  v_order_id t5003_order_revenue_sample_dtls.c501_order_id%TYPE;
	  BEGIN
	    v_old_po_dtls := :OLD.c901_po_dtls;
	    v_old_do_dtls := :OLD.c901_do_dtls;
	    v_new_po_dtls := :NEW.c901_po_dtls;
	    v_new_do_dtls := :NEW.c901_do_dtls;
	    v_order_id    := :OLD.c501_order_id;
	    v_po_trail_id := 1241; -- Audit id for PO
	    v_do_trail_id := 1242; -- Audit id for DO
	    -- Check the new po details not equal to old po
	    IF NVL (v_old_po_dtls, 0) <> NVL (v_new_po_dtls, 0) THEN
		      SELECT get_code_name (v_old_po_dtls),
		        get_code_name (v_new_po_dtls)
		      INTO v_old_po_dtls,
		        v_new_po_dtls
		      FROM DUAL;
	      -- save the po details in audit log based on history flag
	      IF (:OLD.c5003_po_history_fl IS NULL) AND (:OLD.c5003_po_created_date IS NOT NULL) THEN
	        gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (v_order_id , v_old_po_dtls , v_po_trail_id , :OLD.c5003_po_created_by , :OLD.c5003_po_created_date );
	        gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (v_order_id , v_new_po_dtls , v_po_trail_id , :NEW.c5003_po_last_updated_by , :NEW.c5003_po_last_updated_date );
			:NEW.c5003_po_history_fl := 'Y';
		  ELSIF :OLD.c5003_po_created_date IS NOT NULL THEN
			 gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (v_order_id , v_new_po_dtls , v_po_trail_id , :NEW.c5003_po_last_updated_by , :NEW.c5003_po_last_updated_date );
	      END IF;
	    END IF;
	    --Check the new do details not equal to old do
	    IF NVL (v_old_do_dtls, 0) <> NVL (v_new_do_dtls, 0) THEN
		      SELECT get_code_name (v_old_do_dtls),
		        get_code_name (v_new_do_dtls)
		      INTO v_old_do_dtls,
		        v_new_do_dtls
		      FROM DUAL;
	      -- save the Do details in audit log based on history flag
	      IF (:OLD.c5003_do_history_fl IS NULL) AND (:OLD.c5003_do_created_date IS NOT NULL) THEN
	        gm_pkg_cm_audit_trail.gm_cm_sav_audit_log ( v_order_id , v_old_do_dtls , v_do_trail_id , :OLD.c5003_do_created_by , :OLD.c5003_do_created_date );
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log ( v_order_id , v_new_do_dtls , v_do_trail_id , :NEW.c5003_do_last_updated_by , :NEW.c5003_do_last_updated_date );
		      :NEW.c5003_do_history_fl := 'Y';
		  ELSIF :OLD.c5003_do_created_date IS NOT NULL THEN
			 gm_pkg_cm_audit_trail.gm_cm_sav_audit_log ( v_order_id , v_new_do_dtls , v_do_trail_id , :NEW.c5003_do_last_updated_by , :NEW.c5003_do_last_updated_date );
		  END IF;
	     
	    END IF;
  END trg_t5003_order_revenue_log;
  /
