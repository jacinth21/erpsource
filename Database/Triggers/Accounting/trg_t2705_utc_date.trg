/*
 * Trigger will last updated UTC everytime insert/update.
 */

CREATE OR REPLACE TRIGGER trg_t2705_utc_date
   BEFORE INSERT OR UPDATE
   ON t2705_cycle_count_lock    
   FOR EACH ROW
DECLARE
BEGIN
	:NEW.c2705_last_updated_date_utc := SYSTIMESTAMP;
	
END trg_t2705_utc_date;
/