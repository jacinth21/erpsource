/*
 * Trigger will last updated UTC everytime insert/update.
 */

CREATE OR REPLACE TRIGGER trg_t2703_utc_date
   BEFORE INSERT OR UPDATE
   ON t2703_cycle_count_rank    
   FOR EACH ROW
DECLARE
BEGIN
	:NEW.c2703_last_updated_date_utc := SYSTIMESTAMP;
	
END trg_t2703_utc_date;
/