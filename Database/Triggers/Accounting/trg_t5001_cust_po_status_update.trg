-- @"c:\database\triggers\Accounting\trg_t5001_cust_po_status_update.trg";
-- Trigger is used to Save/update PO Status
-- while changing the PO status from a/r report screen

CREATE OR REPLACE TRIGGER trg_t5001_cust_po_status_update
   BEFORE UPDATE OF c901_cust_po_status
   ON t5001_order_po_dtls
   FOR EACH ROW
DECLARE
   v_old_po_status  t5002_order_po_dtls_log.c5002_po_dtls_value%TYPE;
   v_new_po_status  t5002_order_po_dtls_log.c5002_po_dtls_value%TYPE;
BEGIN
   -- check old po status and new po status
   IF (:old.c901_cust_po_status <> :new.c901_cust_po_status)
   THEN
      SELECT get_code_name (:old.c901_cust_po_status),
             get_code_name (:new.c901_cust_po_status)
        INTO v_old_po_status, v_new_po_status
        FROM DUAL;
      -- Check old history flag
      IF (:old.c5001_po_status_history_fl IS NULL)
      THEN
         -- Save/update PO Details and PO Status
         gm_pkg_ar_po_trans.gm_sav_po_details_log (
            :old.c5001_order_po_dtls_id,
            109660,
            :old.c901_cust_po_status,
            v_old_po_status,
            :old.c5001_po_status_updated_by);
        -- updating history flag
         :new.c5001_po_status_history_fl := 'Y';
      END IF;

      -- Save/update PO Details and PO Status
      gm_pkg_ar_po_trans.gm_sav_po_details_log (
         :new.c5001_order_po_dtls_id,
         109660,
         :new.c901_cust_po_status,
         v_new_po_status,
         :new.c5001_po_status_updated_by);
   END IF;
END trg_t5001_cust_po_status_update;
/
