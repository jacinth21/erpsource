/*
 * Trigger will last updated UTC everytime insert/update.
 */

CREATE OR REPLACE TRIGGER trg_t2707_utc_date
   BEFORE INSERT OR UPDATE
   ON t2707_cycle_count_txn_ref    
   FOR EACH ROW
DECLARE
BEGIN
	:NEW.c2707_last_updated_date_utc := SYSTIMESTAMP;
	
END trg_t2707_utc_date;
/