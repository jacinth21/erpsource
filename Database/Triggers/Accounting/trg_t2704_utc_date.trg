/*
 * Trigger will last updated UTC everytime insert/update.
 */

CREATE OR REPLACE TRIGGER trg_t2704_utc_date
   BEFORE INSERT OR UPDATE
   ON t2704_cycle_count_schedule    
   FOR EACH ROW
DECLARE
BEGIN
	:NEW.c2704_last_updated_date_utc := SYSTIMESTAMP;
	
END trg_t2704_utc_date;
/