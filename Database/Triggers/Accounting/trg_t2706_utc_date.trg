/*
 * Trigger will last updated UTC everytime insert/update.
 */

CREATE OR REPLACE TRIGGER trg_t2706_utc_date
   BEFORE INSERT OR UPDATE
   ON t2706_cycle_count_lock_dtls    
   FOR EACH ROW
DECLARE
BEGIN
	:NEW.c2706_last_updated_date_utc := SYSTIMESTAMP;
	
END trg_t2706_utc_date;
/