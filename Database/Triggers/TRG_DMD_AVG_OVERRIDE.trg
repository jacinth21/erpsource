/* Formatted on 2013/07/03  */
--@"c:\database\triggers\TRG_DMD_AVG_OVERRIDE.trg";

CREATE OR REPLACE TRIGGER TRG_DMD_AVG_OVERRIDE
	BEFORE UPDATE OF C4032_QTY
	ON T4032_DEMAND_OVERRIDE
	FOR EACH ROW
DECLARE
	v_old		   T4032_DEMAND_OVERRIDE.C4032_QTY%TYPE;
	v_new		   T4032_DEMAND_OVERRIDE.C4032_QTY%TYPE;
BEGIN
--
	v_old		:= :OLD.C4032_QTY;
	v_new		:= :NEW.C4032_QTY;

--
	IF (NVL(v_old,-9999) <> NVL(v_new,-9999))
	THEN
		--
		IF (:OLD.C4032_HISTORY_FL IS NULL)
		THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.C4032_OVERRIDE_ID
													 , v_old
													 , 1054
													 , NVL(:OLD.C4032_LAST_UPDATED_BY,:OLD.C4032_CREATED_BY)
													 ,  NVL(:OLD.C4032_LAST_UPDATED_DATE,:OLD.C4032_CREATED_DATE)
													  );
		END IF;

		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.C4032_OVERRIDE_ID
												 , v_new
												 , 1054
												 , :NEW.C4032_LAST_UPDATED_BY
												 , :NEW.C4032_LAST_UPDATED_DATE
												  );
		--
		:NEW.C4032_HISTORY_FL := 'Y';
--
	END IF;
--
END TRG_DMD_AVG_OVERRIDE;
/