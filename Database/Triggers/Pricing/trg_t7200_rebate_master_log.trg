--@"c:\database\triggers\trg_t7200_rebate_master_log.trg";
CREATE OR REPLACE TRIGGER trg_t7200_rebate_master_log
	BEFORE UPDATE OF c901_rebate_part_type, c901_rebate_category
		, c7200_rebate_rate , c7200_reb_eff_dt ON t7200_rebate_master
		
	FOR EACH ROW
	
	DECLARE 
	
	v_old_rebate_part_type t7200_rebate_master.c901_rebate_part_type%TYPE;
    v_new_rebate_part_type t7200_rebate_master.c901_rebate_part_type%TYPE;
    --
    v_old_rebate_category t7200_rebate_master.c901_rebate_category%TYPE;
    v_new_rebate_category t7200_rebate_master.c901_rebate_category%TYPE;
    --
    v_old_rebate_rate t7200_rebate_master.c7200_rebate_rate%TYPE;
    v_new_rebate_rate t7200_rebate_master.c7200_rebate_rate%TYPE;
    --
    v_old_rebate_eff_dt t7200_rebate_master.c7200_reb_eff_dt%TYPE;
    v_new_rebate_eff_dt t7200_rebate_master.c7200_reb_eff_dt%TYPE;
    --
    v_part_type_num     NUMBER := 107702; -- Rebate part type
    v_category_type_num NUMBER := 107703; -- Rebate Category
    v_rate_num          NUMBER := 107704; -- Rebate rate
    v_effective_num     NUMBER := 107705; -- Rebate effective date
    --
    
    v_date_fmt VARCHAR2 (20) ;
    
    BEGIN
	    
        /*
        * This trigger used to log audit trail for Part type, Category, Rate and effecitv date.
        * 1. While change the values in rebate master setup screen - to capture log (who modified - what values changes
        information)
        *
        */
	    
        v_old_rebate_part_type := NVL (:old.c901_rebate_part_type, '-9999') ;
        v_new_rebate_part_type := NVL (:new.c901_rebate_part_type, '-9999') ;
        --
        v_old_rebate_category := NVL (:old.c901_rebate_category, '-9999') ;
        v_new_rebate_category := NVL (:new.c901_rebate_category, '-9999') ;
        --
        v_old_rebate_rate := NVL (:old.c7200_rebate_rate, 0) ;
        v_new_rebate_rate := NVL (:new.c7200_rebate_rate, 0) ;
        --
        v_old_rebate_eff_dt := :old.c7200_reb_eff_dt;
        v_new_rebate_eff_dt := :new.c7200_reb_eff_dt;
        
        -- 1. Rebate Party type compare and insert to Log table
        
        IF v_old_rebate_part_type <> v_new_rebate_part_type THEN
            --
            IF (:OLD.c7200_rebate_part_type_fl IS NULL) --if first time insert of record
                THEN
                
                -- to log the data to T740a table
                gm_pkg_sm_rebate_setup.gm_sav_rebate_master_log (:old.c7200_rebate_id, v_part_type_num, get_code_name (
                :old.c901_rebate_part_type), :old.c7200_last_updated_by, :old.c7200_last_updated_dt) ;
                
                -- update the flag
                
                :NEW.c7200_rebate_part_type_fl := 'Y';
                
            END IF;
            
            gm_pkg_sm_rebate_setup.gm_sav_rebate_master_log (:new.c7200_rebate_id, v_part_type_num, get_code_name (
            :new.c901_rebate_part_type), :new.c7200_last_updated_by, :new.c7200_last_updated_dt) ;
            
        END IF; -- end of v_old_rebate_part_type check
        
        
        -- 2. Rebate Category update
        
        IF v_old_rebate_category              <> v_new_rebate_category THEN
        	--
            IF (:OLD.c7200_rebate_category_fl IS NULL) --if first time insert of record
                THEN
                -- to log the data to T740a table
                
                gm_pkg_sm_rebate_setup.gm_sav_rebate_master_log (:old.c7200_rebate_id, v_category_type_num,
                get_code_name (:old.c901_rebate_category), :old.c7200_last_updated_by, :old.c7200_last_updated_dt) ;
                
                -- update the flag
                
                :NEW.c7200_rebate_category_fl := 'Y';
                
            END IF; -- end of null check
            
            gm_pkg_sm_rebate_setup.gm_sav_rebate_master_log (:new.c7200_rebate_id, v_category_type_num, get_code_name (
            :new.c901_rebate_category), :new.c7200_last_updated_by, :new.c7200_last_updated_dt) ;
            
        END IF; -- end of v_old_rebate_category check
        
        
        -- 3. Rebate Rate update
        
        IF v_old_rebate_rate              <> v_new_rebate_rate THEN
        	--
        	
            IF (:OLD.c7200_rebate_rate_fl IS NULL) --if first time insert of record
                THEN
                -- to log the data to T740a table
                
                gm_pkg_sm_rebate_setup.gm_sav_rebate_master_log (:old.c7200_rebate_id, v_rate_num,
                :old.c7200_rebate_rate, :old.c7200_last_updated_by, :old.c7200_last_updated_dt) ;
                
                -- update the flag
                
                :NEW.c7200_rebate_rate_fl := 'Y';
                
            END IF;
            
            gm_pkg_sm_rebate_setup.gm_sav_rebate_master_log (:new.c7200_rebate_id, v_rate_num, :new.c7200_rebate_rate,
            :new.c7200_last_updated_by, :new.c7200_last_updated_dt) ;
            
        END IF; -- end of v_old_rebate_rate check
        
        -- 4. Rebate effective Date update
        
        IF v_old_rebate_eff_dt <> v_new_rebate_eff_dt THEN
        
            -- to get date format
             SELECT get_compdtfmt_frm_cntx ()
               INTO v_date_fmt
               FROM dual;
            --
            IF (:OLD.c7200_reb_eff_dt_fl IS NULL) --if first time insert of record
                THEN
                
                -- to log the data to T740a table
                
                gm_pkg_sm_rebate_setup.gm_sav_rebate_master_log (:old.c7200_rebate_id, v_effective_num, TO_CHAR (
                v_old_rebate_eff_dt, v_date_fmt), :old.c7200_last_updated_by, :old.c7200_last_updated_dt) ;
                
                -- update the flag.
                
                :NEW.c7200_reb_eff_dt_fl := 'Y';
                
            END IF; -- end of null check
            
            gm_pkg_sm_rebate_setup.gm_sav_rebate_master_log (:new.c7200_rebate_id, v_effective_num, TO_CHAR (
            v_new_rebate_eff_dt, v_date_fmt), :new.c7200_last_updated_by, :new.c7200_last_updated_dt) ;
            
        END IF;-- end of v_old_rebate_eff_dt check
        
    END trg_t7200_rebate_master_log;
    / 