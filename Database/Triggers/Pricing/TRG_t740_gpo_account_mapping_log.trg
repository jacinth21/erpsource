--@"c:\database\triggers\trg_t740_gpo_account_mapping_log.trg";
CREATE OR REPLACE TRIGGER trg_t740_gpo_account_mapping_log
	BEFORE UPDATE OF c101_party_id
	ON t740_gpo_account_mapping
	FOR EACH ROW
	
	DECLARE
	
	v_old_partyid t740_gpo_account_mapping.c101_party_id%TYPE;
    v_new_partyid t740_gpo_account_mapping.C101_PARTY_ID%TYPE;
    
    BEGIN
	    
        /*
        * This trigger used to log the group price book information.
        * 1. While change the GPB in Account setup - to track the log
        *  It capture - how much time same GPB group mapped (time frame)
        *
        */
	    
        v_old_partyid := NVL (:old.c101_party_id, '-9999') ;
        v_new_partyid := NVL (:new.c101_party_id, '-9999') ;
        
        --
        
        IF v_old_partyid <> v_new_partyid THEN
        
        	-- to log the data to T740a table
        	
            gm_pkg_sm_pricparam_setup.gm_sav_gpo_account_map_log (v_old_partyid, :old.c704_account_id,
            :OLD.c740_created_date, :old.c740_last_updated_date, :old.c740_last_updated_by) ;
            
            -- to reset the created date (so, that next time change GPB we know exact time.
            :NEW.c740_created_date := CURRENT_DATE;
            
        END IF; -- end of party id check
        
    END trg_t740_gpo_account_mapping_log;
    / 
    
    
    