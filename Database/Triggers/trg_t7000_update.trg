/* Formatted on 2010/01/05 14:27 (Formatter Plus v4.8.0) */

 --@"C:\DATABASE\Triggers\trg_t7000_update.trg";
CREATE OR REPLACE TRIGGER trg_t7000_update
	BEFORE UPDATE OF c7000_effective_date, c901_request_status
	ON t7000_account_price_request
	FOR EACH ROW
DECLARE
	v_old_date	   t7000_account_price_request.c7000_effective_date%TYPE;
	v_new_date	   t7000_account_price_request.c7000_effective_date%TYPE;
	v_old_status   t7000_account_price_request.c901_request_status%TYPE;
	v_new_status   t7000_account_price_request.c901_request_status%TYPE;
BEGIN
--
/*	v_old_date	:= :OLD.c7000_effective_date;
	v_new_date	:= :NEW.c7000_effective_date;

--
	IF (v_old_date <> v_new_date)
	THEN
		--
		IF (:OLD.c7000_effective_date_historyfl IS NULL)
		THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.c7000_account_price_request_id
													 , TO_CHAR (v_old_date, 'MM/DD/YYYY')
													 , 1032
													 , :OLD.c7000_created_by
													 , :OLD.c7000_created_date
													  );
			:NEW.c7000_effective_date_historyfl := 'Y';
		END IF;

		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.c7000_account_price_request_id
												 , TO_CHAR (v_new_date, 'MM/DD/YYYY')
												 , 1032
												 , :NEW.c7000_last_updated_by
												 , :NEW.c7000_last_updated_date
												  );
	END IF;
*/
	v_old_status := :OLD.c901_request_status;
	v_new_status := :NEW.c901_request_status;

	IF (v_old_status <> v_new_status)
	THEN
		IF (:OLD.c7000_status_historyfl IS NULL)
		THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.c7000_account_price_request_id
													 , get_code_name (v_old_status)
													 , 1034
													 , :OLD.c7000_created_by
													 , :OLD.c7000_created_date
													  );
		END IF;

		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.c7000_account_price_request_id
												 , get_code_name (v_new_status)
												 , 1034
												 , :NEW.c7000_last_updated_by
												 , :NEW.c7000_last_updated_date
												  );
		:NEW.c7000_status_historyfl := 'Y';
	END IF;
--
END trg_t7000_update;
/
