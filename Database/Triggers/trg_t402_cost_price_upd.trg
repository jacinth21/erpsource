/* Formatted on 11/17/2009 09:39 (Formatter Plus v4.8.0) */
--@"C:\database\Triggers\trg_t402_cost_price_upd.trg";
CREATE OR REPLACE TRIGGER trg_t402_cost_price_upd
	BEFORE UPDATE OF c402_cost_price
	ON t402_work_order
	FOR EACH ROW
DECLARE
	v_old		   t402_work_order.c402_cost_price%TYPE;
	v_new		   t402_work_order.c402_cost_price%TYPE;
	v_subject	   VARCHAR2 (100);
	v_mail_body    VARCHAR2 (3000);
	v_to_mail	   VARCHAR2 (200);
	crlf  CONSTANT VARCHAR2 (2) := CHR (13) || CHR (10);
BEGIN
--
	v_old		:= :OLD.c402_cost_price;
	v_new		:= :NEW.c402_cost_price;

--
	IF (v_old <> v_new)
	THEN
		IF (:OLD.c402_price_history_fl IS NULL)
		THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.c402_work_order_id
													 , v_old
													 , 1026
													 , :NEW.c402_created_by
													 , :NEW.c402_created_date
													  );
		END IF;

		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.c402_work_order_id
												 , v_new
												 , 1026
												 , :NEW.c402_last_updated_by
												 , :NEW.c402_last_updated_date
												  );
		:NEW.c402_price_history_fl := 'Y';
		v_to_mail	:= get_rule_value ('WOPRICEUPD', 'EMAIL');
		v_subject	:= 'Cost updated for Work Order ' || :NEW.c402_work_order_id;
		v_mail_body :=
			   'Part Number: '
			|| :NEW.c205_part_number_id
			|| crlf
			|| 'Description: '
			|| get_partnum_desc (:NEW.c205_part_number_id)
			|| crlf
			|| 'Project: '
			|| get_project_name (get_project_id_from_part (:NEW.c205_part_number_id))
			|| crlf
			|| 'Purchase Order: '
			|| :NEW.c401_purchase_ord_id
			|| crlf
			|| 'Original Cost: '
			|| get_code_name_alt (get_vendor_currency (:NEW.c301_vendor_id))
			|| ' '
			|| TRIM (TO_CHAR (v_old, '9,999.00'))
			|| crlf
			|| 'New Cost: '
			|| get_code_name_alt (get_vendor_currency (:NEW.c301_vendor_id))
			|| ' '
			|| TRIM (TO_CHAR (v_new, '9,999.00'))
			|| crlf
			|| 'Vendor: '
			|| get_vendor_name (:NEW.c301_vendor_id)
			|| crlf
			|| 'Updated By: '
			|| get_user_name (:NEW.c402_last_updated_by)
			|| crlf
			|| 'Updated On: '
			|| TO_CHAR (SYSDATE, 'mm/dd/yyyy hh24:mi:ss')
			|| crlf;
		gm_com_send_email_prc (v_to_mail, v_subject, v_mail_body);
	END IF;
--
END trg_t402_cost_price_upd;
/
