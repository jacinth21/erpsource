/* Formatted on 2009/08/07 09:27 (Formatter Plus v4.8.0) */

--@"C:\DATABASE\Triggers\trg_t622_patient_list.trg"

CREATE OR REPLACE TRIGGER trg_t622_patient_list
	BEFORE UPDATE OF c622_date, c622_initials
	ON t622_patient_list
	FOR EACH ROW
DECLARE
	v_old_date	   t622_patient_list.c622_date%TYPE;
	v_new_date	   t622_patient_list.c622_date%TYPE;
	v_old_initials t622_patient_list.c622_initials%TYPE;
	v_new_initials t622_patient_list.c622_initials%TYPE;
BEGIN
	v_old_date	:= :OLD.c622_date;
	v_new_date	:= :NEW.c622_date;

	IF (v_old_date <> v_new_date)
	THEN
		IF (:OLD.c622_date_history_fl IS NULL)
		THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.c622_patient_list_id
													 , TO_CHAR (v_old_date, 'MM/DD/YYYY')
													 , 1017
													 , :OLD.c622_created_by
													 , :OLD.c622_created_date
													  );
		END IF;

		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.c622_patient_list_id
												 , TO_CHAR (v_new_date, 'MM/DD/YYYY')
												 , 1017
												 , :NEW.c622_last_updated_by
												 , :NEW.c622_last_updated_date
												  );
		:NEW.c622_date_history_fl := 'Y';
	END IF;

	v_old_initials := :OLD.c622_initials;
	v_new_initials := :NEW.c622_initials;

	IF (v_old_initials <> v_new_initials)
	THEN
		IF (:OLD.c622_initials_history_fl IS NULL)
		THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.c622_patient_list_id
													 , v_old_initials
													 , 1018
													 , :OLD.c622_created_by
													 , :OLD.c622_created_date
													  );
		END IF;

		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.c622_patient_list_id
												 , v_new_initials
												 , 1018
												 , :NEW.c622_last_updated_by
												 , :NEW.c622_last_updated_date
												  );
		:NEW.c622_initials_history_fl := 'Y';
	END IF;
END trg_t622_patient_list;
/
