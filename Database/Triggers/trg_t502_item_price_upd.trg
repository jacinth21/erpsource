--@"C:\Database\Triggers\trg_t502_item_price_upd.trg";
CREATE OR REPLACE TRIGGER trg_t502_item_price_upd
	BEFORE INSERT OR UPDATE OF c502_item_price
	ON t502_item_order
	FOR EACH ROW
DECLARE		        
	v_old_item_price			   t502_item_order.c502_item_price%TYPE;
	v_new_item_price			   t502_item_order.c502_item_price%TYPE;
	v_comptxn_curr NUMBER;
	v_rept_curr NUMBER; 
	v_curr_from varchar2(10);
	v_curr_rate NUMBER(15,6);
	v_compid t501_order.c1900_company_id%TYPE;
	v_corp_item_price number(15,2);
	v_acc_id				t501_order.c704_account_id%TYPE;	
	v_acc_currn			    T704_ACCOUNT.C901_CURRENCY%TYPE;
	v_exch_rate_fl			t906_rules.c906_rule_value%TYPE;
	v_order_id				t501_order.c501_order_id%TYPE;
	v_atrb_type				t906_rules.c906_rule_value%TYPE;
	v_exchange_rate			t501a_order_attribute.c501a_attribute_value%TYPE;
	v_order_date            T501_ORDER.C501_ORDER_DATE%TYPE;
	
BEGIN
--
	v_order_id := NVL(:NEW.c501_order_id,:OLD.c501_order_id);
	
	BEGIN
		SELECT c1900_company_id , c704_account_id , C501_ORDER_DATE
		  INTO v_compid , v_acc_id , v_order_date
		  FROM t501_order
		 WHERE c501_order_id = v_order_id;
		 
		SELECT c901_txn_currency, c901_reporting_currency
		  INTO v_comptxn_curr, v_rept_curr
		  FROM t1900_company
		 WHERE c1900_company_id = v_compid;
		EXCEPTION 
			WHEN NO_DATA_FOUND THEN
			 raise_application_error ('-20501', 'For the DO '||NVL(:NEW.c501_order_id,:OLD.c501_order_id)||' Company Information is not available. DO Cannot be placed.' );
	END;
	
	BEGIN
		-- Get the Account Currency to do the Currency Conversion.
		SELECT C901_CURRENCY
		INTO   v_acc_currn
		FROM   T704_ACCOUNT
		WHERE  C704_ACCOUNT_ID = v_acc_id;
	EXCEPTION WHEN NO_DATA_FOUND THEN
			 raise_application_error ('-20501', 'For the Account ID: '||v_acc_id||' No Currency Available. DO Cannot be placed.' );
	END;
	
	/*Get the exchange currency rate , 1,101102- average*/  
    SELECT GET_CURRENCY_CONVERSION (v_acc_currn,v_rept_curr,v_order_date,1,101102) , GET_RULE_VALUE(v_compid,'EXCH_RATE_CALC')
	         INTO v_curr_rate, v_exch_rate_fl
	 FROM DUAL;  
	 
 
	 -- If the exchange rate flag is 'Y', then calculate the C502_CORP_ITEM_PRICE by dividing the exchange rate from item price
	 IF v_exch_rate_fl = 'Y'
	 THEN
	 	SELECT get_rule_value ('EXCH_RATE', 'EXCH_RATE_CALC') into v_atrb_type FROM dual;

	 	BEGIN
			SELECT c501a_attribute_value  INTO v_exchange_rate 
			FROM t501a_order_attribute
			WHERE c501_order_id = v_order_id  
			AND C901_ATTRIBUTE_TYPE = v_atrb_type;
		     EXCEPTION
	          WHEN NO_DATA_FOUND THEN
	            v_exchange_rate := NULL;
	    END;
	     SELECT DECODE(v_exchange_rate,0,1,v_exchange_rate) INTO v_exchange_rate FROM DUAL;
	    
		 IF v_exchange_rate IS NOT NULL AND (INSERTING OR UPDATING) THEN
			v_corp_item_price := NVL(:NEW.c502_item_price,0) / v_exchange_rate;
	        :NEW.C502_CORP_ITEM_PRICE := v_corp_item_price;    
		END IF;
	 RETURN;
	 END IF;
	    
	 IF v_curr_rate = 0
	 THEN
	 	raise_application_error ('-20501', 'Currency Conversion Rate Not Available From : '||GET_CODE_NAME_ALT(v_acc_currn)||' to '||GET_CODE_NAME_ALT(v_rept_curr)||'. DO Cannot be placed.' );
	 END IF;
	         
	IF INSERTING OR UPDATING THEN
		v_corp_item_price := NVL(:NEW.c502_item_price,0) * v_curr_rate;
        :NEW.C502_CORP_ITEM_PRICE := v_corp_item_price;    
	END IF;
	
	-- Update Price at the time to recaluclate the rebate
	
	IF UPDATING THEN
	
	 	v_old_item_price := NVL (:OLD.c502_item_price, 0) ;	
        v_new_item_price := NVL (:NEW.c502_item_price, 0) ;
	
        IF (v_old_item_price <> v_new_item_price AND :OLD.c502_rebate_process_fl IS NOT NULL AND NVL(:OLD.C7200_REBATE_RATE, 0 ) <> 0)
        THEN
        
         			:NEW.c502_rebate_process_fl := NULL;
		            :NEW.c7200_rebate_rate      := NULL;
		            -- to void the DS orders
		            gm_pkg_ac_order_rebate_txn.gm_void_discount_order (:OLD.c501_order_id, 30301) ;
            
         END IF;   
         --
         
	END IF;
--
END trg_t502_item_price_upd;
/