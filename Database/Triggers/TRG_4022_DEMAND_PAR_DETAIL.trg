/* Formatted on 2008/03/26 16:32 (Formatter Plus v4.8.0) */
CREATE OR REPLACE TRIGGER trg_4022_demand_par_detail
	BEFORE UPDATE OF c4022_par_value
	ON t4022_demand_par_detail
	FOR EACH ROW
DECLARE
	v_old		   t4022_demand_par_detail.c4022_par_value%TYPE;
	v_new		   t4022_demand_par_detail.c4022_par_value%TYPE;
BEGIN
--
	v_old		:= :OLD.c4022_par_value;
	v_new		:= :NEW.c4022_par_value;

-- If type equals to forecast and the value then load the value
	IF (NVL(v_old,-9999) <> NVL(v_new,-9999))
	THEN
		-- For first time initial load
		IF (:OLD.c4022_history_fl IS NULL)
		THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.c4022_demand_par_detail_id
													 , v_old
													 , 1005
													 , :OLD.c4022_updated_by
													 , :OLD.c4022_updated_date
													  );
		END IF;

		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.c4022_demand_par_detail_id
												 , v_new
												 , 1005
												 , :NEW.c4022_updated_by
												 , :NEW.c4022_updated_date
												  );
		--
		:NEW.c4022_history_fl := 'Y';
--
	END IF;
--
END trg_4022_demand_sheet_detail;
/