--@"C:\Database\Triggers\trg_t611_clinical_study_upd.trg"
CREATE OR REPLACE TRIGGER trg_t611_clinical_study_upd
  BEFORE UPDATE OF c901_study_status ON t611_clinical_study
  FOR EACH ROW
  DECLARE
  	v_old_status t611_clinical_study.c901_study_status%TYPE;
    v_new_status t611_clinical_study.c901_study_status%TYPE;
    BEGIN
        v_old_status            := :OLD.c901_study_status;
        v_new_status            := :NEW.c901_study_status;
        IF :OLD.c611_history_fl IS NULL THEN
        	--
            gm_pkg_cr_common.gm_sav_study_log (
            	  :NEW.C611_STUDY_ID
            	, :NEW.C611_STUDY_NM
            	, v_old_status
            	, :OLD.c611_study_comments
            	, :OLD.c901_study_log_reason
            	, :NEW.C611_LAST_UPDATED_BY);
        END IF;
        --
        gm_pkg_cr_common.gm_sav_study_log (
        		  :NEW.C611_STUDY_ID
        		, :NEW.C611_STUDY_NM
        		, v_new_status
        		, :NEW.c611_study_comments
        		, :NEW.c901_study_log_reason
        		, :NEW.C611_LAST_UPDATED_BY);
        --
        :NEW.C611_HISTORY_FL := 'Y';
END trg_t611_clinical_study_upd;
/
