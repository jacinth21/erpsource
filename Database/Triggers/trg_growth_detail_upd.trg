/* Formatted on 2007/08/29 15:55 (Formatter Plus v4.8.0) */
-- @"c:\Database\Trigger\trg_growth_detail_upd.trg"

CREATE OR REPLACE TRIGGER trg_growth_detail_upd
	BEFORE UPDATE OF c4031_value
	ON t4031_growth_details
	FOR EACH ROW
DECLARE
	v_old		   t4042_demand_sheet_detail.c4042_qty%TYPE;
	v_new		   t4042_demand_sheet_detail.c4042_qty%TYPE;
BEGIN
--
	v_old		:= :OLD.c4031_value;
	v_new		:= :NEW.c4031_value;

--
	IF (NVL(v_old,-999) <> NVL(v_new,-999))
	THEN
		--
		IF (:OLD.c4031_history_fl IS NULL)
		THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.c4031_growth_details_id
													 , v_old
													 , 1002
													 , :OLD.c4031_created_by
													 , :OLD.c4031_created_date
													  );
		END IF;

		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.c4031_growth_details_id
												 , v_new
												 , 1002
												 , :NEW.C4031_LAST_UPDATED_BY
												 , :NEW.C4031_LAST_UPDATED_DATE
												  );
		--
		:NEW.c4031_history_fl := 'Y';
--
	END IF;
--
END trg_growth_detail_upd;
/
