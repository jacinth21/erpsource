
-- @"c:\Database\Triggers\TRG_T701_DISTRIBUTOR_UPDATE.trg";

create or replace
TRIGGER TRG_T701_DISTRIBUTOR_UPDATE
   BEFORE INSERT OR UPDATE OF C701_BILL_ADD1,C701_BILL_ADD2,C701_BILL_CITY,C701_BILL_STATE,C701_BILL_COUNTRY,C701_BILL_ZIP_CODE,
   					C701_SHIP_ADD1,C701_SHIP_ADD2,C701_SHIP_CITY,C701_SHIP_STATE,C701_SHIP_COUNTRY,C701_SHIP_ZIP_CODE,
   					C701_DISTRIBUTOR_NAME,C901_DISTRIBUTOR_TYPE,C701_ACTIVE_FL,C701_VOID_FL,C701_REGION ,C701_DISTRIBUTOR_ID
   ON t701_distributor
   FOR EACH ROW

DECLARE
 v_action   VARCHAR2(20) ;
 v_company_id  t701_distributor.C1900_COMPANY_ID%type;

BEGIN
 v_company_id  := :new.C1900_COMPANY_ID;
   IF (UPDATING AND(:NEW.C701_BILL_ADD1 IS NULL
              OR :NEW.C701_BILL_CITY IS NULL
              OR :NEW.C701_BILL_STATE IS NULL
              OR :NEW.C701_BILL_COUNTRY IS NULL
              OR :NEW.C701_BILL_ZIP_CODE IS NULL)
      )
   THEN
	 raise_application_error('-20535','');
   END IF;
  IF (UPDATING AND (:OLD.C701_BILL_ADD1 <> :NEW.C701_BILL_ADD1)
                OR (:OLD.C701_BILL_CITY <> :NEW.C701_BILL_CITY)
                OR (:OLD.C701_BILL_STATE <> :NEW.C701_BILL_STATE)
                OR (:OLD.C701_BILL_COUNTRY <> :NEW.C701_BILL_COUNTRY)
                OR (:OLD.C701_BILL_ZIP_CODE <> :NEW.C701_BILL_ZIP_CODE)
      )
  THEN
		        :NEW.c701_BILL_LATITUDE := NULL;
		        :NEW.c701_BILL_LONGITUDE := NULL;
		        :NEW.c701_BILL_GEO_LOCATION := NULL;
		        
		        v_action := '103123'; -- Updated
	
  END IF;
  IF (UPDATING AND (:OLD.C701_SHIP_ADD1 <> :NEW.C701_SHIP_ADD1)
  				OR (:OLD.C701_SHIP_ADD2 <> :NEW.C701_SHIP_ADD2)
                OR (:OLD.C701_SHIP_CITY <> :NEW.C701_SHIP_CITY)
                OR (:OLD.C701_SHIP_STATE <> :NEW.C701_SHIP_STATE)
                OR (:OLD.C701_SHIP_COUNTRY <> :NEW.C701_SHIP_COUNTRY)
                OR (:OLD.C701_SHIP_ZIP_CODE <> :NEW.C701_SHIP_ZIP_CODE)
      )
   THEN
   				:NEW.c701_SHIP_LATITUDE := NULL;
		        :NEW.c701_SHIP_LONGITUDE := NULL;
		        :NEW.c701_SHIP_GEO_LOCATION := NULL;
		        
		        v_action := '103123'; -- Updated
	END IF;
	
	IF INSERTING THEN
        v_action := '103122'; -- New
    ELSIF UPDATING THEN
  				IF ((:OLD.C701_DISTRIBUTOR_NAME <> :NEW.C701_DISTRIBUTOR_NAME) OR (:OLD.C901_DISTRIBUTOR_TYPE <> :NEW.C901_DISTRIBUTOR_TYPE) 
  						OR (NVL(:OLD.C701_BILL_ADD2,'999')) <> (:NEW.C701_BILL_ADD2) OR (NVL(:OLD.C701_SHIP_ADD2,'999')) <> (:NEW.C701_SHIP_ADD2) 
  						OR (:OLD.C701_SHIP_NAME <> :NEW.C701_SHIP_NAME)
  						OR (:OLD.C701_REGION <> :NEW.C701_REGION) OR (NVL(:OLD.C701_VOID_FL,'N') <> NVL(:NEW.C701_VOID_FL,'N') AND NVL(:NEW.C701_VOID_FL,'N') = 'N')
  						OR (:OLD.C701_ACTIVE_FL <> :NEW.C701_ACTIVE_FL AND :NEW.C701_ACTIVE_FL ='Y') )
  				THEN
  				v_action := '103123'; -- Updated
  				END IF;
      END IF;
      
		 -- When a distributor is voided, the If condition will satisfy.
      IF (NVL(:OLD.C701_VOID_FL,'N') <> NVL(:NEW.C701_VOID_FL,'N') AND NVL(:NEW.C701_VOID_FL,'N') = 'Y') 
     	 OR (:OLD.C701_ACTIVE_FL <> :NEW.C701_ACTIVE_FL AND :NEW.C701_ACTIVE_FL ='N')
       THEN
        v_action  := '4000412'; -- Voided
      END IF;
      
   IF (v_action IS NOT NULL AND :NEW.c901_ext_country_id IS NULL AND :NEW.C1900_COMPANY_ID = v_company_id ) THEN --Passing companyID dynamically from the table
        -- 103097 English, 103100 Product Catalog , 4000521 Ref Type
        gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (:NEW.C701_DISTRIBUTOR_ID, '4000521', v_action, '103097', '103100', NVL (:NEW.c701_created_by, :NEW.c701_last_updated_by ), NULL,'Y',v_company_id ) ;
   END IF;
END; 
/