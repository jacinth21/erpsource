create or replace
TRIGGER trg_4011_group_detail_ipad_update
BEFORE UPDATE 
OF C205_PART_NUMBER_ID, C901_PART_PRICING_TYPE, C4011_PART_NUMBER_HISTORYFL, C901_ACTION_TYPE, C4011_PRIMARY_PART_LOCK_FL ON t4011_group_detail FOR EACH ROW

DECLARE	

 v_old_partnum 		  t4011_group_detail.C205_PART_NUMBER_ID%TYPE;
 v_new_partnum    	  t4011_group_detail.C205_PART_NUMBER_ID%TYPE;
 v_old_part_prctyp    t4011_group_detail.C901_PART_PRICING_TYPE%TYPE;
 v_new_part_prctyp    t4011_group_detail.C901_PART_PRICING_TYPE%TYPE;
 v_old_acttype        t4011_group_detail.C901_ACTION_TYPE%TYPE;
 v_new_acttype        t4011_group_detail.C901_ACTION_TYPE%TYPE;
 v_old_part_lfl       t4011_group_detail.C4011_PRIMARY_PART_LOCK_FL%TYPE;
 v_new_part_lfl       t4011_group_detail.C4011_PRIMARY_PART_LOCK_FL%TYPE;
 
BEGIN 
-- 
  v_old_partnum			:= NVL(:OLD.C205_PART_NUMBER_ID,'-9999'); 
  v_new_partnum			:= NVL(:NEW.C205_PART_NUMBER_ID,'-9999');
  v_old_part_prctyp		:= NVL(:NEW.C901_PART_PRICING_TYPE,'-9999');
  v_new_part_prctyp		:= NVL(:NEW.C901_PART_PRICING_TYPE,'-9999');
  v_old_acttype			:= NVL(:OLD.C901_ACTION_TYPE,'-9999'); 
  v_new_acttype			:= NVL(:NEW.C901_ACTION_TYPE,'-9999');
  v_old_part_lfl		:= NVL(:OLD.C4011_PRIMARY_PART_LOCK_FL,'N'); 
  v_new_part_lfl		:= NVL(:NEW.C4011_PRIMARY_PART_LOCK_FL,'N');
  
--if any changes in t4011_group_detail , then Update ipad sync flag as null and ipad sync date as current date in t4011_group_detail table
 
IF ((v_old_partnum <> v_new_partnum) OR (v_old_part_prctyp <> v_new_part_prctyp) OR	(v_old_acttype <> v_new_acttype) OR (v_old_part_lfl <> v_new_part_lfl))
	THEN 
					:NEW.C4011_IPAD_SYNCED_FLAG := NULL;
   					:NEW.C4011_IPAD_SYNCED_DATE := NULL;
   					
END IF;
 
END trg_4011_group_detail_ipad_update;
/