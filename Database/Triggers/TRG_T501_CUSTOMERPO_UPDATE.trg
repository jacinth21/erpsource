-- @"c:\Database\Triggers\trg_t501_customerpo_update.trg";

CREATE OR REPLACE TRIGGER TRG_T501_CUSTOMERPO_UPDATE BEFORE INSERT OR UPDATE OF C501_CUSTOMER_PO ON t501_order 
FOR EACH ROW 
DECLARE 
BEGIN

/*If PO is entered the first time than customer PO entered date should get updated.
If the PO is edited or revenue sampling information is changed then PO entered Date should not be updated.
If the PO is removed then the customer PO entered date should be removed.
*/	
	IF INSERTING AND :NEW.c501_customer_po IS NOT NULL THEN 
			:NEW.C501_CUSTOMER_PO_DATE := current_date; --PC-3880 New field in record PO Date in GM Italy
	ELSIF UPDATING AND :NEW.c501_customer_po IS NOT NULL AND :OLD.C501_CUSTOMER_PO_DATE IS NULL THEN
			:NEW.C501_CUSTOMER_PO_DATE := nvl(:NEW.C501_CUSTOMER_PO_DATE,current_date); ----PC-3880 New field in record PO Date in GM Italy
	ELSIF UPDATING AND :NEW.c501_customer_po IS NULL THEN
			:NEW.C501_CUSTOMER_PO_DATE := NULL;
	END IF; 
END TRG_T501_CUSTOMERPO_UPDATE; 
/ 
