/* Formatted on 2011/10/25 10:11 (Formatter Plus v4.8.0) */
-- @"c:\Database\Triggers\trg_consignment_trans_void.trg";

CREATE OR REPLACE TRIGGER trg_consignment_trans_void
	AFTER UPDATE OF c504_void_fl
	ON t504_consignment
	FOR EACH ROW
DECLARE
BEGIN
	IF :NEW.c504_void_fl = 'Y'
	THEN
		gm_pkg_allocation.gm_inv_void_bytxn (:NEW.c504_consignment_id, :NEW.c504_type, :NEW.c504_last_updated_by);
		gm_pkg_op_item_control_txn.gm_void_control_number_txn (:NEW.c504_consignment_id, :NEW.c504_last_updated_by);
	END IF;
END trg_consignment_trans_void;
/
