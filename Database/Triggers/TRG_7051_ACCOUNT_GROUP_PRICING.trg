/* Formatted on 2009/10/06 19:07 (Formatter Plus v4.8.0) */

--@"C:\Database\Triggers\TRG_7051_ACCOUNT_GROUP_PRICING.trg";
CREATE OR REPLACE TRIGGER TRG_7051_ACCOUNT_GROUP_PRICING
	BEFORE INSERT OR UPDATE OF C7051_PRICE,C7051_VOID_FL
   ON T7051_ACCOUNT_GROUP_PRICING
   FOR EACH ROW

DECLARE
	v_old_price		   		T7051_ACCOUNT_GROUP_PRICING.C7051_PRICE%TYPE;
	v_new_price			    T7051_ACCOUNT_GROUP_PRICING.C7051_PRICE%TYPE;
	v_new_acct_grp_price	T7051_ACCOUNT_GROUP_PRICING.C7051_ACCOUNT_GROUP_PRICING_ID%TYPE;
	v_old_acct_grp_price	T7051_ACCOUNT_GROUP_PRICING.C7051_ACCOUNT_GROUP_PRICING_ID%TYPE;
	v_old_gpoid				T7051_ACCOUNT_GROUP_PRICING.C101_GPO_ID %TYPE;
	v_new_gpoid				T7051_ACCOUNT_GROUP_PRICING.C101_GPO_ID %TYPE;
	v_old_voidfl			T7051_ACCOUNT_GROUP_PRICING.C7051_VOID_FL%TYPE;
	v_new_voidfl			T7051_ACCOUNT_GROUP_PRICING.C7051_VOID_FL%TYPE;
	v_old_actfl				T7051_ACCOUNT_GROUP_PRICING.C7051_ACTIVE_FL %TYPE;
	v_new_actfl				T7051_ACCOUNT_GROUP_PRICING.C7051_ACTIVE_FL %TYPE;
	v_action	varchar2(50);
	v_ref_type varchar2(50);
	v_account_id 			T7051_ACCOUNT_GROUP_PRICING.C704_ACCOUNT_ID%TYPE;
	v_gpo_id				T7051_ACCOUNT_GROUP_PRICING.C101_GPO_ID%TYPE;
	 v_company_id        	t704_account.c1900_company_id%TYPE;
BEGIN
--
	v_old_price				:= :OLD.C7051_PRICE;
	v_new_price				:= :NEW.C7051_PRICE;
	v_new_acct_grp_price	:= :OLD.C7051_ACCOUNT_GROUP_PRICING_ID;
	v_old_acct_grp_price	:= :NEW.C7051_ACCOUNT_GROUP_PRICING_ID;
	v_old_gpoid				:= :OLD.C101_GPO_ID;
	v_new_gpoid				:= :NEW.C101_GPO_ID;
	v_old_voidfl			:= :OLD.C7051_VOID_FL;
	v_new_voidfl			:= :NEW.C7051_VOID_FL;
	v_old_actfl				:= :OLD.C7051_ACTIVE_FL;
	v_new_actfl				:= :NEW.C7051_ACTIVE_FL;
--	
	
	BEGIN
	    SELECT T7051.C704_ACCOUNT_ID 
	   		 INTO V_ACCOUNT_ID
	   	 FROM T7051_ACCOUNT_GROUP_PRICING T7051    
	   	 WHERE T7051.C7051_account_group_pricing_id = :NEW.C7051_ACCOUNT_GROUP_PRICING_ID
	   	 AND T7051.C7051_VOID_FL IS NULL;
	   	 
	 EXCEPTION 
	 WHEN OTHERS THEN
	   V_ACCOUNT_ID := NULL;
	END;
	
	IF V_ACCOUNT_ID IS NULL THEN
	    SELECT c101_gpo_id 
	    	INTO v_gpo_id
	    FROM T7051_ACCOUNT_GROUP_PRICING t7051    
	    WHERE t7051.C7051_account_group_pricing_id = :NEW.C7051_ACCOUNT_GROUP_PRICING_ID
	    AND T7051.C7051_VOID_FL IS NULL;
	END IF;
	
	BEGIN
	SELECT  t704.c1900_company_id 
		INTO v_company_id
	FROM t740_gpo_account_mapping t740 , t704_account t704
	WHERE t740.c704_account_id = t704.c704_account_id 
	AND ( t740.c101_party_id = v_gpo_id OR t740.c704_account_id = v_account_id )
	AND rownum = 1;
	EXCEPTION WHEN OTHERS THEN
	   v_company_id := null ;
	END;

  
	IF INSERTING THEN
        v_action := '103122'; -- New one
    ELSIF UPDATING THEN    
		IF (:OLD.C7051_PRICE IS NULL)	--if first time insert of record
		THEN   -- if insert
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.C7051_ACCOUNT_GROUP_PRICING_ID
													 , v_new_price
													 , 1041
													 , :NEW.C7051_LAST_UPDATED_BY
													 , :NEW.C7051_LAST_UPDATED_DATE
													 );
			-- if any of these values are updated, then it as an update to device										  );
		ELSIF ( (v_old_price <> v_new_price) 
				OR (v_new_acct_grp_price <> v_old_acct_grp_price) 
				OR (v_old_gpoid <> v_new_gpoid)
				OR (v_old_voidfl <> v_new_voidfl)
				OR (v_old_actfl <> v_new_actfl))   
		THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.C7051_ACCOUNT_GROUP_PRICING_ID
													 , v_old_price
													 , 1041
													 , :NEW.C7051_LAST_UPDATED_BY
													 , :NEW.C7051_LAST_UPDATED_DATE
													  );
			:NEW.C7051_PRICE_HISTORY_FL := 'Y';
			
			v_action  := '103123'; -- Updated 
		END IF;
	END IF;
	
	IF v_action IS NOT NULL THEN 
	Select  DECODE(v_new_gpoid,'','4000526','4000524') into v_ref_type from dual;
        -- 103097 English, 103100 Product Catalog , 4000524 Ref Type
        gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (NVL(:NEW.c101_GPO_ID,:NEW.c704_account_id) , v_ref_type, v_action, 
        											'103097', '103100', NVL (:NEW.c7051_created_by, :NEW.c7051_last_updated_by ), NULL,'Forceupdate',v_company_id) ;
     END IF;
--
END TRG_7051_ACCOUNT_GROUP_PRICING;
/
