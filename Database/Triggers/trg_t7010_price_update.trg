/* Formatted on 2009/11/15 13:10 (Formatter Plus v4.8.0) */

--@"C:\DATABASE\Triggers\trg_t7010_price_update.trg";

CREATE OR REPLACE TRIGGER trg_t7010_price_update
	BEFORE INSERT OR UPDATE OF c7010_price
	ON t7010_group_price_detail
	FOR EACH ROW
DECLARE
	v_old		   t7010_group_price_detail.c7010_price%TYPE;
	v_new		   t7010_group_price_detail.c7010_price%TYPE;
	v_type		   t7010_group_price_detail.c7010_price%TYPE;
	p_override_id	NUMBER;
	p_user_id		t7010_group_price_detail.C7010_CREATED_BY%TYPE;
    v_company_id      t7010_group_price_detail.c1900_company_id%TYPE;
BEGIN
--
	v_old		:= :OLD.c7010_price;
	v_new		:= :NEW.c7010_price;

	
	 --Getting company id from context.   
    SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;

	SELECT NVL(:OLD.C901_PRICE_TYPE,:NEW.C901_PRICE_TYPE) INTO v_type FROM DUAL;
	
	SELECT NVL(:NEW.C7010_CREATED_BY, :NEW.c7010_last_updated_by) INTO p_user_id FROM DUAL;
--
	IF ( (v_type ='52060' OR v_type ='52063') AND NVL(v_old,'-9999') <> NVL(v_new,'-9999') )-- LIST PRICE , TWP
	THEN
		--Call the New price log procedure.		
		SELECT DECODE( v_type, '52060', '103021', '52063', '103020') INTO v_type FROM dual;	
		   
		--During update if history flag is null, need to track the previous price value . 
       IF :OLD.C7010_PRICE_HISTORY_FL IS NULL AND :OLD.c7010_price IS NOT NULL
       THEN 
	      	gm_pkg_pd_group_pricing.gm_sav_grp_part_price_log (
													      NVL(:OLD.c4010_group_id, :NEW.c4010_group_id)
													     , v_type
														 , ' '
													     , v_old
													     , 1030
													     , NVL(:OLD.c7010_last_updated_by, :NEW.C7010_CREATED_BY)
													     , NVL(:OLD.c7010_last_updated_date,:NEW.C7010_CREATED_DATE)
													     , v_company_id
													   );
       END IF;
	
	  		
		gm_pkg_pd_group_pricing.gm_sav_grp_part_price_log (
												      NVL(:OLD.c4010_group_id, :NEW.c4010_group_id)
												     , v_type
													 , :NEW.C70101_last_comment
												     , v_new
												     , 1030
												     , NVL(:NEW.c7010_last_updated_by, :NEW.C7010_CREATED_BY)
												     , NVL(:NEW.c7010_last_updated_date,:NEW.C7010_CREATED_DATE)
												     , v_company_id
												   );		
		
		:NEW.c7010_price_history_fl := 'Y';
		
	END IF;
	
		:NEW.C70101_last_comment 	:= '';	
--
END trg_t7010_price_update;
/