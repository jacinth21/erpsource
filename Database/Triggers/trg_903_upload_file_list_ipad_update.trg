--@"c:\database\triggers\trg_903_upload_file_list_ipad_update.trg";

  CREATE OR REPLACE TRIGGER trg_903_upload_file_list_ipad_update 
BEFORE UPDATE ON t903_upload_file_list 
FOR EACH ROW 

DECLARE	

 v_old_ref_id       t903_upload_file_list.C903_REF_ID%TYPE;
 v_new_ref_id       t903_upload_file_list.C903_REF_ID%TYPE;
 v_old_file_name    t903_upload_file_list.C903_FILE_NAME%TYPE;
 v_new_file_name    t903_upload_file_list.C903_FILE_NAME%TYPE;
 v_old_del_fl       VARCHAR2(100);
 v_new_del_fl       VARCHAR2(100);
 v_old_ref_type     t903_upload_file_list.C901_REF_TYPE%TYPE;
 v_new_ref_type     t903_upload_file_list.C901_REF_TYPE%TYPE;
 v_old_type     	t903_upload_file_list.C901_TYPE%TYPE;
 v_new_type 		t903_upload_file_list.C901_TYPE%TYPE;
 v_old_ref_grp 		t903_upload_file_list.C901_REF_GRP%TYPE;
 v_new_ref_grp 		t903_upload_file_list.C901_REF_GRP%TYPE;
 v_old_file_sz 		t903_upload_file_list.C903_FILE_SIZE%TYPE;
 v_new_file_sz 		t903_upload_file_list.C903_FILE_SIZE%TYPE;
 v_old_file_seqno 	t903_upload_file_list.C903_FILE_SEQ_NO%TYPE;
 v_new_file_seqno 	t903_upload_file_list.C903_FILE_SEQ_NO%TYPE;
 v_old_file_title 	t903_upload_file_list.C901_FILE_TITLE%TYPE;
 v_new_file_title 	t903_upload_file_list.C901_FILE_TITLE%TYPE;
 v_old_cmpid 		t903_upload_file_list.C1900_COMPANY_ID%TYPE;
 v_new_cmpid 		t903_upload_file_list.C1900_COMPANY_ID%TYPE;
 v_status			t2082_set_release_mapping.C901_STATUS_ID%TYPE;
 v_grp_count		NUMBER;
 v_group_type		t4012_group_company_mapping.C901_GROUP_COM_MAP_TYP%TYPE;
 v_part_count		NUMBER;
 v_part_type		t2023_part_company_mapping.C901_PART_MAPPING_TYPE%TYPE;
 v_count		NUMBER;
 
BEGIN 
-- 
  v_old_ref_id				:= NVL(:OLD.C903_REF_ID,'-9999'); 
  v_new_ref_id				:= NVL(:NEW.C903_REF_ID,'-9999'); 
  v_old_file_name			:= NVL(:OLD.C903_FILE_NAME,'-9999'); 
  v_new_file_name			:= NVL(:NEW.C903_FILE_NAME,'-9999');
  v_old_del_fl				:= NVL(:OLD.C903_DELETE_FL, 'N'); 
  v_new_del_fl				:= NVL(:NEW.C903_DELETE_FL, 'N');
  v_old_ref_type			:= NVL(:OLD.C901_REF_TYPE,'-9999'); 
  v_new_ref_type			:= NVL(:NEW.C901_REF_TYPE,'-9999');
  v_old_type				:= NVL(:OLD.C901_TYPE,'-9999'); 
  v_new_type				:= NVL(:NEW.C901_TYPE,'-9999');
  v_old_ref_grp				:= NVL(:OLD.C901_REF_GRP,'-9999'); 
  v_new_ref_grp				:= NVL(:NEW.C901_REF_GRP,'-9999');
  v_old_file_seqno			:= NVL(:OLD.C903_FILE_SEQ_NO,'-9999'); 
  v_new_file_seqno			:= NVL(:NEW.C903_FILE_SEQ_NO,'-9999');
  v_old_file_title			:= NVL(:OLD.C901_FILE_TITLE,'-9999'); 
  v_new_file_title			:= NVL(:NEW.C901_FILE_TITLE,'-9999');
  
--if any changes in t903_upload_file_list , then Update ipad sync flag as null in t903_upload_file_list table
-- to check group and part details
 
IF ((v_old_ref_id <> v_new_ref_id) OR (v_old_file_name <> v_new_file_name) OR (v_old_del_fl <> v_new_del_fl) OR (v_old_ref_type <> v_new_ref_type) OR 
	(v_old_type <> v_new_type) OR (v_old_ref_grp <> v_new_ref_grp) OR (v_old_file_seqno <> v_new_file_seqno) OR 
	(v_old_file_title <> v_new_file_title))
	THEN 
	-- 103092 System 103091 Set
	IF v_new_ref_grp = 103092 OR v_new_ref_grp = 103091
		THEN 
			SELECT  count(1) INTO v_count FROM t2080_set_company_mapping WHERE c207_set_id = v_new_ref_id AND c901_ipad_released_status IN (105361);
				-- 105361 if system or set released
				IF v_count <> 0	
					THEN
   					:NEW.C903_IPAD_SYNCED_FLAG := NULL;
   					:NEW.C903_IPAD_SYNCED_DATE := NULL;
   				END IF;
   				--
   	ELSIF v_new_ref_grp = 103094
   		THEN
   			--to check group details 
        	SELECT count(1) INTO v_grp_count FROM t4012_group_company_mapping WHERE c4010_group_id = v_new_ref_id AND c901_group_com_map_typ IN (105360, 105361);
   			-- 105360 Created or 105361 Released
   			IF v_grp_count <> 0
   				THEN
   				:NEW.C903_IPAD_SYNCED_FLAG := NULL;
   				:NEW.C903_IPAD_SYNCED_DATE := NULL;
   			END IF;
   			--
   	ELSIF v_new_ref_grp = 103090
   		THEN
   			--to check part details
   			SELECT count(1) INTO v_part_count FROM t2023_part_company_mapping WHERE C205_PART_NUMBER_ID = v_new_ref_id and C901_PART_MAPPING_TYPE IN (105360, 105361);	
 			-- 105360 Created or 105361 Released
   			IF v_part_count <> 0
   				THEN
   				:NEW.C903_IPAD_SYNCED_FLAG := NULL;
   				:NEW.C903_IPAD_SYNCED_DATE := NULL;
   			END IF;
   			--
   	END IF;-- end of new ref type check (v_new_ref_grp)
   	--
END IF; -- end of all field check
 
END trg_903_upload_file_list_ipad_update;
/
