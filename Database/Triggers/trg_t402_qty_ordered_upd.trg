/* Formatted on 11/13/2009 13:24 (Formatter Plus v4.8.0) */
--@"C:\database\Triggers\trg_t402_qty_ordered_upd.trg";

CREATE OR REPLACE TRIGGER trg_t402_qty_ordered_upd
	BEFORE UPDATE OF C402_QTY_ORDERED
	ON t402_work_order
	FOR EACH ROW
DECLARE
	v_old		   t402_work_order.C402_QTY_ORDERED%TYPE;
	v_new		   t402_work_order.C402_QTY_ORDERED%TYPE;
BEGIN
--
	v_old		:= :OLD.C402_QTY_ORDERED;
	v_new		:= :NEW.C402_QTY_ORDERED;

--
	IF (v_old <> v_new)
	THEN
		IF (:OLD.C402_QTY_HISTORY_FL IS NULL)   --if first time insert of record
		THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.c402_work_order_id
													 , v_old
													 , 1027
													 , :NEW.c402_created_by
													 , :NEW.c402_created_date
													  );
		END IF;

		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.c402_work_order_id
												 , v_new
												 , 1027
												 , :NEW.c402_last_updated_by
												 , :NEW.c402_last_updated_date
												  );
		:NEW.C402_QTY_HISTORY_FL := 'Y';
	END IF;
--
END trg_t402_qty_ordered_upd;
/
