-- @"c:\Database\Triggers\T704A_ACCOUNT_ATTRIBUTE.trg";

create or replace TRIGGER T704A_ACCOUNT_ATTRIBUTE
   BEFORE INSERT OR UPDATE OF C704A_ACCOUNT_ATTRIBUTE_ID,C704_ACCOUNT_ID,C704A_ATTRIBUTE_VALUE,C704A_VOID_FL,C901_ATTRIBUTE_TYPE,C704A_HISTORY_FL
   ON T704A_ACCOUNT_ATTRIBUTE
   FOR EACH ROW

DECLARE

	v_old		   T704A_ACCOUNT_ATTRIBUTE.C704A_ATTRIBUTE_VALUE%TYPE;
	v_new		   T704A_ACCOUNT_ATTRIBUTE.C704A_ATTRIBUTE_VALUE%TYPE;
	--
	v_old_val	   T704A_ACCOUNT_ATTRIBUTE.C704A_ATTRIBUTE_VALUE%TYPE;
	v_new_val	   T704A_ACCOUNT_ATTRIBUTE.C704A_ATTRIBUTE_VALUE%TYPE;
		
	v_attribute_val t941_audit_trail_log.c941_value%TYPE;
	v_audit_trail_id t940_audit_trail.c940_audit_trail_id%TYPE;
	
	v_save_code_name VARCHAR2(20);
	--
BEGIN
--
	v_old		:= NVL(:OLD.C704A_ATTRIBUTE_VALUE,'-999');
	v_new		:= NVL(:NEW.C704A_ATTRIBUTE_VALUE,'-999');

--
   IF (v_old <> v_new)
	THEN
		--
		BEGIN
			SELECT get_rule_value (:OLD.c901_attribute_type, 'AUDIT_TRAIL_ID')
   				INTO v_audit_trail_id
   			FROM dual;		
		END;
		
		--
		IF (v_audit_trail_id IS NOT NULL)
		THEN
			--
			v_old_val := :OLD.C704A_ATTRIBUTE_VALUE;
			v_new_val := :NEW.C704A_ATTRIBUTE_VALUE;
			--
			BEGIN
				SELECT NVL(get_rule_value (:OLD.c901_attribute_type, 'SAVE_REF_NAME'),' ')
	   				INTO v_save_code_name
	   			FROM dual;			
			END;
			--
			IF (v_save_code_name ='REP')
			THEN
					SELECT get_rep_name (:OLD.C704A_ATTRIBUTE_VALUE), get_rep_name (:NEW.C704A_ATTRIBUTE_VALUE)
					  INTO v_old_val, v_new_val
					FROM dual;
			ELSIF ( v_save_code_name ='CODE')
			THEN
					SELECT get_code_name (:OLD.C704A_ATTRIBUTE_VALUE), get_code_name (:NEW.C704A_ATTRIBUTE_VALUE)
					  INTO v_old_val, v_new_val
					FROM dual;
			END IF;
			--
			IF (:OLD.C704A_HISTORY_FL IS NULL)
			THEN
				gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.C704_ACCOUNT_ID
														 , v_old_val
														 , v_audit_trail_id
														 , :OLD.C704A_CREATED_BY 
														 , :OLD.C704A_CREATED_DATE
														  );
			END IF;-- end history check
	
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:NEW.C704_ACCOUNT_ID
													 , v_new_val
													 , v_audit_trail_id
													 , :NEW.C704A_LAST_UPDATED_BY 
													 , :NEW.C704A_LAST_UPDATED_DATE
													  );
			--
			:NEW.C704A_HISTORY_FL := 'Y';
		END IF;
	END IF;
END T704A_ACCOUNT_ATTRIBUTE; 
/   