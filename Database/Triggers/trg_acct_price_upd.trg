/*
---------------------------------------------------------------------------
Purpose: To log updates from the Account Pricing Table
Logic Structure: Everytime the price is updated, then a new row is inserted
-----------------------------------------------------------------------------
*/ 
--@"C:\Database\Triggers\trg_acct_price_upd.trg";
CREATE OR REPLACE TRIGGER trg_acct_price_upd
	BEFORE INSERT OR UPDATE OF c705_unit_price,c705_void_fl
	ON t705_account_pricing
	FOR EACH ROW
DECLARE		        
	v_action  VARCHAR2(20) ;
	v_old_price			   t705_account_pricing.c705_unit_price%TYPE;
	v_new_price			   t705_account_pricing.c705_unit_price%TYPE;
	v_old_account_id	   t705_account_pricing.C704_ACCOUNT_ID%TYPE;
	v_new_account_id	   t705_account_pricing.C704_ACCOUNT_ID%TYPE;
	v_old_acctprice_id		t705_account_pricing.C705_ACCOUNT_PRICING_ID %TYPE;
	v_new_acctprice_id		t705_account_pricing.C705_ACCOUNT_PRICING_ID %TYPE;
	v_old_pnum		   		t705_account_pricing.C205_PART_NUMBER_ID%TYPE;
	v_new_pnum		   		t705_account_pricing.C205_PART_NUMBER_ID%TYPE;
	v_old_voidfl			t705_account_pricing.C705_VOID_FL%TYPE;
	v_new_voidfl			t705_account_pricing.C705_VOID_FL%TYPE;
	v_reftype				varchar2(50);
	v_gpo_cnt NUMBER;
	v_party_id  		t705_account_pricing.c101_party_id%TYPE;
	v_company_id          t705_account_pricing.C1900_COMPANY_ID%TYPE;
	CURSOR accids_cur
	IS
	SELECT c704_account_id acctid,
			c1900_company_id cmpid
       FROM t704_account
      WHERE c101_party_id = v_party_id
        AND c704_void_fl IS NULL;
BEGIN
--
	v_old_price				:= :OLD.c705_unit_price;
	v_new_price				:= :NEW.c705_unit_price;
	v_old_account_id		:= :OLD.C704_ACCOUNT_ID;
	v_new_account_id		:= :NEW.C704_ACCOUNT_ID;
	v_old_acctprice_id		:= :OLD.C705_ACCOUNT_PRICING_ID;
	v_new_acctprice_id		:= :NEW.C705_ACCOUNT_PRICING_ID;
	v_old_pnum				:= :OLD.C205_PART_NUMBER_ID;
	v_new_pnum				:= :NEW.C205_PART_NUMBER_ID;
	v_old_voidfl			:= :OLD.C705_VOID_FL;
	v_new_voidfl			:= :NEW.C705_VOID_FL;
	
	 
	BEGIN
		SELECT c1900_company_id
		  INTO v_company_id
		  FROM t101_party t101 
		 WHERE t101.c101_party_id = v_party_id
		   AND c901_party_type = 4000877 -- Parent account
		   AND c1900_company_id is not null ;
	EXCEPTION WHEN NO_DATA_FOUND THEN
		   v_company_id := NULL;
	END;
	 
	 IF v_company_id IS NULL THEN
	BEGIN
		SELECT c1900_company_id 
 		  INTO v_company_id
  		  FROM t704_account t704 
		 WHERE t704.c704_account_id = v_new_account_id;
	EXCEPTION WHEN NO_DATA_FOUND THEN 
  		 v_company_id := NULL;
	END;
	END IF; 
	 
	IF INSERTING THEN
        v_action := '103122'; -- New one
    ELSIF UPDATING 
    THEN				-- if any of these values are updated, then it as an update to device
	 IF ( (v_old_price <> v_new_price) 
			OR (v_old_acctprice_id <> v_new_acctprice_id) 
			OR (v_old_pnum <> v_new_pnum)
			OR (v_old_voidfl <> v_new_voidfl) 
			OR (:OLD.c705_history_fl <> :NEW.c705_history_fl ))
	THEN
		--	when a part is newly mapped to account history flag value is null,updated to 'Y'
		IF (:OLD.c705_history_fl IS NULL)
		THEN
			-- saving price into t706_account_pricing_log table
			gm_pkg_PRT_currentPrice_txn.gm_sav_account_pricing_log(:OLD.c705_account_pricing_id
													 , v_old_price
													 , :OLD.c705_last_updated_by
													 , :OLD.c7501_account_price_req_dtl_id
													 , v_company_id
													 );													  
			:NEW.c705_history_fl := 'Y';
		END IF;
		-- updating with new price into t706_account_pricing_log table
		gm_pkg_PRT_currentPrice_txn.gm_sav_account_pricing_log(:OLD.c705_account_pricing_id
													 , v_new_price
													 , :NEW.c705_last_updated_by
													 , :NEW.c7501_account_price_req_dtl_id
													 , v_company_id
													 );

		-- WHEN THE UNIT PRICE IS UPDATED, THE OLD PRICE WILL BE UPDATED BACK TO THE OLD_PRICE COLUMN.
		:NEW.C705_OLD_PRICE :=v_old_price;
		
		v_action  := '103123'; -- Updated 
		END IF;
	END IF;
	
		IF v_action IS NOT NULL THEN 
		
	       --
	       v_party_id:=:NEW.c101_party_id;
	      --Check GPO account
			SELECT COUNT(1)   
			 INTO v_gpo_cnt 
			FROM t740_gpo_account_mapping
			WHERE c101_party_id = v_party_id
			AND C740_VOID_FL IS NULL;
			
			IF v_gpo_cnt > 0 --gpo
			THEN
				
				v_reftype:='4000524';
				 gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (v_party_id ,v_reftype, v_action,
	        				'103097', '103100', NVL (:NEW.c705_created_by, :NEW.c705_last_updated_by ), NULL,'Forceupdate',v_company_id) ;
			ELSE -- account
				v_reftype:='4000526';
				FOR v_accids IN accids_cur
				LOOP
					 gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (v_accids.acctid ,v_reftype, v_action,
		        				'103097', '103100', NVL (:NEW.c705_created_by, :NEW.c705_last_updated_by ), NULL,'Forceupdate',v_accids.cmpid) ;
				END LOOP;
			END IF;
		
		 /* -- 103097 English, 103100 Product Catalog , 4000526 - AcctPrice 4000524 - gpoprice (Ref Type)
		  	Select  DECODE(v_new_account_id,'','4000524','4000526') into v_reftype from dual;
		   gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (NVL(:NEW.c101_party_id,:NEW.c704_account_id) ,v_reftype, v_action,
	        	        				'103097', '103100', NVL (:NEW.c705_created_by, :NEW.c705_last_updated_by ), NULL,'Forceupdate',v_company_id) ;*/
	     END IF;

--
END trg_acct_price_upd;
/