/* Formatted on 2010/03/18 09:51 (Formatter Plus v4.8.0) */


CREATE OR REPLACE PROCEDURE gm_save_partnumber_price (
	p_eprice	IN	 t205_part_number.c205_equity_price%TYPE
  , p_lprice	IN	 t205_part_number.c205_loaner_price%TYPE
  , p_cprice	IN	 t205_part_number.c205_consignment_price%TYPE
  , p_liprice	IN	 t205_part_number.c205_list_price%TYPE
  , p_partnum	IN	 t205_part_number.c205_part_number_id%TYPE
  , p_userid	IN	 t205_part_number.c205_created_by%TYPE
)
AS
  v_company_id NUMBER;
BEGIN

            SELECT get_compid_frm_cntx()
	          INTO v_company_id 
	          FROM dual;
	--Below update query needs to remove, once the fetch related query for part price changed to T2052_PART_PRICE_MAPPING table.          
	UPDATE t205_part_number
	   SET c205_equity_price = p_eprice
		 , c205_loaner_price = p_lprice
		 , c205_consignment_price = p_cprice
		 , c205_list_price = p_liprice
		 , c205_last_updated_by = p_userid
		 , c205_last_updated_date = CURRENT_DATE
		 , C205_PART_PRICE_UPDATED_DATE = CURRENT_DATE
		 , c205_part_price_updated_by =  p_userid
	 WHERE c205_part_number_id = p_partnum;
	 
	--by using the below procedure we are saving the price for the part in T2052_PART_PRICE_MAPPING.
	        gm_pkg_pd_part_pricing.gm_sav_part_pricing(p_partnum, p_eprice, p_lprice, p_cprice, p_liprice, v_company_id, p_userid);
	        
END gm_save_partnumber_price;
/
