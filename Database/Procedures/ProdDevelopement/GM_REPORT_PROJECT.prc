CREATE OR REPLACE PROCEDURE GM_REPORT_PROJECT
(
 	   p_column    IN  VARCHAR2,
	   p_message   OUT VARCHAR2,
	   p_recordset OUT Types.cursor_type)
AS
/*
      Description           :This procedure is called for Project Reporting
      Parameters            :p_column
                            :p_recordset
*/
v_str VARCHAR2(2000);
	v_company_id NUMBER;
BEGIN
p_message := p_column;
--
   	OPEN p_recordset FOR
		SELECT	T202.C202_PROJECT_ID ID,	C202_PROJECT_NM NAME,
				GET_USER_NAME(C202_CREATED_BY) PMANAGER,
				C202_CREATED_DATE CDATE,
				C202_INSERT_ID INSID,
				C202_INSERT_REV INSREV
		FROM	T202_PROJECT T202 ,T2021_PROJECT_COMPANY_MAPPING T2021
		WHERE	NVL(C202_RELEASE_FOR_SALE,'Y')  = DECODE(p_column, 'APRICE', 'Y', NVL(C202_RELEASE_FOR_SALE,'Y') ) 
		  AND   T2021.C1900_COMPANY_ID = v_company_id
		  AND   T202.C202_PROJECT_ID = T2021.C202_PROJECT_ID
		  AND   T2021.C2021_VOID_FL IS NULL
		ORDER BY DECODE(p_column,'NAME',C202_PROJECT_NM,'ID',T202.C202_PROJECT_ID,'PMAN',C202_CREATED_BY,'CDATE',C202_CREATED_DATE, 'APRICE', C202_PROJECT_NM);
		
		SELECT get_compid_frm_cntx() 
		  INTO v_company_id  
		  FROM DUAL;
	 --
	 EXCEPTION
	 WHEN OTHERS THEN
	          p_message := '1000';
END GM_REPORT_PROJECT;
/

