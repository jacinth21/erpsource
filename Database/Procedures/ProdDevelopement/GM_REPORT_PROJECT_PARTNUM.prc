CREATE OR REPLACE PROCEDURE GM_REPORT_PROJECT_PARTNUM
(
 	   p_projid    IN  VARCHAR2,
	   p_message   OUT VARCHAR2,
	   p_recordset OUT Types.cursor_type)
AS
/*
      Description           :This procedure is called for Project Reporting
      Parameters            :p_column
                            :p_recordset
*/
	v_company_id t1900_company.c1900_company_id%TYPE;
BEGIN
 	SELECT get_compid_frm_cntx()
    	INTO v_company_id
    	FROM DUAL;
    	
   	OPEN p_recordset FOR
			  SELECT T205.C205_Part_Number_Id ID, T205.C205_Part_Num_Desc NAME
   FROM T205_Part_Number T205, (
         SELECT T2023.C205_Part_Number_Id ID
           FROM T2023_PART_COMPANY_MAPPING T2023, T205_Part_Number T205A
          WHERE T205A.C202_Project_Id     = p_projid
            AND T205A.C205_PART_NUMBER_ID = T2023.C205_Part_Number_Id
            AND T2023.C2023_Void_Fl      IS NULL
            AND T2023.C1900_COMPANY_ID    = v_company_id
            
          UNION
         SELECT T209.C205_Part_Number_Id ID
           FROM T209_GENERIC_PARTNUMBER T209
          WHERE T209.C202_Project_Id = p_projid
          
          UNION
         SELECT T2021.C205_Part_Number_Id ID
           FROM T2021_Part_Project_Mapping T2021
          WHERE T2021.C202_Project_Id = p_projid
          AND   T2021.C2021_VOID_FL IS NULL
    )
    Part
  WHERE T205.c205_part_number_id = Part.Id
  ORDER BY T205.c205_part_number_id;--Added for PMT-51082,to sort by part
		/*SELECT	C205_PART_NUMBER_ID ID,	C205_PART_NUM_DESC NAME
		FROM
			 T205_PART_NUMBER
		WHERE
			  C202_PROJECT_ID = p_projid
		ORDER BY C205_PART_NUMBER_ID;
		*/
	 EXCEPTION
	 WHEN OTHERS THEN
	          p_message := '1000';
END GM_REPORT_PROJECT_PARTNUM;
/

