CREATE OR REPLACE PROCEDURE GM_SAVE_DUPL_PARTNUMBER
(
	   p_PartNum 			IN T209_GENERIC_PARTNUMBER.C205_PART_NUMBER_ID%TYPE,
	   p_ProjId 			IN T209_GENERIC_PARTNUMBER.C202_PROJECT_ID%TYPE,
	   p_ParentProjId 		IN T209_GENERIC_PARTNUMBER.C202_PROJECT_ID%TYPE,
	   p_UserId				IN T209_GENERIC_PARTNUMBER.C209_CREATED_BY%TYPE,
	   p_message 			OUT  VARCHAR2
)
AS
v_GenPartNum NUMBER;
v_FlValue VARCHAR2(1);
BEGIN
	 SELECT C205_MULTIPLE_PROJ_FL INTO v_FlValue FROM T205_PART_NUMBER WHERE C205_PART_NUMBER_ID = p_PartNum;
	 IF v_FlValue = 'Y' THEN
		 BEGIN
			  -- Insert a row for the Duplicate Project
		 	  SELECT S209_GENERIC_PARTNUM.NEXTVAL INTO v_GenPartNum FROM DUAL;
              INSERT INTO T209_GENERIC_PARTNUMBER (
			  	  	C209_GENERIC_PARTNUM_ID,
			  		C205_PART_NUMBER_ID,
					C202_PROJECT_ID,
					C209_CREATED_BY,
					C209_CREATED_DATE
			  )
			  VALUES (
			  	    v_GenPartNum,
			   		p_PartNum,
					p_ProjId,
					p_UserId,
					SYSDATE
			  );
			  EXCEPTION WHEN OTHERS
      	 	  THEN
                       p_message := SQLERRM;
              RETURN;
         END;
	 ELSE
	 	  BEGIN
			  UPDATE T205_PART_NUMBER
				SET
				  	 C205_MULTIPLE_PROJ_FL = 'Y',
				  	 C205_LAST_UPDATED_BY = p_UserId,
				  	 C205_LAST_UPDATED_DATE = SYSDATE
				WHERE
				  	 C205_PART_NUMBER_ID = p_PartNum;
			  -- Insert a row for the Parent Project
		 	  SELECT S209_GENERIC_PARTNUM.NEXTVAL INTO v_GenPartNum FROM DUAL;
              INSERT INTO T209_GENERIC_PARTNUMBER (
			  	  	C209_GENERIC_PARTNUM_ID,
			  		C205_PART_NUMBER_ID,
					C202_PROJECT_ID,
					C209_CREATED_BY,
					C209_CREATED_DATE
			  )
			  VALUES (
			  	    v_GenPartNum,
			   		p_PartNum,
					p_ParentProjId,
					p_UserId,
					SYSDATE
			  );
			  -- Insert a row for the Duplicate Project
		 	  SELECT S209_GENERIC_PARTNUM.NEXTVAL INTO v_GenPartNum FROM DUAL;
              INSERT INTO T209_GENERIC_PARTNUMBER (
			  	  	C209_GENERIC_PARTNUM_ID,
			  		C205_PART_NUMBER_ID,
					C202_PROJECT_ID,
					C209_CREATED_BY,
					C209_CREATED_DATE
			  )
			  VALUES (
			  	    v_GenPartNum,
			   		p_PartNum,
					p_ProjId,
					p_UserId,
					SYSDATE
			  );
			  EXCEPTION WHEN OTHERS
      	 	  THEN
                       p_message := SQLERRM;
              RETURN;
		  END;
     END IF;
END GM_SAVE_DUPL_PARTNUMBER;
/

