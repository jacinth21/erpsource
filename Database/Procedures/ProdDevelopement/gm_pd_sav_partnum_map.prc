/* Formatted on 2009/09/15 17:05 (Formatter Plus v4.8.0) */
--@"C:\DATABASE\Procedures\ProdDevelopement\gm_pd_sav_partnum_map.prc"


CREATE OR REPLACE PROCEDURE gm_pd_sav_partnum_map (
	p_partnum	 IN   t205_part_number.c205_part_number_id%TYPE
  , p_type		 IN   VARCHAR2
  , p_inputstr	 IN   VARCHAR2
  , p_userid     IN   t205_part_number.c205_created_by%TYPE
  , p_loop	 	 IN   NUMBER
)
AS
	v_strlen	   NUMBER := NVL (LENGTH (p_inputstr), 0);
	v_string	   VARCHAR2 (30000) := p_inputstr;
	v_frompnum	   VARCHAR2 (20);
	v_topnum	   VARCHAR2 (20);
	v_type		   VARCHAR2 (20);
	v_qty		   VARCHAR2 (20);
	v_substring    VARCHAR2 (1000);
	v_cyclpnum	   VARCHAR2 (20);
	v_sub_part_to_order NUMBER;
	v_multipro_fl	t205_part_number.c205_crossover_fl%TYPE;
	
BEGIN
	IF (p_loop = 1)
	THEN
		IF (p_type = 'pnum')
		THEN
		UPDATE t205_part_number
				   SET c205_sub_component_fl = NULL
		             , c205_last_updated_date = SYSDATE
                     , c205_last_updated_by = p_userid
				 WHERE c205_part_number_id IN (SELECT c205_to_part_number_id
	    FROM t205a_part_mapping
	    WHERE c205_from_part_number_id = p_partnum
	      AND c205a_void_fl is null);  ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping

			DELETE FROM t205a_part_mapping
				  WHERE c205_from_part_number_id = p_partnum;
			
			--check for the input parent part as multi part or not.
			BEGIN	
				select c205_crossover_fl 
				  into v_multipro_fl
				  from t205_part_number  
				 WHERE c205_part_number_id = p_partnum;
			EXCEPTION WHEN OTHERS
			THEN
				v_multipro_fl :=null;
			END ;
				 
		ELSE
		UPDATE t205_part_number
			   SET c205_sub_component_fl = NULL
	             , c205_last_updated_date = SYSDATE
                           , c205_last_updated_by = p_userid
			 WHERE c205_part_number_id = p_partnum;

			DELETE FROM t205a_part_mapping
				  WHERE c205_to_part_number_id = p_partnum;
		END IF;
	END IF;
	

	IF v_strlen > 0
	THEN
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_frompnum	:= NULL;
			v_topnum	:= NULL;
			v_qty		:= NULL;
			v_type		:= NULL;
			v_frompnum	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_topnum	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_qty		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_type		:= v_substring;

			INSERT INTO t205a_part_mapping
						(c205a_part_mapping_id, c205_from_part_number_id, c205_to_part_number_id, c205a_qty, c901_type,c205a_void_fl
						)
				 VALUES (s205a_part_mapping.NEXTVAL, v_frompnum, v_topnum, v_qty, v_type,NULL
						);

			SELECT count(1) INTO v_cyclpnum FROM
											(SELECT T205A.*, CONNECT_BY_ISCYCLE CYCL
											 FROM T205A_PART_MAPPING T205A
			START WITH C205_FROM_PART_NUMBER_ID = v_frompnum
			CONNECT BY NOCYCLE PRIOR T205A.C205_TO_PART_NUMBER_ID = T205A.C205_FROM_PART_NUMBER_ID
			AND T205A.C205A_VOID_FL IS NULL) TEMP    ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
			WHERE TEMP.CYCL > 0;

			IF v_cyclpnum > 0
			THEN
			raise_application_error (-20175, '');
			END IF;

			UPDATE t205_part_number
			   SET c205_sub_component_fl = 'Y'
                 , c205_last_updated_date = SYSDATE
			 WHERE c205_part_number_id = v_topnum;
			 
			 SELECT COUNT(1) INTO v_sub_part_to_order
			 FROM T205D_SUB_PART_TO_ORDER
			 WHERE C205D_VOID_FL IS NULL
			 AND   C205_PART_NUMBER_ID = v_topnum;
			 
			 --If the Part is a sub-part to order and the parent parent part is multi-project,
			 --mark the current part also as multi-project part.
			 IF v_multipro_fl IS NOT NULL AND v_sub_part_to_order >0 
			 THEN
			 	UPDATE t205_part_number
				   SET c205_crossover_fl = 'Y'
	                 , c205_last_updated_date = SYSDATE
				 WHERE c205_part_number_id = v_topnum;
			 
			 END IF;
		END LOOP;
		-- to update the part number UDI parameter details.
		gm_pkg_pd_sav_udi.gm_sav_qa_parent_dmdi_param (p_partnum, p_userid);
		gm_pkg_pd_sav_udi.gm_sav_sub_com_bulk_parameter (p_partnum, p_userid);
	END IF;
END gm_pd_sav_partnum_map;
/
