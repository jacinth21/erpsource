/* Formatted on 2007/06/29 11:34 (Formatter Plus v4.8.0) */
---@"c:\database\Procedures\ProdDevelopement\GM_SAVE_SETPARTMAP.prc"
CREATE OR REPLACE PROCEDURE gm_save_setpartmap (
	p_setid    IN		t208_set_details.c207_set_id%TYPE
  , p_str	   IN		VARCHAR2
  , p_userid   IN		VARCHAR2
  , p_errmsg   OUT		VARCHAR2
)
AS
--
/********************************************************************************************
 * Description	:This procedure is used for saving set list info
 * Change Details:
 *		 James	Nov2,2006	Changed Formatting according to Standards
		 James	Nov2,2006	Changes for Sales Consignment and new layout
*********************************************************************************************/
--
	v_strlen	   NUMBER := NVL (LENGTH (p_str), 0);
	v_string	   VARCHAR2 (30000) := p_str;
	v_pnum		   VARCHAR2 (20);
	v_qty		   NUMBER;
	v_inset_fl	   CHAR (1);
	v_critic_fl    CHAR (1);
	v_critic_tp    NUMBER;
	v_mid		   NUMBER;
	v_fl		   CHAR (1);
	v_substring    VARCHAR2 (1000);
	v_msg		   VARCHAR2 (1000);
	v_critic_qty   NUMBER;
	v_critic_tag   VARCHAR2 (100);
	v_tmp_partid   VARCHAR2 (20);
  v_unit_type   NUMBER;
  v_count NUMBER;
  v_void_fl VARCHAR2 (20);
  v_tissue_fl  CHAR (1); -- PC-5506 Tissue Set update from set mapping
--
BEGIN
	IF v_strlen > 0
	THEN
		
		-- Populating the temp table,so we can compare if the data is modified after the insert / update.
		DELETE FROM MY_TEMP_T208_SET_DETAILS;
		INSERT INTO MY_TEMP_T208_SET_DETAILS
		SELECT
			C208_SET_DETAILS_ID,
			C207_SET_ID,
			C205_PART_NUMBER_ID,
			C208_SET_QTY,
			C208_INSET_FL,
			C208_VOID_FL,
			C208_CRITICAL_QTY,
			C208_CRITICAL_TAG,
			C208_CRITICAL_FL,
			C901_CRITICAL_TYPE,
			C208_LAST_UPDATED_DATE,
			C208_LAST_UPDATED_BY,
			C901_UNIT_TYPE
			FROM t208_set_details
			WHERE c207_set_id = p_setid;
			
			
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			--
			v_pnum		:= NULL;
			v_qty		:= NULL;
			v_inset_fl	:= NULL;
			v_critic_fl := NULL;
			v_critic_qty := NULL;
			v_critic_tp := NULL;
			v_critic_tag := NULL;
      v_unit_type := NULL;
			v_fl		:= NULL;
			v_mid		:= NULL;
			--
			v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_qty		:= TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1));
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_inset_fl	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_critic_fl := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_critic_qty := TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1));
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_critic_tp := TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1));
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
      v_unit_type := TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1));
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_critic_tag := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_fl		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_mid		:= TO_NUMBER (v_substring);
BEGIN
				SELECT t205.c205_part_number_id
				  INTO v_tmp_partid
				  FROM t205_part_number t205
				 WHERE t205.c205_part_number_id = v_pnum;
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					raise_application_error (-20012, '');
			END;
			-- validate the duplicate entry
			 SELECT COUNT (1)
			   INTO v_count
			   FROM t208_set_details
			  WHERE c207_set_id         = p_setid
			    AND c205_part_number_id = v_pnum;
			--
			IF v_count > 1 THEN
			    raise_application_error ( - 20999, 'Duplicate records found for the part #: '||v_pnum) ;
			END IF;
			--
			IF (v_mid = 0 OR v_mid IS NULL) AND v_count <> 0 THEN
			     SELECT c208_set_details_id, c208_void_fl
			       INTO v_mid, v_void_fl
			       FROM t208_set_details
			      WHERE c207_set_id         = p_setid
			        AND c205_part_number_id = v_pnum;
			    -- to set the update flag
			    v_fl := 'U';
			    --
			    IF v_void_fl IS NOT NULL THEN
			        -- to update the void flag as null
			         UPDATE t208_set_details
			        SET c208_void_fl            = NULL, c208_last_updated_by = p_userid, c208_last_updated_date = SYSDATE
			          WHERE c208_set_details_id = v_mid;
			    END IF; -- end v_void_fl check
			END IF; -- end v_mid check
			--
			IF v_fl = 'I'
			THEN
				INSERT INTO t208_set_details
							(c208_set_details_id, c207_set_id, c205_part_number_id, c208_set_qty, c208_inset_fl
						   , c208_critical_fl, c208_critical_qty, c901_critical_type, c208_critical_tag, c901_unit_type
							)
					 VALUES (s208_setmap_id.NEXTVAL, p_setid, v_pnum, v_qty, v_inset_fl
						   , v_critic_fl, v_critic_qty, v_critic_tp, v_critic_tag, v_unit_type
							);
			ELSIF v_fl = 'U'
			THEN
				UPDATE t208_set_details
				   SET c208_set_qty = v_qty
					 , c208_inset_fl = v_inset_fl
					 , c208_critical_fl = v_critic_fl
					 , c208_critical_qty = v_critic_qty
					 , c901_critical_type = v_critic_tp
					 , c208_critical_tag = v_critic_tag
           , c901_unit_type = v_unit_type
				 WHERE c208_set_details_id = v_mid;
			END IF;
			-- PC-5506 Tissue Set update from set mapping
			SELECT COUNT(1) INTO v_tissue_fl FROM t205_part_number WHERE c205_product_material = '100845' AND c205_part_number_id = v_pnum;
			IF v_tissue_fl > 0 THEN
			    UPDATE t207_set_master
			    SET
			        c207_last_updated_date = current_date,
			        c207_last_updated_by = p_userid,
			        c207_tissue_fl = decode(v_tissue_fl, '1', 'Y', c207_tissue_fl)
			    WHERE
			        c207_set_id = p_setid;
			END IF;			
		END LOOP;

		--
		UPDATE t207_set_master
		   SET c207_last_updated_date = SYSDATE
			 , c207_last_updated_by = p_userid
		 WHERE c207_set_id = p_setid;
	ELSE
		p_errmsg	:= 'p_str is empty';
	END IF;
--This procedure will check if the Set details is modified and will mark the set as updated for Product Catalog.
	gm_pkg_pdpc_prodcattxn.gm_chk_set_detail_update(p_setid,p_userid);
END gm_save_setpartmap;
/
