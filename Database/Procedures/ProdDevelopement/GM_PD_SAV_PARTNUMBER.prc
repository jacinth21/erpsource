/* Formatted on 2011/02/08 18:48 (Formatter Plus v4.8.0) */
--@"C:\DATABASE\Procedures\ProdDevelopement\gm_pd_sav_partnumber.prc";

CREATE OR REPLACE PROCEDURE gm_pd_sav_partnumber (
	p_partnum		IN		 t205_part_number.c205_part_number_id%TYPE
  , p_projid		IN		 t205_part_number.c202_project_id%TYPE
  , p_partdesc		IN		 t205_part_number.c205_part_num_desc%TYPE
  , p_prodfly		IN		 t205_part_number.c205_product_family%TYPE
  , p_prodmat		IN		 t205_part_number.c205_product_material%TYPE
  , p_class 		IN		 t205_part_number.c205_product_class%TYPE
  , p_activefl		IN		 t205_part_number.c205_active_fl%TYPE
  , p_measdev		IN		 t205_part_number.c205_measuring_dev%TYPE
  , p_matspec		IN		 t205_part_number.c205_material_spec%TYPE
  , p_drawnum		IN		 t205_part_number.c205_part_drawing%TYPE
  , p_revnum		IN		 t205_part_number.c205_rev_num%TYPE
  , p_userid		IN		 t205_part_number.c205_created_by%TYPE
  , p_insprev		IN		 t205_part_number.c205_inspection_rev%TYPE
  , p_tracimpl		IN		 t205_part_number.c205_tracking_implant_fl%TYPE
  , p_partstatus	IN		 t205_part_number.c901_status_id%TYPE
  , p_relflag		IN		 t205_part_number.c205_release_for_sale%TYPE
  , p_supplyflag	IN		 t205_part_number.c205_supply_fl%TYPE
  , p_subassmflag	IN		 t205_part_number.c205_sub_asmbly_fl%TYPE
  , p_salesgroup	IN		 t208_set_details.c207_set_id%TYPE
  , p_insertid		IN		 t205_part_number.c205_insert_id%TYPE  
  , p_uom			IN		 t205_part_number.c901_uom%TYPE
  , p_minaccptrev	IN		 t205_part_number.c205_min_accpt_rev_lel%TYPE  
  , p_rfs			IN		 VARCHAR2
  , p_part_attr		IN		 VARCHAR2
  , p_taggable_fl	IN		 t205d_part_attribute.c205d_attribute_value%TYPE
  , p_detail_desc	IN		 t205_part_number.c205_part_num_dtl_desc%TYPE
  , p_part_info  	OUT 	 TYPES.cursor_type
)
AS
	v_set_details_id t208_set_details.c208_set_details_id%TYPE;
	v_curr_rev	   t205_part_number.c205_rev_num%TYPE;
	v_flag		   CHAR (1);
	v_count 	   NUMBER;
	v_relflag	   t205_part_number.c205_release_for_sale%TYPE;
	v_rfs		   VARCHAR2 (1000) := '';	
	v_class 	   t205_part_number.c205_product_class%TYPE;
	v_spec		   t205_part_number.c205_material_spec%TYPE;
	v_drawnum	   t205_part_number.c205_part_drawing%TYPE;
    v_active_fl    t205_part_number.c205_part_drawing%TYPE;
    v_publish_fl   VARCHAR2 (20);
	v_priced_type  VARCHAR2 (100);
	v_list_price   NUMBER;
 	v_loaner_price NUMBER;
	v_consignment_price  NUMBER;
	v_equity_price NUMBER;
	v_cur_set_id   t207_set_master.c207_set_id%TYPE;
	v_force_upd  	VARCHAR2(1):='N';
	v_action  VARCHAR2(10) ;
	v_cur_sys_pub_pc NUMBER;
	v_new_sys_pub_pc NUMBER;
	v_parent_cnt NUMBER;
	v_new_rfs_cnt  NUMBER;
  	v_old_rfs_cnt  NUMBER;
  	v_system_id T207_SET_MASTER.C207_SET_ID%TYPE;
	v_part_status  NUMBER;
	v_void_old_cnt NUMBER := 0; 
    v_void_new_cnt NUMBER := 0; 
	v_old_cnt NUMBER := 0;
	v_new_cnt NUMBER := 0;
	v_cmp_cnt NUMBER;
	--
	v_old_prod_material t205_part_number.c205_product_material%TYPE;
	v_old_prod_family t205_part_number.c205_product_family%TYPE;
	v_old_prod_class t205_part_number.c205_product_class%TYPE;
	v_company_id NUMBER;
	v_new_part VARCHAR2(1) :='N';
	--
	-- Cursro to get all open Work Orders for the Part modified
	CURSOR work_order
	IS
		SELECT DISTINCT t205a.c205_from_part_number_id ppnum, t402.c402_work_order_id woid
					  , t205a.c205_to_part_number_id spart
				   FROM t402_work_order t402, t205a_part_mapping t205a
				  WHERE (t205a.c205_to_part_number_id = p_partnum OR t402.c205_part_number_id = p_partnum)
					AND t402.c205_part_number_id = t205a.c205_from_part_number_id(+)
					AND t402.c402_status_fl < 3
					AND t402.c402_void_fl IS NULL
					AND t205a.c205a_void_fl is null;   ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping

	-- Cursor to get the sub-components of the part
	CURSOR pop_val
	IS
		SELECT t205.c205_part_number_id subid, t205.c205_rev_num rev
		  FROM t205a_part_mapping t205a, t205_part_number t205
		 WHERE t205a.c205_from_part_number_id = p_partnum
		   AND t205a.c205_to_part_number_id = t205.c205_part_number_id
		   AND t205a.c901_type IN (30031, 30034);
BEGIN
	BEGIN
		SELECT c205_rev_num, c205_release_for_sale , c205_product_class, c205_material_spec mspec
		, c205_part_drawing draw,c205_active_fl, c205_product_material
		, c205_product_family, c205_product_class
		  INTO v_curr_rev, v_relflag , v_class, v_spec, v_drawnum,v_active_fl
		, v_old_prod_material, v_old_prod_family, v_old_prod_class
		  FROM t205_part_number
		 WHERE c205_part_number_id = p_partnum FOR UPDATE;--Part Number Setup screen is timing out while saving data--(PMT-27473);
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_curr_rev	:= p_revnum;			
			v_class 	:= p_class;
			v_spec		:= p_matspec;
			v_drawnum	:= p_drawnum;
			v_active_fl := NULL;
			v_old_prod_material := -999;
			v_old_prod_family := '-999';
			v_old_prod_class := -999;
	END;
	
	SELECT get_compid_frm_cntx() 
	  INTO v_company_id  
	  FROM DUAL;

/*	SELECT gm_pkg_pd_partnumber.get_rfs_info(p_partnum)
	INTO v_rfs
	FROM DUAL;

IF (v_rfs is null and p_rfs is not null)

THEN

	SELECT COUNT (0)
	  INTO v_count
	  FROM t4010_group t4010, t4011_group_detail t4011
	 WHERE t4010.c4010_void_fl IS NULL
		AND t4010.c4010_publish_fl IS NOT NULL
		AND t4010.c901_priced_type = 52110
		AND t4010.c901_type = 40045
		AND t4010.c901_group_type IS NOT NULL
		AND t4010.c4010_group_id = t4011.c4010_group_id
		AND t4011.c205_part_number_id = p_partnum;	--'101.117';

	IF v_count < 1
	THEN
		raise_application_error ('-20283', '');
	END IF;

END IF;
*/
	  
	  
/* PMT-32340 Work Order Revision Update Dashboard
 * To remove the OUT parameter (p_message, p_recordset).
   Move the cursor code to new procedure (GM_PKG_WO_TXN.gm_fch_open_wo_details)
   Add the OUT parameter (cursor type) - p_out_part_num_attribute_change_cur
   Before update the Part number details -
   Select the data from T205 - Product class, Part Drawing , Material specification, Revision number  
 */
-- 		
	open  p_part_info
	for
	select c205_product_class pclass,
		   c205_part_drawing pdraw,
		   c205_material_spec pmaterialspec,
		   c205_rev_num prevnum
	 from    t205_part_number 
	 WHERE c205_part_number_id = p_partnum;
	 
	 
	UPDATE t205_part_number
	   SET c202_project_id = p_projid
		 , c205_part_num_desc = p_partdesc
		 , c205_product_family = p_prodfly
		 , c205_product_material = p_prodmat
		 , c205_product_class = p_class
		 , c205_active_fl = p_activefl
		 , c205_measuring_dev = p_measdev
		 , c205_material_spec = p_matspec
		 , c205_part_drawing = p_drawnum
		 , c205_rev_num = p_revnum
		 , c205_last_updated_by = p_userid
		 , c205_last_updated_date = CURRENT_DATE	
		 , c205_inspection_rev = p_insprev
		 , c205_tracking_implant_fl = p_tracimpl
		 , c901_status_id = DECODE (p_partstatus, 0, NULL, p_partstatus)
		 , c205_release_for_sale = p_relflag
		 , c205_supply_fl = p_supplyflag
		 , c205_sub_asmbly_fl = p_subassmflag
		 , c205_insert_id = p_insertid		 
		 , c901_uom = DECODE (p_uom, 0, NULL, p_uom)
		 , c205_min_accpt_rev_lel = p_minaccptrev
		 , c205_part_num_dtl_desc = p_detail_desc
	 WHERE c205_part_number_id = p_partnum;

	IF (SQL%ROWCOUNT = 0)
	THEN
		INSERT INTO t205_part_number
					(c205_part_number_id, c202_project_id, c205_part_num_desc, c205_product_family
				   , c205_product_material, c205_product_class, c205_active_fl, c205_measuring_dev
				   , c205_material_spec, c205_part_drawing, c205_rev_num, c205_created_by, c205_created_date
				   ,c205_inspection_rev, c205_tracking_implant_fl, c901_status_id
				   , c205_release_for_sale, c205_supply_fl, c205_sub_asmbly_fl, c205_insert_id 
				   , c901_uom, c205_min_accpt_rev_lel, c205_part_num_dtl_desc
					)
			 VALUES (p_partnum, p_projid, p_partdesc, p_prodfly
				   , p_prodmat, p_class, p_activefl, p_measdev
				   , p_matspec, p_drawnum, p_revnum, p_userid, CURRENT_DATE
				   , p_insprev, p_tracimpl, 20365
				   , p_relflag, p_supplyflag, p_subassmflag, p_insertid 
				   , DECODE (p_uom, 0, NULL, p_uom), p_minaccptrev, p_detail_desc
					);	 -- hard coded the status to 20365 (created) during the insert
		
					v_new_part := 'Y';
	END IF;
	
		SELECT count(1) ,COUNT(c205d_void_fl)
			INTO  v_old_cnt ,v_void_old_cnt  
			FROM t205_part_number t205, t205d_part_attribute t205d 
			WHERE t205.c205_part_number_id = t205d.c205_part_number_id
			AND t205.c205_part_number_id = p_partnum
			AND t205.c205_active_fl IS NOT NULL
			AND t205d.c901_attribute_type = 80180
			AND t205.c205_product_family IN ( SELECT C906_RULE_VALUE
											   FROM T906_RULES
											  WHERE C906_RULE_ID     = 'PART_PROD_FAMILY'
											    AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
											    AND c906_void_fl    IS NULL);
	
	/*The Below Statement is added For MDO-88, Only when Part is created For First Time then There should an be an Entry into  T2001_master_data_updates
     * From This Procedure.
     */
		 	SELECT count(1) 
			INTO  v_old_rfs_cnt   
			FROM t205_part_number t205, t205d_part_attribute t205d 
			WHERE t205.c205_part_number_id = t205d.c205_part_number_id
			AND t205.c205_part_number_id = p_partnum
			AND c901_status_id = 20367
			AND t205.c205_active_fl IS NOT NULL
			AND t205d.c901_attribute_type = 80180
			AND t205.c205_product_family IN (SELECT C906_RULE_VALUE
											   FROM T906_RULES
											  WHERE C906_RULE_ID     = 'PART_PROD_FAMILY'
											    AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
											    AND c906_void_fl    IS NULL)
			AND t205d.c205d_void_fl IS NULL;
		 
		gm_pkg_pd_partnumber.gm_sav_part_qa_attribute (p_partnum, p_part_attr, p_rfs, p_userid, p_taggable_fl, p_prodfly);	
	  --The Below Code is Added for MDO-88 Whenever New Part is Added and Release For Sale is Given Then There Should be an Entry in T2001_master_data_updates.
            v_system_id := get_set_id_from_part(p_partnum);
           	SELECT count(1) 
			INTO  v_new_rfs_cnt   
			FROM t205_part_number t205, t205d_part_attribute t205d 
			WHERE t205.c205_part_number_id = t205d.c205_part_number_id
			AND t205.c205_part_number_id = p_partnum
			AND c901_status_id = 20367
			AND t205.c205_active_fl IS NOT NULL
			AND t205d.c901_attribute_type = 80180
			AND t205.c205_product_family IN (SELECT C906_RULE_VALUE
											   FROM T906_RULES
											  WHERE C906_RULE_ID     = 'PART_PROD_FAMILY'
											    AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
											    AND c906_void_fl    IS NULL)
			AND t205d.c205d_void_fl IS NULL;
		-- if we change any thing in release for sale(RFS) ,in that case we are caliculating  void fl count and count for that part .
		-- If void fl differs from old to new ,in this case we are updating the T2001_master_data_updates table.
			SELECT count(1) ,COUNT(c205d_void_fl)
			INTO  v_new_cnt ,v_void_new_cnt  
			FROM t205_part_number t205, t205d_part_attribute t205d 
			WHERE t205.c205_part_number_id = t205d.c205_part_number_id
			AND t205.c205_part_number_id = p_partnum
			AND t205.c205_active_fl IS NOT NULL
			AND t205d.c901_attribute_type = 80180
			AND t205.c205_product_family IN (SELECT C906_RULE_VALUE
											   FROM T906_RULES
											  WHERE C906_RULE_ID     = 'PART_PROD_FAMILY'
											    AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
											    AND c906_void_fl    IS NULL);
			
			
						--check part's project is in exclude project list
                    SELECT count(1) INTO v_cmp_cnt
				      FROM t2023_part_company_mapping 
				     WHERE c205_part_number_id = p_partnum
				       AND c1900_company_id = 1000
				       AND c2023_void_fl IS NULL;	
			/*New part insertion will not have value in t2023.
			So, New part flag 'Y' consider as update for part attribute.
			If active flag is changed and rfs count is greater than zero then consider as update for part attribute.
			*/ 
			IF ((v_old_cnt <> v_new_cnt OR v_void_old_cnt <> v_void_new_cnt) 
			AND (v_cmp_cnt > 0 OR v_new_part = 'Y'))
			OR (NVL(v_active_fl,'N') <> NVL(p_activefl,'N') AND  v_new_rfs_cnt > 0 )
			THEN
    		-- 4000827 Part ATTRIBUTE , 103097 English, 103100 Product Catalog ,103123 UPDATED
    				gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd(p_partnum
							     , '4000827', 103123 
							     ,'103097','103100'
							     ,CURRENT_DATE
							     ,v_system_id
							     ,'Y'
							     );
	   		END IF;                               
			
			IF ( v_new_rfs_cnt > 0 AND v_old_rfs_cnt = 0)
			THEN
				v_action := '103122'; --Insertion
			ELSIF ( v_new_rfs_cnt = 0 AND v_old_rfs_cnt > 0)
			THEN
				v_action := '4000412'; --Voided
			END IF;
	--
	IF (v_old_prod_material <> p_prodmat OR v_old_prod_family <> p_prodfly OR v_old_prod_class <> p_class) THEN
	    -- to update the UDI parameter values
	    gm_pkg_pd_sav_udi.gm_sav_qa_udi_param (p_partnum, p_userid);
	END IF;	
	--If a part moved from one sales group(FROM) to another sales group(TO), need to update the FROM set updated Date.
	BEGIN
		SELECT t208.c207_set_id INTO v_cur_set_id
		FROM t208_set_details t208, t207_set_master t207
		WHERE t208.c205_part_number_id = p_partnum
		AND t207.c901_set_grp_type = '1600'	 --sales group
		AND t208.c207_set_id = t207.c207_set_id;
	EXCEPTION WHEN OTHERS 
	THEN
		v_cur_set_id :='';
	END ;
		
	IF v_cur_set_id IS NOT NULL AND (v_cur_set_id <>p_salesgroup)
	THEN
		UPDATE t207_set_master
		SET c207_last_updated_by = p_userid
		     , c207_last_updated_date = CURRENT_DATE
		WHERE c207_set_id = v_cur_set_id;
		
		--	Part sync is not checking system publish to PC.
		--  So,Part update no need to check part�s system published to PC.

		/*
		SELECT COUNT(1) into v_cur_sys_pub_pc
           FROM t207_set_master T207, t207c_set_attribute t207c
          WHERE T207.c207_set_id       = t207c.c207_set_id
            AND T207.c207_set_id = v_cur_set_id
            AND T207.c901_set_grp_type = '1600' --System Report Grp
            AND T207.c207_void_fl     IS NULL
            AND t207c.c207c_void_fl   IS NULL
            AND c901_attribute_type    = '103119' --Publish to
            AND c207c_attribute_value  = '103085'; --Product Catalog

	        SELECT COUNT(1) into v_new_sys_pub_pc
	       FROM t207_set_master T207, t207c_set_attribute t207c
	      WHERE T207.c207_set_id       = t207c.c207_set_id
	        AND T207.c207_set_id = p_salesgroup
	        AND T207.c901_set_grp_type = '1600' --System Report Grp
	        AND T207.c207_void_fl     IS NULL
	        AND t207c.c207c_void_fl   IS NULL
	        AND c901_attribute_type    = '103119' --Publish to
	        AND c207c_attribute_value  = '103085'; --Product Catalog
	        */
	        -- If the Curent System is released for PC and the new System is not , then the part should be removed from Product Catl .
	        /*Part sync is not based on System which is published to PC.
	         *When System is unpublished. no need to void their parts.So, both action should be 'updated'.
	         */
	        IF (v_new_rfs_cnt > 0)
	        THEN
	        	v_force_upd := 'Y';
	        	v_action := '103123'; --Updated
	             	
	        END IF;
	END IF;

	UPDATE t208_set_details
	   SET c207_set_id = p_salesgroup
	     , c208_last_updated_by = p_userid
	     , c208_last_updated_date = CURRENT_DATE
	 WHERE c208_set_details_id =
			   (SELECT t208.c208_set_details_id
				  FROM t208_set_details t208, t207_set_master t207
				 WHERE c205_part_number_id = p_partnum
				   AND t207.c901_set_grp_type = '1600'	 -- hardcoding sales group
				   AND t208.c207_set_id = t207.c207_set_id);

	IF (SQL%ROWCOUNT = 0)
	THEN
		INSERT INTO t208_set_details
					(c208_set_details_id, c207_set_id, c205_part_number_id, c208_set_qty, c208_inset_fl
				   , c208_critical_fl
					)
			 VALUES (s208_setmap_id.NEXTVAL, p_salesgroup, p_partnum, 1, NULL
				   , NULL
					);	 -- '300.026' was hardcoded for setid earlier. '300.026' is unassigned part
	END IF;

	--When ever the detail is updated, the set master table has to be updated ,so ETL can Sync it.
	UPDATE t207_set_master
	   SET c207_last_updated_by = p_userid
	     , c207_last_updated_date = CURRENT_DATE
	 WHERE c207_set_id = p_salesgroup;
	 
	IF v_active_fl IS NULL AND p_activefl = 'Y' THEN
	    BEGIN
	         SELECT c4010_publish_fl, c901_priced_type, gm_pkg_pd_group_pricing.get_base_group_price (t4010.c4010_group_id,
	            52060), gm_pkg_pd_group_pricing.get_base_group_price (t4010.c4010_group_id, 52061),
	            gm_pkg_pd_group_pricing.get_base_group_price (t4010.c4010_group_id, 52062),
	            gm_pkg_pd_group_pricing.get_base_group_price (t4010.c4010_group_id, 52063)
	           INTO v_publish_fl, v_priced_type, v_list_price,
	            v_loaner_price, v_consignment_price, v_equity_price
	           FROM t4010_group t4010, t4011_group_detail t4011
	          WHERE t4011.c4010_group_id             = t4010.c4010_group_id
	            AND t4011.c4011_primary_part_lock_fl = 'Y'
	            AND t4010.c4010_void_fl             IS NULL
	            AND t4011.c205_part_number_id        = p_partnum;
	    EXCEPTION
	    WHEN OTHERS THEN
	        v_list_price   := NULL;
	        v_loaner_price := NULL;
	        v_priced_type  := NULL;
	    END;
	    IF v_priced_type = 52110 AND v_list_price <> 0 AND v_loaner_price <> 0 -- group
	        THEN
	       --Below update query needs to remove ,once the fetch related query for price are changed to T2052_PART_PRICE_MAPPING table.
	        UPDATE t205_part_number
	           SET c205_list_price        = v_list_price, 
	               c205_loaner_price      = v_loaner_price, 
	               c205_consignment_price = v_consignment_price, 
	               c205_equity_price      = v_equity_price, 
	               c205_last_updated_date = CURRENT_DATE,
	               c205_last_updated_by   = p_userid
	         WHERE c205_part_number_id    = p_partnum
	            AND c205_active_fl        = 'Y';
	            
	         UPDATE T2052_PART_PRICE_MAPPING
	            SET c2052_list_price   = v_list_price, 
	                c2052_loaner_price = v_loaner_price, 
	                c2052_consignment_price = v_consignment_price, 
	                c2052_equity_price  = v_equity_price, 
	                c2052_updated_date  = CURRENT_DATE,
	                c2052_updated_by    = p_userid,
	                C2052_Last_updated_date = CURRENT_DATE,
	                C2052_Last_updated_by = p_userid
	          WHERE c205_part_number_id = p_partnum
	            AND c1900_company_id       =  v_company_id
	            AND c2052_void_fl is NULL;
	    END IF;
	END IF;
	
	/* To save group description and detailed description values
	* 103090: Part Number
	*/
	gm_pkg_pd_language.gm_sav_lang_details(p_partnum, '103090', '', p_partdesc, p_detail_desc, p_userid);
	 
	-- When a Part is created, we need to load the Part in T2023 table,so this newly created part will be visible for the created company.
	IF v_new_part = 'Y'
	THEN
		-- We do not need to pass the Country ID .70171 -- RFS Flag : YES.
		gm_pkg_qa_rfs.gm_sav_part_company_map(p_partnum,v_company_id,'','105360','70171',p_userid);
		
		--By using the below procedure we are creating  the record for part price in T2052_PART_PRICE_MAPPING.
		gm_pkg_pd_part_pricing.gm_sav_part_pricing(p_partnum ,v_equity_price , v_loaner_price, v_consignment_price, v_list_price, v_company_id, p_userid);
		
	END IF;

	IF (v_action IS NOT NULL)
	THEN
	
		--check part's project is in exclude project list
        SELECT count(1) INTO v_cmp_cnt
		  FROM t2023_part_company_mapping 
		 WHERE c205_part_number_id = p_partnum
		   AND c1900_company_id = 1000
		   AND c2023_void_fl IS NULL;			
			
		IF v_cmp_cnt > 0  THEN
	
		 -- 4000411  Part Number
		 -- 103097   English
		 -- 103100   Product Catalog
		gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd(p_partnum
								     , '4000411', v_action 
								     ,'103097','103100'
								     ,p_userid
								     ,p_salesgroup
								     ,v_force_upd
								     );
		END IF;
	END IF;
		--PMT-32340 - Work Order Revision Update Dashboard 
		gm_pkg_wo_txn.gm_void_wo_revision(p_partnum,p_userid);
END gm_pd_sav_partnumber;
/
