/* Formatted on 2007/05/18 11:50 (Formatter Plus v4.8.0) */


CREATE OR REPLACE PROCEDURE gm_pd_save_growth (
	p_reftype	 IN 	  t4001_growth.c901_ref_type%TYPE
  , p_refclass	 IN 	  t4001_growth.c901_ref_class%TYPE
  , p_refid 	 IN 	  t4001_growth.c4001_ref_id%TYPE
  , p_year		 IN 	  NUMBER
  , p_inputstr	 IN 	  VARCHAR2
  , p_userid	 IN 	  t4001_growth.c4001_created_by%TYPE
  , p_returnid	 OUT	  VARCHAR2
)
AS
	v_growthvalue	VARCHAR2 (20);
	v_month 		VARCHAR2 (20);
	v_strlen		NUMBER			 := NVL (LENGTH (p_inputstr), 0);
	v_string		VARCHAR2 (30000) := p_inputstr;
	v_substring 	VARCHAR2 (1000);
	v_start_date	VARCHAR2 (20);
	v_end_date		DATE;
	v_returnid		VARCHAR2 (20);
	v_cnt			NUMBER;
	v_cnt_1 		NUMBER;
BEGIN
--
	IF p_reftype = 20295
	THEN
		--
		SELECT COUNT (*)
		  INTO v_cnt
		  FROM t205_part_number
		 WHERE c205_part_number_id = p_refid;
	--
	END IF;

	IF v_cnt = 0
	THEN
		--
		raise_application_error (-20029, '');
	--
	END IF;

	WHILE INSTR (v_string, '|') <> 0
	LOOP
		v_substring 			   := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
		v_string				   := SUBSTR (v_string, INSTR (v_string, '|') + 1);
		--
		v_month 				   := NULL;
		v_growthvalue			   := NULL;
		v_month 				   := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
		v_substring 			   := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
		v_growthvalue			   := v_substring;
		--
		v_start_date			   := v_month || '/01/' || p_year;

		SELECT LAST_DAY (TO_DATE (v_start_date, 'MM/DD/YYYY'))
		  INTO v_end_date
		  FROM DUAL;

		--
		UPDATE t4001_growth
		   SET c4001_ref_id = p_refid
			 , c4001_value = TO_NUMBER (v_growthvalue)
			 , c901_ref_type = p_reftype
			 , c901_ref_class = p_refclass
			 , c4001_start_date = TO_DATE (v_start_date, 'mm/dd/yyyy')
			 , c4001_end_date = v_end_date
			 , c4001_last_updated_by = p_userid
			 , c4001_last_updated_date = SYSDATE
		 WHERE c4001_start_date = TO_DATE (v_start_date, 'mm/dd/yyyy') AND c4001_ref_id = p_refid;

		--
		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT COUNT (*)
			  INTO v_cnt_1
			  FROM t4001_growth
			 WHERE c4001_ref_id = p_refid
			   AND c901_ref_type = p_reftype
			   AND c901_ref_class = p_refclass
			   AND TO_CHAR (c4001_start_date, 'YYYY') = p_year;

			IF v_cnt_1 > 0
			THEN
				--
				raise_application_error (-20030, '');
			--
			END IF;

			--
			INSERT INTO t4001_growth
						(c4001_growth_id, c4001_value, c4001_ref_id, c901_ref_type, c901_ref_class
					   , c4001_start_date, c4001_end_date, c4001_created_by, c4001_created_date
						)
				 VALUES (s4001_growth.NEXTVAL, TO_NUMBER (v_growthvalue), p_refid, p_reftype, p_refclass
					   , TO_DATE (v_start_date, 'mm/dd/yyyy'), v_end_date, p_userid, SYSDATE
						);
		--
		END IF;
	--
	END LOOP;

	SELECT TO_CHAR (c4001_start_date, 'YYYY') || '-' || c4001_ref_id
	  INTO v_returnid
	  FROM t4001_growth
	 WHERE TO_CHAR (c4001_start_date, 'YYYY') = p_year AND c4001_ref_id = p_refid AND ROWNUM = 1;

	p_returnid				   := v_returnid;
--

--
END gm_pd_save_growth;
/
