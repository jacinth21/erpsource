CREATE OR REPLACE PROCEDURE GM_SAVE_EDIT_PARTNUMBER_TEMP
(
 		  p_str 	  IN	VARCHAR2,
	   	  p_UserId	  IN 	T206_SUB_ASSEMBLY.C206_LAST_UPDATED_BY%TYPE,
		  p_partNum	  IN	T206_SUB_ASSEMBLY.C205_PART_NUMBER_ID%TYPE,
		  p_Control	  IN	T205_PART_NUMBER.C205_SUB_COMPONENT_FL%TYPE,
		  p_errmsg    OUT	VARCHAR2
)
AS
v_strlen     NUMBER          := NVL(LENGTH(p_str),0);
v_string     VARCHAR2(30000) := p_str;
v_PNum	 	 VARCHAR2(20);
v_PDesc		 VARCHAR2(255);
v_MatSp	 	 VARCHAR2(20);
v_Draw		 VARCHAR2(20);
v_Rev		 VARCHAR2(20);
v_Fl		 CHAR(1);
v_last_id	 VARCHAR2(20);
v_substring  VARCHAR2(1000);
v_msg	     VARCHAR2(1000);
e_others	 EXCEPTION;
v_cnt		 NUMBER;
v_Id 		 NUMBER;
BEGIN

 	IF v_strlen > 0
    THEN
   		WHILE INSTR(v_string,'|') <> 0 LOOP
    		  v_substring := SUBSTR(v_string,1,INSTR(v_string,'|')-1);
    		  v_string    := SUBSTR(v_string,INSTR(v_string,'|')+1);
			  v_PNum	  := NULL;
  			  v_PDesc	  := NULL;
			  v_MatSp  	  := NULL;
			  v_Draw	  := NULL;
			  v_Rev		  := NULL;
			  v_Fl	  	  := NULL;
			  v_last_id	  := NULL;
			  v_PNum 	  := REPLACE(SUBSTR(v_substring,1,INSTR(v_substring,'^')-1),'`','');
			  v_substring := SUBSTR(v_substring,INSTR(v_substring,'^')+1);
			  v_PDesc 	  := REPLACE(SUBSTR(v_substring,1,INSTR(v_substring,'^')-1),'`','');
			  v_substring := SUBSTR(v_substring,INSTR(v_substring,'^')+1);
			  v_MatSp 	  := REPLACE(SUBSTR(v_substring,1,INSTR(v_substring,'^')-1),'`','');
			  v_substring := SUBSTR(v_substring,INSTR(v_substring,'^')+1);
			  v_Draw 	  := REPLACE(SUBSTR(v_substring,1,INSTR(v_substring,'^')-1),'`','');
			  v_substring := SUBSTR(v_substring,INSTR(v_substring,'^')+1);
			  v_Rev 	  := REPLACE(SUBSTR(v_substring,1,INSTR(v_substring,'^')-1),'`','');
			  v_substring := SUBSTR(v_substring,INSTR(v_substring,'^')+1);
			  v_Fl		  := REPLACE(SUBSTR(v_substring,1,INSTR(v_substring,'^')-1),'`','');
			  v_substring := SUBSTR(v_substring,INSTR(v_substring,'^')+1);
			  v_last_id	  := TO_NUMBER(v_substring);

			  IF v_Fl = 'I' THEN
			  SELECT S206_SUB_ASMBLY_ID.nextval INTO v_Id FROM dual;
				 INSERT INTO T206_SUB_ASSEMBLY (C206_SUB_ASMBLY_SEQ_ID, C206_SUB_ASMBLY_ID,C206_SUB_ASMBLY_DESC,C206_MATERIAL_SPEC,
				 		C205_PART_NUMBER_ID,C206_SUB_ASMBLY_DRAWING,C206_SUB_ASMBLY_REV_NUM,C206_CREATED_DATE,C206_CREATED_BY)
					 VALUES
					 	   (v_Id, v_PNum,v_PDesc,v_MatSp,
						   p_partNum,v_Draw,v_Rev,SYSDATE,p_UserId);
			  ELSIF v_Fl = 'U' THEN
				 UPDATE T206_SUB_ASSEMBLY
				 		SET
						    C206_SUB_ASMBLY_ID = v_PNum,
							C206_SUB_ASMBLY_DESC  = v_PDesc,
							C206_MATERIAL_SPEC = v_MatSp,
							C206_SUB_ASMBLY_DRAWING = v_Draw,
							C206_SUB_ASMBLY_REV_NUM = v_Rev,
							C206_LAST_UPDATED_DATE = SYSDATE,
							C206_LAST_UPDATED_BY = p_UserId
						WHERE
							  C206_SUB_ASMBLY_SEQ_ID = v_last_id;
			  ELSIF v_Fl = 'D' THEN
			  		DELETE T206_SUB_ASSEMBLY WHERE C206_SUB_ASMBLY_SEQ_ID = v_last_id;
			  END IF;

			  SELECT count(*) into v_cnt FROM T206_SUB_ASSEMBLY WHERE C205_PART_NUMBER_ID = p_partNum;
			  IF v_cnt > 0 THEN
			   	 UPDATE T205_PART_NUMBER SET C205_SUB_ASMBLY_FL = 1, C205_SUB_COMPONENT_FL = p_Control,
			   	 C205_LAST_UPDATED_BY = p_UserId, C205_LAST_UPDATED_DATE = SYSDATE
				 WHERE C205_PART_NUMBER_ID = p_partNum;
			  ELSE
			  	  UPDATE T205_PART_NUMBER SET C205_SUB_ASMBLY_FL = '' , C205_SUB_COMPONENT_FL = '',
			  	  C205_LAST_UPDATED_BY = p_UserId, C205_LAST_UPDATED_DATE = SYSDATE
				  WHERE C205_PART_NUMBER_ID = p_partNum;
			  END IF;

		END LOOP;
	ELSE
	    v_msg := 'p_str is empty';
		RAISE e_others;
	END IF;
	v_msg := 'Success';
	COMMIT WORK;
    p_errmsg  := v_msg;
EXCEPTION
	  WHEN e_others THEN
	  ROLLBACK WORK;
	  p_errmsg  := v_msg;
	  WHEN OTHERS THEN
	  ROLLBACK WORK;
	  p_errmsg  := SQLERRM;
NULL;
END GM_SAVE_EDIT_PARTNUMBER_TEMP;
/

