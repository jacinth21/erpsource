/* Formatted on 2009/10/05 16:35 (Formatter Plus v4.8.0) */
CREATE OR REPLACE PROCEDURE gm_save_project (
	p_projnm		IN		 t202_project.c202_project_nm%TYPE
  , p_projdesc		IN		 t202_project.c202_project_desc%TYPE
  , p_projid		IN		 t202_project.c202_project_id%TYPE
  , p_devclass		IN		 t202_project.c202_device_class%TYPE
  , p_status		IN		 t202_project.c901_status_id%TYPE
  , p_group 		IN		 t202_project.c901_group_id%TYPE
  , p_segment		IN		 t202_project.c901_segment_id%TYPE
  , p_userid		IN		 t202_project.c202_created_by%TYPE
  , p_action		IN		 VARCHAR2
  , p_projecttype	IN		 t202_project.c901_project_type%TYPE
  , p_message		OUT 	 VARCHAR2
  , p_projectid 	OUT 	 t202_project.c202_project_id%TYPE
)
AS
	v_proj_id	   NUMBER;
	v_string	   VARCHAR2 (10);
	v_id_string    VARCHAR2 (10);
BEGIN
	IF p_action = 'Add'
	THEN
		SELECT s202_project.NEXTVAL
		  INTO v_proj_id
		  FROM DUAL;

		IF LENGTH (v_proj_id) = 1
		THEN
			v_id_string := '0' || v_proj_id;
		ELSE
			v_id_string := v_proj_id;
		END IF;

		SELECT 'GM' || TO_CHAR (SYSDATE, 'YY') || '-' || v_id_string
		  INTO v_string
		  FROM DUAL;

		p_projectid := v_string;

		--SELECT CONCAT(v_proj_id,v_string) INTO v_string FROM DUAL;
		INSERT INTO t202_project
					(c202_project_id, c202_project_nm, c202_project_desc, c202_device_class, c901_status_id
				   , c901_group_id, c901_segment_id, c202_created_by, c202_created_date, c901_project_type
					)
			 VALUES (v_string, p_projnm, p_projdesc, p_devclass, p_status
				   , p_group, p_segment, p_userid, SYSDATE, p_projecttype
					);
	ELSE
		UPDATE t202_project
		   SET c202_project_nm = p_projnm
			 , c202_project_desc = p_projdesc
			 , c202_device_class = p_devclass
			 , c901_status_id = p_status
			 , c901_group_id = p_group
			 , c901_segment_id = p_segment
			 , c202_last_updated_by = p_userid
			 , c202_last_updated_date = SYSDATE
			 , c901_project_type = DECODE (p_projid, 'GM00-01', 1501, 1500)
		 WHERE c202_project_id = p_projid;
	END IF;
END gm_save_project;
/
