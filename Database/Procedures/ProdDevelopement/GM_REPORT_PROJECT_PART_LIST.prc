CREATE OR REPLACE PROCEDURE gm_report_project_part_list (
	p_projid	  IN	   t205_part_number.c202_project_id%TYPE
  , p_recordset   OUT	   TYPES.cursor_type
)
AS
/****************************************************************************************
 * Description			 :This procedure is called to get PART NUMBER ALLOCATION DETAILS
 * Parameters			 :p_column
 *						 :p_recordset
 ****************************************************************************************/
--
	v_projid	   VARCHAR2 (20);
--
BEGIN
	IF p_projid = '0'
	THEN
		v_projid	:= NULL;
	ELSE
		v_projid	:= p_projid;
	END IF;

--
	OPEN p_recordset
	 FOR
		 SELECT   t205.c205_part_number_id pnum, t205.c205_part_num_desc pdesc
				, get_mfg_partnum_inv_qty(t205.c205_part_number_id) rmqty
				, (get_partnumber_qty(t205.c205_part_number_id,90813) - nvl ((quar_alloc_to_scrap + quar_alloc_to_inv), 0)) inquaran
				, get_partnumber_qty(t205.c205_part_number_id,90814) - (NVL (in_wip_set, 0) + NVL (bulk_alloc_to_inv, 0)) inbulk
				, (in_wip_set-tbe_alloc) in_wip_set ,tbe_alloc
				, get_partnumber_qty(t205.c205_part_number_id,90812) inreturns, c205_rev_num drawrev
				, get_partnumber_po_pend_qty (t205.c205_part_number_id) popend
				, get_partnumber_po_wip_qty (t205.c205_part_number_id) powip, sales_allocated, cons_alloc
				, quar_alloc_self_quar, quar_alloc_to_scrap, quar_alloc_to_inv, bulk_alloc_to_set, bulk_alloc_to_inv
				, inv_alloc_to_pack
				,	get_partnumber_qty(t205.c205_part_number_id,90800)
				  - (  NVL (inv_alloc_to_pack, 0)
					 + NVL (sales_allocated, 0)
					 + NVL (cons_alloc, 0)
					 + NVL (quar_alloc_self_quar, 0)
					 + NVL (bulk_alloc_to_set, 0)
					 + NVL (get_partnumber_qty(t205.c205_part_number_id,90812), 0)
					 + NVL (get_partnumber_qty(t205.c205_part_number_id,90814), 0)
					 + NVL (loan_alloc, 0)
					) inshelf
				, (  NVL (sales_allocated, 0)
				   + NVL (cons_alloc, 0)
				   + NVL (quar_alloc_self_quar, 0)
				   + NVL (bulk_alloc_to_set, 0)
				   + NVL (loan_alloc, 0)
				  ) shelfalloc
				, (quar_alloc_to_scrap + quar_alloc_to_inv) quaralloc, bulk_alloc_to_inv bulkalloc
			 FROM t205_part_number t205
				, (SELECT	b.c205_part_number_id, SUM (b.c502_item_qty) sales_allocated
					   FROM t501_order a, t502_item_order b
					  WHERE a.c501_order_id = b.c501_order_id
						AND a.c501_status_fl < 3
						AND a.c501_status_fl > 0
						AND a.c501_void_fl IS NULL
						AND (a.c901_ext_country_id IS NULL
						OR a.c901_ext_country_id  in (select country_id from v901_country_codes))
						AND NVL (a.c901_order_type, -9999) <> 2524
						AND NVL (c901_order_type, -9999) NOT IN (
			                SELECT t906.c906_rule_value
			                  FROM t906_rules t906
			                 WHERE t906.c906_rule_grp_id = 'ORDTYPE'
			                   AND c906_rule_id = 'EXCLUDE')
						AND a.c501_shipping_date IS NULL
						AND b.c901_type = 50300
				   GROUP BY b.c205_part_number_id) sls
				, (SELECT	b.c205_part_number_id
						  , SUM (DECODE (SIGN (4113 - a.c504_type), 1, b.c505_item_qty, 0)) cons_alloc
						  , SUM (DECODE (a.c504_type, 4113, b.c505_item_qty, 0)) quar_alloc_self_quar
						  , SUM (DECODE (a.c504_type, 4114, b.c505_item_qty, 0)) inv_alloc_to_pack
						  , SUM (DECODE (a.c504_type, 4115, b.c505_item_qty, 0)) quar_alloc_to_scrap
						  , SUM (DECODE (a.c504_type, 4116, b.c505_item_qty, 0)) quar_alloc_to_inv
					   FROM t504_consignment a, t505_item_consignment b
					  WHERE a.c504_consignment_id = b.c504_consignment_id
						AND a.c504_status_fl < 4
						AND a.c207_set_id IS NULL
						AND a.c504_type IN (4110, 4111, 4112, 4113, 4114, 4115, 4116)
						AND a.c504_void_fl IS NULL
						AND a.c504_ship_date IS NULL
				   GROUP BY b.c205_part_number_id) cons
				, (SELECT	b.c205_part_number_id, SUM (DECODE (a.c412_type, 4117, c413_item_qty, 0)) bulk_alloc_to_set
						  , SUM (DECODE (a.c412_type, 4118, c413_item_qty, 0)) bulk_alloc_to_inv
						  , SUM (DECODE (a.c412_type, 50150, c413_item_qty, 50155, c413_item_qty, 0)) loan_alloc
					   FROM t412_inhouse_transactions a, t413_inhouse_trans_items b
					  WHERE a.c412_inhouse_trans_id = b.c412_inhouse_trans_id
						AND a.c412_verified_date IS NULL
						AND a.c412_void_fl IS NULL
						AND a.c412_type IN (4117, 4118, 50150, 50155)
				   GROUP BY b.c205_part_number_id) blktxn
				, (SELECT	a.c205_part_number_id, SUM (NVL (a.c505_item_qty, 0)) in_wip_set
							,SUM (DECODE (a.C505_CONTROL_NUMBER, 'TBE', a.c505_item_qty, 0)) TBE_ALLOC
					   FROM t505_item_consignment a, t504_consignment b
					  WHERE a.c504_consignment_id = b.c504_consignment_id
						AND TRIM (a.c505_control_number) IS NOT NULL
						AND b.c504_status_fl < 3
						AND b.c207_set_id IS NOT NULL
						AND b.c504_verify_fl = '0'
						AND b.c504_void_fl IS NULL
				   GROUP BY a.c205_part_number_id) wip
			WHERE t205.c202_project_id = NVL (v_projid, c202_project_id)
			  AND t205.c205_part_number_id = sls.c205_part_number_id(+)
			  AND t205.c205_part_number_id = cons.c205_part_number_id(+)
			  AND t205.c205_part_number_id = blktxn.c205_part_number_id(+)
			  AND t205.c205_part_number_id = wip.c205_part_number_id(+)
			  AND INSTR (t205.c205_part_number_id, '.', 1, 2) = 0
		 ORDER BY t205.c205_part_number_id;
--
END gm_report_project_part_list;
/
