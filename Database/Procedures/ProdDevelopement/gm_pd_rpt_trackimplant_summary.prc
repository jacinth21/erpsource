CREATE OR REPLACE PROCEDURE gm_pd_rpt_trackimplant_summary (
	p_setid 			 IN 	  t207_set_master.c207_set_id%TYPE
  , p_trackimplant_cur	 OUT	  TYPES.cursor_type
)
AS
/***************************************************************************************************
		 * This Procedure is to fetch the summary of implants that are to be tracked for the given set id
		 * Author : vprasath
		 * Date  : 03/09/07
***************************************************************************************************/
--
BEGIN
	OPEN p_trackimplant_cur
	 FOR
		 SELECT   t205.c205_part_number_id partnum, t205.c205_part_num_desc partdesc
			 FROM t207_set_master t207, t208_set_details t208, t205_part_number t205
			WHERE t207.c207_set_id = p_setid
			  AND t207.c901_set_grp_type = 1600
			  AND t207.c207_set_id = t208.c207_set_id
			  AND t208.c205_part_number_id = t205.c205_part_number_id
			  AND t205.c205_tracking_implant_fl = 'Y'
			  AND t208.c208_void_fl IS NULL
		 ORDER BY t205.c205_part_number_id;
END gm_pd_rpt_trackimplant_summary;
