/* Formatted on 2009/06/01 18:43 (Formatter Plus v4.8.0) */
-- @ "C:\Database\Procedures\ProdDevelopement\gm_save_setmaster.prc"

create or replace
PROCEDURE              GM_SAVE_SETMASTER (
	p_projid	  IN   t207_set_master.c202_project_id%TYPE
  , p_setid 	  IN   t207_set_master.c207_set_id%TYPE
  , p_setnm 	  IN   t207_set_master.c207_set_nm%TYPE
  , p_setdesc	  IN   t207_set_master.c207_set_desc%TYPE
  , p_setcat	  IN   t207_set_master.c207_category%TYPE
  , p_settype	  IN   t207_set_master.c207_type%TYPE
  , p_type		  IN   t207_set_master.c901_set_grp_type%TYPE
  , p_userid	  IN   t207_set_master.c207_created_by%TYPE
  , p_action	  IN   VARCHAR2
  , p_status	  IN   t207_set_master.c901_status_id%TYPE
  , p_revlvl	  IN   t207_set_master.c207_rev_lvl%TYPE
  , p_coresetfl   IN   t207_set_master.c207_core_set_fl%TYPE
  , p_det_desc    IN   t207_set_master.c207_set_dtl_desc%TYPE
  , p_division_id IN   t207_set_master.c1910_division_id%TYPE
  ,p_lottrackfl   IN   t207_set_master.c207_lot_track_fl%TYPE
)
AS
	v_tmp_setid    t207_set_master.c207_set_id%TYPE;
	v_old_status   t207_set_master.c901_status_id%TYPE;
	v_system_id    t207_set_master.c207_set_sales_system_id%TYPE;
	v_action	   VARCHAR2 (20) ;
	v_portal_comp_id t207_set_master.c1900_company_id%TYPE;
	v_cmp_cnt 	   NUMBER;
BEGIN
	

	IF p_action <> 'Add' THEN
	    BEGIN
	         SELECT c901_status_id, c207_set_sales_system_id
	           INTO v_old_status, v_system_id
	           FROM t207_set_master
	          WHERE c207_set_id   = p_setid
	            AND c207_void_fl IS NULL;
	    EXCEPTION
	    WHEN NO_DATA_FOUND THEN
	        v_old_status := NULL;
	        v_system_id  := NULL;
	    END;
	END IF; 
	
	IF p_action = 'Add'
	THEN
		BEGIN
			SELECT c207_set_id
			  INTO v_tmp_setid
			  FROM t207_set_master
			 WHERE c207_set_id = p_setid;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_tmp_setid := '';
		END;

		IF v_tmp_setid = p_setid
		THEN
			raise_application_error (-20032, '');
		ELSE
			INSERT INTO t207_set_master
						(c202_project_id, c207_set_id, c207_set_nm, c207_set_desc, c207_category, c207_type
					   , c901_set_grp_type, c207_created_by, c207_created_date, c901_status_id, c207_rev_lvl
					   , c207_core_set_fl , c207_set_dtl_desc ,c1900_company_id ,c1910_division_id,c207_lot_track_fl
						)
				 VALUES (p_projid, p_setid, p_setnm, p_setdesc, p_setcat, p_settype
					   , p_type, p_userid, CURRENT_DATE, 20365, p_revlvl
					   , p_coresetfl , p_det_desc -- For saving Detaied Description
					    ,v_portal_comp_id,p_division_id,p_lottrackfl
						);
		END IF;
	ELSE
		UPDATE t207_set_master
		   SET c202_project_id = p_projid 
		   	 , c207_set_nm = p_setnm
			 , c207_set_desc = p_setdesc
			 , c207_category = p_setcat
			 , c207_type = p_settype
			 , c207_last_updated_by = p_userid
			 , c207_last_updated_date = CURRENT_DATE
			 , c901_status_id = p_status
			 , c207_rev_lvl = p_revlvl
			 , c207_core_set_fl = p_coresetfl
			 , c207_set_dtl_desc = p_det_desc -- Updating Detailed Description
			 , c1900_company_id = v_portal_comp_id
			 , c1910_division_id = p_division_id
			 ,c207_lot_track_fl = p_lottrackfl
		 WHERE c207_set_id = p_setid;
	END IF;

	UPDATE t205_part_number t205
	   SET c205_core_part_fl = p_coresetfl
	     , c205_last_updated_by = p_userid
		 , c205_last_updated_date = CURRENT_DATE
	 WHERE t205.c205_part_number_id IN (SELECT c205_part_number_id
										  FROM t208_set_details
										 WHERE c207_set_id = p_setid AND c208_void_fl IS NULL AND c208_set_qty > 0);
	-- Caliing this procedure is for saving the Language Details for the Set ID.
	gm_pkg_pd_language.gm_sav_lang_details(p_setid,'103091',p_setnm,p_setdesc,p_det_desc,p_userid,'103097'); --103097[English] , 103091[Set]
    
	IF (p_type = '1601') THEN  --1601 [Set Report Group Type]
		IF (v_old_status = 20367 AND p_status <> 20367) THEN	  ---20367 [Approved status]
			v_action := '4000412';	                       ---[Voided]
		ELSIF (v_old_status <> 20367 AND p_status = 20367) THEN
			v_action := '103123';                          ---[Updated]
		END IF;
		
         	    SELECT COUNT(1) INTO v_cmp_cnt
				  FROM T207_SET_MASTER T207,T2080_SET_COMPANY_MAPPING T2080 
				 WHERE T207.C207_SET_ID=T2080.C207_SET_ID
				   AND T207.C207_SET_ID = p_setid
				   AND T207.C901_SET_GRP_TYPE = '1601' -- Set
                   AND T2080.C2080_VOID_FL IS NULL
                   AND T2080.C1900_COMPANY_ID IN ('1000')
                   AND T207.C207_EXCLUDE_IPAD_FL IS NULL
				   AND T207.C207_VOID_FL IS NULL;
				   

	    --Calling this prc to update the entry in t2000 master update table.
	    IF v_action IS NOT NULL AND v_cmp_cnt > 0 THEN 
			gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (p_setid, '103111', v_action, '103097',     --103111[Set Device Sync Type], 103100 [Product Catalog Appln] 
														  '103100', p_userid, v_system_id, NULL) ;
	    END IF;
	END IF;
 				
END gm_save_setmaster;

/
