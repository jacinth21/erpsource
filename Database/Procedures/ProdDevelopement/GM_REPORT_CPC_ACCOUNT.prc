/* @"C:\Database\Procedures\ProdDevelopement\GM_REPORT_CPC_ACCOUNT.prc";*/
CREATE OR REPLACE PROCEDURE gm_report_cpc_account (	
   p_recordset   OUT	TYPES.cursor_type
)
AS
/*
	  Description			:This procedure is used to fetch ContractProcessingClient values
	  Parameters			:p_recordset
*/
	v_company_id     NUMBER;
	v_plant_id       NUMBER;
BEGIN
	/*SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
      INTO v_company_id, v_plant_id 
      FROM DUAL;
	  */
	  
	OPEN p_recordset
	 FOR
		 SELECT MIN(c704b_account_cpc_map_id) ID, c704b_cpc_name NAME				
		  FROM t704b_account_cpc_mapping
			WHERE c704b_void_fl IS NULL 
			  AND c704b_active_fl = 'Y' 
			  --AND c1900_company_id = v_company_id 
			  --AND c5040_plant_id = v_plant_id 
	    GROUP BY c704b_cpc_name 		  
		ORDER BY c704b_cpc_name;
END gm_report_cpc_account;
/

