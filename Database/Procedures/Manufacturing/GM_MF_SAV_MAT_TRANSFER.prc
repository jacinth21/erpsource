CREATE OR REPLACE PROCEDURE GM_MF_SAV_MAT_TRANSFER
(
	p_Ids		IN	VARCHAR2
	,p_UserId	IN	t205b_part_receive.c205b_last_updated_by%TYPE
)
AS
--
v_string	VARCHAR2(30000) := p_Ids||'|';
v_strlen	NUMBER := NVL(LENGTH(p_Ids),0);
v_substring	VARCHAR2(1000);
v_partnum	VARCHAR2(20);
v_qty		NUMBER;
--
/********************************************************************************************
 * Description  :This procedure is used for savinf Pending Transfer requests
		 between Main Inventory and Supply Inventory.
 * Change Details:
 *		 James	Sep12,2006	Created	
 *********************************************************************************************/
--
BEGIN
--
	UPDATE t205b_part_receive SET 
		c205b_last_updated_by		= p_UserId
		,c205b_last_updated_date	= SYSDATE
		,c205b_received_dt		= SYSDATE
		,c205b_received_by		= p_UserId
	WHERE 
		c205b_part_receive_id	IN (p_Ids);
--
	IF v_strlen > 0 THEN
	--
		WHILE INSTR(v_string,'|') <> 0 LOOP
		--
		v_substring := SUBSTR(v_string,1,INSTR(v_string,'|')-1);
		v_string    := SUBSTR(v_string,INSTR(v_string,'|')+1);
		--
		SELECT 
			t205a.c205_to_part_number_id
			,t205b.c205b_received_qty
		INTO	
			v_partnum
			,v_qty
		FROM 
			t205b_part_receive t205b, t205a_part_mapping t205a
		WHERE 
			t205a.c205a_part_mapping_id = v_substring
		AND 
			t205b.c205a_part_mapping_id = t205a.c205a_part_mapping_id
	    AND t205a.c205a_void_fl is null;    ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
		-- Updating Qty in Stock for the MFG Part #
		gm_cm_sav_partqty (v_partnum,
                               v_qty,
                               v_substring,
                               p_UserId,
                               90800, -- FG Qty
                               4301,	-- Plus
                               4314	-- Purchase
                               );
		--
		
		END LOOP;
	--
	END IF;
--
END GM_MF_SAV_MAT_TRANSFER;
/