/* Formatted on 2011/02/03 13:06 (Formatter Plus v4.8.0) */
  --@"C:\DATABASE\PROCEDURES\manufacturing\gm_mf_fch_bomdetails_woini.prc";

CREATE OR REPLACE PROCEDURE gm_mf_fch_bomdetails_woini (
	p_bomid 	   IN		t207_set_master.c207_set_id%TYPE
  , p_bomdetails   OUT		TYPES.cursor_type
)
AS
/***************************************************************************************
 * Description			 :This procedure is called to get the Manufacturing BOM Details
 *
 ****************************************************************************************/
--

--
BEGIN
	--
	OPEN p_bomdetails
	 FOR
		 SELECT   '' forcebo, c205_part_number_id pnum, get_partnum_desc (c205_part_number_id) pdesc
				, c208_set_qty setqty
				, DECODE (SIGN (get_mfg_partnum_inv_qty (c205_part_number_id))
						, -1, 0
						, get_mfg_partnum_inv_qty (c205_part_number_id)
						 ) invqty
				, get_code_name (c901_unit_type) unitnm, c901_unit_type unittype, c208_critical_tag tag, '' reqqty
				, get_rule_value (c205_part_number_id, 'BOM-QTY-EDIT') qtyedit
			 FROM t208_set_details
			WHERE c207_set_id = p_bomid AND c208_set_qty > 0 AND c208_void_fl IS NULL AND c208_inset_fl = 'Y'
		 ORDER BY tag ASC, invqty DESC;
--
END gm_mf_fch_bomdetails_woini;
/
