CREATE OR REPLACE PROCEDURE GM_MF_FCH_BOMPARTS
(
	p_mfgwodhrid	IN	T408_DHR.C408_DHR_ID%TYPE,
	p_bomdetails	OUT	Types.cursor_type
)
AS
/***************************************************************************************
 * Description           :This procedure is called to get the Manufacturing BOM part list
 *		Progbuild verification		
 ****************************************************************************************/
--

--
BEGIN
	--
	OPEN p_bomdetails FOR
	--
	SELECT c205_part_number_id PNUM
	FROM t208_set_details t208, t207_set_master t207
	WHERE t207.c207_set_id = t208.c207_set_id 
	AND   t207.c207_set_desc = 
		(SELECT t408.c205_part_number_id  
		FROM t303_material_request t303, t408_dhr t408
	WHERE	t408.c408_dhr_id = p_mfgwodhrid
	AND	t408.c408_dhr_id = t303.c408_dhr_id
	AND ROWNUM <= 1);
	--
END GM_MF_FCH_BOMPARTS;
/

