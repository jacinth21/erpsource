
--@"C:\DATABASE\PROCEDURES\manufacturing\GM_INITIATE_MFG_WO.prc";

CREATE OR REPLACE PROCEDURE GM_INITIATE_MFG_WO
(
	p_qtytomfg		IN	t408_dhr.c408_qty_received%TYPE
	,p_createdby	IN	t408_dhr.c408_created_by%TYPE
	,p_statusflag	IN	t408_dhr.c408_status_fl%TYPE
	,p_woid			IN	t402_work_order.c402_work_order_id%TYPE
	,p_partnum		IN	t205_part_number.c205_part_number_id%TYPE
	,p_vendorid		IN	t301_vendor.c301_vendor_id%TYPE
	,p_bomid		IN	t207_set_master.c207_set_id%TYPE
	,p_expiry_date	IN	DATE 
	,p_bo_inputstr  IN  VARCHAR2
	,p_inputstr     IN  VARCHAR2
	,p_donor_num	IN	t2540_donor_master.c2540_donor_number%TYPE
	,p_rwflag		IN	t408_dhr.c408_rw_fl%TYPE
	,p_lotcode		OUT	VARCHAR2
	,p_mfgdhrwoid	OUT	VARCHAR2		
)
AS
/***************************************************************************************
 * Description           :This procedure is used to Initiate Manufacturing Work Order
 *				
 ****************************************************************************************/
v_msg		VARCHAR2(250);
v_mfwo_seq_no	t408_dhr.c408_dhr_id%TYPE;
v_control_no	t408_dhr.c408_control_number%TYPE;
v_recv_date	t408_dhr.c408_received_date%TYPE;
v_matreq_seq_no	t303_material_request.c303_material_request_id%TYPE;
v_status_fl	t905_status_details.c905_status_fl%TYPE;
v_manf_date	VARCHAR2(20);
v_year		VARCHAR2(4);
v_lot_code	t301_vendor.c301_lot_code%TYPE;
v_year_code	t901_code_lookup.c902_code_nm_alt%TYPE;
v_rev_lvl	t402_work_order.c402_rev_num%TYPE;
v_manf_run	NUMBER;
v_lot_no	t901_code_lookup.c902_code_nm_alt%TYPE;
v_po_type	NUMBER;
v_zero_cnt	NUMBER;
v_cnt		NUMBER := 0;
v_zero_str	VARCHAR2(10);
v_partid    t208_set_details.c205_part_number_id%TYPE;
v_req_Qty	NUMBER;
v_Inv_Qty	NUMBER;
v_tag		t208_set_details.c208_critical_tag%TYPE;
v_tag_fl 	VARCHAR2(100) :='N';
v_first_time_flag  BOOLEAN := FALSE;
v_bo_inputstr	   VARCHAR2 (4000) := p_bo_inputstr;
v_backorderqty  t208_set_details.c208_set_qty%TYPE;
v_out_matreq_id t303_material_request.c303_material_request_id%TYPE;
v_status	t303_material_request.c303_status_fl%TYPE;
v_inputstr	VARCHAR2 (4000) := p_inputstr; 
v_count		NUMBER;
v_julian_rule   NUMBER;
v_new_manf_run	NUMBER;
v_UDI_Num	VARCHAR2 (4000);
v_part_udi  VARCHAR2 (200);
v_di_prefix VARCHAR2(10);
v_cn_prefix VARCHAR2(10);
v_ed_prefix VARCHAR2(10);
v_exp_date  VARCHAR2(10);
v_donor_id	t2540_donor_master.c2540_donor_id%TYPE;
v_company_id  t1900_company.c1900_company_id%TYPE;
 v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
v_po_id t401_purchase_order.c401_purchase_ord_id%TYPE;
--
--  Get Inventory Qty and Set Qty for all parts in the set
		 
CURSOR cur_part_details
	IS
		SELECT	 t208.c205_part_number_id pnum, t208.c901_unit_type unit_type
				 , DECODE (t208.c901_unit_type
						 , 40010, t208.c208_set_qty * p_qtytomfg
						 , 40011, t208.c208_set_qty
						 , 40012, 0
						  ) req_qty
			 	 , DECODE (my_context.get_my_inlist_ctx(t208.c205_part_number_id), NULL, get_mfg_partnum_inv_qty (t208.c205_part_number_id), 0) invqty 
				 , NVL(t208.c208_critical_tag,'1') tag
			  FROM t208_set_details t208, t205_part_number t205 --, v_in_list
			 WHERE t208.c207_set_id = p_bomid
			   AND t208.c205_part_number_id = t205.c205_part_number_id
		 	--   AND t208.c205_part_number_id = v_in_list.token(+)
			   AND t208.c208_set_qty <> 0
			   AND t208.c208_inset_fl = 'Y'
			   AND t208.c208_void_fl IS NULL
			   AND t208.c901_unit_type <> 40012
		  ORDER BY tag ASC, invqty DESC  
	  	FOR UPDATE;   --40010  - 'one each',  40011 - 'No multiplication', 40012 - 'One each Info'
BEGIN 
	
	
	v_bo_inputstr	:= REPLACE (v_bo_inputstr, '^', ',');		--force Back order String, format 'MS.0101, MS.0115, MS0103'
	my_context.set_my_inlist_ctx (v_bo_inputstr);
	my_context.set_double_inlist_ctx (v_inputstr); 				--valid parts string, format 'MS.0101,3|MS.0115,3|MS0103,0|'
	
	--For saving the donor number and to get the donor id
	
	IF p_donor_num IS NOT NULL THEN
		gm_pkg_op_dhr.gm_op_sav_donor_number(p_donor_num, p_vendorid, p_createdby, v_donor_id);
	END IF;
	
	--	 validation. If set id is NULL, throw Error
	IF (p_bomid IS NULL)
	THEN
		raise_application_error (-20074, '');	-- SET ID cant be null while initiating MR
	END IF; 
	 		
		
	-- Get the Rev level of part at the time of placing PO
	SELECT	TRIM(c402_rev_num), get_potype_from_woid(p_woid), c401_purchase_ord_id
	INTO	v_rev_lvl, v_po_type, v_po_id
	FROM	t402_work_order
	WHERE	c402_work_order_id = p_woid;
	-- To get the company id from PO
 	SELECT c1900_company_id
      INTO v_company_id
    FROM t401_purchase_order
      WHERE c401_purchase_ord_id = v_po_id
    AND c401_void_fl IS NULL;

	--	 validation. PO type is product then throw error
	IF v_po_type = 3100
	THEN
		raise_application_error (-21000, '');	-- Cannot initiate Mfg run. Incorrect WO type.
	END IF;


	IF v_po_type = 3105 THEN
		SELECT s408_mfg_lot.nextval INTO v_lot_no FROM DUAL;
		SELECT 6 - LENGTH(v_lot_no) INTO v_zero_cnt FROM DUAL;
		--
		WHILE (v_cnt < v_zero_cnt) LOOP
			v_zero_str := v_zero_str || '0';
			v_cnt := v_cnt + 1;
		END LOOP;
		--
		v_control_no := 'LN'|| v_zero_str || v_lot_no;
	ELSIF v_po_type = 3104 THEN
		-- Format of Control # should be <MFGCODE><Y><JULIANDATE><LOTCODE><REV#> eg: BGE156DE
		SELECT	NVL(get_rule_value(c301_vendor_id,'Vendor-Lot-Code'),c301_lot_code), TO_CHAR(CURRENT_DATE,'YYYY')
		INTO	v_lot_code, v_year
		FROM	t301_vendor
		WHERE	c301_vendor_id = p_vendorid;
		
		-- Get the Year code from code lookup table
		SELECT	c902_code_nm_alt
		INTO	v_year_code
		FROM	t901_code_lookup
		WHERE	c901_code_nm = v_year
		AND	c901_code_grp = 'YEAR';
		
		--PC-3470 Fetch Manfacture Date
		SELECT max(lpad(TO_NUMBER (SUBSTR (c408_control_number, 4, 3)), 3, '0')) 
		INTO v_manf_date 
		FROM t408_dhr 
		WHERE c408_control_number like TRIM(v_lot_code||v_year_code) || '%';
		
		v_manf_date := NVL(v_manf_date,'001'); --setting default sequence
		--
		v_control_no := v_lot_code||v_year_code||v_manf_date;
		--
	    
		-- Get Number of DHRs already created for this part and for this day
		SELECT	COUNT(*)
		INTO	v_manf_run
		FROM	t408_dhr
		WHERE	c408_control_number like TRIM(v_control_no) || '%'; 
     
		--
		v_manf_run := NVL(v_manf_run,0) + 1; 
    
		--
		v_new_manf_run := v_manf_run;
		--PC-3470 Throw error if the lot no has 999 and exceeding 22 of max count (dhr lotno count)
		IF( v_manf_date = '999' AND v_manf_run > 22) THEN
		    raise_application_error ('-20999', 'Number of batches exceeds the limit.') ;
		END IF;
		
		IF v_manf_run   > 22 THEN
		   
		  --to form a v_manf_date based on adding one PC 3470
		    v_manf_date := lpad(TO_NUMBER (v_manf_date + 1), 3, '0'); --to adding one as three digit number like 001
		    
		    -- to form a new control number based on adding one PC 3470
		    v_control_no := v_lot_code||v_year_code||v_manf_date;
		    
		    -- get the new control number count.
		     SELECT COUNT ( *)
		       INTO v_new_manf_run
		       FROM t408_dhr
		      WHERE c408_control_number LIKE TRIM (v_control_no) || '%';
		      
		    --to form a v_new_manf_run based on adding one PC 3470
		   v_new_manf_run:= NVL(v_new_manf_run,0) + 1;  
		END IF;
		
		-- Get the Lot number from code lookup table
	  BEGIN
		SELECT	c902_code_nm_alt
		INTO	v_lot_no
		FROM	t901_code_lookup
		WHERE	c901_code_nm = to_char(v_new_manf_run)
		AND	c901_code_grp = 'LOTNO';
     EXCEPTION
        WHEN NO_DATA_FOUND THEN
          RETURN;
     END;
		-- Formulating Final Control number
		v_control_no := v_lot_code||v_year_code||v_manf_date||v_lot_no||trim(v_rev_lvl);
    
	END IF;
	--
	 
	-- Hardcoding Status Flag to 0 indicating Initialize status
	v_status_fl := 0;
	--
	-- Creating an Internal work Order (Manufacturing Run ID)
	SELECT 'GM-MWO-' || s408_dhr.NEXTVAL, get_plantid_frm_cntx()  INTO v_mfwo_seq_no, v_plant_id FROM dual;
	INSERT INTO T408_DHR
	(C408_DHR_ID
	,C408_CONTROL_NUMBER
	,C408_RECEIVED_DATE
	,C408_QTY_RECEIVED
	,C408_CREATED_DATE
	,C408_CREATED_BY
	,C408_STATUS_FL
	,C402_WORK_ORDER_ID
	,C205_PART_NUMBER_ID
	,C301_VENDOR_ID
	,C408_MANF_DATE
	,C2540_DONOR_ID
	,C2550_EXPIRY_DATE
	,c1900_company_id
	,C5040_PLANT_ID
	,C408_RW_FL  
	)
	VALUES (
	 v_mfwo_seq_no
	 ,v_control_no
	 ,CURRENT_DATE
	 ,p_qtytomfg
	 ,CURRENT_DATE
	 ,p_createdby
	 ,p_statusflag
	 ,p_woid
	 ,p_partnum
	 ,p_vendorid
	 ,TRUNC(CURRENT_DATE)
	 ,v_donor_id
	 ,p_expiry_date
	 ,v_company_id
	 ,v_plant_id
	 ,p_rwflag
	 );
	--
	 
	-- Commenting the below lines since it is using during verify DHR
    --IF p_expiry_date IS NOT NULL THEN
    --       GM_OP_SAV_LOT_EXPIRY_DATE (p_partnum,v_control_no,p_expiry_date,p_createdby); 
    --END IF;
    
    v_part_udi := gm_pkg_pd_rpt_udi.get_part_di_number(p_partnum); -- Get the DI No for Part 
    v_UDI_Num := NULL;
        IF v_part_udi IS not null THEN
        -- UDI No is formed   
 			v_UDI_Num := gm_pkg_pd_rpt_udi.get_dhr_part_udi(v_mfwo_seq_no, p_partnum);
        END IF;     

        -- To save/update UDI No value 
        gm_pkg_op_dhr.gm_save_di_no(v_mfwo_seq_no,v_UDI_Num,p_createdby);
     
	FOR var_part_detail IN cur_part_details
	LOOP
		v_partid	:= var_part_detail.pnum;
		v_req_qty	:= var_part_detail.req_qty;
		v_inv_qty	:= var_part_detail.invqty;
		v_tag		:= var_part_detail.tag;
		
		SELECT COUNT (1)
		  INTO v_count
		  FROM v_double_in_list
		 WHERE token = v_partid and tokenii > 0; 
  
	IF (v_count > 0)
	THEN		  
	 	IF v_inv_qty < 0
		THEN
			v_inv_qty	:= 0;
		END IF;
	 
		IF v_tag_fl <> v_tag
			THEN
			 	
			 	IF   v_inv_qty > 0 
				THEN
					v_status := 20;
				ELSE
					v_status := 10;
				END IF;
		

		--	IF ((v_inv_qty >= v_req_qty) OR (v_inv_qty < v_req_qty AND v_inv_qty > 0))
					--- create another MR for different Tag
				gm_mf_sav_request (30000
							 , TRUNC (CURRENT_DATE)
							 , p_bomid
							 , v_mfwo_seq_no
							 , v_status
							 , p_createdby
							 , CURRENT_DATE
							 , v_out_matreq_id
							  );
				-- Calling the procedure for updating the Status Details table
				gm_save_status_details (v_out_matreq_id
									  , v_status
									  , p_createdby
									  , '0'   --delete fl
									  , CURRENT_DATE
									  , 30100
									  , 30200
									   );
		  
		--
		END IF;
	 	  
		  
		--	Since we do orderby on invqty and the first invqty is > 0, we create MR
		IF (NOT v_first_time_flag)
		THEN
			IF v_inv_qty = 0
			THEN
				-- The request is a backorder since none of the parts have Req qty > Inv qty
				UPDATE t303_material_request
				   SET c303_status_fl = '10'	--'3'   --20(0), Initiated,  30(1) In process, 40(2) Completed,	10(3) Back Order
				 WHERE c303_material_request_id = v_out_matreq_id;

				UPDATE t905_status_details
				   SET c905_status_fl = '10'	--'3'
				 WHERE c905_ref_id = v_out_matreq_id;
			END IF;
		END IF;
		
		SELECT c905_status_fl
		  INTO v_status_fl
		  FROM t905_status_details
		 WHERE c905_ref_id = v_out_matreq_id;
		
	 	  
		--	Consign the parts which has sufficient bulkqty
		IF v_inv_qty >= v_req_qty
		THEN
			gm_mf_sav_load_bom_parts (v_out_matreq_id, v_req_qty , p_bomid, v_partid);
		END IF;
 
		IF v_inv_qty < v_req_qty
		THEN
			--	 Calculate backorderQty and update request detail  accordingly
			v_backorderqty := v_req_qty - v_inv_qty;

			IF v_inv_qty > 0
			THEN
				--
				gm_mf_sav_load_bom_parts (v_out_matreq_id, v_inv_qty , p_bomid, v_partid);
			END IF;
			
			 
			
				--- create another MR for Back order
			IF v_status_fl <> '10'	--'3'
			THEN
				gm_mf_sav_request (30000
								 , TRUNC (CURRENT_DATE)
								 , p_bomid
								 , v_mfwo_seq_no
								 , '10'			--'3'	 --v_status_fl
								 , p_createdby
								 , CURRENT_DATE
								 , v_out_matreq_id
								  );
				gm_save_status_details (v_out_matreq_id, '10'	--'3'   --v_status_fl
															, p_createdby, '0', CURRENT_DATE, 30100, 30200);
				--
				gm_mf_sav_load_bom_parts (v_out_matreq_id, v_backorderqty , p_bomid, v_partid);
			ELSE
			
				gm_mf_sav_load_bom_parts (v_out_matreq_id, v_backorderqty , p_bomid, v_partid);
			END IF;		  
		 
		END IF;
		
		v_first_time_flag := TRUE;
		v_tag_fl	:= v_tag;
		END IF;
	END LOOP;
  
 p_lotcode := v_control_no;
 p_mfgdhrwoid := v_mfwo_seq_no;

	--To save hrf and mrf in t2550
	BEGIN 
		GM_UPDATE_UDI_FORMAT(v_mfwo_seq_no || ',', p_createdby);
	EXCEPTION
	WHEN OTHERS THEN
		v_msg:= v_msg; -- Just a dummy statement
	END;
 
 END GM_INITIATE_MFG_WO;
 /