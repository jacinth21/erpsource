CREATE OR REPLACE PROCEDURE GM_MF_FCH_WO_STATUS
(
	p_woid		IN  t408_dhr.c402_work_order_id%TYPE
	,p_recordset	OUT Types.cursor_type
)
AS
--
/********************************************************************************************
 * Description  :This procedure is used for fetching status of Globus' Work Orders.
 * Change Details:
 *		 James	Sep25,2006	Created	
 *********************************************************************************************/
--
BEGIN
 --
	OPEN p_recordset FOR
	SELECT 
		c408_dhr_id DHRID
		,c408_qty_received QTY
		,c408_control_number CNUM
		,get_user_name(c408_created_by) CUSER
		,TO_CHAR(c408_created_date,get_rule_value('DATEFMT','DATEFORMAT')) CDATE
		,c205_part_number_id PNUM
		,c408_status_fl SFL
	FROM 
		t408_dhr
	WHERE 
		c402_work_order_id = p_woid;
 --
END GM_MF_FCH_WO_STATUS;
/