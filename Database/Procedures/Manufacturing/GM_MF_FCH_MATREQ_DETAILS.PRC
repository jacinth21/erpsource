
--@"C:\DATABASE\PROCEDURES\manufacturing\GM_MF_FCH_MATREQ_DETAILS.prc";

CREATE OR REPLACE PROCEDURE GM_MF_FCH_MATREQ_DETAILS
(
	p_mfgdhrwoid	IN	t408_dhr.c408_dhr_id%TYPE,
	p_recordset	OUT	Types.cursor_type)
AS
/***************************************************************************************
 * Description           :	This procedure is called to get the Manufacturing Material Request details
 *						 
 ****************************************************************************************/
v_str VARCHAR2(2000);
BEGIN
--
	IF p_mfgdhrwoid IS NOT NULL THEN
	OPEN p_recordset FOR
		SELECT	t303.c303_material_request_id matreqid
			,t303.c303_material_request_dt reqdate
			,GET_USER_NAME(t303.c303_created_by) requestor
		--	,DECODE(t303.c303_status_fl,0,'Initiated',1, 'In Progress',2, 'Released', 3, 'Back Order') statusfl
			,DECODE(t303.c303_status_fl,20,'Initiated',30, 'In Progress',40, 'Released', 10, 'Back Order') statusfl
			,t408.c408_control_number CNUM
			,t408.c205_part_number_id PNUM
		FROM
			t303_material_request t303, t408_dhr t408
		WHERE	t408.c408_dhr_id = p_mfgdhrwoid
		AND		t408.c408_dhr_id = t303.c408_dhr_id
    AND t408.c408_void_fl is null 
    AND t303.c303_void_fl is null
    AND t303.c303_delete_fl is null;
	ELSE
	OPEN p_recordset FOR
		SELECT	t303.c303_material_request_id matreqid
			,t303.c303_material_request_dt reqdate
			,get_user_name(t303.c303_created_by) requestor
		--	,DECODE(t303.c303_status_fl,0,'Initiated',1, 'In Progress',2, 'Released', 3, 'Back Order') statusfl
			,DECODE(t303.c303_status_fl,20,'Initiated',30, 'In Progress',40, 'Released', 10, 'Back Order') statusfl
			,t408.c408_dhr_id woid
			,t303.c207_set_id bomid
			,t303.c303_parent_id parentid
			,get_code_name(t303.c901_type) rtype
			,t408.c408_control_number CNUM
			,t408.c205_part_number_id PNUM
		FROM
			t303_material_request t303, t408_dhr t408
		WHERE	t408.c408_dhr_id = t303.c408_dhr_id
		AND		t303.c303_status_fl < 40	--2;
    AND t408.c408_void_fl is null 
    AND t303.c303_void_fl is null
    AND t303.c303_delete_fl is null;
	END IF;
--
END GM_MF_FCH_MATREQ_DETAILS;
/

