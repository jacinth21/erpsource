CREATE OR REPLACE PROCEDURE GM_MF_FCH_PEND_PARTS_RECEIVE
(
    p_recordset	OUT Types.cursor_type
)
AS
--
/********************************************************************************************
 * Description  :This procedure is used for fetching Pending Transfer requests
		 between Main Inventory and Supply Inventory.
 * Change Details:
 *		 James	Sep12,2006	Created	
 *********************************************************************************************/
--
BEGIN
 --
	OPEN p_recordset FOR
	SELECT 
		t205b.c402_work_order_id WOID
		,t205a.c205a_part_mapping_id MAPID
		,t205b.c205b_received_qty QTY
		,GET_USER_NAME(t205b.c205b_created_by) CUSER
		,TO_CHAR(t205b.c205b_created_date,'mm/dd/yyyy') CDATE
		,t205a.c205_to_part_number_id TOPARTNUM
		,t205a.c205_from_part_number_id FROMPARTNUM
	FROM 
		t205b_part_receive t205b, t205a_part_mapping t205a
	WHERE 
		c205b_received_dt IS NULL
	AND 
		t205b.c205a_part_mapping_id = t205a.c205a_part_mapping_id
		AND t205a.c205a_void_fl is null;   ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
 --
END GM_MF_FCH_PEND_PARTS_RECEIVE;
/