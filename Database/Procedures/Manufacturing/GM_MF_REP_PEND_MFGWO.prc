CREATE OR REPLACE PROCEDURE GM_MF_REP_PEND_MFGWO
(
	p_recordset		OUT	Types.cursor_type
)
AS
v_compid_frm_cntx VARCHAR2(50);

/***************************************************************************************
 * Description           :This procedure is called to get the Pending work Orders
 *				
 ****************************************************************************************/
--
BEGIN
	--
SELECT get_compid_frm_cntx() INTO v_compid_frm_cntx  FROM DUAL;
     --
	OPEN p_recordset FOR
		SELECT 
			t402.c402_work_order_id WOID
			,t402.c205_part_number_id PNUM
			,Get_Partnum_Desc(t402.c205_part_number_id) PDESC
			,t402.c402_created_date CDATE
			,t402.c402_qty_ordereD QTYORD
			,Get_Wo_Mfg_Pend_Qty(t402.c402_work_order_id,t402.c402_qty_ordered) QTYPEND
			,t402.c402_status_fl SFL
		FROM
			T401_PURCHASE_ORDER t401, T402_WORK_ORDER t402
		WHERE
			t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
		AND T401.C1900_COMPANY_ID = v_compid_frm_cntx
		AND	t401.c401_type IN (3104,3105)
		AND	t402.c402_status_fl < 3
		AND	t402.c402_void_fl IS NULL
		ORDER BY t402.c205_part_number_id;
	--
END GM_MF_REP_PEND_MFGWO;
/
