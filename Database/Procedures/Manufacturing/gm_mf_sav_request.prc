/* Formatted on 2010/12/07 11:00 (Formatter Plus v4.8.0) */

--@"C:\DATABASE\PROCEDURES\manufacturing\gm_mf_sav_request.prc";

CREATE OR REPLACE PROCEDURE gm_mf_sav_request (
	p_type			  IN	   t303_material_request.c901_type%TYPE
  , p_request_dt	  IN	   t303_material_request.c303_material_request_dt%TYPE
  , p_bomid 		  IN	   t303_material_request.c207_set_id%TYPE
  , p_mfwo_seq_no	  IN	   t303_material_request.c408_dhr_id%TYPE
  , p_status_fl 	  IN	   t303_material_request.c303_status_fl%TYPE
  , p_created_by	  IN	   t303_material_request.c303_created_by%TYPE
  , p_created_date	  IN	   t303_material_request.c303_created_date%TYPE
  , p_matreq_seq_no   OUT	   t303_material_request.c303_material_request_id%TYPE
)
AS
BEGIN
	SELECT 'GM-MR-' || s303_matreq_seq_no.NEXTVAL
	  INTO p_matreq_seq_no
	  FROM DUAL;

	INSERT INTO t303_material_request
				(c303_material_request_id, c901_type, c303_material_request_dt, c207_set_id, c408_dhr_id
			   , c303_status_fl, c303_created_by, c303_created_date
				)
		 VALUES (p_matreq_seq_no, p_type, p_request_dt, p_bomid, p_mfwo_seq_no
			   , p_status_fl, p_created_by, p_created_date
				);
END gm_mf_sav_request;
/