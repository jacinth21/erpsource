CREATE OR REPLACE PROCEDURE Gm_Mf_Fch_Bomprint
(
	p_mfgmatreqid	IN	T408_DHR.C408_DHR_ID%TYPE,
	p_mfgmatDhrid	IN	T408_DHR.C408_DHR_ID%TYPE,
	p_bomdetails	OUT	Types.cursor_type
)
AS
/****************************************************************************************
 * Description           : This procedure is called to get the Manufacturing BOM part list
 * 						 	   for printing purposes.
 *					     : Input would be the Material Request ID		      						 
 *				
 ****************************************************************************************/
--

--
BEGIN
	--
	IF p_mfgmatreqid  IS NOT NULL THEN
		OPEN p_bomdetails FOR 
			SELECT T304.C205_PART_NUMBER_ID PNUM
			     , Get_Partnum_Desc(T304.C205_PART_NUMBER_ID) PDESC
			     , T304.C304_CONTROL_NUMBER CONTROLNUM
			     , T304.C304_ITEM_QTY QTY
			     , T304.C304_MATERIAL_REQUEST_ITEM_ID MATREQID
			  FROM T304_MATERIAL_REQUEST_ITEM T304
		     WHERE T304.C303_MATERIAL_REQUEST_ID = p_mfgmatreqid;
	     
	ELSIF p_mfgmatDhrid IS NOT NULL THEN
	
		OPEN p_bomdetails FOR 
			SELECT pnum,pdesc,CONTROLNUM,sum(qty) QTY FROM (
				SELECT t304.c205_part_number_id pnum
					 , get_partnum_desc(t304.c205_part_number_id) PDESC
		             , c304_control_number CONTROLNUM
					 , SUM (DECODE (t303.c901_type, 30000, t304.c304_item_qty, 0)
					      - DECODE (t303.c901_type, 30001, t304.c304_item_qty, 0)
					      - DECODE (t303.c901_type, 30002, t304.c304_item_qty, 0)) qty
				  FROM t303_material_request T303
					 , t304_material_request_item T304
				 WHERE t303.c303_material_request_id = t304.c303_material_request_id
		           AND t303.c408_dhr_id = p_mfgmatDhrid
				   AND t303.c303_void_fl IS NULL
		        GROUP BY t304.c205_part_number_id,c304_control_number)
 		  GROUP BY pnum,pdesc,CONTROLNUM;
 	END IF;
 	--
 	IF p_mfgmatreqid IS NULL AND p_mfgmatDhrid IS NULL
	THEN
		OPEN p_bomdetails FOR
		SELECT * FROM DUAL WHERE 1=0;
	END IF;
	--
END Gm_Mf_Fch_Bomprint;
/

