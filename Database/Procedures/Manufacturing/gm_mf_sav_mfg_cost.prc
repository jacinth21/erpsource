-- @"C:\Database\Procedures\Manufacturing\gm_mf_sav_mfg_cost.prc";
CREATE OR REPLACE
PROCEDURE gm_mf_sav_mfg_cost (
        p_pnum   IN T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
        p_cost   IN T205_PART_NUMBER.c205_cost%TYPE,
        p_userid IN T205_PART_NUMBER.c205_last_updated_by%TYPE,
        p_message OUT VARCHAR2)
AS
    --
    /*******************************************************************************************
    *    Description    :This procedure is called to update Manufacturing Cost
    ********************************************************************************************/
    v_cost T205_PART_NUMBER.c205_cost%TYPE;
    v_count NUMBER;
BEGIN
    --
     SELECT COUNT (1)
       INTO v_count
       FROM t205_part_number t205
      WHERE c205_part_number_id = p_pnum;
    IF (v_count                 = 0) THEN
        raise_application_error ('-20108', '') ;
    END IF;
     UPDATE t205_part_number
    SET c205_cost               = p_cost, c205_last_updated_by = p_userid, c205_last_updated_date = SYSDATE
      WHERE c205_part_number_id = p_pnum;
    p_message                  := 'Manufacturing Cost Updated for the Part #: '||p_pnum;
    --
END gm_mf_sav_mfg_cost;
/