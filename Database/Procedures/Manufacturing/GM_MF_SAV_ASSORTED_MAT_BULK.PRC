/* Formatted on 2009/01/27 16:23 (Formatter Plus v4.8.0) */


CREATE OR REPLACE PROCEDURE gm_mf_sav_assorted_mat_bulk (
	p_inputstr	   IN	VARCHAR2
  , p_mat_req_id   IN	t303_material_request.c303_material_request_id%TYPE
)
AS
	v_string	   VARCHAR2 (30000) := p_inputstr;
	v_strlen	   NUMBER := NVL (LENGTH (p_inputstr), 0);
	v_partnumid    t205_part_number.c205_part_number_id%TYPE;
	v_itemqty	   t304_material_request_item.c304_item_qty%TYPE;
	v_controlno    t304_material_request_item.c304_control_number%TYPE;
	v_substring    VARCHAR2 (4000);
	v_inventoryqty t304_material_request_item.c304_item_qty%TYPE;
--
/*******************************************************************************************************
 * Description			 : This procedure is used to Save Material Request.
 *		 The Part Number and Qty which is sent in Input String will be tokenized
 *		 and sent to GM_MF_SAV_ASSORTED_MAT_SINGLE for insertion
 *
 ********************************************************************************************************/
BEGIN
--
	COMMIT;

	IF v_strlen > 0
	--
	THEN
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_partnumid := NULL;
			v_itemqty	:= NULL;
			v_controlno := NULL;
			v_inventoryqty := NULL;
			v_partnumid := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_itemqty	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_controlno := v_substring;
			v_inventoryqty := get_mfg_partnum_inv_qty (v_partnumid);

			IF v_itemqty > v_inventoryqty
			THEN
				raise_application_error (-20141, '');
			END IF;

			-- Calling Procedure to Insert Each of Part Number and Item Quantity
			gm_mf_sav_assorted_mat_single (p_mat_req_id, v_partnumid, v_itemqty, v_controlno);
		--
		END LOOP;
	--
	END IF;
--
END gm_mf_sav_assorted_mat_bulk;
/
