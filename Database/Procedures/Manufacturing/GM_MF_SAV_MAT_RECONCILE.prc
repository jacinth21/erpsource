--@"C:\DATABASE\PROCEDURES\manufacturing\gm_mf_sav_mat_reconcile.prc";

CREATE OR REPLACE PROCEDURE gm_mf_sav_mat_reconcile (
    p_lotcode       IN       t408_dhr.c408_control_number%TYPE
  , p_qtycomp       IN       NUMBER
  , p_returnstr     IN       VARCHAR2
  , p_scrapstr      IN       VARCHAR2
  , p_createdby     IN       t408_dhr.c408_created_by%TYPE
  , p_mat_rec_ids   OUT      VARCHAR2
)
AS
/***************************************************************************************
 * Description           :  This procedure is used to Save Material reconciliation details
 * 1.   48049   'WIP to Scrap'( if there is scrap qty)
 * 2.   48050   'WIP to Raw Material' (if there is return qty)
 * 3.   48051   'WIP to DHR'
 * 4.   48052   'WIP to Material Cost'
 * 5.   48053   'WIP to WIP Changes'
 *
 ****************************************************************************************/
    v_id           VARCHAR2 (20);
    v_dhr_id       VARCHAR2 (20);
    v_qty          VARCHAR2 (20);
    v_mat_req_status VARCHAR2 (20);
    v_mfgdhrwoid   t408_dhr.c408_dhr_id%TYPE;
    v_return_id    VARCHAR2 (20);
    v_scrap_id     VARCHAR2 (20);
    v_cost         NUMBER (15, 4);
    v_partqty      NUMBER;
    v_partnum      VARCHAR2 (20);
    v_diff         NUMBER (15, 4);
    v_usedqtyamt   NUMBER (15, 4);
    v_totalcost    NUMBER (15, 4);
    v_workorderid  VARCHAR2 (20);
    v_potype       NUMBER (10);
    v_itemcost     NUMBER (15, 4);
    v_status 	   t408_dhr.c408_status_fl%TYPE;
    -- International posting changes
    v_source_comp_id t1900_company.c1900_company_id%TYPE;
    v_mwo_posting_type NUMBER;

--
    CURSOR pop_val
    IS
        SELECT c205_part_number_id ID, c304_item_qty qty
          FROM t304_material_request_item
         WHERE c303_material_request_id = v_id AND TRIM (c304_control_number) IS NOT NULL;

    -- Insert product has separate account to post (same amount as DR/CR)
    -- So, based on product family to decide the posting type.      
    -- 26240467 Insert product family
    -- 48052	WIP to Material Cost
	-- 48050	WIP to Raw Material   
	  
    CURSOR cur_matcost
    IS
        SELECT   t304.c205_part_number_id partnum
               , SUM (  DECODE (t303.c901_type, 30000, t304.c304_item_qty, 0)
                      - DECODE (t303.c901_type, 30001, t304.c304_item_qty, 0)
                      - DECODE (t303.c901_type, 30002, t304.c304_item_qty, 0)
                     ) usedmatqty
               , t304.c304_item_price itemprice
               , 48052 posting_id --PC 2499 MWO Insert part GL account changes
            FROM t303_material_request t303, t304_material_request_item t304
            , t205_part_number t205
           WHERE t303.c408_dhr_id = v_dhr_id
             AND t303.c303_material_request_id = t304.c303_material_request_id
             AND t205.c205_part_number_id = t304.c205_part_number_id
             AND TRIM (t304.c304_control_number) IS NOT NULL
        GROUP BY t304.c205_part_number_id, t304.c304_item_price,t205.c205_product_family;
--
BEGIN
--
-- Getting the DHR ID alias MFG Work Order id from the lotcode
    SELECT get_dhrid_from_lotcode (p_lotcode)
      INTO v_mfgdhrwoid
      FROM DUAL;

	SELECT c408_status_fl 
		INTO v_status
		FROM t408_dhr
	WHERE c408_dhr_id = v_mfgdhrwoid
	FOR UPDATE;
	
	IF(v_status <> 0)
		THEN 
		raise_application_error (-20111, '');		
	END IF;
--
-- Updating the Qty received field to the value sent from the screen
-- This override is when the Qty manufactured is different (great or less) than the Qty meant to be manufactured in
-- the production Run.
-- also updating the Status flag to 1 to show under to be Inpsected
     --Reverted PMT-45686 code change
     UPDATE t408_dhr
       SET c408_shipped_qty = c408_qty_received
         , c408_qty_received = p_qtycomp
         , c408_status_fl = 1
     WHERE c408_dhr_id = v_mfgdhrwoid;

     -- Retrieving the PO type to determine the Account to post to.
    -- If the PO Type is 3105 (BioMat Manufactured Supply eg:gelatin), then post to Raw materials (48050)
    -- Else post to DHR (48051)
    -- Get the PO Type by fetching the WO id from DHR id in DHR table and then join the WO and PO table to get PO Type

    -- Fetching WO id
    SELECT t408.c402_work_order_id
      INTO v_workorderid
      FROM t408_dhr t408
     WHERE t408.c408_dhr_id = v_mfgdhrwoid;

    -- Fetching the PO type
    SELECT t401.c401_type, NVL(t401.c1900_source_company_id, t401.c1900_company_id)
      INTO v_potype, v_source_comp_id
      FROM t401_purchase_order t401, t402_work_order t402
     WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id AND t402.c402_work_order_id = v_workorderid;
--
-- IF during recon, there are parts that are to be returned to the Supply Inventory
    IF TRIM (p_returnstr) IS NOT NULL
    THEN
        gm_mf_sav_mat_reconcile_trans (v_mfgdhrwoid, 'RETURN', p_returnstr, p_createdby, v_return_id);
        --
        v_id        := v_return_id;

        OPEN pop_val;

        LOOP
            FETCH pop_val
             INTO v_partnum, v_partqty;

            EXIT WHEN pop_val%NOTFOUND;

            --
	    gm_cm_sav_partqty(v_partnum,v_partqty,v_id,p_createdby,90802,4301,30002);
	    --
            /*UPDATE t205_part_number
               SET c205_qty_in_stock = NVL (c205_qty_in_stock, 0) + v_partqty
                 , c901_action = 4301   -- Plus
                 , c901_type = 4313   -- Quarantine
                 , c205_last_update_trans_id = v_id
                 , c205_last_updated_by = p_createdby
             WHERE c205_part_number_id = v_partnum;
	    */
            --
            -- This posting is from WIP to Raw Material
            gm_save_ledger_posting (48050, CURRENT_DATE, v_partnum, NULL, v_return_id, v_partqty, 0, p_createdby, NULL, v_source_comp_id, 0);
        --
        END LOOP;

        CLOSE pop_val;

        p_mat_rec_ids := p_mat_rec_ids || v_return_id || ',';
--
    END IF;

--
-- IF during recon, there are parts that need to be scrapped
    IF TRIM (p_scrapstr) IS NOT NULL
    THEN
        gm_mf_sav_mat_reconcile_trans (v_mfgdhrwoid, 'SCRAP', p_scrapstr, p_createdby, v_scrap_id);
        --
        v_id        := v_scrap_id;

        OPEN pop_val;

        LOOP
            FETCH pop_val
             INTO v_partnum, v_partqty;

            EXIT WHEN pop_val%NOTFOUND;
            --
            -- This posting is from Bio WIP to Scrap
            gm_save_ledger_posting (48153, CURRENT_DATE, v_partnum, NULL, v_scrap_id, v_partqty, 0, p_createdby, NULL, v_source_comp_id, 0);
        END LOOP;

        CLOSE pop_val;

        --
        p_mat_rec_ids := p_mat_rec_ids || v_scrap_id;
--
    END IF;

--
-- To Post to DHR Account from WIP. Taking Cost from WO Table
    SELECT c205_part_number_id
      INTO v_partnum
      FROM t408_dhr
     WHERE c408_dhr_id = v_mfgdhrwoid;

--
   /* SELECT c402_cost_price
      INTO v_cost
      FROM t402_work_order t402, t408_dhr t408
     WHERE t402.c402_work_order_id = t408.c402_work_order_id AND t408.c408_dhr_id = v_mfgdhrwoid;
  */
  
     SELECT NVL(t205.c205_cost,decode(t408.c901_type, 80191 , t402.c402_split_cost ,decode(t402.c402_rm_inv_fl, 'Y', t402.c402_cost_price - t402.c402_split_cost , t402.c402_cost_price)))
      INTO v_cost
      FROM T402_WORK_ORDER t402, T408_DHR t408, t205_part_number t205
     WHERE t402.c402_work_order_id = t408.c402_work_order_id 
     AND t402.c205_part_number_id = t205.c205_part_number_id
     AND t408.c408_dhr_id = v_mfgdhrwoid;
    --

    -- for PO type 3104 (product) and 3105(Mfg modified supply), this will post to DHR Hold Area.
    -- There wont be posting from WIP as there is no qty in WIP at that time (only supplies in WIP)
    gm_save_ledger_posting (48051, CURRENT_DATE, v_partnum, NULL, v_mfgdhrwoid, p_qtycomp, v_cost, p_createdby, NULL, v_source_comp_id, v_cost);
    --
    -- To Post from WIP to Material Cost
    v_dhr_id    := v_mfgdhrwoid;
    
	-- PMT-39738: Manufacturing Work Order One Sided Posting Correction
	-- Based on product family getting the posting type
    OPEN cur_matcost;
	
    LOOP
        FETCH cur_matcost
         INTO v_partnum, v_partqty, v_itemcost, v_mwo_posting_type;

        EXIT WHEN cur_matcost%NOTFOUND;
        gm_save_ledger_posting (v_mwo_posting_type, CURRENT_DATE, v_partnum, NULL, v_mfgdhrwoid, v_partqty, v_itemcost, p_createdby, NULL, v_source_comp_id, v_itemcost);
    END LOOP;

    CLOSE cur_matcost;

    -- PMT-39738: Manufacturing Work Order One Sided Posting Correction
 	-- To handle Insert and FSI parts
 	
	--31	13020000	Inventory not Inspected
	--32	73200000	Bio Applied OH and Variances

-- To Post from WIP to WIP Change
    SELECT NVL(ROUND(SUM (c810_cr_amt * c810_qty),4), 0)
      INTO v_usedqtyamt
      FROM t810_posting_txn
     WHERE c810_txn_id = v_mfgdhrwoid
       AND c901_account_txn_type = 48052   -- This value maps to WIP to Material Cost
       AND c801_account_element_id NOT IN (32, 31);   -- This value maps to Bio MFG WIP to WIP changes in the Cost Mapping table

--  v_TotalCost is the total Value of the Manufactured Run of Finished Product
    v_totalcost := ROUND((v_cost * p_qtycomp),4);
--
    v_diff      := v_totalcost - v_usedqtyamt;

    -- For Negative values of v_diff, posting has to happen at 48058, so that we dont have a negative credit value
    IF (v_diff < 0)
    THEN
        v_diff      := v_diff * -1;
        gm_save_ledger_posting (48058, CURRENT_DATE, 'MFGEXPENSE', NULL, v_mfgdhrwoid, 1, v_diff, p_createdby, NULL, v_source_comp_id, v_diff);
    ELSE
        gm_save_ledger_posting (48053, CURRENT_DATE, 'MFGEXPENSE', NULL, v_mfgdhrwoid, 1, v_diff, p_createdby, NULL, v_source_comp_id, v_diff);
    END IF;
--
END gm_mf_sav_mat_reconcile;
/