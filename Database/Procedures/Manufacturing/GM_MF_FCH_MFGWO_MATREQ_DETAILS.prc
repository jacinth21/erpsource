
--@"C:\DATABASE\PROCEDURES\manufacturing\GM_MF_FCH_MFGWO_MATREQ_DETAILS.prc";

CREATE OR REPLACE PROCEDURE GM_MF_FCH_MFGWO_MATREQ_DETAILS
(
	 p_lotcode		IN	t408_dhr.c408_control_number%TYPE
	,p_type			IN	VARCHAR2
	,p_mfgwo_matreq_details	OUT	Types.cursor_type
)
AS
/****************************************************************************
 * Description           :This procedure is called to fetch Mat Req Details 
			  either by Int WO ID or by Mat Req ID
 ****************************************************************************/
v_mfgdhrwoid VARCHAR2(20);
v_company_id  t1900_company.c1900_company_id%TYPE;
BEGIN
--
	 select get_compid_frm_cntx()
       into v_company_id
       from dual;
       
	IF p_type <> 'MATREQ' THEN
		SELECT	c408_dhr_id
		INTO	v_mfgdhrwoid
		FROM	t408_dhr
		WHERE	TRIM(c408_control_number) = p_lotcode
		  AND c1900_company_id = v_company_id
		  AND   c408_void_fl is null;
		
	END IF;
	--
	IF p_type = 'MATREQ' THEN
	OPEN p_mfgwo_matreq_details FOR
		SELECT  
			t304.c205_part_number_id PARTNUM
			,get_partnum_desc(t304.c205_part_number_id) PARTDESC
			,GET_MFG_MATREQ_PARTNUM_QTY(t303.c303_material_request_id,t304.c205_part_number_id) QTY
			,t304.c304_item_qty IQTY
			,c304_control_number CNUM
		--	,get_mfg_partnum_inv_qty(t304.c205_part_number_id) QTYONHAND
			--need to check status fl, if it's 10 (back order) or not
			,DECODE (t303.c303_status_fl
			  , 10, (DECODE (SIGN (get_mfg_partnum_inv_qty (t304.c205_part_number_id))
						   , -1, 0
						   , get_mfg_partnum_inv_qty (t304.c205_part_number_id)
							)
				 )
			  ,   get_mfg_partnum_inv_qty (t304.c205_part_number_id)
				+ get_mfg_matreq_partnum_qty (t303.c303_material_request_id, t304.c205_part_number_id)
			   ) qtyonhand
			,t303.c303_status_fl STATUSFL
			,t408.c408_status_fl DHRSTATUSFL
		FROM   
			t303_material_request T303
			,t304_material_request_item T304
			, t408_dhr t408
		WHERE t303.c303_material_request_id = t304.c303_material_request_id
		AND t303.c303_material_request_id  = p_lotcode
		AND t408.c408_dhr_id = t303.c408_dhr_id(+)
		AND t303.c303_void_fl IS NULL
		AND t408.c408_void_fl IS NULL;
	--
	ELSIF p_type = 'MATRECON' THEN
	OPEN p_mfgwo_matreq_details FOR
		SELECT
			t304.c205_part_number_id PARTNUM
			,get_partnum_desc(t304.c205_part_number_id) PARTDESC
			,SUM(t304.c304_item_qty) QTY
			,t304.c304_control_number CRLNUM
		--	,get_mfg_partnum_inv_qty(t304.c205_part_number_id) QTYONHAND
			,DECODE (SIGN (get_mfg_partnum_inv_qty (t304.c205_part_number_id))
						, -1, 0
						, get_mfg_partnum_inv_qty (t304.c205_part_number_id)
						 ) QTYONHAND
		FROM
			t303_material_request T303
			,t304_material_request_item T304
		WHERE
			t303.c303_material_request_id = t304.c303_material_request_id
		AND
			t303.c408_dhr_id  = v_mfgdhrwoid
		AND 
			t303.c303_status_fl = 40	--= 2
		AND
			TRIM(t304.c304_control_number) IS NOT NULL
		AND	
			t303.c303_delete_fl IS NULL
		AND 	
			t303.c303_void_fl IS NULL
		GROUP BY 
			c205_part_number_id, c304_control_number
		ORDER BY
			 t304.c205_part_number_id;
	--
	ELSE
	OPEN p_mfgwo_matreq_details FOR
		SELECT
			t304.c205_part_number_id PARTNUM
			,get_partnum_desc(t304.c205_part_number_id) PARTDESC
			,t304.c304_item_qty QTY
			,t304.c304_control_number CRLNUM
		--	,get_mfg_partnum_inv_qty(t304.c205_part_number_id) QTYONHAND
			,DECODE (SIGN (get_mfg_partnum_inv_qty (t304.c205_part_number_id))
						, -1, 0
						, get_mfg_partnum_inv_qty (t304.c205_part_number_id)
						 ) QTYONHAND
		FROM
			t303_material_request T303
			,t304_material_request_item T304
		WHERE
			t303.c303_material_request_id = t304.c303_material_request_id
		AND
			t303.c408_dhr_id  = v_mfgdhrwoid
		AND 	
			t303.c303_void_fl IS NULL
		ORDER BY
			t304.c205_part_number_id ;
	END IF;
--
END GM_MF_FCH_MFGWO_MATREQ_DETAILS;
/
