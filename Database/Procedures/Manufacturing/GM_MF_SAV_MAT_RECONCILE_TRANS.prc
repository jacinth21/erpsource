--@"C:\DATABASE\PROCEDURES\manufacturing\GM_MF_SAV_MAT_RECONCILE_TRANS.prc";

CREATE OR REPLACE PROCEDURE GM_MF_SAV_MAT_RECONCILE_TRANS
(	 
	p_mfgdhrwoid	IN	t408_dhr.c408_dhr_id%TYPE
	,p_type		IN	VARCHAR2
	,p_inputstr	IN	VARCHAR2
	,p_createdby	IN	t408_dhr.c408_created_by%TYPE
	,p_mat_req_id	OUT	t303_material_request.c303_material_request_id%TYPE
)
 AS
/***************************************************************************************
 * Description           :	This procedure is used to Save Material reconciliation details
 *				This can either be Return to Inventory or to Scrap
 *						 
 ****************************************************************************************/
v_mat_req_seq_no t303_material_request.c303_material_request_id%TYPE;
v_type	NUMBER;
--
BEGIN
--
IF p_type = 'RETURN' THEN
	SELECT 'GM-RT-' || s303_matreq_seq_no.NEXTVAL INTO v_mat_req_seq_no FROM dual;
	v_type := 30002;
ELSIF  p_type = 'SCRAP' THEN
	SELECT 'GM-SC-' || s303_matreq_seq_no.NEXTVAL INTO v_mat_req_seq_no FROM dual;
	v_type := 30001;
END IF;
--
	INSERT INTO t303_material_request
	(c303_material_request_id
	,c901_type
	,c303_material_request_dt
	,c408_dhr_id
	,c303_status_fl
	,c303_created_by
	,c303_created_date
	)
	VALUES(
	v_mat_req_seq_no
	,v_type
	,TRUNC(SYSDATE)
	,p_mfgdhrwoid
	--,2
  ,40
	,p_createdby
	,SYSDATE
	);
--
GM_MF_SAV_ASSORTED_MAT_BULK (p_inputstr, v_mat_req_seq_no);
--
p_mat_req_id := v_mat_req_seq_no;
--
END GM_MF_SAV_MAT_RECONCILE_TRANS;
/
