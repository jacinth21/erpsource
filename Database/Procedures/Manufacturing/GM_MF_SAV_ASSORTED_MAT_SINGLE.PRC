CREATE OR REPLACE PROCEDURE GM_MF_SAV_ASSORTED_MAT_SINGLE
(
  p_mat_req_id IN T303_MATERIAL_REQUEST.C303_MATERIAL_REQUEST_ID%TYPE
 ,p_partnumid IN T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE
 ,p_itemqty  IN T304_MATERIAL_REQUEST_ITEM.C304_ITEM_QTY%TYPE
 ,p_controno  IN t304_material_request_item.c304_control_number%TYPE
)
AS
--
/*******************************************************************************************************
 * Description           : This procedure is used to Save Material Request.
 *       The Part Number and Qty which are tokenized from GM_MF_SAV_ASSORTED_MAT_BULK
 *        are inserted into T304_MATERIAL_REQUEST_ITEM
 *
 ********************************************************************************************************/
exp_incorrect_part EXCEPTION;
PRAGMA EXCEPTION_INIT(exp_incorrect_part,-02291);

BEGIN
--
	INSERT INTO T304_MATERIAL_REQUEST_ITEM
	(  
	c304_material_request_item_id
	,c303_material_request_id
	,c205_part_number_id
	,c304_item_qty
	,c304_control_number
	,c304_item_price
	)VALUES(
	s304_matreqitem_seq_no.NEXTVAL
	,p_mat_req_id
	,p_partnumid
	,p_itemqty
	,p_controno
	,GET_INVENTORY_COGS_VALUE(p_partnumid)
	);
EXCEPTION
WHEN exp_incorrect_part
THEN
	RAISE_APPLICATION_ERROR( -20007, '' );
--
END GM_MF_SAV_ASSORTED_MAT_SINGLE;
/