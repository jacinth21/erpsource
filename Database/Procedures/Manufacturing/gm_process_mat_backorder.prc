/* Formatted on 2010/12/06 16:48 (Formatter Plus v4.8.0) */

--@"C:\DATABASE\PROCEDURES\manufacturing\gm_process_mat_backorder.prc";

CREATE OR REPLACE PROCEDURE gm_process_mat_backorder (
	p_mareq_id	 IN   t303_material_request.c303_material_request_id%TYPE
  , p_user_id	 IN   t303_material_request.c303_last_updated_by%TYPE
)
AS
	--
	v_onhand_qty   NUMBER;
	v_req_qty	   NUMBER;

	CURSOR cur_part_details
	IS
		SELECT get_mfg_matreq_partnum_qty (t303.c303_material_request_id, t304.c205_part_number_id) qty
			 , get_mfg_partnum_inv_qty (t304.c205_part_number_id) qtyonhand, t303.c303_status_fl statusfl
		  FROM t303_material_request t303, t304_material_request_item t304
		 WHERE t303.c303_material_request_id = t304.c303_material_request_id
		   AND t303.c303_material_request_id = p_mareq_id;
--
BEGIN
	--
	FOR var_part_detail IN cur_part_details
	LOOP
		v_onhand_qty := var_part_detail.qtyonhand;
		v_req_qty	:= var_part_detail.qty;

		IF v_req_qty > v_onhand_qty
		THEN
			raise_application_error (-20093, '');
		END IF;
	END LOOP;

	UPDATE t303_material_request
	   SET c303_status_fl = '20'
		 , c303_last_updated_by = p_user_id
		 , c303_last_updated_date = SYSDATE
	 WHERE c303_material_request_id = p_mareq_id;

	UPDATE t905_status_details
	   SET c905_status_fl = '20'
		 , c905_updated_by = p_user_id
		 , c905_updated_date = SYSDATE
	 WHERE c905_ref_id = p_mareq_id;
--
END gm_process_mat_backorder;
/