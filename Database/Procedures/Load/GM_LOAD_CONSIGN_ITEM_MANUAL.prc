CREATE OR REPLACE PROCEDURE GM_LOAD_CONSIGN_ITEM_MANUAL
(
  p_consignment_id  IN  t504_consignment.c504_consignment_id%TYPE
 ,p_set_id   IN t504_consignment.c207_set_id%TYPE
)
AS
--
 CURSOR setmaster_cursor IS
  SELECT   c207_set_id
    ,c205_part_number_id
    ,temo_qty
  FROM  temp_set_master
  WHERE  c207_set_id = p_set_id
  ORDER BY c205_part_number_id;
 --
 v_part_num t205_part_number.c205_part_number_id%TYPE;
BEGIN
--
 --
 FOR setmaster_val IN setmaster_cursor
 LOOP
 --
  v_part_num := setmaster_val.c205_part_number_id;
  INSERT INTO T505_ITEM_CONSIGNMENT
  (  c505_item_consignment_id
      ,c504_consignment_id
      ,c205_part_number_id
      ,c505_control_number
      ,c505_item_qty
      ,c505_item_price
     )
  VALUES
  (   s504_consign_item.NEXTVAL
   ,p_consignment_id
   ,setmaster_val.c205_part_number_id
   ,'NOC#'
      ,setmaster_val.temo_qty
      ,GET_PART_PRICE(setmaster_val.c205_part_number_id,'E')
     );
 --
 END LOOP;
--
EXCEPTION
 WHEN OTHERS
    THEN
     DBMS_OUTPUT.put_line ('Pricing Error @ Part #' || v_part_num  || ' -*- ' ||p_set_id  );
--
END GM_LOAD_CONSIGN_ITEM_MANUAL;
/

