CREATE OR REPLACE PROCEDURE GM_LOAD_INHOUSE_MANUAL
AS
--
/******************************************************************************
 * Description : This procedure is used to load all inhouse consignment based on user input
 *
 *
 *******************************************************************************/
--
 CURSOR setdetail_cursor IS
  SELECT  c207_set_id
       ,c901_inhouse_purpose
       ,GET_CODE_NAME(c901_inhouse_purpose) purpose_nm
      ,temp_count
  FROM  temp_set_detail
  ORDER BY c207_set_id
       ,temp_count;
 --
 v_set_id  temp_set_detail.c207_set_id%TYPE;
 v_purpose  temp_set_detail.c901_inhouse_purpose%TYPE;
 v_temp_count temp_set_detail.temp_count%TYPE;
 --
 i PLS_INTEGER := 0;
 v_con_id  VARCHAR2(20);
 v_etch_id  t504a_consignment_loaner.c504a_etch_id%TYPE;
--
BEGIN
--
 -- To get the list of record to be posted
 FOR setdetail_val IN setdetail_cursor
 LOOP
 --
  v_set_id   := setdetail_val.c207_set_id;
  v_purpose   := setdetail_val.c901_inhouse_purpose;
  v_temp_count := setdetail_val.temp_count;
  --
  i := 0;
  --DBMS_OUTPUT.put_line ('Temp Count  Value ' || v_temp_count || ' Set Value ' || v_set_id || setdetail_val.purpose_nm);
  --
  WHILE i < v_temp_count
    LOOP
    --
     i := i + 1;
     --
     SELECT 'GM-CN-' || S504_CONSIGN.NEXTVAL
     INTO  v_con_id
     FROM  dual;
     --
     -- To frame etch number
     IF (v_purpose = 40000)
     THEN
      v_etch_id := 'TS #' || i;
     ELSIF (v_purpose = 40001)
     THEN
      v_etch_id := 'DISPLAY #' || i;
     ELSIF (v_purpose = 40002)
     THEN
      v_etch_id := 'PRDCT. RM #' || i;
     ELSIF (v_purpose = 40003)
     THEN
      v_etch_id := 'Ext.Cad #' || i;
     ELSIF (v_purpose = 40004)
     THEN
      v_etch_id := 'Int. Cad #' || i;
   END IF;
     --
     -- DBMS_OUTPUT.put_line (v_purpose || ' - ' || ' v_etch_id ' || v_etch_id);
     GM_LOAD_CONSIGNMENT_MANUAL (v_con_id, v_purpose, v_set_id, v_etch_id);
   GM_LOAD_CONSIGN_ITEM_MANUAL (v_con_id, v_set_id);
    --
    END LOOP;
    --

 --
 END LOOP; -- for loop ends here

--
END GM_LOAD_INHOUSE_MANUAL;
/

