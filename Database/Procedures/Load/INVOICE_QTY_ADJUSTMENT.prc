/* Formatted on 2009/01/23 18:46 (Formatter Plus v4.8.0) */


CREATE OR REPLACE PROCEDURE invoice_qty_adjustment
AS
	CURSOR qb_cursor
	IS
		SELECT c215_part_qty_adj_id p_adj_id, c205_part_number_id p_num, c216_qty_to_be_adjusted q_adj
		  FROM t216_part_qty_adj_details;

	v_qa_order_count NUMBER (2);
	v_qa_invoice_count NUMBER (2);
	v_invoice_count NUMBER (2);
	v_rec_payment_count NUMBER (2);
	v_temp		   VARCHAR2 (20);
	v_part_number  t214_transactions.c205_part_number_id%TYPE;
	v_tran_id	   t214_transactions.c214_transactions_id%TYPE;
	v_31_qty	   t214_transactions.c214_new_qty%TYPE;
	v_adj_value    t214_transactions.c214_new_qty%TYPE;
BEGIN
--
	FOR qb_val IN qb_cursor
	LOOP
		--
		v_part_number := qb_val.p_num;
		v_adj_value := qb_val.q_adj;

		--
		BEGIN
			--
			v_temp		:= 'No Part Exception ';

			--
			SELECT c214_transactions_id, c214_new_qty
			  INTO v_tran_id, v_31_qty
			  FROM t214_transactions
			 WHERE c214_transactions_id =
					   (SELECT MAX (c214_transactions_id)
						  FROM t214_transactions
						 WHERE TRUNC (c214_created_dt) <= TO_DATE ('10/31/2005', 'MM/DD/YYYY')
						   AND c205_part_number_id = v_part_number);

			--
			INSERT INTO t214_transactions
						(c214_transactions_id, c214_created_dt, c214_created_by, c205_part_number_id
					   , c214_orig_qty, c214_id, c214_trans_qty, c214_new_qty, c901_action, c901_type
						)
				 VALUES (s214_transactions.NEXTVAL, TO_DATE ('10/31/2005', 'MM/DD/YYYY'), 303043, v_part_number
					   , v_31_qty, qb_val.p_adj_id, v_adj_value, (v_31_qty + v_adj_value), 4303, 4316
						);

			--
			v_temp		:= 'Update	Exception ';

			--
			UPDATE t214_transactions
			   SET c214_orig_qty = c214_orig_qty + v_adj_value
				 , c214_new_qty = c214_new_qty + v_adj_value
			 WHERE c205_part_number_id = v_part_number
				   AND TRUNC (c214_created_dt) >= TO_DATE ('11/01/2005', 'MM/DD/YYYY');

			--
			
			 gm_cm_sav_partqty (v_part_number,
                               v_adj_value,
                                qb_val.p_adj_id,
                               303043,
                               90800, -- FG Qty
                               4303,  -- Manual
                               4316  -- Manual
                               );
			
		--
/*
		--
		EXCEPTION
			WHEN OTHERS
			THEN
				--
			--	gm_procedure_log (v_temp, ' Part Number ' || v_part_number);
		--
*/
		END;
	END LOOP;
--
EXCEPTION
	WHEN OTHERS
	THEN
--
		DBMS_OUTPUT.put_line (SQLERRM);
--
END invoice_qty_adjustment;
/
