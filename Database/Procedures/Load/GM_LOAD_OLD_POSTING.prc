CREATE OR REPLACE PROCEDURE GM_LOAD_OLD_POSTING
AS
--
/******************************************************************************
 * Description :	This procedure is used to load old back dated account entry 
 *					into the system . Takes the value from Transaction log and loaded 
 *					the value into the system 
 *					Type of Transaction 
 *					'******************'
 *					4310	- Sales			*
 *					4311	- Consignment	
 *					4312	- Return		*
 *					4313	- Quarantine	*
 *					4314	- Purchase		*
 *					4316	- Manual Adjustment	** NOT IN CURRENT SCOPE
 *
 *******************************************************************************/
--
	CURSOR txtp_cursor IS
		SELECT	 c899_created_dt        
				,c899_created_by        
				,c899_txn_id            
				,c205_part_number_id    
				,c899_qty               
				,c989_type              
		FROM	t899_transaction_backup
		WHERE 	c989_type = 911
		AND C899_ERROR_TYPE = 'E'
		ORDER BY c899_created_dt;
--WHERE 	c989_type = 4314 -- 1. Purchase (Check If) 
--WHERE 	c989_type = 4312 -- 2. Returns 
--WHERE 	c989_type = 4313 -- 3. Inventory to Quaran (Check If) 
--WHERE 	c989_type = 910  -- 4. Rework PO
--WHERE 	c989_type = 4314 -- 5. Rework Purchase (Check If) 
-- WHERE 	c989_type = 912  -- 6.  Quaran to Scrap
-- WHERE 	c989_type = 4313  -- 7. Quaran to Inventory ( Check If)
-- WHERE 	c989_type = 4310 -- 8. Sales 
-- WHERE 	c989_type = 4311 -- 9. Consignment (Inv to Buildset)
-- WHERE 	c989_type = 911  -- 10. B.set to Rep or inhouse
-- ****** Code to fix the issue ************ 
-- WHERE 	c989_type = 4311  AND C899_ERROR_TYPE = 'E'  ; Consignment 
-- WHERE 	c989_type = 4310  AND C899_ERROR_TYPE = 'E'  ; Sales
-- WHERE 	c989_type = 911  AND C899_ERROR_TYPE = 'F'  ; B.set to Other 
-- WHERE 	c989_type = 4312  AND C899_ERROR_TYPE = 'E'	Sales Return 
-- WHERE 	c989_type = 4311  AND C899_ERROR_TYPE = 'C'	' Inventory to Consignement 
-- WHERE 	c989_type = 4313  AND C899_ERROR_TYPE = 'E'	' Quaran to Inventory ( Check If)
-- WHERE 	c989_type = 912  AND C899_ERROR_TYPE = 'C'	' Quaran to Scrap
-- SELECT COUNT(1) FROM t822_costing_error_log 
-- SELECT COUNT(1)  FROM V810_POSTING_TXN
-- SELECT * from PROCEDURE_LOG -- Need to verify 
-- exec GM_LOAD_OLD_POSTING **
	v_trans_type		t214_transactions.c901_type%TYPE;
	v_trans_id			t899_transaction_backup.c899_txn_id%TYPE;		
	v_qty 				t899_transaction_backup.c899_qty%TYPE;		
	v_actual_qty 		t899_transaction_backup.c899_qty%TYPE;		
	v_created_dt 		t899_transaction_backup.c899_created_dt%TYPE;
	v_part_number		t899_transaction_backup.c205_part_number_id%TYPE;
	v_set_id 			t504_consignment.c207_set_id%TYPE;
	v_account_id		t504_consignment.c704_account_id%TYPE;
	v_distributor_id	t504_consignment.c701_distributor_id%TYPE;
	v_inhouse_purpose	t504_consignment.c504_inhouse_purpose%TYPE;
	v_purchase_id		t402_work_order.c401_purchase_ord_id%TYPE;
	v_posting_type 		NUMBER;
	v_work_order_id		t408_dhr.c402_work_order_id%TYPE;
	v_costing_id  		t820_costing.c820_costing_id%TYPE;
	v_cost_price		t402_work_order.c402_cost_price%TYPE;
	v_party_id     		t810_posting_txn.c803_period_id%TYPE;
--
BEGIN
--
	-- To get the list of record to be posted 
	FOR txtp_val IN txtp_cursor
	LOOP
	--
		v_trans_type 	:= txtp_val.c989_type;
		v_trans_id	 	:= txtp_val.c899_txn_id;
		v_qty		 	:= txtp_val.c899_qty;
		v_actual_qty	:= txtp_val.c899_qty;
		v_created_dt 	:= txtp_val.c899_created_dt;
		v_part_number	:= txtp_val.c205_part_number_id;
		--
		if ( v_qty < 0 ) 
		THEN 
			v_qty := (-1) * v_qty;
		END IF;
		--****************************************************
		-- IF MAPPED TO PURCHASE -- Starting point of posting
		--****************************************************
		-- Then follow the below process
		IF (v_trans_type = 4314) 
		THEN 
		--
			-- To Load the information in costing table 
			SELECT 	 c402_work_order_id
					,c301_vendor_id
			INTO	 v_work_order_id
					,v_party_id
			FROM 	t408_dhr
			WHERE 	c408_dhr_id = 	v_trans_id;				
			--
			SELECT	 DECODE(c301_vendor_id, 16,(c402_cost_price * 1.22) ,c402_cost_price)
					,c401_purchase_ord_id
			INTO	 v_cost_price
					,v_purchase_id
			FROM	t402_work_order
			WHERE	c402_work_order_id = v_work_order_id;
			--
			SELECT  DECODE(c401_type, 3101, 4826, 4810)
			INTO	v_posting_type 
			FROM	t401_purchase_order
			WHERE	c401_purchase_ord_id = v_purchase_id;	
			--
			-- DBMS_OUTPUT.put_line ('v_cost_price  ' || v_cost_price ||v_work_order_id || ' ** ' ||v_posting_type);
			-- Purchase
			IF (v_posting_type = 4826) -- Rework 
			--IF (v_posting_type = 4810) -- Main DHR To Inventory Purchase
			THEN 
  				GM_SAVE_LEDGER_POSTING ( v_posting_type ,v_created_dt
  													,v_part_number ,v_party_id
  													,v_trans_id ,v_qty, v_cost_price, 30300 );
  			END IF;
  			--								
		--*****************************************
		-- IF MAPPED TO SALES
		--*****************************************
		ELSIF (v_trans_type = 4310) 
		THEN 
		--
			-- To get the party if infromation (Account Information)
			BEGIN 
			--
				SELECT 	c704_account_id
				INTO	v_party_id
				FROM	t501_order
				WHERE 	c501_order_id  = v_trans_id;
				--
				GM_SAVE_LEDGER_POSTING ( 4811	,v_created_dt
											,v_part_number ,v_party_id
											,v_trans_id	,v_qty, NULL, 30300 );
			EXCEPTION
			WHEN NO_DATA_FOUND THEN
				DBMS_OUTPUT.put_line ('v_trans_id	- ' || v_trans_id);
			END ;								
		--*****************************************
		--IF MAPPED TO CONSIGNMENT
		--*****************************************
		ELSIF (v_trans_type = 4311) 
		THEN 
			-- To find if its part of build set 
			SELECT 	 c207_set_id
					,c704_account_id
					,c701_distributor_id
					,c504_inhouse_purpose
			INTO	 v_set_id
					,v_account_id
					,v_distributor_id
					,v_inhouse_purpose
			FROM 	t504_consignment 
			WHERE	c504_consignment_id = v_trans_id
					AND c504_void_fl IS NULL;
			-- If set id NULL means build set 
			IF (v_set_id IS NOT NULL) 
			THEN 
			--
				-- IF less than zero inventory to build set 
				IF	(v_actual_qty < 0 ) 
				THEN
				--
					v_posting_type := 4812;
				--
				ELSE
				-- build set  to inventory
					v_posting_type := 4823;
				--
				END IF;
			--
			-- In house item consignment 
			ELSIF (v_account_id = '01' AND  v_set_id IS NULL)
			THEN
			--
				SELECT 	get_rule_value(v_inhouse_purpose, 'CON-ITEM-INHOUSE')
				INTO	v_posting_type 
				FROM	DUAL;
			-- Sales Rep consignment 
			ELSIF (v_distributor_id IS NOT NULL AND  v_set_id IS NULL)
			THEN
			--
				v_posting_type := 4822;
			--
			END IF;
			GM_SAVE_LEDGER_POSTING ( v_posting_type	,v_created_dt
												,v_part_number ,v_party_id
												,v_trans_id	,v_qty, NULL, 30300 );
		--*****************************************
		-- Set Consignment to vendor or inhouse
		--*****************************************
		ELSIF (v_trans_type = 911) 
		THEN 
		--
			-- To find if its part of build set 
			SELECT 	 c207_set_id
					,c704_account_id
					,c701_distributor_id
					,c504_inhouse_purpose
			INTO	 v_set_id
					,v_account_id
					,v_distributor_id
					,v_inhouse_purpose
			FROM 	t504_consignment 
			WHERE	c504_consignment_id = v_trans_id
					AND c504_void_fl IS NULL;
			IF (v_account_id = '01')
			THEN
			--
				SELECT 	get_rule_value(v_inhouse_purpose, 'CON-SET-INHOUSE')
				INTO	v_posting_type 
				FROM	DUAL;
			-- Sales Rep consignment 
			ELSIF (v_distributor_id IS NOT NULL)
			THEN
			--
				v_posting_type := 4814;
			--
			END IF;
			--
			GM_SAVE_LEDGER_POSTING ( v_posting_type	,v_created_dt
												,v_part_number ,v_party_id
												,v_trans_id	,v_qty, NULL, 30300 );
			-- 
		--*****************************************************************
		-- IF MAPPED TO RETURN (CAN BE BOTH CONSIGNMENT AND SALES RETURN) 
		--*****************************************************************
		ELSIF (v_trans_type = 4312)
		THEN
		-- 
			-- Below code to check if its an item consignment 
			-- To get the party if infromation (Account Information)
			SELECT 	 NVL(c701_distributor_id, c704_account_id)
					,DECODE(c704_account_id,NULL,4819, '01', 4828, 4818) 
			INTO	 v_party_id	
					,v_posting_type
			FROM	t506_returns
			WHERE 	c506_rma_id  = v_trans_id
					AND c506_void_fl IS NULL;

			--
			GM_SAVE_LEDGER_POSTING ( v_posting_type	,v_created_dt
													,v_part_number ,v_party_id
													,v_trans_id	,v_qty, NULL, 30300 );
			--		
		--*****************************************************************	
		-- IF MAPPED TO QUARANTINE 
		--*****************************************************************
		ELSIF (v_trans_type = 4313)
		THEN
		-- 
			IF	(v_actual_qty < 0 ) 
			THEN
			--
				v_posting_type := 4815;
			--
			ELSE
			-- QUARANTINE  to inventory
				v_posting_type := 4824;
			--
			END IF;
			--
			IF (v_posting_type = 4815) -- Inventory to Quarantine  
			--IF (v_posting_type = 4824) -- Quarantine   to inventory
			THEN 
				GM_SAVE_LEDGER_POSTING ( v_posting_type	,v_created_dt
													,v_part_number ,v_party_id
													,v_trans_id	,v_qty, NULL, 30300 );
			END IF;										
			--		
		--*****************************************************************	
		-- MOVED FROM QUARANTINE TO SCRAP
		--*****************************************************************
		ELSIF (v_trans_type = 912)
		THEN
		-- 
			v_posting_type := 4820;
			--
			GM_SAVE_LEDGER_POSTING ( v_posting_type	,v_created_dt
													,v_part_number ,v_party_id
													,v_trans_id	,v_qty, NULL, 30300 );
			--		
			--
		--*****************************************************************	
		-- MOVED FROM QUARANTINE TO REWORK PO
		--*****************************************************************
		ELSIF (v_trans_type = 910)
		THEN
		-- 
			v_posting_type := 4825;
			--
			GM_SAVE_LEDGER_POSTING ( v_posting_type	,v_created_dt
													,v_part_number ,v_party_id
													,v_trans_id	,v_qty, NULL, 30300 );
			--	
		--*****************************************************************	
		-- MOVED FROM QUARANTINE TO REWORK PO
		--*****************************************************************
		ELSIF (v_trans_type = 4823)
		THEN
		-- 
			v_posting_type := 4823;
			--
			GM_SAVE_LEDGER_POSTING ( v_posting_type	,v_created_dt
													,v_part_number ,v_party_id
													,v_trans_id	,v_qty, NULL, 30300 );
			--	
		END IF;
	--
	END LOOP; -- for loop ends here
--
END GM_LOAD_OLD_POSTING; 
/

