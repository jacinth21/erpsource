CREATE OR REPLACE PROCEDURE qb_migrate_invoice_line
AS
	CURSOR qb_inv_line_cursor IS
		SELECT    qb_editseqid                   
				 ,qb_invoice_txnid               
				 ,qb_invoicelineitemreffullname
				 ,qb_invoicelineitemreflistid    
				 ,qb_invoicelineseqno            
				 ,qb_invoice_line_qty            
				 ,qb_invoice_line_amt
				 ,qb_customfieldinvoicelineother
				 ,qb_customfieldinvoicelinecontr
		FROM	 qb_invoiceline
		WHERE	 qb_invoicelineitemreffullname IS NOT NULL 
		AND		 qb_invoicelineitemreffullname NOT IN ('Sub Total')		 
		AND		qb_invoice_txnid    IN 
					(	SELECT  qb_invoice_txnid 
						FROM	qb_invoice
						WHERE	qb_txndate	<= TO_DATE('10/31/2004','MM/DD/YYYY') )  ; 
						--AND		qb_customerreflistid  IN ('440000-1057938754','470000-1057944890','460000-1057944603' )  
		--WHERE	qb_invoice_txnid  = '1E2F-1065559262'; -- '40AD4-1096493200';
	--
	v_gm_account_id			qb_invoiceline.qb_invoice_txnid%TYPE;
	v_c205_part_number_id	gm_qb_item.c205_part_number_id%TYPE;
	v_c502_control_number	t502_item_order.c502_control_number%TYPE;
	--
BEGIN
--
	FOR qb_inv_line_val IN qb_inv_line_cursor
	LOOP
		--
		v_gm_account_id			:= qb_get_invoicerefnumber(qb_inv_line_val.qb_invoice_txnid);
		v_c205_part_number_id 	:= qb_get_item(qb_inv_line_val.qb_invoicelineitemreflistid);
		--
		if (qb_inv_line_val.qb_customfieldinvoicelineother IS NOT NULL) 
		THEN
			v_c502_control_number := qb_inv_line_val.qb_customfieldinvoicelineother;
		ELSE
			v_c502_control_number := qb_inv_line_val.qb_customfieldinvoicelinecontr;
		END IF;
		--
		--
		--
		IF (v_c205_part_number_id IS NOT NULL) AND
				(v_c205_part_number_id != 'UPS' AND v_c205_part_number_id != 'Shipping' AND 
				 v_c205_part_number_id != 'Loaner Fee')
		THEN 
		--
			-- DBMS_OUTPUT.PUT_LINE('v_gm_account_id ' || v_gm_account_id);
			 -- DBMS_OUTPUT.PUT_LINE('v_c205_part_number_id ' || v_c205_part_number_id);
			INSERT INTO T502_ITEM_ORDER
			(    c502_item_order_id			, c501_order_id          
				,c205_part_number_id		,c502_item_qty          
				,c502_control_number		,c502_item_price        
				,c502_void_fl				,c502_delete_fl
				,c502_item_seq_no
			)
			VALUES
			(  s502_item_order.nextval	,'QB' || v_gm_account_id
			  ,v_c205_part_number_id	,qb_inv_line_val.qb_invoice_line_qty
			  ,v_c502_control_number	,qb_inv_line_val.qb_invoice_line_amt
			  ,NULL						,NULL	
			  ,qb_inv_line_val.qb_invoicelineseqno
			);
		--
		END IF;
		-- To update the shipping cost 
		IF (v_c205_part_number_id = 'UPS' OR v_c205_part_number_id = 'Shipping' OR 
				 v_c205_part_number_id = 'Loaner Fee')
		THEN
		--
			UPDATE	 T501_ORDER
			SET		 C501_SHIP_COST		= (C501_SHIP_COST + NVL(qb_inv_line_val.qb_invoice_line_amt,0))
					,C501_TOTAL_COST	= (C501_TOTAL_COST - NVL(qb_inv_line_val.qb_invoice_line_amt,0))
			WHERE	 C501_ORDER_ID		= 'QB' ||v_gm_account_id;
		--
		END IF;
	END LOOP;
	--
EXCEPTION WHEN OTHERS THEN 
--	
	 DBMS_OUTPUT.PUT_LINE('Account ID  -- ' || 'QB' || v_gm_account_id || 'Part Number  -- ' || v_c205_part_number_id );
     DBMS_OUTPUT.PUT_LINE(SQLERRM);
--	
END qb_migrate_invoice_line;
/

