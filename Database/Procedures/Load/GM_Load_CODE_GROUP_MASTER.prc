/* Formatted on 2010/11/10 10:28 (Formatter Plus v4.8.0) */
-- @C:\database\Procedures\Common\GM_CODE_GROUP_MASTER_RESET.prc;

CREATE OR REPLACE PROCEDURE gm_load_code_group_master
AS
   cd_master_cursor   TYPES.cursor_type;
   v_code_grp         t901b_code_group_master.c901b_code_grp%TYPE;
   v_total            NUMBER;
BEGIN
   -- Loading all the Code Group from C901_CODE_LOOKUP, which are not present in C901B_CODE_GROUP_MASTER
   OPEN cd_master_cursor
    FOR
       SELECT DISTINCT c901.c901_code_grp,
                       (SELECT COUNT (*)
                          FROM t901_code_lookup
                         WHERE c901_code_grp = c901.c901_code_grp) total
                  FROM t901_code_lookup c901
                 WHERE c901.c901_code_grp NOT IN (
                                                 SELECT c901b_code_grp
                                                   FROM t901b_code_group_master);

   LOOP
      FETCH cd_master_cursor
       INTO v_code_grp, v_total;

      EXIT WHEN cd_master_cursor%NOTFOUND;

      -- Insert into T901B_CODE_GROUP_MASTER from T901_CODE_LOOKUP
      INSERT INTO t901b_code_group_master
                  (c901b_code_grp_id, c901b_code_grp, c901b_lookup_needed
                  )
           VALUES (s901b_code_group_master.NEXTVAL, v_code_grp, v_total
                  );
   END LOOP;

   CLOSE cd_master_cursor;
END gm_load_code_group_master;
/
