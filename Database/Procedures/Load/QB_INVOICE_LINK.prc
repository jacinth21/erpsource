CREATE OR REPLACE PROCEDURE qb_invoice_link
AS
	CURSOR qb_cursor IS
		SELECT   b.qb_txndate				deposit_dt
				,b.qa_refnumber				check_no
				,b.qb_appliedtotxnrefnumb   invoice_no 
				,b.qb_appliedtotxndate		invoice_dt
				,a.qb_ponumber				cust_po
				,NVL(a.qb_terms_reffullname,'Net 30') net_p_info
				,b.qb_appliedtotxn_amount chk_amount
		FROM	 qb_invoicelink a
				,qb_receive_payment b 
		where 	 a.qb_refnumber (+) =  b.qb_appliedtotxnrefnumb 
		AND		 b.qb_appliedtotxnrefnumb is NOT NULL 
		AND		 nvl(a.qb_linkedtxntype,'x') NOT LIKE '%C'; 	 
	v_qa_order_count		NUMBER(2);
	v_qa_invoice_count		NUMBER(2);
	v_invoice_count			NUMBER(2);
	v_rec_payment_count		NUMBER(2);
	v_temp					VARCHAR2(20);
	v_qb_refnumber			qb_invoicelink.qb_refnumber%TYPE;
	v_qb_order				t501_order.c501_order_id%TYPE;
	v_account_id			t501_order.c704_account_id%TYPE;
	v_terms_name			qb_invoicelink.qb_terms_reffullname%TYPE;
	v_term_id				t503_invoice.c503_terms%TYPE;
BEGIN
--
FOR qb_val IN qb_cursor
LOOP
	v_qb_refnumber			:= qb_val.invoice_no;
	v_qb_order				:= 'QB' ||qb_val.invoice_no ;
	--
	-- Below code is used to check of the order is loaded from Quick books
	-- If loaded from Quick Books then load the invoice record and load the payment information
	--
	SELECT	COUNT(1)
	INTO	v_qa_order_count
	FROM	t501_order
	WHERE	c501_order_id	= v_qb_order;
	--
	--
	if ( v_qa_order_count = 1)
	THEN
		--
		-- Code to check if the invoice record is created 
		-- if created dont insert and only do an update for the payment
		--
		SELECT	COUNT(1)
		INTO	v_qa_invoice_count
		FROM	t503_invoice
		WHERE	c503_invoice_id	= v_qb_order;
		--
		-- if invoice record NOT created then create the invoice record before posting the payment 
		-- DBMS_OUTPUT.PUT_LINE('Invoice #' || v_qb_refnumber  || ' Order #'|| v_qb_order || ' - Count ' || v_qa_invoice_count );
		IF (v_qa_invoice_count = 0) THEN
			--
			-- 
			SELECT	c704_account_id
			INTO	v_account_id
			FROM	t501_order
			WHERE	c501_order_id	= v_qb_order;
			--
			v_terms_name := qb_val.net_p_info;
			--
			--
			v_term_id := 4100;
			--
			IF (v_terms_name <> 'Net 30' ) THEN 
			--
				v_term_id := 4101;
			--
			END IF;
			--
			SELECT	COUNT(1)
			INTO	v_qa_invoice_count
			FROM	t503_invoice
			WHERE	c704_account_id		= v_account_id
			AND		c503_customer_po	= qb_val.cust_po;
			--
			IF (v_qa_invoice_count = 0) THEN
			--
				INSERT INTO T503_INVOICE (
						  c503_invoice_id
						, c704_account_id
						, c503_invoice_date
						, c503_terms
						, c503_payment_received_details
						, c503_customer_po
						, c503_created_date
						, c503_created_by
				) VALUES (  v_qb_order 
						, v_account_id
						, qb_val.invoice_dt 
						, v_term_id
						, 'Record loaded from Quickbooks' 
						, qb_val.cust_po 
						, SYSDATE
						, 30300
				);
			--
			ELSE
			-- get the invoice number 
				SELECT	c503_invoice_id
				INTO	v_qb_order
				FROM	t503_invoice
				WHERE	c704_account_id		= v_account_id
				AND		c503_customer_po	= qb_val.cust_po;
			--
			END IF;
		END IF;
		--
		-- Insert record in receivables table 
		--
		-- Check if the record is already inserted 
		-- means duplicated invoiice for different customer
		-- If duplicate then skip the entry 
		SELECT	COUNT(1)
		INTO	v_rec_payment_count
		FROM	t508_receivables
		WHERE	c503_invoice_id		= 	v_qb_refnumber
		AND		c508_payment_amt	=	qb_val.chk_amount;
		--
		IF (v_rec_payment_count = 0) THEN
		--
			INSERT INTO t508_receivables (
				c508_receivables_id
			   ,c503_invoice_id
			   ,c508_received_date
			   ,c508_received_mode
			   ,c508_payment_amt
			   ,c508_details
			) VALUES ( 
				c508_receivables.nextval
			   ,v_qb_order
			   ,qb_val.deposit_dt
			   ,5010
			   ,qb_val.chk_amount 
			   ,qb_val.check_no || ' - Loaded from Quickbooks ' );
		--
		END IF;
	ELSE
	-- 
		v_temp := SUBSTR(v_qb_refnumber,length(v_qb_refnumber) -1, 2) ;
		IF (v_temp = '-D' ) THEN
		   v_qb_refnumber := SUBSTR(v_qb_refnumber,1, length(v_qb_refnumber) -2) ;
		END IF;
		-- If data not loaded from QB Books check if the invoice
		-- is already generated in the system 
		-- 
		SELECT	COUNT(1)
		INTO	v_invoice_count
		FROM	t503_invoice 
		WHERE	c503_invoice_id = v_qb_refnumber;
		--
		if ( v_invoice_count <> 0)
		THEN
			--
			-- Insert record in receivables table 
			--
			SELECT	COUNT(1)
			INTO	v_rec_payment_count
			FROM	t508_receivables
			WHERE	c503_invoice_id		= 	v_qb_refnumber
			AND		( c508_payment_amt	=	qb_val.chk_amount
			OR		  c508_details NOT LIKE '%Quickbooks%' ) 	  	;
			--
			IF (v_rec_payment_count = 0) THEN
			--
				INSERT INTO t508_receivables (
					c508_receivables_id
				   ,c503_invoice_id
				   ,c508_received_date
				   ,c508_received_mode
				   ,c508_payment_amt
				   ,c508_details
				) VALUES ( 
					c508_receivables.nextval
				   ,v_qb_refnumber
				   ,qb_val.deposit_dt
				   ,5010
				   ,qb_val.chk_amount 
				   ,qb_val.check_no || ' - Loaded from Quickbooks ');			
			--
			END IF;
		--
		ELSE
			gm_procedure_log('Invoice not found date -- ' || qb_val.deposit_dt ,  v_qb_refnumber);
		END IF;
	--
	END IF;
END LOOP;
	--
EXCEPTION WHEN OTHERS THEN
--
	 DBMS_OUTPUT.PUT_LINE('Invoice #' || v_qb_refnumber  || ' Order #'|| v_qb_order);
     DBMS_OUTPUT.PUT_LINE(SQLERRM);
--
END qb_invoice_link;
/

