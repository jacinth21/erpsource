CREATE OR REPLACE PROCEDURE GM_LOAD_CONSIGNMENT_MANUAL
(
  p_consignment_id  IN  t504_consignment.c504_consignment_id%TYPE
 ,p_inhouse_purpose IN t504_consignment.c504_inhouse_purpose%TYPE
 ,p_set_id   IN t504_consignment.c207_set_id%TYPE
 ,p_etch_id   IN t504a_consignment_loaner.c504a_etch_id%TYPE
)
AS
--
  v_company_id  t1900_company.c1900_company_id%TYPE;
  v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
 BEGIN
	   
   SELECT NVL(GET_COMPID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL')), NVL(GET_PLANTID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_PLANT','ONEPORTAL'))
     INTO v_company_id, v_plant_id
     FROM DUAL;
--
   INSERT INTO T504_CONSIGNMENT
   (  c504_consignment_id
  ,c504_comments
  ,c504_created_date
  ,c504_created_by
  ,c504_status_fl
  ,c704_account_id
  ,c504_inhouse_purpose
  ,c504_update_inv_fl
  ,c504_type
  ,c504_verify_fl
  ,c504_verified_date
  ,c504_verified_by
  ,c207_set_id
  ,c1900_company_id
  ,c5040_plant_id
 ) VALUES
 (  p_consignment_id
  ,'Manual Load after cleaning the inhouse consignment'
  ,CURRENT_DATE
  ,30301
  ,4
  ,'01'
  ,p_inhouse_purpose
  ,1
  ,4119
  ,1
  ,CURRENT_DATE
  ,30301
  ,p_set_id
  ,v_company_id
  ,v_plant_id
 );
 --
 -- To load loaner information
 INSERT INTO  T504A_CONSIGNMENT_LOANER
  (  c504_consignment_id
  ,c504a_etch_id
  ,c504a_status_fl
  ,c504a_loaner_create_by
  ,c504a_loaner_create_date
  ,c5040_plant_id
 )VALUES
 (   p_consignment_id
  ,p_etch_id
  ,0
  ,30301
  ,CURRENT_DATE
  ,v_plant_id
 );
--
END GM_LOAD_CONSIGNMENT_MANUAL;
/

