/* Formatted on 2010/01/27 09:50 (Formatter Plus v4.8.0) */

 /*******************************************************
   * Description : Procedure used to execute all the required
   * load functionality, If will executed at 1.00  AM
   *******************************************************/
--@"C:\Database\Procedures\Load\GM_OP_LD_ORDERPLANNING_MAIN.prc" ;

CREATE OR REPLACE PROCEDURE gm_op_ld_orderplanning_main
AS
--
	CURSOR dmaster_cursor
	IS
		SELECT c4020_demand_master_id
		  FROM t4020_demand_master t4020
		 WHERE t4020.c4020_void_fl IS NULL AND t4020.c4020_inactive_fl IS NULL
		 AND  t4020.c901_company_id IS NULL ;

	--
	v_demandsheetid VARCHAR2 (20);
	v_start_time   DATE;
	v_count 	   NUMBER;
	v_plant_id     T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
--
BEGIN
--
	SELECT  get_rule_value('DEFAULT_PLANT','ONEPORTAL') INTO v_plant_id FROM DUAL;
	v_start_time := CURRENT_DATE;
	gm_pkg_op_ld_no_stack.gm_op_nostack_main;
	gm_pkg_op_ld_inventory.gm_op_ld_demand_main (20430);

	--
	FOR dmaster_val IN dmaster_cursor
	LOOP
		--
		v_demandsheetid := dmaster_val.c4020_demand_master_id;
		gm_pkg_op_ld_demand.gm_op_ld_demand_main (v_demandsheetid);
	--
	END LOOP;

	--
	--
	-- Making order qty  to zero  (If not ordered)
	UPDATE t205c_part_qty x
	   SET x.c205_available_qty = 0
		 , c205_last_updated_by = 303043
		 , c901_action = 4302
		 , c901_type = 4316
		 , c205_last_updated_date = CURRENT_DATE
		,  c205_last_update_trans_id = 'OP Load Clean' 
	 WHERE x.c205_available_qty > 0 AND x.c901_transaction_type = 90801 AND x.C5040_PLANT_ID = v_plant_id;

	--
	--
	-- Remove all instock -ive qty (system should not have -ive qty)
	-- Need to work on the same
	UPDATE t251_inventory_lock_detail t251
	   SET t251.c251_qty = 0
	 WHERE t251.c251_qty < 0
	   AND t251.c250_inventory_lock_id IN (SELECT MAX (t250.c250_inventory_lock_id)
											 FROM t250_inventory_lock t250
											WHERE t250.c901_lock_type = 20430 AND t250.c250_void_fl IS NULL);

	COMMIT;

	--
	SELECT COUNT (1)
	  INTO v_count
	  FROM t4040_demand_sheet t4040
	 WHERE t4040.c4040_demand_period_dt = TO_DATE ('01/' || TO_CHAR (CURRENT_DATE, 'MM/YYYY'), 'DD/MM/YYYY');

	gm_pkg_cm_job.gm_cm_notify_load_info (v_start_time
										, CURRENT_DATE
										, 'S'
										, 'SUCCESS'
										, v_count || ' Demand Sheet Data Created *** '
										 );
	--
	COMMIT;
EXCEPTION
	WHEN OTHERS
	THEN
		ROLLBACK;
		-- Final log insert statement (Error Message)
		gm_pkg_cm_job.gm_cm_notify_load_info (v_start_time, CURRENT_DATE, 'E', SQLERRM, 'gm_op_ld_orderplanning_main');
		COMMIT;
END gm_op_ld_orderplanning_main;
/
