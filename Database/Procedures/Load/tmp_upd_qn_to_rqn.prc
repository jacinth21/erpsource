/* Formatted on 2011/02/16 14:55 (Formatter Plus v4.8.0) */


CREATE OR REPLACE PROCEDURE tmp_upd_qn_to_rqn
AS
   CURSOR curr_records
   IS
      SELECT c504_consignment_id cnid
        FROM t504_consignment
       WHERE c504_type = 4113
         AND c504_status_fl < 4
         AND c504_reprocess_id IS NOT NULL
         AND c504_void_fl IS NULL;

   v_action        VARCHAR2 (100);
   v_type          VARCHAR2 (100);

   TYPE v_pnum_array IS TABLE OF t505_item_consignment.c205_part_number_id%TYPE;

   TYPE v_pnumqty_array IS TABLE OF t505_item_consignment.c505_item_qty%TYPE;

   pnum_array      v_pnum_array;
   pnumqty_array   v_pnumqty_array;
BEGIN
   SELECT get_rule_value ('INTP_GRP', '400064')
     INTO v_type
     FROM DUAL;

   FOR currindex IN curr_records
   LOOP
      DBMS_OUTPUT.put_line ('consignment id ' || currindex.cnid);

      UPDATE t504_consignment
         SET c504_type = 400064
           , c504_last_updated_by = 303341
		   , c504_last_updated_date = SYSDATE
       WHERE c504_consignment_id = currindex.cnid;

      SELECT c205_part_number_id, c505_item_qty
      BULK COLLECT INTO pnum_array, pnumqty_array
        FROM t505_item_consignment
       WHERE c504_consignment_id = currindex.cnid AND c505_void_fl IS NULL;

-- SELECT DECODE(ruleid_array (i),'PLUS','4301','MINUS','4302') INTO v_action FROM DUAL;
      FOR i IN pnum_array.FIRST .. pnum_array.LAST
      LOOP
         DBMS_OUTPUT.put_line ('Partnumber id ' || pnum_array (i));

         --reduce qty in stock
         UPDATE t205_part_number
            SET c205_qty_in_returns_hold =
                          NVL (c205_qty_in_returns_hold, 0)
                          + pnumqty_array (i),
                c901_action = 4302                                     --minus
                                  ,
                c901_type = v_type,
                c205_last_update_trans_id = currindex.cnid,
                c205_last_updated_by = 303341,
                c205_last_updated_date = SYSDATE
          WHERE c205_part_number_id = pnum_array (i);

         -- increase returns in hold
         UPDATE t205_part_number
            SET c205_qty_in_returns_hold =
                          NVL (c205_qty_in_returns_hold, 0)
                          + pnumqty_array (i),
                c901_action = 4301                                      --plus
                                  ,
                c901_type = v_type,
                c205_last_update_trans_id = currindex.cnid,
                c205_last_updated_by = 303341,
                c205_last_updated_date = SYSDATE
          WHERE c205_part_number_id = pnum_array (i);
      END LOOP;
   END LOOP;
END tmp_upd_qn_to_rqn;
/