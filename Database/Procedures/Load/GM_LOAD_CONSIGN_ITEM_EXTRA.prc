CREATE OR REPLACE PROCEDURE GM_LOAD_CONSIGN_ITEM_EXTRA
AS
--
 CURSOR setextra_cursor IS
  SELECT   c207_set_id
    ,c205_part_number_id
    ,temp_etch_id
    ,temp_remove_qty
  FROM   temp_set_extra_parts;
 --
 v_set_id   t504_consignment.c207_set_id%TYPE;
 v_etch_id   t504a_consignment_loaner.c504a_etch_id%TYPE;
 v_consignment_id t504_consignment.c504_consignment_id%TYPE;
 --
BEGIN
--
 --
 FOR setextra_val IN setextra_cursor
 LOOP
 --
  v_set_id  := setextra_val.c207_set_id;
  v_etch_id := setextra_val.temp_etch_id ;
  --
  -- To get the consignment id
  BEGIN
   SELECT   t504.c504_consignment_id
   INTO  v_consignment_id
   FROM   t504_consignment t504
     ,t504a_consignment_loaner t504a
   WHERE   t504.c504_consignment_id  = t504a.c504_consignment_id
   AND   t504.c207_set_id   = setextra_val.c207_set_id
   AND   t504a.c504a_etch_id  = setextra_val.temp_etch_id 
   AND	 t504.c504_void_fl IS NULL;
   --
   --
   UPDATE   t505_item_consignment
   SET   c505_item_qty    = c505_item_qty  + setextra_val.temp_remove_qty
   WHERE   c504_consignment_id = v_consignment_id
   AND   c205_part_number_id = setextra_val.c205_part_number_id ;

   IF ( SQL%ROWCOUNT = 0)
   THEN
    DBMS_OUTPUT.put_line ('Update Failed Pl validate #' || v_set_id || ' - ' ||   v_etch_id  );
   END IF ;

  EXCEPTION
  WHEN OTHERS
     THEN
      DBMS_OUTPUT.put_line ('Pricing Error @ Part #' || v_set_id || ' - ' ||   v_etch_id  );
  END ;
 --
 END LOOP;
--
--
END GM_LOAD_CONSIGN_ITEM_EXTRA;
/

