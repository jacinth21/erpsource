 CREATE OR REPLACE PROCEDURE  gm_ind_new_account_alert_temp 
 AS
    v_to_mail        VARCHAR2 (1000);
    v_subject        VARCHAR2 (1000);
    v_mail_body      VARCHAR2 (2000);
    v_ac_count       NUMBER;
    v_corp_pri_count NUMBER;
BEGIN
    --This is a temporary procedure for sending alter notification if any account created without currency or if any order placed with null corp price
    SELECT COUNT(1) INTO v_ac_count
    FROM t704_account
    WHERE c901_currency    IS NULL
    AND c704_void_fl       IS NULL
    AND nvl(c901_ext_country_id,-999)<>1140; --Exclude Israel account
    
    SELECT COUNT(1) INTO v_corp_pri_count
    FROM t501_order T501
    WHERE EXISTS
        ( SELECT T502.c501_order_id
        FROM t502_item_order T502
        WHERE T501.c501_order_id=t502.c501_order_id
        AND T502.c502_corp_item_price IS NULL
        )
    AND T501.c501_void_fl               IS NULL;
    
        IF (v_ac_count<>0 OR v_corp_pri_count<>0)
        THEN
        v_to_mail                  := get_rule_value ('SALES_ETL_FAILURE', 'DATA_LOAD_NOTIF');
        v_subject                  := 'Account created without currency or Corp price is null';
        v_mail_body                :='Either account created without currency OR Corporate price has null for Order';
        gm_com_send_email_prc (v_to_mail, v_subject, v_mail_body);
    END IF;
   END gm_ind_new_account_alert_temp;
/