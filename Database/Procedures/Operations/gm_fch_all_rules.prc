
--@"C:\Database\Procedures\Operations\gm_fch_all_rules.prc"

create or replace PROCEDURE gm_fch_all_rules(
	    p_rule_grp	           IN    VARCHAR2
	    ,p_out_rules	   OUT 	 TYPES.cursor_type
	    )
AS
	v_rule_id	   t906_rules.c906_rule_id%TYPE;
    
BEGIN
    --
    
       
    SELECT C906_RULE_VALUE
		 INTO v_rule_id
         FROM t906_rules
   WHERE c906_rule_id = 'CURRENT_DB' ;

   -- get comma delimated values to store in v_in_list
	  my_context.set_my_inlist_ctx (p_rule_grp) ;

    OPEN p_out_rules FOR 
	   SELECT c906_rule_id rule_id,
	      c906_rule_desc rule_desc ,
	      c906_rule_value rule_value,
	      c906_rule_grp_id rule_group_id,
	      t901.c902_code_nm_alt rule_type,
        c906_rule_seq_id seq
	      FROM t906_rules, t901_code_lookup t901
	     WHERE c906_rule_grp_id = to_char(c901_code_id)
               AND c901_code_grp IN 
	         ('EXPRUL','UDISRC','EXPFMT')
	       AND c901_void_fl IS NULL
	       AND c901_active_fl = 1
	       AND c906_void_fl IS NULL 
	       AND c906_active_fl='Y' 
	     
		UNION ALL
		--Add Union here to check if the country is US or not when log in.
		SELECT c906_rule_id rule_id,
	      c906_rule_desc rule_desc ,
	      c906_rule_value rule_value,
	      c906_rule_grp_id rule_group_id,
	      'COUNTRY' rule_type,
       	      c906_rule_seq_id seq
         	FROM t906_rules
   		WHERE c906_rule_id = v_rule_id
   		 AND  c906_rule_grp_id = 'LBLCHK';

END gm_fch_all_rules;
/