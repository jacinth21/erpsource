/* Formatted on 2007/08/21 16:45 (Formatter Plus v4.8.0) */

--GM_INITIATE_ITEM_RETURN
--GM_SAVE_RETURN_SET_BUILD

CREATE OR REPLACE PROCEDURE gm_op_sav_order_return (
	p_type			   IN		t506_returns.c506_type%TYPE
  , p_reason		   IN		t506_returns.c506_reason%TYPE
  , p_distid		   IN		VARCHAR2
  , p_comments		   IN		t506_returns.c506_comments%TYPE
  , p_expdate		   IN		VARCHAR2
  , p_orderid		   IN		t506_returns.c501_order_id%TYPE
  , p_userid		   IN		t506_returns.c506_created_by%TYPE
  , p_parent_orderid   IN		t506_returns.c501_order_id%TYPE
  , p_empname		   IN		t101_user.c101_user_id%TYPE
  , p_str			   IN		VARCHAR2
  , p_raid			   OUT		VARCHAR2
  , p_message		   OUT		VARCHAR2
)
AS
--
	v_ra_id 	   NUMBER;
	v_id		   NUMBER;
	v_string	   VARCHAR2 (30000);
	v_id_string    VARCHAR2 (20);
	v_price 	   VARCHAR2 (20);
	v_date		   VARCHAR2 (10);
	v_empname	   NUMBER;
	v_status	   CHAR (1) := 0;
	v_ret_date	   DATE;
	v_rep_date	   DATE;
	-- Loop variable declaration
	v_strlen	   NUMBER := 0;
	v_substring    VARCHAR2 (1000);
	v_pnum		   VARCHAR2 (20);
	v_qty		   NUMBER;
	v_control	   t507_returns_item.c507_control_number%TYPE;
	v_orgcontrol   t507_returns_item.c507_org_control_number%TYPE;
	v_itemtype	   NUMBER;
	v_order_id	   t506_returns.c501_order_id%TYPE;
	v_company_id  t1900_company.c1900_company_id%TYPE;
    v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
    
   BEGIN
	   
      SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
        INTO v_company_id, v_plant_id
        FROM DUAL;
	--
	SELECT 'GM-RA-' || s506_return.NEXTVAL
	  INTO p_raid
	  FROM DUAL;

	--
	IF LENGTH (p_expdate) < 1
	THEN
		v_date		:= CURRENT_DATE + 4;
	ELSE
		v_date		:= p_expdate;
	END IF;

	--
	-- Mark the order as duplicate order or as Item Return consignment
	gm_order_return (p_orderid, p_type, p_reason, p_parent_orderid);

	--
	-- 3317 means Parts Swap (Loaner to Consign)
	-- used by loaner program
	IF p_reason = '3317'
	THEN
		v_status	:= 1;
		v_ret_date	:= CURRENT_DATE;
		v_rep_date	:= CURRENT_DATE;
	END IF;

	--
	INSERT INTO t506_returns
				(c506_rma_id, c704_account_id, c501_order_id, c506_type, c506_reason, c506_comments
			   , c506_expected_date, c506_status_fl, c506_created_by, c506_created_date, c506_return_date
			   , c501_reprocess_date, c1900_company_id, c5040_plant_id
				)
		 VALUES (p_raid, p_distid, p_orderid, p_type, p_reason, p_comments
			   , v_date, v_status, p_userid, CURRENT_DATE, v_ret_date
			   , v_rep_date, v_company_id, v_plant_id
				);

	--
	-- Load item information
	v_string	:= p_str;
	v_strlen	:= NVL (LENGTH (p_str), 0);

	--
	WHILE INSTR (v_string, '|') <> 0
	LOOP
		--
		v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
		v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
		v_pnum		:= NULL;
		v_qty		:= NULL;
		v_control	:= NULL;
		v_orgcontrol := NULL;
		v_itemtype	:= NULL;
		v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
		v_qty		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
		v_control	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
		v_orgcontrol := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
		v_itemtype	:= TO_NUMBER (v_substring);

		--
		SELECT c502_item_price
		  INTO v_price
		  FROM t502_item_order
		 WHERE c501_order_id = p_parent_orderid AND c205_part_number_id = v_pnum AND c502_control_number = v_orgcontrol AND C502_VOID_FL IS NULL;

		--
		INSERT INTO t507_returns_item
					(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_control_number, c507_item_qty
				   , c507_item_price, c507_status_fl, c507_org_control_number, c901_type
					)
			 VALUES (s507_return_item.NEXTVAL, p_raid, v_pnum, v_control, v_qty
				   , v_price, 'R', NVL (v_orgcontrol, '-'), v_itemtype
					);
	END LOOP;

	INSERT INTO t507_returns_item
				(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_item_qty, c507_item_price, c507_status_fl)
		SELECT s507_return_item.NEXTVAL, v_ra_id, c205_part_number_id, item_qty, c507_item_price, 'Q'
		  FROM (SELECT	 c205_part_number_id, c507_item_price, SUM (c507_item_qty) item_qty
					FROM t507_returns_item
				   WHERE c506_rma_id = p_raid AND c507_status_fl = 'R'
				GROUP BY c205_part_number_id, c507_item_price);
--
END gm_op_sav_order_return;
/
