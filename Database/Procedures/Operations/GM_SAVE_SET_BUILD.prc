/* Formatted on 2011/06/10 11:07 (Formatter Plus v4.8.0) */
-- @"c:\database\procedures\operations\gm_save_set_build.prc"

/* Description: This procedure is used to update the consignment 
 * status when it is beign completed or verified, while completing, 
 * the status of the consignment will be updated to "Pending
 * Verification" ( Consignment Status Fl =2 and Verify Fl = 0 )
 * and while verifying the following gets updated
 * 
 * 1. Consignment Status = 2 & Verify Fl = 1 ( Ready to Consign )
 * 
 * 2. if the consignment has the details of whom it is getting 
 *    shipped to then Consignment Status will be updated to 
 *    Ready to Ship ( Consignment Status = 3 & Verify Fl = 1 ).
 *     
 * 3. if the consignment has the details of whom it is getting
 *    shipped to then a shipping record gets created / updated
 *    to pending shipping status.
 * 
 * 4. Inventory Fl gets updated saying the parts are taken out
 *    from inventory and Bulk Qty in part number table gets reduced
 * 
 * 5. Posting happens for Built Set ( 4812 ) 
 */
CREATE OR REPLACE PROCEDURE gm_save_set_build (
	p_consignid   IN	   t505_item_consignment.c504_consignment_id%TYPE
  , p_str		  IN	   VARCHAR2
  , p_comm		  IN	   t504_consignment.c504_comments%TYPE
  , p_flag		  IN	   t504_consignment.c504_status_fl%TYPE
  , p_verflag	  IN	   t504_consignment.c504_verify_fl%TYPE
  , p_userid	  IN	   t504_consignment.c504_last_updated_by%TYPE
  , p_errmsg	  OUT	   VARCHAR2
  , p_source	  IN	   VARCHAR2 DEFAULT 'SCREEN' 
)
AS
	v_strlen	   NUMBER := NVL(LENGTH(p_str), 0);
	v_string	   VARCHAR2(30000) := p_str;
	v_pnum		   VARCHAR2(20);
	v_qty		   NUMBER;
	v_control	   t5010_tag.c5010_control_number%TYPE;
	v_substring    VARCHAR2(1000);
	v_msg		   VARCHAR2(1000);
	e_others	   EXCEPTION;
	v_id		   NUMBER;
	v_price 	   VARCHAR2(20);
	v_verify_by    VARCHAR2(20);
	v_verify_date  DATE;
	p_invfl 	   VARCHAR2(10);
	v_void_fl	   VARCHAR2(10);
	v_out_consign_id t505_item_consignment.c504_consignment_id%TYPE;
	v_shipto	   t504_consignment.c504_ship_to_id%TYPE;
	v_mrid		   t504_consignment.c520_request_id%TYPE;
	v_flag		   t504_consignment.c504_status_fl%TYPE;
	v_cnt		   NUMBER := 0;
	v_isset 	   VARCHAR2(10);
	v_cnt_log	   NUMBER := 0;
	v_status_type  VARCHAR2(1000);
	v_cons_type    t504_consignment.c504_type%TYPE;
	v_inhouse_purpose t504_consignment.c504_inhouse_purpose%TYPE;
	v_tag_validation_fl BOOLEAN;
	v_wip_count    NUMBER := 0;
	v_type		   t504_consignment.c504_type%TYPE;
	v_plan_ship_date   T520_REQUEST.C520_PLANNED_SHIP_DATE%TYPE;
	v_type_id      t504_consignment.c504_type%TYPE;
	v_tagid	       t5010_tag.c5010_tag_id%TYPE;
	v_invtype	t5010_tag.c901_inventory_type%TYPE;
	v_status_fl     VARCHAR2(10);
	v_country 		VARCHAR2(10);
	v_req_status_fl     VARCHAR2(10);	
	v_intransid    t504e_consignment_in_trans.c504_consignment_id%TYPE;
	v_party_id     VARCHAR2(20);
	v_owner_company_id t504_consignment.c1900_company_id%TYPE;
	v_post_id	   VARCHAR2 (20);
	v_set_type		   T207_SET_MASTER.c207_type%TYPE;
	
    -- Fetch the consignment ID values for do the local cost posting(In-Trasnit) 
	CURSOR cur_consignment
		IS
	       SELECT c205_part_number_id ID, SUM (c505_item_qty) qty
		         ,c505_item_price price 
			FROM t505_item_consignment
		   WHERE c504_consignment_id = p_consignid AND c505_control_number IS NOT NULL
		GROUP BY c205_part_number_id,c505_item_price;
BEGIN
--
   -- Validate if the Consignment is already voided
   
   	
	SELECT t504.c504_void_fl, t504.c504_type, NVL(t504.c504_inhouse_purpose, '-9999')
	     , t504e.c504_consignment_id , t504e.c1900_source_company_id
	  INTO v_void_fl, v_cons_type, v_inhouse_purpose, v_intransid , v_owner_company_id
	  FROM t504_consignment t504,t504e_consignment_in_trans t504e
	 WHERE t504.c504_consignment_id = p_consignid
	   AND t504.c504_reprocess_id = t504e.c504_consignment_id(+)
       AND t504.c504_void_fl IS NULL
       AND t504e.c504e_void_fl IS NULL
	FOR UPDATE;

	IF v_void_fl IS NOT NULL
	THEN
		-- Error message is Consignment already Voided.
		raise_application_error(-20770, '');
	END IF;

	IF v_strlen > 0 OR p_source = 'SETBUILD'
	THEN
		IF p_verflag <> '1'
		THEN
			UPDATE t504_consignment
			   SET c504_comments = NVL(p_comm,c504_comments)
				 , c504_status_fl = p_flag
				 , c504_last_updated_date = SYSDATE
				 , c504_last_updated_by = p_userid
			 WHERE c504_consignment_id = p_consignid;
		END IF;

		IF p_source != 'SETBUILD' THEN
			DELETE t505_item_consignment
		     WHERE c504_consignment_id = p_consignid;
		END IF;
		
		WHILE INSTR(v_string, '|') <> 0
		LOOP
			v_substring := SUBSTR(v_string, 1, INSTR(v_string, '|') - 1);
			v_string	:= SUBSTR(v_string, INSTR(v_string, '|') + 1);
			v_pnum		:= NULL;
			v_qty		:= NULL;
			v_control	:= NULL;
			v_pnum		:= SUBSTR(v_substring, 1, INSTR(v_substring, '^') - 1);
			v_substring := SUBSTR(v_substring, INSTR(v_substring, '^') + 1);
			v_qty		:= SUBSTR(v_substring, 1, INSTR(v_substring, '^') - 1);
			v_substring := SUBSTR(v_substring, INSTR(v_substring, '^') + 1);
			v_control	:= SUBSTR(v_substring, 1, INSTR(v_substring, '^') - 1);
			v_substring := SUBSTR(v_substring, INSTR(v_substring, '^') + 1);
			v_type_id	:= v_substring;

			--
			SELECT get_part_attribute_value(v_pnum, 92340)
			  INTO v_isset
			  FROM DUAL;

			IF v_isset = 'Y'
			THEN
			
				SELECT COUNT(1)
				  INTO v_cnt
				  FROM t5010_tag t5010
				 WHERE t5010.c205_part_number_id = v_pnum
				   AND t5010.c5010_last_updated_trans_id = p_consignid
				   AND t5010.c5010_control_number = v_control
				   AND t5010.c5010_void_fl IS NULL;

				IF v_cons_type != 4112	-- In House Consignment
				            OR (v_cons_type = 4112	-- In House Consignment
					       AND (v_inhouse_purpose = 4130   -- Training Set
							OR v_inhouse_purpose = 4132   -- Display Set
							OR v_inhouse_purpose = 4131   -- Product Room
							OR v_inhouse_purpose = 4138   -- Ext Cad lab
							OR v_inhouse_purpose = 4133   -- Int Cad lab
							OR v_inhouse_purpose = 4141
						   )
					  )   -- Photography
				THEN
					v_tag_validation_fl := TRUE;
				ELSE
					v_tag_validation_fl := FALSE;
				END IF;
	 		   
	 
				IF v_cnt = 0 AND p_verflag = '1' AND v_tag_validation_fl = TRUE
				THEN
				 
					-- Error message is TagID cannot be blank, please enter TagID.
					raise_application_error(-20912, '');
				END IF;
				
				-- This procedure will check if the control number in db and passed in control number is differnt,
				-- If it is, the control number will be update in tag table.
				 
			 gm_pkg_op_tag_trans.gm_sav_tag_control_num(v_pnum,p_consignid,v_control,p_userid);
			 
			END IF;

			SELECT s504_consign_item.NEXTVAL
			  INTO v_id
			  FROM DUAL;

			IF (p_verflag = '1' AND v_isset = 'Y')	 --verified
			THEN
				UPDATE t5010_tag t5010
				   SET t5010.c901_location_type = 40033   -- In-house
					 , t5010.c5010_location_id = 52098	 -- set building
					 , t5010.c5010_last_updated_by = p_userid
					 , t5010.c5010_last_updated_date = SYSDATE
				 WHERE t5010.c5010_last_updated_trans_id = p_consignid
				   AND t5010.c205_part_number_id = v_pnum
				   AND t5010.c5010_control_number = v_control;
				   
				--get tag id for update inventory type.
				BEGIN
					SELECT t5010.c5010_tag_id 
					  INTO v_tagid
					  FROM t5010_tag t5010
					 WHERE t5010.c5010_last_updated_trans_id = p_consignid
					   AND t5010.c205_part_number_id = v_pnum
					   AND t5010.c5010_control_number = v_control
					   AND t5010.c5010_void_fl IS NULL;
				EXCEPTION WHEN NO_DATA_FOUND THEN
					v_tagid := NULL;
				END;

			END IF;
			
			IF v_intransid IS NULL -- Call the exist flow. Part Price consider as Equity price.
			THEN	    
					--
					v_price 	:= get_part_price(v_pnum, 'E');
					--
					gm_pkg_op_request_master.gm_sav_consignment_detail(v_id
																	  , p_consignid																	 
																	  , v_pnum
																	  , v_control
																	  , v_qty
																	  , v_price
																	  , NULL
																	  , NULL
																	  , NULL
																	  , v_out_consign_id
																	  );
			ELSE
			
			       gm_pkg_in_transit_trans.gm_sav_consignment_detail(v_id
																	  , p_consignid
																	  , v_intransid
																	  , v_pnum
																	  , v_control
																	  , v_qty																	   
																	  , NULL
																	  , NULL
																	  , NULL
																	  , v_out_consign_id
																	  );													   
			END IF;												   
															   
		END LOOP;

		--4072	In-House, 4073	Training, 4074	Loaner, 4077  Bone Model, 4078	Demo, 4127   Product Loaner, 4119  In house Loaner, 4070 Consignment, 
		IF p_verflag = '1'
		THEN
			    SELECT c504_update_inv_fl, c504_ship_to, c520_request_id, DECODE(get_master_set_type(c207_set_id), 4077, 4119, 4072,4119, 4078,4119,4073,4119, 4074, 4127, 4070, 4110)
			           ,get_master_set_type(c207_set_id)
				  INTO p_invfl, v_shipto, v_mrid, v_type , v_set_type
				  FROM t504_consignment
				 WHERE c504_consignment_id = p_consignid
			FOR UPDATE;
			
			IF v_type_id IS NOT NULL AND v_type_id != '0' THEN
				v_type := v_type_id;
			END IF;

			IF p_invfl IS NULL OR p_invfl = ''
			THEN
				--			
				IF v_intransid IS NULL -- Skip the update inventory for intrasit because bulk transaction not happened.
				THEN
					gm_update_inventory(p_consignid, 'CSET', 'MINUS', p_userid, v_msg);
				ELSE
				   -- To get Party Id for Ledger Posting 
				   SELECT get_acct_party_id (p_consignid, 'CSET')
				     INTO v_party_id
				     FROM DUAL;
				   
				   IF v_set_type = 4078
				   THEN
				   		v_post_id := '26240400'; -- Intrans Demo set Posting id 
				   ELSE		
				   		v_post_id := '26240375';
				   END IF;
				   -- Intransit local cost posting (PMT-18235)  
	            	FOR rad_val IN cur_consignment
					LOOP
						gm_save_ledger_posting (v_post_id,
						                        CURRENT_DATE,
						                        rad_val.ID,
						                        v_party_id,
						                        p_consignid,
						                        rad_val.qty,
						                        rad_val.price,
						                        p_userid,
						                        NULL,
						                        v_owner_company_id,
						                        rad_val.price);
					END LOOP; 
				END IF;
			--
			END IF;
			
			--get value from rule table to check if it's US or not.
	        SELECT get_rule_value ('US', 'COUNTRY') INTO v_country FROM DUAL;
			
			IF NVL(v_country, '-999') = 'Y' 
			THEN
				v_status_fl := '1.20'; -- Pending Putaway for US
				v_req_status_fl := '20'; --ready to consign for US
			ELSE
				 SELECT DECODE (v_shipto, NULL, p_flag, '3') INTO v_status_fl FROM DUAL ;
				 SELECT DECODE (v_shipto, NULL, '20', '30') INTO v_req_status_fl FROM DUAL ;
			END IF;
						
			UPDATE t504_consignment
			   SET c504_update_inv_fl = '1'
				 , c504_status_fl =   v_status_fl   --change to (pending FG) instead of DECODE (v_shipto, NULL, p_flag, '3')
				 , c504_verify_fl = p_verflag
				 , c504_verified_date = SYSDATE
				 , c504_verified_by = p_userid
				 , c504_last_updated_by = p_userid
				 , c504_last_updated_date = SYSDATE
				 , c504_type = NVL(v_type, c504_type)
			 WHERE c504_consignment_id = p_consignid;

			 SELECT DECODE(v_type, 4119, 10001, 4127,10004, 4110, 10003) 
				INTO  v_invtype
			 FROM DUAL;
			 
		 	 /*
		 	  * When the set type is updated the corresponding inventory type
		 	  * need to be updated in the tag table too.
		 	  */		 	 			 
			 UPDATE t5010_tag t5010
			   SET   --10001 In-House Loaner/Consignment, 10003	Loose Item/Sales Consignment,  10004  Product Loaner
				 -- 4119 inhouse loaner, 4127 product loaner,  4110 consignment
				   t5010.c901_inventory_type =  v_invtype 
				 , t5010.c5010_last_updated_by = p_userid
				 , t5010.c5010_last_updated_date = SYSDATE 
			 WHERE t5010.c5010_tag_id = v_tagid; 

			gm_pkg_allocation.gm_ins_invpick_assign_detail (93604, p_consignid, p_userid);
			--93604 consignment set , to insert record in the t5050_INVPICK_ASSIGN_DETAIL

			UPDATE t520_request
			   SET c520_status_fl =   v_req_status_fl --change to (Ready to Consign) instead of DECODE (v_shipto, NULL, '20', '30')
				 , c520_last_updated_by = p_userid
				 , c520_last_updated_date = SYSDATE
			 WHERE c520_request_id = v_mrid AND c520_void_fl IS NULL;

			 SELECT C520_PLANNED_SHIP_DATE
				into v_plan_ship_date
			 FROM T520_REQUEST
			 WHERE c520_request_id = v_mrid AND c520_void_fl IS NULL;

			-- If this RQ has a child request, make the Child like an Item consignment
			UPDATE t520_request
			   SET c207_set_id = NULL
				 , c520_last_updated_by = p_userid
				 , c520_last_updated_date = SYSDATE
			 WHERE c520_master_request_id = v_mrid;

			SELECT c504_status_fl
			  INTO v_flag
			  FROM t504_consignment
			 WHERE c504_consignment_id = p_consignid;

			IF v_flag = 3
			THEN
				--51076 Type of Source logging Transaction Status
				gm_pkg_cm_shipping_trans.gm_sav_release_shipping(p_consignid, 50181, p_userid, '51076');
			END IF;
		END IF;

		IF (p_verflag = '0' OR p_verflag = '1')
		THEN
			--91161 Control , 91101 Consignment
			SELECT DECODE (p_verflag, '0', 'Controlled', '1', 'Verified')
			  INTO v_status_type
			  FROM DUAL;

			gm_pkg_cm_shipping_trans.gm_sav_shipping_status_log(p_consignid, 50181, p_userid, v_status_type);
		END IF;

		--Any consignment which none of parts inside has valid control number and status is 1-WIP , Should be changed to '0' -initiated.
		SELECT COUNT(1)
		  INTO v_wip_count
		  FROM t505_item_consignment
		 WHERE c504_consignment_id = p_consignid AND c505_control_number IS NOT NULL AND c505_control_number != 'TBE';

		IF v_wip_count = 0
		THEN
			UPDATE t504_consignment
			   SET c504_status_fl = '0'
				 , c504_last_updated_date = SYSDATE
				 , c504_last_updated_by = p_userid
			 WHERE c504_consignment_id = p_consignid;
		END IF;
	ELSE
		--
		v_msg		:= 'p_str is empty';
	--
	END IF;

	SELECT c504_status_fl 
	  INTO v_flag 
	  FROM t504_consignment
	 WHERE c504_consignment_id = p_consignid;
	
	IF  (p_verflag = '1' and (v_type = 4127 OR v_type = 4119))  -- Product Loaner or In house Loaner
		THEN
	
			gm_update_set_consign(p_consignid, v_type, 0, 0, 0, 0, NULL, '', NULL, p_userid, 0, 0, NULL, v_msg, NULL, v_plan_ship_date);
			
	/*
	 * Moving the below 2 procedure lines to the  gm_update_set_consign procedure
	 *  Author: gpalani. PMT: 13955 Date: 09/11/2017
	 */
		-- gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv(p_consignid,'T505',p_consignid,v_invtype,NULL,v_type,p_userid,v_type);
			
		-- gm_pkg_set_lot_track.gm_pkg_set_lot_update(p_consignid,p_userid,p_consignid,v_invtype);
			
        		
	 END IF;
	    
     IF(p_verflag = '1'  AND v_status_fl = '1.20') THEN
	    --To Clear the Location of the WIP SET Consignment on verifying the SET
		gm_pkg_op_consignment.gm_clear_wip_loc(p_consignid,p_userid);
	 END IF;

	v_msg		:= 'Success';
	p_errmsg	:= v_msg;
END gm_save_set_build;
/
