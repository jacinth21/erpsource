-- @"C:\database\Procedures\Operations\GM_UPDATE_PO_OPER.prc";
CREATE OR REPLACE PROCEDURE GM_UPDATE_PO_OPER
(
	   p_POId				IN T401_PURCHASE_ORDER.C401_PURCHASE_ORD_ID%TYPE,
	   p_ReqDate			IN VARCHAR2,
	   p_Comments			IN T401_PURCHASE_ORDER.C401_PO_NOTES%TYPE,
	   p_UserId				IN T401_PURCHASE_ORDER.C401_LAST_UPDATED_BY%TYPE,
	   p_POTotal			IN T401_PURCHASE_ORDER.C401_PO_TOTAL_AMOUNT%TYPE,
	   p_publishfl          IN T401_PURCHASE_ORDER.c401_publish_fl%TYPE,
	   p_alPOType			IN T401_PURCHASE_ORDER.c401_type%TYPE,
	   p_message 			OUT  VARCHAR2			
)
AS
	   v_total_amt          T401_PURCHASE_ORDER.C401_PO_TOTAL_AMOUNT%TYPE; 
BEGIN
	   SELECT GET_PO_TOTAL_AMOUNT(p_POId) INTO v_total_amt FROM DUAL;
		
	   IF (p_publishfl = 'Y') THEN	   -- In two places the procedure is triggered so needs to update the publish fields when the publish flag is Y
	   		UPDATE T401_PURCHASE_ORDER SET
					C401_DATE_REQUIRED = to_date(p_ReqDate,NVL(get_rule_value('DATEFMT','DATEFORMAT'),'mm/dd/yyyy')),
					C401_PO_NOTES = p_Comments,
					C401_PO_TOTAL_AMOUNT = v_total_amt,
          			C401_PUBLISH_FL = p_publishfl,
			   		C401_PUBLISHED_DT = CURRENT_DATE,
			        C401_PUBLISHED_BY = p_UserId,
         			C901_STATUS = '108360',
					C401_LAST_UPDATED_BY = p_UserId,
					C401_LAST_UPDATED_DATE = CURRENT_DATE,
					C401_TYPE = p_alPOType
			  WHERE C401_PURCHASE_ORD_ID = p_POId
			  AND C401_VOID_FL IS NULL;			  
	   ELSE 	   
			  UPDATE T401_PURCHASE_ORDER SET
					C401_DATE_REQUIRED = to_date(p_ReqDate,NVL(get_rule_value('DATEFMT','DATEFORMAT'),'mm/dd/yyyy')),
					C401_PO_NOTES = p_Comments,
					C401_PO_TOTAL_AMOUNT = v_total_amt,
					C401_LAST_UPDATED_BY = p_UserId,
					C401_LAST_UPDATED_DATE = CURRENT_DATE,
					C401_TYPE = p_alPOType
			  WHERE C401_PURCHASE_ORD_ID = p_POId 
			  AND C401_VOID_FL IS NULL;			  
         END IF;
       		  EXCEPTION WHEN OTHERS
      	 	  THEN
                       p_message := SQLERRM;
              RETURN;
END GM_UPDATE_PO_OPER;
/