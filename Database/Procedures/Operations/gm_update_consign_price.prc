/* Formatted on 2010/01/25 15:13 (Formatter Plus v4.8.0) */
-- @ "C:\database\procedures\Operations\gm_update_consign_price.prc"

CREATE OR REPLACE PROCEDURE gm_update_consign_price (
	p_consign_id   IN	t504_consignment.c504_consignment_id%TYPE
  , p_party_id	   IN	t101_party.c101_party_id%TYPE
)
AS
--
-- Greece : Party ID:202
-- India : Party ID: 203
-- UK	  : Party ID: 204
--	Malaysia (Primamedik) Party ID: 205
--
	CURSOR pop_val
	IS
		SELECT b.c505_item_consignment_id seqid
			 , get_account_part_pricing (NULL, b.c205_part_number_id, p_party_id) price
		  --get_ac_cogs_value (b.c205_part_number_id, 4900) price
		FROM   t504_consignment a, t505_item_consignment b
		 WHERE a.c504_consignment_id = b.c504_consignment_id
		   AND a.c504_consignment_id = p_consign_id
		   AND b.c505_item_qty <> 0;
--
BEGIN
--
	FOR rad_val IN pop_val
	LOOP
		IF rad_val.price IS NULL
		THEN
			raise_application_error (-20096, '');
		END IF;

		UPDATE t505_item_consignment
		   SET c505_item_price = rad_val.price
		 WHERE c505_item_consignment_id = rad_val.seqid;
	END LOOP;

   --When ever the detail is updated, the consignment table has to be updated ,so ETL can Sync it to GOP.
   UPDATE t504_consignment
      SET c504_last_updated_date = Sysdate
    WHERE c504_consignment_id = p_consign_id;	
--
END gm_update_consign_price;
/
