/* Formatted on 2010/05/18 12:38 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\common\gm_pkg_common_cancel_op.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_common_cancel_op
IS
--
	PROCEDURE gm_fc_sav_void_demandsheet (
		p_demandsheetid   IN   t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_user_id		  IN   t101_user.c101_user_id%TYPE
	)
	AS
		/*******************************************************
			  * Description : Procedure to void Demand Sheet
			  * Parameters		: 1. Demand Sheet Id
			  * 				2. User Id
			  *
			  *******************************************************/
		v_void_fl	   t4020_demand_master.c4020_void_fl%TYPE;
	--
	BEGIN
		SELECT		  c4020_void_fl
				 INTO v_void_fl
				 FROM t4020_demand_master
				WHERE c4020_demand_master_id = p_demandsheetid
		FOR UPDATE OF c4020_void_fl;

		IF v_void_fl IS NOT NULL
		THEN
			-- Error message is Sorry. Demand Sheet has already been voided.
			raise_application_error (-20049, '');
		END IF;

		UPDATE t4020_demand_master
		   SET c4020_void_fl = 'Y'
			 , c4020_last_updated_by = p_user_id
			 , c4020_last_updated_date = SYSDATE
		 WHERE c4020_demand_master_id = p_demandsheetid;
	END gm_fc_sav_void_demandsheet;

	--
	PROCEDURE gm_fc_sav_void_group (
		p_groupid	IN	 t4010_group.c4010_group_id%TYPE
	  , p_user_id	IN	 t101_user.c101_user_id%TYPE
	)
	AS
		/*******************************************************
		 * Description : Procedure to void Group
		 * Parameters  : 1. Group Id
		 *				   2. User Id
		 *
		 *******************************************************/
		v_void_fl	   t4010_group.c4010_void_fl%TYPE;
		v_publish_fl   t4010_group.c4010_publish_fl%TYPE;
	--
	BEGIN
		SELECT		  c4010_void_fl
				 INTO v_void_fl
				 FROM t4010_group
				WHERE c4010_group_id = p_groupid
		FOR UPDATE OF c4010_void_fl;

		IF v_void_fl IS NOT NULL
		THEN
			-- Error message is Sorry.Group has already been voided.
			raise_application_error (-20050, '');
		END IF;

		v_publish_fl := gm_pkg_pd_group_pricing.get_publish_fl (p_groupid);

		IF v_publish_fl = 'Y'
		THEN
			-- Group can not be voided, as it is already published.
			raise_application_error (-20277, '');
		END IF;

		UPDATE t4010_group
		   SET c4010_void_fl = 'Y'
			 , c4010_last_updated_by = p_user_id
			 , c4010_last_updated_date = SYSDATE
		 WHERE c4010_group_id = p_groupid;
	END gm_fc_sav_void_group;

--
  --
	PROCEDURE gm_fc_sav_void_ttp (
		p_ttpid 	IN	 t4050_ttp.c4050_ttp_id%TYPE
	  , p_user_id	IN	 t101_user.c101_user_id%TYPE
	)
	AS
		/*******************************************************
		 * Description : Procedure to void TTP
		 * Parameters  : 1. Group Id
		 *				   2. User Id
		 *
		 *******************************************************/
		v_void_fl	   t4050_ttp.c4050_void_fl%TYPE;
	--
	BEGIN
		SELECT		  c4050_void_fl
				 INTO v_void_fl
				 FROM t4050_ttp
				WHERE c4050_ttp_id = p_ttpid
		FOR UPDATE OF c4050_void_fl;

		IF v_void_fl IS NOT NULL
		THEN
			-- Error message is Sorry.TTP has already been voided.
			raise_application_error (-20051, '');
		END IF;

		UPDATE t4050_ttp
		   SET c4050_void_fl = 'Y'
			 , c4050_last_updated_by = p_user_id
			 , c4050_last_updated_date = SYSDATE
		 WHERE c4050_ttp_id = p_ttpid;
	END gm_fc_sav_void_ttp;

--
	PROCEDURE gm_fc_sav_void_part_growth (
		p_refid 	IN	 t4030_demand_growth_mapping.c4030_demand_growth_id%TYPE
	  , p_user_id	IN	 t101_user.c101_user_id%TYPE
	)
	AS
		/*******************************************************
		 * Description : Procedure to void Group Part Growth
		 * Parameters  : 1. Group Id
		 *				   2. User Id
		 *
		 *******************************************************/
		v_void_fl	   t4050_ttp.c4050_void_fl%TYPE;
	--
	BEGIN
		SELECT c4030_void_fl
		  INTO v_void_fl
		  FROM t4030_demand_growth_mapping t4030
		 WHERE c4030_ref_id = p_refid;

		IF v_void_fl IS NOT NULL
		THEN
			-- Error message is Sorry.This has already been voided.
			raise_application_error (-20051, '');
		END IF;

		UPDATE t4030_demand_growth_mapping t4030
		   SET t4030.c4030_void_fl = 'Y'
			 , t4030.c4030_last_updated_by = p_user_id
			 , t4030.c4030_last_updated_date = SYSDATE
		 WHERE t4030.c4020_demand_master_id = 26 AND t4030.c901_ref_type = 20298;
	END gm_fc_sav_void_part_growth;

		/*******************************************************
	* Description : Procedure to execute demand sheet
	* Parameters	: 1. Month demand sheet Id
	*					2. User Id
	*******************************************************/
	PROCEDURE gm_op_exec_dsload (
		p_dsmonthid   IN   t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_user_id	  IN   t101_user.c101_user_id%TYPE
	)
	AS
		v_status_fl    t4040_demand_sheet.c901_status%TYPE;
		v_master_demand_id t4040_demand_sheet.c4020_demand_master_id%TYPE;
	BEGIN
		SELECT c901_status, c4020_demand_master_id
		  INTO v_status_fl, v_master_demand_id
		  FROM t4040_demand_sheet
		 WHERE c4040_demand_sheet_id = p_dsmonthid;

		-- If Sheet is not Open (50550) then throw error
		IF v_status_fl <> '50550'
		THEN
			-- Error message is Sorry. Only OPEN sheets can be reloaded
			raise_application_error (-20125, '');
		END IF;

		gm_pkg_op_ld_demand.gm_op_sav_load_detail (v_master_demand_id, 91951, p_user_id);
	END gm_op_exec_dsload;

	/*******************************************************
	* Description : Procedure to Rollback a loaner return
	*******************************************************/
	PROCEDURE gm_cs_sav_rollback_loaner_retn (
		p_conid    IN	t504_consignment.c504_consignment_id%TYPE
	  , p_userid   IN	t101_user.c101_user_id%TYPE
	)
	AS
		v_date		   VARCHAR2 (20);	--t504a_loaner_transaction.c504a_return_dt%TYPE;
		v_shipto	   t907_shipping_info.c901_ship_to%TYPE;
		v_shiptoid	   t907_shipping_info.c907_ship_to_id%TYPE;
		v_consignedto  t504a_loaner_transaction.c901_consigned_to%TYPE;
		v_consignedtoid t504a_loaner_transaction.c504a_consigned_to_id%TYPE;
		v_repid 	   t504a_loaner_transaction.c703_sales_rep_id%TYPE;
		v_accid 	   t504a_loaner_transaction.c704_account_id%TYPE;
		v_expdtrt	   t504a_loaner_transaction.c504a_expected_return_dt%TYPE;
		v_loanerdt	   t504a_loaner_transaction.c504a_loaner_dt%TYPE;
	BEGIN
		SELECT gm_pkg_op_loaner.get_return_dt (p_conid)
		  INTO v_date
		  FROM DUAL;

		SELECT c901_consigned_to, c504a_consigned_to_id, c703_sales_rep_id, c704_account_id, c504a_expected_return_dt
			 , c504a_loaner_dt
		  INTO v_consignedto, v_consignedtoid, v_repid, v_accid, v_expdtrt
			 , v_loanerdt
		  FROM t504a_loaner_transaction
		 WHERE c504_consignment_id = p_conid AND c504a_return_dt = TO_DATE (v_date, 'mm/dd/yyyy');

		UPDATE t504_consignment
		   SET c701_distributor_id = v_consignedtoid
			 , c704_account_id = v_accid
		 -- , c504_ship_to = v_shipto
		 -- , c504_ship_to_id = v_shiptoid
		WHERE  c504_consignment_id = p_conid AND c504_void_fl IS NULL;

		UPDATE t504a_consignment_loaner
		   SET c504a_loaner_dt = v_loanerdt
			 , c504a_expected_return_dt = v_expdtrt
			 , c504a_status_fl = 20   -- rollback to pending return
		 WHERE c504_consignment_id = p_conid;

		UPDATE t504a_loaner_transaction
		   SET c504a_return_dt = NULL
			 , c504a_last_updated_by = p_userid
			 , c504a_last_updated_date = SYSDATE
		 WHERE c504_consignment_id = p_conid
		   AND c504a_return_dt = TO_DATE (v_date, 'mm/dd/yyyy')
		   AND c504a_void_fl IS NULL;

		gm_save_status_details (p_conid, '', p_userid, NULL, SYSDATE, 91125, 91103);
	END gm_cs_sav_rollback_loaner_retn;

	/*******************************************************
	* Description : Procedure to void inhouse transaction
	*******************************************************/
	PROCEDURE gm_op_sav_void_inhousetxn (
		p_txn_id   IN	t907_cancel_log.c907_ref_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_ref_id	   t504_consignment.c504_reprocess_id%TYPE;
		v_table 	   VARCHAR (10);
		v_verify_fl    VARCHAR2 (1);
	BEGIN
		BEGIN
			SELECT t504.c504_verify_fl
			  INTO v_verify_fl
			  FROM t504_consignment t504
			 WHERE t504.c504_consignment_id = p_txn_id;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				BEGIN
					SELECT t412.c412_verify_fl
					  INTO v_verify_fl
					  FROM t412_inhouse_transactions t412
					 WHERE c412_inhouse_trans_id = p_txn_id;
				END;
		END;

		IF v_verify_fl IS NOT NULL
		THEN
			raise_application_error (-20166, '');
		END IF;

		BEGIN
			SELECT t504.c504_reprocess_id, 'CONS'
			  INTO v_ref_id, v_table
			  FROM t504_consignment t504
			 WHERE t504.c504_consignment_id = p_txn_id;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				BEGIN
					SELECT c412_ref_id, 'INHOUSE'
					  INTO v_ref_id, v_table
					  FROM t412_inhouse_transactions
					 WHERE c412_inhouse_trans_id = p_txn_id;
				END;
		END;

		IF v_ref_id IS NOT NULL
		THEN
			raise_application_error (-20165, '');
		END IF;

		IF v_table = 'INHOUSE'
		THEN
			UPDATE t412_inhouse_transactions
			   SET c412_void_fl = 'Y'
				 , c412_last_updated_date = SYSDATE
				 , c412_last_updated_by = p_userid
			 WHERE c412_inhouse_trans_id = p_txn_id;
		ELSE
			UPDATE t504_consignment
			   SET c504_void_fl = 'Y'
				 , c504_last_updated_date = SYSDATE
				 , c504_last_updated_by = p_userid
			 WHERE c504_consignment_id = p_txn_id;
		END IF;
	END gm_op_sav_void_inhousetxn;

	/*******************************************************
	 * Description : Procedure to rollback Loaner process (WIP,Pending Veri,Available->Pending Processing)
	 *******************************************************/
	PROCEDURE gm_cs_sav_rollback_loaner_pros (
		p_cons_id		 IN   t504a_consignment_loaner.c504_consignment_id%TYPE
	  , p_cancelreason	 IN   t907_cancel_log.c901_cancel_cd%TYPE
	  , p_canceltype	 IN   t901_code_lookup.c902_code_nm_alt%TYPE
	  , p_comments		 IN   t907_cancel_log.c907_comments%TYPE
	  , p_user_id		 IN   t504a_loaner_transaction.c504a_created_by%TYPE
	)
	AS
		CURSOR loaner_inhouse_trans_cur (
			p_txn_id   IN	t504a_loaner_transaction.c504a_loaner_transaction_id%TYPE
		)
		IS
			SELECT c412_inhouse_trans_id loanid, c412_status_fl statusfl, c412_type typ
			  FROM t412_inhouse_transactions
			 WHERE c412_ref_id = p_cons_id AND c504a_loaner_transaction_id = p_txn_id AND c412_void_fl IS NULL;

		CURSOR loaner_inhouse_trans_item_cur (
			p_loanid   IN	t412_inhouse_transactions.c412_inhouse_trans_id%TYPE
		)
		IS
			SELECT c205_part_number_id pnum, c413_item_qty qty
			  FROM t413_inhouse_trans_items
			 WHERE c412_inhouse_trans_id = p_loanid;

		CURSOR loaner_incident_cur (
			p_txn_id   IN	t504a_loaner_transaction.c504a_loaner_transaction_id%TYPE
		)
		IS
			SELECT t9200.c9200_incident_id incid, t9201.c9201_charge_details_id chrid
			  FROM t9200_incident t9200, t9201_charge_details t9201
			 WHERE t9200.c9200_incident_id = t9201.c9200_incident_id
			   AND c9200_ref_id = p_txn_id
			   AND c9200_ref_type = 4127
			   AND t9200.c9200_void_fl IS NULL
			   AND t9201.c9201_void_fl IS NULL;

		v_txn_id	   t504a_loaner_transaction.c504a_loaner_transaction_id%TYPE;
		v_rule_value   t906_rules.c906_rule_value%TYPE;
		v_return_dt    t504a_loaner_transaction.c504a_return_dt%TYPE;
		v_canceltype   t901_code_lookup.c902_code_nm_alt%TYPE;
		v_cancelreason t907_cancel_log.c901_cancel_cd%TYPE;
		v_status_fl    t504a_consignment_loaner.c504a_status_fl%TYPE;
	BEGIN
		SELECT c504a_status_fl
		  INTO v_status_fl
		  FROM t504a_consignment_loaner
		 WHERE c504_consignment_id = p_cons_id;

		IF v_status_fl <> '30' AND v_status_fl <> '40' AND v_status_fl <> '0'
		THEN
			--Sorry.Cannot Rollback this consignment
			raise_application_error (-20179, '');
		END IF;

		SELECT c504a_loaner_transaction_id, c504a_return_dt
		  INTO v_txn_id, v_return_dt
		  FROM t504a_loaner_transaction t504a
		 WHERE c504a_loaner_transaction_id IN (SELECT MAX (TO_NUMBER (c504a_loaner_transaction_id))
												 FROM t504a_loaner_transaction t504a
												WHERE t504a.c504_consignment_id = p_cons_id);

		v_rule_value := get_rule_value ('ROLLBACKLOANER', 'ROLLBACK');

		IF TO_DATE (v_rule_value, 'MM/DD/YYYY') > v_return_dt
		THEN
			--Sorry.Cannot Rollback this consignment due to Return Date
			raise_application_error (-20180, '');
		END IF;

-- Loaner Update
		UPDATE t504a_consignment_loaner
		   SET c504a_status_fl = '25'
			 , c504a_available_date = NULL
			 , c504a_last_updated_by = p_user_id
			 , c504a_last_updated_date = SYSDATE
		 WHERE c504_consignment_id = p_cons_id;

		UPDATE t504a_loaner_transaction
		   SET c504a_processed_date = NULL
			 , c504a_last_updated_by = p_user_id
			 , c504a_last_updated_date = SYSDATE
		 WHERE c504a_loaner_transaction_id = v_txn_id;

-- Loaner Incident Update
		FOR loaner_incident IN loaner_incident_cur (v_txn_id)
		LOOP
			UPDATE t9200_incident
			   SET c9200_void_fl = 'Y'
				 , c9200_updated_date = SYSDATE
				 , c9200_updated_by = p_user_id
			 WHERE c9200_incident_id = loaner_incident.incid;

			UPDATE t9201_charge_details
			   SET c9201_void_fl = 'Y'
				 , c9201_updated_date = SYSDATE
				 , c9201_updated_by = p_user_id
			 WHERE c9201_charge_details_id = loaner_incident.chrid;
		END LOOP;

-- Outer loop to void all In-house Transaction
		FOR loan_trans_id IN loaner_inhouse_trans_cur (v_txn_id)
		LOOP
			UPDATE t412_inhouse_transactions
			   SET c412_void_fl = 'Y'
				 , c412_last_updated_by = p_user_id
				 , c412_last_updated_date = SYSDATE
			 WHERE c412_inhouse_trans_id = loan_trans_id.loanid;

			gm_pkg_common_cancel.gm_cm_sav_cancelrow (loan_trans_id.loanid
													, p_cancelreason
													, 'RLNTX'
													, p_comments
													, p_user_id
													 );

			IF	   (loan_trans_id.typ = 50155 OR loan_trans_id.typ = 50157 OR loan_trans_id.typ = 50158)
			   AND loan_trans_id.statusfl = '4'
			THEN
				UPDATE t505_item_consignment
				   SET c505_void_fl = 'Y'
				 WHERE c505_ref_id = loan_trans_id.loanid;

-- Inner Loop for ledger posting
				FOR rad_val IN loaner_inhouse_trans_item_cur (loan_trans_id.loanid)
				LOOP
					gm_save_ledger_posting (get_rule_value (loan_trans_id.typ, 'ROLLBACK')
										  , SYSDATE
										  , rad_val.pnum
										  , NULL
										  , loan_trans_id.loanid
										  , rad_val.qty
										  , NULL
										  , p_user_id
										   );

-- Updating the respective buckets based on Transaction type
					UPDATE t205_part_number t205
					   SET t205.c205_qty_in_stock =
							   DECODE (loan_trans_id.typ
									 , 50155, t205.c205_qty_in_stock + rad_val.qty
									 , 50157, t205.c205_qty_in_stock - rad_val.qty
									 , t205.c205_qty_in_stock
									  )
						 , t205.c205_last_update_trans_id = loan_trans_id.loanid
						 , t205.c901_action = DECODE (loan_trans_id.typ, 50155, 4301, 50157, 4302, 50158, 4302)
						 , t205.c901_type = 4127
						 , t205.c205_last_updated_by = p_user_id
						 , t205.c205_last_updated_date = SYSDATE
					 WHERE t205.c205_part_number_id = rad_val.pnum;
           
           UPDATE t205_part_number t205
					   SET t205.c205_qty_in_quarantine =
							   DECODE (loan_trans_id.typ
									 , 50158, t205.c205_qty_in_quarantine - rad_val.qty
									 , t205.c205_qty_in_quarantine
									  )
						 , t205.c205_last_update_trans_id = loan_trans_id.loanid
						 , t205.c901_action = DECODE (loan_trans_id.typ, 50155, 4301, 50157, 4302, 50158, 4302)
						 , t205.c901_type = 4127
						 , t205.c205_last_updated_by = p_user_id
						 , t205.c205_last_updated_date = SYSDATE
					 WHERE t205.c205_part_number_id = rad_val.pnum;
           
				END LOOP;
			END IF;
		END LOOP;
	END gm_cs_sav_rollback_loaner_pros;
END gm_pkg_common_cancel_op;
/
