/* Formatted on 2011/05/11 10:35 (Formatter Plus v4.8.0) */
-- @"C:\database\Procedures\Operations\gm_update_inventory_returns.prc"

CREATE OR REPLACE PROCEDURE gm_update_inventory_returns (
	p_id		   IN	VARCHAR2
  , p_type		   IN	VARCHAR2
  , p_action	   IN	VARCHAR2
  , p_updated_by   IN	VARCHAR2
)
/**************************************************************
 * Code Lookup values
 * ACTION
 * -----------
 * 4301 - Plus
 * 4302 - Minus
 * TYPE
 * ------------
  * 4312,'Return'
 **************************************************************/
AS
--
	v_party_id	   VARCHAR2 (20);
	v_post_id	   VARCHAR2 (20);
	v_rule_value   VARCHAR2 (20);
	v_dist_id	   VARCHAR2 (20);
	v_acc_id	   VARCHAR2 (20);
	v_reason	   VARCHAR2 (20);
	v_ra_type	   NUMBER;
	v_item_type    NUMBER;
	v_inv_fl	   NUMBER := 1;
	v_sign		   NUMBER := 1;
	v_post_type1   VARCHAR2 (20);
	v_post_type2   VARCHAR2 (20);
	v_post_type3   VARCHAR2 (20);
	v_sign_action  NUMBER := 4301;
	v_fs_sign	   NUMBER := 4302;
	v_disttype	   NUMBER;
	v_post_type4   VARCHAR2 (20);
	v_post_type5   VARCHAR2 (20);
	v_ord_typ	   t501_order.c901_order_type%TYPE;
	v_ord_dist_id t701_distributor.c701_distributor_id%TYPE;
	v_ord_acc_id t704_account.c704_account_id%TYPE;
	v_fs_upd_fl VARCHAR2(1);
	v_ous_sales_fl VARCHAR2(1);
	v_warehouse_ref_id  t5052_location_master.c5052_ref_id%TYPE;
	v_account_warehouse_type NUMBER;
	v_repost_id t906_rules.c906_rule_value%TYPE;
	v_add_post_id t906_rules.c906_rule_value%TYPE;
	v_add_post_fl VARCHAR2(1);
	v_fs_repost_fl VARCHAR2(1);
	v_set_type t207_set_master.c207_type%TYPE;
	v_destination_cmpid t820_costing.c1900_owner_company_id%TYPE;
    v_owner_cmpid t820_costing.c1900_owner_company_id%TYPE;
    v_parent_cmpid t701_distributor.c1900_parent_company_id%TYPE;
    v_order_returns_post_id NUMBER;
    v_trans_company_id t1900_company.c1900_company_id%TYPE;
   	v_trans_plant_id t5040_plant_master.c5040_plant_id%TYPE;
   	v_order_fl VARCHAR2(1);
  --PMT-32995 Duplicate Orders on Bill Only Orders are transacting like Bill and Ship Orders
    v_parent_ord_id t501_order.c501_order_id%TYPE;
    v_org_ord_type t501_order.c901_order_type%TYPE;
--
V_ICTACCID T704_account.c704_account_id%TYPE;
	CURSOR cur_credit
	IS
		SELECT	 c205_part_number_id ID, SUM (c507_item_qty) qty, c507_item_price price, c901_type itype
			FROM t507_returns_item
		   WHERE c506_rma_id = p_id AND c507_status_fl = 'C'
		GROUP BY c205_part_number_id, c507_item_price, c901_type;
--
BEGIN		
--
	IF p_type = 'R'
	THEN
		v_post_type1 := 'CON-RETURN-AGENT';
		v_post_type2 := 'CON-RETURN-INHOUSE';
		v_post_type3 := 'SALES-RETURN';
		v_post_type4 := 'CON-RETURN-AGENT-ICT';
		v_sign_action := 4301;
		-- FS warehouse
		v_fs_sign := 4302;
	ELSIF p_type = 'CR'
	THEN
		v_sign		:= -1;
		v_post_type1 := 'R-CON-RETURN-AGENT';
		v_post_type2 := 'R-CON-RETURN-INHOUSE';
		v_post_type3 := 'R-SALES-RETURN';
		v_post_type4 := 'R-CON-RETURN-ICT';
		v_sign_action := 4302;
		-- FS warehouse
		v_fs_sign := 4301;
	END IF;

	-- To get Party Id for Ledger Posting -- Added Feb 9
	SELECT get_acct_party_id (p_id, p_type)
	  INTO v_party_id
	  FROM DUAL;

	IF v_party_id IS NULL
	THEN
		raise_application_error (-20922, '');
	END IF;

	-- to get the transactions company id and plant id
	gm_pkg_common.gm_fch_trans_comp_plant_id(p_id, v_trans_company_id, v_trans_plant_id);
	--
    IF v_trans_company_id IS NOT NULL AND v_trans_plant_id IS NOT NULL THEN             
     	gm_pkg_cor_client_context.gm_sav_client_context(v_trans_company_id,v_trans_plant_id);
    END IF;
	--
	-- Join the order table to get the order type
	SELECT t506.c701_distributor_id, t506.c704_account_id, t506.c506_reason ,
		  t506.c506_type, NVL(t501.c901_order_type, 2521), t501.c501_distributor_id,
		  t501.c704_account_id, t207.c207_type,
		  get_rule_value (NVL(get_order_attribute_value(t501.c501_order_id, 101720), NVL(t501.c901_order_type, '-9999')), 'ORDER_RETURN_POSTING'),
		  t501.c501_order_id
		  --101720 Original Order Type
		INTO v_dist_id, v_acc_id, v_reason,
		  v_ra_type, v_ord_typ, v_ord_dist_id, 
		  v_ord_acc_id, v_set_type, v_order_returns_post_id,v_parent_ord_id
		FROM t506_returns t506,
		  t501_order t501,
		  t207_set_master t207
	WHERE t506.c506_rma_id      = p_id
	AND t506.c501_order_id = t501.c501_order_id(+)
	AND t506.c506_void_fl IS NULL
	AND t501.c501_void_fl(+) IS NULL
	AND t506.c207_set_id = t207.c207_set_id(+)
	AND c207_void_fl(+) IS NULL;

	BEGIN
		SELECT c901_distributor_type,c1900_parent_company_id
		  INTO v_disttype, v_parent_cmpid 
		  FROM t701_distributor
		 WHERE c701_distributor_id = v_dist_id;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			NULL;
	END;
    -- To get the warehouse transactions details information
	BEGIN
	     SELECT DECODE (t704.c5052_location_id, NULL, v_ord_dist_id, v_ord_acc_id)
	     	-- Account consignment - warehouse to be Account warehouse
	     	, DECODE(t704.c5052_location_id, NULL, NULL, 3304)
	       INTO v_warehouse_ref_id, v_account_warehouse_type
	       FROM t704_account t704
	      WHERE t704.c704_account_id = v_ord_acc_id;
	EXCEPTION
	WHEN NO_DATA_FOUND THEN
	    v_warehouse_ref_id := NULL;
		v_account_warehouse_type := NULL;
	END;
	
	--In-transit RA posting
	IF v_ra_type IN ('26240177', '3307') --ICT Returns - Items/Sets
	THEN
		IF p_type = 'R'
		THEN
			v_post_type5 := 'CON-IN-TRANSIT';
			
			SELECT NVL(get_compid_frm_cntx(),1000),v_parent_cmpid 
			  INTO v_destination_cmpid,v_owner_cmpid 
			  FROM DUAL;
		ELSIF p_type = 'CR'
		THEN 
			v_post_type5 := 'R-CON-IN-TRANSIT';
			
			SELECT v_parent_cmpid, v_parent_cmpid
			  INTO v_destination_cmpid,v_owner_cmpid 
			  FROM DUAL;
		END IF;
		-- If parent company empty then, to skip the In-Transit posting
		SELECT DECODE (v_parent_cmpid, NULL, NULL, v_post_type5)
			INTO v_post_type5
		FROM DUAL;
		--
	END IF;
		
	IF v_dist_id IS NOT NULL
	THEN
		-- Added the IF condition for ICT Returns which has to be posted to new account I/C Consignment Return Sales Rep(48155) in the rule group CON-RETURN-AGENT-ICT
		IF v_disttype = 70103 OR v_disttype = 70106
		THEN
			v_post_id	:= NVL(get_rule_value (v_set_type, v_post_type4),NVL(get_rule_value (v_reason, v_post_type5),get_rule_value (v_reason, v_post_type4)));
		ELSE
			v_post_id	:= NVL(get_rule_value (v_set_type, v_post_type1),NVL(get_rule_value (v_reason, v_post_type5),get_rule_value (v_reason, v_post_type1)));
		END IF;
	ELSIF v_dist_id IS NULL AND v_acc_id = '01'
	THEN
		v_post_id	:= get_rule_value (v_reason, v_post_type2);
	ELSIF v_acc_id IS NOT NULL AND v_ra_type = '3304' -- account consignment
	THEN
		v_post_id	:= get_rule_value (v_reason, v_post_type1);
	ELSIF v_acc_id IS NOT NULL AND v_ra_type = '3300' AND v_reason = '3312'-- Duplicate for BOS order
	THEN
		v_post_id	:= get_rule_value (v_reason, v_post_type3);
	ELSE
		v_post_id	:= get_rule_value (v_reason, v_post_type3);
		--
		v_fs_upd_fl := 'Y';
	END IF;
	
	v_rule_value := NVL(get_rule_value (v_ord_typ, 'SHOW-ICT-CREDIT'),'N');
	
	--PMT-32995 Duplicate Orders on Bill Only Orders are transacting like Bill and Ship Orders 
	-- check the order type is duplicate order (2522) , inside the if condition get the order attribute value
       IF v_ord_typ = '2522'
       
       THEN
          v_org_ord_type := NVL(get_order_attribute_value(v_parent_ord_id,'101720'),'-999');
	
       END IF;
	FOR rad_val IN cur_credit
	LOOP
		--
		v_item_type := rad_val.itype;
		--
		IF v_ra_type = '3300' AND v_item_type = 50301 AND p_type = 'R'
		THEN
			v_post_id	:= 48068;
			v_inv_fl	:= 0;
			v_fs_upd_fl := 'N';
			
		  ELSIF v_ra_type = '3300' AND (v_rule_value = 'Y') AND p_type = 'R'
		THEN
			/* No need to increase the inventory while returning a Bill only sales consignment order 
			 * 2532: Bill Only Sales consignment; 3300: Sales Items
			 * 26240234: Install (same as Bill only CN)
			*/
			IF ( v_ord_typ != 2521 )
			THEN
			v_inv_fl	:= 0;
			-- FS warehouse
			v_fs_sign := 4301;       --Bill and Ship Sales RA - Field Sales Net Quantity upadted (PMT-31040)
			END IF;
			v_fs_upd_fl := 'Y';
			v_post_id	:= 4891;
			
		ELSIF v_ra_type = '3300' AND v_item_type = 50301 AND p_type = 'CR'
		THEN
			v_post_id	:= 48065;
			v_inv_fl	:= 0;
		ELSIF v_ra_type = '3308' AND p_type = 'R'  -- Code Added for Inhouse Item Return Direct Posting 
		THEN 
			v_post_id	:= 48069;
			v_inv_fl	:= 1;
		ELSIF v_ra_type = '3308' AND p_type = 'CR'  -- Code Added for Inhouse Item Return Reverse Posting 
		THEN 
			v_post_id	:= 4821;
			v_inv_fl	:= 1;
		ELSIF p_type = 'ICR'
		THEN
			v_post_id	:= 48097;	-- ICT COGS to ICT Dist
	   --PMT-32995 Duplicate Orders on Bill Only Orders are transacting like Bill and Ship Orders 	
		--if condition as re type is 3300 and type as R or CR and parent order type as Bill only loaner or Bill only sales consignment
           
			ELSIF v_ra_type = '3300' AND (p_type = 'R' OR p_type = 'CR') AND (v_org_ord_type = 2530 OR  v_org_ord_type = 2532)
        THEN
        v_inv_fl := 0;	
        
		ELSE
			v_inv_fl	:= 1;
		END IF;
		-- to increase the field sales net Qty
		IF v_reason = '3317' AND v_ord_typ = 2530 
		THEN
			-- FS warehouse
			v_fs_sign := 4301;
		ELSIF 	v_ord_typ = 2530 AND v_reason <> '3317'
		THEN
			v_fs_upd_fl := 'N';
		END IF;
--
		-- to check the reverse posting.
		v_repost_id := get_rule_value (v_ord_typ ||'-'|| v_item_type, 'SALES_RA_REVERSE');
		-- Direct orders changes.
		IF v_order_returns_post_id IS NOT NULL
		THEN
			v_fs_upd_fl := 'N';
			v_post_id := v_order_returns_post_id;
			v_repost_id := NULL;
			-- new posting type (5000900) should be used in OUS distributor order (102080)and return posting.
					IF v_ord_typ= '102080' THEN
			 			SELECT get_rule_value (C101_PARTY_INTRA_MAP_ID, 'ICTACCOUNTMAP') into V_ICTACCID FROM T701_DISTRIBUTOR WHERE C701_DISTRIBUTOR_ID = NVL(v_ord_dist_id,v_dist_id);
						--  PC-4299-ous-distributor-orders
						v_post_id := NVL(get_rule_value (V_ICTACCID, 'RETURNOUSDIST'), get_rule_value ('OUS_ORDER_RETURN', 'DEFAULT_POSTING_TYPE'));
						
					END IF;		
			
		END IF;
		--
		IF v_inv_fl > 0
		THEN
			gm_cm_sav_partqty (rad_val.ID,
                               (rad_val.qty * v_sign),
                               p_id,
                               p_updated_by,
                               90812, -- returns_hold Qty
                               v_sign_action,  -- Plus
                               4312   -- Return
                               );
             
             --Enabling lot tracking for returns from the orders (bill and ship, hold and intercompany) - PMT-29682
             SELECT DECODE(v_ra_type,'3300',NVL(get_rule_value(v_ord_typ,'LOTINCORDERTYPE'),'N'),'N') INTO v_order_fl FROM DUAL;
             IF v_ra_type = '3302' OR v_ra_type = '3304' OR v_ord_typ = '2521' OR v_order_fl = 'Y' --Consignment - Items && bill and ship, hold and intercompany orders
			 THEN
			 gm_pkg_op_lot_track.gm_lot_track_main(rad_val.ID,
                               (rad_val.qty * -1),
                               p_id,
                               p_updated_by,
                               4000339, -- FS  Qty
                               4302,-- Minus
                               4312   -- Return
                               );
                               
        	gm_pkg_op_lot_track.gm_lot_track_main(rad_val.ID,
                               rad_val.qty,
                               p_id,
                               p_updated_by,
                               90812, -- returns_hold Qty
                               4301,-- PLUS
                               4312   -- Return                               
                               );
			 
			 END IF;
		END IF;

		IF p_type = 'CR'
		THEN
			UPDATE t507_returns_item
			   SET c507_status_fl = 'R'
			 WHERE c205_part_number_id = rad_val.ID AND c506_rma_id = p_id;
			 
			--When ever the child is updated/Inserted, the parent table has to be updated ,so ETL can Sync it to GOP.
			UPDATE t506_returns
			   SET C506_LAST_UPDATED_BY = p_updated_by, 
			       C506_LAST_UPDATED_DATE = SYSDATE
			WHERE C506_rma_Id = p_id;

			INSERT INTO t507_returns_item
						(c507_returns_item_id, c507_item_qty, c507_item_price, c205_part_number_id, c506_rma_id
					   , c507_status_fl, c901_type
						)
				 VALUES (s507_return_item.NEXTVAL, rad_val.qty, rad_val.price, rad_val.ID, p_id
					   , 'Q', rad_val.itype
						);
		END IF;

		--
		IF v_post_id IS NULL
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','323','');
		END IF;
		
		-- if reverse posting then - 4819 (CN return)
		IF v_repost_id IS NOT NULL
		THEN
			--
			--OUS distributor does not required inventory 
			SELECT DECODE (v_add_post_fl,'Y',NULL,'Y') INTO v_fs_repost_fl FROM DUAL;
			--
			gm_save_ledger_posting (v_repost_id
								  , CURRENT_DATE
								  , rad_val.ID
								  , v_party_id
								  , p_id
								  , rad_val.qty
								  , NULL
								  , p_updated_by
								   );
		END IF;
		
		gm_save_ledger_posting (v_post_id
							  , CURRENT_DATE
							  , rad_val.ID
							  , v_party_id
							  , p_id
							  , rad_val.qty
							  , NULL
							  , p_updated_by
							  , v_destination_cmpid
							  , v_owner_cmpid
							   );
	--
	END LOOP;
   -- to update the Warehouse
	IF v_fs_upd_fl = 'Y' AND v_warehouse_ref_id IS NOT NULL 
	THEN
		--102908 Return
		-- 4301 Plus
		-- 102900 - Consignment
		gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv (p_id, 'T506', v_warehouse_ref_id, 102900, v_fs_sign, 102908, p_updated_by, v_account_warehouse_type) ;
	END IF;
	--
	IF v_fs_repost_fl = 'Y'
	THEN
		--102908 Return
		-- 4301 Plus
		-- 102900 - Consignment
		gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv (p_id, 'T506', v_warehouse_ref_id, 102900, 4301, 102908, p_updated_by, v_account_warehouse_type) ;
	END IF;
END gm_update_inventory_returns;
/
