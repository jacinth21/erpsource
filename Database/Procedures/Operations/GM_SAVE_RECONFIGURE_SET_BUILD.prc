CREATE OR REPLACE PROCEDURE GM_SAVE_RECONFIGURE_SET_BUILD
(
	p_ConsignId	IN	t504_consignment.c504_consignment_id%TYPE,
	p_SetId		IN	t504_consignment.c207_set_id%TYPE,
	p_ReloadId	IN	t504_consignment.c504_consignment_id%TYPE,
	p_Action	IN	VARCHAR2,
	p_message	OUT	VARCHAR2
)
AS
/*****************************************************************************************************
  * This Procedure is to reconfigure the set list if  the Set Master was changed 
    and also to reload control numbers from Master CN's -- Added 4/27/2007 James
  * Author : D James
  * Date   : Modified 4/27/07
******************************************************************************************************/
v_price VARCHAR2(20);
v_Id NUMBER;
--
CURSOR pop_val IS
	SELECT C.C205_PART_NUMBER_ID  ID, C.C208_SET_QTY QTY
	FROM  T504_CONSIGNMENT A, T208_SET_DETAILS C  
	WHERE A.C207_SET_ID = p_SetId AND A.C207_SET_ID = C.C207_SET_ID 
	AND A.C504_CONSIGNMENT_ID = p_ConsignId
	AND C.C208_SET_QTY <> 0  AND C.C208_INSET_FL = 'Y'  
	AND C.C205_PART_NUMBER_ID NOT IN (
		   SELECT C205_PART_NUMBER_ID 
		   FROM  T505_ITEM_CONSIGNMENT 
		   WHERE C504_CONSIGNMENT_ID = p_ConsignId);
--
CURSOR pop_val_remove IS
	SELECT C.C205_PART_NUMBER_ID  ID, C.C208_SET_QTY QTY
	FROM  T504_CONSIGNMENT A, T208_SET_DETAILS C  
	WHERE A.C207_SET_ID = p_SetId AND A.C207_SET_ID = C.C207_SET_ID 
	AND A.C504_CONSIGNMENT_ID = p_ConsignId
	AND C.C208_SET_QTY = 0  AND C.C208_INSET_FL = 'N' 
	AND C.C205_PART_NUMBER_ID IN (
		   SELECT C205_PART_NUMBER_ID 
		   FROM  T505_ITEM_CONSIGNMENT 
		   WHERE C504_CONSIGNMENT_ID = p_ConsignId);
--
CURSOR pop_val_reload IS
	SELECT	c205_part_number_id ID, c505_item_qty QTY,
		c505_control_number CNUM, c505_item_price PRICE
	FROM	t505_item_consignment
	WHERE	c504_consignment_id = p_ReloadId
	AND	c505_void_fl IS NULL;
--
BEGIN
	IF p_Action = 'Reconfigure' THEN
		FOR rad_val IN pop_val
		LOOP
			 SELECT S504_CONSIGN_ITEM.nextval INTO v_Id FROM dual;
			 v_price := GET_PART_PRICE(rad_val.ID,'E');
			 INSERT INTO T505_ITEM_CONSIGNMENT (
					C505_ITEM_CONSIGNMENT_ID, C504_CONSIGNMENT_ID,
					C205_PART_NUMBER_ID, C505_CONTROL_NUMBER,
					C505_ITEM_QTY, C505_ITEM_PRICE
			 )VALUES(
					 v_Id,p_ConsignId,
					 rad_val.ID,'',
					 rad_val.QTY,v_price);
		END LOOP;
		--			
		FOR rad_val1 IN pop_val_remove
		LOOP
			DELETE T505_ITEM_CONSIGNMENT 
			WHERE	C205_PART_NUMBER_ID	= rad_val1.ID 
			AND C504_CONSIGNMENT_ID = p_ConsignId;
		END LOOP;
		
		 --When ever the detail is updated, the consignment table has to be updated ,so ETL can Sync it to GOP.
		 UPDATE t504_consignment
			SET c504_last_updated_date = SYSDATE
		  WHERE c504_consignment_id = p_ConsignId;
		  
	 ELSIF p_Action = 'Reload' THEN
		DELETE t505_item_consignment 
		WHERE  c504_consignment_id = p_ConsignId;
		--
		FOR rad_val IN pop_val_reload
		LOOP
			INSERT INTO t505_item_consignment (
					c505_item_consignment_id, c504_consignment_id,
					c205_part_number_id, c505_control_number,
					c505_item_qty, c505_item_price
			 )VALUES(
					 s504_consign_item.nextval,p_ConsignId,
					 rad_val.ID,rad_val.CNUM,
					 rad_val.QTY,rad_val.PRICE);
		END LOOP;
		
		 --When ever the detail is updated, the consignment table has to be updated ,so ETL can Sync it to GOP.
		 UPDATE t504_consignment
			SET c504_last_updated_date = SYSDATE
		  WHERE c504_consignment_id = p_ConsignId;
		  
	 END IF;
END GM_SAVE_RECONFIGURE_SET_BUILD;
/