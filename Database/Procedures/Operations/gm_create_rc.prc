/* Formatted on 2010/09/21 11:42 (Formatter Plus v4.8.0) */
-- @"C:\database\Procedures\Operations\gm_create_rc.prc"

CREATE OR REPLACE PROCEDURE gm_create_rc
AS
--
   v_msg       VARCHAR2 (1000);
   v_rc_id     VARCHAR2 (20);
   v_post_id   VARCHAR2 (20);

   --
   CURSOR cur_wos
   IS
      SELECT t401.c301_vendor_id vendorid, t402.c205_part_number_id pnum,
             t402.c402_work_order_id woid, t402.c402_qty_ordered qty,
             t301.c301_rm_invoice_fl rm_invoice_fl,
             t402.c402_split_percentage split_pert,
             t402.c402_split_cost split_cost,
             t401.c1900_company_id comp_id
        FROM t401_purchase_order t401, t402_work_order t402,
             t301_vendor t301
       WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
         AND t401.c401_void_fl IS NULL
         AND t402.c402_void_fl IS NULL
         AND TRUNC (t401.c401_purchase_ord_date) <= TRUNC (SYSDATE)
         --     - get_rule_value (t401.c301_vendor_id, 'Sec199_Rule')     --14
         AND t301.c301_vendor_id = t401.c301_vendor_id
         AND t402.c402_posting_fl IS NULL
         AND t402.c402_split_percentage IS NOT NULL;
--
BEGIN
   --new part 100.000 raw material cost
  -- gm_procedure_log ('testing1', 'test');

--   commit;
   FOR var_wos IN cur_wos
   LOOP
      IF (var_wos.rm_invoice_fl = 'Y')
      THEN
         --create RC
         gm_create_dhr (var_wos.woid,
                        var_wos.vendorid,
                        '100.000',                             --var_wos.pnum,
                        'NOC#',
                        var_wos.qty,
                        TO_CHAR (SYSDATE, 'mm/dd/yyyy'),
                        '',
                        TO_CHAR (SYSDATE, 'mm/dd/yyyy'),
                        'Invoice Receipt for raw material',
                        'RC',
                        30301,
                        v_rc_id,
                        v_msg
                       );
-- v_rc_id, v_msg are outputs
-- v_WO,p_VendId,v_PNum,v_Control,v_Qty,v_ManfDate,p_PackSlip,p_RecDate,p_Comments,p_Type,p_UserId,v_pdt_id,v_msg
      END IF;

      gm_save_ledger_posting
         (48116,                                                  --v_post_id,
          SYSDATE,
          '100.000',                                           --var_wos.pnum,
          var_wos.vendorid,
          var_wos.woid,
          var_wos.qty,
         (get_currency_conversion (get_vendor_currency (var_wos.vendorid),
                                       get_comp_curr(var_wos.comp_id),
                                       SYSDATE,
                                       NVL (var_wos.split_cost, 0)
                                      )
             ),
          30301
         );

      UPDATE t402_work_order t402
         SET t402.c402_posting_fl = 'Y'
       WHERE t402.c402_work_order_id = var_wos.woid;
   END LOOP;
END gm_create_rc;
/
