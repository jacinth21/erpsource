/* Formatted on 2013/11/05 (Formatter Plus v4.8.0) */
--@"C:\Database\Procedures\Operations\gm_op_fch_ln_req_fl.prc";

CREATE OR REPLACE PROCEDURE gm_op_fch_ln_req_fl (
	p_req_det_id	IN		t525_product_request.c525_product_request_id%TYPE
  , p_flag			OUT		VARCHAR2
)
AS
	v_count		NUMBER;
BEGIN
	
	SELECT  count(1) 
		INTO v_count 
	FROM t526_product_request_detail 
	WHERE c526_product_request_detail_id = p_req_det_id 
	and c526_status_fl = '10'
	AND c526_void_fl IS NULL;
--	
	IF v_count > 0 THEN
		p_flag := 'Y';
	ELSE
		p_flag := 'N';
	END IF;
--	
END gm_op_fch_ln_req_fl;
/
