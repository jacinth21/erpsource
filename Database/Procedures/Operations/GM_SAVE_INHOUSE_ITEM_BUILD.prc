/* Formatted on 2010/12/17 14:32 (Formatter Plus v4.8.0) */
--@"C:\database\Procedures\Operations\gm_save_inhouse_item_build.prc";

CREATE OR REPLACE PROCEDURE gm_save_inhouse_item_build (
	p_consignid   IN	   t412_inhouse_transactions.c412_inhouse_trans_id%TYPE
  , p_str		  IN	   VARCHAR2
  , p_userid	  IN	   t412_inhouse_transactions.c412_last_updated_by%TYPE
  , p_msg		  OUT	   VARCHAR2
  , p_action      IN       VARCHAR2 DEFAULT NULL
)
AS
--
--
	v_strlen	   NUMBER := NVL (LENGTH (p_str), 0);
	v_string	   CLOB := p_str;
	v_pnum		   VARCHAR2 (20);
	v_qty		   NUMBER;
	v_control	   T413_INHOUSE_TRANS_ITEMS.C413_CONTROL_NUMBER%TYPE;
	v_price 	   NUMBER (15, 2);
	v_client_pnum  VARCHAR2 (20);
	v_substring    VARCHAR2 (1000);
	v_msg		   VARCHAR2 (1000);
	e_others	   EXCEPTION;
	v_id		   NUMBER;
	v_stock 	   NUMBER;
	v_bstock	   NUMBER;
	v_type		   NUMBER;
	v_aqty		   NUMBER;
	v_repackqty	   NUMBER;
	v_statusfl	   VARCHAR2 (1);
	v_refid 	   t412_inhouse_transactions.c412_ref_id%TYPE;
	v_ref_type     t413_inhouse_trans_items.c901_ref_type%TYPE;
	v_pndnstring   CLOB := p_str;
	v_purpose	   t412_inhouse_transactions.c412_inhouse_purpose%TYPE;
	v_comments     CLOB;
	v_temp_comments VARCHAR2(4000);
	v_length NUMBER;
	v_comma_count	  NUMBER;
	v_fg_flag      CHAR(1);
	v_bulk_flag    CHAR(1);
	v_flag			VARCHAR2(1) := 'N';
	v_err_cnt			NUMBER;
	v_comp_id		t1900_company.c1900_company_id%TYPE;
	v_excl_txn_cnt	NUMBER;
	v_comp_flag VARCHAR2(1);
	v_suffix		VARCHAR2(5);
--
BEGIN
-- IF v_strlen > 0
-- THEN
	  --
	 SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_comp_id FROM DUAL;
	 
	DELETE		t413_inhouse_trans_items
		  WHERE c412_inhouse_trans_id = p_consignid;

	--
	SELECT c412_type, c412_status_fl, c412_ref_id, c412_inhouse_purpose
	  INTO v_type, v_statusfl, v_refid, v_purpose
	  FROM t412_inhouse_transactions
	 WHERE c412_inhouse_trans_id = p_consignid;

	 v_temp_comments := get_rule_value ('RE-DESIG PART COMMENTS',v_type);	 

	WHILE INSTR (v_string, '|') <> 0
	LOOP
		v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
		v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
		--count how many commas in each substring
	    SELECT trim( LENGTH( v_substring ) ) - trim( LENGTH( TRANSLATE( v_substring, 'A,', 'A' ) ) )
	    INTO v_comma_count
		FROM dual;
		v_pnum		:= NULL;
		v_qty		:= NULL;
		v_control	:= NULL;
		v_price 	:= NULL;
		v_client_pnum := NULL;		
		v_refid  := NULL;
		v_ref_type  := NULL;
		v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
		v_qty		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
		v_control	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
		IF v_comma_count != 3
    	THEN
		v_client_pnum 	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
		END IF;
		-- if ref id and ref type is available, get it from the string
		IF SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1) IS NOT NULL AND v_comma_count != 3
		THEN
			v_price 	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_refid 	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_ref_type	:= v_substring;
		ELSE
			v_price		:= v_substring;
		END IF;
			
		--
		v_comments := v_comments|| v_temp_comments || ' '||v_pnum || ',';
		
		SELECT	   NVL(GET_QTY_IN_INVENTORY(p_consignid,'90800',c205_part_number_id),0) stock, NVL(GET_QTY_IN_INVENTORY(p_consignid,'90814',c205_part_number_id),0) bstock
				 , (get_partnumber_qty (c205_part_number_id, 90802) - get_qty_in_transaction_rm (c205_part_number_id)) aqty
				 , get_qty_in_repack_partredesig(c205_part_number_id,v_control) repackqty 																								   
			  INTO v_stock, v_bstock
				 , v_aqty, v_repackqty 
			  FROM t205_part_number
			 WHERE c205_part_number_id = v_pnum
		FOR UPDATE;

		--adding 2 types more - Move from Shelf to Loaner Set and
		IF	  (v_stock < v_qty AND (v_type = 4117 OR v_type = 40054 OR v_type = 50155 OR v_type = 50154))
		   OR (v_bstock < v_qty AND v_type = 4118)
		   OR (v_aqty < v_qty AND (v_type = 40053 OR v_type = 40051 OR v_type = 40052))
		   OR (v_repackqty < v_qty AND v_type = 103932) 
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','317',v_pnum); 
		END IF;
      
			--adding 2 types more - Move from Shelf to Loaner Set and  
	      IF    (v_stock < v_qty AND (v_type = 4117 OR v_type = 40054 or v_type =50155 or v_type = 50154))
	         OR (v_bstock < v_qty AND v_type = 4118)
	         OR (v_aqty < v_qty AND (v_type = 40053 OR v_type = 40051 OR v_type = 40052))
	      THEN
	         GM_RAISE_APPLICATION_ERROR('-20999','317',v_pnum);
	      END IF;
	      -- getting transaction type from rule and validating required qty with stock qty
	      v_fg_flag := NVL(GET_RULE_VALUE(v_type,'INV_SHELF'),'N');
	      v_bulk_flag := NVL(GET_RULE_VALUE(v_type,'INV_BULK'),'N');	      
	      	
	      IF ((v_stock < v_qty AND v_fg_flag = 'Y' ) OR (v_bstock < v_qty AND v_bulk_flag = 'Y'))
	      THEN
	         GM_RAISE_APPLICATION_ERROR('-20999','317',v_pnum);
	      END IF;
	      
			gm_pkg_op_inhouse_master.gm_sav_inhouse_trans_items (p_consignid, v_pnum, v_qty, v_control, v_price ,v_client_pnum,v_refid,v_ref_type);
		
			 SELECT COUNT(1) INTO v_excl_txn_cnt FROM t906_rules
			  WHERE c906_rule_id = v_type
			  	AND c906_rule_grp_id = 'TXN_RULE'
			  	AND c906_void_fl IS NULL;
	  	
		-- Check whether the validation is applicable for the part number or not, 104480: Lot tracking required ?
			v_flag := get_part_attr_value_by_comp(v_pnum,'104480',v_comp_id);
			-- Get the error count from the t5055 table to update the lot status based on that
		  SELECT count(1) INTO v_err_cnt
			FROM t413_inhouse_trans_items
		  WHERE c412_inhouse_trans_id    = p_consignid
			AND c413_error_dtls IS NOT NULL
			AND c413_void_fl    IS NULL;
   
		   IF p_action = 'ReleaseControl' AND v_err_cnt = 0 AND v_flag = 'Y' AND v_excl_txn_cnt > 0 THEN
				UPDATE t2550_part_control_number
				SET c901_lot_controlled_status = '105008' --Controlled
				  , c2550_last_updated_by = p_userid
				  , c2550_last_updated_date = CURRENT_DATE
				WHERE c205_part_number_id = v_pnum
					AND c2550_control_number = v_control;
			END IF;
			
	END LOOP;	
	
	IF(NVL(GET_RULE_VALUE('RE-DESIG PART',v_type),'N') = 'Y') then
	Select Length (v_comments)-1 into v_length From Dual;
	IF v_length > 3995
	THEN
		v_length := 3995;
		v_suffix := '...';
	END IF;
		v_comments := SUBSTR (v_comments,0,v_length) ;
		UPDATE t412_inhouse_transactions
		 SET c412_comments = v_comments || v_suffix
		   , c412_last_updated_by = p_userid
		   , c412_last_updated_date = SYSDATE
       WHERE c412_inhouse_trans_id      = p_consignid;
    END IF;
	 --Whenever the detail is updated, the Order table has to be updated ,so ETL can Sync it to GOP.  
 	  UPDATE t501_order
		 SET c501_last_updated_by = p_userid
		   , c501_last_updated_date = SYSDATE
       WHERE c501_order_id      = p_consignid;
       

/*This txn Pending shipping to shelf (100406) has moved to new package gm_pkg_op_item_control_txn
	IF v_refid IS NOT NULL AND v_statusfl = '3' AND v_type = 100406
	THEN
		gm_update_pendship_ord (v_refid, v_purpose, v_pndnstring, p_userid, p_msg);
	END IF;
--*/
END gm_save_inhouse_item_build;
/