CREATE OR REPLACE PROCEDURE GM_INITIATE_SET_RETURN
(
 	   p_DistId	  			IN VARCHAR2,
	   p_SetId 			    IN T506_RETURNS.C207_SET_ID%TYPE,
	   p_Reason				IN T506_RETURNS.C506_REASON%TYPE,
	   p_Comments			IN T506_RETURNS.C506_COMMENTS%TYPE,
	   p_UserId				IN T506_RETURNS.C506_CREATED_BY%TYPE,
	   p_ExpDate			IN VARCHAR2,
	   p_EmpName			IN T101_USER.C101_USER_ID%TYPE,
	   p_RAId				OUT VARCHAR2,
	   p_message 			OUT  VARCHAR2
)
AS
v_RA_id NUMBER;
v_Id NUMBER;
v_string VARCHAR2(20);
v_id_string VARCHAR2(20);
v_price VARCHAR2(20);
v_date VARCHAR2(10);
v_empName NUMBER;
v_company_id  t1900_company.c1900_company_id%TYPE;
v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
CURSOR pop_val IS
	   	SELECT C205_PART_NUMBER_ID ID, C208_SET_QTY QTY
		FROM
			 T208_SET_DETAILS
		WHERE C207_SET_ID = p_SetId
		--AND C208_SET_QTY <> 0
		ORDER BY C205_PART_NUMBER_ID;
	
 BEGIN
	   
      SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
        INTO v_company_id, v_plant_id
        FROM DUAL;

 -- Checking if EmpName is 0. If it is Zero, make it as NULL
 IF p_EmpName = 0
 THEN
 v_empName := NULL;
 ELSE
 v_empName := p_EmpName;
 END IF;
 
	 	  SELECT S506_RETURN.NEXTVAL INTO v_RA_id FROM dual;
			  IF LENGTH(v_RA_id) = 1
			  	 THEN
				 v_id_string := '0' || v_RA_id;
				 ELSE
				 v_id_string := v_RA_id;
			  END IF;
			  SELECT 'GM-RA-' || v_id_string INTO v_string FROM dual;
		  	  p_RAId := v_string;
			  IF LENGTH(p_ExpDate) < 1 THEN
			  	  v_date := CURRENT_DATE+4;
			  ELSE
			  	 v_date := p_ExpDate;
			  END IF;
			  IF p_DistId = '01' THEN
			      INSERT INTO T506_RETURNS (
				  		C506_RMA_ID,
						C704_ACCOUNT_ID,
						C207_SET_ID,
						C506_TYPE,
						C506_REASON,
						C506_COMMENTS,
						C506_EXPECTED_DATE,
						C506_STATUS_FL,
						C506_CREATED_BY,
						C506_CREATED_DATE,
						C101_USER_ID,
						c1900_company_id,
						c5040_plant_id
				  )
				  VALUES (
				   		v_string,
						p_DistId,
						p_SetId,
						3301,
						p_Reason,
						p_Comments,
						v_Date,
						'0',
						p_UserId,
						CURRENT_DATE,
						v_empName,
						v_company_id,
						v_plant_id
				  );
				ELSE
				      INSERT INTO T506_RETURNS (
					  		C506_RMA_ID,
							C701_DISTRIBUTOR_ID,
							C207_SET_ID,
							C506_TYPE,
							C506_REASON,
							C506_COMMENTS,
							C506_EXPECTED_DATE,
							C506_STATUS_FL,
							C506_CREATED_BY,
							C506_CREATED_DATE,
							c1900_company_id,
							c5040_plant_id
					  )
					  VALUES (
					   		v_string,
							p_DistId,
							p_SetId,
							3301,
							p_Reason,
							p_Comments,
							v_Date,
							'0',
							p_UserId,
							CURRENT_DATE,
							v_company_id,
							v_plant_id
					  );
				END IF;
			 BEGIN
			  	 FOR rad_val IN pop_val
				 	LOOP
						 SELECT S507_RETURN_ITEM.nextval INTO v_Id FROM dual;
			  			 v_price := GET_PART_PRICE(rad_val.ID,'E');
				 		 INSERT INTO T507_RETURNS_ITEM (
				 		 		C507_RETURNS_ITEM_ID, C506_RMA_ID,
				 		 		C205_PART_NUMBER_ID, C507_CONTROL_NUMBER,
				 		 		C507_ITEM_QTY, C507_ITEM_PRICE
				 		 )VALUES(
				 		 		 v_Id,p_RAId,
						 		 rad_val.ID,' ',
						 		 rad_val.QTY,v_price);
					END LOOP;
			 END;
		COMMIT;
              RETURN;
END GM_INITIATE_SET_RETURN;
/

