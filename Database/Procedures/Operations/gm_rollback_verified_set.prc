--@"C:\Database\Procedures\Operations\gm_rollback_verified_set.prc";
/*
 * 	DESCRIPTION: This procedure is used to rollback the verified sets to "WIP" status
 * 				 and it will revert the postings as well. This will rollback the Request 
 * 				 and the Consign to the "WIP" Status.
 * 	1.	WIP status for Consign is 1
 *  2.	WIP status for Request is 15
 * 
 * Author: Rajkumar J
 */
CREATE OR REPLACE PROCEDURE gm_rollback_verified_set
(
 p_ConsignId IN t505_item_consignment.c504_consignment_id%TYPE,
 p_user_id IN t504_consignment.c504_last_updated_by%TYPE
 )
AS
v_msg  VARCHAR2(100);
v_req_id VARCHAR2(100);
BEGIN
	
	SELECT C520_REQUEST_ID into v_req_id 
		FROM T504_CONSIGNMENT 
		WHERE C504_CONSIGNMENT_ID = p_ConsignId;
		
-- Rollback the Request to WIP Status
UPDATE T520_REQUEST  
	SET C520_STATUS_FL='15',
		C520_LAST_UPDATED_BY=p_user_id,
		C520_LAST_UPDATED_DATE=SYSDATE  
	WHERE 
		C520_REQUEST_ID=v_req_id 
		AND C520_VOID_FL IS NULL;
		
-- Rollback the Consign ID to WIP Status
 UPDATE T504_CONSIGNMENT
 SET  C504_STATUS_FL  = '1',
   C504_VERIFY_FL  = '0',
   C504_VERIFIED_BY = '',
   C504_VERIFIED_DATE = '',
   C504_UPDATE_INV_FL = '',
   C504_LAST_UPDATED_BY=p_user_id,
   C504_LAST_UPDATED_DATE=SYSDATE    
 WHERE C504_CONSIGNMENT_ID = p_ConsignId
	   AND C504_VOID_FL IS NULL;
 --
 -- Rollback is now Revert the Posting
 
  GM_UPDATE_INVENTORY(p_ConsignId,'CROLL','PLUS',p_user_id, v_msg);
 
--
END gm_rollback_verified_set;
/

