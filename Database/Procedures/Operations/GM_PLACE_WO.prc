/* Formatted on 2010/09/29 11:54 (Formatter Plus v4.8.0) */
-- @"C:\database\Procedures\Operations\gm_place_wo.prc"

CREATE OR REPLACE PROCEDURE gm_place_wo (
   p_po         IN       t402_work_order.c401_purchase_ord_id%TYPE,
   p_partnum    IN       t402_work_order.c205_part_number_id%TYPE,
   p_rev        IN       t402_work_order.c402_rev_num%TYPE,
   p_vendorid   IN       t402_work_order.c301_vendor_id%TYPE,
   p_qty        IN       t402_work_order.c402_qty_ordered%TYPE,
   p_cost       IN       t402_work_order.c402_cost_price%TYPE,
   p_critfl     IN       t402_work_order.c402_critical_fl%TYPE,
   p_dhrid      IN       t402_work_order.c408_dhr_id%TYPE,
   p_farfl      IN       t402_work_order.c402_far_fl%TYPE,
   p_validateFl IN       t402_work_order.c402_validation_fl%TYPE,
   p_lotCount   IN       NUMBER,
   p_priceid    IN       t402_work_order.c405_pricing_id%TYPE,
   p_type       IN       t401_purchase_order.c401_type%TYPE,
   p_userid     IN       t402_work_order.c402_created_by%TYPE,
   p_message    OUT      VARCHAR2
)
AS
--
   v_wo_id                NUMBER;
   v_string               VARCHAR2 (20);
   v_id_string            VARCHAR2 (20);
   v_stock                NUMBER;
   v_newstock             NUMBER;
   v_subcompfl            CHAR (1);
   v_sterfl               CHAR (1);
   v_pdtclass             NUMBER;
   v_dhr_qty              NUMBER;
   v_dhr_wo_qty           NUMBER;
   v_part_qty             NUMBER;
   v_vendor_part_pert     NUMBER;
   v_part_material        NUMBER;
   v_part_ma_validate     CHAR (1);
   v_part_prod_validate   CHAR (1);
   v_split_cost           NUMBER;
   v_part_product         NUMBER;
   v_rm_inv_fl            CHAR (1);
   v_qty 				  NUMBER;
   v_lot_count    NUMBER := p_lotCount;
   v_company_id  t1900_company.c1900_company_id%TYPE;
   v_lead_wo_time		  DATE;

   --
   CURSOR pop_val
   IS
      SELECT t205.c205_part_number_id subid, t205.c205_rev_num rev
        FROM t205a_part_mapping t205a, t205_part_number t205
       WHERE t205a.c205_from_part_number_id = p_partnum
         AND t205a.c205_to_part_number_id = t205.c205_part_number_id
         AND t205a.c205a_void_fl is null;  ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
--
BEGIN
   --
   SELECT c205_product_class
     INTO v_pdtclass
     FROM t205_part_number
    WHERE c205_part_number_id = p_partnum;
   -- To get the PO company id
	SELECT c1900_company_id
   		INTO v_company_id
   	FROM t401_purchase_order
  	WHERE c401_purchase_ord_id = p_po
    AND c401_void_fl   IS NULL;
   --
   UPDATE t405_vendor_pricing_details
      SET c405_lock_fl = 'Y'
      	, c405_last_updated_by = p_userid
		, c405_last_updated_date = CURRENT_DATE
    WHERE c301_vendor_id = p_vendorid
      AND c205_part_number_id = p_partnum
      AND c405_active_fl = 'Y'
      AND c405_lock_fl = 'N'
      AND c405_void_fl IS NULL;

   IF v_pdtclass = 4030
   THEN
      v_sterfl := '1';
   ELSE
      v_sterfl := NULL;
   END IF;

   v_vendor_part_pert := get_vendor_split_percentage (p_vendorid);

   IF v_vendor_part_pert IS NOT NULL
   THEN
      v_part_material := get_partnum_material_type (p_partnum);
      v_part_product := get_partnum_product (p_partnum);
      v_part_ma_validate := get_rule_value (v_part_material, 'Globus_199');
      v_part_prod_validate := get_rule_value (v_part_product, 'Globus_199');

      IF (    v_part_ma_validate = 'Y'
          AND v_part_prod_validate = 'Y'
          AND p_type = '3100'
         )
      THEN
         v_split_cost := v_vendor_part_pert * p_cost / 100;
         
         SELECT t301.c301_rm_invoice_fl
           INTO v_rm_inv_fl
			     FROM t301_vendor t301
          WHERE t301.c301_vendor_id = p_vendorid;
      END IF;
   END IF;
   
      -- if part cost zero for PO type 3104(Manufactured FG) and 3105(Manufactured Subcomponent) then raise App Error message(PC-3391)
      IF p_cost = 0 AND (p_type = '3104' OR p_type = '3105') THEN
			raise_application_error (- 20999, 'Please update the price for zero cost part before proceeding');
	  END IF;
		
      IF p_type='3100' AND p_validateFl='Y' AND NVL(p_farfl,'N') <> 'Y' THEN -- only for product(Po type) and validate flag as 'Y'
   
   		IF MOD(p_qty,v_lot_count) <>0  THEN   	   			
   			raise_application_error (-20583, '') ;   
   		END IF;
        v_qty := p_qty / v_lot_count;
      ELSE        
        v_lot_count:=1;
   		v_qty:=p_qty;       
      END IF; 
         FOR lcounter IN 1..v_lot_count
   		LOOP

		 SELECT s402_work.NEXTVAL
		     INTO v_wo_id
		     FROM DUAL;
		
		   --
		   IF LENGTH (v_wo_id) = 1
		   THEN
		      v_id_string := '0' || v_wo_id;
		   ELSE
		      v_id_string := v_wo_id;
		   END IF;
		
		   --
		   SELECT 'GM-WO-' || v_id_string
		     INTO v_string
		     FROM DUAL;
		     
		--Below prc is used to fetch lead time for work order based on part number (PC-3202)
		gm_pkg_wo_lead_time_txn.gm_fch_part_lead_time(p_partnum,p_vendorid,v_lead_wo_time);
		
   INSERT INTO t402_work_order
               (c402_work_order_id, c401_purchase_ord_id,
                c205_part_number_id, c301_vendor_id, c402_qty_ordered,
                c402_cost_price, c402_rev_num, c402_created_by,
                c402_created_date, c402_status_fl, c402_critical_fl,
                c408_dhr_id, c402_sterilization_fl, c402_far_fl,
                c405_pricing_id, c402_split_percentage, c402_split_cost, c402_rm_inv_fl,c402_validation_fl,c1900_company_id,
                c402_wo_due_date,c402_wo_lead_date
               )
        VALUES (v_string, p_po,
                p_partnum, p_vendorid, v_qty,
                p_cost, p_rev, p_userid,
                CURRENT_DATE, '1', p_critfl,
                p_dhrid, v_sterfl, p_farfl,
                p_priceid, v_vendor_part_pert, v_split_cost, v_rm_inv_fl,
                DECODE(p_farfl,'Y',NULL,p_validateFl),v_company_id,
                v_lead_wo_time,v_lead_wo_time

               );
        ----This below prc call is used to time stamp the WO part attribute details   
		gm_pkg_wo_txn.gm_sav_wo_part_info(v_string, p_partnum, p_userid);
   --
   BEGIN
      SELECT NVL(get_partnumber_qty(p_partnum, 90801) ,0)
        INTO v_part_qty
        FROM dual;     
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_part_qty := 0;
   END;

   -- If Qty greater than PO QTY then system will
   -- increase the qty and will update the qty to zero
   IF v_part_qty < v_qty
   THEN
      -- 90801   PO Type
      gm_cm_sav_partqty (p_partnum,
                         (v_qty - v_part_qty),
                         v_string,
                         p_userid,
                         90801,
                         4301,
                         4319
                        );
   END IF;

   -- 90801   PO Type
   gm_cm_sav_partqty (p_partnum,
                      (-1 * v_qty),
                      v_string,
                      p_userid,
                      90801,
                      4302,
                      4319
                     );

   --
   -- The next section checks to see if the part number has associated sub-components
   -- and if it does, then it loads the T403_SUB_WORK_ORDER table with data.
   -- Changed May 13, 2005 for new format of Work Order
   FOR rad_val IN pop_val
   LOOP
      --
      INSERT INTO t403_sub_work_order
                  (c403_sub_work_order_id, c402_work_order_id,
                   c206_sub_asmbly_id, c403_rev_num
                  )
           VALUES (s403_sub_work.NEXTVAL, v_string,
                   rad_val.subid, rad_val.rev
                  );
   --
   END LOOP;
END LOOP;
   --
   -- Updating Inventory (negative) since these are Rework and Sterilization PO's
   IF p_type = '3101'
   THEN            -- For Rework: Updating Qty in Quarantine - Changed July 31
      --
       
        gm_cm_sav_partqty (p_partnum,
                               (p_qty * -1),
                               p_po,
                               p_userid,
                               90813, -- quarantine  Qty
                               4302, -- Minus
                               4314 -- Purchase
                               );

      --
      --   Calling Ledger Posting procedure - changed June 20, 2006
      gm_save_ledger_posting (4825,
                              CURRENT_DATE,
                              p_partnum,
                              p_vendorid,
                              p_po,
                              p_qty,
                              p_cost,
                              p_userid
                             );
   --
   ELSIF p_type = '3102'
   THEN                            -- For Sterilization: Updating Qty in Shelf
      --
      gm_cm_sav_partqty (p_partnum,
                               (p_qty * -1),
                               p_po,
                               p_userid,
                               90800, -- FG  Qty
                               4302, -- Minus
                               4314 -- Purchase
                               );
   --
   END IF;

   --Checking if all parts of the DHR were sent for Sterilization. If yes, then updating flag to 2 so
   -- it doesnt appear in the Sterilization DHR Report
   IF p_dhrid IS NOT NULL
   THEN
      --
      SELECT c408_qty_on_shelf
        INTO v_dhr_qty
        FROM t408_dhr
       WHERE c408_dhr_id = p_dhrid AND c408_void_fl IS NULL;

      --
      SELECT SUM (c402_qty_ordered)
        INTO v_dhr_wo_qty
        FROM t402_work_order
       WHERE c408_dhr_id = p_dhrid AND c402_void_fl IS NULL;

      IF v_dhr_wo_qty = v_dhr_qty
      THEN
         UPDATE t408_dhr
            SET c408_sterile_status_fl = '2'
          WHERE c408_dhr_id = p_dhrid;
      END IF;
   --
   END IF;
END gm_place_wo;
/
