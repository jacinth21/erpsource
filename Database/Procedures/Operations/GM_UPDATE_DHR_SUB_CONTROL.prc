CREATE OR REPLACE PROCEDURE GM_UPDATE_DHR_SUB_CONTROL
(
		  p_DHRId	  IN	T411_SUB_DHR.C408_DHR_ID%TYPE,
 		  p_str 	  IN	VARCHAR2,
		  p_UserId	  IN	T408_DHR.C408_CREATED_BY%TYPE,
		  p_errmsg    OUT	VARCHAR2
)
AS
v_strlen     NUMBER          := NVL(LENGTH(p_str),0);
v_string     VARCHAR2(30000) := p_str;

v_PNum	 	 VARCHAR2(20);
v_SubDHRid	 VARCHAR2(20);
v_CNum		 T411_SUB_DHR.C411_CONTROL_NUMBER%TYPE;
v_ManfDt	 VARCHAR2(20);

v_substring  VARCHAR2(1000);
v_msg	     VARCHAR2(1000);
e_others	 EXCEPTION;
BEGIN
 	IF v_strlen > 0
    THEN
   		WHILE INSTR(v_string,'|') <> 0 LOOP
    		  v_substring := SUBSTR(v_string,1,INSTR(v_string,'|')-1);
    		  v_string    := SUBSTR(v_string,INSTR(v_string,'|')+1);
			  v_PNum	  := NULL;
			  v_SubDHRid  := NULL;
			  v_CNum  	  := NULL;
			  v_ManfDt 	  := NULL;

			  v_PNum 	  := SUBSTR(v_substring,1,INSTR(v_substring,'^')-1);
			  v_substring := SUBSTR(v_substring,INSTR(v_substring,'^')+1);
			  v_SubDHRid  := SUBSTR(v_substring,1,INSTR(v_substring,'^')-1);
			  v_substring := SUBSTR(v_substring,INSTR(v_substring,'^')+1);
			  v_CNum 	  := SUBSTR(v_substring,1,INSTR(v_substring,'^')-1);
			  v_substring := SUBSTR(v_substring,INSTR(v_substring,'^')+1);
			  v_ManfDt 	  := v_substring;

			  UPDATE T411_SUB_DHR
			 	SET
					C411_CONTROL_NUMBER  = v_CNum,
					C411_MANF_DATE = to_date(v_ManfDt,get_rule_value('DATEFMT','DATEFORMAT'))
				WHERE
					C411_SUB_DHR_ID = v_SubDHRid;
		END LOOP;

			UPDATE T408_DHR
			 	SET
					C408_STATUS_FL  = '1',
					C408_LAST_UPDATED_BY = p_UserId,
					C408_LAST_UPDATED_DATE = SYSDATE
				WHERE
					C408_DHR_ID = p_DHRId;

	ELSE
	    v_msg := 'p_str is empty';
		RAISE e_others;
	END IF;
	v_msg := 'Success';
	COMMIT WORK;
    p_errmsg  := v_msg;
EXCEPTION
	  WHEN e_others THEN
	  ROLLBACK WORK;
	  p_errmsg  := v_msg;
	  WHEN OTHERS THEN
	  ROLLBACK WORK;
	  p_errmsg  := SQLERRM;
NULL;
END GM_UPDATE_DHR_SUB_CONTROL;
/

