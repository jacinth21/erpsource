/* Formatted on 2009/02/10 15:16 (Formatter Plus v4.8.0) */
--@"c:\database\procedures\operations\gm_save_reprocess_returns_item.prc";

CREATE OR REPLACE PROCEDURE gm_save_reprocess_returns_item (
	p_id	   IN		t504_consignment.c504_consignment_id%TYPE
  , p_str	   IN		VARCHAR2
  , p_errmsg   OUT		VARCHAR2
)
AS
--
	v_strlen	   NUMBER := NVL (LENGTH (p_str), 0);
	v_string	   VARCHAR2 (30000) := p_str;
	v_pnum		   VARCHAR2 (20);
	v_qty		   NUMBER (4);
	v_control	   t505_item_consignment.c505_control_number%TYPE;
	v_stock_qty    NUMBER (4);
	v_substring    VARCHAR2 (1000);
	v_msg		   VARCHAR2 (1000);
	e_others	   EXCEPTION;
	v_type		   NUMBER (4);
	v_plant_id     T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
	--
--
BEGIN
	SELECT  get_plantid_frm_cntx() INTO v_plant_id FROM DUAL;
--1.	From p_Id, get the type of the transaction (eg from BuiltSet to Shelf)
	SELECT t412.c412_type
	  INTO v_type
	  FROM t412_inhouse_transactions t412
	 WHERE t412.c412_inhouse_trans_id = p_id;

--
	IF v_strlen > 0
	THEN
		--
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			--
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_pnum		:= NULL;
			v_control	:= NULL;
			v_qty		:= NULL;
			v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_control	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_qty		:= TO_NUMBER (v_substring);

			/*2.	Depending on the inventory component (Stock or Bulk or Quarantine) get their corresponding value and validate with v_Qty.
					If v_Qty is greater, then throw error
			*/
			IF v_type = 4117   --4117 : Bulk
			THEN
				SELECT	   get_qty_in_bulk (v_pnum)
					  INTO v_stock_qty
					  FROM t205_part_number
					 WHERE c205_part_number_id = v_pnum
				FOR UPDATE;
			--FG: 40054 - Finished Good to Raw Materials
			ELSIF (v_type = 40054)
			THEN
				SELECT	   get_qty_in_stock (v_pnum)
					  INTO v_stock_qty
					  FROM t205_part_number
					 WHERE c205_part_number_id = v_pnum
				FOR UPDATE;
			ELSIF (v_type = 4113)	--4113 : Quarantine
			THEN
				SELECT	 NVL (t205c.c205_available_qty, 0) INTO v_stock_qty 
				 FROM  t205c_part_qty t205c
	             WHERE t205c.C901_Transaction_Type = 90813 --90813 - Quarantine Qty
	             AND t205c.c205_part_number_id = v_pnum 
	             AND t205c.C5040_PLANT_ID 	   = v_plant_id
	             FOR UPDATE;
			END IF;

			IF v_stock_qty < v_qty
			THEN
				--Error message is " Stock qty less than requested qty."
				raise_application_error (-20142, '');
			END IF;

			INSERT INTO t505_item_consignment
						(c505_item_consignment_id, c504_consignment_id, c205_part_number_id, c505_control_number
					   , c505_item_qty, c505_item_price
						)
				 VALUES (s504_consign_item.NEXTVAL, p_id, v_pnum, v_control
					   , v_qty, TO_NUMBER (get_part_price (v_pnum, 'E'))
						);
		END LOOP;
		
		 --When ever the detail is updated, the consignment table has to be updated ,so ETL can Sync it to GOP.
		 UPDATE t504_consignment
			SET c504_last_updated_date = CURRENT_DATE
		  WHERE c504_consignment_id = p_id;		
	--
	ELSE
		p_errmsg	:= 'Empty String';
	END IF;

	COMMIT WORK;
EXCEPTION
	WHEN e_others
	THEN
--
		ROLLBACK WORK;
		p_errmsg	:= SQLERRM;
--
	WHEN OTHERS
	THEN
--
		ROLLBACK WORK;
		p_errmsg	:= SQLERRM;
		NULL;
--
END gm_save_reprocess_returns_item;
/
