/* Formatted on 2011/01/26 09:52 (Formatter Plus v4.8.0) */
--@"C:\database\Procedures\operations\gm_op_fch_pend_rpt_vendor.prc";

CREATE OR REPLACE PROCEDURE gm_op_fch_pend_rpt_vendor (
	p_type		  IN	   VARCHAR2
  , p_vendorid	  IN	   t401_purchase_order.c301_vendor_id%TYPE
  , p_partnum	  IN	   t205_part_number.c205_part_number_id%TYPE
  , p_projid	  IN	   t205_part_number.c202_project_id%TYPE
  , p_typeofpo	  IN	   t401_purchase_order.c401_type%TYPE
  , p_recordset   OUT	   TYPES.cursor_type
  , p_user_id	  IN	   t401_purchase_order.C401_Created_By%TYPE
)
AS
	v_wo		   t402_work_order.c402_work_order_id%TYPE;
	v_part_number  t205_part_number.c205_part_number_id%TYPE;
	v_qty		   NUMBER (20);
	v_compid_frm_cntx VARCHAR2(50);

--CREATE TYPE wo_list IS TABLE OF t402_work_order.c402_work_order_id%TYPE; -- c_wo_id, c_pnum;
	CURSOR cur_list
	IS
		SELECT t402.c205_part_number_id pnum, t402.c402_work_order_id woid
		  FROM t401_purchase_order t401, t402_work_order t402, t205_part_number t205
		 WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
		   AND T401.C1900_COMPANY_ID = v_compid_frm_cntx
		   AND t402.c402_status_fl < 3
		   AND t402.c402_void_fl IS NULL
		   AND t401.c401_void_fl IS NULL
		   AND t402.c205_part_number_id = t205.c205_part_number_id
		   AND t401.c301_vendor_id = DECODE (p_vendorid, 0, t401.c301_vendor_id, p_vendorid)
		   AND T401.C401_Created_By = DECODE (p_user_id, 0, t401.C401_Created_By, p_user_id)
		   AND t205.c202_project_id = DECODE (p_projid, '', t205.c202_project_id, p_projid)
		AND regexp_like (t402.c205_part_number_id, NVL (REGEXP_REPLACE(p_partnum,'[+]','\+'), REGEXP_REPLACE(t402.c205_part_number_id,'[+]','\+')))
		   AND t401.c401_type = DECODE (p_typeofpo, 0, t401.c401_type, p_typeofpo)
		   AND DECODE (p_type, 'Pend', t401.c401_date_required, TO_DATE ('01/2003', 'MM/YYYY')) <
														DECODE (p_type
															  , 'Pend', CURRENT_DATE
															  , TO_DATE ('01/2004', 'MM/YYYY')
															   )
		   AND t401.c401_date_required BETWEEN DECODE (p_type
													 , NULL, TO_DATE ('01/2003', 'MM/YYYY')
													 , 'Pend', TO_DATE ('01/2003', 'MM/YYYY')
													 , CURRENT_DATE
													  )
										   AND DECODE (p_type
													 , 'Due10', CURRENT_DATE + 10
													 , 'Due15', CURRENT_DATE + 15
													 , 'Due30', CURRENT_DATE + 30
													 , CURRENT_DATE + 365
													  );
BEGIN
-- delete bothtemp tables
	DELETE FROM my_temp_part_list;

	DELETE FROM my_temp_list;
	
	SELECT get_compid_frm_cntx() INTO v_compid_frm_cntx  FROM DUAL;

	FOR wo_num_cur IN cur_list
	LOOP
		v_part_number := wo_num_cur.pnum;
		v_wo		:= wo_num_cur.woid;
		
		UPDATE my_temp_part_list
		   SET c205_part_number_id = v_part_number
		 WHERE c205_part_number_id = v_part_number;

		IF (SQL%ROWCOUNT = 0)
		THEN
			INSERT INTO my_temp_part_list
						(c205_part_number_id, c205_qty
						)
				 VALUES (v_part_number, 0
						);
		END IF;

		INSERT INTO my_temp_list
			 VALUES (v_wo);
	END LOOP;
	
	--Exclude Unwanted Inventory bucket, as we need only Shelf Quantity for this query.
	my_context.set_my_inlist_ctx('70004,70005,70006,70007,70008,70009,70010,70011,70012,70013,70014,70015,70016');
	
	--PC#3451-Adding WO Due date in Pending By Vendor Report[Added Columns: c402_wo_due_date,c402_wo_lead_date]
	OPEN p_recordset
	 FOR
		 SELECT   t402.c205_part_number_id pnum, t401.c401_purchase_ord_id po, T901.C901_CODE_NM potype
				  ,t401.c401_created_date cdate, t401.c401_date_required rdate, t402.c402_wo_due_date wodate, t402.c402_wo_lead_date woleaddate, v205.c205_part_num_desc pdesc
				, get_wo_pend_qty (t402.c402_work_order_id, t402.c402_qty_ordered) pendqty
				, T202.C202_PROJECT_NM pname, t402.c402_qty_ordered ordqty
				, get_wo_wip_qty (t402.c402_work_order_id) wipqty, ROUND (NVL(t402.c402_wo_due_date,t401.c401_date_required) - CURRENT_DATE) days
				, T301.C301_VENDOR_SH_NAME vname, get_log_flag (t402.c402_work_order_id, 1232) LOG
				, t402.c402_work_order_id woid, t402.c402_rev_num worev, v205.c205_inshelf stkqty
				, get_latest_log_comments (t402.c402_work_order_id, 1232) lastcomments 
				,T101.C101_User_F_Name||' '||T101.C101_User_L_Name Raisedby
			 FROM t401_purchase_order t401, t402_work_order t402, v205_qty_in_stock v205, my_temp_list, T202_PROJECT T202
			 , T301_VENDOR T301, T901_CODE_LOOKUP T901,t101_user t101
			WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
			  AND t301.C1900_COMPANY_ID = v_compid_frm_cntx
			  AND t402.c205_part_number_id = v205.c205_part_number_id
			  AND t402.c402_work_order_id = my_temp_list.my_temp_txn_id
			  AND t202.c202_project_id = v205.c202_project_id
			  AND T301.c301_vendor_id = t401.c301_vendor_id
			  AND t401.c401_type = T901.C901_CODE_ID
			  AND t401.C401_Created_By = t101.C101_User_Id(+)
		 ORDER BY t402.c205_part_number_id, t401.c401_created_date;
END gm_op_fch_pend_rpt_vendor;
/
