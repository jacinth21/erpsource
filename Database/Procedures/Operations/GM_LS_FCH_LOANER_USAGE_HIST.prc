/* Formatted on 2008/09/18 17:39 (Formatter Plus v4.8.0) */
-- @"C:\database\Procedures\Operations\gm_ls_fch_loaner_usage_hist.prc";

CREATE OR REPLACE PROCEDURE gm_ls_fch_loaner_usage_hist (
	p_ref_id	  IN	   t412_inhouse_transactions.c412_ref_id%TYPE
  , p_recordset   OUT	   TYPES.cursor_type
)
AS
/***************************************************************************************************
  * This Procedure is to to Fetch Loaner History Transaction
  * Author : Dhinakaran James
  * Date   : 10/18/06
  * for loaner Extension, in select clause added 3 column's name
  * 1)	c504a_is_loaner_extended isext,
  * 2)	c504a_is_replenished isrep
  * 3)	 c504a_parent_loaner_txn_id partid
****************************************************************************************************/
	v_str		   VARCHAR2 (2000);
BEGIN
--
	OPEN p_recordset
	 FOR
		 SELECT   c504a_loaner_transaction_id ltxnid, get_distributor_name (c504a_consigned_to_id) distnm
				, TO_CHAR (c504a_loaner_dt, 'mm/dd/yyyy') ldate
				, TO_CHAR (c504a_expected_return_dt, 'mm/dd/yyyy') erdate
				, TO_CHAR (c504a_return_dt, 'mm/dd/yyyy') rdate, c504a_return_dt - c504a_loaner_dt usagedays
				, get_code_name (c901_consigned_to) loanto, get_account_name (c704_account_id) aname
				, get_rep_name (c703_sales_rep_id) rname, c504a_is_loaner_extended isext, c504a_is_replenished isrep
				, c504a_parent_loaner_txn_id partid
			 FROM t504a_loaner_transaction
			WHERE c504_consignment_id = p_ref_id AND c504a_void_fl IS NULL
		 ORDER BY c504a_loaner_transaction_id;
--
END gm_ls_fch_loaner_usage_hist;
/
