/* Formatted on 2011/03/04 11:31 (Formatter Plus v4.8.0) */
-- @"C:\database\Procedures\Operations\gm_update_wo_oper.prc";
CREATE OR REPLACE
PROCEDURE gm_update_wo_oper(
    p_str    IN VARCHAR2,
    p_userid IN t402_work_order.c402_last_updated_by%TYPE,
    p_str_msg OUT VARCHAR2)
AS
  v_strlen       NUMBER           := NVL (LENGTH (p_str), 0) ;
  v_string       VARCHAR2 (30000) := p_str;
  v_wo           VARCHAR2 (20) ;
  v_qty          NUMBER;
  v_cost         NUMBER (15, 2) ;
  v_fl           CHAR (1) ;
  v_criticalfl   CHAR (1) ;
  v_farfl        CHAR (1) ;
  v_rev          CHAR (10) ;
  v_validfl      CHAR (1) ;
  v_substring    VARCHAR2 (1000) ;
  v_msg          VARCHAR2 (1000) ;
  v_potype       NUMBER;
  v_quarqty      NUMBER;
  v_origqty      NUMBER;
  v_action       CHAR (1) ;
  v_partnum      VARCHAR2 (20) ;
  v_changeqty    NUMBER;
  v_poid         VARCHAR2 (20) ;
  v_sign         NUMBER (1) := 1;
  v_postaction   NUMBER (4) := 4301;
  v_post_id      VARCHAR2 (20) ;
  v_vend_id      NUMBER;
  v_origrev      CHAR (10) ;
  v_newwo        VARCHAR2 (20) ;
  v_priceid      NUMBER;
  v_pendqty      NUMBER;
  v_tempqty      NUMBER;
  v_pend_tmp_qty NUMBER;
  v_pend_dhr_cnt NUMBER;
  v_split_pert   NUMBER (15, 2) ;
  v_posting_fl   CHAR (1) ;
  v_rc_fl        CHAR (1) ;
  v_split_cost   NUMBER (15, 2) ;
  v_rm_inv_fl    CHAR (1) ;
  v_dhr_count    NUMBER;
  v_dhr_fl       VARCHAR2(5);
  v_wo_str       VARCHAR2(4000);
  v_cost_price t402_work_order.c402_cost_price%TYPE;
  v_mfg_wo       NUMBER;
  v_company_id  t1900_company.c1900_company_id%TYPE;
  --
  v_wo_revision_cnt NUMBER;
  v_wo_revision_status NUMBER := 108240; -- Updated
  -- Cursor to get the sub-components of the part
  CURSOR pop_val
  IS
    SELECT t205.c205_part_number_id subid,
      t205.c205_rev_num rev
    FROM t205a_part_mapping t205a,
      t205_part_number t205
    WHERE t205a.c205_from_part_number_id = v_partnum
    AND t205a.c205_to_part_number_id     = t205.c205_part_number_id
    AND t205a.c901_type                 IN (30031, 30034) 
    AND t205a.c205a_void_fl is null;  ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
BEGIN
  
  IF v_strlen                    > 0 THEN
    WHILE INSTR (v_string, '|') <> 0
    LOOP
      v_substring  := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
      v_string     := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
      v_wo         := NULL;
      v_qty        := NULL;
      v_cost       := NULL;
      v_fl         := NULL;
      v_criticalfl := NULL;
      v_farfl      := NULL;
      v_rev        := NULL;
      v_validfl    := NULL;
      v_action     := NULL;
      ---
      v_wo         := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
      v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
      v_qty        := TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
      v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
      v_cost       := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
      v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
      v_fl         := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
      v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
      v_criticalfl := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
      v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
      v_farfl      := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
      v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
      v_rev        := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
      v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
      v_validfl    := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
      v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
      v_action     := v_substring;
      IF v_fl       = 'N' THEN
        v_fl       := NULL;
      END IF;
      SELECT COUNT (1)
      INTO v_dhr_count
      FROM T408_DHR t408
      WHERE t408.c402_work_order_id = v_wo
      AND t408.c408_void_fl        IS NULL;
      -- FOR UPDATE on Part Number table
      SELECT t205.c205_part_number_id
      INTO v_partnum
      FROM t402_work_order t402,
        t205_part_number t205
      WHERE t402.c205_part_number_id = t205.c205_part_number_id
      AND t402.c402_work_order_id    = v_wo FOR UPDATE;
      -- Getting PO Type, Quarantine Qty for removing from Quarantine if there is an increase in the Qty Ordered
      SELECT get_potype_from_woid (t402.c402_work_order_id),
        get_partnumber_qty(t205.c205_part_number_id,90813) - get_qty_in_transactions_quaran (t205.c205_part_number_id),
        t402.c402_qty_ordered,
        t402.c401_purchase_ord_id,
        t402.c301_vendor_id,
        TRIM (c402_rev_num) ,
        c405_pricing_id,
        get_wo_pend_qty (t402.c402_work_order_id, t402.c402_qty_ordered),
        t402.c402_split_percentage,
        t402.c402_posting_fl,
        t402.c402_rc_fl ,
        t402.c402_split_cost,
        t402.c402_rm_inv_fl,
        t402.c402_cost_price
      INTO v_potype,
        v_quarqty,
        v_origqty ,
        v_poid,
        v_vend_id,
        v_origrev ,
        v_priceid,
        v_pendqty,
        v_split_pert ,
        v_posting_fl,
        v_rc_fl,
        v_split_cost ,
        v_rm_inv_fl,
        v_cost_price
      FROM t402_work_order t402,
        t205_part_number t205
      WHERE t402.c205_part_number_id = t205.c205_part_number_id
      AND t402.c402_work_order_id    = v_wo;
      IF v_cost_price               != v_cost AND v_dhr_count > 0 THEN
        GM_RAISE_APPLICATION_ERROR('-20999','336',v_wo);
      END IF;
      -- to be used in the posting
      v_changeqty := TO_NUMBER (v_origqty - v_qty) ;
      --Fetching the count of DHR's which is not verified status .
      SELECT COUNT(1)
      INTO v_pend_dhr_cnt
      FROM T408_DHR T408
      WHERE t408.c402_work_order_id = v_wo
      AND t408.c408_status_fl       < 4 -- Verified
      AND t408.c408_void_fl        IS NULL
      AND t408.c408_dhr_id NOT LIKE 'GM-RC%';
      -- to getting the company id from PO
        SELECT c1900_company_id
	      INTO v_company_id
	    FROM t401_purchase_order
	      WHERE c401_purchase_ord_id = v_poid
	    AND c401_void_fl  IS NULL;
	  --
      IF v_origrev <> v_rev THEN
      
      --The Below code is added for PMT-8134, When Manufacturing Run Initiated for Work Order ,The version cannot be modified.
      --v_mfg_wo  The count is to check whether the Manufacturing Run Initiated for Work Order 
      SELECT COUNT(1)
         INTO  v_mfg_wo
           FROM t408_dhr t408, t402_work_order t402,t401_purchase_order t401
          WHERE t408.c402_work_order_id = t402.c402_work_order_id
          AND t402.c401_purchase_ord_id = t401.c401_purchase_ord_id
          AND t401.c401_type in (3104,3105) --3104 Manufactured FG,3105 Manufactured Subcomponent	
          AND t408.c408_status_fl <4 --IN (0,1,2,3)
          AND t408.c402_work_order_id = v_wo
          AND t408.c408_void_fl IS NULL
          AND t402.C402_VOID_FL IS NULL
          AND t401.C401_VOID_FL IS NULL;
           
           
      IF v_mfg_wo > 0 THEN
        GM_RAISE_APPLICATION_ERROR('-20999','337',v_wo);
      END IF;

        -- Update Current Work ORder to Closed status
        UPDATE t402_work_order
        SET c402_qty_ordered     = v_origqty - v_pendqty,
          c402_status_fl         = DECODE(v_pend_dhr_cnt,0,3,c402_status_fl),
          c402_last_updated_date = CURRENT_DATE ,
          c402_last_updated_by   = p_userid
        WHERE c402_work_order_id = v_wo;
        gm_update_log (v_wo, 'Work Order Qty split due to Rev Change', 1232, p_userid, v_msg) ;
        -- Create new Work Order
        --
        SELECT 'GM-WO-'
          || s402_work.NEXTVAL
        INTO v_newwo
        FROM DUAL;
        INSERT
        INTO t402_work_order
          (
            c402_work_order_id,
            c401_purchase_ord_id,
            c205_part_number_id ,
            c301_vendor_id,
            c402_qty_ordered,
            c402_cost_price ,
            c402_rev_num,
            c402_created_by,
            c402_created_date ,
            c402_status_fl,
            c402_critical_fl,
            c402_far_fl ,
            c405_pricing_id,
            c402_split_percentage,
            c402_posting_fl ,
            c402_rc_fl,
            c402_split_cost,
            c402_rm_inv_fl ,
            c402_validation_fl,
            c1900_company_id
          )
          VALUES
          (
            v_newwo,
            v_poid,
            v_partnum ,
            v_vend_id,
            v_pendqty,
            v_cost ,
            v_rev,
            p_userid,
            CURRENT_DATE ,
            '1',
            v_criticalfl,
            v_farfl ,
            v_priceid,
            v_split_pert,
            v_posting_fl ,
            v_rc_fl,
            v_split_cost,
            v_rm_inv_fl ,
            v_validfl,
            v_company_id
          ) ;
        ----This below prc call is used to time stamp the WO part attribute details
        gm_pkg_wo_txn.gm_sav_wo_part_info(v_newwo, v_partnum, p_userid);
        gm_update_log (v_newwo, 'Work Order Qty split due to Rev Change. Old WO:' || v_wo, 1232, p_userid, v_msg) ;
        
        -- This below prc call is used to insert Purchase Activity Log when the revision is updated      
        gm_pkg_purchase_activity.gm_sav_po_activity(v_newwo,'108441',null,null,v_vend_id,'Updated Revision from '||v_origrev, get_rule_value('UPDREV','POACTLOG')|| ' ' ||v_rev,
                             p_userid,v_company_id);	
        -- This below prc call is to insert purchase activity log as PO updated                     
        gm_pkg_purchase_activity.gm_sav_po_activity(v_poid,'108440',null,null,v_vend_id,null, get_rule_value('UPDATEPO','POACTLOG'), p_userid,v_company_id);
        
        -- Inserting records into the Sub work Order table for sub-components
       
        FOR rad_val IN pop_val
        LOOP
          --
          INSERT
          INTO t403_sub_work_order
            (
            
              c403_sub_work_order_id,
              c402_work_order_id,
              c206_sub_asmbly_id ,
              c403_rev_num
            )
            VALUES
            (
              s403_sub_work.NEXTVAL,
              v_newwo,
              rad_val.subid ,
              rad_val.rev
            ) ;
          --
		     /*
         * PMT-32340 Work Order Revision Update Dashboard
         * If Revision modified then, we are creating the new Work order id.
           Before insert the - to update the sub work order revision (v_wo).
         */
             UPDATE t403_sub_work_order 
                SET c403_rev_num = rad_val.rev 
              WHERE c402_work_order_id=v_wo
	            AND c206_sub_asmbly_id = rad_val.subid;
	        END LOOP;
				 /*
         * PMT-32340 Work Order Revision Update Dashboard
		   To update the work order revision status in New table T402C_WORK_ORDER_REVISION
		   Scenario:
		   	1)Change the status when Update revision in EDIT PO Screen(PO Report Dash)
		 	2)Change the status(open to close) when click Update button in wo revision dashboard screen
		 */
	        -- to get the count 
	        SELECT COUNT(1)
	        INTO v_wo_revision_cnt
	        FROM t402c_work_order_revision
	        WHERE c402_work_order_id=v_wo
	        AND c402c_void_fl IS NULL;
	        
	        --
	        IF v_wo_revision_cnt = 0
	        THEN
	        	v_wo_revision_status := 26240599; -- Updated from WO edit
	        END IF;
	        --
           gm_pkg_wo_txn.gm_sav_wo_revision(v_wo,v_partnum,v_origrev,v_rev,108245,v_wo_revision_status,NULL,NULL,p_userid);
      ELSE
      --  to added the existing function call (SVN branch created time this function is missed)
	  SELECT get_wo_pend_qty (v_wo, v_qty) INTO v_pend_tmp_qty FROM DUAL;
	  --
        IF v_fl = 'Y' THEN
          SELECT gm_pkg_wo_txn.chk_dhr_exist(v_wo) INTO v_dhr_fl FROM dual;
          IF v_dhr_fl = 'Y' THEN
            v_wo_str := v_wo_str || v_wo || ',';
          ELSE
            UPDATE t402_work_order
            SET c402_cost_price      = v_cost,
              c402_qty_ordered       = v_qty,
              c402_void_fl           = v_fl ,
              c402_critical_fl       = v_criticalfl,
              c402_far_fl            = v_farfl,
              c402_rev_num           = TRIM (v_rev) ,
              c402_last_updated_date = CURRENT_DATE,
              c402_last_updated_by   = p_userid,
              c402_status_fl         = DECODE ( v_pend_tmp_qty, 0, DECODE(v_pend_dhr_cnt,0,3,c402_status_fl), c402_status_fl),
              c402_validation_fl     = v_validfl
            WHERE c402_work_order_id = v_wo;
          END IF;
        ELSE
          UPDATE t402_work_order
          SET c402_cost_price      = v_cost,
            c402_qty_ordered       = v_qty,
            c402_void_fl           = v_fl ,
            c402_critical_fl       = v_criticalfl,
            c402_far_fl            = v_farfl,
            c402_rev_num           = TRIM (v_rev) ,
            c402_last_updated_date = SYSDATE,
            c402_last_updated_by   = p_userid,
            c402_status_fl         = DECODE ( v_pend_tmp_qty, 0, DECODE(v_pend_dhr_cnt,0,3,c402_status_fl), c402_status_fl),
            c402_validation_fl     = v_validfl
          WHERE c402_work_order_id = v_wo;
        END IF;
        
      END IF;
      -- If rework PO and any action other than 'No Action'
      IF v_potype       = 3101 AND v_action <> '0' THEN
        IF v_action     = 'R' THEN
          v_postaction := 4302;
          v_post_id    := 4825;
        ELSIF v_action  = 'Q' THEN
          v_postaction := 4301;
          v_post_id    := 48086;
        ELSE
          v_post_id := 48064;
        END IF;
        -- IF the action is 'To Rework', then quarantine should be depleted
        IF v_action = 'R' AND v_quarqty < v_changeqty THEN
          raise_application_error ( - 20009, '') ;
        END IF;
        IF v_action = 'R' AND v_changeqty > 0 THEN
          raise_application_error ( - 20155, '') ;
        END IF;
        IF (v_action = 'Q' OR v_action = 'S') AND (v_changeqty < 0) THEN
          raise_application_error ( - 20154, '') ;
        END IF;
        IF v_action      = 'R' OR v_action = 'Q' THEN
          IF v_fl        = 'Y' THEN
            v_changeqty := v_qty;
          END IF;
          --
          gm_cm_sav_partqty (v_partnum,
                               v_changeqty,
                               v_poid,
                               p_userid,
                               90813, -- quarantine  Qty
                               v_postaction, -- Plus
                               4314 -- Purchase
                               );
        END IF;
        --
        IF v_potype    = 3101 AND v_action = 'R' THEN --Used for Posting to done
          v_changeqty := v_changeqty * ( - 1) ;
        END IF;
        gm_save_ledger_posting (v_post_id, CURRENT_DATE, v_partnum, v_vend_id, v_poid, v_changeqty, v_cost, p_userid ) ;
      END IF;
      --
      -- PMT-27537 : If any WO vendor Commit then update the information 
     	 gm_pkg_pur_wo_vendor_commit.gm_update_vendor_commit_qty (v_wo, v_newwo, p_userid);
      -- to reset the new WO 
      	v_newwo := NULL;
	
		
    END LOOP;
  ELSE
    raise_application_error ( - 20009, '') ;
  END IF;
  IF v_wo_str IS NOT NULL THEN
        	v_wo_str := SUBSTR(v_wo_str,1,LENGTH(v_wo_str)-1);
          v_msg     := ' The following work order(s) has not been voided due to existing DHR . Please contact receiving for further processing '||v_wo_str||'.' ;
        END IF;
  p_str_msg := v_msg;
END gm_update_wo_oper;
/
