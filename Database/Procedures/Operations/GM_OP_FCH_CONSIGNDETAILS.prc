/* Formatted on 2010/05/19 17:12 (Formatter Plus v4.8.0) */

--@"c:\database\procedures\operations\gm_op_fch_consigndetails.prc"

CREATE OR REPLACE PROCEDURE gm_op_fch_consigndetails (
	p_inputstr	  IN	   VARCHAR2
  , p_recordset   OUT	   TYPES.cursor_type
)
AS
v_datefmt VARCHAR2(10);
/***************************************************************************************
 * Description			 : This procedure fetches Consignment header info
 *
 ****************************************************************************************/
BEGIN
	
	SELECT NVL(get_compdtfmt_frm_cntx(),get_rule_value('DATEFMT','DATEFORMAT')) INTO v_datefmt FROM DUAL;

--
	IF p_inputstr IS NOT NULL
	THEN
		my_context.set_my_inlist_ctx (p_inputstr);

		OPEN p_recordset
		 FOR
			SELECT t504.c504_consignment_id consignid, get_set_name (t504.c207_set_id) setname
				  , t504.c207_set_id setid, get_code_name (t504.c504_inhouse_purpose) settype, c504_type ctype
				  , t504a.c504a_etch_id etchid, t504a.c504a_expected_return_dt edate
				  , TO_CHAR(t504a.c504a_expected_return_dt,v_datefmt) eredate
				  , t504a.c504a_loaner_dt ldate
				  , gm_pkg_op_loaner.get_loaner_status (t504a.c504a_status_fl) loansfl
				  , t504a.c504a_status_fl loansflcd
				  , DECODE (get_cs_rep_loaner_bill_nm_add (t504.c504_consignment_id)
						  , ' ', 'Globus Medical'
						  , get_cs_rep_loaner_bill_nm_add (t504.c504_consignment_id)
						   ) billnm
				  , t504.c701_distributor_id distid,GET_DISTRIBUTOR_NAME(t504.c701_distributor_id) distnm
				  , t504al.c703_sales_rep_id repid,get_rep_name(t504al.c703_sales_rep_id) repnm
				  , t504al.c704_account_id accid,get_account_name(t504al.c704_account_id) accnm
				  , get_party_name(get_dealer_id_by_acct_id(t504al.c704_account_id)) delearnm 
				  , DECODE (t504a.c504a_status_fl, 25, gm_pkg_op_loaner.get_return_dt (vlist.token), '') returndt
				  , gm_pkg_op_loaner.get_loaner_trans_id (vlist.token) loantransid
				  , get_cs_ship_email (4121, t504al.c703_sales_rep_id) emailadd
				  , t504.C504_HOLD_FL holdfl
				  , get_compid_from_distid(t504.C701_DISTRIBUTOR_ID) COMPANYID
				  , get_code_name(c504_type) txntypenm
				  , gm_pkg_op_loaner.get_loaner_request_id(t504al.c504a_loaner_transaction_id) txnparentid
				  , t504al.c703_ass_rep_id assocrepid,get_rep_name(t504al.c703_ass_rep_id) assocrepnm
				  , t504.c504_inhouse_purpose setpurpose
			   -- , gm_pkg_op_loaner.get_loaner_set_list_change (vlist.token) setlistfl
			      , t504.c1910_division_id DIVISION_ID
                  , t504.c1900_company_id COMPANYCD
                  , get_company_dtfmt(T504.c1900_company_id) CMPDFMT
                  , t504al.c1900_company_id TRANS_COMPANY_ID
                  -- PC-3784 Surgery date is not updated on loaner stock sheet working in progress
                  , NVL(t504al.c504a_surgery_date,gm_pkg_op_loaner.get_request_surgery_date(t504al. C526_PRODUCT_REQUEST_DETAIL_ID)) surgerydate
                  , t907.lnshipdate shipdate
                  , gm_pkg_op_loaner.get_loaner_requested_by(t504al. C526_PRODUCT_REQUEST_DETAIL_ID) requestedby
                  , GET_TAG_ID(t504.c504_consignment_id) tagid, t504.c207_set_id||' '||get_set_name (t504.c207_set_id) setidname
                  , GET_LATEST_LOG_COMMENTS(t504.c504_consignment_id ,'1222') logcomments
                  ,t907.prodrqid prodloanerrqid
			 FROM	t504_consignment t504
				  , t504a_consignment_loaner t504a
				  , t504a_loaner_transaction t504al
				  , v_in_list vlist
				  , ( SELECT t504a.c504_consignment_id, t907.c907_ship_to_dt lnshipdate,t907.c525_product_request_id prodrqid
                         FROM t504a_consignment_loaner t504a
                         , t504a_loaner_transaction t504al
                         , t526_product_request_detail t526
                         , t907_shipping_info t907
                        WHERE t504a.c504a_status_fl                 = 20 -- PENDING RETURN
                          AND t504al.c504a_return_dt               IS NULL
                          AND t504al.c504a_void_fl                 IS NULL
                          AND t504a.c504_consignment_id             = t504al.c504_consignment_id
                          AND t504al.c526_product_request_detail_id = t526.c526_product_request_detail_id
                          AND t526.c525_product_request_id          = t907.c525_product_request_id
                          AND t504a.c504_consignment_id             = t907.c907_ref_id
                          AND t504a.c504a_void_fl                  IS NULL
                          AND t907.c907_void_fl                    IS NULL
                          AND t526.C526_VOID_FL                    IS NULL) t907
			  WHERE t504.c504_consignment_id = vlist.token
				AND t504.c504_consignment_id = t504a.c504_consignment_id
				AND t504al.c504_consignment_id(+) = t504.c504_consignment_id
				AND t504.c504_consignment_id = t907.c504_consignment_id (+)
				AND t504al.c504a_loaner_transaction_id(+) = gm_pkg_op_loaner.get_loaner_trans_id (t504.c504_consignment_id);
END IF;
--
END gm_op_fch_consigndetails;
/
