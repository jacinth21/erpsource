/* Formatted on 2009/02/13 10:29 (Formatter Plus v4.8.0) */

--@"c:\database\procedures\operations\gm_save_vendor.prc";

CREATE OR REPLACE PROCEDURE gm_save_vendor (
	p_vendornm		  IN	   t301_vendor.c301_vendor_name%TYPE
  , p_vendorshnm	  IN	   t301_vendor.c301_vendor_sh_name%TYPE
  , p_lotcode		  IN	   t301_vendor.c301_lot_code%TYPE
  , p_status		  IN	   t301_vendor.c901_status_id%TYPE
  , p_contactperson   IN	   t301_vendor.c301_contact_person%TYPE
  , p_address1		  IN	   t301_vendor.c301_bill_add1%TYPE
  , p_address2		  IN	   t301_vendor.c301_bill_add2%TYPE
  , p_city			  IN	   t301_vendor.c301_bill_city%TYPE
  , p_state 		  IN	   t301_vendor.c301_bill_state%TYPE
  , p_country		  IN	   t301_vendor.c301_bill_country%TYPE
  , p_pincode		  IN	   t301_vendor.c301_bill_zip%TYPE
  , p_payment		  IN	   t301_vendor.c301_payment_terms%TYPE
  , p_phone 		  IN	   t301_vendor.c301_phone%TYPE
  , p_fax			  IN	   t301_vendor.c301_fax%TYPE
  , p_activefl		  IN	   t301_vendor.c301_active_fl%TYPE
  , p_userid		  IN	   t301_vendor.c301_created_by%TYPE
  , p_currency		  IN	   t301_vendor.c301_currency%TYPE
  , p_action		  IN	   VARCHAR2
  , p_partyid		  IN	   t301_vendor.c301_vendor_id%TYPE
  , p_sec199fl	  IN	   t301_vendor.C301_SEC199_FL%TYPE
  , p_splitpercent	  IN	   t301_vendor.C301_RM_PERCENTAGE%TYPE
  , p_rminvoicefl	  IN	   t301_vendor.C301_RM_INVOICE_FL%TYPE
  , p_vendor_cost_type IN      t301_vendor.c901_vendor_cost_type%TYPE
  , p_vendor_port_fl  IN 	   t301_vendor.c301_vendor_portal_fl%TYPE
  , p_auto_publish_fl IN	   t301_vendor.c301_auto_publish_fl%TYPE
  -- PMT-50795-Capture Purchasing Agent - Vendor Setup screen
  , p_purch_agent   IN 	       t301_vendor.c301_purchasing_agent_id%TYPE 
  , p_email 		IN 			t301_vendor.C301_EMAIL_ID%TYPE
  , v_id			  OUT	   NUMBER
)
AS
	v_vendor_id    NUMBER;
	v_string	   VARCHAR2 (10);
	v_id_string    VARCHAR2 (10);
	v_party_id	   NUMBER;
	v_partyid	   NUMBER;
	subject 	   VARCHAR2 (200);
	to_mail 	   VARCHAR2 (1000);
	mail_body	   VARCHAR2 (4000);
	v_username	   VARCHAR2 (80);
	v_usrmail	   VARCHAR2 (200);
	crlf		   VARCHAR2 (2) := CHR (13) || CHR (10);
	v_company_id   t1900_company.c1900_company_id%TYPE;
	v_active_date  t301_vendor.c301_active_date%type;
	v_inactive_date t301_vendor.c301_inactive_date%type;
	v_active_dt_fl	VARCHAR2(10);
	v_active_fl		VARCHAR2(10);
BEGIN
	SELECT DECODE (p_partyid, 0, NULL, p_partyid) INTO v_partyid  FROM DUAL;

	gm_pkg_cm_party.gm_cm_sav_party_id (v_partyid, NULL, NULL, '', p_vendornm, 7002, p_activefl, p_userid, v_party_id);
	v_id		:= v_party_id;
	
	--PC-3301: Add Active/Inactive Date in Vendor Report
	IF p_activefl = 'N' THEN
		v_inactive_date := current_date;
	ELSE
		v_active_date := current_date;
		v_inactive_date := NULL;
	END IF;
	
	IF p_action = 'Add'
	THEN
		BEGIN
			SELECT s301_vendor.NEXTVAL, get_compid_frm_cntx()
			  INTO v_vendor_id, v_company_id
			  FROM DUAL;

			INSERT INTO t301_vendor
						(c301_vendor_id, c301_vendor_name, c301_vendor_sh_name, c301_lot_code, c901_status_id
					   , c301_currency, c301_contact_person, c301_bill_add1, c301_bill_add2, c301_bill_city
					   , c301_bill_state, c301_bill_country, c301_bill_zip, c301_payment_terms, c301_phone, c301_fax
					   , c301_active_fl, c301_created_by, c301_created_date, c101_party_id, c301_sec199_fl,  c301_rm_percentage ,c301_rm_invoice_fl
					   , c901_vendor_cost_type,c301_vendor_portal_fl,c1900_company_id,c301_auto_publish_fl
					   , c301_purchasing_agent_id, c301_active_date, c301_inactive_date,C301_EMAIL_ID
						)
				 VALUES (v_vendor_id, p_vendornm, p_vendorshnm, p_lotcode, p_status
					   , p_currency, p_contactperson, p_address1, p_address2, p_city
					   , p_state, p_country, p_pincode, p_payment, p_phone, p_fax
					   , p_activefl, p_userid, CURRENT_DATE, v_party_id, p_sec199fl, p_splitpercent, p_rminvoicefl
					 ,p_vendor_cost_type,p_vendor_port_fl,v_company_id,p_auto_publish_fl
					 , p_purch_agent, v_active_date, v_inactive_date,p_email);

			v_username	:= get_user_name (p_userid);
			v_usrmail	:= get_user_emailid (p_userid);
			-- Email Area
			to_mail 	:= get_rule_value_by_company ('APSAGE', 'EMAIL',v_company_id) || ',' || v_usrmail;
			subject 	:= ' Vendor Created: Requires Action ';
			mail_body	:=
				   'A new vendor has been created in the GM Portal. '
				|| crlf
				|| 'Please create a new Vendor in MAS500 and map that to the new Vendor created in the Portal.'
				|| crlf
				|| crlf
				|| 'Following are the details:'
				|| crlf
				|| 'Vendor Name :'
				|| p_vendornm
				|| crlf
				|| 'Vendor ID :'
				|| v_vendor_id
				|| crlf
				|| 'Created by :'
				|| v_username
				|| crlf
				|| 'Created Date :'
				|| TO_CHAR (CURRENT_DATE, GET_RULE_VALUE('DATEFMT','DATEFORMAT') ||' HH12:MI:SS AM')
				|| '.'
				|| crlf
				|| crlf
				|| '** This is an auto-generated email: Please do not reply to this id**';
			gm_com_send_email_prc (to_mail, subject, mail_body);
		END;
	ELSE
		BEGIN
			
			BEGIN
				SELECT nvl(c301_active_fl,-999) INTO v_active_fl
			      FROM T301_VENDOR
			     WHERE c101_party_id = p_partyid;
			EXCEPTION WHEN OTHERS THEN
				v_active_fl:= NULL;
			END;
			
			IF (v_active_fl <> nvl(p_activefl,-999) AND nvl(p_activefl,-999) <> 'N') THEN
				v_active_dt_fl := 'Y';				
			END IF;
			 
			UPDATE t301_vendor
			   SET c301_vendor_sh_name = p_vendorshnm
			   	 , c301_vendor_name = p_vendornm
				 , c301_lot_code = p_lotcode
				 , c901_status_id = p_status
				 , c301_currency = p_currency
				 , c301_contact_person = p_contactperson
				 , c301_bill_add1 = p_address1
				 , c301_bill_add2 = p_address2
				 , c301_bill_city = p_city
				 , c301_bill_state = p_state
				 , c301_bill_country = p_country
				 , c301_bill_zip = p_pincode
				 , c301_payment_terms = p_payment
				 , c301_phone = p_phone
				 , c301_fax = p_fax
				 , c301_active_fl = p_activefl
				 , c301_last_updated_date = CURRENT_DATE
				 , c301_last_updated_by = p_userid
				 , c901_vendor_cost_type=p_vendor_cost_type
				 , c301_vendor_portal_fl = p_vendor_port_fl
				 , C301_EMAIL_ID =p_email
         , C301_SEC199_FL = p_sec199fl
         , C301_RM_PERCENTAGE = p_splitpercent
         , C301_RM_INVOICE_FL = p_rminvoicefl
		 , C301_AUTO_PUBLISH_FL = p_auto_publish_fl
		 -- PMT-50795-Capture Purchasing Agent - Vendor Setup screen
		 , c301_purchasing_agent_id = p_purch_agent
		 , c301_active_date = decode(v_active_dt_fl,'Y',v_active_date,c301_active_date)
		 , c301_inactive_date = decode(p_activefl,'N',v_inactive_date,NULL)
			 WHERE c101_party_id = p_partyid;

			v_id		:= p_partyid;
		END;
	END IF;
END gm_save_vendor;
/
