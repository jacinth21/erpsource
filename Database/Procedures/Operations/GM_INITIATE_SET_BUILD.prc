CREATE OR REPLACE PROCEDURE GM_INITIATE_SET_BUILD
(
	   p_SetId 			    IN T504_CONSIGNMENT.C207_SET_ID%TYPE,
	   p_Type				IN VARCHAR2,
	   p_UserId				IN T504_CONSIGNMENT.C504_CREATED_BY%TYPE,
	   p_ConsignId			OUT VARCHAR2,
	   p_message 			OUT  VARCHAR2
)
AS
v_con_id NUMBER;
v_Id NUMBER;
v_string VARCHAR2(20);
v_id_string VARCHAR2(20);
v_price VARCHAR2(20);
CURSOR pop_val IS
	   	SELECT C205_PART_NUMBER_ID ID, C208_SET_QTY QTY
		FROM
			 T208_SET_DETAILS
		WHERE C207_SET_ID = p_SetId
		--AND C208_SET_QTY <> 0 AND C208_INSET_FL = 'Y'
		AND C208_INSET_FL = 'Y'
		ORDER BY C205_PART_NUMBER_ID;
v_company_id  t1900_company.c1900_company_id%TYPE;
v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
BEGIN
	   
          SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
            INTO v_company_id, v_plant_id
            FROM DUAL;
            
	 	  SELECT S504_CONSIGN.NEXTVAL INTO v_con_id FROM dual;
			  IF LENGTH(v_con_id) = 1
			  	 THEN
				 v_id_string := '0' || v_con_id;
				 ELSE
				 v_id_string := v_con_id;
			  END IF;
			  
			  SELECT 'GM-' ||p_Type ||'-' || v_id_string INTO v_string FROM dual;
		  	  p_ConsignId := v_string;
		      
			  INSERT INTO T504_CONSIGNMENT (
			  		C504_CONSIGNMENT_ID,
					C207_SET_ID,
					C504_STATUS_FL,
					C504_VERIFY_FL,
					C504_CREATED_BY,
					C504_CREATED_DATE,
                    C1900_COMPANY_ID,
                    C5040_PLANT_ID					
			  )
			  VALUES (
			   		v_string,
					p_SetId,
					'0',
					'0',
					p_UserId,
					CURRENT_DATE,
					v_company_id,
                    v_plant_id
			  );
			 BEGIN
			  	 FOR rad_val IN pop_val
				 	LOOP
						 SELECT S504_CONSIGN_ITEM.nextval INTO v_Id FROM dual;
			  			 v_price := GET_PART_PRICE(rad_val.ID,'E');
				 		 INSERT INTO T505_ITEM_CONSIGNMENT (
				 		 		C505_ITEM_CONSIGNMENT_ID, C504_CONSIGNMENT_ID,
				 		 		C205_PART_NUMBER_ID, C505_CONTROL_NUMBER,
				 		 		C505_ITEM_QTY, C505_ITEM_PRICE
				 		 )VALUES(
				 		 		 v_Id,p_ConsignId,
						 		 rad_val.ID,' ',
						 		 rad_val.QTY,v_price);
					END LOOP;
			 END;
			  EXCEPTION WHEN OTHERS
      	 	  THEN
                       p_message := SQLERRM;
              RETURN;
END GM_INITIATE_SET_BUILD;
/

