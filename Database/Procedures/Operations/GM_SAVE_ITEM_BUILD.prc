/* Formatted on 2010/08/04 12:56 (Formatter Plus v4.8.0) */
-- @"c:\database\procedures\operations\gm_save_item_build.prc"

CREATE OR REPLACE PROCEDURE gm_save_item_build (
   p_consignid   IN   t505_item_consignment.c504_consignment_id%TYPE,
   p_str         IN   VARCHAR2,
   p_shipfl      IN   VARCHAR2,
   p_userid      IN   t504_consignment.c504_last_updated_by%TYPE
)
AS
   v_strlen         NUMBER                         := NVL (LENGTH (p_str), 0);
   v_string         VARCHAR2 (30000)                        := p_str;
   v_pnum           VARCHAR2 (20);
   v_qty            NUMBER;
   v_control        t5010_tag.c5010_control_number%TYPE;
   v_price          NUMBER (15, 2);
   v_substring      VARCHAR2 (1000);
   v_msg            VARCHAR2 (1000);
   v_id             NUMBER;
   v_flag           VARCHAR2 (2);
   v_flag1          VARCHAR2 (2);
   v_shipfl         NUMBER;
   v_void_fl        VARCHAR2 (10);
   v_consign_type   NUMBER;
   v_quar_qty       NUMBER;
   v_rqid           t504_consignment.c520_request_id%TYPE;
   v_cnt            NUMBER                                  := 0;
   v_isset          VARCHAR2 (10);
   v_stock          NUMBER;
   v_qstock         NUMBER;
   v_code_grp       VARCHAR2(10);
   v_inv_type       NUMBER;
   v_comp_id		t1900_company.c1900_company_id%TYPE;
   v_comp_flag		VARCHAR2(1);
   v_excl_txn_cnt	NUMBER;
   v_part_valid_fl	VARCHAR2(1) := 'N';
   
--
BEGIN
	-- Get company id
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_comp_id FROM DUAL;
	
   -- Validate if the Consignment is already voided
   SELECT c504_void_fl, c504_type, c520_request_id
     INTO v_void_fl, v_consign_type, v_rqid
     FROM t504_consignment
    WHERE c504_consignment_id = p_consignid;

   -- To check whether the lot tracking validation is needed for the company or not
   SELECT get_rule_value(v_comp_id,'LOT_TRACK') INTO v_comp_flag FROM DUAL;
	-- Check whether the transaction is available in rules table or not
    SELECT COUNT(1) INTO v_excl_txn_cnt FROM t906_rules
	  WHERE c906_rule_id = v_consign_type
	  	AND c906_rule_grp_id = 'TXN_RULE'
	  	AND c906_void_fl IS NULL;
	  	
   IF v_void_fl IS NOT NULL
   THEN
      -- Error message is Consignment already Voided.
      raise_application_error (-20770, '');
   END IF;

   IF v_strlen > 0
   THEN
      --
      DELETE      t505_item_consignment
            WHERE c504_consignment_id = p_consignid;

      --
      WHILE INSTR (v_string, '|') <> 0
      LOOP
         v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
         v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
         v_pnum := NULL;
         v_qty := NULL;
         v_control := NULL;
         v_price := NULL;
         v_pnum := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
         v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
         v_qty := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
         v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
         v_control := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
         v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
         v_price := v_substring;

         IF    v_consign_type = 4114
            OR v_consign_type = 4113
            OR v_consign_type = 4116
         THEN           -- Repackaging/Shelf to quarantine/Quarantine to Shelf
            SELECT      NVL(GET_QTY_IN_INVENTORY(p_consignid,'90800',c205_part_number_id),0) stock,
                       (  get_partnumber_qty (c205_part_number_id, 90813)
                        - get_qty_allocated (c205_part_number_id, 50540)
                       ) qstock
                  INTO v_stock,
                       v_qstock
                  FROM t205_part_number
                 WHERE c205_part_number_id = v_pnum
            FOR UPDATE;
            IF v_consign_type IN (4114, 4113) 
            THEN
              IF v_stock < v_qty
              THEN
                 GM_RAISE_APPLICATION_ERROR('-20999','317',v_pnum);
              END IF;
            END IF;
            IF v_consign_type = 4116
            THEN
               IF v_qstock < v_qty
               THEN
                  GM_RAISE_APPLICATION_ERROR('-20999','317',v_pnum);
               END IF;
            END IF;
         END IF;

         --
         IF v_consign_type = 4115                             -- 4115 is scrap
         THEN
            SELECT     (  get_partnumber_qty(c205_part_number_id, 90813)
                        - get_qty_allocated (c205_part_number_id, 50540)
                       )
                  INTO v_quar_qty
                  FROM t205_part_number
                 WHERE c205_part_number_id = v_pnum
            FOR UPDATE;

            IF v_quar_qty < v_qty
            THEN
               GM_RAISE_APPLICATION_ERROR('-20999','317',v_pnum);
            END IF;
         END IF;

         SELECT get_part_attribute_value (v_pnum, 92340)      -- Taggable Part
           INTO v_isset
           FROM DUAL;

         IF     v_isset = 'Y'
            AND p_shipfl = '3'
            --if the consigned item then
            AND (   v_consign_type = 4110                       -- Consignment
                 OR v_consign_type = 4111                             --Loaner
                --    OR v_consign_type = 4112  --In-House Consignment
                )
         THEN
            SELECT COUNT (1)
              INTO v_cnt
              FROM t5010_tag t5010
             WHERE t5010.c205_part_number_id = v_pnum
               AND t5010.c5010_last_updated_trans_id = p_consignid
               AND t5010.c5010_control_number = v_control
               AND t5010.c5010_void_fl IS NULL;

            IF v_cnt = 0
            THEN
               -- Error message is TagID cannot be blank, please enter TagID.
               raise_application_error (-20912, '');
            END IF;
         END IF;

         SELECT s504_consign_item.NEXTVAL
           INTO v_id
           FROM DUAL;

         --
         INSERT INTO t505_item_consignment
                     (c505_item_consignment_id, c504_consignment_id,
                      c205_part_number_id, c505_item_qty,
                      c505_control_number, c505_item_price
                     )
              VALUES (v_id, p_consignid,
                      v_pnum, v_qty,
                      v_control, v_price
                     );
                     
		-- Check whether the validation is applicable for the part number or not, 104480: Lot tracking required ?
	      v_part_valid_fl := get_part_attr_value_by_comp(v_pnum,'104480',v_comp_id);
	      /* if the release check box is checked and if the transaction is available in rules table
	       * if the company is enabled for lot track validation check and if the part number need to be validated
	       * then change the status of lot number to controlled [105008]
	       */
	      
	      IF p_shipfl = '3' AND v_excl_txn_cnt > 0 AND  v_comp_flag = 'Y' AND v_part_valid_fl = 'Y'
	      THEN
	      	UPDATE t2550_part_control_number
				SET c901_lot_controlled_status = '105008' --Controlled
				  , c2550_last_updated_by = p_userid
				  , c2550_last_updated_date = CURRENT_DATE
				WHERE c205_part_number_id = v_pnum
					AND c2550_control_number = v_control;
	      END IF;
      END LOOP;

      --
      v_shipfl := TO_NUMBER (p_shipfl);

      IF v_shipfl < 2
      THEN
-- This is because by default, the Status flag should be 2 for Items - Refer Status_Fl xls file
         v_flag := '2';
      ELSE
         v_flag := p_shipfl;
      END IF;

      --
      UPDATE t504_consignment
         SET c504_status_fl = v_flag
           , c504_last_updated_by = p_userid
		   , c504_last_updated_date = SYSDATE
       WHERE c504_consignment_id = p_consignid AND c504_void_fl IS NULL;

-- only if type is consignment/in-house/ICT , 111802 - Finished Goods - Inhouse, 111805- Restricted Warehouse - Inhouse
      IF (   v_consign_type = 4110
          OR v_consign_type = 4112
          OR v_consign_type = 40057 OR  v_consign_type = '111802' OR v_consign_type = '111805' 
         )
      THEN
         IF v_flag = 3
         THEN
            gm_pkg_cm_shipping_trans.gm_sav_release_shipping (p_consignid,
                                                              50181,
                                                              p_userid,
                                                              ''
                                                             );
			
         END IF;
      END IF;
      
      IF v_consign_type in ('4110','4112','40057','4113','111802','111805') 
      THEN
      	-- When Consigment is Completed, Wipe out from the Inventory Pick queue Table.
			gm_pkg_op_inv_scan.gm_check_inv_order_status (p_consignid, 'Completed', p_userid);                                                             
      END IF;
      
      IF v_flag = 3 AND v_consign_type in ('4114','4116')
      THEN
        v_code_grp := 'INVPW';        
        BEGIN
          
          SELECT c901_code_id
            INTO v_inv_type
            FROM t901_code_lookup t901
           WHERE t901.c901_code_grp = v_code_grp AND t901.c902_code_nm_alt = to_char(v_consign_type) AND t901.c901_active_fl = '1';
           
           gm_pkg_allocation.gm_ins_invpick_assign_detail (v_inv_type, p_consignid, p_userid);	
           
        EXCEPTION
          WHEN NO_DATA_FOUND
          THEN
            NULL;
        END;       
      END IF;
      
      UPDATE t520_request
         SET c520_status_fl = DECODE (v_flag, '3', '30', c520_status_fl)
           , c520_last_updated_by = p_userid
           , c520_last_updated_date = SYSDATE
       WHERE c520_request_id = v_rqid 
         AND c520_void_fl IS NULL;

      --
      SELECT c504_update_inv_fl
        INTO v_flag1
        FROM t504_consignment
       WHERE c504_consignment_id = p_consignid AND c504_void_fl IS NULL;

      --
      IF v_shipfl = 4 AND v_flag1 IS NULL
      THEN
         --
         gm_update_inventory (p_consignid, 'CITEM', 'MINUS', p_userid, v_msg);

         --
         UPDATE t504_consignment
            SET c504_update_inv_fl = '1',
                -- For indicating that this transaction has updated Inventory
                c504_verify_fl = '1'
                                    -- Automatic Verification. Might change later - James
         ,
                c504_last_updated_date = SYSDATE,
                c504_last_updated_by = p_userid,
                c504_ship_date = TRUNC (SYSDATE)
          -- This value is entered by default when Shipping is not required.
          -- Else wont show in Consignment Report
         WHERE  c504_consignment_id = p_consignid AND c504_void_fl IS NULL;

         UPDATE t520_request
            SET c520_status_fl = '40',
                c520_required_date =
                   CASE
                      WHEN c520_required_date > TRUNC (SYSDATE)
                         THEN TRUNC (SYSDATE)
                      ELSE c520_required_date
                   END,
                c520_last_updated_by = p_userid,
                c520_last_updated_date = SYSDATE
          WHERE c520_request_id = v_rqid AND c520_void_fl IS NULL;
      --
      END IF;
   END IF;
--
END gm_save_item_build;
/
