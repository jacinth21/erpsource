/* Formatted on 2012/06/14 15:30 (Formatter Plus v4.8.0) */
--@"C:\Database\Procedures\Operations\GM_UPDATE_DHR_ROLLBACK.prc";

CREATE OR REPLACE PROCEDURE gm_update_dhr_rollback (
   p_dhrid           IN       t408_dhr.c408_dhr_id%TYPE,
   p_action          IN       VARCHAR2,
   p_userid          IN       t408_dhr.c408_last_updated_by%TYPE,
   p_inv_void_fl     OUT      VARCHAR2,
   p_email_details   OUT      TYPES.cursor_type
)
AS
   v_chkdhrvoidfl    VARCHAR2 (10);
   v_chkstatusfl     NUMBER;
   v_ncmr_cnt        NUMBER;
   v_rollback        VARCHAR2 (1);
   v_ncmr_id         t409_ncmr.c409_ncmr_id%TYPE;
   v_ncmr_sts_fl     NUMBER;
   v_qty_rejected    t408_dhr.c408_qty_rejected%TYPE;
   v_qty_closeout    t409_ncmr.c409_closeout_qty%TYPE;
   v_count           NUMBER;
   v_ncmr_void_fl    t409_ncmr.c409_void_fl%TYPE;
   v_dhr_status_fl   NUMBER;
   v_ncmr_type       t409_ncmr.c901_type%TYPE;
   v_po_type         NUMBER;
   v_payment_id      VARCHAR2 (20);
   v_invoice_id      VARCHAR2 (20);
   v_poid			 VARCHAR2(20);
   v_invoice_cnt     NUMBER;
   v_cur             TYPES.cursor_type;
   v_donor_id		 t408_dhr.c2540_donor_id%TYPE;
   v_inhouse_cnt     NUMBER;
   v_plant_id     	 t5040_plant_master.c5040_plant_id%TYPE;      
   v_company_id		 t1900_company.c1900_company_id%TYPE;
BEGIN
   SELECT c408_void_fl, TO_NUMBER (c408_status_fl)
     INTO v_chkdhrvoidfl, v_chkstatusfl
     FROM t408_dhr
    WHERE c408_dhr_id = p_dhrid;
    
    --validating lot tracking is available for company 
    SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL'))
		  , NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
	  INTO  v_plant_id, v_company_id 
	  FROM DUAL;

   -- If DHR is already voided throw an error
   IF (v_chkdhrvoidfl IS NOT NULL)
   THEN
      --
      raise_application_error (-20004, '');
   --
   END IF;

   -- IF DHR is already verified throw an error
   IF v_chkstatusfl = 4
   THEN
      GM_RAISE_APPLICATION_ERROR('-20999','320',''); 
   END IF;

   BEGIN
      SELECT t409.c409_ncmr_id, TO_NUMBER (t409.c409_status_fl),
             NVL (t408.c408_qty_rejected, 0),
             NVL (t409.c409_closeout_qty, 0), t409.c409_void_fl,
             t409.c901_type
        INTO v_ncmr_id, v_ncmr_sts_fl,
             v_qty_rejected,
             v_qty_closeout, v_ncmr_void_fl,
             v_ncmr_type
        FROM t408_dhr t408, t409_ncmr t409
       WHERE t408.c408_dhr_id = t409.c408_dhr_id
             AND t409.c408_dhr_id = p_dhrid
             AND t409.c409_void_fl IS NULL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_ncmr_id := '';
         v_ncmr_sts_fl := '';
         v_qty_rejected := 0;
         v_qty_closeout := 0;
   END;

   IF v_ncmr_sts_fl = 3 AND v_ncmr_type = 51062
   THEN
      GM_RAISE_APPLICATION_ERROR('-20999','321','');
   END IF;
   
   SELECT COUNT(1) INTO v_inhouse_cnt 
     FROM t412_inhouse_transactions 
    WHERE C412_REF_ID = p_dhrid  
    AND C412_VOID_FL IS NULL;
   
	-- Procedure call for voiding the associated In-House Transactions for the DHR ID.
	-- This is called only for Bulk DHR Process only based on the above v_inhouse_cnt variable. 
    IF v_inhouse_cnt = '0' AND v_chkstatusfl = 3 THEN   
	  gm_update_inhouse_rollback(p_dhrid,p_userid);
	END IF;

   /*Loading the Email Details to sent when user voids the DHR
   For PMT-2600 qty received column is added to show in notification email.
   T401 table joined to get PO raised by email id to send  mail(CC) to him whenever dhr is voided.*/
    OPEN p_email_details
		    FOR
		       SELECT t406.c406_invoice_id invid,
		              get_vendor_name (t408.c301_vendor_id) vname,
		              t408.c401_purchase_ord_id poid, t408.c408_dhr_id dhrid,
		              t408.c205_part_number_id partnm , t408.c408_qty_received qty, get_user_name (p_userid)
		                                                                       voidby,
		              TO_CHAR (SYSDATE, 'mm/dd/yyyy') voiddt,
		              get_user_emailid(t401.c401_created_by)PO_INITIATED_EMAIL
		         FROM t408_dhr t408, t406_payment t406 , T401_PURCHASE_ORDER t401
		        WHERE c408_dhr_id = p_dhrid
		          AND t408.c406_payment_id = t406.c406_payment_id
		          AND T401.C401_PURCHASE_ORD_ID =T408.C401_PURCHASE_ORD_ID
		          AND T401.c401_void_fl IS NULL;
		          /*while voiding DHR the payment detail also voided, If the invoice is linked with single DHR.
		          So, void flag check is not required for t408 and t406.*/
   IF p_action = 'UpdateRollback'
   THEN
      SELECT DECODE (c408_status_fl, '3', '2', '1')
        INTO v_dhr_status_fl
        FROM t408_dhr
       WHERE c408_dhr_id = p_dhrid;

      IF v_ncmr_id IS NOT NULL AND v_dhr_status_fl = 1
      THEN
         -- Rollback the NCMR
         gm_pkg_op_ncmr.gm_rollback_ncmr (v_ncmr_id, p_userid);
      END IF;

      -- we inspect the DHR after which we will process it. In case of any incorrect entry then we can rollback the
      -- transaction in pending verification status, that time it is going to pending inspection status.
      -- But It has to actually go to pending processing status after the DHR rollback. Inspect = 1,Process = 2,
      -- Verify = 3
      IF v_dhr_status_fl = 1
      THEN
         UPDATE t408_dhr
            SET c408_status_fl = v_dhr_status_fl,
                c408_inspected_by = NULL,
                c408_packaged_by = NULL,
                c408_qty_packaged = NULL,
                c408_inspected_ts = NULL,
                c408_packaged_ts = NULL,
                c408_qty_inspected = NULL,
                c408_qty_rejected = NULL,
                c408_last_updated_by = p_userid,
                c408_last_updated_date = SYSDATE
          WHERE c408_dhr_id = p_dhrid AND c408_void_fl IS NULL;
          
          --when rollback from pending process, need reset approve/reject status
           UPDATE t408a_dhr_control_number
		     SET c901_status = NULL,
		     c408a_last_updated_by = p_userid,
		     c408a_last_updated_date = sysdate
		    WHERE c408_dhr_id = p_dhrid AND c408a_void_fl IS NULL;
      ELSE
         UPDATE t408_dhr
            SET c408_status_fl = v_dhr_status_fl,
                c408_packaged_by = NULL,
                c408_qty_packaged = NULL,
                c408_packaged_ts = NULL,
                c408_last_updated_by = p_userid,
                c408_last_updated_date = SYSDATE
          WHERE c408_dhr_id = p_dhrid AND c408_void_fl IS NULL;          
      END IF;
      
      -- AS DHR is going to Void we are deleting the corresponding Lot Numbers from the T2550 table for avoiding the Duplicates.
	       DELETE FROM t2550_part_control_number
                 WHERE c205_part_number_id || c2550_control_number IN ( 
                       SELECT c205_part_number_id || c408a_conrol_number 
                         FROM t408a_dhr_control_number 
                        WHERE c408_dhr_id = p_dhrid
                          AND c408a_void_fl IS NULL )
                   AND c1900_company_id = v_company_id
                   AND c5040_plant_id = v_plant_id;

      --for rollback, need void DHR in inventory table
      gm_pkg_allocation.gm_inv_void_bytxn (p_dhrid, 93335, p_userid);
   ELSIF p_action = 'UpdateVoid'
   THEN
		      --Get invoice id from DHR ID.
		      BEGIN
		         SELECT t408.c406_payment_id, t406.c406_invoice_id, t406.c401_purchase_ord_id
		           INTO v_payment_id, v_invoice_id, v_poid
		           FROM t408_dhr t408, t406_payment t406
		          WHERE c408_dhr_id = p_dhrid
		            AND t408.c406_payment_id = t406.c406_payment_id
		            AND t408.c401_purchase_ord_id = t406.c401_purchase_ord_id
		            AND t408.c408_void_fl IS NULL
		            AND t406.c406_void_fl IS NULL;
		      EXCEPTION
		         WHEN NO_DATA_FOUND
		         THEN
		            v_payment_id := NULL;
		            v_invoice_id := NULL;
		      END;
		
		       --Get Donor id from DHR ID.
		      BEGIN
		         SELECT t408.c2540_donor_id
		           INTO v_donor_id
		           FROM t408_dhr t408
		          WHERE c408_dhr_id = p_dhrid
		            AND t408.c408_void_fl IS NULL;
		      EXCEPTION
		         WHEN NO_DATA_FOUND
		         THEN
		            v_donor_id := '';
		      END;
		
		   IF v_ncmr_id IS NOT NULL AND v_ncmr_void_fl IS NULL AND v_ncmr_type = 51062
		   THEN
		      GM_RAISE_APPLICATION_ERROR('-20999','322','');
		   END IF;
		
		  
		   -- DHR void code
		   IF v_invoice_id IS NOT NULL
		   THEN
		      SELECT COUNT (*)
		        INTO v_invoice_cnt
		        FROM t406_payment t406, t408_dhr t408
		       WHERE c406_invoice_id = v_invoice_id
		         AND t406.c406_payment_id = t408.c406_payment_id
		         AND t406.c401_purchase_ord_id = v_poid
		         AND t408.c408_void_fl IS NULL
		         AND t406.c406_void_fl IS NULL;
		   END IF;
		   IF v_invoice_cnt = 1
		   THEN
		      -- Call the following procedure to void the invoice id
		      gm_pkg_common_cancel.gm_ac_sav_void_ap_invoice (v_payment_id, p_userid);
		      p_inv_void_fl := 'Y';
		   ELSIF v_invoice_cnt > 1
		   THEN
		      p_inv_void_fl := 'N';
		   END IF;
		
		   	-- Need to void NCMR also while voiding DHR
		   IF v_ncmr_id IS NOT NULL
	       THEN
	          gm_pkg_op_ncmr.gm_rollback_ncmr (v_ncmr_id, p_userid);
	       END IF;
	       
	      -- AS DHR is going to Void we are deleting the corresponding Lot Numbers from the T2550 table for avoiding the Duplicates.
	       DELETE FROM t2550_part_control_number
                 WHERE c205_part_number_id || c2550_control_number IN ( 
                       SELECT c205_part_number_id || c408a_conrol_number 
                         FROM t408a_dhr_control_number 
                        WHERE c408_dhr_id = p_dhrid
                          AND c408a_void_fl IS NULL )
                   AND c1900_company_id = v_company_id
                   AND c5040_plant_id = v_plant_id;
	       
		   UPDATE t408_dhr
		      SET c408_void_fl = 'Y',
		          c408_last_updated_by = p_userid,
		          c408_last_updated_date = SYSDATE
		    WHERE c408_dhr_id = p_dhrid;
		
		   v_po_type := get_po_type (p_dhrid);
		
		   IF v_po_type = 3101
		   THEN
		      gm_pkg_op_dhr.gm_op_sav_dhr_posting (p_dhrid, NULL, 48212, p_userid);
		      gm_pkg_op_dhr.gm_op_sav_dhr_posting (p_dhrid, NULL, 48213, p_userid);
		   ELSE
		      gm_pkg_op_dhr.gm_op_sav_dhr_posting (p_dhrid, NULL, 48137, p_userid);
		   END IF;
		   
		   --Need to update the void flag of "T408A_DHR_CONTROL_NUMBER" table
		   
		   UPDATE t408a_dhr_control_number
			SET c408a_void_fl = 'Y',
				c408a_last_updated_by = p_userid,
				c408a_last_updated_date = SYSDATE
			WHERE c408_dhr_id = p_dhrid
			AND c408a_void_fl IS NULL;
		   
   END IF;
   --
   /* Void all the associated inhouse transactions */
   IF v_chkstatusfl = 3
   -- status = 3, processed and inhouse transactions created
   THEN
      UPDATE t412_inhouse_transactions t412
         SET t412.c412_void_fl = 'Y',
             t412.c412_last_updated_by = p_userid,
             t412.c412_last_updated_date = SYSDATE
       WHERE t412.c412_ref_id = p_dhrid;

      UPDATE t413_inhouse_trans_items t413
         SET t413.c413_void_fl = 'Y'
           , c413_last_updated_by = p_userid
	   	   , c413_last_updated_date = SYSDATE
       WHERE t413.c412_inhouse_trans_id IN (
                                           SELECT t412.c412_inhouse_trans_id
                                             FROM t412_inhouse_transactions t412
                                            WHERE t412.c412_ref_id = p_dhrid);
   END IF;
--
END gm_update_dhr_rollback;
/
