CREATE OR REPLACE PROCEDURE "GM_LS_FCH_SETCSGDETAILS_OUS" 
(
    p_consign_id   IN        	t504_consignment.c504_consignment_id%TYPE
   ,p_type         IN       	VARCHAR2
   ,p_recordset    OUT      	TYPES.cursor_type
   ,p_groupfl      IN       	VARCHAR2
   
)
AS
/***************************************************************************************************
  * This Procedure is to to Fetch loaner part details for selected Consignment ID
  * Q Refers to original Qty
  * Author : Joe P Kumar
  * Date   : 10/10/06
****************************************************************************************************/
   v_str   VARCHAR2 (2000);
BEGIN
--
   OPEN p_recordset
    FOR
--
       SELECT *
         FROM (SELECT   t205.c205_part_number_id pnum,
                        DECODE
                           (p_type,
                            'STOCK', get_qty_in_stock
                                                     (t205.c205_part_number_id),
                            0
                           ) stock,cons.cnum,
                       -- t205.c205_part_num_desc pdesc,
                       GET_TRANS_PARTNUM_DESC(t205.c205_part_number_id,p_consign_id) pdesc,
                        NVL(cons.qty, 0) - NVL (miss.qty, 0) qty,
                        t205.c205_loaner_price price,
                        (NVL (miss.qty, 0) + NVL (missing_qty, 0)) missqty,
                        NVL (miss.qtyrecon, 0) qtyrecon,
                        NVL(cons.qty, 0) +  NVL (missing_qty, 0)
                        - NVL (miss.qtyrecon, 0) cogsqty,
                        NVL (setdetails.qty, 0) setlistqty,setdetails.crfl cfl
                   FROM t205_part_number t205,
                        (SELECT   t505.c205_part_number_id,
                                  SUM (t505.c505_item_qty) qty, DECODE(p_groupfl,'Y','',t505.c505_control_number) cnum
                             FROM t505_item_consignment t505
                            WHERE t505.c504_consignment_id = p_consign_id
                              AND t505.c505_void_fl IS NULL
                         GROUP BY t505.c205_part_number_id, DECODE(p_groupfl,'Y','',t505.c505_control_number)) cons,
                        (SELECT   c205_part_number_id, SUM (qty) qty,
                                  SUM (qtyrecon) qtyrecon,
                                  SUM (missing_qty) missing_qty
                             FROM (SELECT t413.c205_part_number_id,
                                          DECODE
                                             (c412_type,
                                              50151, 0,
                                              DECODE
                                                    (t413.c413_status_fl,
                                                     'Q', NVL
                                                          (t413.c413_item_qty,
                                                           0
                                                          ),
                                                     0
                                                    )
                                             ) qty,
                                          DECODE
                                             (c412_type,
                                              50151, DECODE
                                                    (t413.c413_status_fl,
                                                     'Q', NVL
                                                          (t413.c413_item_qty,
                                                           0
                                                          ),
                                                     0
                                                    ),
                                              0
                                             ) missing_qty,
                                          DECODE
                                             (c412_type,
                                              50151, NVL (t413.c413_item_qty,
                                                          0
                                                         ),
                                              DECODE
                                                   (NVL (t413.c413_status_fl,
                                                         'Q'
                                                        ),
                                                    'Q', 0,
                                                    NVL (t413.c413_item_qty,
                                                         0)
                                                   )
                                             ) qtyrecon
                                     FROM t412_inhouse_transactions t412,
                                          t413_inhouse_trans_items t413
                                    WHERE t412.c412_inhouse_trans_id =
                                                    t413.c412_inhouse_trans_id
                                      AND t412.c412_ref_id = p_consign_id
                                      AND t412.c412_void_fl IS NULL
                                      AND t413.c413_void_fl IS NULL)
                         GROUP BY c205_part_number_id) miss,
                        (SELECT t208.c205_part_number_id,
                                t208.c208_set_qty qty, T208.C208_CRITICAL_TAG CRFL
                           FROM t208_set_details t208, t504_consignment t504
                          WHERE t208.c208_void_fl IS NULL
                            AND t208.c208_inset_fl = 'Y'
                            AND t504.c207_set_id = t208.c207_set_id
                            AND t504.c504_void_fl IS NULL
                            AND t504.c504_consignment_id = p_consign_id) setdetails
                  WHERE t205.c205_part_number_id = cons.c205_part_number_id
                    AND t205.c205_part_number_id = miss.c205_part_number_id(+)
                    AND t205.c205_part_number_id = setdetails.c205_part_number_id(+)
               UNION ALL
               SELECT   t208.c205_part_number_id pnum,
                        DECODE
                           (p_type,
                            'STOCK', get_qty_in_stock
                                                     (t208.c205_part_number_id),
                            0
                           ) stock,'' cnum,
                        get_partnum_desc (t208.c205_part_number_id) pdesc,
                        0 qty, 0 price, 0 missqty, 0 qtyrecon, 0 cogsqty,
                        t208.c208_set_qty qty, T208.C208_CRITICAL_TAG CFL
                   FROM t208_set_details t208, t504_consignment t504
                  WHERE t208.c208_void_fl IS NULL
                    AND t208.c208_inset_fl = 'Y'
                    AND t504.c207_set_id = t208.c207_set_id
                    AND t504.c504_void_fl IS NULL
                    AND t504.c504_consignment_id = p_consign_id
                    AND t208.c205_part_number_id NOT IN (
                           SELECT t505.c205_part_number_id
                             FROM t505_item_consignment t505
                            WHERE t505.c504_consignment_id = p_consign_id
                              AND t505.c505_void_fl IS NULL)
               ORDER BY 1)
        WHERE qty != 0 OR setlistqty != 0
        ORDER BY CFL,PNUM;
END GM_LS_FCH_SETCSGDETAILS_OUS;
/