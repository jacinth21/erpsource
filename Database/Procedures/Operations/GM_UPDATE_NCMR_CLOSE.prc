CREATE OR REPLACE PROCEDURE GM_UPDATE_NCMR_CLOSE
(
	   p_NCMRId				IN T409_NCMR.C409_NCMR_ID%TYPE,
	   p_CloseDate			IN VARCHAR2,
	   p_CapaFl				IN T409_NCMR.C409_CAPA_FL%TYPE,
	   p_UserId				IN T409_NCMR.C409_LAST_UPDATED_BY%TYPE,
	   p_message 			OUT  VARCHAR2
)
AS
v_dhr_id t408_dhr.c408_dhr_id%TYPE;
v_part_num t205_part_number.c205_part_number_id%TYPE;

BEGIN
              UPDATE T409_NCMR SET
					C409_CAPA_FL = p_CapaFl,
					C409_CLOSEOUT_DATE = to_date(p_CloseDate,'mm/dd/yyyy'),
					C409_STATUS_FL = '3',
					C409_CLOSEOUT_BY = p_UserId		
			  WHERE C409_NCMR_ID = p_NCMRId;
			  
	-- Find out the DHR ID and Part number for the NCMR transaction.
	-- Checking RTS fl to 0 to make sure the part is not dispute and returned to suplier.
	      BEGIN 		  
		      
	        SELECT t408.c408_dhr_id INTO v_dhr_id
		      FROM t408_dhr t408, t409_ncmr t409
		        WHERE t408.c408_dhr_id       = t409.c408_dhr_id
			    AND t409.c409_rts_fl         = 0
			    AND t409.c409_void_fl       IS NULL
			    AND t409.c409_status_fl      = 3
			    AND t409.c409_ncmr_id=p_NCMRId;
			    
			-- Call DHR Priority update function to assign the priority for the DHR ID: PMT-20850    
			    gm_pkg_dhr_priority.gm_dhr_priority_update(v_dhr_id,p_UserId);
			    
		    EXCEPTION WHEN OTHERS
		     THEN
		      v_dhr_id:=NULL;
		   END;
			
			  EXCEPTION WHEN OTHERS
      	 	  THEN
               p_message := SQLERRM;
              RETURN;
              
END GM_UPDATE_NCMR_CLOSE;
/
