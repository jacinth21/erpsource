/* Formatted on 2011/02/11 15:57 (Formatter Plus v4.8.0) */
-- @"C:\Database\Procedures\Operations\GM_LS_UPD_INLOANERCSG.prc";

CREATE OR REPLACE PROCEDURE gm_ls_upd_inloanercsg (
   p_txnid         IN       t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
   p_type          IN       t412_inhouse_transactions.c412_type%TYPE,
   p_purpose       IN       t412_inhouse_transactions.c412_inhouse_purpose%TYPE,
   p_shipto        IN       t412_inhouse_transactions.c412_release_to%TYPE,
   p_shiptoid      IN       t412_inhouse_transactions.c412_release_to_id%TYPE,
   p_fincomments   IN       t412_inhouse_transactions.c412_comments%TYPE,
   p_userid        IN       t412_inhouse_transactions.c412_last_updated_by%TYPE,
   p_str           IN       VARCHAR2,
   p_consignid     IN       t412_inhouse_transactions.c412_ref_id%TYPE,
   p_action        IN       VARCHAR2,
   p_message       OUT      VARCHAR2
)
/**************************************************************
* This Procedure is to Update InHouse Loaner Consignment
* Author : Joe P Kumar
* Date    : 10/10/06
* Algorithm :
* The 3 status / phases involved are
*  1. Place Order -- Could be for adding parts to Set or Removing Parts from Set
*        a.Call Gm_Save_Inhouse_Item_Consign to update the details of the InHouse COnsignment in T412_INHOUSE_TRANSACTIONS
*          and the corresponding part information in T413_INHOUSE_TRANS_ITEMS
*        b. Update c412_status_fl to 2
*        c. Call Gm_Ls_Upd_Csg_Loanerstatus to Update the IHL Consignment status in T504A_CONSIGNMENT_LOANER
*
*  2. SaveControl -- Once when the control Numbers are entered for the parts
*        a. Call Gm_Save_Inhouse_Item_Build to update the control # info in   T413_INHOUSE_TRANS_ITEMS
*        b. Update c412_status_fl to 3
*        c. Call Gm_Ls_Upd_Csg_Loanerstatus to Update the IHL Consignment status in T504A
*
*  3. Verify -- Once when the new parts are verified and signed Off
*        a. Verify if the Update Status Flag (c412_update_inv_fl) is NULL
*        b. If the Transaction Type (c412_type) is 50150, then the Operation is to Add Items to Set.
*           Update the Inventory through Gm_Update_Inventory.
*                 a. Get Rule for (50150,'INHOUSE-LOANER')
*                 b. Decrement the Qty in Stock
*                 c. Perform Ledger Posting
*        c. If the Transaction Type (c412_type) is 50151, then the Operation is to Remove From Set
*           Update the Inventory through Gm_Update_Inventory.
*                 a. Get Rule for (50151,'INHOUSE-LOANER')
*                 b. Perform Ledger Posting
*        d. Update the Consignment details onto T505_ITEM_CONSIGNMENT from T413_INHOUSE_TRANS_ITEMS
*           For 50151, the Item Qty would be multiplied with -1
*        e. Update InHouse transactions table T412_INHOUSE_TRANSACTIONS to set the following variables
*                 a. c412_UPDATE_INV_FL to 1
*                 b. c412_verify_fl to 1
*                 c. c412_status_fl to 4
*        f. Call Gm_Ls_Upd_Csg_Loanerstatus to Update the IHL Consignment status in T504A
*
* --  Description of Status in T412_INHOUSE_TRANSACTIONS
*     1 -
*     2 - Pending Control Number
*     3 - Pending verification
*     4 - Verified
*
* --  Description of Status in T504A_CONSIGNMENT_LOANER
*     0 - Available for Shipping
*     1 - Pending Shipping
*     2 - Pending Return
*     3 - WIP (Work-In-Progress)
*     4 - Pending verification
* -- For "Loaner extension w/Replenish", add  '50154', 'PDL-PLUS' in  "SELECT DECODE (v_txn_type..)"
      50154 = Move from shelf to Loaner Extension w/Replenish
*
 **************************************************************/
AS
--
   v_update_inv_flag   t412_inhouse_transactions.c412_update_inv_fl%TYPE;
   v_txn_type          t412_inhouse_transactions.c412_type%TYPE;
   v_sign              NUMBER (1)   := 1;
   v_action            VARCHAR2 (100);
   v_setid             t207_set_master.c207_set_id%TYPE;
   v_type              t504_consignment.c504_type%TYPE;
   v_code_grp          VARCHAR2 (10);
   v_inv_type          NUMBER;
   v_table			   VARCHAR2(20);	
   v_fl		   		   CHAR (1);
   v_ref_id            t412_inhouse_transactions.c412_ref_id%TYPE;
   v_po_type           NUMBER; 
   v_rework_id         t906_rules.c906_rule_value%TYPE;
   v_part_price   	   VARCHAR2(20);
   v_part              VARCHAR2 (1000);
   v_consign_id        VARCHAR2(20);
   v_back_ord_id	   t412_inhouse_transactions.c412_ref_id%TYPE;	
   v_back_ord_qty	   NUMBER;	
   v_dhr_status 	   t408_dhr.c408_status_fl%TYPE;
   v_dhr_id            t408_dhr.c408_dhr_id%TYPE;
   v_dhr_cnt           NUMBER;
   v_company_id        t1900_company.c1900_company_id%TYPE;
   v_context_cmp_id    T1900_company.c1900_company_id%TYPE;
   v_plant_id           t5040_plant_master.c5040_plant_id%TYPE;
   v_trans_company_id   t1900_company.c1900_company_id%TYPE;
   v_trans_plant_id     t5040_plant_master.c5040_plant_id%TYPE;
--
	--
	CURSOR cur_item_price
	 IS
		SELECT t412.c412_inhouse_trans_id, t413.C413_TRANS_ID transid, t413.c413_item_price price 
  		, t413.c205_part_number_id pnum, t2052.c2052_equity_price eprice 
   		FROM t413_inhouse_trans_items t413, ( 
         		SELECT c205_part_number_id, c2052_equity_price 
           		FROM t2052_part_price_mapping 
          		WHERE c1900_company_id = v_company_id 
            	AND c2052_void_fl IS NULL 
    	) 
    	t2052, t412_inhouse_transactions t412 
  	WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id 
    AND t412.c412_inhouse_trans_id = p_consignid 
    AND t413.c205_part_number_id = t2052.c205_part_number_id(+) 
    AND t413.c413_void_fl IS NULL 
    AND c413_item_price IS NULL 
    AND t412.c412_void_fl IS NULL 
    AND t412.c1900_company_id = v_company_id; 

BEGIN
--		
       
    SELECT get_compid_frm_cntx(),get_plantid_frm_cntx()
	  INTO  v_context_cmp_id,v_plant_id 
		FROM DUAL;
    	
   IF (p_action = 'PlaceOrder')
   THEN
	
   	IF p_consignid IS NOT NULL THEN	
   	  gm_pkg_common.gm_fch_trans_comp_plant_id(p_consignid,v_trans_company_id,v_trans_plant_id);
   	  IF (v_trans_company_id IS NOT NULL) AND (v_context_cmp_id != v_trans_company_id) THEN
         gm_pkg_cor_client_context.gm_sav_client_context(v_trans_company_id,v_plant_id);
      END IF; 
   	END IF;  
	
      gm_save_inhouse_item_consign (p_txnid,
                                    p_type,
                                    p_purpose,
                                    p_shipto,
                                    p_shiptoid,
                                    p_fincomments,
                                    p_userid,
                                    p_str,
                                    p_message
                                   );

      --
      UPDATE t412_inhouse_transactions
         SET c412_status_fl = '2',
             c412_ref_id = p_consignid,
             c412_last_updated_by = p_userid,
             c412_last_updated_date = CURRENT_DATE
       WHERE c412_inhouse_trans_id = p_txnid;

      --
      gm_ls_upd_csg_loanerstatus (2, 30, p_consignid);
   END IF;

   SELECT     c412_update_inv_fl, c412_type,c412_ref_id
         INTO v_update_inv_flag, v_txn_type, v_ref_id
         FROM t412_inhouse_transactions
        WHERE c412_inhouse_trans_id = p_txnid
   FOR UPDATE;

   IF p_action = 'SaveControl' OR p_action = 'ReleaseControl'
   THEN
      --
      IF p_action = 'ReleaseControl'
      THEN
         UPDATE t412_inhouse_transactions
            SET c412_status_fl = '3',
                c412_last_updated_by = p_userid,
                c412_last_updated_date = CURRENT_DATE
          WHERE c412_inhouse_trans_id = p_txnid;

         --
         IF v_txn_type NOT IN ('50154','1006575')
         THEN
            gm_ls_upd_csg_loanerstatus (3, 40, p_consignid);
         END IF;
      END IF;

      --
           --
      gm_save_inhouse_item_build (p_txnid, p_str, p_userid, p_message, p_action);

      IF p_message IS NOT NULL
      THEN
         gm_pkg_common_cancel_op.gm_cm_sav_cancelrow (p_txnid,
                                                      100405,
                                                      'VPTDO',
                                                      p_message,
                                                      p_userid
                                                     );
      END IF;
   END IF;

   IF (p_action = 'Verify')
   THEN
   --verify whether the transaction is related to DHR
        SELECT COUNT(*) INTO v_dhr_cnt
		   FROM t901_code_lookup
		  WHERE c901_code_grp   IN ('DHRLC', 'MWLC', 'SHLC') --MWLC-Manf location ,SHLC-Supply location ,DHRLC - Regular
		    AND c902_code_nm_alt = TO_CHAR(v_txn_type)
		    AND c901_active_fl   = 1
		    AND c901_void_fl    IS NULL;
	
		    --If transaction is related to DHR then fetching  the DHR status and ID
		    IF v_dhr_cnt > 0 THEN
		     BEGIN
			     SELECT T408.c408_status_fl,T408.c408_dhr_id INTO v_dhr_status,v_dhr_id
				   FROM t412_inhouse_transactions T412, t408_dhr T408
				  WHERE T412.c412_inhouse_trans_id = p_txnid
				    AND T412.c412_void_fl         IS NULL
				    AND T412.c412_type             = v_txn_type
				    AND T412.c412_ref_id           = T408.c408_dhr_id
				    AND T408.c408_void_fl         IS NULL;
			      EXCEPTION
        			 WHEN NO_DATA_FOUND
         			 THEN
         			 --this is to check if DHR associated with Bulck DHR
         			 BEGIN
         			 	SELECT gm_pkg_op_bulk_dhr_rpt.get_bulk_dhr_status(t412.c412_ref_id), t412.c412_ref_id
						       INTO v_dhr_status,v_dhr_id
						   FROM t4082_bulk_dhr_mapping t4082, t412_inhouse_transactions t412 
						  WHERE t4082.c4081_dhr_bulk_id    = t412.c412_ref_id 
						    AND t412.c412_inhouse_trans_id = p_txnid
						    AND t412.c412_type             = v_txn_type
						    AND T412.c1900_company_id     = v_context_cmp_id
						    AND t4082.c4082_void_fl       IS NULL
						    AND t412.c412_void_fl         IS NULL 
                          GROUP BY  t412.c412_ref_id; 
					 EXCEPTION
	        			 WHEN NO_DATA_FOUND
	         			 THEN							  
	            			v_dhr_status := '';
            	 	END;
      		   END;
      		   --If dhr is not verfied then throwing error
			      IF v_dhr_status <> '4' THEN --dhr verified status 4
			       GM_RAISE_APPLICATION_ERROR('-20999','313',v_dhr_id);
			      END IF;		    
		    END IF;
		
		BEGIN
		SELECT  c1900_company_id
    	INTO    v_company_id
    	FROM    t412_inhouse_transactions where c412_inhouse_trans_id = p_txnid and C412_VOID_FL is null;
    	EXCEPTION
 		WHEN NO_DATA_FOUND
    	THEN
      			v_company_id := NULL;
      	END;
      		   
		FOR item_val IN cur_item_price
		LOOP
		    		
		    IF item_val.eprice IS NULL
		    THEN
		        v_part := v_part || item_val.pnum || ',';
		    ELSE
		        UPDATE t413_inhouse_trans_items
		           SET c413_item_price = item_val.eprice
       	 			 , c413_last_updated_by = p_userid
	   	 			 , c413_last_updated_date = CURRENT_DATE		           
		         WHERE c413_trans_id = item_val.transid;
		    END IF;
		END LOOP;
		
		IF v_part IS NOT NULL
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','314',v_part||'#$#'||p_consignid);
		END IF;

	  /* For  new transactions make entry for 
	   * ACTION and SIGN in rule table to avoid
	   * editing the following IF else statement
	   */		
      SELECT get_rule_value ('ACTION', p_type),
             get_rule_value ('SIGN', p_type),
             get_rule_value('TRANS_TABLE',p_type)
        INTO v_action,
             v_sign,
             v_table
        FROM DUAL;
        
      
        
     /* IF rule table has no entry for ACTION and SIGN 
	  * then execute the follwoing if else statement
	  */   
      IF (v_action IS NULL OR v_sign IS NULL)
      THEN
         -- Transactions for Adding Items into Set
         -- Updating Inventory and Posting
         -- add the type '50154': move from Shelf to Loaner Extension w/Replenish
         SELECT DECODE (v_txn_type,
                        '50150', 'IHL-PLUS'           -- Shelf to In-House Set
                                           ,
                        '50151', 'IHL-MINUS'          -- In-House Set to Scrap
                                            ,
                        '50152', 'IHL-MINUS-FG'       -- In-House Set to Shelf
                                               ,
                        '50153', 'IHL-MINUS-QR'  -- In-House Set to Quarantine
                                               ,
                        '50154', 'PDL-PLUS'
                                           -- Shelf to Loaner Set (For Replenish)
                ,
                        '50155', 'PDL-PLUS'             -- Shelf to Loaner Set
                                           ,
                        '50156', 'PDL-MINUS'            -- Loaner Set to Scrap
                                            ,
                        '50157', 'PDL-MINUS-FG'         -- Loaner Set to Shelf
                                               ,
                        '50158', 'PDL-MINUS-QR'    -- Loaner Set to Quarantine
                                               ,
                        '50159', 'PDL-MINUS-LS'  -- Loaner Set to Loaner Sales                        
                                               ,
					    '1006571', 'PDL-MINUS-LS'  -- Inhouse Loaner Set Missing Parts
							                   ,                            
                        '50160', 'BSP-PLUS-BS'          -- Shelf to Built Sets
                                              ,
                        '50161', 'BSP-MINUS-BS'         -- Built Sets to Shelf
                                               ,
                        '50162', 'BSP-MINUS-QR'    -- Built Sets to Quarantine
                       ),
                DECODE (v_txn_type,
                        '50150', 1,
                        '50154', 1,
                        '50155', 1,
                        '50160', 1,
                        -1
                       )
           INTO v_action,
                v_sign
           FROM DUAL;
      END IF;

      --
      IF v_update_inv_flag IS NULL 
      THEN
        IF v_table IS NULL THEN
         gm_update_inventory (p_txnid,
                              v_txn_type,
                              v_action,
                              p_userid,
                              p_message
                             );

         -- Updating Inventory Flag in InHouse Transactions Table
         UPDATE t412_inhouse_transactions
            SET c412_update_inv_fl = '1',
                c412_verified_by = p_userid,
                c412_verified_date = CURRENT_DATE,
                c412_verify_fl = '1',
                c412_status_fl = '4',
                c412_last_updated_date = CURRENT_DATE,
                c412_last_updated_by = p_userid
          WHERE c412_inhouse_trans_id = p_txnid;
          
          
         --Removed ,'50151' in below if condtn. Brinal Email
         IF v_txn_type NOT IN ('50161', '50162') 
         THEN
               
         		IF v_txn_type = '1006573' OR v_txn_type = '1006570' THEN --IHIS OR BLIS need to get the CN id	    		
               			v_consign_id := p_consignid;
	    		 		gm_close_ih_set_back_order(p_txnid, v_txn_type, v_consign_id);
	    		ELSE
	    		        v_consign_id:=p_consignid;
	    		END IF;
	    		
            -- Updating T505 Item Consignment Table
            INSERT INTO t505_item_consignment
                        (c505_item_consignment_id, c504_consignment_id,
                         c205_part_number_id, c505_control_number,
                         c505_item_qty, c505_item_price, c901_type,
                         c505_ref_id)
               SELECT s504_consign_item.NEXTVAL, v_consign_id,
                      c205_part_number_id, c413_control_number,
                      c413_item_qty * v_sign, c413_item_price, v_txn_type,
                      p_txnid
                 FROM t413_inhouse_trans_items
                WHERE c412_inhouse_trans_id = p_txnid;
    
              --When ever the detail is updated, the consignment table has to be updated ,so ETL can Sync it to GOP.
			   UPDATE T504_Consignment
			      SET c504_last_updated_by = p_userid
			        , c504_last_updated_date = CURRENT_DATE
			    WHERE C504_Consignment_Id = v_consign_id;
									    
            IF v_txn_type NOT IN ('50160','50154', '1006575') 
            THEN
               gm_ls_upd_csg_loanerstatus (4, 50, v_consign_id);
            END IF;
            
         END IF;
         ELSE 
			IF p_type = '100065' OR p_type = '100070' OR p_type = '100066' OR p_type = '100069' THEN
				v_po_type := get_po_type(v_ref_id);

				IF v_po_type = 3101 -- Rework
				THEN
					SELECT get_rule_value ('REWORK_TXN_REMAP', v_txn_type)
					  INTO v_rework_id
					FROM DUAL;
				END IF;
			END IF;
	    IF p_type = '100065' OR p_type = '100066' OR p_type = '100068' OR p_type = '100079' OR p_type = '100082' 
	    OR p_type = '104621' OR p_type = '104622' THEN 
		-- 100065-DHR to Bulk, 100066-DHR to Repack,100067-DHR to Finished Goods,100068-DHR to Quarantine
		-- 100082-Rework DHR to Repack,100078-Rework DHR to Finished goods,100079-Rework DHR to Bulk
		--104620-RSFG, 104621-RSPN,104622-RSQN
		gm_pkg_op_lot_track.gm_lot_track_main(NULL,
	                               NULL,
	                               p_txnid,
	                               p_userid,
	                               90801, -- DHR  
	                               4302,-- MINUS
	                               50982 -- DHR
	                               );
	    END IF;

		IF v_rework_id IS NOT NULL
		THEN
				gm_pkg_op_inventory_qty.gm_sav_inventory_main(p_txnid,v_rework_id,p_userid);
		ELSE
         		gm_pkg_op_inventory_qty.gm_sav_inventory_main(p_txnid,v_txn_type,p_userid);
		END IF;
         	
         	  -- Updating Inventory Flag in InHouse Transactions Table
         UPDATE t412_inhouse_transactions
            SET c412_update_inv_fl = '1',
                c412_verified_by = p_userid,
                c412_verified_date = CURRENT_DATE,
                c412_verify_fl = '1',
                c412_status_fl = '4',
                c412_last_updated_date = CURRENT_DATE,
                c412_last_updated_by = p_userid
          WHERE c412_inhouse_trans_id = p_txnid;
         	
         END IF;	
      END IF;

      IF v_txn_type IN ('50154','1006575')
      THEN
--chk if this needs to be called under any condition
         gm_pkg_cm_shipping_trans.gm_sav_release_shipping (p_txnid,
                                                           50183,
                                                           p_userid,
                                                           ''
                                                          );
      END IF;

      gm_pkg_op_inv_scan.gm_check_inv_order_status (p_txnid,
                                                    'Completed',
                                                    p_userid
                                                   );
   END IF;

   IF     (p_action = 'SaveControl' OR p_action = 'ReleaseControl')
      AND v_txn_type IN ('50154', '50155', '50150', '50160','1006575')
   THEN
      -- When Request is Completed, Wipe out from the Inventory Pick queue Table.
      gm_pkg_op_inv_scan.gm_check_inv_order_status (p_txnid,
                                                    'Completed',
                                                    p_userid
                                                   );
   END IF;

   -- Allocation logic
   IF (p_action = 'ReleaseControl' AND v_txn_type IN ('50157', '50152'))
   THEN
      v_code_grp := 'INVPW';

      BEGIN
         SELECT c901_code_id
           INTO v_inv_type
           FROM t901_code_lookup t901
          WHERE t901.c901_code_grp = v_code_grp
            AND t901.c902_code_nm_alt = TO_CHAR (v_txn_type)
            AND t901.c901_active_fl = '1';

         gm_pkg_allocation.gm_ins_invpick_assign_detail (v_inv_type,
                                                         p_txnid,
                                                         p_userid
                                                        );
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            NULL;
      END;
   END IF;
 
BEGIN
     SELECT c412_update_inv_fl
       INTO v_update_inv_flag
       FROM t412_inhouse_transactions
      WHERE c412_inhouse_trans_id = p_txnid  FOR UPDATE;
EXCEPTION
WHEN NO_DATA_FOUND THEN
    v_update_inv_flag := NULL;
END;

 if ((p_type='50158' or p_type='50156' or p_type='100063') AND (v_update_inv_flag='1')) THEN
 gm_pkg_op_inv_field_sales_tran.gm_sav_loaner_lot(p_txnid,p_userid);
 
 END IF;
 
END gm_ls_upd_inloanercsg;
/
