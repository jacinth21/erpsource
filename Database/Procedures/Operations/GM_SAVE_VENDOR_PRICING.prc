/* Formatted on 2009/08/12 18:22 (Formatter Plus v4.8.0) */
--@"C:\database\Procedures\Operations\gm_save_vendor_pricing.prc"

CREATE OR REPLACE PROCEDURE gm_save_vendor_pricing (
	p_str		IN	 VARCHAR2
  , p_partnum	IN	 t405_vendor_pricing_details.c205_part_number_id%TYPE
  , p_userid	IN	 t405_vendor_pricing_details.c405_created_by%TYPE
)
AS
--
/********************************************************************************************
 * Description	:This procedure is used for saving Vendor Pricing Details
 * Change Details:
 *		 James	May29,2006	Changed Formatting according to Standards
		 James	May29,2006	Changes for UOM
 *********************************************************************************************/
--
	v_strlen	   NUMBER := NVL (LENGTH (p_str), 0);
	v_string	   VARCHAR2 (30000) := p_str;
	v_substring    VARCHAR2 (1000);
--
	v_vid		   VARCHAR2 (20);
	v_pid		   NUMBER;
	v_cost		   VARCHAR2 (20);
	v_fl		   VARCHAR2 (1);
	v_price_id	   NUMBER;
	v_qtyq		   VARCHAR2 (4000);
	v_dframe	   VARCHAR2 (72);
	v_qid		   VARCHAR2 (20);
	v_uom		   VARCHAR2 (20);
	v_uomqty	   VARCHAR2 (20);
	v_lockfl	   CHAR (1);
	v_activefl	   CHAR (1);
	v_wocount	   NUMBER;
	v_oldcost	   NUMBER;
	v_company_id  t1900_company.c1900_company_id%TYPE;
--
BEGIN
	IF v_strlen > 0
	THEN
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_vid		:= NULL;
			v_cost		:= NULL;
			v_qtyq		:= NULL;
			v_dframe	:= NULL;
			v_qid		:= NULL;
			v_uom		:= NULL;
			v_uomqty	:= NULL;
			v_lockfl	:= NULL;
			v_fl		:= NULL;
			v_activefl	:= NULL;
			v_pid		:= NULL;
			--
			v_vid		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_cost		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_qtyq		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_dframe	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_qid		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_uom		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_uomqty	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_lockfl	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_activefl	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_fl		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_pid		:= TO_NUMBER (v_substring);

			--
			IF v_fl = 'I'
			THEN
				SELECT s405_vend_price_id.NEXTVAL, get_compid_frm_cntx()
				  INTO v_price_id, v_company_id
				  FROM DUAL;

				INSERT INTO t405_vendor_pricing_details
							(c405_pricing_id, c301_vendor_id, c205_part_number_id, c405_cost_price, c405_active_fl
						   , c405_created_date, c405_created_by, c405_qty_quoted, c405_delivery_frame
						   , c405_vendor_quote_id, c901_uom_id, c405_uom_qty, c405_lock_fl, c1900_company_id
							)
					 VALUES (v_price_id, v_vid, p_partnum, v_cost, v_activefl
						   , CURRENT_DATE, p_userid, v_qtyq, v_dframe
						   , v_qid, v_uom, v_uomqty, v_lockfl, v_company_id
							);
			ELSIF v_fl = 'U'
			THEN
				SELECT c405_cost_price
				  INTO v_oldcost
				  FROM t405_vendor_pricing_details
				 WHERE c405_pricing_id = v_pid
				 AND c405_void_fl IS NULL;

				IF v_oldcost <> TO_NUMBER (v_cost)
				THEN
					SELECT COUNT (c402_work_order_id)
					  INTO v_wocount
					  FROM t402_work_order
					 WHERE c405_pricing_id = v_pid AND c402_void_fl IS NULL;

					IF v_wocount > 0
					THEN
						raise_application_error (-20163, '');
					END IF;
				END IF;

				UPDATE t405_vendor_pricing_details
				   SET c301_vendor_id = v_vid
					 , c405_cost_price = v_cost
					 , c405_qty_quoted = v_qtyq
					 , c405_delivery_frame = v_dframe
					 , c405_vendor_quote_id = v_qid
					 , c901_uom_id = v_uom
					 , c405_uom_qty = v_uomqty
					 , c405_lock_fl = v_lockfl
					 , c405_active_fl = v_activefl
					 , c405_last_updated_by = p_userid
					 , c405_last_updated_date = CURRENT_DATE
				 WHERE c405_pricing_id = v_pid
				 AND c405_void_fl IS NULL;
			END IF;
		END LOOP;
	END IF;
END gm_save_vendor_pricing;
/
