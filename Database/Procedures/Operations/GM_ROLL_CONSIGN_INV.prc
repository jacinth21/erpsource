CREATE OR REPLACE PROCEDURE GM_ROLL_CONSIGN_INV
(
 p_ConsignId IN t505_item_consignment.c504_consignment_id%TYPE,
 p_user_id IN t504_consignment.c504_last_updated_by%TYPE,
 p_errmsg    OUT VARCHAR2
)
AS
--
p_InvFl  VARCHAR2(10);
v_msg  VARCHAR2(100);
--
BEGIN
--
 UPDATE T504_CONSIGNMENT
 SET  C504_STATUS_FL  = '1',
   C504_VERIFY_FL  = '0',
   C504_VERIFIED_BY = '',
   C504_VERIFIED_DATE = '',
   C504_UPDATE_INV_FL = '',
   c504_last_updated_by = p_user_id,
   c504_last_updated_date = SYSDATE
 WHERE C504_CONSIGNMENT_ID = p_ConsignId
	   AND C504_VOID_FL IS NULL;
 --
 SELECT  c504_update_inv_fl
 INTO  p_InvFl
 FROM  t504_consignment
 WHERE  c504_consignment_id = p_consignid
		AND C504_VOID_FL IS NULL;
 --
 IF p_InvFl IS NULL THEN
  GM_UPDATE_INVENTORY(p_ConsignId,'CROLL','PLUS',p_user_id, v_msg);
 END IF;
--
END GM_ROLL_CONSIGN_INV;
/

