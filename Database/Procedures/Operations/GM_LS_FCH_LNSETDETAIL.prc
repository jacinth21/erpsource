--@"C:\Database\Procedures\Operations\GM_LS_FCH_LNSETDETAIL.prc";
create or replace
PROCEDURE gm_ls_fch_lnsetdetail (
   p_consign_id   IN       t504_consignment.c504_consignment_id%TYPE,
   p_recordset    OUT      TYPES.cursor_type
)
AS
BEGIN
OPEN p_recordset
    FOR
select b.pnum pnum, get_trans_partnum_desc(b.pnum,p_consign_id) pdesc, '' cnum, b.qty oqty, nvl((b.qty-a.qty) ,b.qty) qty							
	from						
	(select count(1), t413.c205_part_number_id pnum, sum(t413.c413_item_qty) qty						
		 from t412_inhouse_transactions t412, t413_inhouse_trans_items t413					
		 where t412.c412_ref_id =p_consign_id
		 and t413.c412_inhouse_trans_id = t412.c412_inhouse_trans_id					
		 and t412.c412_type IN (50159,50156,50158,50157)					
     and NVL(DECODE (CASE							
						   WHEN t412.c412_type = 50159	
							   THEN
                      CASE							
                        WHEN t413.c413_status_fl = 'W'							
                        THEN -999							
                        WHEN t413.c413_status_fl = 'Q'							
                        -- when the Loaner Usage is Controlled, the stock sheet should reflect it.							
                        THEN -999							
                        WHEN t413.c413_status_fl = 'R'							
                        THEN -999							
                      END							
						   WHEN t412.c412_type = 50158 or t412.c412_type = 50157 or t412.c412_type = 50156	
               THEN -9999							
               END							
					 , -999, -999,-9999		
					  ),-999) = -999		
		 and t412.c412_void_fl IS NULL					
		 group by t413.c205_part_number_id) a,					
	(select T504.*, T208.C208_CRITICAL_TAG ctag from 						
       (select count(1),t505.c205_part_number_id pnum, sum(t505.c505_item_qty) qty,  t504.c207_set_id setid							
                                from t505_item_consignment t505, t504_consignment t504 							
                                 where t505.c504_consignment_id = p_consign_id						
                                and t505.c504_consignment_id = t504.c504_consignment_id							
                                and T504.C504_VOID_FL is null							
				and T505.C505_VOID_FL is null			
     group by T505.C205_PART_NUMBER_ID, T504.C207_SET_ID) T504, T208_SET_DETAILS T208							
     where T504.PNUM = T208.C205_PART_NUMBER_ID (+) 							
     AND T504.setid = T208.C207_SET_ID (+)							
     AND T208.c208_void_fl IS NULL							
     order by t208.c208_critical_tag) b							
	Where b.pnum = a.pnum (+)	
	and nvl((b.qty-a.qty) ,b.qty) > 0						
  order by ctag, pnum;			

 END gm_ls_fch_lnsetdetail;
 /