/* Formatted on 2009/05/13 13:44 (Formatter Plus v4.8.0) */
--	@ "C:\Database\Procedures\Operations\gm_op_fch_vendor_info_rpt.prc"

CREATE OR REPLACE PROCEDURE gm_op_fch_vendor_info_rpt (
	p_vendor_id    IN		t301_vendor.c301_vendor_id%TYPE
  , p_location	   IN		t301_vendor.c301_bill_state%TYPE
  , p_month 	   IN		NUMBER
  , p_project_id   IN		t205_part_number.c202_project_id%TYPE
  , p_active_fl    IN		t301_vendor.c301_active_fl%TYPE
  , p_po_type	   IN		t401_purchase_order.c401_type%TYPE
  , p_out_cursor   OUT		TYPES.cursor_type
)
AS
v_compid_frm_cntx VARCHAR2(50);
BEGIN
	SELECT get_compid_frm_cntx() INTO v_compid_frm_cntx  FROM DUAL;
	
	OPEN p_out_cursor
	 FOR
		 SELECT   t301.c301_vendor_id ID, NVL (c301_vendor_sh_name, c301_vendor_name) NAME
				, NVL (work_order.imp_count, 0) imp_count, NVL (work_order.ins_count, 0) ins_count
				, NVL (work_order.core_imp_count, 0) core_imp_count, NVL (work_order.core_ins_count, 0) core_ins_count
				, NVL (work_order.non_core_imp_count, 0) non_core_imp_count
				, NVL (work_order.non_core_ins_count, 0) non_core_ins_count, NVL (ncmr.past_six_months, 0) nc_6
				, NVL (ncmr.six_months_to_one_year, 0) nc_6_12, NVL (late_deliveries.dhr_six_months, 0) ld_6
				, NVL (late_deliveries.dhr_six_twelve, 0) ld_6_12, NVL (ncmr_repeat_three_six.cnt, 0) ncmr_rpt_cnt_6
				, NVL (ncmr_repeat_six_twelve.cnt, 0) ncmr_rpt_cnt_6_12, NVL (open_wo_three_six.cnt, 0) open_po_cnt_6
				, NVL (open_wo_three_six.qty, 0) open_po_qty_6, NVL (open_wo_six_twelve.cnt, 0) open_po_cnt_6_12
				, NVL (open_wo_six_twelve.qty, 0) open_po_qty_6_12
				, ROUND (NVL (overall_business_three_six.per, 0), 2) per_business_6
				, ROUND (NVL (overall_business_six_twelve.per, 0), 2) per_business_6_12
			 FROM t301_vendor t301
				, (SELECT	c301_vendor_id, SUM (imp_count) imp_count, SUM (ins_count) ins_count
						  , SUM (core_imp_count) core_imp_count, SUM (core_ins_count) core_ins_count
						  , SUM (non_core_imp_count) non_core_imp_count, SUM (non_core_ins_count) non_core_ins_count
					   FROM (SELECT   t402.c301_vendor_id, DECODE (t205.c205_product_family, 4050, 1, 0) imp_count
									, DECODE (t205.c205_product_family, 4051, 1, 0) ins_count
									, (CASE
										   WHEN t205.c205_core_part_fl = 'Y' AND t205.c205_product_family = 4050
											   THEN 1
										   ELSE 0
									   END
									  ) core_imp_count
									, (CASE
										   WHEN t205.c205_core_part_fl = 'Y' AND t205.c205_product_family = 4051
											   THEN 1
										   ELSE 0
									   END
									  ) core_ins_count
									, (CASE
										   WHEN t205.c205_core_part_fl IS NULL AND t205.c205_product_family = 4050
											   THEN 1
										   ELSE 0
									   END
									  ) non_core_imp_count
									, (CASE
										   WHEN t205.c205_core_part_fl IS NULL AND t205.c205_product_family = 4051
											   THEN 1
										   ELSE 0
									   END
									  ) non_core_ins_count
								 FROM t402_work_order t402, t205_part_number t205
								WHERE t205.c205_part_number_id = t402.c205_part_number_id
								  AND t402.C1900_COMPANY_ID = v_compid_frm_cntx
								  AND t402.c402_void_fl IS NULL
								  AND t205.c202_project_id = NVL (p_project_id, t205.c202_project_id)
								  AND t205.c205_product_family IN (4050, 4051)
							 GROUP BY t402.c301_vendor_id
									, t402.c205_part_number_id
									, t205.c205_product_family
									, t205.c205_core_part_fl)
				   GROUP BY c301_vendor_id) work_order
				, (SELECT	t409.c301_vendor_id
						  , SUM ((CASE
									  WHEN t409.c409_created_date BETWEEN ADD_MONTHS (TRUNC (CURRENT_DATE), -p_month / 2)
																	  AND TRUNC (CURRENT_DATE)
										  THEN 1
									  ELSE 0
								  END
								 )
								) past_six_months
						  , SUM
								((CASE
									  WHEN t409.c409_created_date BETWEEN ADD_MONTHS (TRUNC (CURRENT_DATE), -p_month)
																	  AND ADD_MONTHS (TRUNC (CURRENT_DATE), -p_month / 2) - 1
										  THEN 1
									  ELSE 0
								  END
								 )
								) six_months_to_one_year
					   FROM t402_work_order t402, t409_ncmr t409, t205_part_number t205
					  WHERE t402.c402_work_order_id = t409.c402_work_order_id
					    AND t402.C1900_COMPANY_ID = v_compid_frm_cntx
					   	AND t205.c205_part_number_id = t402.c205_part_number_id
						AND t409.c409_created_date BETWEEN ADD_MONTHS (TRUNC (CURRENT_DATE), -p_month) AND TRUNC (CURRENT_DATE)
						AND t402.c402_void_fl IS NULL
						AND t409.c409_void_fl IS NULL
						AND t409.c409_rts_fl = 1
						AND t409.C901_type <> 51061
						AND t205.c202_project_id = NVL (p_project_id, t205.c202_project_id)
						AND t205.c205_product_family IN (4050, 4051)
				   GROUP BY t409.c301_vendor_id) ncmr
				, (SELECT	t401.c301_vendor_id
						  , SUM ((CASE
									  WHEN (   t402.c402_completed_date IS NULL
											OR TRUNC (t401.c401_date_required) < t402.c402_completed_date
										   )
									  AND t401.c401_date_required BETWEEN ADD_MONTHS (TRUNC (CURRENT_DATE), -p_month / 2)
																	  AND TRUNC (CURRENT_DATE)
										  THEN 1
									  ELSE 0
								  END
								 )
								) dhr_six_months
						  , SUM ((CASE
									  WHEN (   t402.c402_completed_date IS NULL
											OR TRUNC (t401.c401_date_required) < t402.c402_completed_date
										   )
									  AND t401.c401_date_required BETWEEN ADD_MONTHS (TRUNC (CURRENT_DATE), -p_month)
																	  AND ADD_MONTHS (TRUNC (CURRENT_DATE), -p_month / 2) - 1
										  THEN 1
									  ELSE 0
								  END
								 )
								) dhr_six_twelve
					   FROM t402_work_order t402, t401_purchase_order t401, t205_part_number t205
					  WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
					    AND t401.C1900_COMPANY_ID = v_compid_frm_cntx
						AND t205.c205_part_number_id = t402.c205_part_number_id
						AND t402.c402_void_fl IS NULL
						AND t401.c401_void_fl IS NULL
						AND t401.c401_date_required BETWEEN ADD_MONTHS (TRUNC (CURRENT_DATE), -p_month) AND TRUNC (CURRENT_DATE)
						AND t401.c401_date_required < TRUNC (CURRENT_DATE)
						AND t205.c202_project_id = NVL (p_project_id, t205.c202_project_id)
						AND t205.c205_product_family IN (4050, 4051)
				   GROUP BY t401.c301_vendor_id) late_deliveries
				, (SELECT	c301_vendor_id, SUM (ncmr_repeat_count) cnt
					   FROM (SELECT   t409.c301_vendor_id, (COUNT (t409.c205_part_number_id) - 1) ncmr_repeat_count
								 FROM t409_ncmr t409
									, t205_part_number t205
									, t402_work_order t402
									, t401_purchase_order t401
								WHERE t409.c409_rts_fl = 1
								  AND t401.C1900_COMPANY_ID = v_compid_frm_cntx
								  AND t409.C901_type <> 51061
								  AND t402.c402_work_order_id = t409.c402_work_order_id
								  AND t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
								  AND t409.c901_reason_type IS NOT NULL
								  AND t409.c409_void_fl IS NULL
								  AND t205.c205_part_number_id = t409.c205_part_number_id
								  AND t205.c202_project_id = NVL (p_project_id, t205.c202_project_id)
								  AND t401.c401_date_required BETWEEN ADD_MONTHS (TRUNC (CURRENT_DATE), -p_month / 2)
																  AND TRUNC (CURRENT_DATE)
								  AND t205.c205_product_family IN (4050, 4051)
							 GROUP BY t409.c301_vendor_id
									, t409.c205_part_number_id
									, t409.c901_reason_type
									, t402.c401_purchase_ord_id
							   HAVING COUNT (1) > 1
							 ORDER BY t409.c301_vendor_id, t409.c205_part_number_id)
				   GROUP BY c301_vendor_id) ncmr_repeat_three_six
				, (SELECT	c301_vendor_id, SUM (ncmr_repeat_count) cnt
					   FROM (SELECT   t409.c301_vendor_id, (COUNT (t409.c205_part_number_id) - 1) ncmr_repeat_count
								 FROM t409_ncmr t409
									, t205_part_number t205
									, t402_work_order t402
									, t401_purchase_order t401
								WHERE t409.c409_rts_fl = 1
								  AND t401.C1900_COMPANY_ID = v_compid_frm_cntx
								  AND t409.C901_type <> 51061
								  AND t402.c402_work_order_id = t409.c402_work_order_id
								  AND t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
								  AND t409.c901_reason_type IS NOT NULL
								  AND t409.c409_void_fl IS NULL
								  AND t402.c402_void_fl IS NULL
						          AND t401.c401_void_fl IS NULL
								  AND t205.c205_part_number_id = t409.c205_part_number_id
								  AND t205.c202_project_id = NVL (p_project_id, t205.c202_project_id)
								  AND t205.c205_product_family IN (4050, 4051)
								  AND t401.c401_date_required BETWEEN ADD_MONTHS (TRUNC (CURRENT_DATE), -p_month)
																  AND ADD_MONTHS (TRUNC (CURRENT_DATE), -p_month / 2) - 1
							 GROUP BY t409.c301_vendor_id
									, t409.c205_part_number_id
									, t409.c901_reason_type
									, t402.c401_purchase_ord_id
							   HAVING COUNT (1) > 1
							 ORDER BY t409.c301_vendor_id, t409.c205_part_number_id)
				   GROUP BY c301_vendor_id) ncmr_repeat_six_twelve
				, (SELECT po_cnt.c301_vendor_id, po_cnt.cnt, po_qty.qty
					 FROM (SELECT	c301_vendor_id, COUNT (c401_purchase_ord_id) cnt
							   FROM (SELECT   t401.c301_vendor_id, t401.c401_purchase_ord_id
										 FROM t401_purchase_order t401, t402_work_order t402, t205_part_number t205
										WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
										  AND t401.C1900_COMPANY_ID = v_compid_frm_cntx
										  AND t205.c205_part_number_id = t402.c205_part_number_id
										  AND t401.c401_void_fl IS NULL
										  AND t402.c402_void_fl IS NULL
										  AND t402.c402_status_fl < 3
										  AND t205.c202_project_id = NVL (p_project_id, t205.c202_project_id)
										  AND t205.c205_product_family IN (4050, 4051)
										  AND t401.c401_type = NVL (p_po_type, t401.c401_type)
										  AND t401.c401_date_required BETWEEN ADD_MONTHS (TRUNC (CURRENT_DATE), -p_month)
																		  AND TRUNC (CURRENT_DATE)
									 GROUP BY t401.c301_vendor_id, t401.c401_purchase_ord_id)
						   GROUP BY c301_vendor_id) po_cnt
						, (SELECT	t401.c301_vendor_id, SUM (t402.c402_qty_ordered) qty
							   FROM t401_purchase_order t401, t402_work_order t402, t205_part_number t205
							  WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
							    AND t401.C1900_COMPANY_ID = v_compid_frm_cntx
								AND t205.c205_part_number_id = t402.c205_part_number_id
								AND t401.c401_void_fl IS NULL
								AND t402.c402_void_fl IS NULL
								AND t402.c402_status_fl < 3
								AND t205.c202_project_id = NVL (p_project_id, t205.c202_project_id)
								AND t205.c205_product_family IN (4050, 4051)
								AND t401.c401_type = NVL (p_po_type, t401.c401_type)
								AND t401.c401_date_required BETWEEN ADD_MONTHS (TRUNC (CURRENT_DATE), -p_month)
																AND TRUNC (CURRENT_DATE)
						   GROUP BY t401.c301_vendor_id) po_qty
					WHERE po_qty.c301_vendor_id = po_cnt.c301_vendor_id) open_wo_three_six
				, (SELECT po_cnt.c301_vendor_id, po_cnt.cnt, po_qty.qty
					 FROM (SELECT	c301_vendor_id, COUNT (c401_purchase_ord_id) cnt
							   FROM (SELECT   t401.c301_vendor_id, t401.c401_purchase_ord_id
										 FROM t401_purchase_order t401, t402_work_order t402, t205_part_number t205
										WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
										  AND t401.C1900_COMPANY_ID = v_compid_frm_cntx
										  AND t205.c205_part_number_id = t402.c205_part_number_id
										  AND t401.c401_void_fl IS NULL
										  AND t402.c402_void_fl IS NULL
										  AND t402.c402_status_fl < 3
										  AND t205.c202_project_id = NVL (p_project_id, t205.c202_project_id)
										  AND t205.c205_product_family IN (4050, 4051)
										  AND t401.c401_type = NVL (p_po_type, t401.c401_type)
										  AND t401.c401_date_required BETWEEN ADD_MONTHS (TRUNC (CURRENT_DATE), -p_month)
																		  AND	ADD_MONTHS (TRUNC (CURRENT_DATE)
																						  , -p_month / 2
																						   )
																			  - 1
									 GROUP BY t401.c301_vendor_id, t401.c401_purchase_ord_id)
						   GROUP BY c301_vendor_id) po_cnt
						, (SELECT	t401.c301_vendor_id, SUM (t402.c402_qty_ordered) qty
							   FROM t401_purchase_order t401, t402_work_order t402, t205_part_number t205
							  WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
							    AND t401.C1900_COMPANY_ID = v_compid_frm_cntx
								AND t205.c205_part_number_id = t402.c205_part_number_id
								AND t401.c401_void_fl IS NULL
								AND t402.c402_void_fl IS NULL
								AND t402.c402_status_fl < 3
								AND t205.c202_project_id = NVL (p_project_id, t205.c202_project_id)
								AND t205.c205_product_family IN (4050, 4051)
								AND t401.c401_type = NVL (p_po_type, t401.c401_type)
								AND t401.c401_date_required BETWEEN ADD_MONTHS (TRUNC (CURRENT_DATE), -p_month)
																AND ADD_MONTHS (TRUNC (CURRENT_DATE), -p_month / 2) - 1
						   GROUP BY t401.c301_vendor_id) po_qty
					WHERE po_qty.c301_vendor_id = po_cnt.c301_vendor_id) open_wo_six_twelve
				, (SELECT vendor.c301_vendor_id, ((vendor.per / total.per) * 100) per
					 FROM (SELECT	t401.c301_vendor_id, SUM (t402.c402_cost_price) per
							   FROM t401_purchase_order t401
								  , t402_work_order t402
								  , t205_part_number t205
								  , t301_vendor t301
							  WHERE t401.c401_void_fl IS NULL
							    AND t402.c402_void_fl IS NULL
								AND t301.c301_vendor_id = t401.c301_vendor_id
								AND t301.C1900_COMPANY_ID = v_compid_frm_cntx
							    AND t301.C1900_COMPANY_ID =t401.C1900_COMPANY_ID 
								AND t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
								AND t205.c205_part_number_id = t402.c205_part_number_id
								AND t205.c202_project_id = NVL (p_project_id, t205.c202_project_id)
								AND t205.c205_product_family IN (4050, 4051)
								AND t401.c401_date_required BETWEEN ADD_MONTHS (TRUNC (CURRENT_DATE), -p_month)
																AND TRUNC (CURRENT_DATE)
						   GROUP BY t401.c301_vendor_id) vendor
						, (SELECT SUM (t402.c402_cost_price) per
							 FROM t401_purchase_order t401
								, t402_work_order t402
								, t205_part_number t205
								, t301_vendor t301
							WHERE t401.c401_void_fl IS NULL
							  AND t402.c402_void_fl IS NULL
							  AND t301.c301_vendor_id = t401.c301_vendor_id
							  AND t301.C1900_COMPANY_ID = v_compid_frm_cntx
							  AND t301.C1900_COMPANY_ID =t401.C1900_COMPANY_ID 
							  AND t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
							  AND t205.c205_part_number_id = t402.c205_part_number_id
							  AND t205.c202_project_id = NVL (p_project_id, t205.c202_project_id)
							  AND t205.c205_product_family IN (4050, 4051)
							  AND t401.c401_date_required BETWEEN ADD_MONTHS (TRUNC (CURRENT_DATE), -p_month) AND TRUNC
																												(CURRENT_DATE)) total) overall_business_three_six
				, (SELECT vendor.c301_vendor_id, ((vendor.per / total.per) * 100) per
					 FROM (SELECT	t401.c301_vendor_id, SUM (t402.c402_cost_price) per
							   FROM t401_purchase_order t401
								  , t402_work_order t402
								  , t205_part_number t205
								  , t301_vendor t301
							  WHERE t401.c401_void_fl IS NULL
							    AND t402.c402_void_fl IS NULL
								AND t301.c301_vendor_id = t401.c301_vendor_id
								AND t301.C1900_COMPANY_ID = v_compid_frm_cntx
							    AND t301.C1900_COMPANY_ID =t401.C1900_COMPANY_ID 
								AND t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
								AND t205.c205_part_number_id = t402.c205_part_number_id
								AND t205.c202_project_id = NVL (p_project_id, t205.c202_project_id)
								AND t205.c205_product_family IN (4050, 4051)
								AND t401.c401_date_required BETWEEN ADD_MONTHS (TRUNC (CURRENT_DATE), -p_month)
																AND ADD_MONTHS (TRUNC (CURRENT_DATE), -p_month / 2) - 1
						   GROUP BY t401.c301_vendor_id) vendor
						, (SELECT SUM (t402.c402_cost_price) per
							 FROM t401_purchase_order t401
								, t402_work_order t402
								, t205_part_number t205
								, t301_vendor t301
							WHERE t401.c401_void_fl IS NULL
							  AND t402.c402_void_fl IS NULL
							  AND t301.c301_vendor_id = t401.c301_vendor_id
							  AND t301.C1900_COMPANY_ID = v_compid_frm_cntx
							  AND t301.C1900_COMPANY_ID =t401.C1900_COMPANY_ID 
							  AND t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
							  AND t205.c205_part_number_id = t402.c205_part_number_id
							  AND t205.c202_project_id = NVL (p_project_id, t205.c202_project_id)
							  AND t205.c205_product_family IN (4050, 4051)
							  AND t401.c401_date_required BETWEEN ADD_MONTHS (TRUNC (CURRENT_DATE), -p_month)
															  AND ADD_MONTHS (TRUNC (CURRENT_DATE), -p_month / 2) - 1) total) overall_business_six_twelve
			WHERE t301.c301_vendor_id = work_order.c301_vendor_id(+)
			  AND t301.c301_vendor_id = ncmr.c301_vendor_id(+)
			  AND t301.c301_vendor_id = late_deliveries.c301_vendor_id(+)
			  AND t301.c301_vendor_id = ncmr_repeat_three_six.c301_vendor_id(+)
			  AND t301.c301_vendor_id = ncmr_repeat_six_twelve.c301_vendor_id(+)
			  AND t301.c301_vendor_id = open_wo_three_six.c301_vendor_id(+)
			  AND t301.c301_vendor_id = open_wo_six_twelve.c301_vendor_id(+)
			  AND t301.c301_vendor_id = overall_business_three_six.c301_vendor_id(+)
			  AND t301.c301_vendor_id = overall_business_six_twelve.c301_vendor_id(+)
			  AND t301.c301_vendor_id = NVL (p_vendor_id, t301.c301_vendor_id)
			  AND t301.C1900_COMPANY_ID = v_compid_frm_cntx
			  AND NVL (t301.c301_bill_state, -999) = NVL (p_location, NVL (t301.c301_bill_state, -999))
		 ORDER BY NAME;
END gm_op_fch_vendor_info_rpt;
/
