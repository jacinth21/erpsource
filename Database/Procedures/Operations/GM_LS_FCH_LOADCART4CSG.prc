CREATE OR REPLACE PROCEDURE Gm_Ls_Fch_Loadcart4csg
(
 p_consignid  IN T504_CONSIGNMENT.c504_consignment_id%TYPE,
 p_part_num  IN T205_PART_NUMBER.c205_part_number_id%TYPE,
 p_recordset  OUT Types.cursor_type)
AS

v_company_id  VARCHAR2(20);
/*****************************************************************************************************
  * This Procedure is to Fetch the Load Part Details for the Consignment
  * Author : Joe P Kumar
  * Date   : 10/10/06
******************************************************************************************************/

BEGIN
--
	SELECT  get_compid_frm_cntx()
    INTO    v_company_id
    FROM    DUAL;
    
 OPEN p_recordset FOR
 SELECT t205.C205_PART_NUMBER_ID ID
   ,t2052.C2052_EQUITY_PRICE PRICE
   ,t205.C205_PART_NUM_DESC PDESC
   ,Get_Qty_In_Stock(t205.C205_PART_NUMBER_ID) STOCK
   ,Get_Count_Partid_Consign(p_consignid,t205.C205_PART_NUMBER_ID) QTY_IN_SET
   ,get_partnumber_qty(t205.C205_PART_NUMBER_ID,90813) QSTOCK
   ,get_partnumber_qty(t205.C205_PART_NUMBER_ID,90814) BSTOCK
   ,Get_Partnumber_Fa_Wip_Qty(t205.C205_PART_NUMBER_ID) WIPQTY
  FROM T205_PART_NUMBER t205 ,T2052_PART_PRICE_MAPPING t2052
  WHERE t205.C205_PART_NUMBER_ID IN (p_part_num)
  AND t205.c205_part_number_id = t2052.c205_part_number_id(+)
  AND   T2052.C1900_COMPANY_ID(+) = v_company_id
  AND t2052.c2052_void_fl(+) is NULL;
--
END Gm_Ls_Fch_Loadcart4csg;
/

