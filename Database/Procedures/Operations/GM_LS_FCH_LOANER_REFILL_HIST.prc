/* Formatted on 2009/06/29 18:22 (Formatter Plus v4.8.0) */
-- @"c:\database\Procedures\Operations\gm_ls_fch_loaner_refill_hist.prc"

CREATE OR REPLACE PROCEDURE gm_ls_fch_loaner_refill_hist (
	p_ref_id	  IN	   t412_inhouse_transactions.c412_ref_id%TYPE
  , p_recordset   OUT	   TYPES.cursor_type
)
AS
/***************************************************************************************************
  * This Procedure is to to Fetch Loaner History Transaction
  * Author : Dhinakaran James
  * Date   : 10/18/06
****************************************************************************************************/
	v_str		   VARCHAR2 (2000);
	v_datefmt VARCHAR2(10);
BEGIN
		SELECT NVL(get_compdtfmt_frm_cntx(),get_rule_value('DATEFMT','DATEFORMAT')) INTO v_datefmt FROM DUAL;
--
	OPEN p_recordset
	 FOR
		 SELECT   t412.c412_inhouse_trans_id transid, get_code_name (t412.c412_inhouse_purpose) purpose
				, TO_CHAR (t412.c412_created_date, v_datefmt) cdate, get_user_name (t412.c412_created_by) cuser
				, t413.c205_part_number_id pnum, t413.c413_control_number cnum, t413.c413_item_qty qty
				, t412.c412_ref_id refid, get_user_name (t412.c412_verified_by) vuser
				, TO_CHAR (t412.c412_verified_date, v_datefmt) vdate
				, get_cs_loaner_hist_bill_nm_add (t504a.c504_consignment_id, t504a.c504a_loaner_transaction_id) cname
			 FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413, t504a_loaner_transaction t504a
			WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
			  AND t412.c412_ref_id = p_ref_id
			  AND NVL (t413.c413_status_fl, -9999) NOT IN ('S' ,'R','W','C')
			  AND t412.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id(+)
			  AND t504a.c504a_void_fl IS NULL
			  AND t412.c412_void_fl IS NULL
			  AND t413.c413_void_fl IS NULL
		 ORDER BY t412.c412_created_date DESC;
--
END gm_ls_fch_loaner_refill_hist;
/
