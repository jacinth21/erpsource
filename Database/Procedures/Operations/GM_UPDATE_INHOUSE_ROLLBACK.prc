/* Formatted on 2012/06/14 15:30 (Formatter Plus v4.8.0) */
--@"C:\Database\Procedures\Operations\GM_UPDATE_INHOUSE_ROLLBACK.prc";

CREATE OR REPLACE PROCEDURE gm_update_inhouse_rollback (
   p_dhrid           IN       t408_dhr.c408_dhr_id%TYPE,
   p_userid          IN       t408_dhr.c408_last_updated_by%TYPE
)
AS
   v_inhouse_cnt     NUMBER;
   v_rs_id           VARCHAR2(50);
   v_inhouse_id      VARCHAR2(50);
   CURSOR cur_408a
	IS
		 SELECT t408a.c205_part_number_id pnumber
              , t408a.c408a_conrol_number cnumber 
              , t4082.c4081_dhr_bulk_id RSid
          FROM t408a_dhr_control_number t408a, t4082_bulk_dhr_mapping t4082 
         WHERE t408a.c408_dhr_id = t4082.c408_dhr_id
           AND t408a.c408_dhr_id = p_dhrid
           AND t408a.c408a_void_fl IS NULL;    
BEGIN
  
	   FOR rad_val IN cur_408a
		LOOP		
      		-- Getting the Inhouse Trasnaction ID for the Resulted Lot and Part Number
			SELECT c412_inhouse_trans_id INTO v_inhouse_id
	          FROM t413_inhouse_trans_items 
	         WHERE c413_control_number = rad_val.cnumber 
	           AND c205_part_number_id = rad_val.pnumber
	           AND c413_void_fl IS NULL ;		                       
	       -- Updating the Void flag for Part and Lot Details in T413 Table.                
	        UPDATE t413_inhouse_trans_items t413
	           SET t413.c413_void_fl = 'Y'
	             , c413_last_updated_by = p_userid
	             , c413_last_updated_date = SYSDATE
	         WHERE t413.c412_inhouse_trans_id IN (v_inhouse_id)
	                 AND t413.c205_part_number_id = rad_val.pnumber
	                 AND t413.c413_control_number = rad_val.cnumber;
	   	END LOOP; 
	   	
	   	-- Getting the Count for voiding the inhouse transaction in t412 table
	     -- IF count is ZERO, then we can void the record from T412, orherwise, 
	     -- should not as we have some other parts mapped to this transction and which are not voided in T413.
			 SELECT count(1) INTO v_inhouse_cnt
			   FROM t413_inhouse_trans_items 
			  WHERE c412_inhouse_trans_id IN (
		               SELECT c412_inhouse_trans_id 
		                 FROM t412_inhouse_transactions 
		                WHERE c412_ref_id IN (
		            			SELECT c4081_dhr_bulk_id 
		            			  FROM t4082_bulk_dhr_mapping 
		            			 WHERE c408_dhr_id = p_dhrid)) 
		          AND c413_void_fl IS NULL 
		          AND c412_inhouse_trans_id = v_inhouse_id; 
          
          IF v_inhouse_cnt = 0 THEN
          
	          UPDATE t412_inhouse_transactions t412
		         SET t412.c412_void_fl = 'Y',
		         t412.c412_last_updated_by = p_userid,
		         t412.c412_last_updated_date = SYSDATE 
		      WHERE t412.c412_inhouse_trans_id IN (v_inhouse_id);
          END IF;	         
--
END gm_update_inhouse_rollback;
/
