CREATE OR REPLACE PROCEDURE GM_LS_FCH_LOADSETMASTER
(
	p_txn_id  IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
	p_recordset  OUT Types.cursor_type
)
AS
/*****************************************************************************************************
  * This Procedure is to Fetch Set Details for the InHouse Transaction given the Transaction ID
  * Author : Joe P Kumar
  * Date   : 10/10/06
******************************************************************************************************/
v_str VARCHAR2(2000);
v_plant_id     		 T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
BEGIN
--
SELECT  get_plantid_frm_cntx() INTO v_plant_id FROM DUAL;
	OPEN p_recordset FOR
	SELECT 
			t413.c205_part_number_id PNUM
			,Get_Partnum_Desc(t413.c205_part_number_id) PDESC
			,t413.c413_item_qty QTYREF
			,decode(C412_STATUS_FL,2, NVL(t413.c413_control_number,'TBE'), NVL(t413.c413_control_number,'')) CNUM -- display TBE during controlling otherwise display the CN#
			,t413.c413_item_price PRICE
			,GET_QTY_IN_STOCK(t413.c205_part_number_id) QTYSHELF
			,GET_COUNT_PARTID_CONSIGN(t412.c412_ref_id ,t413.c205_part_number_id) QTY_IN_SET
			,GET_OP_QTY_INHOUSE_PARTID(t412.c412_inhouse_trans_id ,t413.c205_part_number_id) QTY_IN_TRANS
			,t412.c412_type TXNTYPE
			,t412.c412_ref_id CONSIGNID, t413.c413_error_dtls errordtls
	FROM 
			t412_inhouse_transactions t412, t413_inhouse_trans_items t413
	WHERE
			t412.c412_inhouse_trans_id = p_txn_id
	        AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id 
			AND t412.c5040_plant_id    = v_plant_id
			AND c413_void_fl IS NULL 
			AND C412_VOID_FL IS NULL
		    ORDER BY t413.c205_part_number_id;
--
END GM_LS_FCH_LOADSETMASTER;
/

