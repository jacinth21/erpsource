/* Formatted on 2011/04/26 15:39 (Formatter Plus v4.8.0) */
/*
@"C:\database\Procedures\Operations\gm_create_dhr.prc"
*/

CREATE OR REPLACE PROCEDURE gm_create_dhr (
   p_woid       IN       t408_dhr.c402_work_order_id%TYPE,
   p_vendid     IN       t408_dhr.c301_vendor_id%TYPE,
   p_partnum    IN       t408_dhr.c205_part_number_id%TYPE,
   p_control    IN       t408_dhr.c408_control_number%TYPE,
   p_qty        IN       t408_dhr.c408_qty_received%TYPE,
   p_manfdate   IN       VARCHAR2,
   p_packslip   IN       t408_dhr.c408_packing_slip%TYPE,
   p_recdate    IN       VARCHAR2,
   p_comments   IN       t408_dhr.c408_comments%TYPE,
   p_type       IN       VARCHAR2,
   p_userid     IN       t408_dhr.c408_created_by%TYPE,
   p_pdt_id     OUT      VARCHAR2,
   p_message    OUT      VARCHAR2,
   p_donor_id   IN       t408_dhr.c2540_donor_id%TYPE DEFAULT NULL,
   P_exp_date   IN       VARCHAR2 DEFAULT NULL
)
AS
--
/********************************************************************************************
 * Description :This procedure is used for creating a new DHR from the PO receive screen
 * Change Details:
 *     James Jun3,2006    Changed Formatting according to Standards
 *     James     Jun3,2006    Changes for UOM
 *     James     Jul25     Changes for Manufacturing
 *********************************************************************************************/
 --
   v_pdt_id          VARCHAR2 (20);
   v_string          VARCHAR2 (20);
   v_id_string       VARCHAR2 (20);
   v_subcompfl       CHAR (1);
   v_statusfl        NUMBER;
   v_qtyinshelf      NUMBER;
   v_control         t408_dhr.c408_control_number%TYPE;
   v_uomqty          NUMBER;
   v_po_type         NUMBER;
   v_po_id           VARCHAR2 (20);
   v_type            VARCHAR2 (20);
   v_dhr_type        NUMBER;
   v_amt             NUMBER (15, 2);
   v_partyid         VARCHAR2 (20);
   v_costing_id      NUMBER;
   v_post_id         VARCHAR2 (20);
   v_p_type          NUMBER;
   v_split_percent   NUMBER;
   v_split_cost      NUMBER;
   v_rc_fl           CHAR (1);
   v_total_qty       NUMBER                              := 0;
   v_pnum            VARCHAR2 (20);
   v_cnum            VARCHAR2 (20);
   v_qty             NUMBER;
   v_wo_uom_qty      NUMBER;
   v_rule_value		VARCHAR2(10);
   v_msg 			VARCHAR2(1);
   v_msg_ncmr		VARCHAR2(1);
   k   VARCHAR2 (10);
   v_company_id  t1900_company.c1900_company_id%TYPE;
   v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
   v_context_cmp_id t1900_company.c1900_company_id%TYPE;
   v_context_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
   v_dateformat varchar2(100);
   v_source_company_id t1900_company.c1900_company_id%TYPE;
--
   CURSOR pop_val
   IS
      SELECT t205.c205_part_number_id subid, t205.c205_rev_num rev
        FROM t205a_part_mapping t205a, t205_part_number t205
       WHERE t205a.c205_from_part_number_id = p_partnum
         AND t205a.c205_to_part_number_id = t205.c205_part_number_id
         AND t205a.c205a_void_fl is null;   ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
--
BEGIN
--	
	SELECT get_plantid_frm_cntx()
    INTO v_context_plant_id
    FROM DUAL;
    v_type := p_type;
    
--
   SELECT s408_dhr.NEXTVAL
     INTO v_pdt_id
     FROM DUAL;

--
   IF LENGTH (v_pdt_id) = 1
   THEN
      v_id_string := '0' || v_pdt_id;
   ELSE
      v_id_string := v_pdt_id;
   END IF;

--
   SELECT t401.c401_type, t401.c401_purchase_ord_id, t401.c1900_company_id
     INTO v_po_type, v_po_id, v_company_id
     FROM t402_work_order t402, t401_purchase_order t401
    WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
      AND t402.c402_work_order_id = p_woid;

--
   -- Manufacturing DHR's need to be moved to Pending QC Acceptance
   IF v_po_type = 3103
   THEN
      v_type := 'MANF';
   END IF;
--
   IF v_type = 'DUMMY'
   THEN
      SELECT 'GM-DMY-' || v_id_string
        INTO v_string
        FROM DUAL;

      v_statusfl := 4;
      v_qtyinshelf := p_qty;
   ELSIF v_type = 'MANF'
   THEN
      SELECT 'GM-SHR-' || v_id_string
        INTO v_string
        FROM DUAL;

      v_statusfl := 0;
      v_qtyinshelf := NULL;
   ELSIF v_type = 'RC'
   THEN
      SELECT 'GM-RC-' || v_id_string
        INTO v_string
        FROM DUAL;

      v_statusfl := 4;                                          ---4, verified
      v_qtyinshelf := p_qty;
      v_dhr_type := 80191;
   ELSE
      SELECT 'GM-DHR-' || v_id_string
        INTO v_string
        FROM DUAL;
      v_statusfl := 1;
      v_qtyinshelf := NULL;
   END IF;
   
   v_control := p_control;

   IF LENGTH (p_control) = 8 AND SUBSTR (p_control, 8, 1) = '#'
   THEN
      v_control := SUBSTR (p_control, 1, 7);
   END IF;

   --
   p_pdt_id := v_string;
--
-- For calucalating UOM Qty
BEGIN
   SELECT t405.c405_uom_qty
     INTO v_uomqty
     FROM t402_work_order t402, t405_vendor_pricing_details t405
    WHERE t402.c402_work_order_id = p_woid
      AND t402.c405_pricing_id = t405.c405_pricing_id
      AND t405.c405_void_fl IS NULL;
EXCEPTION WHEN NO_DATA_FOUND THEN
	v_uomqty := 1;
	-- Handle No Data Found for parts without Vendor Price.
END;
IF v_uomqty IS NULL THEN
	v_uomqty := 1;
END IF;
--

select get_compdtfmt_frm_cntx() into v_dateformat from dual;
   INSERT INTO t408_dhr
               (c408_dhr_id, c301_vendor_id, c402_work_order_id,
                c205_part_number_id, c408_control_number,
                c408_qty_received,
                c408_manf_date, c408_packing_slip,
                c408_received_date, c408_comments,
                c408_status_fl, c408_created_by,
                c408_created_date, c408_qty_on_shelf,
                c408_shipped_qty, c401_purchase_ord_id,
                c901_type,c2540_donor_id,c2550_expiry_date,
                c1900_company_id, C5040_PLANT_ID)
        VALUES (v_string, p_vendid, p_woid,
                p_partnum, v_control,
                p_qty * v_uomqty        -- Multiplying UOM with PO Qty Shipped
                                ,
                TO_DATE (p_manfdate, v_dateformat), p_packslip,
                TO_DATE (p_recdate, v_dateformat), p_comments,
                v_statusfl             -- Status Flag for Received / Initiated
                          , p_userid,
                CURRENT_DATE, v_qtyinshelf,
                p_qty                   -- Actualy PO Qty shipped from Vendor.
                     , v_po_id,
                v_dhr_type,p_donor_id, TO_DATE (P_exp_date, v_dateformat),
                v_company_id, v_context_plant_id);

-- The next section will dump values into the SUB_DHR table if the WO had a flag for Sub_Components.
-- Will enable user to input separate control Numbers for the sub-components
   SELECT c402_sub_component_fl
     INTO v_subcompfl
     FROM t402_work_order
    WHERE c402_work_order_id = p_woid AND c402_void_fl IS NULL;

--
   IF v_subcompfl = '1'
   THEN
      FOR rad_val IN pop_val
      LOOP
         INSERT INTO t411_sub_dhr
                     (c411_sub_dhr_id, c408_dhr_id, c206_sub_asmbly_id
                     )
              VALUES (s411_sub_dhr.NEXTVAL, v_string, rad_val.subid
                     );
      END LOOP;

      UPDATE t408_dhr
         SET c408_status_fl = '0'
       WHERE c408_dhr_id = v_string;
   END IF;

   /*
    * The following posting is
    * Moved from Verify DHR to Create DHR
    */

   -- Fetching Values for Postings
   SELECT t401.c301_vendor_id,
          NVL (DECODE (t401.c401_type, 3104, t205.c205_cost, t402.c402_cost_price),
               t402.c402_cost_price
              ),
          t401.c401_type, t402.c402_split_percentage, t402.c402_split_cost,
          NVL(t402.c402_rc_fl,'N'), NVL(t401.c1900_source_company_id, t401.c1900_company_id)
     INTO v_partyid,
          v_amt,
          v_p_type, v_split_percent, v_split_cost,
          v_rc_fl, v_source_company_id
     FROM t402_work_order t402, t401_purchase_order t401, t205_part_number t205
    WHERE t402.c402_work_order_id = p_woid
      AND t402.c401_purchase_ord_id = t401.c401_purchase_ord_id
      AND t402.c205_part_number_id = t205.c205_part_number_id;

   -- For Ledger Posting
   v_post_id := get_rule_value (v_p_type, 'PO-TYPE');

   IF (v_split_cost IS NOT NULL AND p_userid != 30301)
   THEN
      IF (v_rc_fl = 'Y')
      THEN
         -- Material Vendor Site - Credit
         gm_save_ledger_posting (48149,
                                 CURRENT_DATE,
                                 '100.000',
                                 v_partyid,
                                 p_pdt_id,
                                 p_qty * v_uomqty,
                                 NVL (v_split_cost, 0),
                                 p_userid,
                                 NULL,
                                 v_source_company_id,
                                 NVL (v_split_cost, 0)
                                );
         -- Unvouchered A/P - Credit
         gm_save_ledger_posting (48148,
                                 CURRENT_DATE,
                                 p_partnum,
                                 v_partyid,
                                 p_pdt_id,
                                 p_qty * v_uomqty,
                                 v_amt - NVL (v_split_cost, 0),
                                 p_userid,
                                 NULL,
                                 v_source_company_id,
                                 v_amt - NVL (v_split_cost, 0)
                                );
         --Inventory not Inspected - Debit
         gm_save_ledger_posting (48150,
                                 CURRENT_DATE,
                                 p_partnum,
                                 v_partyid,
                                 p_pdt_id,
                                 p_qty * v_uomqty,
                                 v_amt,
                                 p_userid,
                                 NULL,
                                 v_source_company_id,
                                 v_amt
                                );
      ELSE
         gm_pkg_op_dhr.gm_op_sav_dhr_posting (p_pdt_id, NULL, 48129,
                                              p_userid);
         -- Purchase  to Unvouchered A/P
         gm_save_ledger_posting (48151,
                                 CURRENT_DATE,
                                 '100.000',
                                 v_partyid,
                                 p_pdt_id,
                                 p_qty * v_uomqty,
                                 NVL (v_split_cost, 0),
                                 p_userid,
                                 NULL,
                                 v_source_company_id,
                                 NVL (v_split_cost, 0)
                                );         
      END IF;
      --handle if received more than ordered
         gm_pkg_op_dhr.gm_op_sav_extra_rc (p_pdt_id,
                                           p_partnum,
                                           p_woid,
                                           p_qty * v_uomqty,
                                           p_userid,
                                           v_partyid,
                                           v_split_cost
                                          );
   ELSIF ( p_userid != 30301 ) THEN
   			-- If  PO type is rework - 3101, need to pass new txn type	48201--Rework to the Inventory Not Inspected
		IF v_po_type = 3101
		THEN
			gm_pkg_op_dhr.gm_op_sav_dhr_posting (p_pdt_id, NULL, 48201, p_userid);   
            gm_pkg_op_dhr.gm_op_sav_dhr_posting (p_pdt_id, NULL, 48209, p_userid);
 
		ELSE
			gm_pkg_op_dhr.gm_op_sav_dhr_posting (p_pdt_id, NULL, v_post_id, p_userid);
		END IF;       
   END IF;
   -- THIS SECTION IS TO CHECK FOR THE SKIP DHR STEPS 
   	SELECT get_rule_value('SKIP_INSP','SKIP_DHR_STEPS') 
   		INTO v_rule_value 
   		FROM DUAL;
   	IF v_po_type = 3100
   	THEN
   		IF v_rule_value = 'Y' THEN
   			gm_update_dhr_inspect(v_string, p_qty * v_uomqty, 0, p_userid, null, null, '2', null, p_userid, '1',null, v_msg, v_msg_ncmr);
   		END IF;
   	END IF;
   	--- END OF SKIP SHR STEPS 
--
END gm_create_dhr;
/
