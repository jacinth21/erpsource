CREATE OR REPLACE PROCEDURE gm_update_inventory_sales (
   p_id           IN       VARCHAR2,
   p_type         IN       VARCHAR2,
   p_action       IN       VARCHAR2,
   p_updated_by   IN       VARCHAR2
)
/**************************************************************
 * Code Lookup values
 * ACTION
 * -----------
 * 4301  - Plus
 * 4302  - Minus
 * 50300 - Sales Consignment
 * 50301 - Loaner Consignment
 * POSTING DETAILS
 * ---------------
 * 4811  - 'FG to COGS'
 * 48065 - 'Loaner to COGS'
 **************************************************************/
AS
--
   v_stock      NUMBER;
   v_party_id   VARCHAR2 (20);
   v_post_id    VARCHAR2 (20);
   v_acc_id     VARCHAR2 (20);
   v_item_type	NUMBER;
   v_item_qty	NUMBER;
   v_post_fl	NUMBER;
   --
   v_order_type   t501_order.c901_order_type%TYPE;
   v_dist_id 	   t701_distributor.c701_distributor_id%TYPE;
   v_rep_id		t703_sales_rep.c703_sales_rep_id%TYPE;
   v_account_id T704_account.c704_account_id%TYPE;
   v_location_id  t704_account.c5052_location_id%TYPE;
   v_dist_type NUMBER;
   v_cn_post_id VARCHAR2(220);
   v_plus_fs_fl VARCHAR2(10);
   v_minus_fs_fl VARCHAR2(10);
   v_warehouse_trans_id VARCHAR2(40);
   v_account_warehouse_type NUMBER;
   v_order_sale_post_id NUMBER;
   v_add_post_id t906_rules.c906_rule_value%TYPE;
   v_order_fl VARCHAR2(1);
--
	V_ICTACCID T704_account.c704_account_id%TYPE;
   CURSOR cur_sales
   IS
      SELECT   t502.c205_part_number_id ID, SUM (t502.c502_item_qty) qty,
               t502.c502_item_price price, t502.c901_type itype, NVL(t501.c901_order_type, '-9999') ordertype,
               GET_RULE_VALUE (NVL(t501.c901_order_type, '-9999'), 'ORDER_SALES_POSTING') order_sales_post_id
          FROM t502_item_order t502, t501_order t501
         WHERE t502.c501_order_id = p_id
           AND t502.c501_order_id = t501.c501_order_id AND t502.C502_VOID_FL IS NULL 
      GROUP BY t502.c205_part_number_id, t502.c502_item_price, t502.c901_type, t501.c901_order_type ;
--
BEGIN
--
   -- To get Party Id for Ledger Posting -- Added Feb 9
   SELECT get_acct_party_id (p_id, p_type)
   INTO v_party_id
   FROM DUAL;

   -- To get the order information
   BEGIN
	 SELECT NVL(t501.c901_order_type, 2521), t501.c703_sales_rep_id, c501_distributor_id
	  , t501.c704_account_id, t704.c5052_location_id, get_distributor_type (c501_distributor_id)
	  , DECODE(t704.c5052_location_id, NULL, c501_distributor_id, t704.c704_account_id)
	  -- Account consignment - warehouse to be Account warehouse
	  , DECODE(t704.c5052_location_id, NULL, NULL, 2532)
	   INTO v_order_type, v_rep_id, v_dist_id
	  , v_account_id, v_location_id, v_dist_type
      , v_warehouse_trans_id
      , v_account_warehouse_type
	   FROM t501_order t501, t704_account t704
	  WHERE t501.c501_order_id   = p_id
	    AND t704.c704_account_id = t501.c704_account_id
	    AND t704.c704_void_fl   IS NULL
	    AND t501.C501_VOID_FL   IS NULL;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_order_type := NULL;
			v_rep_id := NULL;
			v_dist_id := NULL;
			v_account_id := NULL;
			v_location_id := NULL;
			v_dist_type := NULL;
			v_warehouse_trans_id := NULL;
	END;    
	 --   
     FOR rad_val IN cur_sales
     LOOP
         --
	 v_item_type := rad_val.itype;
	 v_item_qty := rad_val.qty;
	 v_cn_post_id := NULL;
	 -- For parts that came out of Sales Consignment Kits
	 -- Install - 26240234
	 IF v_item_type = 50300 AND v_item_qty > 0 AND (rad_val.ordertype != 2532 AND rad_val.ordertype != 26240234) THEN
		gm_cm_sav_partqty (rad_val.ID,
                               (rad_val.qty * -1),
                               p_id,
                               p_updated_by,
                               90800, -- FG  Qty
                               4302,-- Minus
                               4310 -- Sales
                               );
		 --
		v_order_sale_post_id :=  rad_val.order_sales_post_id;
		
		-- Direct and intercompany (order posting are different)
		IF v_order_sale_post_id IS NOT NULL
         THEN
			v_post_id := v_order_sale_post_id; --Sales
			v_post_fl := 1;
			-- new posting type (5000900) should be used in OUS distributor order (102080) and return posting.
			IF rad_val.ordertype= '102080' THEN
    		 SELECT get_rule_value (C101_PARTY_INTRA_MAP_ID, 'ICTACCOUNTMAP') into V_ICTACCID FROM T701_DISTRIBUTOR WHERE C701_DISTRIBUTOR_ID = v_dist_id;
    		 -- PC-4299-ous-distributor-orders
			 v_post_id := NVL(get_rule_value (V_ICTACCID, 'ORDEROUSDIST'), get_rule_value ('OUS_ORDER_SALES', 'DEFAULT_POSTING_TYPE'));
            END IF;
				
			
			ELSE
			
			-- Bill and Ship order posting changed for this PMT-23894
			-- Current Posting is CN to COGS, FG to CN
			-- Swap order posting (PMT-23894)  - FG to CN, CN to COGS.
			
			v_cn_post_id := 4811; --Sales (FG to sales consignment)
			v_post_id := 48085; --Sales Consignments to COGS
			v_post_fl := 1;
			-- to call the new posting for sales consignment to cogs
			gm_save_ledger_posting (v_cn_post_id, 
					 CURRENT_DATE,
					 rad_val.ID,
					 v_party_id,
					 p_id,
					 rad_val.qty,
					 rad_val.price,
					 p_updated_by
					);
		 END IF;			
		--		
		gm_pkg_op_lot_track.gm_lot_track_main(rad_val.ID,
                               (rad_val.qty * -1),
                               p_id,
                               p_updated_by,
                               90800, -- FG  Qty
                               4302,-- Minus
                               4310 -- Sales
                               );
      
        --Direct order should not increase field sales inventory
        --Enabling lot tracking for returns from the orders (bill and ship, hold and intercompany) - PMT-29682                 
        SELECT NVL(get_rule_value(v_order_type,'LOTINCORDERTYPE'),'N') INTO v_order_fl FROM DUAL;
        IF v_order_fl = 'Y' THEN
        	gm_pkg_op_lot_track.gm_lot_track_main(rad_val.ID,
                               rad_val.qty,
                               p_id,
                               p_updated_by,
                               4000339, -- FS  Qty
                               4301,-- PLUS
                               4310 -- Sales
                               );
        END IF;
	 ELSIF v_item_type = 50300 AND v_item_qty < 0 THEN
		-- No Posting 
		v_post_fl := 0;
	 -- For parts that came out of Loaner Kits
	 ELSIF v_item_type = 50301 AND v_item_qty > 0 THEN
		v_post_id := 48065;
		v_post_fl := 1;
	 ELSIF v_item_type = 50301 AND v_item_qty < 0 THEN
		-- No Posting 
		v_post_fl := 0;
    ELSIF (rad_val.ordertype = 2532 OR rad_val.ordertype = 26240234) AND v_item_qty > 0 THEN
    	-- posting id to set Sales Consignments to COGS
		v_post_id := 48085;
		
		v_post_fl := 1;
	 END IF;
	 --
	 IF v_post_fl > 0 THEN
	 gm_save_ledger_posting (v_post_id,
				 CURRENT_DATE,
				 rad_val.ID,
				 v_party_id,
				 p_id,
				 rad_val.qty,
				 rad_val.price,
				 p_updated_by
				);
	 END IF;
      --
     END LOOP;
	-- FS warehouse Qty Plus/Minus
	v_minus_fs_fl := NVL(get_rule_value (v_order_type, 'MINUS_FS_WAREHOUSE'), 'N');
    v_plus_fs_fl := NVL(get_rule_value (v_order_type, 'PLUS_FS_WAREHOUSE'), 'N');
	-- reduce the FS warehouse Qty 
	IF v_minus_fs_fl = 'Y' THEN   
		-- 4302 Minus
		-- 102900 - Consignment
		gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv (p_id, 'T501', v_warehouse_trans_id, 102900, 4302, 4000337, p_updated_by, v_account_warehouse_type) ;
	END IF;
	-- Increase FS warehouse Qty
	IF v_plus_fs_fl ='Y' THEN
	    -- 4000932 Bill and Ship Orders
		gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv (p_id, 'T501', v_warehouse_trans_id, 102900, 4301, 4000932, p_updated_by, v_account_warehouse_type);
	END IF;
--     
END gm_update_inventory_sales;
/