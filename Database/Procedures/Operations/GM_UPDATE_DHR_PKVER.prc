/* Formatted on 2011/04/26 12:45 (Formatter Plus v4.8.0) */
-- @ "C:\Database\Procedures\Operations\GM_UPDATE_DHR_PKVER.prc";

CREATE OR REPLACE PROCEDURE gm_update_dhr_pkver (
   p_dhrid         IN   t408_dhr.c408_dhr_id%TYPE,
   p_partnum       IN   t408_dhr.c205_part_number_id%TYPE,
   p_woid          IN   t408_dhr.c402_work_order_id%TYPE,
   p_inputstr      IN   VARCHAR2,
   p_labelskipfl   IN   CHAR,
   p_sterilefl     IN   CHAR,
   p_transby       IN   NUMBER,
   p_action        IN   CHAR,
   p_updstatusfl   IN   t408_dhr.c408_status_fl%TYPE,
   p_udi_chk_fl    IN   t408_dhr.c408_udi_verified%TYPE
)
/*************************************************************************************************
 * Description      :This procedure is called to get UPDATE PACKAGING AND VERIFICATION INFORMATION
 * Parameters      :
 *
 * Change history
 * Joe P Kumar on May 10 - Added code to check if the value is updated by other user
 * Dhinakaran James  Jun3,2006 - For UOM changes
 *
 **************************************************************************************************/
AS
   v_cnt               NUMBER;
   v_wo_cnt            NUMBER;
   v_dhr_wo_cnt        NUMBER;
   v_amt               NUMBER (15, 2);
   v_partyid           VARCHAR2 (20);
   v_costing_id        NUMBER;
   v_post_id           VARCHAR2 (20);
   v_type              NUMBER;
   v_inv_fl            CHAR (1);
   v_chkstatusfl       NUMBER (1);
   v_chkdhrvoidfl      CHAR (1);
   v_chkwordervoidfl   CHAR (1);
   v_wo_uom_str        VARCHAR2 (20);
   v_wo_uom_qty        NUMBER;
   v_payment_fl        CHAR (1);
   v_dhrqty            NUMBER;
   v_currid            NUMBER;
   v_split_percent     NUMBER;
   v_split_cost        NUMBER;
   v_rc_fl             CHAR (1);
   v_total_qty         NUMBER         := 0;
   v_pnum              VARCHAR2 (20);
   v_cnum              t408_dhr.c408_control_number%TYPE;
   v_qty               NUMBER;
   v_dhfg_txn_id       VARCHAR2 (20);
   v_rule_value		   VARCHAR2 (10);
   v_po_type		   VARCHAR2(10);
   v_prodclass         VARCHAR2(10);  
   v_status_fl   	   VARCHAR2(10);
   v_date_fmt		   t906_rules.c906_rule_value%TYPE;
   v_count 			   NUMBER;
   v_txn_id			   VARCHAR2 (20);
   v_dhrrec_qty		   NUMBER;
   v_company_id        t1900_company.c1900_company_id%TYPE;
   CURSOR v_inh_txns
   IS
      SELECT c412_inhouse_trans_id trans_id, c412_type trans_type
        FROM t412_inhouse_transactions t412
       WHERE t412.c412_ref_id = p_dhrid
         AND t412.c412_void_fl IS NULL
         AND t412.c412_type IN ('100073', '100074', '100077');
         
	CURSOR dhrDtlsCur
	IS
		SELECT NVL(t408a.c408a_conrol_number, t408.c408_control_number) ctrlnum, to_char(t408.c2550_expiry_date,v_date_fmt) expdate
			, t408a.c408a_custom_size custsize, t408.c301_vendor_id vendorid, to_char(t408.c408_manf_date,v_date_fmt) manfdate
        	, DECODE(t408a.c408a_conrol_number, NULL,NULL,t408.c2540_donor_id) donorid, t408.c408_packing_slip packslip
	       FROM t408_dhr t408, T408A_DHR_CONTROL_NUMBER T408A
    	 WHERE T408.C408_DHR_ID = T408A.C408_DHR_ID(+)
      		AND t408.c408_dhr_id = p_dhrid
        	AND t408a.c408a_void_fl(+) IS NULL ;
        	
    CURSOR dhrCtrlNumCur
	IS
		SELECT t408a.c408a_conrol_number ctrlnum
	       FROM T408A_DHR_CONTROL_NUMBER T408A
    	 WHERE  T408A.c408_dhr_id = p_dhrid
        	AND t408a.c408a_void_fl IS NULL
        	AND NVL(c901_status,'1400') = '1400';--Approved
BEGIN
--
 	-- Get the date company date format
 	SELECT  NVL(get_compdtfmt_frm_cntx(),get_rule_value('DATEFMT','DATEFORMAT'))
       INTO v_date_fmt 
       FROM DUAL;
       
   -- Check for DHR Void Flag. If the Flag is turned ON, then throw an Application Error
   SELECT t408.c408_void_fl, t402.c402_void_fl, t408.c408_control_number, t408.c1900_company_id
     INTO v_chkdhrvoidfl, v_chkwordervoidfl, v_cnum, v_company_id
     FROM t408_dhr t408, t402_work_order t402
    WHERE t408.c408_dhr_id = p_dhrid
      AND t408.c402_work_order_id = t402.c402_work_order_id;

   IF (v_chkdhrvoidfl IS NOT NULL)
   THEN
      --
      raise_application_error (-20004, '');
   --
   END IF;

   IF (v_chkwordervoidfl IS NOT NULL)
   THEN
      --
      GM_RAISE_APPLICATION_ERROR('-20999','318',p_woid);
   --
   END IF;

   --
   -- End of Check for DHR Void Flag and  check if the value is updated by other user
   SELECT     c408_update_inv_fl, TO_NUMBER (c408_status_fl)
         INTO v_inv_fl, v_chkstatusfl
         FROM t408_dhr
        WHERE c408_dhr_id = p_dhrid
   FOR UPDATE;

   --
   IF v_chkstatusfl > TO_NUMBER (p_updstatusfl)
   THEN
      --
      IF p_action = 'P'
      THEN
         raise_application_error (-20002, '');
      ELSIF p_action = 'V'
      THEN
         raise_application_error (-20003, '');
      END IF;
   --
   END IF;

   --
   IF p_action = 'P'
   THEN
       	  
      SELECT t408.c205_part_number_id, c408_control_number
        -- need to check whether the price is important for this inhouse transaction
      INTO   v_pnum, v_cnum
        FROM t408_dhr t408
       WHERE t408.c408_dhr_id = p_dhrid;
       
      --To check whether control numbers are generated      
   	  IF v_cnum IS NULL
   	  THEN   	  
   	  	gm_pkg_op_dhr_rpt.gm_validate_generated_lotNum(p_dhrid);
   	  
   	  END IF;

      gm_pkg_op_dhr.gm_op_sav_ihtxns (p_dhrid,
                                      p_inputstr,
                                      v_pnum,
                                      v_cnum,
                                      p_transby,
                                      v_total_qty
                                     );
                                     
                                     
		-- For Posting into QC Samples Account
	      SELECT   NVL (SUM (t408.c408_qty_received), 0)
	             - NVL (SUM (t408.c408_qty_rejected), 0)
	             + NVL (SUM (t409.c409_closeout_qty), 0)
	             , NVL (SUM (t408.c408_qty_received), 0)
	        INTO v_dhrqty, v_dhrrec_qty
	        FROM t408_dhr t408, t409_ncmr t409
	       WHERE t408.c408_dhr_id = t409.c408_dhr_id(+)
	         AND t408.c408_dhr_id = p_dhrid
	         AND t408.c408_void_fl IS NULL
	         AND t409.c409_void_fl(+) IS NULL;
       
	   	  SELECT COUNT (1)
	   	  	INTO v_count
	       FROM T408A_DHR_CONTROL_NUMBER T408A
    	 WHERE  T408A.c408_dhr_id = p_dhrid
        	AND t408a.c408a_void_fl IS NULL ;
	  -- if we reject any quantity from received qty, then no need to assign lot number automatically
	  -- remove the condition, since user did which lot to reject, still need to assign lot number automatically
	  -- if only one type inhouse transaction
      IF  v_count > 0
      THEN
      
		  SELECT COUNT (1)
		  	INTO v_count
		   FROM t412_inhouse_transactions
		  WHERE c412_ref_id   = p_dhrid
		    AND c412_void_fl IS NULL;
		  --If there's only one type inhouse transaction, then automatically assign Lot number to it.
		  IF v_count = 1
	  		 THEN
	  		 	SELECT c412_inhouse_trans_id
	  		 		INTO v_txn_id
				   FROM t412_inhouse_transactions
				  WHERE c412_ref_id   = p_dhrid
				    AND c412_void_fl IS NULL;  		 		
	  		 
	  		 	UPDATE t413_inhouse_trans_items
			       SET C413_VOID_FL = 'Y',
			       C413_LAST_UPDATED_BY = p_transby,
			       C413_LAST_UPDATED_DATE = SYSDATE
		 		   WHERE c412_inhouse_trans_id = v_txn_id;
		 		   --AND c413_control_number IS NULL;			 		   
			 
			 	 FOR v_ctrlNum IN dhrCtrlNumCur
	     		 LOOP	   
				 	INSERT INTO t413_inhouse_trans_items
		                		(c413_trans_id, c412_inhouse_trans_id, c205_part_number_id,
		                        c413_item_qty, c413_control_number, c413_item_price, c413_created_date
		                        )
						SELECT s413_inhouse_item.NEXTVAL, v_txn_id, c205_part_number_id,
		                		1, v_ctrlNum.ctrlnum, c413_item_price, SYSDATE
		                FROM t413_inhouse_trans_items 
		                WHERE c412_inhouse_trans_id = v_txn_id  
		                AND ROWNUM = 1; 
		          END LOOP;
		  END IF;
      END IF;

      IF NVL (v_dhrqty, 0) != NVL (v_total_qty, 0)
      THEN
         GM_RAISE_APPLICATION_ERROR('-20999','319','');
            
      END IF;

      UPDATE t408_dhr
         SET c408_status_fl = '3',
             c408_packaged_by = p_transby,
             c408_label_skip_fl = p_labelskipfl,
             c408_packaged_ts = SYSDATE,
             c408_qty_packaged = v_total_qty,
             c408_last_updated_by = p_transby,
             c408_last_updated_date = SYSDATE,
             c408_udi_verified = p_udi_chk_fl
       WHERE c408_dhr_id = p_dhrid;
	   
       SELECT get_potype_from_woid(p_woid) 
       		INTO v_po_type 
       		FROM DUAL;

IF v_po_type='3100' OR v_po_type = '102365' --STOCK TRANSFER PO
       		THEN
       			SELECT get_rule_value_by_company('SKIP_VER','SKIP_DHR_STEPS',v_company_id)   
       				INTO v_rule_value 
       				FROM DUAL;
       			IF v_rule_value = 'Y'
       			THEN
       				gm_update_dhr_pkver (p_dhrid,p_partnum,p_woid,v_total_qty,p_labelskipfl,p_sterilefl,p_transby,'V','3',p_udi_chk_fl);
       			END IF;
       		END IF;
   ELSIF p_action = 'V' AND v_inv_fl IS NULL
   THEN
   	  --To check whether the lot number is assigned to the inhouse transaction
   	  IF v_cnum IS NULL
   	  THEN   	  
   	  	gm_pkg_op_dhr_rpt.gm_validate_assign_lotNum(p_dhrid);
   	  
   	  END IF;
   
      v_qty := p_inputstr;

      FOR v_index IN dhrDtlsCur
      LOOP
      -- Need to save expiry date details in t2550_part_control_number table at the time of verification
      		GM_OP_SAV_LOT_EXPIRY_DATE (p_partnum, v_index.ctrlnum,  v_index.custsize,  v_index.vendorid, v_index.manfdate, v_index.donorid, v_index.expdate, p_transby, v_index.packslip, p_dhrid);
      END LOOP;
      --
      SELECT DECODE (get_potype_from_woid (p_woid),
                     3104, 'Y',
                     3105, 'Y',
                     NULL
                    )
        INTO v_payment_fl
        FROM DUAL;

      UPDATE t408_dhr
         SET c408_verified_by = p_transby,
             c408_verified_ts = SYSDATE,
             c408_qty_on_shelf = v_qty,
             c408_status_fl = '4',
             c408_sterile_status_fl = p_sterilefl
                                                 -- Value 1 means that this DHR's details has to used for next Sterlization WO
      ,
             c408_last_updated_by = p_transby,
             c408_last_updated_date = SYSDATE,
             c408_payment_fl =
                DECODE (c408_payment_fl,
                        NULL, v_payment_fl,
                        c408_payment_fl
                       ),                                      --v_payment_fl
             c408_udi_verified = p_udi_chk_fl
       WHERE c408_dhr_id = p_dhrid;

      gm_pkg_op_inv_scan.gm_check_inv_order_status (p_dhrid,
                                                    'Completed',
                                                    p_transby
                                                   );

      --
      -- Get the Qty Ordered from Work Order table
      SELECT c402_qty_ordered
        INTO v_wo_cnt
        FROM t402_work_order
       WHERE c402_work_order_id = p_woid AND c402_void_fl IS NULL;

      --
      -- Get the Total Qty received for that particular WO
      SELECT SUM (c408_qty_on_shelf)
        INTO v_dhr_wo_cnt
        FROM t408_dhr
       WHERE c402_work_order_id = p_woid
         AND c408_void_fl IS NULL
         AND c901_type IS NULL;  --c901_type, null is regular DHR, 80191 is RC

      --
      -- To get UOM Qty
      SELECT get_wo_uom_type_qty (p_woid)
        INTO v_wo_uom_str
        FROM DUAL;

      --
      v_wo_uom_qty :=
         TO_NUMBER (NVL (SUBSTR (v_wo_uom_str, INSTR (v_wo_uom_str, '/') + 1),
                         1
                        )
                   );
      -- Function return Type/Qty. So stripping Type/ from that
      v_wo_cnt := v_wo_cnt * v_wo_uom_qty;     -- Multiply WO Qty with UOM Qty

      --
      -- If the Qty Ordered is equal to the sum of Qty received for the WO,
      -- then update the status flag to 3 (Closed)
      IF v_wo_cnt = v_dhr_wo_cnt OR v_wo_cnt < v_dhr_wo_cnt
      THEN
         --
         UPDATE t402_work_order
            SET c402_status_fl = 3,
                c402_completed_date = TRUNC (SYSDATE),
                c402_last_updated_by = p_transby,
                c402_last_updated_date = SYSDATE
          WHERE c402_work_order_id = p_woid;
      --
      END IF;

      -- Inserting records into table t907A_currency_TRANS
      SELECT get_currency_id (get_vendor_currency (v_partyid), 1)
        INTO v_currid
        FROM DUAL;

      IF (v_currid <> 0)
      THEN
         INSERT INTO t907a_currency_trans
                     (c907a_curr_trans_id, c907_curr_seq_id, c907a_ref_id,
                      c901_ref_type, c907a_created_date, c907a_created_by
                     )
              VALUES (s907a_curr_trans.NEXTVAL, v_currid, p_dhrid,
                      91380, SYSDATE, p_transby
                     );
      END IF;

      --
      -- Updating INV flag to avoid duplicate posting
      UPDATE t408_dhr
         SET c408_update_inv_fl = '1'
       WHERE c408_dhr_id = p_dhrid;

      gm_pkg_ac_payment.gm_ac_update_invc_status (NULL, p_dhrid);

      select T408.C408_Control_Number,get_potype_from_woid(T408.C402_Work_Order_Id),T205.c205_product_class
        into v_cnum,v_po_type,v_prodclass 
	    from T408_Dhr T408, T205_Part_Number T205
	  where T408.C408_Dhr_Id  = p_dhrid
         and T205.c205_part_number_id = p_partnum
         and T408.C205_Part_Number_Id = T205.C205_Part_Number_Id
         and T408.C408_Void_Fl Is Null
         and t205.c205_active_fl = 'Y'; -- Get the control num,po type,part num type value  
	    
	    IF v_prodclass = '4031' AND v_po_type='3101' AND v_cnum='NOC#' -- Part:Non-Sterile,Po type: Rework,Control Number: NOC# 
	    THEN 
	    v_status_fl := '2'; --when status is 2, it is pending control
	    ELSE
	    v_status_fl := '3'; --when status is 3, it is pending shipping
	    END IF;
    
      UPDATE t412_inhouse_transactions
         SET c412_status_fl = v_status_fl,
             c412_last_updated_date = SYSDATE,
             c412_last_updated_by = p_transby
       WHERE c412_ref_id = p_dhrid;

      FOR v_index IN v_inh_txns
      LOOP
         /*QC Samples an R&D Transactions need
          to be verified as it is not required to
          be verified by the users manually  and create postings for those transactions*/
         UPDATE t412_inhouse_transactions
            SET c412_update_inv_fl = '1',
                c412_verified_by = p_transby,
                c412_verified_date = SYSDATE,
                c412_verify_fl = '1',
                c412_status_fl = '4',
                c412_last_updated_date = SYSDATE,
                c412_last_updated_by = p_transby
          WHERE c412_inhouse_trans_id = v_index.trans_id;

         gm_pkg_op_inventory_qty.gm_sav_inventory_main (v_index.trans_id,
                                                        v_index.trans_type,
                                                        p_transby
                                                       );
      END LOOP;

      BEGIN
         SELECT c412_inhouse_trans_id
           INTO v_dhfg_txn_id
           FROM t412_inhouse_transactions t412
          WHERE t412.c412_ref_id = p_dhrid
            AND t412.c412_void_fl IS NULL
            AND t412.c412_type IN ('100067', '100071');                             -- DHFG, MWFG
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_dhfg_txn_id := NULL;
      END;

      IF v_dhfg_txn_id IS NOT NULL
      THEN
         gm_pkg_allocation.gm_ins_invpick_assign_detail (93350,
                                                         v_dhfg_txn_id,
                                                         p_transby
                                                        );
      END IF;
      
      gm_pkg_op_lot_track.gm_lot_track_main(NULL,
                               NULL,
                               p_dhrid,
                               p_transby,
                               90801, -- DHR  
                               4301,-- PLUS
                               50982 -- DHR
                               );
   --
   END IF;
--
END gm_update_dhr_pkver;
/
