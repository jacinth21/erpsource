CREATE OR REPLACE PROCEDURE GM_PLACE_PO
(
	p_VendId	IN	t401_purchase_order.c301_vendor_id%TYPE,
	p_POTotal	IN	t401_purchase_order.c401_po_total_amount%TYPE,
	p_UserId	IN	t401_purchase_order.c401_created_by%TYPE,
	p_Type		IN	t401_purchase_order.c401_type%TYPE,
	p_source_company_id IN t401_purchase_order.c1900_source_company_id%TYPE,
	p_po_id		OUT	VARCHAR2,
	p_message	OUT	VARCHAR2
)
AS
--
/********************************************************************************************
 * Description  :This procedure is used for creating a new PO during the PO Initiation process
 * Change Details:
 *		 James	Jul25,2006	Changed Formatting according to Standards
		 James	Jul25,2006	Changes for Manufacturing PO
 *********************************************************************************************/
 --
v_po_id		NUMBER;
v_string	VARCHAR2(20);
v_id_string	VARCHAR2(20);
v_po_string	VARCHAR2(200);
v_country_code	VARCHAR2(10);
v_po_prefix VARCHAR2(10);
v_company_id  t1900_company.c1900_company_id%TYPE;
v_plant_id	  t5040_plant_master.C5040_PLANT_ID%TYPE;
v_ven_fl      t301_vendor.c301_vendor_portal_fl%TYPE;
v_autopublishFL	t301_vendor.C301_AUTO_PUBLISH_FL%TYPE;
v_autopublish_rule t906_rules.C906_RULE_VALUE%TYPE;
v_msg VARCHAR2(20);
--
BEGIN
--
  BEGIN
	SELECT gm_pkg_vendor_po_report.get_vendor_portal_access_fl(p_VendId)
	  INTO v_ven_fl
	  FROM DUAL;
	 EXCEPTION WHEN NO_DATA_FOUND THEN
		  v_ven_fl := 'N';
  END;
	 
	SELECT	s401_purchase.nextval, get_plantid_frm_cntx()
	INTO	v_po_id, v_plant_id
	FROM	dual;
	--
	IF LENGTH(v_po_id) = 1 THEN
		v_id_string := '0' || v_po_id;
	ELSE
		v_id_string := v_po_id;
	END IF;

	--get country code from rule
	SELECT GET_RULE_VALUE('OUS','COUNTRY') 
		INTO  v_country_code
	FROM DUAL;
	
	--Getting the PO-Prefix from rules
 	SELECT NVL(GET_RULE_VALUE(v_country_code,'POPREFIX'),'GM') 
 		INTO v_po_prefix 
 	FROM DUAL;

	 -- When Raised Po for Chennai or Delhi get rule value CH/DL. other wise the prefix value will be contrycode. 
 	SELECT NVL(GET_RULE_VALUE(p_VendId, UPPER(v_country_code)), v_po_prefix) 
 		INTO v_po_prefix 
 	FROM DUAL;	
	
	-- To get the vendor company id
	SELECT c1900_company_id
         INTO v_company_id
    FROM t301_vendor
    WHERE C301_VENDOR_ID = p_VendId;
	-- Manufacturing PO's need a diff format - GM-MF-XXXXXXX
	IF p_Type = 3103 THEN
		SELECT	'GM-MF-' || v_id_string 
		INTO	v_string
		FROM	dual;
	ELSE
		SELECT	v_po_prefix||'-PO-' || v_id_string
		INTO	v_string
		FROM	dual;
	END IF;
	--
	p_po_id := v_string ;
	--
	INSERT INTO t401_purchase_order(
		c401_purchase_ord_id
		,c301_vendor_id
		,c401_purchase_ord_date
		,c401_date_required
		,c401_po_total_amount
		,c401_type
		,c401_created_by
		,c401_created_date
		,c1900_company_id
		,c5040_plant_id
		,c1900_source_company_id
		,c901_status
		) VALUES (
	        v_string
		,p_VendId
		,TRUNC(CURRENT_DATE)
		,TO_DATE(CURRENT_DATE + 49)
		--,p_DateReq
		,p_POTotal
		,p_Type
		,p_UserId
		,CURRENT_DATE
		,v_company_id
		,v_plant_id
		,p_source_company_id
		,decode(v_ven_fl,'Y',108366,NULL)        --108366:Pending Publish status
	);
	
	BEGIN
	SELECT c301_AUTO_PUBLISH_FL    
	INTO v_autopublishFL
	 FROM t301_vendor
	 WHERE C301_VENDOR_ID = p_VendId; 
	 EXCEPTION WHEN NO_DATA_FOUND THEN
		  v_autopublishFL := null;
	
    END;
	SELECT get_rule_value_by_company(p_Type, 'AUTO_PUBLISH_PO', v_company_id)
		INTO v_autopublish_rule
	FROM DUAL;
	
	SELECT NVL (get_rule_value_by_company(p_Type, 'AUTO_PUBLISH_PO', v_company_id) , 'N') 
	    INTO v_autopublish_rule
 	FROM DUAL;
		
	IF v_autopublishFL = 'Y' AND  v_autopublish_rule  = 'Y' AND v_ven_fl = 'Y' THEN
	
	GM_PKG_VENDOR_PO_REPORT.gm_sav_blk_publish_po (v_string, p_UserId, 'Y', v_msg);
	
	END IF;
	
--
END GM_PLACE_PO;
/
