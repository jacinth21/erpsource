/* Formatted on 2008/10/26 18:16 (Formatter Plus v4.8.0) */
-- @"c:\database\procedures\operations\gm_update_inventory_substores.prc"

CREATE OR REPLACE PROCEDURE gm_update_inventory_substores (
	p_id	   IN	VARCHAR2
  , p_type	   IN	VARCHAR2
  , p_userid   IN	t101_user.c101_user_id%TYPE
)
--
AS
--
	v_stock 	   NUMBER;
	v_newstock	   NUMBER;
	v_party_id	   VARCHAR2 (20);
	v_set_id	   VARCHAR2 (20);
	v_purpose	   VARCHAR2 (20);
	v_type		   VARCHAR2 (20);
	v_post_id	   VARCHAR2 (20);
	v_dist_id	   VARCHAR2 (20);
	v_acc_id	   VARCHAR2 (20);
	v_reason	   VARCHAR2 (20);

--
	CURSOR cur_consign
	IS
		SELECT	 c205_part_number_id ID, SUM (c505_item_qty) qty, c505_item_price price
			FROM t505_item_consignment
		   WHERE c504_consignment_id = p_id AND TRIM (c505_control_number) IS NOT NULL
		GROUP BY c205_part_number_id, c505_item_price;
--
BEGIN
	--
	IF p_type = 'SET-REPROCESS'
	THEN
		--
		FOR rad_val IN cur_consign
		LOOP
			--
		gm_cm_sav_partqty (rad_val.ID,
                               (rad_val.qty * -1),
                               p_id,
                               p_userid,
                               90812, -- returns_hold Qty
                               4302,  -- Minus
                               4311  -- Consignment
                               );
       
       gm_cm_sav_partqty (rad_val.ID,
                               rad_val.qty,
                               p_id,
                               p_userid,
                               90814, -- Bulk Qty
                               4301,  -- Plus
                               4311  -- Consignment
                               );
		--
		END LOOP;
	--
	ELSIF p_type = 'QUAR-REPROCESS' OR p_type = 'PACK-REPROCESS'
	THEN
		FOR rad_val IN cur_consign
		LOOP
			--
			gm_cm_sav_partqty (rad_val.ID,
                               (rad_val.qty * -1),
                               p_id,
                               p_userid,
                               90812, -- returns_hold Qty
                               4302,  -- Minus
                               4311  -- Consignment
                               );
		--
		END LOOP;
	END IF;
--
END gm_update_inventory_substores;
/
