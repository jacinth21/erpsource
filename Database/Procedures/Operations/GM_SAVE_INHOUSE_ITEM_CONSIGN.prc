/* Formatted on 2011/04/06 14:14 (Formatter Plus v4.8.0) */
--@"C:\database\Procedures\Operations\GM_SAVE_INHOUSE_ITEM_CONSIGN.prc";

CREATE OR REPLACE PROCEDURE gm_save_inhouse_item_consign (
	p_consgid		IN		 t412_inhouse_transactions.c412_inhouse_trans_id%TYPE
  , p_type			IN		 t412_inhouse_transactions.c412_type%TYPE
  , p_purpose		IN		 t412_inhouse_transactions.c412_inhouse_purpose%TYPE
  , p_shipto		IN		 t412_inhouse_transactions.c412_release_to%TYPE
  , p_shiptoid		IN		 t412_inhouse_transactions.c412_release_to_id%TYPE
  , p_fincomments	IN		 t412_inhouse_transactions.c412_comments%TYPE
  , p_userid		IN		 t412_inhouse_transactions.c412_last_updated_by%TYPE
  , p_str			IN		 VARCHAR2
  , p_message		OUT 	 VARCHAR2
)
/************************
  for "loaner extension", p_type is:
  50154 = Move from shelf to Loaner Exension w/Replenish
   or 50128 = Loaner Exension with Replenish

*******************************/
AS
--
	v_flag		   VARCHAR2 (20);
	v_flag1 	   VARCHAR2 (20);
	v_msg		   VARCHAR2 (1000);
	v_price 	   VARCHAR2 (20);
	v_status	   VARCHAR2 (2);
	v_inv_type	   NUMBER;
	v_allocate_fl  VARCHAR2 (1);
	v_company_id   t1900_company.c1900_company_id%TYPE;
	v_default_company   t1900_company.c1900_company_id%TYPE;
	v_plant_id     t5040_plant_master.c5040_plant_id%TYPE;
--
BEGIN
--
	IF p_purpose = '0'
	THEN
		v_flag		:= '';
	ELSE
		v_flag		:= p_purpose;
	END IF;

	--
	IF p_shiptoid = '0'
	THEN
		v_flag1 	:= '';
	ELSE
		v_flag1 	:= p_shiptoid;
	END IF;
	
	SELECT  get_compid_frm_cntx()
			  INTO  v_company_id
			  FROM DUAL;
	
	SELECT  get_plantid_frm_cntx()
			  INTO  v_plant_id
			  FROM DUAL;
			  
	SELECT get_plant_parent_comp_id(v_plant_id)
	          INTO v_default_company 
	          FROM DUAL;
	--
	IF p_type = '1006483' -- Request Items for In-House Inv
	THEN
		v_status	:= '0';
	ELSIF p_type = '103932' -- Part Redesignation
	THEN
		v_status	:= '1';
	ELSIF p_type = '400084' AND v_plant_id IN ('3001', '3013')
	THEN
		v_status	:= '2';
	ELSIF p_type = '40051'
	   OR p_type = '40052'
	   OR p_type = '40053'
	   OR p_type = '40054'
	   OR p_type = '50160'
	   OR p_type = '50154'
	   OR p_type = '100406'
	   OR get_rule_value ('CONTROL_REQUIRED', p_type) = 'Y'
	THEN
		v_status	:= '2';
	ELSE
		v_status	:= '3';
	END IF;


	
	IF v_default_company IS NOT NULL AND v_default_company !=  v_company_id THEN
	   v_company_id := v_default_company;
	   gm_pkg_cor_client_context.gm_sav_client_context(v_default_company,v_plant_id);
	END IF;
	
	INSERT INTO t412_inhouse_transactions
				(c412_inhouse_trans_id, c412_type, c412_inhouse_purpose, c412_release_to, c412_release_to_id
			   , c412_comments, c412_status_fl, c412_created_by, c412_created_date, c412_last_updated_by
			   , c412_last_updated_date , c1900_company_id , c5040_plant_id
				)
		 VALUES (p_consgid, p_type, v_flag, p_shipto, v_flag1
			   , p_fincomments, v_status, p_userid, CURRENT_DATE, p_userid
			   , CURRENT_DATE , v_company_id , v_plant_id
				);

	--
	gm_save_inhouse_item_build (p_consgid, p_str, p_userid, v_msg);

	-- Create Record in T5050 table for Device to process
	-- If the transaction is from "shelf ->" it has to be picked from device.
	BEGIN
		SELECT c901_code_id
		  INTO v_inv_type
		  FROM t901_code_lookup t901
		 WHERE t901.c901_code_grp IN ('INVPT', 'INVPW')
		   AND t901.c902_code_nm_alt = TO_CHAR (p_type)
		   AND t901.c901_active_fl = '1';

		v_allocate_fl := gm_pkg_op_inventory.chk_txn_details (p_consgid, v_inv_type);

		IF NVL (v_allocate_fl, 'N') = 'Y'
		THEN
			gm_pkg_allocation.gm_ins_invpick_assign_detail (TO_NUMBER (v_inv_type), p_consgid, p_userid);
		END IF;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN;
	END;
--
END gm_save_inhouse_item_consign;
/
