/* Formatted on 2010/06/07 09:58 (Formatter Plus v4.8.0) */

--@"c:\database\Procedures\Operations\gm_update_set_consign.prc";

CREATE OR REPLACE PROCEDURE gm_update_set_consign (
	p_consgid		 IN 	  t504_consignment.c504_consignment_id%TYPE
  , p_type			 IN 	  t504_consignment.c504_type%TYPE
  , p_purpose		 IN 	  t504_consignment.c504_inhouse_purpose%TYPE
  , p_billto		 IN 	  t504_consignment.c701_distributor_id%TYPE
  , p_shipto		 IN 	  t504_consignment.c504_ship_to%TYPE
  , p_shiptoid		 IN 	  t504_consignment.c504_ship_to_id%TYPE
  , p_fincomments	 IN 	  t504_consignment.c504_final_comments%TYPE
  , p_shipfl		 IN 	  t504_consignment.c504_ship_req_fl%TYPE
  , p_statusfl		 IN 	  t504_consignment.c504_status_fl%TYPE
  , p_userid		 IN 	  t504_consignment.c504_last_updated_by%TYPE
  , p_loc_accnt_id	 IN 	  t504_consignment.c704_location_account_id%TYPE
  , p_loc_rep_id	 IN 	  t504_consignment.c703_location_sales_rep_id%TYPE
  , p_ict_notes		 IN 	  t504_consignment.C504_COMMENTS%TYPE 
  , p_message		 OUT	  VARCHAR2
  , p_link_ln_req	 IN 	  t504_consignment.c526_product_request_detail_id%TYPE DEFAULT NULL
  , p_plan_ship_date	 IN	  T520_REQUEST.C520_PLANNED_SHIP_DATE%TYPE DEFAULT NULL
)
AS
	v_flag			VARCHAR2 (20);
	v_flag1			VARCHAR2 (20);
	v_ship_flag		VARCHAR2 (20);
	v_price			VARCHAR2 (20);
	v_post_id		VARCHAR2 (20);
	v_mrid			t504_consignment.c520_request_id%TYPE;
	v_billto		VARCHAR2 (20);
	v_shipto		NUMBER;
	v_consignstatus t504_consignment.c504_status_fl%TYPE;
	v_count			NUMBER;
	v_status		NUMBER := 0;
	v_req_status	t520_request.c520_status_fl%TYPE;
	v_shipid		t907_shipping_info.c907_shipping_id%TYPE;
	v_pdt_req_id	t526_product_request_detail.c525_product_request_id%TYPE;
	v_link_flag		VARCHAR2 (20);
	v_country 		VARCHAR2(10);
	v_dist_type 	NUMBER;
	v_division_id   t1910_division.c1910_division_id%TYPE;
	v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
	v_company_id  t1900_company.c1900_company_id%TYPE;
	v_req_company_id  t1900_company.c1900_company_id%TYPE;
	v_set_type 		  t207_set_master.c207_type%TYPE;
	v_ln_status      t504_consignment.c504_status_fl%TYPE;
	v_primary_dist_id t701_distributor.c701_distributor_id%TYPE;
--
	CURSOR pop_val
	IS
		SELECT c205_part_number_id ID
		  FROM t505_item_consignment
		 WHERE c504_consignment_id = p_consgid;

--
	CURSOR cur_consign
	IS
		SELECT	 c205_part_number_id ID, SUM (c505_item_qty) qty, c505_item_price price
			FROM t505_item_consignment
		   WHERE c504_consignment_id = p_consgid AND TRIM (c505_control_number) IS NOT NULL
		GROUP BY c205_part_number_id, c505_item_price;
--
BEGIN

-- checking Bill to ID
  IF p_billto = '0' THEN
	 v_billto	:= NULL;
  ELSE
	 v_billto	:= p_billto;
  END IF;
--

  BEGIN
  	SELECT t504.c1900_company_id, t504.c5040_plant_id,
  	       DECODE(get_rule_value(t504.c504_type,'DEMO_CN_TYPE'),'Y',t207.c207_type,NULL)
  	  INTO v_company_id,v_plant_id, v_set_type 
  	  FROM t504_consignment t504, t207_set_master t207
  	 WHERE c504_consignment_id = p_consgid
  	   AND t504.c207_set_id =  t207.c207_set_id(+);

  EXCEPTION WHEN NO_DATA_FOUND THEN
      v_company_id := NULL;
  END;
  
  IF v_company_id IS NULL THEN
    SELECT  get_compid_frm_cntx(),get_plantid_frm_cntx()
     INTO  v_company_id,v_plant_id
     FROM DUAL;
  END IF;
  
    gm_pkg_cor_client_context.gm_sav_client_context(v_company_id,v_plant_id);
  	
	SELECT	   c520_request_id, c504_status_fl
		  INTO v_mrid, v_consignstatus
		  FROM t504_consignment
		 WHERE c504_consignment_id = p_consgid
	FOR UPDATE;
	 
	--get value from rule table to check if it's US or not.
	 SELECT get_rule_value ('US', 'COUNTRY') 
	  	INTO v_country 
  	FROM DUAL;

	IF v_consignstatus >= 4
	THEN
		raise_application_error ('-20085', '');
	END IF;

-- checking InHouse Purpose
	IF p_purpose = '0'
	THEN
		v_flag		:= NULL;
	ELSE
		v_flag		:= p_purpose;
	END IF;

-- checking ShipToId
	IF p_shiptoid = '0'
	THEN
		v_flag1 	:= NULL;
	ELSE
		v_flag1 	:= p_shiptoid;
	END IF;

--

-- checking Ship to
	IF p_shipto = '0'
	THEN
		v_shipto	:= NULL;
	ELSE
		v_shipto	:= p_shipto;
	END IF;

--
	IF p_shipfl = '1'
	THEN
		IF v_consignstatus = '3'
		THEN
			v_ship_flag := '3';
		ELSIF NVL(v_country, '-999') = 'Y'		--if it's US, change to pending pick else change to pendign shipping .
		THEN
			v_ship_flag :=   '2.20';
		 	-- change to '2.20' pending Pick instead of '3' pendign shipping, if already '3', no change
		ELSE
			v_ship_flag := '3';
		END IF;
	ELSE
		v_ship_flag := '4';
	END IF;

--
--The next section was added for Allosource Loaner program to allow control # entering by CS. 10/18/2005 - James -
	IF p_statusfl IS NOT NULL
	THEN
		v_ship_flag := p_statusfl;
	END IF;
	
		 v_dist_type := GET_DISTRIBUTOR_TYPE (v_billto);

		 IF p_type = 102930 AND NVL(v_dist_type,'-999') <> 70103
		 THEN
			GM_RAISE_APPLICATION_ERROR('-20999','334',''); --Please select valid distributor for type ICT
		 END IF;
--
	gm_pkg_op_request_validate.gm_op_consigment_validate (v_mrid, v_billto);
	
	--getting sales rep's primary division id when CN location sales rep id is updating.
	--This division id updated in t504, t520 tables.
	BEGIN
		 SELECT c1910_division_id INTO v_division_id
		   FROM T703_SALES_REP
		  WHERE C703_SALES_REP_ID = p_loc_rep_id
		    AND c703_void_fl     IS NULL;		   
		EXCEPTION WHEN NO_DATA_FOUND
			THEN
				v_division_id := NULL;		
	END;
	
	
	
	UPDATE t504_consignment
	   SET c504_type = DECODE (p_type,102930, 4110, p_type)
		 , c504_inhouse_purpose = v_flag
		 , c504_ship_to = v_shipto
		 , c504_ship_to_id = v_flag1
		 , c504_final_comments = p_fincomments
		 , c504_status_fl = v_ship_flag
		 , c504_ship_req_fl = p_shipfl
		 , c504_last_updated_by = p_userid
		 , c504_last_updated_date = CURRENT_DATE
		 , c504_ship_date = DECODE(v_ship_flag, '4', TRUNC (CURRENT_DATE),NULL)
		 , c704_location_account_id = p_loc_accnt_id
		 , c703_location_sales_rep_id = p_loc_rep_id
		 , c1910_division_id = v_division_id
		 , C504_COMMENTS=p_ict_notes 
		 , c526_product_request_detail_id = p_link_ln_req
		 , c1900_company_id = NVL(v_company_id,c1900_company_id) 
	 WHERE c504_consignment_id = p_consgid AND c504_void_fl IS NULL;

	IF NVL(v_country, '-999') = 'Y'
	THEN
		-- if 4110 consignment or 4112 inhouse consignment,  and if cn status is not 3 pending shipping, request status should be '20' ready to consign, otherwiase should be 30 ready to ship.
		IF (p_type = 4110 OR p_type = 4112) AND v_consignstatus !='3' AND v_ship_flag != '3'
		THEN
			v_req_status := '20';
		ELSIF ( v_consignstatus ='3' OR v_ship_flag = '3')
		THEN
			v_req_status := '30';
		ELSE
			v_req_status := '40';
		END IF;
	ELSE
		SELECT DECODE (p_type, 4110, '30', 102930, '30', 4112, '30', '40') INTO v_req_status FROM DUAL;
	END IF;

	UPDATE t520_request
	   SET c520_status_fl = v_req_status
		 , c520_request_to = v_billto
		 , c520_last_updated_by = p_userid
		 , c520_last_updated_date = CURRENT_DATE
		 , c901_ship_to = v_shipto
		 , c520_ship_to_id = v_flag1
		 , c901_purpose = v_flag
		 , c520_request_for = DECODE (p_type, 4110, 40021,102930, 102930,26240144,26240144,40022) --26240144:Return to Owner company
		 , c1910_division_id = v_division_id 
		 , c1900_company_id = NVL(v_company_id,c1900_company_id) 
	 WHERE c520_request_id = v_mrid;

	UPDATE t520_request
	   SET c520_request_to = v_billto
		 , c520_last_updated_by = p_userid
		 , c520_last_updated_date = CURRENT_DATE
		 , c901_ship_to = v_shipto
		 , c520_ship_to_id = v_flag1
		 , c901_purpose = v_flag
		 , c520_request_for = DECODE (p_type, 4110, 40021, 102930, 102930,26240144,26240144,40022) --26240144:Return to Owner company
		 , c1910_division_id = v_division_id 
		 , c1900_company_id = NVL(v_company_id,c1900_company_id) 
	 WHERE c520_master_request_id = v_mrid;

	--When type is return to owner company then void backorder requests.
	IF p_type = 26240144 --26240144:Return to Owner company
	THEN
		UPDATE t520_request 
		   SET c520_void_fl='Y'
		     , c520_last_updated_by = p_userid
		     , c520_last_updated_date = CURRENT_DATE
		 WHERE c520_master_request_id = v_mrid
		   AND c520_status_fl = 10 --backOrder request
		   AND c520_void_fl IS NULL;
	END IF;
--
	IF p_type = '4111'
	THEN
		FOR rad_val IN pop_val
		LOOP
			v_price 	:= get_part_price (rad_val.ID, 'L');

			UPDATE t505_item_consignment
			   SET c505_item_price = v_price
			 WHERE c205_part_number_id = rad_val.ID AND c504_consignment_id = p_consgid;
		END LOOP;
	ELSIF p_type = '26240144'--Return to Owner company
	THEN
		 BEGIN
		    SELECT get_rule_value_by_company(C1900_PARENT_COMPANY_ID,'PRIMARY-DIST',v_company_id)
		      INTO v_primary_dist_id
		      FROM t701_distributor
		     WHERE c701_distributor_id = v_billto
		       AND C701_VOID_FL IS NULL;
		  EXCEPTION
		  WHEN NO_DATA_FOUND THEN
	    	v_primary_dist_id := NULL;
	    END;
    	
	    IF v_primary_dist_id IS NULL 
	    THEN
	    	raise_application_error ('-20537', ''); -- invalid distributor id.
	    END IF;
	       
	    FOR rad_val IN pop_val
		LOOP
			v_price := NVL(get_account_part_pricing (NULL, rad_val.ID, get_distributor_inter_party_id (v_primary_dist_id)),0);

			UPDATE t505_item_consignment
			   SET c505_item_price = v_price
			 WHERE c205_part_number_id = rad_val.ID AND c504_consignment_id = p_consgid;
		END LOOP;
	ELSE
		FOR rad_val IN pop_val
		LOOP
			v_price 	:= get_part_price (rad_val.ID, 'E');

			UPDATE t505_item_consignment
			   SET c505_item_price = v_price
			 WHERE c205_part_number_id = rad_val.ID AND c504_consignment_id = p_consgid;
		END LOOP;
	END IF;

--
	IF p_type = '4119' OR p_type = '4127' OR p_type = '40050'
	THEN
		IF p_type = '4119' OR p_type = '4127'
		THEN
--check to see if there's any open inhouse transaction exists
			DELETE FROM my_temp_list;

			INSERT INTO my_temp_list
						(my_temp_txn_id)
				SELECT get_rule_value (c412_type, 'OPEN-INHOUSE-TRANS')
				  FROM t412_inhouse_transactions
				 WHERE c412_ref_id = p_consgid AND c412_void_fl IS NULL;

			--50160 Move from Shelf to Built Sets (SB)
			--50161 Move from Built sets to Shelf (BS)
			--50162 Move from Built sets to Quarantine (BQ)
			SELECT COUNT (1)
			  INTO v_count
			  FROM my_temp_list
			 WHERE my_temp_txn_id IS NOT NULL;

			IF (v_count > 0)
			THEN
				raise_application_error ('-20225', '');
			--There are some open inhouse transactions exist, please void or complete them to proceed process.
			END IF;

--Check to see if there's any back order request exists
			SELECT COUNT (1)
			  INTO v_count
			  FROM t504_consignment t504, t520_request t520
			 WHERE t504.c520_request_id = t520.c520_master_request_id
			   AND t504.c504_consignment_id = p_consgid
			   AND t520.c520_status_fl = 10
			   AND t520.c520_void_fl IS NULL
			   AND t504.c504_void_fl IS NULL;
	-- this is comment for merge OUS change, Create Loaner back order transactions for missing parts in a set when moving it to the loaner pool 

		/*	IF (v_count > 0)
			THEN
				raise_application_error ('-20226', '');
			--There are one or more  back order request exist, please void or complete them to proceed process.
			END IF;	*/
		END IF;

		UPDATE t504_consignment
		   SET c504_status_fl = 4
			 , c504_inhouse_purpose = NULL
			 , c504_ship_date = NULL
			 , c504_ship_to = NULL
			 , c504_ship_to_id = NULL
			 , c504_ship_req_fl = NULL
			 , C504_COMMENTS=p_ict_notes
			 , c504_last_updated_by = p_userid
			 , c504_last_updated_date = CURRENT_DATE			 
		 WHERE c504_consignment_id = p_consgid AND c504_void_fl IS NULL;
		 
		IF p_type = '4119'		 --In House Loaner
		THEN
			 v_status 	:= 58;   --Pending FG
		ELSIF p_type = '4127'	 --Product Loaner
		THEN
		  IF get_rule_value (v_company_id,'SKIP_PENDING_PICTURE')  = 'Y' THEN
			 v_status 	:= 58;   --Pending FG
		  ELSE
		     v_status 	:= 55;  --Pending Picture
		  END IF;   
		END IF;

		INSERT INTO t504a_consignment_loaner
					(c504_consignment_id, c504a_status_fl, c504a_loaner_create_date, c504a_loaner_create_by, C504A_PLANNED_SHIP_DATE,c5040_plant_id
					)
			 VALUES (p_consgid,  v_status, TRUNC (CURRENT_DATE), p_userid, p_plan_ship_date,v_plant_id
					);	--change to v_status instead of 0 (available)
	
	--Changes added for PMT-12455 and it's used to trigger an update to iPad				
	gm_pkg_op_loaner_set_txn.GM_SAV_AVAIL_LN_SET_UPDATE (p_consgid, p_userid, 'Y'); -- consignment_id, user_id
--
		IF p_type = '4119'
		THEN
			--PC-992: Built set Demo to In-house consignment posting changes
			v_post_id	:= NVL(get_rule_value (v_set_type, 'INHOUSE-LOANER'),get_rule_value ('INHLN', 'INHOUSE-LOANER'));
		ELSIF p_type = '4127'
		THEN
			v_post_id	:= NVL(get_rule_value (v_set_type, 'PRODUCT-LOANER'),get_rule_value ('PDTLN', 'PRODUCT-LOANER'));
		ELSIF p_type = '40050'
		THEN
			v_post_id	:= get_rule_value ('HPTLN', 'HOSPITAL-LOANER');
		END IF;

--
			FOR rad_val IN cur_consign
			LOOP
				--
				gm_save_ledger_posting (v_post_id
									  , CURRENT_DATE
									  , rad_val.ID
									  , '01'
									  , p_consgid
									  , rad_val.qty
									  , rad_val.price
									  , p_userid
									   );
			--
			END LOOP;	

		-- if consg type is In-House Loaner/Product Loaner/Hospital Consignment, and if shipping record exists then void it
		gm_pkg_common_cancel.gm_void_shipping (p_consgid, p_userid, 50181);
--
	ELSE
		IF v_billto <> '01'
		THEN
			UPDATE t504_consignment
			   SET c701_distributor_id = v_billto
				 , c704_account_id = ''
			     , c504_last_updated_by = p_userid
			     , c504_last_updated_date = CURRENT_DATE				 
			 WHERE c504_consignment_id = p_consgid AND c504_void_fl IS NULL;
		ELSE
			UPDATE t504_consignment
			   SET c701_distributor_id = ''
				 , c704_account_id = v_billto
			     , c504_last_updated_by = p_userid
			     , c504_last_updated_date = CURRENT_DATE				 
			 WHERE c504_consignment_id = p_consgid AND c504_void_fl IS NULL;
		END IF;
		
		IF v_ship_flag = '2.20'			--Pending Pick
		THEN
			gm_pkg_allocation.gm_ins_invpick_assign_detail (93601, p_consgid, p_userid);
			--93601 consignment set , to insert record in the t5050_INVPICK_ASSIGN_DETAIL
		END IF;
		
		v_shipid := gm_pkg_cm_shipping_info.get_shipping_id (p_consgid, 50181, '');
		
		BEGIN
			
			--check if request is valid
			gm_op_fch_ln_req_fl (p_link_ln_req, v_link_flag);

			IF p_link_ln_req IS NOT NULL AND v_link_flag = 'N'
			THEN
				GM_RAISE_APPLICATION_ERROR('-20999','335',p_link_ln_req);
			END IF;

			SELECT c525_product_request_id
				INTO v_pdt_req_id
			FROM t526_product_request_detail
			WHERE c526_product_request_detail_id = p_link_ln_req
			AND c526_void_fl IS NULL;
			    
			EXCEPTION WHEN NO_DATA_FOUND
			THEN
				v_pdt_req_id := NULL;
		END;
         
		UPDATE t907_shipping_info
		SET c525_product_request_id = NVL(v_pdt_req_id,c525_product_request_id)                    
			, C901_temp_source = NVL2(p_link_ln_req,50182,NULL)
			, c907_last_updated_by = p_userid
			, c907_last_updated_date = CURRENT_DATE
		WHERE  c907_shipping_id = v_shipid
		AND c907_void_fl IS NULL;


	--comment the code since  status changed to Pending Pick instead of Pending Shipping
	--uncomment the code for OUS, since it's status is pending shipping
		IF NVL(v_country, '-999') != 'Y'
		 THEN
		 	gm_pkg_cm_shipping_trans.gm_sav_release_shipping (p_consgid, 50181, p_userid, '51076',p_link_ln_req);
		END IF;
	
	END IF;
	-- this is migrated from OUS
 	IF (v_count > 0)
	THEN
		gm_pkg_op_loaner.gm_sav_create_bcklon_item(p_consgid,p_type,p_shipto,p_shiptoid,p_userid,p_message);
		
		-- Voiding the backorder request as it is been moved to the back order loaner request.
		UPDATE t520_request SET c520_void_fl='Y',
		C520_LAST_UPDATED_BY = p_userid,C520_LAST_UPDATED_DATE = CURRENT_DATE
		WHERE c520_request_id  IN (SELECT T520.c520_request_id
		FROM t504_consignment t504, t520_request t520
			 WHERE t504.c520_request_id = t520.c520_master_request_id
			 AND t504.c504_consignment_id = p_consgid
			 AND t520.c520_status_fl = 10
			 AND t520.c520_void_fl IS NULL
			 AND t504.c504_void_fl IS NULL);
	END IF; 
	
-- Check the Loaner CN Status Value
	BEGIN
	
	SELECT c504_status_fl
   	 INTO v_ln_status
     FROM t504_consignment
     WHERE c504_consignment_id = p_consgid;
	
	EXCEPTION WHEN NO_DATA_FOUND THEN
      v_ln_status := NULL;     
    END;
    
-- If the Status value is 4 and Loaner type is 4127 or 4119 move the Set details to Loaner Net Number table
-- PMT : 13955. Author :gpalani Date: 09/2017	
	if (v_ln_status='4' and (p_type='4127' or p_type='4119')) THEN
		
			
		    gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv(p_consgid,'T505',p_consgid,p_type,NULL,p_type,p_userid,p_type);
			
			gm_pkg_set_lot_track.gm_pkg_set_lot_update(p_consgid,p_userid,p_consgid,p_type,'6');
	
	END IF;
--
END gm_update_set_consign;
/
