CREATE OR REPLACE PROCEDURE GM_OP_UPD_INFORMALRECV (
	p_VendId	IN	T408_DHR.C301_VENDOR_ID%TYPE,
    p_Comments	IN  T408_DHR.C408_COMMENTS%TYPE,
    p_PackSlip	IN	T408_DHR.C408_PACKING_SLIP%TYPE,
    p_str		IN	VARCHAR2,
    p_RecDate	IN  VARCHAR2,
    p_Type		IN  VARCHAR2,
    p_UserId	IN  T408_DHR.C408_CREATED_BY%TYPE,
    p_dhr_ids	OUT VARCHAR2
)
AS
--
/******************************************************************************
 * Description :  This procedure is to update the DHR's initated for the parts like
 *				  Allosource where the DHR has to be received and dont neeed 
 *				  Inspection, Packaging and Verification
 *				  
 *******************************************************************************/
v_string		VARCHAR2(30000);
v_substring		VARCHAR2(1000);
v_qty_recv		t408_dhr.c408_qty_received%TYPE;
v_vend_id		t301_vendor.c301_vendor_id%TYPE; 
v_part_num		t205_part_number.c205_part_number_id%TYPE;
v_cost_price	t402_work_order.c402_cost_price%TYPE;
p_errmsg		VARCHAR2(200);
--
BEGIN
--
GM_INITIATE_DHR(p_VendId,p_Comments,p_PackSlip,p_str,p_RecDate,p_Type,p_UserId,p_dhr_ids,p_errmsg);
v_string := p_dhr_ids;
--
 
--
WHILE INSTR(v_string,',') <> 0 
LOOP
-- Getting the each from the list of DHR's which have been created during the PO receive process
	v_substring		:= SUBSTR(v_string,1,INSTR(v_string,',')-1);
	v_string		:= SUBSTR(v_string,INSTR(v_string,',')+1);
--	Selecting values which needs to be updated for the Receive process
			SELECT	 t408.c408_qty_received
					,t408.c301_vendor_id 
					,t408.c205_part_number_id 
					,decode(t408.c901_type, 80191 , t402.c402_split_cost ,decode(t402.c402_rm_inv_fl, 'Y', t402.c402_cost_price - t402.c402_split_cost , t402.c402_cost_price)) 
			INTO	 v_qty_recv
					,v_vend_id
					,v_part_num
					,v_cost_price
			FROM	t408_dhr t408, 
					t402_work_order t402
			WHERE	t408.c408_dhr_id = v_substring
			AND		t408.c402_work_order_id = t402.c402_work_order_id -- joining WO table to get the price
			AND		t408.c408_update_inv_fl	IS NULL ; -- checking if the inventory flag is null to avoid duplicate updation

-- Update Inventory
		gm_cm_sav_partqty (v_part_num,
                               v_qty_recv,
                               v_substring,
                               p_userid,
                               90800, -- FG Qty
                               4301,  -- Plus
                               4314  -- Purchase
                               );
--		
		GM_SAVE_LEDGER_POSTING(4810,SYSDATE,v_part_num,65,v_substring,v_qty_recv,v_cost_price,p_userid);
-- Set Inventory Status Flag to 4 thereby making the DHR complete verification
 		UPDATE   t408_dhr
		SET		 c408_status_fl = 4
				,c408_qty_on_shelf = v_qty_recv
				,c408_update_inv_fl = 1
		WHERE c408_dhr_id = v_substring;
END LOOP;
--
END GM_OP_UPD_INFORMALRECV;
/

