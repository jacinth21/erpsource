CREATE OR REPLACE PROCEDURE gm_create_ncmr
(
	 p_dhrid	  		IN  t409_ncmr.c408_dhr_id%TYPE
	,p_userid			IN  t409_ncmr.c409_created_by%TYPE
	,p_rejreason_type	IN	t409_ncmr.c901_reason_type%TYPE
	,p_rejreason		IN  t409_ncmr.c409_rej_reason%TYPE
	,p_ncmrid			OUT VARCHAR2
	,p_message 			OUT VARCHAR2
)
AS
--
	v_pdt_id	VARCHAR2(20);
	v_string	VARCHAR2(20);
	v_id_string VARCHAR2(20);
	v_company_id  t1900_company.c1900_company_id%TYPE;
   v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
--
CURSOR pop_val IS
   	SELECT  c205_part_number_id id,
			c402_work_order_id woid,
			c301_vendor_id vid
	FROM	t408_dhr
	WHERE	c408_dhr_id = p_dhrid;
--
BEGIN
--
	SELECT	s409_ncmr.nextval, get_compid_frm_cntx(), get_plantid_frm_cntx()
	INTO	v_pdt_id, v_company_id, v_plant_id
	FROM	dual;
	--
	IF LENGTH(v_pdt_id) = 1
	THEN
		v_id_string := '0' || v_pdt_id;
	ELSE
		v_id_string := v_pdt_id;
	END IF;
	--
	SELECT 'GM-NCMR-' || v_id_string
	INTO	v_string
	FROM	dual;
	--
	p_NCMRId := v_string;
	--
	FOR rad_val IN pop_val
	LOOP
		INSERT INTO t409_ncmr(
			 c409_ncmr_id
			,c408_dhr_id
			,c402_work_order_id
			,c205_part_number_id
			,c301_vendor_id
			,c409_status_fl
			,c409_created_by
			,c409_created_date
			,c409_rej_reason
			,c901_reason_type
			,c1900_company_id
			,C5040_PLANT_ID
		) VALUES (
			 v_string
			,p_dhrid
			,rad_val.woid
			,rad_val.id
			,rad_val.vid
			,1			-- Status Flag for Initiated
			,p_userid
			,CURRENT_DATE
			,p_rejreason
			,DECODE(p_rejreason_type,0,NULL,p_rejreason_type)
			,v_company_id
			,v_plant_id
		 );
	END LOOP;
	--
END gm_create_ncmr;
/

