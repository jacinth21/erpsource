/* Formatted on 2011/04/06 14:13 (Formatter Plus v4.8.0) */
CREATE OR REPLACE PROCEDURE gm_save_item_consign (
	p_consgid		IN	 t504_consignment.c504_consignment_id%TYPE
  , p_type			IN	 t504_consignment.c504_type%TYPE
  , p_purpose		IN	 t504_consignment.c504_inhouse_purpose%TYPE
  , p_billto		IN	 t504_consignment.c701_distributor_id%TYPE 
  , p_shipto		IN	 t504_consignment.c504_ship_to%TYPE
  , p_shiptoid		IN	 t504_consignment.c504_ship_to_id%TYPE
  , p_fincomments	IN	 t504_consignment.c504_comments%TYPE
  , p_shipfl		IN	 t504_consignment.c504_ship_req_fl%TYPE
  , p_userid		IN	 t504_consignment.c504_last_updated_by%TYPE
  , p_str			IN	 VARCHAR2
 
  
)
AS
--
	v_flag		   VARCHAR2 (20);
	v_flag1 	   VARCHAR2 (20);
	v_msg		   VARCHAR2 (1000);
	v_price 	   VARCHAR2 (20);
	v_voidflag	   VARCHAR2 (20);
	v_inv_type	   VARCHAR2 (30);
	v_allocate_fl  VARCHAR2 (1);
    v_company_id  t1900_company.c1900_company_id%TYPE;
    v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
    v_dist_company_id  t1900_company.c1900_company_id%TYPE;
	v_loaner_filter   VARCHAR2(20);
	v_default_company   t1900_company.c1900_company_id%TYPE;
	v_shipid 	   t504_consignment.c504_ref_id%TYPE;   
	
	
	CURSOR pop_val
	IS
		SELECT c205_part_number_id ID
		  FROM t505_item_consignment
		 WHERE c504_consignment_id = p_consgid;
BEGIN

	   
      SELECT  NVL(GET_COMPID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL')), NVL(GET_PLANTID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_PLANT','ONEPORTAL'))
        INTO v_company_id, v_plant_id
        FROM DUAL;	
        
--
	IF p_purpose = '0'
	THEN
		v_flag		:= '';
	ELSE
		v_flag		:= p_purpose;
	END IF;

	--
	IF p_shiptoid = '0'
	THEN
		v_flag1 	:= '';
	ELSE
		v_flag1 	:= p_shiptoid;
	END IF;
	
	SELECT get_plant_parent_comp_id(v_plant_id)
	          INTO v_default_company 
	          FROM DUAL;
	          
	IF v_default_company IS NOT NULL AND v_default_company !=  v_company_id THEN
	   v_company_id := v_default_company;
	   gm_pkg_cor_client_context.gm_sav_client_context(v_default_company,v_plant_id);
	END IF;
   
	--
	INSERT INTO t504_consignment
				(c504_consignment_id, c504_type, c504_inhouse_purpose, c504_ship_to, c504_ship_to_id, c504_comments
			   , c504_ship_req_fl, c504_status_fl, c504_created_by, c504_created_date, c504_last_updated_by
			   , c504_last_updated_date, c1900_company_id, c5040_plant_id
				)
		 VALUES (p_consgid, p_type, v_flag, p_shipto, v_flag1, p_fincomments
			   , p_shipfl, '2', p_userid, CURRENT_DATE, p_userid
			   , CURRENT_DATE, v_company_id, v_plant_id
				);

	--
	/*IF p_Type = '4111' THEN
	 BEGIN
		 FOR rad_val IN pop_val
		  LOOP
			 v_price := GET_PART_PRICE(rad_val.ID,'L');
			 UPDATE T505_ITEM_CONSIGNMENT SET C505_ITEM_PRICE = v_price WHERE C205_PART_NUMBER_ID = rad_val.ID;
		  END LOOP;
	 END;
	ELSE
	 BEGIN
		 FOR rad_val IN pop_val
		  LOOP
			 v_price := GET_PART_PRICE(rad_val.ID,'E');
			 UPDATE T505_ITEM_CONSIGNMENT SET C505_ITEM_PRICE = v_price WHERE C205_PART_NUMBER_ID = rad_val.ID;
		  END LOOP;
	 END;
	END IF;
	*/
	--
	IF p_billto <> '01'
	THEN
		UPDATE t504_consignment
		   SET c701_distributor_id = p_billto
			 , c704_account_id = ''
			 , c504_last_updated_by = p_userid
			 , c504_last_updated_date = CURRENT_DATE
		 WHERE c504_consignment_id = p_consgid;
	ELSE
		UPDATE t504_consignment
		   SET c701_distributor_id = ''
			 , c704_account_id = p_billto
			 , c504_last_updated_by = p_userid
			 , c504_last_updated_date = CURRENT_DATE
		 WHERE c504_consignment_id = p_consgid;
	END IF;

	--
	gm_save_item_build (p_consgid, p_str, p_shipfl, p_userid);
	
	IF get_rule_value ('SHIPPING_REQUIRED', p_type) = 'Y' THEN
			--Shipping detail
			gm_pkg_cm_shipping_trans.gm_sav_ship_info (p_consgid	 --refid
													, 4000518   --RETURNS 4121source
													, 4120	 -- (sales rep)p_ship_to
													, p_billto	--North America
													, 5001	 -- carrier
													, 5004	 -- delivery mode
													, NULL	 -- tracking_number
													, NULL	 -- address_id
													, 0   -- freight amount
													, p_userid
													, v_shipid	 -- out shipid
													 );
	END IF;

	--

	--Type is Dummy Consignment
	IF p_type = 4129
	THEN
		UPDATE t504_consignment
		   SET c504_update_inv_fl = 1
		   	 , c504_last_updated_by = p_userid
			 , c504_last_updated_date = CURRENT_DATE
		 WHERE c504_consignment_id = p_consgid;

		UPDATE t505_item_consignment
		   SET c505_control_number = 'NOC##'
		 WHERE c504_consignment_id = p_consgid;
	END IF;

	-- Create Record in T5050 table for Device to process
	-- If the transaction is from "shelf ->Quarantine" it has to be picked from device.
	BEGIN
		SELECT c901_code_id
		  INTO v_inv_type
		  FROM t901_code_lookup t901
		 WHERE t901.c901_code_grp IN ('INVPT', 'INVPW')
		   AND t901.c902_code_nm_alt = TO_CHAR (p_type)
		   AND t901.c901_active_fl = '1';

		v_allocate_fl := gm_pkg_op_inventory.chk_txn_details (p_consgid, v_inv_type);

		IF NVL (v_allocate_fl, 'N') = 'Y'
		THEN
			gm_pkg_allocation.gm_ins_invpick_assign_detail (TO_NUMBER (v_inv_type), p_consgid, p_userid);
		END IF;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN;
	END;
END gm_save_item_consign;
/
