/* Formatted on 2009/05/13 13:44 (Formatter Plus v4.8.0) */
--@"C:\Database\Procedures\Operations\Gm_Initiate_Dhr.prc"
create or replace
PROCEDURE Gm_Initiate_Dhr (
        p_VendId IN T408_DHR.C301_VENDOR_ID%TYPE,
        p_Comments IN T408_DHR.C408_COMMENTS%TYPE,
        p_PackSlip IN T408_DHR.C408_PACKING_SLIP%TYPE,
        p_str IN CLOB,
        p_RecDate IN VARCHAR2,
        p_Type IN VARCHAR2,
        p_UserId IN T408_DHR.C408_CREATED_BY%TYPE,
        v_all_pdt_ids OUT CLOB,
        p_errmsg OUT VARCHAR2)
AS
    v_string    CLOB := p_str;
    v_PNum      VARCHAR2 (20) ;
    v_Qty       VARCHAR2 (20) ;
    v_Control   t408_dhr.c408_control_number%TYPE;
    v_WO        VARCHAR2 (20) ;
    V_MANFDATE  varchar2 (20) ;
    v_ExpDate   VARCHAR2 (20) ;
    v_substring VARCHAR2 (1000) ;
    v_msg       VARCHAR2 (1000) ;
    v_pdt_id    VARCHAR2 (20) ;
    v_UDI_Num	VARCHAR2 (200);
    v_part_udi  VARCHAR2 (200);
    v_di_prefix VARCHAR2(10);
    v_cn_prefix VARCHAR2(10);
    v_ed_prefix VARCHAR2(10);
    v_exp_date  VARCHAR2(10);
    v_show_udi_box VARCHAR2(10);
    V_DonorNum t2540_donor_master.c2540_donor_number%TYPE;
    v_Prod_Type NUMBER;
    v_donor_id NUMBER;
    
BEGIN
    --
    
       
    gm_pkg_op_dhr_rpt.gm_validate_control_no (p_str) ;
    WHILE INSTR (v_string, '|') <> 0
    LOOP
        v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
        v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1) ;
        v_PNum := NULL;
        v_WO := NULL;
        v_Qty := NULL;
        v_Control := NULL;
        V_DonorNum := NULL;
        V_MANFDATE := NULL;
        v_ExpDate := NULL;
        v_UDI_Num := NULL;
        v_Prod_Type := NULL;
        --
        v_PNum := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
        v_WO := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
        v_Qty := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
        v_Control := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        V_SUBSTRING := SUBSTR (V_SUBSTRING, INSTR (V_SUBSTRING, '^') + 1) ;
        V_DonorNum := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        V_SUBSTRING := SUBSTR (V_SUBSTRING, INSTR (V_SUBSTRING, '^') + 1) ;
        V_MANFDATE := SUBSTR (V_SUBSTRING, 1, INSTR (V_SUBSTRING, '^') - 1) ;
        V_SUBSTRING := SUBSTR (V_SUBSTRING, INSTR (V_SUBSTRING, '^') + 1) ;        
        v_ExpDate := SUBSTR (V_SUBSTRING, 1, INSTR (V_SUBSTRING, '^') - 1);
        V_SUBSTRING := SUBSTR (V_SUBSTRING, INSTR (V_SUBSTRING, '^') + 1) ;
        v_UDI_Num := SUBSTR (V_SUBSTRING, 1, INSTR (V_SUBSTRING, '^') - 1);
        V_SUBSTRING := SUBSTR (V_SUBSTRING, INSTR (V_SUBSTRING, '^') + 1) ;
        v_Prod_Type :=  V_SUBSTRING;
        
       IF V_DonorNum IS NOT NULL
       THEN        
        gm_pkg_op_dhr.gm_op_sav_donor_number(V_DonorNum, p_VendId, p_UserId, v_donor_id);
       
       END IF;
       
        -- To create Product Traveller
       
        Gm_Create_Dhr (v_WO, p_VendId, v_PNum, v_Control, v_Qty, v_ManfDate, p_PackSlip, p_RecDate, p_Comments, p_Type,
        p_UserId, v_pdt_id, v_msg,v_donor_id,v_ExpDate) ;
        
       
        --comment following code since UDI number will get from front side by UDI format
    /*    v_part_udi := gm_pkg_pd_rpt_udi.get_part_di_number(v_PNum); -- Get the DI No for Part 
        v_show_udi_box := gm_pkg_op_dhr.get_di_no_from_vendor(v_PNum); -- Get the SHow UDI TExt box value.
        v_UDI_Num := NULL;
        IF v_part_udi IS not null THEN
        -- UDI No is formed   
        IF (v_UDI_Num IS null AND v_show_udi_box <> 'Y')  THEN
        	v_UDI_Num := gm_pkg_pd_rpt_udi.get_dhr_part_udi(v_pdt_id, v_PNum);
        END IF;
        END IF;
       */ 
        -- To save/update UDI No value 
        gm_pkg_op_dhr.gm_save_di_no(v_pdt_id,v_UDI_Num,p_UserId);
        
        -- To create Product Traveller Item
        --GM_CREATE_DHR_ITEM(v_pdt_id,v_WO,v_PNum,v_Control,v_Qty,v_ManfDate,p_PackSlip,p_RecDate,p_UserId,v_msg);
        v_all_pdt_ids := v_all_pdt_ids || v_pdt_id || ',';
        -- Save Expiry Date-- This Code is commented for BBA-73. This Code is moved when DHR is verified section
        /*IF V_EXPDATE IS NOT NULL THEN
            GM_OP_SAV_LOT_EXPIRY_DATE (V_PNUM, V_CONTROL,V_EXPDATE,P_USERID);
        END IF;*/
    END LOOP;
END Gm_Initiate_Dhr;
/