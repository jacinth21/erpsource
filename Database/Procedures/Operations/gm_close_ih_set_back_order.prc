
--@"C:\Database\Procedures\Operations\gm_close_ih_set_back_order.prc";

CREATE OR REPLACE PROCEDURE gm_close_ih_set_back_order (
   p_txnid      IN       t412_inhouse_transactions.c412_inhouse_trans_id%TYPE
 , p_txn_type   IN       t412_inhouse_transactions.c412_type%TYPE
 , p_consignid	IN OUT   t504_consignment.c504_consignment_id%TYPE
)
AS
   v_consign_id        VARCHAR2(20);
   v_back_ord_id	   t412_inhouse_transactions.c412_ref_id%TYPE;	
   v_back_ord_qty	   NUMBER;	
BEGIN
	v_consign_id:=gm_pkg_op_ihloaner_item_rpt.get_back_order_consign_id(p_txnid,p_txn_type);
 		-- Check and close ISBO transaction
 IF v_consign_id IS NOT NULL AND v_consign_id != p_consignid THEN
	 BEGIN
		 SELECT c412_inhouse_trans_id, SUM(C413_ITEM_QTY) 
		   INTO v_back_ord_id, v_back_ord_qty
		   FROM t413_inhouse_trans_items t413
		  WHERE t413.c412_inhouse_trans_id IN 
				(SELECT t412.c412_ref_id
				   FROM t412_inhouse_transactions t412 
				  WHERE t412.c412_inhouse_trans_id = p_txnid
				 )
	   GROUP BY c412_inhouse_trans_id;		 
	 EXCEPTION WHEN NO_DATA_FOUND THEN
		v_back_ord_id := NULL;
	    v_back_ord_qty := -1;
	 END;
	
	 IF v_back_ord_qty = 0 THEN
	   UPDATE t412_inhouse_transactions t412
		  SET t412.c412_status_fl = 4 
            , t412.c412_last_updated_date = SYSDATE
	    WHERE t412.c412_inhouse_trans_id = v_back_ord_id;
	 END IF;	    
 END IF;
 
 IF v_consign_id IS NULL OR v_consign_id = p_txnid THEN
		v_consign_id := p_consignid;
 END IF;
 
 -- Return p_consign_id
 
 p_consignid := v_consign_id;
 
END gm_close_ih_set_back_order;	    	
/
	    		 		 