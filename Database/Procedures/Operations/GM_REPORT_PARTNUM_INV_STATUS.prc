/* Formatted on 2007/08/28 17:30 (Formatter Plus v4.8.0) */
CREATE OR REPLACE PROCEDURE gm_report_partnum_inv_status (
	p_num		  IN	   t205_part_number.c205_part_number_id%TYPE
  , p_recordset   OUT	   TYPES.cursor_type
)
AS
/****************************************************************************************
 * Description			 :This procedure is called to get PART NUMBER ALLOCATION DETAILS
 ****************************************************************************************/
--
BEGIN
	OPEN p_recordset
	 FOR
		 SELECT t205.c205_part_number_id pnum, t205.c205_part_num_desc pdesc
			  , (get_partnumber_qty(t205.c205_part_number_id,90813) - NVL ((quar_alloc_to_scrap + quar_alloc_to_inv), 0)) inquaran
			  , get_partnumber_qty(t205.c205_part_number_id,90814) - NVL (bulk_alloc_to_inv, 0) inbulk, get_partnumber_qty(t205.c205_part_number_id,90812) inreturns
			  , c205_rev_num drawrev, get_partnumber_po_pend_qty (t205.c205_part_number_id) popend
			  , get_partnumber_po_wip_qty (t205.c205_part_number_id) powip, sales_allocated, cons_alloc, loan_alloc
			  , quar_alloc_self_quar, quar_alloc_to_scrap, quar_alloc_to_inv, bulk_alloc_to_set, bulk_alloc_to_inv
			  , inv_alloc_to_pack
			  ,   get_partnumber_qty(t205.c205_part_number_id,90800)
				- (  NVL (inv_alloc_to_pack, 0)
				   + NVL (sales_allocated, 0)
				   + NVL (cons_alloc, 0)
				   + NVL (quar_alloc_self_quar, 0)
				   + NVL (bulk_alloc_to_set, 0)
				   + NVL (get_partnumber_qty(t205.c205_part_number_id,90812), 0)
				   + NVL (get_partnumber_qty(t205.c205_part_number_id,90814), 0)
				   + NVL (loan_alloc, 0)
				  ) inshelf
			  , (  NVL (sales_allocated, 0)
				 + NVL (cons_alloc, 0)
				 + NVL (quar_alloc_self_quar, 0)
				 + NVL (inv_alloc_to_pack, 0)
				 + NVL (bulk_alloc_to_set, 0)
				 + NVL (loan_alloc, 0)
				) shelfalloc
			  , (quar_alloc_to_scrap + quar_alloc_to_inv) quaralloc, bulk_alloc_to_inv bulkalloc
		   FROM t205_part_number t205
			  , (SELECT   b.c205_part_number_id, SUM (b.c502_item_qty) sales_allocated
					 FROM t501_order a, t502_item_order b, t205_part_number c
					WHERE a.c501_order_id = b.c501_order_id
					  AND a.c501_status_fl < 3
					  AND a.c501_status_fl > 0
					  AND a.c501_void_fl IS NULL
					  AND b.C502_VOID_FL IS NULL 
					  AND (a.c901_ext_country_id IS NULL
					  OR a.c901_ext_country_id  in (select country_id from v901_country_codes))
					  AND NVL (a.c901_order_type, -9999) <> 2524   -- Exclude the Price Adjustment Order
					  AND NVL (c901_order_type, -9999) NOT IN (
		                SELECT t906.c906_rule_value
		                  FROM t906_rules t906
		                 WHERE t906.c906_rule_grp_id = 'ORDTYPE'
		                   AND c906_rule_id = 'EXCLUDE')
					  --AND 	  a.c901_order_type IS NULL
					  AND a.c501_shipping_date IS NULL
					  AND b.c205_part_number_id = c.c205_part_number_id
					  AND c.c205_part_number_id = p_num
				 GROUP BY b.c205_part_number_id) sls
			  , 
				/* Below code to fetch allocated qty for
				 * 4110, 4111, 4112 maps to item consignment
				 * 4113 maps to self to quar , 4114 maps to self to repacking
				 * 4115 maps to quar to scrap, 4116 maps to quar to inventory (shelf)
				*/
				(SELECT   b.c205_part_number_id
						, SUM (DECODE (SIGN (4113 - a.c504_type), 1, b.c505_item_qty, 0)) cons_alloc
						, SUM (DECODE (a.c504_type, 4113, b.c505_item_qty, 0)) quar_alloc_self_quar
						, SUM (DECODE (a.c504_type, 4114, b.c505_item_qty, 0)) inv_alloc_to_pack
						, SUM (DECODE (a.c504_type, 4115, b.c505_item_qty, 0)) quar_alloc_to_scrap
						, SUM (DECODE (a.c504_type, 4116, b.c505_item_qty, 0)) quar_alloc_to_inv
					 FROM t504_consignment a, t505_item_consignment b, t205_part_number c
					WHERE a.c504_consignment_id = b.c504_consignment_id
					  AND a.c504_status_fl < 4
					  AND a.c207_set_id IS NULL
					  AND a.c504_type IN (4110, 4111, 4112, 4113, 4114, 4115, 4116)
					  AND a.c504_void_fl IS NULL
					  AND a.c504_ship_date IS NULL
					  AND b.c205_part_number_id = c.c205_part_number_id
					  AND c.c205_part_number_id = p_num
					  AND a.c504_void_fl IS NULL
				 GROUP BY b.c205_part_number_id) cons
			  , 
				/* Below code to fetch allocated qty for
				 * 4117 and 4118 maps to Bulk Transactions (IN and OUT)
				 * 50150 maps to 'Shelf To InHouse Set' and 50155 maps to 'Shelf to Loaner Set'
				*/
				(SELECT   b.c205_part_number_id, SUM (DECODE (a.c412_type, 4117, c413_item_qty, 0)) bulk_alloc_to_set
						, SUM (DECODE (a.c412_type, 4118, c413_item_qty, 0)) bulk_alloc_to_inv
						, SUM (DECODE (a.c412_type, 50150, c413_item_qty, 50155, c413_item_qty, 0)) loan_alloc
					 FROM t412_inhouse_transactions a, t413_inhouse_trans_items b, t205_part_number c
					WHERE a.c412_inhouse_trans_id = b.c412_inhouse_trans_id
					  AND a.c412_verified_date IS NULL
					  AND a.c412_void_fl IS NULL
					  AND a.c412_type IN (4117, 4118, 50150, 50155)
					  AND b.c205_part_number_id = c.c205_part_number_id
					  AND c.c205_part_number_id = p_num
				 GROUP BY b.c205_part_number_id) blktxn
		  WHERE t205.c205_part_number_id = p_num
			AND t205.c205_part_number_id = sls.c205_part_number_id(+)
			AND t205.c205_part_number_id = cons.c205_part_number_id(+)
			AND t205.c205_part_number_id = blktxn.c205_part_number_id(+);
--
END gm_report_partnum_inv_status;
/
