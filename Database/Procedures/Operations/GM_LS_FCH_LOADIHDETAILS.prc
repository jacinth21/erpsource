/* Formatted on 06/03/2010 11:00 (Formatter Plus v4.8.0) */
--@"c:\database\procedures\operations\gm_ls_fch_loadihdetails.prc"
CREATE OR REPLACE PROCEDURE gm_ls_fch_loadihdetails (
	p_txn_id	  IN	   t412_inhouse_transactions.c412_inhouse_trans_id%TYPE
  , p_recordset   OUT	   TYPES.cursor_type
)
AS
v_comp_dt_fmt VARCHAR2 (20);
v_plant_id    T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
/*****************************************************************************************************
  * This Procedure is to Fetch the details of the InHouse Transaction given the Transaction ID
  * Author : Joe P Kumar
  * Date   : 10/10/06
******************************************************************************************************/
BEGIN
--
	SELECT get_compdtfmt_frm_cntx(),get_plantid_frm_cntx() INTO v_comp_dt_fmt,v_plant_id FROM DUAL;
	OPEN p_recordset
	 FOR
	  SELECT t412.c412_inhouse_trans_id cid
	       , get_account_name ('01') aname
	       , get_user_name (t412.c412_created_by) uname
	  	   , to_char (t412.c412_created_date, v_comp_dt_fmt ||' hh:mi am') cdate
	  	   , t412.c412_verify_fl vfl
	  	   , t412.c412_status_fl sfl
	  	   , t412.c412_type ctype
	  	   , t412.c412_comments fcomments
	  	   , t412.c412_comments comments
	       , get_code_name (t412.c412_type) type
	       , t412.c412_verified_by vuser
	       , t412.c412_verified_date vdate
	       , get_user_name (t412.c412_release_to_id) rname
	       , get_code_name (t412.c412_inhouse_purpose) purp
	       , get_user_name (t412.c412_last_updated_by) luname
	       , to_char (t412.c412_last_updated_date, v_comp_dt_fmt ||' hh:mi am') ludate
	       , t412.c412_ref_id consignid
	       , gm_pkg_op_loaner.get_loaner_etch_id (t412.c412_ref_id) etchid
	       , get_set_name (gm_pkg_op_consignment.get_set_id (t412.c412_ref_id)) sname
	       , gm_pkg_op_loaner.get_cs_fch_loaner_status (t412.c412_ref_id) loanerstatus
	       , t412.c412_inhouse_purpose purposeid
	       , get_ticket_id (t412.c412_inhouse_trans_id) ticketid
	       , gm_pkg_op_loaner.get_loaner_trans_id (t412.c412_ref_id) transid
	       , t504a.c703_sales_rep_id repid
	       , get_rep_name(t504a.c703_sales_rep_id) repname
	       , t504a.c704_account_id accid
	       , get_account_dist_id (t504a.c704_account_id) distid
	       , get_distributor_name (get_account_dist_id (t504a.c704_account_id)) distnm
	       , t504a.c526_product_request_detail_id REQID
           , GET_LOG_FLAG(t504a.c526_product_request_detail_id,4000316) RECONCMTCNT
           , DECODE(t504a.C504A_TRANSF_REF_ID,'','N','Y') TRANSFER_FL
           , t504a.c703_ass_rep_id assocrepid
	       , get_rep_name(t504a.c703_ass_rep_id) assocrepnm
	       , t413.errcnt errcnt
	       , gm_pkg_op_loaner_set_txn.get_loaner_ship_date(p_txn_id) loandt
	   FROM t412_inhouse_transactions t412, t504a_loaner_transaction t504a,
	   		(SELECT count(1) errcnt 
				  FROM t413_inhouse_trans_items 
			 WHERE c412_inhouse_trans_id = p_txn_id 
				AND c413_error_dtls IS NOT NULL 
				AND c413_void_fl IS NULL) t413
	  WHERE t504a.c504a_loaner_transaction_id (+) = t412.c504a_loaner_transaction_id
	    AND t412.c412_inhouse_trans_id            = p_txn_id 
	    AND t412.c5040_plant_id                   = v_plant_id
	    AND t412.c412_void_fl                    IS NULL
	    AND t504a.c504a_void_fl                  IS NULL;
--
END gm_ls_fch_loadihdetails;
/
