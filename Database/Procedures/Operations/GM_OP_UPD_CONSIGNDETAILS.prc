CREATE OR REPLACE PROCEDURE GM_OP_UPD_CONSIGNDETAILS
(
  p_consignid IN T504_CONSIGNMENT.c504_consignment_id%TYPE
 ,p_etchid  IN T504A_CONSIGNMENT_LOANER.c504a_etch_id%TYPE
 ,p_purpose  IN T504_CONSIGNMENT.c504_inhouse_purpose%TYPE
)
AS

BEGIN

UPDATE  T504_CONSIGNMENT
SET  c504_inhouse_purpose = p_purpose
   , c504_last_updated_date = Sysdate
WHERE c504_consignment_id = p_consignid
AND C504_VOID_FL IS NULL;

UPDATE  T504A_CONSIGNMENT_LOANER
SET  c504a_etch_id = p_etchid
   , c504a_last_updated_date = Sysdate
WHERE c504_consignment_id = p_consignid;

END GM_OP_UPD_CONSIGNDETAILS;
/

