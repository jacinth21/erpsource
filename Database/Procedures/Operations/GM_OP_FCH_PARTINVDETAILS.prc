/* Formatted on 2009/05/13 13:55 (Formatter Plus v4.8.0) */

--@"c:\database\procedures\operations\gm_op_fch_partinvdetails.prc"

create or replace
PROCEDURE gm_op_fch_partinvdetails ( 
	p_part_id IN t205_part_number.c205_part_number_id%TYPE 
  , p_opt IN VARCHAR2 
  , p_recordset OUT TYPES.cursor_type 
) 
AS 
    v_comp_id T1900_COMPANY.C1900_COMPANY_ID%TYPE; 
    v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE; 
/***************************************************************************************************** 
  * This Procedure is to Fetch Inventory drilldown details from Inv Report 
  * Author : Dhinakaran James 
  * Date : 11/16/06 
******************************************************************************************************/ 
BEGIN 
-- 
SELECT get_compid_frm_cntx() INTO v_comp_id FROM DUAL; 
SELECT get_plantid_frm_cntx() INTO v_plant_id FROM DUAL; 
IF p_opt = 'PO' 
THEN 
OPEN p_recordset 
FOR 
SELECT * FROM ( 
SELECT get_vendor_name (c301_vendor_id) vname, c401_purchase_ord_id po, c402_qty_ordered ordqty 
, c402_work_order_id wo, get_wo_pend_qty (c402_work_order_id, c402_qty_ordered) pendqty 
, get_wo_wip_qty (c402_work_order_id) wip, c402_created_date cdate 
FROM t402_work_order 
WHERE c205_part_number_id = p_part_id AND c402_void_fl IS NULL AND c402_status_fl < 3 AND c1900_company_id = v_comp_id 
) WHERE pendqty > 0;

	-- Returns Hold Allocated Out Transaction 
	ELSIF p_opt = 'RHOLD'
	THEN
		OPEN p_recordset
		 FOR
			 SELECT a.c504_consignment_id refid, get_user_name (a.c504_ship_to_id) refname
				  , get_code_name (a.c504_type) reftype, get_code_name (a.c504_inhouse_purpose) refreason
				  , get_user_name (a.c504_created_by) cuser, a.c504_created_date cdate
				  , b.c505_item_qty qty
			   FROM t504_consignment a, t505_item_consignment b
			  WHERE a.c504_consignment_id = b.c504_consignment_id
				AND a.c504_status_fl < 4
				AND A.C207_SET_ID IS NULL
				AND a.c504_type IN ( select c906_rule_value from t906_rules where C906_RULE_ID='CONSIGNMENT' and C906_RULE_DESC='60007' and  C906_RULE_GRP_ID='V205C_ALLOCATED_QTY' and c906_void_fl is null)
				AND a.c504_void_fl IS NULL
				AND a.c504_ship_date IS NULL
				AND b.c205_part_number_id = p_part_id
				AND a.c5040_plant_id = v_plant_id
		UNION ALL
			SELECT a.c412_inhouse_trans_id refid, get_user_name (a.c412_release_to_id) refname
				  , get_code_name (a.c412_type) reftype, get_code_name (a.c412_inhouse_purpose) refreason
				  , get_user_name(a.c412_created_by) cuser, a.c412_created_date cdate, b.c413_item_qty qty
			   FROM t412_inhouse_transactions a, t413_inhouse_trans_items b
			  WHERE a.c412_inhouse_trans_id = b.c412_inhouse_trans_id
				AND a.c412_verified_date IS NULL
				AND a.c412_void_fl IS NULL
				AND b.c413_void_fl IS NULL 
				AND a.c412_type in(select c906_rule_value from t906_rules where C906_RULE_ID='INHOUSE' and C906_RULE_DESC='60007' and  C906_RULE_GRP_ID='V205C_ALLOCATED_QTY' and c906_void_fl is null)
				AND b.c205_part_number_id = p_part_id
				AND a.c5040_plant_id = v_plant_id;
	-- FG Allocated Out Transaction 
	ELSIF p_opt = 'SALLOC' 
	THEN
		OPEN p_recordset
		 FOR
			 SELECT a.c504_consignment_id refid
				  , DECODE (get_distributor_name (a.c701_distributor_id)
						  , '', get_account_name (a.c704_account_id)
						  , get_distributor_name (a.c701_distributor_id)
						   ) refname
				  , get_code_name (a.c504_type) reftype, get_code_name (a.c504_inhouse_purpose) refreason
				  , get_user_name (a.c504_created_by) cuser, a.c504_created_date cdate
				  , b.c505_item_qty qty
			   FROM t504_consignment a, t505_item_consignment b
			  WHERE a.c504_consignment_id = b.c504_consignment_id
				AND a.c504_status_fl < 4
				AND A.C207_SET_ID IS NULL
				AND a.c504_type IN (select c906_rule_value from t906_rules where C906_RULE_ID='CONSIGNMENT' and C906_RULE_DESC='60001' and  C906_RULE_GRP_ID='V205C_ALLOCATED_QTY' and c906_void_fl is null)
		        AND NVL(b.c901_warehouse_type,999) = DECODE(a.c504_type,4110,90800,4112,90800,111802,90800,NVL(b.c901_warehouse_type,999))
				AND a.c504_void_fl IS NULL
				AND a.c504_ship_date IS NULL
				AND b.c205_part_number_id = p_part_id
				AND a.c5040_plant_id = v_plant_id
			 UNION ALL 
			 SELECT a.c412_inhouse_trans_id refid, DECODE(a.c412_type, 9106, GET_CS_SHIP_NAME(a.c412_release_to, a.c412_release_to_id),get_user_name (a.c412_release_to_id)) refname
				  , get_code_name (a.c412_type) reftype, get_code_name (a.c412_inhouse_purpose) refreason
				  , get_user_name(a.c412_created_by) cuser, a.c412_created_date cdate, b.c413_item_qty qty
			   FROM t412_inhouse_transactions a, t413_inhouse_trans_items b
			  WHERE a.c412_inhouse_trans_id = b.c412_inhouse_trans_id
				AND a.c412_verified_date IS NULL
				AND A.C412_VOID_FL IS NULL
				AND a.c412_type IN (select c906_rule_value from t906_rules where C906_RULE_ID='INHOUSE' and C906_RULE_DESC='60001' and  C906_RULE_GRP_ID='V205C_ALLOCATED_QTY' and c906_void_fl is null)
				AND b.c205_part_number_id = p_part_id
				AND a.c5040_plant_id = v_plant_id
			 UNION ALL 
			 SELECT a.c501_order_id refid, get_account_name (a.c704_account_id) refname
				  , get_code_name (a.c901_order_type) reftype, '' refreason, get_user_name (a.c501_created_by) cuser
				  , a.c501_created_date cdate, b.c502_item_qty qty
			   FROM t501_order a, t502_item_order b
			  WHERE a.c501_order_id = b.c501_order_id
				AND a.c501_status_fl < 3
				AND a.c501_status_fl > 0
				AND a.c501_shipping_date IS NULL
				AND a.c501_void_fl IS NULL
				AND b.C502_VOID_FL IS NULL 
				AND (a.c901_ext_country_id IS NULL
				OR a.c901_ext_country_id  in (select country_id from v901_country_codes))
				AND NVL (a.c901_order_type, -9999) NOT IN ('2524','2520','101260')-- added to exclude qty price adjustment, quote,Acknowledgement Order
				AND NVL (c901_order_type, -9999) NOT IN (
		                SELECT t906.c906_rule_value
		                  FROM t906_rules t906
		                 WHERE t906.c906_rule_grp_id = 'ORDTYPE'
		                   AND c906_rule_id = 'EXCLUDE')
				-- Exclude the Price Adjustment Order
				AND b.c502_item_qty > 0 -- because consign-to-loaner swap is excluded
				AND b.c901_type = 50300
				AND b.c205_part_number_id = p_part_id
				AND a.c5040_plant_id = v_plant_id
			UNION ALL
			SELECT t5062.c5062_transaction_id refid,get_account_name (t501.c704_account_id) refname, get_code_name (t501.c901_order_type) reftype ,
                        '' refreason,get_user_name (c5062_reserved_by) cuser,
                        c5062_reserved_date cdate, c5062_reserved_qty
                  FROM t5062_control_number_reserve t5062,t5060_control_number_inv t5060,t501_order t501
              WHERE T5060.C5060_Control_Number_Inv_Id = T5062.C5060_Control_Number_Inv_Id
                AND T5062.C5062_Transaction_Id = T501.C501_Order_Id
                AND t5060.c205_part_number_id = p_part_id
                AND c901_status IN('103960')  -- open,in-progress
                AND C901_Transaction_Type IN ('103980') -- order trans
                AND t5060.c5040_plant_id = v_plant_id
                AND t5062.c5040_plant_id = v_plant_id
                AND t501.c5040_plant_id = v_plant_id
                AND t501.c1900_company_id = v_comp_id
                AND C5062_Void_Fl IS NULL;
	-- Quarantine Allocated Out Transaction 
	ELSIF p_opt = 'QALLOC'
	THEN
		OPEN p_recordset
		 FOR
			 SELECT a.c504_consignment_id refid, get_user_name (a.c504_ship_to_id) refname
				  , get_code_name (a.c504_type) reftype, get_code_name (a.c504_inhouse_purpose) refreason
				  , get_user_name (a.c504_created_by) cuser, a.c504_created_date cdate
				  , b.c505_item_qty qty
			   FROM t504_consignment a, t505_item_consignment b
			  WHERE a.c504_consignment_id = b.c504_consignment_id
				AND a.c504_status_fl < 4
				AND A.C207_SET_ID IS NULL
				AND a.c504_type IN (select c906_rule_value from t906_rules where C906_RULE_ID='CONSIGNMENT' and C906_RULE_DESC='60006' and  C906_RULE_GRP_ID='V205C_ALLOCATED_QTY' and c906_void_fl is null)
				AND a.c504_void_fl IS NULL
				AND a.c504_ship_date IS NULL
				AND b.c205_part_number_id = p_part_id
				AND a.c5040_plant_id = v_plant_id
			UNION ALL
			SELECT a.c412_inhouse_trans_id refid, get_user_name (a.c412_release_to_id) refname
				  , get_code_name (a.c412_type) reftype, get_code_name (a.c412_inhouse_purpose) refreason
				  , get_user_name(a.c412_created_by) cuser, a.c412_created_date cdate, b.c413_item_qty qty
			   FROM t412_inhouse_transactions a, t413_inhouse_trans_items b
			  WHERE a.c412_inhouse_trans_id = b.c412_inhouse_trans_id
				AND a.c412_verified_date IS NULL
				AND A.C412_VOID_FL IS NULL
				AND a.c412_type in(select c906_rule_value from t906_rules where C906_RULE_ID='INHOUSE' and C906_RULE_DESC='60006' and  C906_RULE_GRP_ID='V205C_ALLOCATED_QTY' and c906_void_fl is null)
				AND b.c205_part_number_id = p_part_id
				AND a.c5040_plant_id = v_plant_id;
				
	-- Repack Allocated Out Transaction 
	ELSIF p_opt = 'REPACKALLOC'
	THEN
		OPEN p_recordset
		 FOR
			 SELECT a.c504_consignment_id refid, get_user_name (a.c504_ship_to_id) refname
				  , get_code_name (a.c504_type) reftype, get_code_name (a.c504_inhouse_purpose) refreason
				  , get_user_name (a.c504_created_by) cuser, a.c504_created_date cdate
				  , b.c505_item_qty qty
			   FROM t504_consignment a, t505_item_consignment b
			  WHERE a.c504_consignment_id = b.c504_consignment_id
				AND a.c504_status_fl < 4
				AND a.c207_set_id IS NULL
				AND a.c504_type IN (select c906_rule_value from t906_rules where C906_RULE_ID='CONSIGNMENT' and C906_RULE_DESC='60005' and  C906_RULE_GRP_ID='V205C_ALLOCATED_QTY' and c906_void_fl is null)
				AND a.c504_void_fl IS NULL
				AND a.c504_ship_date IS NULL
				AND b.c205_part_number_id = p_part_id
				AND a.c5040_plant_id = v_plant_id
			UNION ALL
			SELECT a.c412_inhouse_trans_id refid, get_user_name (a.c412_release_to_id) refname
				  , get_code_name (a.c412_type) reftype, get_code_name (a.c412_inhouse_purpose) refreason
				  , get_user_name(a.c412_created_by) cuser, a.c412_created_date cdate, b.c413_item_qty qty
			   FROM t412_inhouse_transactions a, t413_inhouse_trans_items b
			  WHERE a.c412_inhouse_trans_id = b.c412_inhouse_trans_id
				AND a.c412_verified_date IS NULL
				AND A.C412_VOID_FL IS NULL
				AND a.c412_type in(select c906_rule_value from t906_rules where C906_RULE_ID='INHOUSE' and C906_RULE_DESC='60005' and  C906_RULE_GRP_ID='V205C_ALLOCATED_QTY' and c906_void_fl is null)
				AND b.c205_part_number_id = p_part_id
				AND a.c5040_plant_id = v_plant_id;
				
	-- Bulk Allocated Out Transaction 
	ELSIF p_opt = 'BALLOC'
	THEN
		OPEN p_recordset
		 FOR
			 SELECT a.c412_inhouse_trans_id refid, get_user_name (a.c412_release_to_id) refname
				  , get_code_name (a.c412_type) reftype, get_code_name (a.c412_inhouse_purpose) refreason
				  , get_user_name(a.c412_created_by) cuser, a.c412_created_date cdate, b.c413_item_qty qty
			   FROM t412_inhouse_transactions a, t413_inhouse_trans_items b
			  WHERE a.c412_inhouse_trans_id = b.c412_inhouse_trans_id
				AND a.c412_verified_date IS NULL
				AND A.C412_VOID_FL IS NULL
				AND a.c412_type in( select c906_rule_value from t906_rules where C906_RULE_ID='INHOUSE' and C906_RULE_DESC='60003' and  C906_RULE_GRP_ID='V205C_ALLOCATED_QTY' and c906_void_fl is null)
				AND b.c205_part_number_id = p_part_id
				AND a.c5040_plant_id = v_plant_id
			UNION ALL 
			SELECT a.c504_consignment_id refid, get_user_name (a.c504_ship_to_id) refname
				  , get_code_name (a.c504_type) reftype, get_code_name (a.c504_inhouse_purpose) refreason
				  , get_user_name (a.c504_created_by) cuser, a.c504_created_date cdate
				  , b.c505_item_qty qty
			   FROM t504_consignment a, t505_item_consignment b
			  WHERE a.c504_consignment_id = b.c504_consignment_id
				AND a.c504_status_fl < 4
				AND a.c207_set_id IS NULL
				AND a.c504_type IN (select c906_rule_value from t906_rules where C906_RULE_ID='CONSIGNMENT' and C906_RULE_DESC='60003' and  C906_RULE_GRP_ID='V205C_ALLOCATED_QTY' and c906_void_fl is null)
				AND a.c504_void_fl IS NULL
				AND a.c504_ship_date IS NULL
				AND b.c205_part_number_id = p_part_id
				AND a.c5040_plant_id = v_plant_id;
	--	
	ELSIF p_opt = 'PALLOC'
	THEN
		OPEN p_recordset
		 FOR
			 SELECT a.c504_consignment_id refid, get_user_name (a.c504_ship_to_id) refname
				  , get_code_name (a.c504_type) reftype, get_code_name (a.c504_inhouse_purpose) refreason
				  , get_user_name (a.c504_created_by) cuser, a.c504_created_date cdate
				  , b.c505_item_qty qty
			   FROM t504_consignment a, t505_item_consignment b
			  WHERE a.c504_consignment_id = b.c504_consignment_id
				AND a.c504_status_fl < 4
				AND a.c207_set_id IS NULL
				AND a.c504_type IN (4114,4116)
				AND a.c504_void_fl IS NULL
				AND a.c504_ship_date IS NULL
				AND b.c205_part_number_id = p_part_id
				AND a.c5040_plant_id = v_plant_id;
	--
	ELSIF p_opt = 'WIPSET'
	THEN
		OPEN p_recordset
		 FOR
			 SELECT   a.c504_consignment_id refid, b.c505_control_number refname, a.c207_set_id reftype, get_set_name(a.c207_set_id) refreason
					, get_user_name (a.c504_created_by) cuser, a.c504_created_date cdate
					, SUM (b.c505_item_qty) qty
				 FROM t504_consignment a, t505_item_consignment b
				WHERE a.c504_consignment_id = b.c504_consignment_id
				  AND a.c504_status_fl < 3
				  AND a.c207_set_id IS NOT NULL
				  AND a.c504_void_fl IS NULL
				  AND a.c504_verify_fl = '0'
				  AND TRIM (b.c505_control_number) IS NOT NULL
				  AND TRIM (b.c505_control_number) <> 'TBE'
				  AND b.c205_part_number_id = p_part_id
				  AND a.c5040_plant_id = v_plant_id
			 GROUP BY a.c504_consignment_id, b.c505_control_number, a.c504_created_by, a.c504_created_date,a.c207_set_id;
--
	ELSIF p_opt = 'TBESET'
	THEN
		OPEN p_recordset
		 FOR
			 SELECT   a.c504_consignment_id refid, '' refname, a.c207_set_id reftype, get_set_name(a.c207_set_id) refreason
					, get_user_name (a.c504_created_by) cuser, a.c504_created_date cdate
					, SUM (b.c505_item_qty) qty
				 FROM t504_consignment a, t505_item_consignment b
				WHERE a.c504_consignment_id = b.c504_consignment_id
				  AND a.c504_status_fl < 3
				  AND a.c207_set_id IS NOT NULL
				  AND a.c504_void_fl IS NULL
				  AND a.c504_verify_fl = '0'
				  AND TRIM (b.c505_control_number) IS NOT NULL
				  AND TRIM (b.c505_control_number) = 'TBE'
				  AND b.c205_part_number_id = p_part_id
				  AND a.c5040_plant_id = v_plant_id
			 GROUP BY a.c504_consignment_id, a.c504_created_by, a.c504_created_date,a.c207_set_id;
--
	ELSIF p_opt = 'OPEREQ'
	THEN
		OPEN p_recordset
		 FOR
			 SELECT   t520.c520_request_id refid, '' || t520.c520_required_date refname
					, t521.c521_qty qty, get_code_name (t520.c901_request_source) refreason
					, gm_pkg_op_request_summary.get_request_status (c520_status_fl) reftype
					, t520.c520_created_date cdate, get_user_name (t520.c520_created_by) cuser
					, t520.c520_required_date rqdt
				 FROM t520_request t520, t521_request_detail t521
				WHERE t520.c520_request_id = t521.c520_request_id
				  AND t520.c520_void_fl IS NULL
				  AND t520.c520_delete_fl IS NULL
				  AND t520.c520_status_fl > 0
				  AND t520.c520_status_fl < 40
				  AND t520.c901_request_source <> 50617
				  AND t521.c205_part_number_id = p_part_id
				  AND t520.c5040_plant_id = v_plant_id
			 UNION ALL
			 SELECT   t520.c520_request_id refid
					, t504.c504_consignment_id || ' - ' || t520.c520_required_date refname
					, t505.c505_item_qty qty, get_code_name (t520.c901_request_source) refreason
					, gm_pkg_op_request_summary.get_request_status (c520_status_fl) reftype
					, t520.c520_created_date cdate, get_user_name (t520.c520_created_by) cuser
					, t520.c520_required_date rqdt
				 FROM t520_request t520, t504_consignment t504, t505_item_consignment t505
				WHERE t520.c520_request_id = t504.c520_request_id
				  AND t504.c504_consignment_id = t505.c504_consignment_id
				  AND t520.c520_void_fl IS NULL
				  AND t520.c520_delete_fl IS NULL
				  AND t520.c520_status_fl > 0
				  AND t520.c520_status_fl < 40
				  AND t520.c901_request_source <> 50617
				  AND t505.c205_part_number_id = p_part_id
				  AND t520.c5040_plant_id = v_plant_id
				  AND t520.c5040_plant_id = t504.c5040_plant_id
			 ORDER BY rqdt;
	-- Raw Material Allocated Out 
	ELSIF p_opt = 'RMALLOC'
	THEN
		OPEN p_recordset
		 FOR
			SELECT t412.c412_inhouse_trans_id refid,
			       get_user_name (t412.c412_release_to_id) refname,
			       get_code_name (t412.c412_type) reftype,
			       get_code_name (t412.c412_inhouse_purpose) refreason,
			       get_user_name (t412.c412_created_by) cuser,
			       t412.c412_created_date cdate,
			       t413.c413_item_qty qty
			  FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
			 WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
			   AND t412.c412_verified_date IS NULL
			   AND T412.C412_VOID_FL IS NULL
			   AND t412.c412_type IN (select c906_rule_value from t906_rules where C906_RULE_ID='RAWMAT' and C906_RULE_DESC='40051' and  C906_RULE_GRP_ID='V205C_ALLOCATED_QTY' and c906_void_fl is null)
			   AND t413.c205_part_number_id = p_part_id
			   AND t412.c5040_plant_id = v_plant_id
			UNION ALL
			SELECT t303.c303_material_request_id refid, t408.c408_control_number refname,
			       t303.c408_dhr_id reftype, get_code_name (t303.c901_type) refreason,
			       get_user_name (t303.c303_created_by) cuser,
			       t303.c303_created_date cdate,
			       t304.c304_item_qty qty
			  FROM t303_material_request t303,
			       t304_material_request_item t304,
			       t408_dhr t408
			 WHERE t303.c303_material_request_id = t304.c303_material_request_id
			   AND t303.c408_dhr_id = t408.c408_dhr_id
			   AND t303.c303_status_fl < 40
			   AND t303.c303_void_fl IS NULL
			   AND t304.c205_part_number_id = p_part_id
			   AND t408.c5040_plant_id = v_plant_id;

	ELSIF p_opt = 'BUILTSET'
	THEN
		OPEN p_recordset
		 FOR
			 SELECT a.c504_consignment_id refid
				  , DECODE (get_distributor_name (a.c701_distributor_id)
						  , '', get_account_name (a.c704_account_id)
						  , get_distributor_name (a.c701_distributor_id)
						   ) refname,
						    a.c207_set_id reftype, get_set_name(a.c207_set_id) refreason
				  , get_user_name (a.c504_created_by) cuser, a.c504_created_date cdate
				  , b.c505_item_qty qty
			   FROM t504_consignment a, t505_item_consignment b
			  WHERE a.c504_consignment_id = b.c504_consignment_id
				AND a.c504_status_fl IN  (1.20, 2, 2.20, 3)			-- built set should include 'pending putaway 1.20' and 'pending pick 2.20'
				AND a.c207_set_id IS NOT NULL
				AND a.c504_verify_fl = '1'
				AND a.c504_ship_date IS NULL
				AND a.c504_void_fl IS NULL
				AND TRIM (b.c505_control_number) IS NOT NULL
				AND b.c205_part_number_id = p_part_id
				AND a.c5040_plant_id = v_plant_id;
	-- DHR Allocated Out Transaction 
	ELSIF (p_opt='DHRALLOC')
	THEN
		OPEN p_recordset
		 FOR 
		 SELECT t412.c412_inhouse_trans_id refid,
			      -- get_user_name (t412.c412_release_to_id) refname,
			       t412.c412_ref_id refname,
			       get_code_name (t412.c412_type) reftype,
			       get_code_name (t412.c412_inhouse_purpose) refreason,
			       get_user_name (t412.c412_created_by) cuser,
			       t412.c412_created_date cdate,
			       t413.c413_item_qty qty	
			  FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
			 WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
			   AND t412.c412_verified_date IS NULL
			   AND t412.c412_void_fl IS NULL
			   AND t413.c413_void_fl IS NULL
			    AND T412.C412_STATUS_FL > 0 -- Status > Pending Release
			   AND t412.c412_type IN (select c906_rule_value from t906_rules where C906_RULE_ID='INHOUSE' and C906_RULE_DESC='60010' and  C906_RULE_GRP_ID='V205C_ALLOCATED_QTY' and c906_void_fl is null)
			   AND t413.c205_part_number_id = p_part_id
			   AND t412.c5040_plant_id = v_plant_id;
 -- SHIPPING Allocated Out Transaction
ELSIF (p_opt='SHIPALLOC')
THEN
	OPEN p_recordset
	 FOR 
	 SELECT t412.c412_inhouse_trans_id refid,
		       get_user_name (t412.c412_created_by) refname,
		       get_code_name (t412.c412_type) reftype,
		       get_code_name (t412.c412_inhouse_purpose) refreason,
		       get_user_name (t412.c412_created_by) cuser,
		       t412.c412_created_date cdate,
		       t413.c413_item_qty qty
  FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
  WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
	   AND t412.c412_verified_date IS NULL
	   AND T412.C412_VOID_FL IS NULL
	   AND t412.c412_type IN (select c906_rule_value from t906_rules where C906_RULE_ID='INHOUSE' and C906_RULE_DESC='60011' and  C906_RULE_GRP_ID='V205C_ALLOCATED_QTY' and c906_void_fl is null)
	   AND t413.c205_part_number_id = p_part_id
	   AND t412.c5040_plant_id = v_plant_id;
	ELSIF (p_opt = 'MFGTBR' OR p_opt = 'MFGREL')
	THEN
		OPEN p_recordset
		 FOR
			 SELECT t303.c303_material_request_id refid, t408.c205_part_number_id refname
				  , get_partnum_desc (t408.c205_part_number_id) refreason, t408.c408_control_number reftype
				  , get_user_name (t303.c303_created_by) cuser, t303.c303_created_date cdate
				  , t304.c304_item_qty qty, t304.c205_part_number_id
			   FROM t303_material_request t303, t304_material_request_item t304, t408_dhr t408
			  WHERE t303.c303_material_request_id = t304.c303_material_request_id
				AND t408.c408_dhr_id = t303.c408_dhr_id
				AND t408.c408_status_fl = 0
				AND t408.c408_void_fl IS NULL
				AND t303.c303_void_fl IS NULL
				AND t304.c304_delete_fl IS NULL
				AND t303.c303_status_fl >= DECODE (p_opt, 'MFGTBR',  20, 40) 
				AND t303.c303_status_fl <= DECODE (p_opt, 'MFGTBR',  30, 40) 
				AND t304.c205_part_number_id = p_part_id
				AND t408.c5040_plant_id = v_plant_id;
	ELSIF (p_opt = 'DHRWIP' OR p_opt = 'DHRREC' OR p_opt = 'DHRQCINS' OR p_opt = 'DHRQCR')
	THEN
		OPEN p_recordset
		 FOR
			 SELECT t408.c408_dhr_id refid, t408.c408_control_number refname, '' reftype, '' refreason
				  , get_user_name (t408.c408_created_by) cuser, t408.c408_created_date cdate
				  , DECODE (t408.c408_status_fl
						  , 0, t408.c408_qty_received
						  , (  NVL (t408.c408_shipped_qty, 0)
							 - NVL (t408.c408_qty_rejected, 0)
							 + NVL (t409.c409_closeout_qty, 0)
							)
						   ) qty
			   FROM t408_dhr t408, t409_ncmr t409
			  WHERE t408.c408_dhr_id = t409.c408_dhr_id(+)
				AND t408.c408_status_fl = DECODE (p_opt, 'DHRWIP', 0, 'DHRREC', 1, 'DHRQCINS', 2, 'DHRQCR', 3, 0)
				AND t408.c205_part_number_id = p_part_id
				AND t408.c408_void_fl IS NULL
				AND t409.c409_void_fl IS NULL
				AND t408.c5040_plant_id = v_plant_id;
	ELSIF (p_opt = 'IHALLOC')
	THEN
		OPEN p_recordset
			 FOR
				 SELECT a.c412_inhouse_trans_id refid, get_user_name (a.c412_release_to_id) refname
					  , get_code_name (a.c412_type) reftype, get_code_name (a.c412_inhouse_purpose) refreason
					  , get_user_name(a.c412_created_by) cuser, a.c412_created_date cdate, b.c413_item_qty qty
				   FROM t412_inhouse_transactions a, t413_inhouse_trans_items b
				  WHERE a.c412_inhouse_trans_id = b.c412_inhouse_trans_id
					AND a.c412_verified_date IS NULL
					AND a.c412_void_fl IS NULL
					AND A.C412_STATUS_FL IN ('2','3')
					AND a.c412_type in( select c906_rule_value from t906_rules where C906_RULE_ID='IHWAREHOUSE' and  C906_RULE_GRP_ID='V205C_ALLOCATED_QTY' and c906_void_fl is null ) 
					AND b.c205_part_number_id = p_part_id
					AND a.c5040_plant_id = v_plant_id
				UNION ALL 
				SELECT a.c504_consignment_id refid, get_cs_ship_name(a.c504_ship_to, a.c504_ship_to_id ) refname
					  , get_code_name (a.c504_type) reftype, get_code_name (a.c504_inhouse_purpose) refreason
					  , get_user_name (a.c504_created_by) cuser, a.c504_created_date cdate
					  , b.c505_item_qty qty
				   FROM t504_consignment a, t505_item_consignment b
				  WHERE a.c504_consignment_id = b.c504_consignment_id
					AND a.c504_status_fl < 4
					AND a.c207_set_id IS NULL
					AND a.c504_type IN (select c906_rule_value from t906_rules where C906_RULE_ID='IHWAREHOUSE' and  C906_RULE_GRP_ID='V205C_ALLOCATED_QTY' and c906_void_fl is null) 
					AND a.c504_void_fl IS NULL
					AND a.c504_ship_date IS NULL
					AND b.c205_part_number_id = p_part_id
					AND a.c5040_plant_id = v_plant_id;
	ELSIF (p_opt = 'IHITEM')
	THEN
		OPEN p_recordset
			 FOR
				SELECT t506.c506_rma_id refid,
				       DECODE (t506.c701_distributor_id,
					       NULL, DECODE (t506.c703_sales_rep_id,
							     NULL, get_user_name (t506.c101_user_id),
							     get_rep_name (t506.c703_sales_rep_id)
							    ),
					       get_distributor_name (t506.c701_distributor_id)
					      ) refname,
				       get_code_name (t506.c901_ref_type) reftype, get_code_name (t506.c506_reason) refreason,
				       t507.c507_item_qty qty, get_user_name (t506.c506_created_by) cuser,
				       t506.c506_created_date cdate,
				       t507.c205_part_number_id pnum
				  FROM t506_returns t506, t507_returns_item t507
				 WHERE t507.c205_part_number_id = p_part_id
				   AND t507.c506_rma_id = t506.c506_rma_id
				   AND ( ( t506.c506_status_fl    = 0 AND t507.c507_status_fl = 'Q') 
                         OR (  t506.c506_status_fl    = 1 AND t507.c507_status_fl = 'R') )
				   AND t506.c901_ref_type = '9110'
				   AND t506.c506_void_fl IS NULL
				   AND t506.c5040_plant_id = v_plant_id
				UNION ALL 
				SELECT misrec.c412_inhouse_trans_id refid,
					   DECODE (t506.c701_distributor_id,
							   NULL, DECODE (t506.c703_sales_rep_id,
											 NULL, get_user_name (t506.c101_user_id),
											 get_rep_name (t506.c703_sales_rep_id)
											),
							   get_distributor_name (t506.c701_distributor_id)
							  ) refname,
					   get_code_name (misrec.c412_type) reftype,
					   get_code_name (misrec.c412_inhouse_purpose) refreason, misrec.qty,
					   get_user_name (t506.c506_created_by) cuser,
					   t506.c506_created_date cdate,
					   misrec.c205_part_number_id pnum
				  FROM t506_returns t506,
					   (SELECT   t412.c412_inhouse_trans_id, t412.c412_ref_id, t412.c412_type,
								 t412.c412_inhouse_purpose, t413.c205_part_number_id,
								 SUM (DECODE (t413.c413_status_fl,'Q', c413_item_qty,c413_item_qty * -1)) qty
							FROM t412_inhouse_transactions t412,
								 t413_inhouse_trans_items t413
						   WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
							 AND t412.c412_verified_date IS NULL
							 AND t412.c412_void_fl IS NULL
							 AND t413.c205_part_number_id = p_part_id
							 AND t412.c412_status_fl NOT IN (4)
							 AND t412.c412_type IN (9112)                       --IH Allocated
							 AND t412.c5040_plant_id = v_plant_id
						GROUP BY t412.c412_inhouse_trans_id,
								 t412.c412_ref_id,
								 t412.c412_type,
								 t412.c412_inhouse_purpose,
								 t413.c205_part_number_id) misrec
				 WHERE t506.c506_rma_id = misrec.c412_ref_id AND t506.c5040_plant_id = v_plant_id;
	ELSIF (p_opt = 'RWALLOC')
	THEN
		OPEN p_recordset
			 FOR
				 SELECT a.c412_inhouse_trans_id refid, get_user_name (a.c412_release_to_id) refname
					  , get_code_name (a.c412_type) reftype, get_code_name (a.c412_inhouse_purpose) refreason
					  , get_user_name(a.c412_created_by) cuser, a.c412_created_date cdate, b.c413_item_qty qty
				   FROM t412_inhouse_transactions a, t413_inhouse_trans_items b, t906_rules t906A, t906_rules t906B
				  WHERE a.c412_inhouse_trans_id = b.c412_inhouse_trans_id
					AND a.c412_verified_date IS NULL
					AND a.c412_void_fl IS NULL
					AND a.c412_status_fl in ('2','3')
					AND a.c412_type  = t906B.c906_rule_grp_id                               -- Get In house Table RW transactions
                                                        AND t906A.c906_rule_grp_id=t906B.c906_rule_grp_id
                                                        AND  t906A.c906_rule_id ='MINUS' AND t906A.c906_rule_value = 'QTY_IN_INV'
                                                        AND t906B.c906_rule_id='TRANS_TABLE' AND t906B.c906_rule_value ='IN_HOUSE'  
					AND b.c205_part_number_id = p_part_id
					AND a.c5040_plant_id = v_plant_id
				UNION ALL 
				SELECT a.c504_consignment_id refid					  
					  , DECODE (get_distributor_name (a.c701_distributor_id)
						  , '', get_account_name (a.c704_account_id)
						  , get_distributor_name (a.c701_distributor_id)
						   ) refname
					  , get_code_name (a.c504_type) reftype, get_code_name (a.c504_inhouse_purpose) refreason
					  , get_user_name (a.c504_created_by) cuser, a.c504_created_date cdate
					  , b.c505_item_qty qty
				   FROM t504_consignment a, t505_item_consignment b
				  WHERE a.c504_consignment_id = b.c504_consignment_id
					AND a.c504_status_fl IN ('2', '3')
					AND a.c207_set_id IS NULL
                    AND a.c504_type  IN (4110,4112,111805) 
					AND b.c901_warehouse_type = 56001 -- For RW only
					AND a.c504_void_fl IS NULL
					AND a.c504_ship_date IS NULL
					AND b.c205_part_number_id = p_part_id
					AND a.c5040_plant_id = v_plant_id;
	ELSIF (p_opt = 'INTRANSALLOC')
	THEN
		OPEN p_recordset
			 FOR				     
			      SELECT t412.c412_inhouse_trans_id refid,t412.c412_ref_id refname
		       			 , get_code_name (t412.c412_type) reftype,get_code_name (t412.c412_inhouse_purpose) refreason
		       			 , get_user_name (t412.c412_created_by) cuser,t412.c412_created_date cdate,t413.c413_item_qty qty
  					FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
  				   WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
	   				 AND t412.c412_verified_date IS NULL
	   				 AND t412.c412_void_fl IS NULL
	   				 AND t412.c412_type IN ('26240358','26240359','26240360')
	   				 AND t413.c205_part_number_id = p_part_id
	   				 AND t412.c5040_plant_id = v_plant_id
	   			UNION ALL 
				  SELECT t504.c504_consignment_id refid ,t504.c504_reprocess_id refname
					     , get_code_name (t504.c504_type) reftype, get_code_name (t504.c504_inhouse_purpose) refreason
					     , get_user_name (t504.c504_created_by) cuser, t504.c504_created_date cdate, t505.c505_item_qty qty					     
				    FROM t504_consignment t504, t505_item_consignment t505 , t504e_consignment_in_trans t504e
				   WHERE t504.c504_consignment_id = t505.c504_consignment_id
				     AND t504.c504_reprocess_id = t504e.c504_consignment_id
					 AND t504.c504_status_fl < 4	
					 AND t504.c504_verify_fl = '0'
					 AND t504.c504_void_fl IS NULL
					 AND t505.c505_void_fl IS NULL
					 AND t504e.c504e_void_fl IS NULL					 
					 AND t505.c205_part_number_id = p_part_id
					 AND t504.c5040_plant_id = v_plant_id; 			
					 
   END IF;
--
END gm_op_fch_partinvdetails;
/