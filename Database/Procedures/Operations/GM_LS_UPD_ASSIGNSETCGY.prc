CREATE OR REPLACE PROCEDURE Gm_Ls_Upd_Assignsetcgy
(
  p_consignid IN T504_CONSIGNMENT.c504_consignment_id%TYPE
 ,p_etchid  IN T504A_CONSIGNMENT_LOANER.c504a_etch_id%TYPE
 ,p_purpose  IN T504_CONSIGNMENT.c504_inhouse_purpose%TYPE
)
AS
v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
/***************************************************************************************************
  * This Procedure is to Update the new Etch Id (c504a_etch_id) and the purpose (c504_inhouse_purpose)
  * in T504A_CONSIGNMENT_LOANER and T504_CONSIGNMENT
  * c504_inhouse_purpose is Code Lookup Value of group - CONPU
  * Author : Joe P Kumar
  * Date   : 10/10/06
****************************************************************************************************/
--
BEGIN
SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
--
UPDATE  T504_CONSIGNMENT
SET  c504_inhouse_purpose = p_purpose
,c504_last_updated_date = SYSDATE 
WHERE c504_consignment_id = p_consignid
	  AND c504_void_fl IS NULL;
--
UPDATE  T504A_CONSIGNMENT_LOANER
SET  c504a_etch_id = p_etchid
,c504a_last_updated_date = SYSDATE 
WHERE c504_consignment_id = p_consignid
AND c5040_plant_id = v_plant_id;
--
END Gm_Ls_Upd_Assignsetcgy;
/

