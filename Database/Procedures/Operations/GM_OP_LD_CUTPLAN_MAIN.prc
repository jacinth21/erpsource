--@"C:\Database\Procedures\Operations\GM_OP_LD_CUTPLAN_MAIN.prc"
CREATE OR REPLACE
PROCEDURE GM_OP_LD_CUTPLAN_MAIN
AS
    --
    v_start_time DATE;
    v_pre_sales_count    NUMBER;
    v_pre_ack_ord_count  NUMBER;
    v_post_sales_count   NUMBER;
    v_post_ack_ord_count NUMBER;
    v_load_type          VARCHAR2 (20) := '4000564';
    v_dmd_sheet_id       VARCHAR2 (10) := 'BBA-1';
    --
BEGIN
    --
    v_start_time := current_date;
    --
     SELECT COUNT (1)
       INTO v_pre_sales_count
       FROM V4070_CUTPLAN_SALES
   ORDER BY C205_PART_NUMBER_ID;
   --
     SELECT COUNT (1)
       INTO v_pre_ack_ord_count
       FROM V4071_CUTPLAN_ACKORD
   ORDER BY C205_PART_NUMBER_ID;
	--
	 DELETE
	   FROM T253F_ITEM_ORDER_LOCK T253F
	  WHERE EXISTS
	    (
	         SELECT t253E.C501_ORDER_ID
	           FROM T253E_ORDER_LOCK t253E
	          WHERE t253E.C501_ORDER_ID     = T253F.C501_ORDER_ID
	            AND t253E.c1900_company_id  = 1001
	            AND C250_INVENTORY_LOCK_ID IN
	            (
	                 SELECT C250_INVENTORY_LOCK_ID
	                   FROM T250_INVENTORY_LOCK
	                  WHERE C250_LOCK_DATE = TRUNC (CURRENT_DATE - 1)
	                  AND c901_lock_type = v_load_type
	            )
	    ) ;
     --   
     DELETE
       FROM T253E_ORDER_LOCK
      WHERE C250_INVENTORY_LOCK_ID IN
        (
             SELECT C250_INVENTORY_LOCK_ID
               FROM T250_INVENTORY_LOCK
              WHERE C250_LOCK_DATE = TRUNC (current_date - 1)
              AND c901_lock_type = v_load_type
        )
        AND c1900_company_id = 1001;
    --3001 San Antonio
    --1001 - BBA Company
    gm_pkg_op_ld_inventory.gm_op_ld_demand_main (v_load_type, 1001, 3001) ;
    --
    gm_pkg_op_ld_demand.gm_op_ld_demand_main (v_dmd_sheet_id, '', v_load_type) ;
    COMMIT;
    --
     SELECT COUNT (1)
       INTO v_post_sales_count
       FROM V4070_CUTPLAN_SALES
   ORDER BY C205_PART_NUMBER_ID;
   --
     SELECT COUNT (1)
       INTO v_post_ack_ord_count
       FROM V4071_CUTPLAN_ACKORD
   ORDER BY C205_PART_NUMBER_ID;
   --
    gm_pkg_cm_job.gm_cm_notify_load_info (v_start_time, current_date, 'S', ' * Sales View Before: '||v_pre_sales_count||
    ' * Ack Order Before: '||v_pre_ack_ord_count||' * Sales View After : '||v_post_sales_count||' * Ack Order After : '
    ||v_post_ack_ord_count, ' GM_OP_LD_CUTPLAN_MAIN Job Status') ;
    --
    COMMIT;
EXCEPTION
WHEN OTHERS THEN
    ROLLBACK;
    -- Final log insert statement (Error Message)
    gm_pkg_cm_job.gm_cm_notify_load_info (v_start_time, current_date, 'E', SQLERRM, 'GM_OP_LD_CUTPLAN_MAIN') ;
    COMMIT;
END GM_OP_LD_CUTPLAN_MAIN;
/