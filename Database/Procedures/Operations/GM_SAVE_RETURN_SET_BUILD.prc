CREATE OR REPLACE PROCEDURE GM_SAVE_RETURN_SET_BUILD
(
	p_RAId  	IN	t506_returns.c506_rma_id%TYPE,
	p_str 		IN	VARCHAR2,
	p_RetDate	IN	VARCHAR2,
	p_Flag		IN	VARCHAR2,
	p_UserId	IN	t506_returns.c506_last_updated_by%TYPE,
	p_errmsg	OUT	VARCHAR2
)
AS
--
/********************************************************************************************
 * Description  :This procedure is used for processing Returns
 * Change Details:
 *		 James	Jul10,2006	Changed Formatting according to Standards
		 James	Jul10,2006	To Fix Error when its an Item Return
 *********************************************************************************************/
v_strlen	NUMBER := NVL(LENGTH(p_str),0);
v_string	VARCHAR2(30000) := p_str;
v_PNum	 	VARCHAR2(20);
v_Qty	 	NUMBER;
v_Control	 t507_returns_item.c507_control_number%TYPE;
v_OrgControl t507_returns_item.c507_org_control_number%TYPE;
v_substring	VARCHAR2(1000);
v_msg		VARCHAR2(1000);
e_others	EXCEPTION;
v_price		VARCHAR2(20);
v_verify_date	DATE;
v_fl		VARCHAR2(10);
v_PartQtyOrig	NUMBER;
v_PartQty	NUMBER;
v_PartNum	VARCHAR2(20);
v_DiffInNum	NUMBER;
v_ItemType	NUMBER;
--
v_order_id	t506_returns.c501_order_id%TYPE;
v_reason	t506_returns.c506_reason%TYPE;
v_Items		t507_returns_item%ROWTYPE;
--
e_void_exp  EXCEPTION;  -- Exception for checking if the Void flag is NULL
v_void_fl   VARCHAR2(10);

-- This Cursor gets the Original Qty before SP execution
CURSOR pop_val IS
	SELECT	SUM(c507_item_qty) PQTY,
		c205_part_number_id PNUM,
		c507_org_control_number OCNUM,
		c901_type ITYPE
	FROM	t507_returns_item
	WHERE	c506_rma_id = p_RAId
	GROUP BY c205_part_number_id
		 ,c507_org_control_number
		 ,c901_type;
--
BEGIN
--
-- Validate if the Returns is already voided
	SELECT C506_VOID_FL
	INTO	v_void_fl
	FROM   T506_RETURNS
	WHERE  c506_rma_id = p_RAId;
	
	IF v_void_fl IS NOT NULL
	THEN
	-- Error message is Returns already Voided. 
	RAISE e_void_exp;
	END IF;

	OPEN pop_val;
	SELECT	c207_set_id
		,c501_order_id
	INTO	v_fl
		,v_order_id
	FROM	t506_returns
	WHERE	c506_rma_id = p_RAId;
	--
 	IF v_strlen > 0
	THEN
	--
	DELETE t507_returns_item WHERE c506_rma_id = p_raid;
	--
   	WHILE INSTR(v_string,'|') <> 0
		LOOP
		--
			v_substring	:= SUBSTR(v_string,1,INSTR(v_string,'|')-1);
			v_string	:= SUBSTR(v_string,INSTR(v_string,'|')+1);
			v_PNum		:= NULL;
			v_Qty  		:= NULL;
			v_Control	:= NULL;
			v_OrgControl	:= NULL;
			v_ItemType	:= NULL;
			v_PNum		:= SUBSTR(v_substring,1,INSTR(v_substring,',')-1);
			v_substring	:= SUBSTR(v_substring,INSTR(v_substring,',')+1);
			v_Qty		:= SUBSTR(v_substring,1,INSTR(v_substring,',')-1);
			v_substring	:= SUBSTR(v_substring,INSTR(v_substring,',')+1);
			v_Control	:= SUBSTR(v_substring,1,INSTR(v_substring,',')-1);
			v_substring	:= SUBSTR(v_substring,INSTR(v_substring,',')+1);
			v_OrgControl	:= SUBSTR(v_substring,1,INSTR(v_substring,',')-1);
			v_substring	:= SUBSTR(v_substring,INSTR(v_substring,',')+1);
			v_ItemType	:= TO_NUMBER(v_substring);
		--
			-- If Consignment get the Equity Value
			-- or get the order value
			IF (v_order_id IS NULL) THEN
				v_price := get_part_price(v_PNum,'E');
			ELSE
				SELECT	c502_item_price
				INTO	v_price
				FROM	t502_item_order
				WHERE	c501_order_id		= v_order_id
				AND	c205_part_number_id	= v_PNum
				AND	c502_control_number	= v_OrgControl;
			END IF;
			--
			INSERT INTO t507_returns_item
			(
				c507_returns_item_id
				,c506_rma_id
				,c205_part_number_id
				,c507_control_number
				,c507_item_qty
				,c507_item_price
				,c507_status_fl
				,c507_org_control_number
				,c901_type
			)VALUES(
				s507_return_item.nextval
				,p_RAId
				,v_PNum
				,v_Control
				,v_Qty
				,v_price
				,'R'
				,NVL(v_OrgControl,'-')
				,v_ItemType);
			--
			-- To generate Q record 
			INSERT INTO t507_returns_item
			(
				c507_returns_item_id
				,c506_rma_id
				,c205_part_number_id
				,c507_control_number
				,c507_item_qty
				,c507_item_price
				,c507_status_fl
				,c507_org_control_number
				,c901_type
			)VALUES(
				s507_return_item.nextval
				,p_RAId
				,v_PNum
				,v_Control
				,v_Qty
				,v_price
				,'Q'
				,NVL(v_OrgControl,'-')
				,v_ItemType);				
		END LOOP;
	--
		UPDATE	t506_returns
		SET	c506_return_date = to_date(p_RetDate,'mm/dd/yyyy'),
			c506_last_updated_by	= p_UserId,
			c506_last_updated_date	= SYSDATE
		WHERE	c506_rma_id = p_RAId;
		--
		-- If the uses checks the 'completed' checkbox, then the following code is executed
		IF p_Flag = '1'	THEN
		--
			UPDATE	t506_returns
			SET	c506_status_fl		= 1,
				c506_last_updated_by	= p_UserId,
				c506_last_updated_date	= SYSDATE
			WHERE	c506_rma_id		= p_RAId;
			--
			IF v_fl IS NOT NULL THEN
			--
				DELETE	t507_returns_item
				WHERE	c506_rma_id = p_RAId
				AND	c507_control_number = ' ';
			--
			END IF;
			--
			SELECT	c501_order_id
				,c506_reason
			INTO	v_order_id
				,v_reason
			FROM	t506_returns
			WHERE	c506_rma_id = p_raid;

			--
			-- Changed the status to Payment Received
			-- This is applicable only if its a duplicate order
			-- 3312 maps to Duplicate order
			IF ( (v_order_id IS NOT NULL) AND (v_reason = 3312) )
			THEN
				UPDATE	t501_order
				SET	c501_status_fl = '3'
				 ,  c501_last_updated_by = p_UserId
		         ,  c501_last_updated_date = SYSDATE
				WHERE	c501_order_id = v_order_id;
			END IF ;
		END IF;
	ELSE
		v_msg := 'p_str is empty';
	END IF;
	--
    BEGIN
		IF v_fl IS NOT NULL
		THEN
			SELECT SYSDATE INTO v_verify_date FROM DUAL;
		ELSE
			-- Added this code on July10 - Have to check later. 
			IF p_Flag = '1'	THEN
				DELETE	t507_returns_item
				WHERE	c506_rma_id = p_RAId
				AND	trim(c507_control_number) is null;
			--
			END IF;
			--
			LOOP
			FETCH pop_val INTO v_PartQtyOrig,v_PartNum,v_OrgControl,v_ItemType;
			EXIT WHEN pop_val%NOTFOUND;
			--
			   IF (v_order_id IS NULL) THEN
				SELECT	nvl(SUM(c507_item_qty),0)
				INTO	v_PartQty
				FROM	t507_returns_item
				WHERE	c506_rma_id		= p_RAId
				AND	c205_part_number_id	= v_PartNum
				AND	c507_org_control_number = v_OrgControl;
			  ELSE
				SELECT	nvl(SUM(c507_item_qty),0)
				INTO	v_PartQty
				FROM	t507_returns_item
				WHERE	c506_rma_id		= p_RAId
				AND	c205_part_number_id	= v_PartNum
				AND	c507_org_control_number = v_OrgControl
				AND	c901_type		= v_ItemType;
			  END IF;
				--
				v_DiffInNum :=  v_PartQtyOrig - v_PartQty;
				--
				IF  v_DiffInNum > 0 THEN
				--
					-- If Consignment get the Equity Value
					-- or get the order value
					IF (v_order_id IS NULL) THEN
					   v_price := get_part_price(v_PNum,'E');
					ELSE
					   SELECT	c502_item_price
					   INTO		v_price
					   FROM		t502_item_order
					   WHERE	c501_order_id		= v_order_id
					   AND		c205_part_number_id	= v_PNum
					   AND		c502_control_number	= v_OrgControl
					   AND		c901_type		= v_ItemType;
					END IF;
					--
					INSERT INTO t507_returns_item (
						c507_returns_item_id
						,c506_rma_id
					  	,c205_part_number_id
						,c507_control_number
					  	,c507_item_qty
						,c507_item_price
						,c507_status_fl
						,c507_org_control_number
						,c901_type
					)VALUES(
						s507_return_item.nextval
						,p_RAId
						,v_PartNum
						,''
						,v_DiffInNum
						,v_price
						,'M'
						,v_OrgControl
						,v_ItemType
					);
				END IF;
			--
			END LOOP;
			CLOSE pop_val;
		END IF;
	END;
	v_msg := SQLERRM;
	--v_msg := 'Success';
    p_errmsg  := v_msg;
EXCEPTION
WHEN e_void_exp THEN
-- Raising Application error with msg "Consignment already Voided."
	 RAISE_APPLICATION_ERROR( -20021, '' );	
--
END GM_SAVE_RETURN_SET_BUILD;
/

