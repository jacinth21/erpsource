CREATE OR REPLACE PROCEDURE GLOBUS_APP.gm_update_bulk_qty(
	p_c504_consignment_id IN t504_consignment.c504_consignment_id%type
	,p_action 	IN t205_part_number.c901_action%type
	,p_type 		IN t205_part_number.c901_type%type
)
AS



CURSOR allocate_qty
IS
   SELECT c205_part_number_id partnumberid, sum(c505_item_qty) itemqty,c504_consignment_id cnid
   FROM t505_item_consignment  WHERE
   c504_consignment_id IN(SELECT token FROM v_in_list)
   GROUP BY c205_part_number_id ,c505_item_qty,c504_consignment_id;

   BEGIN
	 BEGIN
		my_context.set_my_inlist_ctx (p_c504_consignment_id);
   		FOR allocate_curr IN allocate_qty
       	LOOP
              
    IF p_action = '4302' THEN
        gm_cm_sav_partqty (allocate_curr.partnumberid,
                               (allocate_curr.itemqty * -1),
                               allocate_curr.cnid,
                               '30301',
                               90814, -- bulk  Qty
                               p_action, 
                               p_type 
                               );
      END IF;

    	END LOOP;
END;
END gm_update_bulk_qty;
/

