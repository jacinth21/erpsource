/* Formatted on 2011/12/19 14:48 (Formatter Plus v4.8.0) */
--@"C:\database\Procedures\Operations\gm_update_pendship_ord.prc";

CREATE OR REPLACE PROCEDURE gm_update_pendship_ord (
   p_refid      IN       t412_inhouse_transactions.c412_ref_id%TYPE,
   p_purpose    IN       t412_inhouse_transactions.c412_inhouse_purpose%TYPE,
   p_userid     IN       t412_inhouse_transactions.c412_last_updated_by%TYPE,
   p_trans_id   IN       t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
   p_msg        OUT      VARCHAR2
)
AS
   v_itemordid   t502_item_order.c502_item_order_id%TYPE;
   v_org_qty     t502_item_order.c502_item_qty%TYPE;
   v_pnum        VARCHAR2 (20);
   v_qty         NUMBER;
   v_control     t502_item_order.c502_control_number%TYPE;
   v_price       NUMBER (15, 2);
   v_total       NUMBER (15, 2);
   v_substring   VARCHAR2 (1000);
   v_loop_cnt    NUMBER;
   v_bo_cnt      NUMBER;
   v_len         NUMBER;
   v_lastbo      NUMBER;
   v_pnum_str    VARCHAR2 (4000);
   v_boid_str    VARCHAR2 (4000);
   v_out_boid    t501_order.c501_order_id%TYPE;
   v_attr_id     NUMBER;
   v_case_id     NUMBER;
   v_ref_id		t413_inhouse_trans_items.c413_ref_id%TYPE;
   v_order_info_id t500_order_info.c500_order_info_id%TYPE;
   v_order_usage_fl VARCHAR2 (10);
   v_pdt_family NUMBER;
   CURSOR cur_t413
   IS
      SELECT t413.c205_part_number_id pnum, t413.c413_item_qty qty,
             t413.c413_control_number control, t413.c413_item_price price, t413.c413_ref_id refid
        FROM t413_inhouse_trans_items t413, t412_inhouse_transactions t412
       WHERE t413.c412_inhouse_trans_id = p_trans_id
         AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
         AND t412.c412_void_fl IS NULL;
BEGIN
   v_loop_cnt := 0;
   v_bo_cnt := 0;
   v_lastbo := 0;

       SELECT COUNT (*)
       INTO v_len
       FROM t502_item_order t502
       WHERE t502.c501_order_id = p_refid AND t502.c502_void_fl IS NULL;
	-- to get the order usage data flag
	   SELECT NVL (get_rule_value_by_company (c1900_company_id, 'ORDER_USAGE', c1900_company_id), 'N')
	   INTO v_order_usage_fl
	   FROM t501_order
	   WHERE c501_order_id = p_refid;
	 
	--    
   FOR var_cur IN cur_t413
   LOOP
      v_loop_cnt := v_loop_cnt + 1;
      v_pnum := var_cur.pnum;
      v_qty := var_cur.qty;
      v_control := var_cur.control;
      v_price := var_cur.price;
      v_ref_id := var_cur.refid;
      
      --PMT-32396 Back Order email
      --Get the product family inside the for loop 
	  BEGIN
		  SELECT c205_product_family INTO v_pdt_family
		  FROM t205_part_number
		  WHERE c205_part_number_id = v_pnum;
	  EXCEPTION WHEN NO_DATA_FOUND
	  THEN
		v_pdt_family := NULL;
	  END;
        
      SELECT t502.c502_item_order_id, t502.c502_item_qty, c502_attribute_id, c500_order_info_id
      INTO v_itemordid, v_org_qty, v_attr_id, v_order_info_id
      FROM t502_item_order t502
      WHERE t502.c501_order_id = p_refid
	  AND t502.c502_item_order_id = DECODE(v_ref_id, NULL, t502.c502_item_order_id,v_ref_id)
	  AND t502.c502_control_number = v_control
	  AND t502.c502_item_price = v_price
	  AND t502.c205_part_number_id = v_pnum
	  AND t502.c502_void_fl IS NULL;

      IF v_org_qty = v_qty AND p_purpose = 100409
      THEN
         v_bo_cnt := v_bo_cnt + 1;
      END IF;
      
      --PMT-32396 Back Order email, Back order should not be generated for usage code parts
      IF v_bo_cnt = v_loop_cnt AND v_len = v_loop_cnt and v_pdt_family != '26240096' 
      THEN
         UPDATE t501_order
            SET c501_status_fl = '0',
                c501_last_updated_by = p_userid,
                c501_last_updated_date = SYSDATE,
                c901_order_type = 2525
          WHERE c501_order_id = p_refid;
          
          --PMT-47806 Reverted backorders not listed parts while control
          UPDATE t5055_control_number
          SET c5055_void_fl = 'Y',
              c5055_last_updated_by = p_userid,
              c5055_last_updated_date = SYSDATE
          WHERE c5055_ref_id = p_refid
          AND c5055_void_fl IS NULL;
           
          --PMT-47806 Reverted backorders not listed parts while control

         --Whenever we update parent order type to backorder. We need to remove the order id from the shipping dashboard.
         -- i.e, we change the shipping record status (Shipping) back to â€œInitiatedâ€�.
         gm_pkg_cm_shipping_trans.gm_sav_remove_shipping (p_refid,
                                                          50180,
                                                          p_userid,
                                                          ''
                                                         );
         v_lastbo := 1;
      ELSIF v_org_qty = v_qty AND v_qty IS NOT NULL
      THEN
         v_pnum_str := v_pnum_str || v_pnum || ',';

         DELETE FROM t502_item_order
               WHERE c502_item_order_id = v_itemordid;
         -- when entered back order qty then - remove the usage table (t502B)
         IF v_order_usage_fl = 'Y'
         THEN
         	gm_pkg_cs_usage_lot_number.gm_upd_backorder_qty (v_order_info_id, v_qty, p_userid);
         END IF;	
      ELSIF v_org_qty > v_qty AND v_qty IS NOT NULL
      THEN
         v_pnum_str := v_pnum_str || v_pnum || ',';

         UPDATE t502_item_order
            SET c502_item_qty = c502_item_qty - NVL (v_qty, 0)
          WHERE c502_item_order_id = v_itemordid;
          -- when entered back order qty then - remove the usage table (t502B)
          IF v_order_usage_fl = 'Y'
          THEN
          	gm_pkg_cs_usage_lot_number.gm_upd_backorder_qty (v_order_info_id, v_qty, p_userid);
          END IF;	
      ELSIF v_org_qty < v_qty
      THEN
         raise_application_error ('-20796', '');
      END IF;
      
      --PMT-32396 Back Order email, Back order should not be generated for usage code parts
      IF p_purpose = 100409 AND v_lastbo <> 1 and v_pdt_family != '26240096' 
      THEN
         gm_save_item_ord_backorder (p_refid,
                                     v_pnum,
                                     v_qty,
                                     v_price,
                                     NULL,
                                     NULL,
                                     v_out_boid
                                    );
		-- to create the new records to order info and usage table. 
		IF v_order_usage_fl = 'Y'
        THEN                                   
			gm_pkg_cs_usage_lot_number.gm_sav_order_info (v_out_boid, 'Y', p_userid) ;
		END IF;	
		--
         v_boid_str := v_boid_str || v_out_boid || ',';

         -- update the attribute id for the newly created order
         IF v_attr_id IS NOT NULL
         THEN
            UPDATE t502_item_order t502
               SET c502_attribute_id = v_attr_id
             WHERE t502.c501_order_id = v_out_boid;
         END IF;
      END IF;
   END LOOP;

   IF v_pnum_str IS NOT NULL AND p_purpose != 100409
   THEN
      v_pnum_str := SUBSTR (v_pnum_str, 1, LENGTH (v_pnum_str) - 1);
      gm_pkg_common_cancel_op.gm_cm_sav_cancelrow
                           (p_refid,
                            100405,
                            'VPTDO',
                               'The Part numbers that has been removed are '
                            || v_pnum_str,
                            p_userid
                           );
      p_msg :=
          'The parts have been removed for the current DO are: ' || v_pnum_str;

      --Discount order for only remove qty case not for the BO...
      BEGIN
         SELECT t501.c7100_case_info_id
           INTO v_case_id
           FROM t501_order t501
          WHERE t501.c501_order_id = p_refid;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_case_id := '';
      END;

      IF v_case_id IS NOT NULL
      THEN
         my_context.set_my_inlist_ctx (v_pnum_str);

         -- void the attribute records for all the parts that are removed from the order
         UPDATE t502a_item_order_attribute t502a
            SET t502a.c502a_void_fl = 'Y',
                t502a.c502a_last_updated_by = p_userid,
                t502a.c502a_last_updated_date = SYSDATE
          WHERE t502a.c502_attribute_id IN (
                   SELECT t502.c502_attribute_id
                     FROM t502_item_order t502
                    WHERE t502.c501_order_id = p_refid
                      AND t502.c205_part_number_id IN (
                                                       SELECT token
                                                         FROM v_in_list
                                                        WHERE token IS NOT NULL)
                      AND t502.c502_void_fl IS NULL);

         -- viod the tags  for all the parts that are removed from the order
         UPDATE t5012_tag_usage t5012
            SET t5012.c5012_void_fl = 'Y',
                c5012_last_updated_by = p_userid,
                c5012_last_updated_date = SYSDATE
          WHERE c5012_ref_id = v_case_id
            AND c901_ref_type = 2570
            AND c205_part_number_id IN (SELECT token
                                          FROM v_in_list
                                         WHERE token IS NOT NULL)
            AND t5012.c5012_void_fl IS NULL;
      END IF;
   --
   END IF;

   IF p_purpose = 100409
   THEN
      v_boid_str := SUBSTR (v_boid_str, 1, LENGTH (v_boid_str) - 1);
      p_msg :=
            'The following transaction(s) have been generated for the backordered part(s): '
         || v_boid_str;
   END IF;

	-- PMT-28394 (To avoid the DS order creation - Job will create the DS order)
		
	-- update the rebate process flag - So, that job will pick the order and calculate discount

    gm_pkg_ac_order_rebate_txn.gm_update_rebate_fl (p_refid, NULL, NULL);
	gm_pkg_ac_order_rebate_txn.gm_void_discount_order (p_refid, p_userid);
   
   --Updating the order table with the required cost update.
   SELECT SUM (NVL (TO_NUMBER (c502_item_qty) * TO_NUMBER (c502_item_price),
                    0))
     INTO v_total
     FROM t502_item_order
    WHERE c501_order_id = p_refid AND C502_VOID_FL IS NULL;

   UPDATE t501_order
      SET c501_total_cost = NVL (v_total, 0),
          c501_last_updated_by = p_userid,
          c501_last_updated_date = SYSDATE
    WHERE c501_order_id = p_refid;

   gm_pkg_op_item_control_txn.gm_sav_psfg_parent_ord_detail (p_refid,
                                                             p_trans_id,
                                                             p_userid
                                                            );
--
END gm_update_pendship_ord;
/
