/* Formatted on 2011/04/21 16:36 (Formatter Plus v4.8.0) */
--@"C:\database\Procedures\Operations\GM_UPDATE_REPROCESS_MATERIAL.prc";

CREATE OR REPLACE PROCEDURE gm_update_reprocess_material (
	p_consignid    IN		t504_consignment.c504_consignment_id%TYPE
  , p_comments	   IN		t504_consignment.c504_final_comments%TYPE
  , p_type		   IN		t504_consignment.c504_type%TYPE
  , p_verifiedby   IN		t504_consignment.c504_verified_by%TYPE
  , p_userid	   IN		t504_consignment.c504_last_updated_by%TYPE
  , p_message	   OUT		VARCHAR2
)
AS
--
	v_fl		   CHAR (1);
	v_post_id	   VARCHAR2 (20);
	v_sign_from    NUMBER (1) := 1;
	v_sign_to	   NUMBER (1) := 1;
	v_action	   NUMBER (4) := 4301;
	v_table 	   t906_rules.c906_rule_value%TYPE;
	v_ref_id	   t412_inhouse_transactions.c412_ref_id%TYPE;
	v_sign		   NUMBER (1) := 1;
	v_status_fl	   VARCHAR2(5);
	v_po_type      NUMBER;
	v_rework_id    t906_rules.c906_rule_value%TYPE;
	v_type         t504_consignment.c504_type%TYPE;
	v_part_price   VARCHAR2(20);
	v_part         VARCHAR2 (1000);
	v_consign_id   VARCHAR2(20);
	v_company_id        VARCHAR2(20);
	
	CURSOR cur_consign
	IS
		SELECT	 c205_part_number_id ID, SUM (c413_item_qty) qty
			FROM t413_inhouse_trans_items
		   WHERE c412_inhouse_trans_id = p_consignid AND c413_control_number IS NOT NULL
		GROUP BY c205_part_number_id;

--

	--
	CURSOR cur_consignment
	IS
		SELECT	 c205_part_number_id ID, SUM (c505_item_qty) qty
			FROM t505_item_consignment
		   WHERE c504_consignment_id = p_consignid AND c505_control_number IS NOT NULL
		GROUP BY c205_part_number_id;
	--
	CURSOR cur_item_price
	 IS
		 SELECT t412.c412_inhouse_trans_id, t413.C413_TRANS_ID transid, t413.c413_item_price price 
  		, t413.c205_part_number_id pnum, t2052.c2052_equity_price eprice 
   		FROM t413_inhouse_trans_items t413, ( 
         		SELECT c205_part_number_id, c2052_equity_price 
           		FROM t2052_part_price_mapping 
          		WHERE c1900_company_id = v_company_id 
            	AND c2052_void_fl IS NULL 
    	) 
    	t2052, t412_inhouse_transactions t412 
  	WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id 
    AND t412.c412_inhouse_trans_id = p_consignid 
    AND t413.c205_part_number_id = t2052.c205_part_number_id(+) 
    AND t413.c413_void_fl IS NULL 
    AND c413_item_price IS NULL 
    AND t412.c412_void_fl IS NULL 
    AND t412.c1900_company_id = v_company_id; 

BEGIN
--
	BEGIN
	SELECT  c1900_company_id
    	INTO    v_company_id
    	FROM    t412_inhouse_transactions where c412_inhouse_trans_id = p_consignid and C412_VOID_FL is null;
    EXCEPTION
 		WHEN NO_DATA_FOUND
    	THEN
      			v_company_id := NULL;
      	END;
    	
	v_type := p_type;
	
	FOR item_val IN cur_item_price
	    LOOP 

		IF item_val.eprice IS NULL
		THEN
		    v_part := v_part || item_val.pnum || ',';
		ELSE
		    UPDATE t413_inhouse_trans_items
		       SET c413_item_price = item_val.eprice
		      	 , c413_last_updated_by = p_userid
			     , c413_last_updated_date = CURRENT_DATE
		     WHERE c413_trans_id = item_val.transid;
		     
			--Whenever the detail is updated, the inhouse transactions table has to be updated ,so ETL can Sync it to GOP.
			UPDATE t412_inhouse_transactions 
			   SET c412_last_updated_by = p_userid
			     , c412_last_updated_date = CURRENT_DATE
			 WHERE c412_inhouse_trans_id = p_consignid;		     
		END IF;
	    END LOOP;

	    IF v_part IS NOT NULL
	    THEN
		GM_RAISE_APPLICATION_ERROR('-20999','332',v_part||'#$#'||p_consignid);
	    END IF;
            
	SELECT get_rule_value ('TRANS_TABLE', p_type)
	  INTO v_table
	  FROM DUAL;

	gm_pkg_op_item_control_txn.gm_validate_txn_verify(p_consignid, p_type);
	
	
	IF p_type = '4113' OR p_type = '4114' OR p_type = '4115' OR p_type = '4116' OR v_table = 'CONSIGNMENT'
	THEN
		SELECT	   c504_update_inv_fl, c504_status_fl
			  INTO v_fl, v_status_fl
			  FROM t504_consignment
			 WHERE c504_consignment_id = p_consignid AND c504_void_fl IS NULL
		FOR UPDATE;

		IF v_status_fl < 3
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','333','');
		-- ORA20411 = Transaction is not released for verification.
		END IF;
		IF v_fl = 4
		THEN
			raise_application_error (-20936, '');
		-- ORA20062 = Inventory already updated
		END IF;
		
		
	   -- Changes done for Return-In-Hold Changes.
	   -- No need to update t205_part_number here, will be handled in the bottom
	 /* IF (p_type = '4114' ) AND v_fl IS NULL
	  THEN
		 FOR rad_val IN cur_consignment
		 LOOP
			UPDATE t205_part_number
			   SET c205_qty_in_stock = NVL (c205_qty_in_stock, 0) - rad_val.qty,
				   c901_action = v_action,
				   c901_type = p_type,
				   c205_last_update_trans_id = p_consignid,
				   c205_last_updated_by = p_userid
			 WHERE c205_part_number_id = rad_val.ID;

			  UPDATE t205_part_number
			   SET c205_qty_in_packaging = NVL (c205_qty_in_packaging, 0) + rad_val.qty,
				   c901_action = v_action,
				   c901_type = p_type,
				   c205_last_update_trans_id = p_consignid,
				   c205_last_updated_by = p_userid
			 WHERE c205_part_number_id = rad_val.ID;
	 END LOOP;
	 END IF;
	*/
--
 
		UPDATE t504_consignment
		   SET c504_final_comments = p_comments
			 , c504_verified_by = p_verifiedby
			 , c504_verified_date = TRUNC (CURRENT_DATE)
			 , c504_status_fl = '4'
			 , c504_last_updated_date = CURRENT_DATE
			 , c504_last_updated_by = p_userid
			 , c504_update_inv_fl = '1'
			 , c504_verify_fl = '1'
			 -- hardcode update as this transaction only requires verification
		,	   c504_ship_req_fl = NULL
-- Updating to NULL as this is an InHouse trans. Used ShipReqFl for getting it on
		WHERE  c504_consignment_id = p_consignid
												-- Dashboard. After completed trans , no use !!
			   AND c504_void_fl IS NULL;

		IF p_type = '4114' OR p_type = '4116'
		THEN
			gm_pkg_op_inv_scan.gm_check_inv_order_status (p_consignid, 'Completed', p_userid);
		END IF;

--
		IF v_fl IS NULL
		THEN
			IF p_type = '4113'
			THEN
				gm_update_inventory (p_consignid, 'Q', 'PLUS', p_userid, p_message);
			ELSIF p_type = '4115'
			THEN
				gm_update_inventory (p_consignid, 'Q', 'MINUS', p_userid, p_message);
			ELSIF p_type = '4116'
			THEN
				gm_update_inventory (p_consignid, 'Q', 'PLUSMINUS', p_userid, p_message);
			ELSIF p_type = '111800' OR p_type = '111801' OR p_type = '111802' OR p_type = '111803' OR p_type = '111804' OR p_type = '111805' 
			THEN
			--111800 - Quarantine - Inhouse ,111801 - Bulk - Inhouse, 111802 - Finished Goods - Inhouse, 111803 -Repackaging - Inhouse
			--111804 - IH Inventory - Inhouse, 111805- Restricted Warehouse - Inhouse
				gm_update_inventory (p_consignid, 'CITEM', 'MINUS', p_userid, p_message);
			END IF;
		END IF;
--
	ELSIF	 p_type = '4117'
		  OR p_type = '4118'
		  OR p_type = '40051'
		  OR p_type = '40052'
		  OR p_type = '40053'
		  OR p_type = '40054'
		  OR p_type = '100406'
		  OR v_table = 'IN_HOUSE'
	THEN
--
		SELECT	   c412_update_inv_fl, c412_ref_id, c412_status_fl 
			  INTO v_fl, v_ref_id, v_status_fl
			  FROM t412_inhouse_transactions
			 WHERE c412_inhouse_trans_id = p_consignid
		FOR UPDATE;
		
		IF v_status_fl < 3
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','333','');
		-- ORA20411 = Transaction is not released for verification.
		END IF;

		IF v_fl IS NOT NULL
		THEN
			raise_application_error ('-20936', '');
		END IF;

--
		IF p_type = '4117' AND v_fl IS NULL
		THEN
			FOR rad_val IN cur_consign
			LOOP
				
				-- Shelf to Bulk
				v_action:= 4301;
				gm_cm_sav_partqty (rad_val.ID,
                               rad_val.qty,
                               p_consignid,
                               p_userid,
                               90814, -- bulk  Qty
                               v_action,
                               p_type 
                               );
                gm_pkg_op_lot_track.gm_lot_track_main(rad_val.ID,
                               rad_val.qty,
                               p_consignid,
                               p_userid,
                               90814, -- bulk  Qty
                               v_action,
                               p_type
                               );

--
				-- Modified for MNTTASK-576
				 v_action:= 4302;
				 gm_cm_sav_partqty (rad_val.ID,
                               (rad_val.qty * -1),
                               p_consignid,
                               p_userid,
                               90800, -- FG  Qty
                               v_action,
                               p_type 
                               );
                 gm_pkg_op_lot_track.gm_lot_track_main(rad_val.ID,
                               (rad_val.qty * -1),
                               p_consignid,
                               p_userid,
                               90800, -- FG  Qty
                               v_action,
                               p_type
                               );
			END LOOP;
--
		ELSIF p_type = '4118' AND v_fl IS NULL
		THEN
			FOR rad_val IN cur_consign
			LOOP
				-- Bulk to Shelf
				gm_cm_sav_partqty (rad_val.ID,
                               (rad_val.qty * -1),
                               p_consignid,
                               p_userid,
                               90814, -- bulk  Qty
                               v_action,
                               p_type 
                               );

				--
				gm_cm_sav_partqty (rad_val.ID,
                               rad_val.qty,
                               p_consignid,
                               p_userid,
                               90800, -- FG  Qty
                               v_action,
                               p_type 
                               );
			END LOOP;

			gm_pkg_op_inv_scan.gm_check_inv_order_status (p_consignid, 'Completed', p_userid);
		--
		ELSIF	 (p_type = '40051' AND v_fl IS NULL)
			  OR (p_type = '40052' AND v_fl IS NULL)
			  OR (p_type = '40053' AND v_fl IS NULL)
			  OR (p_type = '40054' AND v_fl IS NULL)
		THEN
			v_post_id	:= get_rule_value (p_type, 'RAW-MATERIAL');

			IF p_type = '40051' OR p_type = '40052' OR p_type = '40053'
			THEN
				v_sign_from := -1;
				v_sign_to	:= 1;
				v_action	:= 4302;
			ELSIF p_type = '40054'
			THEN
				v_sign_from := 1;
				v_sign_to	:= -1;
				v_action	:= 4301;
			END IF;

			--
			FOR rad_val IN cur_consign
			LOOP
				gm_cm_sav_partqty (rad_val.ID, rad_val.qty * v_sign_from, p_consignid, p_userid, 90802, 4302, 4322);
				gm_save_ledger_posting (v_post_id, CURRENT_DATE, rad_val.ID, '01', p_consignid, rad_val.qty, 0, p_userid);

				IF p_type = '40052'
				THEN   -- Raw Materials to Quarantine
					gm_cm_sav_partqty (rad_val.ID,
                               (rad_val.qty * v_sign_to),
                               p_consignid,
                               p_userid,
                               90813, -- quarantine  Qty
                               v_action,
                               p_type 
                               );
				ELSIF p_type = '40053' OR p_type = '40054'
				THEN
					-- 40053 Raw Materials to Finished Goods / 40054 FG to Raw Materials
					gm_cm_sav_partqty (rad_val.ID,
                               (rad_val.qty * v_sign_to),
                               p_consignid,
                               p_userid,
                               90800, -- FG  Qty
                               v_action,
                               p_type 
                               );
				END IF;
			END LOOP;
		--
		-- code added for the transactions like Shelf to Built Set, shelf to loaner set and shelf to inhouse set, Shelf to Loaner Extension w/Replenish, In-House Set to Shelf,Loaner Set to Shelf
		ELSIF	 (p_type = '50160' AND v_fl IS NULL)
			  OR (p_type = '50155' AND v_fl IS NULL)
			  OR (p_type = '50150' AND v_fl IS NULL)
			  OR (p_type = '50154' AND v_fl IS NULL)
			  OR (p_type = '50152' AND v_fl IS NULL)
			  OR (p_type = '50157' AND v_fl IS NULL)
		THEN
		
			IF (p_type = '50150') 
			THEN
	    		gm_close_ih_set_back_order(p_consignid, p_type, v_ref_id);	    			    		
			END IF;
		
			-- Updating T505 Item Consignment Table
			SELECT DECODE (p_type, '50150', 1, '50154', 1, '50155', 1, '50160', 1, -1)
			  INTO v_sign
			  FROM DUAL;
			 	IF p_type = '50150' THEN --FGIS transaction need to get CN#
	    			v_consign_id:=gm_pkg_op_ihloaner_item_rpt.get_back_order_consign_id(p_consignid,p_type);
	    			IF v_consign_id IS NULL OR v_consign_id = p_consignid THEN
		  				v_consign_id := v_ref_id;
	 				END IF;
	    		ELSE
	    		v_consign_id:=v_ref_id;
	    		END IF;
			INSERT INTO t505_item_consignment
						(c505_item_consignment_id, c504_consignment_id, c205_part_number_id, c505_control_number
					   , c505_item_qty, c505_item_price, c901_type, c505_ref_id)
				SELECT s504_consign_item.NEXTVAL, v_consign_id, c205_part_number_id, c413_control_number, c413_item_qty * v_sign
					 , c413_item_price, p_type, p_consignid
				  FROM t413_inhouse_trans_items
				 WHERE c412_inhouse_trans_id = p_consignid;
		
		--When ever the child is updated, the parent table has to be updated ,so ETL can Sync it to GOP.		 
		UPDATE t504_consignment
		   SET c504_last_updated_date = CURRENT_DATE
			 , c504_last_updated_by = p_userid
		WHERE  c504_consignment_id = v_consign_id;
		
		END IF;

--
		UPDATE t412_inhouse_transactions
		   SET c412_verify_fl = '1'
			 , c412_status_fl = '4'
			 , c412_update_inv_fl = '1'
			 , c412_verified_by = p_userid
			 , c412_verified_date = CURRENT_DATE
			 , c412_last_updated_date = CURRENT_DATE
			 , c412_last_updated_by = p_userid
		 WHERE c412_inhouse_trans_id = p_consignid;

--
	-- Below code has been added for the FGLN and FGIH transaction to update the status properly on Manage-loaner screen.
		IF p_type IN ('50155', '50150', '50152', '50157')
		THEN
			gm_ls_upd_csg_loanerstatus (4, 50, v_ref_id);
		END IF;

		IF p_type IN ('50154')
		THEN
			--chk if this needs to be called under any condition
			gm_pkg_cm_shipping_trans.gm_sav_release_shipping (p_consignid, 50183, p_userid, '');
		END IF;
	--
	END IF;
	
	IF p_type = '100067' OR p_type = '100069'  THEN
		v_po_type := get_po_type(v_ref_id);

		IF v_po_type = 3101 -- Rework
		THEN
			SELECT get_rule_value ('REWORK_TXN_REMAP', p_type)
			  INTO v_rework_id
			FROM DUAL;
			
			IF v_rework_id IS NOT NULL
			THEN
			    v_type := v_rework_id;
			END IF;
		END IF;
	 END IF;

	IF v_table IS NOT NULL
	THEN
	
	IF v_type = '100067' OR v_type = '100065' OR v_type = '100066' OR v_type = '100068' OR v_type = '100079' OR v_type = '100078' OR v_type = '100082' 
	    OR v_type = '104620' OR v_type = '104621' OR v_type = '104622' THEN 
	-- 100065-DHR to Bulk, 100066-DHR to Repack,100067-DHR to Finished Goods,100068-DHR to Quarantine
	-- 100082-Rework DHR to Repack,100078-Rework DHR to Finished goods,100079-Rework DHR to Bulk
	--104620-RSFG, 104621-RSPN,104622-RSQN
	gm_pkg_op_lot_track.gm_lot_track_main(NULL,
                               NULL,
                               p_consignid,
                               p_userid,
                               90801, -- DHR  
                               4302,-- MINUS
                               50982 -- DHR
                               );
    END IF;
    
		gm_pkg_op_inventory_qty.gm_sav_inventory_main (p_consignid, v_type, p_userid);
	END IF;

	
	BEGIN
     SELECT c412_update_inv_fl
       INTO v_fl
       FROM t412_inhouse_transactions
      WHERE c412_inhouse_trans_id = p_consignid FOR UPDATE;
EXCEPTION
WHEN NO_DATA_FOUND THEN
    v_fl := NULL;
END;
	
IF ((p_type = '50155' or  p_type='50157') AND v_fl='1')THEN

    gm_pkg_op_inv_field_sales_tran.gm_sav_loaner_lot (p_consignid, p_userid) ;
END IF;
END gm_update_reprocess_material;
/
