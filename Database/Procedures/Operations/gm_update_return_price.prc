/* Formatted on 2011/05/19 17:25 (Formatter Plus v4.8.0) */
-- @ "C:\database\procedures\Operations\gm_update_return_price.prc"

CREATE OR REPLACE PROCEDURE gm_update_return_price (
	p_rmaid 	 IN   t507_returns_item.c506_rma_id%TYPE
  , p_party_id	 IN   t101_party.c101_party_id%TYPE
)
AS
--
-- Update the price for ICT Returns

	--
	CURSOR pop_val
	IS
		SELECT b.c507_returns_item_id seqid, get_account_part_pricing (NULL, b.c205_part_number_id, p_party_id) price
		  FROM t506_returns a, t507_returns_item b
		 WHERE a.c506_rma_id = b.c506_rma_id AND a.c506_rma_id = p_rmaid
		 		AND b. c507_status_fl!='M';
--
BEGIN
--
	FOR rad_val IN pop_val
	LOOP
		IF rad_val.price IS NULL
		THEN
			raise_application_error (-20096, '');
		END IF;

		UPDATE t507_returns_item
		   SET c507_item_price = rad_val.price
		 WHERE c507_returns_item_id = rad_val.seqid;
	END LOOP;
	
	--When ever the detail is updated, the return table has to be updated ,so ETL can Sync it to GOP.
 	UPDATE t506_returns
	   SET c506_last_updated_date = Sysdate
     WHERE C506_rma_Id = p_rmaid;
--
END gm_update_return_price;
/
