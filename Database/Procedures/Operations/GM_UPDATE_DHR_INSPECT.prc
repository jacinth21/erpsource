/* Formatted on 2011/08/09 16:44 (Formatter Plus v4.8.0) */
-- @"c:\database\procedures\operations\gm_update_dhr_inspect.prc"
CREATE OR REPLACE
PROCEDURE gm_update_dhr_inspect (
        p_dhrid          IN t408_dhr.c408_dhr_id%TYPE,
        p_qtyinsp        IN t408_dhr.c408_qty_inspected%TYPE,
        p_qtyrej         IN t408_dhr.c408_qty_rejected%TYPE,
        p_inspby         IN t408_dhr.c408_inspected_by%TYPE,
        p_rejreason_type IN t409_ncmr.c901_reason_type%TYPE,
        p_rejreason      IN t409_ncmr.c409_rej_reason%TYPE,
        p_statusfl       IN t408_dhr.c408_status_fl%TYPE,
        p_sterilefl      IN CHAR,
        p_userid         IN t501_order.c501_created_by%TYPE,
        p_updstatusfl    IN t408_dhr.c408_status_fl%TYPE,
        p_udi_chk_fl     IN t408_dhr.c408_udi_verified%TYPE,
        p_ncmrid OUT VARCHAR2,
        p_message OUT VARCHAR2)
AS
    --
    v_labelskipfl  CHAR (1) ;
    v_packskipfl   CHAR (1) ;
    v_chkstatusfl  NUMBER (1) ;
    v_chkdhrvoidfl CHAR (1) ;
    v_potype t401_purchase_order.c401_type%TYPE;
    v_statusfl t408_dhr.c408_status_fl%TYPE;
    v_skippackfl VARCHAR2 (255) ;
     v_ncmr_id	   t409_ncmr.c409_ncmr_id%TYPE;
    v_ncmr_sts_fl  NUMBER;
    v_qty_rejected	 t408_dhr.c408_qty_rejected%TYPE;
    v_qty_closeout	t409_ncmr.c409_closeout_qty%TYPE;
    v_donor_id		t408_dhr.c2540_donor_id%TYPE;
    v_sex			t2540_donor_master.c901_sex%TYPE;
	v_age			t2540_donor_master.c2540_age%TYPE;
	v_intrnuse		t2540_donor_master.c901_international_use%TYPE;
	v_research		t2540_donor_master.c901_research%TYPE;
	v_company_id    t1900_company.c1900_company_id%TYPE;
	v_serial_num_fl	t205d_part_attribute.c205d_attribute_value%TYPE;
    --
BEGIN
	
	--raise_application_error (-20999, 'Entered Proc') ;
	 SELECT NVL(get_compid_frm_cntx(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL'))
	  INTO v_company_id 
	  FROM dual;
	
    -- Check for DHR Void Flag. If the Flag is turned ON, then throw an Application Error
     SELECT t408.c408_void_fl, t401.c401_type, get_rule_value (t402.c205_part_number_id, 'SKIP-OPER-PACK'), t408.c2540_donor_id
     	, get_part_attr_value_by_comp(t408.C205_PART_NUMBER_ID,'106040',v_company_id) 
       INTO v_chkdhrvoidfl, v_potype, v_skippackfl,v_donor_id, v_serial_num_fl
       FROM t408_dhr t408, t402_work_order t402, t401_purchase_order t401
      WHERE t408.c408_dhr_id          = p_dhrid
        AND t408.c402_work_order_id   = t402.c402_work_order_id
        AND t402.c401_purchase_ord_id = t401.c401_purchase_ord_id;
    v_statusfl                       := p_statusfl;
    IF (v_chkdhrvoidfl               IS NOT NULL) THEN
        --
        raise_application_error ( - 20004, '') ;
        --
    END IF;
    
    -- Donor Parameters are mandatory 
    BEGIN
	    SELECT c901_sex, c2540_age, c901_international_use, c901_research
	    	INTO v_sex, v_age, v_intrnuse, v_research
	    FROM T2540_DONOR_MASTER 
	    WHERE C2540_DONOR_ID = v_donor_id
	    	AND c2540_void_fl IS NULL;
	    EXCEPTION
	    WHEN NO_DATA_FOUND THEN
	        v_sex      	:= NULL;
	        v_age  		:= NULL;
	        v_intrnuse 	:= NULL;
	        v_research 	:= NULL;
    END;
    
    IF v_donor_id IS NOT NULL AND (v_sex IS NULL OR v_age IS NULL OR v_intrnuse IS NULL OR v_research IS NULL)
    THEN
    	raise_application_error('-20999','Donor Parameters are missing, Please fill the donor parameters to proceed');
    END IF;
    
    --To check whether control numbers are generated      
   	  IF v_donor_id IS NOT NULL OR v_serial_num_fl = 'Y'
   	  THEN   	  
   	  	gm_pkg_op_dhr_rpt.gm_validate_generated_lotNum(p_dhrid,v_serial_num_fl,v_donor_id);
   	  
   	  END IF;
    
    
    -- End of Check for DHR Void Flag
    IF v_potype = 3100 OR v_potype = 3101 OR v_potype = 3102 -- product/rework/steralize
        THEN
        IF p_sterilefl = '1' OR v_skippackfl = 'Y' THEN
            --
            v_labelskipfl := '1';
            v_packskipfl  := '1';
            v_statusfl    := 2;
            --
        ELSE
            --
            v_labelskipfl := '';
            v_packskipfl  := '';
            --
        END IF;
    END IF;
    --
     SELECT TO_NUMBER (c408_status_fl)
       INTO v_chkstatusfl
       FROM t408_dhr
      WHERE c408_dhr_id = p_dhrid;
    --
    IF v_chkstatusfl > TO_NUMBER (p_updstatusfl) THEN
        --
        raise_application_error ( - 20001, '') ;
        --
    END IF;
    
    BEGIN
         SELECT t409.c409_ncmr_id, TO_NUMBER(t409.c409_status_fl), NVL (t408.c408_qty_rejected, 0)
          , NVL (t409.c409_closeout_qty, 0)
           INTO v_ncmr_id, v_ncmr_sts_fl, v_qty_rejected
          , v_qty_closeout
           FROM t408_dhr t408, t409_ncmr t409
          WHERE t408.c408_dhr_id = t409.c408_dhr_id
            AND t409.c408_dhr_id = p_dhrid
            AND t409.c409_void_fl IS NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_ncmr_id      := NULL;
        v_ncmr_sts_fl  := NULL;
        v_qty_rejected := 0;
        v_qty_closeout := 0;
    END;
    
     
    
    IF v_ncmr_sts_fl = 3 AND v_qty_rejected != p_qtyrej THEN
        raise_application_error ( - 20999, 'NCMR is already closed, cannot change the rejected Qty') ;
    END IF;
     UPDATE t408_dhr
    SET c408_qty_inspected = p_qtyinsp, c408_qty_rejected = p_qtyrej, c408_status_fl = v_statusfl
      , c408_inspected_by  = p_inspby, c408_inspected_ts = SYSDATE, c408_label_skip_fl = v_labelskipfl
      , c408_pack_skip_fl  = v_packskipfl, c408_last_updated_by = p_userid, c408_last_updated_date = SYSDATE , c408_udi_verified = p_udi_chk_fl
      WHERE c408_dhr_id    = p_dhrid;
    --
    IF p_qtyrej <> 0 AND v_ncmr_id IS NULL THEN
        --commented for issue#4379 Create EVAL instead of NCMR
        --gm_create_ncmr (p_dhrid, p_userid, p_rejreason_type, p_rejreason, p_ncmrid, p_message);
        gm_pkg_op_ncmr.gm_create_eval_ncmr (p_dhrid, p_userid, p_rejreason_type, p_rejreason, p_ncmrid, p_message);        
        GM_UPDATE_LOG(p_dhrid,'Qty rejected and'||'<b>'||' EVAL ID: '||'</b>'||p_ncmrid,'1205',p_userid,p_message);
    ELSIF p_qtyrej      <> 0 AND v_ncmr_id IS NOT NULL THEN
        IF v_ncmr_sts_fl = 2 THEN
            -- Rollback the NCMR
            gm_pkg_op_ncmr.gm_rollback_ncmr (v_ncmr_id, p_userid) ;
        END IF;
        IF v_ncmr_sts_fl <= 2 THEN
             UPDATE t409_ncmr
            SET c409_qty_rejected                                                  = p_qtyrej, c409_rej_reason = p_rejreason, c901_reason_type = DECODE (
                p_rejreason_type, 0, NULL, p_rejreason_type), c409_last_updated_by = p_userid, c409_last_updated_date =
                SYSDATE
              WHERE c409_ncmr_id = v_ncmr_id;
        END IF;
    END IF;
END gm_update_dhr_inspect;
/
