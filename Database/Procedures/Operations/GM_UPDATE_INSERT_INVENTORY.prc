/*Formatted on 2010/06/18 20:50 (Formatter Plus v4.8.0) */
-- @"C:\database\Procedures\Operations\gm_update_inventory.prc"

CREATE OR REPLACE PROCEDURE GM_UPDATE_INSERT_INVENTORY (
   p_id           IN       VARCHAR2,
   p_type         IN       VARCHAR2,
   p_action       IN       VARCHAR2,
   p_updated_by   IN       VARCHAR2,
   p_message      OUT      VARCHAR2
)
/**************************************************************
 * Code Lookup values
 * ACTION
 * -----------
 * 4301 - Plus
 * 4302 - Minus
 * TYPE
 * ------------
 * 4310,'Sales'
 * 4311,'Consignment'
 * 4312,'Return'
 * 4313,'Quarantine'
 * 4314,'Purchase'
 * 4315,'Sterilization PO'
 * 4316,'Manual Adjustment'
 * 4317,'InHouse Loaner'
 **************************************************************/
AS
--
   v_stock       NUMBER;
   v_newstock    NUMBER;
   v_party_id    VARCHAR2 (20);
   v_set_id      VARCHAR2 (20);
   v_purpose     VARCHAR2 (20);
   v_type        VARCHAR2 (20);
   v_post_id     VARCHAR2 (20);
   v_dist_id     VARCHAR2 (20);
   v_acc_id      VARCHAR2 (20);
   v_reason      VARCHAR2 (20);
   v_sign        NUMBER (1)    := 1;
   v_action      NUMBER (4)    := 4301;
   v_item_type   NUMBER;
   v_item_qty    NUMBER;
   v_post_fl     NUMBER;
   v_dist_type   NUMBER;
   v_cn_status   t504_consignment.c504_status_fl%TYPE;
   -- International posting changes
   v_ict_post_id  VARCHAR2 (20);
   v_set_type t207_set_master.c207_type%TYPE;
   v_destination_comp_id t1900_company.c1900_company_id%TYPE;
   v_ous_cn_post_id t906_rules.c906_rule_value%TYPE;
   v_international_post_id t906_rules.c906_rule_value%TYPE;
   v_trans_company_id t1900_company.c1900_company_id%TYPE;
   v_trans_plant_id t5040_plant_master.c5040_plant_id%TYPE;
   v_cost_value NUMBER;
   v_cost_err_str VARCHAR2(4000) := NULL;
   v_insert_flag 		VARCHAR2 (10);
   v_reduce_FG_qty_fl	VARCHAR2 (10);
--
   
   CURSOR cur_consign
   IS
       SELECT t5056.c205_insert_id ID,1 qty,NULL price,NULL whtype
         FROM t5056_transaction_insert t5056, t205c_part_qty t205c
        WHERE t5056.c5056_ref_id = p_id
          AND t5056.c205_insert_id = t205c.c205_part_number_id      -- PMT-48520 - Remove Insert Part Number in Shipping Job
          AND t205c.c901_transaction_type = 90800  
          AND t205c.c5040_plant_id =  v_trans_plant_id 
          AND t205c.c205_available_qty > 0
          AND t5056.c5056_void_fl IS NULL;

--
   CURSOR cur_ihl
   IS
       SELECT t5056.c205_insert_id ID,1 qty,NULL price
         FROM t5056_transaction_insert t5056, t205c_part_qty t205c
        WHERE t5056.c5056_ref_id = p_id
          AND t5056.c205_insert_id = t205c.c205_part_number_id     -- PMT-48520 - Remove Insert Part Number in Shipping Job
          AND t205c.c901_transaction_type = 90800  
          AND t205c.c5040_plant_id =  v_trans_plant_id 
          AND t205c.c205_available_qty > 0
          AND t5056.c5056_void_fl IS NULL;
          
--
BEGIN
--
	-- to get the transactions company id and plant id
	gm_pkg_common.gm_fch_trans_comp_plant_id(p_id, v_trans_company_id, v_trans_plant_id);
	--
     IF v_trans_company_id IS NOT NULL AND v_trans_plant_id IS NOT NULL THEN             
     	gm_pkg_cor_client_context.gm_sav_client_context(v_trans_company_id,v_trans_plant_id);
     END IF;
        
      --Validating the Plant is Enabled for INSERT
   	   SELECT get_rule_value(v_trans_plant_id,'INSERT_PART') INTO v_insert_flag FROM DUAL; 
	   
	    --Validating the Plant is Enabled for reduce FG qty
   	   SELECT get_rule_value(v_trans_plant_id,'INSERT_REDUCE_FG') 
   	   	INTO v_reduce_FG_qty_fl 
   	   FROM DUAL; 
	   
  IF v_insert_flag = 'Y' AND v_reduce_FG_qty_fl = 'Y' THEN
	   
   -- To get Party Id for Ledger Posting -- Added Feb 9
   SELECT get_acct_party_id (p_id, p_type)
     INTO v_party_id
     FROM DUAL;

   -- To get details of Consignment
   --
   BEGIN
      SELECT t504.c207_set_id, c504_inhouse_purpose, t504.c504_type,
             c701_distributor_id, get_distributor_type (t504.c701_distributor_id) , t504.c704_account_id,
             DECODE(get_rule_value(t504.c504_type,'DEMO_CN_TYPE'),'Y',
             DECODE(t520.c520_request_for,40021,t207.c207_type,NULL),NULL)
        INTO v_set_id, v_purpose, v_type,
             v_dist_id, v_dist_type , v_acc_id,
             v_set_type
        FROM t504_consignment t504, t207_set_master t207, t520_request t520
       WHERE c504_consignment_id = p_id 
         AND c504_void_fl IS NULL
         AND t504.c207_set_id =  t207.c207_set_id(+)
  	   	 AND t504.c520_request_id = t520.c520_request_id(+)
       	 AND t520.c520_void_fl(+) IS NULL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         NULL;
   END;

   --
   -- For SALES processing
   IF p_type = 'S'
   THEN
      --
      gm_update_insert_inv_sales (p_id, p_type, p_action, p_updated_by);
      --
   -- For All Consignments :
   -- 1. If Sets, then this is called when moving from FG to Built Sets - Verification Step
   -- 2. If Items, then this is called when moving from FG to Consignment Bucket - Shipping step
   ELSIF ((p_type = 'CSET' OR p_type = 'CITEM') AND p_action = 'MINUS')
   THEN
      SELECT t504.c207_set_id, t504.c504_inhouse_purpose, t504.c504_type,
             t504.c701_distributor_id, t504.c504_status_fl
            ,t701.c1900_parent_company_id
        INTO v_set_id, v_purpose, v_type,
             v_dist_id, v_cn_status,
             v_destination_comp_id
        FROM t504_consignment t504, T701_DISTRIBUTOR t701
       WHERE t504.c504_consignment_id = p_id
       AND t504.c701_distributor_id = t701.c701_distributor_id (+)
       AND c504_void_fl IS NULL;

      IF p_type = 'CSET'
      THEN
			SELECT NVL(GET_RULE_VALUE (v_set_type, 'FGDMO'),4812) INTO v_post_id FROM DUAL;--26240101; -- Demo Set
      ELSIF p_type = 'CITEM'
      THEN
         IF v_dist_id IS NOT NULL OR (v_acc_id IS NOT NULL AND v_acc_id <> '01') --ACCOUNT CONSIGNMENT
         THEN
            IF v_type = '40057'
            THEN
               IF v_dist_type = 70105                                   --ICS
               THEN
                  v_post_id := get_rule_value (50250, 'FGICS');
               ELSE                                                     -- ICT
               	  -- to get the OUS CN and international posting id
               	  SELECT get_rule_value ('50252', 'FGICT'), get_rule_value ('50252_ICT', 'FGICT')
				   INTO v_ous_cn_post_id, v_international_post_id
				   FROM dual;
				  -- to set the correct posting id
 				  SELECT DECODE (v_destination_comp_id, NULL, v_ous_cn_post_id, v_international_post_id)
					   INTO v_ict_post_id
					   FROM dual;
				  --	   
                  v_post_id := v_ict_post_id;
               END IF;
            ELSE
               v_post_id := 4822;
            END IF;
         ELSIF v_type = '106703' OR v_type = '106704'  -- 106703 - FG-ST and 106704 - BL-ST
         THEN
         	v_post_id := 26240423;         -- 26240423 Stock Transfer post id
         ELSIF v_purpose IS NOT NULL AND v_dist_id IS NULL
         THEN
            v_post_id := get_rule_value (v_purpose, 'CON-ITEM-INHOUSE');
         END IF;
      END IF;

      --
      FOR rad_val IN cur_consign 
      LOOP         	
       IF p_type = 'CITEM' AND (rad_val.whtype IS NULL OR rad_val.whtype NOT IN (56001)) THEN

            gm_cm_sav_partqty (rad_val.ID,
                               (rad_val.qty * -1),
                               p_id,
                               p_updated_by,
                               90800, -- FG  Qty
                               4302,-- Minus
                               4311 -- Consignment
                               );
        END IF;
        
      END LOOP;
   -- For InHouse and Loaner Sets Posting
   ELSIF    p_action = 'IHL-PLUS'
         OR p_action = 'PDL-PLUS'
         OR p_action = 'IHL-MINUS'
         OR p_action = 'PDL-MINUS'
         OR p_action = 'PDL-MINUS-FG'
         OR p_action = 'IHL-MINUS-FG'
         OR p_action = 'PDL-MINUS-QR'
         OR p_action = 'IHL-MINUS-QR'
         OR p_action = 'BSP-PLUS-BS'
         OR p_action = 'BSP-MINUS-BS'
         OR p_action = 'BSP-MINUS-QR'
         OR p_action = 'BULK-MINUS'
         OR p_action = 'QTY_IN_IHINV'
   THEN
   
	BEGIN
		
		SELECT DECODE(get_rule_value(t504.c504_type,'DEMO_CN_TYPE'),'Y',t207.c207_type,NULL)
		  INTO v_set_type
		  FROM t504_consignment t504, t207_set_master t207, t412_inhouse_transactions t412
		 WHERE t412.c412_inhouse_trans_id = p_id
           AND c504_consignment_id = t412.c412_ref_id
           AND t504.c207_set_id = t207.c207_set_id
           AND c412_void_fl IS NULL
		   --AND c504_void_fl IS NULL
		   AND c207_void_fl IS NULL;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			v_set_type:= NULL;
	END; 	
   
   
      IF SUBSTR (p_action, 0, 3) = 'IHL' OR  p_action = 'QTY_IN_IHINV'
      -- For all InHouse Set Transactions, take Rule for 'INHOUSE-LOANER'
      THEN
         v_post_id := get_rule_value (p_type, 'INHOUSE-LOANER');
         v_type := 4119;
      ELSIF SUBSTR (p_action, 0, 3) = 'BSP'
      THEN
         v_post_id := NVL(get_rule_value (p_type, 'BSP-MOVE-'||v_set_type),get_rule_value (p_type, 'BSP-MOVE'));
         v_type := 4311;
      ELSE
         v_post_id := NVL(get_rule_value (p_type, 'PRODUCT-LOANER-'||v_set_type),get_rule_value (p_type, 'PRODUCT-LOANER'));
         v_type := 4127;
      END IF;
      --
      --
      IF    p_type = 50152
         OR p_type = 50157
         OR p_type = 50161       -- This is because we are adding to shelf qty
         OR p_type = 1006573
         OR p_type = 1006575
      THEN
         v_sign := -1;
      END IF;

      -- This rule will specify if the transaction makes a -ve or +ve impact
      v_action := get_rule_value (p_type, 'TXN-SIGN-ACTION');

      --
      --
      FOR rad_val IN cur_ihl
      LOOP
         IF (   p_action = 'IHL-PLUS'
             OR p_action = 'PDL-PLUS'
             OR p_action = 'PDL-MINUS-FG'
             OR p_action = 'IHL-MINUS-FG'
             OR p_action = 'BSP-PLUS-BS'
             OR p_action = 'BSP-MINUS-BS'
             OR p_action = 'IHL-MINUS'
            )
         THEN
            gm_cm_sav_partqty (rad_val.ID,
                               ((rad_val.qty * v_sign) * -1),
                               p_id,
                               p_updated_by,
                               90800, -- FG  Qty
                               v_action,
                               v_type
                               );
         ELSIF (   p_action = 'PDL-MINUS-QR'
                OR p_action = 'IHL-MINUS-QR'
                OR p_action = 'BSP-MINUS-QR'
               )
         THEN
            gm_cm_sav_partqty (rad_val.ID,
                               (rad_val.qty * v_sign),
                               p_id,
                               p_updated_by,
                               90813, -- quarantine  Qty
                               v_action,
                               v_type
                               );
         ELSIF (   p_action = 'BULK-MINUS'  )
         THEN
            gm_cm_sav_partqty (rad_val.ID,
                               ((rad_val.qty * v_sign) * -1),
                               p_id,
                               p_updated_by,
                               90814, -- BULK  Qty
                               v_action,
                               v_type
                               );
         ELSIF (   p_action = 'QTY_IN_IHINV'  )
         THEN  
    		 gm_cm_sav_partqty (rad_val.ID,
                   rad_val.qty * v_sign,
                   p_id,
                   p_updated_by,
                   90816, -- Inhouse Inventory Qty
                   v_action,
                   4324 -- Inhouse Inventory
                   );                
        END IF;
         --                               
      END LOOP;
   --
   END IF;
--
END IF;
END GM_UPDATE_INSERT_INVENTORY;
/
