/* Formatted on 2011/01/10 12:03 (Formatter Plus v4.8.0) */


CREATE OR REPLACE PROCEDURE gm_ls_upd_csg_loanerstatus (
   p_status_fl   t412_inhouse_transactions.c412_status_fl%TYPE,
   p_status      t504a_consignment_loaner.c504a_status_fl%TYPE,
   p_ref_id      t412_inhouse_transactions.c412_ref_id%TYPE,
   p_userid 	 t504_consignment.c504_created_by%TYPE DEFAULT NULL
)
AS
--
/***************************************************************************************************************
  * This Procedure is to Update Loaner Consignment Status in T504A_CONSIGNMENT_LOANER
  * Algorithm:
  *  a. Get the Minimum Status number from the Set of all transactions associated with a Consignment
  *  b. If the status flag of the current transaction is greater than or equal to the min value, then
  *   update the status (c504a_status_fl) with the status value passed as param
  * Author : Joe P Kumar
  * Date   : 10/10/06
***************************************************************************************************************/
   v_status_fl   t412_inhouse_transactions.c412_status_fl%TYPE;
   v_status      t504a_consignment_loaner.c504a_status_fl%TYPE;
--
BEGIN
--
   BEGIN
      SELECT c412_status_fl
        INTO v_status_fl
        FROM t412_inhouse_transactions
       WHERE c412_status_fl =
                (SELECT MIN (c412_status_fl)
                   FROM t412_inhouse_transactions
                  WHERE c412_ref_id = p_ref_id
                    AND c412_type NOT IN (50159,1006571,50160, 50161, 50162)
                    AND c412_void_fl IS NULL)
         AND c412_ref_id = p_ref_id
         AND ROWNUM = 1;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN;
   END;

   SELECT gm_pkg_op_loaner.get_cs_fch_loaner_status(p_ref_id) INTO v_status FROM DUAL;
   
   -- Hold the CN if CN status is Allocated or PendingShip
   IF v_status IN ('5','10')
   THEN
	   	gm_pkg_op_consignment.gm_sav_hold_fl(p_ref_id,p_userid);
   END IF;
--
   IF v_status_fl = 0
   THEN
      v_status_fl := 2;
   END IF;

   IF v_status_fl >= p_status_fl
   THEN
       BEGIN 
	      
   		SELECT c504a_status_fl INTO v_status_fl 
   		FROM t504a_consignment_loaner 
   		WHERE c504_consignment_id = p_ref_id;
        EXCEPTION WHEN NO_DATA_FOUND then
        v_status_fl := null;
        end;
   		IF v_status_fl != 60 and v_status_fl is not null 
   		THEN
   			gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status (p_ref_id, p_status, p_userid) ;
   	
	      UPDATE t504a_consignment_loaner
	         SET c504a_available_date = DECODE (p_status, 0, SYSDATE, NULL),
	             c504a_last_updated_by = p_userid,
				 c504a_last_updated_date = SYSDATE
	       WHERE c504_consignment_id = p_ref_id AND c504a_status_fl != 60;
	    END IF;
   END IF;
   
   --Changes added for PMT-12455 and it's used to trigger an update to iPad
   --gm_pkg_op_loaner_set_txn.GM_SAV_AVAIL_LN_SET_UPDATE (p_ref_id, p_userid); -- consignment_id, user_id, company_id 
   	
--
END gm_ls_upd_csg_loanerstatus;
/
