CREATE OR REPLACE PROCEDURE gm_update_insert_inv_sales (
   p_id           IN       VARCHAR2,
   p_type         IN       VARCHAR2,
   p_action       IN       VARCHAR2,
   p_updated_by   IN       VARCHAR2
)
AS
--
   v_item_type	NUMBER;
   v_item_qty	NUMBER;
   v_trans_company_id t1900_company.c1900_company_id%TYPE;
   v_trans_plant_id t5040_plant_master.c5040_plant_id%TYPE;
--
   CURSOR cur_sales
   
   IS
        SELECT t5056.c205_insert_id ID,1 qty,NULL price,50300 itype,NVL(t501.c901_order_type, '-9999') ordertype,
		       NULL order_sales_post_id
		  FROM t5056_transaction_insert t5056, t501_order t501, t205c_part_qty t205c
	   	 WHERE t5056.c5056_ref_id = p_id
		   AND t5056.c5056_ref_id = t501.c501_order_id
		   AND t5056.c205_insert_id = t205c.c205_part_number_id      -- PMT-48520 - Remove Insert Part Number in Shipping Job
		   AND t205c.c901_transaction_type = 90800  
		   AND t205c.c5040_plant_id =  v_trans_plant_id 
           AND t205c.c205_available_qty > 0
		   AND t5056.c5056_void_fl IS NULL
		   AND t501.C501_VOID_FL IS NULL;		    	    
--
BEGIN
--   
     
    -- to get the transactions company id and plantid
	gm_pkg_common.gm_fch_trans_comp_plant_id(p_id, v_trans_company_id, v_trans_plant_id);

	IF v_trans_company_id IS NOT NULL AND v_trans_plant_id IS NOT NULL THEN             
     	gm_pkg_cor_client_context.gm_sav_client_context(v_trans_company_id,v_trans_plant_id);
     END IF;

     FOR rad_val IN cur_sales
     LOOP
         --
		 v_item_type := rad_val.itype;
		 v_item_qty := rad_val.qty;
		 IF v_item_type = 50300 AND v_item_qty > 0 AND (rad_val.ordertype != 2532 AND rad_val.ordertype != 26240234) THEN
			gm_cm_sav_partqty (rad_val.ID,
	                               (rad_val.qty * -1),
	                               p_id,
	                               p_updated_by,
	                               90800, -- FG  Qty
	                               4302,-- Minus
	                               4310 -- Sales
	                               );
		 END IF;		
     END LOOP;
--     
END gm_update_insert_inv_sales;
/