/* Formatted on 2008/10/24 15:47 (Formatter Plus v4.8.0) */
-- @"c:\database\procedures\operations\gm_save_reprocess_returns.prc"
CREATE OR REPLACE PROCEDURE gm_save_reprocess_returns (
	p_raid		  IN	   t506_returns.c506_rma_id%TYPE
  , p_cnstr 	  IN	   VARCHAR2
  , p_qnstr 	  IN	   VARCHAR2
  , p_pnstr 	  IN	   VARCHAR2
  , p_userid	  IN	   t506_returns.c506_last_updated_by%TYPE
  , p_returnids   OUT	   VARCHAR2
  , p_errmsg	  OUT	   VARCHAR2
)
AS
--
	v_id		   VARCHAR2 (20);
	v_msg		   VARCHAR2 (1000);
	e_others	   EXCEPTION;
	--
	v_set_id	   t506_returns.c207_set_id%TYPE;
	v_reprocess_dt t506_returns.c207_set_id%TYPE;
--
	v_company_id  t1900_company.c1900_company_id%TYPE;
	v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
BEGIN
	   
    SELECT NVL(GET_COMPID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL')), NVL(GET_PLANTID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_PLANT','ONEPORTAL'))
      INTO v_company_id, v_plant_id
      FROM DUAL;
--
	SELECT	   c207_set_id, c501_reprocess_date
		  INTO v_set_id, v_reprocess_dt
		  FROM t506_returns
		 WHERE c506_rma_id = p_raid AND c506_void_fl IS NULL
	FOR UPDATE;

	IF v_reprocess_dt IS NOT NULL
	THEN
		raise_application_error (-20943, '');	-- ORA20062 = Inventory already updated
	END IF;

	--
	IF p_cnstr IS NOT NULL
	THEN
		--
		SELECT get_next_consign_id ('Consign')
		  INTO v_id
		  FROM DUAL;

		--
		INSERT INTO t504_consignment
					(c504_consignment_id, c207_set_id, c504_reprocess_id, c504_status_fl, c504_verify_fl
				   , c504_created_by, c504_created_date, c1900_company_id, c5040_plant_id
					)
			 VALUES (v_id, v_set_id, p_raid, '0', '0'
				   , p_userid, CURRENT_DATE, v_company_id, v_plant_id
					);

		--
		gm_save_reprocess_returns_item (v_id, p_cnstr, p_errmsg);
		--
		gm_update_inventory_substores (v_id, 'SET-REPROCESS', p_userid);
		--
		p_returnids := p_returnids || ',' || v_id;
	END IF;

	--
	IF p_qnstr IS NOT NULL
	THEN
		--
		SELECT get_next_consign_id ('QuaranIn')
		  INTO v_id
		  FROM DUAL;

		--
		INSERT INTO t504_consignment
					(c504_consignment_id, c704_account_id, c504_type, c504_inhouse_purpose, c504_reprocess_id
				   , c504_status_fl, c504_ship_to, c504_ship_to_id, c504_created_by, c504_created_date, c504_ship_req_fl
				   , c1900_company_id, c5040_plant_id
					)
			 VALUES (v_id, '01', 4113, 4151, p_raid
				   , '3', 4123, p_userid, p_userid, CURRENT_DATE, 1
				   , v_company_id, v_plant_id
					);

		--
		gm_save_reprocess_returns_item (v_id, p_qnstr, p_errmsg);
		--
		gm_update_inventory_substores (v_id, 'QUAR-REPROCESS', p_errmsg);
		--
		p_returnids := p_returnids || ',' || v_id;
	END IF;

	--
	IF p_pnstr IS NOT NULL
	THEN
		--
		SELECT get_next_consign_id ('Repack')
		  INTO v_id
		  FROM DUAL;

		--
		INSERT INTO t504_consignment
					(c504_consignment_id, c704_account_id, c504_type, c504_inhouse_purpose, c504_reprocess_id
				   , c504_status_fl, c504_ship_to, c504_ship_to_id, c504_created_by, c504_created_date, c504_ship_req_fl
				   , c1900_company_id, c5040_plant_id
					)
			 VALUES (v_id, '01', 4114, 4140, p_raid
				   , '3', 4123, p_userid, p_userid, CURRENT_DATE, 1
				   , v_company_id, v_plant_id
					);

		--
		gm_save_reprocess_returns_item (v_id, p_pnstr, p_errmsg);
		--
		gm_update_inventory_substores (v_id, 'PACK-REPROCESS', p_errmsg);
		--
		p_returnids := p_returnids || ',' || v_id;
	END IF;

	--
	UPDATE t506_returns
	   SET c501_reprocess_date = CURRENT_DATE
	     , c506_last_updated_by = p_userid
		 , c506_last_updated_date = CURRENT_DATE
	 WHERE c506_rma_id = p_raid AND c506_void_fl IS NULL;
--
END gm_save_reprocess_returns;
/
