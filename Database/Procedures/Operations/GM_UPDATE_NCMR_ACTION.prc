-- @"C:\Database\Procedures\Operations\GM_UPDATE_NCMR_ACTION.prc";
CREATE OR REPLACE
PROCEDURE gm_update_ncmr_action (
        p_ncmrid           IN t409_ncmr.c409_ncmr_id%TYPE,
        p_action           IN t409_ncmr.c409_disposition%TYPE,
        p_rtsfl            IN t409_ncmr.c409_rts_fl%TYPE,
        p_respon           IN t409_ncmr.c409_responsibility%TYPE,
        p_remarks          IN t409_ncmr.c409_disp_remarks%TYPE,
        p_authby           IN t409_ncmr.c409_disp_by%TYPE,
        p_closeqty         IN t409_ncmr.c409_closeout_qty%TYPE,
        p_dispdate         IN VARCHAR2,
        p_userid           IN t409_ncmr.c409_last_updated_by%TYPE,
        p_rejection_reason IN t409_ncmr.c409_rej_reason%TYPE,
        p_message OUT VARCHAR2)
AS
    v_dhr_id t408_dhr.c408_dhr_id%TYPE;
    v_qty NUMBER := 0;
    v_control t408_dhr.c408_control_number%TYPE;
    v_qtyrec t408_dhr.c408_qty_received%TYPE;
    v_pnum t408_dhr.c205_part_number_id%TYPE;
    v_workordid t408_dhr.c402_work_order_id%TYPE;
    v_partyid t401_purchase_order.c301_vendor_id%TYPE;
    v_wo_uom_str VARCHAR2 (100) ;
    v_amt t402_work_order.c402_cost_price%TYPE;
    v_uomqty NUMBER;
    v_chkdhrvoidfl t408_dhr.c408_void_fl%TYPE;
    v_chkstatusfl   NUMBER;
    v_po_type       NUMBER;
    v_post_id       NUMBER;
    v_ncmr_sts_fl   NUMBER;
    v_close_out_qty NUMBER;
    v_qty_rejected  NUMBER;
    v_dhr_void_fl	t408_dhr.c408_void_fl%TYPE;
    v_dhr_status	NUMBER;
    v_qty_inspected	NUMBER;
    v_received_date DATE;
    v_ncmr_type		t409_ncmr.c901_type%TYPE;
    v_rej_count     NUMBER;
    v_count     NUMBER;
    v_donor_id		t408_dhr.c2540_donor_id%TYPE;
    v_qty_received	t408_dhr.c408_qty_received%TYPE;
    v_show_error  	BOOLEAN := TRUE;
    v_dateformat 	varchar2(100);    
    v_company_id    t1900_company.c1900_company_id%TYPE;
	v_serial_num_fl	t205d_part_attribute.c205d_attribute_value%TYPE;
	--
	v_trans_comp_id t1900_company.c1900_company_id%TYPE;
	v_owner_comp_id t1900_company.c1900_company_id%TYPE;
	v_price NUMBER (15,2);
	v_vendor_cost_type t301_vendor.c901_vendor_cost_type%TYPE;
	v_vendor_std_cost t302_vendor_standard_cost.c302_standard_cost%TYPE;
BEGIN
	 select get_compdtfmt_frm_cntx(), NVL(get_compid_frm_cntx(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL')) into v_dateformat, v_company_id from dual;
		
     SELECT TO_NUMBER(t409.c409_status_fl), c408_qty_inspected,  NVL (t408.c408_qty_rejected, 0), NVL (t409.c409_closeout_qty, 0), t408.c408_qty_received
      , t408.c408_dhr_id, TO_NUMBER(t408.c408_status_fl), t408.c408_void_fl, t408.c408_received_date, t409.c901_type, t408.c2540_donor_id
      , get_part_attr_value_by_comp(t408.C205_PART_NUMBER_ID,'106040',v_company_id)
       INTO v_ncmr_sts_fl, v_qty_inspected,  v_qty_rejected, v_close_out_qty, v_qty_received
      , v_dhr_id, v_dhr_status, v_dhr_void_fl, v_received_date, v_ncmr_type, v_donor_id, v_serial_num_fl
       FROM t409_ncmr t409, t408_dhr t408
      WHERE t409.c409_ncmr_id = p_ncmrid
        AND t408.c408_dhr_id  = t409.c408_dhr_id
	  FOR UPDATE;

    IF v_ncmr_type IS NULL AND  p_closeqty IS NOT NULL AND p_closeqty > 0 THEN
    		GM_RAISE_APPLICATION_ERROR('-20999','324','');
    END IF;
     
    IF v_qty_inspected IS NULL THEN
    	GM_RAISE_APPLICATION_ERROR('-20999','325','');
    END IF;
        
    IF v_dhr_void_fl IS NOT NULL THEN
    	GM_RAISE_APPLICATION_ERROR('-20999','326','');
    END IF;
    
    IF v_ncmr_sts_fl  = 3 THEN
        GM_RAISE_APPLICATION_ERROR('-20999','327','');
    END IF;
    
    IF v_dhr_status >= 2 AND v_ncmr_sts_fl >= 2 AND v_close_out_qty != p_closeqty THEN
        GM_RAISE_APPLICATION_ERROR('-20999','328','');
    END IF;
     
    IF v_ncmr_sts_fl >= 2 AND   v_close_out_qty != p_closeqty  THEN
        gm_pkg_op_ncmr.gm_revert_ncmr_posting (p_ncmrid, p_userid);
    END IF;
    
    SELECT COUNT (1)
	    INTO v_rej_count
	   FROM t408a_dhr_control_number
	  WHERE c408_dhr_id = v_dhr_id
	    AND c901_status = 1401 		--- 1401 rejected
		AND c408a_void_fl  IS NULL; 
		
	SELECT gm_pkg_op_bulk_dhr_appr.get_bulk_shipment_id(v_dhr_id)
		INTO v_count	
	FROM DUAL;
	
	-- If the rececived qty = rejected qty and closeout qty is 0, skip validation
	IF (v_qty_received = v_qty_rejected AND p_closeqty = 0)
	THEN
		v_show_error := FALSE;
	END IF;
	
	--if the DHR from Bulk shipment and closeout qty not match with qty approved, throw error.
	IF (v_count >0 AND p_closeqty != (v_qty_rejected - v_rej_count))
	THEN
		GM_RAISE_APPLICATION_ERROR('-20999','329','');
	ELSIF((v_donor_id IS NOT NULL OR v_serial_num_fl = 'Y') AND p_closeqty = v_qty_rejected AND v_count <= 0 AND v_rej_count > 0)
	THEN 
		GM_RAISE_APPLICATION_ERROR('-20999','331','');
	ELSIF((v_donor_id IS NOT NULL OR v_serial_num_fl = 'Y') AND p_closeqty != (v_qty_rejected - v_rej_count) AND v_show_error)
	THEN
		GM_RAISE_APPLICATION_ERROR('-20999','330','');
	ELSIF (NOT v_show_error AND v_rej_count >0 AND v_count <=0 )-- If the rececived qty = rejected qty and closeout qty is 0 but in donor details screen control number is rejected
	THEN
		GM_RAISE_APPLICATION_ERROR('-20999','331','');
    END IF;	
    
     UPDATE t409_ncmr
    SET c409_disposition  = p_action, c409_rts_fl = p_rtsfl, c409_responsibility = p_respon
      , c409_disp_remarks = p_remarks, c409_disp_by = p_authby, c409_closeout_qty = p_closeqty
      , c409_rej_reason   = p_rejection_reason, c409_last_updated_by = p_userid, c409_last_updated_date = CURRENT_DATE
      , c409_disp_date    = TO_DATE(p_dispdate,v_dateformat), c409_status_fl = '2'
      WHERE c409_ncmr_id  = p_ncmrid;
    
      v_qty                := v_qty_rejected - p_closeqty;
    
    IF v_qty              > 0 AND  ((v_close_out_qty = 0 AND p_closeqty = 0 AND v_ncmr_sts_fl = 1) OR (v_close_out_qty != p_closeqty))  THEN
         SELECT c408_void_fl, TO_NUMBER (c408_status_fl), c408_control_number
          , c408_qty_received, c205_part_number_id, c402_work_order_id
           INTO v_chkdhrvoidfl, v_chkstatusfl, v_control
          , v_qtyrec, v_pnum, v_workordid
           FROM t408_dhr
          WHERE c408_dhr_id        = v_dhr_id;
          
         SELECT b.c301_vendor_id, get_wo_uom_type_qty (c402_work_order_id), NVL (DECODE (b.c401_type, 3104, c.c205_cost
            , a.c402_cost_price), a.c402_cost_price), b.c401_type, b.c1900_company_id
            , NVL(b.c1900_source_company_id, b.c1900_company_id)
            , gm_pkg_pur_vendor_cost.get_vendor_cost_type(b.c301_vendor_id)
           INTO v_partyid, v_wo_uom_str, v_amt
          , v_po_type, v_trans_comp_id
          , v_owner_comp_id, v_vendor_cost_type
           FROM t402_work_order a, t401_purchase_order b, t205_part_number c
          WHERE c402_work_order_id     = v_workordid
            AND a.c401_purchase_ord_id = b.c401_purchase_ord_id
            AND a.c205_part_number_id  = c.c205_part_number_id;
            
        IF (v_po_type                  = 3104 OR v_po_type = 3105) THEN
            v_post_id                 := 48082;
        ELSE
        	v_post_id := 48130;
        	-- Only Regular PO - get the Standard Cost
        	--106561 Standard Cost
	        IF NVL(v_vendor_cost_type, '-999') = '106561' AND v_po_type = 3100
	        THEN
	        	SELECT gm_pkg_pur_vendor_cost.get_vendor_std_cost (v_partyid, v_pnum)
	        		INTO v_vendor_std_cost
	        	FROM DUAL;
	        	--
	   			v_amt := v_vendor_std_cost;
	   			-- Setting the posting id (Standard Cost) - code changed for this PMT-20749
	   			v_post_id := 26240418;				   
	   		END IF;	-- end vendor cost type
        END IF;
        v_uomqty := TO_NUMBER (NVL (SUBSTR (v_wo_uom_str, INSTR (v_wo_uom_str, '/') + 1), 1)) ;
        -- to calculate the company currency cost
        v_price := (get_currency_conversion (get_vendor_currency (v_partyid), get_comp_curr(v_trans_comp_id), v_received_date, v_amt) / v_uomqty);
        --
        gm_save_ledger_posting (v_post_id, CURRENT_DATE, v_pnum, v_partyid, p_ncmrid, v_qty, v_price, p_userid, NULL, v_owner_comp_id, v_price) ;
    END IF;

   /*v_count: To check whether the dhr is associated with bulk shipment or not
    * v_donor_id :  to check whether the part is tissue part 
    * if rejected qty is same as closeout qty, need to update the control number status of all parts as approved
    * if received qty and rejected qty is equal and closeout qty is 0, then upadate the status as rejected 
    */
    IF (v_count = 0 AND v_donor_id IS NOT NULL AND v_qty_rejected = p_closeqty)
    THEN
    	UPDATE T408A_DHR_CONTROL_NUMBER
		    SET c901_status = '1400' --Approved
		    	, c408a_last_updated_by = p_userid
		    	, c408a_last_updated_date = CURRENT_DATE
		    WHERE c408_dhr_id = v_dhr_id
		    AND c408a_void_fl IS NULL;
    ELSIF (v_count = 0 AND v_donor_id IS NOT NULL AND v_qty_received = v_qty_rejected AND p_closeqty = '0')
    THEN
    	UPDATE T408A_DHR_CONTROL_NUMBER
		    SET c901_status = '1401' --Rejected
		    	, c408a_last_updated_by = p_userid
		    	, c408a_last_updated_date = CURRENT_DATE
		    WHERE c408_dhr_id = v_dhr_id
		    AND c408a_void_fl IS NULL;
    END IF;
    
END gm_update_ncmr_action;
/
