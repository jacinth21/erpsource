/* Formatted on 2011/04/05 20:37 (Formatter Plus v4.8.0) */
--@"C:\Database\Procedures\Operations\GM_MODIFY_DHR_RECEIVE.prc";
CREATE OR REPLACE
PROCEDURE gm_modify_dhr_receive (
        p_dhrid       IN t408_dhr.c408_dhr_id%TYPE,
        p_qtyrec      IN t408_dhr.c408_qty_received%TYPE,
        p_control     IN t408_dhr.c408_control_number%TYPE,
        p_packslip    IN t408_dhr.c408_packing_slip%TYPE,
        p_comments    IN t408_dhr.c408_comments%TYPE,
        p_manfdt      IN VARCHAR2,
        p_userid      IN t408_dhr.c408_last_updated_by%TYPE,
        p_workordid   IN t408_dhr.c402_work_order_id%TYPE,
        p_updstatusfl IN t408_dhr.c408_status_fl%TYPE,
        p_exp_date    IN VARCHAR,
        p_udi		  IN t408_dhr.c408_udi_no%TYPE DEFAULT NULL
)     
AS
    v_chkstatusfl  NUMBER (1) ;
    v_chkdhrvoidfl CHAR (1) ;
    v_uomqty       NUMBER (6) ;
    v_control t408_dhr.c408_control_number%TYPE;
    v_qtyrec t408_dhr.c408_qty_received%TYPE;
    v_pnum t408_dhr.c205_part_number_id%TYPE;
    v_post_id    NUMBER;
    v_diff_qty   NUMBER;
    v_rw_post_id NUMBER;
    v_po_type    NUMBER;
    v_udi_num    VARCHAR2(100);
    v_udi_number VARCHAR2(100);
    v_cn_prefix  VARCHAR2(10);
    v_ed_prefix  VARCHAR2(10);
    v_exp_date   VARCHAR2(10); 
    v_vendor_id		t408_dhr.c301_vendor_id%TYPE;
    v_date_fmt		varchar2(100);

    -- Get the control number details of the dhr
    CURSOR dhrDtlsCur
	IS
		SELECT NVL(t408a.c408a_conrol_number, t408.c408_control_number) ctrlnum
	       FROM t408_dhr t408, T408A_DHR_CONTROL_NUMBER T408A
    	 WHERE T408.C408_DHR_ID = T408A.C408_DHR_ID(+)
      		AND t408.c408_dhr_id = p_dhrid
        	AND t408a.c408a_void_fl(+) IS NULL ;
BEGIN
	select get_compdtfmt_frm_cntx() into v_date_fmt from dual;
    -- Check for DHR Void Flag. If the Flag is turned ON, then throw an Application Error
     SELECT t408.c408_void_fl, TO_NUMBER (t408.c408_status_fl), t408.c408_control_number
      , t408.c408_qty_received, t408.c205_part_number_id, t401.C401_TYPE , t408.c408_udi_no, t408.c301_vendor_id
       INTO v_chkdhrvoidfl, v_chkstatusfl, v_control
      , v_qtyrec, v_pnum, v_po_type , v_udi_num, v_vendor_id
       FROM t408_dhr t408, t402_work_order t402, t401_purchase_order t401
      WHERE t408.c408_dhr_id          = p_dhrid
        AND t402.c402_work_order_id   = t408.c402_work_order_id
        AND t402.c401_purchase_ord_id = t401.c401_purchase_ord_id ;
    --validate control number duplication
    --Updating the control number in existing UDI number 
 	IF v_udi_num IS NOT NULL THEN
        -- Form the UDI No based on update/new Control no from UPDATE DHR screen
         v_cn_prefix := get_rule_value ('CN','103501'); -- Get the Prefix for Control number
         v_ed_prefix := get_rule_value ('ED','103501'); -- Get the Prefix for Expiry Date
         IF p_exp_date IS NOT NULL THEN
         select to_char(to_date(p_exp_date,v_date_fmt),get_rule_value('UDIFMT','DATEFORMAT')) INTO v_exp_date FROM DUAL; 
         select substr(v_udi_num,0,INSTR(v_udi_num,v_ed_prefix)+length(v_ed_prefix)-1) INTO v_udi_number FROM DUAL;
    	 v_udi_number := v_udi_number || v_exp_date || v_cn_prefix|| p_control;
         ELSE
    	 select substr(v_udi_num,0,INSTR(v_udi_num,v_cn_prefix)+length(v_cn_prefix)-1) INTO v_udi_number FROM DUAL;
    	 v_udi_number := v_udi_number || p_control;
    	 END IF;
 	END IF;	 
    gm_pkg_op_dhr_rpt.gm_validate_control_no (v_pnum||'^'||p_workordid||'^ ^'||p_control||'^|') ;
    IF (v_chkdhrvoidfl IS NOT NULL) THEN
        --
        raise_application_error ( - 20004, '') ;
        --
    END IF;
    --
    -- End of Check for DHR Void Flag and  check if the value is updated by other user
    IF v_chkstatusfl > TO_NUMBER (p_updstatusfl) THEN
        raise_application_error ( - 20001, '') ;
        --
    END IF;
    -- IF DHR is verified and qty received is changed throw an error
    IF v_chkstatusfl = 4 AND v_qtyrec != p_qtyrec THEN
        GM_RAISE_APPLICATION_ERROR('-20999','315',''); 
    END IF;
    -- IF DHR is in Pending Verification status and qty received is changed throw an error
    IF v_chkstatusfl = 3 AND v_qtyrec != p_qtyrec THEN
        GM_RAISE_APPLICATION_ERROR('-20999','316','');
    END IF;
    -- End of Check for DHR Status Flag and check if the value is updated by other user
    -- To get UOM Quantity
     SELECT TO_NUMBER (SUBSTR (get_wo_uom_type_qty (p_workordid), 4))
       INTO v_uomqty
       FROM DUAL;
    -- Update Shipped Qty if Mod of Recieved Qty / UOM Qty is 0
    -- No need to change the date format, since manufacturing date is in US format
    IF MOD (p_qtyrec, v_uomqty) = 0 THEN
         UPDATE t408_dhr
        SET c408_qty_received                             = p_qtyrec, c408_control_number = p_control, c408_packing_slip = p_packslip
          , c408_comments                                 = p_comments, c408_shipped_qty = (p_qtyrec / v_uomqty),
          c408_manf_date = NVL(TO_DATE (p_manfdt, v_date_fmt),c408_manf_date), c408_last_updated_by = p_userid, c408_last_updated_date = CURRENT_DATE , c408_udi_no = decode(p_udi,null,v_udi_number,p_udi)
          WHERE c408_dhr_id                               = p_dhrid;
    ELSE
        raise_application_error ( - 20134, '') ;
    END IF;
    IF v_control != p_control THEN
         UPDATE t413_inhouse_trans_items t413
        SET t413.c413_control_number        = p_control
          , c413_last_updated_by = p_userid
	   	  , c413_last_updated_date = CURRENT_DATE 
          WHERE t413.c412_inhouse_trans_id IN
            (
                 SELECT t412.c412_inhouse_trans_id
                   FROM t412_inhouse_transactions t412
                  WHERE t412.c412_ref_id = p_dhrid
            ) ;
         UPDATE t412_inhouse_transactions t412
        SET t412.c412_last_updated_by = p_userid, t412.c412_last_updated_date = CURRENT_DATE
          WHERE t412.c412_ref_id      = p_dhrid;
    END IF;
    IF v_qtyrec           < p_qtyrec THEN
        v_diff_qty       := p_qtyrec - v_qtyrec;
        IF v_po_type      = 3101 THEN
            v_post_id    := 48201;
            v_rw_post_id := 48209;
        ELSE
            v_post_id := 48138;
        END IF;
    ELSIF v_qtyrec        > p_qtyrec THEN
        v_diff_qty       := v_qtyrec - p_qtyrec;
        IF v_po_type      = 3101 THEN
            v_post_id    := 48212;
            v_rw_post_id := 48213;
        ELSE
            v_post_id := 48139;
        END IF;
    END IF;
    IF v_post_id IS NOT NULL THEN
        gm_pkg_op_dhr.gm_op_sav_dhr_posting (p_dhrid, v_diff_qty, v_post_id, p_userid) ;
        IF v_po_type = 3101 THEN
            gm_pkg_op_dhr.gm_op_sav_dhr_posting (p_dhrid, v_diff_qty, v_rw_post_id, p_userid) ;
        END IF;
    END IF;
    -- End of update Shipped Qty
    -- Save Expiry Date
    IF p_exp_date IS NOT NULL THEN
    	--comment following save record in t2550 here, will populate to t2550 when verify DHR
        --GM_OP_SAV_LOT_EXPIRY_DATE (v_pnum, p_control, p_exp_date,p_userid);
        
    	IF v_chkstatusfl = 4 -- should update t2550_part_control_number table only if the dhr is verified
    	THEN
   	    	FOR v_index IN dhrDtlsCur
   	    	LOOP
	    		UPDATE t2550_part_control_number
				SET c2550_expiry_date = to_date(p_exp_date, v_date_fmt)
					, c2550_last_updated_by = p_userid
					, c2550_last_updated_date = CURRENT_DATE
				WHERE c2550_control_number = v_index.ctrlnum
				AND c205_part_number_id = v_pnum
				AND c301_vendor_id = v_vendor_id;
			END LOOP;
    	END IF;
    
         UPDATE t408_dhr
         SET c2550_expiry_date = to_date(p_exp_date, v_date_fmt),
         	c408_last_updated_by = p_userid, c408_last_updated_date = CURRENT_DATE
         WHERE c408_dhr_id                               = p_dhrid;
    END IF;
	
    --Update in t2550
    	GM_UPDATE_UDI_FORMAT(p_dhrid || ',', p_userid);
	
END gm_modify_dhr_receive;
/
