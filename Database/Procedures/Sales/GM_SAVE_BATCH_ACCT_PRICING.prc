/* Formatted on 12/03/2009 16:08 (Formatter Plus v4.8.0) */
-- @"C:\database\Procedures\Sales\gm_save_batch_acct_pricing.prc";

CREATE OR REPLACE PROCEDURE gm_save_batch_acct_pricing (
	p_accid 	  IN	   t705_account_pricing.c704_account_id%TYPE
  , p_str		  IN	   VARCHAR2
  , p_userid	  IN	   t705_account_pricing.c705_last_updated_by%TYPE
  , p_type		  IN	   VARCHAR2
  , p_recordset   OUT	   TYPES.cursor_type
  , p_invalidParts  OUT	   CLOB
)
AS
	v_strlen	   NUMBER := NVL (LENGTH (p_str), 0);
	v_string	   VARCHAR2 (30000) := p_str;
	v_pnum		   VARCHAR2 (20);
	v_price 	   NUMBER (15, 2);
	v_substring    VARCHAR2 (1000);
	v_start_time   DATE;
	v_end_time	   DATE;
	v_project_id   T202_project.c202_project_id%TYPE;
    v_invalid_partstr    CLOB;
    v_count        NUMBER;
    v_party_id     t705_account_pricing.c101_party_id%TYPE;
    v_company_id   t1900_company.c1900_company_id%TYPE;
	
BEGIN
	v_start_time := CURRENT_DATE;
	v_party_id:=p_accid;
	
	SELECT get_compid_frm_cntx()
      INTO v_company_id
      FROM DUAL;
	
	SELECT get_compid_frm_cntx()
      INTO v_company_id
      FROM DUAL;
	--
	WHILE INSTR (v_string, '|') <> 0
	LOOP
		v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
		v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
		v_pnum		:= NULL;
		v_price 	:= NULL;
		v_pnum		:= TRIM(SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
		v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
		v_price 	:= v_substring;
		--
		SELECT get_project_id_from_part(v_pnum)
		  INTO v_project_id
		  FROM DUAL;
		-- We can allow the part, If the part is RFS for that company only.
		SELECT count(1)
		  INTO v_count
		  FROM t205_part_number t205, t2023_part_company_mapping t2023
		 WHERE t2023.c205_part_number_id = v_pnum
		   AND t205.c205_part_number_id = t2023.c205_part_number_id
		   AND t2023.c1900_company_id = v_company_id
		   AND t2023.c2023_void_fl IS NULL;

		IF v_project_id = ' ' OR v_count = '0'
		THEN
			 v_invalid_partstr := v_invalid_partstr ||v_pnum ||', ';       -- separating invalid parts
		ELSE
			gm_save_account_partnum_price (v_price, 0, v_pnum, p_accid, p_userid, p_type, 'NO');
		END IF;

	--
	END LOOP;
	 	IF v_invalid_partstr IS NOT NULL
			THEN
			 v_invalid_partstr := SUBSTR (v_invalid_partstr, 1, (LENGTH(v_invalid_partstr)-2) ); 
			 GM_RAISE_APPLICATION_ERROR('-20999','369',v_invalid_partstr);
			 --
		END IF;                                  
	--
	v_end_time	:= CURRENT_DATE;
	 p_invalidParts := v_invalid_partstr;
	--
	IF p_type = 'Account'
	THEN
		BEGIN
			SELECT C101_PARTY_ID
			  INTO v_party_id
			FROM t704_account
			WHERE c704_account_id = v_party_id
			AND C704_VOID_FL IS NULL;
		 EXCEPTION WHEN NO_DATA_FOUND	
		THEN
				GM_RAISE_APPLICATION_ERROR('-20999','370','');
		END;
		
	END IF;
				 
	OPEN p_recordset
	 FOR
		 SELECT t705.c205_part_number_id ID, t705.c705_unit_price price, t205.C205_PART_NUM_DESC pdesc
				, t705.c705_discount_offered disc, t705.c705_account_pricing_id apid
				, t705.c705_created_date cdate, get_user_name (t705.c705_created_by) cuser
				, t705.c705_last_updated_date ldate, get_user_name (t705.c705_last_updated_by) luser
				, t705.c705_history_fl hisfl
		        , '' grouptype
		        , '1019' audit_id -- 1019 - audit Trail id for getting History(T705_ACCOUNT_PRICING table) - we are using this Group Type and Audit ID in DTPriceReportWrapper for getting getID() and getHISFL()
			 FROM t705_account_pricing t705,t205_part_number t205
			WHERE t705.c101_party_id =   TO_NUMBER (v_party_id)
			  AND t705.c205_part_number_id =t205.c205_part_number_id
			  AND NVL(t705.c705_last_updated_date,t705.c705_created_date) BETWEEN v_start_time AND v_end_time
			  AND NVL(t705.c705_last_updated_by, t705.c705_created_by) = p_userid
			  AND t705.C705_VOID_FL IS NULL
		 ORDER BY t705. c205_part_number_id;

END gm_save_batch_acct_pricing;
/
