/* Formatted on 2008/01/08 11:16 (Formatter Plus v4.8.0) */
--@"c:\database\procedures\sales\gm_load_account_pricing.prc";
create or replace PROCEDURE gm_load_account_pricing (
	p_acc_id	IN		 VARCHAR2
  , p_proj_id	IN		 VARCHAR2
  , p_user_id	IN		 VARCHAR2
  , p_acct_pricing   IN  VARCHAR2 DEFAULT NULL
)
AS
/*
	  Description			:This procedure is called for
	  Parameters			:p_column
							:p_recordset
*/
	
	v_country	VARCHAR2 (20);
	
	-- 4203: Tier 1, 4201:Tier 2, 4202:Tier 3, 4200:Tier 4 			
	CURSOR pop_val
	IS
		SELECT c205_part_number_id ID, c205_list_price tier1, c205_loaner_price tier2, c205_consignment_price tier3
			 , c205_equity_price tier4, C202_PROJECT_ID projid
			 , DECODE(p_acct_pricing,'4203',c205_list_price,'4201',c205_loaner_price,'4202',c205_consignment_price,'4200',c205_equity_price) TIER   
		  FROM t205_part_number
		 WHERE c205_part_number_id IN (
				   SELECT c205_part_number_id
					 FROM t205_part_number
					WHERE c202_project_id = NVL(p_proj_id,c202_project_id)
					  AND c205_active_fl = 'Y')
		AND c205_equity_price IS NOT NULL
		AND c205_loaner_price IS NOT NULL
		AND c205_consignment_price IS NOT NULL
		AND c205_list_price IS NOT NULL;
		
BEGIN
	
	SELECT 	get_rule_value('CURRENT_DB', 'COUNTRYCODE')
		INTO v_country 
		FROM	DUAL;
		
		FOR rad_val IN pop_val
		LOOP
			IF (v_country != 'en')-- For OUS countries
			THEN
			
				UPDATE t705_account_pricing 
				SET  c705_unit_price = rad_val.TIER ,
				   c705_last_updated_date = CURRENT_DATE, 
				   c705_last_updated_by =p_user_id,
				    c7501_account_price_req_dtl_id = null
				WHERE c205_part_number_id=rad_val.ID
				AND c704_account_id=p_acc_id;
		      
				IF (SQL%ROWCOUNT = 0 )
				THEN      
					INSERT INTO t705_account_pricing
								(c705_account_pricing_id, c704_account_id, c205_part_number_id, c705_unit_price
							    , c705_discount_offered, c202_project_id, c705_created_date, c705_created_by
								)
					 VALUES (s704_price_account.NEXTVAL, p_acc_id, rad_val.ID, rad_val.TIER
						 		, 0 , rad_val.projid, CURRENT_DATE, p_user_id
								);
				END IF;
			
			ELSE  -- For US countries
			INSERT INTO t705_account_pricing
					(c705_account_pricing_id, c704_account_id, c205_part_number_id, c705_unit_price
				   , c705_discount_offered, c202_project_id, c705_created_date, c705_created_by
					)
			 VALUES (s704_price_account.NEXTVAL, p_acc_id, rad_val.ID, DECODE(NVL(p_acct_pricing,'NO'),'YES',rad_val.tier1,0), 0
				   , rad_val.projid, CURRENT_DATE, p_user_id
					);
			END IF;
		END LOOP;

END gm_load_account_pricing;
/
