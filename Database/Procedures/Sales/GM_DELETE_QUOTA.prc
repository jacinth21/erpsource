
--@"c:\database\procedures\sales\gm_delete_quota.prc";
CREATE OR REPLACE PROCEDURE gm_delete_quota (
	p_repid	 IN 	  t709_quota_breakup.c703_sales_rep_id%TYPE
  ,p_year	 IN 	  VARCHAR2
  
  
)
AS
	
	v_yearlen	   NUMBER := NVL (LENGTH (p_year), 0);
BEGIN
	my_context.set_my_inlist_ctx (p_year);
--
	IF v_yearlen > 0
	THEN
		
	DELETE FROM t709_quota_breakup WHERE c703_sales_rep_id =p_repid AND to_char(C709_START_DATE,'YYYY') in (SELECT * FROM v_in_list);	
	
	END IF;



--
END gm_delete_quota;
/
