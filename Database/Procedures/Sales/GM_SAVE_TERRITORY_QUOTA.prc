CREATE OR REPLACE PROCEDURE GM_SAVE_TERRITORY_QUOTA
(
 		  p_TerrId    IN  T702_TERRITORY.C702_TERRITORY_ID%TYPE,
		  p_str 	  IN  VARCHAR2,
		  p_errmsg    OUT	VARCHAR2
)
AS
v_strlen     NUMBER          := NVL(LENGTH(p_str),0);
v_string     VARCHAR2(30000) := p_str;
v_Quota 	 VARCHAR2(20);
v_Start_Date VARCHAR2(20);
v_End_Date VARCHAR2(20);
v_substring  VARCHAR2(1000);
v_msg	     VARCHAR2(1000);
e_others	 EXCEPTION;
v_Id	 	 NUMBER;
v_Fl		 CHAR(1);
v_Quota_Type NUMBER;

BEGIN
 	IF v_strlen > 0
    THEN
   		WHILE INSTR(v_string,'|') <> 0 LOOP
    		  v_substring := SUBSTR(v_string,1,INSTR(v_string,'|')-1);
    		  v_string    := SUBSTR(v_string,INSTR(v_string,'|')+1);
			  v_Start_Date:= NULL;
			  v_End_Date  := NULL;
			  v_Quota	  := NULL;			  
			  v_Quota_Type:= NULL;
			  v_Fl 		  := NULL;
			  v_Id	  	  := NULL;
			  			  
			  v_Start_Date:= SUBSTR(v_substring,1,INSTR(v_substring,'^')-1);
			  v_substring := SUBSTR(v_substring,INSTR(v_substring,'^')+1);			  
			  v_End_Date  := SUBSTR(v_substring,1,INSTR(v_substring,'^')-1);
			  v_substring := SUBSTR(v_substring,INSTR(v_substring,'^')+1);
			  v_Quota     := SUBSTR(v_substring,1,INSTR(v_substring,'^')-1);
			  v_substring := SUBSTR(v_substring,INSTR(v_substring,'^')+1);	
			  v_Quota_Type:= SUBSTR(v_substring,1,INSTR(v_substring,'^')-1);
			  v_substring := SUBSTR(v_substring,INSTR(v_substring,'^')+1);			  		  
			  v_Fl  	  := SUBSTR(v_substring,1,INSTR(v_substring,'^')-1);
			  v_substring := SUBSTR(v_substring,INSTR(v_substring,'^')+1);			  			  
			  v_Id	  	  := TO_NUMBER(v_substring);

			  IF v_Fl = 'I' THEN
				 SELECT T709_TERR_QUOTA_ID.NEXTVAL INTO v_Id FROM dual;
				 
				 INSERT INTO T709_TERR_QUOTA_BREAKUP 
				 		(C709_TERR_QUOTA_ID, 
				 		 C709_QUOTA_BRK_AMT, 
				 		 C709_START_DATE,
						 C709_END_DATE,
						 C709_QUOTA_TYPE,
						 C702_TERRITORY_ID)
					 VALUES
					 	   (v_Id,
						   v_Quota,
						   to_date(v_Start_Date,'mm/dd/yyyy'),
						   to_date(v_End_Date,'mm/dd/yyyy'),
						   v_Quota_Type,
						   p_TerrId);
			  ELSIF v_Fl = 'U' THEN
				 UPDATE T709_TERR_QUOTA_BREAKUP
				 		SET
							C709_QUOTA_BRK_AMT  = v_Quota,
							C709_START_DATE = to_date(v_Start_Date,'mm/dd/yyyy'),
							C709_END_DATE = to_date(v_End_Date,'mm/dd/yyyy'),
							C709_QUOTA_TYPE = v_Quota_Type
						WHERE
							  C709_TERR_QUOTA_ID = v_Id;
			  ELSIF v_Fl = 'D' THEN
			  		DELETE T709_TERR_QUOTA_BREAKUP WHERE C709_TERR_QUOTA_ID = v_Id;
			  END IF;
			  
		END LOOP;
	ELSE
	    v_msg := 'p_str is empty';
		RAISE e_others;
	END IF;
	v_msg := 'Success';
	COMMIT WORK;
    p_errmsg  := v_msg;
EXCEPTION
	  WHEN e_others THEN
	  ROLLBACK WORK;
	  p_errmsg  := SQLERRM;
	  WHEN OTHERS THEN
	  ROLLBACK WORK;
	  p_errmsg  := SQLERRM;
NULL;
END GM_SAVE_TERRITORY_QUOTA;
/

