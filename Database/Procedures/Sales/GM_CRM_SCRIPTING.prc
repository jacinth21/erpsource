/****************************************************************
	* Description : Procedure to save or update surgeon details for CRM
	* Author      : cskumar
*****************************************************************/

CREATE OR REPLACE 
PROCEDURE gm_crm_party_prc
AS
    v_id    NUMBER;
    p_id    NUMBER;
    c_id    NUMBER;
    a_id    NUMBER;
    vp_cnt  NUMBER;
    v_exist NUMBER;
    v_tmpid   NUMBER;
    CURSOR C_CRM_SCRIPTING IS
        SELECT * FROM CRM_SCRIPTING_TEMP WHERE COMPLETION_FL IS NULL;
    -- completion flag Y denotes those records that are already executed
 BEGIN
 
    FOR REC IN C_CRM_SCRIPTING
    LOOP
        BEGIN
            -- setting primary key of current record into a variable so that it can be used outside the loop
            -- in the case of execution breaking out of the loop in the middle
            v_tmpid := REC.CRM_TEMP_ID;
        
            p_id := 0;
            c_id := 0;
            a_id := 0;
            vp_cnt := 1;
            -- p_id, c_id, a_id are empty values sent as the primary ids for T101_party,
            -- T107_contact and T106_address respectively for their procedures to go after
            -- insert script instead of update script.
            -- Initialized to zero for each loop since primary key is an IN/OUT param in their prc
        
            -- v_exist is used to get count of any other surgeon with same name in the database
            v_exist := 0;
            
            -- avoid party name constrain (I101_PARTY_UNIQUE_IDX) by checking if 
            -- a surgeon with the same name already exists.
            SELECT COUNT(*) INTO v_exist
            FROM T101_PARTY 
            WHERE C101_FIRST_NM = REC.C101_FIRST_NM
            AND C101_LAST_NM = REC.C101_LAST_NM
            AND NVL(C101_MIDDLE_INITIAL,'-9999') = NVL(REC.C101_MIDDLE_INITIAL,'-9999') AND C901_PARTY_TYPE = REC.C901_PARTY_TYPE;
            
            IF (v_exist = 0) THEN
                UPDATE CRM_SCRIPTING_TEMP SET COMPLETION_FL = 'Y', SCRIPTING_DATE = SYSDATE, ERROR = 'E' WHERE CRM_TEMP_ID = REC.CRM_TEMP_ID;
            ELSE
                -- update the temp table record's partyid field with the existing partyid.
                SELECT C101_PARTY_ID INTO v_id
                FROM T101_PARTY 
                WHERE C101_FIRST_NM = REC.C101_FIRST_NM
                AND C101_LAST_NM = REC.C101_LAST_NM
                AND NVL(C101_MIDDLE_INITIAL,'-9999') = NVL(REC.C101_MIDDLE_INITIAL,'-9999') AND C901_PARTY_TYPE = REC.C901_PARTY_TYPE;

                UPDATE CRM_SCRIPTING_TEMP SET C101_PARTY_ID = v_id
                WHERE CRM_TEMP_ID = REC.CRM_TEMP_ID;
                -- execute only if npi data is provided
                IF(REC.C6600_SURGEON_NPI_ID IS NOT NULL) THEN
                    gm_pkg_crm_surgeon.gm_sav_partysurgeon(v_id, REC.C6600_SURGEON_NPI_ID, REC.C6600_SURGEON_ASSISTANT_NM, REC.C6600_SURGEON_MANAGER_NM, REC.C6600_VOID_FL, REC.SCRIPTING_USER);
                END IF;

                -- execute only if contact data is provided
                IF(REC.C107_CONTACT_VALUE IS NOT NULL) THEN
                    -- voiding existing primary contact if inserting a primary entry
                    IF(REC.C107_PRIMARY_FL IS NOT NULL) THEN
                        UPDATE T107_CONTACT SET C107_VOID_FL = 'Y', C107_LAST_UPDATED_BY = REC.SCRIPTING_USER, C107_LAST_UPDATED_DATE = SYSDATE WHERE C101_PARTY_ID = v_id AND C107_VOID_FL IS NULL AND C107_PRIMARY_FL = 'Y' AND NVL(C107_CONTACT_TYPE,'-9999') = NVL(REC.C107_CONTACT_TYPE,'-9999') AND C901_MODE = REC.C901_MODE;
                    END IF;

                    gm_pkg_crm_contact.gm_sav_contactinfo(c_id, v_id, REC.C901_MODE, REC.C107_CONTACT_TYPE, REC.C107_CONTACT_VALUE, REC.C107_SEQ_NO, REC.C107_PRIMARY_FL, REC.C107_INACTIVE_FL, REC.SCRIPTING_USER, REC.C107_VOID_FL);
                END IF;

                -- execute only if address data is provided
                IF(REC.C106_ADD1 IS NOT NULL) THEN
                    -- unset existing primary address if inserting a primary entry
                    IF(REC.C106_PRIMARY_FL IS NOT NULL) THEN

                        -- few existing records in db have empty zipcode.
                        -- setting zipcode of those existing records to '-', if null since zip is mandatory in address update trigger
                        -- only then we can void existing primary address flag
                        UPDATE T106_ADDRESS SET C106_ZIP_CODE = '-', C106_LAST_UPDATED_BY = REC.SCRIPTING_USER, C106_LAST_UPDATED_DATE = SYSDATE WHERE C101_PARTY_ID = v_id AND C106_VOID_FL IS NULL AND C106_PRIMARY_FL = 'Y' AND C901_ADDRESS_TYPE = REC.C901_ADDRESS_TYPE AND C106_ZIP_CODE IS NULL;

                        UPDATE T106_ADDRESS SET C106_VOID_FL = 'Y', C106_LAST_UPDATED_BY = REC.SCRIPTING_USER, C106_LAST_UPDATED_DATE = SYSDATE WHERE C101_PARTY_ID = v_id AND C106_VOID_FL IS NULL AND C106_PRIMARY_FL = 'Y' AND C901_ADDRESS_TYPE = REC.C901_ADDRESS_TYPE;

                    END IF;

                    gm_pkg_crm_address.gm_crm_sav_addressinfo(v_id, a_id, REC.C901_ADDRESS_TYPE, REC.C106_ADD1, REC.C106_ADD2, REC.C106_CITY, REC.C901_STATE, REC.C901_COUNTRY, REC.C106_ZIP_CODE, REC.C106_SEQ_NO, REC.SCRIPTING_USER, NULL, REC.C106_PRIMARY_FL, REC.C901_CARRIER, REC.C106_VOID_FL);
                END IF;

                -- marking the record as completed, only after all details are executed.
                -- This won't get executed again on recursive call.
                UPDATE CRM_SCRIPTING_TEMP SET COMPLETION_FL = 'Y', SCRIPTING_DATE = SYSDATE WHERE CRM_TEMP_ID = REC.CRM_TEMP_ID;
            END IF;
            
            COMMIT;
        END;
    END LOOP;
    
    EXCEPTION WHEN OTHERS THEN 
        DBMS_OUTPUT.put_line (' Error for Temp ID --> ' || v_tmpid || ' -- PartyID (S101_PARTY) --> ' || p_id ||' Error Reason--> ' || SQLERRM);
        -- incase of execution failure, we mark the record as completed and set error flag
        -- so that it won't get executed again on recursive call and for testing purpose.
        UPDATE CRM_SCRIPTING_TEMP SET COMPLETION_FL = 'Y', 
        SCRIPTING_DATE = SYSDATE, ERROR = 'Y' 
        WHERE CRM_TEMP_ID = v_tmpid;
        
        -- committing at exception block incase the last record / series of records are having error
        COMMIT;
        -- making the prc recursive to complete only after execution of all records in CRM_SCRIPTING_TEMP
        gm_crm_party_prc();
    
END gm_crm_party_prc;
/