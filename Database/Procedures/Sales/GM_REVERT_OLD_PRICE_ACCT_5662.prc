/* Formatted on 2008/01/08 11:16 (Formatter Plus v4.8.0) */
--@"c:\database\procedures\sales\GM_REVERT_OLD_PRICE_ACCT_5662.prc";
CREATE OR REPLACE PROCEDURE GM_REVERT_OLD_PRICE_ACCT_5662 
AS

	CURSOR old_price_cur
	IS
		
		select  t705.c705_account_pricing_id idn, t705.c205_part_number_id parnumid
		, t705.c705_unit_price currprice, t941.c941_value oldvalue , c941_updated_date updatedate
		from  t705_account_pricing t705,t941_audit_trail_log t941
		where t705.c704_account_id='5662'
		and trunc(t705.c705_last_updated_date) = to_date('03/09/2011','MM/DD/YYYY')
		and t705.c705_last_updated_by = '303212'
		and t941.c941_ref_id = t705.c705_account_pricing_id
		and t941.c940_audit_trail_id='1019'
		--and t705.c205_part_number_id='124.051'
		and t941.c941_updated_date is not null
		and t941.c941_updated_date = (select max(t941a.c941_updated_date) from  t705_account_pricing t705a,t941_audit_trail_log t941a
                              where t705a.c704_account_id='5662'
                              and trunc(t705a.c705_last_updated_date) = to_date('03/09/2011','MM/DD/YYYY')
                              --and t705a.c705_last_updated_by = '303212'
                              and t941a.c941_ref_id = t705a.c705_account_pricing_id
                              and t941a.c940_audit_trail_id='1019'
                              and t705.c205_part_number_id = t705a.c205_part_number_id
                              --and t705a.c205_part_number_id='124.051'
                              and t941a.c941_updated_date is not null
                              and trunc(t941a.c941_updated_date) < to_date('03/09/2011','MM/DD/YYYY'))
	        order by t705.c205_part_number_id;
BEGIN
	
		FOR old_price IN old_price_cur
		LOOP
			update t705_account_pricing
			set c705_old_price=old_price.oldvalue
			where c704_account_id='5662' 
			and c205_part_number_id=old_price.parnumid;
			
		END LOOP;
		commit;
END GM_REVERT_OLD_PRICE_ACCT_5662;
/