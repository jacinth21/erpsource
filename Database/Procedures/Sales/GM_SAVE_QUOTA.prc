create or replace
PROCEDURE gm_save_quota (
	p_repid	   IN 	  t709_quota_breakup.c703_sales_rep_id%TYPE
  , p_str	   IN		VARCHAR2
  , p_errmsg   OUT		VARCHAR2
)
AS
	v_strlen	   NUMBER := NVL (LENGTH (p_str), 0);
	v_string	   VARCHAR2 (9000) := p_str;
	v_quota 	   NUMBER (15, 2);
	v_month 	   VARCHAR2 (20);
	v_start_date   VARCHAR2 (20);
	v_end_date	   DATE;
	v_substring    VARCHAR2 (1000);
	v_msg		   VARCHAR2 (1000);
	e_others	   EXCEPTION;
	v_id		   NUMBER;
	v_fl		   CHAR (1);
	v_quota_type   NUMBER;
	v_cnt		   NUMBER;
	v_year		   VARCHAR2 (4);
	v_compid       NUMBER ;
	v_comptxn_curr NUMBER;
	v_rept_curr    NUMBER;
	v_curr_from    varchar2(10);
	v_curr_rate    NUMBER(15,4);
	v_quota_base   NUMBER (15, 2);

BEGIN
	IF v_strlen > 0
	THEN


		BEGIN
			SELECT C1900_COMPANY_ID
			  INTO v_compid
			  FROM T703_SALES_REP WHERE C703_SALES_REP_ID=p_repid;

			SELECT c901_txn_currency, c901_reporting_currency
			  INTO v_comptxn_curr, v_rept_curr
			  FROM t1900_company
			 WHERE c1900_company_id = v_compid;
			EXCEPTION
				WHEN NO_DATA_FOUND THEN
				 v_comptxn_curr := '1';
				 v_rept_curr := '1';
		END;


		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			--
			v_month 	:= NULL;
			v_quota 	:= NULL;
			v_month 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_year		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_quota 	:= v_substring;
			--
			v_start_date := v_month || '/01/' || v_year;

			--
			SELECT COUNT (*)
			  INTO v_cnt
			  FROM t709_quota_breakup
			 WHERE c703_sales_rep_id = p_repid
			   AND TO_CHAR (c709_start_date, 'YYYY') = v_year
			   AND TO_CHAR (c709_start_date, 'MM') = v_month
			   ;

				
				SELECT GET_CURRENCY_CONVERSION (v_comptxn_curr,v_rept_curr,current_date,1,101103)
	         	INTO v_curr_rate
	         	FROM DUAL;

	         	-- Convert quota from Original value to Base Currency. User want to have two digit rounding.
	         	v_quota_base := ROUND(v_quota*v_curr_rate,2);
	         	
		     
			IF v_cnt = 0
			THEN
				--
				SELECT LAST_DAY (TO_DATE (v_start_date, 'MM/DD/YYYY'))
				  INTO v_end_date
				  FROM DUAL;

				INSERT INTO t709_quota_breakup
							(c709_quota_breakup_id, c703_sales_rep_id, c709_start_date, c709_end_date
						   , c709_quota_breakup_amt, c901_type , c709_quota_breakup_amt_base
							)
					 VALUES (t709_terr_quota_id.NEXTVAL, p_repid, TO_DATE (v_start_date, 'mm/dd/yyyy'), v_end_date
						   , v_quota, 20750 , v_quota_base
							);
			ELSE
				UPDATE t709_quota_breakup
				   SET c709_quota_breakup_amt = v_quota
				      ,c709_quota_breakup_amt_base = v_quota_base
				 WHERE c709_start_date = TO_DATE (v_start_date, 'mm/dd/yyyy')
				 AND c703_sales_rep_id = p_repid ;
			END IF;

		--
		END LOOP;
	ELSE
		v_msg		:= 'p_str is empty';
		RAISE e_others;
	END IF;

	v_msg		:= 'Success';
	COMMIT WORK;
	p_errmsg	:= v_start_date;
EXCEPTION
	WHEN e_others
	THEN

		ROLLBACK WORK;
		p_errmsg	:= SQLERRM;
	WHEN OTHERS
	THEN

		ROLLBACK WORK;
		p_errmsg	:= SQLERRM;
		NULL;
END gm_save_quota;
/