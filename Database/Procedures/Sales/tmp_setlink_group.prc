/* Formatted on 2009/03/25 18:05 (Formatter Plus v4.8.0) */
CREATE OR REPLACE PROCEDURE tmp_setlink_group
AS
	v_set_id	   t207_set_master.c207_set_id%TYPE;
	v_set_rmn_implant_id t207_set_master.c207_set_id%TYPE;
	v_set_rmn_ins_id t207_set_master.c207_set_id%TYPE;
	v_parentsetid  t207_set_master.c207_set_id%TYPE;
	v_parentsetdesc t207_set_master.c207_set_nm%TYPE;

	CURSOR cur_sales_group
	IS
		SELECT	 v207.c207_set_id ID
			   , NVL (SUBSTR (v207.c207_set_nm, 1, INSTR (v207.c207_set_nm, '(') - 1)
					, regexp_replace (v207.c207_set_nm, '.?reg;')
					 ) NAME
			   , v207.c207_set_nm
			FROM t730_virtual_consignment t730
			   , v207a_set_consign_link v207
			   , (SELECT DISTINCT ad_id, ad_name, region_id, region_name, d_id, d_name, v700.d_active_fl
							 FROM v700_territory_mapping_detail v700) v700
		   WHERE t730.c207_set_id = v207.c207_actual_set_id AND v700.d_id = t730.c701_distributor_id
		GROUP BY v207.c207_set_id, v207.c207_set_nm, v207.c207_seq_no
		ORDER BY v207.c207_set_id;
BEGIN
	FOR set_val IN cur_sales_group
	LOOP
		v_parentsetid := set_val.ID;
		v_parentsetdesc := set_val.NAME;

		SELECT 'SET.' || s207_set_master.NEXTVAL
		  INTO v_set_id
		  FROM DUAL;

		SELECT 'RMN.' || s207_set_master.NEXTVAL
		  INTO v_set_rmn_implant_id
		  FROM DUAL;

		SELECT 'RMN.' || s207_set_master.NEXTVAL
		  INTO v_set_rmn_ins_id
		  FROM DUAL;

		INSERT INTO t207_set_master
					(c207_set_id, c207_set_nm, c207_set_desc, c202_project_id
				   , c207_created_date, c207_created_by, c207_last_updated_date, c207_last_updated_by, c207_category
				   , c207_type, c207_skip_rpt_fl, c901_set_grp_type, c207_seq_no, c207_void_fl, c207_obsolete_fl
				   , c901_cons_rpt_id, c901_vanguard_type, c901_status_id, c207_rev_lvl, c207_core_set_fl
					)
			 VALUES (v_set_id, v_parentsetdesc || ' Set Consignment', v_parentsetdesc || ' Set Consignment', NULL
				   , SYSDATE, '30301', NULL, NULL, 4060
				   , NULL, NULL, 1604, 3, NULL, NULL
				   , 20101, NULL, NULL, NULL, NULL
					);

		INSERT INTO t207_set_master
					(c207_set_id, c207_set_nm, c207_set_desc, c202_project_id, c207_created_date
				   , c207_created_by, c207_last_updated_date, c207_last_updated_by, c207_category, c207_type
				   , c207_skip_rpt_fl, c901_set_grp_type, c207_seq_no, c207_void_fl, c207_obsolete_fl
				   , c901_cons_rpt_id, c901_vanguard_type, c901_status_id, c207_rev_lvl, c207_core_set_fl
					)
			 VALUES (v_set_rmn_implant_id, v_parentsetdesc || ' Implant', v_parentsetdesc || ' Implant', NULL, SYSDATE
				   , '30301', NULL, NULL, 4060, NULL
				   , NULL, 1605, NULL, NULL, NULL
				   , NULL, NULL, NULL, NULL, NULL
					);

		INSERT INTO t207_set_master
					(c207_set_id, c207_set_nm, c207_set_desc, c202_project_id
				   , c207_created_date, c207_created_by, c207_last_updated_date, c207_last_updated_by, c207_category
				   , c207_type, c207_skip_rpt_fl, c901_set_grp_type, c207_seq_no, c207_void_fl, c207_obsolete_fl
				   , c901_cons_rpt_id, c901_vanguard_type, c901_status_id, c207_rev_lvl, c207_core_set_fl
					)
			 VALUES (v_set_rmn_ins_id, v_parentsetdesc || ' Instruments', v_parentsetdesc || ' Instruments', NULL
				   , SYSDATE, '30301', NULL, NULL, 4061
				   , NULL, NULL, 1605, NULL, NULL, NULL
				   , NULL, NULL, NULL, NULL, NULL
					);

		INSERT INTO t207a_set_link
					(t207a_set_link_id, c207_main_set_id, c207_link_set_id, c901_type
					)
			 VALUES (s207a_set_link.NEXTVAL, v_set_id, v_set_rmn_implant_id, 20003
					);

		INSERT INTO t207a_set_link
					(t207a_set_link_id, c207_main_set_id, c207_link_set_id, c901_type
					)
			 VALUES (s207a_set_link.NEXTVAL, v_set_id, v_set_rmn_ins_id, 20003
					);

		INSERT INTO t207a_set_link
					(t207a_set_link_id, c207_main_set_id, c207_link_set_id, c901_type
					)
			 VALUES (s207a_set_link.NEXTVAL, v_parentsetid, v_set_id, 20002
					);
	END LOOP;
END tmp_setlink_group;
/
