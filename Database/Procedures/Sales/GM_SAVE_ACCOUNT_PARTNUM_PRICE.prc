/* Formatted on 12/15/2009 14:12 (Formatter Plus v4.8.0) */
-- @"C:\database\Procedures\Sales\gm_save_account_partnum_price.prc";
/*
 * Common Procesdure to save Account Price, can be called using account id or Party ID
 * 
 * */
CREATE OR REPLACE PROCEDURE gm_save_account_partnum_price (
	p_price 	 IN   t705_account_pricing.c705_unit_price%TYPE
  , p_discount	 IN   t705_account_pricing.c705_discount_offered%TYPE
  , p_partnum	 IN   t705_account_pricing.c205_part_number_id%TYPE
  , p_accid 	 IN   t705_account_pricing.c704_account_id%TYPE
  , p_userid	 IN   t705_account_pricing.c705_last_updated_by%TYPE
  , p_type		 IN   VARCHAR2
  , p_validate_pnum IN VARCHAR2  DEFAULT 'YES'
)
AS
	v_party_id   t705_account_pricing.c101_party_id%TYPE;
	v_accid   t705_account_pricing.c704_account_id%TYPE;


BEGIN
	v_accid:=p_accid;
	
	
	IF p_type = 'Account'
	THEN
			BEGIN
				SELECT C101_PARTY_ID
					INTO v_party_id
				FROM t704_account
				WHERE c704_account_id = v_accid
				AND C704_VOID_FL IS NULL;
			
				v_accid:=v_party_id;
			EXCEPTION WHEN NO_DATA_FOUND	
			THEN
				raise_application_error ('-20670', '');
			 END;
	END IF;
	--Save Price based on Party ID
	gm_pkg_sm_party_price.gm_sav_acct_party_price(v_accid,p_price,p_discount,p_partnum,p_validate_pnum,p_userid);
	
END gm_save_account_partnum_price;
/
