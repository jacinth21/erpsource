-- @"C:\database\Procedures\Sales\GM_SAV_ACCOUNT_SHIP_SETUP.prc";

CREATE OR REPLACE PROCEDURE gm_sav_account_ship_setup (
	p_accid					IN	t704_account.c704_account_id%TYPE
  , p_attn					IN	t704_account.c704_ship_attn_to%TYPE
  , p_acct_ship_name		IN	t704_account.c704_ship_name%TYPE
  , p_acct_ship_add1		IN	t704_account.c704_ship_add1%TYPE
  , p_acct_ship_add2		IN	t704_account.c704_ship_add2%TYPE
  , p_acct_ship_city		IN	t704_account.c704_ship_city%TYPE
  , p_acct_ship_state		IN	t704_account.c704_ship_state%TYPE
  , p_acct_ship_country		IN	t704_account.c704_ship_country%TYPE
  , p_acct_shipzip			IN	t704_account.c704_ship_zip_code%TYPE
  , p_userid				IN	t704_account.c704_last_updated_by%TYPE
  , p_prefcarrier				IN	t704_account.c901_carrier%TYPE
)
AS
v_party_id t704_account.c101_party_id%TYPE;
v_acct_ship_add1	t704_account.c704_ship_add1%TYPE;
v_acct_ship_city	t704_account.c704_ship_city%TYPE;
v_acct_ship_state    t704_account.c704_ship_state%TYPE;
v_acct_ship_country  t704_account.c704_ship_country%TYPE;
v_acct_shipzip		t704_account.c704_ship_zip_code%TYPE;
v_acct_shipaddfl	t704a_account_attribute.c704a_attribute_value%TYPE;

BEGIN
	BEGIN
     SELECT c101_party_id, c704_ship_add1, c704_ship_city
      , c704_ship_state, c704_ship_country, c704_ship_zip_code
      , get_account_attrb_value (c704_account_id, 101191)
       INTO v_party_id, v_acct_ship_add1, v_acct_ship_city
      , v_acct_ship_state, v_acct_ship_country, v_acct_shipzip
      , v_acct_shipaddfl
       FROM t704_account
      WHERE c704_account_id = p_accid;
	EXCEPTION
	WHEN NO_DATA_FOUND THEN
	    v_party_id := NULL;
	END;
    --
	UPDATE t704_account
	SET c704_ship_attn_to = p_attn
		, c704_ship_name = p_acct_ship_name
		, c704_ship_add1 = p_acct_ship_add1
		, c704_ship_add2 = p_acct_ship_add2
		, c704_ship_city = p_acct_ship_city
		, c704_ship_state = p_acct_ship_state
		, c704_ship_country = p_acct_ship_country
		, c704_ship_zip_code = p_acct_shipzip
		, c704_last_updated_by = p_userid
		, c704_last_updated_date = CURRENT_DATE
		, c901_carrier = p_prefcarrier  -- Updating preffered Carrier value for the particuler account
	WHERE c101_party_id = v_party_id
	AND C704_VOID_FL IS NULL;
	-- Check the country - US
	IF p_acct_ship_country = 1101 THEN
	    -- to compare the shipping address values - if not equal then setting the attribute value as null.
	    IF (v_acct_ship_add1                         <> p_acct_ship_add1 OR v_acct_ship_city <> p_acct_ship_city OR v_acct_ship_state <>
	        p_acct_ship_state OR v_acct_ship_country <> p_acct_ship_country OR v_acct_shipzip <> p_acct_shipzip) THEN
	        --
	        v_acct_shipaddfl := NULL;
	    END IF;
	ELSE
	    v_acct_shipaddfl := NULL;
	END IF;
	-- to update the account attribute value
	gm_pkg_sm_acct_trans.gm_sav_acct_attr (p_accid, v_acct_shipaddfl, 101191, p_userid) ;
END gm_sav_account_ship_setup;
/