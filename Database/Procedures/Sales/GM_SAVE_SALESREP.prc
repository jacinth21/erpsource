/* Formatted on 2010/07/21 11:31 (Formatter Plus v4.8.0) */
--@"c:\database\procedures\sales\gm_save_salesrep.prc";

CREATE OR REPLACE PROCEDURE gm_save_salesrep (
	p_firstnm	   IN		t101_party.c101_first_nm%TYPE
  , p_lastnm	   IN		t101_party.c101_last_nm%TYPE
  , p_repnmen      IN       t703_sales_rep.c703_sales_rep_name_en%TYPE
  , p_distid	   IN		t703_sales_rep.c701_distributor_id%TYPE
  , p_category	   IN		t703_sales_rep.c703_rep_category%TYPE
  , p_salesrepid   IN OUT	VARCHAR2
  , p_userid	   IN		t703_sales_rep.c703_created_by%TYPE
  , p_action	   IN		VARCHAR2
  , p_terrid	   IN		t703_sales_rep.c702_territory_id%TYPE
  , p_activefl	   IN		t703_sales_rep.c703_active_fl%TYPE
  , p_partyid	   IN		t703_sales_rep.c101_party_id%TYPE
  , p_startdt	   IN		t703_sales_rep.c703_start_date%TYPE
  , p_enddt 	   IN		t703_sales_rep.c703_end_date%TYPE
  , p_dsgn		   IN		t703_sales_rep.c901_designation%TYPE
  , p_username	   IN		t102_user_login.c102_login_username%TYPE
  , p_createlogin   IN		VARCHAR2
  , p_primarydivid  IN		t1910_division.c1910_division_id%TYPE
)
AS

--
	v_terrid	   NUMBER;
	s_partyid	   NUMBER;
	sout_party_id  VARCHAR2 (20) := p_partyid;
	v_usr_name	   VARCHAR2 (100);
	v_accs_lvl_id  NUMBER;
	v_user_type    NUMBER;
	v_dsgn		   NUMBER;
	v_user_status  NUMBER;
	v_auth_type    NUMBER;
	v_sales_rep_id t703_sales_rep.c703_created_by%TYPE;
	v_username VARCHAR2 (100) := p_username;
	v_count 	   NUMBER;
	v_contact_mailid t107_contact.c107_contact_value%TYPE;
--
BEGIN
--
	v_sales_rep_id := p_salesrepid;

	SELECT DECODE (sout_party_id, 0, NULL, sout_party_id)
	  INTO sout_party_id
	  FROM DUAL;

	SELECT DECODE (p_dsgn, 0, NULL, p_dsgn)
	  INTO v_dsgn
	  FROM DUAL;

	--
	IF p_terrid = 0
	THEN
		v_terrid	:= NULL;
	ELSE
		v_terrid	:= p_terrid;
	END IF;

	--
	IF p_activefl = 'Y'
	THEN
		v_user_status := 311;
	ELSE
		v_user_status := 314;
	END IF;
	
	IF NVL(p_createlogin,'N') = 'Y' THEN
	
	BEGIN	
		SELECT c107_contact_value INTO v_contact_mailid
		    FROM t107_contact
				 WHERE c101_party_id = p_partyid
				 AND c901_mode = '90452'
				 AND c107_primary_fl = 'Y'
				 AND C107_INACTIVE_FL IS NULL;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			v_contact_mailid := NULL;				 
	END;
	IF v_contact_mailid IS NULL 
			THEN
			raise_application_error (-20339, '');	
		END IF;
	END IF;
	gm_save_salesrep_table (p_firstnm
						  , p_lastnm
						  , p_repnmen 
						  , p_distid
						  , p_category
						  , p_salesrepid
						  , p_userid
						  , v_terrid
						  , p_activefl
						  , sout_party_id
						  , p_startdt
						  , p_enddt
						  , v_dsgn
						  , NULL
						  , p_primarydivid
						  , p_salesrepid
						   );

	IF (p_category = 4020)
	THEN   --Distributor Rep
		v_accs_lvl_id := 1;
		v_user_type := 302;
		v_auth_type := 321;
	ELSIF (p_category = 40029)
	THEN   --Distributor Principal
		v_accs_lvl_id := 2;
		v_user_type := 303;
		v_auth_type := 321;
	END IF;
	-- for Sales Rep Setup screen , If no direct sales rep or login credentials is true then function call.
	IF p_category <> 4021  THEN
		gm_pkg_it_login.gm_save_user (p_salesrepid
								, p_firstnm
								, p_firstnm
								, p_lastnm
								, NVL(v_contact_mailid , 'djames@globusmedical.com')
								, 2005
								, v_accs_lvl_id
								, p_userid
								, v_user_status
								, v_user_type
								, sout_party_id
								, 7005
								, p_activefl
								, NULL
								, sout_party_id
								, p_salesrepid
								 );
	END IF;

	UPDATE t101_user
	   SET c701_distributor_id = p_distid
	    ,c101_last_updated_date = CURRENT_DATE
	 	,c101_last_updated_by = p_userid
	 WHERE c101_user_id = p_salesrepid;
	 	 

	SELECT SUBSTR (p_firstnm, 0, 1) || p_lastnm
	  INTO v_usr_name
	  FROM DUAL;

	UPDATE t703_sales_rep
	   SET c101_party_id = sout_party_id
	   ,c703_last_updated_date = CURRENT_DATE
	   ,c703_last_updated_by = p_userid
	 WHERE c703_sales_rep_id = p_salesrepid;

	 
	IF p_category <> 4021  AND NVL(p_createlogin,'N') = 'Y' THEN
		
		IF p_username IS NULL THEN
			v_username := v_usr_name;
		END IF;
		
		-- PMT-28843 : Currently all the sales rep created/mapped in ADS.
			--So, to set the default values LDAP and Authentication type values.
			--1 LDAP - Globus (DEFAULT_LDAP)
			--2 Auth Type - ADS (320)
				
		gm_pkg_it_login.gm_save_user_login (p_salesrepid, v_username, 'Globus!234', 320, get_rule_value ('DEFAULT_LDAP', 'LDAP'));
	END IF;
	
	-- Updating firstname, lastname of direct sales rep in party and user tables 
	IF p_category = 4021 THEN
	
		UPDATE t101_party
		   SET c101_first_nm = p_firstnm
			 , c101_last_nm = p_lastnm
			 , c101_last_updated_by = p_userid
			 , c101_last_updated_date = CURRENT_DATE
		 WHERE c101_party_id = p_partyid;
		 
		UPDATE t101_user
           SET c101_user_f_name = p_firstnm
             , c101_user_l_name = p_lastnm
             , c101_last_updated_by = p_userid
             , c101_last_updated_date = CURRENT_DATE
         WHERE c101_user_id = p_salesrepid;
         
		
	END IF;
	--Inactivate Sales Rep 
	IF p_activefl <> 'Y' THEN
		gm_pkg_it_user.gm_sav_inactive_user(p_salesrepid);
		
		UPDATE t106_address
		   SET c106_inactive_fl = 'Y'
			 , c106_last_updated_by = p_userid
			 , c106_last_updated_date = CURRENT_DATE
		 WHERE c101_party_id = p_partyid;
	END IF;
--
END gm_save_salesrep;
/
