/* Formatted on 2011/08/04 16:00 (Formatter Plus v4.8.0) */
--@"c:\database\procedures\sales\gm_pricing_remove_systems.prc";

CREATE OR REPLACE PROCEDURE gm_pricing_remove_systems (
	p_requestid 	  IN	   t7000_account_price_request.c7000_account_price_request_id%TYPE
)
AS
	CURSOR groupid_details
		IS
			SELECT t4010.c4010_group_id groupid
			  FROM t4010_group t4010
			 WHERE t4010.c207_set_id IN (SELECT MY_TEMP_TXN_ID FROM MY_TEMP_LIST)
			   AND c4010_void_fl IS NULL
			   AND c901_type = 40045
			   AND c901_group_type IS NOT NULL
			   AND c4010_inactive_fl IS NULL
			   AND t4010.c4010_publish_fl IS NOT NULL;
BEGIN
	
	--get the group ids for the system ids passed
		FOR currindex1 IN groupid_details
		LOOP
			UPDATE t7001_group_part_pricing t7001 SET c7001_void_fl = 'Y'
			 WHERE t7001.c7000_account_price_request_id = p_requestid AND t7001.c7001_ref_id = currindex1.groupid;
		END LOOP;

END gm_pricing_remove_systems;
/
