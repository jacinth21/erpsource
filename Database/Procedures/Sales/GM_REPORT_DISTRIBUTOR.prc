CREATE OR REPLACE PROCEDURE GM_REPORT_DISTRIBUTOR
(	p_message   OUT VARCHAR2,
	p_recordset OUT Types.cursor_type)
AS
/****************************************************************************
 * Description           :This procedure is called to get distributor list
 * Parameters            :p_column
 *                       :p_recordset
 ****************************************************************************/
v_str VARCHAR2(2000);
BEGIN
--
   	OPEN p_recordset FOR
		SELECT	C701_DISTRIBUTOR_ID ID,
				 C701_DISTRIBUTOR_NAME NAME,
				C701_DIST_SH_NAME
		FROM	T701_DISTRIBUTOR
		ORDER BY NAME;
EXCEPTION
WHEN OTHERS THEN
	p_message := '1000';
END GM_REPORT_DISTRIBUTOR;
/

