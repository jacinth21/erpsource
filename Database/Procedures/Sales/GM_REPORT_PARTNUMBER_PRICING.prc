CREATE OR REPLACE PROCEDURE GM_REPORT_PARTNUMBER_PRICING
(
 	   p_column    IN  VARCHAR2,
 	   p_ProjId    IN  VARCHAR2,
	   p_message   OUT VARCHAR2,
	   p_recordset OUT Types.cursor_type)
AS
/*
      Description           :This procedure is called for Vendor Reporting
      Parameters            :p_column
                            :p_recordset
*/
v_str VARCHAR2(2000);
v_company_id  VARCHAR2(20);
BEGIN
	
	SELECT  get_compid_frm_cntx()
    INTO    v_company_id
    FROM    DUAL;
    
   	OPEN p_recordset FOR
		SELECT	t205.C205_PART_NUMBER_ID ID,REGEXP_REPLACE(C205_PART_NUM_DESC,'[^0-9A-Za-z]', ' ') PDESC,
				t2052.C2052_EQUITY_PRICE EPRICE, t2052.C2052_LOANER_PRICE LPRICE,
				t2052.C2052_CONSIGNMENT_PRICE CPRICE, t2052.C2052_LIST_PRICE LIPRICE
		FROM
			 T205_PART_NUMBER t205, t2052_part_price_mapping t2052, (
         SELECT c205_part_number_id, c202_project_id
           FROM t2021_part_project_mapping
          WHERE c202_project_id = p_ProjId
            AND c2021_void_fl  IS NULL
    )
    t2021
	    WHERE C205_ACTIVE_FL = 'Y'
	    AND t205.c205_part_number_id = t2052.c205_part_number_id(+) 
	    AND t205.c205_part_number_id  = t2021.c205_part_number_id(+)
    	AND (t2021.c202_project_id    = p_ProjId
    	OR t205.C202_PROJECT_ID       = p_ProjId)
		AND t2052.c1900_company_id(+) = v_company_id
		AND t2052.c2052_void_fl(+) is NULL
		ORDER BY DECODE(p_column,'ID',t205.C205_PART_NUMBER_ID,'PDESC',C205_PART_NUM_DESC);
	 EXCEPTION
	 WHEN OTHERS THEN
	          p_message := SQLERRM;
END GM_REPORT_PARTNUMBER_PRICING;
/

