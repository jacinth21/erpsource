--@"C:\Database\Procedures\Common\GM_COM_CREATE_EMAIL_CONNECTION.prc";
/***************************************************************************************************
* Description : This procedure used to detect the email protocol for the emails sending from Oracle
* Author	  : Karthik
****************************************************************************************************/

CREATE OR REPLACE PROCEDURE GM_COM_CREATE_EMAIL_CONNECTION
(
  p_conn 	  		IN OUT  utl_smtp.connection
)
AS
 v_host     		VARCHAR2(200) := get_rule_value('EMAIL_HOST','EMAIL_DETAILS');
 
 -- wallet created from output of openssl cmd
 -- openssl s_client -host email-smtp.us-east-1.amazonaws.com -port 587 -prexit -showcerts -crlf -starttls smtp
 v_wallet_loc      VARCHAR2(200) := get_rule_value('WALLET_LOC','EMAIL_DETAILS');
 v_wallet_pwd      VARCHAR2(200)  := get_rule_value('WALLET_PASSWORD','EMAIL_DETAILS');
 
 -- SES API Username and Password for AWS SES
 v_user_name       VARCHAR2(200) := get_rule_value('AWS_USERNAME','EMAIL_DETAILS');
 v_user_pwd        VARCHAR2(200) := get_rule_value('AWS_PASSWORD','EMAIL_DETAILS');
 v_conn 	       utl_smtp.connection := p_conn;
 v_email_protocol  VARCHAR2(20);
 --
BEGIN
	SELECT get_rule_value('EMAIL_PROTOCOL','EMAIL_DETAILS') 
		INTO v_email_protocol 
		 FROM DUAL;
		 
	IF v_email_protocol = 'AWS' THEN 	 
		 -- Make a secure connection using the SSL port configured with your SMTP server
			BEGIN
			 v_conn  := utl_smtp.open_connection
			 	(
			 		host                          => v_host,
				 	port                          => get_rule_value('PORT','EMAIL_DETAILS'),
				    wallet_path                   => v_wallet_loc,
				    wallet_password               => v_wallet_pwd,
				    secure_connection_before_smtp => FALSE
			    );
			EXCEPTION WHEN OTHERS THEN
			  dbms_output.put_line('Error in creating connection: '|| sqlerrm);
			  RAISE;
			END;
		 
		 -- Starting TLS
			BEGIN
			    utl_smtp.starttls(v_conn);
			EXCEPTION WHEN OTHERS THEN
			  dbms_output.put_line('Error in starting TLS: '|| sqlerrm);
			  RAISE;
			END;
		
			
		  -- Call the Auth procedure to authorized a user for access to the mail server
		  -- Schemes should be set appropriatelty for mail server
			BEGIN
			    utl_smtp.AUTH(
			        c        => v_conn,
			        username => v_user_name,
			        password => v_user_pwd,
			        schemes  => 'LOGIN'
			     );
				EXCEPTION WHEN OTHERS THEN
				  dbms_output.put_line('Error in authentication: '|| sqlerrm);
				  raise;
			END;
	 ELSE
		  v_conn  := UTL_SMTP.OPEN_CONNECTION(v_host);
	 END IF;
	 
	UTL_SMTP.helo (v_conn, v_host);
	p_conn := v_conn;
	
END GM_COM_CREATE_EMAIL_CONNECTION;
/

