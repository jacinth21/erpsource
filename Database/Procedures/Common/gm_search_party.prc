/* Formatted on 2008/11/24 12:54 (Formatter Plus v4.8.0) */
--@"c:\Database\Procedures\Common\gm_search_party.prc"

CREATE OR REPLACE PROCEDURE gm_search_party (
	p_search_str	IN		 VARCHAR2
  , p_search_type	IN		 VARCHAR2	-- not used now, will contain the id and search type ( '=' or 'LIKE' or 'IN' etc.,)
  , p_recordset 	OUT 	 TYPES.cursor_type
)
AS
/*****************************************************************************************************
  * This Procedure is to Fetch the Party ID's for the given Search Lookup and Search String
  * Author : Vprasath
******************************************************************************************************/
	v_table_name   t912_search.c912_table_nm%TYPE;
	v_column_name  t912_search.c912_table_nm%TYPE;
	v_search_string VARCHAR2 (2000);
	v_sql		   VARCHAR2 (20000);

	CURSOR v_cursor
	IS
		SELECT	 c912_table_nm table_nm, c912_column_nm column_nm
			   , DECODE (c912_search_code_grp, '', tokenii, get_code_id (tokenii, c912_search_code_grp)) search_val
			FROM v_double_in_list vlist, t912_search t912
		   WHERE t912.c912_search_id = vlist.token AND token IS NOT NULL AND tokenii IS NOT NULL
		ORDER BY table_nm;

	v_prev_row_tbl_nm VARCHAR2 (100);
	v_closed_flag  CHAR (1) := 'N';
BEGIN
--
	my_context.set_double_inlist_ctx (p_search_str);
	v_sql		:= ' select  c101_party_id PARTY_ID, c101_last_nm || '' '' || c101_first_nm PARTY_NM';
	v_sql		:= v_sql || ' , NVL(t903.C903_UPLOAD_FILE_LIST,-999) file_id ';
	v_sql		:= v_sql || ' from t101_party t101, t903_upload_file_list t903 ';
	v_sql		:= v_sql || ' where t101.C101_PARTY_ID = t903.C903_REF_ID(+) ';
	v_sql		:= v_sql || ' and t903.C901_REF_TYPE(+) = 91182 ';
	v_sql		:= v_sql || ' and t101.C101_VOID_FL IS NULL ';
	v_sql		:= v_sql || ' and t903.C903_DELETE_FL IS NULL ';

	FOR current_row IN v_cursor
	LOOP
		IF v_prev_row_tbl_nm IS NULL OR v_prev_row_tbl_nm <> current_row.table_nm
		THEN
			IF v_prev_row_tbl_nm IS NOT NULL
			THEN
				v_sql		:= v_sql || ')';
				v_closed_flag := 'Y';
			END IF;

			v_closed_flag := 'N';
			v_sql		:= v_sql || ' and	t101.C101_PARTY_ID IN ( SELECT c101_party_id from ';
			v_sql		:=
				   v_sql
				|| current_row.table_nm
				|| ' where '
				|| current_row.column_nm
				|| ' = '
				|| ''''
				|| current_row.search_val
				|| '''';
			v_prev_row_tbl_nm := current_row.table_nm;
		ELSE
			v_closed_flag := 'N';
			v_sql		:=
					   v_sql || '
				AND ' ||		   current_row.column_nm || ' = ' || '''' || current_row.search_val || '''';
			v_prev_row_tbl_nm := current_row.table_nm;
		END IF;
	END LOOP;

	IF v_closed_flag = 'N'
	THEN
		v_sql		:= v_sql || ')';
	END IF;

	gm_procedure_log ('v_sql', v_sql);
	COMMIT;

	OPEN p_recordset
	 FOR v_sql;
--
END gm_search_party;
/
