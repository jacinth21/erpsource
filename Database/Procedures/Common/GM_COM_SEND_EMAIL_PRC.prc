--@"C:\Database\Procedures\Common\GM_COM_SEND_EMAIL_PRC.prc";

CREATE OR REPLACE PROCEDURE GM_COM_SEND_EMAIL_PRC
(
  p_to_user 	IN VARCHAR2
 ,p_subject 	IN VARCHAR2
 ,p_mail_body   IN VARCHAR2
)
AS
BEGIN
	/*Removed the mail content from GM_COM_SEND_EMAIL_PRC Procedure and call 
	 * the below common procedure to send the email
	 * */
		gm_com_send_html_email(p_to_user,p_subject,NULL,p_mail_body);
	
END GM_COM_SEND_EMAIL_PRC;
/

