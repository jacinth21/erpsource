/* Formatted on 2009/03/30 15:01 (Formatter Plus v4.8.0) */
--@"C:\Database\Procedures\Common\gm_save_email_details.prc"

CREATE OR REPLACE PROCEDURE gm_save_email_details (
	p_fromId   IN   t916_email_log.c916_email_from%TYPE
  , p_toId 	   IN   t916_email_log.c916_email_to%TYPE
  , p_cc	   IN   t916_email_log.c916_email_cc%TYPE
  , p_subject  IN   t916_email_log.c916_email_subject%TYPE
  , p_message  IN   t916_email_log.c916_email_msg%TYPE
  )
AS

BEGIN

INSERT INTO t916_email_log
	(c916_email_log_id, c916_email_from, c916_email_to
	, c916_email_cc, c916_email_subject, c916_email_msg, c916_email_sent_date 
	)
	 VALUES (s916_email_log_dtls.NEXTVAL, p_fromId,p_toId,p_cc,p_subject,p_message,SYSTIMESTAMP
	);
	
END gm_save_email_details;
/
