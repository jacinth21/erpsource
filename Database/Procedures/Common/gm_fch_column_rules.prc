/* Formatted on 2008/09/22 14:31 (Formatter Plus v4.8.0) */

-- @"C:\Database\Procedures\Common\gm_fch_column_rules.prc"

/*	Description 	: THIS PROCEDURE is used to get the next val and FORMAT 
					  it for the given ref_type and ref id
 *	Parameters		: p_ref_id, p_ref_type
 */

create or replace
PROCEDURE gm_fch_column_rules(
	    p_rule_grp	           IN    VARCHAR2
	    ,p_out_rules	   OUT 	 TYPES.cursor_type
	    )
AS
	v_rule_id	   t906_rules.c906_rule_id%TYPE;
    
BEGIN
    --
     
    OPEN p_out_rules FOR  
	      
		SELECT c906_rule_id ruleid, c906_rule_value ruleval
         	FROM t906_rules
   		WHERE  c906_rule_grp_id = p_rule_grp
		order by C906_RULE_SEQ_ID;

END gm_fch_column_rules;
/