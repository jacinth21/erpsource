/* Formatted on 2010/02/12 20:53 (Formatter Plus v4.8.0) */
--@ "C:\Database\Procedures\Common\gm_sav_update_account_price.prc";

CREATE OR REPLACE PROCEDURE gm_sav_update_account_price
AS
	v_accountid    t705_account_pricing.c704_account_id%TYPE;
	subject 	   VARCHAR2 (200);
	to_mail 	   VARCHAR2 (400);
	mail_body	   VARCHAR2 (4000);

--
	CURSOR cur_details1
	IS
		SELECT t705b.c704_account_id accountid, get_account_name (t705b.c704_account_id) accountnm
		  FROM t705b_price_increase_exclude t705b
		 WHERE t705b.c705b_exclude_fl IS NULL
		   AND t705b.c705b_price_update_fl IS NULL
		   AND TO_CHAR (c705b_price_year, 'YYYY') = '2010'
		   AND ROWNUM <= 20;

	CURSOR cur_details2
	IS
		SELECT t705c.c205_part_number_id pnum, t705c.c705c_new_price newprice
		  FROM t705c_price_increase t705c
		 WHERE t705c.c704_account_id = v_accountid AND t705c.c705c_void_fl IS NULL;
BEGIN
	FOR var_detail1 IN cur_details1
	LOOP
		v_accountid := var_detail1.accountid;

		FOR var_detail2 IN cur_details2
		LOOP
			UPDATE t705_account_pricing t705
			   SET t705.c705_unit_price = var_detail2.newprice
				 , t705.c705_last_updated_by = 30301
				 , t705.c705_last_updated_date = SYSDATE
				 , t705.c705_old_price = t705.c705_unit_price
				 , t705.C7501_ACCOUNT_PRICE_REQ_DTL_ID = null
			 WHERE t705.c704_account_id = v_accountid AND t705.c205_part_number_id = var_detail2.pnum;
		END LOOP;

		UPDATE t705b_price_increase_exclude t705b
		   SET t705b.c705b_price_update_fl = 'Y'
			 , t705b.c705b_last_updated_by = 30301
			 , t705b.c705b_last_updated_date = SYSDATE
		 WHERE t705b.c704_account_id = v_accountid;

		COMMIT;
		to_mail 	:=
				  'djames@globusmedical.com,richardk@globusmedical.com,vprasath@globusmedical.com,xqu@globusmedical.com';
		subject 	:= ' Account Price Updated';
		mail_body	:= ' The price for the account, ' || var_detail1.accountnm || ', ' || ' has been updated';
		gm_com_send_email_prc (to_mail, subject, mail_body);
	END LOOP;
END gm_sav_update_account_price;
/
