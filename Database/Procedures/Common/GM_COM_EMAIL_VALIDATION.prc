--@"C:\Database\Procedures\Common\GM_COM_EMAIL_VALIDATION.prc";
/***************************************************************************************************
* Description : This procedure used to detect the email protocol for the emails sending from Oracle
* Author	  : Karthik
****************************************************************************************************/

CREATE OR REPLACE PROCEDURE GM_COM_EMAIL_VALIDATION
(
     p_to_user  IN OUT VARCHAR2
	,p_subject  IN 	  VARCHAR2
)
AS
 	v_ruleval_ser  VARCHAR2 (20);
	v_ruleval_sys  VARCHAR2 (1000);
	v_host_nm 	   VARCHAR2 (50);
	v_to_user  	   VARCHAR2 (2000);
	v_test_email   VARCHAR2 (2000);
	v_rul_test_ser VARCHAR2 (20);
	v_email_subject VARCHAR2 (1000);
	v_machine      VARCHAR2 (50);
 --
BEGIN
	SELECT lower(SYS_CONTEXT ('USERENV', 'HOST', 15)) INTO v_host_nm FROM dual;
	v_ruleval_ser    := get_rule_value(v_host_nm,'WHITELISTSERVER');
	v_rul_test_ser   := get_rule_value(v_host_nm,'TESTINGSERVER');
	v_ruleval_sys    := get_rule_value(v_host_nm,'SYSTEM');
	v_test_email     := get_rule_value(v_host_nm,'TESTEMAIL');
	v_email_subject  := get_rule_value(v_host_nm,'EMAILSUBJECT');
		IF v_ruleval_ser IS NULL AND v_rul_test_ser IS NULL AND v_ruleval_sys IS NULL THEN
		  GM_RAISE_APPLICATION_ERROR('-20999','31',v_host_nm);
		END IF;
		IF v_ruleval_ser = 'TRUE' THEN
		  v_to_user := p_to_user;
		ELSIF v_rul_test_ser = 'TRUE' THEN
		  IF (INSTR (LOWER (p_subject), LOWER (v_email_subject)) > 0) THEN
		    IF v_test_email IS NULL THEN
		      GM_RAISE_APPLICATION_ERROR('-20999','32',v_host_nm);
		    END IF;
		    v_to_user := v_test_email;
		  ELSE
		    v_machine := 'TRUE';
		  END IF;
		END IF;
		IF v_machine  = 'TRUE' OR v_ruleval_sys IS NOT NULL THEN
		  IF v_ruleval_sys IS NULL THEN
		    GM_RAISE_APPLICATION_ERROR('-20999','33',v_host_nm);
		  END IF;
		  v_to_user := v_ruleval_sys;
		END IF;
	p_to_user := v_to_user;
END GM_COM_EMAIL_VALIDATION;
/

