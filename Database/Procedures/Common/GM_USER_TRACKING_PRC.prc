/* Formatted on 2008/07/04 18:45 (Formatter Plus v4.8.0) */
CREATE OR REPLACE PROCEDURE gm_user_tracking_prc (
	p_user_id	 IN 	  t102_user_login.c101_user_id%TYPE
  , p_action	 IN 	  VARCHAR2
  , p_sessid	 IN 	  t103_user_login_track.c103_session_id%TYPE
  , p_clientid	 IN 	  t103_user_login_track.c103_client_ip%TYPE
  , p_message	 OUT	  VARCHAR2
)
AS
/*
	  Description			:This procedure is called while login and logout.
	  Parameters			:p_employee_id
							:p_message
*/
	v_action	   VARCHAR2 (12);
BEGIN
	v_action	:= p_action;

	IF v_action = 'logon'
	THEN
		INSERT INTO t103_user_login_track
					(c103_tracking_id, c103_login_ts, c101_user_id, c103_session_id, c103_client_ip
					)
			 VALUES (s101_login.NEXTVAL, SYSDATE, p_user_id, p_sessid, p_clientid
					);
	ELSE
		UPDATE t103_user_login_track
		   SET c103_logoff_ts = SYSDATE
		 WHERE c101_user_id = p_user_id AND c103_logoff_ts IS NULL AND c103_session_id = p_sessid;
	END IF;
EXCEPTION
	WHEN OTHERS
	THEN
		p_message	:= SQLERRM;
END gm_user_tracking_prc;
/
