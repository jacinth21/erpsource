/* Formatted on 2007/08/17 15:13 (Formatter Plus v4.8.0) */


CREATE OR REPLACE PROCEDURE gm_cm_fch_employee_list (
	p_dept_access	IN		 t101_user.c101_dept_id%TYPE
  , p_dept_id		IN		 t101_user.c901_dept_id%TYPE
  , p_recordset 	OUT 	 TYPES.cursor_type
)
AS
/***************************************************************************************************
	  * This procedure is to fetch the names of the employees of a dept with a specific access
	  * Author : vprasath
	  * Date	: 08/13/07
***************************************************************************************************/
BEGIN
	--
	OPEN p_recordset
	 FOR
		 SELECT c101_user_id ID, c101_user_sh_name || ' ' || c101_user_l_name NAME
		   FROM t101_user
		  WHERE c101_dept_id = p_dept_access
			AND c901_dept_id = p_dept_id
			AND c901_user_status = 311
			AND c101_termination_date IS NULL;
END gm_cm_fch_employee_list;
