/* Formatted on 2009/08/29 13:35 (Formatter Plus v4.8.0) */
--@"C:\Database\Procedures\Common\gm_populate_tags.prc";
-- EXEC gm_populate_tags (1, 20000)
-- EXEC gm_populate_tags (9201, 20000);
--EXEC GLOBUS_APP.gm_populate_tags (9201, 20000);

CREATE OR REPLACE PROCEDURE gm_populate_tags (
	p_start_tag   IN   NUMBER
  , p_end_tag	  IN   NUMBER
)
AS
	v_start_tag    NUMBER := p_start_tag;
	v_end_tag	   NUMBER := p_end_tag;
BEGIN
	WHILE v_start_tag <= v_end_tag
	LOOP
		INSERT INTO t5010_tag
					(c5010_tag_id, c901_status, c5010_created_by, c5010_created_date
					)
			 VALUES (TRIM (TO_CHAR (v_start_tag, '0000000')), 51010 , 303149, SYSDATE   
					);

		v_start_tag := v_start_tag + 1;
	--DBMS_OUTPUT.PUT_LINE(TO_CHAR(v_start_tag, '0000000') || 'Inserted.');
	END LOOP;

	COMMIT;
END gm_populate_tags;
/
