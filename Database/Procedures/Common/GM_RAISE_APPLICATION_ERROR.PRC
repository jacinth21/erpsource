CREATE OR REPLACE PROCEDURE GM_RAISE_APPLICATION_ERROR (  
   P_ORA_ERROR_CODE        IN   T100_ERROR_MSG.C100_ORA_ERROR_CODE%TYPE,   
   P_ERROR_CODE        IN   T100_ERROR_MSG.C100_ERROR_CODE %TYPE,
   P_MSG_VALUES        IN   T100_ERROR_MSG.C100_ERROR_MSG%TYPE
)
AS
v_company_id   T1900_COMPANY.C1900_COMPANY_ID%TYPE;
v_language_id  NUMBER;
v_error_msg    T100_ERROR_MSG.C100_ERROR_MSG%TYPE;
v_msg_value   T100_ERROR_MSG.C100_ERROR_MSG%TYPE;
v_result_error_msg T100_ERROR_MSG.C100_ERROR_MSG%TYPE;
v_substring VARCHAR2 (1000) ;
v_string CLOB   := P_MSG_VALUES;
v_index NUMBER;
v_parent_comp_id T1900_COMPANY.C1900_COMPANY_ID%TYPE;
v_party_id T101_PARTY.C101_PARTY_ID%TYPE;

/***************************************************************************************************
   * This procedure is used to raise application error based on language of the company
   * Author : MANOJ
   * Date : 11/16/2016
***************************************************************************************************/
BEGIN  
              SELECT get_compid_frm_cntx(),get_partyid_frm_cntx()
	            INTO v_company_id,v_party_id 
	            FROM dual;
	          
	        select gm_pkg_it_user.get_default_party_company(v_party_id) 
	      	  INTO v_parent_comp_id
	      	  from dual;
	      	  
	    IF (v_parent_comp_id = '1022' AND v_company_id = '1022')
	    THEN
	    
	    	v_language_id := '10306122';
	    ELSE
	    
	    	v_language_id := '103097';
	    END IF;
	    
       SELECT C100_ERROR_MSG 
         INTO v_error_msg 
         FROM T100_ERROR_MSG 
        WHERE C901_LANGUAGE_ID= v_language_id
          AND C100_ERROR_CODE = P_ERROR_CODE
          AND C100_ERROR_VOID_FL IS NULL;
                
          v_string := v_string||'#$#';
          v_index :=1;
      WHILE INSTR (v_string, '#$#') <> 0
	    LOOP
	        v_substring  := SUBSTR (v_string, 1, INSTR (v_string, '#$#') - 1) ;
	        v_string     := SUBSTR (v_string, INSTR (v_string, '#$#')    + 3) ;
	        v_msg_value       := NULL;
	        v_msg_value     := v_substring ;  
           
            v_result_error_msg :=NVL(REPLACE(v_error_msg,'['||v_index||']',v_msg_value),(v_error_msg||v_msg_value));
            v_error_msg :=  v_result_error_msg;
            v_index := v_index+1;
    	       
	    END LOOP;     

   raise_application_error (P_ORA_ERROR_CODE,v_result_error_msg) ;

END GM_RAISE_APPLICATION_ERROR;
/
