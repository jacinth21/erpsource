/* Formatted on 2008/03/05 10:47 (Formatter Plus v4.8.8) */
-- @"C:\Database\Procedures\Common\gm_cm_sav_partqty.prc";

CREATE OR REPLACE PROCEDURE gm_cm_sav_partqty (
    p_part_number_id     IN   t205c_part_qty.c205_part_number_id%TYPE
  , p_update_qty         IN   t205c_part_qty.c205_available_qty%TYPE
  , p_trans_id           IN   t205c_part_qty.c205_last_update_trans_id%TYPE
  , p_updated_by         IN   t205c_part_qty.c205_last_updated_by%TYPE
  , p_transaction_type   IN   t205c_part_qty.c901_transaction_type%TYPE
  , p_action             IN   t205c_part_qty.c901_action%TYPE
  , p_type               IN   t205c_part_qty.c901_type%TYPE
)
AS
v_plant_id     	t5040_plant_master.c5040_plant_id%TYPE;
/****************************************************************************************************************************************
 * This procedure is to update part qty information in the new t205c_part_qty table
 *
*****************************************************************************************************************************************/
BEGIN
	SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
	
	IF v_plant_id IS NULL THEN
	--ERROR - PART NO, TRANSTYPE, TRAINSID
	GM_RAISE_APPLICATION_ERROR('-20999','30',p_part_number_id||'#$#'||get_code_name(p_transaction_type)||'#$#'||p_trans_id);
	END IF;
    --
    UPDATE t205c_part_qty
       SET c205_available_qty = NVL(c205_available_qty,0) + p_update_qty
         , c205_last_update_trans_id = p_trans_id
         , c205_last_updated_by = p_updated_by
         , c901_action = p_action
         , c901_type = p_type
	 WHERE c205_part_number_id = p_part_number_id AND c901_transaction_type = p_transaction_type AND c5040_plant_id = v_plant_id;
	 
    IF (SQL%ROWCOUNT = 0)
    THEN
    
    
    
        INSERT INTO t205c_part_qty
                    (c205c_part_qty_id, c205_part_number_id, c205_available_qty, c205_last_update_trans_id
                   , c205_last_updated_by, c205_last_updated_date, c901_transaction_type, c901_action, c901_type, c5040_plant_id
                    )
             VALUES (s205c_part_qty.NEXTVAL, p_part_number_id, p_update_qty, p_trans_id
                   , p_updated_by, CURRENT_DATE, p_transaction_type, p_action, p_type, v_plant_id
                    );
    END IF;
END gm_cm_sav_partqty;
/