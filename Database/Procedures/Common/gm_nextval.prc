/* Formatted on 2008/09/22 14:31 (Formatter Plus v4.8.0) */

-- @"C:\Database\Procedures\Common\gm_nextval.prc"

/*	Description 	: THIS PROCEDURE is used to get the next val and FORMAT 
					  it for the given ref_type and ref id
 *	Parameters		: p_ref_id, p_ref_type
 */

CREATE OR REPLACE PROCEDURE gm_nextval (
	p_ref_id	   IN		t908_gm_nextval.c908_ref_id%TYPE
  , p_ref_type	   IN		t908_gm_nextval.c901_ref_type%TYPE
  , p_out_result   OUT		VARCHAR2
)
AS

BEGIN
	SELECT	   TO_NUMBER (c908_start_pattern_id || TRIM (TO_CHAR (t908.c908_currval + 1, t901.c902_code_nm_alt)))
		  INTO p_out_result
		  FROM t908_gm_nextval t908, t901_code_lookup t901
		 WHERE t908.c901_pattern_format = t901.c901_code_id AND c908_ref_id = p_ref_id AND c901_ref_type = p_ref_type
	FOR UPDATE;

	UPDATE t908_gm_nextval
	   SET c908_currval = c908_currval + 1
	 WHERE c908_ref_id = p_ref_id AND c901_ref_type = p_ref_type;
END gm_nextval;
/