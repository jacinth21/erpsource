/* Formatted on 2010/10/07 15:32 (Formatter Plus v4.8.0) */
-- @"C:\database\Procedures\Common\gm_update_password.prc"

create or replace
PROCEDURE gm_update_password (
   p_userid        IN       t102_user_login.c102_login_username%TYPE,
   p_oldpassword   IN       t102_user_login.c102_user_password%TYPE,
   p_newpassword   IN       t102_user_login.c102_user_password%TYPE,
   p_message       OUT      VARCHAR2
)
/*
   Description        :  This component modify the user password.
   Parameters        :  p_userid -- The user id of the employee.
                      p_password -- The password of the employee
                      MESSAGE    -- Out put variable for sucess or failure of procedure
*/
AS
   v_oldpwd            t102_user_login.c102_user_password%TYPE;
   v_userid            t102_user_login.c101_user_id%TYPE;
   exp_incorrect_pwd   EXCEPTION;
   v_count             NUMBER;
   v_password_hist     NUMBER;
   V_LAST_PWD_CHANGE   NUMBER;
   V_CHK_PWD_CNT       NUMBER;
BEGIN
   BEGIN
      SELECT c102_user_password, c101_user_id
        INTO v_oldpwd, v_userid
        FROM t102_user_login
       WHERE UPPER (c102_login_username) = UPPER (p_userid);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         p_message := 'Invalid User';
         raise_application_error (-20787, '');
   END;

   v_password_hist := TO_NUMBER(get_rule_value('PASSWDHIST', 'PASSWDHIST'));
   
   SELECT COUNT (1)
     INTO v_count
     FROM
     (SELECT * FROM 
     (SELECT   t102a.c102a_password
               FROM t102_user_login t102, t102a_user_password_history t102a
              WHERE t102.c102_user_login_id = t102a.c102_user_login_id
                AND t102.c102_login_username = p_userid
           ORDER BY t102a.c102a_created_date DESC)
    WHERE ROWNUM <= v_password_hist)
    WHERE c102a_password = p_newpassword;

   IF v_count > 0
   THEN
      raise_application_error (-20788, '');
   END IF;
   
   v_last_pwd_change := TO_NUMBER(get_rule_value('LASTPWDCHANGE', 'LASTPWDCHANGE'));

   SELECT COUNT (1)
     INTO v_count
     FROM t102_user_login t102
    WHERE TRUNC (SYSDATE) - TRUNC (t102.c102_password_updated_date) <= v_last_pwd_change
      AND t102.c102_login_username = p_userid;

   IF v_count > 0
   THEN
      raise_application_error (-20788, '');
   END IF;

    gm_check_password(p_userid,p_newpassword,V_CHK_PWD_CNT);

    IF V_CHK_PWD_CNT > 0 THEN
      RAISE_APPLICATION_ERROR(-20316,'');
    END IF;

   IF (v_oldpwd = p_oldpassword)
   THEN
      UPDATE t102_user_login
         SET c102_user_password = p_newpassword,
             c102_password_expired_fl = NULL,
             c102_password_updated_date = SYSDATE,
             c102_last_updated_by = v_userid,
             c102_last_updated_date = SYSDATE
       WHERE UPPER (c102_login_username) = UPPER (p_userid);

      gm_update_log (v_userid, 'change password', '1242', v_userid, p_message);
   ELSE
      raise_application_error (-20789, '');
   END IF;
END gm_update_password;
/
