--@"C:\Database\Procedures\Common\gm_cm_fch_grp_map_details.prc";

CREATE OR REPLACE PROCEDURE gm_cm_fch_grp_map_details (
	p_group_id        IN       t1500_group.c1500_group_id%TYPE,
	p_recordset      OUT      TYPES.cursor_type
)
AS
/***
 * procedure to get the users list from security group
 ***/
BEGIN
   OPEN p_recordset
    FOR
	SELECT c101_user_id ID,
              c101_user_sh_name || ' ' || c101_user_l_name NAME
         FROM t101_user
        WHERE c901_user_status = 311
          AND c101_termination_date IS NULL 
          AND c101_user_id IN (select c101_mapped_party_id from t1501_group_mapping where c1500_group_id = p_group_id  and c1501_void_fl is null)
          ORDER BY NAME ASC;
END gm_cm_fch_grp_map_details;
/