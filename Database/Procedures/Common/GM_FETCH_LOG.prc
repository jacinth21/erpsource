--@"c:\database\procedures\common\GM_FETCH_LOG.prc";
create or replace PROCEDURE GM_FETCH_LOG
(	
	p_logid          IN  t902_log.c902_LOG_ID%TYPE, 
	p_LOG_DETAIL 	 OUT TYPES.cursor_type
)
AS
--
BEGIN
--
	
	OPEN p_LOG_DETAIL FOR
	SELECT C902_LOG_ID LOGID,C902_COMMENTS COMMENTS
	FROM  T902_LOG WHERE  C902_LOG_ID=p_logid
	AND C902_VOID_FL IS NULL;

END GM_FETCH_LOG;

/