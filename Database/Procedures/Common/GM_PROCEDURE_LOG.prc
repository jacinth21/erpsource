CREATE OR REPLACE PROCEDURE gm_procedure_log
(
  p_pro_name IN procedure_log.p_name%TYPE
 ,p_log_ds IN procedure_log.log_ds%TYPE
)
AS
--
BEGIN
--
 INSERT INTO procedure_log
 ( p_name
    ,log_ds
 )VALUES
 (  p_pro_name
  ,p_log_ds
 );
--
END gm_procedure_log;
/

