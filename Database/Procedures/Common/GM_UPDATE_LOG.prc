CREATE OR REPLACE PROCEDURE GM_UPDATE_LOG
(	
	p_c902_ref_id 	 IN  t902_log.c902_ref_id%TYPE,
	p_c902_comments	 IN  t902_log.c902_comments%TYPE,
	p_c902_type		 IN  t902_log.c902_type%TYPE,	
	p_userid		 IN  t902_log.c902_created_by%TYPE,
	p_message 		 OUT VARCHAR2
)
AS
v_company_id t1900_company.c1900_company_id%TYPE;
v_req_company_id  t1900_company.c1900_company_id%TYPE;
BEGIN
--getting the company id
	SELECT get_compid_frm_cntx () INTO  v_company_id FROM DUAL;
	
	SELECT get_transaction_company_id (v_company_id,p_c902_ref_id,'LOANERLOGFILTER','LOANERREQUEST') INTO v_req_company_id FROM DUAL;
	
	--
	INSERT INTO T902_LOG (
	   c902_log_id, 
	   c902_ref_id, 
	   c902_comments, 
	   c902_type, 
	   c902_created_by, 
	   c902_created_date,
	   c1900_company_id) 
	VALUES ( 
		s507_log.NEXTVAL, 
		p_c902_ref_id, 
		p_c902_comments,
		p_c902_type, 
		p_userid, 
		CURRENT_DATE,
		v_req_company_id
	);
--
EXCEPTION WHEN OTHERS
THEN
--
	p_message := SQLERRM;
--
END GM_UPDATE_LOG;
/

