--@"c:\database\procedures\common\GM_SAVE_LOG.prc";
create or replace PROCEDURE GM_SAVE_LOG
(	
	p_c902_ref_id 	 IN  t902_log.c902_ref_id%TYPE,
	p_c902_comments	 IN  t902_log.c902_comments%TYPE,
	p_c902_type	 IN  t902_log.c902_type%TYPE,	
	p_userid	 IN  t902_log.c902_created_by%TYPE,
	p_logid          IN  t902_log.c902_LOG_ID%TYPE, 
	p_message 	 OUT VARCHAR2
)
AS
--
BEGIN
--
	
	IF p_logid >0 THEN
		UPDATE T902_LOG set c902_comments =p_c902_comments
		,C902_LAST_UPDATED_BY=p_userid ,C902_LAST_UPDATED_DATE=sysdate
		where c902_log_id=p_logid;	
	ELSE	
	   GM_UPDATE_LOG(p_c902_ref_id,p_c902_comments,p_c902_type,p_userid,p_message);
	END IF;
--
EXCEPTION WHEN OTHERS
THEN
--
	p_message := SQLERRM;
--
END GM_SAVE_LOG;

/