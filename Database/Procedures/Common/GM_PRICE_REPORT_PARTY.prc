/* Formatted on 2017/03/22 10:42 (Formatter Plus v4.8.0) */


CREATE OR REPLACE PROCEDURE GM_PRICE_REPORT_PARTY (
	p_user_id	  IN 	   T101_USER.C101_USER_ID%TYPE
  , p_type		  IN	   t101_party.c901_party_type%TYPE
  , p_searchtext  IN	   VARCHAR2
  , p_gpb_acc_id   IN	t101_party.c101_party_id%TYPE
  , p_message	  OUT	   VARCHAR2
  , p_recordset   OUT	   TYPES.cursor_type
)
AS
/*
	  Description			:This procedure is called to FETCH party name
	  Parameters			:p_type   - holds party type to be fetched
							:p_recordset - holds list of party name
*/
	v_str		   VARCHAR2 (2000);
	v_portal_comp_id t101_party.c1900_company_id%TYPE;
BEGIN
--
	SELECT get_compid_frm_cntx () INTO v_portal_comp_id FROM DUAL;
	--
		OPEN p_recordset
		 FOR
			 SELECT   c101_party_id ID
					, DECODE (c101_last_nm || c101_first_nm
							, NULL, c101_party_nm
							, c101_last_nm || ' ' || c101_first_nm
							 ) NAME, c1900_company_id portal_company_id
				 FROM t101_party
				WHERE c901_party_type = p_type
				  AND c101_delete_fl IS NULL
				  AND c1900_company_id = NVL(v_portal_comp_id, c1900_company_id)
				  AND c101_void_fl IS NULL
				   AND  (REGEXP_like(c101_party_nm,p_searchtext,'i') OR c101_party_id = p_gpb_acc_id)
				  AND c101_active_fl IS NULL
			 ORDER BY UPPER (TRIM (c101_party_nm));

	EXCEPTION WHEN OTHERS
	THEN
		p_message	:= '1000';
END GM_PRICE_REPORT_PARTY;
/