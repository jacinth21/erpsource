CREATE OR REPLACE PROCEDURE gm_cm_fch_employee_list_by_al (
   p_dept_id        IN       t101_user.c901_dept_id%TYPE,
   p_frmaccesslvl   IN       t101_user.c101_access_level_id%TYPE,
   p_toaccesslvl    IN       t101_user.c101_access_level_id%TYPE,
   p_recordset      OUT      TYPES.cursor_type
)
AS
/***************************************************************************************************
   * This procedure is to fetch the names of the employees of a dept for the passed in access level
   * Author : Himanshu
   * Date : 04/06/10
***************************************************************************************************/
BEGIN
   --
   OPEN p_recordset
    FOR
       SELECT c101_user_id ID,
              c101_user_sh_name || ' ' || c101_user_l_name NAME
         FROM t101_user
        WHERE c901_dept_id = p_dept_id
          AND c901_user_status = 311
          AND c101_access_level_id >=
                                    NVL (p_frmaccesslvl, c101_access_level_id)
          AND c101_access_level_id <=
                                     NVL (p_toaccesslvl, c101_access_level_id)
          AND c101_termination_date IS NULL ORDER BY NAME ASC;
END gm_cm_fch_employee_list_by_al;
/
