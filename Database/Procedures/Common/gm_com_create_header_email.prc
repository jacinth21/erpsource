-- @"C:\Database\Procedures\Common\gm_com_create_header_email.prc";
/******************************************************************************************************
* Description : This procedure used to create common header email for the mail's sending from Oracle
* Author	  : Karthik
*******************************************************************************************************/

CREATE OR REPLACE PROCEDURE gm_com_create_header_email (
   p_html		IN OUT	 VARCHAR2
)
IS
	v_mail_body	   		   VARCHAR2 (30000);
	v_image_path	   	   t906_rules.c906_rule_value%TYPE := get_rule_value('gmivmprod','SERVERNAME');
	v_host_nm 			   VARCHAR2 (50):=  lower(SYS_CONTEXT ('USERENV', 'HOST', 15));
	v_email_protocol  	   t906_rules.c906_rule_value%TYPE := get_rule_value('EMAIL_PROTOCOL','EMAIL_DETAILS');
	v_email_footer  	   t906_rules.c906_rule_value%TYPE := get_rule_value('EMAIL_FOOTER','EMAIL_DETAILS');
BEGIN
		 /* Appending the Email header with the Message body
		  * */
		 p_html := regexp_replace(p_html,chr(13)||chr(10),'<BR>');
		 v_mail_body := v_mail_body || '<html><head><title></title><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><style type="text/css">a {text-decoration: none}</style></head>';
		 v_mail_body := v_mail_body || '<body text="#000000" link="#000000" alink="#000000" vlink="#000000">';
		 v_mail_body := v_mail_body || '<html><head><title></title><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>';
		 v_mail_body := v_mail_body || '<table cellpadding="0" cellspacing="0" style="min-width:850px;border-style: solid; border-width:1px;border-color: #00366e;">';
		 v_mail_body := v_mail_body || '<tr style="background-color:#00366e;font-family:Trebuchet MS;font-size:25px;color:#FFFFFF;">';
		 v_mail_body := v_mail_body || '<td style="background-color:#00366e;padding:1em;">';
		 v_mail_body := v_mail_body || '<img src="'|| v_image_path||'/images/globus-medical-logo.png" alt="Globus Medical" / ></td>';
		 v_mail_body := v_mail_body || '<td align="right" style="background-color:#00366e;padding:1em;"></td></tr>';
		 v_mail_body := v_mail_body || '<tr><td colspan="4">';
		 v_mail_body := v_mail_body ||  p_html;
		 v_mail_body := v_mail_body || '</td></tr>';
		 v_mail_body := v_mail_body || '</table></body></html>'; 
		 
		 /* Appending the Email Footer
		  * */
		 v_host_nm := v_host_nm ||' ' || '('|| v_email_protocol||')';
		 v_email_footer :=  REPLACE(v_email_footer,'<systemname>', v_host_nm);
	 	 v_mail_body:= v_mail_body || v_email_footer;
	 	 
	 	 /* v_mail_body contains the header of the email,body content of the email and Footer of the email 
	 	  * */
	 	 p_html:= v_mail_body;

END gm_com_create_header_email;
/
