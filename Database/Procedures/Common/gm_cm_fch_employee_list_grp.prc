
--@"C:\Database\Procedures\Common\gm_cm_fch_employee_list_grp.prc";

CREATE OR REPLACE PROCEDURE gm_cm_fch_employee_list_grp (
	p_dept_id        IN       t101_user.c901_dept_id%TYPE,
	p_frmaccesslvl   IN       t101_user.c101_access_level_id%TYPE,
	p_toaccesslvl    IN       t101_user.c101_access_level_id%TYPE,
	p_recordset      OUT      TYPES.cursor_type
)
AS
/***
 * procedure to restrict users from security group
 ***/
BEGIN
   OPEN p_recordset
    FOR
	SELECT c101_user_id ID,
              c101_user_sh_name || ' ' || c101_user_l_name NAME
         FROM t101_user
        WHERE c901_dept_id = p_dept_id
          AND c901_user_status = 311
          AND c101_access_level_id >=
                                    NVL (p_frmaccesslvl, c101_access_level_id)
          AND c101_access_level_id <=
                                     NVL (p_toaccesslvl, c101_access_level_id)
          AND c101_termination_date IS NULL 
          AND c101_user_id NOT IN (select c101_mapped_party_id from t1501_group_mapping where C1500_GROUP_ID='CLI_SITE_MONITOR' and c1501_void_fl is null)
          ORDER BY NAME ASC;
END gm_cm_fch_employee_list_grp;
/
