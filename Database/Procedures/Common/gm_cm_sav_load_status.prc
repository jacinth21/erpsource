/* Formatted on 2009/03/30 15:01 (Formatter Plus v4.8.0) */
--@"C:\Database\Procedures\Common\gm_cm_sav_load_status.prc"

CREATE OR REPLACE PROCEDURE gm_cm_sav_load_status (
	p_logname		  IN   t910_load_log_table.c910_log_name%TYPE
  , p_starttime 	  IN   VARCHAR2
  , p_endtime		  IN   VARCHAR2
  , p_statusdetails   IN   t910_load_log_table.c910_status_details%TYPE
  , p_statusfl		  IN   t910_load_log_table.c910_status_fl%TYPE
)
AS
v_date_fmt		t906_rules.c906_rule_value%TYPE;
BEGIN
	--
	SELECT NVL(get_compdtfmt_frm_cntx(),get_rule_value('DATEFMT','DATEFORMAT')) INTO v_date_fmt FROM DUAL;
	--
	INSERT INTO t910_load_log_table
				(c910_load_log_id, c910_log_name, c910_state_dt
			   , c910_end_dt, c910_status_details, c910_status_fl
				)
		 VALUES (s910_load_log.NEXTVAL, p_logname, TO_DATE (p_starttime, v_date_fmt ||' hh:mi:ss')
			   , TO_DATE (p_endtime, v_date_fmt ||' hh:mi:ss'), p_statusdetails, p_statusfl
				);
END gm_cm_sav_load_status;
/
