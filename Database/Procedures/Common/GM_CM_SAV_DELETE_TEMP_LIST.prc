/* Formatted on 2008/10/03 16:12 (Formatter Plus v4.8.0) */
-- @"C:\database\Procedures\Common\gm_cm_sav_delete_temp_list.prc";

CREATE OR REPLACE PROCEDURE gm_cm_sav_delete_temp_list
IS
BEGIN
	DELETE FROM my_temp_list;
END gm_cm_sav_delete_temp_list;
/
