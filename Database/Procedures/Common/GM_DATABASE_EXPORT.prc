/* Formatted on 2010/03/08 10:08 (Formatter Plus v4.8.0) */
/*
update the password field
update the t906_rules table with email
update the t101_user table with email
update rep table with email
update contact table  with email
update the distributor table with email 
update the account table with email
*/

CREATE OR REPLACE PROCEDURE gm_database_export
AS
BEGIN
	UPDATE t102_user_login
	   SET c102_user_password = 'password';

	UPDATE t906_rules
	   SET c906_rule_value = 'mpatel@globusmedical.com'
	 WHERE c906_rule_seq_id IN (SELECT c906_rule_seq_id
								  FROM t906_rules
								 WHERE c906_rule_value LIKE '%@%');

	UPDATE t101_user
	   SET c101_email_id = 'mpatel@globusmedical.com';

	UPDATE t703_sales_rep
	   SET c703_email_id = 'mpatel@globusmedical.com';

	UPDATE t107_contact
	   SET c107_contact_value = 'mpatel@globusmedical.com'
	 WHERE c901_mode = 90452;

	UPDATE t701_distributor t701
	   SET t701.c701_email_id = 'mpatel@globusmedical.com'
	 WHERE t701.c701_email_id IS NOT NULL AND t701.c701_void_fl IS NULL;

	UPDATE t704_account t704
	   SET t704.c704_email_id = 'mpatel@globusmedical.com'
	 WHERE t704.c704_email_id IS NOT NULL AND t704.c704_void_fl IS NULL;

-- Refresh existing mviews. Need to test and fix
/*	dbms_mview.REFRESH ('v621_patient_study_list');
	dbms_mview.REFRESH ('V9001_MONTH_LIST');
	dbms_mview.REFRESH ('v207a_set_consign_link');
	dbms_mview.REFRESH ('v207b_set_part_details');
	dbms_mview.REFRESH ('V240A_SALES_TARGET_LIST');
	dbms_mview.REFRESH ('v700_territory_mapping_detail');
	*/
END gm_database_export;
/
