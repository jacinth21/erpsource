CREATE OR REPLACE PROCEDURE P3152_SEND_EMAIL_PRC
(
 	   p_subject  IN		VARCHAR2,
	   p_ErrMsg	  IN		VARCHAR2
)
AS
  	from_user VARCHAR2(100) :='sys@globusmedical.com';
	to_user VARCHAR2(100) := 'djames@globusmedical.com';
	subject VARCHAR2(100) :='Error';
	v_msg VARCHAR2(100) := 'Testing';
	p_file_hdl	UTL_FILE.FILE_TYPE;
	p_new_line 	varchar2(87);
	p_reply		UTL_SMTP.reply;
	p_host 	 	VARCHAR2(30) := 'gmimail';
	p_mail_body	VARCHAR2(4000);
	conn		UTL_SMTP.CONNECTION;
	crlf	  	varchar2(2) := chr(13)||chr(10);
BEGIN
	--p_file_hdl:=UTL_FILE.FOPEN(utldir_path ,'mail.prop' ,'r');
	--UTL_FILE.GET_LINE(p_file_hdl,p_new_line);
	--p_host := trim(substr(p_new_line,6));
		conn := UTL_SMTP.open_connection(p_host);
		p_reply := UTL_SMTP.helo(conn,p_host );
		UTL_SMTP.mail( conn, from_user);
		UTL_SMTP.rcpt( conn, to_user);
		p_mail_body :=  'From : ' || from_user || crlf || 'To: ' || to_user
				|| crlf || 'Date:' || to_char(sysdate,'dd-Mon-YYYYHH24:MI:SS')
				|| crlf || 'Subject: ' || p_subject ||crlf|| crlf ||
			  	p_ErrMsg ||crlf;
		UTL_SMTP.data( conn, p_mail_body);
		UTL_SMTP.quit( conn );
  EXCEPTION
     WHEN OTHERS THEN
       DBMS_OUTPUT.PUT_LINE('Problem with emailing : '||sqlcode||' -'||sqlerrm);
end P3152_SEND_EMAIL_PRC;
/

