/* Formatted on 2010/04/05 14:46 (Formatter Plus v4.8.0) */
-- @"C:\Database\Procedures\Common\gm_com_send_html_email.prc";

CREATE OR REPLACE PROCEDURE gm_com_send_html_email (
	p_to		IN	 VARCHAR2
  , p_subject	IN	 VARCHAR2
  , p_text		IN	 VARCHAR2 DEFAULT NULL
  , p_html		IN	 VARCHAR2 DEFAULT NULL
)
IS
	l_boundary	   VARCHAR2 (255) DEFAULT 'a1b2c3d4e3f2g1';
	l_connection   UTL_SMTP.connection;
	l_body_html    CLOB := EMPTY_CLOB;	 --This LOB will be the email message
	l_offset	   NUMBER;
	l_ammount	   NUMBER;
	l_temp		   VARCHAR2 (32767) DEFAULT NULL;
	from_user	   VARCHAR2 (1000) := get_rule_value('FROMID','EMAIL_DETAILS');
	v_cc_user	   VARCHAR2 (1000) := get_rule_value('CCID','EMAIL_DETAILS');
	p_smtp_portnum NUMBER := 25;
	v_add_src	   VARCHAR2 (2000);
	v_addr		   VARCHAR2 (100);
	slen		   NUMBER := 1;
	v_to_user  	   VARCHAR2 (2000);
	v_html_msg	   VARCHAR2 (30000);
BEGIN
	
/*
 * GM_COM_CREATE_EMAIL_CONNECTION : Procedure to set the AWS Email credential with SMTP connection
 * Create wallet from output of openssl cmd to store the user credential
 * Make a secure connection using the SSL port configured with your SMTP server
 * Starting TLS to change the insecure connection to secure connection
 * Call the Auth procedure to authorized a user for access to the mail server
 * 
 */	
 	GM_COM_CREATE_EMAIL_CONNECTION(l_connection);
	UTL_SMTP.mail (l_connection, from_user);
	
 /*
  * GM_COM_EMAIL_VALIDATION : Procedure to validate the to email id based on the environemnts
  */
 	v_to_user := p_to;
 	GM_COM_EMAIL_VALIDATION(v_to_user,p_subject);
 
	v_add_src	:= REPLACE (v_to_user, ' ', '_');

	--
	IF (INSTR (v_to_user, ',') = 0)
	THEN
		UTL_SMTP.rcpt (l_connection, v_add_src);
	ELSE
		v_add_src	:= REPLACE (v_to_user, ' ', '_') || ',';

		WHILE (INSTR (v_add_src, ',', slen) > 0)
		LOOP
			--
			v_addr		:= SUBSTR (v_add_src, slen, INSTR (SUBSTR (v_add_src, slen), ',') - 1);
			slen		:= slen + INSTR (SUBSTR (v_add_src, slen), ',');
			--
			UTL_SMTP.rcpt (l_connection, v_addr);
		--
		END LOOP;
	END IF;
	
	UTL_SMTP.rcpt (l_connection, v_cc_user);
	/*
	 * GM_COM_CREATE_HEADER_EMAIL : Procedure used to create the common header email
	 * with the SpineIT header logo
	 */	
		v_html_msg := p_html;
		IF v_html_msg IS NOT NULL THEN 
		   gm_com_create_header_email(v_html_msg);
		END IF;

	l_temp		:= l_temp || 'MIME-Version: 1.0' || CHR (13) || CHR (10);
	l_temp		:= l_temp || 'To: ' || v_add_src || CHR (13) || CHR (10);
	l_temp		:= l_temp || 'From: ' || from_user || CHR (13) || CHR (10);
	l_temp		:= l_temp || 'Subject: ' || p_subject || CHR (13) || CHR (10);
	l_temp		:= l_temp || 'Reply-To: ' || v_add_src || CHR (13) || CHR (10);
	l_temp		:= l_temp || 'Cc: ' || v_cc_user || CHR (13) || CHR (10);
	l_temp		:=
		   l_temp
		|| 'Content-Type: multipart/alternative; boundary='
		|| CHR (34)
		|| l_boundary
		|| CHR (34)
		|| CHR (13)
		|| CHR (10);
----------------------------------------------------
-- Write the headers
	DBMS_LOB.createtemporary (l_body_html, FALSE, 10);
	DBMS_LOB.WRITE (l_body_html, LENGTH (l_temp), 1, l_temp);
----------------------------------------------------
-- Write the text boundary
--	l_offset	:= DBMS_LOB.getlength (l_body_html) + 1;
--	l_temp		:= '--' || l_boundary || CHR (13) || CHR (10);
--	l_temp		:= l_temp || 'content-type: text/plain; charset=us-ascii' || CHR (13) || CHR (10) || CHR (13)
--				   || CHR (10);
--	DBMS_LOB.WRITE (l_body_html, LENGTH (l_temp), l_offset, l_temp);
------------------------------------------------------
---- Write the plain text portion of the email
--	l_offset	:= DBMS_LOB.getlength (l_body_html) + 1;
--	DBMS_LOB.WRITE (l_body_html, LENGTH (p_text), l_offset, p_text);
----------------------------------------------------
-- Write the HTML boundary
	l_temp		:= CHR (13) || CHR (10) || CHR (13) || CHR (10) || '--' || l_boundary || CHR (13) || CHR (10);
	l_temp		:= l_temp || 'content-type: text/html;' || CHR (13) || CHR (10) || CHR (13) || CHR (10);
	l_offset	:= DBMS_LOB.getlength (l_body_html) + 1;
	DBMS_LOB.WRITE (l_body_html, LENGTH (l_temp), l_offset, l_temp);
----------------------------------------------------
-- Write the HTML portion of the message
	l_offset	:= DBMS_LOB.getlength (l_body_html) + 1;
	DBMS_LOB.WRITE (l_body_html, LENGTH (v_html_msg), l_offset, v_html_msg);
----------------------------------------------------
-- Write the final html boundary
	l_temp		:= CHR (13) || CHR (10) || '--' || l_boundary || '--' || CHR (13);
	l_offset	:= DBMS_LOB.getlength (l_body_html) + 1;
	DBMS_LOB.WRITE (l_body_html, LENGTH (l_temp), l_offset, l_temp);
----------------------------------------------------
-- Send the email in 1900 byte chunks to UTL_SMTP
	l_offset	:= 1;
	l_ammount	:= 1900;
	UTL_SMTP.open_data (l_connection);

	WHILE l_offset < DBMS_LOB.getlength (l_body_html)
	LOOP
		UTL_SMTP.write_data (l_connection, DBMS_LOB.SUBSTR (l_body_html, l_ammount, l_offset));
		l_offset	:= l_offset + l_ammount;
		l_ammount	:= LEAST (1900, DBMS_LOB.getlength (l_body_html) - l_ammount);
	END LOOP;
    gm_save_email_details(from_user,p_to,v_cc_user,p_subject,v_html_msg);
	UTL_SMTP.close_data (l_connection);
	UTL_SMTP.quit (l_connection);
	DBMS_LOB.freetemporary (l_body_html);
END gm_com_send_html_email;
/
