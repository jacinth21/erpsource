CREATE OR REPLACE PROCEDURE GM_SAVE_COSTING_TRACK_LOG
(  p_from_costing_id t821_costing_track_log.c820_from_costing_id%TYPE
 ,p_to_costing_id t821_costing_track_log.c820_to_costing_id%TYPE
 ,p_qty_added  t821_costing_track_log.c820_qty_added%TYPE
 ,p_purchase_amt  t821_costing_track_log.c820_purchase_amt%TYPE
 ,p_local_company_id t1900_company.c1900_company_id%TYPE
 ,p_owner_company_id t1900_company.c1900_company_id%TYPE
)
AS
/***************************************************************************************
 * Description           :This procedure is used to save costing log information
 *        to track the transfer information
  ****************************************************************************************/
--
BEGIN
--
 INSERT INTO t821_costing_track_log (
   c820_costing_track_log_id
  ,c820_from_costing_id
  ,c820_to_costing_id
  ,c820_qty_added
  ,c820_purchase_amt
  ,c1900_company_id
  ,c1900_owner_company_id
 )
 VALUES
 (  S821_costing_track_log.NEXTVAL
  ,p_from_costing_id
  ,p_to_costing_id
  ,p_qty_added
  ,p_purchase_amt
  ,p_local_company_id
  ,p_owner_company_id
 );
 --
--
END GM_SAVE_COSTING_TRACK_LOG;
/

