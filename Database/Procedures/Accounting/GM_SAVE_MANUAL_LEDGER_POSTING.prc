CREATE OR REPLACE PROCEDURE GM_SAVE_MANUAL_LEDGER_POSTING
(
	p_manual_id	t216_part_qty_adj_details.c215_part_qty_adj_id%TYPE
)
AS
--
	CURSOR qb_cursor IS
		SELECT	 t216.c215_part_qty_adj_id		p_adj_id
				,t216.c205_part_number_id		p_num
				,t216.c216_item_price			p_price
				,t216.c216_qty_to_be_adjusted	q_adj
				,t216.c216_created_by			created_by
				,t215.c901_adjustment_source 	a_source
				,t215.c215_adj_date 			a_date
				,NVL(c215_last_updated_by, c215_created_by) updated_by
		FROM	 t216_part_qty_adj_details t216
				,t215_part_qty_adjustment 	t215
		WHERE	t216.c216_qty_to_be_adjusted <> 0
		AND		t216.c215_part_qty_adj_id = p_manual_id
		AND		t216.c215_part_qty_adj_id = t215.c215_part_qty_adj_id;
--
v_part_number	T214_TRANSACTIONS.c205_part_number_id%TYPE;
v_adj_value		T214_TRANSACTIONS.c214_new_qty%TYPE;
v_temp			VARCHAR2(20);
v_posting_type 	NUMBER;
v_source		t215_part_qty_adjustment.c901_adjustment_source%TYPE;
--
BEGIN
--
	FOR qb_val IN qb_cursor
	LOOP
	 --
		 v_part_number	:= qb_val.p_num;
		 v_adj_value	:= qb_val.q_adj;
		 v_source		:= qb_val.a_source;
		 --
		v_temp := 'No Part Exception ';
		--
		-- If maps to Quarantine item (Scrap to Quarantine)
		IF ( v_source = 4902 AND  v_adj_value > 0 ) 
		THEN 
		--
			v_posting_type := 4829;		
		--Quarantine to Scrap
		--
		ELSIF ( v_source = 4902 AND  v_adj_value < 0 ) 
		THEN 
		--
			v_adj_value	   := v_adj_value * -1;
			v_posting_type := 4820;	
		--
		--Scrap to inhouse
		ELSIF ( v_source = 4903 AND  v_adj_value > 0 ) 
		THEN 
		--
			v_posting_type := 4830;	
		--
		--Inventory to Scrap
		ELSIF ( v_source = 4900 AND  v_adj_value < 0 ) 
		THEN 
		--
			v_posting_type := 4831;	
			v_adj_value	   := v_adj_value * -1;
		--
		--Scrap to Inventory 
		ELSIF ( v_source = 4900 AND  v_adj_value > 0 ) 
		THEN 
		--
			v_posting_type := 4832;	
		--
		--Scrap to Rework  
		ELSIF ( v_source = 4905 AND  v_adj_value > 0 ) 
		THEN 
		--
			v_posting_type := 4833;	
		--
		--Sales Consignment Inventory to Buildset   
		ELSIF ( v_source = 4904 AND  v_adj_value > 0 ) 
		THEN 
		--
			v_posting_type := 4834;	
		--
		--Maps directly to source type and not from Account type 
		ELSE 
		--
			v_posting_type := v_source;	
		--
		END IF;
		--
		DBMS_OUTPUT.PUT_LINE(v_part_number || ' ** ' || v_adj_value || ' ** ' ||v_posting_type || ' ** ' ||	qb_val.p_adj_id) ;
		--
		GM_SAVE_LEDGER_POSTING ( v_posting_type	,qb_val.a_date
											,v_part_number ,NULL
											,qb_val.p_adj_id,v_adj_value,qb_val.p_price , qb_val.updated_by );
		--
	END LOOP;
	 --
END GM_SAVE_MANUAL_LEDGER_POSTING;
/

