/* Formatted on 2011/05/24 16:02 (Formatter Plus v4.8.0) */
--@"C:\Database\Procedures\Accounting\GM_FETCH_PART_COGS_VALUE.prc";

CREATE OR REPLACE PROCEDURE gm_fetch_part_cogs_value (
	p_txn_id			  IN	   t810_posting_txn.c810_txn_id%TYPE
  , p_partnum			  IN	   t820_costing.c205_part_number_id%TYPE
  , p_qty				  IN	   t820_costing.c820_qty_received%TYPE
  , p_type				  IN	   NUMBER
  , p_from_costing_type   IN	   t820_costing.c901_costing_type%TYPE
  , p_to_costing_type	  IN	   t820_costing.c901_costing_type%TYPE
  , p_price 			  IN	   t820_costing.c820_purchase_amt%TYPE
  , p_updatedby 		  IN	   t820_costing.c820_last_updated_by%TYPE
  , p_txntype	 IN   t804_posting_ref.c901_transaction_type%TYPE
  , p_party_id			  IN	   t810_posting_txn.c803_period_id%TYPE
  , p_division_id                 IN       t810_posting_txn.c901_company_id%TYPE
  , p_country_id                 IN       t810_posting_txn.c901_country_id%TYPE
  , p_owner_company IN t820_costing.c1900_owner_company_id%TYPE
  , p_local_cost  IN t810_posting_txn.c810_cr_amt%TYPE
  , p_loc_company_id    IN	t820_costing.c1900_company_id%TYPE
  , p_plant_id		IN  t820_costing.c5040_plant_id%TYPE
  , p_txn_comp_id   IN t810_posting_txn.c1900_txn_company_id%TYPE
  , p_destination_comp_id   IN t810_posting_txn.c1900_destination_company_id%TYPE
  , p_cost				  OUT	   t820_costing.c820_purchase_amt%TYPE
  , p_approved_qty		  OUT	   t820_costing.c820_qty_received%TYPE
  , p_out_owner_comp_id   OUT 	   t820_costing.c1900_owner_company_id%TYPE
  , p_out_local_cost	  OUT      t820_costing.c820_purchase_amt%TYPE
  , p_costing_id		  OUT	   t820_costing.c820_costing_id%TYPE
)
AS
/******************************************************************************
 * Description			 :This procedure is used to fecth costing information
 *						  for the selected part
 *						  4800 -- 'Pending Progress' waiting in the line to be used
 *						  4801 -- 'Active' currently used for costing
 *						  4802 -- 'Closed' Qty used and the transaction closed
 *
 *				p-type	  4806 -- COGS ADD Values to costing table
 *						  4807 -- COGS Value Posting
 *						  4808 -- COGS Return value posting
 *						  4805 -- COGS Value only no Qty
 *
 *******************************************************************************/
-- International posting changes (PMT-13199) - to added the owner and local cost
	v_qty_on_hand  t820_costing.c820_qty_on_hand%TYPE;
	v_qty_remaining t820_costing.c820_qty_on_hand%TYPE;
	v_exp		   NUMBER;
	v_qty		   t820_costing.c820_qty_received%TYPE;
	v_qty_added    t820_costing.c820_qty_received%TYPE;
	v_costing_id   t820_costing.c820_costing_id%TYPE;
	v_company_id_ctx  t1900_company.c1900_company_id%TYPE;
	v_archived_fl CHAR (1);
	--
	v_owner_company_id t820_costing.c1900_owner_company_id%TYPE := p_owner_company;
	v_local_cost t810_posting_txn.c810_cr_amt%TYPE := p_local_cost;
	
	
	v_costing_type   t820_costing.c901_costing_type%TYPE;
	v_from_costing_type_cnt NUMBER;
	v_to_costing_type_cnt NUMBER;
--
	v_plant_count NUMBER;
	v_comp_count NUMBER;
	v_from_company_id  t1900_company.c1900_company_id%TYPE;
	v_from_plant_id T5040_PLANT_MASTER.c5040_plant_id%TYPE;
--
	v_to_company_id  t1900_company.c1900_company_id%TYPE;
	v_to_plant_id T5040_PLANT_MASTER.c5040_plant_id%TYPE;
    v_local_cost_cnt  NUMBER := 0;	
    v_cost_fl t906_rules.c906_rule_value%TYPE;

BEGIN
--
	v_qty		:= p_qty;
	v_qty_added := 0;

	-- To get the plant and company infromation
	BEGIN
		SELECT DECODE(c823_plant_level, 'Y', p_plant_id, NULL)
	    INTO v_from_plant_id
	   FROM t823_costing_type_ref
	  WHERE c901_costing_type = p_from_costing_type
	    AND c823_void_fl     IS NULL;
    --
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
		v_from_plant_id := NULL;
		v_from_company_id := NULL;
	END;	
    --
    BEGIN
	    SELECT DECODE(c823_plant_level, 'Y', p_plant_id, NULL)
	    INTO v_to_plant_id
	   FROM t823_costing_type_ref
	  WHERE c901_costing_type = p_to_costing_type
	    AND c823_void_fl     IS NULL;
	--
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
		v_to_plant_id := NULL;
		v_to_company_id := NULL;
	END;
	--
	v_from_company_id := p_loc_company_id;
	v_to_company_id := p_loc_company_id;
	--

-- *********************************************************************
-- if p-type  4806 will add the value and will return the approved value
-- Will be and currenlty used for purchase
-- *********************************************************************
	IF (p_type = 4806)
	THEN
		-- to check if cost is blank then, send the COGS error email
		IF NVL(p_price , 0) = 0
		THEN
			-- PC-991: To pass the local cost information and it used at repost COGS error
			gm_save_costing_error_log (p_partnum
											 , p_qty
											 , p_txn_id
											 , p_from_costing_type
											 , p_to_costing_type
											 , p_txntype
											 , p_party_id
											 , p_updatedby
											 , p_division_id
											 , p_country_id
											 , 26240362 -- Zero Costing Error
											 , p_loc_company_id
											 , p_plant_id
											 , p_txn_comp_id
											 , p_destination_comp_id
											 , NVL(v_owner_company_id, p_owner_company)
											 , p_local_cost
											  );		
		ELSE 
			gm_save_costing (p_partnum, p_from_costing_type, p_qty, p_price, p_updatedby, v_from_company_id, v_from_plant_id, v_owner_company_id,  v_local_cost, p_costing_id);
		END IF;
		--
		p_cost		:= p_price;
		p_approved_qty := p_qty;
		--
		p_out_owner_comp_id := v_owner_company_id;
		p_out_local_cost := v_local_cost;
		--
		RETURN;
	--
	END IF;
	-- In-transit - then to assign the company id to owner company id 
    IF get_rule_value (p_txntype, 'POST-FRM-COMP') = 'Y'--p_txntype = 4893
	THEN
		v_from_plant_id := NULL;
	 	v_from_company_id := v_owner_company_id;
	END IF;
	IF get_rule_value (p_txntype, 'POST-TO-COMP') = 'Y'
	THEN
		v_to_plant_id := NULL;
	 	v_to_company_id := v_owner_company_id;
	END IF;
	-- In- Transit transactions should have local cost posting.
	IF get_rule_value (p_txntype, 'LOCAL_COST_POSTING') = 'Y'
	THEN
	   v_local_cost_cnt := 1;
	END IF;
-- *********************************************************************
-- Below code will check if any Active Costing availabe for selected parts
-- if not FETCH from pending parts
-- *********************************************************************
	BEGIN
		--
		v_exp		:= 4801;

		SELECT	   c820_qty_on_hand, c820_costing_id, c820_purchase_amt
				, c1900_owner_company_id, DECODE(v_local_cost_cnt, 1, p_local_cost, c820_local_company_cost)
			  INTO v_qty_on_hand, p_costing_id, p_cost
			  , v_owner_company_id, v_local_cost
			  FROM t820_costing
			 WHERE c205_part_number_id = p_partnum AND c901_costing_type = p_from_costing_type AND c901_status = 4801
			 AND c1900_company_id = v_from_company_id
			 AND NVL(c5040_plant_id, -999) = NVL(v_from_plant_id, - 999)
		FOR UPDATE;
	--
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_exp		:= 4800;
	END;

	--
	-- If  no active TXN found pick the pending txn
	-- based on FIFO method
	IF (v_exp = 4800)
	THEN
		--
		BEGIN
			--
			SELECT	   c820_qty_on_hand, c820_costing_id, c820_purchase_amt
				, c1900_owner_company_id, DECODE(v_local_cost_cnt, 1, p_local_cost, c820_local_company_cost)
				  INTO v_qty_on_hand, p_costing_id, p_cost
				  , v_owner_company_id, v_local_cost
				  FROM t820_costing
				 WHERE c820_costing_id =
						   (SELECT MIN (c820_costing_id)
							  FROM t820_costing
							 WHERE c205_part_number_id = p_partnum
							   AND c901_costing_type = p_from_costing_type
							   AND c901_status = 4800
							   AND c1900_company_id = v_from_company_id
							   AND NVL(c5040_plant_id, -999) = NVL(v_from_plant_id, - 999))
					FOR UPDATE;
		--
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				-- if type is COSG RETURN
				-- no active cost then fetch from closed cost
				IF (p_type = 4808)
				THEN
					v_exp		:= 4802;
				ELSE
					-- If other type log error and exit
					-- PC-991: To pass the local cost information and it used at repost COGS error
					
					gm_save_costing_error_log (p_partnum
											 , p_qty
											 , p_txn_id
											 , p_from_costing_type
											 , p_to_costing_type
											 , p_txntype
											 , p_party_id
											 , p_updatedby
											 , p_division_id
											 , p_country_id
											 , 102940 --102940 Costing Error
											 , p_loc_company_id
											 , p_plant_id
											 , p_txn_comp_id
											 , p_destination_comp_id
											 , NVL(v_owner_company_id, p_owner_company)
											 , p_local_cost
											  );
					--
					p_approved_qty := p_qty;   --Assign to default value
					p_costing_id := NULL;
					p_cost		:= NULL;
					v_exp		:= NULL;
					--
					p_out_owner_comp_id := NULL;
					p_out_local_cost := NULL;
					RETURN;
				END IF;
		--
		END;
	--
	END IF;

	--
	-- If  no active cost  then pick closed COSG only applicable for
	-- COGS Return
	IF (v_exp = 4802 AND p_type = 4808)
	THEN
		--
		BEGIN
			--
			BEGIN
				SELECT	   c820_qty_on_hand, c820_costing_id, c820_purchase_amt
				, c1900_owner_company_id, DECODE(v_local_cost_cnt, 1, p_local_cost, c820_local_company_cost)
					  INTO v_qty_on_hand, p_costing_id, p_cost
					  , v_owner_company_id, v_local_cost
					  FROM t820_costing
					 WHERE c820_costing_id =
							   (SELECT MAX (c820_costing_id)
								  FROM t820_costing
								 WHERE c205_part_number_id = p_partnum
								   AND c901_costing_type = p_from_costing_type
								   AND c901_status = 4802
								   AND c1900_company_id = v_from_company_id
								   AND NVL(c5040_plant_id, -999) = NVL(v_from_plant_id, - 999))
				FOR UPDATE;
			EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				SELECT	   c820_qty_on_hand, c820_costing_id, c820_purchase_amt,'Y'
				, c1900_owner_company_id, DECODE(v_local_cost_cnt, 1, p_local_cost, c820_local_company_cost)
					  INTO v_qty_on_hand, p_costing_id, p_cost,v_archived_fl
					  , v_owner_company_id, v_local_cost
					  FROM t820_costing_archive
					 WHERE c820_costing_id =
							   (SELECT MAX (c820_costing_id)
								  FROM t820_costing_archive
								 WHERE c205_part_number_id = p_partnum
								   AND c901_costing_type = p_from_costing_type
								   AND c901_status = 4802
								   AND c1900_company_id = v_from_company_id
								   AND NVL(c5040_plant_id, -999) = NVL(v_from_plant_id, - 999)
								  )
				FOR UPDATE;
			END;
		--
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				-- If other type log error and exit
				-- PC-991: To pass the local cost information and it used at repost COGS error
				
				gm_save_costing_error_log (p_partnum
										 , p_qty
										 , p_txn_id
										 , p_from_costing_type
										 , p_to_costing_type
										 , p_txntype
										 , p_party_id
										 , p_updatedby
										 , p_division_id
										 , p_country_id
										 , 102940 --102940 Costing Error
										 , p_loc_company_id
										 , p_plant_id
										 , p_txn_comp_id
										 , p_destination_comp_id
										 , NVL(v_owner_company_id, p_owner_company)
										 , p_local_cost
										  );
				--
				p_approved_qty := p_qty;   --Assign to default value
				p_costing_id := NULL;
				p_cost		:= NULL;
				v_exp		:= NULL;
				--
				p_out_owner_comp_id := NULL;
				p_out_local_cost := NULL;
				RETURN;
		--
		END;
	--
	END IF;

	--
	-- if the value is COGS only and no adjustment then
	-- send the value back and no update
	IF (p_type = 4805)
	THEN
		--
		--DBMS_OUTPUT.put_line ('AMOUNT - ' || p_cost);
		--DBMS_OUTPUT.put_line ('QTY	- ' || p_qty);
		p_approved_qty := p_qty;
		--
		p_out_owner_comp_id := NVL(v_owner_company_id, p_owner_company);
		p_out_local_cost := NVL(v_local_cost, p_local_cost);
		RETURN;
	--
	END IF;

	--
	-- if value is an return then convert the
	-- value to -ive and add the same to costing
	IF (p_type = 4808)
	THEN
		--
		v_qty		:= (-1 * v_qty);
		v_qty_added := -1 * v_qty;
	--
	END IF;

	--
	-- if parts and associated cost are found then
	-- apply cost and flag the txn
	IF (v_exp IS NOT NULL)
	THEN
		--
		v_qty_remaining := v_qty_on_hand - v_qty;

		--
		-- if still the specified line item have the parts
		-- then update the value
		IF (v_qty_remaining > 0)
		THEN
			--
			p_approved_qty := p_qty;

			--
			--If archive flag is Y then for returns Posting the closed layer will be in the archive table ,Hence we open the layer and copy it into T820_costing Table
			IF v_archived_fl = 'Y'
			THEN
				UPDATE t820_costing_archive
				   SET c820_last_updated_by = p_updatedby
					 , c820_last_updated_date = SYSDATE
				 WHERE c820_costing_id = p_costing_id;
			--
			  INSERT
			    INTO T820_COSTING
			        (
			            c820_costing_id      , c205_part_number_id, c901_costing_type
			          , c820_part_received_dt, c820_qty_received  , c820_purchase_amt
			          , c820_qty_on_hand     ,C820_QTY_ADDED, c901_status , c820_delete_fl
			          , c820_created_by      , c820_created_date  , c1900_company_id,C820_PURCHASE_AMT_TEMP
			      	)
				SELECT  s820_costing.NEXTVAL , c205_part_number_id, c901_costing_type
				      , c820_part_received_dt, c820_qty_received  , c820_purchase_amt
				      , (c820_qty_on_hand-v_qty)     ,(C820_QTY_ADDED+v_qty_added), 4801, c820_delete_fl
				      , p_updatedby      , SYSDATE  , c1900_company_id,C820_PURCHASE_AMT_TEMP
				     FROM T820_COSTING_ARCHIVE
				    WHERE c820_costing_id = p_costing_id;
				  --
			ELSE
				UPDATE t820_costing
				   SET c820_qty_on_hand = c820_qty_on_hand - v_qty
					 , c820_qty_added = c820_qty_added + v_qty_added
					 , c901_status = 4801
					 , c820_last_updated_by = p_updatedby
					 , c820_last_updated_date = SYSDATE
				 WHERE c820_costing_id = p_costing_id;
			END IF;
		--
		ELSE
			p_approved_qty := v_qty_on_hand;

			--
			-- close the active transaction
			UPDATE t820_costing
			   SET c820_qty_on_hand = 0
				 , c901_status = 4802
				 , c820_last_updated_by = p_updatedby
				 , c820_last_updated_date = SYSDATE
			 WHERE c820_costing_id = p_costing_id;
		--
		END IF;
		
		--
		-- To perform in case of transfer
		-- Post the costing is associated transaction
		IF (p_to_costing_type IS NOT NULL)
		THEN
		  --PMT-24148 Assigning the local cost of the owner company
		     v_cost_fl := get_rule_value (p_txntype, 'OVERRIDE_LOCAL_COST');
		 IF v_cost_fl = 'Y' 
		 THEN
                v_local_cost := p_cost;
	     END IF;
			gm_save_costing (p_partnum, p_to_costing_type, p_approved_qty, p_cost, p_updatedby, v_to_company_id, v_to_plant_id, v_owner_company_id,  v_local_cost, v_costing_id);
			--
			-- To save the transfer information
			gm_save_costing_track_log (p_costing_id, v_costing_id, p_approved_qty, p_cost, v_to_company_id, v_owner_company_id);
			--
		--
		END IF;
	--
	END IF;
	p_out_owner_comp_id := v_owner_company_id;
	p_out_local_cost := v_local_cost;
--
END gm_fetch_part_cogs_value;
/
