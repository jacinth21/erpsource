/* Formatted on 2012/01/25 18:36 (Formatter Plus v4.8.0) */
-- @"C:\Database\Procedures\Accounting\GM_UPDATE_INVOICE.prc"

CREATE OR REPLACE PROCEDURE gm_update_invoice (
   p_invnum           IN   t508_receivables.c503_invoice_id%TYPE,
   p_paydate          IN   DATE,
   p_payamt           IN   t508_receivables.c508_payment_amt%TYPE,
   p_paymode          IN   t508_receivables.c508_received_mode%TYPE,
   p_paydetails       IN   t508_receivables.c508_details%TYPE,
   p_userid           IN   t503_invoice.c503_created_by%TYPE,
   p_accid            IN   t503_invoice.c704_account_id%TYPE,
   p_custpo           IN   t503_invoice.c503_customer_po%TYPE,
   p_type             IN   t508_receivables.c901_type%TYPE,
   p_pay_details_id   IN   t510_payment_details.c510_payment_id%TYPE
--    Removing the below variable
--    p_message         OUT  VARCHAR2
)
AS
   v_id        NUMBER;
   v_amt       NUMBER (15, 2);
   v_inv_amt   NUMBER (15, 2);
   v_status_cnt NUMBER(1);
   v_tot_inv_amt NUMBER (15, 2);
   v_paid_amt 	 NUMBER (15, 2);
   v_inv_type  t503_invoice.c901_invoice_type%TYPE;
   V_COMPANY_ID T1900_COMPANY.C1900_COMPANY_ID%TYPE;
   --
   v_account_id t704_account.c704_account_id%TYPE;
   v_dealer_id t101_party_invoice_details.c101_party_id%TYPE;
   v_monthly_inv_company_id t1900_company.c1900_company_id%TYPE;
   
  
   CURSOR v_case
   IS
      SELECT t501.c7100_case_info_id case_id
        FROM t501_order t501
       WHERE t501.c503_invoice_id = p_invnum
         AND t501.c501_void_fl IS NULL
         AND t501.c501_delete_fl IS NULL
		 AND (t501.c901_ext_country_id IS NULL
		 OR t501.c901_ext_country_id  in (select country_id from v901_country_codes))
         AND t501.c7100_case_info_id IS NOT NULL
         AND t501.C1900_COMPANY_ID = V_COMPANY_ID;
BEGIN
--     BEGIN
          
          SELECT get_compid_frm_cntx() 
            INTO V_COMPANY_ID 
            FROM dual;
	/*
		SELECT  COUNT(1) INTO v_status_cnt FROM t503_invoice
     			WHERE c503_void_fl IS NULL
     			AND c503_status_fl = 2
        		AND c503_invoice_id = p_invnum;
     	
        -- Whne do Post Payments/CashApplication for invoice that time will throw validation if already closed.
		IF v_status_cnt = 1 THEN
			raise_application_error (-20999, 'Payment already received for Invoice ID <b>'||p_invnum||'</b>');
		END IF;
   	*/
   	
		-- When do Post Payments/CashApplication , will call this procedure and give amount greater than invoice amount should throw validation.
		SELECT  NVL(C503_INV_AMT,0), c704_account_id, c101_dealer_id,
				get_rule_value(c1900_company_id,'MONTHLY_INV_COMP') 
			INTO v_tot_inv_amt, v_account_id, v_dealer_id,
				v_monthly_inv_company_id
		FROM t503_invoice
		where c503_invoice_id=p_invnum;
		 
		SELECT NVL(C503_INV_PYMNT_AMT,0) INTO v_paid_amt FROM t503_invoice
		where c503_invoice_id=p_invnum;

		v_paid_amt := v_paid_amt + p_payamt;
		
	/* user should be able to post an overpayment , comment validation here	 
		SELECT c901_invoice_type
			INTO v_inv_type
		FROM t503_invoice
			WHERE c503_invoice_id = p_invnum
		    AND c503_void_fl   IS NULL
		    FOR UPDATE;
		    
		 -- validate the invoice amount and paymnet amount.  
		IF v_inv_type  = '50202' -- Credit
		THEN
		    IF ( (v_tot_inv_amt < 0 AND p_payamt > 0) OR v_tot_inv_amt > v_paid_amt) THEN
		        raise_application_error ( - 20999, 'The <B>Amount to Post</B> should be less than or equal to the <B>Invoice Amount </B> for invoice: <B>'|| p_invnum||'</B>');
		    END IF;
		ELSE
		    IF v_paid_amt > v_tot_inv_amt THEN
		        raise_application_error ( - 20999, 'The <B>Amount to Post</B> should be less than or equal to the <B>Invoice Amount </B> for invoice: <B>'|| p_invnum||'</B>') ;
		    END IF;
		END IF;
	*/
   		SELECT c508_receivables.NEXTVAL
     	INTO v_id
     	FROM DUAL;
			
   -- Changed to Update the Payment details ID (C510_PAYMENT_ID) to Reference ID column of T508_RECEIVABLES
   INSERT INTO t508_receivables
               (c508_receivables_id, c503_invoice_id, c508_received_date,
                c508_received_mode, c508_payment_amt, c508_details,
                c901_type, c508_reference_id ,c508_created_by, c508_created_date
               )
        VALUES (v_id, p_invnum, p_paydate,
                p_paymode, p_payamt, p_paydetails,
                p_type, p_pay_details_id,p_userid,CURRENT_DATE
               );

   SELECT SUM (c508_payment_amt)
     INTO v_amt
     FROM t508_receivables
    WHERE c503_invoice_id = p_invnum;
	--Update the invoice table and update the C503_INV_PYMNT_AMT column with v_amt;
 	 UPDATE t503_invoice
	SET c503_inv_pymnt_amt = v_amt
	WHERE c503_invoice_id = p_invnum;
	
   SELECT C503_INV_AMT
     INTO v_inv_amt
     FROM t503_invoice
	WHERE c503_invoice_id = p_invnum;

   -- If Paid Amt is Less or More than the Invoice Amt, then C503_Status_fl = 1.
   -- Else C503_Status_fl = 2
   -- Changes due to Credit Memo
   
   IF v_amt = v_inv_amt
   THEN
      UPDATE t503_invoice
         SET c503_status_fl = 2
       WHERE c503_invoice_id = p_invnum;

      FOR v_index IN v_case
      LOOP
         gm_pkg_sm_casebook_txn.gm_check_and_close_case (v_index.case_id);
      END LOOP;
      
   ELSE
      UPDATE t503_invoice
         SET c503_status_fl = 1
       WHERE c503_invoice_id = p_invnum;
   END IF;
   
   -- PC-3144: AR summary report timout (to capture invoice received date)
   -- 5013, 5014, 5019 (Discount, Writeoff, Refund)
   
   IF p_paymode <> 5013 AND p_paymode <> 5014 AND p_paymode <> 5019
   THEN
   		--1 to update the invoice table
   		gm_pkg_ac_invoice.gm_upd_inv_last_payment_received_date (p_invnum, p_paydate, p_userid);
   		
   		-- 2 based on account/Dealer to update the recevied date
   		IF v_monthly_inv_company_id IS NOT NULL
   		THEN
   			gm_pkg_ac_invoice.gm_upd_dealer_last_payment_received_date (v_dealer_id, p_paydate, p_userid);
   		ELSE
   			gm_pkg_ac_invoice.gm_upd_account_last_payment_received_date (v_account_id, p_paydate, p_userid);
   		END IF;
   		
   END IF;
--    END;
END gm_update_invoice;
/
