/* Formatted on 10/30/2013  (Formatter Plus v4.8.0) */
-- @"C:\database\Procedures\Accounting\gm_report_account_ship_setup.prc";

CREATE OR REPLACE PROCEDURE gm_report_account_ship_setup (
	p_accid			IN	   VARCHAR2
  , p_recordset		OUT	   TYPES.cursor_type
)
AS
/*
	  Description		:This procedure is called to get account shipping info
	  Parameters	IN	:p_accid
					OUT	:p_recordset
*/
BEGIN
	OPEN p_recordset
	 FOR
		SELECT C704_ACCOUNT_ID ACCID
			,C704_ACCOUNT_NM ACCNAME
			,C704_SHIP_NAME SHIPNAME
			,C704_SHIP_ADD1 SHIPADD1
			,C704_SHIP_ADD2 SHIPADD2
			,c704_ship_city SHIPCITY
			,C704_SHIP_STATE SHIPSTATE
			,C704_SHIP_COUNTRY SHIPCOUNTRY
			,C704_SHIP_ZIP_CODE SHIPZIP
			,C704_SHIP_ATTN_TO SHIPATTNTO
			,C901_CARRIER CARRIER  -- preffered Carrier value for the particuler account
		FROM T704_ACCOUNT
		WHERE C704_ACCOUNT_ID = p_accid
		AND C704_VOID_FL IS NULL;
		
END gm_report_account_ship_setup;
/