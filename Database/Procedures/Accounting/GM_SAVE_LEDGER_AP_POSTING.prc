/* Formatted on 2011/05/24 15:04 (Formatter Plus v4.8.0) */
--@"C:\Database\Procedures\Accounting\GM_SAVE_LEDGER_AP_POSTING.prc";

CREATE OR REPLACE PROCEDURE gm_save_ledger_ap_posting (
	p_txntype	  IN   t810_ap_txn.c901_account_txn_type%TYPE
  , p_act_type	  IN   t802_account_object_mapping.c901_account_type%TYPE
  , p_acct_date   IN   t810_ap_txn.c810_acct_date%TYPE
  , p_party_id	  IN   t810_ap_txn.c810_party_id%TYPE
  , p_txn_id	  IN   t810_ap_txn.c810_txn_id%TYPE
  , p_partnum	  IN   t810_ap_txn.c205_part_number_id%TYPE
  , p_qty		  IN   t810_ap_txn.c810_qty%TYPE
  , p_dr_amount   IN   t810_ap_txn.c810_dr_amt%TYPE
  , p_cr_amount   IN   t810_ap_txn.c810_cr_amt%TYPE
  , p_updtedby	  IN   t820_costing.c820_created_by%TYPE
  ,p_company_id   IN       t810_ap_txn.c901_company_id%TYPE
  ,p_country_id   IN       t810_ap_txn.c901_country_id%TYPE
)
AS
/************************************************************************************
 * Description			 :This procedure is used to get accounting information
 *						  based on account type mapping and record the ledger
 *						  transaction  into AP Table
 *						  'Level of search'
 *						  *****************
 *						  2 level search
 *						  I.  @ Account Level (If a party is mapped to specific account)
 *						  II. If not found @ Type level
 *************************************************************************************/
--
--
	v_account_element_id t810_ap_txn.c801_account_element_id%TYPE;
	v_company_id_ctx  t1900_company.c1900_company_id%TYPE;
BEGIN
--
	 SELECT get_compid_frm_cntx() 
      	INTO v_company_id_ctx  
      FROM dual;
	--
	SELECT get_partnum_account_element (p_act_type, p_partnum)
	  INTO v_account_element_id
	  FROM DUAL;

	IF v_account_element_id IS NULL
	THEN
			--102940 Costing Error
		gm_save_costing_error_log (p_partnum, p_qty, p_txn_id, p_act_type, -9000, p_txntype, p_party_id, p_updtedby,p_company_id,p_country_id,102940);
	END IF;

	--
	INSERT INTO t810_ap_txn
				(c810_account_txn_id, c901_account_txn_type, c810_party_id, c810_txn_id
			   , c803_period_id, c205_part_number_id
			   , c801_account_element_id, c810_txn_date, c810_acct_date, c810_desc
			   , c810_dr_amt, c810_cr_amt, c810_qty, c810_delete_fl, c810_created_by, c810_created_date,c901_company_id,c901_country_id,c1900_company_id
				)
		 VALUES (s810_ap_txn.NEXTVAL, p_txntype, p_party_id, p_txn_id
			   , NULL	-- Currently period is going to hold null value (handle the same in future release)
					 , p_partnum
			   , v_account_element_id	-- Need to work on the same
									 , TRUNC (SYSDATE), p_acct_date, 'Credit Posting '
			   , p_dr_amount, p_cr_amount, p_qty, 'N', p_updtedby, SYSDATE,p_company_id,p_country_id,v_company_id_ctx
				);
--
END gm_save_ledger_ap_posting;
/
