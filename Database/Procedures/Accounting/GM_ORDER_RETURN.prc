CREATE OR REPLACE PROCEDURE gm_order_return
(
   p_orderid			IN t501_order.c501_order_id%TYPE,
   p_type				IN t506_returns.c506_type%TYPE,
   p_reason				IN t506_returns.c506_reason%TYPE,
   p_parent_order_id	IN t501_order.c501_parent_order_id%TYPE
)
AS
--
-- This procedure is used to update the order return status information
--
	v_check_parent_id  t501_order.c501_order_id%TYPE;
--
BEGIN
	-- Below condition is used to check if the p_parent_order_id
	-- is found in the Database
	IF (p_parent_order_id IS NOT NULL) THEN
	--
		SELECT	c501_order_id
		INTO	v_check_parent_id
		FROM	t501_order
		WHERE	c501_order_id = p_parent_order_id;
	--
	END IF;
	-- IF its a Sales Item Return Mark the order record
	-- to duplicate
	-- 3312 means duplicate order
	IF  (p_orderid IS NOT NULL) AND (p_Reason = '3312')
	THEN
	--
		UPDATE	 t501_order
		SET		-- c501_status_fl		  = 4			-- Invoice Skipped
			 	 c901_order_type	  = 2522		-- Duplicate Order
			--	,c501_parent_order_id = p_parent_order_id	-- parent order id will store in comment log			
		       , c501_last_updated_date = SYSDATE
		WHERE	 c501_order_id		  = p_orderid;
	--
	END IF;
--
END gm_order_return;
-- test vss 
/

