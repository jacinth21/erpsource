CREATE OR REPLACE PROCEDURE GM_REPORT_PENDING_INVOICE
(
    p_Action    IN CHAR,
    p_message   OUT VARCHAR2,
    p_recordset OUT Types.cursor_type)
AS
/*
      Description           :This procedure is called for Project Reporting
      Parameters            :p_column
                            :p_recordset
*/
BEGIN
    OPEN p_recordset FOR
   SELECT
       A.C704_ACCOUNT_ID ACCID,
    B.C503_INVOICE_ID INVID,
    to_char(C503_INVOICE_DATE,'mm/dd/yyyy') INVDT,
    to_char(C503_INVOICE_DATE,'yyyymmdd') INVDTREPORT,
     A.C501_CUSTOMER_PO PO,
    GET_CUSTOMER_PO_AMOUNT(A.C501_CUSTOMER_PO,A.C704_ACCOUNT_ID) COST,
    --SUM(nvl(A.C501_TOTAL_COST,0)+nvl(A.C501_SHIP_COST,0)) COST,
    GET_ACCOUNT_NAME(A.C704_ACCOUNT_ID) NAME,
    B.C503_INV_PYMNT_AMT AMTPAID,
    GET_LOG_FLAG(B.C503_INVOICE_ID,1201) CALL_FLAG
   FROM
     T501_ORDER A, T503_INVOICE B
   WHERE
      A.C501_STATUS_FL IS NOT NULL AND A.C501_STATUS_FL = p_Action
     AND A.C501_CUSTOMER_PO = B.C503_CUSTOMER_PO
     AND A.C704_ACCOUNT_ID = B.C704_ACCOUNT_ID
     AND A.C501_DELETE_FL IS NULL
     AND (A.c901_ext_country_id IS NULL
     OR  A.c901_ext_country_id  in (select country_id from v901_country_codes))
     AND (B.c901_ext_country_id IS NULL
     OR  B.c901_ext_country_id  in (select country_id from v901_country_codes))
      GROUP BY
      A.C501_CUSTOMER_PO,A.C704_ACCOUNT_ID, B.C503_INVOICE_ID, B.C503_INVOICE_DATE
   ORDER BY
      NAME ASC,INVDTREPORT DESC;
  EXCEPTION
  WHEN OTHERS THEN
           p_message := SQLERRM;
END GM_REPORT_PENDING_INVOICE;
/

