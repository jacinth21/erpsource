CREATE OR REPLACE PROCEDURE gm_update_partnum_man_adjust
(
	 p_num    IN  t205_part_number.c205_part_number_id%TYPE,
	 p_qty	  IN  NUMBER,
	 p_userid IN  t215_part_qty_adjustment.c215_created_by%TYPE
)
AS
/****************************************************************************************
 * Description           :This procedure is called to adjust Inventory Values for a part 
 ****************************************************************************************/
--
v_seq	NUMBER;
v_id	VARCHAR2(20);
BEGIN
--
	 SELECT s215_part_qty_adjustment.NEXTVAL 
	 INTO 	v_seq 
	 FROM 	dual;
	 v_id := 'GM-MA-'||v_seq; 
--
  	 INSERT INTO t215_part_qty_adjustment 
	 ( 
	    c215_part_qty_adj_id
	   ,c215_adj_date
	   ,c901_adjustment_source
	   ,c215_comments
	   ,c215_validated_by
	   ,c215_approved_by
	   ,c215_created_by
	   ,c215_created_date
	   ,c215_last_updated_by
	   ,c215_last_updated_date 
	 )VALUES( 
	    v_id
		,SYSDATE
		,4900
		,'Manual Adjustment'
		,'303029'
		,'303009'
		,p_userid
		,SYSDATE
		,NULL
		,NULL
	 ); 
--
	 INSERT INTO t216_part_qty_adj_details 
	 (
	  	 c216_part_qty_adj_id
		,c215_part_qty_adj_id
		,c205_part_number_id
		,c216_qty_to_be_adjusted
		,c216_item_price
		,c216_created_by
		,c216_created_date
	 )VALUES(
	 	 s216_part_qty_adj_details.NEXTVAL
		,v_id
		,p_num
		,p_qty
		,get_inventory_cogs_value(p_num)
		,p_userid
		,SYSDATE
	 );
--
	 gm_update_qty_manual(v_id); 		    
	 gm_save_manual_ledger_posting(v_id);
--
END gm_update_partnum_man_adjust;
/

