/* Formatted }on 2011/04/08 09:24 (Formatter Plus v4.8.0) */
--@"C:\Database\Procedures\Accounting\GM_UPDATE_PO.prc"

CREATE OR REPLACE PROCEDURE gm_update_po (
	p_ordid 		IN		 t501_order.c501_order_id%TYPE
  , p_po			IN		 t501_order.c501_customer_po%TYPE  
  , p_userid		IN		 t501_order.c501_last_updated_by%TYPE
  , p_str			IN		 VARCHAR2
  , p_ord_comments 	IN 		 t501_order.c501_comments%TYPE DEFAULT NULL
  , p_po_amt		IN		 t501_order.c501_po_amount%TYPE 
  , p_copay		    IN		 t501_order. c501_copay %TYPE
  , p_cap_amount	IN		 t501_order. c501_cap_amount %TYPE
  , p_copay_action	IN		 VARCHAR2
  , p_po_status		IN		 t5003_order_revenue_sample_dtls.c901_po_dtls%TYPE
  , p_do_status		IN		 t5003_order_revenue_sample_dtls.c901_do_dtls%TYPE
  , p_message		OUT 	 VARCHAR2
  , p_po_date		IN		 VARCHAR2 DEFAULT NULL ---- Added p_po_date as parameter for PC-3880 New field in record PO Date in GM Italy
 
)
AS
--
/*******************************************************************************************
 *	   Description			 :This procedure is called to update customer PO information
 ********************************************************************************************/
	v_po		   VARCHAR2 (20);
	v_ship		   VARCHAR2 (20);
	v_fl		   VARCHAR2 (1);
	v_confl 	   VARCHAR2 (1);
	v_order_date   DATE;
	v_dept_id	   t101_user.c901_dept_id%TYPE;
	v_old_po	   t501_order.c501_customer_po%TYPE;
	-- to check Revenue Sampling data
	v_strlen         NUMBER                      := NVL (LENGTH (p_str), 0);
    v_string         VARCHAR2 (30000)                              := p_str;
    v_rev_sample_fl  BOOLEAN := FALSE;
    v_ord_type       VARCHAR2(20);
    v_company_id t1900_company.c1900_company_id%TYPE;
	v_po_dtls     t5003_order_revenue_sample_dtls.c901_po_dtls%TYPE;
    v_do_dtls     t5003_order_revenue_sample_dtls.c901_do_dtls%TYPE;
	v_old_po_dtls     t5003_order_revenue_sample_dtls.c901_po_dtls%TYPE;
    v_old_do_dtls     t5003_order_revenue_sample_dtls.c901_do_dtls%TYPE;
    v_date_fmt  VARCHAR2 (20); --PC-3880 New field in record PO Date in GM Italy
    v_po_date DATE;--PC-3880 New field in record PO Date in GM Italy
BEGIN
-- Get v_date_fmt for PC-3880 New field in record PO Date in GM Italy
	SELECT TRUNC (c501_order_date), get_dept_id (p_userid), NVL(c501_customer_po,'-999') , c901_order_type
		, get_compid_frm_cntx(),get_compdtfmt_frm_cntx ()
	  INTO v_order_date, v_dept_id, v_old_po , v_ord_type
	  	, v_company_id,v_date_fmt
	  FROM t501_order
	 WHERE c501_order_id = p_ordid;

	--if the order date is passed then only A/R should be able to edit the PO details in US Country
	IF NVL(get_rule_value_by_company ('US_1000','COUNTRY', v_company_id),'-999') = 'Y' AND v_order_date != TRUNC (CURRENT_DATE) AND v_dept_id != 2020  AND v_dept_id != 2005
	THEN
		raise_application_error ('-20309', '');
	--Cannot change the PO Details after the day DO was raised
	END IF;
	-- Save the order to invoice batch (based on the PO)
	gm_pkg_ac_ar_batch_trans.gm_sav_batch_updated_po (p_ordid, NVL(p_po,'-999'), 18751,p_userid);
	--PC-3880 New field in record PO Date in GM Italy
	IF p_po_date IS NOT NULL THEN
	select to_date (p_po_date, v_date_fmt)
	into  v_po_date
	from dual;
	END IF;
	
	UPDATE t501_order
	   SET c501_customer_po = p_po
	   	 ,c501_po_amount = p_po_amt
	     , c501_comments  = p_ord_comments
	     ,c501_copay = DECODE(p_copay_action,'CopyAction',p_copay,c501_copay)
         ,c501_cap_amount  = DECODE(p_copay_action,'CopyAction',p_cap_amount,c501_cap_amount)
		 , c501_last_updated_by = p_userid
		 , c501_last_updated_date = CURRENT_DATE
		 ,c501_customer_po_date = v_po_date --PC-3880 New field in record PO Date in GM Italy
	 WHERE c501_order_id = p_ordid;
	 
	IF v_ord_type = '101260' -- Ack Order
       THEN                      
              UPDATE t501_order
                 SET c501_customer_po = p_po
                 	,c501_po_amount = p_po_amt
                    ,c501_last_updated_by = p_userid
                    ,c501_last_updated_date = CURRENT_DATE
                    ,c501_customer_po_date = v_po_date --PC-3880 New field in record PO Date in GM Italy
                    
               WHERE c501_order_id IN (SELECT t501.c501_order_id 
                                         FROM t501_order t501, t907_shipping_info t907 
                                        WHERE t501.c501_order_id =  t907.c907_ref_id
                                          AND t501.c501_ref_id IN (SELECT c907_ref_id 
                                                                     FROM t907_shipping_info 
                                                                    WHERE c907_ref_id = p_ordid 
                                                                      AND c907_status_fl <> '40' 
                                                                      AND c907_void_fl IS NULL)   
                                          AND c907_status_fl <> '40'
                                          AND c501_void_fl IS NULL  
                                          AND c503_invoice_id IS NULL
                    );
   
       END IF;

	 IF v_old_po <> NVL(p_po,'-999')
	 THEN
		gm_save_status_details (p_ordid, '', p_userid, NULL, CURRENT_DATE, 91174, 91102);
	 END IF;

	--
	-- To update the child Orders if they havent been invoiced - James Oct 26,2007
	-- Sales Discount orders need not be Po ed -Mihir
	UPDATE t501_order
	   SET c501_customer_po = p_po
	   	 ,c501_po_amount = p_po_amt
	   	 , c501_comments  = p_ord_comments
		 , c501_last_updated_by = p_userid
		 , c501_last_updated_date = CURRENT_DATE
		 ,c501_customer_po_date = v_po_date --PC-3880 New field in record PO Date in GM Italy
	 WHERE c501_order_id IN (
				  SELECT c501_order_id
					FROM t501_order
				   WHERE c501_parent_order_id = p_ordid AND c503_invoice_id IS NULL
						 AND NVL (c901_order_type, -999) NOT IN (2535,102080)   --2535 Sales Discount, 102080 OUS Distributor orders has system generated PO
						 AND NVL (c901_order_type, -9999) NOT IN (
			                SELECT t906.c906_rule_value
			                  FROM t906_rules t906
			                 WHERE t906.c906_rule_grp_id = 'ORDTYPE'
			                   AND c906_rule_id = 'EXCLUDE')
						 )
			AND (c901_ext_country_id IS NULL
			OR c901_ext_country_id  in (select country_id from v901_country_codes));

	-- to check the revenue sampling is change.
		--Call the new Procedure as Part of PMT-57310,
		--To split the PO and DO details or save the DO and PO details. 
		
			BEGIN
				SELECT NVL (c901_po_dtls, '-999'),NVL (c901_do_dtls, '-999')
					INTO v_old_po_dtls,  v_old_do_dtls
				FROM t5003_order_revenue_sample_dtls
					WHERE c501_order_id = p_ordid
					AND c5003_void_fl           IS NULL;
			EXCEPTION
			WHEN NO_DATA_FOUND THEN
				v_old_po_dtls := '-999';
				v_old_do_dtls := '-999';
			END;
			--to insert/update the PO and DO Details
			gm_pkg_ac_ar_revenue_sampling.gm_sav_revenue_sampling_dtls(p_ordid,p_po_status,p_do_status,p_userid);
 			-- to set the flag as true. based on this flag we insert the order status log
 			IF (v_old_po_dtls <> NVL (p_po_status, '-999')) OR (v_old_do_dtls <> NVL (p_do_status, '-999')) THEN
 				v_rev_sample_fl          := TRUE;
 			END IF; -- end v_old_attr_value
	
	-- if only chagned Revenue Sampling - to insert the revenue sample comments log.
     IF v_old_po = NVL(p_po,'-999') AND v_rev_sample_fl = TRUE
     THEN
       	gm_save_status_details (p_ordid, '', p_userid, NULL, CURRENT_DATE, 91180, 91102); --- 91180 Revenue Track Updated
     END IF; -- end v_rev_sample_fl
--
END gm_update_po;
/





