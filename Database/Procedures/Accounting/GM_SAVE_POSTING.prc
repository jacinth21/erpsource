CREATE OR REPLACE PROCEDURE GM_SAVE_POSTING
(
	 p_table_ref        IN 	t805_posting_table_ref.c805_table_identifier%TYPE
	,p_txntype			IN	t810_posting_txn.c901_account_txn_type%TYPE
	,p_account_type		IN	t802_account_object_mapping.c901_account_type%TYPE	
	,p_acc_date			IN	t810_posting_txn.c810_acct_date%TYPE
	,p_party_id			IN	t810_posting_txn.c810_party_id%TYPE
	,p_txn_id			IN	t810_posting_txn.c810_txn_id%TYPE
	,p_partnum 			IN	t810_posting_txn.c205_part_number_id%TYPE
	,p_costing_id		IN	t810_posting_txn.c820_costing_id%TYPE
	,p_qty				IN	t810_posting_txn.c810_qty%TYPE
	,p_dr_amt			IN	t810_posting_txn.c810_dr_amt%TYPE
	,p_cr_amt			IN	t810_posting_txn.c810_cr_amt%TYPE
	,p_updatedby		IN	t810_posting_txn.c810_created_by%TYPE
	,p_posting_type		IN	t804_posting_ref.c901_posting_type%TYPE
	,p_cr_dr_post_type	IN	VARCHAR
	,p_company_id       IN  t810_posting_txn.c901_company_id%TYPE
	,p_country_id       IN  t810_posting_txn.c901_country_id%TYPE
	,p_local_company_id IN t820_costing.c1900_owner_company_id%TYPE
	,p_plant_id IN t5040_plant_master.c5040_plant_id%TYPE
	,p_owner_company_id IN t820_costing.c1900_owner_company_id%TYPE
	,p_txn_company_id IN t810_posting_txn.c1900_txn_company_id%TYPE
	,p_destination_company_id IN t820_costing.c1900_owner_company_id%TYPE
	,p_local_dr_amt  IN t810_posting_txn.c810_cr_amt%TYPE
	,p_local_cr_amt  IN t810_posting_txn.c810_cr_amt%TYPE
)
AS
/************************************************************************************
 * Description           :This procedure is used to get all associated table 
 *						  and post the value in required tables 
 *
 **************************************************************************************/
--
	v_costing_id t810_posting_txn.c820_costing_id%TYPE;
	v_desc VARCHAR2(100);
BEGIN
--
	v_costing_id:=p_costing_id;
	 IF (p_table_ref = 'CONS')
     THEN
	 	v_desc:=' Ledger Posting ';
	 ELSE
	 	v_desc:=' Credit Posting ';
	 END IF;
	 
	 IF (p_table_ref = 'AP')
     THEN
     	v_costing_id:=NULL;
     END IF;
	 
	 gm_save_ledger_txn_posting (p_txntype,
                                   p_account_type,
                                   p_acc_date,
                                   p_party_id,
                                   p_txn_id,
                                   p_partnum,
                                   v_costing_id,
                                   p_qty,
                                   p_dr_amt,
                                   p_cr_amt,
                                   p_updatedby,
                                   --p_posting_type,
                                   --p_cr_dr_post_type,
                                   p_company_id,
                                   p_country_id,
                                   v_desc,
                                   p_local_company_id,
                                   p_plant_id,
                                   p_owner_company_id,
                                   p_txn_company_id,
                                   p_destination_company_id,
								   p_local_dr_amt,
								   p_local_cr_amt
                                  );
--
END GM_SAVE_POSTING;
/

