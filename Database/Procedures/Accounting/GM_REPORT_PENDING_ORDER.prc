CREATE OR REPLACE PROCEDURE GM_REPORT_PENDING_ORDER
(
	   p_message   OUT VARCHAR2,
	   p_recordset OUT Types.cursor_type)
AS
/*
      Description           :This procedure is called for Project Reporting
      Parameters            :p_column
                            :p_recordset
*/
BEGIN
   	OPEN p_recordset FOR
		SELECT
			   C704_ACCOUNT_ID ACCID,
			   C501_ORDER_ID ORDID,
			   GET_ACCOUNT_NAME(C704_ACCOUNT_ID) NAME,
			   to_char(C501_ORDER_DATE_TIME,'MM/DD/YYYY HH:MI AM') ORDDT,
			   to_char(C501_ORDER_DATE_TIME,'yyyymmdd HH:MI AM') ORDDTREPORT,
			   GET_USER_NAME(C501_CREATED_BY) CS,
			   GET_TOTAL_ORDER_AMT(C501_ORDER_ID) COST,
			   decode(trunc((sysdate-C501_ORDER_DATE)/30),0,'','Y') FL
		FROM
			 T501_ORDER
		WHERE
			  C501_STATUS_FL IS NOT NULL AND C501_STATUS_FL = 2
			  AND C501_DELETE_FL IS NULL
			  AND (c901_ext_country_id IS NULL
			  OR c901_ext_country_id  in (select country_id from v901_country_codes))
		ORDER BY
			  NAME, ORDDTREPORT DESC;
	 EXCEPTION
	 WHEN OTHERS THEN
	          p_message := SQLERRM;
END GM_REPORT_PENDING_ORDER;
/

