CREATE OR REPLACE PROCEDURE GM_UPDATE_DHR_PAYMENT_MULTIPLE
(
	   p_VendId				IN T406_PAYMENT.C301_VENDOR_ID%TYPE,
	   p_InvoiceId			IN T406_PAYMENT.C406_INVOICE_ID%TYPE,
	   p_PayDate			IN VARCHAR2,
 	   p_PayMode			IN T406_PAYMENT.C406_PAYMENT_MODE%TYPE,
	   p_PayAmt				IN T406_PAYMENT.C406_PAYMENT_AMOUNT%TYPE,
	   p_CheckNum			IN T406_PAYMENT.C406_CHECK_NUMBER%TYPE,
	   p_Comments			IN T406_PAYMENT.C406_COMMENTS%TYPE,
	   p_DHRStr 			IN VARCHAR2,
	   p_UserId				IN T406_PAYMENT.C406_CREATED_BY%TYPE,
	   p_Message			OUT	VARCHAR2
)
AS
v_id		 NUMBER;
v_DHR		 VARCHAR2(20);
v_strlen     NUMBER          := NVL(LENGTH(p_DHRStr),0);
v_string     VARCHAR2(30000) := p_DHRStr;
BEGIN
    --
	SELECT S406_PAYMENT.NEXTVAL INTO v_id FROM DUAL;
	--
	INSERT INTO T406_PAYMENT(
		 c406_payment_id
		,c406_invoice_id
		,c301_vendor_id
		,c406_entered_dt
		,c406_payment_mode
		,c406_check_number
		,c406_payment_date
		,c406_payment_amount
		,c406_comments
		,c406_created_date
		,c406_created_by) 
		VALUES (
		v_id
		,p_InvoiceId
		,p_VendId
		,trunc(SYSDATE)
		,p_PayMode
		,p_CheckNum
		,to_date(p_PayDate,'mm/dd/yyyy')
		,p_PayAmt
		,p_Comments
		,SYSDATE
		,p_UserId
	);
	--
	p_Message:= v_id;
	--	
	  WHILE INSTR(v_string,'^') <> 0
	  LOOP
	   --
	   v_DHR  := NULL;
	   v_DHR := SUBSTR(v_string,1,INSTR(v_string,'^')-1);
	   v_string    := SUBSTR(v_string,INSTR(v_string,'^')+1);
	   --
		UPDATE T408_DHR
		SET 
		  C406_PAYMENT_ID = v_id,
		  C408_PAYMENT_FL = 'Y'
		WHERE C408_DHR_ID = v_DHR;	   
		--
	  END LOOP;
	  IF v_string IS NOT NULL THEN
		UPDATE T408_DHR
		SET 
		  C406_PAYMENT_ID = v_id,
		  C408_PAYMENT_FL = 'Y'
		WHERE C408_DHR_ID = v_string;
	 END IF;	  	 
	  
	--
END GM_UPDATE_DHR_PAYMENT_MULTIPLE;
/

