CREATE OR REPLACE PROCEDURE GM_SAVE_MONTHLY_INV_ATTRIBUTE
( 
  p_batch_id t9600_batch.c9600_batch_id%TYPE
 ,p_inv_id t503b_invoice_attribute.C503_INVOICE_ID%TYPE
 ,p_inv_close_dt t503b_invoice_attribute.C901_INVOICE_CLOSING_DATE%TYPE
 ,p_dt_issue_from  t503b_invoice_attribute.C503B_DATE_OF_ISSUE_FROM%TYPE
 ,p_dt_issue_to  t503b_invoice_attribute.C503B_DATE_OF_ISSUE_TO%TYPE
 ,p_dealer_id t101_party.c101_party_id%TYPE
 ,p_account_id t503_invoice.c704_account_id%TYPE
 ,p_user_id   t503b_invoice_attribute.C503B_CREATED_BY%TYPE
)
AS
/***************************************************************************************
 * Description           :This procedure is used to save monthly invoice report details
  ****************************************************************************************/
--
v_opening_bal NUMBER;
v_paid_inv_amt NUMBER;
v_transaction_amt NUMBER;
v_tax NUMBER;
v_tax_amt NUMBER;
v_attr_count NUMBER := 0;
v_dealer_prev_inv t503b_invoice_attribute.C503_INVOICE_ID%TYPE;
v_acct_prev_inv t503b_invoice_attribute.C503_INVOICE_ID%TYPE;
v_company_id t1900_company.c1900_company_id%TYPE;
v_total_sales_amt NUMBER;
v_ending_ar_amt NUMBER;

BEGIN
--

		SELECT get_compid_frm_cntx()
		  INTO v_company_id 
		  FROM dual;
	  
		SELECT count(*) INTO v_attr_count
		  FROM t503b_invoice_attribute
		WHERE c503_invoice_id = p_inv_id
		  AND c503b_void_fl    IS NULL;
	
	-- Get the Dealer/Account  previous invoice	  
		IF p_dealer_id IS NOT NULL
		THEN 
			BEGIN
				SELECT t503a.Inv_ID INTO v_dealer_prev_inv 
				FROM
						    ( SELECT t503.c503_invoice_id Inv_ID
							    From T503_Invoice T503
							   WHERE c101_dealer_id = p_dealer_id
							     AND c1900_company_id = v_company_id
							     And C503_Void_Fl    Is Null
							     And C503_Invoice_Id <> p_inv_id
							     AND c503_invoice_date < p_dt_issue_from
							     AND C901_Invoice_Type in ('50200','26240439')
							ORDER BY C503_created_date DESC
						    )t503a
				WHERE Rownum = 1;
			 EXCEPTION WHEN NO_DATA_FOUND THEN
			 	v_dealer_prev_inv := NULL;
		 	 END;
		ELSE
			BEGIN
				SELECT t503a.Inv_ID  INTO v_acct_prev_inv  
				FROM
					    ( SELECT t503.c503_invoice_id Inv_ID
						    FROM t503_invoice t503
						   WHERE c704_account_id = p_account_id
						     AND c1900_company_id = v_company_id
						     AND c503_void_fl    IS NULL
						     AND c503_invoice_id <> p_inv_id
						     AND c503_invoice_date < p_dt_issue_from
						     AND C901_Invoice_Type in ('50200','26240439')
						ORDER BY C503_created_date DESC
					    )t503a
			    WHERE rownum = 1;
			 EXCEPTION WHEN NO_DATA_FOUND THEN
				 	v_acct_prev_inv := NULL;
			 END;
		END IF;
		  
		  
 IF v_attr_count = 0 
 THEN
			--  Getting the opening balance for the Dealer/Account by carrying the Ending AR balance of previous invoice,
			--  Otherwise setting opening balance as zero 
		 BEGIN
		    IF p_dealer_id IS NOT NULL AND v_dealer_prev_inv IS NOT NULL
		    THEN
			    	SELECT NVL(c503b_ending_ar_bal,0) INTO v_opening_bal
					  FROM t503b_invoice_attribute 
					 WHERE c503_invoice_id = v_dealer_prev_inv
					   AND c503b_void_fl IS NULL;
		    
			ELSIF p_account_id IS NOT NULL AND v_acct_prev_inv IS NOT NULL
			THEN	
					SELECT NVL(c503b_ending_ar_bal,0) INTO v_opening_bal
					  FROM t503b_invoice_attribute 
					 WHERE c503_invoice_id = v_acct_prev_inv
					   AND c503b_void_fl IS NULL;
			ELSE
				v_opening_bal := 0;
			END IF;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			v_opening_bal := 0;
		END;

	SELECT get_dealer_acct_inv_paid_amt(p_dealer_id,p_account_id,p_dt_issue_from,p_dt_issue_to)
	   INTO v_paid_inv_amt FROM DUAL;
	   
	SELECT GET_DEALER_ACCT_SALE_IN_MONTH(p_batch_id,p_dealer_id,p_account_id,p_dt_issue_from,p_dt_issue_to,p_inv_id) -- PMT-40781 excluded orders are listing
	   INTO v_transaction_amt FROM DUAL;

	 v_tax := get_rule_value_by_company (v_company_id,'VAT_RATE', v_company_id);
	
	-- Fetch tax amount from GET_DEALER_ACCT_TAX_IN_MONTH function , Added for PMT-21186
	 SELECT NVL(GET_DEALER_ACCT_TAX_IN_MONTH(p_dealer_id,p_account_id,p_dt_issue_from,p_dt_issue_to,v_tax,p_inv_id),0) -- PMT-42615
	 INTO v_tax_amt FROM DUAL; 
	
	   
	-- Sum of Transaction Amount and Tax amount.
	v_total_sales_amt := v_transaction_amt + v_tax_amt;
	
	--Ending AR Balance: Sum of Begining Bal(A) (Unpaid Amount) and Total Sales amount(B) minus Paid Amount(C), E= (A+B)-C
	v_ending_ar_amt := ((v_opening_bal + v_total_sales_amt) - v_paid_inv_amt);
	
	 INSERT INTO t503b_invoice_attribute (
	   C503B_INVOICE_ATTRIBUTE_ID,
	   C503_INVOICE_ID,
	   C901_INVOICE_CLOSING_DATE,
	   C503B_DATE_OF_ISSUE_FROM,
	   C503B_DATE_OF_ISSUE_TO,
	   C503B_UNPAID_INVOICE_AMT,
	   C503B_PAID_INVOICE_AMT,
	   C503B_TRANSACTION_AMT,
	   C503B_CONSUMPTION_TAX_AMOUNT,
	   C503B_TOTAL_SALES_AMT,
	   C503B_ENDING_AR_BAL,
	   C503B_CREATED_DATE,
	   C503B_CREATED_BY
	 )
	 VALUES
	 (  
	   S503B_INVOICE_ATTRIBUTE_ID.NEXTVAL
	  ,p_inv_id
	  ,p_inv_close_dt
	  ,p_dt_issue_from
	  ,p_dt_issue_to
	  ,v_opening_bal
	  ,v_paid_inv_amt
	  ,v_transaction_amt
	  ,v_tax_amt
	  ,v_total_sales_amt
	  ,v_ending_ar_amt
	  ,CURRENT_DATE
	  ,p_user_id
	 
	 );
	 
END IF;
 --
--
END GM_SAVE_MONTHLY_INV_ATTRIBUTE;
/

