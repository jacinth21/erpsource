CREATE OR REPLACE PROCEDURE GM_REPORT_ACCOUNT_RECEIVABLE
( p_type  IN VARCHAR2,
 p_message   OUT VARCHAR2,
 p_recordset OUT Types.cursor_type
)
AS
/****************************************************************************************
 * Description           :This procedure is called to get ACCOUNT RECEIVABLE INFROMATION
 * Parameters            : p_type  IN D -- Detail Report  S -- Summary Information
 *       p_message OUT Error Message IF Any
 *       p_recordset OUT Values with AR Information
 *****************************************************************************************/
BEGIN
--
 IF (p_type = 'D') THEN
 --
     OPEN p_recordset FOR
   SELECT  T501.C704_ACCOUNT_ID ID,
     GET_ACCOUNT_NAME(T501.C704_ACCOUNT_ID) NAME,
     SUM(DECODE(
       SIGN( (TRUNC(SYSDATE) - C501_SHIPPING_DATE) - 30),
       -1,C501_TOTAL_COST ,0 )) CURRENTVALUE,
     SUM(DECODE(
       SIGN( (TRUNC(SYSDATE) - C501_SHIPPING_DATE) - 29 ),  1,
     DECODE(SIGN( (TRUNC(SYSDATE) - C501_SHIPPING_DATE) - 61 ), -1, NVL(C501_TOTAL_COST,0) ,0) ,0 )) ONE_THIRTY,
     SUM(DECODE(
       SIGN( (TRUNC(SYSDATE) - C501_SHIPPING_DATE) - 60 ),  1,
     DECODE(SIGN( (TRUNC(SYSDATE) - C501_SHIPPING_DATE) - 91 ), -1, NVL(C501_TOTAL_COST,0) ,0) ,0 )) THIRTYONE_SIXTY,
     SUM(DECODE(
       SIGN( (TRUNC(SYSDATE) - C501_SHIPPING_DATE) - 90 ),  1,
     DECODE(SIGN( (TRUNC(SYSDATE) - C501_SHIPPING_DATE) - 121 ), -1, NVL(C501_TOTAL_COST,0) ,0) ,0 )) SIXTYONE_NINETY,
     SUM(DECODE(
       SIGN( (TRUNC(SYSDATE) - C501_SHIPPING_DATE) - 120),
       1,C501_TOTAL_COST ,0 )) GREATER_NINETY
   FROM T501_ORDER T501
   WHERE T501.C501_STATUS_FL = 4
   AND T501.C501_DELETE_FL IS NULL
   AND T501.C501_VOID_FL IS NULL
   AND (T501.c901_ext_country_id IS NULL
   OR T501.c901_ext_country_id  in (select country_id from v901_country_codes))
   GROUP BY T501.C704_ACCOUNT_ID
   ORDER BY NAME;
 --
 ELSIF (p_type = 'S') THEN
 --
     OPEN p_recordset FOR
   SELECT  SUM(DECODE(
       SIGN( (TRUNC(SYSDATE) - C501_SHIPPING_DATE) - 30),
       -1,C501_TOTAL_COST ,0 )) CURRENTVALUE,
     SUM(DECODE(
       SIGN( (TRUNC(SYSDATE) - C501_SHIPPING_DATE) - 29 ),  1,
     DECODE(SIGN( (TRUNC(SYSDATE) - C501_SHIPPING_DATE) - 61 ), -1, NVL(C501_TOTAL_COST,0) ,0) ,0 )) ONE_THIRTY,
     SUM(DECODE(
       SIGN( (TRUNC(SYSDATE) - C501_SHIPPING_DATE) - 60 ),  1,
     DECODE(SIGN( (TRUNC(SYSDATE) - C501_SHIPPING_DATE) - 91 ), -1, NVL(C501_TOTAL_COST,0) ,0) ,0 )) THIRTYONE_SIXTY,
     SUM(DECODE(
       SIGN( (TRUNC(SYSDATE) - C501_SHIPPING_DATE) - 90 ),  1,
     DECODE(SIGN( (TRUNC(SYSDATE) - C501_SHIPPING_DATE) - 121 ), -1, NVL(C501_TOTAL_COST,0) ,0) ,0 )) SIXTYONE_NINETY,
     SUM(DECODE(
       SIGN( (TRUNC(SYSDATE) - C501_SHIPPING_DATE) - 120),
       1,C501_TOTAL_COST ,0 )) GREATER_NINETY
   FROM T501_ORDER T501
   WHERE T501.C501_STATUS_FL = 4
   AND T501.C501_DELETE_FL IS NULL
   AND (T501.c901_ext_country_id IS NULL	
   OR T501.c901_ext_country_id  in (select country_id from v901_country_codes))
   AND  T501.C501_VOID_FL IS NULL;
 --
 END IF;
--
EXCEPTION
WHEN OTHERS THEN
--
 p_message := SQLERRM;
--
END GM_REPORT_ACCOUNT_RECEIVABLE;
/

