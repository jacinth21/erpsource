/* Formatted on 2011/01/10 18:19 (Formatter Plus v4.8.0) */
-- @"C:\database\Procedures\Accounting\gm_save_credit_order.prc"

CREATE OR REPLACE PROCEDURE gm_save_credit_order (
	p_raid			 IN 	  t506_returns.c506_rma_id%TYPE
  , p_creditdt		 IN 	  DATE
  , p_usercomments	 IN 	  t501_order.c501_comments%TYPE
  , p_userid		 IN 	  t501_order.c501_created_by%TYPE
  , p_credit_str     IN       CLOB
  , p_new_inv_id	 OUT	  t503_invoice.c503_invoice_id%TYPE
  , p_type           IN       VARCHAR2 DEFAULT 'N'
)
/****************************************************************************
 * Description : This procedure is called to update the order return process
 *			  If its a duplicate entry will reverse the entry
 *
 * Parameters	: p_raid Order return id
 *
 *****************************************************************************/
AS
--
	v_type		   t506_returns.c506_type%TYPE;
	v_reason	   t506_returns.c506_reason%TYPE;
	v_order_id	   t506_returns.c501_order_id%TYPE;
	v_rev_order_id t506_returns.c501_order_id%TYPE;
	v_invid 	   t503_invoice.c503_invoice_id%TYPE;
	v_newinvid	   t503_invoice.c503_invoice_id%TYPE;
	v_accid 	   t503_invoice.c704_account_id%TYPE;
	v_terms 	   t503_invoice.c503_terms%TYPE;
	v_cust_po	   t503_invoice.c503_customer_po%TYPE;
	v_toal_cost    t501_order.c501_total_cost%TYPE;
	v_order_type   t501_order.c901_order_type%TYPE;
	v_act_ship	   t501_order.c501_ship_cost%TYPE;	 -- Actual shipping cost
	v_rev_act_ship t501_order.c501_ship_cost%TYPE := NULL;	 -- Reversed Shipping cost
	v_order_count  NUMBER (2);
	v_cnt		   NUMBER;
	v_exclude_vat  VARCHAR2(5);
	v_rule_value   VARCHAR2(20);
	v_company_id        t1900_company.c1900_company_id%TYPE;
	-- check const. flag  
    v_con_vat_fl	VARCHAR2(1);
	v_const_fl VARCHAR2(1);
	--
	v_used_lot_cnt NUMBER;
	v_order_usage_fl VARCHAR2(10);
	v_contract T704d_Account_Affln.c901_contract%TYPE;
	v_acctid   t501_order.c704_account_id%TYPE;
	v_tmp_order_id  t506_returns.c501_order_id%TYPE;
	v_new_order_id  t506_returns.c501_order_id%TYPE;
	v_ordatt	   VARCHAR2(2000) :='';
	v_comp_snyc_id   t1900_company.c1900_company_id%TYPE;
	v_parent_order_id t506_returns.c501_order_id%TYPE;
	v_acc_cmp_id    t704_account.c704_account_id%TYPE;
    v_user_plant_id   t501_order.c5040_plant_id%TYPE;
	--
	v_org_parent_order_id t501_order.c501_parent_order_id%TYPE;
BEGIN
    -- 
	SELECT c506_type, c506_reason, c501_order_id, c1900_company_id
	  INTO v_type, v_reason, v_order_id, v_company_id
	  FROM t506_returns
	 WHERE c506_rma_id = p_raid AND c506_void_fl IS NULL;
	--
	-- If Sales Return (3300) Then Perform the below
	-- 3300 maps to sales and remaining id maps to consignment
   IF( v_type != '3300' AND p_type='N' )          --- p_type='N' used PC-135 -Voiding Prior Month backorders
	THEN
		RETURN;
	END IF;
	SELECT COUNT (1)
	  INTO v_order_count
	  FROM t501_order
	 WHERE c506_rma_id = p_raid;
	IF (v_order_count <> 0)
	THEN
		-- Error message is   RA already credited cant recredit
		raise_application_error (-20036, '');
	END IF;

	--
	-- Check if the order is invoice if yes then
	-- mark the order as
	-- Need to revalidate the function
	BEGIN
		--
		SELECT c503_invoice_id, c501_ship_cost,c704_account_id
			, get_parent_order_id (c501_order_id)
			, c501_parent_order_id
		  INTO v_invid, v_act_ship,v_acctid
		  	, v_parent_order_id
		  	, v_org_parent_order_id
		  FROM t501_order
		 WHERE c501_order_id = v_order_id
		  AND c501_void_fl   IS NULL
          AND c501_delete_fl IS NULL;
	--
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_invid 	:= NULL;
	END;
	--getting the company id to perform oprations only for Chennai / Delhi
		SELECT get_rule_value_by_company(v_company_id,'GST_COMP',v_company_id) 
	  INTO v_comp_snyc_id 
	  FROM DUAL; 

	-- Defaulting to the Order type for Sales Adjustment (2529)
	v_order_type := 2529;

	--
	IF v_invid IS NOT NULL
	THEN
		--
		UPDATE t506_returns
		   SET c503_invoice_id = v_invid
			 , c506_credit_memo_id = v_invid || 'C'
			 , c506_ac_credit_dt = NVL (p_creditdt, TRUNC (CURRENT_DATE))
			 , c506_last_updated_by = p_userid
			 , c506_last_updated_date = CURRENT_DATE
		 WHERE c506_rma_id = p_raid AND c506_void_fl IS NULL;

		-- Since the Invoice ID is NOT NULL, the Order type is set to Credit Memo (2528)
		v_order_type := 2528;

		-- Get the necessary data from T503_INVOICE for updating with the new Invoice
		SELECT c704_account_id, c503_terms, c503_customer_po
		  INTO v_accid, v_terms, v_cust_po
		  FROM t503_invoice
		 WHERE c503_invoice_id = v_invid;

		-- Adding the below code to bypass the condition (v_reason = 3310) OR (v_reason = 3311) below which sets the order_type to 2523
		SELECT 'GM' || s501_order.NEXTVAL || 'R'
		  INTO v_rev_order_id
		  FROM DUAL;
	--
	ELSE
		--
		UPDATE t506_returns
		   SET c506_ac_credit_dt = NVL (p_creditdt, TRUNC (CURRENT_DATE))
		     , c506_last_updated_by = p_userid
			 , c506_last_updated_date = CURRENT_DATE
		 WHERE c506_rma_id = p_raid AND c506_void_fl IS NULL;
	--
	END IF;
	
	v_rule_value := NVL(get_rule_value (v_reason, 'SHOW-ICT-CREDIT-SALE'),'N') ;
	
	--
	-- If the order is duplicate
	IF (v_reason = 3312)
	THEN
		--
	-- Assiging the order id to a temp variable
 	    v_tmp_order_id := v_order_id;

 	-- Calling the procedure to check if the order exists.    
		gm_pkg_ac_invoice.gm_chk_order(v_tmp_order_id,'D','1',v_new_order_id);
	
		v_rev_order_id:=v_new_order_id;
		v_rev_act_ship := -1 * v_act_ship;
		v_order_type := 2522;
	--
	
	  ELSIF (v_rule_value = 'Y')  --3310 -Wrong Order,3311 - Broken, 3319 -  Wear and Tear, 3253 -  QA Evaluation, 26240385  -  Field Corrective Action
	THEN
		--
		SELECT 'GM' || s501_order.NEXTVAL || 'R'
		  INTO v_rev_order_id
		  FROM DUAL;

		IF v_invid IS NULL
		THEN
			v_order_type := 2529;
			--
			-- PMT-24821 : Back order, Returns Qty not reduce the invoice paperwork. (26240469,'Sales Adjustment Order - Mapping')
			-- Based on Order id to mapping the Return Order id (Attribute table).
			-- Using Function  GET_INVOICE_PART_QTY, we are getting the Part Qty - based on this mapping we can reduce the Qty
			
				gm_pkg_cm_order_attribute.gm_sav_order_attribute (v_order_id,
	                                                              26240469,
	                                                              v_rev_order_id,
	                                                              NULL,
	                                                              p_userid
	                                                             );
             
		ELSIF v_order_type <> 2528
		THEN
			v_order_type := 2523;
		END IF;
	END IF;

	--
	-- To Fetch the Total return value
	--
	SELECT -1 * SUM (c507_item_qty * c507_item_price)
	  INTO v_toal_cost
	  FROM t507_returns_item
	 WHERE c506_rma_id = p_raid AND c507_status_fl <> 'Q';
	--
	--getting contract value from account
	  SELECT GET_ACCT_CONTRACT(v_acctid) 
	    INTO v_contract 
	    FROM DUAL; 
	     BEGIN
	 --getting company id and plant id mapped from account
           SELECT c1900_company_id,gm_pkg_op_plant_rpt.get_default_plant_for_comp(gm_pkg_it_user.get_default_party_company(p_userid))
           INTO v_acc_cmp_id, v_user_plant_id  
           FROM T704_account 
           WHERE c704_account_id = v_acctid 
           AND c704_void_fl IS NULL;
         EXCEPTION
           WHEN NO_DATA_FOUND
           THEN
           v_acc_cmp_id := NULL;
           v_user_plant_id := NULL;
         END;
	-- Create a reverse entry in Order Table
	---- Added c501_customer_po_date as field for PC-3880 New field in record PO Date in GM Italy
	INSERT INTO t501_order
				(c501_order_id, c704_account_id, c703_sales_rep_id, c501_customer_po, c501_order_date, c501_total_cost
			   , c501_created_date, c501_created_by, c501_status_fl, c501_ship_cost
			   , c501_update_inv_fl   --,c707_acc_terr_rep_id
								   , c501_order_date_time, c901_order_type, c501_parent_order_id, c501_comments
			   , c506_rma_id, c1900_company_id, c5040_plant_id, c1910_division_id, c101_dealer_id, c501_surgery_date,c901_contract,c501_customer_po_date)
		SELECT v_rev_order_id, c704_account_id, c703_sales_rep_id, c501_customer_po, TRUNC (CURRENT_DATE), v_toal_cost
			 , CURRENT_DATE, p_userid, '3', v_rev_act_ship, c501_update_inv_fl
																		 --,c707_acc_terr_rep_id
			   , CURRENT_DATE, v_order_type, v_parent_order_id, p_usercomments, p_raid, nvl(c1900_company_id,v_acc_cmp_id), nvl(c5040_plant_id,v_user_plant_id), c1910_division_id,c101_dealer_id, c501_surgery_date,v_contract,c501_customer_po_date
		  FROM t501_order
		 WHERE c501_order_id = v_order_id;

		 --Prc will create item order entery from credit_str string.
		 --PMT-6740 partial issue credit 
		 gm_pkg_ar_credit_order.gm_sav_partial_credit_ord_dtls(p_raid,v_rev_order_id,p_credit_str,p_userid,v_order_id);


	-- For Discount Order Creation
	-- PMT-28394 (To avoid the DS order creation - Job will create the DS order)
	-- gm_save_dsorder (v_rev_order_id);

	--For carrying the Parent Order atributes while issuing returns credit memo for Delhi/Chennai.
 IF v_company_id = v_comp_snyc_id THEN
 
       GM_PKG_AC_INVOICE_INFO.gm_sync_ord_attribute(v_order_id,v_rev_order_id,p_userid);
							
END IF;

	
	IF v_invid IS NOT NULL
	THEN
       --
	   gm_generate_invoice (v_cust_po, v_accid, v_terms, p_creditdt, p_userid, 'CREDITORDER', 50202, 50255
						   , v_newinvid);
	   --The Below mentioned code is added for PMT-5901 .This is used to update invoice status as closed when the total amount is zer dollar.
	   gm_pkg_ac_invoice.gm_upd_invoice_status(50255,v_newinvid,p_userid);
               
    	  ---This update code is moved here from the bottom of this if condition, to resolve the Credit Returns VAT problem. 
		UPDATE t501_order
		   SET c503_invoice_id = v_newinvid
		   	 , c501_last_updated_by = p_userid
			 , c501_last_updated_date = CURRENT_DATE
		 WHERE c501_order_id = v_rev_order_id;           

	     
 --to fetch vat rate from parent order (PMT-36590-Issue Credit and Debit to use VAT Rate from Sales Order)
		gm_pkg_ac_order.gm_sav_vatrate_by_parent_order (NVL(v_order_id,v_parent_order_id),v_rev_order_id);
 --
		p_new_inv_id := v_newinvid;
 --
	END IF;
	
	gm_pkg_ac_invoice_txn.gm_sav_invoice_amount(p_new_inv_id,p_userid);
	
	--
	-- to get the order usage data flag
	 SELECT NVL (get_rule_value_by_company (v_company_id, 'ORDER_USAGE', v_company_id), 'N')
	   INTO v_order_usage_fl
	   FROM DUAL;


	IF v_order_usage_fl = 'Y' THEN
		
	--Commented for PMT-17912 - to update used lot details for returns order by gm_upd_return_order_dtls procedure
	--  gm_pkg_cs_usage_lot_number.gm_upd_return_credit_dtls (v_rev_order_id, p_raid, p_userid) ;
		
		gm_pkg_cs_usage_lot_number.gm_upd_return_order_dtls(v_order_id,v_rev_order_id,p_raid,p_userid);
	    
	    gm_pkg_set_lot_track.GM_PKG_SET_LOT_UPDATE(v_rev_order_id,p_userid,null,'50180','5','4301');
  	  --  gm_pkg_set_lot_track.GM_PKG_SET_LOT_UPDATE(v_order_id,p_userid,null,'50180','5','4301');
	END IF;

	--to save the corresponding attribute value
	gm_pkg_sav_snapshot_info.gm_sav_snapshot_info('ORDER',v_rev_order_id,'SNAPSHOT_INFO',v_company_id,'1200',p_userid);	
    --
    
    --to save Surgery Information's should carry from invoice to issue credit and debit (PMT-46595)
    gm_pkg_cs_credit_surg_info.gm_sav_surg_info(v_order_id,v_rev_order_id,p_userid);
    
END gm_save_credit_order;
/
