CREATE OR REPLACE PROCEDURE GM_UPDATE_INVOICE_MULTIPLE
(
	   p_InputStr 			IN CLOB,
	   p_PayDate			IN date,
 	   p_PayMode			IN T508_RECEIVABLES.C508_RECEIVED_MODE%TYPE,
	   p_PayDetails			IN T508_RECEIVABLES.C508_DETAILS%TYPE,
	   p_UserId				IN T503_INVOICE.C503_CREATED_BY%TYPE,
	   p_AccId				IN T503_INVOICE.C704_ACCOUNT_ID%TYPE,
	   p_PayAmt				IN T510_PAYMENT_DETAILS.C510_PAYMENT_AMT%TYPE
--		Removing the below variable
--	   p_message 			OUT  VARCHAR2
)
AS
v_strlen     NUMBER          := NVL(LENGTH(p_InputStr),0);
v_string     CLOB := p_InputStr;
v_payMode    NUMBER;-- := p_PayMode;
v_Po		 VARCHAR2(40);
v_InvId 	 VARCHAR2(40);
v_inv_amt 	 NUMBER(15,2);
v_substring  VARCHAR2(4000);
v_msg	     VARCHAR2(1000);
v_type 		NUMBER;
e_others	 EXCEPTION;
v_pay_details_id NUMBER;
v_paymentmode VARCHAR2(1000);
v_comment VARCHAR2(1000);


BEGIN

SELECT S510_PAYMENT_DETAILS.NEXTVAL INTO v_pay_details_id FROM dual;

SELECT DECODE(p_PayMode,'0',NULL,p_PayMode) INTO v_payMode FROM DUAL; 

-- Updating T510_PAYMENT_DETAILS as per CM Changes
	INSERT INTO T510_PAYMENT_DETAILS (
				 C510_PAYMENT_ID
				,C510_PAYMENT_DT
				,C510_PAYMENT_AMT
				,C901_PAYMENT_MODE
				,C510_PAYMENT_DETAILS
				,C510_CREATED_DATE
				,C510_CREATED_BY)
	VALUES (
				 v_pay_details_id
				 ,p_PayDate
				 ,p_PayAmt
				 ,v_payMode
				 ,p_PayDetails
				 ,CURRENT_DATE
				 ,p_UserId);
			


	IF v_strlen > 0
    THEN
   		WHILE INSTR(v_string,'|') <> 0 LOOP
    		  v_substring := SUBSTR(v_string,1,INSTR(v_string,'|')-1);
    		  v_string    := SUBSTR(v_string,INSTR(v_string,'|')+1);
			  v_InvId	  := NULL;
			  v_Po		  := NULL;
			  v_inv_amt	  := NULL;		
			  v_type 	  := NULL;
			  v_InvId	  := SUBSTR(v_substring,1,INSTR(v_substring,'^')-1);
			  v_substring := SUBSTR(v_substring,INSTR(v_substring,'^')+1);			  
			  v_Po		  := SUBSTR(v_substring,1,INSTR(v_substring,'^')-1);
			  v_substring := SUBSTR(v_substring,INSTR(v_substring,'^')+1);
			  
			  v_paymentmode	  := SUBSTR(v_substring,1,INSTR(v_substring,'^')-1);
			  v_substring := SUBSTR(v_substring,INSTR(v_substring,'^')+1);
			  
			  v_comment	  := SUBSTR(v_substring,1,INSTR(v_substring,'^')-1);
			  v_substring := SUBSTR(v_substring,INSTR(v_substring,'^')+1);
			  
			  v_inv_amt	  := SUBSTR(v_substring,1,INSTR(v_substring,'^')-1);
			  v_substring := SUBSTR(v_substring,INSTR(v_substring,'^')+1);	  			  
			  v_type		  :=  TO_NUMBER(v_substring);
			  
			  -- Checking if the Payment Type (v_type) is that of a discount (2701)
			  -- If it is, then update the Payment mode as 2701

			  v_payMode := v_paymentmode; 
			  GM_UPDATE_INVOICE(v_InvId,p_PayDate,v_inv_amt,v_payMode,NVL(p_PayDetails,v_comment),p_UserId,p_AccId,v_Po,v_type,v_pay_details_id);
		END LOOP;
	-- ELSE

	END IF;
/*	    v_msg := 'p_str is empty';
		RAISE e_others;
	END IF;
	v_msg := 'Success';
	COMMIT WORK;
    p_message  := v_msg;
EXCEPTION
	  WHEN e_others THEN
	  ROLLBACK WORK;
	  p_message  := SQLERRM;
	  WHEN OTHERS THEN
	  ROLLBACK WORK;
	  p_message  := SQLERRM; */
END GM_UPDATE_INVOICE_MULTIPLE;
/

