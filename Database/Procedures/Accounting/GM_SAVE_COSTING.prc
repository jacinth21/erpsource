CREATE OR REPLACE PROCEDURE GM_SAVE_COSTING
(	 p_partnum 		IN	t820_costing.c205_part_number_id%TYPE
	,p_costing_type	IN	t820_costing.c901_costing_type%TYPE
	,p_qty			IN	t820_costing.c820_qty_received%TYPE
	,p_price		IN	t820_costing.c820_purchase_amt%TYPE
	,p_updatedby	IN	t820_costing.c820_created_by%TYPE
	,p_company_id IN t820_costing.c1900_company_id%TYPE
	,p_plant_id IN t5040_plant_master.c5040_plant_id%TYPE
	,p_owner_company IN t820_costing.c1900_owner_company_id%TYPE
    ,p_company_cost  IN t810_posting_txn.c810_cr_amt%TYPE
	,p_costing_id  OUT	t820_costing.c820_costing_id%TYPE
	
)
AS
/***************************************************************************************
 * Description           :This procedure is used to save purchase and price information 
 *						  in costing table 
 * 						  1. Will post the information based on the type 
 *						  2. if the active and the last inserted for has the same 
 *						  	 value then will update the value, else will create new value
 ****************************************************************************************/
--
	v_division_id T1910_DIVISION.C1910_DIVISION_ID%TYPE;
	v_division_nm T1910_DIVISION.C1910_DIVISION_NAME%TYPE;
--
BEGIN
--
	      

	BEGIN
	--
		-- If record already found then for the part and same QTY then update the 
		-- record with new value 
		SELECT 	c820_costing_id
		INTO	p_costing_id 
		FROM	t820_costing
		WHERE 	c820_costing_id IN 
					( SELECT	MAX(c820_costing_id) 
					  FROM 		t820_costing
					  WHERE		c205_part_number_id	= p_partnum
					  AND		c901_costing_type	= p_costing_type		
					  AND		c901_status			<> 4802
					  AND      c1900_company_id = p_company_id
					  AND DECODE(p_plant_id, NULL, -999, c5040_plant_id) = NVL(p_plant_id, - 999))  	
		AND		c820_purchase_amt = p_price
		AND    c1900_company_id = p_company_id
		AND DECODE(p_company_cost, NULL, -999, c820_local_company_cost) = NVL(p_company_cost, -999)
		AND DECODE(p_plant_id, NULL, -999, c5040_plant_id) = NVL(p_plant_id, - 999)
		AND DECODE(p_owner_company, NULL, -999, c1900_owner_company_id) = NVL(p_owner_company, - 999);
		--
		UPDATE	 t820_costing
		SET		 c820_qty_received 		= c820_qty_received + p_qty
				,c820_qty_on_hand  		= c820_qty_on_hand + p_qty
				,c820_last_updated_by 	= p_updatedby
				,c820_last_updated_date = SYSDATE		
		WHERE	 c820_costing_id	  	= p_costing_id;
	--	
	EXCEPTION 				  
	WHEN NO_DATA_FOUND 
	THEN 
	
	--Get the Division info for the Part Number;
	BEGIN
		  SELECT t1910.C1910_DIVISION_ID, t1910.C1910_DIVISION_NAME  
		  INTO v_division_id , v_division_nm
		  FROM T205_PART_NUMBER t205,T202_PROJECT t202,T1910_DIVISION t1910
		  WHERE t205.c202_project_id = t202.c202_project_id (+)
		  AND t202.C1910_DIVISION_ID = t1910.C1910_DIVISION_ID (+)
		  AND t202.C202_VOID_FL(+) is null
		  AND t1910.C1910_VOID_FL(+) is null
		  AND t205.C205_PART_NUMBER_ID=p_partnum;
	EXCEPTION WHEN OTHERS
	THEN
		v_division_id :='';
		v_division_nm :='';
	END;
	
	--
		-- If not found then create the t820_costing 
		SELECT	s820_costing.NEXTVAL
		INTO	p_costing_id 
		FROM	DUAL;
		--
		INSERT INTO t820_costing (
			 c820_costing_id
			,c205_part_number_id
			,c901_costing_type
			,c820_part_received_dt
			,c820_qty_received
			,c820_purchase_amt
			,c820_qty_on_hand       
			,c901_status
			,c820_delete_fl
			,c820_created_by
			,c820_created_date
			,c1900_company_id
			,c5040_plant_id
			,c1900_owner_company_id
			, c901_owner_currency
			,c820_local_company_cost
			,c901_local_company_currency
			,C1910_DIVISION_ID
			,C1910_DIVISION_NAME
		) 
		VALUES 
		(	 p_costing_id
			,p_partnum
			,p_costing_type
			,TRUNC(CURRENT_DATE)
			,p_qty
			,p_price
			,p_qty
			,4800
			,'N'
			,p_updatedby
			,CURRENT_DATE
			,p_company_id
			,p_plant_id
			,p_owner_company
			,get_comp_curr (p_owner_company)
			,p_company_cost
			,get_comp_curr (p_company_id)
			,v_division_id
			,v_division_nm
		);
	--	
	END ;	
--
END GM_SAVE_COSTING;
/

