/* Formatted on 2010/06/17 11:49 (Formatter Plus v4.8.0) */
-- @"c:\database\procedures\accounting\gm_report_pedning_payment.prc"

CREATE OR REPLACE PROCEDURE gm_jp_report_pedning_payment (
   p_dealer_id    IN       t501_order.C101_DEALER_ID%TYPE,
   p_report_type   IN       VARCHAR2,
   p_inv_source    IN       NUMBER,
   p_from_day	   IN       NUMBER,
   p_to_day		   IN		NUMBER,
   p_txntp         IN       VARCHAR2,
   p_recordset     OUT      TYPES.cursor_type
)
AS
/****************************************************************************************
 * Description        :This procedure is called to get ACCOUNT RECEIVABLE INFROMATION
 *                  for unpaid bill
 *
 * Parameters         : p_account_id   IN Passes the Account Name
 *                   p_report_tye   IN Type of report
 *                      129  Means  1 to 29 days  3160 Means 31 to 60 Days
 *                      6190 Means 61 to 90 days 91120 Means 91 to 120 Days
 *                      121  Means Greater than 120 Days
 *
 *                   p_message      OUT Error Message IF Any
 *                   p_recordset OUT Values with AR Information
 *
 * Revision History
 * ================
 * 04/26/06    Richard  modified to fecth the value from the Invoice table
 *****************************************************************************************/
--
v_company_id        t1900_company.c1900_company_id%TYPE;
BEGIN
	 SELECT get_compid_frm_cntx() 
      INTO v_company_id  
      FROM dual;
	
   -- code is removed because p_from_day and p_to_day is directly coming from the screen.
   OPEN p_recordset
    FOR
--
       SELECT v503.v503_invoice_id inv_id,
              v503.v503_invoice_date inv_date,
              v503.v503_customer_po customer_po,
              NVL (v503.v503_dealer_id, v503.v503_distributor_id) acct_id,
              NVL (v503.v503_dealer_nm, v503.v503_distributor_name) NAME,
              NVL (v503.v503_inv_amt, 0) inv_amt,
              NVL (v503.v503_inv_paid,0) payment_amt,
              get_log_flag (v503.v503_invoice_id, 1201) call_flag,
              v503_acc_currency_nm currency,
              t101p.C101_EMAIL_FL EMAILREQ  , t101p.C901_EMAIL_FILE_FMT EVERSION,
			  v503.v503_pdf_id PDFFILEID,  v503.v503_csv_id CSVFILEID,
			  v503.v503_invoice_date INVYEAR
         FROM v503_invoice_reports v503,t101_party_invoice_details t101p
        WHERE v503.v503_dealer_id = p_dealer_id
          AND t101p.C101_party_id = v503.v503_dealer_id
          AND v503.v503_status_fl < 2
          AND V503_COMPANY_ID = v_company_id
          AND ( TRUNC (CURRENT_DATE) - trunc(v503.v503_invoice_date +DECODE(p_report_type,'ARByDueDt',DECODE(p_from_day,'0',0,V503_TERMS_DAYS),0))) >= p_from_day  -- for Duedate report curr column no need to add terms. so, that used decode here.
          AND (TRUNC (CURRENT_DATE) - trunc(v503.v503_invoice_date +DECODE(p_report_type,'ARByDueDt',V503_TERMS_DAYS,0))) <= p_to_day; -- condition changed to get data based on due date also
END gm_jp_report_pedning_payment;
/
