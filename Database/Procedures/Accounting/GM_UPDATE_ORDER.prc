/* Formatted on 2012/05/03 18:18 (Formatter Plus v4.8.0) */
--@"C:\DATABASE\PROCEDURES\ACCOUNTING\gm_update_order.prc";

CREATE OR REPLACE PROCEDURE gm_update_order (
	p_ordid 		  IN	   t501_order.c501_order_id%TYPE
  , p_accid 		  IN	   t501_order.c704_account_id%TYPE
  --, p_shiptoid			IN	 t501_order.c501_ship_to_id%TYPE
  --, p_shipto		  IN   t501_order.c501_ship_to%TYPE
,	p_po			  IN	   t501_order.c501_customer_po%TYPE
  , p_userid		  IN	   t501_order.c501_created_by%TYPE
  , p_omode 		  IN	   t501_order.c501_receive_mode%TYPE
  , p_pname 		  IN	   t501_order.c501_received_from%TYPE
  --, p_shipdt		  IN   VARCHAR2
  --, p_shipcar 	  IN	t501_order.c501_delivery_carrier%TYPE
  --, p_shipmode			IN	 t501_order.c501_delivery_mode%TYPE
  --, p_tracknum			IN	 t501_order.c501_tracking_number%TYPE
  --, p_shipcost			IN	 t501_order.c501_ship_cost%TYPE
,	p_comments		  IN	   t501_order.c501_comments%TYPE
  , p_parentorderid   IN	   t501_order.c501_parent_order_id%TYPE
  , p_invoiceid 	  IN	   t501_order.c503_invoice_id%TYPE
  , p_salesrep		  IN	   t501_order.c703_sales_rep_id%TYPE
  , p_rmqtystr		  IN	   CLOB
  , p_boqtystr		  IN	   CLOB
  , p_hard_po	  	  IN	   t501_order.c501_hard_po_fl%TYPE
  , p_quotestr		  IN  VARCHAR2
  , p_usedlotstr      IN  t501_order.c501_comments%TYPE
  , p_surgery_date    IN  t501_order.c501_surgery_date%TYPE
  , p_order_date      IN  t501_order.c501_order_date%TYPE
  , p_rmqtymsgstr	  OUT	   VARCHAR2
  , p_boqtymsgstr	  OUT	   VARCHAR2
)
AS
--
	v_parent_cnt   NUMBER;
	v_inv_cnt	   NUMBER;
	v_ship_cnt	   NUMBER;
	v_order_type   t501_order.c901_order_type%TYPE;
	v_current_order_status t501_order.c501_status_fl%TYPE;
	v_total 	   NUMBER (15, 2);
	v_ds_orderid   t501_order.c501_order_id%TYPE;
	v_gpoid 	   VARCHAR2 (20);
	v_old_acctid   t501_order.c704_account_id%TYPE;
	v_comments	   VARCHAR2 (200);
	v_parentorderid t501_order.c501_parent_order_id%TYPE;
	v_rmqtystr		CLOB;
	v_boqtystr		CLOB;
	v_rmqtymsgstr	VARCHAR2(4000);
	v_boqtymsgstr	VARCHAR2(4000);
	v_strlen 		NUMBER 	:= NVL (LENGTH (p_quotestr), 0);
 	v_string 		VARCHAR2 (30000) := p_quotestr;
 	v_substring 	VARCHAR2 (1000);
 	v_attrtype 		t501a_order_attribute.c901_attribute_type%TYPE;
 	v_attrval 		t501a_order_attribute.c501a_attribute_value%TYPE;
 	v_ordatbid 		t501a_order_attribute.c501a_order_attribute_id%TYPE;
 	v_trans_company_id VARCHAR2(20);
    v_trans_plant_id VARCHAR2(20);
    v_company_id    t1900_company.c1900_company_id%TYPE;
    v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
    v_order_date    T501_ORDER.C501_ORDER_DATE%TYPE;
    v_ship_date		T501_ORDER.C501_SHIPPING_DATE%TYPE;
    v_order_dt_time	T501_ORDER.C501_ORDER_DATE_TIME%TYPE;
    v_contract T704d_Account_Affln.c901_contract%TYPE;
    v_dealer_id     t501_order.c101_dealer_id%TYPE;
    v_division_id   t501_order.c1910_division_id%TYPE;
    v_sales_rep		t501_order.c703_sales_rep_id%TYPE;
	v_surgry_date  t501_order.c501_surgery_date%TYPE;
--
BEGIN
	
	-- When enter the surgery details in edit order screen ,will call the save order attribute for the UPDATE/SAVE.
	WHILE INSTR (v_string, '|') <> 0
 	LOOP
 		v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
 		v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
 		--
 		v_ordatbid := NULL;
 		v_attrtype := NULL;
 		v_attrval := NULL;
 		--
		v_ordatbid := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
		v_attrtype := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
		v_attrval := v_substring;
		gm_pkg_cm_order_attribute.gm_sav_order_attribute (p_ordid,v_attrtype,v_attrval,v_ordatbid,p_userid);
		--IF (v_attrtype = 400152) THEN
			-- When enter exchange rate in surgery section from edit order screen, we need to also capture the exchange rate for order.
		--	GM_PKG_AC_ORDER.gm_sav_ext_cost(p_ordid);
		--END IF;
 	END LOOP;
 	
	-- Fetch current order status
	SELECT	   t501.c501_status_fl, NVL (t501.c901_order_type, '-999999'), t501.c704_account_id, t501.c1900_company_id, C501_ORDER_DATE
			  , C501_SHIPPING_DATE, C501_ORDER_DATE_TIME, C501_SURGERY_DATE
		  INTO v_current_order_status, v_order_type, v_old_acctid, v_trans_company_id, v_order_date,
		  	   v_ship_date, v_order_dt_time, v_surgry_date
		  FROM t501_order t501
		 WHERE t501.c501_order_id = p_ordid
	FOR UPDATE;

    ---Check if surgery date was updated --   
    IF (v_surgry_date is null and p_surgery_date is not null) or (v_surgry_date is not Null AND p_surgery_date is null) 
	    OR (v_surgry_date is not null and p_surgery_date is not null and v_surgry_date != p_surgery_date)
	THEN  
		v_attrtype := 106600;   --- Surgery date edited
	    v_attrval := 'Y';
		v_ordatbid := NULL;
	   gm_pkg_cm_order_attribute.gm_sav_order_attribute (p_ordid,v_attrtype,v_attrval,v_ordatbid,p_userid);
    END IF;
		
	SELECT  get_compid_frm_cntx(),get_plantid_frm_cntx()
			  INTO  v_company_id,v_plant_id
			  FROM DUAL;
	
	
	-- setting transaction company id into context incase of mismatch  
	  IF (v_trans_company_id IS NOT NULL) AND (v_company_id != v_trans_company_id) THEN
         gm_pkg_cor_client_context.gm_sav_client_context(v_trans_company_id,v_plant_id);
      END IF;
	--
	IF p_parentorderid IS NOT NULL
	THEN
		SELECT COUNT (*)
		  INTO v_parent_cnt
		  FROM t501_order
		 WHERE c501_order_id = p_parentorderid
		   AND c501_delete_fl IS NULL
		   AND c501_void_fl IS NULL
		   AND c704_account_id = p_accid;

		IF v_parent_cnt = 0
		THEN
			raise_application_error (-20094, '');
                --validation added here, to make sure if order's parent order exist, need get it as new order's parent order
		ELSE
			SELECT NVL (c501_parent_order_id, c501_order_id) orderid
			  INTO v_parentorderid
			  FROM t501_order
			 WHERE c501_order_id = p_parentorderid
			   AND c501_delete_fl IS NULL
			   AND c501_void_fl IS NULL
			   AND c704_account_id = p_accid;
		END IF;
	END IF;

	IF p_invoiceid IS NOT NULL
	THEN
		SELECT COUNT (*)
		  INTO v_inv_cnt
		  FROM t503_invoice
		 WHERE c503_invoice_id = p_invoiceid
		   AND c503_delete_fl IS NULL
		   AND c503_void_fl IS NULL
		   AND c704_account_id = p_accid;

		IF v_inv_cnt = 0
		THEN
			raise_application_error (-20095, '');
		END IF;
	END IF;

	--
	--getting contract value from account
	  SELECT GET_ACCT_CONTRACT(p_accid) 
	  INTO v_contract 
	  FROM DUAL; 
	      
    --getting dealer id for the account
    BEGIN
	  SELECT C101_DEALER_ID 
	  INTO v_dealer_id 
	  FROM T704_ACCOUNT 
	  WHERE C704_ACCOUNT_ID=p_accid AND C704_VOID_FL IS NULL;
	  EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_dealer_id := NULL;
          
      END;
      
--if account name changed for the order or the sales rep is selected as 'Choose one', then getting the sales rep from the account,
--else selected sales rep will update.
	IF p_accid <> v_old_acctid OR p_salesrep = '0'
	THEN
   BEGIN
	  SELECT  C703_SALES_REP_ID
	  INTO  v_sales_rep
	  FROM T704_ACCOUNT 
	  WHERE C704_ACCOUNT_ID=p_accid AND C704_VOID_FL IS NULL;
	  EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
           
            v_sales_rep := NULL;
      END;
      ELSE    
      v_sales_rep := p_salesrep;
	 END IF;	  
	  
	--getting division id for the sales rep
	BEGIN
	SELECT C1910_DIVISION_ID 
	  INTO v_division_id
	  FROM T703_SALES_REP 
	  WHERE C703_SALES_REP_ID=v_sales_rep 
	  AND C703_VOID_FL IS NULL;
	EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_division_id := NULL;
      END;  

	  

	UPDATE t501_order
	   SET c704_account_id = p_accid
		 , c703_sales_rep_id = v_sales_rep
		 -- , c501_ship_to = p_shipto
		 -- , c501_ship_to_id = p_shiptoid
	,	   c501_customer_po = p_po
		 , c501_receive_mode = p_omode
		 , c501_received_from = p_pname
		 -- , c501_shipping_date = DECODE (c501_shipping_date, NULL, NULL, TO_DATE (p_shipdt, 'MM/DD/YYYY'))
		 -- , c501_delivery_carrier = p_shipcar
		 -- , c501_delivery_mode = p_shipmode
		 -- , c501_tracking_number = p_tracknum
	,	   c501_last_updated_by = p_userid
		 , c501_last_updated_date = SYSDATE
		 -- , c501_ship_cost = p_shipcost
	,	   c501_comments = p_comments
		 , c501_parent_order_id = v_parentorderid
		 , c503_invoice_id = p_invoiceid
		 , c501_hard_po_fl = p_hard_po
		 , c501_surgery_date = p_surgery_date
		 , c901_contract = v_contract
		 , c101_dealer_id = v_dealer_id
		 , c1910_division_id = v_division_id
	 WHERE c501_order_id = p_ordid;
	-- PC-3825 A/R PO - Remapped account orders display in Dashboard	 
	UPDATE t5001_order_po_dtls
	SET
	    c704_account_id = p_accid
	WHERE
	    c501_order_id = p_ordid;
 -- Order date is modifed due to reclassification.
	  IF p_order_date <> v_order_date
	 THEN
	   UPDATE t501_order
	   SET 
	 	C501_ORDER_DATE_TIME = p_order_date
	 	, c501_order_date = p_order_date
	 	-- Shipping date to be modified only if the order date time, ship date time are same.
	 ,  C501_SHIPPING_DATE = DECODE( TRUNC(v_ship_date), TRUNC(v_order_dt_time),p_order_date,C501_SHIPPING_DATE  )
	 ,  c501_last_updated_by = p_userid
	 ,  c501_last_updated_date = CURRENT_DATE
	 WHERE 
	 	c501_order_id = p_ordid
	 	AND c501_void_fl IS NULL;
	 	
	 END IF;
--Case 1: For Order transferred from HPG to HPG account.The update of account ID below will take care.
--Case 2: For order transferred from Non-HPG to HPG account .The proc gm_save_dsorder will create a new DS order.
--Case 3: For order transferred from HPG to Non-HPG account.
--Case 4: For order transferred from Non-HPG to Non-HPG account.
--
-- For remove qty/BO qty logic
	IF p_rmqtystr IS NOT NULL OR p_boqtystr IS NOT NULL
	THEN
		--
		SELECT COUNT (*)
		  INTO v_ship_cnt
		  FROM t907_shipping_info
		 WHERE c907_ref_id = p_ordid AND c907_void_fl IS NULL AND c907_status_fl = '40';

		-- if order is not shipped then only call the below code. if it is shipped dont do anything.
		IF v_ship_cnt = 0
		THEN	
		--remove qty has loaner parts or bo qty has loner parts
			IF v_current_order_status <= 1 OR (v_current_order_status > 1 AND (INSTR(p_rmqtystr,'50301') <>0 OR INSTR(p_boqtystr,'50301') <> 0))
			THEN
					 IF	v_current_order_status > 1 THEN	
					 	v_rmqtystr := get_cntrl_str_from_type(p_rmqtystr,'50301'); --getting loaner parts from remove input string
					 	v_boqtystr := get_cntrl_str_from_type(p_boqtystr,'50301');	--getting loaner parts from back order input string			 	
					 ELSE
					   v_rmqtystr := p_rmqtystr;
					   v_boqtystr := p_boqtystr;					   
					 END IF;		
			
						IF v_rmqtystr IS NOT NULL OR v_boqtystr IS NOT NULL THEN
							gm_save_notcontrolled_ord (p_ordid, p_userid, v_rmqtystr, v_boqtystr, v_rmqtymsgstr, v_boqtymsgstr);
			
							SELECT SUM (NVL (TO_NUMBER (c502_item_qty) * TO_NUMBER (c502_item_price), 0))
							  INTO v_total
							  FROM t502_item_order
							 WHERE c501_order_id = p_ordid AND c502_VOID_FL IS NULL;
			
							UPDATE t501_order
							   SET c501_total_cost = NVL (v_total, 0)
								 , c501_last_updated_by = p_userid
								 , c501_last_updated_date = SYSDATE
							 WHERE c501_order_id = p_ordid;
					     END IF;
			END IF;
			IF v_current_order_status > 1
			THEN
				gm_save_controlled_ord (p_ordid, p_userid, p_rmqtystr, p_boqtystr, p_usedlotstr, p_rmqtymsgstr, p_boqtymsgstr);
				--
			ELSE
				gm_pkg_cs_usage_lot_number.gm_upd_remove_qty (p_ordid, p_usedlotstr, p_userid);
			END IF;
		END IF;
	--
	ELSE
--For Case 2:

		-- PMT-28394 (To avoid the DS order creation - Job will create the DS order)
		
		-- update the rebate process flag - So, that job will pick the order and calculate discount
		
		gm_pkg_ac_order_rebate_txn.gm_update_rebate_fl (p_ordid, NULL, NULL);
		gm_pkg_ac_order_rebate_txn.gm_void_discount_order (p_ordid, p_userid);
		
	END IF;

--For Case 3 will void the order and Case 4 will result into exception
	v_gpoid 	:= get_account_gpo_id (p_accid);

	IF v_gpoid = 0
	THEN
		BEGIN
			SELECT	 t501.c501_order_id
				INTO v_ds_orderid
				FROM t501_order t501
			   WHERE t501.c501_parent_order_id = p_ordid AND t501.c901_order_type = 2535 AND t501.c501_void_fl IS NULL
			GROUP BY t501.c501_order_id;

			v_comments	:= 'Order transferred from HPG account id' || v_old_acctid || 'to Non-HPG account' || p_accid;

			UPDATE t501_order
			   SET c501_void_fl = 'Y'
				 , c501_last_updated_by = p_userid
				 , c501_last_updated_date = SYSDATE
				 , c501_comments = v_comments
			 WHERE c501_order_id = v_ds_orderid;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				NULL;
		END;
	END IF;

	--For Case 1: and all order where c501_parent_order_id = p_ordid (e.g Price Adjustment),may update multiple rows.
	UPDATE t501_order
	   SET c704_account_id = p_accid
		 , c703_sales_rep_id = v_sales_rep
		 , c501_receive_mode = p_omode
		 , c501_received_from = p_pname
		 , c501_last_updated_by = p_userid
		 , c501_last_updated_date = SYSDATE
		 , c501_comments = p_comments
		 , c501_surgery_date = p_surgery_date
		 , c101_dealer_id = v_dealer_id
		 , c1910_division_id = v_division_id
	 WHERE c501_parent_order_id = p_ordid AND c501_void_fl IS NULL;
	 
	 IF v_current_order_status <= 1 AND v_order_type = '-999999'
	 THEN
		 -- while editing the order[Bill & SHip], need to check whether all the parts in the order is loaner or not
		 -- If all are loaner parts, then convert it into Bill Only Loaner and release it from shipping
		 gm_pkg_op_order_txn.gm_sav_release_lnorder(p_ordid,v_order_type,p_userid);
	 END IF;
	 
	 -- To validate for the attribute qty and item qty.
   --  gm_pkg_op_do_process_trans.gm_validate_do_attribute_qty(p_ordid,101723);
   IF v_rmqtymsgstr IS NOT NULL THEN
	 p_rmqtymsgstr := p_rmqtymsgstr || '<br>' ||v_rmqtymsgstr;
   END IF;
   
   IF v_boqtymsgstr IS NOT NULL THEN
	 p_boqtymsgstr := p_boqtymsgstr || '<br>' ||v_boqtymsgstr;
   END IF;
     
END gm_update_order;
/
