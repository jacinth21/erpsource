/* Formatted on 2011/07/26 15:54 (Formatter Plus v4.8.0) */
--@"C:\Database\Procedures\Accounting\GM_SAVE_CREDIT_ORDER_SWAP_TYPE.prc";

CREATE OR REPLACE PROCEDURE gm_save_credit_order_swap_type (
	p_raid			 IN   t506_returns.c506_rma_id%TYPE
  , p_reason		 IN   t506_returns.c506_reason%TYPE
  , p_id			 IN   t506_returns.c501_order_id%TYPE
  , p_creditdt		 IN   VARCHAR2
  , p_usercomments	 IN   t501_order.c501_comments%TYPE
  , p_userid		 IN   t501_order.c501_created_by%TYPE
)
/****************************************************************************
 * Description : This procedure is called to update the order return process
 *		 for parts swap between consignments and loaners and vice versa
 *
 * Parameters	: p_raid Order return id
 *****************************************************************************/
AS
--
	v_order_id	   t506_returns.c501_order_id%TYPE := p_id;
	v_rev_order_id t506_returns.c501_order_id%TYPE;
	v_invid 	   t503_invoice.c503_invoice_id%TYPE;
	v_newinvid	   t503_invoice.c503_invoice_id%TYPE;
	v_accid 	   t503_invoice.c704_account_id%TYPE;
	v_terms 	   t503_invoice.c503_terms%TYPE;
	v_cust_po	   t503_invoice.c503_customer_po%TYPE;
	v_toal_cost    t501_order.c501_total_cost%TYPE;
	v_order_type   t501_order.c901_order_type%TYPE;
	v_act_ship	   t501_order.c501_ship_cost%TYPE;	 -- Actual shipping cost
	v_rev_act_ship t501_order.c501_ship_cost%TYPE := NULL;	 -- Reversed Shipping cost
	v_status_fl    t501_order.c501_status_fl%TYPE;
	v_qty_dr_type  NUMBER;
	v_qty_cr_type  NUMBER;
	v_control_dr_num t502_item_order.c502_control_number%TYPE;
	v_control_cr_num t502_item_order.c502_control_number%TYPE;
	v_item_dr_type NUMBER;
	v_item_cr_type NUMBER;
	v_ship_to	   t907_shipping_info.c901_ship_to%TYPE;
	v_ship_to_id   t907_shipping_info.c907_ship_to_id%TYPE;
	v_del_mode	   t907_shipping_info.c901_delivery_mode%TYPE;
	v_del_carr	   t907_shipping_info.c901_delivery_carrier%TYPE;
	v_addid 	   t907_shipping_info.c106_address_id%TYPE;
	v_ship_id	   VARCHAR2 (20);
	v_rep_id	   t501_order.c703_sales_rep_id%TYPE;
	v_temp		   VARCHAR2 (20);
	v_shipid	   VARCHAR2 (20);
	v_stock 	   NUMBER := 0;
	v_qty		   NUMBER := 0;
	v_price 	   NUMBER := 0;
	v_bo_cnt	   NUMBER := 0;
	v_pos_qty	   NUMBER := 0;    
	v_bo_qty	   NUMBER := 0;
	v_pnum		   t205_part_number.c205_part_number_id%TYPE;
	v_bo_string    VARCHAR2 (1000);
	v_part_cnt	   NUMBER := 0;
	v_close_order  VARCHAR2 (10);
	v_strlen	   NUMBER := 0;
	v_total 	   NUMBER := 0;
	v_list_price		t502_item_order.c502_list_price%TYPE;
    v_unit_price 		t502_item_order.c502_unit_price%TYPE;
    v_unit_prc_adj_code t502_item_order.c901_unit_price_adj_code%TYPE;
    v_unit_price_adj_val t502_item_order.c502_unit_price_adj_value%TYPE;
    v_disc_offered 		t502_item_order.c502_discount_offered%TYPE;
    v_disc_type			t502_item_order.c901_discount_type%TYPE;
    v_acc_price 		t502_item_order.C502_DO_UNIT_PRICE%TYPE;
    v_adj_code_val		t502_item_order.C502_ADJ_CODE_VALUE%TYPE;
    v_contract          T704d_Account_Affln.c901_contract%TYPE;
    v_acctid            t501_order.c704_account_id%TYPE;
    v_company_id    t501_order.c1900_company_id%TYPE;
	v_parent_order_id t501_order.c501_order_id%TYPE;
	v_acc_cmp_id    t704_account.c704_account_id%TYPE;
    v_user_plant_id   t501_order.c5040_plant_id%TYPE;
--
	CURSOR cur_sales
	IS
		SELECT	 c205_part_number_id pnum, SUM (c507_item_qty) qty, c507_item_price price
			FROM t507_returns_item
		   WHERE c506_rma_id = p_raid AND c507_status_fl = 'C'
		GROUP BY c205_part_number_id, c507_item_price;
--
BEGIN
	--
	SELECT 'GM' || s501_order.NEXTVAL || 'S'
	  INTO v_rev_order_id
	  FROM DUAL;
	  
	  	--Getting company id from context.   
    SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM dual;

	--
	v_toal_cost := 0;
	v_status_fl := 1;	-- Status as 1 so it shows on CS dashboard for Shipping or Verification
	--
	v_order_type := 2531;	-- Marking it as 'Loan-Consign Swap' order type

	--
	IF p_reason = 3316	 -- Consign To Loaner
	THEN
		v_qty_dr_type := -1;
		v_qty_cr_type := 1;
		v_control_dr_num := 'NOC#';
		v_control_cr_num := 'NOC#';
		v_item_dr_type := 50300;
		v_item_cr_type := 50301;
	--
	ELSIF p_reason = 3317	-- Loaner to Consign
	THEN
		v_qty_dr_type := 1;
		v_qty_cr_type := -1;
		v_control_dr_num := '';
		v_control_cr_num := 'NOC#';
		v_item_dr_type := 50300;
		v_item_cr_type := 50301;
	--
	END IF;
 --getting Account id from order table
    BEGIN		
		SELECT c704_account_id, get_parent_order_id(c501_order_id)
		  INTO v_acctid, v_parent_order_id
		  FROM t501_order
		 WHERE c501_order_id = v_order_id
		  AND c501_void_fl   IS NULL
          AND c501_delete_fl IS NULL;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_acctid 	:= NULL;
	END;
 --getting contract value from account
	  SELECT GET_ACCT_CONTRACT(v_acctid) 
	    INTO v_contract 
	    FROM DUAL;
	   BEGIN
	 --getting company id and plant id mapped from account
           SELECT c1900_company_id,gm_pkg_op_plant_rpt.get_default_plant_for_comp(gm_pkg_it_user.get_default_party_company(p_userid))
           INTO v_acc_cmp_id, v_user_plant_id  
           FROM T704_account 
           WHERE c704_account_id = v_acctid 
           AND c704_void_fl IS NULL; 
       EXCEPTION
           WHEN NO_DATA_FOUND
           THEN
           v_acc_cmp_id := NULL;
           v_user_plant_id := NULL;
       END;
	-- Create a reverse entry in Order Table
	---- Added c501_customer_po_date as field for PC-3880 New field in record PO Date in GM Italy
	INSERT INTO t501_order
				(c501_order_id, c704_account_id, c703_sales_rep_id, c501_customer_po, c501_order_date, c501_total_cost
			   , c501_created_date, c501_created_by, c501_status_fl, c501_order_date_time, c901_order_type
			   , c501_parent_order_id, c501_comments, c506_rma_id, c503_invoice_id, c1900_company_id, c5040_plant_id, c1910_division_id
			   , c101_dealer_id, c501_surgery_date,c901_contract,c501_customer_po_date)
		SELECT v_rev_order_id, c704_account_id, c703_sales_rep_id, c501_customer_po, TRUNC (CURRENT_DATE), v_toal_cost
			 , CURRENT_DATE, p_userid, v_status_fl, CURRENT_DATE, v_order_type, v_parent_order_id, p_usercomments, p_raid
			 , c503_invoice_id, nvl(c1900_company_id,v_acc_cmp_id), nvl(c5040_plant_id,v_user_plant_id), c1910_division_id
			 , c101_dealer_id, c501_surgery_date,v_contract,c501_customer_po_date
		  FROM t501_order
		 WHERE c501_order_id = v_order_id;
			-- p_reason have a swap type (Parts Swap (Consign To Loaner)- 3316 or Parts Swap (Loaner to Consign)-3317)
		 gm_pkg_cm_status_log.gm_sav_status_details (v_order_id, '', p_userid, NULL, CURRENT_DATE, p_reason, 91102);
	--
	BEGIN
		SELECT c901_ship_to, c907_ship_to_id, c901_delivery_mode, c901_delivery_carrier, c106_address_id
		  INTO v_ship_to, v_ship_to_id, v_del_mode, v_del_carr, v_addid
		  FROM t907_shipping_info
		 WHERE c907_ref_id = v_order_id AND c901_source = 50180 AND c907_void_fl IS NULL;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			BEGIN
				SELECT t501.c703_sales_rep_id, t106.c106_address_id
				  INTO v_rep_id, v_addid
				  FROM t501_order t501, t703_sales_rep t703, t106_address t106
				 WHERE c501_order_id = v_order_id
				   AND t501.c703_sales_rep_id = t703.c703_sales_rep_id
				   AND t703.c101_party_id = t106.c101_party_id
				   AND t106.c106_primary_fl = 'Y'
				   AND t106.c106_void_fl IS NULL
				   AND (t501.c901_ext_country_id IS NULL
				   OR t501.c901_ext_country_id  in (select country_id from v901_country_codes))
				   AND (t703.c901_ext_country_id IS NULL
				   OR t703.c901_ext_country_id  in (select country_id from v901_country_codes));
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					raise_application_error (-20205, '');
			END;

			v_ship_to	:= 4121;
			v_ship_to_id := v_rep_id;
			v_del_mode	:= 5004;
			v_del_carr	:= 5001;
	END;

	gm_pkg_cm_shipping_trans.gm_sav_shipping (v_rev_order_id
											, 50180   --orders
											, v_ship_to
											, v_ship_to_id
											, v_del_carr
											, v_del_mode
											, NULL
											, v_addid
											, 0
											, p_userid
											, v_ship_id
											 );

	--
	FOR rad_val IN cur_sales
	LOOP
		IF p_reason = 3317
		THEN
			SELECT	   get_qty_in_stock (c205_part_number_id)
				  INTO v_stock
				  FROM t205_part_number
				 WHERE c205_part_number_id = rad_val.pnum
			FOR UPDATE;

			v_qty		:= rad_val.qty;
			v_price 	:= rad_val.price;
			v_pos_qty	:= v_qty;
			v_part_cnt	:= v_part_cnt + 1;
			v_close_order := 'NO';
			v_strlen	:= 0;

			IF (v_stock < v_qty)
			THEN
				--
				IF v_stock <= 0
				THEN
					v_bo_qty	:= v_qty;
					v_qty		:= 0;
					v_pos_qty	:= 0;
					v_bo_cnt	:= v_bo_cnt + 1;
				ELSE
					v_bo_qty	:= v_qty - v_stock;
					v_qty		:= v_stock;
					v_pos_qty	:= v_stock;
				END IF;

				v_bo_string := v_bo_string || rad_val.pnum || ',' || v_bo_qty || ',' || v_price || '|';
			END IF;
		END IF;

		IF p_reason = 3316
		-- Consign To Loaner
		THEN
			v_pos_qty	:= rad_val.qty;
		END IF;

	  	SELECT	C507_LIST_PRICE,C507_UNIT_PRICE,C901_UNIT_PRICE_ADJ_CODE,C507_UNIT_PRICE_ADJ_VALUE,C507_DISCOUNT_OFFERED,C901_DISCOUNT_TYPE , C507_DO_UNIT_PRICE,C507_ADJ_CODE_VALUE
	  		INTO  v_list_price, v_unit_price, v_unit_prc_adj_code, v_unit_price_adj_val, v_disc_offered,v_disc_type ,v_acc_price, v_adj_code_val
		FROM t507_returns_item
	   	WHERE c506_rma_id =  p_raid AND c205_part_number_id = rad_val.pnum AND c507_item_price = rad_val.price
       	AND ROWNUM = 1;
		
		IF v_pos_qty > 0
		THEN
			INSERT INTO t502_item_order
						(c502_item_order_id, c501_order_id, c205_part_number_id, c901_type
					   , c502_item_qty, c502_control_number, c502_item_price,C502_LIST_PRICE,C502_UNIT_PRICE,
					   C901_UNIT_PRICE_ADJ_CODE,C502_UNIT_PRICE_ADJ_VALUE,C502_DISCOUNT_OFFERED,C901_DISCOUNT_TYPE, C502_DO_UNIT_PRICE,C502_ADJ_CODE_VALUE
						)
				 VALUES (s502_item_order.NEXTVAL, v_rev_order_id, rad_val.pnum, v_item_dr_type
					   , v_qty_dr_type * v_pos_qty, v_control_dr_num, rad_val.price,
					   v_list_price, v_unit_price, v_unit_prc_adj_code, v_unit_price_adj_val, v_disc_offered,v_disc_type ,v_acc_price, v_adj_code_val
						);
		END IF;

		--
		INSERT INTO t502_item_order
					(c502_item_order_id, c501_order_id, c205_part_number_id, c901_type
				   , c502_item_qty, c502_control_number, c502_item_price,C502_LIST_PRICE,C502_UNIT_PRICE,
					   C901_UNIT_PRICE_ADJ_CODE,C502_UNIT_PRICE_ADJ_VALUE,C502_DISCOUNT_OFFERED,C901_DISCOUNT_TYPE , C502_DO_UNIT_PRICE,C502_ADJ_CODE_VALUE
					)
			 VALUES (s502_item_order.NEXTVAL, v_rev_order_id, rad_val.pnum, v_item_cr_type
				   , v_qty_cr_type * rad_val.qty, v_control_cr_num, rad_val.price,
				   v_list_price, v_unit_price, v_unit_prc_adj_code, v_unit_price_adj_val, v_disc_offered,v_disc_type ,v_acc_price, v_adj_code_val
					);
	--
	END LOOP;

	IF p_reason = 3317 -- Loaner to Consign - It will insert record in T5050_INVPICK_ASSIGN_DETAIL with ref_type (type of request), ref_id (id of request), created_by-input: ref_type, ref_id, created_by and created_date
                                THEN
                      gm_pkg_allocation.gm_ins_invpick_assign_detail('93001',v_rev_order_id, p_userid);
        END IF;
       IF (v_part_cnt = v_bo_cnt)
	THEN
		v_close_order := 'YES';
	END IF;

	v_strlen	:= NVL (LENGTH (v_bo_string), 0);

	IF (v_strlen > 0)
	THEN
		gm_pkg_op_order_master.gm_op_sav_lon_con_swap (v_bo_string
													 , p_raid
													 , v_parent_order_id
													 , p_usercomments
													 , 50300
													 , 0
													 , p_userid
													  );

		--For back order,  need to update total in order table.
		SELECT SUM (NVL (TO_NUMBER (c502_item_qty) * TO_NUMBER (c502_item_price), 0))
		  INTO v_total
		  FROM t502_item_order
		 WHERE c501_order_id = v_rev_order_id AND c502_void_fl IS NULL;

		UPDATE t501_order
		   SET c501_total_cost = NVL (v_total, 0)
			 , c501_last_updated_by = p_userid
			 , c501_last_updated_date = CURRENT_DATE
		 WHERE c501_order_id = v_rev_order_id;
	END IF;

--
	IF p_reason = 3316 OR v_close_order = 'YES'   -- Consign To Loaner
	THEN
		gm_pkg_cm_shipping_trans.gm_sav_release_shipping (v_rev_order_id, 50180, p_userid, '');
		-- When Order is Completed, Wipe out from the Inventory Pick queue Table.
		gm_pkg_op_inv_scan.gm_check_inv_order_status (v_rev_order_id, 'Completed', p_userid);
		-- To make status of ship info to 40 with Tracking # as N/A
		gm_pkg_cm_shipping_trans.gm_sav_shipout (v_rev_order_id
											   , 50180
											   , ''
											   , ''
											   , ''
											   , ''
											   , 'N/A'
											   , ''
											   , ''
											   , p_userid
											   , ''
											   , v_temp
											   , ''
											   , v_temp
												);
		-- To disable email being sent from the Email Job
		v_shipid	:= gm_pkg_cm_shipping_info.get_shipping_id (v_rev_order_id, 50180, '');
		gm_pkg_common_cancel.gm_op_sav_disable_ship_email (v_shipid, p_userid);
	END IF;
	--to save the corresponding attribute value
	gm_pkg_sav_snapshot_info.gm_sav_snapshot_info('ORDER',v_rev_order_id,'SNAPSHOT_INFO',v_company_id,'1200',p_userid);
--
END gm_save_credit_order_swap_type;
/
