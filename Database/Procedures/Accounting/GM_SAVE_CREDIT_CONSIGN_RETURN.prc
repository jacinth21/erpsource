/* Formatted on 2007/05/02 11:58 (Formatter Plus v4.8.0) */
CREATE OR REPLACE PROCEDURE gm_save_credit_consign_return (
	p_raid			 IN   t506_returns.c506_rma_id%TYPE
  , p_creditdt		 IN   VARCHAR2
  , p_usercomments	 IN   t501_order.c501_comments%TYPE
  , p_userid		 IN   VARCHAR2
  , p_str			 IN   VARCHAR2
)
AS
/*
	  Description			:This procedure is called for Project Reporting
	  Parameters			:p_column
							:p_recordset
*/
	v_strlen	   NUMBER := NVL (LENGTH (p_str), 0);
	v_string	   VARCHAR2 (30000) := p_str;
	v_pnum		   VARCHAR2 (20);
	v_qty		   VARCHAR2 (20);
	v_fl		   VARCHAR2 (10);
	v_substring    VARCHAR2 (1000);
	v_msg		   VARCHAR2 (1000);
	v_id		   NUMBER;
	v_price 	   VARCHAR2 (20);
	v_flag1 	   VARCHAR2 (2);
	v_type		   t506_returns.c506_type%TYPE;
	v_reason	   t506_returns.c506_reason%TYPE;
	v_order_id	   t506_returns.c501_order_id%TYPE;
	v_invid 	   VARCHAR2 (20);
BEGIN
	SELECT c506_type, c506_reason, c501_order_id
	  INTO v_type, v_reason, v_order_id
	  FROM t506_returns
	 WHERE c506_rma_id = p_raid AND c506_void_fl IS NULL;

	--
	UPDATE t507_returns_item
	   SET c507_status_fl = 'C'
	 WHERE c506_rma_id = p_raid AND c507_status_fl = 'R';

	--
	DELETE		t507_returns_item
		  WHERE c506_rma_id = p_raid AND c507_status_fl = 'M';

	--
	IF v_strlen > 0
	THEN
		--
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			--
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_pnum		:= NULL;
			v_qty		:= NULL;
			v_fl		:= NULL;
			v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_qty		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_fl		:= v_substring;

			--
			SELECT s507_return_item.NEXTVAL
			  INTO v_id
			  FROM DUAL;

			--
			v_price 	:= get_part_price (v_pnum, 'E');

			--
			INSERT INTO t507_returns_item
						(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_control_number, c507_item_qty
					   , c507_item_price, c507_status_fl
						)
				 VALUES (v_id, p_raid, v_pnum, '', v_qty
					   , v_price, v_fl
						);
		--
		END LOOP;
	ELSE
		v_msg		:= 'p_str is empty';
	END IF;

	-- This procedure is called to credit the order is its a sales return
	-- Save Credit order proc will be used by Accounting Credit Process. So commenting out the below proc call
	-- GM_SAVE_CREDIT_ORDER(p_RAId, p_usercomments,p_userId);

	--
	IF (v_reason = 3316 OR v_reason = 3317)
	THEN
		gm_save_credit_order_swap_type (p_raid, v_reason, v_order_id, p_creditdt, p_usercomments, p_userid);
	END IF;

--
	UPDATE t506_returns
	   SET c506_status_fl = '2'
		 , c506_credit_date =
			   NVL (TO_DATE (p_creditdt, 'MM/DD/YYYY'), TRUNC (SYSDATE))   -- Will be displayed as Credited Date in credit memo screen
		 , c506_last_updated_by = p_userid
		 , c506_last_updated_date = SYSDATE
	 WHERE c506_rma_id = p_raid AND c506_void_fl IS NULL;

	--
	SELECT c506_update_inv_fl
	  INTO v_flag1
	  FROM t506_returns
	 WHERE c506_rma_id = p_raid;

	--
	IF v_flag1 IS NULL
	THEN
		gm_update_inventory (p_raid, 'R', '', p_userid, v_msg);

		--
		UPDATE t506_returns
		   SET c506_update_inv_fl = '1'
		     , c506_last_updated_by = p_userid
			 , c506_last_updated_date = SYSDATE
		 WHERE c506_rma_id = p_raid AND c506_void_fl IS NULL;
	--
	END IF;

	--
	v_msg		:= v_msg;
--
END gm_save_credit_consign_return;
/
