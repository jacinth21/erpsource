/* 
Purpose: Call Status updater procedure which will update email and invoice statuses as needed
Author: Andrews Stanley
*/
create or replace PROCEDURE gm_rpa_invoice_updater AS
BEGIN
    gm_pkg_rpa_ap_invoice_validator.gm_rpa_apinv_lock_main();
END gm_rpa_invoice_updater;
/