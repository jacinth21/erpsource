/* Formatted on 2011/05/24 15:06 (Formatter Plus v4.8.0) */
--@"C:\Database\Procedures\Accounting\gm_posting_comp_error_mail.prc"

CREATE OR REPLACE PROCEDURE gm_posting_comp_error_mail (
	p_part_number_id   t822_costing_error_log.c205_part_number_id%TYPE
  , p_qty			   t822_costing_error_log.c822_qty%TYPE
  , p_txn_id		   t822_costing_error_log.c822_txn_id%TYPE
  , p_from_cost_id	   t822_costing_error_log.c822_from_costing_type%TYPE
  , p_to_cost_id	   t822_costing_error_log.c822_to_costing_type%TYPE
  , p_txntype		   t804_posting_ref.c901_transaction_type%TYPE
  , p_created_by	   t822_costing_error_log.c822_created_by%TYPE
  ,p_division_id   IN       t810_posting_txn.c901_company_id%TYPE
  ,p_country_id   IN       t810_posting_txn.c901_country_id%TYPE
  ,p_cost_err_type   IN    t822_costing_error_log.c901_country_id%TYPE
  ,p_company_id		IN  t822_costing_error_log.c1900_company_id%TYPE
  ,p_txn_company_id IN  t810_posting_txn.c1900_txn_company_id%TYPE
  ,p_plant_id		IN  t822_costing_error_log.c5040_plant_id%TYPE
)
AS
/********************************************************************************************
 * Description			 :This procedure is used to costing Error log information
 *		  If no cost information found then will log the error into the system
  *******************************************************************************************/
--
	subject 	   VARCHAR2 (100) := 'COGS ERROR (POSTING FAILED) ';
	mail_body	   VARCHAR2 (3000);
	crlf		   VARCHAR2 (2) := CHR (13) || CHR (10);
	to_mail 	   t906_rules.c906_rule_value%TYPE;
	v_company_name  t1900_company.c1900_company_name%TYPE;
	v_txn_company_name  t1900_company.c1900_company_name%TYPE;
	v_plant_name t5040_plant_master.c5040_plant_name%TYPE;
	v_cost_error_type t901_code_lookup.c901_code_nm%TYPE;
	v_from_bucket_nm t901_code_lookup.c901_code_nm%TYPE;
	v_to_bucket_nm t901_code_lookup.c901_code_nm%TYPE;
	v_user_nm VARCHAR2(4000);
--
BEGIN
--
	SELECT get_company_name (p_company_id), get_plant_name (p_plant_id), get_code_name (p_cost_err_type)
  		, get_code_name (p_from_cost_id), get_code_name (p_to_cost_id), get_user_name (p_created_by)
  		, get_company_name (p_txn_company_id)
  	INTO v_company_name, v_plant_name, 	v_cost_error_type
  	, v_from_bucket_nm, v_to_bucket_nm, v_user_nm
  	, v_txn_company_name
   FROM dual;
	subject := subject || ' - ' || v_company_name;
	--
	-- Below code to generate error and mail the same to end user
	--
	mail_body	:=
		' Company Name :- '
		|| v_company_name
		|| crlf
		||  ' Transaction Company   :- '
		|| v_txn_company_name
		|| crlf
		|| ' Plant Name   :- '
		|| v_plant_name
		|| crlf
		|| ' Costing Error Type  :- '
		|| v_cost_error_type
		|| crlf
		||  ' Part Number   :- '
		|| p_part_number_id
		|| crlf
		|| ' Qty Failed   :- '
		|| p_qty
		|| crlf
		|| ' Transaction ID  :- '
		|| p_txn_id
		|| crlf
		|| ' From Costing ID :- '
		|| v_from_bucket_nm
		|| crlf
		|| ' To Costing ID	 :- '
		|| v_to_bucket_nm
		|| crlf
		|| ' Created By 	 :- '
		|| v_user_nm
		|| crlf;
	
	--
	IF p_cost_err_type IS NULL
	THEN
		to_mail 	:= get_rule_value ('COGS_IT', 'EMAIL');
		subject     := 'POSTING CONTEXT NOT FOUND ERROR ';
	ELSE
		to_mail 	:= get_rule_value ('COGS', 'EMAIL');
	END IF;
	
	--
	gm_com_send_email_prc (to_mail, subject, mail_body);
--
END gm_posting_comp_error_mail;
/
