CREATE OR REPLACE PROCEDURE GM_UPDATE_CUSTOMER_PO
(
	p_accid		IN T503_INVOICE.C704_ACCOUNT_ID%TYPE,
	p_invid 	IN T503_INVOICE.C503_INVOICE_ID%TYPE,
	p_oldpo		IN T503_INVOICE.C503_CUSTOMER_PO%TYPE,
	p_newpo		IN T501_ORDER.C501_CUSTOMER_PO%TYPE,
	p_userid	IN T501_ORDER.C501_LAST_UPDATED_BY%TYPE,
	p_message 	OUT  VARCHAR2
)
AS
/****************************************************
 * This procedure is used to update the PO number 
 *****************************************************/
BEGIN
	--
	UPDATE	 T501_ORDER
	SET		 C501_CUSTOMER_PO		= p_newpo
			,C501_LAST_UPDATED_BY	= p_UserId
			,C501_LAST_UPDATED_DATE	= SYSDATE
	WHERE	C503_INVOICE_ID			= p_invid
	AND		C704_ACCOUNT_ID			= p_accid;
	--
	UPDATE	T503_INVOICE
	SET		C503_CUSTOMER_PO		= p_newpo
	WHERE	C503_INVOICE_ID			= p_invid;
	--
EXCEPTION WHEN OTHERS
THEN
	p_message := SQLERRM;
END GM_UPDATE_CUSTOMER_PO;
/
