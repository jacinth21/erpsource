
/* Formatted on 2011/04/08 09:21 (Formatter Plus v4.8.0) */
/* @"c:\database\procedures\accounting\gm_report_invoice_dashboard.prc"; */

CREATE OR REPLACE PROCEDURE gm_report_invoice_dashboard (
	p_acc_curr	IN t704_account.c901_currency%TYPE default NULL,
	p_message	  OUT	VARCHAR2
  , p_recordset   OUT	TYPES.cursor_type
)
AS
 v_company_id        t1900_company.c1900_company_id%TYPE;
/*************************************************************************************************
 * Description		:This procedure is called to get the Pending Invoice Generation Dashboard data
 * Parameters		:
 *
 * Change history
 * D James - May 16 - Added MDATA to show the Order Date on Screen.
 * 
 **************************************************************************************************/
BEGIN
	--my_context.set_my_inlist_ctx (get_rule_value ('DASHEXLIST', 'DASHORDERTYPE'));
SELECT get_compid_frm_cntx() INTO v_company_id  FROM dual;
	OPEN p_recordset
	 FOR
	     
		 SELECT   t501.c501_customer_po po, SUM (t501.c501_total_cost + NVL (t501.c501_ship_cost, 0)) COST
				, get_account_name (t501.c704_account_id) NAME, get_account_dist_name (t501.c704_account_id) dname
				, DECODE (MIN (t501.c501_status_fl)
						, 0, 'Back Order'
						, 1, 'Pending Control Number'
						, 2, 'Pending Shipment'
						, 3, 'Shipped'
						 ) ordstatus
				, t501.c704_account_id ID, MIN (t501.c501_order_date) mdate, MIN (t501.c501_surgery_date) surg_date
				, NVL (t501.c501_hold_fl, '-') holdfl
				, MIN(t501.c501_customer_po_date) cust_po_date
				,t101.C101_PARTY_NM paracctnm 
                ,t101.c101_party_id paracctid 
                , get_code_name(t704.c901_currency) currsymbol 
               , t1900.c1900_currency_format currformat  
                 , t704.c704_account_id ACCTID
			 FROM t501_order t501,t704_account t704, t101_party t101, t1900_company t1900
			WHERE t501.c501_customer_po IS NOT NULL
			  AND t501.c704_account_id = t704.c704_account_id
              AND t704.c101_party_id = t101.c101_party_id
              AND t501.c1900_company_id = t1900.c1900_company_id
              AND t501.c1900_company_id = v_company_id
              AND t704.c901_currency = NVL(p_acc_curr, t704.c901_currency)
			  AND t501.c503_invoice_id IS NULL
			  AND (t501.c901_ext_country_id is NULL
			  OR t501.c901_ext_country_id  in (select country_id from v901_country_codes))
			  AND t501.c501_delete_fl IS NULL
			  AND t501.c501_void_fl IS NULL
			  AND NVL(t704.c901_account_type, -999) <> '26240482' -- added this line to exclude OUS PO displaying in US Dashboard - PMT-31253
			  AND NVL (t501.c901_order_type, -999) NOT IN ( 
			  SELECT t906.c906_rule_value
                  FROM t906_rules t906
                 WHERE T906.C906_RULE_GRP_ID = 'DASHORDERTYPE'
                   AND c906_rule_id = 'DASHEXLIST')   --currently Sales Discount order type (2535) are excluded
			  AND NVL (t501.c901_order_type, -9999) NOT IN (
                SELECT t906.c906_rule_value
                  FROM t906_rules t906
                 WHERE t906.c906_rule_grp_id = 'ORDTYPE'
                   AND c906_rule_id = 'EXCLUDE')	
              AND NVL (t501.c901_order_type, -9999) NOT IN (
                SELECT t906.c906_rule_value
                  FROM t906_rules t906
                 WHERE t906.c906_rule_grp_id = 'ORDERTYPE'
                   AND c906_rule_id = 'EXCLUDE')												
		 GROUP BY t501.c501_customer_po, t501.c704_account_id, t501.c501_hold_fl,
		          t101.C101_PARTY_NM,t101.c101_party_id
		          ,t1900.c1900_currency_format
		          , t704.c704_account_id
		          , t704.c901_currency
		 ORDER BY NAME, po;
END gm_report_invoice_dashboard;
/
