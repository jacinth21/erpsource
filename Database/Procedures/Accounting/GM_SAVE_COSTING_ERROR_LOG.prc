/* Formatted on 2011/05/24 15:06 (Formatter Plus v4.8.0) */
--@"C:\Database\Procedures\Accounting\gm_save_costing_error_log.prc"

CREATE OR REPLACE PROCEDURE gm_save_costing_error_log (
	p_part_number_id   t822_costing_error_log.c205_part_number_id%TYPE
  , p_qty			   t822_costing_error_log.c822_qty%TYPE
  , p_txn_id		   t822_costing_error_log.c822_txn_id%TYPE
  , p_from_cost_id	   t822_costing_error_log.c822_from_costing_type%TYPE
  , p_to_cost_id	   t822_costing_error_log.c822_to_costing_type%TYPE
  , p_txntype		   t804_posting_ref.c901_transaction_type%TYPE
  , p_party_id		   t810_posting_txn.c803_period_id%TYPE
  , p_created_by	   t822_costing_error_log.c822_created_by%TYPE
  ,p_division_id   IN       t810_posting_txn.c901_company_id%TYPE
  ,p_country_id   IN       t810_posting_txn.c901_country_id%TYPE
  ,p_cost_err_type   IN    t822_costing_error_log.c901_country_id%TYPE
  ,p_local_company_id IN t822_costing_error_log.c1900_owner_company_id%TYPE
  ,p_plant_id IN t822_costing_error_log.c5040_plant_id%TYPE
  ,p_txn_comp_id   IN t810_posting_txn.c1900_txn_company_id%TYPE
  ,p_destination_comp_id   IN t810_posting_txn.c1900_destination_company_id%TYPE
  ,p_owner_comp_id		IN t810_posting_txn.c1900_owner_company_id%TYPE
  ,p_local_comp_cost IN t822_costing_error_log.c822_local_company_cost%TYPE
)
AS
/********************************************************************************************
 * Description			 :This procedure is used to costing Error log information
 *		  If no cost information found then will log the error into the system
  *******************************************************************************************/
--
-- PC-991: To capture the local cost information and repost time used the local cost.
-- Added the new parameter value (p_local_comp_cost)

	subject 	   VARCHAR2 (100) := 'COGS ERROR (POSTING FAILED) ';
	mail_body	   VARCHAR2 (3000);
	crlf		   VARCHAR2 (2) := CHR (13) || CHR (10);
	to_mail 	   t906_rules.c906_rule_value%TYPE;
	v_company_id_ctx  t1900_company.c1900_company_id%TYPE;
	v_division_id T1910_DIVISION.C1910_DIVISION_ID%TYPE;
	v_division_nm T1910_DIVISION.C1910_DIVISION_NAME%TYPE;
--
BEGIN
--
	--
	
	--Get the Division info for the Part Number;
	BEGIN
		  SELECT t1910.C1910_DIVISION_ID, t1910.C1910_DIVISION_NAME  
		  INTO v_division_id , v_division_nm
		  FROM T205_PART_NUMBER t205,T202_PROJECT t202,T1910_DIVISION t1910
		  WHERE t202.c202_project_id = t205.c202_project_id (+)
		  AND t1910.C1910_DIVISION_ID = t202.C1910_DIVISION_ID(+)
		  AND t202.C202_VOID_FL(+) is null
		  AND t1910.C1910_VOID_FL(+) is null
		  AND t205.C205_PART_NUMBER_ID = p_part_number_id;
	EXCEPTION WHEN OTHERS
	THEN
		v_division_id :='';
		v_division_nm :='';
	END;
	
	INSERT INTO t822_costing_error_log
				(c822_costing_error_log_id, c822_qty, c205_part_number_id, c822_txn_id, c822_from_costing_type
			   , c822_to_costing_type, c822_created_by, c822_created_date, c901_posting_type, c810_party_id,c901_company_id,c901_country_id,c901_error_type,c1900_company_id
			   , c5040_plant_id, c1900_txn_company_id, c1900_destination_company_id, c1900_owner_company_id,C1910_DIVISION_ID,C1910_DIVISION_NAME
			   , c822_local_company_cost
				)
		 VALUES (s822_costing_error_log.NEXTVAL, p_qty, p_part_number_id, p_txn_id, p_from_cost_id
			   , p_to_cost_id, p_created_by, SYSDATE, p_txntype, p_party_id,p_division_id,p_country_id,p_cost_err_type,p_local_company_id
			   , p_plant_id, p_txn_comp_id, p_destination_comp_id, p_owner_comp_id,v_division_id,v_division_nm
			   , p_local_comp_cost
				);

	
	--
	-- Below code to generate error and mail the same to end user
	-- If there are any error send email
	gm_posting_comp_error_mail(p_part_number_id,p_qty,p_txn_id,p_from_cost_id,p_to_cost_id,p_txntype,p_created_by,p_division_id,p_country_id,p_cost_err_type,p_local_company_id, p_txn_comp_id, p_plant_id);
	
END gm_save_costing_error_log;
/
