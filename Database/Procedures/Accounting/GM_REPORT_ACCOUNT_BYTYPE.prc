/* Formatted on 2008/07/30 15:03 (Formatter Plus v4.8.0) */
CREATE OR REPLACE PROCEDURE gm_report_account_bytype (
	p_accounttypes	 IN 	  VARCHAR
  , p_recordset 	 OUT	  TYPES.cursor_type
)
AS
/*
	 Description		:This procedure is called for Project Reporting
	 Parameters 		:p_column
					 :p_recordset
*/
	v_str		   VARCHAR2 (2000);
BEGIN
	my_context.set_my_inlist_ctx (p_accounttypes);

	OPEN p_recordset
	 FOR
		 SELECT   c704_account_id ID, c704_account_nm NAME, c703_sales_rep_id repid
				, get_dist_rep_name (c703_sales_rep_id) dname, get_rep_name (c703_sales_rep_id) rname
				, get_account_gpo_id (c704_account_id) gpoid, get_distributor_id (c703_sales_rep_id) did
			 FROM t704_account
			WHERE t704_account.c901_account_type IN (SELECT token
													   FROM v_in_list)
			      AND (t704_account.c901_ext_country_id is NULL
			      OR t704_account.c901_ext_country_id  in (select country_id from v901_country_codes))
		 ORDER BY c704_account_nm;
END gm_report_account_bytype;
/
