/* Formatted on 2011/05/24 15:04 (Formatter Plus v4.8.0) */
--@"C:\Database\Procedures\Accounting\GM_SAVE_LEDGER_TXN_POSTING.prc";

CREATE OR REPLACE PROCEDURE GM_SAVE_LEDGER_TXN_POSTING (
	p_txntype			IN	 t810_posting_txn.c901_account_txn_type%TYPE
  , p_act_type			IN	 t802_account_object_mapping.c901_account_type%TYPE
  , p_acct_date 		IN	 t810_posting_txn.c810_acct_date%TYPE
  , p_party_id			IN	 t810_posting_txn.c810_party_id%TYPE
  , p_txn_id			IN	 t810_posting_txn.c810_txn_id%TYPE
  , p_partnum			IN	 t810_posting_txn.c205_part_number_id%TYPE
  , p_costing_id		IN	 t810_posting_txn.c820_costing_id%TYPE
  , p_qty				IN	 t810_posting_txn.c810_qty%TYPE
  , p_dr_amount 		IN	 t810_posting_txn.c810_dr_amt%TYPE
  , p_cr_amount 		IN	 t810_posting_txn.c810_cr_amt%TYPE
  , p_updtedby			IN	 t810_posting_txn.c810_created_by%TYPE
  ,p_company_id         IN   t810_posting_txn.c901_company_id%TYPE
  ,p_country_id         IN   t810_posting_txn.c901_country_id%TYPE
  ,p_desc               IN   t810_posting_txn.c810_desc%TYPE
  , p_local_company_id IN t810_posting_txn.c1900_company_id%TYPE
  , p_plant_id IN t5040_plant_master.c5040_plant_id%TYPE
  , p_owner_company_id IN t810_posting_txn.c1900_owner_company_id%TYPE
  , p_txn_company_id IN t810_posting_txn.c1900_txn_company_id%TYPE
  , p_destination_company_id IN t810_posting_txn.c1900_owner_company_id%TYPE
  , p_local_dr_amt  IN t810_posting_txn.c810_cr_amt%TYPE
  , p_local_cr_amt  IN t810_posting_txn.c810_cr_amt%TYPE
)
AS
v_local_cost NUMBER;

/************************************************************************************
 * Description			 :This procedure is used to get accounting information
 *						  based on account type mapping and record the ledger
 *						  transaction  into Inventory  Table
 *						  All Transaction related to inventory will be stored in this table
 *							'Level of search'
 *							*****************
 *							2 level search
 *							I.	Part Level Mapping Search
 *							II. Part Type level Search (Instrument/Implants/Graph Case)
 *		p_posting_type		'Each TXN Holds below type of posting'
 *							**************************************
 *							4806 -- Actual Value Posting
 *							4807 -- COGS Value Posting
 *							4808 -- COGS Return value posting
 *************************************************************************************/
--
	v_account_element_id t810_posting_txn.c801_account_element_id%TYPE;
	v_division_id T1910_DIVISION.C1910_DIVISION_ID%TYPE;
	v_division_nm T1910_DIVISION.C1910_DIVISION_NAME%TYPE;
BEGIN
--
	SELECT get_partnum_account_element (p_act_type, p_partnum)
		, DECODE(p_local_dr_amt, 0, p_local_cr_amt, p_local_dr_amt) 
	  INTO v_account_element_id, v_local_cost
	  FROM DUAL;

	IF v_account_element_id IS NULL
	THEN
		--102940 Costing Error
		-- PC-991: To pass the local cost information and it used at repost COGS error
		
		gm_save_costing_error_log (p_partnum, p_qty, p_txn_id, p_act_type, -9000, p_txntype, p_party_id, p_updtedby,p_company_id,p_country_id,102940, p_local_company_id, p_plant_id, p_txn_company_id, p_destination_company_id, p_owner_company_id
			, v_local_cost);
	END IF;
	
	
	--Get the Division info for the Part Number;
	BEGIN
		  SELECT t1910.C1910_DIVISION_ID, t1910.C1910_DIVISION_NAME  
		  INTO v_division_id , v_division_nm
		  FROM T205_PART_NUMBER t205,T202_PROJECT t202,T1910_DIVISION t1910
		  WHERE t202.c202_project_id = t205.c202_project_id(+)
		  AND t1910.C1910_DIVISION_ID = t202.C1910_DIVISION_ID (+)
		  AND t202.C202_VOID_FL(+) is null
		  AND t1910.C1910_VOID_FL(+) is null
		  AND t205.C205_PART_NUMBER_ID=p_partnum;
	EXCEPTION WHEN OTHERS
	THEN
		v_division_id :='';
		v_division_nm :='';
	END;

	INSERT INTO t810_posting_txn
				(c810_account_txn_id, c901_account_txn_type, c810_party_id, c810_txn_id
			   , c803_period_id, c205_part_number_id
			   , c801_account_element_id, c820_costing_id, c810_txn_date
			   , c810_acct_date, c810_desc, c810_dr_amt, c810_cr_amt, c810_qty, c810_delete_fl, c810_created_by
			   , c810_created_date,c901_company_id,c901_country_id,c1900_company_id
			   , c5040_plant_id, c1900_owner_company_id, c901_owner_currency
			   , c810_local_company_dr_amt, c810_local_company_cr_amt, c901_local_company_currency
			   , c1900_destination_company_id, c1900_txn_company_id ,C1910_DIVISION_ID,C1910_DIVISION_NAME
				)
		 VALUES (s810_posting_txn.NEXTVAL, p_txntype, p_party_id, p_txn_id
			   , NULL	-- Currently period is going to hold null value (handle the same in future release)
					 , p_partnum
			   , v_account_element_id	-- To get the Account Mapping information
									 , p_costing_id, TRUNC (CURRENT_DATE)
			   , p_acct_date, p_desc, p_dr_amount, p_cr_amount, p_qty, 'N', p_updtedby
			   , CURRENT_DATE,p_company_id,p_country_id,p_local_company_id
			   , p_plant_id, p_owner_company_id, get_comp_curr(p_owner_company_id)
			   , p_local_dr_amt, p_local_cr_amt, get_comp_curr(p_local_company_id)
			   , p_destination_company_id, p_txn_company_id , v_division_id, v_division_nm
				);
--
END GM_SAVE_LEDGER_TXN_POSTING;
/
