/* Formatted on 2009/12/30 18:49 (Formatter Plus v4.8.0) */
CREATE OR REPLACE PROCEDURE gm_report_account (
    p_message	  OUT	VARCHAR2
  , p_recordset   OUT	TYPES.cursor_type
  , p_acct_type_id IN VARCHAR2 DEFAULT NULL
)
AS
/*
	  Description			:This procedure is called for Project Reporting
	  Parameters			:p_column
							:p_recordset
*/
	v_str		   VARCHAR2 (2000);
	v_company_id   t704_account.c1900_company_id%TYPE;
	v_plant_id     t5040_plant_master.c5040_plant_id%TYPE;
	v_comp_lang_id T1900_COMPANY.C901_LANGUAGE_ID%TYPE;
	v_com_plant_Fl  VARCHAR2(20);
	v_condition     CLOB;
	v_query         CLOB;
	v_acct_type_id  NUMBER;
BEGIN
	--
     SELECT get_compid_frm_cntx (),get_plantid_frm_cntx() INTO v_company_id , v_plant_id  FROM DUAL;
	 SELECT NVL(get_complangid_frm_cntx(),GET_RULE_VALUE('DEFAULT_LANGUAGE','ONEPORTAL')) INTO  v_comp_lang_id FROM DUAL;
     BEGIN
     	SELECT GET_RULE_VALUE(v_company_id,'LOANERINITFILTER') INTO v_com_plant_Fl FROM DUAL;
     EXCEPTION WHEN NO_DATA_FOUND THEN
		v_com_plant_Fl := NULL;
	 END;
	 
     IF v_com_plant_Fl =  'Plant'   THEN
		 v_company_id := null;
	 ELSE
	 	 v_plant_id := null;
	 END IF;      	
	
	BEGIN
		SELECT to_number(TRIM(p_acct_type_id)) 
		INTO v_acct_type_id FROM dual;
	EXCEPTION WHEN OTHERS THEN
		v_acct_type_id := NULL;
	END;
	
	OPEN p_recordset
	 FOR 
	  SELECT   c704_account_id ID, DECODE(v_comp_lang_id,'103097',NVL(c704_account_nm_en,c704_account_nm),c704_account_nm) NAME, c703_sales_rep_id repid
				, get_dist_rep_name (c703_sales_rep_id) dname, get_rep_name (c703_sales_rep_id) rname
				, get_account_gpo_id (c704_account_id) gpoid, get_distributor_id (c703_sales_rep_id) did,C101_DEALER_ID dealid
			 FROM t704_account
			WHERE c704_void_fl IS NULL
			AND c901_account_type = nvl(v_acct_type_id,c901_account_type) AND (c1900_company_id = v_company_id OR  c1900_company_id IN (SELECT c1900_company_id FROM t5041_plant_company_mapping WHERE c5040_plant_id = v_plant_id  AND c5041_void_fl IS NULL AND c5041_primary_fl = 'Y'))
			AND (c901_ext_country_id is NULL	
			OR c901_ext_country_id  in (select country_id from v901_country_codes)) 
		 ORDER BY UPPER(NAME);
	    
EXCEPTION
	WHEN OTHERS
	THEN
		p_message	:= '1000';
END gm_report_account;
/
