/* Formatted on 2008/04/14 17:27 (Formatter Plus v4.8.0) */
CREATE OR REPLACE PROCEDURE gm_save_order_consign_to_cogs (
	p_ordid 	   IN	t501_order.c501_order_id%TYPE
  , p_updated_by   IN	VARCHAR2
--,p_ra_id OUT VARCHAR2 uncomment later when this is called from the Application
)
/****************************************************************************
 * Description : This procedure is called to
 *
 * Parameters	:
 *****************************************************************************/
AS
--
	v_status_fl    CHAR (1);
	v_ord_type	   NUMBER;
	v_void_fl	   CHAR (1);
	v_inv_fl	   CHAR (1);
	v_party_id	   VARCHAR2 (20);
	v_dist_id	   VARCHAR2 (20);
	--v_string	   VARCHAR2 (20);
	subject 	   VARCHAR2 (100);
	mail_body	   VARCHAR2 (3000);
	to_mail 	   t906_rules.c906_rule_value%TYPE;
	v_user_name    VARCHAR2 (100);
	v_dist_name    VARCHAR2 (100);
	crlf		   VARCHAR2 (2) := CHR (13) || CHR (10);
	v_company_id   t1900_company.c1900_company_id%TYPE;
	v_plant_id 	   t5040_plant_master.c5040_plant_id%TYPE;
--
	CURSOR cur_sales
	IS
		SELECT c205_part_number_id pnum, c502_item_qty qty, c502_item_price price
		  FROM t502_item_order
		 WHERE c501_order_id = p_ordid;
--
BEGIN

	SELECT NVL(GET_COMPID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL')), NVL(GET_PLANTID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_PLANT','ONEPORTAL'))
          INTO v_company_id, v_plant_id
          FROM DUAL;
	


	--
	SELECT	   c501_status_fl, c901_order_type, c501_void_fl, c501_update_inv_fl
			 , get_distributor_id (c703_sales_rep_id), get_distributor_name(get_distributor_id (c703_sales_rep_id))
		  INTO v_status_fl, v_ord_type, v_void_fl, v_inv_fl, v_dist_id, v_dist_name
		  FROM t501_order
		 WHERE c501_order_id = p_ordid
	FOR UPDATE;

	--
	-- Throw error if the status flag is not 0 or 1
	IF v_status_fl > 1
	THEN
		raise_application_error (-20500, 'Order is already controlled/shipped/verified');
	END IF;

	--
	-- Throw error if the order is voided
	IF v_void_fl IS NOT NULL
	THEN
		raise_application_error (-20500, 'Order is already voided');
	END IF;

	--
	-- Throw error if the posting has already happened
	IF v_inv_fl IS NOT NULL
	THEN
		raise_application_error (-20500, 'Posting has already happened for this Order');
	END IF;

	--
	-- To get Party Id for Ledger Posting
	SELECT get_acct_party_id (p_ordid, 'S')
	  INTO v_party_id
	  FROM DUAL;

	-- Updating Order type to -'Bill Only - From Sales Consignments' and status flag to 2
	UPDATE t501_order
	   SET c901_order_type = 2532
		 , c501_status_fl = 2
		 , c501_update_inv_fl = 1
		 , c501_shipping_date = SYSDATE
		 , c501_last_updated_by = p_updated_by
		 , c501_last_updated_date = SYSDATE
	 WHERE c501_order_id = p_ordid;
	
	
		-- update the FS warehouse Qty
		-- 4302 Minus
		-- 102900 - Consignment
		-- 4000337 - Bill Only Consignment
		gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv(p_ordid, 'T501', v_dist_id, 102900, 4302, 4000337, p_updated_by);
			
	--
	-- Posting from Sales Consignment to COGS
	FOR rad_val IN cur_sales
	LOOP
		gm_save_ledger_posting (48085
							  , CURRENT_DATE
							  , rad_val.pnum
							  , v_party_id
							  , p_ordid
							  , rad_val.qty
							  , rad_val.price
							  , p_updated_by
							   );
	--
	END LOOP;

	-- Commented since Bill only consignment adjust (decreases) Field Sales warehouse qty and for bill only consignment we don't create a return
/*	-- Insert a RA Record (Writeoff type) 3318 - No Accounting Posting
	SELECT 'GM-RA-' || s506_return.NEXTVAL
	  INTO v_string
	  FROM DUAL;

	--
	INSERT INTO t506_returns
				(c506_rma_id, c701_distributor_id, c506_type, c506_reason, c506_expected_date, c506_status_fl
			   , c506_created_by, c506_created_date, c506_return_date, c506_credit_date, c501_reprocess_date
			   , c506_comments, c1900_company_id, c5040_plant_id
				)
		 VALUES (v_string, v_dist_id, 3302, 3318, SYSDATE, 2
			   , p_updated_by, TRUNC (SYSDATE), TRUNC (SYSDATE), TRUNC (SYSDATE), TRUNC (SYSDATE)
			   , 'RA Created to offset Bill Only Order: ' || p_ordid, v_company_id, v_plant_id
				);

	-- Insert into Returns Items Table
	FOR rad_val IN cur_sales
	LOOP
		INSERT INTO t507_returns_item
					(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_item_qty, c507_item_price
				   , c507_control_number, c507_status_fl
					)
			 VALUES (s507_return_item.NEXTVAL, v_string, rad_val.pnum, rad_val.qty, rad_val.price
				   , '#NOC', 'W'
					);
	--
	END LOOP;*/

--
	--p_ra_id := v_string; -- uncomment later when this is called from the Application
		--to_mail 	:= 'suchitra@globusmedical.com,djames@globusmedical.com,lmohn@globusmedical.com,mpatel@globusmedical.com';
		to_mail 	:= 'mpatel@globusmedical.com';
		v_user_name	:= get_user_name (p_updated_by);
		subject 	:= 'Bill Only (Consignment) Order Credit for ' || v_dist_name;
		mail_body	:=
			   'Field Sales Warehouse updated to offset Order ID:'||p_ordid
			|| crlf
			|| 'Distributor name :- '
			|| v_dist_name
			|| crlf
			|| 'Created by :- '
			|| v_user_name
			|| crlf
			|| 'Created Date	:- '
			|| TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss')
			|| crlf
			|| crlf
			|| 'Please consign the replacement parts to the Distributor and also print/send the Credit Memo for the Order ID'
			;
		gm_com_send_email_prc (to_mail, subject, mail_body);
	--
	DBMS_OUTPUT.put_line ('Field Sales Warehouse updated to offset Order: ' || p_ordid);

	COMMIT;
EXCEPTION
	WHEN OTHERS
	THEN
		DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
		ROLLBACK;
END gm_save_order_consign_to_cogs;
/
