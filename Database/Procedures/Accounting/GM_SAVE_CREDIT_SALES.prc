/* Formatted on 2011/01/10 18:20 (Formatter Plus v4.8.0) */
 -- @"C:\database\Procedures\Accounting\Gm_Save_Credit_Sales.prc"

CREATE OR REPLACE PROCEDURE gm_save_credit_sales (
	p_invid 	 IN 	  t506_returns.c503_invoice_id%TYPE
  , p_accid 	 IN 	  t506_returns.c704_account_id%TYPE
  , p_str		 IN 	  VARCHAR2
  , p_comments	 IN 	  t506_returns.c506_comments%TYPE
  , p_userid	 IN 	  t506_returns.c506_created_by%TYPE
  , p_custpo	 IN 	  t503_invoice.c503_customer_po%TYPE
  , p_inv_type	 IN 	  VARCHAR2
  , p_order_id	 OUT	  VARCHAR2
  , p_newinvid	 OUT	  VARCHAR2
)
AS
--
		
	v_new_order_id VARCHAR2 (20);
	v_cur_order_id VARCHAR2 (20);
	v_prv_order_id VARCHAR2 (20) := 'DUMMY';
	v_order_type   NUMBER := 2524;
	v_strlen	   NUMBER := NVL (LENGTH (p_str), 0);
	v_string	   VARCHAR2 (30000) := p_str;
	v_pnum		   VARCHAR2 (20);
	v_qty		   NUMBER;
	v_price 	   NUMBER (15, 2);
	v_total_price  NUMBER (15, 2);
	v_substring    VARCHAR2 (1000);
	v_msg		   VARCHAR2 (1000);
	v_id		   NUMBER;
	v_id_string    VARCHAR2 (20);
	v_new_invid    t503_invoice.c503_invoice_id%TYPE;
	v_pay_terms    t503_invoice.c503_terms%TYPE;
	v_itemtype	   NUMBER;
	v_cnt		   NUMBER;
	v_inv_type	   NUMBER;	
	v_price_type   NUMBER;
    v_exclude_vat  VARCHAR2(5);
    v_ordatt	   VARCHAR2(2000) :='';
    v_country_fl   VARCHAR2(10); 
    v_invoice_source NUMBER;
    v_distid   VARCHAR2(20); 
    v_ref_id VARCHAR2(20);
    v_totamt NUMBER:=0;
    v_company_id        t1900_company.c1900_company_id%TYPE;
    v_unitprice	t502_item_order.c502_unit_price%TYPE;
    v_list_price 	t502_item_order.c502_list_price%TYPE;
    v_adjcode	t502_item_order.c901_unit_price_adj_code%TYPE;
    v_adjval	t502_item_order.c502_unit_price_adj_value%TYPE;
    v_discoff	t502_item_order.c502_discount_offered%TYPE;
    v_disctype	t502_item_order.c901_discount_type%TYPE;
    v_syscalunitprice	t502_item_order.c502_system_cal_unit_price%TYPE;
    v_comma_count	NUMBER;
    v_item_ord_id	t502_item_order.c502_item_order_id%TYPE;
    v_new_item_ord_id	t502_item_order.c502_item_order_id%TYPE;
    -- Below value is to check whether only for values are getting passing in the string or more that that.
    -- Existing string is having only 4 values. This condition is to avoid issues if the procedure is getting called from any other screen
    v_str_value_cnt	NUMBER:= 4;
    -- check const. flag  
    v_con_vat_fl	VARCHAR2(1);
	v_const_fl VARCHAR2(1);
	v_order_usage_fl  VARCHAR2(10);
	v_dealer_id t503_invoice.c101_dealer_id%TYPE;
	v_accid   t506_returns.c704_account_id%TYPE;
	v_sync_comp_id   t1900_company.c1900_company_id%TYPE;
	v_hsn_code        t502_item_order.c502_hsn_code%TYPE;
	--
	v_master_parent_ord_id t501_order.c501_order_id%TYPE;
	v_vat_rate  t502_item_order.C502_VAT_RATE%TYPE;
	v_cgst_rate  t502_item_order.C502_CGST_RATE%TYPE;
	v_igst_rate  t502_item_order.C502_IGST_RATE%TYPE;
	v_sgst_rate  t502_item_order.C502_SGST_RATE%TYPE;
	v_debit_rule VARCHAR2(1);
	v_acc_cmp_id    t704_account.c704_account_id%TYPE;
    v_user_plant_id   t501_order.c5040_plant_id%TYPE;
--
BEGIN
	--26240213 dealer,50253 - ICS ,50255 - Account
	SELECT c503_terms, DECODE(C901_INVOICE_SOURCE,26240213,26240213,50253,50253,50255) ,c701_distributor_id
		, c1900_company_id, NVL(get_rule_value(c1900_company_id,'CORONA_VAT'),'N') -- PC-3593 Revert original VAT rate in Germany
	  INTO v_pay_terms, v_invoice_source, v_distid
	  	, v_company_id, v_debit_rule
	  FROM t503_invoice
	 WHERE c503_invoice_id = p_invid;

	 SELECT NVL(GET_RULE_VALUE('US','COUNTRY'),'-999') INTO v_country_fl FROM DUAL;
	 --to get the company id to perform operations for Chennai/Delhi
	SELECT get_rule_value_by_company(v_company_id,'GST_COMP',v_company_id) 
	  INTO v_sync_comp_id 
	  FROM DUAL; 
 
	 
	 IF p_inv_type = 'CREDIT' THEN 
     	v_inv_type   := 50202; -- Credit invoice 
    	v_price_type := -1;    -- negative amount 
	 ELSIF p_inv_type = 'DEBIT' THEN
    	v_inv_type   := 50203; -- debit invoice 
     	v_price_type := 1;     -- positive amount 
	 END IF;
	 
	 --From Japan , p_accid will hold the dealer id , if they loaded the dealer invoice,  passing the same for dealer param .
	--Based on invoice source we are generating the invoice. 
	 IF v_invoice_source = '26240213'
	 THEN
	 	v_dealer_id := p_accid;
	 	v_accid := NULL;
	 ELSE
	 	v_dealer_id := NULL;
	 	v_accid := p_accid;
	 END IF;
	 
	 --
	v_order_usage_fl   := get_rule_value_by_company (v_company_id, 'ORDER_USAGE', v_company_id) ;
	
	gm_generate_invoice (p_custpo, v_accid, v_pay_terms, TRUNC(CURRENT_DATE), p_userid, 'CREDITSALES', v_inv_type, v_invoice_source, v_new_invid,v_dealer_id,NULL);
	
	/* Currency is getting displayed based on distributor on post payment screen.
	 * Update Script is added for PMT-834 which is needed to get the currency value for newly generated invoice	 *
	 *
	 */
	IF v_invoice_source = 50253 THEN --50253 ICS
		UPDATE t503_invoice 
		SET    c701_distributor_id = v_distid , 
		       c503_last_updated_by = p_userid , 
		       c503_last_updated_date = CURRENT_DATE 
	   WHERE   c503_invoice_id = v_new_invid
	   AND     c503_void_fl  is NULL;  
	END IF;
	
	WHILE INSTR (v_string, '|') <> 0
	LOOP
		v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
		v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
		--count how many commas in each substring
	    SELECT trim( LENGTH( v_substring ) ) - trim( LENGTH( TRANSLATE( v_substring, 'A^', 'A' ) ) )
	    	INTO v_comma_count
		FROM dual;
		v_cur_order_id := NULL;
		v_pnum		:= NULL;
		v_qty		:= NULL;
		v_price 	:= NULL;
		v_cur_order_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
		v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
		v_qty		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
		v_price 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
		IF v_comma_count = v_str_value_cnt
		THEN
			v_itemtype	:= v_substring;
		ELSE
			v_itemtype 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_item_ord_id := v_substring;
		END IF;
		
		SELECT get_parent_order_id(v_cur_order_id)
		 INTO v_master_parent_ord_id
		from dual;
		
		/* DBMS_OUTPUT.PUT_LINE('Before the Loop -- ' || v_prv_order_id || '***' || v_cur_order_id );
		DBMS_OUTPUT.PUT_LINE('v_cur_order_id -- ' || v_cur_order_id);
		DBMS_OUTPUT.PUT_LINE('v_PNum -- ' || v_PNum);
		DBMS_OUTPUT.PUT_LINE('v_Qty -- ' || v_Qty);
		DBMS_OUTPUT.PUT_LINE('v_Price -- ' || v_Price); */
		IF (v_prv_order_id != v_cur_order_id)
		THEN
			--
				-- DBMS_OUTPUT.PUT_LINE('Inside the Loop' );
				--Update the reduce order value in the order table
				--
			IF (v_prv_order_id != 'DUMMY')
			THEN
				SELECT SUM (c502_item_qty * c502_item_price)
				  INTO v_total_price
				  FROM t502_item_order
				 WHERE c501_order_id = v_new_order_id AND C502_VOID_FL IS NULL;

				--
				UPDATE t501_order
				   SET c501_total_cost = v_total_price
				     , c501_last_updated_by = p_userid
					 , c501_last_updated_date = CURRENT_DATE
				 WHERE c501_order_id = v_new_order_id;
			END IF;

			--
			-- Creating new order for the selected credit sales
			--
			SELECT 'GM' || s501_order.NEXTVAL || 'A'
			  INTO v_new_order_id
			  FROM DUAL;
			--
			p_order_id	:= v_new_order_id;
			--
		   BEGIN
	     --getting company id and plant id mapped from account
           SELECT c1900_company_id,gm_pkg_op_plant_rpt.get_default_plant_for_comp(gm_pkg_it_user.get_default_party_company(p_userid))
           INTO v_acc_cmp_id, v_user_plant_id  
           FROM T704_account 
           WHERE c704_account_id = p_accid 
           AND c704_void_fl IS NULL; 
           EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
            v_acc_cmp_id := NULL;
            v_user_plant_id := NULL;
           END;
			---- Added c501_customer_po_date as field for PC-3880 New field in record PO Date in GM Italy
			INSERT INTO t501_order
						(c501_order_id, c704_account_id, c703_sales_rep_id, c501_customer_po, c501_order_date
					   , c501_created_date, c501_created_by, c501_status_fl, c501_update_inv_fl, c501_order_date_time
					   , c901_order_type, c501_parent_order_id, c501_comments, c501_total_cost
					   , c503_invoice_id, c1900_company_id, c5040_plant_id, c1910_division_id, c101_dealer_id, c501_surgery_date,c501_customer_po_date)
				SELECT v_new_order_id, c704_account_id, c703_sales_rep_id, c501_customer_po, TRUNC (CURRENT_DATE), CURRENT_DATE
					 , p_userid, 3, c501_update_inv_fl, CURRENT_DATE, v_order_type, v_master_parent_ord_id, p_comments, 0
					 , v_new_invid, nvl(c1900_company_id,v_acc_cmp_id), nvl(c5040_plant_id,v_user_plant_id), c1910_division_id, c101_dealer_id, c501_surgery_date,c501_customer_po_date
				  FROM t501_order
				 WHERE c501_order_id = v_cur_order_id;
				 WITH v_txns 
					AS (
					SELECT ('^' || C901_ATTRIBUTE_TYPE || '^' || C501A_ATTRIBUTE_VALUE  ) trans_id
					FROM T501A_ORDER_ATTRIBUTE
					WHERE C501_ORDER_ID = v_cur_order_id and C501A_VOID_FL is null)
					SELECT MAX (SYS_CONNECT_BY_PATH (trans_id,'|')) INTO v_ordatt
					FROM  (SELECT trans_id, rownum curr FROM v_txns)
					CONNECT BY curr -1 = PRIOR curr START WITH curr = 1;
					
					v_ordatt := substr(v_ordatt,2) || '|';
					--allowing Chennai/Delhi to sync order attribute
                    IF (v_country_fl <> 'Y' OR v_sync_comp_id = v_company_id) -- Checked the country as not US then insert order attribute values. 
                    THEN
					gm_pkg_ac_order.gm_sav_rev_sampling(v_new_order_id,p_userid, v_ordatt ,'Y');
					--Save the child order PO and DO Details in t5003_order_revenue_sample_dtls for PMT-53710. 
					gm_pkg_ac_ar_revenue_sampling.gm_sav_child_order_revenue_dtls(v_new_order_id,v_master_parent_ord_id,p_userid);
				    END IF;

			--
			v_prv_order_id := v_cur_order_id;
		--
		END IF;
		-- To getting the construct flag 
  		BEGIN
			SELECT c502_construct_fl,c502_vat_rate,c502_cgst_rate,c502_igst_rate,c502_sgst_rate
				 INTO v_const_fl,v_vat_rate,v_cgst_rate,v_igst_rate,v_sgst_rate
				 FROM t502_item_order
				 WHERE c501_order_id = v_cur_order_id
				 AND c205_part_number_id =  v_pnum 
				 AND c502_item_order_id = v_item_ord_id 
				 AND C502_VOID_FL IS NULL; 
		EXCEPTION WHEN NO_DATA_FOUND THEN
                 v_const_fl := NULL;
                   v_vat_rate := 0;
                   v_cgst_rate := NULL;
                   v_igst_rate := NULL;
                   v_sgst_rate := NULL;
                   
                   --PC-643 :: show list price on invoices
                  
       	END;
		--
		
		SELECT s502_item_order.NEXTVAL, TRIM(GET_PART_PRICE_LIST(v_pnum,'L')) INTO v_new_item_ord_id, v_list_price FROM DUAL;

		INSERT INTO t502_item_order
					(c502_item_order_id, c501_order_id, c205_part_number_id, c502_item_qty, c502_control_number
				   , c502_item_price, c901_type, c502_unit_price, c502_construct_fl,c502_vat_rate,c502_cgst_rate,c502_igst_rate,c502_sgst_rate
				   , c502_list_price
					)
			 VALUES (v_new_item_ord_id, v_new_order_id, v_pnum, v_qty, 'NOC#'
				   ,  v_price_type * v_price	-- Adding -ive value to reduce the price
								 , v_itemtype, v_price_type * v_price, v_const_fl, v_vat_rate, v_cgst_rate, v_igst_rate, v_sgst_rate
								 , v_list_price
					);
		
	v_totamt := v_totamt + (v_qty * v_price);
	
	--to save Surgery Information's should carry from invoice to issue credit and debit (PMT-46595)
	gm_pkg_cs_credit_surg_info.gm_sav_surg_info(v_cur_order_id,v_new_order_id,p_userid);
	
	END LOOP;
	--updating the HSN code for Chennai/Delhi
	IF v_company_id = v_sync_comp_id
	THEN
		GM_PKG_AC_INVOICE_INFO.gm_upd_hsn_code(v_new_invid,'1201',p_userid);
		
	END IF;
	
	/*The Insert Script is needed to fetch the Invoice Amount for Invoice when we do issue credit or issue debit
		 * After inserting into t503a_invoice_txn amount is fetched.
	* */
	IF v_totamt <> 0 THEN -- v_item_price_value is changed to v_totamt ,in order to fix the issue in credit memo.The code is changed as part of PMT-5510.
		IF v_invoice_source = 50253 THEN	--50253 ICS	
		 	INSERT into t503a_invoice_txn
						(C503A_INVOICE_TXN_ID ,C503_INVOICE_ID,C503A_CUSTOMER_PO ,C503A_TOTAL_AMT,C503A_REF_ID,C501_ORDER_ID ) 
				VALUES 
				        (s503a_invoice_txn_id.NEXTVAL,v_new_invid,p_custpo,v_totamt*v_price_type,NULL, v_new_order_id); 
		END IF;
	END IF;
	
	--
	-- Update the total amount for the final invoice
	IF (v_prv_order_id != 'DUMMY')
	THEN
		-- DBMS_OUTPUT.PUT_LINE('Inside the Final Update **** ' || v_new_order_id );
		SELECT SUM (c502_item_qty * c502_item_price)
		  INTO v_total_price
		  FROM t502_item_order
		 WHERE c501_order_id = v_new_order_id 
		 AND c502_VOID_FL IS NULL;

		--
		UPDATE t501_order
		   SET c501_total_cost = v_total_price
		     , c501_last_updated_by = p_userid
			 , c501_last_updated_date = CURRENT_DATE
		 WHERE c501_order_id = v_new_order_id;
	END IF;

	p_newinvid	:= v_new_invid;
	
	-- PMT-43914: to get the orders comments and update to Invoice table.
	gm_pkg_ac_invoice_txn.gm_sav_invoice_comments_from_orders (v_new_invid, v_company_id, p_userid);
			 
	-- PMT-28394 (To avoid the DS order creation - Job will create the DS order)
	-- gm_save_dsorder (v_new_order_id);
	
		   --The Below mentioned code is added for PMT-5901 This is used to update invoice status as closed when the total amount is zer dollar.
	gm_pkg_ac_invoice.gm_upd_invoice_status(v_invoice_source,v_new_invid,p_userid);
	gm_pkg_ac_invoice_txn.gm_sav_invoice_amount(v_new_invid,p_userid);
	
	-- to insert t502B usage table.
	IF v_order_usage_fl = 'Y' THEN
	    gm_pkg_cs_usage_lot_number.gm_credit_invoice_update (v_new_invid, p_userid) ;
	END IF;

	--to save the corresponding attribute value
	gm_pkg_sav_snapshot_info.gm_sav_snapshot_info('ORDER',v_new_order_id,'SNAPSHOT_INFO',v_company_id,'1200',p_userid);
	
	--PC-1519: Updated VAT rate and amount not displayed on DE invoice
	IF (v_company_id = 1008 AND p_inv_type = 'DEBIT' AND v_debit_rule = 'Y') THEN	-- PC-3593 Revert original VAT rate in Germany
		gm_germany_vat_upd(v_new_invid,p_custpo,p_accid,v_company_id);
	END IF;
	
		
END gm_save_credit_sales;
/
