CREATE OR REPLACE PROCEDURE gm_germany_vat_upd (
	  p_invoice_id   IN   t501_order.c503_invoice_id%TYPE,
      p_custpo       IN   t501_order.c501_customer_po%TYPE,
      p_accid        IN   t501_order.c704_account_id%TYPE,
      p_comp_id        IN   t1900_company.c1900_company_id%TYPE
 )
 AS
 v_acc_vat_rate  NUMBER;
 v_company_vat_rate NUMBER;
 v_vat_per          T502_ITEM_ORDER.C502_VAT_RATE%TYPE;
 v_invoice_dt       DATE;
 v_con_vatrate    VARCHAR2 (20) ;
 v_noncon_vatrate VARCHAR2 (20) ;

CURSOR cur_inv_det
    IS
	
	SELECT count(1),t502.c205_part_number_id part_num
                  FROM t501_order t501,t502_item_order t502
                 WHERE t501.c501_order_id = t502.c501_order_id
                   AND c501_customer_po = p_custpo
                   AND c704_account_id = p_accid
                   AND c503_invoice_id = p_invoice_id
                   AND c501_void_fl IS NULL
                   AND c502_void_fl IS NULL
                   GROUP BY t502.c205_part_number_id;

 BEGIN
		
	FOR cur_inv IN cur_inv_det
	LOOP
		-- get vat rate by part attribute
	
			SELECT NVL(TRIM(GET_CODE_NAME_ALT (get_part_attr_value_by_comp(cur_inv.part_num,'200002', p_comp_id))),
						get_rule_value_by_company(p_comp_id,'VAT_RATE', p_comp_id))
			  INTO v_vat_per
	          FROM DUAL;
			
		IF v_vat_per IS not NULL THEN
		
			UPDATE t502_item_order 
			   SET c502_vat_rate = v_vat_per 
			 WHERE c501_order_id IN (
		 		SELECT t501.c501_order_id
                  FROM t501_order t501
                 WHERE c501_customer_po = p_custpo
                   AND c704_account_id = p_accid
                   AND c503_invoice_id = p_invoice_id
                   AND c1900_company_id = p_comp_id
                   AND c501_void_fl IS NULL
                 )
               AND c205_part_number_id = cur_inv.part_num
               AND c502_void_fl IS NULL;
		 END IF;
	END LOOP;
  END gm_germany_vat_upd;
  /