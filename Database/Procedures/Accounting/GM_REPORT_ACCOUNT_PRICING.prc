/* Formatted on 12/03/2009 15:53 (Formatter Plus v4.8.0) */
-- @"C:\database\Procedures\Accounting\gm_report_account_pricing.prc";

CREATE OR REPLACE PROCEDURE gm_report_account_pricing (
	p_accid 	  IN	   VARCHAR2
  , p_projid	  IN	   VARCHAR2
  , p_type		  IN	   VARCHAR2
  , p_recordset   OUT	   TYPES.cursor_type
)
AS
/*
	  Description			:This procedure is called for Vendor Reporting
	  Parameters			:p_column
							:p_recordset
*/
	v_str		   VARCHAR2 (2000);
BEGIN
	OPEN p_recordset
	 FOR
		 SELECT   c205_part_number_id ID, c705_unit_price price, get_partnum_desc (c205_part_number_id) pdesc
				, c705_discount_offered disc, c705_account_pricing_id apid
				, TO_CHAR (c705_created_date, 'mm/dd/yyyy') cdate, get_user_name (c705_created_by) cuser
				, TO_CHAR (c705_last_updated_date, 'mm/dd/yyyy') ldate, get_user_name (c705_last_updated_by) luser
				, c705_history_fl hisfl
			 FROM t705_account_pricing
			WHERE DECODE (p_type, 'Account', c704_account_id, c101_party_id) =
															   DECODE (p_type
																	 , 'Account', p_accid
																	 , TO_NUMBER (p_accid)
																	  )
			  AND c202_project_id = NVL (p_projid, c202_project_id)
		 ORDER BY c205_part_number_id;
END gm_report_account_pricing;
/
