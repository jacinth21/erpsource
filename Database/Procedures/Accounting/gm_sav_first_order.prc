-- @"c:\database\procedures\Accounting\gm_sav_first_order.prc"
/*******************************************************************
* Description : Procedure to save first order of the account to
* account attribute table and send email to user
********************************************************************/
CREATE OR REPLACE
PROCEDURE gm_sav_first_order
AS
    subject   VARCHAR2 (400) ;
    to_mail   VARCHAR2 (400) ;
    mail_body VARCHAR2 (4000) ;
    v_str     VARCHAR2 (4000) ;
    v_account VARCHAR2 (100) := '';
    crlf      VARCHAR2 (2)   := CHR (13) || CHR (10) ;
    CURSOR acct_cur
    IS
         SELECT t501.c501_order_id orderid, t704.c704_account_id acctid, t704.c704_account_nm acctnm
           FROM t501_Order T501, T704_Account T704
          WHERE t704.c704_account_id      = t501.c704_account_id
            AND t704.c704_account_id NOT IN
            (
                 SELECT t704.c704_account_id
                   FROM t704a_account_attribute t704a, t704_account t704
                  WHERE t704.c704_account_id        = t704a.c704_account_id
                    AND t704a.C901_ATTRIBUTE_TYPE   = 5800
                    AND t704a.C704A_ATTRIBUTE_VALUE = 'Y'
                    AND t704a.C704A_VOID_FL        IS NULL
            )
        AND TRUNC (t501.c501_order_date) = TRUNC (SYSDATE)
        AND t704.c704_active_fl          = 'Y'
        AND t704.c704_void_fl           IS NULL
        AND t501.c501_void_fl           IS NULL
   ORDER BY t704.c704_account_id, t501.c501_order_id;
BEGIN
    FOR acct_val IN acct_cur
    LOOP
        BEGIN
             UPDATE t704a_account_attribute
            SET C704A_ATTRIBUTE_VALUE   = 'Y', C704A_LAST_UPDATED_BY = 30301, C704A_LAST_UPDATED_DATE = SYSDATE
              WHERE C704_ACCOUNT_ID     = acct_val.acctid
                AND C901_ATTRIBUTE_TYPE = 5800
                AND C704A_VOID_FL      IS NULL;
            IF (SQL%ROWCOUNT            = 0) THEN
                 INSERT
                   INTO t704a_account_attribute
                    (
                        C704A_ACCOUNT_ATTRIBUTE_ID, C704A_ATTRIBUTE_VALUE, C704A_CREATED_BY
                      , C704A_CREATED_DATE, C704_ACCOUNT_ID, C901_ATTRIBUTE_TYPE
                    )
                    VALUES
                    (
                        s704a_account_attribute.nextval, 'Y', 30301
                      , sysdate, acct_val.acctid, 5800
                    ) ;
            END IF;
        END;
        -- Below begin and End statement added to 4000 plus character
        BEGIN
            IF (NVL (v_account, -9999) != acct_val.acctid) THEN
                v_str                   := v_str || 'Order ' || acct_val.orderid ||
                ' is the first order entered for account ' || acct_val.acctid || ': ' || acct_val.acctnm || crlf ||
                crlf;
            END IF;
            v_account := acct_val.acctid;
        EXCEPTION
        WHEN OTHERS THEN
            NULL;
        END;
    END LOOP;
    IF v_str      IS NOT NULL THEN
        to_mail   := get_rule_value ('FIRST_ORDER_NOTIFI', 'EMAIL') ;
        subject   := ' First Orders for account ';
        mail_body := v_str ;
        gm_com_send_email_prc (to_mail, subject, mail_body) ;
    END IF;
END gm_sav_first_order;
/ 