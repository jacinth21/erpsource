/* Formatted on 2008/09/10 15:01 (Formatter Plus v4.8.0) */
CREATE OR REPLACE PROCEDURE gm_report_account_order (
	p_frmdate	  IN	   VARCHAR2
  , p_todate	  IN	   VARCHAR2
  , p_sales_filter IN      VARCHAR2
  , p_ar_curr_symbol IN     t704_account.C901_CURRENCY %TYPE
  , p_message	  OUT	   VARCHAR2
  , p_recordset   OUT	   TYPES.cursor_type
)
AS
/*
	  Description			:This procedure is called for Project Reporting
	  Parameters			:p_column
							:p_recordset
*/
v_query	 		VARCHAR2(4000);
v_date_fmt      VARCHAR2(100); 
v_company_id    t1900_company.c1900_company_id%TYPE;   
v_ar_currency   t704_account.C901_CURRENCY %TYPE;
BEGIN
	
	SELECT get_compdtfmt_frm_cntx() 
	   INTO v_date_fmt 
	   FROM dual;
	   
	 SELECT get_compid_frm_cntx()
       INTO v_company_id 
       FROM DUAL;
       
  v_ar_currency := p_ar_curr_symbol;
  v_query := 'SELECT   t501.c704_account_id accid, t501.c501_order_id ordid
	            , get_account_name (t501.c704_account_id) NAME
				, t501.c501_order_date_time orddt
				, t501.c501_order_date_time orddtreport
				, get_user_name (t501.c501_created_by) cs
				, get_total_order_amt (t501.c501_order_id) COST
				, t501.c501_parent_order_id parentordid
				, t501.c901_order_type ordertype
				, t101.C101_PARTY_ID parentacid
                , t101.C101_PARTY_NM parentacctnm
			 FROM t501_order t501,t704_account t704,t101_party t101
			WHERE t501.c501_order_date BETWEEN TO_DATE ('''||p_frmdate||''', '''|| v_date_fmt ||''') AND TO_DATE ('''||p_todate||''', '''|| v_date_fmt ||''')
			  AND t501.c704_account_id = t704.c704_account_id
              AND t704.c101_party_id = t101.c101_party_id
              AND t704.C901_CURRENCY = '||v_ar_currency||'
              AND t704.C704_VOID_FL is NULL
              AND t101.C101_VOID_FL is NULL
			  AND t501.c501_delete_fl IS NULL
			  AND t501.c501_void_fl IS NULL
			  AND t501.C1900_COMPANY_ID = ' ||v_company_id ||'
			  AND (t501.c901_ext_country_id is NULL
              OR  t501.c901_ext_country_id  in (select country_id from v901_country_codes))  
			  AND NVL (t501.c901_order_type, -9999) NOT IN ( ''2533'', ''101260'')
			  AND NVL (t501.c901_order_type, -9999) NOT IN (
                SELECT t906.c906_rule_value
                  FROM t906_rules t906
                 WHERE t906.c906_rule_grp_id = ''ORDTYPE''
                   AND c906_rule_id = ''EXCLUDE'')
			AND '|| p_sales_filter ||'
		 ORDER BY parentacctnm, orddtreport DESC ';
	-- 	 
	OPEN p_recordset FOR v_query;
		 
EXCEPTION
	WHEN OTHERS
	THEN
		p_message	:= SQLERRM;
END gm_report_account_order;
/
