/* Formatted on 2011/05/25 12:50 (Formatter Plus v4.8.0) */
--@"C:\Database\Procedures\Accounting\gm_save_ledger_posting.prc"

CREATE OR REPLACE PROCEDURE gm_save_ledger_posting (
	p_txntype	 IN   t804_posting_ref.c901_transaction_type%TYPE
  , p_acc_date	 IN   t810_posting_txn.c810_acct_date%TYPE
  , p_partnum	 IN   t810_posting_txn.c205_part_number_id%TYPE
  , p_party_id	 IN   t810_posting_txn.c803_period_id%TYPE
  , p_txn_id	 IN   t810_posting_txn.c810_txn_id%TYPE
  , p_qty		 IN   t810_posting_txn.c810_qty%TYPE
  , p_amount	 IN   t810_posting_txn.c810_cr_amt%TYPE
  , p_updtedby	 IN   t820_costing.c820_created_by%TYPE
  , p_destination_comp_id IN t820_costing.c1900_owner_company_id%TYPE DEFAULT NULL
  , p_owner_company_id IN t820_costing.c1900_owner_company_id%TYPE DEFAULT NULL
  , p_local_company_cost  IN t810_posting_txn.c810_cr_amt%TYPE DEFAULT NULL
)
AS
--
/******************************************************************************
 * Description :  This procedure is used to post ledger posting information
 *			   for the selected part. Ledger posting expects following
 *			   type if transaction information
 *			   4810 -- 'Purchase' To post purchase ledger posting
 *			   4811 -- 'Sales' To post sales ledger posting
 *			   4812 -- 'Build Set' To post Build Build set transfer posting
 *			   'Each TXN Holds below type of posting'
 *			   **************************************
 *			   4806 -- Actual Value Posting
 *			   4807 -- COGS Value Posting
 *			   4808 -- COGS Return value posting
 *			   4809 -- Transfer ( Will hold only TXN ID and system will perform the remaining operation)
 *			   'Posting reference holds what type of table the transaction going to post'
 *			   *************************************************************************
 *			   'AP'  -- Post the record in t810_posting_txn
 *			   'INV' -- Post the record in t810_posting_txn
 *
 *******************************************************************************/
-- International costing changes (PMT-13199) - Added the owner and local company cost changes.
	p_account_type t804_posting_ref.c901_account_type_dr%TYPE;
	p_dr_amt	   t810_posting_txn.c810_dr_amt%TYPE;
	p_cr_amt	   t810_posting_txn.c810_cr_amt%TYPE;
	p_posting_type t804_posting_ref.c901_posting_type%TYPE;
	v_table_ref    t805_posting_table_ref.c805_table_identifier%TYPE;
	v_amount	   t810_posting_txn.c810_cr_amt%TYPE;
	v_costing_id   t810_posting_txn.c820_costing_id%TYPE;
	v_approved_qty t810_posting_txn.c810_qty%TYPE;
	--
	v_cosg_amount  t810_posting_txn.c810_cr_amt%TYPE;
	v_cosg_qty	   t810_posting_txn.c810_qty%TYPE;
	v_cosg_cost_id t810_posting_txn.c820_costing_id%TYPE;
	v_from_cosg_cost_id t810_posting_txn.c820_costing_id%TYPE;
	-- Costing Type
	v_from_costing_type t804_posting_ref.c901_from_costing_type%TYPE;
	v_to_costing_type t804_posting_ref.c901_to_costing_type%TYPE;
	--
	-- Variable used to hold COGS Logic
	p_cogs_qty	   t810_posting_txn.c810_qty%TYPE;
	p_type_flag    CHAR (1);
	p_cogs_flag    CHAR (1);
	-- Variable to check if the COGS Flag is requested
	v_txn_post_type CHAR (1);	-- Will hold 'A' for actual and 'C' for COGS
	p_cr_dr_post_type CHAR (1);
	-- hold 'D' for debit posting and 'C' for Credit Posting
	v_loop_count   NUMBER (3);
	v_division_id t810_posting_txn.c901_company_id%TYPE;
	v_country_id t810_posting_txn.c901_country_id%TYPE;
	v_company_id_ctx  t1900_company.c1900_company_id%TYPE;
	v_company_id  t1900_company.c1900_company_id%TYPE;
	v_posting_req  t1900_company.c901_posting_reqd%TYPE;
	--
	v_local_company_dr_amt	   t810_posting_txn.c810_dr_amt%TYPE;
	v_local_company_cr_amt	   t810_posting_txn.c810_cr_amt%TYPE;
	v_local_cost		t810_posting_txn.c810_cr_amt%TYPE;
	v_owner_company_id  t820_costing.c1900_owner_company_id%TYPE;
	v_context_plant_id T5040_PLANT_MASTER.c5040_plant_id%TYPE;
	v_txn_company_id t810_posting_txn.c1900_company_id%TYPE;
	v_destination_company_id t810_posting_txn.c1900_company_id%TYPE;
	v_ict_cnt NUMBER;
	-- Based on Part number - family to avoid the posting (PMT-23258)
	v_skip_posting_fl t906_rules.c906_rule_value%TYPE;
	v_part_prodcut_family t205_part_number.c205_product_family%TYPE;
	
	-- Below cursor is going to fetch 2 or more value for the selected
	-- transaction
	CURSOR txtp_cursor
	IS
		SELECT	 c901_account_type_dr account_dr, NULL account_cr
			   , get_posting_table_ref (c901_account_type_dr) table_ref_nm, c901_posting_type p_type
			   , c901_from_costing_type f_type, c901_to_costing_type t_type, 1 seq_id, c804_posting_ref_id ref_id
			FROM t804_posting_ref
		   WHERE c901_transaction_type = p_txntype AND c901_account_type_dr IS NOT NULL
		UNION
		SELECT	 NULL account_dr, c901_account_type_cr account_cr
			   , get_posting_table_ref (c901_account_type_cr) table_ref_nm, c901_posting_type p_type
			   , c901_from_costing_type f_type, c901_to_costing_type t_type, 2 seq_id, c804_posting_ref_id ref_id
			FROM t804_posting_ref
		   WHERE c901_transaction_type = p_txntype AND c901_account_type_cr IS NOT NULL
		ORDER BY ref_id, seq_id;
--
--
BEGIN
	 -- PMT-23258 (Based on part family to avoid the posting changes)
	 -- To get the product family and skip the posting for non-spine parts.  
	BEGIN
	 -- Based on Product family to skip the posting (Construct Code, Non Stock, Non Inventory Items and Usage Code)
	 	
     SELECT NVL (get_rule_value (t205.c205_product_family, 'SKIP_INV_POSTING'), 'N'), t205.c205_product_family
       INTO v_skip_posting_fl, v_part_prodcut_family
       FROM t205_part_number t205
      WHERE t205.c205_part_number_id = p_partnum;
      
	EXCEPTION
	WHEN NO_DATA_FOUND THEN
	    v_skip_posting_fl := 'N';
	END;
	--
	IF (v_skip_posting_fl = 'Y') THEN
	    -- to store the skipped posting details (t811)
	    SELECT get_compid_frm_cntx()
      	INTO v_company_id_ctx
      	FROM dual;
      	--
	    gm_pkg_cm_posting_txn.gm_sav_skip_posting_dtls (p_txn_id, p_partnum, p_qty, p_txntype, v_part_prodcut_family, v_company_id_ctx, p_updtedby) ;
	    --
	    RETURN;
	END IF;
	
	 -- to store the company and plant information to T830 table
	 gm_pkg_cm_posting_txn.gm_sav_trans_comp_plant_id (p_txn_id, p_updtedby);
	 --
	 SELECT get_compid_frm_cntx() , get_plantid_frm_cntx()
      	INTO v_company_id_ctx  , v_context_plant_id
      FROM dual;
    -- to get the company Id based on plant id
     v_company_id := get_plant_parent_comp_id (v_context_plant_id);
    
      --Check if the company requires posting YES or NO
     SELECT C901_POSTING_REQD
      	INTO v_posting_req
	 FROM T1900_COMPANY
	 WHERE C1900_COMPANY_ID = v_company_id
	 AND C1900_VOID_FL IS NULL;
	 
      --105305	No if a country doesn't require posting then return
	 IF(v_posting_req = 105305)
	 THEN
	 	RETURN;
	 END IF;
	
	
	p_cogs_qty	:= p_qty;
	p_type_flag := 'F';
	p_cogs_flag := 'N';
	v_loop_count := 1;
	--
	
	-- If context is NUll then send email
	IF v_company_id IS NULL
	THEN
		gm_posting_comp_error_mail(p_partnum,p_qty,p_txn_id,NULL,NULL,p_txntype,p_updtedby,v_division_id,v_country_id,NULL, v_company_id, v_company_id_ctx, v_context_plant_id);
	END IF;
	
	IF (p_txntype IS NULL)
	THEN
		-- PC-991: To pass the local cost information and it used to repost the local cost.
		--102940 Cositng Error 
		gm_save_costing_error_log (p_partnum, p_qty, p_txn_id, NULL, NULL, p_txntype, p_party_id, p_updtedby,v_division_id,v_country_id,102940,v_company_id,v_context_plant_id, v_company_id_ctx, p_destination_comp_id, p_owner_company_id
				,p_local_company_cost );
		 --gm_procedure_log('GM_SAVE_LEDGER_POSTING NULL TRANSACTION IS' ,
		 -- 	 'TRANSACTION ID NOT FOUND FOR PART ' || p_partnum || ' - REQUESTED BY ' || p_txn_id  || ' - REQUESTED QTY ' || p_qty);
	--
	END IF;
	
	<<cogs_loop>>
	WHILE (p_cogs_qty > 0)
	LOOP

		--
		-- To get the list of record to be posted
		<<cursor_loop>>
		FOR txtp_val IN txtp_cursor
		LOOP
			--
			   --
			p_posting_type := txtp_val.p_type;
			v_table_ref := txtp_val.table_ref_nm;
			v_from_costing_type := txtp_val.f_type;
			v_to_costing_type := txtp_val.t_type;
			--
			-- GET THE POSTING TYPE COGS OR ACTUAL VALUE
		--	DBMS_OUTPUT.put_line ('Inside for loop of saveledger posting *** ');
			v_txn_post_type := get_posting_type (p_posting_type);
			
		--	DBMS_OUTPUT.put_line (v_txn_post_type||'   '||p_cogs_flag);
			
			IF (v_txn_post_type = 'C' AND p_cogs_flag = 'N')
			THEN
				--
				gm_fetch_part_cogs_value (p_txn_id
										, p_partnum
										, p_cogs_qty
										, p_posting_type
										, v_from_costing_type
										, v_to_costing_type
										, p_amount
										, p_updtedby	
										, p_txntype
										, p_party_id
										, v_division_id
										, v_country_id
										, p_owner_company_id
										, p_local_company_cost
										, v_company_id
										, v_context_plant_id
										, v_company_id_ctx
										, p_destination_comp_id
										, v_cosg_amount
										, v_cosg_qty
										, v_owner_company_id
										, v_local_cost
										, v_cosg_cost_id
										 );
				p_type_flag := 'T';
				p_cogs_flag := 'Y';
				--
			--	DBMS_OUTPUT.put_line ('Amount  ' || v_cosg_amount);
			--	DBMS_OUTPUT.put_line ('QTY Approved  ' || v_cosg_qty);
			--	DBMS_OUTPUT.put_line ('COSG ID	' || v_cosg_cost_id);
			--
			END IF;
			-- DBMS_OUTPUT.put_line (v_txn_post_type||'amt'||v_cosg_amount);
			--
			-- *** COGS Value Posting ***
			
			IF (v_txn_post_type = 'C')
			THEN
				--
				v_amount	:= v_cosg_amount;
				v_approved_qty := v_cosg_qty;
				v_costing_id := v_cosg_cost_id;
			--
			-- *** Actual Value Posting ***
			ELSE
			--
				v_amount	:= p_amount;
				v_approved_qty := p_qty;
				v_costing_id := NULL;
				--International posting changes (Actual value to COGS to set the owner company information)
				v_local_cost := p_local_company_cost;
				v_owner_company_id := p_owner_company_id;
			--
			END IF;
			
			--
			--
			-- process data record
			-- If the row is mapped to COSG then get COSTING Amount
			IF (txtp_val.account_dr IS NOT NULL)
			THEN
				--
				p_account_type := txtp_val.account_dr;
				p_dr_amt	:= v_amount;
				p_cr_amt	:= 0;
				v_local_company_dr_amt := v_local_cost;
				v_local_company_cr_amt := 0;
				p_cr_dr_post_type := 'D';
			--
			ELSE
				--
				p_account_type := txtp_val.account_cr;
				p_dr_amt	:= 0;
				v_local_company_dr_amt := 0;
				p_cr_amt	:= v_amount;
				v_local_company_cr_amt := v_local_cost;
				p_cr_dr_post_type := 'C';
			--
			END IF;

			--
			/* DBMS_OUTPUT.put_line ('******************************');
			DBMS_OUTPUT.put_line ('DR AMT  - ' || p_dr_amt);
			DBMS_OUTPUT.put_line ('CR AMT  - ' || p_cr_amt);
			DBMS_OUTPUT.put_line ('COSG ID - ' || v_costing_id);
			DBMS_OUTPUT.put_line ('APPR QTY    - ' || v_approved_qty);
			DBMS_OUTPUT.put_line ('TABLE REF - ' || v_table_ref);
			DBMS_OUTPUT.put_line ('POSTING TYPE - ' || p_posting_type);
			DBMS_OUTPUT.put_line ('******************************');*/
			--
			-- procedure should execute the loop more than once
			-- if COGS valyes are requested
			
			IF ((v_loop_count = 1) OR (v_txn_post_type = 'C'))
			THEN
				--
				gm_save_posting (v_table_ref
							   , p_txntype
							   , p_account_type
							   , p_acc_date
							   , p_party_id
							   , p_txn_id
							   , p_partnum
							   , v_costing_id
							   , v_approved_qty
							   , p_dr_amt
							   , p_cr_amt
							   , p_updtedby
							   , p_posting_type
							   , p_cr_dr_post_type
							   , v_division_id
							   , v_country_id
							   , v_company_id
							   , v_context_plant_id
							   , v_owner_company_id
							   , v_company_id_ctx
							   , p_destination_comp_id
							   , v_local_company_dr_amt
							   , v_local_company_cr_amt
								);
			--
			END IF;
		--
		END LOOP cursor_loop;	-- for loop ends here

		--
		-- To validate if the transaction is mapped with COGS Account
		--	If mapped to flag F means non of the txan is mapped to COGS
		IF (p_type_flag = 'F')
		THEN
			--
			p_cogs_qty	:= 0;
		--
		ELSE
			--
			p_cogs_qty	:= p_cogs_qty - v_cosg_qty;
		--
		END IF;

		--
		-- Reset all the values
		p_cogs_flag := 'N';
		v_loop_count := v_loop_count + 1;
	--
	END LOOP cogs_loop;
						  -- while loop ends here
--
END gm_save_ledger_posting;
/
