/* Formatted on 2009/01/23 18:22 (Formatter Plus v4.8.0) */
CREATE OR REPLACE PROCEDURE gm_update_qty_manual (
	p_manual_id   t216_part_qty_adj_details.c215_part_qty_adj_id%TYPE
)
AS
--
	CURSOR qb_cursor
	IS
		SELECT c215_part_qty_adj_id p_adj_id, c205_part_number_id p_num, c216_qty_to_be_adjusted q_adj
			 , c216_created_by created_by
		  FROM t216_part_qty_adj_details
		 WHERE c216_qty_to_be_adjusted <> 0 AND c215_part_qty_adj_id = p_manual_id;

--
	v_part_number  t214_transactions.c205_part_number_id%TYPE;
	v_adj_value    t214_transactions.c214_new_qty%TYPE;
	v_temp		   VARCHAR2 (20);
--
BEGIN
--
	FOR qb_val IN qb_cursor
	LOOP
		--
		v_part_number := qb_val.p_num;
		v_adj_value := qb_val.q_adj;

		--
		BEGIN
			--
			v_temp		:= 'No Part Exception ';
			DBMS_OUTPUT.put_line (v_part_number || v_adj_value);

			--
			gm_cm_sav_partqty (v_part_number,
                               v_adj_value,
                               p_manual_id,
                               qb_val.created_by,
                               90800, -- FG Qty
                               4303,  -- Manual
                               4316  -- Manual
                               );
--
/* EXCEPTION WHEN OTHERS THEN
 --
  gm_procedure_log(v_temp , ' Part Number ' || v_part_number || SQLERRM);
 --*/
		END;
	END LOOP;
--
EXCEPTION
	WHEN OTHERS
	THEN
--
		DBMS_OUTPUT.put_line (SQLERRM);
--
END gm_update_qty_manual;
/
