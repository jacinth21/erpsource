
CREATE OR REPLACE PROCEDURE gm_sav_rollfwd_op_bal_arch (
	p_year    VARCHAR2
)
/******************************************************************************
 * Description : This Procedure is used to archive posting.
 *p_year :Contains the year for which posting has to be archived.
 *******************************************************************************/
AS
v_frmdt DATE;
v_todt	DATE;
BEGIN
	SELECT TO_DATE('01/01/'||p_year,'mm/dd/yyyy') ,TO_DATE('12/31/'||p_year,'mm/dd/yyyy')
	INTO v_frmdt,v_todt
	FROM dual;
		 INSERT INTO T810_ROLLFORWARD(
        C810_ROLLFORWARD_ID
        ,C801_ACCOUNT_ELEMENT_ID
        ,C810_AMT
        ,C810_TXN_DATE
        ,C901_STATUS
        ,C810_VOID_FL
        ,C901_COUNTRY_ID
        ,C901_COMPANY_ID
        ,C1900_COMPANY_ID
        ,C810_CREATED_BY
        ,C810_CREATED_DATE
		,C1910_DIVISION_ID
		,C1910_DIVISION_NAME)
     SELECT S810_ROLLFORWARD.nextval
     ,temp.*
     FROM ( SELECT t810.C801_ACCOUNT_ELEMENT_ID
			 ,SUM( NVL(t810.C810_QTY,0) * NVL(t810.C810_DR_AMT,0) ) -SUM( NVL(t810.C810_QTY,0) * NVL(t810.C810_CR_AMT, 0)  ) AMT
                --,TO_DATE('12/31/2012','mm/dd/yyyy') 
                --,MAX(t810.C810_TXN_DATE )
				,TO_DATE('12/31/'||EXTRACT(year FROM t810.C810_TXN_DATE),'mm/dd/yyyy')
                ,105901 approved
                ,NULL
                , t810.c901_country_id COUNTRYID
                ,t810.c901_company_id COMPID
                ,t810.C1900_COMPANY_ID comp
                ,303497
                ,TRUNC(SYSDATE),
				t810.C1910_DIVISION_ID 
				, t810.C1910_DIVISION_NAME
				from t810_posting_txn t810
				WHERE  c810_delete_fl     <> 'Y'
                --t810.C801_ACCOUNT_ELEMENT_ID IN (2,3)
				AND t810.C810_TXN_DATE >=v_frmdt
				AND t810.C810_TXN_DATE <=v_todt
               	GROUP BY t810.C801_ACCOUNT_ELEMENT_ID
                , t810.c901_company_id
                , t810.c901_country_id
                ,t810.C1900_COMPANY_ID
				, t810.C1910_DIVISION_ID 
				, t810.C1910_DIVISION_NAME
                ,EXTRACT(year FROM t810.C810_TXN_DATE) 
                ORDER by EXTRACT(year FROM t810.C810_TXN_DATE)asc) temp;
END gm_sav_rollfwd_op_bal_arch;
/
