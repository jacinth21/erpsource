
CREATE OR REPLACE PROCEDURE gm_sav_costing_data_archive
/******************************************************************************
 * Description : This Procedure is used to archive closed costing layers.
  *******************************************************************************/
AS
v_user_id NUMBER;
BEGIN
-- TSK-18054 : Costing archived (2020)
-- to ge the user id
	SELECT it_pkg_cm_user.get_valid_user ()
	INTO v_user_id
	FROM DUAL;
	
--4802 Closed Status
	INSERT INTO T820_COSTING_ARCHIVE select * from T820_COSTING where C901_STATUS = 4802;
	DELETE FROM T820_COSTING where C901_STATUS = 4802;
	 UPDATE T906_RULES
			SET C906_RULE_VALUE =to_char(CURRENT_DATE,'yyyy-mm-dd')
			,C906_LAST_UPDATED_BY = v_user_id
			,C906_LAST_UPDATED_DATE= CURRENT_DATE
		WHERE C906_RULE_ID ='COST_ARCHIVE_DATE'
		AND C906_RULE_GRP_ID ='ARCHIVE';
END gm_sav_costing_data_archive;
/
