
CREATE OR REPLACE PROCEDURE gm_sav_posting_data_archive (
	p_year    VARCHAR2
)
/******************************************************************************
 * Description : This Procedure is used to archive posting.
 *p_year :Contains the year for which posting has to be archived.
 *******************************************************************************/
AS
v_frmdt DATE;
v_todt	DATE;
BEGIN
	SELECT TO_DATE('01/01/'||p_year,'mm/dd/yyyy') ,TO_DATE('12/31/'||p_year,'mm/dd/yyyy')
	INTO v_frmdt,v_todt
	FROM dual;
		
	 INSERT INTO T810_POSTING_TXN_ARCHIVE (C810_ACCOUNT_TXN_ID 
		  , C901_ACCOUNT_TXN_TYPE           
		  , C810_TXN_DATE 
          , C801_ACCOUNT_ELEMENT_ID                 
		  , C810_DESC        
		  , C810_ACCT_DATE 
          , C810_DR_AMT      
		  , C810_CR_AMT          
		  , C810_QTY 
          , C803_PERIOD_ID                 
		  , C205_PART_NUMBER_ID
		  , C810_PARTY_ID    
		  , C810_DELETE_FL      
		  ,C810_CREATED_BY 
		  , C810_CREATED_DATE               
		  ,C810_LAST_UPDATED_BY     
		  , C810_LAST_UPDATED_DATE          
		  ,C820_COSTING_ID                 
		  , C810_TXN_ID      
		  ,C810_CR_AMT_TEMP       
		  , C810_DR_AMT_TEMP 
		  ,C901_COUNTRY_ID                
		  , C901_COMPANY_ID 
		  ,C1900_COMPANY_ID
		  ,C1910_DIVISION_ID
		  ,C1910_DIVISION_NAME		  )
    SELECT 
            S810_POSTING_TXN.nextval  
		  , C901_ACCOUNT_TXN_TYPE           
		  , C810_TXN_DATE 
          , C801_ACCOUNT_ELEMENT_ID                 
		  , C810_DESC        
		  , C810_ACCT_DATE 
          , C810_DR_AMT      
		  , C810_CR_AMT          
		  , C810_QTY 
          , C803_PERIOD_ID                 
		  , C205_PART_NUMBER_ID
		  , C810_PARTY_ID    
		  , C810_DELETE_FL      
		  ,C810_CREATED_BY 
		  , C810_CREATED_DATE               
		  --,C810_LAST_UPDATED_BY     
		 --,C810_LAST_UPDATED_DATE 
          ,303497
          ,SYSDATE
		  ,C820_COSTING_ID                 
		  , C810_TXN_ID      
		 --,C810_CR_AMT_TEMP       
         --,C810_DR_AMT_TEMP 
          ,NULL
          ,NULL
		  ,C901_COUNTRY_ID                
		  , C901_COMPANY_ID 
		  ,C1900_COMPANY_ID
		  ,C1910_DIVISION_ID
		  ,C1910_DIVISION_NAME
          FROM T810_POSTING_TXN
        WHERE C810_TXN_DATE >= v_frmdt
        AND C810_TXN_DATE <= v_todt;
		
        --
        DELETE FROM T810_POSTING_TXN WHERE C810_TXN_DATE >= v_frmdt AND C810_TXN_DATE <= v_todt;
        
		 UPDATE T906_RULES
			SET C906_RULE_VALUE = to_char(v_todt,'yyyy-mm-dd') 
			,C906_LAST_UPDATED_BY =303497
			,C906_LAST_UPDATED_DATE=SYSDATE
		WHERE C906_RULE_ID ='POST_ARCHIVE_DATE'
		AND C906_RULE_GRP_ID ='ARCHIVE';
END gm_sav_posting_data_archive;
/
