/* Formatted on 2009/01/28 16:02 (Formatter Plus v4.8.0) */


CREATE OR REPLACE PROCEDURE gm_update_dhr_payment (
	p_inputstr	 IN 	  VARCHAR2
  , p_userid	 IN 	  t408_dhr.c408_created_by%TYPE
  , p_message	 OUT	  VARCHAR2
)
AS
	v_strlen	   NUMBER := NVL (LENGTH (p_inputstr), 0);
	v_string	   VARCHAR2 (30000) := p_inputstr;
	v_dhr		   VARCHAR2 (40);
	v_flag		   CHAR (1);
	v_substring    VARCHAR2 (1000);
	v_msg		   VARCHAR2 (1000);
--
BEGIN
	IF v_strlen > 0
	THEN
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			--
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			--
			v_dhr		:= NULL;
			v_flag		:= NULL;
			--
			v_dhr		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_flag		:= v_substring;

					--
			--		gm_procedure_log (v_dhr, v_flag);
			UPDATE t408_dhr
			   SET c408_payment_fl = DECODE (v_flag, 'Y', 'Y', NULL)
				 , c408_last_updated_by = p_userid
				 , c408_last_updated_date = SYSDATE
			 WHERE c408_dhr_id = v_dhr;
		--
		END LOOP;
	END IF;

	--
	COMMIT;
	p_message	:= 'Success';
--
EXCEPTION
	WHEN OTHERS
	THEN
		--gm_procedure_log ('Error', SQLERRM);
		ROLLBACK;
		p_message	:= SQLERRM;
END gm_update_dhr_payment;
/
