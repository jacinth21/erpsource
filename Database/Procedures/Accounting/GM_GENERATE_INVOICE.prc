/* Formatted on 2011/01/10 18:04 (Formatter Plus v4.8.0)  */

/*
 * For BBA company the Invoice generation is based on the Ship Date, 
Ship To and the Customer PO combination,
hence for the same PO system generates multiple Invoices,
so this condition will be applicable for BBA company.*/

CREATE OR REPLACE PROCEDURE gm_generate_invoice (
	p_custpo	   IN		t503_invoice.c503_customer_po%TYPE
  , p_accid 	   IN		t503_invoice.c704_account_id%TYPE
  , p_payterms	   IN		t503_invoice.c503_terms%TYPE
  , p_invdate	   IN		DATE
  , p_userid	   IN		t503_invoice.c503_created_by%TYPE
  , p_action	   IN		VARCHAR2
  , p_invtype	   IN		t503_invoice.c901_invoice_type%TYPE
  , p_inv_source   IN		t503_invoice.c901_invoice_source%TYPE
  , p_inv_id	   OUT		CLOB
  , p_dealer_id    IN		t503_invoice.c101_dealer_id%TYPE DEFAULT NULL
  , p_batch_type   IN		t9600_batch.c901_batch_type%TYPE DEFAULT NULL
  -- As BBA will have multiple invoice ID for the Same PO, we need to change the column type to CLOB.
  -- This was the data type in BBA Prod as well.
) 
AS
	
	v_invoice_id   VARCHAR2 (20);
	v_cnt		   NUMBER;
	v_vat_fl	   VARCHAR2(5);
	v_con_vat_fl	VARCHAR2(5);	
	v_company_id        t1900_company.c1900_company_id%TYPE;
	v_division_id   t1910_division.c1910_division_id%TYPE;
	v_rep_id		t703_sales_rep.c703_sales_rep_id%TYPE;

--
BEGIN
	-- OUS Distributor - don't have the account id  
	IF p_accid IS NOT NULL 
	THEN
		SELECT c1900_company_id 
	      INTO v_company_id  
	      FROM t704_account
	     WHERE c704_account_id = p_accid;
	ELSE
		SELECT get_compid_frm_cntx() 
	      INTO v_company_id  
	      FROM dual;
	END IF;

	--Generating invoice(Default,credit and Debit) for Dealer	
	IF p_inv_source = '26240213'
	THEN
		gm_pkg_ac_invoice_txn.gm_sav_single_dealer_invoice(p_custpo,p_accid,p_dealer_id,p_payterms,p_invdate,p_userid,p_action,p_invtype,p_inv_source,'','',p_inv_id);	
	ELSE
		IF(v_company_id <> 1001 OR p_action  IN ('CREDITSALES','CREDITORDER'))THEN
			gm_pkg_ac_invoice_txn.gm_sav_single_invoice(p_custpo,p_accid,p_payterms,p_invdate,p_userid,p_action,p_invtype,p_inv_source,'','',p_inv_id);
		ELSE 
			gm_pkg_ac_invoice_txn.gm_sav_multiple_invoice(p_custpo,p_accid,p_payterms,p_invdate,p_userid,p_action,p_invtype,p_inv_source,p_inv_id);
		END IF;
	END IF;
	gm_pkg_ac_invoice_txn.gm_sav_invoice_amount(p_inv_id,p_userid);--PMT-25905:=Save Invoice and Payment Amount
		
END gm_generate_invoice;
/
