CREATE OR REPLACE PROCEDURE GM_REPORT_DIST_ACCOUNT
(
	   p_message   OUT VARCHAR2,
	   p_recordset OUT Types.cursor_type)
AS
/*
      Description           :This procedure is called for Project Reporting
      Parameters            :p_column
                            :p_recordset
*/
v_str VARCHAR2(2000);
BEGIN
   	OPEN p_recordset FOR
		SELECT
				GET_DISTRIBUTOR_ID(C703_SALES_REP_ID) DID, C703_SALES_REP_ID REPID,
				GET_DISTRIBUTOR_NAME(GET_DISTRIBUTOR_ID(C703_SALES_REP_ID)) DNAME,
				C704_ACCOUNT_ID ID,	C704_ACCOUNT_NM NAME,
				GET_REP_NAME(C703_SALES_REP_ID) REPNAME,
				C704_BILL_CITY || ', ' || GET_CODE_NAME_ALT(C704_BILL_STATE) LOC,
				GET_ACCOUNT_DAY_SALES(C704_ACCOUNT_ID) DAYSALES,
				GET_ACCOUNT_MONTH_SALES(C704_ACCOUNT_ID) MONSALES,
				GET_ACCOUNT_LAST_MONTH_SALES(C704_ACCOUNT_ID) LASTMONSALES
		FROM
			 	T704_ACCOUNT
		WHERE
			  	C703_SALES_REP_ID <> 00
		AND
				C704_ACCOUNT_ID <> '00' AND C704_ACCOUNT_ID <> '01'
		AND 	(c901_ext_country_id is NULL
		OR      c901_ext_country_id  in (select country_id from v901_country_codes)) 
		ORDER BY
			  	DNAME, C704_ACCOUNT_NM;
	 EXCEPTION
	 WHEN OTHERS THEN
	          p_message := '1000';
END GM_REPORT_DIST_ACCOUNT;
/

