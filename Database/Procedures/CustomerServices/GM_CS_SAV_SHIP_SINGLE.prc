CREATE OR REPLACE PROCEDURE GM_CS_SAV_SHIP_SINGLE
(
 p_shipid IN t907_shipping_info.c907_shipping_id%TYPE
 ,p_mode  IN t907_shipping_info.c901_delivery_mode%TYPE
 ,p_carr  IN t907_shipping_info.c901_delivery_carrier%TYPE
 ,p_track IN t907_shipping_info.c907_tracking_number%TYPE
 ,p_UserId IN t907_shipping_info.c907_last_updated_by%TYPE
)
/***************************************************************************************
 * Description           : This procedure saves shipping info
 *
 ****************************************************************************************/
AS
--
BEGIN
--
 UPDATE t907_shipping_info
 SET
  c901_delivery_mode  = p_mode
  ,c901_delivery_carrier = p_carr
  ,c907_tracking_number = p_track
  ,c907_last_updated_by = p_UserId
  ,c907_last_updated_date = SYSDATE
 WHERE
  c907_shipping_id  = p_shipid;
--
END GM_CS_SAV_SHIP_SINGLE;
/

