/* Formatted on 05/24/2010 18:20 (Formatter Plus v4.8.0) */
--@"C:\database\Procedures\CustomerServices\GM_CS_SAV_LOANER_RETURN.prc";

CREATE OR REPLACE PROCEDURE gm_cs_sav_loaner_return (
	p_conid 		IN		 t504_consignment.c504_consignment_id%TYPE
  , p_expretdt		IN		 VARCHAR2
  , p_inputstr		IN		 VARCHAR2
  , p_retstr		IN		 VARCHAR2
  , p_quarstr		IN		 VARCHAR2
  , p_repstr		IN		 VARCHAR2
  , p_scrapstr      IN       VARCHAR2
  , p_bckstr		IN 		 VARCHAR2  
  , p_bulkstr		IN 		 VARCHAR2
  , p_ihrepstr		IN		 VARCHAR2
  , p_ihmovestr		IN		 VARCHAR2
  , p_type			IN		 VARCHAR2
  , p_userid		IN		 t504_consignment.c504_last_updated_by%TYPE
  , p_inh_id		OUT 	 VARCHAR2
  , p_replenishid	OUT 	 VARCHAR2
  , p_extReason     IN       NUMBER DEFAULT NULL
)
AS
/***************************************************************************************
 * Description			 : This procedure is used to Return InHouse Sets ship info
 *
 VALUES 	4127 = Product Loaner
	  4119 = InHouse Loaner
	  50151 = InHouse to Scrap
	  50156 = Loaner to Scrap
	  50320 = Loaner Extension
	  50183 = Move from Shelf to Loaner Replenish
	  50154 = Move from shelf to Loaner Exension w/Replenish
	  50128 = Loaner Exension with Replenish

	  for "Loaner extension", this procedure will handle the case p_type = 50320
 ****************************************************************************************/
	v_scrapid	   VARCHAR2 (20);
	v_returnid	   VARCHAR2 (20);
	v_quarid	   VARCHAR2 (20);
	v_repid 	   VARCHAR2 (20);
	v_ihmoveid	   VARCHAR2 (20);	
	v_transrefid   VARCHAR2 (20);
	v_flag		   VARCHAR2 (20);
	p_message	   VARCHAR2 (100);
	v_chk		   CHAR (1);
	v_trans 	   VARCHAR2 (20);
	v_type		   NUMBER;
	v_reason	   NUMBER;
	v_comments	   VARCHAR2 (100);
	v_setid 	   t207_set_master.c207_set_id%TYPE;
	v_loanid	   VARCHAR2 (20);
	v_ret_date	   DATE;
	v_statusfl t504a_consignment_loaner.C504A_STATUS_FL%type;
	v_substring 	VARCHAR2(4000);
	v_string 		VARCHAR2(4000);
	v_backorder_count number;
	v_err_cnt NUMBER;
	v_status_fl 	NUMBER;
	v_shipto		t504a_loaner_transaction.C901_CONSIGNED_TO%TYPE := NULL;
	v_shiptoid		t504a_loaner_transaction.C504A_CONSIGNED_TO_ID%TYPE := NULL;
	v_add_month    NUMBER;
	v_day		   VARCHAR2 (10);
	v_final_dd	   VARCHAR2 (2);
	v_ih_trans_type t412_inhouse_transactions.c412_type%TYPE;
	v_tagid		VARCHAR2 (20);
	v_trans_comp_id  VARCHAR2(20);
	v_trnas_plant_id VARCHAR2(20);
	v_trans_masret_cmp_id VARCHAR2(20);
	v_412_type T412_INHOUSE_TRANSACTIONS.C412_TYPE%type;
	v_txn_id t504_consignment.c504_consignment_id%TYPE;
    v_skip_noc_fl VARCHAR2(20);
    v_reqyest_company_id VARCHAR2(20);
    v_status_ih_fl  t412_inhouse_transactions.c412_status_fl%type;
BEGIN
--
	v_ih_trans_type := 100062;  -- Loaner Back Order
	-- 
	IF p_type = '4127' OR p_type = '50320'
	THEN
		v_trans 	:= 'LoanerAddRem';
	ELSIF p_type = '4119'
	THEN
		v_trans 	:= 'InHouseAddRem';
		v_ih_trans_type := 1006572; -- IH Loaner Set Back Order
	ELSIF p_type = '50320'
	THEN
		v_trans 	:= 'LoanerExtension';
	END IF;

	--
	v_comments	:= 'Reprocess Transaction from Loaner ID: ' || p_conid;

	--
		-- Getting the Loaner Transaction PK to update the Txn_Ref_Id.
		-- This is because its needed for pulling up this missing transaction (50159) during reconciliation
		--changed the query as we dont need to fetch the return date and the max trans id will be fetched via the function call alone
		begin
			select gm_pkg_op_loaner.get_loaner_trans_id (p_conid)  
				into v_transrefid from dual;
		exception when no_data_found then
			v_transrefid :=0;
		end;
		
   BEGIN
		SELECT t504a.c1900_company_id, t504a.c5040_plant_id, t504.c1900_company_id,t526.c1900_company_id
		  INTO v_trans_comp_id , v_trnas_plant_id , v_trans_masret_cmp_id, v_reqyest_company_id
		  FROM t504a_loaner_transaction t504a, t504_consignment t504, t526_product_request_detail t526
		 WHERE t504a.c504a_loaner_transaction_id = v_transrefid
		 AND t504a.c504_consignment_id = t504.c504_consignment_id
		 AND t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id
		 AND t504.c504_void_fl IS NULL
		   AND t504a.c504a_void_fl IS NULL; 
  EXCEPTION WHEN NO_DATA_FOUND THEN

		SELECT get_compid_frm_cntx(), get_plantid_frm_cntx() 
          INTO v_trans_comp_id , v_trnas_plant_id 
          FROM DUAL;
          v_trans_masret_cmp_id := v_trans_comp_id;
  END;
  
		
	SELECT c504a_status_fl
     INTO v_statusfl
     FROM t504a_consignment_loaner
    WHERE c504_consignment_id = p_conid AND c504a_void_fl IS NULL;

   IF (   v_statusfl != '0'
       and v_statusfl != '5'
       and v_statusfl != '10'
       and v_statusfl != '20'
       and v_statusfl != '25'
       and v_statusfl != '58'
       and v_statusfl != '24'
      )
   THEN
      raise_application_error (-20790, '');
   END IF;

--
-- For LOANER SALES Transaction
	IF p_inputstr IS NOT NULL
	THEN
		--
		IF p_type = '4127' OR p_type = '50320'
		THEN
			v_type		:= 50159;
			-- To identify the parts that were recorded as missing. Doesnt Post anywhere
			v_reason	:= 50127;
		ELSE
			v_type		:= 1006571;
			v_reason	:= 50107;
		END IF;
		
		
		v_scrapid	:= get_next_consign_id (v_type);
		p_inh_id	:= p_inh_id || v_scrapid || ',';


		-- Fetching the Loan To and Loan To Name and save it with the ISMP transaction 
		IF v_type = 1006571
		THEN
			BEGIN
				SELECT T7103.c901_loan_to_org, T7103.C101_LOAN_TO_ID
						INTO v_shipto, v_shiptoid
					   FROM T504A_LOANER_TRANSACTION T504A
					      , T526_PRODUCT_REQUEST_DETAIL T526
					      , T7104_CASE_SET_INFORMATION T7104
					  	  , T7103_SCHEDULE_INFO T7103
					  WHERE T7103.C7100_CASE_INFO_ID             = T7104.C7100_CASE_INFO_ID
					    AND T7104.C526_PRODUCT_REQUEST_DETAIL_ID = T526.C526_PRODUCT_REQUEST_DETAIL_ID
					    AND T526.C526_PRODUCT_REQUEST_DETAIL_ID  = T504A.C526_PRODUCT_REQUEST_DETAIL_ID
					    AND T504A.C504A_LOANER_TRANSACTION_ID    = v_transrefid
					    AND T7104.C7104_VOID_FL                 IS NULL
					    AND T504A.C504A_VOID_FL                 IS NULL
					    AND T526.C526_VOID_FL                   IS NULL
					    AND ROWNUM=1;
			EXCEPTION WHEN NO_DATA_FOUND
			THEN
				v_shipto := NULL;
				v_shiptoid := NULL;
			END;
		
		END IF;
		
		--
		gm_save_inhouse_item_consign (v_scrapid
									, v_type
									, v_reason
									, v_shipto
									, v_shiptoid
									, v_comments
									, p_userid
									, p_inputstr
									, p_message
									 );

		--	-- updating to 3 as this step doesnt need control #'s
		UPDATE t412_inhouse_transactions
		   SET c412_status_fl = '3'
			 , c412_ref_id = p_conid
			 , c504a_loaner_transaction_id = v_transrefid
			 , c412_last_updated_by = p_userid
             , c412_last_updated_date = SYSDATE
             , c1900_company_id = v_reqyest_company_id
             , c5040_plant_id = v_trnas_plant_id
		 WHERE c412_inhouse_trans_id = v_scrapid;
		 
		 --- updating loaner parts control number for japan country 
	 SELECT get_rule_value_by_company ('SKIP_NOC_UPD', v_type,v_trans_comp_id)
				  INTO v_skip_noc_fl
				  FROM DUAL;
				  

		-- updating control #'s to NOC#
		UPDATE t413_inhouse_trans_items
		   SET c413_control_number = DECODE(v_skip_noc_fl,'Y',c413_control_number,'NOC#')
		   , c413_status_fl = 'Q'
       	 	 , c413_last_updated_by = p_userid
	   	 	 , c413_last_updated_date = SYSDATE 			 
		 WHERE c412_inhouse_trans_id = v_scrapid;
  
    BEGIN
		 
    select C412_TYPE
	INTO v_412_type
	from T412_INHOUSE_TRANSACTIONS where C412_INHOUSE_TRANS_ID=v_scrapid;
	v_txn_id :=v_scrapid;
	EXCEPTION
	 WHEN NO_DATA_FOUND THEN
    v_412_type := NULL;
	END;
		 
		 
		-- For In House Back Orders (ISMP), update the date that this transaction should get written off
		 IF v_type = 1006571
		 THEN
			 BEGIN
				 SELECT TO_CHAR (SYSDATE, 'DD'), get_rule_value ('FINAL', 'EMAIL')
				  INTO v_day, v_final_dd
				  FROM DUAL;
			  EXCEPTION WHEN NO_DATA_FOUND THEN
			  		v_day := 0;
			   v_final_dd := 0;
			  END;
	
			IF v_day >= v_final_dd
			THEN
				v_add_month := 1;
			ELSE
				v_add_month := 0;
			END IF;
	
				UPDATE t412_inhouse_transactions
				   SET c412_expected_return_date =
						   LAST_DAY(TO_DATE (   TO_CHAR (ADD_MONTHS (SYSDATE, v_add_month), 'MM')
									|| '/01/'
									|| TO_CHAR (ADD_MONTHS (SYSDATE, v_add_month), 'YYYY')
								  , 'MM/DD/YYYY'
								   ))
					 , c412_last_updated_date = SYSDATE
					 , c412_last_updated_by = p_userid
				 WHERE c412_inhouse_trans_id = v_scrapid 
				   AND c412_void_fl IS NULL;
				   --ISMP Transaction Post without Job Run
	             gm_pkg_op_loaner.gm_post_inih_missing_item(v_scrapid);
		 END IF;
	  --
	--v_Chk := 'Y';
	
	END IF;

	-- FOR MOVING BACK TO SHELF transaction
	IF p_retstr IS NOT NULL
	THEN
		--
		IF p_type = '4127' OR p_type = '50320'
		THEN
			v_type		:= 50157;
			v_reason	:= 50120;
		ELSE
			v_type		:= 50152;
			v_reason	:= 50105;
		END IF;

		v_returnid	:= get_next_consign_id (v_type);
		p_inh_id	:= p_inh_id || v_returnid || ',';
		
		--
		gm_save_inhouse_item_consign (v_returnid
									, v_type
									, v_reason
									, NULL
									, NULL
									, v_comments
									, p_userid
									, p_retstr
									, p_message
									 );

		--
		UPDATE t412_inhouse_transactions
		   SET c412_status_fl = '2'
			 , c412_ref_id = p_conid
			 , c504a_loaner_transaction_id = v_transrefid
			 , c412_last_updated_by = p_userid
             , c412_last_updated_date = SYSDATE
             , c1900_company_id = v_trans_masret_cmp_id
             , c5040_plant_id = v_trnas_plant_id
		 WHERE c412_inhouse_trans_id = v_returnid;

		--
		v_chk		:= 'Y';
	END IF;

	-- FOR QUARANTINE TRANSACTION
	IF p_quarstr IS NOT NULL
	THEN
		--
		IF p_type = '4127' OR p_type = '50320'
		THEN
			v_type		:= 50158;
			v_reason	:= 50126;
		ELSE
			v_type		:= 50153;
			v_reason	:= 50106;
		END IF;

		v_quarid	:= get_next_consign_id (v_type);
		p_inh_id	:= p_inh_id || v_quarid || ',';
		
		--
		gm_save_inhouse_item_consign (v_quarid, v_type, v_reason, NULL, NULL, v_comments, p_userid, p_quarstr
									, p_message);

		--
		UPDATE t412_inhouse_transactions
		   SET c412_status_fl = '2'
			 , c412_ref_id = p_conid
			 , c504a_loaner_transaction_id = v_transrefid
			 , c412_last_updated_by = p_userid
             , c412_last_updated_date = SYSDATE
             , c1900_company_id = v_trans_masret_cmp_id
             , c5040_plant_id = v_trnas_plant_id
		 WHERE c412_inhouse_trans_id = v_quarid;

		--
		v_chk		:= 'Y';
	END IF;

	-- FOR REPLENISHMENT (SHELF TO SET)
	IF p_repstr IS NOT NULL
	THEN
		--
		IF p_type = '4127'
		THEN
			v_type		:= 50155;
			v_reason	:= 50123;			
		-- IF replenished with loaner extension
		ELSIF p_type = '50320'
		THEN
			v_type		:= 50154;
			v_reason	:= 50128;
		ELSIF p_type = '50324'
		THEN
			v_type		:= 1006575;
			v_reason	:= 50128;	
		ELSE
			v_type		:= 50150;
			v_reason	:= 50101;			
		END IF;

		v_repid 	:= get_next_consign_id (v_type);
		p_replenishid := v_repid;
		p_inh_id	:= p_inh_id || v_repid || ',';
		
		--
		gm_save_inhouse_item_consign (v_repid, v_type, v_reason, NULL, NULL, v_comments, p_userid, p_repstr, p_message);
		
		IF  v_type = '50154' AND v_reason = '50128' AND (nvl(get_rule_value_by_company(p_extReason,'LEEXTAPPR',v_trans_masret_cmp_id),'N') = 'Y') THEN
			v_status_ih_fl := '1';
		ELSE
			v_status_ih_fl := '2';
		END IF;
		
		--
		UPDATE t412_inhouse_transactions
		   SET c412_status_fl = v_status_ih_fl
			 , c412_ref_id = p_conid
			 , c504a_loaner_transaction_id = v_transrefid
			 , c412_last_updated_by = p_userid
             , c412_last_updated_date = SYSDATE
             , c1900_company_id = v_trans_masret_cmp_id    
             , c5040_plant_id = v_trnas_plant_id
		 WHERE c412_inhouse_trans_id = v_repid;

		--
		v_chk		:= 'Y';
	END IF;
	
		-- FOR REPLENISHMENT (BULK TO LOANER SET)
	IF p_bulkstr IS NOT NULL
	THEN
		--
		IF p_type = '4127'
		THEN
			v_type		:= 100063;
			v_reason	:= 50123;		
		ELSIF p_type = '4119' 
		THEN	
			v_type		:= 1006570;
			v_reason	:= 50123;	
		END IF;

		v_repid 	:= get_next_consign_id (v_type);
		p_replenishid := v_repid;
		p_inh_id	:= p_inh_id || v_repid || ',';
		
		--
		gm_save_inhouse_item_consign (v_repid, v_type, v_reason, NULL, NULL, v_comments, p_userid, p_bulkstr, p_message);

		--
		UPDATE t412_inhouse_transactions
		   SET c412_status_fl = '2'
			 , c412_ref_id = p_conid
			 , c504a_loaner_transaction_id = v_transrefid
			 , c412_last_updated_by = p_userid
             , c412_last_updated_date = SYSDATE
             , c1900_company_id = v_trans_masret_cmp_id
             , c5040_plant_id = v_trnas_plant_id
		 WHERE c412_inhouse_trans_id = v_repid;

		--
		v_chk		:= 'Y';
	END IF;
	
	-- FOR REPLENISHMENT INHOUSE SHELF TO INHOUSE LOANER SET
	IF p_ihrepstr IS NOT NULL
	THEN
		--
		IF p_type = '4119' 
		THEN	
			v_type		:= 1006573;
			v_reason	:= 50123;	
		END IF;

		v_repid 	:= get_next_consign_id (v_type);
		p_replenishid := v_repid;
		p_inh_id	:= p_inh_id || v_repid || ',';
		
		--
		gm_save_inhouse_item_consign (v_repid, v_type, v_reason, NULL, NULL, v_comments, p_userid, p_ihrepstr, p_message);

		--
		UPDATE t412_inhouse_transactions
		   SET c412_status_fl = '2'
			 , c412_ref_id = p_conid
			 , c504a_loaner_transaction_id = v_transrefid
			 , c412_last_updated_by = p_userid
             , c412_last_updated_date = SYSDATE
             , c1900_company_id = v_trans_comp_id
             , c5040_plant_id = v_trnas_plant_id
		 WHERE c412_inhouse_trans_id = v_repid;

		--
		v_chk		:= 'Y';
	END IF;

	-- FOR Back Order 
	IF p_type <> '50320' AND p_type <> '50324' THEN
		IF p_bckstr IS NOT NULL
		THEN
		 v_string := p_bckstr;
		 
		 IF p_type = '4127'
		THEN
			 v_type := 100062;
			 v_reason := 50123;
		 ELSIF p_type = '4119'
		 THEN
			 v_type := 1006572;
			 v_reason := 102787;
		 END IF;
		 
		WHILE INSTR (v_string, '|') <> 0
	      LOOP
		      v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
		      v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
		      
				v_repid 	:= get_next_consign_id (v_type);
				p_replenishid := v_repid;
				p_inh_id	:= p_inh_id || v_repid || ',';
		
				gm_save_inhouse_item_consign (v_repid,v_type , v_reason, NULL, NULL, v_comments, p_userid, v_substring || '|', p_message);

				--
				UPDATE t412_inhouse_transactions
				   SET c412_status_fl = '0'
					 , c412_ref_id = p_conid
					 , c504a_loaner_transaction_id = v_transrefid
					 , c412_last_updated_by = p_userid
                     , c412_last_updated_date = SYSDATE
                     , c1900_company_id = v_trans_masret_cmp_id
             	     , c5040_plant_id = v_trnas_plant_id
				 WHERE c412_inhouse_trans_id = v_repid;
				--
				v_chk		:= 'Y';
		  END LOOP;	
		END IF;
		
SELECT SUM (err_cnt) INTO v_err_cnt
  FROM (SELECT set_details.c205_part_number_id,
               NVL (set_details.qty, 0) set_list_qty,
               NVL (ihtxn.qty, 0) bo_qty,
               CASE
                  WHEN NVL (set_details.qty, 0) - NVL (ihtxn.qty, 0) <
                                                                    0
                     THEN 1
                  ELSE 0
               END err_cnt
          FROM (SELECT   t413.c205_part_number_id,
                         SUM (t413.c413_item_qty) qty
                    FROM t412_inhouse_transactions t412,
                         t413_inhouse_trans_items t413
                   WHERE t412.c412_inhouse_trans_id =
                                                    t413.c412_inhouse_trans_id
                     AND t412.c412_ref_id = p_conid
                     AND c412_status_fl < 4
                     AND c412_void_fl IS NULL
                     AND c412_type = v_ih_trans_type -- Back Order Transaction
                GROUP BY t413.c205_part_number_id) ihtxn,
               (SELECT t208.c205_part_number_id, t208.c208_set_qty qty
                  FROM t208_set_details t208, t504_consignment t504
                 WHERE t208.c208_void_fl IS NULL
                   AND t208.c208_inset_fl = 'Y'
                   AND t504.c207_set_id = t208.c207_set_id
                   AND t504.c504_void_fl IS NULL
                   AND t504.c504_consignment_id = p_conid) set_details
         WHERE set_details.c205_part_number_id = ihtxn.c205_part_number_id(+));
         
         IF (v_err_cnt > 0 ) THEN
         	GM_RAISE_APPLICATION_ERROR('-20999','38','');
         
         END IF;
		
	  END IF;
	  
	-- loaner set to scrap transaction
	   IF p_scrapstr IS NOT NULL
	   THEN

	   IF p_type = '4127'
	      THEN
	         v_type := 50156;                          -- product loaner to scrap
	         v_reason := 50124;                                 --Broken/ Damaged
	      ELSE
	         v_type := 50151;                             -- inhouse set to scrap
	         v_reason := 50100;                                 --Broken/ Damaged
	      END IF;
      
	      v_scrapid := get_next_consign_id (v_type);
	      p_inh_id := p_inh_id || v_scrapid || ',';
	      
	      gm_save_inhouse_item_consign (v_scrapid,
	                                    v_type,
	                                    v_reason,
	                                    NULL,
	                                    NULL,
	                                    v_comments,
	                                    p_userid,
	                                    p_scrapstr,
	                                    p_message
	                                   );
	
	      --
	      UPDATE t412_inhouse_transactions
	         SET c412_status_fl = '2',
	             c412_ref_id = p_conid,
                 c504a_loaner_transaction_id = v_transrefid,
                 c412_last_updated_by = p_userid,
                 c412_last_updated_date = SYSDATE,
                 c1900_company_id = v_trans_masret_cmp_id,
                 c5040_plant_id = v_trnas_plant_id
	       WHERE c412_inhouse_trans_id = v_scrapid;
	
	      v_chk := 'Y';
	   END IF;
	   
	   -- inhouse loaner set to inhouse shelf transaction
	   IF p_ihmovestr IS NOT NULL
	   THEN

	   IF p_type = '4119'
	      THEN
	         v_type := 1006574;                          -- product loaner to scrap
	         v_reason := 50100;                                 --Broken/ Damaged	                                     
	      END IF;
      
	      v_ihmoveid := get_next_consign_id (v_type);
	      p_inh_id := p_inh_id || v_ihmoveid || ',';
	      
	      gm_save_inhouse_item_consign (v_ihmoveid,
	                                    v_type,
	                                    v_reason,
	                                    NULL,
	                                    NULL,
	                                    v_comments,
	                                    p_userid,
	                                    p_ihmovestr,
	                                    p_message
	                                   );
	
	      --
	      UPDATE t412_inhouse_transactions
	         SET c412_status_fl = '2',
	             c412_ref_id = p_conid,
                 c504a_loaner_transaction_id = v_transrefid,
 				 c412_last_updated_by = p_userid,
                 c412_last_updated_date = SYSDATE,
                 c1900_company_id = v_trans_comp_id,
                 c5040_plant_id = v_trnas_plant_id
	       WHERE c412_inhouse_trans_id = v_ihmoveid;
	
	      v_chk := 'Y';
	   END IF;

	IF p_type <> '50320' AND p_type <> '50324' 
	THEN
		IF v_chk = 'Y' 
		THEN
			gm_ls_upd_csg_loanerstatus (2, 30, p_conid);
		ELSE
		-- opened back order Transactions and Open shelf to loaner set count
		
		SELECT COUNT(1) INTO v_backorder_count           
		FROM T412_INHOUSE_TRANSACTIONS T412
		WHERE t412.c412_ref_id = p_conid
		AND t412.c412_type IN (100062,50155,1006572,50150) -- 100062 - Loaner Back Order,50155 - Shelf to Loaner Set, 1006572 - IH Loaner Set Back Order,50150 - Shelf to In-House Set
		AND t412.c412_status_fl < 4
        AND t412.c412_void_fl is NULL;
        
        -- If a set has been returned with a part already on backorder, moving it to WIP status 
        -- If a set has been returned with a part already on backorder and released the part from back order when the set is in Process Check,
        -- moving it to WIP status as there are some open transaction for shelf to loaner set
        
        IF v_backorder_count = 0 THEN
			v_status_fl := 50; -- pending Process
        ELSE
			v_status_fl := 30; -- WIP status
        END IF;
	

		--when reconfig loaner, check if set in location, need to delete from location

		gm_pkg_op_set_txn.gm_location_set_clear(p_conid);

 		-- Call common procedure to update the status
 		gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status (p_conid, v_status_fl, p_userid) ;
		UPDATE t504a_consignment_loaner
			   SET c504a_available_date = SYSDATE
				 , c504a_last_updated_date = SYSDATE
				 , C504a_missed_fl = null 		--In WIP/Pending process status missed flag should be null. (PMT-49655)
				 , c504a_last_updated_by = p_userid
			 WHERE c504_consignment_id = p_conid 
			 AND C504a_VOID_FL IS NULL;
			 
	
	  	 
			 /* commenting this code for now as john doesnt want the changes done in 27205 vs
			IF p_type = '4127' THEN
				UPDATE t504a_consignment_loaner
			   	SET c504a_status_fl = 50
					 , c504a_available_date = SYSDATE
					, c504a_last_updated_date = SYSDATE
				 	, c504a_last_updated_by = p_userid
			 	WHERE c504_consignment_id = p_conid 
			 	AND C504a_VOID_FL IS NULL;
			 ELSIF p_type = '4119' THEN
				UPDATE t504a_consignment_loaner
			   	SET c504a_status_fl = 0 
					 , c504a_available_date = SYSDATE
					, c504a_last_updated_date = SYSDATE
				 	, c504a_last_updated_by = p_userid
			 	WHERE c504_consignment_id = p_conid 
			 	AND C504a_VOID_FL IS NULL;			 
			 
			 END IF; */
/*
		SELECT 
			gm_pkg_op_loaner.get_set_list (c207_set_id)  INTO v_qty  
		FROM 
			t504_consignment
		WHERE 
			c504_consignment_id = p_conid AND c504_type = p_type AND c504_void_fl IS NULL;
			
		gm_sav_loaner_status_log(p_logid, p_conid, v_transrefid, v_qty, 25, p_userid);
	*/		 
		END IF;
	END IF;

	--when loaner extension w/ replenish , after relenish, set is_relenish 'Y'
	UPDATE t504a_loaner_transaction
	   SET c504a_is_replenished = 'Y'
	 WHERE c504a_loaner_transaction_id = v_transrefid;

-- if  "Loaner extension", will update t504_laoner_transaction in gm_pkg_op_loaner_extension.gm_op_sav_loaner_extension

	if (v_statusfl = 25 ) then -- if processing 
		
		IF p_type <> '50320' AND p_type <> '50324'
		THEN
			-- when processing loaner return, update the status log for loaner
			gm_save_status_details (p_conid, '', p_userid, NULL, SYSDATE, 91123, 91103);
			
		END IF;
	
		--for non-usage charges
		v_loanid	:= gm_pkg_op_loaner.get_loaner_trans_id (p_conid);
	
		UPDATE t504a_loaner_transaction
		   SET c504a_processed_date = TRUNC (SYSDATE)
			 , c504a_last_updated_date = SYSDATE
			 , c504a_last_updated_by = p_userid
		 WHERE c504a_loaner_transaction_id = v_loanid;
		IF p_type <> '4119' THEN
		gm_pkg_op_charges.gm_sav_usage_flg_processing (v_loanid, p_userid);
		END IF;
	end if;
	if (v_statusfl = 5 or v_statusfl = 10) then
		gm_pkg_op_consignment.gm_sav_hold_fl(p_conid,p_userid);
	end if;
	  	 	
	
	if(v_412_type='50159') then
	gm_pkg_op_inv_field_sales_tran.gm_sav_loaner_lot(v_txn_id,p_userid);
	
	end if;


END gm_cs_sav_loaner_return;
/
