/* Formatted on 2011/12/19 15:06 (Formatter Plus v4.8.0) */
--@"C:\Database\Procedures\CustomerServices\GM_UPDATE_NOTCNTRL_STR.prc";

CREATE OR REPLACE PROCEDURE gm_update_notcntrl_str (
   p_ordid         IN       t501_order.c501_order_id%TYPE,
   p_userid        IN       t501_order.c501_created_by%TYPE,
   p_str           IN       CLOB,
   p_boflag        IN       VARCHAR2,
   p_rmqtymsgstr   OUT      VARCHAR2,
   p_boqtymsgstr   OUT      VARCHAR2
)
AS
   v_string       CLOB;
   v_substring    CLOB;
   v_orgqty       VARCHAR2 (100);
   v_itemordid    VARCHAR2 (100);
   v_pnum         VARCHAR2 (100);
   v_ptype        VARCHAR2 (100);
   v_cnum         VARCHAR2 (100);
   v_price        VARCHAR2 (100);
   v_qty          VARCHAR2 (100);
   v_pnum_str     VARCHAR2 (4000);
   v_boid_str     VARCHAR2 (4000);
   v_order_date   DATE;
   v_out_boid     VARCHAR2 (50);
   v_org_qty      NUMBER;
   v_loop_cnt     NUMBER;
   v_bo_cnt       NUMBER;
   v_len          NUMBER;
   v_lastbo       NUMBER;
   v_attr_id      NUMBER;
   v_case_id      NUMBER;
   v_process_cnt  NUMBER;
   v_list_price			t502_item_order.c502_list_price%TYPE;
   v_unit_price 		t502_item_order.c502_unit_price%TYPE;
   v_unit_prc_adj_code 	t502_item_order.c901_unit_price_adj_code%TYPE;
   v_unit_price_adj_val t502_item_order.c502_unit_price_adj_value%TYPE;
   v_disc_offered 		t502_item_order.c502_discount_offered%TYPE;
   v_disc_type			t502_item_order.c901_discount_type%TYPE;
   v_account_price 		t502_item_order.C502_DO_UNIT_PRICE%TYPE;
   v_adj_code_value 	t502_item_order.C502_ADJ_CODE_VALUE%TYPE;
   --
   v_order_info_id NUMBER;
   v_tmp_qty NUMBER;
   v_order_usage_fl VARCHAR2 (10);
   v_order_type   t501_order.c901_order_type%TYPE;
   v_current_order_status t501_order.c501_status_fl%TYPE;
   v_pdt_family NUMBER;
BEGIN
   v_pnum_str := '';
   v_string := p_str;
   v_loop_cnt := 0;
   v_bo_cnt := 0;
   v_lastbo := 0;

	-- to get the order usage data flag
	 SELECT NVL (get_rule_value_by_company (c1900_company_id, 'ORDER_USAGE', c1900_company_id), 'N'),c501_status_fl, NVL (c901_order_type, '-999999') 
	   INTO v_order_usage_fl,v_current_order_status, v_order_type
	   FROM t501_order
	  WHERE c501_order_id = p_ordid;
	--    
   WHILE INSTR (v_string, '|') <> 0
   LOOP
      v_loop_cnt := v_loop_cnt + 1;
      v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
      v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
      v_orgqty := NULL;
      v_itemordid := NULL;
      v_pnum := NULL;
      v_ptype := NULL;
      v_price := NULL;
      v_qty := NULL;
      v_cnum := NULL;
      --OrgQty^ItemOrdId^PNUM^PType^RmQty
      v_orgqty := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
      v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
      v_itemordid := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
      v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
      v_pnum := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
      v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
      v_ptype := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
      v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
      v_price := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
      v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
      v_qty := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
      v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
      v_cnum := v_substring;
      
       --	Get the product family inside the for loop 32396
      BEGIN
		  SELECT c205_product_family INTO v_pdt_family
		  FROM t205_part_number
		  WHERE c205_part_number_id = v_pnum;
	  EXCEPTION WHEN NO_DATA_FOUND
	  	THEN
			v_pdt_family := NULL;
		END;
      -- the below validation added to avoid discrepancy between transaction and location
		IF v_current_order_status = 1 AND v_qty > 0 THEN
			SELECT count(1)
			INTO v_process_cnt
			FROM t5055_control_number t5055
			WHERE t5055.c5055_ref_id     = p_ordid
			AND t5055.c5055_void_fl   IS NULL;
			   
			IF v_process_cnt > 0
			THEN
				raise_application_error ('-20999', 'This action (editing quantities) cannot be done until transaction is in Pending Shipping status.');
			END IF;
		END IF;

      BEGIN
      SELECT t502.c502_item_qty orgqty, c502_attribute_id, C502_LIST_PRICE,C502_UNIT_PRICE,C901_UNIT_PRICE_ADJ_CODE,C502_UNIT_PRICE_ADJ_VALUE,C502_DISCOUNT_OFFERED,C901_DISCOUNT_TYPE, C502_DO_UNIT_PRICE, C502_ADJ_CODE_VALUE
      , c500_order_info_id 
      INTO v_org_qty, v_attr_id, v_list_price, v_unit_price, v_unit_prc_adj_code, v_unit_price_adj_val, v_disc_offered,v_disc_type , v_account_price , v_adj_code_value
      , v_order_info_id
      FROM t502_item_order t502
      WHERE t502.c502_item_order_id = v_itemordid
      AND t502.c502_void_fl IS NULL
      AND t502.c502_delete_fl IS NULL;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      v_org_qty := NULL;
      v_attr_id := NULL;
      END;

      v_len := NVL (LENGTH (v_string), 0);
      IF v_org_qty IS NOT NULL THEN
      
      IF v_org_qty = v_qty AND p_boflag = 'Y'
      THEN
         v_bo_cnt := v_bo_cnt + 1;
      END IF;
      --PMT-32396 Back Order email, Back order should not be generated for usage code parts
      IF v_bo_cnt = v_loop_cnt AND v_len = 0 AND p_boflag = 'Y' and v_pdt_family != '26240096' 
      THEN
         UPDATE t501_order
            SET c501_status_fl = '0',
                c501_last_updated_by = p_userid,
                c501_last_updated_date = SYSDATE,
                c901_order_type = 2525
          WHERE c501_order_id = p_ordid;

         v_lastbo := 1;
      ELSIF v_org_qty = v_qty AND v_qty IS NOT NULL AND v_qty > 0
      THEN
         v_pnum_str := v_pnum_str || v_pnum || ',';

         DELETE FROM t502_item_order
               WHERE c502_item_order_id = v_itemordid;
         -- when entered back order qty then - remove the usage table (t502B)
         IF p_boflag = 'Y' AND v_order_usage_fl = 'Y'
         THEN
         	gm_pkg_cs_usage_lot_number.gm_upd_backorder_qty (v_order_info_id, v_qty, p_userid);
         END IF;
      ELSIF v_org_qty > v_qty AND v_qty > 0
      THEN
         v_pnum_str := v_pnum_str || v_pnum || ',';

         UPDATE t502_item_order
            SET c502_item_qty = c502_item_qty - TO_NUMBER (v_qty)
          WHERE c502_item_order_id = v_itemordid;
          -- when entered back order qty then - remove the usage table (t502B)
          IF p_boflag = 'Y' AND v_order_usage_fl = 'Y'
          THEN
          	gm_pkg_cs_usage_lot_number.gm_upd_backorder_qty (v_order_info_id, v_qty, p_userid);
          END IF;
      ELSIF v_org_qty < v_qty AND v_org_qty > 0 --Should not show the validation, if the quantity is '-ve'
      THEN
         raise_application_error ('-20796', '');
      END IF;
      --PMT-32396 Back Order email, Back order should not be generated for usage code parts
      IF p_boflag = 'Y' AND v_lastbo <> 1 AND v_qty > 0 and v_pdt_family != '26240096' 
      THEN
         gm_save_item_ord_backorder (p_ordid,
                                     v_pnum,
                                     v_qty,
                                     v_price,
                                     NULL,
                                     NULL,
                                     v_out_boid
                                    );
		-- to create the new records to order info and usage table.
		IF v_order_usage_fl = 'Y'
		THEN
			gm_pkg_cs_usage_lot_number.gm_sav_order_info (v_out_boid, 'Y', p_userid) ;
			-- to carry the lot and ddt number from existing orders
			gm_pkg_cs_usage_lot_number.gm_sync_back_order_usage_lot (v_out_boid, v_order_info_id, p_userid);
		END IF;	
		--
		UPDATE t502_item_order 
               SET C502_LIST_PRICE = v_list_price,
				   C502_UNIT_PRICE = v_unit_price,
				   C901_UNIT_PRICE_ADJ_CODE = v_unit_prc_adj_code,
				   C502_UNIT_PRICE_ADJ_VALUE = v_unit_price_adj_val,
				   C502_DISCOUNT_OFFERED = v_disc_offered,
				   C901_DISCOUNT_TYPE = v_disc_type ,
				   C502_DO_UNIT_PRICE = v_account_price,
				   C502_ADJ_CODE_VALUE = v_adj_code_value
             WHERE c501_order_id = v_out_boid;

		v_boid_str := v_boid_str || v_out_boid || ',';

         -- update the attribute id for the newly created order
         IF v_attr_id IS NOT NULL
         THEN
            UPDATE t502_item_order t502
               SET c502_attribute_id = v_attr_id
             WHERE t502.c501_order_id = v_out_boid;
         END IF;
      END IF;
    END IF;  
   END LOOP;

   IF v_pnum_str IS NOT NULL AND p_boflag IS NULL
   THEN
      v_pnum_str := SUBSTR (v_pnum_str, 1, LENGTH (v_pnum_str) - 1);
      gm_pkg_common_cancel_op.gm_cm_sav_cancelrow
                           (p_ordid,
                            100405,
                            'VPTDO',
                               'The Part numbers that are removed from the current DO are'
                            || v_pnum_str,
                            p_userid
                           );
      p_rmqtymsgstr :=
          'The parts that are removed for the current DO are: ' || v_pnum_str;

      BEGIN
         SELECT t501.c7100_case_info_id
           INTO v_case_id
           FROM t501_order t501
          WHERE t501.c501_order_id = p_ordid;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_case_id := '';
      END;

      IF v_case_id IS NOT NULL
      THEN
         my_context.set_my_inlist_ctx (v_pnum_str);

         -- void the attribute records for all the parts that are removed from the order
         UPDATE t502a_item_order_attribute t502a
            SET t502a.c502a_void_fl = 'Y',
                t502a.c502a_last_updated_by = p_userid,
                t502a.c502a_last_updated_date = SYSDATE
          WHERE t502a.c502_attribute_id IN (
                   SELECT t502.c502_attribute_id
                     FROM t502_item_order t502
                    WHERE t502.c501_order_id = p_ordid
                      AND t502.c205_part_number_id IN (
                                                       SELECT token
                                                         FROM v_in_list
                                                        WHERE token IS NOT NULL)
                      AND t502.c502_void_fl IS NULL);

         -- viod the tags  for all the parts that are removed from the order
         UPDATE t5012_tag_usage t5012
            SET t5012.c5012_void_fl = 'Y',
                c5012_last_updated_by = p_userid,
                c5012_last_updated_date = SYSDATE
          WHERE c5012_ref_id = v_case_id
            AND c901_ref_type = 2570
            AND c205_part_number_id IN (SELECT token
                                          FROM v_in_list
                                         WHERE token IS NOT NULL)
            AND t5012.c5012_void_fl IS NULL;
      END IF;
   --
   END IF;

   IF p_boflag IS NOT NULL AND v_boid_str IS NOT NULL
   THEN
      v_boid_str := SUBSTR (v_boid_str, 1, LENGTH (v_boid_str) - 1);
      p_boqtymsgstr :=
            'The following transaction(s) have been generated for the backordered part(s): '
         || v_boid_str;
   END IF;
END gm_update_notcntrl_str;
/
