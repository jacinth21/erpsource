/* Formatted on 2012/06/19 16:55 (Formatter Plus v4.8.0) */
--@"C:\database\Procedures\CustomerServices\gm_cs_sav_loaner_recon.prc";

CREATE OR REPLACE PROCEDURE gm_cs_sav_loaner_recon (
   p_transid          IN       t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
   p_soldstr          IN       VARCHAR2,
   p_constr           IN       VARCHAR2,
   p_writestr         IN       VARCHAR2,
   p_retstr           IN       VARCHAR2,
   p_userid           IN       t412_inhouse_transactions.c412_last_updated_by%TYPE,
   p_out              OUT      VARCHAR2,
   p_reconciled_cur   OUT      TYPES.cursor_type,
   p_missing_cur      OUT      TYPES.cursor_type,
   p_ticket_id        OUT      t9800_jira_ticket.c9800_ticket_id%TYPE
)
AS
/***************************************************************************************
 * Description        : This procedure is used to
 ****************************************************************************************/
--
   v_qty        NUMBER;
   v_id         VARCHAR2 (20);
   v_distid     VARCHAR2 (20);
   v_transid    VARCHAR2 (20);
   v_type       NUMBER;
   v_reason     NUMBER;
   v_comments   VARCHAR2 (100);
   v_request_id VARCHAR2(20);
   v_trans_company_id VARCHAR2(20);
   v_trans_plant_id VARCHAR2(20);
   v_company_id              t1900_company.c1900_company_id%TYPE;
   v_plant_id                t5040_plant_master.c5040_plant_id%TYPE;  
   v_string		VARCHAR2(30000)  :=p_retstr;
   v_substring	VARCHAR2(1000);
   v_qrnstr  VARCHAR2(3000);
   v_retstr  VARCHAR2(3000);
   v_out VARCHAR2(3000);
--
BEGIN
   --
   IF p_soldstr IS NOT NULL
   THEN
      gm_cs_sav_loaner_recon_items (p_transid, p_soldstr, 'S', NULL,
                                    p_userid);
   END IF;

   --
   IF p_constr IS NOT NULL
   THEN
      -- Getting the Distributor ID
      SELECT t504a.c504a_consigned_to_id ,  NVL(t504a.c1900_company_id,GET_COMPID_FRM_CNTX()) , NVL(t504a.c5040_plant_id,GET_PLANTID_FRM_CNTX())
        INTO v_distid , v_trans_company_id , v_trans_plant_id
        FROM t504a_loaner_transaction t504a, t412_inhouse_transactions t412
       WHERE t412.c412_inhouse_trans_id = p_transid
         AND t412.c504a_loaner_transaction_id =
                                             t504a.c504a_loaner_transaction_id;

      --
      v_id := get_next_consign_id ('Consign');
      gm_save_item_consign
                        (v_id,
                         4110,
                         0,
                         v_distid,
                         NULL,
                         0,
                            'Consigned after reconciling Loaner Transaction:'
                         || p_transid,
                         NULL,
                         p_userid,
                         p_constr
                        );

      --
      UPDATE t504_consignment
         SET c504_update_inv_fl = '1',
             c504_verify_fl = '1',
             c504_last_updated_date = SYSDATE,
             c504_last_updated_by = p_userid,
             c504_ship_date = TRUNC (SYSDATE),
             c504_status_fl = 4,
             c504_reprocess_id = p_transid,
             c1900_company_id  = v_trans_company_id,
             c5040_plant_id = v_trans_plant_id
       WHERE c504_consignment_id = v_id;

      --
      gm_cs_sav_loaner_recon_items (p_transid, p_constr, 'C', v_id, p_userid);
      
      -- PC-1779 - To increase field sales inventory while moving part as consigned in Loaner Reconciliation
      gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv(v_id, 'T505', v_distid, 102900, 4301, 102907, p_userid);
      
   END IF;

   --
   IF p_writestr IS NOT NULL
   THEN
      gm_cs_sav_loaner_recon_items (p_transid,
                                    p_writestr,
                                    'W',
                                    NULL,
                                    p_userid
                                   );
   END IF;
   --
   IF p_retstr IS NOT NULL
   THEN
   
   WHILE INSTR(v_string, '~') <> 0
		LOOP
			v_substring := SUBSTR(v_string, 1, INSTR(v_string, '~') - 1);
			v_string	:= SUBSTR(v_string, INSTR(v_string, '~') + 1);
			v_qrnstr := v_qrnstr ||  v_string;
			v_retstr :=v_retstr || v_substring;
	
	END LOOP;
	
	IF v_retstr IS NOT NULL THEN
	
   	SELECT  get_compid_frm_cntx(),get_plantid_frm_cntx()
		  INTO  v_company_id,v_plant_id
		  FROM DUAL;
		  
   	 --validate the company based on ref_id and saving ref company into inhouse trasnaction incase of company mismatch 
	  IF p_transid IS NOT NULL THEN
	   gm_pkg_common.gm_fch_trans_comp_plant_id(p_transid,v_trans_company_id,v_trans_plant_id);
		IF (v_trans_company_id IS NOT NULL) AND (v_company_id != v_trans_company_id) THEN
     	 gm_pkg_cor_client_context.gm_sav_client_context(v_trans_company_id,v_plant_id);
       	END IF;
	  END IF;
	  
      -- if loaner type = "Product Loaner"
      v_type := 100064;                 --Loaner Reconcilation to repackaging
      v_reason := 100261;               --Loaner Reconcilation to repackaging
      v_transid := get_next_consign_id (v_type);      
      v_comments := 'Reprocess Transaction from Inhouse ID: ' || p_transid;
    
      gm_cs_sav_loaner_recon_items (p_transid, v_retstr, 'R', v_transid, p_userid);
      
      gm_save_inhouse_item_consign (v_transid,
                                    v_type,
                                    v_reason,
                                    NULL,
                                    NULL,
                                    v_comments,
                                    p_userid,
                                    v_retstr,
                                    p_out
                                   );

      UPDATE t412_inhouse_transactions
         SET c412_ref_id = p_transid,
             c412_last_updated_date = SYSDATE,
             c412_last_updated_by = p_userid
       WHERE c412_inhouse_trans_id = v_transid;
       
      v_out :=v_transid||','|| v_out;
   END IF;
      
    IF v_qrnstr IS NOT NULL THEN
     
    SELECT get_compid_frm_cntx(),get_plantid_frm_cntx() INTO v_company_id,v_plant_id FROM DUAL;
  
   --validate the company based on ref_id and saving ref company into inhouse trasnaction incase of company mismatch 
     IF p_transid IS NOT NULL THEN 
        gm_pkg_common.gm_fch_trans_comp_plant_id(p_transid,v_trans_company_id,v_trans_plant_id);
		IF (v_trans_company_id IS NOT NULL) AND (v_company_id != v_trans_company_id) THEN
     	 gm_pkg_cor_client_context.gm_sav_client_context(v_trans_company_id,v_plant_id);
       	END IF;
	  END IF;

		-- if loaner type = "Product Loaner"
		v_type := 26241141;-- new rule value for LRQN;
		v_reason := 100261; --new rule value for LRQN;;
		v_transid := get_next_consign_id (v_type);
		v_comments := 'Reprocess Transaction from Inhouse ID:'  || p_transid;
		
		gm_cs_sav_loaner_recon_items (p_transid, v_qrnstr, 'R', v_transid, p_userid);
		
		gm_save_inhouse_item_consign (v_transid,v_type,v_reason,NULL,NULL,v_comments,p_userid,v_qrnstr,p_out);
		
		UPDATE t412_inhouse_transactions
		SET c412_ref_id = p_transid,
		c412_last_updated_date = current_date,
		c412_last_updated_by = p_userid
		WHERE c412_inhouse_trans_id = v_transid;
		
		  v_out :=v_transid||','|| v_out;
    END IF;
   END IF;
   --
   p_out := v_out;
--
   SELECT   NVL (SUM (c413_item_qty), 0)
          - get_cs_loaner_recon_qty (p_transid, NULL)
     INTO v_qty
     FROM t413_inhouse_trans_items
    WHERE c412_inhouse_trans_id = p_transid AND c413_status_fl = 'Q';

   --
   IF v_qty = 0
   THEN
      UPDATE t412_inhouse_transactions
         SET c412_status_fl = 4,
             c412_verified_date = SYSDATE,
             c412_verified_by = p_userid,
             c412_last_updated_by = p_userid,
			 c412_last_updated_date = SYSDATE
       WHERE c412_inhouse_trans_id = p_transid;
   END IF;

   --
   gm_pkg_op_charges.gm_sav_usage_flg_reconcilation (p_transid, p_userid);

   OPEN p_reconciled_cur
    FOR
       SELECT   t413.c205_part_number_id pnum, SUM (c413_item_qty) qty,
                DECODE (c413_status_fl,
                        'S', 'Sold',
                        'C', 'Consigned',
                        'W', '',
                        'R', 'Returned'
                       ) reconciled_as
           FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
          WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
            AND t412.c412_inhouse_trans_id = p_transid
            AND c413_status_fl IN ('S', 'C', 'W', 'R')
       GROUP BY t413.c205_part_number_id, c413_status_fl;

   SELECT t526.c525_product_request_id
     INTO v_request_id
     FROM t412_inhouse_transactions t412,
          t504a_loaner_transaction t504a,
          t526_product_request_detail t526
    WHERE t412.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id
      AND t504a.c526_product_request_detail_id =
                                           t526.c526_product_request_detail_id
      AND t412.c412_inhouse_trans_id = p_transid;

   OPEN p_missing_cur
    FOR
       SELECT c205_part_number_id pnum,
              (NVL (missing_qty, 0) - NVL (reconciled_qty, 0)) qty,
              get_part_price_list (c205_part_number_id, '') price,
                get_part_price_list (c205_part_number_id, '')
              * (NVL (missing_qty, 0) - NVL (reconciled_qty, 0)) eprice
         FROM (SELECT   t413.c205_part_number_id,
                        NVL (SUM (DECODE (c413_status_fl,
                                          'Q', c413_item_qty,
                                          NULL
                                         )
                                 ),
                             0
                            ) missing_qty,
                        NVL (SUM (DECODE (c413_status_fl,
                                          'S', c413_item_qty,
                                          'C', c413_item_qty,
                                          'W', c413_item_qty,
                                          'R', c413_item_qty,
                                          'Q', NULL
                                         )
                                 ),
                             0
                            ) reconciled_qty
                   FROM t412_inhouse_transactions t412,
                        t413_inhouse_trans_items t413
                  WHERE t412.c412_inhouse_trans_id =
                                                    t413.c412_inhouse_trans_id
                    AND t412.c412_inhouse_trans_id IN (
                           SELECT c412_inhouse_trans_id
                             FROM t412_inhouse_transactions t412,
                                  t504a_loaner_transaction t504a,
                                  t526_product_request_detail t526
                            WHERE t412.c504a_loaner_transaction_id =
                                             t504a.c504a_loaner_transaction_id
                              AND t504a.c526_product_request_detail_id =
                                           t526.c526_product_request_detail_id
                              AND t526.c525_product_request_id = v_request_id
                              AND c412_type = 50159)
                    AND c413_status_fl IN ('S', 'C', 'W', 'R', 'Q')
               GROUP BY c205_part_number_id)
        WHERE missing_qty - reconciled_qty > 0;

   BEGIN
      SELECT t9800.c9800_ticket_id
        INTO p_ticket_id
        FROM t412_inhouse_transactions t412,
             t504a_loaner_transaction t504a,
             t9800_jira_ticket t9800,
             t526_product_request_detail t526
       WHERE t412.c504a_loaner_transaction_id =
                                             t504a.c504a_loaner_transaction_id
         AND t504a.c526_product_request_detail_id =
                                           t526.c526_product_request_detail_id
         AND t526.c526_product_request_detail_id = t9800.c9800_ref_id
         AND t9800.c9800_ref_type = 10056
         AND t9800.c9800_void_fl IS NULL
         AND t412.c412_inhouse_trans_id = p_transid;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         BEGIN
		  SELECT t9800.c9800_ticket_id
			INTO p_ticket_id
			FROM t412_inhouse_transactions t412,
				 t504a_loaner_transaction t504a,
				 t9800_jira_ticket t9800,
				 t526_product_request_detail t526
		   WHERE t412.c504a_loaner_transaction_id =
												 t504a.c504a_loaner_transaction_id
			 AND t504a.c526_product_request_detail_id =
											   t526.c526_product_request_detail_id
			 AND t526.c525_product_request_id = t9800.c9800_ref_id
			 AND t9800.c9800_ref_type = 110521
			 AND t9800.c9800_void_fl IS NULL
			 AND t412.c412_inhouse_trans_id = p_transid;
	   EXCEPTION
		  WHEN NO_DATA_FOUND
		  THEN
			 p_ticket_id := '';
	   END;
   END;
END gm_cs_sav_loaner_recon;
/
