/* Formatted on 2012/03/10 16:13 (Formatter Plus v4.8.0) */
CREATE OR REPLACE PROCEDURE gm_process_backorder (
   p_orderid   IN       t501_order.c501_order_id%TYPE,
   p_user_id   IN       t501_order.c501_last_updated_by%TYPE,
   p_errmsg    OUT      VARCHAR2
)
AS
--
   p_invfl         VARCHAR2 (10);
   v_msg           VARCHAR2 (100);
   v_qty           NUMBER;
   v_stock         NUMBER;
   v_allocate_fl   VARCHAR2 (1);
   v_chk_fl        number;
   v_pnum          t502_item_order.c205_part_number_id%TYPE;
--
BEGIN
--
   select c205_part_number_id , c502_item_qty
     INTO v_pnum, v_qty
     FROM t502_item_order
    where c501_order_id = p_orderid AND c502_VOID_FL IS NULL 
      FOR UPDATE;

     select get_qty_in_stock (v_pnum) 
       INTO v_stock
       from t205_part_number
      where c205_part_number_id = v_pnum
        FOR UPDATE;


   IF v_qty > v_stock
   THEN
      raise_application_error (-20792, '');
   END IF;

   UPDATE t501_order
      SET c501_status_fl = '1',
          c501_last_updated_by = p_user_id,
          c501_last_updated_date = SYSDATE
    WHERE c501_order_id = p_orderid;
    
          --PMT-47806 Reverted backorders not listed parts while control
          UPDATE t5055_control_number
          SET c5055_void_fl = 'Y',
          c5055_last_updated_by = p_user_id,
          c5055_last_updated_date = SYSDATE
          WHERE c5055_ref_id = p_orderid
          AND c5055_void_fl IS NULL;
           
          --PMT-47806 Reverted backorders not listed parts while control

--
   SELECT COUNT (1)
     INTO v_chk_fl
     FROM t501a_order_attribute t501a
    WHERE t501a.c501_order_id = p_orderid
      AND c901_attribute_type = 11241                       -- External Vendor
      AND c501a_void_fl IS NULL;

   IF v_chk_fl = 0
   THEN
-- When the BO status is updated to Backlog, record should be entered in T5050 for processing from device.
   -- If the Transaction has Literature part, it should not be allocated to device.
      v_allocate_fl := gm_pkg_op_inventory.chk_txn_details (p_orderid, 93001);

      IF NVL (v_allocate_fl, 'N') = 'Y'
      THEN
         gm_pkg_allocation.gm_ins_invpick_assign_detail (93001,
                                                         p_orderid,
                                                         p_user_id
                                                        );
      END IF;
   END IF;
END gm_process_backorder;
/
