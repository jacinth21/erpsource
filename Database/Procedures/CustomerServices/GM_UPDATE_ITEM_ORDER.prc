CREATE OR REPLACE PROCEDURE GM_UPDATE_ITEM_ORDER
(
	   p_ItemOrdId			IN T502_ITEM_ORDER.C502_ITEM_ORDER_ID%TYPE,
	   p_Control			IN T502_ITEM_ORDER.C502_CONTROL_NUMBER%TYPE,
	   p_OrdId				IN T501_ORDER.C501_ORDER_ID%TYPE,
	   p_UserId				IN T501_ORDER.C501_LAST_UPDATED_BY%TYPE,
	   p_message 			OUT  VARCHAR2
)
AS
BEGIN
              UPDATE T502_ITEM_ORDER
			  		 SET
					 	C502_CONTROL_NUMBER = p_Control
					WHERE
						C502_ITEM_ORDER_ID = p_ItemOrdId;
-- To move this to the bean or to a separate SP since this SP is called in a loop and this portion executes everytime which is not needed
-- From here
			  UPDATE T501_ORDER
			  		 SET
					 	C501_STATUS_FL = '2',
						C501_LAST_UPDATED_BY = p_UserId,
						C501_LAST_UPDATED_DATE = SYSDATE
					WHERE
						C501_ORDER_ID = p_OrdId;
-- Till here
			  EXCEPTION WHEN OTHERS
      	 	  THEN
                       p_message := SQLERRM;
              RETURN;
END GM_UPDATE_ITEM_ORDER;
/

