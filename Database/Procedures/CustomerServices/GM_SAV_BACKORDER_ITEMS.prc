/* Formatted on 2020/12/05 12:59 (Formatter Plus v4.8.0) */
-- @"C:\database\Procedures\CustomerServices\GM_SAV_BACKORDER_ITEMS.prc"

--PC-3430 Sales back order release issue

create or replace PROCEDURE gm_sav_backorder_items (
	p_orderid	  IN   t501_order.c501_order_id%TYPE,
    p_out	Out 	  t501_order.c501_order_id%TYPE
    
    )
AS

    v_order_id         t501_order.c501_order_id%TYPE;
    v_part             t502_item_order.C205_PART_NUMBER_ID%TYPE;
    v_part_qty         t502_item_order.C502_ITEM_QTY%TYPE;
    v_itm_prc          t502_item_order.C502_ITEM_PRICE%TYPE;
    v_cons_fl          t502_item_order.C502_CONSTRUCT_FL%TYPE;
    v_temp_boid        t501_order.c501_order_id%TYPE;
    v_part_refid       t502_item_order.C502_REF_ID%TYPE;
	v_bo_part		   t205_part_number.c205_part_number_id%TYPE;
    v_total_cost       t501_order.C501_TOTAL_COST%TYPE;
    v_part_cnt		   NUMBER;

    CURSOR bo_parts_cur
    IS
           
			  SELECT t502.c205_part_number_id v_bo_part 
			  	FROM t501_order t501, t502_item_order t502 
			   WHERE t501.C501_ORDER_ID = t502.C501_ORDER_ID 
			   	 AND t501.c901_order_type='2525' 
			   	 AND t502.C501_ORDER_ID = p_orderid 
			   	 AND C502_VOID_FL IS NULL;
				
				BEGIN
				
					FOR bo_part_upd IN bo_parts_cur
					LOOP
				
				BEGIN
					SELECT C501_ORDER_ID,C205_PART_NUMBER_ID,C502_ITEM_QTY,C502_ITEM_PRICE,c502_construct_fl,c502_ref_id 
	                  INTO v_order_id,v_part,v_part_qty,v_itm_prc,v_cons_fl,v_part_refid
					  FROM t502_item_order 
					 WHERE c501_order_id =p_orderid AND c205_part_number_id = bo_part_upd.v_bo_part AND C502_VOID_FL IS NULL;
					EXCEPTION
	                 WHEN NO_DATA_FOUND
	            THEN
						v_part:='';
						v_order_id:='';
						v_part_qty :=0;
						v_itm_prc :=0;
						v_cons_fl :='';
						v_part_refid :='';
				END;
				
				BEGIN
														
					SELECT count(t502.c205_part_number_id) 
					  INTO v_part_cnt  
					  FROM t501_order t501, t502_item_order t502 
					 WHERE t501.C501_ORDER_ID = t502.C501_ORDER_ID 
					   AND t501.c901_order_type='2525' 
					   AND t502.C501_ORDER_ID = p_orderid 
					   AND C502_VOID_FL IS NULL;
				END;
											
				IF v_part_cnt > 1 THEN
				
					UPDATE t502_item_order 
				   		SET C502_VOID_FL='Y' 
				 	WHERE C501_ORDER_ID =p_orderid 
				   		AND c205_part_number_id=bo_part_upd.v_bo_part;
				
				gm_save_item_ord_backorder (v_order_id,v_part,v_part_qty,v_itm_prc,v_cons_fl,v_part_refid,v_temp_boid);
				                  
               END IF;
         END LOOP;
                
		            SELECT sum(NVL(t502.c502_item_qty, 0) * NVL (t502.c502_item_price, 0)) 
		              INTO v_total_cost  
		              FROM t501_order t501, t502_item_order t502 
		             WHERE t501.C501_ORDER_ID = t502.C501_ORDER_ID 
		               AND t501.c901_order_type='2525'  
		               AND t502.C501_ORDER_ID = p_orderid 
		               AND t502.C502_VOID_FL IS NULL;
		              
			        UPDATE t501_order 
			            SET c501_total_cost= v_total_cost 
			        WHERE c501_order_id =p_orderid;
            
    END GM_SAV_BACKORDER_ITEMS;
/