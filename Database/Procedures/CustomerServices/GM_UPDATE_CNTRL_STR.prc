/* Formatted on 2010/12/14 09:29 (Formatter Plus v4.8.0) */
--@"C:\Database\Procedures\CustomerServices\GM_UPDATE_CNTRL_STR.prc";

CREATE OR REPLACE PROCEDURE gm_update_cntrl_str (
	p_ordid 		IN		 t501_order.c501_order_id%TYPE
  , p_userid		IN		 t501_order.c501_created_by%TYPE
  , p_str			IN		 CLOB
  , p_partnumstr	OUT		 VARCHAR2
  , p_qtymsgstr		OUT 	 VARCHAR2
)
AS
	v_string	   CLOB;
	v_substring    VARCHAR2 (4000);
	v_orgqty	   VARCHAR2 (100);
	v_itemordid    VARCHAR2 (100);
	v_pnum		   VARCHAR2 (100);
	v_ptype 	   VARCHAR2 (100);
	v_cnum		   VARCHAR2 (100);
	v_price 	   VARCHAR2 (100);
	v_qty		   VARCHAR2 (100);
	v_pnum_str	   VARCHAR2 (4000);
	v_boid_str	   VARCHAR2 (4000);
	v_order_date   DATE;
	v_out_boid	   VARCHAR2 (50);
	v_org_qty	   NUMBER;
	v_qty_str		VARCHAR2 (4000);
	v_qty_cnt 	number;
BEGIN
	
	v_string	:= p_str;
	v_qty_cnt := 0;
	WHILE INSTR (v_string, '|') <> 0
	LOOP
		v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
		v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
		v_orgqty	:= NULL;
		v_itemordid := NULL;
		v_pnum		:= NULL;
		v_ptype 	:= NULL;
		v_price 	:= NULL;
		v_qty		:= NULL;
		v_cnum		:= NULL;
		--OrgQty^ItemOrdId^PNUM^PType^RmQty
		v_orgqty	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
		v_itemordid := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
		v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
		v_ptype 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
		v_price 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
		v_qty		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
		v_cnum		:= v_substring;
		--may need to group by part# and price
		
		IF v_qty > 0 AND v_ptype <> '50301' --excluding loaner parts -PMT 234
		THEN
			v_pnum_str	:= v_pnum_str || v_pnum || ',';	
			--105140: Edit Order , type to be inserted in t413_inhouse_trans_items table
			v_qty_str := v_qty_str || v_pnum||','||v_qty||','||''||','||''||','||v_price || ','||v_itemordid||','||'105140'||'|';
		END IF;
	END LOOP;
	
	p_partnumstr := v_pnum_str;
	p_qtymsgstr := v_qty_str;
	
END gm_update_cntrl_str;
/
