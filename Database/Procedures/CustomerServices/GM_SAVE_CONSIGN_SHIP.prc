/* Formatted on 2009/02/20 11:03 (Formatter Plus v4.8.0) */
-- @"C:\database\Procedures\CustomerServices\GM_SAVE_CONSIGN_SHIP.prc"
-- this procedure is no longer called now. any changes to this procedure should be done to gm_pkg_cm_shipping_trans.gm_save_consign_ship

CREATE OR REPLACE PROCEDURE gm_save_consign_ship (
	p_consgid	   IN		t504_consignment.c504_consignment_id%TYPE
  , p_shipdt	   IN		VARCHAR2
  , p_shipcar	   IN		t504_consignment.c504_delivery_carrier%TYPE
  , p_shipmode	   IN		t504_consignment.c504_delivery_mode%TYPE
  , p_track 	   IN		t504_consignment.c504_tracking_number%TYPE
  , p_userid	   IN		t504_consignment.c504_last_updated_by%TYPE
  , p_freightamt   IN		t501_order.c501_ship_cost%TYPE
  , p_poid		   IN OUT	t501_order.c501_customer_po%TYPE
)
AS
--
	v_flag		   VARCHAR2 (2);
	v_msg		   VARCHAR2 (100);
	v_set_id	   VARCHAR2 (20);
	v_dist_id	   VARCHAR2 (20);
	v_dist_type    VARCHAR2 (20);
	v_inter_party_id t701_distributor.c101_party_intra_map_id%TYPE;
	v_post_id	   VARCHAR2 (20);
	v_party_id	   VARCHAR2 (20);
	v_purpose	   VARCHAR2 (20);
	v_status_fl    VARCHAR2 (1);
	v_void_fl	   VARCHAR2 (10);
	v_type		   t504_consignment.c504_type%TYPE;
	v_rqid		   t504_consignment.c520_request_id%TYPE;
	v_date_fmt	   VARCHAR2 (20);
	v_set_type t207_set_master.c207_type%TYPE;
--
	CURSOR cur_consign
	IS
		SELECT	 c205_part_number_id ID, SUM (c505_item_qty) qty, c505_item_price price
			FROM t505_item_consignment
		   WHERE c504_consignment_id = p_consgid AND TRIM (c505_control_number) IS NOT NULL
		GROUP BY c205_part_number_id, c505_item_price;
--
BEGIN
--

	--getting date format from rules table
	v_date_fmt := NVL(get_rule_value('DATEFMT','DATEFORMAT'),'MM/DD/YYYY'); 
	
-- Getting values to be used later in the procedure
	SELECT	   c504_void_fl, c504_type, c207_set_id, c701_distributor_id, c504_inhouse_purpose, c504_update_inv_fl
			 , NVL (c504_status_fl, 0), c520_request_id, get_distributor_inter_party_id (c701_distributor_id)
		  INTO v_void_fl, v_type, v_set_id, v_dist_id, v_purpose, v_flag
			 , v_status_fl, v_rqid, v_inter_party_id
		  FROM t504_consignment
		 WHERE c504_consignment_id = p_consgid
	FOR UPDATE;

	IF v_void_fl IS NOT NULL
	THEN
		-- Error message is Consignment already Voided.
		raise_application_error (-20793, '');
	END IF;

	IF v_inter_party_id IS NULL AND v_dist_id IS NOT NULL
	THEN
		gm_pkg_pd_partnumber.gm_val_release_for_sale (p_consgid, 4110);
	END IF;

	IF v_type = 4129
	THEN
		UPDATE t504_consignment
		   SET c504_status_fl = 4
			 , c504_last_updated_by = p_userid
			 , c504_last_updated_date = SYSDATE
		 WHERE c504_consignment_id = p_consgid;

		--
		RETURN;
	--
	END IF;

		 -- Starting validation for status flag. If status flag is not 3, then throw error.
	-- This might happen if someone rollsback a consignment and click back button and perform an update operation
	IF (v_status_fl <> 3)
	THEN
		-- Error message is  Set Consignment is not in Shipped Status and cannot be rolled back.
		raise_application_error (-20771, '');
	END IF;

	-- updating transaction with Shipping info
	UPDATE t504_consignment
	   SET c504_delivery_carrier = p_shipcar
		 , c504_delivery_mode = p_shipmode
		 , c504_tracking_number = p_track
		 , c504_ship_date = TO_DATE (p_shipdt, v_date_fmt)
		 , c504_status_fl = 4
		 , c504_last_updated_by = p_userid
		 , c504_last_updated_date = SYSDATE
		 , c504_type = DECODE (v_inter_party_id, NULL, c504_type, DECODE (c504_type, 4110, 40057, c504_type))
	 WHERE c504_consignment_id = p_consgid;

	UPDATE t520_request
	   SET c520_status_fl = '40'
		 , c520_required_date = CASE
								   WHEN c520_required_date > TRUNC (SYSDATE)
									   THEN TRUNC (SYSDATE)
								   ELSE c520_required_date
							   END
		 , c520_last_updated_by = p_userid
		 , c520_last_updated_date = SYSDATE
	 WHERE c520_request_id = v_rqid AND c520_void_fl IS NULL;

	SELECT t504.c504_type, DECODE(get_rule_value(t504.c504_type,'DEMO_CN_TYPE'),'Y',t207.c207_type,NULL)
	  INTO v_type, v_set_type
	  FROM t504_consignment t504, t207_set_master t207
	 WHERE t504.c504_consignment_id = p_consgid
	 AND t504.c207_set_id = t207.c207_set_id(+)
	 AND c207_void_fl(+) IS NULL;

	-- This is called for Item consignments to update inventory
	IF v_set_id IS NULL AND v_flag IS NULL
	THEN
		--
		gm_update_inventory (p_consgid, 'CITEM', 'MINUS', p_userid, v_msg);

		--
		UPDATE t504_consignment
		   SET c504_update_inv_fl = '1'
		 WHERE c504_consignment_id = p_consgid;
	--
	END IF;

	-- For posting Transfers - Sets
	IF v_set_id IS NOT NULL AND v_status_fl <> '4'
	THEN
		-- For Agent Transfers
		IF v_dist_id IS NOT NULL
		THEN

			v_party_id	:= v_dist_id;
			SELECT NVL(get_rule_value (v_set_type, 'BSDMO'),4814) INTO v_post_id FROM DUAL;
		-- For In-House Transfers
		ELSE
			v_post_id	:= get_rule_value (v_purpose, 'CON-SET-INHOUSE');
			v_party_id	:= 01;
		END IF;

		IF v_type = '40057'
		THEN
			SELECT NVL(get_rule_value (v_set_type, 'BSICDMO'),get_rule_value (50250, 'BSICT')) INTO v_post_id FROM DUAL;
		END IF;

		FOR rad_val IN cur_consign
		LOOP
			--
			gm_save_ledger_posting (v_post_id
								  , CURRENT_DATE
								  , rad_val.ID
								  , v_party_id
								  , p_consgid
								  , rad_val.qty
								  , rad_val.price
								  , p_userid
								   );
		--
		END LOOP;
	END IF;

	-- chk if the dist type is ICT
	IF v_type = 40057
	THEN
		--gm_update inventory to perform
		gm_pkg_op_ict.gm_op_sav_initiateict (p_consgid, 50250, p_poid, p_freightamt, p_userid, v_dist_id,v_purpose);
	END IF;
END gm_save_consign_ship;
/
