CREATE OR REPLACE PROCEDURE GM_UPDATE_SHIP_ITEM_ORDER
(
	p_ShipDt	  		IN VARCHAR2,
	p_ShipCar			IN t501_order.c501_delivery_carrier%TYPE,
	p_ShipMode			IN t501_order.c501_delivery_mode%TYPE,
	p_TrackNum			IN t501_order.c501_tracking_number%TYPE,
	p_OrdId				IN t501_order.c501_order_id%TYPE,
	p_UserId			IN t501_order.c501_last_updated_by%TYPE,
	p_ShipCost			IN t501_order.c501_ship_cost%TYPE,
	p_message 			OUT  VARCHAR2
)
AS
/***************************************************************************************************
  * This Procedure is to save ship info for orders.
    Changed on 10/5/2007 to remove the Exception block
  * Date   : 10/5/07
****************************************************************************************************/
v_PO	VARCHAR2(20);
v_Flag	VARCHAR2(1);
p_Msg	VARCHAR2(100);
p_InvFl	VARCHAR2(10);
BEGIN
--
	SELECT	c501_customer_po
	INTO	v_po
	FROM	t501_order
	WHERE	c501_order_id = p_ordid;
	--
	IF v_po IS NULL
	THEN
		v_flag := '2';
	ELSE
		v_flag := '3';
	END IF;
	--
	UPDATE	t501_order
	SET		c501_shipping_date		= TO_DATE(p_ShipDt,'MM/DD/YYYY'),
			c501_delivery_carrier	= p_ShipCar,
			c501_delivery_mode		= p_ShipMode,
			c501_tracking_number	= p_TrackNum,
			c501_status_fl			= v_Flag,
			c501_last_updated_by	= p_UserId,
			c501_last_updated_date	= SYSDATE,
			c501_ship_cost			= p_ShipCost
	WHERE	c501_order_id			= p_OrdId;
	--
	SELECT	c501_update_inv_fl
	INTO	p_invfl
	FROM	t501_order
	WHERE	c501_order_id = p_ordid;
	--
	IF p_InvFl IS NULL
	THEN
		UPDATE	t501_order
		SET		c501_update_inv_fl	= '1'
		WHERE	c501_order_id		= p_OrdId;
		--
		GM_UPDATE_INVENTORY(p_OrdId,'S','',p_UserId, p_Msg);
	END IF;
	--
END GM_UPDATE_SHIP_ITEM_ORDER;
/

