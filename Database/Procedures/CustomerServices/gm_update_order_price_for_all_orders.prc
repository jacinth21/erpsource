/* Formatted on 2010/04/14 11:35 (Formatter Plus v4.8.0) */
-- @ "C:\DATABASE\Procedures\CustomerServices\gm_update_order_price_for_all_orders.prc";

CREATE OR REPLACE PROCEDURE gm_update_order_price_for_all_orders (
	p_orderid	 IN 	  t501_order.c501_order_id%TYPE
  , p_str		 IN 	  VARCHAR2
  , p_shipcost	 IN 	  t501_order.c501_ship_cost%TYPE
  , p_total 	 IN 	  t501_order.c501_total_cost%TYPE
  , p_userid	 IN 	  t501_order.c501_last_updated_by%TYPE
  , p_errmsg	 OUT	  VARCHAR2
)
AS

/***************************************************************************************
 * Description           : This procedure is used to update price for noth Parent and child orders for PMT-38606
 *
 ****************************************************************************************/
 
	v_strlen	   NUMBER := NVL (LENGTH (p_str), 0);
	v_string	   VARCHAR2 (30000) := p_str;
	v_itemid	   VARCHAR2 (20);
	v_price 	   NUMBER (15, 2);
	v_substring    VARCHAR2 (1000);
	v_msg		   VARCHAR2 (1000);
	e_others	   EXCEPTION;
	v_id		   NUMBER;
	v_cnt		   NUMBER := 0;
	v_olditempr    NUMBER (15, 2);
	v_order_month  NUMBER;
	v_current_month NUMBER;
	v_total_cost	NUMBER (15, 2);
	v_unit_price   VARCHAR2(10);
	v_unit_price_adj   VARCHAR2(10);
	v_adj_code   VARCHAR2(10);
	v_acc_price  VARCHAR2(10);
	v_adj_code_per   VARCHAR2(10);
	v_part_num   t502_item_order.c205_part_number_id%TYPE;
	v_adj_code_val		t502_item_order.C502_ADJ_CODE_VALUE%TYPE;
	v_price_cur TYPES.cursor_type;
	v_account_id	t501_order.c704_account_id%TYPE;
	--
	v_usage_price_upd_fl VARCHAR2 (10);
	v_company_id t501_order.c1900_company_id%TYPE;
	
		 v_ref_id  t501_order.c501_ref_id%type;
	 v_ack_total_cost NUMBER (15, 2);
	
	--Fetch the Child orders 
	 CURSOR v_order_list
   IS
      SELECT t501.c501_order_id porderid
        FROM t501_order t501
      WHERE (t501.C501_ORDER_ID = p_orderid OR c501_parent_order_id = p_orderid)
        AND  NVL(t501.c901_order_type,2521) NOT IN (SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_GRP_ID = 'EXCHANGEPRICINGORD' )
     AND t501.c501_void_fl IS NULL
     AND t501.c501_delete_fl IS NULL;
	
BEGIN

/* no more need this. handled in the code now.
    SELECT TO_CHAR (c501_order_date, 'MM'), TO_CHAR (CURRENT_DATE, 'MM')
     INTO v_order_month, v_current_month
     FROM t501_order
    WHERE c501_order_id = p_orderid;

   IF v_order_month != v_current_month
   THEN
      raise_application_error ('-20310', '');
   --Cannot change pricing after the month has passed
   END IF;
  */ 
  FOR var_ordlist IN v_order_list
    LOOP   
  
     select c501_ref_id into v_ref_id from t501_order
  where c501_order_id = var_ordlist.porderid;
  
   IF v_strlen > 0
	THEN
		WHILE INSTR (v_string, '|') <> 0
		LOOP			
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_itemid	:= NULL;
			v_price 	:= NULL;
			v_unit_price := NULL;
			v_unit_price_adj := NULL;
			v_adj_code := NULL;
			v_itemid	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_price	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_unit_price	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_unit_price_adj	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);			
			v_adj_code 	:= v_substring;
					
			--issue 8350 Maintenance Request: '$0' discount on freight
			SELECT	   c502_item_price , c205_part_number_id
				  INTO v_olditempr , v_part_num
				  FROM t502_item_order
				 WHERE c502_item_order_id = v_itemid AND c502_void_fl IS NULL
			FOR UPDATE;

			-- Get the account id corresponding to the order
			BEGIN
				SELECT c704_account_id
					INTO v_account_id
				FROM t501_order
				WHERE c501_order_id = var_ordlist.porderid
					AND c501_void_fl IS NULL;
			EXCEPTION
			WHEN NO_DATA_FOUND THEN
				v_account_id := NULL;
			END;
				
			IF v_olditempr <> v_price
			THEN
				v_cnt		:= v_cnt + 1;
			END IF;
			-- Get the unit price/ list price based on the pricing parameter selection in account setup screen
			gm_pkg_sm_price_adj_rpt.gm_fch_acc_part_list_price(v_account_id, v_part_num, v_adj_code, v_price_cur);
			LOOP
				FETCH v_price_cur
	               INTO v_unit_price;
	            EXIT
	        		WHEN v_price_cur%NOTFOUND;
	        END LOOP;
	        v_unit_price := NVL(v_unit_price,0);
			v_adj_code_per := gm_pkg_sm_adj_txn.get_adj_code_value(var_ordlist.porderid,v_part_num,v_itemid, v_adj_code);
			v_adj_code_val	:= ((NVL(v_unit_price,0) - NVL(v_unit_price_adj,0)) * NVL(v_adj_code_per,0)/100);
			-- Get the unit price using below formula, net unit price + unit adjustment value + adjustment code value
	        v_acc_price := NVL(v_price,0) + NVL(v_unit_price_adj,0) + NVL(v_adj_code_val,0);

			UPDATE t502_item_order
			   SET c502_item_price = v_price	   
			     , c502_unit_price = v_acc_price
			     , c502_unit_price_adj_value = NVL(v_unit_price_adj,0)
			     , c901_unit_price_adj_code = DECODE(v_adj_code, '0', null,v_adj_code)
			     , C502_DO_UNIT_PRICE = v_unit_price
			     , C502_ADJ_CODE_VALUE = v_adj_code_val
			 WHERE c502_item_order_id = v_itemid;
			 
			 			 ----------------------------------------------
			 if v_ref_id is not null then
			 UPDATE t502_item_order
			   SET c502_item_price = v_price	   
			     , c502_unit_price = v_acc_price
			     , c502_unit_price_adj_value = NVL(v_unit_price_adj,0)
			     , c901_unit_price_adj_code = DECODE(v_adj_code, '0', null,v_adj_code)
			     , C502_DO_UNIT_PRICE = v_unit_price
			     , C502_ADJ_CODE_VALUE = v_adj_code_val
			 WHERE c501_order_id = v_ref_id
			 and c205_part_number_id = v_part_num;
			 end if;
			 ----------------------------------------------
		END LOOP;

		IF NVL(p_shipcost,0) < 0 THEN
			GM_RAISE_APPLICATION_ERROR('-20999','39','');
		END IF;

		gm_pkg_op_do_process_trans.gm_update_ship_cost (var_ordlist.porderid, NVL(p_shipcost,0), p_userid);
		
		select sum( nvl(c502_item_price,0) *  nvl(t502.c502_item_qty,0))
		  into v_total_cost 
		  from t502_item_order t502
		 where t502.c501_order_id = var_ordlist.porderid
		   and t502.c502_delete_fl is null
		   and t502.c502_void_fl is NULL;
		   
		    -----------------------------------------
		     if v_ref_id is not null then
		   select sum( nvl(c502_item_price,0) *  nvl(t502.c502_item_qty,0))
		  into v_ack_total_cost 
		  from t502_item_order t502
		 where t502.c501_order_id = v_ref_id
		   and t502.c502_delete_fl is null
		   and t502.c502_void_fl is NULL;
		   end if;
		   -----------------------------------------
		
			UPDATE t501_order
		   SET c501_total_cost = NVL(v_total_cost,0)
		 WHERE c501_order_id = var_ordlist.porderid;
	-- To update the item order usage - price info (only for Italy company)
	 SELECT c1900_company_id
	   INTO v_company_id
	   FROM t501_order
	  WHERE c501_order_id = var_ordlist.porderid
	    AND c501_void_fl IS NULL;
	--    
	 if v_ref_id is not null then
	UPDATE t501_order
		   SET c501_total_cost = NVL(v_ack_total_cost,0)
		 WHERE c501_order_id = v_ref_id;
		 end if ;
	v_usage_price_upd_fl := get_rule_value_by_company (v_company_id, 'UPD_ORD_USAGE_PRICE', v_company_id) ;
	--
		IF v_usage_price_upd_fl = 'Y' THEN
		    gm_pkg_cs_usage_lot_number.gm_upd_price (var_ordlist.porderid, p_userid) ;
		END IF; -- v_usage_price_upd_fl
	--	
	END IF;

	gm_save_status_details (var_ordlist.porderid, '', p_userid, NULL, CURRENT_DATE, 91177, 91102);

	-- For Discount Order Creation
	IF v_cnt > 0
	THEN
	
		-- PMT-28394 (To avoid the DS order creation - Job will create the DS order)
	
		-- update the rebate process flag - So, that job will pick the order and calculate discount

		gm_pkg_ac_order_rebate_txn.gm_update_rebate_fl (var_ordlist.porderid, NULL, NULL);
		gm_pkg_ac_order_rebate_txn.gm_void_discount_order (var_ordlist.porderid, p_userid);
		
	END IF;


END LOOP;
	--
	p_errmsg	:= 'success';
END gm_update_order_price_for_all_orders;
/
