CREATE OR REPLACE PROCEDURE GM_SPLIT_ITEM_ORDER
(
 		  p_str 	  IN	VARCHAR2,
		  p_OrdId  	  IN	T502_ITEM_ORDER.C501_ORDER_ID%TYPE,
		  p_ItemOrdId IN	T502_ITEM_ORDER.C502_ITEM_ORDER_ID%TYPE,
		  p_PartNum   IN	T502_ITEM_ORDER.C205_PART_NUMBER_ID%TYPE,
		  p_Price	  IN 	T502_ITEM_ORDER.C502_ITEM_PRICE%TYPE,
		  p_UserId	  IN	T501_ORDER.C501_LAST_UPDATED_BY%TYPE,
		  p_errmsg    OUT	VARCHAR2
)
AS
v_strlen     NUMBER          := NVL(LENGTH(p_str),0);
v_string     VARCHAR2(30000) := p_str;
v_ConNum	 t502_item_order.c502_control_number%TYPE;
v_Qty		 VARCHAR2(20);
v_item_order_id	 NUMBER;
v_substring  VARCHAR2(1000);
v_msg	     VARCHAR2(1000);
e_others	 EXCEPTION;
 v_list_price 	t502_item_order.c502_list_price%TYPE;
BEGIN
 	IF v_strlen > 0
    THEN
   		WHILE INSTR(v_string,'|') <> 0 LOOP
    		  v_substring := SUBSTR(v_string,1,INSTR(v_string,'|')-1);
    		  v_string    := SUBSTR(v_string,INSTR(v_string,'|')+1);
			  v_ConNum	  := NULL;
			  v_Qty		  := NULL;
			  v_ConNum 	  := SUBSTR(v_substring,1,INSTR(v_substring,',')-1);
			  v_substring := SUBSTR(v_substring,INSTR(v_substring,',')+1);
			  v_Qty	  	  := TO_NUMBER(v_substring);
			  SELECT S502_ITEM_ORDER.nextval INTO v_item_order_id FROM DUAL;
			  

			--PC-643 :: show list price on invoices
	  		SELECT TRIM(GET_PART_PRICE_LIST(p_PartNum,'L')) INTO v_list_price FROM DUAL;
			  
	          INSERT INTO T502_ITEM_ORDER (
			  		C502_ITEM_ORDER_ID,
					C501_ORDER_ID,
					C205_PART_NUMBER_ID,
					C502_ITEM_QTY,
					C502_CONTROL_NUMBER,
					C502_ITEM_PRICE,
					c502_list_price
			  )
			  VALUES (
			  		v_item_order_id,
			   		p_OrdId,
					p_PartNum,
					v_Qty,
					v_ConNum,
					p_Price,
					v_list_price
			  );
		END LOOP;
        UPDATE T502_ITEM_ORDER
  		SET
		 	C502_VOID_FL = 'Y'
		WHERE
			C502_ITEM_ORDER_ID = p_ItemOrdId;
		UPDATE T501_ORDER
 		SET
	 	   C501_STATUS_FL = '2',
		   C501_LAST_UPDATED_BY = p_UserId,
		   C501_LAST_UPDATED_DATE = SYSDATE
		WHERE
			C501_ORDER_ID = p_OrdId;
	ELSE
	    v_msg := 'p_str is empty';
		RAISE e_others;
	END IF;
	v_msg := 'Success';
	COMMIT WORK;
    p_errmsg  := v_msg;
EXCEPTION
	  WHEN e_others THEN
	  ROLLBACK WORK;
	  p_errmsg  := v_msg;
	  WHEN OTHERS THEN
	  ROLLBACK WORK;
	  p_errmsg  := SQLERRM;
NULL;
END GM_SPLIT_ITEM_ORDER;
/

