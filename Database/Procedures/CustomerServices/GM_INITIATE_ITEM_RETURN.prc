CREATE OR REPLACE PROCEDURE gm_initiate_item_return (
   p_type             IN       t506_returns.c506_type%TYPE,
   p_reason           IN       t506_returns.c506_reason%TYPE,
   p_distid           IN       VARCHAR2,
   p_comments         IN       t506_returns.c506_comments%TYPE,
   p_expdate          IN       VARCHAR2,
   p_orderid          IN       t506_returns.c501_order_id%TYPE,
   p_userid           IN       t506_returns.c506_created_by%TYPE,
   p_parent_orderid   IN       t506_returns.c501_order_id%TYPE,
   p_empname          IN       t101_user.c101_user_id%TYPE,
   p_raid             OUT      VARCHAR2,
   p_message          OUT      VARCHAR2
)
AS
--
   v_ra_id       NUMBER;
   v_id          NUMBER;
   v_string      VARCHAR2 (20);
   v_id_string   VARCHAR2 (20);
   v_price       VARCHAR2 (20);
   v_date        VARCHAR2 (10);
   v_empname     NUMBER;
   v_status	CHAR(1) := 0;
   v_ret_date	DATE;
   v_rep_date	DATE;
   v_company_id  t1900_company.c1900_company_id%TYPE;
   v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
--
BEGIN


	SELECT NVL(GET_COMPID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL')), NVL(GET_PLANTID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_PLANT','ONEPORTAL'))
          INTO v_company_id, v_plant_id
          FROM DUAL;
          
   --
   -- Checking if EmpName is 0. If it is Zero, make it as NULL
   IF p_empname = 0
   THEN
      v_empname := NULL;
   ELSE
      v_empname := p_empname;
   END IF;

   SELECT s506_return.NEXTVAL
     INTO v_ra_id
     FROM DUAL;
      
   --
   IF LENGTH (v_ra_id) = 1
   THEN
      v_id_string := '0' || v_ra_id;
   ELSE
      v_id_string := v_ra_id;
   END IF;

   --
   SELECT 'GM-RA-' || v_id_string
     INTO v_string
     FROM DUAL;

   --
   p_raid := v_string;

   IF LENGTH (p_expdate) < 1
   THEN
      v_date := TO_CHAR (SYSDATE + 4, 'mm/dd/yyyy');
   ELSE
      v_date := p_expdate;
   END IF;

   --
   IF p_type = '3300' OR p_distid = '01' THEN
       -- Mark the order as duplicate order or as Item Return consignment
      gm_order_return (p_orderid,
                       p_type,
                       p_reason,
                       p_parent_orderid                      
                      );
      --
      -- If insert throws error return the procedure with error message
      IF (p_message IS NOT NULL) THEN
         RETURN;
      END IF;
      --
      IF p_reason = '3317' THEN
	v_status := 1;
	v_ret_date := SYSDATE;
	v_rep_date := SYSDATE;
      END IF;
      --
      INSERT INTO t506_returns
                  (c506_rma_id, c704_account_id, c501_order_id, c506_type,
                   c506_reason, c506_comments, c506_expected_date,
                   c506_status_fl, c506_created_by, c506_created_date,
                   c101_user_id, c506_return_date, c501_reprocess_date, c1900_company_id, c5040_plant_id
                  )
           VALUES (v_string, p_distid, p_orderid, p_type,
                   p_reason, p_comments, TO_DATE (v_date, 'mm/dd/yyyy'),
                   v_status, p_userid, SYSDATE,
                   v_empname, v_ret_date, v_rep_date , v_company_id, v_plant_id
                  );
   ELSE
      INSERT INTO t506_returns
                  (c506_rma_id, c701_distributor_id, c506_type, c506_reason,
                   c506_comments, c506_expected_date, c506_status_fl,
                   c506_created_by, c506_created_date, c1900_company_id, c5040_plant_id
                  )
           VALUES (v_string, p_distid, p_type, p_reason,
                   p_comments, TO_DATE (v_date, 'mm/dd/yyyy'), v_status,
                   p_userid, SYSDATE, v_company_id, v_plant_id
                  );
   END IF;
--
   COMMIT;
   RETURN;
END gm_initiate_item_return;
/
