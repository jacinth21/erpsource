/* Formatted on 2010/05/14 14:40 (Formatter Plus v4.8.0) */
--@"C:\database\Procedures\CustomerServices\gm_cs_sav_loaner_ship.prc";

CREATE OR REPLACE PROCEDURE gm_cs_sav_loaner_ship (
	p_conid 		  IN	   t504_consignment.c504_consignment_id%TYPE
  , p_conto 		  IN	   t504a_loaner_transaction.c901_consigned_to%TYPE
  , p_contoid		  IN	   t504a_loaner_transaction.c504a_consigned_to_id%TYPE
  , p_expretdt		  IN	   VARCHAR2
  , p_userid		  IN	   t504_consignment.c504_last_updated_by%TYPE
  , p_req_id		  IN	   VARCHAR2
  , p_repid 		  IN	   t504a_loaner_transaction.c703_sales_rep_id%TYPE
  , p_accid 		  IN	   t504a_loaner_transaction.c704_account_id%TYPE
  , p_shipto		  IN	   t907_shipping_info.c901_ship_to%TYPE
  , p_shiptoid		  IN	   t907_shipping_info.c907_ship_to_id%TYPE
  , p_shipcarr		  IN	   t907_shipping_info.c901_delivery_carrier%TYPE
  , p_shipmode		  IN	   t907_shipping_info.c901_delivery_mode%TYPE
  , p_addressid 	  IN	   t907_shipping_info.c106_address_id%TYPE
  , p_shipid		  OUT	   VARCHAR2
  , p_loan_trans_id   OUT	   VARCHAR2
)
AS
/***************************************************************************************
 * Description			 : This procedure is modified to only Save InHouse Sets info
 *
 ****************************************************************************************/
	v_distid	   VARCHAR2 (20);
	v_acctid	   VARCHAR2 (20);
	v_status	   CHAR (1);
	v_dateformat   varchar2(100);
	v_company_id  t1900_company.c1900_company_id%TYPE;
	v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
--
BEGIN
--
	
	SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
     	INTO v_company_id, v_plant_id
     	FROM DUAL;
	
	IF p_conto = 50170
	THEN   -- If ConsignTo is a Distributor
		v_distid	:= p_contoid;
	ELSIF p_conto = 50171
	THEN   -- If ConsignTo is an Account or Employee
		v_acctid	:= p_contoid;
	END IF;

--
--
	SELECT	   c504a_status_fl
		  INTO v_status
		  FROM t504a_consignment_loaner
		 WHERE c504_consignment_id = p_conid
	FOR UPDATE;

--
	IF v_status <> '0'
	THEN
		raise_application_error (-20791, '');
	END IF;

--
	UPDATE t504_consignment
	   SET c701_distributor_id = v_distid
		 , c704_account_id = v_acctid
		 , c504_last_updated_by = p_userid
		 , c504_last_updated_date = CURRENT_DATE
	 WHERE c504_consignment_id = p_conid AND c504_void_fl IS NULL;

--
	SELECT s504a_loaner_trans_id.NEXTVAL
	  INTO p_loan_trans_id
	  FROM DUAL;
    select get_compdtfmt_frm_cntx() into v_dateformat from dual;
	INSERT INTO t504a_loaner_transaction
				(c504a_loaner_transaction_id, c504_consignment_id, c504a_loaner_dt, c504a_expected_return_dt
			   , c901_consigned_to, c504a_consigned_to_id, c504a_created_by, c504a_created_date, c703_sales_rep_id
			   , c704_account_id,c1900_company_id,c5040_plant_id
				)
		 VALUES (p_loan_trans_id, p_conid, TRUNC (CURRENT_DATE), TO_DATE (p_expretdt, v_dateformat)
			   , p_conto, p_contoid, p_userid, CURRENT_DATE, DECODE (p_repid, 0, NULL, p_repid)
			   , DECODE (p_accid, 0, NULL, p_accid),v_company_id,v_plant_id
				);

--
/*	gm_cs_sav_ship_info (p_conid
						, 50182
						, p_shipto
						, p_shiptoid
						, TO_CHAR (CURRENT_DATE, 'mm/dd/yyyy')
						, p_shipcarr
						, p_shipmode
						, NULL
						, NULL
						, p_userid
						, 'INSERT'
						, NULL
						);
*/
--
	gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status (p_conid, 10, p_userid) ;-- Pending Shipping
	UPDATE t504a_consignment_loaner
	   SET c504a_loaner_dt = TRUNC (CURRENT_DATE)
		 , c504a_expected_return_dt = TO_DATE (p_expretdt, v_dateformat)
		 , c504a_last_updated_by = p_userid
		 , c504a_last_updated_date = CURRENT_DATE 
	 WHERE c504_consignment_id = p_conid;

--
/*
	IF p_req_id IS NOT NULL
	THEN
		UPDATE t526_product_request_detail
		   SET c526_qty_requested = c526_qty_requested - 1
		 WHERE c526_product_request_detail_id = p_req_id;
	END IF;
*/
--
	gm_pkg_cm_shipping_trans.gm_sav_shipping (p_conid
											, 50182   --loaners
											, p_shipto
											, p_shiptoid
											, p_shipcarr
											, p_shipmode
											, NULL
											, p_addressid
											, NULL
											, p_userid
											, p_shipid
											 );
--for loaner alone doing controlling forcefully as in shipping the status needs to change from 15 to 30
	gm_pkg_cm_shipping_trans.gm_sav_release_shipping (p_conid, 50182, p_userid, '');
END gm_cs_sav_loaner_ship;
/
