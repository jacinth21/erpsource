/* Formatted on 2011/04/12 12:26 (Formatter Plus v4.8.0) */
--@"C:\database\Procedures\CustomerServices\GM_SAVE_DSORDER.prc"

CREATE OR REPLACE PROCEDURE gm_save_dsorder (
	p_orderid	IN	 t501_order.c501_order_id%TYPE
)
AS
	v_gpbid 	   VARCHAR2 (20);
	v_dsorderid    VARCHAR2 (20);
	v_ord_id	   VARCHAR2 (20);
	v_acctid	   VARCHAR2 (20);
	v_omode 	   NUMBER;
	v_name		   t501_order.c501_received_from%TYPE;
	v_rep_id	   NUMBER;
	v_shipto	   NUMBER;
	v_shipto_id    VARCHAR2 (20);
	v_cust_po	   t501_order.c501_customer_po%TYPE;
	v_total 	   NUMBER;
	v_userid	   VARCHAR2 (10);
	v_shipcarr	   NUMBER;
	v_shipmode	   NUMBER;
	v_shipcost	   NUMBER; 
	v_invoice_id   VARCHAR2 (20);
	v_rma_id	   VARCHAR2 (20);
	v_qty_type	   NUMBER;
	v_percentage   NUMBER;
	v_price 	   NUMBER;
	v_orderid	   t501_order.c501_order_id%TYPE;
	v_order_type   t501_order.c901_order_type%TYPE;
	v_parent_ord_id t501_order.c501_parent_order_id%TYPE;
	v_ds_orderid   t501_order.c501_order_id%TYPE;
	v_attribute_type	   t205d_part_attribute.c901_attribute_type%TYPE;
	v_company_id    t501_order.c1900_company_id%TYPE;
	v_plant_id      t501_order.c5040_plant_id%TYPE;
	v_division		t501_order.c1910_division_id%TYPE;
	v_gpo 			t704d_account_affln.c101_gpo%TYPE;
	v_idn		  	t704d_account_affln.c101_idn%TYPE;
	v_contract		t704d_account_affln.c901_contract%TYPE;
	v_admin_fl		t704d_account_affln.c901_admin_flag%TYPE;
	v_admin_fee		t704d_account_affln.c704d_admin_fee%TYPE;
	v_part_map_cnt  NUMBER;
	v_part_flag		VARCHAR2(1);
	v_dealer_id  t501_order.c101_dealer_id%TYPE;
	v_surgery_dt t501_order.c501_surgery_date%TYPE;
	v_contract_value T704d_Account_Affln.c901_contract%TYPE;
	--
	v_master_parent_ord_id t501_order.c501_order_id%TYPE;
    v_acc_cmp_id    t704_account.c704_account_id%TYPE;
    v_user_plant_id   t501_order.c5040_plant_id%TYPE;
BEGIN
	
	--Getting company id from context.   
    SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM dual;
	--Getting plant id from context.
	SELECT  get_plantid_frm_cntx()
		INTO  v_plant_id
	FROM DUAL;
	
	v_qty_type	:= -1;

	SELECT c704_account_id, c501_receive_mode, c501_received_from, c703_sales_rep_id, c501_ship_to, c501_ship_to_id
		 , c501_customer_po, c501_total_cost, c501_created_by, c501_delivery_carrier, c501_delivery_mode
		 , c501_ship_cost, c503_invoice_id, c506_rma_id, c901_order_type, c501_parent_order_id, c101_dealer_id, c501_surgery_date
	  INTO v_acctid, v_omode, v_name, v_rep_id, v_shipto, v_shipto_id
		 , v_cust_po, v_total, v_userid, v_shipcarr, v_shipmode
		 , v_shipcost, v_invoice_id, v_rma_id, v_order_type, v_parent_ord_id, v_dealer_id, v_surgery_dt
	  FROM t501_order
	 WHERE c501_order_id = p_orderid;

	-- Get the division
	v_division := gm_pkg_sm_mapping.get_salesrep_division(v_rep_id);
		  
	IF v_order_type IN (2529, 2522)
	THEN   -- wrong order/duplicate order/price adjustment
		v_orderid	:= v_parent_ord_id;
	--v_qty_type	:= 1; -- -ve Adjustment are being created . hence commented
	END IF;
	
	BEGIN
		SELECT C101_GPO,
		  C101_IDN,
		  C901_CONTRACT,
		  C901_ADMIN_FLAG,
		  C704D_ADMIN_FEE
		  INTO
		  v_gpo,
		  v_idn,
		  v_contract,
		  v_admin_fl,
		  v_admin_fee
		FROM t704d_account_affln
		WHERE c704_account_id = v_acctid
		AND c704d_void_fl IS NULL;
	EXCEPTION WHEN NO_DATA_FOUND THEN 
		RETURN;
	END;

	IF NVL(v_admin_fl,'-9999') = '7003' THEN -- GROUP PRICE BOOK
	 	v_gpbid 	:= get_account_gpo_id (v_acctid);
	 	v_attribute_type := get_rule_value (v_gpbid, 'ATTRVAL');
		v_part_flag := 'Y';

	ELSIF NVL(v_admin_fl,'-9999') = '7011' THEN --GPO
		v_attribute_type := get_rule_value (v_gpo, 'ATTRVAL');
				
		SELECT count(1) into v_part_map_cnt
		  FROM t205d_part_attribute
		WHERE c901_attribute_type = v_attribute_type 
		  AND c205d_attribute_value = 'Y' 
		  AND c205d_void_fl IS NULL; 
		  
		IF v_part_map_cnt > 0  THEN
			v_part_flag := 'Y';
		ELSE
			v_part_flag := 'N';
			v_attribute_type := NULL;
		END IF;
	END IF;

	-- For any new GPO which is added to rule table will work here.
	--v_percentage := NVL (GET_ACCOUNT_ATTRB_VALUE(v_acctid,'106005'), 0); --Admin Fee
	v_percentage := NVL(v_admin_fee, 0);
	
	IF v_percentage <> 0   -- if there is no percentage for any gpo id then Return.
	THEN
		-- to get the parent order id
			SELECT get_parent_order_id(p_orderid)
			 INTO v_master_parent_ord_id
			FROM dual;
		-- to get the Order amount	
		SELECT SUM (NVL (TO_NUMBER (c502_item_qty) * TO_NUMBER (c502_item_price), 0))
		  INTO v_total
		  FROM t502_item_order t502, t501_order t501
		 WHERE t501.c501_order_id = t502.C501_ORDER_ID
  			AND (t501.c501_order_id = v_master_parent_ord_id
		 	OR t501.c501_parent_order_id  = v_master_parent_ord_id)
		    AND NVL (t501.c901_order_type, - 9999)          <> 2535
		    AND NVL (t501.c901_order_type, - 9999) NOT IN
		    (
		         SELECT C906_RULE_VALUE FROM v901_ORDER_TYPE_GRP
		    )	
		   AND t502.c502_void_fl IS NULL
		   AND t501.c501_void_fl         IS NULL
		   AND t501.c501_delete_fl       IS NULL
		   AND (c205_part_number_id IN (SELECT c205_part_number_id
										 FROM t205d_part_attribute
										WHERE c901_attribute_type = v_attribute_type 
										AND c205d_attribute_value = 'Y'
										AND c205d_void_fl IS NULL)
			---- 92344 is used to Calculate rebate for GPO on all parts or on total sales for each DO. If the attr type does not match , it means that specific list of part is used from part attr table
			OR c205_part_number_id = DECODE (v_attribute_type, '92344', c205_part_number_id, '-99999')
			OR v_part_flag = 'N');

		--Calculation for t502 table record.
		IF v_rma_id IS NOT NULL THEN
		--2528	Credit Memo
		--2529	Sales Adjustment
		--2522  wrong order/duplicate order
			v_price 	:= ROUND (NVL (v_total, 0) * v_percentage / 100, 2);
		ELSE
			v_price 	:= ROUND (NVL (ABS(v_total), 0) * v_percentage / 100, 2);
		END IF;

		IF v_price = 0
		THEN
			RETURN;
		END IF;
	
		BEGIN
			SELECT	 t501.c501_order_id
				INTO v_ds_orderid
				FROM t501_order t501
			   WHERE t501.c501_parent_order_id = v_master_parent_ord_id 
			     AND t501.c901_order_type = 2535 
			     AND t501.c501_void_fl IS NULL 
			     AND (t501.c901_ext_country_id IS NULL 
			     	  OR t501.c901_ext_country_id  in (select country_id from v901_country_codes)
			     	 )			   
			GROUP BY t501.c501_order_id;
	
			UPDATE t501_order
			   SET c501_total_cost = NVL (v_price, 0) * v_qty_type
				 , c501_last_updated_by = v_userid
				 , c501_last_updated_date = CURRENT_DATE
			 WHERE c501_order_id = v_ds_orderid;
	
			UPDATE t502_item_order
			   SET c502_item_price = NVL (v_price, 0)
			     , c502_unit_price = NVL (v_price, 0)
			     , C502_UNIT_PRICE_ADJ_VALUE = 0
			     , C502_DO_UNIT_PRICE = NVL (v_price, 0)
		         , C502_ADJ_CODE_VALUE = 0
		         , c502_corp_item_price = NULL
			 WHERE c501_order_id = v_ds_orderid 
			   AND c205_part_number_id = 'GPODISC';
			 
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				SELECT s501_order.NEXTVAL
				  INTO v_ord_id
				  FROM DUAL;
	
				v_dsorderid := 'GM' || v_ord_id || 'DS';
				v_orderid	:= p_orderid;
				-- to get the parent order id
				v_orderid := get_parent_order_id(v_orderid);
				
				--getting contract value from account
	             SELECT GET_ACCT_CONTRACT(v_acctid) 
	             INTO v_contract_value 
	            FROM DUAL;
	       BEGIN
	     --getting company id and plant id mapped from account
           SELECT c1900_company_id,gm_pkg_op_plant_rpt.get_default_plant_for_comp(gm_pkg_it_user.get_default_party_company(v_userid))
           INTO v_acc_cmp_id, v_user_plant_id  
           FROM T704_account 
           WHERE c704_account_id = v_acctid 
           AND c704_void_fl IS NULL; 
           EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
            v_acc_cmp_id := NULL;
            v_user_plant_id := NULL;
           END;
				--2535 is for Discount Order
				INSERT INTO t501_order
							(c501_order_id, c704_account_id, c501_receive_mode, c501_received_from, c703_sales_rep_id
						   , c501_ship_to, c501_ship_to_id, c501_comments, c501_total_cost, c501_order_date
						   , c501_created_by, c501_created_date, c501_status_fl, c501_order_date_time, c901_order_type
						   , c501_delivery_carrier, c501_delivery_mode, c501_ship_cost, c501_parent_order_id, c1900_company_id, c5040_plant_id, c1910_division_id
						   , c101_dealer_id, c501_surgery_date,c901_contract
							)
					 VALUES (v_dsorderid, v_acctid, v_omode, v_name, v_rep_id
						   , v_shipto, v_shipto_id, 'Discount Order for DO - ' || p_orderid, 0, TRUNC (CURRENT_DATE)
						   , v_userid, CURRENT_DATE, '3', CURRENT_DATE, 2535
						   , v_shipcarr, v_shipmode, 0, v_orderid, nvl(v_company_id,v_acc_cmp_id), nvl(v_plant_id,v_user_plant_id), v_division
						   , v_dealer_id ,v_surgery_dt,v_contract_value
							);
	
				INSERT INTO t502_item_order
							(c502_item_order_id, c501_order_id, c205_part_number_id, c502_item_qty, c502_control_number
						   , c502_item_price, c901_type, c502_unit_price, C502_UNIT_PRICE_ADJ_VALUE , C502_DO_UNIT_PRICE , C502_ADJ_CODE_VALUE
							)
					 VALUES (s502_item_order.NEXTVAL, v_dsorderid, 'GPODISC', v_qty_type, 'NOC#'
						   , v_price, 50300, v_price, 0 , v_price , 0
							);
	
				UPDATE t501_order
				   SET c501_total_cost = NVL (v_price, 0) * v_qty_type
					 , c501_last_updated_by = v_userid
					 , c501_last_updated_date = CURRENT_DATE
				 WHERE c501_order_id = v_dsorderid;
		END;
	END IF; -- v_percentage <>0
	--to save the corresponding order attribute value
	gm_pkg_sav_snapshot_info.gm_sav_snapshot_info('ORDER',v_dsorderid,'SNAPSHOT_INFO',v_company_id,'1200',v_userid);
END;
/
