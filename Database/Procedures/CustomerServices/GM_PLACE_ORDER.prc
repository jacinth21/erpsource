CREATE OR REPLACE PROCEDURE GM_PLACE_ORDER
(
	   p_OrdId				IN T501_ORDER.C501_ORDER_ID%TYPE,
	   p_AccId 			    IN T501_ORDER.C704_ACCOUNT_ID%TYPE,
	   p_RepId				IN T501_ORDER.C703_SALES_REP_ID%TYPE,
 	   p_ShipTo				IN T501_ORDER.C501_SHIP_TO%TYPE,
	   p_PO					IN T501_ORDER.C501_CUSTOMER_PO%TYPE,
	   p_Total				IN T501_ORDER.C501_TOTAL_COST%TYPE,
	   p_UserId				IN T501_ORDER.C501_CREATED_BY%TYPE,
	   p_OMode				IN T501_ORDER.C501_RECEIVE_MODE%TYPE,
	   p_PName				IN T501_ORDER.C501_RECEIVED_FROM%TYPE,
	   p_message 			OUT  VARCHAR2
)
AS
BEGIN
              INSERT INTO T501_ORDER (
			  		C501_ORDER_ID,
					C704_ACCOUNT_ID,
					C703_SALES_REP_ID,
					C501_CUSTOMER_PO,
					C501_SHIP_TO,
					C501_ORDER_DATE,
					C501_TOTAL_COST,
					C501_CREATED_BY,
					C501_CREATED_DATE,
					C501_STATUS_FL,
					C501_RECEIVE_MODE,
					C501_RECEIVED_FROM,
					C501_ORDER_DATE_TIME
			  )
			  VALUES (
			   		p_OrdId,
					p_AccId,
					p_RepId,
					p_PO,
					p_ShipTo,
					trunc(SYSDATE),
					p_Total,
					p_UserId,
					SYSDATE,
					'1',
					p_OMode,
					p_PName,
					SYSDATE
			  );
			  EXCEPTION WHEN OTHERS
      	 	  THEN
                       p_message := SQLERRM;
              RETURN;
END GM_PLACE_ORDER;
/

