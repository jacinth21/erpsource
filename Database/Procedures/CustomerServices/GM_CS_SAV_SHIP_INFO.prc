CREATE OR REPLACE PROCEDURE GM_CS_SAV_SHIP_INFO
(
 p_RefId  IN t907_shipping_info.c907_ref_id%TYPE
 ,p_Source IN t907_shipping_info.c901_source%TYPE
 ,p_ShipTo IN t907_shipping_info.c901_ship_to%TYPE
 ,p_ShipToId IN t907_shipping_info.c907_ship_to_id%TYPE
 ,p_ShipDt IN VARCHAR2
 ,p_ShipCarr IN t907_shipping_info.c901_delivery_carrier%TYPE
 ,p_ShipMode IN t907_shipping_info.c901_delivery_mode%TYPE
 ,p_TrackNo IN t907_shipping_info.c907_tracking_number%TYPE
 ,p_VoidFl IN t907_shipping_info.c907_void_fl%TYPE
 ,p_UserId IN t907_shipping_info.c907_last_updated_by%TYPE
 ,p_Mode  IN VARCHAR2
 ,p_ShipId IN NUMBER
)
 AS
/***************************************************************************************
 * Description           : This procedure is used to Save shipping info
 *
 ****************************************************************************************/
--
BEGIN
--
 IF p_Mode = 'INSERT' THEN
  INSERT INTO t907_shipping_info
  (
   c907_shipping_id
   ,c907_ref_id
   ,c901_source
   ,c901_ship_to
   ,c907_ship_to_id
   ,c907_ship_to_dt
   ,c901_delivery_carrier
   ,c901_delivery_mode
   ,c907_created_by
   ,c907_created_date
  )
  VALUES
  (
   s907_ship_id.NEXTVAL
   ,p_RefId
   ,p_Source
   ,p_ShipTo
   ,p_ShipToId
   ,TO_DATE(p_ShipDt,'MM/DD/YYYY')
   ,p_ShipCarr
   ,p_ShipMode
   ,p_UserId
   ,SYSDATE

  );
--
 ELSIF p_Mode = 'UPDATE' THEN
  UPDATE t907_shipping_info
  SET
   c901_ship_to   = p_ShipTo
   ,c907_ship_to_id  = p_ShipToId
   ,c907_ship_to_dt  = TO_DATE(p_ShipDt,'MM/DD/YYYY')
   ,c901_delivery_carrier = p_ShipCarr
   ,c901_delivery_mode  = p_ShipMode
   ,c907_tracking_number = p_TrackNo
   ,c907_void_fl   = p_VoidFl
   ,c907_last_updated_by = p_UserId
   ,c907_last_updated_date = SYSDATE
  WHERE c907_shipping_id  = p_ShipId;
--
 END IF;
--
END GM_CS_SAV_SHIP_INFO;
/

