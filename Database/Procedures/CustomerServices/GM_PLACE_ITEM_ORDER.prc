CREATE OR REPLACE PROCEDURE GM_PLACE_ITEM_ORDER
(
	   p_OrdId				IN T502_ITEM_ORDER.C501_ORDER_ID%TYPE,
	   p_PartNum			IN T502_ITEM_ORDER.C205_PART_NUMBER_ID%TYPE,
	   p_Qty				IN T502_ITEM_ORDER.C502_ITEM_QTY%TYPE,
	   p_Price				IN T502_ITEM_ORDER.C502_ITEM_PRICE%TYPE,
	   p_message 			OUT  VARCHAR2
)
AS
v_Date VARCHAR2(12);
v_item_order_id VARCHAR2(20);
v_stock NUMBER;
v_newStock NUMBER;
BEGIN
	 SELECT S502_ITEM_ORDER.nextval INTO v_item_order_id FROM DUAL;
              INSERT INTO T502_ITEM_ORDER (
			  		C502_ITEM_ORDER_ID,
					C501_ORDER_ID,
					C205_PART_NUMBER_ID,
					C502_ITEM_QTY,
					C502_ITEM_PRICE
			  )
			  VALUES (
			  		v_item_order_id,
			   		p_OrdId,
					p_PartNum,
					p_Qty,
					p_Price
			  );
			  EXCEPTION WHEN OTHERS
      	 	  THEN
                       p_message := SQLERRM;
              RETURN;
END GM_PLACE_ITEM_ORDER;
/

