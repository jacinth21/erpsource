/* Formatted on 2010/12/14 10:16 (Formatter Plus v4.8.0) */
--@"C:\Database\Procedures\CustomerServices\gm_save_item_ord_backorder.prc";

CREATE OR REPLACE PROCEDURE gm_save_item_ord_backorder (
	p_orderid	 IN 	  t501_order.c501_order_id%TYPE
  , p_pnum		 IN 	  t502_item_order.c205_part_number_id%TYPE
  , p_bo_qty	 IN 	  NUMBER
  , p_price 	 IN 	  NUMBER
  , p_cap_fl	 IN 	  CHAR
  , p_refid 	 IN 	  VARCHAR2
  , p_out_boid	 OUT	  VARCHAR2
)
AS
--
	e_others	   EXCEPTION;
	v_id		   VARCHAR2 (30);
	v_stock 	   NUMBER;
	v_total 	   NUMBER (15, 2);
	v_shipid	   VARCHAR2 (20);
	v_shipto	   t907_shipping_info.c901_ship_to%TYPE;
	v_shiptoid	   t907_shipping_info.c907_ship_to_id%TYPE;
	v_addressid    t907_shipping_info.c106_address_id%TYPE;
	v_userid	   t501_order.c501_created_by%TYPE;
	v_attnto 	   t907_shipping_info.c907_override_attn_to%TYPE;
	v_parentorderid  t501_order.c501_order_id%TYPE;
	v_shipcarr	 	t907_shipping_info.c901_delivery_carrier%TYPE;
	v_shipmode	 	t907_shipping_info.c901_delivery_mode%TYPE;
	v_contract      T704d_Account_Affln.c901_contract%TYPE;
	v_acctid	    t501_order.c704_account_id%TYPE;
	v_company_id  t1900_company.c1900_company_id%TYPE;
	v_list_price 	t502_item_order.c502_list_price%TYPE;
    v_acc_cmp_id    t704_account.c704_account_id%TYPE;
    v_user_plant_id   t501_order.c5040_plant_id%TYPE;
--
BEGIN
--
	v_id		:= get_next_order_id (NULL);
	
	--getting the company id
	  select get_compid_frm_cntx()
		into v_company_id
		from dual;
	-- fetch parent order id to update backorder parent
	BEGIN
     SELECT c501_parent_order_id,c704_account_id
       INTO v_parentorderid,v_acctid
       FROM t501_order
      WHERE c501_order_id   = p_orderid
        AND c501_void_fl   IS NULL
        AND c501_delete_fl IS NULL;
	EXCEPTION
	WHEN NO_DATA_FOUND THEN
	    v_parentorderid := NULL;
	END;

	--getting contract value from account
	  SELECT GET_ACCT_CONTRACT(v_acctid) 
	  INTO v_contract 
	  FROM DUAL; 
	  
	  --PC-643 :: show list price on invoices
	  SELECT TRIM(GET_PART_PRICE_LIST(p_pnum,'L')) INTO v_list_price FROM DUAL;
	 
	--
	
		BEGIN
		SELECT c901_ship_to, c907_ship_to_id,c901_delivery_carrier,c901_delivery_mode, c106_address_id, c907_created_by, c907_override_attn_to
		  INTO v_shipto, v_shiptoid,v_shipcarr,v_shipmode, v_addressid, v_userid, v_attnto
		  FROM t907_shipping_info
		 WHERE c907_ref_id = p_orderid AND c901_source = 50180 AND c907_void_fl IS NULL;
	  EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_shipto	:= NULL;
			v_shiptoid	:= NULL;
			v_addressid := NULL;
    	END;
    	BEGIN
	     --getting company id and plant id mapped from account
           SELECT c1900_company_id,gm_pkg_op_plant_rpt.get_default_plant_for_comp(gm_pkg_it_user.get_default_party_company(v_userid))
           INTO v_acc_cmp_id, v_user_plant_id  
           FROM T704_account 
           WHERE c704_account_id = v_acctid 
           AND c704_void_fl IS NULL; 
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
            v_acc_cmp_id := NULL;
            v_user_plant_id := NULL;
        END;
	--PMT-42210 Order date should be parent order date for orders moved to Back Order from Modify Order Screen
	--Replace the existing date (CURRENT_DATE) by getting the Order date from the parent order
	--c501_customer_po_date as field for PC-3880 New field in record PO Date in GM Italy
	INSERT INTO t501_order
				(c501_order_id, c704_account_id, c501_receive_mode, c501_received_from, c703_sales_rep_id
			   , c501_ship_to, c501_ship_to_id, c501_comments, c501_customer_po, c501_total_cost, c501_order_date
			   , c501_created_by, c501_created_date, c501_status_fl, c501_order_date_time, c501_parent_order_id
			   , c901_order_type, c1900_company_id, c5040_plant_id, c1910_division_id, c101_dealer_id, c501_surgery_date,c901_contract,c501_customer_po_date)
		SELECT v_id, c704_account_id, c501_receive_mode, c501_received_from, c703_sales_rep_id, c501_ship_to
			 , c501_ship_to_id, c501_comments, c501_customer_po, (p_bo_qty * p_price), TRUNC(c501_order_date)
			 , c501_created_by, CURRENT_DATE, 0, c501_order_date_time, NVL(v_parentorderid,p_orderid)
			 , 2525,  nvl(c1900_company_id,v_acc_cmp_id), nvl(c5040_plant_id,v_user_plant_id), c1910_division_id, c101_dealer_id, c501_surgery_date,v_contract,c501_customer_po_date
		  FROM t501_order
		 WHERE c501_order_id = p_orderid;
	INSERT INTO t502_item_order
				(c502_item_order_id, c501_order_id, c205_part_number_id, c502_item_qty, c502_control_number
			   , c502_item_price, c901_type, c502_construct_fl, c502_ref_id, c502_list_price
				)
		 VALUES (s502_item_order.NEXTVAL, v_id, p_pnum, p_bo_qty, ''
			   , p_price, 50300, p_cap_fl, p_refid, v_list_price
				);

-- Calling the below Procedure to fetch and save the Sub Component Mapping details from T2020 table to t2020a_sub_cmap_trans_details
	gm_pkg_pd_sub_cmp_mpng.gm_sav_part_mpng_txn_dtls(v_id,p_pnum,v_userid);		

-- Carrier and mode of back order should be same as parent order 
	gm_pkg_cm_shipping_trans.gm_sav_ship_info (v_id
											 , 50180
											 , v_shipto
											 , v_shiptoid
											 , v_shipcarr
											 , v_shipmode
											 , NULL
											 , v_addressid
											 , NULL
											 , v_userid
											 , v_shipid
											 , v_attnto
											  );

	--
	SELECT SUM (NVL (TO_NUMBER (c502_item_qty) * TO_NUMBER (c502_item_price), 0))
	  INTO v_total
	  FROM t502_item_order
	 WHERE c501_order_id = v_id AND C502_VOID_FL IS NULL;

	--
	UPDATE t501_order
	   SET c501_total_cost = NVL (v_total, 0)
	     , c501_last_updated_by = v_userid
		 , c501_last_updated_date = CURRENT_DATE
	 WHERE c501_order_id = v_id;

	-- For Discount Order Creation
	-- PMT-28394 (To avoid the DS order creation at order create time - Job will create the DS order)
	-- gm_save_dsorder (v_id);

	p_out_boid	:= v_id;
	--to save the corresponding order attribute value
	gm_pkg_sav_snapshot_info.gm_sav_snapshot_info('ORDER',v_id,'SNAPSHOT_INFO',v_company_id,'1200',v_userid);
--
END gm_save_item_ord_backorder;
/
