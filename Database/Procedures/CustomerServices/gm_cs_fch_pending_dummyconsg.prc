CREATE OR REPLACE PROCEDURE gm_cs_fch_pending_dummyconsg (
   p_recordset   OUT   TYPES.cursor_type
)
AS
/***************************************************************************************************
      * This procedure is to fetch the pending pending dummyconsignment,
        will be called from the cusomer service dashboard
      * Author : vprasath
      * Date    : 02/28/06
***************************************************************************************************/
BEGIN
   --
   OPEN p_recordset
    FOR
       SELECT   c504_consignment_id condid,
                get_distributor_name (c701_distributor_id) fromname,
                TO_CHAR (c504_created_date, 'MM/DD/YYYY') conddate,
                get_user_name (c504_created_by) initiatorname,
                c504_comments comments
           FROM t504_consignment
          WHERE c504_status_fl = 2
            AND c504_void_fl IS NULL
            AND c504_type = 4129
       ORDER BY c504_created_date;
--
END gm_cs_fch_pending_dummyconsg;
