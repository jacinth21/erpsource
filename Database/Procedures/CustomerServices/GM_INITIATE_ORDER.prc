CREATE OR REPLACE PROCEDURE GM_INITIATE_ORDER
(
	   p_AccId 			    IN T501_ORDER.C704_ACCOUNT_ID%TYPE,
	   p_RepId				IN T501_ORDER.C703_SALES_REP_ID%TYPE,
 	   p_ShipTo				IN T501_ORDER.C501_SHIP_TO%TYPE,
	   p_PO					IN T501_ORDER.C501_CUSTOMER_PO%TYPE,
	   p_UserId				IN T501_ORDER.C501_CREATED_BY%TYPE,
	   p_OrdId				OUT VARCHAR2,
	   p_ship_add			OUT VARCHAR2,
	   p_message 			OUT  VARCHAR2
)
AS
v_Date VARCHAR2(12);
v_ord_id NUMBER;
v_string VARCHAR2(10);
v_id_string VARCHAR2(10);
v_ship_add VARCHAR2(1000);
BEGIN
	 SELECT SYSDATE INTO v_Date FROM DUAL;
	 	  SELECT S501_ORDER.NEXTVAL INTO v_ord_id FROM dual;
			  IF LENGTH(v_ord_id) = 1
			  	 THEN
				 v_id_string := '0' || v_ord_id;
				 ELSE
				 v_id_string := v_ord_id;
			  END IF;
			  SELECT 'GM' || v_id_string INTO v_string FROM dual;
		  	  p_OrdId := v_string;
			  v_ship_add := GET_SHIP_ADD(p_ShipTo,p_AccId,p_RepId,0);
			  -- DOnt know if this Procedure is called. But adding 0 for SHIP_TO_ID as function was changed.
			  p_ship_add := v_ship_add;

END GM_INITIATE_ORDER;
/

