/* Formatted on 2009/01/09 19:30 (Formatter Plus v4.8.0) */
-- @"C:\database\Procedures\CustomerServices\GM_REPORT_ORDER_CONTROL.prc"

CREATE OR REPLACE PROCEDURE gm_report_order_control (
	p_message	  OUT	VARCHAR2
  , p_recordset   OUT	TYPES.cursor_type
)
AS
v_comp_id T1900_COMPANY.C1900_COMPANY_ID%TYPE;
v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
v_filter T906_RULES.C906_RULE_VALUE%TYPE;
/*
	  Description			:This procedure is called for Project Reporting
	  Parameters			:p_column
							:p_recordset
*/
BEGIN
	SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
	  INTO v_comp_id, v_plant_id
	  FROM DUAL;
	
	  SELECT DECODE(GET_RULE_VALUE(v_comp_id,'ORDFILTERDASH'), null, v_comp_id, v_plant_id) --ORDFILTER- To fetch the records for EDC entity countries based on plant
      INTO v_filter 
      FROM DUAL;
      
	OPEN p_recordset
	 FOR
		 SELECT   t501.c501_order_id ID, get_account_name (c704_account_id) NAME, t501.c704_account_id accid
		        , get_account_curr_symb(c704_account_id) ACC_CURR_SYMB
				, get_count_sum (c501_order_id) SUM, get_count_id (c501_order_id) cnt
				, t501.c501_order_date_time dt, get_user_name (c501_created_by) cs
				, DECODE (c501_status_fl, 1, 'Y', '-') cfl
														  --,decode(C501_SHIPPING_DATE,'','Y','-') SFL
														  --,decode(C501_CUSTOMER_PO,'','Y','-') POFL
				  , t501.c501_status_fl fl, get_total_order_amt (c501_order_id) total
																				--,SUM(C501_TOTAL_COST+C501_SHIP_COST) TOTAL
																				--,C501_TOTAL_COST TOTAL
				  , get_code_name (c901_order_type) otype, t5057.c5057_building_short_name buildsname    --added for PMT-33514
			 FROM t501_order t501, t5057_building t5057
			WHERE t501.c501_status_fl IS NOT NULL
			  AND t501.c501_status_fl = 1   --> 0 AND C501_STATUS_FL < 3
			  AND t501.c501_shipping_date IS NULL
			  AND t501.c501_void_fl IS NULL
			  AND t5057.c5057_active_fl(+) = 'Y'			--added for PMT-33514
			  AND t5057.c5057_void_fl IS NULL			--added for PMT-33514
			  AND t501.c501_delete_fl IS NULL
			  AND t501.c5057_building_id = t5057.c5057_building_id(+)    --added for PMT-33514
			  AND (t501.c901_ext_country_id IS NULL
			  OR t501.c901_ext_country_id  in (select country_id from v901_country_codes))
			  AND (t501.c901_order_type IS NULL OR  t501.c901_order_type IN (
			  SELECT t906.c906_rule_value
                  FROM t906_rules t906
                   WHERE t906.c906_rule_grp_id = 'WIP_ORDER_GRP'
                   AND c906_rule_id = 'WIP_ORDER'
                   AND c906_void_fl is null))  
			  AND (t501.c1900_company_id = v_filter
			  OR t501.c5040_plant_id   = v_filter)
		 ORDER BY NAME, t501.c501_order_date_time DESC;
EXCEPTION
	WHEN OTHERS
	THEN
		p_message	:= SQLERRM;
END gm_report_order_control;
/
