CREATE OR REPLACE PROCEDURE gm_cs_sav_loaner_recon_insert (
   p_Transid    IN   t413_inhouse_trans_items.c412_inhouse_trans_id%TYPE,
   p_pnum       IN   t413_inhouse_trans_items.c205_part_number_id%TYPE,
   p_qty		IN	 t413_inhouse_trans_items.c413_item_qty%TYPE,
   p_cnum		IN	 t413_inhouse_trans_items.c413_control_number%TYPE,
   p_price		IN	 t413_inhouse_trans_items.c413_item_price%TYPE,
   p_type		IN	 t413_inhouse_trans_items.c413_status_fl%TYPE,
   p_userid		IN	 VARCHAR2 DEFAULT '30301'
)
AS
--
/***************************************************************************************
 * Description           : This procedure is used to insert values into InHouse Trans Items
			   Table. This is called inside a loop in the parent function
 ****************************************************************************************/
 v_control_num   t413_inhouse_trans_items.c413_control_number%TYPE
                                                             := p_cnum;
 BEGIN
   --
    IF (v_control_num = 'TBE' OR v_control_num = 'tbe')
      THEN
         v_control_num := '';
      END IF;
      
    INSERT INTO t413_inhouse_trans_items (
    c413_trans_id, c412_inhouse_trans_id,
    c205_part_number_id, c413_item_qty,
    c413_control_number, c413_item_price,c413_status_fl,C413_LAST_UPDATED_BY,C413_LAST_UPDATED_DATE
    )VALUES(
    s413_inhouse_item.nextval,p_Transid,
    p_PNum,p_Qty,
    v_control_num,p_price,p_type,p_userid,SYSDATE);
END gm_cs_sav_loaner_recon_insert;
/
