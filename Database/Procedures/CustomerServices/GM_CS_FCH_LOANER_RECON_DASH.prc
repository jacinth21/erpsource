/* Formatted on 2009/03/10 16:03 (Formatter Plus v4.8.0) */
CREATE OR REPLACE PROCEDURE gm_cs_fch_loaner_recon_dash (
	p_recordset   OUT	TYPES.cursor_type
)
AS
/***************************************************************************************************
	  * This procedure is to fetch the unreconciled loaner transactions
	  * Author : D James
	  * Date	: 05/09/2007
***************************************************************************************************/
BEGIN
	--
	/**
		This procedure is no longer called. Instead The query is directly written in java now.
		changes done for Loaner Reconciliation Rpt
	**/
	OPEN p_recordset
	 FOR
		 SELECT   t504a.c504a_consigned_to_id did, get_distributor_name (t504a.c504a_consigned_to_id) dname
				, t412.c412_inhouse_trans_id inhid, t504a.c504_consignment_id conid, t504b.c504a_etch_id etchid
				, get_set_name (t504.c207_set_id) sname, TO_CHAR (t504a.c504a_loaner_dt, 'mm/dd/yyyy') ldate
				, TO_CHAR (t504a.c504a_return_dt, 'mm/dd/yyyy') rdate, get_user_name (t412.c412_created_by) cuser
				, TO_CHAR (t412.c412_created_date, 'mm/dd/yyyy') cdate
			 FROM t504a_loaner_transaction t504a
				, t412_inhouse_transactions t412
				, t504_consignment t504
				, t504a_consignment_loaner t504b
			WHERE t504a.c504a_loaner_transaction_id = t412.c504a_loaner_transaction_id
			  AND t504a.c504_consignment_id = t504.c504_consignment_id
			  AND t504.c504_consignment_id = t504b.c504_consignment_id
			  AND t412.c412_type = 50159
			  AND t412.c412_status_fl < 4
			  AND t412.c412_void_fl IS NULL
		 ORDER BY dname;
--
END gm_cs_fch_loaner_recon_dash;
/
