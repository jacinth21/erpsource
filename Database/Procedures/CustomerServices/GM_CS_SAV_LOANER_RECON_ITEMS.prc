/* Formatted on 2009/06/19 15:22 (Formatter Plus v4.8.0) */


CREATE OR REPLACE PROCEDURE gm_cs_sav_loaner_recon_items (
	p_transid	IN	 t412_inhouse_transactions.c412_inhouse_trans_id%TYPE
  , p_str		IN	 VARCHAR2
  , p_type		IN	 CHAR
  , p_id		IN	 t412_inhouse_transactions.c412_inhouse_trans_id%TYPE
  , p_userid	IN	 t412_inhouse_transactions.c412_last_updated_by%TYPE
)
AS
/***************************************************************************************
 * Description			 : This procedure is used to

 ****************************************************************************************/
	v_strlen	   NUMBER := NVL (LENGTH (p_str), 0);
	v_string	   VARCHAR2 (30000) := p_str;
	v_pnum		   VARCHAR2 (20);
	v_qty		   NUMBER;
	v_control	   VARCHAR2 (20);
	v_price 	   NUMBER;
	v_substring    VARCHAR2 (1000);
	v_date		   DATE;
	v_reconqty	   NUMBER;
	v_unreconqty   NUMBER;
	v_type		   VARCHAR2(20);
	v_txn_type		VARCHAR2(20);
	v_company_id_ctx  t1900_company.c1900_company_id%TYPE;
	v_trans_company_id  t1900_company.c1900_company_id%TYPE;
	v_company_id  t1900_company.c1900_company_id%TYPE;
	v_plant_id t5040_plant_master.c5040_plant_id%TYPE;
	
--
BEGIN
	--
	BEGIN
		
		SELECT c412_type, c1900_company_id, c5040_plant_id
		   INTO v_txn_type, v_company_id, v_plant_id
		   FROM t412_inhouse_transactions
		  WHERE c412_inhouse_trans_id = p_transid;
		-- to set the transaction company id to context
		gm_pkg_cor_client_context.gm_sav_client_context(v_company_id,v_plant_id);
		--
		SELECT get_compid_frm_cntx() 
      	  INTO v_company_id_ctx  
          FROM dual;
		
	EXCEPTION WHEN NO_DATA_FOUND
	THEN
		v_txn_type := NULL;
	END;
	WHILE INSTR (v_string, '|') <> 0
	LOOP
		v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
		v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
		v_pnum		:= NULL;
		v_qty		:= NULL;
		v_control	:= NULL;
		v_price 	:= NULL;
		v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
		v_qty		:= TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
		v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
		v_control	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
		v_price 	:= TO_NUMBER (v_substring);
		v_reconqty	:= NULL;
		v_unreconqty := NULL;

		--before insert value into table, we should confirm the ReconQty less than UnReconqty
		SELECT NVL (SUM (c413_item_qty), 0) unreconqty
		  INTO v_unreconqty
		  FROM t413_inhouse_trans_items
		 WHERE c412_inhouse_trans_id = p_transid AND c205_part_number_id = v_pnum AND c413_status_fl = 'Q';

		SELECT NVL (SUM (c413_item_qty), 0) reconqty
		  INTO v_reconqty
		  FROM t413_inhouse_trans_items
		 WHERE c412_inhouse_trans_id = p_transid AND c205_part_number_id = v_pnum AND c413_status_fl <> 'Q'
		 AND C413_VOID_FL IS NULL;
		
		IF ((v_unreconqty > (v_reconqty + v_qty)) OR (v_unreconqty = (v_reconqty + v_qty)))
		THEN
			--Calling this proc to insert values into the Items table
			gm_cs_sav_loaner_recon_insert (p_transid, v_pnum, v_qty, v_control, v_price, p_type,p_userid);
			
			IF p_type = 'R'
			THEN
				-- Updating LRPN, RHIH, RHPN - as a Missing Txns Reference ID 
				BEGIN
					SELECT C412_TYPE INTO v_type
						FROM T412_INHOUSE_TRANSACTIONS
						WHERE C412_INHOUSE_TRANS_ID = p_id
						AND C412_VOID_FL IS NULL;
				EXCEPTION WHEN NO_DATA_FOUND
				THEN
					v_type := NULL;
				END;
				
				UPDATE t413_inhouse_trans_items
					SET c413_ref_id               = p_id,
						C901_REF_TYPE			  = v_type,
						C413_LAST_UPDATED_BY	  = p_userid,
						C413_LAST_UPDATED_DATE	  = SYSDATE
				  WHERE c412_inhouse_trans_id = p_transid
				    AND c205_part_number_id   = v_pnum
				    AND c413_item_qty         = v_qty
				    AND C413_status_fl = 'R' 
				    AND c413_void_fl         IS NULL
				    AND C413_REF_ID	IS NULL;
				    
				    --Whenever the detail is updated, the inhouse transactions table has to be updated ,so ETL can Sync it to GOP.
					UPDATE t412_inhouse_transactions 
					   SET c412_last_updated_by = p_userid
					     , c412_last_updated_date = SYSDATE
					 WHERE c412_inhouse_trans_id = p_transid;	
			END IF;
			
			--
			IF p_type = 'C'
			THEN
				
					-- Loaner to Consignment Posting. Consignment already created in calling Proc
					gm_save_ledger_posting (48070, CURRENT_DATE, v_pnum, '01', p_id, v_qty, NULL, p_userid);
					
				-- Update the Consign ID as RefId in T413 table for consigned part
				UPDATE t413_inhouse_trans_items
					SET c413_ref_id               = p_id,
						C901_REF_TYPE			  = '4110',
						C413_LAST_UPDATED_BY	  = p_userid,
						C413_LAST_UPDATED_DATE	  = SYSDATE					
				  WHERE c412_inhouse_trans_id = p_transid
				    AND c205_part_number_id   = v_pnum
				    AND c413_item_qty         = v_qty
				    AND C413_status_fl = 'C' 
				    AND c413_void_fl         IS NULL
				    AND C413_REF_ID	IS NULL;
				    
				    --Whenever the detail is updated, the inhouse transactions table has to be updated ,so ETL can Sync it to GOP.
					UPDATE t412_inhouse_transactions 
					   SET c412_last_updated_by = p_userid
					     , c412_last_updated_date = SYSDATE
					 WHERE c412_inhouse_trans_id = p_transid;	
			ELSIF p_type = 'W'
			THEN
			
				BEGIN
					SELECT C412_TYPE INTO v_type
						FROM T412_INHOUSE_TRANSACTIONS
						WHERE C412_INHOUSE_TRANS_ID = p_id
							AND C412_VOID_FL IS NULL;
				EXCEPTION WHEN NO_DATA_FOUND
				THEN
					v_type := NULL;
				END;
				
				UPDATE t413_inhouse_trans_items
					SET c413_ref_id               = p_id,
						C901_REF_TYPE			  = v_type,
						C413_LAST_UPDATED_BY	  = p_userid,
						C413_LAST_UPDATED_DATE	  = SYSDATE
				  WHERE c412_inhouse_trans_id = p_transid
				    AND c205_part_number_id   = v_pnum
				    AND c413_item_qty         = v_qty
				    AND C413_status_fl = 'W' 
				    AND c413_void_fl         IS NULL
				    AND C413_REF_ID	IS NULL;
					
				    --Whenever the detail is updated, the inhouse transactions table has to be updated ,so ETL can Sync it to GOP.
					UPDATE t412_inhouse_transactions 
					   SET c412_last_updated_by = p_userid
					     , c412_last_updated_date = SYSDATE
					 WHERE c412_inhouse_trans_id = p_transid;	
					 
				-- WriteOff Posting
					IF v_txn_type IN ('9112','1006571')
					THEN
						gm_save_ledger_posting (1006655, CURRENT_DATE, v_pnum, '01', p_transid, v_qty, NULL, p_userid);
					ELSE
						gm_save_ledger_posting (4845, CURRENT_DATE, v_pnum, '01', p_transid, v_qty, NULL, p_userid);
					END IF;
			END IF;
		ELSIF (v_unreconqty < (v_reconqty + v_qty))
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','37',v_pnum);
		END IF;   --if (  v_unreconqty >= (v_reconqty +v_qty)){
	--
	END LOOP;
--
END gm_cs_sav_loaner_recon_items;
/
