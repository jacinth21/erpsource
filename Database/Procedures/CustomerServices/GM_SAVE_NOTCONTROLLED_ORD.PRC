/* Formatted on 2010/12/14 10:49 (Formatter Plus v4.8.0) */
--@"C:\Database\Procedures\CustomerServices\GM_SAVE_NOTCONTROLLED_ORD.PRC";

CREATE OR REPLACE PROCEDURE gm_save_notcontrolled_ord (
	p_ordid 		IN		 t501_order.c501_order_id%TYPE
  , p_userid		IN		 t501_order.c501_created_by%TYPE
  , p_rmqtystr		IN		 CLOB
  , p_boqtystr		IN		 CLOB
  , p_rmqtymsgstr	OUT 	 VARCHAR2
  , p_boqtymsgstr	OUT 	 VARCHAR2
)
AS
	v_rmqtymsgstr	 VARCHAR2 (4000);
	v_boqtymsgstr	 VARCHAR2 (4000);
BEGIN
	IF p_rmqtystr IS NOT NULL
	THEN
		gm_update_notcntrl_str (p_ordid, p_userid, p_rmqtystr, '', v_rmqtymsgstr, v_boqtymsgstr);		
		p_rmqtymsgstr := v_rmqtymsgstr;		
	END IF;

	IF p_boqtystr IS NOT NULL
	THEN		
		gm_update_notcntrl_str (p_ordid, p_userid, p_boqtystr, 'Y', v_rmqtymsgstr, v_boqtymsgstr);
		p_boqtymsgstr := v_boqtymsgstr;
	END IF;
	IF p_boqtymsgstr IS NOT NULL OR p_rmqtymsgstr IS NOT NULL
	THEN
	
		-- PMT-28394 (To avoid the DS order creation - Job will create the DS order)

		-- update the rebate process flag - So, that job will pick the order and calculate discount

		gm_pkg_ac_order_rebate_txn.gm_update_rebate_fl (p_ordid, NULL, NULL);
		gm_pkg_ac_order_rebate_txn.gm_void_discount_order (p_ordid, p_userid);

	END IF;
END gm_save_notcontrolled_ord;
/
