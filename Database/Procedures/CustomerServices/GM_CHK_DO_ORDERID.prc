--@"C:\Database\Procedures\CustomerServices\GM_CHK_DO_ORDERID.prc";
CREATE OR REPLACE
PROCEDURE "GM_CHK_DO_ORDERID" (
        p_order_id IN t501_order.c501_order_id%TYPE,
        p_message OUT VARCHAR2,
        p_surg_date OUT VARCHAR2)
AS
    /***************************************************************************************
    * Description : This procedure checks availability of order_id info.
    *
    ****************************************************************************************/
    v_count NUMBER;
    v_do_format VARCHAR2(10);
	v_do_date VARCHAR2(10);
	v_date_format VARCHAR2(10);
	v_company_id        t1900_company.c1900_company_id%TYPE;
BEGIN
    --
    BEGIN
         SELECT COUNT (1)
           INTO v_count
           FROM t501_order
          WHERE c501_order_id   = p_order_id
            AND c501_delete_fl IS NULL
            AND c501_void_fl IS NULL;
        IF (v_count             = 0) THEN
            p_message := 'AVAILABLE';
        ELSE
            p_message := 'DUPLICATE';
        END IF;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        p_message := 'AVAILABLE';
    END;
    
     SELECT get_compid_frm_cntx() 
      INTO v_company_id  
      FROM dual;
    
    -- getting DO Format and Native Date Format
		SELECT GET_RULE_VALUE_BY_COMPANY('DO_FMT_ORDER','DOFORMAT',v_company_id) , get_company_dtfmt(v_company_id)
		INTO v_do_format , v_date_format
		FROM DUAL;
		
	    -- based on the Order id we are getting the DO date by substring p_orderid and formating with the native date format to save it as Surgery Date.	
	    SELECT TO_CHAR(TO_DATE(SUBSTR(SUBSTR(p_order_id,instr(p_order_id,'-')+1),0,6),v_do_format),v_date_format) INTO p_surg_date FROM DUAL;
	   	    
END GM_CHK_DO_ORDERID;
/
