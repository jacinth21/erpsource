/* Formatted on 2010/05/14 12:59 (Formatter Plus v4.8.0) */
-- @"C:\database\Procedures\CustomerServices\GM_SAVE_ITEM_ORD_ITEMS.prc"

CREATE OR REPLACE PROCEDURE gm_save_item_ord_items (
	p_orderid	  IN   t501_order.c501_order_id%TYPE
  , p_str		  IN   VARCHAR2
  , p_statusfl	  IN   VARCHAR2
  , p_userid	  IN   t501_order.c501_last_updated_by%TYPE
  , p_action	  IN   VARCHAR2
  , p_ordertype   IN   t501_order.c901_order_type%TYPE
  , p_parentid	  IN   t501_order.c501_parent_order_id%TYPE
)
AS
	v_strlen	   NUMBER := NVL (LENGTH (p_str), 0);
	v_string	   VARCHAR2 (30000) := p_str;
	v_pnum		   VARCHAR2 (20);
	v_qty		   NUMBER;
	v_control	   t502_item_order.c502_control_number%TYPE;
	v_price 	   NUMBER (15, 2);
	v_boflag	   VARCHAR2 (10);
	v_substring    VARCHAR2 (1000);
	v_msg		   VARCHAR2 (1000);
	e_others	   EXCEPTION;
	v_stock 	   NUMBER;
	v_bo_qty	   NUMBER;
	v_bo_cnt	   NUMBER;
	v_cnt		   NUMBER;
	v_ord_cnt	   NUMBER;
	v_total 	   NUMBER (15, 2);
	v_check_pnum   t205_part_number.c205_part_number_id%TYPE;
	v_partordtyp   VARCHAR2 (10);
	v_capfl 	   VARCHAR2 (10);
	v_len		   NUMBER;
	v_bo		   NUMBER;
	v_statusfl	   CHAR (1);
	--v_orderid	   VARCHAR2 (20);
	v_order_type   t501_order.c901_order_type%TYPE;
	v_current_order_status t501_order.c501_status_fl%TYPE;
	v_ra_reason    NUMBER;
	v_shipid	   VARCHAR2 (20);
	v_temp		   VARCHAR2 (20);
	v_refid 	   VARCHAR2 (40);
	v_flag		   VARCHAR2 (1);
	v_out_boid	   VARCHAR2 (40);
	v_tot_item 	   NUMBER;
	v_ln_item 	   NUMBER;
	v_customer_po t501_order.c501_customer_po%TYPE;
	v_ack_order_id  t501_order.c501_order_id%type;
	v_pdt_family NUMBER;
	--PC-3430 Sales back order release issue
	v_order_id		   VARCHAR2 (20);
	v_bo_id 			t501_order.c501_order_id%TYPE;
	v_part_cnt			NUMBER;
	
--
BEGIN
	-- Fetch current order status
	SELECT	   t501.c501_status_fl, NVL (t501.c901_order_type, '-999999'), NVL(c501_customer_po,'-9999')
		  INTO v_current_order_status, v_order_type,v_customer_po
		  FROM t501_order t501
		 WHERE t501.c501_order_id = p_orderid
	FOR UPDATE;
	
	--
	IF p_action = 'UPDATE'
	THEN
		DELETE		t502_item_order
			  WHERE c501_order_id = p_orderid AND c205_part_number_id <> 'GPODISC';
	END IF;

	--
	v_cnt		:= 0;
	v_bo_cnt	:= 0;
	v_total 	:= 0;
	v_ord_cnt	:= 0;
	v_bo		:= p_ordertype;
	v_statusfl	:= p_statusfl;

	IF v_order_type = 2531
	THEN
		v_ra_reason := gm_pkg_op_return.get_return_reason (p_orderid, 'orders');
		v_bo		:= v_order_type;
	END IF;

	--
	WHILE INSTR (v_string, '|') <> 0
	LOOP
		v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
		v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
		v_pnum		:= NULL;
		v_qty		:= NULL;
		v_control	:= NULL;
		v_price 	:= NULL;
		v_boflag	:= NULL;
		v_bo_qty	:= NULL;
		v_partordtyp := NULL;
		v_refid 	:= NULL;
		v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
		v_qty		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
		v_control	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
		v_partordtyp := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
		v_refid 	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
		v_capfl 	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
		v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
------------------------ REMOVE LATER
		v_price 	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
		
		 --	Get the product family inside the for loop 32396
		BEGIN
			SELECT c205_product_family INTO v_pdt_family
			FROM t205_part_number
			WHERE c205_part_number_id = v_pnum;
		EXCEPTION WHEN NO_DATA_FOUND
		THEN
			v_pdt_family:= NULL;
		END;

		IF v_refid IS NOT NULL
		THEN
			v_flag		:= gm_pkg_cm_cart.get_refid_flag (v_refid, v_partordtyp, v_pnum);

			IF v_flag IS NULL
			THEN
				raise_application_error (-20795, '');
			END IF;
		END IF;

		IF v_price IS NULL
		THEN
			v_price 	:= v_substring;
		ELSE
			v_price 	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_boflag	:= v_substring;
		END IF;

------------------------
		--
		v_cnt		:= v_cnt + 1;
		v_len		:= NVL (LENGTH (v_string), 0);

		--
		-- 2520: Quote, If the order type is quote, no need to decrease the quantity in shelf
		-- 101260 : Ack Order , If the order type is Ack Order, no need to decrease the quantity in shelf
		IF v_partordtyp = 50300 AND v_order_type NOT IN ('2520','101260')
		THEN   -- This check is for parts from Sales Kits only
			    SELECT NVL(GET_QTY_IN_INVENTORY(p_orderid,'90800',v_pnum),0)
				  INTO v_stock
				  FROM t205_part_number
				 WHERE c205_part_number_id = v_pnum
				FOR UPDATE;
			
	   -- To avoid back order if the FG qty as Zero but the reservation done for the order with enough FG Qty.  
			BEGIN 
				SELECT c501_ref_id
				  INTO v_ack_order_id 
				  FROM t501_order t501 
				 WHERE t501.c501_order_id =  p_orderid
				   AND c501_void_fl is null;			
			   EXCEPTION WHEN NO_DATA_FOUND THEN
		 		 v_ack_order_id := NULL;
			END;			
					    
			IF (v_ack_order_id IS NOT NULL)
			THEN
			v_stock := v_stock + gm_pkg_op_ack_order.get_reserved_qty (v_ack_order_id,v_pnum);			
			END IF;

			-- While generating BBA order from ACK order, if the shelf qty is not enough, then should not created back order
			IF(v_stock < v_qty AND v_ack_order_id IS NOT NULL)
			THEN
				GM_RAISE_APPLICATION_ERROR('-20999','40',v_pnum);
			END IF;
			
			--
			--PMT-32396 Back Order email, Back order should not be generated for usage code parts
			IF (((v_stock < v_qty) AND p_action = 'INSERT') OR (v_boflag = 'Y' AND p_action = 'INSERT')) AND v_pdt_family != '26240096'
			THEN
				--
				v_bo_cnt	:= v_bo_cnt + 1;

				IF v_stock <= 0 OR v_boflag = 'Y'
				THEN
					v_bo_qty	:= v_qty;
					v_qty		:= 0;
				ELSE
					v_bo_qty	:= v_qty - v_stock;
					v_qty		:= v_stock;
				END IF;

			--	Parent ID is not needed as details can be fetched 
			--  from the same order that is split in to a back order. Changes made for (TKT-3856)
			
			/*	IF p_parentid IS NOT NULL
				THEN
					v_orderid	:= p_parentid;
				ELSE
					v_orderid	:= p_orderid;
				END IF;
			*/
								
				-- Changes made for (TKT-3856)  
				-- After all the parts are looped ( v_len = 0 ), 
				-- and if back order part count matches the looped part count ( v_cnt = v_bo_cnt )
				-- and if back order qty is greater than 0 ( v_bo_qty > 0 )
				-- and ( if stock qty is less than 0 ( v_stock <= 0 ) OR if back order flag is selected by user )							
				IF (v_cnt = v_bo_cnt AND v_len = 0 AND v_bo_qty > 0 AND (v_stock <= 0 OR v_boflag = 'Y'))
				THEN
				-- If there is not stock then entire order should go in BO.
					v_qty		:= v_bo_qty;
					v_bo		:= 2525;
					v_statusfl	:= 0;
				ELSIF (v_cnt = v_bo_cnt AND v_len = 0 AND v_bo_qty > 0 AND v_stock > 0 )
				THEN
				 -- If there is qty in stock but required is more then it should generate one regular order with available qty and one BO for rest of the qty.
					gm_save_item_ord_backorder (p_orderid, v_pnum, v_bo_qty, v_price, v_capfl, v_refid,v_out_boid);
				ELSIF v_cnt > 1 OR (v_len = 0 AND v_bo_qty > 0 AND v_cnt > 1)
				THEN
					gm_save_item_ord_backorder (p_orderid, v_pnum, v_bo_qty, v_price, v_capfl, v_refid,v_out_boid);
				ELSIF (v_cnt = 1 AND v_len > 0 AND v_bo_qty > 0)
				THEN
					gm_save_item_ord_backorder (p_orderid, v_pnum, v_bo_qty, v_price, v_capfl, v_refid,v_out_boid);
				END IF;
				
				/*If the Bo Flag is coming as 'Y' from New ORder Screen,then need to insert the INSERT Part number details for
				 * the Order
				 * 106760:= ORDER;
				 * 93342 := Pick Inititated
				 */
				IF v_boflag = 'Y' THEN
					gm_pkg_op_sav_insert_txn.gm_sav_insert_rev_detail(v_out_boid,106760,93342,p_userid,NULL);
				END IF;
				
			END IF;
		--
		END IF;

		--
		IF v_qty > 0 OR v_partordtyp = 50301 OR v_ra_reason = 3316
		THEN
			INSERT INTO t502_item_order
						(c502_item_order_id, c501_order_id, c205_part_number_id, c502_item_qty, c502_control_number
					   , c502_item_price, c901_type, c502_construct_fl, c502_ref_id
						)
				 VALUES (s502_item_order.NEXTVAL, p_orderid, v_pnum, v_qty, v_control
					   , v_price, v_partordtyp, v_capfl, v_refid
						);
            -- Calling the below Procedure to fetch and save the Sub Component Mapping details from T2020 table to t2020a_sub_cmap_trans_details
		     gm_pkg_pd_sub_cmp_mpng.gm_sav_part_mpng_txn_dtls(p_orderid,v_pnum,p_userid);
		END IF;
	--
	END LOOP;
		
		IF v_order_type ='-999999' -- CORRESPONDS TO BILL & SHIP.
		THEN	
		
			SELECT COUNT(*)  ,SUM(DECODE(C901_TYPE,50301,1,0)) 
			INTO v_tot_item,v_ln_item
			FROM T502_ITEM_ORDER 
			WHERE C501_ORDER_ID=p_orderid;
			
			--If all Items in order are of Loaner type, order should be updated to BillOnlyLoaner.
			--This happens when few items are maked as 'L' and few are maked as 'C' and all of 'C' parts in BO.
			IF(v_tot_item =v_ln_item )
			THEN

			-- Even if customer PO is there or not, need to update status flag as 3 for Bill only loaner
				v_statusfl := 3;
				v_order_type := 2530; 
				
				UPDATE t501_order
				   SET c501_status_fl = v_statusfl
				   	 , c501_last_updated_by = p_userid
					 , c501_last_updated_date = CURRENT_DATE
					 , c901_order_type = v_order_type
				 WHERE c501_order_id = p_orderid;
		 	END IF;
		 	
		 --PC-3430 Sales back order release issue 

			SELECT count(t502.c205_part_number_id) 
              INTO v_part_cnt  
              FROM t501_order t501, t502_item_order t502 
             WHERE t501.C501_ORDER_ID = t502.C501_ORDER_ID 
              AND t501.c901_order_type='2525' 
              AND t502.C501_ORDER_ID = p_orderid 
              AND C502_VOID_FL IS NULL;
                                  
           IF v_part_cnt > 1  THEN
                  gm_sav_backorder_items(p_orderid,v_bo_id);
           END IF;    
			 	
		END IF;
	--
	SELECT SUM (NVL (TO_NUMBER (c502_item_qty) * TO_NUMBER (c502_item_price), 0))
	  INTO v_total
	  FROM t502_item_order
	 WHERE c501_order_id = p_orderid;

	--
	IF v_bo = 2521
	THEN
		v_bo		:= NULL;
	END IF;

	-- If current status is not controlled the call below order
	IF (v_current_order_status <= 1)
	THEN
	
	  	--PC-3430 Sales back order release issue
	 	SELECT count(t502.c205_part_number_id) 
          INTO v_part_cnt  
          FROM t501_order t501, t502_item_order t502 
         WHERE t501.C501_ORDER_ID = t502.C501_ORDER_ID 
           AND t501.c901_order_type='2525' 
           AND t502.C501_ORDER_ID = p_orderid 
           AND C502_VOID_FL IS NULL;
                                  
                           IF v_part_cnt > 1  THEN
                                   gm_sav_backorder_items(p_orderid,v_bo_id);
                           END IF;
                           	  
		UPDATE t501_order
		   SET c501_status_fl = v_statusfl
			 , c501_total_cost = NVL (v_total, 0)
			 , c501_last_updated_by = p_userid
			 , c501_last_updated_date = CURRENT_DATE
			 , c901_order_type = DECODE (v_order_type, 2530, 2530,102364,102364,v_bo) -- Stock Transfer order:102364
		 WHERE c501_order_id = p_orderid;
	END IF;

	--If order is bill only loaner(2530) or bill only from sales consignment(2532) and if release of shipping checked then
	-- do posting (add the Install order type (26240234) as well)
	IF ( (v_statusfl = 2 OR  v_statusfl = 3) AND (v_order_type = 2530 OR v_order_type = 2532 OR v_order_type = 26240234) OR v_ra_reason = 3316)
	THEN
		gm_pkg_cm_shipping_trans.gm_sav_order_ship (p_orderid, 0, p_userid, v_msg);
	END IF;


-- bill only loaner(2530) or bill only from sales consignment(2532) , shipping information not required
	IF (v_statusfl = 2 AND v_order_type <> 2530 AND v_order_type <> 2532)
	THEN
		gm_pkg_cm_shipping_trans.gm_sav_release_shipping (p_orderid, 50180, p_userid, '');
		
		-- When Order is Completed, Wipe out from the Inventory Pick queue Table.
		GM_PKG_OP_INV_SCAN.gm_check_inv_order_status(p_orderid,'Completed',p_userid);

		IF v_ra_reason = 3316
		THEN
			-- To make status of ship info to 40 with Tracking # as N/A
			gm_pkg_cm_shipping_trans.gm_sav_shipout (p_orderid
												   , 50180
												   , ''
												   , ''
												   , ''
												   , ''
												   , 'N/A'
												   , ''
												   , ''
												   , p_userid
												   , ''
												   , v_temp
												   , ''
												   , v_temp
													);
			-- To disable email being sent from the Email Job
			v_shipid	:= gm_pkg_cm_shipping_info.get_shipping_id (p_orderid, 50180, '');
			gm_pkg_common_cancel.gm_op_sav_disable_ship_email (v_shipid, p_userid);
		END IF;
	END IF;
END gm_save_item_ord_items;
/
