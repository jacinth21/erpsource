CREATE OR REPLACE PROCEDURE GM_CS_SAV_SHIP_MULTIPLE
(
 p_str   IN VARCHAR2
 ,p_UserId IN T504_CONSIGNMENT.C504_LAST_UPDATED_BY%TYPE
)
/***************************************************************************************
 * Description           : This procedure saves shipping info
 *
 ****************************************************************************************/
AS
v_strlen  NUMBER          := NVL(LENGTH(p_str),0);
v_string  VARCHAR2(30000) := p_str;
v_shipid   NUMBER;
v_mode    NUMBER;
v_carr   NUMBER;
v_track   VARCHAR2(20);
v_substring  VARCHAR2(1000);
v_loanid  VARCHAR2(20);
BEGIN
--
 IF v_strlen > 0 THEN
     WHILE INSTR(v_string,'|') <> 0 LOOP
   v_substring := SUBSTR(v_string,1,INSTR(v_string,'|')-1);
   v_string := SUBSTR(v_string,INSTR(v_string,'|')+1);
   v_shipid := NULL;
   v_mode  := NULL;
   v_carr  := NULL;
   v_track  := NULL;
   v_shipid := TO_NUMBER(SUBSTR(v_substring,1,INSTR(v_substring,'^')-1));
   v_substring := SUBSTR(v_substring,INSTR(v_substring,'^')+1);
   v_mode  := TO_NUMBER(SUBSTR(v_substring,1,INSTR(v_substring,'^')-1));
   v_substring := SUBSTR(v_substring,INSTR(v_substring,'^')+1);
   v_carr  := TO_NUMBER(SUBSTR(v_substring,1,INSTR(v_substring,'^')-1));
   v_substring := SUBSTR(v_substring,INSTR(v_substring,'^')+1);
   v_track  := v_substring;
   --
   GM_CS_SAV_SHIP_SINGLE(v_shipid,v_mode,v_carr,v_track,p_UserId);
   --
   -- Next section to update Loaner Info - To move this to a new logic for
   -- all shipping requests
   SELECT c907_ref_id
   INTO v_loanid
   FROM t907_shipping_info
   WHERE c907_shipping_id = v_shipid;
   --
   
   gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status (v_loanid, 2, p_userid) ;
   --
  END LOOP;
 END IF;
--
END GM_CS_SAV_SHIP_MULTIPLE;
/

