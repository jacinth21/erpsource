/* Formatted on 2009/09/22 12:25 (Formatter Plus v4.8.0) */

--@"c:\database\procedures\CustomerServices\gm_cs_rpt_loaner_recon_by_part.prc"

CREATE OR REPLACE PROCEDURE gm_cs_rpt_loaner_recon_by_part (
	p_distid		   IN		NUMBER
  , p_num			   IN		t413_inhouse_trans_items.c205_part_number_id%TYPE
  , p_loan_type 	   IN		t504_consignment.c504_type%TYPE
  , p_loan_from_date   IN		VARCHAR2
  , p_loan_to_date	   IN		VARCHAR2
  , p_partdetails	   OUT		TYPES.cursor_type
)
AS
/***************************************************************************************
  This procedure fetches the loaner reconcilation details
 ****************************************************************************************/
--
v_type		VARCHAR2(2000):= '50159,50151,1006571';
v_company_id  t1900_company.c1900_company_id%TYPE;
v_date_fmt VARCHAR2(100);
BEGIN
--
IF p_loan_type = '4127' 
THEN
	v_type := '50159';
ELSIF p_loan_type = '4119'
THEN
	v_type := '50151,1006571';
END IF;
	
	my_context.set_my_inlist_ctx(v_type);
	
	
	select get_compid_frm_cntx(),get_compdtfmt_frm_cntx() into v_company_id,v_date_fmt from dual;
	
	OPEN p_partdetails
	 FOR
		 SELECT pnum, pdesc, pnum || ' - ' || pdesc pnumdesc, NVL (qtymiss, 0) qtymiss, NVL (qtyrecon, 0) qtyrecon, qtymiss - qtyrecon qtyunrecon
			  , NVL (qtysold, 0) qtysold, NVL (qtywrite, 0) qtywrit, NVL (qtyreturn, 0) qtyret
			  , NVL (qtyconsign, 0) qtyconsg, (cogs * qtymiss - qtyrecon) cogs, transid inv_id
			  , get_distributor_name (conid) did
		   FROM (SELECT   c205_part_number_id pnum, get_partnum_desc (c205_part_number_id) pdesc
						, t412.c412_inhouse_trans_id transid
						, SUM (DECODE (t413.c413_status_fl, 'Q', (t413.c413_item_qty), 0)) qtymiss
						, SUM (DECODE (t413.c413_status_fl, 'Q', 0, t413.c413_item_qty)) qtyrecon
						, SUM (DECODE (t413.c413_status_fl, 'S', t413.c413_item_qty, 0)) qtysold
						, SUM (DECODE (t413.c413_status_fl, 'W', t413.c413_item_qty, 0)) qtywrite
						, SUM (DECODE (t413.c413_status_fl, 'R', t413.c413_item_qty, 0)) qtyreturn
						, SUM (DECODE (t413.c413_status_fl, 'C', t413.c413_item_qty, 0)) qtyconsign
						, t504a.c504a_consigned_to_id conid, NVL (get_ac_cogs_value (c205_part_number_id, 4906)
																, 0) cogs
					 FROM t504a_loaner_transaction t504a
						, t412_inhouse_transactions t412
						, t413_inhouse_trans_items t413
						, t504_consignment t504
					WHERE t504a.c504a_loaner_transaction_id = t412.c504a_loaner_transaction_id
					  AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
					  AND t504.c504_consignment_id = t504a.c504_consignment_id
					  AND t504a.c504a_consigned_to_id = t504a.c504a_consigned_to_id
					  AND t504a.c901_consigned_to IN (50170, 50172)
					  AND t412.c412_type IN (SELECT token FROM v_in_list)
					  AND t412.c412_inhouse_purpose IN (50127, 50107)
					  AND t504a.c504a_void_fl IS NULL
					  AND t412.c412_void_fl IS NULL
					  AND t413.c413_void_fl IS NULL
					  AND t504.c504_void_fl IS NULL
					  AND t504a.c1900_company_id =v_company_id
					  AND t504a.c504a_consigned_to_id =
												 NVL (DECODE (p_distid, 0, NULL, p_distid), t504a.c504a_consigned_to_id)
					  --   AND t412.c412_inhouse_trans_id = 'GM-LN-71738'
					  AND TRUNC (t412.c412_created_date) >=
							  DECODE (TO_DATE (p_loan_from_date, v_date_fmt)
									, '', TRUNC (t412.c412_created_date)
									, TO_DATE (p_loan_from_date, v_date_fmt)
									 )
					  AND TRUNC (t412.c412_created_date) <=
							  DECODE (TO_DATE (p_loan_to_date, v_date_fmt)
									, '', TRUNC (t412.c412_created_date)
									, TO_DATE (p_loan_to_date, v_date_fmt)
									 )
					  AND regexp_like (t413.c205_part_number_id, NVL (REGEXP_REPLACE(p_num,'[+]','\+'), REGEXP_REPLACE(t413.c205_part_number_id,'[+]','\+')))
					  AND t504.c504_type = DECODE (p_loan_type, 0, t504.c504_type, p_loan_type)
				 GROUP BY t412.c412_ref_id
						, c205_part_number_id
						, t412.c412_inhouse_trans_id
						, t504a.c504a_consigned_to_id
						, t412.c412_created_date
						, t412.c412_ref_id
				 ORDER BY c205_part_number_id)
		  WHERE (qtysold <> 0 OR qtymiss <> 0)
	UNION
	 SELECT pnum, pdesc, pnum || ' - ' || pdesc pnumdesc
	  , NVL (qtymiss, 0) qtymiss, NVL (qtyrecon, 0) qtyrecon, qtymiss - qtyrecon qtyunrecon
	  , NVL (qtysold, 0) qtysold, NVL (qtywrite, 0) qtywrit, NVL (qtyreturn, 0) qtyret
	  , NVL (qtyconsign, 0) qtyconsg, (cogs * qtymiss - qtyrecon) cogs, transid inv_id
	  , get_distributor_name (conid) did
	   FROM
	    (
	         SELECT t413.c205_part_number_id pnum, get_partnum_desc (t413.c205_part_number_id) pdesc,
	            t412.c412_inhouse_trans_id transid, 
	            SUM (DECODE (t413.c413_status_fl, 'Q', (t413.c413_item_qty), 0)) qtymiss, 
	            SUM (DECODE (t413.c413_status_fl, 'Q', 0, t413.c413_item_qty)) qtyrecon, 
	            SUM (DECODE (t413.c413_status_fl, 'S', t413.c413_item_qty, 0)) qtysold, 
	            SUM (DECODE (t413.c413_status_fl, 'W',t413.c413_item_qty, 0)) qtywrite, 
	            SUM (DECODE (t413.c413_status_fl, 'R', t413.c413_item_qty, 0)) qtyreturn,
	            SUM (DECODE (t413.c413_status_fl, 'C', t413.c413_item_qty, 0)) qtyconsign, 
	            t504.c504_ship_to_id conid, NVL(get_ac_cogs_value (c205_part_number_id, 4906), 0) cogs
	           FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413, t506_returns t506
	          , t504_consignment t504
	          WHERE t504.c504_consignment_id        = t506.c506_ref_id
	            AND	t504.c504_ship_to_id			= DECODE(p_distid,'0',t504.c504_ship_to_id,p_distid)
	          	AND t506.c506_rma_id                = t412.c412_ref_id
	          	 AND regexp_like (t413.c205_part_number_id, NVL (REGEXP_REPLACE(p_num,'[+]','\+'), REGEXP_REPLACE(t413.c205_part_number_id,'[+]','\+')))
	            AND t413.c412_inhouse_trans_id      = t412.c412_inhouse_trans_id
	            AND t412.c412_type                 IN ('9112', '9115')
	            AND TRUNC (T412.C412_created_date) >= to_date (p_loan_from_date, v_date_fmt)
	            AND TRUNC (T412.C412_created_date) <= to_date (p_loan_to_date, v_date_fmt)
	            AND t412.c412_void_fl              IS NULL
	            AND t506.c506_void_fl              IS NULL
	            AND c412_status_fl                  < 4
	            AND p_loan_type IN ('4119','0')
	            AND t412.c1900_company_id =v_company_id
	       GROUP BY t413.c205_part_number_id, t412.c412_inhouse_trans_id, t504.c504_ship_to_id
	    )
	  WHERE (qtysold <> 0
	    OR qtymiss   <> 0) ;
	
--
END gm_cs_rpt_loaner_recon_by_part;
/
