/* Formatted on 2008/08/14 12:34 (Formatter Plus v4.8.0) */
-- @"C:\database\Procedures\Clinical\gm_update_patient.prc"


CREATE OR REPLACE PROCEDURE gm_update_patient (
	p_patient_ide_no   IN		t621_patient_master.c621_patient_ide_no%TYPE
  , p_study_id		   IN		t621_patient_master.c611_study_id%TYPE
  , p_surgery_type	   IN		t621_patient_master.c901_surgery_type%TYPE
  , p_surgery_date	   IN		VARCHAR2
  , p_study_site_id    IN		t614_study_site.c614_study_site_id%TYPE
  , p_surgeon_id	   IN		t621_patient_master.c101_surgeon_id%TYPE
  , p_complete_flag    IN		t621_patient_master.c621_surgery_comp_fl%TYPE
  , p_user			   IN		t621_patient_master.c621_created_by%TYPE
  , p_id_out		   IN OUT	VARCHAR2
)
AS
/*******************************************************************************************
 *	   Description			 :This procedure is called to store Patient information
 *	   Parameters			 :patient id, study, type, surgery date, account id, surgeon id
 *							 :patient id, study forms the primary key
 ********************************************************************************************/
	v_account_id   t621_patient_master.c704_account_id%TYPE;
	v_patient_ide_cnt NUMBER;
	v_party_id	   VARCHAR (20);
BEGIN
	SELECT c704_account_id
	  INTO v_account_id
	  FROM t614_study_site
	 WHERE c614_study_site_id = p_study_site_id;

	--
	UPDATE t621_patient_master
	   SET c621_patient_ide_no = p_patient_ide_no
		 , c611_study_id = p_study_id
		 , c901_surgery_type = p_surgery_type
		 , c621_surgery_date = TO_DATE (p_surgery_date, 'MM/DD/YYYY')
		 , c704_account_id = v_account_id
		 , c101_surgeon_id = p_surgeon_id
		 , c621_last_updated_by = p_user
		 , c621_last_updated_date = SYSDATE
		 , c621_surgery_comp_fl = p_complete_flag
	 WHERE c621_patient_id = p_id_out;

	--
	IF (SQL%ROWCOUNT = 0)
	THEN
		SELECT s621_patient_master.NEXTVAL
		  INTO p_id_out
		  FROM DUAL;

		--
		INSERT INTO t621_patient_master
					(c621_patient_id, c621_patient_ide_no, c611_study_id, c901_surgery_type, c621_surgery_date
				   , c704_account_id, c101_surgeon_id, c621_delete_fl, c621_created_by, c621_created_date
				   , c621_surgery_comp_fl
					)
			 VALUES (p_id_out, p_patient_ide_no, p_study_id, p_surgery_type, TO_DATE (p_surgery_date, 'MM/DD/YYYY')
				   , v_account_id, p_surgeon_id, 'N', p_user, SYSDATE
				   , p_complete_flag
					);
	END IF;

	SELECT COUNT (c621_patient_ide_no)
	  INTO v_patient_ide_cnt
	  FROM t621_patient_master
	 WHERE c621_patient_ide_no = p_patient_ide_no AND c621_delete_fl = 'N';

	IF v_patient_ide_cnt > 1
	THEN
		raise_application_error ('-20101', '');
	END IF;
   --existing parameter p_id_out  passed to update patient setup changes to v621_patient_study_list
	gm_pkg_cr_common.gm_sav_site_patient_details(p_id_out,NULL,NULL);
END gm_update_patient;
/
