/* Formatted on 2007/10/11 10:40 (Formatter Plus v4.8.0) */
-- @"C:\Database\Procedures\Clinical\gm_report_patient_study_list.prc"
CREATE OR REPLACE PROCEDURE gm_report_patient_study_list (
	p_patient_id   IN		t621_patient_master.c611_study_id%TYPE
  , p_study_id	   IN		t621_patient_master.c621_patient_ide_no%TYPE
  , p_message	   OUT		VARCHAR2
  , p_recordset    OUT		TYPES.cursor_type
)
AS
/***************************************************************************************
 * Description	 :This procedure is called to FETCH Study List FOR THE SELECTED PATIENT
 * Parameters	 :N/A
 *							 :p_recordset	- holds list of party name
 **************************************************************************************/
	v_str		   VARCHAR2 (2000);
BEGIN
--
	OPEN p_recordset
	 FOR
		 SELECT   v621.c612_study_list_id slist_id, v621.c611_study_id s_id, v621.c601_form_id f_id
				, get_form_name (v621.c601_form_id) f_ds, v621.c612_study_form_ds sform_ds
				, v621.c613_study_period_id sp_id, v621.c613_study_period_ds p_ds, v621.c613_duration DURATION
				, TO_CHAR (v621.c621_exp_date_start, 'MM/DD/YYYY') earliest_date
				, TO_CHAR (v621.c621_exp_date, 'MM/DD/YYYY') expected_date
				, TO_CHAR (v621.c621_exp_date_final, 'MM/DD/YYYY') latest_date
				, TO_CHAR (temp_t1.c622_date, 'MM/DD/YYYY') rec_date, NVL (temp_t1.c622_verify_fl, 'N') v_fl
				, temp_t1.verified_by
				, DECODE (temp_t1.c622_date, NULL ,  DECODE (SIGN (SYSDATE - v621.c621_exp_date_final), -1, 'N', 'Y') , 'N' )  due_flag
				, v621.c901_study_period stperiod
			 FROM v621_patient_study_list v621
				, (SELECT t622.c622_patient_list_id, t622.c613_study_period_id, t622.c622_date, t622.c622_verify_fl
						, get_patient_verified_by (t622.c622_patient_list_id) verified_by
					 FROM t622_patient_list t622
					WHERE t622.c621_patient_id IN (SELECT c621_patient_id
													 FROM t621_patient_master
													WHERE c621_patient_ide_no = p_patient_id) AND NVL(t622.C622_DELETE_FL,'N')='N') temp_t1
			WHERE v621.c621_patient_ide_no = p_patient_id
			  AND v621.c611_study_id = p_study_id
			  AND v621.c613_study_period_id = temp_t1.c613_study_period_id(+)
		 ORDER BY  v621.c613_seq_no, v621.c612_seq;
END gm_report_patient_study_list;
/
