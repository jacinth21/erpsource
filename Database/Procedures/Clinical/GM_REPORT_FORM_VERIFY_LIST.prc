CREATE OR REPLACE PROCEDURE GM_REPORT_FORM_VERIFY_LIST
(	
	 p_message		OUT VARCHAR2
	,p_recordset	OUT Types.cursor_type
)
AS
/***************************************************************************************
 * Description   :This procedure is called to FETCH the list of form that need to be filled 
 * Parameters    :N/A
 *               :p_recordset	- holds list of list of due form for the entire patient
 **************************************************************************************/
v_str VARCHAR2(2000);
BEGIN
--
	OPEN p_recordset FOR
	SELECT	 c611_study_id s_id
			,c613_study_period_ds  s_ds
			,c901_study_period s_p_id
			,c613_study_period_id s_p_key
			,get_code_seq_no(c901_study_period) seq_no
			,c704_account_id a_id
			,get_account_name(c704_account_id) a_name
			,c621_patient_ide_no p_ide_no
			,c621_patient_id p_key
			,c601_form_id f_id
			,get_form_name(c601_form_id) f_name			
			,duration
			,NVL(g_period_before,0) g_period_before
			,NVL(g_period_after ,0) g_period_after
			,get_code_name_alt(c901_grace_period_type) ptype_id	 
			,DECODE( c621_surgery_date, NULL, NULL,
				   TO_CHAR(exp_date,'MM/DD/YYYY')  
				   ) exp_date	-- FOR Preoperative IF no surgery date returns null 
			,DECODE ( c621_surgery_date, NULL, NULL, 
			 TO_CHAR(
				DECODE(c901_grace_period_type,
					 6181,(exp_date + duration),
					 6184,(exp_date + (duration * 7) ),
					 6182,add_months(exp_date,duration),
					 6183,add_months(exp_date,(duration * 12)),
					 exp_date) 	,'MM/DD/YYYY')  
				) Final_Due_Date 
		   ,DECODE ( c621_surgery_date, NULL, 'N',
				   DECODE( SIGN(SYSDATE - decode(c901_grace_period_type,
					 6181,(exp_date + duration),
					 6184,(exp_date + (duration * 7) ),
					 6182,add_months(exp_date,duration),
					 6183,add_months(exp_date,(duration * 12)),
					 exp_date) ),1,'Y','N') 
					) over_due
	FROM   ( SELECT	 t621.c621_patient_ide_no
					,t621.c621_patient_id 
					,t621.c611_study_id
					,t612.c601_form_id
					,t613.c613_study_period_id
					,t613.c613_study_period_ds  
					,t621.c704_account_id
					,t613.c613_duration duration
					,t613.c613_grace_period_before g_period_before
					,t613.c613_grace_period_after g_period_after
					,t613.c901_study_period
					,t613.c901_period_type
					,t613.c901_grace_period_type
					,t621.c621_surgery_date
					,NVL ( DECODE(c901_period_type,
							6181,(t621.c621_surgery_date + t613.c613_duration), -- Day 
							6184,(t621.c621_surgery_date + (t613.c613_duration * 7) ), --  Wks
							6182,add_months(t621.c621_surgery_date,t613.c613_duration), -- Month 
							6183,add_months(t621.c621_surgery_date,(t613.c613_duration * 12)), -- Years
							t621.c621_surgery_date ), 
							DECODE( c901_study_period, 6300, SYSDATE, NULL) )  exp_date -- This code calculates exp due date 
					,t612.c612_seq
			 FROM	 t621_patient_master t621
					,t612_study_form_list  t612
					,t613_study_period t613
			WHERE	t621.c621_delete_fl = 'N'
			AND		t612.c611_study_id = t621.c611_study_id 
			AND		t612.c612_study_list_id = t613.c612_study_list_id
			AND   (T621.C621_PATIENT_ID, T613.C613_STUDY_PERIOD_ID ) NOT IN 
				( SELECT  t622.c621_patient_id
						,t622.c613_study_period_id 
				FROM     t622_patient_list t622 
				WHERE	NVL(t622.c622_verify_fl,'N') = 'N'
				AND NVL(t622.C622_DELETE_FL,'N')='N')   
			) TEMP
	WHERE	exp_date <= SYSDATE -- Only retrieves the due information 
	ORDER BY s_id, seq_no,a_name , p_ide_no , c612_seq;
	-- Value sorted by Study ID, Study Period, account name, patient id, Form Sequence number
	-- 
EXCEPTION
	WHEN OTHERS THEN
		p_message := '1000';
END GM_REPORT_FORM_VERIFY_LIST;
/

