--@"C:\Database\Procedures\Clinical\GM_UPDATE_EVENT_NO.prc";
CREATE OR REPLACE
PROCEDURE gm_update_event_no (
        p_form_id         IN t601_form_master.c601_form_id%TYPE,
        p_study_period_id IN t613_study_period.c613_study_period_id%TYPE,
        p_user_id         IN t622_patient_list.c622_last_updated_by%TYPE)
AS
    /*******************************************************************************************
    *    Description   :This procedure is used to update event number (Froms that converted to multi event form)
    *    Author        :Dhanreddy
    *    Parameters    :form id, study period id,user id
    ********************************************************************************************/
    v_study_exam_type t613_study_period.c613_study_exam_type%TYPE ;
    -- cursor to fetch patient ids
    CURSOR patientid_cursor
    IS
        SELECT DISTINCT t622.c621_patient_id patient_id
           FROM t622_patient_list t622
          WHERE t622.c613_study_period_id = p_study_period_id
            AND t622.c601_form_id         = p_form_id;
BEGIN
    BEGIN
	     -- fetch exam type
         SELECT t613.c613_study_exam_type
           INTO v_study_exam_type
           FROM t613_study_period t613, t612_study_form_list t612
          WHERE t613.c612_study_list_id   = t612.c612_study_list_id
            AND t612.c601_form_id         = p_form_id
            AND t613.c613_study_period_id = p_study_period_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_study_exam_type := NULL;
    END;
    IF v_study_exam_type = 'D' THEN
        FOR curindex    IN patientid_cursor
        LOOP -- loop for each patient id
            FOR patientlist_cursor IN -- cursor to fetch patient list ids
            (SELECT t622.c622_patient_list_id patient_list_id, ROW_NUMBER() OVER ( ORDER BY t622.c622_patient_list_id) AS patient_event_no
               FROM t622_patient_list t622
              WHERE t622.c613_study_period_id = p_study_period_id
                AND t622.c601_form_id         = p_form_id
                AND t622.c621_patient_id      = curindex.patient_id
           ORDER BY t622.c622_patient_list_id
            )
            LOOP -- loop for each patient list id to update event no 
                 UPDATE t622_patient_list t622
                SET t622.c622_patient_event_no         = patientlist_cursor.patient_event_no, t622.c622_last_updated_date =
                    sysdate, t622.c622_last_updated_by = p_user_id
                  WHERE t622.c613_study_period_id      = p_study_period_id
                    AND t622.c601_form_id              = p_form_id
                    AND t622.c621_patient_id           = curindex.patient_id
                    AND t622.c622_patient_list_id      = patientlist_cursor.patient_list_id;
            END LOOP;--end of patientlist_cursor
        END LOOP; -- end of patientid_cursor
    END IF;
END gm_update_event_no;
/