-- @"C:\Database\Procedures\Clinical\GM_UPDATE_DUPLICATE_EVENT_NO.prc";
CREATE OR REPLACE
PROCEDURE gm_update_duplicate_event_no (
        p_form_id         IN t623_patient_answer_list.c601_form_id%type,
        p_study_period_id IN t622_patient_list.c613_study_period_id%TYPE,
        p_question_id     IN t623_patient_answer_list.c602_question_id%TYPE,
        p_user_id         IN t622_patient_list.c622_created_by%type)
AS
 /*******************************************************************************************
    *    Description   :This procedure is used to create new event for duplicate answers
    *    Author        :Dhanreddy
    ********************************************************************************************/

    v_new_patient_list_id NUMBER;
    v_count               NUMBER;
    v_patient_list_id     NUMBER;
    v_patient_event_no    NUMBER;
    
    -- to find duplicate patient list ids and patient ids
    CURSOR duplicate_event_cursor
    IS
         SELECT c622_patient_list_id patient_list_id, c621_patient_id patient_id
           FROM t623_patient_answer_list t623
          WHERE t623.c601_form_id   = p_form_id
            AND c602_question_id    = p_question_id
            AND t623.c623_delete_fl = 'N'
       GROUP BY c622_patient_list_id, c602_question_id, c621_patient_id
        HAVING COUNT (c602_question_id) > 1;
BEGIN
    FOR cur_index IN duplicate_event_cursor
    LOOP
        BEGIN
        -- to find duplicate count for single patient_list_id
             SELECT COUNT (c602_question_id), c622_patient_list_id
               INTO v_count, v_patient_list_id
               FROM t623_patient_answer_list t623
              WHERE t623.c601_form_id         = p_form_id
                AND c602_question_id          = p_question_id
                AND t623.c621_patient_id      = cur_index.patient_id
                AND t623.c622_patient_list_id = cur_index.patient_list_id
                AND t623.c623_delete_fl       = 'N'
           GROUP BY c622_patient_list_id, c602_question_id
            HAVING COUNT (c602_question_id) > 1;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_count           := 0;
            v_patient_list_id := NULL;
        END;
        
        FOR i IN 1.. v_count - 1
        LOOP
          -- generate new patient list id
             SELECT s622_patient_list.NEXTVAL INTO v_new_patient_list_id FROM DUAL;
             
             -- to generate event number
             SELECT MAX (c622_patient_event_no) + 1
               INTO v_patient_event_no
               FROM t622_patient_list
              WHERE c601_form_id         = p_form_id
                AND c613_study_period_id = p_study_period_id
                AND c621_patient_id      = cur_index.patient_id;
                
                -- to insert new patient list record
             INSERT
               INTO t622_patient_list
                (
                    c622_patient_list_id, c622_date, c622_initials,
                    c601_form_id, c621_patient_id, c613_study_period_id,
                    c622_created_by, c622_created_date, c901_type,
                    c622_delete_fl, c621_cal_value, c622_missing_follow_up_fl,
                    c622_verify_fl, c622_patient_event_no
                )
             SELECT v_new_patient_list_id, c622_date, c622_initials,
                c601_form_id, c621_patient_id, c613_study_period_id,
                c622_created_by, c622_created_date, c901_type,
                c622_delete_fl, c621_cal_value, c622_missing_follow_up_fl,
                c622_verify_fl, v_patient_event_no
               FROM t622_patient_list
              WHERE c622_patient_list_id = v_patient_list_id;
              
             -- to update the t623a_patient_answer_history table
             UPDATE t623a_patient_answer_history
            SET c622_patient_list_id          = v_new_patient_list_id, c623_last_updated_by = p_user_id, c623_last_updated_date= sysdate
              WHERE c623_patient_ans_list_id IN
                (SELECT t623.c623_patient_ans_list_id
                   FROM t623_patient_answer_list t623
                  WHERE t623.c621_patient_id      = cur_index.patient_id
                    AND t623.c601_form_id         = p_form_id
                    AND t623.c622_patient_list_id = v_patient_list_id
                    AND t623.rowid                > ANY
                    (SELECT t623_1.rowid
                       FROM t623_patient_answer_list t623_1
                      WHERE c621_patient_id           = cur_index.patient_id
                        AND c601_form_id              = p_form_id
                        AND t623.c622_patient_list_id = t623_1.c622_patient_list_id
                        AND t623.c621_patient_id      = t623_1.c621_patient_id
                        AND t623.c603_answer_grp_id   = t623_1.c603_answer_grp_id
                    )
                ) ;
                
                 -- to update the t623_patient_answer_list table
                 
             UPDATE t623_patient_answer_list
            SET c622_patient_list_id          = v_new_patient_list_id, c623_last_updated_by = p_user_id, c623_last_updated_date= sysdate
              WHERE c623_patient_ans_list_id IN
                (SELECT t623.c623_patient_ans_list_id
                   FROM t623_patient_answer_list t623
                  WHERE t623.c621_patient_id      = cur_index.patient_id
                    AND t623.c601_form_id         = p_form_id
                    AND t623.c622_patient_list_id = v_patient_list_id
                    AND t623.rowid                > ANY
                    (SELECT t623_1.rowid
                       FROM t623_patient_answer_list t623_1
                      WHERE c621_patient_id           = cur_index.patient_id
                        AND c601_form_id              = p_form_id
                        AND t623.c622_patient_list_id = t623_1.c622_patient_list_id
                        AND t623.c621_patient_id      = t623_1.c621_patient_id
                        AND t623.c603_answer_grp_id   = t623_1.c603_answer_grp_id
                    )
                ) ;
            v_patient_list_id := v_new_patient_list_id;
        END LOOP;
    END LOOP;
END gm_update_duplicate_event_no;
/