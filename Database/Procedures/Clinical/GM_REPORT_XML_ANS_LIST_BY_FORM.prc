--  @"C:\Database\Procedures\Clinical\GM_REPORT_XML_ANS_LIST_BY_FORM.prc"
CREATE OR REPLACE PROCEDURE "GM_REPORT_XML_ANS_LIST_BY_FORM" 
(
  p_form_id IN   t612_study_form_list.c612_study_form_ds%TYPE
 ,p_recordset  OUT  Types.cursor_type
)
AS
/********************************************************************************************
 * Description  :This procedure is called to FETCH party data and xml tag information
 *       used to frame XML data
 * Parameters   :p_patient_id - holds Patient information
                :p_recordset - holds list of Patient ID and Site information
 *********************************************************************************************/
--
BEGIN
--
OPEN p_recordset FOR
----
 SELECT   t621.c621_patient_ide_no ide
       --,TO_CHAR(t622.c622_date,'MM/DD/YYYY') vdate 
      ,TO_CHAR(t622.c622_date,'MM/DD/YYYY') || t622.C622_PATIENT_EVENT_NO vdate -- For SC14- Adverse Events
      ,t603.c603_xml_tag_id xml_tag 
      , NVL (DECODE (t603.c901_control_type
                                 , 6100, to_char(t605.c605_answer_value)
                                 , to_char(t623.c904_answer_ds)
                                  )
                         , ' '
                          ) answer
      --,DECODE(t623.c605_answer_list_id,NULL, t623.c904_answer_ds, t605.c605_answer_value) answer
      , decode(t603.c901_control_type, 6100,t623.c904_answer_ds,'') other_answer
      , ( select t605.C605_XML_TAG_ID from t603_answer_grp t603, t605_answer_list t605 
          WHERE t603.C904_ANSWER_GRP = t605.C605_ANSWER_GRP
          AND   t603.C603_ANSWER_GRP_ID =  t623.C603_ANSWER_GRP_ID AND ROWNUM = 1) other_xml_tag       
       --, decode(t603.c901_control_type, 6100,t605.C605_XML_TAG_ID,'') other_xml_tag
      ,t613.c613_study_period_ds  p_ds
      --,t622.c621_cal_value other_xml_tag
 FROM     t622_patient_list    t622
   ,t623_patient_answer_list t623
   ,t603_answer_grp     t603
   ,t613_study_period    t613
   ,t612_study_form_list   t612
   ,t605_answer_list    t605
   ,t604_form_ques_link    t604
   ,t621_patient_master  t621
 WHERE  t622.c622_patient_list_id  = t623.c622_patient_list_id
 AND   t612.c612_study_form_ds   = p_form_id
 AND   t603.c603_answer_grp_id   = t623.c603_answer_grp_id
 AND   t603.c603_xml_required_fl  = 'Y'
 AND   t623.c605_answer_list_id   = t605.c605_answer_list_id(+)
 AND   t613.c613_study_period_id  = t622.c613_study_period_id
 AND   t612.c612_study_list_id   = t613.c612_study_list_id
 AND   t604.c601_form_id       = t623.c601_form_id
 AND   t604.c602_question_id      =  t623.c602_question_id
 AND  t621.c621_patient_id   = t622.c621_patient_id
 AND  t621.c621_delete_fl NOT IN ('Y','D')
 AND t604.C604_DELETE_FL = 'N' -- For SC14- Adverse Events
 AND t603.C603_DELETE_FL = 'N'-- For SC14- Adverse Events
 AND t622.c622_date is not null -- Added for SC9 , since there were some records that didnt have dates James 2008
 AND t623.C623_DELETE_FL = 'N'
 AND NVL(t622.C622_DELETE_FL,'N')='N'
 --AND t621.c621_patient_ide_no IN('0110','1704') 
 ORDER BY t621.c621_patient_ide_no
        ,t622.C622_PATIENT_EVENT_NO -- For SC14- Adverse Events
     ,vdate
     ,t604.c604_seq_no
     ,t603.c603_answer_grp_id;
--
END gm_report_xml_ans_list_by_form;
/