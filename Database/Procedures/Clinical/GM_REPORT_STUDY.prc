CREATE OR REPLACE PROCEDURE GM_REPORT_STUDY
(
 p_message   OUT VARCHAR2,
 p_recordset OUT Types.cursor_type
)
AS
/*
      Description           :This procedure is called to FETCH Study List
      Parameters            :N/A
                            :p_recordset - holds list of party name
*/
v_str VARCHAR2(2000);
BEGIN
--
 OPEN p_recordset FOR
  SELECT  c611_study_id ID
    ,c611_study_nm NAME
  FROM  t611_clinical_study
  WHERE  c611_delete_fl = 'N'
  ORDER BY UPPER(c611_study_id);
EXCEPTION
 WHEN OTHERS THEN
  p_message := '1000';
END GM_REPORT_STUDY;
/

