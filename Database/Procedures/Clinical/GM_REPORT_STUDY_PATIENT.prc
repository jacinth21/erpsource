/* Formatted on 2010/05/14 12:13 (Formatter Plus v4.8.0) */


CREATE OR REPLACE PROCEDURE gm_report_study_patient (
	p_study_id	  IN	   t621_patient_master.c611_study_id%TYPE
  , p_studysite_id	  IN	   t614_study_site.c614_study_site_id%TYPE
  , p_message	  OUT	   VARCHAR2
  , p_recordset   OUT	   TYPES.cursor_type
)
AS
/*
	  Description			:This procedure is called to FETCH party name
	  Parameters			:p_study_id  - holds Study ID information
							:p_recordset - holds list of Patient ID and Site information
*/
	v_str		   VARCHAR2 (2000);
BEGIN
--
	OPEN p_recordset
	 FOR
		 SELECT t621.c621_patient_ide_no ID
					   , get_account_name (t621.c704_account_id) || ' - ' || t621.c621_patient_ide_no NAME
					FROM t621_patient_master t621, t614_study_site t614
				   WHERE t621.c611_study_id = p_study_id
					 AND t614.c614_study_site_id = p_studysite_id
					 AND t614.c611_study_id = t621.c611_study_id
					 AND t614.c704_account_id = t621.c704_account_id
					 AND t621.c621_delete_fl = 'N'
				ORDER BY t621.c621_patient_ide_no;
EXCEPTION
	WHEN OTHERS
	THEN
		p_message	:= '1000';
END gm_report_study_patient;
/
