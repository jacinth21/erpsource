/* Formatted on 2008/06/13 12:22 (Formatter Plus v4.8.0) */
/* @"c:\database\procedures\clinical\gm_report_form_due_verify_list.prc";*/

CREATE OR REPLACE PROCEDURE gm_report_form_due_verify_list (
	p_study_id			 IN 	  t611_clinical_study.c611_study_id%TYPE
  , p_cra_id			 IN 	  t101_user.c101_user_id%TYPE
  , p_rsetforms 		 OUT	  TYPES.cursor_type
  , p_rsetoutofwindow	 OUT	  TYPES.cursor_type
  , p_rsetverification	 OUT	  TYPES.cursor_type
)
AS
/***************************************************************************************
 * Description	 :This procedure is called to FETCH the list of form that need to be filled
 * Parameters	 :N/A
 *				 :p_recordset	- holds list of list of due form for the entire patient
 **************************************************************************************/
BEGIN
	--	 Pending Forms
	OPEN p_rsetforms
	 FOR
		 SELECT   v621.c611_study_id s_id, v621.c613_study_period_ds s_ds, v621.c901_study_period s_p_id
				, v621.c613_study_period_id s_p_key, c704_account_id a_id
				,	 DECODE (v621.c614_site_id, '', '', v621.c614_site_id || '-')
				  || get_account_name (v621.c704_account_id)
				  || DECODE (v621.c101_user_id, '', '', '-' || get_user_name (v621.c101_user_id)) a_name
				, v621.c621_patient_ide_no p_ide_no, v621.c621_patient_id p_key, v621.c601_form_id f_id
				, v621.c612_study_form_ds || ' - ' || get_form_name (v621.c601_form_id) f_name
				, TO_CHAR (c621_exp_date_start, 'MM/DD/YYYY') exp_start_date
				, TO_CHAR (v621.c621_exp_date, 'MM/DD/YYYY') exp_date
				, TO_CHAR (v621.c621_exp_date_final, 'MM/DD/YYYY') final_due_date
				, DECODE (SIGN (SYSDATE - DECODE (c621_exp_date_final, '', SYSDATE, c621_exp_date_final))
						, 1, 'Y'
						, 'N'
						 ) over_due
				, c612_seq seq
			 FROM v621_patient_study_list v621
			WHERE (v621.c621_patient_id, v621.c613_study_period_id) NOT IN (
																 SELECT t622.c621_patient_id
																	  , t622.c613_study_period_id
																   FROM t622_patient_list t622
																  WHERE t622.c621_patient_id = v621.c621_patient_id
																  AND NVL(t622.C622_DELETE_FL,'N')='N'
																  )
			  AND v621.c621_exp_date_start <= SYSDATE	-- Only retrieves the due information
			  AND v621.c621_exp_date_final >= SYSDATE
			  AND v621.c101_user_id = DECODE (p_cra_id, '', v621.c101_user_id, p_cra_id)
			  AND v621.c611_study_id = DECODE (p_study_id, '', v621.c611_study_id, p_study_id)
			  AND v621.c621_patient_id NOT IN (SELECT c621_patient_id
												 FROM t622_patient_list
												WHERE c601_form_id = 15 AND c613_study_period_id = 91
												AND NVL(C622_DELETE_FL,'N')='N')
		 ORDER BY s_id, a_name, p_ide_no, s_p_id, seq;

--		 Out Of Window
	OPEN p_rsetoutofwindow
	 FOR
		 SELECT   v621.c611_study_id s_id, v621.c613_study_period_ds s_ds, v621.c901_study_period s_p_id
				, v621.c613_study_period_id s_p_key, c704_account_id a_id
				,	 DECODE (v621.c614_site_id, '', '', v621.c614_site_id || '-')
				  || get_account_name (v621.c704_account_id)
				  || DECODE (v621.c101_user_id, '', '', '-' || get_user_name (v621.c101_user_id)) a_name
				, v621.c621_patient_ide_no p_ide_no, v621.c621_patient_id p_key, v621.c601_form_id f_id
				, v621.c612_study_form_ds || ' - ' || get_form_name (v621.c601_form_id) f_name
				, TO_CHAR (c621_exp_date_start, 'MM/DD/YYYY') exp_start_date
				, TO_CHAR (v621.c621_exp_date, 'MM/DD/YYYY') exp_date
				, TO_CHAR (v621.c621_exp_date_final, 'MM/DD/YYYY') final_due_date
				, DECODE (SIGN (SYSDATE - DECODE (c621_exp_date_final, '', SYSDATE, c621_exp_date_final))
						, 1, 'Y'
						, 'N'
						 ) over_due
				, c612_seq seq
			 FROM v621_patient_study_list v621
			WHERE (v621.c621_patient_id, v621.c613_study_period_id) NOT IN (
																  SELECT t622.c621_patient_id
																	   , t622.c613_study_period_id
																	FROM t622_patient_list t622
																   WHERE t622.c621_patient_id = v621.c621_patient_id
																   AND NVL(t622.C622_DELETE_FL,'N')='N')
			  AND v621.c621_exp_date_final < SYSDATE
			  AND v621.c101_user_id = DECODE (p_cra_id, '', v621.c101_user_id, p_cra_id)
			  AND v621.c611_study_id = DECODE (p_study_id, '', v621.c611_study_id, p_study_id)
			  AND v621.c621_patient_id NOT IN (SELECT c621_patient_id
												 FROM t622_patient_list
												WHERE c601_form_id = 15 AND c613_study_period_id = 91
												AND NVL(C622_DELETE_FL,'N')='N')
		 ORDER BY s_id, a_name, p_ide_no, s_p_id, seq;

--		 Pending Verification
	OPEN p_rsetverification
	 FOR
		 SELECT   t621.c611_study_id s_id, t613.c613_study_period_ds s_ds, t613.c901_study_period s_p_id
				, t613.c613_study_period_id s_p_key, get_code_seq_no (c901_study_period) seq_no
				, t621.c704_account_id a_id
				,	 DECODE (t614.c614_site_id, '', '', t614.c614_site_id || '-')
				  || get_account_name (t614.c704_account_id)
				  || DECODE (t614.c101_user_id, '', '', '-' || get_user_name (t614.c101_user_id)) a_name
				, t621.c621_patient_ide_no p_ide_no, t621.c621_patient_id p_key, t612.c601_form_id f_id
				, t612.c612_study_form_ds || ' - ' || get_form_name (t612.c601_form_id) f_name
				, TO_CHAR (NVL (t622.c622_date, ''), 'MM/DD/YYYY') e_date
				, get_user_name (NVL (t622.c622_last_updated_by, t622.c622_created_by)) entered_by
			 FROM t621_patient_master t621
				, t612_study_form_list t612
				, t613_study_period t613
				, t614_study_site t614
				, t622_patient_list t622
			WHERE t621.c621_delete_fl = 'N'
			  AND NVL(t622.C622_DELETE_FL,'N')='N'
			  AND t612.c611_study_id = t621.c611_study_id
			  AND t612.c612_study_list_id = t613.c612_study_list_id
			  AND t621.c621_patient_id = t622.c621_patient_id
			  AND t612.c611_study_id = DECODE (p_study_id, '', t612.c611_study_id, p_study_id)
			  AND t613.c613_study_period_id = t622.c613_study_period_id
			  AND t621.c704_account_id = t614.c704_account_id
			  AND NVL (t622.c622_verify_fl, 'N') = 'N'
		 ORDER BY s_id, a_name, p_ide_no, c612_seq;
--
END gm_report_form_due_verify_list;
/
