/* Formatted on 2011/04/01 16:27 (Formatter Plus v4.8.0) */
CREATE OR REPLACE PROCEDURE gm_save_clinic_form (
   p_formid      IN   t622_patient_list.c601_form_id%TYPE,
   p_patientid   IN   t622_patient_list.c621_patient_id%TYPE,
   p_formdate    IN   VARCHAR2,
   p_stdperid    IN   t622_patient_list.c613_study_period_id%TYPE,
   p_initials    IN   t622_patient_list.c622_initials%TYPE,
   p_inputstr    IN   VARCHAR2,
   p_userid      IN   t622_patient_list.c622_created_by%TYPE,
   p_patlistid   IN   t622_patient_list.c622_patient_list_id%TYPE,
   p_action      IN   VARCHAR2,
   p_verifycd    IN   t625_patient_verify_list.c901_verify_level%TYPE,
   p_comments    IN   t902_log.c902_comments%TYPE,
   p_calcvalue   IN   t622_patient_list.c621_cal_value%TYPE,
   p_follow_fl   IN   t622_patient_list.c622_missing_follow_up_fl%TYPE,
   p_event_no    IN   VARCHAR2,
   p_flag        IN   VARCHAR2
)
AS
/*******************************************************************************
 * Description        :This procedure is called to save Patient Form information
 * p_flag = 'D' is a dropdown value when the period is N/A
 * p_flag = 'N' is a flag for non dropdown values when the period is N/A
 ********************************************************************************/
   v_strlen           NUMBER                  := NVL (LENGTH (p_inputstr), 0);
   v_msg              VARCHAR2 (1000);
   v_string           VARCHAR2 (30000)                          := p_inputstr;
   v_quesid           VARCHAR2 (20);
   v_ansgrpid         VARCHAR2 (20);
   v_anslstid         VARCHAR2 (20);
   v_ansds            VARCHAR2 (4000);
   v_patanslistid     VARCHAR2 (20);
   v_verifyfl         t622_patient_list.c622_verify_fl%TYPE;
   v_id               NUMBER;
   v_substring        VARCHAR2 (1000);
   v_patanslistid_n   VARCHAR2 (20);
   v_study_period     t613_study_period.c901_study_period%TYPE;
   v_patient_list_n   VARCHAR2 (20);
   v_update_cnt       NUMBER;
   v_cnt              NUMBER;
   v_count            NUMBER;
   v_patient_listid   t622_patient_list.c622_patient_list_id%TYPE;
   v_updated_by       t622_patient_list.c622_last_updated_by%TYPE;
   v_created_by       t622_patient_list.c622_created_by%TYPE;
BEGIN
-- Check if there is any lock set for the Form. If there is a lock defined, throw an exception.
   BEGIN
      SELECT t622.c622_patient_list_id
        INTO v_patient_listid
        FROM t622_patient_list t622, t625_patient_verify_list t625
       WHERE t622.c622_patient_list_id = t625.c622_patient_list_id
         AND t622.c601_form_id = p_formid
         AND t622.c621_patient_id = p_patientid
         AND t622.c613_study_period_id = p_stdperid
         AND t622.c622_date = TO_DATE (p_formdate, 'MM/DD/YYYY')
         AND t625.c901_verify_level = 6195                            --locked
         AND t625.c625_delete_fl IS NULL;

      IF v_patient_listid IS NOT NULL
      THEN
-- Throw an exception that the form is locked and cannot be updated.
         raise_application_error ('-20257', '');
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_patient_listid := '';
   END;

   SELECT COUNT (t6510.c6510_query_id)
     INTO v_count
     FROM t6510_query t6510
    WHERE t6510.c622_patient_list_id = p_patlistid
      AND t6510.c901_status != 92444
      AND t6510.c6510_void_fl IS NULL;

   -- gm_procedure_log (v_count, p_verifycd || ',' || p_patlistid);
   IF (v_count > 0 AND (p_verifycd = 6192 OR p_verifycd = 6191))
   THEN
      raise_application_error ('-20250', '');
   END IF;

--
   IF (p_verifycd = 6192)
   THEN
      v_verifyfl := 'Y';
   END IF;

--
   IF p_action = 'SaveAns'
   THEN
--
      SELECT s622_patient_list.NEXTVAL
        INTO v_id
        FROM DUAL;

--
      IF p_flag = 'D'
      THEN
         SELECT NVL (MAX (c622_patient_event_no), 0) + 1
           INTO v_patient_list_n
           FROM t622_patient_list
          WHERE c613_study_period_id = p_stdperid
            AND c601_form_id = p_formid
            AND c621_patient_id = p_patientid;
      END IF;

      INSERT INTO t622_patient_list
                  (c622_patient_list_id, c622_date, c622_initials,
                   c601_form_id, c621_patient_id, c613_study_period_id,
                   c622_created_by, c622_created_date, c901_type,
                   c622_delete_fl, c621_cal_value, c622_missing_follow_up_fl,
                   c622_verify_fl, c622_patient_event_no
                  )
           VALUES (v_id, TO_DATE (p_formdate, 'mm/dd/yyyy'), p_initials,
                   p_formid, p_patientid, p_stdperid,
                   p_userid, SYSDATE, 6151,
                   'N', p_calcvalue, p_follow_fl,
                   p_follow_fl, v_patient_list_n
                  -- if missing follow up is 'Y' then the verify flag should be 'Y'
                  );

      /************************************************************
       * Below code is used to save patient answer list
       ************************************************************/
      IF NVL (p_follow_fl, 'N') <> 'Y' AND v_strlen > 0
      THEN
         --
         WHILE INSTR (v_string, '|') <> 0
         LOOP
            --
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
            v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
            v_quesid := NULL;
            v_ansgrpid := NULL;
            v_anslstid := NULL;
            v_ansds := NULL;
            v_quesid := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_ansgrpid :=
                         SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_anslstid :=
                         SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_ansds := TRIM (v_substring);

            --
            INSERT INTO t623_patient_answer_list
                        (c623_patient_ans_list_id, c622_patient_list_id,
                         c602_question_id, c603_answer_grp_id,
                         c605_answer_list_id, c904_answer_ds, c601_form_id,
                         c621_patient_id, c623_created_by,
                         c623_created_date, c623_delete_fl
                        )
                 VALUES (s623_patient_answer_list.NEXTVAL, v_id,
                         v_quesid, v_ansgrpid,
                         v_anslstid, TRIM (v_ansds), p_formid,
                         p_patientid, p_userid,
                         SYSDATE, 'N'
                        );
         --
         END LOOP;
      END IF;
   ---
   ELSIF p_action = 'EditAns'
   THEN
      IF p_flag = 'N'
      THEN
         SELECT c622_patient_list_id
           INTO v_patanslistid_n
           FROM t622_patient_list
          WHERE c601_form_id = p_formid
            AND c621_patient_id = p_patientid
            AND c613_study_period_id = p_stdperid
            --AND c622_date = TO_DATE (p_formdate, 'mm/dd/yyyy')
            AND c622_delete_fl = 'N';
      ELSIF p_flag = 'D'
      THEN
         SELECT c622_patient_list_id
           INTO v_patanslistid_n
           FROM t622_patient_list
          WHERE c601_form_id = p_formid
            AND c621_patient_id = p_patientid
            AND c613_study_period_id = p_stdperid
            AND c622_patient_event_no = p_event_no
            AND c622_delete_fl = 'N';

         v_patient_list_n := p_event_no;
      ELSE
         SELECT c622_patient_list_id
           INTO v_patanslistid_n
           FROM t622_patient_list
          WHERE c601_form_id = p_formid
            AND c621_patient_id = p_patientid
            AND c613_study_period_id = p_stdperid
            AND c622_delete_fl = 'N';
      END IF;

      -- Same person cannot verify his work
      SELECT c622_created_by, c622_last_updated_by
        INTO v_created_by, v_updated_by
        FROM t622_patient_list
       WHERE c622_patient_list_id = v_patanslistid_n;

      IF ( (p_follow_fl IS NULL) AND ( (    v_created_by = p_userid
              AND v_updated_by IS NULL
              AND v_verifyfl = 'Y'
             )
          OR (    v_updated_by IS NOT NULL
              AND v_updated_by = p_userid
              AND v_verifyfl = 'Y'
             )
           )
         )
      THEN
         raise_application_error ('-20280', '');
      -- This form needs to be verified by a different user.
      END IF;

      --
      -- Below code to save the Answer List Information
      --
      IF v_strlen > 0
      THEN
         --
         WHILE INSTR (v_string, '|') <> 0
         LOOP
            --
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
            v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
            v_patanslistid := NULL;
            v_anslstid := NULL;
            v_ansds := NULL;
            v_patanslistid :=
                         SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_anslstid :=
                         SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_ansds := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_quesid := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_ansgrpid := TRIM (v_substring);

            --
            UPDATE t623_patient_answer_list
               SET c605_answer_list_id = v_anslstid,
                   c904_answer_ds = TRIM (v_ansds),
                   c623_last_updated_by = p_userid,
                   c623_last_updated_date = SYSDATE
             WHERE c623_patient_ans_list_id = v_patanslistid;

            IF (SQL%ROWCOUNT = 0)
            THEN
               INSERT INTO t623_patient_answer_list
                           (c623_patient_ans_list_id,
                            c622_patient_list_id, c602_question_id,
                            c603_answer_grp_id, c605_answer_list_id,
                            c904_answer_ds, c601_form_id, c621_patient_id,
                            c623_created_by, c623_created_date,
                            c623_delete_fl
                           )
                    VALUES (s623_patient_answer_list.NEXTVAL,
                            v_patanslistid_n, v_quesid,
                            v_ansgrpid, v_anslstid,
                            TRIM (v_ansds), p_formid, p_patientid,
                            p_userid, SYSDATE,
                            'N'
                           );
            END IF;
         --
         END LOOP;
      --
      END IF;

--
      UPDATE t622_patient_list
         SET c622_date = TO_DATE (p_formdate, 'mm/dd/yyyy'),
             c622_initials = p_initials,
             c622_last_updated_by = p_userid,
             c622_last_updated_date = SYSDATE,
             c622_verify_fl = DECODE(p_follow_fl,'Y',p_follow_fl,v_verifyfl),--Updating the status as verified when missing follow up is checked.
             c621_cal_value = p_calcvalue,
             c622_missing_follow_up_fl = p_follow_fl
       WHERE c622_patient_list_id = v_patanslistid_n;
   END IF;

--
 /*
    *Update out of window flag if the entered formdate is less than the
    *'window period' (calculated and stored in c621_exp_date_final column of v621_patient_study_list)
    * 'D' is a dropdown value and has not out-of-window option
    */
   IF (p_flag = '' OR p_flag IS NULL)
   THEN
-- HD# 13546 When Missing Followup is checked, we should not set out of window.
      IF p_follow_fl IS NULL
      THEN
         UPDATE t622_patient_list
            SET c622_out_of_window_fl = 'Y'
          WHERE c622_patient_list_id IN (
                   SELECT c622_patient_list_id
                     FROM t622_patient_list t622,
                          v621_patient_study_list v621
                    WHERE t622.c613_study_period_id =
                                                     v621.c613_study_period_id
                      AND t622.c621_patient_id = v621.c621_patient_id
                      AND v621.c621_patient_id = p_patientid
                      AND v621.c613_study_period_id = p_stdperid
                      AND (   v621.c621_exp_date_final <
                                            TO_DATE (p_formdate, 'mm/dd/yyyy')
                           OR v621.c621_exp_date_start >
                                            TO_DATE (p_formdate, 'mm/dd/yyyy')
                          ));
      END IF;

      UPDATE t622_patient_list
         SET c622_out_of_window_fl = ''
       WHERE c622_patient_list_id IN (
                SELECT c622_patient_list_id
                  FROM t622_patient_list t622, v621_patient_study_list v621
                 WHERE t622.c613_study_period_id = v621.c613_study_period_id
                   AND t622.c621_patient_id = v621.c621_patient_id
                   AND v621.c621_patient_id = p_patientid
                   AND v621.c613_study_period_id = p_stdperid
                   AND (    v621.c621_exp_date_final >=
                                            TO_DATE (p_formdate, 'mm/dd/yyyy')
                        AND v621.c621_exp_date_start <=
                                            TO_DATE (p_formdate, 'mm/dd/yyyy')
                       ))
         AND c622_out_of_window_fl IS NOT NULL;
   END IF;

   /************************************************************
      * Below code is used to save patient verification list
      ************************************************************/
   IF p_flag = 'N'
   THEN
      SELECT c622_patient_list_id
        INTO v_patanslistid_n
        FROM t622_patient_list
       WHERE c601_form_id = p_formid
         AND c621_patient_id = p_patientid
         AND c613_study_period_id = p_stdperid
         AND c622_date = TO_DATE (p_formdate, 'mm/dd/yyyy')
         AND c622_delete_fl = 'N';
   ELSIF p_flag = 'D'
   THEN
      SELECT c622_patient_list_id
        INTO v_patanslistid_n
        FROM t622_patient_list
       WHERE c601_form_id = p_formid
         AND c621_patient_id = p_patientid
         AND c613_study_period_id = p_stdperid
         AND c622_patient_event_no = v_patient_list_n
         AND c622_delete_fl = 'N';
   ELSE
      SELECT c622_patient_list_id
        INTO v_patanslistid_n
        FROM t622_patient_list
       WHERE c601_form_id = p_formid
         AND c621_patient_id = p_patientid
         AND c613_study_period_id = p_stdperid
         AND c622_delete_fl = 'N';
   END IF;

   --
   SELECT s625_patient_verify.NEXTVAL
     INTO v_id
     FROM DUAL;

--
   INSERT INTO t625_patient_verify_list
               (c625_pat_verify_list_id, c622_patient_list_id, c101_user_id,
                c625_date, c901_verify_level
               )
        VALUES (v_id, v_patanslistid_n, p_userid,
                TRUNC (SYSDATE), p_verifycd
               );

   --
   -- Code to save the user entered comments information
   --
   gm_update_log (v_id, p_comments, 1204, p_userid, v_msg);
   gm_pkg_cr_edit_check.gm_sav_incidents (v_patanslistid_n, p_userid);
END gm_save_clinic_form;
/
