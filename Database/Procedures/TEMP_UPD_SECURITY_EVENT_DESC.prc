CREATE OR REPLACE PROCEDURE TEMP_UPD_SECURITY_EVENT_DESC
AS
CURSOR v_cur IS
 SELECT MYTAB.GRPID, GET_CODE_NAME (MYTAB.GRPID) CODENM, MAX (DECODE (MYTAB.RULID, 'FN_INITIATE', MYTAB.RULVAL)) FN_INIT
  , MAX (DECODE (MYTAB.RULID, 'FN_CONTROL', MYTAB.RULVAL)) FN_CONT, MAX (DECODE (MYTAB.RULID, 'FN_VERIFY', MYTAB.RULVAL
    )) FN_VERIFY, MAX (DECODE (MYTAB.RULID, 'TRANS_PREFIX', MYTAB.RULVAL)) TRN_PREFIX
   FROM
    (
         SELECT T906.C906_RULE_GRP_ID GRPID, T906.C906_RULE_VALUE RULVAL, T906.C906_RULE_ID RULID
           FROM T906_RULES T906, T901_CODE_LOOKUP T901
          WHERE T906.C906_RULE_GRP_ID = TO_CHAR (T901.C901_CODE_ID)
            AND T906.C906_RULE_ID    IN ('FN_INITIATE', 'FN_CONTROL', 'FN_VERIFY', 'TRANS_PREFIX')
            AND T901.C901_VOID_FL    IS NULL
            AND T906.C906_VOID_FL    IS NULL
       ORDER BY TO_NUMBER (T906.C906_RULE_GRP_ID)
    )
    MYTAB
GROUP BY MYTAB.GRPID;
BEGIN

	FOR v_item IN v_cur
	LOOP
		UPDATE T1520_FUNCTION_LIST 
		SET C1520_FUNCTION_DESC = v_item.TRN_PREFIX || 'Initiate_Access', 
			C1520_LAST_UPDATED_BY = '614728',
			C1520_LAST_UPDATED_DATE = SYSDATE
		WHERE 
			C1520_FUNCTION_ID = v_item.FN_INIT
			AND C1520_VOID_FL IS NULL;
		
		UPDATE T1520_FUNCTION_LIST 
		SET C1520_FUNCTION_DESC = v_item.TRN_PREFIX || 'Control_Access', 
			C1520_LAST_UPDATED_BY = '614728',
			C1520_LAST_UPDATED_DATE = SYSDATE
		WHERE 
			C1520_FUNCTION_ID = v_item.FN_CONT
			AND C1520_VOID_FL IS NULL;
			
		UPDATE T1520_FUNCTION_LIST 
		SET C1520_FUNCTION_DESC = v_item.TRN_PREFIX || 'Verify_Access', 
			C1520_LAST_UPDATED_BY = '614728',
			C1520_LAST_UPDATED_DATE = SYSDATE
		WHERE 
			C1520_FUNCTION_ID = v_item.FN_VERIFY
			AND C1520_VOID_FL IS NULL;
			
	END LOOP;
END TEMP_UPD_SECURITY_EVENT_DESC;
/
