// PRODUCTION SERVER PATHS
var URL_WSServer = "https://wbs.spineit.net/resources/";  //Production path for Web Service
var URL_FileServer = "http://mobile.spineit.net/globusapp/";  //Production path for Images
var URL_CRMFileServer = "http://mobile.spineit.net/globusapp/crm/"; //Production path for CRM files and Images
var URL_Cloud_FileServer = "http://sync.globusone.com/globusapp/"; // NGINX file path
//var URL_Cloud_FileServer = "https://gmed.egnyte.com/pubapi/v1/fs-content/Shared/GMI/spineIT/Prod/www/globusapp/"; //Egnyte Cloud File Path
var URL_Portal = "https://erp.spineit.net/"; //Production Portal URL
strEnvironment = "Production";
var intServerCountryCode = '80120';
var gmReportToMail = "ITSC@globusmedical.com";