/**********************************************************************************
 * File: gmCreo.js
 * Description: 
 * Author:     	karthik
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device'       
      ], 
    
    function ($, _, Backbone, Handlebars, Global, Device) {

	'use strict';

	var GmCreoRouter = Backbone.Router.extend({
        routes: {
			"creosetguide": "creosetguide",
		},
        
         events : {
			"click .btn-lnk-creo" : "creoHome"	
		},
        
        
		creosetguide: function () {
            $("#div-inv-title-main").css("display", "none");
            $("#div-inv-main-content").html('<object data="globusMobileApp/creoclient/index.html" style="width:101%;height:100%;" />');
            $(".btn-app-link").fadeToggle("fast");
            $(".btn-app-link").css("display", "none");
		}, 
        
		
	});

	return GmCreoRouter;
});
