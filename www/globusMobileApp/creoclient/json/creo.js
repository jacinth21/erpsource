    var mainApp = angular.module("mainApp", ['ngRoute','ui.grid','ui.grid.pinning']);
    var mainCreoJsonData = new Array();
    var creoValuesArray=new Array();
    var localcreo = "";
   

	mainApp.config(function($routeProvider) {
		$routeProvider
			.when('/home', {
				templateUrl: 'pages/home.html',
				controller: 'CreohomeController'
			})
			.when('/creo', {
				templateUrl: 'pages/creo.html',
				controller: 'CreoController'
			})
			.when('/creofile', {
				templateUrl: 'pages/creofile.html',
				controller: 'CreofileController'
			})
			.when('/approach/:linkId', {
				templateUrl: 'pages/approach.html',
				controller: 'ApproachController'
			})
			.when('/creovalue/:linkId', {
				templateUrl: 'pages/creo/creovalue.html',
				controller: 'CreovalueController'
			})
			
			.when('/systems', {
				templateUrl: 'pages/systems.html',
				controller: 'ApproachController'
			})
            .when('/colosseum',{ 
                templateUrl: 'pages/colosseum.html',
				controller: 'ColosseumController'
         })
			
			.otherwise({
				redirectTo: '/home'
			});
	});
    /* For Colosseum */

    mainApp.controller('ColosseumController', function($scope,$http,$window,$rootScope,$location,uiGridConstants) {
        $scope.init = function () {
            $http.get(url+'json/colosseum.json').success(function (data){
                $scope.myData =  data.Sheet1;
            });
             
            $scope.gridOptions = {
            data: 'myData',
        enableFiltering: true,
        columnDefs: [
            { name: "Pre-Cut Cage",displayName:"Pre-Cut Cage", width: 200,enableFiltering:false},
            { name: "Upper EndCap",displayName:"Upper EndCap", width: 200,enableFiltering:false},
            { name: "Lower EndCap",displayName:"Lower EndCap", width: 200,enableFiltering:false},
            { name: "Footprint",displayName:"Footprint",width: 110, sort: { direction: uiGridConstants.ASC, priority: 1 }},
                    { displayName:"Total Height",name: "Total_Height", width: 110 },
                    { displayName:"Total Angulation",name: "Total_Angulation", width: 110 },
                    {displayName:"Height",name: "Pre_Cut_HT", width: 110 },
                    { displayName:"Part #",name: "Pre_Cut_Part_Id", width: 110},
                    { displayName:"Height/ Angulation",name: "UP_HT_Angulation", width: 110 },
                    { displayName:"Part #",name: "UP_Endcap_Part_Id", width: 110 },
                    { displayName:"Height/ Angulation",name: "LW_HT_Angulation", width: 110 },
                    { displayName:"Part #",name: "LW_Endcap_Part_Id", width: 110 }
                    ],
        onRegisterApi: function (gridApi) {
$scope.grid1Api = gridApi;
}
    };
          
            };

});
 /* For Creo */

	mainApp.controller('CreohomeController', function($scope,$location) {
		
	});
	mainApp.controller('CreoController', function($scope,$location) {
		
	});
	mainApp.controller('CreofileController', function($scope,$location) {
		
	});
    
	mainApp.controller('ApproachController', function($scope, $location, $routeParams, $http) {
		$http.get(url+'json/approach.json').success(function (data){
			    var linkId = $routeParams.linkId; 
				$scope.values = data;
				if(linkId=='approach'){
				$scope.approach = data.approach;
				}else if (linkId=="open"){
				$scope.approach = data.open;
				}else if (linkId=="openyes"){
				$scope.approach = data.openyes;
				}else if (linkId=="openno"){
				$scope.approach = data.openno;
				}else if (linkId=="percutaneous"){
				$scope.approach = data.percutaneous;
				}else if (linkId=="perno"){
				$scope.approach = data.perno;
				}else if (linkId=="degan"){
				$scope.approach = data.degan;
				}else if (linkId=="pediclescrew"){
				$scope.approach = data.pediclescrew;
				}else if (linkId=="pediclelocthread"){
				$scope.approach = data.pediclelocthread;
				}else if (linkId=="tiscrew"){
				$scope.approach = data.tiscrew;
				}else if (linkId=="tiscrewrd55"){
				$scope.approach = data.tiscrewrd55;
				}else if (linkId=="tiscrewmodular"){
				$scope.approach = data.tiscrewmodular;
				}else if (linkId=="screwtype"){
				$scope.approach = data.screwtype;
				}else if(linkId=="screwtypecocr"){
                $scope.approach = data.screwtypecocr
                } else if (linkId=="cocrrod"){
				$scope.approach = data.cocrrod;
				} else if(linkId=="cocrrodmodular"){
                $scope.approach = data.cocrrodmodular;
                } else if (linkId=="rod4"){
				$scope.approach = data.rod4;
				}else if (linkId=="rod5"){
				$scope.approach = data.rod5;
				}else if (linkId=="pedilocnonth"){
				$scope.approach = data.pedilocnonth;
				}else if (linkId=="pedilocnonthti"){
				$scope.approach = data.pedilocnonthti;
				}else if (linkId=="pedilocnonthcocr"){
				$scope.approach = data.pedilocnonthcocr;
				}else if (linkId=="midlinescrew"){
				$scope.approach = data.midlinescrew;
				}else if (linkId=="deformity"){
				$scope.approach = data.deformity;
				}else if (linkId=="deformitylocth"){
				$scope.approach = data.deformitylocth;
				}else if (linkId=="deformityscrewtype"){
				$scope.approach = data.deformityscrewtype;
				}else if (linkId=="deformitylocthtimodular"){
				$scope.approach = data.deformitylocthtimodular;
				}else if (linkId=="deformitylocnonth"){
				$scope.approach = data.deformitylocnonth;
				}else if (linkId=="deformitylocnonthti"){
				$scope.approach = data.deformitylocnonthti;
				}else if (linkId=="deformityscrewtypecocr"){
				$scope.approach = data.deformityscrewtypecocr;
				}else if(linkId=="deformitylocthcocrmodular"){
                $scope.approach = data.deformitylocthcocrmodular
                }else if (linkId=="deformitylocnonthcocr"){
				$scope.approach = data.deformitylocnonthcocr;
				}else if (linkId=="deformitylocthti"){
				$scope.approach = data.deformitylocthti;
				}else if (linkId=="ti5"){
				$scope.approach = data.ti5;
				}else if (linkId=="ti6"){
				$scope.approach = data.ti6;
				}else if (linkId=="ti6pre"){
				$scope.approach = data.ti6pre;
				}else if (linkId=="deformitylocthcocr"){
				$scope.approach = data.deformitylocthcocr;
				}else if (linkId=="cocr4"){
				$scope.approach = data.cocr4;
				}else if (linkId=="cocr5"){
				$scope.approach = data.cocr5;
				}else if (linkId=="cocr6"){
				$scope.approach = data.cocr6;
				}else {
					
				}
				
			});
			});
			
			mainApp.controller('CreovalueController', function($scope, $routeParams, $location, $http) {
              
			$http.get(url+'json/creo.json').success(function (data){
		    $scope.values = data;
			var linkId = $routeParams.linkId; 
            var setidvalue;
			
			if(linkId=='creofenestt'){
			$scope.head = data.headcreofenestt;
			$scope.standardhead = data.standardheadcreofenestt;
			$scope.requiredhead = data.REQUIREDheadcreofenestt;
			$scope.optionalhead = data.OPTIONALheadcreofenestt;
			$scope.demosetshead = data.DEMOSETSheadcreofenestt;
			$scope.literaturehead = data.literatureheadcreofenestt;	
			$scope.standard = data.standardcreofenestt;
			$scope.required =data.REQUIREDcreofenestt;
			$scope.valuesone =data.valuesonecreofenestt;
			$scope.valuestwo =data.valuestwocreofenestt;
			$scope.optional =data.OPTIONALcreofenestt;
			$scope.demosets =data.DEMOSETScreofenestt;
			$scope.literature =data.literaturecreofenestt;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
                
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
                            }
                     }
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					message = message1+message2;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
                 
			}
			}
			else if(linkId=='creofensnonth'){
			$scope.head = data.headcreofensnonth;
			$scope.standardhead = data.standardheadcreofensnonth;
			$scope.requiredhead =data.REQUIREDheadcreofensnonth;
		    $scope.optionalhead =data.OPTIONALheadcreofensnonth;
			$scope.demosetshead =data.DEMOSETSheadcreofensnonth;
			$scope.literaturehead =data.literatureheadcreofensnonth;
			$scope.standard = data.standardcreofensnonth;
			$scope.required =data.REQUIREDcreofensnonth;
			$scope.valuesone =data.valuesonecreofensnonth;
			$scope.valuestwo =data.valuestwocreofensnonth;
			$scope.optional =data.OPTIONALcreofensnonth;
			$scope.demosets =data.DEMOSETScreofensnonth;
			$scope.literature =data.literaturecreofensnonth;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                           updateCreoJson(std1);
						}
					}
					message = message1+message2;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
                        
					}
			}
			}
			else if(linkId=='creofensmis'){
			$scope.head = data.headcreofensmis;
			$scope.standardhead = data.standardheadcreofensmis;
			$scope.requiredhead =data.REQUIREDheadcreofensmis;
			$scope.optionalhead =data.OPTIONALheadcreofensmis;
			$scope.demosetshead =data.DEMOSETSheadcreofensmis;
			$scope.literaturehead =data.literatureheadcreofensmis;
			$scope.standard = data.standardcreofensmis;
			$scope.required =data.REQUIREDcreofensmis;
			$scope.valuesone =data.valuesonecreofensmis;
			$scope.valuestwo =data.valuestwocreofensmis;
			$scope.optional =data.OPTIONALcreofensmis;
			$scope.demosets =data.DEMOSETScreofensmis;
			$scope.literature =data.literaturecreofensmis;
			
			$scope.GetValue = function () {
                var that = this;
               
				
			  var message = "";
			  var message1 = "";
			  var message2 = "";
                var id,name,qty,date,reqdt,systype, setidvalue;
                var indexValue = $(event.currentTarget).parent().parent().attr('indexValue');

			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					message = message1+message2;
					if(message == '' ){
						showErrorAlert();
					}
					else{
                        showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creoside'){
			$scope.head = data.headcreoside;
			$scope.standardhead = data.standardheadcreoside;
			$scope.standsethead =data.standsetheadcreoside;
			$scope.optionalhead =data.OPTIONALheadcreoside;
			$scope.literaturehead =data.literatureheadcreoside;
			$scope.standard = data.standardcreoside;
			$scope.standset =data.Standsetcreoside;
			$scope.optional =data.OPTIONALcreoside;
			$scope.literature =data.literaturecreoside;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  var message3 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					for (var i = 0; i < $scope.standset.length; i++) {
						if ($scope.standset[i].Selected) {
							var std2 = $scope.standset[i].name;
							message3 += "Value3: " + std2 +  "\n";
                            updateCreoJson(std2);
						}
					}
					message = message1+message2+message3;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creo5deformity'){
			$scope.head = data.headcreo5deformity;
			$scope.standardhead = data.standardheadcreo5deformity;
			$scope.optionalhead =data.OPTIONALheadcreo5deformity;
			$scope.decorationhead=data.DECORATIONheadcreo5deformity;
			$scope.demosetshead =data.DEMOSETSheadcreo5deformity;
			$scope.literaturehead =data.literatureheadcreo5deformity;
			$scope.standard = data.standardcreo5deformity;
			$scope.optional =data.OPTIONALcreo5deformity;
			$scope.decoration=data.DECORATIONcreo5deformity;
			$scope.demosets =data.DEMOSETScreo5deformity;
			$scope.literature =data.literaturecreo5deformity;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  var message3 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
                            
						}
					}
					for (var i = 0; i < $scope.decoration.length; i++) {
						if ($scope.decoration[i].Selected) {
							var std2 = $scope.decoration[i].name;
							message3 += "Value3: " + std2 +  "\n";
                            updateCreoJson(std2);
						}
					}
					message = message1+message2+message3;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creo5degan'){
			$scope.head = data.headcreo5degan;
			$scope.standardhead = data.standardheadcreo5degan;
			$scope.optionalhead =data.OPTIONALheadcreo5degan;
			$scope.demosetshead =data.DEMOSETSheadcreo5degan;
			$scope.literaturehead =data.literatureheadcreo5degan;
			$scope.standard = data.standardcreo5degan;
			$scope.optional =data.OPTIONALcreo5degan;
			$scope.demosets =data.DEMOSETScreo5degan;
			$scope.literature =data.literaturecreo5degan;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                             updateCreoJson(std1);
						}
					}
					message = message1+message2;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
                       
					}
			}
			}
			else if(linkId=='creoamp5deformity'){
			$scope.head = data.headcreoamp5deformity;
			$scope.standardhead = data.standardheadcreoamp5deformity;
			$scope.optionalhead =data.OPTIONALheadcreoamp5deformity;
			$scope.decorationhead=data.DECORATIONheadcreoamp5deformity;
			$scope.demosetshead =data.DEMOSETSheadcreoamp5deformity;
			$scope.literaturehead =data.literatureheadcreoamp5deformity;
			$scope.standard = data.standardcreoamp5deformity;
			$scope.optional =data.OPTIONALcreoamp5deformity;
			$scope.decoration=data.DECORATIONcreoamp5deformity;
			$scope.demosets =data.DEMOSETScreoamp5deformity;
			$scope.literature =data.literaturecreoamp5deformity;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  var message3 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
                            
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					for (var i = 0; i < $scope.decoration.length; i++) {
						if ($scope.decoration[i].Selected) {
							var std2 = $scope.decoration[i].name;
							message3 += "Value3: " + std2 +  "\n";
                            updateCreoJson(std2);
						}
					}
					message = message1+message2+message3;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
                        
					}
			}
			}
			else if(linkId=='creoamp5degan'){
			$scope.head = data.headcreoamp5degan;
			$scope.standardhead = data.standardheadcreoamp5degan;
			$scope.optionalhead =data.OPTIONALheadcreoamp5degan;
			$scope.demosetshead =data.DEMOSETSheadcreoamp5degan;
			$scope.literaturehead =data.literatureheadcreoamp5degan;
			$scope.standard = data.standardcreoamp5degan;
			$scope.optional =data.OPTIONALcreoamp5degan;
			$scope.demosets =data.DEMOSETScreoamp5degan;
			$scope.literature =data.literaturecreoamp5degan;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
                            
						}
					}
					message = message1+message2;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
                         
					}
			}
			}
			else if(linkId=='creothdegan6'){
			$scope.head = data.headcreothdegan6;
			$scope.standardhead = data.standardheadcreothdegan6;
			$scope.optionalhead =data.OPTIONALheadcreothdegan6;
			$scope.demosetshead =data.DEMOSETSheadcreothdegan6;
			$scope.literaturehead =data.literatureheadcreothdegan6;
			$scope.standard = data.standardcreothdegan6;
			$scope.optional =data.OPTIONALcreothdegan6;
			$scope.demosets =data.DEMOSETScreothdegan6;
			$scope.literature =data.literaturecreothdegan6;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					message = message1+message2;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creoampthcocr4deformity'){
			$scope.head = data.headcreoampthcocr4deformity;
			$scope.standardhead = data.standardheadcreoampthcocr4deformity;
			$scope.optionalhead =data.OPTIONALheadcreoampthcocr4deformity;
			$scope.decorationhead =data.DECORATIONheadcreoampthcocr4deformity;
			$scope.literaturehead =data.literatureheadcreoampthcocr4deformity;
			$scope.standard = data.standardcreoampthcocr4deformity;
			$scope.optional =data.OPTIONALcreoampthcocr4deformity;
			$scope.decoration =data.DECORATIONcreoampthcocr4deformity;
			$scope.literature =data.literaturecreoampthcocr4deformity;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  var message3 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                           updateCreoJson(std1);
						}
					}
					for (var i = 0; i < $scope.decoration.length; i++) {
						if ($scope.decoration[i].Selected) {
							var std2 = $scope.decoration[i].name;
							message3 += "Value3: " + std2 +  "\n";
                            updateCreoJson(std2);
						}
					}
					message = message1+message2+message3;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creoampthcocr4degan'){
			$scope.head = data.headcreoampthcocr4degan;
			$scope.standardhead = data.standardheadcreoampthcocr4degan;
			$scope.optionalhead =data.OPTIONALheadcreoampthcocr4degan;
			$scope.literaturehead =data.literatureheadcreoampthcocr4degan;
			$scope.standard = data.standardcreoampthcocr4degan;
			$scope.optional =data.OPTIONALcreoampthcocr4degan;
			$scope.literature =data.literaturecreoampthcocr4degan;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					message = message1+message2;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creoampthcocrdeformity'){
			$scope.head = data.headcreoampthcocrdeformity;
			$scope.standardhead = data.standardheadcreoampthcocrdeformity;
			$scope.optionalhead =data.OPTIONALheadcreoampthcocrdeformity;
			$scope.decorationhead=data.DECORATIONheadcreoampthcocrdeformity;
			$scope.demosetshead =data.DEMOSETSheadcreoampthcocrdeformity;
			$scope.literaturehead =data.literatureheadcreoampthcocrdeformity;
			$scope.standard = data.standardcreoampthcocrdeformity;
			$scope.optional =data.OPTIONALcreoampthcocrdeformity;
			$scope.decoration=data.DECORATIONcreoampthcocrdeformity;
			$scope.demosets =data.DEMOSETScreoampthcocrdeformity;
			$scope.literature =data.literaturecreoampthcocrdeformity;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  var message3 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					for (var i = 0; i < $scope.decoration.length; i++) {
						if ($scope.decoration[i].Selected) {
							var std2 = $scope.decoration[i].name;
							message3 += "Value3: " + std2 +  "\n";
                            updateCreoJson(std2);
						}
					}
					message = message1+message2+message3;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creoampthcocrdeformity6'){
			$scope.head = data.headcreoampthcocrdeformity6;
			$scope.standardhead = data.standardheadcreoampthcocrdeformity6;
			$scope.optionalhead =data.OPTIONALheadcreoampthcocrdeformity6;
			$scope.decorationhead=data.DECORATIONheadcreoampthcocrdeformity6;
			$scope.literaturehead =data.literatureheadcreoampthcocrdeformity6;
			$scope.standard = data.standardcreoampthcocrdeformity6;
			$scope.optional =data.OPTIONALcreoampthcocrdeformity6;
			$scope.decoration=data.DECORATIONcreoampthcocrdeformity6;
			$scope.literature =data.literaturecreoampthcocrdeformity6;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  var message3 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					for (var i = 0; i < $scope.decoration.length; i++) {
						if ($scope.decoration[i].Selected) {
							var std2 = $scope.decoration[i].name;
							message3 += "Value3: " + std2 +  "\n";
                            updateCreoJson(std2);
						}
					}
					message = message1+message2+message3;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creoampthcocrdegan'){
			$scope.head = data.headcreoampthcocrdegan;
			$scope.standardhead = data.standardheadcreoampthcocrdegan;			
			$scope.optionalhead =data.OPTIONALheadcreoampthcocrdegan;
			$scope.demosetshead =data.DEMOSETSheadcreoampthcocrdegan;
			$scope.literaturehead =data.literatureheadcreoampthcocrdegan;
			$scope.standard = data.standardcreoampthcocrdegan;			
			$scope.optional =data.OPTIONALcreoampthcocrdegan;
			$scope.demosets =data.DEMOSETScreoampthcocrdegan;
			$scope.literature =data.literaturecreoampthcocrdegan;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					message = message1+message2;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creoampthdeformity'){
			$scope.head = data.headcreoampthdeformity;
			$scope.standardhead = data.standardheadcreoampthdeformity;
			$scope.optionalhead =data.OPTIONALheadcreoampthdeformity;
			$scope.decorationhead=data.DECORATIONheadcreoampthdeformity;
			$scope.demosetshead =data.DEMOSETSheadcreoampthdeformity;
			$scope.literaturehead =data.literatureheadcreoampthdeformity;
			$scope.standard = data.standardcreoampthdeformity;
			$scope.optional =data.OPTIONALcreoampthdeformity;
			$scope.decoration=data.DECORATIONcreoampthdeformity;
			$scope.demosets =data.DEMOSETScreoampthdeformity;
			$scope.literature =data.literaturecreoampthdeformity;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  var message3 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					for (var i = 0; i < $scope.decoration.length; i++) {
						if ($scope.decoration[i].Selected) {
							var std2 = $scope.decoration[i].name;
							message3 += "Value3: " + std2 +  "\n";
                            updateCreoJson(std2);
						}
					}
					message = message1+message2+message3;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creoampthdeformity6'){
			$scope.head = data.headcreoampthdeformity6;
			$scope.standardhead = data.standardheadcreoampthdeformity6;
			$scope.optionalhead =data.OPTIONALheadcreoampthdeformity6;
			$scope.decorationhead=data.DECORATIONheadcreoampthdeformity6;
			$scope.demosetshead =data.DEMOSETSheadcreoampthdeformity6;
			$scope.literaturehead =data.literatureheadcreoampthdeformity6;
			$scope.standard = data.standardcreoampthdeformity6;
			$scope.optional =data.OPTIONALcreoampthdeformity6;
			$scope.decoration=data.DECORATIONcreoampthdeformity6;
			$scope.demosets =data.DEMOSETScreoampthdeformity6;
			$scope.literature =data.literaturecreoampthdeformity6;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  var message3 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					for (var i = 0; i < $scope.decoration.length; i++) {
						if ($scope.decoration[i].Selected) {
							var std2 = $scope.decoration[i].name;
							message3 += "Value3: " + std2 +  "\n";
                            updateCreoJson(std2);
						}
					}
					message = message1+message2+message3;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creoampthdegan'){
			$scope.head = data.headcreoampthdegan;
			$scope.standardhead = data.standardheadcreoampthdegan;			
			$scope.optionalhead =data.OPTIONALheadcreoampthdegan;
			$scope.demosetshead =data.DEMOSETSheadcreoampthdegan;
			$scope.literaturehead =data.literatureheadcreoampthdegan;
			$scope.standard = data.standardcreoampthdegan;			
			$scope.optional =data.OPTIONALcreoampthdegan;
			$scope.demosets =data.DEMOSETScreoampthdegan;
			$scope.literature =data.literaturecreoampthdegan;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					message = message1+message2;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creofens5'){
			$scope.head = data.headcreofens5;
			$scope.standardhead = data.standardheadcreofens5;
			$scope.requiredhead =data.REQUIREDheadcreofens5;
			$scope.optionalhead =data.OPTIONALheadcreofens5;
			$scope.demosetshead =data.DEMOSETSheadcreofens5;
			$scope.literaturehead =data.literatureheadcreofens5;
			$scope.standard = data.standardcreofens5;
			$scope.required =data.REQUIREDcreofens5;
			$scope.valuesone =data.valuesonecreofens5;
			$scope.valuestwo =data.valuestwocreofens5;
			$scope.optional =data.OPTIONALcreofens5;
			$scope.demosets =data.DEMOSETScreofens5;
			$scope.literature =data.literaturecreofens5;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					message = message1+message2;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creomcs'){
			$scope.head = data.headcreomcs;
			$scope.standardhead = data.standardheadcreomcs;
			$scope.optionalhead =data.OPTIONALheadcreomcs;
			$scope.demosetshead =data.DEMOSETSheadcreomcs;
			$scope.literaturehead =data.literatureheadcreomcs;
			$scope.standard = data.standardcreomcs;
			$scope.optional =data.OPTIONALcreomcs;
			$scope.demosets =data.DEMOSETScreomcs;
			$scope.literature =data.literaturecreomcs;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					message = message1+message2;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creomcsth'){
			$scope.head = data.headcreomcsth;
			$scope.standardhead = data.standardheadcreomcsth;
			$scope.optionalhead =data.OPTIONALheadcreomcsth;
			$scope.literaturehead =data.literatureheadcreomcsth;
			$scope.standard = data.standardcreomcsth;
			$scope.optional =data.OPTIONALcreomcsth;
			$scope.literature =data.literaturecreomcsth;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					message = message1+message2;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creoampthdeformity'){
			$scope.head = data.headcreoampthdeformity;
			$scope.standardhead = data.standardheadcreoampthdeformity;
			$scope.optionalhead =data.OPTIONALheadcreoampthdeformity;
			$scope.decorationhead=data.DECORATIONheadcreoampthdeformity;
			$scope.demosetshead =data.DEMOSETSheadcreoampthdeformity;
			$scope.literaturehead =data.literatureheadcreoampthdeformity;
			$scope.standard = data.standardcreoampthdeformity;
			$scope.optional =data.OPTIONALcreoampthdeformity;
			$scope.decoration=data.DECORATIONcreoampthdeformity;
			$scope.demosets =data.DEMOSETScreoampthdeformity;
			$scope.literature =data.literaturecreoampthdeformity;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  var message3 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					for (var i = 0; i < $scope.decoration.length; i++) {
						if ($scope.decoration[i].Selected) {
							var std2 = $scope.decoration[i].name;
							message3 += "Value3: " + std2 +  "\n";
                            updateCreoJson(std2);
						}
					}
					message = message1+message2+message3;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creodlxdeformity6'){
			$scope.head = data.headcreodlxdeformity6;
			$scope.standardhead = data.standardheadcreodlxdeformity6;
			$scope.optionalhead =data.OPTIONALheadcreodlxdeformity6;
			$scope.decorationhead=data.DECORATIONheadcreodlxdeformity6;
			$scope.demosetshead =data.DEMOSETSheadcreodlxdeformity6;
			$scope.literaturehead =data.literatureheadcreodlxdeformity6;
			$scope.standard = data.standardcreodlxdeformity6;
			$scope.optional =data.OPTIONALcreodlxdeformity6;
			$scope.decoration=data.DECORATIONcreodlxdeformity6;
			$scope.demosets =data.DEMOSETScreodlxdeformity6;
			$scope.literature =data.literaturecreodlxdeformity6;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  var message3 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					for (var i = 0; i < $scope.decoration.length; i++) {
						if ($scope.decoration[i].Selected) {
							var std2 = $scope.decoration[i].name;
							message3 += "Value3: " + std2 +  "\n";
                            updateCreoJson(std2);
						}
					}
					message = message1+message2+message3;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creodlxdeformity'){
			$scope.head = data.headcreodlxdeformity;
			$scope.standardhead = data.standardheadcreodlxdeformity;
			$scope.optionalhead =data.OPTIONALheadcreodlxdeformity;
			$scope.decorationhead=data.DECORATIONheadcreodlxdeformity;
			$scope.demosetshead =data.DEMOSETSheadcreodlxdeformity;
			$scope.literaturehead =data.literatureheadcreodlxdeformity;
			$scope.standard = data.standardcreodlxdeformity;
			$scope.optional =data.OPTIONALcreodlxdeformity;
			$scope.decoration=data.DECORATIONcreodlxdeformity;
			$scope.demosets =data.DEMOSETScreodlxdeformity;
			$scope.literature =data.literaturecreodlxdeformity;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  var message3 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					for (var i = 0; i < $scope.decoration.length; i++) {
						if ($scope.decoration[i].Selected) {
							var std2 = $scope.decoration[i].name;
							message3 += "Value3: " + std2 +  "\n";
                            updateCreoJson(std2);
						}
					}
					message = message1+message2+message3;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creo5cocrdeformity'){
			$scope.head = data.headcreo5cocrdeformity;
			$scope.standardhead = data.standardheadcreo5cocrdeformity;
			$scope.optionalhead =data.OPTIONALheadcreo5cocrdeformity;
			$scope.decorationhead=data.DECORATIONheadcreo5cocrdeformity;
			$scope.demosetshead =data.DEMOSETSheadcreo5cocrdeformity;
			$scope.literaturehead =data.literatureheadcreo5cocrdeformity;
			$scope.standard = data.standardcreo5cocrdeformity;
			$scope.optional =data.OPTIONALcreo5cocrdeformity;
			$scope.decoration=data.DECORATIONcreo5cocrdeformity;
			$scope.demosets =data.DEMOSETScreo5cocrdeformity;
			$scope.literature =data.literaturecreo5cocrdeformity;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  var message3 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
                           
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					for (var i = 0; i < $scope.decoration.length; i++) {
						if ($scope.decoration[i].Selected) {
							var std2 = $scope.decoration[i].name;
							message3 += "Value3: " + std2 +  "\n";
                            updateCreoJson(std2);
						}
					}
					message = message1+message2+message3;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creoamp5cocrdeformity'){
			$scope.head = data.headcreoamp5cocrdeformity;
			$scope.standardhead = data.standardheadcreoamp5cocrdeformity;
			$scope.optionalhead =data.OPTIONALheadcreoamp5cocrdeformity;
			$scope.decorationhead=data.DECORATIONheadcreoamp5cocrdeformity;
			$scope.demosetshead =data.DEMOSETSheadcreoamp5cocrdeformity;
			$scope.literaturehead =data.literatureheadcreoamp5cocrdeformity;
			$scope.standard = data.standardcreoamp5cocrdeformity;
			$scope.optional =data.OPTIONALcreoamp5cocrdeformity;
			$scope.decoration=data.DECORATIONcreoamp5cocrdeformity;
			$scope.demosets =data.DEMOSETScreoamp5cocrdeformity;
			$scope.literature =data.literaturecreoamp5cocrdeformity;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  var message3 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
                           
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
                           
						}
					}
					for (var i = 0; i < $scope.decoration.length; i++) {
						if ($scope.decoration[i].Selected) {
							var std2 = $scope.decoration[i].name;
							message3 += "Value3: " + std2 +  "\n";
                            updateCreoJson(std2);
						}
					}
					message = message1+message2+message3;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creothcocr4degan'){
			$scope.head = data.headcreothcocr4degan;
			$scope.standardhead = data.standardheadcreothcocr4degan;			
			$scope.optionalhead =data.OPTIONALheadcreothcocr4degan;			
			$scope.literaturehead =data.literatureheadcreothcocr4degan;
			$scope.standard = data.standardcreothcocr4degan;			
			$scope.optional =data.OPTIONALcreothcocr4degan;			
			$scope.literature =data.literaturecreothcocr4degan;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
                           
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
                          
						}
					}
					message = message1+message2;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creothcocrdeformity'){
			$scope.head = data.headcreothcocrdeformity;
			$scope.standardhead = data.standardheadcreothcocrdeformity;
			$scope.optionalhead =data.OPTIONALheadcreothcocrdeformity;
			$scope.decorationhead=data.DECORATIONheadcreothcocrdeformity;
			$scope.demosetshead =data.DEMOSETSheadcreothcocrdeformity;
			$scope.literaturehead =data.literatureheadcreothcocrdeformity;
			$scope.standard = data.standardcreothcocrdeformity;
			$scope.optional =data.OPTIONALcreothcocrdeformity;
			$scope.decoration=data.DECORATIONcreothcocrdeformity;
			$scope.demosets =data.DEMOSETScreothcocrdeformity;
			$scope.literature =data.literaturecreothcocrdeformity;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  var message3 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);  
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);  
						}
					}
					for (var i = 0; i < $scope.decoration.length; i++) {
						if ($scope.decoration[i].Selected) {
							var std2 = $scope.decoration[i].name;
							message3 += "Value3: " + std2 +  "\n";
                            updateCreoJson(std2);
						}
					}
					message = message1+message2+message3;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creothcocrdeformity6'){
			$scope.head = data.headcreothcocrdeformity6;
			$scope.standardhead = data.standardheadcreothcocrdeformity6;
			$scope.optionalhead =data.OPTIONALheadcreothcocrdeformity6;
			$scope.decorationhead=data.DECORATIONheadcreothcocrdeformity6;
			$scope.demosetshead =data.DEMOSETSheadcreothcocrdeformity6;
			$scope.literaturehead =data.literatureheadcreothcocrdeformity6;
			$scope.standard = data.standardcreothcocrdeformity6;
			$scope.optional =data.OPTIONALcreothcocrdeformity6;
			$scope.decoration=data.DECORATIONcreothcocrdeformity6;
			$scope.demosets =data.DEMOSETScreothcocrdeformity6;
			$scope.literature =data.literaturecreothcocrdeformity6;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  var message3 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);  
						}
					}
					for (var i = 0; i < $scope.decoration.length; i++) {
						if ($scope.decoration[i].Selected) {
							var std2 = $scope.decoration[i].name;
							message3 += "Value3: " + std2 +  "\n";
                            updateCreoJson(std2); 
						}
					}
					message = message1+message2+message3;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creothcocrdegan'){
			$scope.head = data.headcreothcocrdegan;
			$scope.standardhead = data.standardheadcreothcocrdegan;
			$scope.optionalhead =data.OPTIONALheadcreothcocrdegan;
			$scope.demosetshead =data.DEMOSETSheadcreothcocrdegan;
			$scope.literaturehead =data.literatureheadcreothcocrdegan;
			$scope.standard = data.standardcreothcocrdegan;
			$scope.optional =data.OPTIONALcreothcocrdegan;
			$scope.demosets =data.DEMOSETScreothcocrdegan;
			$scope.literature =data.literaturecreothcocrdegan;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					message = message1+message2;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creothdeformity'){
			$scope.head = data.headcreothdeformity;
			$scope.standardhead = data.standardheadcreothdeformity;
			$scope.optionalhead =data.OPTIONALheadcreothdeformity;
			$scope.decorationhead=data.DECORATIONheadcreothdeformity;
			$scope.demosetshead =data.DEMOSETSheadcreothdeformity;
			$scope.literaturehead =data.literatureheadcreothdeformity;
			$scope.standard = data.standardcreothdeformity;
			$scope.optional =data.OPTIONALcreothdeformity;
			$scope.decoration=data.DECORATIONcreothdeformity;
			$scope.demosets =data.DEMOSETScreothdeformity;
			$scope.literature =data.literaturecreothdeformity;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  var message3 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					for (var i = 0; i < $scope.decoration.length; i++) {
						if ($scope.decoration[i].Selected) {
							var std2 = $scope.decoration[i].name;
							message3 += "Value3: " + std2 +  "\n";
                            updateCreoJson(std2);
						}
					}
					message = message1+message2+message3;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creothdeformity6'){
			$scope.head = data.headcreothdeformity6;
			$scope.standardhead = data.standardheadcreothdeformity6;
			$scope.optionalhead =data.OPTIONALheadcreothdeformity6;
			$scope.decorationhead=data.DECORATIONheadcreothdeformity6;
			$scope.demosetshead =data.DEMOSETSheadcreothdeformity6;
			$scope.literaturehead =data.literatureheadcreothdeformity6;
			$scope.standard = data.standardcreothdeformity6;
			$scope.optional =data.OPTIONALcreothdeformity6;
			$scope.decoration=data.DECORATIONcreothdeformity6;
			$scope.demosets =data.DEMOSETScreothdeformity6;
			$scope.literature =data.literaturecreothdeformity6;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  var message3 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					for (var i = 0; i < $scope.decoration.length; i++) {
						if ($scope.decoration[i].Selected) {
							var std2 = $scope.decoration[i].name;
							message3 += "Value3: " + std2 +  "\n";
                            updateCreoJson(std2);
						}
					}
					message = message1+message2+message3;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creothdegan'){
			$scope.head = data.headcreothdegan;
			$scope.standardhead = data.standardheadcreothdegan;
			$scope.requiredhead =data.REQUIREDheadcreothdegan;
			$scope.optionalhead =data.OPTIONALheadcreothdegan;
			$scope.demosetshead =data.DEMOSETSheadcreothdegan;
			$scope.literaturehead =data.literatureheadcreothdegan;
			$scope.standard = data.standardcreothdegan;
			$scope.required =data.REQUIREDcreothdegan;
			$scope.valuesone =data.valuesonecreothdegan;
			$scope.valuestwo =data.valuestwocreothdegan;
			$scope.optional =data.OPTIONALcreothdegan;
			$scope.demosets =data.DEMOSETScreothdegan;
			$scope.literature =data.literaturecreothdegan;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					message = message1+message2;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creodlxdegan'){
			$scope.head = data.headcreodlxdegan;
			$scope.standardhead = data.standardheadcreodlxdegan;
			$scope.optionalhead =data.OPTIONALheadcreodlxdegan;
			$scope.demosetshead =data.DEMOSETSheadcreodlxdegan;
			$scope.literaturehead =data.literatureheadcreodlxdegan;
			$scope.standard = data.standardcreodlxdegan;
			$scope.optional =data.OPTIONALcreodlxdegan;
			$scope.demosets =data.DEMOSETScreodlxdegan;
			$scope.literature =data.literaturecreodlxdegan;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					message = message1+message2;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creo5cocrdegan'){
			$scope.head = data.headcreo5cocrdegan;
			$scope.standardhead = data.standardheadcreo5cocrdegan;
			$scope.optionalhead =data.OPTIONALheadcreo5cocrdegan;
			$scope.demosetshead =data.DEMOSETSheadcreo5cocrdegan;
			$scope.literaturehead =data.literatureheadcreo5cocrdegan;
			$scope.standard = data.standardcreo5cocrdegan;
			$scope.optional =data.OPTIONALcreo5cocrdegan;
			$scope.demosets =data.DEMOSETScreo5cocrdegan;
			$scope.literature =data.literaturecreo5cocrdegan;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					message = message1+message2;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creoamp5cocrdegan'){
			$scope.head = data.headcreoamp5cocrdegan;
			$scope.standardhead = data.standardheadcreoamp5cocrdegan;
			$scope.optionalhead =data.OPTIONALheadcreoamp5cocrdegan;
			$scope.demosetshead =data.DEMOSETSheadcreoamp5cocrdegan;
			$scope.literaturehead =data.literatureheadcreoamp5cocrdegan;
			$scope.standard = data.standardcreoamp5cocrdegan;
			$scope.optional =data.OPTIONALcreoamp5cocrdegan;
			$scope.demosets =data.DEMOSETScreoamp5cocrdegan;
			$scope.literature =data.literaturecreoamp5cocrdegan;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					message = message1+message2;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creodlxdegan6'){
			$scope.head = data.headcreodlxdegan6;
			$scope.standardhead = data.standardheadcreodlxdegan6;
			$scope.optionalhead =data.OPTIONALheadcreodlxdegan6;
			$scope.demosetshead =data.DEMOSETSheadcreodlxdegan6;
			$scope.literaturehead =data.literatureheadcreodlxdegan6;					
			$scope.standard = data.standardcreodlxdegan6;
			$scope.optional =data.OPTIONALcreodlxdegan6;
			$scope.demosets =data.DEMOSETScreodlxdegan6;
			$scope.literature =data.literaturecreodlxdegan6;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					message = message1+message2;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creothcocr4deformity'){
			$scope.head = data.headcreothcocr4deformity;
			$scope.standardhead = data.standardheadcreothcocr4deformity;
			$scope.optionalhead =data.OPTIONALheadcreothcocr4deformity;
			$scope.decorationhead=data.DECORATIONheadcreothcocr4deformity;
			$scope.literaturehead =data.literatureheadcreothcocr4deformity;
			$scope.standard = data.standardcreothcocr4deformity;
			$scope.optional =data.OPTIONALcreothcocr4deformity;
			$scope.decoration=data.DECORATIONcreothcocr4deformity;
			$scope.literature =data.literaturecreothcocr4deformity;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  var message3 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					for (var i = 0; i < $scope.decoration.length; i++) {
						if ($scope.decoration[i].Selected) {
							var std2 = $scope.decoration[i].name;
							message3 += "Value3: " + std2 +  "\n";
                            updateCreoJson(std2);
						}
					}
					message = message1+message2+message3;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
					}
			}
			}
			else if(linkId=='creomis'){
			$scope.head = data.headcreomis;
			$scope.standardhead = data.standardheadcreomis;
			$scope.requiredhead =data.REQUIREDheadcreomis;
			$scope.optionalhead =data.OPTIONALheadcreomis;
			$scope.demosetshead =data.DEMOSETSheadcreomis;
			$scope.literaturehead =data.literatureheadcreomis;
			$scope.standard = data.standardcreomis;
			$scope.required =data.REQUIREDcreomis;
			$scope.valuesone =data.valuesonecreomis;
			$scope.valuestwo =data.valuestwocreomis;
			$scope.optional =data.OPTIONALcreomis;
			$scope.demosets =data.DEMOSETScreomis;
			$scope.literature =data.literaturecreomis;
			
			$scope.GetValue = function () {
			  var message = "";
			  var message1 = "";
			  var message2 = "";
			  
			        for (var i = 0; i < $scope.standard.length; i++) {
						if ($scope.standard[i].Selected) {
							var std = $scope.standard[i].name;
							message1 += "Value1: " + std +  "\n";
                            updateCreoJson(std);
						}
					}
					for (var i = 0; i < $scope.optional.length; i++) {
						if ($scope.optional[i].Selected) {
							var std1 = $scope.optional[i].name;
							message2 += "Value2: " + std1 +  "\n";
                            updateCreoJson(std1);
						}
					}
					message = message1+message2;
					if(message == '' ){
						showErrorAlert();
					}
					else{
					    showSuccessAlert();
                        setJsonToLocalStorage();
                        
					}
			}
			}
			else{
			alert('Json file not found');	
			}
		    });
                
            /*updateCreoJson(): Common function used to push the selected set to the JSON
             Author: Karthik
             */    
            function updateCreoJson(std) {
                 if (localStorage.getItem("creoData") != "" ){
                        localcreo = window.localStorage.getItem("creoData");
                    }
                    var count=0;
                    if( localcreo !="" ){
                          var creoset=JSON.parse(localcreo);  
                           angular.forEach(creoset, function(value, key) {
                               if(value.setid==std) { 
                                    count= ++count;
                                }
                            });  
                         if(count==0){
                                mainCreoJsonData.push({'setid' : std}); 
                            }
                    }
                    else{  
                        mainCreoJsonData.push({'setid' : std});
                    }
                return null;
                }
                
            /* setJsonToLocalStorage(): Common function used to set the JSON to the Local storage
               Author: Karthik
            */    
            function setJsonToLocalStorage() {
                 window.localStorage.setItem("creoData", JSON.stringify(mainCreoJsonData));
            }
                
            /* showErrorAlert(): Common function used to show the validation
               Author: Karthik
            */    
            function showErrorAlert() {
            		bootbox.alert("<div class='submitMessage'>Please select the Set.</div>");
			}
                
            /* showSuccessAlert(): Common function used to show the success message
               Author: Karthik
            */ 
   			function showSuccessAlert() {
   					bootbox.alert("<div class='submitMessage'>Selected Sets are added to the Product Loaner Screen</div>"); 
			} 
 
			});
			
		
	
	