/**********************************************************************************
 * File:        GmLoginView.js
 * Description: Contains the Login View.
 *              This view is initiated when the route is "", after the Main View is
 *              loaded.
 * Version:     1.0.0
 * Author:      tvenkateswarlu
 **********************************************************************************/

define([
    'jquery', 'underscore', 'backbone', 'handlebars', 'i18n!nls/GmLocale',
    'authentication', 'device', 'global', 'notification', 'daomarketingcollateral',
    'fieldview', 'usermodel', 'datasync',

    // Pre-Compiled Template
    'gmtTemplate'
], 

function ($, _, Backbone, Handlebars, Locale, Authentication, Device, Global, Notification, DAOMarketingCollateral, FieldView, UserModel, DataSync, GMTTemplate) {

    'use strict';
    
    var LoginView = Backbone.View.extend({

        el: "#div-main",

        events: {
            "blur #txt-userid, #txt-password": "validate",
            "click #btn-login, #div-login-background": "authenticate"
        },
    
        template: fnGetTemplate(URL_Login_Template, "GmLogin"),
        template_loginfailed: fnGetTemplate(URL_Login_Template, 'GmLoginFailed'),

        initialize: function() {
            // showMsgView("001");
            $("#div-messagebar").hide();
            $(".div-btn-header-1").hide();
            $(".div-btn-header").hide();
            $(".div-btn-locale").show();
            $("#div-app-title").removeClass("gmVisibilityVisible").addClass("gmVisibilityHidden");
//            $("#div-app-title").removeClass("gmMediaVisibilityV").addClass("gmMediaVisibilityH");
            $(".div-moble-menu").removeClass("gmMediaVisibilityV").addClass("gmMediaVisibilityH");
            $("#btn-updates").hide();
			$("#div-header").addClass("iphone-header-small").removeClass("iphone-header-big");
            $("body").removeClass("background-2").addClass("background-1 background-image");
            localStorage.removeItem('token');
            localStorage.removeItem('shname');
	    $("#btn-sidebar-menu").hide();
            //this.render();
        },

        //render the initial content after mainview loaded 
        render: function () {
            this.$el.html(this.template({label: Locale}));
            $("#div-popup").html(this.template_loginfailed());
//             $("#div-footer").addClass("hide");
            if(!$("#div-footer").hasClass("hide")){
           $("#div-footer").addClass("hide") 
        }
            $("#ipad-menu-bar").addClass("hide");
            $("#li-update-count").addClass("hide");
            $("#li-pending-sync").addClass("hide");
            $("#li-LocalDB").addClass("hide");
            $(".ul-language-name").removeClass("hide");
            $(".li-ipad-user-pic").addClass("hide");
            $("#mobile-menu-top-user-name").addClass("hide");
//            $(".li-ipad-user-pic").addClass("hide");
            if(strEnvironment == "Production"){
                this.$("#div-env").hide();
            }
            else {
                this.$("#spn-version").html(version);
                this.$("#span-envvalue").html(strEnvironment);
            }
            return this;
        },

        //authenticate the username and password
        authenticate: function() {
            var strUserID = this.$("#txt-userid").val().toLowerCase();
            var strPasswd = this.$("#txt-password").val();
			var that = this;
			console.log("data for login strUserID"+strUserID);
			
            if(this.$("#btn-login").text().trim().length==5) {
                if((strUserID.length==0) || (strPasswd.length==0)) {
                    var userModel = new UserModel();
                    this.$("input").each(function() {
                        var view = new FieldView({el: this, model: userModel});
                        view.validate();
                    });
                }
                else { 
                    this.$("#btn-login").html("Logging in...");
                    if(navigator.onLine) {
                        fnAuthenticate(strUserID, strPasswd, function(data) {
                            console.log("data for login "+data);
                            that.authorize(data);
                        });
                    }
                    else
                        that.initializeOfflineMode(strUserID);
             }
        }
        },

        initializeOfflineMode: function (strUserID) {

            var that = this;
            var userNm = window.localStorage.getItem('userName');
            if((userNm!=null) && (userNm == strUserID)) {
                intOfflineMode = 1;
                showNativeAlert("You are logged in offline mode due to unavailability of internet connection.\nYou may need to login again when the device gets connected to internet.","Offline","OK",function  () {
                   that.close();
                   window.location.href = "#home";
                })
            }
            else {
                that.$("#txt-userid").val('');
                that.$("#txt-password").val('');
                that.$("#btn-login").html("Login");
                showNativeAlert("You are Offline.\nTo login in offline mode, you need to enter the same userid and password used previously in this application","Invalid Offline Credentials","OK");
            }
                

        },
        
        //authorize the user 
        authorize: function(data) {
            if(data=="Internal Server Error") {
                // showMsgView("010");
                var that = this;
                this.$("#btn-login").html("Login");
                showNativeAlert("Invalid Username / Password", "Invalid Credentials", 'OK', function (argument) {
                    that.$("#txt-userid").val('');
                    that.$("#txt-password").val('');
                });
            }
            else if(typeof(data)=="object") {
                var device = GM.Global.Device;
                var v700 = localStorage.getItem('V700SYNC');
             //   var locale = localStorage.getItem('locale');
                GM.Global = {}; 
                GM.Global.Device = device;
                localStorage.clear();
                //Set User Data objects in local storage
                GM.Global.UserData = data;
                localStorage.setItem("userData", JSON.stringify(data));
               //Setting all the user's company info into local storage so that we send along with all web service calls
                localStorage.setItem('moduleaccess',data.moduleaccess); 
                localStorage.setItem('cmpid',data.cmpid);
                localStorage.setItem('plantid',data.plantid);
                localStorage.setItem('cmptzone',data.cmptzone);
                localStorage.setItem('cmpdfmt',data.cmpdfmt);
               localStorage.setItem('cmpcurrfmt',data.cmpcurrfmt);
                localStorage.setItem('currtype',data.currtype);
                localStorage.setItem('cmpcurrsmb',data.cmpcurrsmb);
             //Assign currency symbol based on user's company detail
                    if (data.cmpcurrsmb == "JPY") {
                           localStorage.setItem('cmpcurrsmb', String.fromCharCode(165));
                    } else {
                        localStorage.setItem('cmpcurrsmb', data.cmpcurrsmb);
                    }
                //PMT-12763 Getting the PDF failure Try Count while Logging IN
                localStorage.setItem('pdffailurecnt',data.pdffailurecnt);
                localStorage.setItem('compcd',data.compcd);
                localStorage.setItem('defcurrncysymbol',data.defcurrncysymbol);
                // Egnyte Token Store in LocalStorage
                localStorage.setItem('egnytetkn',data.egnytetkn);
                localStorage.setItem('egnytefl',data.egnytefl);
                localStorage.setItem('rimbguide',data.rimbguideid);
                localStorage.setItem("repDivision", data.repdiv); // rep division id
                localStorage.setItem("autoUpdateTimer", data.autoupdtimer); // rep division id
                
                localStorage.setItem("locale",data.compcd);
                localStorage.setItem("deviceLng",data.compcd);
                localStorage.setItem('V700SYNC',v700);
                window.localStorage.setItem("token", data.token);
                window.localStorage.setItem("strShortName", data.shname);
                window.localStorage.setItem("userName", data.usernm); 
                window.localStorage.setItem("userID", data.userid);
                window.localStorage.setItem("userCompany", data.companyid);
                window.localStorage.setItem("userDivision", data.divid);
                window.localStorage.setItem("partyID", data.partyid);
                window.localStorage.setItem("AcID", data.acid);
                window.localStorage.setItem("Dept", data.deptseq);
                window.localStorage.setItem("fName", data.fname);
                window.localStorage.setItem("lName", data.lname);
                //getting the company address for DO
                window.localStorage.setItem("strremitadress", data.strremitadress);
                window.localStorage.setItem("strcmpadress", data.strcmpadress);
                window.localStorage.setItem("strcompfax", data.strcompfax);
                window.localStorage.setItem("strcompname", data.strcompname);
                window.localStorage.setItem("strcompphn", data.strcompphn);
                window.localStorage.setItem("pdffilepath", data.pdffilepath);
                window.localStorage.setItem("emailid", data.emailid);
                
                // CRM local storage variables
                window.localStorage.setItem("acid", data.acid);
                window.localStorage.setItem("deptid", data.deptid);
                window.localStorage.setItem("creoData", "");
                var token = localStorage.getItem("token");
                GM.Global.SalesRep = isRep();
                GM.Global.UserName = window.localStorage.getItem("fName") + " " + window.localStorage.getItem("lName");
                GM.Global.UserID = window.localStorage.getItem("userID");
                intLogin = 1;
                intOfflineMode = 0;
//                chkUpdate();
//                fnTimerSplashScreen();
//                fnCheckShareHistory();    Due to PMT-23618 code has been commented
                fnCheckDOPDFPendingSync();
                getLocaleData();
//                fnGetRFSCompanymappingID(localStorage.getItem('cmpid'),function(data){
//                    localStorage.setItem("rfsmappingid",data[0].RFSID)
//                });
                if(parseInt(data.acid)>2)
                	fnFetchFieldSalesList();
                this.close();
                fnDelSetAccessData();
                // The following code is added for hiding the Pring Module Icon for the Users who are not mapped to PRICING_APP_ACCESS Security Group
                var input = {"token":localStorage.getItem("token"),"userid":localStorage.getItem("userID"),"deptid":localStorage.getItem("Dept"),"partyid":localStorage.getItem("partyID")};
                var mobModule = '';
                    fnGetWebServerData("auth/AppModuleAccess/", "gmLogonAccess", input, function (data) {
//                        console.log(data);
                        if (data != null) {
                            _.each(data, function (data) {
                                window.localStorage.setItem(data.FUNCID, data.UPDATEACC);
                                window.localStorage.setItem(data.FUNCNM, data.UPDATEACC);
                                mobModule =  data.FUNCNM.substring(0, data.FUNCNM.indexOf("-mob"));
                                if ($(window).width() < 480) {
                                    window.localStorage.removeItem(mobModule);
                                }
                            });
                           
                            window.location.href = "#home";					
                    }
                });
				this.securityEvents(data.listGmSecurityEventVO);
				this.getAccountIds();//Added for Office Administrator fetch accounts based on access level.
                syncServiceComponent(); //function used to load firebase credentials   
                loadReadImageAPIEndpoint(); //used to take Amazon computer vision end point from rule table
                loadFaxAuthDetails(); //used to get the Fax authorization code
            }
            else {
                // fnShowStatus("Server Unreachable! Please Check after sometime");
                showMsgView("025");
                this.$("#btn-login").html("Login");
            }
        },

        //validate the username and password
        validate: function() {
            var user = new UserModel();  
            $("input").each(function() {
                new FieldView({el: this, model: user});
            });
        },

        close: function() {
            this.unbind();
        },
        getAccountIds: function () {
            console.log("getAccountIds");
            var input = {
                "token": localStorage.getItem("token"),
                "userid": localStorage.getItem("userID"),
                "acslvl": localStorage.getItem("AcID")
            };
            console.log(input);
            fnGetWebServerData("auth/AppViewAccess", "gmLogonAccess", input, function (data) {
                console.log(data);
                
                if (data != null) {
                        _.each(data.alAccess, function (data) {
                            window.localStorage.setItem(data.FUNCID, data.FUNCNM);
                        });
                    _.each(data.alAccountId, function (dt) {
                        fnSetAccessData(dt.AC_ID);
                    });
					//Call the below function to sync the Tag data in App Startup
                     fnGetLocalTagDataCount(function (datalen){
						if(datalen==0){
						   fnsyncTagDataInAppStartup();
						}
					});
                }
            });
		fnGetRuleValues('MODE_EXCLUSION', 'MODEEXCLUDE', localStorage.getItem("cmpid"), function (data) {
            if(data !=null ){
                if(data.rulevalue != ''){
                    localStorage.setItem("ModeExclusion",data.rulevalue);  
                }
            }
        });  
        },
        securityEvents: function(listGmSecurityEvent){
			fnDeleteSecurityEventsSyncPage();
            if(listGmSecurityEvent != null && listGmSecurityEvent != undefined){
              var isArray = Array.isArray(listGmSecurityEvent);
              if(isArray == true){
                 var jsnListGmSecurityEvent = $.parseJSON(JSON.stringify(listGmSecurityEvent));
                 _.each(jsnListGmSecurityEvent, function (gmSecurityEvent) {                                                                                                     
					 fnInsertSecurityEvents(gmSecurityEvent.eventname, gmSecurityEvent.tagname,      
                                            gmSecurityEvent.pagename, gmSecurityEvent.updateaccessflg, gmSecurityEvent.readaccessflg, gmSecurityEvent.voidaccessflg, 
						                    gmSecurityEvent.accessvoidflg,gmSecurityEvent.groupmappingvoidflg,gmSecurityEvent.rulesvoidflg);
                   });
              }else{
                     fnInsertSecurityEvents(listGmSecurityEvent.eventname, listGmSecurityEvent.tagname,      
                                            listGmSecurityEvent.pagename, listGmSecurityEvent.updateaccessflg, listGmSecurityEvent.readaccessflg, listGmSecurityEvent.voidaccessflg, 
						                    listGmSecurityEvent.accessvoidflg,listGmSecurityEvent.groupmappingvoidflg,listGmSecurityEvent.rulesvoidflg);
            
                   }

            }
        }

      });

  return LoginView;
});