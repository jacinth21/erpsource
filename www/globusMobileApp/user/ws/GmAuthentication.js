function fnAuthenticate(strUserID, strPasswd, callback) {
    var user = { "usernm": strUserID, "passwd": strPasswd};
    devModel = deviceModel.toUpperCase();
    var deviceName = "Simulator";
    var deviceType = "26240659";
    
   
    switch(true) {
        case (devModel.indexOf("IPHONE") != -1):
            deviceType = "103106";
            deviceName = "iPhone";
            break;        
    
        case (devModel.indexOf("IPOD") != -1):
            deviceType = "103106";
            deviceName = "iPod Touch";
            break;        
    
        case (devModel.indexOf("IPAD") != -1):
            deviceType = "103105";
            deviceName = "iPad";
            break; 
            
        case (devModel.indexOf("ANDROID") != -1):
            deviceType = "26240658";
            deviceName = "Android";
            break;        
    }

    console.log("This application is running on " + deviceName);
    console.log("DeviceType : " + deviceType);
    $.ajax({
        url: URL_WSServer + "auth",
        method: "POST",
        contentType: "application/json",
        data: JSON.stringify(user),
        datatype: "json",
        success: function (data) {
            console.log(data);
            $.ajax({
                url: URL_WSServer + "auth/deviceinfo",
                method: "POST",
                contentType: "application/json",
                data: JSON.stringify({ "token" : data.token, "devicetype": deviceType, "uuid": deviceUUID, "devicetoken": deviceToken }),
                datatype: "json",
                success: function (datas) {
                    callback(data);
                },
                 error: function (jqXHR,status,error) {
                    console.log(error);
                    callback(error);
                }
            });
        },
         error: function (jqXHR,status,error) {
            console.log(error);
            callback(error);
        }
    });
}
