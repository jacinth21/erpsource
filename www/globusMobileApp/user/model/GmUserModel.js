/**********************************************************************************
 * File:        GmUserModel.js
 * Description: User Model
 * Version:     1.0
 * Author:      tvenkateswarlu
 **********************************************************************************/
define([
    'underscore', 'backbone',
], 

function (_, Backbone) {
    
    'use strict';
    var User = Backbone.Model.extend({

        //Validate the username and password
        validate: function(attrs) {

            var errors = this.errors = {};
            if (!attrs.userid) errors.userid = '* Username is required';
            if (!attrs.password) errors.password = '* Password is required';
            if (!_.isEmpty(errors)) return errors;
          }
      });

  return User;
});