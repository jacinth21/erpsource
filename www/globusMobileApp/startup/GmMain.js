/**********************************************************************************
 * File:        GmMain.js
 * Description: Startup Logic. This file loads first when the app loads.
 *              1. This file has the code to load the necessary support-files for
 *                 the app to run.
 *              2. All CSS are loaded in here
 *              3. Device readiness is checked in this code
 *              4. Dependencies are set before the support-files are loaded
 * Version:     1.0
 * Author:      ejayabalan
 **********************************************************************************/

(function() {

	window.GM = {
		Model: {},
		Collection: {},
		View: {},
		Router: {},
		Template: {},
		Global: {}
	};

'use strict';

//Require.js allows us to configure shortcut alias
require.config({

	// The shim config allows us to configure dependencies for
	// scripts that do not call define() to register a module
	shim: {
		underscore: {
			exports: '_'
		},

		backbone: {
			deps: ['underscore', 'jquery'],
			exports: 'Backbone'
		},

		daosearch: {
			exports: 'DAOSearch'
		},
		
		daosetup: {
			exports: 'DAOSetup'
		},

		daodo: {
			exports: 'DAODO'
		},
		daoinv: {
			exports: 'DAOInv'
		},
		daoproductcatalog: {
			exports: 'DAOProductCatalog'
		},
		
		daomarketingcollateral: {
			exports: 'DAOMarketingCollateral'
		},
		
		database: {
			deps: ['jquery'],
			exports: 'DB'
		},

		fastclick: {
			deps: ['jquery'],
			exports: 'Fastclick'
		},

		

		jqueryui: {
			deps: ['jquery'],
			exports: 'jqueryui'
		},

		jqueryuitouch: {
			deps: ['jquery', 'jqueryui'],
			exports: 'jqueryuitouch'
		},

		timezoneJS: {
			deps: ['jquery'],
			exports: 'timezoneJS'
		},

		jqueryslider: {
			deps: ['jquery'],
			exports: 'jqueryslider'
		},
        
        server: {
            deps: ['global'],
            exports: 'server'
        },

		mapui: {
			deps: ['jquery'],
			exports: 'mapui'
		},

		mapclusterer: {
			deps: ['jquery', 'mapui'],
			exports: 'mapclusterer'
		},

		global: {
			deps: ['handlebars','handlebars.runtime'],
			exports: 'global'
		},
        pricingCommon: {
                    deps: ['handlebars','handlebars.runtime'],
                    exports: 'pricingApproval'
                },

		gmtTemplate: {
			deps: ['handlebars.runtime'],
			exports: 'gmtTemplate'
		},
        date: {
			deps: ['jquery'],
			exports: 'Date'
		},
        moment: {
			deps: ['jquery'],
			exports: 'Moment'
		},
        evendar: {
			deps: ['jquery'],
			exports: 'Evendar'
		},
		commonutil: {
			deps: ['handlebars','handlebars.runtime','global'],
			exports: 'commonutil'
		}
	},

	/* Once these paths are defined, the pathnames are the ones referred in the rest of the application code */
	paths: {
		
		/* External Libraries */
		timezoneJS: '../lib/timezone-js-master/src/date',
		backbone: '../lib/backbone/backbone-min',
		fastclick: '../lib/fastclick/fastclick',
		handlebars: '../lib/handlebars/handlebars-v4.0.5',
		'handlebars.runtime': '../lib/handlebars/handlebars.runtime-v4.0.5',
		canvas: '../lib/threesixty/heartcode-canvasloader-min',
		jquery: '../lib/jquery/jquery-1.11.0.min',
		jqueryui: '../lib/jquery/jquery-ui.min',
		jqueryuitouch: '../lib/jquery/jquery.ui.touch-punch.min',
		underscore: '../lib/underscore/underscore-min',
		jqueryscroll: '../lib/jquery/jquery.scrollTo.min',
		jqueryvisible: '../lib/jquery/jquery.visible.min',
		jqueryzoom: '../lib/jquery/jquery.panzoom.min',
		threesixty: '../lib/threesixty/threesixty',
		jqueryslider: '../lib/jquery/jquery.slides.min',
		jcrop: '../lib/jcrop/jcrop',
        mapclusterer: '../lib/map/markerclusterer',
		mapui: '../lib/map/mapui',
        date:'../lib/date/date',
        moment: '../lib/evendar/moment.min',
        evendar: '../lib/evendar/evendar.min',
		
		/* Common */
		global: '../common/GmGlobal',
		commonutil: '../common/GmCommonUtil',
		server: '../common/GmServer',
		notification: '../common/GmNotification',

		/* Database */
		daocase:  '../db/GmDAOCase',
		daosearch:  '../db/GmDAOSearch',
		daosetup:  '../db/GmDAOSetUp',
		daoproductcatalog:  '../db/GmDAOProductCatalog',
		daomarketingcollateral:  '../db/GmDAOMarketingCollateral',
		database: '../db/GmDatabase',
		daodo: '../db/GmDAODO',
		daoinv:'../db/GmDAOInv',
        
        //crm local db calls
        crmdb:'../db/GmCRMDB',
		
		/* Synchronization */	
		datasync: '../sync/GmDataSync',
		filesync: '../sync/GmFileSync',
		authentication: '../user/ws/GmAuthentication',

		/* Device */
		device: '../device/GmDevice',

		/* Routers */
		
		mainrouter: '../router/GmMainRouter',
		caserouter: '../sales/case/router/GmCaseRouter',
		catalogrouter: '../sales/catalog/router/GmCatalogRouter',
		collateralrouter: '../sales/collateral/router/GmCollateralRouter',
		dashboardrouter: '../sales/dashboard/router/GmDashboardRouter',
		dorouter: '../sales/DO/router/GmDORouter',
		invrouter: '../sales/inv/router/GmInvRouter',
        pricerouter: '../pricing/router/gmrPrice',
        gmcreorouter: '../creoclient/router/gmCreo',

		
		/* ----------- Application Specific ---------- */

		/* Model */
		addressmodel: '../common/model/GmAddressModel',
		casemodel: '../common/model/GmCaseModel',
		itemmodel: '../common/model/GmItemModel',
		usermodel: '../user/model/GmUserModel',
		contactsmodel: '../sales/collateral/model/GmValidateContactsModel',
		msgmodel: '../message/model/GmMsgModel',
		dropdownmodel: '../common/model/GmDropDownModel',
		saleitemmodel: '../common/model/GmSaleItemModel',
		datemodel: '../common/model/GmDateModel',
		searchmodel: '../common/model/GmSearchModel',

		/* View */
		addressview: '../common/view/GmAddressView',
		addresslistview: '../common/view/GmAddressListView',
		shipaddressview: '../common/view/GmShipAddressView',
		invshipaddressview: '../common/view/GmInvShipAddressView',
		loaderview: '../common/view/GmLoaderView',
		catalogview: '../sales/catalog/view/GmCatalogView',
		collateralview: '../sales/collateral/view/GmCollateralView',
		fieldview: '../common/view/GmFieldView',
		homeview: '../home/view/GmHomeView',
		mpcimageview: '../sales/catalog/view/GmCatalogImageView',
		mmcimageview: '../sales/collateral/view/GmCollateralImageView',
		imageviewerview: '../common/view/GmImageViewerView',
		itemdetailview: '../sales/catalog/view/GmItemDetailView',
		collateraldetailview: '../sales/collateral/view/GmCollateralDetailView',
		collateralemailtrackview: '../sales/collateral/view/GmvCollateralEmailTrackView',
		itemlistview: '../common/view/GmItemListView',
		itemview: '../common/view/GmItemView',
		loginview: '../user/view/GmLoginView',
		mainview: '../common/view/GmMainView',
		settingsview: '../settings/view/GmSettingsView',
		mpcsidepanelview: '../sales/catalog/view/GmCatalogSidePanelView',
		mmcsidepanelview: '../sales/collateral/view/GmCollateralSidePanelView',
		shareview: '../sales/collateral/view/GmShareView',
		threesixtyview: '../sales/collateral/view/GmThreeSixtyView',
		contactsview: '../sales/collateral/view/GmContactsView',
		sharestatusview: '../sales/collateral/view/GmShareStatusView',
		documentview: '../common/view/GmDocumentView',
		dropdownview: '../common/view/GmDropDownView',
		searchview: '../common/view/GmSearchView',
		casemainview: '../sales/case/view/GmCaseMainView',
		casedetailview: '../sales/case/view/GmCaseDetailView',
		casereportview: '../sales/case/view/GmCaseReportView',
		casescheduleview: '../sales/case/view/GmCaseScheduleView',
		numberkeypadview: '../common/view/GmNumberKeypadView',
		imagecropview: '../common/view/GmImageCropView',
        gmtSideMenu: '../common/template/GmSideMenu',
        dorecordtagssearchview: '../common/view/GmRecordTagsSearchView',

		dashmainview: '../sales/dashboard/view/GmDashMainView',
		dashoverviewview: '../sales/dashboard/view/GmDashOverviewView',
		dashmorefilterview: '../sales/dashboard/view/GmDashMoreFilterView',
		saleitemlistview: '../common/view/GmSaleItemListView',
		saletodayview: '../sales/dashboard/view/GmSaleTodayView',
		salemtdview: '../sales/dashboard/view/GmSaleMTDView',
		dashdrilldownview: '../sales/dashboard/view/GmDashDrillDownView',
		mtddrilldownview: '../sales/dashboard/view/GmMTDDrillDownView',
		daterangeview: '../common/view/GmDateRangeView',
		domainview: '../sales/DO/view/GmDOMainView',
		dodetailview: '../sales/DO/view/GmDODetailView',
		dosurgerydetailview: '../sales/DO/view/GmDOSurgeryDetailView',
        donpidetailview: '../sales/DO/view/GmDONPIDetailView',
        donpiprevsurgeoninfo: '../sales/DO/view/GmDONPIPrevSurgeonInfo',
        dorecordtagsview: '../sales/DO/view/GmDORecordTagsView',
        
		dobillinginfoview:'../sales/DO/view/GmDOBillingInfoView',
		doreportview:'../sales/DO/view/GmDOReportView',
		dopartsusedview:'../sales/DO/view/GmDOPartsUsedView',
		dosignatureview:'../sales/DO/view/GmDOSignatureView',
		dobookedview:'../sales/DO/view/GmDOBookedView',
		doshiptoinfo:'../sales/DO/view/GmDOShipToInfoView',
		signatureview:'../common/view/GmSignatureView',
		dotagidview: '../sales/DO/view/GmDOTagIDView',
		docommentsview: '../sales/DO/view/GmDOCommentsView',
		doudiview: '../sales/DO/view/GmDOUDIView',

		// CSM Files
		csmmainview: '../csm/view/GmCsmMainView',
		
		invmainview: '../sales/inv/view/GmInvMainView',
		invdetailview: '../sales/inv/view/GmInvDetailView',
		invsentrequestinfoview: '../sales/inv/view/GmInvReqInfoView',
		inviteminitiateview: '../sales/inv/view/GmInvItemInitiateView',
		invloanerextensionview: '../sales/inv/view/GmInvLoanerExtensionView',
		invreportview: '../sales/inv/view/GmInvReportView',
        invSetReqBorrowRpt: '../sales/inv/view/GmInvSetReqBorrowRptView',
		invlotonhandview: '../sales/inv/view/GmInvLotOnHandView',
		invsetinitiateview: '../sales/inv/view/GmInvSetInitiateView',
		invsetsusedview: '../sales/inv/view/GmInvSetsUsedView',
		invshiptoinfoview: '../sales/inv/view/GmInvShipToInfoView',	
		invsurgeryinfoview: '../sales/inv/view/GmInvSurgeryInfoView',
		invsummaryview: '../sales/inv/view/GmInvSummaryView',
		invpartsusedview: '../sales/inv/view/GmInvPartsUsedView',
		invcomreportview:'../sales/inv/view/GmInvComReportView',
		invsetreportview:'../sales/inv/view/GmInvSetReportView',
		invfieldreportview:'../sales/inv/view/GmInvFieldReportView',
		invfieldreportsubview: '../sales/inv/view/GmFieldReportSubView',
		invmorefilterview: '../sales/inv/view/GmInvMoreFilterView',
		invcommentsview: '../sales/inv/view/GmInvCommentsView',
		setlistview: '../common/view/GmSetListView',
        pricemainview: '../pricing/view/gmvPriceMain',
		priceStatusReport: '../pricing/view/gmvPriceStatusReport',
		pricingApproval: '../pricing/view/gmvPricingApproval',
        pricingCommon: '../pricing/view/gmvPricingCommon',
		priceRequest:'../pricing/view/gmvPriceRequest',
		
		/* Collection */
		addresscollection: '../common/collection/GmAddressCol',
		itemcollection: '../common/collection/GmItemCol',
		dropdowncollection: '../common/collection/GmDropDownCol',
		saleitemcollection: '../common/collection/GmSaleItemCol',
		
		
		/* Locale */
		i18n: '../startup/i18n',


		// CRM Files
		/* Routers */
		// gmrMain: '../router/gmrMain',
		gmrActivity: '../crm/activity/router/gmrActivity',
		gmrPhysicianVisit: '../crm/physicianvisit/router/gmrPhysicianVisit',
		gmrSurgeon: '../crm/surgeon/router/gmrSurgeon',
        gmrLead: '../crm/lead/router/gmrLead',
        gmrMERC: '../crm/merc/router/gmrMERC',
        gmrSalesCall: '../crm/salescall/router/gmrSalesCall',
        gmrTraining: '../crm/training/router/gmrTraining',
        gmrCRF: '../crm/crf/router/gmrCRF',
        gmrFieldSales: '../crm/fieldsales/router/gmrFieldSales',
        gmrPS: '../crm/preceptorship/router/gmrPS',
		gmrEval: '../quality/eventevalform/router/gmrEval',
        gmrMerc: '../crm/merc/router/gmrMerc',
				
		/* ----------- Application Specific ---------- */

		/* View */
		gmvApp: '../common/view/gmvApp',
		gmvCRMDashboard: '../crm/dashboard/view/gmvCRMDashboard',
		gmvCRMData: '../common/view/gmvCRMData',
		gmvCRMMain: '../common/view/gmvCRMMain',
		gmvCRMTab: '../common/view/gmvCRMTab',
		gmvField: '../common/view/gmvField',
		gmvItem: '../common/view/gmvItem',
		gmvLogin: '../common/view/gmvLogin',
		gmvMain: '../common/view/gmvMain',
		gmvCRMSelectPopup: '../common/view/gmvCRMSelectPopup',
        gmvSearch: '../common/view/gmvSearch',
		gmvActivityCriteria: '../crm/activity/view/gmvActivityCriteria',
        gmvActivityDetail: '../crm/activity/view/gmvActivityDetail',
        gmvActivityFilter: '../crm/activity/view/gmvActivityFilter',
        gmvCRMMap: '../common/view/gmvCRMMap',
        gmvMoreFilter: '../common/view/gmvMoreFilter',
        
		gmvPhysicianVisitCriteria: '../crm/physicianvisit/view/gmvPhysicianVisitCriteria',
		gmvPhysicianVisitDetail: '../crm/physicianvisit/view/gmvPhysicianVisitDetail',
		gmvPhysicianVisitFilter: '../crm/physicianvisit/view/gmvPhysicianVisitFilter',
		gmvLeadCriteria: '../crm/lead/view/gmvLeadCriteria',
		gmvLeadDetail: '../crm/lead/view/gmvLeadDetail',
        gmvLeadFilter: '../crm/lead/view/gmvLeadFilter',
        gmvTradeShowDetail: '../crm/lead/view/gmvTradeShowDetail',
        gmvMerc: '../crm/merc/view/gmvMerc',
        gmvMercCriteria: '../crm/merc/view/gmvMercCriteria',
		gmvMERCDetail: '../crm/merc/view/gmvMERCDetail',
        gmvMERCFilter: '../crm/merc/view/gmvMERCFilter',
        gmvTrainingCriteria: '../crm/training/view/gmvTrainingCriteria',
		gmvTrainingDetail: '../crm/training/view/gmvTrainingDetail',
        gmvTrainingFilter: '../crm/training/view/gmvTrainingFilter',
        gmvCRFCriteria: '../crm/crf/view/gmvCRFCriteria',
		gmvCRFDetail: '../crm/crf/view/gmvCRFDetail',
        gmvCRFFilter: '../crm/crf/view/gmvCRFFilter',
        gmvFieldSalesCriteria: '../crm/fieldsales/view/gmvFieldSalesCriteria',
        gmvFieldSalesDetail: '../crm/fieldsales/view/gmvFieldSalesDetail',
        gmvFieldSalesFilter: '../crm/fieldsales/view/gmvFieldSalesFilter',
        gmvFieldSalesData: '../crm/fieldsales/view/gmvFieldSalesData',
		gmvSalesCallCriteria: '../crm/salescall/view/gmvSalesCallCriteria',
        gmvSalesCallDetail: '../crm/salescall/view/gmvSalesCallDetail',
        gmvSalesCallFilter: '../crm/salescall/view/gmvSalesCallFilter',
		gmvSurgeonCriteria: '../crm/surgeon/view/gmvSurgeonCriteria',
		gmvSurgeonDetail: '../crm/surgeon/view/gmvSurgeonDetail',               
        gmvSurgeonSurgicalActivity: '../crm/surgeon/view/gmvSurgeonSurgicalActivity',
        gmvSurgeonSurgicalUtil: '../crm/surgeon/view/gmvSurgeonSurgicalUtil',
        gmvSurgeonSummaryReport: '../crm/surgeon/view/gmvSurgeonSummaryReport',
		gmvSurgeonEducation: '../crm/surgeon/view/gmvSurgeonEducation',
		gmvSurgeonReport: '../crm/surgeon/view/gmvSurgeonReport',
		gmvValidation: '../common/view/gmvValidation',
        gmvItemList: '../common/view/gmvItemList',
        gmvCRMAddress: '../common/view/gmvCRMAddress',
        gmvCRMFileUpload: '../common/view/gmvCRMFileUpload',
        gmvMultiFileUpload: '../common/view/gmvMultiFileUpload',
        gmvImageCrop: '../common/view/gmvImageCrop',
        gmvSurgeonFilter: '../crm/surgeon/view/gmvSurgeonFilter',
        gmvSurgeonSurgicalCalendar: '../crm/surgeon/view/gmvSurgeonSurgicalCalendar',
        gmvAccountSurgeonRpt: '../crm/Account/view/gmvAccountSurgeonRpt',
        gmvAccountSurgicalActivity: '../crm/Account/view/gmvAccountSurgicalActivity',
        gmvAccountSurgicalCalendar: '../crm/Account/view/gmvAccountSurgicalCalendar',
        gmvPSCriteria: '../crm/preceptorship/view/gmvPSCriteria',
        gmvPSSchedule: '../crm/preceptorship/view/gmvPSSchedule',
        gmvPSHost: '../crm/preceptorship/view/gmvPSHost',
        gmvPSCase: '../crm/preceptorship/view/gmvPSCase',
        gmvPSUtil: '../crm/preceptorship/view/gmvPSUtil',
        gmvPSList: '../crm/preceptorship/view/gmvPSList',
		gmvEventEvalForm: '../quality/eventevalform/view/gmvEventEvalForm',
		gmvEventEvalRpt: '../quality/eventevalform/view/gmvEventEvalRpt',
        gmvMercReport: '../crm/merc/view/gmvMercReport',
		
		/* Model */
		gmmItem: '../common/model/gmmItem',
		gmmUser: '../common/model/gmmUser',
		gmmSurgeon: '../crm/surgeon/model/gmmSurgeon',
        gmmSearch: '../common/model/gmmSearch',
        gmmAddress: '../common/model/gmmAddress',
        gmmActivity: '../common/model/gmmActivity',
        gmmFileUpload: '../common/model/gmmFileUpload',
        gmmPS: '../crm/preceptorship/model/gmmPS',
        gmmMerc: '../crm/merc/model/gmmMerc',
		
		/* Collection */
		gmcItem: '../common/collection/gmcItem',
        gmcAddress: '../common/collection/gmcAddress',
		
		/*Case Scheduler*/        
        gmrCaseScheduler: '../sales/casescheduler/router/gmrCaseScheduler',        
        gmmCase: '../sales/casescheduler/model/gmmCase',
        gmvCase: '../sales/casescheduler/view/gmvCase',
        gmvCaseDash: '../sales/casescheduler/view/gmvCaseDash',
		gmvCaseCalendar: '../sales/casescheduler/view/gmvCaseCalendar',
        
        /*Surgery Kit*/        
        gmrKit: '../sales/kit/router/gmrKit',
        gmvKit: '../sales/kit/view/gmvKit',
        gmvKitMgmt: '../sales/kit/view/gmvKitMgmt',
        gmtKitHome: '../sales/kit/view/gmtKitHome',        
        gmmKit: '../sales/kit/model/gmmKit',
	
	/* Price Collection and models */
	gmmGroup: '../pricing/model/gmmGroup',
	gmmSystem: '../pricing/model/gmmSystem',
        gmmAccountPriceRequest: '../pricing/model/gmmAccountPriceRequest',
        gmmBusinessQuestions: '../pricing/model/gmmBusinessQuestions',
	gmcGroup: '../pricing/collection/gmcGroup',
	gmcSystem: '../pricing/collection/gmcSystem',
        gmcBusinessQuestions: '../pricing/collection/gmcBusinessQuestions',
            gmcFileUpload: '../common/collection/gmcFileUpload',
            gmmFileUpload: '../common/model/gmmFileUpload',

        gmtTemplate: '../template/template',

		gmvLocalDB: '../common/view/gmvLocalDB'        
	}
});

require({
			// The local storage variable is checked here to get the locale value, if assigned
			locale : localStorage.getItem('locale')
		}, 
		[ 
	        'jquery',
	        'backbone',
	        'fastclick',
	        'database',
	        'datasync',
	        'global',
	        'commonutil',
	        'pricingCommon',
	        'server',
			'mainrouter',
	        'notification',
	        'catalogrouter',
	        'collateralrouter',
	        'dashboardrouter',
	        'dorouter',
            	'pricerouter',
	        'gmrCaseScheduler',
			'invrouter',
			'timezoneJS',
    'date',
			'gmrSurgeon', 'gmrActivity', 'gmrCRF', 'gmrFieldSales', 'gmrPS', 'gmvApp','gmrEval','gmcreorouter', 'gmrKit','gmrMerc','globusMobileApp/sales/scorecard/router/gmrScorecard.js'], function ($, Backbone, Fastclick, DB, DataSync, Global,CommonUtil, pricingCommon, Server, MainRouter, Notification, CatalogRouter, CollateralRouter, DashboardRouter, DORouter,PriceRouter, GMRCaseScheduler,InvRouter,timezoneJS,Date,
         			  GMRSurgeon, GMRActivity, GMRCRF, GMRFieldSales, GMRPS, GMVApp,GMREval,GmCreoRouter, GMRKit,GMRMerc,GMRScorecard) {

	$(document).ready(function() {

		initiateHelpers();

		/* Loading CSS */
		loadCSS(URL_CSS + "app/GmReset.css");
		loadCSS(URL_CSS + "app/GmCommonStyle.css");
		loadCSS(URL_CSS + "app/GmLoginStyle.css");
		loadCSS(URL_CSS + "app/GmCatalogStyle.css");
		loadCSS(URL_CSS + "app/GmCollateralStyle.css");
		loadCSS(URL_CSS + "app/GmDetailViewStyle.css");
		loadCSS(URL_CSS + "app/GmiPhoneStyle.css");
		loadCSS(URL_CSS + "app/GmThreeSixty.css");
		loadCSS(URL_CSS + "lib/font-awesome.css");
		loadCSS(URL_CSS + "app/GmDashboardStyle.css");
		loadCSS(URL_CSS + "app/GmSettingsStyle.css");
		loadCSS(URL_CSS + "lib/jquery-ui.min.css");
		loadCSS(URL_CSS + "lib/jquery.Jcrop.css");
		loadCSS(URL_CSS + "lib/jquery.timepicker.min.css");
		loadCSS(URL_CSS + "app/GmDOStyle.css");
		loadCSS(URL_CSS + "app/GmAddressStyle.css");
		loadCSS(URL_CSS + "app/GmCaseStyle.css");
		loadCSS(URL_CSS + "app/GmInvStyle.css");
		/* Loading CRM CSS */
        
        
	
        
        loadCSS(URL_CSS + "app/GmCRMCommon.css");
        loadCSS(URL_CSS + "app/GmCRMDashboard.css");
        loadCSS(URL_CSS + "app/GmCRMLogin.css");
        loadCSS(URL_CSS + "app/GmCRMSalesCall.css");
        loadCSS(URL_CSS + "app/GmCRMFieldSales.css");
        loadCSS(URL_CSS + "app/GmCRMSurgeon.css");
        /* Loading Pricing CSS start*/
	   loadCSS(URL_CSS + "app/GmPriceStyle.css");
        loadCSS(URL_CSS + "app/GmPriceCommon.css");
         /* Loading Pricing CSS End*/
        loadCSS(URL_CSS + "app/GmCRMSurgeonCalendar.css");
        loadCSS(URL_CSS + "app/GmCRMPhysicianVisit.css");
        loadCSS(URL_CSS + "app/GmCRMLead.css");
        loadCSS(URL_CSS + "app/GmCRMMERC.css");
        loadCSS(URL_CSS + "app/GmCRMTraining.css");
        loadCSS(URL_CSS + "app/GmCRMCRF.css");
        loadCSS(URL_CSS + "app/GmPreceptor.css");
		loadCSS(URL_CSS + "app/GmEvalForm.css");
        loadCSS(URL_CSS + "app/GmCRMGrid.css");
        loadCSS(URL_CSS + "app/GmKitStyle.css");
		loadCSS(URL_CSS + "app/GmCase.css");
        loadCSS(URL_CSS + "app/GmScorecard.css");
        loadCSS(URL_CSS + "lib/dhtmlxScheduler/dhtmlxscheduler.css");		
	
		
		document.addEventListener("deviceready", onDeviceReady, false);  // on app in focus
        
		/* PhoneGap is loaded. It is now safe to make calls to PhoneGap functions */
		function onDeviceReady() {
            GM.Global = $.parseJSON(localStorage.getItem("GlobalVariables"));
            if(GM.Global==null)
                GM.Global = {};
			checkConnection();
			Fastclick.attach(document.body);
			document.addEventListener("online", onOnline, false);
			document.addEventListener("offline", onOffline, false);
            cordova.getAppVersion.getVersionNumber().then(function (appVersion) {
                version = appVersion;
            });
            
		}

		/* Check internet connectivity status of the device. If connected, display the type of connection */
		function checkConnection() {
			var networkState = navigator.connection.type;

			var states = {};

			states[Connection.UNKNOWN]  = 'Unknown connection';
			states[Connection.ETHERNET] = 'Ethernet connection';
			states[Connection.WIFI]     = 'WiFi connection';
			states[Connection.NONE]     = 'No network connection';
			states[Connection.CELL] = 'Cellular';

			var NetworkStatus = states[networkState];

			if(networkState === Connection.NONE) {
				showError("Connection Status: " + NetworkStatus);
                GM.Global.Device = 1;
                if(window.location.hash == ""){     // login page
                    $(".CRMoffline").addClass("hide");
                    $(".CRMonline").addClass("hide");
                }
                else{
                    if(localStorage.getItem("moduleaccess").indexOf("btn-crm")<1)
                         if ($(window).width() <= 480) {
                                   $('.CRMoffline').addClass('hide');
                                }
                    else{
                       $(".CRMoffline").removeClass("hide");  
                    }
                       
                    else
                        $(".CRMoffline").remove();
					// we are removing crm offline based on User availability in CRM_APP_ACCESS security Group.   
                    if(localStorage.getItem("CRM_APP_ACCESS")=="N"){
                       $(".CRMoffline").remove();
                    }
                    $(".CRMonline").addClass("hide");
                }
			}
			else {
				showSuccess("Connection Status: " + NetworkStatus);
                GM.Global.Device = 0;
                if(window.location.hash == ""){         // login page
                    $(".CRMoffline").addClass("hide");
                    $(".CRMonline").addClass("hide");
                }
                else{
                    $(".CRMoffline").addClass("hide");
                    $(".CRMonline").removeClass("hide");
                }
                if(window.location.hash == "#pending") //to navigate away from pending sync screen on internet
                    window.location.href = "#crm";
                
				if(intLogin) {
//					fnCheckShareHistory(); Due to PMT-23618 code has been commented
                    fnProcessTagImageForPendingSyncDO(); //To Process the DO TAG Images for Offline DO's
                    setTimeout(function () {
                         fnCheckPendingSync();
                         fnCheckDOPDFPendingSync();
                     }, 15000); //Need time to Process the Image and after some seconds call the Pending Sync DO's
					fnCheckCasePendingSync();
					fnCheckRequestPendingSync();
                    fnCheckCRMPendingSync();
				}
				else if(intOfflineMode){
					navigator.notification.confirm(
				        "Internet connection is available. But you are currently working in Offline Mode. Would you like to login again to take advantage of all internet-requiring functionalities within the application?",
				        function(btnValue){
				         	if(btnValue==1) 
								window.location.href = "#";
                            else{
                                GM.Global.Device = 1;   // Even with internet if the user wants to be offline, let him be
                                if ($(window).width() <= 480) {
                                   $('.CRMoffline').addClass('hide');
                                }
                                else{
                                   $(".CRMoffline").removeClass("hide"); 
                                }
                                $(".CRMonline").addClass("hide");
                            }
                                
				        },           
				        "Online",
				        ["Yes","No"]    
				    );
				}
			}
		}
        
        

		/* Activities to be done when the device has internet connectivity */
		function onOnline() {
			checkConnection();
		}

		/* Activities to be done when the device has no internet connectivity */
		function onOffline() {
			checkConnection();
		}

        GM.Global.Mobile = 0;
        
		/* Default the Locale value to English if the local storage is null */
		if(!localStorage.getItem('locale')) {
			localStorage.setItem('locale','en-US');
		}
        if(localStorage.getItem('moduleaccess') == null ) {
			localStorage.setItem('moduleaccess','');
		}
        
		if(typeof device != 'undefined'){   // Checking if it is a Device. Only in case of a device will it  not be of undefined type. If 'yes' then setting device mode
            deviceModel = device.model;
		    deviceUUID = device.uuid;
		    deviceVersion = device.platform +" "+ device.version;
            GM.Global.Browser = 0;
            devicePlatform = device.platform;
        }
        else{
              deviceModel = "device.model";
		     deviceUUID = "device.uuid";    
              GM.Global.Browser = 1;
        }


		if(deviceModel.toUpperCase().indexOf('IPAD') == -1 ) {
            $('head').append('<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, target-densityDpi=device-dpi" />');
		}
        
        // Global variable set for differentiate the mobile and iPad devices 
        if (window.innerWidth <= "480") {
            GM.Global.Mobile = 1;
        }
		/* Open Database */
		fnOpenDB(function() {
            
			getTable("TBL_LOCALE");
			fnGetMessageData();
       	fnGetTableCount(function(data){
                    getSyncCount(data);
            var token = localStorage.getItem("token");
            if(token!=null){
				//Call the below function to sync the Tag data in App Startup
                fnGetLocalTagDataCount(function (datalen){
				if(datalen==0){
				   fnsyncTagDataInAppStartup();
				}
				});
             }
            });
            
		});
        fnGetRuleValues('MODE_EXCLUSION', 'MODEEXCLUDE', localStorage.getItem("cmpid"), function (data) {
            if(data !=null ){
                if(data.rulevalue != ''){
                    localStorage.setItem("ModeExclusion",data.rulevalue);  
                }
            }
        }); 
        if(GM.Global.Browser){
            GM.Global = $.parseJSON(localStorage.getItem("GlobalVariables"));
            if(GM.Global==null)
                GM.Global = {};
        }
        
        GM.Global.Url = [window.location.hash];
        $(window).on('hashchange', function(){  // Used to set last and previous URL
             if(GM.Global.Portal)
                GM.Global.PortalUrl = window.location.hash;
            if(GM.Global.Url == undefined)
                GM.Global.Url = [];     
            GM.Global.Url.push(window.location.hash);
            GM.Global.PreUrl = GM.Global.Url[(GM.Global.Url.length-2)];
        });
        GM.Global.CRMMain = undefined;
        GM.Global.FSViews = undefined;
        if(GM.Global.Device){
            GM.Global.UserTabs = (localStorage.getItem('userTabs')!=null)?JSON.parse(localStorage.getItem('userTabs')):[];
            GM.Global.Admin = (localStorage.getItem('userAdmin')!=null)?JSON.parse(localStorage.getItem('userAdmin')):[];
            GM.Global.UserAccess = (localStorage.getItem('userAccess')!=null)?JSON.parse(localStorage.getItem('userAccess')):[];
        }

		// PMT-29490: Japan label change
        getLocaleData(); // func used for to get locale values
        syncServiceComponent(); //function used to load firebase credentials
        loadReadImageAPIEndpoint(); //used to take Amazon computer vision end point from rule table
        loadFaxAuthDetails(); //used to get the Fax authorization code
		new MainRouter();
		new GMRCaseScheduler();
		new CatalogRouter();
		new CollateralRouter();
		new DashboardRouter();
		new DORouter();
        	new PriceRouter();
		new InvRouter();
        new GmCreoRouter();
		
		// CRM Routers
		new GMRActivity();
		new GMRCRF();
        new GMRSurgeon();
        new GMRFieldSales();
        new GMRPS();
		new GMREval();        
        new GMRKit();
        new GMRMerc();
        new GMRScorecard();

        gmvApp = new GMVApp();

		Backbone.history.start();
	});
});
})();
