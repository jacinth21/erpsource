define({
    /* 
      ROOT is the default settings to which the contents of the app will default.
      When the locale value is set, but if that locale does not have a translation 
      for a particular content, the app will default to the root's values
    */
    'root': {
        back: "Back",
        code: "en-EN",
      	region: "English",
        home: "Home",
        settings: 'Settings',
        updates: 'Updates',
        logout: 'Logout',
        placeholder: { 
          	username: 'Username',
          	password: 'Password'
        },
        image: { 
            region: "US.png"
        }
    },

    /* Here is where we tell which locales can fall back to the root values above */
    'de-DE': true,
    'fr-FR': true
});