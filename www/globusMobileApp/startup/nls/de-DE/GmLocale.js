define({
	/* This is the set of values to which the app would translate, when the locale is German */
  	back: "Zuruck",
  	code: "de-DE",
  	region: "Deutsch",
  	settings: 'Einstellungen',
  	updates: 'Aktualisierung',
  	logout: "Abmeldung",
	image: { 
      	region: "DE.png"
	}
});