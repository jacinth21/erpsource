define({
	/* This is the set of values to which the app would translate, when the locale is French */
  	code: "fr-FR",
  	region: "Française",
  	image: { 
      	region: "FR.png"
	}
});