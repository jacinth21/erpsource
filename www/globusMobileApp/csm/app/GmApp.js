// module creation - variable name should be same as module name
var caseManagement = angular.module("caseManagement", ["ngRoute"]);

// View Routing - always define controllers inside "when" block
caseManagement.config(['$routeProvider', function($routeProvider){
    $routeProvider
        .when('/GmDashboard', {
            templateUrl: 'template/GmDashboard.html',
            controller: 'GmDashboardController'
        })
        .when('/GmCaseSetup/:action/:caseId', {
            templateUrl: 'template/GmCaseSetup.html',
            controller: 'GmCaseSetupController'
        })
        .when('/GmCalendarView', {
            templateUrl: 'template/GmCalendarView.html',
            controller: 'GmCalendarViewController'
        })
        .when('/GmAdvanceSearchReport/:action', {
            templateUrl: 'template/GmAdvanceSearchReport.html',
            controller: 'GmAdvanceSearchReportController'
        }).when('/GmQuickSearch', {
            templateUrl: 'template/GmQuickSearch.html',
            controller: 'GmQuickSearchController'
        })
        .when('/GmShareCase/:caseId', {
            templateUrl: 'template/GmShareCase.html',
            controller: 'GmShareCaseController'
        })
        .otherwise({
            redirectTo: '/GmDashboard'
        });
}]);
