/*Constant variables for Alert */
var fileSizeLimit ="File size cannot exceed 10 MB";
var fileEmpty ="No files uploaded";
var fileMaxLimit ="Max. total file size 50 MB";
var fileValidation ="Invalid File";
var validFormats = ['jpg','jpeg','png','svg','pdf','mp4','avi','mkv','doc','txt','docx'];
var fileDuplicate ="The File already exists";
var ipLength =3;

// Firesbase project configuration
var GM_firebaseConfig = {
    apiKey: "AIzaSyBektzeHPb9JdDqcA4WqkdcXVTrUUNuJlU",
    authDomain: "svcfiredev-cdccc.firebaseapp.com",
    projectId: "svcfiredev-cdccc",
    storageBucket: "svcfiredev-cdccc.appspot.com",
    messagingSenderId: "638853643248",
    appId: "1:638853643248:web:669fac561490f524237b35",
    measurementId: "G-Q9FBT6YQ3B"
};