// Singleton sqlite object
var GmSqlite = (function () {
    var connection;

    function createSqlite() {
        // var db = window.openDatabase("gmiData", "1.0", "Globus Mobile Database", 10000);
        var db = parent.window.db;
        return db;
    }

    return {
        getConnection: function () {
            if (!connection) {
                connection = createSqlite();
            }
            return connection;
        }
    };
})();