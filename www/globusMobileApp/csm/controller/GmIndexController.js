// always pass $scope variable to the service layer
caseManagement.controller("gmIndexController", function($scope){  
    $scope.openCalendar= function(){
        window.location.href="#!GmCalendarView";
    }
	$scope.openCreateCase= function(){
        window.location.href="#!GmCaseSetup/create/0";
    }
    $scope.openReportCase= function(){
        window.location.href="#!GmAdvanceSearchReport/upcomingCases";
    }
    $scope.openDashboard= function(){
        window.location.href="#!GmDashboard";
    }
    $scope.openSearchCase= function(){
        window.location.href="#!GmAdvanceSearchReport/search";
    }
});