caseManagement.controller("GmCaseSetupController",function($scope,$routeParams){
    $scope.edit=false;
    $scope.create=true;
    var TypeLength = 2;
    $scope.accountChange = false;
    $scope.caseCoveredByChange = false;
    $scope.surgeonData = {};
    $scope.surgeonChip = [];
    $scope.shareChip = [];
    $scope.surgeonChipTag = [];
    $scope.shareChipTag = [];
    $scope.accountsDetails = {};
    $scope.coveredByDetails = {};
    $scope.shareWithDetails = {};
    $scope.surgeonList = {};
    $scope.salesRepDetails = {};
    $scope.editFiles = [];
    $scope.loginTimezone=Intl.DateTimeFormat().resolvedOptions().timeZone;
    //Initiate the case setup
    $scope.init = function(){
        //Date format form local storage
        $scope.dateFormat = localStorage.getItem('cmpdfmt');
        $scope.isLoginUserCaseOwner = function() {

    }
    $scope.newCase={};
    //Fetch the data from the Edit field
    $scope.fetchCase = function () {
      $scope.editCaseId = $routeParams.caseId;
      $scope.newCase = new Object();
      const caseController = new GmCaseDao('FT7600_CASE');
      $scope.cases = new Array;
      editCase(caseController);
      async function editCase(caseController) {
        await caseController.editCase($scope);
      }
    }
    // Edit field data
    if ($routeParams.action == "edit") {
      $scope.action = "edit";
      $scope.fetchCase();
      $scope.edit = true;
      $scope.create = false;
      $scope.deletedFilesList = [];
      $scope.deleteFileFromForm = function (fId, fNm, fPath, fType, fSize, index) {
        $scope.deletedFilesList.push({ "fId": fId, "fNm": fNm, "fPath": fPath, "fType": fType, "fSize": fSize });
        $scope.editFiles.splice(index, 1);
      }
    }
    else if ($routeParams.action == "copy") {
      $scope.action = "copy";
      $scope.fetchCase();
    }
  }
  //Time validation
  $scope.endTimeChange = function () {
    if ($scope.newCase.endTime != undefined && $scope.newCase.startTime != undefined) {
      var startTime = $scope.newCase.startTime;
      var endTime = $scope.newCase.endTime;
    
      function getTimeDiff(start, end) {
          return moment.duration(moment(end, "HH:mm:ss a").diff(moment(start, "HH:mm:ss a")));
      }

      diff = getTimeDiff(startTime, endTime);
      if (diff.hours() < 1) {
        var dateValidateModal = document.getElementById('dateValidateModal');
        var instance = M.Modal.init(dateValidateModal, {});
        instance.open();
        $scope.newCase.endTime = undefined;
      }
    }
  }
  //procedure List from firebase
  var codeLookupController = new GmCodeLookupDao('T901_CODE_LOOKUP');
  $scope.procedureListArray = [];
  loadCodeLookup(codeLookupController);
  async function loadCodeLookup(codeLookupController) {
    codeLookupController.loadCodeLookup($scope, function ($scope) {
      var procArr = $scope.procedureListArray;
      for (var i = 0; i < procArr.length; i++) {
        $('#procedure').append($("<option></option>").attr("value", procArr[i].id).text(procArr[i].name));
        M.AutoInit();
      }
    });
  }
  //timezone List from firebase
  var timezoneController = new GmCodeLookupDao('TimeZone');
  $scope.timezoneListArray = new Array();
  loadTimeZone(timezoneController);
  async function loadTimeZone(timezoneController) {
    timezoneController.loadTimeZone($scope, function ($scope) {
      for (var i = 0; i < $scope.timezoneListArray.length; i++) {
        if ($scope.timezoneListArray[i].timezone == $scope.loginTimezone) {
          $('#timeZone').append($("<option selected></option>").attr("value", $scope.timezoneListArray[i].timezone).text($scope.timezoneListArray[i].timezone));
        } else {
          $('#timeZone').append($("<option></option>").attr("value", $scope.timezoneListArray[i].timezone).text($scope.timezoneListArray[i].timezone));
        }
        M.AutoInit();
      }
    });
  }
  $scope.empty = false;
  $scope.selectedAccount = {};
  //hospital List from sqlite
  $scope.typeHospital = function () {
    if ($scope.newCase.hospital != null && $scope.newCase.hospital.length > TypeLength) {
      $scope.accountChange = true;

      const obj = new GmAccountDao();
      $scope.accounts = {};
      $scope.accountObj = new Object();
      console.log("loading Accounts");
      loadAccounts(obj);
      console.log("after Accounts");
      async function loadAccounts(obj) {
        obj.getAccounts($scope, function ($scope) {
          var hospital = M.Autocomplete.getInstance(document.getElementById('hospital'));
          hospital.updateData($scope.accounts);
          hospital.open();
        });
      }
    }
    else {
      $scope.accounts = {};
      var hospital = document.getElementById('hospital');
      M.Autocomplete.init(hospital, { data: $scope.accounts, noOptionsText: $scope.options });
    }
  }
  //Surgeon data from sqlite
  $scope.typeSurgeon = function () {
    const obj = new GmSurgeonDao();
    searchSurgeonInfo(obj);

            async function searchSurgeonInfo(obj) {
              obj.searchSurgeonInfo($scope, function ($scope) {
                console.log("callback");
               });
            }
    }
    //sales Rep data from sqlite
    $scope.typeShareWith=function(){
          const obj = new GmSalesRepDao();
          searchRep(obj);
          async function searchRep(obj) {
            obj.searchRep($scope, function ($scope) {
             console.log("callback");
            });
          }
    }
	$scope.procedureChange = function(){
       $( "#procedurespan").remove();
      $( ".procedure" ).append( "<span id='procedurespan'><b>"+ $("#procedure option:selected").text() +"</b></span>" );
    }
    $('.chips-autocomplete').chips({
        placeholder: 'Search..',
        appendTo: "#someElem",
        noItem : 'no data found',
        autocompleteOptions: {
          function :$scope.typeSurgeon(),
          data: $scope.surgeonList,
          limit: Infinity,
          minLength: 3,
          delay: 1000,
        },
        onChipAdd: function(e, chip){
          var item = chip.childNodes[0].textContent;
          if($scope.surgeonList[item] !== null){
              $('.chips-autocomplete .chip').last().remove();
          }
          else{
            $scope.surgeonChip.push(item);
			$('#tags').val(item).change();
            $scope.check = false;
          }
        },
        onChipDelete: function(e, chip){
          var item = chip.childNodes[0].textContent;
          var index = $scope.surgeonChip.indexOf(item);
          $scope.surgeonChip.splice(index, 1);
          $scope.check = false;
          $scope.tags = $.grep(tags, function(value) {
            return value != item;
        });
        }
      });
		$("#tags").bind("change",function(){
        $( ".surgeon" ).append( "<span><b>"+$('#tags').val()+"</b></span>" );
      });
      $('.chips-autocomplete-share').chips({
        placeholder: 'Search..',
        appendTo: "#someElem",
        autocompleteOptions: {
          function :$scope.typeShareWith(),
          data: $scope.salesRepDetails,
          limit: Infinity,
          minLength: 3,
          delay: 1000,
        },
        onChipAdd: function(e, chip){
          var item = chip.childNodes[0].textContent;
          if($scope.salesRepDetails[item] !== null){
              $('.chips-autocomplete .chip').last().remove();
          }
          else{
            $scope.shareChip.push(item);
            $scope.check = false;
          }
        },
        onChipDelete: function(e, chip){
          var item = chip.childNodes[0].textContent;
          var index = $scope.shareChip.indexOf(item);
          $scope.shareChip.splice(index, 1);
          $scope.check = false;
          $scope.tags = $.grep(tags, function(value) {
            return value != item;
        });
        }
      });
      //set case covered name in search box
      $scope.typeSalesRep = function(){
        if($scope.newCase.caseCoveredBy != null && $scope.newCase.caseCoveredBy.length > TypeLength){
           $scope.caseCoveredByChange = true;
            $scope.salesRep = {};
            $scope.salesRepSearchKey =  $scope.newCase.caseCoveredBy;
            const obj = new GmSalesRepDao();
            loadSalesRep(obj);
            async function loadSalesRep(obj) {
              obj.getSalesRep($scope, function ($scope) {
                var caseCoveredBy = M.Autocomplete.getInstance(document.getElementById('caseCoveredBy'));
                caseCoveredBy.updateData($scope.salesRep);
                caseCoveredBy.open();
              });
            }
        }
        else{
            $scope.salesRep = {};
            var caseCoveredBy = document.getElementById('caseCoveredBy');
            M.Autocomplete.init(caseCoveredBy, { data: $scope.salesRep ,   noOptionsText :  $scope.options});
        }
    }
    $scope.$watchGroup(['newCase.hospital','newCase.excelsius','newCase.procedure','newCase.level','newCase.procedure','newCase.patientId','newCase.startDate','newCase.startTime','newCase.endTime','newCase.timeZone','newCase.caseCoveredBy','newCase.shareWith','newCase.notes','newCase.caseTitle'], function(newVal, oldVal){
      for(i=0;i<oldVal.length;i++){
            if(oldVal[i] != newVal[i]){
              $scope.check = false;
              return false;
            }else{
              $scope.check = true;
            }
        }
      },true);

     $scope.$on('$locationChangeStart', function( event ) {
         if( $scope.check == false){
           var answer = confirm("Are you sure you want to leave this page?");
           if (!answer) {
             event.preventDefault();
           }
         }
       });

/*Add and Remove the Files,File Size checking*/
$scope.caseId = $routeParams.caseId;
$scope.selectedFiles= [];

$scope.onSelectedFiles = function(element) {
    $scope.theFile = element.files;
    var flag = 0;
    var totalSelectedFileSize = 0;
   
    var fileLength = element.files.length;
    $scope.validationMessage=fileSizeLimit;
    $scope.validationMessageOfMax=fileMaxLimit;
    $scope.fileValidation=fileValidation;
    $scope.validFormats=validFormats;
    for (var j = 0; j < fileLength; j++){
        var fileNameForExt = element.files[j].name;
        var fileExt= fileNameForExt.substr(fileNameForExt.lastIndexOf('.') + 1);
        var fileValid = $scope.validFormats.includes(fileExt);  
        if(fileValid == true){
            $scope.fileSize =element.files[j].size;
            $scope.fileList = element.files[j];
            totalSelectedFileSize += $scope.fileSize ;
            $scope.selectedFiles.push($scope.fileList);
            }else{
               $scope.selectedFiles= [];
               var fileInvalidValidateModal = document.getElementById('fileInvalidValidateModal');
               var instance = M.Modal.init(fileInvalidValidateModal,{});
               instance.open();
               return false;
        }
    }
    $scope.selectedFiles;
    $scope.$apply();
}; 

/*Remove the File from selected Files*/
$scope.removeFile = function(index) {
    $scope.selectedFiles.splice(index,1);
}

  $scope.createCase = function () {
    var caseController = new GmCaseDao('FT7600_CASE');
    var procedure = $("#procedure option:selected").text();
    var timeZone = $('#timeZone').val();
    $scope.newCase.procedureId = $('#procedure').val();
    if ($scope.surgeonChip.length == 0) {
      var conformModal = document.getElementById('conformModal');
      var instance = M.Modal.init(conformModal, {});
      instance.open();
      return false;
    }
    $scope.newCase.procedure = procedure;
    $scope.newCase.timeZone = timeZone;
    console.log("procedure:" + $scope.newCase.procedure);
    if ($scope.accountChange == true) {
      $scope.newCase.accountId = $scope.accountsDetails[$scope.newCase.hospital];
    }
    if ($scope.caseCoveredByChange == true) {
      $scope.newCase.caseCoveredById = $scope.coveredByDetails[$scope.newCase.caseCoveredBy];
    }
    $scope.newCase.accountName = $scope.newCase.hospital;
    const obj = new GmAccountDao();
    $scope.editedFiles = $scope.editFiles;
    $scope.fileDuplicateMsg = fileDuplicate;

    if ($scope.newCase.caseId == undefined) {
      var totalFileSize = 0;
      for (let k = 0; k < $scope.selectedFiles.length; k++) {
        totalFileSize += $scope.selectedFiles[k].size;
        if ($scope.selectedFiles[k].size / 1024 / 1024 > 10) {
          $scope.individualFileName = $scope.selectedFiles[k].name;
          var individualFileLimitCheck = document.getElementById('individualFileLimitCheck');
          var instance = M.Modal.init(individualFileLimitCheck, {});
          instance.open();
          return false;
        }
        for (let l = k + 1; l < $scope.selectedFiles.length; l++) {
          if ($scope.selectedFiles[k].name == $scope.selectedFiles[l].name) {
            $scope.fDuplicateName = $scope.selectedFiles[k].name;
            var fileDuplicationValidateModal = document.getElementById('fileDuplicationValidateModal');
            var instance = M.Modal.init(fileDuplicationValidateModal, {});
            instance.open();
            return false;

          }
        }
      }
      if (totalFileSize / 1024 / 1024 > 50) {
        var totalFileSizeCheck = document.getElementById('totalFileSizeCheck');
        var instance = M.Modal.init(totalFileSizeCheck, {});
        instance.open();
        return false;
      }

    } else {
      var editTotalFileSize = 0;
      for (let i = 0; i < $scope.editedFiles.length; i++) {
        for (let j = 0; j < $scope.selectedFiles.length; j++) {
          editTotalFileSize += $scope.selectedFiles[j].size;
          if ($scope.selectedFiles[j].size / 1024 / 1024 > 10) {
            $scope.individualFileName = $scope.selectedFiles[j].name;
            var individualFileLimitCheck = document.getElementById('individualFileLimitCheck');
            var instance = M.Modal.init(individualFileLimitCheck, {});
            instance.open();
            return false;
          }
          if ($scope.selectedFiles[j].name === $scope.editedFiles[i].nm) {
            $scope.fDuplicateName = $scope.selectedFiles[j].name;
            var fileDuplicationValidateModal = document.getElementById('fileDuplicationValidateModal');
            var instance = M.Modal.init(fileDuplicationValidateModal, {});
            instance.open();
            return false;
          }
        }
      }
      if (editTotalFileSize / 1024 / 1024 > 50) {
        var totalFileSizeCheck = document.getElementById('totalFileSizeCheck');
        var instance = M.Modal.init(totalFileSizeCheck, {});
        instance.open();
        return false;
      }
    }
    obj.getAccountRepInfo(function (repId, repNm) {
      $scope.newCase.accRepId = repId;
      $scope.newCase.accRepName = repNm;
      caseController.createCase($scope);
      $scope.check = true;
      var createSuccessModal = document.getElementById('createSuccessModal');
      var instance = M.Modal.init(createSuccessModal, {});
      instance.open();
    }, $scope.newCase.accountId);
  }
  $scope.initCalendar = function ($scope) {
    console.log("init calendar");
    $scope.newCase.startDate = "";
    // init calendar without container, use null instead of container
    switch($scope.dateFormat){
      case "MM/dd/yyyy":
        var format = "%m/%d/%y";
        break;
      case "dd/MM/yyyy":
        var format = "%d/%m/%y";
        break;
      case "yyyy/MM/dd":
        var format = "%y/%m/%d";
        break;
    }
    const calendar = new dhx.Calendar(null, { dateFormat: format, css: "dhx_widget--bordered" });
    // init popup and attach calendar
    const popup = new dhx.Popup();
    popup.attach(calendar);
    const dateInput = document.getElementById("startDate");
    popup.show(dateInput);

    calendar.events.on("change", function () {
      dateInput.value = calendar.getValue();
      $scope.newCase.startDate = calendar.getValue();
      $scope.$apply();
      popup.hide();
    });

  }

  $scope.initStartTime = function ($scope) {
    console.log("init start time");
    $scope.newCase.startTime = "";
    const popup2 = new dhx.Popup({
      css: "dhx_widget--bordered"
    });

    const timepicker1 = new dhx.Timepicker({
      timeFormat: 12
    });
    const startTime = document.getElementById("startTime");
    timepicker1.events.on("change", function (res) {
      startTime.value = res;
      $scope.newCase.startTime = res;
    });

    popup2.attach(timepicker1);
    popup2.show("startTime");

  }

  $scope.initEndTime = function ($scope) {
    console.log("init End time");
    $scope.newCase.endTime = "";
    const popup1 = new dhx.Popup({
      css: "dhx_widget--bordered"
    });

    const timepicker2 = new dhx.Timepicker({
      timeFormat: 12
    });
    const endTime = document.getElementById("endTime");
    timepicker2.events.on("change", function (res) {
      endTime.value = res;
      $scope.newCase.endTime = res;
    });
    popup1.attach(timepicker2);
    popup1.show("endTime");

  }

});