caseManagement.controller("GmCaseSetupSearchController", ['$scope', function($scope){
    //procedure List from firebase
    var codeLookupController = new GmCodeLookupDao('T901_CODE_LOOKUP');
    $scope.codeLookup = new Array();
    codeLookupController.loadCodeLookup($scope);
    //timezone List from firebase
    var timezoneController = new GmCodeLookupDao('TimeZone');
    timezoneController.loadTimeZone($scope);
    var TypeLength = 2;
    //Search SalesRep details from sqlite
    $scope.salesRepSearch = function() {
        const obj = new GmSalesRepDao("Cases");
        loadSalesRep(obj);

        async function loadSalesRep(obj) {
            var accts = await obj.getSalesRep($scope);
        }
      }
      $scope.empty=false;
    //hospital data from sqlite
    $scope.typeHospital=function(){
        if($scope.newCase.hospital != null && $scope.newCase.hospital.length > TypeLength){
            const obj = new GmAccountDao("Cases");
            $scope.accounts = {};
            $scope.accountObj = new Object();
            loadAccounts(obj);

            async function loadAccounts(obj) {
                var accts = await obj.getAccounts($scope);
                var hospital = document.getElementById('hospital');
                var instances = M.Autocomplete.init(hospital, { data: $scope.accounts  , noItem : 'no data found' });
            }
        }
        else{
            $scope.accounts = {};
            var hospital = document.getElementById('hospital');
            var instances = M.Autocomplete.init(hospital, { data: $scope.accounts ,   noOptionsText :  $scope.options});
        }
    }
    $scope.surgeonList = {};
    //Surgeon data from sqlite
    $scope.typeSurgeon = function() {
            const obj = new GmSurgeonDao("Cases");
            loadSurgeon(obj);

            async function loadSurgeon(obj) {
                await obj.getSurgeons($scope);
            }
    }
    $('.chips-autocomplete').chips({
        autocompleteOptions: {
          function :$scope.typeSurgeon(),
          data: $scope.surgeonList,
          limit: Infinity,
          minLength: 3,
          delay: 1000,
          appendTo: "#someElem",
          noItem : 'no data found'
        }
      });
    
      //set case covered name in search box
      $scope.typeSalesRep=function(){
        if($scope.newCase.caseCoveredBy != null && $scope.newCase.caseCoveredBy.length > TypeLength){
            $scope.salesRep = {};
            $scope.salesRepSearchKey =  $scope.newCase.caseCoveredBy;
            $scope.salesRepSearch();
                var caseCoveredBy = document.getElementById('caseCoveredBy');
                var instances = M.Autocomplete.init(caseCoveredBy, { data: $scope.salesRep  , noItem : 'no data found' });
        }
        else{
            $scope.salesRep = {};
            var caseCoveredBy = document.getElementById('caseCoveredBy');
            var instances = M.Autocomplete.init(caseCoveredBy, { data: $scope.salesRep ,   noOptionsText :  $scope.options});
        }
    }
    //set shareWith name in search box
    $scope.typeShareWith=function(){
        if($scope.newCase.shareWith != null && $scope.newCase.shareWith.length > TypeLength){
            $scope.salesRep = {};
            $scope.salesRepSearchKey = $scope.newCase.shareWith;
            $scope.salesRepSearch();
                var shareWith = document.getElementById('shareWith');
                var instances = M.Autocomplete.init(shareWith, { data: $scope.salesRep  , noItem : 'no data found' });
        }
        else{
            $scope.salesRep = {};
                var shareWith = document.getElementById('shareWith');
                var instances = M.Autocomplete.init(shareWith, { data: $scope.salesRep  , noItem : 'no data found' });
        }
    }
}]);