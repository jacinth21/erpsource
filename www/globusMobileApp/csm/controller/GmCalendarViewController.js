caseManagement.controller("GmCalendarViewController", function($scope , $exceptionHandler){
    $scope.cmpId = localStorage.getItem("cmpid");
    $scope.cmpdfmt = localStorage.getItem("cmpdfmt");
    var gmUserData = JSON.parse(localStorage.getItem("userData"));
    $scope.userId = gmUserData.userid;
    var gmDeptId = gmUserData.deptid;
    $scope.partyId = gmUserData.partyid;
    $scope.init=function($scope){
        const gmSalesRepDao= new GmSalesRepDao();
        gmSalesRepDao.getUserSalesRep(function(){
             $scope.loadCalendar($scope);
        },$scope);
    }
    
    //This method is default loading calendar in get the eventList
    $scope.loadCalendar =async function($scope){
        var date = new Date();
		var evenDetailList =[];
		var calendarList=[];
        // Date Process (before six month date and after six month date)
        $scope.activeDate = new Date(date.getFullYear(), date.getMonth(), 1);  
        $scope.fromDate = new Date(date.getFullYear(),date.getMonth() - 6,0);
		$scope.toDate = new Date(date.getFullYear(), date.getMonth() + 6, 0);
        const caseController = new GmCaseDao('FT7600_CASE');
        $scope.cases = new Array;
        $scope.eventList=await caseController.loadCalendarCases($scope);
        $scope.eventList.forEach(doc => {
            var caseNew = doc.data();
            evenDetailList.push(caseNew);
            $scope.$apply();
        }) 
        for(var i=0; i<evenDetailList.length; i++){
            hospital = evenDetailList[i].hospital ;
            caseTitle = evenDetailList[i].c7600_title ;
            start = evenDetailList[i].c7600_startdt ;
            end = evenDetailList[i].c7600_enddt;
            // convert date format timestamp to  date format
            var startDateFormat =await start.toDate();
			var endDateFormat =await end.toDate();
            startDate = startDateFormat.toLocaleString();
            endDate = endDateFormat.toLocaleString();
			
            calendarList.push({
                "id" : i,
                "name"  : caseTitle,
                "startDate" :startDate,
                "endDate" : endDate,
            });
            $scope.$apply();
        };
        //evendar template get the script id values
        $("#evendar-ex-4").evendar({
            calendarCount: 1,
            oneCalendarWidth: 300,
            autoCloseOnSelect: true,
            showEventDetails: false,
            format: "MM/DD/YYYY HH:mm",
            events: calendarList,
            inline: true
        });
    }
 });