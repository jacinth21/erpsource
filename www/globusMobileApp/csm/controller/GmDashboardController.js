// always pass $scope variable to the service layer
caseManagement.controller("GmDashboardController", ['$scope',function($scope,$exceptionHandler){
    const gmCaseDao = new GmCaseDao("FT7600_CASE");   
    //getting today and upcoming case count from gmCaseDao
    $scope.setCSMDashboard =  function($scope){
        gmCaseDao.getTodaysCaseCount($scope);
        gmCaseDao.getUpcomingCaseCount($scope);
    }  
}]);