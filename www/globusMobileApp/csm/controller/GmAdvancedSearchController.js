caseManagement.controller("GmAdvancedSearchController", ['$scope', '$rootScope', function ($scope, $rootScope) {
    var ipLen = 3;
    $scope.advSearch = {};
    //search by Surgeons
    $scope.searchSurgeons = function ($scope) {
        if (($scope.advSearch.surgeon != null) && ($scope.advSearch.surgeon.length >= ipLen)) {
            const gmSurgeonDao = new GmSurgeonDao();
            $scope.surgns = new Array;
            $scope.autoCompData = {};
            gmSurgeonDao.searchSurgeonInfoCallback($scope, $scope.advSearch.surgeon, function () {
                var surgeons = M.Autocomplete.getInstance(document.getElementById('surgeon'));
                surgeons.updateData($scope.autoCompData);
                surgeons.open();
            });
        }
    }
//search by Accounts
    $scope.searchAccounts = function ($scope) {
        if (($scope.advSearch.account != null) && ($scope.advSearch.account.length >= ipLen)) {
            const gmAccountsDao = new GmAccountDao();
            $scope.accounts = new Array;
            gmAccountsDao.searchAccountInfo($scope, $scope.advSearch.account, function () {
                var accounts = M.Autocomplete.getInstance(document.getElementById('account'));
                accounts.updateData($scope.accounts);
                accounts.open();
            });
        }
    }
//search by Case Title
    $scope.searchCaseByTitle = function () {
        if (($scope.advSearch.caseTitle != null) && ($scope.advSearch.caseTitle.length >= ipLen)) {
            console.log("getting case titles!!");
            const gmDaoCase = new GmCaseDao('FT7600_CASE');
            $scope.caseTitles = new Array;
            $scope.autoCompData = {};
            gmDaoCase.loadCaseByTitle($scope, $scope.advSearch.caseTitle, function () {
                var cases = M.Autocomplete.getInstance(document.getElementById('caseTitle'));
                cases.updateData($scope.autoCompData);
                cases.open();
            });
        }
    }
//search by Case SalesRep
    $scope.searchSalesRep = function ($scope) {
        if (($scope.advSearch.salesRep != null) && ($scope.advSearch.salesRep.length >= ipLen)) {
            console.log("getting Sales Rep!!");
            const gmDaoSalesRep = new GmSalesRepDao();
            $scope.salesReps = new Array;
            $scope.autoCompData = {};
            gmDaoSalesRep.getSalesRepCallback($scope, $scope.advSearch.salesRep, function () {
                var salesRep = M.Autocomplete.getInstance(document.getElementById('salesRep'));
                salesRep.updateData($scope.autoCompData);
                salesRep.open();
            });
        }
    }
//After search Report
    $scope.filterReport = function ($scope) {
        $scope.filterReportMain($scope, function (filter) {
            localStorage.setItem("CSM_ADV_FILTER", JSON.stringify(filter));
            $rootScope.$emit("CallAdvSearchReportInit", {});
        });
    }
//setting filter and calling other controller method
    $scope.filterReportMain = function ($scope, callback) {
        console.log("setting filter and calling other controller method");
        var filter = {
            surgeon: "",
            account: "",
            caseTitle: "",
            startDate: "",
            endDate: "",
            salesRep: "",
            procedure: ""
        };
        if ($scope.advSearch.surgeonPair) {
            filter.surgeon = $scope.advSearch.surgeonPair;
        }
        if ($scope.advSearch.accountPair) {
            filter.account = $scope.advSearch.accountPair;
        }
        if ($scope.advSearch.casePair) {
            filter.caseTitle = $scope.advSearch.casePair;
        }
        if ($scope.advSearch.salesRepPair) {
            filter.salesRep = $scope.advSearch.salesRepPair;
        }
        if ($scope.advSearch.startDate) {
            filter.startDate = $scope.advSearch.startDate;
        }
        if ($scope.advSearch.endDate) {
            filter.endDate = $scope.advSearch.endDate;
        }
        callback(filter);
    }

//Clear the data in search popup
    $scope.clearSearch = function ($scope, field) {
        switch (field) {
            case "surgeon":
                $scope.advSearch.surgeon = "";
                $scope.advSearch.surgeonId = null;
                $scope.advSearch.surgeonPair = null;
                break;
            case "account":
                $scope.advSearch.account = "";
                $scope.advSearch.accountId = null;
                $scope.advSearch.accountPair = null;
                break;
            case "caseTitle":
                $scope.advSearch.caseTitle = "";
                $scope.advSearch.caseId = null;
                $scope.advSearch.casePair = null;
                break;
            case "salesRep":
                $scope.advSearch.salesRep = "";
                $scope.advSearch.salesRepId = null;
                $scope.advSearch.salesRepPair = null;
                break;
            case "startDate":
                $scope.advSearch.startDate = "";
            case "endDate":
                $scope.advSearch.endDate = "";
        }
    }

    $scope.initStartDate = function ($scope) {
        console.log("init Start Date Calendar");
        var dateFormat = localStorage.getItem('cmpdfmt');
        $scope.advSearch.startDate = "";
        // init calendar without container, use null instead of container
        switch (dateFormat) {
            case "MM/dd/yyyy":
                var format = "%m/%d/%y";
                break;
            case "dd/MM/yyyy":
                var format = "%d/%m/%y";
                break;
            case "yyyy/MM/dd":
                var format = "%y/%m/%d";
                break;
        }
        const calendar = new dhx.Calendar(null, { dateFormat: format, css: "dhx_widget--bordered" });
        
        // init popup and attach calendar
        const popup1 = new dhx.Popup();
        popup1.attach(calendar);
        const startDate = document.getElementById("startDate");
        popup1.show(startDate);

        calendar.events.on("change", function () {
            startDate.value = calendar.getValue();
            $scope.advSearch.startDate = calendar.getValue();
            $scope.$apply();
            popup1.hide();
        });

    }

    $scope.initEndDate = function ($scope) {
        console.log("init End Date Calendar");
        var dateFormat = localStorage.getItem('cmpdfmt');
        $scope.advSearch.endDate = "";

        // init calendar without container, use null instead of container
        switch (dateFormat) {
            case "MM/dd/yyyy":
                var format = "%m/%d/%y";
                break;
            case "dd/MM/yyyy":
                var format = "%d/%m/%y";
                break;
            case "yyyy/MM/dd":
                var format = "%y/%m/%d";
                break;
        }
        const calendar = new dhx.Calendar(null, { dateFormat: format, css: "dhx_widget--bordered" });
        // init popup and attach calendar
        const popup2 = new dhx.Popup();
        popup2.attach(calendar);
        const endDate = document.getElementById("endDate");
        popup2.show(endDate);

        calendar.events.on("change", function () {
            endDate.value = calendar.getValue();
            $scope.advSearch.endDate = calendar.getValue();
            $scope.$apply();
            popup2.hide();
        });

    }

}]);