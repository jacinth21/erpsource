caseManagement.controller("GmQuickSearchController", ['$scope', function ($scope) {
    var ipLen = ipLength;

    $scope.quickSearchInit = function ($scope) {
        console.log("Quick Search");

        $scope.urlCaseTitle = "#!GmAdvanceSearchReport/searchResults";
        $scope.urlSurgeon = "#!GmAdvanceSearchReport/searchResults";
        $scope.urlProcedure = "#!GmAdvanceSearchReport/searchResults";

        // hiding tabs
        var el = document.querySelectorAll(".tabs");
        var instance = M.Tabs.init(el, {});
        document.getElementById("searchTabs").style.display = 'none';

        if (localStorage.recentSearchArr == null || localStorage.recentSearchArr == '') {
            localStorage.recentSearchArr = JSON.stringify(new Array);
        }

        if (localStorage.CSM_ADV_FILTER == null || localStorage.CSM_ADV_FILTER == '') {
            console.log("resetting filter...");
            var filter = {
                surgeon: "",
                account: "",
                caseTitle: "",
                patientId: "",
                startDate: "",
                endDate: "",
                salesRep: "",
                procedure: ""
            }
            localStorage.CSM_ADV_FILTER = JSON.stringify(filter);
        }

        //setting recent search
        $scope.recentSearchArr = JSON.parse(localStorage.recentSearchArr);

        //getting party-id
        var gmUserData = JSON.parse(localStorage.getItem("userData"));
        // var gmDeptId = gmUserData.deptid;
        $scope.partyId = gmUserData.partyid;

        // getting Sales Rep id's
        const gmDaoSalesRep = new GmSalesRepDao();
        gmDaoSalesRep.getUserSalesRep(function () {
            localStorage.arrayReps = JSON.stringify($scope.arrayReps);
        }, $scope);
    }

    $scope.clearRecentSearches = function ($scope) {
        console.log("clearing Recent Searches...");
        $scope.recentSearchArr = [];
        localStorage.recentSearchArr = JSON.stringify(new Array);
    }

    $scope.delRes = function ($scope, index, textPair) {
        $scope.recentSearchArr.splice(index, 1);
        localStorage.recentSearchArr = JSON.stringify($scope.recentSearchArr);
    }

    $scope.getTabs = function ($scope) {

        // array init
        $scope.caseTitles = [];
        $scope.surgns = [];
        $scope.procedures = [];

        if (($scope.search.searchText != null) && ($scope.search.searchText.length >= ipLen)) {
            console.log("getting Tabs!!!");

            // getting case titles
            const gmDaoCase = new GmCaseDao("FT7600_CASE");
            gmDaoCase.loadCaseByTitle($scope, $scope.search.searchText, function () {
                console.log("displaying Case Titles");
                console.log($scope.caseTitles);
                if ($scope.caseTitles[0][0] == "No data found.") {
                    $scope.urlCaseTitle = "";
                    $scope.$apply();
                }
            });

            // getting Surgeon names
            const gmSurgeonDao = new GmSurgeonDao();
            gmSurgeonDao.searchSurgeonInfoCallback($scope, $scope.search.searchText, function () {
                console.log("displaying surgeon info...");
                console.log($scope.surgns);
                if ($scope.surgns[0][0] == "No data found.") {
                    $scope.urlSurgeon = "";
                    $scope.$apply();
                }
            });

            // getting Procedures
            const gmCodeLookupDao = new GmSqliteDao();
            gmCodeLookupDao.searchProcedure($scope, function () {
                console.log("displaying Procedure info...");
                if ($scope.procedures.length == 0) {
                    $scope.procedures.push(["No data found.", ""]);
                    $scope.urlProcedure = "";
                    $scope.$apply();
                }
                console.log($scope.procedures);
            });

            // unhiding the tabs
            document.getElementById("recentSearches").style.display = 'none';
            document.getElementById("searchTabs").style.display = 'grid';
        }
        else {
            $scope.recentSearchArr = JSON.parse(localStorage.recentSearchArr);
            document.getElementById("searchTabs").style.display = 'none';
            document.getElementById("recentSearches").style.display = 'initial';
        }
    }

    $scope.saveRecentSearch = function (searchRes, type) {
        var tempRecentSearchArr = JSON.parse(localStorage.recentSearchArr);
        if (searchRes[0] != "No data found.") {
            if ((tempRecentSearchArr.length == 0) ||
                (tempRecentSearchArr.length == 1 && searchRes[0] != tempRecentSearchArr[0][0][0]) ||
                (tempRecentSearchArr.length == 2 && searchRes[0] != tempRecentSearchArr[0][0][0] && searchRes[0] != tempRecentSearchArr[1][0][0]) ||
                (tempRecentSearchArr.length == 3 && searchRes[0] != tempRecentSearchArr[0][0][0] && searchRes[0] != tempRecentSearchArr[1][0][0] && searchRes[0] != tempRecentSearchArr[2][0][0])) {
                console.log("adding new data");
                tempRecentSearchArr.splice(0, 0, [searchRes, type]);
                if (tempRecentSearchArr.length > 3) {
                    tempRecentSearchArr.pop();
                }
            }
            localStorage.recentSearchArr = JSON.stringify(tempRecentSearchArr);
            $scope.setGlobalFilter(searchRes, type);
        }
    }

    $scope.clearSearch = function ($scope) {
        if ($scope.search) {
            $scope.search.searchText = "";
            document.getElementById("searchTabs").style.display = 'none';
            document.getElementById("recentSearches").style.display = 'initial';
            console.log($scope.surgeons);
        }
    }

    $scope.resetRecentSearchOrder = function (searchRes, type, index) {
        console.log(index);
        var tempRecentSearchArr = JSON.parse(localStorage.getItem("recentSearchArr"));
        tempRecentSearchArr.splice(index, 1);
        tempRecentSearchArr.splice(0, 0, [searchRes, type]);
        localStorage.setItem("recentSearchArr", JSON.stringify(tempRecentSearchArr));
        $scope.setGlobalFilter(searchRes, type);
    }


    $scope.setGlobalFilter = function (searchRes, type) {
        var filter = JSON.parse(localStorage.CSM_ADV_FILTER);
        if (type == "local_hospital") {
            filter.caseTitle = searchRes;
        }
        else if (type == "assignment_ind") {
            filter.surgeon = searchRes;
        }
        else if (type == "assignment") {
            filter.procedure = searchRes;
        }
        localStorage.CSM_ADV_FILTER = JSON.stringify(filter);
        console.log("calling advanced search report screen");
    }

}]);