// always pass $scope variable to the service layer
caseManagement.controller("GmShareCaseController", function($scope,$routeParams){
    var gmUserData = JSON.parse(localStorage.getItem("userData"));
    $scope.userId = gmUserData.userid;
    $scope.shareWithIds=[];
    $scope.shareWithNames=[];
    $scope.sharedRepIds=[];
    $scope.sharedRepNames=[];
    $scope.editCaseId=$routeParams.caseId;
    $scope.searchRepList={};
    $scope.shareWithDetails={};
    $scope.salesRepDetails={};
    const gmCaseDao = new GmCaseDao('FT7600_CASE');
  //Fetch casedetails from firestore
    $scope.init = function() {
      $scope.newCase=new Object();
      $scope.cases = new Array;
      fetchCase(gmCaseDao);
      async function fetchCase(gmCaseDao) {
          await gmCaseDao.editCase($scope);
      }
    }
   
    //SalesRep data from sqlite
    $scope.shareWith = function() {    
      const gmSurgeonDao= new GmSalesRepDao("Cases");
      loadSurgeon(gmSurgeonDao);
      async function loadSurgeon(gmSurgeonDao) {
          await gmSurgeonDao.searchRep($scope);
      }
    }
    //share with input field autocomplete function
    $('.chips-autocomplete').chips({
      placeholder: 'Enter ShareWith..',
      autocompleteOptions: {
        function :$scope.shareWith(),
        data: $scope.salesRepDetails,
        limit: Infinity,
        minLength: 3
      }
    });
    
    //Get removed  share rep name and id
    $scope.removeSharedRep= function(shareNm,shareId){   
      $scope.shareWithNames.push(shareNm);
      $scope.shareWithIds.push(shareId);
    }

    //save the shared rep details to firestore
    $scope.saveSharedWith=  function(){
      var shareWithList=(M.Chips.getInstance($('.schips')).chipsData);
      for(var i=0;i<shareWithList.length;i++){
        var shareRepNm=JSON.stringify(shareWithList[i]);
        $scope.sharedRepNames.push(JSON.parse(shareRepNm).tag);
        $scope.sharedRepIds.push($scope.shareWithDetails[JSON.parse(shareRepNm).tag]);      
      }
      gmCaseDao.saveSharedWith($scope);
    }
});