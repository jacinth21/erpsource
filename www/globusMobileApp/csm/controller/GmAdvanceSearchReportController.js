caseManagement.controller("GmAdvanceSearchReportController", ['$scope', '$rootScope', '$routeParams', function ($scope, $rootScope, $routeParams) {

    $(document).ready(function () {
        $('.modal').modal();
    });

    var gmUserData = JSON.parse(localStorage.getItem("userData"));
    $scope.userId = gmUserData.userid;
    $scope.today=new Date();
	$scope.summaryAction=$routeParams.action;
    $rootScope.$on("CallAdvSearchReportInit", function () {
        $scope.summaryAction = "searchResults";
        $scope.loadCaseReport($scope);
    });
    const gmDaoCase = new GmCaseDao('FT7600_CASE');
    $scope.loadCaseReport = async function ($scope) {
        if ($scope.summaryAction == "search") {
            console.log("searching");
            var ins = document.querySelectorAll('.collapsible');
            M.AutoInit();
            M.Collapsible.getInstance(ins[0]).open(0);
            document.getElementById("advSearch").style.display = 'grid';
        }
        else if ($scope.summaryAction == "upcomingCases" || $scope.summaryAction == "todaysCases" || $scope.summaryAction == "searchResults") {
            document.getElementById("advSearch").style.display = 'none';
        }

        $scope.cases = new Array;

        var filter = JSON.parse(localStorage.getItem("CSM_ADV_FILTER"));

        if (localStorage.CSM_ADV_FILTER == undefined || localStorage.CSM_ADV_FILTER == '') {
            console.log("filter null!!! Displaying all the case details");
            gmDaoCase.loadReport($scope, function ($scope) {
                if ($scope.cases.length == 0) {
                    document.getElementById("casesList").style.display = 'none';
                    document.getElementById("noEventsDiv").style.display = 'grid';
                }
                else {
                    document.getElementById("casesList").style.display = 'grid';
                    document.getElementById("noEventsDiv").style.display = 'none';
                }
            });
            $scope.resetCsmAdvFilter($scope);
        }
        else if (filter.caseTitle != ""
            || filter.surgeon != ""
            || filter.procedure != ""
            || filter.account != ""
            || filter.startDate != ""
            || filter.endDate != ""
            || filter.salesRep != "") {

            $scope.advSearch = {};

            console.log("$scope.advSearch");
            console.log($scope.advSearch);

            console.log("Filter data");
            console.log(filter);

            // setting filter values to scope variables

            // possible values from quick search
            if (filter.caseTitle) {
                console.log("setting case title to text box");
                $scope.advSearch.caseTitle = filter.caseTitle[0];
                $scope.advSearch.caseId = filter.caseTitle[1];
                $scope.advSearch.casePair = [filter.caseTitle[0], filter.caseTitle[1]]
            }
            if (filter.surgeon) {
                $scope.advSearch.surgeon = filter.surgeon[0];
                $scope.advSearch.surgeonId = filter.surgeon[1];
                $scope.advSearch.surgeonPair = [filter.surgeon[0], filter.surgeon[1]];
            }
            if (filter.procedure) {
                $scope.advSearch.procedure = filter.procedure[0];
            }

            // values that can be set only from Advanced Search'
            if (filter.account) {
                console.log("setting account name to text box");
                $scope.advSearch.account = filter.account[0];
                $scope.advSearch.accountId = filter.account[1];
                $scope.advSearch.accountPair = [filter.account[0], filter.account[1]];
            }
            if (filter.startDate) {
                $scope.advSearch.startDate = filter.startDate;
            }
            if (filter.endDate) {
                $scope.advSearch.endDate = filter.endDate;
            }
            if (filter.salesRep) {
                $scope.advSearch.salesRep = filter.salesRep[0];
                $scope.advSearch.salesRepId = filter.salesRep[1];
                $scope.advSearch.salesRepPair = [filter.salesRep[0], filter.salesRep[1]];
            }

            await gmDaoCase.loadAdvancedSearchReport($scope, function ($scope) {
                console.log($scope.cases);
                if ($scope.cases.length == 0) {
                    console.log("No Events.");
                    document.getElementById("casesList").style.display = 'none';
                    document.getElementById("noEventsDiv").style.display = 'initial';
                }
                else {
                    document.getElementById("casesList").style.display = 'initial';
                    document.getElementById("noEventsDiv").style.display = 'none';
                }
            });
        }
        else {
            console.log("filter not set!!! Displaying all the case details");
            gmDaoCase.loadReport($scope, function ($scope) {
                if ($scope.cases.length == 0) {
                    console.log("No Events.");
                    document.getElementById("casesList").style.display = 'none';
                    document.getElementById("noEventsDiv").style.display = 'initial';
                }
                else {
                    document.getElementById("casesList").style.display = 'initial';
                    document.getElementById("noEventsDiv").style.display = 'none';
                }
            });
        }

        $scope.resetCsmAdvFilter();
    }

    $scope.resetFilters = function ($scope) {
        console.log("resetting filters...");
        var currDate = new Date();
        $scope.advSearch.surgeon = null;
        $scope.advSearch.surgeonId = null;
        $scope.advSearch.surgeonPair = null;
        $scope.advSearch.account = null;
        $scope.advSearch.accountId = null;
        $scope.advSearch.accountPair = null;
        $scope.advSearch.caseTitle = null;
        $scope.advSearch.caseId = null;
        $scope.advSearch.casePair = null;
        $scope.advSearch.salesRep = null;
        $scope.advSearch.salesRepId = null;
        $scope.advSearch.salesRepPair = null;
        $scope.advSearch.startDate = null;
        $scope.advSearch.endDate = null;
        $scope.resetCsmAdvFilter();
    }

    $scope.resetCsmAdvFilter = function () {
        var filter = {
            surgeon: "",
            account: "",
            caseTitle: "",
            startDate: "",
            endDate: "",
            salesRep: "",
            procedure: ""
        }
        localStorage.setItem("CSM_ADV_FILTER", JSON.stringify(filter));
    }

    $scope.expandAll = function () {
        document.getElementById("advSearch").style.display = 'grid';
        var ins = document.querySelectorAll('.collapsible');
        M.AutoInit();
        M.Collapsible.getInstance(ins[0]).open(0);
        var elems = document.querySelectorAll('.datepicker');
        var instances = M.Datepicker.init(elems, {
            setDefaultDate: true,
            defaultDate: new Date()
        });
    }

    $scope.collapseAll = function () {
        var ins = document.querySelectorAll('.collapsible');
        M.AutoInit();
        M.Collapsible.getInstance(ins[0]).close(0);
        document.getElementById("advSearch").style.display = 'none';
    }

    //open modal for confirmation message
    $scope.cancelCase =  function(caseId){     
        $scope.caseId=caseId;
        $('#conformModal').modal('open'); 
    }
    //cancel a case
    $scope.cancelCaseSubmit = async function(caseId){     
      const gmCaseDao = new GmCaseDao('FT7600_CASE');
      $scope.caseId=caseId;
      gmCaseDao.cancelCase($scope);
   }
   $scope.IsVisible = false;
   $scope.loadFiles= function(caseNew, index){
       console.log("Loading files....");
       console.log(caseNew.getFileUpload());
       var fileEle = document.getElementById("fileList"+index);
       if(fileEle.classList.contains("hide")){
           fileEle.classList.remove("hide");
       }
       else {
            fileEle.classList.add("hide");
       }
   }
}]);