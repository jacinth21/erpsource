define([
    'jquery', 'underscore', 'backbone', 'handlebars', 'global'
],

    function ($, _, Backbone, Handlebars, Global) {

        'use strict';
        var CsmMainView = Backbone.View.extend({

            el: "#div-main",

            name: "CSM Main View",

            initialize: function () {
                $("body").removeClass();
                $("#btn-home").show();
                $("#btn-back").show();
                $("#div-app-title").html("Case Management");
                this.render();
            },

            render: function () {
                console.log("CSM Template");
                // this.$el.html('<object data="globusMobileApp/csm/index.html" width="1024" height="700" />');
                this.$el.html('<object data="globusMobileApp/csm/index.html" style="width:100%;height:93vh;" />');
                return this;
            }

        });

        return CsmMainView;
    });
