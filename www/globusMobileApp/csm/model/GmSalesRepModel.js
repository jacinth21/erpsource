class GmSalesRepModel{
    constructor(repid, repnm){
        this.repid = repid;
        this.repnm = repnm;
    }

    toString() {  return "repid=" + this.repid +"repnm="+ this.repnm ; }

    setRepId = function (repid) { this.repid = repid ; }
    getRepId = function () { return this.repid; }
    setRepNm = function (repnm) { this.repnm = repnm ; }
    getRepNm = function () { return this.repnm; }

}
// Firestore data converter
var salesRepConverter = { 
    toFirestore: function (salesRep) {
        return {
            repid: salesRep.repid,
            repnm: salesRep.repnm
        };
    },
    fromFirestore: function (snapshot, options) {
        const salesRep = snapshot.data(options);
        return new GmSalesRepModel(salesRep.repid, salesRep.repnm);
    }
};

