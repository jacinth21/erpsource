class GmSurgeonModel {
    constructor(surgnId, surgnNm, surgnNpi) {
        this.surgnId = surgnId;
        this.surgnNm = surgnNm;
        this.surgnNpi = surgnNpi;
    }

    toString() {
        return "Surgeon ID=" + this.surgnId + ", Surgeon Name=" + this.surgnNm + ", Surgeon NPI=" + this.surgnNpi ;
    }

    setSurgnId = function (surgnId) { this.surgnId = surgnId }
    getSurgnId = function () { return this.surgnId; }
    setSurgnNm = function (surgnNm) { this.surgnNm = surgnNm }
    getSurgnNm = function () { return this.surgnNm; }
    setSurgnNpi = function (surgnNpi) { this.surgnNpi = surgnNpi }
    getSurgnNpi = function () { return this.surgnNpi; }
}

// Firestore data converter
var surgeonConverter = {
    toFirestore: function (surgn) {
        return {
            sid: surgn.surgnId,
            nm: surgn.surgnNm,
            npi: surgn.surgnNpi
        };
    },
    fromFirestore: function (snapshot, options) {
        const surgn = snapshot.data(options);
        return new SurgeonModel(surgn.surgnId, surgn.surgnNm, surgn.surgnNpi);
    }
};

