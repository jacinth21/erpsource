class GmCodeLookupModel {
    constructor(id, nm, altnm,seq) {
        this.id = id;
        this.nm = nm;
        this.altnm = altnm;
        this.seq = seq;
    }
    toString() {
        return "procedureid=" + this.id +"name=" +this.nm +"altname=" +this.altnm +"sequence=" +this.seq;
    }

    setId = function (id) { this.id = id }
    getId = function () { return this.id; }
    setNm = function (nm) { this.nm = nm }
    getNm = function () { return this.nm; }
    setAltName = function (altnm) { this.altnm = altnm }
    getAltName = function () { return this.altnm; }
    setSeq = function (seq) { this.seq = seq }
    getSeq = function () { return this.seq; }
}

// Firestore data converter
var codeLookupConverter = {
    fromFirestore: function (snapshot, options) {
        const codeLookup = snapshot.data(options);
        return new GmCodeLookupModel(codeLookup.C901_CODE_ID,codeLookup.C901_CODE_NM,codeLookup.C902_CODE_NM_ALT,codeLookup.C901_CODE_SEQ_NO);
    }
};
