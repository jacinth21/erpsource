class Files {
    constructor(caseId,fileName,type,fileUrl,fileSize) {
        this.caseId = caseId;
        this.fileName = fileName;
        this.type = type;
        this.fileUrl = fileUrl;
        this.fileSize = fileSize;
        
    }

    toString() {
        return "caseId=" + this.caseId + ", fileName=" + this.fileName + ",type=" + this.type + ", fileUrl=" + this.fileUrl +",fileSize=" + this.fileSize;
    }

    setCaseId = function (caseId) { this.caseId = caseId }
    getCaseId = function () { return this.caseId; }
    setFileName = function (fileName) { this.fileName = fileName }
    getFileName = function () { return this.fileName; }
    setType = function (type) { this.type = type }
    getType = function () { return this.type; }
    setFileUrl= function (fileUrl) { this.fileUrl = fileUrl }
    getFileUrl = function () { return this.fileUrl; }
    setFileSize= function (fileSize) { this.fileSize = fileSize }
    getFileSize = function () { return this.fileSize; }
}

// Firestore data converter
var filesConverter = {
    toFirestore: function (caseNew) {
        return {
            fid: caseNew.caseId,
            nm: caseNew.fileName,
            type:caseNew.type,
            path: caseNew.fileUrl,
            size:caseNew.fileSize
        };
    },
    fromFirestore: function (snapshot, options) {
        const caseNew = snapshot.data(options);
        return new Files(caseNew.caseId, caseNew.fileName,caseNew.type,caseNew.fileUrl,caseNew.size);
    }
};
