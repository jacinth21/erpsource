// file name and class name should be same for class files
class GmCaseModel {
    constructor(caseId, accountId, accountName ,accRepId ,accRepName , surgeons = [], surgeonsId = [],
        surgeonsNpi = [], capitalEqip, procedure = [],procedureId =[], level ,caseCoveredById ,caseCoveredBy ,
        start,end, timeZone, shared = [] , sharedRepId = [] ,notes,
        caseTitle, companyId ,updateDate ,voidFl ,userId,eventId, statusId,fileUpload = [],asscId,asscUserIds=[]) { 
        this.caseId = caseId;
        this.accountId = accountId;
        this.accountName = accountName;
        this.accRepId = accRepId;
        this.accRepName =accRepName;
        this.surgeons = surgeons;
        this.surgeonsId = surgeonsId;
        this.surgeonsNpi = surgeonsNpi;
        this.capitalEqip = capitalEqip;
        this.procedure = procedure;
        this.level = level;
        this.caseCoveredById = caseCoveredById;
        this.caseCoveredBy = caseCoveredBy;
        this.start = start;
        this.end = end;
        this.timeZone = timeZone;
        this.shared = shared;
        this.sharedRepId = sharedRepId;
        this.notes = notes;
        this.caseTitle = caseTitle;
        this.companyId = companyId;
        this.updateDate = updateDate;
        this.voidFl = voidFl;
        this.userId = userId;
        this.eventId = eventId;
        this.statusId = statusId;
        this.fileUpload = fileUpload;
        this.asscId=asscId;
        this.asscUserIds=asscUserIds;
        this.procedureId = procedureId;
    }

    // toString() {
    //     return "hospital=" + this.hospital + ", surgeon=" + this.surgeon + ", procedure=" + this.procedure + ", patientId="
    //         + this.patientId + ", start=" + this.start + ", end=" + this.end
    //         + ", timeZone=" + this.timeZone + ", notes=" + this.notes + ", caseTitle=" + this.caseTitle +
    //         ", uploads=" + this.uploads;
    // }
    
    setCaseId = function (caseId) { this.caseId = caseId }
    getCaseId = function () { return this.caseId; }
    setAccountId = function (accountId) { this.accountId = accountId; }
    getAccountId = function () { return this.accountId; }
    setAccountName = function (accountName) { this.accountName = accountName; }
    getAccountName = function () { return this.accountName; }
    setAccRepId = function (accRepId) { this.accRepId = accRepId; }
    getAccRepId = function () { return this.accRepId; }
    setAccRepName = function (accRepName) { this.accRepName = accRepName; }
    getAccRepName = function () { return this.accRepName; }
    setSurgeons = function (surgeons) { this.surgeons = surgeons; }
    getSurgeons = function () { return this.surgeons; }
    setSurgeonsId = function (surgeonsNpi) { this.surgeonsNpi = surgeonsNpi; }
    getSurgeonsId = function () { return this.surgeonsNpi; }
    setSurgeonsNpi = function (surgeonsNpi) { this.surgeonsNpi = surgeonsNpi; }
    getSurgeonsNpi = function () { return this.surgeonsNpi; }
    setCapitalEqip = function (capitalEqip) { this.capitalEqip = capitalEqip; }
    getCapitalEqip = function () { return this.capitalEqip; }
    setProcedure = function (procedure) { this.procedure = procedure; }
    getProcedure = function () { return this.procedure; }
    setProcedureId= function (procedureId) { this.procedureId = procedureId; }
    getProcedureId = function () { return this.procedureId; }
    setLevel = function (level) { this.level = level; }
    getLevel = function () { return this.level; }
    setCaseCoveredById = function (caseCoveredById) { this.caseCoveredById = caseCoveredById; }
    getCaseCoveredById = function () { return this.caseCoveredById; }
    setCaseCoveredBy = function (caseCoveredBy) { this.caseCoveredBy = caseCoveredBy; }
    getCaseCoveredBy = function () { return this.caseCoveredBy; }
    setStart = function (start) { this.start = start; }
    getStart = function () { return this.start; }
    setEnd = function (end) { this.end = end; }
    getEnd = function () { return this.end; }
    setTimeZone = function (timeZone) { this.timeZone = timeZone; }
    getTimeZone = function () { return this.timeZone; }
    setShared = function (shared) { this.shared = shared; }
    getShared = function () { return this.shared; }
    setSharedRepId = function (sharedRepId) { this.sharedRepId = sharedRepId; }
    getSharedRepId = function () { return this.sharedRepId; }
    setNotes = function (notes) { this.notes = notes; }
    getNotes = function () { return this.notes; }
    setCaseTitle = function (caseTitle) { this.caseTitle = caseTitle; }
    getCaseTitle = function () { return this.caseTitle; }
    setCompanyId = function (companyId) { this.companyId = companyId; }
    getCompanyId = function () { return this.companyId; }
    setUpdateDate = function (updateDate) { this.updateDate = updateDate; }
    getUpdateDate = function () { return this.updateDate; }
    setVoidFl = function (voidFl) { this.voidFl = voidFl; }
    getVoidFl = function () { return this.voidFl; }
    setUserId = function (userId) { this.userId = userId; }
    getUserId = function () { return this.userId; }
    setEventId = function (eventId) { this.eventId = eventId; }
    getEventId = function () { return this.eventId; }
    setStatusId = function (statusId) { this.statusId = statusId; }
    getStatusId = function () { return this.statusId; }
    setFileUpload = function (fileUpload) { this.fileUpload = fileUpload; }
    getFileUpload = function () { return this.fileUpload; }
    setAsscId= function (asscId) { this.asscId = asscId; }
    getAsscId = function () { return this.asscId; }
    setAsscUserIds= function (asscUserIds) { this.asscUserIds = asscUserIds; }
    getAsscUserIds = function () { return this.asscUserIds; }
}

// Firestore data converter
var caseConverter = {
    toFirestore: function (caseNew) {
        return {
            c7600_caseid: caseNew.caseId,
            c704_acctid: caseNew.accountId,
            c704_acctnm: caseNew.accountName,
            c703_acctrepid: caseNew.accRepId,
            c703_acctrepnm: caseNew.accRepName,
            ca7600_surgns: caseNew.surgeons,
            ca6700_surgnids: caseNew.surgeonsId,
            ca6700_sugnsnpis: caseNew.surgeonsNpi,
            c7600_robot: caseNew.capitalEqip,
            ca901_procedures: caseNew.procedure,
            ca901_proceduresids: caseNew.procedureId,
            c7600_lvl: caseNew.level,
            c703_coveredbyid: caseNew.caseCoveredById,
            c703_coveredbynm: caseNew.caseCoveredBy,
            c7600_startdt: caseNew.start,
            c7600_enddt: caseNew.end,
            c7600_timezn: caseNew.timeZone,
            ca703_shared: caseNew.shared,
            ca703_sharedrepids: caseNew.sharedRepId,
            c7600_notes: caseNew.notes,
            c7600_title: caseNew.caseTitle,
            c9100_compid: caseNew.companyId,
            c6700_updatedt: caseNew.updateDate,
            c6700_voidfl: caseNew.voidFl,
            c6700_updatedby: caseNew.userId,
            c6700_eventid: caseNew.eventId,
            c6700_statusid: caseNew.statusId,
            c701_assc_d_id:caseNew.asscId,
            ca101_assc_userids:caseNew.asscUserIds
        };
    },
    fromFirestore: function (snapshot, options) {
        const caseNew = snapshot.data(options);
        return new GmCaseModel(caseNew.c7600_caseid, caseNew.c704_acctid, caseNew.c704_acctnm,
            caseNew.c703_acctrepid, caseNew.c703_acctrepnm,
            caseNew.ca7600_surgns, caseNew.ca6700_surgnids,
            caseNew.ca6700_sugnsnpis, caseNew.c7600_robot, caseNew.ca901_procedures,caseNew.ca901_proceduresids,
            caseNew.c7600_lvl ,caseNew.c703_coveredbyid,
            caseNew.c703_coveredbynm, caseNew.c7600_startdt, caseNew.c7600_enddt,
            caseNew.c7600_timezn, caseNew.ca703_shared,caseNew.ca703_sharedrepids, caseNew.c7600_notes,
            caseNew.c7600_title, caseNew.c9100_compid, caseNew.c6700_updatedt, caseNew.c6700_voidfl,
            caseNew.c6700_updatedby, caseNew.c6700_eventid,caseNew.c6700_statusid,caseNew.ca6700_uploads,
            caseNew.c701_assc_d_id,caseNew.ca101_assc_userids);
    }
};
