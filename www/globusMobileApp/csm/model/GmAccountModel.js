class GmAccountModel {
    constructor(acctid ,acctnm ,acctrepid) {
        this.acctid = acctid;
        this.acctnm = acctnm;
        this.acctrepid = acctrepid;
    }

    toString() {        return "acctid=" + this.acctid +", acctnm=" +this.acctnm +", acctrepid=" +this.acctrepid;    }

    setAcctId = function (acctid) { this.acctid = acctid ; }
    getAcctId = function () { return this.acctid; }
    setAcctNm = function (acctnm) { this.acctnm = acctnm ; }
    getAcctNm = function () { return this.acctnm; }
    setAcctRepId = function (acctrepid) { this.acctrepid = acctrepid ; }
    getAcctRepId = function () { return this.acctrepid; }

}