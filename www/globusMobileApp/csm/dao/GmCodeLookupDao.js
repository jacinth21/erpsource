// file name and class name should be same for class files
class GmCodeLookupDao {
    constructor(col) {
        this.collection = GmFirebase.getCollection(col);
    }
    loadCodeLookup = function($scope,callback) {
        this.collection.where("C901_CODE_GRP", "==", "TECHQ").withConverter(codeLookupConverter).get().then((querySnapshot) => {
        querySnapshot.forEach(doc => {
            var codeLookup = doc.data();
            var obj = new Object();
            obj.id = codeLookup.getId();
            obj.name = codeLookup.getNm();
            $scope.procedureListArray.push(obj);
        })
        $scope.$apply();
        callback($scope);
        });
    }
    loadTimeZone = function ($scope,callback) {
        this.collection.get().then((querySnapshot) => {
            querySnapshot.forEach(doc => {
                var timeZone = doc.data();
                var obj = new Object();
                obj.timezone = timeZone.C901_CODE_NM;
                $scope.timezoneListArray.push(obj);
            })
            $scope.$apply();
            callback($scope);
        });
    };
	
	searchProcedure = function ($scope, callback) {
        const db = GmSqlite.getConnection();
        db.transaction(async function (tx, rs) {
            var query = ""
            tx.executeSql(
                "select t901.c901_code_nm nm" +
                " from t901_code_lookup t901" +
                " where t901.c901_code_grp = 'TECHQ' " +
                " and lower(t901.c901_code_nm) like '" + $scope.search.searchText + "%'" +
                " AND T901.C901_ACTIVE_FL = 1" +
                " and c901_void_fl = ''" +
                " ORDER BY T901.C901_CODE_SEQ_NO;", [],
                function (tx, rs) {
                    var i = 0;
                    $scope.precedures = [];
                    for (i = 0; i < rs.rows.length; i++) {
                        $scope.procedures.push([rs.rows.item(i).nm, null]);
                        $scope.$apply();
                    }
                    callback();
                },
                function (tx, error) {
                    console.log(error);
                });
        });
    };
}