class GmSqliteDao {
    constructor() {}
    getAccounts = function ($scope) {
        const db = GmSqlite.getConnection();
        db.transaction(getAccounts, errorCB, successCB);

        function getAccounts(tx) {
            tx.executeSql("select c704_account_nm from t704_account", [],
                function (tx, rs) {
                    if (rs.rows.length > 0) {
                        for (var i = 0; i < rs.rows.length; i++) {
                            $scope.accounts[rs.rows.item(i).C704_ACCOUNT_NM] = null;
                            $scope.$apply();
                        }
                    }
                });
        }

        function errorCB(tx, err) {
            console.log(tx);
            console.log(err);
            showMsgView("009");
        }

        function successCB() {
            //showMsgView("008");
        }
    };

    searchProcedure = function ($scope, callback) {
        const db = GmSqlite.getConnection();
        db.transaction(async function (tx, rs) {
            var query = ""
            tx.executeSql(
                "select t901.c901_code_nm nm" +
                " from t901_code_lookup t901" +
                " where t901.c901_code_grp = 'TECHQ' " +
                " and lower(t901.c901_code_nm) like '" + $scope.search.searchText + "%'" +
                " AND T901.C901_ACTIVE_FL = 1" +
                " and c901_void_fl = ''" +
                " ORDER BY T901.C901_CODE_SEQ_NO;", [],
                function (tx, rs) {
                    var i = 0;
                    $scope.precedures = [];
                    for (i = 0; i < rs.rows.length; i++) {
                        $scope.procedures.push([rs.rows.item(i).nm, null]);
                        $scope.$apply();
                    }
                    callback();
                },
                function (tx, error) {
                    console.log(error);
                });
        });
    };
}