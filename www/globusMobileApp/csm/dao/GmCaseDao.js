// file name and class name should be same for class files
class GmCaseDao {
    constructor(col) {
        this.collection = GmFirebase.getCollection(col);
    }
    // getting Today cases from Firestore database and passing it to GmCtlDashboard
    getTodaysCaseCount = async function ($scope) {
        var currentDate = new Date();
        var arrayGmMdlCases = [];

        currentDate.setDate(currentDate.getDate() + 1);
        const lowerLimit = firebase.firestore.Timestamp.fromDate(new Date(new Date().setHours(0, 0, 0, 0)));
        const upperLimit = firebase.firestore.Timestamp.fromDate(new Date(currentDate.setHours(0, 0, 0, 0)));

        var dashboardQuery = this.accessFilterForCase($scope);
        //Todays cases
        arrayGmMdlCases = await dashboardQuery.where("c7600_startdt", ">=", lowerLimit).where("c7600_startdt", "<=", upperLimit).get();
        $scope.todaysCases = arrayGmMdlCases.size;
        if (!arrayGmMdlCases.size) {
            $scope.todaysCases = 0;
        }
        $scope.$apply();
    }
    //apply Access Filter for case
    accessFilterForCase = function ($scope) {
		var gmUserData = JSON.parse(localStorage.getItem("userData"));
		var dashBoardQuery = this.collection; 
        $scope.accessLvl = gmUserData.acid; 
        $scope.userId = gmUserData.userid; 
        $scope.distId = gmUserData.did;
        dashBoardQuery.where("c6700_voidfl", "==", false);
        if ($scope.accessLvl == 1 || $scope.accessLvl == 3 || $scope.accessLvl == 4 || $scope.accessLvl == 6 || $scope.accessLvl == 7) {
            dashBoardQuery = dashBoardQuery.where("ca101_assc_userids", "array-contains", Number($scope.userId));
        }
        if ($scope.accessLvl == 2) {
            dashBoardQuery = dashBoardQuery.where("c701_assc_d_id", "==", $scope.distId);
        }
        return dashBoardQuery;
    }

    // getting Upcoming cases from Firestore database and passing it to GmCtlDashboard
    getUpcomingCaseCount = async function ($scope) {
        var currentDate = new Date();
        var arrayGmMdlCases = [];
        $scope.arrayReps = [];
        currentDate.setDate(currentDate.getDate() + 1);
        const upperLimit = firebase.firestore.Timestamp.fromDate(new Date(currentDate.setHours(0, 0, 0, 0)));
        var dashboardQuery = this.accessFilterForCase($scope);
        // Upcoming Cases
        arrayGmMdlCases = await dashboardQuery.where("c7600_startdt", ">=", upperLimit).get();
        $scope.upcomingCases = arrayGmMdlCases.size;
        if (!arrayGmMdlCases.size) {
            $scope.upcomingCases = 0;
        }
        $scope.$apply();
    }

    //Load Calendar cases
    loadCalendarCases = async function ($scope) {
        var cases = new Array;
        const lowerLimit = firebase.firestore.Timestamp.fromDate($scope.fromDate);
        const upperLimit = firebase.firestore.Timestamp.fromDate($scope.toDate);
        if (!$scope.arrayReps) {
            const eventList = await this.collection.where("c7600_startdt", ">=", lowerLimit).where("c7600_startdt", "<=", upperLimit).get();
            return eventList;
        } else {
            console.log("$scope.arrayReps");
            console.log($scope.arrayReps);
            const eventList = await this.collection.where("c7600_startdt", ">=", lowerLimit).where("c7600_startdt", "<=", upperLimit)
                .where("c703_coveredbyid", "==", Number($scope.userId)).where("c9100_compid", "==", $scope.cmpId).where("c6700_voidfl", "==", "false").get();
            return eventList;

        }
    }

    // getting data from GmCtlCaseSetup and stroing it as a document in Firestore collection
    createCase = async function ($scope) {
        if ($scope.newCase.caseId == undefined) {
            var caseNew = this.collection.doc();
        }
        else {
            var caseNew = this.collection.doc($scope.newCase.caseId);
        }
        // var start = new Date($scope.newCase.startDate.toISOString().slice(0, 10) + "T" +
        //     $scope.newCase.startTime.toString().slice(16, 21) + ":00");
        // var startDate = firebase.firestore.Timestamp.fromDate(start);
        // console.log(startDate);
        // var end = new Date($scope.newCase.startDate.toISOString().slice(0, 10) + "T" +
        //     $scope.newCase.endTime.toString().slice(16, 21) + ":00");
        // var endDate = firebase.firestore.Timestamp.fromDate(end);
        // console.log(endDate);
        var start = new Date(new Date($scope.newCase.startDate).toISOString().slice(0, 10) + "T" + $scope.newCase.startTime + ":00");
        var startDate = firebase.firestore.Timestamp.fromDate(start);
        console.log(start);
        console.log(startDate);
        var end = new Date(new Date($scope.newCase.startDate).toISOString().slice(0, 10) + "T" + $scope.newCase.endTime + ":00");
        var endDate = firebase.firestore.Timestamp.fromDate(end);
        console.log(end);
        console.log(endDate);
        var companyId = localStorage.getItem('cmpid');
        var userId = localStorage.getItem('userID')
        var updateDate = new Date();
        var voidFl = "false";
        var eventId = '';
        var statusId = 0;

        var surgeons = new Array;
        var surgeonsId = new Array;
        var surgeonsNpi = new Array;
        var shared = new Array;
        var sharedRepId = new Array;
		$scope.caseTitle = "";
        $scope.caseTitle = $scope.newCase.accountName ;
		
        if ($scope.newCase.capitalEqip == undefined) {
            $scope.newCase.capitalEqip = null;
        }
        if ($scope.newCase.procedure == undefined) {
            $scope.newCase.procedure = null;
        }
        if ($scope.newCase.level == undefined) {
            $scope.newCase.level = null;
        }
        if ($scope.newCase.caseCoveredById == undefined) {
            $scope.newCase.caseCoveredById = null;
        }
        if ($scope.newCase.caseCoveredBy == undefined) {
            $scope.newCase.caseCoveredBy = null;
        }
        if ($scope.newCase.timeZone == undefined) {
            $scope.newCase.timeZone = null;
        }
        if ($scope.newCase.notes == undefined) {
            $scope.newCase.notes = null;
        }
        for (var i = 0; $scope.surgeonChip.length > i; i++) {
            surgeonsId.push($scope.surgeonData[$scope.surgeonChip[i]].id);
            surgeonsNpi.push($scope.surgeonData[$scope.surgeonChip[i]].npi);
            var surgeonJson = surgeonConverter.toFirestore(new GmSurgeonModel($scope.surgeonData[$scope.surgeonChip[i]].id, $scope.surgeonChip[i], $scope.surgeonData[$scope.surgeonChip[i]].npi));
            surgeons.push(surgeonJson);
			$scope.caseTitle = $scope.caseTitle + $scope.surgeonChip[i];
        }
		$scope.caseTitle = $scope.caseTitle  + $scope.newCase.procedure;
        for (var i = 0; $scope.shareChip.length > i; i++) {
            var shareWithJson = salesRepConverter.toFirestore(new GmSalesRepModel($scope.shareWithDetails[$scope.shareChip[i]], $scope.shareChip[i]));
            shared.push(shareWithJson);
            sharedRepId.push($scope.shareWithDetails[$scope.shareChip[i]]);
        }
        if ($scope.newCase.caseId == undefined) {
            $scope.asscId = "";
            $scope.caseCoveredById = "";
            $scope.asscUserIds = [];
            $scope.caseCoveredById = $scope.newCase.caseCoveredById;
            $scope.caseId = caseNew.id;

            const gmSalesHierarchy = new GmSalesHierarchyDao("FT700_SALES_HIERARCHY");
            gmSalesHierarchy.updateSalesAccessFields(function () { }, $scope);

            caseNew
                .withConverter(caseConverter)
                .set(new GmCaseModel(caseNew.id, $scope.newCase.accountId, $scope.newCase.accountName, $scope.newCase.accRepId,
                    $scope.newCase.accRepName, surgeons, surgeonsId, surgeonsNpi, $scope.newCase.capitalEqip, $scope.newCase.procedure, $scope.newCase.procedureId,
                    $scope.newCase.level, $scope.newCase.caseCoveredById, $scope.newCase.caseCoveredBy,
                    startDate, endDate, $scope.newCase.timeZone, shared, sharedRepId,
                    $scope.newCase.notes, $scope.caseTitle, companyId, updateDate, voidFl,
                    userId, eventId, statusId, [], $scope.asscId, $scope.asscUserIds));
        }
        else {
			$scope.caseCoveredById=$scope.newCase.caseCoveredById;      
            $scope.caseId=$scope.newCase.caseId;
		    if($scope.editCaseCoveredBy!=$scope.newCase.caseCoveredBy){
				const gmSalesHierarchy = new GmSalesHierarchyDao("FT700_SALES_HIERARCHY");
				gmSalesHierarchy.updateSalesAccessFields(function(){},$scope);
			}
            caseNew
                .update({
                    "c704_acctid": $scope.newCase.accountId,
                    "c704_acctnm": $scope.newCase.accountName,
                    "c703_acctrepid": $scope.newCase.accRepId,
                    "c703_acctrepnm": $scope.newCase.accRepName,
                    "ca7600_surgns": surgeons,
                    "ca6700_surgnids": surgeonsId,
                    "ca6700_sugnsnpis": surgeonsNpi,
                    "c7600_robot": $scope.newCase.capitalEqip,
                    "ca901_procedures": $scope.newCase.procedure,
					"ca901_proceduresids" : $scope.newCase.procedureId,
                    "c7600_lvl": $scope.newCase.level,
                    "c703_coveredbyid": $scope.newCase.caseCoveredById,
                    "c703_coveredbynm": $scope.newCase.caseCoveredBy,
                    "c7600_startdt": startDate,
                    "c7600_enddt": endDate,
                    "c7600_timezn": $scope.newCase.timeZone,
                    "ca703_shared": shared,
                    "ca703_sharedrepids": sharedRepId,
                    "c7600_notes": $scope.newCase.notes,
                    "c7600_title": $scope.newCase.caseTitle,
                    "c9100_compid": companyId,
                    "c6700_updatedt": updateDate,
                    "c6700_updatedby": userId
                });
        }

        if ($scope.newCase.caseId == undefined) {
            var ca6700_uploads = [];
            var fileLength = $scope.selectedFiles.length;
            var caseID = caseNew.id;
            for (var i = 0; i < fileLength; i++) {
                uploadFiles($scope.selectedFiles[i]);
            }
            function uploadFiles(totalFile) {
                return new Promise(function (resolve, reject) {
                    var storageRef = firebase.storage().ref().child('CSM').child(caseNew.id).child(totalFile.name);
                    var uploadFilesStorage = storageRef.put(totalFile);
                    uploadFilesStorage.on('state_changed',
                        function progress(snapshot) { },
                        function error(err) { },
                        function complete() {
                            uploadFilesStorage.snapshot.ref.getDownloadURL().then(function (downloadURL) {
                                var fileSize = totalFile.size / 1024 / 1024;
                                var fileExtension = totalFile.name.substr(totalFile.name.lastIndexOf('.') + 1);
                                caseNew.update({
                                    "ca6700_uploads": firebase.firestore.FieldValue.arrayUnion(
                                        { "fid": caseID, "nm": totalFile.name, "path": downloadURL, "type": fileExtension, "size": fileSize })
                                });
                            });
                        });
                });
            }
        }
        else {
            var fileLength = $scope.selectedFiles.length;
            for (var j = 0; j < $scope.deletedFilesList.length; j++) {
                var fId = $scope.deletedFilesList[j].fId;
                var fNm = $scope.deletedFilesList[j].fNm;
                var fPath = $scope.deletedFilesList[j].fPath;
                var fType = $scope.deletedFilesList[j].fType;
                var fSize = $scope.deletedFilesList[j].fSize;
                firebase.storage().ref().child('CSM').child($scope.newCase.caseId).child(fNm).delete();
                caseNew.update({
                    "ca6700_uploads": firebase.firestore.FieldValue.arrayRemove
                        ({ "fid": fId, "nm": fNm, "path": fPath, "type": fType, "size": fSize })
                });
            }
            for (var i = 0; i < fileLength; i++) {
                updateFilesCase($scope.selectedFiles[i])
            }
            function updateFilesCase(totalFiles) {
                return new Promise(function (resolve, reject) {
                    var storageRef = firebase.storage().ref().child('CSM').child($scope.newCase.caseId).child(totalFiles.name);
                    var updateFiles = storageRef.put(totalFiles);
                    updateFiles.on('state_changed', function progress(snapshot) { },
                        function error(err) { },
                        function complete() {
                            updateFiles.snapshot.ref.getDownloadURL().then(function (downloadURL) {
                                var fileExtension = totalFiles.name.substr(totalFiles.name.lastIndexOf('.') + 1);
                                var fileSize = totalFiles.size / 1024 / 1024;
                                caseNew.update({
                                    ca6700_uploads: firebase.firestore.FieldValue.arrayUnion(
                                        { "fid": $scope.newCase.caseId, "nm": totalFiles.name, "path": downloadURL, "type": fileExtension, "size": fileSize })
                                });
                            });
                        });
                });
            }

        }


    }

    // getting all the case documents from Firestore collection and passing it GmCaseSetupController
    editCase = function ($scope) {
        // Firestore query to get cases from firestore
        this.collection.where("c7600_caseid", "==", $scope.editCaseId).withConverter(caseConverter).get().then((querySnapshot) => {
            querySnapshot.forEach(doc => {
                var caseNew = doc.data();
                $scope.cases.push(caseNew);
            })
            $scope.newCase.caseId = $scope.cases[0].getCaseId();
            $scope.newCase.accountId = $scope.cases[0].getAccountId();
            $scope.newCase.accountName = $scope.cases[0].getAccountName();
            $scope.newCase.hospital = $scope.cases[0].getAccountName();
            $scope.newCase.accRepId = $scope.cases[0].getAccRepId();
            $scope.newCase.accRepName = $scope.cases[0].getAccRepName();
            $scope.newCase.surgeons = $scope.cases[0].getSurgeons();
            $scope.newCase.surgeonsId = $scope.cases[0].getSurgeonsId();
            $scope.newCase.surgeonsNpi = $scope.cases[0].getSurgeonsNpi();
            $scope.newCase.capitalEqip = $scope.cases[0].getCapitalEqip();
            $('#capitalEqip').val($scope.newCase.capitalEqip);
            $('#capitalEqip').formSelect();
            $scope.newCase.procedure = $scope.cases[0].getProcedure();
            $scope.newCase.level = $scope.cases[0].getLevel();
            $scope.newCase.caseCoveredById = $scope.cases[0].getCaseCoveredById();
            $scope.newCase.caseCoveredBy = $scope.cases[0].getCaseCoveredBy();
            $scope.newCase.start = $scope.cases[0].getStart();
            $scope.newCase.end = $scope.cases[0].getEnd();
            $scope.newCase.caseTitle = $scope.cases[0].getCaseTitle();
            $scope.newCase.companyId = $scope.cases[0].getCompanyId();
            $scope.newCase.updateDate = $scope.cases[0].getUpdateDate();
            $scope.newCase.voidFl = $scope.cases[0].getVoidFl();
            $scope.newCase.userId = $scope.cases[0].getUserId();
            $scope.newCase.statusId = $scope.cases[0].getEventId();
			$scope.newCase.asscUserIds=$scope.cases[0].getAsscUserIds();   
			$scope.editCaseCoveredBy=$scope.cases[0].getCaseCoveredBy();   

            if ($scope.action == "edit") {
                $scope.newCase.notes = $scope.cases[0].getNotes();
                $scope.newCase.timeZone = $scope.cases[0].getTimeZone();
                $scope.newCase.shared = $scope.cases[0].getShared();
                $scope.newCase.sharedRepId = $scope.cases[0].getSharedRepId();
                $scope.newCase.eventId = $scope.cases[0].getStatusId();
                $scope.newCase.fileList = $scope.cases[0].getFileUpload();
                $('#timeZone').val($scope.newCase.timeZone);
                $('#timeZone').formSelect();
                //set start date and end date
                var start = $scope.newCase.start;
                var startDate = start.toDate();
                $scope.newCase.startDate = startDate;
                $scope.newCase.startTime = startDate;
                var end = $scope.newCase.end;
                var endDate = end.toDate();
                $scope.newCase.endTime = endDate;
                // $scope.editFiles.push($scope.cases[0].getFileUpload());
                for (var i = 0; i < $scope.newCase.fileList.length; i++) {
                    $scope.editFiles.push($scope.newCase.fileList[i]);
                }

                for (var i = 0; i < $scope.newCase.surgeons.length; i++) {
                    var obj = new Object();
                    obj.tag = $scope.newCase.surgeons[i].nm;
                    $scope.surgeonChip.push($scope.newCase.surgeons[i].nm);
                    $scope.surgeonChipTag.push(obj);
                }
                for (var i = 0; i < $scope.newCase.shared.length; i++) {
                    var obj = new Object();
                    obj.tag = $scope.newCase.shared[i].repnm;
                    $scope.shareChip.push($scope.newCase.shared[i].repnm);
                    $scope.shareChipTag.push(obj);
                }
                //Surgeon data from sqlite
                $scope.typeSurgeon = function () {
                    const obj = new GmSurgeonDao("Cases");
                    searchSurgeonInfo(obj);

                    async function searchSurgeonInfo(obj) {
                        await obj.searchSurgeonInfo($scope);
                    }
                }
                //sales Rep data from sqlite
                $scope.typeShareWith = function () {
                    const obj = new GmSalesRepDao("Cases");
                    searchRep(obj);
                    async function searchRep(obj) {
                        await obj.searchRep($scope);
                    }
                }
                $('.chips-autocomplete-edit').chips({
                    placeholder: 'Search..',
                    appendTo: "#someElem",
                    noItem: 'no data found',
                    data: $scope.surgeonChipTag,
                    autocompleteOptions: {
                        function: $scope.typeSurgeon(),
                        data: $scope.surgeonList,
                        limit: Infinity,
                        minLength: 3,
                        delay: 1000,
                    },
                    onChipAdd: function (e, chip) {
                        var item = chip.childNodes[0].textContent;
                        if ($scope.surgeonList[item] !== null) {
                            $('.chips-autocomplete .chip').last().remove();
                        }
                        else {
                            $scope.surgeonChip.push(item);
                            $scope.check = false;
                        }
                    },
                    onChipDelete: function (e, chip) {
                        var item = chip.childNodes[0].textContent;
                        var index = $scope.surgeonChip.indexOf(item);
                        $scope.surgeonChip.splice(index, 1);
                        $scope.check = false;
                        $scope.tags = $.grep(tags, function (value) {
                            return value != item;
                        });
                    }
                });
                $('.chips-autocomplete-share-edit').chips({
                    placeholder: 'Search..',
                    appendTo: "#someElem",
                    noItem: 'no data found',
                    data: $scope.shareChipTag,
                    autocompleteOptions: {
                        function: $scope.typeShareWith(),
                        data: $scope.salesRepDetails,
                        limit: Infinity,
                        minLength: 3,
                        delay: 1000,
                    },
                    onChipAdd: function (e, chip) {
                        var item = chip.childNodes[0].textContent;
                        if ($scope.salesRepDetails[item] !== null) {
                            $('.chips-autocomplete .chip').last().remove();
                        }
                        else {
                            $scope.shareChip.push(item);
                            $scope.check = false;
                        }
                    },
                    onChipDelete: function (e, chip) {
                        var item = chip.childNodes[0].textContent;
                        var index = $scope.shareChip.indexOf(item);
                        $scope.shareChip.splice(index, 1);
                        $scope.check = false;
                        $scope.tags = $.grep(tags, function (value) {
                            return value != item;
                        });
                    }
                });
            }

            $('#procedure').val($scope.newCase.procedure);
            $('#procedure').formSelect();
            if ($scope.newCase.exclusive == "no") {
                $("#excelsius_no").prop("checked", true);
            } else {
                $("#excelsius_yes").prop("checked", true);
            }
            $scope.$apply();
        }).catch((error) => {
            console.log("Error getting document:", error);
        });

    };

    loadCases = function ($scope) {
        this.collection.orderBy('c7600_startdt').withConverter(caseConverter).get().then((querySnapshot) => {
            querySnapshot.forEach(doc => {
                var caseNew = doc.data();
                $scope.cases.push(caseNew);
                $scope.$apply();
            })
        }).catch((error) => {
        });
    };

    loadCaseByTitle = async function ($scope, searchText, callback) {
        var queryRes = await this.collection
            .orderBy('c7600_title')
            .startAt(searchText)
            .endAt(searchText + "\uf8ff").get();
        if (queryRes.size > 0) {
            await this.collection
                .orderBy('c7600_title')
                .startAt(searchText)
                .endAt(searchText + "\uf8ff").get()
                .then((querySnapshot) => {
                    querySnapshot.forEach(doc => {
                        var caseNew = doc.data();
                        $scope.caseTitles.push([caseNew.c7600_title, caseNew.c7600_caseid]);
                        if ($scope.autoCompData) {
                            $scope.autoCompData[caseNew.c7600_title] = null;
                        }
                        if ($scope.advSearch) {
                            $scope.advSearch.casePair = [caseNew.c7600_title, caseNew.c7600_caseid];
                        }
                        $scope.$apply();
                    });
                    callback();
                }).catch((error) => {
                    $scope.caseTitles.push("No data found.");
                    if ($scope.advSearch) {
                        $scope.advSearch.casePair = null;
                    }
                    $scope.$apply();
                });
        }
        else {
            $scope.caseTitles.push(["No data found.", ""]);
            if ($scope.advSearch) {
                $scope.advSearch.casePair = null;
            }
            $scope.$apply();
            callback();
        }
    }
	// getting all the case documents from Firestore collection and passing it GmAdvanceSearchReportController
    loadReport = function ($scope, callback) {     
        var currentDate = new Date();    
        var reportQuery =  this.collection;
        currentDate.setDate(currentDate.getDate() + 1);
        const upperLimit = firebase.firestore.Timestamp.fromDate(new Date(currentDate.setHours(0, 0, 0, 0)));
        //Using route param get summary access from dashboard Upcoming case count
        //Adding Start date greater than condition to the query
        if ($scope.summaryAction == "upcomingCases" || $scope.summaryAction == "search") {
            $scope.summaryTitle = "Upcoming Cases";
            console.log("upcoming cases");
            reportQuery = this.accessFilterForCase($scope);
            reportQuery = reportQuery.where("c7600_startdt", ">=", upperLimit);
            // Firestore query to get cases from firestore from footer summary icon
            reportQuery.orderBy('c7600_startdt').where("c6700_voidfl", "==", "false").withConverter(caseConverter).get().then((querySnapshot) => {
                querySnapshot.forEach(doc => {
                    var caseNew = doc.data();
                    $scope.cases.push(caseNew);
                    $scope.$apply();
                });
                callback($scope);
            }).catch((error) => {
                console.log("Error getting document:", error);
            });
        }
        //Using route param get summary access from dashboard Today's case count
        //Adding upperLimit and lowerLimit condition to the query
        else if ($scope.summaryAction == "todaysCases") {
            console.log("todays cases");
            const lowerLimit = firebase.firestore.Timestamp.fromDate(new Date(new Date().setHours(0, 0, 0, 0)));
            // reportQuery= this.accessFilterForCase($scope);
            $scope.summaryTitle = "Today's Cases";
            reportQuery = reportQuery.where("c7600_startdt", ">=", lowerLimit).where("c7600_startdt", "<=", upperLimit);
            // Firestore query to get cases from firestore from footer summary icon
            reportQuery.orderBy('c7600_startdt').where("c6700_voidfl", "==", "false").withConverter(caseConverter).get().then((querySnapshot) => {
                querySnapshot.forEach(doc => {
                    var caseNew = doc.data();
                    $scope.cases.push(caseNew);
                    $scope.$apply();
                });
                callback($scope);
            }).catch((error) => {
                console.log("Error getting document:", error);
            });
        }

    };
    loadAdvancedSearchReport = async function ($scope, callback) {
        $scope.summaryTitle = "Search Results";
        if ($scope.advSearch.salesRepId) {
            var advSearchQuery1 = this.getFirestoreQuery($scope);
            advSearchQuery1 = advSearchQuery1.where("ca703_sharedrepids", "array-contains", Number($scope.advSearch.salesRepId));
            $scope.casesBySharedSalesRepIds = new Array;
            await advSearchQuery1.orderBy('c7600_startdt').withConverter(caseConverter).get().then((querySnapshot) => {
                querySnapshot.forEach(doc => {
                    var caseNew = doc.data();
                    $scope.casesBySharedSalesRepIds.push(caseNew);
                    var ele = document.querySelectorAll('.dropdown-trigger');
                    M.AutoInit();
                    var instances = M.Dropdown.init(ele, {
                        constrainWidth: false
                    });
                    $scope.$apply();
                });
            }).catch((error) => {
                console.log("Error getting document:", error);
            });

            var advSearchQuery2 = this.getFirestoreQuery($scope);
            advSearchQuery2 = advSearchQuery2.where("c703_coveredbyid", "==", Number($scope.advSearch.salesRepId));
            $scope.casesByCaseCoveredById = new Array;
            await advSearchQuery2.orderBy('c7600_startdt').withConverter(caseConverter).get().then((querySnapshot) => {
                querySnapshot.forEach(doc => {
                    var caseNew = doc.data();
                    $scope.casesByCaseCoveredById.push(caseNew);
                    var ele = document.querySelectorAll('.dropdown-trigger');
                    M.AutoInit();
                    var instances = M.Dropdown.init(ele, {
                        constrainWidth: false
                    });
                    $scope.$apply();
                });
            }).catch((error) => {
                console.log("Error getting document:", error);
            });
            if ($scope.casesBySharedSalesRepIds.length > 0 && $scope.casesByCaseCoveredById.length > 0) {
                this.merge($scope);
                $scope.$apply();
                callback($scope);
            }
            else if ($scope.casesBySharedSalesRepIds.length > 0) {
                $scope.cases = $scope.casesBySharedSalesRepIds;
                $scope.$apply();
                callback($scope);
            }
            else {
                $scope.cases = $scope.casesByCaseCoveredById;
                $scope.$apply();
                callback($scope);
            }
        }
        else {
            var advSearchQuery = this.getFirestoreQuery($scope);
            // Getting data from Firestore
            await advSearchQuery.orderBy('c7600_startdt').withConverter(caseConverter).get().then((querySnapshot) => {
                querySnapshot.forEach(doc => {
                    var caseNew = doc.data();
                    $scope.cases.push(caseNew);
                    var ele = document.querySelectorAll('.dropdown-trigger');
                    M.AutoInit();
                    var instances = M.Dropdown.init(ele, {
                        constrainWidth: false
                    });
                    $scope.$apply();
                });
                callback($scope);
            }).catch((error) => {
                console.log("Error getting document:", error);
            });
        }
    };

    getFirestoreQuery = function ($scope) {
        var advSearchQuery = this.collection.where("c6700_voidfl", "==", "false"); //.where("c703_acctrepid", "in", JSON.parse(localStorage.arrayReps));
        if ($scope.advSearch.caseId) {
            //Applying Case Id filter
            advSearchQuery = advSearchQuery.where("c7600_caseid", "==", $scope.advSearch.caseId);
        }
        if ($scope.advSearch.surgeonId) {
            //Applying Surgeon Id filter
            advSearchQuery = advSearchQuery.where("ca6700_surgnids", "array-contains", Number($scope.advSearch.surgeonId));
        }
        if ($scope.advSearch.procedure) {
            //Applying Procedure filter
            advSearchQuery = advSearchQuery.where("ca901_procedures", "array-contains", $scope.advSearch.procedure);
        }
        if ($scope.advSearch.account) {
            //Applying account Id filter
            advSearchQuery = advSearchQuery.where("c704_acctid", "==", $scope.advSearch.accountId);
        }
        if ($scope.advSearch.startDate && $scope.advSearch.endDate) {
            console.log("Applying Dates filter");
            $scope.advSearch.startDate = new Date($scope.advSearch.startDate);
            $scope.advSearch.endDate = new Date($scope.advSearch.endDate);
            console.log($scope.advSearch.startDate.toISOString());
            console.log($scope.advSearch.endDate.toISOString());
            if ($scope.advSearch.startDate.toISOString() == $scope.advSearch.endDate.toISOString()) {
                console.log("Same day");
                var start = new Date($scope.advSearch.startDate.toISOString().slice(0, 10) + "T" + "00:00:00");
                var startDate = firebase.firestore.Timestamp.fromDate(start);
                var end = new Date($scope.advSearch.endDate.toISOString().slice(0, 10) + "T" + "23:59:59");
                var endDate = firebase.firestore.Timestamp.fromDate(end);
                console.log(start);
                console.log(end);
            }
            else {
                console.log("Different day");
                var start = new Date($scope.advSearch.startDate.toISOString().slice(0, 10) + "T" + "00:00:00");
                var startDate = firebase.firestore.Timestamp.fromDate(start);
                var end = new Date($scope.advSearch.endDate.toISOString().slice(0, 10) + "T" + "23:59:59");
                var endDate = firebase.firestore.Timestamp.fromDate(end);
                console.log(start);
                console.log(end);
            }
            advSearchQuery = advSearchQuery.where("c7600_startdt", ">=", startDate).where("c7600_startdt", "<=", endDate);
        }
        return advSearchQuery;
    }

    getAccounts = function ($scope) {
        const db = GmSqlite.getConnection();
        db.transaction(getAccounts, errorCB, successCB);
        function getAccounts(tx) {
            tx.executeSql("select c704_account_nm from t704_account where c704_account_nm like \"" + $scope.newCase.hospital + "%\"", [],
                function (tx, rs) {
                    if (rs.rows.length > 0) {
                        for (var i = 0; i < rs.rows.length; i++) {
                            $scope.accounts[rs.rows.item(i).C704_ACCOUNT_NM] = null;
                            $scope.$apply();
                        }
                    }
                });
        }
        function errorCB(tx, err) {
            console.log(tx);
            console.log(err);
            showMsgView("009");
        }
        function successCB() {
            //showMsgView("008");
        }
    };

    merge = function ($scope) {
        var i = 0, j = 0, k = 0;
        $scope.cases = new Array;
        while (i < $scope.casesByCaseCoveredById.length && j < $scope.casesBySharedSalesRepIds.length) {
            if ($scope.casesByCaseCoveredById[i].getStart().seconds < $scope.casesBySharedSalesRepIds[j].getStart().seconds) {
                $scope.cases[k++] = $scope.casesByCaseCoveredById[i++];
                $scope.$apply();
            }
            else {
                $scope.cases[k++] = $scope.casesBySharedSalesRepIds[j++];
                $scope.$apply();
            }
        }
        while (i < $scope.casesByCaseCoveredById.length) {
            $scope.cases[k++] = $scope.casesByCaseCoveredById[i++];
            $scope.$apply();
        }
        while (j < $scope.casesBySharedSalesRepIds.length) {
            $scope.cases[k++] = $scope.casesBySharedSalesRepIds[j++];
            $scope.$apply();
        }
    }

    // Cancel a case by updating void flag
    cancelCase = async function ($scope) {
        this.collection.doc($scope.caseId).update({ c6700_voidfl: true });
        $('#conformModal').modal('close');
        window.location.href = "#!GmAdvanceSearchReport/search";
    }
    //Save and remove share with rep from firestore
    saveSharedWith = async function ($scope) {
        const collectionRef = this.collection.doc($scope.editCaseId);
        const todayTimestamp = firebase.firestore.Timestamp.fromDate(new Date(new Date().setHours(0, 0, 0, 0)));
        //Remove share with
        if ($scope.shareWithIds.length > 0) {
            for (var i = 0; i < $scope.shareWithIds.length; i++) {
                collectionRef.update({
                    "ca703_shared": firebase.firestore.FieldValue.arrayRemove({ "repid": $scope.shareWithIds[i], "repnm": $scope.shareWithNames[i] }),
                    "ca703_sharedrepids": firebase.firestore.FieldValue.arrayRemove($scope.shareWithIds[i]),
                    "c6700_updatedby": $scope.userId, "c6700_updatedt": todayTimestamp
                })
            }
        }
        //Save share with
        if ($scope.sharedRepIds.length > 0) {
            for (var j = 0; j < $scope.sharedRepIds.length; j++) {
                collectionRef.update({
                    "ca703_sharedrepids": firebase.firestore.FieldValue.arrayUnion($scope.sharedRepIds[j]),
                    "ca703_shared": firebase.firestore.FieldValue.arrayUnion
                        ({ "repid": $scope.sharedRepIds[j], "repnm": $scope.sharedRepNames[j] }),
                    "c6700_updatedby": $scope.userId, "c6700_updatedt": todayTimestamp
                });
            }
        }
        window.location.href = "#!GmShareCase/" + $scope.editCaseId;
    }
    //files List in report screen
    loadAttachments = function ($scope) {
        $scope.files = new Array;
        this.collection.where("c7600_caseid", "==", $scope.caseId).get().then((querySnapshot) => {
            querySnapshot.forEach(doc => {
                console.log(doc.data()); console.log(doc.data().ca6700_uploads);
                if (doc.data().ca6700_uploads.length != undefined) {
                    for (var i = 0; i < doc.data().ca6700_uploads.length; i++) {
                        var caseNew = doc.data().ca6700_uploads[i];
                        $scope.files.push(caseNew);
                    }
                }
                $scope.$apply();
            })
        }).catch((error) => {
            console.log("Error getting document:", error);
        });
    };
}