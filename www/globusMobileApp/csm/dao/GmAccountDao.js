// file name and class name should be same for class files
class GmAccountDao {
   
	getAccounts = function ($scope,callback) {
		const db = GmSqlite.getConnection();
		db.transaction(getAccounts, errorCB, successCB);
		function getAccounts(tx) {
			tx.executeSql("SELECT C704_ACCOUNT_ID AS ID,C704_ACCOUNT_NM AS NAME,C703_SALES_REP_ID AS ACCTREPID FROM T704_ACCOUNT WHERE C704_ACCOUNT_NM LIKE \""+$scope.newCase.hospital + "%\"", [],
				function (tx, rs) {
					if (rs.rows.length > 0) {
						for (var i = 0; i < rs.rows.length; i++) {
							const obj = new GmAccountModel(rs.rows.item(i).ID, rs.rows.item(i).NAME, rs.rows.item(i).ACCTREPID);
                            
                            console.log(rs.rows.item(i).ID+"-"+ rs.rows.item(i).NAME);
							$scope.accountsDetails[obj.getAcctNm()] = obj.getAcctId();
							$scope.accounts[obj.getAcctNm()] = null;
							$scope.accountEmpty = false;
							
						}
                        $scope.$apply();
                        callback($scope);
					}
					else{
                        console.log("error");
						$scope.accountEmpty=true;
						$scope.$apply();
					}
				});
		}
		function errorCB(tx, err) {
			console.log(tx);
			console.log(err);
		}
		function successCB() {
            console.log("success");
			//showMsgView("008");
		}
	};
	
	getAccountRepInfo = function (callback,accountId){
		const db = GmSqlite.getConnection();
		db.transaction(getAccountRepInfo, errorCB, successCB);
		function getAccountRepInfo(tx) {
			tx.executeSql("select t703.c703_sales_rep_id acctrepid, t703.c703_sales_rep_name acctrepnm from t704_account t704, t703_sales_rep t703 where t704.c703_sales_rep_id =t703.c703_sales_rep_id and t704.c704_account_id = '" + accountId + "' and t704.c704_void_fl = '' and t703.c703_void_fl = ''", [],
				function (tx, rs) {
					if (rs.rows.length > 0) {
						callback(rs.rows.item(0).acctrepid , rs.rows.item(0).acctrepnm);
					}
				});
		}
		function errorCB(tx, err) {
			console.log(tx);
			console.log(err);
			//showMsgView("009");
		}
		function successCB() {
			//showMsgView("008");
		}
	}
	
	searchAccountInfo = function ($scope, searchText, callback) {
        const db = GmSqlite.getConnection();
        db.transaction(function getAccounts(tx) {
            tx.executeSql("SELECT C704_ACCOUNT_ID AS ID,C704_ACCOUNT_NM AS NAME,C703_SALES_REP_ID AS ACCTREPID FROM T704_ACCOUNT WHERE C704_ACCOUNT_NM LIKE \"" + searchText + "%\"", [],
                function (tx, rs) {
                    if (rs.rows.length > 0) {
                        for (var i = 0; i < rs.rows.length; i++) {
                            $scope.accounts[rs.rows.item(i).NAME] = null;
                            $scope.advSearch.accountPair = [rs.rows.item(i).NAME, rs.rows.item(i).ID];
                            $scope.$apply();
                        }
                    }
                    else {
                        if ($scope.advSearch) {
                            $scope.advSearch.accountPair = null;
                        }
                        $scope.$apply();
                    }
                    callback();
                });
        });

    };

}
