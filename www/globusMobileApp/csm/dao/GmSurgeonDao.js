// file name and class name should be same for class files
class GmSurgeonDao {
	
    searchSurgeonInfo = function ($scope,callback) {
    const db = GmSqlite.getConnection();
    db.transaction(searchSurgeonInfo, errorCB, successCB);
    function searchSurgeonInfo(tx) {
       tx.executeSql("SELECT t101.c101_party_id surgnid,t101.c101_first_nm surgnfnm,t101.c101_last_nm surgnlnm,t6600.c6600_surgeon_npi_id npi,(t101.c101_first_nm)|| ' ' || (t101.c101_last_nm)|| ' ' || t6600.c6600_surgeon_npi_id surgnfullnamenpi FROM t101_party t101,t6600_party_surgeon t6600 WHERE t101.c901_party_type = 7000 AND t101.c101_party_id = t6600.c101_party_surgeon_id AND t101.c101_void_fl ='' AND t6600.c6600_void_fl ='';", [],
            function (tx, rs) {
                if (rs.rows.length > 0) {
                    for (var i = 0; i < rs.rows.length; i++) {
                        const obj = new GmSurgeonModel(rs.rows.item(i).surgnid, rs.rows.item(i).surgnfullnamenpi, rs.rows.item(i).npi);
                        $scope.surgeonData[obj.getSurgnNm()] = {
                            'id' : obj.getSurgnId(),
                            'npi' : obj.getSurgnNpi()
                        };
                        
                        $scope.surgeonList[obj.getSurgnNm()] = null;
                    }
                    $scope.$apply();
                    callback($scope);
                }
            });
    }

    function errorCB(tx, err) {
        console.log(tx);
        console.log(err);
        //showMsgView("009");
    }

    function successCB() {
        //showMsgView("008");
    }
};
searchSurgeonInfoCallback = function ($scope, searchText, callback) {
        const db = GmSqlite.getConnection();
        db.transaction(function (tx, rs) {
            var query = "select t101.c101_party_id SurgnID," +
            " t6600.c6600_surgeon_npi_id NPI," +
            " Upper(t101.c101_first_nm) || ' ' || Upper(t101.c101_last_nm) surgnfullname" +
            " from t101_party t101, t6600_party_surgeon t6600" +
            " where t101.c101_party_id = t6600.c101_party_surgeon_id ";
            if(isNaN(searchText)){
                query = query + " and surgnfullname like '" + searchText + "%' ";
            }
            else {
                query = query + " and NPI like '" + searchText + "%' ";
            }
            query = query + " and t101.c901_party_type = 7000 " +
            " and t6600.c6600_void_fl = '' " +
            " and (t101.c101_active_fl = '' or t101.c101_active_fl='Y') ";
            tx.executeSql(query, [],
                function (tx, rs) {
                    var i = 0;
                    if (rs.rows.length > 0) {
                        for (i = 0; i < rs.rows.length; i++) {
                            var surgnInfo = new Array;
                            surgnInfo[0] = rs.rows.item(i).surgnfullname;
                            surgnInfo[1] = rs.rows.item(i).SurgnID;
                            surgnInfo[2] = rs.rows.item(i).NPI;
                            $scope.surgns.push(surgnInfo);
                            if ($scope.autoCompData) {
                                $scope.autoCompData[surgnInfo[0]] = null;
                                $scope.autoCompData[surgnInfo[2]] = null;
                            }
                            if ($scope.advSearch) {
                                $scope.advSearch.surgeonPair = surgnInfo;
                            }
                            $scope.$apply();
                        }
                    }
                    else {
                        $scope.surgns.push(["No data found.", ""]);
                        if ($scope.advSearch) {
                            $scope.advSearch.surgeonPair = null;
                        }
                        $scope.$apply();
                    }
                    callback();
                },
                function (tx, error) {
                    console.log(error);
                });
        });
    };

}