class GmSalesHierarchyDao {
    constructor(col) {
        this.collection = GmFirebase.getCollection(col);
    }
    //getting values from  sales hierarchy and region table and update to access fields in case table
    updateSalesAccessFields= async function(callback,$scope){
        $scope.regionID=null;
		$scope.asscUserIds=[];       
        var regionCollection=GmFirebase.getCollection(" FT708_REGION_ASD_MAPPING");
        var caseCollection=GmFirebase.getCollection("FT7600_CASE");
        //getting values from  sales hierarchy
        this.collection.where("C703_REP_ID", "==",Number($scope.caseCoveredById)).get()
        .then((salesSnapshot) => {salesSnapshot.forEach((doc) => {
                $scope.regionID=doc.data().C901_REGION_ID;
                $scope.asscId=doc.data().C701_D_ID;
                $scope.asscUserIds.push(doc.data().C101_REP_USERID);
                $scope.asscUserIds.push(doc.data().C101_AD_ID);
                $scope.asscUserIds.push(doc.data().C101_VP_ID); 
                if($scope.edit == true){
                  const caseCollectionRef = caseCollection.doc($scope.caseId);
                  for(var i=0;i<$scope.newCase.asscUserIds.length;i++)
                  caseCollectionRef.update({
                  "ca101_assc_userids":firebase.firestore.FieldValue.arrayRemove($scope.newCase.asscUserIds[i])});
                }
                regionMapping(function(){},$scope);
             });
        })
        .catch((error) => {
            console.log("Error getting documents: ", error);
        });
        
        //update values for  associate fields in case 
        function regionMapping(callback, $scope){
        regionCollection.where("C901_REGION_ID", "==",  $scope.regionID).get()
        .then((regionSnapshot) => {regionSnapshot.forEach((doc) => {
                $scope.asscUserIds.push(doc.data().C101_USER_ID);
                const caseCollectionRef = caseCollection.doc($scope.caseId);
                caseCollectionRef.update({"c701_assc_d_id":$scope.asscId});
                for(var j=0;j<$scope.asscUserIds.length;j++){
                    caseCollectionRef.update({"ca101_assc_userids": firebase.firestore.FieldValue.arrayUnion($scope.asscUserIds[j])});
                }
                callback();
            });
        })
        .catch((error) => {
            console.log("Error getting documents: ", error);
        });
        }
    }
}