// file name and class name should be same for class files
class GmSalesRepDao {
	
	getSalesRepCallback = function ($scope, searchText, callback) {
        const db = GmSqlite.getConnection();
        db.transaction(function (tx, rs) {
            tx.executeSql("SELECT C703_SALES_REP_ID AS ID, C703_SALES_REP_NAME AS DispName FROM T703_SALES_REP  WHERE C703_ACTIVE_FL = 'Y' AND C703_VOID_FL = '' AND C703_SALES_REP_NAME LIKE '" + searchText + "%'", [],
                function (tx, rs) {
                    if (rs.rows.length > 0) {
                        for (var i = 0; i < rs.rows.length; i++) {
                            var salesRepInfo = new Array;
                            salesRepInfo[0] = rs.rows.item(i).DispName;
                            salesRepInfo[1] = rs.rows.item(i).ID;
                            $scope.salesReps.push(salesRepInfo);
                            if ($scope.autoCompData) {
                                $scope.autoCompData[salesRepInfo[0]] = null;
                            }
                            if ($scope.advSearch) {
                                $scope.advSearch.salesRepPair = salesRepInfo;
                            }
                            $scope.$apply();
                        }
                    }
                    else {
                        $scope.salesReps.push(["No data found.", ""]);
                        if ($scope.advSearch) {
                            $scope.advSearch.salesRepPair = null;
                        }
                        $scope.$apply();
                    }
                    callback();
                });
        });
    };
    getSalesRep = function ($scope,callback) {
        const db = GmSqlite.getConnection();
        db.transaction(getSalesRep, errorCB, successCB);
        function getSalesRep(tx) {
            tx.executeSql("SELECT C703_SALES_REP_ID AS ID, C703_SALES_REP_NAME AS DispName FROM T703_SALES_REP  WHERE C703_ACTIVE_FL = 'Y' AND C703_VOID_FL = '' AND C703_SALES_REP_NAME LIKE \""+$scope.salesRepSearchKey+ "%\"", [],
                function (tx, rs) {
                    if (rs.rows.length > 0) {
                        for (var i = 0; i < rs.rows.length; i++) {
                            const obj = new GmSalesRepModel(rs.rows.item(i).ID,rs.rows.item(i).DispName);
                            $scope.coveredByDetails[rs.rows.item(i).DispName] = rs.rows.item(i).ID;
                            $scope.salesRep[rs.rows.item(i).DispName] = null;
                            $scope.emptySalesRep=false;
                        }
                        $scope.$apply();
                        callback($scope);
                    }
                    else{
                        $scope.emptySalesRep = true;
                        $scope.$apply();
                        callback($scope);
                    }
                    
                });
        }
        function errorCB(tx, err) {
            console.log(tx);
            console.log(err);
            //showMsgView("009");
        }
        function successCB() {
            //showMsgView("008");
        }
    };
    searchRep = function ($scope,callback) {
        const db = GmSqlite.getConnection();
        db.transaction(searchRep, errorCB, successCB);
        function searchRep(tx) {
            tx.executeSql("SELECT C703_SALES_REP_ID AS ID, C703_SALES_REP_NAME AS DispName FROM T703_SALES_REP  WHERE C703_ACTIVE_FL = 'Y' AND C703_VOID_FL = ''", [],
                function (tx, rs) {
                    if (rs.rows.length > 0) {
                        for (var i = 0; i < rs.rows.length; i++) {
                            const obj = new GmSalesRepModel(rs.rows.item(i).ID,rs.rows.item(i).DispName);
                            $scope.shareWithDetails[obj.getRepNm()] = obj.getRepId();
                            $scope.salesRepDetails[obj.getRepNm()] = null;
                        }
                        $scope.$apply();
                        callback($scope);
                    }
                });
        }
        function errorCB(tx, err) {
            console.log(tx);
            console.log(err);
            //showMsgView("009");
        }
        function successCB() {
            //showMsgView("008");
        }
    };
    getUserSalesRep = function (callback,$scope) {
        const db = GmSqlite.getConnection();
        db.transaction(function (tx, rs) {
            tx.executeSql('SELECT C703_SALES_REP_ID FROM T703_SALES_REP where C101_PARTY_ID  ="'+$scope.partyId+'"',[],
            function(tx,rs) {
                var i=0;
                $scope.arrayReps=[];
                for (i = 0; i < rs.rows.length; i++) {
                    $scope.arrayReps.push(rs.rows.item(i).C703_SALES_REP_ID);
                }
                callback();
            },
            function(error){console.log(error);});
        });
    };
}