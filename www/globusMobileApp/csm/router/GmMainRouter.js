/**********************************************************************************
 * File:        GmMainRouter.js
 * Description: Main Router common to all applications
 * Version:     1.0
 * Author:      ejayabalan
 **********************************************************************************/

define([
  "jquery",
  "underscore",
  "backbone",
  "catalogview",
  "homeview",
  "loginview",
  "mainview",
  "settingsview",
  "collateralview",
  "gmvApp",
  "gmvCRMMain",
  "gmvMain",
  "csmmainview",
], function (
  $,
  _,
  Backbone,
  CatalogView,
  HomeView,
  LoginView,
  MainView,
  SettingsView,
  CollateralViews,
  GMVApp,
  GMVCRMMain,
  GMVMain,
  CsmMainView,
) {
  "use strict";

  var MainRouter = Backbone.Router.extend({
    routes: {
      "": "login",
      crm: "crm",
      csm: "csm",
      home: "home",
      logout: "login",
      settings: "settings",
      pending: "pending",
    },

    initialize: function () {
      mainview = new MainView();
    },

    back: function () {
      window.history.back();
    },

    home: function () {
      GM.Global.Token = window.localStorage.getItem("token");
      if (homeview) homeview.close();
      if (GM.Global.UserTabs) {
        homeview = new HomeView();
      } else {
        this.getTabs(function () {
          homeview = new HomeView();
        });
      }
    },

    login: function () {
      if (window.localStorage.getItem("token") == null) {
        var view = new LoginView();
        view.render();
      } else {
        intLogin = 1;
        //            chkUpdate();
        //            fnCheckShareHistory(); Due to PMT-23618 code has been commented
        //            fnTimerSplashScreen();
        $("#btn-updates").show();
        window.location.href = "#home";
      }
    },

    settings: function () {
      var view = new SettingsView();
      $("#div-main").html(view.el);
    },

    csm: function () {
      var view = new CsmMainView();
    },

    crm: function () {
      GM.Global.SalesRep = isRep();
      GM.Global.CRMMain = true;
      var gmvCRMMain = new GMVCRMMain();
      gmvCRMMain.render();
      // gmvApp.showView(gmvCRMMain);
      $("#div-messagebar").show();
      window.location.href = "#crm/dashboard";
    },

    getTabs: function (callback) {
      fnGetWebServerData(
        "crm/usertabs",
        "gmCRMTabsVO",
        { token: GM.Global.Token },
        function (arrTabs) {
          //                 console.log("UserTabs is as follows " + JSON.stringify(arrTabs));
          var userTabs = new Array();
          var userAccess = new Array();
          GM.Global.Admin = [];
          userTabs.push(tabData["Dashboard"]);
          userTabs[0].tabcaption = "Dashboard";
          if (arrTabs != null) {
            // console.log("ArrTABS is " + arrTabs);
            arrTabs = arrayOf(arrTabs);

            //console.log("UserTabs ARRAY is as follows " + JSON.stringify(arrTabs));
            var innerTabs = [];
            for (var i = 0; i < arrTabs.length && arrTabs.length > 0; i++) {
              var tab = tabData[arrTabs[i].funcnm];

              if (tab != undefined) {
                tab.tabcaption = arrTabs[i].funcnm;
                tab.readAcc = arrTabs[i].readacc;
                tab.updateAcc = arrTabs[i].updateacc;
                tab.voidAcc = arrTabs[i].voidacc;
                tab.functionID = arrTabs[i].funcid;
                if (!_.contains(innerTabs, tab.taburl)) userTabs.push(tab);
                innerTabs.push(tab.taburl);
              } else userAccess.push(arrTabs[i]);
            }
          } else {
            userTabs.push(tabData["Surgeon"]);
            userTabs[1].tabcaption = "Surgeon";
          }

          GM.Global.UserTabs = userTabs;
          GM.Global.UserAccess = userAccess;

          _.each(userAccess, function (data) {
            if (data.funcid.indexOf("CRMRQST-") >= 0) {
              var module = data.funcid.replace("CRMRQST-", "");
              module = module.toLowerCase();

              if (module == "physvisit") module = "physvisitrqst";

              if (!_.contains(GM.Global.Admin, module))
                // Loading all accessed modules into global array without duplicates.
                GM.Global.Admin.push(module);
            }
            if (data.funcid.indexOf("CRMADMIN-") >= 0) {
              var module = data.funcid.replace("CRMADMIN-", "");
              module = module.toLowerCase();
              if (module == "lead") {
                if (!_.contains(GM.Global.Admin, "lead"))
                  GM.Global.Admin.push("lead");
                if (!_.contains(GM.Global.Admin, "tradeshow"))
                  // Loading tradeshow if we have lead
                  GM.Global.Admin.push("tradeshow");
              }
              if (module == "physvisit") module = "physicianvisit";

              if (!_.contains(GM.Global.Admin, module))
                // Loading all accessed modules into global array without duplicates.
                GM.Global.Admin.push(module);
            }
          });
          window.localStorage.setItem("userTabs", JSON.stringify(userTabs));
          window.localStorage.setItem("userAccess", JSON.stringify(userAccess));
          window.localStorage.setItem(
            "userAdmin",
            JSON.stringify(GM.Global.Admin)
          );
          if (callback != undefined) callback();
        }
      );
    },

    pending: function () {
      GM.Global.SalesRep = isRep();
      GM.Global.CRMMain = true;
      var gmvCRMMain = new GMVCRMMain();
      gmvCRMMain.render();
      // gmvApp.showView(gmvCRMMain);
      $("#div-messagebar").show();
      mainview.getPendSync();
    },
  });

  return MainRouter;
});
