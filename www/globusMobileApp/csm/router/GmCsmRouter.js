/**********************************************************************************
 * File: GmCsmRouter.js
 * Description:Router file for CSM app
 * Author:Sajin RS
 **********************************************************************************/

define([
  'jquery', 'underscore', 'backbone', 'handlebars',
  'global', 'device'
],

  function ($, _, Backbone, Handlebars, Global, Device) {

    'use strict';

    var GmCsmRouter = Backbone.Router.extend({
      routes: {
        "csm": "csm",
      },

      events: {
        "click .btn-lnk-csm": "csmHome"
      },


      csm: function () {
        $("#div-inv-title-main").css("display", "none");
        $("#div-inv-main-content").html('<object data="globusMobileApp/csm/index.html" style="width:101%;height:100%;" />');
        $(".btn-app-link").fadeToggle("fast");
        $(".btn-app-link").css("display", "none");
      },


    });

    return GmCsmRouter;
  });
