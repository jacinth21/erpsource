/**********************************************************************************
 * File:        GmMsgModel.js
 * Description: Message Model
 * Version:     1.0
 * Author:      praja
**********************************************************************************/

define([
    'underscore',
    'backbone',
],

function (_, Backbone) {

	'use strict';

	var Message = Backbone.Model.extend({

		// Default values of the model
		defaults : {
			ID : "Default ID",
			Lang: "Default Lang",
			Message : "No Message to display",
			Reason : "No Reason to show",
			Action : "No Action to show"
		}
	});	

	return Message;
});
