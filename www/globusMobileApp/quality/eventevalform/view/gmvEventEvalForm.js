    define([
    'jquery', 'underscore', 'backbone', 'handlebars', 'global', 'gmvSearch', 'gmvValidation', 'gmmActivity', 'gmtTemplate', 'gmvCRMSelectPopup', 'loaderview', 'commonutil','searchview','dropdownview'
       ],

    	function ($, _, Backbone, Handlebars, Global, GMVSearch, GMVValidation, GMMActivity, GMTTemplate, GMVCRMSelectPopup, LoaderView, CommonUtil,SearchView,DropDownView) {

    		'use strict';


    		GM.View.FormDetail = Backbone.View.extend({

    			el: "#div-main",

    			events: {
    				'change #form-lookup-part input': 'noResult',
    				'click #form-back': "backwardForm",
    				'click .btn-listby': "openPopup",
    				'click #btn-sign-submit': "submit",
    				'click #btn-main-save': "saveForm",
    				'click #create_surg': "Submit",
    				'click #btn-main-clear': "clearForm",
    				'click #form-lookup-part input': "lineExt",
    				'change #event-list, #adverse-list, #carrier-list': "removeSelect",
					'click .fa-calendar, #eventEval-original-dt label,#eventEval-event-dt label,#eventEval-shipped-dt label' : "showDatepicker",
			        "change #date-shipped,#evnt-date,#original-date" : "getFormattedDate",
    			},

    			name: "Form View",

    			//preloading developer's templates
 		
    			template_formLoad: fnGetTemplate(URL_Form_Template, "gmtEventEvalForm"),
    			
				
				
    			initialize: function (options) {
    				GM.Global["Form"] = options;
                    
    				GM.Global.Form.FilterID = "{}";
    				GM.Global.Form.FilterName = "{}";
    			},


    			render: function () {
    				var that = this;
                    this.errors = "";
                    if(Object.keys(GM.Global.Form.data).length>0){
                        var evFrmDt = GM.Global.Form.data[0];
                    }
                    else{
                         var evFrmDt = '';
                    }     
                    if (GM.Global.Form.FilterID == undefined)
    					GM.Global.Form.FilterID = "{}";
    				if (GM.Global.Form.FilterName == undefined)
    					GM.Global.Form.FilterName = "{}";
                    $(".gmPanelHead").addClass("gmPanelFormCustom");                    
                    $("#div-app-title").show();
                    $("#div-app-title").html("Evaluation Form");
                    $("#div-app-title").removeClass("gmVisibilityHidden");
                    $("#btn-main-void span").text("Clear");
                    $("#btn-back").show();
                    $("#btn-home").show();
                    $(".search-select-options").hide();
                    $('.datepick').datepicker();
					that.setFormValues(evFrmDt);
    			},
                
                setFormValues: function (formData) {
                        var that = this;
                        this.$el.html(this.template_formLoad(formData));
                        this.searchPart();
//                        $('.datepicker').datepicker();
                        $("#btn-eval-form").addClass("btn-selected");
                        if (formData != '') {
                            $("#eval-form-id").val(formData.evalformid);
                            $("#eval-part-id").val(formData.partid);
                            $("#eval-part-id").data("partnm", formData.partnm);
                            $("#form-lookup-part input").val(formData.partnm);

                            that.showDropDown("#report-condition", formData.rptcond);
                            that.showDropDown("#revision-surgery", formData.revsurg);
                            that.showDropDown("#port-oprt", formData.postoprt);
                            that.showDropDown("#prc-outcome", formData.prcout);
                            that.showDropDown("#replacement", formData.replacement);
                            that.showDropDown("#part-available", formData.partaval);
                            that.showDropDown("#shipped", formData.shipped);
                            that.showDropDown("#decontaminated", formData.decontaminated);
                            that.showDropDown("#field-service", formData.fldservice);
                            that.getcdLkupVal("#event-list", "#evnt-list", "EVTDUR", "", formData.evnthappen);
                            that.getcdLkupVal("#adverse-list", "#adv-list", "EVTADV", "", formData.advefct);
                            that.getcdLkupVal("#carrier-list", "#crr-list", "DCAR", "107443", formData.carrier);

                        } else {
                            that.showDropDown("#report-condition", "");
                            that.showDropDown("#revision-surgery", "");
                            that.showDropDown("#port-oprt", "");
                            that.showDropDown("#prc-outcome", "");
                            that.showDropDown("#replacement", "");
                            that.showDropDown("#part-available", "");
                            that.showDropDown("#shipped", "");
                            that.showDropDown("#decontaminated", "");
                            that.showDropDown("#field-service", "");
                            that.getcdLkupVal("#event-list", "#evnt-list", "EVTDUR", "", "0");
                            that.getcdLkupVal("#adverse-list", "#adv-list", "EVTADV", "", "0");
                            that.getcdLkupVal("#carrier-list", "#crr-list", "DCAR", "107443", "0");
                        }
                        that.dropDownSelect("#report-condition", "#rpt-cond");
                        that.dropDownSelect("#revision-surgery", "#rev-surg");
                        that.dropDownSelect("#port-oprt", "#post-oprt");
                        that.dropDownSelect("#prc-outcome", "#prc-out");
                        that.dropDownSelect("#replacement", "#replac");
                        that.dropDownSelect("#part-available", "#part-aval");
                        that.dropDownSelect("#shipped", "#ship");
                        that.dropDownSelect("#decontaminated", "#decont");
                        that.dropDownSelect("#field-service", "#fld-ser");

                        if (formData != '')
                            that.typeSaleFunc(formData);
                        $("#btn-case-link").unbind('click').bind('click', function () {
                            that.openLink();
                        });
                    },

                    dropDownSelect: function (renderDiv, hiddenDiv) {
                        var that = this;
                        that.$(renderDiv).bind('onchange', function (e, selectedId, SelectedName) {
                            $(hiddenDiv).val(selectedId);
                        });
                    },
				
				showDropDown: function (divElem, defaultVal) {
				    var that = this;
                    if(divElem == "#prc-outcome"){
                        var statusType = {
                        "arrStatusType": [{
                                "ID": "",
                                "Name": "Choose One"
                            },
                            {
                                "ID": "105304",
                                "Name": "Completed"
                            },
                            {
                                "ID": "105305",
                                "Name": "Aborted"
                            }
                        ]};
                    }
                    else{
                        var statusType = {
                        "arrStatusType": [{
                                "ID": "",
                                "Name": "Choose One"
                            },
                            {
                                "ID": "105304",
                                "Name": "Yes"
                            },
                            {
                                "ID": "105305",
                                "Name": "No"
                            }
                        ]};
                    }
                    

				    this.statusDropDownView = new DropDownView({
				        el: this.$(divElem),
				        data: statusType.arrStatusType,
				        DefaultID: defaultVal
				    });

				},
				
                showDatepicker: function (event) {
                    var elem = event.currentTarget;
                    $($(elem).parent().find("input")).datepicker({
                        maxDate: 0,
                        duration: 'fast'
                    });
                    var visible = $('#ui-datepicker-div').is(':visible');
                    $($(elem).parent().find("input")).datepicker(visible ? 'hide' : 'show');

                    event.stopPropagation();
                     $(window).click(function (event) {
                        if (document.getElementById("ui-datepicker-div").style.display != "none") {
                            var elem = event.target;
                            if (!($(elem).hasClass("ui-datepicker-prev") || $(elem).hasClass("ui-datepicker-next") || $(elem).parent().hasClass("ui-datepicker-prev") || $(elem).parent().hasClass("ui-datepicker-next"))) {
                                var visible = $('#ui-datepicker-div').is(':visible');
                                $($(elem).parent().find("input")).datepicker(visible ? 'hide' : 'show');
                            }
                        }
                    });
                },
                
                getFormattedDate: function(event){
                    var that = this;
                    var elem = event.currentTarget;
                    this.formatDate = $(elem).val();
                    var fmtEventDate = formattedDate(this.formatDate, localStorage.getItem("cmpdfmt"));
                    this.$(elem).parent().find("label").html(fmtEventDate);
                },

				 
				openLink: function() {
						$("#btn-case-link").each(function(){	
						$(this).toggleClass("show-app");
						});
						$(".btn-app-link").toggle("fast");
				},


				
    			removeSelect: function (e) {
    				var that = this;

    				var divID = "#" + e.currentTarget.id;
    				var SelectVal = $(e.currentTarget).val();
					
    				var codeLkupVal = "";
    				var accsCode = "";
					 
    				if (divID == '#carrier-list') {
    					codeLkupVal = "DCAR";
    					accsCode = "107443";
    				} else if (divID == '#event-list')
    					codeLkupVal = "EVTDUR";
    				else if (divID == '#adverse-list')
    					codeLkupVal = "EVTADV";
    				else
    					codeLkupVal = "";

    				getCLValToDiv(divID, "", "", codeLkupVal, "", accsCode, "", "list", false, false, function (divID, attrVal) {
						if (SelectVal != "")
							that.setSelect(divID, SelectVal);
    				});
    			},


    			
    			searchPart: function () {

    			    var that = this;
    			    var fnName = "";

    			    var renderOptions = {
    			        "el": this.$('#form-lookup-part'),
    			        "ID": "txt-keyword",
    			        "Name": "txt-search-name",
    			        "Class": "txt-keyword-initial",
    			        "minChar": "3",
    			        "autoSearch": true,
    			        "placeholder": "Key in at least 3 characters to search",
    			        "fnName": fnGetDOParts,
    			        "usage": 1,
    			        "template": 'GmSearchResult',
    			        "template_URL": URL_Common_Template,
    			        "callback": function (pList) {
    			            $("#form-lookup-part input").val(pList.Name);
    			            $("#eval-part-id").val(pList.ID);
    			            $("#eval-part-id").attr("data-partnm", pList.Name);
    			        }
    			    };

    			    var searchview = new SearchView(renderOptions);
    			},

    			// Open Single / Multi Select box        
    			openPopup: function (event) {
    			    var that = this;
    			    var fnName = "";


    			    that.systemSelectOptions = {
    			        'title': $(event.currentTarget).attr("data-title"),
    			        'storagekey': $(event.currentTarget).attr("data-storagekey"),
    			        'webservice': $(event.currentTarget).attr("data-webservice"),
    			        'resultVO': $(event.currentTarget).attr("data-resultVO"),
    			        'IDParameter': $(event.currentTarget).attr("data-IDParameter"),
    			        'NameParameter': $(event.currentTarget).attr("data-NameParameter"),
    			        'inputParameters': $(event.currentTarget).attr("data-inputParameters"),
    			        'codegrp': $(event.currentTarget).attr("data-codegrp"),
    			        'sortby': $(event.currentTarget).attr("data-sortby"),
    			        'module': this.module,
    			        'renderOne': $(event.currentTarget).attr("redata"),
    			        'event': event,
    			        'parent': this,
    			        'moduleLS': GM.Global.Form.FilterID,
    			        'moduleCLS': GM.Global.Form.FilterName,
    			        'fnName': fnName,
    			        'callback': function (ID, Name) {
    			            GM.Global.Form.FilterID = ID;
    			            GM.Global.Form.FilterName = Name;
    			            if (Object.keys(JSON.parse(GM.Global.Form.FilterID)).length > 0)
    			                $("#tos").text(JSON.parse(GM.Global.Form.FilterName)["Type of Sale"]);
    			            else
    			                $("#tos").text("Choose One");
    			            $(".evalpopup-icon span").text("");


    			        }
    			    };

    			    that.popupSelectView = new GMVCRMSelectPopup(that.systemSelectOptions);
    			    $("#gmBorderRadius3p").removeClass("gmBtn");
    			    $("#gmBorderRadius3p").addClass("evalbtn");

    			},
				
				getcdLkupVal: function (renderDiv, hiddenDiv, codeGrp, codeaccid, defaultData) {
				    var that = this;
				    if (defaultData != '' || null) {
				        var defaultid = defaultData;
				    } else {
				        var defaultid = 0;
				    }
                    var input = {
                        "token": localStorage.getItem("token"),
                        "codegrp": codeGrp
                    };
                    if(codeaccid != ""){
                        input.codeaccid = codeaccid;
                    }
				    
				    fnGetWebServerData("search/codelookup", "gmCodeLookUpVO", input, function (data) {
				        data = $.parseJSON(JSON.stringify(data).replace(/codeid/g, 'ID').replace(/codenm/g, 'Name'));

				        data.push({
				            "ID": 0,
				            "Name": "Choose One"
				        });
				        that.dropdownview = new DropDownView({
				            el: that.$(renderDiv),
				            data: data,
				            DefaultID: defaultid
				        });

				        that.$(renderDiv).bind('onchange', function (e, selectedId, SelectedName) {
				            $(hiddenDiv).val(selectedId);
				            $(hiddenDiv).trigger("change");
				        });
				    });
				},




    			saveForm: function () {
    				var that = this;
                    var strOpt= "draft";
    				var userID = localStorage.getItem("userID");
					var compdtfmt = localStorage.getItem("cmpdfmt");
					var currentDate = new Date();
					var strDate = $.datepicker.formatDate('mm/dd/yy', currentDate);
					var eventdate = $("#evnt-date").val();
					var originaldate = $("#original-date").val();
					var shippeddate = $("#date-shipped").val();
						eventdate = formattedDate(eventdate,compdtfmt);
					 	originaldate = formattedDate(originaldate,compdtfmt);
					 	shippeddate = formattedDate(shippeddate,compdtfmt);
					
					
    				if (Object.keys(JSON.parse(GM.Global.Form.FilterID)).length > 0) {

    					var filterID = JSON.parse(GM.Global.Form.FilterID);
    					var filterName = JSON.parse(GM.Global.Form.FilterName);

    					var res = filterID.codeid.split(",");

    				} else
    					var res = {};
						
    					var input = {};						
                        if ($("#form-lookup-part input").val() == $("#eval-part-id").attr("data-partnm")){
    			        input.partid = $("#eval-part-id").val();
                        input.partnm = $("#eval-part-id").attr("data-partnm");
                        }
    			        else{
    			        input.partid = "";
    			        input.partnm = "";
                        }
						input.lotnumber = $("#lot-number").val();
    					input.qty = $("#qty").val();
    					input.consignmentfl = "";
    					input.loanerfl = "";
    					input.soldfl = "";
    					input.stropt = strOpt;

    					if (res.length > 0) {
    						_.each(res, function (fid) {
    							
    							if (fid == '107440')
    								input.consignmentfl = "Y";
    							else if (fid == '107441')
    								input.loanerfl = "Y";
    							else if (fid == '107442')
    								input.soldfl = "Y";
    							else
    								input.others = fid;

    						});
    					}

    					input.descr = $("#desc").val();
    					input.evntdt = eventdate;
						input.evnthappen = $("#evnt-list").val();
    					input.rptcond = $("#rpt-cond").val();
						input.revsurg = $("#rev-surg").val();
    					input.revsurgtxt = $("#rev-surgery-txt").val();
    					input.advefct = $("#adv-list").val();
    					input.typofsurg = $("#type-of-surgery").val();
    					input.surg = $("#surgery").val();
    					input.hospital = $("#hospital").val();
    					input.postoprt = $("#post-oprt").val();
    					input.orgdate = originaldate;
    					input.prcout = $("#prc-out").val();
    					input.replacement = $("#replac").val();
    					input.partaval = $("#part-aval").val();
    					input.shipped = $("#ship").val();
    					input.decontaminated = $("#decont").val();
    					input.dateshipped = shippeddate;
    					input.carrier = $("#crr-list").val();
    					input.trknumber = $("#track-number").val();
    					input.fldservice = $("#fld-ser").val();
    					input.repid = userID;
						input.cmpdfmt = compdtfmt;
    					input.evalformid = $("#eval-form-id").val();
                    
    					window.scrollTo(0,0);
                    
                    this.validateFields(input);
                    
    			    if (this.errors == "") {
						getLoader("#div-main");	
					  console.log("save Input " + JSON.stringify(input));
    					fnGetWebServerData('form/save', 'gmEvalFormVO', input, function (data) {
    						console.log("save Output " + JSON.stringify(data));
    						if (data != null) {
    							showSuccess("Form details saved successfully");
      							window.location.href = "#eval/dash";

    						}  else {
                                showError("Form details save failed, Please try again"); 
                                that.setFormValues(input);
							}
    					});
						
    				} else {
    			        showError(this.errors);				
    			    }
    			},


    			submit: function () {
    			    var that = this;
                    var strOpt= "pdf";
    			    var userID = localStorage.getItem("userID");
    			    var compdtfmt = localStorage.getItem("cmpdfmt");
    			    var eventdate = $("#evnt-date").val();
    			    var originaldate = $("#original-date").val();
    			    var shippeddate = $("#date-shipped").val();
    			    eventdate = formattedDate(eventdate, compdtfmt);
    			    originaldate = formattedDate(originaldate, compdtfmt);
    			    shippeddate = formattedDate(shippeddate, compdtfmt);

    			    if (Object.keys(JSON.parse(GM.Global.Form.FilterID)).length > 0) {

    			        var filterID = JSON.parse(GM.Global.Form.FilterID);
    			        var filterName = JSON.parse(GM.Global.Form.FilterName);


    			        var res = filterID.codeid.split(",");

    			    } else
    			        var res = {};

    			    var input = {};
    			    if ($("#form-lookup-part input").val() == $("#eval-part-id").attr("data-partnm")){
    			        input.partid = $("#eval-part-id").val();
                        input.partnm = $("#eval-part-id").attr("data-partnm");
                    }
    			    else{
    			        input.partid = "";
    			        input.partnm = "";
                    }
    			    input.lotnumber = $("#lot-number").val();
    			    input.qty = $("#qty").val();

    			    input.consignmentfl = "";
    			    input.loanerfl = "";
    			    input.soldfl = "";
                    input.stropt = strOpt;

    			    if (res.length > 0) {
    			        _.each(res, function (fid) {

    			            if (fid == '107440')
    			                input.consignmentfl = "Y";
    			            else if (fid == '107441')
    			                input.loanerfl = "Y";
    			            else if (fid == '107442')
    			                input.soldfl = "Y";
    			            else
    			                input.others = fid;

    			        });
    			    }



    			    input.descr = $("#desc").val();
    			    input.evntdt = eventdate;
    			    input.evnthappen = $("#evnt-list").val();
    			    input.rptcond = $("#rpt-cond").val();
    			    input.revsurg = $("#rev-surg").val();
    			    input.revsurgtxt = $("#rev-surgery-txt").val();
    			    input.advefct = $("#adv-list").val();
    			    input.typofsurg = $("#type-of-surgery").val();
    			    input.surg = $("#surgery").val();
    			    input.hospital = $("#hospital").val();
    			    input.postoprt = $("#post-oprt").val();
    			    input.orgdate = originaldate;
    			    input.prcout = $("#prc-out").val();
    			    input.replacement = $("#replac").val();
    			    input.partaval = $("#part-aval").val();
    			    input.shipped = $("#ship").val();
    			    input.decontaminated = $("#decont").val();
    			    input.dateshipped = shippeddate;
    			    input.carrier = $("#crr-list").val();
    			    input.trknumber = $("#track-number").val();
    			    input.fldservice = $("#fld-ser").val();
    			    input.repid = userID;
    			    input.cmpdfmt = compdtfmt;
    			    input.evalformid = $("#eval-form-id").val();

                    this.validateFields(input);
                    
    			    if (this.errors == "") {
    			        getLoader("#div-main");
    			        console.log("Submit Input >>>>>>>>>" + JSON.stringify(input));
    			        fnGetWebServerData('form/save', 'gmEvalFormVO', input, function (data) {
    			            console.log("Submit Output " + JSON.stringify(data));
    			            if (data != null) {
    			                showSuccess("Form details submitted successfully");
    			                if (data.mailstatus == "fail") {
                                    that.setFormValues(data);
                                    var err = {"message" : "Email failed to send"};
                                    showAppError(err, "gmvEventEvalForm:submit() form id :: "+ data.evalformid);
    			                } else {
                                    showSuccess("Email sent successfully");
    			                    window.location.href = "#eval/dash";
    			                }
    			            } else {
    			                showError("Form details submitted failed, please try again");
    			                that.setFormValues(input);
    			            }
    			        });
    			    } else {
    			        showError(this.errors);		
    			    }
    			    window.scrollTo(0, 0);

    			},

                validateFields: function(input){
                    var that = this;
                    this.errors = "";
                    
                    if($("#desc").val().length >= 1200){
                       this.errors += "Detailed Description cannot be more than 1200 characters\n";
                       $("#desc").addClass("gmValidationError");
                    }
                    
    			    var eventdate = $("#evnt-date").val();
    			    var originaldate = $("#original-date").val();
    			    var shippeddate = $("#date-shipped").val();
    			    eventdate = formattedDate(eventdate, 'mm/dd/yyyy');
    			    originaldate = formattedDate(originaldate, 'mm/dd/yyyy');
    			    shippeddate = formattedDate(shippeddate, 'mm/dd/yyyy');

                    
                    var currentDate = new Date();
                    var strDate = $.datepicker.formatDate('mm/dd/yy', currentDate);
                    
                    $("#desc").removeClass("gmValidationError");
                    $("#event-list").removeClass("gmValidationError");
                    $("#revision-surgery").removeClass("gmValidationError");
                    $("#adverse-list").removeClass("gmValidationError");
                    
                    if($("#desc").val().trim() == ""){
                        this.errors += "Input is required in Detailed Description of Event\n";
                        $("#desc").addClass("gmValidationError");
                    }
                    if (input.evnthappen == "" || input.evnthappen == "0") {
                        this.errors += "Selection Did this Event Happen During is required\n";
                        $("#event-list").addClass("gmValidationError");
                    }
                    if (input.revsurg == "" || input.revsurg == "0") {
                        this.errors += "Selection for Is this a revision surgery? is required\n";
                        $("#revision-surgery").addClass("gmValidationError");
                    }
                    if (input.advefct == "" || input.advefct == "0") {
                        this.errors += "Selection for Any adverse effect to patient? is required\n";
                        $("#adverse-list").addClass("gmValidationError");
                    }
                    if (eventdate > strDate) {
                        this.errors += "Event Date cannot be a future date, should be less than or equal to today\n";
                    }
                    if (originaldate > strDate) {
                        this.errors += "Original Date cannot be a future date, should be less than or equal to today\n";
                    }
                    if (shippeddate > strDate) {
                        this.errors += "Date Shipped cannot be a future date, should be less than or equal to today\n";
                    }
                    if (($("#rev-surg").val() == '105304') && ($("#rev-surgery-txt").val().trim() == "")) {
                        this.errors += "An explanation is required for a Revision Surgery\n";
                    }
                    if ($("#qty").val() == '0') {
                        this.errors += "Please enter a number greater than 0 in Qty\n";
                    }
                    
                },

    			setSelect: function (controlid, selectedvalue) {
    				$(controlid).val(selectedvalue);
    			},


                typeSaleFunc: function(data){
                        var tosID=[];
						var tosName=[];
						if(data.consignmentfl == "Y"){
							tosID.push(107440);
							tosName.push("Consignment");
						}
						if(data.loanerfl == "Y"){
							tosID.push(107441);
							tosName.push("Loaner");
						}
						if(data.soldfl == "Y"){
							tosID.push(107442);
							tosName.push("Sold");
						}
						console.log(tosName + tosID);
                        if(tosID.length>0){
							GM.Global.Form.FilterID = {"codeid":tosID.toString()};
							GM.Global.Form.FilterName = {"Type Of Sale":tosName.join(" / ")};
							
							
							GM.Global.Form.FilterID = JSON.stringify(GM.Global.Form.FilterID);
							GM.Global.Form.FilterName = JSON.stringify(GM.Global.Form.FilterName);
							
							
							$("#tos").text(JSON.parse(GM.Global.Form.FilterName)["Type Of Sale"]);
							$(".evalpopup-icon span").text("");
							
						}
                },

    			clearForm: function (data) {
					var that = this;
    				
    				dhtmlx.confirm({
    					title: "Delete Info",
    					ok: "Yes",
    					cancel: "No",
    					type: "confirm",
    					text: "Are you sure you want to Clear this form? ",
    					callback: function (result) {
    						if (result) {    						
						      GM.Global.Form.FilterID = "{}";
    					      GM.Global.Form.FilterName = "{}";
                              GM.Global.Form.data=[];  
                              that.render();                       
							}
    					}
    				});

    			},




    		});

    		return GM.View.FormDetail;
    	});
