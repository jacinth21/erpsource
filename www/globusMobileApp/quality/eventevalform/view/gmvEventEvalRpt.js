    define([
    'jquery', 'underscore', 'backbone', 'handlebars', 'global', 'gmvSearch', 'gmvValidation', 'gmmActivity', 'gmtTemplate', 'gmvCRMSelectPopup', 'loaderview',  'commonutil', 'dropdownview'
   ],

    	function ($, _, Backbone, Handlebars, Global, GMVSearch, GMVValidation, GMMActivity, GMTTemplate, GMVCRMSelectPopup, LoaderView,CommonUtil,DropDownView) {

    		'use strict';


    		GM.View.DashDetail = Backbone.View.extend({

    			el: "#div-main",

    			events: {
    				'change #form-lookup-part input': 'noResult',
    				'change #div-poc-rep-name-search input': 'noResult',
    				'click #form-back': "backwardForm",
    				'click #btn-main-load': "load",
    				'click .fetch-form': "fetchForm",
    				'click #btn-main-void': "deleteForm",
    				'click #view-pdf': "viewDocument",
    				'click #form-lookup-part input': "lineExt",
					'click .fa-calendar, .evalFrmDt label, .evalToDt label' : "showDatepicker",
			        "change #from-date, #to-date" : "getFormattedDate",
					'click .removeForm' : "deleteForm",
                    'click .rptEval' : "togglerptEval"

 
    			},

    			name: "Dash View",

    			//preloading developer's templates
    			template_mainmodule: fnGetTemplate(URL_Common_Template, "gmtCRMMainModule"),
    			template_dashLoad: fnGetTemplate(URL_Form_Template, "gmtEventEvalDash"),

    			initialize: function (options) {
    				GM.Global["Dash"] = options;
					 
    			},


    			render: function () {
    				var that = this;

    				var currentDate = new Date();
					var compdtfmt = localStorage.getItem("cmpdfmt");
					var strDate = $.datepicker.formatDate('mm/dd/yy', currentDate);
					var date = new Date();
					var FirstDay = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);
					var monthday = $.datepicker.formatDate('mm/dd/yy', FirstDay);
					$("#div-main").html(this.template_dashLoad());
					$('.datepick').datepicker();
					$("#div-app-title").show().html("Dashboard");
                    $('#div-app-title').removeClass('gmVisibilityHidden').addClass('gmVisibilityVisible');
    				$("#from-date").val(monthday);
					$("#to-date").val(strDate);
					$(".gmPanelHead").addClass("panelFormCustom");
    				$("#btn-main-void span").text("Clear");
					this.load();
					$("#btn-eval-dash").addClass("btn-selected");
					$("#btn-case-link").unbind('click').bind('click', function () {
						 that.openLink();
					 });
                     if($(window).width() <= 480){
                         $("#btn-back").addClass("evlShow");
                         $("#btn-home").addClass("evlShow");
                     }
                    

    			},
				
				showDatepicker: function (event) {
				var elem = event.currentTarget;
				$($(elem).parent().find("input")).datepicker({
					maxDate: 0,
					duration: 'fast'
				});
				var visible = $('#ui-datepicker-div').is(':visible');
				$($(elem).parent().find("input")).datepicker(visible ? 'hide' : 'show');

			event.stopPropagation();
                    $(window).click(function (event) {
                        if (document.getElementById("ui-datepicker-div").style.display != "none") {
                            var elem = event.target;
                            if (!($(elem).hasClass("ui-datepicker-prev") || $(elem).hasClass("ui-datepicker-next") || $(elem).parent().hasClass("ui-datepicker-prev") || $(elem).parent().hasClass("ui-datepicker-next"))) {
                                var visible = $('#ui-datepicker-div').is(':visible');
                                $($(elem).parent().find("input")).datepicker(visible ? 'hide' : 'show');
                            }
                        }
                    });
			},
                
                //Get formatted date to append in date field label
                getFormattedDate: function(event){
                    var that = this;
                    var elem = event.currentTarget;
                    this.formatDate = $(elem).val();
                    var fmtEventDate = formattedDate(this.formatDate, localStorage.getItem("cmpdfmt"));
                    this.$(elem).parent().find("label").html(fmtEventDate);
                },
				 
				openLink: function() {
							$("#btn-case-link").each(function(){	
							$(this).toggleClass("show-app");
							})
							$(".btn-app-link").toggle("fast");
				},

    			load: function () {

    				var that = this;
    				var userID = localStorage.getItem("userID");
					var compdtfmt = localStorage.getItem("cmpdfmt");
    				var input = {};
    				input.viodfl = $("#status").val();
                    var getFromDt = $("#from-date").val();
                    var getToDt = $("#to-date").val();
                    var defaultFormatFromDt = formattedDate(getFromDt, localStorage.getItem("cmpdfmt"));
                    var defaultFormatToDt = formattedDate(getToDt, localStorage.getItem("cmpdfmt"));
    				input.fromdate = defaultFormatFromDt;
    				input.todate = defaultFormatToDt;
    				input.repid = userID;
					
							var date1 = new Date(getFromDt);
                            var date2 = new Date(getToDt);
                            var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                            
										
					if(new Date(getToDt) < new Date(getFromDt))
					{
						showError("Please Enter Proper Date");
						
					} else if(getFromDt == '' && getToDt != '' ){
						showError("Please Enter From Date");
					} else if(getFromDt == '' && getToDt != ''){
						showError("Please Enter To Date");
					} else if(diffDays >31){
						showError("Date range cannot exceed 31 days");
					} else {
    				fnGetWebServerData('form/fetchlist', undefined, input, function (data) {
    				    if (data != null) {
    				        _.each(data, function (dt) {
    				            var text = dt.DESCR;
    				            var len = text.length;

    				            if (len > 47)
    				                dt.DESCR = text.substr(0, 47) + '...';
    				        });
                            data.reverse();
    				        that.$el.html(that.template_dashLoad(data));
                            if ($(window).width() < 768) {
                                that.swipeReportFn();
                            }
                            if(input.viodfl != null){
                                that.showDropDown("#status-list", input.viodfl);
                            }
                            else{
                                that.showDropDown("#status-list", "");
                            }
                            that.dropDownSelect("#status-list","#status");
    				        $("#from-date").val(getFromDt);
    				        $("#to-date").val(getToDt);
                            $(".evalFrmDt label").html(defaultFormatFromDt);
                            $(".evalToDt label").html(defaultFormatToDt);
    				        $("#status").val(input.viodfl);
    				        $("#btn-eval-dash").addClass("btn-selected");

                            

    				        $("#btn-case-link").unbind('click').bind('click', function () {
    				            that.openLink();
    				        });

    				    } else{
                            $(".evalListDash").html("<p>No Data Found</p>");
                            $("#btn-back").show();
                        } 
    				});
    			} },
                
                 swipeReportFn: function () {
                        $(".evalListSwipe").on('swipeleft', function (e, data) {
                            $(this).find(".li-content").addClass("onShowLi");
                            $(this).find(".delete-pu-span").addClass("onShowEl");
                        });

                        $(".evalListSwipe").on('tap', function (e, data) {
                            $(this).find(".li-content").removeClass("onShowLi");
                            $(this).find(".delete-pu-span").removeClass("onShowEl");
                        });
                },

                dropDownSelect: function(renderDiv, hiddenDiv) {
					var that = this;
					that.$(renderDiv).bind('onchange', function (e, selectedId, SelectedName) {
                        $(hiddenDiv).val(selectedId);
                    });
				},
				
				showDropDown: function (divElem, defaultVal) {
				    var that = this;
                    var statusType = {
                    "arrStatusType": [{
                            "ID": "",
                            "Name": "All"
                        },
                        {
                            "ID": "1",
                            "Name": "Draft"
                        },
                        {
                            "ID": "2",
                            "Name": "Submitted"
                        }
                    ]};

				    this.statusDropDownView = new DropDownView({
				        el: this.$(divElem),
				        data: statusType.arrStatusType,
				        DefaultID: defaultVal
				    });

				},

    			deleteForm: function (event) {
    				var that = this;
    							var userID = localStorage.getItem("userID");
    							var evalformid = $(event.currentTarget).data("formid");
    							var voidId = $(event.target).next("li").text();
    							var voidfl = $("#status").val();
					event.stopImmediatePropagation();
    				dhtmlx.confirm({
    					title: "Delete Info",
    					ok: "Yes",
    					cancel: "No",
    					type: "confirm",
    					text: "Are you sure you want to delete this form? ",
    					callback: function (result) {
    						if (result) {
    							var input = {};
								input.repid = userID;
    							input.evalformid = evalformid;
								fnGetWebServerData('form/voidlist', undefined, input, function (data) {
    								showSuccess("Form Deleted Successfully");
    								Backbone.history.loadUrl();
    							});
    						}

    					}
    				});

    			},

    			fetchForm: function (event) {
    				var that = this;
    				var userID = localStorage.getItem("userID");
    				var evalformid = $(event.currentTarget).data("formid");
    				var status = $(event.currentTarget).data("status");
										
    				if ($(event.currentTarget).data("status") != 'Submitted')
    					window.location.href = "#eval/form/" + evalformid;
    			},

				
				 viewDocument: function (event) {
					 var that = this;
					 var path="";
					 var fileID = $(event.currentTarget).data("formid");
					 var src = URL_FileServer;
					 path = src +"quality/eventEvalForm/"+ fileID+".pdf";
					  
                if (path != undefined && path != "")
                    var DOCViewer = window.open(decodeURI(path), '_blank', 'location=no,EnableViewPortScale=yes');
                    //openPdfInWindowOpener(path);
            },
                
                 togglerptEval: function(e){
                     var width = $(window).width();
                  if (width <= 480) {
                     var classname= $('#eval-main').attr('class');
                      if(classname == 'div-eval-dash-container mobrptEval'){
                        $('#eval-main').removeClass('mobrptEval');
                      }
                      else
                          $('#eval-main').addClass('mobrptEval');
                  }
                },
                



    		});

    		return GM.View.DashDetail;
    	});
