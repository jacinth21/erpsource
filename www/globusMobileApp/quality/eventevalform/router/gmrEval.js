define([

 'jquery', 'underscore', 'backbone', 'gmvCRMMain','gmvEventEvalForm','gmvEventEvalRpt'
],

	function ($, _, Backbone,
		GMVCRMMain,
		GMVEventEvalForm,GMVEventEvalRpt) {

		'use strict';

		
		GM.Router.Eval = Backbone.Router.extend({

			//redirect to the web page
			routes: {
				"eval/form": "viewForm",
				"eval/form/:id": "formDetail",
				"eval/dash": "viewDash",
				"eval/dash/id/:partid": "dashDetail",
			},

			//pre execute the function before start the transaction
			execute: function (callback, args, name) {

				if (GM.Global.CRMMain == undefined) {
					commonSetUp();
					GM.Global.Module = "Form";
					GM.Global.CRMMain = true;


				}
				$("#form").addClass("");


				if (callback) callback.apply(this, args);

			},

			//View
			viewForm: function () {

				var gmvEventEvalForm = new GMVEventEvalForm({
					"partid": 0,
					"data": {}
				});
				gmvApp.showView(gmvEventEvalForm);
                $("#btn-back").show();
			},


			//detail

			formDetail: function (evalformid) {
				if (evalformid != 0) {

					var input = {
						"token": localStorage.getItem("token"),
						"evalformid": evalformid
					};
					fnGetWebServerData("form/fetch/", 'gmEvalFormVO', input, function (data) {
						if (data != null) {
							var gmvEventEvalForm = new GMVEventEvalForm({
								"evalformid": evalformid,
								"data": data
							});
							gmvApp.showView(gmvEventEvalForm);
                            $("#btn-back").show();
						} else {
							GM.Global.AlertMsg = "No Such Form";
							window.location.href = "#eval/form";
						}
					}, true);
				} else {
					var gmvEventEvalForm = new GMVEventEvalForm({
						"evalformid": 0,
						"data": {}
					});
					gmvApp.showView(gmvEventEvalForm);
                    $("#btn-back").show();
				}
			},
				
				
					//View
			viewDash: function () {

				var gmvEventEvalRpt = new GMVEventEvalRpt({
					"partid": 0,
					"data": {}
				});
				gmvApp.showView(gmvEventEvalRpt);
			},


			//detail

			dashDetail: function (partid) {
				if (partid != 0) {

					var input = {
						"token": localStorage.getItem("token"),
						"partid": partid
					};
					fnGetWebServerData("form/info/", 'gmEvalFormVO', input, function (data) {
						if (data != null) {
							var gmvEventEvalForm = new GMVEventEvalRpt({
								"partyid": partid,
								"data": data
							});
							gmvApp.showView(gmvEventEvalRpt);
						} else {
							GM.Global.AlertMsg = "No Such Form";
							window.location.href = "#crm/dash";

						}
					}, true);
				} else {
					var gmvEventEvalRpt = new GMVEventEvalRpt({
						"partid": 0,
						"data": {}
					});
					gmvApp.showView(gmvEventEvalRpt);
				}
			}
		});
		return GM.Router.Eval;

	});
