/**********************************************************************************
 * File:        GmFileSync.js
 * Description: Functions for Accessing the FileSystem in the Device
 * Version:     1.0
 **********************************************************************************/

var FilesDownloaded = 0,
    Failed = 0,
    SuccessCnt = 0,
    FileDownloadFailed = 0;

function fnGetLocalFileSystemPath(callback) {
    FilesDownloaded = 0;
    Failed = 0;
    SuccessCnt = 0;
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        fileSystem.root.getDirectory("Globus", {
            create: true
        }, function (d) {

            callback(decodeURI(d.toURL()));
        }, onError);
    }, null);
}

function fnGetImage(URL, callback) {
    window.resolveLocalFileSystemURL(encodeURI(URL), callback, function (code) {
        callback("error");
    });
}

//To get the File from the Document directory
function fnGetFileFromDocumentDir(filename, callback) {
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        fileSystem.root.getFile(filename, {
            create: true
        }, function (d) {
            callback(decodeURI(d.toURL()));
        }, onError);
    }, null);
}

// PC-4132 - files download from azure
function fnDownload(Source, Destination, callback, secondCallback, failCallback) {

    var ft = new FileTransfer();
    ft.onprogress = function (progressEvent) {
        if (progressEvent.lengthComputable) {
            if (callback != undefined) {
                callback(progressEvent.loaded / progressEvent.total);
                if ((progressEvent.loaded / progressEvent.total) == 1) {
                    secondCallback(FilesDownloaded, Failed);
                }
            } else {
                if ((progressEvent.loaded / progressEvent.total) == 1) {
                    downloadSuccessful();
                }
            }
        } else {
            console.log("download failed! ");
        }
    };

    // if ((FileDownloadFailed == 1) && (localStorage.getItem('egnytefl') == "YES")) {
    //     if(Source.indexOf(URL_Cloud_FileServer) > -1) {
    //         Source = Source.replace(URL_Cloud_FileServer,URL_FileServer_Collateral);
    //     } 
    // }
    
    var newSource = encodeURI(Source);
    var TokenCheck = _.contains(newSource, AzureSASToken);
    console.log("TokenCheck >>>>>>." + TokenCheck);
    if (!TokenCheck) {
        newSource = newSource+AzureSASToken;
    }

    Source = newSource;
    Destination = encodeURI(Destination);
    console.log("New Source >>>>>>." + Source);
    console.log("Destination >>>>>>." + Destination);

    if (failCallback != undefined) {
        ft.download(Source, Destination, function () {
            console.log("download complete!");
        }, failCallback, true);
    } else {
        ft.download(Source, Destination, function (a) {
            console.log("download complete!");
            FilesDownloaded += 1;
            console.log("FilesDownloaded" + FilesDownloaded)
            if (FileDownloadFailed == 1){
                Failed--;
            }
            FileDownloadFailed = 0;
        }, function(error) {
            FileDownloadFailed += 1;
            if (FileDownloadFailed == 1) {
                Failed++;
            }
        // if ((FileDownloadFailed == 1) && (localStorage.getItem('egnytefl') == "YES")) {
        //     fnDownload(Source, Destination, callback, secondCallback, failCallback);
        // }
        }, true);
    }
}

function downloadSuccessful(e) {
    SuccessCnt += 1;
    showMsgView("030", SuccessCnt.toString());
}

function downloadFailed() {
    Failed++;
    FileDownloadFailed += 1;
}

function onError(e) {
    showMsgView("032");
    console.log("Error Retrieving Device Memory: " + JSON.stringify(e));
}

function deleteFolder() {
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        fileSystem.root.getDirectory("Globus/System/300.121", {}, function (d) {
            alert(d.toURL());
            d.removeRecursively(function () {
                alert('deleted')
            }, onError);
        }, onError);
    }, null);
}

function fnGetDirectory(directory, callback) {
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        fileSystem.root.getDirectory(directory, {}, function (d) {
            callback(d.toURL())
        }, function () {
            callback("error")
        });
    }, null);
}

function fnGetLocalFileSystem(callback) {
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        fileSystem.root.getDirectory("Globus", {
            create: true
        }, function (entry) {
            callback(entry);
        }, onError);
    }, null);
}

function fnDeleteDirectory(folderEntry, directoryPath, callback) {
    folderEntry.getDirectory(directoryPath, {}, function (entry) {
        console.log(entry.toURL());
        entry.removeRecursively(function () {
            callback('success');
        }, function () {
            callback('fail');
        });
    }, function () {
        callback('success');
    });
}

function fnRemoveFiles(callback) {
    var that = this;
    if (!GM.Global.Browser) {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
            fileSystem.root.getDirectory("Globus", {
                create: true
            }, function (entry) {
                console.log("Found / Created Globus directory");
                                fileSystem.root.getDirectory("Globus/System", {
                    create: true
                }, function (entry) {
                    console.log("Found / Created Globus/System");
                                            entry.removeRecursively(function () {
                        console.log("Removed Globus/System directory");
                                                        fileSystem.root.getDirectory("Globus/Group", {
                            create: true
                        }, function (entry) {
                            console.log("Found / Created Globus/Group");
                                                                    entry.removeRecursively(function () {
                                console.log("Removed Globus/Group directory");
                                                                                fileSystem.root.getDirectory("Globus/Set", {
                                    create: true
                                }, function (entry) {
                                    console.log("Found / Created Globus/Set");
                                                                                            entry.removeRecursively(function () {
                                        console.log("Removed Globus/Set directory");
                                        fileSystem.root.getDirectory("Globus/103112", {
                                            create: true
                                        }, function (entry) {
                                            console.log("Found / Created Globus/103112");
                                            entry.removeRecursively(function () {
                                                console.log("Removed Globus/103112 directory");
                                                fileSystem.root.getDirectory("Globus/107994", {
                                                    create: true
                                                }, function (entry) {
                                                    console.log("Found / Created Globus/107994");
                                                    entry.removeRecursively(function () {
                                                        console.log("Removed Globus/107994 directory");
                                                        fileSystem.root.getDirectory("Globus/Part Number", {
                                                            create: true
                                                        }, function (entry) {
                                                            console.log("Found / Created Globus/Part Number");
                                                            entry.removeRecursively(callback, that.onRemError("Part Number"));
                                                            console.log("Removed Globus/Part Number directory");
                                                        }, that.onDirError("Part Number"));
                                                    }, that.onRemError("107994"));
                                                }, that.onDirError("107994"));
                                            }, that.onRemError("103112"));
                                        }, that.onDirError("103112"));
                                                                                               
                                    }, that.onRemError("Set"));
                                                                                   
                                }, that.onDirError("Set"));
                                                                       
                            }, that.onRemError("Group"));
                                                           
                        }, that.onDirError("Group"));
                                               
                    }, that.onRemError("System"));
                                   
                }, that.onDirError("System"));
            }, onDirError("Globus"));
                   
        }, null);
    } else {
        $("#msg-overlay").fadeOut("fast");
        $("#div-confirm-reset").fadeOut("fast");
        showMsgView("028");
    }
         
}

function onDirError(DirName) {
    showMsgView("054");
    console.log("Cannot find directory - " + DirName);
}

function onRemError(DirName) {
    showMsgView("055");
    console.log("Cannot remove directory - " + DirName);
}

function fnRemoveFile(URL) {
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        fileSystem.root.getFile("Globus/" + URL, {}, function (entry) {
            entry.remove(null, onError)
        }, function () {
            console.log('error');
        });
    }, null);
}

function fnUnzipp(Source, Destination, callback) {
    fnGetLocalFileSystemPath(function (fileSystem) {
        zip.unzip(encodeURI(Source), encodeURI(Destination), function (d) {
            // alert('success');            
            callback(encodeURI(Destination));
        }, []);
    });

}

function fnResolvePath(URL, callback) {
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        fileSystem.root.getDirectory("Globus", {
            create: true
        }, function (d) {
            var ParentURL = decodeURI(d.toURL());
            window.resolveLocalFileSystemURI(encodeURI(ParentURL + URL), function (entry) {
                callback(1, decodeURI(entry.toURL()));
            }, function (error) {
                callback(0, ParentURL);
            });
        }, onError);
    }, null);

}

function createPricingPDFPath(callback) {
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        fileSystem.root.getDirectory("Globus", {
            create: true
        }, function (d) {
            console.log(d.toURL());
            d.getDirectory("PRICING", {
                create: true
            }, function (wp) {
                console.log(wp.toURL());
                wp.getDirectory("ImpactPDF", {
                    create: true
                }, function (wop) {
                    console.log(wp.toURL());
                    callback(decodeURI(wop.toURL()));
                }, onError);

            }, onError);
        }, onError);
    }, null);
}

function createPDFPath(callback) {
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        fileSystem.root.getDirectory("Globus", {
            create: true
        }, function (d) {
            console.log(d.toURL());
            d.getDirectory("DO", {
                create: true
            }, function (wp) {
                console.log(wp.toURL());
                wp.getDirectory("PDF", {
                    create: true
                }, function (wop) {
                    wop.getDirectory("WP", {
                        create: true
                    }, function (k) {
                        // callback(decodeURI(k.toURL()));
                    }, onError);
                    wop.getDirectory("WOP", {
                        create: true
                    }, function (k) {
                        callback(decodeURI(k.toURL()));
                    }, onError);
                }, onError);

            }, onError);
        }, onError);
    }, null);
}
//Uploads the DO PDF to Server using Cordova Plugin File Transfer 
//FileTransfer -> cordova plugin for fileupload
function uploadPDF(FileName, callback) {
    fnGetLocalFileSystemPath(function (URL) {
        var ft = new FileTransfer();
        var Destination = encodeURI(URL_WSServer + 'uploadedFile/singlefileupload');
        //        var Source = encodeURI((URL + "DO/PDF/WP/")+ FileName + '.pdf');
        var Source = encodeURI(GM.Global.WPPdfPath);
        console.log("Source of pdf path while uploading" + Source);
        var options = new FileUploadOptions();
        options.fileName = FileName + '.pdf';
        options.mimeType = "multipart/form-data";
        var params = {};
        params.token = localStorage.getItem('token');
        //CompanyID needs to Be sent To Get the CompanyLocale
        var cmpid = localStorage.getItem('cmpid');
        if (cmpid != undefined && cmpid != "")
            params.cmpid = cmpid
        else
            params.cmpid = "";
        params.ordid = FileName;
        params.filename = FileName + '.pdf';
        options.params = params;
        ft.trustAllHosts = true;
        ft.upload(Source,
            Destination,
            function (success) {
                console.log(success);
                console.log("uploaded!");
                callback(1);
            },
            function (error) {
                console.log(error);
                console.log('failed');
                callback(0);
            },
            options);
    })

}

function fnWriteFile(filePath, fileName, data, callback) {
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        createDirectory(fileSystem.root, filePath, function (path) {
            console.log(path.toURL());
            path.getFile(fileName, {
                create: true,
                exclusive: false
            }, function (fileEntry) {
                fileEntry.createWriter(function (writer) {
                    writer.onwriteend = function (event) {
                        console.log(">>>>>>>>>>>>>File Created<<<<<<<")
                        callback(fileEntry.toURL());
                    }
                    writer.write(data);
                }, onError);
            }, onError);
        });
    }, null);
}

function fnReadFile(URL, callback) {
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        fileSystem.root.getFile(URL, {}, function (entry) {
            entry.file(function (file) {
                var reader = new FileReader();
                reader.onloadend = function (evt) {
                    console.log(">>>>>>>>>Read as text<<<<<<<<");
                    callback(evt.target.result);
                };
                reader.readAsText(file);
            }, function (argument) {
                console.log('error file');
            })


        }, function () {
            console.log('error getting file');
        });
    }, null);
}

function createDirectory(root, path, success) {
    var dirs = path.split("/").reverse();
    var createDir = function (dir) {
        root.getDirectory(dir, {
            create: true,
            exclusive: false
        }, successCD, failCD);
    };

    var successCD = function (entry) {
        root = entry;
        if (dirs.length > 0) {
            createDir(dirs.pop());
        } else {
            success(entry);
        }
    };

    var failCD = function () {
        console.log("failed to create dir " + dir);
    };

    createDir(dirs.pop());
}

function createPartListPDFPath(callback) {
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        fileSystem.root.getDirectory("Globus", {
            create: true
        }, function (setlist) {
            console.log(setlist.toURL());
            setlist.getDirectory("Setlistinfo", {
                create: true
            }, function (tagid) {
                callback();
            }, onError);
        }, onError);
    }, null);
}

/*sendDOPDFEmailViaMobile: Function used to send an DO PDF email through Iphone
*/
function sendDOPDFEmailViaMobile(callback) {
    cordova.plugins.email.open({
                        to:      [],
                        cc:      [],
                        subject: lblNm("msg_s_globus_order_s") + GM.Global.PDFORDERID,
                        attachments: [ GM.Global.PDFPATH + GM.Global.DOPDFFOLDER + GM.Global.PDFORDERID + ".pdf"],
                        body:    lblNm("msg_find_attached_pdf")+"<b>" + GM.Global.PDFORDERID + "</b><br/>"+lblNm("msg_let_me_know")+"<br/><br/>"+lblNm("msg_thank_you")+"<br/>" + localStorage.getItem('fName') + " " + localStorage.getItem('lName'),
                        isHtml: true
                    });
}

/*sendDOPDFEmailViaIpad: Function used to send an DO PDF email through Iphone
*/
function sendDOPDFEmailViaIpad(callback) {
    window.plugin.email.open({
                        to:      [],
                        cc:      [],
                        subject: lblNm("msg_s_globus_order_s") + GM.Global.PDFORDERID,
                        attachments: [ GM.Global.PDFPATH + GM.Global.DOPDFFOLDER + GM.Global.PDFORDERID + ".pdf"],
                        body:    lblNm("msg_find_attached_pdf")+"<b>" + GM.Global.PDFORDERID+ "</b><br/>"+lblNm("msg_let_me_know")+"<br/><br/>"+lblNm("msg_thank_you")+"<br/>" + localStorage.getItem('fName') + " " + localStorage.getItem('lName'),
                        isHtml: true
                    });
}

/* createPDF: Common Function to detect the platform to Generate PDF and store it in the Local Device
 * Author : Karthik
 */
 function createPDF(pdfHtmlContent , pdfFolderSubPath ,fileName ,successcallback,failurecallback) {
        console.log(" Inside CreatePDF Method");
        if (parseNull(devicePlatform) == "") {
            devicePlatform = "ios";
        }
        if (parseNull(androidDevicePlatform) == "") {
            androidDevicePlatform = "android";
        }
         console.log(" Global devicePlatform Variable= " + devicePlatform);
         console.log(" androidDevicePlatform = " +androidDevicePlatform );

        if (devicePlatform.toLowerCase().includes(androidDevicePlatform)){
             createPDFInAndroid(pdfHtmlContent , pdfFolderSubPath ,fileName ,successcallback,failurecallback);
        } else{
             createPDFInIOS(pdfHtmlContent , pdfFolderSubPath ,fileName ,successcallback,failurecallback);
        }
 }

/* createPDFInAndroid:  Function to Create PDF with the cordova-pdf-generator Plugin in Android Devices
 * @see https://www.npmjs.com/package/cordova-pdf-generator#api
 * Author : Karthik
 */
function createPDFInAndroid(pdfHtmlContent , pdfFolderSubPath ,fileName ,callback,err) {
    console.log(" Inside createPDFInAndroid Method");
    var options = {
        documentSize: 'A4',
        type: 'base64'
    };
    pdfFolderSubPath = "DO/PDF/"+pdfFolderSubPath;
    fnGetLocalFileSystemPath(function(URL) {
     try{
        console.log(" URL createPDFInAndroid == " + URL);
        pdf.fromData(pdfHtmlContent , options) // cordova-pdf-generator Plugin
            .then(function(base64){
                // To define the type of the Blob
                var contentType = "application/pdf";

                // if cordova.file is not available use instead :
                // var folderpath = "file:///storage/emulated/0/Download/";
                var folderpath = URL + pdfFolderSubPath; //you can select other folders
                savebase64AsPDF(folderpath, fileName, base64, contentType);
                callback(folderpath+fileName);
            });
            //.catch((err)=>console.log(" err createPDFInAndroid == " + err));
        }catch(err){
            console.log(" err createPDFInAndroid == " + err)
        }
     });
	}

/* createPDFInIOS:  Function to Create PDF with the window.html2pdf Plugin in IOS Devices
 * @see https://github.com/moderna/cordova-plugin-html2pdf
 * Author : Karthik
 */
    function createPDFInIOS(pdfHtmlContent , pdfFolderSubPath ,fileName ,callback,err) {
            console.log(" Inside createPDFInIOS Method");
            pdfFolderSubPath = "~/Documents/Globus/DO/PDF/"+pdfFolderSubPath;
            window.html2pdf.create(pdfHtmlContent , pdfFolderSubPath+fileName , callback , err );
    	}


/**
 * Convert a base64 string in a Blob according to the data and contentType.
 *
 * @param b64Data {String} Pure base64 string without contentType
 * @param contentType {String} the content type of the file i.e (application/pdf - text/plain)
 * @param sliceSize {Int} SliceSize to process the byteCharacters
 * @see http://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
 * @return Blob

 * Author: Karthik
 */
  function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

  var blob = new Blob(byteArrays, {type: contentType});
  return blob;
  }

/**
 * Create a PDF file according to its database64 content only.
 *
 * @param folderpath {String} The folder where the file will be created
 * @param filename {String} The name of the file that will be created
 * @param content {Base64 String} Important : The content can't contain the following string (data:application/pdf;base64). Only the base64 string is expected.

 * Author: Karthik
 */
 function savebase64AsPDF(folderpath,filename,content,contentType){
    // Convert the base64 string in a Blob
    var DataBlob = b64toBlob(content,contentType);

    console.log("Starting to write the file :3");

    window.resolveLocalFileSystemURL(folderpath, function(dir) {
        console.log("Access to the directory granted succesfully");
        dir.getFile(filename, {create:true}, function(file) {
            console.log("File created succesfully.");
            file.createWriter(function(fileWriter) {
                console.log("Writing content to file");
                fileWriter.write(DataBlob);
            }, function(){
                alert('Unable to save file in path '+ folderpath);
            });
        });
    });
 }

/* openPdfFromLocalFileSystem: Common Function to detect the platform for to Open the PDF from Local Device
 * Author : Karthik
 */
    function openPdfFromLocalFileSystem(pdfpath) {
        console.log(" Inside openPdfFromLocalFileSystem Method");
           if (parseNull(devicePlatform) == "") {
               devicePlatform = "ios";
           }
           if (parseNull(androidDevicePlatform) == "") {
               androidDevicePlatform = "android";
           }
           console.log(" Global devicePlatform Variable= " + devicePlatform + androidDevicePlatform + pdfpath);
          if (devicePlatform.toLowerCase().includes(androidDevicePlatform)){
                openPdfInFileOpener2Plugin(pdfpath);
           } else{
                openPdfInWindowOpener(pdfpath);
           }
    }

/* openPdfInFileOpener2Plugin: Function to Open PDF from the Local File System Path using cordova-plugin-file-opener2 Plugin in Android
 * Author : Karthik
 */
    function openPdfInFileOpener2Plugin(pdfpath) {
        console.log(" pdfpath in openPdfInFileOpener2Plugin== " + pdfpath);
        cordova.plugins.fileOpener2.open(
              pdfpath,
             'application/pdf',
            {
                error : function(err){ console.log("openPdfInFileOpener2Plugin error == " + err.message + "=" + err.status ) },
                success : function(status){ console.log(" openPdfInFileOpener2Plugin status== " + status) }
            }
        );
    }

/* openPdfInWindowOpener: Function to Open PDF from the Local File System Path using window.opener in IOS
 * Author : Karthik
 */
    function openPdfInWindowOpener(pdfpath) {
		var iosVersion = "";
		console.log(" pdfpath in openPdfInWindowOpener== " + pdfpath);
		if(typeof device != 'undefined'){
			console.log(" pdfpath in parseInt(device.version) == " + parseInt(device.version));
			iosVersion = parseInt(device.version);
        }
		/*
		** cordova.plugins.fileOpener2.open plugin supported by the Android 4.4+ / iOS 9+ versions 
		*/
        if (iosVersion >= 9)
            openPdfInFileOpener2Plugin(pdfpath); // call the openPdfInFileOpener2Plugin() to view the PDF in fileOpener2 Plugin viewer for the IOS devices also
        else
            var DOCViewer = window.open(pdfpath, '_blank', 'location=no');

    }


/* fnCreateDirectoryForTagImage: create directory for tag image to write
 * Author : Karthik
 */
function fnCreateDirectoryForTagImage(callback) {
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        fileSystem.root.getDirectory("Globus", {
            create: true
        }, function (d) {
            console.log(d.toURL());
            d.getDirectory("DO", {
                create: true
            }, function (tag) {
                console.log(tag.toURL());
                tag.getDirectory("TAGIMAGE", {
                    create: true
                }, function (tagImgPath) {
                    console.log(tag.toURL());
                    callback(decodeURI(tagImgPath.toURL()));
                }, onError);

            }, onError);
        }, onError);
    }, null);
}

/* fnWriteImage: write image in the desired path
 * Author : Karthik Somanathan
 */
function fnWriteImage(filePath, fileName, imageData, contentType,callback) {
     // Convert the base64 string in a Blob
    var DataBlob = b64toBlob(imageData,contentType);

    fnWriteFile(filePath,fileName,DataBlob,function(data) {
        callback(data);
     });
}

/* fnReadImageFromFileSystemAsArrayBuffer: this function is used to read image from the file system and send it to   * the fnReadTextFrmImgAPI
 * Author : Karthik Somanathan
 */
function fnReadImageFromFileSystemAsArrayBuffer(orderid, savefl, ContentType, subscriptionKey, URLBase, processedDt, tagImageDetailId, tagImagePath) {
    console.log('fnReadImageFromFileSystemAsArrayBuffer URL===' + tagImagePath);
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        fileSystem.root.getFile("Globus/" + tagImagePath, {}, function (entry) {
            entry.file(function (file) {
                var reader = new FileReader();
                reader.onload = function (evt) {
                    // callback(evt.target.result);
                    fnReadTextFrmImgAPI(subscriptionKey, URLBase, ContentType, evt.target.result, function (textImgData) {
                        if (textImgData != undefined && textImgData != "") {
                            for (var j = 0; j < textImgData.length; j++) {
                                fnGetDOTagScan(textImgData[j], function (tagdata) {
                                    console.log("tagdata ==" + JSON.stringify(tagdata));
                                    if (tagdata != "") {
                                        //to update the tag from image(response from service)
                                        fnUpdDOTagImageDetails(tagImageDetailId, orderid, "", "", tagdata[0].tagid, processedDt);
                                        if (savefl == 'Y') {
                                            fnSaveTagsForProcessImage(orderid, tagdata[0].tagid, tagdata[0].setid, tagdata[0].setnm, '', '', 'Pending Sync');
                                        }
                                    }else{
                                        //to update the processed date
                                        fnUpdDOTagProcessedDate(tagImageDetailId, orderid, processedDt);
                                    }
                                });
                            }
                        }else{
                            //to update the processed date
                             fnUpdDOTagProcessedDate(tagImageDetailId, orderid, processedDt);
                        }

                    });

                };
                reader.readAsArrayBuffer(file);
            }, function (argument) {
                console.log('fnReadImageFromFileSystemAsArrayBuffer(): error file');
            })


        }, function () {
            console.log('fnReadImageFromFileSystemAsArrayBuffer(): error getting file');
        });
    }, null);
}


/* openImageInFileOpener2Plugin: Function to Open Image File from the Local File System Path using cordova-plugin-file-opener2 Plugin
 * Author : Karthik
 */
    function openImageInFileOpener2Plugin(pdfpath) {
        console.log(" Image path in openImageInFileOpener2Plugin== " + pdfpath);
        cordova.plugins.fileOpener2.open(
              pdfpath,
             'image/jpeg',
            {
                error : function(err){ console.log("openImageInFileOpener2Plugin error == " + err.message + "=" + err.status ) },
                success : function(status){ console.log(" openImageInFileOpener2Plugin status== " + status) }
            }
        );
    }

//  PC-5704 - To get the device token which is saved in the text file
function fnGetFirebasePNSToken(callback) {
    window.resolveLocalFileSystemURL(cordova.file.documentsDirectory+'TokenFile.txt', function success(fileEntry) {
      console.log(fileEntry);
        fileEntry.file(function(file) {
                var reader = new FileReader();
                reader.onloadend = function(e) {
                    console.log("Readed Text on FCM token: "+this.result);
                    callback(this.result);
                }
                reader.readAsText(file);
            }, function (error) {
                        console.log("error >>>>>>>>>" + error);
                        callback();
                    });
    }, function (error) {
                        console.log("error >>>>>>>>>" + error);
                        callback();
                    });
}

// PC-5704 - Firebase push notification get URL from url text file on appdelegate function
function appLaunchInit (callback) {
console.log("appLaunchInit >>>>>>>>>>");

            window.resolveLocalFileSystemURL(cordova.file.documentsDirectory+'URL.txt', function success(fileEntry) {
                console.log("fileEntry >>>>>>>>>>" + fileEntry);
              console.log(fileEntry);
                
                fileEntry.file(function(file) {
                        var reader = new FileReader();

                        reader.onloadend = function(e) {
                            console.log("Readed Text on FCM UR >>>>>: "+this.result);
                            if (parseNull(this.result) != "") {
                                localStorage.setItem("FCMAppOpenURL", this.result); 
                            }

                            delURLTextFile("URL.txt");
                          callback(this.result);
                        }

                        reader.readAsText(file);
                    }, function (error) {
                        console.log("error >>>>>>>>>" + error);
                        callback();
                    });
            }, function (err) {
                    console.log("err >>>>>>>>>" + err);
                    callback();
            });
}

//PC-5704 - Delete URL File
function delURLTextFile(fileName) {
    console.log("delURLTextFile >>>>>>>>");
    window.resolveLocalFileSystemURL(cordova.file.documentsDirectory, function (dir) {
        dir.getFile(fileName, {create: false}, function (fileEntry) {
            fileEntry.remove(function (file) {
                console.log("file removed!");
            }, function (error) {
                console.log("error occurred: " + error.code);
            }, function () {
                console.log("file does not exist");
            });
        });
    });

}

