var pageno = 1,
    totalpages = 0,
    percent, callbackFn, VO, totalsize = 0,
    localSize = 0,
    pagesize, refType, GroupPartDeleteCount, sql = "",
    master = 1,
    updateSql = "",
    fileIDs = [],
    arrAccGPOMapp = [],
    arrSyncedAccounts = [];

var updatepageno = 1,
    updatetotalsize = 0,
    updatetotalpages = 0,
    updatelocalSize = 0,
    arrMain;

var customInput = "",
    clearedCases = new Array();

var casesync = false;

//Master Call to sync 
function DataSync_SL(VOname, input, callback) {
    try {
        var WSName = eval(VOname + ".url");
        var parentStatus = eval(VOname + ".parentStatus")
        if (parentStatus != undefined && parentStatus == "failure") {
            callback("success");
        } else {

            VO = VOname;
            sql = "";
            if (input != undefined)
                customInput = input;
            if (callback != undefined)
                callbackFn = callback;
            fnGetSyncInfo(eval(VOname + ".syncType"), function(syncInfo) {
                try {
                    if (eval(VOname + ".syncSeq") == "1")
                        master = syncInfo;
                    getServerChanges(WSName, function(ServerChanges) {
                        try {
                            if (VOname == "listGmGroupPartVO" && !boolSyncing) {
                                GroupPartDeleteCount = 0;
                                chkGroupPartUpdate(VOname, ServerChanges, fnSyncStatusUpdate)
                            } else if ((VO == 'listGmRulesVO')) {
                                changeLocalDB(0, VOname, ServerChanges, fnSyncStatusUpdate)
                            } else if ((VO == 'listGmCaseInfoVO') || (VO == 'listGmCaseAttributeVO'))
                                fnDeleteCases(VOname, ServerChanges);
                            else if (VO == 'listGmAcctPartPriceVO' || VO == 'listGmAcctGPOPriceVO')
                                fnDelPrice(ServerChanges);
                            else
                                updateLocalDB(VOname, ServerChanges, fnSyncStatusUpdate);
                        } catch (err) {
                            showAppError(err, "GmDataSync:DataSync_SL(2)")
                        }
                    });
                } catch (err) {
                    showAppError(err, "GmDataSync:DataSync_SL(1)")
                }
            });
        }
    } catch (err) {
        showAppError(err, "GmDataSync:DataSync_SL(0) ");
    }
}
/*fnSyncStatusCheck: Check previous sync status from t9151 table.
 *if status is failure then clear local table and void sync detail in server for current sync type.
 *Start fresh sync for the sync type.
 */
function fnSyncStatusCheck(VOname, callback) {
    try {
        var syncType = eval(VOname + ".code");
        var parentCode = eval(VOname + ".parentCode");
        var query = "SELECT c901_sync_status status FROM t9151_device_sync_dtl WHERE c901_sync_type ='" + syncType + "'";
        fnExecuteSelectQuery(query, function(data) {
            try {
                var pre_status = "";

                if (data.length > 0)
                    pre_status = data[0].status;

                if (pre_status == '103124' || pre_status == '103125') { // Failure
                    resetServer(syncType, function() {
                        try {
                            var querystr = "DELETE FROM " + eval(VOname + ".tableName");
                            /* 
                             *SYSTEM & SET MASTER are using same table "T207_SET_MASTER".
                             *For System, the c207_type is null.So, 
                             *the below if-else condition to delete only SYSTEM records or set master records.
                             */
                            if (VOname == "listGmSystemVO") { //SYSTEM
                                querystr += " WHERE c207_type IS NULL";
                            } else if (VOname == "listGmSetMasterVO") { //SET
                                querystr += " WHERE c207_type IS NOT NULL ";
                            }

                            if (VOname == "listGmAccountVO"){
                                fnGetSyncedAccountPriceNameList(function (arrAccInfo) {
                                        arrSyncedAccounts = _.pluck(arrAccInfo, "ID");
                                        fnExecuteSelectQuery(querystr, function(data) {
                                            try {
                                                console.log("status failure");
                                                if (pre_status == '103124')
                                                    fnUpdateSyncStatus(VOname, "103125"); //sync status as WIP
                                                callback("success");
                                            } catch (err) {
                                                showAppError(err, "GmDataSync:fnSyncStatusCheck(2)");
                                            }
                                        });
                                });
                            } else {
                                fnExecuteSelectQuery(querystr, function(data) {
                                    try {
                                        console.log("status failure");
                                        if (pre_status == '103124')
                                            fnUpdateSyncStatus(VOname, "103125"); //sync status as WIP
                                        callback("success");
                                    } catch (err) {
                                        showAppError(err, "GmDataSync:fnSyncStatusCheck(2)");
                                    }
                                });
                            }
                        } catch (err) {
                            showAppError(err, "GmDataSync:fnSyncStatusCheck(3)");
                        }
                    });
                } else {
                    /*Check parent sync status before sync starting. If parent failure then skip the child sync
                    Ex. if part fails then skip part attribute sync*/
                    if (pageno == 1 && parentCode != undefined) {
                        var query = "SELECT c901_sync_status status FROM t9151_device_sync_dtl WHERE c901_sync_type ='" + parentCode + "'";
                        fnExecuteSelectQuery(query, function(data) {
                            try {
                                var pre_status = data[0].status;
                                console.log("pre_status=" + pre_status);
                                if (pre_status == '103124' || pre_status == '103125') { // Failure
                                    console.log("status success");
                                    callback("failure");
                                } else {
                                    console.log("status success");
                                    fnUpdateSyncStatus(VOname, "103125"); //sync status as WIP
                                    callback("success");
                                }
                            } catch (err) {
                                showAppError(err, "GmDataSync:fnSyncStatusCheck(4)");
                            }
                        });

                    } else {

                        console.log("status success");
                        fnUpdateSyncStatus(VOname, "103125"); //sync status as WIP
                        callback("success");
                    }
                }
            } catch (err) {
                showAppError(err, "GmDataSync:fnSyncStatusCheck(1)");
            }
        });
    } catch (err) {
        showAppError(err, "GmDataSync:fnSyncStatusCheck(0)");
    }
}
function fnUpdateSyncStatus(VOname, status) {
    try {
        var data = [];
        var json = {
            "reftype": eval(VOname + ".code"),
            "statusid": status,
            "syncgrp": eval(VOname + ".syncGrp"),
            "syncseq": eval(VOname + ".syncSeq").toString()
        }
        data.push(json);
        updateLocalDB("GmProdCatlReqVO", data, function() {
            console.log("local sync table insert success  VOname=" + eval(VOname + ".code") + " :: status=" + status);
        });
    } catch (err) {
        showAppError(err, "GmDataSync:fnUpdateSyncStatus()");
    }
}

function fnSyncStatusUpdate(SyncStatus) {
    if (SyncStatus === "Success") {
        if (pageno != 1) {
            DataSync_SL(VO);
        } else {
            getLocalDBRecCount(function (localRecCount) {
                acknowlegdeServer(SyncStatus, localRecCount);
            });
        }
    }
}
var start_time;
var record_count;

function updateLocalDB(VOname, ResultSet, fnCallbackStatus) {
    try {
        this.start_time = (new Date()).getTime();
        var ln = ResultSet.length;
        this.record_count = ln;
        var j, l, k;
        var bulk = 500;

    var iteration = Math.floor(ln / bulk);

    if ((ln % bulk) > 0)
        iteration += 1;

    var remaining = ln % bulk;
    var tblName;

    var start, end;

    start = 0;
    end = start + bulk;

    if (ln < end) {
        end = ln;
        iteration = 1;
    }

        intializePrepareNewQuery(0, iteration, start, end, bulk, VOname, ResultSet, fnCallbackStatus);
    } catch (err) {
        showAppError(err, "GmDataSync:updateLocalDB()");
    }
}


function intializePrepareNewQuery(iValue, iteration, start, end, bulk, VOname, ResultSet, fnCallbackStatus) {
    try {
        if (iValue < iteration) {
            var l = ResultSet.slice(start, end);
            prepareNewQuery(VOname, l, function(data) {
                try {
                    start = end;
                    end = end + bulk;

                    if (end > ResultSet.length)
                        end = ResultSet.length;

                    tableName = eval(VOname + ".tableName");
                    insertQuery(tableName, data, function() {
                        try {
                            intializePrepareNewQuery(iValue + 1, iteration, start, end, bulk, VOname, ResultSet, fnCallbackStatus)
                        } catch (err) {
                            showAppError(err, "GmDataSync:intializePrepareNewQuery(2)");
                        }
                    });
                } catch (err) {
                    showAppError(err, "GmDataSync:intializePrepareNewQuery(1)");
                }
            });
        } else
            fnCallbackStatus("Success")
    } catch (err) {
        showAppError(err, "GmDataSync:intializePrepareNewQuery(0)");
    }

}


function prepareNewQuery(VOname, ResultSet, callback) {
    try {
        var i = 0;
        var f = 0; // Length of the FieldNames array
        var l = ResultSet.length;
        var row;

        var newQuery = true,
            query = '';
        var FieldNames, FieldValues;
        var tableName = eval(VOname + ".tableName");

        getRow(tableName);

        localSize += l;
        for (i = 0; i < l; i++) {

            row = ResultSet[i];

        if (newQuery) {

            FieldNames = getFieldNames(VOname, row);
            FieldValues = getFieldValues(VOname, row);

            f = FieldNames.length;
            //INSERT OR REPLACE INTO
            query = "INSERT OR REPLACE INTO " + tableName + "(";

            for (j = 0; j < f; j++) {

                if (FieldNames[j] !== "ID") {
                    query += ("'" + FieldNames[j] + "'");

                    if (j < f - 1) {
                        query += ",";
                    }
                }
            }

            query += ") SELECT " + getFieldValues(VOname, row);
            newQuery = false;
        } else {

            query += ' UNION ALL SELECT ' + getFieldValues(VOname, row);
        }

    }



    query = query.replace(/\"/g, "\&quot;");

        if (i == l) {
            callback(query)
            newQuery = true;
        }
    } catch (err) {
        showAppError(err, "GmDataSync:prepareNewQuery()");
    }

}

function insertQuery(tableName, query, fnCallback) {
    db.transaction(function(tx, rs) {
        tx.executeSql(query, [], function(tx, rs) {
            if (tableName != "T9151_DEVICE_SYNC_DTL") { // No change in progress bar when inserting into t9151
                console.log(rs.rowsAffected + " Records were successfully inserted");
                var syncSeq = eval(VO + ".syncSeq"),
                    totalSeq = eval(VO + ".totalSeq");
                var percentage = (localSize / totalsize) * 100;
                if (syncSeq != undefined) {
                    var percent = ((syncSeq - 1) * (100 / totalSeq)) + (percentage / totalSeq);
                    $(".spn-current-progress").css("width", percent + "%");
                }
                console.log(rs.rows.length + " Records were successfully retrieved");
            }
            fnCallback("Success");
        }, function(tx, err) {
            console.log("Failed Query: " + query);
            console.log("Failure " + JSON.stringify(err));
            fnCallback("Fail");
        });
    });

}

function getQuery(tableName, callback) {
    var query = "SELECT * FROM " + tableName;
    db.transaction(function(tx, rs) {
        tx.executeSql(query, [], function(tx, rs) {

            var syncSeq = eval(VO + ".syncSeq"),
                totalSeq = eval(VO + ".totalSeq");
            var percentage = (localSize / totalsize) * 100;
            var percent = ((syncSeq - 1) * (100 / totalSeq)) + (percentage / totalSeq);
            $(".spn-current-progress").css("width", percent + "%");
            console.log(rs.rows.length + " Records were successfully retrieved");
            callback("Success");
        }, function (tx, err) {
            console.log("Failed Query: " + query);
            console.log("Failure " + JSON.stringify(err));
        });
    });

}

//Insertion into LocalDB is made
function changeLocalDB(iValue, VOname, ResultSet, callback) {
    try {
        var l = ResultSet.length;
        // var sql = "";
        var e;
        var TableName = eval(VOname + ".tableName");
        var now = new Date();

    db.transaction(
        function (tx, result) {
            if (iValue < l) {
                e = ResultSet[iValue];

                var LMDate = new Date(e.LastModified);
                if (sql == "")
                    sql = LDB_InsertUpdateSQL(VOname, e);
                params = getFieldValuesForChangeDB(VOname, e);

                tx.executeSql(sql, params,
                    function () {

                        localSize++;
                        var syncSeq = eval(VO + ".syncSeq"),
                            totalSeq = eval(VO + ".totalSeq");
                        var percentage = (localSize / totalsize) * 100;

                        var percent = ((syncSeq - 1) * (100 / totalSeq)) + (percentage / totalSeq);
                        $(".spn-current-progress").css("width", percent + "%");
                        if (VO == 'listGmFileVO') {
                            fnConfigPathFor(e.fileid, function () {
                                changeLocalDB(iValue + 1, VOname, ResultSet, callback);
                            });
                        } else {
                            changeLocalDB(iValue + 1, VOname, ResultSet, callback);
                        }

                    },
                    function (tx, err) {

                            showAppError(err, "GmDataSync:changeLocalDB(1)");
                            callbackFn("fail");
                        }
                    );
                } else {
                    if ((VO == 'listGmFileVO') || (VO == 'listGmRulesVO')) {
                        fnRunShareRuleUpdate(function() {
                            if (VO == 'listGmFileVO') {
                                fnRefreshShareCart(function() {
                                    callback("Success");
                                });
                            } else
                                callback("Success");
                        })
                    } else
                        callback("Success");
                }
            }, this.txErrorHandler, null
        );
    } catch (err) {
        showAppError(err, "GmDataSync:changeLocalDB(0)");
    }
}

//Get the data from the server
function getServerChanges(WSName, callback) {
    try {
        var token = localStorage.getItem("token");
        var postData;
        if (customInput == "")
            postData = {
                "token": token,
                "langid": langid,
                "pageno": pageno,
                "uuid": deviceUUID,
                "countryid": intServerCountryCode
            };
        else {
            customInput.token = token;
            customInput.langid = langid;
            customInput.pageno = pageno;
            customInput.uuid = deviceUUID;
            customInput.countryid = intServerCountryCode;
            postData = customInput;
        }
        fnGetWebServerData(WSName, undefined, postData, function(data, model, response, errorReport) {
            if (model == undefined)
                processServerData(data, callback);
            else {
                callbackFn("Fail");
            }
        }, true);
    } catch (err) {
        showAppError(err, "GmDataSync:getServerChanges(0)");
    }
}

//Calulates the Pages / Deletes unwanted information from the server 
function processServerData(data, callback) {
    try {
        refType = data.reftype;
        totalsize = data.totalsize;
        if (data.totalpages != "")
            totalpages = data.totalpages;
        else
            totalpages = pageno;

        pagesize = parseInt(data.pagesize);
        // percent = parseInt((pageno/totalpages)*100); 
        if (data.pageno != data.totalpages) {
            pageno++;
        } else {
            pageno = 1;
        }
        if (data.totalpages != "") {
            var val;
            var valLength;
            // VO = "listGmGroupVO";//testing 
            var dataHolder = (eval(VO + ".dataHolder")) ? eval(VO + ".dataHolder") : VO;


            /* Below Code Added for PMT-26444 
             *  local VO name mismatch with VO name in web service JSON. 
             *  Get the VO name based on reftype in web service JSON. 
             */
            if (eval("data." + dataHolder) == undefined) {
                VO = jsnUpdateCode[data.reftype];
                dataHolder = (eval(VO + ".dataHolder")) ? eval(VO + ".dataHolder") : VO;
            }

            if (eval("data." + dataHolder).length == undefined) {
                val = [];
                val.push(eval("data." + dataHolder))
            } else
                val = eval("data." + dataHolder);


            valLength = val.length;
            for (var i = 0; i < valLength; i++) {
                var currRow = val[i];
                for (var column in currRow) {
                    if (eval(VO + "." + column) == undefined)
                        delete currRow[column];
                }
            }

            if (VO == "listGmFileVO")
                fileIDs = fileIDs.concat(_.pluck(val, 'fileid'));
            else if (VO == "listGmAcctGpoMapVO")
                arrAccGPOMapp = arrAccGPOMapp.concat(val);
            //        if (VO == "listGmAccountVO" && data.pageno == 1) 
            //            fnGetSyncedAccountPriceNameList(function (arrAccInfo) { 
            //                arrSyncedAccounts = _.pluck(arrAccInfo, "ID"); 
            //                callback(val); 
            //            }); 
            // else 
            callback(val);

        } else {
            var syncSeq = eval(VO + ".syncSeq"),
                totalSeq = eval(VO + ".totalSeq");
            var percent = (syncSeq / totalSeq) * 100;
            $(".spn-current-progress").animate({
                "width": percent + "%"
            }, "fast");
            getLocalDBRecCount(function (localRecCount) {
                acknowlegdeServer("", localRecCount);
            });

        }
    } catch (err) {
        errArrLog.push_arr_ltd("<br> GmDataSync:processServerData():ErrData<br> VOname:" + VO + "<br> " + JSON.stringify(data) + '<br>'); // Error data will pushed in errArrLog[ ] 
        showAppError(err, "GmDataSync:processServerData()");
    }
}

//Updates the Sync Status to the Server
function acknowlegdeServer(status, localRecCount) {
    pageno = 1;
    localSize = 0;
    sql = "";
    var token = localStorage.getItem("token");

    var postData = {
        "token": token,
        "langid": langid,
        "reftype": refType,
        "totalsize": localRecCount,
        "statusid": "103121",
        "uuid": deviceUUID,
        "countryid": intServerCountryCode,
        "lastrec" : "SYNCJSON"
    };
    if (VO != "listGmDataUpdateVO") {
        console.log(postData);
        fnGetWebServerData("devicesync/status", undefined, postData, function(data, model, response, errorReport) {
            if (model == undefined) {
                if (data != undefined) {
                    var currArray = new Array();
                    var tmpData = {};
                    tmpData.reftype = data.reftype;
                    tmpData.statusid = data.statusid;
                    tmpData.totalsize = data.totalsize;
                    tmpData.svrcnt = data.svrcnt;
                    tmpData.syncgrp = eval(VO + ".syncGrp");
                    tmpData.syncseq = eval(VO + ".syncSeq").toString();
                    tmpData.syncdt = (new Date()).toString();
                    currArray.push(tmpData);
                    data = currArray;
                }

                updateLocalDB("GmProdCatlReqVO", data, function () {
                    console.log("GmDataSync :: acknowlegdeServer() :: local sync table insert success");
                });
                //}

                localSize = 0;
                // fnShowStatus("Database Sync from Server to Device successful");
                if ((VO != "listGmCaseAttributeVO") && (VO != "listGmFileVO") && (VO != "listGmAcctGpoMapVO") && (VO != "listGmAccountVO"))
                    callbackFn("success");
                else if (VO == "listGmFileVO")
                    fnConfigFilePath();
                else if (VO == "listGmAcctGpoMapVO")
                    fnCheckForGPOPriceUpdate();
                else if (VO == "listGmAccountVO") {
                    fnUpdatePriceSyncInfo(arrSyncedAccounts.join(","), "Y", function () {
                        callbackFn("success");
                    }); 
                }
                else
                    fnCreateEvents();
            } else {
                console.log("Nothing to be Sync'd from the Server!");
                callbackFn("fail");
                showMsgView("027");
            }
        }, true);
    }

}

function getLocalDBRecCount(callback) {
    var querystr = "select count(1) as COUNT FROM " + eval(VO + ".tableName");
    if (VO == "listGmSystemVO") {
        querystr += " WHERE c207_type IS NULL AND C207_VOID_FL = ''";
    } else if (VO == "listGmSetMasterVO") {
        querystr += " WHERE c207_type IS NOT NULL AND c901_status_id = '20367' AND C207_VOID_FL = ''";
    } else if (VO == "listGmPartAttributeVO") {
        querystr = "select count(distinct(t205d.c205_part_number_id)) as COUNT FROM " + eval(VO + ".tableName");
        querystr += " T205D, t205_part_number T205 where c205d_void_fl=''" +
            " AND t205.c205_part_number_id =t205d.c205_part_number_id AND T205.c205_active_fl ='Y'";
    } else if (VO == "listGmPartNumberVO") {
        querystr += " WHERE C205_ACTIVE_FL ='Y'";
    } else if (VO == "listGmSetPartVO") {
        querystr = "select count(distinct(t208.c207_set_id)) as COUNT FROM " + eval(VO + ".tableName");
        querystr += " t208, T207_SET_MASTER t207 WHERE t207.c207_set_id = t208.c207_set_id AND t208.c208_void_fl ='' AND t207.c901_status_id = '20367'";
    } else if (VO == "listGmSetAttributeVO") {
        querystr = "select count(distinct(t207c.c207_set_id)) as COUNT FROM " + eval(VO + ".tableName");
        querystr += " T207C, T207_SET_MASTER t207 WHERE t207.c207_set_id = t207c.c207_set_id AND T207C.c207c_void_fl ='' AND t207.c901_status_id = '20367'";
    } else if (VO == "listGmSystemAttributeVO") {
            querystr = "select count(distinct(t207c.c207_set_id)) as COUNT FROM " + eval(VO + ".tableName");
            querystr += " t207c, t207_set_master t207 WHERE t207.c207_set_id = t207c.c207_set_id AND c207_type IS NULL AND C207_VOID_FL = ''";
    } else if (VO == "listGmAccountVO") {
        querystr += " WHERE C704_ACTIVE_FL = 'Y' AND C704_VOID_FL = ''";
    } else if (VO == "listGmAcctGpoMapVO") {
        querystr += " t740, T704_ACCOUNT t704 WHERE t704.C704_ACCOUNT_ID = t740.C704_ACCOUNT_ID AND t740.c740_void_fl = '' AND t704.C704_ACTIVE_FL = 'Y' AND t704.C704_VOID_FL = ''";
    } else if (VO == "listGmSalesRepVO") {
        querystr += " WHERE C703_ACTIVE_FL = 'Y' AND C703_VOID_FL = ''";
    } else if (VO == "listGmAssocRepMapVO") {
        querystr += " WHERE C704A_ACTIVE_FL = 'Y' AND C704A_VOID_FL = ''";
    } else if (VO == "listGmDistributorVO") {
        querystr += " WHERE C701_ACTIVE_FL = 'Y' AND C701_VOID_FL = ''";
    } else if (VO == "listGmAddressVO") {
        querystr += " WHERE C106_INACTIVE_FL = '' AND C106_VOID_FL = ''";
    } else if (VO == "listGmGroupVO") {
        querystr += " WHERE C4010_PUBLISH_FL = 'Y' AND C4010_VOID_FL = ''";
    } else if (VO == "listGmGroupPartVO") {
        querystr = "select count(distinct(t4011.C4010_GROUP_ID)) as COUNT FROM " + eval(VO + ".tableName");
        querystr += " t4011, T4010_GROUP t4010 WHERE T4010.C4010_GROUP_ID = T4011.C4010_GROUP_ID AND C4010_PUBLISH_FL = 'Y' AND C4010_VOID_FL = ''";
    } else if (VO == "listGmPartyVO") {
        querystr += " WHERE C101_VOID_FL = '' AND (C101_ACTIVE_FL = 'Y' OR C101_ACTIVE_FL = '')";
    } else if (VO == "listGmPartyContactVO") {
        querystr += " t107, t101_party t101 WHERE t101.c101_party_id =t107.c101_party_id " +
            " AND t101.C101_VOID_FL = '' AND (t101.C101_ACTIVE_FL = 'Y' OR t101.C101_ACTIVE_FL = '') " +
            " AND t107.C107_VOID_FL = '' AND t107.C107_INACTIVE_FL = ''";
    } else if (VO == "listGmUserVO") {
        querystr += " WHERE C901_USER_STATUS = 311"; //Active User
    } else if (VO == "listGmFileVO") {
        querystr = "select count(distinct(C903_REF_ID)) as COUNT FROM " + eval(VO + ".tableName");
        querystr += " WHERE C903_DELETE_FL = ''";
    } else if (eval(VO + ".voidfl") != undefined) {
        querystr += " WHERE " + eval(VO + ".voidfl") + " =''";
    }




    if (eval(VO + ".tableName") != undefined) {
        db.transaction(function(tx, rs) {
            tx.executeSql(querystr, [],
                function (tx, rs) {
                    callback(rs.rows.item(0).COUNT);
                }, null);

        });
    } else {
        callback("0");
    }
}

function fnCheckForGPOPriceUpdate() {
    if (master) {
        fnGetSyncedAccountPriceNameList(function (data) {
            if (data.length > 0) {
                data = _.pluck(data, 'ID');
                var mainCallback = callbackFn;
                var accID = new Array(),
                    input = {};
                $(arrAccGPOMapp).each(function () {
                    if (_.contains(data, this.acctid) && this.voidfl == "" && this.partyid != "")
                        accID.push(this.acctid);
                });
                if (accID.length > 0) {
                    accID = _.uniq(accID);
                    input.acctid = accID.join(",");
                    input.reftype = 4000524;
                    delete listGmAcctGPOPriceVO.syncSeq;
                    DataSync_SL("listGmAcctGPOPriceVO", input, function () {
                        arrAccGPOMapp = [];
                        listGmAcctGPOPriceVO.syncSeq = 1;
                        fnClearUpdates("4000524", function () {
                            fnGetUpdates(function (length) {
                                if (length > 0) {
                                    $(".divUpdatesNo").html('');
                                    $(".divUpdatesNo").html("&nbsp;" + length + "&nbsp;");
                                    $(".divUpdatesNo").trigger("refresh");
                                    $(".divUpdatesNo").css("display", "inline");
                                } else {
                                    $(".divUpdatesNo").text("");
                                    $(".divUpdatesNo").html("&nbsp;0&nbsp;");
                                    $(".divUpdatesNo").addClass("hide");
                                }
                                mainCallback("success");
                            });
                        });
                    });
                } else
                    callbackFn("success");
            } else
                callbackFn("success");
        });
    } else
        callbackFn("success");

}

function resetServer(syncType, callback) {
    try {
        var token = localStorage.getItem("token");
        var postData = {"token": token, "langid": langid, "pageno": pageno, "refType": syncType, "uuid": deviceUUID };
        console.log("GmDataSync:resetServer() input >>> " + JSON.stringify(postData));
        fnGetWebServerData("devicesync/clear", undefined, postData, function(data, model, response, errorReport) {
            console.log("GmDataSync:resetServer() output model >>> " + model);
            if (model == undefined) {
                if (syncType == "") {
                    fnResetDB(function() {
                        $(".divUpdatesNo").html('');
                        $(".divUpdatesNo").html("&nbsp;0&nbsp;");
                        $(".divUpdatesNo").css("display", "inline");
                        callback();
                    });
                } else {
                    callback();
                }
            } else {
                showMsgView("034");
            }
        }, true);
    } catch (err) {
        showAppError(err, "GmDataSync:resetServer()");
    }
}


function chkGroupPartUpdate(VOname, ResultSet, callback) {
    try {
        var len = ResultSet.length;
        if (GroupPartDeleteCount < len) {
            fnDeleteGroupPartData(ResultSet[GroupPartDeleteCount].groupid, function() {
                if (GroupPartDeleteCount == (len - 1)) {
                    updateLocalDB(VOname, ResultSet, callback);
                } else {
                    GroupPartDeleteCount++;
                    chkGroupPartUpdate(VOname, ResultSet, callback);
                }
            });
        }
    } catch (err) {
        showAppError(err, "GmDataSync:chkGroupPartUpdate()");
    }
}

//For Deleting the price of given Account ID / Part Number or Account ID / Group ID or GPO ID / Part Number or GPO ID / Group ID
function fnDelPrice(ResultSet) {
    try {
        var arrAcct = _.uniq(_.pluck(ResultSet, 'acctid'));
        var arrGpo = _.uniq(_.pluck(ResultSet, 'gpoid'));
        var arrPart = _.without(_.uniq(_.pluck(ResultSet, 'partnum')), "");
        var arrGrp = _.without(_.uniq(_.pluck(ResultSet, 'grpid')), "");
        var strQuery = null;

        if (VO == 'listGmAcctPartPriceVO') {
            strQuery = "DELETE FROM T705_ACCOUNT_PRICING WHERE C704_ACCOUNT_ID IN (" + arrAcct.join(',') + ")";
            if (arrPart.length > 0)
                strQuery += " AND C205_PART_NUMBER_ID IN ('" + arrPart.join("','") + "')";
            if (arrGrp.length > 0)
                strQuery += " AND C4010_GROUP_ID IN ('" + arrGrp.join("','") + "')";
        } else {
            strQuery = "DELETE FROM T705_ACCOUNT_PRICING WHERE C101_GPO_ID IN (" + arrGpo.join(',') + ")";
            if (arrPart.length > 0)
                strQuery += " AND C205_PART_NUMBER_ID IN ('" + arrPart.join("','") + "')";
            if (arrGrp.length > 0)
                strQuery += " AND C4010_GROUP_ID IN ('" + arrGrp.join("','") + "')";
        }


        fnExecuteDeleteQuery(strQuery, function() {
            //Get the maximum of the primary key (C705_ACCOUNT_PRICING_ID) and increment the ID by one for each record in ResulSet variable (Server Data)
            fnExecuteSelectQuery("SELECT MAX(C705_ACCOUNT_PRICING_ID) AS ID FROM T705_ACCOUNT_PRICING", function(arrMaxIDData) {
                var maxID = 0;
                if (arrMaxIDData[0].ID != null)
                    maxID = arrMaxIDData[0].ID + 1;
                for (var i = 0; i < ResultSet.length; i++)
                    ResultSet[i].acctpriceid = (maxID++).toString();

                updateLocalDB(VO, ResultSet, fnSyncStatusUpdate);
            });
        });
    } catch (err) {
        showAppError(err, "GmDataSync:fnDelPrice()");
    }
}

function chkUpdate() {
    try {
        console.log("chkUpdate >>>>>>>>>" )
        window.localStorage.setItem("lastUpdateChkTime", new Date());
        //    if (!boolSyncing) {
        //        UpdateTimer = window.setTimeout(function() {
        var token = localStorage.getItem("token");
        var postData = { "token": token, "langid": langid, "pageno": 1, "uuid": deviceUUID };
        var updatescnt = 0;
        fnGetWebServerData("devicesync/updatescnt", undefined, postData, function (data, model, response, errorReport) {
            console.log(data);
            if (model == undefined) {
                var updateData = data;
                if (data.totalsize > 0) {
                    $("#btn-updates").show();
                    $(".divUpdatesNo").text("");
                    $(".divUpdatesNo").html("&nbsp;" + data.totalsize + "&nbsp;");
                    $(".divUpdatesNo").removeClass("hide");
                    fnAutoSyncAllUpdates();
                } else {
                    $(".divUpdatesNo").text("");
                    $(".divUpdatesNo").html("&nbsp;0&nbsp;");
                    $(".divUpdatesNo").addClass("hide");
                    if (updateData.listAutoUpdates != undefined) {
                        fnAutoUpdate(updateData);
                    }
                }
            } else {
                console.log("Error in Checking Updates");
                console.log(model.responseText);
                //                    chkUpdate();
            }
        }, true);

        //        }, TimerMilliSeconds);
        //    }
    } catch (err) {
        showAppError(err, "GmDataSync:chkUpdate()");
    }
}

function fnAutoSyncAllUpdates() {
    try {
        //        if (!GM.Global.Browser)
        //            window.clearTimeout(timerSplashScreen);
        boolAutoUpdateStatus = 1;
        $("#div-data-sync").trigger("collide");
//        $("#div-updates").html("<li class='li-info-center'><img />&nbsp;Fetching Updates...</li>").delay(5000).slideUp(100);
        autoUpdateCollision = window.setInterval(function () {
            // $("#div-data-sync").html("<div id='div-settings-collision'>The App is being updated. This may take few seconds. <br/> Please wait...</div>");
            $("#div-wifi-note").hide();
            $("#div-updates").addClass("div-updates-updating");
        }, 100);

        getServerUpdates(function () {
            fnGetRequiredUpdates(function (refData) {
                if (refData.length > 0) {
                    fnAutoSyncVO(0, refData, function () {
                        window.clearInterval(autoUpdateCollision);
                        $("#div-updates").removeClass("div-updates-updating").slideUp(100);
                        boolAutoUpdateStatus = 0;
                        console.log("success");
                        fnTimerSplashScreen();
                        //                        chkUpdate();

                    })
                }
            });
        });
        //    else
        //        chkUpdate();
    } catch (err) {
        showAppError(err, "GmDataSync:fnAutoSyncAllUpdates()");
    }
}

function fnAutoSyncVO(iValue, refData, callback) {
    if (iValue < refData.length) {
        var currRefData = _.pluck(refData, 'TYPE');
        var input = '';
        if (currRefData[iValue] == "4000524" || currRefData[iValue] == "4000526") {
            input = {};
            fnGetSyncedAccountPriceNameList(function (data) {
                input.acctid = _.pluck(data, 'ID').join();
                input.reftype = currRefData[iValue];
                DataSync_SL(jsnUpdateCode[currRefData[iValue]], input, function() {
                    fnClearUpdates(currRefData[iValue], function() {
                        fnGetUpdates(function(length) {
                            if (length > 0) {
                                $(".divUpdatesNo").html('');
                                $(".divUpdatesNo").html("&nbsp;" + length + "&nbsp;");
                                $(".divUpdatesNo").trigger("refresh");                                
                                $(".divUpdatesNo").css("display", "inline");
                            } else {
                                $(".divUpdatesNo").text("");
                                $(".divUpdatesNo").html("&nbsp;0&nbsp;");
                                $(".divUpdatesNo").addClass("hide");
                            }
                            fnAutoSyncVO(iValue + 1, refData, callback);
                        });
                    });
                });
            });
        } else {
            DataSync_SL(jsnUpdateCode[currRefData[iValue]], input, function () {
                fnClearUpdates(currRefData[iValue], function () {
                    fnGetUpdates(function (length) {
                        if (length > 0) {
                            $(".divUpdatesNo").html('');
                            $(".divUpdatesNo").html("&nbsp;" + length + "&nbsp;");
                            $(".divUpdatesNo").trigger("refresh");
                            $(".divUpdatesNo").css("display", "inline");
                        } else {
                            $(".divUpdatesNo").text("");
                            $(".divUpdatesNo").html("&nbsp;0&nbsp;");
                            $(".divUpdatesNo").addClass("hide");
                        }
                        fnAutoSyncVO(iValue + 1, refData, callback);
                    });
                });
            });

        }


    } else
        callback();
}


function fnAutoUpdate(data) {
    if (data.listAutoUpdates != undefined) {
        if (casesync == false) {
            casesync = true;
            DataSync_SL('listGmCaseInfoVO', '', function () {
                DataSync_SL('listGmCaseAttributeVO', '', function () {
                    casesync = false;
//                    chkUpdate();
                });
            });
        } else {
//            chkUpdate();
        }
    } else {
//        chkUpdate();
    }
}


function getServerUpdates(callback) {
    try {
        console.log("getServerUpdates : boolSyncing >>>>>" + boolSyncing);
        updateCallbackFn = callback;
        if (!boolSyncing) {
            var token = localStorage.getItem("token");
            var postData = { "token": token, "langid": langid, "pageno": updatepageno, "uuid": deviceUUID };
            console.log("GmDataSync : getServerUpdates() >>>>>" + JSON.stringify(postData));
            fnGetWebServerData("devicesync/updates", undefined, postData, function (data, model, response, errorReport) {
                console.log(data);
                console.log(model);
                if (model == undefined) {
                    updatetotalsize = data.totalsize;
                    $(".divUpdatesNo").html('');
                    $(".divUpdatesNo").html("&nbsp;" + data.totalsize + "&nbsp;");
                    $(".divUpdatesNo").removeClass("hide");
                    if (data.pageno != data.totalpages) {
                        updatepageno++;
                    } else {
                        updatepageno = 1;
                    }
                    if (data.totalpages != "") {
                        updatetotalpages = data.totalpages;
                        var val;
                        if (eval("data.listGmDataUpdateVO").length == undefined) {
                            val = [];
                            val.push(eval("data.listGmDataUpdateVO"))
                        } else
                            val = eval("data.listGmDataUpdateVO");

                        for (var i = 0; i < val.length; i++) {
                            var currRow = val[i];
                            for (var column in currRow) {
                                if (eval("listGmDataUpdateVO." + column) == undefined)
                                    delete currRow[column];
                            }
                        }
                        fnLoadUpdates(0, val);
                    } else {
                        updatepageno = 1;
                        updaetetotalpages = updatepageno;
                        $(".divUpdatesNo").addClass("hide");
                        updateCallbackFn("no data");
                    }
                } else {
                    console.log("Nothing to be Sync'd from the Server with URL ");
                    updateCallbackFn("fail");
                }
            }, true);
        } else {
            console.log("getServerUpdates : else >>>>>");
            updateCallbackFn("no data");
        }
    } catch (err) {
        showAppError(err, "GmDataSync:getServerUpdates()");
    }
}

function fnLoadUpdates(iValue, ResultSet) {
    try {
        var l = ResultSet.length;
        var sql = "";
        var e;
        var TableName = eval("listGmDataUpdateVO.tableName");
        var now = new Date();

        db.transaction(
            function (tx, result) {
                if (iValue < l) {
                    e = ResultSet[iValue];
                    var LMDate = new Date(e.LastModified);
                    if (updateSql == "")
                        sql = LDB_InsertUpdateSQL("listGmDataUpdateVO", e);
                    params = getFieldValuesForChangeDB('listGmDataUpdateVO', e);
                    tx.executeSql(sql, params,
                        function () {
                            updatelocalSize++;
                            fnLoadUpdates(iValue + 1, ResultSet);
                        },
                        function (tx, err) {
                            console.log(JSON.stringify(err));
                            fnUpdateDataSync("fail");
                        }
                    );
                } else {
                    fnUpdateDataSync("Success");
                }
            }, this.txErrorHandler, null)
    } catch (err) {
        showAppError(err, "GmDataSync:fnLoadUpdates()");
    }
}


function fnUpdateDataSync(SyncStatus) {
    try {
        if (SyncStatus === "Success") {
            if (updatepageno != 1) {
                getServerUpdates(updateCallbackFn);
            } else {
                updatepageno = 1;
                updateCallbackFn(SyncStatus);
            }
        }
    } catch (err) {
        showAppError(err, "GmDataSync:fnUpdateDataSync()");
    }
}

function fnGetReason() {

    var token = localStorage.getItem('token');

    if (arrPendingDOList.join(",") != "") {
        var postData = {
            "token": token,
            "txnid": arrPendingDOList.join(","),
            "txntype": "90217"
        };

        fnGetWebServerData("common/fetchlogdetails", undefined, postData, function(data, model, response, errorReport) {
            if (model == undefined) {
                var reasons = new Array();
                if (data.length == undefined)
                    reasons.push(data);
                else
                    reasons = data;

                for (var i = 0; i < reasons.length; i++) {
                    fnUpdateRejectReason(reasons[i].txnid, reasons[i].txntype);
                }

                fnGetUpdatedDO(function(data) {
                    if (data.length > 0) {
                        var tobeCleared = _.pluck(data, 'ID');
                        arrPendingDOList = _.difference(arrPendingDOList, tobeCleared);
                    }
                });

            } else {
                console.log("Nothing to be Sync'd from the Server with URL ");
            }
        }, true);

    }

}


function fnConfigFilePath() {
    var now = new Date();
    var thisMonth = ((now.getMonth() + 1) > 9) ? (now.getMonth() + 1) : "0" + (now.getMonth() + 1);
    var prevMonth = (now.getMonth() > 9) ? now.getMonth() : "0" + now.getMonth();
    var query = "UPDATE T903_UPLOAD_FILE_LIST SET C901_TYPE = '4000790' WHERE C901_TYPE IN ('4000787','4000788','4000789') AND C903_LAST_UPDATED_DATE NOT LIKE '" + now.getFullYear() + "-" + thisMonth + "-%' AND C903_LAST_UPDATED_DATE NOT LIKE '" + now.getFullYear() + "-" + prevMonth + "-%'";

    db.transaction(function (tx, rs) {
        tx.executeSql("UPDATE T903_UPLOAD_FILE_LIST SET C903_FILE_PATH = (SELECT T901.C901_CODE_NM FROM T901_CODE_LOOKUP T901 WHERE T903_UPLOAD_FILE_LIST.C901_REF_GRP = T901.C901_CODE_ID) || '/' || C903_REF_ID || '/' || C901_REF_TYPE || '/' || C901_TYPE || '/' || C903_FILE_NAME WHERE T903_UPLOAD_FILE_LIST.C901_TYPE NOT IN ('4000790')", [],
            function(tx, rs) {
                db.transaction(function(tx, rs) {
                    tx.executeSql(query, [],
                        function (tx, rs) {
                            if (!master && fileIDs.length > 0) {
                                fnRemoveSyncedFiles();
                            } else {
                                callbackFn("success");
                            }
                        },
                        function(tx, err) {
                            showNativeAlert(lblNm("msg_sync_app"), lblNm("msg_priority_clash"));
                        });
                });
            },
            function(tx, err) {
                showNativeAlert(lblNm("msg_sync_app"), lblNm("msg_priority_clash"));
            });
    });



}

function fnRemoveSyncedFiles() {
    var searchTerm = [];
    var i = 0;
    for (i = 0;
        ((i < fileIDs.length) && (i < 500)); i++) {
        searchTerm.push("C903_UPLOAD_FILE_LIST = '" + fileIDs[i] + "'");
    }
    var query = searchTerm.join(" OR ");

    query = "SELECT C903_FILE_PATH FROM T903_UPLOAD_FILE_LIST WHERE " + query;

    db.transaction(function (tx, rs) {
        tx.executeSql(query, [],
            function (tx, rs) {
                var k = 0;
                results = [];
                for (k = 0; k < rs.rows.length; k++)
                    fnRemoveFile(rs.rows.item(k).C903_FILE_PATH);
                fileIDs.splice(0, i);
                if (fileIDs.length > 0)
                    fnRemoveSyncedFiles()
                else {
                    callbackFn("success");
                }
            },
            function (tx, err) {
                console.log(JSON.stringify(err));
                alert('error');
            });
    });
}

// function fnConfigPathFor (currFile,callback) {
//        var originalPath = '';
//        db.transaction(function (tx, rs) {
//             tx.executeSql("SELECT T901.C901_CODE_NM || '/' || T903.C903_REF_ID || '/' || T903.C901_REF_TYPE || '/' || T903.C901_TYPE || '/' || T903.C903_FILE_NAME AS REFGROUP FROM T903_UPLOAD_FILE_LIST T903, T901_CODE_LOOKUP T901 WHERE T903.C901_REF_GRP = T901.C901_CODE_ID AND T903.C903_UPLOAD_FILE_LIST = ?", [currFile],
//               function(tx,rs) {
//                 if(rs.rows.length == 0) {
//                   showNativeAlert('Cannot Sync Files without Syncing App','Sync Priority Error','OK',callbackFn);
//                 }
//                 else {
//                   originalPath += rs.rows.item(0).REFGROUP;

//                   db.transaction(function (tx, rs) {
//                       tx.executeSql("UPDATE T903_UPLOAD_FILE_LIST SET C903_FILE_PATH = ? WHERE C903_UPLOAD_FILE_LIST = ?", [originalPath,currFile],
//                         function(tx,rs) {
//                           if(!master)
//                             fnRemoveFile(originalPath);
//                           callback();
//                       },
//                       function(tx,err) {console.log(JSON.stringify(err)); alert('error');
//                     });
//                   });

//               }
//             },
//             function(tx,err) {console.log(JSON.stringify(err)); alert('error');
//           });
//       });
// }


function fnRetrieveStatus(postData, fromDate, toDate, callback) {
    console.log(URL_WSServer + "share/sharedStatus");
    // fnGetWebServerData("share/sharedStatus", undefined, postData, function(data, model, response, errorReport) {
    //     if (model == undefined) {
    //         console.log(data);
    //         arrMain = new Array();
    //         if (data.listGmShareEngineVO != undefined) {
    //             processStatus(0, data.listGmShareEngineVO, callback);
    //         }
    //         // callback(0,data.listGmShareEngineVO);
    //         else {
    //             showNativeAlert(lblNm("msg_artifacts_shared"), lblNm("msg_no_emails"));
    //             //            showMsgView("011");
    //         }

    //     } else {
    //         console.log("Nothing to be Sync'd from the Server with URL ");
    //         callback(response);
    //     }
    // }, true);

    var userid = localStorage.getItem("userID");

    fnGetWebServerviceCloudData(Azure_Cloud_MC_URL+"marketing/sharedStatus/"+userid+"/"+fromDate+"/"+toDate, undefined, function (data, response) {
        if (response != "error") {
            console.log(data);
            arrMain = new Array();
            processStatus(0, data, callback);
        } else {
            console.log("Nothing to be Sync'd from the Server with URL ");
           	callback("error");
            showError(lblNm("msg_try_after"));
        }
    });

}

function processStatus(iValue, data, callback) {

    var len;
    if (data.length == undefined) {
        var currArray = new Array();
        currArray.push(data);
        data = currArray;
        len = 1;
    } else
        len = data.length;

    if (iValue < len) {
        var jsnMain = {};
        jsnMain.Subject = data[iValue].subject;

		//PC-4134: To remove the split logic - Date values get from directly from JSON
        //var arrSentDate = data[iValue].emailsentdate.split('-');
		//jsnMain.MailDate = arrSentDate[1] + "-" + arrSentDate[2] + "-" + arrSentDate[0];
		
        jsnMain.MailDate = data[iValue].emailsentdate;

        fnGetCodeNM(data[iValue].sharestatus, function (status) {
            jsnMain.Status = status;
            prepareMailList(0, data[iValue].gmSharedEmailVO, [], [], function (mailList) {
                jsnMain.MailList = mailList;
                prepareFileList(0, data[iValue].gmSharedFileVO, [], [], [], function (fileList) {
                    jsnMain.FileList = fileList;
                    arrMain.push(jsnMain);

                    processStatus(iValue + 1, data, callback);
                });
            });

        })
    } else {
        callback(arrMain);
    }
}

function prepareMailList(iValue, data, toList, ccList, callback) {

    var len;
    if (data.length == undefined) {
        var currArray = new Array();
        currArray.push(data);
        data = currArray;
        len = 1;
    } else
        len = data.length;

    if (iValue < len) {
        var jsnMailList = {};
        jsnMailList.Email = data[iValue].emailid;
        var strStatus = ""
        if (data[iValue].emailstatus != "")
            strStatus = data[iValue].emailstatus

        fnGetCodeNM(data[iValue].recipienttype, function (type) {
            fnGetCodeNM(strStatus, function (status) {
                jsnMailList.Type = type;
                if (status != '')
                    jsnMailList.status = status;
                else
                    jsnMailList.status = 'Not Yet Processed';
                fnGetParty(data[iValue].refid, jsnMailList.Email, function (Name) {
                    if (data[iValue].username != '')
                        jsnMailList.Name = data[iValue].username + " - ";
                    else if ((Name != undefined))
                        jsnMailList.Name = Name.LastName + ", " + Name.FirstName + " - ";
                    if (type == 'TO')
                        toList.push(jsnMailList);
                    else
                        ccList.push(jsnMailList);
                    prepareMailList(iValue + 1, data, toList, ccList, callback);
                });
            });
        });
    } else {
        var mailList = {};
        mailList.To = toList;
        mailList.Cc = ccList;
        callback([mailList]);
    }
}

function prepareFileList(iValue, data, documents, images, videos, callback) {

    var len;
    if (data.length == undefined) {
        var currArray = new Array();
        currArray.push(data);
        data = currArray;
        len = 1;
    } else
        len = data.length;

    if (iValue < len) {
        fnGetFileDetail(data[iValue].updfileid, function (file) {
            if (file != undefined) {
                var TypeID = file.TypeID;
                if ((TypeID == '4000441') || (TypeID == '103114'))
                    documents.push(file);
                else if (TypeID == '103112')
                    images.push(file);
                else
                    videos.push(file);
            }

            prepareFileList(iValue + 1, data, documents, images, videos, callback);
        });
    } else {
        var fileList = {};
        fileList.Documents = documents;
        fileList.Images = images;
        fileList.Videos = videos;
        callback([fileList]);
    }
}

// This function used for eamil share the marketing Collateral documents
function sendEmail(JSONData, callback) {
    fnGetWebServerviceCloudData(Azure_Cloud_MC_URL+"marketing/shareEmail", JSONData, function (data,response) {
       console.log(data);
       console.log(response);
       if (response == "error") {
            callback("error");
            showError(lblNm("msg_try_after"));
        }
        else {
             callback(data);
       }
    });
}

function reSendEmail(iValue, results) {

    if (iValue < results.length) {
        var currEmail = results[iValue];
        var JSONData = $.parseJSON(currEmail.JSON);
        JSONData.token = localStorage.getItem('token');
        var strJSONData = JSON.stringify(JSONData);

        fnUpdateJSON(currEmail.SHARE_ID, strJSONData, function () {
            sendEmail(strJSONData, function (data) {
                if (data != 'error') {
                    fnUpdateStatus(data.shextrefid, data.shareid, data.sharestatus, function () {
                        reSendEmail(iValue + 1, results);
                    });
                } else {
                    reSendEmail(iValue + 1, results);
                }
            });
        });

    }
}


function fnRunShareRuleUpdate(callback) {
    try {
        fnGetShareRules(function(rules) {
            fnUpdateShareRule(0, rules, callback);
        });
    } catch (err) {
        showAppError(err, "GmDataSync:fnRunShareRuleUpdate()");
    }
}

function fnUpdateShareRule(iValue, rules, callback) {
    if (iValue < rules.length) {
        db.transaction(function (tx, rs) {
            tx.executeSql("UPDATE T903_UPLOAD_FILE_LIST SET C903_SHARE_RULE = ? WHERE C901_TYPE = ?", [rules[iValue].VALUE, rules[iValue].ID],
                function (tx, rs) {
                    fnUpdateShareRule(iValue + 1, rules, callback);
                },
                function(e, err) {
                    showAppError(err, "GmDataSync:fnUpdateShareRule()");

                    alert('error');
                    console.log(JSON.stringify(err));
                });
        });
    } else
        callback();
}

//Loads the Server Data for Sales Dashboard
function fnGetServerData(seviceurl, voname, input, callback) {
    var token = localStorage.getItem("token");
    if (input == undefined)
        input = {
            "token": token
        };


    fnGetWebServerData(seviceurl, voname, input, function(data, model, response, errorReport) {
        console.log("GmDataSync:fnGetServerData() >>>>>>>>>>");
        if (model == undefined) {
            if (eval("data." + voname) != undefined) {
                callback(eval("data." + voname));
            } else {
                callback(data);

            }
        } else {
            console.log("Nothing to be Sync'd from the Server with URL " + URL_WSServer + seviceurl);
            $(".div-dash-page-loader").hide();
            $(".div-detail").hide();
            $("#div-dash-nointernet-connection").show();
            $("#div-dash-nointernet-connection").html(" <div> <i class='fa fa-exclamation-triangle fa-lg'></i></div>"+lblNm("msg_server_not_reachable"));
        }
    }, true);

}

//Achieving Handshake while coming from Offline to Online
function syncDOPdf(data) {
	try{
	    var token = localStorage.getItem("token");
	    if (data.length != 0)
	        var input = {};
	    var that= this;
	    input.gmSyncPDFInfoVO = data;
	    fnGetWebServerData("DOPdf/sync", undefined, input, function(data, model, response, errorReport) {
	        if (model == undefined) {
	            _.each(data.gmSyncPDFInfoVO, function (data) {
	                if (data.pdffl == "N")	
	                {		
	                    var orderid = data.doid;
	                    var orderdate = "";
	                   fnGetDOInfo(orderid, function(doinfo) {
	                        that.JSONData = $.parseJSON(doinfo.JSON);
	                        orderdate = that.JSONData.orderdate;
	                        var input = {
	                            "token": token,
	                            "orderdate": orderdate,
	                            "prefixordid": orderid
	                           };
	                        fnGetWebServiceData("DeliveredOrder/getlatestdo", undefined, input, function (status, Dodata) {
	                            if (status) {
	                                if (Dodata.orderid == "" && Dodata.orderid == undefined) {
	                                    iterateOffline(0, data)
	                                }
	                            }else{
	                                    iterateOffline(0, data)
	                            }
	                        });
	                    });
	               }
	            });
	        } else {
	            console.log("Failure PDF");
	        }
	    }, true);
	}catch (err) {
        showAppError(err, "GmDataSync:syncDOPdf()");
    }
}
//If upload PDF -> save in Local table  else failure->try 3 times here so totally 6 and Update Localtable 
function iterateOffline(n, data) {
    uploadPDF(data.doid, function(uploadStatus) {
        var time = new Date();
        if (uploadStatus) {
            //enters here when upload is success so set PDFFL to Y
            fnInsertPdfFlagInfo(data.doid, data.doid + ".pdf", "Y", n + 4);
        } else if (n == eval(localStorage.getItem("pdffailurecnt")) - 1) {
            //enters here when upload is failyre so set PDFFL to N
            fnInsertPdfFlagInfo(data.doid, data.doid + ".pdf", "N", n + 4);
        } else
            iterateOffline(n + 1, data);
    });
}

//book DO
function bookDO(DO, callback) {
    for (key in DO) {
         if (DO.hasOwnProperty(key)) {
                 delete DO['arrDORecordappendData'];
                 delete DO['arrDORecordPartVO'];
         }
    }
    try{
    var token = localStorage.getItem("token");
    var arrGmOrderVO = new Array();

    fnSplitDO(DO, function (finalDO) {
        if (finalDO.length != undefined)
            arrGmOrderVO = finalDO;
        else
            arrGmOrderVO.push(finalDO);
        var dateFormat = localStorage.getItem("cmpdfmt");
        var postData = {
            "token": token,
            "arrGmOrderVO": arrGmOrderVO,
            "uuid": deviceUUID
        };
        fnGetOldDOID(postData.arrGmOrderVO[0].orderid,function(olddoid){
            console.log(olddoid.OLDDOID)
            if (olddoid.OLDDOID != ''){
            postData.arrGmOrderVO[0].ordercomments= postData.arrGmOrderVO[0].ordercomments + ".Old Order ID is " + olddoid.OLDDOID ;
            }
            postData.arrGmOrderVO[0].orderdate = formattedDate(postData.arrGmOrderVO[0].orderdate, localStorage.getItem("cmpdfmt"));
        postData.arrGmOrderVO[0].arrGmOrderAttrVO = _.each(postData.arrGmOrderVO[0].arrGmOrderAttrVO, function (data) {
            if (data.attrtype == "103601") //Surgery Date on billing info tab
                data.attrvalue = formattedDate(data.attrvalue, dateFormat);
        });
        postData.arrGmOrderVO[0].arrDoSurgeryDetailVO = _.each(postData.arrGmOrderVO[0].arrDoSurgeryDetailVO, function (data) {
            if (data.attrtype == "10304734") //AckOrderOn on Surgery Detail tab
                data.attrvalue = formattedDate(data.attrvalue, dateFormat);
            if (data.attrtype == "400151") //Date of surgery on Surgery Detail tab
                data.attrvalue = formattedDate(data.attrvalue, dateFormat);
        });
        _.each(postData.arrGmOrderVO[0].arrDoNPIDetailVO, function (data) {
             if (isNaN(data.id)) {
                 data.id = "";
              }
            fnSaveNPISurgeonDetail(postData.arrGmOrderVO[0].acctid,postData.arrGmOrderVO[0].repid,data.id,data.name,postData.arrGmOrderVO[0].orderdate);
        });
            
        postData.arrGmOrderVO[0].arrDoNPIDetailVO = _.each(postData.arrGmOrderVO[0].arrDoNPIDetailVO, function (data) {
            if (isNaN(data.id)) {
                data.id = "";
             }
        });
            
        postData.arrGmOrderVO[0].arrGmOrderItemVO = _.each(postData.arrGmOrderVO[0].arrGmOrderItemVO, function (item) {
            delete item["usedFl"];
        });

        console.log("Post Data Final" + JSON.stringify(postData));
            
        fnGetWebServerData("DeliveredOrder/syncdo", undefined, postData, function(data, model, response, errorReport) {
            if (model == undefined) {
                callback(1, data);
            } else {
                callback(0, model.responseText);
            }
        }, true);
        });
        
    });
      
    } catch (err) {
        showAppError(err, "GmDataSync:bookDO():DeliveredOrder/syncdo");
    }
}
//AutoBookDO will book all the DOS that are booked Offline one by One
function fnAutoBookDO(iValue, JSONData) {
	try{
	    if (iValue < JSONData.length) {
	        var parentJSON = $.parseJSON(JSONData[iValue].JSON);
	        fnShowStatus(lblNm("msg_booking_do_c") + parentJSON.orderid + "...");
	        fnGeneratePDF(parentJSON.orderid, parentJSON.orderid, undefined, $.parseJSON(JSONData[iValue].OTHER_INFO), parentJSON.repid, function () {
	                var token = localStorage.getItem("token");
	                var orderid = parentJSON.orderid;
	                var orderdate = parentJSON.orderdate;
	                var input = {
	                    "token": token,
	                    "orderdate": orderdate,
	                    "prefixordid": orderid
	                   };
	
	                 fnGetWebServiceData("DeliveredOrder/getlatestdo", undefined, input, function (status, Dodata) {
	                                if (status) {
	                                    if (Dodata.orderid == "" || Dodata.orderid == undefined) {
	                                         //PMT-12763 Achieving Handshake For DO PDF sync
	                                         iterate(0, parentJSON, iValue, JSONData);
	                                    }else{
											saveDO(parentJSON, iValue, JSONData);
										}
	                                }else{
	                                       //PMT-12763 Achieving Handshake For DO PDF sync
	                                         iterate(0, parentJSON, iValue, JSONData);
	                                }
	                 });
	        });
	    }
	}catch (err) {
        showAppError(err, "GmDataSync:fnAutoBookDO()");
    }
}

//If upload PDF ->Success->BooDO else failure->try 3 times totally to BookDO and if failure for three times allow user to BookDO() 
function iterate(n, parentJSON, iValue, JSONData) {
    uploadPDF(parentJSON.orderid, function(uploadStatus) {
        var time = new Date();
        if (uploadStatus) {
            fnInsertPdfFlagInfo(parentJSON.orderid, parentJSON.orderid + ".pdf", parentJSON.pdffl, n + 1);
            parentJSON.arrGmOrderAttrVO.push({
                "orderid": parentJSON.orderid,
                "attrtype": "26230806",
                "attrvalue": "Y"
            });
            parentJSON.arrGmOrderAttrVO.push({
                "orderid": parentJSON.orderid,
                "attrtype": "26230805",
                "attrvalue": n + 1
            });
            saveDO(parentJSON, iValue, JSONData);
        } else if (n == eval(5) - 1) {
            parentJSON.arrGmOrderAttrVO.push({
                "orderid": parentJSON.orderid,
                "attrtype": "26230806",
                "attrvalue": "N"
            });
            parentJSON.arrGmOrderAttrVO.push({
                "orderid": parentJSON.orderid,
                "attrtype": "26230805",
                "attrvalue": n + 1
            });
            saveDO(parentJSON, iValue, JSONData);
        } else
            iterate(n + 1, parentJSON, iValue, JSONData);
    });
}
//Books DO 
function saveDO(parentJSON, iValue, JSONData) {
    bookDO(parentJSON, function (status, data) {
        if (status) {
            fnAutoBookDO(iValue + 1, JSONData);
            fnRemoveFile("DO/Signature/" + parentJSON.orderid + ".txt");
            fnUpdateDOStatus(parentJSON.orderid, 'Pending CS Confirmation');
            console.log("TAG SAVING OFFLINE=== "+parentJSON.orderid);
            fnSaveTagsForPendingSyncDO(); //To Add the tag(s) processed from the image to the server for Offline DO's
            fnShowStatus(parentJSON.orderid + " booked successfully!");
        } else {
            data = $.parseJSON(data);
            if (data.message == "DO already entered in the System") {
                var oldDOID = parentJSON.orderid;
                var orderdate = parentJSON.orderdate;
                var serverseq, localseq, seq;
                var DOFormat = oldDOID.split("-")[0] + "-" + oldDOID.split("-")[1] + "-";
                fnCheckDOID(DOFormat, function (DOs) {
                    var currSeq = (DOs.length) ? (_.max(_.pluck(DOs, 'DOID'), function (id) {
                        return parseInt(id.split('-')[2]);
                    })) : "0";
                    localseq = parseInt(currSeq.replace(DOFormat, "")) + 1;
                    var prefixordid = oldDOID.split("-")[0] + "-" + oldDOID.split("-")[1];
                    var input = {
                        "token": localStorage.getItem("token"),
                        "orderdate": orderdate,
                        "prefixordid": prefixordid
                    };
                    fnGetWebServiceData("DeliveredOrder/getlatestdo", undefined, input, function (status, data) {
                        if (status) {
                            if (data.orderid != "") {
                                var orderid = data.orderid;
                                serverseq = orderid.split("-")[2];
                                serverseq = parseInt(serverseq) + 1;
                            } else {
                                serverseq = 1;
                            }

                            if (serverseq == localseq)
                                seq = serverseq;
                            else if (serverseq > localseq)
                                seq = serverseq;
                            else
                                seq = localseq;
                        } else {
                            seq = localseq;
                        }

                        var newDOID = oldDOID.split("-")[0] + "-" + oldDOID.split("-")[1] + "-" + seq;
                        showNativeConfirm(lblNm("msg_do_id_s")+ oldDOID + lblNm("msg_s_prefer_update") + newDOID + "?", lblNm("msg_do_id_conflict"), [lblNm("msg_yes"), lblNm("msg_no")], function (index) {
                            if (index == 1) {
                                fnGeneratePDF(parentJSON.orderid, newDOID, undefined, $.parseJSON(JSONData[iValue].OTHER_INFO), $.parseJSON(JSONData[iValue].JSON).repid, function () {
                                    var exp = new RegExp(oldDOID, 'g');
                                    var str = JSONData[iValue].JSON;
                                    JSONData[iValue].JSON = str.replace(exp, newDOID);
                                    fnDeleteDO(oldDOID);
                                    fnSaveDO(newDOID, parentJSON.acctid, str.replace(exp, newDOID), JSONData[iValue].JSONParts, JSONData[iValue].COVERINGREPFL, JSONData[iValue].SIGNATURE, JSONData[iValue].ADDITIONAL_INFO, JSONData[iValue].OTHER_INFO);
                                    fnUpdateOldDoID(newDOID, localStorage.getItem("OLDDOID"));
                                    fnUpdateDOStatus(newDOID, 'Pending Sync');
                                    //This is related to Read Tag from Image Changes and this code is to Update the Order id to the Tag Image related tables
                                    fnUpdDOTagImgDtls(oldDOID, newDOID);
                                    fnUpdDOTagDetails(oldDOID, newDOID);
                                    fnCheckPendingSync();
                                });
                            } else {
                                fnUpdateDOStatus(oldDOID, 'DRAFT');
                                //                                  fnShowStatus("");

                            }

                        });
                    });
                });
            } else if (data.message.indexOf("A DO with the same Account-Part-Quantity combination already exists with ID") >= 0) {
                
                 var replacemessage = data.message.replace("A DO with the same Account-Part-Quantity combination already exists with ID",lblNm("msg_do_with_same_account"));
                
                
                showNativeConfirm(replacemessage + lblNm("msg_want_continue_do"), lblNm("msg_duplicate_order"), [lblNm("msg_yes"), lblNm("msg_no")], function (index) {
                    if (index == 1) {
                        parentJSON.skipvalidatefl = 'Y'
                        fnSaveDO(parentJSON.orderid, parentJSON.acctid, JSON.stringify(parentJSON), JSONData[iValue].JSONParts, JSONData[iValue].COVERINGREPFL, JSONData[iValue].SIGNATURE, JSONData[iValue].ADDITIONAL_INFO, JSONData[iValue].OTHER_INFO);
                        fnUpdateOldDoID(parentJSON.orderid, localStorage.getItem("OLDDOID"));
                        fnUpdateDOStatus(parentJSON.orderid, 'Pending Sync');
                        fnCheckPendingSync();
                    } else {
                        fnDeleteDO(parentJSON.orderid);
                        //This is related to Read Tag from Image Changes and this code is to Update the Order id to the Tag Image related tables
                        //Delete when they click NO
                        fnDelDOFromTagImgDtls(parentJSON.orderid);
                        fnDelTagsForProcessImage(parentJSON.orderid);
                    }
                });
            } else if (data.message.indexOf("Control Number") >= 0) {
                fnUpdateDOStatus(parentJSON.orderid, 'DRAFT');
                showNativeConfirm(lblNm("msg_invalid_lot") + parentJSON.orderid + lblNm("msg_want_change_now")+"\n", lblNm("msg_error_on") + parentJSON.orderid, [lblNm("msg_yes"), lblNm("msg_no")], function (index) {
                    if (index == 1)
                        window.location.href = "#do/" + parentJSON.orderid;
                });
            } else if (data.message.indexOf("valid tag") >= 0) {
                fnUpdateDOStatus(parentJSON.orderid, 'DRAFT');
                showNativeConfirm(lblNm("msg_invalidtag") + parentJSON.orderid + lblNm("msg_want_change_now")+"\n", lblNm("msg_error_on") + parentJSON.orderid, [lblNm("msg_yes"), lblNm("msg_no")], function (index) {
                    if (index == 1)
                        window.location.href = "#do/" + parentJSON.orderid;
                });
            } else if (data.errorCode == "20643") {
                DataSync_SL('listGmAccountVO', '', function (status) {
                    if (status != "success") {
                        fnAutoBookDO(iValue + 1, JSONData);
                    } else {
                        fnGetAccountInfo(parentJSON.acctid, function (data) {
                            var oldrep = parentJSON.repid;
                            parentJSON.repid = data.REP;
                            var oldID = parentJSON.orderid;
                            parentJSON.orderid = parentJSON.orderid.replace(oldrep, parentJSON.repid);
                            fnDeleteDO(oldID);
                            fnSaveDO(parentJSON.orderid, parentJSON.acctid, JSON.stringify(parentJSON), JSONData[iValue].JSONParts, JSONData[iValue].COVERINGREPFL, JSONData[iValue].SIGNATURE, JSONData[iValue].ADDITIONAL_INFO, JSONData[iValue].OTHER_INFO);
                            fnUpdateOldDoID(parentJSON.orderid, localStorage.getItem("OLDDOID"));
                            fnUpdateDOStatus(newDOID, 'Pending Sync');
                            fnGeneratePDF(parentJSON.orderid, newDOID, data, $.parseJSON(JSONData[iValue].OTHER_INFO), parentJSON.repid, function () {
                            fnCheckPendingSync();
                            });
                            //This is related to Read Tag from Image Changes and this code is to Update the Order id to the Tag Image related tables (DOUBLE CHECK)
                            fnUpdDOTagImgDtls(oldID, parentJSON.orderid);
                            fnUpdDOTagDetails(oldID, parentJSON.orderid);
                        });
                    }
                });
            } else if ((data.message != undefined) && (data.message != "")) {
                fnUpdateDOStatus(parentJSON.orderid, 'DRAFT');
                showNativeAlert(data.message + lblNm("msg_do_in_draft")+"\n", lblNm("msg_error_in"), [lblNm("msg_ok")], function () {
                    fnAutoBookDO(iValue + 1, JSONData);
                });
            } else {
                fnAutoBookDO(iValue + 1, JSONData);
            }
        }

    });
}

function fnGeneratePDF(orderID, newDOID, RepInfo, otherInfo, repid, callback) {
    var that = this;
    var template_wp = fnGetTemplate(URL_DO_Template, "GmDOPDFLayout" + localStorage.getItem("cmpid"));
    var template_wop = fnGetTemplate(URL_DO_Template, "GmDOPDFLayoutWithOutPrice" + localStorage.getItem("cmpid"));
    if (template_wp == undefined)
        template_wp = fnGetTemplate(URL_DO_Template, "GmDOPDFLayout1000");
    if (template_wop == undefined)
        template_wop = fnGetTemplate(URL_DO_Template, "GmDOPDFLayoutWithOutPrice1000");

    if (otherInfo.HCAAccount) {
        template_wp = fnGetTemplate(URL_DO_Template, "GmDOHCALayout");
        template_wop = fnGetTemplate(URL_DO_Template, "GmDOHCALayoutWithOutPrice");
    }
    fnReadFile("Globus/DO/Signature/" + orderID + ".txt", function (data) {
        var JSONPDF = $.parseJSON(data);
        // var newDOID = orderID.split("-")[0]+ "-" + orderID.split("-")[1] + "-" + (parseInt(orderID.split("-")[2]) + 1);
        JSONPDF[0].DOID = newDOID;
        if (RepInfo != undefined) {
            JSONPDF[0].RepID = RepInfo.REP;
            JSONPDF[0].FirstName = RepInfo.RepName.split(" ")[0];
            JSONPDF[0].LastName = RepInfo.RepName.split(" ")[1];
        }
        //      fnGetDORepContactInfo(repid, function(status, contactInfo) {
        //          if(status) {
        //              JSONPDF[0].email = contactInfo.email;
        //              JSONPDF[0].phone = contactInfo.phone;
        //          }
        createPDFPath(function () {
            createPDF(template_wp(JSONPDF),"WP/", JSONPDF[0].DOID + ".pdf", // on iOS,
                function (status) {
                     if (parseNull(devicePlatform) == "") {
                          devicePlatform = "ios";
                     }
                     if (parseNull(androidDevicePlatform) == "") {
                           androidDevicePlatform = "android";
                     }
                     if (devicePlatform.toLowerCase().includes(androidDevicePlatform)){
                         that.wppath = status;
                     }else{
                         that.wppath = "file://"+status.match(/\(([^)]+)\)/)[1]; //Check in IOS Method
                     }

                    /* Set the pdf path to a GLobal variable in DO offline mode as a temporary fix for PMT-13726
                     */
                    GM.Global.WPPdfPath = that.wppath;
                    if (orderID != JSONPDF[0].DOID)
                        fnRemoveFile("DO/PDF/WP/" + orderID + ".pdf");
                    createPDF(template_wop(JSONPDF),"WOP/", JSONPDF[0].DOID + ".pdf", // on iOS,
                        function (status) {
                            if (orderID != JSONPDF[0].DOID)
                                fnRemoveFile("DO/PDF/WOP/" + orderID + ".pdf");
                            console.log("DO PDF was generated successfully");
                            fnWriteFile("Globus/DO/Signature", newDOID + ".txt", JSON.stringify(JSONPDF), callback);
                        },
                        function (status) {
                            console.log(status);
                        });

                },
                function (status) {
                    console.log(status);
                });
        });
        //      });

    });
}


//DO Split Logic
function fnSplitDO(DO, callback) {
    var NoOfShips = _.uniq(_.pluck(_.pluck(DO.gmTxnShipInfoVO, "ShipInfo"), "addressid"));
    var PartTypes = _.uniq(_.pluck(DO.arrGmOrderItemVO, "parttype"));

    // window.DO =DO;

    var OrderExp = new RegExp(DO.orderid, 'g');
    var finalDO = new Array();

    // If the Shipping Address is more that one and atleast one part is in Consignment type (50300)
    if ((NoOfShips.length > 1) && (_.contains(PartTypes, '50300'))) {

        for (var i = 0; i < NoOfShips.length; i++) {

            //Preparing Child Order
            if (i < (NoOfShips.length - 1)) {
                var childDO = $.parseJSON(JSON.stringify(DO));

                delete childDO['arrGmOrderItemAttrVO'];
                delete childDO['arrGmOrderItemVO'];
                delete childDO['arrGmTagUsageVO'];
                delete childDO['gmTxnShipInfoVO'];

                //Prepare Shipping Info
                var ShippingInfo = {},
                    index = new Array();

                for (var s = 0; s < DO.gmTxnShipInfoVO.length; s++) {
                    if (DO.gmTxnShipInfoVO[s].ShipInfo.addressid == NoOfShips[i]) {
                        ShippingInfo = $.parseJSON(JSON.stringify(DO.gmTxnShipInfoVO[s].ShipInfo));
                        index.push(DO.gmTxnShipInfoVO[s].indexValue);
                    }
                }
                delete ShippingInfo["indexValue"];
                delete ShippingInfo["carrierName"];
                delete ShippingInfo["modeName"];

                if (ShippingInfo.shipto != "4121")
                    ShippingInfo.addressid = "";

                //Preparing Parts Info
                var PartsInfo = $.parseJSON(JSON.stringify(DO.arrGmOrderItemVO));
                var childParts = new Array();
                var total = 0;
                for (var p = 0; p < PartsInfo.length; p++) {
                    if (index.indexOf(PartsInfo[p].indexValue) >= 0) {
                        childParts.push(PartsInfo[p]);
                        var strChild = JSON.stringify(PartsInfo[p]);
                        DO.arrGmOrderItemVO = $.parseJSON(JSON.stringify(DO.arrGmOrderItemVO).replace(strChild, '').replace(',,', ',').replace('[,', '[').replace(',]', ']'));
                        delete PartsInfo[p]["indexValue"];
                        total += parseFloat(parseFloat(PartsInfo[p].price) * parseInt(PartsInfo[p].qty));
                    }
                }
                console.log(_.difference(DO.arrGmOrderItemVO, childParts));

                //Preparing Parts Attr Info
                var PartsAttrInfo = $.parseJSON(JSON.stringify(DO.arrGmOrderItemAttrVO));
                var childAttr = new Array();
                for (var a = 0; a < PartsAttrInfo.length; a++) {
                    if (_.contains(index, PartsAttrInfo[a].indexValue)) {
                        childAttr.push(PartsAttrInfo[a]);
                        var strChild = JSON.stringify(PartsAttrInfo[a]);
                        DO.arrGmOrderItemAttrVO = $.parseJSON(JSON.stringify(DO.arrGmOrderItemAttrVO).replace(strChild, '').replace(',,', ',').replace('[,', '[').replace(',]', ']'));
                        delete PartsAttrInfo[a]["indexValue"];
                    }
                }

                //Preparing TAG Info
                var TagInfo = $.parseJSON(JSON.stringify(DO.arrGmTagUsageVO));
                var childTag = new Array();
                for (var t = 0; t < TagInfo.length; t++) {
                    if (index.indexOf(TagInfo[t].indexValue)) {
                        childTag.push(TagInfo[t]);
                        var strChild = JSON.stringify(TagInfo[t]);
                        DO.arrGmTagUsageVO = $.parseJSON(JSON.stringify(DO.arrGmTagUsageVO).replace(strChild, '').replace(',,', ',').replace('[,', '[').replace(',]', ']'));
                        delete TagInfo[t]["indexValue"];
                        delete TagInfo[t]["Name"];
                    }
                }

                //Order Attr
                var orderAttr = $.parseJSON(JSON.stringify(DO.arrGmOrderAttrVO));
                for (var o = 0; o < orderAttr.length; o++) {
                    if (orderAttr[o].attrtype == '4000536')
                        delete orderAttr.splice(o, 1);
                }

                //Combining child Order data
                childDO.gmTxnShipInfoVO = ShippingInfo;
                childDO.arrGmOrderItemVO = childParts;
                childDO.arrGmOrderItemAttrVO = childAttr;
                childDO.arrGmTagUsageVO = childTag;
                childDO.arrGmOrderAttrVO = orderAttr;
                childDO.total = total;


                childDO = $.parseJSON(JSON.stringify(childDO).replace(OrderExp, ''));
                childDO.parentorderid = DO.orderid;

                finalDO.push(childDO);
                console.log(DO);
            }

            //Preparing Parent Order
            else {
                //Prepare Shipping Info
                var ShippingInfo = {};

                for (var s = 0; s < DO.gmTxnShipInfoVO.length; s++) {
                    if (DO.gmTxnShipInfoVO[s].ShipInfo.addressid == NoOfShips[i]) {
                        ShippingInfo = $.parseJSON(JSON.stringify(DO.gmTxnShipInfoVO[s].ShipInfo));
                    }
                }
                delete ShippingInfo["indexValue"];
                delete ShippingInfo["carrierName"];
                delete ShippingInfo["modeName"];


                if (ShippingInfo.shipto != "4121")
                    ShippingInfo.addressid = "";

                var total = 0;
                for (var x = 0; x < DO.arrGmOrderItemVO.length; x++) {
                    delete DO.arrGmOrderItemVO[x]["indexValue"];
                    total += parseFloat(parseFloat(DO.arrGmOrderItemVO[x].price) * parseInt(DO.arrGmOrderItemVO[x].qty));
                }
                DO.total = total;

                for (var x = 0; x < DO.arrGmOrderItemAttrVO.length; x++) {
                    delete DO.arrGmOrderItemAttrVO[x]["indexValue"];
                }

                for (var x = 0; x < DO.arrGmTagUsageVO.length; x++) {
                    delete DO.arrGmTagUsageVO[x]["indexValue"];
                    delete DO.arrGmTagUsageVO[x]["Name"];
                }

                DO.gmTxnShipInfoVO = $.parseJSON(JSON.stringify(ShippingInfo));

                DO.total = total;

                finalDO.push(DO);
            }

        }
        callback(finalDO.reverse());
    } else { // Creating Order with only one Shipping Address or All loaners (No Split incorporated)
        var total = 0;
        for (var x = 0; x < DO.arrGmOrderItemVO.length; x++) {
            delete DO.arrGmOrderItemVO[x]["indexValue"];
            total += parseFloat(parseFloat(DO.arrGmOrderItemVO[x].price) * parseInt(DO.arrGmOrderItemVO[x].qty));
        }
        DO.total = total;

        for (var x = 0; x < DO.arrGmOrderItemAttrVO.length; x++) {
            delete DO.arrGmOrderItemAttrVO[x]["indexValue"];
        }

        for (var x = 0; x < DO.arrGmTagUsageVO.length; x++) {
            delete DO.arrGmTagUsageVO[x]["indexValue"];
            delete DO.arrGmTagUsageVO[x]["Name"];
        }

        if ((DO.gmTxnShipInfoVO.length != undefined) && (DO.gmTxnShipInfoVO[0] != undefined))
            DO.gmTxnShipInfoVO = DO.gmTxnShipInfoVO[0].ShipInfo;
        else
            DO.gmTxnShipInfoVO = DO.gmTxnShipInfoVO;

        if (DO.gmTxnShipInfoVO.source == undefined)
            DO.gmTxnShipInfoVO = {
                "source": "",
                "shipto": "",
                "ship_to_id": "",
                "shipcarrier": "",
                "shipmode": "",
                "addressid": "",
                "attnto": "",
                "shipinstruction": ""
            };


        delete DO.gmTxnShipInfoVO["indexValue"];
        delete DO.gmTxnShipInfoVO["carrierName"];
        delete DO.gmTxnShipInfoVO["modeName"];


        if (DO.gmTxnShipInfoVO.shipto != "4121")
            DO.gmTxnShipInfoVO.addressid = "";

        finalDO.push(DO);
        callback(finalDO);

    }

}
//Loads the Server Data for DO
function fnGetWebServiceData(seviceurl, voname, input, callback) {
    var token = localStorage.getItem("token");
    if (input == undefined)
        input = {
            "token": token
        };
    input.cmpid = localStorage.getItem("cmpid");
    input.cmptzone = localStorage.getItem("cmptzone");
    input.cmpdfmt = localStorage.getItem("cmpdfmt");
    input.plantid = localStorage.getItem("plantid");

    $.ajax({
        url: URL_WSServer + seviceurl,
        method: "POST",
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(input),
        success: function (data) {
            if (voname == undefined) {
                callback(1, data);
            } else if (data != null && eval("data." + voname) != undefined && eval("data." + voname) != "") {
                callback(1, eval("data." + voname));
            } else {
                callback(0, data);
            }

        },
        error: function (model, response, errorReport) {
            console.log("Nothing to be Sync'd from the Server with URL " + URL_WSServer + seviceurl);
            callback(0, model.responseText);
        }
    });
}



function fnGetReqExtn(passname, reqdtlids, callback) {

    var token = localStorage.getItem("token");
    var input = {
        "token": token,
        "passname": passname,
        "reqdtlids": reqdtlids
    };


    $.ajax({
        url: URL_WSServer + "loaners/dueloaners",
        method: "POST",
        contentType: "application/json",
        data: JSON.stringify(input),
        datatype: "json",
        success: function (data) {
            callback(eval("data.listGmLoanerDrilldownVO"));
        },
        error: function (jqXHR, status, error) {
            console.log(error);
        }
    });
}


function fnSubmitReqExtn(input, callback) {

    var token = localStorage.getItem("token");
    if (input == undefined)
        input = { "token": token };
    else
        input.token = token;
    //passing user's company info with all the web service calls
    input.cmpid = localStorage.getItem("cmpid");
    input.cmptzone = localStorage.getItem("cmptzone");
    input.cmpdfmt = localStorage.getItem("cmpdfmt");
    input.plantid = localStorage.getItem("plantid");
    
    $.ajax({
        url: URL_WSServer + "loanerext/saveloanerext",
        method: "POST",
        contentType: "application/json",
        data: JSON.stringify(input),
        datatype: "json",
        success: function (data) {
            callback(data);
        },
        error: function(jqXHR, status, error) {
            if ($.parseJSON(jqXHR.responseText).errorCode == "20644") {
                showNativeAlert(lblNm("msg_cannot_extend_loner_request"), lblNm("msg_cannot_extend"), lblNm("msg_ok"), function () {
                    $("#btn-req-submit").text(lblNm("submit"));
                });
            }else if ($.parseJSON(jqXHR.responseText).errorCode == "20649") {
                showNativeAlert("One or more Loaner sets are in Pending Approval status. Please contact FAM team to proceed.", lblNm("msg_cannot_extend"), lblNm("msg_ok"), function () {
                    $("#btn-req-submit").text(lblNm("submit"));
                });
            }else if ($.parseJSON(jqXHR.responseText).message != "") {
                showNativeAlert($.parseJSON(jqXHR.responseText).message, lblNm("msg_cannot_extend"), lblNm("msg_ok"), function () {
                    $("#btn-req-submit").text(lblNm("submit"));
                });
            } else
                showNativeAlert(error, lblNm("msg_loaner_extension"), lblNm("msg_ok"));
        }
    });
}



function fnPostCase(casedata, localcainfoid, callback) {
    var token = localStorage.getItem("token");
    var userid = localStorage.getItem("userID");
    if (casedata.caseid != "") {
        var caseid = casedata.caseid;
        var cainfoid = casedata.cainfoid;
    } else {
        var caseid = "";
        var cainfoid = "";
    }
    var surdt = casedata.surdt;
    var did = casedata.did;
    var repid = casedata.repid;
    var acid = casedata.acid;
    var voidfl = casedata.voidfl;
    var prnotes = casedata.prnotes;
    var type = 1006503;
    if (casedata.status != "")
        var status = casedata.status;
    else
        var status = "11091";
    var prtcaid = casedata.prtcaid;
    var skipvalidationfl = casedata.skipvalidationfl;
    var arrGmCaseAttributeVO = casedata.arrGmCaseAttributeVO;
    var now = new Date(casedata.sursdate + " " + casedata.surstime + ":00");
    var surtime = this.formatNumber(now.getUTCMonth() + 1) + "/" + this.formatNumber(now.getUTCDate()) + "/" + now.getUTCFullYear() + " " + this.formatNumber(now.getUTCHours()) + ":" + this.formatNumber(now.getUTCMinutes()) + ":" + this.formatNumber(now.getUTCSeconds()) + " +00:00";
    var postData = {
        "token": token,
        "userid": userid,
        "cainfoid": cainfoid,
        "caseid": caseid,
        "surdt": surdt,
        "surtime": surtime,
        "did": did,
        "repid": repid,
        "acid": acid,
        "voidfl": voidfl,
        "prnotes": prnotes,
        "type": type,
        "status": status,
        "prtcaid": prtcaid,
        "skipvalidationfl": skipvalidationfl,
        "arrGmCaseAttributeVO": arrGmCaseAttributeVO
    };


    fnGetWebServerData("request/saverequest", undefined, postData, function(data, model, response, errorReport) {
        if (model == undefined) {
            if (localcainfoid != "") {
                fnUpadteCaseLocal(localcainfoid, data, function () {
                    callback(data);
                    fnUpdateCase(function () {
                        fnGetCaseInfoID(function (result) {
                            fnDeleteCaseLocalTable(_.pluck(result, 'ID'), function () {

                            });
                        });
                    });

                });
            } else {
                callback(data);
            }
        } else {
            callback(0, model.responseText);
        }
    }, true);

}

function fnRemoveCaseLocal(iValue, result) {
    if (iValue < result.length) {
        fnDeleteCaseLocalTable(result[iValue].ID, function () {
            iValue++;
            fnRemoveCaseLocal(iValue, result);
        });
    }
}

function formatNumber(num) {
    var s = num + "";
    while (s.length < 2) s = "0" + s;
    return s;
}


function fnShareCase(casedata, callback) {
    var token = localStorage.getItem("token");
    var arrGmCaseAttributeVO = casedata;

    var postData = {
        "token": token,
        "arrGmCaseAttributeVO": arrGmCaseAttributeVO
    };

    fnGetWebServerData("request/savereqattr", undefined, postData, function(data, model, response, errorReport) {

        if (model == undefined) {
            callback(data);
        } else {
            callback(0, model.responseText);
        }
    }, true);

}

function fnUpdateCase(callback) {
    if (!boolSyncing) {
        var token = localStorage.getItem("token");

        var postData = {
            "token": token,
            "langid": langid,
            "pageno": pageno,
            "uuid": deviceUUID
        };

        fnGetWebServerData("devicesync/updatescnt", undefined, postData, function(data, model, response, errorReport) {
            if (model == undefined) {
                if (data.listAutoUpdates != undefined) {
                    autoSync(0, callback);
                } else {
                    callback();
                }
            } else {
                console.log(model.responseText);
            }
        }, true);

    } else {
        callback();
    }
}


function fnAutoBookCase(iValue, results) {
    if (iValue < results.length) {
        fnPostCase($.parseJSON(results[iValue].CASEDATA), "", function(data) {
            //     fncreateEvent("update", data.cainfoid, results[iValue].CASE_INFO_ID, $.parseJSON(results[iValue].CASEDATA));
            fnDeleteCaseLocal(results[iValue].CASE_INFO_ID, function () {
                iValue++;
                fnAutoBookCase(iValue, results);
            });
        });
    } else {
        fnUpdateCase(function () {
            console.log("Updated Case");
        });
    }
}



function autoSync(iValue, callback) {
    var ws;
    var input = '';
    var syncElements = ["listGmCaseInfoVO", "listGmCaseAttributeVO"];
    ws = syncElements[iValue];
    if (casesync == false) {
        casesync = true;
        DataSync_SL(ws, input, function(status) {
            casesync = false;
            iValue++;
            if (iValue < syncElements.length) {
                autoSync(iValue, callback);
            } else {
                callback();
            }
        });
    } else {
        fnUpdateCase();
    }
}




function fncreateEvent(type, newtitle, oldtitle, data) {
    var dd, mm, yyyy;
    var newdate = new Date(data.sursdate);
    var month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    dd = ("0" + newdate.getDate()).slice(-2), mm = month[newdate.getMonth()], yyyy = newdate.getFullYear();
    var startDate = new Date(mm + " " + dd + ", " + yyyy + " " + data.surstime + ":00");
    var endDate = new Date(data.suredate);

    var location = data.acname;
    var notes = data.prnotes;
    var success = function () {
        console.log("Success!");
    };
    var error = function(message) {
        console.log(message);
    };
    var title = data.sname + " - " + data.categoriename;

    if (type == "new") {
        // var title = newtitle+" Case Event";
        //    window.plugins.calendar.deleteEvent(title,location,notes,startDate,endDate,success,error);
        //    window.plugins.calendar.createEvent(title,location,notes,startDate,endDate, success, error);
    } else if (type == "delete") {
        // var title = newtitle+" Case Event";
        //   window.plugins.calendar.deleteEvent(title,location,notes,startDate,endDate,success,error);
    } else {
        // var title = oldtitle+" Case Event";
        // var newTitle = newtitle+" Case Event";
        //    window.plugins.calendar.modifyEvent(title,location,notes,startDate,endDate,title,location,notes,startDate,endDate,success,error);
    }

}

function fnSendICSFile(method, data, sharedEmails, filename, status) {
    var subject = filename + " " + status + " Case Event Notification";
    var toemail = sharedEmails;
    document.addEventListener("deviceready", onDeviceReady, false);

    function onDeviceReady() {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
    }

    function gotFS(fileSystem) {
        fileSystem.root.getFile(filename + ".ics", {
            create: true,
            exclusive: false
        }, gotFileEntry, fail);
    }

    var sampleurl;

    function gotFileEntry(fileEntry) {
        sampleurl = fileEntry.toURL();
        fileEntry.createWriter(gotFileWriter, fail);
    }

    function gotFileWriter(writer) {
        writer.onwriteend = function (evt) {
            console.log("contents of file now 'some sample text'");
            if ($(window).width() < 480) 
                  cordova.plugins.email.open({
                    to: toemail,
                    subject: subject,
                    attachments: [sampleurl],
                    isHtml: true
                    });
            else
                 window.plugin.email.open({
                    to: toemail,
                    subject: subject,
                    attachments: [sampleurl],
                    isHtml: true
                    });
        };
        writer.write("BEGIN:VCALENDAR" + "\n" + "VERSION:2.0" + "\n" + "PRODID:-//SYFADIS//PORTAIL FORMATION//FR" + "\n" + "METHOD:" + method + "\n" + data + "END:VCALENDAR");
    }

    function fail(error) {
        console.log(error.code);
    }
}

function fnAutoRequest(iValue, results) {



    if (iValue < results.length) {


        sendRequest((results[iValue].REQUESTDATA), function (status, data) {


            showNativeAlert(lblNm("case_id")+" - "+ data.caseid + lblNm("msg_created_successfully"), lblNm("msg_success"), lblNm("msg_ok"), function () {


            });

            fnDeleteRequestLocal(results[iValue].REQUEST_ID, function () {
                iValue++;
                fnAutoRequest(iValue, results);
            });
        });
    }


}


function splitRequest(requestData) {

    var NoOfShips = _.uniq(_.pluck(_.pluck(requestData.arrGmCaseSetInfoVO, "gmTxnShipInfoVO"), "addressid"));
    var arrCaseSetInfo = "";
    var MainJSONData = new Array();
    if (NoOfShips.length > 0) {
        for (var i = 0; i < NoOfShips.length; i++) {
            var planShipDate = _.uniq(_.pluck(requestData.arrGmCaseSetInfoVO, "planshipdt"));
            var reqDate = _.uniq(_.pluck(requestData.arrGmCaseSetInfoVO, "reqdt"));
            MainJSONData.push({
                "casetid": "",
                "cainfoid": "",
                "setid": "",
                "systp": "103644",
                "reqdt": reqDate[0],
                "planshipdt": planShipDate[0]
            });
            arrCaseSetInfo = MainJSONData;
            arrCaseSetInfo[i].arrGmPartDetailVO = [];
            arrCaseSetInfo[i].gmTxnShipInfoVO = {};
            var arrShipInfo = _.pluck(_.where(requestData.arrGmCaseSetInfoVO, {
                "addressid": NoOfShips[i]
            }), "gmTxnShipInfoVO");
            var arrPartsInfo = _.pluck(_.where(requestData.arrGmCaseSetInfoVO, {
                "addressid": NoOfShips[i]
            }), "arrGmPartDetailVO");

            arrCaseSetInfo[i].gmTxnShipInfoVO.source = arrShipInfo[0].source;
            arrCaseSetInfo[i].gmTxnShipInfoVO.shipto = arrShipInfo[0].shipto;
            arrCaseSetInfo[i].gmTxnShipInfoVO.ship_to_id = arrShipInfo[0].ship_to_id;
            arrCaseSetInfo[i].gmTxnShipInfoVO.shipcarrier = arrShipInfo[0].shipcarrier;
            arrCaseSetInfo[i].gmTxnShipInfoVO.shipmode = arrShipInfo[0].shipmode;
            arrCaseSetInfo[i].gmTxnShipInfoVO.addressid = arrShipInfo[0].addressid;
            arrCaseSetInfo[i].gmTxnShipInfoVO.attnto = arrShipInfo[0].attnto;
            for (var len = 0; len < arrPartsInfo.length; len++) {
                arrCaseSetInfo[i].arrGmPartDetailVO.push(arrPartsInfo[len][0]);
                delete arrCaseSetInfo[i].arrGmPartDetailVO[len]["tissueFlag"];
            }
        }
    }
    return $.parseJSON(JSON.stringify(arrCaseSetInfo));
}

function sendRequest(requestdata, callback) {

    var data = $.parseJSON(requestdata);

    var token = localStorage.getItem("token");
    var userid = localStorage.getItem("userID");
    var caseid = data.caseid;
    var cainfoid = data.caseinfoid;
    var surdt = data.surdt;

    var did = "";


    //     var repid = data.repid;
    //     var assocrepid = data.asrepid;
    var acid = data.acctid;
    var voidfl = data.voidfl;
    var prnotes = data.prnotes;
    var type = data.type;
    var status = "19524";
    var prtcaid = data.prtcaid;
    var skipvalidationfl = data.skipvalidationfl;
    var arrGmCaseAttributeVO = data.arrGmCaseAttributeVO;
    var repid = "";
    var assocrepid = "";
    if (type == 1006505) {
        repid = data.repid;
        if (data.asrepid != undefined)
            assocrepid = data.asrepid.toString();
    }
    if (type != 107286) {
         did = data.did;
     }

    var arrGmCaseSetInfoVO = "";
    if (type == 1006507 || type == 107286) {//107286 Account Item Consignment
        repid = data.repid;
        arrGmCaseSetInfoVO = splitRequest(data)
    } else {
        arrGmCaseSetInfoVO = data.arrGmCaseSetInfoVO;
    }

    if (repid != undefined)
        repid = repid.toString();
    if (acid != undefined)
        acid = acid.toString();
    for (var x = 0; x < data.arrGmCaseSetInfoVO.length; x++) {

        delete data.arrGmCaseSetInfoVO[x]["indexValue"];
        delete data.arrGmCaseSetInfoVO[x]["tissueFlag"];
        delete data.arrGmCaseSetInfoVO[x].gmTxnShipInfoVO["indexValue"];
        delete data.arrGmCaseSetInfoVO[x].gmTxnShipInfoVO["carrierName"];
        delete data.arrGmCaseSetInfoVO[x].gmTxnShipInfoVO["modeName"];
        delete data.arrGmCaseSetInfoVO[x].gmTxnShipInfoVO["addresstype"];
   
    }

    for (l = 0; l < arrGmCaseSetInfoVO.length; l++) {
        var shipToIDFl = false;
        if(arrGmCaseSetInfoVO[l].gmTxnShipInfoVO["shipto"] == "4121")
            shipToIDFl = true;
        else if(arrGmCaseSetInfoVO[l].gmTxnShipInfoVO["shipto"] == "107801")
            shipToIDFl = true;

        if (shipToIDFl == false) // if Not Sales Rep Address id id should be empty.
        {
            arrGmCaseSetInfoVO[l].gmTxnShipInfoVO["ship_to_id"] = arrGmCaseSetInfoVO[l].gmTxnShipInfoVO["addressid"];
            arrGmCaseSetInfoVO[l].gmTxnShipInfoVO["addressid"] = "";
        }
    }

    delete data.arrGmPartDetailVO;
    delete data.gmTxnShipInfoVO;

    var postData = {
        "token": token,
        "cainfoid": cainfoid,
        "caseid": caseid,
        "surdt": surdt,
        "surtime": "",
        "did": did,
        "repid": repid,
        "asrepid": assocrepid,
        "acid": acid,
        "voidfl": voidfl,
        "prnotes": prnotes,
        "type": type,
        "status": status,
        "prtcaid": prtcaid,
        "skipvalidationfl": skipvalidationfl,
        "arrGmCaseAttributeVO": arrGmCaseAttributeVO,
        "arrGmCaseSetInfoVO": arrGmCaseSetInfoVO
    };

    fnGetWebServerData("request/saverequest", undefined, postData, function(data, model, response, errorReport) {
        if (model == undefined) {
            callback(1, data);
        } else {
            callback(0, model.responseText);
        }
    }, true);

}


function fnCreateEvents() {
    fnGetCalenderEventInfo('', function(data) {
        callbackFn("success");
        fnCreateEvent(0, data, true, function () {
            clearedCases = [];
        });
    });
}

function fnCreateEvent(iValue, data, actionFlag, callback) {
    if (iValue < data.length) {
        if ((data[iValue].DURATION != "") && (data[iValue].ZONE != "")) {

            var hr = data[iValue].DURATION.replace("h", "");

            var newdate = new Date((data[iValue].surstime).split("+")[0] + "UTC");
            var stime = this.formatNumber(newdate.getHours()) + ":" + this.formatNumber(newdate.getMinutes());
            var dd, mm, yyyy;
            dd = ("0" + newdate.getDate()).slice(-2), mm = ("0" + (newdate.getMonth() + 1)).slice(-2), yyyy = newdate.getFullYear();
            var month = new Array();
            month[0] = "January";
            month[1] = "February";
            month[2] = "March";
            month[3] = "April";
            month[4] = "May";
            month[5] = "June";
            month[6] = "July";
            month[7] = "August";
            month[8] = "September";
            month[9] = "October";
            month[10] = "November";
            month[11] = "December";
            mm = month[newdate.getMonth()];
            var sursdate = mm + " " + dd + ", " + yyyy;
            var starttimehours = (stime).split(":")[0];
            var starttimeminutes = (stime).split(":")[1];
            var endtimeminutes = parseInt(starttimeminutes);
            var endtimehours = parseInt(starttimehours) + parseInt(hr);
            var date = new Date(sursdate);
            if (endtimehours >= 24) {
                endtimehours = endtimehours - 24;
                date = new Date(date.getTime() + 24 * 60 * 60 * 1000);
            }
            if (endtimeminutes >= 60) {
                endtimehours = endtimehours + 1;
                endtimeminutes = endtimeminutes - 60;
            }

            dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
            endtimehours = (endtimehours.toString().length == 1) ? "0" + endtimehours : endtimehours;
            endtimeminutes = (endtimeminutes.toString().length == 1) ? "0" + endtimeminutes : endtimeminutes;
            var suretime = yyyy + mm + dd + "T" + endtimehours + endtimeminutes + "00";

            mm = month[date.getMonth()];
            var suredate = mm + " " + dd + ", " + yyyy + " " + endtimehours + ":" + endtimeminutes + ":00";

            newdate = new Date(sursdate);
            dd = ("0" + newdate.getDate()).slice(-2), mm = month[newdate.getMonth()], yyyy = newdate.getFullYear();

            var startDate = new Date(mm + " " + dd + ", " + yyyy + " " + stime + ":00");
            var endDate = new Date(suredate);

            var assignee = data[iValue].ASSIGNEE;

            var location = data[iValue].ACCOUNT_NM;
            var notes = data[iValue].prnotes;
            var success = function () {
                console.log("Success!");
                fnCreateEvent(iValue + 1, data, actionFlag, callback)
            };
            var error = function (message) {
                console.log("Error!");
                console.log(message);
                fnCreateEvent(iValue + 1, data, actionFlag, callback)
            };


            var title = data[iValue].SURGEON_NAME + " - " + data[iValue].CATEGORY;

            if (actionFlag) {
                window.plugins.calendar.deleteEvent(title, location, notes, startDate, endDate);
                window.plugins.calendar.createEvent(title, location, notes, startDate, endDate, success, error);
            } else {
                window.plugins.calendar.deleteEvent(title, location, notes, startDate, endDate, success, error);
            }
        } else
            fnCreateEvent(iValue + 1, data, actionFlag, callback);
    } else
        callback();
}

function fnDeleteCases(VOName, resultSet) {
    try {
        var IDs = _.difference(_.pluck(resultSet, 'cainfoid'), clearedCases);
        fnProcessSubDeletion(0, IDs, function() {
            console.log("Completed!!!");
            updateLocalDB(VOName, resultSet, fnSyncStatusUpdate);
        })
    } catch (err) {
        showAppError(err, "GmDataSync:fnDeleteCases()");
    }
}

function fnProcessSubDeletion(iValue, IDs, callback) {
    try {
        var currIDs, increment;
        if ((iValue + 500) <= IDs.length) {
            currIDs = IDs.splice(iValue, iValue + 500);
            increment = 500;
        } else {
            currIDs = IDs.splice(iValue, IDs.length);
            increment = IDs.length - iValue;
        }

        fnGetCaseEventInfoFor(currIDs, function(data) {
            fnCreateEvent(0, data, false, function() {
                clearedCases = currIDs.concat(clearedCases);
                if (IDs.length > (iValue + increment))
                    fnProcessSubDeletion(iValue + increment, IDs, callback);
                else
                    callback();
            });
        });
    } catch (err) {
        showAppError(err, "GmDataSync:fnProcessSubDeletion()");
    }
}

function fnSendFax(file, Number) {

    AuthorizedCode = localStorage.getItem("faxAuthCode");
    
    if (parseNull(AuthorizedCode) == "") {
        AuthorizedCode = "Z2xvYnVzcHJvZDpHbG9idXMhMjU2MA==";
    }

    if (navigator.onLine) {
        $.ajax({
            url: "https://rest.interfax.net/outbound/faxes?faxNumber=" + Number + "&pageOrientation=portrait&pageSize=a4",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + AuthorizedCode);
            },
            type: 'POST',
            dataType: 'text',
            contentType: 'application/pdf',
            processData: false,
            data: file,
            success: function (data) {
                showNativeAlert(lblNm("msg_fax_successfully_sent") + Number + "\n", lblNm("msg_fax_sent"));
            },
            error: function(jqXHR, status, error) {
                showNativeAlert(lblNm("msg_error_occured_on_fax") + jqXHR.statusText, lblNm("msg_failed"));
            }
        });
    } else
        showNativeAlert(lblNm("msg_fax_cannot_send"), lblNm("msg_no_internet"));


}




function fnFetchFieldSalesList() {
    if ((navigator.onLine) && (!intOfflineMode)) {
        var input = {
            "token": localStorage.getItem('token')
        };

        fnGetWebServerData('salesfilter/morefilter', undefined, input, function(data, model, response, errorReport) {

            if (model == undefined) {
                if (data.listGmFieldSalesListVO != undefined) {
                    var arrData = new Array();
                    if (data.listGmFieldSalesListVO.length == undefined)
                        arrData.push(data.listGmFieldSalesListVO)
                    else
                        arrData = data.listGmFieldSalesListVO;

                    var arrFieldSalesList = _.pluck(arrData, 'fieldsalesid');

                    localStorage.setItem('FieldSalesList', JSON.stringify(arrFieldSalesList));
                    localStorage.setItem('dteFieldSalesList', new Date());
                }
            } else {

                console.log("Nothing to be Sync'd from the Server with URL " + URL_WSServer);
            }
        }, true);

    }
}


function fnCheckDOStatus() {
    if (arrPendingDOList.length != 0) {
        var token = localStorage.getItem("token");
        var doPostData = {
            "token": token,
            "stropt": "DOREPORT",
            "doid": arrPendingDOList.join(","),
            "passname": "includevoidedorders",
            "accountid": "",
            "fromdate": "",
            "todate": ""
        };
        fnGetWebServerData("orders/submitted", undefined, doPostData, function(DOInfo, model, response, errorReport) {

            if (model == undefined) {
                var DOStatus = new Array();
                if (DOInfo.listGmOrdersDrilldownVO != undefined) {
                    if (DOInfo.listGmOrdersDrilldownVO.length == undefined)
                        DOStatus.push(DOInfo.listGmOrdersDrilldownVO);
                    else
                        DOStatus = DOInfo.listGmOrdersDrilldownVO;

                    DOStatus = _.reject(DOStatus, function (obj) {
                        return obj.status == 'Pending CS Confirmation';
                    });
                    arrUpdatedDO = _.pluck(DOStatus, 'donum');
                    for (var i = 0; i < DOStatus.length; i++)
                        fnUpdateDOStatus(DOStatus[i].donum, DOStatus[i].status);

                    fnGetReason();
                }

            } else {
                console.log("Error in Checking DO Updates");
                console.log(model.responseText);
            }
        }, true);

    }

}


function fnGetDORepContactInfo(repid, callback) {
    var query = "SELECT T101.C101_USER_ID FROM T101_USER T101, T703_SALES_REP T703 WHERE T101.C101_PARTY_ID = T703.C101_PARTY_ID AND T703.C703_SALES_REP_ID = " + repid;
    fnExecuteSelectQuery(query, function (data) {
        var input = {
            "token": localStorage.getItem("token")
        };
        input.userid = data[0].C101_USER_ID;
        fnGetWebServiceData("usercontactinfo", undefined, input, callback);
    });
}

//fnGetLocalTagDataCount:= Below function is used to get the Tag tablw count frm local table
function fnGetLocalTagDataCount(callback){
    db.transaction(function (tx, rs) {
            tx.executeSql("select count(1) tagcount from T5010_TAG_DTL where c5010_void_fl !='';",[],function (tx, rs) {
                    callback(rs.rows.item(0).tagcount);
                },null);
        });
}

/* fnTagSetMapCountValidation():Function to get the count from T5010_TAG_DTL local table to show the sync validation while clicking DO module icon.
 */
function fnTagSetMapCountValidation(callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT COUNT(1) count FROM T5010_TAG_DTL WHERE (C207_SET_ID IS NULL OR C207_SET_ID ='null')AND (C5010_Void_Fl IS NULL OR C5010_Void_Fl='null'); ", [],
            function (tx, rs) {
               callback(rs.rows.item(0).count);
            },null);
    });
}

/* fnDeleteTagRecord():Function to delete the Tag details in Tag Table
 */
function fnDeleteTagRecord(callback) {
    console.log("fnDeleteTagRecord inside");
    resetServer('4001234', function() {
    db.transaction(function (tx, rs) {
        tx.executeSql("DELETE FROM T5010_TAG_DTL", [],
            function (tx, rs) {
               callback("success");
            },null);
    });
    });
}




//PC-4658 - Get the Code Lookup Data from Cloud Firestore
function fnGetCodeLKpFromCloud(codeGrp,callback){

        if (firedb != undefined) {
                 var codelkpData = [];
                  firedb.collection("T901_CODE_LOOKUP").where("C901_CODE_GRP","==",codeGrp).where("C901_ACTIVE_FL","==",1).where("C901_VOID_FL","==","").where("C901_CODE_SEQ_NO","!=","").orderBy("C901_CODE_SEQ_NO")
                                           .get().then((querySnapshot) => {
                             var source = querySnapshot.metadata.fromCache ? "local cache" : "server";
                             console.log("Cloud Code Lookup Data came from " + source);
                            
                             querySnapshot.forEach((doc) => {
                                                          codelkpData.push({"ID" :doc.data().C901_CODE_ID,"Name":doc.data().C901_CODE_NM});
                             });
                             console.log("Current codelkpData:", codelkpData);
                                           callback(codelkpData);
                              });
        }
          
}
//Get the Rules Data from Cloud Firestore
function fnGetRulesFromCloud(ruleId,ruleGrp,compId,callback){
    
    if (firedb != undefined) {
                  var ruleData = [];
                  firedb.collection("T906_RULES").where("C906_RULE_GRP_ID","==",ruleGrp).where("C906_RULE_ID","==",ruleId).where("C1900_COMPANY_ID","==",compId).where("C906_VOID_FL","==","")
                                           .get().then((querySnapshot) => {
                                           var source = querySnapshot.metadata.fromCache ? "local cache" : "server";
                             console.log("Cloud Rules Data came from " + source);
                                 
                             querySnapshot.forEach((doc) => {
                                         ruleData.push({"RULE_ID" :doc.data().C906_RULE_ID,"RULE_GRP_ID":doc.data().C906_RULE_GRP_ID,"RULE_VALUE":doc.data().C906_RULE_VALUE,"RULE_DESC":doc.data().C906_RULE_DESC,"RULE_COMPANY":doc.data().C1900_COMPANY_ID});
                             });
                             console.log("Current ruleData-:", ruleData);
                             callback(ruleData);
                            
                          });
    }
}
//to get the valid Part Number Details from Cloud
function fnGetValidPartDtls(partcno, callback) {
   
     if(partcno != '' && partcno != undefined && partcno != null){

        fnGetValidPartCnumDtlsFromCloud(partcno,"C2550_UDI_MRF",function(data){
            console.log(data);
                    if(data != ''){
                        callback(data);
                    }else{
        fnGetValidPartCnumDtlsFromCloud(partcno,"C2550_UDI_HRF",function(data){
                        console.log(data);
                                if(data != ''){
                                    callback(data);
                                }else{
        fnGetValidPartCnumDtlsFromCloud(partcno,"C2550_CONTROL_NUMBER",function(data){
                        console.log(data);
                                                    if(data != ''){
                                                        callback(data);
                                                    }else{
        fnValidateControlNo(partcno,function(validPartcno){
                        console.log(validPartcno);
            
        fnGetValidPartCnumDtlsFromCloud(validPartcno,"C2550_CONTROL_NUMBER",function(data){
                        console.log(data);
                                                    if(data != ''){
                                                        callback(data);
                                                    }else{
                                                          callback();  
                                                     }
                                                });  
                                        });
                                    }
                                  });  
                                 }
                            });   
                     }
        }); 
    }
}
//to get the valid Part Control Number from Cloud
function fnGetValidPartCnumDtlsFromCloud(partcno,whereCol,callback){
         //get live data from firestore for the user
          if (firedb != undefined) {
              var partDtlsData = [];
firedb.collection("T2550_PART_CONTROL_NUMBER").where(whereCol,"==",partcno)
                            .get().then((querySnapshot) => { 
                            var source = querySnapshot.metadata.fromCache ? "local cache" : "server"; 
                             console.log("Cloud Code Lookup Data came from " + source);
                              querySnapshot.forEach((doc) => {
                                       //family,material,etc  get required values
                            //push required values 
                            partDtlsData.push({"ID" :doc.data().C205_PART_NUMBER_ID,"expirydate":doc.data().C2550_EXPIRY_DATE,"lotno":doc.data().C2550_CONTROL_NUMBER,"pdesc":doc.data().C205_PART_NUM_DESC,"pfamily":doc.data().C205_PRODUCT_FAMILY,"pmat":doc.data().C205_PRODUCT_MATERIAL,"pclass":doc.data().C205_PRODUCT_CLASS});

                                                         }); 
                            callback(partDtlsData);
                                console.log("----------MAIN_FIREBASE----------------------->");
                                        console.log(partDtlsData);
                                                    });
                                      }
}
//to validate Part Control Number from Cloud
function fnValidatePartCnumFromCloud(partnum,partcno,callback){
         //get live data from firestore for the user
          if (firedb != undefined) {
              var partCtrlValidDtls = [];
              firedb.collection("T2550_PART_CONTROL_NUMBER").where("C2550_CONTROL_NUMBER","==",partcno).where("C205_PART_NUMBER_ID","==",partnum).get().then((querySnapshot) => { 
                var source = querySnapshot.metadata.fromCache ? "local cache" : "server"; 
                console.log("Cloud Code Lookup Data came from " + source);
                querySnapshot.forEach((doc) => {
                if (doc.exists){
                    partCtrlValidDtls.push(doc.data());
                }                                       
                }); 
                callback(partCtrlValidDtls);
 		console.log(partCtrlValidDtls);
                  });
                }
}