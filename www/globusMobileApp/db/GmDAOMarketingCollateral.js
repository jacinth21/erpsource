function fnGetMappedDocuments(strID,callback) {


db.transaction(function (tx, rs) {
			
			tx.executeSql("SELECT C903_UPLOAD_FILE_LIST AS ID, C903_FILE_NAME AS Name, T901.C901_CODE_NM || '/' || C903_REF_ID || '/' AS Path, C901_FILE_TITLE as Title FROM T903_UPLOAD_FILE_LIST T903, T901_CODE_LOOKUP T901 WHERE T903.C901_TYPE = T901.C901_CODE_ID AND T903.C901_TYPE='103113' AND C903_DELETE_FL='' AND C901_REF_GRP='103092' AND C903_REF_ID=? ORDER BY T903.C903_FILE_SEQ_NO",[strID],
				function(tx,rs) {

					var i=0, results = [];
					for(i=0;i<rs.rows.length;i++) {
						results.push(rs.rows.item(i));
					}
					callback(results);
			},
			function(e){showMsgView("009");
		});
	});
}


function fnGetPathforDoc (SubType, callback) {
	db.transaction(function (tx, rs) {
				tx.executeSql("SELECT C903_UPLOAD_FILE_LIST AS ID, C903_FILE_NAME AS Name, T901.C901_CODE_NM || '/' || C903_REF_ID || '/' AS Path, C901_FILE_TITLE as Title FROM T903_UPLOAD_FILE_LIST T903, T901_CODE_LOOKUP T901 WHERE T903.C901_TYPE = T901.C901_CODE_ID AND T903.C901_TYPE='103113' AND C903_DELETE_FL='' AND C901_REF_GRP='103092' AND C903_REF_ID=? ORDER BY T903.C903_FILE_SEQ_NO",[strID],
					function(tx,rs) {
						var i=0, results = [];
						for(i=0;i<rs.rows.length;i++) {
							results.push(rs.rows.item(i));
						}
						callback(results);
				},
				function(e){showMsgView("009");
			});
		});
}


function fnInsertShare(FileID, callback) {
	fnRemoveShare(FileID,function() {
		db.transaction(function (tx, rs) {
			var time = new Date();
			tx.executeSql("INSERT OR REPLACE INTO TBL_SHARE_LIST(FILE_LIST_ID,CREATED_DATE) VALUES(?,?)", [FileID, time],
					function(tx,rs) {
						callback();
			},
			function(){showMsgView("009");});
		});
	})
	
}

function fnRemoveShare(FileID, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("DELETE FROM TBL_SHARE_LIST WHERE FILE_LIST_ID = ?", [FileID],
			function(tx,rs) {
				callback();
		},null);
	});
}

function fnGetSharedList(callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT SHR.FILE_LIST_ID AS ID,T903.C903_FILE_NAME As Name FROM TBL_SHARE_LIST SHR, T903_UPLOAD_FILE_LIST T903 WHERE T903.c903_upload_file_list = SHR.FILE_LIST_ID", [],
			function(tx,rs) {
				var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
						results.push(rs.rows.item(i));
				}
				callback(results);
		},null);
	});
}

function fnGetSharedDocuments(callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT SHR.FILE_LIST_ID AS ID,T903.C903_FILE_NAME As Name, T903.C901_FILE_TITLE as Title,T903.C205_PART_NUMBER_ID as PartNum FROM TBL_SHARE_LIST SHR, T903_UPLOAD_FILE_LIST T903 WHERE T903.c903_upload_file_list = SHR.FILE_LIST_ID AND (T903.C901_REF_TYPE='103114' OR T903.C901_REF_TYPE='4000441')", [],
			function(tx,rs) {
				var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
				}
				callback(results);
		},null);
	});
}

function fnGetSharedImages(callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT SHR.FILE_LIST_ID AS ID,T903.C903_FILE_NAME As Name, T903.C901_FILE_TITLE as Title FROM TBL_SHARE_LIST SHR, T903_UPLOAD_FILE_LIST T903 WHERE T903.c903_upload_file_list = SHR.FILE_LIST_ID AND T903.C901_REF_TYPE='103112'", [],
			function(tx,rs) {
				var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
						results.push(rs.rows.item(i));
				}
				callback(results);
		},null);
	});
}

function fnGetSharedVideos(callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT SHR.FILE_LIST_ID AS ID,T903.C903_FILE_NAME As Name, T903.C901_FILE_TITLE as Title FROM TBL_SHARE_LIST SHR, T903_UPLOAD_FILE_LIST T903 WHERE T903.c903_upload_file_list = SHR.FILE_LIST_ID AND T903.C901_REF_TYPE='103113'", [],
			function(tx,rs) {
				var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
						results.push(rs.rows.item(i));
				}
				callback(results);
		},null);
	});
}

function fnGetSharedFiles(callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT FILE_LIST_ID AS updfileid FROM TBL_SHARE_LIST", [],
			function(tx,rs) {
				var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
						results.push(rs.rows.item(i));
				}
				callback(results);
		},null);
	});
}

function fnGetShareCount(callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT * FROM TBL_SHARE_LIST", [],
			function(tx,rs) {
				callback(rs.rows.length);
		},null);
	});
}

function fnCheckDocuments(ID,callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT * FROM  TBL_SHARE_LIST WHERE  FILE_LIST_ID= ?", [ID],function(tx,rs) {
			if(rs.rows.length>0) {
				callback("success");
			}
			else {
				callback("fail");
			}
		},null);
	});
}

function fnGetFavoriteContactCount(callback) {

	db.transaction(function (tx, rs) {
		tx.executeSql("DELETE FROM TBL_FAVORITE WHERE TYPE ='contacts' AND SYSTEM_ID =(SELECT T107TC.C107_CONTACT_ID from T101_PARTY T101TP,T107_CONTACT T107TC WHERE T101TP.C101_PARTY_ID = T107TC.C101_PARTY_ID  AND T107TC.C107_CONTACT_TYPE = '90452' AND T101TP.C101_ACTIVE_FL = '' AND T107TC.C107_INACTIVE_FL = '' AND T101TP.C101_VOID_FL = '' AND T107TC.C107_VOID_FL = '')",[],
			// tx.executeSql("SELECT COUNT(*) AS Length FROM TBL_FAVORITE TF LEFT OUTER JOIN T107_CONTACT TC ON TF.SYSTEM_ID = TC.C107_CONTACT_ID AND TF.TYPE ='contacts' AND TC.C107_VOID_FL ='' AND TC.C107_INACTIVE_FL=''  LEFT OUTER JOIN T101_PARTY TP ON TP.C101_PARTY_ID= TC.C101_PARTY_ID AND TP.C101_VOID_FL ='' AND TP.C101_ACTIVE_FL=''",[],

			function(tx,rs) {
				db.transaction(function (tx, rs) {
					tx.executeSql("SELECT COUNT(*) AS Length FROM TBL_FAVORITE WHERE TYPE='contacts'",[],
						// tx.executeSql("SELECT COUNT(*) AS Length FROM TBL_FAVORITE TF LEFT OUTER JOIN T107_CONTACT TC ON TF.SYSTEM_ID = TC.C107_CONTACT_ID AND TF.TYPE ='contacts' AND TC.C107_VOID_FL ='' AND TC.C107_INACTIVE_FL=''  LEFT OUTER JOIN T101_PARTY TP ON TP.C101_PARTY_ID= TC.C101_PARTY_ID AND TP.C101_VOID_FL ='' AND TP.C101_ACTIVE_FL=''",[],
						
						function(tx,rs) {
							
							callback(rs.rows.item(0).Length);
							
							console.log(rs.rows.item(0).Length);
					},null);
				});
		},null);
	});
}


function fnGetFavoriteEmail(callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("Select UN.ID AS ID, UN.PARTY_ID AS PartyID, UN.FirstName AS FirstName, UN.LastName AS LastName, UN.Email AS Email, 'contacts' AS FavClass FROM (SELECT TP.C101_FIRST_NM AS FirstName, TP.C101_LAST_NM AS LastName, TC.C107_CONTACT_VALUE As Email, TC.C107_CONTACT_ID as ID, TC.C101_PARTY_ID AS PARTY_ID FROM T107_CONTACT TC, T101_PARTY TP, TBL_FAVORITE WHERE TP.C101_PARTY_ID= TC.C101_PARTY_ID AND TC.C107_CONTACT_ID =TBL_FAVORITE.SYSTEM_ID AND TP.C101_ACTIVE_FL='' AND TC.C107_INACTIVE_FL = '' AND TP.C101_VOID_FL = '' AND TC.C107_VOID_FL = '' UNION SELECT TP.FIRST_NM AS FirstName, TP.LAST_NM AS LastName, TC.CONTACT_VALUE As Email,TC.CONTACT_ID as ID, TC.PARTY_ID AS PARTY_ID FROM TBL_CONTACT TC, TBL_PARTY TP,TBL_FAVORITE WHERE TP.PARTY_ID= TC.PARTY_ID AND TC.CONTACT_ID=TBL_FAVORITE.SYSTEM_ID) UN, TBL_FAVORITE FAV WHERE UN.ID LIKE FAV.SYSTEM_ID", [],
			function(tx,rs) {
				var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
					results.push(rs.rows.item(i));
				}
			callback(results);
			console.log(results);
		},null);
	});
}


function fnChkFavoriteEmail(txtSearch,callback) {
	txtSearch = txtSearch.split(' ');
	var searchTerm = [];
	for(var i=0; i<txtSearch.length;i++) {
	    searchTerm.push("EmailSearchName LIKE '%"+txtSearch[i]+"%'");
	}

	var query = searchTerm.join(" AND ");
	query = "SELECT ID, FirstName, LastName, Email, FirstName || ' ' || LastName || ' ' || Email AS EmailSearchName FROM (Select TC.CONTACT_ID AS ID,TP.FIRST_NM AS FirstName,TP.LAST_NM AS LastName,TC.CONTACT_VALUE AS Email from TBL_PARTY TP, TBL_CONTACT TC, TBL_FAVORITE TF WHERE TP.PARTY_ID=TC.PARTY_ID AND TF.SYSTEM_ID = TC.CONTACT_ID  UNION SELECT T107TC.C107_CONTACT_ID AS ID,T101TP.C101_FIRST_NM AS FirstName,T101TP.C101_LAST_NM AS LastName,T107TC.C107_CONTACT_VALUE AS Email from T101_PARTY T101TP,T107_CONTACT T107TC,TBL_FAVORITE TF WHERE T101TP.C101_PARTY_ID = T107TC.C101_PARTY_ID AND TF.SYSTEM_ID = T107TC.C107_CONTACT_ID) WHERE "+query;

	// query = "SELECT FIRST_NM AS FirstName, C101_MIDDLE_INITIAL AS MI, LAST_NM AS LastName, CONTACT_VALUE AS EMail, PARTY_ID as ID, FAVORITE,  FIRST_NM || ' ' || C101_MIDDLE_INITIAL|| ' ' || LAST_NM AS EmailSearchName from TBL_CONTACT WHERE "+ query ;

	db.transaction(function (tx, rs) {
		tx.executeSql(query, [],
			function(tx,rs) {
				var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
				}
                 console.log(results);
				callback(results);

			},
			function(e,err){showMsgView("009");
		});
	});
}


function fnGetRecentEmail(type,callback) {
	var i=0, results = [];
	db.transaction(function (tx, rs) {
						// tx.executeSql("SELECT COM.ID AS ID, COM.FirstName, COM.LastName, COM.Email, FAV.TYPE As FavClass FROM (Select UN.ID, UN.FirstName,UN.LastName,UN.Email FROM (SELECT TP.C101_FIRST_NM AS FirstName, TP.C101_LAST_NM AS LastName, TC.C107_CONTACT_VALUE As Email, TC.C107_CONTACT_ID as ID FROM T107_CONTACT TC, T101_PARTY TP, TBL_RECENT WHERE TP.C101_PARTY_ID= TC.C101_PARTY_ID AND TC.C107_CONTACT_ID = TBL_RECENT.DETAIL_ID UNION SELECT TP.FIRST_NM AS FirstName, TP.LAST_NM AS LastName, TC.CONTACT_VALUE As Email,TC.CONTACT_ID as ID FROM TBL_CONTACT TC, TBL_PARTY TP,TBL_RECENT WHERE TP.PARTY_ID= TC.PARTY_ID AND TC.CONTACT_ID = TBL_RECENT.DETAIL_ID) UN, TBL_RECENT REC WHERE UN.ID LIKE REC.DETAIL_ID AND REC.TYPE = ? ORDER BY REC.TIME DESC LIMIT 10) Combined COM LEFT JOIN TBL_FAVORITE FAV ON FAV.SYSTEM_ID = COM.ID", [type],function(tx,rs) {
							tx.executeSql("Select UN.ID, UN.FirstName,UN.LastName,UN.Email, UN.C101_PARTY_ID AS PartyID, FAV.TYPE As FavClass FROM (SELECT TP.C101_FIRST_NM AS FirstName, TP.C101_LAST_NM AS LastName, TC.C107_CONTACT_VALUE As Email, TC.C107_CONTACT_ID as ID, TC.C101_PARTY_ID FROM T107_CONTACT TC, T101_PARTY TP, TBL_RECENT WHERE TP.C101_PARTY_ID= TC.C101_PARTY_ID AND TC.C107_CONTACT_ID = TBL_RECENT.DETAIL_ID AND TP.C101_ACTIVE_FL='' AND TC.C107_INACTIVE_FL = '' AND TP.C101_VOID_FL = '' AND TC.C107_VOID_FL = '' UNION SELECT TP.FIRST_NM AS FirstName, TP.LAST_NM AS LastName, TC.CONTACT_VALUE As Email,TC.CONTACT_ID as ID, TC.PARTY_ID as PARTY_ID FROM TBL_CONTACT TC, TBL_PARTY TP,TBL_RECENT WHERE TP.PARTY_ID= TC.PARTY_ID AND TC.CONTACT_ID = TBL_RECENT.DETAIL_ID) UN LEFT JOIN TBL_FAVORITE FAV ON FAV.SYSTEM_ID LIKE UN.ID, TBL_RECENT REC WHERE UN.ID LIKE REC.DETAIL_ID AND REC.TYPE = ? ORDER BY REC.TIME DESC LIMIT 10", [type],function(tx,rs) {
							for(i=0;i<rs.rows.length;i++) {
								results.push(rs.rows.item(i));
							}
							callback(results);
						},
						function(e,err){
							console.log(JSON.stringify(err));
							showNativeAlert('Please ReInstall App','Database Error Occured','OK');
						});
					});
	
}


function fnGetContactsAdded(id,callback) {
	
	db.transaction(function (tx, rs) {
	
				tx.executeSql("Select ID, partyid, FirstName, LastName, Email, Type, MI, npiid, contacttype, Module FROM (SELECT TC.C107_CONTACT_ID AS ID, TC.C107_CONTACT_VALUE AS Email, TC.C107_CONTACT_TYPE AS contacttype, TP.C901_PARTY_TYPE as Type,  TP.C101_PARTY_ID as partyid, TP.C101_FIRST_NM AS FirstName, TP.C101_LAST_NM AS LastName, TP.C101_MIDDLE_INITIAL as MI, '' as npiid, 'SIM' As Module from T107_CONTACT TC, T101_PARTY TP WHERE TP.C101_PARTY_ID= TC.C101_PARTY_ID UNION SELECT TC.CONTACT_ID AS ID, TC.CONTACT_VALUE AS Email, TC.CONTACT_TYPE AS contacttype, TP.PARTY_TYPE as Type, TP.PARTY_ID as partyid, TP.FIRST_NM AS FirstName,TP.LAST_NM AS LastName, TP.C101_MIDDLE_INITIAL as MI, TP.NPI AS npiid, 'Local' AS Module from TBL_CONTACT TC, TBL_PARTY TP WHERE TP.PARTY_ID= TC.PARTY_ID) WHERE ID = ?", [id],
					//tx.executeSql("Select ID, FirstName, LastName, Email, Type, MI, npiid, contacttype, partyid, phoneNo, Module FROM (SELECT TC.C107_CONTACT_ID AS ID, TC.C107_CONTACT_VALUE AS Email,TC.C107_CONTACT_VALUE AS phoneNo, TC.C107_CONTACT_TYPE AS contacttype, TP.C101_PARTY_ID as partyid, TP.C901_PARTY_TYPE as Type, TP.C101_FIRST_NM AS FirstName, TP.C101_LAST_NM AS LastName, TP.C101_MIDDLE_INITIAL as MI, '' as npiid, 'SIM' As Module from T107_CONTACT TC, T101_PARTY TP WHERE TP.C101_PARTY_ID= TC.C101_PARTY_ID UNION SELECT TC.CONTACT_ID AS ID, TC.CONTACT_VALUE AS Email,TC.CONTACT_VALUE AS phoneNo, TC.CONTACT_TYPE AS contacttype, TP.PARTY_ID as partyid, TP.PARTY_TYPE as Type, TP.FIRST_NM AS FirstName,TP.LAST_NM AS LastName, TP.C101_MIDDLE_INITIAL as MI, TP.NPI AS npiid, 'Local' AS Module from TBL_CONTACT TC, TBL_PARTY TP WHERE TP.PARTY_ID= TC.PARTY_ID) WHERE ID = ?", [id],
						function(tx,rs) {
							callback(rs.rows.item(0));
				},null);
			});
}

function fnGetPhoneNO(partyid,callback){
	db.transaction(function (tx, rs) {
	
				tx.executeSql("Select ID, partyid, contactvalue, contacttype FROM(SELECT TP.C101_PARTY_ID AS partyid, TC.C107_CONTACT_VALUE AS contactvalue, TC.C107_CONTACT_TYPE as contacttype, TC.C107_CONTACT_ID AS ID FROM T107_CONTACT TC, T101_PARTY TP WHERE TP.C101_PARTY_ID= TC.C101_PARTY_ID UNION SELECT TP.PARTY_ID AS partyid,TC.CONTACT_VALUE AS contactvalue, TC.CONTACT_TYPE as contacttype, TC.CONTACT_ID AS ID FROM TBL_CONTACT TC, TBL_PARTY TP WHERE TP.PARTY_ID= TC.PARTY_ID) WHERE partyid = ?", [partyid],
					function(tx,rs) {
							callback(rs.rows.item(0));
				},null);
			});
}



function fnInsertNewParty(type,npiid,fname,mname,lname,emailid,phoneNo,callback) {


	db.transaction(function (tx, rs) {
		tx.executeSql("INSERT OR REPLACE INTO TBL_PARTY (FIRST_NM, LAST_NM,C101_MIDDLE_INITIAL,PARTY_TYPE, NPI) VALUES (?,?,?,?,?)", [fname,lname,mname,type,npiid],
			function(tx,rs) {
				tx.executeSql("SELECT MAX(PARTY_ID) AS partyid FROM TBL_PARTY", [],
				function(tx,rs) {
					for(i=0;i<rs.rows.length;i++) {
					// alert(rs.rows.item(i).partyid);
					var partyid = rs.rows.item(i).partyid;
					tx.executeSql("INSERT OR REPLACE INTO TBL_CONTACT (PARTY_ID, CONTACT_VALUE, PRIMARY_FL, CONTACT_TYPE) VALUES (?,?,?,?)", [partyid,emailid,"Y","90452"],
					function(tx,rs) {
						//callback();
					tx.executeSql("INSERT OR REPLACE INTO TBL_CONTACT (PARTY_ID, CONTACT_VALUE, PRIMARY_FL, CONTACT_TYPE) VALUES (?,?,?,?)", [partyid,phoneNo,"Y","90450"],
						function(tx,rs) {
						callback();
					},null);
					
					},null);
				}
			},null);

		},null);

	});

}



function fnUpdatePartyContact(contactID, type,npiid,fname,mname,lname,emailid,phoneNo, phoneNoContactID,callback) {

	db.transaction(function (tx, rs) {
		tx.executeSql("UPDATE TBL_CONTACT SET CONTACT_VALUE=? WHERE CONTACT_ID=?", [emailid,contactID],
			function(tx,rs) {
				tx.executeSql("UPDATE TBL_CONTACT SET CONTACT_VALUE=? WHERE CONTACT_ID=?", [phoneNo,phoneNoContactID],
					function(tx,rs) {
						tx.executeSql("SELECT PARTY_ID AS partyID FROM TBL_CONTACT WHERE CONTACT_ID=?", [contactID],
							function(tx,rs) {
								var partyid = rs.rows.item(0).partyID;
									tx.executeSql("UPDATE TBL_PARTY SET FIRST_NM=?, LAST_NM=?, C101_MIDDLE_INITIAL=?, PARTY_TYPE=?, NPI=? WHERE PARTY_ID=?", [fname,lname,mname,type,npiid,partyid],
										function(tx,rs) {
										callback();
									},function(tx,err) {console.log(JSON.stringify(err)); alert('error');});
				
							},function(tx,err) {console.log(JSON.stringify(err)); alert('error');});

					},function(tx,err) {console.log(JSON.stringify(err)); alert('error');});

			},function(tx,err) {console.log(JSON.stringify(err)); alert('error');});
		});

}




function fnGetItemHeaders(itemID, callback) {

	itemIDs = itemID.split(',');
	var conditionalTerm = [];
	for(var i=0; i<itemIDs.length;i++) {
	    conditionalTerm.push("C902_CODE_NM_ALT = '"+itemIDs[i]+"'");
	}
		
	var query = conditionalTerm.join(" OR ");
	
	query = "SELECT DISTINCT T901.C901_CODE_ID AS ID, T901.C901_CODE_NM AS Name, T901.C902_CODE_NM_ALT AS Type, T906.C906_RULE_VALUE AS Share from T901_CODE_LOOKUP T901 left join T906_RULES T906 on T901.C901_CODE_ID = T906.C906_RULE_ID where T901.C901_CODE_GRP = 'FSBTYP' AND T901.C901_CODE_ID NOT IN ('4000787','4000788','4000789') AND T901.C901_ACTIVE_FL='1' AND "+ query;
	
	db.transaction(function (tx, rs) {
		tx.executeSql(query, [],
			function(tx,rs) {
				var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
						results.push(rs.rows.item(i));
				}
				callback(results);
		},null);
	});
}


function fnGetFilesBySubType(SysID, ID, callback) {
    
    
    var query = "SELECT T903.C903_UPLOAD_FILE_LIST AS ID, T903.C903_FILE_NAME AS Name, T903.C903_FILE_PATH AS Path, T903.C901_FILE_TITLE as Title, (IFNULL(T903.C903_SHARE_RULE,'')|| ''  ||IFNULL(T903.C903_SHARE_FLAG,'')) AS ShareValue FROM T903_UPLOAD_FILE_LIST T903, (SELECT T205.C205_PART_NUMBER_ID pnum FROM T205_PART_NUMBER T205, T205D_PART_ATTRIBUTE T205D WHERE T205.C205_ACTIVE_FL = 'Y' AND T205.C205_PART_NUMBER_ID    = T205D.C205_PART_NUMBER_ID AND T205D.C901_ATTRIBUTE_TYPE = '80180' AND T205D.C205D_ATTRIBUTE_VALUE = '" + intServerCountryCode + "' AND T205D.C205D_VOID_FL = '' ) PLIST WHERE T903.C901_TYPE = '" + ID + "' AND T903.C903_REF_ID='" + SysID + "' AND T903.C903_DELETE_FL = '' AND T903.C901_REF_TYPE  ='103114' AND T903.C205_PART_NUMBER_ID = PLIST.pnum UNION ALL SELECT T903.C903_UPLOAD_FILE_LIST AS ID, T903.C903_FILE_NAME AS Name, T903.C903_FILE_PATH AS Path, T903.C901_FILE_TITLE as Title, (IFNULL(T903.C903_SHARE_RULE,'')|| ''  ||IFNULL(T903.C903_SHARE_FLAG,'')) AS ShareValue FROM T903_UPLOAD_FILE_LIST T903 WHERE T903.C901_TYPE = '" + ID + "' AND T903.C903_REF_ID='" + SysID + "' AND T903.C903_DELETE_FL = '' AND T903.C901_REF_TYPE  !='103114'";
    
    /*var query = "SELECT T903.C903_UPLOAD_FILE_LIST AS ID, T903.C903_FILE_NAME AS Name, T903.C903_FILE_PATH AS Path, T903.C901_FILE_TITLE as Title, (IFNULL(T903.C903_SHARE_RULE,'')|| ''  ||IFNULL(T903.C903_SHARE_FLAG,'')) AS ShareValue FROM T903_UPLOAD_FILE_LIST T903 WHERE T903.C901_TYPE = '" + ID + "' AND T903.C903_REF_ID='" + SysID + "' AND T903.C903_DELETE_FL = ''";*/

	db.transaction(function (tx, rs) {
		tx.executeSql(query, [],
			function(tx,rs) {
				var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
					results.push(rs.rows.item(i));
				}
				callback(results);
		},function(tx,err) {console.log(JSON.stringify(err)); alert('error');});
	});
}

// Function to pass document ID and Update the file path in T903_UPLOAD_FILE_LIST table and return the file path
function fnGetFilePath(docId, callback) {
    var query = "SELECT C903_FILE_PATH AS Path FROM T903_UPLOAD_FILE_LIST WHERE C903_DELETE_FL='' AND C903_UPLOAD_FILE_LIST = ?";
    db.transaction(function(tx, rs) {
        tx.executeSql("UPDATE T903_UPLOAD_FILE_LIST SET C903_FILE_PATH = (SELECT T901.C901_CODE_NM FROM T901_CODE_LOOKUP T901 WHERE T903_UPLOAD_FILE_LIST.C901_REF_GRP = T901.C901_CODE_ID) || '/' || C903_REF_ID || '/' || C901_REF_TYPE || '/' || C901_TYPE || '/' || C903_FILE_NAME WHERE T903_UPLOAD_FILE_LIST.C901_TYPE NOT IN ('4000790') AND C903_DELETE_FL='' AND C903_UPLOAD_FILE_LIST = ?", [docId],
            function(tx, rs) {
                db.transaction(function(tx, rs) {
                    tx.executeSql(query, [docId],
                        function(tx, rs) {
                            var i = 0,
                                results = [];
                            for (i = 0; i < rs.rows.length; i++) {
                                results.push(rs.rows.item(i));
                            }
                            callback(results);
                        },
                        function(tx, err) {
                            showError("Please Sync App before syncing Files.", "Sync Priority Clash");
                        });
                });
            },
            function(tx, err) {
                showError("Please Sync App before syncing Files.", "Sync Priority Clash");
            });
    });
}
function fnGetCodeNM(codeID, callback) {
	
     db.transaction(function (tx, rs) {
		tx.executeSql("SELECT C901_CODE_NM FROM T901_CODE_LOOKUP WHERE C901_CODE_ID = ?", [codeID],
			function(tx,rs) {
				if(rs.rows.length>0)
					callback(rs.rows.item(0).C901_CODE_NM);
				else
					callback('')
		},function (tx,err) {
			console.log(JSON.stringify(err));
			alert('error');
		});
	});
}

function fnGetParty(PartyID, EmailID, callback) {
	if(PartyID.indexOf('.')<0){
		db.transaction(function (tx, rs) {
			tx.executeSql("SELECT C101_FIRST_NM AS FirstName, C101_LAST_NM AS LastName FROM T101_PARTY WHERE C101_PARTY_ID = ?", [PartyID],
				function(tx,rs) {
					if(rs.rows.length>0) {
						callback(rs.rows.item(0));
					}
					else{
						fnGetPartyByEmail(EmailID, callback);
					}
			},function (tx,err) {
				console.log(JSON.stringify(err));
				alert('error');
			});
		});
	}
	else{
		db.transaction(function (tx, rs) {
			tx.executeSql("SELECT FIRST_NM AS FirstName, LAST_NM AS LastName FROM TBL_PARTY WHERE PARTY_ID = ?", [PartyID.split('.')[0]],
				function(tx,rs) {
					if(rs.rows.length>0) {
						callback(rs.rows.item(0));
					}
					else{
						fnGetPartyByEmail(EmailID, callback);
					}
			},function (tx,err) {
				console.log(JSON.stringify(err));
				alert('error');
			});
		});

	}
	
}

function fnGetPartyByEmail (EmailID, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT PARTY.FIRST_NM AS FirstName, PARTY.LAST_NM AS LastName FROM TBL_PARTY PARTY, TBL_CONTACT CONTACT WHERE PARTY.PARTY_ID = CONTACT.PARTY_ID AND CONTACT.CONTACT_VALUE = ?", [EmailID],
			function(tx,rs) {
				if(rs.rows.length>0)
					callback(rs.rows.item(0));
				else
					callback();
		},function (tx,err) {
			console.log(JSON.stringify(err));
			alert('error');
		});
	});
}

function fnGetFileDetail (FileID, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT T903.C903_UPLOAD_FILE_LIST AS ID, T903.C903_FILE_NAME AS Name, T903.C901_FILE_TITLE AS Title, T901.C901_CODE_NM AS Type, T903.C901_REF_TYPE As TypeID FROM T903_UPLOAD_FILE_LIST T903, T901_CODE_LOOKUP T901 WHERE T901.C901_CODE_ID = T903.C901_REF_TYPE AND C903_UPLOAD_FILE_LIST = ?", [FileID],
			function(tx,rs) {
				if(rs.rows.length > 0)
					callback(rs.rows.item(0));
				else
					callback();
		},function (tx,err) {
			console.log(JSON.stringify(err));
			alert('error');
		});
	});
}

//Get the file details by file id
function fnGetFileInfo(FileID, callback) {
    var selectQuery = "SELECT T903.C903_UPLOAD_FILE_LIST AS ID, T903.C903_FILE_NAME AS Name, T903.C901_FILE_TITLE AS Title, T901.C901_CODE_NM AS Type, T903.C901_REF_TYPE As TypeID, T903.C903_FILE_PATH AS Path, T903.C903_REF_ID AS REFID FROM T903_UPLOAD_FILE_LIST T903, T901_CODE_LOOKUP T901 WHERE T901.C901_CODE_ID = T903.C901_REF_TYPE AND C903_DELETE_FL='' AND C903_UPLOAD_FILE_LIST = ?";
    db.transaction(function(tx, rs) {
        tx.executeSql("SELECT count(1) as COUNT FROM T903_UPLOAD_FILE_LIST", [],
            function(tx, rs) {
                if (rs.rows.item(0).COUNT > 0) {
                    db.transaction(function(tx, rs) {
                        tx.executeSql(selectQuery, [FileID],
                            function(tx, rs) {
                               if(rs.rows.length > 0)
                                   	callback(rs.rows.item(0));
                                else
                                    callback('')
                            },
                            function(tx, err) {
                                showAppError(err, "GmDAOMarketingCollateral:fnGetFileInfo(2)");
                            });
                    }); 
                } else {
                    GM.Global.GuideErr = "Please Sync App before use it.";
                    window.history.back();
                }
            },
            function(tx, err) {
                showAppError(err, "GmDAOMarketingCollateral:fnGetFileInfo(1)");
            });
    });
}

function fnInsertShareHistory (json, callback) {
	var user = window.localStorage.getItem('userName');
	db.transaction(function (tx, rs) {
		var now = new Date();
		tx.executeSql("INSERT OR REPLACE INTO TBL_SHARE(JSON,USER,CREATED_DATE) VALUES(?,?,?)", [json,user,now],
			function(tx,rs) {


				db.transaction(function (tx, rs) {
					tx.executeSql("SELECT SHARE_ID FROM TBL_SHARE WHERE CREATED_DATE=?", [now],
						function(tx,rs) {
							createShareExtID(rs.rows.item(0).SHARE_ID,now,callback);
					},function (tx,err) {
						console.log(JSON.stringify(err));
						alert('error');
					});
				});	



			},function (tx,err) {
				console.log(JSON.stringify(err));
				alert('error');
		});
	});
}

function createShareExtID(ID,now,callback) {

	var userID = window.localStorage.getItem("userID");
	var ShareExtID = userID + ID + (now.getMonth() + 1) + now.getDate() + (now.getFullYear().toString()).substring(2,4) + now.getHours();

	db.transaction(function (tx, rs) {
		tx.executeSql("UPDATE TBL_SHARE SET C1016_SHARE_EXT_REF_ID = ? WHERE SHARE_ID = ?", [ShareExtID,ID],
			function(tx,rs) {
				callback(ShareExtID);
		},function (tx,err) {
			console.log(JSON.stringify(err));
			alert('error');
		});
	});	
}



function fnCheckShareHistory () {
	var user = window.localStorage.getItem('userName');
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT * FROM TBL_SHARE WHERE STATUS IS NULL AND USER=?", [user],
			function(tx,rs) {
				var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
					results.push(rs.rows.item(i));
				}
				reSendEmail(0,results);
			},function (tx,err) {
				console.log(JSON.stringify(err));
				alert('error');
		});
	});

}


function fnUpdateStatus (shareID, serverShareID, status, callback) {

	db.transaction(function (tx, rs) {
		var now = new Date();
		tx.executeSql("UPDATE TBL_SHARE SET C1016_SHARE_ID=?, STATUS=? WHERE C1016_SHARE_EXT_REF_ID=?", [serverShareID,status,shareID],
			function(tx,rs) {
				fnDeleteShareList(callback);
			},function (tx,err) {
				console.log(JSON.stringify(err));
				alert('error');
		});
	});
}

function fnDeleteShareList (callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("DELETE FROM TBL_SHARE_LIST", [],
			function(tx,rs) {
				callback();
		},function (tx,err) {
			console.log(JSON.stringify(err));
			alert('error');
		});
	});			
}

function fnUpdateJSON (shareID, JSON, callback) {
	db.transaction(function (tx, rs) {
		var now = new Date();
		tx.executeSql("UPDATE TBL_SHARE SET JSON=? WHERE C1016_SHARE_EXT_REF_ID=?", [JSON,shareID],
			function(tx,rs) {
				callback();
			},function (tx,err) {
				console.log(JSON.stringify(err));
				alert('error');
		});
	});
}

function fnDeletePartyContact(id,callback) {
	
	db.transaction(function (tx, rs) {
		tx.executeSql("DELETE FROM TBL_CONTACT WHERE CONTACT_ID=?", [id],
			function(tx,rs) {
				db.transaction(function (tx, rs) {
					tx.executeSql("DELETE FROM TBL_FAVORITE WHERE SYSTEM_ID=?", [id],
						function(tx,rs) {
							callback();
						});
				});
			});
	});
}

function fnRefreshShareCart (callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("DELETE FROM TBL_SHARE_LIST WHERE FILE_LIST_ID in (SELECT C903_UPLOAD_FILE_LIST FROM T903_UPLOAD_FILE_LIST WHERE C903_DELETE_FL='Y')", [],
			function(tx,rs) {
				callback();
			},null);
	});
}

// PC-4132 : Function used for get user primary contact details 
function fnGetUserContactInfo(partyid, mode, callback) {
	var query = "SELECT T107.C107_CONTACT_VALUE conval  FROM T101_PARTY T101, T107_CONTACT T107 WHERE T101.C101_PARTY_ID = T107.C101_PARTY_ID AND T107.C107_VOID_FL = '' AND T107.C107_PRIMARY_FL = 'Y' AND T107.C901_MODE = "+mode+" AND T101.C101_PARTY_ID = "+partyid+"";
	console.log(query);
	db.transaction(function (tx, rs) {
		tx.executeSql(query, [],
			function (tx, rs) {
				if (rs.rows.length > 0)
					callback(rs.rows.item(0));
				else
					callback();
			}, function (tx, err) {
				console.log(JSON.stringify(err));
				alert('error');
			});
	});
}


