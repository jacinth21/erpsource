function fnGetSurgeonDetail(partyid, callback) {
    console.log(partyid)
	db.transaction(function (tx, rs) {
		tx.executeSql(" SELECT T101.C101_PARTY_ID partyid, T101.C101_FIRST_NM firstnm, T101.C101_LAST_NM lastnm, T101.C101_MIDDLE_INITIAL midinitial, T6600.c6600_surgeon_npi_ID npid, T6600.C6600_SURGEON_ASSISTANT_NM asstnm, T6600.C6600_SURGEON_MANAGER_NM mngrnm, pic.picurl FROM T101_PARTY T101 LEFT JOIN T6600_PARTY_SURGEON T6600 ON T6600.C101_PARTY_SURGEON_ID = T101.C101_PARTY_ID LEFT JOIN (SELECT C901_REF_GRP ||'/'|| C903_REF_ID ||'/'|| C903_FILE_NAME picurl , C903_REF_ID refid from T903_UPLOAD_FILE_LIST where c901_ref_grp = '103112' and c903_delete_fl = '') pic on pic.refid = T101.C101_PARTY_ID WHERE T101.C101_VOID_FL = '' AND t101.c101_party_id = ? ", [partyid],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
			console.log("fnGetSurgeonDetail Success");
		},
		function(){
			showMsgView("009");
			console.log("fnGetSurgeonDetail Failed");
		});
	});
}

function fnGetSurgeonPicture(partyid, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql(" SELECT C901_REF_GRP ||'/'   || C903_REF_ID   ||'/' || C903_FILE_NAME picurl from T903_UPLOAD_FILE_LIST where c901_ref_grp = '103112' and C903_REF_ID = ? ", [partyid],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
			console.log("fnGetSurgeonPicture Success");
		},
		function(){
			showMsgView("009");
			console.log("fnGetSurgeonPicture Failed");
		});
	});
}

function fnGetSurgeonHospitalDetail(partyid, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql(" SELECT C1014_PARTY_AFFILIATION_ID afflid, C704_ACCOUNT_ID accid, C1014_ORGANIZATION_NAME accnm, C901_AFFILIATION_PRIORITY priority, C1014_AFFILIATION_PERCENT casepercent, C1014_CITY_NM acccity, (SELECT C901_CODE_NM FROM T901_CODE_LOOKUP WHERE C901_CODE_ID = C901_STATE ) accstate, C901_STATE accstateid, C1014_VOID_FL voidfl, C1014_COMMENT comments, (SELECT C901_CODE_NM FROM T901_CODE_LOOKUP WHERE C901_CODE_ID = C1014_COMMENT ) commentnm FROM T1014_PARTY_AFFILIATION WHERE C1014_VOID_FL  = '' AND c101_party_id = ? ", [partyid],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
			console.log("fnGetSurgeonHospitalDetail Success");
		},
		function(){
			showMsgView("009");
			console.log("fnGetSurgeonHospitalDetail Failed");
		});
	});
}

function fnGetSurgeonSurgicalDetail(partyid, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT C901_PROCEDURE procedureid, C901_PROCEDURE_NM procedurenm, C6610_CASES cases, C207_SET_ID sysid,   C6610_NON_GLOBUS_SYSTEM sysnm, C6610_COMPANY_NM company, C6610_SURGICAL_ACTIVITY_ID sactid, C6610_VOID_FL voidfl, C6610_COMMENT comments, (SELECT C901_CODE_NM FROM T901_CODE_LOOKUP WHERE C901_CODE_ID = C6610_COMMENT ) commentnm FROM T6610_SURGICAL_ACTIVITY WHERE C6610_VOID_FL = '' AND c101_party_id = ? ", [partyid],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
			console.log("fnGetSurgeonSurgicalDetail Success");
		},
		function(){
			showMsgView("009");
			console.log("fnGetSurgeonSurgicalDetail Failed");
		});
	});
}

function fnGetSurgeonAttributeDetail(partyid, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT C1012_PARTY_OTHER_INFO_ID attrid, C901_TYPE attrtype, C1012_DETAIL_NM attrnm, C1012_DETAIL attrvalue, C1012_VOID_FL voidfl, C101_PARTY_ID partyid FROM T1012_PARTY_OTHER_INFO  WHERE C1012_VOID_FL = '' AND C101_PARTY_ID = ? ", [partyid],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
			console.log("fnGetSurgeonAttributeDetail Success");
		},
		function(){
			showMsgView("009");
			console.log("fnGetSurgeonAttributeDetail Failed");
		});
	});
}

function fnGetSurgeonAddressDetail(partyid, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("select C106_ADDRESS_ID addid, C106_ADD1 add1, C106_ADD2 add2, C106_CITY city, C901_STATE stateid, (SELECT C901_CODE_NM FROM T901_CODE_LOOKUP WHERE C901_CODE_ID  = C901_STATE) statenm, C901_COUNTRY countryid,(SELECT C901_CODE_NM FROM T901_CODE_LOOKUP WHERE C901_CODE_ID  = C901_COUNTRY) countrynm, C106_PRIMARY_FL primaryfl, C106_VOID_FL voidfl, C106_ZIP_CODE zip from t106_address where C106_VOID_FL = '' and c101_party_id = ? ", [partyid],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
			console.log("fnGetSurgeonAddressDetail Success");
		},
		function(){
			showMsgView("009");
			console.log("fnGetSurgeonAddressDetail Failed");
		});
	});
}

function fnGetSurgeonContactDetail(partyid, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT C107_CONTACT_ID conid, C901_MODE conmode, C107_CONTACT_TYPE contype, C107_CONTACT_VALUE convalue, C107_PRIMARY_FL primaryfl FROM T107_CONTACT WHERE C107_VOID_FL IS '' AND C101_PARTY_ID = ? ", [partyid],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
			console.log("fnGetSurgeonContactDetail Success");
		},
		function(){
			showMsgView("009");
			console.log("fnGetSurgeonContactDetail Failed");
		});
	});
}

function fnGetDocumentDetail(refid, callback) {
    console.log(refid)
	db.transaction(function (tx, rs) {
		tx.executeSql(" SELECT C903_UPLOAD_FILE_LIST fileid, C903_REF_ID refid, C903_FILE_NAME filename, C901_REF_TYPE reftype, C901_TYPE type, C901_REF_GRP refgroup, C901_FILE_TITLE filetitle FROM T903_UPLOAD_FILE_LIST WHERE C903_REF_ID = ? AND C901_REF_TYPE IN ('105220','105221','105222') AND C903_DELETE_FL  = '' ", [refid],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
			console.log("fnGetDocumentDetail Success");
		},
		function(){
            showMsgView("009");
            console.log("fnGetDocumentDetail Failed");
        });
	});
}

function fnGetSurgeonRepDetail(partyid, callback) {
     console.log(partyid)
	db.transaction(function (tx, rs) {
		tx.executeSql(" SELECT DISTINCT v700.rep_id repid, v700.rep_name repnm, v700.ad_id adid, v700.ad_name adnm, v700.vp_name vpnm, t703.c101_party_id reppartyid FROM v700_territory_mapping_detail v700 LEFT JOIN t703_sales_rep t703 ON v700.rep_id = t703.c703_sales_rep_id WHERE t703.c101_party_id = CASE ( SELECT COUNT(*) FROM T1014_PARTY_AFFILIATION  t1014 LEFT JOIN T704_ACCOUNT T704 ON T704.C704_ACCOUNT_ID = T1014.C704_ACCOUNT_ID JOIN t703_sales_rep t703 ON t703.c703_sales_rep_id = t704.c703_sales_rep_id WHERE  T1014.C1014_VOID_FL = '' AND T1014.C901_AFFILIATION_PRIORITY = '1' AND T1014.C101_PARTY_ID = ? ) WHEN 0 then ( SELECT C1012_DETAIL reppartyid FROM t1012_party_other_info WHERE C901_TYPE = '11509' AND c101_party_id = ? and C1012_VOID_FL = '' ) else ( SELECT t703.c101_party_id reppartyid FROM T1014_PARTY_AFFILIATION T1014 LEFT JOIN T704_ACCOUNT T704 ON T704.C704_ACCOUNT_ID = T1014.C704_ACCOUNT_ID JOIN t703_sales_rep t703 ON t703.c703_sales_rep_id = t704.c703_sales_rep_id WHERE T1014.C1014_VOID_FL = '' and T1014.C901_AFFILIATION_PRIORITY = '1' and T1014.C101_PARTY_ID = ? ) END ", [partyid, partyid, partyid],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
			console.log("fnGetSurgeonAccntRepDetail Success");
		},
		function(){
            showMsgView("009");
            console.log("fnGetSurgeonAccntRepDetail Failed");
        });
	});   
}

function fnGetSurgeonAccntRepDetail(partyid, callback) {
    console.log(partyid)
	db.transaction(function (tx, rs) {
		tx.executeSql(" SELECT t703.c101_party_id reppartyid FROM T1014_PARTY_AFFILIATION T1014 LEFT JOIN T704_ACCOUNT T704 ON T704.C704_ACCOUNT_ID =  T1014.C704_ACCOUNT_ID JOIN t703_sales_rep t703 ON t703.c703_sales_rep_id  = t704.c703_sales_rep_id WHERE T1014.C1014_VOID_FL IS NULL AND T1014.C901_AFFILIATION_PRIORITY = '1' AND T1014.C101_PARTY_ID  = ? ", [partyid],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
			console.log("fnGetSurgeonAccntRepDetail Success");
		},
		function(){
            showMsgView("009");
            console.log("fnGetSurgeonAccntRepDetail Failed");
        });
	});
}

function fnGetSurgeonAttrRepDetail(partyid, callback) {
    console.log(partyid)
	db.transaction(function (tx, rs) {
		tx.executeSql(" SELECT C1012_DETAIL reppartyid FROM t1012_party_other_info WHERE C901_TYPE   = '11509' AND c101_party_id = ? And c1012_void_fl = ''   ", [partyid],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
			console.log("fnGetSurgeonAttrRepDetail Success");
		},
		function(){
            showMsgView("009");
            console.log("fnGetSurgeonAttrRepDetail Failed");
        });
	});
}

function fnGetSurgeonGlobusActDetail(partyid, callback) {
    console.log(partyid)
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    if(dd<10) { dd='0'+dd };
    if(mm<10) { mm='0'+mm };
    today = yyyy+'/'+mm+'/'+dd;
	db.transaction(function (tx, rs) {
		tx.executeSql(" SELECT T6630.C6630_ACTIVITY_NM activitytitle, T6630.C6630_ACTIVITY_ID activityid, T6630.C6630_FROM_DT activitydate,   T6630.C901_ACTIVITY_CATEGORY_NM activitytype FROM T6630_ACTIVITY T6630 LEFT JOIN T6631_ACTIVITY_PARTY_ASSOC T6631 ON  T6630.C6630_ACTIVITY_ID = T6631.C6630_ACTIVITY_ID WHERE T6630.C6630_VOID_FL = '' AND T6630.C901_ACTIVITY_CATEGORY IN ('105267','105265','105266') AND substr(T6630.C6630_FROM_DT, 7, 4) || '/' || substr(T6630.C6630_FROM_DT, 1,2) || '/' || substr(T6630.C6630_FROM_DT, 4, 2) >= '"+today+"'  AND T6631.C101_PARTY_ID  =  ? ", [partyid],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
			console.log("fnGetSurgeonGlobusActDetail Success");
		},
		function(){
            showMsgView("009");
            console.log("fnGetSurgeonGlobusActDetail Failed");
        });
	});
}

function fnGetCodeNMByGRP(GRP, callback, alt) {
    console.log(GRP)
    console.log(alt)
    var query = " SELECT C901_CODE_NM codenm, C901_CODE_NM Name, C901_CODE_ID codeid, C901_CODE_ID ID FROM T901_CODE_LOOKUP WHERE C901_CODE_GRP  = '"+GRP+"' ";
    if(alt){
        query = query + " AND C902_CODE_NM_ALT = '"+alt+"' ";
    }
    console.log("fnGetCodeNMByGRP")
    console.log(query);
	db.transaction(function (tx, rs) {
		tx.executeSql(query, [],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
            results = _.filter(results, function(data){
                return data.codenm != "TBD"; 
            });
			callback(results);
		},
		function(){
            showMsgView("009");});
	});
    
}

function fnGetCodeNMByID(ID, callback) {
    console.log(ID)
	db.transaction(function (tx, rs) {
		tx.executeSql(" SELECT C901_CODE_NM codenm, C901_CODE_NM Name, C901_CODE_ID codeid, C901_CODE_ID ID, C901_CODE_GRP codegrp FROM T901_CODE_LOOKUP WHERE C901_CODE_ID IN  ("+ID+") ", [],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
            results = _.filter(results, function(data){
                return data.codenm != "TBD"; 
            });
			callback(results);
		},
		function(){
            showMsgView("009");});
	});
    
}

function fnSearchTS(val, callback) {
    console.log(val)
	db.transaction(function (tx, rs) {
		tx.executeSql(" SELECT C6630_ACTIVITY_NM AS Name, C6630_ACTIVITY_NM AS actnm, C6630_ACTIVITY_ID as actid, C6630_ACTIVITY_ID as ID, C6630_FROM_DT actstartdate, C6630_TO_DT actenddate FROM T6630_ACTIVITY WHERE C6630_VOID_FL = '' AND c901_activity_category = '105270' AND UPPER(C6630_ACTIVITY_NM) LIKE UPPER('%"+val+"%')  ORDER BY UPPER(C6630_ACTIVITY_NM) ", [],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
		},
		function(){
            showMsgView("009");});
	});
}

function fnSearchRep(val, callback) {
    console.log(val)
	db.transaction(function (tx, rs) {
		tx.executeSql(" SELECT t703.c703_sales_rep_id as repid ,t703.c703_sales_rep_id as ID,  t703.c101_party_id as partyid, t703.c703_sales_rep_name as repname, t703.c703_sales_rep_name as Name, advp.adnm as adname, advp.vpnm as vpname FROM t703_sales_rep t703 LEFT JOIN (select distinct ad_name adnm, vp_name vpnm, rep_id FROM v700_territory_mapping_detail )advp ON advp.rep_id = t703.c703_sales_rep_id WHERE t703.c703_active_fl = 'Y' AND t703.c703_void_fl   = '' AND upper(t703.c703_sales_rep_name) LIKE upper('%"+val+"%') ORDER BY t703.c703_sales_rep_name  ", [],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
		},
		function(){
            showMsgView("009");});
	});
}

function fnGetADVPbyRepID(val, callback) {
    console.log(val)
	db.transaction(function (tx, rs) {
		tx.executeSql(" SELECT t703.c703_sales_rep_id as repid,  t703.c101_party_id as reppartyid, t703.c703_sales_rep_name as repname, advp.adnm as adname, advp.vpnm as vpname, advp.adid as adid FROM t703_sales_rep t703 LEFT JOIN (select distinct ad_name adnm, vp_name vpnm, rep_id, AD_ID adid FROM v700_territory_mapping_detail )advp ON advp.rep_id = t703.c703_sales_rep_id WHERE t703.c703_active_fl = 'Y' AND t703.c703_void_fl   = '' AND t703.c101_party_id = '"+val+"' ORDER BY t703.c703_sales_rep_name  ", [],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
		},
		function(){
            showMsgView("009");});
	});
}

function fnSearchAccnt(val, callback) {
    console.log(val)
	db.transaction(function (tx, rs) {
		tx.executeSql(" SELECT C704_ACCOUNT_ID as accid, C704_ACCOUNT_ID as ID, C704_ACCOUNT_NM as accnm, C704_ACCOUNT_NM as Name, C704_BILL_CITY as acccity, (select c901_code_nm from t901_code_lookup where c901_code_id = C704_BILL_STATE) as accstate, C704_BILL_STATE as accstateid FROM  T704_ACCOUNT WHERE C704_VOID_FL = '' AND C704_ACTIVE_FL IS NOT NULL and upper(c704_account_nm) like upper('%"+val+"%') ORDER BY C704_ACCOUNT_NM  ", [],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
		},
		function(){
            showMsgView("009");});
	});
}

function fnGetAccnt(id, callback) {
    console.log(id)
	db.transaction(function (tx, rs) {
		tx.executeSql(" SELECT C704_ACCOUNT_ID as accid, C704_ACCOUNT_ID as ID, C704_ACCOUNT_NM as accnm, C704_BILL_CITY as acccity, (select c901_code_nm from t901_code_lookup where c901_code_id = C704_BILL_STATE) as accstate, C704_BILL_STATE as accstateid FROM  T704_ACCOUNT WHERE C704_VOID_FL = '' AND C704_ACTIVE_FL IS NOT NULL and C704_ACCOUNT_ID = ?  ", [id],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
		},
		function(){
            showMsgView("009");});
	});
}

function fnSearchProcedure(val, callback) {
    console.log(val)
	db.transaction(function (tx, rs) {
		tx.executeSql(" SELECT C901_CODE_ID ID, C901_CODE_NM Name FROM T901_CODE_LOOKUP  WHERE C901_ACTIVE_FL = '1' AND C901_VOID_FL = ''  AND C901_CODE_GRP in ('TECHQ')  AND upper(C901_CODE_NM) LIKE upper('%"+val+"%')  ORDER BY C901_CODE_SEQ_NO   ", [],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
		},
		function(){
            showMsgView("009");});
	});
}

function fnSearchState(val, callback) {
    console.log(val)
	db.transaction(function (tx, rs) {
		tx.executeSql(" SELECT C901_CODE_ID ID, C901_CODE_NM Name FROM T901_CODE_LOOKUP  WHERE C901_ACTIVE_FL = '1' AND C901_VOID_FL = ''  AND C901_CODE_GRP in ('STATE')  AND upper(C901_CODE_NM) LIKE upper('%"+val+"%')  ORDER BY C901_CODE_SEQ_NO   ", [],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
		},
		function(){
            showMsgView("009");});
	});
}

function fnSearchSystem(val, callback) {
    console.log(val)
	db.transaction(function (tx, rs) {
		tx.executeSql(" SELECT C207_SET_ID ID, C207_SET_NM Name  FROM T207_SET_MASTER WHERE C207_VOID_FL = ''  AND upper(C207_SET_NM) LIKE upper('%"+val+"%')  ORDER BY C207_SET_NM ", [],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
		},
		function(){
            showMsgView("009");});
	});
}

function fnGetSystem(val, callback) {
    console.log(val)
	db.transaction(function (tx, rs) {
		tx.executeSql(" SELECT C207_SET_ID ID, C207_SET_NM Name  FROM T207_SET_MASTER WHERE C207_VOID_FL = ''  ORDER BY C207_SET_NM ", [],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
		},
		function(){
            showMsgView("009");});
	});
}

function fnGetDistributorByID(val, callback) {
    console.log(val)
    var query = " SELECT C701_DISTRIBUTOR_ID did, C701_DISTRIBUTOR_NAME dname FROM T701_DISTRIBUTOR WHERE C701_VOID_FL = '' AND (C701_ACTIVE_FL = '' OR C701_ACTIVE_FL ='Y') AND C701_DISTRIBUTOR_ID IN (" + val + ")  ORDER BY C701_DISTRIBUTOR_NAME ";
    console.log(query);
	db.transaction(function (tx, rs) {
		tx.executeSql(query, [],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
		},
		function(){
            showMsgView("009");});
	});
}

function fnGetSystemNM(ID, callback) {
    console.log(ID)
	db.transaction(function (tx, rs) {
		tx.executeSql(" SELECT C207_SET_ID sysid, C207_SET_NM sysnm  FROM T207_SET_MASTER WHERE C207_VOID_FL = '' and C207_SET_ID IN ("+ID+")  ORDER BY C207_SET_NM ", [],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
		},
		function(){
            showMsgView("009");});
	});
}

function fnSearchProject(val, callback) {
    console.log(val)
	db.transaction(function (tx, rs) {
		tx.executeSql(" SELECT C202_PROJECT_ID ID, C202_PROJECT_NM Name FROM T202_PROJECT WHERE C202_VOID_FL = ''  AND UPPER(C202_PROJECT_NM) LIKE UPPER('%"+val+"%')  ORDER BY C202_PROJECT_NM ", [],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
		},
		function(){
            showMsgView("009");});
	});
}

function fnSearchParticipant(val, callback) {
    console.log(val)
	db.transaction(function (tx, rs) {
		tx.executeSql(" select t101.c101_party_id ID, t101.C101_FIRST_NM || ' ' || t101.c101_last_nm Name, t6600.c6600_surgeon_npi_id npid from t101_party t101 left join t6600_party_surgeon t6600 on t6600.c101_party_surgeon_id = t101.c101_party_id where (t101.c101_active_fl = '' or t101.c101_active_fl = 'Y') and t101.c101_void_fl = '' and t101.c901_party_type   in ('7005','7006') AND upper(t101.C101_FIRST_NM || ' ' || t101.c101_last_nm) like upper('%"+val+"%') ORDER BY t101.C101_FIRST_NM ", [],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
		},
		function(){
            showMsgView("009");});
	});
}

function fnSearchSurgeon(val, callback, calbk) {
    console.log(val)
    console.log(callback)
    if(calbk){  // to make it compatible with older searchview 
        val = callback;
        callback = calbk;
    }
	db.transaction(function (tx, rs) {
		tx.executeSql(" select t101.c101_party_id ID, t101.C101_FIRST_NM || ' ' || t101.c101_last_nm Name, t6600.c6600_surgeon_npi_id npid from t101_party t101 left join t6600_party_surgeon t6600 on t6600.c101_party_surgeon_id = t101.c101_party_id where (t101.c101_active_fl = '' or t101.c101_active_fl = 'Y') and t101.c101_void_fl = '' and t101.c901_party_type   in ('7000') AND upper(t101.C101_FIRST_NM || ' ' || t101.c101_last_nm) like upper('%"+val+"%') ORDER BY t101.C101_FIRST_NM ", [],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
		},
		function(){
            showMsgView("009");});
	});
}

function fnGetActivityDetail(actid, callback) {
    console.log(actid)
	db.transaction(function (tx, rs) {
		tx.executeSql(" SELECT C6630_ACTIVITY_ID actid, C6630_ACTIVITY_NM actnm, C6630_FROM_DT actstartdate, C6630_TO_DT actenddate, C901_ACTIVITY_TYPE acttype,   C901_ACTIVITY_STATUS actstatus, C901_ACTIVITY_CATEGORY actcategory, C6630_PARTY_ID partyid, C6630_PARTY_NM partynm, C6630_FROM_TM  actstarttime, C6630_TO_TM actendtime, C6630_VOID_FL voidfl, C901_ACTIVITY_TYPE_NM acttypenm, C6630_CREATED_BY createdbyid FROM T6630_ACTIVITY WHERE C6630_VOID_FL = '' AND C6630_ACTIVITY_ID =  ? ", [actid],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
		},
		function(){
            showMsgView("009");});
	});
}

function fnGetActivityAttrDetail(actid, callback) {
    console.log(actid)
	db.transaction(function (tx, rs) {
		tx.executeSql(" SELECT c6632_activity_attribute_id attrid, c6630_activity_id actid, c6632_attribute_value attrval, c6632_attribute_nm attrnm, c901_attribute_type attrtype,   c6632_void_fl voidfl FROM t6632_activity_attribute WHERE c6632_void_fl = '' and c6630_activity_id =  ? ", [actid],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
		},
		function(){
            showMsgView("009");});
	});
}

function fnGetActivityAddrDetail(addid, callback) {
    console.log(addid)
	db.transaction(function (tx, rs) {
		tx.executeSql(" select C106_ADDRESS_ID addid, C106_ADD1 add1, C106_ADD2 add2, C106_CITY city, C901_STATE stateid, (SELECT C901_CODE_NM FROM T901_CODE_LOOKUP WHERE C901_CODE_ID  = C901_STATE) statenm, C901_COUNTRY countryid,(SELECT C901_CODE_NM FROM T901_CODE_LOOKUP WHERE C901_CODE_ID  = C901_COUNTRY) countrynm, C106_PRIMARY_FL primaryfl, C106_VOID_FL voidfl, C106_ZIP_CODE zip from t106_address where C106_VOID_FL = '' and C106_ADDRESS_ID =  ? ", [addid],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
		},
		function(){
            showMsgView("009");});
	});
}

function fnGetCRMDocumentDetail(refid, refgrp, callback) {
    console.log(refid)
    console.log(refgrp)
    if(refgrp == undefined)
        refgrp = "AND C901_REF_GRP IN ('107993','107994','103112')";
    else
        refgrp = "AND C901_REF_GRP IN ("+refgrp+")";
    var query = " SELECT C903_UPLOAD_FILE_LIST fileid, C903_REF_ID refid, C903_FILE_NAME filename, C901_REF_TYPE reftype, C901_TYPE type, C901_REF_GRP refgroup, C901_FILE_TITLE filetitle, C903_LAST_UPDATED_DATE updateddate, C903_LAST_UPDATED_BY updatedby FROM T903_UPLOAD_FILE_LIST WHERE C903_REF_ID = '"+refid+"' "+refgrp+" AND C903_DELETE_FL  = '' ";
    console.log(query);
	db.transaction(function (tx, rs) {
		tx.executeSql(query, [],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
		},
		function(){
            showMsgView("009");});
	});
}

function fnGetActivityPartyDetail(actid, callback) {
    console.log(actid)
	db.transaction(function (tx, rs) {
		tx.executeSql(" SELECT T6631.C6631_ACTIVITY_PARTY_ID actpartyid, T6631.C6630_ACTIVITY_ID actid, T6631.C901_ACTIVITY_PARTY_ROLE roleid, T6631.C901_ACTIVITY_PARTY_ROLE_NM rolenm, T6631.C101_PARTY_ID partyid, T6631.C6631_VOID_FL voidfl, T6631.C901_PARTY_STATUS statusid, T6631.C901_PARTY_STATUS_NM statusnm, T101.C101_FIRST_NM || ' ' || T101.C101_LAST_NM partynm , T6600.C6600_SURGEON_NPI_ID partynpid, t101.c901_party_type partytype FROM T6631_ACTIVITY_PARTY_ASSOC T6631 LEFT JOIN T101_PARTY T101 ON T6631.C101_PARTY_ID = T101.C101_PARTY_ID LEFT JOIN T6600_PARTY_SURGEON T6600 ON T6631.C101_PARTY_ID = T6600.C101_PARTY_SURGEON_ID WHERE T6631.C6631_VOID_FL = '' AND T6631.C6630_ACTIVITY_ID = ? ", [actid],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
		},
		function(){
            showMsgView("009");});
	});
}

function fnSaveSurgeon(ID, data, callback) {
    console.log("fnSaveSurgeon")
    console.log(data)	
    var dispData = $.parseJSON(JSON.stringify(data));
    dispData.arrgmcrmfileuploadvo = _.filter(dispData.arrgmcrmfileuploadvo, function(data){
        return data.deletefl != "Y";
    });
    dispData.arrgmcrmsurgeonaffiliationvo = _.filter(dispData.arrgmcrmsurgeonaffiliationvo, function(data){
        return data.voidfl != "Y";
    });
    dispData.arrgmcrmsurgeonattrvo = _.filter(dispData.arrgmcrmsurgeonattrvo, function(data){
        return data.voidfl != "Y";
    });
    dispData.arrgmcrmsurgeonsurgicalvo = _.filter(dispData.arrgmcrmsurgeonsurgicalvo, function(data){
        return data.voidfl != "Y";
    });
    dispData.arrgmsurgeoncontactsvo = _.filter(dispData.arrgmsurgeoncontactsvo, function(data){
        return data.voidfl != "Y";
    });
    dispData.arrgmsurgeonsaddressvo = _.filter(dispData.arrgmsurgeonsaddressvo, function(data){
        return data.voidfl != "Y";
    });
//    data.arrgmcrmfileuploadvo = _.uniq(data.arrgmcrmfileuploadvo, function(data){
//        return data.filetitle && data.reftype;
//    });
//    data.arrgmcrmsurgeonaffiliationvo = _.uniq(data.arrgmcrmsurgeonaffiliationvo, function(data){
//        return data.accnm && data.accid;
//    });
//    data.arrgmcrmsurgeonattrvo = _.uniq(data.arrgmcrmsurgeonattrvo, function(data){
//        return data.attrvalue;
//    });
//    data.arrgmcrmsurgeonsurgicalvo = _.uniq(data.arrgmcrmsurgeonsurgicalvo, function(data){
//        return data.sysid && data.procedureid;
//    });
//    data.arrgmsurgeoncontactsvo = _.uniq(data.arrgmsurgeoncontactsvo, function(data){
//        return data.conmode && data.convalue;
//    });
//    data.arrgmsurgeonsaddressvo = _.uniq(data.arrgmsurgeonsaddressvo, function(data){
//        return data.add1 && data.add2 && data.city && data.stateid && data.countryid;
//    });
    console.log("dispData");
    console.log(dispData);
    var count = 0;
	var user = localStorage.getItem('userID');
    var tempID = window.location.href.split('/');
    var size = tempID.length;   // taking id from url 'coz we use temp ids for create type data
    tempID = tempID[size-1];
    if(window.location.href.indexOf('id')>=0 && tempID!=0 && tempID!=undefined)
        var partyid = tempID;
    else
        var partyid = "";
    db.transaction(function (tx, rs) {     // In case we save before table sync (for offline mode) 
		tx.executeSql(" SELECT SURGEON_REQUEST_ID id FROM TBL_SURGEON WHERE USER = ? AND STATUS = 'Pending Sync' AND PARTYID = ? ", [user,partyid],
				function(tx,rs) {   
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
            db.transaction(function (tx, rs) {
                    tx.executeSql("SELECT MAX(SURGEON_REQUEST_ID) count FROM TBL_SURGEON ", [],
                        function(tx,rs) {
                            console.log(rs);
                            var i=0, res = [];
                            for(i=0;i<rs.rows.length;i++) {
                                res.push(rs.rows.item(i));
                            }
                            if(res.length)
                                count = res[0].count;
                            console.log(res);
                            console.log(results);
                            if(results.length){
                                db.transaction(function (tx, rs) {
                                    tx.executeSql("INSERT OR REPLACE INTO TBL_SURGEON(SURGEON_REQUEST_ID, USER, REQUESTDATA, DISPLAYDATA, STATUS, PARTYID) VALUES (?,?,?,?,?,?) ", [results[0].id, user, JSON.stringify(data),JSON.stringify(dispData), 'Pending Sync', partyid],
                                        function(tx,rs) {
                                            callback("success", partyid);
                                        },
                                    function(e,err){
                                        console.log(JSON.stringify(err));
                                    });
                                });
                            }
                            else{                   // New Detail
                                console.log(count)
                                if(partyid==""){
                                    var ID = "sgn"+((count+1).toString()); // use SURGEON_REQUEST_ID
                                }
                                else
                                    var ID = partyid;
                                console.log(ID)
                                db.transaction(function (tx, rs) {
                                        tx.executeSql("INSERT INTO TBL_SURGEON(USER, REQUESTDATA, DISPLAYDATA, STATUS, PARTYID) VALUES (?,?,?,?,?) ", [user, JSON.stringify(data),JSON.stringify(dispData),'Pending Sync', ID],
                                            function(tx,rs) {
                                            if(rs.rows.length>0)
                                                console.log(rs.rows.item(0));
                                            callback("success", ID);
                                            },
                                        function(e,err){
                                            console.log(JSON.stringify(err));
                                        });
                                    });
                            }
                        },
                    function(e,err){
                        console.log(JSON.stringify(err));
                    });
                });
            
            
		},  
		function(){
            showMsgView("009");});
	});
}

function fnCheckSurgeonPendingSync() {
    var userId = localStorage.getItem("userID");
    var token = localStorage.getItem("token");
    var refresh = "", module = "", id = "";
        
    if(token!=null){
        db.transaction(function (tx, rs) {
            tx.executeSql("SELECT * FROM TBL_SURGEON WHERE USER = ? AND STATUS = 'Pending Sync'", [userId],
                function(tx,rs) {
                    var i=0, results = [];
                    for(i=0;i<rs.rows.length;i++) {
                        results.push(rs.rows.item(i));
                    }
                console.log(results);
                var len = results.length-1;
                _.each(results, function(dbData, i){
                    var input = $.parseJSON(dbData.REQUESTDATA);
                    var dataID = dbData.SURGEON_REQUEST_ID;
                    input.token = token;
                    console.log(input)
                    fnGetWebServerData("surgeon/save", undefined, input, function(data){
                        console.log(data);
                        if(data!=null){
                            db.transaction(function (tx, rs) {
                                tx.executeSql("UPDATE TBL_SURGEON  SET PARTYID = '"+data.partyid+"' , STATUS = 'Pending Local Sync'  WHERE SURGEON_REQUEST_ID = ?", [dataID],
                                function(tx,rs) {
                                    console.log("success");
                                    if(i == len)
                                        fnUpdateSurgeon();
                                },
                                function(e,err){
                                    //none
                                });
                            });
                        }
                    }, true);
                });
            },function (tx,err) {
                console.log(JSON.stringify(err));
            });
        });
    }
}

function fnCheckCriteriaPendingSync() {
    
    var userId = localStorage.getItem("userID");
    var token = localStorage.getItem("token");
    if(token!=null){
        db.transaction(function (tx, rs) {
            tx.executeSql("SELECT * FROM TBL_CRITERIA WHERE USER = ? AND STATUS = 'Pending Sync'", [userId],
                function(tx,rs) {
                    var i=0, results = [];
                    for(i=0;i<rs.rows.length;i++) {
                        results.push(rs.rows.item(i));
                    }
                console.log(results);
                var len = results.length-1;
                _.each(results, function(dbData, i){
                    var input = $.parseJSON(dbData.REQUESTDATA);
                    var dataID = dbData.CRITERIA_REQUEST_ID;
                    var crtid = dbData.CRITERIAID
                    input.token = token;
                    console.log(input)
                    fnGetWebServerData("criteria/save", undefined, input, function(data){
                        console.log(data);
                        if(data!=null){
                            db.transaction(function (tx, rs) {
                                tx.executeSql("UPDATE TBL_CRITERIA  SET CRITERIAID = '"+data.criteriaid+"' , STATUS = 'Pending Local Sync'  WHERE CRITERIA_REQUEST_ID = ?", [dataID],
                                function(tx,rs) {
                                    console.log("success");
                                    if(i == len)
                                        fnUpdateCriteria();
                                },
                                function(e,err){
                                    //none
                                });
                            });
                        }
                    }, true);
                });
            },function (tx,err) {
                console.log(JSON.stringify(err));
            });
        });
    }
}

function fnGetOfflineSurgeonSavedData(partyid, callback) {
    console.log(partyid)
    var userId = localStorage.getItem("userID");
	db.transaction(function (tx, rs) {
		tx.executeSql(" SELECT DISPLAYDATA data FROM TBL_SURGEON WHERE STATUS = 'Pending Sync' AND USER = ? AND PARTYID = ? ", [userId, partyid],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
		},
		function(){
            showMsgView("009");});
	});
}


// Saves the data from Activies in Offline Mode
function fnSaveActivity(data, callback) {
	console.log(data)	
    var dispData = $.parseJSON(JSON.stringify(data));    // Prepare data to display post save. Removes data voided in Offline Mode
    dispData.arrgmactivityattrvo = _.filter(data.arrgmactivityattrvo, function(data){
        return data.voidfl != "Y";
    });
    dispData.arrgmactivitypartyvo = _.filter(data.arrgmactivitypartyvo, function(data){
        return data.voidfl != "Y";
    });
    dispData.arrgmcrmaddressvo = _.filter(data.arrgmcrmaddressvo, function(data){
        return data.voidfl != "Y";
    });
    dispData.arrgmcrmlogvo = _.filter(data.arrgmcrmlogvo, function(data){
        return data.voidfl != "Y";
    });
    console.log(data)
    console.log(dispData)
    
    // Removing Duplicate Data
//    data.arrgmactivityattrvo = _.uniq(data.arrgmactivityattrvo, function(data){
//        return data.attrval && data.attrnm;
//    });
//    data.arrgmactivitypartyvo = _.uniq(data.arrgmactivitypartyvo, function(data){
//        return data.partyid && data.roleid;
//    });
//    data.arrgmcrmaddressvo = _.uniq(data.arrgmcrmaddressvo, function(data){
//        return data.add1 && data.add2 && data.city && data.stateid && data.countryid;
//    });
    console.log(data)
    var count = 0;
	var user = localStorage.getItem('userID');
    console.log(lastOf(GM.Global.Url));
    var tempID = lastOf(GM.Global.Url).split('/');
    var size = tempID.length;
    tempID = tempID[size-1];
    console.log(tempID);
    // identify create/edit mode
    if(lastOf(GM.Global.Url).indexOf('id')>=0 && tempID!=0 && tempID)
        var actid = tempID;
    else
        var actid = "";
    db.transaction(function (tx, rs) {     // Check if saving data that is already in temporary table for Pending Sync for SAME USER
		tx.executeSql(" SELECT ACTIVITY_REQUEST_ID id FROM TBL_ACTIVITY WHERE USER = ? AND STATUS = 'Pending Sync' AND ACTID = ? ", [user,actid],
				function(tx,rs) {   
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
            db.transaction(function (tx, rs) {
                    tx.executeSql("SELECT ROWID count FROM TBL_ACTIVITY ", [],
                        function(tx,rs) {
                            var i=0, res = [];
                            for(i=0;i<rs.rows.length;i++) {
                                res.push(rs.rows.item(i));
                            }
                            if(res.length)
                                count = res[0].count;
                            console.log(res);
                            console.log(results);
                            if(results.length){     // Data that is already in temporary table for Pending Sync
                                db.transaction(function (tx, rs) {
                                    tx.executeSql("INSERT OR REPLACE INTO TBL_ACTIVITY(ACTIVITY_REQUEST_ID, USER, DISPLAYDATA, REQUESTDATA, STATUS, ACTID) VALUES (?,?,?,?,?,?) ", [results[0].id, user, JSON.stringify(dispData),JSON.stringify(data), 'Pending Sync', actid],
                                        function(tx,rs) {
                                            callback("success", actid);
                                        },
                                    function(e,err){
                                        console.log(JSON.stringify(err));
                                    });
                                });
                            }
                            else{                   // New Detail
                                console.log(count)
                                if(actid=="")
                                    var ID = "act"+((count+1).toString()); // use ACTIVITY_REQUEST_ID
                                else
                                    var ID = actid;
                                console.log(ID)
                                db.transaction(function (tx, rs) {
                                        tx.executeSql("INSERT INTO TBL_ACTIVITY(USER, DISPLAYDATA, REQUESTDATA, STATUS, ACTID) VALUES (?,?,?,?,?) ", [user,JSON.stringify(dispData), JSON.stringify(data),'Pending Sync', ID],
                                            function(tx,rs) {
                                            if(rs.rows.length>0)
                                                console.log(rs.rows.item(0));
                                            callback("success", ID);
                                            },
                                        function(e,err){
                                            console.log(JSON.stringify(err));
                                        });
                                    });
                            }
                        },
                    function(e,err){
                        console.log(JSON.stringify(err));
                    });
                });
		},  
		function(){
            showMsgView("009");});
	});
}

function fnPendInfoActivity(callback) {
    var userID = localStorage.getItem("userID");
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT DISPLAYDATA data, ACTID id FROM TBL_ACTIVITY WHERE USER = '"+userID+"' AND STATUS = 'Pending Sync'", [],
            function(tx,rs) {
                var i=0, results = [];
                for(i=0;i<rs.rows.length;i++) {
                    results.push(rs.rows.item(i));
                }
                console.log(results);
                if(results.length){
                    _.each(results, function(a,i){
                        var d = $.parseJSON(a.data);
                        a.actnm = d.actnm;
                        a = actAttr(a, d.actcategory);
                        if((i+1) == results.length)      
                            callback(results);
                    });
                }
                else
                    callback([]);
            },
        function(e,err){
            console.log(JSON.stringify(err));
        });
    });
}

function fnPendInfoSurgeon(callback) {
    var userID = localStorage.getItem("userID");
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT DISPLAYDATA data, PARTYID id FROM TBL_SURGEON WHERE USER = '"+userID+"' AND STATUS = 'Pending Sync'", [],
            function(tx,rs) {
                var i=0, results = [];
                for(i=0;i<rs.rows.length;i++) {
                    results.push(rs.rows.item(i));
                }
                console.log(results);
                if(results.length){
                    _.each(results, function(a,i){
                        var d = $.parseJSON(a.data);
                        a.name = d.firstnm + (d.midinitial ? (" " + d.midinitial) : "") + " " + d.lastnm;
                        a.npid = d.npid;
                        if((i+1) == results.length)      
                            callback(results);
                    });
                }
                else
                    callback([]);
            },
        function(e,err){
            console.log(JSON.stringify(err));
        });
    });
}

function fnPendInfoCriteria(callback) {
    var userID = localStorage.getItem("userID");
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT DISPLAYDATA data, CRITERIAID id FROM TBL_CRITERIA WHERE USER = '"+userID+"' AND STATUS = 'Pending Sync'", [],
            function(tx,rs) {
                var i=0, results = [];
                for(i=0;i<rs.rows.length;i++) {
                    results.push(rs.rows.item(i));
                }
                console.log(results);
                if(results.length){
                    _.each(results, function(a,i){
                        var d = $.parseJSON(a.data);
                        console.log(d)
                        a.module = d.criteriatyp
                        a.criteriatyp = d.criteriatyp ? d.criteriatyp.toUpperCase() : d.criteriatyp ;
                        a.criterianm = d.criterianm;
                        if((i+1) == results.length)      
                            callback(results);
                    });
                }
                else
                    callback([]);
            },
        function(e,err){
            console.log(JSON.stringify(err));
        });
    });
}

function fnUpdateSurgeon() {
    var input = {"token":GM.Global.Token,"langid":"103097","pageno":1,"uuid":"device.uuid","countryid":"80120"};
    
//    DataSync_SL("GmCRMSurgeonAffiliationVO",input,function(status) {
//        console.log("GmCRMSurgeonAffiliationVO--------------->"+status);
//        DataSync_SL("GmCRMSurgeonSurgicalVO",input,function(status) {
//            console.log("GmCRMSurgeonSurgicalVO----------------->"+status);
            DataSync_SL("GmCRMSurgeonPartyVO",input,function(status) {
                 console.log("GmCRMSurgeonPartyVO----------------->"+status);
//                DataSync_SL("GmCRMSurgeonAttrVO",input,function(status) {
//                 console.log("GmCRMSurgeonAttrVO----------------->"+status);
                    DataSync_SL("listGmFileVO",input,function(status) {
                     console.log("listGmFileVO----------------->"+status);
                        DataSync_SL("listGmPartyVO",input,function(status) {
                            console.log("listGmPartyVO----------------->"+status);
                            DataSync_SL("listGmPartyContactVO",input,function(status) {
                                console.log("listGmPartyContactVO----------------->"+status);
                                DataSync_SL("listGmAddressVO",input,function(status) {
                                    console.log("listGmAddressVO----------------->"+status);
                                    fnSurgeonSyncCompleted();
                                });
                            });
                        });
                    });
//                });
            });
//        });
//    });
    
}
function fnUpdateActivity() {
    var input = {"token":GM.Global.Token,"langid":"103097","pageno":1,"uuid":"device.uuid","countryid":"80120"};
    var VO = ["listGmActivityVO", "listGmActivityPartyVO", "listGmActivityAttrVO"];
    DataSync_SL("listGmActivityVO",input,function(status) {
        console.log("listGmActivityVO--------------->"+status);
        DataSync_SL("listGmActivityPartyVO",input,function(status) {
            console.log("listGmActivityPartyVO----------------->"+status);
            DataSync_SL("listGmActivityAttrVO",input,function(status) {
                 console.log("listGmActivityAttrVO----------------->"+status);
                DataSync_SL("listGmAddressVO",input,function(status) {
                    console.log("listGmAddressVO----------------->"+status);
                    fnActivitySyncCompleted();
                });
            });
        });
    });
    
}


function fnUpdateCRF() {
    var input = {"token":GM.Global.Token,"langid":"103097","pageno":1,"uuid":"device.uuid","countryid":"80120"};
    DataSync_SL("listGmCRFVO",input,function(status) {
        console.log("listGmCRFVO--------------->"+status);
                    fnCRFSyncCompleted();
    });
    
}
function fnUpdateCriteria() {
    var input = {"token":GM.Global.Token,"langid":"103097","pageno":1,"uuid":"device.uuid","countryid":"80120"};
    DataSync_SL("listGmCriteriaVO",input,function(status) {
        console.log("listGmCriteriaVO--------------->"+status);
        DataSync_SL("listGmCriteriaAttrVO",input,function(status) {
        console.log("listGmCriteriaAttrVO--------------->"+status);
                    fnCriteriaSyncCompleted();
        });
    });
    
}
function fnUpdateCRF_APPR() {
    var input = {"token":GM.Global.Token,"langid":"103097","pageno":1,"uuid":"device.uuid","countryid":"80120"};
    DataSync_SL("listGmCRFApprvVO",input,function(status) {
        console.log("listGmCRFApprvVO--------------->"+status);
                    fnCRF_APPRSyncCompleted();
    });
    
}

function fnSurgeonSyncCompleted(){
    db.transaction(function (tx, rs) {
        tx.executeSql("DELETE FROM TBL_SURGEON WHERE STATUS = 'Pending Local Sync'", [],
        function(tx,rs) {
            console.log("success of Surgeon Table Sync Completion")
            if(window.location.hash.indexOf("sgn")>=0){
                showSuccess("Updating your Information...");
                window.location.href = "#crm/dashboard";
            }
        },
        function(e,err){
            //none
        });
    });
}

function fnCriteriaSyncCompleted(){
    db.transaction(function (tx, rs) {
        tx.executeSql("DELETE FROM TBL_CRITERIA WHERE STATUS = 'Pending Local Sync'", [],
        function(tx,rs) {
            console.log("success of Criteria Table Sync Completion")
            if(window.location.hash.indexOf("crt")>=0){
                showSuccess("Updating your Information...");
                window.location.href = "#crm/dashboard";
            }
        },
        function(e,err){
            //none
        });
    });
}

function fnActivitySyncCompleted(){
    db.transaction(function (tx, rs) {
        tx.executeSql("DELETE FROM TBL_ACTIVITY WHERE STATUS = 'Pending Local Sync'", [],
        function(tx,rs) {
            console.log("success of Activity Table Sync Completion");
            if(window.location.hash.indexOf("act")>=0){
                showSuccess("Updating your Information...");
                window.location.href = "#crm/dashboard";
            }
                
        },
        function(e,err){
            //none
        });
    });
    
}

function fnCheckActivityPendingSync() {
	var userId = localStorage.getItem("userID");
    var token = localStorage.getItem("token");
        
    if(token!=null){
        db.transaction(function (tx, rs) {
            tx.executeSql("SELECT * FROM TBL_ACTIVITY WHERE USER = ? AND STATUS = 'Pending Sync'", [userId],
                function(tx,rs) {
                    var i=0, results = [];
                    for(i=0;i<rs.rows.length;i++) {
                        results.push(rs.rows.item(i));
                    }
                console.log(results);
                var len = results.length-1;
                _.each(results, function(dbData, i){
                    var input = $.parseJSON(dbData.REQUESTDATA);
                    var dataID = dbData.ACTIVITY_REQUEST_ID;
                    input.token = token;
                    console.log(input)
                    var val = $.parseJSON(JSON.stringify(input));
                    var todayDate = new Date();
                    todayDate.setHours(0, 0, 0, 0);
                    var visitSDate = new Date(val.actstartdate);
                    visitSDate.setHours(0, 0, 0, 0);
                    if (input.pvEdit)
                        delete input.pvEdit;
                    fnGetWebServerData("crmactivity/save", undefined, input, function(data){
                        console.log(data);
                        if(data!=null){
                            console.log(val.devicefl)
                            console.log(val.actcategory)
                            if(val.devicefl == "Y" && val.actcategory == "105267")
                                createEvent(data);
                            db.transaction(function (tx, rs) {
                                tx.executeSql("UPDATE TBL_ACTIVITY  SET ACTID = '"+val.actid+"' , STATUS = 'Pending Local Sync'  WHERE ACTIVITY_REQUEST_ID = ?", [dataID],
                                function(tx,rs) {
                                    console.log("success");
                                    if(i == len)
                                        fnUpdateActivity();
                                },
                                function(e,err){
                                    //none
                                });
                            });
                        }
                    }, true);
                });
            },function (tx,err) {
                console.log(JSON.stringify(err));
            });
        });
    }
}

function fnGetOfflineCriteriaSavedData(cid, callback) {
    console.log(cid)
    var userId = localStorage.getItem("userID");
	db.transaction(function (tx, rs) {
		tx.executeSql(" SELECT DISPLAYDATA data FROM TBL_CRITERIA WHERE STATUS = 'Pending Sync' AND USER = ? AND CRITERIAID = ? ", [userId, cid],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
		},
		function(){
            showMsgView("009");});
	});
}

function fnGetOfflineActivitySavedData(actid, callback) {
    console.log(actid)
    var userId = localStorage.getItem("userID");
	db.transaction(function (tx, rs) {
		tx.executeSql(" SELECT DISPLAYDATA data FROM TBL_ACTIVITY WHERE USER = ? AND ACTID = ? ", [userId, actid],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
		},
		function(){
            showMsgView("009");});
	});
}

function fnGetListActivity(inp,type, callback) {
    console.log("Activity List Input")
    console.log(inp)
    var mid = "", mid2 = "", query = "",mid3 = "";
    if(type == "UPC"  || inp.actcategory == '105267'){
        mid = " AND substr(T6630.C6630_FROM_DT, 7, 4) || '/' || substr(T6630.C6630_FROM_DT, 1,2) || '/' || substr(T6630.C6630_FROM_DT, 4, 2) >= '"+inp.today+"' ";
        if(type == "UPC" && !(inp.actcategory == '105268' || inp.actcategory == '105265'))
            mid = mid+" AND T6630.C901_ACTIVITY_STATUS  IN ("+inp.actstat+") ";
    }
    if(type == "PEND"){
        mid = mid+" AND T6605.C901_STATUS IN ("+inp.status+") ";
    }
    if(inp.acttypeid){
        mid = mid+" AND T6630.C901_ACTIVITY_TYPE IN ("+inp.acttypeid+") ";
    }
    if(inp.fn || inp.ln){
        if(inp.fn!="" && inp.ln!="")
            mid = mid+" AND UPPER(T101.C101_FIRST_NM || ' ' || T101.C101_LAST_NM) LIKE UPPER('%' || '"+inp.fn+"' ||'%') AND UPPER(T101.C101_FIRST_NM || ' ' || T101.C101_LAST_NM) LIKE UPPER('%' || '"+inp.ln+"' ||'%') ";
        else if(inp.fn!="" && inp.ln=="")
            mid = mid+" AND UPPER(T101.C101_FIRST_NM) LIKE UPPER('%' || '"+inp.fn+"' ||'%') ";
        else if(inp.fn=="" && inp.ln!="")
            mid = mid+" AND UPPER(T101.C101_LAST_NM) LIKE UPPER('%' || '"+inp.ln+"' ||'%') ";
    }
    if(inp.crfstatus && type == ""){
        mid = mid+" AND T6605.C901_STATUS IN ("+inp.crfstatus+")  ";
        mid2 = mid2+" join T6605_CRF T6605 on T6630.C6630_ACTIVITY_ID = T6605.C6630_ACTIVITY_ID  ";
    }
    if(inp.actstartdate && type == ""){
        mid = mid+" AND substr(T6630.C6630_FROM_DT, 1, 2) || '/' || substr(T6630.C6630_FROM_DT, 4,2) || '/' || substr(T6630.C6630_FROM_DT, 7, 4) >= '"+inp.actstartdate+"' AND substr(T6630.C6630_TO_DT, 1, 2) || '/' || substr(T6630.C6630_TO_DT, 4,2) || '/' || substr(T6630.C6630_TO_DT, 7, 4) <= '"+inp.actenddate+"' ";
    }
    if(type == "RQST"){
        mid = mid+" AND substr(T6630.C6630_FROM_DT, 7, 4) || '/' || substr(T6630.C6630_FROM_DT, 1,2) || '/' || substr(T6630.C6630_FROM_DT, 4, 2) >= '"+inp.today+"' AND T6630.C901_ACTIVITY_STATUS  IN ("+inp.actstat+") ";
    }
    if(inp.actnm){
        mid = mid+" AND UPPER(T6630.C6630_ACTIVITY_NM) LIKE UPPER('%"+inp.actnm+"%') ";
    }
    if(inp.procedure){
        mid2 = mid2+" join (SELECT T6632.C6630_ACTIVITY_ID ACTID FROM t6632_activity_attribute T6632 WHERE T6632.C901_ATTRIBUTE_TYPE  = '105588' AND t6632.C6632_VOID_FL = '' AND T6632.C6632_ATTRIBUTE_VALUE IN ("+inp.procedure+") ) TECHS on T6630.C6630_ACTIVITY_ID = TECHS.ACTID ";
    }
    if(inp.systemid){
        mid2 = mid2+" join(SELECT T6632.C6630_ACTIVITY_ID ACTID FROM t6632_activity_attribute T6632 WHERE T6632.C901_ATTRIBUTE_TYPE  = '105582' AND t6632.C6632_VOID_FL = ''   AND T6632.C6632_ATTRIBUTE_VALUE IN ("+inp.systemid+") ) PDTS on T6630.C6630_ACTIVITY_ID = PDTS.ACTID "; 
    }
    if(inp.state){
        mid = mid+" and address.state = '"+inp.state+"' ";
        if(inp.actcategory == "105269") {    //Lead
            mid2 = mid2+" join (SELECT t106.C901_STATE state, (SELECT c901_code_nm FROM t901_code_lookup WHERE c901_code_id = t106.C901_STATE ) ||', ' || (SELECT c901_code_nm FROM t901_code_lookup WHERE c901_code_id = t106.C901_country ) AS location, t6632.C6630_ACTIVITY_ID actid   FROM t6632_activity_attribute t6632 join t106_address t106 on t106.C106_ADDRESS_ID =  t6632.C6632_ATTRIBUTE_VALUE WHERE t6632.C901_ATTRIBUTE_TYPE = '7777' AND T6632.c6632_void_fl  = '') address on T6630.C6630_ACTIVITY_ID    = address.actid ";
        }
            
    }
    if(type == "MYLDSC"){       //My Lead / Sales Call
        var id = localStorage.getItem("partyID");
        
        if(inp.repid)
            id = inp.repid
        if(inp.id)
            id = inp.id
            mid = mid+" AND T6630.C6630_PARTY_ID = '"+id+"' ";
    }
    
    
    if(inp.actcategory == '105266'){                 // PhysicianVisit
        if(inp.surgeon)
            mid = mid + " and T6631.C101_PARTY_ID = "+inp.surgeon+" ";
        query =  " SELECT DISTINCT GROUP_CONCAT(title, ', ') title, id, actstartdate, actstarttime, actendtime,  acttype, actstatus FROM (SELECT DISTINCT T101.C101_FIRST_NM || ' ' || T101.C101_LAST_NM  AS title,      T6630.C6630_ACTIVITY_ID     AS id, T6630.C6630_FROM_DT         AS actstartdate, T6630.C901_ACTIVITY_TYPE_NM AS acttype, T6630.C6630_FROM_TM actstarttime, T6630.C6630_TO_TM actendtime, (select c901_code_nm from t901_code_lookup where c901_code_id = T6630.C901_ACTIVITY_STATUS) actstatus   FROM T6630_ACTIVITY T6630,     T6631_ACTIVITY_PARTY_ASSOC T6631, T101_PARTY T101 "+mid2+" WHERE T6630.C6630_ACTIVITY_ID    = T6631.C6630_ACTIVITY_ID AND T101.C101_PARTY_ID =  T6631.C101_PARTY_ID  AND T101.C901_PARTY_TYPE         = '7000' AND T6630.C901_ACTIVITY_CATEGORY = '105266' AND T6630.C6630_VOID_FL  = '' "+mid+" ) GROUP BY id order by upper(title) ";
    }
    else if(inp.actcategory == '105265' || inp.actcategory == '105268'){           // Merc - Training
       if(inp.surgeon){ // Merc - Training
             mid2 = mid2+" join T6631_ACTIVITY_PARTY_ASSOC T6631 on T6631.c6630_activity_id = t6630.c6630_activity_id ";
            mid = mid + " and T6631.c101_party_id = "+inp.surgeon+" ";
        }
        query = " SELECT DISTINCT T6630.C6630_ACTIVITY_NM AS topic, T6630.C6630_ACTIVITY_NM  AS actnm, T6630.C6630_FROM_DT AS DATE, T6630.C6630_FROM_DT  AS actstartdate,   T6630.C6630_TO_DT  AS actenddate, T6630.C6630_ACTIVITY_ID AS id, T6630.C6630_ACTIVITY_ID AS actid,   address.location AS location FROM T6630_ACTIVITY T6630 left join (SELECT t106.C901_STATE state, t106.c106_city ||', ' || (SELECT c901_code_nm FROM t901_code_lookup WHERE c901_code_id = t106.C901_STATE ) AS location, t6632.C6630_ACTIVITY_ID actid   FROM t6632_activity_attribute t6632 left join t106_address t106 on t106.C106_ADDRESS_ID =  t6632.C6632_ATTRIBUTE_VALUE WHERE t6632.C901_ATTRIBUTE_TYPE = '7777' AND T6632.c6632_void_fl  = '') address on T6630.C6630_ACTIVITY_ID    = address.actid "+mid2+" WHERE T6630.C901_ACTIVITY_CATEGORY = '"+inp.actcategory+"' and T6630.C6630_VOID_FL  = '' "+mid+" order by upper(T6630.C6630_ACTIVITY_NM) ";
    }
    else if(inp.crf){
        if(inp.crffrmdate)
            mid = mid + " AND substr(T6605.C6605_REQUEST_DATE, 7, 4) || '/' || substr(T6605.C6605_REQUEST_DATE, 1,2) || '/' || substr(T6605.C6605_REQUEST_DATE, 4, 2) >= '"+inp.crffrmdate+"' AND substr(T6605.C6605_REQUEST_DATE, 7, 4) || '/' || substr(T6605.C6605_REQUEST_DATE, 1,2) || '/' || substr(T6605.C6605_REQUEST_DATE, 4, 2) <= '"+inp.crftodate+"' ";
        query = " SELECT distinct T101.C101_FIRST_NM firstnm, T101.C101_LAST_NM lastnm, T6605.C101_PARTY_ID partyid, T6605.C6605_PRIORITY_FL priorityfl, T6605.C6630_ACTIVITY_ID actid,   T6630.C6630_ACTIVITY_NM actnm, T6605.C901_STATUS statusid, (select c901_code_nm from t901_code_lookup where c901_code_id = T6605.C901_STATUS) statusnm, T6605.C6605_CREATED_BY createdby FROM T6605_CRF T6605, T6630_ACTIVITY T6630, T101_PARTY T101 WHERE  T6605.C101_PARTY_ID   = T101.C101_PARTY_ID AND T6605.C6630_ACTIVITY_ID = T6630.C6630_ACTIVITY_ID AND T6605.C6605_VOID_FL = '' AND T6630.C6630_VOID_FL    = '' "+mid+" order by T6605.C6605_PRIORITY_FL desc ";
    }
    else if(inp.actcategory == "105269"|| inp.actcategory == "105267" || inp.actcategory == "105270"){ //Lead SC TS
        if(inp.actcategory == "105269"){
            mid3 = mid3+" , source.C6632_ATTRIBUTE_NM source ";
            mid2 = mid2+" LEFT JOIN (SELECT *  FROM t6632_activity_attribute WHERE C901_ATTRIBUTE_TYPE = '105580' AND c6632_void_fl = '' ) source ON SOURCE.C6630_ACTIVITY_ID IN (T6630.C6630_ACTIVITY_ID) ";
        }
        if(inp.actcategory == "105267"){
            mid3 = mid3+" , result.C6632_ATTRIBUTE_NM result, pic.picurl ";
            mid2 = mid2+" LEFT JOIN (SELECT *  FROM t6632_activity_attribute WHERE C901_ATTRIBUTE_TYPE = '105581' AND c6632_void_fl = '' ) result ON result.C6630_ACTIVITY_ID IN (T6630.C6630_ACTIVITY_ID) LEFT JOIN (SELECT DISTINCT C903_REF_ID REFID, C901_REF_GRP     ||'/'|| C903_REF_ID ||'/'|| C903_FILE_NAME picurl FROM T903_UPLOAD_FILE_LIST WHERE C903_DELETE_FL  = '' AND C901_REF_GRP = '103112' ) pic ON pic.REFID IN (party.surgid) ";
            if(inp.surgeon)
                mid = mid + " and party.surgid = "+inp.surgeon+" ";
        }
        if(inp.actcategory == "105269" || inp.actcategory == "105267"){
            mid3 = mid3 + " ,prdts.pdts products";
            mid2 = mid2 + " left join (SELECT T6632.C6630_ACTIVITY_ID ACTID, GROUP_CONCAT(T6632.C6632_ATTRIBUTE_NM, ', ') pdts FROM t6632_activity_attribute T6632 WHERE T6632.C901_ATTRIBUTE_TYPE = '105582' AND t6632.C6632_VOID_FL = '' GROUP BY T6632.C6630_ACTIVITY_ID ) prdts on T6630.C6630_ACTIVITY_ID = prdts.ACTID ";
        }
        if(inp.actcategory == "105269" || inp.actcategory == "105267")
        query = " SELECT distinct T6630.C6630_ACTIVITY_NM actnm, T6630.C6630_ACTIVITY_ID AS actid, T6630.C6630_FROM_DT AS actstartdate "+mid3+"   ,party.firstnm AS firstnm, party.surgid as partyid,   party.lastnm AS lastnm, party.phynm AS phynm, T6630.C6630_party_nm AS assignee, T6630.C6630_FROM_TM actstarttime, T6630.C6630_TO_TM actendtime FROM t6630_activity t6630  LEFT JOIN (SELECT c6630_activity_id actid, c101_party_id surgid FROM T6631_ACTIVITY_PARTY_ASSOC WHERE c6631_void_fl = '' AND c901_activity_party_role = '105564'   ) actparty ON actparty.actid IN (T6630.C6630_ACTIVITY_ID) LEFT JOIN (SELECT C101_FIRST_NM AS firstnm, C101_LAST_NM AS lastnm, C101_FIRST_NM || ' ' || C101_LAST_NM AS phynm, c101_party_id surgid FROM T101_PARTY WHERE c101_void_fl = '' AND c101_active_fl = '' ) party ON party.surgid IN (actparty.surgid) "+mid2+" WHERE  T6630.C901_ACTIVITY_CATEGORY = '"+inp.actcategory+"' AND T6630.c6630_void_fl = '' "+mid+" order by upper(T6630.C6630_ACTIVITY_NM) ";
    }
    console.log(query);
    
	db.transaction(function (tx, rs) {
        tx.executeSql(query, [],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
            
            if(results.length){
                _.each(results, function(a,i){
                    if(a.actstarttime)
                        a.actstarttime = ampm(a.actstarttime);
                    if(a.actendtime)
                        a.actendtime = ampm(a.actendtime);
                    if((i+1) == results.length)
                        callback(results);
                })
            }
            else
                callback(results);
			
		},
		function(){
			showMsgView("009");});
	});
}

function fnGetMyLead(userPartyId, callback) {
	db.transaction(function (tx, rs) {
        tx.executeSql("SELECT distinct T6630.C6630_ACTIVITY_NM actnm, T6630.C6630_ACTIVITY_ID AS actid, T6630.C6630_FROM_DT AS actstartdate, source.C6632_ATTRIBUTE_NM source,   party.firstnm AS firstnm, party.surgid as partyid,   party.lastnm AS lastnm, party.phynm AS phynm, T6630.C6630_party_nm AS assignee, pdts.pdts products FROM t6630_activity t6630 LEFT JOIN   (SELECT *  FROM t6632_activity_attribute WHERE C901_ATTRIBUTE_TYPE = '105580' AND c6632_void_fl = '' ) source ON SOURCE.C6630_ACTIVITY_ID IN (T6630.C6630_ACTIVITY_ID) LEFT JOIN (SELECT c6630_activity_id actid, c101_party_id surgid FROM T6631_ACTIVITY_PARTY_ASSOC WHERE c6631_void_fl = '' AND c901_activity_party_role = '105564'   ) actparty ON actparty.actid IN (T6630.C6630_ACTIVITY_ID) LEFT JOIN (SELECT C101_FIRST_NM AS firstnm, C101_LAST_NM AS lastnm, C101_FIRST_NM || ' ' || C101_LAST_NM AS phynm, c101_party_id surgid FROM T101_PARTY WHERE c101_void_fl = '' AND c101_active_fl = '' ) party ON party.surgid IN (actparty.surgid) left join (SELECT T6632.C6630_ACTIVITY_ID ACTID, GROUP_CONCAT(T6632.C6632_ATTRIBUTE_NM, ', ') pdts FROM t6632_activity_attribute T6632 WHERE T6632.C901_ATTRIBUTE_TYPE = '105582' AND t6632.C6632_VOID_FL = '' GROUP BY T6632.C6630_ACTIVITY_ID ) pdts on T6630.C6630_ACTIVITY_ID = pdts.ACTID WHERE  T6630.C901_ACTIVITY_CATEGORY = '105269' AND T6630.c6630_void_fl = '' AND T6630.C6630_PARTY_ID = ? order by upper(T6630.C6630_ACTIVITY_NM) ", [userPartyId],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
            if(results.length){
                _.each(results, function(data, i){
                    data["module"] = "lead";
                    data["lead"] = "Y";
                    data["index"] = i + 1;
                    data.gendate = data.actstartdate;
                });
            }
            callback(results);
		},
		function(){
			showMsgView("009");});
	});
}

function fnGetLeadADVP(callback) {
    var userId = localStorage.getItem("userID");
	db.transaction(function (tx, rs) {
        tx.executeSql(" SELECT distinct T6630.C6630_ACTIVITY_NM actnm, T6630.C6630_ACTIVITY_ID AS actid, T6630.C6630_FROM_DT AS actstartdate, source.C6632_ATTRIBUTE_NM source, party.firstnm AS firstnm, party.surgid AS partyid, party.lastnm AS lastnm, party.phynm AS phynm, T6630.C6630_party_nm AS assignee, pdts.pdts products FROM t6630_activity t6630 LEFT JOIN   (SELECT * FROM t6632_activity_attribute WHERE C901_ATTRIBUTE_TYPE = '105580' AND c6632_void_fl = '' ) source ON SOURCE.C6630_ACTIVITY_ID IN (T6630.C6630_ACTIVITY_ID) LEFT JOIN (SELECT c6630_activity_id actid, c101_party_id surgid FROM T6631_ACTIVITY_PARTY_ASSOC WHERE c6631_void_fl = '' AND c901_activity_party_role = '105564' ) actparty ON actparty.actid IN (T6630.C6630_ACTIVITY_ID) LEFT JOIN (SELECT C101_FIRST_NM AS firstnm, C101_LAST_NM        AS lastnm,    C101_FIRST_NM || ' ' || C101_LAST_NM AS phynm, c101_party_id surgid FROM T101_PARTY WHERE c101_void_fl = '' AND c101_active_fl = '' ) party  ON party.surgid IN   (actparty.surgid) LEFT JOIN (SELECT c6632_attribute_value addrid, c6630_activity_id actid FROM t6632_activity_attribute WHERE c6632_void_fl = '' AND c901_attribute_type = '7777') addrid ON addrid.actid IN (T6630.C6630_ACTIVITY_ID) LEFT JOIN t106_address t106 ON c106_address_id IN (addrid.addrid) join (SELECT DISTINCT v700.ad_id adid, v700.ad_name adnm, v700.vp_name , t704.c704_bill_zip_code zip, t704.c704_bill_state state FROM t704_account t704, v700_territory_mapping_detail v700 WHERE t704.c704_account_id = v700.ac_id   AND (v700.ad_id = ? OR v700.vp_id = ? ) ) ADVP ON (t106.c901_state = advp.state OR t106.c106_zip_code = advp.zip) left join (SELECT T6632.C6630_ACTIVITY_ID ACTID, GROUP_CONCAT(T6632.C6632_ATTRIBUTE_NM, ', ') pdts FROM t6632_activity_attribute T6632 WHERE T6632.C901_ATTRIBUTE_TYPE = '105582' AND t6632.C6632_VOID_FL = '' GROUP BY T6632.C6630_ACTIVITY_ID ) pdts on T6630.C6630_ACTIVITY_ID = pdts.ACTID WHERE T6630.C901_ACTIVITY_CATEGORY = '105269' AND T6630.c6630_void_fl = '' order by upper(T6630.C6630_ACTIVITY_NM) ", [userId,userId],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			if(results.length){
                _.each(results, function(data, i){
                    data["module"] = "lead";
                    data["lead"] = "Y";
                    data["index"] = i + 1;
                    data.gendate = data.actstartdate;
                });
            }
            callback(results);
			
		},
		function(){
			showMsgView("009");});
	});
}

function fnGetMySurgeon(userID, partyID, callback) {
    var query = "SELECT DISTINCT T101.C101_PARTY_ID AS partyid, T101.C101_FIRST_NM ||' '|| T101.C101_LAST_NM AS title, t101.c101_middle_initial as midinitial, T101.C101_FIRST_NM AS firstnm, T101.C101_LAST_NM    AS lastnm,  picinfo.picurl AS picurl FROM T101_PARTY T101 LEFT JOIN (SELECT DISTINCT C903_REF_ID REFID, C901_REF_GRP     ||'/'|| C903_REF_ID ||'/'|| C903_FILE_NAME picurl FROM T903_UPLOAD_FILE_LIST WHERE C903_DELETE_FL = '' AND C901_REF_GRP = '103112' ) picinfo ON picinfo.refid in (T101.C101_PARTY_ID) JOIN (SELECT T1012.C101_PARTY_ID FROM T1012_PARTY_OTHER_INFO T1012 WHERE T1012.C1012_DETAIL = "+partyID+" AND T1012.C901_TYPE =  '11509' AND T1012.c1012_void_fl  = '' UNION ALL SELECT t1014.C101_PARTY_ID FROM t1014_party_affiliation t1014 WHERE t1014.c704_account_id IN ( SELECT c704_account_id FROM t704_account WHERE c703_sales_rep_id = "+userID+" and c704_void_fl = '') AND t1014.c901_affiliation_priority = 1 AND t1014.c1014_void_fl = '' ) rep ON T101.C101_PARTY_ID      in (rep.C101_PARTY_ID) WHERE T101.C901_PARTY_TYPE = '7000' AND T101.C101_VOID_FL  = '' and t101.c101_active_fl <> 'N' ORDER BY upper(T101.C101_FIRST_NM ||' '|| T101.C101_LAST_NM) ";
    console.log(query);
	db.transaction(function (tx, rs) {
        tx.executeSql(query, [],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
            var dtl = results;
            var dlen = dtl.length;
            if(results.length){
                _.each(dtl, function(rst, index){

                    var query = " select C106_City city, (select c901_code_nm from t901_code_lookup where c901_code_id = C901_State) state from T106_Address where C101_Party_Id = "+rst.partyid+" and C106_Void_Fl = '' order by c106_primary_fl desc limit 1 ";
                    db.transaction(function (tx, rs) {
                        tx.executeSql(query, [],
                                function(tx,rs) {
                            var i=0, results = [];
                            for(i=0;i<rs.rows.length;i++) {
                                results.push(rs.rows.item(i));
                            }
                            if(results.length){
                                rst.city = results[0].city;
                                rst.state = results[0].state;
                            }
                            else{
                                rst.city = "";
                                rst.state = "";
                            }
                            if(dlen == (index+1)){
                                _.each(dtl, function(rst, index){
                                     var query = " select C1014_ORGANIZATION_NAME account from T1014_PARTY_AFFILIATION where C1014_VOID_FL = '' and C101_PARTY_ID = "+rst.partyid+" order by C901_AFFILIATION_PRIORITY asc limit 1; ";
                                    db.transaction(function (tx, rs) {
                                        tx.executeSql(query, [],
                                                function(tx,rs) {
                                            var i=0, results = [];
                                            for(i=0;i<rs.rows.length;i++) {
                                                results.push(rs.rows.item(i));
                                            }
                                            if(results.length){
                                                rst.account = results[0].account;
                                            }
                                            else{
                                                rst.account = "";
                                            }
                                            if(dlen == (index+1))
                                                callback(dtl);
                                        },
                                        function(){
                                            showMsgView("009");});
                                    });
                                });
                            }
                        },
                        function(){
                            showMsgView("009");});
                    });
                })
            }
            else
                callback([]);
            
            
			
		},
		function(){
			showMsgView("009");});
	});
}

function fnGetDashFS(callback) {
    var id = localStorage.getItem("userID");
	db.transaction(function (tx, rs) {
        tx.executeSql(" SELECT distinct v700.REGION_NAME regnm, v700.REGION_ID regid FROM v700_territory_mapping_detail v700 WHERE v700.AD_ID = ? or v700.VP_ID = ? ORDER BY V700.REGION_NAME ", [id,id],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			if(results.length){
                _.each(results, function(data, i){
                    data["module"] = "fieldsales";
                    data["id"] = data.regid;
                    data["title"] = data.regnm;
                    data["fieldsales"] = "Y";
                    data["index"] = i + 1;
                });
            }
            callback(results);
			
		},
		function(){
			showMsgView("009");});
	});
}

function fnGetSurgeonList(inp, callback) {
    console.log(inp)
    var mid = "", mid2 = "", query = "";
    if(inp.fn != undefined || inp.ln != undefined){
        if(inp.fn!="" && inp.ln!="")
            mid = " and upper(t101.c101_first_nm) like upper('%' ||'"+inp.fn+"' ||'%') and upper(t101.c101_last_nm) like upper('%' ||'"+inp.ln+"' ||'%') ";
        else if(inp.fn!="" && inp.ln=="")
            mid = " and upper(t101.c101_first_nm) like upper('%' ||'"+inp.fn+"' ||'%') ";
        else if(inp.fn=="" && inp.ln!="")
            mid = " and upper(t101.c101_last_nm) like upper('%' ||'"+inp.ln+"' ||'%') ";
    }
    if(inp.keywords!=undefined){
        mid2 = mid2+" join (SELECT C101_PARTY_ID FROM T1012_PARTY_OTHER_INFO WHERE C901_TYPE = '103168' AND C1012_VOID_FL = '' AND UPPER(C1012_DETAIL) IN (UPPER('"+inp.keywords+"'))) KEYWORD on t101.c101_party_id  = KEYWORD.C101_PARTY_ID ";
    }
    if(inp.state!=undefined){
        mid2 = mid2+" join (SELECT T106.C101_PARTY_ID FROM T106_ADDRESS T106 WHERE T106.c106_void_fl = '' AND T106.C901_STATE IN ('"+inp.state+"') GROUP BY T106.C101_PARTY_ID  ) Add_ACT on ADD_ACT.C101_PARTY_ID  = T101.C101_PARTY_ID ";
    }
    if(inp.procedure!=undefined){
        mid2 = mid2+" join (SELECT T6610.C101_PARTY_ID FROM T6610_SURGICAL_ACTIVITY T6610 WHERE T6610.c6610_void_fl = '' AND T6610.C901_PROCEDURE  IN ("+inp.procedure+")   GROUP BY T6610.C101_PARTY_ID ) SURG_ACT on SURG_ACT.C101_PARTY_ID = T101.C101_PARTY_ID ";
    }
    if(inp.systemid!=undefined){
        mid2 = mid2+" join (SELECT T6610.C101_PARTY_ID FROM T6610_SURGICAL_ACTIVITY T6610 WHERE T6610.c6610_void_fl = '' AND T6610.C207_SET_ID IN ("+inp.systemid+") GROUP BY T6610.C101_PARTY_ID) SURGIC_ACT on SURGIC_ACT.C101_PARTY_ID = T101.C101_PARTY_ID ";
    }
    if(inp.specality!=undefined){
        mid2 = mid2+" join (SELECT T1012.C101_PARTY_ID FROM T1012_PARTY_OTHER_INFO T1012 WHERE T1012.C1012_DETAIL IN ("+inp.specality+") ) ACC_ATTR on ACC_ATTR.C101_PARTY_ID = T101.C101_PARTY_ID ";
    }
    if(inp.role!=undefined){
        mid2 = mid2+" join (SELECT DISTINCT T6631.C101_PARTY_ID PARTY_ID FROM T6630_ACTIVITY T6630, T6631_ACTIVITY_PARTY_ASSOC T6631 WHERE T6631.C6630_ACTIVITY_ID =  T6630.C6630_ACTIVITY_ID AND T6631.C901_ACTIVITY_PARTY_ROLE IN ("+inp.role+") AND T6630.C6630_VOID_FL = '' AND T6631.C6631_VOID_FL = '') role  on role.PARTY_ID = T101.C101_PARTY_ID ";
    }
    if(inp.acttyp!=undefined){
        mid2 = mid2+" join ( SELECT DISTINCT T6631.C101_PARTY_ID PARTY_ID FROM T6630_ACTIVITY T6630, T6631_ACTIVITY_PARTY_ASSOC T6631 WHERE T6631.C6630_ACTIVITY_ID                         = T6630.C6630_ACTIVITY_ID AND T6630.C901_ACTIVITY_TYPE IN ("+inp.acttyp+") AND T6630.C6630_VOID_FL = ''   AND T6631.C6631_VOID_FL = '' ) ACT  on ACT.PARTY_ID = T101.C101_PARTY_ID "; 
    }
    if(inp.actstartdate != undefined){
        mid2 = mid2+" join ( SELECT DISTINCT T6631.C101_PARTY_ID PARTY_ID FROM T6630_ACTIVITY T6630, T6631_ACTIVITY_PARTY_ASSOC T6631 WHERE T6631.C6630_ACTIVITY_ID                         = T6630.C6630_ACTIVITY_ID AND T6630.C6630_VOID_FL = ''   AND T6631.C6631_VOID_FL = '' AND substr(T6630.C6630_FROM_DT, 7, 4) || '/' || substr(T6630.C6630_FROM_DT, 1,2) || '/' || substr(T6630.C6630_FROM_DT, 4, 2) >= '"+inp.actstartdate+"' AND substr(T6630.C6630_TO_DT, 7, 4) || '/' || substr(T6630.C6630_TO_DT, 1,2) || '/' || substr(T6630.C6630_TO_DT, 4, 2) <= '"+inp.actenddate+"' ) ACTDT  on ACTDT.PARTY_ID = T101.C101_PARTY_ID "; 
    }
    var query = " select distinct t101.c101_party_id partyid, t101.c101_first_nm firstnm, t101.c101_last_nm lastnm, t101.c101_middle_initial midintial, addr.state, addr.city from t101_party t101 left join (select distinct a.c101_party_id, b.c106_address_id, b.c106_city as city, (select c901_code_nm from t901_code_lookup where c901_code_id = b.c901_state ) as state, b.c106_primary_fl from t101_party a     join t106_address b on a.c101_party_id = b.c101_party_id where (select count(*) from t106_address b2 where b2.c106_address_id >= b.c106_address_id and b2.c101_party_id = b.c101_party_id       and b.c106_void_fl = '') = 1 order by b.c106_primary_fl ) addr on addr.c101_party_id in (t101.c101_party_id) "+mid2+" where t101.c901_party_type = '7000' and t101.c101_active_fl = '' and t101.c101_void_fl = '' "+mid+" ";
    
    console.log(query);
	db.transaction(function (tx, rs) {
        tx.executeSql(query, [],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
            callback(results);
			
		},
		function(){
			showMsgView("009");});
	});
}

function fnGetCriteria(module, callback) {
    var id = localStorage.getItem("userID");
	db.transaction(function (tx, rs) {
        tx.executeSql(" SELECT T1101.C1101_CRITERIA_ID criteriaid, T1101.C1101_CRITERIA_NAME criterianm, T1101.C2201_MODULE_ID criteriatyp, T101.C101_USER_F_NAME || T101.C101_USER_L_NAME usernm FROM T1101_CRITERIA T1101, T101_USER T101 WHERE T101.C101_USER_ID = T1101.C101_USER_ID AND T1101.C1101_VOID_FL = '' AND T1101.C101_USER_ID = ? AND T1101.C2201_MODULE_ID = ? ", [id,module],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
            callback(results);
		},
		function(){
			showMsgView("009");});
	});
}

function fnGetPartyByNPI(id, callback) {
	db.transaction(function (tx, rs) {
        tx.executeSql(" SELECT C101_PARTY_SURGEON_ID partyid FROM T6600_PARTY_SURGEON where C6600_VOID_FL = '' AND C6600_SURGEON_NPI_ID =  ? ", [id],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
            callback(results);
		},
		function(){
			showMsgView("009");});
	});
}

function fnGetFSRepInfo(id, callback) {
	db.transaction(function (tx, rs) {
        tx.executeSql(" SELECT T101.C101_PARTY_ID partyid, T101.C101_FIRST_NM firstnm, T101.C101_LAST_NM lastnm, T703.C703_SALES_REP_NAME partynm, email.email email, phone.phone phone, T101.C101_MIDDLE_INITIAL midinitial, T703.C703_SALES_REP_ID repid,  (select c901_code_nm from t901_code_lookup where c901_code_id = T703.C901_DESIGNATION) title FROM T101_PARTY T101 join T703_SALES_REP T703 on T101.C101_PARTY_ID = T703.C101_PARTY_ID left join (select T107.C101_PARTY_ID, T107.C107_CONTACT_VALUE email from T107_CONTACT T107 where T107.C107_PRIMARY_FL = 'Y' and T107.C901_MODE = '90452' and t107.c107_void_fl = '') email on email.C101_PARTY_ID = T101.C101_PARTY_ID left join (select T107.C101_PARTY_ID, T107.C107_CONTACT_VALUE phone from T107_CONTACT T107 where T107.C107_PRIMARY_FL = 'Y' and T107.C901_MODE = '90450' and t107.c107_void_fl = '') phone on phone.C101_PARTY_ID = T101.C101_PARTY_ID WHERE T703.C703_VOID_FL = '' AND T101.C901_PARTY_TYPE IN ('7005','7006') AND T101.C101_VOID_FL = '' AND T703.C703_SALES_REP_ID =  ? ORDER BY T101.C101_FIRST_NM ", [id],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
            callback(results);
		},
		function(){
			showMsgView("009");});
	});
}

function fnGetFSRepInfoByFilter(inp, callback) {
    console.log(inp)
    var mid1 = "", mid2 = "";
    if(inp.dist!=undefined){
        mid2 = " AND T703.C701_DISTRIBUTOR_ID IN ("+inp.dist+") ";
    }
    if(inp.reg!=undefined){
        mid1 = " join (SELECT DISTINCT V700.TER_ID FROM V700_TERRITORY_MAPPING_DETAIL V700  WHERE V700.REGION_ID IN ("+inp.reg+") and V700.TER_ID <> '' GROUP BY V700.TER_ID ) REG on REG.TER_ID = T703.C702_TERRITORY_ID ";
    }
    var query = " SELECT T101.C101_PARTY_ID partyid, T101.C101_FIRST_NM firstnm, T101.C101_LAST_NM lastnm, email.email email, phone.phone phone, T703.C703_SALES_REP_NAME partynm, T101.C101_MIDDLE_INITIAL midinitial, T703.C703_SALES_REP_ID repid,  (select c901_code_nm from t901_code_lookup where c901_code_id = T703.C901_DESIGNATION) title FROM T101_PARTY T101 join T703_SALES_REP T703 on T101.C101_PARTY_ID = T703.C101_PARTY_ID left join (select T107.C101_PARTY_ID, T107.C107_CONTACT_VALUE email from T107_CONTACT T107 where T107.C107_PRIMARY_FL = 'Y' and T107.C901_MODE = '90452' and t107.c107_void_fl = '') email on email.C101_PARTY_ID = T101.C101_PARTY_ID left join (select T107.C101_PARTY_ID, T107.C107_CONTACT_VALUE phone from T107_CONTACT T107 where T107.C107_PRIMARY_FL = 'Y' and T107.C901_MODE = '90450' and t107.c107_void_fl = '') phone on phone.C101_PARTY_ID = T101.C101_PARTY_ID "+mid1+"  WHERE T703.C703_VOID_FL  = '' AND T101.C901_PARTY_TYPE IN ('7005','7006') AND T101.C101_VOID_FL = '' and UPPER(T101.C101_FIRST_NM) not like ('%INACTIVE%')  "+mid2+" ORDER BY T101.C101_FIRST_NM ";
    console.log(query);
	db.transaction(function (tx, rs) {
        tx.executeSql(query, [],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
            callback(results);
		},
		function(){
			showMsgView("009");});
	});
}

function fnGetFSRepInfoByNM(inp, callback) {
    var mid = "";
    if(inp.fn != undefined || inp.ln != undefined){
        if(inp.fn!="" && inp.ln!="")
            mid = " AND UPPER(T101.C101_FIRST_NM || ' ' || T101.C101_LAST_NM) LIKE UPPER('%' || '"+inp.fn+"' ||'%') AND UPPER(T101.C101_FIRST_NM || ' ' || T101.C101_LAST_NM) LIKE UPPER('%' || '"+inp.ln+"' ||'%') ";
        else if(inp.fn!="" && inp.ln=="")
            mid = " AND UPPER(T101.C101_FIRST_NM) LIKE UPPER('%' || '"+inp.fn+"' ||'%') ";
        else if(inp.fn=="" && inp.ln!="")
            mid = " AND UPPER(T101.C101_LAST_NM) LIKE UPPER('%' || '"+inp.ln+"' ||'%') ";
    }
    
	db.transaction(function (tx, rs) {
        tx.executeSql(" SELECT T101.C101_PARTY_ID partyid, T101.C101_FIRST_NM firstnm, T101.C101_LAST_NM lastnm, email.email email, phone.phone phone, T703.C703_SALES_REP_NAME partynm, T101.C101_MIDDLE_INITIAL midinitial, T703.C703_SALES_REP_ID repid,  (select c901_code_nm from t901_code_lookup where c901_code_id = T703.C901_DESIGNATION) title FROM T101_PARTY T101 join T703_SALES_REP T703 on T101.C101_PARTY_ID = T703.C101_PARTY_ID left join (select T107.C101_PARTY_ID, T107.C107_CONTACT_VALUE email from T107_CONTACT T107 where T107.C107_PRIMARY_FL = 'Y' and T107.C901_MODE = '90452' and t107.c107_void_fl = '') email on email.C101_PARTY_ID = T101.C101_PARTY_ID left join (select T107.C101_PARTY_ID, T107.C107_CONTACT_VALUE phone from T107_CONTACT T107 where T107.C107_PRIMARY_FL = 'Y' and T107.C901_MODE = '90450' and t107.c107_void_fl = '') phone on phone.C101_PARTY_ID = T101.C101_PARTY_ID WHERE T703.C703_VOID_FL = '' AND T101.C901_PARTY_TYPE IN ('7005','7006') AND T101.C101_VOID_FL = '' "+mid+" ORDER BY T101.C101_FIRST_NM  ", [],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
            callback(results);
		},
		function(){
			showMsgView("009");});
	});
}

function fnGetFSDistributor(id, callback) {
    var id = localStorage.getItem("userID");
	db.transaction(function (tx, rs) {
        tx.executeSql(" SELECT DISTINCT v700.D_NAME Name, v700.D_ID ID FROM v700_territory_mapping_detail v700 WHERE (v700.AD_ID = ? OR v700.VP_ID = ?) AND  UPPER(DISTTYPENM) = 'DISTRIBUTOR' AND (UPPER(V700.D_NAME) NOT LIKE ('%INACTIVE%') AND UPPER(V700.D_NAME) NOT LIKE ('%UAD%')) ORDER BY V700.D_NAME  ", [id,id],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
            callback(results);
		},
		function(){
			showMsgView("009");});
	});
}

function fnGetFSRegion(id, callback) {
    var id = localStorage.getItem("userID");
	db.transaction(function (tx, rs) {
        tx.executeSql(" SELECT DISTINCT v700.REGION_NAME regnm, v700.REGION_NAME Name, v700.REGION_ID regid, v700.REGION_ID ID, 'Y' as FSRegion FROM v700_territory_mapping_detail v700 WHERE (v700.AD_ID = ? OR v700.VP_ID = ?) ORDER BY V700.REGION_NAME  ", [id,id],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
            callback(results);
		},
		function(){
			showMsgView("009");});
	});
}

function fnGetCriteriaAttr(cid,module, callback) {
    console.log(cid)
    console.log(module)
    var id = localStorage.getItem("userID");
	db.transaction(function (tx, rs) {
        tx.executeSql(" SELECT T1101.C1101_CRITERIA_ID criteriaid, T1101.C1101_CRITERIA_NAME criterianm, T1101.C2201_MODULE_ID criteriatyp, T101.C101_USER_F_NAME || T101.C101_USER_L_NAME usernm FROM T1101_CRITERIA T1101, T101_USER T101 WHERE T101.C101_USER_ID = T1101.C101_USER_ID AND T1101.C1101_VOID_FL = '' AND T1101.C101_USER_ID = ? AND T1101.C1101_CRITERIA_ID = ? AND T1101.C2201_MODULE_ID = ? ", [id,cid, module],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
            if(results.length){
                db.transaction(function (tx, rs) {
                    tx.executeSql(" SELECT DISTINCT C1101A_CRITERIA_KEY key, C1101A_CRITERIA_VALUE value, C1101A_CRITERIA_ATTRIBUTE_ID attrid FROM T1101A_CRITERIA_ATTRIBUTE WHERE C1101_CRITERIA_ID  = ? and c1101a_void_fl = '' ", [cid],
                            function(tx,rs) {
                        var i=0, res = [];
                        for(i=0;i<rs.rows.length;i++) {
                            res.push(rs.rows.item(i));
                        }
                        
                        console.log(res);
                        if(res.length){
                            results[0].listGmCriteriaAttrVO = [];
                            results[0].listGmCriteriaAttrVO = res;
                        }
                        callback(results);
                    },
                    function(){
                        showMsgView("009");
                        callback([]); });
                });
                        
            }
            
		},
		function(){
			showMsgView("009");});
	});
}

function fnSaveCriteria(id, data, callback) {
	console.log(data)	
    var dispData = $.parseJSON(JSON.stringify(data));
    dispData.listGmCriteriaAttrVO = _.filter(dispData.listGmCriteriaAttrVO, function(data){
        return data.voidfl != "Y";
    });	
    console.log(id);
    var count = 0;
	var user = localStorage.getItem('userID');
    db.transaction(function (tx, rs) {     // In case we save before table sync (for offline mode) 
		tx.executeSql(" SELECT CRITERIA_REQUEST_ID id FROM TBL_CRITERIA WHERE USER = ? AND STATUS = 'Pending Sync' AND CRITERIAID = ? ", [user,id],
				function(tx,rs) {   
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
            db.transaction(function (tx, rs) {
                    tx.executeSql("SELECT MAX(CRITERIA_REQUEST_ID) count FROM TBL_CRITERIA ", [],
                        function(tx,rs) {
                            console.log(rs);
                            var i=0, res = [];
                            for(i=0;i<rs.rows.length;i++) {
                                res.push(rs.rows.item(i));
                            }
                            if(res.length)
                                count = res[0].count;
                            console.log(res);
                            console.log(results);
                            if(results.length){
                                db.transaction(function (tx, rs) {
                                    tx.executeSql("INSERT OR REPLACE INTO TBL_CRITERIA(CRITERIA_REQUEST_ID, USER, DISPLAYDATA, REQUESTDATA, STATUS, CRITERIAID) VALUES (?,?,?,?,?,?) ", [results[0].id, user, JSON.stringify(dispData), JSON.stringify(data), 'Pending Sync', id],
                                        function(tx,rs) {
                                            callback("success",id);
                                        
                                        },
                                    function(e,err){
                                        console.log(JSON.stringify(err));
                                    });
                                });
                            }
                            else{                   // New Detail
                                console.log(count)
                                if(id=="")
                                    var ID = "crt"+((count+1).toString()); // use Criteria_REQUEST_ID
                                else
                                    var ID = id;
                                console.log(ID)
                                db.transaction(function (tx, rs) {
                                        tx.executeSql("INSERT INTO TBL_CRITERIA(USER, DISPLAYDATA, REQUESTDATA, STATUS, CRITERIAID) VALUES (?,?,?,?,?) ", [user, JSON.stringify(dispData), JSON.stringify(data),'Pending Sync', ID],
                                            function(tx,rs) {
                                            if(rs.rows.length>0)
                                                console.log(rs.rows.item(0));
                                            callback("success", ID);
                                            },
                                        function(e,err){
                                            console.log(JSON.stringify(err));
                                        });
                                    });
                            }
                        },
                    function(e,err){
                        console.log(JSON.stringify(err));
                    });
                });
            
            
		},  
		function(){
            showMsgView("009");});
	});
}


// -------------------------------------------SYNC FUNCTIONS------------------------------------------------------------------------
function fnGetUpdatedActivity(callback) {
	var i=0, results = [] ,forAct = 0, forParty = 0, forAttr = 0, forCritera = 0, forCrtAttr= 0, forPrj = 0;
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T6630.C6630_ACTIVITY_ID AS Name, T2001.C901_ACTION FROM T2001_MASTER_DATA_UPDATES T2001, T6630_ACTIVITY T6630 WHERE T2001.C2001_REF_ID = T6630.C6630_ACTIVITY_ID AND T2001.C901_REF_TYPE = '10306170' AND C901_ACTION != '103122'", [],
				function(tx,rs) {
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
				forAct++;
			}

			db.transaction(function (tx, rs) {
				tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '10306170'", [],
						function(tx,rs) {
					var diff = parseInt(rs.rows.item(0).Name) - forAct;
					if(diff > 0)
						results.push({"Name" : diff + " New Activity(s)"});

					db.transaction(function (tx, rs) {
						tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T6631.C6630_ACTIVITY_ID AS Name, T2001.C901_ACTION FROM T2001_MASTER_DATA_UPDATES T2001, T6631_ACTIVITY_PARTY_ASSOC T6631 WHERE T2001.C2001_REF_ID = T6631.C6630_ACTIVITY_ID AND T2001.C901_REF_TYPE = '10306171' AND C901_ACTION != '103122'", [],
								function(tx,rs) {
							for(i=0;i<rs.rows.length;i++) {
								results.push(rs.rows.item(i));
								forParty++;
							}
							db.transaction(function (tx, rs) {
								tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '10306171'", [],
										function(tx,rs) {
									var diff = parseInt(rs.rows.item(0).Name) - forParty;
									if(diff > 0)
										results.push({"Name" : diff + " New Act Party(s)"});
							db.transaction(function (tx, rs) {
										tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T6632.C6630_ACTIVITY_ID AS Name, T2001.C901_ACTION FROM T2001_MASTER_DATA_UPDATES T2001, T6632_ACTIVITY_ATTRIBUTE T6632 WHERE T2001.C2001_REF_ID = T6632.C6630_ACTIVITY_ID AND T2001.C901_REF_TYPE = '10306172' AND C901_ACTION != '103122'", [],
											function(tx,rs) {
												for(i=0;i<rs.rows.length;i++) {
													results.push(rs.rows.item(i));
													forAttr++;
												}
												db.transaction(function (tx, rs) {
													tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '10306172'", [],
														function(tx,rs) {
															var diff = parseInt(rs.rows.item(0).Name) - forAttr;
															if(diff > 0)
																results.push({"Name" : diff + " New Act Attr(s)"});
                                                        db.transaction(function (tx, rs) {
						tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T1101.C1101_CRITERIA_NAME AS Name, T2001.C901_ACTION FROM T2001_MASTER_DATA_UPDATES T2001, T1101_CRITERIA T1101 WHERE T2001.C2001_REF_ID = T1101.C1101_CRITERIA_ID AND T2001.C901_REF_TYPE = '10306210' AND C901_ACTION != '103122'", [],
								function(tx,rs) {
							for(i=0;i<rs.rows.length;i++) {
								results.push(rs.rows.item(i));
								forCritera++;
							}
							db.transaction(function (tx, rs) {
								tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '10306210'", [],
										function(tx,rs) {
									var diff = parseInt(rs.rows.item(0).Name) - forCritera;
									if(diff > 0)
										results.push({"Name" : diff + " New Critera(s)"});
							db.transaction(function (tx, rs) {
										tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T1101A.C1101A_CRITERIA_ATTRIBUTE_ID AS Name, T2001.C901_ACTION FROM T2001_MASTER_DATA_UPDATES T2001, T1101A_CRITERIA_ATTRIBUTE T1101A WHERE T2001.C2001_REF_ID = T1101A.C1101A_CRITERIA_ATTRIBUTE_ID AND T2001.C901_REF_TYPE = '10306209' AND C901_ACTION != '103122'", [],
											function(tx,rs) {
												for(i=0;i<rs.rows.length;i++) {
													results.push(rs.rows.item(i));
													forCrtAttr++;
												}
												db.transaction(function (tx, rs) {
													tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '10306209'", [],
														function(tx,rs) {
															var diff = parseInt(rs.rows.item(0).Name) - forCrtAttr;
															if(diff > 0)
																results.push({"Name" : diff + " New Criteria Attr(s)"});
                                                        db.transaction(function (tx, rs) {
										tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T202.C202_PROJECT_ID AS Name, T2001.C901_ACTION FROM T2001_MASTER_DATA_UPDATES T2001, T202_PROJECT T202 WHERE T2001.C2001_REF_ID = T202.C202_PROJECT_ID AND T2001.C901_REF_TYPE = '103109' AND C901_ACTION != '103122'", [],
											function(tx,rs) {
												for(i=0;i<rs.rows.length;i++) {
													results.push(rs.rows.item(i));
													forPrj++;
												}
												db.transaction(function (tx, rs) {
													tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '103109'", [],
														function(tx,rs) {
															var diff = parseInt(rs.rows.item(0).Name) - forPrj;
															if(diff > 0)
																results.push({"Name" : diff + " New Project(s)"});
                                                        callback(results);
													},null);
												});
										},null);
									});
													},null);
												});
										},null);
									});
								},null);
							});

						},null);
					});
													},null);
												});
										},null);
									});
								},null);
							});

						},null);
					});

				},null);

			});

		},null);
	});
}

function fnGetUpdatedSurgeon(callback) {
	var i=0, results = [], foParty = 0, forAttr = 0, forSurg = 0, foAffl = 0;
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T6600.C6600_SURGEON_NPI_ID AS Name, T2001.C901_ACTION FROM T2001_MASTER_DATA_UPDATES T2001, T6600_PARTY_SURGEON T6600 WHERE T2001.C2001_REF_ID = T6600.C6600_SURGEON_NPI_ID AND T2001.C901_REF_TYPE = '10306161' AND T2001.C901_ACTION != '103122'", [],
				function(tx,rs) {
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
				foParty++;
			}

			db.transaction(function (tx, rs) {
				tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '10306161'", [],
						function(tx,rs) {
					var diff = parseInt(rs.rows.item(0).Name) - foParty;
					if(diff > 0)
						results.push({"Name" : diff + " New Surgeon Party(s)"});

					db.transaction(function (tx, rs) {
						tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T1012.C1012_PARTY_OTHER_INFO_ID AS Name FROM T1012_PARTY_OTHER_INFO T1012, T2001_MASTER_DATA_UPDATES T2001 WHERE T1012.C1012_PARTY_OTHER_INFO_ID = T2001.C2001_REF_ID AND T2001.C901_REF_TYPE = '10306162'", [],
								function(tx,rs) {
							for(i=0;i<rs.rows.length;i++) {
								results.push(rs.rows.item(i));
								forAttr++;
							}
							db.transaction(function (tx, rs) {
								tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '10306162'", [],
										function(tx,rs) {
									var diff = parseInt(rs.rows.item(0).Name) - forAttr;
									if(diff > 0)
										results.push({"Name" : diff + " New Surgeon Attr(s)"});
							db.transaction(function (tx, rs) {
										tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T6610.C6610_SURGICAL_ACTIVITY_ID AS Name FROM T6610_SURGICAL_ACTIVITY T6610, T2001_MASTER_DATA_UPDATES T2001 WHERE T2001.C2001_REF_ID = T6610.C6610_SURGICAL_ACTIVITY_ID AND T2001.C901_REF_TYPE = '10306163'", [],
											function(tx,rs) {
												for(i=0;i<rs.rows.length;i++) {
													results.push(rs.rows.item(i));
													forSurg++;
												}
												db.transaction(function (tx, rs) {
													tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '10306163'", [],
														function(tx,rs) {
															var diff = parseInt(rs.rows.item(0).Name) - forSurg;
															if(diff > 0)
																results.push({"Name" : diff + " New Surgeon Surg-Act(s)"});
                                                        	db.transaction(function (tx, rs) {
                                                                tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T1014.C1014_PARTY_AFFILIATION_ID AS Name, T2001.C901_ACTION FROM T2001_MASTER_DATA_UPDATES T2001, T1014_PARTY_AFFILIATION T1014 WHERE T2001.C2001_REF_ID = T1014.C1014_PARTY_AFFILIATION_ID AND T2001.C901_REF_TYPE = '10306164' AND T2001.C901_ACTION != '103122'", [],
                                                                        function(tx,rs) {
                                                                    for(i=0;i<rs.rows.length;i++) {
                                                                        results.push(rs.rows.item(i));
                                                                        foAffl++;
                                                                    }

                                                                    db.transaction(function (tx, rs) {
                                                                        tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '10306164'", [],
                                                                                function(tx,rs) {
                                                                            var diff = parseInt(rs.rows.item(0).Name) - foAffl;
                                                                            if(diff > 0)
                                                                                results.push({"Name" : diff + " New Surgeon Affl(s)"});
                                                                            callback(results);
                                                                        },null);

                                                                    });

                                                                },null);
                                                            });
													},null);
												});
										},null);
									});
								},null);
							});

						},null);
					});

				},null);

			});

		},null);
	});
}

//fn to delete v700 contents
function fnClearV700(cb){
    db.transaction(function (tx, rs) {
        tx.executeSql(" Delete FROM v700_territory_mapping_detail", [],
				function(tx,rs) {
            cb();
		},
		function(){
            cb();
			showMsgView("009");
        });
	});
}
