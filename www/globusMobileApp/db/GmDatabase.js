/*Open Database. If there isn't one, this command creates a database*/
function fnOpenDB(callback) {
	try{
		// db = window.openDatabase("gmiData", "1.0", "Globus Mobile Database", 10000); //use this code to execute in Chrome Browser
		db = window.sqlitePlugin.openDatabase({name: "gmisqlite",location: 'default'}); 
		
		// db.transaction(dropTables);
		db.transaction(createTables, errorCB, successCB);
		testAlter(0);
		//fnMigrateDB();//migrate from websql to sqlite
		fnUpdatePendingDOList();
        fnCacheSyncDeleteInLocal();
		callback();
	 } catch (err){
		 showAppError(err, "GmDatabase:fnOpenDB(1)");
	 }
}

function fnCacheSyncDeleteInLocal() {
    var that = this;
    //Remove CRM Local Data sync Refid ---- 10306170,10306171,10306172,10306176,10306206,10306210,10306209,103109
    //Remove Surgeon Local Data sync Refid ------- '10306164','10306163','10306162','10306169'
    db.transaction(function (tx, rs) {
        tx.executeSql("delete FROM t9151_Device_sync_dtl where C901_SYNC_TYPE IN ('10306164','10306163','10306162','10306169','10306170','10306171','10306172','10306176','10306206','10306210','10306209','103109')", [],
            function (tx, err) {
                console.log(JSON.stringify(err));
            });
    });
}

/*Create Tables*/
function createTables(tx) {
    //PMT-11763 PDF Sync status will be saved in DO_PDF_INFO
    tx.executeSql("CREATE TABLE IF NOT EXISTS DO_PDF_INFO(DOID NUMBER PRIMARY KEY, PDFNAME VARCHAR2(200), PDFFL VARCHAR2(3) , COUNT NUMBER)", []);
    //CRM - Surgeon Attribute
    tx.executeSql("CREATE TABLE IF NOT EXISTS T1012_PARTY_OTHER_INFO(C1012_PARTY_OTHER_INFO_ID NUMBER PRIMARY KEY, C901_TYPE NUMBER, C1012_DETAIL_NM VARCHAR2(200), C1012_DETAIL VARCHAR2(1000), C1012_VOID_FL VARCHAR2(1), C101_PARTY_ID NUMBER)", []);

    //CRM - Surgeon Party
    tx.executeSql("CREATE TABLE IF NOT EXISTS T6600_PARTY_SURGEON(C6600_SURGEON_NPI_ID VARCHAR2(20), C6600_SURGEON_ASSISTANT_NM VARCHAR2(250), C6600_SURGEON_MANAGER_NM VARCHAR2(250), C6600_VOID_FL VARCHAR2(1), C101_PARTY_SURGEON_ID NUMBER PRIMARY KEY)", []);

    //CRM - Hospital Affiliation
    tx.executeSql("CREATE TABLE IF NOT EXISTS T1014_PARTY_AFFILIATION(C1014_PARTY_AFFILIATION_ID NUMBER PRIMARY KEY, C101_PARTY_ID NUMBER, C704_ACCOUNT_ID VARCHAR2(20), C1014_ORGANIZATION_NAME VARCHAR2(255), C901_AFFILIATION_PRIORITY NUMBER, C1014_AFFILIATION_PERCENT NUMBER(4), C1014_CITY_NM VARCHAR2(100), C901_STATE_NM VARCHAR2(200), C901_STATE NUMBER, C1014_VOID_FL VARCHAR2(1), C1014_COMMENT VARCHAR2(10))", []);

    //CRM - Surgical Activity 
    tx.executeSql("CREATE TABLE IF NOT EXISTS T6610_SURGICAL_ACTIVITY(C901_PROCEDURE NUMBER , C901_PROCEDURE_NM VARCHAR2(200), C6610_CASES NUMBER(4), C207_SET_ID VARCHAR2(20), C6610_NON_GLOBUS_SYSTEM VARCHAR2(255), C6610_COMPANY_NM VARCHAR2(255), C6610_SURGICAL_ACTIVITY_ID NUMBER PRIMARY KEY, C6610_VOID_FL VARCHAR2(1), C101_PARTY_ID NUMBER, C6610_COMMENT VARCHAR2(10))", []);

    //CRM - Surgical Rep / AD / VP info 
    tx.executeSql("CREATE TABLE IF NOT EXISTS v700_territory_mapping_detail(AD_ID NUMBER , AD_NAME VARCHAR2(4000), VP_NAME VARCHAR2(4000), REP_NAME VARCHAR2(72), REP_ID NUMBER, VP_ID NUMBER, TER_ID NUMBER, REGION_ID NUMBER, REGION_NAME VARCHAR2(100), TER_NAME VARCHAR2(100), D_NAME VARCHAR2(255), D_ID VARCHAR2(20), AC_ID VARCHAR2(20),DISTTYPENM VARCHAR2(100))", []);

    //CRM - Surgeon Comments
    tx.executeSql("CREATE TABLE IF NOT EXISTS t902_log(c902_log_id NUMBER NOT NULL PRIMARY KEY, c902_comments VARCHAR2(4000), c902_comment_nm VARCHAR2(1000), C902_REF_ID VARCHAR2(20), c902_type NUMBER, c902_void_fl VARCHAR2(1))", []);

    //CRM - Activity  Info
    tx.executeSql("CREATE TABLE IF NOT EXISTS T6630_ACTIVITY(C6630_ACTIVITY_ID VARCHAR2(35) NOT NULL PRIMARY KEY, C901_ACTIVITY_TYPE NUMBER, C901_ACTIVITY_TYPE_NM VARCHAR2(100), C6630_ACTIVITY_NM VARCHAR2(100), C6630_FROM_DT DATE, C6630_TO_DT DATE,  C6630_VOID_FL VARCHAR2(1), c901_activity_status NUMBER, C6630_PARTY_ID NUMBER, C6630_PARTY_NM, C901_ACTIVITY_CATEGORY NUMBER, c901_activity_category_nm VARCHAR2(100), C6630_TO_TM TIMESTAMP(6), C6630_FROM_TM TIMESTAMP(6), C6630_CREATED_BY VARCHAR2(10))", []);

    //CRM - Activity  Party Info
    tx.executeSql("CREATE TABLE IF NOT EXISTS t6631_activity_party_assoc(c6631_activity_party_id NUMBER NOT NULL PRIMARY KEY, c6630_activity_id VARCHAR2(35), C901_ACTIVITY_PARTY_ROLE NUMBER, c901_activity_party_role_nm VARCHAR2(100), c101_party_id NUMBER, c6631_void_fl VARCHAR2(1), C901_PARTY_STATUS NUMBER, C901_PARTY_STATUS_NM VARCHAR2(100))", []);

    //CRM - Activity  Attribute Info
    tx.executeSql("CREATE TABLE IF NOT EXISTS t6632_activity_attribute(c6632_activity_attribute_id NUMBER NOT NULL PRIMARY KEY, c6630_activity_id VARCHAR2(35), C6632_ATTRIBUTE_VALUE VARCHAR2(1000), C6632_ATTRIBUTE_NM VARCHAR2(1000), C901_ATTRIBUTE_TYPE NUMBER, C6632_VOID_FL VARCHAR2(1))", []);

    //CRM - Project Info
    tx.executeSql("CREATE TABLE IF NOT EXISTS T202_PROJECT(C202_PROJECT_ID VARCHAR2(20) NOT NULL PRIMARY KEY, C202_PROJECT_NM VARCHAR2(72) NOT NULL, C202_VOID_FL CHAR(1))", []);

    //CRM - CRF Info
    tx.executeSql("CREATE TABLE IF NOT EXISTS T6605_CRF(C6605_CRF_ID NUMBER NOT NULL PRIMARY KEY, C101_PARTY_ID NUMBER, C6630_ACTIVITY_ID VARCHAR2(35), C901_REQUEST_TYPE NUMBER, C6605_SERVICE_TYPE VARCHAR2(50), C6605_REASON VARCHAR2(50), C6605_SERVICE_DESCRIPTION VARCHAR2(4000), C6605_HCP_QUALIFICATION VARCHAR2(4000), C6605_PREP_TIME NUMBER, C6605_WORK_HOURS NUMBER, C6605_TIME_FL VARCHAR2(1), C6605_AGREEMENT_FL VARCHAR2(1), C6605_TRAVEL_FROM VARCHAR2(50), C6605_TRAVEL_TO VARCHAR2(50), C6605_TRAVEL_TIME NUMBER, C6605_TRAVEL_MILES NUMBER, C6605_HOUR_RATE NUMBER, C6605_VOID_FL VARCHAR2(1), C6605_CREATED_BY VARCHAR2(10), C901_STATUS NUMBER, C6605_REQUEST_DATE DATE, C6605_HONORARIA_STATUS NUMBER, C6605_HONORARIA_WORK_HRS NUMBER, C6605_HONORARIA_PREP_HRS NUMBER, C6605_HONORARIA_COMMENT VARCHAR2(1000), C6605_PRIORITY_FL VARCHAR2(10))", []);

    //CRM - CRF  Approver Info
    tx.executeSql("CREATE TABLE IF NOT EXISTS T6605A_CRF_APPROVAL(C6605A_CRF_APPROVAL_ID NUMBER PRIMARY KEY, C6605_CRF_ID NUMBER, C101_PARTY_ID NUMBER, C901_STATUS NUMBER, C6605A_COMMENTS VARCHAR2(1000))", []);

    //Save Surgeon Data
    tx.executeSql("CREATE TABLE IF NOT EXISTS TBL_SURGEON(SURGEON_REQUEST_ID INTEGER PRIMARY KEY AUTOINCREMENT, USER VARCHAR2(50), REQUESTDATA VARCHAR2(1000), DISPLAYDATA VARCHAR2(1000), STATUS VARCHAR2(25), PARTYID NUMBER)", []);

    //Save Activity Data
    tx.executeSql("CREATE TABLE IF NOT EXISTS TBL_ACTIVITY(ACTIVITY_REQUEST_ID INTEGER PRIMARY KEY AUTOINCREMENT, USER VARCHAR2(50), REQUESTDATA VARCHAR2(1000), DISPLAYDATA VARCHAR2(1000), STATUS VARCHAR2(25), ACTID NUMBER)", []);

    //Save CRF Data
    tx.executeSql("CREATE TABLE IF NOT EXISTS TBL_CRF(CRF_REQUEST_ID INTEGER PRIMARY KEY AUTOINCREMENT, USER VARCHAR2(50), REQUESTDATA VARCHAR2(1000), DISPLAYDATA VARCHAR2(1000), STATUS VARCHAR2(25), CRFID NUMBER)", []);

    //Save Criteria Data
    tx.executeSql("CREATE TABLE IF NOT EXISTS TBL_CRITERIA(CRITERIA_REQUEST_ID INTEGER PRIMARY KEY AUTOINCREMENT, USER VARCHAR2(50), REQUESTDATA VARCHAR2(1000), DISPLAYDATA VARCHAR2(1000), STATUS VARCHAR2(25), CRITERIAID NUMBER)", []);

    //Save CRF Approver Data
    tx.executeSql("CREATE TABLE IF NOT EXISTS TBL_CRF_APPR(CRF_APPR_REQUEST_ID INTEGER PRIMARY KEY AUTOINCREMENT, USER VARCHAR2(50), REQUESTDATA VARCHAR2(1000), DISPLAYDATA VARCHAR2(1000), STATUS VARCHAR2(25), CRF_APPRID NUMBER)", []);

    //Save CRM Criteria Data
    tx.executeSql("CREATE TABLE IF NOT EXISTS T1101_CRITERIA(C1101_CRITERIA_ID VARCHAR2(20) PRIMARY KEY , C1101_CRITERIA_NAME VARCHAR2(150), C2201_MODULE_ID VARCHAR2(20), C101_USER_ID NUMBER, C1101_VOID_FL VARCHAR2(20))", []);

    //Save CRM Criteria Attr Data
    tx.executeSql("CREATE TABLE IF NOT EXISTS T1101A_CRITERIA_ATTRIBUTE(C1101A_CRITERIA_ATTRIBUTE_ID NUMBER(20) PRIMARY KEY, C1101_CRITERIA_ID VARCHAR2(20), C1101A_CRITERIA_KEY VARCHAR2(20), C1101A_CRITERIA_VALUE VARCHAR2(250), C1101A_VOID_FL VARCHAR2(20))", []);

    //Sync and Sync Detail
    tx.executeSql("CREATE TABLE IF NOT EXISTS T9151_DEVICE_SYNC_DTL (C901_SYNC_TYPE VARCHAR2 (20) PRIMARY KEY, C901_SYNC_STATUS VARCHAR2(20), C9151_LAST_SYNC_DATE  DATE, C9151_SYNC_COUNT VARCHAR2(20), C9151_SERVER_COUNT VARCHAR2(20), SYNC_GRP VARCHAR2(50),SYNC_SEQ NUMBER )", []);

    //Group and Group Detail
    tx.executeSql("CREATE TABLE IF NOT EXISTS T4010_GROUP (C4010_GROUP_ID VARCHAR2 (20) PRIMARY KEY, C4010_GROUP_NM VARCHAR2(100), C4010_GROUP_DESC VARCHAR2(255), C4010_GROUP_DTL_DESC VARCHAR2(255), C4010_INACTIVE_FL VARCHAR2(1), C4010_VOID_FL VARCHAR2(1), C4010_CREATED_BY VARCHAR2(10), C4010_CREATED_DATE DATE, C4010_LAST_UPDATED_BY VARCHAR2(10), C4010_LAST_UPDATED_DATE DATE, C901_TYPE NUMBER, C207_SET_ID VARCHAR2(20), C901_GROUP_TYPE NUMBER, C4010_VISIBLE_AC_FL VARCHAR2(1), C4010_PUBLISH_FL VARCHAR2(1), C901_PRICED_TYPE NUMBER, C4010_PRINCIPAL_GRP_FL VARCHAR2(1), C4010_GROUP_PUBLISH_DATE DATE, C101_GROUP_PUBLISH_BY NUMBER, C101_GROUP_OWNER NUMBER)", []);
    tx.executeSql("CREATE TABLE IF NOT EXISTS T4011_GROUP_DETAIL(C4010_GROUP_ID NUMBER, C205_PART_NUMBER_ID VARCHAR2(20), C901_PART_PRICING_TYPE NUMBER, PRIMARY KEY(C4010_GROUP_ID,C205_PART_NUMBER_ID))", []);

    //Set, Set Detail and System Attribute
    tx.executeSql("CREATE TABLE IF NOT EXISTS T207_SET_MASTER (C207_SET_ID VARCHAR2 (20) PRIMARY KEY, C207_SET_NM VARCHAR2 (100), C207_SET_DESC VARCHAR2 (255), C207_SET_DETAIL VARCHAR2 (500), C207_CATEGORY VARCHAR2 (20), C207_SET_SALES_SYSTEM_ID VARCHAR2 (20),C207_TYPE VARCHAR2 (20),C901_HIERARCHY VARCHAR2 (20), C901_STATUS_ID NUMBER, C207_VOID_FL VARCHAR(1))", []);
    tx.executeSql("CREATE TABLE IF NOT EXISTS T208_SET_DETAILS(C207_SET_ID VARCHAR2 (20),C205_PART_NUMBER_ID  VARCHAR2 (20),C208_SET_QTY NUMBER (15, 4), C208_INSET_FL CHAR (1), C208_VOID_FL VARCHAR(1), PRIMARY KEY(C207_SET_ID,C205_PART_NUMBER_ID))", []);
    tx.executeSql("CREATE TABLE IF NOT EXISTS T207C_SET_ATTRIBUTE(C207_SET_ID VARCHAR2(100), C901_ATTRIBUTE_TYPE VARCHAR2(100), C207C_ATTRIBUTE_VALUE VARCHAR2(255), C207C_VOID_FL VARCHAR2(1), PRIMARY KEY(C207_SET_ID, C901_ATTRIBUTE_TYPE, C207C_ATTRIBUTE_VALUE));", []);

    //New Set Attribute
    tx.executeSql("CREATE TABLE IF NOT EXISTS TBL_SET_ATTRIBUTE(C207_SET_ID VARCHAR2(100), C901_ATTRIBUTE_TYPE VARCHAR2(100), C207C_ATTRIBUTE_VALUE VARCHAR2(255), C207C_VOID_FL VARCHAR2(1), PRIMARY KEY(C207_SET_ID, C901_ATTRIBUTE_TYPE));", []);
    
     //Tag Sync
    tx.executeSql("CREATE TABLE IF NOT EXISTS T5010_TAG_DTL(C5010_TAG_ID VARCHAR2(40) PRIMARY KEY, C207_Set_Id VARCHAR2(20),C901_Status NUMBER,C5010_Void_Fl VARCHAR2(1));", []);

    //Part Number and Part Attribute
    tx.executeSql("CREATE TABLE IF NOT EXISTS T205_PART_NUMBER (C205_PART_NUMBER_ID VARCHAR2(20) PRIMARY KEY, C205_PART_NUM_DESC VARCHAR2(255), C205_PART_NUM_DETAIL VARCHAR2(255), C205_PRODUCT_FAMILY VARCHAR2(72), C205_PRODUCT_CLASS VARCHAR2(50),C205_PRODUCT_MATERIAL VARCHAR2(50), C901_STATUS_ID NUMBER, C205_ACTIVE_FL CHAR(1), C207_SET_SALES_SYSTEM_ID VARCHAR2 (20),C2060_DI_NUMBER VARCHAR2(20),UDI_Pattern VARCHAR2(50))", []);
    tx.executeSql("CREATE TABLE IF NOT EXISTS T205D_PART_ATTRIBUTE(C2051_PART_ATTRIBUTE_ID NUMBER PRIMARY KEY, C205_PART_NUMBER_ID VARCHAR2(20), C901_ATTRIBUTE_TYPE NUMBER, C205D_ATTRIBUTE_VALUE VARCHAR2(4000), C205D_VOID_FL VARCHAR2(1));", []);
    tx.executeSql("CREATE TABLE IF NOT EXISTS T901C_RFS_COMPANY_MAPPING(C901C_RFS_COMP_MAP_ID NUMBER NOT NULL PRIMARY KEY, C901_RFS_ID NUMBER, C1900_COMPANY_ID NUMBER, C901C_VOID_FL VARCHAR2(1));", []);

    //Favorites and Recents
    tx.executeSql("CREATE TABLE IF NOT EXISTS TBL_RECENT(REC_ID INTEGER PRIMARY KEY AUTOINCREMENT, DETAIL_ID VARCHAR(50) UNIQUE, TYPE VARCHAR(20), TIME DATE)", []);
    tx.executeSql("CREATE TABLE IF NOT EXISTS TBL_FAVORITE(SYSTEM_ID VARCHAR2(100) NOT NULL PRIMARY KEY, TYPE VARCHAR(20), CREATED_DATE DATE);", []);

    //Local Initial build
    tx.executeSql("CREATE TABLE IF NOT EXISTS TBL_MSG_STATUS(MSGID VARCHAR(20),LANG VARCHAR(20), MESSAGE VARCHAR(20), REASON VARCHAR(20), ACTION VARCHAR(20))", []);
    tx.executeSql("CREATE TABLE IF NOT EXISTS TBL_LOCALE(LOCALE_ID NUMBER PRIMARY KEY, LOCALE_NM VARCHAR2 (100) NOT NULL, LOCALE_CODE VARCHAR2 (10), LOCALE_FLAG VARCHAR2 (10), ACTIVE_FLAG NUMBER);", []);

    //Updates and Last SYnc Info
    tx.executeSql("CREATE TABLE IF NOT EXISTS T2001_MASTER_DATA_UPDATES(C2001_REF_ID VARCHAR2(100), C901_REF_TYPE VARCHAR2(50), C901_ACTION VARCHAR2(50), C901_LANGUAGE_ID VARCHAR2(50), C2001_REF_UPDATED_DT DATE, C901_APP_ID VARCHAR2(100), C2001_LAST_UPDATED_DT DATE, C2001_LAST_UPDATED_BY VARCHAR2(75), C2001_VOID_FL VARCHAR(1), PRIMARY KEY(C2001_REF_ID,C901_REF_TYPE));", []);
    tx.executeSql("CREATE TABLE IF NOT EXISTS TBL_LASTSYNC(SYNCNAME VARCHAR2(20) PRIMARY KEY, LASTSYNC DATE);", []);

    //Local Contacts
    tx.executeSql("CREATE TABLE IF NOT EXISTS TBL_PARTY(PARTY_ID INTEGER PRIMARY KEY AUTOINCREMENT, FIRST_NM VARCHAR2(255), LAST_NM VARCHAR2(255), C101_MIDDLE_INITIAL VARCHAR2(20), PARTY_TYPE VARCHAR2(20), NPI VARCHAR2(20), VOID_FL VARCHAR2(1));", []);
    tx.executeSql("CREATE TABLE IF NOT EXISTS TBL_CONTACT(CONTACT_ID INTEGER PRIMARY KEY AUTOINCREMENT, PARTY_ID VARCHAR2(20), CONTACT_VALUE VARCHAR2(255), PRIMARY_FL VARCHAR2(20), CONTACT_TYPE VARCHAR2(20),FAVORITE VARCHAR2(20), VOID_FL VARCHAR2(1));", []);

    //Server Contacts
    tx.executeSql("CREATE TABLE IF NOT EXISTS T101_PARTY(C101_PARTY_ID NUMBER PRIMARY KEY, C101_FIRST_NM VARCHAR2(100), C101_LAST_NM VARCHAR2(100), C901_PARTY_TYPE VARCHAR2(50), C101_MIDDLE_INITIAL VARCHAR2(1), C101_ACTIVE_FL VARCHAR2(1),C101_PARTY_NM VARCHAR2(100), C101_VOID_FL VARCHAR2(1));", []);
    tx.executeSql("CREATE TABLE IF NOT EXISTS T107_CONTACT(C101_PARTY_ID NUMBER, C107_CONTACT_ID VARCHAR2(50) PRIMARY KEY, C107_CONTACT_VALUE VARCHAR2(50), C107_PRIMARY_FL VARCHAR2(1), C107_CONTACT_TYPE VARCHAR2(50), C107_INACTIVE_FL VARCHAR2(1), C107_VOID_FL VARCHAR2(1),C901_MODE NUMBER);", []);

    //File and Keyword Ref
    tx.executeSql("CREATE TABLE IF NOT EXISTS T903_UPLOAD_FILE_LIST(C903_UPLOAD_FILE_LIST NUMBER PRIMARY KEY, C903_FILE_NAME VARCHAR2 (100) NOT NULL, C903_REF_ID VARCHAR2 (100), C901_TYPE VARCHAR2(50), C903_FILE_SIZE VARCHAR2 (50), C903_FILE_SEQ_NO VARCHAR2 (50), C901_FILE_TITLE VARCHAR2 (100), C901_REF_GRP  VARCHAR2 (50), C901_REF_TYPE VARCHAR2(50), C903_SHARE_FLAG VARCHAR2(1), C903_LAST_UPDATED_DATE DATE, C903_LAST_UPDATED_BY VARCHAR2 (20), C903_DELETE_FL VARCHAR2(1), C903_FILE_PATH VARCHAR2(100), C903_SHARE_RULE VARCHAR2(1), C205_PART_NUMBER_ID VARCHAR2(20));", []);
    tx.executeSql("CREATE TABLE IF NOT EXISTS T2050_KEYWORD_REF(C2050_KEYWORD_REF_ID VARCHAR2(50) PRIMARY KEY, C2050_KEYWORD_REF_NAME VARCHAR2(255), C2050_REF_ID VARCHAR2(50), C901_REF_TYPE VARCHAR2(50), C2050_VOID_FL VARCHAR2(1), C2050_LAST_UPDATED_BY VARCHAR2(50), C2050_LAST_UPDATED_DT VARCHAR2(50));", []);

    //Lookup Masters
    tx.executeSql("CREATE TABLE IF NOT EXISTS T901_CODE_LOOKUP(C901_CODE_ID NUMBER NOT NULL PRIMARY KEY, C901_CODE_NM VARCHAR2 (100) NOT NULL, C901_CODE_GRP VARCHAR2 (6), C901_ACTIVE_FL NUMBER (1, 0), C901_CODE_SEQ_NO NUMBER, C902_CODE_NM_ALT VARCHAR2 (20), C901_CONTROL_TYPE VARCHAR2 (6), C901_CREATED_DATE DATE, C901_CREATED_BY  VARCHAR2 (20), C901_LAST_UPDATED_DATE DATE, C901_LAST_UPDATED_BY VARCHAR2 (20), C901_VOID_FL VARCHAR2 (1), C901_LOCK_FL VARCHAR2 (1))", []);
    tx.executeSql("CREATE TABLE IF NOT EXISTS T906_RULES(C906_RULE_SEQ_ID NUMBER PRIMARY KEY, C906_RULE_ID VARCHAR2(50), C906_RULE_GRP_ID VARCHAR2(50), C906_RULE_DESC VARCHAR2(255), C906_RULE_VALUE VARCHAR2(50), C906_LAST_UPDATED_BY VARCHAR2(50), C906_LAST_UPDATED_DATE DATE, C906_VOID_FL VARCHAR2(1),C1900_COMPANY_ID NUMBER);", []);
    tx.executeSql("CREATE TABLE IF NOT EXISTS T901D_CODE_LKP_COMPANY_EXCLN(C901D_LOOKUP_EXC_ID NUMBER NOT NULL PRIMARY KEY,C901_CODE_ID NUMBER,C1900_COMPANY_ID NUMBER ,C901D_VOID_FL VARCHAR2(1))", []);
    tx.executeSql("CREATE TABLE IF NOT EXISTS T901A_LOOKUP_ACCESS(C901A_CODE_ACCESS_ID NUMBER NOT NULL PRIMARY KEY,C901_ACCESS_CODE_ID NUMBER,C901_CODE_ID NUMBER,C1900_COMPANY_ID NUMBER)", []);

    //Share Related
    tx.executeSql("CREATE TABLE IF NOT EXISTS TBL_SHARE_LIST(FILE_LIST_ID NUMBER PRIMARY KEY, CREATED_DATE DATE);", []);
    tx.executeSql("CREATE TABLE IF NOT EXISTS TBL_SHARE(SHARE_ID INTEGER PRIMARY KEY AUTOINCREMENT, C1016_SHARE_EXT_REF_ID VARCHAR2(20), JSON VARCHAR2(400), C1016_SHARE_ID VARCHAR2(50), STATUS VARCHAR2(50), USER VARCHAR2(50), CREATED_DATE DATE);", []);

    //Dashboard configuration
    tx.executeSql("CREATE TABLE IF NOT EXISTS TBL_DASHBOARD_SEQ(SEQ_ID INTEGER PRIMARY KEY AUTOINCREMENT, GROUP_ID VARCHAR2(50), GROUP_NAME VARCHAR2(50), COLUMN_NAME VARCHAR2(50), SEQ_NO NUMBER(2), DEFAULT_SEQ_NO NUMBER(2), SHORT_NM VARCHAR2(2));", []);

    //Account
    tx.executeSql("CREATE TABLE IF NOT EXISTS T704_ACCOUNT(C704_ACCOUNT_ID VARCHAR2(20) PRIMARY KEY, C704_ACCOUNT_NM VARCHAR2(255), C703_SALES_REP_ID NUMBER, C740_GPO_CATALOG_ID VARCHAR2(20), C704_BILL_NAME VARCHAR2(100), C704_BILL_ADD1 VARCHAR2(100), C704_BILL_ADD2 VARCHAR2(100), C704_BILL_CITY VARCHAR2(100), C704_BILL_STATE NUMBER, C704_BILL_COUNTRY NUMBER, C704_BILL_ZIP_CODE VARCHAR2(100), C704_SHIP_NAME VARCHAR2(100), C704_SHIP_ADD1 VARCHAR2(100), C704_SHIP_ADD2 VARCHAR2(100), C704_SHIP_CITY VARCHAR2(100), C704_SHIP_STATE VARCHAR2(100), C704_SHIP_COUNTRY NUMBER, C704_SHIP_ZIP_CODE VARCHAR2(10), C704_ACTIVE_FL VARCHAR(1), C704_VOID_FL VARCHAR(1), C901_COMPANY_ID NUMBER, C901_EXT_COUNTRY_ID NUMBER, SYNCED VARCHAR2(1),C101_DEALER_ID  NUMBER)", []);
    tx.executeSql("CREATE TABLE IF NOT EXISTS T740_GPO_ACCOUNT_MAPPING(C740_GPO_ACCOUNT_MAP_ID NUMBER PRIMARY KEY, C704_ACCOUNT_ID VARCHAR2(20), C101_PARTY_ID NUMBER, C740_GPO_CATALOG_ID VARCHAR2(20), C740_VOID_FL VARCHAR2(1))", []);

    //SalesRep
    tx.executeSql("CREATE TABLE IF NOT EXISTS T703_SALES_REP(C703_SALES_REP_ID NUMBER PRIMARY KEY, C703_SALES_REP_NAME VARCHAR2(72), C701_DISTRIBUTOR_ID VARCHAR2(20), C101_PARTY_ID NUMBER, C703_ACTIVE_FL VARCHAR(1), C702_TERRITORY_ID NUMBER, C703_VOID_FL CHAR(1), C901_EXT_COUNTRY_ID NUMBER, C901_DESIGNATION NUMBER);", []);

    //Associate SalesRep
    tx.executeSql("CREATE TABLE IF NOT EXISTS T704a_ACCOUNT_REP_MAPPING(C704A_ACCOUNT_REP_MAP_ID NUMBER PRIMARY KEY, C704_ACCOUNT_ID VARCHAR2(20), C703_SALES_REP_ID NUMBER, C704A_ACTIVE_FL VARCHAR2(1), C704A_VOID_FL VARCHAR2(1));", []);

    //Price
    tx.executeSql("CREATE TABLE IF NOT EXISTS T705_ACCOUNT_PRICING(C705_ACCOUNT_PRICING_ID NUMBER PRIMARY KEY, C704_ACCOUNT_ID VARCHAR2(20), C205_PART_NUMBER_ID VARCHAR2(20), C7051_PRICE NUMBER, C101_GPO_ID NUMBER, C4010_GROUP_ID VARCHAR2(20), C901_GROUP_TYPE VARCHAR2(20), C7051_VOID_FL VARCHAR2(1), C7051_ACTIVE_FL VARCHAR2(1));", []);

    //Save Booked DO
    tx.executeSql("CREATE TABLE IF NOT EXISTS TBL_DO(DOID VARCHAR2(50) PRIMARY KEY, ACCOUNT VARCHAR2(250), USER VARCHAR2(50), JSON VARCHAR2(1000), STATUS VARCHAR2(25), JSONPARTS VARCHAR2(1000), REASON VARCHAR2(20), COVERINGREPFL VARCHAR2(20), SIGNATURE VARCHAR2(5000), ADDITIONAL_INFO VARCHAR2(5000), OTHER_INFO VARCHAR2(5000),OLDDOID VARCHAR2(5000));", []);

    //Address
    tx.executeSql("CREATE TABLE IF NOT EXISTS T106_ADDRESS(C106_ADDRESS_ID NUMBER PRIMARY KEY, C901_ADDRESS_TYPE NUMBER, C101_PARTY_ID NUMBER, C106_ADD1 VARCHAR2(255), C106_ADD2 VARCHAR2(255), C106_CITY VARCHAR2(255), C901_STATE NUMBER, C901_COUNTRY NUMBER, C106_ZIP_CODE VARCHAR2(10), C106_SEQ_NO NUMBER(4), C106_INACTIVE_FL VARCHAR2(1), C106_PRIMARY_FL VARCHAR2(1), C106_VOID_FL VARCHAR2(1));", []);

    //Distributor
    tx.executeSql("CREATE TABLE IF NOT EXISTS T701_DISTRIBUTOR(C701_DISTRIBUTOR_ID VARCHAR2(20) PRIMARY KEY, C701_DISTRIBUTOR_NAME VARCHAR2(255), C701_REGION NUMBER, C701_ACTIVE_FL VARCHAR2(1), C701_BILL_ADD1 VARCHAR2(100), C701_BILL_NAME VARCHAR2(100), C701_BILL_ADD2 VARCHAR2(100), C701_BILL_STATE NUMBER, C701_BILL_COUNTRY NUMBER, C701_BILL_CITY VARCHAR2(100), C701_BILL_ZIP_CODE VARCHAR2(10), C701_SHIP_NAME VARCHAR2(100), C701_SHIP_ADD1 VARCHAR2(100), C701_SHIP_ADD2 VARCHAR2(100), C701_SHIP_CITY NUMBER, C701_SHIP_STATE NUMBER, C701_SHIP_COUNTRY NUMBER, C701_SHIP_ZIP_CODE VARCHAR2(10), C901_EXT_COUNTRY_ID NUMBER, C701_VOID_FL VARCHAR(1));", []);

    //Server Case Tables
    tx.executeSql("CREATE TABLE IF NOT EXISTS T7100_CASE_INFORMATION(C7100_CASE_INFO_ID INTEGER PRIMARY KEY AUTOINCREMENT, C7100_CASE_ID VARCHAR2(20), C7100_SURGERY_DATE DATE, C7100_SURGERY_TIME TIME, C701_DISTRIBUTOR_ID VARCHAR2(20), C703_SALES_REP_ID NUMBER, C704_ACCOUNT_ID VARCHAR2(20), C4010_VOID_FL VARCHAR2(1), C7100_PERSONAL_NOTES VARCHAR(255), C901_CASE_STATUS NUMBER, C7100_CREATED_BY VARCHAR2(10), C7100_CREATED_DATE DATE, C7100_LAST_UPDATED_BY VARCHAR2(10), C7100_LAST_UPDATED_DATE DATE, C7100_PARENT_CASE_INFO_ID NUMBER, C901_TYPE VARCHAR2(20));", []);
    tx.executeSql("CREATE TABLE IF NOT EXISTS T7102_CASE_ATTRIBUTE(C7102_CASE_ATTRIBUTE_ID NUMBER PRIMARY KEY, C7100_CASE_INFO_ID NUMBER, C901_ATTRIBUTE_TYPE VARCHAR2(100), C7102_ATTRIBUTE_VALUE VARCHAR2(100), C7102_VOID_FL CHAR(1));", []);

    //Local Case Tables
    tx.executeSql("CREATE TABLE IF NOT EXISTS TBL_CASE_INFORMATION(COL_CASE_INFO_ID INTEGER PRIMARY KEY AUTOINCREMENT, COL_CASE_ID VARCHAR2(20), COL_SURGERY_DATE DATE, COL_SURGERY_TIME TIME, C701_DISTRIBUTOR_ID VARCHAR2(20), C703_SALES_REP_ID NUMBER, C704_ACCOUNT_ID VARCHAR2(20), C4010_VOID_FL VARCHAR2(1), TBL_PERSONAL_NOTES VARCHAR(255), C901_CASE_STATUS NUMBER, TBL_CREATED_BY VARCHAR2(10), TBL_CREATED_DATE DATE, TBL_LAST_UPDATED_BY VARCHAR2(10), TBL_LAST_UPDATED_DATE DATE, TBL_PARENT_CASE_INFO_ID NUMBER, C901_TYPE VARCHAR2(20));", []);
    tx.executeSql("CREATE TABLE IF NOT EXISTS TBL_CASE_ATTRIBUTE(COL_CASE_ATTRIBUTE_ID NUMBER PRIMARY KEY, COL_CASE_INFO_ID NUMBER, C901_ATTRIBUTE_TYPE VARCHAR2(100), COL_ATTRIBUTE_VALUE VARCHAR2(100), COL_VOID_FL CHAR(1));", []);

    //Combined Case View
    tx.executeSql("CREATE VIEW IF NOT EXISTS VW_CASE_INFORMATION AS SELECT C7100_CASE_INFO_ID AS CASE_INFO_ID, C7100_CASE_ID AS CASE_ID, C7100_SURGERY_DATE AS SURGERY_DATE, C7100_SURGERY_TIME AS SURGERY_TIME, C701_DISTRIBUTOR_ID AS DISTRIBUTOR_ID, C703_SALES_REP_ID AS SALES_REP_ID, C704_ACCOUNT_ID AS ACCOUNT_ID, C4010_VOID_FL AS VOID_FL, C7100_PERSONAL_NOTES AS PERSONAL_NOTES, C901_CASE_STATUS AS CASE_STATUS, C7100_CREATED_BY AS CREATED_BY, C7100_CREATED_DATE AS CREATED_DATE, C7100_LAST_UPDATED_BY AS LAST_UPDATED_BY, C7100_LAST_UPDATED_DATE AS LAST_UPDATED_DATE, C7100_PARENT_CASE_INFO_ID AS PARENT_CASE_INFO_ID, C901_TYPE AS TYPE, 'SERVER' AS LOCATION FROM T7100_CASE_INFORMATION UNION SELECT COL_CASE_INFO_ID AS CASE_INFO_ID, COL_CASE_ID AS CASE_ID, COL_SURGERY_DATE AS SURGERY_DATE, COL_SURGERY_TIME AS SURGERY_TIME, C701_DISTRIBUTOR_ID AS DISTRIBUTOR_ID, C703_SALES_REP_ID AS SALES_REP_ID, C704_ACCOUNT_ID AS ACCOUNT_ID, C4010_VOID_FL AS VOID_FL, TBL_PERSONAL_NOTES AS PERSONAL_NOTES, C901_CASE_STATUS AS CASE_STATUS, TBL_CREATED_BY AS CREATED_BY, TBL_CREATED_DATE AS CREATED_DATE, TBL_LAST_UPDATED_BY AS LAST_UPDATED_BY, TBL_LAST_UPDATED_DATE AS LAST_UPDATED_DATE, TBL_PARENT_CASE_INFO_ID AS PARENT_CASE_INFO_ID, C901_TYPE AS TYPE, 'LOCAL' AS LOCATION FROM TBL_CASE_INFORMATION;", []);
    tx.executeSql("CREATE VIEW IF NOT EXISTS VW_CASE_ATTRIBUTE AS SELECT C7102_CASE_ATTRIBUTE_ID AS CASE_ATTRIBUTE_ID, C7100_CASE_INFO_ID AS CASE_INFO_ID, C901_ATTRIBUTE_TYPE AS ATTRIBUTE_TYPE, C7102_ATTRIBUTE_VALUE AS ATTRIBUTE_VALUE, C7102_VOID_FL AS VOID_FL, 'SERVER' AS LOCATION FROM T7102_CASE_ATTRIBUTE UNION SELECT COL_CASE_ATTRIBUTE_ID AS CASE_ATTRIBUTE_ID, COL_CASE_INFO_ID AS CASE_INFO_ID, C901_ATTRIBUTE_TYPE AS ATTRIBUTE_TYPE, COL_ATTRIBUTE_VALUE AS ATTRIBUTE_VALUE, COL_VOID_FL AS VOID_FL, 'LOCAL' AS LOCATION FROM TBL_CASE_ATTRIBUTE;", []);

    tx.executeSql("CREATE TABLE IF NOT EXISTS TBL_CASE(CASE_INFO_ID INTEGER PRIMARY KEY, USER VARCHAR2(50), CASEDATA VARCHAR2(1000), STATUS VARCHAR2(25));", []);

    //User
    tx.executeSql("CREATE TABLE IF NOT EXISTS T101_USER(C101_USER_ID NUMBER PRIMARY KEY, C101_USER_F_NAME VARCHAR2(100) NOT NULL, C101_USER_L_NAME VARCHAR2(100), C101_USER_SH_NAME VARCHAR2(20) NOT NULL, C101_EMAIL_ID VARCHAR2(200) NOT NULL, C101_DEPT_ID CHAR(1), C101_ACCESS_LEVEL_ID NUMBER NOT NULL, C701_DISTRIBUTOR_ID VARCHAR2(20), C101_PARTY_ID NUMBER, C901_USER_STATUS NUMBER, C901_USER_TYPE NUMBER, C101_HIRE_DT DATE NOT NULL, C101_TERMINATION_DATE DATE);", []);

    //Save Request Data
    tx.executeSql("CREATE TABLE IF NOT EXISTS TBL_REQUEST(REQUEST_ID INTEGER PRIMARY KEY AUTOINCREMENT, USER VARCHAR2(50), REQUESTDATA VARCHAR2(1000), STATUS VARCHAR2(25));", []);

    tx.executeSql("CREATE TABLE IF NOT EXISTS T1530A_ACCESS(C1530A_EVENT_NAME VARCHAR2(35), C1530A_TAG_NAME VARCHAR2(30), C1530A_PAGE_NAME VARCHAR2(30), C1530A_UPDATE_ACCESS_FL VARCHAR2(1), C1530A_READ_ACCESS_FL VARCHAR2(1), C1530A_VOID_ACCESS_FL VARCHAR2(1), C1530A_VOID_FL VARCHAR2(1), C1501_VOID_FL VARCHAR2(1), C906_VOID_FL VARCHAR2(1), PRIMARY KEY(C1530A_EVENT_NAME, C1530A_TAG_NAME, C1530A_PAGE_NAME));", []);
    
    //Save NPI Details with the combination of Accountid and Repid
    tx.executeSql("CREATE TABLE IF NOT EXISTS TBL_DO_NPI_SURGEON_INFO(ACCOUNTID VARCHAR2(50),REPID NUMBER,NPIID NUMBER, SURGEONNAME VARCHAR2(250),ORDERDATE DATE);", []);
    
    // TEMPORARY TABLE TO STORE ACCOUNT DETAILS
    tx.executeSql("CREATE TABLE IF NOT EXISTS T101D_USER_ACCESS(C101D_USER_ACCESS_ID NUMBER(8,2) PRIMARY KEY,C101_USER_ID NUMBER(8,2),C704_ACCOUNT_ID VARCHAR2(20),C1900_COMPANY_ID NUMBER(8,2),C101D_VOID_FL VARCHAR2(1));", []);

    //Tag Image Details From Image
    tx.executeSql("CREATE TABLE IF NOT EXISTS TBL_DO_TAG_IMG_DTL (TAG_IMG_DTL_ID INTEGER PRIMARY KEY AUTOINCREMENT , DO_ID VARCHAR2(50), IMG_NM VARCHAR2(500),IMG_PATH LONGBLOB,PROCESSED_TAGS VARCHAR2(800),PROCESSED_DT DATE);", []);
    
    //Tag DO Details 
    tx.executeSql("CREATE TABLE IF NOT EXISTS TBL_DO_TAG_DETAILS (TAG_DO_DTL_ID INTEGER PRIMARY KEY AUTOINCREMENT , DO_ID VARCHAR2(50), TAG_ID VARCHAR2(40),SET_ID VARCHAR2 (20), SET_NM VARCHAR2 (100),LATITUDE VARCHAR2(200),LONGITUDE VARCHAR2(200),STATUS VARCHAR2(25));", []);
        
}

//Transaction error callback
function errorCB(tx, err) {
    console.log(tx);
    console.log(err);
    showMsgView("009");
}

function getSyncCount(data) {
    if (data[0].no == 0) {
        var data = [];
        _.each(jsnUpdateCode, function (key, value) {
            var json = {
                reftype: eval(key).code,
                statusid: "103120",
                syncgrp: eval(key).syncGrp,
                syncseq: eval(key + ".syncSeq").toString()
            }
        });
        updateLocalDB("GmProdCatlReqVO", data, function() {
            console.log("local sync table insert success");
        });
    }
}

// Transaction success callback
function successCB() {
    //showMsgView("008");
}

function dropTables(tx) {
    // tx.executeSql("DROP TABLE T205_PART_NUMBER", []);
    // tx.executeSql("DROP TABLE T207_SET_MASTER", []);
    // tx.executeSql("DROP TABLE T208_SET_DETAILS", []);
    // tx.executeSql("DROP TABLE T207C_SET_ATTRIBUTE", []);
    // tx.executeSql("DROP TABLE T901_CODE_LOOKUP", []);
    // tx.executeSql("DROP TABLE T903_UPLOAD_FILE_LIST", []);
    // tx.executeSql("DROP TABLE T2001_MASTER_DATA_UPDATES", []);
    // tx.executeSql("DROP TABLE T4010_GROUP", []);
    // tx.executeSql("DROP TABLE T4011_GROUP_DETAIL", []);  
    // tx.executeSql("DROP TABLE T101_PARTY", []);
    // tx.executeSql("DROP TABLE T107_CONTACT", []);  
    // tx.executeSql("DROP TABLE TBL_FAVORITE", []);
    // tx.executeSql("DROP TABLE TBL_LOCALE", []);
    // tx.executeSql("DROP TABLE TBL_MSG_STATUS", []);
    // tx.executeSql("DROP TABLE TBL_RECENT", []);
    // tx.executeSql("DROP TABLE TBL_SHARE_LIST", []);
    // tx.executeSql("DROP TABLE TBL_PARTY", []);
    // tx.executeSql("DROP TABLE TBL_CONTACT", []);
    // tx.executeSql("DROP TABLE TBL_DO", []);
    // tx.executeSql("DROP TABLE TBL_REQUEST", []);
    // tx.executeSql("DROP TABLE TBL_REQUESTDATA", []);
    //    tx.executeSql("DROP TABLE TBL_SURGEON", []);
    //    tx.executeSql("DROP TABLE TBL_ACTIVITY", []);
    //    tx.executeSql("DROP TABLE TBL_CRITERIA", []);
    //    tx.executeSql("DROP TABLE v700_territory_mapping_detail", []);
    //    tx.executeSql("DROP TABLE t902_log", []);
    //    tx.executeSql("DROP TABLE T703_SALES_REP", []);
}


function testAlter(iValue) {
    if (iValue < arrAlterTables.length) {
        db.transaction(function (tx, rs) {
            tx.executeSql("SELECT sql FROM sqlite_master WHERE type='table' and name=?", [arrAlterTables[iValue]],
                function(tx, rs) {
                    testColumn(iValue, rs.rows.item(0).sql);
                },
                function (tx, err) {
                    console.log(JSON.stringify(err))
                });

        });
    }
}

function testColumn(iValue, query) {
    var arrQuery = query.substring(query.indexOf('(') + 1, query.length - 1).split(',');
    var arrCurrColumns = new Array();
    for (var i = 0; i < arrQuery.length; i++) {
        arrCurrColumns.push(arrQuery[i].trim().split(' ')[0]);
    }

    $.ajax({
        url: "globusMobileApp/datafiles/tables.json",
        method: "GET",
        datatype: "application/json",
        success: function (data) {
            if (typeof (data) != "object")
                data = $.parseJSON(data);
            var currRow = data[arrAlterTables[iValue]];
            var arrColumns = _.pluck(currRow, "column");

            var newArray = _.difference(arrColumns, arrCurrColumns);
            if (newArray.length > 0) {
                var newConfigColumn = new Array();
                for (var i = 0; i < currRow.length; i++) {
                    if (newArray.indexOf(currRow[i].column) >= 0) {
                        newConfigColumn.push(currRow[i]);
                    }
                }
                testAlterTable(0, newConfigColumn, arrAlterTables[iValue], function() {
                    testAlter(iValue + 1);
                });
            } else
                testAlter(iValue + 1);


        }
    });

}

function testAlterTable(iValue, newConfigColumn, tableName, callback) {
    if (iValue < newConfigColumn.length) {
        var query = "ALTER TABLE " + tableName + " ADD COLUMN " + newConfigColumn[iValue].column + " " + newConfigColumn[iValue].type;
        db.transaction(function(tx, rs) {
            tx.executeSql(query, [],
                function(tx, rs) {
                    testAlterTable(iValue + 1, newConfigColumn, tableName, callback)
                },
                function (tx, err) {
                    console.log(JSON.stringify(err))
                });

        });
    } else
        callback();

}

function fnResetDB(callback) {
    try {
        deleteTables(0, deleteTables, callback);
    } catch (err) {
        showAppError(err, "GmDatabase:fnResetDB");
    }
}

function deleteTables(iValue, innerCallback, outerCallback) {

    var query = "DELETE FROM " + resetTables[iValue];
    var tableName = resetTables[iValue];
    switch (tableName) {
        case "T1530A_ACCESS":
            query = query + " WHERE C1530A_PAGE_NAME NOT IN ('SYNC_DATA','SETTINGS')";
            break;
    }
    db.transaction(function (tx, rs) {
        tx.executeSql(query, [],
            function (tx, rs) {
                console.log("deleted - " + query);
                if (iValue != (resetTables.length - 1))
                    innerCallback(iValue + 1, deleteTables, outerCallback);
                else
                    outerCallback();
            },
            function(tx, err) {
                showAppError(err, "GmDatabase:deleteTables()");
            });
    });
}

function getTable(tableName) {

    var url = "globusMobileApp/sql/" + tableName + ".sql";
    $.get(url, loadTable);
}

function loadTable(data) {

    var query = data.split('\n');

    // Execute Insert Queries
    db.transaction(function (tx) {
        for (var i = 0; i < query.length; i++) {
            tx.executeSql(query[i], [], function (a) {
                console.log("Successfully loaded " + i + " records");
            }, function (e) {
                console.log(query[i]);
            });
        }
    });
}

// Function Name: getFieldValues
// Purpose: This function will get the field-names in a table-row
//
function getFieldNames(VOname, row) {
    try {
        var FieldNames = []

        for (var column in row) {
            FieldNames.push(eval(VOname + "." + column));
        }

        return FieldNames;
    } catch (err) {
        showAppError(err, "GmDatabase:getFieldNames()");
    }
}

// Function Name: getFieldValues
// Purpose: This function will get the field-values in a table-row
//
function getFieldValues(VOname, row) {
    try {
        var FieldValues = [];

        for (var column in row) {
            if (!row.hasOwnProperty(column))
                continue;
            if (eval(VOname + "." + column) != undefined) {
                if (VOname == "listGmTagInfoVO"){
                     FieldValues.push("'" + row[column]+ "'");
                }else{
                    FieldValues.push("'" + row[column].replace(/\(/g, "\(").replace(/\)/g, "\)").replace(/\'/g, "\&#39;") + "'");
                }
            }

        }
        return FieldValues;
    } catch (err) {
        showAppError(err, "GmDatabase:getFieldValues()");
    }
}


function getFieldValuesForChangeDB(VOname, row) {
    try {
        var FieldValues = [];

        for (var column in row) {
            if (!row.hasOwnProperty(column))
                continue;
            if (eval(VOname + "." + column) != undefined) {
                FieldValues.push(row[column].replace(/\(/g, "\(").replace(/\)/g, "\(").replace(/\'/g, "\("));
            }

        }

        return FieldValues;
    } catch (err) {
        showAppError(err, "GmDatabase:getFieldValuesForChangeDB()");
    }
}

// Function Name: LDB_InsertUpdateSQL
// Purpose: This function will create the SQL to INSERT OR REPLACE values into a table with name "tableName"
//
function LDB_InsertUpdateSQL(VOname, row) {
    try {
        var FieldNames = getFieldNames(VOname, row);
        var FieldValues = getFieldValues(row);
        var l = FieldNames.length;
        // var values = "";
        var tableName = eval(VOname + ".tableName");

        var sql = "INSERT OR REPLACE INTO " + tableName + " (";

        for (i = 0; i < l; i++) {
            if (FieldNames[i] !== "ID") {
                sql += FieldNames[i];
                // values += "'" + row[FieldValues[i]].replace(/'/g,"''") + "'";

                if (i < l - 1) {
                    sql += ",";
                    // values += ","
                }

                if (i === l - 1) {
                    sql += ") VALUES (";
                    for (var k = 0; k < FieldNames.length; k++) {
                        sql += "?";
                        if (k < FieldNames.length - 1)
                            sql += ",";
                    }
                    sql += ")";

                    // values += ")";
                }
            }
        }
        // sql += values;

        return sql;
    } catch (err) {
        showAppError(err, "GmDatabase:LDB_InsertUpdateSQL()");
    }
}

function getRow(tableName) {
    db.transaction(function(tx, rs) {
        tx.executeSql("SELECT * FROM " + tableName + " LIMIT 1", [],
            function(tx, rs) {},
            function(err) {
                showAppError(err, "GmDatabase:getRow()");
            });
    });
}

function fnExecuteDeleteQuery(query, callback) {
    db.transaction(function(tx, rs) {
        tx.executeSql(query, [],
            function(tx, rs) {
                callback(true);
            },
            function(tx, err) {
                callback(false);
                showAppError(err, "GmDatabase:fnExecuteDeleteQuery()");
            });
    });
}


function fnExecuteSelectQuery(query, callback) {
    var results = new Array();
    db.transaction(function(tx, rs) {
        tx.executeSql(query, [],
            function(tx, rs) {
                for (var i = 0; i < rs.rows.length; i++)
                    results.push(rs.rows.item(i));
                callback(results);
            },
            function(tx, err) {
                callback(results);
                showAppError(err, "GmDatabase:fnExecuteSelectQuery()");
            });
    });
}

//This function will migrate data from websql to sqlite
function fnMigrateDB() {
	console.log("export start");
dbWebSql = window.openDatabase("gmiData", "1.0", "Globus Mobile Database", 10000);

fnExecuteSelectQuery("SELECT name name FROM sqlite_master WHERE type ='table' AND  name NOT LIKE '%WebKit%'",function(data){
  _.each(data, function(tb) {
	 // var tb = "T906_RULES";
	  console.log("inserting table "+tb.name);
	  
  fnExportWebSqlData(tb.name);
  //fnDropWebSqlTable(tb.name);
  });
});
	console.log("export end");
}


//This function will fetch data from websql and create insert script by 500 records
function fnExportWebSqlData(_tbl_name){
	dbWebSql.transaction(function(tx1, rs) {
		
	tx1.executeSql("SELECT * FROM " + _tbl_name + ";", [], 
    function(transaction, results) {
        if (results.rows) {
        	var that =  this;
			var _fields = [];
			var insertStr = "";
			var selectStr = "";
			var joinStr = " SELECT ";
			var n =results.rows.length;
            for (var i = 0; i < n; i++) {
                var row = results.rows.item(i);
                var _values = [];
                for (col in row) {
					if (i==0){
						_fields.push(col);
						insertStr = "INSERT OR REPLACE INTO " + _tbl_name + " (" + _fields.join(",") + ")";
					}
					if(row[col] == null)
						_values.push("null");
					else
						_values.push("'" + row[col] + "'");
                }
				
	
				   selectStr += joinStr + _values.join(",") +" ";
			    if (i%500 == 0 || i == (n -1) ){
			    	
			    	if(i == (n -1)){ // last record execution
			    		fnImportSqliteData(insertStr + selectStr,function(status,err){
			    			if(status){
			    				fnDropWebSqlTable(_tbl_name); //drop table
			    			}else{
			    				if(_tbl_name.trim() != "TBL_DO")
			    					showAppError(err, "GmDatabase:fnExportWebSqlData table:"+_tbl_name+" query:"+insertStr + selectStr);
			    				that.break; //exit loop
			    			}
			    		});
			    	}else{
			    		fnImportSqliteData(insertStr + selectStr,function(status,err){
			    			if(!status){
			    				if(_tbl_name.trim() != "TBL_DO")
			    					showAppError(err, "GmDatabase:fnExportWebSqlData table:"+_tbl_name+" query:"+insertStr + selectStr);
			    				that.break;//exit loop
			    			}
			    		});
			    	}
			    	
					selectStr = "";
					joinStr = " SELECT ";
				}else{
					joinStr = " UNION ALL SELECT ";
				}
                
            }
        }
    }
);
});	
}

//This function will insert data in sqlite db table
function fnImportSqliteData(query,callback){
	//console.log(query);
 	db.transaction(function(tx, rs) {
        tx.executeSql(query, [],
            function(tx, rs) {
        	  callback(true,null);
            },
            function(tx, err) {
              callback(false,err);	
              console.log(err);
            });
    });
	
}

//This function will drop table from websql
function fnDropWebSqlTable(_tbl_name){
	//console.log("drop table"+_tbl_name);
	dbWebSql.transaction(function(tx1, rs) {
	tx1.executeSql("DROP TABLE " + _tbl_name + ";", [], 
    function(transaction, results) {
		console.log("Drop table "+_tbl_name);
    }
);
});
}

/*
*fnsyncTagDataInAppStartup(): This function is used to call the tag sync  call in App startup itself.
*and this is to avoid the manual sync in Settings page of GlobusApp
*/
function fnsyncTagDataInAppStartup() {
   var cmpid= localStorage.getItem("cmpid");
if ((navigator.onLine) && (!intOfflineMode)) {
    fnGetRuleValues('TAG_APP_SYNC', 'APP_STARTUP_SYNC', cmpid, function (data) {
    if (data != null) {
     console.log(data.rulevalue);
if(data.rulevalue=='Y'){
    console.log(" Inside fnsyncTagDataInAppStartup");
    showMsgView("024",'TAG');
    console.log(" Inside listGmTagInfoVO");
var that = this;
var ws = "listGmTagInfoVO";
var code = "4001234";
var syncname = "Tag";
var now = new Date();
                fnSyncStatusCheck(ws, function(syncstatus) {
                    try {
                        if (syncstatus != undefined) {
                            var objVO = eval(ws);
                            objVO.parentStatus = syncstatus;
                        }
                       // that.$("#div-updates").html("<li class='li-info-center'><img />&nbsp;"+"Fetching Tag Data..."+"</li>").slideDown(100);
                        DataSync_SL(ws, '', function(status) {
                            try {
                                console.log(" ws ws" + ws);
                                that.i++;
                                fnInsertLastsync(syncname.split(' ').join(''), now);
                                fnClearUpdates(code, function(sts) {
                                    try {                                        
                                    } catch (err) {
                                        showAppError(err, "GmSettingsView:sync(3) ");
                                    }
                                });
                            } catch (err) {
                                showAppError(err, "GmSettingsView:sync(2) ");
                            }
                             console.log("status??"+status)
                            if (status == "success"){
                                  showMsgView("013");
                            }
                            else{
                              showMsgView("014");  
                            }
                        });
                       
                    } catch (err) {
                        showAppError(err, "GmSettingsView:sync(1) ");
                    }
                });          
}
 }
});
    
}
}
