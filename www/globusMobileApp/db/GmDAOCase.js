
var CASE_INFORMATION = "SELECT C7100_CASE_INFO_ID AS CASE_INFO_ID, C7100_CASE_ID AS CASE_ID, C7100_SURGERY_DATE AS SURGERY_DATE, C7100_SURGERY_TIME AS SURGERY_TIME, C701_DISTRIBUTOR_ID AS DISTRIBUTOR_ID, C703_SALES_REP_ID AS SALES_REP_ID, C704_ACCOUNT_ID AS ACCOUNT_ID, C4010_VOID_FL AS VOID_FL, C7100_PERSONAL_NOTES AS PERSONAL_NOTES, C901_CASE_STATUS AS CASE_STATUS, C7100_CREATED_BY AS CREATED_BY, C7100_CREATED_DATE AS CREATED_DATE, C7100_LAST_UPDATED_BY AS LAST_UPDATED_BY, C7100_LAST_UPDATED_DATE AS LAST_UPDATED_DATE, C7100_PARENT_CASE_INFO_ID AS PARENT_CASE_INFO_ID, C901_TYPE AS TYPE FROM T7100_CASE_INFORMATION UNION SELECT COL_CASE_INFO_ID AS CASE_INFO_ID, COL_CASE_ID AS CASE_ID, COL_SURGERY_DATE AS SURGERY_DATE, COL_SURGERY_TIME AS SURGERY_TIME, C701_DISTRIBUTOR_ID AS DISTRIBUTOR_ID, C703_SALES_REP_ID AS SALES_REP_ID, C704_ACCOUNT_ID AS ACCOUNT_ID, C4010_VOID_FL AS VOID_FL, TBL_PERSONAL_NOTES AS PERSONAL_NOTES, C901_CASE_STATUS AS CASE_STATUS, TBL_CREATED_BY AS CREATED_BY, TBL_CREATED_DATE AS CREATED_DATE, TBL_LAST_UPDATED_BY AS LAST_UPDATED_BY, TBL_LAST_UPDATED_DATE AS LAST_UPDATED_DATE, TBL_PARENT_CASE_INFO_ID AS PARENT_CASE_INFO_ID, C901_TYPE AS TYPE FROM TBL_CASE_INFORMATION";

var CASE_ATTRIBUTE = "SELECT C7102_CASE_ATTRIBUTE_ID AS CASE_ATTRIBUTE_ID, C7100_CASE_INFO_ID AS CASE_INFO_ID, C901_ATTRIBUTE_TYPE AS ATTRIBUTE_TYPE, C7102_ATTRIBUTE_VALUE AS ATTRIBUTE_VALUE, C7102_VOID_FL AS VOID_FL FROM T7102_CASE_ATTRIBUTE UNION SELECT COL_CASE_ATTRIBUTE_ID AS CASE_ATTRIBUTE_ID, COL_CASE_INFO_ID AS CASE_INFO_ID, C901_ATTRIBUTE_TYPE AS ATTRIBUTE_TYPE, COL_ATTRIBUTE_VALUE AS ATTRIBUTE_VALUE, COL_VOID_FL AS VOID_FL FROM TBL_CASE_ATTRIBUTE";



function fnSaveCase(CaseInfoID, CaseData,callback) {

	console.log("fnSaveCase CaseID : " + CaseInfoID + " CaseData: " + CaseData);

	var userId = localStorage.getItem("userID");
	db.transaction(function (tx, rs) {
		tx.executeSql("INSERT OR REPLACE INTO TBL_CASE(CASE_INFO_ID, CASEDATA, USER, STATUS) VALUES(?,?,?,'DRAFT')", [CaseInfoID, CaseData, userId],
			function(tx,rs) {
			console.log("Case Updated for Case ID " + CaseInfoID);
			callback();
			},
		function(e,err){showMsgView("009");});
	});
}


function fnGetCase(CaseID, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT CASE_INFO_ID AS caseinid, CASE_ID AS caseid, SURGERY_DATE AS surdt, SURGERY_TIME AS surstime, DISTRIBUTOR_ID AS did,  IFNULL(T701.C701_DISTRIBUTOR_NAME,'') AS DIST_NAME, SALES_REP_ID AS repid,  IFNULL(T703.C703_SALES_REP_NAME,'') AS REP_NAME, ACCOUNT_ID AS acid, ACCOUNT_ID || ' ' || IFNULL(T704.C704_ACCOUNT_NM,'') AS ACCOUNT_NM, PERSONAL_NOTES AS prnotes, CASE_STATUS AS status, LOCATION FROM VW_CASE_INFORMATION LEFT OUTER JOIN T701_DISTRIBUTOR T701 ON T701.C701_DISTRIBUTOR_ID = DISTRIBUTOR_ID LEFT OUTER JOIN T703_SALES_REP T703 ON T703.C703_SALES_REP_ID = SALES_REP_ID LEFT OUTER JOIN T704_ACCOUNT T704 ON T704.C704_ACCOUNT_ID = ACCOUNT_ID WHERE CASE_ID=?", [CaseID],
			function(tx,rs) {
				if(rs.rows.length>0) {
					var casedata = rs.rows.item(0);
					callback(casedata);
				}
				else {
					//alert("Failed");
//					console.log("No case with CaseID " + CaseID + " to edit");
				}
			},
		function(e,err){console.log(err);});
	});
}


function fnGetCaseAttribute(CaseInfoID, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT CASE_ATTRIBUTE_ID, ATTRIBUTE_TYPE, VOID_FL, ATTRIBUTE_VALUE,  IFNULL(T703.C703_SALES_REP_NAME,'') AS REP_NAME, IFNULL(T101.C101_USER_F_NAME || ' ' || T101.C101_USER_L_NAME,'') AS USER_NAME FROM VW_CASE_ATTRIBUTE LEFT OUTER JOIN T703_SALES_REP T703 ON T703.C703_SALES_REP_ID = ATTRIBUTE_VALUE LEFT OUTER JOIN T101_USER T101 ON T101.C101_USER_ID = ATTRIBUTE_VALUE WHERE CASE_INFO_ID=?", [CaseInfoID],
			function(tx,rs) {
				var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
					results.push(rs.rows.item(i));
				}
				callback(results);
			},
		function(e,err){console.log(JSON.stringify(err))});
	});
}



function fnGetSchedule(fdate,tdate,callback) {
	console.log("Schedule From: " + fdate + " To: " + tdate);
	
	var query = "SELECT VW_INFO.CASE_INFO_ID AS CAINID, " +
		"VOID_FL, CASE_ID AS CAID, CREATED_BY AS USERID, " +
		"SURGERY_DATE AS SDATE, SURGERY_TIME AS STIME, ACCOUNT_ID AS ACCOUNTID, " +
		"IFNULL(T701.C701_DISTRIBUTOR_NAME,'') AS DID, " +
		"IFNULL(T703.C703_SALES_REP_NAME,'') AS RID, " +
		"ACCOUNT_ID || ' ' || IFNULL(T704.C704_ACCOUNT_NM,'') AS ACID, " +
		"CASE_STATUS AS STATUS, PARENT_CASE_INFO_ID AS PCINID, TYPE AS TYPE, " +
		"IFNULL(TEMP_CATEGORY.CATEGORY,'No Category') AS CATEGORY, " +
		"IFNULL(TEMP_SURGEON.SURGEON_NAME,'') AS SURGEON_NAME " +
		"FROM VW_CASE_INFORMATION VW_INFO " +
		"LEFT OUTER JOIN T701_DISTRIBUTOR T701 " + 
		"ON T701.C701_DISTRIBUTOR_ID = DISTRIBUTOR_ID " +
		"LEFT OUTER JOIN T703_SALES_REP T703 " +
		"ON T703.C703_SALES_REP_ID = SALES_REP_ID " +
		"LEFT OUTER JOIN T704_ACCOUNT T704 " +
		"ON T704.C704_ACCOUNT_ID = ACCOUNT_ID " +
		"LEFT OUTER JOIN " +
		 	"(SELECT VW_ATTR.CASE_INFO_ID, " +
		     "IFNULL(T901.C901_CODE_NM,'') AS CATEGORY " +
		     "FROM VW_CASE_ATTRIBUTE VW_ATTR " +
		     "LEFT OUTER JOIN T901_CODE_LOOKUP T901 ON T901.C901_CODE_ID = ATTRIBUTE_TYPE " +
		     "WHERE ( " +
		     	"ATTRIBUTE_TYPE = '11180' " +
		     	"OR ATTRIBUTE_TYPE = '11181' " +
		     	"OR ATTRIBUTE_TYPE = '11182' " +
		     	"OR ATTRIBUTE_TYPE = '11183' " +
		     	"OR ATTRIBUTE_TYPE = '11184' " +
		     	"OR ATTRIBUTE_TYPE = '11185' " +
		     	"OR ATTRIBUTE_TYPE = '11186') " +
		     "AND ATTRIBUTE_VALUE='Y' " +
		     "AND (VOID_FL IS NULL OR VOID_FL IS '')) TEMP_CATEGORY "+
		"ON TEMP_CATEGORY.CASE_INFO_ID = VW_INFO.CASE_INFO_ID " +
		"LEFT OUTER JOIN " +
			"(SELECT VW_ATTR.CASE_INFO_ID, " +
			"ATTRIBUTE_VALUE AS SURGEON_NAME " +
			"FROM VW_CASE_ATTRIBUTE VW_ATTR " +
			"WHERE ATTRIBUTE_TYPE = '103640' " +
			"AND (VOID_FL IS NULL OR VOID_FL IS '')) TEMP_SURGEON " +
		"ON TEMP_SURGEON.CASE_INFO_ID = VW_INFO.CASE_INFO_ID " +
		"WHERE (VOID_FL='' OR VOID_FL IS NULL ) " +
		"AND SURGERY_DATE BETWEEN ? AND ? " +
		"ORDER BY SURGERY_DATE, SURGERY_TIME, SURGEON_NAME, CATEGORY, ACID ASC";
	
	console.log(query);
	
	db.transaction(function (tx, rs) {
		tx.executeSql(query, [fdate, tdate],
				function(tx,rs) {
			var i=0, results = [];
			console.log("Here are the Cases Scheduled");
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
		},
		function(e,err){//alert("Failed"); 
			console.log(JSON.stringify(err));
		showMsgView("009");});
	});
}

function fnGetScheduleMore(fdate,callback) {
	
	var query = "SELECT VW_INFO.CASE_INFO_ID AS CAINID, " +
		"VOID_FL, CASE_ID AS CAID, CREATED_BY AS USERID, " +
		"SURGERY_DATE AS SDATE, SURGERY_TIME AS STIME, ACCOUNT_ID AS ACCOUNTID, " +
		"IFNULL(T701.C701_DISTRIBUTOR_NAME,'') AS DID, " +
		"IFNULL(T703.C703_SALES_REP_NAME,'') AS RID, " +
		"ACCOUNT_ID || ' ' || IFNULL(T704.C704_ACCOUNT_NM,'') AS ACID, " +
		"CASE_STATUS AS STATUS, PARENT_CASE_INFO_ID AS PCINID, TYPE AS TYPE, " +
		"IFNULL(TEMP_CATEGORY.CATEGORY,'No Category') AS CATEGORY, " +
		"IFNULL(TEMP_SURGEON.SURGEON_NAME,'') AS SURGEON_NAME " +
		"FROM VW_CASE_INFORMATION VW_INFO " +
		"LEFT OUTER JOIN T701_DISTRIBUTOR T701 " + 
		"ON T701.C701_DISTRIBUTOR_ID = DISTRIBUTOR_ID " +
		"LEFT OUTER JOIN T703_SALES_REP T703 " +
		"ON T703.C703_SALES_REP_ID = SALES_REP_ID " +
		"LEFT OUTER JOIN T704_ACCOUNT T704 " +
		"ON T704.C704_ACCOUNT_ID = ACCOUNT_ID " +
		"LEFT OUTER JOIN " +
		 	"(SELECT VW_ATTR.CASE_INFO_ID, " +
		     "IFNULL(T901.C901_CODE_NM,'') AS CATEGORY " +
		     "FROM VW_CASE_ATTRIBUTE VW_ATTR " +
		     "LEFT OUTER JOIN T901_CODE_LOOKUP T901 ON T901.C901_CODE_ID = ATTRIBUTE_TYPE " +
		     "WHERE ( " +
		     	"ATTRIBUTE_TYPE = '11180' " +
		     	"OR ATTRIBUTE_TYPE = '11181' " +
		     	"OR ATTRIBUTE_TYPE = '11182' " +
		     	"OR ATTRIBUTE_TYPE = '11183' " +
		     	"OR ATTRIBUTE_TYPE = '11184' " +
		     	"OR ATTRIBUTE_TYPE = '11185' " +
		     	"OR ATTRIBUTE_TYPE = '11186') " +
		     "AND ATTRIBUTE_VALUE='Y' " +
		     "AND (VOID_FL IS NULL OR VOID_FL IS '')) TEMP_CATEGORY "+
		"ON TEMP_CATEGORY.CASE_INFO_ID = VW_INFO.CASE_INFO_ID " +
		"LEFT OUTER JOIN " +
			"(SELECT VW_ATTR.CASE_INFO_ID, " +
			"ATTRIBUTE_VALUE AS SURGEON_NAME " +
			"FROM VW_CASE_ATTRIBUTE VW_ATTR " +
			"WHERE ATTRIBUTE_TYPE = '103640' " +
			"AND (VOID_FL IS NULL OR VOID_FL IS '')) TEMP_SURGEON " +
		"ON TEMP_SURGEON.CASE_INFO_ID = VW_INFO.CASE_INFO_ID " +
		"WHERE (VOID_FL='' OR VOID_FL IS NULL ) " +
		// "AND SURGERY_DATE  > ?" +
		"ORDER BY SURGERY_DATE, SURGERY_TIME, SURGEON_NAME, CATEGORY, ACID ASC";
	
	console.log(query);
	
	db.transaction(function (tx, rs) {
		tx.executeSql(query, [],
				function(tx,rs) {
			var i=0, results = [];
			console.log("Here are the Cases Scheduled");
			for(i=0;i<rs.rows.length;i++) {
				var fromdate = new Date(fdate);
				var todate = new Date(rs.rows.item(i).SDATE);
				var diffDays = todate.getTime() - fromdate.getTime(); 
				console.log(diffDays);
				if(diffDays >= 0) { 
					results.push(rs.rows.item(i));
				}
				
//				results.push(rs.rows.item(i));
			}
			
			console.log(results);
			callback(results);
		},
		function(e,err){//alert("Failed"); 
			console.log(JSON.stringify(err));
		showMsgView("009");});
	});
}

function fnBookCase(data, callback) {
	console.log("fnBookCase Data: " + JSON.stringify(data));
	db.transaction(function (tx, rs) {
		tx.executeSql("INSERT OR REPLACE INTO T7100_CASE_INFORMATION(C7100_CASE_INFO_ID, C7100_CASE_ID,  C7100_SURGERY_DATE, C7100_SURGERY_TIME, C701_DISTRIBUTOR_ID, C703_SALES_REP_ID, C704_ACCOUNT_ID,  C4010_VOID_FL, C7100_PERSONAL_NOTES, C901_CASE_STATUS, C7100_PARENT_CASE_INFO_ID, C901_TYPE) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)", [data.cainfoid, data.caseid, data.surdt, data.surstime, data.did, data.repid, data.acid, data.voidfl, data.prnotes, data.status, data.prtcaid, data.type],
				function(tx,rs) { console.log(rs.rowsAffected + " Records were inserted into the T7100 Case Info Table");
				fnGetBookedCase(data.caseid,callback);
				//fnGetSchedule(data.caseid);
		},null);
		
	});
}

function fnSaveDraftCase(data) {
	db.transaction(function (tx, rs) {
		tx.executeSql("INSERT OR REPLACE INTO T7100_CASE_INFORMATION(C7100_CASE_INFO_ID, C7100_CASE_ID,  C7100_SURGERY_DATE, C7100_SURGERY_TIME, C701_DISTRIBUTOR_ID, C703_SALES_REP_ID, C704_ACCOUNT_ID,  C4010_VOID_FL, C7100_PERSONAL_NOTES, C901_CASE_STATUS, C7100_PARENT_CASE_INFO_ID, C901_TYPE) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)", [data.cainfoid, data.caseid, data.surdt, data.surtime, data.did, data.repid, data.acid, data.voidfl, data.prnotes, data.status, data.prtcaid, data.type],
				function(tx,rs) { //alert(rs.rowsAffected + " Records were inserted");
				fnGetBookedCase(data.caseid);
//				fnGetSchedule(data.caseid);
		},null);
		
	});
}

function fnGetBookedCase(CaseID, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT * FROM T7100_CASE_INFORMATION WHERE C7100_CASE_ID=?", [CaseID],
			function(tx,rs) {
				if(rs.rows.length>0) {
					console.log("Here is the case just booked");
					for(var i=0; i<rs.rows.length; i++) {
						console.log(JSON.stringify(rs.rows.item(i)));		
					}
					
					callback("Success");
				}
			},
		function(e,err){showMsgView("009");});
	});
}

function fnGetUser(url,txtSearch, callback) {
	txtSearch = txtSearch.split(' ');
	var searchTerm = [];
	for(var i=0; i<txtSearch.length;i++) {
	       searchTerm.push("Name LIKE '%"+txtSearch[i]+"%'");
	}

	var query = searchTerm.join(" AND ");
	
	query = "SELECT  T101.C101_USER_ID AS ID,  T101.C101_USER_F_NAME || ' ' || T101.C101_USER_L_NAME AS DispName, T101.C101_USER_F_NAME  || ' ' ||  T101.C101_USER_ID  || ' ' ||  T101.C101_USER_L_NAME AS Name FROM T101_USER T101 WHERE " + query;
	
	db.transaction(function (tx, rs) {
		tx.executeSql(query, [],
			function(tx,rs) {
				var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
			},
		function(e,err){showMsgView("009");});
	});
}


function fnGetSalesRepVoidfl(repID,callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT C703_VOID_FL As voidfl FROM T703_SALES_REP WHERE C703_SALES_REP_ID=?", [repID],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
		},
		function(){showMsgView("009");});
	});
}

function fnSaveAttribute(caseid,repid,attributetype) {
	db.transaction(function (tx, rs) {
		tx.executeSql("INSERT OR REPLACE INTO T7102_CASE_ATTRIBUTE(C7100_CASE_INFO_ID, C7102_ATTRIBUTE_VALUE, C901_ATTRIBUTE_TYPE) VALUES (?,?,?)", [caseid,repid,attributetype],
				function(tx,rs) { 
		},null);
		
	});
}


function fnGetAssignData(caseid,callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT DISTINCT C7102_ATTRIBUTE_VALUE AS ID, C901_ATTRIBUTE_TYPE AS TYPE FROM T7102_CASE_ATTRIBUTE WHERE C7100_CASE_INFO_ID=?", [caseid],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
				console.log(rs.rows.item(i));
			}
			callback(results);
		},
		function(){ 
		showMsgView("009");});
	});
}


function fnGetEmailID(id,callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT DISTINCT C101_EMAIL_ID AS ID FROM T101_USER WHERE C101_USER_ID=?", [id],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
				console.log(rs.rows.item(i));
			}
			callback(results);
		},
		function(){ 
		showMsgView("009");});
	});
}

function fnGetCaseStatus(CaseStatus, callback) {

	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT * FROM TBL_CASE WHERE STATUS=?", [CaseStatus],
			function(tx,rs) {
				var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
					results.push(rs.rows.item(i));
					console.log(rs.rows.item(i));
				}
				
					callback(results);

			},
		function(e,err){showMsgView("009");});
	});
}


function fnSaveCaseLocal(CaseData, status, callback) {
	console.log(CaseData)
	var now = new Date(CaseData.surdt+" "+CaseData.surstime+":00");	 
	var surtime = this.formatNumber(now.getUTCMonth()+1) +"/"+this.formatNumber(now.getUTCDate())+"/"+now.getUTCFullYear()+" "+this.formatNumber(now.getUTCHours())+":"+this.formatNumber(now.getUTCMinutes())+":"+this.formatNumber(now.getUTCSeconds())+" +00:00";	
	var user = localStorage.getItem('userID');
//	var surdt = (CaseData.surdt).split('/')[2] + "-" + (CaseData.surdt).split('/')[0] + "-" + (CaseData.surdt).split('/')[1];
	db.transaction(function (tx, rs) {
		tx.executeSql("INSERT INTO TBL_CASE_INFORMATION(COL_CASE_ID, COL_SURGERY_DATE, COL_SURGERY_TIME, C701_DISTRIBUTOR_ID, C703_SALES_REP_ID, C704_ACCOUNT_ID, TBL_PERSONAL_NOTES, C901_CASE_STATUS, TBL_CREATED_BY, C901_TYPE) VALUES (?,?,?,?,?,?,?,?,?,?) ", [CaseData.caseid, CaseData.surdt, surtime, CaseData.did, CaseData.repid, CaseData.acid, CaseData.prnotes, status, user, '1006503'],
			function(tx,rs) {
				db.transaction(function (tx, rs) {
					tx.executeSql("SELECT MAX(COL_CASE_INFO_ID) AS ID FROM TBL_CASE_INFORMATION", [],
						function(tx,rs) {
							fnSaveCaseAttrLocal(rs.rows.item(0).ID, CaseData, callback);
						},
					function(e,err){
						console.log(JSON.stringify(err));
					});
				});
			},
		function(e,err){
			console.log(JSON.stringify(err));
		});
	});
}

function fnSaveCaseAttrLocal(ID, CaseData, callback) {
	
	console.log(ID);

	var attrData = CaseData.arrGmCaseAttributeVO;
	console.log(attrData);
	var query = "INSERT INTO TBL_CASE_ATTRIBUTE(COL_CASE_INFO_ID, C901_ATTRIBUTE_TYPE, COL_ATTRIBUTE_VALUE, COL_VOID_FL) SELECT '" + ID + "','" + CaseData.arrGmCaseAttributeVO[0].attrtp + "','" + CaseData.arrGmCaseAttributeVO[0].attrval + "','" + CaseData.arrGmCaseAttributeVO[0].voidfl + "'";

	for(var i=1;i<attrData.length;i++) {
		query += " UNION SELECT '" + ID + "','" + CaseData.arrGmCaseAttributeVO[i].attrtp + "','" + CaseData.arrGmCaseAttributeVO[i].attrval +"','" + CaseData.arrGmCaseAttributeVO[i].voidfl +"'";
	}

	console.log(query);
	db.transaction(function (tx, rs) {
		tx.executeSql(query, [],
			function(tx,rs) {
			console.log(ID);
				callback(ID);
			},
		function(e,err){
			console.log(JSON.stringify(err));
		});
	});
}

function fnCheckCaseID(ID, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT * FROM VW_CASE_INFORMATION WHERE CASE_ID LIKE ?", ["%" + ID + "%"],
			function(tx,rs) {
				var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
					results.push(rs.rows.item(i));
				}
				callback(results);
			},
		function(e,err){showMsgView("009");});
	});
}

function fnCaseGetFieldSales (url,txtSearch, callback) {
	txtSearch = txtSearch.split(' ');
	var searchTerm = [];
	for(var i=0; i<txtSearch.length;i++) {
	       searchTerm.push("Name LIKE '%"+txtSearch[i]+"%'");
	}

	var query = searchTerm.join(" AND ");
	
	query = "SELECT  T701.C701_DISTRIBUTOR_ID AS ID,  T701.C701_DISTRIBUTOR_NAME AS DispName, T701.C701_DISTRIBUTOR_NAME  || ' ' ||  T701.C701_DISTRIBUTOR_ID AS Name FROM T701_DISTRIBUTOR T701 WHERE  T701.C701_ACTIVE_FL = 'Y' AND T701.C701_VOID_FL = '' AND " + query;
	
	db.transaction(function (tx, rs) {
		tx.executeSql(query, [],
			function(tx,rs) {
				var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
			},
		function(e,err){showMsgView("009");});
	});
}


function fnCheckCasePendingSync () {
	console.log("fnCheckCasePendingSync");
	var userId = localStorage.getItem("userID");
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT * FROM TBL_CASE WHERE USER = ? ", [userId],
			function(tx,rs) {
				var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
					results.push(rs.rows.item(i));
				}
				fnAutoBookCase(0,results);
		},function (tx,err) {
			console.log(JSON.stringify(err));
			
		});
	});
}

function fnDeleteCaseLocal (id, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("DELETE FROM TBL_CASE_INFORMATION WHERE COL_CASE_INFO_ID=?", [id],
			function(tx,rs) {
				fnDeleteCaseAttrLocal(id, callback);
			},
		function(e,err){showMsgView("009");});
	});
}

function fnDeleteCaseAttrLocal(id, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("DELETE FROM TBL_CASE_ATTRIBUTE WHERE COL_CASE_INFO_ID=?", [id],
			function(tx,rs) {
				fnDeleteCase(id, callback);
			},
		function(e,err){showMsgView("009");});
	});
}


function fnDeleteCase(id, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("DELETE FROM TBL_CASE WHERE CASE_INFO_ID=?", [id],
			function(tx,rs) {
				callback();
			},
		function(e,err){showMsgView("009");});
	});
}

function fnUpadteCaseLocal(id, data, callback) {
	console.log(data)
	console.log(id);
	db.transaction(function (tx, rs) {
		tx.executeSql("UPDATE TBL_CASE_INFORMATION SET COL_CASE_INFO_ID=?, COL_CASE_ID=? WHERE COL_CASE_INFO_ID=?", [data.cainfoid,data.caseid,id],
				function(tx,rs) {
					fnUpdateCaseAttrLocal(id, data, callback);
				},
			function(e,err){showMsgView("009");});
		});
}

function fnUpdateCaseAttrLocal(id, data, callback) {
	console.log(id);
	db.transaction(function (tx, rs) {
		tx.executeSql("UPDATE TBL_CASE_ATTRIBUTE SET COL_CASE_INFO_ID=? WHERE COL_CASE_INFO_ID=?", [data.cainfoid,id],
			function(tx,rs) {
				callback();
			},
		function(e,err){showMsgView("009");});
	});
}

function fnGetCaseInfoID (callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT C7100_CASE_INFO_ID AS ID FROM T7100_CASE_INFORMATION", [],
			function(tx,rs) {
				var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
					results.push(rs.rows.item(i));
				}
				callback(results);
		},
		function(e,err){showMsgView("009");});
	});
}

function fnDeleteCaseLocalTable (IDs, callback) {

	 if(typeof(IDs)!='object')
	 	IDs = [IDs];
	if(IDs.length>0) {
		var searchKey = new Array();
		for(var i=0;i<IDs.length;i++)
			searchKey.push("COL_CASE_INFO_ID = " + IDs[i]);
		var query = "DELETE FROM TBL_CASE_INFORMATION WHERE " + searchKey.join(' OR ');
		db.transaction(function (tx, rs) {
			tx.executeSql(query, [],
				function(tx,rs) {
					fnDeleteCaseAttrLocalTable(IDs, callback);
				},
			function(e,err){showMsgView("009");});
		});
	}
	else {
		callback();
	}
	
}

function fnDeleteCaseAttrLocalTable(IDs, callback) {

	if(typeof(IDs)!='object')
	 	IDs = [IDs];

	var searchKey = new Array();
	for(var i=0;i<IDs.length;i++)
		searchKey.push("COL_CASE_INFO_ID = " + IDs[i]);
	var query = "DELETE FROM TBL_CASE_ATTRIBUTE WHERE " + searchKey.join(' OR ');
	db.transaction(function (tx, rs) {
		tx.executeSql(query, [],
			function(tx,rs) {
			callback();
			},
		function(e,err){showMsgView("009");});
	});
}

function fnRemoveCaseDetails (id, callback) {
	console.log(id);
	db.transaction(function (tx, rs) {
		tx.executeSql("UPDATE T7100_CASE_INFORMATION SET C4010_VOID_FL = 'Y' WHERE C7100_CASE_INFO_ID=?", [id],
			function(tx,rs) {
//			fnRemoveCaseAttrDetails(id, callback);
			callback();
			},
		function(e,err){showMsgView("009");});
	});
}

function fnRemoveCaseAttrDetails(id, callback) {
	console.log(id);
	db.transaction(function (tx, rs) {
		tx.executeSql("DELETE FROM T7102_CASE_ATTRIBUTE WHERE C7100_CASE_INFO_ID=?", [id],
			function(tx,rs) {
			callback();
			},
		function(e,err){showMsgView("009");});
	});
}

function fnSaveCaseDetailsLocal(CaseData, status, callback) {
	console.log(CaseData)
	var now = new Date(CaseData.surdt+" "+CaseData.surstime+":00");	 
	var surtime = this.formatNumber(now.getUTCMonth()+1) +"/"+this.formatNumber(now.getUTCDate())+"/"+now.getUTCFullYear()+" "+this.formatNumber(now.getUTCHours())+":"+this.formatNumber(now.getUTCMinutes())+":"+this.formatNumber(now.getUTCSeconds())+" +00:00";	
	var user = localStorage.getItem('userID');
//	var surdt = (CaseData.surdt).split('/')[2] + "-" + (CaseData.surdt).split('/')[0] + "-" + (CaseData.surdt).split('/')[1];
	db.transaction(function (tx, rs) {
		tx.executeSql("INSERT INTO TBL_CASE_INFORMATION(COL_CASE_INFO_ID, COL_CASE_ID, COL_SURGERY_DATE, COL_SURGERY_TIME, C701_DISTRIBUTOR_ID, C703_SALES_REP_ID, C704_ACCOUNT_ID, TBL_PERSONAL_NOTES, C901_CASE_STATUS, TBL_CREATED_BY, C901_TYPE) VALUES (?,?,?,?,?,?,?,?,?,?,?) ", [CaseData.cainfoid, CaseData.caseid, CaseData.surdt, surtime, CaseData.did, CaseData.repid, CaseData.acid, CaseData.prnotes, status, user, '1006503'],
		function(tx,rs) {
			fnSaveCaseAttrLocal(CaseData.cainfoid, CaseData, callback);
		},
		function(e,err){showMsgView("009");});
	});
}



function fnGetCategories(GRP,callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT C901_CODE_ID AS ID, C901_CODE_NM AS Name FROM T901_CODE_LOOKUP WHERE C901_CODE_GRP=? AND C901_VOID_FL ='' AND C901_ACTIVE_FL = '1' ORDER BY C901_CODE_NM ASC", [GRP],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
		},
		function(){showMsgView("009");});
	});
}

function fnGetTimeZone(zoneID, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT C901_CODE_ID AS ID, C901_CODE_NM AS NAME FROM T901_CODE_LOOKUP WHERE C901_CODE_GRP='TMZONE' AND C901_CODE_ID=?", [zoneID],
			function(tx,rs) {
				var result= new Array();
				var timezone =  eval(rs.rows.item(0).NAME);
				result.push({"ZONE": "-" + timezone + ":00" });
				callback(result);
			},
		function(e,err){console.log(JSON.stringify(err))});
	});
	
}

function fnGetTimeZoneID(zone, callback) {
	
//	zone = zone.replace("-","-0").replace(":",".");
	
	
	
	
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT C901_CODE_ID AS ID, C901_CODE_NM AS Name, C902_CODE_NM_ALT AS ZONE FROM T901_CODE_LOOKUP WHERE C901_CODE_GRP='TMZONE' AND C902_CODE_NM_ALT=?", [zone],
			function(tx,rs) {
				var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
					results.push(rs.rows.item(i));
				}
				callback(results);
			},
		function(e,err){console.log(JSON.stringify(err))});
	});
}


function fnGetCaseCategorie(CaseInfoID, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT ATTRIBUTE_TYPE, VOID_FL, ATTRIBUTE_VALUE FROM VW_CASE_ATTRIBUTE  WHERE CASE_INFO_ID=? AND ATTRIBUTE_TYPE IN(SELECT c901_code_id FROM t901_code_lookup WHERE c901_code_grp='CASCAT')AND ATTRIBUTE_VALUE ='Y'and VOID_FL is ''", [CaseInfoID],
			function(tx,rs) {
				var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
					results.push(rs.rows.item(i));
				}
				callback(results);
			},
		function(e,err){console.log(JSON.stringify(err))});
	});
}

function fnGetDistributorInfo (did, callback) {	
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT  T701.C701_DISTRIBUTOR_ID AS ID,  T701.C701_DISTRIBUTOR_NAME AS Name FROM T701_DISTRIBUTOR T701 WHERE  T701.C701_ACTIVE_FL = 'Y' AND T701.C701_VOID_FL = '' AND  C701_DISTRIBUTOR_ID=?", [did],
			function(tx,rs) {
				var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
			},
		function(e,err){showMsgView("009");});
	});
}


function fnGetFieldSalesID (repid, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT C701_DISTRIBUTOR_ID AS ID FROM T703_SALES_REP WHERE C703_SALES_REP_ID=?", [repid],
			function(tx,rs) {
				var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
					results.push(rs.rows.item(i));
				}
				callback(results);
			},
		function(e,err){console.log(JSON.stringify(err))});
	});
}

function fnGetRep (url,txtSearch, callback) {
	txtSearch = txtSearch.split(' ');
	var searchTerm = [];
	for(var i=0; i<txtSearch.length;i++) {
	       searchTerm.push("Name LIKE '%"+txtSearch[i]+"%'");
	}

	var query = searchTerm.join(" AND ");
	
	query = "SELECT  T703.C703_SALES_REP_ID AS ID,  T703.C703_SALES_REP_NAME AS DispName, T703.C703_SALES_REP_NAME  || ' ' ||  T703.C703_SALES_REP_ID AS Name FROM T703_SALES_REP T703 WHERE  T703.C703_ACTIVE_FL = 'Y' AND T703.C703_VOID_FL = '' AND " + query;
	
	db.transaction(function (tx, rs) {
		tx.executeSql(query, [],
			function(tx,rs) {
				var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
			},
		function(e,err){showMsgView("009");});
	});
}


function fnGetCategorieName(id,GRP,callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT C901_CODE_ID AS ID, C901_CODE_NM AS Name FROM T901_CODE_LOOKUP WHERE C901_CODE_ID=? AND C901_CODE_GRP=? AND C901_VOID_FL ='' AND C901_ACTIVE_FL = '1' ORDER BY C901_CODE_SEQ_NO ASC", [id,GRP],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
		},
		function(){showMsgView("009");});
	});
}

function fnGetCalenderEventInfo(IDs,callback) {
	
	var date = new Date();
	var dd = ("0" + date.getDate()).slice(-2); var mm = ("0" + (date.getMonth() + 1)).slice(-2); var yyyy = date.getFullYear();
	 var todaydate = mm + "/" + dd + "/" + yyyy;
	
	
	
	var query = "SELECT VW_INFO.CASE_INFO_ID AS caseinid," +
       "CASE_ID AS caseid," +
       "SURGERY_DATE AS surdt," +
       "SURGERY_TIME AS surstime," +
       "DISTRIBUTOR_ID AS did," +
       "IFNULL(T701.C701_DISTRIBUTOR_NAME,'') AS DIST_NAME," +
       "ACCOUNT_ID AS acid," +
       "ACCOUNT_ID || ' ' || IFNULL(T704.C704_ACCOUNT_NM,'') AS ACCOUNT_NM," +
       "PERSONAL_NOTES AS prnotes," +
       "CASE_STATUS AS status," +
       "LOCATION," +
       "IFNULL(TEMP_ASSIGNEE.ASSIGNEE,'') AS ASSIGNEE," +
"IFNULL(TEMP_CATEGORY.CATEGORY,'No Category') AS CATEGORY, " +
"IFNULL(TEMP_DURATION.DURATION,'') AS DURATION, " +
"IFNULL(TEMP_ZONE.ZONE,'') AS ZONE, " +
"IFNULL(TEMP_SURGEON.SURGEON_NAME,'') AS SURGEON_NAME " +
"FROM VW_CASE_INFORMATION VW_INFO " +
"LEFT OUTER JOIN T701_DISTRIBUTOR T701 ON T701.C701_DISTRIBUTOR_ID = DISTRIBUTOR_ID " +
"LEFT OUTER JOIN T704_ACCOUNT T704 ON T704.C704_ACCOUNT_ID = ACCOUNT_ID LEFT OUTER JOIN " +
     "(SELECT VW_ATTR.CASE_INFO_ID,  group_concat(IFNULL(T703.C703_SALES_REP_NAME,''),'\n') AS ASSIGNEE " +
"FROM VW_CASE_ATTRIBUTE VW_ATTR " +
"LEFT OUTER JOIN T703_SALES_REP T703 ON T703.C703_SALES_REP_ID = ATTRIBUTE_VALUE " +
"WHERE ATTRIBUTE_TYPE = '103646' " +
"GROUP BY ATTRIBUTE_TYPE) TEMP_ASSIGNEE ON TEMP_ASSIGNEE.CASE_INFO_ID = VW_INFO.CASE_INFO_ID LEFT OUTER JOIN " +
     "(SELECT VW_ATTR.CASE_INFO_ID, " +
     " IFNULL(T901.C901_CODE_NM,'') AS CATEGORY " +
"FROM VW_CASE_ATTRIBUTE VW_ATTR " +
"LEFT OUTER JOIN T901_CODE_LOOKUP T901 ON T901.C901_CODE_ID = ATTRIBUTE_TYPE " +
"WHERE (ATTRIBUTE_TYPE = '11180' OR ATTRIBUTE_TYPE = '11181' OR ATTRIBUTE_TYPE = '11182' OR ATTRIBUTE_TYPE = '11183' OR ATTRIBUTE_TYPE = '11184' OR ATTRIBUTE_TYPE = '11185' OR ATTRIBUTE_TYPE = '11186') AND ATTRIBUTE_VALUE='Y' AND (VOID_FL IS NULL OR VOID_FL IS '')) TEMP_CATEGORY ON TEMP_CATEGORY.CASE_INFO_ID = VW_INFO.CASE_INFO_ID " +
" LEFT OUTER JOIN " +
     "(SELECT VW_ATTR.CASE_INFO_ID, " +
      "ATTRIBUTE_VALUE AS DURATION " +
"FROM VW_CASE_ATTRIBUTE VW_ATTR " +
"WHERE ATTRIBUTE_TYPE = '4000541' AND (VOID_FL IS NULL OR VOID_FL IS '')) TEMP_DURATION ON TEMP_DURATION.CASE_INFO_ID = VW_INFO.CASE_INFO_ID " +
" LEFT OUTER JOIN " +
"(SELECT VW_ATTR.CASE_INFO_ID, " +
      "ATTRIBUTE_VALUE AS SURGEON_NAME " +
"FROM VW_CASE_ATTRIBUTE VW_ATTR " +
"WHERE ATTRIBUTE_TYPE = '103640' AND (VOID_FL IS NULL OR VOID_FL IS '')) TEMP_SURGEON ON TEMP_SURGEON.CASE_INFO_ID = VW_INFO.CASE_INFO_ID " +
" LEFT OUTER JOIN " +
     "(SELECT VW_ATTR.CASE_INFO_ID, " +
      "ATTRIBUTE_VALUE AS ZONE " +
"FROM VW_CASE_ATTRIBUTE VW_ATTR " +
"WHERE ATTRIBUTE_TYPE = '4000534' AND (VOID_FL IS NULL OR VOID_FL IS '')) TEMP_ZONE ON TEMP_ZONE.CASE_INFO_ID = VW_INFO.CASE_INFO_ID WHERE (VW_INFO.CASE_STATUS = '11091' OR VW_INFO.CASE_STATUS = '11090' OR VW_INFO.CASE_STATUS = '11092') AND (VW_INFO.VOID_FL = '' OR VW_INFO.VOID_FL IS NULL)";

	if(IDs!='') {
		var serachKey = new Array();
		console.log("IDs.length>>>>>" + IDs.length);
		
		for(var i=0; i<IDs.length; i++) {
			serachKey.push("VW_INFO.CASE_INFO_ID = '" + IDs[i] + "'");
		}
		query += " AND (" + serachKey.join(" OR ") + ")";
	}
	else {
		query += " AND (VW_INFO.CASE_INFO_ID IN ( SELECT CASE_INFO_ID FROM VW_CASE_ATTRIBUTE WHERE ATTRIBUTE_TYPE = '103646' AND ATTRIBUTE_VALUE = '" + localStorage.getItem('userID') + "' AND (VOID_FL IS NULL OR VOID_FL = '')) OR VW_INFO.CREATED_BY = '" + localStorage.getItem('userID') + "')";
	}
	
	
	

console.log("Query>>>>>>");
console.log(query);
	db.transaction(function (tx, rs) {
		tx.executeSql(query, [],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				var fdate = new Date(todaydate);
				var tdate = new Date(rs.rows.item(i).surdt);
				var diffDays = tdate.getTime() - fdate.getTime(); 
				if(diffDays >= 0) { 
					results.push(rs.rows.item(i));
				}
			}
			callback(results);
		},
		function(e,err){
			console.log(JSON.stringify(err));
		    alert("009");});
	});
}


function fnGetCaseEventInfoFor(IDs, callback) {

	if(IDs.length>0) {
		var serachKey = new Array();
		console.log("IDs.length>>>>>" + IDs.length);
		
		for(var i=0; i<IDs.length; i++) {
			serachKey.push("VW_INFO.CASE_INFO_ID = '" + IDs[i] + "'");
		}
		
		// console.log("serachKey>>>>>");
		// console.log(serachKey);
		
		
		var query = "SELECT VW_INFO.CASE_INFO_ID AS caseinid," +
			       "CASE_ID AS caseid," +
			       "SURGERY_DATE AS surdt," +
			       "SURGERY_TIME AS surstime," +
			       "DISTRIBUTOR_ID AS did," +
			       "IFNULL(T701.C701_DISTRIBUTOR_NAME,'') AS DIST_NAME," +
			       "ACCOUNT_ID AS acid," +
			       "ACCOUNT_ID || ' ' || IFNULL(T704.C704_ACCOUNT_NM,'') AS ACCOUNT_NM," +
			       "PERSONAL_NOTES AS prnotes," +
			       "CASE_STATUS AS status," +
			       "LOCATION," +
			       "IFNULL(TEMP_ASSIGNEE.ASSIGNEE,'') AS ASSIGNEE," +
			"IFNULL(TEMP_CATEGORY.CATEGORY,'No Category') AS CATEGORY, " +
			"IFNULL(TEMP_DURATION.DURATION,'') AS DURATION, " +
			"IFNULL(TEMP_ZONE.ZONE,'') AS ZONE, " +
			"IFNULL(TEMP_SURGEON.SURGEON_NAME,'') AS SURGEON_NAME " +
			"FROM VW_CASE_INFORMATION VW_INFO " +
			"LEFT OUTER JOIN T701_DISTRIBUTOR T701 ON T701.C701_DISTRIBUTOR_ID = DISTRIBUTOR_ID " +
			"LEFT OUTER JOIN T704_ACCOUNT T704 ON T704.C704_ACCOUNT_ID = ACCOUNT_ID LEFT OUTER JOIN " +
			     "(SELECT VW_ATTR.CASE_INFO_ID,  group_concat(IFNULL(T703.C703_SALES_REP_NAME,''),'\n') AS ASSIGNEE " +
			"FROM VW_CASE_ATTRIBUTE VW_ATTR " +
			"LEFT OUTER JOIN T703_SALES_REP T703 ON T703.C703_SALES_REP_ID = ATTRIBUTE_VALUE " +
			"WHERE ATTRIBUTE_TYPE = '103646' " +
			"GROUP BY ATTRIBUTE_TYPE) TEMP_ASSIGNEE ON TEMP_ASSIGNEE.CASE_INFO_ID = VW_INFO.CASE_INFO_ID LEFT OUTER JOIN " +
			     "(SELECT VW_ATTR.CASE_INFO_ID, " +
			     " IFNULL(T901.C901_CODE_NM,'') AS CATEGORY " +
			"FROM VW_CASE_ATTRIBUTE VW_ATTR " +
			"LEFT OUTER JOIN T901_CODE_LOOKUP T901 ON T901.C901_CODE_ID = ATTRIBUTE_TYPE " +
			"WHERE (ATTRIBUTE_TYPE = '11180' OR ATTRIBUTE_TYPE = '11181' OR ATTRIBUTE_TYPE = '11182' OR ATTRIBUTE_TYPE = '11183' OR ATTRIBUTE_TYPE = '11184' OR ATTRIBUTE_TYPE = '11185' OR ATTRIBUTE_TYPE = '11186') AND ATTRIBUTE_VALUE='Y' AND (VOID_FL IS NULL OR VOID_FL IS '')) TEMP_CATEGORY ON TEMP_CATEGORY.CASE_INFO_ID = VW_INFO.CASE_INFO_ID " +
			" LEFT OUTER JOIN " +
			     "(SELECT VW_ATTR.CASE_INFO_ID, " +
			      "ATTRIBUTE_VALUE AS DURATION " +
			"FROM VW_CASE_ATTRIBUTE VW_ATTR " +
			"WHERE ATTRIBUTE_TYPE = '4000541' AND (VOID_FL IS NULL OR VOID_FL IS '')) TEMP_DURATION ON TEMP_DURATION.CASE_INFO_ID = VW_INFO.CASE_INFO_ID " +
			" LEFT OUTER JOIN " +
			"(SELECT VW_ATTR.CASE_INFO_ID, " +
			      "ATTRIBUTE_VALUE AS SURGEON_NAME " +
			"FROM VW_CASE_ATTRIBUTE VW_ATTR " +
			"WHERE ATTRIBUTE_TYPE = '103640' AND (VOID_FL IS NULL OR VOID_FL IS '')) TEMP_SURGEON ON TEMP_SURGEON.CASE_INFO_ID = VW_INFO.CASE_INFO_ID " +
			" LEFT OUTER JOIN " +
			     "(SELECT VW_ATTR.CASE_INFO_ID, " +
			      "ATTRIBUTE_VALUE AS ZONE " +
			"FROM VW_CASE_ATTRIBUTE VW_ATTR " +
			"WHERE ATTRIBUTE_TYPE = '4000534' AND (VOID_FL IS NULL OR VOID_FL IS '')) TEMP_ZONE ON TEMP_ZONE.CASE_INFO_ID = VW_INFO.CASE_INFO_ID WHERE VW_INFO.CASE_INFO_ID IN (SELECT CASE_INFO_ID FROM VW_CASE_ATTRIBUTE) AND (" + serachKey.join(" OR ") +")";

				
	//			query="select * from t7100_case_information";


			// console.log("Query>>>>>>");
			// console.log(query);
				db.transaction(function (tx, rs) {
					tx.executeSql(query, [],
							function(tx,rs) {
						var i=0, results = [];
						for(i=0;i<rs.rows.length;i++) {
							results.push(rs.rows.item(i));
						}
						callback(results);
					},
					function(e,err){
						console.log(JSON.stringify(err));
					    alert("009");});
				});
	}
	else
		callback([]);
	
}



function fnGetStateForAccount(stateid, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT C902_CODE_NM_ALT AS Name FROM T901_CODE_LOOKUP WHERE C901_CODE_ID=? AND C901_CODE_GRP= 'STATE' AND C901_VOID_FL ='' AND C901_ACTIVE_FL = '1' ", [stateid],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
		},
		function(){showMsgView("009");});
	});
}

function fnGetAccountState(acid, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT C704_BILL_STATE AS STATE FROM T704_ACCOUNT WHERE C704_ACCOUNT_ID=?", [acid],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
		},
		function(){showMsgView("009");});
	});
}

//function to fetch default usage lot display rule value
function fnGetExcSetFL (callback) {
    fnGetRuleValueByCompany("EXC_KIT_SET", "CONTAB", localStorage.getItem("cmpid"), function (data) {
        if (data.length > 0) {
            callback(data[0].RULEVALUE);
            $(".div-kit-detail-navigation .btn-detail").css("width","33.33%");
            $(".div-case-detail-navigation .btn-detail").css("width","100%");
            $(".case-tab-hide-Y").addClass("hide");
            $(".div-inv-detail-navigation").addClass("hide");
        } else {
            callback("N");
        }
    });
}

function fnGetAccountSearch(url, txtSearch, callback){  
    txtSearch=txtSearch.toLowerCase();
    var accList= GM.Global.AccList;    
    //Duplicate removal
//    accList = accList.reduce(function (item, a1) {  
//            var matches = item.filter(function (a2)  
//            { return a1.ID == a2.ID});  
//            if (matches.length == 0) {  
//                item.push(a1);  
//            }  
//            return item;  
//        }, []); 
    //end
    var accres=accList.filter(function(model) {
        var accnm = model.Name;
        var accid = model.ID;
        if((accnm.toLowerCase().includes(txtSearch) == true) || (accid.toLowerCase().includes(txtSearch) == true))
            return true;  
    });    
    callback(accres);
}