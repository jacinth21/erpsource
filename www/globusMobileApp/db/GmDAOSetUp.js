function fnGetLocale(name, callback) {
//changes removed for setting Dynamic locale(PMT-29507)
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT * FROM TBL_LOCALE WHERE LOCALE_FLAG = ? AND ACTIVE_FLAG=1", [name],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function () {
                showMsgView("009");
            });
    });
}

function fnGetLocaleItems(name, callback) {
    console.log(name);
//changes removed for setting Dynamic locale(PMT-29507)
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT * FROM TBL_LOCALE WHERE LOCALE_FLAG <> ? AND ACTIVE_FLAG=1", [name],
    //    tx.executeSql("SELECT * FROM TBL_LOCALE WHERE LOCALE_FLAG <> ? ", [name],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function () {
                showMsgView("009");
            });
    });
}

function fnGetUpdates(callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT COUNT(*) AS Length FROM T2001_MASTER_DATA_UPDATES", [],
            function (tx, rs) {
                callback(rs.rows.item(0).Length);
            }, null);

    });
}


function fnGetRequiredUpdates(callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT GROUP_CONCAT(C2001_REF_ID,',') AS ID, C901_REF_TYPE AS TYPE FROM T2001_MASTER_DATA_UPDATES GROUP BY C901_REF_TYPE", [],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            }, null);

    });
}

function fnGetUpdatedSystem(callback) {
    var i = 0,
        results = [],
        forNewSystem = 0,
        forNewSystemAttrib = 0;
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT T207_SET_MASTER.C207_SET_ID AS ID, T207_SET_MASTER.C207_SET_NM AS Name FROM T2001_MASTER_DATA_UPDATES T2001 JOIN T207_SET_MASTER ON T2001.C2001_REF_ID = T207_SET_MASTER.C207_SET_ID WHERE T2001.C901_REF_TYPE = '103108' AND T2001.C901_ACTION != '103122'", [],
            function (tx, rs) {
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                    forNewSystem++;
                }

                db.transaction(function (tx, rs) {
                    tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '103108'", [],
                        function (tx, rs) {
                            var diff = parseInt(rs.rows.item(0).Name) - forNewSystem;
                            if (diff > 0)
                                results.push({
                                    "Name": diff + " New System(s)"
                                });

                            db.transaction(function (tx, rs) {
                                tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T207_SET_MASTER.C207_SET_NM || ' Attribute' AS Name FROM T207_SET_MASTER, T2001_MASTER_DATA_UPDATES T2001 WHERE T2001.C2001_REF_ID = T207_SET_MASTER.C207_SET_ID AND T2001.C901_REF_TYPE = '4000416'", [],
                                    function (tx, rs) {
                                        for (i = 0; i < rs.rows.length; i++) {
                                            results.push(rs.rows.item(i));
                                            forNewSystemAttrib++;
                                        }
                                        db.transaction(function (tx, rs) {
                                            tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '4000416'", [],
                                                function (tx, rs) {
                                                    var diff = parseInt(rs.rows.item(0).Name) - forNewSystemAttrib;
                                                    if (diff > 0)
                                                        results.push({
                                                            "Name": diff + " New System Attributes"
                                                        });
                                                    callback(results);
                                                }, null);
                                        });
                                    }, null);
                            });

                        }, null);
                });
            }, null);
    });
}

function fnGetUpdatedSet(callback) {
    var i = 0,
        results = [],
        forNewSets = 0,
        forNewSetParts = 0,
        forNewLoanerPoolSets = 0;
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T2001.C2001_REF_ID || ' ' || T207_SET_MASTER.C207_SET_NM AS Name, T2001.C901_ACTION FROM T2001_MASTER_DATA_UPDATES T2001, T207_SET_MASTER WHERE T2001.C2001_REF_ID = T207_SET_MASTER.C207_SET_ID AND T2001.C901_REF_TYPE = '103111' AND T2001.C901_ACTION != '103122'", [],
            function (tx, rs) {
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                    forNewSets++;
                }

                db.transaction(function (tx, rs) {
                    tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '103111'", [],
                        function (tx, rs) {
                            var diff = parseInt(rs.rows.item(0).Name) - forNewSets;
                            if (diff > 0)
                                results.push({
                                    "Name": diff + " New Set(s)"
                                });

                            db.transaction(function (tx, rs) {
                                tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T207.C207_SET_ID || ' - Part Mapping' AS Name FROM T207_SET_MASTER T207, T2001_MASTER_DATA_UPDATES T2001 WHERE T207.C207_SET_ID = T2001.C2001_REF_ID AND T2001.C901_REF_TYPE = '4000409'", [],
                                    function (tx, rs) {
                                        for (i = 0; i < rs.rows.length; i++) {
                                            results.push(rs.rows.item(i));
                                            forNewSetParts++;
                                        }
                                        db.transaction(function (tx, rs) {
                                            tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '4000409'", [],
                                                function (tx, rs) {
                                                    var diff = parseInt(rs.rows.item(0).Name) - forNewSetParts;
                                                    if (diff > 0)
                                                        results.push({
                                                            "Name": diff + " New Set Part Mapping"
                                                        });
                                                    db.transaction(function (tx, rs) {
                                                        tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T207_SET_MASTER.C207_SET_NM || ' Attribute' AS Name FROM T207_SET_MASTER, T2001_MASTER_DATA_UPDATES T2001 WHERE T2001.C2001_REF_ID = T207_SET_MASTER.C207_SET_ID AND T2001.C901_REF_TYPE = '4000736'", [],
                                                            function (tx, rs) {
                                                                for (i = 0; i < rs.rows.length; i++) {
                                                                    results.push(rs.rows.item(i));
                                                                    forNewLoanerPoolSets++;
                                                                }
                                                                db.transaction(function (tx, rs) {
                                                                    tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '4000736'", [],
                                                                        function (tx, rs) {
                                                                            var diff = parseInt(rs.rows.item(0).Name) - forNewLoanerPoolSets;
                                                                            if (diff > 0)
                                                                                results.push({
                                                                                    "Name": diff + " New Loaner Sets"
                                                                                });
                                                                            callback(results);
                                                                        }, null);
                                                                });
                                                            }, null);
                                                    });
                                                }, null);
                                        });

                                    }, null);
                            });

                        }, null);

                });

            }, null);
    });
}

function fnGetUpdatedPart(callback) {
    var i = 0,
        results = [],
        forNewPartAttr = 0;
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T205.C205_PART_NUMBER_ID || ' Part' AS Name, T2001.C901_ACTION FROM T2001_MASTER_DATA_UPDATES T2001, T205_PART_NUMBER T205 WHERE T2001.C2001_REF_ID = T205.C205_PART_NUMBER_ID AND T2001.C901_REF_TYPE = '4000411' AND C901_ACTION != '103122'", [],
            function (tx, rs) {
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                db.transaction(function (tx, rs) {
                    tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '4000411'", [],
                        function (tx, rs) {
                            var diff = parseInt(rs.rows.item(0).Name) - results.length;
                            if (diff > 0)
                                results.push({
                                    "Name": diff + " New Part(s)"
                                });
                            db.transaction(function (tx, rs) {
                                tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T205.C205_PART_NUMBER_ID || ' Attribute' AS Name, T2001.C901_ACTION FROM T2001_MASTER_DATA_UPDATES T2001, T205_PART_NUMBER T205 WHERE T2001.C2001_REF_ID = T205.C205_PART_NUMBER_ID AND T2001.C901_REF_TYPE = '4000827' AND C901_ACTION != '103122'", [],
                                    function (tx, rs) {
                                        for (i = 0; i < rs.rows.length; i++) {
                                            results.push(rs.rows.item(i));
                                            forNewPartAttr++;
                                        }
                                        db.transaction(function (tx, rs) {
                                            tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '4000827'", [],
                                                function (tx, rs) {
                                                    var diff = parseInt(rs.rows.item(0).Name) - forNewPartAttr;
                                                    if (diff > 0)
                                                        results.push({
                                                            "Name": diff + " New PartAttr"
                                                        });
                                                    callback(results);
                                                }, null);
                                        });
                                    }, null);
                            });
                        }, null);
                });
            }, null);
    });
}


function fnGetUpdatedGroup(callback) {
    var i = 0,
        results = [],
        forNewGroups = 0,
        forNewGroupParts = 0;
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T4010_GROUP.C4010_GROUP_NM AS Name FROM T2001_MASTER_DATA_UPDATES T2001 JOIN T4010_GROUP ON T4010_GROUP.C4010_GROUP_ID = T2001.C2001_REF_ID WHERE T2001.C901_REF_TYPE = '103110' AND C901_ACTION != '103122'", [],
            function (tx, rs) {
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                    forNewGroups++;
                }

                db.transaction(function (tx, rs) {
                    tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '103110'", [],
                        function (tx, rs) {
                            var diff = parseInt(rs.rows.item(0).Name) - forNewGroups;
                            if (diff > 0)
                                results.push({
                                    "Name": diff + " New Group(s)"
                                });

                            db.transaction(function (tx, rs) {
                                tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T4010.C4010_GROUP_NM || ' - Part Mapping' AS Name FROM T4010_GROUP T4010, T2001_MASTER_DATA_UPDATES T2001 WHERE T4010.C4010_GROUP_ID = T2001.C2001_REF_ID AND T2001.C901_REF_TYPE = '4000408'", [],
                                    function (tx, rs) {
                                        for (i = 0; i < rs.rows.length; i++) {
                                            results.push(rs.rows.item(i));
                                            forNewGroupParts++;
                                        }
                                        db.transaction(function (tx, rs) {
                                            tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '4000408'", [],
                                                function (tx, rs) {
                                                    var diff = parseInt(rs.rows.item(0).Name) - forNewGroupParts;
                                                    if (diff > 0)
                                                        results.push({
                                                            "Name": diff + " New Group Part Mapping"
                                                        });
                                                    callback(results);
                                                }, null);
                                        });
                                    }, null);
                            });

                        }, null);
                });

            }, null);

    });
}

function fnGetUpdatedFile(callback) {
    var i = 0,
        results = [],
        forNewFiles = 0,
        forFileKeywords = 0;
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T903.C901_FILE_TITLE AS Name FROM T2001_MASTER_DATA_UPDATES T2001 JOIN T903_UPLOAD_FILE_LIST T903 ON T903.C903_UPLOAD_FILE_LIST = T2001.C2001_REF_ID WHERE T2001.C901_REF_TYPE = '4000410' AND C901_ACTION != '103122'", [],
            function (tx, rs) {
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                    forNewFiles++;
                }
                db.transaction(function (tx, rs) {
                    tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '4000410'", [],
                        function (tx, rs) {
                            var diff = parseInt(rs.rows.item(0).Name) - forNewFiles;
                            if (diff > 0)
                                results.push({
                                    "Name": diff + " New File(s)"
                                });
                            db.transaction(function (tx, rs) {
                                tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '4000442'", [],
                                    function (tx, rs) {
                                        if (rs.rows.item(0).Name > 0)
                                            results.push({
                                                "Name": rs.rows.item(0).Name + " Keywords"
                                            });
                                        callback(results);
                                    }, null);
                            });

                        }, null);
                });

            }, null);

    });
}


function fnGetUpdatedContact(callback) {
    var i = 0,
        results = [],
        forNewParty = 0,
        forNewContact = 0;
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T101.C101_FIRST_NM || ' ' || T101.C101_LAST_NM AS Name FROM T2001_MASTER_DATA_UPDATES T2001 JOIN T101_PARTY T101 ON T101.C101_PARTY_ID = T2001.C2001_REF_ID WHERE T2001.C901_REF_TYPE = '4000445' AND C901_ACTION != '103122'", [],
            function (tx, rs) {
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                    forNewParty++;
                }

                db.transaction(function (tx, rs) {
                    tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '4000445'", [],
                        function (tx, rs) {
                            var diff = parseInt(rs.rows.item(0).Name) - forNewParty;
                            if (diff > 0)
                                results.push({
                                    "Name": diff + " New Contact(s)"
                                });

                            db.transaction(function (tx, rs) {
                                tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, United.Name AS Name FROM T2001_MASTER_DATA_UPDATES T2001 JOIN (SELECT T107.C107_CONTACT_ID AS ID, T101.C101_FIRST_NM || ' ' || T101.C101_LAST_NM AS Name FROM T107_CONTACT T107, T101_PARTY T101 WHERE T107.C101_PARTY_ID = T101.C101_PARTY_ID) United ON United.ID = T2001.C2001_REF_ID WHERE T2001.C901_REF_TYPE = '4000448'", [],
                                    function (tx, rs) {
                                        for (i = 0; i < rs.rows.length; i++) {
                                            results.push(rs.rows.item(i));
                                            forNewContact++;
                                        }
                                        db.transaction(function (tx, rs) {
                                            tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '4000448'", [],
                                                function (tx, rs) {
                                                    var diff = parseInt(rs.rows.item(0).Name) - forNewContact;
                                                    if (diff > 0)
                                                        results.push({
                                                            "Name": diff + " New Contact Info"
                                                        });
                                                    callback(results);
                                                }, null);
                                        });
                                    }, null);
                            });

                        }, null);
                });

            }, null);

    });
}

function fnGetUpdatedAccount(callback) {
    var i = 0,
        results = [],
        forNewAccounts = 0,
        forNewGPOAccounts = 0;
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT  T2001.C2001_REF_ID, T704_ACCOUNT.C704_ACCOUNT_NM AS Name, T2001.C901_ACTION FROM T2001_MASTER_DATA_UPDATES T2001, T704_ACCOUNT WHERE  T2001.C2001_REF_ID = T704_ACCOUNT.C704_ACCOUNT_ID AND T2001.C901_REF_TYPE = '4000519' AND C901_ACTION != '103122'", [],
            function (tx, rs) {
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                    forNewAccounts++;
                }

                db.transaction(function (tx, rs) {
                    tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '4000519'", [],
                        function (tx, rs) {

                            var diff = parseInt(rs.rows.item(0).Name) - forNewAccounts;

                            if (diff > 0)
                                results.push({
                                    "Name": diff + " New Account(s)"
                                });
                            db.transaction(function (tx, rs) {
                                tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T740.C740_GPO_ACCOUNT_MAP_ID AS Name FROM T740_GPO_ACCOUNT_MAPPING T740, T2001_MASTER_DATA_UPDATES T2001 WHERE T2001.C2001_REF_ID = T740.C740_GPO_ACCOUNT_MAP_ID AND T2001.C901_REF_TYPE = '4000530'", [],
                                    function (tx, rs) {
                                        for (i = 0; i < rs.rows.length; i++) {
                                            forNewGPOAccounts++;
                                        }
                                        if (rs.rows.length > 0)
                                            results.push({
                                                "Name": rs.rows.length + " GPO Account Mapping"
                                            })
                                        db.transaction(function (tx, rs) {
                                            tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '4000530'", [],
                                                function (tx, rs) {
                                                    var diff = parseInt(rs.rows.item(0).Name) - forNewGPOAccounts;
                                                    if (diff > 0)
                                                        results.push({
                                                            "Name": diff + " New GPO Account Mapping"
                                                        });
                                                    callback(results);
                                                }, null);
                                        });

                                    }, null);
                            });

                        }, null);

                });

            }, null);
    });
}


function fnGetUpdatedSalesRep(callback) {
    var i = 0,
        results = [];
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T703.C703_SALES_REP_NAME AS Name, T2001.C901_ACTION FROM T2001_MASTER_DATA_UPDATES T2001, T703_SALES_REP T703 WHERE T2001.C2001_REF_ID = T703.C703_SALES_REP_ID AND T2001.C901_REF_TYPE = '4000520' AND C901_ACTION != '103122'", [],
            function (tx, rs) {
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                db.transaction(function (tx, rs) {
                    tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '4000520'", [],
                        function (tx, rs) {
                            var diff = parseInt(rs.rows.item(0).Name) - results.length;
                            if (diff > 0)
                                results.push({
                                    "Name": diff + " New SalesRep(s)"
                                });
                            callback(results);
                        }, null);
                });
            }, null);
    });
}

// fn to get the info of updated Associate Sales Rep Records
// 4000629 is the codeID of Sync for Associate Sales Rep 
function fnGetUpdatedASSalesRep(callback) {
    var i = 0,
        results = [];
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T704A.C703_SALES_REP_ID AS Name, T2001.C901_ACTION FROM T2001_MASTER_DATA_UPDATES T2001, T704a_ACCOUNT_REP_MAPPING T704a WHERE T2001.C2001_REF_ID = T704A.C704A_ACCOUNT_REP_MAP_ID AND T2001.C901_REF_TYPE = '4000629' AND C901_ACTION != '103122'", [],
            function (tx, rs) {
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                db.transaction(function (tx, rs) {
                    tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '4000629'", [],
                        function (tx, rs) {
                            var diff = parseInt(rs.rows.item(0).Name) - results.length;
                            if (diff > 0)
                                results.push({
                                    "Name": diff + " New Associate SalesRep(s)"
                                });
                            callback(results);
                        }, null);
                });
            }, null);
    });
}

function fnGetUpdatedDistributor(callback) {
    var i = 0,
        results = [];
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T701.C701_DISTRIBUTOR_NAME AS Name, T2001.C901_ACTION FROM T2001_MASTER_DATA_UPDATES T2001, T701_DISTRIBUTOR T701 WHERE T2001.C2001_REF_ID = T701.C701_DISTRIBUTOR_ID AND T2001.C901_REF_TYPE = '4000521' AND C901_ACTION != '103122'", [],
            function (tx, rs) {
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                db.transaction(function (tx, rs) {
                    tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '4000521'", [],
                        function (tx, rs) {
                            var diff = parseInt(rs.rows.item(0).Name) - results.length;
                            if (diff > 0)
                                results.push({
                                    "Name": diff + " New Distributor(s)"
                                });
                            callback(results);
                        }, null);
                });
            }, null);
    });
}

function fnGetUpdatedPrice(callback) {
    var i = 0,
        results = [],
        forDeletedGPOS = 0;
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T704_ACCOUNT.C704_ACCOUNT_NM AS Name, T2001.C901_ACTION FROM T2001_MASTER_DATA_UPDATES T2001, T704_ACCOUNT WHERE T2001.C2001_REF_ID = T704_ACCOUNT.C704_ACCOUNT_ID AND T2001.C901_REF_TYPE = '4000526'", [],
            function (tx, rs) {
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                db.transaction(function (tx, rs) {
                    tx.executeSql("SELECT TBL.ACCID AS ID, TBL.NAME || ' - GPO Price' AS Name FROM (SELECT T704.C704_ACCOUNT_ID AS ACCID, T704.C704_ACCOUNT_NM AS NAME, T740.C101_PARTY_ID AS ID FROM T704_ACCOUNT T704 LEFT JOIN T740_GPO_ACCOUNT_MAPPING T740 ON T704.C704_ACCOUNT_ID = T740.C704_ACCOUNT_ID AND SYNCED = 'Y' ) TBL, T2001_MASTER_DATA_UPDATES T2001 WHERE T2001.C2001_REF_ID = TBL.ID AND T2001.C901_REF_TYPE = '4000524'", [],
                        function (tx, rs) {
                            for (i = 0; i < rs.rows.length; i++) {
                                forDeletedGPOS++;
                                results.push(rs.rows.item(i));
                            }
                            db.transaction(function (tx, rs) {
                                tx.executeSql("SELECT TBL.ACCID AS ID, TBL.NAME || ' - GPO Price' AS Name FROM (SELECT T704.C704_ACCOUNT_ID AS ACCID, T704.C704_ACCOUNT_NM AS NAME, T740.C101_PARTY_ID AS ID FROM T704_ACCOUNT T704 LEFT JOIN T740_GPO_ACCOUNT_MAPPING T740 ON T704.C704_ACCOUNT_ID = T740.C704_ACCOUNT_ID) TBL, T2001_MASTER_DATA_UPDATES T2001 WHERE T2001.C2001_REF_ID = TBL.ID AND T2001.C901_REF_TYPE = '4000524'", [],
                                    function (tx, rs) {
                                        var deletedGPO = rs.rows.length - forDeletedGPOS
                                        if (deletedGPO > 0)
                                            results.push({
                                                "ID": "",
                                                "Name": deletedGPO + "(s) GPO Price"
                                            });
                                        if (results.length > 0) {
                                            fnGetSyncedAccountPriceNameList(function (data) {
                                                if (data.length > 0) {
                                                    for (var i = 0; i < results.length; i++) {
                                                        if (i == 0)
                                                            results[i].ID = _.pluck(data, 'ID').join('');
                                                        else
                                                            results[i].ID = "";
                                                    }
                                                    callback(results);
                                                } else
                                                    callback(data);
                                            });
                                        } else {
                                            callback(results);
                                        }

                                    }, null);
                            });
                        }, null);
                });
            }, null);
    });
}

function fnGetUpdatedAddress(callback) {
    var i = 0,
        results = [];
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T106.C106_ADDRESS_ID AS Name, T2001.C901_ACTION FROM T2001_MASTER_DATA_UPDATES T2001, T106_ADDRESS T106 WHERE T2001.C2001_REF_ID = T106.C106_ADDRESS_ID AND T2001.C901_REF_TYPE = '4000522' AND C901_ACTION != '103122'", [],
            function (tx, rs) {
                if (rs.rows.length > 0)
                    results.push({
                        "Name": rs.rows.length + " Address(s)"
                    });
                db.transaction(function (tx, rs) {
                    tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '4000522'", [],
                        function (tx, rs) {
                            var diff = parseInt(rs.rows.item(0).Name) - results.length;
                            if (diff > 0)
                                results.push({
                                    "Name": diff + " New Address(s)"
                                });
                            callback(results);
                        }, null);
                });
            }, null);
    });
}

function fnGetUpdatedApp(callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT COUNT(*) As Name FROM T2001_MASTER_DATA_UPDATES WHERE C901_REF_TYPE = '103107' OR C901_REF_TYPE = '4000443' OR C901_REF_TYPE = '7000443' OR C901_REF_TYPE = '26230802' OR C901_REF_TYPE = '26230804' ", [],
            function (tx, rs) {
                callback(rs.rows.item(0).Name);
            }, null);

    });
}

function fnClearUpdates(updated, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("DELETE FROM T2001_MASTER_DATA_UPDATES WHERE C901_REF_TYPE LIKE ?", ["%" + updated + "%"],
            function (tx, rs) {
                callback("success");
            }, null);

    });
}

function fnSetLangId() {
    var current = localStorage.getItem('deviceLng');
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT LOCALE_ID FROM TBL_LOCALE WHERE LOCALE_CODE = ?", [current],
            function (tx, rs) {
                langid = rs.rows.item(0).LOCALE_ID;
            }, null);

    });
}

function fnDeleteGroupPartData(GrpID, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("DELETE FROM T4011_GROUP_DETAIL WHERE C4010_GROUP_ID = ? ", [GrpID],
            function (tx, rs) {
                callback();
            },
            function(tx, err) {
                showAppError(err, "GmDAOSetUp:fnDeleteGroupPartData()");
            });
    });
}

function fnInsertLastsync(syncname, now) {
    db.transaction(function (tx, rs) {
        tx.executeSql("INSERT OR REPLACE INTO TBL_LASTSYNC (SYNCNAME,LASTSYNC) VALUES (?,?) ", [syncname.toUpperCase(), now],
            function (tx, rs) {}, null);

    });
}

function fnGetLastSyncDate(callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT * FROM TBL_LASTSYNC", [],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function(tx, err) {
                showAppError(err, "GmDAOSetUp:fnGetLastSyncDate()");
            });
    });
}

function fnGetImageof(groupID, itemID, callback) {

    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT C903_UPLOAD_FILE_LIST AS ID, C903_FILE_NAME AS Name, T903.C903_FILE_PATH AS Path, C901_FILE_TITLE as Title FROM T903_UPLOAD_FILE_LIST T903 WHERE T903.C901_REF_TYPE='103112' AND C903_DELETE_FL='' AND C901_REF_GRP=? AND C903_REF_ID=? AND T903.C903_FILE_SEQ_NO <> '' ORDER BY T903.C903_FILE_SEQ_NO", [groupID, itemID],
            function (tx, rs) {
                if ((groupID == "103090") && (rs.rows.length == 0)) {
                    fnGetImageofPart(itemID, callback);
                } else {
                    var i = 0,
                        results = [];
                    for (i = 0; i < rs.rows.length; i++) {
                        results.push(rs.rows.item(i));
                    }
                    callback(results);
                }
            },
            function (e) {
                showMsgView("009");
            });
    });


}
//Used to get the success count of no of files that are Synced
function fnGetCount(name, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT SUM((CASE C901_SYNC_status WHEN '103121' THEN 1 ELSE 0 END)) AS SUCCESS_CNT FROM T9151_DEVICE_SYNC_DTL WHERE SYNC_GRP = ? ", [name],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function(tx, err) {
                showAppError(err, "GmDAOSetUp:fnGetCount()");
            });
    });

}
//Used to get the no of entries in T9151_DEVICE_SYNC_DTL
function fnGetTableCount(callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("select count(*) as no from T9151_DEVICE_SYNC_DTL", [],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            }, null);
    });

}

//Get local count and DB count data from T9151_DEVICE_SYNC_DTL
function fnPopup(name, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT C901_CODE_NM NAME, C901_SYNC_STATUS STATUS, C9151_SYNC_COUNT LOCALCNT, C9151_SERVER_COUNT DBCNT FROM T901_CODE_LOOKUP T901 , T9151_DEVICE_SYNC_DTL T9151 WHERE C901_CODE_ID = C901_SYNC_TYPE AND SYNC_GRP = ? order by SYNC_SEQ ASC ", [name],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            }, null);
    });

}



function fnGetImageofPart(itemID, callback) {

    var query = "SELECT " + itemID + " AS PARTID, " +
        "C903_UPLOAD_FILE_LIST AS ID, " +
        "C903_FILE_NAME AS Name, " +
        "T903.C903_FILE_PATH AS Path, " +
        "C901_FILE_TITLE as Title " +
        "FROM T903_UPLOAD_FILE_LIST T903 " +
        "WHERE T903.C901_REF_TYPE='103112' " +
        "AND C903_DELETE_FL='' " +
        //				"AND C901_REF_GRP=? " +
        "AND (" +
        "C903_REF_ID= '" + itemID + "'" +
        " OR C903_REF_ID IN " +
        " (" +
        "SELECT T4011.C4010_GROUP_ID " +
        "FROM T4011_GROUP_DETAIL T4011, " +
        "T4010_GROUP T4010 " +
        "WHERE T4011.C205_PART_NUMBER_ID = '" + itemID + "'" +
        " AND T4011.C901_PART_PRICING_TYPE = '52080' " +
        "AND T4011.C4010_GROUP_ID = T4010.C4010_GROUP_ID " +
        "AND T4010.C901_PRICED_TYPE = '52110' " +
        "AND T4010.C4010_VOID_FL = '' " +
        " )" +
        ") AND T903.C903_FILE_SEQ_NO <> '' " +
        "ORDER BY T903.C901_REF_GRP, T903.C903_FILE_SEQ_NO";

    db.transaction(function (tx, rs) {
        tx.executeSql(query, [],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function (tx, err) {
                console.log(JSON.stringify(err));
            });
    });
}


function fnGetAllFiles(groupID, itemID, callback) {
    db.transaction(function (tx, rs) {
        var query = "SELECT T903.C903_UPLOAD_FILE_LIST AS ID, T903.C903_FILE_NAME AS Name, T903.C901_FILE_TITLE as Title, T903.C903_FILE_PATH AS Path, T903.C903_FILE_SIZE AS Size FROM T903_UPLOAD_FILE_LIST T903 WHERE T903.C903_REF_ID= '" + itemID + "' AND T903.C901_REF_GRP = '" + groupID + "' AND T903.C903_DELETE_FL = '';";

        tx.executeSql(query, [],
            function (tx, rs) {
                var i = 0;
                results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function (e, err) {
                showNativeAlert('Cannot Download Files without Syncing File Data', 'Sync Priority Error!', 'OK');
                callback('error');
            });
    });
}

function fnGetFileSize(arrTotalFiles, callback) {
    var query = "",
        conditionalTerm = [];
    for (var i = 0; i < arrTotalFiles.length; i++) {
        conditionalTerm.push("C903_UPLOAD_FILE_LIST = " + arrTotalFiles[i].ID);
    }
    query = "SELECT SUM(C903_FILE_SIZE) AS SIZE FROM T903_UPLOAD_FILE_LIST WHERE " + conditionalTerm.join(" OR ");
    db.transaction(function (tx, rs) {
        tx.executeSql(query, [],
            function (tx, rs) {
                callback(rs.rows.item(0).SIZE);
            },
            function (e, err) {
                showNativeAlert('Cannot Download Files without Syncing File Data', 'Sync Priority Error!', 'OK');
                callback('error');
            });
    });
}

function fnClearDeviceContacts(callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("DELETE FROM TBL_DEVICE_CONTACT", [],
            function (tx, rs) {
                callback();
            },
            function (e, err) {
                alert('error');
                console.log(JSON.stringify(err));
            });
    });
}


function fnInsertDeviceContact(JSONData) {
    db.transaction(function (tx, rs) {

        for (var j = 0; j < JSONData.length; j++) {
            var data = JSONData[j];
            tx.executeSql("INSERT OR REPLACE INTO  TBL_DEVICE_CONTACT(FIRST_NM, LAST_NM, CONTACT_VALUE) VALUES (?,?,?)", [data.FirstName, data.LastName, data.EMail],
                function (tx, rs) {},
                function (e, err) {
                    alert('error');
                    console.log(JSON.stringify(err));
                });
        }
    });
}


function fnGetShareRules(callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT C906_RULE_ID AS ID, C906_RULE_VALUE AS VALUE FROM T906_RULES WHERE C906_RULE_GRP_ID = 'SHAREGRP'", [],
            function (tx, rs) {
                var i = 0;
                results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function(e, err) {
                showAppError(err, "GmDAOSetUp:fnGetShareRules()");
            });
    });
}

function fnGetSyncInfo(syncType, callback) {
    db.transaction(function(tx, rs) {
        tx.executeSql("SELECT * FROM TBL_LASTSYNC WHERE SYNCNAME=?", [syncType],
            function(tx, rs) {
                if (rs.rows.length > 0)
                    callback(0);
                else
                    callback(1);
            },
            function(e, err) {
                showAppError(err, "GmDAOSetUp:fnGetSyncInfo()");
            });
    });
}

function fnGetDashboardGroups(callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT DISTINCT GROUP_ID, GROUP_NAME FROM TBL_DASHBOARD_SEQ ORDER BY GROUP_NAME", [],
            function (tx, rs) {
                var i = 0;
                results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function (e, err) {
                console.log(JSON.stringify(err));
            });
    });
}

function fnInsertDashboardSeq(grpID, grpName, colName, seqNo, shrtName, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("INSERT OR REPLACE INTO TBL_DASHBOARD_SEQ (GROUP_ID, GROUP_NAME, COLUMN_NAME, SEQ_NO, DEFAULT_SEQ_NO, SHORT_NM) VALUES (?,?,?,?,?,?)", [grpID, grpName, colName, seqNo, seqNo, shrtName],
            function (tx, rs) {
                callback();
            },
            function (e, err) {
                console.log(JSON.stringify(err));
            });
    });

}


function fnGetDashColumnsFor(id, nm, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT SEQ_ID, COLUMN_NAME, SEQ_NO, SHORT_NM FROM TBL_DASHBOARD_SEQ WHERE GROUP_ID=? OR GROUP_NAME=? ORDER BY SEQ_NO", [id, nm],
            function (tx, rs) {
                var i = 0;
                results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function (e, err) {
                console.log(JSON.stringify(err));
            });
    });
}

function fnUpdateDashboardSeq(seqID, seqNO, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("UPDATE TBL_DASHBOARD_SEQ SET SEQ_NO = ? WHERE SEQ_ID LIKE ?", [seqNO, seqID],
            function (tx, rs) {
                callback();
            },
            function (tx, err) {
                console.log(JSON.stringify(err));

            });
    });
}

function fnResetDashSeq(grpID, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("UPDATE TBL_DASHBOARD_SEQ SET SEQ_NO = DEFAULT_SEQ_NO WHERE GROUP_ID LIKE ?", [grpID],
            function (tx, rs) {
                callback();
            },
            function (tx, err) {
                console.log(JSON.stringify(err));

            });
    });
}

function fnDeleteOrderDashColumns() {
    db.transaction(function (tx, rs) {
        tx.executeSql("DELETE FROM TBL_DASHBOARD_SEQ ", [],
            function (tx, err) {
                console.log(JSON.stringify(err));
            });
    });
}

function fnUpdatePendingDOList() {
    var userId = localStorage.getItem("userID");
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT DOID AS ID FROM TBL_DO WHERE STATUS='Pending CS Confirmation' AND USER = ?", [userId],
            function (tx, rs) {
                var i = 0;
                arrPendingDOList = new Array();
                for (i = 0; i < rs.rows.length; i++) {
                    arrPendingDOList.push(rs.rows.item(i).ID.toString());
                }
            },
            function (tx, err) {
                console.log(JSON.stringify(err));
            });
    });
}


function fnGetUpdatedDO(callback) {

    var arrDOIDs = new Array();
    for (var i = 0; i < arrPendingDOList.length; i++) {
        arrDOIDs.push("'" + arrPendingDOList[i] + "'");
    }

    var query = "SELECT DOID AS ID, DOID || ' - ' || STATUS AS Name FROM TBL_DO WHERE DOID IN (" + arrDOIDs.join(',') + ") AND STATUS<> 'Pending CS Confirmation'";
    db.transaction(function(tx, rs) {
        tx.executeSql(query, [],
            function (tx, rs) {
                var i = 0,
                    results = new Array();
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                // alert(JSON.stringify(results));
                callback(results);
            },
            function (tx, err) {
                console.log(JSON.stringify(err));
            });
    });
}


function fnUpdateRejectReason(DO, Reason) {
    db.transaction(function(tx, rs) {
        tx.executeSql("UPDATE TBL_DO SET STATUS = 'Rejected', REASON = ? WHERE DOID = ?", [Reason, DO],
            function (tx, rs) {},
            function (tx, err) {
                console.log(JSON.stringify(err));
            });
    });
}

function fnGetUpdatedUser(callback) {
    var i = 0,
        results = [];
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T101.C101_USER_F_NAME || ' ' || T101.C101_USER_L_NAME AS Name, T2001.C901_ACTION FROM T2001_MASTER_DATA_UPDATES T2001, T101_USER T101 WHERE T2001.C2001_REF_ID = T101.C101_USER_ID AND T2001.C901_REF_TYPE = '4000535' AND C901_ACTION != '103122'", [],
            function (tx, rs) {
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                db.transaction(function (tx, rs) {
                    tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '4000535'", [],
                        function (tx, rs) {
                            var diff = parseInt(rs.rows.item(0).Name) - results.length;
                            if (diff > 0)
                                results.push({
                                    "Name": diff + "New User(s)"
                                });
                            callback(results);
                        }, null);
                });
            }, null);
    });
}

function fnGetUpdatedTag(callback) {
    var i = 0,
        results = [];
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT T2001.C2001_REF_ID AS ID, T101.C101_USER_F_NAME || ' ' || T101.C101_USER_L_NAME AS Name, T2001.C901_ACTION FROM T2001_MASTER_DATA_UPDATES T2001, T101_USER T101 WHERE T2001.C2001_REF_ID = T101.C101_USER_ID AND T2001.C901_REF_TYPE = '4001234' AND C901_ACTION != '103122'", [],
            function (tx, rs) {
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                db.transaction(function (tx, rs) {
                    tx.executeSql("SELECT COUNT(*) AS Name FROM T2001_MASTER_DATA_UPDATES T2001 WHERE C901_REF_TYPE = '4001234'", [],
                        function (tx, rs) {
                            var diff = parseInt(rs.rows.item(0).Name) - results.length;
                            if (diff > 0)
                                results.push({
                                    "Name": diff + "New Tag(s)"
                                });
                            callback(results);
                        }, null);
                });
            }, null);
    });
}


function fnGetFilesFor(systemID, callback) {
    var filepath = "globusMobileApp/sql/GmFetchFilesBySystem.sql";
    if (GM.Global.CRMFile)
        filepath = "globusMobileApp/sql/GmFetchCRMFiles.sql"
    $.get(filepath, function (data) {
        var query = data.toString().replace(/<systemid>/g, "'" + systemID + "'");
        if (GM.Global.CRMFile)
            query = data.toString();
        db.transaction(function(tx, rs) {
            tx.executeSql(query, [],
                function (tx, rs) {
                    var i = 0,
                        results = new Array();
                    for (i = 0; i < rs.rows.length; i++) {
                        results.push(rs.rows.item(i));
                    }
                    // alert(JSON.stringify(results));
                    callback(results);
                },
                function (tx, err) {
                    console.log(JSON.stringify(err));
                });
        });
    });
}


function fnGetRequiredUpdates(callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT GROUP_CONCAT(C2001_REF_ID,',') AS ID, C901_REF_TYPE AS TYPE FROM T2001_MASTER_DATA_UPDATES GROUP BY C901_REF_TYPE", [],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            }, null);

    });
}


function fnGetSplashFiles(callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT T903.C903_UPLOAD_FILE_LIST AS ID, T903.C903_REF_ID AS SYSTEM_ID, T903.C901_TYPE AS TYPE, T903.C901_FILE_TITLE TITLE, '" + URL_FileServer + "' || T903.C903_FILE_PATH AS imageURL FROM T903_UPLOAD_FILE_LIST T903, T207_SET_MASTER T207 WHERE T207.C207_SET_ID = T903.C903_REF_ID AND T903.C901_TYPE IN ('4000787','4000788','4000789') AND T903.C901_REF_GRP = '103092' AND T903.C903_DELETE_FL ='' AND T207.C207_VOID_FL = '' ORDER BY T903.C903_FILE_SEQ_NO ", [],
            function (tx, rs) {
                var i = 0,
                    results = new Array();
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function(tx, err) {
                console.log(JSON.stringify(err));
            });
    });
}

function fnUpdateSplashFile(id) {
    db.transaction(function (tx, rs) {
        tx.executeSql("UPDATE T903_UPLOAD_FILE_LIST SET C901_TYPE = '4000790' WHERE C903_UPLOAD_FILE_LIST=?", [id],
            function (tx, rs) {}, null);
    });
}

function fnDeleteSecurityEventsSyncPage() {
    db.transaction(function (tx, rs) {
        tx.executeSql("DELETE FROM T1530A_ACCESS WHERE C1530A_PAGE_NAME IN ('SYNC_DATA','SETTINGS')", [],
            function (tx, rs) {}, null);
    });
}

function fnInsertSecurityEvents(eventName, tagName, pageName, updateAccessFlg, readAccessFlg, voidAccessFlg, accessVoidFlg, groupMappingVoidFlg, rulesVoidFlg) {
    db.transaction(function (tx, rs) {
        tx.executeSql("INSERT OR REPLACE INTO T1530A_ACCESS (C1530A_EVENT_NAME, C1530A_TAG_NAME, C1530A_PAGE_NAME, C1530A_UPDATE_ACCESS_FL, C1530A_READ_ACCESS_FL, C1530A_VOID_ACCESS_FL, C1530A_VOID_FL, C1501_VOID_FL, C906_VOID_FL) VALUES (?,?,?,?,?,?,?,?,?)", [eventName, tagName, pageName, updateAccessFlg, readAccessFlg, voidAccessFlg, accessVoidFlg, groupMappingVoidFlg, rulesVoidFlg],
            function (tx, rs) {},
            function (e, err) {
                console.log(JSON.stringify(err));
            });
    });
}

function fnGetSecurityEvents(pageName, callback) {
    var query = "SELECT C1530A_EVENT_NAME, C1530A_TAG_NAME, C1530A_PAGE_NAME, C1530A_UPDATE_ACCESS_FL, C1530A_READ_ACCESS_FL, C1530A_VOID_ACCESS_FL FROM T1530A_ACCESS  WHERE C1530A_VOID_FL != 'Y' AND C1501_VOID_FL != 'Y' AND C906_VOID_FL != 'Y' AND C1530A_PAGE_NAME ='" + pageName + "'";
    db.transaction(function (tx, rs) {
        tx.executeSql(query, [],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            }, null);

    });
}
