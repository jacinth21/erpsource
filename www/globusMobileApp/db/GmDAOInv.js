function fnGetIMSets (url,txtSearch, callback) {

	var orderby;
    var keyText = txtSearch;

    (parseInt(txtSearch))?orderby='T207.C207_SET_ID':orderby='T207.C207_SET_NM';

	txtSearch = txtSearch.split(' ');
	var searchTerm = [];
	for(var i=0; i<txtSearch.length;i++) {
		(parseInt(keyText))?searchTerm.push("SystemSetName LIKE '%"+txtSearch[i]+"%'"):searchTerm.push("SystemSetName LIKE '%"+txtSearch[i]+"%'");
	}
	
	var query = searchTerm.join(" AND ");
	// query = "SELECT T205.C205_PART_NUMBER_ID AS ID, T205.C205_PART_NUMBER_ID || ' ' || T205.C205_PART_NUM_DESC AS Name, T205.C205_PART_NUMBER_ID || ' ' || T205.C205_PART_NUM_DESC AS SystemPartName, '" + url + "/detail/part/' AS URL FROM T205_PART_NUMBER T205 WHERE T205.C205_ACTIVE_FL = 'Y' AND " + query;
	query = "SELECT T207.C207_SET_ID AS ID,T207.C207_SET_NM AS Desc, T207.C207_SET_ID || ' ' || T207.C207_SET_NM AS Name, T207.C207_SET_ID || ' ' || T207.C207_SET_NM AS SystemSetName,'" + url + "/detail/set/' AS URL FROM T207_SET_MASTER T207 WHERE T207.C207_TYPE = '4070' AND (Name NOT LIKE  '%EUR%' OR '%EURLN%') AND T207.C207_VOID_FL = '' AND " + query + " ORDER BY " + orderby;
	
	db.transaction(function (tx, rs) {
		tx.executeSql(query, [],
			function(tx,rs) {
				var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
			},
		function(e,err){alert('error');console.log(JSON.stringify(err))});
	});
}
function fnGetInvSetsInfo (url,txtSearch, callback) {

	txtSearch = txtSearch.split(' ');
	var searchTerm = [];
	for(var i=0; i<txtSearch.length;i++) {
	       searchTerm.push("SystemSetName LIKE '%"+txtSearch[i]+"%'");
	}
	var query = searchTerm.join(" AND ");
	// query = "SELECT T205.C205_PART_NUMBER_ID AS ID, T205.C205_PART_NUMBER_ID || ' ' || T205.C205_PART_NUM_DESC AS Name, T205.C205_PART_NUMBER_ID || ' ' || T205.C205_PART_NUM_DESC AS SystemPartName, '" + url + "/detail/part/' AS URL FROM T205_PART_NUMBER T205 WHERE T205.C205_ACTIVE_FL = 'Y' AND " + query;
	query = "SELECT T207.C207_SET_ID AS ID,T207.C207_SET_NM AS Desc, T207.C207_SET_ID || ' ' || T207.C207_SET_NM AS Name, T207.C207_SET_ID || ' ' || T207.C207_SET_NM AS SystemSetName,'" + url + "/detail/set/' AS URL FROM T207_SET_MASTER T207 WHERE T207.C207_VOID_FL = '' AND " + query;
	
	if(window.invSystem != undefined && window.invSystem != "")
		{
		var searchSystem = "AND  C207_SET_SALES_SYSTEM_ID ="+window.invSystem; 
		query = query+searchSystem; 
	}
	db.transaction(function (tx, rs) {
		tx.executeSql(query, [],
			function(tx,rs) {
				var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
			},
		function(e,err){alert('error');console.log(JSON.stringify(err))});
	});
}

function fnGetIMLoanerSets (url,txtSearch, callback) {

    var orderby;
    var keyText = txtSearch;

    (parseInt(txtSearch))?orderby='T207.C207_SET_ID':orderby='T207.C207_SET_NM';

	txtSearch = txtSearch.split(' ');
	var searchTerm = [];
	for(var i=0; i<txtSearch.length;i++) {
		(parseInt(keyText))?searchTerm.push("SystemSetName LIKE '%"+txtSearch[i]+"%'"):searchTerm.push("SystemSetName LIKE '%"+txtSearch[i]+"%'");
	}
	var query = searchTerm.join(" AND ");
	// query = "SELECT T207.C207_SET_ID AS ID,T207.C207_SET_NM AS Desc, T207.C207_SET_ID || ' ' || T207.C207_SET_NM AS Name, T207.C207_SET_ID || ' ' || T207.C207_SET_NM AS SystemSetName,'" + url + "/detail/set/' AS URL FROM T207_SET_MASTER T207 WHERE T207.C207_TYPE = '4074' AND (Name NOT LIKE  '%EUR%' OR '%EURLN%') AND T207.C207_VOID_FL = '' AND " + query + " ORDER BY " + orderby;
	query = "SELECT T207.C207_SET_ID AS ID, T207C.C207_SET_ID, T207C.C901_ATTRIBUTE_TYPE AS type, T207C.C207C_ATTRIBUTE_VALUE AS attrbvalue,  T207.C207_SET_NM AS Desc, T207.C207_SET_ID || ' ' || T207.C207_SET_NM AS Name, T207.C207_SET_ID || ' ' || T207.C207_SET_NM AS SystemSetName,'" + url + "/detail/set/' AS URL FROM T207_SET_MASTER T207, TBL_SET_ATTRIBUTE T207C WHERE T207.C207_SET_ID = T207C.C207_SET_ID AND T207.C207_VOID_FL = '' AND T207C.C901_ATTRIBUTE_TYPE = '104740' AND T207C.C207C_ATTRIBUTE_VALUE = 'Y' AND T207.C207_TYPE NOT IN ('4078') AND " + query + " ORDER BY " + orderby;
	db.transaction(function (tx, rs) {
		tx.executeSql(query, [],
			function(tx,rs) {
				var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			
			callback(results);
			},
		function(e,err){alert('error');console.log(JSON.stringify(err))});
	});
}

function fnGetDistributorByRep(repid, callback) {
db.transaction(function (tx, rs) {
		tx.executeSql("SELECT T701.C701_DISTRIBUTOR_ID AS ID , T701.C701_DISTRIBUTOR_NAME AS Name  FROM T703_SALES_REP T703,T701_DISTRIBUTOR T701 WHERE T703.C701_DISTRIBUTOR_ID = T701.C701_DISTRIBUTOR_ID AND T703.C703_SALES_REP_ID = ? AND T701.C701_ACTIVE_FL = 'Y'  AND T701.C701_VOID_FL = '' AND T703.C703_VOID_FL = '' ",[repid],
			function(tx,rs) {
				var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
			console.log(results);
			},
		function(e,err){alert('error');console.log(JSON.stringify(err))});
	});
}

function fnGetAssocSalesRep (repid, callback) {
console.log(repid);
db.transaction(function (tx, rs) {
		tx.executeSql("SELECT DISTINCT T704A.C703_SALES_REP_ID ASSOCREPID,T703.C703_SALES_REP_ID REPID, T703.C703_SALES_REP_NAME REPNM, T101.C101_ACCESS_LEVEL_ID ACCID FROM T704A_ACCOUNT_REP_MAPPING T704A, T704_ACCOUNT T704, T703_SALES_REP T703, T101_USER T101 WHERE T703.C101_PARTY_ID = T101.C101_PARTY_ID AND T704A.C704_ACCOUNT_ID = T704.C704_ACCOUNT_ID AND T704.C703_SALES_REP_ID = T703.C703_SALES_REP_ID AND T703.C703_SALES_REP_ID= T703.C703_SALES_REP_ID AND T704A.C703_SALES_REP_ID = ? AND T703.C703_VOID_FL = '' AND T704A.C704A_VOID_FL = '' AND T704.C704_VOID_FL = ''",[repid],
			function(tx,rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                console.log(results);
                callback(results);
            },
            function (e, err) {
                console.log(JSON.stringify(err));
            });
    });
}
function fnGetAssocSalesAccnt(accnt, callback) {
    console.log(accnt);
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT T704A.C703_SALES_REP_ID ID,(select C703_SALES_REP_NAME FROM T703_SALES_REP where C703_SALES_REP_ID =T704A.C703_SALES_REP_ID)Name, T703.C703_SALES_REP_ID REPID,T703.C703_SALES_REP_NAME REPNM,T101.C101_ACCESS_LEVEL_ID ACCID FROM T704A_ACCOUNT_REP_MAPPING T704A, T704_ACCOUNT T704 ,T703_SALES_REP t703,t101_user t101 WHERE  T704.C704_ACCOUNT_ID = T704A.C704_ACCOUNT_ID AND T704.C703_SALES_REP_ID =T703.C703_SALES_REP_ID AND T101.C101_PARTY_ID =T703.C101_PARTY_ID and T704.C704_ACCOUNT_ID = ?  AND t703.C703_VOID_FL = '' AND T704A.C704A_VOID_FL = '' AND T704.C704_VOID_FL = ''", [accnt],
            function (tx, rs) {
                var i = 0,
                    results = [];
			 for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			 }
			console.log(results);
			callback(results);
			},
		function(e,err){
			console.log(JSON.stringify(err));
		});
	});
}



function fnGetDistributorName (did, callback) {

db.transaction(function (tx, rs) {
		tx.executeSql("SELECT C701_DISTRIBUTOR_NAME AS Name FROM T701_DISTRIBUTOR WHERE C701_DISTRIBUTOR_ID = ?",[did],
			function(tx,rs) {
				var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
			},
		function(e,err){alert('error');console.log(JSON.stringify(err))});
	});
}

function fnGetFieldSalesInfo (repid, callback) {

db.transaction(function (tx, rs) {
		tx.executeSql("SELECT C701_DISTRIBUTOR_ID AS ID FROM T703_SALES_REP WHERE C703_SALES_REP_ID = ?",[repid],
			function(tx,rs) {
				var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
			},
		function(e,err){alert('error');console.log(JSON.stringify(err))});
	});
}

function fnGetFieldSales (url,txtSearch, callback) {
	txtSearch = txtSearch.split(' ');
	var searchTerm = [];
	for(var i=0; i<txtSearch.length;i++) {
	       searchTerm.push("FieldSalesName LIKE '%"+txtSearch[i]+"%'");
	}

	var query = searchTerm.join(" AND ");
	
	query = "SELECT  T701.C701_DISTRIBUTOR_ID AS ID, T701.C701_DISTRIBUTOR_ID || ' ' || T701.C701_DISTRIBUTOR_NAME AS Name, T701.C701_DISTRIBUTOR_NAME  || ' ' ||  T701.C701_DISTRIBUTOR_ID AS FieldSalesName FROM T701_DISTRIBUTOR T701 WHERE  T701.C701_ACTIVE_FL = 'Y' AND T701.C701_VOID_FL = '' AND " + query;
	
	db.transaction(function (tx, rs) {
		tx.executeSql(query, [],
			function(tx,rs) {
				var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
			},
		function(e,err){alert('error');console.log(JSON.stringify(err))});
	});
}

function fnSalesRepDfltAddressInv(ID, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT T703.C703_SALES_REP_NAME AS Name, T106.C106_ADDRESS_ID as AddressID,T106.C901_ADDRESS_TYPE as AddressType, T106.C101_PARTY_ID as PartyID,T106.C106_ADD1 as Add1, T106.C106_ADD2 as Add2, T106.C106_CITY as City, T106.C901_STATE as State, T106.C901_COUNTRY as Country, T106.C106_ZIP_CODE as Zip,T901.C902_CODE_NM_ALT as State FROM T703_SALES_REP T703, T106_ADDRESS T106 LEFT JOIN T901_CODE_LOOKUP T901 ON T106.C901_STATE = T901.C901_CODE_ID WHERE T703.C101_PARTY_ID=T106.C101_PARTY_ID AND T106.C106_PRIMARY_FL = 'Y' AND T703.C703_SALES_REP_ID=?", [ID],
		function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}

			callback(results);
		},
		function(tx,err) {console.log(JSON.stringify(err));});
	});
}
//function to get account /dist address
function fnGetAddressByID (id, typ, callback) {
    var query = "";
    if (typ == "4122") {
        query = "SELECT C704_SHIP_NAME AS Name, C704_ACCOUNT_ID as AddressID, C704_SHIP_ADD1 as Add1, C704_SHIP_ADD2 as Add2, C704_SHIP_CITY as City, C704_SHIP_STATE as State, C704_SHIP_COUNTRY as Country,C704_SHIP_ZIP_CODE as Zip, T901.C902_CODE_NM_ALT as State FROM T704_ACCOUNT T704 LEFT JOIN T901_CODE_LOOKUP T901 ON T704.C704_BILL_STATE = T901.C901_CODE_ID WHERE T704.C704_ACCOUNT_ID=" + id;
    } else if (typ == "4121") {
        query = "SELECT T703.C703_SALES_REP_NAME AS Name,T106.C106_ADDRESS_ID as AddressID, T106.C901_ADDRESS_TYPE as AddressType, T106.C101_PARTY_ID as PartyID, T106.C106_ADD1 as Add1, T106.C106_ADD2 as Add2, T106.C106_CITY as City, T106.C901_STATE as State, T106.C901_COUNTRY as Country, T106.C106_ZIP_CODE as Zip, T106.C106_INACTIVE_FL, T901.C902_CODE_NM_ALT as State FROM T703_SALES_REP T703, T106_ADDRESS T106 LEFT JOIN T901_CODE_LOOKUP T901 ON T106.C901_STATE = T901.C901_CODE_ID WHERE T703.C101_PARTY_ID =T106.C101_PARTY_ID AND T106.C106_INACTIVE_FL <> 'Y' AND T703.C703_SALES_REP_ID="+id;
    } else if (typ == "107801") {
        query = "SELECT T101.C101_PARTY_NM AS Name, T106.C106_ADDRESS_ID as AddressID,T106.C901_ADDRESS_TYPE as AddressType, T106.C101_PARTY_ID as PartyID,T106.C106_ADD1 as Add1, T106.C106_ADD2 as Add2, T106.C106_CITY as City, T106.C901_STATE as State, T106.C901_COUNTRY as Country, T106.C106_ZIP_CODE as Zip,T901.C902_CODE_NM_ALT as State FROM  T106_ADDRESS T106, T101_PARTY T101 LEFT JOIN T901_CODE_LOOKUP T901 ON T106.C901_STATE = T901.C901_CODE_ID " +
				"WHERE T101.C101_PARTY_ID=T106.C101_PARTY_ID AND T101.C101_VOID_FL='' AND T101.C101_ACTIVE_FL='Y'  AND T101.C101_PARTY_ID=" + id;
    } else {
        query = "SELECT C701_SHIP_NAME AS Name, C701_DISTRIBUTOR_ID as AddressID, C701_SHIP_ADD1 as Add1, C701_SHIP_ADD2 as Add2, C701_SHIP_CITY as City, C701_SHIP_STATE as State, C701_SHIP_COUNTRY as Country,C701_SHIP_ZIP_CODE as Zip, T901.C902_CODE_NM_ALT as State FROM T701_DISTRIBUTOR T701 LEFT JOIN T901_CODE_LOOKUP T901 ON T701.C701_BILL_STATE = T901.C901_CODE_ID WHERE T701.C701_DISTRIBUTOR_ID=" + id;
    }
    
    
    console.log(query);
    
	db.transaction(function (tx, rs) {
		tx.executeSql(query, [],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
		},
		function(tx,err) {console.log(JSON.stringify(err));});
	});
}


// Function used to search dealer address
function fnGetDealerAdd(url, txtSearch, callback) {
    txtSearch = txtSearch.split(' ');
    var searchTerm = [];
    for (var i = 0; i < txtSearch.length; i++) {
        searchTerm.push("Name LIKE '%" + txtSearch[i] + "%'");
    }

    var query = searchTerm.join(" AND ");

    query = "SELECT T101.C101_PARTY_NM AS Name, T106.C101_PARTY_ID AS ID, T106.C106_ADDRESS_ID as AddressID,T106.C901_ADDRESS_TYPE as AddressType, T106.C101_PARTY_ID as PartyID,T106.C106_ADD1 as Add1, T106.C106_ADD2 as Add2, T106.C106_CITY as City, T106.C901_STATE as State, T106.C901_COUNTRY as Country, T106.C106_ZIP_CODE as Zip,T901.C902_CODE_NM_ALT as State FROM  T106_ADDRESS T106, T101_PARTY T101 LEFT JOIN T901_CODE_LOOKUP T901 ON T106.C901_STATE = T901.C901_CODE_ID " +
				"WHERE T101.C101_PARTY_ID=T106.C101_PARTY_ID AND T101.C101_VOID_FL='' AND T101.C101_ACTIVE_FL='Y'  AND " + query;

    db.transaction(function (tx, rs) {
        tx.executeSql(query, [],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function (e, err) {
                showMsgView("009");
            });
    });
}


function fnGetAcctAddressByRepID (id, callback) {
	console.log(id);
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT T704.C704_SHIP_NAME AS Name, T704.C704_ACCOUNT_ID as AddressID,T704.C704_SHIP_ADD1 as Add1,T704.C704_SHIP_ADD2 as Add2, T704.C704_SHIP_CITY as City, T704.C704_SHIP_STATE as State, T704.C704_SHIP_COUNTRY as Country, T704.C704_SHIP_ZIP_CODE as Zip,T901.C902_CODE_NM_ALT as State FROM T704_ACCOUNT T704 ,T703_SALES_REP T703 LEFT JOIN T901_CODE_LOOKUP T901 ON T704.C704_BILL_STATE = T901.C901_CODE_ID WHERE T704.C703_SALES_REP_ID = T703.C703_SALES_REP_ID AND T703.C703_SALES_REP_ID=?", [id],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
		},
		function(tx,err) {console.log(JSON.stringify(err));});
	});
}
function fnGetDistInfo(DistID, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT * FROM T703_SALES_REP WHERE C701_DISTRIBUTOR_ID = ?", [DistID],
			function(tx,rs) {
				callback(rs.rows.item(0));
		},function (tx,err) {
			console.log(JSON.stringify(err));
			
		});
	});
}

function fnGetRepInfoInv(RepID, callback) {
	
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT * FROM T703_SALES_REP WHERE C703_SALES_REP_ID = ?", [RepID],
			function(tx,rs) {
				callback(rs.rows.item(0));
		},function (tx,err) {
			console.log(JSON.stringify(err));
			
		});
	});
}

function fnSaveRequest(Requestdata,cb) {
 
	var userId = localStorage.getItem("userID");
	// REQUEST_ID INTEGER PRIMARY KEY AUTOINCREMENT,CASEID VARCHAR2(250),CASEINFOID VARCHAR2(250),REQUESTTYPE VARCHAR2(250), ACCOUNT VARCHAR2(250), USER VARCHAR2(50), JSON VARCHAR2(1000), STATUS VARCHAR2(25), JSONPARTS VARCHAR2(1000), COMMENTS VARCHAR2(5000)
	// REQUEST_ID INTEGER PRIMARY KEY, USER VARCHAR2(50), REQUESTDATA VARCHAR2(1000), STATUS VARCHAR2(25)
	db.transaction(function (tx, rs) {
		 tx.executeSql("INSERT OR REPLACE INTO TBL_REQUEST(USER, REQUESTDATA, STATUS) VALUES(?,?,'DRAFT')", [userId,Requestdata],
			
			function(tx,rs) {
			console.log("updated")

			cb(true);
			},
		function(e,err){
				console.log(err);
				console.log(e);
				console.log(JSON.stringify(err))
				});
	});
}

function fnGetRepFromFS (fsid, callback) {

	
	//query = "SELECT  T701.C701_DISTRIBUTOR_ID AS ID, T701.C701_DISTRIBUTOR_ID || ' ' || T701.C701_DISTRIBUTOR_NAME AS Name, T701.C701_DISTRIBUTOR_NAME  || ' ' ||  T701.C701_DISTRIBUTOR_ID AS FieldSalesName FROM T701_DISTRIBUTOR T701 WHERE  T701.C701_ACTIVE_FL = 'Y' AND T701.C701_VOID_FL = '' AND " + query;
	
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT T703.C703_SALES_REP_ID AS ID ,T703.C703_SALES_REP_NAME AS Name FROM T703_SALES_REP T703 WHERE T703.C701_DISTRIBUTOR_ID=?", [fsid],
			function(tx,rs) {
				var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
			},
		function(e,err){alert('error');console.log(JSON.stringify(err))});
	});
}
function fnGetAccountFromRep (repids, callback) {
	
	//query = "SELECT  T701.C701_DISTRIBUTOR_ID AS ID, T701.C701_DISTRIBUTOR_ID || ' ' || T701.C701_DISTRIBUTOR_NAME AS Name, T701.C701_DISTRIBUTOR_NAME  || ' ' ||  T701.C701_DISTRIBUTOR_ID AS FieldSalesName FROM T701_DISTRIBUTOR T701 WHERE  T701.C701_ACTIVE_FL = 'Y' AND T701.C701_VOID_FL = '' AND " + query;
	
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT T704.C704_ACCOUNT_ID AS ID, T704.C704_ACCOUNT_ID || ' ' || T704.C704_ACCOUNT_NM AS Name FROM T704_ACCOUNT T704 WHERE T704.C703_SALES_REP_ID =?", [repids],
			function(tx,rs) {
				var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
			},
		function(e,err){alert('error');console.log(JSON.stringify(err))});
	});
}

function fnGetSearchReqInfo(keyword,reqtype,callback) {

//	var keyword = keyword.split(" "), searchTerm = new Array();
//
//	for(var i=0; i<keyword.length;i++) {
//	    searchTerm.push("Name LIKE '"+keyword[i]+"'");
//	}
//
//	var query = searchTerm.join(" OR ");
//	
	// query = "SELECT REQUEST.REQUEST_ID AS REQID, REQUEST.CASEINFOID AS CASEINFO,  STATUS, REQUEST.CASEINFOID || ' ' || STATUS AS Name FROM TBL_REQUEST REQUEST WHERE STATUS IN ('DRAFT','Pending Sync','Rejected') AND REQUEST.USER = " + localStorage.getItem("userID") ;
	 console.log(reqtype);
	

	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT REQUEST.REQUEST_ID AS REQID, REQUEST.CASEID || ' ' || STATUS || ' '|| REQUEST.FSID AS Name ,REQUEST.REQUESTTYPENM AS REQTYPENM, REQUEST.CASEINFOID AS CASEINFO,REQUEST.CASEID AS CASEID,REQUEST.FSID AS DISTNM, JSON, STATUS FROM TBL_REQUEST REQUEST WHERE STATUS IN ('DRAFT','Active') AND REQUEST.USER = " + localStorage.getItem("userID")+" AND REQUEST.REQUESTTYPE = ? " , [reqtype],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
		},
		function(){alert(err);console.log(JSON.stringify(err))});
	});
}
function fnUpdateReqStatus(status,caseid,cainfoid,reqid) {
	db.transaction(function (tx, rs) {
		tx.executeSql("UPDATE TBL_REQUEST SET STATUS = ? ,CASEID = ? ,CASEINFOID = ? WHERE  REQUEST_ID = ?", [status,caseid,cainfoid,reqid],
			function(tx,rs) {
				
			},function (tx,err) {
			console.log(JSON.stringify(err));
			
			
		});
	});	
}

function fnCheckRequestID(ID, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT * FROM TBL_REQUEST WHERE REQUEST_ID LIKE ?", ["%" + ID + "%"],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			
			console.log(results);
			callback(results);
		},
		function(tx, err){alert('error');console.log(JSON.stringify(err))});
	});
}

function fnGetRequestInfo(Request, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT * FROM TBL_REQUEST WHERE REQUEST_ID = ?", [Request],
			function(tx,rs) {
				if(rs.rows.length>0)
					callback(rs.rows.item(0));
				else
					callback(0);
			},
		function(e,err){showMsgView("009");});
	});
}
function fnGetDistInfo(DistID, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT * FROM T701_DISTRIBUTOR WHERE C701_DISTRIBUTOR_ID = ?", [DistID],
			function(tx,rs) {
				callback(rs.rows.item(0));
		},function (tx,err) {
			console.log(JSON.stringify(err));
			
		});
	});
}
function fnGetAccounttInfo(AcctId, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT * FROM T701_DISTRIBUTOR WHERE C701_DISTRIBUTOR_ID = ?", [AcctId],
			function(tx,rs) {
				callback(rs.rows.item(0));
		},function (tx,err) {
			console.log(JSON.stringify(err));
			
		});
	});
}

function fnGetSystemInfo (url,txtSearch, callback) {


	db.transaction(function (tx, rs) {
	
		tx.executeSql("SELECT C207_SET_ID AS ID, C207_SET_NM AS Name FROM T207_SET_MASTER WHERE C207_VOID_FL = '' AND C207_SET_ID LIKE ? AND C207_SET_NM LIKE ?", ['300.%','%' + txtSearch + '%'],
			function(tx,rs) {
				var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
					results.push(rs.rows.item(i));
				}
				callback(results);
		},
		function(tx,e){showMsgView("009");});
		});
}	
	



function fnGetInvAppData (CodeGRP,callback) {

	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT C901_CODE_ID AS ID, C901_CODE_NM AS Name, C902_CODE_NM_ALT AS ALT FROM T901_CODE_LOOKUP WHERE C901_CODE_GRP=? AND C901_VOID_FL ='' AND C901_ACTIVE_FL = '1' ORDER BY C901_CODE_SEQ_NO ASC", [CodeGRP],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
		},
		function (tx,err) {
			console.log(JSON.stringify(err));
			
		});
	});
}

function fnRejectReasonData (CodeGRP,callback) {

	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT C901_CODE_ID AS ID, C901_CODE_NM AS Name, C902_CODE_NM_ALT AS ALT FROM T901_CODE_LOOKUP WHERE C901_CODE_GRP=? AND C901_VOID_FL ='' AND C901_ACTIVE_FL = '1' ORDER BY C901_CODE_SEQ_NO ASC", [CodeGRP],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
		},
		function (tx,err) {
			console.log(JSON.stringify(err));
			
		});
	});
}

function fnDeleteRequestLocal (id, callback) {
	
	db.transaction(function (tx, rs) {
		tx.executeSql("DELETE FROM TBL_REQUEST WHERE REQUEST_ID=?", [id],
			function(tx,rs) {
				callback();
			},
		function(e,err){showMsgView("009");});
	});
}

function fnCheckRequestPendingSync () {
	
	var userId = localStorage.getItem("userID");
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT * FROM TBL_REQUEST WHERE USER =?", [userId],
			function(tx,rs) {
				var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
					
					results.push(rs.rows.item(i));
				}
				console.log(results);
				fnAutoRequest(0,results);
		},function (tx,err) {
			console.log(JSON.stringify(err));
			
		});
	});
}


function fnGetDistributorAccounts(distID, callback) {

	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT T704.C704_ACCOUNT_ID AS ID, T704.C704_ACCOUNT_ID || ' ' || T704.C704_ACCOUNT_NM AS Name FROM T704_ACCOUNT T704, T703_SALES_REP T703 WHERE T704.C703_SALES_REP_ID = T703.C703_SALES_REP_ID AND T703.C701_DISTRIBUTOR_ID = ?  AND T703.C703_VOID_FL = '' AND T704.C704_VOID_FL = ''", [distID],
			function(tx,rs) {
				var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
			console.log(results);
		},
		function (tx,err) {
			console.log(JSON.stringify(err));
		});
	});
}


function fnGetAccountByAccess(userid, callback) {
    console.log(userid);
    var query = "SELECT T704.C704_ACCOUNT_ID AS ID,T704.C704_ACCOUNT_ID|| ' '|| T704.C704_ACCOUNT_NM AS Name FROM T704_ACCOUNT T704,T101D_USER_ACCESS T101D WHERE T704.C704_ACCOUNT_ID = T101D.C704_ACCOUNT_ID AND T101D.C101_USER_ID = ? AND T101D.C101D_VOID_FL IS NULL AND T704.C704_VOID_FL = '' order by ID";
    console.log(query);
    db.transaction(function (tx, rs) {
        tx.executeSql(query, [userid],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                console.log(results);
                callback(results);
            },
            function (e, err) {
                alert('error');
                console.log(JSON.stringify(err))
            });
    });
}

function fnGetDetailsFromAccount(accid, callback) {
    console.log(accid);
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT DISTINCT T701.C701_DISTRIBUTOR_ID AS DISTID,T701.C701_DISTRIBUTOR_NAME AS DISTNAME,T703.C703_SALES_REP_ID AS REPID,T703.C703_SALES_REP_NAME AS REPNAME FROM T701_DISTRIBUTOR T701,T703_SALES_REP T703,T704_ACCOUNT T704 WHERE T703.C703_SALES_REP_ID = T704.C703_SALES_REP_ID AND T703.C701_DISTRIBUTOR_ID = T701.C701_DISTRIBUTOR_ID AND T703.C703_VOID_FL = '' AND T701.C701_VOID_FL = '' AND T704.C704_VOID_FL = '' AND C704_ACCOUNT_ID = ?", [accid],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function (e, err) {
                alert('error');
                console.log(JSON.stringify(err))
            });
    });
}

function fnGetDistByAccess(callback) {
    var query = "SELECT DISTINCT T703.C701_DISTRIBUTOR_ID AS ID, (SELECT C701_DISTRIBUTOR_NAME FROM T701_DISTRIBUTOR WHERE C701_DISTRIBUTOR_ID = T703.C701_DISTRIBUTOR_ID and C701_VOID_FL = '') as Name FROM T704_ACCOUNT T704, T703_SALES_REP T703, T101D_USER_ACCESS T101D WHERE T704.C703_SALES_REP_ID = T703.C703_SALES_REP_ID AND T101D.C704_ACCOUNT_ID = T704.C704_ACCOUNT_ID AND T703.C703_VOID_FL = ''AND T704.C704_VOID_FL = '' ORDER BY Name ";
    console.log(query);
    db.transaction(function (tx, rs) {
        tx.executeSql(query, [],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                console.log(results);
                callback(results);
            },
            function (e, err) {
                alert('error');
                console.log(JSON.stringify(err))
            });
    });

}
function fnuserAccess(callback) {
    var query = "SELECT COUNT( * ) AS COUNT FROM T101D_USER_ACCESS";
    console.log(query);
    db.transaction(function (tx, rs) {
        tx.executeSql(query, [],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                console.log(results);
                callback(results);
            },
            function (e, err) {
                alert('error');
                console.log(JSON.stringify(err))
            });
    });
}

function fnGetDistributorByPartyid(partyid, callback) {
db.transaction(function (tx, rs) {
		tx.executeSql("SELECT t701.c701_distributor_id DISTID , t701.c701_distributor_name DISTNM FROM t703_sales_rep t703 , t701_distributor t701 WHERE t703.c701_distributor_id = t701.c701_distributor_id AND t703.c101_party_id = ? AND t703.c703_void_fl  = '' AND t701.c701_void_fl = '' AND t703.c703_active_fl  = 'Y' ORDER BY t701.c701_distributor_name ASC",[partyid],
			function(tx,rs) {
				var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
			console.log(results);
			},
		function(e,err){console.log(JSON.stringify(err))});
	});
}

//Function to fetch address details by party id
function fnPartyAddressInv (ID, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT T101.C101_PARTY_NM AS Name, T106.C106_ADDRESS_ID as AddressID,T106.C901_ADDRESS_TYPE as AddressType, T106.C101_PARTY_ID as PartyID,T106.C106_ADD1 as Add1, T106.C106_ADD2 as Add2, T106.C106_CITY as City, T106.C901_STATE as State, T106.C901_COUNTRY as Country, T106.C106_ZIP_CODE as Zip,T901.C902_CODE_NM_ALT as State FROM  T106_ADDRESS T106, T101_PARTY T101 LEFT JOIN T901_CODE_LOOKUP T901 ON T106.C901_STATE = T901.C901_CODE_ID " +
				"WHERE T101.C101_PARTY_ID=T106.C101_PARTY_ID AND T101.C101_VOID_FL='' AND T101.C101_ACTIVE_FL='Y'  AND T101.C101_PARTY_ID=?", [ID],
		function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}

			callback(results);
		},
		function(tx,err) {console.log(JSON.stringify(err));});
	});
}

//function to get inventory ship to rule value  
function fnGetDftShipById(cb) {
    fnGetRuleValueByCompany("INV_SHIPADD_BY_ID", "SHIP_CARR_MODE_VAL", localStorage.getItem("cmpid"), function (data) {
        if (data.length > 0) {
            GM.Global.DefaultShipto = data[0].RULEVALUE;
        }
        cb(data);
    });
}


// Function to get Default Carrier in the shipping section based on the loggin company
function fnGetDflCarrier(callback) {
    fnGetRuleValueByCompany("SHIP_CARRIER", "SHIP_CARR_MODE_VAL", localStorage.getItem("cmpid"), function (data) {
        if (data.length > 0) {
            GM.Global.DefaultCarrier = data[0].RULEVALUE;
            fnGetCodeNamebyId(GM.Global.DefaultCarrier, function (data) {

                GM.Global.DefaultCarrierName = data.Name;
                callback(data);
            })
        }
        else
            callback(data);
    });
}


//fnGetDflMode() : Function to get Default Mode in the shipping section based on the loggin company
function fnGetDflMode(callback) {
    fnGetRuleValueByCompany("SHIP_MODE", "SHIP_CARR_MODE_VAL", localStorage.getItem("cmpid"), function (data) {
        GM.Global.DefaultMode = data[0].RULEVALUE;
        fnGetCodeNamebyId(GM.Global.DefaultMode, function (data) {
            GM.Global.DefaultModeName = data.Name;
            callback("success");
        })
    });
}

//Function to get set details
function fnGetSetsDetails (url,txtSearch, callback) {
    var orderby;
    var keyText = txtSearch;

    (parseInt(txtSearch))?orderby='ID':orderby='ID';

    txtSearch = txtSearch.split(' ');
    var searchTerm = [];
    for(var i=0; i<txtSearch.length;i++) {
        (parseInt(keyText))?searchTerm.push("SystemSetName LIKE '%"+txtSearch[i]+"%'"):searchTerm.push("SystemSetName LIKE '%"+txtSearch[i]+"%'");
    }
    var query = searchTerm.join(" AND ");  
    query = "SELECT * FROM ( SELECT T207.C207_SET_ID AS ID, T207.C207_SET_NM AS DESC, T207.C207_SET_ID || ' ' || T207.C207_SET_NM AS Name, T207.C207_SET_ID || ' ' || T207.C207_Set_Nm As Systemsetname, '" + url + "/detail/set/' AS URL FROM T207_SET_MASTER T207 WHERE T207.C207_TYPE = '4070' AND (Name NOT LIKE '%EUR%' Or '%EURLN%') And T207.C207_Void_Fl = '' UNION  Select T207.C207_Set_Id As Id, T207.C207_Set_Nm As Desc, T207.C207_SET_ID || ' ' || T207.C207_SET_NM AS Name, T207.C207_SET_ID || ' ' || T207.C207_Set_Nm As Systemsetname, '" + url + "/detail/set/' AS URL FROM T207_SET_MASTER T207, TBL_SET_ATTRIBUTE T207C WHERE T207.C207_SET_ID = T207C.C207_SET_ID AND T207.C207_VOID_FL = '' AND T207C.C901_ATTRIBUTE_TYPE   = '104740' AND (Name NOT LIKE '%EUR%' Or '%EURLN%') And T207c.C207c_Attribute_Value = 'Y' And T207.C207_Type NOT IN ('4078')) WHERE " + query + " ORDER BY " + orderby; 
    console.log(query);
    db.transaction(function (tx, rs) {
        tx.executeSql(query, [],
            function(tx,rs) {
                var i=0, results = [];
            for(i=0;i<rs.rows.length;i++) {
                results.push(rs.rows.item(i));
            }
            
            callback(results);
            },
        function(e,err){alert('error');console.log(JSON.stringify(err))});
    });
}

function fnGetSystemSearchInfo (callback) {
    db.transaction(function (tx, rs) {
		tx.executeSql("SELECT C207_SET_ID AS id, C207_SET_NM AS value FROM T207_SET_MASTER WHERE C207_VOID_FL = '' AND C207_SET_ID LIKE ? ORDER BY C207_SET_NM ", ['300.%'],
			function(tx,rs) {
				var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
					results.push(rs.rows.item(i));
				}
				callback(results);
		},
		function(tx,e){showMsgView("009");});
		});
}

function fnGetInvSetsSearchInfo (callback) {
    
	var query;
    var searchSystem ;
	query = "SELECT T207.C207_SET_ID AS id, T207.C207_SET_NM AS value FROM T207_SET_MASTER T207 WHERE T207.C207_VOID_FL = '' AND c207_TYPE IN (4070,1605,4071) ORDER BY  T207.C207_SET_NM  ";
/*	
	if(window.invBorrowSystem != undefined && window.invBorrowSystem != "")
		{
            searchSystem = "AND  C207_SET_SALES_SYSTEM_ID ="+window.invBorrowSystem; 
            query = query+searchSystem; 
	    }else{
            searchSystem = "ORDER BY  T207.C207_SET_NM "; 
            query = query+searchSystem; 
        }*/
	db.transaction(function (tx, rs) {
		tx.executeSql(query, [],
			function(tx,rs) {
				var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
			},
		function(e,err){alert('error');console.log(JSON.stringify(err))});
	});
}

//Function to get parts details from T205_PART_NUMBER table using part number(PC-2428)
function fnGetPartsInfo(pnum, callback) {
    query = "SELECT T205.C205_PART_NUMBER_ID AS ID, T205.C205_PART_NUMBER_ID || ' ' || T205.C205_PART_NUM_DESC AS Name,T205.C205_PRODUCT_MATERIAL AS ProductMaterial, T205.C205_PRODUCT_CLASS AS ProductClass,T205.C205_PRODUCT_FAMILY AS ProductFamily FROM T205_PART_NUMBER T205 WHERE T205.C205_PART_NUMBER_ID IN('"+pnum+"')";
    db.transaction(function (tx, rs) {
        tx.executeSql(query, [],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function (e, err) {
                showAppError(err, "GmDAODO:fnGetDOParts()");
            });
    });
}