function fnGetFilters (filter, SelectedItem, callback) {
	
		db.transaction(function (tx, rs) {
			tx.executeSql("SELECT C901_CODE_ID AS ID, C901_CODE_NM AS Name FROM T901_CODE_LOOKUP WHERE C901_CODE_GRP=? and C901_CODE_NM <> ? AND C901_VOID_FL ='' AND C901_ACTIVE_FL = '1' ORDER BY C901_CODE_SEQ_NO ASC", [filter, SelectedItem],
					function(tx,rs) {
				var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
					results.push(rs.rows.item(i));
				}
				callback(results);
			},
			function(){showMsgView("009");});
		});
	}
	
function fnGetParts (url,txtSearch, callback) {

	txtSearch = txtSearch.split(' ');
	var searchTerm = [];
	for(var i=0; i<txtSearch.length;i++) {
	       searchTerm.push("SystemPartName LIKE '%"+txtSearch[i]+"%'");
	}

	var query = searchTerm.join(" AND ");
	
	query = "SELECT PLIST.C205_PART_NUMBER_ID AS ID, PLIST.C205_PART_NUMBER_ID || ' ' || PLIST.C205_PART_NUM_DESC AS Name, T207.C207_SET_NM || ' ' || PLIST.C205_PART_NUMBER_ID || ' ' || PLIST.C205_PART_NUM_DESC AS SystemPartName, '" + url + "/detail/part/' AS URL FROM (SELECT T205.C205_PART_NUMBER_ID, T205.C205_PART_NUM_DESC, T205.C207_SET_SALES_SYSTEM_ID FROM T205_PART_NUMBER T205, T205D_PART_ATTRIBUTE T205D WHERE T205.C205_ACTIVE_FL = 'Y' AND T205.C205_PART_NUMBER_ID = T205D.C205_PART_NUMBER_ID AND T205D.C901_ATTRIBUTE_TYPE = '80180' AND T205D.C205D_ATTRIBUTE_VALUE = '" + intServerCountryCode + "' AND T205D.C205D_VOID_FL = '') PLIST, T207_SET_MASTER T207 WHERE T207.C207_SET_ID = PLIST.C207_SET_SALES_SYSTEM_ID  AND T207.C207_VOID_FL = '' AND " + query;

	db.transaction(function (tx, rs) {
		tx.executeSql(query, [],
			function(tx,rs) {
				var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
			},
		function(e,err){showMsgView("009");});
	});
}


function fnGetQuicklinks (callback) {

	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT C901_CODE_ID AS ID, C901_CODE_NM AS Name FROM T901_CODE_LOOKUP WHERE C901_CODE_GRP=? AND C901_VOID_FL ='' AND C901_ACTIVE_FL = '1' ORDER BY C901_CODE_SEQ_NO ASC", ['PCNAVI'],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
		},
		function(){showMsgView("009");});
	});
}


function fnGetSystems (url,txtSearch, callback) {


	db.transaction(function (tx, rs) {
	
		tx.executeSql("SELECT C207_SET_ID AS ID, C207_SET_NM AS Name, ? AS URL FROM T207_SET_MASTER WHERE C207_VOID_FL = '' AND C207_SET_ID LIKE ? AND C207_SET_NM LIKE ?", [url+'/detail/system/','300.%','%' + txtSearch + '%'],
			function(tx,rs) {
				var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
					results.push(rs.rows.item(i));
				}
				callback(results);
		},
		function(tx,e){showMsgView("009");});
		});
}

function fnGetSystemList (url,callback) {

	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT C207_SET_ID AS ID, C207_SET_NM AS Name, ? AS URL FROM T207_SET_MASTER WHERE C207_VOID_FL = '' AND C207_SET_ID LIKE ? ORDER BY Name", [url+"/detail/system/",'300.%'],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
		},
		function(tx,e){showMsgView("009");});
	});
}

function fnGetTechniqueList(url,callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT C901_CODE_ID AS ID, C901_CODE_NM AS Name, ? AS URL FROM T901_CODE_LOOKUP WHERE C901_CODE_GRP=? AND C901_VOID_FL ='' AND C901_ACTIVE_FL = '1' ORDER BY C901_CODE_SEQ_NO ASC", [url+"/Technique/",'TECHQ'],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
		},
		function(){showMsgView("033");});
	});
}

function fnGetTechniqueName(id,callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT C901_CODE_NM AS Name FROM T901_CODE_LOOKUP WHERE C901_CODE_GRP='TECHQ' AND C901_CODE_ID=? AND C901_VOID_FL ='' AND C901_ACTIVE_FL = '1'", [id],
				function(tx,rs) {
			callback(rs.rows.item(0).Name);
		},
		function(){showMsgView("033");});
	});
}
function fnGetSystemListForTech(url,id,callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT C207.C207_SET_ID AS ID, C207.C207_SET_NM AS Name, ? AS URL FROM T207_SET_MASTER C207, T207C_SET_ATTRIBUTE T207C WHERE C207.C207_VOID_FL = '' AND T207C.C207C_VOID_FL = '' AND C207.C207_SET_ID = T207C.C207_SET_ID AND T207C.C207C_ATTRIBUTE_VALUE = ?", [url+"/detail/system/",id],
				function(tx,rs) {
					var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
		},
		function(){showMsgView("033");});
	});
}

function fnCollatealGetSystemListForTech(url,id,callback) {
    	 db.transaction(function (tx, rs) {
		//tx.executeSql("SELECT C207.C207_SET_ID AS ID, C207.C207_SET_NM AS Name, ? AS URL FROM T207_SET_MASTER C207, T207C_SET_ATTRIBUTE T207C WHERE C207.C207_VOID_FL = '' AND T207C.C207C_VOID_FL = '' AND C207.C207_SET_ID = T207C.C207_SET_ID AND T207C.C207C_ATTRIBUTE_VALUE = ?", [url+"/detail/system/",id],
    	tx.executeSql("SELECT T207.C207_SET_ID As ID, T207.C207_SET_NM AS Name, ? AS URL FROM T207_SET_MASTER T207, T207C_SET_ATTRIBUTE T207C, ( SELECT DISTINCT (T207.c207_set_id) systemid FROM t207_set_master T207, t207c_set_attribute t207c WHERE T207.c207_set_id = t207c.c207_set_id AND T207.c207_void_fl = '' AND t207c.c207c_void_fl = '' AND C901_ATTRIBUTE_TYPE = '103119' AND c207c_attribute_value = '103086' ) sysdata WHERE T207.C207_VOID_FL ='' AND T207C.C207C_VOID_FL ='' AND T207.C207_SET_ID = T207C.C207_SET_ID and sysdata.systemid = T207.C207_SET_ID AND T207C.C207C_ATTRIBUTE_VALUE = ?", [url+"/detail/system/",id],
		function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
		},
		function(){showMsgView("033");});
	});
}


function fnInsertRecent(Type, ID) {

	fnDeleteExtraRecent(Type,function() {
		db.transaction(function (tx, rs) {
			var time = new Date();
			tx.executeSql("INSERT OR REPLACE INTO TBL_RECENT(DETAIL_ID,TYPE,TIME) VALUES(?,?,?)", [ID, Type, time],
					function(tx,rs) {
			},
			function(){showMsgView("009");});
		});
	});
	
}

function fnDeleteExtraRecent(Type,callback) {
	db.transaction(function (tx, rs) {
			tx.executeSql("DELETE FROM TBL_RECENT WHERE TYPE=? AND REC_ID NOT IN (SELECT REC_ID FROM TBL_RECENT WHERE TYPE=? ORDER BY TIME DESC LIMIT 20)", [Type, Type],
					function(tx,rs) {
						callback();
			},
			function(){showMsgView("009");});
		});
}

function fnGetRecentSystem(url,callback) {
	var i=0, results = [];
	db.transaction(function (tx, rs) {
						tx.executeSql("SELECT TBL_RECENT.DETAIL_ID AS ID, T207_SET_MASTER.C207_SET_NM AS Name, ? AS URL FROM TBL_RECENT, T207_SET_MASTER WHERE TBL_RECENT.DETAIL_ID = T207_SET_MASTER.C207_SET_ID AND T207_SET_MASTER.C207_SET_ID LIKE ? ORDER BY TBL_RECENT.TIME DESC LIMIT 10", [url+"/detail/system/",'300.%'],function(tx,rs) {
							for(i=0;i<rs.rows.length;i++) {
								results.push(rs.rows.item(i));
							}
							callback(results);
						},
						function(){showMsgView("009");});
					});
	
}

function fnGetRecentSets(url,callback) {
	var i=0, results = [];
	db.transaction(function (tx, rs) {
				tx.executeSql("SELECT TBL_RECENT.DETAIL_ID AS ID, T207_SET_MASTER.C207_SET_NM AS Name, ? AS URL FROM TBL_RECENT, T207_SET_MASTER WHERE TBL_RECENT.DETAIL_ID = T207_SET_MASTER.C207_SET_ID AND TBL_RECENT.TYPE = ? ORDER BY TBL_RECENT.TIME DESC LIMIT 10",[url+"/detail/set/",'set'],function(tx,rs) {
					for(i=0;i<rs.rows.length;i++) {
						results.push(rs.rows.item(i));
					}
					callback(results);
				},
				function(){showMsgView("009");});
			});
}

function fnGetRecentParts(url,callback) {
	var i=0, results = [];
	db.transaction(function (tx, rs) {
				tx.executeSql("SELECT TBL_RECENT.DETAIL_ID AS ID, T205_PART_NUMBER.C205_PART_NUMBER_ID AS Name, ? AS URL FROM TBL_RECENT, T205_PART_NUMBER WHERE T205_PART_NUMBER.C205_ACTIVE_FL = 'Y' AND TBL_RECENT.DETAIL_ID = T205_PART_NUMBER.C205_PART_NUMBER_ID ORDER BY TBL_RECENT.TIME DESC LIMIT 10", [url+"/detail/part/"],function(tx,rs) {
					for(i=0;i<rs.rows.length;i++) {
						results.push(rs.rows.item(i));
					}
					callback(results);
				},
				function(){showMsgView("009");});
			});
}

function fnGetRecentGroups(url,callback) {
	var i=0, results = [];
		db.transaction(function (tx, rs) {
			tx.executeSql("SELECT TBL_RECENT.DETAIL_ID AS ID, T4010_GROUP.C4010_GROUP_NM AS Name, ? AS URL FROM TBL_RECENT, T4010_GROUP WHERE TBL_RECENT.DETAIL_ID = T4010_GROUP.C4010_GROUP_ID ORDER BY TBL_RECENT.TIME DESC LIMIT 10", [url+"/detail/group/"],function(tx,rs) {
				for(i=0;i<rs.rows.length;i++) {
					results.push(rs.rows.item(i));
				}
				callback(results);
			},
			function(){showMsgView("009");});
		});
}

function fnInsertFavorite(SysID,Type,callback) {
	var now = new Date();
	db.transaction(function (tx, rs) {
		tx.executeSql("INSERT INTO TBL_FAVORITE(SYSTEM_ID,TYPE,CREATED_DATE) VALUES (?,?,?)", [SysID,Type,now],function(tx,rs) {
			callback("success");
		},
		function(){showMsgView("009");});
	});
}

function fnRemoveFavorite(SysID,callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("DELETE FROM TBL_FAVORITE WHERE SYSTEM_ID = ?", [SysID],function(tx,rs) {
			callback("success");
		},
		function(){showMsgView("009");});
	});
}

function fnChkFavorite(SysID,type,callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT * FROM TBL_FAVORITE WHERE SYSTEM_ID = ? AND TYPE = ? ", [SysID, type],function(tx,rs) {
			if(rs.rows.length>0) {
				callback("success");
			}
			else {
				callback("fail");
			}
		},
		function(e,err){showMsgView("009");});
	});
}


function fnGetFavorites(url,type,callback) {
	var i=0, results = [];
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT C207_SET_ID AS ID, C207_SET_NM AS Name, ? AS URL FROM T207_SET_MASTER, TBL_FAVORITE WHERE T207_SET_MASTER.C207_SET_ID = TBL_FAVORITE.SYSTEM_ID AND TBL_FAVORITE.TYPE = ? ORDER BY TBL_FAVORITE.CREATED_DATE DESC LIMIT 10 ", [url+"/detail/system/",type],
			function(tx,rs) {
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
		},
		function(){showMsgView("009");});
	});
}

function fnGetMessageData() {

	$.ajax({
		url : "globusMobileApp/datafiles/msg.json",
		method : "GET",
		datatype : "application/json",
		success : function(data) {
			data = $.parseJSON(data);
			for (var i=0;i<data.length;i++) {
				fnInsertMessages(data[i]);
			}
		}
	});
}

function fnInsertMessages(data) {

	db.transaction(function (tx, rs) {
				tx.executeSql("INSERT OR REPLACE INTO TBL_MSG_STATUS (MSGID,LANG,MESSAGE,REASON,ACTION) VALUES (?,?,?,?,?)", [data.ID,data.Lang,data.Message,data.Reason,data.Action],
						function(tx,rs) {
				},null);
				
			});
}

/*To display messages in the message bar*/
function fnGetMessages(msgID, callback) {

    var language = "EN";
    if (GM.Global.LANG != undefined)
        language = GM.Global.LANG.toUpperCase();


    console.log(msgID, language)
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT MSGID AS ID,LANG AS Lang,MESSAGE AS Message,REASON AS Reason,ACTION AS Action FROM TBL_MSG_STATUS WHERE MSGID = ? AND LANG = ?", [msgID, language],
            function (tx, rs) {
            console.log(rs.rows.item(0));
                callback((rs.rows.item(0)));
            },
            function () {
                showMsgView("009");
            });
    });
}

function fnGetSystemImages(url,txtSearch, callback) {
    txtSearch = txtSearch.split(' ');
	var searchTerm = [];
		for(var i=0; i<txtSearch.length;i++) {
		    searchTerm.push("SearchItemName LIKE '%"+txtSearch[i]+"%'");
		}
		var query = searchTerm.join(" AND ");
		
		query   = "SELECT ID, Name, FileName, Path, ShareValue, Type, URL, Name || ' ' || FileName ||' '|| SYSTEM_NAME ||' '|| IFNULL(Keywords,'') AS SearchItemName FROM ( SELECT T903.C903_UPLOAD_FILE_LIST AS ID, T903.C901_FILE_TITLE AS Name, '" + url + "/detail/system/' || T207.C207_SET_ID  || '/images/'  AS URL, T903.C903_FILE_NAME AS FileName, T903.C903_FILE_PATH AS Path, IFNULL(T903.C903_SHARE_RULE,'') || ''  || IFNULL(T903.C903_SHARE_FLAG,'') AS ShareValue, T903.C901_Type AS Type, T207.C207_SET_ID AS SYSTEM_ID, T207.C207_SET_NM AS SYSTEM_NAME, KEYWORD.KEYNAME as Keywords FROM T903_UPLOAD_FILE_LIST T903 LEFT OUTER JOIN (SELECT group_concat(T2050.C2050_KEYWORD_REF_NAME,' ') KEYNAME, (T2050.C2050_REF_ID) KEYID FROM T2050_KEYWORD_REF T2050 WHERE T2050.C2050_VOID_FL ='' GROUP BY T2050.C2050_REF_ID) KEYWORD ON KEYWORD.KEYID = T903.C903_UPLOAD_FILE_LIST, T207_SET_MASTER T207, ( SELECT DISTINCT (T207.c207_set_id) SYSTEMID FROM t207_set_master T207, t207c_set_attribute t207c WHERE T207.c207_set_id = t207c.c207_set_id AND T207.c207_void_fl ='' AND t207c.c207c_void_fl ='' AND C901_ATTRIBUTE_TYPE   = '103119'  AND c207c_attribute_value = '103086') SYSDATA WHERE SYSDATA.SYSTEMID = T207.C207_SET_ID AND T903.C903_REF_ID = T207.C207_SET_ID AND T903.C903_DELETE_FL ='' AND T903.C901_REF_TYPE  ='103112' AND T207.C207_VOID_FL = '') WHERE "+ query;

		db.transaction(function (tx, rs) {
			tx.executeSql(query, [],
				function(tx,rs) {
					var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
					results.push(rs.rows.item(i));
				}
				callback(results);
				},
			function(e,err){showMsgView("009");});
		});
}


function fnGetDocuments(url,txtSearch, callback) {

	txtSearch = txtSearch.split(' ');
	var searchTerm = [];
	for(var i=0; i<txtSearch.length;i++) {
	    searchTerm.push("SearchItemName LIKE '%"+txtSearch[i]+"%'");
	}
	
	var query = searchTerm.join(" AND ");
    
    
 /*   query   = "SELECT ID, Name, FileName, Path, ShareValue, Type, URL, Name || ' ' || FileName ||' '|| SYSTEM_NAME ||' '|| IFNULL(Keywords,'') AS SearchItemName FROM ( SELECT T903.C903_UPLOAD_FILE_LIST AS ID, T903.C901_FILE_TITLE AS Name, '" + url + "/detail/system/' || T207.C207_SET_ID  || '/documents/'  AS URL, T903.C903_FILE_NAME AS FileName, T903.C903_FILE_PATH AS Path, IFNULL(T903.C903_SHARE_RULE,'') || ''  || IFNULL(T903.C903_SHARE_FLAG,'') AS ShareValue, T903.C901_Type AS Type, T207.C207_SET_ID AS SYSTEM_ID, T207.C207_SET_NM AS SYSTEM_NAME, KEYWORD.KEYNAME as Keywords FROM T903_UPLOAD_FILE_LIST T903 LEFT OUTER JOIN (SELECT group_concat(T2050.C2050_KEYWORD_REF_NAME,' ') KEYNAME, (T2050.C2050_REF_ID) KEYID FROM T2050_KEYWORD_REF T2050 WHERE T2050.C2050_VOID_FL ='' GROUP BY T2050.C2050_REF_ID) KEYWORD ON KEYWORD.KEYID = T903.C903_UPLOAD_FILE_LIST, T207_SET_MASTER T207, ( SELECT DISTINCT (T207.c207_set_id) SYSTEMID FROM t207_set_master T207, t207c_set_attribute t207c WHERE T207.c207_set_id = t207c.c207_set_id AND T207.c207_void_fl ='' AND t207c.c207c_void_fl ='' AND C901_ATTRIBUTE_TYPE   = '103119'  AND c207c_attribute_value = '103086') SYSDATA WHERE SYSDATA.SYSTEMID = T207.C207_SET_ID AND T903.C903_REF_ID = T207.C207_SET_ID AND T903.C903_DELETE_FL ='' AND (T903.C901_REF_TYPE  ='103114' OR T903.C901_REF_TYPE ='4000441') AND T207.C207_VOID_FL = '') WHERE "+ query;*/
	

    query   = "SELECT ID, Name, FileName, Path, ShareValue, Type, URL, Name || ' ' || FileName || ' ' || SYSTEM_NAME || ' ' || IFNULL(Keywords, '') AS SearchItemName FROM (SELECT T903.C903_UPLOAD_FILE_LIST AS ID, T903.C901_FILE_TITLE AS Name, '" + url + "/detail/system/' || T207.C207_SET_ID || '/documents/' AS URL, T903.C903_FILE_NAME AS FileName, T903.C903_FILE_PATH AS Path, IFNULL(T903.C903_SHARE_RULE, '') || '' || IFNULL(T903.C903_SHARE_FLAG, '') AS ShareValue, T903.C901_Type AS Type, T207.C207_SET_ID AS SYSTEM_ID, T207.C207_SET_NM AS SYSTEM_NAME, KEYWORD.KEYNAME as Keywords FROM T903_UPLOAD_FILE_LIST T903 LEFT OUTER JOIN (SELECT group_concat(T2050.C2050_KEYWORD_REF_NAME, ' ') KEYNAME, (T2050.C2050_REF_ID) KEYID FROM T2050_KEYWORD_REF T2050 WHERE T2050.C2050_VOID_FL = ''GROUP BY T2050.C2050_REF_ID ) KEYWORD ON KEYWORD.KEYID = T903.C903_UPLOAD_FILE_LIST, T207_SET_MASTER T207, (SELECT DISTINCT (T207.c207_set_id) SYSTEMID FROM t207_set_master T207, t207c_set_attribute t207c WHERE T207.c207_set_id = t207c.c207_set_id AND T207.c207_void_fl = '' AND t207c.c207c_void_fl = '' AND C901_ATTRIBUTE_TYPE = '103119' AND c207c_attribute_value = '103086') SYSDATA WHERE SYSDATA.SYSTEMID = T207.C207_SET_ID AND T903.C903_REF_ID = T207.C207_SET_ID AND T903.C903_DELETE_FL = '' AND T903.C901_REF_TYPE = '4000441' AND T207.C207_VOID_FL = '' UNION ALL SELECT T903.C903_UPLOAD_FILE_LIST AS ID, T903.C901_FILE_TITLE AS Name, '" + url + "/detail/system/' || T207.C207_SET_ID || '/documents/' AS URL, T903.C903_FILE_NAME AS FileName, T903.C903_FILE_PATH AS Path, IFNULL(T903.C903_SHARE_RULE, '') || '' || IFNULL(T903.C903_SHARE_FLAG, '') AS ShareValue, T903.C901_Type AS Type, T207.C207_SET_ID AS SYSTEM_ID, T207.C207_SET_NM AS SYSTEM_NAME, KEYWORD.KEYNAME as Keywords FROM T903_UPLOAD_FILE_LIST T903 LEFT OUTER JOIN (SELECT group_concat(T2050.C2050_KEYWORD_REF_NAME, ' ') KEYNAME, (T2050.C2050_REF_ID) KEYID FROM T2050_KEYWORD_REF T2050 WHERE T2050.C2050_VOID_FL = ''GROUP BY T2050.C2050_REF_ID ) KEYWORD ON KEYWORD.KEYID = T903.C903_UPLOAD_FILE_LIST, T207_SET_MASTER T207, (SELECT DISTINCT (T207.c207_set_id) SYSTEMID FROM t207_set_master T207, t207c_set_attribute t207c WHERE T207.c207_set_id = t207c.c207_set_id AND T207.c207_void_fl = ''AND t207c.c207c_void_fl = ''AND C901_ATTRIBUTE_TYPE = '103119'AND c207c_attribute_value = '103086') SYSDATA, (SELECT T205.C205_PART_NUMBER_ID pnum FROM T205_PART_NUMBER T205, T205D_PART_ATTRIBUTE T205D WHERE T205.C205_ACTIVE_FL = 'Y' AND T205.C205_PART_NUMBER_ID    = T205D.C205_PART_NUMBER_ID AND T205D.C901_ATTRIBUTE_TYPE = '80180' AND T205D.C205D_ATTRIBUTE_VALUE = '" + intServerCountryCode + "' AND T205D.C205D_VOID_FL = '' ) PLIST WHERE SYSDATA.SYSTEMID = T207.C207_SET_ID AND T903.C903_REF_ID = T207.C207_SET_ID AND T903.C903_DELETE_FL = '' AND T903.C901_REF_TYPE = '103114' AND T207.C207_VOID_FL = '' AND T903.C205_PART_NUMBER_ID = PLIST.pnum ) WHERE "+ query;
    

	db.transaction(function (tx, rs) {
		tx.executeSql(query, [],
			function(tx,rs) {
				var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
			},
		function(e,err){showMsgView("009");});
	});
}


function fnGetVideos(url,txtSearch, callback) {
   txtSearch = txtSearch.split(' ');
	var searchTerm = [];
		for(var i=0; i<txtSearch.length;i++) {
		    searchTerm.push("SearchItemName LIKE '%"+txtSearch[i]+"%'");
		}
		var query = searchTerm.join(" AND ");
		
		query   = "SELECT ID, Name, FileName, Path, ShareValue, URL, Name || ' ' || FileName ||' '|| SYSTEM_NAME ||' '|| IFNULL(Keywords,'') AS SearchItemName FROM ( SELECT T903.C903_UPLOAD_FILE_LIST AS ID, T903.C901_FILE_TITLE AS Name, '" + url + "/detail/system/' || T207.C207_SET_ID  || '/videos/'  AS URL, T903.C903_FILE_NAME AS FileName, T903.C903_FILE_PATH AS Path, IFNULL(T903.C903_SHARE_RULE,'') || ''  || IFNULL(T903.C903_SHARE_FLAG,'') AS ShareValue, T207.C207_SET_ID AS SYSTEM_ID, T207.C207_SET_NM AS SYSTEM_NAME, KEYWORD.KEYNAME as Keywords FROM T903_UPLOAD_FILE_LIST T903 LEFT OUTER JOIN (SELECT group_concat(T2050.C2050_KEYWORD_REF_NAME,' ') KEYNAME, (T2050.C2050_REF_ID) KEYID FROM T2050_KEYWORD_REF T2050 WHERE T2050.C2050_VOID_FL ='' GROUP BY T2050.C2050_REF_ID) KEYWORD ON KEYWORD.KEYID = T903.C903_UPLOAD_FILE_LIST, T207_SET_MASTER T207, ( SELECT DISTINCT (T207.c207_set_id) SYSTEMID FROM t207_set_master T207, t207c_set_attribute t207c WHERE T207.c207_set_id = t207c.c207_set_id AND T207.c207_void_fl ='' AND t207c.c207c_void_fl ='' AND C901_ATTRIBUTE_TYPE   = '103119'  AND c207c_attribute_value = '103086') SYSDATA WHERE SYSDATA.SYSTEMID = T207.C207_SET_ID AND T903.C903_REF_ID = T207.C207_SET_ID AND T903.C903_DELETE_FL ='' AND T903.C901_REF_TYPE  ='103113' AND T207.C207_VOID_FL = '') WHERE "+ query;

		db.transaction(function (tx, rs) {
			tx.executeSql(query, [],
				function(tx,rs) {
					var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
					results.push(rs.rows.item(i));
				}
				callback(results);
				},
			function(e,err){showMsgView("009");});
		});

}


function fnSearchEmail(txtSearch, callback) {

	txtSearch = txtSearch.split(' ');
	var searchTerm = [];
	for(var i=0; i<txtSearch.length;i++) {
	    searchTerm.push("EmailSearchName LIKE '%"+txtSearch[i]+"%'");
	}

	var query = searchTerm.join(" AND ");

   query = "SELECT RSL.id AS ID, RSL.party_id AS PartyID, RSL.firstname AS FirstName, RSL.lastname AS LastName, RSL.email AS Email, RSL.emailsearchname AS EmailSearchName, RSL.edit_fl AS EditFL, FAV.type AS FavClass FROM(SELECT id, party_id, firstname, lastname, email, firstname || ' ' || lastname || ' ' || email AS EmailSearchName,edit_fl FROM (SELECT TC.contact_id AS ID, TP.first_nm AS FirstName, TP.last_nm AS LastName, TC.contact_value AS Email, TC.contact_type AS ContactType, TC.party_id AS PARTY_ID, 'Y' AS EDIT_FL FROM tbl_party TP, tbl_contact TC WHERE TP.party_id = TC.party_id AND TC.contact_type = '90452' UNION SELECT T107TC.c107_contact_id AS ID, T101TP.c101_first_nm AS FirstName, T101TP.c101_last_nm AS LastName, T107TC.c107_contact_value AS Email, T107TC.c107_contact_type AS ContactType, T107TC.c101_party_id, '' AS EDIT_FL FROM t101_party T101TP, t107_contact T107TC WHERE T101TP.c101_party_id = T107TC.c101_party_id AND T107TC.c901_mode = '90452' AND T101TP.c101_active_fl = '' AND T107TC.c107_inactive_fl = '' AND T101TP.c101_void_fl = '' AND T107TC.c107_void_fl = '' UNION SELECT '' AS ID, '' AS FirstName, '' AS LastName, TR.detail_id AS Email, '' AS ContactType, '' AS PARTY_ID, '' AS EDIT_FL FROM tbl_recent TR WHERE TR.type = 'emails') WHERE " + query + " AND email !='') RSL LEFT JOIN tbl_favorite FAV ON FAV.system_id LIKE RSL.id";

	db.transaction(function (tx, rs) {
		tx.executeSql(query, [],
			function(tx,rs) {
				var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
				}

				callback(results);

			},
			function(e,err){showMsgView("009");
		});
	});
}

function fnGetGroups (url,txtSearch, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT T4010.C4010_GROUP_ID AS ID, T4010.C4010_GROUP_NM AS Name, ? AS URL FROM T4010_GROUP T4010, T207_SET_MASTER T207 WHERE T4010.C207_SET_ID = T207.C207_SET_ID AND T207.C207_VOID_FL = '' AND T4010.C4010_VOID_FL = '' AND T4010.C4010_PUBLISH_FL='Y' AND T4010.C4010_GROUP_NM LIKE ?", [url+'/detail/group/','%' + txtSearch + '%'],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
		},
		function(){showMsgView("009");});
	});
}

function fnGetCollateralSystemList (url,callback) {

	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT T207.C207_SET_ID AS ID, T207.C207_SET_NM AS Name, ? AS URL FROM T207_SET_MASTER T207,T207C_SET_ATTRIBUTE T207C WHERE T207C.C207_SET_ID = T207.C207_SET_ID AND T207C.C207C_ATTRIBUTE_VALUE = '103086' AND  T207.C207_VOID_FL = '' AND T207C.C207C_VOID_FL = '' AND C901_ATTRIBUTE_TYPE    = '103119' AND T207.C207_SET_ID LIKE ? ORDER BY Name", [url+"/detail/system/",'300.%'],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
		},
		function(tx,e){showMsgView("009");});
	});
}

function fnGetCollateralRecentSystem(url,callback) {
	var i=0, results = [];
	db.transaction(function (tx, rs) {
			tx.executeSql("SELECT DISTINCT TBL_RECENT.DETAIL_ID AS ID, T207.C207_SET_NM AS Name, ? AS URL FROM TBL_RECENT, T207_SET_MASTER T207,T207C_SET_ATTRIBUTE T207C WHERE TBL_RECENT.DETAIL_ID = T207.C207_SET_ID AND  T207.C207_SET_ID = T207C.C207_SET_ID AND T207C.C207C_ATTRIBUTE_VALUE = '103086' AND T207C.C207C_VOID_FL = '' AND T207.C207_VOID_FL = '' AND T207.C207_SET_ID LIKE ? ORDER BY TBL_RECENT.TIME DESC LIMIT 10", [url+"/detail/system/",'300.%'],function(tx,rs) {
				for(i=0;i<rs.rows.length;i++) {
					results.push(rs.rows.item(i));
				}
				callback(results);
			},
			function(){showMsgView("009");});
	});
	
}

function fnSearchContact(txtSearch, callback) {

	txtSearch = txtSearch.split(' ');
	var searchTerm = [];
	for(var i=0; i<txtSearch.length;i++) {
    	searchTerm.push("EmailSearchName LIKE '%"+txtSearch[i]+"%'");
	}

	var query = searchTerm.join(" AND ");
	query = "SELECT ID, FirstName, LastName, LastName || ', ' || FirstName AS Name, Email, FirstName || ' ' || LastName || ' ' || Email || ' ' || NPI AS EmailSearchName FROM (Select TP.PARTY_ID AS ID,TP.FIRST_NM AS FirstName,TP.LAST_NM AS LastName, TC.CONTACT_VALUE AS Email, TP.NPI AS NPI from TBL_PARTY TP, TBL_CONTACT TC WHERE TP.PARTY_ID=TC.PARTY_ID AND TC.CONTACT_TYPE = '90452' UNION SELECT T101TP.C101_PARTY_ID AS ID,T101TP.C101_FIRST_NM AS FirstName,T101TP.C101_LAST_NM AS LastName,T107TC.C107_CONTACT_VALUE AS Email, '' AS NPI from T101_PARTY T101TP,T107_CONTACT T107TC WHERE T101TP.C101_PARTY_ID = T107TC.C101_PARTY_ID AND T107TC.C107_CONTACT_TYPE = '90452' AND T101TP.C101_ACTIVE_FL='' AND T107TC.C107_INACTIVE_FL = '' AND T101TP.C101_VOID_FL = '' AND T107TC.C107_VOID_FL = '' ) WHERE "+query;

	db.transaction(function (tx, rs) {
		tx.executeSql(query, [],
		function(tx,rs) {
		var i=0, results = [];
		for(i=0;i<rs.rows.length;i++) {
		results.push(rs.rows.item(i));
	}

	callback(results);
	},
		function(e,err){showMsgView("009");
		});
	});
}

function fnGetCollateralSystems (url,txtSearch, callback) {

	 txtSearch = txtSearch.split(' ');
		var searchTerm = [];
			for(var i=0; i<txtSearch.length;i++) {
			    searchTerm.push("SearchItemName LIKE '%"+txtSearch[i]+"%'");
			}
	var query = searchTerm.join(" AND ");
	
	query = "SELECT DISTINCT T207.C207_SET_ID AS ID, T207.C207_SET_NM AS Name, T207.C207_SET_NM ||' '|| IFNULL(KEYWORD.KEYNAME,'') As SearchItemName,'" + url + "/detail/system/' AS URL FROM T207_SET_MASTER T207 LEFT JOIN (SELECT GROUP_CONCAT(T2050.C2050_KEYWORD_REF_NAME)KEYNAME, (T2050.C2050_REF_ID) KEYID FROM T2050_KEYWORD_REF T2050 WHERE C901_REF_TYPE = '103108' AND T2050.C2050_VOID_FL='' GROUP BY T2050.C2050_REF_ID) KEYWORD ON KEYWORD.KEYID = T207.C207_SET_ID , T207C_SET_ATTRIBUTE T207C WHERE T207.C207_SET_ID= T207C.C207_SET_ID AND T207C.C207C_VOID_FL = '' AND T207.C207_VOID_FL = '' AND C207C_ATTRIBUTE_VALUE='103086' AND T207.C207_SET_ID LIKE '300.%' AND "+ query;


	db.transaction(function (tx, rs) {
	
		tx.executeSql( query, [],
			function(tx,rs) {
				var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
					results.push(rs.rows.item(i));
				}
				callback(results);
		},
		function(tx,e){showMsgView("009");});
		});
}


function fnGetCollateralFavorites(url,type,callback) {
	var i=0, results = [];
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT DISTINCT T207.C207_SET_ID AS ID, T207.C207_SET_NM AS Name, ? AS URL FROM T207_SET_MASTER T207,T207C_SET_ATTRIBUTE T207C, TBL_FAVORITE WHERE  TBL_FAVORITE.SYSTEM_ID = T207.C207_SET_ID AND T207.C207_SET_ID = T207C.C207_SET_ID AND T207C.C207C_ATTRIBUTE_VALUE = '103086' AND T207C.C901_ATTRIBUTE_TYPE = '103119' AND T207C.C207C_VOID_FL = '' AND TBL_FAVORITE.TYPE = ? ORDER BY TBL_FAVORITE.CREATED_DATE DESC LIMIT 10 ", [url+"/detail/system/",type],
				function(tx,rs) {
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
		},
		function(){showMsgView("009");});
	});
}

function fnCheckCollateralSystems(id, callback) {
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT  T207.c207_set_id FROM t207_set_master T207, t207c_set_attribute t207c WHERE T207.c207_set_id = t207c.c207_set_id AND T207.c207_void_fl  ='' AND t207c.c207c_void_fl  ='' AND C901_ATTRIBUTE_TYPE   = '103119' AND c207c_attribute_value = '103086' AND T207.c207_set_id =?", [id],
			function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
		});
	});
}

