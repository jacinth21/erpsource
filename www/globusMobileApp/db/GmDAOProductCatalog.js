function fnGetGroupDetail (itemID,callback) {

	db.transaction(function (tx, rs) {
			
			tx.executeSql("SELECT C4010_GROUP_ID AS ID, C4010_GROUP_NM AS Name FROM T4010_GROUP WHERE C4010_VOID_FL = '' AND C4010_GROUP_ID=?", [itemID],
					function(tx,rs) {
				   callback(rs.rows.item(0));
				
			},
			function(){showMsgView("009");});
		});
}

function fnGetMappedGroups (url,sysID, callback) {
    db.transaction(function (tx, rs) {
		tx.executeSql("SELECT T4010_GROUP.C4010_GROUP_ID as ID, T901_CODE_LOOKUP.C901_CODE_NM AS Type, T4010_GROUP.C4010_GROUP_NM AS Name, ? AS URL  FROM T4010_GROUP, T901_CODE_LOOKUP WHERE  T4010_GROUP.C4010_VOID_FL = '' AND T4010_GROUP.C4010_PUBLISH_FL='Y' AND T901_CODE_LOOKUP.C901_CODE_ID= T4010_GROUP.C901_GROUP_TYPE AND T4010_GROUP.C207_SET_ID=?", [url+"/detail/group/",sysID],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
		},
		function(){showMsgView("009");});
	});
}

function fnGetPartsForSystem(systemID,callback) {
  	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT T205.C205_PART_NUMBER_ID AS ID, T205.C205_PART_NUM_DESC AS Description, T205.C205_PART_NUMBER_ID || ' ' || T205.C205_PART_NUM_DESC AS Name FROM T205_PART_NUMBER T205, T207_SET_MASTER T207, T205D_PART_ATTRIBUTE T205D WHERE T205.C205_ACTIVE_FL = 'Y' AND T207.C207_SET_ID = T205.C207_SET_SALES_SYSTEM_ID AND T205.C205_PART_NUMBER_ID = T205D.C205_PART_NUMBER_ID AND T205D.C901_ATTRIBUTE_TYPE = '80180' AND T205D.C205D_ATTRIBUTE_VALUE = ? AND T205D.C205D_VOID_FL = '' AND T207.C207_VOID_FL = '' AND T205.C207_SET_SALES_SYSTEM_ID = ?",[intServerCountryCode,systemID],
			function(tx,rs) { 
			    var i=0, results = [];
				for(i=0;i<rs.rows.length;i++) {
					results.push(rs.rows.item(i));
				}
				callback(results);
			},
		function(){showMsgView("009");});
	});
}

function fnGetPartsForSet(url,setID,callback) {
	var query="SELECT PLIST.C205_PART_NUMBER_ID AS ID, PLIST.C205_PART_NUM_DESC AS Description, PLIST.C205_PART_NUMBER_ID || ' ' || PLIST.C205_PART_NUM_DESC AS Name, T208.C208_SET_QTY AS Quantity, ? AS URL, group_concat(t903.c903_upload_file_list) AS FileList FROM (SELECT T205.C205_PART_NUMBER_ID, T205.C205_PART_NUM_DESC FROM T205_PART_NUMBER T205, T205D_PART_ATTRIBUTE T205D WHERE T205.C205_ACTIVE_FL = 'Y' AND T205.C205_PART_NUMBER_ID = T205D.C205_PART_NUMBER_ID AND T205D.C901_ATTRIBUTE_TYPE = '80180' AND T205D.C205D_ATTRIBUTE_VALUE = '" + intServerCountryCode + "' AND T205D.C205D_VOID_FL = '') PLIST, T208_SET_DETAILS T208 LEFT JOIN T903_UPLOAD_FILE_LIST t903 ON (T208.c205_part_number_id = t903.c903_ref_id AND T903.C903_DELETE_FL = '') WHERE C208_VOID_FL = '' AND T208.C205_PART_NUMBER_ID = PLIST.C205_PART_NUMBER_ID AND T208.C208_VOID_FL = '' AND T208.C207_SET_ID = ? GROUP BY PLIST.C205_PART_NUMBER_ID";
	db.transaction(function (tx, rs) {
		tx.executeSql(query,[url+"/detail/part/",setID],
				function(tx,rs) { 
					var i=0, results = [];
					for(i=0;i<rs.rows.length;i++) {
						results.push(rs.rows.item(i));
					}
					callback(results);
			},
		function(){showMsgView("009");});
	});
}

function fnGetPartsForGroup(url,groupID,callback) {

	var query = "SELECT PLIST.C205_PART_NUMBER_ID AS ID, PLIST.C205_PART_NUM_DESC AS Description, PLIST.C205_PART_NUMBER_ID || ' ' || PLIST.C205_PART_NUM_DESC AS Name, '" + url + "/detail/part/' AS URL, group_concat(t903.c903_upload_file_list) AS FileList FROM (SELECT T205.C205_PART_NUMBER_ID, T205.C205_PART_NUM_DESC FROM T205_PART_NUMBER T205, T205D_PART_ATTRIBUTE T205D WHERE T205.C205_ACTIVE_FL = 'Y' AND T205.C205_PART_NUMBER_ID = T205D.C205_PART_NUMBER_ID AND T205D.C901_ATTRIBUTE_TYPE = '80180' AND T205D.C205D_ATTRIBUTE_VALUE = '" + intServerCountryCode + "' AND T205D.C205D_VOID_FL = '') PLIST, T4011_GROUP_DETAIL T4011 LEFT JOIN T903_UPLOAD_FILE_LIST t903 ON (T4011.c205_part_number_id = t903.c903_ref_id AND T903.C903_DELETE_FL = '') WHERE T4011.C205_PART_NUMBER_ID = PLIST.C205_PART_NUMBER_ID AND T4011.C4010_GROUP_ID = '" +groupID+ "' GROUP BY PLIST.C205_PART_NUMBER_ID";
			db.transaction(function (tx, rs) {
				tx.executeSql(query,[],
						function(tx,rs) {
					var i=0, results = [];
					for(i=0;i<rs.rows.length;i++) {

						results.push(rs.rows.item(i));
					}
					callback(results);
				},
			function(){showMsgView("009");});
		});
}

function fnGetPartDetail (url,ID, callback) {

	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT C205_PART_NUMBER_ID AS ID, C205_PART_NUMBER_ID || ' - ' || C205_PART_NUM_DESC AS Name, C205_PART_NUM_DESC AS Desc, ? AS URL FROM T205_PART_NUMBER WHERE T205_PART_NUMBER.C205_ACTIVE_FL = 'Y' AND C205_PART_NUMBER_ID = ?", [url+"detail/part/",ID],
			function(tx,rs) {
				callback(rs.rows.item(0));
			},
		function(){showMsgView("009");});
	});
}

function fnGetMappedSets (url,SysID, callback) {

	db.transaction(function (tx, rs) {
		tx.executeSql("Select t207.c207_set_id AS ID, t207.c207_set_id || ' - ' || t207.c207_set_nm AS Name, t207.c207_set_nm AS Description, (Select c901_code_nm from t901_code_lookup where c901_code_id = t207.c207_category) AS Category, (Select c901_code_nm from t901_code_lookup where c901_code_id = t207.c207_type ) AS Type, (Select c901_code_nm from t901_code_lookup where c901_code_id = t207.c901_hierarchy ) AS Hierarchy,? AS URL from t207_set_master t207,t901_code_lookup t901 where t207.C207_VOID_FL = '' AND t207.c207_category = t901.c901_code_id AND t207.c207_set_sales_system_id = ?", [url+'/detail/set/',SysID],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			callback(results);
		},
		function(){showMsgView("009");});
	});
}

function fnGetSystemDetail (itemID,callback) {

	db.transaction(function (tx, rs) {
			
			tx.executeSql("SELECT C207_SET_ID AS ID, C207_SET_NM AS Name, C207_SET_DESC AS Desc, C207_SET_DETAIL AS Detail FROM T207_SET_MASTER WHERE C207_VOID_FL = '' AND C207_SET_ID=?", [itemID],
					function(tx,rs) {
				   callback(rs.rows.item(0));
				
			},
			function(){showMsgView("009");
                          ;});
		});
}

function fnGetSetDetail (itemID,callback) {

	db.transaction(function (tx, rs) {
			
			tx.executeSql("SELECT C207_SET_ID AS ID, C207_SET_ID || ' - ' || C207_SET_NM AS Name, C207_SET_DESC AS Desc, C207_SET_DETAIL AS Detail FROM T207_SET_MASTER WHERE C207_VOID_FL = '' AND C207_SET_ID=?", [itemID],
					function(tx,rs) {
				   callback(rs.rows.item(0));
			},
			function(){showMsgView("009");
                          ;});
		});
}
