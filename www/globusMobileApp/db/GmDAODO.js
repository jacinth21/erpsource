function fnGetGOPMapping(accIds, callback) {

    var arrAccounts = accIds.split(","),
        searchTerm = new Array();
    for (var i = 0; i < arrAccounts.length; i++) {
        searchTerm.push("C704_ACCOUNT_ID = '" + arrAccounts[i] + "'");
    }

    var query = searchTerm.join(" OR ");

    query = "SELECT DISTINCT C704_ACCOUNT_ID, C101_PARTY_ID FROM T740_GPO_ACCOUNT_MAPPING T740 WHERE " + query + " AND C740_VOID_FL = ''";


    db.transaction(function(tx, rs) {
        tx.executeSql(query, [],
            function(tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function(tx, err) {
                // showMsgView("009");
                showAppError(err, "GmDAODO:fnGetGOPMapping()");
            });
    });
}
//Inserts the status of PDF sync into the Local Table 
function fnInsertPdfFlagInfo(doid, pdfname, pdfflag, tr) {
    db.transaction(function(tx, rs) {
        tx.executeSql("INSERT OR REPLACE INTO  DO_PDF_INFO(DOID, PDFNAME, PDFFL, COUNT) VALUES (?,?,?,?)", [doid, pdfname, pdfflag, tr],
            function(tx, rs) {
                console.log("DO pdf data successfully saved in LocalDB")
            },
            function(e, err) {
                showAppError(err, "GmDAODO:fnInsertPdfFlagInfo()");
            });

    });
}
//Gets the DO PDFs that are not synced to Server 
function fnGetDOPdfData(callback) {
    var cnt = eval(localStorage.getItem("pdffailurecnt")) * 2;
    db.transaction(function(tx, rs) {
        tx.executeSql("SELECT DOID doid, PDFNAME pdfname ,PDFFL pdffl FROM DO_PDF_INFO WHERE PDFFL ='N' and COUNT < ? ", [cnt],
            function(tx, rs) {
                var i = 0;
                results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            })
    });
}


function fnGetGPOToAccountMapping(GPOs, callback) {
    var searchTerm = new Array();
    if (GPOs.length == undefined) {
        GPOs = [GPOs];
    }
    for (var i = 0; i < GPOs.length; i++) {
        searchTerm.push("C101_PARTY_ID = '" + GPOs[i] + "'");
    }
    var query = searchTerm.join(" OR ");

    query = "SELECT C704_ACCOUNT_ID FROM T740_GPO_ACCOUNT_MAPPING WHERE T740.C740_VOID_FL = '' AND (" + query + ")";

    db.transaction(function(tx, rs) {
        tx.executeSql(query, [],
            function(tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function() {
                showMsgView("009");
            });
    });
}

function fnGetSyncedAccountPriceNameList(callback) {
    db.transaction(function(tx, rs) {

        tx.executeSql("SELECT T704_ACCOUNT.C704_ACCOUNT_ID AS ID, C101_PARTY_ID AS GPOID, C704_ACCOUNT_NM AS Name FROM T704_ACCOUNT LEFT JOIN T740_GPO_ACCOUNT_MAPPING ON T704_ACCOUNT.C704_ACCOUNT_ID = T740_GPO_ACCOUNT_MAPPING.C704_ACCOUNT_ID WHERE SYNCED='Y'", [],
            function(tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function(tx, err) {
                showAppError(err, "GmDAODO:fnGetSyncedAccountPriceNameList()");
            });
    });
}


function fnDeleteAccountPrice(Acctid, callback) {
    var searchTerm = new Array();

    for (var i = 0; i < Acctid.length; i++) {
        searchTerm.push("C704_ACCOUNT_ID = '" + Acctid[i] + "'");
    }

    var query = searchTerm.join(" OR ");
    query = "DELETE FROM T705_ACCOUNT_PRICING WHERE " + query;
    db.transaction(function(tx, rs) {
        tx.executeSql(query, [],
            function(tx, rs) {
                callback();
            },
            function(tx, err) {
                showAppError(err, "GmDAODO:fnDeleteAccountPrice()");
            });
    });
}

function fnDeleteGPOPrice(GPOs, callback) {
    var searchTerm = new Array();

    for (var i = 0; i < GPOs.length; i++) {
        searchTerm.push("C101_GPO_ID = '" + GPOs[i] + "'");
    }

    var query = searchTerm.join(" OR ");
    query = "DELETE FROM T705_ACCOUNT_PRICING WHERE " + query;

    db.transaction(function(tx, rs) {
        tx.executeSql(query, [],
            function(tx, rs) {
                callback();
            },
            function(tx, err) {
                showAppError(err, "GmDAODO:fnDeleteGPOPrice()");
            });
    });
}


//Price Logic
function fnGetPriceFor(PartNum, AccountId, callback) {
    var strQuery = "SELECT IFNULL(IFNULL((SELECT T705.C7051_PRICE FROM T705_ACCOUNT_PRICING T705 WHERE T705.<refid> AND T705.C7051_VOID_FL = '' AND T705.C4010_GROUP_ID IN (SELECT T4011.C4010_GROUP_ID FROM T4011_GROUP_DETAIL T4011 WHERE T4011.<pnum>)), (SELECT T705.C7051_PRICE FROM T705_ACCOUNT_PRICING T705 WHERE T705.<refid> AND T705.<pnum> AND T705.C7051_VOID_FL = '')), " +
        "IFNULL((SELECT T705.C7051_PRICE FROM T705_ACCOUNT_PRICING T705 WHERE T705.C704_ACCOUNT_ID = '" + AccountId + "' AND T705.C7051_VOID_FL = '' AND T705.C4010_GROUP_ID IN (SELECT T4011.C4010_GROUP_ID FROM T4011_GROUP_DETAIL T4011 WHERE T4011.<pnum>)), (SELECT T705.C7051_PRICE FROM T705_ACCOUNT_PRICING T705 WHERE T705.C704_ACCOUNT_ID = '" + AccountId + "' AND T705.<pnum> AND T705.C7051_VOID_FL = ''))" +
        " ) AS PRICE";
    var pnum = new RegExp("<pnum>", 'g');
    var refid = new RegExp("<refid>", 'g');
    strQuery = strQuery.replace(pnum, "C205_PART_NUMBER_ID = '" + PartNum + "'");
    fnGetGPOFor(AccountId, function(gpoID) {

        if (gpoID == "")
            strQuery = strQuery.replace(refid, "C704_ACCOUNT_ID = '" + AccountId + "'")

        else
            strQuery = strQuery.replace(refid, "C101_GPO_ID = '" + gpoID + "'")

        db.transaction(function(tx, rs) {
            tx.executeSql(strQuery, [],
                function(tx, rs) {
                    if (rs.rows.length > 0) {
                        callback(rs.rows.item(0).PRICE)
                    }
                },
                function(tx, err) {
                    console.log(JSON.stringify(err));
                    showAppError(JSON.stringify(err), "GmDAODO:fnGetPriceFor()");
                });
        });

    });
}

function fnGetGPOFor(AccountId, callback) {
    db.transaction(function(tx, rs) {
        tx.executeSql("SELECT C101_PARTY_ID FROM T740_GPO_ACCOUNT_MAPPING T740 WHERE T740.C704_ACCOUNT_ID = ?", [AccountId],
            function(tx, rs) {
                if (rs.rows.length > 0) {
                    callback(rs.rows.item(0).C101_PARTY_ID);
                } else
                    callback("")
            },
            function() {
                showMsgView("009");
            });
    });
}

function fnUpdatePriceSyncInfo(AccID, SyncValue, callback) {
    var arrAccounts = AccID.split(","),
        searchTerm = new Array();

    for (var i = 0; i < arrAccounts.length; i++) {
        searchTerm.push("C704_ACCOUNT_ID = '" + arrAccounts[i] + "'");
    }

    var query = searchTerm.join(" OR ");
    query = "UPDATE T704_ACCOUNT SET SYNCED=? WHERE " + query;

    db.transaction(function(tx, rs) {
        tx.executeSql(query, [SyncValue],
            function(tx, rs) {
                callback();
            },
            function() {
                showMsgView("009");
            });
    });

}


function fnGetDOParts(url, txtSearch, callback) {
    var orderby;
    var keyText = txtSearch;

    orderby = 'T205.C205_PART_NUMBER_ID';

    txtSearch = txtSearch.split(' ');
    var searchTerm = [];
    for (var i = 0; i < txtSearch.length; i++) {
        (parseInt(keyText)) ? searchTerm.push("SystemPartName LIKE '" + txtSearch[i] + "%'"): searchTerm.push("SystemPartName LIKE '%" + txtSearch[i] + "%'");
    }
    var query = searchTerm.join(" AND ");

    //    query = "SELECT T205.C205_PART_NUMBER_ID AS ID, T205.C205_PART_NUMBER_ID || ' ' || T205.C205_PART_NUM_DESC AS Name,T205.C205_PRODUCT_MATERIAL AS ProductMaterial, T205.C205_PRODUCT_CLASS AS ProductClass, T205.C205_PRODUCT_FAMILY AS ProductFamily, T205.C205_PART_NUMBER_ID || ' ' || T205.C205_PART_NUM_DESC AS SystemPartName, '" + url + "/detail/part/' AS URL FROM T205_PART_NUMBER T205, T205D_PART_ATTRIBUTE T205D WHERE T205.C205_ACTIVE_FL = 'Y' AND T205.C205_PART_NUMBER_ID = T205D.C205_PART_NUMBER_ID AND T205D.C901_ATTRIBUTE_TYPE = '80180' AND T205D.C205D_ATTRIBUTE_VALUE = '"+data[0].RFSID+"' AND T205D.C205D_VOID_FL = ''  AND " + query + " ORDER BY " + orderby;

    query = "SELECT T205.C205_PART_NUMBER_ID AS ID, T205.C205_PART_NUMBER_ID || ' ' || T205.C205_PART_NUM_DESC AS Name,T205.C205_PRODUCT_MATERIAL AS ProductMaterial, T205.C205_PRODUCT_CLASS AS ProductClass, T205.C205_PRODUCT_FAMILY AS ProductFamily, T205.C205_PART_NUMBER_ID || ' ' || T205.C205_PART_NUM_DESC AS SystemPartName, '" + url + "/detail/part/' AS URL FROM T205_PART_NUMBER T205, T205D_PART_ATTRIBUTE T205D,T901C_RFS_COMPANY_MAPPING T901C WHERE T205.C205_ACTIVE_FL = 'Y' AND T205.C205_PART_NUMBER_ID = T205D.C205_PART_NUMBER_ID AND T205D.C901_ATTRIBUTE_TYPE = '80180' And T205D.C205D_ATTRIBUTE_VALUE = T901C.C901_RFS_ID  AND T901C.C1900_COMPANY_ID = '" + localStorage.getItem("cmpid") + "' AND T205D.C205D_VOID_FL = '' AND T901C.C901C_VOID_FL is null AND " + query + " ORDER BY " + orderby;

    db.transaction(function(tx, rs) {
        tx.executeSql(query, [],
            function(tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function(e, err) {
                showAppError(err, "GmDAODO:fnGetDOParts()");
            });
    });
}


function fnGetAccounts(url, txtSearch, callback) {

    txtSearch = txtSearch.split(' ');
    var searchTerm = [];
    for (var i = 0; i < txtSearch.length; i++) {
        searchTerm.push("AccountName LIKE '%" + txtSearch[i] + "%'");
    }

    var searchVal = searchTerm.join(" AND ");

    var queryUserCnt = "SELECT COUNT( * ) As count FROM T101D_USER_ACCESS";

    // AND T703.C703_ACTIVE_FL = 'Y' 
    // Due to PMT-27056, the above code was remvoed from below query

    var query = "SELECT T704.C703_SALES_REP_ID AS REP, T703.C703_SALES_REP_NAME AS RepName, T704.C704_ACCOUNT_ID AS ID, T704.C704_ACCOUNT_ID || ' ' || T704.C704_ACCOUNT_NM AS Name, T704.C704_ACCOUNT_NM  || ' ' ||  T704.C704_ACCOUNT_ID AS AccountName, T704.SYNCED,T704.C101_DEALER_ID Dealid , T704.C901_COMPANY_ID divCompanyId FROM T704_ACCOUNT T704, T703_SALES_REP T703 WHERE T704.C703_SALES_REP_ID = T703.C703_SALES_REP_ID AND T704.C704_ACTIVE_FL = 'Y' AND T704.C704_VOID_FL = '' AND T703.C703_VOID_FL = '' AND " + searchVal + " ORDER BY T704.C704_ACCOUNT_ID";

    query1 = "SELECT T704.C703_SALES_REP_ID AS REP, T703.C703_SALES_REP_NAME AS RepName, T704.C704_ACCOUNT_ID AS ID, T704.C704_ACCOUNT_ID || ' ' || T704.C704_ACCOUNT_NM AS Name, T704.C704_ACCOUNT_NM  || ' ' ||  T704.C704_ACCOUNT_ID AS AccountName, T704.SYNCED,T704.C101_DEALER_ID Dealid, T704.C901_COMPANY_ID divCompanyId FROM T704_ACCOUNT T704, T703_SALES_REP T703, T101D_USER_ACCESS T101D   WHERE T704.C703_SALES_REP_ID = T703.C703_SALES_REP_ID AND T101D.C704_ACCOUNT_ID = T704.C704_ACCOUNT_ID AND T704.C704_ACTIVE_FL = 'Y' AND T704.C704_VOID_FL = '' AND T703.C703_VOID_FL = '' AND " + searchVal + " ORDER BY T704.C704_ACCOUNT_ID";

    console.log(query1)

    //    db.transaction(function (tx, rs) {
    //        tx.executeSql(queryUserCnt, [],
    //            function (tx, rs) {
    //                console.log(rs.rows.item(0).count > 0);
    //                    tx.executeSql(query1, [],
    //                        function (tx, rs) {
    //                        console.log(rs);
    //                            var i = 0,
    //                                results = [];
    //                            for (i = 0; i < rs.rows.length; i++) {
    //                                results.push(rs.rows.item(i));
    //                            }
    //                            callback(results);
    //                        },
    //                        function (e, err) {
    //                            showMsgView("009");
    //                        });
    //                        });
    //            });
    //
    db.transaction(function (tx, rs) {
        tx.executeSql(queryUserCnt, [],
            function (tx, rs) {
                console.log(rs);
                console.log(rs.rows.item(0).count > 0);
                if (rs.rows.item(0).count > 0 && localStorage.getItem('ORDER_APP_ACCESS') == 'N') {
                    console.log("2");
                    tx.executeSql(query1, [],
                        function (tx, rs) {
                   
                            var i = 0,
                                results = [];
                            for (i = 0; i < rs.rows.length; i++) {
                                results.push(rs.rows.item(i));
                            }
                            callback(results);
                        },
                        function (e, err) {
                            showMsgView("009");
                        });
                } else {
                    tx.executeSql(query, [],
                        function (tx, rs) {

                            var i = 0,
                                results = [];
                            for (i = 0; i < rs.rows.length; i++) {
                                results.push(rs.rows.item(i));
                            }
                            callback(results);
                        },
                        function (e, err) {
                            showMsgView("009");
                        });
                }
            });
    });
}


function fnGetAccountsForPrice(url, txtSearch, callback) {
    txtSearch = txtSearch.split(' ');
    var searchTerm = [];
    for (var i = 0; i < txtSearch.length; i++) {
        searchTerm.push("AccountName LIKE '%" + txtSearch[i] + "%'");
    }

    var searchVal = searchTerm.join(" AND ");

    var queryUserCnt = "SELECT COUNT( * ) As count FROM T101D_USER_ACCESS";


    var query = "SELECT T704.C703_SALES_REP_ID AS REP, T703.C703_SALES_REP_NAME AS RepName, T704.C704_ACCOUNT_ID AS ID, T704.C704_ACCOUNT_ID || ' ' || T704.C704_ACCOUNT_NM AS Name, T704.C704_ACCOUNT_NM  || ' ' ||  T704.C704_ACCOUNT_ID AS AccountName FROM T704_ACCOUNT T704, T703_SALES_REP T703 WHERE T704.C703_SALES_REP_ID = T703.C703_SALES_REP_ID AND T704.C704_ACTIVE_FL = 'Y' AND T704.C704_VOID_FL = '' AND T703.C703_ACTIVE_FL = 'Y' AND T703.C703_VOID_FL = '' AND T704.SYNCED IS NULL AND " + searchVal + " ORDER BY T704.C704_ACCOUNT_ID";

    console.log(query)
    query1 = "SELECT DISTINCT T704.C703_SALES_REP_ID AS REP, T703.C703_SALES_REP_NAME AS RepName, T704.C704_ACCOUNT_ID AS ID, T704.C704_ACCOUNT_ID || ' ' || T704.C704_ACCOUNT_NM AS Name, T704.C704_ACCOUNT_NM  || ' ' ||  T704.C704_ACCOUNT_ID AS AccountName FROM T704_ACCOUNT T704, T703_SALES_REP T703, T101D_USER_ACCESS T101D   WHERE T704.C703_SALES_REP_ID = T703.C703_SALES_REP_ID AND T101D.C704_ACCOUNT_ID = T704.C704_ACCOUNT_ID AND T704.C704_ACTIVE_FL = 'Y' AND T704.C704_VOID_FL = '' AND T703.C703_ACTIVE_FL = 'Y' AND T703.C703_VOID_FL = '' AND T704.SYNCED IS NULL AND " + searchVal + " ORDER BY T704.C704_ACCOUNT_ID";

    console.log(query1)

    db.transaction(function (tx, rs) {
        tx.executeSql(queryUserCnt, [],
            function (tx, rs) {
                console.log(rs);
                console.log(rs.rows.item(0).count > 0);
                if (rs.rows.item(0).count > 0 && localStorage.getItem('ORDER_APP_ACCESS') == 'N') {
                    tx.executeSql(query1, [],
                        function (tx, rs) {
                            var i = 0,
                                results = [];
                            for (i = 0; i < rs.rows.length; i++) {
                                results.push(rs.rows.item(i));
                            }
                            callback(results);
                        },
                        function (e, err) {
                            showMsgView("009");
                        });
                } else {
                    tx.executeSql(query, [],
                        function (tx, rs) {
                            var i = 0,
                                results = [];
                            for (i = 0; i < rs.rows.length; i++) {
                                results.push(rs.rows.item(i));
                            }
                            callback(results);
                        },
                        function (e, err) {
                            showMsgView("009");
                        });
                }
            });

    });
}

function fnSaveDO(DOID, ACCOUNT, JSON, JSONPARTS, COVERINGREPFL, Signature, AdditionalInfo, otherInfo) {
    var userId = localStorage.getItem("userID");
    db.transaction(function(tx, rs) {
        tx.executeSql("INSERT OR REPLACE INTO TBL_DO(DOID, ACCOUNT, USER, JSON, JSONPARTS, STATUS, COVERINGREPFL, SIGNATURE, ADDITIONAL_INFO, OTHER_INFO) VALUES(?,?,?,?,?,'DRAFT',?,?,?,?)", [DOID, ACCOUNT, userId, JSON, JSONPARTS, COVERINGREPFL, Signature, AdditionalInfo, otherInfo],
            function(tx, rs) {
                console.log("updated")
            },
            function(e, err) {
                showAppError(err, "GmDAODO:fnSaveDO()");
            });
    });
}

function fnGetSearchBooked(keyword, callback) {

    var keyword = keyword.split(" "),
        searchTerm = new Array();

    for (var i = 0; i < keyword.length; i++) {
        searchTerm.push("Name LIKE '" + keyword[i] + "'");
    }

    var query = searchTerm.join(" OR ");

    query = "SELECT DO.DOID AS DOID, T704.C704_ACCOUNT_NM AS ACCOUNT, JSON, STATUS, DO.DOID || ' ' || T704.C704_ACCOUNT_NM || ' ' || STATUS AS Name, REASON, COVERINGREPFL FROM TBL_DO DO LEFT JOIN T704_ACCOUNT T704 ON T704.C704_ACCOUNT_ID = DO.ACCOUNT WHERE STATUS IN ('DRAFT','Pending Sync','Rejected') AND DO.USER = " + localStorage.getItem("userID") + " OR " + query;

    db.transaction(function(tx, rs) {
        tx.executeSql(query, [],
            function(tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function() {
                showMsgView("009");
            });
    });
}



function fnGetAllAddress(callback) {
    db.transaction(function(tx, rs) {
        tx.executeSql("SELECT * FROM T106_ADDRESS", [],
            function(tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function() {
                showMsgView("009");
            });
    });
}

function fnGetRepAddressByID(id, callback) {
    db.transaction(function(tx, rs) {
        tx.executeSql("SELECT T703.C703_SALES_REP_NAME AS Name, " +
            "T106.C106_ADDRESS_ID as AddressID, " +
            "T106.C901_ADDRESS_TYPE as AddressType, " +
            "T106.C101_PARTY_ID as PartyID, " +
            "T106.C106_ADD1 as Add1, " +
            "T106.C106_ADD2 as Add2, " +
            "T106.C106_CITY as City, " +
            "T106.C901_STATE as StateID, " +
            "T106.C901_COUNTRY as Country, " +
            "T106.C106_ZIP_CODE as Zip, " +
            "T106.C106_INACTIVE_FL as InactiveFL, " +
            "T106.C106_PRIMARY_FL as PrimaryFL, " +
            "T901.C902_CODE_NM_ALT as State " +
            "FROM T703_SALES_REP T703, T106_ADDRESS T106 " +
            "LEFT JOIN T901_CODE_LOOKUP T901 " +
            "ON T106.C901_STATE = T901.C901_CODE_ID " +
            "WHERE T703.C101_PARTY_ID =T106.C101_PARTY_ID AND T106.C106_INACTIVE_FL <> 'Y' AND T703.C703_SALES_REP_ID=? ORDER BY T106.C106_PRIMARY_FL DESC", [id],
            function(tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function(tx, err) {
                console.log(JSON.stringify(err));
            });
    });
}

function fnGetAcctAddressByID(id, callback) {
    db.transaction(function(tx, rs) {
        tx.executeSql("SELECT C704_SHIP_NAME AS Name, C704_ACCOUNT_ID as AddressID, " +
            "C704_SHIP_ADD1 as Add1, " +
            "C704_SHIP_ADD2 as Add2, " +
            "C704_SHIP_CITY as City, " +
            // "C704_SHIP_STATE as State, " + 
            "C704_SHIP_COUNTRY as Country, " +
            "C704_SHIP_ZIP_CODE as Zip, " +
            // "T901.C901_CODE_ID, " +
            "T901.C902_CODE_NM_ALT as State " +
            "FROM T704_ACCOUNT T704 " +
            "LEFT JOIN T901_CODE_LOOKUP T901 " +
            "ON T704.C704_SHIP_STATE = T901.C901_CODE_ID " +
            "WHERE T704.C704_ACCOUNT_ID=?", [id],
            function(tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function(tx, err) {
                console.log(JSON.stringify(err));
            });
    });
}

function fnGetDistAddressByID(id, callback) {
    db.transaction(function(tx, rs) {
        tx.executeSql("SELECT C701_SHIP_NAME AS Name, C701_DISTRIBUTOR_ID as AddressID, " +
            "C701_SHIP_ADD1 as Add1, " +
            "C701_SHIP_ADD2 as Add2, " +
            "C701_SHIP_CITY as City, " +
            "C701_SHIP_STATE as State, " +
            "C701_SHIP_COUNTRY as Country, " +
            "C701_SHIP_ZIP_CODE as Zip, " +
            // "T901.C901_CODE_ID, " +
            "T901.C902_CODE_NM_ALT as State " +
            "FROM T701_DISTRIBUTOR T701 " +
            "LEFT JOIN T901_CODE_LOOKUP T901 " +
            "ON T701.C701_SHIP_STATE = T901.C901_CODE_ID " +
            "WHERE T701.C701_DISTRIBUTOR_ID=?", [id],
            function(tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function(tx, err) {
                console.log(JSON.stringify(err));
            });
    });
}

function fnGetpartyIDByEmployeeID(id, callback) {
    db.transaction(function(tx, rs) {
        tx.executeSql("SELECT c101_party_id AS Partyid FROM t101_user WHERE c901_user_type IN (300)  AND c901_user_status = 311 AND  c101_user_id=?", [id],
            function(tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function(tx, err) {
                console.log(JSON.stringify(err));
            });
    });
}

function fnGetShipToAddressByID(id, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT c106_address_id as ID, " +
            "c106_add1 as Add1, " +
            "c106_add2 as Add2, " +
            "c106_city as City, " +
            "c901_state as State, " +
            "C901_country as Country, " +
            "C106_ZIP_CODE as Zip " +
            "FROM t106_address " +
            "WHERE c101_party_id=? AND c106_void_fl = '' AND c106_inactive_fl <> 'Y' ORDER BY c106_primary_fl", [id],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function (tx, err) {
                console.log(JSON.stringify(err));
            });
    });
}


function fnGetDestType(callback) {
    if (GM.Global.currentModule == "DO") {
        db.transaction(function (tx, rs) {
            tx.executeSql("SELECT C901_CODE_ID AS ID, C901_CODE_NM AS Name FROM T901_CODE_LOOKUP WHERE C901_CODE_GRP=? AND C901_VOID_FL ='' AND C901_ACTIVE_FL = '1' ORDER BY C901_CODE_SEQ_NO ASC", ["CONSP"],
                function (tx, rs) {
                    var i = 0,
                        results = [];
                    for (i = 0; i < rs.rows.length; i++) {
                        results.push(rs.rows.item(i));
                    }
                    callback(results);
                },
                function () {
                    showMsgView("009");
                });
        });
    } else if (GM.Global.currentModule == "INV") {
        db.transaction(function (tx, rs) {
            tx.executeSql("SELECT C901_CODE_ID AS ID, C901_CODE_NM AS Name FROM T901_CODE_LOOKUP WHERE C901_CODE_GRP=? and C901_CODE_ID in ('4000519', '4000520', '4000521','107800') AND C901_VOID_FL ='' AND C901_ACTIVE_FL = '1' ORDER BY C901_CODE_SEQ_NO ASC", ["DESYTY"],
                function (tx, rs) {
                    var i = 0,
                        results = [];
                    for (i = 0; i < rs.rows.length; i++) {
                        results.push(rs.rows.item(i));
                    }
                    callback(results);
                },
                function () {
                    showMsgView("009");
                });
        });
    }
}

function fnGetReps() {
    //alert("fnGetReps");
}

function fnGetDist(url, txtSearch, callback) {
    txtSearch = txtSearch.split(' ');
    var searchTerm = [];
    for (var i = 0; i < txtSearch.length; i++) {
        searchTerm.push("DistName LIKE '%" + txtSearch[i] + "%'");
    }

    var query = searchTerm.join(" AND ");

   // query = "SELECT C701_DISTRIBUTOR_ID AS ID, C701_DISTRIBUTOR_NAME AS Name, C701_DISTRIBUTOR_NAME  || ' ' ||  C701_DISTRIBUTOR_ID AS DistName FROM T701_DISTRIBUTOR T701 WHERE C701_ACTIVE_FL //= 'Y' AND  T701.C701_DISTRIBUTOR_ID =T101D.  AND C701_VOID_FL = '' AND " + query + " ORDER BY C701_DISTRIBUTOR_NAME";
    
    query = "SELECT DISTINCT T703.C701_DISTRIBUTOR_ID AS ID, (SELECT C701_DISTRIBUTOR_NAME FROM T701_DISTRIBUTOR WHERE C701_DISTRIBUTOR_ID = T703.C701_DISTRIBUTOR_ID and C701_VOID_FL = '') as Name, (SELECT C701_DISTRIBUTOR_NAME || ' ' ||  C701_DISTRIBUTOR_ID FROM T701_DISTRIBUTOR WHERE C701_DISTRIBUTOR_ID = T703.C701_DISTRIBUTOR_ID and C701_VOID_FL = '') as DistName FROM T704_ACCOUNT T704, T703_SALES_REP T703, T101D_USER_ACCESS T101D WHERE T704.C703_SALES_REP_ID = T703.C703_SALES_REP_ID AND T101D.C704_ACCOUNT_ID = T704.C704_ACCOUNT_ID AND T703.C703_VOID_FL = ''AND T704.C704_VOID_FL = '' AND " + query + " ORDER BY Name";
    
    db.transaction(function(tx, rs) {
        tx.executeSql(query, [],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function (e, err) {
                showMsgView("009");
            });
    });
}

function fnGetSalesRep(url, txtSearch, callback) {
    txtSearch = txtSearch.split(' ');
    var searchTerm = [];
    for (var i = 0; i < txtSearch.length; i++) {
        searchTerm.push("SalesRepName LIKE '%" + txtSearch[i] + "%'");
    }

    var searchVal = searchTerm.join(" AND ");
    var queryUserCnt = "SELECT COUNT( * ) As count FROM T101D_USER_ACCESS";

    var query = "SELECT  T703.C703_SALES_REP_ID AS ID, T703.C703_SALES_REP_ID || ' ' || T703.C703_SALES_REP_NAME AS Name, T703.C703_SALES_REP_NAME  || ' ' ||  T703.C703_SALES_REP_ID AS SalesRepName FROM T703_SALES_REP T703 WHERE  T703.C703_ACTIVE_FL = 'Y' AND T703.C703_VOID_FL = '' AND " + searchVal;
    
   /* "SELECT DISTINCT T703.C703_SALES_REP_ID AS ID, T703.C703_SALES_REP_ID || ' ' || T703.C703_SALES_REP_NAME AS Name, T703.C703_SALES_REP_NAME  || ' ' ||  T703.C703_SALES_REP_ID AS SalesRepName FROM T703_SALES_REP T703,T101D_USER_ACCESS T101D,T704_ACCOUNT T704 WHERE   T704.C704_ACCOUNT_ID = T101D.C704_ACCOUNT_ID AND T703.C703_SALES_REP_ID = T704.C703_SALES_REP_ID and T703.C703_ACTIVE_FL = 'Y' AND T703.C703_VOID_FL = '' AND  " + searchVal;*/

    var query1 = "SELECT DISTINCT T703.C703_SALES_REP_ID AS ID,T703.C703_SALES_REP_ID|| ' '|| T703.C703_SALES_REP_NAME AS Name,T703.C703_SALES_REP_NAME|| ' '|| T703.C703_SALES_REP_ID AS SalesRepName FROM T703_SALES_REP T703,T101D_USER_ACCESS T101D,T704_ACCOUNT T704 WHERE T704.C704_ACCOUNT_ID = T101D.C704_ACCOUNT_ID AND T703.C703_SALES_REP_ID = T704.C703_SALES_REP_ID AND T703.C703_ACTIVE_FL    = 'Y' and t703.c703_void_fl      = '' AND " + searchVal + " UNION SELECT DISTINCT T703.C703_SALES_REP_ID AS ID,T703.C703_SALES_REP_ID|| ' '|| T703.C703_SALES_REP_NAME AS Name, T703.C703_SALES_REP_NAME|| ' ' || T703.C703_SALES_REP_ID AS SalesRepName FROM T703_SALES_REP T703,t101d_user_access t101d,T704_ACCOUNT T704,t704a_account_rep_mapping T704A WHERE T704.C704_ACCOUNT_ID = T101D.C704_ACCOUNT_ID and t703.c703_sales_rep_id = t704.c703_sales_rep_id and t704.c704_account_id = t704a.c704_account_id AND T703.C703_ACTIVE_FL    = 'Y'AND T703.C703_VOID_FL      = '' AND  " + searchVal; 
    db.transaction(function (tx, rs) {
        tx.executeSql(queryUserCnt, [],
            function (tx, rs) {
                console.log(rs);
                console.log(rs.rows.item(0).count > 0);
                if (rs.rows.item(0).count > 0 && localStorage.getItem('ORDER_APP_ACCESS') == 'N') {
                    tx.executeSql(query1, [],
                        function (tx, rs) {
                            var i = 0,
                                results = [];
                            for (i = 0; i < rs.rows.length; i++) {
                                results.push(rs.rows.item(i));
                            }
                            callback(results);
                        },
                        function (e, err) {
                            showMsgView("009");
                        });
                } else {
        tx.executeSql(query, [],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function (e, err) {
                showMsgView("009");
            });
                }
            });
    });
}
/*fnGetEmployee() - Function to get the employee list 
 */
function fnGetEmployee(url, txtSearch, callback) {
    txtSearch = txtSearch.split(' ');
    var searchTerm = [];
    for (var i = 0; i < txtSearch.length; i++) {
        searchTerm.push("EmployeeName LIKE '%" + txtSearch[i] + "%'");
    }

    var query = searchTerm.join(" AND ");

    query = "SELECT t101U.C101_USER_ID ID , t101U.C101_USER_ID ||' '|| t101U.C101_USER_F_NAME ||' '|| t101U.C101_USER_L_NAME Name,t101U.C101_USER_F_NAME ||' '|| t101U.C101_USER_ID EmployeeName , t101P.C101_PARTY_ID  PARTYID FROM T101_USER t101U, T101_PARTY t101P WHERE  t101P.C101_PARTY_ID = t101U.C101_PARTY_ID AND t101P.C101_VOID_FL = '' AND " + query;

    db.transaction(function (tx, rs) {
        tx.executeSql(query, [],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function (e, err) {
                showMsgView("009");
            });
    });
}

/*fnGetShippingAccount() - Function to get the SHipping account list
 */
function fnGetShippingAccount(url, txtSearch, callback) {
    txtSearch = txtSearch.split(' ');
    var searchTerm = [];
    for (var i = 0; i < txtSearch.length; i++) {
        searchTerm.push("PartyName LIKE '%" + txtSearch[i] + "%'");
    }

    var query = searchTerm.join(" AND ");
    //4000641 - party type for Shipping account
    query = "SELECT c101_party_id ID, c101_party_nm Name, c101_party_id || ' ' ||c101_party_nm PartyName FROM t101_party WHERE c901_party_type = '4000641' AND c101_void_fl = '' AND c101_active_fl != 'N' AND " + query;

    db.transaction(function (tx, rs) {
        tx.executeSql(query, [],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function (e, err) {
                showMsgView("009");
            });
    });
}


function fnCheckDOID(DO, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT * FROM TBL_DO WHERE DOID LIKE ?", [DO + "%"],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function (e, err) {
                showAppError(err, "GmDAODO:fnCheckDOID()");
            });
    });
}

function fnGetDOInfo(DO, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT * FROM TBL_DO WHERE DOID = ?", [DO],
            function (tx, rs) {
                if (rs.rows.length > 0)
                    callback(rs.rows.item(0));
                else
                    callback(0);
            },
            function (e, err) {
                showMsgView("009");
            });
    });
}


function fnGetDoBookedList(callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT * FROM TBL_DOBOOKED", [],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            }, null);
    });
}

function fnVoidDO(DOID, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("UPDATE TBL_DOBOOKED SET VOID_FL ='Y' WHERE DOID = ?", [DOID],
            function (tx, rs) {
                callback();
            },
            function (tx, err) {
                console.log(JSON.stringify(err));

            });
    });
}

function fnUpdateDOStatus(DO, Status) {
    db.transaction(function (tx, rs) {
        tx.executeSql("UPDATE TBL_DO SET STATUS = ? WHERE  DOID = ?", [Status, DO],
            function (tx, rs) {

            },
            function (tx, err) {
               console.log(JSON.stringify(err));
               showAppError(err, "GmDAODO:fnUpdateDOStatus()");

            });
    });
}

function fnDeleteDO(DOID) {
    db.transaction(function (tx, rs) {
        tx.executeSql("DELETE FROM TBL_DO WHERE  DOID = ?", [DOID],
            function (tx, rs) {},
            function (tx, err) {
               console.log(JSON.stringify(err));
               showAppError(err, "GmDAODO:fnDeleteDO()");

            });
    });
}


function fnGetAccountName(accid, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT T704.C704_ACCOUNT_NM AS Name FROM T704_ACCOUNT T704 WHERE T704.C704_ACCOUNT_ID = ?", [accid],
            function (tx, rs) {
                callback(rs.rows.item(0).Name);
            },
            function (tx, err) {
                console.log(JSON.stringify(err));

            });
    });
}


function fnCheckPendingSync() {
    var userId = localStorage.getItem("userID");
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT * FROM TBL_DO WHERE USER = ? AND STATUS = 'Pending Sync'", [userId],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                fnAutoBookDO(0, results);
            },
            function (tx, err) {
               console.log(JSON.stringify(err));
               showAppError(err, "GmDAODO:fnCheckPendingSync()");

            });
    });
}

function fnCheckDOPDFPendingSync() {
    fnGetDOPdfData(function (data) {
        if (data.length > 0)
            syncDOPdf(data);
    })
}

function fnGetRepInfo(RepID, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT * FROM T703_SALES_REP WHERE C703_SALES_REP_ID = ?", [RepID],
            function (tx, rs) {
                callback(rs.rows.item(0));
            },
            function (tx, err) {
                console.log(JSON.stringify(err));

            });
    });
}

function fnGetUserDOID(PartyID, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT * FROM T703_SALES_REP WHERE C101_PARTY_ID = ?", [PartyID],
            function (tx, rs) {
                callback(rs.rows.item(0));
            },
            function (tx, err) {
                console.log(JSON.stringify(err));

            });
    });
}

function fnGetCountryList(callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT C901_CODE_ID AS ID,C901_CODE_NM AS Name FROM T901_CODE_LOOKUP WHERE C901_CODE_GRP =? AND C901_VOID_FL = '' AND C901_ACTIVE_FL = 1 ORDER BY Name", ["CNTY"],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function (tx, err) {
                showMsgView("009");
            });
    });

}

function fnGetStateList(callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT C901_CODE_ID AS ID,C901_CODE_NM AS Name FROM T901_CODE_LOOKUP WHERE C901_CODE_GRP =? AND C901_VOID_FL = '' AND C901_ACTIVE_FL = 1 ORDER BY Name", ["STATE"],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function (tx, err) {
                showMsgView("009");
            });
    });

}




function fnSalesRepDfltAddress(ID, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT T703.C703_SALES_REP_NAME AS Name, " +
            "T106.C106_ADDRESS_ID as AddressID, " +
            "T106.C901_ADDRESS_TYPE as AddressType, " +
            "T106.C101_PARTY_ID as PartyID, " +
            "T106.C106_ADD1 as Add1, " +
            "T106.C106_ADD2 as Add2, " +
            "T106.C106_CITY as City, " +
            "T106.C901_STATE as State, " +
            "T106.C901_COUNTRY as Country, " +
            "T106.C106_ZIP_CODE as Zip, " +
            // "T901.C901_CODE_ID, " +
            "T901.C902_CODE_NM_ALT as State " +
            "FROM T703_SALES_REP T703, T106_ADDRESS T106 " +
            "LEFT JOIN T901_CODE_LOOKUP T901 " +
            "ON T106.C901_STATE = T901.C901_CODE_ID " +
            "WHERE T703.C101_PARTY_ID=T106.C101_PARTY_ID AND T106.C106_PRIMARY_FL = 'Y' AND T703.C703_SALES_REP_ID=?", [ID],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function (tx, err) {
                console.log(JSON.stringify(err));
            });
    });
}

function fnGetPendingConfirm(callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT DOID AS ID FROM TBL_DO WHERE STATUS='Pending CS Confirmation'", [],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function (tx, err) {
                showMsgView("009");
            });
    });
}

function fnSaveNewAddress(accJSON, callback) {
    var userId = localStorage.getItem("userID");
    var ADDRID = accJSON["addrid"];
    var ADDRTYPE = accJSON["addresstype"];
    var PARTYID = accJSON["partyid"];
    var BILLADD1 = accJSON["billadd1"];
    var BILLADD2 = accJSON["billadd2"];
    var BILLCITY = accJSON["billcity"];
    var STATE = accJSON["state"];
    var COUNTRY = accJSON["country"];
    var ZIP = accJSON["zipcode"];
    var ACTIVEFL = accJSON["activeflag"];
    var PRIMARYFL = accJSON["primaryflag"];
    var VOIDFL = accJSON["voidfl"];
    //	var STATENM = accJSON["statename"];
    //	var COUNTRYNM = accJSON["countryname"];
    //	var ADDRTYPENM = accJSON["addresstypename"];
    db.transaction(function (tx, rs) {
        tx.executeSql("INSERT OR REPLACE INTO T106_ADDRESS (C106_ADDRESS_ID,C901_ADDRESS_TYPE,C101_PARTY_ID," +
            " C106_ADD1,C106_ADD2,C106_CITY,C901_STATE,C901_COUNTRY,C106_ZIP_CODE,C106_PRIMARY_FL," +
            " C106_INACTIVE_FL, C106_VOID_FL) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)", [ADDRID, ADDRTYPE, PARTYID, BILLADD1, BILLADD2, BILLCITY, STATE, COUNTRY, ZIP, PRIMARYFL, ACTIVEFL, VOIDFL],
            function (tx, rs) {

                console.log("updated");
                callback();
            },
            function (e, err) {
                showMsgView("009");
            });
    });


}




function fnGetSurgeryLevel(GRP, callback) {

    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT C901_CODE_ID AS ID, C901_CODE_NM AS Name FROM T901_CODE_LOOKUP WHERE C901_CODE_GRP=? AND C901_VOID_FL ='' AND C901_ACTIVE_FL = '1' ORDER BY C901_CODE_SEQ_NO ASC", [GRP],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function () {
                showMsgView("009");
            });
    });
}



function fnGetAllDOIDs(callback) {

    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT DOID FROM TBL_DO", [],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function () {
                showMsgView("009");
            });
    });
}


function fnGetAccountInfo(AccID, callback) {

    query = "SELECT T704.C703_SALES_REP_ID AS REP, T703.C703_SALES_REP_NAME AS RepName, T704.C704_ACCOUNT_ID AS ID, T704.C704_ACCOUNT_ID || ' ' || T704.C704_ACCOUNT_NM AS Name, T704.C704_ACCOUNT_NM  || ' ' ||  T704.C704_ACCOUNT_ID AS AccountName FROM T704_ACCOUNT T704, T703_SALES_REP T703 WHERE T704.C703_SALES_REP_ID = T703.C703_SALES_REP_ID AND T704.C704_ACTIVE_FL = 'Y' AND T704.C704_VOID_FL = '' AND T703.C703_ACTIVE_FL = 'Y' AND T703.C703_VOID_FL = '' AND T704.C704_ACCOUNT_ID = " + AccID;

    db.transaction(function (tx, rs) {
        tx.executeSql(query, [],
            function (tx, rs) {
                if (rs.rows.length > 0)
                    callback(rs.rows.item(0));
                else
                    callback(0)
            },
            function (e, err) {
                alert(JSON.stringify(err))
            });
    });
}

function fnGetCaseID(acctid, repid, sdate, callback) {
    console.log(acctid + " " + repid + " " + sdate);
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT CASE_ID AS Name, CASE_INFO_ID AS ID FROM VW_CASE_INFORMATION  WHERE SURGERY_DATE=? AND SALES_REP_ID=? AND ACCOUNT_ID=?", [sdate, repid, acctid],
            function (tx, rs) {
                if (rs.rows.length > 0) {
                    var results = [];
                    for (var i = 0; i < rs.rows.length; i++) {
                        results.push(rs.rows.item(i));
                    }
                    callback(results);
                } else {
                    callback("");
                }
            },
            function (e, err) {
                showMsgView("009");
            });
    });
}

function fnGetSurgeon(cainid, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT ATTRIBUTE_VALUE AS SNAME FROM VW_CASE_ATTRIBUTE WHERE CASE_INFO_ID=? AND ATTRIBUTE_TYPE=103640", [cainid],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function (e, err) {
                console.log(JSON.stringify(err))
            });
    });
}

function fnGetUDINumber(partid, callback) {

    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT C2060_DI_NUMBER diid, UDI_PATTERN pattern FROM T205_PART_NUMBER WHERE C205_PART_NUMBER_ID =?", [partid],
            function (tx, rs) {
                if (rs.rows.length > 0) {
                    var results = [];
                    for (var i = 0; i < rs.rows.length; i++) {
                        results.push(rs.rows.item(i));
                    }
                    callback(results);
                }

            },
            function (e, err) {
                console.log(JSON.stringify(err))
            });
    });
}

//Get only parts that have DI number
function fnGetUDIParts(url, txtSearch, callback) {

    var orderby;
    var keyText = txtSearch;

    orderby = 'T205.C205_PART_NUMBER_ID';

    txtSearch = txtSearch.split(' ');
    var searchTerm = [];
    for (var i = 0; i < txtSearch.length; i++) {
        (parseInt(keyText)) ? searchTerm.push("SystemPartName LIKE '" + txtSearch[i] + "%'"): searchTerm.push("SystemPartName LIKE '%" + txtSearch[i] + "%'");
    }
    var query = searchTerm.join(" AND ");

    query = "SELECT T205.C205_PART_NUMBER_ID AS ID, T205.C205_PART_NUMBER_ID || ' ' || T205.C205_PART_NUM_DESC AS Name,T205.C205_PRODUCT_MATERIAL AS ProductMaterial, T205.C205_PRODUCT_CLASS AS ProductClass, T205.C205_PRODUCT_FAMILY AS ProductFamily, T205.C205_PART_NUMBER_ID || ' ' || T205.C205_PART_NUM_DESC AS SystemPartName, '" + url + "/detail/part/' AS URL FROM T205_PART_NUMBER T205, T205D_PART_ATTRIBUTE T205D WHERE T205.C205_ACTIVE_FL = 'Y' AND T205.C205_PART_NUMBER_ID = T205D.C205_PART_NUMBER_ID AND T205D.C901_ATTRIBUTE_TYPE = '80180' AND T205D.C205D_ATTRIBUTE_VALUE = '" + intServerCountryCode + "' AND T205D.C205D_VOID_FL = '' AND IFNULL(T205.C2060_DI_NUMBER,'') != '' AND " + query + " ORDER BY " + orderby;

    db.transaction(function (tx, rs) {
        tx.executeSql(query, [],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function (e, err) {
                showMsgView("009");
            });
    });
}

function fnChk_DI_Updated(callback) {

    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT C2060_DI_NUMBER FROM T205_PART_NUMBER WHERE IFNULL(C2060_DI_NUMBER,'') != '' LIMIT 1 ", [],
            function (tx, rs) {
                if (rs.rows.length > 0) {

                    callback(true);
                } else {
                    callback(false);
                }
            },
            function (e, err) {
                console.log(JSON.stringify(err))
            });
    });
}

function fnGetCodeList(COMPANYID, CODEGROUP, callback) {
	var query = "";
	if (COMPANYID != undefined && COMPANYID == '1000') {
		// to exclude TNT and Courier from shipping address carrier dropdown 
		query = " SELECT C901_CODE_ID ID ,C901_CODE_NM Name ,C902_CODE_NM_ALT CODENMALT,C901_CONTROL_TYPE CONTROLTYP,C901_CODE_SEQ_NO CODESEQNO FROM T901_CODE_LOOKUP T901 WHERE C901_CODE_GRP =? AND T901.C901_ACTIVE_FL = '1' AND C901_VOID_FL ='' AND T901.C901_CODE_ID NOT IN (102040,5035) AND T901.C901_CODE_ID NOT IN (SELECT T901D.C901_CODE_ID FROM T901D_CODE_LKP_COMPANY_EXCLN T901D, T901_CODE_LOOKUP T901 WHERE T901D.C901D_VOID_FL ='' AND T901D.C1900_COMPANY_ID = ? AND T901D.C901_CODE_ID     = T901.C901_CODE_ID AND T901.C901_CODE_GRP = ? AND T901.C901_ACTIVE_FL = '1' AND T901.C901_VOID_FL   ='' ) ORDER BY T901.C901_CODE_SEQ_NO, UPPER(T901.C901_CODE_NM)";
	}else{
		query = " SELECT C901_CODE_ID ID ,C901_CODE_NM Name ,C902_CODE_NM_ALT CODENMALT,C901_CONTROL_TYPE CONTROLTYP,C901_CODE_SEQ_NO CODESEQNO FROM T901_CODE_LOOKUP T901 WHERE C901_CODE_GRP =? AND T901.C901_ACTIVE_FL = '1' AND C901_VOID_FL ='' AND T901.C901_CODE_ID NOT IN (SELECT T901D.C901_CODE_ID FROM T901D_CODE_LKP_COMPANY_EXCLN T901D, T901_CODE_LOOKUP T901 WHERE T901D.C901D_VOID_FL ='' AND T901D.C1900_COMPANY_ID = ? AND T901D.C901_CODE_ID     = T901.C901_CODE_ID AND T901.C901_CODE_GRP = ? AND T901.C901_ACTIVE_FL = '1' AND T901.C901_VOID_FL   ='' ) ORDER BY T901.C901_CODE_SEQ_NO, UPPER(T901.C901_CODE_NM)";
	}	
    db.transaction(function (tx, rs) {
        tx.executeSql(query, [CODEGROUP, COMPANYID, CODEGROUP],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function (e, err) {
                console.log(JSON.stringify(err))
            });
    });
}

function fnGetModeList(CARRIERID, COMPANYID, callback) {
	var query = "";
    var modeCompany =  localStorage.getItem("ModeExclusion"); 
    if(modeCompany == undefined || modeCompany == null || modeCompany == "")
    {
            modeCompany = "4000612,4000608,4000606,5043,5003,5038";
    }    
    if (COMPANYID != undefined && COMPANYID == '1000') {
		// to exclude Next Day Air Early A.M., Saturday-Next Day Air Early A.M, First Overnight, Saturday-First Over, Early AM, Same Day from shipping address mode dropdown
        query = "  SELECT T901A.C901_CODE_ID ID  FROM T901A_LOOKUP_ACCESS T901A WHERE T901A.C901_CODE_ID NOT IN ("+modeCompany+") AND C901_ACCESS_CODE_ID = ? AND c1900_company_id = ? ";
	}else{
		query = "  SELECT T901A.C901_CODE_ID ID  FROM T901A_LOOKUP_ACCESS T901A WHERE C901_ACCESS_CODE_ID = ? AND c1900_company_id = ? ";
	}
    db.transaction(function (tx, rs) {
        tx.executeSql(query, [CARRIERID, COMPANYID],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function (e, err) {
                console.log(JSON.stringify(err))
            });
    });
}

function fnGetCodeNamebyCodeGroup(CODEGROUP, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql(" SELECT C901_CODE_ID ID ,C901_CODE_NM Name ,C902_CODE_NM_ALT CODENMALT,C901_CONTROL_TYPE CONTROLTYP,C901_CODE_SEQ_NO CODESEQNO FROM T901_CODE_LOOKUP T901 WHERE C901_CODE_GRP IN (?) AND T901.C901_ACTIVE_FL = '1' AND C901_VOID_FL ='' ORDER BY T901.C901_CODE_SEQ_NO, UPPER(T901.C901_CODE_NM)", [CODEGROUP],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function (e, err) {
                console.log(JSON.stringify(err))
            });
    });
}

function fnGetCodeNamebyId(codeid, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql(" SELECT C901_CODE_NM Name  FROM T901_CODE_LOOKUP T901 WHERE c901_code_id = ?", [codeid],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results[0]);
            },
            function (e, err) {
                console.log(JSON.stringify(err))
            });
    });
}

/*fnGetRuleForDOSurgery(): Get the surgery details tab fields in DO screen basd on the rule and loggined company
 */
function fnGetRuleForDOSurgery(callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT C906_RULE_ID AS RULEID,C906_RULE_VALUE AS RULEVALUE FROM T906_RULES WHERE C906_RULE_GRP_ID = 'SURGERYDETAIL' AND C1900_COMPANY_ID = ? AND C906_VOID_FL = '' ", [localStorage.getItem("cmpid")],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function () {
                showMsgView("009");
            });
    });
}
/*fnGetRuleValueByCompany(): Function to get the rule value based on the company
  parameter:Rule id,Rule group,company id 
*/
function fnGetRuleValueByCompany(Ruleid, RuleGrp, Companyid, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT C906_RULE_VALUE AS RULEVALUE FROM T906_RULES WHERE C906_RULE_ID =? AND C906_RULE_GRP_ID = ? AND C1900_COMPANY_ID = ? AND C906_VOID_FL= '' ", [Ruleid, RuleGrp, Companyid],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function () {
                showMsgView("009");
            });
    });
}

/*fnGetRuleValue():  common Function to get the rule value 
  parameter:Rule id,Rule group
*/
function fnGetRuleValue(Ruleid, RuleGrp, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT C906_RULE_VALUE AS RULEVALUE FROM T906_RULES WHERE C906_RULE_ID =? AND C906_RULE_GRP_ID = ? AND C906_VOID_FL= '' ", [Ruleid, RuleGrp],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function () {
                showMsgView("009");
            });
    });
}

function fnGetAcctDefultAddressByID(id, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT C704_SHIP_NAME AS Name, C704_ACCOUNT_ID as AddressID, " +
            "C704_SHIP_ADD1 as Add1, " +
            "C704_SHIP_ADD2 as Add2, " +
            "C704_SHIP_CITY as City, " +
            // "C704_SHIP_STATE as State, " + 
            "C704_SHIP_COUNTRY as Country, " +
            "C704_SHIP_ZIP_CODE as Zip, " +
            // "T901.C901_CODE_ID, " +
            "T901.C902_CODE_NM_ALT as State " +
            "FROM T704_ACCOUNT T704 , T106_ADDRESS T106 " +
            "LEFT JOIN T901_CODE_LOOKUP T901 " +
            "ON T704.C704_SHIP_STATE = T901.C901_CODE_ID " +
            "WHERE T704.C101_PARTY_ID=T106.C101_PARTY_ID AND T106.C106_PRIMARY_FL = 'Y' AND T704.C704_ACCOUNT_ID=?", [id],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function (tx, err) {
                console.log(JSON.stringify(err));
            });
    });
}

function fnGetCodeName(codeid, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql(" SELECT C901_CODE_NM Name  FROM T901_CODE_LOOKUP T901 WHERE c901_code_id = ?", [codeid],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                return (results[0]);
            },
            function (e, err) {
                console.log(JSON.stringify(err))
            });
    });
}

/* fnGetRFSCompanymappingID():   Function to get the RFS value based on the company
   parameter:Rule id,Rule group
*/
function fnGetRFSCompanymappingID(COMPANYID, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT C901_RFS_ID RFSID FROM T901C_RFS_COMPANY_MAPPING WHERE C1900_COMPANY_ID = ? AND C901C_VOID_FL IS NULL", [COMPANYID],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function () {
                showMsgView("009");
            });
    });
}

/* fnCodelookupCountValidation():Function to get the count from T901_CODE_LOOKUP local table to show the validation while clicking DO module icon
 */
function fnCodelookupCountValidation() {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT COUNT(*) count FROM T901_CODE_LOOKUP WHERE C901_CODE_GRP IN (?,?)", ['CONSP', 'ADDTP'],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                GM.Global.CodelookupCount = results[0].count;
            },
            function (e, err) {
                console.log(JSON.stringify(err))
            });
    });
}

/* fnCodelookupExclusionCountValidation():Function to get the count from T901D_CODE_LKP_COMPANY_EXCLN local table to show the validation while clicking DO module icon
 */
function fnCodelookupExclusionCountValidation() {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT COUNT(*) count FROM T901D_CODE_LKP_COMPANY_EXCLN WHERE C901_CODE_ID IN (SELECT C901_CODE_ID FROM T901_CODE_LOOKUP WHERE C901_CODE_GRP IN (?,?,?))", ['DCAR', 'DMODE', 'STATE'],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                GM.Global.CodelookupExclusionCount = results[0].count;
            },
            function (e, err) {
                console.log(JSON.stringify(err))
            });
    });
}

/* fnCodelookupAccessCountValidation():Function to get the count from T901A_LOOKUP_ACCESS local table to show the validation while clicking DO module icon
 */
function fnCodelookupAccessCountValidation() {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT COUNT(*) count FROM T901A_LOOKUP_ACCESS WHERE C901_CODE_ID IN (SELECT C901_CODE_ID FROM T901_CODE_LOOKUP WHERE C901_CODE_GRP IN (?,?))", ['DCAR', 'DMODE'],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                GM.Global.CodelookupAccessCount = results[0].count;
            },
            function (e, err) {
                console.log(JSON.stringify(err))
            });
    });
}

/* fnRulesCountValidation():Function to get the count from T906_RULES local table to show the validation while clicking DO module icon.
 */
function fnRulesCountValidation() {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT COUNT(*) count FROM T906_RULES WHERE C906_RULE_GRP_ID IN (?,?)", ['SURGERYDETAIL', 'DO_ORDER'],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                GM.Global.RulesCount = results[0].count;
            },
            function (e, err) {
                console.log(JSON.stringify(err))
            });
    });
}

/* fnPartRFSCountValidation():Function to get the count from T901C_RFS_COMPANY_MAPPING local table to show the sync validation while clicking DO module icon.
 */
function fnPartRFSCountValidation() {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT COUNT(*) count FROM T901C_RFS_COMPANY_MAPPING WHERE C901C_VOID_FL IS NULL", [],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                GM.Global.PartRFSCount = results[0].count;
            },
            function (e, err) {
                console.log(JSON.stringify(err))
            });
    });
}

function fnUpdateOldDoID(NEWDOID, OLDDOID) {
    console.log(NEWDOID)
    console.log(OLDDOID)
    db.transaction(function (tx, rs) {
        tx.executeSql("UPDATE TBL_DO SET OLDDOID = ? WHERE  DOID = ?", [OLDDOID, NEWDOID],
            function (tx, rs) {

            },
            function (tx, err) {
                showAppError(err, "GmDAODO:fnUpdateOldDoID()");

            });
    });
}


function fnGetOldDOID (DOID, callback) {
	db.transaction(function (tx, rs) {
        tx.executeSql("SELECT OLDDOID FROM TBL_DO WHERE DOID =? ", [DOID],
			function(tx,rs) {
            console.log(rs);
				if(rs.rows.length > 0)
					callback(rs.rows.item(0));
				else
					callback();
		},function (tx,err) {
			console.log(JSON.stringify(err));
		});
	});
}

function fnGetSurgNameList(url, txtSearch, callback) {
    txtSearch = txtSearch.split(' ');
    var searchTerm = [];
    for (var i = 0; i < txtSearch.length; i++) {
        searchTerm.push("NPIName LIKE '%" + txtSearch[i] + "%'");
    }
    var query = searchTerm.join(" AND ");   

     query = "SELECT T6600.C6600_SURGEON_NPI_ID AS ID, t101.c101_party_id AS PARTYID, t101.c101_first_nm || ' ' || t101.c101_last_nm || ' ' || t101.c101_middle_initial ||', ' ||IFNULL(t901.c902_code_nm_alt,'') AS Name, t101.c101_first_nm || ' ' || t101.c101_last_nm AS repname, t101.c101_first_nm || ' ' || t101.c101_last_nm || ' ' || IFNULL(T6600.C6600_SURGEON_NPI_ID,'') AS NPIName FROM T101_PARTY t101 LEFT OUTER JOIN t6600_party_surgeon t6600 ON t101.c101_party_id = t6600.c101_party_surgeon_id AND t6600.C6600_VOID_FL = '' LEFT OUTER JOIN t106_address t106 ON t101.c101_party_id = t106.c101_party_id  and t106.c106_void_fl ='' AND t106.c106_primary_fl = 'Y' LEFT OUTER JOIN t901_code_lookup t901 ON t901.c901_code_id = t106.c901_state and t901.c901_void_fl ='' WHERE t101.c901_party_type = '7000' AND t101.C101_VOID_FL ='' AND (t101.c101_active_fl = '' OR t101.c101_active_fl = 'Y') AND " + query + " ORDER BY t6600.C6600_SURGEON_NPI_ID";
    db.transaction(function(tx, rs) {
        tx.executeSql(query, [],
            function(tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function(e, err) {
                showMsgView("009");
            });
    });
}

function fnGetNPIIdList(url, txtSearch, callback) {
    txtSearch = txtSearch.split(' ');
    var searchTerm = [];
    for (var i = 0; i < txtSearch.length; i++) {
        searchTerm.push("NPIName LIKE '%" + txtSearch[i] + "%'");
    }
    var query = searchTerm.join(" AND ");

    query = "SELECT t6600.C6600_SURGEON_NPI_ID AS Name, t101.c101_party_id AS PARTYID, t101.c101_first_nm || ' ' || t101.c101_last_nm || ' ' || t101.c101_middle_initial ||', ' || IFNULL(t901.c902_code_nm_alt,'') AS ID, t101.c101_first_nm || ' ' || t101.c101_last_nm AS repname, t101.c101_first_nm || ' ' || t101.c101_last_nm || ' ' || IFNULL(T6600.C6600_SURGEON_NPI_ID,'') AS NPIName FROM T101_PARTY t101 LEFT OUTER JOIN t6600_party_surgeon t6600 ON t101.c101_party_id = t6600.c101_party_surgeon_id AND t6600.C6600_VOID_FL = '' LEFT OUTER JOIN t106_address t106 ON t101.c101_party_id = t106.c101_party_id and t106.c106_void_fl  = '' AND t106.c106_primary_fl = 'Y' LEFT OUTER JOIN t901_code_lookup    t901  ON t901.c901_code_id = t106.c901_state and t901.c901_void_fl  = '' WHERE t101.c901_party_type = '7000' AND t101.C101_VOID_FL  = '' AND (t101.c101_active_fl  = '' OR t101.c101_active_fl = 'Y') AND " + query + " ORDER BY t6600.C6600_SURGEON_NPI_ID";
    db.transaction(function(tx, rs) {
        tx.executeSql(query, [],
            function(tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function(e, err) {
                showMsgView("009");
            });
    });
}

function fnSaveNPISurgeonDetail(ACCOUNTID, REPID, NPIID, SURGEONNAME,ORDERDATE) {
    db.transaction(function(tx, rs) {
        tx.executeSql("INSERT OR REPLACE INTO TBL_DO_NPI_SURGEON_INFO(ACCOUNTID, REPID, NPIID, SURGEONNAME,ORDERDATE) VALUES(?,?,?,?,?)", [ ACCOUNTID, REPID, NPIID, SURGEONNAME,ORDERDATE],
            function(tx, rs) {
                console.log("updated")
            },
            function(e, err) {
                showMsgView("009");
            });
    });
}

function fnGetPreviousNPISurgeonDetails(ACCID,REPID, callback) {
    var orderby;
    orderby = 'ORDERDATE DESC';
    var query ="SELECT DISTINCT NPIID AS prev_npiid,SURGEONNAME AS prev_surgeonnm FROM TBL_DO_NPI_SURGEON_INFO WHERE REPID="+REPID+" ORDER BY " + orderby;
    
    db.transaction(function (tx, rs) {
        tx.executeSql(query, [],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function (tx, err) {
                console.log(JSON.stringify(err));
            });
    });
}

function fnDelSetAccessData() {
    console.log("fnDelSetAccessData");
    db.transaction(function (tx, rs) {
        tx.executeSql("DELETE FROM T101D_USER_ACCESS ", [],
            function (tx, rs) {
                console.log("updated")
            },
            function (e, err) {
                showMsgView("009");
            });
    });
}


function fnSetAccessData(accid) {
    var userid = localStorage.getItem("userID");
    var compid = localStorage.getItem("cmpid");
    console.log(accid);
    db.transaction(function (tx, rs) {
        tx.executeSql("INSERT OR REPLACE INTO T101D_USER_ACCESS (C101D_USER_ACCESS_ID,C101_USER_ID,C704_ACCOUNT_ID,C1900_COMPANY_ID,C101D_VOID_FL) VALUES (null,?,?,?,null)", [userid, accid, compid], function (tx, rs) {
                console.log("fnSetAccessData Inserted");
            },
            function (e, err) {
                showMsgView("009");
            });
    });
}

function fnGetDOID(DOID, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT DOID FROM TBL_DO WHERE DOID LIKE ?", [DOID + "%"],
		
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function (e, err) {
                showAppError(err, "GmDAODO:fnGetDOID()");
            });
    });
}

//get rule ids by rule group
function fnGetRuleByGRP(GRP, callback){
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT C906_RULE_ID AS ID,C906_RULE_VALUE AS Name FROM T906_RULES WHERE C906_RULE_GRP_ID = ?  AND C906_VOID_FL = '' ", [GRP],
                function (tx, rs) {
                    var i = 0,
                        results = [];
                    for (i = 0; i < rs.rows.length; i++) {
                        results.push(rs.rows.item(i));
                    }
                    callback(results);
                },
                function () {
                    showMsgView("009");
                });
        });  
}

//function to fetch dealer id by account id
function fnGetDealerIDByAccId(id, callback) {
    db.transaction(function(tx, rs) {
        tx.executeSql("SELECT C101_DEALER_ID AS Dealerid FROM t704_ACCOUNT WHERE C704_VOID_FL='' AND C704_ACCOUNT_ID=?", [id],
            function(tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function(tx, err) {
                console.log(JSON.stringify(err));
            });
    });
}

//function to fetch default usage lot display rule value
  function fnGetLotUseFL (callback){
            fnGetRuleValueByCompany ("DO_APP_LOT_FL","DO_ORDER",localStorage.getItem("cmpid"),function(data){
                if(data.length>0){
                     callback (data[0].RULEVALUE);
                }else{
                     callback ("N");
                }
            });
        }

//function iPad Loaner in Type field disable in Parts Used tab PMT-43383
  function fnDisableDOReqType (callback){
            fnGetRuleValueByCompany ("DO_APP_REQ_TYPE","DO_ORDER",localStorage.getItem("cmpid"),function(data){
                if(data.length>0){
                     callback (data[0].RULEVALUE);
                }else{
                     callback ("N");
                }
            });
        }
//function to Get Tag details
function fnGetDOTagInfo (url,txtSearch, callback) {

	var orderby;
    var keyText = txtSearch;
    
    (parseInt(txtSearch))?orderby='T5010.C5010_Tag_Id':orderby='T207.C207_SET_NM';

	txtSearch = txtSearch.split(' ');
	var searchTerm = [];
	for(var i=0; i<txtSearch.length;i++) {
		(parseInt(keyText))?searchTerm.push("SystemSetName LIKE '%"+txtSearch[i]+"%'"):searchTerm.push("SystemSetName LIKE '%"+txtSearch[i]+"%'");
	}
	var query = searchTerm.join(" AND ");
	query = "SELECT  T5010.C207_Set_Id || '*' || T207.C207_SET_NM ID, T5010.C5010_Tag_Id Name, T5010.C5010_Tag_Id || ' ' || T207.C207_SET_NM AS SystemSetName,'" + url + "/detail/set/' AS URL FROM T5010_TAG_DTL T5010,  t207_set_master T207  WHERE T5010.C207_SET_ID = T207.C207_SET_ID AND (T5010.C5010_Void_Fl IS NULL OR T5010.C5010_Void_Fl='null') AND " + query + " ORDER BY " + orderby;
	console.log(query);
	db.transaction(function (tx, rs) {
		tx.executeSql(query, [],
			function(tx,rs) {
				var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
            
			},
		function(e,err){alert('error');console.log(JSON.stringify(err))});
	});
}

//function to Get Tag details while scan
function fnGetDOTagScan (id, callback) {
  
	var query;
	db.transaction(function (tx, rs) {
		tx.executeSql("SELECT  T5010.C5010_Tag_Id tagid, T5010.C207_Set_Id setid, T207.C207_SET_NM setnm FROM T5010_TAG_DTL T5010,  t207_set_master T207  WHERE T5010.C207_SET_ID = T207.C207_SET_ID AND (T5010.C5010_Void_Fl IS NULL OR T5010.C5010_Void_Fl='null') AND T5010.C5010_Tag_Id = ?" , [id],
			function(tx,rs) {
				var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log('results'+results);
			callback(results);
            
			},
		function(e,err){alert('error');console.log(JSON.stringify(err))});
	});
}


//function to fetch setlist 
function fnGetSetListDataBySetId(val, callback) {
    console.log(val);
    var query = " SELECT t207.c207_set_id AS setid,t207.c207_set_nm AS setnm,t208.c205_part_number_id AS partid, t205.c205_part_num_desc AS partnm FROM T207_Set_Master T207,T208_Set_Details T208,T205_Part_Number T205 WHERE T207.C207_Void_Fl = '' AND T208.C208_Void_Fl = '' AND T207.C207_Set_Id = T208.C207_Set_Id AND T208.C205_Part_Number_Id = T205.C205_Part_Number_Id AND T205.C205_Active_Fl = 'Y' AND T207.C207_Set_Id IN (" + val + ") ";
    console.log(query);
	db.transaction(function (tx, rs) {
		tx.executeSql(query, [],
				function(tx,rs) {
			var i=0, results = [];
			for(i=0;i<rs.rows.length;i++) {
				results.push(rs.rows.item(i));
			}
			console.log(results);
			callback(results);
		},
		function(){
            showMsgView("009");});
	});
}

//function to fetch the part details seleced from Set list popup screen from record Tag screen 
function fnGetDOPartsforTagList(url, pnum, callback) {
    var query = "";
    query = " SELECT T205.C205_PART_NUMBER_ID AS ID, T205.C205_PART_NUMBER_ID || ' ' || T205.C205_PART_NUM_DESC AS Name,T205.C205_PRODUCT_MATERIAL AS ProductMaterial, T205.C205_PRODUCT_CLASS AS ProductClass, T205.C205_PRODUCT_FAMILY AS ProductFamily, T205.C205_PART_NUMBER_ID || ' ' || T205.C205_PART_NUM_DESC AS SystemPartName, '" + url + "/detail/part/' AS URL FROM T205_PART_NUMBER T205, T205D_PART_ATTRIBUTE T205D,T901C_RFS_COMPANY_MAPPING T901C WHERE T205.C205_ACTIVE_FL = 'Y' AND T205.C205_PART_NUMBER_ID = T205D.C205_PART_NUMBER_ID AND T205D.C901_ATTRIBUTE_TYPE = '80180' And T205D.C205D_ATTRIBUTE_VALUE = T901C.C901_RFS_ID  AND T901C.C1900_COMPANY_ID = '" + localStorage.getItem("cmpid") + "' AND T205D.C205D_VOID_FL = '' AND T901C.C901C_VOID_FL is null AND T205.C205_PART_NUMBER_ID = '"+pnum+"'  ORDER BY T205.C205_PART_NUMBER_ID "; 
    
    db.transaction(function(tx, rs) {
        tx.executeSql(query, [],
            function(tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                callback(results);
            },
            function(e, err) {
                showAppError(err, "GmDAODO:fnGetDOPartsforTagList()");
            });
    });
}

// fnUpdateAddressFL - Function is to empty the primary flag for the given partyID if the primaryUpd parameter is Y
// Parameters partyID, primryUpd - need to empty ('Y') or not(''), callback
function fnUpdateAddressFL(partyID, primryUpd, callback){   
if(primryUpd == 'Y'){
    db.transaction(function (tx, rs) {
        tx.executeSql("UPDATE T106_ADDRESS SET C106_PRIMARY_FL = '' WHERE  C101_PARTY_ID = ? AND C106_VOID_FL = '' AND C106_PRIMARY_FL='Y'", [partyID],
            function (tx, rs) {
                console.log("Updated Primary Flag as Empty");
                callback();
            },
            function (tx, err) {
               console.log(JSON.stringify(err));
               showAppError(err, "GmDAODO:fnUpdateAddressFL()");

            });
    });
}
    else{
        callback();
    }
}

/* Get the distributor details
   Author: Karthik Somanathan    
*/
function fnGetDistDtlsByPartyid(partyid, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT t701.c701_distributor_id DISTID , t701.c701_distributor_name DISTNM ,t701.c701_region regionid ,t703.C703_sales_rep_id repid FROM t703_sales_rep t703 , t701_distributor t701 WHERE t703.c701_distributor_id = t701.c701_distributor_id AND t703.c101_party_id = ? AND t703.c703_void_fl  = '' AND t701.c701_void_fl = '' AND t703.c703_active_fl  = 'Y' AND t701.c701_active_fl  = 'Y'",[partyid],
            function(tx,rs) {
                var i=0, results = [];
            for(i=0;i<rs.rows.length;i++) {
                results.push(rs.rows.item(i));
            }
            callback(results);
            console.log("fnGetDistDtlsByPartyid = "+results);
            },
        function(e,err){console.log(JSON.stringify(err))});
    });
}

/* Get the Sales Rep details and set it to the google firebase document ID variable to fetch or save the tag details
   for the Favorites
   Author: Karthik Somanathan    
*/
function fnGetRepDtlsByPartyid(partyid, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT t703.c701_distributor_id distid ,t703.C703_sales_rep_id repid FROM t703_sales_rep t703  WHERE  t703.c101_party_id = ? AND t703.c703_void_fl  = '' AND t703.c703_active_fl  = 'Y'",[partyid],
            function(tx,rs) {
                var i=0, results = [];
            for(i=0;i<rs.rows.length;i++) {
                results.push(rs.rows.item(i));
            }
            callback(results);
            console.log("fnGetDistDtlsByPartyid = "+results);
            },
        function(e,err){console.log(JSON.stringify(err))});
    });
}


function fnGetUserSalesRep(PartyID, callback) {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT C703_SALES_REP_ID REPID , C703_SALES_REP_NAME REPNM FROM T703_SALES_REP WHERE C101_PARTY_ID = ?", [PartyID],
            function(tx,rs) {
                var i=0, results = [];
            for(i=0;i<rs.rows.length;i++) {
                results.push(rs.rows.item(i));
            }
            callback(results);
            console.log("fnGetUserSalesRep = "+results);
            },
            function(e,err){console.log(JSON.stringify(err))});
    });
}
/* fnSaveDOTagImageDetails: The function used to save Images and its details for the corresponding DO ID
 * Author : Paveethra Pandiyan
 */
function fnSaveDOTagImageDetails(DOID, IMAGENAME, IMAGEPATH, PROCESSEDTAGS,PROCESSEDDATE) {
     console.log("fnSaveDOTagImageDetails IMAGEPATH ==="+IMAGEPATH);
    db.transaction(function(tx, rs) {
        tx.executeSql("INSERT INTO TBL_DO_TAG_IMG_DTL(DO_ID, IMG_NM, IMG_PATH, PROCESSED_TAGS, PROCESSED_DT) VALUES(?,?,?,?,?)", [ DOID, IMAGENAME, IMAGEPATH, PROCESSEDTAGS,PROCESSEDDATE],
            function(tx, rs) {
                console.log("fnSaveDOTagImageDetails : inserted")
            },
            function(e, err) {
                showMsgView("009");
            });
    });
}
/* fnUpdDOTagImageDetails: The function used to update Images and its details for the corresponding DO ID
 * Author : Paveethra Pandiyan
 */
function fnUpdDOTagImageDetails(TAGIMGDTLID, DOID, IMAGENAME, IMAGEPATH, PROCESSEDTAGS,PROCESSEDDATE) {

     db.transaction(function (tx, rs) {
        tx.executeSql("UPDATE TBL_DO_TAG_IMG_DTL SET PROCESSED_TAGS = CASE WHEN PROCESSED_TAGS IS NOT NULL AND PROCESSED_TAGS != '' THEN PROCESSED_TAGS||','||? ELSE ?  END ,PROCESSED_DT= ?  WHERE TAG_IMG_DTL_ID = ? AND DO_ID = ?", [PROCESSEDTAGS,PROCESSEDTAGS,PROCESSEDDATE,TAGIMGDTLID,DOID],
            function (tx, rs) {
                console.log("fnUpdDOTagImageDetails : updated")
            },
            function (e, err) {
               console.log(JSON.stringify(err));
            });
    });

}
/* fnUpdDOTagProcessedDate: The function used to update date once the image is processed through API
 * Author : Paveethra Pandiyan
 */
function fnUpdDOTagProcessedDate(TAGIMGDTLID, DOID,PROCESSEDDATE) {

     db.transaction(function (tx, rs) {
        tx.executeSql("UPDATE TBL_DO_TAG_IMG_DTL SET PROCESSED_DT= ?  WHERE TAG_IMG_DTL_ID = ? AND DO_ID = ?", [PROCESSEDDATE,TAGIMGDTLID,DOID],
            function (tx, rs) {
                console.log("fnUpdDOTagProcessedDate : updated")
            },
            function (e, err) {
               console.log(JSON.stringify(err));
            });
    });

}
/* fnGetDOTagImageDtls: The function used to get the DO tag image details
 * Author : Paveethra Pandiyan
 */

function fnGetDOTagImageDtls(orderid, callback) {
    var query;
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT  * FROM TBL_DO_TAG_IMG_DTL WHERE DO_ID = ?", [orderid],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                console.log('results' + results);
                callback(results);

            },
            function (e, err) {
                console.log(JSON.stringify(err))
            });
    });
}

/* fnGetTagImageDtlsCount: The function used to get the maximum DO tag image details id 
 * Author : Paveethra Pandiyan
 */
function fnGetTagImageDtlsCount(callback) {
    var query;
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT MAX(TAG_IMG_DTL_ID) COUNT FROM TBL_DO_TAG_IMG_DTL", [],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                   results.push(rs.rows.item(i));
                }

            var countValue = results[0].COUNT;
            console.log(countValue);
            console.log(countValue == null);

            if(countValue == null || countValue == ''){
                 callback (1);
                }else{
                    callback (results[0].COUNT+1);
                }
                

            },
            function (e, err) {
                console.log(JSON.stringify(err))
            });
    });
}
/* fnGetDOUnprocessedTagImgDtls: The function used to get the unprocessed DO tag image details 
 * Author : Paveethra Pandiyan
 */
function fnGetDOUnprocessedTagImgDtls(orderid, callback) {
    var query;
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT  * FROM TBL_DO_TAG_IMG_DTL WHERE PROCESSED_TAGS='' AND DO_ID = ?", [orderid],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                console.log('results-----' + results);
                callback(results);

            },
            function (e, err) {
                console.log(JSON.stringify(err))
            });
    });
}
/* fnGetDOUnprocessedTagImgDtls: The function used to get the processed DO tag image details 
 * Author : Paveethra Pandiyan
 */
function fnGetDOProcessedTagImgDtls(orderid, callback) {
    var query;
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT  * FROM TBL_DO_TAG_IMG_DTL WHERE PROCESSED_TAGS != '' AND DO_ID = ?", [orderid],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                console.log('results-----' + results);
                callback(results);

            },
            function (e, err) {
                console.log(JSON.stringify(err))
            });
    });
}
/* fnDelDOTagImageDtls: The function used to delete the DO tag image details 
 * Author : Paveethra Pandiyan
 */
function fnDelDOTagImageDtls(TAGIMGDTLID,DOID) {
    db.transaction(function (tx, rs) {
        tx.executeSql("DELETE FROM TBL_DO_TAG_IMG_DTL WHERE  TAG_IMG_DTL_ID = ? AND DO_ID = ? ", [TAGIMGDTLID,DOID],
            function (tx, rs) {
            console.log("fnDelDOTagImageDtls:Deleted");
        },
            function (tx, err) {
               console.log(JSON.stringify(err));
               showAppError(err, "GmDAODO:fnDelDOTagImageDtls()");

            });
    });
}
/* fnSaveTagsForProcessImage: The function used to save the DO tags from processed images 
 * Author : Paveethra Pandiyan
 */
function fnSaveTagsForProcessImage(DOID, TAGID, SETID, SETNM, LATITUDE,LONGITUDE,STATUS) {
     console.log("fnSaveTagsForProcessImage : inserted"+DOID);
    db.transaction(function(tx, rs) {
        tx.executeSql("INSERT INTO TBL_DO_TAG_DETAILS(DO_ID, TAG_ID, SET_ID, SET_NM, LATITUDE, LONGITUDE, STATUS) VALUES(?,?,?,?,?,?,?)", [ DOID, TAGID, SETID, SETNM, LATITUDE, LONGITUDE, STATUS],
            function(tx, rs) {
                console.log("fnSaveTagsForProcessImage : inserted");
            },
            function(e, err) {
            console.log("error : fnSaveTagsForProcessImage");
                showMsgView("009");
            });
    });
}
/* fnDelTagsForProcessImage: The function used to delete the DO tags  
 * Author : Paveethra Pandiyan
 */
function fnDelTagsForProcessImage(DOID) {
    db.transaction(function (tx, rs) {
        tx.executeSql("DELETE FROM TBL_DO_TAG_DETAILS WHERE DO_ID = ? ", [DOID],
            function (tx, rs) {
            console.log("fnDelTagsForProcessImage:Deleted");
        },
            function (tx, err) {
               console.log(JSON.stringify(err));
               showAppError(err, "GmDAODO:fnDelDOTagImageDtls()");

            });
    });
}
/* fnUpdateDOStatusinTagDetails: The function used to delete the DO tags  
 * Author : Paveethra Pandiyan
 */
function fnUpdateDOStatusinTagDetails(DO, Status) {
    db.transaction(function (tx, rs) {
        tx.executeSql("UPDATE TBL_DO_TAG_DETAILS SET STATUS = ? WHERE  DO_ID = ?", [Status, DO],
            function (tx, rs) {

            },
            function (tx, err) {
               console.log(JSON.stringify(err));
               showAppError(err, "GmDAODO:fnUpdateDOStatus()");

            });
    });
}
/* fnProcessTagImageForPendingSyncDO: The function used to fetch the order id which is having unprocessed images 
 * and send to azure to process those images
 * Author : Paveethra Pandiyan
 */
function fnProcessTagImageForPendingSyncDO() {
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT  DO_ID DOID FROM TBL_DO_TAG_IMG_DTL WHERE PROCESSED_TAGS='' AND PROCESSED_DT = '' GROUP BY DO_ID", [],
            function (tx, rs) {
                if (rs.rows.length > 0) {
                    for (var i = 0; i < rs.rows.length; i++) {
                        var orderid = rs.rows.item(i).DOID;
                        fnReadTextFrmImageThruAPI(orderid, 'Y');

                    }
                }
            },
            function (e, err) {
                showAppError(JSON.stringify(err), "GmDAODO:fnProcessTagImageForPendingSyncDO()");
            });
    });
}
/* fnSaveTagsForPendingSyncDO: The function used to save the tags from processed images to corresponding order id
 * Author : Paveethra Pandiyan
 */
function fnSaveTagsForPendingSyncDO() {
    console.log("Inside fnSaveTagsForPendingSyncDO");
    var query;
    var token = localStorage.getItem("token");
    var userID = localStorage.getItem("userID");
    var cmpid = localStorage.getItem("cmpid");
    var plantid = localStorage.getItem("plantid");
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT  DO_ID, TAG_ID, SET_ID, SET_NM, LATITUDE, LONGITUDE FROM TBL_DO_TAG_DETAILS WHERE STATUS = 'Pending Sync' ORDER BY DO_ID", [],
            function (tx, rs) {
                var i = 0,
                    results = [];
                if (rs.rows.length > 0) {
                    for (i = 0; i < rs.rows.length; i++) {
                        var doid = rs.rows.item(i).DO_ID;
                        var input = {
                            "token": token,
                            "userid": userID,
                            "orderid": rs.rows.item(i).DO_ID,
                            "tagid": rs.rows.item(i).TAG_ID,
                            "setid": rs.rows.item(i).SET_ID,
                            "tagusagefl": "",
                            "latitude": rs.rows.item(i).LATITUDE,
                            "longitude": rs.rows.item(i).LONGITUDE,
                            "cmpid": cmpid,
                            "plantid": plantid,
                        };
                        fnGetWebServerData("DeliveredOrder/saveDOTags", undefined, input, function (data, model, response, errorReport) {
                            if (model == undefined) {
                                fnUpdateDOStatusinTagDetails(doid, 'Pending CS Confirmation');
                            } else {
                                showAppError(model.responseText, "GmDAODO:fnSaveTagsForPendingSyncDO() 1");
                            }
                        }, true);
                    }
                }
            },
            function (e, err) {
                showAppError(JSON.stringify(err), "GmDAODO:fnSaveTagsForPendingSyncDO() 2");
            });
    });
}

/* fnReadTextFrmImageThruAPI: The function used to extract the text from the Image using Microsoft Azure API
 * Author : Karthik Somanathan
 */
function fnReadTextFrmImageThruAPI(orderid, savefl) {
    var ContentType = "application/octet-stream";
    var subscriptionKey = localStorage.getItem("readImageSubKey");
    var uriBase = localStorage.getItem("readImageEndPoint");
    var params = {
        "language": "en"
    };

    var URLBase = uriBase + "?" + $.param(params);
  
    var date = new Date();
    var dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
    var processedDt = formattedDate(mm+"/"+dd+"/"+yyyy, localStorage.getItem("cmpdfmt"));
   

    var i, tagImageDetailId, tagImagePath;
     if (navigator.onLine) {
         fnGetDOUnprocessedTagImgDtls(orderid, function (data) {
             console.log("fnReadTextFrmImageThruAPI ==" + JSON.stringify(data));
             for (i = 0; i < data.length; i++) {
                 tagImageDetailId = data[i].TAG_IMG_DTL_ID;
                 tagImagePath = data[i].IMG_PATH;
                 //tagImagePath = tagImagePath.split('Globus/')[1];
                 tagImagePath = tagImagePath;
                 console.log("tagImageDetailId ==" + tagImageDetailId);
                 fnReadImageFromFileSystemAsArrayBuffer(orderid, savefl, ContentType, subscriptionKey, URLBase, processedDt, tagImageDetailId, tagImagePath);
             }
         });
     }
}
/* fnFetchDOTagDetails: The function used to fetch the DO tags details
 * Author : Paveethra Pandiyan
 */
function fnFetchDOTagDetails(doid,callback){
        var query;
        var token = localStorage.getItem("token");
        var userID = localStorage.getItem("userID");
        var cmpid = localStorage.getItem("cmpid");
        var plantid = localStorage.getItem("plantid");
   
                var input = {
                    "token"     : token,
                    "userid"    : userID,
                    "orderid"   : doid,
                    "cmpid"     : cmpid,
                    "plantid"   : plantid,
                };

                fnGetWebServiceData("DOTag/fetchDOTagDtls", undefined, input, function (status, data) {
                    console.log(status);
                    console.log(data);
                  if (status) {
                       callback(data);
                  }
                   
                });


}
/* fnSaveTagsDetailsToDO: The function used to save the tags details to the corresponding DO
 * Author : Paveethra Pandiyan
 */
function fnSaveTagsDetailsToDO(data){
        var query;
        var token = localStorage.getItem("token");
        var userID = localStorage.getItem("userID");
        var cmpid = localStorage.getItem("cmpid");
        var plantid = localStorage.getItem("plantid");

                for (var i = 0; i < data.length; i++) {
                 
                    
                var input = {
                    "token"     : token,
                    "userid"    : userID,
                    "orderid"   : data[i].orderId,
                    "tagid"     : data[i].tagId,
                    "setid"     : data[i].setId,
                    "latitude"  : data[i].latitude,
                    "longitude" : data[i].longitude,
                    "cmpid"     : cmpid,
                    "plantid"   : plantid,
                };
                fnGetWebServiceData("DeliveredOrder/saveDOTags", undefined, input, function (status, data) {
                   
                    if (status) {
                        console.log("saved successfully");              
                    } 
                });
                }
           
}
/* fnDelDOFromTagImgDtls: The function used to delete the tag image details 
 * Author : Paveethra Pandiyan
 */
function fnDelDOFromTagImgDtls(DOID) {
    db.transaction(function (tx, rs) {
        tx.executeSql("DELETE FROM TBL_DO_TAG_IMG_DTL WHERE  DO_ID = ?", [DOID],
            function (tx, rs) {
        	console.log("fnDelDOFromTagImgDtls:Deleted");
        },
            function (tx, err) {
               console.log(JSON.stringify(err));
               showAppError(err, "GmDAODO:fnDelDOFromTagImgDtls()");

            });
    });
}
/* fnUpdDOTagImgDtls: The function used to update the do id for corresponding tag image details 
 * Author : Paveethra Pandiyan
 */
function fnUpdDOTagImgDtls(OLDDO, NEWDO) {
    db.transaction(function (tx, rs) {
        tx.executeSql("UPDATE TBL_DO_TAG_IMG_DTL SET DO_ID = ? WHERE  DO_ID = ?", [NEWDO, OLDDO],
            function (tx, rs) {
        	console.log("fnUpdDOTagImgDtls:Updated");
            },
            function (tx, err) {
               console.log(JSON.stringify(err));
               showAppError(err, "GmDAODO:fnUpdDOTagImgDtls()");

            });
    });
}
/* fnUpdDOTagDetails: The function used to update the do id for corresponding tag details 
 * Author : Paveethra Pandiyan
 */
function fnUpdDOTagDetails(OLDDO, NEWDO) {
    db.transaction(function (tx, rs) {
        tx.executeSql("UPDATE TBL_DO_TAG_DETAILS SET DO_ID = ? WHERE  DO_ID = ?", [NEWDO, OLDDO],
            function (tx, rs) {
        	console.log("fnUpdDOTagDetails:Updated");
            },
            function (tx, err) {
               console.log(JSON.stringify(err));
               showAppError(err, "GmDAODO:fnUpdDOTagDetails()");

            });
    });
}
/* fnDelTags: The function used to delete the do id for corresponding tag details 
 * Author : Paveethra Pandiyan
 */
function fnDelTags() {
    db.transaction(function (tx, rs) {
        tx.executeSql("DELETE FROM TBL_DO_TAG_DETAILS ", [],
            function (tx, rs) {
            console.log("fnDelTags:Deleted");
        },
            function (tx, err) {
               console.log(JSON.stringify(err));
               showAppError(err, "GmDAODO:fnDelTags()");

            });
    });
}
/* fnDOReadableTagsFromImageCnt: The function used to fetch the count of readable tags from image 
 * Author : Paveethra Pandiyan
 */
function fnDOReadableTagsFromImageCnt(orderid,callback) {
    var query;
    db.transaction(function (tx, rs) {
        tx.executeSql("SELECT COUNT(*) CNT FROM TBL_DO_TAG_IMG_DTL WHERE PROCESSED_TAGS !='' AND DO_ID = ? AND PROCESSED_DT != ''", [orderid],
            function (tx, rs) {
                var i = 0,
                    results = [];
                for (i = 0; i < rs.rows.length; i++) {
                    results.push(rs.rows.item(i));
                }
                console.log('results-----' + results[0].CNT);
                callback(results[0].CNT);

            },
            function (e, err) {
                console.log(JSON.stringify(err))
            });
    });
}