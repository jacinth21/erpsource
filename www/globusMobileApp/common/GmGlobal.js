/**********************************************************************************
 * File:        GmGlobal.js
 * Description: Contains global variables and functions commonly used across the
 *              Application
 * Version:     1.0
 * Author:      ejayabalan
 **********************************************************************************/
var version = "3.0.0";
var deviceModel = "" ,
    deviceVersion = "" ,
    devicePlatform = "";
var androidDevicePlatform = "android";
var iosDevicePlatform = "ios";
var deviceUUID = "";
var deviceToken = "";
var gmvApp;
var pchistory = "#catalog";
var mchistory = "#collateral";
var strEnvironment = "";
var locale;
var pcBreadcrumb = new Array(),
    pcRecentBreadCrumbs = new Array(),
    mcBreadcrumb = new Array(),
    mcRecentBreadCrumbs = new Array(),
    recentHash = "",
    moreFilterHash = "";
var UpdateTimer, TimerMilliSeconds = 900000; //2 Minutes ---- Converting to 15 minutes as Auto Update is implemeted
var intWarnSize = 5;
var toList = new Array();
var intDBStatus = 0,
    intLogin = 0,
    intOfflineMode = 0;
// var connectionType = (navigator.connection.type).toLowerCase();
var arrPendingDOList = new Array(),
    arrUpdatedDO = new Array();
//DO_PDF_INFO has been altered for PMT-12763
var arrAlterTables = ['T903_UPLOAD_FILE_LIST'];
var arrRequest = new Array();
var arrOrder = new Array();
var arrSelectedAccounts = new Array();
var arrExtnData = new Array();
var boolUpdate = true,
    boolSyncing = false,
    boolAutoUpdateStatus = 0,
    autoUpdateCollision;
var timerSplashLimit = 500; //	Minimum timer to reset the slider
var slidNumber;
var mainview;
var Handlebars = require('handlebars');
var catalogInitial, catalogDetail, collateralInitial, collateralDetail, shareView, shareStatusScreen, dashmainview, domainview, invmainview, homeview, setlistview, pricemainview,collateralEmailTrack;
var salescallDays = 180;
var accountIdVal = "";
var typeIdVal = "903107";
//Normal Difference of GMT with Local American Time
var Atlantic = 4,
    Eastern = 5,
    Central = 6,
    Mountain = 7,
    Pacific = 8,
    Alaska = 9,
    Hawaii = 10;
$.xhrPool = [];
var pricetypedisc;
var companyId = localStorage.getItem("cmpid");
var errArrLog = [];
//Initialize Cloud Firestore
var firedb;

// Auto Check Update 
var timeDelay = 5000;
var timerId = setTimeout(function requestTimeOut() {
    console.log("setTimeout >>>>>>>>>>>"+ timeDelay);
    var token = localStorage.getItem("token");
    if (!boolSyncing && parseNull(token) != "") {
        var now = new Date();
        timeDelay = Math.abs(window.localStorage.getItem("autoUpdateTimer"));
        if (isNaN(timeDelay)) {
            timeDelay = 1800000;
        } else if (timeDelay == 0) {
            timeDelay = 1800000;
        }
        var lastUpdateChkTime = window.localStorage.getItem("lastUpdateChkTime");
        var lastDate = new Date(lastUpdateChkTime);
        if (parseNull(lastUpdateChkTime) == "")
            chkUpdate();
        else if (Math.abs(lastDate - now) > timeDelay) 
            chkUpdate();
    }
    timerId = setTimeout(requestTimeOut, timeDelay);
}, timeDelay);


Date.prototype.dst = function (city) {
    if (city != undefined) {
        var year = this.getFullYear();
        var month = ("0" + (this.getMonth() + 1)).slice(-2);
        var date = ("0" + this.getDate()).slice(-2);
        var fuldate = month + "/" + date + "/" + year;
        timezoneJS.timezone.zoneFileBasePath = 'globusMobileApp/lib/timezone-js-master/tz';
        timezoneJS.timezone.init({
            callback: null
        });
        var jan = new timezoneJS.Date(fuldate, city);
        return jan.getTimezoneOffset(); 
    }
}

function chkDaylightSaving() {
    var today = new Date();

    //Difference of GMT with Local American Time in Daylight Saving time
    if (today.dst()) {
        Atlantic = 4;
        Eastern = 4;
        Central = 5;
        Mountain = 6;
        Pacific = 7;
        Alaska = 8;
        Hawaii = 10;
    }
}
//chkDaylightSaving();

var TimeZones = {
    "Atlantic": "America/Blanc-Sablon",
    "Eastern": "America/New_York",
    "Central": "America/Chicago",
    "Mountain": "America/Denver",
    "Pacific": "America/Los_Angeles",
    "Alaska": "America/Anchorage",
    "Hawaii": "Pacific/Honolulu",
    "Arizona": "America/Phoenix"
};

Number.prototype.cformat = function (n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};



var URL_DO_Template = "globusMobileApp/sales/DO/template/";
var URL_Inv_Template = "globusMobileApp/sales/inv/template/";
var URL_Dashboard_Template = "globusMobileApp/sales/dashboard/template/";
var URL_Case_Template = "globusMobileApp/sales/case/template/";
var URL_Catalog_Template = "globusMobileApp/sales/catalog/template/";
var URL_Collateral_Template = "globusMobileApp/sales/collateral/template/";
var URL_Common_Template = "globusMobileApp/common/template/";
var URL_CSS = "globusMobileApp/css/";
var URL_Home_Template = "globusMobileApp/home/template/";
var URL_Login_Template = "globusMobileApp/user/template/";
var URL_Main_Template = "globusMobileApp/common/template/";
var URL_Setup_Template = "globusMobileApp/setup/template/";
var URL_Msg_Template = "globusMobileApp/message/template/";
var URL_LoginFailed_Template = "globusMobileApp/user/template/";
var URL_Logout_Template = "globusMobileApp/user/template/";
var URL_Settings_Template = "globusMobileApp/settings/template/";
var URL_Kit_Template = "globusMobileApp/sales/kit/template/";
var URL_CaseSchdlr_Template = "globusMobileApp/sales/casescheduler/template/";
//scorecard template
var URL_Scorecard_Template = "globusMobileApp/sales/scorecard/template/";


var URL_Kit_Template = "globusMobileApp/sales/kit/template/";

// CRM templates
var URL_CRMDash_Template = "globusMobileApp/crm/dashboard/template/";
var URL_Lead_Template = "globusMobileApp/crm/activity/template/lead/";
var URL_PhysicianVisit_Template = "globusMobileApp/crm/activity/template/physicianvisit/";
var URL_Surgeon_Template = "globusMobileApp/crm/surgeon/template/";
var URL_SalesCall_Template = "globusMobileApp/crm/activity/template/salescall/";
var URL_FieldSales_Template = "globusMobileApp/crm/fieldsales/template/";
var URL_Training_Template = "globusMobileApp/crm/activity/template/training/";
var URL_MERC_Template = "globusMobileApp/crm/activity/template/merc/";
var URL_CRF_Template = "globusMobileApp/crm/crf/template/";
var URL_Preceptor_Template = "globusMobileApp/crm/preceptorship/template/";
var URL_Form_Template = "globusMobileApp/quality/eventevalform/template/";
var URL_CRMMerc_Template = "globusMobileApp/crm/merc/template/";
var URL_Account_Template = "globusMobileApp/crm/Account/template/";

//Pricing templates
var URL_Price_Template = "globusMobileApp/pricing/template/";

var tabData = {
    Dashboard: {
        taburl: "dashboard",
        tabcolor: "gmFontWhite",
        icon: ["fa fa-tachometer"],
        module: ""
    },
//    "Field Sales": {
//        taburl: "fieldsales",
//        tabcolor: "tabfieldsales gmFontWhite",
//        icon: ["fa-users fa-lg"],
//        title: "Sales Region",
//        url: "fieldsales/regterdis",
//        module: "fieldsales"
//    },
//    Lead: {
//        taburl: "lead",
//        tabcolor: "tablead gmFontWhite",
//        icon: ["fa-users fa-lg", "fa-search fa-lg"],
//        css: "font-size: 0.75em; margin-left: 5px; margin-right : 8px;",
//        title: "Lead",
//        url: "crmactivity/info",
//        voname: "gmCRMActivityVO",
//        module: "lead"
//    },
    "Sales Call": {
        taburl: "salescall",
        tabcolor: "tabsalescall gmFontWhite",
        icon: ["fa-comments-o fa-lg"],
        title: "Sales Call",
        url: "crmactivity/info",
        voname: "gmCRMActivityVO",
        module: "salescall"
    },
//    Training: {
//        taburl: "training",
//        tabcolor: "tabtraining gmFontWhite",
//        icon: ["fa fa-empire fa-lg"],
//        title: "Training",
//        url: "crmactivity/info",
//        voname: "crmactivity/info",
//        module: "training"
//    },
    Surgeon: {
        taburl: "surgeon",
        tabcolor: "tabsurgeon gmFontWhite",
        icon: ["fa-user-md fa-lg"],
        title: "Surgeon",
        url: "surgeon/info",
        voname: "gmCRMSurgeonlistVO",
        module: "surgeon"
    },
    "Physician Visit": {
        taburl: "physicianvisit",
        tabcolor: "tabphysicianvisit gmFontWhite",
        icon: ["fa-user fa-lg"],
        title: "Physician Visit",
        url: "crmactivity/info",
        voname: "gmCRMActivityVO",
        module: "physicianvisit"
    },
    MERC: {
        taburl: "merc",
        tabcolor: "tabmerc gmFontWhite",
        icon: ["fa-graduation-cap fa-lg"],
        title: "MERC",
        url: "merc/dash",
        voname: undefined,
        module: "merc"
    },
//    CRF: {
//        taburl: "crf",
//        tabcolor: "tabcrf gmFontWhite",
//        icon: ["fa-suitcase fa-lg"],
//        title: "CRF",
//        url: "crf/list",
//        voname: "gmCRMCRFVO",
//        module: "crf"
//    },
    Preceptorship: {
        taburl: "preceptorship",
        tabcolor: "tabpreceptorship gmFontWhite",
        icon: ["fa-suitcase fa-lg"],
        title: "Preceptorship",
        url: "preceptorship/list",
        voname: "gmCRMPSVO",
        module: "preceptorship"
    },
    Tradeshow: {
        taburl: "tradeshow",
        tabcolor: "tabtradeshow gmFontWhite",
        icon: ["fa-suitcase fa-lg"],
        title: "Tradeshow",
        url: "tradeshow/dash",
        voname: "gmCRMTradeshowVO",
        module: "tradeshow"
    },
    "Quick Reference": {
        taburl: "quickreference",
        tabcolor: "tabquickreference gmFontWhite",
        icon: ["fa-suitcase fa-lg"],
        title: "Quick Reference",
        url: "quickref/list",
        voname: "gmCRMQuickRefVO",
        module: "quickreference"
    }
};

var dashConfig = [
    {
        "SecGrp": "CRMDB-SREP",
        "Primary": "surgeon",
        "Secondary": ["physicianvisit", "merc","quickreference", "salescall", "preceptorship","tradeshow"]
    },
    {
        "SecGrp": "CRMDB-SMGT",
        "Primary": "physicianvisit",
        "Secondary": ["salescall", "merc","quickreference", "preceptorship","tradeshow"]
    },
    {
        "SecGrp": "CRMDB-PHYS",
        "Primary": "physicianvisit",
        "Secondary": ["salescall", "merc","quickreference", "preceptorship","tradeshow"]
    },
    {
        "SecGrp": "CRMDB-MERC",
        "Primary": "merc",
        "Secondary": ["physicianvisit", "preceptorship","quickreference","tradeshow"]
    }
    /*,
    {
        "SecGrp": "CRMDB-CRF",
        "Primary": "crf",
        "Secondary": ["merc", "physicianvisit"]
    },
    {
        "SecGrp": "CRMDB-EXEC",
        "Primary": "crf",
        "Secondary": ["merc", "physicianvisit"]
    },
    {
        "SecGrp": "CRMDB-INTUSE",
        "Primary": "crf",
        "Secondary": ["merc", "training", "physicianvisit"]
    },
    {
        "SecGrp": "CRMDB-TRAINING",
        "Primary": "training",
        "Secondary": ["crf", "merc", "physicianvisit"]
    },
    {
        "SecGrp": "CRMDB-MKTG",
        "Primary": "lead",
        "Secondary": ["physicianvisit", "merc", "training", "crf"]
    }*/
]

function arrayOf(data) {
    var arrdata = [];

    if (data == undefined || data.length == undefined) {
        arrdata.push(data);
        return arrdata;
    } else {
        return data;
    }
}

function loadCSS(url) {
    var link = document.createElement("link");
    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = url;
    document.getElementsByTagName("head")[0].appendChild(link);
}

function fnGetTemplate(strPath, strName) {
    if (Handlebars.templates === undefined || Handlebars.templates[strName] === undefined) {
        $.ajax({
            url: strPath + strName + ".hbs",
            success: function (data) {
                if (Handlebars.templates === undefined) {
                    Handlebars.templates = {};
                }
                Handlebars.templates[strName] = Handlebars.compile(data);
            },
            async: false
        });
    }
    return Handlebars.templates[strName];
}

function formatCurrency(numdata, decimal) {
    if (decimal == undefined)
        decimal = 2;
    var amtdata = parseFloat(numdata).toFixed(decimal);
    return amtdata.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

//var jsnUpdateCode = { '103107': 'listGmCodeLookupVO', '4000443': 'listGmRulesVO', '26230802': 'listGmCodeLookupExclusionVO', '26230804': 'listGmCodeLkpAccessVO', '26230803': 'listGmRFSpartVO', '103110': 'listGmGroupVO', '4000408': 'listGmGroupPartVO', '4000411': 'listGmPartNumberVO', '103111': 'listGmSetMasterVO', '4000409': 'listGmSetPartVO', '4000736': 'listGmSetAttributeVO', '103108': 'listGmSystemVO', '4000416': 'listGmSystemAttributeVO', '4000410': 'listGmFileVO', '4000442': 'listGmKeywordRefVO', '4000445': 'listGmPartyVO', '4000448': 'listGmPartyContactVO', '4000519': 'listGmAccountVO', '4000530': 'listGmAcctGpoMapVO', '4000520': 'listGmSalesRepVO', '4000629': 'listGmAssocRepMapVO', '4000524': 'listGmAcctGPOPriceVO', '4000526': 'listGmAcctPartPriceVO', '4000521': 'listGmDistributorVO', '4000533': 'listGmCaseInfoVO', '4000538': 'listGmCaseAttributeVO', '4000535': 'listGmUserVO', '4000522': 'listGmAddressVO', '4000827': 'listGmPartAttributeVO', '10306164': 'GmCRMSurgeonAffiliationVO', '10306163': 'GmCRMSurgeonSurgicalVO', '10306161': 'GmCRMSurgeonPartyVO', '10306162': 'GmCRMSurgeonAttrVO', '10306169': 'listGmSurgeonRepAdVpVO', '10306170': 'listGmActivityVO', '10306171': 'listGmActivityPartyVO', '10306172': 'listGmActivityAttrVO', '10306176': 'listGmCRFVO', '10306206': 'listGmCRFApprvVO', '10306210': 'listGmCriteriaVO', '10306209': 'listGmCriteriaAttrVO', '103109': 'listGmProjectVO', '7000443': 'listGmSecurityEventVO', '4001234': 'listGmTagInfoVO' };

var jsnUpdateCode = { '103107': 'listGmCodeLookupVO', '4000443': 'listGmRulesVO', '26230802': 'listGmCodeLookupExclusionVO', '26230804': 'listGmCodeLkpAccessVO', '26230803': 'listGmRFSpartVO', '103110': 'listGmGroupVO', '4000408': 'listGmGroupPartVO', '4000411': 'listGmPartNumberVO', '103111': 'listGmSetMasterVO', '4000409': 'listGmSetPartVO', '4000736': 'listGmSetAttributeVO', '103108': 'listGmSystemVO', '4000416': 'listGmSystemAttributeVO', '4000410': 'listGmFileVO', '4000442': 'listGmKeywordRefVO', '4000445': 'listGmPartyVO', '4000448': 'listGmPartyContactVO', '4000519': 'listGmAccountVO', '4000530': 'listGmAcctGpoMapVO', '4000520': 'listGmSalesRepVO', '4000629': 'listGmAssocRepMapVO', '4000524': 'listGmAcctGPOPriceVO', '4000526': 'listGmAcctPartPriceVO', '4000521': 'listGmDistributorVO', '4000533': 'listGmCaseInfoVO', '4000538': 'listGmCaseAttributeVO', '4000535': 'listGmUserVO', '4000522': 'listGmAddressVO', '4000827': 'listGmPartAttributeVO', '10306161': 'GmCRMSurgeonPartyVO', '7000443': 'listGmSecurityEventVO', '4001234': 'listGmTagInfoVO' };

//CRM - Hospital Affliation
//var GmCRMSurgeonAffiliationVO = { "tableName": "T1014_PARTY_AFFILIATION", "url": "surgeon/infohospafflsync", "dataHolder": "arrgmcrmsurgeonaffiliationvo", "code": "10306164", "syncGrp": "SURGEON", "syncType": "SURGEON", "syncSeq": 4, "totalSeq": 5, "afflid": "C1014_PARTY_AFFILIATION_ID", "accid": "C704_ACCOUNT_ID", "accnm": "C1014_ORGANIZATION_NAME", "priority": "C901_AFFILIATION_PRIORITY", "casepercent": "C1014_AFFILIATION_PERCENT", "acccity": "C1014_CITY_NM", "accstate": "C901_STATE_NM", "accstateid": "C901_STATE", "voidfl": "C1014_VOID_FL", "partyid": "C101_PARTY_ID", "comments": "C1014_COMMENT" }
//CRM - Surgical Activity
//var GmCRMSurgeonSurgicalVO = { "tableName": "T6610_SURGICAL_ACTIVITY", "url": "surgeon/infosurgactsync", "dataHolder": "arrgmcrmsurgeonsurgicalvo", "code": "10306163", "syncGrp": "SURGEON", "syncType": "SURGEON", "syncSeq": 3, "totalSeq": 5, "procedureid": "C901_PROCEDURE", "procedurenm": "C901_PROCEDURE_NM", "cases": "C6610_CASES", "sysid": "C207_SET_ID", "sysnm": "C6610_NON_GLOBUS_SYSTEM", "company": "C6610_COMPANY_NM", "sactid": "C6610_SURGICAL_ACTIVITY_ID", "voidfl": "C6610_VOID_FL", "partyid": "C101_PARTY_ID", "comments": "C6610_COMMENT" }
//CRM - Surgeon Party Info
var GmCRMSurgeonPartyVO = { "tableName": "T6600_PARTY_SURGEON", "url": "surgeon/infosurgparty", "dataHolder": "arrgmsurgeonlistvo", "code": "10306161", "syncGrp": "SURGEON", "syncType": "SURGEON", "syncSeq": 1, "totalSeq": 1, "npid": "C6600_SURGEON_NPI_ID", "partyid": "C101_PARTY_SURGEON_ID", "asstnm": "C6600_SURGEON_ASSISTANT_NM", "mngrnm": "C6600_SURGEON_MANAGER_NM", "voidfl": "C6600_VOID_FL" }

//CRM - Surgeon Attribute
//var GmCRMSurgeonAttrVO = { "tableName": "T1012_PARTY_OTHER_INFO", "url": "surgeon/infosurgattrparty", "dataHolder": "arrgmcrmsurgeonattrvo", "code": "10306162", "syncGrp": "SURGEON", "syncType": "SURGEON", "syncSeq": 2, "totalSeq": 5, "attrid": "C1012_PARTY_OTHER_INFO_ID", "attrtype": "C901_TYPE", "attrnm": "C1012_DETAIL_NM", "attrvalue": "C1012_DETAIL", "voidfl": "C1012_VOID_FL", "partyid": "C101_PARTY_ID" }

//CRM - REP / AD / VP Info
//var listGmSurgeonRepAdVpVO = { "tableName": "v700_territory_mapping_detail", "url": "surgeon/inforepadvpsync", "dataHolder": "arrgmadvpvo", "code": "10306169", "syncGrp": "SURGEON", "syncType": "SURGEON", "syncSeq": 5, "totalSeq": 5, "adid": "AD_ID", "adnm": "AD_NAME", "vpnm": "VP_NAME", "repnm": "REP_NAME", "repid": "REP_ID", "vpid": "VP_ID", "did": "D_ID", "dnm": "D_NAME", "regionid": "REGION_ID", "regionnm": "REGION_NAME", "ternm": "TER_NAME", "terid": "TER_ID", "acid": "AC_ID", "disttypenm": "DISTTYPENM" };
//CRM - Activity Info
//var listGmActivityVO = { "tableName": "T6630_ACTIVITY", "url": "crmactivity/infoactivitysync", "dataHolder": "arrgmactivityvo", "code": "10306170", "syncGrp": "CRM", "syncType": "CRM", "syncSeq": 1, "totalSeq": 8, "actid": "C6630_ACTIVITY_ID", "acttype": "C901_ACTIVITY_TYPE", "acttypenm": "C901_ACTIVITY_TYPE_NM", "actnm": "C6630_ACTIVITY_NM", "actstartdate": "C6630_FROM_DT", "actenddate": "C6630_TO_DT", "voidfl": "C6630_VOID_FL", "actstatus": "c901_activity_status", "partyid": "C6630_PARTY_ID", "partynm": "C6630_PARTY_NM", "actcategory": "C901_ACTIVITY_CATEGORY", "activitytype": "c901_activity_category_nm", "actstarttime": "C6630_FROM_TM", "actendtime": "C6630_TO_TM", "createdbyid": "C6630_CREATED_BY" };

//CRM - Activity Party Info
//var listGmActivityPartyVO = { "tableName": "t6631_activity_party_assoc", "url": "crmactivity/infoactivitypartysync", "dataHolder": "arrgmactivitypartyvo", "code": "10306171", "syncGrp": "CRM", "syncType": "CRM", "syncSeq": 2, "totalSeq": 8, "actpartyid": "c6631_activity_party_id", "actid": "c6630_activity_id", "roleid": "c901_activity_party_role", "rolenm": "c901_activity_party_role_nm", "partyid": "c101_party_id", "voidfl": "c6631_void_fl", "statusid": "C901_PARTY_STATUS", "statusnm": "C901_PARTY_STATUS_NM" };

//CRM - Activity Attribute Info
//var listGmActivityAttrVO = { "tableName": "t6632_activity_attribute", "url": "crmactivity/infoactivityattrsync", "dataHolder": "arrgmactivityattrvo", "code": "10306172", "syncGrp": "CRM", "syncType": "CRM", "syncSeq": 3, "totalSeq": 8, "attrid": "c6632_activity_attribute_id", "actid": "c6630_activity_id", "attrval": "c6632_attribute_value", "attrnm": "c6632_attribute_nm", "attrtype": "c901_attribute_type", "voidfl": "c6632_void_fl" };

//CRM - CRF Info
//var listGmCRFVO = { "tableName": "T6605_CRF", "url": "crf/sync", "dataHolder": "arrgmcrfvo", "code": "10306176", "syncGrp": "CRM", "syncType": "CRM", "syncSeq": 4, "totalSeq": 8, "crfid": "C6605_CRF_ID", "partyid": "C101_PARTY_ID", "actid": "C6630_ACTIVITY_ID", "reqtypeid": "C901_REQUEST_TYPE", "servicetyp": "C6605_SERVICE_TYPE", "reason": "C6605_REASON", "servicedesc": "C6605_SERVICE_DESCRIPTION", "hcpqualification": "C6605_HCP_QUALIFICATION", "preptime": "C6605_PREP_TIME", "workhrs": "C6605_WORK_HOURS", "timefl": "C6605_TIME_FL", "agrfl": "C6605_AGREEMENT_FL", "trvlfrom": "C6605_TRAVEL_FROM", "trvlto": "C6605_TRAVEL_TO", "trvltime": "C6605_TRAVEL_TIME", "trvlmile": "C6605_TRAVEL_MILES", "hrrate": "C6605_HOUR_RATE", "voidfl": "C6605_VOID_FL", "createdby": "C6605_CREATED_BY", "statusid": "C901_STATUS", "reqdate": "C6605_REQUEST_DATE", "honstatus": "C6605_HONORARIA_STATUS", "honworkhrs": "C6605_HONORARIA_WORK_HRS", "honprephrs": "C6605_HONORARIA_PREP_HRS", "honcomment": "C6605_HONORARIA_COMMENT", "priorityfl": "C6605_PRIORITY_FL" };

//CRF Approver Info
//var listGmCRFApprvVO = { "tableName": "T6605A_CRF_APPROVAL", "url": "crf/apprsync", "dataHolder": "arrgmcrfapprvvo", "code": "10306206", "syncGrp": "CRM", "syncType": "CRM", "syncSeq": 5, "totalSeq": 8, "crfapprid": "C6605A_CRF_APPROVAL_ID", "crfid": "C6605_CRF_ID", "partyid": "C101_PARTY_ID", "statusid": "C901_STATUS", "comment": "C6605A_COMMENTS" };

//CRM Criteria Info
//var listGmCriteriaVO = { "tableName": "T1101_CRITERIA", "url": "criteria/criteriasync", "dataHolder": "arrgmcriteriavo", "code": "10306210", "syncGrp": "CRM", "syncType": "CRM", "syncSeq": 6, "totalSeq": 8, "criteriaid": "C1101_CRITERIA_ID", "crfid": "C6605_CRF_ID", "criterianm": "C1101_CRITERIA_NAME", "criteriatyp": "C2201_MODULE_ID", "userid": "C101_USER_ID", "voidfl": "C1101_VOID_FL" };

//CRM Criteria Attr Info
//var listGmCriteriaAttrVO = { "tableName": "T1101A_CRITERIA_ATTRIBUTE", "url": "criteria/criteriaattrsync", "dataHolder": "arrgmcriteriaattrvo", "code": "10306209", "syncGrp": "CRM", "syncType": "CRM", "syncSeq": 7, "totalSeq": 8, "criteriaid": "C1101_CRITERIA_ID", "key": "C1101A_CRITERIA_KEY", "value": "C1101A_CRITERIA_VALUE", "voidfl": "C1101A_VOID_FL", "attrid": "C1101A_CRITERIA_ATTRIBUTE_ID" };

//CRM Project Info
//var listGmProjectVO = { "tableName": "T202_PROJECT", "url": "crmactivity/projectsync", "dataHolder": "arrgmprojectvo", "code": "103109", "syncGrp": "CRM", "syncType": "CRM", "syncSeq": 8, "totalSeq": 8, "projectid": "C202_PROJECT_ID", "projectname": "C202_PROJECT_NM", "voidfl": "C202_VOID_FL" };

//VO - Table / URL / Column Mapping
var listGmDataUpdateVO = { "tableName": "T2001_MASTER_DATA_UPDATES", "url": "devicesync/updates", "code": "", "syncType": "", "refid": "C2001_REF_ID", "reftype": "C901_REF_TYPE", "action": "C901_ACTION", "appid": "C901_APP_ID", "updateddt": "C2001_REF_UPDATED_DT", "updatedby": "C2001_LAST_UPDATED_BY", "voidfl": "C2001_VOID_FL" };

//sync detail
var GmProdCatlReqVO = { "tableName": "T9151_DEVICE_SYNC_DTL", "url": "devicesync/status", "reftype": "C901_SYNC_TYPE", "statusid": "C901_SYNC_STATUS", "syncdt": "C9151_LAST_SYNC_DATE", "totalsize": "C9151_SYNC_COUNT", "svrcnt": "C9151_SERVER_COUNT", "syncgrp": "SYNC_GRP", "syncseq": "SYNC_SEQ" };

//App
var listGmCodeLookupVO = { "tableName": "T901_CODE_LOOKUP", "url": "codelookup/master", "code": "103107", "syncType": "APP", "syncGrp": "APP", "syncSeq": 1, "totalSeq": 5, "codeid": "C901_CODE_ID", "codenm": "C901_CODE_NM", "codegrp": "C901_CODE_GRP", "activefl": "C901_ACTIVE_FL", "seqno": "C901_CODE_SEQ_NO", "codenmalt": "C902_CODE_NM_ALT", "controltype": "C901_CONTROL_TYPE", "createddate": "C901_CREATED_DATE", "createdby": "C901_CREATED_BY", "lastupdateddate": "C901_LAST_UPDATED_DATE", "lastupdatedby": "C901_LAST_UPDATED_BY", "voidfl": "C901_VOID_FL", "lockfl": "C901_LOCK_FL" };

var listGmCodeLookupExclusionVO = { "tableName": "T901D_CODE_LKP_COMPANY_EXCLN", "url": "codelookup/codelookupexcl", "code": "26230802", "parentCode": "103107", "syncType": "APP", "syncGrp": "APP", "syncSeq": 2, "totalSeq": 5, "lookupexcid": "C901D_LOOKUP_EXC_ID", "codeid": "C901_CODE_ID", "cmpid": "C1900_COMPANY_ID", "voidfl": "C901D_VOID_FL" };
 var listGmCodeLkpAccessVO = { "tableName": "T901A_LOOKUP_ACCESS", "url": "codelookup/codelookupaccess", "code": "26230804", "parentCode": "103107", "syncType": "APP", "syncGrp": "APP", "syncSeq": 3, "totalSeq": 5, "codeaccessid": "C901A_CODE_ACCESS_ID", "accessid": "C901_ACCESS_CODE_ID", "codeid": "C901_CODE_ID", "cmpid": "C1900_COMPANY_ID" };
var listGmRulesVO = { "tableName": "T906_RULES", "url": "rules/rulelookup", "code": "4000443", "syncType": "APP", "syncGrp": "APP", "syncSeq": 4, "totalSeq": 5, "ruleseqid": "C906_RULE_SEQ_ID", "ruleid": "C906_RULE_ID", "rulegrpid": "C906_RULE_GRP_ID", "ruledesc": "C906_RULE_DESC", "rulevalue": "C906_RULE_VALUE", "lastupdatedby": "C906_LAST_UPDATED_BY", "lastupdateddate": "C906_LAST_UPDATED_DATE", "voidfl": "C906_VOID_FL", "cmpid": "C1900_COMPANY_ID" };
var listGmSecurityEventVO = { "tableName": "T1530A_ACCESS", "url": "securityevent/securityeventlist", "code": "7000443", "syncType": "APP", "syncGrp": "APP", "syncSeq": 5, "totalSeq": 5, "eventname": "C1530A_EVENT_NAME", "tagname": "C1530A_TAG_NAME", "pagename": "C1530A_PAGE_NAME", "updateaccessflg": "C1530A_UPDATE_ACCESS_FL", "readaccessflg": "C1530A_READ_ACCESS_FL", "voidaccessflg": "C1530A_VOID_ACCESS_FL", "accessvoidflg": "C1530A_VOID_FL", "groupmappingvoidflg": "C1501_VOID_FL", "rulesvoidflg": "C906_VOID_FL" };
//Group
/*var listGmGroupVO = { "tableName": "T4010_GROUP", "url": "gopgroup/master", "code": "103110", "syncType": "GROUP", "syncGrp": "GROUP", "syncSeq": 1, "totalSeq": 2, "groupid": "C4010_GROUP_ID", "groupnm": "C4010_GROUP_NM", "groupdesc": "C4010_GROUP_DESC", "groupdtldesc": "C4010_GROUP_DTL_DESC", "grouppublishfl": "C4010_PUBLISH_FL", "grouptypeid": "C901_GROUP_TYPE", "grouppricedtypeid": "C901_PRICED_TYPE", "systemid": "C207_SET_ID", "voidfl": "C4010_VOID_FL" };
var listGmGroupPartVO = { "tableName": "T4011_GROUP_DETAIL", "url": "gopgroup/detail", "code": "4000408", "parentCode": "103110", "syncType": "GROUP", "syncGrp": "GROUP", "syncSeq": 2, "totalSeq": 2, "groupid": "C4010_GROUP_ID", "partnum": "C205_PART_NUMBER_ID", "pricingtypeid": "C901_PART_PRICING_TYPE" };*/

// PMT# Group sync in iPad with Interantional Changes
var listGmGroupVO = { "tableName": "T4010_GROUP", "url": "gopgroup/masterjson", "code": "103110", "syncType": "GROUP", "syncGrp": "GROUP", "syncSeq": 1, "totalSeq": 2, "gid": "C4010_GROUP_ID", "gnm": "C4010_GROUP_NM", "gdes": "C4010_GROUP_DESC", "gdd": "C4010_GROUP_DTL_DESC", "gpf": "C4010_PUBLISH_FL", "gt": "C901_GROUP_TYPE", "gpt": "C901_PRICED_TYPE", "sid": "C207_SET_ID", "vfl": "C4010_VOID_FL" };
var listGmGroupPartVO = { "tableName": "T4011_GROUP_DETAIL", "url": "gopgroup/detailjson", "code": "4000408", "parentCode": "103110", "syncType": "GROUP", "syncGrp": "GROUP", "syncSeq": 2, "totalSeq": 2, "gid": "C4010_GROUP_ID", "pid": "C205_PART_NUMBER_ID", "gpt": "C901_PART_PRICING_TYPE" };


//Part
/*var listGmPartNumberVO = { "tableName": "T205_PART_NUMBER", "url": "parts/master", "code": "4000411", "syncType": "PART", "syncGrp": "PART", "syncSeq": 1, "totalSeq": 3, "partnum": "C205_PART_NUMBER_ID", "partdesc": "C205_PART_NUM_DESC", "partdtldesc": "C205_PART_NUM_DETAIL", "partstat": "C901_STATUS_ID", "partfmly": "C205_PRODUCT_FAMILY", "partactfl": "C205_ACTIVE_FL", "systemid": "C207_SET_SALES_SYSTEM_ID", "productmaterial": "C205_PRODUCT_MATERIAL", "productclass": "C205_PRODUCT_CLASS", "dinumber": "C2060_DI_NUMBER", "udipattern": "UDI_Pattern" };
var listGmPartAttributeVO = { "tableName": "T205D_PART_ATTRIBUTE", "url": "parts/partattr", "code": "4000827", "parentCode": "4000411", "syncType": "PART", "syncGrp": "PART", "syncSeq": 2, "totalSeq": 3, "partattrid": "C2051_PART_ATTRIBUTE_ID", "pnum": "C205_PART_NUMBER_ID", "partattrtype": "C901_ATTRIBUTE_TYPE", "partattrval": "C205D_ATTRIBUTE_VALUE", "partattrvoidfl": "C205D_VOID_FL" }; //
var listGmRFSpartVO = { "tableName": "T901C_RFS_COMPANY_MAPPING", "url": "parts/partRFS", "code": "26230803", "parentCode": "4000411", "syncType": "PART", "syncGrp": "PART", "syncSeq": 3, "totalSeq": 3, "rfsmapid": "C901C_RFS_COMP_MAP_ID", "rfsid": "C901_RFS_ID", "cmpid": "C1900_COMPANY_ID", "voidl": "C901C_VOID_FL" };*/

// PMT# Part sync in iPad with Interantional Changes
var listGmPartNumberVO = { "tableName": "T205_PART_NUMBER", "url": "parts/masterjson", "code": "4000411", "syncType": "PART", "syncGrp": "PART", "syncSeq": 1, "totalSeq": 3, "pid": "C205_PART_NUMBER_ID", "pdesc": "C205_PART_NUM_DESC", "pdet": "C205_PART_NUM_DETAIL", "pst": "C901_STATUS_ID", "pfm": "C205_PRODUCT_FAMILY", "paf": "C205_ACTIVE_FL", "sid": "C207_SET_SALES_SYSTEM_ID", "prm": "C205_PRODUCT_MATERIAL", "prc": "C205_PRODUCT_CLASS", "pdi": "C2060_DI_NUMBER", "udi": "UDI_Pattern" };
var listGmPartAttributeVO = { "tableName": "T205D_PART_ATTRIBUTE", "url": "parts/partattrjson", "code": "4000827", "parentCode": "4000411", "syncType": "PART", "syncGrp": "PART", "syncSeq": 2, "totalSeq": 3, "paid": "C2051_PART_ATTRIBUTE_ID", "pid": "C205_PART_NUMBER_ID", "pat": "C901_ATTRIBUTE_TYPE", "pav": "C205D_ATTRIBUTE_VALUE", "pavf": "C205D_VOID_FL" };
var listGmRFSpartVO = { "tableName": "T901C_RFS_COMPANY_MAPPING", "url": "parts/partRFS", "code": "26230803", "parentCode": "4000411", "syncType": "PART", "syncGrp": "PART", "syncSeq": 3, "totalSeq": 3, "rfsmapid": "C901C_RFS_COMP_MAP_ID", "rfsid": "C901_RFS_ID", "cmpid": "C1900_COMPANY_ID", "voidl": "C901C_VOID_FL" };

//Tag
var listGmTagInfoVO = {"tableName": "T5010_TAG_DTL", "url": "tag/fetchTagInfo", "syncType": "TAG", "syncGrp": "TAG", "code": "4001234", "syncSeq": 1, "totalSeq": 1, "tid": "C5010_TAG_ID", "sid": "C207_Set_Id" ,"sts": "C901_Status", "vfl": "C5010_Void_Fl"};

//Set
/*var listGmSetMasterVO = { "tableName": "T207_SET_MASTER", "url": "system/setmaster", "code": "103111", "syncType": "SET", "syncGrp": "SET", "syncSeq": 1, "totalSeq": 3, "setid": "C207_SET_ID", "setnm": "C207_SET_NM", "setdesc": "C207_SET_DESC", "setdtldesc": "C207_SET_DETAIL", "systemid": "C207_SET_SALES_SYSTEM_ID", "setcategoryid": "C207_CATEGORY", "settypeid": "C207_TYPE", "sethierarchyid": "C901_HIERARCHY", "setstatusid": "C901_STATUS_ID", "voidfl": "C207_VOID_FL" };
var listGmSetPartVO = { "tableName": "T208_SET_DETAILS", "url": "system/setpart", "code": "4000409", "parentCode": "103111", "syncType": "SET", "syncGrp": "SET", "syncSeq": 2, "totalSeq": 3, "setid": "C207_SET_ID", "partnum": "C205_PART_NUMBER_ID", "setqty": "C208_SET_QTY", "insetfl": "C208_INSET_FL", "voidfl": "C208_VOID_FL" };
var listGmSetAttributeVO = { "tableName": "TBL_SET_ATTRIBUTE", "url": "system/setAttr", "syncType": "SET", "syncGrp": "SET", "code": "4000736", "parentCode": "103111", "syncSeq": 3, "totalSeq": 3, "setid": "C207_SET_ID", "attrtype": "C901_ATTRIBUTE_TYPE", "attrvalue": "C207C_ATTRIBUTE_VALUE", "voidfl": "C207C_VOID_FL" };*/

// PMT# Set sync in iPad with Interantional Changes
var listGmSetMasterVO = { "tableName": "T207_SET_MASTER", "url": "system/setmasterjson", "code": "103111", "syncType": "SET", "syncGrp": "SET", "syncSeq": 1, "totalSeq": 3, "sid": "C207_SET_ID", "snm": "C207_SET_NM", "sdc": "C207_SET_DESC", "sdd": "C207_SET_DETAIL", "ssi": "C207_SET_SALES_SYSTEM_ID", "cid": "C207_CATEGORY", "typ": "C207_TYPE", "hid": "C901_HIERARCHY", "sts": "C901_STATUS_ID", "vfl": "C207_VOID_FL" };
var listGmSetPartVO = { "tableName": "T208_SET_DETAILS", "url": "system/setpartjson", "code": "4000409", "parentCode": "103111", "syncType": "SET", "syncGrp": "SET", "syncSeq": 2, "totalSeq": 3, "sid": "C207_SET_ID", "pnm": "C205_PART_NUMBER_ID", "qty": "C208_SET_QTY", "ifl": "C208_INSET_FL", "vfl": "C208_VOID_FL" };
var listGmSetAttributeVO = { "tableName": "TBL_SET_ATTRIBUTE", "url": "system/setAttrjson", "syncType": "SET", "syncGrp": "SET", "code": "4000736", "parentCode": "103111", "syncSeq": 3, "totalSeq": 3, "sid": "C207_SET_ID", "att": "C901_ATTRIBUTE_TYPE", "atv": "C207C_ATTRIBUTE_VALUE", "vfl": "C207C_VOID_FL" };

//System
/*var listGmSystemVO = { "tableName": "T207_SET_MASTER", "url": "system/master", "code": "103108", "syncType": "SYSTEM", "syncGrp": "SYSTEM", "syncSeq": 1, "totalSeq": 2, "systemid": "C207_SET_ID", "systemnm": "C207_SET_NM", "systemdesc": "C207_SET_DESC", "systemdtldesc": "C207_SET_DETAIL", "systemstatusid": "C901_STATUS_ID", "voidfl": "C207_VOID_FL" };
var listGmSystemAttributeVO = { "tableName": "T207C_SET_ATTRIBUTE", "url": "system/systemattrib", "syncType": "SYSTEM", "syncGrp": "SYSTEM", "code": "4000416", "parentCode": "103108", "syncSeq": 2, "totalSeq": 2, "systemid": "C207_SET_ID", "attribtypeid": "C901_ATTRIBUTE_TYPE", "attribvalue": "C207C_ATTRIBUTE_VALUE", "voidfl": "C207C_VOID_FL" };*/

// PMT# System sync in iPad with Interantional Changes
var listGmSystemVO = { "tableName": "T207_SET_MASTER", "url": "system/masterjson", "code": "103108", "syncType": "SYSTEM", "syncGrp": "SYSTEM", "syncSeq": 1, "totalSeq": 2, "sid": "C207_SET_ID", "snm": "C207_SET_NM", "sdc": "C207_SET_DESC", "sdd": "C207_SET_DETAIL", "sts": "C901_STATUS_ID", "vfl": "C207_VOID_FL" };
var listGmSystemAttributeVO = { "tableName": "T207C_SET_ATTRIBUTE", "url": "system/systemattribjson", "syncType": "SYSTEM", "syncGrp": "SYSTEM", "code": "4000416", "parentCode": "103108", "syncSeq": 2, "totalSeq": 2, "sid": "C207_SET_ID", "att": "C901_ATTRIBUTE_TYPE", "atv": "C207C_ATTRIBUTE_VALUE", "vfl": "C207C_VOID_FL" };

//File
//var listGmFileVO = { "tableName": "T903_UPLOAD_FILE_LIST", "url": "uploadedFile/master", "code": "4000410", "syncType": "FILE", "syncGrp": "FILE", "syncSeq": 1, "totalSeq": 2, "fileid": "C903_UPLOAD_FILE_LIST", "filenm": "C903_FILE_NAME", "filerefid": "C903_REF_ID", "type": "C901_REF_TYPE", "filesize": "C903_FILE_SIZE", "fileseq": "C903_FILE_SEQ_NO", "filetitle": "C901_FILE_TITLE", "filerefgroup": "C901_REF_GRP", "uploadedby": "C903_LAST_UPDATED_BY", "uploadeddt": "C903_LAST_UPDATED_DATE", "voidfl": "C903_DELETE_FL", "subtype": "C901_TYPE", "shareablefl": "C903_SHARE_FLAG" };

var listGmFileVO = { "tableName": "T903_UPLOAD_FILE_LIST", "url": "uploadedFile/masterjson", "code": "4000410", "syncType": "FILE", "syncGrp": "FILE", "syncSeq": 1, "totalSeq": 2, "fid": "C903_UPLOAD_FILE_LIST", "fnm": "C903_FILE_NAME", "frid": "C903_REF_ID", "frt": "C901_REF_TYPE", "fsz": "C903_FILE_SIZE", "fsq": "C903_FILE_SEQ_NO", "fti": "C901_FILE_TITLE", "frg": "C901_REF_GRP", "lub": "C903_LAST_UPDATED_BY", "lud": "C903_LAST_UPDATED_DATE", "vfl": "C903_DELETE_FL", "fst": "C901_TYPE", "fsf": "C903_SHARE_FLAG", "fpid": "C205_PART_NUMBER_ID" };
var listGmKeywordRefVO = { "tableName": "T2050_KEYWORD_REF", "url": "keyword/master", "code": "4000442", "syncType": "FILE", "syncGrp": "FILE", "syncSeq": 2, "totalSeq": 2, "keywordid": "C2050_KEYWORD_REF_ID", "keywordnm": "C2050_KEYWORD_REF_NAME", "refid": "C2050_REF_ID", "reftype": "C901_REF_TYPE", "voidfl": "C2050_VOID_FL", "updatedby": "C2050_LAST_UPDATED_BY", "updateddt": "C2050_LAST_UPDATED_DT" };
//Contact
var listGmPartyVO = { "tableName": "T101_PARTY", "url": "party/master", "code": "4000445", "syncType": "CONTACT", "syncGrp": "CONTACT", "syncSeq": 1, "totalSeq": 2, "partynm": "c101_party_nm", "partyid": "C101_PARTY_ID", "firstnm": "C101_FIRST_NM", "lastnm": "C101_LAST_NM", "midinitial": "C101_MIDDLE_INITIAL", "partytype": "C901_PARTY_TYPE", "activefl": "C101_ACTIVE_FL", "voidfl": "C101_VOID_FL" };
var listGmPartyContactVO = { "tableName": "T107_CONTACT", "url": "party/contact", "code": "4000448", "syncType": "CONTACT", "syncGrp": "CONTACT", "syncSeq": 2, "totalSeq": 2, "partyid": "C101_PARTY_ID", "contactid": "C107_CONTACT_ID", "contactmode": "C901_MODE", "contactval": "C107_CONTACT_VALUE", "primaryfl": "C107_PRIMARY_FL", "inactivefl": "C107_INACTIVE_FL", "voidfl": "C107_VOID_FL", "contacttype": "C107_CONTACT_TYPE" };
//Account
var listGmAccountVO = { "tableName": "T704_ACCOUNT", "url": "master/accounts", "code": "4000519", "syncType": "ACCOUNT", "syncGrp": "MASTER", "syncSeq": 1, "totalSeq": 6, "acctid": "C704_ACCOUNT_ID", "acctname": "C704_ACCOUNT_NM", "repid": "C703_SALES_REP_ID", "acctype": "C740_GPO_CATALOG_ID", "billname": "C704_BILL_NAME", "billaddr1": "C704_BILL_ADD1", "billaddr2": "C704_BILL_ADD2", "billcity": "C704_BILL_CITY", "billstate": "C704_BILL_STATE", "billcountry": "C704_BILL_COUNTRY", "billzipcode": "C704_BILL_ZIP_CODE", "shipname": "C704_SHIP_NAME", "shipaddr1": "C704_SHIP_ADD1", "shipaddr2": "C704_SHIP_ADD2", "shipcity": "C704_SHIP_CITY", "shipstate": "C704_SHIP_STATE", "shipcountry": "C704_SHIP_COUNTRY", "shipzipcode": "C704_SHIP_ZIP_CODE", "actvfl": "C704_ACTIVE_FL", "voidfl": "C704_VOID_FL", "compid": "C901_COMPANY_ID", "extcountryid": "C901_EXT_COUNTRY_ID", "dealid": "C101_DEALER_ID"
};
var listGmAcctGpoMapVO = { "tableName": "T740_GPO_ACCOUNT_MAPPING", "url": "master/acctgpomap", "code": "4000530", "parentCode": "4000519", "syncType": "ACCOUNT", "syncGrp": "MASTER", "syncSeq": 2, "totalSeq": 6, "acctgpomapid": "C740_GPO_ACCOUNT_MAP_ID", "acctid": "C704_ACCOUNT_ID", "partyid": "C101_PARTY_ID", "gpocatalogid": "C740_GPO_CATALOG_ID", "voidfl": "C740_VOID_FL" };
//SalesRep
var listGmSalesRepVO = { "tableName": "T703_SALES_REP", "url": "master/salesreps", "code": "4000520", "syncType": "SALESREP", "syncGrp": "MASTER", "syncSeq": 3, "totalSeq": 6, "repid": "C703_SALES_REP_ID", "repname": "C703_SALES_REP_NAME", "did": "C701_DISTRIBUTOR_ID", "partyid": "C101_PARTY_ID", "actvfl": "C703_ACTIVE_FL", "territoryid": "C702_TERRITORY_ID", "voidfl": "C703_VOID_FL", "extcountryid": "C901_EXT_COUNTRY_ID", "desgn": "C901_DESIGNATION" };
//Associate SalesRep
var listGmAssocRepMapVO = { "tableName": "T704a_ACCOUNT_REP_MAPPING", "url": "master/assocreps", "code": "4000629", "syncType": "ASSALESREP", "syncGrp": "MASTER", "syncSeq": 4, "totalSeq": 6, "accasrepmapid": "C704A_ACCOUNT_REP_MAP_ID", "acctid": "C704_ACCOUNT_ID", "repid": "C703_SALES_REP_ID", "activefl": "C704A_ACTIVE_FL", "voidfl": "C704A_VOID_FL" };
//Distributor
var listGmDistributorVO = { "tableName": "T701_DISTRIBUTOR", "url": "master/distributors", "code": "4000521", "syncType": "DISTRIBUTOR", "syncGrp": "MASTER", "syncSeq": 5, "totalSeq": 6, "did": "C701_DISTRIBUTOR_ID", "dname": "C701_DISTRIBUTOR_NAME", "regionid": "C701_REGION", "actvfl": "C701_ACTIVE_FL", "billaddr1": "C701_BILL_ADD1", "billname": "C701_BILL_NAME", "billaddr2": "C701_BILL_ADD2", "billstate": "C701_BILL_STATE", "billcountry": "C701_BILL_COUNTRY", "billcity": "C701_BILL_CITY", "billzipcode": "C701_BILL_ZIP_CODE", "shipname": "C701_SHIP_NAME", "shipaddr1": "C701_SHIP_ADD1", "shipaddr2": "C701_SHIP_ADD2", "shipcity": "C701_SHIP_CITY", "shipstate": "C701_SHIP_STATE", "shipcountry": "C701_SHIP_COUNTRY", "shipzipcode": "C701_SHIP_ZIP_CODE", "extcountryid": "C901_EXT_COUNTRY_ID", "voidfl": "C701_VOID_FL" };
//Address
var listGmAddressVO = { "tableName": "T106_ADDRESS", "url": "address/master", "code": "4000522", "syncType": "ADDRESS", "syncGrp": "MASTER", "syncSeq": 6, "totalSeq": 6, "addrid": "C106_ADDRESS_ID", "addresstype": "C901_ADDRESS_TYPE", "partyid": "C101_PARTY_ID", "billadd1": "C106_ADD1", "billadd2": "C106_ADD2", "billcity": "C106_CITY", "state": "C901_STATE", "country": "C901_COUNTRY", "zipcode": "C106_ZIP_CODE", "preference": "C106_SEQ_NO", "activeflag": "C106_INACTIVE_FL", "primaryflag": "C106_PRIMARY_FL", "voidfl": "C106_VOID_FL" };

//Price
var listGmAcctGPOPriceVO = { "tableName": "T705_ACCOUNT_PRICING", "dataHolder": "listGmAcctPartPriceVO", "url": "accounts/grpprice", "code": "4000524", "syncType": "PRICE", "syncGrp": "PRICE", "syncSeq": 1, "totalSeq": 2, "acctpriceid": "C705_ACCOUNT_PRICING_ID", "acctid": "C704_ACCOUNT_ID", "partnum": "C205_PART_NUMBER_ID", "price": "C7051_PRICE", "gpoid": "C101_GPO_ID", "grpid": "C4010_GROUP_ID", "grptype": "C901_GROUP_TYPE", "voidfl": "C7051_VOID_FL", "actvfl": "C7051_ACTIVE_FL" };
var listGmAcctPartPriceVO = { "tableName": "T705_ACCOUNT_PRICING", "dataHolder": "listGmAcctPartPriceVO", "url": "accounts/acctprice", "code": "4000526", "syncType": "PRICE", "syncGrp": "PRICE", "syncSeq": 2, "totalSeq": 2, "acctpriceid": "C705_ACCOUNT_PRICING_ID", "acctid": "C704_ACCOUNT_ID", "partnum": "C205_PART_NUMBER_ID", "price": "C7051_PRICE", "gpoid": "C101_GPO_ID", "grpid": "C4010_GROUP_ID", "grptype": "C901_GROUP_TYPE", "voidfl": "C7051_VOID_FL", "actvfl": "C7051_ACTIVE_FL" };

//Reset
var GmDeviceSyncClear = { "tableName": "", "url": "devicesync/clear" };
var resetTables = ["T205_PART_NUMBER", "T207_SET_MASTER", "T208_SET_DETAILS", "T901_CODE_LOOKUP", "T903_UPLOAD_FILE_LIST", "T2001_MASTER_DATA_UPDATES", "T4010_GROUP", "T4011_GROUP_DETAIL", "TBL_FAVORITE", "TBL_RECENT", "TBL_LASTSYNC", "T101_PARTY", "T107_CONTACT", "TBL_PARTY", "TBL_CONTACT", "T906_RULES", "TBL_SHARE_LIST", "TBL_SHARE", "T704_ACCOUNT", "T740_GPO_ACCOUNT_MAPPING", "T703_SALES_REP", "T705_ACCOUNT_PRICING", "T701_DISTRIBUTOR", "TBL_DASHBOARD_SEQ", "T7100_CASE_INFORMATION", "T7102_CASE_ATTRIBUTE", "TBL_CASE_INFORMATION", "TBL_SET_ATTRIBUTE", "T101_USER", "T9151_DEVICE_SYNC_DTL", "T704a_ACCOUNT_REP_MAPPING", "T106_ADDRESS", "TBL_REQUEST", "T2050_KEYWORD_REF", "T205D_PART_ATTRIBUTE", "T207C_SET_ATTRIBUTE", "TBL_CASE", "TBL_CASE_ATTRIBUTE", "T1012_PARTY_OTHER_INFO", "T6600_PARTY_SURGEON", "T1014_PARTY_AFFILIATION", "T6610_SURGICAL_ACTIVITY", "v700_territory_mapping_detail", "t902_log", "T6630_ACTIVITY", "t6631_activity_party_assoc", "t6632_activity_attribute", "TBL_SURGEON", "TBL_ACTIVITY", "T6605_CRF", "T6605A_CRF_APPROVAL", "T1101_CRITERIA", "T1101A_CRITERIA_ATTRIBUTE", "t202_project", "T901D_CODE_LKP_COMPANY_EXCLN", "T901A_LOOKUP_ACCESS",
"T901C_RFS_COMPANY_MAPPING", "T1530A_ACCESS","T5010_TAG_DTL","TBL_DO_TAG_IMG_DTL","TBL_DO_TAG_DETAILS"];

//case Schedule
var listGmCaseInfoVO = { "tableName": "T7100_CASE_INFORMATION", "url": "caseschdl/sync", "code": "4000533", "syncType": "CASE", "syncGrp": "CASE", "syncSeq": 1, "totalSeq": 2, "cainfoid": "C7100_CASE_INFO_ID", "caseid": "C7100_CASE_ID", "surdt": "C7100_SURGERY_DATE", "surtime": "C7100_SURGERY_TIME", "did": "C701_DISTRIBUTOR_ID", "repid": "C703_SALES_REP_ID", "acid": "C704_ACCOUNT_ID", "createdby": "C7100_CREATED_BY", "voidfl": "C4010_VOID_FL", "prnotes": "C7100_PERSONAL_NOTES", "status": "C901_CASE_STATUS", "prtcaid": "C7100_PARENT_CASE_INFO_ID", "type": "C901_TYPE" };
var listGmCaseAttributeVO = { "tableName": "T7102_CASE_ATTRIBUTE", "url": "caseschdl/syncattr", "code": "4000538", "syncType": "CASE", "syncGrp": "CASE", "syncSeq": 2, "totalSeq": 2, "caattrid": "C7102_CASE_ATTRIBUTE_ID", "cainfoid": "C7100_CASE_INFO_ID", "attrtp": "C901_ATTRIBUTE_TYPE", "attrval": "C7102_ATTRIBUTE_VALUE", "voidfl": "C7102_VOID_FL" };

//User
var listGmUserVO = { "tableName": "T101_USER", "url": "master/users", "code": "4000535", "syncType": "USER", "syncGrp": "USER", "syncSeq": 1, "totalSeq": 1, "userid": "C101_USER_ID", "fname": "C101_USER_F_NAME", "lname": "C101_USER_L_NAME", "shname": "C101_USER_SH_NAME", "emailid": "C101_EMAIL_ID", "deptid": "C101_DEPT_ID", "acslvl": "C101_ACCESS_LEVEL_ID", "did": "C701_DISTRIBUTOR_ID", "partyid": "C101_PARTY_ID", "status": "C901_USER_STATUS", "type": "C901_USER_TYPE", "hiredt": "C101_HIRE_DT","termtdt": "C101_TERMINATION_DATE"
};


//Dashboard Drilldown Passnames
var jsnDrillDown = {
    "loaner": {

        "VO": "listGmLoanerDrilldownVO",
        "title": "Loaners",
        "linkKey": "setid",
        "secondParameter": "setnm",
        "url": "/GmSetReportServlet?hOpt=SETRPT&hFromPage=Parent&hAction=Drilldown&hidePaging=YES&hSetId=",
        "cssOptions": "table.DtTable850 {margin: auto!important;} .even{background-color:#eee!important;}",
        "scriptOptions": "",

        "requested": {
            "passname": "REQUESTED",
            "title": "Requested"
        },
        "approved": {
            "passname": "APPROVED",
            "title": "Approved"
        },
        "allloaners": {
            "passname": "Shipped",
            "title": "All Loaners"
        },
        "duetoday": {
            "passname": "DueToday",
            "title": "Due Today"
        },
        "duein1day": {
            "passname": "Due1Day",
            "title": "Due in 1 Day"
        },
        "duein2days": {
            "passname": "Due2Day",
            "title": "Due in 2 Days"
        },
        "overdue": {
            "passname": "OverDue",
            "title": "Overdue"
        },
        "missingparts": {
            "passname": "",
            "title": "Missing Parts"
        },
        "latefeeaccrstmt": {
            "passname": "accrlatefeespastmon",
            "title": "Late Fee Accrual Statement"
        },
        "recentlatefeeaccr": {
            "passname": "accrlatefeescurr",
            "title": "Recent Late Fee Accruals"
        }
    },

    "order": {

        "VO": "listGmOrdersDrilldownVO",
        "title": "Orders",
        "url": "/GmEditOrderServlet?hMode=PrintPrice&hParantForm=SALESDASHBOARD&hOrdId=",
        "cssOptions": " table { margin:auto!important; } .button-red{ display:none; }",
        "scriptOptions": "",

        "submitted": {
            "passname": "",
            "title": "Submitted"
        },
        "today": {
            "stropt": "todaysales",
            "title": ""
        },
        "shippedinlast7days": {
            "passname": "",
            "title": "Shipped in last 7 days"
        },
        "heldorders": {
            "passname": "",
            "title": "Held Orders"
        },
        "backorders": {
            "passname": "",
            "title": "Back Orders"
        },
        "pendingpo14days": {
            "passname": "PO14day",
            "title": "Pending PO < 14 days"
        },
        "pendingpo1421days": {
            "passname": "PO14-21day",
            "title": "Pending PO 14 - 21 days"
        },
        "pendingpo21days": {
            "passname": "PO21day",
            "title": "Pending PO > 21 days"
        }
    },

    "return": {

        "VO": "listGmReturnsDrilldownVO",
        "title": "Returns",
        "url": "/GmPrintCreditServlet?hAction=VIEW&hRAID=",
        "cssOptions": "table {margin: auto!important;}",
        "scriptOptions": "",

        "rainitiatedinlast60days": {
            "passname": "Initiated",
            "title": "RA - Initiated in Last 60 days"
        },
        "raclosedinlast60days": {
            "passname": "Closed",
            "title": "RA - Closed in Last 60 days"
        }
    },

    "case": {

        "VO": "listGmCaseDrilldownVO",
        "title": "Cases Scheduled",
        "url": "/gmCasePost.do?parentForm=SALESDASHBOARD&caseInfoID=",
        "cssOptions": "table {margin: auto!important;} input[type='button'], img{display:none!important;} .oddshade {background-color:#eee!important;} a[href='#none']{display: block!important;} .even{background-color:#eee!important;}",
        "scriptOptions": "",

        "today": {
            "passname": "Today",
            "title": "Today"
        },
        "tomorrow": {
            "passname": "Tomorrow",
            "title": "Tomorrow"
        },
        "innext5days": {
            "passname": "NextXDays",
            "title": "In next 5 days"
        }
    },

    "consignedsets": {

        "VO": "listGmConsignmentDrilldownVO",
        "title": "Consignment - Sets",
        "url": "/gmRequestMaster.do?strOpt=print&strPgBrk=no&requestId=",
        "cssOptions": "table.DtTable500 {margin: auto!important;} body{background-color:#eee;}",
        "scriptOptions": "var tds = document.getElementsByTagName('td'); for(var i=0;i<tds.length;i++) { if(tds[i].style.backgroundColor=='rgb(178, 210, 242)') {tds[i].style.backgroundColor = '#999'; tds[i].getElementsByTagName('span')[0].style.backgroundColor = '#999';} }  ",

        "requested": {
            "passname": "",
            "title": "Requested"
        },
        "approved": {
            "passname": "",
            "title": "Approved"
        },
        "shippedinlast7days": {
            "passname": "",
            "title": "Shipped in last 7 days"
        },
        "backorder": {
            "passname": "",
            "title": "Back Orders"
        },
        "allsets": {
            "passname": "",
            "title": "All Sets"
        },
        "setsmissinglocation": {
            "passname": "",
            "title": "Sets missing location"
        }
    },

    "consigneditems": {

        "VO": "listGmConsignmentDrilldownVO",
        "title": "Consignment - Items",
        "url": "/gmRequestMaster.do?strOpt=print&strPgBrk=no&requestId=",
        "cssOptions": "table.DtTable500 {margin: auto!important;} body{background-color:#eee;}",
        "scriptOptions": "",

        "requested": {
            "passname": "Requested",
            "title": "Requested"
        },
        "approved": {
            "passname": "Approved",
            "title": "Approved"
        },
        "shippedinlast7days": {
            "passname": "Shipped",
            "title": "Shipped in last 7 days"
        },
        "backorder": {
            "passname": "BO",
            "title": "Back Orders"
        },
    }
}





//Image Type Mapping
var imgsystem = '103092';
var imgset = '103091';
var imggroup = '103094';
var imgpart = '103090';


var langid = "103097";

 var JSONInvReport = { "token": "", "companyid": "", "divids": "", "zoneids": "", "regionids": "", "fsids": "", "action": "", "salestype": "", "contype": "", "turns": "3", "showai": "Y", "frmmonth": "01", "frmyear": "2014", "tomonth": "01", "toyear": "2015", "sysonlycon": "", "baselinenm": "", "setnum": "", "did": "", "type": "", "loantypdisable": "", "pnum": "", "conperiod": "", "hsetnum": "", "consummtype": "", "repid": "", "showarrowfl": "", "consettype": "", "hpnum": "", "adid": "", "acctid": "", "loanertype": "", "headprefix": "", "status": "", "hcbonm": "", "vpid": "", "hideamtfl": "", "turnslabel": "", "state": "", "showdisabled": "", "terrid": "", "showpercentfl": "", "setid": "", "condition": "", "accessfilter": "", "region": "", "groupaccid": "" }



var adjustNow = new Date();
adjustNow.dst();


//New Product Launch Splash Screens

//Open the Splash Screen in full screen wider and initializes the slider
function openSplashScreen(data) {
    if ($("#div-splash-container").css("display") == "none") {
        var splashtmplte = fnGetTemplate(URL_Common_Template, "GmSplashList");

        var i = 1;
        _.each(data, function (row) {
            row.sequence = i++;
            row.total = data.length;
        });

        var randomSplashID = parseInt(Math.random() * 1000000).toString();

        $("#div-splash-container .div-splash-slides").remove();
        var divSplash = document.createElement('div');
        divSplash.id = "splash-slides-" + randomSplashID;
        $(divSplash).addClass("div-splash-slides");
        $("#div-splash-container").append(divSplash);

        $("#splash-slides-" + randomSplashID).html(splashtmplte(data));

        $("#splash-slides-" + randomSplashID).find('#div-splash-image-container').each(function () {
            if ($(this).css("background-size") == "contain") {
                var divHolder = this,
                    src = $(this).css("background-image").replace('url(', '').replace(')', '');
                console.log(src);
                $("<img/>").attr("src", src).error(function () {
                    $(divHolder).css("background-image", "url('globusMobileApp/image/icon/ios/missing.jpeg')");
                    $(this).remove();
                })
            }
        });

        $("#div-splash-container").fadeIn('fast');

        if (data.length > 1) {
            $(".btn-splash-navigation").show();
            $("#splash-slides-" + randomSplashID).slidesjs({
                width: 1024,
                height: 700,
                navigation: false,
                pagination: false,
                callback: {
                    loaded: function (number) {
                        fnHideSplashButtons(number);
                    },
                    complete: function (number) {
                        fnHideSplashButtons(number);
                    }
                }
            });
        } else {
            $(".btn-splash-navigation").hide();
            $(".div-splash-image-container").addClass("div-nosplash-image-container");
            fnHideSplashButtons(1);
        }

        $(".btn-splash").bind("click", fnHideSplash);
        $("#div-splash-backward").click(function () {
            $(".btn-splash-backward").trigger("click");
        });

        $("#div-splash-forward").click(function () {
            $(".btn-splash-forward").trigger("click");
        });
    }
}

function fnHideSplashButtons(number) {
    var splashDisplay = document.getElementsByClassName("div-splash-single-container");
    slidNumber = parseInt(number - 1);
    var target = splashDisplay[slidNumber];
    var targetclass = $(target).attr("class");
    targetclass = targetclass.replace("div-splash-single-container", "");
    targetclass = targetclass.replace("slidesjs-slide", "");
    targetclass = targetclass.replace("div-splash-type-", "");
    targetclass = targetclass.replace(" ", "");
    $(".btn-splash").show();
    if (targetclass == 4000788) {
        $(".btn-splash-learn").hide();
    } else if (targetclass == 4000789) {
        $(".btn-splash-accept").hide();
    }
}


//Closes the Splash screen after Accept or Learn More is chosed by the user
function fnHideSplash(event) {
    $(".btn-splash").unbind("click");
    $("#div-splash-container").fadeOut('fast');
    var splashDisplay = document.getElementsByClassName("div-splash-single-container");
    var target = splashDisplay[slidNumber];
    var fileid = $(target).attr("file-id");
    var sysid = $(target).attr("sys-id");
    fnUpdateSplashFile(fileid);
    if ($(event.currentTarget).hasClass("btn-splash-learn")) {
        if (catalogDetail) {
            catalogDetail.unbind();
            catalogDetail.remove();
            catalogDetail = undefined;
            $("#btn-back").unbind();
        }
        setTimeout(function () {
            fnTimerSplashScreen()
        }, 120000); // 2 Miniutes for Test purpose
        pcBreadcrumb = [];
        pcBreadcrumb.push("<a href='#catalog/System'>SYSTEM</a>");
        $("#div-app-title").show().html("Product Catalog");
        window.location.href = "#catalog/detail/system/" + sysid;
    } else if (splashDisplay.length > 1) {
        $("#div-splash-container").css("display", "none");
        fnGetSplashFiles(function (data) {
            setTimeout(function () {
                openSplashScreen(data)
            }, 250); //Timer given for the slider to refresh
        });
    }
}

//Checks for any New Product Launch Splash Availbility
function fnTimerSplashScreen() {
    if (navigator.onLine) {
        window.timerSplashScreen = window.setTimeout(function () {
            var loc = window.location.hash;
            if (loc != "" && loc.indexOf('dashboard') < 0 && loc.indexOf('do') < 0 && loc.indexOf('inv') < 0 && loc.indexOf('case') < 0 && loc.indexOf('settings') < 0) {
                fnGetSplashFiles(function (data) {
                    if (data.length > 0)
                        openSplashScreen(data);
                });
            } 
//            else
//                fnTimerSplashScreen();
        }, timerSplashLimit);
    }
}

/* Get Server Data through Web Service */
function fnGetWebServerData(serviceurl, voname, input, callback, keepCall) {
    //console.log("Calling fnGetWebServerData with the following data <br> " + JSON.stringify(input));
 
    if (input == undefined)
        input = { "token": token };
    else
        input.token = token;
    //passing user's company info with all the web service calls
    input.cmpid = localStorage.getItem("cmpid");
    input.cmptzone = localStorage.getItem("cmptzone");
    input.cmpdfmt = localStorage.getItem("cmpdfmt");
    input.plantid = localStorage.getItem("plantid");
    $.support.cors = true;
    $.ajax({
        beforeSend: function (jqXHR) {
            if (keepCall == undefined)
                $.xhrPool.push(jqXHR);
        },
        async: true,
        url: URL_WSServer + serviceurl,
        method: "POST",
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(input),
        success: function (data) {
            // console.log(JSON.stringify(data));
            if ((data != null) && (voname != undefined) && eval("data." + voname) != undefined) {
                callback(eval("data." + voname));
            } else {
                callback(data);
            }
        },
        error: function (model, response, errorReport) {
            console.log("Model ResponseText: " + model.responseText);
            //            console.log($.parseJSON(model.responseText))
            var string = true;
            try {
                $.parseJSON(model.responseText);
            } catch (e) {
                string = false;
            }
            if (model.responseText && string && $.parseJSON(model.responseText).message == "Invalid token, Please try login again.") {
                localStorage.removeItem("token");
                window.location.href = "#";
            } else if ((model.responseText != null) && string && (model.responseText != undefined) && (model.responseText != "") && $.parseJSON(model.responseText).message != "Record already exists. Please verify the entered record.")
                showError($.parseJSON(model.responseText).message);
            else
                console.log("Error serviceurl " + serviceurl);
            console.log("Error voname " + voname);
            console.log("Error input " + JSON.stringify(input));
            // showError("Something went wrong! Please try after some time");
            //Since all web service pass through this function error callback parametrs are added here (response,errorReport)
            callback(null, model, response, errorReport);
        }
    });
}

/* Get Server Data through MicroApp Service */
function fnGetMicroAppServerData(serviceurl, input, callback, keepCall) {
    //console.log("Calling fnGetWebServerData with the following data <br> " + JSON.stringify(input));
  
    //passing user's company info with all the web service calls
    input.strCompId = localStorage.getItem("cmpid");
    input.cmpdateFmt = localStorage.getItem("cmpdfmt");
    input.strPlantId = localStorage.getItem("plantid");
    input.strDeptId = localStorage.getItem("deptid");
    input.strUserId = localStorage.getItem("userID");
    input.strAccLvl = localStorage.getItem("AcID");
    $.support.cors = true;
    $.ajax({
        beforeSend: function (jqXHR) {
            if (keepCall == undefined)
                $.xhrPool.push(jqXHR);
        },
        async: true,
        url: URL_MicroAppServer + serviceurl,
        method: "POST",
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(input),
        success: function (data) {
                callback(data);
        },
        error: function (model, response, errorReport) {
            console.log("Model ResponseText: " + model.responseText);
            //            console.log($.parseJSON(model.responseText))
            var string = true;
            try {
                $.parseJSON(model.responseText);
            } catch (e) {
                string = false;
            }
            if (model.responseText && string && $.parseJSON(model.responseText).message == "Invalid token, Please try login again.") {
                localStorage.removeItem("token");
                window.location.href = "#";
            } else if ((model.responseText != null) && string && (model.responseText != undefined) && (model.responseText != "") && $.parseJSON(model.responseText).message != "Record already exists. Please verify the entered record.")
                showError($.parseJSON(model.responseText).message);
            else
                console.log("Error serviceurl " + serviceurl);
            console.log("Error input " + JSON.stringify(input));
            // showError("Something went wrong! Please try after some time");
            //Since all web service pass through this function error callback parametrs are added here (response,errorReport)
            callback(null, model, response, errorReport);
        }
    });
}


// Localization for Labels

function getLocaleData(callback) {
    var currLang = localStorage.getItem('deviceLng');
    var userLanguage = "";

    if (parseNull(currLang) == "JP") {
        if (GM.Global) {
            GM.Global.LANG = "jp";
        }
        localStorage.setItem("lang", "jp");
        minKeyinChar = 2;
    }else if (parseNull(currLang) == "DE") {
        if (GM.Global) {
            GM.Global.LANG = "de";
        }
        localStorage.setItem("lang", "de");
        minKeyinChar = 3;
    }else {
        if (GM.Global) {
            GM.Global.LANG = "en";
        }
        localStorage.setItem("lang", "en");
        minKeyinChar = 3;
    }

    if (GM.Global.LANG == undefined || GM.Global.LANG == null || GM.Global.LANG == "") {
        userLanguage = localStorage.getItem("lang");
    } else {
        userLanguage = GM.Global.LANG;
    }

    $.get("globusMobileApp/datafiles/locales/" + userLanguage + ".json", function (data) {
        if (typeof (data) != "object")
            data = $.parseJSON(data);
        GM.Global.Locale = data;
        localStorage.setItem("localedata", JSON.stringify(data));

        if (callback != undefined)
            callback(GM.Global.Locale);
    });
    
}

//To get Label Name related on user
function lblNm(label) {
    var localeData = "";
        if(GM.Global.Locale == undefined  || GM.Global.Locale == null || GM.Global.Locale == ""   ){
                localeData = $.parseJSON(localStorage.getItem("localedata"));
            }else{
                localeData = GM.Global.Locale;
            }
    var labVal = _.pluck(localeData, label);
    console.log(typeof(labVal));
    return labVal[0];
}



// CRM functions
function onrefresh() {
    var that = this;
    $(".mobile-menu-top-user-name").html(localStorage.getItem("fName") + " " + localStorage.getItem("lName"))
    var token = localStorage.getItem("token");


    $("#iPad-fname").html(localStorage.getItem("fName"));
    $("#iPad-lname").html(localStorage.getItem("lName"));
    $(".ul-language-name").addClass("hide");
    if (token != null) {
        
    $("#ul-header-right-pic").removeClass("hide");
         $("#ul-header-right-pic-menubar").removeClass("hide");
        var partyID = window.localStorage.getItem("partyID");
        var input = { "token": localStorage.getItem("token"), "refid": partyID, "deletefl": "", "refgroup": "103112" };
        if (GM.Global.Device) {
            fnGetCRMDocumentDetail(partyID, 103112, function (fileData) {
                if (fileData.length) {
                    fileData = fileData[0];
                    GM.Global.UserPicID = fileData.fileid;
                    $("#ul-header-right-pic").find("img").removeAttr("onerror").css("display", "inline-block");
                    $("#ul-header-right-pic-menubar").find("img").removeAttr("onerror").css("display", "inline-block");
                    $("#mobile-menu-top-user").find("img").removeAttr("onerror").css("display", "inline-block");
                    if (GM.Global.Browser) {
                        $("#ul-header-right-pic").find("img").attr("src", URL_CRMFileServer + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());
                         $("#ul-header-right-pic-menubar").find("img").attr("src", URL_CRMFileServer + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());
                        $("#mobile-menu-top-user").find("img").attr("src", URL_CRMFileServer + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());
//                         GM.Global.UserPicID = URL_CRMFileServer + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random();
                    } else {
                        fnGetLocalFileSystemPath(function (localpath) {
                            $("#ul-header-right-pic").find("img").attr("src", localpath + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());
                            $("#ul-header-right-pic-menubar").find("img").attr("src", localpath + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());
                            $("#mobile-menu-top-user").find("img").attr("src", localpath + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());
//                            GM.Global.UserPicID = localpath + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random();
                        });
                    }


                }
                $("#ul-header-right-pic").find("img").attr("onerror", "this.style.display = 'none'");
                 $("#ul-header-right-pic-menubar").find("img").attr("onerror", "this.style.display = 'none'");
                $("#mobile-menu-top-user").find("img").attr("onerror", "this.style.display = 'none'");
            });
        } else {
            fnGetWebServerData("file/get", "gmCRMFileUploadVO", input, function (fileData) {
//                console.log(fileData)
            if (fileData != null) {
                fileData = arrayOf(fileData);
                        fileData = fileData[0];
                    GM.Global.UserPicID = fileData.fileid;
                    $("#ul-header-right-pic").find("img").removeAttr("onerror").css("display", "inline-block");
                $("#ul-header-right-pic-menubar").find("img").removeAttr("onerror").css("display", "inline-block");
                    $("#mobile-menu-top-user").find("img").removeAttr("onerror").css("display", "inline-block");
                    $("#ul-header-right-pic").find("img").attr("src", URL_CRMFileServer + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());
                $("#ul-header-right-pic-menubar").find("img").attr("src", URL_CRMFileServer + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());
                    $("#mobile-menu-top-user").find("img").attr("src", URL_CRMFileServer + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());
//                GM.Global.UserPicID = URL_CRMFileServer + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random();
                }
                $("#ul-header-right-pic").find("img").attr("onerror", "this.style.display = 'none'");
                $("#ul-header-right-pic-menubar").find("img").attr("onerror", "this.style.display = 'none'");
                $("#mobile-menu-top-user").find("img").attr("onerror", "this.style.display = 'none'");
            }, true);
        }

    }
}


function openPicturePopup(parent, ImgCrpView) {
    if (!GM.Global.Device) {
        console.log(parent);
        var userPartyId = window.localStorage.getItem("partyID");
        var that = parent;
        if (userPartyId == 0)
            showNativeAlert("Please Save the Surgeon Details to Upload a Profile Pic", "Save Profile");
        else {
            showPopup();
            $("#div-crm-overlay-content-container").find('.modal-footer').addClass("hide");

            $("#div-crm-overlay-content-container").find('.modal-title').html("Edit Profile Picture");

            var template_picture = fnGetTemplate(URL_Common_Template, "gmtUserProfilePic");
            $("#div-crm-overlay-content-container").find('.modal-body').html(template_picture());
            if ($(window).width() <= 480) {
                var userurl = $("#mobile-menu-top-user img").attr('src');
                var menuuserurl = $("#mobile-menu-top-user img").attr('src');
            } else {
                var userurl = $("#ul-header-right-pic img").attr('src');
                var menuuserurl = $("#ul-header-right-pic-menubar img").attr('src');
            }
            $("#div-user-pic").append("<img class = 'image-user-pic'>");
            $(".image-user-pic").attr("src", userurl);
            $("#div-crm-overlay-content-container").find('.modal-body #input-user-picture').unbind('change').bind('change', function (e) {
                var file = e.target.files[0];
                var fileDisplayArea = document.getElementById('div-user-pic');
                var imageType = /image.*/;
                if (file.type.match(imageType)) {
                    var ori = 0;
                    loadOImage.parseMetaData(file, function (data) {
                        if (data.exif)
                            ori = data.exif.get('Orientation');
                        var loadingImage = loadOImage(
                            file,
                            function (img) {
                                console.log(img)
                                var cropdata = { //declaring crop data and calling crop view 
                                    'parent': that,
                                    'src': img.toDataURL(),
                                    'el': fileDisplayArea,
                                    'callback': function (val) {
                                        getCrop(val, parent);
                                    }
                                }
                                var imagecrop = new ImgCrpView(cropdata);
                            }, {
                                minWidth: 100,
                                maxWidth: 300,
                                canvas: true,
                                orientation: ori
                            }
                        );
                        if (!loadingImage) {
                            fileDisplayArea.innerHTML = "File not supported!"
                            that.picData = "";
                        }
                    });
                } else {
                    fileDisplayArea.innerHTML = "File not supported!"
                    that.picData = "";
                }

            });

            $("#div-crm-overlay-content-container").find('.modal-body #input-user-picture').unbind('click').bind('click', function (e) {
                //to reload original image on rerendering the same pic
                if (e.target.files[0] != "" || e.target.files[0] != undefined)
                    $('#input-user-picture').val("");
            });
        }
    } else
        showError("Network Connection is Required");

}

function getCrop(data, parent) {
    var that = parent;
    var fileDisplayArea = document.getElementById('div-user-pic');
    fileDisplayArea.innerHTML = "";
    var img = new Image();
    img.src = data;
    img.className = "img-user-pic";
    fileDisplayArea.appendChild(img);
    $("#div-user-pic").append("<div id='div-crop-ok' class='div-btn-crop gmText100 gmBGphysicianvisit gmBtn gmFontWhite gmTextCenter gmText150'> Ok</div>");
    var dataURI = data;
    var binary = atob(dataURI.split(',')[1]);
    var array = [];
    for (var i = 0; i < binary.length; i++)
        array.push(binary.charCodeAt(i));
    var picture = new Blob([new Uint8Array(array)], {
        type: 'image/jpeg'
    });
    updatePic(picture);
}

function updatePic(picfile) {
    var that = this;
    var $form = $("#frm-user-picture");
    var params = $form.serializeArray();
    this.picData = new FormData();
    this.picData.append('file', picfile, window.localStorage.getItem("partyID") + "-ProfilePic.jpeg");
    $.each(params, function (i, val) {
        if (val.name != "file")
            that.picData.append(val.name, val.value);
    });
    console.log(JSON.stringify(that.picData))
    console.log($("#div-crop-ok").length)
    $("#div-crm-overlay-content-container #div-crop-ok").unbind('click').bind('click', function () {
        if (that.picData != "" && that.picData != undefined) {
            that.picData.append('refid', window.localStorage.getItem("partyID"));
            that.picData.append('refgrp', '103112');
            that.picData.append('token', localStorage.getItem('token'));
            that.picData.append('fileid', '');
            that.picData.append('reftype', '91182');
            that.picData.append('filetitle', 'User Profile Picture');
            console.log(this.picData)

            $.ajax({
                url: URL_WSServer + 'uploadedFile/crmfileupload',
                data: that.picData,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function (result) {
                    console.log("Pic Uploaded >>> " + JSON.stringify(result));
                    var token = localStorage.getItem("token");
                    var input = {
                        "token": token,
                        "refid": window.localStorage.getItem("partyID"),
                        "filename": window.localStorage.getItem("partyID") + "-ProfilePic.jpeg",
                        "deletefl": "",
                        "refgroup": "103112",
                        "filetitle": "User Profile Picture - " + window.localStorage.getItem("userName")
                    };
                    console.log(GM.Global.UserPicID)
                    if (GM.Global.UserPicID)
                        input['fileid'] = GM.Global.UserPicID;
                    console.log(GM.Global.UserPicID);
                    console.log(input);
                    fnGetWebServerData("file/savefile", undefined, input, function (fileData) {
                        if (fileData != null) {
                            GM.Global.UserPicID = fileData.fileid;
                            $("#ul-header-right-pic").find("img").removeAttr("onerror").css("display", "inline-block");
                            $("#ul-header-right-pic-menubar").find("img").removeAttr("onerror").css("display", "inline-block");
                            $("#mobile-menu-top-user").find("img").removeAttr("onerror").css("display", "inline-block");
                            $("#ul-header-right-pic").find("img").attr("src", URL_CRMFileServer + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());
                            $("#ul-header-right-pic-menubar").find("img").attr("src", URL_CRMFileServer + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());
                            $("#mobile-menu-top-user").find("img").attr("src", URL_CRMFileServer + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());
                        }

                        $("#ul-header-right-pic").find("img").attr("onerror", "this.style.display = 'none'");
                        $("#ul-header-right-pic-menubar").find("img").attr("onerror", "this.style.display = 'none'");
                        $("#mobile-menu-top-user").find("img").attr("onerror", "this.style.display = 'none'");
                    });
                },
                error: function (model, response, errorReport) {
                    console.log(model);
                    console.log("Pic Upload Failed >>> " + JSON.stringify(model));
                }
            });
        }

        hidePopup();

    });
}

function initiateHelpers() {
    Handlebars.registerHelper("math", function (lvalue, operator, rvalue, options) {
        lvalue = parseFloat(lvalue);
        rvalue = parseFloat(rvalue);

        return {
            "+": lvalue + rvalue,
            "-": lvalue - rvalue,
            "*": lvalue * rvalue,
            "/": lvalue / rvalue,
            "%": lvalue % rvalue
        }[operator];
    });
    Handlebars.registerHelper('ifEquals', function (arg1, arg2, options) {
        return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
    });
    Handlebars.registerHelper('iftrue', function (v1, v2, options) {
        var inverse = options.inverse,
            fn = options.fn;

        if (v1 === v2) {
            return fn(this);
        }
        return inverse(this);
    });
    Handlebars.registerHelper('ifanytrue', function (v1, v2, options) {
        var inverse = options.inverse,
            fn = options.fn;

        if (_.contains(v2.split('/'), v1)) {
            return fn(this);
        }
        return inverse(this);
    });

    Handlebars.registerHelper('iffalse', function (v1, v2, options) {
        var inverse = options.inverse,
            fn = options.fn;

        if (v1 !== v2) {
            return fn(this);
        }
        return inverse(this);
    });
    

    // For Localization--Dynamic Label Changes
    
        Handlebars.registerHelper("lblNm", function (label) {
        
            return lblNm(label);
    });


    Handlebars.registerHelper('ifgreater', function (v1, v2, options) {
        var inverse = options.inverse,
            fn = options.fn;

        if (v1 > v2) {
            return fn(this);
        }
        return inverse(this);
    });
    
    Handlebars.registerHelper('iflesser', function (v1, v2, options) {
        var inverse = options.inverse,
            fn = options.fn;

 

        if (v1 < v2) {
            return fn(this);
        }
        return inverse(this);
    });
    Handlebars.registerHelper('removeTag', function (el) {
        el = parseNull(el);
        if(el.indexOf(">") == -1)
            var resultEl=el;
        else
            var resultEl = el.substring(el.indexOf(">")+1,el.lastIndexOf("<"));
        
        return resultEl;
    });
    
    Handlebars.registerHelper("formatDate", function (dt) {
        if (dt != "" && dt != "null" && dt != undefined) {
            var dateArray = dt.split("-");
            //        console.log("dateArray"+dateArray);
            var year = dateArray[0];
            var month = dateArray[1];
            var date = dateArray[2];

            var fmtDate = month + "/" + date + "/" + year;
            if (month != undefined && date != undefined)
                return fmtDate;
            else
            if (dt == "aN/aN/NaN")
                return "";
            else
                return dateArray;
        }
    });

    Handlebars.registerHelper("formatPhoneNumber", function (phoneNumber) {
        phoneNumber = phoneNumber.toString();
        if (phoneNumber.indexOf("-") >= 0) {
            return phoneNumber;
        } else {
            phoneNumber = phoneNumber.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, "$1-$2-$3");
            return phoneNumber;
        }

        //        return "(" + phoneNumber.substr(0,3) + ") " + phoneNumber.substr(3,3) + "-" + phoneNumber.substr(6,4);
    });

    Handlebars.registerHelper("AMPM", function (time) {
        if (time && time != "") {
            if (GM.Global.Device && time.toString().indexOf("-") >= 0)
                time = time.split(" ")[1];
            var hours = time.split(":")[0];
            var minutes = time.split(":")[1];
            var suffix = hours >= 12 ? "PM" : "AM";
            hours = hours % 12 || 12;
            hours = hours < 10 ? "0" + hours : hours;

            var displayTime = hours + ":" + minutes + " " + suffix;
            return displayTime;
        }
    });

    //Price Format for Pricing Tool
    Handlebars.registerHelper("formatPrice", function (price) {

        price = price.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        return price;
    });

    //Company Date Format
    Handlebars.registerHelper("companyDateFormat", function (dt) {
        return formattedDate(dt, localStorage.getItem("cmpdfmt"));
    });

    // Function for return the Empty value if the value passed as null/undefined/empty.
    Handlebars.registerHelper("parseNull", function (value) {
        return parseNull(value);
    }); 
    
    // Function for return the Object length if the Object is Passed.
    Handlebars.registerHelper("objLength", function (obj) {
        return Object.keys(obj).length;
    });
    
    /* 
     ifavailable() -To compare the passing string with the GM.Global.PRSYSDIVISION global varaible for pricing module
     Author: Karthik
     */
     Handlebars.registerHelper('ifavailable', function (v1,v2, options) {
            var inverse = options.inverse;
            var fn = options.fn;
            var sysIndex = GM.Global.PRSYSDIVISION.indexOf(v1);
            var sysFlag= "";
            if (sysIndex < 0  ){
                sysFlag = "N";
            }else{
                sysFlag = "Y";
            }
            if (sysFlag === v2) {
                return fn(this);
            }
            return inverse(this);
        });
    
}

/* fomattedDate() -To get the date format based on the company */
/*function formattedDate(dt,dateformat) {
	if (dt != "" && dt != null && dt != undefined
	    && dateformat != "" && dateformat != null && dateformat != undefined) {
		try{
			 var dateArray = dt.split("/");
			 var month = dateArray[0];
			 var date = dateArray[1];
			 var year = dateArray[2];  
    		 if(month != "" && month != null && month != undefined &&
					   date != "" && date != null && date != undefined &&
					   year != "" && year != null && year != undefined ){
						 if(dateformat == "dd/MM/yyyy"){
							 dt = date + "/" + month + "/" + year;
						 }else if(dateformat == "dd.MM.yyyy"){
							  dt = date + "." + month + "." + year;
						 }
			 }
				
		   }catch(err) {
			}
        }
      return dt;
}*/

/* fomattedDate() -Common method to get the date format based on the company 
 */
function formattedDate(dt, dateformat) {
    try {
        if (dt != "" && dt != null && dt != undefined &&
            dateformat != "" && dateformat != null && dateformat != undefined) {
            if (dateformat != "yyddMM" && dateformat != "yyMMdd") {
                $.datepicker.parseDate('mm/dd/yy', dt);
            }
            var resultedDate = $.format.date(new Date(dt), dateformat);
            if (resultedDate != "" || resultedDate != null || resultedDate != undefined || resultedDate != "NaN/NaN/NaN") {
                dt = resultedDate;
            }
        }
    } catch (err) {

    }
    return dt;
}

function formatPrice(price) {
    price = price.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    return price;
}

function getCompDateFmt(dt, callback) {
    callback(formattedDate(dt, localStorage.getItem("cmpdfmt")));
}

function getDateByFormat(dt) {
    if (dt != "" && dt != "null" && dt != undefined) {
        if (dt.indexOf("-") >= 0)
            var dateArray = dt.split("-");
        else if (dt.indexOf("/") >= 0) {
            var dateArray = dt.split("/");
            if (GM.Global.Device) {
                dateArray = undefined;
                return dt;
            }
        } else if (dt.indexOf(",") >= 0)
            var dateArray = dt.split(",");
        //        console.log("dateArray"+dateArray);
        if (dateArray != undefined) {
            var year = dateArray[0];
            var month = dateArray[1];
            var date = dateArray[2];

            var fmtDate = month + "/" + date + "/" + year;
            if (month != undefined && date != undefined)
                return fmtDate;
            else
                return dateArray;
        }
    }
}

function convertTime(data) {
    var time = data.toString();
    var lastIndex = time.lastIndexOf(" ");
    time = time.substring(0, lastIndex);
    var d = new Date(time);
    console.log(d);
    var ndate = d.getDate();
    var nmonth = d.getMonth();
    var nyear = d.getFullYear();
    var hh = d.getHours();
    var m = d.getMinutes();
    var s = d.getSeconds();
    var dd = "AM";
    var h = hh;
    if (h >= 12) {
        h = hh - 12;
        dd = "PM";
    }
    if (h == 0) {
        h = 12;
    }
    h = h < 10 ? "0" + h : h;
    m = m < 10 ? "0" + m : m;
    s = s < 10 ? "0" + s : s;
    var pattern = new RegExp("0?" + hh + ":" + m + ":" + s);
    var replacement = ndate + "-" + nmonth + "-" + nyear + " " + h + ":" + m;
    //    replacement += " "+s;
    //    replacement += " "+dd;
    console.log(replacement);
    return replacement;

    //    var ampm = (d.getHours() >= 12) ? "PM" : "AM";
    //    var hours = (d.getHours() >= 12) ? d.getHours()-12 : d.getHours();
    //
    //    return hours+' : '+d.getMinutes()+' '+ampm;

}

function ampm(time) {
    if (time && time != "" && time.toString().indexOf("AM") < 0 && time.toString().indexOf("PM") < 0) {
        if (GM.Global.Device && time.toString().indexOf("-") >= 0)
            time = time.split(" ")[1];
        var hours = time.split(":")[0];
        var minutes = time.split(":")[1];
        var suffix = hours >= 12 ? "PM" : "AM";
        hours = hours % 12 || 12;
        hours = hours < 10 ? "0" + hours : hours;

        var displayTime = hours + ":" + minutes + " " + suffix;
        return displayTime;
    } else if (time.toString().indexOf("AM") >= 0 || time.toString().indexOf("PM") >= 0)
        return time;
    else
        return "";

}

function fmtTime(time) {
    if (time && time != "") {
        if (GM.Global.Device && time.toString().indexOf("-") >= 0)
            time = time.split(" ")[1];
        var hours = time.split(":")[0];
        var minutes = time.split(":")[1];
        hours = hours % 12 || 12;
        hours = hours < 10 ? "0" + hours : hours;

        var displayTime = hours + ":" + minutes;
        return displayTime;
    } else
        return "";

}

function dateToTime(data) {
    console.log("dateTOTime" + data)
    var date = data.toString();
    var lastIndex = date.lastIndexOf(" ");
    date = date.substring(0, lastIndex);
    var time = new Date(date);
    var hours = time.getHours();
    hours = ("0" + hours).slice(-2);
    var minutes = time.getMinutes();
    minutes = ("0" + minutes).slice(-2);
    var fmtTime = hours + ":" + minutes;

    if (fmtTime && fmtTime != "aN:aN")
        return fmtTime;
    else
        return "";
}

//To hide/show mobile menu bar
$(document).on("click", function (event) {
    if (event.target.id == "button-mobile-menu")
        $("#div-crm-tab").addClass("show-panel");
    else
        $("#div-crm-tab").removeClass("show-panel");
});

function getAllowedTasks(userid) {
    if (GM.Global.UserAccess != undefined) {

    }
}
// To show popup box
function showPopup() {
    $('.div-overlay').show();
    $('.div-crm-overlay-content-container').addClass('hide');
    $('#div-crm-overlay-content-container').removeClass('hide');
    $(".modal-footer").removeClass("hide");

    $('.div-crm-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hidePopup();
        });
    });
}

// To hide popup box
function hidePopup() {
    $('.div-overlay').hide();
    $('.div-crm-overlay-content-container').addClass('hide');
    $(".hideDefaultBtn").addClass("hide");
}

// Create select options box
function fnCreateOptions(select, arrData) {
    arrData = arrayOf(arrData);
    for (var i = 0; i < arrData.length; i++) {
        var option = document.createElement('option');
        option.value = arrData[i].ID;
        option.innerHTML = arrData[i].Name;
        $(select).append(option);
    }
}

// Get surgeon detail info
function getSurgeonPartyInfo(npid, callback) {
    var input = {
        "token": localStorage.getItem("token"),
        "npid": npid
    };
    var surgeonData;
    fnGetWebServerData("search/npid", "gmPartyBasicVO", input, function (data) {
        callback(data);
    });
}

// variable for success/error message
var op = 1,
    toErr;
var aniErr = setInterval(function () {
    if (op == 1) {
        op = 0.75;
    } else {
        op = 1;
    }
    $('.alert').animate({
        opacity: "" + op + ""
    }, "slow");
}, 1000);

// show failure message
function showError(errorMsg, hideAfter) {
    displayMessage(errorMsg, 'alertError', hideAfter);
    $("#fa-close-alert").removeClass("hide");
}

// show messages
function showSuccess(successMsg, hideAfter) {
    displayMessage(successMsg, 'alertSuccess', hideAfter);
    $("#fa-close-alert").removeClass("hide");
}

// show success message
function showMessages(alertMsg, hideAfter) {
    displayMessage(alertMsg, 'alertMessages', hideAfter);
    $("#fa-close-alert").removeClass("hide");
}

// Display Message from the page
function displayMessage(Message, Type, hideAfter) {
    window.clearTimeout(toErr);
    $("#fa-send-report").addClass("hide");
    $('.alert').removeClass().addClass('alert ' + Type).html(Message.replace(/\n/g, '<br>'));
    toErr = setTimeout(function () {
        hideMessages();
    }, 8000);

}

// show App Error message
function showAppError(errorMsg, traceInfo, hideAfter) {
    displayAppErrors(traceInfo + ":<br> " + errorMsg.message, 'alertError', hideAfter);
    $("#fa-send-report").removeClass("hide");
    $("#fa-close-alert").removeClass("hide");
}

// Display Message from the page
function displayAppErrors(Message, Type, hideAfter) {
    window.clearTimeout(toErr);
    var d = new Date();
    var timeNow = d.toDateString() + " " + d.toLocaleTimeString();
    errArrLog.push_arr_ltd("<br>" + timeNow + " : " + Message.replace(/\n/g, '<br>'));
    $('.alert').removeClass().addClass('alert ' + Type).html(lblNm("msg_error_occured")+"<div id='error-report-content' class='hide'>" + errArrLog + "</div><br>");
    toErr = setTimeout(function () {
        hideMessages();
    }, 1000000);

}

// Hide Message from the page
function hideMessages() {
    //    window.clearTimeout(toErr);
    //    var timeOutTime = 0, animateTime = 100;
    //    if($('.alert').hasClass('alertSuccess')) {
    //        timeOutTime = 1000;
    //        animateTime = 1000;
    //    } 
    //        
    //    toErr = setTimeout(function() {
    if ($("#fa-send-report").is(":visible")) {
        errArrLog = [];
    }
    $('.alert').slideUp('fast', function () {
        $(this).css("display", "block").addClass('hide');
        $("#fa-close-alert").addClass("hide");
        $("#fa-send-report").addClass("hide");
    })
    //    }, timeOutTime)  
    //     

}

// Check the user is salesrep or not
function isRep() {

    var acid, deptid = "";

    var result = false;

    if (localStorage.getItem("acid"))
        acid = localStorage.getItem("acid");

    if (localStorage.getItem("deptid"))
        deptid = localStorage.getItem("deptid");

    if (deptid.toUpperCase() == "S") {
        switch (acid) {
            case "1":
            case "2":
                console.log("It is a Rep");
                result = true;
                break;
            default:
                console.log("It is NOT a Rep because acid is " + acid);
                result = false;
        }
    } else {
        console.log("It is NOT a Rep because deptid is " + deptid);
        result = false;
    }

    return result;
}
// Check the user is AD, VP or not
function isADVP() {
    var acid, deptid = "";
    var result = false;
    if (localStorage.getItem("acid"))
        acid = localStorage.getItem("acid");
    if (localStorage.getItem("Dept"))
        Dept = localStorage.getItem("Dept");
    if (Dept == "2005") {
        switch (acid) {
            case "3": // AD Filter Condition
            case "4": // VP Filter Condition
            case "6": // other AD's like Kirk Tovey want to see other region
                result = true;
                break;
            default:
                result = false;
        }
    } else {
        result = false;
    }
    return result;
}

//Sets the right tab with focal look
function colorControl(e, groupclass, commonclass, changeclass) {
    var ctrl = e.currentTarget;

    if ($("." + groupclass).hasClass(commonclass))
        $("." + groupclass).removeClass(commonclass);

    $("." + groupclass).addClass(commonclass).removeClass(changeclass);
    $(ctrl).removeClass(commonclass).addClass(changeclass);
}

// Module level inside the modules
function fnGetAccessInfo(moduleID) {
    var userAccessInfo = $.parseJSON(localStorage.getItem('userTabs'));
    return _.findWhere(userAccessInfo, {
        "functionID": "CRM-" + moduleID
    });
}

function mailDetection(containerSubID) {
    $("#txt-popup-mail-" + containerSubID).bind("keyup", function (e) {
        var searchDataSpace = e.keyCode;
        var strSearchData = $(e.currentTarget).val();
        var intWordLength = strSearchData.length;
        var strSearchLastWord = strSearchData.slice(-1);

        if (searchDataSpace == 32 || searchDataSpace == 13 || strSearchLastWord == ',' || strSearchLastWord == ';' || strSearchLastWord == ' ') {
            console.log(strSearchData);
            strSearchData = strSearchData.replace(',', '').replace(';', '').replace(' ', '');
            var strEmailID = strSearchData;
            var atpos = strEmailID.indexOf("@");
            var dotpos = strEmailID.lastIndexOf(".");

            if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= strEmailID.length) {
                console.log("Not a valid e-mail address");
            } else {
                var parentDiv = $(document.createElement('div')).attr({
                    'email': strEmailID,
                    'class': 'div-parent-div div-parent-div-' + containerSubID + ' gmBtn gmAlignHorizontal gmMargin2p'
                });
                var parentDivSpan = $(document.createElement('span')).attr('class', 'spn-parent-span');
                var spanDivText = $(document.createElement('div')).attr('class', 'div-child-text gmAlignHorizontal');
                var spanDivClose = $(document.createElement('div')).attr('class', 'spn-spandiv-close fa fa-times-circle fa-lg gmAlignHorizontal gmMarginLeft5p');
                $(spanDivText).text(strEmailID);
                $("#li-popup-mail-" + containerSubID + "-container").append(parentDiv);
                $(parentDiv).append(parentDivSpan);
                $(parentDivSpan).append(spanDivText);
                $(parentDivSpan).append(spanDivClose);
                $(e.currentTarget).val('');
                $("#div-crm-overlay-content-container2 .modal-body .spn-spandiv-close").unbind("click").bind("click", function (event) {
                    event.stopPropagation();
                    $(event.currentTarget).parent().parent().remove();
                });
                $("#div-crm-overlay-content-container2 .div-parent-div-" + containerSubID).unbind("click").bind("click", function (event) {
                    $("#txt-popup-mail-" + containerSubID).val($(event.currentTarget).text().trim());
                    $(event.currentTarget).remove();
                });
            }
        }
    });
}

function commonSetUp() {
    GM.Global.UserData = localStorage.getItem("userData");
    GM.Global.SalesRep = isRep();
    GM.Global.UserName = window.localStorage.getItem("fName") + " " + window.localStorage.getItem("lName");
    GM.Global.UserTabs = $.parseJSON(localStorage.getItem("userTabs"));
    if (GM.Global.Device) {
        if ($(window).width() <= 480) {
                                   $('.CRMoffline').addClass('hide');
                                }
                                else{
                                   $(".CRMoffline").removeClass("hide"); 
                                }
        $(".CRMonline").addClass("hide");
    } else {
        $(".CRMoffline").addClass("hide");
        $(".CRMonline").removeClass("hide");
    }
}

/* Get Local Data through AJAX */
function fnGetLocalData(path, callback) {

    $.ajax({
        url: "datafiles/conf.json",
        method: "GET",
        datatype: "application/json",
        success: function (data) {
            console.log(JSON.stringify(data));
        },
        error: function () {
            console.log("Error");
        }
    });
}

// To show popup box
function showAddressPopup() {
    $('.div-overlay').show();
    $('.div-crm-overlay-content-container').addClass('hide');
    $('#div-crm-overlay-content-container3').removeClass('hide');
    $(".modal-footer").removeClass("hide");

    $('.div-crm-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hideAddressPopup();
        });
    });
}

function showRepInfoPopup() {
    $('.div-overlay').show();
    $('.div-crm-overlay-content-container').addClass('hide');
    $('#div-crm-overlay-content-container4').removeClass('hide');


    $('.div-crm-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hideRepInfoPopup();
        });
    });
}

function hideRepInfoPopup() {
    $('.div-overlay').hide();
    $('.div-crm-overlay-content-container').addClass('hide');
}
// To hide popup box
function hideAddressPopup() {
    $('.div-overlay').hide();
    $('.div-crm-overlay-content-container').addClass('hide');
}


function getSelectState() {
    var that = this;
    $.get("globusMobileApp/datafiles/state.json", function (data) {
        console.log(data)
        if (typeof (data) != "object")
            data = $.parseJSON(data);
        var arrSelectOpt = new Array();
        arrSelectOpt.push({
            "ID": "",
            "Name": "Select"
        });
        data = arrSelectOpt.concat(_.sortBy(data, function (data) {
            return data.codenm;
        }));
        var strSelectOptions = JSON.stringify(data).replace(/codeid/g, 'ID').replace(/codenm/g, 'Name');
        that.statenm = $.parseJSON(strSelectOptions);
        $('.select-address-state').each(function () {
            fnCreateOptions(this, that.statenm);
            $(this).val($(this).attr("value"));
        });

    });


}

function getSelectCountry() {
    var that = this;
    $.get("globusMobileApp/datafiles/country.json", function (data) {
        if (typeof (data) != "object")
            data = $.parseJSON(data);
        var arrSelectOpt = new Array();
        arrSelectOpt.push({
            "ID": "",
            "Name": "Select"
        });
        //   GM.Global.Country = arrSelectOpt.concat(GM.Global.Country);
        data = arrSelectOpt.concat(_.sortBy(data, function (data) {
            return data.codenm;
        }));
        var strSelectOptions = JSON.stringify(data).replace(/codeid/g, 'ID').replace(/codenm/g, 'Name');
        that.countrynm = $.parseJSON(strSelectOptions);
        $('.select-address-country').each(function () {
            fnCreateOptions(this, that.countrynm);
            $(this).val($(this).attr("value"));
        });
    });

}

function resizeFont() {
    $('.li-index-number').each(function (i, box) {
        var width = $(box).width(),
            html = '<span style="white-space:nowrap">',
            line = $(box).wrapInner(html).children()[0],
            n = 50;
        $(box).css('font-size', n);
        while ($(line).width() > width) {
            $(box).css('font-size', --n);
        }
        $(box).text($(line).text());
    });
}

function resizeHeadWidth(parentClass, rchild, lchild) {
    var fWidth = $(parentClass).width();
    var rWidth = $(rchild).width();
    var newlWidth = fWidth - rWidth - 2;
    $(lchild).css({
        "width": newlWidth + "px"
    });
    $(lchild).addClass("gmEllipses");
}


function createEvent(data) {
    console.log("createEvent >> " + JSON.stringify(data));
    if (!GM.Global.Browser) { // Only Devices!
        console.log("Check Device >> " + GM.Global.Browser);
        var startDate = new Date(data.actstartdate + " " + data.actstarttime);
        var endDate = new Date(data.actenddate + " " + data.actendtime);
        var title = data.actnm;

        if (data.arrgmcrmaddressvo != "") {
            var address = arrayOf(data.arrgmcrmaddressvo);
            var eventLocation = address[0].add1 + " " + address[0].add2 + " " + address[0].city + " " + address[0].statenm + " " + address[0].zip;
        } else
            var eventLocation = "Unavailable";

        data.arrgmactivityattrvo = arrayOf(data.arrgmactivityattrvo);
        if (data.arrgmactivityattrvo != "") {
            var products = _.filter(data.arrgmactivityattrvo, function (data) {
                return data.attrtype == "105582";
            });
            var dataResult = [];
            _.each(products, function (data) {
                dataResult.push(data.attrnm);
            });
            var notes = dataResult.toString();
        } else
            var notes = "Unavailable";

        var success = function (message) {
            console.log("Success: " + JSON.stringify(message));
        };
        var error = function (message) {
            console.log("Error: " + message);
        };
        if (data.voidfl == "") {
            if (data.actstatus == "105762")
                window.plugins.calendar.deleteEvent(title, eventLocation, notes, startDate, endDate, success, error);
            else
                window.plugins.calendar.createEvent(title, eventLocation, notes, startDate, endDate, success, error);
        } else {
            window.plugins.calendar.deleteEvent(title, eventLocation, notes, startDate, endDate, success, error);
        }
    }
}

function getImageDoc(partyID, callback) {
    var token = localStorage.getItem("token");
    var partyID = partyID;

    if (GM.Global.Device) {
        fnGetLocalFileSystemPath(function (localpath) {
            console.log("fnGetLocalFileSystemPath localpath" + localpath);
            fnGetCRMDocumentDetail(userID, 103112, function (fileData) {
                console.log("fnGetCRMDocumentDetail fileData" + JSON.stringify(fileData));
                if (fileData.length) {
                    fileData = fileData[0];
                    console.log(localpath);
                    fnGetImage(localpath + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename, function (status) {
                        console.log(JSON.stringify(status))
                        if (status != "error") {
                            var URL = status.nativeURL + "?num=" + Math.random();
                            callback(URL);
                        } else
                            callback("");
                    });

                }
            });
        });

    } else {
        var input = {
            "token": token,
            "refid": partyID,
            "deletefl": "",
            "refgroup": "103112"
        };
        fnGetWebServerData("file/get", "gmCRMFileUploadVO", input, function (data) {
            console.log("PICDATA");
            console.log(data);
            if (data != "" && data != null) {
                if (data.length > 1)
                    var fileData = data[0];
                else
                    var fileData = data;
//                console.log(fileData)
                if (fileData != null) {
                    console.log($("#mobile-menu-top-user").length);
                    console.log($("#ul-header-right-pic").length);
                    console.log($("#ul-header-right-pic-menubar").length);
                    console.log($("#mobile-menu-top-user-name").length);
                    var URL = URL_CRMFileServer + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random();
                    callback(URL);
                }
            }

        });
    }

}

function dateOfnPastDays(countOfDays) {
    var d = new Date();
    d.setDate(d.getDate() - countOfDays);

    var dd = d.getDate();
    var mm = d.getMonth() + 1;
    var yyyy = d.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    d = mm + '/' + dd + '/' + yyyy;
    return d;
}

// Common Surgeon Info call from localtable
function surgeonInfo(partyID, callback, type) {
    GM.Global.SurgeonExist = undefined
    fnGetOfflineSurgeonSavedData(partyID, function (existData) {
        if (existData.length) {
            GM.Global.SurgeonExist = "Y";
            var data = $.parseJSON(existData[0].data);
            console.log("POPData")
            console.log(data)
            if (data.activefl != "N") {
                console.log(data.repid)
                if (data.repid == undefined) {
                    data.arrgmcrmsurgeonattrvo = arrayOf(data.arrgmcrmsurgeonattrvo);
                    var rep = _.filter(data.arrgmcrmsurgeonattrvo, function (t) {
                        return t.attrtype == '11509';
                    })
                    console.log("rep")
                    console.log(rep);
                    if (rep.length) {
                        fnGetADVPbyRepID(rep[0].attrvalue, function (d) {
                            console.log(d);
                            if (d.length) {
                                console.log("POPOPO")
                                console.log(d)
                                d = $.parseJSON(JSON.stringify(d[0]));
                                data.reppartyid = d.reppartyid;
                                data.repnm = d.repname;
                                data.adnm = d.adname;
                                data.vpnm = d.vpname;
                                data.adid = d.adid;
                                data.repid = d.repid;
                            }
                            callback(data);
                        });
                    } else
                        callback(data);
                } else
                    callback(data);
            } else if (type == "gmvSurgeon") {
                showError("Surgeon Voided");
                window.location.href = "#crm/surgeon";
            } else
                callback(null);
        } else {
            fnGetSurgeonDetail(partyID, function (data) {
                console.log(data);
                if (data.length) {
                    data = $.parseJSON(JSON.stringify(data));
                    fnGetSurgeonAttributeDetail(partyID, function (attrData) {
                        console.log(attrData);
                        fnGetSurgeonAddressDetail(partyID, function (addrData) {
                            console.log(addrData);
                            if (addrData.length)
                                addrData = $.parseJSON(JSON.stringify(addrData));
                            fnGetSurgeonContactDetail(partyID, function (conData) {
                                console.log(conData);
                                fnGetSurgeonRepDetail(partyID, function (d) {
                                    if (d.length) {
                                        console.log("POPOPO")
                                        console.log(d)
                                        d = $.parseJSON(JSON.stringify(d[0]));
                                        data[0].reppartyid = d.reppartyid;
                                        data[0].repnm = d.repnm;
                                        data[0].adnm = d.adnm;
                                        data[0].vpnm = d.vpnm;
                                        data[0].adid = d.adid;
                                        data[0].repid = d.repid;
                                    }
                                    data[0].arrgmcrmsurgeonattrvo = attrData;
                                    data[0].arrgmsurgeonsaddressvo = addrData;
                                    data[0].arrgmsurgeoncontactsvo = conData;
                                    callback(data[0]);
                                });
                            });
                        });
                    });
                } else if (type == "gmvSurgeon") {
                    GM.Global.AlertMsg = "No Such Surgeon";
                    window.location.href = "#crm/surgeon";
                } else
                    callback(null);
            });
        }
    });
}

// Common Activity Info call from localtable
function activityInfo(actid, callback, module, type) {
    fnGetOfflineActivitySavedData(actid, function (existData) {
        console.log(existData);
        if (existData.length) {
            var data = $.parseJSON(existData[0].data);
            if (data.voidfl != "Y") {
                if (actid.indexOf("act") >= 0) {
                    fnGetCodeNMByID(data.acttype, function (a) {
                        if (a.length)
                            data.acttypenm = a[0].codenm;
                        var ln = data.arrgmactivitypartyvo.length;
                        var roles = _.filter(data.arrgmactivitypartyvo, function (x) {
                            return x.roleid && x.roleid != "";
                        });
                        var rolecnt = roles.length;
                        var statuss = _.filter(data.arrgmactivitypartyvo, function (x) {
                            return x.statusid && x.statusid != "";
                        });
                        var statuscnt = statuss.length;
                        if (rolecnt) {
                            _.each(data.arrgmactivitypartyvo, function (x, i) {
                                fnGetCodeNMByID(x.roleid, function (b) {
                                    if (b.length) {
                                        x.rolenm = b[0].codenm;
                                    }
                                    fnGetCodeNMByID(x.statusid, function (c) {
                                        if (c.length) {
                                            x.statusnm = c[0].codenm;

                                        }
                                        if (ln == (i + 1))
                                            callback(data);
                                    })
                                })
                            });
                        } else if (statuscnt) {
                            _.each(data.arrgmactivitypartyvo, function (x, i) {
                                fnGetCodeNMByID(x.statusid, function (c) {
                                    if (c.length) {
                                        x.statusnm = c[0].codenm;
                                    }
                                    if (ln == (i + 1))
                                        callback(data);
                                })
                            });
                        } else
                            callback(data);

                    })

                } else
                    callback(data);

            } else if (type == "gmvActivity") {
                showError("Activity Voided");
                window.location.href = "#crm/" + module;
            }
        } else {
            fnGetActivityDetail(actid, function (data) {
                console.log(data)
                if (data.length) {
                    data = data[0];
                    data = $.parseJSON(JSON.stringify(data));
                    fnGetActivityAttrDetail(actid, function (attrData) {
                        console.log(attrData);
                        if (attrData.length)
                            data.arrgmactivityattrvo = attrData;
                        var addr = _.filter(attrData, function (chkaddr) {
                            return chkaddr.attrtype == "7777"
                        });
                        console.log(addr)
                        if (addr.length) {
                            _.each(attrData, function (aD) {
                                if (aD.attrtype == "7777") { // Address
                                    console.log(aD)
                                    fnGetActivityAddrDetail(aD.attrval, function (addrData) {
                                        console.log(addrData)
                                        if (addrData.length)
                                            data.arrgmcrmaddressvo = addrData[0];
                                        fnGetActivityPartyDetail(actid, function (partyData) {
                                            console.log(partyData);
                                            if (partyData.length)
                                                data.arrgmactivitypartyvo = partyData;
                                            callback(data);
                                        });
                                    });
                                }
                            });
                        } else {
                            fnGetActivityPartyDetail(actid, function (partyData) {
                                console.log(partyData);
                                if (partyData.length)
                                    data.arrgmactivitypartyvo = partyData;
                                callback(data);
                            });
                        }
                    });
                } else if (type == "gmvActivity") {
                    GM.Global.AlertMsg = "No Such Activity";
                    window.location.href = "#crm/" + module;
                }
            });
        }
    });
}

// returns the last value of a given array
function lastOf(a) {
    if (typeof (a) != "undefined" && $.type(a) == "array" && a.length)
        return a[(a.length - 1)];
    else
        return undefined;
}

// returns attributes for activity of given actcategory
function actAttr(data, actcategoryID) {
    console.log(actcategoryID)
    actcategoryID = actcategoryID ? actcategoryID.toString() : actcategoryID;
    switch (actcategoryID) {
        case '105269':
            data.module = "Lead";
            data.color = "lead";
            data.link = "lead";
            break;
        case '105270':
            data.module = "Tradeshow";
            data.color = "lead";
            data.link = "tradeshow";
            break;
        case '105266':
            data.module = "Physician Visit";
            data.color = "physicianvisit";
            data.link = "physicianvisit";
            break;
        case '105268':
            data.module = "Training";
            data.color = "training";
            data.link = "training";
            break;
        case '105265':
            data.module = "Merc";
            data.color = "merc";
            data.link = "merc";
            break;
        case '105267':
            data.module = "Sales Call";
            data.color = "salescall";
            data.link = "salescall";
            break;
        default:
            data = data;
    }

    return data;
}

/* Pending CRM Sync */
function fnCheckCRMPendingSync() {
    fnCheckSurgeonPendingSync();
//    fnCheckActivityPendingSync();
//    fnCheckCriteriaPendingSync();
}

function getOrientation(file, callback) {
    var reader = new FileReader();
    reader.onload = function (e) {
        var view = new DataView(e.target.result);
        if (view.getUint16(0, false) != 0xFFD8) return callback(-2);
        var length = view.byteLength,
            offset = 2;
        while (offset < length) {
            var marker = view.getUint16(offset, false);
            offset += 2;
            if (marker == 0xFFE1) {
                if (view.getUint32(offset += 2, false) != 0x45786966) return callback(-1);
                var little = view.getUint16(offset += 6, false) == 0x4949;
                offset += view.getUint32(offset + 4, little);
                var tags = view.getUint16(offset, little);
                offset += 2;
                for (var i = 0; i < tags; i++)
                    if (view.getUint16(offset + (i * 12), little) == 0x0112)
                        return callback(view.getUint16(offset + (i * 12) + 8, little));
            } else if ((marker & 0xFF00) != 0xFF00) break;
            else offset += view.getUint16(offset, false);
        }
        return callback(-1);
    };
    reader.readAsArrayBuffer(file);
}

function showInfoPopup() {
    $('.div-overlay').show();
    $('.div-price-overlay-content-container').addClass('hide');
    $('#div-price-overlay-content-container5').removeClass('hide');

    $('.div-price-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hideInfoPopup();
        });
    });
}

function hideInfoPopup() {
    $('.div-overlay').hide();
    $('.div-price-overlay-content-container').addClass('hide');
}


function showPriceindicatorPopup() {
    $('.div-overlay').show();
    $('.div-price-overlay-content-container').addClass('hide');
    $('#div-price-overlay-content-container14').removeClass('hide');


    $('.div-price-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hidePriceindicatorPopup();
        });
    });
}

function hidePriceindicatorPopup() {
    $('.div-overlay').hide();
    $('.div-price-overlay-content-container').addClass('hide');
}

function showAccountInfoPopup() {
    $('.div-overlay').show();
    $('.div-price-overlay-content-container').addClass('hide');
    $('#div-price-overlay-content-container6').removeClass('hide');


    $('.div-price-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hideAccountInfoPopup();
        });
    });
}

function showPartDetailsPopup() {
    $('.div-overlay').show();
    $('.div-price-overlay-content-container').addClass('hide');
    $('#div-price-overlay-content-container12').removeClass('hide');


    $('.div-price-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hidePartDetailsPopup();
        });
    });
}

function hidePartDetailsPopup() {
    $('.div-overlay').hide();
    $('.div-price-overlay-content-container').addClass('hide');
}

function hideAccountInfoPopup() {
    $('.div-overlay').hide();
    $('.div-price-overlay-content-container').addClass('hide');
}

function showFileUploadPopup() {
    $('.div-overlay').show();
    $('.div-price-overlay-content-container').addClass('hide');
    $('#div-price-overlay-content-container11').removeClass('hide');


    $('.div-price-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hideFileUploadPopup();
        });
    });
}

function hideFileUploadPopup() {
    $('.div-overlay').hide();
    $('.div-price-overlay-content-container11').addClass('hide');
}

function showLoaderPopup() {
    console.log("stop");
    $('.div-overlay').show();
    $('.div-price-overlay-content-container').addClass('hide');
    $('#div-price-overlay-content-container9').removeClass('hide');


    $('.div-price-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hideLoaderPopup();
        });
    });
}

function hideLoaderPopup() {
    $('.div-overlay').hide();
    $('.div-price-overlay-content-container').addClass('hide');
}

function showGroupPartInfoPopup() {
    $('.div-overlay').show();
    $('.div-price-overlay-content-container').addClass('hide');
    $('#div-price-overlay-content-container7').removeClass('hide');


    $('.div-price-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hideGroupPartInfoPopup();
        });
    });
}

function hideGroupPartInfoPopup() {
    $('.div-overlay').hide();
    $('.div-price-overlay-content-container').addClass('hide');
}

function fnGetIMSetsTest(url, txtSearch, callback) {
    var that = this;
    var token = localStorage.getItem("token");
    var searchText = $("#div-selected-system-list input").val();
    var input = {
        "token": token,
        "searchtext": searchText
    };
    fnGetWebServerData("pricingRequest/pricingSystems/", "gmSystemVO", input, function (data) {
        var results = [];
        if (data != null) {
            var data = arrayOf(data);
            $.each(data, function (i, val) {
                results.push({
                    "ID": val.systemId,
                    "Name": val.systemName
                });
            });
        }
        callback(results);

    });
}

function fnGetIMSystem(url, txtSearch, callback) {
    var that = this;
    var token = localStorage.getItem("token");
    var searchText = $("#div-acc-selected-system-list input").val();
    var input = {
        "token": token,
        "searchtext": searchText
    };
    fnGetWebServerData("pricingRequest/pricingSystems/", "gmSystemVO", input, function (data) {
        var results = [];
        if (data != null) {
            var data = arrayOf(data);
            $.each(data, function (i, val) {
                results.push({
                    "ID": val.systemId,
                    "Name": val.systemName
                });
            });
        }
        callback(results);
    });
}

function fnGetPricingAccounts(url, txtSearch, callback) {
    var that = this;
    var token = localStorage.getItem("token");
    var acid = localStorage.getItem("acid");
    var deptid = localStorage.getItem("deptid");
    var userID = localStorage.getItem("userID");
    var searchText = $("#div-rep-account-list input").val();
    var cmpid = localStorage.getItem("cmpid");
    var input = {
        "token": token,
        "typeid": typeIdVal,
        "acid": acid,
        "deptid": deptid,
        "userid": userID,
        "searchtext": searchText,
        "cmpid": localStorage.getItem("cmpid")
    };
    console.log(input);
    fnGetWebServerData("pricingRequest/accountDrpDownData/", "gmPriceAccountTypeVO", input, function (data) {
        var results = [];
        if (data != null) {
            var data = arrayOf(data);
            $.each(data, function (i, val) {
                if (typeIdVal == "903107")
                    results.push({
                        "ID": val.accid,
                        "Name": val.accname
                    });
                else
                    results.push({
                        "ID": val.partyid,
                        "Name": val.accname
                    });
            });
        } else
            data = [];
        callback(results);
    });
}

function fnGetCodeLookUpValues(input, callback) {
    fnGetWebServerData("common/codelookuplist/", "gmCodeLookupVO", input, function (data) {
        if (data != null) {
            var drpDwn = [];
            _.each(data, function (d, i) {
                drpDwn.push({
                    "ID": d.codeid,
                    "Name": d.codenm
                });
                if ((i + 1) == data.length)
                    callback(drpDwn);
            })
        } else
            callback([]);
    });
}

function showCancelPriceRequestPopup() {
    $('.div-overlay').show();
    $('.div-price-overlay-content-container').addClass('hide');
    $('#div-price-overlay-content-container8').removeClass('hide');


    $('.div-price-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hideAccountInfoPopup();
        });
    });
}

function hideCancelPriceRequestPopup() {
    $('.div-overlay').hide();
    $('.div-price-overlay-content-container').addClass('hide');
}

function getRepAddress(id, callback) {
    fnGetRepAddressByID(id, function (data) {
        GM.Global.defaultAddress = data;
        callback("success");
    });
}

function getAcctAddress(id, callback) {
    fnGetAcctAddressByID(id, function (data) {
        GM.Global.defaultAddress = data;
        callback("success");
    });
}

function getAcctAddressFromRep(id, callback) {
    fnGetAcctAddressByRepID(id, function (data) {
        GM.Global.defaultAddress = data;
        callback("success");
    });
}

function getDistAddress(id, callback) {
    fnGetDistAddressByID(id, function (data) {
        GM.Global.defaultAddress = data;
        callback("success");
    });
}

function getEmployeeAddress(id, callback) {
    fnGetShipToAddressByID(id, function (data) {
        GM.Global.defaultAddress = data;
        callback("success");
    });

}

function getshippingAccountAddress(id, callback) {
    fnGetShipToAddressByID(id, function (data) {
        GM.Global.defaultAddress = data;
        callback("success");
    });
}

function securityEventAccess(className, tagAttributeName, pageName) {
    fnGetSecurityEvents(pageName, function (syncdata) {
        var hideFlg = 0;
        if (syncdata.length > 0) {
            _.each($(className), function (sdata) {
                _.each(syncdata, function (data) {
                    if (data.C1530A_UPDATE_ACCESS_FL == "Y" &&
                        $(sdata).attr(tagAttributeName).toUpperCase() == data.C1530A_TAG_NAME.toUpperCase()) {
                        if (hideFlg == 0) {
                            $(className).addClass("hide");
                            hideFlg = 1;
                        }
                        $(sdata).removeClass("hide");
                        return;
                    }
                });
            });
        }
    });
}


function showImpactPopup() {
    $('.div-overlay').show();
    $('.div-price-overlay-content-container').addClass('hide');
    $('#div-price-overlay-content-container10').removeClass('hide');


    $('.div-price-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hideInfoPopup();
        });
    });
}

function hideImpactPopup() {
    $('.div-overlay').hide();
    $('.div-price-overlay-content-container').addClass('hide');
}


function strToDate(dateStr) {
    // create a JavaScript date object from it
    var dateObj = new Date(dateStr);
    // use toString from datejs
    var formattedDate = dateObj.toDateString("mm/dd/yyyy");
    return formattedDate;
}

function ValidateFileType(filename) {
    var UPLOAD_WHITELIST = [".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx", ".pdf", ".txt", ".rtf", ".jpg", ".gif", ".png", ".jpeg"]
    var bValid = true;
    var fileExtn = filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 1);
    if (UPLOAD_WHITELIST.indexOf(fileExtn) === -1) {
        bValid = false;
    }
    return bValid;

}

//function to convert copyright,registered and degree symbols
function uniToString(sName) {
    var retStr = sName;
    if (sName.indexOf("&reg;") > 0)
        retStr = sName.replace('&reg;', '\u00AE');
    else if (sName.indexOf("&copy;") > 0)
        retStr = sName.replace('&copy;', '\u00A9');
    else if (sName.indexOf("&deg;") > 0)
        retStr = sName.replace('&deg;', '\u00B0');

    return retStr;
}

// PMT-52360 - Below code has been commented
function setFileServer() {
    var egnyteflag = localStorage.getItem('egnytefl');
    if (egnyteflag == "YES") {
        return URL_Cloud_FileServer;
    } else {
        return URL_FileServer_Collateral;
    }
}

function hidePartDetailsPopup() {
    $('.div-overlay').hide();
    $('.div-price-overlay-content-container').addClass('hide');
}

function hidePartSysDetailsPopup() {
    $('.div-overlay').hide();
    $('.div-price-overlay-content-container').addClass('hide');
}

/*fnGetRuleValues: function to get the rule value basedon the company
 * Parameter : Ruleid,RuleGroupid,company id
 */
function fnGetRuleValues(ruleid, rulegrpid, cmpid, callback) {
    var appToken = localStorage.getItem("token");
    if (parseNull(appToken) != "") {
    var input = {
        "token": localStorage.getItem("token"),
        "ruleid": ruleid,
        "rulegrpid": rulegrpid,
        "cmpid": cmpid
    };
    fnGetWebServerData("common/fetchRuleValue", "gmRulesVO", input, function (data) {
        callback(data);
    }); 
}
}

/* fnValidateControlNo: TO validate the control number 
   Author: Karthik
 */
function fnValidateControlNo(partcno, callback) {
    var indexof = '0';
    var cntlindexof = '0';
    var machinecntlindexof = '0';
    var partnum = '';
    var controlNo = '';
    indexof = partcno.indexOf("^"); // get the index of '^'
    cntlindexof = partcno.indexOf("(10)"); // get the index of '(10)'
    machinecntlindexof = partcno.lastIndexOf("10"); //get the last occurence of 10
    if (indexof != -1) { // While scanning only format will be "partno^controlno"
        var cnumarr = partcno.split("^");
        partnum = cnumarr[0]; // get the characters before '^'
        controlNo = cnumarr[1]; // Getting the remaining character, after "^"
    } else if (cntlindexof != -1) { // While scanning only format will be "(01)udi number(17)exp date(10)controlno"     
        var cnumArray = partcno.split("(10)");
        controlNo = cnumArray[1]; //Getting the remaining character, after "(10)"         
    } else if (machinecntlindexof != -1) { //Getting the remaining character of last occurence of 10
        var machinecnumArray = partcno.substring(machinecntlindexof + 2, partcno.length);
        controlNo = machinecnumArray;
    } else {
        controlNo = partcno;
    }
    controlNo = controlNo.toUpperCase();
    callback(controlNo);
}

function showSalesAnalysisPopup() {
    $('.div-overlay').show();
    $('.div-price-overlay-content-container').addClass('hide');
    $('#div-price-overlay-content-container-sales-analysis').removeClass('hide');


    $('.div-price-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hideInfoPopup();
        });
    });
}

function hideSalesAnalysisPopup() {
    $('.div-overlay').hide();
    $('.div-price-overlay-content-container').addClass('hide');
}
//To get and set RFS mappingID based on Company
function fnGetRFSCompanyMapID(){
    fnGetRFSCompanymappingID(localStorage.getItem('cmpid'),function(data){
                        if (data.length > 0) {
                            intServerCountryCode = data[0].RFSID;
                        }
                    });
}


// Function for return the Empty value if the value passed as null/undefined/empty.
function parseNull(value) {
    return (value == undefined || value == null || value == "") ? "" : value.trim();
}

//function - used to Get the Fax authorization code
function loadFaxAuthDetails(){
    fnGetRuleValues('FAX_AUTH_CODE', 'FAX_DETAILS', localStorage.getItem("cmpid"), function (datafaxdtl) {
         if (datafaxdtl != null) {
            localStorage.setItem("faxAuthCode", datafaxdtl.rulevalue);
            console.log("faxAuthCode..."+localStorage.getItem("faxAuthCode"));
         }
    });
}

//function - used to connect firebase db
function syncServiceComponent(){
    //Firebase Configuration
    var firebaseConfig;
    //Get the Firebase Configuration from Rule Entry
    fnGetRuleValues('FIREBASE_CONFIG', 'FIREBASE_CREDENTIAL', localStorage.getItem("cmpid"), function (data) {
         if (data != null) {
            firebaseConfig = JSON.parse(data.rulevalue);

            //firebase is a global namespace from which all Firebase services are accessed.
            firebase.initializeApp(firebaseConfig);

            //Initialize firebase database     
            firedb = firebase.firestore();

            // caches a copy of the Cloud Firestore data that your app is actively using, so your app can access the data when the device is offline
            //[START initialize_persistence]
            firebase.firestore().enablePersistence();
            console.log("Firebase connected");
         }
    });
}

/* fnReadTextFrmImgAPI: 
 * Author : Karthik Somanathan
 * description: Read Text From Image  through Microsoft Azure API 
 */
function fnReadTextFrmImgAPI(subscriptionKey, URLBase, ContentType, arrayBuffer, callback) {
    console.log("fnReadTextFrmImgAPI Inside" + arrayBuffer);

    $.ajax({
            url: URLBase,
            beforeSend: function (jqXHR) {
                jqXHR.setRequestHeader("Content-Type", ContentType);
                jqXHR.setRequestHeader("Ocp-Apim-Subscription-Key", subscriptionKey);
            },
            type: "POST",
            processData: false,
            data: arrayBuffer
        })
        .done(function (data, textStatus, jqXHR) {
            // Show progress.
            setTimeout(function () {
                // The "Operation-Location" in the response contains the URI to retrieve the recognized text.
                var operationLocation = jqXHR.getResponseHeader("Operation-Location");

                $.ajax({
                        url: operationLocation,
                        beforeSend: function (jqXHR) {
                            jqXHR.setRequestHeader("Content-Type", "application/json");
                            jqXHR.setRequestHeader("Ocp-Apim-Subscription-Key", subscriptionKey);
                        },
                        type: "GET",
                    })

                    .done(function (data) {
                        // Show formatted JSON on webpage.
                        var responsedata = data;
                        console.log("responsedata==" + JSON.stringify(responsedata));
                        var i;
                        if (responsedata.analyzeResult.readResults[0] != undefined) {
                            var results = [];
                            for (i = 0; i < responsedata.analyzeResult.readResults[0].lines.length; i++) {
                                var extractedText = responsedata.analyzeResult.readResults[0].lines[i].text;
                                console.log("extractedText==" + extractedText);
                                results.push(extractedText);
                            }
                            callback(results);
                        }
                    })

                    .fail(function (jqXHR, textStatus, errorThrown) {
                        // Display error message.
                        var errorString = (errorThrown === "") ? "Error1. " : errorThrown + " (" + jqXHR.status + "): ";
                        errorString += (jqXHR.responseText === "") ? "" : (jQuery.parseJSON(jqXHR.responseText).message) ?
                            jQuery.parseJSON(jqXHR.responseText).message : jQuery.parseJSON(jqXHR.responseText).error.message;
                    fnShowStatus(" Error in Extracting Tag from the Image: " + errorString);
                    callback();
                    });
            }, 4000);
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            // Display error message.
            var errorString = (errorThrown === "") ? "Error2. " : errorThrown + " (" + jqXHR.status + "): ";
            errorString += (jqXHR.responseText === "") ? "" : (jQuery.parseJSON(jqXHR.responseText).message) ?
                jQuery.parseJSON(jqXHR.responseText).message : jQuery.parseJSON(jqXHR.responseText).error.message;
             fnShowStatus(" Error in extracting Tag from the Image: " + errorString);
             callback();
        })
}

//PC-5704 - When user click push notification, the below function will call and get the URL from appdelegate.m file
function openAppURL(appURL) {
    localStorage.setItem("FCMAppOpenURL", appURL);
    // Since the redirect fun written on Home page, if app running on home page then the redirect fun will not 
    // work when we comes from push notificaion url
    window.location.href = "";
    
}

//PC-5704 - When user click push notification, the below function will call and get the URL from appdelegate.m file
function openAppURLTop(url) {
    console.log("openAppURLTop >>>>>>>>>>>>>>");
    console.log(appURL);
    localStorage.setItem("FCMAppOpenURL", url);
    console.log("openAppURLTop global var>>>>>>>>>>>>>>" + url);
}

//PC-5704 - Firebase registration token fetch from appdelegate.m file
function openAppLaunch(FCMToken) {
    console.log("openAppLaunch >>>>>>>>>>>>>>.");
    console.log(FCMToken);
//    deviceToken = FCMToken;
}

//PC-5704 - Navigate app url from firebase url and then set empty once it redirected
function openFCMURL() {
    var FCMAppOpenURL = localStorage.getItem("FCMAppOpenURL");
    localStorage.setItem("FCMAppOpenURL", "");
    console.log("open FCM URL from localStorage>>>>>>>>>>"+ FCMAppOpenURL);
    return FCMAppOpenURL;
}



//function - used to take Amazon computer vision end point from rule table
function loadReadImageAPIEndpoint(){
    fnGetRuleValues('AMZ_ENDPOINT', 'AMAZON_CREDENTIAL', localStorage.getItem("cmpid"), function (dataendpoint) {
         if (dataendpoint != null) {
            localStorage.setItem("readImageEndPoint", dataendpoint.rulevalue);
            console.log("Amazon endpoint loaded..."+localStorage.getItem("readImageEndPoint"));
         }
    });
    
    fnGetRuleValues('AMZ_SUBKEY', 'AMAZON_CREDENTIAL', localStorage.getItem("cmpid"), function (datasubkey) {
         if (datasubkey != null) {
            localStorage.setItem("readImageSubKey", datasubkey.rulevalue);
         }
    });
}
/* Get Server Data through MicroApp Service */
function fnGetMicroAppServerData(serviceurl, input, callback, keepCall) {
    //console.log("Calling fnGetWebServerData with the following data <br> " + JSON.stringify(input));
  
    //passing user's company info with all the web service calls
    input.strCompId = localStorage.getItem("cmpid");
    input.cmpdateFmt = localStorage.getItem("cmpdfmt");
    input.strPlantId = localStorage.getItem("plantid");
    input.strDeptId = localStorage.getItem("deptid");
    input.strUserId = localStorage.getItem("userID");
    input.strAccLvl = localStorage.getItem("AcID");
    $.support.cors = true;
    $.ajax({
        beforeSend: function (jqXHR) {
           if (keepCall == undefined)
                $.xhrPool.push(jqXHR);
        },
        async: true,
        url: URL_MicroAppServer + serviceurl,
        method: "POST",
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(input),
        success: function (data) {
                callback(data);
        },
        error: function (model, response, errorReport) {
            console.log("Model ResponseText: " + model.responseText);
            //            console.log($.parseJSON(model.responseText))
            var string = true;
            try {
                $.parseJSON(model.responseText);
            } catch (e) {
                string = false;
            }
            if (model.responseText && string && $.parseJSON(model.responseText).message == "Invalid token, Please try login again.") {
                localStorage.removeItem("token");
                window.location.href = "#";
           } else if ((model.responseText != null) && string && (model.responseText != undefined) && (model.responseText != "") && $.parseJSON(model.responseText).message != "Record already exists. Please verify the entered record.")
                showError($.parseJSON(model.responseText).message);
            else
                console.log("Error serviceurl " + serviceurl);
            console.log("Error input " + JSON.stringify(input));
            // showError("Something went wrong! Please try after some time");
            //Since all web service pass through this function error callback parametrs are added here (response,errorReport)
            callback(null, model, response, errorReport);
        }
   });
}