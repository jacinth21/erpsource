define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global',
        'gmvCRMTab',

    	// Pre-Compiled Template
    	'gmtTemplate'
        ], 

function ($, _, Backbone, Handlebars, Global, GMVCRMTab, GMTTemplate) {

	'use strict';
	GM.View.CRMMain = Backbone.View.extend({

		events : {
			"click #div-crm-overlay-content-main" 		   : "closeOverlay",
			"click .btn-detail" 						   : "setColor",
			"click .close-modal, .close"				   : "closePopup",
            "click #physicianvisit, #salescall"            : "closeScheduler"
		},

		el: "#div-main",

		name: "CRM Main View",

		// Load the templates
		template_crmmain: fnGetTemplate(URL_Common_Template, "gmtCRMMain"),
		template_crmtab: fnGetTemplate(URL_Common_Template, "gmtCRMTab"),
		
		initialize: function() {
			GM.Global.Token = window.localStorage.getItem("token");

			$("body").removeClass();
			
			$("#div-crm-user").html(localStorage.getItem("fName") + " " + localStorage.getItem("lName"));
//			$("#header-user-fname").html(localStorage.getItem("fName"));
//            $("#header-user-lname").html(localStorage.getItem("lName"));
            $("#ul-header-right").removeClass("hide");
			$("#ul-mobile-main-header").removeClass("hide");
			$("#btn-home").show();
			// $("#div-messagebar").addClass("background-1 background-image");
			$("#div-header").removeClass("background-2").addClass("background-1 background-image");  
			
			
		},

		render: function () {
			this.$el.html(this.template_crmmain());
            if ((navigator.onLine) && (!intOfflineMode)) {
                var username = window.localStorage.getItem("fName") + " " + window.localStorage.getItem("lName");
                $("#div-crm-userdash").html("<b>" + username + "</b>");
                $("#div-header-right").show();
                this.showTabs();
                $("#div-messagebar").html("");
                if (!GM.Global.Browser)
                    fnGetLocalFileSystemPath(function (localpath) {
                        GM.Global.LocalFilePath = localpath;
                    });
            } else {
                this.$("#div-crm-main-view").html("<div class='div-crm-nointernet-connection'> <div> <i class='fa fa-exclamation-triangle fa-lg'></i>" + lblNm("msg_no_internet_connection") + "</div>" + lblNm("msg_connect_wifi_cellular_crm") + "</div>");
            }
            
            return this;
		},

		showTabs: function() {
			var gmvCRMTab = new GMVCRMTab({el: $("#div-crm-tab"), data: ($.parseJSON(localStorage.getItem("userTabs")))});
		},
        
		closeOverlay: function(event) {
			if(event.target.id == "div-crm-overlay-content-main" || event.target.id == "spn-crm-overlay-content-close" || event.target.id == "span-overlay-save") {
				this.$("#div-crm-overlay-content-main").hide();
				this.$("#div-crm-overlay").hide();
			}
		},

		//close unclosed elements to prevent Overlay in Popupview
		closePopup: function(){
            $("#div-crm-overlay-content-container").find(".div-date-content").hide();
            $("#div-crm-overlay-content-container").find(".div-one-date-content").hide();
            $("#div-crm-overlay-content-container").find("#select-surg-merc-status").hide();
            $("#div-crm-overlay-content-container").find(".div-search-content").hide();
            $("#div-crm-overlay-content-container").find("#input-crm-location").hide();
            $("#div-crm-overlay-content-container").find("#select-surg-act-type").hide();
            $("#div-crm-overlay-content-container").find("#select-surg-act-role").hide();
            $("#div-crm-overlay-content-container").find("#select-fieldsales-region").hide();
            $("#div-crm-overlay-content-container").find("#select-surg-location").hide();
            $("#div-crm-overlay-content-container").find("#select-surg-technique").hide();
            $("#div-crm-overlay-content-container").find("#select-surg-skillset").hide();
        },
        
        closeScheduler: function () {
            $(".scheduler-main-container").addClass("hide");
            $("#scheduler-info-view").addClass("hide");
            $("#scheduler-info-container").addClass("hide");
            $(".gmBtnGroup #li-close").addClass("hide");
        },
        
		// Close the view and unbind all the events
		close: function() {
			this.unbind();
			this.undelegateEvents();
		}

	});	

	return GM.View.CRMMain;
});
