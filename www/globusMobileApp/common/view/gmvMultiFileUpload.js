define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars',

    // Common
    'global', 'commonutil',

    // Pre-Compiled Template
    'gmtTemplate'

], 

    function ($, _, Backbone, Handlebars, Global, CommonUtil, GMTTemplate) {

    'use strict';
    
    GM.View.MultiFileUploadView = Backbone.View.extend({
        
        container: "#div-crm-overlay-content-container",
        
        // templates
        template: fnGetTemplate(URL_Common_Template, "gmtMultiFileUpload"),
        template_list: fnGetTemplate(URL_Common_Template, "gmtMultiFileUploadList"),
        template_load: fnGetTemplate(URL_Common_Template, "gmtSpinner"),

        initialize: function(options) { 
            
            var that = this;
            this.newFile = false;
            this.parent = options.parent;
            this.parentID = options.parentID;
            this.viewMode = options.viewMode;
            this.refgroup = options.refgroup;
            this.arrAcceptetFormat = options.arrAcceptetFormat;
            
            // removing extra parameters fetched from ws like cmpid, plantid, etc.
            this.createFileData(options.fileData, function(data){
                that.fileData = data;
                that.render();
            });
            
            hidePopup();
        },

        // renders the view
        render: function() {
            var that = this;
            showPopup(); 
            $(this.container).find('.modal-title').html("File Upload");
            $(this.container).find('.modal-body').html(this.template());
            $(".done-model").removeClass("hide");
            $(".load-model").addClass("hide");
            $(".save-model").addClass("hide");
            $('.submit-model').addClass("hide");
            $('.cancel-model').addClass("hide");
            
            var data = $.parseJSON(JSON.stringify(this.fileData));
            
            // Adding extra object to show the add new file option
            if(this.newFile){
                data.push({
                    filename: "",
                    formData: "",
                    refid: this.parentID,
                    refgroup: this.refgroup
                })
            }
            
            // populate template with data
            $("#ul-mutli-fileupld-container").html(this.template_list(data));
            
            // Binding elents to elements since we are binding elements whose scope belongs to mainview
            $(this.container).find('#div-mutli-fileupld-add').unbind('click').bind('click', function(e){
                that.moreFiles(e)
            });
            $(this.container).find('.li-mutli-fileupld i').unbind('click').bind('click', function(e){
                that.removeFiles(e)
            });
            $(this.container).find('.div-mutli-fileupld-popup-choose').unbind('click').bind('click', function(e){
                that.chooseFile(e)
            });
            $(this.container).find('.inp-mutli-fileupld').unbind('change').bind('change', function(e){
                that.selectFile(e)
            });
            $(this.container).find('.div-mutli-fileupld-title input').unbind('change').bind('change', function(e){
                that.setFileTitle(e)
            });
            $(this.container).find('#div-mutli-fileupld-all').unbind('click').bind('click', function(e){
                if (that.viewMode == "create")
                    that.closePopUp();
                else
                    that.uploadFile();
            });
            $(this.container).find('.done-model').unbind('click').bind('click', function() {
                that.closePopUp();
            });
        },
        
        // selects files to save and calls the ws fn
        uploadFile: function(refId, callback){
            var that = this;
            if(parseNull(refId) != "") {
                this.parentID = refId;
            }
            
            var forms = _.filter(this.fileData, function(o){
                return o.formData;
            });
            if(!forms.length)
                displayPopupMsg("Please select a File");
            else{
                var allFiles = this.fileData;
                _.each(allFiles, function(o, i){
                     o.index = i;
                });
                var files = _.filter(allFiles, function(o){
                    return o.fileid == "" && o.formData;
                });
                this.upldLen = files.length;
                this.upldCnt = 0;
                
                // Displaying loader during file save delay
                $(this.container).find("#div-mutli-fileupld-container").html(this.template_load());
                $(this.container).find(".img-loader-spinner").css({"margin-top": 0});
                
                //  Upload Files Simultaniously
                _.each(files, function(v){
                    v["refid"] = that.parentID;
                    that.upldForm(v,v.index, callback);
                })
            }
        },
        
        // calls ws to save actual file and file information
        upldForm: function(file, i, callback){
            var that = this;
            file.formData.append('token', GM.Global.Token);
            var input = file.formData;
            if (this.viewMode == "create")
                input.set('refid', this.parentID);
            
            $.ajax({
                url: URL_WSServer + 'uploadedFile/crmfileupload',
                data: input,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function(result) {
                    console.log("File Uploaded result >>>>>>" + JSON.stringify(result));
                    var ipt = {"token": GM.Global.Token, "refid": that.parentID, "filename": file.filename, "deletefl": "", "refgroup" : that.refgroup, "filetitle" : file.filetitle};
                    fnGetWebServerData("file/savefile", undefined, ipt, function(filenm){
                         console.log("File Saved result >>>>>>" + JSON.stringify(filenm));
                        that.fileData[i].fileid = filenm.fileid;
                        that.fileData[i].formData = undefined;
                        that.chkUpldDone();
                        callback("success");
                    },true);
                    
                },
                error: function (model, response, errorReport) {
                    // take count even if file upload fails to ensure loader is replaced with saved files
                    that.chkUpldDone();
                    callback("Error");
                }
            });  
        },
        
        // Tracks files saved and rerenders the view and the parent view after all files are saved
        chkUpldDone: function () {
            ++this.upldCnt;
            if (this.viewMode != "create"){
                if (this.upldCnt == this.upldLen) {
                    this.render();
                    this.parent.render();
                }
            }
        },
        
        // Provides new file uploading option
        moreFiles: function(e){
            var that = this;
            if((this.fileData.length == $('.li-mutli-fileupld').length)){
                this.newFile = true;
                this.render();
            }
        },
        
        // Deletes the file and rerenders the view and the parent view
        removeFiles: function(e){
            var that = this;
            var index = $(this.container).find('.li-mutli-fileupld i').index($(e.currentTarget));
            if(this.fileData[index]){
                if(this.fileData[index].fileid == ""){
                    this.fileData.splice(index, 1);
                    this.render();    
                    this.parent.render();
                }
                else{
                    var ipt = {"token": GM.Global.Token, "refid": this.parentID, "filename": this.fileData[index].filename, "deletefl": "Y", "refgroup" : this.refgroup, "filetitle" : this.fileData[index].filetitle, fileid: this.fileData[index].fileid};
                    $(this.container).find('.li-mutli-fileupld i').eq(index).remove();
                    this.fileData.splice(index, 1);
                    fnGetWebServerData("file/savefile", undefined, ipt, function(filenm){
                        that.render();
                        that.parent.render();
                    },true);
                }
            }
            else{
                
                $(this.container).find('.li-mutli-fileupld').eq(index).remove();
            }
        },
        
        // option to open directory and select a file
        chooseFile: function(e){
            var index = $(this.container).find('.div-mutli-fileupld-popup-choose').index($(e.currentTarget));
            $(".inp-mutli-fileupld").eq(index).val('');
            $(".inp-mutli-fileupld").eq(index).trigger("click");
        },

        // Loads the selected file's data into the view
        selectFile: function(e) {
            var that = this;
            var file = e.target.files[0];
            if(this.validateSize(e)) {
            var format = _.last(file.name.split('.'));
            if(!_.contains(this.arrAcceptetFormat,format.toLowerCase())) 
                displayPopupMsg("Invalid File Format. Please select any ." + this.arrAcceptetFormat.join(' or .') + " file");
            else {
                hideMessages();
                var index = $('.inp-mutli-fileupld').index($(e.currentTarget));
                var $form    = $(e.currentTarget).parent(".frm-mutli-fileupld"),
                params   = $form.serializeArray(),
                files    = $form.find('[name="file"]')[0].files;
                var formData = new FormData();    
                $.each(files, function(i, file) {
                    formData.append('file', file);
                });
                $.each(params, function(i, val) {
                    formData.append(val.name, val.value);
                }); 
                var fileName = file.name;
                var fileTitle = fileName .substr(0, fileName .lastIndexOf('.'));
                if(this.fileData[index]){
                    this.fileData[index].filename = fileName;
                    this.fileData[index].filetitle = fileTitle;
                    this.fileData[index].filesize = file.size;
                    this.fileData[index].refid = this.parentID;
                    this.fileData[index].refgroup = this.refgroup;
                    this.fileData[index].formData = formData;
                }
                else{
                    this.newFile = false;
                    this.fileData.push({
                        filename: fileName,
                        filetitle: fileTitle,
                        filesize: file.size,
                        refid: this.parentID,
                        refgroup: this.refgroup,
                        formData: formData,
                        fileid: ""
                    });
                }
                this.render();
            } 
            } else {
                displayPopupMsg("File size exceeds 5 MB");
            }
        },
        
        // to set data from a provided set of raw data from ws
        createFileData: function(v, cb){
            var data = [];
            v = arrayOf(v);
            if(!v.length)
                cb(data);
            else{
                _.each(v, function(o, i){
                    data.push({
                        fileid    : (o.fileid ? o.fileid : ""),
                        filename  : (o.filename ? o.filename : ""),
                        filetitle : (o.filetitle ? o.filetitle : ""),
                        refgroup  : (o.refgroup ? o.refgroup : ""),
                        refid     : (o.refid ? o.refid : ""),
                        reftype   : (o.reftype ? o.reftype : ""),
                        deletefl  : (o.deletefl ? o.deletefl : ""),
                        save      : ""
                    });
                    if((i+1) == v.length)
                        cb(data);
                });
            }
        },
        
        // sets Files Title
        setFileTitle: function(e){
            var index = $('.div-mutli-fileupld-title input').index($(e.currentTarget));
            this.fileData[index].filetitle = $(e.currentTarget).val();
            this.render();
        },
        
        // close popup / validate any unsaved file
        closePopUp: function () {
            if (this.viewMode == "create") {
                $('#div-crm-overlay-msg').addClass('hide').html("");
                hidePopup();
            } else {
                var filesToSave = _.filter(this.fileData, function (o) {
                    return o.formData;
                });
                if (filesToSave.length)
                    displayPopupMsg("Some Files are yet to be uploaded");
                else {
                    $('#div-crm-overlay-msg').addClass('hide').html("");
                    hidePopup();
                }
            }

        },
        
        validateSize: function(e) {
            var FileSize = e.target.files[0].size / 1024 / 1024; // in MB
            if (FileSize > 5) {
                return false;
                 $(e).val(''); //for clearing with Jquery
            } else {
                return true;
            }
        }
    });

    return GM.View.MultiFileUploadView;
});
