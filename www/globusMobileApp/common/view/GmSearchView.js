/**********************************************************************************
 * File:        GmSearchView.js
 * Description: Common View which will load a serach text box and able to do dynamic search
 				Uses SearchModel, ItemCollection and ItemView
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone', 'handlebars',
        'global','searchmodel', 'itemcollection', 'itemview'

        ], function ($, _, Backbone, Handlebars, Global, SearchModel, ItemCollection, ItemView) {

	'use strict';
	var rowValue;
	var SearchView = Backbone.View.extend({

		events: {
			"keyup #div-keyword-search input": "searchKeyword",
			"click #div-search-result-multiple": "selectMore",
			"click .item-searchlist": "select",
			"click #div-common-search-x": "clearKeyword",
			"click #div-select-all": "getAllRowItem"
		},

		template: fnGetTemplate(URL_Common_Template,"GmSearch"),
		
		//Initialize and Config the View 
		initialize: function (Options) {

			//Required Paramters
			this.el = Options.el;									//Place where this view is going to be placed (Required)
			this.fnName = Options.fnName;							//DAO function which gives the data (Required)

			//Setting default options through model
			this.searchConfig = new SearchModel(Options);
			this.config = this.searchConfig.toJSON();

			//Optional Paramters - setting for view
			//Auto search options
			this.autoSearch = this.config.autoSearch;				//Option to set the Auto Search option (Default: true) (Optional)
			this.targetArea = this.config.target;					//Place where the results will be displayed (Required if autoSearch is false)
			this.itemTemplate = this.config.template;				//Template used to render Result (Required if autoSearch is false)
			this.template_URL = this.config.template_URL;			//Template URL of the itemTemplate mentioned above (Required if autoSearch is false)

			this.callback = this.config.callback;					//Callback on selecting any result (Optional)
			this.minChar = this.config.minChar;						//Minimum characters to search (Default: 3) (Optional)
			this.usage = this.config.usage;							//1 determines normal search, 2 determines multiple select this.config (Default: 1) (Optional)
			this.ID = this.config.ID;								//ID of the search text box (Default: "txt-search-keyword") (Optional)
			
			this.render();
			
		},
		
		// Render the initial contents when the app starts
		render: function () {
			var that = this;
			this.$el.html(this.template(this.config));

			//If Target is not specified, set the default TargetArea
			if(this.targetArea=="") 
				this.targetArea = this.$("#ul-search-resultlist");
			
			$(window).on('orientationchange', function(){that.setCssProperties()});
			return this;
		},

		//Set the CSS properties of auto search result dynamically based on the textbox Location
		setCssProperties: function() {
			if(this.autoSearch) {
				this.$("#div-search-result-main").css("top",this.$("#div-keyword-search input").offset().top + this.$("#div-keyword-search input").height() + parseInt(this.$("#div-keyword-search input").css("margin-top")) + parseInt(this.$("#div-keyword-search input").css("padding-top")) + parseInt(this.$("#div-keyword-search input").css("margin-bottom")) + parseInt(this.$("#div-keyword-search input").css("padding-bottom")) +2 + "px")
				this.$("#div-search-result-main").css("left",this.$("#div-keyword-search input").offset().left + "px")
				this.$("#div-search-result-main").css("width",this.$("#div-keyword-search input").width());
				this.$("#div-search-result-main").css("padding-left",this.$("#div-keyword-search input").css("padding-left"));
				this.$("#div-search-result-main").css("padding-right",this.$("#div-keyword-search input").css("padding-right"));
				this.$("#div-search-result-main").css("margin",this.$("#div-keyword-search input").css("margin"));

				if(this.usage==2) 
					this.$("#div-search-result-multiple").css("margin-left","-" + parseInt(this.$("#div-search-result-main").css("padding-left")) + "px");
				
			}
			
		},
		
		//Render the results
		renderResult: function() {
			var that = this;
			this.itemCol.each(function(item) {
				var itemview = new ItemView({model: item});
				$(that.targetArea).append(itemview.render({template_URL : that.template_URL, template: that.itemTemplate}).el);
			}, this);
		},
//		******* PMT-31849 Searched Part Missing is Missing from Search Results *******
//		unlimitedScrollData: function() {
//			var that = this;
//			var appendData = [];
//			for(var i=that.dataadded;i<(that.dataadded+10);i++) {
//				if(that.searchdata[i]!=undefined) {
//					appendData.push(that.searchdata[i]);
//				}
//			}
//			that.dataadded += 10;
//			that.itemMainCol = new ItemCollection(appendData);
//			that.itemCol = that.itemMainCol;
//			this.renderResult();
//		},

		//Perform Search operation
		searchKeyword: function(e){
			var that = this;
			var searchData = $(e.currentTarget).val();
			var wordLen = searchData.length;
			this.setX(searchData.length);

			$(this.targetArea).empty();
			this.$("#div-search-result-multiple").addClass("hide");

			if(this.$("#div-search-result-multiple").hasClass("initialized")) {
				this.$("#div-search-result-multiple").removeClass("initialized");
				this.$("#div-search-result-multiple").text("Multiple");
				this.multipleSelectionMode = false;
			}

			if(wordLen==this.minChar){

				//fnName is the Given DAO Function Name
				this.fnName('', searchData, function(data) {

					if(data.length>0) {
						that.setCssProperties();
						
//						if(data.length>500) {
//							var sample = [];
//							for(var i=0;i<500;i++) {
//								sample.push(data[i]);
//							}
//							that.dataadded = 100;
//							that.searchdata = data;
//							that.minDataLen = 0;
//							that.itemMainCol = new ItemCollection(sample);
//							that.itemCol = that.itemMainCol;
//							that.renderResult();
//							if((that.autoSearch) && (data.length>1) && (that.usage == 2))
//								that.$("#div-search-result-multiple").removeClass("hide");
//							
//							that.$("#ul-search-resultlist").on("scroll",function() {
//								var scrollPercentage = 100 * this.scrollTop / (this.scrollHeight-this.clientHeight);
//								if((scrollPercentage>75) && (that.searchdata.length > that.dataadded)) {
//									that.unlimitedScrollData();
//								}
//							});
//						}
//						
//						else {
							that.itemMainCol = new ItemCollection(data);
							that.itemCol = that.itemMainCol;
							that.renderResult();
							if((that.autoSearch) && (data.length>1) && (that.usage == 2))
								that.$("#div-search-result-multiple").removeClass("hide");
//						}																															
					}
					else 
						$(that.targetArea).html("<li>"+lblNm("msg_not_found")+"</li>");

					if(that.autoSearch) 
						that.$("#div-search-result-main").show();
					else
						$(that.targetArea).show();
				});
				
			}

			else if(wordLen>this.minChar) {
				if(this.itemMainCol!=undefined) {
					this.itemCol = new ItemCollection(this.itemMainCol.search('Name',searchData));
					if(this.itemCol.length>0) {
						this.renderResult();
						if((this.autoSearch) && (this.itemCol.length>1) && (this.usage == 2))
							this.$("#div-search-result-multiple").removeClass("hide");
					}
					else 
						$(this.targetArea).html("<li>"+lblNm("msg_not_found")+"</li>");

					if(this.autoSearch) 
						this.$("#div-search-result-main").show();
					else
						$(this.targetArea).show();
				}
				else {
					_.delay(function() {
						if(wordLen == $(e.currentTarget).val().length()) {
							that.itemCol = new ItemCollection(that.itemMainCol.search('Name',searchData));
							if(that.itemCol.length>0) {
								that.renderResult();
								if((that.autoSearch) && (that.itemCol.length>1) && (that.usage == 2))
									that.$("#div-search-result-multiple").removeClass("hide");
							}
							else 
								$(that.targetArea).html("<li>"+lblNm("msg_not_found")+"</li>");

							if(that.autoSearch) 
								that.$("#div-search-result-main").show();
							else
								$(that.targetArea).show();
						}
						
					},300);
				}
				
			}

			else {
				if(this.autoSearch) 
					this.$("#div-search-result-main").hide();
			}	
		},

		//Initializes Multiple select / passes the selected value
		selectMore: function(e) {
			var elem = e.currentTarget;
			var that = this;

			if($(elem).hasClass("initialized")) {
				$(elem).removeClass("initialized");
				$(elem).text(lblNm("multiple"));
				this.multipleSelectionMode = false;
				this.result = new Array();

				this.$(".selected-item").each(function() {
					that.result.push({"ID" : this.id, "Name" : this.name, "Element" : this});
				});
				
				this.$("#div-common-search-x").addClass('hide');
				this.callback(this.result);
				if(this.autoSearch) 
					this.$("#div-search-result-main").hide();
				else
					$(this.target).hide();
			}

			else {
				$(elem).addClass("initialized");
				$(elem).text(lblNm("select")+" (0)");
				this.multipleSelectionMode = true;
			}
		},

		//Selects a element
		select: function(e) {
			if(this.multipleSelectionMode) {
				$(e.currentTarget).toggleClass("selected-item");
				this.$(".initialized").text(lblNm("select")+"(" + this.$(".selected-item").length + ")");
			}
			else {
				this.$("#div-common-search-x").addClass('hide');
				console.log($(e.currentTarget).attr("productmaterial"));
				this.result = {"ID" : e.currentTarget.id, "Name" : e.currentTarget.name, "Element" : e.currentTarget };
				console.log(this.result)
				this.callback(this.result);
				if(this.autoSearch) 
					this.$("#div-search-result-main").hide();
				else
					$(this.target).hide();
			}
		},
		
		
		clearKeyword: function(e) {

			var currElem = $(e.currentTarget).parent().find('input');
			$(currElem).val("");
			$(currElem).trigger("keyup");
			$(e.currentTarget).addClass('hide');
		    
		},
		
		
		setX: function(wordLength) {
			
			if(wordLength == 0) {
				this.$("#div-common-search-x").addClass('hide');
			}
			else {
				this.$("#div-common-search-x").removeClass('hide');
			}
		}
		
		
	});

	return SearchView;
});
