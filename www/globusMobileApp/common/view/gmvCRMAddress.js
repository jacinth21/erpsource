/**********************************************************************************
 * File                 :        CRMAddressView.js
 * Description     :       Common View to represent Address Details
 * Version           :       1.0
 * Author            :        cskumar
 **********************************************************************************/
define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars',

    // Common
    'global', 'gmmAddress', 'gmvCRMSelectPopup',
    
    //Model
    'gmmSurgeon',

    // Pre-Compiled Template
    'gmtTemplate'
], 

function ($, _, Backbone, Handlebars,
           Global, GMMAddress, GMVCRMSelectPopup,
           GMMSurgeon, GMTTemplate) {

    'use strict';
    
    GM.View.CRMAddress = Backbone.View.extend({
        
        container: "#div-crm-overlay-content-container",
        
        //templates
       template_addr: fnGetTemplate(URL_Common_Template , "gmtAddress"),
       template_address_list : fnGetTemplate(URL_Common_Template, "gmtAddressList"),
        
        //events
        events: {
            "click .addr-tab"                                                          : "showAddress",
            "click #div-addr-add"                                                  : "addAddrTab",
            "click .ul-addr .fa-trash-o"                                           : "deleteAddress",
            "click .addr-cancel"                                                     : "cancelAddress",
            "click .primaryfl-toggle"                                             : "chkAddrPrimfl",
            "click .btn-surgeon-addr-done"                                   : "addressDone",          
	        "click .div-module-country, .div-module-state"         : "openPopup"
        },

         //Initial configurations
        initialize: function(options) {
            var that = this;
            this.$el = options.$el;
            this.parent = options.parent;
            this.module = options.module;
            this.container = options.container;
            if(this.container!=undefined)
                this.$el = this.$el.find(this.container);
            console.log(this.container);
            console.log(this.$el);
//            this.loader = new initiateLoader($(this.el),"");
            this.addrCountry = [];
            this.addrState = [];
            this.render(options);
        },
        
        //to load the template and data
        render: function(options){
            var that = this;
                this.$el.html(that.template_addr({"module":"surg"}));
//            if( that.parent.model.get("arrgmsurgeonsaddressvo") == undefined )
                 console.log(that.parent.model)
            console.log(that.parent.model.arrgmsurgeonsaddressvo);
            if(that.parent.model.get("arrgmsurgeonsaddressvo")[0])
            _.each(that.parent.model.get("arrgmsurgeonsaddressvo"), function(data, i){
                that.addrCountry[i] = data.countryid;
                that.addrState[i] = data.stateid;
            });
            that.$("#div-module-addr-content").html(that.template_address_list(that.parent.model.get("arrgmsurgeonsaddressvo")));
         
            // Create as many tabs as there are addresses
            if(that.parent.model.get("arrgmsurgeonsaddressvo")[0])
            _.each(that.parent.model.get("arrgmsurgeonsaddressvo"), function(value, i){
                if(value.addid!="" && value.addid!=undefined)
                    that.$("#div-module-addr-tab").append("<ul class='ul-addr gmMediaWidth100 gmAlignHorizontal gmVerticalMiddle'><li id='btnaddr-" + (i+1) + "' class='addr-tab gmMediaWidth75 ul-sur-edit-addr-header gmTab gmTabPad gmBGLGrey gmFontBlack gmAlignHorizontal' tabcount='" + i + "'>Address " + (i+1) + "</li><li id='btndel-" + i + "' class='addr-dlt sur-edit-addr-delete addr-del edit gmAlignHorizontal gmTabDel gmFontWhite' tabcount='" + i + "'><i class='fa fa-trash-o fa-lg'></i></li></ul>");
                else
                    that.$("#div-module-addr-tab").append("<ul class='ul-addr gmMediaWidth100 gmAlignHorizontal gmVerticalMiddle'><li id='btnaddr-" + (i+1) + "' class='addr-tab btndel-0 gmMediaWidth75 gmTab gmTabPad gmAlignHorizontal gmBGLGrey gmFontBlack' tabcount='" + i + "'>Address " + (i+1) + "</li><li id='btndel-" + i + "' class='addr-cnc gmTabCnc gmAlignHorizontal gmFontWhite addr-can' tabcount='" + i + "'>X</li></ul>"); 
            });

            // Hide all address tabs except the first one
            $(".div-module-addr-edit:not(:first)").addClass('hide');
            $("#div-module-addr-tab .addr-tab:first").addClass("gmBGLGrey");
            $("#div-module-addr-tab .addr-del:first").addClass("addr-delete");
            
            
            if(!$("#tab-surg-address").hasClass("hide")){   //Mobile Layout
                $(".div-module-addr-edit").addClass("hide");
                $("#div-module-addr-tab .addr-tab").addClass("gmBGLGrey");
            }
            colorControl({"currentTarget": "#btnaddr-1"}, "addr-tab", "gmBGLGrey gmFontBlack", "gmBGDarkerGrey gmFontWhite");
            $("#div-surg-address #div-addr-add .fa").removeClass("hide");
        },
        
        //to show the particular address on clicking the tab
        showAddress: function(e){
            var that = this;
            var index = $(".addr-tab").index($(e.currentTarget));
            console.log(index);
            if($(".iPad").css("display")=="none"){
                $(".ul-addr").addClass("hide");
                $(".ul-addr .addr-tab").eq(index).parent().removeClass("hide");
            }
            
            $("#div-surg-address .btn-surgeon-addr-done").removeClass("hide");
            $("#div-surg-address #div-addr-add .fa").addClass("hide");
            var btn = e.currentTarget;
            var clickedTab = $(btn).attr("tabcount");
            this.$('.div-'+that.module+'-addr-detail').addClass('hide');
            this.$('.div-'+that.module+'-addr-edit').addClass('hide');
            this.$('.addr-dlt').removeClass('addr-delete').addClass("addr-del");
            this.$('.addr-cnc').removeClass('addr-cancel').addClass("addr-can");
            var control = ".div-module-addr-detail";

            if(GM.Global.Surgeon.Edit==true) 
                control = ".div-module-addr-edit";
            $(control).addClass('hide');
            var $div = $(control).eq(clickedTab);
            $div.removeClass('hide');
            var btnClicked = "#btndel-" + clickedTab;

            if($(btnClicked).hasClass("addr-dlt"))
                $(btnClicked).removeClass("addr-del").addClass('addr-delete');

            if($(btnClicked).hasClass("addr-cnc"))
                $(btnClicked).removeClass("addr-can").addClass('addr-cancel');

            $div.find('.data-add1').focus();
            $(".div-module-addr-edit").eq(clickedTab).find(".span-module-state-search input").val($(".div-module-addr-edit").eq(clickedTab).find(".span-module-state-search").parent().find(".data-statenm").val());
            
            if(1) {
                colorControl(e,  "addr-tab",  "gmBGLGrey gmFontBlack",  "gmBGDarkerGrey gmFontWhite");
                $(".addr-tab").removeClass("gmBGDarkerGrey gmFontWhite");
                $(e.currentTarget).addClass("gmBGDarkerGrey gmFontWhite");
            } else 
                $(".addr-tab:first").removeClass("gmBGLGrey gmFontBlack").addClass("gmBGGreen gmFontWhite");

            
            $("#div-surgeon-phone-content .ul-addr").addClass("hide");
            
            $("#div-surgeon-phone-content .addr-tab").each(function () {
                if($(this).hasClass("gmBGDarkerGrey")) {
                    $(this).parent().removeClass("hide");
                }
            });
            $("#div-surg-address #div-addr-add .fa").removeClass("hide");
        },
        
        //to add a new address template and it's tab
        addAddrTab: function() {
            
            var maxTab = this.$(".addr-tab:last").attr("tabcount");
            var that = this;
            var tabCount = 0;

            if(maxTab!=undefined) 
                tabCount = parseInt(maxTab) + 1;
            console.log(tabCount);
            console.log($(".div-module-addr-edit").eq(maxTab).find("#span-module-add1").val())
            if($(".div-module-addr-edit").eq(maxTab).find("#span-module-add1").val()!=""){
                $("#div-surg-address #div-addr-add .fa").addClass("hide");
                $("#div-surg-address .btn-surgeon-addr-done").removeClass("hide");
                 this.$(".div-module-addr-edit").addClass("hide");
                 this.$("#div-module-addr-tab").append("<ul class='ul-addr gmMediaWidth100 gmAlignHorizontal gmVerticalMiddle'><li id='btnaddr-" + (tabCount+1) +  "' class='addr-tab gmMediaWidth75 gmTab gmTabPad ul-sur-edit-addr-header gmBGLGrey gmFontWhite gmAlignHorizontal' tabcount='" + tabCount + "'>Address " + (tabCount+1) + "</li><li id='btndel-" + tabCount + "' class='addr-cnc phone-edit-addr-cancel addr-cancel gmTabCnc gmAlignHorizontal gmFontWhite' tabcount='" + tabCount + "'>x</li></ul>");

                var model = new GMMAddress();
                console.log(model)
                if(this.module == "surg")
                    that.parent.model.get("arrgmsurgeonsaddressvo").push({
                        "addid": "",
                        "add1": "",
                        "add2": "",
                        "city": "",
                        "statenm": "",
                        "stateid": "",
                        "countrynm": "",
                        "countryid": "",
                        "zip": "",
                        "primaryfl": "",
                        "comments": "",
                        "commentid": "",
                        "phoneid" : "",
                        "phone" : "",
                        "emailid" : "",
                        "email" : ""
                    });
                that.$("#div-module-addr-content").append(that.template_address_list(arrayOf(model.toJSON())));
//                if($(".iPhone").css("display")!="none")
//                    $(".ul-surgeon-address-state").remove();
//                else
//                    $(".ul-phone-surgeon-address-state").remove();
                    
                that.$(".addr-tab:last").trigger("click");
                $(".addr-tab").each(function(i){
                   $(this).removeClass("gmBGDarkerGrey gmFontWhite").addClass("gmBGLGrey gmFontBlack");
                    if((i+1) == $(".addr-tab").length){
                        console.log($(this))
                        $(".addr-tab:last").removeClass("gmBGLGrey gmFontBlack").addClass("gmBGDarkerGrey gmFontWhite");
                    }
                });
                $("#div-surg-address #div-addr-add .fa").removeClass("hide");
            }

        },
        
        // show all address tab on mobile address content page
        addressDone: function () {
            this.$el.find(".ul-addr").removeClass("hide");
            this.$el.find("#div-addr-add .fa").removeClass("hide");
            this.$el.find(".div-module-addr-edit").addClass("hide");
        },
        
        //to remove an address details from view n model
        deleteAddress: function(e) {
            var btndel = $(e.currentTarget).parent().attr("tabcount");
            var that = this;
            showNativeConfirm("Are you sure you want to delete this address?","Confirm",["Yes","No"], function(index) {
                if (index == 1) {
                    that.parent.model.get("arrgmsurgeonsaddressvo")[btndel].voidfl = "Y";
                    that.parent.saveSurgeon(true);
                }
            });
            
        },
        
        //cancelling unrequired addresses who's values got updated by updateJson fn
        cancelAddress: function(e){
            if($(".addr-tab").length>0){
                var tabcount = parseInt($(e.currentTarget).attr("tabcount"));
                $(".addr-cnc:last").remove();
                $(".addr-tab:last").remove();
                $(".div-module-addr-detail").eq(tabcount).remove();
                $(".div-module-addr-edit").eq(tabcount).remove();
                console.log(tabcount);
                this.addrCountry[tabcount] = "";
                this.addrState[tabcount] = "";
                if(this.module == "surg")
                    delete this.parent.model.get("arrgmsurgeonsaddressvo")[tabcount];
                $(".div-surg-address-tab").trigger("click");
                $(".addr-tab").eq(tabcount-1).trigger("click");
                $("#div-surg-address #div-addr-add .fa").removeClass("hide");
            }
//            alert()
        },
        
         //to disallow from selecting an address as primary more than once
        chkAddrPrimfl: function(e){
            var val = new Array();
            val = (this.parent.model.get("arrgmsurgeonsaddressvo"));
            val = arrayOf(val);
            val = _.filter(val, function (addr) {
                    return addr.primaryfl=="Y"
                });
           if(val.length==1){
                if($(e.currentTarget).hasClass("primaryfl-yes"))
                    this.togglePrimaryAddr(e);
                 else
                     showNativeAlert("You Have Already Chosen A Primary Address", "No Duplicate");
            }
            else if(val.length==0)
                this.togglePrimaryAddr(e);
        },

        
        //to toggle the div that decides primary fl value
        togglePrimaryAddr: function(e){
            if($(e.currentTarget).hasClass("primaryfl-yes")){          //make it NO
                $(e.currentTarget).removeClass("primaryfl-yes");
                $(e.currentTarget).find(".div-primaryfl-toggle").css({"transform":"translateX(-84px)"})
                $(e.currentTarget).find(".data-primaryfl").val("");
            }
            else{                                                                                   //make it YES
                $(e.currentTarget).addClass("primaryfl-yes");               
                $(e.currentTarget).find(".div-primaryfl-toggle").css({"transform":"translateX(-27px)"})
                $(e.currentTarget).find(".data-primaryfl").val("Y");
            }
            if(this.module == "surg")
                this.parent.updateModel({ "currentTarget":$(e.currentTarget).find(".data-primaryfl")});
        },
        
        //popup providing single-select option
        openPopup: function(event){
            var that = this;
            var item = "";
            var reData = [];
            if($(event.currentTarget).hasClass("div-module-state")){
                item = "state";
                reData = that.addrState;
            }
            else if($(event.currentTarget).hasClass("div-module-country")){
                item = "country";
                reData = that.addrCountry;
            }
            var index = $(".div-module-"+item).index($(event.currentTarget));
            that.systemSelectOptions = {                                                                                                              
                'title': $(event.currentTarget).attr("data-title"),
                'storagekey': $(event.currentTarget).attr("data-storagekey"),
                'webservice': $(event.currentTarget).attr("data-webservice"),   
                'resultVO': $(event.currentTarget).attr("data-resultVO"),
                'IDParameter': $(event.currentTarget).attr("data-IDParameter"),
                'NameParameter': $(event.currentTarget).attr("data-NameParameter"),
                'inputParameters': $(event.currentTarget).attr("data-inputParameters"),
                'codegrp': $(event.currentTarget).attr("data-codegrp"),
                'module': "surgeon",
                'event': event,
                'type': 2,
                'renderOne': reData[index],
                'parent': this,
                'moduleLS': "",
                'moduleCLS': "",
                'fnName': fnGetCodeNMByGRP,
                'callback': function(data1, data2){
                    console.log(data1)
                    console.log(data2)
                    $(event.currentTarget).html(data2);
                    if(item == "state"){
                        $(event.currentTarget).parent().find(".data-stateid").val(data1);
                        that.addrState[index] = data1;
                        $(event.currentTarget).parent().find(".data-statenm").val(data2);
                    }
                    else if (item == "country"){
                        $(event.currentTarget).parent().find(".data-countryid").val(data1);
                        that.addrCountry[index] = data1;
                        $(event.currentTarget).parent().find(".data-countrynm").val(data2);
                    }
                    if(that.module=="surg")
                        that.parent.updateModel(event, data1[0], data2[0]);
                }
            };
            that.systemSelectView = new GMVCRMSelectPopup(that.systemSelectOptions);
        },
        
        //Close view function to undelegate the events
        close: function(view) {
        	view.unbind();
        	view.undelegateEvents();
        	view.$el.empty();
            return null;
        }
        
    });
    
     return GM.View.CRMAddress;
});
