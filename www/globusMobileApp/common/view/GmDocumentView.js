/**********************************************************************************
 * File:        GMDocumentView.js
 * Description: Makes a Image slide show for the given images
 * Version:     1.0
 * Author:      praja 
 **********************************************************************************/


/*global define*/
define([
    'canvas', 'jquery', 'underscore', 'backbone', 'handlebars', 'jqueryscroll', 'jqueryvisible', 'jqueryzoom',
    'global', 'commonutil', 'filesync', 'datasync'
],

    function (Canvas, $, _, Backbone, Handlebars, JqueryScroll, Visible, Panzoom, Global, CommonUtil, FileSync, Datasync) {

        'use strict';


        var DocumentView = Backbone.View.extend({


            el: "#div-image-viewer-background",

            template: fnGetTemplate(URL_Common_Template, "GmDocumentViewer"),

            initialize: function (info) {
                this.triggeredEvent = info.event;
                this.parent = info.parent;
                this.docPath = info.path;
                this.docID = info.docId;
                this.render();
            },

            render: function () {
                try {
                    var that = this;
                    var data = [];
                    var className = this.triggeredEvent.target.className,
                        path = $(this.triggeredEvent.currentTarget).attr('name'),
                        docID = $(this.triggeredEvent.currentTarget).attr('id');
                    var PDFTitle = $(this.triggeredEvent.currentTarget).text();

                    if (parseNull(docID) == "") 
                        docID = this.docID;
                    
                    if (parseNull(path) == "")
                        path = this.docPath;

                    if ((window.top.location.hash).indexOf('share') < 0) {

                        if (parseNull(path) == "") {
                            fnGetFilePath(docID, function (data) {
                                path = path = data[0].Path; 
                                if (path != "")
                                    that.fnGetDocResolvePath(path, docID);
                                else
                                    throw new Error("GmDocumentView:render() : Path is not available(3)."+ path + "and id is "+ docID);
                                
                            });
                        }
                        else if (parseNull(path) != "") {
                            this.fnGetDocResolvePath(path, docID);
                        } 
                        else {
                            throw new Error("GmDocumentView:render() : Path is not available(2)."+ path + "and id is "+ docID);
                        }
                    }

                } catch (err) {
                    showAppError(err, "GmDocumentView:render()");
                }

            },

            // func to pass file path for download the file
            fnGetDocResolvePath: function(path, docID) {
                try {
                    var that = this;
                    var fileServer = setFileServer();
                    fnResolvePath(path, function(status, devicepath) {
                        if (status) {
                            var DOCViewer = window.open(decodeURI(devicepath), '_blank', 'location=no,EnableViewPortScale=yes');
                        } else if (navigator.onLine) {
                            var connectionType = (navigator.connection.type).toLowerCase();
                            if ((connectionType.indexOf('cellular') >= 0) && that.parent.confirmDownload) {
                                showNativeConfirm(lblNm("msg_device_connected_dataplan"), lblNm("msg_warning"), [lblNm("msg_yes"), lblNm("msg_do_not_ask"), lblNm("msg_no")], function(btnIndex) {
                                    if (btnIndex == 1)
                                        that.download(fileServer, devicepath, path, docID);
                                    else if (btnIndex == 2) {
                                        that.parent.confirmDownload = false;
                                        that.download(fileServer, devicepath, path, docID);
                                    }
                                })
                            } else
                                that.download(fileServer, devicepath, path, docID);
                        } else {
                            showMsgView("057");
                        }
                    });
                } catch (err) {
                    showAppError(err, "GmDocumentView:fnGetDocResolvePath()");
                }
            },

            download: function (fileServer, devicepath, path, docID) {
                fnDownload(fileServer + path, devicepath + path, function (percent) {
                    $('#spn-prg-' + docID).show();
                    $('#spn-prg-' + docID).find('progress').val(percent * 100);
                }, function () {
                    $('#spn-prg-' + docID).hide();
                    var DOCViewer = window.open(decodeURI(devicepath) + path, '_blank', 'location=no,EnableViewPortScale=yes');

                });
            },


            //Closes the View
            close: function (event) {
                this.unbind();
                this.undelegateEvents();
                this.$el.unbind();
                this.$el.hide();
                this.$el.empty();
            }

        });

        return DocumentView;
    });