define([
        'jquery', 'underscore', 'backbone', 'handlebars',
        'global',

    	// Pre-Compiled Template
    	'gmtTemplate'
        ], function ($, _, Backbone, Handlebars, Global, GMTTemplate) {

	'use strict';
	GM.View.Item = Backbone.View.extend({

		initialize: function(options) {
			
			this.$el = options.el;
			this.template = fnGetTemplate(options.template_URL,options.template);
            this.render();
        },
        
        render: function (options) {
//            console.log("Eshcol Data: " + JSON.stringify(this.model));
			this.$el.append(this.template(this.model.toJSON()));
			return this;
		}
	});

	return GM.View.Item;
});
