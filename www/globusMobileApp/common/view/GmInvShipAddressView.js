/**********************************************************************************
 * File:        GmInvShipAddressView.js
 * Description: This view contains the Shipping Addresses.
 * Version:     1.0
 * Author:      ejayabalan
 **********************************************************************************/

define([
    'jquery', 'underscore', 'backbone', 'handlebars',
    'global', 'notification', 'device',
    'dropdownview','searchview', 'addresscollection', 'addresslistview'
],

function ($, _, Backbone, Handlebars, Global, Notification, Device, DropDownView, SearchView, AddressCollection, AddressListView) {

	'use strict';

	var InvShipAddressView = Backbone.View.extend({

		events: {
			"click #div-address-to-list .div-dropdown-main, #div-address-to-list .div-dropdown-list li": "toggleArrow",
			"click .div-address-row": "assignAddress",
			"click #li-add-new-address" :"getNewAddress",
			"click #span-do-add-new-address-header-x ,#div-do-add-new-address-cancel" : "closeNewAddressTab",
            "click #span-do-edit-ship-addrfl-header-x" : "closeINVEditAddrFL",
			"click #div-do-add-new-address-submit" : "updateNewAddress",
			"click #div-do-edit-ship-addrfl-submit" : "updateINVAddressFL",
			"change .div-address-options select" : "getAddressModeCarrier",
			"click .div-address-row i" : "openPopup"
		},

		template: fnGetTemplate(URL_Common_Template,"GmShipAddress"),
		template_newaddress: fnGetTemplate(URL_Common_Template, "GmDOAddNewAddress"),
		template_addressfl: fnGetTemplate(URL_Common_Template, "GmAddressfl"),

		initialize: function(options) {
			this.options = options;
			this.render(options);            
            this.addressCollection="";
		},

		render: function () {

			var that = this;

			this.$el.html(this.template());
			
			fnGetDestType(function(data) {
                                data = _.filter(data, function (data) {
                                       return data.ID != 4124
                                });
				that.showDestinationType(data);
			});

							fnGetCodeList(localStorage.getItem("cmpid"),
									'DCAR', function(data) {
										GM.Global.totalCarrier = data;
									});
			this.getRepAddress(this.options.dfltRep);
            this.$(".li-search").hide();
            this.$("#li-search-rep").show();
			// fnGetAddressByID(2977, function(data) {
			// 	that.showAddressListView("#div-ship-address-list", data, "GmInvAddress", 0, 0, that.displayResult, undefined, URL_Common_Template);	
			// });
			console.log(this.options.dfltRep);
			var renderOptions = {
				"el":this.$('#li-search-acct'),
				"ID" : "txt-billing-account",
        		"Name" : "txt-search-name",
        		"Class" : "txt-keyword-account",   	
                "minChar" : minKeyinChar,
        		"autoSearch": true,
        		"placeholder": lblNm("account_search"),
        		"fnName": fnGetAccounts,
        		"usage": 1,
        		"template" : 'GmSearchResult',
				"template_URL" : URL_Common_Template,
				"callback": function(rowValue) {that.getAcctAddress(rowValue.ID);}
        	};
						
			var acctsearchview = new SearchView(renderOptions);

 				renderOptions = {
				"el":this.$('#li-search-rep'),
				"ID" : "txt-billing-account",
        		"Name" : "txt-search-name",
        		"Class" : "txt-keyword-account",
                "minChar" : minKeyinChar,    
        		"autoSearch": true,
        		"placeholder": lblNm("sales_rep_search"),
        		"fnName": fnGetSalesRep,
        		"usage": 1,
        		"template" : 'GmSearchResult',
				"template_URL" : URL_Common_Template,
				"callback": function(rowValue) {that.getRepAddress(rowValue.ID);}
        	};
						
			var repsearchview = new SearchView(renderOptions);

				renderOptions = {
				"el":this.$('#li-search-dist'),
				"ID" : "txt-billing-account",
        		"Name" : "txt-search-name",
        		"Class" : "txt-keyword-account",
                "minChar" : minKeyinChar,    
        		"autoSearch": true,
        		"placeholder": lblNm("msg_distributor_search"),
        		"fnName": fnGetDist,
        		"usage": 1,
        		"template" : 'GmSearchResult',
				"template_URL" : URL_Common_Template,
				"callback": function(rowValue) {that.getDistAddress(rowValue.ID);}
        	};
						
			var	distsearchview = new SearchView(renderOptions);
				renderOptions = {
				"el":this.$('#li-search-dealer'),
				"ID" : "txt-billing-account",
        		"Name" : "txt-search-name",
        		"Class" : "txt-keyword-account",
                "minChar" : minKeyinChar,    
        		"autoSearch": true,
        		"placeholder": lblNm("msg_distributor_search"),
        		"fnName": fnGetDealerAdd,
        		"usage": 1,
        		"template" : 'GmSearchResult',
				"template_URL" : URL_Common_Template,
				"callback": function(rowValue) {that.getPartyAddress(rowValue.ID);}
        	};
						
			var	dealersearchview = new SearchView(renderOptions);

			this.$("#li-search-acct").hide();
			this.$("#li-search-dist").hide();

 			return this;
		},
		
		getAddressModeCarrier: function(event) {
			var parent = $(event.currentTarget).parent();
			$(parent).trigger("click");
		},

		getRepAddress: function(id) {
			var that = this;
			this.currRepID = id;
			this.$("#txt-billing-account").val("");
			this.$("#div-ship-address-list").show();
			console.log(id);
			fnGetRepAddressByID(id, function(data) {
				that.showAddressListView("#div-ship-address-list", data, "GmInvAddress", 0, 0, that.displayResult, undefined, URL_Common_Template);	
                that.$(".div-address_fl").show();
			});
		},

		getAcctAddress: function(id) {
			var that = this;
			this.currAcctID = id;
			this.$("#txt-billing-account").val("");
			this.$("#div-ship-address-list").show();
			fnGetAcctAddressByID(id, function(data) {
				console.log(data);
				that.showAddressListView("#div-ship-address-list", data, "GmInvAddress", 0, 0, that.displayResult, undefined, URL_Common_Template);	
			});
		},
		getAcctAddressFromRep: function(id) {
			var that = this;
			//this.currAcctID = id;
			this.$("#txt-billing-account").val("");
			this.$("#div-ship-address-list").show();
			fnGetAcctAddressByRepID(id, function(data) {
				console.log(data);
				that.showAddressListView("#div-ship-address-list", data, "GmInvAddress", 0, 0, that.displayResult, undefined, URL_Common_Template);	
			});
		},

		getDistAddress: function(id) {
			var that = this;
			this.currDistID = id;
			this.$("#txt-billing-account").val("");
			this.$("#div-ship-address-list").show();
			fnGetDistAddressByID(id, function(data) {
				that.showAddressListView("#div-ship-address-list", data, "GmInvAddress", 0, 0, that.displayResult, undefined, URL_Common_Template);	
			});
		},
        

		showAddressListView: function(controlID, data, addresstemplate, columnheader, renderType, callback, sortoptions, template_URL) {
			var that=this;
            that.addressCollection = new AddressCollection(data);
			this.addresslistview = new AddressListView(
				{ 
					el: controlID, 
					collection: that.addressCollection,
					columnheader: columnheader, 
					addresstemplate: addresstemplate,
					renderType: renderType,
					sortoptions: sortoptions,
					template_URL: template_URL
				});
			if(this.options.onListRender!=undefined)
				this.options.onListRender(this.addresslistview);
		},

		displayResult: function() {
			this.$("#div-ship-address-list").show();
		},

		showDestinationType: function (data) {
		    var that = this;
		    var defaultShipto = GM.Global.DefaultShipto;
		    GM.Global.ShipToInfoId = GM.Global.DefaultShipto;
		    this.drpDownView = new DropDownView({
		        el: this.$("#li-address-to-list"),
		        data: data,
		        DefaultID: "4000520"
		    });

		    this.$("#li-address-to-list .arrow-right").hide();
		    that.getData(defaultShipto);
		    this.$("#li-address-to-list").bind('onchange',
		        function (e, selectedId) {
		            GM.Global.ShipToInfoId = selectedId;
		            that.getData(selectedId);
		        });
		},
		
		showAccountType:function(){
			
			var accType = {"arrAccType" : [{"ID":"90400","Name":"Home"}
            ,{"ID":"90401","Name":"Office"}
            ,{"ID":"90403","Name":"HFPU"}
            ,{"ID":"26240675","Name":"Hospital"}
            ,{"ID":"90402","Name":"Others"}]};
			
			this.acctDropDownView = new DropDownView({
				el : this.$("#div-acct-list"),
				data : accType.arrAccType,
				DefaultID : "90402"
			});
			
		},
		
		showCountryList:function(data){
			this.cntyDropDownView = new DropDownView({
				el : this.$("#div-country-list"),
				data : data,
				DefaultName : "United States"
			});

			this.$("#div-country-list").bind('onchange', function (e, selectedId,SelectedName) {
				this.country = selectedId  ;
				this.countryname =SelectedName; 
			});		
		},
		showStateList:function(data){

			data.push({"ID":"0","Name":"Choose State"});
			this.stateDropDownView = new DropDownView({
				el : this.$("#div-state-list"),
				data : data,
				DefaultID : "0"
			});
			this.$("#div-state-list").bind('onchange', function (e, selectedId,SelectedName) {
				this.state = selectedId  ;
				this.statename =SelectedName; 
			});
		},

		getData: function (selectedId) {
		    if (selectedId == "4000519") {
		        this.$(".li-search").hide();
		        this.$("#li-search-acct").show();
		        this.$("#li-add-new-address").hide();
		        if (this.options.dfltAcct == undefined || this.options.dfltAcct == "")
		            this.getAcctAddressFromRep(this.options.dfltRep);
		        else
		            this.getAcctAddress(this.options.dfltAcct);

		    }

		    if (selectedId == "4000520") {
		        this.$(".li-search").hide();
		        this.$("#li-search-rep").show();
		        this.$("#li-add-new-address").show();
		        this.getRepAddress(this.options.dfltRep);
		    }

		    if (selectedId == "4000521") {
		        this.$(".li-search").hide();
		        this.$("#li-search-dist").show();
		        this.$("#li-add-new-address").hide();
		        this.getDistAddress(this.options.dfltDist);
		    }

		    if (selectedId == "107800") { // ship to  dealer
		        this.$(".li-search").hide();
		        this.$("#li-search-dealer").show();
		        this.$("#li-add-new-address").hide();
		        this.getPartyAddress(this.options.dfltDealer);
		    }


		},

		getShipInfo: function () {
		    var status = {};
		    status.shipTo = this.drpDownView.getCurrentValue()[0];
		    switch (status.shipTo) {
		        case 4000519:
		            status.shipTo = "4122";
		            status.shipToID = this.currAcctID;
		            break;
		        case 4000520:
		            status.shipTo = "4121";
		            status.shipToID = this.currRepID;
		            break;
		        case 107800:
		            status.shipTo = "107801";
		            status.shipToID = this.currDeaLID;
		            break;
		        case 4000521:
		            status.shipTo = "4120";
		            status.shipToID = this.currDistID;
		    }
		    return status;
		},
        
		validateNewAddress: function () {
			
		},

		updateNewAddress:function(event){
//			fnShowStatus("");
			this.$("#spn-addrtype").hide();
			var addrtype = this.acctDropDownView.getCurrentValue()[0];
			var addrtypename = this.acctDropDownView.getCurrentValue()[1];
			var name = this.$("#li-do-add-new-address-name").find('input').val();
			var addr1 = this.$("#li-do-add-new-address-addr1").find('input').val();	
			var addr2 = this.$("#li-do-add-new-address-addr2").find('input').val();	
			var city = this.$("#li-do-add-new-address-city").find('input').val();	
			var state = this.stateDropDownView.getCurrentValue()[0];
			var statename = this.stateDropDownView.getCurrentValue()[1];
			var zip = this.$("#li-do-add-new-address-zip").find('input').val();	
			var country = this.cntyDropDownView.getCurrentValue()[0];	
			var countryname = this.cntyDropDownView.getCurrentValue()[1];
			var partyid = "";
			if((addr1!="")&&(city!="")&&(state!="0")&&(zip!="")) {
				console.log(addrtype);
				if(this.acctDropDownView.getCurrentValue()[0] == undefined)
					{
						this.$("#spn-addrtype").show();
					    return;			    	
					}
				this.$("#div-do-add-new-address-submit").text(lblNm("msg_submitting"));
				var that = this;
				fnGetRepInfo(that.options.dfltRep,function(data){
				     that.partyid = data['C101_PARTY_ID'];
						
					   console.log(that.partyid);
				
					that.serverInput = {
							"token": localStorage.getItem("token"),
		                    "addrid" : "",
		                    "addresstype" : addrtype,
		                    "partyid" : that.partyid,
		                    "billadd1" :addr1,
		                    "billadd2" :addr2,
		                    "billcity" :city,
		                    "state" : state,
		                    "country" : country,
		                    "zipcode" :zip,
		                    "statename": statename,
		                    "countryname": countryname,
		                    "addresstypename": addrtypename                                                      

						};
					console.log(that.serverInput);
				
					fnGetWebServiceData("address/syncaddr",undefined, that.serverInput, function(status,data){
						if((navigator.onLine) && (!intOfflineMode)) {
							if(status)
								{
									console.log(data);
									fnSaveNewAddress(data,function(){
										that.getRepAddress(that.currRepID);
									});							
									that.$("#div-do-addnew-address").hide();
								}
							else
								{							
								 	if(data["message"] != null || data["message"] != undefined)
								 		{
						                showNativeAlert(data["message"], lblNm("msg_new_address"), lblNm("msg_ok"), function (argument) {
						                	
						                });
								 		}
								 	else
								 		{				 		
								 		  showNativeAlert(lblNm("msg_error_occured_contact"), lblNm("msg_new_address"), lblNm("msg_ok"), function (argument) {				                	
							                });
								 		}					
								}
						}
						else
							{
					 		  showNativeAlert(lblNm("msg_connect_wifi_newaddress"), lblNm("msg_no_internet_connection"), lblNm("msg_ok"), function (argument) {
				                	
				                });
							}
						that.$("#div-do-add-new-address-submit").text(lblNm("submit"));
					});
				});	
			}
			else {
				showNativeAlert(lblNm("msg_fill_all_fields"),lblNm("msg_validation"));
			}
			
			
		},

		toggleArrow: function() {
			if (this.$("#li-address-to-arrow i").hasClass("fa-caret-right")) {
				this.$("#li-address-to-arrow i").removeClass("fa-caret-right").addClass("fa-caret-down");
			}
			else {
				this.$("#li-address-to-arrow i").removeClass("fa-caret-down").addClass("fa-caret-right");
			}
		},

		assignAddress: function(event) {
			var id = (event.currentTarget.id).replace('div-address-','');
			this.$el.trigger("onselect", [id, event.currentTarget]);
		},

		openPopup: function(event) {
			event.stopImmediatePropagation();
            var that=this;
			var elem = event.currentTarget;
			if($(elem).hasClass('fa-pencil-square-o')){
				var text = $(elem).parent().parent().text().trim().replace(/\n/g," ").replace(/\t/g,"");
				var currAttnTo = $(elem).parent().text().trim();
				showNativePrompt(text.replace(currAttnTo,''),lblNm("msg_attn_to"),undefined, currAttnTo, function(data) {
					if(data.buttonIndex==1) {
						$(elem).parent().find(".spn-attn-to").text(data.input1);
						$(elem).parent().trigger("click");
					}
				});
			}
            else if($(elem).hasClass('div-address_fl')){
                var addrid= $(elem).attr("data-id");
                var modelData=that.addressCollection.find(function(model) { 
                    return model.get('AddressID') == addrid;                 
                });
                console.log(modelData.toJSON());
                this.$("#div-do-editship-addressfl").show();
                this.$("#div-do-editship-addressfl").html(this.template_addressfl(modelData.toJSON()));
            }
			else {
				var text = $(elem).parent().parent().find(".div-address").text().trim().replace(/\n/g," ").replace(/\t/g,"");
				var currInstrction = $(elem).attr("data-instruction");
				currInstrction = (currInstrction)?currInstrction:"";
				showNativePrompt(text,lblNm("msg_shipping"),undefined, currInstrction, function(data) {
					if(data.buttonIndex==1) {
						$(elem).attr("data-instruction",data.input1);
						$(elem).parent().trigger("click");
					}
				});
			}
		},

		getNewAddress:function(event){
			this.$("#div-do-addnew-address").show();
			this.$("#div-do-addnew-address").html(this.template_newaddress());
			this.$("#spn-addrtype").hide();
			var that = this;
			
			that.showAccountType();
			
			fnGetStateList(function(data){
				console.log(data);
				that.showStateList(data);			
			});
			
			fnGetCountryList(function(data){
				console.log(data);
				that.showCountryList(data);			
			});	
				
		},
		closeNewAddressTab: function() {
			this.$("#div-do-addnew-address").hide();
		},
                
		//Function to fetch address details by party id(Dealer address)
        getPartyAddress: function(id) {
			var that = this;
			this.currDeaLID = id;
			this.$("#txt-billing-account").val("");
			this.$("#div-ship-address-list").show();
			fnPartyAddressInv (id, function(data) {
				that.showAddressListView("#div-ship-address-list", data, "GmInvAddress", 0, 0, that.displayResult, undefined, URL_Common_Template);	
			});
		}, 
                
        // Update Shipping Address Flag
        updateINVAddressFL: function (event) {
            var that = this;
            var primaryFL = $('#primaryFL').is(':checked') ? 'on' : 'off';
            var inactiveFL = $('#inactiveFL').is(':checked') ? 'on' : 'off';
            var saveValid = true;
            var notValid = false;

            var addrid = $(event.currentTarget).attr("data-id");
            console.log(addrid);
            var modelData = that.addressCollection.find(function (model) {
                return model.get('AddressID') == addrid;
            });
            console.log(modelData.toJSON());
            modelData = modelData.toJSON();
            //validate if no changes made
            if ((primaryFL == 'off' && inactiveFL == 'off') || (primaryFL == 'off' && (modelData.PrimaryFL == 'N' || modelData.PrimaryFL == '') && inactiveFL == 'off') || (primaryFL == 'on' && modelData.PrimaryFL == 'Y' && inactiveFL == 'off')) {
                saveValid = false;
                notValid = true;
            }
            // validate if uncheck the primary flag or inactive the address which has primary flag
            if ((primaryFL == 'off' && modelData.PrimaryFL == 'Y') || (primaryFL == 'on' && inactiveFL == 'on')) {
                saveValid = false;
                notValid = false;
            }

            if (saveValid) {
                modelData.PrimaryFL = primaryFL;
                modelData.InactiveFL = inactiveFL;
                var addressflInput = {
                    "token": localStorage.getItem("token"),
                    "addrid": modelData.AddressID,
                    "addresstype": modelData.AddressType,
                    "partyid": modelData.PartyID,
                    "billadd1": modelData.Add1,
                    "billadd2": modelData.Add2,
                    "billcity": modelData.City,
                    "state": modelData.StateID,
                    "country": modelData.Country,
                    "zipcode": modelData.Zip,
                    "statename": "",
                    "countryname": "",
                    "addresstypename": "",
                    "primaryflag": modelData.PrimaryFL,
                    "activeflag": modelData.InactiveFL

                };
                console.log(addressflInput);
                fnGetWebServiceData("address/syncaddr", undefined, addressflInput, function (status, data) {
                    if ((navigator.onLine) && (!intOfflineMode)) {
                        if (status) {
                            data.primaryflag = (data.primaryflag == "on") ? 'Y' : '';
                            data.activeflag = (data.activeflag == "on") ? 'Y' : '';
                            console.log("MODified>>>" + JSON.stringify(data));
                            fnUpdateAddressFL(data.partyid, data.primaryflag, function () {
                                fnSaveNewAddress(data, function () {
                                    that.getRepAddress(that.currRepID);
                                });
                            });
                            console.log("Success");
                            that.$("#div-do-editship-addressfl").hide();
                        } else {
                            if (data["message"] != null || data["message"] != undefined) {
                                showNativeAlert(data["message"], lblNm("edit_shipping_address_properties"), lblNm("msg_ok"), function (argument) {

                                });
                            } else {
                                showNativeAlert(lblNm("msg_error_occured_contact"), lblNm("edit_shipping_address_properties"), lblNm("msg_ok"), function (argument) {});
                            }
                        }
                    } else {
                        showNativeAlert(lblNm("msg_connect_wifi_newaddress"), lblNm("msg_no_internet_connection"), lblNm("msg_ok"), function (argument) {

                        });
                    }
                    that.$("#div-do-edit-ship-addrfl-submit").text(lblNm("submit"));
                });
            } else {
                if (notValid == true)
                    showNativeAlert(lblNm("ship_address_no_changes"), lblNm("edit_shipping_address_properties"), lblNm("msg_ok"), function (argument) {});
                else
                    showNativeAlert(lblNm("ship_address_primaryfl_required"), lblNm("edit_shipping_address_properties"), lblNm("msg_ok"), function (argument) {});
            }
        },
        
        closeINVEditAddrFL: function(e) {
			$(e.currentTarget).parent().parent().parent().parent().hide();
		},
        
        close: function() {
        	this.unbind();
        	this.undelegateEvents();
        	this.$el.empty();
        }
	});

	return InvShipAddressView;
});


