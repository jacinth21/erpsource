/**********************************************************************************
 * File:        GmMainView.js
 * Description: Master Page for the whole application
 * Version:     1.0
 * Author:      ejayabalan
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone', 'handlebars', 'i18n!nls/GmLocale',
        'device', 'global', 'commonutil', 'notification', 'daosetup', 'daosearch',
        'itemcollection', 'itemlistview', 'loginview', 'msgmodel', 'jqueryslider', 'gmvImageCrop', 'gmvLocalDB'
    ],
    function($, _, Backbone, Handlebars, Locale, Device, Global, CommonUtil, Notification, DAOSetup, DAOSearch, Items, ItemListView, LoginView, MsgModel, JQuerySlider, GMVImageCrop, GMVLocalDB) {

        'use strict';
        var strMsgID, strMsgValue;

        var MainView = Backbone.View.extend({

            el: "body",

            events: {
                "click #btn-back": "back",
                "click #btn-home": "linkPage",
                "click #btn-logout,#btn-logout-menu": "confirmation",
                "click #btn-settings": "settings",
                "click .btn-spn-update": "update",
                "click #btn-updates, #li-update-count, #btn-update-menu": "getUpdates",
                "click .div-btn-header": "setHeaderButton",
                "click .div-btn-locale": "showLocalePanel",
                "click #div-main, #div-messagebar": "hidePanels",
                "change #div-messagebar": "populateMsgSlider",
                "click .div-msg-icon": "showMsgSlider",
                "click #div-msg-ok": "hideMsgSlider",
                "click #div-no, #div-ok": "hideconfirmation",
                "click #div-yes": "changepage",
                "click .region-locale": "setLocale",
                "click #spn-update-refresh": "refreshUpdates",
                "click #ul-updates .li-above": "toggleUpdateDetail",
                "click #btn-mobile-header-left-icon, #div-main,#div-header": "toogleMenu",
                "click .li-ipad-user-pic,#mobile-menu-top-user": "openPicturePopup",
                "click #li-LocalDB": "showLocalDB",
                "click #fa-close-alert": "closeMsgBar",
                "click #fa-send-report": "sendReport",
                "click #btn-sidebar-menu": "openSidebarMenu",
                "click #btn-close-sidebar-menu , .li-do-child-menus-mobile, #btn-home, #btn-logout,#btn-updates,#btn-logout-menu, #btn-settings, #btn-catalog, #btn-update-menu, #btn-collateral, #btn-guide, #ul-header-right-pic-menubar": "closeSidebarMenu"
            },

            /* Templates */
            template: fnGetTemplate(URL_Main_Template, "GmMain"),
            
            template_MenuBar: fnGetTemplate(URL_Main_Template, "GmMainMenuBar"),

            template_locale: fnGetTemplate(URL_Main_Template, "GmLocale"),

            template_logout: fnGetTemplate(URL_Logout_Template, "GmLogout"),

            template_message: fnGetTemplate(URL_Msg_Template, "GmMsg"),

            template_updates: fnGetTemplate(URL_Main_Template, "GmUpdates"),

            template_MainModule: fnGetTemplate(URL_Common_Template, "gmtCRMMainModule"),

            template_PendingSync: fnGetTemplate(URL_Common_Template, "gmtPendingSync"),
            
            template_SideMenu: fnGetTemplate(URL_Common_Template, "GmSideMenu"),

            initialize: function () {
                this.render();
                fnSetLangId();
                //            $("#div-LocalDB").hide();

            },

            render: function () {
                this.$el.html(this.template({
                    label: Locale
                }));

                /* Get the Module tab access based on the company while clicking the DO module,This is to avoid the logot/login functionality each time when we enable / disable the tab in DO Module
                 */
                var cmpid = localStorage.getItem("cmpid");
                if(navigator.onLine) {
                    fnGetRuleValues(cmpid, 'MODULEACCESS', cmpid, function (data) {
                         if (data != null) {
                            console.log(data.rulevalue);
                            localStorage.setItem('moduleaccess',data.rulevalue); 
                         }
                    });
                }
                
                console.log(localStorage.getItem("moduleaccess"));
                _.each(localStorage.getItem("moduleaccess").split(','), function (data) {
                    $("#" + data).addClass("hide");
                });

                if(GM.Global.DoValidation != undefined ){
                    showError(GM.Global.DoValidation);
                    GM.Global.DoValidation = undefined;
                }

                if(GM.Global.GuideFl == "Y" && GM.Global.GuideErr != undefined) {
                    showError(GM.Global.GuideErr);
                    GM.Global.GuideErr = undefined;
                }
                GM.Global.GuideFl = "N";
                
                
                $(".btn-app").each(function () {
                    var btnID = $(this).attr("id");
                    var btnData = '#' + btnID;
                    if (localStorage.getItem(btnID) == null)
                        $(btnData).addClass("hide");
                    else
                        $(btnData).removeClass("hide");
                });

                if (GM.Global.Device) {
                    if ($(window).width() <= 480) {
                                   $('.CRMoffline').addClass('hide');
                                }
                                else{
                                   $(".CRMoffline").removeClass("hide"); 
                                }
                    $(".CRMonline").addClass("hide");
                } else {
                    $(".CRMoffline").addClass("hide");
                    $(".CRMonline").removeClass("hide");
                }

                this.$("#div-locale").hide();
                this.$("#div-app-title").removeClass("gmVisibilityVisible").addClass("gmVisibilityHidden");
                $("#div-header").removeClass("background-2").addClass("background-1 background-image");
                $("#div-messagebar").addClass("background-1 background-image");

                onrefresh();
                 if ($(window).width() < 480) {
                    $("#li-update-count").addClass("hide");
                 }
                $("#div-footer").addClass("hide");
                
                return this;
            },

            back: function () {
                var intMinusVal = 2;

                //For Product Catalog
                if (pcRecentBreadCrumbs[pcRecentBreadCrumbs.length - intMinusVal] != undefined)
                    pcBreadcrumb = pcRecentBreadCrumbs[pcRecentBreadCrumbs.length - intMinusVal].slice(0);

                pcRecentBreadCrumbs.pop();
                pcRecentBreadCrumbs.pop();

                //For Marketing Collateral
                if (mcRecentBreadCrumbs[mcRecentBreadCrumbs.length - intMinusVal] != undefined)
                    mcBreadcrumb = mcRecentBreadCrumbs[mcRecentBreadCrumbs.length - intMinusVal].slice(0);

                mcRecentBreadCrumbs.pop();
                mcRecentBreadCrumbs.pop();

                window.history.back();
            },

            openSidebarMenu: function () {
                $("#SidebarMenu").empty();
                 var approvers = _.findWhere(GM.Global.UserAccess, {
                    funcid: "CRMADMIN-CRFAPPR"
                });
                var userTabs = GM.Global.UserTabs;
                
                $("#SidebarMenu").html(this.template_SideMenu());
                _.each(userTabs, function (data) {
                    if(data.title=="Sales Call"){
                       $('#scsidemenu').removeClass("hide"); 
                    }

                });
                $(".mobile-menu-top-user-name").html(localStorage.getItem("fName")+" "+localStorage.getItem("lName"));
                $(".btn-app-phone").each(function () {
                    console.log($(this))
                    var btnID = $(this).parent().attr("id");
                    console.log(btnID);
                    var btnData = '.' + btnID;
                    if (localStorage.getItem(btnID) == null)
                        $(btnData).addClass("hide");
                    else
                        $(btnData).removeClass("hide");
                });
                // open the child menu in the menu section if we click the module name / down arrow icon in the side menu section
                $("#mySidenav").addClass("width-nav");
                $(".sidenav a.fa-ic").unbind('click').bind('click', function () {
                    $(this).parent("li").siblings("li").children("ul").slideUp();
                    $(this).next("ul").slideToggle(300);
                });
                onrefresh();
//                 $("#ul-header-right-pic-menubar").find("img").removeAttr("onerror").css("display","inline-block");
//                 $("#ul-header-right-pic-menubar").find("img").attr("src",  GM.Global.UserPicID);
            },
            
            closeSidebarMenu: function(e) {
                $("#mySidenav").removeClass("width-nav");
                $(".gmFloatCenter").addClass("sidenav-block");
            },

            toogleMenu: function(event) {
                if (event.target.id == "ipad-menu-bar"){
                    $("#section-app-menu").removeClass("hide");
                    $("#section-app-menu").html(this.template_MenuBar()); 
                }
                else {
                    $("#section-app-menu").addClass("hide"); 
                }

            },
            openPicturePopup: function () {
                openPicturePopup(this, GMVImageCrop);
            },

            showLocalDB: function () {
                var gmvLocalDB = new GMVLocalDB();
//                 $("#div-LocalDB").removeClass("hide");
                $("#div-LocalDB").remove();
            },
			
              changepage: function () {
//                window.clearTimeout(UpdateTimer);
                $(".div-user-icon-pic img").attr("src", "");
                $("#msg-overlay").hide();
                $("#div-locale").removeClass("div-settings-locale");
                localStorage.removeItem("token");
                window.location.href = "#";
                          
            },
 


            //Displays the Logout Confirmation popup
            confirmation: function (event) {
                var that = this;
                if (!GM.Global.Browser) {
                    navigator.notification.confirm(
                        lblNm("msg_want_logout"), // message
                        function (buttonIndex) {
                            if (buttonIndex == '1') {
                                intLogin = 0;
                                intOfflineMode = 0;
                                that.changepage();
                            }
                        },
                        lblNm("msg_confirm"), // title
                    [lblNm("msg_yes"), lblNm("msg_no")] // buttonLabels
                    );
                    this.hidePanels(0);
                } else {
                    intLogin = 0;
                    intOfflineMode = 0;
                    that.changepage();
                }

            },

            //Hides the logout confirmation and stays logged in
            hideconfirmation: function () {
                $("#msg-overlay").fadeToggle("fast");
                $("#div-confirm-logout").fadeToggle("fast");
                $("#div-login-failed").fadeToggle("fast");
                this.$("#txt-userid").val("");
                this.$("#txt-password").val("");
                this.$("#txt-userid").focus();
                $(".div-btn-header").removeClass("btn-header-visited");
            },

            hideMsgSlider: function () {
                $("#msg-overlay").fadeToggle("fast");
            },

            //Hide all the Panels
            hidePanels: function () {
                $(".div-btn-header").removeClass("btn-header-visited");
                if (($('#div-locale').css('display') == 'block') && (window.top.location.hash != "#")) {
                    if (window.top.location.hash == "#settings") {
                        //					showMsgView("011");
                    }
                    // else{														
                    //    showMsgView("001");
                    // }
                }

                arrUpdatedDO = new Array();
                this.$("#div-updates").slideUp(100);
                this.$("#div-locale").fadeOut(100);
//                chkUpdate();
            },

            //Link the page to home page
            linkPage: function () {
                this.hidePanels();
                window.location.href = "#home";
            },

            //Load the Brief Message into the Message Popup
            populateMsgSlider: function (event) {
                if (!$("#div-popup").hasClass("sync"))
                    $("#div-popup").html("");
                var that = this;
                strMsgID = $(event.currentTarget).attr("msg-id");
                strMsgValue = $(event.currentTarget).attr("msg-value");
                fnGetMessages(strMsgID, function (data) {
                    if (data.Reason.length > 0 || data.Action.length > 0) {
                        that.$("#div-messagebar").append('<div class="div-msg-icon"></div>');
                    }
                });


            },

            getUpdates: function (event) {
                    try {
                        var that = this;
                        if (!$(event.currentTarget).hasClass("btn-updates-syncmode")) {
                            if ((navigator.onLine) && (!intOfflineMode)) {
                                if ($(".btn-quicklink").hasClass('hide')) {
                                    $(".panel").animate({
                                        left: "+=301"
                                    }, 100, function () {
                                        // Animation complete.
                                        $("#div-sidepanel").hide();
                                    });
                                    $(".btn-quicklink").removeClass('hide').addClass('show');
                                    window.location.href = "#closedSidePanel";
                                }
                                console.log("GmMainView : getUpdates >>>>>>>>>>")
                                $(".btn-quicklink").removeClass("btn-quicklink-light").addClass("btn-quicklink-dark");
//                                console.log($("#div-updates").text().trim());
//                                if ($("#div-updates").text().trim() != "Fetching Updates...") {
//                                    that.$("#div-updates").slideToggle(100);
//                                       console.log($("#div-updates").text().trim());
                                    that.$("#div-updates").html("<li class='li-info-center'><img />&nbsp;"+lblNm("msg_fetching_updates")+"</li>").slideDown(100);
//                                    window.clearTimeout(UpdateTimer);
                                    getServerUpdates(function (status) {
                                        console.log("GmMainView : getServerUpdates() >>>>>>>>>>")
                                        console.log(status);
                                        if (status == "Success") {
                                            that.showUpdatesPanel(event);
                                        } else if (status == "no data") {
                                            that.$("#div-updates").html("<li class='li-info-center'>"+lblNm("msg_no_updates")+"</li>");
                                            //                                    chkUpdate();
                                        } else {
                                            that.$("#div-updates").html("<li class='li-info-center'>"+lblNm("msg_error_fetching")+"</li>");
                                            //                                    chkUpdate();
                                        }
                                    });
//                                } else {
//                                    that.$("#div-updates").slideToggle(100);
//                                }
                                setTimeout(function () {
                                    console.log("setTimeout Fetching Updates...");
//                                   if ($("#div-updates").text().trim() != "Fetching Updates...") 
                                       $("#div-updates").slideUp(100);
                                }, 6000); // 1 min

                            }
                        }
                    } catch (err) {
                        showAppError(err, "GmMainView:getUpdates()");
                    }
            },

            refreshUpdates: function (event) {
                // this.$("#div-updates").slideToggle();
                var that = this;
                this.getUpdates(event);
            },

            //Set the Header buttonbackground indicating the current Header button touched
            setHeaderButton: function (e) {
                $(".div-btn-header").removeClass("btn-header-visited");
                $("#" + e.currentTarget.id).addClass("btn-header-visited");
            },

            //Set the locale language
            setLocale: function (e) {
                var targetElement = e.currentTarget;
                var strID = targetElement.id;
                localStorage.setItem('deviceLng', strID);
                $("#div-locale").empty();
                getLocaleData(function(callback){
//                    location.reload();
                     $("#div-locale").hide();
                    window.location.href = "#home";
//                    window.location.href = "#settings";
                });
            },

            //Move to the Settings Page
            settings: function () {
                this.hidePanels();
                window.location.href = "#settings";
            },

            //Display the locale panel
            showLocalePanel: function (event) {
                var that = this;
                var strLocaleName = localStorage.getItem("deviceLng");
                fnGetLocale(strLocaleName, function (data) {
                    if (data.length > 0) {
                        showMsgView("012");
                        that.$("#div-locale").html(that.template_locale(data));
                        that.$("#div-locale").toggle();
                        if (document.getElementById('div-locale').style.display == 'none') {
                            //						if(window.top.location.hash!="")																
                            //						   showMsgView("011");
                            // else
                            // 	showMsgView("001");
                        }
                    }
                });
            },

            //Syncs when the updates are done
            sync: function (element) {
                var that = this;
                var ws = element;
                var strUpdatedService = "",
                    code = eval(ws + ".code");
                var input = '';
                switch (ws) {
                    case "listGmSystemVO":
                        strUpdatedService = "SYSTEM";
                        break;

                    case "listGmSystemAttributeVO":
                        strUpdatedService = "SYSTEM";
                        break;

                    case "listGmSetMasterVO":
                        strUpdatedService = "SET";
                        break;

                    case "listGmSetPartVO":
                        strUpdatedService = "SET";
                        break;

                    case "listGmSetAttributeVO":
                        strUpdatedService = "SET";
                        break;

                    case "listGmPartNumberVO":
                        strUpdatedService = "PART";
                        break;

                    case "listGmPartAttributeVO":
                        strUpdatedService = "PART";
                        break;

                    case "listGmGroupVO":
                        strUpdatedService = "GROUP";
                        break;

                    case "listGmGroupPartVO":
                        strUpdatedService = "GROUP";
                        break;

                    case "listGmCodeLookupVO":
                        strUpdatedService = "APP";
                        break;

                    case "listGmFileVO":
                        strUpdatedService = "APP";
                        break;

                    case "listGmSecurityEventVO":
                        strUpdatedService = "APP";
                        break;

                    case "listGmPartyVO":
                        strUpdatedService = "CONTACT";
                        break;

                    case "listGmPartyContactVO":
                        strUpdatedService = "CONTACT";
                        break;

                    case "listGmSalesRepVO":
                        strUpdatedService = "SALESREP";
                        break;

                    case "listGmDistributorVO":
                        strUpdatedService = "DISTRIBUTOR";
                        break;

                    case "listGmAddressVO":
                        strUpdatedService = "ADDRESS";
                        break;

                    case "listGmAccountVO":
                        strUpdatedService = "ACCOUNT";
                        break;

                    case "listGmAcctGpoMapVO":
                        strUpdatedService = "ACCOUNT";
                        break;

                    case "listGmAcctGPOPriceVO":
                        strUpdatedService = "PRICE";
                        input = {};
                        input.acctid = that.arrAccid.join();
                        input.reftype = 4000524;
                        break;

                    case "listGmAcctPartPriceVO":
                        input = {};
                        input.acctid = that.arrAccid.join();
                        input.reftype = 4000526;
                        strUpdatedService = "PRICE";
                        break;

                    case "listGmUserVO":
                        strUpdatedService = "USER";
                        break;

                }
                boolUpdate = false;
//                if (!GM.Global.Browser)
//                    window.clearTimeout(timerSplashScreen);
                DataSync_SL(ws, input, function (status) {
                    that.i++;
                    fnClearUpdates(code, function (status) {
                        if (that.i < that.syncElements.length) {
                            that.sync(that.syncElements[that.i]);
                        } else {
                            boolUpdate = true;
                            fnTimerSplashScreen();
                            chkUpdate();
                            showMsgView("023", strUpdatedService);
                            that.hidePanels();
                        }

                        fnGetUpdates(function (length) {
                            $(".divUpdatesNo").html('');
                            $(".divUpdatesNo").html("&nbsp;" + length + "&nbsp;");
                            if (length == 0) {
                                that.hidePanels();
                                $(".divUpdatesNo").addClass("hide");
                            }
                        });
                    });
                });
            },

            showMsgSlider: function (event) {
                fnGetMessages(strMsgID, function (data) {
                    if (data.Reason.length > 0 || data.Action.length > 0) {
                        var model = new MsgModel(data);
                        if (strMsgValue != null) {
                            var strReplaceMsg = data.Message.replace('?', strMsgValue);
                            model.set({
                                Message: strReplaceMsg
                            });
                        }
                        var Message = data.Reason + ".\n" + data.Action;
                        var Title = data.Message;
                        showNativeAlert(Message, Title, "OK");
                    }
                });
            },

            //Displays Updates panel
         showUpdatesPanel: function (event) {
             try {
                 var that = this;
                 that.$("#div-updates").html(that.template_updates());
                 var elmUpdateAll = $("#div-update-header").find(".btn-spn-update");
                 var arrUpdateAll = new Array();
                 fnGetUpdatedSystem(function (data) {
                     if (data.length > 0) {

                         var idUpdateAll = $("#update-systems").find(".btn-spn-update").attr('id').split(',');
                         for (var i = 0; i < idUpdateAll.length; i++)
                             arrUpdateAll.push(idUpdateAll[i]);
                         $(elmUpdateAll).attr('id', arrUpdateAll.join(","));

                         var itemscol = new Items(data);
                         var itemlistview = new ItemListView({
                             el: $("#ul-updates-detail-systems"),
                             collection: itemscol,
                             columnheader: 0,
                             itemtemplate: "GmItem",
                             renderType: 0,
                             template_URL: URL_Main_Template
                         });
                         $("#update-systems").show();
                         $("#ul-updates-detail-systems").find('a').each(function () {
                             $(this).removeAttr("href");
                         });
                     } else {
                         $("#update-systems").hide();
                     }
                 });
                 fnGetUpdatedSet(function (data) {

                     if (data.length > 0) {


                         var idUpdateAll = $("#update-sets").find(".btn-spn-update").attr('id').split(',');
                         for (var i = 0; i < idUpdateAll.length; i++)
                             arrUpdateAll.push(idUpdateAll[i]);
                         $(elmUpdateAll).attr('id', arrUpdateAll.join(","));

                         var itemscol = new Items(data);
                         var itemlistview = new ItemListView({
                             el: $("#ul-updates-detail-sets"),
                             collection: itemscol,
                             columnheader: 0,
                             itemtemplate: "GmItem",
                             renderType: 0,
                             template_URL: URL_Main_Template
                         });
                         $("#update-sets").show();
                         $("#ul-updates-detail-sets").find('a').each(function () {
                             $(this).removeAttr("href");
                         });
                     } else {
                         $("#update-sets").hide();
                     }
                 });
                 fnGetUpdatedPart(function (data) {
                     if (data.length > 0) {


                         var idUpdateAll = $("#update-parts").find(".btn-spn-update").attr('id').split(',');
                         for (var i = 0; i < idUpdateAll.length; i++)
                             arrUpdateAll.push(idUpdateAll[i]);
                         $(elmUpdateAll).attr('id', arrUpdateAll.join(","));

                         var itemscol = new Items(data);
                         var itemlistview = new ItemListView({
                             el: $("#ul-updates-detail-parts"),
                             collection: itemscol,
                             columnheader: 0,
                             itemtemplate: "GmItem",
                             renderType: 0,
                             template_URL: URL_Main_Template
                         });
                         $("#update-parts").show();
                         $("#ul-updates-detail-parts").find('a').each(function () {
                             $(this).removeAttr("href");
                         });
                     } else {
                         $("#update-parts").hide();
                     }
                 });
                 fnGetUpdatedGroup(function (data) {
                     if (data.length > 0) {

                         var idUpdateAll = $("#update-groups").find(".btn-spn-update").attr('id').split(',');
                         for (var i = 0; i < idUpdateAll.length; i++)
                             arrUpdateAll.push(idUpdateAll[i]);
                         $(elmUpdateAll).attr('id', arrUpdateAll.join(","));

                         var itemscol = new Items(data);
                         var itemlistview = new ItemListView({
                             el: $("#ul-updates-detail-groups"),
                             collection: itemscol,
                             columnheader: 0,
                             itemtemplate: "GmItem",
                             renderType: 0,
                             template_URL: URL_Main_Template
                         });
                         $("#update-groups").show();
                         $("#ul-updates-detail-groups").find('a').each(function () {
                             $(this).removeAttr("href");
                         });
                     } else {
                         $("#update-groups").hide();
                     }
                 });
                 fnGetUpdatedFile(function (data) {
                     if (data.length > 0) {
                         var idUpdateAll = $("#update-files").find(".btn-spn-update").attr('id').split(',');
                         for (var i = 0; i < idUpdateAll.length; i++)
                             arrUpdateAll.push(idUpdateAll[i]);
                         $(elmUpdateAll).attr('id', arrUpdateAll.join(","));

                         var itemscol = new Items(data);
                         var itemlistview = new ItemListView({
                             el: $("#ul-updates-detail-files"),
                             collection: itemscol,
                             columnheader: 0,
                             itemtemplate: "GmItem",
                             renderType: 0,
                             template_URL: URL_Main_Template
                         });
                         $("#update-files").show();
                         $("#ul-updates-detail-files").find('a').each(function () {
                             $(this).removeAttr("href");
                         });
                     } else {
                         $("#update-files").hide();
                     }
                 });
                 fnGetUpdatedContact(function (data) {
                     if (data.length > 0) {


                         var idUpdateAll = $("#update-contacts").find(".btn-spn-update").attr('id').split(',');
                         for (var i = 0; i < idUpdateAll.length; i++)
                             arrUpdateAll.push(idUpdateAll[i]);
                         $(elmUpdateAll).attr('id', arrUpdateAll.join(","));

                         var itemscol = new Items(data);
                         var itemlistview = new ItemListView({
                             el: $("#ul-updates-detail-contacts"),
                             collection: itemscol,
                             columnheader: 0,
                             itemtemplate: "GmItem",
                             renderType: 0,
                             template_URL: URL_Main_Template
                         });
                         $("#update-contacts").show();
                         $("#ul-updates-detail-contacts").find('a').each(function () {
                             $(this).removeAttr("href");
                         });
                     } else {
                         $("#update-contacts").hide();
                     }
                 });
                 fnGetUpdatedApp(function (data) {
                     if (data > 0) {


                         var idUpdateAll = $("#update-apps").find(".btn-spn-update").attr('id').split(',');
                         for (var i = 0; i < idUpdateAll.length; i++)
                             arrUpdateAll.push(idUpdateAll[i]);
                         $(elmUpdateAll).attr('id', arrUpdateAll.join(","));

                         $("#update-apps").show();
                         $("#update-apps").find("#spn-app-count").html("- " + data);
                     } else {
                         $("#update-apps").hide();
                     }
                 });
                 fnGetUpdatedSalesRep(function (data) {
                     if (data.length > 0) {


                         var idUpdateAll = $("#update-salesrep").find(".btn-spn-update").attr('id').split(',');
                         for (var i = 0; i < idUpdateAll.length; i++)
                             arrUpdateAll.push(idUpdateAll[i]);
                         $(elmUpdateAll).attr('id', arrUpdateAll.join(","));

                         var itemscol = new Items(data);
                         var itemlistview = new ItemListView({
                             el: $("#ul-updates-detail-salesrep"),
                             collection: itemscol,
                             columnheader: 0,
                             itemtemplate: "GmItem",
                             renderType: 0,
                             template_URL: URL_Main_Template
                         });
                         $("#update-salesrep").show();
                         $("#ul-updates-detail-salesrep").find('a').each(function () {
                             $(this).removeAttr("href");
                         });
                     } else {
                         $("#update-salesrep").hide();
                     }
                 });

                 // fn to display the info of updated Associate Sales Rep Records
                 fnGetUpdatedASSalesRep(function (data) {
                     if (data.length > 0) {
                         var idUpdateAll = $("#update-assalesrep").find(".btn-spn-update").attr('id').split(',');
                         for (var i = 0; i < idUpdateAll.length; i++)
                             arrUpdateAll.push(idUpdateAll[i]);
                         $(elmUpdateAll).attr('id', arrUpdateAll.join(","));
                         var itemscol = new Items(data);
                         var itemlistview = new ItemListView({
                             el: $("#ul-updates-detail-assalesrep"),
                             collection: itemscol,
                             columnheader: 0,
                             itemtemplate: "GmItem",
                             renderType: 0,
                             template_URL: URL_Main_Template
                         });
                         $("#update-assalesrep").show();
                         $("#ul-updates-detail-assalesrep").find('a').each(function () {
                             $(this).removeAttr("href");
                         });
                     } else
                         $("#update-assalesrep").hide();
                 });
                 fnGetUpdatedDistributor(function (data) {
                     if (data.length > 0) {


                         var idUpdateAll = $("#update-distributor").find(".btn-spn-update").attr('id').split(',');
                         for (var i = 0; i < idUpdateAll.length; i++)
                             arrUpdateAll.push(idUpdateAll[i]);
                         $(elmUpdateAll).attr('id', arrUpdateAll.join(","));

                         var itemscol = new Items(data);
                         var itemlistview = new ItemListView({
                             el: $("#ul-updates-detail-distributor"),
                             collection: itemscol,
                             columnheader: 0,
                             itemtemplate: "GmItem",
                             renderType: 0,
                             template_URL: URL_Main_Template
                         });
                         $("#update-distributor").show();
                         $("#ul-updates-detail-distributor").find('a').each(function () {
                             $(this).removeAttr("href");
                         });
                     } else {
                         $("#update-distributor").hide();
                     }
                 });
                 fnGetUpdatedAddress(function (data) {
                     if (data.length > 0) {


                         var idUpdateAll = $("#update-address").find(".btn-spn-update").attr('id').split(',');
                         for (var i = 0; i < idUpdateAll.length; i++)
                             arrUpdateAll.push(idUpdateAll[i]);
                         $(elmUpdateAll).attr('id', arrUpdateAll.join(","));

                         var itemscol = new Items(data);
                         var itemlistview = new ItemListView({
                             el: $("#ul-updates-detail-address"),
                             collection: itemscol,
                             columnheader: 0,
                             itemtemplate: "GmItem",
                             renderType: 0,
                             template_URL: URL_Main_Template
                         });
                         $("#update-address").show();
                         $("#ul-updates-detail-address").find('a').each(function () {
                             $(this).removeAttr("href");
                         });
                     } else {
                         $("#update-address").hide();
                     }
                 });
                 fnGetUpdatedAccount(function (data) {
                     if (data.length > 0) {


                         var idUpdateAll = $("#update-accounts").find(".btn-spn-update").attr('id').split(',');
                         for (var i = 0; i < idUpdateAll.length; i++)
                             arrUpdateAll.push(idUpdateAll[i]);
                         $(elmUpdateAll).attr('id', arrUpdateAll.join(","));

                         var itemscol = new Items(data);
                         var itemlistview = new ItemListView({
                             el: $("#ul-updates-detail-accounts"),
                             collection: itemscol,
                             columnheader: 0,
                             itemtemplate: "GmItem",
                             renderType: 0,
                             template_URL: URL_Main_Template
                         });
                         $("#update-accounts").show();
                         $("#ul-updates-detail-accounts").find('a').each(function () {
                             $(this).removeAttr("href");
                         });
                     } else {
                         $("#update-accounts").hide();
                     }
                 });
//                 fnGetUpdatedActivity(function (data) {
//                     if (data.length > 0) {
//                         var idUpdateAll = $("#update-activity").find(".btn-spn-update").attr('id').split(',');
//                         for (var i = 0; i < idUpdateAll.length; i++)
//                             arrUpdateAll.push(idUpdateAll[i]);
//                         $(elmUpdateAll).attr('id', arrUpdateAll.join(","));
//
//                         var itemscol = new Items(data);
//                         var itemlistview = new ItemListView({
//                             el: $("#ul-updates-detail-activity"),
//                             collection: itemscol,
//                             columnheader: 0,
//                             itemtemplate: "GmItem",
//                             renderType: 0,
//                             template_URL: URL_Main_Template
//                         });
//                         $("#update-activity").show();
//                         $("#ul-updates-detail-activity").find('a').each(function () {
//                             $(this).removeAttr("href");
//                         });
//                     } else {
//                         $("#update-activity").hide();
//                     }
//                 });

                 fnGetUpdatedSurgeon(function (data) {
                     if (data.length > 0) {
                         var idUpdateAll = $("#update-surgeon").find(".btn-spn-update").attr('id').split(',');
                         for (var i = 0; i < idUpdateAll.length; i++)
                             arrUpdateAll.push(idUpdateAll[i]);
                         $(elmUpdateAll).attr('id', arrUpdateAll.join(","));

                         var itemscol = new Items(data);
                         var itemlistview = new ItemListView({
                             el: $("#ul-updates-detail-surgeon"),
                             collection: itemscol,
                             columnheader: 0,
                             itemtemplate: "GmItem",
                             renderType: 0,
                             template_URL: URL_Main_Template
                         });
                         $("#update-surgeon").show();
                         $("#ul-updates-detail-surgeon").find('a').each(function () {
                             $(this).removeAttr("href");
                         });
                     } else {
                         $("#update-surgeon").hide();
                     }
                 });

                 fnGetUpdatedPrice(function (data) {
                     if (data.length > 0) {
                         var idUpdateAll = $("#update-price").find(".btn-spn-update").attr('id').split(',');
                         for (var i = 0; i < idUpdateAll.length; i++)
                             arrUpdateAll.push(idUpdateAll[i]);
                         $(elmUpdateAll).attr('id', arrUpdateAll.join(","));

                         var itemscol = new Items(data);
                         var itemlistview = new ItemListView({
                             el: $("#ul-updates-detail-price"),
                             collection: itemscol,
                             columnheader: 0,
                             itemtemplate: "GmItem",
                             renderType: 0,
                             template_URL: URL_Main_Template
                         });
                         $("#update-price").show();
                         $("#ul-updates-detail-price").find('a').each(function () {
                             $(this).removeAttr("href");
                         });
                     } else {
                         $("#update-price").hide();
                     }
                 });
            /*
              fnGetUpdatedTag(): This function is used to get the Tag details from T2001 Master sync table.
              Author: Karthik Somanathan
             */
                 fnGetUpdatedTag(function (data) {
					 if (data != undefined && data != "") { 
						 if (data.length > 0) {

							 var idUpdateAll = $("#update-tag").find(".btn-spn-update").attr('id').split(',');
							 for (var i = 0; i < idUpdateAll.length; i++)
								 arrUpdateAll.push(idUpdateAll[i]);
							 $(elmUpdateAll).attr('id', arrUpdateAll.join(","));

							 var itemscol = new Items(data);
							 var itemlistview = new ItemListView({
								 el: $("#ul-updates-detail-tag"),
								 collection: itemscol,
								 columnheader: 0,
								 itemtemplate: "GmItem",
								 renderType: 0,
								 template_URL: URL_Main_Template
							 });
							 $("#update-tag").show();
							 $("#ul-updates-detail-tag").find('a').each(function () {
								 $(this).removeAttr("href");
							 });
						 } else {
							 $("#update-tag").hide();
						 }
					 }
                 });
                 
                 fnGetUpdatedUser(function (data) {
                     if (data.length > 0) {


                         var idUpdateAll = $("#update-user").find(".btn-spn-update").attr('id').split(',');
                         for (var i = 0; i < idUpdateAll.length; i++)
                             arrUpdateAll.push(idUpdateAll[i]);
                         $(elmUpdateAll).attr('id', arrUpdateAll.join(","));

                         var itemscol = new Items(data);
                         var itemlistview = new ItemListView({
                             el: $("#ul-updates-detail-user"),
                             collection: itemscol,
                             columnheader: 0,
                             itemtemplate: "GmItem",
                             renderType: 0,
                             template_URL: URL_Main_Template
                         });
                         $("#update-user").show();
                         $("#ul-updates-detail-user").find('a').each(function () {
                             $(this).removeAttr("href");
                         });
                     } else {
                         $("#update-user").hide();
                     }
                 });
             } catch (err) {
                 showAppError(err, "GmMainView:showUpdatesPanel()");
             }

         },

            //Toggles the Updates Detail in the updates panel
            toggleUpdateDetail: function (event) {
                if (!$(event.originalEvent.target).hasClass("btn-spn-update")) {
                    var strulID = $(event.currentTarget).find(".spn-detail").attr("id").replace("btn", "ul-updates");
                    var parent = $(event.currentTarget);
                    $("#" + strulID).slideToggle("fast");
                    if ($(parent).hasClass("li-border-bottom")) {
                        $(parent).removeClass("li-border-bottom");
                        $(event.currentTarget).find(".spn-detail").removeClass("arrow-down").addClass("arrow-right");
                    } else {
                        $(parent).addClass("li-border-bottom");
                        $(event.currentTarget).find(".spn-detail").removeClass("arrow-right").addClass("arrow-down");
                    }
                }
            },

            //Starts updating the available updates
            update: function (event) {
                var that = this;
                that.arrAccid = new Array();

                $("#ul-updates-detail-price").find('a').each(function() {
                    that.arrAccid.push(this.id);
                });
                showMsgView("022");

                this.i = 0;
                this.syncElements = $(event.currentTarget).attr("id").split(',');
                $('.curr-updating').removeClass("curr-updating");
                $(event.currentTarget).addClass("curr-updating");



                $(".li-above").each(function () {
                    if (!$(this).find('.btn-spn-update').hasClass('curr-updating')) {
                        var id = ($(this).attr('id')).replace('update-', '');
                        $("#ul-updates-detail-" + id).hide();
                        $(this).hide();
                    }
                });

//                window.clearTimeout(UpdateTimer);
                $(event.currentTarget).hide();
                $(event.currentTarget).parent().find('img').show();



                if ($(event.currentTarget).parent().attr('id') == 'div-update-header') {
                    that.$("#div-updates").html("<li class='li-info-center'><img />&nbsp;"+lblNm("msg_updating_all")+"</li>");
                }

                this.sync(this.syncElements[this.i]);
            },

            closeMsgBar: function () {
                hideMessages();
            },

            getPendSync: function () {
                var that = this;
                $(".gmTab").removeClass().addClass("gmTab");
                $("#div-crm-module").html(this.template_MainModule({
                    module: "PendSync"
                }))
                $(".gmPanelTitle").html(lblNm("msg_data_pending"));
                var dispData = { activity: [], surgeon: [], favcrt: [], act: "", srg: "", crt: "", empty: "", alen: 0, slen: 0, flen: 0 };
                fnPendInfoActivity(function(data) {
                    dispData.activity = data;
                    dispData.alen = data.length;
                    if (data.length)
                        dispData.act = "Y";

                    fnPendInfoSurgeon(function(data) {
                        dispData.surgeon = data;
                        dispData.slen = data.length;
                        if (data.length)
                            dispData.srg = "Y";
                        fnPendInfoCriteria(function(data) {
                            dispData.favcrt = data;
                            dispData.flen = data.length;
                            if (data.length)
                                dispData.crt = "Y";
                            if (dispData.act == "" && dispData.srg == "" && dispData.crt == "")
                                $("#div-main-content").html(that.template_PendingSync({
                                    empty: "Y"
                                }));
                            else
                                $("#div-main-content").html(that.template_PendingSync(dispData));
                        });
                    });
                });
            },

            // Send email the app errors 
            sendReport: function(e) {
                var that = this;
                var fileName, fileType, mailTo, mailCC, mailsub, mailAttach, mailContent, quality;
                fileName = "screenshot";
                fileType = "jpg";
                quality = "100";
                getScreenShot(fileType, quality, fileName, function(fileURL) {
                    mailTo = gmReportToMail;
                    mailCC = "";
                    mailsub = localStorage.getItem('fName') + " " + localStorage.getItem('lName') + lblNm("msg_s_globus_error");
                    mailAttach = fileURL;
                    mailContent = lblNm("msg_user_name") + localStorage.getItem('userName') + lblNm("msg_device_model") + "<br>" + deviceModel + lblNm("msg_app_version") + "<br>"+ version +"<br>"+ lblNm("msg_device_version")+ deviceVersion + "<br>" + lblNm("msg_device_id") + deviceUUID + "<br> <br> <br>" + $("#error-report-content").text();
                    sendMail(mailTo, mailCC, mailsub, mailAttach, mailContent);
                });
            }
            
        });

        return MainView;
    });
