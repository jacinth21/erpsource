/**********************************************************************************
 * File:        GmImageCropView.js
 * Description: 
 * Version:     1.0
 * Author:     	praja
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device', 'jcrop'
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Jcrop) {

	'use strict';
	var jcrop_api;
	var ImageCropView = Backbone.View.extend({

		events : {
			 "click #div-crop" : "cropImage",
			 "click #div-crop-cancel" : "cancel"
		},

		/* Load the templates */ 
		template: fnGetTemplate(URL_Common_Template, "GmImageCrop"),
		
		el: '#div-image-viewer-background',
		
		initialize: function(options) {
			this.parent = options.parent;
			this.src = options.src;
			this.callback = options.callback;
			this.render();	
		},

		// Render the initial contents when the View is instantiated
		render: function () {
			this.$el.html(this.template());
			this.$('#cropbox').attr("src",this.src);
			this.$el.show();
			var that = this;
			
			$('#cropbox').Jcrop({
				allowSelect: false,
				bgColor:     'black',
	            bgOpacity:   .4,
	            setSelect:   [ 1200, 1200, 200, 200 ],
	            boxWidth: 650, 
	            boxHeight: 500,
	            onChange : that.updatePreview
			},function(){
			    jcrop_api = this;
			  });

			
		},
		
		updatePreview: function(c){

			if(parseInt(c.w) > 0) {
				var imageObj = $('#cropbox')[0];
				var canvas = $("#preview")[0];
				canvas.width = c.w;
				canvas.height = c.h;
				var context = canvas.getContext("2d");
				context.drawImage(imageObj, c.x, c.y, c.w, c.h, 0, 0, c.w, c.h);

			}

		},
		
		cropImage: function() {
			var canvas = this.$("#preview")[0];
			jcrop_api.destroy();
			console.log(canvas.toDataURL("image/jpeg", 1));
			if(this.callback!=undefined)
				this.callback(canvas.toDataURL("image/jpeg", 1));
			this.close();
		},
		
		cancel: function() {
			jcrop_api.destroy();
			this.close();
		},

		close: function() {
			this.$el.hide();
			this.unbind();
			this.undelegateEvents();
		}
	});	

	return ImageCropView;
});