/**********************************************************************************
 * File:        GmImageCropView.js
 * Description: 
 * Version:     1.0
 * Author:     	praja
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global', 'jcrop',

    	// Pre-Compiled Template
    	'gmtTemplate'
        ], 

        function ($, _, Backbone, Handlebars, Global, Jcrop, GMTTemplate) {

	'use strict';
	var jcrop_api;
	GM.Global.gmvImageCrop = Backbone.View.extend({

		events : {
			 "click #div-crop-user" : "cropImage",
			 "click #div-crop-cancel" : "cancel"
		},

		/* Load the templates */ 
		template: fnGetTemplate(URL_Common_Template, "gmtImageCrop"),
		
		
		initialize: function(options) {
			this.parent = options.parent;
            this.el = options.el;
            this.src = options.src;
			this.callback = options.callback;
			this.render();	
		},

		// Render the initial contents when the View is instantiated
		render: function () {
			this.$el.html(this.template());
            this.$('#cropbox-user').attr("src",this.src);
			this.$el.show();
			var that = this;
                $('#cropbox-user').Jcrop({
                    allowSelect: false,
                    bgColor:     'black',
                    bgOpacity:   .4,
                    setSelect:   [  10, 10, 100, 100 ],
                    boxWidth: 300, 
                    boxHeight: 300,
                    aspectRatio: 1,
                    onChange : that.updatePreview
                },function(){
                    jcrop_api = this;
                });
		},    
		
		updatePreview: function(c){
			if(parseInt(c.w) > 0) {
				var imageObj = $('#cropbox-user')[0];
				var canvas = $("#preview-user")[0];
				canvas.width = c.w;
				canvas.height = c.h;
				var context = canvas.getContext("2d");
				context.drawImage(imageObj, c.x, c.y, c.w, c.h, 0, 0, c.w, c.h);
			}
		},
  
		cropImage: function() {
			var canvas =  $("#preview-user")[0];
			jcrop_api.destroy();
			if(this.callback!=undefined)
				this.callback(canvas.toDataURL("image/jpeg", 1));
			this.close();
		},
		
		cancel: function() {
            this.$el.parent().parent().find("input[type=file]").val("");
            this.$el.html("");
			jcrop_api.destroy();
			this.close();
		},

		close: function() {
			this.unbind();
			this.undelegateEvents();
		}
	});	

	return GM.Global.gmvImageCrop;
});
