//This initDHTMLXValut() definition used for initialize dhtmlxVault file drag and drop div element and supports file upload, delete, download process.
/*
 * divRef - Reference Id for div element in HTML 
 * uploadURL - URL to upload file into directory
 * saveInfoURL - URL to save uploaded file details into DB 
 * fetchInfoURL - URL to fetch saved file details from DB
 * doUpdateFlURL - URL for update DO flag on t501 table when file delete
 * viewMode - display files on vault with specific view [list or grid]
 * scaleFactor - to mention the quality of preview image
 * autoSendFl - this flag used to send upload file to server automatically based on flag value[true or false]
 * divWidth - Width of vault
 * divHeight - Height of vault
 * refID - Order Id for file upload
 * refSubtype - DO document upload[26230732]
 * refGroupType - DO[26240412]
 * fileDetArr - uploaded file details used to parse initialization
 * deleteInfoURL - URL for file delete
 * AccessFl - User access flag
 * fileNameArr - Uploaded file names to check for validation
 * strDtFormat - Company date format
 * strCompTimeZone - Company time zone
 * uploadPath - directory to store file on server
 * strUserid - login user id
*/
function initDHTMLXValut(divRef, uploadURL,uploadPath,saveInfoURL,fileURL,fetchInfoURL,cmnFileUploadVO,downloadURL,doUpdateFlURL,fileDetArr,fileNameArr,AccessFl,refID,refGroupType,refType,refSubtype,viewMode,divWidth,divHeight, strUserid) {

    var fileInfoArr = fileDetArr;
    
	if (divWidth != "") {
		$("#" + divRef).height(divWidth);
	}
	if (divHeight != "") {
		$("#" + divRef).height(divHeight);
	}

	//initialization of vault to display file drag and drop screen
	var vault = new dhx.Vault(divRef, {
		uploader: {
			target: uploadURL,
			autosend: true,
			params: {
				uploadDir: uploadPath,
				token: localStorage.getItem("token"),
				strRefId: refID,
                locale : localStorage.getItem("locale")
			}
		},
        downloadURL: downloadURL,
        mode: viewMode
	});

    console.log("fileDetArr " + JSON.stringify(fileDetArr));
    
    if (AccessFl != 'Y') {
        vault.toolbar.data.remove("add");
        vault.toolbar.data.remove("remove-all");
    }
    
	vault.data.parse(fileDetArr);
    
    //This events is called when file is added to upload
	vault.events.on("BeforeAdd", function (file) {
		var inputFileName = file.file.name;
		var extension = file.file.name.split(".").pop().toLowerCase();
		var filePredicate = true;
		var size = file.file.size;
		var sizeLimit = '20000000'; // maximum file size limits
		if (AccessFl == 'Y') {
			if (fileNameArr.length > 0) {
				for (var i = 0; i < fileNameArr.length; i++) {
					if (inputFileName == fileNameArr[i]) {
						filePredicate = false;
						break;
					}
				}
				// this condition checks file name flag and shows validation when file name already exist 
				if (!filePredicate) {
					dhx.message({
						text: "File " + inputFileName + " already exist, please upload new file",
						css: "dhx-error",
						expire: 6000
					});

					return filePredicate;
				}

			}
			// this condition checks file extension flag and shows validation when the extension is not allowed
			if (filePredicate) {
				//allowed file extension
				filePredicate = (extension == "png" || extension == "txt" || extension == "docx" || extension == "pdf" || extension == "xlsx" || extension == "xls" || extension == "doc" || extension == "jpg" || extension == "msg" || extension == "jpeg");
				if (!filePredicate) {
					dhx.message({
						text: "Invalid file format, please upload valid file",
						css: "dhx-error",
						expire: 6000
					});
				}
			}
			// this condition checks file size flag and shows validation when the size greater than the limit
			if (filePredicate) {
				filePredicate = size < sizeLimit;
				if (!filePredicate) {
					dhx.message({
						text: "File size is greater than the limit, upload valid file",
						css: "dhx-error",
						expire: 6000
					});
				}
			}
		}
		else {
			filePredicate = false;
		}

		return filePredicate;
	});
    
	//This events is called when new file is uploaded to list
	vault.events.on("UploadFile", function (file) {  
		var fileid = file.id;
		file.link = file.name;
		var fileName = file.name;
		var fileSize = file.size;
		var fileTitle = fileName.substr(0, fileName.lastIndexOf('.'));
        
        var fileSavInput = {
			refid: refID, // ORder Id
			userid: strUserid, // Login user id
			filename: fileName, // File name 
			reftype: refType, // DO
			type: refSubtype, // DO document upload
			refgroup: refGroupType, // DO
			filesize: fileSize, // File size
		};

		fnSaveFileInfo(fileSavInput, saveInfoURL,cmnFileUploadVO, function (dt) {
            var doUpdateInput = {
				refid: refID, // Order Id
				userid: strUserid, // Login user id
				deletefl: 'Y'   // Delete flag value
			}
			// this condition is used to update the DO flag in t501 table if the type is do document
			if (refSubtype == '26230732') { // Do document upload
				fnDoUpdateFlag(doUpdateInput, doUpdateFlURL); // to update do flag in t501 table
			}
            var getFileInput = {
                    refgroup: refGroupType, // DO
                    refid: refID, // Order Id
                    reftype: refType, // DO
                    type: refSubtype, // DO
                    token: localStorage.getItem("token")
            };
            fnGetUploadedFiles(getFileInput,fetchInfoURL,cmnFileUploadVO,function(arrServerFile,strArrFileNames){
                vault.data.parse(arrServerFile); 
                fileInfoArr = arrServerFile; // serverFiles info stored to fileInfoArr to use in remove all event
                fileNameArr = strArrFileNames; // serverFileName info stored to fileNameArr to use in file name duplicate check
            });
        });
	});
    
    //This events is called when all files are removed from uploaded list
	vault.events.on("removeAll", function () {
		var successFl = false;
        var removeAllAccessFl = "";
        _.each(fileInfoArr,function(item){
            if(strUserid != item.userid)
                removeAllAccessFl = 'Y';
        });
        if(removeAllAccessFl != 'Y'){
            dhtmlx.confirm({
                title: "Confirm",
                ok: "Yes",
                cancel: "No",
                type: "confirm",
                text: "Are you sure you want to delete all files?",
                callback: function (result) {
                    if (result == true) {
                        console.log(fileInfoArr);
                        if (fileInfoArr.length > 0) {
                            for (var i = 0; i < fileInfoArr.length; i++) {
                                var fileDelInput = {
                                    "gmCommonFileUploadVO": [{
                                        fileid: fileInfoArr[i].filelistid, // int, internal file ID
                                        updatedby: fileInfoArr[i].userid
                                    }]
                                };

                                fnDeleteInfo(fileDelInput, fileURL, function (dt) {
                                    fileInfoArr = [];
                                    fileNameArr = [];
                                    vault.data.parse(fileInfoArr);
                                });
                            }
                        }
                    }
                    else {
                        vault.data.parse(fileInfoArr);
                    } 
                }
            });
            vault.data.parse(fileInfoArr);
        }
        else{
            dhtmlx.alert({
                title: "File Remove",
                type: "alert-error",
                text: "You don't have enough permission to delete all files."
            });
            vault.data.parse(fileInfoArr);
        }

	});
    
        
    //This events is called when file is removed from uploaded list
    vault.events.on("BeforeRemove", function (file) {
        // file is an object with details
        var fileid = file.filelistid;
        var filename = file.name;
        var userId = localStorage.getItem("userID");
        var inputdata = {
            "gmCommonFileUploadVO": [{fileid: fileid, // int, internal file ID
            updatedby: userId}]
        };
        // your code here, for example
        if (userId == file.userid) {
            dhtmlx.confirm({
                title: "Confirm",
                ok: "Yes",
                cancel: "No",
                type: "confirm",
                text: "Are you sure you want to delete " + file.name + "?",
                callback: function (result) {
                    if (result == true) {
                        fnDeleteInfo(inputdata,fileURL,function(data){
                            if (fileNameArr.length > 0) {
                                for (var i = 0; i < fileNameArr.length; i++) {
                                    if (fileNameArr[i] == filename) {
                                        fileNameArr.splice(i, 1);
                                        break;
                                    }
                                }
                            }
                            if (fileInfoArr.length > 0) {
                                for (var i = 0; i < fileInfoArr.length; i++) {
                                    if (fileInfoArr[i].name == filename) {
                                        fileInfoArr.splice(i, 1);
                                        vault.data.parse(fileInfoArr);
                                        break;
                                    }
                                }
                            }
                        });
                    }
                }
            });
        }
        else{
            dhtmlx.alert({
                title: "File Remove",
                type: "alert-error",
                text: "You don't have enough permission to delete the file."
            });
        }
        return false;
    });
}

/*This method used to delete selected files in uploaded list*/
function fnDeleteInfo(inputdata, fileURL, callback) {
    fnGetWebServerData(fileURL, undefined, inputdata, function (data) {
        console.log("Data>>> " + JSON.stringify(data));
        callback(data);
    }, true);
}

/* This function used save uploaded files details into database*/
function fnSaveFileInfo(inputdata, fileURL, voname, callback) {
	fnGetWebServerData(fileURL, voname, inputdata, function (data) {
        console.log("Data>>> " + JSON.stringify(data));
        callback(data);
    }, true);
}

/* This function used save uploaded files details into database*/
function fnDoUpdateFlag(inputdata, fileURL) {
	fnGetWebServerData(fileURL, undefined, inputdata, function (data) {
    }, true);
}


/* This function used get uploaded files for selected orderid*/
function fnGetUploadedFiles(inputdata, inputURL, voname, callback) {
	var that = this;
	var arrServerFile = [];
	var strArrFileNames = [];
    
    fnGetWebServerData(inputURL, voname, inputdata, function (data) {
            if (data != null) {
                for (var i = 0; i < data.length; i++) {
                    console.log(JSON.stringify(data));
                    var fileObj = data[i];
                    arrServerFile.push({
                        "name": fileObj.filename, // Uploaded file name
                        "size": fileObj.filesize, // Uploaded file size
                        "link": fileObj.filename, // Uploaded file download link
                        "status": "uploaded", // Status of uploaded file
                        "filelistid": fileObj.fileid, // File list of uploaded file
                        "userid": fileObj.userid // userid
                    });
                }
                strArrFileNames.push(fileObj.filename);
            }
            callback(arrServerFile,strArrFileNames);
			console.log("arrServerFile=>" + arrServerFile.length);
    });

}

//this method used to convert file size into respective storage units
function fnGetVaultFileSize(fileSize)
{
    var inputFileSize = fileSize;
        var c = false;
        var a = ["B", "KB", "MB", "GB", "TB", "PB", "EB"];
        for (var e = 0; e < a.length; e++) {
            if (inputFileSize > 1024) {
                inputFileSize = inputFileSize / 1024
            } else {
                if (c === false) {
                    c = e
                }
            }
        }
        if (c === false) {
            c = a.length - 1
        }
        return Math.round(inputFileSize * 100) / 100 + " " + a[c];
}


// this function used to load file icon type 
function getFileTypeIcon(t) {
    switch (t) {
        case "jpg":
        case "jpeg":
        case "gif":
        case "png":
        case "bmp":
        case "tiff":
        case "pcx":
        case "svg":
        case "ico":
            return "fa-file-image-o icon-color-img";
        case "avi":
        case "mpg":
        case "mpeg":
        case "rm":
        case "move":
        case "mov":
        case "mkv":
        case "flv":
        case "f4v":
        case "mp4":
        case "3gp":
        case "wmv":
        case "webm":
        case "vob":
            return "fa-file-video-o";
        case "rar":
        case "zip":
        case "tar":
        case "tgz":
        case "arj":
        case "gzip":
        case "bzip2":
        case "7z":
        case "ace":
        case "apk":
        case "deb":
        case "zipx":
        case "cab":
        case "tar-gz":
        case "rpm":
        case "xar":
            return "fa-file-archive-o";
        case "xlr":
        case "xls":
        case "xlsm":
        case "xlsx":
        case "ods":
        case "csv":
        case "tsv":
            return "fa-file-excel-o icon-color-excel";
        case "doc":
        case "docx":
        case "docm":
        case "dot":
        case "dotx":
        case "odt":
        case "wpd":
        case "wps":
        case "pages":
            return "fa-file-word-o icon-color-word";
        case "wav":
        case "aiff":
        case "au":
        case "mp3":
        case "aac":
        case "wma":
        case "ogg":
        case "flac":
        case "ape":
        case "wv":
        case "m4a":
        case "mid":
        case "midi":
            return "fa-file-audio-o";
        case "pot":
        case "potm":
        case "potx":
        case "pps":
        case "ppsm":
        case "ppsx":
        case "ppt":
        case "pptx":
        case "pptm":
        case "odp":
        case "psd":
            return "fa-file-powerpoint-o";
        case "html":
        case "htm":
        case "eml":
            return "fa-globe";
        case "exe":
            return "fa-desktop";
        case "dmg":
            return "fa-apple";
        case "pdf":
        case "ps":
        case "eps":
            return "fa-file-pdf-o icon-color-pdf";
        case "txt":
        case "djvu":
        case "nfo":
        case "xml":
            return "fa-file-text-o icon-color-text";
        case "msg":
        case "ost":
        case "pst":
            return "fa-envelope-o icon-color-message";
        default:
            return "fa-file-o"
    }
};
