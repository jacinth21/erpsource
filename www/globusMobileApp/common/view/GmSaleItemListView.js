/**********************************************************************************
 * File:        GmSaleItemListView.js
 * Description: This view contains the list items required for MSD, MDO asd CSM. (An improved copy of ItemListView)
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/

define([
    'jquery', 'underscore', 'backbone', 'handlebars',
    'global', 'notification', 'device',
    'saleitemcollection', 'saleitemmodel', 'itemview'
],

function ($, _, Backbone, Handlebars, Global, Notification, Device, SaleItemCol, SaleItemModel, ItemView) {

	'use strict';

	var minCollapsible = 10;

	var SaleItemListView = Backbone.View.extend({

		tagName: "ul",

		events: {
			"blur #txt-saleitemlist-search": "showFooter",
			"click .btn-column-head": "sortItems",
            "click #div-saleitemlist-detail-search-x": "clearKeyword",
			"click .li-letter-above": "collapseSystems",
			"keyup #txt-saleitemlist-search": "searchItemList",
			"change #sel-attrib, #sel-sort-by": "sortiPhoneItems",
			"focus #txt-saleitemlist-search": "hideFooter"
           },

		template: fnGetTemplate(URL_Common_Template,"GmSaleItemList"),

		initialize: function(options) {
			this.itemtemplate = options.itemtemplate;
			this.columnheader = options.columnheader;
		    this.renderType = options.renderType;
            this.sortoptions = options.sortoptions;
            this.currentcollection = this.collection;
            this.intResultCount =this.collection.length;
            this.template_URL = options.template_URL;
            this.callbackOnReRender =options.callbackOnReRender;
            
            var that = this;
			this.currentcollection.bind('sort', function(){
				that.render();
				if(that.callbackOnReRender!=undefined)
					that.callbackOnReRender(that);
			});
             if(this.sortoptions != undefined) {
            	this.DefaultSort = options.sortoptions[0].DefaultSort;
 				this.sortoptions = this.sortoptions[0]["Elements"];
            }
		   	if(this.renderType != undefined) {
			   	this.currentcollection.sortAsc("Name");
			}
			else if(this.DefaultSort != undefined) {
				this.currentcollection.sortAsc(this.DefaultSort);
			}
		   	this.selectvalue = 0;
		   	this.render();
		   	this.sortFirst();
		},

		render: function () {
			var that = this;
			if(this.renderType == 1) {
				this.$el.append(this.template());
			}
			else {
				this.$el.html(this.template());
			}
			
			switch(this.columnheader) {
				case 0:
					this.$("#div-saleitemlist-column").remove();
					this.$(".div-saleitemlist-search").remove();
					break;

				case 1:
					this.$("#div-saleitemlist-column").remove();
					break;

				case 2:
					this.$(".div-saleitemlist-search").remove();
					break;
			}

			var item = new SaleItemModel();
			var Current_Alphabet;
			var appendArea = "#div-saleitemlist";
			this.currentcollection.each(function(item) {
				if((that.renderType == 2) && (that.currentcollection.length > minCollapsible)) {
					if((item.get("Name").substring(0,1)!=Current_Alphabet) || (Current_Alphabet==undefined)) {
						Current_Alphabet = item.get("Name").substring(0,1).toUpperCase();
						var li = document.createElement('li');
						li.innerHTML = Current_Alphabet;
						li.className = "li-letter-above";
						var spn = document.createElement('span');
						spn.className = "spn-collpase-count";
						var spnCollapse = document.createElement('span');
						spnCollapse.className = "spn-indicate-collpase";
						spnCollapse.innerHTML = "+";
						li.appendChild(spnCollapse);
						li.appendChild(spn);
						that.$("#div-saleitemlist").append(li);
						var ul = document.createElement('ul');
						ul.id = "ul-under-" + Current_Alphabet.toLowerCase();
						that.$("#div-saleitemlist").append(ul);
						appendArea = ul;
					}
				}
				else if((that.renderType == 2) && (that.itemtemplate == "GmSubItem")) {
					that.itemtemplate = "GmItem";
				}
				var itemview = new ItemView({ model: item });
				//that.$(appendArea).append(itemview.render({template: this.itemtemplate}).el);
				that.$(appendArea).append(itemview.render({template_URL : that.template_URL, template: that.itemtemplate}).el);
				}, this);

            if((that.renderType == 2) && (that.currentcollection.length > minCollapsible)) {
				$(".li-letter-above").each(function() {
					var liAbove = this;
					var alphabet = $(this).text().toLowerCase().replace('+','');
					var count = 0;
					$("#ul-under-"+alphabet).find('li').each(function() {
						$(liAbove).find('.spn-collpase-count').html(count+1);
						count++;
					});
					
				});
			 }

 			if(this.sortoptions!=undefined) {
 				
 				//for dropdown headers
       			var select = document.createElement('select');
        		select.id = "sel-attrib";
        		var rightSelect = document.createElement('select');
        		rightSelect.id = "sel-sort-by";
        		var rightSelectOption1 = document.createElement('option');
        		var rightSelectOption2 = document.createElement('option');
        		rightSelectOption1.value = "ascending";
        		rightSelectOption2.value = "descending";
        		rightSelectOption1.innerHTML = "Ascending";
        		rightSelectOption2.innerHTML = "Descending";
        		rightSelect.appendChild(rightSelectOption1);
        		rightSelect.appendChild(rightSelectOption2);

        		for(var i=0; i<this.sortoptions.length; i++) {
        		    var option = this.sortoptions[i];
					var anOption = document.createElement('option');
					anOption.innerHTML = option["DispName"];
					anOption.value = option["ID"];
					select.appendChild(anOption);
							
					this.$("#div-saleitemlist-column").append(select);
					this.$("#div-saleitemlist-column").append(rightSelect);
        		}
			   
        	   //for row header
			   for(var i=0; i<this.sortoptions.length; i++) {
					var option = this.sortoptions[i];
					var anOption = document.createElement('li');
					var divArrow = document.createElement('div');
					anOption.innerHTML = option["DispName"];
					$(anOption).append(divArrow);
					var ClassName = option["ClassName"];
					anOption.id = option["ID"];
					this.$("#div-saleitemlist-column").append(anOption);
					$(anOption).addClass("btn-column-head");
					$(anOption).addClass(ClassName);
					$(anOption).addClass("ascending");
					$(divArrow).attr('ID','td-arrow-head');
					$(divArrow).addClass("arrow-down-sort");
				}
			}

 			
 			switch(this.itemtemplate){
 				case "GmItemGroup" : 
 						this.$("#txt-saleitemlist-search").attr("placeholder",lblNm("msg_search_for_groups"));
 						$("#div-saleitemlist-count").show();
  						$("#span-view-type").html(lblNm("msg_groups"));
 			            $("#spn-saleitemlist-count").html("&nbsp;" + this.intResultCount + "&nbsp;");
						break;
 				case "GmItemSet" :
 						this.$("#txt-saleitemlist-search").attr("placeholder",lblNm("msg_search_for_sets"));
 						$("#div-saleitemlist-count").show();
						$("#span-view-type").html(lblNm("msg_sets"));
 			            $("#spn-saleitemlist-count").html("&nbsp;" + this.intResultCount + "&nbsp;");
						break;
 				case "GmItemSetPart" :
 						this.$("#txt-saleitemlist-search").attr("placeholder",lblNm("msg_search_for_parts"));
 						$("#div-saleitemlist-count").show();
						$("#span-view-type").html(lblNm("parts"));
 			            $("#spn-saleitemlist-count").html("&nbsp;" + this.intResultCount + "&nbsp;");
						break;
 				case "GmItemGroupPart" :
						this.$("#txt-saleitemlist-search").attr("placeholder",lblNm("msg_search_for_parts"));
						$("#div-saleitemlist-count").show();
						$("#span-view-type").html(lblNm("parts"));
			            $("#spn-saleitemlist-count").html("&nbsp;" + this.intResultCount + "&nbsp;");
						break;
 			}

 			if(this.callbackOnReRender!=undefined)
	 			this.callbackOnReRender(this);

 			return this;
		},
       
		/*show the sorting arrow for the sorted headers*/
		sortFirst: function() {
			var that = this;
			this.$(".btn-column-head").each(function() {
					if($(this).attr("id")==that.DefaultSort) {
						that.$("#"+that.DefaultSort).find('div').show();
						that.$("#"+that.DefaultSort).removeClass("ascending").addClass("descending");
						that.$("#"+that.DefaultSort).css({backgroundColor: '#ccc', color: 'black'});
					}
				});
			this.$("#sel-attrib").find('option').each(function() {
				if($(this).val() == that.DefaultSort)
					$(this).attr("selected","selected");
			})
				that.DefaultSort = null;
		},
        /*triggers the keyup event in the search textbox*/
		clearKeyword: function(e) {
			e.preventDefault();
			this.$("#txt-saleitemlist-search").val("");
			this.$("#txt-saleitemlist-search").trigger("keyup");
		    $("#div-search-count").hide();
		},

        /*Shows and hides the close button in search textbox*/
		setX: function(wordLength) {
			if(wordLength == 0) {
				this.$("#div-saleitemlist-detail-search-x").hide();
			}
			else {
				this.$("#div-saleitemlist-detail-search-x").show();
			}
		},
		//footer hide on focus textbox
		hideFooter: function(){
			$("#div-detail-navigation").hide();
		},

		//footer show on focus textbox
		showFooter: function(){
			$("#div-detail-navigation").show();
		},
		
		/*Process the search and render the result*/
		searchItemList: function (e) {
			var intSearchCount = 0;
			var letters = $(e.currentTarget).val();
			var wordLength = letters.length;
			var that = this;
			this.letters = letters;
			this.setX(wordLength);
			// var id = $(e.currentTarget).parent().parent().attr("id");
			// var chkTechnique = $("#div-node").text();

            this.$("#div-saleitemlist").empty();

            var letters = $(e.currentTarget).val();
            if(letters.length >=1){
    			$("#div-search-count").show();
				var items = this.collection.search("Name",letters);
				this.currentcollection=new SaleItemCol(items);
				this.currentcollection.bind('sort', this.render, this);
				var item = new SaleItemModel();
				that.$("#div-saleitemlist").empty();
				_.each(items,function(item){
					var view = new ItemView({model:item});
				    //that.$("#div-saleitemlist").append(view.render({template: that.itemtemplate}).el);
				    that.$("#div-saleitemlist").append(view.render({template_URL : that.template_URL, template:  that.itemtemplate}).el);
				});
				
				intSearchCount = this.currentcollection.length;
			}
			else if(letters.length ==0) {
	   			$("#div-search-count").hide();
				this.currentcollection = this.collection;
				this.render();
				// if((id=="div-sidepanel-list") && (chkTechnique != "TECHNIQUE")) {
				// 	$(".btn-saleitemlist").each(function() {
				// 		$(this).removeClass("btn-saleitemlist").addClass("btn-close-catalog");
				// 	});
				// }
				this.$("#txt-saleitemlist-search").focus();
			}

			if(that.$("#div-saleitemlist").text().trim().length<1) {
				that.$("#div-saleitemlist").html("<li class='li-no-display'>"+lblNm("msg_result_not_found")+"</li>");
			}
			
            $("#span-search-count").html("&nbsp;" + intSearchCount + "&nbsp;");


            var seleced = this.collection.getSelected();
			for(var i=0;i<seleced.length;i++) {
				this.$el.find("#"+seleced[i]).parent().addClass("selected");
			}

			if(this.callbackOnReRender!=undefined)
	 			this.callbackOnReRender(this);
			
			// if((id=="div-sidepanel-list") && (chkTechnique != "TECHNIQUE")) {
			// 	$(".btn-saleitemlist").each(function() {
			// 		$(this).removeClass("btn-saleitemlist").addClass("btn-close-catalog");
			// 	});
			// }
		},
        
		/* Sort items in Ascending/Descending order */
		sortItems: function(e) {
			var elem = e.currentTarget;
			var currentClass= $(elem).attr('class');
			
			if ( currentClass.indexOf("ascending")>0 ) {
				this.currentcollection.sortAsc(elem.id);
				this.$("#txt-saleitemlist-search").val(this.letters);

				if(this.letters!=undefined) {
					this.setX(this.letters.length);
				}
				this.$("#" + elem.id).css({backgroundColor: '#ccc','color': 'black'});
				this.$("#" + elem.id).removeClass("ascending").addClass("descending");
				this.$("#" + elem.id).find("#td-arrow-head").removeClass("arrow-up-sort").addClass("arrow-down-sort").show();
	 		}
			
			else {
				this.currentcollection.sortDesc(elem.id);
				this.$("#txt-saleitemlist-search").val(this.letters);
				if(this.letters!=undefined){
					this.setX(this.letters.length);
				}
				this.$("#" + elem.id).css({backgroundColor: '#ccc','color': 'black'});
				this.$("#" + elem.id).removeClass("descending").addClass("ascending");
			    this.$("#" + elem.id).find("#td-arrow-head").removeClass("arrow-down-sort").addClass("arrow-up-sort").show();
			}
			
		},
		
		// sort ascending/ descending order in dropdown select for iphone
		sortiPhoneItems: function(e) {
			
			var attrib = $("#sel-attrib").val();
			var sortBy = $("#sel-sort-by").val();
			if ( sortBy== "ascending" ) {
				this.currentcollection.sortAsc(attrib);
				this.$("#txt-saleitemlist-search").val(this.letters);

				if(this.letters!=undefined) {
					this.setX(this.letters.length);
				}
	 		}
			else {
				this.currentcollection.sortDesc(attrib);
				this.$("#txt-saleitemlist-search").val(this.letters);
				if(this.letters!=undefined){
					this.setX(this.letters.length);
				}
			}
			this.$("#sel-attrib").find('option').each(function() {
				if($(this).val() == attrib) {
					$(this).attr("selected",lblNm("msg_selected"));
				}
			});
			this.$("#sel-sort-by").find('option').each(function() {
				if($(this).val() == sortBy) {
					$(this).attr("selected",lblNm("msg_selected"));
				}
			});
		},

		
        /* Display the systems in alphabetic order in a collapsible list */
        collapseSystems: function(event) {
        	var that = this;
            var alphabet = $(event.currentTarget).text().split('')[0].toLowerCase();
            var indicator = $(event.currentTarget).find(".spn-indicate-collpase");
            var indicatorText = $(indicator).text();
            this.$(".li-letter-above").each(function() {
            	$(this).removeClass("li-border-bottom");
            });
            this.$(".spn-indicate-collpase").each(function() {
            	$(this).text("+");
            });
            if(indicatorText == "+") {
            	$(indicator).text("-");
	            $(event.currentTarget).addClass("li-border-bottom");
	            if(this.$("#ul-under-"+alphabet).css("display")=="none") {
	            	this.$("#div-saleitemlist ul").each(function() {
	            		$(this).slideUp("fast");
		           	});
		           this.$("#ul-under-"+alphabet).slideDown("fast",function() {
			           that.$("#div-saleitemlist").animate({
					       scrollTop: that.$("#div-saleitemlist").scrollTop() + $("#ul-under-"+alphabet).position().top - 192
					   }, "fast");
		           });
	            }
            }
            else {
            	this.$("#ul-under-"+alphabet).slideUp("fast");
            }
        },

        close: function() {
        	this.unbind();
        	this.undelegateEvents();
        	this.$el.empty();
        }
	});

	return SaleItemListView;
});


