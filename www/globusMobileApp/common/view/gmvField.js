define([
    'jquery', 'underscore', 'backbone', 'handlebars', 'global',

    // Pre-Compiled Template
    'gmtTemplate'
], 

function ($, _, Backbone, Handlebars, Global, GMTTemplate) {

    'use strict';
    
    GM.View.Field = Backbone.View.extend({
    
        events: {
            blur: "validate",
            
        },

	    initialize: function() {
            this.name = this.$el.attr("name");
            this.$msg = $('#spn-' + this.name);
            console.log(this.name)
        },
        
        //validate the username and password
        validate: function() {
            this.model.set(this.name, this.$el.val(), {validate:true});
            
            if(this.name=="add"||this.name=="state"||this.name=="zip"||this.name=="city"){
                this.$el.parent().find("#spn-"+ this.name).text(this.model.errors[this.name] || '');
                if(this.name=="state"){
                    this.$el.closest("#div-module-state").find("#spn-"+ this.name).text(this.model.errors[this.name] || '');
                }
            }
            else{
                this.$msg.text(this.model.errors[this.name] || '');
            }
        }

    });

	return GM.View.Field;
});