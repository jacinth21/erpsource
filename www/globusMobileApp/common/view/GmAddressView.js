/**********************************************************************************
 * File:        GmAddressView.js
 * Description: Subview for ItemListView to render a single address of the List
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone', 'handlebars',
        'global',
        'addressmodel'
        ], function ($, _, Backbone, Handlebars, Global, Address) {

	'use strict';
	var AddressView = Backbone.View.extend({

		tagName: "li",
		
		// Render the initial contents when the app starts
		render: function (options) {
			this.template = fnGetTemplate(options.template_URL,options.template),	
			this.$el.html(this.template(this.model.toJSON()));
			return this;
		}
	});

	return AddressView;
});
