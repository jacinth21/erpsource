/**********************************************************************************
 * File:                 gmvSearch.js
 * Description:    Common View which will load a serach text box and able to do dynamic search.
 				           Uses SearchModel, GMCItem and ItemView.
 * Version:          1.0
 * Author:            praja
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone', 'handlebars',
        'global', 'commonutil', 'gmmSearch', 'gmcItem', 'gmvItem',

    	// Pre-Compiled Template
    	'gmtTemplate'

        ], function ($, _, Backbone, Handlebars, Global, CommonUtil, GMMSearch, GMCItem, GMVItem, GMTTemplate) {

	'use strict';
	
    GM.View.Search = Backbone.View.extend({

		events: {
			"keyup #div-keyword-search input": "searchKeyword",
            "keydown #div-keyword-search input": "moveOverResult",
			"click #div-search-result-multiple": "selectMore",
			"click .gmSearchListItem": "select",
			"click #div-common-search-x": "clearKeyword",
            "click #div-keyword-search input": "hideSearchResult"
		},

		template: fnGetTemplate(URL_Common_Template,"gmtSearch"),
		
		//Initialize and Config the View 
		initialize: function (Options) {
			//Required Paramters
			this.el = Options.el;									//Place where this view is going to be placed (Required)
			// this.fnName = Options.fnName;							//DAO function which gives the data (Required)
			this.url = Options.url;
			this.inputParameters = Options.inputParameters;
			this.keyParameter = Options.keyParameter;
			this.resWidth = Options.resWidth;
			this.resultVO = Options.resultVO;
            if(Options.fnName!=undefined)
                this.fnName = Options.fnName;
			//Setting default options through model
			this.searchConfig = new GMMSearch(Options);
			this.config = this.searchConfig.toJSON();

			//Optional Paramters - setting for view
			//Auto search options
			this.autoSearch = this.config.autoSearch;				//Option to set the Auto Search option (Default: true) (Optional)
			this.targetArea = this.config.target;					//Place where the results will be displayed (Required if autoSearch is false)
			this.itemTemplate = this.config.template;				//Template used to render Result (Required if autoSearch is false)
			this.template_URL = this.config.template_URL;			//Template URL of the itemTemplate mentioned above (Required if autoSearch is false)
			this.IDParameter = this.config.IDParameter;				// Server Key Parameter for ID (Default : ID)
			this.NameParameter = this.config.NameParameter;			// Server Key Parameter for Name (Default : Name)
            
			this.callback = this.config.callback;					//Callback on selecting any result (Optional)
			this.minChar = this.config.minChar;						//Minimum characters to search (Default: 3) (Optional)
			this.usage = this.config.usage;							//1 determines normal search, 2 determines multiple select this.config (Default: 1) (Optional)
			this.ID = this.config.ID;								//ID of the search text box (Default: "txt-search-keyword") (Optional)
			this.ModalResult = this.config.modalResult;				// For bootstrap modal window, setting css property of top and left. (true/false) Default : False
			//console.log(this.inputParameters);
			this.render();
			
		},
		
		// Render the initial contents when the app starts
		render: function () {
			var that = this;

			this.$el.html(this.template(this.config));
			that.setCssProperties();

			//If Target is not specified, set the default TargetArea
			if(this.targetArea=="") 
				this.targetArea = this.$("#ul-search-resultlist");

			this.$("input").keydown(function(e){
			   if(e.which == 38 || e.which == 40) 
			       e.preventDefault();
			});
			
			$(window).on('orientationchange', function(){that.setCssProperties()});
			return this;
		},

		//Set the CSS properties of auto search result dynamically based on the textbox Location
		setCssProperties: function() {

			if(this.autoSearch) {
				if(!this.ModalResult) {
					this.$("#div-search-result-main").css("top",(this.$("#div-keyword-search input").offset().top + this.$("#div-keyword-search input").height() + parseInt(this.$("#div-keyword-search input").css("margin-top")) + parseInt(this.$("#div-keyword-search input").css("padding-top")) + parseInt(this.$("#div-keyword-search input").css("margin-bottom")) + parseInt(this.$("#div-keyword-search input").css("padding-bottom")) +2) + 6 + "px")
					this.$("#div-search-result-main").css("left",this.$("#div-keyword-search input").offset().left + "px");
                    this.$("#div-search-result-main").removeClass("arrow_box_down");
                    this.$("#div-search-result-main").removeClass("arrow_box_up");
                    this.$("#div-search-result-main").addClass("arrow_box_up");
				}
				this.$("#div-search-result-main").css("width",parseInt(this.$("#div-keyword-search input").width()) + parseInt(this.$("#div-keyword-search input").css("padding-left")) + parseInt(this.$("#div-keyword-search input").css("padding-right")) + "px" );
				this.$("#div-search-result-main").css("margin",this.$("#div-keyword-search").css("margin"));
                
                if($(window).height() - this.$("#div-keyword-search input").offset().top + this.$("#div-keyword-search input").height() < 250) {
                    this.$("#div-search-result-main").removeClass("arrow_box_up");
                    this.$("#div-search-result-main").removeClass("arrow_box_down");
                    this.$("#div-search-result-main").addClass("arrow_box_down");
                    this.$("#div-search-result-main").css("top","");
                    this.$("#div-search-result-main").css("bottom", ($(window).height() - this.$("#div-keyword-search input").offset().top + this.$("#div-keyword-search input").height()) - 8 +"px");
                }

                if(parseNull(this.resWidth) != "")
                    this.$("#div-search-result-main").css("width", this.resWidth);
                
				if(this.usage==2) { 
					this.$("#div-search-result-multiple").css("margin-left","-" + parseInt(this.$("#div-search-result-main").css("padding-left")) + "px"); 
                    this.$("#div-search-result-multiple").addClass("gmBGDarkBlue gmFontWhite gmPad10p");
                    
                }
				
			}
			
		},
		
		//Render the results
		renderResult: function() {
			var that = this;
            console.log(this.itemCol)
			this.itemCol.each(function(item) {
                var el = document.createElement('li');
				var itemview = new GMVItem({model: item, el : that.targetArea, template_URL : that.template_URL, template: that.itemTemplate});
			}, this);
			$(this.targetArea).find('li').hover(function() {
                that.$('.active').removeClass('active');
				$(this).addClass('active')
			});
            if(!this.$("input").is(":focus")) {
                var searchText = this.$("input").val().trim().toLowerCase();
                $(this.targetArea).find('li').each(function(index) {
                    var firstText = $(that.targetArea).find('li:eq(' + index +')').text().trim().toLocaleLowerCase();
                   if($(this).text().trim().toLowerCase() == searchText || ($(that.targetArea).find('li').length==1 && firstText.indexOf(searchText)>=0)) {
                       that.$('.active').removeClass('active');
                       $(this).addClass('active');
                       $(this).find(".item-searchlist").trigger("click");
                       return false;
                   }
                });
            }
			this.setCssProperties();
            console.log(this.itemCol);
            if (that.usage == 2 && this.itemCol.length > 1)
                that.$("#div-search-result-multiple").removeClass("hide");
            else
                that.$("#div-search-result-multiple").addClass("hide");
		},
        
        moveOverResult: function(e) {
            if($(e.currentTarget).val().trim()!=""){
                if(e.keyCode==40) {
                    var targetList = $(this.targetArea).find('li.active').length>0?$(this.targetArea).find('li.active').next():$(this.targetArea).find('li').first();
                    $('.active').removeClass('active');
                    $(targetList).addClass('active');
                }
                else if(e.keyCode==38) {
                    var targetList = $(this.targetArea).find('li.active').length>0?$(this.targetArea).find('li.active').prev():$(this.targetArea).find('li').last();
                    $('.active').removeClass('active');
                    $(targetList).addClass('active');
                }
            }
			
        },
		
		//Perform Search operation
		searchKeyword: function(e){
			var that = this;
			var searchData = $(e.currentTarget).val();
			var wordLen = searchData.length;
            if(searchData.indexOf("’")!=-1)
			     searchData=searchData.replace("’","'");
			this.setX(searchData.length);
            
            if(e.keyCode==13) {
				$('.active .item-searchlist').trigger('click');
			}
			else if(that.prevWord!=searchData){
				that.prevWord = searchData;
                that.searchMode = true;
                
				$(this.targetArea).empty();
                this.$("#div-search-result-multiple").addClass("hide");
                
                
				if(this.$("#div-search-result-multiple").hasClass("initialized")) {
					this.$("#div-search-result-multiple").removeClass("initialized");
					this.$("#div-search-result-multiple").text("Multiple");
					this.multipleSelectionMode = false;
				}
                
                if(GM.Global.Device){
                    if(wordLen>=this.minChar){
                        this.fnName(searchData, function(data) {
                            if(data.length>0) {
                                that.setCssProperties();
                                that.itemMainCol = new GMCItem(data);
                                that.itemCol = that.itemMainCol;
                                that.renderResult();
                                if((that.autoSearch) && (data.length>1) && (that.usage == 2))
                                    that.$("#div-search-result-multiple").removeClass("hide");
                            }
                            else 
                                $(that.targetArea).html("<li>No Results Found!</li>");

                            if(that.autoSearch) 
                                that.$("#div-search-result-main").show();
                            else
                                $(that.targetArea).show();
                        });
                    }
                    
                }
                else{
                    if(wordLen>=this.minChar){
                        var input = this.inputParameters;
                        if(this.serverCall !=undefined) 
                            this.serverCall.abort();

                            this.inputParameters[this.keyParameter] = searchData;
                            this.serverCall = $.ajax({
                                url: this.url,
                                method: "POST",
                                contentType: "application/json",
                                dataType: "json",
                                data: JSON.stringify(input),
                                success: function (data) {
                                    console.log(data);
                                    var dataLength = 0;
                                    if(that.resultVO!=undefined){
                                        dataLength = data[that.resultVO].length;
                                    }
                                    else
                                       dataLength =  data.length;
                                    console.log(dataLength);
                                    if(dataLength > 0) {
                                        that.$("#div-search-result-main").find("#ul-search-resultlist").html("")
                                        if(that.resultVO!=undefined){
                                            if(that.resultVO == "gmCRMActivityVO")
                                                data = data.GmCRMListVO;
                                            var regExpID = new RegExp(that.IDParameter,'g'), regExpName = new RegExp(that.NameParameter,'g');
                                            var arrDispData = $.parseJSON(JSON.stringify(data[that.resultVO]).replace(regExpID,'ID').replace(regExpName,'Name'));

                                            that.itemMainCol = new GMCItem(arrDispData);
                                        }
                                        else {
                                            var regExpID = new RegExp(that.IDParameter,'g'), regExpName = new RegExp(that.NameParameter,'g');
                                            var arrDispData = $.parseJSON(JSON.stringify(data).replace(regExpID,'ID').replace(regExpName,'Name'));
                                            that.itemMainCol = new GMCItem(arrDispData);
                                        }
                                        that.itemCol = that.itemMainCol;
                                        $(that.targetArea).show();
                                        if(that.usage==1)
                                            that.$("#div-search-result-main").show()
                                        if(that.usage==2)
                                            that.$("#div-search-result-main").show()
                                        
                                        console.log("usage >>>>>>>>>>>"+that.usage);
                                        that.renderResult();
                                    }
                                    else {
                                        that.$("#div-search-result-main").show();
                                        that.$("#ul-search-resultlist").show();
                                        that.$("#ul-search-resultlist").html("<li><div id = 'div-search-null'>No Result Found</div></li>")

                                    }
                                },
                                error: function (err) {
                                  if(that.usage==1)
                                            that.$("#div-search-result-main").find("#ul-search-resultlist").html("<li><div id = 'div-search-null'>No Result Found</div></li>")
                                            that.$("#div-search-result-main").show();
                                            that.$("#ul-search-resultlist").show();
                                }
                            });
                    }
                    else
                        this.serverCall = undefined;

                    if(this.serverCall == undefined) {
                            $(that.targetArea).hide();	
                            if(that.usage==1)
                                that.$("#div-search-result-main").css({"display":"none"});
                    }
                }
                
                
                

			}
            that.setCssProperties();
			
		},

		//Initializes Multiple select / passes the selected value
		selectMore: function(e) {
            var that = this;
			var elem = e.currentTarget;
			if($(elem).hasClass("initialized")) {
				$(elem).removeClass("initialized");
				$(elem).text("Multiple");
				this.multipleSelectionMode = false;
				this.result = new Array();

				this.$(".selected-item").each(function() {
                    that.result.push({"ID" : $(this).find("span").attr("id") , "Name" : $(this).find("span").text(), "Element" : e.currentTarget , "AD":$(this).find("span").attr("adname") , "VP": $(this).find("span").attr("vpname"), "partyid":$(this).find("span").attr("partyid"), "npid":$(this).find("span").attr("npid")});
				});
				
				this.$("#div-common-search-x").addClass('hide');
				this.callback(this.result);
				if(this.autoSearch) 
					this.$("#div-search-result-main").hide();
				else
					$(this.target).hide();
			}

			else {
				$(elem).addClass("initialized");
				$(elem).text("Select (0)");
				this.multipleSelectionMode = true;
			}
		},

		//Selects a element
		select: function(e) {
			if(this.multipleSelectionMode) {
				$(e.currentTarget).toggleClass("selected-item");
				this.$(".initialized").text("Select (" + this.$(".selected-item").length + ")");
			}
			else {
				this.$("#div-common-search-x").addClass('hide');
                
                console.log($(e.currentTarget).find("span").text())
                
				this.result = {"ID" : $(e.currentTarget).find("span").attr("id") , "Name" : $(e.currentTarget).find("span").text(), "Element" : e.currentTarget , "AD":$(e.currentTarget).find("span").attr("adname") , "VP": $(e.currentTarget).find("span").attr("vpname"), "partyid":$(e.currentTarget).find("span").attr("partyid"), "npid":$(e.currentTarget).find("span").attr("npid"), "partycv":$(e.currentTarget).find("span").attr("partycv")};
				this.callback(this.result);
				var trimTarget = $(e.currentTarget).text();
				var trimmedTarget = trimTarget.trim();
				$("#txt-search-favourite-search").val(trimmedTarget);
                this.searchMode = false;
				if(this.autoSearch)  
                    this.$("#div-search-result-main").hide();
				else
					$(this.target).hide();
			}
            
            $("#ul-search-resultlist li").each(function(){
                if($(this).hasClass("active")){
                    $(this).removeClass("active");
                }
            });
		},
		
		
		clearKeyword: function(e) {
			var currElem = $(e.currentTarget).parent().parent().find('input');
		
			$(currElem).val("");
			$(currElem).trigger("keyup");
			$(e.currentTarget).addClass('hide');
            this.$("#div-search-result-main").hide();
		},
		
		
		setX: function(wordLength) {
			
			if(wordLength == 0) {
				this.$("#div-common-search-x").addClass('hide');
                this.searchMode = false;
			}
			else {
				this.$("#div-common-search-x").removeClass('hide');
			}
            
            var offsetParent = this.$("#div-common-search-x").offsetParent().position();
            var xElem = this.$("#div-common-search-x");
            
            if($(window).width() > 1024){
            if(offsetParent.top!=0 && offsetParent.left!=0) {
                $(xElem).css("left", (this.$("#" + this.ID).offset().left + this.$("#" + this.ID).width() - 110) + "px" );
            }
            else if(offsetParent.top!=0 && offsetParent.left==0) {
                $(xElem).css("left", (this.$("#" + this.ID).offset().left - this.$("#" + this.ID).width() - 15) + "px" );
                $(xElem).css("top", "20px");
            }
            else if(offsetParent.top==0 && offsetParent.left==0) {
                $(xElem).css("left", (this.$("#" + this.ID).offset().left + this.$("#" + this.ID).width() + 15) + "px" );
                $(xElem).css("top", (this.$("#" + this.ID).offset().top + 10) + "px");
            }
            else
                $(xElem).css("left", (this.$("#" + this.ID).offset().left + this.$("#" + this.ID).width() - 110 - offsetParent.left) + "px" );
            }
		},

		//To hide other search results dropdown
		hideSearchResult: function (e) {
		    var mainElem = $(e.currentTarget).parent().parent().parent().parent();
		    $(mainElem).find("#div-common-search-x").addClass('hide');
		    $(mainElem).find("#div-search-result-main").css("display", "none");
		}
		
	});

	return GM.View.Search;
});
