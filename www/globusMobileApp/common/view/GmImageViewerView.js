/**********************************************************************************
 * File:        GMImageViewerView.js
 * Description: Makes a Image slide show for the given images
 * Version:     1.0
 * Author: 		praja 
 **********************************************************************************/


/*global define*/
define([
    'canvas', 'jquery', 'underscore', 'backbone', 'handlebars', 'jqueryscroll', 'jqueryvisible', 'jqueryzoom', 'threesixty',
    'global', 'filesync'
],

function (Canvas, $, _, Backbone, Handlebars, JqueryScroll, Visible, Panzoom, ThreeSixty, Global, FileSync) {

	'use strict';

	var prevScroll, swipeNow = true, XDist, YDist, prevTouch = false, fingerMoved = false;

	var ImageViewerView = Backbone.View.extend({

		events: {
			"click .img-thumbnails" : "openViewer",
			"click #arrow-go-previous" : "movePrevious",
			"click #arrow-go-next" : "moveNext",
			"click #div-image-viewer-close, #div-threesixty-close" : "close",
			"touchend .active-image img" : "dblTapZoom",
			"touchmove .active-image img" : "senseMove",
			"click #btn-download-viewer" : "download",
			"click .img-download-threesixty" : "downloadThreeSixty"
		},

		el: "#div-image-viewer-background",

		template: fnGetTemplate(URL_Common_Template, "GmImageViewer"),

		initialize: function(info) {
			this.source = info.source;
			this.ImageId = info.ImageId;
			this.callback = info.callback;
			$(window).on('orientationchange', this.onOrientationChange);
			this.render();
		},

		render: function() {
			var that = this;
			var data = [];
			var len = $(this.source).find('img').length;
			$(this.source).find('img').each(function(index) {

				var source = $(this).attr("src");

				var className = $(this).attr("path");
	

				if((source.indexOf("http:") != -1) || (source.indexOf("file:") != -1)) {
					data.push({"src" : $(this).attr("src") , "id" : $(this).attr("id"), "alt" : $(this).attr("alt")});
					if(index == len-1) {
						that.$el.html(that.template(data));
						that.proceed();

					}
				}
				else {
				
					var id = $(this).attr("id");
					var alt = $(this).attr("alt");
					var path = $(this).attr("src");
					var name = $(this).attr("name");
					var src = "";
					
					var self =this;
					fnGetLocalFileSystemPath(function(localpath) {
						
	           			fnGetImage(localpath+path, function(status) {
			                if(status!="error") {
			                    src = localpath + path;
			                    data.push({"id" :  id , "Name" : name , "src" :  src , "alt" :  alt });
			 		           	if(index == len-1) {
									that.$el.html(that.template(data));
									that.proceed();
								}
			                }

			                if($(self).hasClass("cls-threesixty-default")){
			               		data.push({"id" :  id , "Name" : name , "src" :  path , "alt" :  alt , "class" : "img-thumbnail-threesixty", "value" : $(self).attr("value")});
			 		           	if(index == len-1) {
									that.$el.html(that.template(data));
									that.proceed();
								}
			               	}

			                else if(navigator.onLine) {

			                	// PC-4132 - files download from azure
			                  	 src = path;
			                  	 //  src = URL_FileServer+path;
			                  
			                   	 data.push({"id" :  id , "Name" : name , "src" :  src , "alt" :  alt });
			                    	
		                  		if(index == len-1) {

		                  			that.$el.html(that.template(data));
									that.proceed();
								}
			                }
			                else {
			                	that.$("#div-sub-result-image").html("");
			                	showMsgView("006");
			                }
			                
			            });
           		 	});
				}
			});
			return this;
		},

		// Scroll and swipe controls intitialization after render
		proceed: function() {


			if(this.ImageId!=undefined /*&& (!$("#img-thumbnail-"+this.ImageId).hasClass('img-thumbnail-threesixty')) */) {
				this.$("#img-thumbnail-" + this.ImageId).trigger('click');
			}
			
			if(this.callback!=undefined) {
				this.callback();
			}
			this.$(".img-curr-thumbnail").trigger("click");
			var that =this;
			this.$("#div-image-viewer").on("scroll",function(){
				var leftVal = this.scrollLeft;	
				clearTimeout($.data(this, 'scrollTimer'));
			    $.data(this, 'scrollTimer', setTimeout(function() {
			       if(!$(".active-image").visible()) {

					if(swipeNow)
						if(leftVal>prevScroll) {
							$("#arrow-go-next").trigger("click");
						}
						else {
							$("#arrow-go-previous").trigger("click");
						}
				}
				else 
					that.goTo();
			    }, 50));
				
			});
			if($("#btn-download").hasClass("show-btn-download-viewer"))
				this.$("#btn-download-viewer").show();

			this.$("img").each(function(){
				that.processError(this);
			});
		},
		
		// Downloads the images in the viewer
		download: function() {
			var that = this;
			var i = 0;
			
			if(navigator.onLine) {
				showMsgView("038");
				this.$(".img-thumbnails").each(function(){
					if(!$(this).hasClass('img-thumbnail-threesixty')) {
						var Source = $(this).attr("src");
						fnGetLocalFileSystemPath( function(path) {
							var Destination = path + (Source).replace(URL_FileServer,'');
                            Source = Source.replace(URL_FileServer,setFileServer());
							fnDownload(Source,Destination);
							that.$("#btn-download-viewer").hide();
							$("#btn-download").hide();
							$("#btn-download").removeClass("show-btn-download-viewer");
						});
					}
				});
			}
			else {
				showMsgView("007");
			}
		},

		//Function To Prevent Double Tab Zoom on Two Finger Zooming
		senseMove: function  (e) {
			fingerMoved = true;
		},

		//Hides all other controls on a dobule tap zoom 
		dblTapZoom: function(e) {
			if(prevTouch) {
				prevTouch = false;
				var elem = this.$(".active-image").find("img");
				var scale = $(elem).panzoom("getMatrix");
				if(scale[0]==1) {
					$("#div-image-viewer-container").addClass("image-zoom");
					$(elem).parent().addClass("zoom-image");
					$(elem).panzoom("option", "increment", 2.2);
					$(elem).panzoom("zoom");
					$(elem).panzoom("option", "increment", 0.3);
				}
				else {
					$("#div-image-viewer-container").removeClass("image-zoom");
					$(elem).parent().removeClass("zoom-image");
					$(elem).css('-webkit-transform',"matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.002, 0, 0, 0, 1)");
				}
			}
			else if(!fingerMoved) {
				prevTouch = true;
			}

			setTimeout(function() {
				prevTouch = false;
			},300);

			fingerMoved = false;
		},

		// Triggered when clicking on the thumbnail
		// Senses whether image viewer should be loaded or 360˚ should be loaded
		openViewer: function(event) {
			var that = this;
			var currSrc = $(event.currentTarget).attr("src");
			
			var imageTitle = $(event.currentTarget).prop("alt");
			this.$(".img-curr-thumbnail").removeClass("img-curr-thumbnail");
			this.$(".img-thumbnails").each(function() {
				if($(this).attr("src") == currSrc) {
					$(this).addClass("img-curr-thumbnail");
					that.currentImage = $(this);
				}
			});
			this.$("#arrow-go-next").show();
			this.$("#arrow-go-previous").show();
			if($(this.currentImage).prev().attr("id") == undefined) {
				this.$("#arrow-go-previous").hide();
			}
			if($(this.currentImage).next().attr("id") == undefined) {
				this.$("#arrow-go-next").hide();
			}
			this.$("#div-image-viewer").trigger("create");
			
			
			if($(event.currentTarget).hasClass("img-thumbnail-threesixty")){
				this.$("#div-img-title").html($(event.currentTarget).attr('alt'));
			
				if($('#threesixty_images').find('li').length ) {
					$("#div-image-viewer").hide();
					$("#div-threesixty-viewer").show();
				}
				else {
					var value = $(event.currentTarget).attr('value');
					var name = $(event.currentTarget).attr('name');
					var id = $(event.currentTarget).attr('id').replace('img-thumbnail-','');
					this.startThreeSixty(value, name, id);
				}
			}
			else {
				this.$("#div-download-threesixty").hide();
				this.$("#div-threesixty-viewer").hide();
				this.$("#div-image-viewer").show();
				this.$(".image-title").html(imageTitle);
				this.fixHeight();
			}
			this.$el.show();

		},

		// Calcultes the height of the image and fix it inside the container
		fixHeight: function() {
			var currWidth = this.$("#div-image-viewer").width() + "px";
			var currHeight = this.$("#div-image-viewer").height() + "px";
			this.$(".div-inner-adjustable").each(function() {
				var innerDiv = this
				$(this).css("width",currWidth);
				$(this).find('img').load(function(){
					$(this).css("max-height",currHeight);
					$(this).css("width","auto");
					if(this.width > (currWidth).replace('px','')){
						$(this).css("width",currWidth);
						$(this).css("height","auto");
					}
					$(innerDiv).css("width",currWidth);
					$(innerDiv).css("height",currHeight);
					var margin_top = (currHeight.replace('px','') - this.height)/2;
					$(this).css("margin-top", margin_top + "px");
				});
			});
			this.goTo();
		},

		//Handles the orietaion change in the iPad
		onOrientationChange: function(event) {
			this.$(".img-curr-thumbnail").trigger("click");
		},

		//Process previous image
		movePrevious: function(event) {
			if(this.$("#arrow-go-previous").css("display") != "none") {
				this.$("#arrow-go-next").show();
				this.currentImage = $(this.currentImage).prev();
				this.$("#div-image-viewer").show();
				this.$("#div-download-threesixty").hide();
				this.$("#div-threesixty-viewer").hide();
				this.goTo();
				if($(this.currentImage).prev().attr("id") == undefined) {
					this.$("#arrow-go-previous").hide();
				}
			}
		},

		//Process Next Image
		moveNext: function(event) {
			if(this.$("#arrow-go-next").css("display") != "none") {
				this.$("#arrow-go-previous").show();
				this.currentImage = $(this.currentImage).next();
				this.$("#div-download-threesixty").hide();
				if(!$(this.currentImage).hasClass('img-thumbnail-threesixty')) 
					this.goTo();
				else {
					this.$(".img-thumbnail-threesixty").trigger("click");
				}
				if($(this.currentImage).next().attr("id") == undefined) {
					this.$("#arrow-go-next").hide();
				}
			}
		},

		// Moves to the current image and 
		// initializes the pinch and zoom
		goTo: function() {
			swipeNow=false;
			fingerMoved = false;
			this.$(".img-curr-thumbnail").removeClass("img-curr-thumbnail");
			$(this.currentImage).addClass("img-curr-thumbnail");

			this.$(".active-image").removeClass("active-image");
			var id = $(this.currentImage).attr("id").replace("img-thumbnail-","");

			this.$("#"+id).parent().parent().addClass("active-image");
			var that = this;

				
			this.$("#div-img-title").html($(this.currentImage).attr("alt"))
			this.$("#div-thumbnails").scrollTo(this.$(this.currentImage));
			this.$("#div-image-viewer").scrollTo(this.$(".active-image"),250,{onAfter:function(){
				prevScroll = $("#div-image-viewer").scrollLeft();
				swipeNow = true;
				that.$('.img-main-viewer').each(function() {
					$(this).panzoom("reset");
				});
				that.$(".active-image").find('img').panzoom({
					minScale: 1,
					maxScale: 3
				});
			}});

			


			that.$(".active-image").find("img").on('panzoomend', function(e, panzoom, transform) {

				var currMatrix = that.$(".active-image").find("img").panzoom("getMatrix");

				if(currMatrix[0]==1) {
					// currMatrix[0] = 1;
					// that.$(".active-image").find("img").panzoom("setMatrix", currMatrix);
					if((!$(".active-image").find("img").visible()) && (swipeNow)) {
						if(($(".active-image").find("img").offset().left<0) && (that.$("#arrow-go-next").css("display") != "none"))
							$("#arrow-go-next").trigger("click");
						else if(($(".active-image").find("img").offset().left>0) && (that.$("#arrow-go-previous").css("display") != "none"))
							$("#arrow-go-previous").trigger("click");
						else
							that.$(".active-image").find("img").panzoom("resetPan");
					}
					else 
						that.$(".active-image").find("img").panzoom("resetPan");
				}

			});
		},

		//Displays default Error Image on unavailbality of images
		processError: function(img) {
			img.onerror = function() {
				console.log("Error on image : " + this.src);
				this.src = "globusMobileApp/image/icon/ios/missing.jpeg";
				that.$("#btn-download-viewer").hide();
				this.className = "img-missing";
			}
		},

		//Check the availability of the 360 zip file and show / hide the download / 360 view
		startThreeSixty: function(path,name, id) {
			var url, that = this;
			that.$("#div-image-viewer").hide();
			fnGetLocalFileSystemPath(function (devicepath) {
				var mainpath = path.replace(URL_FileServer,'').replace(devicepath,'');
				fnGetImage((devicepath + mainpath), function (status) {
					if((status!='error') && (!that.$("#prg-dowload-threesixty-" + id).hasClass("downloading"))) {
						var source = decodeURI(status.toURL());
						var arrFolder = source.split("/");
						var strFinalFolder = arrFolder.pop().replace('.zip','');
						var Destination = arrFolder.join("/") + "/";
						fnUnzipp(source, Destination, function(folderPath) {
							url = folderPath + "/" + strFinalFolder + "/" + name.split('_')[0] + "_";
							// url = folderPath + "/" + "App_3D" + "_";
							var max = parseInt(name.replace('.zip','').split('_')[1]);
							that.$('#threesixty_images').html('');
							that.$("#spinner").html("<span>0%</span>");
							that.$("#div-download-threesixty").hide();
							that.$("#div-threesixty-viewer").show();
							addSpinner(max);
							loadImage(url);
						});
					}
					else {
						that.$("#div-download-fail-" + id).addClass('hide');
						that.$("#div-image-viewer").hide();
						that.$("#img-download-" + id).show();
						that.$("#div-download-threesixty").show();
					}	
				});
			});	
			
		},

		//Downloads the Zip File
		downloadThreeSixty: function(e) {
			var that = this;
			var id = e.currentTarget.id.replace('img-download-','');
			$(e.currentTarget).hide();
			var path = $(this.currentImage).attr('value');
			var name = $(this.currentImage).attr('name');
			fnGetLocalFileSystemPath(function (localpath) {
				fnDownload(path.replace(URL_FileServer,setFileServer()), localpath + path.replace(URL_FileServer,''), function(percent){
					that.$("#img-download-" + id).hide();
					that.$("#prg-dowload-threesixty-" + id).val(percent*100).show();
					that.$("#prg-dowload-threesixty-" + id).addClass("downloading");
					that.$("#div-download-fail-" + id).addClass('hide');
					that.$("#div-threesixty-download-" + id).show();
					that.$("#div-threesixty-download-" + id).find("span").html(parseInt(percent*100)).show();
				},function(status) {
					if(status!='error') {
						that.$("#prg-dowload-threesixty-" + id).removeClass("downloading");
						that.startThreeSixty(localpath + path.replace(URL_FileServer,''),name);
					}
				},function  () {
					that.$("#prg-dowload-threesixty-" + id).removeClass("downloading");
					that.$("#prg-dowload-threesixty-" + id).hide();
					that.$("#div-threesixty-download-" + id).hide();
					that.$("#div-download-fail-" + id).removeClass('hide');
				});
			});
			
		},

	    //Closes the View
	    close: function(event) {
	    	this.unbind();
        	this.undelegateEvents();
        	this.$el.unbind();
	    	this.$el.hide();
	    	this.$el.empty();
	    }

	});

	return ImageViewerView;
});
