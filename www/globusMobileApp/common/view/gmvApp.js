define([
    'jquery', 'underscore', 'backbone', 'handlebars', 'global'
], 

function ($, _, Backbone, Handlebars, Global) {

    'use strict';
    
    GM.View.App = Backbone.View.extend({
    
        showView: function(view) {
            var closingView = this.view;
            this.view = view;
            if ((closingView != undefined) && (closingView.name != 'CRM Main View') )
                this.closeView(closingView);    
            this.view.render();
            //$(this.view.el).hide();
            //this.el.append(this.view.el);

            this.openView(this.view);
            //console.log("Closing View's Name is " + closingView.name);
//            console.log(closingView.name)
            
        },

        openView: function(view) {
            //$(view.el).slideToggle(500);
            if(view.el!=undefined)
                $(view.el).show();
            console.log("Open " + this.view.name)
            
            var width = $( window ).width();
            if(width <= 480){
                $("#div-footer").addClass("hide");
                
                if(Backbone.history.fragment == "crm/dashboard") {
                    $("#link-home").removeClass("hide");
                    $("#link-back").addClass("hide"); 
                }
                else {
                    $("#link-home").addClass("hide");
                    $("#link-back").removeClass("hide"); 
                } 
            }
            $("#btn-back").hide();
        },

        closeView: function(view) {
            if (view) {
                if($.xhrPool.length){
                    _.each($.xhrPool, function(xhr){
                        if(xhr && xhr.readyState != 4){
                            xhr.abort();
                        }
                    })
                    $.xhrPool = [];
                }
                console.log("Close " + view.name);
                view.unbind();
                view.undelegateEvents();
                view = undefined;  //memory killer
//                $("#div-footer").hide();
                //view.remove();
                // $(view.el).slideToggle(500, function(){
                //     $(view).remove();
                // });
            }
        }
    });

	return GM.View.App;
});
