/**********************************************************************************
 * File             :        gmvMoreFilter.js
 * Description      :        View to represent Common Functions of scheduler Filter
 * Path             :        /web/GlobusCRM/common/view/gmvMoreFilter.js
 * Author           :        tmuthusamy
 **********************************************************************************/

/*Filter Values loaded based on type*/
function filterValuesLoadInit(webServicePath, filterInput,customTabs, cb) {
    fnGetWebServerData(webServicePath, undefined, filterInput, function (data) {
        console.log(">>>> " + JSON.stringify(data));
        GM.Global.filterTabs = [];
        if (data != null) {
            for (var key in data) {
                GM.Global.filterTabs.push(key);
                var keyCapNm = key.substr(0, 1).toUpperCase() + key.substr(1);
                $(".mFilterheaderLinks").append('<li class="filterLinks" data-showDiv="divFilter' + keyCapNm + '">' + keyCapNm + '</li>');
                _.each(data[key], function (item) {
                    $("#divfilter-" + key + "").append('<li><input type="checkbox" name="' + item.id + '" data-text="' + item.name + '" data-category="' + key + '"><span>' + item.name + '</span></li>');
                });
            }
            GM.Global.filterTabs.push(customTabs.toString());
            cb();
        }
    });
}

/*show Filter values when select on the popup filter section*/
function showMoreFilters(filterData, cb) {
    var that = this;
    
    if (Object.keys(filterData).length != 0)
        var filters = filterData;
    else
        var filters = {};

    $('.moreFilterLists').removeClass('hide');
    $(".filterLinks").unbind('click').bind('click', function (e) {
        var showElem = $(e.currentTarget).attr('data-showDiv');
        $(".filterLinks").removeClass("activeTab");
        $(this).addClass("activeTab");
        $(".divFilter").addClass('hide');
        $('.' + showElem).removeClass('hide');
    });
    

    var filter_inputs = $(".divFilter").find("input");
    
    

    for (var i = 0; i < filter_inputs.length; i++) {
        var filter_input = filter_inputs[i];
        var fCategory = $(filter_input).data("category");
        var fname = $(filter_input).attr("name");
        var isFilter = false;
        if (typeof (filters[fCategory]) == "object") {
            var chkClctn = filters[fCategory];
            var chkres = _.findWhere(filters[fCategory], {
                id: fname
            });
            if (typeof (chkres) == "object")
                isFilter = true;

        }
        // set initial input value based on filters settings
        filter_input.checked = isFilter;

        // attach event handler to update filters object and refresh view (so filters will be applied)
        filter_input.onchange = function () {
            filters[this.name] = !!this.checked;
            var chkFilters = {};
            _.each(GM.Global.filterTabs,function(dt){
                chkFilters[dt] = [];
            });
            var $this, text, id, category;
            $('.divFilter li input').each(function () {
                $this = $(this);
                id = $this.attr("name");
                text = $this.data("text");
                category = $this.data("category");
                if ($this.is(':checked') == true) {
                    var chkLen = chkFilters[category].length;
                    chkFilters[category][chkLen] = {};
                    chkFilters[category][chkLen].id = id;
                    chkFilters[category][chkLen].text = text;
                    chkFilters[category][chkLen].category = category;
                }

            });

            if (GM.Global.PSScheduler.productArr.length > 0) {
                for (var j = 0; j < GM.Global.PSScheduler.productArr.length; j++) {
                    chkFilters["product"][j] = {};
                    chkFilters["product"][j].id = GM.Global.PSScheduler.productArr[j].ID;
                    chkFilters["product"][j].text = GM.Global.PSScheduler.productArr[j].Name;
                    chkFilters["product"][j].category = "product";
                }
            }

            console.log("chkFilters" + JSON.stringify(chkFilters));
            cb(chkFilters);
        }
    }

}

/*show Selected filter Elements*/
function showSelectedFilters(filterData) {
    var that = this;
    var filterres = filterData;
    console.log(JSON.stringify(filterres));
    this.selectedFilterElem = "";
    for (var key in filterres) {
        _.each(filterres[key], function (data) {
            that.selectedFilterElem += "<li class='liSelFilter gmAlignHorizontal'><span class='filterTxt'>" + data.text + "</span> <span class='deleteFilter' data-id='" + data.id + "' data-category='" + data.category + "'>X</span></li>";
        });
    }
    $('.show_filters').html(that.selectedFilterElem);

}

/*Remove Selected Filter*/
function removeFilters(e, filterData, cb) {
    var id = $(e.currentTarget).data('id');
    var catgry = $(e.currentTarget).data('category');
    var filterres = filterData;
    filterres[catgry] = _.reject(filterres[catgry], function (csd) {
        return csd.id == id;
    });
    cb(filterres);
}

/*Remove All Selected Filter*/
function removeAllFilters(cb) {
    $('.moreFilterLists').addClass('hide');
    cb();
}

/*add products by searching on the search box*/
function addProducts(data) {
    var that = this;
    _.each(arrayOf(data), function (dt) {
        GM.Global.PSScheduler.productArr.push(dt);
    });
    GM.Global.PSScheduler.productArr = _.uniq(GM.Global.PSScheduler.productArr, function (item) {
        return item.ID;
    });
    $(".filteredListProduct").empty();
    _.each(GM.Global.PSScheduler.productArr, function (dl) {
        $(".filteredListProduct").append("<div class='liSelFilter gmAlignHorizontal' id='" + dl.ID + "'><span class='filterTxt'>" + dl.Name + "</span><span class='deleteFilter'> x</span></div>");
    });
    $("#divfilter-product input").val("");

    $(".deleteFilter").unbind('click').bind('click', function (e) {
        var removeProductId = $(e.currentTarget).parent(".liSelFilter").attr("id");
        GM.Global.PSScheduler.productArr = _.reject(GM.Global.PSScheduler.productArr, function (dt) {
            return dt.ID == removeProductId;
        });
        $(e.currentTarget).parent(".liSelFilter").remove();

    });
    console.log("addProduct >>> " + JSON.stringify(GM.Global.PSScheduler.productArr));
}