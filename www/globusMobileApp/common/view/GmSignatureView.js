/*global define*/
define([
	'jquery', 'underscore', 'backbone', 'handlebars'  
], function ($, _, Backbone, Handlebars ) {
	'use strict';
  
  var canvas;
  var context;
  var radius = 2;
  var dragging = false;
  var targetTouch;
  var rect;
  
  var SignatureView = Backbone.View.extend({
    
    events: {
       "touchstart #SignCanvas": "engage",
       "touchmove #SignCanvas": "putPoint",
       "touchend #SignCanvas": "disengage",  
       "click #div-signature-clear": "clear",
       "click #div-sig-submit" : "submit",
       "click #div-sig-cancel" : "closeSignatureView"
    },
    
    //Compile and assign the template
	template:  fnGetTemplate(URL_Common_Template, "GmSignature"), 

    //Contructor
	initialize: function (options) {
      if(options!=undefined)
        this.callback = options.callback;
      this.signed = false;
      this.render();
    },
    
    //Incorporate Template and Render the contents
    render: function() {
      this.$el.html(this.template());
      this.delegateEvents();
      var that = this;
      setTimeout(function() {
        that.prepSignPad()
      }, 0);
      this.$el.trigger("create");
      return this;
    },
    
    clear: function() {
      this.signed = false;
      this.initialize();
    },
    
    prepSignPad: function(callback) {
      canvas = document.getElementById("SignCanvas");
      context = canvas.getContext("2d");
      context.lineWidth = radius * 2;
      callback;
    },
    
    putPoint: function(e) {
      
      e.preventDefault();
      targetTouch = e.originalEvent.targetTouches[0];
      
      rect = canvas.getBoundingClientRect();
      
      var x = targetTouch.pageX - rect.left;
      var y = targetTouch.pageY - rect.top;

      if(dragging) {
        context.lineTo(x, y);
        context.stroke();
        context.beginPath();
        
        context.arc(x, y, radius, 0, Math.PI * 2);
        context.fill();
        context.beginPath();
        context.moveTo(x, y);
      }
    },
    
    engage: function(e) {
      this.signed = true;
      dragging = true;
      putPoint(e);
    },

    disengage: function() {
      dragging = false;
      context.beginPath();
    },

    getSignature: function() {
    	var imageURL = canvas.toDataURL("image/png");
    	return (this.signed)?imageURL:"";
    },

    submit: function() {
      if((this.callback!=undefined) && (this.getSignature()!=""))
        this.callback(this.getSignature());
      else
        this.closeSignatureView();
    },
    
    closeSignatureView: function() {
    	this.$el.parent().hide();
    },
    
    close: function() {
      this.unbind();
      this.remove();
    }

	});

	return SignatureView;
});
