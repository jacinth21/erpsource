/**********************************************************************************
 * File:        GmSetListView.js
 * Description: View which loads the sets data
 * Version:     1.0
 * Author:      tvenkateswarlu
 **********************************************************************************/

define([
    'jquery', 'underscore', 'backbone', 'handlebars',
    'global', 'notification', 'device',
    'itemmodel', 'itemcollection', 'itemlistview', 'numberkeypadview', "loaderview"
],

function ($, _, Backbone, Handlebars, Global, Notification, Device, Item, Items, ItemListView, NumberKeypadView , LoaderView) {

	'use strict';
	var SetListView = Backbone.View.extend({

		events : {
			"click #div-common-setlist-close-icon" : "close",
			"click #div-common-setlist-fax" : "prepareFax",
			"click #div-common-setlist-email" : "sendEmail",
			"click #span-common-setlist-load" : "loadParts",
			"click #div-common-setlist-tagid i" : "getBarCode"
		},

		/* Load the templates */ 
		template: fnGetTemplate(URL_Common_Template, "GmSetList"),
		tplSpinner: fnGetTemplate(URL_Common_Template, "GmProgressSpinner"),
		template_PDF: fnGetTemplate(URL_Common_Template, "GmSetListPDF"),
		
		initialize: function(options) {
			this.el = options.el;
			this.tagid = options.tagid.trim();
			this.render(options);
			var that = this;
			$("#btn-home").bind("click", function() {
				that.close();
			});
			$("#btn-settings").bind("click", function() {
				that.close();
			});
			$("#btn-back").bind("click", function() {
				that.close();
			});
		},
		
		render: function (options) {
			$("#div-setlist-viewer-background").show();
			this.$el.html(this.template());
			this.$(".div-common-page-loader").html(this.tplSpinner({"message":lblNm("msg_set_info")}));
			this.$("#div-common-setlist-content-main").hide();
			this.$("#div-common-setlist-buttons").hide();
			if(this.tagid!="" && this.tagid != undefined)
				this.showParts();
			
			return this;
		},

		showParts: function() {
			var that = this;
			this.$("#div-common-setlist-content-main").show();
			this.$("#div-common-setlist-buttons").show();
			$("#div-common-setlist-details").hide();
			$("#div-itemlist-count").hide();
			$("#div-search-count").hide();
			$("#div-common-setlist-buttons").hide();
			$("#div-common-setlist-content #div-keyword-detail").hide();
			$("#div-common-setlist-content").hide();
			$("#div-common-setlist-content").empty();
			$("#div-common-setlist-content-main .li-no-display").remove();
			$("#div-common-setlist-content-main .div-common-setlist-content-main-nonetwork").remove();
			if((navigator.onLine) && (!intOfflineMode)) {
				this.$(".div-common-page-loader").show();
				var input = {
					"token": localStorage.getItem("token"),
					"uuid": deviceUUID,	
					"tagid": this.tagid,
					"countryid": intServerCountryCode
				};
				fnGetWebServiceData("tag/fetchTagReq", undefined, input, function(status,data) {
					if(status && data.consignid !=""){
						that.currsetid = data.setid;
						that.currtagid = data.tagid;
						that.currsetnm = data.setnm;
						that.currsettype = data.settypenm;
						that.$("#txt-common-setlist-tagid").val(data.tagid);
						that.$("#span-common-setlist-details-setid").html(data.setid);
						that.$("#span-common-setlist-details-setname").html(data.setnm);
						that.$("#span-common-setlist-details-settype").html(data.settypenm);
						if( data.gmTagPartListVO == undefined){
							$("#div-common-setlist-content").show();
							that.$("#div-common-setlist-content-main").append("<li class='li-no-display'>"+lblNm("msg_parts_not_available")+"</li>");
							that.$(".div-common-page-loader").hide();
						}
						else{
							that.results = new Array();
							that.preparePartList(data.gmTagPartListVO,that.results);
						}
					}
					else {
						if($.parseJSON(data).errorCode != undefined && $.parseJSON(data).errorCode != "")
							showNativeAlert($.parseJSON(data).message + '\n' + $.parseJSON(data).action, 'Data Unavailable','OK');
						else
							showNativeAlert(lblNm("msg_server_down"),lblNm("error"),lblNm("msg_ok"));
						$("#div-common-setlist-content").show();
						that.$("#div-common-setlist-content-main").append("<li class='li-no-display'>"+lblNm("msg_parts_not_available")+"</li>");
						that.$(".div-common-page-loader").hide();
					}
				});
			}
			else {
				$("#div-common-setlist-content").show();
				this.$("#div-common-setlist-content-main").append("<div class='div-common-setlist-content-main-nonetwork'><div> <i class='fa fa-exclamation-triangle fa-lg'></i>"+lblNm("msg_no_internet_connection")+"</div>"+lblNm("msg_wifi_set_list")+"</div>");
			}
		},

		preparePartList: function(data,result){
			if(data.length >0) {
				for(var i=0; i<data.length; i++) {
					var  ID= data[i].pnum;
					var  Description= data[i].pdesc;
					var  Quantity= parseInt(data[i].qty);
					var Name = data[i].pnum + " - "+data[i].pdesc;
					result.push({"ID" : ID, "Description" : Description, "Quantity" : Quantity, "Name" : Name});
				}

				var sortoptions =[{ "DefaultSort" : "Description", "Elements" : [{"ID" : "ID","DispName" : "Part ID","ClassName":"li-item-id"},{"ID" : "Description" , "DispName" : "Part Description","ClassName":"li-item-name"},{"ID" : "Quantity" , "DispName" : "Quantity","ClassName":"li-item-qty"}]}];
				this.showItemListView(this.$("#div-common-setlist-content"), result, "GmItemSetPart", 3, sortoptions, URL_Catalog_Template);
				$("#div-common-setlist-details").show();
				$("#div-itemlist-count").show();
				$("#div-common-setlist-buttons").show();
				$("#div-common-setlist-content #div-keyword-detail").show();
				$("#div-common-setlist-content").show();
				this.$(".div-common-page-loader").hide();
			}

			else {
				$("#div-common-setlist-content").show();
				this.$("#div-common-setlist-content-main").append("<li class='li-no-display'>"+lblNm("msg_parts_not_available")+"</li>");
				this.$(".div-common-page-loader").hide();
			}
		} ,

		showItemListView: function(controlID, data, itemtemplate, columnheader, sortoptions, template_URL) {
			var items = new Items(data);	
			this.itemlistview = new ItemListView({ 
				el: controlID, 
				collection: items,
				columnheader: columnheader, 
				itemtemplate: itemtemplate,
				sortoptions: sortoptions,
				template_URL: template_URL
			});
		},

		getBarCode: function(event) {
			var that = this;
			var parent = $(event.currentTarget).parent();
			cordova.plugins.barcodeScanner.scan(
				function (result) {
					if(result.cancelled !=1) {
						$(parent).find("input").val(result.text);
						$("#span-common-setlist-load").trigger("click");
					}
				}, 
				function (error) {
				  console.log("Scanning failed: " + error);
				}
			);
		},

		loadParts: function(event) {
			var tagid = $(event.currentTarget).parent().find("input").val().trim();
			if(tagid.length > 0) {
				this.tagid = tagid;
//				showMsgView("011");
				this.showParts();
			}
			else{
				fnShowStatus(lblNm("msg_enter_tagid"));
			}
			
		},

		sendEmail: function() {
			var that = this;
			this.preparePDF(function(){
				fnGetLocalFileSystemPath(function(URL) {
						window.plugin.email.open({
						to: [],
						cc: [],
						subject: that.currsetid + "  " + that.currsetnm + lblNm("msg_part_head"),
						attachments: [ URL + "Setlistinfo/" + that.tagid + ".pdf"],
						body: lblNm("msg_find_parts")+"<b>" + that.currtagid + "</b><br/><br/>"+lblNm("msg_let_me_know")+"<br/><br/>"+lblNm("msg_thank_you")+"<br/>" + localStorage.getItem('fName') + " " + localStorage.getItem('lName'),
						isHtml: true
					});
				});
			});
			
		},

		prepareFax: function() {
			var that =this;
			$('#overlay-back').fadeIn(100,function(){
				if(that.keypadview)
					that.keypadview.close();
		 		that.keypadview = new NumberKeypadView({
					el : that.$("#div-common-setlist-keypad"),
					callback: function(value) {
						var path = "";
						that.preparePDF(function(){
							fnGetLocalFileSystemPath(function(URL) {
								path = URL + 'Setlistinfo/' + that.tagid + '.pdf';
								window.resolveLocalFileSystemURL(path,function(entry){
							            entry.file(function (file) {
							                var reader = new FileReader();
							                reader.onloadend = function(evt) {
							                    console.log(">>>>>>>>>Read as binary<<<<<<<<");
							                    fnSendFax(evt.target.result,value);
							                };
							                reader.readAsArrayBuffer(file);
										}, function  (argument) {
										    console.log('error file');
							            })
								        },function() {
								            console.log('error getting file');
								        });
							});
						});
					}
				});	 		
		 		$("#div-number-keypad-title").html(lblNm("fax")+"</div>");
				$("#div-number-keypad-close").show();
				$("#div-number-decimal").hide();
				$("#div-number-keypad-number-symbol").css("width","69px").css("height","17px").css("margin-left","0");
				$("#div-number-keypad-btn-done").html(lblNm("msg_send_fax"));
			});
		},

		preparePDF: function(callback) {
			this.loaderview = new LoaderView({text : lblNm("msg_generating_pdf_c")});
			var filtertext = $("#div-common-setlist-content #txt-itemlist-search").val();
			console.log(filtertext);
			if(filtertext.length != 0){
				$("#div-common-setlist-content #txt-itemlist-search").val("");
				$("#div-common-setlist-content #txt-itemlist-search").trigger("keyup");
			}
				

			var that = this;
			var height = 0;
			var scrollheight = document.getElementById('div-itemlist').scrollHeight;
			var itemlist = document.getElementById('div-itemlist');
		    var JSONDataPDF = {};
			var todaydate = new Date();
			var dd = ("0" + todaydate.getDate()).slice(-2), mm = ("0" + (todaydate.getMonth() + 1)).slice(-2), yyyy = todaydate.getFullYear();
			JSONDataPDF.generateddate = mm+"/"+dd+"/"+yyyy;
			JSONDataPDF.setid = this.currsetid;
			JSONDataPDF.setname = this.currsetnm;
			JSONDataPDF.settype = this.currsettype;
			JSONDataPDF.tagid = this.currtagid;
			JSONDataPDF.logo = "data:image/gif;base64,R0lGODlhigA8AOYAAAELQ9nu9c4WB+iloCmTzgBmmdxgVpy0xwZKiUlomXucuNXf5scYBtMnGQAycwFwtnGUtIHD4/j9/fLMxq3N3CpQgNc7MeSQiQUnW/Xe2gBmql2Gq2Gw2h5LgEqj1SSBugAVY/jv7qLP5xA2dtUzKCNwqe7BveB+dtdQRs8kF/bY1au7zRGDwiE7awBQlIOtywEaTVl0mSlZjaDe+AM4fHCHrM/W4H7M8zNmmd7n7kWWwfD19/f5++q0rx5Ui1mu2wAJWvbm4xtvqG6+6itik95uZNY4K2yixyOOxQEmauablFKNtjlPdQ1XlcPg66O50NEvIxN4usXR3SZCcc4gEYybtkt1npPX9JGsx8/m8LXF1ubv89leUtUvIrLX6tZEOQADVDp2pzyc1JHI5BOOy////wdDgBBNhgAhav339yFhlwBdnmB8pG212xFnpzOZzDSGuQB3vzRZjhdRhvz28/PQzh9ZkLfh9WqMsThXgAw0aoWjvuPn7RZEdgghVL3O2yH5BAQUAP8ALAAAAACKADwAAAf/gGWCg4SFhoeIiYqGaRkTPUonRVxfDQJUFkVKPXV0i5+goaKjpKMhdZCSXChfXxYkDQ1fmiYZIWm4pbq7vL2KdHUDJygWULENXV0ksJgnE56+0dLToxlKRRbHyUbc3Q1UXD1p1OTl5mXWKFApDSTd79xd4BOGO3wLUitHL1pYR1h/+PA4R7CgiSKwusBbaKSBkR6FeNjQQvEFBw4eOETgoEMHHDgbnmwpSDIanQEo2LljCK+BgSCF/hywwSOLBzEefrTh0GbMmDZtIugQgmfFgpJIR9FRYoSKQpYLG5wYN8gGnCg/RIiI0CZnzq4exnjZwcPJETclFNgYmbTtoTRK/0gIeAoVXooLVMuk2fOAjJieWSTsGPIjgtYITrJoHcnjSYkSGyBoGei28oQvcy3UXSjgBLQyISDEEUPAQxZCO7J42ZIlwhgnXgiw8FKGx5EwS9RsWEG5csk0F2JtXmhBABeYgkIoYEGGAIcxhndIkCBoR4AAVyJkCeCkDYExZZw0QRAmDBvevkkGMZBi5fBuFtrXGbRjD3PnF4fcCMBjYH9CWwTFwxgscJDFCyU00QQRfVRBXXrmqICCQ+/BI5cShDyhgV8/dKjfDDsIwgNZlGWRnVhtsIDER3CUYMYZZ0whBYTlqFBJhZx9kcEgfJTwQE5i/KDfFSCK6F8ZO8xww/8QIvDghQg/sKDBGo/RQEMfNYRIozQZpIQjPF0IcAEhWGzowZlCXnHFHQNNV8gOd9zww2llbDHGBy64UIIaDughhxZbmmQAFe592RQUJtAXRhxvICHGRTeoyWZtEQkSwBgBDCLBCxq4gAAOcyRRQZaB9nKBU8ukquqqrMqFAnJl2FACEkg098MYN0Sa6X+E9LYFW16w0AYFTdBwhgwytJBHDqXuUscXJLQi7bTUVtuKVHkd8MEHcdg6RAQ3zHAkIdNJcEembu7QxgdCuIGAAzhYgQEGgDZbCh0h0KHvvvz26y+/hChwVbdvdCjkabxqOp1qY00nggftutGEGj7IoAf/AFXYay8E28bBghggd3hHbf3xWmIW0kmQBRIaaFDCEjiYkUQHUwBQg8bN7uHGB1Gw8AYBb5w5hpZGlmz0iB6wwC5aZ9CQhB5TwJAxzqIEUYcKWGet9dZcY10HrGU8sYYGHyDBAgsE4OTByIYkLIgXcTwwpQtq2OE0Bi1MsQLVoqhQXKuAtyrAq4NI0cQaQuiARLcEpD1GYOQ+OAgPHDzwwBprIOCDD3pgAAMTTPzBtyg9CBf46YcmKsgWRLiwc9lkMPcGB6shMtAWb7Bw+RouvGgxDC2AztbooFyQAl2GhjnmIDXQ4IYQazxAKxIEkNGGF5JnwVOKbzihwwdu8I6A/5Ud5N2CHwr0RvwnJ7RnKDfF6Vj4HE0I4ULL0nuAxM8iiNhGHACMgwaOsAQ4jGd8NOhAApjgBwBM4SjrE8UJ2vE+bohpchCYGAIwt4Yo8OxsHLDUGzRguQcIAQ5LuI0acICDCriwgRiTXARBoQQoQOF98SEBPQSxgATYAQEIyJMQhNCylh1hICKIwhFysAQ3uCEMWMADHhKgBxjgDQYAYAKzZjgKE3whBTg0DqykIIMz+ACIJXCDCzi4Bjcc4QAzysELwgBEOyThjnoAXQxa0IIZcZEUITgBFSj0pc585g960IMa5qCG8eTpkQoizx52IIUO0EAGNdDCAtgigRX8Qf+GfxTFBFAwSEOlYCqD+EMLgNCHMBDhDGaIZSzrZiU0yEEBNYiBHwexhRXYIJS9MAEpKVghbNEnBgBoQQU60IHNwaiMDojmHf0QgxA9SCZ8AGY0JlCEWCCvLg3gwo4GoQUqWlEPfehDBRJQAXVWgI8HkNwCViAFUGqTF0G4QCWICU4SDKAQNohBFWFgxSn44aAwxIIEtjARKajvntKgQw+w4c26yAMFO+TlCtjAhClEjaAw8EMLEjATPtgTotQIQQ+GsY7jfZMb30CBOOqRAxtIQQo2yMFDUYqUDKzUAF/owjGQkSp5WOAEJggBT4mXBhWkwgCsIEEyYiEAAUCBCxf/6IEKbrFU4oUgA3UwQQ8GoAQlXEASRTjBAHowgSCctKtwjatc50rXutqVRjzIgV73ytfe5HWTi9gCXwc7vNrwYbB8JVqdAGuIexS2DIdFbA7eWhktEIEG0cysZmUAwQVUAAh4SAQfEqDZ0kaTDVpagAxMm1kaYGEQVkBDAh6aAzkAAbWCyAEOWBtNK2xxS2ZwwBnwsAcIGBcCe8DDCEAwNc+CNhFPAEESNrAHBRwXAgqwQhJGUK8F+AAEVjiAeA/whAPIAQx5yKYEYjvbQuRABmDAbRkOAIIO7AELCsjvHvbQhySENlBWksNja7MCLbDFuf89BH2TQIFDrAANI9hb/xm8C4TXFkIBYKgAs9YrW9raVr5YAEECFCsILUhhwL4J8Ba38IQWP+EPmuThZxNsiOgmoV6FeAKEJexd2eZXvwrwARogIAgOt5cQtb2tlg6AhjlsAA8biHKUJUPZtqi4xEkAQWaHLOPnIsLGOM7QjnnoAwdY6cyYTQJni8xeDys5t0TgbTQ3UKorI0kKf8iBFnyQBAV0mcY5lm6YB6HjCJMZCBDYgaJJhAf0jsTIbpZvbRZA6UrrlbRmqDMN5JDNQvBBDn3+cyIW3OAaj3nCPgDCHgyxBzA8sDbsBWVt4xsiHuwBBxIuhBUckOlAlfmSRAi2sIlgBjSsesIVAMEZhv8dbBnQ+cFosAMOmE0EIXOXhx1oMgu3HTMgLEsQMUCDGah9WSDUYCA7SAAI5jBtZgcXB6VawAZ8MId627vePmBDpxcQMzvcu95nsAKSIPDvf8vg2GXIQQLMWPA5WGGXUrACvRtuBQiW4Q+gavgc8NDpZpXr4/YEucgjJ/KTllwRJ0dEyu/KcoKEAGxloEMGPjOIEKigDjivwwSUWogg3BznKsgLIWTO857nHOc0L0QGMiB0Qny16Di7gJioEoQvMEB1g0hDEaiwCbNeYJyDyAA3yhoJFPyzEBOgwgkM0YMUFIGsTCGcIQZAgrs0XRBKYABEqAYcFJxARwMwAEL2nvWlrZd1AEkXhAoaUARCnGo+hJhAZwyhBCpkNANBN4TUJ6ACARTh7koQAOE1loYTGKAMPYBC400AhdHrpQgpYKsJJrBzpUMBBUsPKwoMAPUypH3thRgAFQbg8wwUoQhgAw4UTlDWEyw/6QMQk9V03nsIlf70ZQiCUtvu+tILwBt67zlmlvGNIlT/94aYgBHaswwB8J4QPWAAhgYR/bMPou3s/4broxEIADs=";
				
			var id,description,qty;
			var MainJSONDataPDF = new Array();

			var AttrJSON = new Array();
			var AttrSecondJSON = new Array();
			var AttrThirdJSON = new Array();
			var AttrFourthJSON = new Array();
			this.SubJSONParts = [];
			var currJSON = $.parseJSON(JSON.stringify(JSONDataPDF));
			var currParts = $.parseJSON(JSON.stringify(this.results));

			for( var k=0; k < currParts.length ; k++) {
				id = currParts[k].ID;
				description = currParts[k].Description;
				qty = currParts[k].Quantity;
				if(k+1 < currParts.length || k+1 == currParts.length)
					height = height + itemlist.childNodes[k+1].scrollHeight;

				if(height <= 1550) 
					AttrJSON.push({"ID" :id, "Description" : description, "Quantity" : qty});

				else if(height > 1500 && height <= 3430 )
					AttrSecondJSON.push({"ID" :id, "Description" : description, "Quantity" : qty});

				else if(height > 3430 && height <= 5310)
					AttrThirdJSON.push({"ID" :id, "Description" : description, "Quantity" : qty});

				else
					AttrFourthJSON.push({"ID" :id, "Description" : description, "Quantity" : qty});

					
			}
			currJSON.JSONSubParts = $.parseJSON(JSON.stringify(AttrJSON));
			currJSON.JSONSecondSubParts = $.parseJSON(JSON.stringify(AttrSecondJSON));
			currJSON.JSONThirdSubParts = $.parseJSON(JSON.stringify(AttrThirdJSON));
			currJSON.JSONFourthSubParts = $.parseJSON(JSON.stringify(AttrFourthJSON));

			MainJSONDataPDF.push(currJSON);
			MainJSONDataPDF[0].totalpages = 1;

			if(scrollheight > 1550){
				MainJSONDataPDF[0].twoPages = "two";
				MainJSONDataPDF[0].totalpages = 2;
				if(height > 3430) {
					MainJSONDataPDF[0].threePages = "three";
					MainJSONDataPDF[0].totalpages = 3;
					if(height > 5310) {
						MainJSONDataPDF[0].fourPages = "four";
						MainJSONDataPDF[0].totalpages = 4;
					}
				}
			}


			createPartListPDFPath(function() {
					window.html2pdf.create(that.template_PDF(MainJSONDataPDF),"~/Documents/Globus/Setlistinfo/"+ that.tagid +".pdf", // on iOS,
		               function(status) {
			               that.pdfpath = "file://"+status.match(/\(([^)]+)\)/)[1];
			               console.log(that.pdfpath);
			               if(filtertext.length != 0) {
								that.$("#div-common-setlist-content #txt-itemlist-search").val(filtertext);
								that.$("#div-common-setlist-content #txt-itemlist-search").trigger("keyup").trigger("blur");
							}

			               that.loaderview.close();
			               callback();
			           },function(status) {
			           	console.log("Failure");
		               	console.log(status);
		               	if(filtertext.length != 0) {
							that.$("#div-common-setlist-content #txt-itemlist-search").val(filtertext);
							that.$("#div-common-setlist-content #txt-itemlist-search").trigger("keyup").trigger("blur");
						}
		               	that.loaderview.close();
		               	fnShowStatus(lblNm("msg_failure_pdf"));
		           });
			});


		},

     
		// Close the view and unbind all the events
		close: function() {
			this.unbind();
			this.undelegateEvents();
			if(this.keypadview)
				this.keypadview.close();
			$("#btn-home").unbind("click");
			$("#btn-settings").unbind("click");
			$("#btn-back").unbind("click");
			this.$el.empty();
//			showMsgView("011");
			this.$el.hide();
		}

	});	

	return SetListView;
});
