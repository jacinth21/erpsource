/**********************************************************************************
 * File:        GmAddressListView.js
 * Description: This view contains the list of addresses.
 * Version:     1.0
 * Author:      ejayabalan
 **********************************************************************************/

define([
    'jquery', 'underscore', 'backbone', 'handlebars',
    'global', 'notification', 'device',
    'addresscollection', 'addressmodel', 'addressview', 'dropdownview','searchview'
],

function ($, _, Backbone, Handlebars, Global, Notification, Device, AddressCollection, AddressModel, AddressView, DropDownView, SearchView) {

	'use strict';

	var AddressListView = Backbone.View.extend({

		tagName: "ul",

		template: fnGetTemplate(URL_Common_Template,"GmAddressList"),

		initialize: function(options) {
			this.addresstemplate = options.addresstemplate;
			this.currentcollection = this.collection;
			var that = this;
			this.template_URL = options.template_URL;
            this.callbackOnReRender =options.callbackOnReRender;
			
		   	this.render();
		  
		},

		render: function () {

			var that = this;

			this.$el.html(this.template());
			
			var address = new AddressModel();
			
			var appendArea = "#div-address-list";
			
			var colors = new Array();

			colors =  ["#800", "#960", "#036", "#606", "#030","#006","#603", "#330","#2e2f3b", "#8B7765","#4F4F2F","#8B8682","#330000","#802A2A","#5E2612","#85ADFF","#FF66FF","#6B008F","#E6005C","#B2FF99","#036", "#606", "#030","#4F4F2F","#8B8682","#330000","#802A2A"];
			//colors =  ["#006633", "#FFCC33", "#FF00FF", "#0000FF", "#33FF33","#660033","#606060", "#66FFFF","#CC6600", "#666633","#FFB84D","#4D944D","#2952CC","#AD3333","#CC7A00","#85ADFF","#FF66FF","#6B008F","#E6005C","#B2FF99"];

			var i=0;
			this.currentcollection.each(function(address) {
				
				var addressview = new AddressView({ model: address });
				
				that.$(appendArea).append(addressview.render({template_URL : that.template_URL, template: that.addresstemplate}).el);
				addressview.$(".div-address-row").css('background-color', colors[i]);
				i++;
			}, this);
		},
		
        close: function() {
        	this.unbind();
        	this.undelegateEvents();
        	this.$el.empty();
        }
	});

	return AddressListView;
});


