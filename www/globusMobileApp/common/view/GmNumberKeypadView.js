/**********************************************************************************
 * File:        GmNumberKeypadView.js
 * Description: Subview 
 * Version:     1.0
 * Author:      tvenkateswarlu
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone', 'handlebars', 'global'

        ], function ($, _, Backbone, Handlebars, Global) {

	'use strict';
	
	var DropDownView = Backbone.View.extend({

		events : {
			"click .div-number-keypad-keys" : "getKeyValues",
			"click #div-number-keypad-number-symbol" : "deleteKeyValues",
			"click #div-number-keypad-close" : "closeKeypad",
			"click #div-number-keypad-btn-done" : "displayQuantity",
			
		},
		
		template: fnGetTemplate(URL_Common_Template,"GmNumberKeypad"),
		
		initialize: function(options) {
			this.el = options.el;
			this.target = options.target;
			this.callback = options.callback;
			this.render();    
		},
		
		

		render: function () {
			var that = this;
			$(this.el).html(this.template());
			return this;
		},
		
		getKeyValues:function(event){
			
			var targetid = event.currentTarget.id;
			var targettext = $("#"+targetid).text();
			if ( targetid == "div-number-decimal" && $("#div-number-qty-entry-text").text() == "") {
				targettext = "0.";
			}
			
			if( $("#div-number-qty-entry-text").text() != "" ) {
				if( targetid == "div-number-decimal" && $("#div-number-qty-entry-text").text().indexOf(".") >= 0 ) {
					targettext = "";
				}
			}
			$("#div-number-qty-entry-text").append(targettext);
		},
		
		deleteKeyValues:function(event){			
			var number_value = $("#div-number-qty-entry-text").text();
			var str = number_value.substring(0, number_value.length-1);
			$("#div-number-qty-entry-text").text(str);
			
		},
		
		closeKeypad:function(event){
			$('#overlay-back').fadeOut(100);
			$("#div-number-keypad").hide();
		},
		
		displayQuantity:function(event){
			var qty_val;
			//console.log(event.currentTarget.id);
			if(($("#div-number-qty-entry-text").text()) == ""){
				showMsgView("052");
			}
			if(($("#div-number-qty-entry-text").text()) != "" && ($("#div-number-qty-entry-text").text()) == "0"){
				showMsgView("053");
			}
			if($("#div-number-qty-entry-text").text() != "" && $("#div-number-qty-entry-text").text() != "0"){
//				showMsgView("011");
			}
			if($("#div-number-qty-entry-text").text() != "")
				qty_val = $("#div-number-qty-entry-text").text();
			
			this.callback(qty_val);
			
			$("#div-number-keypad").hide();
			$('#overlay-back').fadeOut(100);
		},

		close: function() {
			console.log("closing...");
			$(document).unbind("click."+this.cls);
			this.unbind();
			this.undelegateEvents();
		}
		
	});

	return DropDownView;
});
