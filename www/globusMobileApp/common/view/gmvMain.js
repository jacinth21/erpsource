define([
        'jquery', 'underscore', 'backbone', 'handlebars', 'global', 'gmvField', 'gmmUser','gmvImageCrop',

    	// Pre-Compiled Template
    	'gmtTemplate'
], 

function ($, _, Backbone, Handlebars, Global, GMVField, GMMUser, GMVImageCrop, GMTTemplate ) {

	'use strict';

	GM.View.Main = Backbone.View.extend({

		el: "body",
        
        container: "#div-crm-overlay-content-container",

		events: {
			"click #btn-logout": "logout",
            "click #mobile-menu-top-user-logout": "logout",
			"click #div-crm-overlay-content-main" : "closeOverlay",
             "click #ul-header-right-pic,#mobile-menu-top-user, #ul-header-right-pic-menubar" : "openPicturePopup",
		},

		/* Templates */
		template: fnGetTemplate(URL_Common_Template, "gmtMain"),
        template_picture: fnGetTemplate(URL_Common_Template, "gmtUserProfilePic"),
		
		initialize: function() { 
            
			$("body").removeClass("background-2").addClass("background-1 background-image");
			$("#div-header").removeClass("background-2").addClass("background-1 background-image");
			this.render();
            onrefresh();
            },

		render: function () {
			this.$el.html(this.template());
			this.$("#div-sales").hide();
			this.$("#div-messagebar span").hide();
			this.$("#tmpAdminNon").hide();
            var lastScrollTop = 0;
			$(window).scroll(function() {
				var scrollPercentage = 100 * $(this).scrollTop() / ($(document).height()-$(this).height());
				var st = $(this).scrollTop();
			   if (st > lastScrollTop){
			   		if(scrollPercentage>20) {
			   			$("#div-header").slideUp('fast');
			   		}
			   }
			   else{
		   			$("#div-header").slideDown('fast');
			   }
			   lastScrollTop = st;
			})

			return this;
		},

        openPicturePopup: function(){
            
                openPicturePopup (this, GMVImageCrop);             
        },
        
		logout: function(e) {
			localStorage.removeItem('token');
			localStorage.removeItem("companyid");
			localStorage.removeItem("divisionid");
			localStorage.removeItem("zoneid");
			localStorage.removeItem("regionid");
			localStorage.removeItem("fieldsalesid");
            localStorage.removeItem("DftTradeshow");
            localStorage.removeItem("GlobalVariables");
            GM.Global.UserPicID = undefined;        // Handling User Profile Pic
            $(".div-user-icon-pic img").attr("src","");  
			window.location.href = "#";
		},

		setAdmin: function() {
			$(".adminSurgeon").show();
			$(".edit").hide();
		},

		setUser: function() {
			$(".adminSurgeon").hide();
			$(".edit").hide();
		},

		closeOverlay: function(event) {
			if(event.target.id == "div-crm-overlay-content-main" || event.target.id == "spn-crm-overlay-content-close" || event.target.id == "span-overlay-save") {
				this.$("#div-crm-overlay-content-main").hide();
				this.$("#div-crm-overlay").hide();
			}
		},
        
        validate: function(e){
            var user = new GMMUser();  
            new GMVField({el: $(e.currentTarget), model: user});
        },
	});
	return GM.View.Main;
});
