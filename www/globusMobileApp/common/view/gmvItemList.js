/**********************************************************************************
 * File:        GmItemListView.js
 * Description: This view contains the list items.
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/

define([
    'jquery', 'underscore', 'backbone', 'handlebars',
    'global',
    'gmcItem', 'gmmItem', 'gmvItem',

    // Pre-Compiled Template
    'gmtTemplate'
],

function ($, _, Backbone, Handlebars, Global, GMCItem, GMMItem, GMVItem, GMTTemplate) {

	'use strict';

	var minCollapsible = 10;

	var GMVItemList = Backbone.View.extend({

		events: {
			
			"click .btn-column-head": "sortItems",
            "click #div-crmitemlist-detail-search-x": "clearKeyword",
			
			"keyup #txt-crmitemlist-search": "searchItemList",
			
			"click .gmFilter" : "openFilter",
			"click .li-filter-item" : "selectFilter",
			"click .li-filter-item-select-all" : "selectAllFilter",
			"keyup .txt-filter-search" : "searchFilter",
            "click #btn-crmitemlist-download" : "downloadCSV",
            "click #btn-crm-list-piechart" : "openChart",
            "click #li-filter-done": "closeFilter",
            
            //phone events
            "click #div-sort-phone-btn": "sortPhoneData",
            "click #div-filter-phone-btn": "filterPhoneData",
            
			
           },

		template: fnGetTemplate(URL_Common_Template,"gmtItemList"),
        template_phone: fnGetTemplate(URL_Common_Template,"gmtPhoneItemList"),
		template_Filter: fnGetTemplate(URL_Common_Template,"gmtFilterContainer"),

		initialize: function(options) {
			this.itemtemplate = options.itemtemplate;
			this.columnheader = options.columnheader;
		    this.renderType = options.renderType;
            this.sortoptions = options.sortoptions;
            this.currentcollection = this.collection;
            this.intResultCount =this.collection.length;
            this.template_URL = options.template_URL;
            this.callbackOnReRender =options.callbackOnReRender;
            this.searchContainer = options.searchContainer;
            this.filter = {};
            this.filterKeys = new Array();
            this.filterItems = {};
            this.filterOptions = options.filterOptions;
            this.csvFileName = options.csvFileName;
            this.color = options.color;
            this.mainTemplate = options.itemtemplate;
            console.log(this.currentcollection)
            GM.Global.FilterList = [];
            var that = this;
			this.currentcollection.bind('sort', function(){
				that.render();
				if(that.callbackOnReRender!=undefined){
                    console.log(that.currentcollection)
                    if(that.callbackOnReRender!=undefined && that.currentcollection!= undefined)
                        that.callbackOnReRender(that,that.sortId, that.currentcollection.toJSON());
                    else
                        that.callbackOnReRender(that);
                }
					
			});
             if(this.sortoptions != undefined) {
            	this.DefaultSort = options.sortoptions[0].DefaultSort;
 				this.sortoptions = this.sortoptions[0]["Elements"];
            }
		   	if(this.renderType != undefined) {
			   	this.currentcollection.sortAsc("Name");
			}
			else if(this.DefaultSort != undefined) {
				this.currentcollection.sortAsc(this.DefaultSort);
			}
            
            this.currentcollectioncopy = this.currentcollection;
            
		   	this.selectvalue = 0;
		   	this.render();
		   	this.sortFirst();
		},

		render: function () {
			var that = this;
            console.log(window.innerWidth<700)
            if(window.innerWidth<700){
                this.$el.html(this.template_phone({"module": this.color.replace("gmFont","")}));
            }
			else if(this.renderType == 1) {
				this.$el.append(this.template());
			}
			else {
				this.$el.html(this.template());
			}
			
			switch(this.columnheader) {
				case 0:
					this.$("#div-crmitemlist-colhead").remove();
					this.$(".div-crmitemlist-search").remove();
					break;

				case 1:
					this.$("#div-crmitemlist-colhead").remove();
					break;

				case 2:
					this.$(".div-crmitemlist-search").remove();
					break;
			}
            
            this.$("#btn-crmitemlist-download i").addClass(this.color);
            this.$("#btn-crm-list-piechart i").addClass(this.color);

			var item = new GMMItem();
			var Current_Alphabet;
			var appendArea = "#div-crmitemlist-coldata";
            
            this.currentcollection.each(function(item, index) {
//                console.log(JSON.stringify(crmitem));
				if((that.renderType == 2) && (that.currentcollection.length > minCollapsible)) {
					if((item.get("Name").substring(0,1)!=Current_Alphabet) || (Current_Alphabet==undefined)) {
						Current_Alphabet = item.get("Name").substring(0,1).toUpperCase();
						var li = document.createElement('li');
						li.innerHTML = Current_Alphabet;
						li.className = "li-letter-above";
						var spn = document.createElement('span');
						spn.className = "spn-collpase-count";
						var spnCollapse = document.createElement('span');
						spnCollapse.className = "spn-indicate-collpase";
						spnCollapse.innerHTML = "+";
						li.appendChild(spnCollapse);
						li.appendChild(spn);
						that.$("#div-crmitemlist-coldata").append(li);
						var ul = document.createElement('ul');
						ul.id = "ul-under-" + Current_Alphabet.toLowerCase();
						that.$("#div-crmitemlist-coldata").append(ul);
						appendArea = ul;
					}
				}
				else if((that.renderType == 2) && (that.itemtemplate == "GmSubItem")) {
					that.itemtemplate = "GmItem";
				}
                item.set("index", index+1);
                // This is where each list item is populated by default
                var itemview = new GMVItem({ model: item, template_URL : that.template_URL, 
                                                   template: that.itemtemplate,
                                                   el : that.$("#div-crmitemlist-coldata")});
//				that.$(appendArea).append(crmitemview.render());
				}, this);
            
            

            if((that.renderType == 2) && (that.currentcollection.length > minCollapsible)) {
				$(".li-letter-above").each(function() {
					var liAbove = this;
					var alphabet = $(this).text().toLowerCase().replace('+','');
					var count = 0;
					$("#ul-under-"+alphabet).find('li').each(function() {
						$(liAbove).find('.spn-collpase-count').html(count+1);
						count++;
					});
					
				});
			 }

 			if(this.sortoptions!=undefined) {
 				
 				//for dropdown headers
       			var select = document.createElement('select');
        		select.id = "sel-attrib";
                select.className = "hide";
        		var rightSelect = document.createElement('select');
        		rightSelect.id = "sel-sort-by";
                rightSelect.className = "hide";
        		var rightSelectOption1 = document.createElement('option');
        		var rightSelectOption2 = document.createElement('option');
        		rightSelectOption1.value = "ascending";
        		rightSelectOption2.value = "descending";
        		rightSelectOption1.innerHTML = "Ascending";
        		rightSelectOption2.innerHTML = "Descending";
        		rightSelect.appendChild(rightSelectOption1);
        		rightSelect.appendChild(rightSelectOption2);

        		for(var i=0; i<this.sortoptions.length; i++) {
        		    var option = this.sortoptions[i];
					var anOption = document.createElement('option');
					anOption.innerHTML = option["DispName"];
					anOption.value = option["ID"];
					select.appendChild(anOption);
							
					this.$("#div-crmitemlist-colhead").append(select);
					this.$("#div-crmitemlist-colhead").append(rightSelect);
        		}
			   
                var ulheader = document.createElement('ul');
                this.$("#div-crmitemlist-colhead").append(ulheader);
                
        	   //for row header
			   for(var i=0; i<this.sortoptions.length; i++) {
			   	
					var option = this.sortoptions[i];
                    
					var anOption = document.createElement('li');
					var divArrow = document.createElement('div');
					var iFilter = document.createElement('div');
					
					// console.log(!!_.findWhere(  this.filterOptions,this.sortoptions[i]["ID"] ));
					
					if(_.contains(  this.filterOptions,this.sortoptions[i]["ID"] )){
						$(iFilter).html("<i class='fa fa-lg fa-filter gmFilter'></i>");
						$(iFilter).addClass("gmAlignHorizontal");
					}
					
                    anOption.innerHTML = option["DispName"];
					$(anOption).append(iFilter);
					$(anOption).append(divArrow);
					var ClassName = option["ClassName"];
					anOption.id = option["ID"];
					this.$(ulheader).append(anOption);
					$(anOption).addClass("btn-column-head");
					$(anOption).addClass(ClassName);
                    $(divArrow).attr('ID','td-arrow-head');
                   $(divArrow).addClass("gmAlignHorizontal gmPadLeft10p");
                   
                   $(divArrow).html("<i class='fa fa-arrow-down fa-lg gmArrow'></i>")

                   if(that.DefaultSort==this.sortoptions[i].ID){
                        $(anOption).addClass("ascending");
                        that.$("#" + that.DefaultSort).find('.gmArrow').show();
                   }
                   
                   else
                       this.$("#"+this.sortoptions[i].ID).find('.gmArrow').hide();

                   $(iFilter).addClass("gmFloatRight");
                   

				}

			}
 			

 			if(this.searchContainer!=undefined) {
 				$(this.searchContainer).html(this.$("#div-sale-keyword-detail").html());
 				this.$("#div-sale-keyword-detail").hide();
 				$(this.searchContainer).find("#txt-crmitemlist-search").bind("keyup", function(e) {
 					that.$("#txt-crmitemlist-search").val(e.currentTarget.value).trigger("keyup");
 				});
 				$(this.searchContainer).find("#txt-crmitemlist-search").bind("blur", function(e) {
 					that.$("#txt-crmitemlist-search").trigger("blur");
 				});
 				$(this.searchContainer).find("#txt-crmitemlist-search").bind("focus", function(e) {
 					that.$("#txt-crmitemlist-search").trigger("focus");
 				});
 				$(this.searchContainer).find("#div-crmitemlist-detail-search-x").bind("click", function(e) {
 					that.$("#div-crmitemlist-detail-search-x").trigger("click");
 				});
 			
 			}


 	
    		this.$(".btn-column-head").each(function() {
				if(_.contains(that.filterKeys,this.id)) 
					$(this).find('i').css('color','red');
			});

 			if(this.callbackOnReRender!=undefined)
	 			this.callbackOnReRender(this);

            this.$(".btn-column-head").each(function() {
				$(this).find('i').addClass(that.color);
			});
            resizeFont();
 			return this;
		},
       
		/*show the sorting arrow for the sorted headers*/
		sortFirst: function() {
			var that = this;
			this.$(".btn-column-head").each(function() {
					if($(this).attr("id")==that.DefaultSort) {
						that.$("#"+that.DefaultSort).find('.gmArrow').show();
						that.$("#"+that.DefaultSort).removeClass("ascending").addClass("descending");
					}
				});
			this.$("#sel-attrib").find('option').each(function() {
				if($(this).val() == that.DefaultSort)
					$(this).attr("selected","selected");
			})
//				that.DefaultSort = null;
		},
        /*triggers the keyup event in the search textbox*/
		clearKeyword: function(e) {
			e.preventDefault();
			if(this.searchContainer!=undefined) {
				$(this.searchContainer).find("#txt-crmitemlist-search").val("");
			}
			this.$("#txt-crmitemlist-search").val("");
			this.$("#txt-crmitemlist-search").trigger("keyup");
		    $("#div-search-count").hide();
		},

        /*Shows and hides the close button in search textbox*/
		setX: function(wordLength) {
			if(wordLength == 0) {
				this.$("#div-crmitemlist-detail-search-x").addClass("hide");
				if(this.searchContainer!=undefined) {
					$(this.searchContainer).find("#div-crmitemlist-detail-search-x").addClass("hide");
				}
			}
			else {
				if(this.searchContainer!=undefined) 
					$(this.searchContainer).find("#div-crmitemlist-detail-search-x").removeClass("hide");
				else
					this.$("#div-crmitemlist-detail-search-x").removeClass("hide");
			}
		},
		
		
		/*Process the search and render the result*/
		searchItemList: function (e) {
			var intSearchCount = 0;
			var letters = $(e.currentTarget).val();
			var wordLength = letters.length;
			var that = this;
			this.letters = letters;
			this.setX(wordLength);

            this.$("#div-crmitemlist-coldata").empty();

            var letters = $(e.currentTarget).val();
            if(letters.length >=1){
    			$("#div-search-count").show();
    			var crmitems = this.collection.search('search',letters);
                console.log(crmitems)
				this.currentcollection=new GMCItem(crmitems);
				this.currentcollection.bind('sort', this.render, this);
				var gmmItem = new GMMItem();
				that.$("#div-crmitemlist-coldata").empty();
				_.each(crmitems,function(item, index){
                    item.set("index", index+1);
					var itemview = new GMVItem({ model: item, template_URL : that.template_URL, 
                                                   template: that.itemtemplate,
                                                   el : that.$("#div-crmitemlist-coldata")});
				});
				
				intSearchCount = this.currentcollection.length;
			}
			else if(letters.length ==0) {
	   			$("#div-search-count").hide();
				this.currentcollection = this.collection;
				this.render();
				this.$("#txt-crmitemlist-search").focus();
			}

			if(that.$("#div-crmitemlist-coldata").text().trim().length<1) {
				that.$("#div-crmitemlist-coldata").html("<li class='li-no-display'>No Search Results Found!</li>");
			}
			
            $("#span-search-count").html("&nbsp;" + intSearchCount + "&nbsp;");


            var selected = this.collection.getSelected();
			for(var i=0;i<selected.length;i++) {
				this.$el.find("#" + selected[i]).parent().addClass("selected");
			}

			if(this.callbackOnReRender!=undefined)
	 			this.callbackOnReRender(this);
            
            resizeFont();
			
		},
        
		/* Sort items in Ascending/Descending order */
		sortItems: function(e) {
			var elem = e.currentTarget;
            var currentClass= $(elem).attr('class');
            console.log(currentClass);
			this.sortId= $(elem).attr('id');
            console.log(this.sortId);
			
			if ( currentClass.indexOf("ascending")>0 ) {
				this.currentcollection.sortAsc(elem.id);
				this.$("#txt-crmitemlist-search").val(this.letters);

				if(this.letters!=undefined) {
					this.setX(this.letters.length);
				}
				this.$("#" + elem.id).removeClass("ascending").addClass("descending");
				this.$("#" + elem.id).find("#td-arrow-head .fa").removeClass("fa-arrow-up fa-lg").addClass("fa-arrow-down fa-lg").show();
	 		}
			
			else if( currentClass.indexOf("descending")>0 ){
				this.currentcollection.sortDesc(elem.id);
				this.$("#txt-crmitemlist-search").val(this.letters);
				if(this.letters!=undefined){
					this.setX(this.letters.length);
				}
             
				this.$("#" + elem.id).removeClass("descending").addClass("ascending");
			    this.$("#" + elem.id).find("#td-arrow-head .fa").removeClass("fa-arrow-down fa-lg").addClass("fa-arrow-up fa-lg").show();
			}
            else{
                this.currentcollection.sortAsc(elem.id);
				this.$("#txt-crmitemlist-search").val(this.letters);

				if(this.letters!=undefined) {
					this.setX(this.letters.length);
				}
				this.$("#" + elem.id).addClass("descending");
				this.$("#" + elem.id).find("#td-arrow-head .fa").addClass("fa-arrow-down fa-lg").show();
            }
            
            var id = $(e.currentTarget).attr("id");
            this.$(".btn-column-head").each(function(index){
                    if($(this).attr("id")!=id){
                        $(this).find("#td-arrow-head .fa").hide();
                    }
            });
                    
//            var id = $(e.currentTarget).attr("id");
//
//            $(".btn-column-head").each(function(index){
//                    if($(this).attr("id")!=id){
//                        $(this).find("#td-arrow-head").removeClass();
//                    }
//            });
            if(this.callbackOnReRender!=undefined)
                this.callbackOnReRender(this);
			
		},
        
        //to sort listed items in phone
        sortPhoneData: function(e){
            var elem = "#div-sort-phone-btn";
            var module = $(elem).attr("module");
            var sort_option;
            if(module=="merc" || module=="training" || module=="lead")
                sort_option = "actnm";
            else if(module == "physicianvisit")
                sort_option = "acttype";
            else if(module == "salescall")
                sort_option = "phynm";
            else if(module == "fieldsales" && GM.Global.FieldSalesRegion == "Y")
                sort_option = "regnm";
            else 
                sort_option = "title";
            
            if($(e.currentTarget).hasClass("ascending")){
                this.currentcollection.sortDesc(sort_option);
				this.$("" +elem).removeClass("ascending").addClass("descending");
            }
            else if($(e.currentTarget).hasClass("descending")){
                this.currentcollection.sortAsc(sort_option);
				this.$("" +elem).removeClass("descending").addClass("ascending");
            }
            else{
                this.currentcollection.sortAsc(sort_option);
				this.$("" +elem).addClass("ascending");
            }
            console.log(module, sort_option, this.currentcollection)
        },
		
		openFilter: function(event) {
            console.log(GM.Global.FilterList);
        	event.stopImmediatePropagation();
        	var elem = $(event.currentTarget).parent().parent();
        	this.filterKey = $(elem).attr('id');
            console.log(this.filterKey);
        	var filterIDs;
//        	if(this.filterKeys.length==0)
	        	filterIDs = _.uniq(_.pluck(this.collection.toJSON(),this.filterKey));
//	        else
//	        	filterIDs = this.filterItems[this.filterKey];

            console.log(filterIDs)
        	var that = this;

        	this.$("#div-filter-container").html(this.template_Filter(filterIDs)).show().css("width",$(elem).width());
        	this.$("#div-filter-container").css("top",$(elem).offset().top + $(elem).height() + parseInt($(elem).css("padding-top")) + parseInt($(elem).css("padding-bottom")) + "px");
        	this.$("#div-filter-container").css("left", $(elem).offset().left +"px");
        	this.$("#div-filter-container").css("padding-left", $(elem).css("padding-left"));
        	this.$("#div-filter-container").css("padding-right", $(elem).css("padding-right"));

        	this.cls = parseInt(Math.random()*1000000).toString();

        	this.$("#div-filter-container").find("*").each(function() {
				$(this).addClass(that.cls);
			});

			$(document).bind("click."+that.cls, function(event) {
				if(!$(event.target).hasClass(that.cls)) {
					that.closeFilter();
				}
			});
            console.log(this.filterKeys)
            console.log(this.filterKey)
        	if(GM.Global.FilterList[this.filterKey] && GM.Global.FilterList[this.filterKey].length) {
        		this.$("#div-filter-container .li-filter-item").each(function() {
        			if(!_.contains(GM.Global.FilterList[that.filterKey], $(this).find(".div-filter-name").html().trim()))
                        $(this).find('i').toggleClass('fa-square-o fa-check-square-o');
	        	});

	        	if(this.$(".li-filter-item").find('.fa-check-square-o').length>0 && this.$(".li-filter-item").find('.fa-square-o').length>0) 
					this.$(".li-filter-item-select-all i").removeClass('fa-check-square-o').removeClass('fa-square-o').addClass('fa-square');
				else if(this.$(".li-filter-item").find('.fa-check-square-o').length == this.$(".li-filter-item").length)
					this.$(".li-filter-item-select-all i").addClass('fa-check-square-o').removeClass('fa-square-o').removeClass('fa-square');
				else if(this.$(".li-filter-item").find('.fa-square-o').length == this.$(".li-filter-item").length)
					this.$(".li-filter-item-select-all i").removeClass('fa-check-square-o').addClass('fa-square-o').removeClass('fa-square');
        	}
        },
        
        filterPhoneData: function(event){
            var that = this;
            var elem = $("#div-filter-phone-container");
            event.stopImmediatePropagation();
            console.log($(".filter-show").length);
            if($(".filter-show").length>0){
                    that.$("#div-filter-phone-container").removeClass("filter-show");
                    that.closePhoneFilter();
                }
            else{
                var elem = "#div-sort-phone-btn";
                var module = $(elem).attr("module");
                
                if(module=="merc" || module=="training" || module=="lead")
                    this.filterKey = "actnm"
                else if(module == "fieldsales" && GM.Global.FieldSalesRegion == "Y")
                    this.filterKey = "regnm"
                else if(module == "physicianvisit")
                    this.filterKey = "acttype"
                else if(module == "salescall")
                    this.filterKey = "phynm"
                else 
                    this.filterKey = "title"
                var filterIDs;
               filterIDs = _.uniq(_.pluck(this.collection.toJSON(),this.filterKey));

                this.$("#div-filter-phone-content").html(this.template_Filter(filterIDs)).show().css("width","98%");
                this.$("#div-filter-phone-content").css("top",$(event.target).parent().parent().parent().offset().top + $(event.target).parent().parent().parent().height()+ "px");
                this.$("#div-filter-phone-content").css("left", "1%");
                this.$("#div-filter-phone-content").css("padding-left", $(elem).css("padding-left"));
                this.$("#div-filter-phone-content").css("padding-right", $(elem).css("padding-right"));

                this.cls = parseInt(Math.random()*1000000).toString();
                if($(".filter-show").length==0)
                    this.$("#div-filter-phone-container").addClass("filter-show");
                this.$("#div-filter-phone-container").find("*").each(function() {
                    $(this).addClass(that.cls);
                });
                console.log(GM.Global.FilterList[this.filterKey]);
                if(GM.Global.FilterList[this.filterKey] && GM.Global.FilterList[this.filterKey].length) {
        		this.$("#div-filter-phone-container .li-filter-item").each(function() {
        			if(!_.contains(GM.Global.FilterList[that.filterKey], $(this).find(".div-filter-name").html().trim()))
                        $(this).find('i').toggleClass('fa-square-o fa-check-square-o');
	        	});

	        	if(this.$(".li-filter-item").find('.fa-check-square-o').length>0 && this.$(".li-filter-item").find('.fa-square-o').length>0) 
					this.$(".li-filter-item-select-all i").removeClass('fa-check-square-o').removeClass('fa-square-o').addClass('fa-square');
				else if(this.$(".li-filter-item").find('.fa-check-square-o').length == this.$(".li-filter-item").length)
					this.$(".li-filter-item-select-all i").addClass('fa-check-square-o').removeClass('fa-square-o').removeClass('fa-square');
				else if(this.$(".li-filter-item").find('.fa-square-o').length == this.$(".li-filter-item").length)
					this.$(".li-filter-item-select-all i").removeClass('fa-check-square-o').addClass('fa-square-o').removeClass('fa-square');
        	}
                
                
                
                
                
                
                
            }
            
            resizeFont();
        },

        closeFilter: function() {
			this.$("#div-filter-container").hide();
			$(document).unbind("click."+this.cls);
            
            this.$("#div-filter-phone-container").removeClass("filter-show");
			this.$("#div-filter-phone-content").hide();
            
        },
        
        closePhoneFilter: function() {
            this.$("#div-filter-phone-container").removeClass("filter-show");
			this.$("#div-filter-phone-content").hide();
        },

        selectFilter: function(event) {
        	event.stopImmediatePropagation();
        	var elem = $(event.currentTarget), arrSelectedItems = new Array();
        	$(elem).find('i').toggleClass('fa-square-o fa-check-square-o');
			$(".li-filter-item").find('.fa-check-square-o').each(function() {
				arrSelectedItems.push($(this).next().text().trim());
			});

			if(this.$(".li-filter-item").find('.fa-check-square-o').length>0 && this.$(".li-filter-item").find('.fa-square-o').length>0) 
				this.$(".li-filter-item-select-all i").removeClass('fa-check-square-o').removeClass('fa-square-o').addClass('fa-square');
			else if(this.$(".li-filter-item").find('.fa-check-square-o').length == this.$(".li-filter-item").length)
				this.$(".li-filter-item-select-all i").addClass('fa-check-square-o').removeClass('fa-square-o').removeClass('fa-square');
			else if(this.$(".li-filter-item").find('.fa-square-o').length == this.$(".li-filter-item").length)
				this.$(".li-filter-item-select-all i").removeClass('fa-check-square-o').addClass('fa-square-o').removeClass('fa-square');
            console.log(arrSelectedItems);
            GM.Global.FilterList[this.filterKey] = arrSelectedItems;
			this.filterItemList(arrSelectedItems);
        	
        },

        selectAllFilter: function(event) {
        	event.stopImmediatePropagation();
        	var elem = $(event.currentTarget), arrSelectedItems = new Array();
        	var className = $(elem).find('i').attr("class").replace("fa ",'');

        	console.log(className);

        	if(className.indexOf("fa-square-o")>=0) { 	// If Nothing selected
        		$(elem).find('i').toggleClass('fa-square-o fa-check-square-o');
        		this.$(".li-filter-item").find('.fa-square-o').toggleClass('fa-square-o fa-check-square-o');
        	}
        	else {										// If atleast one selected
        		$(elem).find('i').attr('class','fa fa-square-o');
        		this.$(".li-filter-item").find('.fa-check-square-o').removeClass('fa-check-square-o').removeClass('fa-square').addClass('fa-square-o');
        	}

        	this.$(".li-filter-item").find('.fa-check-square-o').each(function() {
				arrSelectedItems.push($(this).next().text().trim());
			});
            GM.Global.FilterList = [];
            console.log(arrSelectedItems);
			this.filterItemList(arrSelectedItems);

        },


        searchFilter: function(event) {
        	var letters = event.currentTarget.value;
            console.log(letters);
            letters = letters.toLowerCase();
        	this.$(".li-filter-item").addClass('hide').each(function() {
                console.log($(this).text().toLowerCase().indexOf(letters)>=0);
        		if($(this).text().toLowerCase().indexOf(letters)>=0)
        			$(this).removeClass('hide');
        		else
        			$(this).addClass('hide');
        	});

        	if(letters.length>0)
        		this.$(".li-filter-item-select-all").addClass('hide');
        	else
        		this.$(".li-filter-item-select-all").removeClass('hide');
        },

        filterItemList: function(arrItems) {

        	var that = this;

        	this.filter[this.filterKey] = $.parseJSON(JSON.stringify(arrItems));
            console.log(this.filter[this.filterKey]);
        	var index = _.indexOf(this.filterKeys,this.filterKey);
            console.log(index);
        	if(index!=-1)
        		this.filterKeys = this.filterKeys.slice(0,index);
            console.log(this.filterKey);
        	if($(".li-filter-item").length!=arrItems.length && arrItems.length!=0)
	    		this.filterKeys.push(this.filterKey);
            console.log(this.filterKeys);
    		for(var i=0; i<=this.filterKeys.length;i++) {
    			var filterKey = this.filterKeys[i];
                console.log(filterKey);
    			if(i==0) {
    				this.currentcollection = new GMCItem(this.collection.filterBy(filterKey, this.filter[filterKey]));
    				this.$(".btn-column-head").each(function() {
    					if(this.id==filterKey) 
    						that.filterItems[this.id] = _.uniq(_.pluck(that.collection.toJSON(),this.id));
    					else
							that.filterItems[this.id] = _.uniq(_.pluck(that.currentcollection.toJSON(),this.id));
					});
                    console.log(this.currentcollection);
    			}
        		else
        			this.currentcollection = new GMCItem(this.currentcollection.filterBy(filterKey, this.filter[filterKey]));
    		}

            console.log(this.currentcollection);
    		this.$(".btn-column-head").each(function() {
				if(_.contains(that.filterKeys,this.id)) 
					$(this).find('i').css('color','red');
				else
						$(this).find('i').css('color',that.color);
			});

        	this.currentcollection.bind('sort', this.render, this);

            this.$("#div-crmitemlist-coldata").empty();
            this.currentcollection.each(function(item, index) {
                item.set("index", index+1);
                var itemview = new GMVItem({model: item, el : that.$("#div-crmitemlist-coldata"), template_URL : that.template_URL, template: that.mainTemplate});
            }, this);
			
            if(this.callbackOnReRender!=undefined)
                this.callbackOnReRender(this);
            console.log(this);
            
            resizeFont();
        },

        getChartDataCol: function (arrSelectedLabel) {
            console.log(arrSelectedLabel);
            var that = this;
            if (arrSelectedLabel != "")
                this.currentcollection = new GMCItem(this.collection.filterBy("acttype", arrSelectedLabel));
            else
                this.currentcollection = new GMCItem(this.collection);
            
            this.currentcollection.bind('sort', this.render, this);
            console.log($("#div-crmitemlist-coldata").length)
            this.$("#div-crmitemlist-coldata").empty();
            this.currentcollection.each(function(item) {
                console.log(item)
                var itemview = new ItemView({model: item, el : that.$("#div-crmitemlist-coldata"), template_URL : that.template_URL, template: that.mainTemplate});
            }, this);
           $("#div-main-content #chart-content").addClass("hide");
           $("#div-main-content #div-sale-keyword-detail").removeClass("hide");
           $("#div-main-content #div-crmitemlist-colhead").removeClass("hide");
           $("#div-main-content #div-crmitemlist-coldata").removeClass("hide");
           $("#div-main-content .horizondal-line").removeClass("hide");
            
            if(this.callbackOnReRender!=undefined)
                this.callbackOnReRender(this);
            
            
            console.log(this)
        },   
        
        downloadCSV: function(event) {
            var colData = _.pluck(this.sortoptions,'ID');
            var CSVHeaders = (_.pluck(this.sortoptions,'DispName')).join(",");
            
            if(this.csvFileName) 
                event.currentTarget.download = this.csvFileName;
            
            if (typeof (event.currentTarget.download) != "undefined") {
                event.currentTarget.href = "data:text/csv," + encodeURI(CSVHeaders + "\n" + this.currentcollection.getValuesInCSV(colData)); 
            }
            else {
                showNativeAlert('Your Browser is not supported. Please try again in any Chrome / Firefox browsers!', 'No Browser Support');
                var input = {}
                if(this.csvFileName) 
                    input.filename = this.csvFileName;
                else
                    input.filename = "CRM_CSV_Data.csv";
                
                input.filecontent = CSVHeaders + "\n" + this.currentcollection.getValuesInCSV(colData); 
                
                fnGetWebServerData("crm/generatecsv",undefined,input, function(data) {
                    console.log(data);
                });
            }
        },

        openChart: function () {
            var that = this;
            var arrCol = new Array();
            var chartData = new Array();
            var crmitem = new GMMItem();
            var datacol = "";
            $("#div-main-content #div-sale-keyword-detail").addClass("hide");
            $("#div-main-content #div-sale-keyword-detail").addClass("hide");
           $("#div-main-content #div-crmitemlist-colhead").addClass("hide");
           $("#div-main-content #div-crmitemlist-coldata").addClass("hide");
           $("#div-main-content .horizondal-line").addClass("hide");
            console.log($("#chart-content").length)
            if($("#chart-content").length<1)
                $("#div-main-content").append("<div id = 'chart-content'></div>");
            else{
                $("#div-main-content #chart-content").removeClass("hide");
                this.currentcollection = this.currentcollectioncopy;
            }
                
            this.currentcollection.each(function(item) {
                datacol = $.parseJSON(JSON.stringify(item));
                console.log(datacol);
                arrCol.push(datacol["acttype"]);
            });
            console.log(datacol);
            var arrDistinct = new Array();
            $(arrCol).each(function(index, item) {
                if ($.inArray(item, arrDistinct) == -1)
                    arrDistinct.push(item);
            });
            var url = window.location.href;
            url = url.split("#");
            var currenrURL= url[1];
            
            $(arrDistinct).each(function(index, item) {
                var count = $.grep(arrCol, function(elem) {
                  return elem == item;
                }).length;
                var columnData = {"Label":item, "Value":count, "link":"#"+currenrURL};
                console.log(columnData);
                        chartData.push(columnData);
                console.log(chartData);
            });
            var title = document.getElementsByClassName('gmTab gmFontWhite');
            var captionName = title[0].getAttribute( 'id' );
            var charOptions = {
                "caption": captionName + " - Chart",
                "paletteColors": "#0075c2,#1aaf5d,#f8bd19,#e44a00,#33bdda,#6baa01,#583e78,#e49d1a",
                "bgColor": "#ffffff",
                "showborder": "0",
                "slicingdistance": "3",
                "showToolTip": "0",
                "showlegend": "1",
                "legendborder": "0",
                "legendposition": "bottom",
                "enablesmartlabels": "1",
                "use3dlighting": "0",
                "showshadow": "0",
                "legendbgcolor": "#444",
                "legendbgalpha": "20",
                "legendborderalpha": "0",
                "legendshadow": "0",
                "legendnumcolumns": "3",
                "theme": "fint"
            }
            
            this.chart = new FusionCharts({
                type: 'pie2d',
                renderAt: 'chart-content',
                width: '450',
                height: '300',
                dataFormat: 'json',
                dataSource: {"chart": charOptions,"data": chartData},
                "events": {
                   "dataPlotClick": function (eventObj, dataObj) {
                       
                       
                       console.log(eventObj);
                       console.log(dataObj.categoryLabel);
                       var json = eventObj.sender.getJSONData();
                       console.log(json);
                       var arrSelectedLabel = new Array();
                       arrSelectedLabel.push(dataObj.categoryLabel);
                       console.log(arrSelectedLabel);
                       that.getChartDataCol(arrSelectedLabel);
                   },
                }
            }).render();
        },
        
        close: function() {
        	this.unbind();
        	this.undelegateEvents();
        	this.$el.empty();
        },
        
        
	});

	return GMVItemList;
});


