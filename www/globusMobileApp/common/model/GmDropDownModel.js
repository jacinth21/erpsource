/**********************************************************************************
 * File:        GmDropDownModel.js
 * Description: Item Model
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/
define([
    'underscore',
    'backbone',
],

function (_, Backbone) {

	'use strict';

	var DropDownModel = Backbone.Model.extend({

		// Default values of the model
		defaults: {
			ID : "defaultID",
			Class: "itm-filter",
			Name: "Chose One",
			SelectedIndex: false
		}
	});	

	return DropDownModel;
});
