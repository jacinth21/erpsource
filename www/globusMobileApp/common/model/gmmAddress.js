define([
    'underscore',
    'backbone',
],

function (_, Backbone) {

	'use strict';

	GM.Model.Address = Backbone.Model.extend({

		// Default values of the model
		defaults: {
			"addid": "",
			"add1": "",
			"add2": "",
			"city": "",
			"statenm": "",
			"stateid": "",
            "countrynm": "",
			"countryid": "",
			"zip": "",
			"primaryfl": "",
			"comments": "",
			"commentid": "",
			"phoneid" : "",
			"phone" : "",
			"emailid" : "",
			"email" : "",
            "addrtyp": ""
		},

		initialize: function(data) {
			
		},

		validate: function(attrs) {
            var errors = this.errors = {};
            if(!attrs.state) errors.state = "*State is required";
            if(!attrs.zip) errors.zip = "*ZipCode is required";
            if(!attrs.add) errors.add = "*Address1 is required";
            if(!attrs.city) errors.city = "*City is required";
            if (!_.isEmpty(errors)) return errors;
          }

	});	

	return GM.Model.Address;

});
