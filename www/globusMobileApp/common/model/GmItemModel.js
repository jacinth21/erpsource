/**********************************************************************************
 * File:        GmItemModel.js
 * Description: Item Model
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/
define([
    'underscore',
    'backbone',
],

function (_, Backbone) {

	'use strict';

	var Item = Backbone.Model.extend({

		// Default values of the model
		defaults: {
			ID: "ItemID",
			Name: "ItemName",
			URL: "ItemURL",
			Description: "ItemDesc",
			Detail: "ItemDetail",
			File1: "ItemFile1",
			File2: "ItemFile2",
			File3: "ItemFile3",
			File4: "ItemFile4",
			Type: "Type",
			Quantity: "0",
			FirstName: "fname",
			LastName: "lname",
			Email: "emailid"

		}
	});	

	return Item;
});
