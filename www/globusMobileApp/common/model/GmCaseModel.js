/**********************************************************************************
 * File:        GmCaseModel.js
 * Description: Case Model
 * Version:     1.0
 * Author:      ejayabalan
 **********************************************************************************/
define([
    'underscore',
    'backbone',
],

function (_, Backbone) {

	'use strict';

	var Case = Backbone.Model.extend({

		// Default values of the model
		defaults: {
			cainfoid: "CaInfoID",
			caseid: "CaseID", // Rep who is linked to the Account
			surdt: "SurgeryDate", // Covering Rep
			surtime: "SurgeryTime",
			did: "Distributor",
			repid: "RepID",
			acid: "AccountID",
			voidfl: "VoidFlag",
			prnotes: "PersonalNotes",
			type: "CodeLookup",
			status: "Status",
			prtcaid: "ParentCaseID",
			skipvalidationfl: "Y",
			arrGmCaseAttributeVO: ["1", "2", "3"]
		}
	});	

	return Case;
});
