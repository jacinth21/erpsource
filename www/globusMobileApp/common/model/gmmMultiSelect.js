/**********************************************************************************
 * File:        GmMultiSelectModel.js
 * Description: Multiple Selection Model
 * Version:     1.0
 * Author:      cskumar
 **********************************************************************************/
define([
    'underscore',
    'backbone',
],

function (_, Backbone) {

	'use strict';

	GM.Model.MultiSelectModel = Backbone.Model.extend({

		// Default values of the model
		defaults: {

			ID: "",
			Name: "",
			target: "",
			template : 'GmSelectResult',
			template_URL: "globusMobileApp/common/template/",
            selectedColor: "",
            selectedData: "",
            check: ""
		}
	});	

	return GM.Model.MultiSelectModel;
});
