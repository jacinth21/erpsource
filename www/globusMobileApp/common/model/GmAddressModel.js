/**********************************************************************************
 * File:        GmAddressModel.js
 * Description: Address Item Model
 * Version:     1.0
 * Author:      ejayabalan
 **********************************************************************************/
define([
    'underscore',
    'backbone',
],

function (_, Backbone) {

	'use strict';

	var Address = Backbone.Model.extend({

		// Default values of the model
		defaults: {
			AddressID: "AddressID",
			AddressType: "AddressType",
			City: "City",
			State: "State",
			Zip: "Zip"
		}
	});	

	return Address;
});
