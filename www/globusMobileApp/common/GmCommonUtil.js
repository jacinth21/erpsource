/**********************************************************************************
 * File:        GmCommonUtil.js
 * Description: Common function usage for App 
 * Version:     1.0
 * Author:      jgurunthan
 **********************************************************************************/


// Function will open the default mail box in the device with given input values
function sendMail(mailTo, mailCC, mailsub, mailAttach, mailContent) {
    var addressTo = [],
        addressCC = [];
    addressTo = (mailTo.indexOf(',') != -1) ? mailTo.split(',') : mailTo;
    addressCC = (mailCC.indexOf(',') != -1) ? mailCC.split(',') : mailCC;

    if ($(window).width() < 480) 
	    cordova.plugins.email.open({
            to: addressTo,
            cc: addressCC,
            subject: [mailsub],
            attachments: [mailAttach],
            body: [mailContent],
            isHtml: true
      });
     else
	    window.plugin.email.open({
            to: addressTo,
            cc: addressCC,
            subject: [mailsub],
            attachments: [mailAttach],
            body: [mailContent],
            isHtml: true
    });
}

// Get the current page screenshot and return the file file path
function getScreenShot(fileType, quality, fileName, callback) {

    fileType = fileType || 'jpg';
    quality = quality || '100';
    fileName = fileName || "screenshot";

    navigator.screenshot.save(function (error, res) {
        console.log("error >>> " + error);
        console.log("res >>> " + JSON.stringify(res));
        if (error) {
            console.error(error);
        } else {
            console.log('ok', res.filePath);
            callback(res.filePath);
        }
    }, fileType, quality, fileName);
}

// Prototype for push values into the array with limitation. Default is 25
Array.prototype.push_arr_ltd = function (element, limit) {
    var limit = limit || 25;
    var length = this.length;
    if (length == limit) {
        this.shift();
    }
    this.push(element);
}


// Function for return the 0 (zero) value if the value passed as null/undefined/empty and check it is numeric or not
function parseZero(value) {
    return (value == undefined || value == null || value == "" || !$.isNumeric(value)) ? 0 : value;
}

// Function to get month Name by passing number
function getMonthName(mid) {
    const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
    return monthNames[mid - 1];
}

// To display custom msg on popup itself
function displayPopupMsg(Message) {
    $('#div-crm-popup-msg').removeClass('hide').addClass('gmBGRed').html(Message.replace(/\n/g, '<br>')).slideDown(500).fadeOut(10000);
}
// Due to function name duplication the following func is commeted PMT-29490 
// Function to formtting currency with symbol($)
// function formatCurrency(total) {
function fmtPrice(total) {
    var neg = false;
    if (total < 0) {
        neg = true;
        total = Math.abs(total);
    }
    return (neg ? "-$" : '$') + parseFloat(total, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
}

//  Common Function to get CodeLookUp values i/p: code id, code name, code group, code name alt and code access id.

function getCodeLookUpVal(codeid, codenm, codegrp, codenmalt, codeaccid, callback) {
    var input = {
        "token": localStorage.getItem("token")
    };
    if (codeid != "")
        input["codeid"] = codeid;
    if (codenm != "")
        input["codenm"] = codenm;
    if (codegrp != "")
        input["codegrp"] = codegrp;
    if (codenmalt != "")
        input["codenmalt"] = codenmalt;
    if (codeaccid != "")
        input["codeaccid"] = codeaccid;
    
    console.log("getCodeLookUpVal input>>>>>>" + JSON.stringify(input));
    fnGetWebServerData("search/codelookup", "gmCodeLookUpVO", input, function (data) {
        console.log("getCodeLookUpVal output>>>>>>" + JSON.stringify(data));
        callback(data);

    });
}

//This function is used to sort hyplink columns
function sortLink(a,b,order) { 
	a = a.substring(a.indexOf(">")+1,a.lastIndexOf("<"));
	b = b.substring(b.indexOf(">")+1,b.lastIndexOf("<"));
	  return (a.toLowerCase()>b.toLowerCase()?1:-1)*(order=="asc"?1:-1);
};

//This function is used to sort hyper link column Id as number Ex. <a href="">HC-100</a> 
function sortLinkID(a,b,order){ 
	var x = parseInt(a.substring(a.indexOf("-")+1,a.lastIndexOf("<")));
	var y = parseInt(b.substring(b.indexOf("-")+1,b.lastIndexOf("<")));
	x = isNaN(x)?0:x;
	y = isNaN(y)?0:y;
	  return (x>y?1:-1)*(order=="asc"?1:-1);
}



//formtting currency
Handlebars.registerHelper("formatCurrency", function (total) {
    return formatCurrency(total);
});


//Company Date Format
Handlebars.registerHelper("compDateFmt", function (dt) {
    return compDateFmt(dt);
});


/* fomattedDate() -Common method to get the date format based on the company 
 */

function compDateFmt(dt) {
    //    var userData = JSON.parse(localStorage.getItem("userData"));
    //    var dateformat = userData.cmpdfmt;
    //    try {
    //        if (dt != "" && dt != null && dt != undefined &&
    //            dateformat != "" && dateformat != null && dateformat != undefined) {
    //            if (dateformat != "yyddMM" && dateformat != "yyMMdd") {
    //                var resultedDate = $.format.date(new Date(dt), dateformat);
    //                if (resultedDate != "" || resultedDate != null || resultedDate != undefined || resultedDate != "NaN/NaN/NaN") {
    //                    dt = resultedDate;
    //                }
    //            }
    //        }
    //    } catch (err) {
    //        console.log(err);
    //    }
    return getDateByFormat(dt);
}

// Function to formtting currency with symbol($) and change as red color text
function compPriceFmt(price) {
    var userData = JSON.parse(localStorage.getItem("userData"));
    var priceFormat = userData.cmpcurrsmb;
    var neg = false;
    if (price < 0) {
        neg = true;
        price = Math.abs(price);
    }
    if (neg) {
        return new Handlebars.SafeString('<span class="gmFontRed">' + priceFormat + '(' + parseFloat(price, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString() + ')');
    } else {
        return ("$") + parseFloat(price, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
    }
}

//within brackets number function
function bracketsNumber(num) {
    var neg = false;
    if (num < 0) {
        neg = true;
        num = Math.abs(num);
    }
    if (neg) {
        return new Handlebars.SafeString('<span class="gmFontRed">(' + num + ')');
    } else {
        return num;
    }
}

function formatPhoneNumber(phoneNumber) {
    phoneNumber = phoneNumber.toString();
    if (phoneNumber.indexOf("-") >= 0) {
        return phoneNumber;
    } else {
        phoneNumber = phoneNumber.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, "$1-$2-$3");
        return phoneNumber;
    }
}

//within brackets number handlebar function
Handlebars.registerHelper("bracketsNumber", function (num) {
    return bracketsNumber(num);
});

//formtting currency with symbol($) and change as red color text
Handlebars.registerHelper("compPriceFmt", function (price) {
    return compPriceFmt(price);
});
// Common Function To Display DTMLX Grid, Following Parameters as Input
// divRef - Id of the element, where the grid has to be displayed
// gridHeight - Height of the Grid, if it is empty then as 500
// gridData - JSON Object, Grid Data
// setHeader - Array, Grid Header Data
// setInitWidths - String, Width of Each Column in the Grid
// setColAlign - String, Alignment data of Each Column in the Grid (right,left,center,justify)
// setColTypes - String, Type of Data in Each Column in the Grid(ro-read only, ed - editable, txt- text, txttxt- plain text etc.. )
// setColSorting - String, Sort based on type of data in Each Column in the Grid(str, int, date, na or function object for custom sorting)
// enableTooltips - String, True/False to enable or disable tooltips
// setFilter - String, filters like #text_filter, #numeric_filter,..
// footerArry - Array, Grid Footer Data
// footerStyles - Array, Footer Data Styles
// footerExportFL - Boolean, true/false to attach footer icon to export as exdel and pdf
// Returns Grid Object gObj

function loadDHTMLXGrid(divRef, gridHeight, gridData, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL,rptTemplate, mobResArg) {
    if($(window).width() > 767){
      
    var gObj = new dhtmlXGridObject(divRef);
    gObj.enableColSpan(true);
    gObj.setImagePath("globusMobileApp/lib/dhtmlx/dhtmlxGrid/imgs/");
    if (setHeader != undefined && setHeader.length > 0) {
        gObj.setHeader(setHeader);
        var colCount = setHeader.length;
    }
    if (setInitWidths != "")
        gObj.setInitWidths(setInitWidths);
    if (setColAlign != "")
        gObj.setColAlign(setColAlign);
    if (setColTypes != "")
        gObj.setColTypes(setColTypes);
    if (setColSorting != "")
        gObj.setColSorting(setColSorting);
    if (enableTooltips != "")
        gObj.enableTooltips(enableTooltips);
    if (setFilter != "")
        gObj.attachHeader(setFilter);
    if (footerArry != undefined && footerArry.length > 0) {
        var footstr = eval(footerArry).join(",");
        if (footerStyles != undefined && footerStyles.length > 0)
            gObj.attachFooter(footstr, footerStyles);
        else
            gObj.attachFooter(footstr);
    }
    if (footerExportFL)
        fnAttachExptFooter(divRef, colCount, gObj);
    if (gridHeight != "")
        gObj.enableAutoHeight(true, gridHeight, true);
    else
        gObj.enableAutoHeight(true, 500, true);

 

    gObj.init();
    gObj.parse(gridData, "json");
    return gObj;
        
    
}else{
    if(mobResArg==true){
        var gObj = new dhtmlXGridObject(divRef);
    gObj.enableColSpan(true);
    gObj.setImagePath("globusMobileApp/lib/dhtmlx/dhtmlxGrid/imgs/");
    if (setHeader != undefined && setHeader.length > 0) {
        gObj.setHeader(setHeader);
        var colCount = setHeader.length;
    }
    if (setInitWidths != "")
        gObj.setInitWidths(setInitWidths);
    if (setColAlign != "")
        gObj.setColAlign(setColAlign);
    if (setColTypes != "")
        gObj.setColTypes(setColTypes);
    if (setColSorting != "")
        gObj.setColSorting(setColSorting);
    if (enableTooltips != "")
        gObj.enableTooltips(enableTooltips);
    if (setFilter != "")
        gObj.attachHeader(setFilter);
    if (footerArry != undefined && footerArry.length > 0) {
        var footstr = eval(footerArry).join(",");
        if (footerStyles != undefined && footerStyles.length > 0)
            gObj.attachFooter(footstr, footerStyles);
        else
            gObj.attachFooter(footstr);
    }
    if (footerExportFL)
        fnAttachExptFooter(divRef, colCount, gObj);
    if (gridHeight != "")
        gObj.enableAutoHeight(true, gridHeight, true);
    else
        gObj.enableAutoHeight(true, 500, true);

 

    gObj.init();
    gObj.parse(gridData, "json");
    return gObj;
        
    }
    else{
        $("#" + divRef).html(rptTemplate);
    }
    }
    
    
    
    $(".div-dhx-report-arrow").on("click",function(){
        $(this).parent(".div-dhx-report-mbl-header").siblings(".div-dhx-report-mbl-body").toggleClass("onActive");
        $(this).parent(".div-dhx-report-mbl-header").toggleClass("onActive");
        $(this).toggleClass("onActive");
    });
}

//To attach Export Grid to excel option to the DHTMLX grid
//ColCount - add the icon in last two column
//gridData - to attach footer with this arg
function fnAttachExptFooter(divRef, colCount, gridData) {
    var footerExport = [];
    for (var j = 0; j < colCount; j++) {
        footerExport[j] = "";
        if (j == colCount - 2)
            footerExport[j] = "<section><span id='" + divRef + "-export' class='export-to-excel gmAlignHorizontal gmOpacity5'><strong style='vertical-align:top;'>Export Grid</strong><i class='gmFont15em gmMarginLeft10p fa fa-download' aria-hidden='true' title='Export as Excel'></i></span></section> ";
        if (j == colCount - 1)
            footerExport[j] = "#cspan";
    }
    gridData.attachFooter(footerExport);
}

//Export DHTMLX Grid as Excel
//gridData - denotes which grid i have to export
//deleteIndexArr - Array, which are the columns are not need to export
function exportExcel(e, gridData, deleteIndexArr) {
    showNativeAlert("This feature is available only on browser", "Cannot Downloaded");
    if (deleteIndexArr != undefined && deleteIndexArr.length > 0)
        _.each(deleteIndexArr, function (ind) {
            gridData.setColumnHidden(ind, true);
        });
    //    gridData.toExcel('/phpapp/excel/generate.php'); //Not Working In APP
    if (deleteIndexArr != undefined && deleteIndexArr.length > 0)
        _.each(deleteIndexArr, function (ind) {
            gridData.setColumnHidden(ind, false);
        });
}


// Get WS dropdown Code LookUp values to respective div
// render el, code id, code name, code group, code alt name, Code Access ID, repalce by value, model setl name, boolean validation required (true or false), Initail select 
// arg passed 10 input and 1 output callback
function getCLValToDiv(divID, codeID, codeNm, codeGrp, codeNmAlt, codeAccID, replaceBy, attrVal, validate, initSelect, callback) {
    getCodeLookUpVal(codeID, codeNm, codeGrp, codeNmAlt, codeAccID, function (data) {
        console.log(data);
        var arrSelectOpt = new Array();
        if (initSelect)
            arrSelectOpt.push({
                "ID": "",
                "Name": "Select"
            });
        data = arrSelectOpt.concat(_.sortBy(data, function (data) {
            return data.codenm;
        }));
        if (replaceBy == "codenmalt")
            var strSelectOptions = JSON.stringify(data).replace(/codenmalt/g, 'ID').replace(/codenm/g, 'Name');
        else if (replaceBy == "codenm")
            var strSelectOptions = JSON.stringify(data).replace(/codenm/g, 'ID').replace(/codenm/g, 'Name');
        else
            var strSelectOptions = JSON.stringify(data).replace(/codeid/g, 'ID').replace(/codenm/g, 'Name');

        var optionItems = $.parseJSON(strSelectOptions);
        $(divID).empty();
        $(divID).each(function () {
            fnCreateOptions(this, optionItems);
            $(this).find("option").attr("name", attrVal);
        });

        if (validate)
            $(divID).attr("validate", "required");

        callback(divID, attrVal);
    });
}

//Function for used to Validate Url
function UrlValid(url) {
    console.log(url);
    return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
}

//Function For Number Field Check 
function isNumberKey(e){
    console.log(e)
     var curInputCnt = $(e.currentTarget).attr("data-length");
     var curInputVal = $(e.currentTarget).val();
    var charCode = (e.which) ? e.which : event.keyCode;
//    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
            return false; 
    }
    if (curInputVal.length >= parseInt(curInputCnt)) {
            return false;
    }
    return true;
}    


function isPriceKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        var curInputCnt = $(evt.currentTarget).attr("data-length");
        var curInputVal = $(evt.currentTarget).val();
        if(charCode==46){
            if(!(curInputVal.indexOf(".") > -1)){   
                return true;
            }
        }
        if (charCode > 31 && (charCode < 48 || charCode > 57) )
            return false;
        if (curInputVal.length >= parseInt(curInputCnt)) {
            return false;
        }
        return true;   
}

function limitDecimal(el){
    var v = parseFloat(el.value);
    el.value = (isNaN(v)) ? '' : v.toFixed(2);
}
/*-----------------------Summary report---------------------------*/
function dhxReportDataFmt(rows,setHeader,cb) {
    var resdt = [];
    var j=0;
    _.each(rows, function(item){
        resdt[j]={};
        resdt[j].data=[];
        for(i=0;i<item.data.length; i++){
            resdt[j].data[i]={};
            resdt[j].data[i][0]=setHeader[i];
            resdt[j].data[i][1]=item.data[i];
        }
        j++;
    });
    cb(resdt);
}

function loadDatepicker(event,elemID,dateVal,dateFormat,minDate,maxDate){
    $(elemID).datepicker({
        dateFormat: dateFormat,
        minDate : minDate,
        maxDate : maxDate
    });
    var visible = $('#ui-datepicker-div').is(':visible');
    $(elemID).datepicker(visible? 'hide' : 'show');
    
    console.log(">>>> 0" + event);
    if(event != undefined){
        event.stopPropagation();
        $(window).click(function (event) {
             if(document.getElementById("ui-datepicker-div").style.display != "none") {
                var elem = event.target;
               if(!($(elem).hasClass("ui-datepicker-prev") || $(elem).hasClass("ui-datepicker-next") || $(elem).parent().hasClass("ui-datepicker-prev") || $(elem).parent().hasClass("ui-datepicker-next"))) {
                   var visible = $('#ui-datepicker-div').is(':visible');
                   $(elemID).datepicker(visible? 'hide' : 'show');	
               }
           }
         });
    }
}

function loadTimepicker(elemID,timeVal){
    $(elemID).mdtimepicker({
        is24hour: true
    });
    if (timeVal != "") {
        $(elemID).mdtimepicker('setValue', timeVal);
    }
}

//to show Company list as dropdown
function companyDropdown(input,defaultid,mode,DropDownView,elemAdmin,cb){
    var that = this;
    var drpDwnArr = [];
    this.cmpDropDownAdmin = _.findWhere(GM.Global.UserAccess, {
        funcnm: "CRM-SURGEON-ADMIN"
    });
    if (this.cmpDropDownAdmin != undefined){
        var adminAccess = "Y";
        var defaultObj = {};
        defaultObj.ID = "0";
        defaultObj.Name = "Choose One";
        drpDwnArr.push(defaultObj);
        if(mode == "create" || mode == "")
            defaultid = "0";
    }
    else{
        var adminAccess = "N";
        $(elemAdmin).addClass("pointer-event-none");
    }
    console.log(drpDwnArr);
    
    fnGetWebServerData("company/complist", "gmCompanyVO", input, function (data) {
        console.log(JSON.stringify(data));
        _.each(data, function (item) {
            var drpDownObj = {};
            drpDownObj.ID = item.companyid;
            drpDownObj.Name = item.companynm;
            drpDwnArr.push(drpDownObj);
        });
        that.cmpDropDownView = new DropDownView({
            el: $(elemAdmin),
            data: drpDwnArr,
            DefaultID: defaultid
        });
        if(adminAccess != "Y")
            $(elemAdmin+ " .div-arrow-dropdown").addClass("hide");

        cb(defaultid);
        $(elemAdmin).bind('onchange', function (e, selectedId, SelectedName) {
            cb(selectedId);
        });
    });
}

/* Get Server Data through Rest API call for Clould  */
function fnGetWebServerviceCloudData(serviceurl, input, callback) {

    console.log(serviceurl);
    console.log(input);
    var saveData = $.ajax({
        type: 'POST',
        url: serviceurl,
        headers: { 'x-functions-key': Azure_Cloud_MC_FUN_KEY },
        contentType: "application/json",
        data: input,
        success: function(resultData, response) {
            console.log("success >>>>>>>");
            console.log(resultData);
            console.log(response);
            callback(resultData);
        }
    });
    saveData.error(function(model, response, errorReport) { 
        console.log("errorReport >>>>>>");
        console.log(model);
        console.log(response);
        console.log(errorReport);
        callback(model, response, errorReport);
    });
    
}

// Validate email
function validateEmailID(val) {
    var emailPattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    val = val.trim();
        if (val != "" && emailPattern.test(val)!=true){
                return false;
            }
        else {
            return true;
        }
           
}

//PC-4864 Add Online PDF option on Pending PO Screen in Globus App
function viewOnlinePDF(orderid) {
    if (orderid != null) {
        var companyLocale = localStorage.getItem("compcd");
        var uploadDir     = localStorage.getItem("pdffilepath");
       
        /* Need to open the online PDF from FileInputStream
        * Get the FileID [order id ] , Company locale, uploadDir [Constant properties key] and pass to the below servlet
        * it will open the pdf from server */
       var toEncode = "/GmCommonFileOpenServlet?strOpt=IPADORDERS&compLocale=" + companyLocale + "&uploadString=" + uploadDir + "&fileName=" + orderid + ".pdf"
       
       var URL = URL_Portal + "GmLogonServlet?token=" + localStorage.getItem("token") + "&return_to=" + encodeURIComponent(toEncode) + "&randomid=" + Math.random();
       
        window.open(URL, '_blank', 'location=no'); 
    }
}
