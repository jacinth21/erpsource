/**********************************************************************************
 * File:        GmItemCol.js
 * Description: Collection of Item Models
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/
define([
    'underscore',
    'backbone',
    'itemmodel'
], 

function (_, Backbone, ItemModel) {

	'use strict';

	var ItemCollection = Backbone.Collection.extend({

		model: ItemModel,

		initialize: function(data) {
           this.add(data);
		},

		search: function(type,letters){
			var words = letters.split(/\s+/);
            var models = _.clone(this.models);

            _.each(words, function(word) {
            	var pattern = new RegExp(word, "i");
                models = _.filter(models, function(model) {
                    return pattern.test(model.get(type));
                });
            });
            return models;
		},

        sortAsc: function(sortField) {
            var key = sortField;
            this.comparator = function(model) {
                return model.get(key);
            };  
            this.sort();
        },

        sortDesc: function(sortField) {
            var key = sortField;
            this.comparator = function(a, b) {
                a = a.get(key);
                b = b.get(key);
                return a < b ?  1 : a > b ? -1 : 0;
            }
            this.sort();
        }
	});

	return ItemCollection;
});