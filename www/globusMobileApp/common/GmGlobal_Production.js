/**********************************************************************************
 * File:        GmGlobal.js
 * Description: Contains global variables and functions commonly used across the
 *              Application
 * Version:     1.0
 * Author:      ejayabalan
 **********************************************************************************/

var version = "1.0.0";
var deviceModel = "";
var locale;
var breadcrumb = new Array(), recentBreadCrumbs = new Array(), recentHash = "";
var UpdateTimer, TimerMilliSeconds = 10000;   //10 Seconds

var URL_Catalog_Template = "globusMobileApp/sales/catalog/template/";
var URL_Common_Template = "globusMobileApp/common/template/";
var URL_CSS = "globusMobileApp/css/";
var URL_Home_Template = "globusMobileApp/home/template/";
var URL_Login_Template = "globusMobileApp/user/template/";
var URL_Main_Template = "globusMobileApp/common/template/";
var URL_Setup_Template = "globusMobileApp/setup/template/";
var URL_Msg_Template = "globusMobileApp/message/template/";
var URL_LoginFailed_Template = "globusMobileApp/user/template/";
var URL_Logout_Template = "globusMobileApp/user/template/";
var URL_Settings_Template = "globusMobileApp/settings/template/";

function loadCSS(url) {
	var link = document.createElement("link");
	link.type = "text/css";
	link.rel = "stylesheet";
	link.href = url;
	document.getElementsByTagName("head")[0].appendChild(link);
}

function fnGetTemplate(strPath, strName) {
	if (Handlebars.templates === undefined || Handlebars.templates[strName] === undefined) {
    $.ajax({
       url : strPath + strName + ".hbs",
       success : function(data) {
           if (Handlebars.templates === undefined) {
               Handlebars.templates = {};
           }
           Handlebars.templates[strName] = Handlebars.compile(data);
       },
       async : false
     });
	}
	return Handlebars.templates[strName];
}

// PRODUCTION SERVER PATHS
var URL_WSServer = "http://ws.spineit.net/resources/";  //Production path for Web Service
var URL_FileServer = "http://mobile.spineit.net/globusapp/";  //Production path for Images

//VO - Table / URL / Column Mapping
var listGmPartNumberVO = {"tableName" : "T205_PART_NUMBER", "url" : "parts/master", "code" : "4000411", "syncSeq" : 1, "totalSeq" : 1, "partnum" : "C205_PART_NUMBER_ID", "partdesc" : "C205_PART_NUM_DESC", "partdtldesc" : "C205_PART_NUM_DETAIL", "partstat" : "C901_STATUS_ID", "partfmly" : "C205_PRODUCT_FAMILY", "partactfl" : "C205_ACTIVE_FL", "systemid" : "C207_SET_SALES_SYSTEM_ID"};

var listGmSetMasterVO = {"tableName" : "T207_SET_MASTER", "url" : "system/setmaster", "code" : "103111", "syncSeq" : 1, "totalSeq" : 2, "setid" : "C207_SET_ID", "setnm" : "C207_SET_NM", "setdesc" : "C207_SET_DESC", "setdtldesc" : "C207_SET_DETAIL", "systemid" : "C207_SET_SALES_SYSTEM_ID", "setcategoryid" : "C207_CATEGORY", "settypeid" : "C207_TYPE", "sethierarchyid" : "C901_HIERARCHY", "setstatusid" : "C901_STATUS_ID", "voidfl" : "C207_VOID_FL"};
var listGmSetPartVO = {"tableName" : "T208_SET_DETAILS", "url" : "system/setpart", "code" : "4000409", "syncSeq" : 2, "totalSeq" : 2, "setid" : "C207_SET_ID", "partnum" : "C205_PART_NUMBER_ID", "setqty" : "C208_SET_QTY", "insetfl" : "C208_INSET_FL", "voidfl" : "C208_VOID_FL"};

var listGmSystemVO = {"tableName" : "T207_SET_MASTER", "url" : "system/master", "code" : "103108", "syncSeq" : 1, "totalSeq" : 2, "systemid" : "C207_SET_ID", "systemnm" : "C207_SET_NM", "systemdesc" : "C207_SET_DESC", "systemdtldesc" : "C207_SET_DETAIL", "systemstatusid" : "C901_STATUS_ID", "voidfl" : "C207_VOID_FL"};
var listGmSystemAttributeVO = {"tableName" : "T207C_SET_ATTRIBUTE", "url" : "system/systemattrib", "code" : "4000416", "syncSeq" : 2, "totalSeq" : 2, "systemid" : "C207_SET_ID", "attribtypeid" : "C901_ATTRIBUTE_TYPE", "attribvalue" : "C207C_ATTRIBUTE_VALUE", "voidfl" : "C207C_VOID_FL"};

var listGmGroupVO = {"tableName" : "T4010_GROUP", "url" : "gopgroup/master", "code" : "103110", "syncSeq" : 1, "totalSeq" : 2, "groupid" : "C4010_GROUP_ID", "groupnm" : "C4010_GROUP_NM", "groupdesc" : "C4010_GROUP_DESC", "groupdtldesc" : "C4010_GROUP_DTL_DESC", "grouppublishfl" : "C4010_PUBLISH_FL", "grouptypeid" : "C901_GROUP_TYPE", "grouppricedtypeid" : "C901_PRICED_TYPE", "systemid" : "C207_SET_ID", "voidfl" : "C4010_VOID_FL"};
var listGmGroupPartVO = {"tableName" : "T4011_GROUP_DETAIL", "url" : "gopgroup/detail", "code" : "4000408", "syncSeq" : 2, "totalSeq" : 2, "groupid" : "C4010_GROUP_ID", "partnum" : "C205_PART_NUMBER_ID", "pricingtypeid" : "C901_PART_PRICING_TYPE"};

var listGmCodeLookupVO = {"tableName" : "T901_CODE_LOOKUP", "url" : "codelookup/master", "code" : "103107", "syncSeq" : 1, "totalSeq" : 2, "codeid" : "C901_CODE_ID", "codenm" : "C901_CODE_NM", "codegrp" : "C901_CODE_GRP", "activefl" : "C901_ACTIVE_FL", "seqno" : "C901_CODE_SEQ_NO", "codenmalt" : "C902_CODE_NM_ALT", "controltype" : "C901_CONTROL_TYPE", "createddate" : "C901_CREATED_DATE", "createdby" : "C901_CREATED_BY" , "lastupdateddate" : "C901_LAST_UPDATED_DATE", "lastupdatedby" : "C901_LAST_UPDATED_BY", "voidfl" : "C901_VOID_FL", "lockfl" : "C901_LOCK_FL"};
var listGmFileVO = {"tableName" : "T903_UPLOAD_FILE_LIST", "url" : "uploadedFile/master", "code" : "4000410", "syncSeq" : 2, "totalSeq" : 2, "fileid" : "C903_UPLOAD_FILE_LIST", "filenm" : "C903_FILE_NAME", "filerefid" : "C903_REF_ID", "type" : "C901_TYPE", "filesize" : "C903_FILE_SIZE", "fileseq" : "C903_FILE_SEQ_NO", "filetitle" : "C901_FILE_TITLE", "filerefgroup" : "C901_REF_GRP", "uploadedby" : "C903_LAST_UPDATED_BY", "uploadeddt" : "C903_LAST_UPDATED_DATE", "voidfl" : "C903_DELETE_FL" };

var listGmDataUpdateVO = {"tableName" : "T2001_MASTER_DATA_UPDATES", "url" : "devicesync/updates", "code" : "", "refid" : "C2001_REF_ID", "reftype" : "C901_REF_TYPE", "action" : "C901_ACTION", "appid" : "C901_APP_ID", "updateddt" : "C2001_REF_UPDATED_DT", "updatedby" : "C2001_LAST_UPDATED_BY", "voidfl" : "C2001_VOID_FL" }

var GmDeviceSyncClear = {"tableName" : "", "url" : "devicesync/clear" };
var resetTables = ["T205_PART_NUMBER", "T207_SET_MASTER", "T208_SET_DETAILS", "T901_CODE_LOOKUP", "T903_UPLOAD_FILE_LIST", "T2001_MASTER_DATA_UPDATES", "T4010_GROUP", "T4011_GROUP_DETAIL", "TBL_FAVORITE", "TBL_RECENT"];


//Image Type Mapping
var imgsystem = '103092';
var imgset = '103091';
var imggroup = '103094';
var imgpart = '103090';


var langid = "103097";

