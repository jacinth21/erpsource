/**********************************************************************************
 * File:        GmCatalogRouter.js
 * Description: Router specifically meant to route views within the 
 *              Catalog application
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',
        'catalogview', 'itemdetailview'
      ], 
    
    function ($, _, Backbone, CatalogView, ItemDetailView) {

	'use strict';

	var CatalogRouter = Backbone.Router.extend({

		routes: {
			"catalog": "catalog",
			"catalog/:name":"sidePanel",
			"catalog/Technique/:id": "sidePanelTechnique",
			"catalog/detail/:itemType/:itemID": "itemdetail",
			"catalog/detail/:itemType/:itemID/:viewType": "itemdetail",
			"catalog/detail/:itemType/:itemID/:viewType/:docID": "itemdetail",
			"oldCatalog": "reloadCatalog",
			"closedSidePanel" : "closedSidePanel"
		},

		initialize: function () {
			var self = this;
		},

		catalog: function () {
            $("#div-messagebar").show();
            hideMessages();
			pchistory = document.URL;
			recentHash = window.top.location.hash;
			this.closeView(catalogInitial);
			this.closeView(catalogDetail);
			catalogInitial = new CatalogView();
			$("#div-main").html(catalogInitial.el);
		},

		

		itemdetail: function(itemType, itemID, viewType, docID) {
			pchistory = document.URL;
			recentHash = window.top.location.hash;
			var option = [];
			option.push(itemID);
			option.push(itemType);
			option.push(viewType);
			option.push(docID);
			

			if(catalogDetail) {
				this.closeView(catalogDetail);
				catalogDetail = new ItemDetailView(option);
				$("#div-main-detail").html(catalogDetail.el);
			}

			else {
				this.closeView(catalogInitial);
				catalogInitial = new CatalogView(function () {
					catalogInitial.openItemDetail();
					catalogDetail = new ItemDetailView(option);
					$("#div-main-detail").html(catalogDetail.el);
				});
				$("#div-main").html(catalogInitial.el);
			}
			
		},

		sidePanel: function(name) {
			pchistory = document.URL.replace('/' + name,'');
			if(name=='Email Tracker'){
				window.location.href = "#collateral/"+name;
			}
			else if(catalogInitial) {
				catalogInitial.comeBack(['sampleObj']);
				catalogInitial.showSidePanel(name);
			}
		},

		reloadCatalog: function() {
			pchistory = document.URL;
			recentHash = window.top.location.hash;
			this.closeView(catalogDetail);
			catalogInitial.reInitialize(true);
			catalogInitial.$("#div-main-catalog").show();
		},
		
		sidePanelTechnique: function(id) {
			var that = this;
			if(catalogInitial) {
				if($("#div-sidepanel").css("display")!="none") {
					catalogInitial.sidepanelview.showSystemsforTechnique(id);
				}
				else {
					catalogInitial.comeBack(['sampleObj']);
					catalogInitial.showSidePanel("Technique",id);
				}
			}
		},
		
		closedSidePanel: function() {
			window.history.replaceState("",'',recentHash);
		},

		closeView: function(view) {
			if(view!=undefined) {
				view.unbind();
				view.remove();
				view = undefined;
				$("#btn-back").unbind();
			}

			if(collateralInitial!=undefined) {
				collateralInitial.unbind();
				collateralInitial.remove();
				collateralInitial = undefined;
			}

			if(collateralDetail!=undefined) {
				collateralDetail.unbind();
				collateralDetail.remove();
				collateralDetail = undefined;
			}

			if(shareView!=undefined) {
				shareView.unbind();
				shareView.remove();
				shareView = undefined;
			}

			if(shareStatusScreen!=undefined) {
				shareStatusScreen.unbind();
				shareStatusScreen.remove();
				shareStatusScreen = undefined;
			}

		}
	});

	return CatalogRouter;
});
