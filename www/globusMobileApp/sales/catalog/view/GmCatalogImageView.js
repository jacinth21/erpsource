/**********************************************************************************

 * File:        GmImageView.js
 * Description: Contains the system images downloaded from server.
 *              The View has the functions related to Images in the Details page
 *              Loads after the Catalog View.
 * Version:     1.0
 * Author: 		praja 
 **********************************************************************************/


/*global define*/
define([
    'jquery', 'underscore', 'backbone', 'handlebars',
    'global', 'notification', 'daosetup',  
    'itemcollection', 'filesync'
],

function ($, _, Backbone, Handlebars, Global, Notification, DAOSetup, Items, FileSync) {

	'use strict';

	var ImageView = Backbone.View.extend({

		template: fnGetTemplate(URL_Catalog_Template,"GmImages"),

		initialize: function(itemInfo) {
			var that = this;
 			this.strID = itemInfo[0];
			this.strItemType = itemInfo[1];
			this.parentView = itemInfo[3];
			this.partloading = false;
			this.confirmDownload = true;

			fnGetLocalFileSystemPath(function(path) {
                that.localpath = path;
            });
			
			var type = eval("img" + this.strItemType.toLowerCase());

			switch(this.strItemType) {
			case "part":
				fnGetImageof(type,this.strID,function(data) {
				if(data.length>0) {
					that.parentView.$("#div-no-image").html("");
					that.render(data);
				}
				else {
					that.parentView.$("#btn-camera").hide();
					that.parentView.$("#div-no-image").html("Part image unavailable");
				}
        
				});
				break;

			case "set":

				fnGetImageof(type,this.strID,function(data) {
				if(data.length>0) {
					that.parentView.$("#div-no-image").html("");
					that.render(data);
					that.parentView.$("#description").addClass("set-group-itemlist-count").removeClass("set-group-itemlist-count-no-image");
					that.parentView.$("#div-detail-part").removeClass("div-itemlist-big-size").addClass("div-itemlist-small-size");
				}
				else {
					that.parentView.$("#btn-camera").hide();
					that.parentView.$("#div-no-image").html("Set image unavailable");
					that.parentView.$("#description").addClass("set-group-itemlist-count-no-image").removeClass("set-group-itemlist-count");
					that.parentView.$("#div-detail-part").addClass("div-itemlist-big-size").removeClass("div-itemlist-small-size");
				}
        
				});
				break;
          
			case "system":
				fnGetImageof(type,this.strID,function(data) {
				if(data.length>0) {
					that.parentView.$("#div-no-image").html("");
					that.render(data);
				}
				else {
					that.parentView.$("#btn-camera").hide();
					that.parentView.$("#div-no-image").html("System image unavailable");
					that.parentView.$("#div-detail-desc-content").addClass("overview-big-size");
				}
        
				});
				break;

			case "group":
				fnGetImageof(type,this.strID,function(data) {
				if(data.length>0) {
					that.parentView.$("#div-no-image").html("");
					that.render(data);
					that.parentView.$("#description").addClass("set-group-itemlist-count").removeClass("set-group-itemlist-count-no-image");
					that.parentView.$("#div-detail-part").removeClass("div-itemlist-big-size").addClass("div-itemlist-small-size");
				}
				else {
					that.parentView.$("#btn-camera").hide();
					that.parentView.$("#div-no-image").html("Group image unavailable");
					that.parentView.$("#description").addClass("set-group-itemlist-count-no-image").removeClass("set-group-itemlist-count");
					that.parentView.$("#div-detail-part").addClass("div-itemlist-big-size").removeClass("div-itemlist-small-size");
				}
        
				});
				break;
			}
		},

		render: function(data) {
			var that = this;
	        this.images = data;
	        this.i=0;
	        this.configPath(that.images[that.i]);
		},

		showImageDimension: function(e){
	    	if((e.target.href == "") && (!this.partloading)) {
	    		var that = this;
		    	var elem = e.currentTarget;
	            var PartID = $(elem).find("a[name='ID']").text();
	            var type = eval("imgpart");
	            $(".ul-selected-row").each(function(){
	            	$(this).removeClass("ul-selected-row");
	            })
	            $(elem).parent().addClass("ul-selected-row");
	            that.parentView.$("#div-detail-image-container").empty();
	            that.parentView.$("#description").addClass("set-group-itemlist-count-no-image").removeClass("set-group-itemlist-count").removeClass(".no-internet-part-count");
	            fnGetImageof(type,PartID,function(data) {
            		if(data.length>0) {
	    				that.parentView.$("#div-no-image").html("");
	    				that.parentView.$("#btn-download").hide();
	    				that.parentView.$("#description").addClass("set-group-itemlist-count").removeClass("set-group-itemlist-count-no-image");
	    				that.parentView.$("#div-detail-part").removeClass("div-itemlist-big-size").addClass("div-itemlist-small-size");
	    				that.partloading = true;
						that.render(data);
	    			}
	    			else {
	    				that.parentView.$("#div-no-image").html("Image of Part - " + PartID + " unavailable");
	    				that.parentView.$("#description").addClass("set-group-itemlist-count-no-image").removeClass("set-group-itemlist-count");
	    				that.parentView.$("#div-detail-part").addClass("div-itemlist-big-size").removeClass("div-itemlist-small-size");
			            that.parentView.$("#btn-download").hide();
	    			}
				});
	    	}
	    },
		
		/*This function checks if the images are available in the device, otherwise it takes the images from server */
		configPath: function(image) {
			var that = this;
			var src = "";

            var title = image["Title"];
            title = title.replace('"','&quot;');
            title = title.replace(/[\\#,+()$~%'":*?<>{}]/g,'');

			fnGetImage(that.localpath+encodeURI(image["Path"]), function(status) {
		
		    if(status!="error") {
                    src = that.localpath+encodeURI(image["Path"]);
                    var strImage = '[{"ID" : "' + image["ID"] + '", "Name" : "' + image["Name"] + '", "Path" : "' + src + '", "Title" : "' + title + '"}]';
 		            that.$el.html(that.template($.parseJSON(strImage)));
		            that.showImage(image["ID"]);
		            that.incrementI();
                }
                else if(navigator.onLine) {
                    src = setFileServer()+encodeURI(image["Path"])+AzureSASToken;
                    var strImage = '[{"ID" : "' + image["ID"] + '", "Name" : "' + image["Name"] + '", "Path" : "' + src + '", "Title" : "' + title + '"}]';
                   	that.$el.html(that.template($.parseJSON(strImage)));
                   	that.parentView.$("#div-detail-image-container").removeClass('hide');
                	that.parentView.$("#div-no-image").removeClass('hide');
		            that.showImage(image["ID"]);
		            that.incrementI();
		            that.processError(this); 
		            that.parentView.$("#btn-download").show();
		            that.parentView.$("#btn-download").addClass("show-btn-download-viewer");
		            that.parentView.$(".div-detail-navigation-iphone #btn-download").show();
		            
                }
//                else {
//                	that.parentView.$("#description").addClass("no-internet-part-count").removeClass("set-group-itemlist-count").removeClass("set-group-itemlist-count-no-image");
//                	that.parentView.$("#div-detail-image-container").addClass('hide');
//                	that.parentView.$("#div-no-image").addClass('hide');
//                	showMsgView("006");
//                }
               
            });
		},


		processError: function(img) {
			var that = this;
			var image = document.getElementsByClassName("detail-image");
			for(var i=0;i<image.length;i++) {
				image[i].onerror = function() {
					console.log(this.src);
					this.src = "globusMobileApp/image/icon/ios/missing.jpeg";

					that.parentView.$("#div-itemlist-count").addClass("set-group-itemlist-count");
					that.parentView.$("#btn-download").hide();
					$("#btn-download-viewer").hide();
					$("#btn-download").removeClass("show-btn-download-viewer");
					this.className = "img-missing";
				}
			}
		},

		showImage: function(id) {
			
			var imgContainer = this.parentView.$("#div-detail-image-container")
					if(this.i==0){
						$(imgContainer).html("");
					}
			imgContainer.append(this.$el.html());

			var dtlImage = imgContainer.find('img');
            dtlImage.trigger('create');
        
            
            // document.getElementById(id).style.height = "250px";
            // document.getElementById(id).style.width = "auto";
		},

		incrementI: function() {
			var that = this;
			this.i++;
	        if(this.i<this.images.length) {
	        	this.configPath(this.images[this.i]);
	        }
	        else
	        	this.partloading = false;
		},
        
		/* Download the images from server path */
		downloadImages: function() {
			var that = this;
			var i = 0;
			SuccessCnt = 0;
			if(navigator.onLine) {
				showMsgView("038");
				for(i=0;i<that.images.length;i++) {
					var image = that.images[i];
					var Source = setFileServer()+image["Path"];
					var Destination = that.localpath+image["Path"];

					var connectionType = (navigator.connection.type).toLowerCase();
					if((connectionType.indexOf('cellular')>=0) && that.confirmDownload) {
						showNativeConfirm("This device is connected to internet through Data plan. Do you really want to use Data plan to download the file?","Warning",["Yes","Yes, Don't ask me again","No"], function(btnIndex) {
							if(btnIndex==1) 
								fnDownload(Source,Destination);
							else if(btnIndex==2) {
								that.confirmDownload = false;
								fnDownload(Source,Destination);
							}
						})
					}
					else
						fnDownload(Source,Destination);
					
					
					that.parentView.$("#btn-download").hide();
					$("#btn-download-viewer").hide();
					$("#btn-download").removeClass("show-btn-download-viewer");
				}
			}
			else {
				showMsgView("007");
			}
		},

	    //Closes the View
	    close: function(event) {
	    	this.unbind();
	    	this.remove();
	    }
	});

	return ImageView;
});
