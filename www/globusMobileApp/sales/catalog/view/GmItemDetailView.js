/**********************************************************************************
 * File:        GmItemDetailView.js
 * Description: View to display the details of an item, loads after the Catalog View
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone', 'handlebars',
        'global', 'notification', 'daoproductcatalog','daosetup',
        'itemmodel', 'itemcollection', 'itemlistview', 'mpcimageview', 'imageviewerview', 'filesync'
        ],

        function ($, _, Backbone, Handlebars, Global, Notification, DAOProductCatalog, DAOSetup, Item, Items, ItemListView, ImageView, ImageViewerView, FileSync) {

	'use strict';
	var url = "catalog";
	var ItemDetailView = Backbone.View.extend({

		events : {

			"click #btn-overview" : "display",
			"click #btn-download" : "download",
			"click #div-favorite-icon" : "processFavorite",
			"click #div-detail-breadcrumb a" : "removeBreadCrumb",
			"click #btn-search, .btn-itemlist" : "close",
			"click .ul-part-list" : "callImageDimension",
			"click .li-item-part-camera a": "showPartImage",
			"click .btn-detail" : "deleteBreadCrumb",
			"click .detail-image, .imagelist-image, .img-in-camera-icon" : "openImageViewer",
			"click .div-detail-set-group-part a" : "setBreadCrumb",
			// "click #div-detail-menu": "showNavigation",
			"click #btn-camera": "triggerOpenViewer",
			"click #div-detail-group #div-itemlist ul, #div-detail-set #div-itemlist ul" : "triggerNavigation",
			"click #btn-link" : "openLink",
			"click .btn-app-link .catlog-links" : "changeApp"
		},

		template: fnGetTemplate(URL_Catalog_Template,"GmCatalogItemDetail"),
		

		initialize: function(itemInfo) {
			//alert(window.top.location.hash);
			$("body").removeClass("background-1").removeClass("background-2").removeClass("background-image");
            // $("#div-header").removeClass("background-2").addClass("background-1 background-image");
			$("#div-app-title").html(lblNm("product_catalog"));
			var that = this;
			this.strItemID = itemInfo[0];
			this.strItemType = itemInfo[1];
			this.strViewType = itemInfo[2];
			this.strDocID = itemInfo[3];
			if(this.strDocID == undefined)
				itemInfo.pop();
			if(!this.strViewType) {
				this.strViewType = "overview";
			}

			// Show Back and Home buttons
			$("#btn-back").show();
			$("#btn-home").show();
			// Hide Locale button
			$(".div-btn-locale").hide();
			// $("#div-detail-navigation").addClass('hide');
			$("#div-filter-list").hide();
			this.$el.attr("id","div-detail-page");
			itemInfo.push(this);
			switch(this.strItemType) {
			case "set":
				fnGetSetDetail(this.strItemID,function(data) {
					var Model = new Item(data);
					that.render(Model,itemInfo);
					that.$("#li-detail-name").addClass("set-name-wrap"); 
					// that.$("#div-detail-navigation").css({"background-color": "#fff"},"fast");
					// that.$(".btn-detail-navigation").hide();
					that.$("#div-detail-desc-content").hide();
					that.$("#div-detail-view").addClass("div-detail-landscape");
					that.$("#li-detail-image").addClass("li-detail-image-iphone");
					that.showParts(that.strItemID);
				});
				break;

			case "system":
				fnGetSystemDetail(this.strItemID,function(data) {
					var Model = new Item(data);

					if(that.strViewType == "overview") {
						that.detailContent = data.Detail;
						that.descContent = data.Desc;
					}

                    localStorage.setItem("PCSystemID", that.strItemID);
					that.render(Model,itemInfo);
					that.$("#div-detail-overview").addClass("detail-iphone-scroll");
					that.$("#li-detail-image").addClass("li-detail-image-iphone");
					that.$("#div-detail-view").addClass("div-detail-overview-height"); // height of overview won't affect other places

					if(that.strViewType) {
						switch(that.strViewType) {
						case "groups":
							that.$("#div-detail").addClass("detail-page-itemlist-count");
							that.showGroups(that.strItemID);
							break;
						case "sets":
							that.$("#div-detail").addClass("detail-page-itemlist-count");
							that.showSets(that.strItemID);
							break;
						
						}	
					}
				});
				break;

			case "group":
				fnGetGroupDetail(this.strItemID, function(data){
					var Model = new Item(data);
					that.render(Model,itemInfo);
					that.$("#div-detail-desc-content").hide();
					// that.$(".btn-detail-navigation").hide();
					// that.$("#div-detail-navigation").css({"background-color": "#fff"},"fast");
					that.$("#div-detail-view").addClass("div-detail-landscape");
					that.$("#li-detail-image").addClass("li-detail-image-iphone");
					that.showParts(that.strItemID);
				});
				break;

			case "part":
				fnGetPartDetail(url,this.strItemID, function(data){
					var Model = new Item(data);
					that.render(Model,itemInfo);
					// that.$("#div-detail-navigation").css({"background-color": "#fff"},"fast");
					that.$("#div-detail-overview").addClass("detail-iphone-scroll");
					// that.$(".btn-detail-navigation").hide();
					that.$("#div-detail-navigation").addClass("div-detail-navigation-iphone");
					that.$("#li-detail-image").addClass("li-detail-image-iphone-part");
				});
				break;
			}

			fnInsertRecent(this.strItemType,this.strItemID);

			fnGetLocalFileSystemPath(function(path) {
                that.localpath = path;
            });
		},

		// Render the initial contents when the app starts
		render: function (data,itemInfo) {
//			showMsgView("011");
    		this.$el.html(this.template(data.toJSON()));
			//get value from local strage
            var systemID = localStorage.getItem("PCSystemID");
            this.$("#btn-group").attr("href", "#catalog/detail/system/" + systemID + "/groups");
            this.$("#btn-set").attr("href", "#catalog/detail/system/" + systemID + "/sets");
            this.$("#btn-overview").attr("href", "#catalog/detail/system/" + systemID + "/overview");


			if(this.imageview) {
				this.imageview.close();
			}
			
			if(this.strViewType == "overview"){
				this.$("#div-detail-overview").show();
				this.imageview = new ImageView(itemInfo);
			}
			else if(this.strViewType == "images") {
				this.$("#div-detail-overview").hide();
				this.imageview = new ImageView(itemInfo);
			}
			else {
				this.$("#div-detail-overview").hide();
			}
            
      		this.data = data;
			this.$("#div-detail-breadcrumb").html(pcBreadcrumb.join(" > "));
			this.$("#div-detail-breadcrumb").append(" >");
			pcRecentBreadCrumbs.push(pcBreadcrumb.slice(0));
			
			this.hideButtons();
			var title = $("#div-app-title").text();
			
			
			return this;
		},
		
		openLink: function() {
                      
			$("#btn-link").each(function(){	
	            	$(this).toggleClass("show-app");
			})
				$(".btn-app-link").fadeToggle("fast");
		},
		
		changeApp: function(e) { 
            
            if($(e.currentTarget).hasClass("evalLink")){
                
              var url = "#eval/form" ;
					window.location.href = url;  
            }else{
                
               
            var systemID = localStorage.getItem("PCSystemID");
			fnCheckCollateralSystems(systemID,function(data) {
				if(data.length == "0") {
					showNativeAlert(lblNm("msg_system_not_published_for_mc"),lblNm("msg_system_not_published"),lblNm("msg_ok"));
				}
				else {

					mcBreadcrumb = [];
					mcBreadcrumb.push("<a href='"+ document.URL + "'>Links</a>");
					mcRecentBreadCrumbs.push(pcBreadcrumb.slice(0));
					var url = "#collateral/detail/system/"+systemID ;
					window.location.href = url;
				}
			});	
            }
		},

		
		// Check for the Favorite icon
		chkFavorite: function() {
			var that = this;
			fnChkFavorite(this.strItemID,this.strItemType,function(status) {
				if(status=="success") {
					that.$("#div-favorite-icon").addClass("favorite-add");
				}
				else {
					that.$("#div-favorite-icon").addClass("favorite-remove");
				}
			});
		},
		
		// Toggles the Details
		display: function(key) {

			if(key.currentTarget==undefined) {
				this.key = key;
			}
			else {
				this.key = "overview";
				this.$("#btn-download").css("visibility","visible");
			}
			this.$(".btn-detail").each(function() {
				$(this).removeClass("btn-selected");

			});
			this.$(".div-detail").each(function() {
				$(this).hide();
			});

			this.$("#div-detail-" + this.key).show();
			this.$("#btn-" + this.key).addClass("btn-selected");

		},

		// Triggers DownloadImages Function of ImageView
		download: function(event) {
			this.imageview.downloadImages();
		},

		// Hide and Show Necessary buttons
		hideButtons: function() {
			var that = this;
			switch(that.strItemType) {
			case "set":
				// that.$("#btn-group").hide();
				// that.$("#btn-set").hide();
				that.$(".div-top-right-icons-catalog #btn-download").addClass("catalog-download-button");
				break;

			case "system":
				// that.$("#btn-part").hide();
				that.$("#div-favorite-icon").show();
				that.$(".div-top-right-icons-catalog #btn-download").removeClass("catalog-download-button");
				that.chkFavorite();
				break;

			case "group":
				// that.$("#btn-group").hide();
				// that.$("#btn-set").hide();
				that.$(".div-top-right-icons-catalog #btn-download").addClass("catalog-download-button");
				break;

			case "part":
				// that.$("#btn-overview").hide();
				// that.$("#btn-group").hide();
				// that.$("#btn-set").hide();
				// that.$("#btn-part").hide();
				that.$(".div-top-right-icons-catalog #btn-download").addClass("catalog-download-button");
				break;
			}

			if(!that.detailContent) {
				that.$("#li-detail-detail").hide();
			}

			if(!that.descContent) {
				that.$("#li-detail-description").hide();
			}
		},

		openImageViewer: function(event) {
			var elem = event.currentTarget;
			if($(elem).hasClass("detail-image")) {
				if(!$(event.currentTarget).hasClass("img-missing")) {
					this.imageViewerView = new ImageViewerView({source: this.$("#div-detail-image-container"), ImageId : $(elem).attr('id'), callback: function  () {
						this.openViewer(event);
					}});
					this.imageViewerView.openViewer(event);
				}
			}
			else {
					this.imageViewerView = new ImageViewerView({source: $(elem).parent(), callback: function  () {
						this.openViewer(event);
					}});
					this.imageViewerView.openViewer(event);
			}
				
		},

		// Add or Reomve the Current System as Favorite
		processFavorite: function(event) {
			var icon = $(event.currentTarget);
			var iconClass = $(icon).attr("class");
			var that = this;
			if(iconClass.indexOf("favorite-remove")>-1) {
				fnInsertFavorite(this.strItemID,this.strItemType,function(status) {
					if(status == "success") {
						that.$("#div-favorite-icon").removeClass("favorite-remove").addClass("favorite-add");
					}
				});
			}
			else {
				fnRemoveFavorite(this.strItemID,function(status) {
					if(status == "success") {
						that.$("#div-favorite-icon").removeClass("favorite-add").addClass("favorite-remove");
					}
				});
			}
		},

		// Remove BreadCrumb
		removeBreadCrumb : function(e) {
			e.preventDefault();
			var Target = e.currentTarget;
			var bcid = parseInt(e.currentTarget.id);
			var length = pcBreadcrumb.length;
			for(var i=bcid;i<=length; i++){
				pcBreadcrumb.pop();
			}
			var changeLink = Target.href;
			window.location.href = changeLink;
		},

		//Sets the BreadCrumb
		setBreadCrumb: function(e) {
			e.preventDefault();
			var name = this.data.get("Name");
			var id = this.data.get("ID");
			var bc = pcBreadcrumb.length+1; 
			if(e.currentTarget.href) {
				pcBreadcrumb.push("<a id="+ bc +" href='#catalog/detail/" + this.strItemType + "/" + id + "'>" + name + "</a>");
				var showType = "";
				if(this.strViewType == "overview"){
						pcBreadcrumb.push("<a id="+ bc+" href='#catalog/detail/" + this.strItemType + "/" + id + "/" + this.strViewType + "'>PARTS</a>");
						window.location.href = e.currentTarget.href;
				}
				else {
					pcBreadcrumb.push("<a id="+ bc+" href='#catalog/detail/" + this.strItemType + "/" + id + "/" + this.strViewType + "'>" +this.strViewType + "</a>");
					window.location.href = e.currentTarget.href;
				}
			}
		},

		deleteBreadCrumb: function(e){
			e.preventDefault();
			if(e.currentTarget.href) {
				var changeLink = e.currentTarget.href;
				var chkSame = "";
				if(e.currentTarget.id!="btn-overview"){
					 chkSame = e.currentTarget.id + "s";
				}
				else{
					chkSame = e.currentTarget.id;
				}
				if(chkSame != ("btn-" + this.strViewType)){
					window.location.href = changeLink;
				}
			}
		},


		//Display List of Mapped Groups for a System
		showGroups: function(id) {
			var that = this;

			if(this.itemlistview) {
				that.itemlistview.close();
			}
			fnGetMappedGroups(url,id,function(data) {
				if( data.length == 0){
					that.$("#div-detail-group").html("<li class='li-no-display'>"+lblNm("groups_not_available")+"</li>");
				}
				else {
					var sortoptions = [{"DefaultSort" : "Name", "Elements" : [{"ID" : "Name" , "DispName" : "Group Name","ClassName":"li-item-name"},{"ID" : "Type","DispName" : "Group Type","ClassName":"li-item-type"}]}];
					
					that.showItemListView(that.$("#div-detail-group"), data, "GmItemGroup", 3, sortoptions, URL_Catalog_Template);
					$("#txt-itemlist-search").addClass("minus-left-margin-40");
					that.$("#div-detail-set").hide();
					that.$("#div-detail-group").show();
					that.$("#btn-set").show();
					that.$("#btn-overview").show();
					that.$("#btn-camera").hide();
				}
			});
			this.display("group");
		},

		//Generate the ItemListView for List of Mapped Parts / Groups / Sets
		showItemListView: function(controlID, data, itemtemplate, columnheader, sortoptions, template_URL) {
			var items = new Items(data);	
			this.itemlistview = new ItemListView(
					{ 
						el: controlID, 
						collection: items,
						columnheader: columnheader, 
						itemtemplate: itemtemplate,
						sortoptions: sortoptions,
						template_URL: template_URL
					}
			);
		},

		callImageDimension: function(e){
			//e.preventDefault();
			 e.stopPropagation();
			 if($("#div-detail-part #div-itemlist ul li.li-item-part-camera").css("display")== 'none')
				 this.imageview.showImageDimension(e);
		},
		
		 showPartImage: function(e){
			var that = this;
			var elem = $(e.currentTarget).parent();
			var PartID = $(elem).parent().find("a[name='ID']").text();
			var type = eval("imgpart");

			fnGetImageof(type,PartID,function(data) {
				if(data.length >0 ) {
				var src = "";
				fnGetImage(that.localpath+data[0].Path+data[0].Name, function(status) {
					if(status!="error") {
						src =  that.localpath+encodeURI(data[0].Path);
						that.$("#btn-download").addClass("show-btn-download-viewer");
					}
					else {
						src = setFileServer()+encodeURI(data[0].Path)+AzureSASToken;
						that.$("#btn-download").removeClass("show-btn-download-viewer");
					}
				$(elem).find('img').attr('id',data[0].ID);
				$(elem).find('img').attr('src',src);
				$(elem).find('img').attr('alt',data[0].Title);
				$(elem).find('img').hide();
				$(elem).find('img').trigger("click");
			});
			}
			else {
				$(elem).hide();
			}
			});
		},


		//Display List of Mapped Parts for Set / Group
		showParts: function(id) {
			var that = this;
			switch(this.strItemType) {

			case "set":
				fnGetPartsForSet(url,id,function(data) {
					if( data.length == 0){
						that.$("#div-detail-part").html("<li class='li-no-display'>"+lblNm("msg_parts_not_available")+"</li>");
					}
					else{
						var sortoptions =[{ "DefaultSort" : "Description", "Elements" : [{"ID" : "ID","DispName" : "Part ID","ClassName":"li-item-id"},{"ID" : "Description" , "DispName" : "Part Description","ClassName":"li-item-name"},{"ID" : "Quantity" , "DispName" : "Quantity","ClassName":"li-item-qty"}]}];
						that.showItemListView(that.$("#div-detail-part"), data, "GmItemSetPart", 3, sortoptions, URL_Catalog_Template);
					}
				});
				break;

			case "group":
				fnGetPartsForGroup(url,id,function(data) {

					if( data.length == 0){
						that.$("#div-detail-part").html("<li class='li-no-display'>"+lblNm("msg_parts_not_available")+"</li>");
					}
					else{
						var sortoptions = [{ "DefaultSort" : "Description", "Elements" : [{"ID" : "ID","DispName" : "Part ID","ClassName":"li-item-id"},{"ID" : "Description" , "DispName" : "Part Description","ClassName":"li-item-name"}]}];
						that.showItemListView(that.$("#div-detail-part"), data, "GmItemGroupPart", 3, sortoptions, URL_Catalog_Template);
					}
				});
				break;
			}
			this.display("part");
			// that.$("#btn-overview").hide();
			// that.$("#btn-part").hide();
		},	

		//Display List of Mapped Sets for a System
		showSets: function(id) {

			var that = this;
			if(this.itemlistview) {
				that.itemlistview.close();
			}

			fnGetMappedSets(url,id,function(data) {

				if( data.length == 0){
					that.$("#div-detail-set").html("<li class='li-no-display'>"+lblNm("sets_not_available")+"</li>");
				}
				else {
					var sortoptions = [{ "DefaultSort" : "Description", "Elements" : [{"ID" : "ID","DispName" : "Set ID","ClassName":"li-item-id"},{"ID" : "Description" , "DispName" : "Set Description","ClassName":"li-item-name"},{"ID" : "Type" , "DispName" : "Type","ClassName":"li-item-type"},
					                                                                  {"ID" : "Category" , "DispName" : "Category","ClassName":"li-item-category"},{"ID" : "Hierarchy" , "DispName" : "Hierarchy","ClassName":"li-item-hierarchy"}]}];
					
					that.showItemListView(that.$("#div-detail-set"), data, "GmItemSet", 3, sortoptions, URL_Catalog_Template);
					that.$("#div-detail-group").hide();
					that.$("#div-detail-set").show();
					that.$("#btn-group").show();
					that.$("#btn-overview").show();
					that.$("#btn-camera").hide();
				}
			});
			this.display("set");	
		},

	
		// show navigation buttons for phone layout
		showNavigation: function(){			
			$("#div-detail-menu").each(function(){
	            if($(this).hasClass('detail-menu'))	
	            	$(this).removeClass("detail-menu");
	            else
	    	        $("#div-detail-menu").addClass("detail-menu");
	            })
			
			$("#div-detail-navigation" ).slideToggle("fast");
		},
		
		// trigger open viewer on click camera icon for cellphone
		triggerOpenViewer: function(){
			$(".detail-image").first().trigger("click");
		},
		
		triggerNavigation: function(event) {
			if($(event.target).prop("tagName")!="A")
				$(event.currentTarget).find('a').first().trigger("click");
		},

		//Closes the View
		close: function(event) {
			this.unbind();
			$("#btn-back").unbind();
			this.remove();
		}

	});

	return ItemDetailView;
});
