/**********************************************************************************
 * File:        gmvCaseDash.js
 * Description: 
 * Version:     1.0
 * Author:     	
 **********************************************************************************/
    
    define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars',

    // Common
    'global', 'commonutil', 'daocase'
],

    function ($, _, Backbone, Handlebars,
        Global, Commonutil, DAOCase) {

        'use strict';
        var CaseDashView = Backbone.View.extend({
            
            name: "Case Dashboard",

            el: "#div-main",

            events: {
                "click .monthlinks": "openMonth",
                "click .case-details": "casedetail"
            },

            /* Load the templates */            

            template_CaseMainModule: fnGetTemplate(URL_CaseSchdlr_Template, "gmtCaseHome"),
            template_CaseDashboard: fnGetTemplate(URL_CaseSchdlr_Template, "gmtCaseDashboard"),
            template_CaseList: fnGetTemplate(URL_CaseSchdlr_Template, "gmtCaseList"),

            initialize: function (options) {
                $("body").removeClass();
                $("#btn-home").show();
                $("#btn-back").show();
                $("#div-app-title").html(lblNm("case_scheduler"));
            },

            // Render the initial contents when the Case View is instantiated
            render: function () {
                var that = this;
                this.$el.html(this.template_CaseMainModule());
                $(".div-scheduler-case").addClass("hide");
                $("#div-casescheduler-dashboard").removeClass("hide");
                $(".newcase-footer-btns-section").addClass("hide");
                $("#div-casescheduler-dashboard").html(this.template_CaseDashboard());
                this.TMData=[];
                this.NMData= [];
                this.LMData= [];
                this.shared_data=[];
				this.shared_case=[];
                
                console.log(document.getElementById('thisMonth'));
                var input = {
                        "token": localStorage.getItem("token"),
                        'partyid': localStorage.getItem('partyID')
                }
                
                  fnGetWebServerData("case/casedtl", undefined, input, function (data) {
                       console.log(data);
                      that.shared_case=data;
                       });   
				 fnGetWebServerData("case/dashdata", undefined, input, function (data) {
                       console.log(data);
                      that.shared_data=data;
                       });
                
                 fnGetWebServerData("case/dashboard", undefined, input, function (data) {
                    console.log(data);
                     that.TMData = [
                    {
                        "id": "PENDINGDO",
                        "value": data.TM_DOPEND,
                        "label": "= Pending DO",
                        color:  "#d33829"
                    },
                    {
                        "id": "DOBOOKED",
                        "value":  data.TM_DOBOOKED,
                        "label": "= DO Booked",
                        color: "#00366e"
                    },
					{
                        "id": "NOLOANER",
                        "value": data.TM_NOLOANER,
                        "label": "= No Loaners Requested",
                        color: "#00d607"
                    },
                    {
                        "id": "LOANER",
                        "value": data.TM_LOANER,
                        "label": "= Loaners Requested",
                        color: "#ffdd00"
                    },
                    {
                        "id": "CASECL",
                        "value": data.TM_CANCELEDCASE,
                        "label": "= Canceled Case",
                        color: "#d9d9d9"
                    }
                ];
                     that.NMData = [
                    {
                         "id": "PENDINGDO",
                        "value": data.NM_DOPEND,
                        "label": "= Pending DO",
                        color: "#d33829"
                    },
                    {
                        "id": "DOBOOKED",
                        "value": data.NM_DOBOOKED,
                        "label": "= DO Booked",
                        color: "#00366e"
                    },
                    {
                        "id": "NOLOANER",
                        "value": data.NM_NOLOANER,
                        "label": "= No Loaners Requested",
                        color: "#00d607"
                    },
                    {
                         "id": "LOANER",
                        "value": data.NM_LOANER,
                        "label": "= Loaners Requested",
                        color:  "#ffdd00"
                    },
                    {
                         "id": "CASECL",
                        "value": data.NM_CANCELEDCASE,
                        "label": "= Canceled Case",
                        color: "#d9d9d9"
                    }
                ];
                     that.LMData = [
                    {
                         "id": "PENDINGDO",
                        "value": data.LM_DOPEND,
                        "label": "= Pending DO",
                        color: "#d33829"
                    },
                    {
                         "id": "DOBOOKED",
                        "value": data.LM_DOBOOKED,
                        "label": "= DO Booked",
                        color: "#00366e"
                    },
                    {
                         "id": "NOLOANER",
                        "value": data.LM_NOLOANER,
                        "label": "= No Loaners Requested",
                        color: "#00d607"
                    },
                    {
                        "id": "LOANER",
                        "value": data.LM_LOANER,
                        "label": "= Loaners Requested",
                        color: "#ffdd00"
                    },
                    {
                         "id": "CASECL",
                        "value": data.LM_CANCELEDCASE,
                        "label": "= Canceled Case",
                        color: "#d9d9d9"
                    }
                ];
                     $('#defaultOpen').trigger("click");
                });
                
                $("#btn-case-link").unbind('click').bind('click', function () {
						 that.openLink();
                });
               
               fnGetExcSetFL(function(excSetFL){});
            },

            openMonth: function (e) {
                var that = this;
                var tab_id = e.target.id;
                $('.top-header button').removeClass('current');
                $('.monthContent').removeClass('current');
                $("#" + tab_id).addClass('current');
                if (tab_id == 'lastmonthOpen') {
                    $('#lastMonth').addClass('current');                    
                    that.removeCanvas("#chart-container-last");
                    that.opendonutChartLast(); 
					if (this.shared_case.arrlmshare == undefined)
					$("#lastMonth .month-content").html("<div class='noCaseData'> "+ lblNm("no_case_data") +" </div>");
					else
                    $("#lastMonth .month-content").html(this.template_CaseList(this.shared_case.arrlmshare));
                } else if (tab_id == 'nextmonthOpen') {
                    $('#nextMonth').addClass('current');
                    that.removeCanvas("#chart-container-next");
                    that.opendonutChartNext();
					if (this.shared_case.arrnmshare == undefined)
					$("#nextMonth .month-content").html("<div class='noCaseData'> "+ lblNm("no_case_data") +" </div>");
					else
                    $("#nextMonth .month-content").html(this.template_CaseList(this.shared_case.arrnmshare));
                } else {
                    $('#thisMonth').addClass('current');
                    that.removeCanvas("#chart-container-this");
                    that.opendonutChart();      
					if (this.shared_case.arrtmshare == undefined)
					$("#thisMonth .month-content").html("<div class='noCaseData'> "+ lblNm("no_case_data") +" </div>");
					else
                    $("#thisMonth .month-content").html(this.template_CaseList(this.shared_case.arrtmshare));
                }
            },
            
            removeCanvas: function (id) {
                $(id).removeClass('dhx_chart');
                $(id + ' canvas').remove();
                $(id + ' .dhx_chart_legend').remove();
                $(id + ' .dhx_canvas_text').remove();
            },

            showData: function (e) {
                var input = {};
                input.status = "test";
                input.prdreqdid = "100";
                input.acctid = "104";
                input.fromdt = "14-05-2019";
                input.todt = "15-05-2019";
                        $(".Popupchart").addClass("active");
                        $(".close").on("click", function () {
                            $(".Popupchart").removeClass("active");
                        });
            },
            
            opendonutChart: function () {
                var that=this;
                var pieChart = new dhtmlXChart({
                    view: "donut",
                    container: "chart-container-this",
                    value: "#value#",
                    color: "#color#",
                    shadow: false,
                    x: 170,
                    y: 130,
                    legend: {
                        width: 355,
                        align: "center",
                        valign: "bottom",
                        layout: "x",
                        marker: {
                            type: "round",
                            width: 15,
                            height: 15
                        },
                        toggle: "enable",
                        template: "#label#"
                    },
                    pieInnerText: "<b>#value#</b>"
                });

                var chartDt = _.filter(that.TMData, function (dt) {
					return dt.value != 0;
				});
				var chartDtNull= _.filter(that.LMData, function (dt) {
					return dt.value != null;
				});
				if (chartDt != "" && chartDtNull != "")
					pieChart.parse(that.TMData, "json");
				else
					$("#chart-container-this").html("<div class='noCaseData'> "+ lblNm("no_case_data") +" </div>");
                 pieChart.attachEvent("onItemclick", function (id, ev, trg){
                     that.showKitpopup("casePopup", "Upcoming Cases", "caseContent"); 
					 $("#caseContent").addClass(id);
//                     if(id=="PENDINGDO")
                     var resObj=_.filter(arrayOf(that.shared_data.arrtmchart), function(obj){
                         return obj.tmstatus==id
                     });
//                     else
//                         var resObj=_.filter(arrayOf(that.shared_data.arrtmchart), function(obj){
//                         return obj.casetype==id;
//                     });
                     console.log(resObj);
                     $("#caseContent").html(that.template_CaseList(resObj));
					  $(".case-details").unbind('click').bind('click', function (e) {
						  $(".casePopup .close").trigger("click");
						 that.casedetail(e);
					 });
                   
                    
                 });

            },

            opendonutChartNext: function () {
                var that=this;
                var pieChart1 = new dhtmlXChart({
                    view: "donut",
                    container: "chart-container-next",
                    value: "#value#",
                    color: "#color#",
                    shadow: false,
                    x: 170,
                    y: 130,
                    legend: {
                        width: 355,
                        align: "center",
                        valign: "bottom",
                        layout: "x",
                        marker: {
                            type: "round",
                            width: 15,
                            height: 15
                        },
                        toggle: "enable",
                        template: "#label#"
                    },
                    pieInnerText: "<b>#value#</b>"
                });
				
				var chartDt = _.filter(that.NMData, function (dt) {
					return dt.value != 0;

				});
                var chartDtNull= _.filter(that.LMData, function (dt) {
					return dt.value != null;
				});
				if (chartDt != "" && chartDtNull != "")
					pieChart1.parse(that.NMData, "json");
				else
					$("#chart-container-next").html(" <div class='noCaseData'> "+ lblNm("no_case_data") +" </div>");
                   pieChart1.attachEvent("onItemclick", function (id, ev, trg){
                     that.showKitpopup("casePopup", "Upcoming Cases", "caseContent"); 
					    $("#caseContent").addClass(id);
                     if(id=="PENDINGDO")
                     var resObj1 =_.filter(arrayOf(that.shared_data.arrnmchart), function(obj){
                         return obj.casedotype==id
                     });
                     else
                         var resObj1=_.filter(arrayOf(that.shared_data.arrnmchart), function(obj){
                         return obj.casetype==id
                     });
                     console.log(resObj1);
                     $("#caseContent").html(that.template_CaseList(resObj1));
					    $(".case-details").unbind('click').bind('click', function (e) {
						  $(".casePopup .close").trigger("click");
						 that.casedetail(e);
					 });
                 });

            },

            opendonutChartLast: function () {
                var that=this;
                var pieChart2 = new dhtmlXChart({
                    view: "donut",
                    container: "chart-container-last",
                    value: "#value#",
                    color: "#color#",
                    shadow: false,
                    x: 170,
                    y: 130,
                    legend: {
                        width: 355,
                        align: "center",
                        valign: "bottom",
                        layout: "x",
                        marker: {
                            type: "round",
                            width: 15,
                            height: 15
                        },
                        toggle: "enable",
                        template: "#label#"
                    },
                    pieInnerText: "<b>#value#</b>"
                });
				
				var chartDt = _.filter(that.LMData, function (dt) {
					return dt.value != 0;

				});
                var chartDtNull= _.filter(that.LMData, function (dt) {
					return dt.value != null;
				});
				if (chartDt != "" && chartDtNull != "")
					pieChart2.parse(that.LMData, "json");
				else
					$("#chart-container-last").html("<div class='noCaseData'> "+ lblNm("no_case_data") +" </div>");
				
                 pieChart2.attachEvent("onItemclick", function (id, ev, trg){
                     that.showKitpopup("casePopup", "Upcoming Cases", "caseContent"); 
					  $("#caseContent").addClass(id);
                     if(id=="PENDINGDO")
                     var resObj3 =_.filter(arrayOf(that.shared_data.arrlmchart), function(obj){
                         return obj.casedotype==id
                     });
                     else 
                         var resObj3=_.filter(arrayOf(that.shared_data.arrlmchart), function(obj){
                         return obj.casetype==id
                     });
                     $("#caseContent").html(that.template_CaseList(resObj3));
					  $(".case-details").unbind('click').bind('click', function (e) {
						  $(".casePopup .close").trigger("click");
						 that.casedetail(e);
					 });     
                 });
            },
            
            
              showKitpopup:function(containerDiv, titleText, contentID){
                var that=this;
                 showPopup();
                $("#div-crm-overlay-content-container").addClass(containerDiv);
                $("#div-crm-overlay-content-container").find(".modal-footer").addClass("hide");
                $("#div-crm-overlay-content-container").find('.modal-title').text(titleText);
                
                $("#div-crm-overlay-content-container .modal-body").html("<div id='"+contentID+"'></div>");
                $("."+containerDiv+" .close").unbind('click').bind('click', function (e) {
                        that.hideKitpopup(containerDiv);
                 });
                getLoader("#"+contentID);
            },
            
            hideKitpopup: function(containerDiv){
				var that=this;	
                hidePopup();
                $("#div-crm-overlay-content-container").removeClass(containerDiv);
                $("#div-crm-overlay-content-container").find(".modal-footer").removeClass("hide");
				$("#div-crm-overlay-content-container").find("#div-crm-popup-msg").html("");
				$("#div-crm-overlay-content-container").find("#div-crm-popup-msg").hide();
				if(containerDiv=='sharePopup')
                                Backbone.history.loadUrl(Backbone.history.fragment);
            },
             openLink: function () {
                $("#btn-case-link").each(function () {
                    $(this).toggleClass("show-app");
                })
                $(".btn-app-link").fadeToggle("fast");
            },
			
			casedetail: function (event) {
				var that = this;
				var caseinfoid = $(event.currentTarget).data("caseinfoid");
				window.location.href = "#case/id/" + caseinfoid;
			}

        });

        return CaseDashView;
    });
