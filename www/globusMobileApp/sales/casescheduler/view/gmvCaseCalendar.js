/**********************************************************************************
 * File:        gmCaseCalendar.js
 * Description: 
 * Version:     1.0
 * Author:     	tmuthusamy
 **********************************************************************************/
define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars', 'searchview', 'dropdownview', 'moment', 'evendar',

    // Common
    'global', 'commonutil'
],

    function ($, _, Backbone, Handlebars, SearchView, DropDownView, Moment, Evendar,
        Global, Commonutil) {

        'use strict';
        var CaseCalendarView = Backbone.View.extend({

            name: "Case Calendar",

            el: "#div-main",

            events: {},
            /* Load the templates */
            template: fnGetTemplate(URL_CaseSchdlr_Template, "gmtCaseCalendar"),


            initialize: function () {
                $("#div-app-title").html("Case Calendar");
            },

            render: function () {
                var that = this;
				var strDate = $.datepicker.formatDate('dd/mm/yy', new Date());
                this.$el.html(this.template());
				var today = new Date();
				var lastDayMonth =  $.datepicker.formatDate('dd/mm/yy',new Date(today.getFullYear(), today.getMonth()+1, 0));
				$("#div-detail-navigation .btn-detail").removeClass("btn-selected");
                $("#btn-case-report").addClass("btn-selected");
                this.calendarData = [];
                var input = {
                    "token": localStorage.getItem("token"),
                    'partyid': localStorage.getItem('partyID')
                };
                getLoader("#evendar-header");
                fnGetWebServerData("case/casecalendar", undefined, input, function (data) {
                    console.log(data);
                    var i = 0;
                    data = data.arrcaldata;
                    _.each(data, function (dt) {
//						if(!((dt.pendingdo != 0) && ((dt.surgerydt >= strDate) && (dt.surgerydt <= lastDayMonth)))) {
                        that.calendarData[i] = {};
                        that.calendarData[i].id = i;
                        that.calendarData[i].caseinfoid = dt.caseinfoid;
                        that.calendarData[i].place = " ";
                        that.calendarData[i].name = dt.surgerytime + " - " + dt.surgeryendtime + " " + dt.surgeonnm;
                        that.calendarData[i].startDate = dt.surgerydt + " " + dt.surgerytime;
                        that.calendarData[i].endDate = dt.surgerydt + " " + dt.surgeryendtime;
							var splitDt= dt.surgerydt.split("/");
							var compDt=splitDt[1]+"/"+splitDt[0]+"/"+splitDt[2];
                        if (dt.dobooked != 0)
                            that.calendarData[i].color = "#00366e";
				        if(dt.loaner != 0)							
                            that.calendarData[i].color = "#ffdd00";
						if((new Date(compDt) <=today) && (dt.noloaner != 0)){
							that.calendarData[i].color = "#d33829";}
						else if ((dt.noloaner != 0) || (dt.pendingdo != 0)) {
                            that.calendarData[i].color = "#00d607";}
//			            if(dt.pendingdo != 0)							
//                            that.calendarData[i].color = "#d33829";
					    if(dt.cancelcase != 0)							
                            that.calendarData[i].color = "#d9d9d9";
                        that.calendarData[i].excerpt = dt.accname + " " + dt.surglevel;
                        i++;
//						}
                    });
                    console.log(that.calendarData);

                });
                setTimeout(function () {
                    that.fnFetchCalDtl();
                }, 1000);
                $("#btn-case-link").unbind('click').bind('click', function () {
                    that.openLink();
                });

            },


            fnFetchCalDtl: function () {
                var that = this;
                moment.locale('en', {
                    months: 'January_February_March_April_May_June_July_August_Septemper_October_November_December'.split('_'),
                    weekdaysShort : 'SUN_MON_TUE_WED_THU_FRI_SAT'.split('_')
                });
                moment.locale('jp', {
                    months: '１月_２月_３月_４月_５月_６月_７月_８月_９月_１０月_１１月_１２月'.split('_'),
                    weekdaysShort : '日_月_火_水_木_金_土'.split('_')
                });
                $("#evendar-header").evendar({
                    calendarCount: 1,
                    autoCloseOnSelect: true,
                    showEventDetails: false,
                    format: "DD/MM/YYYY HH:mm",
                    events: that.calendarData,
                    inline: true,
                    defaultView: "list",
                    locale: localStorage.getItem("lang"),
                    onafterselect: function (evendar, event) {
                        window.location.href = "#case/id/" + event.caseinfoid;
                    }
                });
                $(".evendar-input").after('<div class="case-cal-legend gmMarginTop15p"><ul><li><span class="cal-leg-red"></span> = '+lblNm("pending_do")+'</li><li><span class="cal-leg-blue"></span> = '+lblNm("do_booked")+'</li><li><span class="cal-leg-yellow"></span> = '+lblNm("loaners_request")+'</li><li><span class="cal-leg-green"></span> = '+lblNm("no_loaner_request")+'</li><li><span class="cal-leg-grey"></span> = '+lblNm("canceled_case")+'</li></ul></div>');

            },

            openLink: function () {
                $("#btn-case-link").each(function () {
                    $(this).toggleClass("show-app");
                })
                $(".btn-app-link").fadeToggle("fast");
            },
        });
        return CaseCalendarView;
    });