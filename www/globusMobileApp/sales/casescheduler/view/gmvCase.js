/**********************************************************************************
 * File             :        gmvkit.js
 * Description      :        View to represent Surgery Kits
 * Path             :        
 * Author           :        mahendrand
 **********************************************************************************/
define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars', 'searchview', 'dropdownview',

    // Common
    'global', 'commonutil', 'gmvSearch', 'daocase', 'gmmCase'
],

    function ($, _, Backbone, Handlebars, SearchView, DropDownView,
        Global, Commonutil, GMVSearch, DAOCase, GMMCase) {


        GM.View.Case = Backbone.View.extend({

            name: "Case Scheduler",

            el: "#div-main",

            template_CaseMainModule: fnGetTemplate(URL_CaseSchdlr_Template, "gmtCaseHome"),
            template_CreateCase: fnGetTemplate(URL_CaseSchdlr_Template, "gmtCreateCase"),
            template_CasePlanInventory: fnGetTemplate(URL_CaseSchdlr_Template, "gmtCasePlanInventory"),
            template_CaseSetMain: fnGetTemplate(URL_CaseSchdlr_Template, "gmtCaseSetMain"),
            template_CaseSetList: fnGetTemplate(URL_CaseSchdlr_Template, "gmtCaseSetList"),
            template_CasePartMain: fnGetTemplate(URL_CaseSchdlr_Template, "gmtCasePartMain"),
            template_CasePartList: fnGetTemplate(URL_CaseSchdlr_Template, "gmtCasePartList"),
            template_CaseShare: fnGetTemplate(URL_CaseSchdlr_Template, "gmtCaseShare"),
            template_CaseSummary: fnGetTemplate(URL_CaseSchdlr_Template, "gmtCaseSummary"),
            template_numberkeypad: fnGetTemplate(URL_DO_Template, "GmDOPartsNumberKeypad"),
            template_DhxReportMobile: fnGetTemplate(URL_Common_Template, "gmtDhxReportMobile"),

            events: {
                "change input,select,textarea": "updateModel",
                "click .liSharebtn": "shareKits",
                "click #addset": "Gmaddset",
                "click #addpart": "Gmaddpart",
                "click #summary-addset": "GmSummaryPlanSetTab",
                "click #summary-addpart": "GmSummaryPlanPartTab",

                "click .qty-plus-pu": "incQty",
                "click .qty-minus-pu": "decQty",

                "click #setbundle": "Gmsetbundle",
                "click #setsearch": "Gmsetsearch",
                "click #btn-sharecase": "shareCasePopup",
                "click #btn-cnclcase": "cancelCase",
                "click #btn-editcase": "editCase",
                "click #bnt-next-case": "nextPageFunc",
                "click #bnt-prev-case": "prevPageFunc",
                "click #btn-cancel-case": "cancelPage",
                "click .btnBookCase": "bookCase",
                "click .btn-add-npi-surgeon": "fnAddCaseSurgeons",
                "change #new-case-surgeon-npi input, #new-case-surgeon-name input": "fnOnChange",
                "click .remove-case-surgeon": "removeParty",
                "click .pcase-part-num span": "partlistPopup",
                "click .repDel": "removeRep",                
                "click #img-barcode": "getBarcode",
                "click .div-keys": "getKeyValues",
                "click .partCaseQty": "showNumberKeypad",
                "click #div-num-symbol": "deleteKeyValues",
                "click #div-keypad-close": "closeKeypad",
                "click #div-btn-done": "displayQuantity"
            },

            initialize: function (options) {
                var that = this;
                $(window).on('orientationchange', this.onOrientationChange);
                console.log(JSON.stringify(options));
                this.caseinfoid = options.caseinfoid;
                this.casedata = options.data;

                console.log(options);
                $('#btn-home').show();
                $("#btn-back").show();

                if (GM.Global.Case == undefined)
                    GM.Global.Case = {};

                if (that.caseinfoid && that.caseinfoid != 0) {
                    this.caseModel = new GMMCase(this.casedata);
                    this.caseModel.set("arrcasesetvo", arrayOf(this.casedata.arrcasesetvo));
                    this.caseModel.set("arrcasepartvo", arrayOf(this.casedata.arrcasepartvo));
                    this.caseModel.set("arrcasepartyvo", arrayOf(this.casedata.arrcasepartyvo));
                    this.caseModel.set("arrcasesharevo", arrayOf(this.casedata.arrcasesharevo));

                    if (this.caseModel.get('arrcasesetvo')[0] == null || this.caseModel.get('arrcasesetvo')[0] == undefined)
                        this.caseModel.set('arrcasesetvo', []);
                    if (this.caseModel.get('arrcasepartvo')[0] == null || this.caseModel.get('arrcasepartvo')[0] == undefined)
                        this.caseModel.set('arrcasepartvo', []); 
                    if (this.caseModel.get('arrcasepartyvo')[0] == null || this.caseModel.get('arrcasepartyvo')[0] == undefined)
                        this.caseModel.set('arrcasepartyvo', []);
                    if (this.caseModel.get('arrcasesharevo')[0] == null || this.caseModel.get('arrcasesharevo')[0] == undefined)
                        this.caseModel.set('arrcasesharevo', []);

                    this.caseModel.set($.parseJSON(JSON.stringify(this.caseModel)));
                    GM.Global.Case.Mode = "view";
                } else {
                    this.caseModel = new GMMCase();
                    GM.Global.Case.Mode = "create";
                }
                this.errors = "";
                that.checkScreen = '';
                this.newTagCheck='';
            },

            render: function () {
                var that = this;
                this.$el.html(this.template_CaseMainModule());
                if ($(window).width() <= 767) {
                    $("#li-update-count").addClass("hide");
                    $("#btn-home").addClass("hide");
                    $(".div-kit-detail-navigation").addClass("hide");
                    this.caseModel.set("device", "iphone");
                } else {
                    this.caseModel.set("device", "ipad");
                }
                $("#div-app-title").removeClass("gmVisibilityHidden").addClass("gmVisibilityVisible");
                if (GM.Global.Case.Mode == "view") {
                    $("#div-app-title").html("Case Summary");
                    console.log("CASE MODEL>>>>>" + this.caseModel.toJSON());
                    $(".div-scheduler-case").addClass("hide");
                    
                    that.splitGrpdModel(that.caseModel.toJSON(),function(data){
                        $("#div-casescheduler-summary").html(that.template_CaseSummary(data));
                    
                    $(".newcase-footer-btns-section").addClass("hide");
                    $("#div-casescheduler-summary").removeClass("hide");
                    });
                } else if (GM.Global.Case.Mode == "create") {
                    $("#div-app-title").html(lblNm("new_case"));
                    $("#div-detail-navigation .btn-detail").removeClass("btn-selected");
                    $("#btn-case-book").addClass("btn-selected");
					$(".div-scheduler-case").addClass("hide");
                    that.splitGrpdModel(this.caseModel.toJSON(),function(data){
                    $("#div-casescheduler-create").html(that.template_CreateCase(data));
                    $("#div-casescheduler-inventory").html(that.template_CasePlanInventory(data));
                    $("#div-casescheduler-share-case").html(that.template_CaseShare(data));
                    
                    $("#set-main").html(that.template_CaseSetMain());
                    $(".setListDetails").html(that.template_CaseSetList(data));
                    $("#part-main").html(that.template_CasePartMain());
                    $(".partListDetails").html(that.template_CasePartList(data));
					$("#div-casescheduler-create").removeClass("hide");
                    $("#addset").trigger("click");
                    $("#btn-back").show();
                    that.getCaseAccountName();
                    that.searchNPI();
                    that.searchSurgeon();
                    
                    var surgLvl = that.caseModel.toJSON().surglvlid;
                    that.getcdLkupVal("#new-case-surgery-level", "#case-surgery-level", "TRLVL", surgLvl);
                    
                    var surgTyp = that.caseModel.toJSON().surgtypeid;
                    that.getcdLkupVal("#new-case-surgery-type", "#case-surgery-type", "SURTYP", surgTyp);
                    
                    that.kitTypeSearch();
                    that.searchRep();
                    that.searchSetBundle();
                    that.searchSet();
                    that.searchPart();
                    $("#bnt-prev-case").addClass("hide");
                       $(".caseMain .newcase-footer-btns-section #bnt-next-case").addClass("btnAlignRight");
                        
                        
                    });
                } else if (GM.Global.Case.Mode == "edit") {
                    $("#div-app-title").html("Edit Case");
                    $("#div-detail-navigation .btn-detail").removeClass("btn-selected");
                    $("#btn-case-book").addClass("btn-selected");
					$(".div-scheduler-case").addClass("hide");
                    that.splitGrpdModel(that.caseModel.toJSON(),function(data){                    
                    $("#div-casescheduler-create").html(that.template_CreateCase(data));
                    $("#div-casescheduler-inventory").html(that.template_CasePlanInventory(data));
                    $("#div-casescheduler-share-case").html(that.template_CaseShare(data));
                    
                    $("#set-main").html(that.template_CaseSetMain());
                    $(".setListDetails").html(that.template_CaseSetList(data));
                    $("#part-main").html(that.template_CasePartMain());
                    $(".partListDetails").html(that.template_CasePartList(data));
					$("#div-casescheduler-create").removeClass("hide");
                    $("#addset").trigger("click");
                    $("#btn-back").show();
                    that.getCaseAccountName();
                    that.searchNPI();
                    that.searchSurgeon();
                    
                    var surgLvl = that.caseModel.toJSON().surglvlid;
                    that.getcdLkupVal("#new-case-surgery-level", "#case-surgery-level", "TRLVL", surgLvl);
                    
                    var surgTyp = that.caseModel.toJSON().surgtypeid;
                    that.getcdLkupVal("#new-case-surgery-type", "#case-surgery-type", "SURTYP", surgTyp);
                    
                    that.kitTypeSearch();
                    that.searchRep();
                    that.searchSetBundle();
                    that.searchSet();
                    that.searchPart();
                    console.log(JSON.stringify(this.caseModel));
                    $("#bnt-prev-case").addClass("hide");
                    $(".caseMain .newcase-footer-btns-section #bnt-next-case").addClass("btnAlignRight");
                    $(".btnBookCase").html("Save Case");
                        
                        
                    });
                }
                
                that.togglePartSetDtl();
                $("body").on("click",function(){
                    if($("#new-case-surgeon-npi input").val() == '' && $("#new-case-surgeon-name input").val() == ''){
                        $(".btn-add-npi-surgeon-grp").hide();
                    }
                });
                $("#btn-case-link").unbind('click').bind('click', function () {
						 that.openLink();
                });
                
                $(".plan-inventory-summary-band").on("click",function(){
                     $('.woscrolalign').toggleClass("onActive");
                     $(this).find(".toggle-circle-arrow").toggleClass("onActive");
                    $('.casesum-label-thirddiv').toggleClass("onActive");
                });
            },
            
            splitGrpdModel: function (data, cb) {
                var splitGrpdModel = data;
                console.log(data);
                if (data.surgdt && data.surgdt != "") 
                       data.surgdt =  formattedDate(data.surgdt,'yyyy-MM-dd');
				
                var arrsetvo = data.arrcasesetvo;
                var arrpartvo = data.arrcasepartvo;
                
                arrsetvo = arrsetvo.filter((dt) => {
                    return dt.voidfl != 'Y';
                });
                
                arrpartvo = arrpartvo.filter((dt) => {
                    return dt.voidfl != 'Y';
                });

                splitGrpdModel.arrcasesetvo = arrsetvo.filter((dt) => {
                    return parseNull(dt.kitid)  != "";
                });
                splitGrpdModel.arrcaseadtlsetvo = arrsetvo.filter((dt) => {
                    return parseNull(dt.kitid) =="";
                });
                splitGrpdModel.arrcasepartvo = arrpartvo.filter((dt) => {
                    return parseNull(dt.kitid) != "";
                });
                splitGrpdModel.arrcaseadtlpartvo = arrpartvo.filter((dt) => {
                    return parseNull(dt.kitid) =="";
                });
                
                
                splitGrpdModel.arrcasesetvo = _.groupBy(splitGrpdModel.arrcasesetvo, function (row) {
                    return row.setid;
                });

                splitGrpdModel.arrcaseadtlsetvo = _.groupBy(splitGrpdModel.arrcaseadtlsetvo, function (row) {
                    return row.setid;
                });
                splitGrpdModel.arrcasepartvo = _.groupBy(splitGrpdModel.arrcasepartvo, function (row) {
                    return row.partnum;
                });
                splitGrpdModel.arrcaseadtlpartvo = _.groupBy(splitGrpdModel.arrcaseadtlpartvo, function (row) {
                    return row.partnum;
                });

                cb(splitGrpdModel);

            },
            
             togglePartSetDtl: function(){
                var that=this;
                 
                 $(".partSetDetailOpen").on("click", function () {
                     
                    $(this).next(".partSetDetailInnerContent").toggleClass("hide");
                    $(this).children(".toggle-circle-arrow").toggleClass("onActive");
                     
                     
                    var setnopen = $("#set-main").find("ul").children("li:nth-child(2)").hasClass("hide");
                    var adsetnopen = $("#set-main").find("ul").children("li:nth-child(4)").hasClass("hide");
                     

                     if ($(window).width() >= 767) {
                     
                     if(setnopen == true && adsetnopen == false)
                         $("#set-main").find("ul").children("li:nth-child(4)").addClass("divsetpartheig");                     
                     else
                        $("#set-main").find("ul").children("li:nth-child(4)").removeClass("divsetpartheig");
                     
                      }
                     
                      else if ($(window).width() <= 414) {

                     if(setnopen == true && adsetnopen == false)
                         $("#set-main").find("ul").children("li:nth-child(4)").addClass("divsetpartheig");                     
                     else
                        $("#set-main").find("ul").children("li:nth-child(4)").removeClass("divsetpartheig");
                     
                      }
                     
                     else if($(window).width() <= 375){

                     if(setnopen == true && adsetnopen == false)
                          $("#set-main").find("ul").children("li:nth-child(4)").addClass("divsetpartheig");                     
                     else
                        $("#set-main").find("ul").children("li:nth-child(4)").removeClass("divsetpartheig");
                     }
                     
                     
                     var partnopen = $("#part-main").find("ul").children("li:nth-child(2)").hasClass("hide");
                     var adspartnopen = $("#part-main").find("ul").children("li:nth-child(4)").hasClass("hide");
                     

                     if ($(window).width() >= 767) {
                     
                     if(partnopen == true && adspartnopen == false)
                         $("#part-main").find("ul").children("li:nth-child(4)").addClass("divsetpartheig");                     
                     else
                        $("#part-main").find("ul").children("li:nth-child(4)").removeClass("divsetpartheig");
                     
                      }
                     
                      else if ($(window).width() <= 414) {

                     if(partnopen == true && adspartnopen == false)
                         $("#part-main").find("ul").children("li:nth-child(4)").addClass("divsetpartheig");                     
                     else
                        $("#part-main").find("ul").children("li:nth-child(4)").removeClass("divsetpartheig");
                     
                      }
                     
                     else if($(window).width() <= 375){

                     if(partnopen == true && adspartnopen == false)
                          $("#part-main").find("ul").children("li:nth-child(4)").addClass("divsetpartheig");                     
                     else
                        $("#part-main").find("ul").children("li:nth-child(4)").removeClass("divsetpartheig");
                     }
                     
                });
                 
                 
                $(".togglebx").unbind().bind('click', function (e) {
                    $(this).parent().parent().parent(".part-set-wrap").toggleClass("toggled")
                    $(this).parent().parent().siblings(".partSetSecRow").toggleClass("hide");
                    $(this).find("img").toggleClass("onActive");
                });
                $(".div-set-del").on("click", function (e) {
                    that.removeSet(e);
                });
                $(".div-part-del").on("click", function (e) {
                    that.removePart(e);
                });
                $(".pcase-tag-num-input").unbind().bind('change', function (e) {
                            that.tagNumUpdate(e);
                    });
                 
                 that.swipeSetFn();
                 that.swipePartFn();
            },            
            
             // Swipe Part to delete in iphone
            swipePartFn: function () {
                if ($(window).width() <= 767) {
                    $(".kitPart").on('swipeleft', function (e, data) {
                        $(this).find(".li-content").addClass("onShowLi");
                        $(this).find(".delete-pu-span").addClass("onShowEl");
                    });

                    $(".kitPart").on('tap', function (e, data) {
                        $(this).find(".li-content").removeClass("onShowLi");
                        $(this).find(".delete-pu-span").removeClass("onShowEl");
                    });
                    
                    $(".addPart").on('swipeleft', function (e, data) {
                        $(this).find(".li-content").addClass("onShowLi");
                        $(this).find(".delete-pu-span").addClass("onShowEl");
                    });

                    $(".addPart").on('tap', function (e, data) {
                        $(this).find(".li-content").removeClass("onShowLi");
                        $(this).find(".delete-pu-span").removeClass("onShowEl");
                    });
                }
            },
            
            // Swipe Set to delete in iphone
            swipeSetFn: function () {
                if ($(window).width() <= 767) {                    
                    $(".kitSet").on('swipeleft', function (e, data) {
                        $(this).find(".li-content").addClass("onShowLi");
                        $(this).find(".delete-pu-span").addClass("onShowEl");
                    });

                    $(".kitSet").on('tap', function (e, data) {
                        $(this).find(".li-content").removeClass("onShowLi");
                        $(this).find(".delete-pu-span").removeClass("onShowEl");
                    });
                    
                    $(".addSet").on('swipeleft', function (e, data) {
                        $(this).find(".li-content").addClass("onShowLi");
                        $(this).find(".delete-pu-span").addClass("onShowEl");
                    });

                    $(".addSet").on('tap', function (e, data) {
                        $(this).find(".li-content").removeClass("onShowLi");
                        $(this).find(".delete-pu-span").removeClass("onShowEl");
                    });
                }
            },

            nextPageFunc: function () {
				var that = this;
				this.validateFunc(this.errors);
				var setVO = this.caseModel.get("arrcasesetvo").length;
				var partVO = this.caseModel.get("arrcasepartvo").length;
						
				if (this.errors == "") {
				
                $(".caseMain .div-scheduler-case").each(function () {
                    if (!($(this).hasClass("hide"))) {
                        that.checkScreen = $(this).attr("id");
                        console.log(that.checkScreen);
                        if(that.checkScreen == 'div-casescheduler-inventory'){
							if((setVO >0) || (partVO > 0)){
								 $("#bnt-next-case").addClass("hide");
								$(this).addClass("hide");
								$(this).next(".div-scheduler-case").removeClass("hide");
                                  $(".caseMain .newcase-footer-btns-section #bnt-prev-case").addClass("btnAlignLeft");
								
							} else {	
								showError("Please select any Planned Case Inventory");
                           
							}
                        }
                        else{
							$(this).addClass("hide");
                        $(this).next(".div-scheduler-case").removeClass("hide");
                            $("#bnt-prev-case").removeClass("hide");
                            $("#bnt-next-case").removeClass("hide");
                            
                              $(".caseMain .newcase-footer-btns-section #bnt-next-case").removeClass("btnAlignRight");
                            $(".caseMain .newcase-footer-btns-section #bnt-prev-case").removeClass("btnAlignLeft");
                            
                        }
                        return false;
                    }

                });
				} else {
					showError(this.errors);
				}
            },
            
            prevPageFunc: function(){
                var that = this;
                $(".caseMain .div-scheduler-case").each(function () {
                    if (!($(this).hasClass("hide"))) {
                        $(this).addClass("hide");
                        $(this).prev(".div-scheduler-case").removeClass("hide");
                        that.checkScreen = $(this).attr("id");
                        if(that.checkScreen == 'div-casescheduler-inventory'){
                            $("#bnt-prev-case").addClass("hide");
                           
                           $(".caseMain .newcase-footer-btns-section #bnt-next-case").addClass("btnAlignRight");
                             
                        }
                        else{
                            
                            $(".caseMain .newcase-footer-btns-section #bnt-prev-case").removeClass("btnAlignLeft");
                            
                            $("#bnt-prev-case").removeClass("hide");
                            $("#bnt-next-case").removeClass("hide");
                            
                             
                        }
                        return false;
                    }

                });
            },
			
			validateFunc: function () {
					var that = this;
					this.errors = "";
					var partyVO = this.caseModel.get("arrcasepartyvo");
					var currentDate = new Date();
					var compdtfmt = localStorage.getItem("cmpdfmt");
					var strDate = $.datepicker.formatDate('mm/dd/yy', currentDate);
					var surgdt = $("#case-surgeon-date").val();
					var surgstime = $("#case-surgeon-start-time").val();
					var surgetime = $("#case-surgeon-end-time").val();
					var acc = $(".div-dropdown-text").val();
					var sur = $(".case-surg-item").val();
					var surglist = $(".new-case-surgeons-list").val();
					var account = this.caseModel.toJSON().accid;
					var surgnm = partyVO.length;
					var surgdate = $.format.date(new Date(surgdt), compdtfmt);
					var sysdate = formattedDate(strDate, compdtfmt);
					var curtime = currentDate.getHours() + ":" + currentDate.getMinutes();
					var sDt = surgdt+"T"+surgstime;
					var formatsDt=	new Date(sDt);

					if (surgdate < strDate) {
						this.errors += "Surgery Date cannot be a past date \n";
					}
					if (surgdt == "") {
						this.errors += "Surgery Date is required for a case, please enter a Surgery Date \n";
					}
                
                    if(surgstime == "")
                        this.errors += "Start Time is required for a case, please enter a Surgery Start Time \n";
                
					if ((surgetime != "") && (surgstime > surgetime)) {
						this.errors += "End Time cannot be before Start Time \n";
					}
					if ((account == "") || (account == 0)) {
						this.errors += "Account information is required for a case, please enter an Account \n";
					}
					if (surgnm == 0) {
						this.errors += "Surgeon/NPI# is required for a case, please enter the Surgeon/NPI # \n";
					}
					if (formatsDt < currentDate) {
						this.errors += "Start Time cannot be a past Time \n";
					}
				},


				cancelPage: function () {
					var that = this;
					if (that.checkScreen == 'div-casescheduler-create') {
						var confirmtxt = "This will remove all inventory selected, on all Inventory Type tabs. Do you want to continue?";
						dhtmlx.confirm({
							title: "Confirm",
							ok: "Yes",
							cancel: "No",
							type: "confirm",
							text: confirmtxt,
							callback: function (result) {
								if (result) {
									that.caseModel.set('arrcasesetvo', []);
									that.caseModel.set('arrcasepartvo', []);
									that.caseModel.set('arrcasesharevo', []);
									console.log(that.caseModel.toJSON());
									that.splitGrpdModel(that.caseModel.toJSON(), function (data) {

										$("#div-casescheduler-inventory").html(that.template_CasePlanInventory());
										$(".setListDetails").html(that.template_CaseSetList(data));
										$(".partListDetails").html(that.template_CasePartList(data));
										$("#set-main").html(that.template_CaseSetMain());
										$(".setListDetails").html(that.template_CaseSetList(data));
										$("#part-main").html(that.template_CasePartMain());
										$(".partListDetails").html(that.template_CasePartList(data));
										that.kitTypeSearch();
										that.searchRep();
										that.searchSetBundle();
										that.searchSet();
										that.searchPart();
									});


								}
							}
						});

					} else {

						var confirmtxt = lblNm("msg_case_leaving_confirm");
						dhtmlx.confirm({
							title: lblNm("msg_confirm"),
							ok: lblNm("msg_yes"),
							cancel: lblNm("msg_no"),
							type: "confirm",
							text: confirmtxt,
							callback: function (result) {
								if (result) {
									Backbone.history.loadUrl(Backbone.history.fragment);

								}
							}
						});
					}

				},

//
//				cancelBck: function () {
//					var that = this;
//					var confirmtxt = "Leaving this page will result in the case not being created, are you sure you want to continue?";
//					dhtmlx.confirm({
//						title: "Confirm",
//						ok: "Yes",
//						cancel: "No",
//						type: "confirm",
//						text: confirmtxt,
//						callback: function (result) {
//							if (result) {
//								alert("cancelBck");
//								// Backbone.history.loadUrl(Backbone.history.fragment);
//
//
//							}
//						}
//					});
//
//				},
            //Update the Model 
            updateModel: function (e) {
                var that = this;
                this.selectDate(e);
                var control;
                control = $(e.currentTarget);
                var attribute = control.attr("name");
                var setid = control.attr("setid");
                var tagid = control.attr("tagid");
                var kitid = control.attr("kitid");
                var partnum = control.attr("partnum");
                var partyid = control.attr("partyid");
                var repid = control.attr("repid");
                var npid = control.attr("npid");
                var VO = control.attr("VO");
                var value = control.val();
                
                if ((attribute != undefined) || (value != undefined)) {
                    if (VO == undefined) {
                        this.caseModel.set(attribute, value);
                    } else {
                        if (VO == "arrcasesetvo") {
                            this.updateSetVO(tagid, setid, kitid, attribute, value);
                        }
                        if (VO == "arrcasepartvo") {
                            this.updatePartVO(partnum, kitid, attribute, value);
                        }
                        if (VO == "arrcasepartyvo") {
                            this.updateCasePartyVO(partyid, npid, attribute, value);
                        }
                        if (VO == "arrcasesharevo") {
                            this.updateCaseShareVO(repid, attribute, value);
                        }
                    }
                }

                var caseIdGen = this.caseModel.toJSON();
                if(caseIdGen.surgdt != '' && caseIdGen.starttm != '' && caseIdGen.accid != ''){
                    var surgdt = compDateFmt(caseIdGen.surgdt);
                    var input = {
                        "token": localStorage.getItem("token"),
                        'userid': localStorage.getItem('userID'),
                        'surgdt': surgdt
                    };
                    if((caseIdGen.caseid == '') || (attribute == 'surgdt')){
                        that.fnGenCaseID(input);
                    }
                    
                }
            },
            
            fnGenCaseID: function(input) {
                var that = this;
                    fnGetWebServerData("case/caseidgen", "gmCaseVO", input, function (data) {
                        $("#case-id-gen").val(data.caseid);
                        $("#case-id-gen").trigger("change");
                        that.render();
                    });                    
            },

            
            //Select Date min-max Condition Check
            selectDate: function (e) {
                var target = $(e.currentTarget).attr("id");
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1;
                var yyyy = today.getFullYear();
                if (dd < 10)
                    dd = '0' + dd
                if (mm < 10)
                    mm = '0' + mm

                var hours = today.getHours();
                var minutes = today.getMinutes();
                var currntTime = hours + ':' + minutes;
                today = yyyy + '-' + mm + '-' + dd;

                var startdate = $("#" + this.module + "-frmdt").val();
                var enddate = $("#" + this.module + "-todt").val();

                if (startdate != "" && target == this.module + "-frmdt") {
                    $("#" + this.module + "-todt").val(startdate).trigger("change");
                }

                if (startdate != "" && enddate != "" && enddate < startdate) {
                    $("#" + this.module + "-todt").val(startdate).trigger("change");
                }
            },


            bookCase: function () {
                var that = this;
                var input = this.caseModel.toJSON();
                var confirmtxt = lblNm("msg_book_case_confirm");
                dhtmlx.confirm({
                    title: lblNm("msg_confirm"),
                    ok: lblNm("msg_yes"),
                    cancel: lblNm("msg_no"),
                    type: "confirm",
                    text: confirmtxt,
                    callback: function (result) {
                        if (result) {
                            that.saveCaseWS(input);                            
                        }
                    }
                });
            },
            
            saveCaseWS: function (input) {
                delete input.device;
                delete input.createddt;
                input.userid = localStorage.getItem("userID");
                input.partyid = localStorage.getItem("partyID");
                if(GM.Global.Case.Mode == "create")
                 if (input.surgdt && input.surgdt != "aN/aN/NaN")
                    input.surgdt = getDateByFormat(input.surgdt);
				var tm = input.starttm;
                
                if (input.endtm == '' )
                    input.endtm =moment.utc(tm,'HH:mm:ss').add(2,'hour').format('HH:mm:ss');
				console.log(input.endtm);
                
                if(GM.Global.Case.Mode == "create")
                    input.mailstatus = 1;
                else if(GM.Global.Case.Mode == "edit")
                    input.mailstatus = 2;
                console.log(JSON.stringify(input));
                fnGetWebServerData("casetxn/savecase", undefined, input, function (data) {
                     if (data != null) {
                    var confirmtxt = lblNm("case_id")+ " "+ data.caseid + lblNm("msg_case_booked_success");
					var title = "Case Booked";
					 if(GM.Global.Case.Mode == "edit"){
					  confirmtxt = lblNm("case_id")+ " "+ data.caseid + lblNm("msg_case_saved_success");
					  title = lblNm("msg_case_saved");
					 }
                    dhtmlx.alert({
                    title: title,
                    text: confirmtxt,
                    callback: function (result) {
                        if (result) {
                            window.location.href = "#case/id/" + data.caseinfoid;
                            if(GM.Global.Case.Mode == "edit")
                                Backbone.history.loadUrl(Backbone.history.fragment);                           
                        }
                    }
                });
                        
                    } else {
                        showError("Case Booked Failed");
                    }
                });
            },

             Gmaddset: function (event) {
                $("#addpart").removeClass("selected");
                $("#addset").addClass("selected");
                $(".sets").show();
                $(".parts").hide();
                
                $("#plan-case-additional-set-bundle input").val("");
                $("#plan-case-additional-set-bundle input").trigger("keyup");
                
                $("#plan-case-additional-set input").val("");
                $("#plan-case-additional-set input").trigger("keyup");

            },

            Gmaddpart: function (event) {
                $("#addpart").addClass("selected");
                $("#addset").removeClass("selected");
                $(".sets").hide();
                $(".parts").show();
                
                $("#plan-case-additional-part input").val("");
                $("#plan-case-additional-part input").trigger("keyup");
              
            },

            GmSummaryPlanSetTab: function () {
                $("#summary-addpart").removeClass("selected");
                $("#summary-addset").addClass("selected");
                $(".sets-summary").show();
                $(".parts-summary").hide();
            },

            GmSummaryPlanPartTab: function () {
                $("#summary-addpart").addClass("selected");
                $("#summary-addset").removeClass("selected");
                $(".sets-summary").hide();
                $(".parts-summary").show();
            },

            Gmsetbundle: function (event) {
                $(".barcode-sec").addClass("hide");
                $("#setbundle").addClass("selected");
                $("#setsearch").removeClass("selected");
                $("#plan-case-additional-set-bundle").show();
                $("#plan-case-additional-set").hide();
                
                $("#plan-case-additional-set-bundle input").val("");
                $("#plan-case-additional-set-bundle input").trigger("keyup");
            },
            Gmsetsearch: function (event) {
                $(".barcode-sec").removeClass("hide");
                $("#setbundle").removeClass("selected");
                $("#setsearch").addClass("selected");
                $("#plan-case-additional-set-bundle").hide();
                $("#plan-case-additional-set").show();
                
                $("#plan-case-additional-set input").val("");
                $("#plan-case-additional-set input").trigger("keyup");
            },
            
            /*Quantity keypad Functions*/
            showNumberKeypad: function (event) {
                this.target = $(event.currentTarget);

                var that = this;
                $('#overlay-back').fadeIn(100, function () {
                    that.$("#div-show-num-keypad").html(that.template_numberkeypad());
                    var attrval = $(event.currentTarget).attr("value");

                    if (attrval == "quantity") {
                        $("#div-keypad-title").html(lblNm("quantity") + "</div>");
                        $("#div-keypad-close").show();
                        $("#div-num-decimal").hide();
                        $("#div-num-symbol").css("width", "69px").css("height", "17px").css("margin-left", "0");
                    }
                });
            },

            getKeyValues: function (event) {
                var targetid = event.currentTarget.id;
                var targettext = $("#" + targetid).text();
                if (targetid == "div-num-decimal" && $("#div-qty-entry-text").text() == "") {
                    targettext = "0.";
                }
                if ($("#div-qty-entry-text").text() != "") {
                    if (targetid == "div-num-decimal" && $("#div-qty-entry-text").text().indexOf(".") >= 0) {
                        targettext = "";
                    }
                }
                $("#div-qty-entry-text").append(targettext);
            },

            deleteKeyValues: function (event) {
                var number_value = $("#div-qty-entry-text").text();
                var str = number_value.substring(0, number_value.length - 1);
                $("#div-qty-entry-text").text(str);

            },

            displayQuantity: function (event) {
                var that = this;
                var qty_val, part_num;

                if (($("#div-qty-entry-text").text()) == "") {
                    showMsgView("052");
                }
                if (($("#div-qty-entry-text").text()) != "" && ($("#div-qty-entry-text").text()) == "0") {
                    showMsgView("053");
                }
                if ($("#div-qty-entry-text").text() != "" && $("#div-qty-entry-text").text() != "0") {
                    //				showMsgView("011");
                }
                if (($("#div-qty-entry-text").text() != "") && ($("#div-qty-entry-text").text() != 0)) {
                    qty_val = parseInt($("#div-qty-entry-text").text());
                    $(that.target).text(qty_val);
                    var partnum = $(that.target).closest(".part-set-wrap").attr("data-partnum");
                    var kitid = $(that.target).closest(".part-set-wrap").attr("data-kitid");
                    that.updQtyModel(qty_val, partnum, kitid);
                }
                $("#div-num-keypad").hide();
                $('#overlay-back').fadeOut(100);
            },

            closeKeypad: function (event) {
                $('#overlay-back').fadeOut(100);
                $("#div-num-keypad").hide();
            },

            incQty: function (e) {
                console.log("inside the incQty");
                var that = this;
                var count = parseInt($(e.currentTarget).siblings("#div-inv-qty-box").text());
                var countEl = $(e.currentTarget).siblings("#div-inv-qty-box");
                
                if($(e.currentTarget).hasClass("adSetCasePlus")){
                    var elem = $(e.currentTarget).parent().parent().parent().parent(".part-set-wrap");
                    var setid=elem.data("setid");
                    
                $(e.currentTarget).parent().parent().parent().siblings(".partSetSecRow").find(".partSetTagDtl").append('<div class="pcase-tag-num gmMarginTop5p">Tag #: <input type="text" class="pcase-tag-num-input" data-caseinfoid="" data-tagid=""></div>');
                 $(".pcase-tag-num-input").unbind().bind('change', function (e) {
                        that.tagNumUpdate(e);
                });
                    count++;
                    countEl.text(count); 
                }
                else{
                    count++;
                    countEl.text(count); 
                    var partnum = $(e.currentTarget).closest(".part-set-wrap").attr("data-partnum");
                    var kitid = $(e.currentTarget).closest(".part-set-wrap").attr("data-kitid");
                    that.updQtyModel(count, partnum, kitid);
                }               
                
                
            },

            decQty: function (e) {
                console.log("inside the decQty");
                var that = this;
                var count = parseInt($(e.currentTarget).siblings("#div-inv-qty-box").text());
                var countEl = $(e.currentTarget).siblings("#div-inv-qty-box");

                if ($(e.currentTarget).hasClass("adSetCaseMinus")) {
                    if ($(e.currentTarget).parent().parent().parent().siblings(".partSetSecRow").find(".partSetTagDtl").children(".pcase-tag-num").length > 1) {

                        var setid = $(e.currentTarget).closest(".part-set-wrap").attr("data-setid");
                        var setnm = $(e.currentTarget).closest(".qty-toggle-section").siblings(".pcase-set-part-section").find(".pcase-part-name").text();
                        var tagid = $(e.currentTarget).parent().parent().parent().siblings(".partSetSecRow").find(".partSetTagDtl").find(".pcase-tag-num:last-of-type").find(".pcase-tag-num-input").attr("data-tagid");
                        var caseinfoid = $(e.currentTarget).parent().parent().parent().siblings(".partSetSecRow").find(".partSetTagDtl").find(".pcase-tag-num:last-of-type").find(".pcase-tag-num-input").attr("data-caseinfoid");
                        $(e.currentTarget).parent().parent().parent().siblings(".partSetSecRow").find(".partSetTagDtl").find(".pcase-tag-num:last-of-type").remove();
                        
                        that.removeTag(setid, tagid, caseinfoid);
                        if (count > 1) {
                            count--;
                            countEl.text(count);
                        }
                    }
                } else {
                    var partnum = $(e.currentTarget).closest(".part-set-wrap").attr("data-partnum");
                    var kitid = $(e.currentTarget).closest(".part-set-wrap").attr("data-kitid");
                    if (count > 1) {
                        count--;
                        countEl.text(count);
                        that.updQtyModel(count, partnum, kitid);
                    }
                }


            },
            
            updQtyModel(Qty, partNum, kitid){
                $("#case-hpartqty").val(Qty);
                $("#case-hpartqty").attr("partnum", partNum);
                $("#case-hpartqty").attr("kitid", kitid);
                $("#case-hpartqty").trigger("change");
            },
            
            addSetValuesInc: function(elem){
                var that=this;
                var setid=elem.data("setid");
                var setdesc=elem.data("setdesc");
                
                var newVOItem = {
                            "caseinfoid": "",
                            "setid": setid,
                            "setdesc": setdesc,
                            "kitid": "",
                            "kitnm": "",
                            "tagid": "",
                            "qty": 1,
                            "voidfl": ""
                        };
                that.caseModel.get("arrcasesetvo").push(newVOItem);
                console.log(that.caseModel);
            },

            getCaseAccountName: function () {
                var that = this;
                var caseModel = this.caseModel.toJSON();    
                 var renderOptions = {
    			        "el": that.$("#new-case-search-account-name"),
    			        "ID": "txt-keyword",
    			        "Name": "txt-search-name",
    			        "Class": "txt-keyword-initial",
    			        "minChar": "3",
    			        "autoSearch": true,
    			        "placeholder": lblNm("msg_character_search"),
    			        "fnName": fnGetAccountSearch,
    			        "usage": 1,
    			        "template": 'GmSearchResult',
    			        "template_URL": URL_Common_Template,
    			        "callback": function (aList) {
                            console.log(aList);
    			            $("#new-case-search-account-name input").val(aList.Name);
    			            $("#case-account-id").val(aList.ID);
    			            $("#case-account-name").val(aList.Name);
                            $("#case-account-id").trigger("change");
                            $("#case-account-name").trigger("change");
    			        }
    			    };

    			    var searchview = new SearchView(renderOptions);
                    console.log(JSON.stringify(caseModel));
                    if(caseModel.accid != ''){
                        $("#new-case-search-account-name input").val(caseModel.accnm);
                    }
            },

            //proving search option to NPI
            searchNPI: function (e) {
                var that = this;
                var fnName = "";
                var searchOptions1 = {
                    'el': $("#new-case-surgeon-npi"),
                    'url': URL_WSServer + 'search/party',
                    'inputParameters': {
                        'token': localStorage.getItem('token'),
                        "partytype": "7000"
                    },
                    'keyParameter': 'partynm',
                    'IDParameter': 'partyid',
                    'NameParameter': 'partynm',
                    'resultVO': 'gmPartyBasicVO',
                    'fnName': fnName,
                    'minChar': 3,
                    'callback': function (result) {
                        if (result != null) {
                            that.cbAddSurgeon(result);
                        }
                    }
                };
                var searchView = new GMVSearch(searchOptions1);
                $("#new-case-surgeon-npi input").attr("placeholder", lblNm("msg_character_search"));
            },
            

            //providing search option to name
            searchSurgeon: function (e) {
                var that = this;
                var fnName = "";
                var searchOptions2 = {
                    'el': $("#new-case-surgeon-name"),
                    'url': URL_WSServer + 'search/party',
                    'inputParameters': {
                        'token': localStorage.getItem('token'),
                        "partytype": "7000"
                    },
                    'keyParameter': 'partynm',
                    'IDParameter': 'partyid',
                    'NameParameter': 'partynm',
                    'resultVO': 'gmPartyBasicVO',
                    'fnName': fnName,
                    'minChar': 3,
                    'callback': function (result) {
                        if (result != null) {
                            that.cbAddSurgeon(result);
                        }
                    }
                };
                var searchView = new GMVSearch(searchOptions2);
                $("#new-case-surgeon-name input").attr("placeholder", lblNm("msg_character_search"));
            },
            
            cbAddSurgeon:function(data){
                var that = this;
                
                var surgnpiData = [];
                surgnpiData.push(data);
                
                _.each(surgnpiData, function (item) {
                    $("#case-surgeon-npi-val").val(item.Name);
                    $("#case-surgeon-npi-val").attr("partyid", item.ID);
                    $("#case-surgeon-npi-val").attr("npid", item.npid);
                    $("#case-surgeon-npi-val").trigger("change");
                });
                
                $("#new-case-surgeon-name input").val("");
                $("#new-case-surgeon-npi input").val("");
                this.render();
            },
            
            fnOnChange: function(e){
                if($("#new-case-surgeon-npi").find("#ul-search-resultlist").children("li").hasClass("gmSearchListItem")){
                    $(".btn-add-npi-surgeon-grp").hide();
                }
                else{
                    $(".btn-add-npi-surgeon-grp").show();
                }
            },
            
            removeParty: function(e) {
                $(e.currentTarget).parent(".case-surg-item").remove();
                var that=this;
                var partyid = $(e.currentTarget).parent().attr("data-partyid");
                var caseinfoid = $(e.currentTarget).parent().attr("data-caseinfoid");
                console.log(partyid);
                if (parseNull(caseinfoid) != "") {
                    _.each(this.caseModel.get("arrcasepartyvo"), function (data) {
                        if (data.partyid == partyid)
                            data.voidfl = "Y";
                    });
                } else {
                    var voidSet = _.reject(this.caseModel.get("arrcasepartyvo"), function (data) {
                        return data.partyid == partyid;
                    });

                    this.caseModel.set("arrcasepartyvo", voidSet);                    
                }              
                console.log(this.caseModel.toJSON());
                this.render();
            },
            
            fnAddCaseSurgeons: function(e){
                var that = this;
                that.createNewSurgeon();                
                $(e.currentTarget).parent(".btn-add-npi-surgeon-grp").hide();
            },            
            
            //Create New Surgeon Function
             createNewSurgeon: function() {
                var that = this;
               
                var surgeonInput = {
                    "token": localStorage.getItem("token"),
                    "partyid": "",
                    "npid": $("#new-case-surgeon-npi input").val(),
                    "firstnm": $("#new-case-surgeon-name input").val(),
                    "lastnm": "",
                    "partynm": "",
                    "midinitial": "",
                    "asstnm": "",
                    "mngrnm": "",

                    "arrgmsurgeonsaddressvo": [],
                    "arrgmsurgeoncontactsvo": [
                        {
                            "contype": "Home",
                            "conmode": "90450",
                            "convalue": "",
                            "conid": "",
                            "primaryfl": "Y",
                            "voidfl": ""
                },
                        {
                            "contype": "Home",
                            "conmode": "90452",
                            "convalue": "",
                            "conid": "",
                            "primaryfl": "Y",
                            "voidfl": ""
                }
                ],
                    "arrgmcrmsurgeonaffiliationvo": [],
                    "arrgmcrmsurgeonsurgicalvo": [],
                    "arrgmcrmsurgeonactivityvo": [],
                    "arrgmcrmsurgeonattrvo": [],
                    "arrgmcrmfileuploadvo": []
                };


                fnGetWebServerData("surgeon/save", undefined, surgeonInput, function (data) {
                    if (data != null) {                        
                        showSuccess("Surgeon Detail created successfully");
                        data.Name = data.firstnm + " " + data.lastnm;
                        data.ID = data.partyid;
                        GM.Global.CRNewSurgeon="Y";
                        console.log(data);                        
                        that.cbAddSurgeon(data);
                        
                    } else
                        showError("Save Failed");
                });
            },

                        
            searchRep: function () {
                var that = this;
                var fnName = "";
                var searchOptions = {
                    'el': $("#case-share-name-search"),
                    'url': URL_WSServer + 'cmsearch/party',
                    'inputParameters': {
                        'token': localStorage.getItem('token'),
                        'userid': localStorage.getItem('userID'),
                        'cmpid': localStorage.getItem('cmpid')
                    },
                    'keyParameter': 'srchkey',
                    'IDParameter': 'ID',
                    'NameParameter': 'NAME',
                    'resultVO': undefined,
                    'fnName': fnName,
                    'usage': 2,
                    'minChar': 3,
                    'callback': function (result) {
                        console.log(result);
                        that.setRepDetails(result);
                        $("#case-share-name-search").find("input").val("");
                    }
                };
                 var searchView = new GMVSearch(searchOptions);
                 $("#case-share-name-search").find("input").attr("placeholder",lblNm("search_repnm_share_with"));
                
                
                
            },
            
            setRepDetails: function (data) {
                console.log(data);
                var that = this;
                if (data.length != undefined) {
                    _.each(data, function (item) {
                        if (item.ID != localStorage.getItem("partyID")) {
                            $("#case-hrepnm").val(item.Name);
                            $("#case-hrepnm").attr("repid", item.ID);

                            $("#case-hrepnm").trigger("change");
                        } else
                            displayPopupMsg("You cannot share your own Case");
                    });
                } else {
                    if (data.ID != localStorage.getItem("partyID")) {
                        $("#case-hrepnm").val(data.Name);
                        $("#case-hrepnm").attr("repid", data.ID);
                        $("#case-hrepnm").trigger("change");
                    } else
                        displayPopupMsg("You cannot share your own Case");
                }
                $("#case-share-name-search").find("input").val("");
                that.loadShareTmp();
            },  
            
            loadShareTmp: function(){
                var that=this;
                console.log(that.caseModel.toJSON());
                
                if(document.getElementById("shareCaseContent")){
                   $("#shareCaseContent").html(that.template_CaseShare(that.caseModel.toJSON()));
                 $(".btnBookCase").addClass("hide");
                 $(".btnCancelShr").removeClass("hide");
                 $(".btnSubmitShr").removeClass("hide");
                $(".btnSubmitShr").unbind('click').bind('click', function (e) {
                    that.saveShare(e);
                });
                $(".repDel").unbind('click').bind('click', function (e) {
                    that.removeRep(e);
                });
                   $(".btnCancelShr").click(function () {
                       that.cancelShareChk(function (statusFl) {
                           if (statusFl) {
                               var confirmtxt = "This will remove all reps the case is currently shared with. Do you want to continue?";
                               dhtmlx.confirm({
                                   title: "Confirm",
                                   ok: "Yes",
                                   cancel: "No",
                                   type: "confirm",
                                   text: confirmtxt,
                                   callback: function (result) {
                                       if (result) {
                                           that.hideKitpopup('caseSharepopup');
                                       }
                                   }
                               });
                           } else
                               that.hideKitpopup('caseSharepopup');
                       });

                   });
                }  
                                    
                else{
                    
                    $("#div-casescheduler-share-case").html(this.template_CaseShare(this.caseModel.toJSON()));
                   
                     }
                that.searchRep();
               
                
            },
            
            cancelShareChk: function (cb) {
                var that = this;
                var shareDt = that.caseModel.get("arrcasesharevo");
                var addedReps = shareDt.filter((dt) => {
                    return dt.caseinfoid == "";
                });
                var deletedReps = shareDt.filter((dt) => {
                    return parseNull(dt.voidfl) != "";
                });
                if (addedReps.length > 0 || deletedReps > 0)
                    cb(true);
                else
                    cb(false);
            },
            
            saveShare: function (e) {
                var that = this;
                var repArr = that.caseModel.get('arrcasesharevo');               
                
                var repldArr = $.parseJSON(JSON.stringify(repArr).replace(/repid/g,'partyid').replace(/repnm/g,'partynm').replace(/caseinfoid/g,'kitid'));
                                   
                var input = {
                    "token": localStorage.getItem("token"),
                    "userid": localStorage.getItem("userID"),
                    "kitid": that.caseModel.get("caseinfoid"),
                    "stropt": "CASE",
                    "accid": that.caseModel.get("accid"),
                    "arrrepdtl": repldArr
                }
                
                if (repArr.length != 0) {
                    fnGetWebServerData("kit/sharekit", "gmKitInputsVO", input, function (data) {
                        if (data != null) {
                            $(".caseSharepopup .close").trigger("click");
                            showSuccess(lblNm("msg_case_share_success"));

                            window.location.href = "#case/id/" + data.kitid;
                            Backbone.history.loadUrl(Backbone.history.fragment);
                        } else
                            displayPopupMsg("Case Shared Failed");
                    });
                }
            },
            
            //Remove Rep after click a Del icon
            removeRep: function (event) {
                var that=this;
                var caseinfoid = $(event.currentTarget).parent().attr("data-caseinfoid");
                var repid = $(event.currentTarget).parent().attr("id");
                if (parseNull(caseinfoid) != "") {
                    _.each(this.caseModel.get("arrcasesharevo"), function (data) {
                        if (data.repid == repid)
                            data.voidfl = "Y";
                    });
                } else {
                    var voidRep = _.reject(this.caseModel.get("arrcasesharevo"), function (data) {
                        return data.repid == repid;
                    });

                    this.caseModel.set("arrcasesharevo", voidRep);
                    
                } 
                 that.loadShareTmp();
            },

            kitTypeSearch: function () {
                var that = this;
                var fnName = "";
                var searchOptions = {
                    'el': $("#div-plan-case-kit-search"),
                    'url': URL_WSServer + 'cmsearch/kitname',
                    'inputParameters': {
                        'token': localStorage.getItem('token'),
                        'userid': localStorage.getItem('userID'),
                        'cmpid': localStorage.getItem('cmpid'),
                        'partyid': localStorage.getItem('partyID'),
						'acid': localStorage.getItem('AcID')
                    },
                    'keyParameter': 'srchkey',
                    'IDParameter': 'ID',
                    'NameParameter': 'NAME',
                    'resultVO': undefined,
                    'fnName': fnName,
                    'usage': 1,
                    'minChar': 3,
                    'callback': function (result) {
                        console.log(result);
                        that.setKitDetails(result);
                        $("#div-plan-case-kit-search").find("input").val("");
                    }
                };
                var searchView = new GMVSearch(searchOptions);
                $("#div-plan-case-kit-search").find("input").attr('placeholder', $('<div />').html('&#xF002;' + lblNm("msg_character_search")).text());
            },
            
            setKitDetails: function(data){
                var that = this;
                var input = {
                        'token': localStorage.getItem('token'),
                        'cmpid': localStorage.getItem('cmpid'),
                        'partyid': localStorage.getItem('partyID'),
                        'kitid': data.ID
                    }
                fnGetWebServerData("kit/fetchkit", undefined, input, function (data) {
                    var setPartArr = [];
                    setPartArr.push(data);
                    _.each(setPartArr, function(dt){
                        that.kitNameVal = dt.kitnm;
                        if(dt.arrkitsetvo){
                            arrSetData = _.each(dt.arrkitsetvo, function(item){
                                item.ID = parseNull(item.tagid) + "-" + item.setid;
                                item.Name = parseNull(item.tagid) + "-" + item.setid + "-" + item.setdesc;
                                item.kitid = item.kitid;
                                item.kitnm = that.kitNameVal;
                                
                                
                                
                            });
							that.setSetDetails(arrSetData);
							if($(".kitSetDetails").next(".partSetDetailInnerContent").hasClass("hide")){
                                    $(".kitSetDetails").trigger("click");
                                    /*$(".setListDetails li:nth-child(2) div").addClass(".divsetpartheig");
                                    $(".setListDetails li:nth-child(2) div").addClass(".divsetparthide");*/
                                    
                                    
                                }
                        }
                        if(dt.arrkitpartvo){
                            arrPartData = _.each(dt.arrkitpartvo, function(item){
                                item.ID = item.partnum;
                                item.Name = item.partdesc;
                                item.kitid = item.kitid;
                                item.kitnm = that.kitNameVal;
                                that.setPartDetails(item);
                                if($(".kitPartDetails").next(".partSetDetailInnerContent").hasClass("hide")){
                                    $(".kitPartDetails").trigger("click");
                                }
                            });
                        }
                        
                    });
                });
            },            
            
            getcdLkupVal: function (renderDiv, hiddenDiv, codeGrp, defaultData) {
                var that = this;
                if (defaultData != ''||null) {
                    var defaultid = defaultData;
                } else {
                    var defaultid = 0;
                }
                var input = {
                    "token": localStorage.getItem("token"),
                    "codegrp": codeGrp
                };
                fnGetWebServerData("search/codelookup", "gmCodeLookUpVO", input, function (data) {                    
                    data=$.parseJSON(JSON.stringify(data).replace(/codeid/g, 'ID').replace(/codenm/g, 'Name'));
                    
                    data.push({
                        "ID": 0,
                        "Name": lblNm("msg_choose_one")
                    });
                    that.dropdownview = new DropDownView({
                        el: that.$(renderDiv),
                        data: data,
                        DefaultID: defaultid
                    });

                    that.$(renderDiv).bind('onchange', function (e, selectedId, SelectedName) {
                        $(hiddenDiv).val(selectedId);
                        $(hiddenDiv).trigger("change");
                    });
                });
            },
            

            updateSetVO: function (tagid, setid, kitid, attribute, value) {

                var that = this;
                var setVO = [];
                var setVOItem;
            
                    setVOItem = _.findWhere(this.caseModel.get("arrcasesetvo"), {
                        "setid": setid, "tagid": tagid , "kitid": kitid
                    });
                
                var obj = _.findWhere(this.caseModel.get("arrcasesetvo"), {
                    "setid": setid, "tagid": tagid, "kitid": kitid,
                    "voidfl": "Y"
                });
                
                if (!setVOItem && !obj) {
                   if(that.newTagCheck == 'newTagId'){
                      setVOItem = {
                            "setid": setid,
                            "setdesc": "",
                            "kitid": "",
                            "kitnm": "",
                            "tagid": "",
                            "qty": "",
                            "voidfl": ""
                        }; 
                   }
                    else{
                        setVOItem = {
                            "caseinfoid": "",
                            "setid": setid,
                            "setdesc": "",
                            "kitid": kitid,
                            "kitnm": "",
                            "tagid": tagid,
                            "qty": "",
                            "voidfl": ""
                        };
                    }
                } else if (obj) {
                        _.each(this.caseModel.get("arrcasesetvo"), function (a) {
                            if ((a.setid == setid) && (a.tagid == tagid) && (a.kitid == kitid))
                                a.voidfl = "";
                        });
                }

                setVO = _.without(this.caseModel.get("arrcasesetvo"), setVOItem);
                setVOItem[attribute] = value;
                setVO.push(setVOItem);
                
                this.caseModel.get("arrcasesetvo").length = 0;
                setVO = arrayOf(setVO);
                $.each(setVO, function (index, item) {
                    that.caseModel.get("arrcasesetvo").push(item);
                });
                
            },
            
            
            updateCasePartyVO : function(partyid, npid, attribute, value){
                var that = this;
                var partyVO = [];
                var partyVOItem;

                partyVOItem = _.findWhere(this.caseModel.get("arrcasepartyvo"), {
                    "partyid": partyid, "npiid": npid
                });


                var obj = _.findWhere(this.caseModel.get("arrcasepartyvo"), {
                    "partyid": partyid, "npiid": npid,
                    "voidfl": "Y"
                });

                if (!partyVOItem && !obj) {
                    partyVOItem = {
                        "caseinfoid": "",
                        "partyid": partyid,
                        "partynm": "",
                        "npiid": npid,
                        "voidfl": ""
                    };
                } else if (obj) {
                    _.each(this.caseModel.get("arrcasepartyvo"), function (a) {
                        if ((a.partyid == partyid) && (a.npiid == npid))
                            a.voidfl = "";
                    })
                }

                partyVO = _.without(this.caseModel.get("arrcasepartyvo"), partyVOItem);
                partyVOItem[attribute] = value;
                partyVO.push(partyVOItem);
                
                this.caseModel.get("arrcasepartyvo").length = 0;
                partyVO = arrayOf(partyVO);
                $.each(partyVO, function (index, item) {
                    that.caseModel.get("arrcasepartyvo").push(item);
                });
            },
            
            updateCaseShareVO : function(repid, attribute, value){
                var that = this;
                var repVO = [];
                var repVOItem;

                repVOItem = _.findWhere(this.caseModel.get("arrcasesharevo"), {
                    "repid": repid
                });
                
                var obj = _.findWhere(this.caseModel.get("arrcasesharevo"), {
                    "repid": repid, "voidfl": "Y"
                });

                if (!repVOItem && !obj) {
                    repVOItem = {
                        "caseinfoid": "",
                        "repid": repid,
                        "repnm": "",                       
                        "voidfl": ""
                    };
                } else if (obj) {
                    _.each(this.caseModel.get("arrcasesharevo"), function (a) {
                        if (a.repid == repid)
                            a.voidfl = "";
                    })
                }

                repVO = _.without(this.caseModel.get("arrcasesharevo"), repVOItem);
                repVOItem[attribute] = value;
                repVO.push(repVOItem);
                
                this.caseModel.get("arrcasesharevo").length = 0;
                repVO = arrayOf(repVO);
                $.each(repVO, function (index, item) {
                    that.caseModel.get("arrcasesharevo").push(item);
                });
            },
            
            
            updatePartVO: function (partnum, kitid, attribute, value) {
                var that = this;
                var partVO = [];
                var partVOItem;

                partVOItem = _.findWhere(that.caseModel.get("arrcasepartvo"), {
                    "partnum": partnum, "kitid": kitid
                });


                var obj = _.findWhere(that.caseModel.get("arrcasepartvo"), {
                    "partnum": partnum, "kitid": kitid, "voidfl": "Y"
                });
                if (!partVOItem && !obj) {
                    partVOItem = {
                        "caseinfoid": "",
                        "partnum": partnum,
                        "partdesc": "",
                        "qty": "",
                        "kitid": kitid,
                        "kitnm": "",
                        "voidfl": ""
                    };
                } else if (obj) {
                    _.each(this.caseModel.get("arrcasepartvo"), function (a) {
                        if ((a.partnum == partnum)&&(a.kitid == kitid))
                            a.voidfl = "";
                    });
                }
                partVO = _.without(that.caseModel.get("arrcasepartvo"), partVOItem);
                partVOItem[attribute] = value;
                partVO.push(partVOItem);
                
                that.caseModel.get("arrcasepartvo").length = 0;
                partVO = arrayOf(partVO);
                _.each(partVO, function (item) {
                    that.caseModel.get("arrcasepartvo").push(item);
                });
            },
            
            searchSetBundle: function () {
                var that = this;
                var fnName = "";
                var searchOptions = {
                    'el': $("#plan-case-additional-set-bundle"),
                    'url': URL_WSServer + 'cmsearch/setbundle',
                    'inputParameters': {
                        'token': localStorage.getItem('token'),
                        'userid': localStorage.getItem('userID'),
                        'cmpid': localStorage.getItem('cmpid'),
                        'partyid': localStorage.getItem('partyID'),
						'acid': localStorage.getItem('AcID')
                    },
                    'keyParameter': 'srchkey',
                    'IDParameter': 'ID',
                    'NameParameter': 'NAME',
                    'resultVO': undefined,
                    'fnName': fnName,
                    'usage': 1,
                    'minChar': 3,
                    'callback': function (result) {
                        console.log(result);
                        var input = {
                            'token': localStorage.getItem('token'),
                            'cmpid': localStorage.getItem('cmpid'),
                            'sbid': result.ID
                        }
                        fnGetWebServerData("case/fetchsetbn", undefined, input, function (data) {
                            console.log(data);
                            _.each(data, function(item){
                                 item.ID = "" + "-" + item.SETID;
                                 item.Name = "" + "-" + item.SETID + "-" + item.SETNAME;
                                 item.kitid = '';
                                 item.kitnm = '';
                                 that.setSetDetails(item);
                                $("#plan-case-additional-set-bundle").find("input").val("");
                                if($(".adtnlSetDetails").next(".partSetDetailInnerContent").hasClass("hide")){
                                    $(".adtnlSetDetails").trigger("click");
                                }
                            });
                        });
                    }
                };
                var searchView = new GMVSearch(searchOptions);
                $("#plan-case-additional-set-bundle").find("input").attr('placeholder', $('<div />').html('&#xF002;  '+ lblNm("search_bundle")).text());
            },
            
            searchSet: function () {
                var that = this;
                var fnName = "";
                var searchOptions = {
                    'el': $("#plan-case-additional-set"),
                    'url': URL_WSServer + 'cmsearch/setbasedcmp',
                    'inputParameters': {
                        'token': localStorage.getItem('token'),
                        'userid': localStorage.getItem('userID'),
                        'cmpid': localStorage.getItem('cmpid'),
                        'partyid': localStorage.getItem('partyID'),
						'acid': localStorage.getItem('AcID')
                    },
                    'keyParameter': 'srchkey',
                    'IDParameter': 'ID',
                    'NameParameter': 'NAME',
                    'resultVO': undefined,
                    'fnName': fnName,
                    'usage': 2,
                    'minChar': 3,
                    'callback': function (result) {
                        console.log(result);
                        that.setSetDetails(result);
                        $("#plan-case-additional-set").find("input").val("");
                        if($(".adtnlSetDetails").next(".partSetDetailInnerContent").hasClass("hide")){
                            $(".adtnlSetDetails").trigger("click");
                        }
                    }
                };
                var searchView = new GMVSearch(searchOptions);
                $("#plan-case-additional-set").find("input").attr('placeholder', $('<div />').html('&#xF002; '+ lblNm("search_set")).text());
            },
            
             setSetDetails: function (setData) {
                var that = this;
                var setObj = [];
                if (setData.length != undefined) {
                    var i = 0;
                    _.each(setData, function (item) {
                        setObj[i] = {};
                        var setArr = item.ID.split("-");
                        var setNM = item.Name;
                        setObj[i].tagid = parseNull(setArr[0]);
                        setObj[i].newtagid = setArr[0];
                        setObj[i].setid = setArr[1];
                        setObj[i].kitid = item.kitid;
                        setObj[i].kitnm = item.kitnm;
                        setObj[i].qty   = 1;
                        var tempsetdesc = setNM.substring(setNM.indexOf('-') + 1);
                        setObj[i].setdesc = tempsetdesc.substring(tempsetdesc.indexOf('-') + 1);
                        i++;
                    });
                } else {
                    setObj[0] = {};
                    var setArr = setData.ID.split("-");
                    var setNM = setData.Name;
                    setObj[0].tagid = parseNull(setArr[0]);
                    setObj[0].newtagid = setArr[0];                    
                    setObj[0].setid = setArr[1];
                    setObj[0].kitid = setData.kitid;
                    setObj[0].kitnm = setData.kitnm;
                    setObj[0].qty   = 1;
                    var tempsetdesc = setNM.substring(setNM.indexOf('-') + 1);
                    setObj[0].setdesc = tempsetdesc.substring(tempsetdesc.indexOf('-') + 1);
                }
                that.setSetValues(setObj);                 
                    that.splitGrpdModel(that.caseModel.toJSON(),function(data){
                $(".partListDetails").html(that.template_CasePartList(data));
                $(".setListDetails").html(that.template_CaseSetList(data));
                 that.togglePartSetDtl();
                    });
            },

            setSetValues: function (data) {
                var that=this;
                _.each(data, function (item) {
                    if(that.newTagCheck == 'newTagId')
                        var tagid=item.tagid;
                    else
                        var tagid=item.newtagid;
                    $("#case-hsetdesc").val(item.setdesc);
                    $("#case-hsetdesc").attr("setid", item.setid);
                    $("#case-hsetdesc").attr("tagid", tagid);
                    $("#case-hsetdesc").attr("kitid", item.kitid);
                    $("#case-hsetkitnm").val(item.kitnm);
                    $("#case-hsetkitnm").attr("setid", item.setid);
                    $("#case-hsetkitnm").attr("tagid", tagid);
                    $("#case-hsetkitnm").attr("kitid", item.kitid);
                    $("#case-hsetqty").val(item.qty);
                    $("#case-hsetqty").attr("setid", item.setid);
                    $("#case-hsetqty").attr("tagid", tagid);
                    $("#case-hsetqty").attr("kitid", item.kitid);   
                    if(that.newTagCheck != 'newTagId') {
                        $("#case-hsetdesc").trigger("change");
                    $("#case-hsetkitnm").trigger("change");
                    $("#case-hsetqty").trigger("change");
                    }
                    else if(that.newTagCheck == 'newTagId'){
                        $("#case-hsettagid").val(item.newtagid);
                        $("#case-hsettagid").attr("setid", item.setid);
                        $("#case-hsettagid").attr("tagid", tagid);
                        $("#case-hsettagid").attr("kitid", item.kitid);
                        $("#case-hsettagid").trigger("change");
                    }
                });
            },
            
            //Remove Part after click a Del icon
            removeSet: function (event) {
                var that=this;
                if($(event.currentTarget).hasClass('delete-pu-span'))
                var elem = $(event.currentTarget).parent(".part-set-wrap");
                else
                var elem = $(event.currentTarget).parent().parent().parent(".part-set-wrap");
                var caseinfoid = elem.attr("data-caseinfoid");
                var kitid = elem.attr("data-kitid");
                var tagid = elem.attr("id");
                var setid = elem.attr("data-setid");
                if (parseNull(caseinfoid) != "") {
                    _.each(this.caseModel.get("arrcasesetvo"), function (data) {
//                        if ((data.setid == setid) && (parseNull(data.tagid) == tagid) && (parseNull(data.kitid) == kitid))
                        if ((data.setid == setid) && (parseNull(data.kitid) == kitid))
                            data.voidfl = "Y";
                    });
                } else {
                    var voidSet = _.reject(this.caseModel.get("arrcasesetvo"), function (data) {
//                        return data.setid == setid && parseNull(data.tagid) == tagid && parseNull(data.kitid) == kitid;
                        return data.setid == setid && parseNull(data.kitid) == kitid;
                    });
                    this.caseModel.set("arrcasesetvo", voidSet);
                    
                }              
                    that.splitGrpdModel(that.caseModel.toJSON(),function(data){
                        $(".setListDetails").html(that.template_CaseSetList(data));                         
                        $(".partListDetails").html(that.template_CasePartList(data));
                        
                that.togglePartSetDtl();
                        if(kitid !="")
                            $(".kitSetDetails").trigger("click");
                        else
                            $(".adtnlSetDetails").trigger("click");
                    });
            },
            
            removeTag: function(setid, tagid, caseinfoid){
                if (parseNull(caseinfoid) != "") {
                    _.each(this.caseModel.get("arrcasesetvo"), function (data) {
                        if ((data.setid == setid) && (parseNull(data.tagid) == tagid) && (parseNull(data.kitid) == ""))
                            data.voidfl = "Y";
                    });
                } else {
                    var voidSet = _.reject(this.caseModel.get("arrcasesetvo"), function (data) {
                        return data.setid == setid && parseNull(data.tagid) == tagid && parseNull(data.kitid) == "";
                    });
                    this.caseModel.set("arrcasesetvo", voidSet);
                    
                }              
            },
            
                //Remove Part after click a Del icon
            removePart: function (event) {
                var that=this;
                if($(event.currentTarget).hasClass('delete-pu-span'))
                var elem = $(event.currentTarget).parent(".part-set-wrap");
                else
                var elem = $(event.currentTarget).parent().parent().parent(".part-set-wrap");
                var caseinfoid = elem.attr("data-caseinfoid");
                var kitid = elem.attr("data-kitid");
                var partnum = elem.attr("data-partnum");
                if (parseNull(caseinfoid) != "") {
                    _.each(this.caseModel.get("arrcasepartvo"), function (data) {
                        if ((data.partnum == partnum) && (parseNull(data.kitid) == kitid))
                            data.voidfl = "Y";
                    });
                } else {
                    var voidSet = _.reject(this.caseModel.get("arrcasepartvo"), function (data) {
                        return data.partnum == partnum && parseNull(data.kitid) == kitid;
                    });
                    this.caseModel.set("arrcasepartvo", voidSet);
                    
                }              
                that.splitGrpdModel(that.caseModel.toJSON(),function(data){
                $(".setListDetails").html(that.template_CaseSetList(data)); 
                $(".partListDetails").html(that.template_CasePartList(data));
                    
                that.togglePartSetDtl();
                    if(kitid !="")
                            $(".kitPartDetails").trigger("click");
                        else
                            $(".adtnlPartDetails").trigger("click");
                    
                });
                
            },
            
            searchPart: function () {
                var that = this;
                var fnName = "";
                var searchOptions = {
                    'el': $("#plan-case-additional-part"),
                    'url': URL_WSServer + 'cmsearch/partlist',
                    'inputParameters': {
                        'token': localStorage.getItem('token'),
                        'userid': localStorage.getItem('userID'),
                        'cmpid': localStorage.getItem('cmpid')
                    },
                    'keyParameter': 'srchkey',
                    'IDParameter': 'ID',
                    'NameParameter': 'NAME',
                    'resultVO': undefined,
                    'fnName': fnName,
                    'usage': 2,
                    'minChar': 3,
                    'callback': function (result) {
                        console.log(result);
                        that.setPartDetails(result);
                        $("#plan-case-additional-part").find("input").val("");
                        if($(".adtnlPartDetails").next(".partSetDetailInnerContent").hasClass("hide")){
                            $(".adtnlPartDetails").trigger("click");
                        }
                    }
                };
                var searchView = new GMVSearch(searchOptions);
                $("#plan-case-additional-part").find("input").attr('placeholder', $('<div />').html('&#xF002; '+lblNm("search_partnum")).text());
            },
            
            
            setPartDetails: function (partData) {
                var that = this;
                var partObj = [];
                if (partData.length != undefined) {
                    var i=0;
                    _.each(partData, function (item) {
                        var partdesc=item.Name;
                        partObj[i] = {};
                        partObj[i].partnum = item.ID;
                        partObj[i].kitid = parseNull(item.kitid);
                        partObj[i].kitnm = parseNull(item.kitnm);
                        partObj[i].qty = 1;
                        partObj[i].partdesc = partdesc.substring(partdesc.indexOf('-') + 2);
                        i++;
                    });
                } else {
                        partObj[0] = {};
                        var partdesc=partData.Name;
                        partObj[0].partnum = partData.ID;
                        partObj[0].kitid = parseNull(partData.kitid);
                        partObj[0].kitnm = parseNull(partData.kitnm);
                        partObj[0].qty = 1;
                        partObj[0].partdesc = partdesc.substring(partdesc.indexOf('-') + 2);
                }
                that.setPartValues(partObj);
                
            },
            
            setPartValues: function (data) {
                var that=this;
                console.log(data);
                _.each(data, function (item) {
                    console.log(item);
                    $("#case-hpartdesc").val(item.partdesc); 
                    $("#case-hpartdesc").attr("partnum", item.partnum);
                    $("#case-hpartdesc").attr("kitid", item.kitid);
                    
                    $("#case-hpartqty").val(item.qty); 
                    $("#case-hpartqty").attr("partnum", item.partnum);
                    $("#case-hpartqty").attr("kitid", item.kitid);
                    
                    $("#case-hpartkitnm").val(item.kitnm); 
                    $("#case-hpartkitnm").attr("partnum", item.partnum);
                    $("#case-hpartkitnm").attr("kitid", item.kitid);
                    
                    $("#case-hpartdesc").trigger("change");
                    $("#case-hpartqty").trigger("change");
                    $("#case-hpartkitnm").trigger("change");
                });
                that.splitGrpdModel(that.caseModel.toJSON(),function(data){
                $(".setListDetails").html(that.template_CaseSetList(data));
                $(".partListDetails").html(that.template_CasePartList(data));
                 that.togglePartSetDtl();
                });
                
            },
            
            
            partlistPopup: function(e){
                var that=this;
                console.log(e);
                var setID=$(e.currentTarget).data("setid");
                var input = {
                        "token": localStorage.getItem("token"),
                        "setid": setID
                    };                
                 that.showKitpopup("setPartList", "Part Details", "gridPartDetails");  
                getLoader("#gridPartDetails");
                fnGetWebServerData("kit/fetchpart", undefined, input, function (data) {
                    if(data !=null){
                    console.log(data);
                    var rows = [], i = 0;
                            _.each(data, function (griddt) {                             
                                rows[i] = {};
                                rows[i].id = i;
                                rows[i].data = [];
                                rows[i].data[0] = griddt.partnum+" "+griddt.partdesc;
                                rows[i].data[1] = griddt.qty;
                                i++;                        
                            });
                var partData = {};
                partData.rows = rows;
                var setInitWidths = "*,150";
                var setColAlign = "left,left";
                var setColTypes = "ro,ro";
                var setColSorting = "str,str";
                var setHeader = ["Parts", "Qty"];
                var enableTooltips = "true,true";
                var footerArry = [];
                var gridHeight = "400";
                var setFilter = "";
                var footerStyles = [];
                var footerExportFL = false;
                var mobResArg=true;
                        
                dhxReportDataFmt(rows,setHeader, function(dt){
                that.rptTemplate = that.template_DhxReportMobile(dt);
                });
        
                var gridData = loadDHTMLXGrid('gridPartDetails', gridHeight, partData, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL,that.rptTemplate,mobResArg);   
                        for (var i = 0; i < gridData.getRowsNum(); i++) {
                         gridData.cells(i, 0).cell.className = 'gridPartDesc';
                        }
                    }
                    else{
                      $("#gridPartDetails").html("No Data Found");  
                    }
                });
            
                
            },
            
            showKitpopup:function(containerDiv, titleText, contentID){
                var that=this;
                 showPopup();
                $("#div-crm-overlay-content-container").addClass(containerDiv);
                $("#div-crm-overlay-content-container").find(".modal-footer").addClass("hide");
                $("#div-crm-overlay-content-container").find('.modal-title').text(titleText);
                
                $("#div-crm-overlay-content-container .modal-body").html("<div id='"+contentID+"'></div>");
                $("."+containerDiv+" .close").unbind('click').bind('click', function (e) {
                        that.hideKitpopup(containerDiv);
                 });
                getLoader("#"+contentID);
            },
            
            hideKitpopup: function(containerDiv){
				var that=this;	
                hidePopup();
                $("#div-crm-overlay-content-container").removeClass(containerDiv);
                $("#div-crm-overlay-content-container").find(".modal-footer").removeClass("hide");
                $("#div-crm-overlay-content-container").find(".modal-body").html("");
				$("#div-crm-overlay-content-container").find("#div-crm-popup-msg").html("");
				$("#div-crm-overlay-content-container").find("#div-crm-popup-msg").hide();
				if(containerDiv=='caseSharepopup'){
                                Backbone.history.loadUrl(Backbone.history.fragment);
                }
            },
            
            shareCasePopup: function(){
                var that=this;
                that.showKitpopup("caseSharepopup", "Case Sharing", "shareCaseContent");
                $(".caseMain .newcase-footer-btns-section #bnt-prev-case").addClass("btnAlignLeft");
                that.loadShareTmp();
            },
            
            cancelCase: function () {
                var that = this;
                var input = {
                    "token": localStorage.getItem("token"),
                    "userid": localStorage.getItem("userID"),
                    "caseinfoid": that.caseModel.get("caseinfoid"),
                    "mailstatus": 3
                };
                console.log(input);
                var confirmtxt = lblNm("msg_cancel_case_confirm");
                dhtmlx.confirm({
                    title: "Confirm",
                    ok: "Yes",
                    cancel: "No",
                    type: "confirm",
                    text: confirmtxt,
                    callback: function (result) {
                        if (result) {
                            fnGetWebServerData("casetxn/cancelcase", undefined, input, function (data) {
                                console.log(data);
                                if (data != null) {
                                    showSuccess(lblNm("msg_case_cancel_success"));
                                    Backbone.history.loadUrl(Backbone.history.fragment);
                                } else {
                                    showError("Case Cancelled Failed!");
                                }
                            });
                        }
                    }
                });

            },
            
            tagNumUpdate: function(e){
                console.log(e);
                var that = this;
                this.newTagid = $(e.currentTarget).val();
                this.oldTagid = $(e.currentTarget).data("tagid");
                this.setDesc = $(e.currentTarget).closest(".part-set-wrap").find(".pcase-part-name").text();
                var setid= $(e.currentTarget).closest(".part-set-wrap").attr("data-setid");
                
                
                 function pad(num, size) {
                            var s = num + "";
                            while (s.length < size) s = "0" + s;
                            return s;
                        }
                that.newTagid = pad(that.newTagid, 7);
                if(!_.findWhere(that.caseModel.get("arrcasesetvo"), {"setid": setid , "tagid": that.newTagid})){
                var input = {
                    "token": localStorage.getItem("token"),
                    "tagid": that.newTagid,
                    "setid": setid
                }
                
                fnGetWebServerData("case/validatetag", undefined, input, function (data) {
                    console.log(data);
                    if(data.tagfl == 'N'){
                        showError('Tag # is not associated to Set <'+ data.setid +'>');
                    }
                    else if(data.tagfl == 'Y'){
                        $(e.currentTarget).val(that.newTagid);
                        $(e.currentTarget).data("tagid",that.newTagid);
                        var caseinfoid=$(e.currentTarget).attr("data-caseinfoid");
                        if(that.oldTagid!=""){
                          that.removeTag(data.setid, that.oldTagid, caseinfoid);  
                        }
                        var tagObj = [];
                        tagObj[0]={};
                        tagObj[0].setid = data.setid;
                        tagObj[0].tagid = that.oldTagid;
                        tagObj[0].newtagid = data.tagid;
                        tagObj[0].setdesc = that.setDesc;
                        tagObj[0].kitid = '';
                        tagObj[0].kitnm = '';
                        tagObj[0].qty = 1;
                        that.newTagCheck = 'newTagId';
                        that.setSetValues(tagObj);
                    }    
                });
                }
                else
                    showError('Tag # is Already associated to Set <'+ setid +'>');
                
            },
            
            openLink: function () {
                $("#btn-case-link").each(function () {
                    $(this).toggleClass("show-app");
                })
                $(".btn-app-link").fadeToggle("fast");
            },
            
            editCase: function(){
              var that=this;
                GM.Global.Case.Mode="edit";
                that.render();
            },
            
            // On clicking the Barcode image Scan the tag
            getBarcode: function (event) {
                var that = this;
                var partyID = localStorage.getItem("partyID");
                var userID = localStorage.getItem("userID");
                var input = {};
                
                cordova.plugins.barcodeScanner.scan(

                    function (result) {
                        var setid = result.text;
                        $("#plan-case-additional-set").find("input").val(setid);

                        input.userid = userID;
                        input.partyid = partyID;
                        input.tagid = setid;

                        fnGetWebServerData('kit/validateTag', undefined, input, function (data) {
                            console.log(data);
                            if (data.tagidVaidate == "Y")
                                showError("Cannot add a loaner tag to a case");
                            else
                                $("#plan-case-additional-set").find("input").trigger("keyup");
                        });

                    },
                    function (error) {
                        console.log("Scanning failed: " + error);
                    }
                );
            },
            

        });
        return GM.View.Case;
    });