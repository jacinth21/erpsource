/**********************************************************************************
 * File:        gmrCaseScheduler.js
 * Description: Router for all of RedisPOC
 * Version:     1.0
 * Author:      mahendrand
 **********************************************************************************/
define([

  // External Libraries
  'jquery', 'underscore', 'backbone',


  //Views
   'gmvCase', 'gmvCaseDash', 'gmvCaseCalendar'
],

    function ($, _, Backbone,
        GMVCase, GMVCaseDash, GMVCaseCalendar) {

        'use strict';

        GM.Router.CaseScheduler = Backbone.Router.extend({

            //routes to its respective web page 
            routes: {
                "case": "caseDashboard",
                "case/id/:caseid": "caseDetail", 
				"case/report": "caseCalendr",
            },


            initialize: function () {
                $("#div-app-title").show();
            },
            
            caseDashboard: function(){
                var gmvCaseDash = new GMVCaseDash();
                            gmvApp.showView(gmvCaseDash);
				 $("#btn-back").show();
            },
			
			caseCalendr: function() {
				var gmvCaseCalendar = new GMVCaseCalendar();
				 gmvApp.showView(gmvCaseCalendar);
				 $("#btn-back").show();
				
			},


            caseDetail: function (caseID) {
                if (caseID != 0) {
                    console.log(caseID);
                    var input = {
                        "token": localStorage.getItem("token"),
                        "partyid": localStorage.getItem("partyID"),
                        "caseinfoid": caseID
                    };
                    fnGetWebServerData("case/fetchcase", undefined, input, function (data) {
                        if (data != null) {
                            console.log("Kit Data" + JSON.stringify(data));
                            var gmvCase = new GMVCase({
                                "caseinfoid": caseID,
                                "data": data
                            });
                            gmvApp.showView(gmvCase);
                        } else {
                            GM.Global.AlertMsg = "No Such Case";
                            window.location.href = "#case";
                        }
                         $("#btn-back").show();
                    }, true);
                } else {
                    console.log("CREATE Case")
                    var input = {
                        "token": localStorage.getItem("token"),
                        'userid': localStorage.getItem('userID')
                    };
                    if (GM.Global.AccList == undefined) {
                        fnGetWebServerData("case/account", undefined, input, function (acclist) {
                            GM.Global.AccList = _.uniq(acclist,function(item){
                                return item.ID;
                            });
                            GM.Global.AccList = _.each(GM.Global.AccList,function(dt){
                                dt.Name = dt.ID + " " + dt.Name;
                            });
                            var gmvCase = new GMVCase({
                                "caseinfoid": 0,
                                data: {}
                            });
                            gmvApp.showView(gmvCase);
                        });
                    } else {
                        var gmvCase = new GMVCase({
                            "caseinfoid": 0,
                            data: {}
                        });
                        gmvApp.showView(gmvCase);
                    }

                }
                 $("#btn-back").show();
            },




        });
        return GM.Router.CaseScheduler;
    });