define([
    'underscore',
    'backbone',
],

    function (_, Backbone) {

        'use strict';

        GM.Model.case = Backbone.Model.extend({

            // Default values of the Activity model
            defaults: {
                "caseid": "",            
                "caseinfoid": "",  
                "voidfl": "",            
                "partyid": "",
                "userid": "", 
                "surgdt": "",
                "starttm": "",
                "endtm": "",
                "accid":"",
                "accnm":"",
                "surglvl":"",
                "surglvlid":"",
                "surgtype":"",
                "surgtypeid":"",
                "status":"",
                "notes":"",
                "mailstatus":"",
                "createddt":"",
                "createdby":"",
                "updateddt":"",
                "updatedby":"",                
                "sharenm":"",                
                "arrcasesetvo": [], //setid, tagid, voidfl
                "arrcasepartvo": [], //partid, qty, voidfl
                "arrcasepartyvo": [],
                "arrcasesharevo": []
            },

            initialize: function (data) {
                this.set("arrcasesetvo", new Array());
                this.set("arrcasepartvo", new Array());
                this.set("arrcasepartyvo", new Array());
                this.set("arrcasesharevo", new Array());
            },


        });

        return GM.Model.case;

    });
