define([
    'underscore',
    'backbone',
],

    function (_, Backbone) {

        'use strict';

        GM.Model.kit = Backbone.Model.extend({

            // Default values of the Activity model
            defaults: {
                "kitid": "",
                "kitnm": "",
                "activefl": "",
                "voidfl": "",
                "kittype": "",  
                "partyid": "",
                "userid": "",                
                "arrkitsetvo": [], //setid, tagid, voidfl
                "arrkitpartvo": [], //partid, qty, voidfl
                "arrrepdtl": [] //partid, qty, voidfl
            },

            initialize: function (data) {
                this.set("arrkitsetvo", new Array());
                this.set("arrkitpartvo", new Array());
                this.set("arrrepdtl", new Array());
            }

        });

        return GM.Model.kit;

    });
