/**********************************************************************************
 * File:        gmrKit.js
 * Description: Router for all of Surgery Kits
 * Version:     1.0
 * Author:      mahendrand
 **********************************************************************************/
define([

  // External Libraries
  'jquery', 'underscore', 'backbone',


  //Views
   'gmvKit', 'gmvKitMgmt'
],

    function ($, _, Backbone,
        GMVKit, GMVKitMgmt) {

        'use strict';

        GM.Router.Kit = Backbone.Router.extend({

            //routes to its respective web page 
            routes: {
                "kit": "kitMgmt",
                "kit/report/:report": "kitReport",
                "kit/id/:kitid": "kitDetail"
            },


            initialize: function () {
                $("#div-app-title").show();
            },

            // Routes to Kit Management View
            kitMgmt: function () {
                console.log("KIT HOME MGMT")
                var gmvKitMgmt = new GMVKitMgmt({
                    view: "kitmgmt"
                });
                gmvApp.showView(gmvKitMgmt);
            },
            
            // Routes to Kit Management to Show Inactive Reports
            kitReport: function () {
                console.log("INACTIVE KIT REPORTS")
                var gmvKitMgmt = new GMVKitMgmt({
                    view: "inactivereport"
                });
                gmvApp.showView(gmvKitMgmt);
            },
            
            // Routes to Kit Detail
            kitDetail: function (kitID) {
                if (kitID != 0) {
                    console.log(kitID);
                    var input = {
                        "token": localStorage.getItem("token"),
                        "partyid": localStorage.getItem("partyID"),
                        "kitid": kitID
                    };
                    fnGetWebServerData("kit/fetchkit", undefined, input, function (data) {
                        if (data != null) {
                            console.log("Kit Data" + JSON.stringify(data));
                            var gmvKit = new GMVKit({
                                "kitid": kitID,
                                "data": data
                            });
                            gmvApp.showView(gmvKit);
                            $("#btn-back").show();
                        } else {
                            GM.Global.AlertMsg = "No Such Kit";
                            window.location.href = "#kit";
                        }
                    }, true);
                } else {
                    console.log("CREATE KIT")
                    var gmvKit = new GMVKit({
                        "kitID": 0,
                        data: {}
                    });
                    gmvApp.showView(gmvKit);
                    $("#btn-back").show();
                }
            },




        });
        return GM.Router.Kit;
    });