/**********************************************************************************
 * File             :        gmvKitMgmt.js
 * Description      :        View to represent Kit Management Reports
 * Path             :        
 * Author           :        mahendrand
 **********************************************************************************/
define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars',

    // Common
    'global', 'commonutil', 'gmvSearch', 'gmmKit', 'daocase'
],

    function ($, _, Backbone, Handlebars,
        Global, Commonutil, GMVSearch, GMMKit, DAOCase) {


        GM.View.Kit = Backbone.View.extend({

            name: "Kit Management",

            el: "#div-main",

            template_KitMainModule: fnGetTemplate(URL_Kit_Template, "gmtKitHome"),
            template_KitManagement: fnGetTemplate(URL_Kit_Template, "gmtKitMgmt"),
            template_KitReport: fnGetTemplate(URL_Kit_Template, "gmtkitReportList"),


            events: {
                "click .liSharebtn": "shareKits",
                "click .liInvntrybtn": "addSetPartList",
                "click #mykit,  .kitmgmt-search, #sharewme,#sharebme": "searchKits",
                "click #btn-inactive-kitrpt, .inactive-search": "inactiveKit",
                "click .div-kit-list-item": "redirectSummary",
                "click #btn-kit-link": "openLink",
            },

            initialize: function (options) {
                $(window).on('orientationchange', this.onOrientationChange);

                this.view = options.view;
                $('#btn-home').show();
                $("#btn-back").show();
            },

            render: function () {
                var that = this;
                this.$el.html(this.template_KitMainModule());

                if ($(window).width() <= 767) {
                    $("#li-update-count").addClass("hide");
                    $("#btn-home").addClass("hide");
                    $(".div-kit-detail-navigation").addClass("hide");

                    $("#btn-back").show();
                    $("#div-app-title").removeClass("gmVisibilityHidden").addClass("gmVisibilityVisible");
                    $("#div-app-title").removeClass("gmMediaVisibilityH").addClass("gmMediaVisibilityV");
                }
                switch (that.view) {
                    case "kitmgmt":
                        that.kitMgmt();
                        break;
                    case "inactivereport":
                        that.inactiveKitReport();
                        that.inactiveKit();
                        break;
                    default:
                        that.kitMgmt();
                        break;

                }
                this.searchKits();
                fnGetExcSetFL(function(excSetFL){});
            },



            kitMgmt: function () {
                $("#div-app-title").html(lblNm("kit_management"));
                $("#btn-kit-mgmt").addClass("btn-selected");
                $("#div-kit-view").html(this.template_KitManagement());
                this.reportId = "mykit";
                $("#mykit").trigger("click");

                $("#btn-searchKit").removeClass("inactive-search");
                $("#btn-searchKit").addClass("kitmgmt-search");
            },


            inactiveKitReport: function () {
                $("#div-app-title").html(lblNm("inactive_kit_report"));
                $("#btn-inactive-kitrpt").addClass("btn-selected");
                $("#div-kit-view").html(this.template_KitManagement());
                $("#kit-mgmt-report").addClass("hide");
                $("#kitHomeMain").addClass("hide");
                $(".ulSharebtns ").addClass("hide");
                $("#kit-inactive-report").removeClass("hide");

                $("#btn-searchKit").removeClass("kitmgmt-search");
                $("#btn-searchKit").addClass("inactive-search");
            },

            shareKits: function (e) {
                var that = this;
                $(".liSharebtn").removeClass("selected");
                $(e.currentTarget).addClass("selected");
                that.reportId = $(e.currentTarget).attr("id");
                console.log(that.reportId);
            },

            searchKits: function (e) {
                var that = this;
                var reportType = "";
                var strOpt = "";

                var userID = localStorage.getItem("userID");
                var partyID = localStorage.getItem("partyID");
                var input = {};

                input.userid = userID;
                input.partyid = partyID;
                input.kitnm = $("#kitName").val();
                input.strOpt = that.reportId;

                console.log(input.strOpt);
                getLoader("#div-kit-list");
                fnGetWebServerData('kit/report', undefined, input, function (data) {
                    $("#btn-back").show();
                    $("#div-kit-list").html(that.template_KitReport(data));
                    $("#div-kit-list").removeClass("gmTextCenter gmPad10p");
                    $("#ul-inact-list").addClass("hide");

                    if (data == "") {
                        $("#div-kit-list").html(lblNm("no_data_found")); 
                        $("#div-kit-list").addClass("gmTextCenter gmPad10p");
                    }
                });
            },

            inactiveKit: function () {

                var that = this;
                var reportType = "";
                var strOpt = "inactiveKit";
                var userID = localStorage.getItem("userID");
                var partyID = localStorage.getItem("partyID");
                var input = {};

                input.userid = userID;
                input.partyid = partyID;
                input.strOpt = strOpt;
                input.kitnm = $("#kitName").val();
                console.log(input.kitnm);

                getLoader("#div-inact-list");
                fnGetWebServerData('kit/report', undefined, input, function (data) {

                    $("#div-inact-list").html(that.template_KitReport(data));

                    $(".kitReport").addClass("hide");
                    $("#div-inact-list").removeClass("gmTextCenter gmPad10p");

                    if (data == "") {
                        $("#div-inact-list").html(lblNm("no_data_found"));
                        $("#div-inact-list").addClass("gmTextCenter gmPad10p");
                    }

                    console.log(data);
                });
            },

            redirectSummary: function (e) {
                var kitid = $(e.currentTarget).attr("kitid");
                window.location.href = "#kit/id/" + kitid;
            },

            openLink: function () {
                $("#btn-kit-link").each(function () {
                    $(this).toggleClass("show-app");
                })
                $(".btn-app-link").toggle("fast");
            },




        });
        return GM.View.Kit;
    });