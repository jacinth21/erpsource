/**********************************************************************************
 * File             :        gmvkit.js
 * Description      :        View to represent Surgery Kits
 * Path             :        
 * Author           :        mahendrand
 **********************************************************************************/
define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars',

    // Common
    'global', 'commonutil', 'gmvSearch', 'gmmKit', 'daocase'
],

    function ($, _, Backbone, Handlebars,
        Global, Commonutil, GMVSearch, GMMKit, DAOCase) {


        GM.View.Kit = Backbone.View.extend({

            name: "Surgery Kits",

            el: "#div-main",

            template_KitMainModule: fnGetTemplate(URL_Kit_Template, "gmtKitHome"),
            template_CreateKit: fnGetTemplate(URL_Kit_Template, "gmtCreateKit"),
            template_numberkeypad: fnGetTemplate(URL_DO_Template, "GmDOPartsNumberKeypad"),
            template_partMain: fnGetTemplate(URL_Kit_Template, "gmtKitPartMain"),
            template_setMain: fnGetTemplate(URL_Kit_Template, "gmtKitSetMain"),
            template_partList: fnGetTemplate(URL_Kit_Template, "gmtKitPartList"),
            template_setList: fnGetTemplate(URL_Kit_Template, "gmtKitSetList"),
            template_kitSummary: fnGetTemplate(URL_Kit_Template, "gmtKitSummary"),
            template_kitShare: fnGetTemplate(URL_Kit_Template, "gmtKitShare"),

            events: {
                "change input": "updateModel",
                "click .liInvntrybtn": "addSetPartList",
                "change .newKitInput": "validateKit",
                "click .div-keys": "getKeyValues",
                "click #div-qty-box": "showNumberKeypad",
                "click #div-num-symbol": "deleteKeyValues",
                "click #div-keypad-close": "closeKeypad",
                "click #div-btn-done": "displayQuantity",
                "click .qty-plus": "incQty",
                "click .qty-minus": "decQty",
                "click #btn-createKit": "saveKit",
                "click #btn-editkit": "editKit",
                "click .removePart": "removePart",
                "click .removeSet": "removeSet",
                "click #img-barcode": "getBarcode",
                "click #btn-kit-link": "openLink",
                "click .div-set-partdtls": "partlistPopup",
                "click .toggleActivate": "toggleActivate",
                "click #btn-sharekit": "shareKit",
            },

            initialize: function (options) {
                var that = this;
                $(window).on('orientationchange', this.onOrientationChange);
                this.kitid = options.data.kitid;
                this.kittype = options.data.kittype;
                this.kitname = options.data.kitnm;
                this.kitdata = options.data;
                this.excsetfl = "";

                console.log(options);
                $('#btn-home').show();
                this.$("#btn-back").show();

                if (GM.Global.SurgeryKit == undefined)
                    GM.Global.SurgeryKit = {};

                if (that.kitid && that.kitid != 0) {
                    this.kitModel = new GMMKit(this.kitdata);
                    this.kitModel.set("arrkitsetvo", arrayOf(this.kitdata.arrkitsetvo));
                    this.kitModel.set("arrkitpartvo", arrayOf(this.kitdata.arrkitpartvo));
                    this.kitModel.set("arrrepdtl", arrayOf(this.kitdata.arrrepdtl));

                    if (this.kitModel.get('arrkitsetvo')[0] == null || this.kitModel.get('arrkitsetvo')[0] == undefined)
                        this.kitModel.set('arrkitsetvo', []);
                    if (this.kitModel.get('arrkitpartvo')[0] == null || this.kitModel.get('arrkitpartvo')[0] == undefined)
                        this.kitModel.set('arrkitpartvo', []);
                    if (this.kitModel.get('arrrepdtl')[0] == null || this.kitModel.get('arrrepdtl')[0] == undefined)
                        this.kitModel.set('arrrepdtl', []);

                    this.kitModel.set($.parseJSON(JSON.stringify(this.kitModel)));
                    GM.Global.SurgeryKit.Mode = "view";
                    GM.Global.SurgeryKit.Model = this.kitModel;
                } else {
                    this.kitModel = new GMMKit();
                    GM.Global.SurgeryKit.Mode = "create";
                }
                this.errors = "";
                this.kitnmAvail = "N";
            },

            render: function () {
                var that = this;
                this.$el.html(this.template_KitMainModule());
                if ($(window).width() <= 767) {
                    $("#li-update-count").addClass("hide");
                    $("#btn-home").addClass("hide");
                    $(".div-kit-detail-navigation").addClass("hide");
                    this.kitModel.set("device", "iphone");
                } else {
                    this.kitModel.set("device", "ipad");
                }
                
                fnGetExcSetFL(function(excSetFL){
	               that.excsetfl=excSetFL;
	          

                $("#div-app-title").removeClass("gmVisibilityHidden").addClass("gmVisibilityVisible");
                $("#div-app-title").removeClass("gmMediaVisibilityH").addClass("gmMediaVisibilityV");
                if (GM.Global.SurgeryKit.Mode == "view") {
                    $("#div-app-title").html(lblNm("kit_summary"));
                    $("#div-kit-view").html(that.template_kitSummary(that.kitModel.toJSON()));
                    
                     if(that.excsetfl == "Y"){
                            $("#ul-kit-set-summary").addClass("hide");
                            $("#setIncludedView").addClass("hide");
                     }

                    if (that.kitModel.get("kitowner") != localStorage.getItem('partyID')) {
                        $("#btn-editkit").addClass("hide");
                        $("#btn-deactivekit").addClass("hide");
                        $("#btn-sharekit").addClass("hide");
                    }
                    $("#btn-kit-mgmt").addClass("btn-selected");
                    $("#btn-back").show();
                } else if (GM.Global.SurgeryKit.Mode == "create") {
                    $("#div-app-title").html(lblNm("surgery_kit"));
                    $("#btn-new-kit").addClass("btn-selected");
                    $("#div-kit-view").html(that.template_CreateKit());
                    
                    if(that.excsetfl == "Y"){
                    $("#addpart").trigger("click");  // change set to part
                    $("#addset").addClass("hide");
                    $("#addpart").addClass("onActiveFl");
                    }else{
                     $("#addset").trigger("click");  // change set to part
                    }  
                    
                } else if (GM.Global.SurgeryKit.Mode == "edit") {
                    $("#div-app-title").html(lblNm("edit_kit"));
                    $("#btn-new-kit").addClass("btn-selected");
                    $("#div-kit-view").html(that.template_CreateKit(that.kitModel.toJSON()));
                    that.kitnm = that.kitModel.get("kitnm");
                    $("#btn-createKit").text(lblNm("save_kit"));
                    if(that.excsetfl == "Y"){
                    $("#addpart").trigger("click");  // change set to part
                    $("#addset").addClass("hide");
                    $("#addpart").addClass("onActiveFl");
                    }else{
                     $("#addset").trigger("click");  // change set to part
                    }
                    $(".newKitInput").trigger("change");
                }
                     });

            },
            
            // Search Tag List for the given Search Key         
            searchTag: function () {
                var that = this;
                var fnName = "";
                var searchOptions = {
                    'el': $("#div-search-tag"),
                    'url': URL_WSServer + 'cmsearch/taglist',
                    'inputParameters': {
                        'token': localStorage.getItem('token'),
                        'userid': localStorage.getItem('userID'),
                        'cmpid': localStorage.getItem('cmpid'),
                        'partyid': localStorage.getItem('partyID'),
                         'acid': localStorage.getItem('AcID')
                    },
                    'keyParameter': 'srchkey',
                    'IDParameter': 'ID',
                    'NameParameter': 'NAME',
                    'resultVO': undefined,
                    'fnName': fnName,
                    'usage': 2,
                    'minChar': 3,
                    'callback': function (result) {
                        console.log(result);
                        that.setSetDetails(result);
                    }
                };
                var searchView = new GMVSearch(searchOptions);
                $("#div-search-tag").find("input").attr("placeholder", "Scan/Enter Tags ID/Name...");
            },
            
            // Search Set List for the Given Search Key
            searchSet: function () {
                var that = this;
                var fnName = "";
                var searchOptions = {
                    'el': $("#div-search-set"),
                    'url': URL_WSServer + 'cmsearch/setlist',
                    'inputParameters': {
                        'token': localStorage.getItem('token'),
                        'userid': localStorage.getItem('userID'),
                        'cmpid': localStorage.getItem('cmpid'),
                        'partyid': localStorage.getItem('partyID'),
                        'acid': localStorage.getItem('AcID')
                    },
                    'keyParameter': 'srchkey',
                    'IDParameter': 'ID',
                    'NameParameter': 'NAME',
                    'resultVO': undefined,
                    'fnName': fnName,
                    'usage': 2,
                    'minChar': 3,
                    'callback': function (result) {
                        console.log(result);
                        that.setSetDetails(result);
                    }
                };
                var searchView = new GMVSearch(searchOptions);
                $("#div-search-set").find("input").attr("placeholder", lblNm("enter_tagid_nm"));
            },
            
            // set the set Details for the selected set
            setSetDetails: function (setData) {
                var that = this;
                var setObj = [];
                if (setData.length != undefined) {
                    console.log(setData);
                    var i = 0;
                    _.each(setData, function (item) {
                        setObj[i] = {};
                        var setArr = item.ID.split("-");
                        var setNM = item.Name;
                        setObj[i].tagid = setArr[0];
                        setObj[i].setid = setArr[1];
                        var tempsetdesc = setNM.substring(setNM.indexOf('-') + 1);
                        setObj[i].setdesc = tempsetdesc.substring(tempsetdesc.indexOf('-') + 1);
                        i++;
                    });
                } else {
                    setObj[0] = {};
                    var setArr = setData.ID.split("-");
                    var setNM = setData.Name;
                    setObj[0].tagid = setArr[0];
                    setObj[0].setid = setArr[1];
                    var tempsetdesc = setNM.substring(setNM.indexOf('-') + 1);
                    setObj[0].setdesc = tempsetdesc.substring(tempsetdesc.indexOf('-') + 1);
                }
                that.setSetValues(setObj);
            },
            
            // Set the values to the set list Template
            setSetValues: function (data) {
                var that = this;
                console.log(data);
                _.each(data, function (item) {
                    $("#sk-hsetdesc").val(item.setdesc);
                    $("#sk-hsetdesc").attr("setid", item.setid);
                    $("#sk-hsetdesc").attr("tagid", item.tagid);
                    $("#sk-hsetdesc").trigger("change");
                });
                $("#div-search-set").find("input").val("");
                $("#div-search-tag").find("input").val("");
                $("#div-setlist").html(this.template_setList(this.kitModel.toJSON()));
                that.swipeSetFn();
                $(".div-set-del").unbind('click').bind('click', function (e) {
                    that.removeSet(e);
                });
            },
            
            // Search Part for the Given Search Key
            searchPart: function () {
                var that = this;
                var fnName = "";
                var searchOptions = {
                    'el': $("#div-part-search"),
                    'url': URL_WSServer + 'cmsearch/partlist',
                    'inputParameters': {
                        'token': localStorage.getItem('token'),
                        'userid': localStorage.getItem('userID'),
                        'cmpid': localStorage.getItem('cmpid')
                    },
                    'keyParameter': 'srchkey',
                    'IDParameter': 'ID',
                    'NameParameter': 'NAME',
                    'resultVO': undefined,
                    'fnName': fnName,
                    'usage': 2,
                    'minChar': 3,
                    'callback': function (result) {
                        console.log(result);
                        that.setPartDetails(result);
                    }
                };
                var searchView = new GMVSearch(searchOptions);
                $("#div-part-search").find("input").attr("placeholder", lblNm("msg_character_search"));
            },
            
            // Set the part details for the selected part
            setPartDetails: function (partData) {
                var that = this;
                var partObj = [];
                if (partData.length != undefined) {
                    var i = 0;
                    _.each(partData, function (item) {
                        var partdesc = item.Name;
                        partObj[i] = {};
                        partObj[i].partnum = item.ID;
                        partObj[i].partdesc = partdesc.substring(partdesc.indexOf('-') + 1);
                        partObj[i].qty = 1;
                        i++;
                    });
                } else {
                    partObj[0] = {};
                    var partdesc = partData.Name;
                    partObj[0].partnum = partData.ID;
                    partObj[0].partdesc = partdesc.substring(partdesc.indexOf('-') + 1);
                    partObj[0].qty = 1;
                }
                that.setPartValues(partObj);
            },
            
            // Set the part values to the part list template
            setPartValues: function (data) {
                var that = this;
                _.each(data, function (item) {
                    $("#sk-hpartdesc").val(item.partdesc);
                    $("#sk-hpartqty").val(item.qty);
                    $("#sk-hpartdesc").attr("partnum", item.partnum);
                    $("#sk-hpartqty").attr("partnum", item.partnum);
                    $("#sk-hpartdesc").trigger("change");
                    $("#sk-hpartqty").trigger("change");
                });
                $("#div-part-search").find("input").val("");
                $("#div-partlist").html(this.template_partList(this.kitModel.toJSON()));
                that.swipePartFn();
                $(".div-part-del").unbind('click').bind('click', function (e) {
                    that.removePart(e);
                });

            },
            
            // on Toggle the part and set, show the respective divs
            addSetPartList: function (e) {
                var that = this;
                console.log($(e.currentTarget).attr("id"));

            if(that.excsetfl == "Y"){
                if ($(e.currentTarget).attr("id") == 'addpart') {
                $(".liInvntrybtn").removeClass("selected");
                $(e.currentTarget).addClass("selected");
                    $("#set-main").addClass("hide");
                    $("#part-main").removeClass("hide");
                    $("#part-main").html(this.template_partMain(this.kitModel.toJSON()));
                    $("#div-partlist").html(this.template_partList(this.kitModel.toJSON()));
                    that.searchPart();
                    that.swipePartFn();
                    $(".div-part-del").unbind('click').bind('click', function (e) {
                        that.removePart(e);
                    });
                    

                } 
            }else{
                 $(".liInvntrybtn").removeClass("selected");
                $(e.currentTarget).addClass("selected");
                if ($(e.currentTarget).attr("id") == 'addpart') {
                    $("#set-main").addClass("hide");
                    $("#part-main").removeClass("hide");
                    $("#part-main").html(this.template_partMain(this.kitModel.toJSON()));
                    $("#div-partlist").html(this.template_partList(this.kitModel.toJSON()));
                    that.searchPart();
                    that.swipePartFn();
                    $(".div-part-del").unbind('click').bind('click', function (e) {
                        that.removePart(e);
                    });


                } 
               else if ($(e.currentTarget).attr("id") == 'addset') {
                    $("#part-main").addClass("hide");
                    $("#set-main").removeClass("hide");
                    $("#set-main").html(this.template_setMain(this.kitModel.toJSON()));
                    $("#div-setlist").html(this.template_setList(this.kitModel.toJSON()));
                    that.searchSet();
//                    that.searchTag();
                    that.swipeSetFn();
                    $(".div-set-del").unbind('click').bind('click', function (e) {
                        that.removeSet(e);
                    });
                }
            }
            },

            //Validate if the Kit Name is already Used or not
            validateKit: function (e) {
                var that = this;
                var kitNm = $(e.currentTarget).val();
                console.log($(e.currentTarget).val());

                if ((kitNm != "") && (kitNm != that.kitnm)) {
                    var input = {
                        "token": localStorage.getItem("token"),
                        "kitnm": kitNm,
                        "userid": localStorage.getItem("userID"),
                        'partyid': localStorage.getItem('partyID')
                    };
                    fnGetWebServerData("kit/validatekitnm", "gmKitInputsVO", input, function (data) {
                        console.log(data);
                        if (data != null) {
                            if (data.kitNameCheck == "N") {
                                that.kitnmAvail = "Y";
                                $(".validateImg img").removeClass("hide");
                                $(".validationMsg").addClass("gmVisibilityHidden");
                                $(".validateImg #img-error").addClass("hide");

                            } else {
                                that.kitnmAvail = "N";
                                $(".validateImg img").removeClass("hide");
                                $(".validationMsg").removeClass("gmVisibilityHidden");
                                $(".validateImg #img-success").addClass("hide");
                            }
                        }
                    });
                } else {
                    that.kitnmAvail = "Y";
                    $(".validateImg img").addClass("hide");
                    $(".validationMsg").addClass("gmVisibilityHidden");
                }
            },


            /*Quantity keypad Functions*/
            showNumberKeypad: function (event) {
                this.target = $(event.currentTarget);

                var that = this;
                $('#overlay-back').fadeIn(100, function () {
                    that.$("#div-show-num-keypad").html(that.template_numberkeypad());
                    var attrval = $(event.currentTarget).attr("value");

                    if (attrval == "quantity") {
                        $("#div-keypad-title").html(lblNm("quantity") + "</div>");
                        $("#div-keypad-close").show();
                        $("#div-num-decimal").hide();
                        $("#div-num-symbol").css("width", "69px").css("height", "17px").css("margin-left", "0");
                    }
                });
            },

            getKeyValues: function (event) {
                var targetid = event.currentTarget.id;
                var targettext = $("#" + targetid).text();
                if (targetid == "div-num-decimal" && $("#div-qty-entry-text").text() == "") {
                    targettext = "0.";
                }

                if ($("#div-qty-entry-text").text() != "") {
                    if (targetid == "div-num-decimal" && $("#div-qty-entry-text").text().indexOf(".") >= 0) {
                        targettext = "";
                    }
                }
                $("#div-qty-entry-text").append(targettext);
            },

            deleteKeyValues: function (event) {
                var number_value = $("#div-qty-entry-text").text();
                var str = number_value.substring(0, number_value.length - 1);
                $("#div-qty-entry-text").text(str);

            },

            displayQuantity: function (event) {
                var that = this;
                var qty_val, part_num;

                if (($("#div-qty-entry-text").text()) == "") {
                    showMsgView("052");
                }
                if (($("#div-qty-entry-text").text()) != "" && ($("#div-qty-entry-text").text()) == "0") {
                    showMsgView("053");
                }
                if ($("#div-qty-entry-text").text() != "" && $("#div-qty-entry-text").text() != "0") {
                    //				showMsgView("011");
                }
                if (($("#div-qty-entry-text").text() != "") && ($("#div-qty-entry-text").text() != 0)) {
                    qty_val = parseInt($("#div-qty-entry-text").text());
                    $(that.target).text(qty_val);
                    part_num = $(that.target).data("partnum");
                    that.updQtyModel(qty_val, part_num);
                }


                $("#div-num-keypad").hide();
                $('#overlay-back').fadeOut(100);
            },

            closeKeypad: function (event) {
                $('#overlay-back').fadeOut(100);
                $("#div-num-keypad").hide();
            },

            // On clicking the Barcode image Scan the tag
            getBarcode: function (event) {
                var that = this;
                var partyID = localStorage.getItem("partyID");
                var userID = localStorage.getItem("userID");
                var input = {};


                var parent = $(event.currentTarget).parent();
                cordova.plugins.barcodeScanner.scan(

                    function (result) {
                        var setid = result.text;
                        $("#div-search-set").find("input").val(setid);

                        input.userid = userID;
                        input.partyid = partyID;
                        input.tagid = setid;

                        fnGetWebServerData('kit/validateTag', undefined, input, function (data) {
                            console.log(data);
                            if (data.tagidVaidate == "Y")
                                showError("Cannot add a loaner tag to a Surgery Kit");
                            else
                                $("#div-search-set").find("input").trigger("keyup");
                        });

                    },
                    function (error) {
                        console.log("Scanning failed: " + error);
                    }
                );
            },
            
            // update the quantity and trigger for set the model
            updQtyModel(Qty, partNum) {
                $("#sk-hpartqty").val(Qty);
                $("#sk-hpartqty").attr("partnum", partNum);
                $("#sk-hpartqty").trigger("change");
            },
            
            // Increments the Quantity on clicking the plus icon
            incQty: function (e) {
                var that = this;
                var count = parseInt($(e.currentTarget).siblings("#div-qty-box").text());
                var countEl = $(e.currentTarget).siblings("#div-qty-box");
                count++;
                countEl.text(count);
                var part_num = countEl.data("partnum");
                that.updQtyModel(count, part_num);
            },
            
            // Decrements the Quantity on clicking the minus icon
            decQty: function (e) {
                var that = this;
                var count = parseInt($(e.currentTarget).siblings("#div-qty-box").text());
                var countEl = $(e.currentTarget).siblings("#div-qty-box");
                if (count > 1) {
                    count--;
                    countEl.text(count);
                    var part_num = countEl.data("partnum");
                    that.updQtyModel(count, part_num);
                }
            },
            
            // Update the set details in the kit Model
            updateSetVO: function (tagid, setid, attribute, value) {
                var that = this;
                var setVO = [];
                var setVOItem;
                setVOItem = _.findWhere(this.kitModel.get("arrkitsetvo"), {
                    "setid": setid,
                    "tagid": tagid
                });
                var obj = _.findWhere(this.kitModel.get("arrkitsetvo"), {
                    "setid": setid,
                    "tagid": tagid,
                    "voidfl": "Y"
                });
                if (!setVOItem && !obj) {
                    setVOItem = {
                        "setid": setid,
                        "setdesc": "",
                        "kitid": "",
                        "tagid": tagid,
						"consignto": "",
                        "voidfl": ""
                    };
                } else if (obj) {
                    _.each(this.kitModel.get("arrkitsetvo"), function (a) {
                        if ((a.setid == setid) && (a.tagid == tagid))
                            a.voidfl = "";
                    })
                }

                setVO = _.without(this.kitModel.get("arrkitsetvo"), setVOItem);
                setVOItem[attribute] = value;
				setVOItem["consignto"] = "";
                setVO.push(setVOItem);
                this.kitModel.get("arrkitsetvo").length = 0;
                setVO = arrayOf(setVO);
                $.each(setVO, function (index, item) {
                    that.kitModel.get("arrkitsetvo").push(item);
                });
            },

            // Update the part details in the kit Model
            updatePartVO: function (partnum, attribute, value) {
                var that = this;
                var partVO = [];
                var partVOItem;

                partVOItem = _.findWhere(this.kitModel.get("arrkitpartvo"), {
                    "partnum": partnum
                });

                var obj = _.findWhere(this.kitModel.get("arrkitpartvo"), {
                    "partnum": partnum,
                    "voidfl": "Y"
                });

                if (!partVOItem && !obj) {
                    partVOItem = {
                        "partnum": partnum,
                        "partdesc": "",
                        "qty": "",
                        "kitid": "",
                        "voidfl": ""
                    };
                } else if (obj) {
                    _.each(this.kitModel.get("arrkitpartvo"), function (a) {
                        if (a.partnum == partnum)
                            a.voidfl = "";
                    })
                }

                partVO = _.without(this.kitModel.get("arrkitpartvo"), partVOItem);
                partVOItem[attribute] = value;
                partVO.push(partVOItem);

                this.kitModel.get("arrkitpartvo").length = 0;
                partVO = arrayOf(partVO);
                $.each(partVO, function (index, item) {
                    that.kitModel.get("arrkitpartvo").push(item);
                });

            },

            //Remove Part after click a Del icon
            removePart: function (event) {
                var that = this;
                var kitid = $(event.currentTarget).parent().attr("data-kitid");
                var partnum = $(event.currentTarget).parent().attr("id");

                if (parseNull(kitid) != "") {
                    _.each(this.kitModel.get("arrkitpartvo"), function (data) {
                        if (data.partnum == partnum)
                            data.voidfl = "Y";
                    });
                } else {
                    var voidPart = _.reject(this.kitModel.get("arrkitpartvo"), function (data) {
                        return data.partnum == partnum;
                    });

                    this.kitModel.set("arrkitpartvo", voidPart);

                }
                console.log(this.kitModel.toJSON());
                $("#div-partlist").html(this.template_partList(this.kitModel.toJSON()));
                that.swipePartFn();
                $(".div-part-del").unbind('click').bind('click', function (e) {
                    that.removePart(e);
                });
            },

            //Remove Set after click a Del icon
            removeSet: function (event) {
                var that = this;
                console.log(event);
                console.log($(event.currentTarget).parent());
                var kitid = $(event.currentTarget).parent().attr("data-kitid");
                var tagid = $(event.currentTarget).parent().attr("id");
                var setid = $(event.currentTarget).parent().attr("data-setid");
                if (parseNull(kitid) != "") {
                    _.each(this.kitModel.get("arrkitsetvo"), function (data) {
                        if ((data.setid == setid) && (parseNull(data.tagid) == tagid))
                            data.voidfl = "Y";
                    });
                } else {
                    var voidSet = _.reject(this.kitModel.get("arrkitsetvo"), function (data) {
                        return data.setid == setid && parseNull(data.tagid) == tagid;
                    });

                    this.kitModel.set("arrkitsetvo", voidSet);

                }
                console.log(this.kitModel.toJSON());
                $("#div-setlist").html(this.template_setList(this.kitModel.toJSON()));
                that.swipeSetFn();
                $(".div-set-del").unbind('click').bind('click', function (e) {
                    that.removeSet(e);
                });
            },


            updateRepVO: function (partyid, attribute, value) {
                var that = this;
                var partyVO = [];
                var partyVOItem;

                partyVOItem = _.findWhere(this.kitModel.get("arrrepdtl"), {
                    "partyid": partyid
                });

                var obj = _.findWhere(this.kitModel.get("arrrepdtl"), {
                    "partyid": partyid,
                    "voidfl": "Y"
                });

                if (!partyVOItem && !obj) {
                    partyVOItem = {
                        "partyid": partyid,
                        "partynm": "",
                        "kitid": "",
                        "voidfl": ""
                    };
                } else if (obj) {
                    _.each(this.kitModel.get("arrrepdtl"), function (a) {
                        if (a.partyid == partyid)
                            a.voidfl = "";
                    })
                }

                partyVO = _.without(this.kitModel.get("arrrepdtl"), partyVOItem);
                partyVOItem[attribute] = value;
                partyVO.push(partyVOItem);

                this.kitModel.get("arrrepdtl").length = 0;
                partyVO = arrayOf(partyVO);
                $.each(partyVO, function (index, item) {
                    that.kitModel.get("arrrepdtl").push(item);
                });
            },
            
            // Update the Model if change in set or part
            updateModel: function (e) {
                var that = this;
                var control;
                control = $(e.currentTarget);
                var attribute = control.attr("name");
                var setid = control.attr("setid");
                var tagid = control.attr("tagid");
                var partnum = control.attr("partnum");
                var partyid = control.attr("partyid");
                var VO = control.attr("VO");
                var value = control.val();

                if ((attribute != undefined) || (value != undefined)) {
                    if (VO == undefined) {
                        this.kitModel.set(attribute, value);
                    } else {
                        if (VO == "arrkitsetvo") {
                            this.updateSetVO(tagid, setid, attribute, value);
                        }
                        if (VO == "arrkitpartvo") {
                            this.updatePartVO(partnum, attribute, value);
                        }
                        if (VO == "arrrepdtl") {
                            this.updateRepVO(partyid, attribute, value);
                        }
                    }
                }
                console.log("Model is >" + JSON.stringify(this.kitModel));

            },
            
            // Validation on Save
            saveValidation: function (input) {
                var that = this;
                this.errors = "";
                var originalArr = input.arrkitsetvo;
                var partArr= input.arrkitpartvo;

                var checksetArr = _.filter(originalArr, function (item) {
                    return item.voidfl === '' || item.voidfl === null;
                });
                
                var checkpartArr = _.filter(partArr, function (item) {
                    return item.voidfl === '' || item.voidfl === null;
                });

                if (input.kitnm == "")
                    this.errors += lblNm("msg_kit_name_mandatory") +"\n";
                else if (that.kitnmAvail == "N")
                    this.errors += lblNm("msg_kit_name_try_another") +"\n";
                
                if(that.excsetfl == "Y"){
                          if (GM.Global.SurgeryKit.Mode == "edit") {
                    if ((checkpartArr == "") || (checkpartArr.length == 0))
                        this.errors += lblNm("msg_part_required") + "\n";
                } else if (GM.Global.SurgeryKit.Mode == "create") {
                    if ((input.arrkitpartvo == "") || (input.arrkitpartvo.length == 0))
                        this.errors += lblNm("msg_part_required")+"\n";
                }
                 }else{
                    if (GM.Global.SurgeryKit.Mode == "edit") {
                    if ((checksetArr == "") || (checksetArr.length == 0))
                        this.errors += lblNm("msg_set_required")+"\n";
                } else if (GM.Global.SurgeryKit.Mode == "create") {
                    if ((input.arrkitsetvo == "") || (input.arrkitsetvo.length == 0))
                        this.errors += lblNm("msg_set_required")+"\n";
                }
                }
                

            },
            
            // Save Kit
            saveKit: function (e) {
                var that = this,
                    confirmtxt;
                var input = this.kitModel.toJSON();
                input.userid = localStorage.getItem("userID");
                input.partyid = localStorage.getItem("partyID");
                console.log(input);
                if (input.kitid == "") {
                    input.activefl = "Y";
                    confirmtxt = lblNm("confirm_kit_create");
                } else
                    confirmtxt = lblNm("confirm_kit_save_changes");
                that.saveValidation(input);
                if (this.errors == "") {
                    dhtmlx.confirm({
                        title: lblNm("confirm_kit_notify"),
                        ok: "Ok",
                        cancel: "Cancel",
                        type: "confirm",
                        text: confirmtxt,
                        callback: function (result) {
                            if (result)
                                that.saveKitWS(input);
                        }
                    });
                } else {
                    showError(this.errors);
                }

            },
            
            // call web service on Save
            saveKitWS: function (input) {
                console.log(input);
                delete input.device;
                delete input.kitowner;
                delete input.caseid;
                delete input.surgdt;
                delete input.sttm;
                delete input.endtm;
                delete input.accnm;
                delete input.surgnm;
                delete input.dofl;
                 
                fnGetWebServerData("kit/save", undefined, input, function (data) {
                    console.log("SAVE KIT");
                    if (data != null) {
                        showSuccess(lblNm("kit_save_success"));
                        console.log(data);
                        GM.Global.SurgeryKit.Mode = "view";
                        window.location.href = "#kit/id/" + data.kitid;
                        Backbone.history.loadUrl(Backbone.history.fragment);
                    } else {
                        showError("Kit saved Failed");
                    }
                });

            },
            
            // Swipe Part to delete in iphone
            swipePartFn: function () {
                if ($(window).width() <= 767) {
                    $(".div-part-list-item").on('swipeleft', function (e, data) {
                        $(this).find(".li-content").addClass("onShowLi");
                        $(this).find(".delete-pu-span").addClass("onShowEl");
                    });

                    $(".div-part-list-item").on('tap', function (e, data) {
                        $(this).find(".li-content").removeClass("onShowLi");
                        $(this).find(".delete-pu-span").removeClass("onShowEl");
                    });
                }
            },
            
            // Swipe Set to delete in iphone
            swipeSetFn: function () {
                if ($(window).width() <= 767) {
                    $(".div-set-list-item").on('swipeleft', function (e, data) {
                        $(this).find(".li-content").addClass("onShowLi");
                        $(this).find(".delete-pu-span").addClass("onShowEl");
                    });

                    $(".div-set-list-item").on('tap', function (e, data) {
                        $(this).find(".li-content").removeClass("onShowLi");
                        $(this).find(".delete-pu-span").removeClass("onShowEl");
                    });
                }
            },
            
            // On click on Edit
            editKit: function (e) {
                var that = this;
                GM.Global.SurgeryKit.Mode = "edit";
                this.render();
            },
            
            // Other Links on Detail Navigation
            openLink: function () {
                $("#btn-kit-link").each(function () {
                    $(this).toggleClass("show-app");
                })
                $(".btn-app-link").toggle("fast");
            },
            
            // Part List Popup
            partlistPopup: function (e) {
                var that = this;
                var setID = $(e.currentTarget).data("setid");
                console.log(setID);
                var input = {
                    "token": localStorage.getItem("token"),
                    "setid": setID
                };
                that.showKitpopup("setPartList", "Part Details", "gridPartDetails");
                getLoader("#gridPartDetails");
                fnGetWebServerData("kit/fetchpart", undefined, input, function (data) {
                    if (data != null) {
                        console.log(data);
                        var rows = [],
                            i = 0;
                        _.each(data, function (griddt) {
                            rows[i] = {};
                            rows[i].id = i;
                            rows[i].data = [];
                            rows[i].data[0] = griddt.partnum + " " + griddt.partdesc;
                            rows[i].data[1] = griddt.qty;
                            i++;
                        });
                        var partData = {};
                        partData.rows = rows;
                        var setInitWidths = "*,150";
                        var setColAlign = "left,left";
                        var setColTypes = "ro,ro";
                        var setColSorting = "str,str";
                        var setHeader = ["Parts", "Qty"];
                        var enableTooltips = "true,true";
                        var footerArry = [];
                        var gridHeight = "400";
                        var setFilter = "";
                        var footerStyles = [];
                        var footerExportFL = false;
                        var gridData = loadDHTMLXGrid('gridPartDetails', gridHeight, partData, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL);
						for (var i = 0; i < gridData.getRowsNum(); i++) {
                         gridData.cells(i, 0).cell.className = 'gridPartDesc';}
                    } else {
                        $("#gridPartDetails").html(lblNm("no_data_found")); 
						
                    }
                });
            },
            
            // Show the Popup
            showKitpopup: function (containerDiv, titleText, contentID) {
                var that = this;
                showPopup();
                $("#div-crm-overlay-content-container").addClass(containerDiv);
                $("#div-crm-overlay-content-container").find(".modal-footer").addClass("hide");
                $("#div-crm-overlay-content-container").find('.modal-title').text(titleText);

                $("#div-crm-overlay-content-container .modal-body").html("<div id='" + contentID + "'></div>");
                $("." + containerDiv + " .close").unbind('click').bind('click', function (e) {
                    that.hideKitpopup(containerDiv);
                });
                getLoader("#" + contentID);
            },
            
            // Hide the popup
            hideKitpopup: function (containerDiv) {
                var that = this;
                hidePopup();
                $("#div-crm-overlay-content-container").removeClass(containerDiv);
                $("#div-crm-overlay-content-container").find(".modal-footer").removeClass("hide");
                $("#div-crm-overlay-content-container").find("#div-crm-popup-msg").html("");
                $("#div-crm-overlay-content-container").find("#div-crm-popup-msg").hide();
                if (containerDiv == 'sharePopup')
                    Backbone.history.loadUrl(Backbone.history.fragment);
            },
            
            // Toggle deactivate and Reactivate Kit
            toggleActivate: function (e) {
                var that = this;
                var divID = $(e.currentTarget).attr("id");
                if (divID == "btn-deactivekit") {
                    var popupTitle = lblNm("deactivate_kit");
                    var popupText = lblNm("deactivate_kit_confrim");
                    var strOpt = "deactivate";
                    var successMsg = lblNm("deactivate_kit_msg");
                    var activeFL = "N";
                } else if (divID == "btn-reactivekit") {
                    var popupTitle = lblNm("reactivate_kit");
                    var popupText = lblNm("reactivate_kit_confrim");
                    var strOpt = "reactivate";
                    var successMsg = lblNm("reactivate_kit_msg");
                    var activeFL = "Y";
                }

                dhtmlx.confirm({
                    title: popupTitle,
                    ok: "Yes",
                    cancel: "No",
                    type: "confirm",
                    text: popupText,
                    callback: function (result) {
                        if (result) {
                            var input = {};
                            input.userid = localStorage.getItem("userID");
                            input.partyid = localStorage.getItem("partyID");
                            input.cmpid = localStorage.getItem("cmpid");
                            input.kitid = that.kitid;
                            input.kittype = that.kittype;
                            input.kitnm = that.kitname;
                            input.stropt = strOpt;
                            input.activefl = activeFL;
                            fnGetWebServerData("kit/toggleactivate", undefined, input, function (data) {
                                if(data != null){
                                console.log(data);
                                showSuccess(successMsg);
                                window.location.href = "#kit/id/" + that.kitid;
                                Backbone.history.loadUrl(Backbone.history.fragment);
                                }
                            });
                        }
                    }
                });
            },
            
            // Share Kit Details trigger the Share popup
            shareKit: function (e) {
                var that = this;
                that.showKitpopup("sharePopup", "Kit Sharing", "shareContent");
                that.shareCommon();
            },
            
            // Search Rep List Based on the search key
            searchRep: function () {
                var that = this;
                var fnName = "";
                var searchOptions = {
                    'el': $("#div-search-rep"),
                    'url': URL_WSServer + 'cmsearch/party',
                    'inputParameters': {
                        'token': localStorage.getItem('token'),
                        'userid': localStorage.getItem('userID'),
                        'cmpid': localStorage.getItem('cmpid')
                    },
                    'keyParameter': 'srchkey',
                    'IDParameter': 'ID',
                    'NameParameter': 'NAME',
                    'resultVO': undefined,
                    'fnName': fnName,
                    'usage': 2,
                    'minChar': 3,
                    'callback': function (result) {
                        console.log(result);
                        that.setRepDetails(result);
                    }
                };
                var searchView = new GMVSearch(searchOptions);
                $("#div-search-rep").find("input").attr("placeholder", lblNm("msg_character_search"));
            },

            // Set Rep Details for the selected Reps
            setRepDetails: function (data) {
                console.log(data);
                var that = this;

                if (data.length != undefined) {
                    _.each(data, function (item) {
                        if (item.ID != localStorage.getItem("partyID")) {
                            $("#sk-hpartynm").val(item.Name);
                            $("#sk-hpartynm").attr("partyid", item.ID);

                            $("#sk-hpartynm").trigger("change");
                        } else
                            displayPopupMsg("You cannot share your own Kit");

                    });
                } else {
                    if (data.ID != localStorage.getItem("partyID")) {
                        $("#sk-hpartynm").val(data.Name);
                        $("#sk-hpartynm").attr("partyid", data.ID);
                        $("#sk-hpartynm").trigger("change");
                    } else
                        displayPopupMsg("You cannot share your own Kit");
                }
                $("#div-search-rep").find("input").val("");
                that.shareCommon();
            },

            // Save Share 
            saveShare: function (e) {
                var that = this;
                var repArr = that.kitModel.get('arrrepdtl');
                var input = {
                    "token": localStorage.getItem("token"),
                    "userid": localStorage.getItem("userID"),
                    "kitid": that.kitModel.get("kitid"),
                    "arrrepdtl": repArr
                }
                console.log(input);
                if (repArr.length != 0) {
                    fnGetWebServerData("kit/sharekit", "gmKitInputsVO", input, function (data) {
                        if (data != null) {
                            $(".sharePopup .close").trigger("click");
                            showSuccess("Kit Shared Successfully");

                            window.location.href = "#kit/id/" + data.kitid;
                            Backbone.history.loadUrl(Backbone.history.fragment);
                        } else
                            displayPopupMsg("Kit Shared Failed");
                    });
                }
            },

            //Remove Part after click a Del icon
            removeRep: function (event) {
                var that = this;
                var kitid = $(event.currentTarget).parent().attr("data-kitid");
                var partyid = $(event.currentTarget).parent().attr("id");

                if (parseNull(kitid) != "") {
                    _.each(this.kitModel.get("arrrepdtl"), function (data) {
                        if (data.partyid == partyid)
                            data.voidfl = "Y";
                    });
                } else {
                    var voidParty = _.reject(this.kitModel.get("arrrepdtl"), function (data) {
                        return data.partyid == partyid;
                    });

                    this.kitModel.set("arrrepdtl", voidParty);

                }
                that.shareCommon();
            },
            
            
            // Share Common Template
            shareCommon: function () {
                var that = this;
                $("#shareContent").html(that.template_kitShare(that.kitModel.toJSON()));
                that.searchRep();
                $("#btn-submitShare").unbind('click').bind('click', function (e) {
                    that.saveShare(e);
                });
                $(".repDel").unbind('click').bind('click', function (e) {
                    that.removeRep(e);
                });
            },
        });
        return GM.View.Kit;
    });