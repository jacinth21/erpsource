/**********************************************************************************
 * File:        GmCaseRouter.js
 * Description: Router specifically meant to route views within the 
 *              Case Scheduler application
 * Version:     1.0
 * Author:      ejayabalan
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',
        'casemainview', 'casedetailview'
      ], 
    
    function ($, _, Backbone, CaseMainView, CaseDetailView) {

	'use strict';

	var CaseRouter = Backbone.Router.extend({

		routes: {
			"case": "case",
			"case/:viewType": "case",
			"case/:viewType/:CaseID": "case"
		},

		case: function(viewType, CaseID) {
            $("#div-messagebar").show();
            hideMessages();
			this.closeView(casemainview);
			var option = [];
			option.push(viewType);
			option.push(CaseID);
			var casemainview = new CaseMainView(option);
			$("#div-main").html(casemainview.el);			
		},

		closeView: function(view) {
			if(view!=undefined) {
				view.unbind();
				view.remove();
				view = undefined;
				$("#btn-back").unbind();
			}

		}
	});

	return CaseRouter;
});
