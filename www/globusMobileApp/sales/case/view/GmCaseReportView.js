/**********************************************************************************
 * File:        GmCaseReportView.js
 * Description: 
 * Version:     1.0
 * Author:     	tvenkateswarlu
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification','datasync','dropdownview','saleitemmodel', 'saleitemcollection', 'saleitemlistview','itemview','searchview','jqueryui'
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification,DataSync,DropDownView, SaleItem, SaleItems, SaleItemListView,ItemView,SearchView,JqueryUI) {

	'use strict';
	var CaseReportView = Backbone.View.extend({

		events : {
			"click #li-case-report-from-date label, #li-case-report-from-date i, #li-case-report-to-date label, #li-case-report-to-date i" : "showDatepicker",
			"change #txt-case-from-date, #txt-case-to-date" : "getReportDate",
		    "click #div-case-report-load":"loadreport"
		},
		/* Load the templates */ 
		template: fnGetTemplate(URL_Case_Template, "GmCaseReport"),
		
		tplSpinner: fnGetTemplate(URL_Common_Template, "GmProgressSpinner"),
		

		initialize: function() {
			
			this.render();
			
		},
		
		// Render the initial contents when the Catalog View is instantiated
		render: function () {
//			showMsgView("011");
			this.$el.html(this.template());
			var that = this;
			var renderOptions = {
					"el":this.$('#div-case-report-account-search'),
					"ID" : "txt-billing-account",
            		"Name" : "txt-search-name",
            		"Class" : "txt-keyword-account",
            		"minChar" : "3",
            		"autoSearch": true,
            		"placeholder": "Key in at least 3 characters to search",
            		"fnName": fnGetAccounts,
            		"usage": 1,
            		"template" : 'GmSearchResult',
					"template_URL" : URL_Common_Template,
					"callback": function(rowValue) {that.getAccountFromSearchView(rowValue);}
            		
            	};

			  this.searchview = new SearchView(renderOptions);
			  this.showStatusType();
                			
				var todaydate = new Date();
				var that = this;
				var fdate = this.fnGetFormatDate(todaydate);
				this.$("#label-case-report-from-date").html(fdate);
				this.$("#label-case-report-to-date").html(fdate);
				this.showCaseReport(fdate,fdate);
				return this;
		},

		loadreport:function(event){	
			var fromdate,todate;
			fromdate = this.$("#label-case-report-from-date").text();
			todate   = this.$("#label-case-report-to-date").text();
			this.showCaseReport(fromdate,todate);	
			console.log(this.accid);
		},

		getAccountFromSearchView: function(rowValue){
			$("#div-itemlist").hide();
			$("#txt-billing-account").val(rowValue.Name);	
			this.accid = rowValue.ID;
		},

		getReportDate: function (event) {
			var elem = event.currentTarget;
			this.$(elem).parent().find("label").html($(elem).val());
		},

		showStatusType:function(){
			
			var statusType = {"arrStatusType" : [{"ID":"0","Name":"All Status"}
			,{"ID":"11091","Name":"Active"}
            ,{"ID":"11093","Name":"Cancelled"}
            ,{"ID":"11094","Name":"Closed"}]};

			this.statusDropDownView = new DropDownView({
				el : this.$("#div-case-report-status-list"),
				data : statusType.arrStatusType,
				DefaultID : "11091"
			});
			
		},

	    parseDate:function(str) {
		    var mdy = str.split('/')
		    return new Date(mdy[2], mdy[0]-1, mdy[1]);
		},

		daydiff:function(first, second) {
		    return (second-first)/(1000*60*60*24)
		},
		
		showCaseReport:function(fromdate,todate){
			
			var fdate = new Date(fromdate);
			var tdate = new Date(todate);
			var diffDays = tdate.getTime() - fdate.getTime(); 
			if(diffDays < 0) {
				fnShowStatus("To date Should be greater than From date ");
			}
			else {
//				fnShowStatus(" ");
				if((navigator.onLine) && (!intOfflineMode)) {

					if($("#txt-billing-account").val() == "")
						 this.accid ="";
					if(this.daydiff(this.parseDate(fromdate),this.parseDate(todate))> 60)
						{
							fnShowStatus("Records Exceeds More Than 60 days old, Please select within 60 days range ");
							return;
						}
					this.serverInput = {
						"token": localStorage.getItem("token"),
						"status": this.statusDropDownView.getCurrentValue()[0],		
						"accountid": this.accid,
						"fromdate": fromdate,
						"todate": todate
					};
					console.log(this.serverInput);
					var that = this;
					this.$("#div-case-report-content").hide();
					this.$(".div-case-page-loader").html(this.tplSpinner({"message":"Fetching data ..."})).show();
					fnGetWebServiceData("caseschdl/report","listGmCaseInfoVO", this.serverInput, function(status,data){
						console.log(data);
	     			if(status){
						if(data != undefined)						
						{	
							console.log(status);
							that.$(".div-case-page-loader").html(that.tplSpinner({"message":"Loading data ..."})).show();
//							for(var i=0;i<data.length;i++) {
//							data[i].total = "$"+data[i].total;
	 //			     	 }
						var items = new SaleItems(data);
						that.showItemListView(that.$("#div-case-report-list"), items, "GmCaseReportList", URL_Case_Template);	
						that.$(".div-case-page-loader").hide();
						that.$("#div-case-report-content").show();
						that.changeTimeZone();
						}
					}
					else	
					{
						that.$("#div-case-report-content").hide();
						that.$(".div-case-page-loader").show();
						that.$(".div-case-page-loader").html("No Data to Display");			
					}
			
					});
				}
				else
					{
					this.$("#div-case-report-content").hide();
					this.$(".div-case-page-loader").show();
					this.$(".div-case-page-loader").html(" <div> <i class='fa fa-exclamation-triangle fa-lg'></i> No internet Connection.</div>Please Connect to WiFi or Cellular Network to use Case Report");
					
					}
			}
			
		},
		
		
		changeTimeZone: function() {
			var that = this;
			$(".div-case-report-surtime").each(function(){
				var newdate = new Date(($(this).text()).split("+")[0]+"UTC");
				var target = $(this);	
				var zone = $(this).attr("zone");
				var acid = $(this).attr("acid");
				if(zone!=undefined) {
					var timeZone = zone.replace(":",'.');
					var givenOffsetMinutes = parseFloat(timeZone)*(-60);
					var finalDate = newdate;
					fnGetTimeZoneID(zone,function(data){
						zone = data[0].Name;
						var city;
						fnGetAccountState(acid,function(accountstate){
							if(accountstate[0].STATE == 1021 && zone == "Mountain")
								city = TimeZones["Arizona"];
							else
								city = TimeZones[zone];

							var offsetvalue = finalDate.dst(city);
							if(offsetvalue != givenOffsetMinutes) {
								givenOffsetMinutes = parseFloat(givenOffsetMinutes-60);
							}
							var offsetMinutes = givenOffsetMinutes + (finalDate.getTimezoneOffset()*-1);
							finalDate.setMinutes(finalDate.getMinutes() - offsetMinutes);
							var stime = that.formatNumber(finalDate.getHours())+":"+that.formatNumber(finalDate.getMinutes());
							var hour = stime.split(":")[0];
							var ext;
							var minute = stime.split(":")[1];
							if(hour > 12){
						        ext = 'PM';
						        hour = (hour - 12);
						        if(hour < 10){
						        	hour = "0" + hour;
						            target.html(hour+":"+minute+" "+ext+" ("+zone+")");
						        }
								
						        else if(hour == 12){
						            hour = "00";
						            ext = 'AM';
						            target.html(hour+":"+minute+" "+ext+" ("+zone+")");
						        }
						        
						        else {
						            target.html(hour+":"+minute+" "+ext+" ("+zone+")");
						        }
						    }
							 else if(hour < 12){
							        ext = 'AM';
								target.html(hour+":"+minute+" "+ext+" ("+zone+")");
							}
						    else if(hour == 12){
						        ext = 'PM';
						        target.html(hour+":"+minute+" "+ext+" ("+zone+")");
						    }

						});

					});
				}
			});
		},
		
		formatNumber: function(num) {
			var s = num+"";
			while (s.length < 2) s = "0" + s;
			return s;
		},

		fnGetFormatDate:function(selectedDate){
			var fdate = new Date(selectedDate);
			  var smm = ("0" + (fdate.getMonth() + 1)).slice(-2), sdd = ("0" + fdate.getDate()).slice(-2), syy = fdate.getFullYear();					  
			  syy =  syy.toString();
			  var formatDate = smm+"/"+sdd+"/"+syy;
			  return formatDate;			
		},

		showItemListView: function(controlID, collection, itemtemplate, template_URL) {
			
			this.listView = new SaleItemListView(
					{ 
						el: controlID, 
						collection: collection, 
						itemtemplate: itemtemplate,
						template_URL: template_URL,
						columnheader: 0
					}
			);
			return this.listView;
		},

		showDatepicker: function (event) {
			var elem = event.currentTarget;
			var parent = $(elem).parent().attr("id");
			var dptext = $("#"+parent +" input").attr("id");
			$("#"+dptext).datepicker( {duration: 'fast'});
			var visible = $('#ui-datepicker-div').is(':visible');
			$("#"+dptext).datepicker(visible? 'hide' : 'show');	

			event.stopPropagation();
			$(window).click(function (event) {
			    if(document.getElementById("ui-datepicker-div").style.display != "none") {
			    	
			    	var elem = event.target;
			       if(!($(elem).hasClass("ui-datepicker-prev") || $(elem).hasClass("ui-datepicker-next") || $(elem).parent().hasClass("ui-datepicker-prev") || $(elem).parent().hasClass("ui-datepicker-next"))) {
			    	   var visible = $('#ui-datepicker-div').is(':visible');
			    	   $("#"+dptext).datepicker(visible? 'hide' : 'show');	
			       }
			    }
			});
		}

	});	

	return CaseReportView;
});