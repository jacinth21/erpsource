/**********************************************************************************
 * File:        GmCaseScheduleView.js
 * Description: View which loads the cases scheduled for today, tomorrow and next 5
 *              days
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification','datasync','saleitemmodel', 'saleitemcollection', 'saleitemlistview', 'dropdownview', 'jqueryui', 'searchview', "loaderview"      
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification, Datasync, SaleItem, SaleItems, SaleItemListView, DropDownView, JqueryUI, SearchView, LoaderView) {

		'use strict';
		var CaseScheduleView = Backbone.View.extend({
			
			events : {
				"click .div-case-schedule-label" : "toggleContent",
				"click .div-schedule-header" : "toggleListDetail",
				"click #btn-edit-case" : "editCase",
				"click .div-case-box" : "selectCase",
				"click #btn-share-case" : "shareCase",
				"click #div-share-case-search-x" : "closeShareSearchList",
				"click .btn-create-event" : "AddEvent",
				"click .btn-send-icon" : "sendICSFile"
			},
			/* Load the templates */ 
			template: fnGetTemplate(URL_Case_Template, "GmCaseSchedule"),

			initialize: function(options) {
				this.options = options;
				this.parent = options.parent;
				this.render();
			},
			
			render: function () {
				console.log(this.$el);
				this.$el.html(this.template());
				this.toggleContent(event);
				return this;
			},
			
			editCase: function(event) {
				var that = this;
				var elem = event.currentTarget;
				var strCaseID = $(elem).parent().parent().attr("caseid");

				if(this.$el.attr("class") == "div-case div-case-detail-schedule" && this.parent.CaseData.caseid!="") {
					this.parent.showConfirm(strCaseID);
				}
				else{					
					console.log("Editing Case with ID: " + strCaseID);
					window.location.href = "#case/detail/" + strCaseID;
				}
	
			},

			toggleListDetail: function (event) {
				if( !($("#div-case-schedule").hasClass("div-case-detail-schedule"))){
					var elem = event.currentTarget;
					var parent = $(elem).parent();
					$(".div-schedule-header-opened").removeClass("div-schedule-header-opened");
					$(".div-schedule-list").slideUp("fast");
					if($(elem).find(".fa-schedule-header-arrow").hasClass("fa-caret-right")) {
						$(".fa-caret-down").removeClass("fa-caret-down").addClass("fa-caret-right");
						$(elem).find(".fa-schedule-header-arrow").addClass("fa-caret-down").removeClass("fa-caret-right");
						$(parent).find(".div-schedule-list").slideDown("fast");
						$(elem).addClass("div-schedule-header-opened");
						this.$("#ul-case-today").animate({
							scrollTop: this.$("#ul-case-today").scrollTop() + $(parent).position().top - 160
						}, "fast");
					}
					else {
						$(elem).find(".fa-schedule-header-arrow").addClass("fa-caret-right").removeClass("fa-caret-down");
						$(parent).find(".div-schedule-list").slideUp("fast");
						$(elem).removeClass("div-schedule-header-opened");
					}
				}
			},
			
			toggleContent: function (event) {
				if(event)
					var tabid = (event.currentTarget.id);
				
				var date, dd, mm, yyyy, target, fdate, tdate;
				
				if(!tabid) {
					tabid = "div-case-today";
				}
				

				switch(tabid) {
					case "div-case-today":
						date = new Date();
						dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
						target = this.$("#ul-case-today");
						 fdate = mm + "/" + dd + "/" + yyyy;
						 tdate = mm + "/" + dd + "/" + yyyy;
						this.display("today");
						break;	
					case "div-case-tomorrow":
						date = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
						dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
						 fdate = mm + "/" + dd + "/" + yyyy;						
						 tdate = mm + "/" + dd + "/" + yyyy;
						target = this.$("#ul-case-tomorrow");
						this.display("tomorrow");
						break;
					case "div-case-next":
						date = new Date(new Date().getTime() + 2*24 * 60 * 60 * 1000);
						dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
						 fdate = mm + "/" + dd + "/" + yyyy;
						tdate = "";
						target = this.$("#ul-case-next");
						this.display("next");
						break;

				}
								
				this.showSchedule(fdate,tdate,target);

			},
			
			
			showSchedule: function(fdate,tdate,target) {
				var that=this;
				fnGetCaseInfoID(function(result){
                    fnDeleteCaseLocalTable(_.pluck(result,'ID'),function(){
                    	that.scheduledCount();
                    	if(tdate != "") {
                    		fnGetSchedule(fdate,tdate, function(data) {
            					var items = new SaleItems(data);
            					that.showItemListView(target, items, "GmCaseScheduleList", URL_Case_Template);	
            					$(".div-case-schedule-date").each(function () {
            						if($(this).text().indexOf("-") >=0) {
            							var date = $(this).text();
            							$(this).text(date.split('-')[1] + "/" + date.split('-')[2] + "/" + date.split('-')[0]);
            						}
            					});
            					that.changeTimeZone();
            					that.showEventButton();
            					that.checkCaseStatus();
            					that.checkShareCase();
            				});
                    	}
                    	
                    	else {
                    		fnGetScheduleMore(fdate, function(data) {
            					var items = new SaleItems(data);
            					that.showItemListView(target, items, "GmCaseScheduleList", URL_Case_Template);	
            					$(".div-case-schedule-date").each(function () {
            						if($(this).text().indexOf("-") >=0) {
            							var date = $(this).text();
            							$(this).text(date.split('-')[1] + "/" + date.split('-')[2] + "/" + date.split('-')[0]);
            						}
            					});
            					that.changeTimeZone();
            					that.showEventButton();
            					that.checkCaseStatus();
            					that.checkShareCase();
            				});
                    	}
        				
                    });
               });
			},
			
			scheduledCount: function() {
				var date, dd, mm, yyyy, target, fdate, tdate;
				date = new Date();
				dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
				target = this.$("#ul-case-today");
				fdate = mm + "/" + dd + "/" + yyyy;
				tdate = mm + "/" + dd + "/" + yyyy;
				fnGetSchedule(fdate,tdate, function(todaydata) {
					$("#spn-scheduled-today-case-count").html(todaydata.length);
					date = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
					dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
					 fdate = mm + "/" + dd + "/" + yyyy;						
					 tdate = mm + "/" + dd + "/" + yyyy;
					 fnGetSchedule(fdate,tdate, function(tomorrowdata) {
						 $("#spn-scheduled-tomorrow-case-count").html(tomorrowdata.length);
						 date = new Date(new Date().getTime() + 2*24 * 60 * 60 * 1000);
						 dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
						 fdate = mm + "/" + dd + "/" + yyyy;
						 fnGetScheduleMore(fdate, function(nextdata) {
							 $("#spn-scheduled-next-case-count").html(nextdata.length);
						 });
						 
					 });
				});
				
			},
			
			
			changeTimeZone: function() {
				var that = this;
				$(".div-case-schedule-stime-header-value").each(function(){
					// var stime = $(this).text();
					// console.log(stime);
					var newdate = new Date(($(this).text()).split("+")[0]+"UTC");
					var target = $(this);
					var cainid = $(this).parent().parent().parent().parent().attr("cainid");
					var acid = $(this).attr("accountid");
					fnGetCaseAttribute(cainid, function(attrData) {
						for(var i=0;i<attrData.length;i++) {
							if(attrData[i].ATTRIBUTE_TYPE == '4000534') {
								console.log(attrData[i]);
								var zone = attrData[i].ATTRIBUTE_VALUE;
								console.log("zone>>>>" + zone);
								var timeZone = zone.replace(":",'.');
								var givenOffsetMinutes = parseFloat(timeZone)*(-60);
								var finalDate = newdate;
								fnGetTimeZoneID(zone,function(data){
									zone = data[0].Name;
									var city;
									fnGetAccountState(acid,function(accountstate){
										if(accountstate[0].STATE == 1021 && zone == "Mountain")
											city = TimeZones["Arizona"];
										else
											city = TimeZones[zone];

										var offsetvalue = finalDate.dst(city);
										if(offsetvalue != givenOffsetMinutes) {
											givenOffsetMinutes = parseFloat(givenOffsetMinutes-60);
										}
										var offsetMinutes = givenOffsetMinutes + (finalDate.getTimezoneOffset()*-1);
										finalDate.setMinutes(finalDate.getMinutes() - offsetMinutes);
	//									var stime = that.formatNumber(finalDate.getHours())+":"+that.formatNumber(finalDate.getMinutes())+" ("+zone+")" ;
										var stime = that.formatNumber(finalDate.getHours())+":"+that.formatNumber(finalDate.getMinutes());
										var hour = stime.split(":")[0];
										var ext;
										var minute = stime.split(":")[1];
										if(hour > 12){
									        ext = 'PM';
									        hour = (hour - 12);
									        if(hour < 10){
									        	hour = "0" + hour;
									            target.html(hour+":"+minute+" "+ext+" ("+zone+")");
									        }
											
									        else if(hour == 12){
									            hour = "00";
									            ext = 'AM';
									            target.html(hour+":"+minute+" "+ext+" ("+zone+")");
									        }
									        
									        else {
									            target.html(hour+":"+minute+" "+ext+" ("+zone+")");
									        }
									    }
									    else if(hour < 12){
									        ext = 'AM';
										target.html(hour+":"+minute+" "+ext+" ("+zone+")");
									    }
									    else if(hour == 12){
									        ext = 'PM';
									        target.html(hour+":"+minute+" "+ext+" ("+zone+")");
									    }
									});
								});
								
							}
						}
					});
				});
			},
			
			checkCaseStatus: function() {
				$(".div-case-schedule-status-value").each(function(){
					var status = $(this).attr("status");
					if(status == "11093") {
						$(this).html("Cancelled");
					}
					else if(status == "11091") {
						$(this).html("Active");
					}
					else if(status == "11090") {
						$(this).html("Draft");
					}
				});
			},

			checkShareCase: function() {
				var that = this;
				if($("#spn-case-share-count").text() != 0) {
					$(".div-schedule-header").each(function(){
						var target = $(this);
						var caseid = $(this).attr("caseid");
						var cainid = $(this).attr("cainid");
						
						for(var i=0; i<that.sharecase.length; i++) {
							if(cainid == that.sharecase[i].cainfoid) {
								target.find(".div-case-box").find("i").removeClass("fa-circle-o").addClass("fa-check-circle");
								target.find(".div-case-box").addClass("selected-case");
							}
						}
						
					});
					
				}
			},

			selectCase: function(event) {
				event.stopPropagation();
				var target = event.currentTarget;
				if(!$(target).hasClass("selected-case")) {
					$(target).addClass("selected-case");
					$(target).find("i").removeClass("fa-circle-o").addClass("fa-check-circle");
					this.selectCaseCount();
				}
				else {
					$(target).removeClass("selected-case");
					$(target).find("i").addClass("fa-circle-o").removeClass("fa-check-circle");
					this.selectCaseCount();
				}
				
			},
			
			selectCaseCount: function() {
				var that = this;
				this.sharecase = new Array();
				this.selectedcase = document.getElementsByClassName('selected-case');
				$("#spn-case-share-count").html(this.selectedcase.length);
				if(this.selectedcase.length>0) {
					$(".selected-case").each(function() {
						var cainfoid = $(this).parent().parent().attr("cainid");
						var caseid = $(this).parent().parent().attr("caseid");
						var acname = $(this).parent().parent().attr("acname");
						var category = $(this).parent().parent().attr("category");
						var sname = $(this).parent().parent().attr("sname");
						that.sharecase.push({"cainfoid" : cainfoid, "caseid" : caseid, "acname" : acname, "category" : category, "sname" : sname});
						console.log(that.sharecase);
					});
				}

				else {
					$("#div-share-case-search").hide();
				}
				
			},
			
			shareCase: function() {
				var that = this;
				if($("#spn-case-share-count").text() != 0) {
					$("#div-share-case-search").show();
					var shareOptions = {
							"el":this.$('#div-share-case-search-list'),
							"ID" : "txt-schedule-case-share",
		            		"Name" : "txt-search-name",
		            		"Class" : "txt-keyword-initial",
		            		"minChar" : "3",
		            		"autoSearch": true,
		            		"placeholder": "Search...",
		            		"fnName": fnGetUser,
		            		"usage": 2,
		            		"template" : 'GmCaseSearchResult',
							"template_URL" : URL_Case_Template,
							"callback": function(rowValue){
								that.getShareListFromSearchView(rowValue)
								}
		            		
		            	};
					
					if(that.sharesearchview)
						that.sharesearchview.close();

					that.sharesearchview = new SearchView(shareOptions);
				}
				else {
					fnShowStatus("Please Select the Case");
				}
			},
			
			closeShareSearchList: function() {
				$("#div-share-case-search").hide();
			},
			
			getShareListFromSearchView: function(rowValue) {
				var that = this;
//				this.sharedEmails = new Array();
				var arrSelectedData = new Array();
				this.arrGmCaseAttributeVO = new Array();
				
				if(rowValue.length==undefined)
					arrSelectedData.push(rowValue);
				else
					arrSelectedData = rowValue;
				
				
				
				if(arrSelectedData.length>0) {
					if((navigator.onLine) && (!intOfflineMode)) {
						this.loaderview = new LoaderView({text : "Sharing Case..."});
//						for(var i=0; i<arrSelectedData.length; i++) {
//							 fnGetEmailID(arrSelectedData[i].ID,function(data){
//							 	that.sharedEmails.push(data[0].ID);
//							 });
//						}

						this.prepareAttributeVO(arrSelectedData, 0);
						
					}
					
					else {
						fnShowStatus("No Internet Connection. Please try after some time ");
						$("#div-share-case-search").hide();
						$("#txt-schedule-case-share").val("");
					}
					
				}				
				else {
					$("#div-share-case-search").hide();
					$("#txt-schedule-case-share").val("");
				}
					
			},
			
			prepareAttributeVO: function(arrSelectedData,iValue) {
				var that = this;
				$("#div-share-case-search").hide();
				$("#txt-schedule-case-share").val("");
				if(iValue < this.sharecase.length){
					for(var i=0; i<arrSelectedData.length; i++) {
						this.arrGmCaseAttributeVO.push({"caattrid" :"", "cainfoid" : this.sharecase[iValue].cainfoid, "attrtp" : "103647" , "attrval" : arrSelectedData[i].ID, "voidfl" : ""});
					}
					iValue++;
					this.prepareAttributeVO(arrSelectedData, iValue);
				}
				
				else {
					console.log(this.arrGmCaseAttributeVO);
					fnShareCase(this.arrGmCaseAttributeVO, function(data){
						console.log(data);
						fnUpdateCase(function(){
							console.log("update Completed");							
							that.loaderview.close();
							that.sharecase = new Array();
							$(".selected-case").find("i").addClass("fa-circle-o").removeClass("fa-check-circle");
							$(".selected-case").removeClass("selected-case");
							$("#spn-case-share-count").html(0);
//							that.ICSFileContent = "";
//							that.createICSFileContent(that.sharecase, 0);
						});
					});
				}
				
			},
			
			createICSFileContent: function(sharecase, value) {
				var that = this;
				if(value < sharecase.length){
					var hr, mn;
					fnGetCase(sharecase[value].caseid,function(data){
						that.CaseData = {};
						var newdate = new Date((data.surstime).split("+")[0]+"UTC");
						var stime = that.formatNumber(newdate.getHours())+":"+that.formatNumber(newdate.getMinutes());			
						that.CaseData.surdt = data.surdt;
						that.CaseData.surstime = stime;

						var dd,mm,yyyy;
						dd = ("0" + newdate.getDate()).slice(-2), mm = ("0" + (newdate.getMonth() + 1)).slice(-2), yyyy = newdate.getFullYear();
						var month = new Array();
					    month[0] = "January"; month[1] = "February";month[2] = "March"; month[3] = "April"; month[4] = "May"; month[5] = "June"; month[6] = "July"; month[7] = "August"; month[8] = "September"; month[9] = "October"; month[10] = "November"; month[11] = "December";
					    mm = month[newdate.getMonth()];
					    that.CaseData.sursdate = mm+" "+dd+", "+yyyy;
						
						fnGetCaseAttribute(data.caseinid, function(attrData) {
							for(var i=0;i<attrData.length;i++) {
								if(attrData[i].ATTRIBUTE_TYPE == '4000541') {
									hr =  attrData[i].ATTRIBUTE_VALUE.replace("h","");
								}								
							}
						
							var starttimehours = (that.CaseData.surstime).split(":")[0];
							var starttimeminutes = (that.CaseData.surstime).split(":")[1];
							var endtimeminutes = parseInt(starttimeminutes);
							var endtimehours = parseInt(starttimehours)+ parseInt(hr);
							var date = new Date(that.CaseData.sursdate);
							// var dd,mm,yyyy;
							if( endtimehours >= 24) {
								endtimehours = endtimehours - 24;			
								date = new Date(date.getTime() + 24 * 60 * 60 * 1000);
							 }
							dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
							endtimehours = (endtimehours.toString().length == 1) ? "0" + endtimehours : endtimehours;
							endtimeminutes = (endtimeminutes.toString().length == 1) ? "0" + endtimeminutes : endtimeminutes;
							that.CaseData.suretime = yyyy+mm+dd+"T"+endtimehours+endtimeminutes+"00";
							
							// var month = new Array();
						 //    month[0] = "January"; month[1] = "February";month[2] = "March"; month[3] = "April"; month[4] = "May"; month[5] = "June"; month[6] = "July"; month[7] = "August"; month[8] = "September"; month[9] = "October"; month[10] = "November"; month[11] = "December";
							mm = month[date.getMonth()];
							that.CaseData.suredate = mm+" "+dd+", "+yyyy+" "+endtimehours+":"+endtimeminutes+":00";	
							
							date = new Date(that.CaseData.sursdate);
							dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
							
							var surstime = stime+"00";
							surstime = surstime.replace(":","");
							var suretime = that.CaseData.suretime;
							var sequence = new Date();
							sequence = parseInt(((sequence.getUTCFullYear()).toString()).slice(-2))+that.formatNumber(sequence.getUTCMonth()+1)+that.formatNumber(sequence.getUTCDate())+that.formatNumber(sequence.getUTCHours())+that.formatNumber(sequence.getUTCMinutes());
							console.log(value + " " + that.ICSFileContent);
							var userId = localStorage.getItem("userID");
							fnGetEmailID (userId, function(emailid) {
								var eid;
								if(emailid[0].ID.indexOf(",") >=0) 
									eid = emailid[0].ID.split(",")[0];
								else 
									eid = emailid[0].ID;
							
								that.ICSFileContent = that.ICSFileContent+"BEGIN:VEVENT"+"\n"+"UID:"+sharecase[value].caseid+"\n"+"SEQUENCE:"+sequence+"\n"+"DTSTAMP:"+yyyy+mm+dd+"T075000Z"+"\n"+"ORGANIZER:"+eid+"\n"+"DTSTART:"+yyyy+mm+dd+"T"+surstime+"\n"+"DTEND:"+suretime+"\n"+"SUMMARY:"+sharecase[value].sname+" - "+sharecase[value].category+"\n"+"LOCATION:"+sharecase[value].acname+"\n"+"STATUS:CONFIRMED"+"\n"+"END:VEVENT"+"\n";
								value++;
								that.createICSFileContent(sharecase, value);								
							});
						});
					});
				}
				else {
					console.log(this.ICSFileContent);
					fnSendICSFile("REQUEST",this.ICSFileContent,this.sharedEmails,"share","");
					this.sharecase = new Array();
					this.selectedcase = document.getElementsByClassName('selected-case');
					$(".selected-case").find("i").addClass("fa-circle-o").removeClass("fa-check-circle");
					$(".selected-case").removeClass("selected-case");
					this.ICSFileContent = "";
					this.sharedEmails = new Array();
					$("#spn-case-share-count").html(0);
				}
			},
			
			showEventButton: function() {
				var that = this;
				var userId = localStorage.getItem("userID");
				var dept = localStorage.getItem("Dept");
				var acid = localStorage.getItem("AcID");
				
				$(".div-schedule-header").each(function(){
					var target = $(this);
					var caseid = $(this).attr("caseid");
					var cainid = $(this).attr("cainid");
					if($(this).attr("status") == "11093") {
						target.find(".div-case-box").hide();
						target.find("#btn-edit-case").hide();
						target.find(".btn-create-event").hide();
						if($(this).attr("createdby") != userId) {
							target.find(".btn-send-icon").hide();
						}
						else {
							target.find(".btn-send-icon").css("display","inline-block");
						}
					}
					else if($(this).attr("status") == "Pending Sync") {
						target.find(".div-case-box").hide();
						target.find("#btn-edit-case").hide();
						target.find(".btn-create-event").hide();
						target.find(".btn-send-icon").hide();
					}
					else if($(this).attr("status") == "11090") {
						target.find(".div-case-box").hide();
						target.find(".btn-send-icon").hide();
					}
					else if($(this).attr("createdby") != userId) {						
						target.find(".div-case-box").hide();
						target.find("#btn-edit-case").hide();
						target.find(".btn-send-icon").hide();
						fnGetCaseAttribute(cainid, function(attrData) {
							for(var i=0;i<attrData.length;i++) {
								if(attrData[i].ATTRIBUTE_TYPE == '103647') {
									if(attrData[i].ATTRIBUTE_VALUE == userId && attrData[i].VOID_FL != "Y") {
										target.find(".btn-create-event").css("display","inline-block");
									}
									else if(attrData[i].ATTRIBUTE_VALUE == userId && attrData[i].VOID_FL == "Y") {
										target.find(".btn-create-event").hide();
									}
								}
								else if(attrData[i].ATTRIBUTE_TYPE == '103646') {
									if(attrData[i].ATTRIBUTE_VALUE == userId  && attrData[i].VOID_FL != "Y") {
										target.find(".btn-create-event").hide();
									}
									else if(attrData[i].ATTRIBUTE_VALUE == userId && attrData[i].VOID_FL == "Y") {
										target.find(".btn-create-event").hide();
									}
								}
								
							}
						});
					}
					
					else if($(this).attr("createdby") == userId) {
						fnGetCaseAttribute(cainid, function(attrData) {
							for(var i=0;i<attrData.length;i++) {
								if(attrData[i].ATTRIBUTE_TYPE == '103646') {
									if(attrData[i].ATTRIBUTE_VALUE == userId && attrData[i].VOID_FL == "Y") {
										target.find(".div-case-box").hide();
										target.find("#btn-edit-case").hide();
										target.find(".btn-create-event").hide();
									}
									else if(attrData[i].ATTRIBUTE_VALUE == userId && attrData[i].VOID_FL != "Y") {
										target.find(".div-case-box").css("display","inline-block");
										target.find("#btn-edit-case").css("display","inline-block");
										target.find(".btn-create-event").hide();
									}
								}
								else if(attrData[i].ATTRIBUTE_TYPE == '103647') {
									if(attrData[i].ATTRIBUTE_VALUE == userId && attrData[i].VOID_FL == "Y") {
										target.find(".div-case-box").hide();
										target.find("#btn-edit-case").hide();
										target.find(".btn-create-event").hide();
									}
									else if(attrData[i].ATTRIBUTE_VALUE == userId && attrData[i].VOID_FL != "Y") {
										target.find(".div-case-box").css("display","inline-block");
										target.find("#btn-edit-case").css("display","inline-block");
										target.find(".btn-create-event").hide();
									}
								}
								
							}
						});
					}
				});
			},

			checkVoidAttribute: function() {
				var that = this;
				var userId = localStorage.getItem("userID");
				$(".div-schedule-header").each(function(){
					var caseid = $(this).attr("caseid");
					var cainid = $(this).attr("cainid");
					var target = $(this);
					console.log(caseid+" "+cainid);
					fnGetCaseAttribute(cainid, function(attrData) {
						for(var i=0;i<attrData.length;i++) {
							if(attrData[i].ATTRIBUTE_TYPE == '103646') {
								if(attrData[i].ATTRIBUTE_VALUE == userId && attrData[i].VOID_FL == "Y") {
									target.find(".div-case-box").hide();
									target.find("#btn-edit-case").hide();
									target.find(".btn-create-event").hide();
								}
							}
							else if(attrData[i].ATTRIBUTE_TYPE == '103647') {
								if(attrData[i].ATTRIBUTE_VALUE == userId && attrData[i].VOID_FL == "Y") {
									target.find(".div-case-box").hide();
									target.find("#btn-edit-case").hide();
									target.find(".btn-create-event").hide();
								}
							}
							
						}
					});
				});
			},
			
			
			AddEvent: function(event) {
				event.stopPropagation();
				var elem = event.currentTarget;
				var cainid = $(elem).parent().parent().attr("cainid");
				console.log(cainid);
				var caseinfid = new Array();
				caseinfid.push(cainid);
				fnGetCalenderEventInfo(caseinfid, function(data) {
				    fnCreateEvent(0, data, true, function() {
				    });
				});

			},
			
			
			formatNumber: function(num) {
				var s = num+"";
				while (s.length < 2) s = "0" + s;
				return s;
			},
			
			sendICSFile: function(event) {
				event.stopPropagation();
				var that = this;
				var elem = event.currentTarget;
				var cainid = $(elem).parent().parent().attr("cainid");
				var caseid = $(elem).parent().parent().attr("caseid");
				var category = $(elem).parent().parent().attr("category");
				var sname = $(elem).parent().parent().attr("sname");
				var acname = $(elem).parent().parent().attr("acname");
				var hr, mn;
				fnGetCase(caseid,function(data){
					that.CaseData = {};
					var newdate = new Date((data.surstime).split("+")[0]+"UTC");
					var stime = that.formatNumber(newdate.getHours())+":"+that.formatNumber(newdate.getMinutes());			
					that.CaseData.surdt = data.surdt;
					that.CaseData.surstime = stime;
					that.CaseData.status = data.status;

					var dd,mm,yyyy;
					dd = ("0" + newdate.getDate()).slice(-2), mm = ("0" + (newdate.getMonth() + 1)).slice(-2), yyyy = newdate.getFullYear();
					var month = new Array();
				    month[0] = "January"; month[1] = "February";month[2] = "March"; month[3] = "April"; month[4] = "May"; month[5] = "June"; month[6] = "July"; month[7] = "August"; month[8] = "September"; month[9] = "October"; month[10] = "November"; month[11] = "December";
				    mm = month[newdate.getMonth()];
				    that.CaseData.sursdate = mm+" "+dd+", "+yyyy;
					
					fnGetCaseAttribute(data.caseinid, function(attrData) {
						that.sendEmails = new Array();
						for(var i=0;i<attrData.length;i++) {
							if(attrData[i].ATTRIBUTE_TYPE == '4000541') {
								hr =  attrData[i].ATTRIBUTE_VALUE.replace("h","");
							}
							else if(attrData[i].ATTRIBUTE_TYPE == '103646') {
								fnGetEmailID(attrData[i].ATTRIBUTE_VALUE,function(data){
									 that.sendEmails.push(data[0].ID);
								});
								
							}								
							else if(attrData[i].ATTRIBUTE_TYPE == '103647') {
								fnGetEmailID(attrData[i].ATTRIBUTE_VALUE,function(data){
									 that.sendEmails.push(data[0].ID);
								});
							}
						}
					
						var starttimehours = (that.CaseData.surstime).split(":")[0];
						var starttimeminutes = (that.CaseData.surstime).split(":")[1];
						var endtimeminutes = parseInt(starttimeminutes);
						var endtimehours = parseInt(starttimehours)+ parseInt(hr);
						var date = new Date(that.CaseData.sursdate);
						if( endtimehours >= 24) {
							endtimehours = endtimehours - 24;			
							date = new Date(date.getTime() + 24 * 60 * 60 * 1000);
						 }
						dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
						endtimehours = (endtimehours.toString().length == 1) ? "0" + endtimehours : endtimehours;
						endtimeminutes = (endtimeminutes.toString().length == 1) ? "0" + endtimeminutes : endtimeminutes;
						that.CaseData.suretime = yyyy+mm+dd+"T"+endtimehours+endtimeminutes+"00";
						
						mm = month[date.getMonth()];
						that.CaseData.suredate = mm+" "+dd+", "+yyyy+" "+endtimehours+":"+endtimeminutes+":00";	
						
						date = new Date(that.CaseData.sursdate);
						dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
						
						var surstime = stime+"00";
						surstime = surstime.replace(":","");
						var suretime = that.CaseData.suretime;
						var sequence = new Date();
						sequence = parseInt(((sequence.getUTCFullYear()).toString()).slice(-2))+that.formatNumber(sequence.getUTCMonth()+1)+that.formatNumber(sequence.getUTCDate())+that.formatNumber(sequence.getUTCHours())+that.formatNumber(sequence.getUTCMinutes());
						var userId = localStorage.getItem("userID");
						fnGetEmailID (userId, function(emailid) {
							var eid;
							if(emailid[0].ID.indexOf(",") >=0) 
								eid = emailid[0].ID.split(",")[0];
							else 
								eid = emailid[0].ID;

							var icscontent = "BEGIN:VEVENT"+"\n"+"UID:"+caseid+"\n"+"SEQUENCE:"+sequence+"\n"+"DTSTAMP:"+yyyy+mm+dd+"T075000Z"+"\n"+"ORGANIZER:"+eid+"\n"+"DTSTART:"+yyyy+mm+dd+"T"+surstime+"\n"+"DTEND:"+suretime+"\n"+"SUMMARY:"+sname+" - "+category+"\n"+"LOCATION:"+acname+"\n"+"STATUS:CONFIRMED"+"\n"+"END:VEVENT"+"\n";	
							if(that.CaseData.status == "11091"){
								fnSendICSFile("REQUEST",icscontent,that.sendEmails,caseid,"Create/Update");
							}
							else if(that.CaseData.status == "11093"){
								fnSendICSFile("CANCEL",icscontent,that.sendEmails,caseid,"Delete");
							}
						});
					});
				});
				
			},
			
			showItemListView: function(controlID, collection, itemtemplate, template_URL) {
				
				this.listView = new SaleItemListView(
						{ 
							el: controlID, 
							collection: collection, 
							itemtemplate: itemtemplate,
							template_URL: template_URL,
							columnheader: 0
						}
				);
				return this.listView;
			},
			
			display: function(key) {

				if(key.currentTarget==undefined) {
					this.key = key;
				}
				else {
					this.key = "today";
					
				}
				this.$(".div-case-schedule-label").each(function() {
					$(this).removeClass("selected-tab");
				});
				

				this.$("#div-case-" + this.key).addClass("selected-tab");
				this.$(".ul-case-schedule-info").hide();
				this.$("#ul-case-" + this.key).show();
			}
			
		});	
	
	return CaseScheduleView;
});
