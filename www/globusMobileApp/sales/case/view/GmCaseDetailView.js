/**********************************************************************************
 * File:        GmCaseDetailView.js
 * Description: 
 * Version:     1.0
 * Author:     	ejayabalan
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification','searchview', 'daocase',
        'casemodel', 'casescheduleview', 'dropdownview', 'loaderview','timezoneJS'
    ], 

    function ($, _, Backbone, Handlebars, Global, Device, Notification, SearchView, DAOCase, CaseModel, CaseScheduleView, DropDownView, LoaderView, TimezoneJS) {

	'use strict';
	var CaseDetailView = Backbone.View.extend({
		
		events : {
			"click #li-case-book-surgerydate label, #li-case-book-surgerydate .date-picker" : "showDatepicker",
			"change #li-case-book-surgerydate .sdate" : "getSurgeryDate",
			"change input" : "updateCase",
			"click #btn-case-post" : "postCase",
			"click .spn-spandiv-close":"removeSalesRep",
			"change #div-hours #hour, #div-hours #minute": "getDuration",
			"click #btn-case-save": "saveDraft",
			"click #btn-case-delete": "deleteCase",
			"keyup #txt-case-book-note": "updateNotes",
			"keyup #txt-billing-account, #txt-billing-salesrep, #txt-billing-fieldsales, #txt-case-book-surgeon-name": "searchCase"
			
		},
		/* Load the templates */ 
		template: fnGetTemplate(URL_Case_Template, "GmCaseDetail"),
		
		initialize: function(options) {

			var that = this;
			this.status;
			this.state = "";
			var casedata, caseid;
			this.options = options;
			this.parent = options.parent;
			this.voidAttributeList = new Array();
			
			$("body").removeClass();
			$("#btn-home").show();
			$("#btn-back").show();
			
			
			caseid = this.options.CaseID;
			
			this.parent.$('#div-case-schedule').addClass("div-case-detail-schedule");
			this.casescheduleview = new CaseScheduleView({'el' :this.parent.$('#div-case-schedule'),  'parent' : this});
			
			this.CaseData = {};

			if(caseid) {
				console.log("CaseID is obtained as " + caseid);
				fnGetCase(this.options.CaseID, function(data) {
					var newdate = new Date((data.surstime).split("+")[0]+"UTC");					
//					var stime = that.formatNumber(newdate.getHours())+":"+that.formatNumber(newdate.getMinutes());					
					that.CaseData.caseid = data.caseid;
					that.CaseData.cainfoid = data.caseinid;
					that.CaseData.surdt = data.surdt;
					that.CaseData.surstime = "";
					that.CaseData.sursdate = "";
					that.CaseData.did = data.did;
					that.CaseData.repid = data.repid;
					that.CaseData.acid = data.acid;
					that.CaseData.prnotes = data.prnotes;
					that.CaseData.status = data.status;
					that.CaseData.categorieid = "";
					that.CaseData.categoriename = "";
					that.CaseData.tmzone = "";
					that.CaseData.tmzoneid = "";
					that.CaseData.suretime = "";
					that.CaseData.suredate = "";
					that.CaseData.voidfl = "";
					that.CaseData.type = "";
					that.CaseData.prtcaid = "";
					that.CaseData.acname = "";
					that.CaseData.repname = "";
					that.CaseData.sname = "";
					that.CaseData.skipvalidationfl = "";
					that.viewJSON = $.parseJSON(JSON.stringify(data));
					that.viewJSON.surstime = "";
//					that.CaseData = data;
//					that.surgerystarttime = stime;
					fnGetCaseCategorie(data.caseinid, function(data){
						for(var i=0;i<data.length;i++) {
							that.CaseData.categorieid = data[i].ATTRIBUTE_TYPE;
							fnGetCategorieName(data[i].ATTRIBUTE_TYPE, "CASCAT", function(result){
								that.CaseData.categoriename = result[0].Name;
								console.log(that.CaseData.categoriename);
							});
						}
					});
					fnGetCaseAttribute(data.caseinid, function(attrData) {
						console.log("got Attr");
						console.log(attrData);
						var assignto = new Array();
						var shareto = new Array();
						for(var i=0;i<attrData.length;i++) {
							if(attrData[i].ATTRIBUTE_TYPE == '103646') {
								assignto.push({"ID" :attrData[i].ATTRIBUTE_VALUE, "Name" : attrData[i].REP_NAME, "voidfl" : attrData[i].VOID_FL});
//								assignto.push(attrData[i].REP_NAME);
								
							}								
							else if(attrData[i].ATTRIBUTE_TYPE == '103647') {
								shareto.push({"ID" :attrData[i].ATTRIBUTE_VALUE, "Name" : attrData[i].USER_NAME, "voidfl" : attrData[i].VOID_FL});
//								shareto.push(attrData[i].USER_NAME);
							}								
							else if(attrData[i].ATTRIBUTE_TYPE == '4000541') {
								that.viewJSON.hr =  attrData[i].ATTRIBUTE_VALUE;
//								that.viewJSON.mn =  attrData[i].ATTRIBUTE_VALUE.split(',')[1];
								that.viewJSON.durationattrid = attrData[i].CASE_ATTRIBUTE_ID;
							}
							else if(attrData[i].ATTRIBUTE_TYPE == '4000534') {
								that.CaseData.tmzone = attrData[i].ATTRIBUTE_VALUE;
								that.viewJSON.tmzoneattrid = attrData[i].CASE_ATTRIBUTE_ID;
								
								var timeZone = that.CaseData.tmzone.replace(":",'.');
								var givenOffsetMinutes = parseFloat(timeZone)*(-60);

								var finalDate = newdate;
								fnGetTimeZoneID(that.CaseData.tmzone,function(data){
									var zone = data[0].Name;
									var city;
									fnGetAccountState(that.CaseData.acid,function(accountstate){
										if(accountstate[0].STATE == 1021 && zone == "Mountain") {
											city = TimeZones["Arizona"];
											that.state = 1021;
										}
										else{
											city = TimeZones[zone];
										}

										var offsetvalue = finalDate.dst(city);
										if(offsetvalue != givenOffsetMinutes) {
											givenOffsetMinutes = parseFloat(givenOffsetMinutes-60);
										}
										var offsetMinutes = givenOffsetMinutes + (finalDate.getTimezoneOffset()*-1);
										
										finalDate.setMinutes(finalDate.getMinutes() - offsetMinutes);
										
										var stime = that.formatNumber(finalDate.getHours())+":"+that.formatNumber(finalDate.getMinutes());

										that.CaseData.surstime = stime;
										that.viewJSON.surstime = stime;
										$("#txt-case-book-surgery-stime").val(stime);
									});
								});
								
							}
							else if(attrData[i].ATTRIBUTE_TYPE == '103640') {
								that.CaseData.sname =  attrData[i].ATTRIBUTE_VALUE;
								that.viewJSON.snameattrid = attrData[i].CASE_ATTRIBUTE_ID;
							}
							else if(attrData[i].ATTRIBUTE_TYPE == '4000666') {
								that.viewJSON.casecloseattrid = attrData[i].CASE_ATTRIBUTE_ID;
							}
						}
//						that.viewJSON.assignto = assignto;
//						that.viewJSON.shareto = shareto;
						that.viewJSON.assignto = $.parseJSON(JSON.stringify(assignto));
						that.viewJSON.shareto = $.parseJSON(JSON.stringify(shareto));
						console.log(that.viewJSON);
						that.render(that.viewJSON); 
					});
					
				});
			}

			else {
				var date = new Date();
				var dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
				var time = this.formatNumber(date.getHours())+":"+this.formatNumber(date.getMinutes());
				this.CaseData = {};
				this.CaseData.caseid = "";
				this.CaseData.cainfoid = "";
				this.CaseData.surdt = mm+"/"+dd+"/"+yyyy;
				this.CaseData.surstime = time;
				this.CaseData.sursdate = "";
				this.CaseData.suretime = "";
				this.CaseData.suredate = "";
				this.CaseData.did = "";
				this.CaseData.repid = "";
				this.CaseData.acid = "";
				this.CaseData.prnotes = "";
				this.CaseData.voidfl = "";
				this.CaseData.type = "";
				this.CaseData.status = "";
				this.CaseData.prtcaid = "";
				this.CaseData.categorieid = "";
				this.CaseData.categoriename = "";
				this.CaseData.tmzone = "";
				this.CaseData.tmzoneid = "";
				this.CaseData.skipvalidationfl = "";
				this.CaseData.acname = "";
				this.CaseData.repname = "";
				this.CaseData.sname = "";
				this.render(this.CaseData);
			}

		},
		
		formatNumber: function(num) {
			var s = num+"";
			while (s.length < 2) s = "0" + s;
			return s;
		},

		showConfirm: function(id) {
			var that = this;
			var option;

			if(that.CaseData.status == "11091")
				option = ['Post Case','Ignore Changes','Cancel'];
			else
				option = ['Post Case','Ignore Changes','Cancel','Save Draft'];

			showNativeConfirm('What do you want to do with this case?','Leave Case Information',option, function(btnIndex) {
				if(btnIndex == 4) {
					that.$("#btn-case-save").trigger("click");
				}
				else if(btnIndex == 1) {
					that.$("#btn-case-post").trigger("click");
				}
				else if(btnIndex == 2) {
					window.location.href = "#case/detail/" + id;
				}
			});
		},

		// Render the initial contents when the Case Detail View is instantiated
		render: function (renderData) {
			var that = this;
			
			if(this.CaseData)
			console.log("Rendering with the data: " + this.CaseData);

			this.$el.html(this.template(renderData));
			this.$("#div-hours").html(this.buildTimePicker());
			this.$("#div-hours").find("#hour").val("1");
			
//			var date = new Date();
//			var dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
//			this.$("#label-case-book-surgery-date").html(mm+"/"+dd+"/"+yyyy);
//			this.$("#li-case-book-surgerydate input").val(mm+"/"+dd+"/"+yyyy);
			
			var accRenderOptions = {
				"el":this.$('#div-case-book-account-list'),
				"ID" : "txt-billing-account",
        		"Name" : "txt-search-name",
        		"Class" : "txt-keyword-account",
        		"minChar" : "3",
        		"autoSearch": true,
        		"placeholder": "Key in at least 3 characters to search",
        		"fnName": fnGetAccounts,
        		"usage": 1,
        		"template" : 'GmSearchResult',
				"template_URL" : URL_Common_Template,
				"callback": function(rowValue) {
					that.getAccount(rowValue);
					
					}
        	};
						
			var acctsearchview = new SearchView(accRenderOptions);

 		

 			var	repRenderOptions = {
				"el":this.$('#div-case-book-salesrep-list'),
				"ID" : "txt-billing-salesrep",
        		"Name" : "txt-search-name",
        		"Class" : "txt-keyword-account",
        		"minChar" : "3",
        		"autoSearch": true,
        		"placeholder": "Key in at least 3 characters to search",
        		"fnName": fnGetRep,
        		"usage": 1,
        		"template" : 'GmCaseSearchResult',
				"template_URL" : URL_Case_Template,
				"callback": function(rowValue) {that.getSalesRep(rowValue);}
        	};
						
			var repsearchview = new SearchView(repRenderOptions);

			var	distRenderOptions = {
				"el":this.$('#div-case-book-fieldsales-list'),
				"ID" : "txt-billing-fieldsales",
        		"Name" : "txt-search-name",
        		"Class" : "txt-keyword-account",
        		"minChar" : "3",
        		"autoSearch": true,
        		"placeholder": "Key in at least 3 characters to search",
        		"fnName": fnCaseGetFieldSales,
        		"usage": 1,
        		"template" : 'GmCaseSearchResult',
				"template_URL" : URL_Case_Template,
				"callback": function(rowValue) {
					that.getFieldsales(rowValue);
				}
        	};
							
			var	distsearchview = new SearchView(distRenderOptions);
			
			
			var assignRenderOptions = {
					"el":this.$('#div-case-book-assign-list'),
					"ID" : "txt-billing-assign",
            		"Name" : "txt-search-name",
            		"Class" : "txt-keyword-initial",
            		"minChar" : "3",
            		"autoSearch": true,
            		"placeholder": "Search...",
            		"fnName": fnGetRep,
            		"usage": 2,
            		"template" : 'GmCaseSearchResult',
					"template_URL" : URL_Case_Template,
					"callback": function(rowValue){
						that.getAssignFromSearchView(rowValue)
						}
            		
            	};

			var assignsearchview = new SearchView(assignRenderOptions);
			
			var shareRenderOptions = {
					"el":this.$('#div-case-book-share-list'),
					"ID" : "txt-billing-share",
            		"Name" : "txt-search-name",
            		"Class" : "txt-keyword-initial",
            		"minChar" : "3",
            		"autoSearch": true,
            		"placeholder": "Search...",
            		"fnName": fnGetUser,
            		"usage": 2,
            		"template" : 'GmCaseSearchResult',
					"template_URL" : URL_Case_Template,
					"callback": function(rowValue){
						that.getUserFromSearchView(rowValue)
						}
            		
            	};

			var sharesearchview = new SearchView(shareRenderOptions);
			
 			if(that.options.CaseID) {
 				$("#txt-billing-fieldsales").val(this.viewJSON.DIST_NAME);
 				$("#txt-billing-account").val(this.viewJSON.ACCOUNT_NM);
 				$("#txt-billing-salesrep").val(this.viewJSON.REP_NAME);
 				$("#txt-case-book-surgeon-name").val(this.CaseData.sname);
 				that.CaseData.acname = this.viewJSON.ACCOUNT_NM;
				that.CaseData.repname = this.viewJSON.REP_NAME;
 				that.$("#div-hours").find("#hour").val((that.viewJSON.hr).replace("h",""));
//				that.$("#div-hours").find("#minute").val((that.viewJSON.mn).replace("m",""));

				this.$(".div-case-book-assign-list").each( function() {
					var repid = $(this).attr("repid");
					if( that.CaseData.repid == repid) {
						$(this).find(".spn-spandiv-close").hide();
						$(this).addClass("div-case-book-assign-salesrep");
					}
					
					if($(this).attr("voidfl") == "Y") {
						that.voidAttributeList.push({"attrtp" :"103646", "attrval" : repid});
						$(this).remove();
					}
					
				});
				
				this.$(".div-case-book-share-list").each( function() {
					var userid = $(this).attr("userid");
					if($(this).attr("voidfl") == "Y") {
						that.voidAttributeList.push({"attrtp" :"103647", "attrval" : userid});
						$(this).remove();
					}
				});
				
 			}
				
 			this.getCategories();
 			this.getTimeZone();
 			this.checkStatus();
			return this;
		},
		
		searchCase: function(event) {
			var text = event.currentTarget.value.trim();
			if(text.length == 0) {
				console.log(event.currentTarget.id);
				switch(event.currentTarget.id) {
				case "txt-billing-account":
					this.CaseData.acid = "";
					this.CaseData.acname = "";
					break;
				case "txt-billing-salesrep":
					this.CaseData.repid = "";
					this.CaseData.repname = "";
					break;
				case "txt-billing-fieldsales":
					this.CaseData.did = "";
					break;
				case "txt-case-book-surgeon-name":
					this.CaseData.sname = "";
					break;
				}				
			}
			
			else {
				console.log(event.currentTarget.id);
				switch(event.currentTarget.id) {
				case "txt-case-book-surgeon-name":
					this.CaseData.sname = text;
					break;
				}
			}
		},
		
		checkStatus: function() {
			var status = this.$("#div-case-book-status").text();
			if(status !="") {
				if(status == "11093") {
					this.$("#div-case-book-status").html("Cancelled");
				}
				else if(status == "11091") {
					this.$("#div-case-book-status").html("Active");
					this.$("#btn-case-save").hide();

				}
				else if(status == "11090") {
					this.$("#div-case-book-status").html("Draft");
				}
			}
		},

		getCategories: function() {
			var that = this;
			var defaultid = 0;
			console.log(this.CaseData.categorieid);
			if( this.CaseData.categorieid !=""){
				defaultid = this.CaseData.categorieid;
			}
			
			else {
				defaultid = 0;
			}
			fnGetCategories("CASCAT",function(data){
				data.push({"ID" : 0, "Name" : "Select a Category"});
				that.dropdownview = new DropDownView({
					el : that.$("#div-case-book-categories-list"),
					data : data,
					DefaultID : defaultid
				});
				
				that.$("#div-case-book-categories-list").bind('onchange', function (e, selectedId,SelectedName) {
					that.CaseData.categorieid = selectedId;
					that.CaseData.categoriename = SelectedName;
					console.log(selectedId);
				});
			});
		},
		
		getTimeZone: function() {
			var that = this;
			var defaultName;
			var tempDate = new Date();
			var dfltText = tempDate.toTimeString().replace(')','').split('(')[1];


			switch(dfltText) {
				case "CDT":
				case "CST":
					defaultName = "Central";
					break;

				case "PDT":
				case "PST":
					defaultName = "Pacific";
					break;

				case "EDT":
				case "EST":
					defaultName = "Eastern";
					break;

				case "MDT":
				case "MST":
					defaultName = "Mountain";
					break;

				case "HST":
					defaultName = "Hawaii";
					break; 

				case "AST":
					defaultName = "Atlantic";
					break;

				case "AKDT":
				case "AKST":
					defaultName = "Alaska";
					break;

				default:
					defaultName = "Eastern";
					break;
			}

			if(this.state!="" && this.state != undefined) {
				fnGetStateForAccount(this.state, function(result){
					var state = result[0].Name;
					$.get("globusMobileApp/datafiles/timezone.json",function(JSONData) {
						if(typeof(JSONData)!="object")
							JSONData = $.parseJSON(JSONData);

						var timezone = _.findWhere(JSONData,{"State":  state});

						defaultName = timezone.Zone;
						that.fnShowTimeZone(defaultName);
					});
				});
			}

			else if( this.CaseData.tmzone !="") {
				var zone = that.CaseData.tmzone;
				fnGetTimeZoneID(zone,function(data){
					defaultName = data[0].Name;
					that.fnShowTimeZone(defaultName);
				});
			}

			else {
				this.fnShowTimeZone(defaultName);
			}
		},

		fnShowTimeZone: function(defaultName) {
			var that = this;
			fnGetCategories("TMZONE",function(data){
				if(that.zoneDropdownView)
						that.zoneDropdownView.close();

				that.zoneDropdownView = new DropDownView({
					el : that.$("#div-time-zone-list"),
					data : data,
					DefaultName : defaultName
				});
				 
				that.CaseData.tmzoneid = (that.zoneDropdownView.getCurrentValue())[0];
				that.CaseData.timezonename = defaultName;
				fnGetTimeZone(that.CaseData.tmzoneid, function(data){
					that.CaseData.tmzone = data[0].ZONE;
					that.getDuration();
				});
								
				that.$("#div-time-zone-list").bind('onchange', function (e, selectedId,SelectedName) {
					that.CaseData.tmzoneid = selectedId ;
					that.CaseData.timezonename = SelectedName;
					fnGetTimeZone(selectedId, function(data){
						that.CaseData.tmzone = data[0].ZONE;
						that.getDuration();
					});
					console.log(selectedId);
				});
			});
		},
		
		removeSalesRep :function(event){
			
			var parentid = $(event.currentTarget).parent().attr("id");
			console.log(parentid);
			var attributetype;
			if(this.CaseData.cainfoid != "" && this.CaseData.status!="11090") {
				if(parentid.indexOf("assign") >= 0) {
					attributetype = "103646";
					var repid = $(event.currentTarget).parent().attr("repid");
					for(var i=0; i<this.viewJSON.assignto.length; i++) {
						if(this.viewJSON.assignto[i].ID == repid) {
							this.voidAttributeList.push({"attrtp" : attributetype, "attrval" : repid});
						}
					}
				}
				else {
					attributetype = "103647";
					var userid = $(event.currentTarget).parent().attr("userid");
					for(var i=0; i<this.viewJSON.shareto.length; i++) {
						if(this.viewJSON.shareto[i].ID == userid) {
							this.voidAttributeList.push({"attrtp" : attributetype, "attrval" : userid});
						}
					}
				}
			} 
			
			$(event.currentTarget).parent().remove();					
			
		},

		
		getDuration: function() {
			var hours = $("#div-hours").find("#hour").val();
//			var minutes = $("#div-hours").find("#minute").val();
			console.log(hours);
			
			if( this.CaseData.surstime != "" && this.CaseData.surdt != "") {
				var dd,mm,yyyy;
				var date = this.getGivenDate();
				var time = this.formatNumber(date.getHours())+":"+this.formatNumber(date.getMinutes());
				this.CaseData.surstime = time;
				
				dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
				var month = new Array();
			    month[0] = "January"; month[1] = "February";month[2] = "March"; month[3] = "April"; month[4] = "May"; month[5] = "June"; month[6] = "July"; month[7] = "August"; month[8] = "September"; month[9] = "October"; month[10] = "November"; month[11] = "December";
			    mm = month[date.getMonth()];
			    this.CaseData.sursdate = mm+" "+dd+", "+yyyy;
				var starttimehours = date.getHours();
				var starttimeminutes = date.getMinutes();
				var endtimeminutes = parseInt(starttimeminutes);
				var endtimehours = parseInt(starttimehours)+ parseInt(hours);
				
				console.log(endtimeminutes);
				console.log(endtimehours);
				
				console.log(date.toString());
				if( endtimehours >= 24) {
					endtimehours = endtimehours - 24;			
					date = new Date(date.getTime() + 24 * 60 * 60 * 1000);
				 }
				dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
				endtimehours = (endtimehours.toString().length == 1) ? "0" + endtimehours : endtimehours;
				endtimeminutes = (endtimeminutes.toString().length == 1) ? "0" + endtimeminutes : endtimeminutes;
				console.log(this.CaseData);
				this.CaseData.suretime = yyyy+mm+dd+"T"+endtimehours+endtimeminutes+"00";
				
				mm = month[date.getMonth()];
				this.CaseData.suredate = mm+" "+dd+", "+yyyy+" "+endtimehours+":"+endtimeminutes+":00";
				
				console.log(this.CaseData.suredate);
				console.log(this.CaseData.suretime);
				console.log(this.CaseData);
				
			}
						
		},

		
			 
		 buildTimePicker:function(){
	
			 var result = document.createElement('span');
		     var hours = document.createElement('select');
		     hours.setAttribute('id', 'hour');
		     for (var h=0; h<6; h++) {
		         var option = document.createElement('option');
		         option.setAttribute('value', h);
		         option.appendChild(document.createTextNode(h + 'h'));
		         hours.appendChild(option);
		     }
		     // var minutes = document.createElement('select');
		     // minutes.setAttribute('id', 'minute');
		     // for (var m=0; m<60; m++) {
		     //     var option = document.createElement('option');
		     //     option.setAttribute('value', m);
		     //     option.appendChild(document.createTextNode(m + 'm'));
		     //     minutes.appendChild(option);
		     // }
		     result.appendChild(hours);
		     // result.appendChild(document.createTextNode(" : "));
		     // result.appendChild(minutes);
		     
		     console.log(result);
		     return result;
	
		},
		
		
		getUserFromSearchView: function (rowValue) {
			var arrSelectedData = new Array();
			
			if(rowValue.length==undefined)
				arrSelectedData.push(rowValue);
			else
				arrSelectedData = rowValue;
			
			$("#div-case-book-share").find("input").val("");
			if(arrSelectedData.length>0) {
				for(var i=0; i<arrSelectedData.length; i++) {
					$("#div-case-book-share").find("#span-share-append-list").append('<div class="div-case-book-share-list" id=div-share-append-'+arrSelectedData[i].ID+' userid='+arrSelectedData[i].ID+'> <span>'+arrSelectedData[i].Name+'</span><div class="spn-spandiv-close fa fa-times-circle fa-lg"></div></div>');
				}
			}		
		},
		
		getAssignFromSearchView: function (rowValue) {
			var arrSelectedData = new Array();
			
			if(rowValue.length==undefined)
				arrSelectedData.push(rowValue);
			else
				arrSelectedData = rowValue;
			
			$("#div-case-book-assign").find("input").val("");
			
			if(arrSelectedData.length>0) {
				for(var i=0; i<arrSelectedData.length; i++) {
					$("#div-case-book-assign").find("#span-assign-append-list").append('<div class="div-case-book-assign-list" id=div-assign-append-'+arrSelectedData[i].ID+' repid='+arrSelectedData[i].ID+'> <span>'+arrSelectedData[i].Name+'</span><div class="spn-spandiv-close fa fa-times-circle fa-lg"></div></div>');
				}
			}			
		},
		
		
		saveDraft: function() {
			if(this.CaseData.caseid != "" ) {
				var that = this;
				this.getDuration();
				this.getAttributeData();
				console.log("Save Draft");
				console.log("Save Case with the data: " + this.CaseData);
				if(that.CaseData.status=="11090" ){
					fnDeleteCaseLocalTable(this.CaseData.cainfoid, function(){
						fnSaveCaseLocal(that.CaseData, '11090', function(id) {
							console.log(id);
							window.location.href = "#case/detail";
						});
					});
				}
				else{
					fnSaveCaseLocal(this.CaseData, '11090', function(id) {
						console.log(id);
						that.initialize(that.options);
					});				
				}
				
			}
			else {
				fnShowStatus("Please provide Account Informaion");
			}
		},

		deleteCase: function() {
			var that = this;
			showNativeConfirm("This action will change the status of this Case to Cancelled.\nDo you want to proceed?","Confirm",["Yes","No"], function(index) {
				if(index==1) {
					that.getDuration();
					that.getAttributeData();
					if (that.CaseData.cainfoid!="" && that.CaseData.status=="11090"){
						fnDeleteCaseLocalTable(that.CaseData.cainfoid, function(){
							window.location.href = "#case/detail";
						});
					}
					
					else if (that.CaseData.cainfoid!="" && that.CaseData.status=="11091"){
						that.status = "close";
						fnRemoveCaseDetails(that.CaseData.cainfoid, function(){
							fnSaveCaseDetailsLocal(that.CaseData, '11093', function(id) {
								that.localcainfoid = id;
								if((navigator.onLine)){
									that.CaseData.status = "11093";
								 	that.bookCaseInfo(that.CaseData);														
								 }	
								if((!navigator.onLine)){
									that.CaseData.status = "11093";
									fnSaveCase(that.localcainfoid, JSON.stringify(that.CaseData), function() {
										window.location.href = "#case/detail";
									});											
								}
							});
						});
					}
					
					else if(that.CaseData.cainfoid!="" && that.CaseData.status=="Pending Sync") {
						fnDeleteCaseLocal(that.CaseData.cainfoid, function(){
							window.location.href = "#case/detail";
						});
					}
					else if(that.CaseData.status=="") {
						that.initialize(that.options);
					}
				}
			});
		},


		postCase: function() {			
			if(this.validate()){
			this.loaderview = new LoaderView({text : "Posting Case..."});
				var that = this;
				console.log(this.CaseData.caseid);
				console.log(this.CaseData.cainfoid);
				this.getDuration();
				this.getAttributeData();
				if(that.CaseData.status=="11090" ){
					fnDeleteCaseLocalTable(this.CaseData.cainfoid, function(){});
				}
				console.log("posting");
				console.log("Posting Case with the data: " + this.CaseData);	
				if (this.CaseData.cainfoid!="" && this.CaseData.status!="11090"){
					this.status = "update";
					fnRemoveCaseDetails(this.CaseData.cainfoid, function(){
						fnSaveCaseDetailsLocal(that.CaseData, 'Pending Sync', function(id) {
							that.localcainfoid = id;
							if((navigator.onLine)){
								that.CaseData.status = "11091";
							 	that.bookCaseInfo(that.CaseData);														
							 }								
							if((!navigator.onLine)){
								that.CaseData.status = "11091";
								// fncreateEvent("update", id, id, that.CaseData);
								fnSaveCase(that.localcainfoid, JSON.stringify(that.CaseData), function() {
									that.loaderview.close();
									if(location.hash == "#case/detail")
										that.initialize(that.options);
									else
										window.location.href = "#case/detail";
								});											
							} 
						});
					});
				}
				else {
					fnSaveCaseLocal(this.CaseData, 'Pending Sync', function(id) {
						console.log(id);
						that.localcainfoid = id;					
						console.log(that.CaseData);
							if((navigator.onLine)){
								that.CaseData.caseid = "";
								that.CaseData.cainfoid = "";
								that.CaseData.status = "11091";
							 	that.bookCaseInfo(that.CaseData);														
							 }								
							if((!navigator.onLine)){
								that.CaseData.caseid = "";
								that.CaseData.status = "11091";
								// fncreateEvent("new", id, "", that.CaseData);
								fnSaveCase(that.localcainfoid, JSON.stringify(that.CaseData), function() {
									that.loaderview.close();
									if(location.hash == "#case/detail")
										that.initialize(that.options);
									else
										window.location.href = "#case/detail";
								});											
							}  				
					});
				}
				
			}														
		},
		
		
		getAttributeData: function() {	
			var that = this;
			var hours = $("#div-hours").find("#hour").val();
//			var minutes = $("#div-hours").find("#minute").val();
			var duration = hours+"h";
			
			this.CaseData.arrGmCaseAttributeVO = new Array();
			if(this.CaseData.cainfoid!="" && this.CaseData.status!="11090") {
				this.CaseData.arrGmCaseAttributeVO.push({"caattrid" :this.viewJSON.durationattrid, "cainfoid" : this.CaseData.cainfoid, "attrtp" : "4000541", "attrval" : duration, "voidfl" : ""});
				this.CaseData.arrGmCaseAttributeVO.push({"caattrid" :this.viewJSON.tmzoneattrid, "cainfoid" : this.CaseData.cainfoid, "attrtp" : "4000534", "attrval" : this.CaseData.tmzone, "voidfl" : ""});
				this.CaseData.arrGmCaseAttributeVO.push({"caattrid" :this.viewJSON.snameattrid, "cainfoid" : this.CaseData.cainfoid, "attrtp" : "103640", "attrval" : this.CaseData.sname, "voidfl" : ""});
				this.CaseData.arrGmCaseAttributeVO.push({"caattrid" :this.viewJSON.casecloseattrid, "cainfoid" : this.CaseData.cainfoid, "attrtp" : "4000666", "attrval" : "104420", "voidfl" : ""});
			}
			else {
				this.CaseData.arrGmCaseAttributeVO.push({"caattrid" :"", "cainfoid" : "", "attrtp" : "4000541", "attrval" : duration, "voidfl" : ""});
				this.CaseData.arrGmCaseAttributeVO.push({"caattrid" :"", "cainfoid" : "", "attrtp" : "4000534", "attrval" : this.CaseData.tmzone, "voidfl" : ""});
				this.CaseData.arrGmCaseAttributeVO.push({"caattrid" :"", "cainfoid" : "", "attrtp" : "103640", "attrval" : this.CaseData.sname, "voidfl" : ""});
				this.CaseData.arrGmCaseAttributeVO.push({"caattrid" :"", "cainfoid" : "", "attrtp" : "4000666", "attrval" : "104420", "voidfl" : ""});
			}			
			this.sharedEmails = new Array();
			this.$(".div-case-book-assign-list").each( function() {
				var repid = $(this).attr("repid");
				var attributetype = "103646";
				if(that.CaseData.cainfoid!="" && that.CaseData.status!="11090") {
					that.CaseData.arrGmCaseAttributeVO.push({"caattrid" :"", "cainfoid" : that.CaseData.cainfoid, "attrtp" : attributetype, "attrval" : repid, "voidfl" : ""});
					var searchvalue = {"attrtp" : attributetype, "attrval" : repid};
					   _.each(that.voidAttributeList, function(data, idx) { 
					      if (_.isEqual(data, searchvalue)) {
					    	  that.voidAttributeList.splice(idx,1);
					      }
					   });
				}
				else {
					that.CaseData.arrGmCaseAttributeVO.push({"caattrid" :"", "cainfoid" : "", "attrtp" : attributetype, "attrval" : repid, "voidfl" : ""});	
				}				
//				fnGetEmailID(repid,function(data){
//					that.sharedEmails.push(data[0].ID);
//				});
				
			});
			
			this.$(".div-case-book-share-list").each( function() {
				var userid = $(this).attr("userid");
				var attributetype = "103647";
				if(that.CaseData.cainfoid!="" && that.CaseData.status!="11090") {
					that.CaseData.arrGmCaseAttributeVO.push({"caattrid" :"", "cainfoid" : that.CaseData.cainfoid, "attrtp" : attributetype, "attrval" : userid, "voidfl" : ""});
					var searchvalue = {"attrtp" : attributetype, "attrval" : userid};
					   _.each(that.voidAttributeList, function(data, idx) { 
					      if (_.isEqual(data, searchvalue)) {
					    	  that.voidAttributeList.splice(idx,1);
					      }
					   });

				}
				else {
					that.CaseData.arrGmCaseAttributeVO.push({"caattrid" :"", "cainfoid" : "", "attrtp" : attributetype, "attrval" : userid, "voidfl" : ""});
				}
				
//				fnGetEmailID(userid,function(data){
//					that.sharedEmails.push(data[0].ID);
//				});
			});
			
			fnGetCategories("CASCAT",function(data){
				console.log(data);	
				console.log(that.CaseData.categorieid);
				if(data.length > 0){
					for(var i=0; i<data.length; i++){
						if(that.CaseData.categorieid == data[i].ID) {
							if(that.CaseData.cainfoid!="" && that.CaseData.status!="11090") {
								that.CaseData.arrGmCaseAttributeVO.push({"caattrid" :"", "cainfoid" :that.CaseData.cainfoid, "attrtp" : data[i].ID, "attrval" :"Y", "voidfl" : ""});
							}
							else {
								that.CaseData.arrGmCaseAttributeVO.push({"caattrid" :"", "cainfoid" :"", "attrtp" : data[i].ID, "attrval" :"Y", "voidfl" : ""});
							}
								
						}
						else {
							if(that.CaseData.cainfoid!="" && that.CaseData.status!="11090") {
								that.CaseData.arrGmCaseAttributeVO.push({"caattrid" :"", "cainfoid" :that.CaseData.cainfoid, "attrtp" : data[i].ID, "attrval" : "Y", "voidfl" : "Y"});
							}
							else {
								that.CaseData.arrGmCaseAttributeVO.push({"caattrid" :"", "cainfoid" :"", "attrtp" : data[i].ID, "attrval" : "Y", "voidfl" : "Y"});
							}							
						}
					}
				}
				
			});
			
			if(this.voidAttributeList.length>0){
				for(var i=0; i<this.voidAttributeList.length; i++) {
					this.CaseData.arrGmCaseAttributeVO.push({"caattrid" :"", "cainfoid" : this.CaseData.cainfoid, "attrtp" : this.voidAttributeList[i].attrtp, "attrval" : this.voidAttributeList[i].attrval, "voidfl" : "Y"});
//					fnGetEmailID(this.voidAttributeList[i].attrval, function(data){
//						that.sharedEmails.push(data[0].ID);
//					});
				}
			}			
			
		},

		validate: function() {
			
			this.assignElements = document.getElementsByClassName('div-case-book-assign-list');

			if(this.CaseData.surdt == "") {
				fnShowStatus("Please provide Surgery Date");
				return false;
			}
			else if(this.CaseData.surstime == "") {
				fnShowStatus("Please provide Surgery Time");
				return false;
			}
			else if(this.CaseData.sname == "") {
				fnShowStatus("Please provide Surgeon name");
				return false;
			}
			else if(this.CaseData.acid == "") {
				fnShowStatus("Please provide Account Informaion");
				return false;
			}
			else if(this.CaseData.repid == "") {
				fnShowStatus("Please provide Sales Rep Information");
				return false;
			}
			else if(this.CaseData.did == "") {
				fnShowStatus("Please provide Field Sales Information");
				return false;
			}
			else if(!this.assignElements.length>0) {
				fnShowStatus("Please provide  Assign To Information");
				return false;
			}
			else if(this.CaseData.tmzone == "" || this.CaseData.tmzoneid == 0) {
				fnShowStatus("Please Select the Time Zone");
				return false;
			}
			else if(this.CaseData.categorieid == ""){
				fnShowStatus("Please Select the Category");
				return false;
			}
			else {
				return true;
			}
		},
		
		
		bookCaseInfo: function(CaseData){
			var that = this;
			this.CaseData = CaseData;
			fnPostCase (CaseData,this.localcainfoid, function(data) {
				if(that.loaderview)
					that.loaderview.close();
				that.CaseData.caseid = data.caseid;
				that.CaseData.cainfoid = data.cainfoid;
				that.CaseData.status = data.status;
				console.log(data.caseid);
				console.log(data.cainfoid);
				if(that.status == "update") {
//					fncreateEvent("update", data.cainfoid, data.cainfoid, that.CaseData);	
//					that.createICSFile("REQUEST",that.CaseData,"Update");
					window.location.href = "#case/detail";
				}
				else if(that.status == "close") {
//					fncreateEvent("delete", data.cainfoid, "", that.CaseData);
//					that.createICSFile("CANCEL",that.CaseData,"Delete");
					 window.location.href = "#case/detail";
//					that.initialize(that.options);
				}
				else{
//					fncreateEvent("new", data.cainfoid, "", that.CaseData);	
//					that.createICSFile("REQUEST",that.CaseData,"Create");
					if(location.hash == "#case/detail")
						that.initialize(that.options);
					else
						window.location.href = "#case/detail";
				}
				
			});
		},
		
		
		createICSFile: function(method, data, status) {
			var that = this;
			console.log(data);
			var date = new Date(data.sursdate);
			var dd, mm, yyyy;
			dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
			var surstime = data.surstime+"00";
			surstime = surstime.replace(":","");
			var suretime = data.suretime;
			console.log(surstime);
			console.log(data.suretime);
			var subject = data.sname+" - "+data.categoriename;
			var location = data.acname;
			var toemail = this.sharedEmails;
			console.log(this.sharedEmails);
			console.log(yyyy+mm+dd+"T"+surstime);
			console.log(suretime);
			console.log(toemail);
			
			var sequence = new Date();
			sequence = parseInt(((sequence.getUTCFullYear()).toString()).slice(-2))+that.formatNumber(sequence.getUTCMonth()+1)+that.formatNumber(sequence.getUTCDate())+that.formatNumber(sequence.getUTCHours())+that.formatNumber(sequence.getUTCMinutes());
			
			var userId = localStorage.getItem("userID");
			
			fnGetEmailID (userId, function(emailid) {
				var content = "BEGIN:VEVENT"+"\n"+"UID:"+data.caseid+"\n"+"SEQUENCE:"+sequence+"\n"+"DTSTAMP:"+yyyy+mm+dd+"T075000Z"+"\n"+"ORGANIZER:"+emailid[0].ID+"\n"+"DTSTART:"+yyyy+mm+dd+"T"+surstime+"\n"+"DTEND:"+suretime+"\n"+"SUMMARY:"+subject+"\n"+"LOCATION:"+location+"\n"+"STATUS:CONFIRMED"+"\n"+"END:VEVENT"+"\n";
				console.log(content);
				fnSendICSFile(method,content,toemail,data.caseid,status);
			});									
	  	},

		
		showDatepicker: function (event) {
			$("#li-case-book-surgerydate .sdate").datepicker( {minDate: 0, duration: 'fast'});
			var visible = $('#ui-datepicker-div').is(':visible');
			   console.log(visible);
			$("#li-case-book-surgerydate .sdate").datepicker(visible? 'hide' : 'show');

			event.stopPropagation();
			$(window).click(function (event) {
			    if(document.getElementById("ui-datepicker-div").style.display != "none") {
			    	var elem = event.target;
			       if(!($(elem).hasClass("ui-datepicker-prev") || $(elem).hasClass("ui-datepicker-next") || $(elem).parent().hasClass("ui-datepicker-prev") || $(elem).parent().hasClass("ui-datepicker-next"))) {
			    	   var visible = $('#ui-datepicker-div').is(':visible');
			    	   $("#li-case-book-surgerydate .sdate").datepicker(visible? 'hide' : 'show');
			       }
			    }
			});

		},
		
		updateNotes: function(event) {
			this.CaseData.prnotes = $(event.currentTarget).val();
		},

		updateCase: function() {
			console.log("Update Case");
			this.CaseTeam =[];

			this.CaseData.surdt = this.$("#li-case-book-surgerydate input").val();
			this.CaseData.surstime = this.$("#li-surgery-time #txt-case-book-surgery-stime").val();
			this.getDuration();
		},
		
		getSurgeryDate: function() {
			$("#label-case-book-surgery-date").html($("#li-case-book-surgerydate input").val());			  
		},
		
		getAccount: function(rowValue) {
			this.CaseData.acid = rowValue.ID;
			$("#txt-billing-account").val(rowValue.Name);
			this.CaseData.acname = rowValue.Name;
			this.getSalesRep({"ID" : $(rowValue.Element).attr("repid"), "Name" :  $(rowValue.Element).attr("repname")});
			this.state = $(rowValue.Element).attr("state");
			this.getTimeZone();
			
		},

		getSalesRep: function(rowvalue) {
			var that = this;
			this.CaseData.repid = rowvalue.ID;
			$("#div-itemlist").hide();
			$("#txt-billing-salesrep").val(rowvalue.Name);
			this.CaseData.repname = rowvalue.Name;

			this.assignSalesRepElements = document.getElementsByClassName('div-case-book-assign-salesrep');
			if(!this.assignSalesRepElements.length>0) {
				$("#div-case-book-assign").find("#span-assign-append-list").append('<div class="div-case-book-assign-list div-case-book-assign-salesrep" id=div-assign-append-'+rowvalue.ID+' repid='+rowvalue.ID+'> <span>'+rowvalue.Name+'</span></div>');
			}
			else{
				$("#div-case-book-assign").find(".div-case-book-assign-salesrep").html('<span>'+rowvalue.Name+'</span>');
				$("#div-case-book-assign").find(".div-case-book-assign-salesrep").attr("repid",rowvalue.ID);
				$("#div-case-book-assign").find(".div-case-book-assign-salesrep").attr("id","div-assign-append-"+rowvalue.ID);
			}
			console.log("This is the Case Data: " + JSON.stringify(this.CaseData) + " with Case ID: " + this.CaseData.caseid);

			var date, dd, mm, yyyy;

			

			date = new Date(this.$("#label-case-book-surgery-date").text());
			dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
			var strCaseID = rowvalue.ID + "-" + yyyy + mm + dd + "-";

			fnCheckCaseID(strCaseID, function(data) {
				console.log(data);
				var currSeq = (data.length)?(_.max(_.pluck(data,'CASE_ID'), function(id){ return parseInt(id.split('-')[3]); })).split('-')[3]:0;
				strCaseID = "D-" + strCaseID + (parseInt(currSeq)+1);
				console.log(strCaseID);
				that.$("#div-case-id").text(strCaseID);
				that.CaseData.caseid = strCaseID;
				that.updateCase();
			});
			
			fnGetFieldSalesID(this.CaseData.repid,function(data){
				fnGetDistributorInfo(data[0].ID, function(result){
					that.getFieldsales({"ID" : result[0].ID, "Name" : result[0].Name});
				});
			});
			
		},

		getGivenDate: function() {


			if((this.zoneDropdownView!=undefined) && (this.zoneDropdownView.getCurrentValue()[0]!=0)) {
				var timeZone = eval(this.zoneDropdownView.getCurrentValue()[1]);
				var givenDate = this.$("#label-case-book-surgery-date").text();
				var givenTime = this.$("#txt-case-book-surgery-stime").val();

				var finalDate = new Date(givenDate + " " + this.$("#txt-case-book-surgery-stime").val());
				var zone = this.CaseData.timezonename;
				var city;
				if(this.state == 1021 && zone == "Mountain")
					city = TimeZones["Arizona"];
				else
					city = TimeZones[zone];

				var offsetvalue = finalDate.dst(city);
				var zoneoffsetvalue  = parseInt(timeZone*60);
				if(offsetvalue != zoneoffsetvalue) {
					timeZone = "-" + (timeZone-1) + ".00"; 
					$("#div-time-zone-offset").html("D");
				}
				else {
					timeZone = "-" + timeZone + ".00"; 
					$("#div-time-zone-offset").html("S");
				}
				
				console.log("getGivenDate");
				var givenOffsetMinutes = parseFloat(timeZone)*(-60);
				console.log(givenOffsetMinutes);

				var offsetMinutes = givenOffsetMinutes + (finalDate.getTimezoneOffset()*-1);

				console.log(offsetMinutes);

				finalDate.setMinutes(finalDate.getMinutes() + offsetMinutes); //Local time exatly for the given time in the given timezone
				return 	finalDate; 
			}
			else
				return new Date();
		},

		
		getFieldsales: function(rowvalue) {
			this.CaseData.did = rowvalue.ID;
			$("#div-itemlist").hide();
			$("#txt-billing-fieldsales").val(rowvalue.Name);
			console.log("This is the Case Data: " + JSON.stringify(this.CaseData) + " with Case ID: " + this.CaseData.caseid);
			this.updateCase();
		}
		
	});	

	return CaseDetailView;
});
