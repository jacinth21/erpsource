/**********************************************************************************
 * File:        GmCaseMainView.js
 * Description: Initial Case View to load other sub views
 * Version:     1.0
 * Author:     	ejayabalan
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification', 'casedetailview', 'casereportview', 'casescheduleview'
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification, CaseDetailView, CaseReportView, CaseScheduleView) {

	'use strict';
	var CaseMainView = Backbone.View.extend({

		events : {
			"click #div-schedule-case": "toggleScheduleCases",
			"click #btn-case-link" : "openLink"
		},

		/* Load the templates */ 
		template: fnGetTemplate(URL_Case_Template, "GmCaseMain"),

		initialize: function(options) {
			console.log("Came into CaseMainView initialize");
			$("body").removeClass();
			$("#btn-home").show();
			$("#btn-back").show();
			$("#div-app-title").html(lblNm("case_scheduler"));
			this.testvar = 1;
			
			this.strViewType = options[0];
			this.CaseID = options[1];
			
			if(this.CaseID) {
				console.log("Came into the Main View with Case ID: " + this.CaseID);	
			} else {
				console.log("No Case Data");
				this.CaseID = "";
			}
			
			$(window).on('orientationchange', this.onOrientationChange);
			

			this.render();
		},

		// Render the initial contents when the Case View is instantiated
		render: function () {
//			showMsgView("011");
			this.$el.html(this.template());	
            if(GM.Global.DoValidation != undefined ){
                showError(GM.Global.DoValidation);
                GM.Global.DoValidation = undefined;
            }
			
			this.$("#btn-case-schedule").attr("href", "#case");
			this.$("#btn-case-book").attr("href", "#case/detail");
			this.$("#btn-case-report").attr("href", "#case/report");

			if(!this.strViewType) {
				this.strViewType = "schedule";
			}
					switch(this.strViewType) {
					case "detail":
						console.log("Came into Detail");
						this.casedetailview = new CaseDetailView({'el' :this.$('#div-case-detail'), 'parent' : this, 'CaseID' : this.CaseID});
						if(this.CaseID) {
							this.$("#div-case-title").html("Edit Case");
							this.display("book");	
						} else {
							this.$("#div-case-title").html("Book Case");
							this.display("book");
						}
						this.$("#div-schedule-case").show();
						break;	
					case "schedule":
						console.log("Came into Schedule");
						this.casescheduleview = new CaseScheduleView({'el' :this.$('#div-case-schedule'), 'parent' : this});
						this.$("#div-case-title").html("Cases Scheduled");
						this.display("schedule");
						this.$("#div-schedule-case").hide();
						break;	
					case "report":
						this.casereportview = new CaseReportView({'el' :this.$('#div-case-report'), 'parent' : this});
						this.$("#div-case-title").html("Report");
						this.$("#div-schedule-case").hide();
						this.display("report");
						break;	
				}
$("#div-footer").addClass("hide");
            $('#div-app-title').removeClass('gmVisibilityHidden').addClass('gmVisibilityVisible');
             //$("#div-app-title").removeClass("gmMediaVisibilityH").addClass("gmMediaVisibilityV");
			return this;
		},
		
		// Toggles the view
		display: function(key) {

			if(key.currentTarget==undefined) {
				this.key = key;
			}
			else {
				this.key = "schedule";
				
			}
			this.$(".btn-detail").each(function() {
				$(this).removeClass("btn-selected");
			});

			this.$("#div-case-" + this.key).show();
			this.$("#btn-case-" + this.key).addClass("btn-selected");

		},

		toggleContent: function (event) {
			var id = (event.currentTarget.id).replace("div-case-detail-label-","");
			this.$(".selected-tab").removeClass("selected-tab");
			$(event.currentTarget).addClass("selected-tab");
		},
		

		
		toggleScheduleCases: function() {
			if($(".div-case-detail-schedule").css("display") == "none") {
				this.$("#div-schedule-case").addClass("div-schedule-case-selected");
				$(".div-case-detail-schedule").css("display","inline-block");
				$(".div-case-detail-schedule").animate({
                    left: "-=550"
				},100, function() {
				});
//				$(".div-case-detail-schedule").css("left","724px");
			}
			else {
				$(".div-schedule-case-selected").removeClass("div-schedule-case-selected");
				$(".div-case-detail-schedule").animate({
                    left: "+=550"
				},100, function() {
					$(".div-case-detail-schedule").css("display","none");
				});
				
//				$(".div-case-detail-schedule").hide();
			}
		},
		
		onOrientationChange: function() {
			switch(window.orientation) {  
			case -90:
			case 90:
				if($(".div-case-detail-schedule").css("display") != "none") {
					$('.div-case-detail-schedule').css({left: 474});
				}
				else {
					$('.div-case-detail-schedule').css({left: 1024}); 
				}
				break; 

			case -180:
			case 180:
			case 0:
			case 0:
				if($(".div-case-detail-schedule").css("display") != "none") {
					$('.div-case-detail-schedule').css({left: 218});
				}
				else {
					$('.div-case-detail-schedule').css({left: 768}); 
				}
			break; 


			default:
				if($(".div-case-detail-schedule").css("display") != "none") {
					$('.div-case-detail-schedule').css({left: 218});
				}
				else {
					$('.div-case-detail-schedule').css({left: 768}); 
				}
			break; 
			}   
		},
		
		openLink: function() {
			$("#btn-case-link").each(function(){	
	            	$(this).toggleClass("show-app");
			})
				$(".btn-app-link").toggle("fast");
		},
		
		// Close the view and unbind all the events
		close: function(e) {

			if(this.casedetail)
				this.casedetail.close();
			this.unbind();
			this.remove();

		}

	});	

	return CaseMainView;
});
