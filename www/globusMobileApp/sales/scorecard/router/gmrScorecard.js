/**********************************************************************************
 * File:        gmrScorecard.js
 * Description: Router for all of Scorecard
 * Version:     1.0
 * Author:      mahendrand
 **********************************************************************************/
define([

  // External Libraries
  'jquery', 'underscore', 'backbone',


  //Views
   'globusMobileApp/sales/scorecard/view/gmvScorecardRpt.js'
],

    function ($, _, Backbone,
        GMVScorecardRpt) {

        'use strict';

        GM.Router.Scorecard = Backbone.Router.extend({

            //routes to its respective web page 
            routes: {
                "scorecard": "scorecard",
                "scorecard/:report": "viewReport",
                "scorecard/:report/:drilldowntype/:id": "drilldownReport"
            },


            initialize: function () {
            },

            // Routes to the scorecard default to AD Report
            scorecard: function () {
                var that=this;
                that.viewReport("overallsummary");
            },
            
            // Routes to the scorecard Reports
            viewReport: function (report) {
                var input={stropt: report};
                var gmvScorecardRpt = new GMVScorecardRpt({
                    view: input
                });
                gmvApp.showView(gmvScorecardRpt);
            },
            
            // Routes to the DrillDown Reports for the AD, FieldSales, System
            drilldownReport: function (report,drilldowntype,id) {
                var input={};
                switch (drilldowntype){
                    case 'ad':
                        input={
                            stropt: report,
                            adid: id
                        };
                        break;
                    case 'fieldsales':
                        input={
                            stropt: report,
                            distributorid: id,
                            distid: id
                        };
                        break;
                    case 'system':
                        input={
                            stropt: report,
                            systemid: id
                        };
                        break;
                }
                var gmvScorecardRpt = new GMVScorecardRpt({
                    view: input
                });
                gmvApp.showView(gmvScorecardRpt);
            },


        });
        return GM.Router.Scorecard;
    });