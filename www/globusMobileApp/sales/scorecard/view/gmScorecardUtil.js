/**********************************************************************************
 * File:        gmScorecardUtil.js
 * Description: Common function usage for Scorecard 
 * Version:     1.0
 * Author:      mahendrand
 **********************************************************************************/

// Common Function To Display DTMLX Grid, Following Parameters as Input
// divRef - Id of the element, where the grid has to be displayed
// gridHeight - Height of the Grid, if it is empty then as 500
// setHeader - Array, Grid Header Data
// setInitWidths - String, Width of Each Column in the Grid
// setColAlign - String, Alignment data of Each Column in the Grid (right,left,center,justify)
// setColTypes - String, Type of Data in Each Column in the Grid(ro-read only, ed - editable, txt- text, txttxt- plain text etc.. )
// setColSorting - String, Sort based on type of data in Each Column in the Grid(str, int, date, na or function object for custom sorting)
// enableTooltips - String, True/False to enable or disable tooltips
// setFilter - String, filters like #text_filter, #numeric_filter,..
// footerArry - Array, Grid Footer Data
// footerStyles - Array, Footer Data Styles
// Returns Grid Object gObj

function initDHTMLXGrid(divRef, gridHeight, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, setFilter, footerArry, footerStyles) {


    var gObj = new dhtmlXGridObject(divRef);
    gObj.enableColSpan(true);
    gObj.setImagePath("globusMobileApp/lib/dhtmlx/dhtmlxGrid/imgs/");
    if (setHeader != undefined && setHeader.length > 0) {
        gObj.setHeader(setHeader);
        var colCount = setHeader.length;
    }
    if (setInitWidths != "")
        gObj.setInitWidths(setInitWidths);
    if (setColAlign != "")
        gObj.setColAlign(setColAlign);
    if (setColTypes != "")
        gObj.setColTypes(setColTypes);
    if (setColSorting != "")
        gObj.setColSorting(setColSorting);
    if (enableTooltips != "")
        gObj.enableTooltips(enableTooltips);
    if (setFilter != "")
        gObj.attachHeader(setFilter);
    if (footerArry != undefined && footerArry.length > 0) {
        var footstr = eval(footerArry).join(",");
        if (footerStyles != undefined && footerStyles.length > 0)
            gObj.attachFooter(footstr, footerStyles);
        else
            gObj.attachFooter(footstr);
    }
    if (gridHeight != "")
        gObj.enableAutoHeight(true, gridHeight, true);
    else
        gObj.enableAutoHeight(true, 500, true);
    gObj.init();
    return gObj;

}

//Format the Column Cell Values as the Given Format for the Given array of Columns
function formatDHTMLXColumnVal(format, colArr, digit, separator, gridObj) {
    colArr.forEach(function (item) {
        gridObj.setNumberFormat(format, item, digit, separator);
    });
}

//If the Column cell has Negative Value then Change the Color to Red
function fnNegativeNumberUpd(scoreVal, rowId, scoreCellId, gridObj) {
    if (scoreVal < 0) {
        gridObj.setCellTextStyle(rowId, scoreCellId, "color:red;");
    }
}

//Hide DHTMLX Column for the given Column Array Index
function hideDHTMLXColumn(colArr, gridObj) {
    colArr.forEach(function (item) {
        gridObj.setColumnHidden(item,true);
    });
}
//Show DHTMLX Column for the given Column Array Index
function showDHTMLXColumn(colArr, gridObj) {
    colArr.forEach(function (item) {
        gridObj.setColumnHidden(item,false);
    });
}

//PC- Add Field sales and System (drill down)
// This function used to show the drill down values (By AD/FS and System)
function fnDrillDownDtls(index, screenName, val) {
	if (index == 0) {
		if (screenName == "system") {
			//AD and FS
			window.location.href = '#scorecard/ad/' + screenName + '/' + val;
		} else if (screenName == "ad") {
			// System and FS
			window.location.href = '#scorecard/system/' + screenName + '/'
					+ val;
		} else if (screenName == "fieldsales") {
			//AD and System
			window.location.href = '#scorecard/ad/' + screenName + '/' + val;
		}
	} else if (index == 1) {
		if (screenName == "system") {
			window.location.href = '#scorecard/fieldsales/' + screenName + '/'
					+ val;
		} else if (screenName == "ad") {
			window.location.href = '#scorecard/fieldsales/' + screenName + '/'
					+ val;
		} else if (screenName == "fieldsales") {
			window.location.href = '#scorecard/system/' + screenName + '/'
					+ val;
		}

	}
}