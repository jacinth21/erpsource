/**********************************************************************************
 * File             :        gmvScorecardRpt.js
 * Description      :        View to represent Scorecard Report
 * Path             :        
 * Author           :        mahendrand
 **********************************************************************************/
define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars',

    // Common
    'global', 'commonutil', 'globusMobileApp/sales/scorecard/view/gmScorecardUtil.js'
],

    function ($, _, Backbone, Handlebars,
        Global, Commonutil, ScorecardUtil) {


        GM.View.ScorecardRpt = Backbone.View.extend({

            name: "Scorecard Reports",

            el: "#div-main",

            events: {
                "click #btn-score-link": "openLink",
                "click .borrowTagBtn": "showBorrowTagPopup",
                "click .scorecardBtn": "toggleScoreCardRpt",
                "click .bulkTagBtn": "showBulkDOPopup",
                "click .score-card-summary-report": "showSummaryReport",
                "click .scoverallsummaryBtn": "toggleScOverallSummaryRpt",
                // PC-5101: Scorecard DO classification changes
                "change #actProcAllowedTag": "fnChangeActProcAllowedTags",
                "change #summaryAllowedTag": "fnChangeSummaryAllowedTags",
                "change #overallSummaryAllowedTag": "fnChangeOverallSummaryAllowedTags"
            },

            template_Scorecard: fnGetTemplate(URL_Scorecard_Template, "gmtScorecard"),
            template_ScoreCardSummary: fnGetTemplate(URL_Scorecard_Template, "gmtScoreCardSummary"),
            template_ScoreCardCn: fnGetTemplate(URL_Scorecard_Template, "gmtScoreCardCn"),
            template_ScoreCardLn: fnGetTemplate(URL_Scorecard_Template, "gmtScoreCardLn"),
            template_ScoreCardActProc: fnGetTemplate(URL_Scorecard_Template, "gmtScoreCardActProc"),
            template_ScoreCardOverallSummary: fnGetTemplate(URL_Scorecard_Template, "gmtScoreCardOverallsummary"),
            template_ScoreCardBulkDO: fnGetTemplate(URL_Scorecard_Template, "gmtScoreCardBulkDO"),
            template_ScoreCardBorrowedTag: fnGetTemplate(URL_Scorecard_Template, "gmtScoreCardBorrowedTag"),

            initialize: function (options) {
                var that = this;
                $("body").removeClass();
                $(window).on('orientationchange', this.onOrientationChange);

                console.log(GM.Global.ScoreCardDispName);
                this.rptView = options.view;
                // PC-5101: Scorecard DO classification changes
                this.rptView["allowedtag"] = "Y";
                console.log(options);
                console.log(this.rptView);
                $("#div-app-title").removeClass("gmVisibilityHidden").addClass("gmVisibilityVisible");
                $('#btn-home').show();
            },

            render: function () {
                var that = this;
                this.activeElem = 'SCBtn';
                this.$el.html(this.template_Scorecard());
                $("#div-app-title").html(lblNm("scorecard"));
                $("#div-dash-subtitle").html(" " + parseNull(GM.Global.ScoreCardDispName));
                GM.Global.ScoreCardDispName = "";
                that.display(this.rptView["stropt"]);
                that.reportType(this.rptView);
            },

            showSummaryReport: function (e) {
                console.log($(e.currentTarget));
                console.log($(e.currentTarget).hasClass("score-card-summary-selected"));
                console.log($(e.currentTarget));

                if ($(e.currentTarget).hasClass("score-card-summary-selected")) {
                    $(e.currentTarget).removeClass("fa-minus").addClass("fa-plus");
                    $(e.currentTarget).parent().parent().find(".score-card-summary-sub").addClass("hide");
                    $(e.currentTarget).removeClass('score-card-summary-selected');
                } else {
                    $(e.currentTarget).addClass("score-card-summary-selected");
                    $(e.currentTarget).parent().parent().find(".score-card-summary-sub").removeClass("hide");
                    $(e.currentTarget).removeClass("fa-plus").addClass("fa-minus");
                }

            },

            // Other Links on Detail Navigation
            openLink: function () {
                var deptID = localStorage.getItem("Dept");
                if (deptID == 2005) {
                    $("#btn-score-link").each(function () {
                        $(this).toggleClass("show-app");
                    })
                    $(".btn-app-link").toggle("fast");
                } else {
                    $(".in-app").addClass("hide");
                    $(".btn-lnk-sales-dashboard").removeClass("hide");
                    $(".btn-app-link").toggle("fast");
                }
            },

            // Toggles the view
            display: function (key) {
                if (key.currentTarget == undefined) {
                    this.key = key;
                }
                if (key == 'ad' || key == 'fieldsales' || key == 'system') {
                    $('#btn-score-summary').addClass("btn-selected");
                }
                else
                    this.$(".btn-detail").each(function () {
                        $(this).removeClass("btn-selected");
                    });
                this.$("#btn-score-" + this.key).addClass("btn-selected");

            },

            //Load Spinner
            getLoader: function (divElem) {
                var template = fnGetTemplate(URL_Common_Template, "gmtSpinner");
                $(divElem).html(template());
            },

            toggleScoreCardRpt: function (e) {
                var that = this;
                $('.scorecardBtn').removeClass('active');
                $(e.currentTarget).addClass('active');
                that.scorecardReport(that.rptView);
                console.log(that.rptView);
            },

            toggleScOverallSummaryRpt: function (e) {
                var that = this;
                if ($(e.target).hasClass('SCBtn'))
                    that.activeElem = 'SCBtn';
                else
                    that.activeElem = 'SCWProjBtn';
                that.overallsummaryReport(that.rptView);

            },

            //ScorecardReport function shows the dhtmlx report for by AD, by fieldsales, by system
            scorecardReport: function (rptInput) {
                $("#allowed-tag").hide();
                $("#overallSummaryAllowedTag").hide();
                var that = this;
                console.log("Summary");
                console.log(rptInput);
                that.rptType = rptInput.stropt;
                that.getLoader('#scorecard-report');
                fnGetWebServerData("scorecard/report", undefined, rptInput, function (data) {
                    console.log(data);
                    if (data != null) {
                        $("#scorecard-report").html(that.template_ScoreCardSummary(data));
                        if (that.rptType == "system") {
                            // PC-5101: Scorecard DO classification changes
                            $("#summaryAllowedTag").val(rptInput["allowedtag"]);
                            $(".reportNm").html("System");
                        } else if (that.rptType == "fieldsales") {
                            // PC-5101: Scorecard DO classification changes
                            $("#summaryAllowedTag").val(rptInput["allowedtag"]);
                            $(".reportNm").html("Fieldsales");
                        } else if (that.rptType == "ad") {
                            // PC-5101: Scorecard DO classification changes
                            $("#summaryAllowedTag").val(rptInput["allowedtag"]);
                            $(".reportNm").html("AD");
                        }
                        if ($(".SCBtn").hasClass("active")) {
                            $('.SCWeffProj').hide();
                        } else {
                            $('.SCWeffProj').show();
                        }

                    } else {
                        $('#scorecard-report').html('No Data Found');
                    }

                    $('#btn-back').show();
                });
            },

            overallsummaryReport: function (rptInput) {
                var that = this;
                console.log("Overall Summary");
                console.log(rptInput);
                that.getLoader('#scorecard-report');
                fnGetWebServerData("scorecard/report", undefined, rptInput, function (data) {
                    console.log(data);
                    if (data != null) {
                        $("#scorecard-report").html(that.template_ScoreCardOverallSummary(data));
                        $("#overallSummaryAllowedTag").val(rptInput["allowedtag"]);
                        //Show/Hide Columns
                        if (that.activeElem == 'SCWProjBtn') {
                            $(".scoverallsummaryBtn").removeClass("active");
                            $(".SCWProjBtn").addClass("active");
                            $('#scoreBtn').hide();
                            $('#scoreProjBtn').show();
                        } else {
                            $(".scoverallsummaryBtn").removeClass("active");
                            $(".SCBtn").addClass("active")
                            $('#scoreBtn').show();
                            $('#scoreProjBtn').hide();
                        }
                    } else {
                        $('#scorecard-report').html('No Data Found');
                    }

                    $('#btn-back').show();
                });
            },

            // To Call the Report function with that inputs based on the report type
            reportType: function (rptInput) {
                var that = this;
                var rptType = rptInput.stropt;
                if (!($(".divTitleRightBtn").hasClass("hide")))
                    $(".divTitleRightBtn").addClass("hide");

                if (!($(".scorecardGrpBtn").hasClass("hide")))
                    $(".scorecardGrpBtn").addClass("hide");

                switch (rptType) {
                    case 'system':
                        $("#div-dash-title").html(lblNm("scorecard_by_system"));
                        $(".scorecardGrpBtn").removeClass("hide");
                        $("#div-detail-navigation-top").removeClass("hide");
                        that.scorecardReport(rptInput);

                        break;
                    case 'ad':
                        $("#div-dash-title").html(lblNm("scorecard_by_ad"));
                        $(".reportNm").html("AD");
                        $(".scorecardGrpBtn").removeClass("hide");
                        $("#div-detail-navigation-top").removeClass("hide");
                        that.scorecardReport(rptInput);
                        break;
                    case 'fieldsales':
                        $("#div-dash-title").html(lblNm("scorecard_by_fieldsales"));
                        $(".reportNm").html("Fieldsales");
                        $(".scorecardGrpBtn").removeClass("hide");
                        $("#div-detail-navigation-top").removeClass("hide");
                        that.scorecardReport(rptInput);
                        break;
                    case 'actualproc':
                        $("#div-dash-title").html(lblNm("scorecard_by_actualproc"));
                        $(".divTitleRightBtn").removeClass("hide");
                        that.actualProcReport(rptInput);
                        break;
                    case 'consignment':
                        $("#div-dash-title").html(lblNm("scorecard_by_consignment"));
                        that.consignmentReport(rptInput);
                        break;
                    case 'loaner':
                        $("#div-dash-title").html(lblNm("scorecard_by_loaner"));
                        that.loanerReport(rptInput);
                        break;
                    case 'overallsummary':
                        $("#div-dash-title").html("OverAll summary");
                        that.overallsummaryReport(rptInput);
                        break;
                }
            },

            //ScorecardReport function shows the dhtmlx report for Actual Procedure
            actualProcReport: function (rptInput) {
                $("#allowed-tag").hide();
                $("#overallSummaryAllowedTag").hide();
                var that = this;
                var input = rptInput;
                that.getLoader('#scorecard-report');
                fnGetWebServerData("scorecard/report", undefined, input, function (data) {
                    console.log(data);
                    if (data != null) {
                        $("#scorecard-report").html(that.template_ScoreCardActProc(data));
                        // PC-5101: Scorecard DO classification changes
                        $("#actProcAllowedTag").val(rptInput["allowedtag"]);
                    } else {
                        $('#scorecard-report').html('No Data Found');
                    }
                    $('#btn-back').show();

                });
            },

            //ScorecardReport function shows the dhtmlx report for consignment Report Usage
            consignmentReport: function (rptInput) {
                $("#allowed-tag").hide();
                $("#overallSummaryAllowedTag").hide();
                var that = this;
                var input = rptInput;

                that.getLoader('#scorecard-report');
                fnGetWebServerData("scorecard/report", undefined, input, function (data) {
                    console.log(data);
                    if (data != null) {
                        $("#scorecard-report").html(that.template_ScoreCardCn(data));
                    } else {
                        $('#scorecard-report').html('No Data Found');
                    }
                    $('#btn-back').show();

                });
            },

            //Scorecard Report function shows the dhtmlx report for Loaner Report Usage
            loanerReport: function (rptInput) {
                $("#allowed-tag").hide();
                $("#overallSummaryAllowedTag").hide();
                var that = this;
                var input = rptInput;

                that.getLoader('#scorecard-report');
                fnGetWebServerData("scorecard/report", undefined, input, function (data) {
                    console.log(JSON.stringify(data));
                    console.log(data);
                    if (data != null) {
                        $("#scorecard-report").html(that.template_ScoreCardLn(data));
                    } else {
                        $('#scorecard-report').html('No Data Found');
                    }
                    $('#btn-back').show();

                });
            },

            //Scorecard Report function shows the dhtmlx report for Borrowed Tag Report
            borrowTagReport: function (rptInput, renderGridAt) {
                var that = this;
                var input = rptInput;

                that.getLoader('#' + renderGridAt);
                fnGetWebServerData("scorecard/report", undefined, input, function (data) {
                    console.log(data);
                    if (data != null) {
                        $('#' + renderGridAt).html(that.template_ScoreCardBorrowedTag(data));
                        $('#' + renderGridAt).find('.score-card-summary-report').unbind('click').bind('click', function (e) {
                            that.showSummaryReport(e);
                        });


                    } else {
                        $('#' + renderGridAt).html('No Data Found');
                    }
                });
            },

            //PC-3677 - Scorecard Report function shows the dhtmlx report for Bulk DO Report
            bulkDOReport: function (rptInput, renderGridAt) {
                console.log(renderGridAt);
                var that = this;
                var input = rptInput;

                that.getLoader('#' + renderGridAt);
                fnGetWebServerData("scorecard/report", undefined, input, function (data) {
                    console.log(data);
                    if (data != null) {
                        $('#' + renderGridAt).html(that.template_ScoreCardBulkDO(data));

                        $('#' + renderGridAt).find('.score-card-summary-report').unbind('click').bind('click', function (e) {
                            that.showSummaryReport(e);
                        });

                    } else {
                        $('#' + renderGridAt).html('No Data Found');
                    }
                });
            },

            // To Show Borrow Tag Report Popup
            showBorrowTagPopup: function () {
                var that = this;
                showPopup();
                $("#div-crm-overlay-content-container").addClass("borrowTagRptPopup");
                $("#div-crm-overlay-content-container.borrowTagRptPopup .modal-body").html("<div id='borrowTagRptGrid'></div>");
                $(".borrowTagRptPopup .modal-title").text('Borrowed Tag Information');
                $(".borrowTagRptPopup .modal-footer").addClass("hide");
                var input = that.rptView;
                input["stropt"] = "borrowed";
                that.borrowTagReport(input, 'borrowTagRptGrid');

                //close Popup remove class
                $(".modal-header .close").on('click', function () {
                    $(".borrowTagRptPopup .modal-title").text('');
                    $("#div-crm-overlay-content-container").removeClass("borrowTagRptPopup");
                });
            },

            //            onOrientationChange: function() {
            //                window.location.href=window.location.href;
            //            }

            // PC-3677 - To Show Bulk DO Report Popup
            showBulkDOPopup: function () {
                var that = this;
                showPopup();
                $("#div-crm-overlay-content-container").addClass("bulkTagRptPopup");
                $("#div-crm-overlay-content-container.bulkTagRptPopup .modal-body").html("<div id='bulkTagRptGrid'></div>");
                $(".bulkTagRptPopup .modal-title").text('Bulk DO');
                $(".bulkTagRptPopup .modal-footer").addClass("hide");
                var input = that.rptView;
                input["stropt"] = "bulk";
                that.bulkDOReport(input, 'bulkTagRptGrid');

                //close Popup remove class
                $(".modal-header .close").on('click', function () {
                    $(".bulkTagRptPopup .modal-title").text('');
                    $("#div-crm-overlay-content-container").removeClass("bulkTagRptPopup");
                });

                //                $("#div-crm-overlay-content-container").find('.score-card-summary-report').unbind('click').bind('click', function (e) {
                //                    that.showSummaryReport(e);
                //                });
            },

            // PC-5101: Scorecard DO classification changes
            // added functions for Actual Procedure and Summary screens

            /**
             * fnChangeActProcAllowedTags - This method used to set the Allowed Tag values
             * to Actual procedure screens 
             * param-event
             */

            fnChangeActProcAllowedTags: function (event) {
                var that = this;
                console.log("Allowed Tag Value: " + event.target.value);
                this.rptView["allowedtag"] = event.target.value;
                that.actualProcReport(this.rptView);
            },

            /**
             * fnChangeSummaryAllowedTags - This method used to set the Allowed Tag values
             * to Summary screens 
             * param-event
             */

            fnChangeSummaryAllowedTags: function (event) {
                var that = this;
                console.log("Allowed Tag Value: " + event.target.value);
                this.rptView["allowedtag"] = event.target.value;
                that.scorecardReport(this.rptView);
            },

            /**
             * fnChangeOverallSummaryAllowedTags - This method used to set the Allowed Tag values
             * to Overall Summary screens 
             * param-event
             */

             fnChangeOverallSummaryAllowedTags: function (event) {
                var that = this;
                console.log("Allowed Tag Value: " + event.target.value);
                this.rptView["allowedtag"] = event.target.value;
                that.overallsummaryReport(this.rptView);
            },

        });
        return GM.View.ScorecardRpt;
    });