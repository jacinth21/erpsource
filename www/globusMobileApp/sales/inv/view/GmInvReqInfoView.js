/**********************************************************************************
 * File:        GmInvReqInfoView.js
 * Description: 
 * Version:     1.0
 * Author:     	tsekaran
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification','saleitemmodel', 'saleitemcollection', 'saleitemlistview','itemview','dropdownview','loaderview'
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification, SaleItem, SaleItems, SaleItemListView,ItemView,DropDownView,LoaderView) {

	'use strict';
	var tabid;
	var arrData = new Array(), currBottomLength = 0, currTopLength = 0;
	var InvReqInfoView = Backbone.View.extend({

		events : {
			"click #li-inv-request-from-date label,#li-inv-request-from-date i,#li-inv-request-to-date label,#li-inv-request-to-date i" : "showDatepicker",
			"change #txt-from-date ,#txt-to-date" : "getRequestDate",
		   	"click #div-inv-request-load":"loadRequest",
			"click .div-drilldown-header" : "toggleListDetail",
		   	"click #div-inv-request-approve":"requestApprove",
		   	"click .div-inv-keypad-values" : "showNumberKeypad",
		   	"click #div-inv-request-reject":"requestReject",
		   	"change .div-req-checkbox input" :"selectRequest",
		   	"click .btn-inv-approve-big" : "reqUpdate",
		   	"click #span-inv-reject-header-x": "closeReject",
		   	"click #div-inv-reject-submit" : "rejectSubmit",
			"click #div-inv-reject-cancel" : "rejectCancel",
			"click #div-request-fromdt-x" : "clearFromDate",
			"click #div-request-todt-x" : "clearToDate",
			"click .fa-comment" : "showComments",
			"click .div-inv-comment-done": "closeComments",
			"keyup #txtarea-inv-add-comments" : "addComments",
			"click .div-inv-add-comments-btn": "submitComments"
		
		},

		/* Load the templates */ 
		template: fnGetTemplate(URL_Inv_Template, "GmInvRequestInfo"),
		template_list: fnGetTemplate(URL_Inv_Template, "GmInvRequestInfoList"),
		template_Comments: fnGetTemplate(URL_Inv_Template, "GmInvShowComments"),
		template_Commentslist: fnGetTemplate(URL_Inv_Template, "GmInvShowCommentsList"),
		tplSpinner: fnGetTemplate(URL_Common_Template, "GmProgressSpinner"),
		
		initialize: function(options) {
			console.log("approvals")
			this.options =options;
			this.addedComments="";
			//this.arrRequest = new Array();
			this.parent = options.parent;
			this.render(options);
		},


		// Render the initial contents when the Catalog View is instantiated
		render: function (options) {
			var that = this;
//			showMsgView("011");
			this.$el.html(this.template());
			var that = this;
			if((navigator.onLine) && (!intOfflineMode)) {
				this.$("#div-request-fromdt-x").hide();
				this.$("#div-request-todt-x").hide();
				console.log(localStorage.getItem("AcID"));
				this.loadFieldSalesInfo();
				if(localStorage.getItem("AcID") != 3 && localStorage.getItem("AcID") != 6 )
					{
						this.$("#div-inv-request-approve").hide();
						this.$("#div-inv-request-reject").hide();
						
					}
				this.loadRequest();
			}
			else
				{
					this.$(".div-detail").hide();
					this.$("#div-inv-request-main").hide();
					this.$("#div-inv-nointernet-connection").show();
					this.$("#div-inv-nointernet-connection").html(" <div> <i class='fa fa-exclamation-triangle fa-lg'></i>"+ lblNm("msg_no_internet_connection")+"</div>"+lblNm("msg_connect_wifi_or_cellular_AD"));
					
				}
			return this;
		},
		
		getRequestDate: function (event) {
			var elem = event.currentTarget;
			this.$(elem).parent().find("label").html($(elem).val());
			this.$(elem).parent().find(".div-planship-date").show();
			},
		
	     parseDate:function(str) {
			    var mdy = str.split('/')
			    return new Date(mdy[2], mdy[0]-1, mdy[1]);
				},

		 daydiff:function(first, second) {
			    return (second-first)/(1000*60*60*24)
				},
			
		 fnGetFormatDate:function(selectedDate){
				var fdate = new Date(selectedDate);
				  var smm = ("0" + (fdate.getMonth() + 1)).slice(-2), sdd = ("0" + fdate.getDate()).slice(-2), syy = fdate.getFullYear();					  
				  syy =  syy.toString();
				  var formatDate = smm+"/"+sdd+"/"+syy;
				  return formatDate;			
				},	
			clearFromDate :	function(){
				
				this.$("#label-inv-request-from-date").text('');
				this.$("#div-request-fromdt-x").hide();
				
			},
			clearToDate :	function(){
				
				this.$("#label-inv-request-to-date").text('');
				this.$("#div-request-todt-x").hide();
				
			},
			loadFieldSalesInfo :function(){
				var that = this;
				fnGetServerData("salesfilter/morefilter",undefined,undefined,function (data) {
					console.log(data);
					if(data.listGmFieldSalesListVO.length != undefined)
						data.listGmFieldSalesListVO = _.where(data.listGmFieldSalesListVO,{"companyid" :"100800"})
					that.fieldsalesData = that.processData(data.listGmFieldSalesListVO,"fieldsales");
					if(that.fsDropDownView)
						that.fsDropDownView.close();
				if(that.fieldsalesData.length > 0)
					that.fieldsalesData.push({"ID":"","Name":"Choose One"});
				that.fsDropDownView = new DropDownView({
					el : that.$("#div-inv-req-approval-fs-dropdown"),
					data : that.fieldsalesData,
					DefaultID : ""
				});
				that.distid = "";
				});			
				this.$("#div-inv-req-approval-fs-dropdown").bind('onchange', function (e, selectedId,SelectedName) {
					that.distid = selectedId.toString();
					$("#txt-report-fieldsales").val(SelectedName);
				});			

			},
			//process the given data to form proper ID and Name
			processData: function(data,key) {
				var strData = JSON.stringify(data);
				var finalData = new Array();
				var processedData = JSON.parse(strData,function (k, v) {
					if (k === (key+"id")) 
				        this.ID = v;
				    else if (k === (key+"nm"))
				        this.Name = v;
				    else
				        return v;
				});

				if(processedData.length==undefined) {
					finalData.push(processedData);
					return finalData; 
				}
				else
					return processedData;
			},
		 loadRequest:function(){	
					var fromdate,todate,reqdtid;
					this.arrRequest = new Array();
					 fromdate = this.$("#label-inv-request-from-date").text();
					 todate   = this.$("#label-inv-request-to-date").text();
					 reqdtid = $("#li-inv-request-inv-id input").val();
					 
					 this.$("#div-search-result-main").hide();
					 this.showRequest(fromdate,todate,reqdtid);	
				},
		reqUpdate: function(event) {
					event.preventDefault();
					var elem = event.currentTarget;
					var that = this;
					var reqDetail = $(elem).attr('name');
					if(!$(elem).hasClass('requested')) {
						this.arrRequest.push(reqDetail);
						$(elem).addClass('requested');
						$(elem).find('i').toggleClass("fa-check-circle fa-circle-o");
						var that = this;
						var reqid = $(elem).parent().find('#div-inv-reqlist-reqid').text();	
						this.$("#div-inv-requestinfo-req-list").find(".div-inv-req-approval-list-item").each(function(index){
							console.log(reqid);						
							if($(this).find("#div-inv-reqlist-reqid").text().toString() == reqid.toString())
								{
								if(!$(this).find(".btn-inv-approve-big").hasClass("requested")){
									that.arrRequest.push($(this).find("#div-inv-reqlist-reqdet").text());
									$(this).find(".btn-inv-approve-big").addClass("requested");
									$(this).find(".btn-inv-approve-big").find('i').toggleClass("fa-check-circle fa-circle-o");
									}
								}
						});
					}
					else {

						var index = this.arrRequest.indexOf(reqDetail);
						if (index > -1) {
							this.arrRequest.splice(index, 1);
						}
						
						$(elem).removeClass('requested');
						$(elem).find('i').toggleClass("fa-check-circle fa-circle-o");
					}
					console.log(this.arrRequest);

			},
		toggleListDetail: function(event) {
		            var elem = event.currentTarget;
		            var parent = $(elem).parent();
		            $(".div-drilldown-header-opened").removeClass("div-drilldown-header-opened");
		            $(".div-drilldown-list").slideUp("fast");
		            if($(elem).find("i").hasClass("fa-caret-right")) {
		                $(".fa-caret-down").removeClass("fa-caret-down").addClass("fa-caret-right");
		                $(elem).find("i").addClass("fa-caret-down").removeClass("fa-caret-right");
		                $(parent).find(".div-drilldown-list").slideDown("fast");
		                $(elem).addClass("div-drilldown-header-opened");
		                this.$("#div-inv-requestinfo-content").animate({
		                    scrollTop: this.$("#div-inv-requestinfo-content").scrollTop() + $(parent).position().top - 160
		                }, "fast");
		            }
		            else {
		                $(elem).find("i").addClass("fa-caret-right").removeClass("fa-caret-down");
		                $(parent).find(".div-drilldown-list").slideUp("fast");
		                $(elem).removeClass("div-drilldown-header-opened");
		            }
			    },
		 rejectCancel :function(){		    	
			    	this.closeReject();			    	
			    },
				
		requestApprove:function(){
			
			if( this.arrRequest.length > 0)
			{
				this.arrGmProdRequestDetVO = [];
				var that = this;
				var reqdtlid,apprqty,reqqty,isSatisfy;
	
				this.$("#div-inv-requestinfo-req-list").find(".div-inv-req-approval-list-item").each(function(index){
					console.log($(this));
					if($(this).find(".btn-inv-approve-big").hasClass("requested")){
						reqdtlid = $(this).find("#div-inv-reqlist-reqdet").text();
						reqqty =  $(this).find("#div-inv-reqlist-reqqty").text();
						apprqty = $(this).find("#div-qty-box").text();
						 if(parseInt(apprqty) > parseInt(reqqty))
						{
						  showNativeAlert(lblNm("msg_request_detail")+reqdtlid+lblNm("msg_greater_qty"),lblNm("msg_alert_message"));
						  isSatisfy = false;
						  return false;
						 }
						 else
						 {
							 that.arrGmProdRequestDetVO.push({"prdreqdid": reqdtlid,"aprqty":apprqty});					 
							 isSatisfy = true;
						 }
					
					}
		
				});
				var arrVO = $.parseJSON(JSON.stringify(this.arrGmProdRequestDetVO));
				this.approveInput = {
						"token": localStorage.getItem("token"),
	   					"stropt": "approve",
	   					"arrGmProdRequestDetVO":arrVO ,
					};
				console.log(this.approveInput);
				if(isSatisfy)
				{
					var that = this;
					this.loaderview = new LoaderView({text : "Approving Request..."});
					fnGetWebServiceData("request/saveitemqty","arrGmProdRequestDetVO", this.approveInput, function(status,data){
						if(status) {
							that.loaderview.close();
							 showNativeAlert(lblNm("msg_request_approved"),lblNm("msg_alert_message"));
							 that.loadRequest();
						}
						else
							{
								that.loaderview.close();
								showNativeAlert(lblNm("msg_error_occured_contact"),lblNm("msg_alert_message"));
							}
					});
				}
			}	
			else
				showNativeAlert(lblNm("msg_select_one_request"),lblNm("msg_alert_message"));
			
		},
		rejectSubmit:function()
		{
				this.rejectInput.comments = $("#txtarea-reject-comments").val()
				this.rejectInput.txnids = this.arrRequest.toString();
				
				var that = this;
				console.log(this.rejectInput);
				this.closeReject();
				this.loaderview = new LoaderView({text : "Rejecting Request..."});
				fnGetWebServiceData("cancel/canceltransaction","", this.rejectInput, function(status,data){
					var errData = new Array();
					errData.push($.parseJSON(data));
					if(errData[0].message != undefined) {
						errData[0].message = errData[0].message.replace("<U><I>"," ");
						errData[0].message = errData[0].message.replace("</I></U>"," ");
						that.loaderview.close();
						showNativeAlert(errData[0].message,lblNm("msg_alert_message"));
						that.loadRequest();
					}
					else
						{
							that.loaderview.close();
							showNativeAlert(lblNm("msg_error"),lblNm("msg_alert_message"));
						}
				});	
			
			
		},
		closeReject :function()
		{
			if(this.loaderview)
				this.loaderview.close();
			this.$("#div-inv-reject-area-main").hide();
			
		},
		requestReject:function(){	
			if( this.arrRequest.length > 0)
				{
				var isQtyMatch,reqdtlid,reqqty,apprqty;
				this.$("#div-inv-requestinfo-req-list").find(".div-inv-req-approval-list-item").each(function(index){
					if($(this).find(".btn-inv-approve-big").hasClass("requested")){
						reqdtlid = $(this).find("#div-inv-reqlist-reqdet").text();
						reqqty =  $(this).find("#div-inv-reqlist-reqqty").text();
						apprqty = $(this).find("#div-qty-box").text();
						 if(parseInt(apprqty) > parseInt(reqqty))
						{
						  showNativeAlert(lblNm("msg_request_detail")+reqdtlid+lblNm("msg_greater_qty"),lblNm("msg_alert_message"));
						  isQtyMatch = false;
						  return false;
						 }
						 else
						 {			 
							 isQtyMatch = true;
						 }
					}
				});
				if(isQtyMatch)
					{
						var that = this;
						var reqdtlid;
						this.$("#div-inv-reject-area-main").show();
						$("#txtarea-reject-comments").val('');
						var reqdt = this.arrRequest.toString();
						console.log(reqdt);
						this.$("#div-reject-txn-detail").html(reqdt);
						this.rejectInput = {
								"token": localStorage.getItem("token"),
								"stropt":"cancel",
								"cancelreasonid":"",     
								"addnlinfo":"",
								"cancelgrp":"ITEMRQ",
								"comments":"",
								"txnids": ""                  
							};
						this.loaderview = new LoaderView({text : "Rejecting Request..."});
						fnRejectReasonData("ITEMRQ",function(data){
							if(that.periodDropDown)
								that.periodDropDown.close();
							that.periodDropDown = new DropDownView({
								el : that.$("#div-reject-reason-drp"),
								data : data,
								DefaultID : data[0].ID
							});
							that.rejectInput.cancelreasonid = data[0].ID;
							that.$("#div-reject-reason-drp").bind('onchange', function (e, selectedId,SelectedName) {
								that.rejectInput.cancelreasonid = selectedId;
							});
						});
		
					}		
				}
			else
				{
				showNativeAlert(lblNm("msg_select_one_request"),lblNm("msg_alert_message"));
				
				}
		},	
		showRequest:function(fromdate,todate,reqdtid){
//			fnShowStatus("");

			if(this.daydiff(this.parseDate(fromdate),this.parseDate(todate))> 60)
				{
					fnShowStatus(lblNm("msg_records_exceed"));
					return;
				}
			this.serverInput = {
				"token": localStorage.getItem("token"),
				"stropt": "400088",
				"fromdt": fromdate,
				"todt": todate,
				"prdreqdid":reqdtid,
				"fsids":this.distid
			};
			console.log(this.serverInput);
			var that = this;
			this.$("#div-saleitemlist").html("");
			this.$(".div-inv-page-loader").html(this.tplSpinner({"message":lblNm("msg_data_fetching")})).show();
			fnGetWebServiceData("caseschdl/fetchItemApprvl","listGmProdRequestDetVO", this.serverInput, function(status,data){
				if(status){
					that.viewData = new Array();
						if(data.length == undefined)
						   that.viewData.push(data)
						else
						   that.viewData = data;
						
						that.processOutputData(that.viewData);						
				}
				else
				{
					that.$(".div-inv-page-loader").show();
					that.$(".div-inv-page-loader").html(lblNm("msg_no_data_display"));	
				}	
										
			});

		},
		processOutputData: function(data) {
			var strData = JSON.stringify(data);
				arrData = $.parseJSON(strData);
			this.collection = new SaleItems(arrData);	
			this.currCollection = this.collection;
			this.$("#div-saleitemlist").html(this.template_list(this.currCollection.toJSON()));
			if(localStorage.getItem("AcID") != 3 && localStorage.getItem("AcID") != 6 )
			{
				this.$(".btn-inv-approve-big").hide();			
			}
			this.$(".div-inv-page-loader").hide();
		},
				//Search the Collection
		searchKeyword: function (event) {	
						this.listView.searchItemList(event);
		},
		showItemListView: function(controlID, collection, itemtemplate, template_URL) {
					
					this.listView = new SaleItemListView(
							{ 
								el: controlID, 
								collection: collection, 
								itemtemplate: itemtemplate,
								template_URL: template_URL,
								columnheader: 0
							}
					);
					return this.listView;
			},
		showDatepicker: function (event) {
			var elem = event.currentTarget;
			var parent = $(elem).parent().attr("id");
			var dptext = $("#"+parent +" input").attr("id");
			$("#"+dptext).datepicker( {duration: 'fast'});
			var visible = $('#ui-datepicker-div').is(':visible');
			$("#"+dptext).datepicker(visible? 'hide' : 'show');	
			
			event.stopPropagation();
			$(window).click(function (event) {
			    if(document.getElementById("ui-datepicker-div").style.display != "none") {
			    	
			    	var elem = event.target;
			       if(!($(elem).hasClass("ui-datepicker-prev") || $(elem).hasClass("ui-datepicker-next") || $(elem).parent().hasClass("ui-datepicker-prev") || $(elem).parent().hasClass("ui-datepicker-next"))) {
			    	   var visible = $('#ui-datepicker-div').is(':visible');
			    	   $("#"+dptext).datepicker(visible? 'hide' : 'show');	
			       }
			       }
			   });
		},

		//Displays the Number keypad 
		showNumberKeypad:function(event){
			var that = this;
			this.parent.showNumericKeypad(event, function(val, target) {
					console.log(event.currentTarget.id);
				if(event.currentTarget.id == "div-inv-req-approval-list"){
					if(val == "0" || val == "00" || val == "000"){
					}else {
						$(target).text(val);
					}
				}else {
					if($("#div-qty-entry-text").text() != "")
						$(target).text(val);
					$(event.currentTarget).attr("value",val);
				}
			
				
			});
		},

		showComments: function(event){
			var that= this;
			this.target = event.currentTarget;
			$(".div-inv-display-comments").html('');
			$(".div-inv-display-comments").show();
			$(".div-inv-display-comments").html(this.template_Comments());
			var partnum = $(event.currentTarget).parent().parent().parent().find("#div-inv-reqlist-pnum").text();
			var partdesc = $(event.currentTarget).parent().parent().parent().find("#div-inv-reqlist-pdesc").text();
			var parentdreqid = $(event.currentTarget).parent().parent().parent().find("#div-inv-reqlist-reqid").text();
			this.caseid = $(event.currentTarget).parent().parent().parent().find("#div-inv-reqlist-caseid").text();
			
			$("#span-inv-comments-part-name").text(partnum);
			$("#span-inv-comments-parentreqid").text(parentdreqid);
			$("#span-inv-comments-part-desc").text(partdesc);
			this.getComments = {
			"token": localStorage.getItem("token"),
			  	 "txnid": this.caseid,
			  	 "txntype":"4000760"
			};
			fnGetWebServiceData("common/fetchComments","gmCommonLogVO", this.getComments, function(status,data){
				if(status && data!=null && data!=undefined){
					that.viewCommentsData = new Array();
						if(data.length == undefined)
						   that.viewCommentsData.push(data)
						else
						   that.viewCommentsData = data;
					   var strData = JSON.stringify(data);
					   arrData = $.parseJSON(strData);
		               that.collection = new SaleItems(arrData);	
		     		   that.$(".div-inv-comments-details").html(that.template_Commentslist(that.collection.toJSON()));
		     		   $(".div-inv-comments-date-name-details").each(function(){
		     		   		var splitstr = $(this).find(".div-inv-comments-date").text();
		     		   			splitstr = splitstr.split(":");
		     		   		$(this).find(".div-inv-comments-date").html(splitstr[0]);
		     		   });			
			    }
				else if(data==null)
					that.$(".div-inv-comments-details").html(lblNm("msg_no_comments_display"));
				else
				that.$(".div-inv-comments-details").html(lblNm("msg_server_not_reach"));
				
			 
			});
		},

		addComments: function(event){

			var maxlength = 500;
			var text = event.currentTarget.value;
			if (text.length >= maxlength) {
		        event.currentTarget.value = text.substring(0, maxlength);
		        text = event.currentTarget.value;
		    }
			this.$("#spn-inv-add-comments-limit").text(maxlength - text.length);
			this.addedComments = text;
			
		},

		submitComments: function(){
			var that =this;
			console.log(this.addedComments);
			console.log(this.addedComments.length);
			if((this.addedComments).length >0) {
				this.$("#txtarea-inv-add-comments").val("");
				this.$("#spn-inv-add-comments-limit").text("500");
				this.saveComments = {
				"token": localStorage.getItem("token"),
				  	 "txnid": this.caseid,
				  	 "txntype":"4000760",
				  	 "comments":this.addedComments
				};
				fnGetWebServiceData("common/saveComments",undefined, this.saveComments, function(status,data){
					if(status){
						$(that.target).trigger("click");
						that.addedComments ="";
						
					 }
				});
			}
			else{
				fnShowStatus(lblNm("msg_no_comments_add"));
			}
				 
		},

		closeComments:function(){
//			fnShowStatus("");
			$(".div-inv-display-comments").hide();
		}
		
		

	});	

	return InvReqInfoView;
});
