/**********************************************************************************
 * File:        GmInvSurgeryInfoView.js
 * Description: 
 * Version:     1.0
 * Author:     	asivaraj
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification', 'searchview', 'dropdownview', 'daoinv'
        ], 

    function ($, _, Backbone, Handlebars, Global, Device, Notification, SearchView, DropDownView, DAOInv) {

	'use strict';
		var InvSurgeryInfoView = Backbone.View.extend({
		
		events : {
			"click #div-inv-surgery-info-surgery-date label, #div-inv-surgery-info-surgery-date i" : "showDatepicker",
			"click #div-inv-surgery-info-plandship-date label, #div-inv-surgery-info-plandship-date i" : "showPlannedShipDatepicker",
			"change #txt-surgery-date" : "getSurgeryDate",
			"change #txt-plandship-date" : "changePlannedShipDate",
			"change input" : "updateJSON",
			"click #div-inv-surgery-info-Account-list" : "showAccounts"
		},

		/* Load the templates */ 
		template: fnGetTemplate(URL_Inv_Template, "GmInvSurgeryInfo"),

		initialize: function(options) {
			this.parent = options.parent;
			this.requesttype = options.requesttype;
			this.render();
		},

		// Render the initial contents when the Catalog View is instantiated
		render: function () {
//			showMsgView("011");
			var that=this
			this.$el.html(this.template());
			if(this.requesttype != 1006505){
				$("#li-inv-surgery-name-type").hide();
				$("#li-inv-surgeon-name").hide();
			}
			this.getSurgeryType();
			this.getSurgeryLevel();
			var partyid = localStorage.getItem("partyID");
                var userid = localStorage.getItem("userID");
                var acid = localStorage.getItem("AcID");
                var deptid = localStorage.getItem("Dept");
//                fnuserAccess(function (cdata) {
//                    if (cdata[0].COUNT == 0) {
//                        var input = {
//                            "token": localStorage.getItem("token"),
//                            "userid": localStorage.getItem("userID"),
//                            "acslvl": localStorage.getItem("AcID")
//                        };
//                        console.log(input);
//                        fnGetWebServerData("auth/AppViewAccess", "gmLogonAccess", input, function (data) {
//                            console.log(data);
//                            if (data != null) {
//                                _.each(data.alAccountId, function (dt) {
//                                    fnSetAccessData(dt.AC_ID);
//                                });
//                            }
//                        });
//                    }
//                });
//			fnGetUserDOID(partyid,function(repList){
//						that.repid = repList.C703_SALES_REP_ID;
//						that.repname = repList.C703_SALES_REP_NAME;
//														
//						fnGetAssocSalesRep(repList.C703_SALES_REP_ID,function(assList){
//							if(assList.length > 0)
//								{
//									that.asssalesrepid = assList[0].ASSOCREPID;
//									that.repid =  assList[0].REPID;
//									that.parent.JSONData.asrepid = assList[0].ASSOCREPID;
//									that.repaccid =  assList[0].ACCID;
//
//									that.repname = assList[0].REPNM;
//									fnGetRepInfoInv(that.asssalesrepid,function(data){
//										that.asssalesrepnm = data.C703_SALES_REP_NAME;
//										$("#div-inv-surgery-assocsalesinfoname").html(data.C703_SALES_REP_NAME);	
//									});
//								
//									$("#div-inv-surgery-info-SalesRep-list").html(assList[0].REPNM);
//																	
//								}
                        console.log(userid);
						this.loadMapAccounts(function(){
                        fnGetAccountByAccess(userid, function (acclist) {
                            console.log(acclist);
                            that.getAccountName(acclist, "");
									});
						});			
//
//				   					});
//
//							});			

			if (!(this.requesttype == "1006505" || this.requesttype == "107286")) {
			    var aclvl = localStorage.getItem("AcID");

			    $("#li-inv-surgery-salesrep-label").hide();
			    $("#li-inv-surgery-account-label").hide();
			    $("#div-inv-surgery-info-loan-to").html(lblNm("bill_to"));
			    $("#div-inv-surgery-info-surgery-date-label").html(lblNm("msg_required_date"));
			    $("#div-inv-surgery-fieldsalesinfoname").show();

                    fnGetDistByAccess(function (data) {
                        if (data.length == 1) {
                            $("#div-inv-surgery-fieldsalesinfoname").html(data[0].Name);
                            $("#div-inv-surgery-fieldsalesinfoid").html(data[0].ID);
                        } else if (data.length == 0) {
                            var partyid = localStorage.getItem("partyID");
                            fnGetUserDOID(partyid, function (repList) {
                                that.repid = repList.C703_SALES_REP_ID;
                                that.repname = repList.C703_SALES_REP_NAME;
                                fnGetDistributorByRep(that.repid, function (data) {
                                    $("#div-inv-surgery-fieldsalesinfoname").html(data[0].Name);
                                    $("#div-inv-surgery-fieldsalesinfoid").html(data[0].ID);
                                });
                            });
                        } else {
                            that.getDistributorName(data, "");
                        } 

                    }); 
			}
            if (this.requesttype == "107286") {
                $("#li-inv-surgery-fieldsalesinfo-label").hide();
                $("#li-inv-surgery-salesrep-label").hide();
                $("#div-inv-surgery-info-surgery-date-label").html(lblNm("msg_required_date"));
            }
			$("#div-footer").addClass("hide");
			return this;
			
		},
		
		// there is no accounts for user, show info alert PMT#51543
		showAccounts: function() {
            fnuserAccess(function (cdata) {
                    if (cdata[0].COUNT == 0) {
                    	showNativeAlert(lblNm("msg_contact_sale_team"), lblNm("msg_account_required"), lblNm("msg_ok"));
               }
            });
        },
		
		//load mapped accounts 
		loadMapAccounts: function (cb) {
			var that =this;
			var input = {
            "token": localStorage.getItem("token"),
            "userid": localStorage.getItem("userID"),
            "acslvl": localStorage.getItem("AcID")
        };
        console.log(input);
			fnGetWebServerData("auth/AppViewAccess", "gmLogonAccess", input, function (data) {
				console.log(data);
				if (data != null) {
					fnDelSetAccessData();
					_.each(data.alAccountId, function (dt) {
						fnSetAccessData(dt.AC_ID);
					});
				}
			});
		cb();
	},

            getDistributorName: function (data, selectedID) {
                var that = this;
                if (that.fieldsalesinfoname)
                    that.fieldsalesinfoname.close();
                data.push({
                    "ID": 0,
                    "Name": "Choose Names"
                });
                this.fieldsalesinfoname = new DropDownView({
                    el: this.$("#div-inv-surgery-fieldsalesinfoname"),
                    data: data,
                    DefaultID: ""
                });
                if (this.requesttype == 1006507 || this.requesttype == 1006506 || this.requesttype == 107286) {//Account Item Consignment
                    //                    PBUG-2803 
                    if (this.requesttype == 1006507 || this.requesttype == 107286){ //107286 - Account Item Consignment
                    
                        var rule = localStorage.getItem('ITEM_APP_ACCESS');
                    }
                     if (this.requesttype == 1006506){ //Consignment Set
                        
                        var rule = localStorage.getItem('SET_APP_ACCESS');
                    }
                 
                    if (rule != localStorage.getItem('AcID')) {
                        if ($(window).width() < 480) {
                              $("#div-inv-surgery-fieldsalesinfoname").width('98%');
                 }
          $("#div-inv-surgery-fieldsalesinfoname").bind('onchange', function (e, selectedId, SelectedName) {
                    $("#txt-surgery-distributor").val(SelectedName);
                    that.parent.JSONData.distid = selectedId.toString();
                    that.parent.JSONData.dnm = selectedId.toString();
                    $("#div-inv-surgery-fieldsalesinfoid").html(selectedId);
                    $("#label-inv-surgery-info-surgery-date").html("");
                    $("#label-inv-surgery-info-plandship-date").html("");
                    that.parent.JSONData.reqdt = "";
                    that.parent.JSONSets = [];
                        if (selectedId == 0){
                            that.parent.JSONData.distid = "";
                            that.parent.JSONData.dnm = "";
                            $("#div-inv-surgery-fieldsalesinfoid").html("");
                        }
                    });
                    } else {
                        $("#div-inv-surgery-fieldsalesinfoname").unbind('click'); /// throws error
                    }

                } else {
                    $("#div-inv-surgery-fieldsalesinfoname").bind('onchange', function (e, selectedId, SelectedName) {
                        $("#txt-surgery-distributor").val(SelectedName);
                        that.parent.JSONData.distid = selectedId.toString();
                        that.parent.JSONData.dnm = selectedId.toString();
                        $("#div-inv-surgery-fieldsalesinfoid").html(selectedId);
                        $("#label-inv-surgery-info-surgery-date").html("");
                        $("#label-inv-surgery-info-plandship-date").html("");
                        that.parent.JSONData.reqdt = "";
                        that.parent.JSONSets = [];
                         if (selectedId == 0){
                            that.parent.JSONData.distid = "";
                            that.parent.JSONData.dnm = "";
                            $("#div-inv-surgery-fieldsalesinfoid").html("");
                        }
                    });
                }
            },
		
			getAccountName:function(data,selectedID){
				var that=this;
					if(that.AccountDropDownView)
						that.AccountDropDownView.close();
				data.push({"ID" : 0, "Name" : "Choose Account Name"});
				this.AccountDropDownView = new DropDownView({
					el : this.$("#div-inv-surgery-info-Account-list"),
					data : data,
					DefaultID : "0"
				});
				var that = this;
                
                if (this.requesttype == 1006505 || this.requesttype == 107286) { 
                    //                    var input = {
                    //                        "token": localStorage.getItem("token"),
                    //                        "ruleid": localStorage.getItem("AcID"),
                    //                        "rulegrpid": 'LOANER_APP_ACCESS',
                    //                        "cmpid": localStorage.getItem("cmpid")
                    //                    };
                    var rule = localStorage.getItem('LOANER_APP_ACCESS');
                    console.log(rule);
                    if (rule != localStorage.getItem('AcID')) {
                                $("#div-inv-surgery-info-Account-list").bind('onchange', function (e, selectedId,SelectedName) {
                                $("#txt-surgery-account").val(SelectedName);
                                that.parent.JSONData.acctid =  selectedId.toString();
                                console.log(that.parent.JSONData.acctid);
                                fnGetAssocSalesAccnt(that.parent.JSONData.acctid, function (assdata) {
                                    if (assdata.length > 0) {
                                    	 if (assdata.length == 1) {
                                    		 $("#div-inv-surgery-assocsalesinfoname").html(assdata[0].Name);
                                    		 } else {
                                    		 that.getAssRepName(assdata, "");
                                    		 }
                                    } else {
                                        $("#div-inv-surgery-assocsalesinfoname").html("");
                                    }
                                });
                                fnGetDetailsFromAccount(that.parent.JSONData.acctid, function (accdata) {
                                    if (accdata.length > 0) {
                                        that.repid = accdata[0].REPID;
                                        that.repname = accdata[0].REPNAME;
                                    // To set the field sales id and
                                    // field sales name from account
                                    $("#div-inv-surgery-fieldsalesinfoid").html(accdata[0].DISTID);
                                    $("#div-inv-surgery-fieldsalesinfoname").html(accdata[0].DISTNAME);
                                    $("#div-inv-surgery-info-SalesRep-list").html(accdata[0].REPNAME);
                                    } else {
                                        $("#div-inv-surgery-fieldsalesinfoid").html("");
                                        $("#div-inv-surgery-fieldsalesinfoname").html("");
                                        $("#div-inv-surgery-info-SalesRep-list").html("");
                                        $("#div-inv-surgery-assocsalesinfoname").html("");
                                    }
                            });	
                                    $("#label-inv-surgery-info-surgery-date").html("");
                                    $("#label-inv-surgery-info-plandship-date").html("");
                                    that.parent.JSONData.reqdt = ""; 
                                    that.parent.JSONData.surdt = "";
                                    that.parent.JSONSets =[];
                                    that.parent.SetsJSONData = [];    
                            });
                        }else{
                             $("#div-inv-surgery-info-Account-list").unbind('click'); 
                        }

            }
        },
		
        getAssRepName: function (data, selectedID) {
        	var that = this;
        	if (that.AssSalesRepnm)
        	that.AssSalesRepnm.close();
        	data.push({
        	"ID": 0,
        	"Name": "Choose Name"
        	});
        	this.AssSalesRepnm = new DropDownView({
        	 el: this.$("#div-inv-surgery-assocsalesinfoname"),
        	data: data,
        	DefaultID: ""
        	});
        	$("#div-inv-surgery-assocsalesinfoname").bind('onchange', function (e, selectedId, SelectedName) {
        	$("#txt-surgery-assorep").val(SelectedName);
                    that.parent.JSONData.asrepid = selectedId.toString();
        	if (selectedId == 0) {
                        that.parent.JSONData.asrepid = "";
        	}
        	});
        },
        	
        fnGetFormatDate: function (selectedDate) {
            var fdate = new Date(selectedDate);
            var smm = ("0" + (fdate.getMonth() + 1)).slice(-2),
                sdd = ("0" + fdate.getDate()).slice(-2),
                syy = fdate.getFullYear();
            syy = syy.toString();
            var formatDate = smm + "/" + sdd + "/" + syy;
            return formatDate;
        },

        changePlannedShipDate: function (event) {
            var elem = event.currentTarget;
            var planDate = $(elem).val();
            planDate = formattedDate(planDate, localStorage.getItem("cmpdfmt"));
            this.$(elem).parent().find("label").html(planDate);
        },


        getSurgeryDate: function (event) {
            var that = this;
            var elem = event.currentTarget;
            this.surgeryDate = $(elem).val();
            var fmtSurgeryDate = formattedDate(this.surgeryDate, localStorage.getItem("cmpdfmt"));
            this.$(elem).parent().find("label").html(fmtSurgeryDate);
            this.showPlannedShipdate(this.surgeryDate);
        },


        showPlannedShipdate: function (sDate) {
            var surgeryDate = new Date(sDate);
            var surgeryDay = surgeryDate.getDay();
            var currDate = new Date();

            if (surgeryDay == 0) { //On Sundays
                var shipDate = new Date(new Date(surgeryDate).setDate(surgeryDate.getDate() - 4));
            } else if (surgeryDay >= 4) { //On Thu, Fri and Sat
                var shipDate = new Date(new Date(surgeryDate).setDate(surgeryDate.getDate() - 3));
            } else { //On Mon, Tue and Wed
                var shipDate = new Date(new Date(surgeryDate).setDate(surgeryDate.getDate() - 5));
            }

            //If the ship date is past, then change it to today's date
            if (shipDate <= currDate) {
                shipDate = currDate;
            }
            var plannedShipDate = this.fnGetFormatDate(shipDate);
            var fmtPlanDate = formattedDate(plannedShipDate, localStorage.getItem("cmpdfmt"));
            $("#label-inv-surgery-info-plandship-date").html(fmtPlanDate);
            this.updateJSON();
        },

		updateJSON: function() {
       
        $("#btn-inv-reset").prop("disabled",false);
		var prevDID = this.parent.JSONData.did;
		console.log(prevDID);
		console.log(this.parent.JSONData.did);
		// this.parent.JSONData.did = prevDID;
		var plandshipdate = $("#label-inv-surgery-info-plandship-date").text();
		this.parent.JSONData.did = this.$("#div-inv-surgery-fieldsalesinfoid").text();
		
		
			
			if(this.requesttype == 1006505){
				this.parent.JSONData.surdt = this.$("#label-inv-surgery-info-surgery-date").text();
				this.parent.JSONData.repid = this.repid;

				
			}else if(this.requesttype == 1006507 || this.requesttype == 107286) {
				this.parent.JSONData.reqdt = this.$("#label-inv-surgery-info-surgery-date").text();
				this.parent.JSONData.repid = this.repid;
			}else {
				this.parent.JSONData.reqdt = this.$("#label-inv-surgery-info-surgery-date").text();
				
			}
			
			this.parent.JSONData.arrGmCaseAttributeVO = new Array();
			this.parent.JSONData.arrGmCaseAttributeVO.push({"caattrid" :"", "cainfoid" : "", "attrtp" : "103641", "attrval" : this.surgerytypeid, "voidfl" : ""}); //Surgeon Type
			this.parent.JSONData.arrGmCaseAttributeVO.push({"caattrid" :"", "cainfoid" : "", "attrtp" : "103640", "attrval" : this.$("#label-inv-surgery-info-surgeon-name").val(), "voidfl" : ""}); //Surgeon Name
			
			if(prevDID!=this.parent.JSONData.did) {
				this.parent.JSONSets = [];
				this.parent.SetsJSONData = [];
					$("#div-inv-sets-list").empty();
					$("#ul-inv-sets-header-title").hide();
					$("#div-inv-shiptoinfo-sets-list").empty();
					this.parent.JSONData.gmTxnShipInfoVO = new Array();
					this.parent.JSONData.arrGmPartDetailVO = new Array();
					
			}		
			
					
			
		},

		showPlannedShipDatepicker:function(event){
		
			var elem = event.currentTarget;
			var parent = $(elem).parent().attr("id");
			var dptext = $("#"+parent +" input").attr("id");
			var sur_date = new Date(this.surgeryDate);
			var showdatepicker = $('#ui-datepicker-div').is(':visible');
			if(showdatepicker == false) {
				if(this.requesttype == 1006505){
					var loanershipddate = sur_date.setDate(sur_date.getDate()-9);
					var loanerdate = new Date(loanershipddate);
					var todaydate = new Date();
					var splitSurgeryDate = this.fnGetFormatDate(loanerdate);
				 	var splitTodayDate = this.fnGetFormatDate(todaydate);
				 	var surgerydateMth = splitSurgeryDate.split('/');
				 	var todaydateMth = splitTodayDate.split('/');
					var ldate = loanerdate.getDate();
					var tdate = todaydate.getDate();
					var firstday = loanerdate.getDay();
					
					if((firstday == 0) && ((surgerydateMth[0] == todaydateMth[0]))) {
					
						var loanerdate = sur_date.setDate(sur_date.getDate()-2);
						var loanerdateship = new Date(loanerdate);
						$("#"+dptext).datepicker('destroy');
						$("#"+dptext).datepicker({minDate: new Date(loanerdateship.setDate(loanerdateship.getDate())), maxDate: new Date(this.surgeryDate)});
					} else if((firstday == 6) && (surgerydateMth[0] == todaydateMth[0])){
						
						var loanerdate = sur_date.setDate(sur_date.getDate()-2);
						var loanerdateship = new Date(loanerdate);
						$("#"+dptext).datepicker('destroy');
						$("#"+dptext).datepicker({minDate: new Date(loanerdateship.setDate(loanerdateship.getDate())), maxDate: new Date(this.surgeryDate)});
					} else if((ldate < tdate) && (surgerydateMth[0] == todaydateMth[0])){
						
						$("#"+dptext).datepicker('destroy');
						$("#"+dptext).datepicker({minDate: 0, maxDate: new Date(this.surgeryDate)});
					} else{
						 
						if((firstday == 0 )){
							
							if((surgerydateMth[0] > todaydateMth[0])){
									var loanerdate = sur_date.setDate(sur_date.getDate()-2);
									var loanerdateship = new Date(loanerdate);
									$("#"+dptext).datepicker('destroy');
									$("#"+dptext).datepicker({minDate: new Date(loanerdateship.setDate(loanerdateship.getDate())), maxDate: new Date(this.surgeryDate)});
								}else{
									var loanerdate = sur_date.setDate(sur_date.getDate()-2);
									var loanerdateship = new Date(loanerdate);
									$("#"+dptext).datepicker('destroy');
									$("#"+dptext).datepicker({minDate: 0, maxDate: new Date(this.surgeryDate)});
							}
							
						}else if((firstday == 6)){
							
							if((surgerydateMth[0] > todaydateMth[0])){
								var loanerdate = sur_date.setDate(sur_date.getDate()-2);
								var loanerdateship = new Date(loanerdate);
								$("#"+dptext).datepicker('destroy');
								$("#"+dptext).datepicker({minDate: new Date(loanerdateship.setDate(loanerdateship.getDate())), maxDate: new Date(this.surgeryDate)});
							} else {
								
								var loanerdate = sur_date.setDate(sur_date.getDate()-2);
								var loanerdateship = new Date(loanerdate);
								$("#"+dptext).datepicker('destroy');
								$("#"+dptext).datepicker({minDate: 0, maxDate: new Date(this.surgeryDate)});
							}
						
					}else {
							
							if((surgerydateMth[0] < todaydateMth[0])){
								
								$("#"+dptext).datepicker('destroy');
								$("#"+dptext).datepicker({minDate: 0, maxDate: new Date(this.surgeryDate)});
							}else{
								$("#"+dptext).datepicker('destroy');
								$("#"+dptext).datepicker({minDate: new Date(loanerdate.setDate(loanerdate.getDate())), maxDate: new Date(this.surgeryDate)});
							}
						
					 }
				}
					
				} else{
					$("#"+dptext).datepicker('destroy');
					$("#"+dptext).datepicker({minDate: 0, maxDate: new Date(this.surgeryDate)});
				}
			}
			

			var visible = $('#ui-datepicker-div').is(':visible');
			$("#"+dptext).datepicker(visible? 'hide' : 'show');	
			$("#"+dptext).datepicker("refresh");
			
			event.stopPropagation();
			$(window).click(function (event) {
			     if(document.getElementById("ui-datepicker-div").style.display != "none") {
			    	
			    	var elem = event.target;
			       if(!($(elem).hasClass("ui-datepicker-prev") || $(elem).hasClass("ui-datepicker-next") || $(elem).parent().hasClass("ui-datepicker-prev") || $(elem).parent().hasClass("ui-datepicker-next"))) {
			    	   var visible = $('#ui-datepicker-div').is(':visible');
			    	   $("#"+dptext).datepicker(visible? 'hide' : 'show');	
			       }
			   }
			 });

		},


		showDatepicker: function (event) {

			var elem = event.currentTarget;
			var parent = $(elem).parent().attr("id");
			var dptext = $("#"+parent +" input").attr("id");
			$("#"+dptext).datepicker( { minDate: 0, duration: 'fast'});
			var visible = $('#ui-datepicker-div').is(':visible');
			$("#"+dptext).datepicker(visible? 'hide' : 'show');	
			
			event.stopPropagation();
			$(window).click(function (event) {
			     if(document.getElementById("ui-datepicker-div").style.display != "none") {
			    	
			    	var elem = event.target;
			       if(!($(elem).hasClass("ui-datepicker-prev") || $(elem).hasClass("ui-datepicker-next") || $(elem).parent().hasClass("ui-datepicker-prev") || $(elem).parent().hasClass("ui-datepicker-next"))) {
			    	   var visible = $('#ui-datepicker-div').is(':visible');
			    	   $("#"+dptext).datepicker(visible? 'hide' : 'show');	
			       }
			   }
			 });
		},

		getSurgeryType: function() {
			var that = this;
			var defaultid;
			
			fnGetSurgeryLevel("SURTYP",function(data){
				data.push({"ID" : 0, "Name" : "Choose One"});
				that.dropdownview = new DropDownView({
					el : that.$("#div-inv-surgery-info-surgerytype-list"),
					data : data,
					DefaultID : 0
				});
				
				that.$("#div-inv-surgery-info-surgerytype-list").bind('onchange', function (e, selectedId,SelectedName) {
					that.surgerytypeid = selectedId ;
					
				});
			});
		},

		getSurgeryLevel: function() {
			var that = this;
			var defaultid;
			if( this.surgerylevelid !=""){
				defaultid = this.surgerylevelid;
			}else {
				defaultid = 0;
			}
			fnGetSurgeryLevel("TRLVL",function(data){
				data.push({"ID" : 0, "Name" : "Choose One"});
				that.dropdownview = new DropDownView({
						el : that.$("#div-inv-surgery-info-surgerylevel-list"),
						data : data,
						DefaultID : 0
					});
					
					that.$("#div-inv-surgery-info-surgerylevel-list").bind('onchange', function (e, selectedId,SelectedName) {
						that.surgerylevelid = selectedId  ;
						that.updateJSON();
					});
			});
		},

		close: function() {
			this.unbind();
			this.undelegateEvents();
		}

	});	

	return InvSurgeryInfoView;
});