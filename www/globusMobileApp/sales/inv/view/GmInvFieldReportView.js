/**********************************************************************************
 * File:        GmInvFieldReportView.js
 * Description: Field Inventory Report - Online Report which handles the main report and drilldown of 'Sy', 'F' and 'A'
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification','datasync','saleitemmodel', 'saleitemcollection', 
        'saleitemlistview','itemview','jqueryui','dropdownview','daoinv', 'invfieldreportsubview'
        ], 
	function ($, _, Backbone, Handlebars, Global, Device, Notification,DataSync, SaleItem, SaleItems, SaleItemListView,ItemView,JqueryUI,DropDownView, DAOInv, FieldReportSubView) {

	'use strict';
	
	var InvFieldReportView = Backbone.View.extend({
		
		events : {
			"click .div-inv-report-settype-label" : "toggleContent",
			"click #li-inv-report-showall i" : "showAiData",
			"click #div-inv-report-load" : "loadData",
			"click .td-index-0 span" : "loadDrillDown",
			"click #div-inv-consignment-detail-close-icon" : "closeConsignmentDetailTab",
			"click #div-inv-consignment-part-detail-close-icon" : "closeConsignmentPartDetailTab",
			"click .div-inv-consignment-detail-type i" : "loadConsignmentDetail"
			
		},

		template: fnGetTemplate(URL_Inv_Template, "GmInvFieldReport"),

		template_list: fnGetTemplate(URL_Inv_Template, "GmInvFieldReportList"),
		
		template_consignmentlist: fnGetTemplate(URL_Inv_Template, "GmInvConsignmentSetDetaillist"),
		
		template_consignmentpartlist: fnGetTemplate(URL_Inv_Template, "GmInvConsignmentPartDetaillist"),
		
		tplSpinner: fnGetTemplate(URL_Common_Template, "GmProgressSpinner"),
		
		initialize: function(options) {

			this.parent = options.parent;
			this.options = options;

			var now = new Date();
			var prev = new Date();
			prev.setMonth(prev.getMonth() - 3);
			
			this.breadCrumb = new Array();

			this.serverInput = {  
				  "token": localStorage.getItem('token'), 
				  "companyid": (this.morefilterCompanyid)?this.morefilterCompanyid:localStorage.getItem("userCompany"),
				  "divids": (this.morefilterDivisionid)?this.morefilterDivisionid:localStorage.getItem("userDivision"),
				  "zoneids": (this.morefilterZoneid)?this.morefilterZoneid:"",
				  "regionids": (this.morefilterRegionid)?this.morefilterRegionid:"",
				  "fsids": (this.morefilterFieldsalesid)?this.morefilterFieldsalesid:"",
				  
				  "action":"LoadDCONS" , 
				  "salestype":"50300",
				  "contype":"",
				  "turns":"1",
				  "showai":"",
				  "frmmonth": prev.getMonth() + 1,
				  "frmyear": prev.getFullYear(),   
				  "tomonth": now.getMonth() + 1, 
				  "toyear": now.getFullYear(),
				  
				  "sysonlycon":"",
				  "baselinenm":"Baseline Sets",
				  "setnum":"",
				  "did":"",
				  "type":"",
				  "loantypdisable":"",
				  "pnum":"",
				  "conperiod":"",
				  "hsetnum":"",
				  "consummtype":"",
				  "repid":"",
				  "showarrowfl":"",
				  "consettype":"",
				  "hpnum":"",
				  "adid":"",
				  "acctid":"",
				  "loanertype":"",
				  "headprefix":"",
				  "status":"",
				  "hcbonm":"",
				  "vpid":"",
				  "hideamtfl":"",
				  "turnslabel":"",
				  "state":"",
				  "showdisabled":"",
				  "terrid":"",
				  "showpercentfl":"1",
				  "setid":"", 
				  
				  "condition":"",
				  "accessfilter":"",
				  "region":"",
				  "groupaccid":""
				};
			
			this.skipCalculations = ["Name", "Loaner Sets", "Consign Sets", "Loaner Usage", "Total Sets", "Baseline Sets", "Loaner case in period", "Loaner <BR>Utilization %", "#Cases<BR> per Month"];	
			this.skipDivide = ["Name", "#Cases<BR> per M/Set", "$ Turns", "#C/M/S<br> Target", "Turns Target", "%Exp Cases", "@Target<br>#Sets", "Excess<br>#Sets", "% Additional", "% to <BR>AI Budget"];
			this.roundOffValues = {
					"Annualized Value" : 1,
					"$ Value" : 1,
					"#Cases<BR>/Month" : 1,
					"#Cases<BR> per M/Set" : 1,
					"$ Turns" : 1,
					"#C/M/S<br> Target" : 1,
					"Turns Target" : 1,
					"%Exp Cases" : 0,
					"@Target<br>#Sets" : 0,
					"Excess<br>#Sets" : 0
			}
			
			this.render();
		},

		// Render the initial contents when the View is instantiated
		render: function () {
			var that = this;
//			showMsgView("011");
			this.$el.html(this.template());
			this.loadInputOptions();
			
			return this;
		},

		loadInputOptions: function () {
			var that = this;
			fnGetInvAppData('CSGTP', function(data) {
				that.typeDropDown = new DropDownView({
					el : that.$("#div-inv-report-type-drp"),
					data : data,
					DefaultID : '50401'
				});
				that.serverInput.contype = '50401';
				
				that.$("#div-inv-report-type-drp").bind('onchange', function (e, selectedId,SelectedName) {
					that.serverInput.contype = selectedId;
				});

				fnGetInvAppData('TURN', function(data) {

					var strdata = JSON.stringify(data);

					strdata = strdata.replace(/ID/g,'CodeID');
					strdata = strdata.replace(/ALT/g,'ID');

					data = $.parseJSON(strdata);

					that.periodDropDown = new DropDownView({
						el : that.$("#div-inv-report-period-drp"),
						data : data,
						DefaultID : data[0].ID
					});
					
					that.$("#div-inv-report-period-drp").bind('onchange', function (e, selectedId,SelectedName) {
						that.serverInput.turns = selectedId;
					});
				});

				fnGetInvAppData('MONTH', function(data) {

					var strdata = JSON.stringify(data);

					strdata = strdata.replace(/ID/g,'CodeID');
					strdata = strdata.replace(/ALT/g,'ID');

					data = $.parseJSON(strdata);

					var now = new Date();
					var prev = new Date();
					prev.setMonth(prev.getMonth() - 3);

					var fromMonth = prev.getMonth() + 1;
					var toMonth = now.getMonth() + 1;

					var fromYear = prev.getFullYear();
					var toYear = now.getFullYear();

					if(parseInt(fromMonth)<10)
						fromMonth = "0" + fromMonth;

					if(parseInt(toMonth)<10)
						toMonth = "0" + toMonth;

					that.fromMonthDropDown = new DropDownView({
						el : that.$("#div-inv-report-from-month"),
						data : data,
						DefaultID : fromMonth
					});
					
					that.$("#div-inv-report-from-month").bind('onchange', function (e, selectedId,SelectedName) {
						that.serverInput.frmmonth = selectedId;
					});

					that.toMonthDropDown = new DropDownView({
						el : that.$("#div-inv-report-to-month"),
						data : data,
						DefaultID : toMonth
					});
					
					that.$("#div-inv-report-to-month").bind('onchange', function (e, selectedId,SelectedName) {
						that.serverInput.tomonth = selectedId;
					});

					fnGetInvAppData('YEAR', function(data) {

						that.fromYearDropDown = new DropDownView({
							el : that.$("#div-inv-report-from-year"),
							data : data,
							DefaultName : fromYear
						});
						
						that.$("#div-inv-report-from-year").bind('onchange', function (e, selectedId,SelectedName) {
							that.serverInput.frmyear = SelectedName;
						});

						that.toMonthDropDown = new DropDownView({
							el : that.$("#div-inv-report-to-year"),
							data : data,
							DefaultName : toYear
						});
						
						that.$("#div-inv-report-to-year").bind('onchange', function (e, selectedId,SelectedName) {
							that.serverInput.toyear = SelectedName;
						});

						var viewBy = new Array();
						viewBy.push({"ID":"LoadVP","Name":"VP"});
						viewBy.push({"ID":"LoadAD","Name":"AD"});
						viewBy.push({"ID":"LoadDCONS","Name":"Field Sales"});

						var defaultView = 'LoadDCONS';
						var userAccLevel = parseInt(localStorage.getItem('AcID'));

						if(userAccLevel==3)
							defaultView = 'LoadAD';
						else if(userAccLevel==4)
							defaultView = 'LoadVP';

						that.serverInput.action = defaultView;

						that.viewByDropDown = new DropDownView({
							el : that.$("#div-inv-report-viewby-drp"),
							data : viewBy,
							DefaultID : defaultView
						});
						
						that.$("#div-inv-report-viewby-drp").bind('onchange', function (e, selectedId, SelectedName) {
							that.serverInput.action = selectedId;
						});

						that.currServerInput = $.parseJSON(JSON.stringify(that.serverInput));

						that.arrServerInput = new Array();

						that.arrServerInput.push(that.currServerInput)
						
						that.toggleContent();
					});

				});

			});
		},

		loadData: function(event) {
			var that = this;
			var input;


			if((event!=undefined)&&(event.currentTarget.id=="div-inv-report-load")) {
				input = this.serverInput;
				this.breadCrumb = []
				this.currServerInput = $.parseJSON(JSON.stringify(this.serverInput));
			}
			else
				input = this.arrServerInput[this.arrServerInput.length-1];

			this.$("#ul-inv-report-" + that.key).html(this.tplSpinner({"message":lblNm("msg_data_fetching")}));

			fnGetServerData('reports/fetchfieldinventoryrpt', undefined, input, function(data) {
				
				that.$("#div-inv-report-breadcrumb").html(that.breadCrumb.join(" > "));

				var arrHeaders = data.header[0].split(',');
				var header = new Array();
				var details = data.details;
				var footer = data.footer;
				
				console.log(JSON.stringify(data));

				details = _.sortBy(details,'Name');
				
				var headerColors = {
						"Name" :  {
							"bgcolor" : "#c3c5b8",
							"color"	: "#000"
						},
						
						"Baseline Sets" : {
							"bgcolor" : "",
							"color"	: ""
						},
						
						"Total <BR>$Value" : {
							"bgcolor" : "",
							"color"	: ""
						},
						
						"#Cases<BR> per Month" : {
							"bgcolor" : "",
							"color"	: ""
						},
						
						"#Cases<BR> per M/Set" : {
							"bgcolor" : "",
							"color"	: ""
						},
						
						"Loaner case in period" : {
							"bgcolor" : "#c3c5b8",
							"color"	: "#000"
						},
						
						"Loaner <BR>Utilization %" : {
							"bgcolor" : "#c3c5b8",
							"color"	: "#000"
						},
						
						"AAS" : {
							"bgcolor" : "#feb469",
							"color"	: "#000"
						},
						
						"AAI" : {
							"bgcolor" : "#feb469",
							"color"	: "#000"
						},
						
						"STD" : {
							"bgcolor" : "#feb469",
							"color"	: "#000"
						},
						
						"% Additional" : {
							"bgcolor" : "#feb469",
							"color"	: "#000"
						},
						
						"Budget" : {
							"bgcolor" : "#c3c5b8",
							"color"	: "#000"
						},
						
						"AI Usage<BR>(Quarter)" : {
							"bgcolor" : "#c3c5b8",
							"color"	: "#000"
						},
						
						"% to <BR>AI Budget" : {
							"bgcolor" : "#c3c5b8",
							"color"	: "#000"
						},
						
				};
				
				var detailColors = {
						"Name" :  {
							"bgcolor" : "#dee0d2",
							"color"	: "#000"
						},
						
						"Baseline Sets" : {
							"bgcolor" : "#e4e6f2",
							"color"	: ""
						},
						
						"Total <BR>$Value" : {
							"bgcolor" : "#f1f2f7",
							"color"	: "#000"
						},
						
						"#Cases<BR> per Month" : {
							"bgcolor" : "#E3F3FF",
							"color"	: "#000"
						},
						
						"#Cases<BR> per M/Set" : {
							"bgcolor" : "#D1DFF1",
							"color"	: "#000"
						},
						
						"Loaner case in period" : {
							"bgcolor" : "#c3c5b8",
							"color"	: "#000"
						},
						
						"Loaner <BR>Utilization %" : {
							"bgcolor" : "#c3c5b8",
							"color"	: "#000"
						},
						
						"AAS" : {
							"bgcolor" : "#fef2cc",
							"color"	: "#000"
						},
						
						"AAI" : {
							"bgcolor" : "#fef2cc",
							"color"	: "#000"
						},
						
						"STD" : {
							"bgcolor" : "#ffecb9",
							"color"	: "#000"
						},
						
						"% Additional" : {
							"bgcolor" : "#ffecb9",
							"color"	: "#000"
						},
						
						"Budget" : {
							"bgcolor" : "#c3c5b8",
							"color"	: "#000"
						},
						
						"AI Usage<BR>(Quarter)" : {
							"bgcolor" : "#c3c5b8",
							"color"	: "#000"
						},
						
						"% to <BR>AI Budget" : {
							"bgcolor" : "#c3c5b8",
							"color"	: "#000"
						},
						
				};

				for(var i=0;i<arrHeaders.length;i++) {
					var headerJSON = {}
					headerJSON["Label"] = arrHeaders[i];
					headerJSON["Color"] = (headerColors[arrHeaders[i]])?headerColors[arrHeaders[i]]["color"]:"";
					headerJSON["BgColor"] = (headerColors[arrHeaders[i]])?headerColors[arrHeaders[i]]["bgcolor"]:"";
					header.push(headerJSON);
				}
				
				var arrDetail = new Array();
				for(var j=0;j<details.length;j++) {
					var detail = new Array();
					for(var k=0;k<arrHeaders.length;k++) {
						var customDetail = {};
						customDetail["Color"] = (detailColors[arrHeaders[k]])?detailColors[arrHeaders[k]]["color"]:"";
						customDetail["BgColor"] = (detailColors[arrHeaders[k]])?detailColors[arrHeaders[k]]["bgcolor"]:"";
						var label = details[j][arrHeaders[k]];
						if(label==undefined || parseFloat(label)==0 || label == "") {
							label = "-";
						}
						else if(!_.contains(that.skipCalculations, arrHeaders[k])) {
							if(that.serverInput.contype=="50400") {
								if(!_.contains(that.skipDivide, arrHeaders[k] )) {
									label = label/1000;
									if(that.roundOffValues[arrHeaders[k]] != undefined) {
										label = label.toFixed(that.roundOffValues[arrHeaders[k]]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
									}
									else
										label = label.toFixed(1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
								}
							}
							else
								label = parseFloat(label).toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						}
						
						customDetail["Label"] = label;
						detail.push(customDetail);
					}
					arrDetail.push({"ID" : details[j]['ID'],"detail" : detail});
				}
				
				
				var arrFooter = new Array();
				footer[0]['Name'] = "Total";
				for(var j=0;j<footer.length;j++) {
					var detail = new Array();
					for(var k=0;k<arrHeaders.length;k++) {
						var customDetail = {};
						customDetail["Color"] = "#fff";
						customDetail["BgColor"] = "#000";
						var label = footer[j][arrHeaders[k]];
						if(label==undefined || parseFloat(label)==0 || label == "") {
							label = "-";
						}
						else if(!_.contains(that.skipCalculations, arrHeaders[k])) {
							if(that.serverInput.contype=="50400") {
								if(!_.contains(that.skipDivide, arrHeaders[k] )) {
									label = label/1000;
									if(that.roundOffValues[arrHeaders[k]] != undefined) {
										label = label.toFixed(that.roundOffValues[arrHeaders[k]]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
									}
									else
										label = label.toFixed(1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
								}
								else {
									label = parseFloat(label).toFixed(1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
								}
									
							}
							else
								label = parseFloat(label).toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						}
						
						
						customDetail["Label"] = label;
						detail.push(customDetail);
					}
					arrFooter.push({"ID" : footer[j]['ID'],"detail" : detail});
				}

				var viewJSON = {};
				viewJSON.header = header;
				viewJSON.details = arrDetail;
				viewJSON.footer = arrFooter;
				
				console.log(viewJSON);

				that.$("#ul-inv-report-" + that.key).html(that.template_list(viewJSON));
				
				that.showIcons();

			});
		},

		showIcons: function() {

			var arrDrillDownIcons = new Array();

			if (this.currServerInput.did=="" && this.currServerInput.adid=="" && (this.currServerInput.action=="LoadGCONS" || this.currServerInput.action=="LoadVP") ) {
				arrDrillDownIcons.push("ad");
			}

			if (this.currServerInput.did=="" && this.currServerInput.setnum =="") {
				arrDrillDownIcons.push(this.currServerInput.action);
			}


			if (this.currServerInput.action=="LoadAD" || this.currServerInput.action=="LoadVP") {
				arrDrillDownIcons.push("LoadGCONS");
				if (this.currServerInput.setnum=="") {
					arrDrillDownIcons.push("sys");
				}
			}


			if (this.currServerInput.contype!="50420" && this.currServerInput.action!="LoadVP" && this.currServerInput.salestype!="50302") 
			{
				arrDrillDownIcons.push("acc");		
				arrDrillDownIcons.push("rep");
			}

			if (this.currServerInput.contype!="50420" && this.currServerInput.action!="LoadAD" && this.currServerInput.action!="LoadVP" && (this.currServerInput.did!="" || this.currServerInput.setnum!="")&& this.currServerInput.salestype!="50302") {
				arrDrillDownIcons.push("fs"); 
				arrDrillDownIcons.push("cons"); 
				arrDrillDownIcons = _.without(arrDrillDownIcons,this.currServerInput.action);
			}

			var that = this;

			_.each(arrDrillDownIcons,function(icon) {
				switch(icon) {
					case "LoadDCONS":
						icon = "sys";
						break;

					case "LoadGCONS":
						icon = "fs";
						break;

					case "LoanerSet":
						icon = "set";
						break;

					case "LoanerDist":
						icon = "sys";
						break;
				}

				that.$(".td-index-0 .spn-inv-field-report-icon-" + icon).removeClass("inv-hide");

			});

		},
		
		closeConsignmentDetailTab: function() {
			this.$("#div-inv-consignment-detail-main").hide();
		},
		
		closeConsignmentPartDetailTab: function() {
			this.$("#div-inv-consignment-part-detail-main").hide();
		},

		loadDrillDown: function(event) {
			var icon = event.currentTarget;

			var input;
			switch(this.currServerInput.action) {
				case "LoadVP":
					this.currServerInput.vpid = $(icon).parent().parent().attr('id');
					this.breadCrumb.push($(icon).parent().text().trim().split('\n').slice(-1)[0].trim() + "(VP)");
					break;

				case "LoadAD":
					this.currServerInput.adid = $(icon).parent().parent().attr('id');
					this.breadCrumb.push($(icon).parent().text().trim().split('\n').slice(-1)[0].trim() + "(AD)");
					break;

				case "LoadDCONS":
					this.currServerInput.did = $(icon).parent().parent().attr('id');
					this.breadCrumb.push($(icon).parent().text().trim().split('\n').slice(-1)[0].trim() + "(FS)");
					break;

				case "LoadGCONS":
					console.log($(icon).parent().parent().attr('id'));
					this.currServerInput.setnum = $(icon).parent().parent().attr('id');
					this.breadCrumb.push($(icon).parent().text().trim().split('\n').slice(-1)[0].trim() + "(S)");
					break;
			}


			if(($(icon).hasClass("spn-inv-field-report-icon-rep")) || ($(icon).hasClass("spn-inv-field-report-icon-set"))) {
				this.$("#div-inv-report-field-content").hide();
				this.$("#div-inv-sub-report-field-content").show();
				this.$("#div-inv-consignment-detail-main").hide();
				if(this.subView)
					this.subView.close();
				this.subView = new FieldReportSubView({el: this.$("#div-inv-sub-report-field-content"), parent: this, triggeredEvent: event});
			}
			else if($(icon).hasClass("spn-inv-field-report-icon-cons")) {
				input = $.parseJSON(JSON.stringify(this.currServerInput));
				input.action = "LoadCons";
				this.loadConsignmentPopUp(input);
			}			
			else {
				this.$("#div-inv-consignment-detail-main").hide();
				if($(icon).hasClass("spn-inv-field-report-icon-ad")) 
					this.currServerInput.action = "LoadAD";
				else if($(icon).hasClass("spn-inv-field-report-icon-fs")) 
					this.currServerInput.action = "LoadDCONS";
				else if($(icon).hasClass("spn-inv-field-report-icon-sys")) 
					this.currServerInput.action = "LoadGCONS";

				
				this.arrServerInput.push(this.currServerInput);
				

				this.loadData();
			}			
		},
		
		loadConsignmentPopUp: function(input) {
			var that = this;
			this.$("#div-inv-consignment-detail-main").show();
			this.$("#div-inv-consignment-detail-list").html(this.tplSpinner({"message":"Fetching data..."}));


			fnGetServerData('reports/fetchconsigndetails', 'listGmConsignDetailVO',  input, function(data) {
				var viewData = new Array();

				if(input.stropt != undefined){
					that.popupBreadCrumb.push(that.setname);
					that.$("#div-inv-report-consignment-breadcrumb").html(that.popupBreadCrumb.join(" > "));
				} 
				else{
					that.$("#div-inv-report-consignment-breadcrumb").html(that.breadCrumb.join(" > "));
					that.popupBreadCrumb = $.parseJSON(JSON.stringify(that.breadCrumb));
					that.breadCrumb.pop();
				}

				console.log(that.popupBreadCrumb);
				
				if(data.length==undefined)
					viewData.push(data);
				else
					viewData = data;
				
				var total = 0;
				
				
				console.log(viewData);
				
				for(var i=0;i<viewData.length;i++) {
					var setID;
					if (that.currServerInput.salestype == "50301")
						setID  = viewData[i].vsetid;
			        else
			        	setID  = viewData[i].lsetid;

			        if ( parseInt(viewData[i].vsetidcount) > 1)
			        	setID = "-";
			        if (setID.indexOf("AA")==0 || setID.indexOf("AI")==0) 
			        	setID = "-";
			        viewData[i].setID = setID;
			        
			        if (viewData[i].vconsrptid=="20100")
			        	 viewData[i].vsetcount +=  " <font color=red>* </font>";
			        
			        if (viewData[i].sharedstatus!="0")
			        	viewData[i].vsetcount += " <font color=blue> ** </font>";
			        
			        
			        
			        if (viewData[i].salestype=="50301") {
			        	if (viewData[i].vsetcount=="0"){
			        		viewData[i].icon = "-";
			        	}
			        	if (parseFloat(viewData[i].vsetidcount) > 0){
			        		viewData[i].icon = "fa-info-circle";
			        	}
			        	else
			        	{
			        		viewData[i].icon = "fa-search";
			        	}
			        }
			        else
			        {
				       if (viewData[i].vsetcost=="0")
				       {
				    	   viewData[i].icon =  "-";
				       }
				       if ((viewData[i].lsetid).indexOf("AI")==0 || (viewData[i].lsetid).indexOf("AA")==0) 
				       {
				    	   viewData[i].icon = "fa-search";
				       }
				       else
				       {
				           
				            	if(parseFloat(viewData[i].vsetidcount) > 1){
				            		viewData[i].icon =  "fa-info-circle";
				            	}
				            	else
				            	{
				            		viewData[i].icon = "fa-search";	                
				            	}
				        }
			        }
			        
			        total += parseFloat(viewData[i].vsetcost);
			        viewData[i].vsetcost = "$ " + parseFloat(viewData[i].vsetcost).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				}
				that.$("#div-inv-consignment-detail-footer-value").text("$ " + total.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
				
				that.$("#div-inv-consignment-detail-list").html(that.template_consignmentlist(viewData));

			});
		},
		
		loadConsignmentDetail: function(event) {
		
			var id= $(event.currentTarget).parent().parent().attr('id');
			this.setname = $(event.currentTarget).parent().parent().attr('setname');
			if($(event.currentTarget).hasClass("fa-search")) {
				
				var input = {
					"token" : localStorage.getItem("token"),
					"action" : "BYSETPART",
					"did" : this.currServerInput.did,
					"setid" : id
				};
				
				this.loadConsignmentPartPopUp(input);
				
			}
			else {
				
				var input = $.parseJSON(JSON.stringify(this.currServerInput));
				input.setnum = id;
				input.stropt = "BYSETPART";
				input.action = "LoadCons";

				this.loadConsignmentPopUp(input); 
			}
		},
		
		loadConsignmentPartPopUp: function(input) {
			var that = this;
			console.log(input.setid);
			this.$("#div-inv-consignment-detail-main").hide();
			this.$("#div-inv-consignment-part-detail-main").show();
			this.$("#div-inv-consignment-part-detail-list").html(this.tplSpinner({"message":"Fetching data..."}));
			fnGetServerData('reports/consigndetailbypart', "listGmConsignDetailByPartVO",  input, function(data) {
				that.popupBreadCrumb.push(that.setname);
				 that.$("#div-inv-report-consignment-part-breadcrumb").html(that.popupBreadCrumb.join(" > "));
				
				
				var totallstvalue = 0, totalqtyconsigned = 0;
				var viewData = new Array();
				if(data.length==undefined)
					viewData.push(data);
				else
					viewData = data;
				for(var i=0;i<viewData.length;i++) {
					totalqtyconsigned += parseFloat(viewData[i].setusage);
					totallstvalue += parseFloat(viewData[i].listvalue);
			        viewData[i].listvalue = "$ " + parseFloat(viewData[i].listvalue).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			        if(viewData[i].crtfl=="N")
			        	delete viewData[i].crtfl;
				}
				that.$("#div-inv-consignment-part-detail-footer-totalqty").text(totalqtyconsigned);
				that.$("#div-inv-consignment-part-detail-footer-totallist").text("$ " + totallstvalue.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
				that.$("#div-inv-consignment-part-detail-list").html(that.template_consignmentpartlist(viewData));
			});
			
		},

		showAiData: function(event) {
			var elem = event.currentTarget;
			if($(elem).hasClass("fa-square-o"))
				this.serverInput.showai = "Y";
			else
				this.serverInput.showai = "";
			$(elem).toggleClass("fa-square-o fa-check-square-o");
		},

		display: function(key) {
			
			if(key.currentTarget==undefined) {
				this.key = key;
			}else{
				this.key = "consign-info";
			}
			this.$(".div-inv-report-settype-label").each(function() {
				$(this).removeClass("selected-tab");
			});

			this.$("#li-inv-report-showall").show();

			if(key=="loaner-info")
				this.$("#li-inv-report-showall").hide();
			
			this.$("#div-inv-report-" + this.key).addClass("selected-tab");
			this.$(".ul-inv-report-info").hide().html("");
			this.$("#ul-inv-report-" + this.key).show();

		},

		toggleContent: function (event) {
			var tabid;
			if(!event) 
				tabid = "div-inv-report-consign-info";
			else
				tabid = (event.currentTarget.id);
			  
			switch(tabid) {
				case "div-inv-report-consign-info":
					this.display("consign-info");
					this.serverInput.salestype = "50300";
					this.serverInput.baselinenm = "Baseline Sets";
					this.$("#div-inv-report-field-content").show();
					this.$("#div-inv-sub-report-field-content").hide();
					this.$("#div-inv-report-load").trigger("click");
					this.breadCrumb = []; 
					break;	

				case "div-inv-report-loaner-info":
					this.display("loaner-info");
					this.serverInput.salestype = "50301";
					this.serverInput.baselinenm = "Loaner Usage";
					this.$("#div-inv-report-field-content").show();
					this.$("#div-inv-sub-report-field-content").hide();
					this.$("#div-inv-report-load").trigger("click");
					this.breadCrumb = [];
				break;
						
				case "div-inv-report-all-info":
					this.display("all-info");
					this.serverInput.salestype = "50302";
					this.serverInput.baselinenm = "Total Sets";
					this.$("#div-inv-report-field-content").show();
					this.$("#div-inv-sub-report-field-content").hide();
					this.$("#div-inv-report-load").trigger("click");
					this.breadCrumb = [];
					break;

				}

		},
		
		moreFilterModified: function() {
			var savedZone = (this.morefilterZoneid)?this.morefilterZoneid:"", savedRegion = (this.morefilterRegionid)?this.morefilterRegionid:"", savedFieldSales = (this.morefilterFieldsalesid)?this.morefilterFieldsalesid:"";
			var savedCompany = (this.morefilterCompanyid)?this.morefilterCompanyid:"", savedDivision = (this.morefilterDivisionid)?this.morefilterDivisionid:"";
			var userCompany = localStorage.getItem("userCompany"), userDivision = localStorage.getItem("userDivision");
			
			
			if((savedZone!="") || (savedRegion!="") || (savedFieldSales!="")) {	
				$("#btn-inv-more-filter").addClass("bg-red");
				
			}
			else if((savedCompany!="") || (savedDivision!="")) {
				if((savedCompany!=userCompany) || (savedDivision!=userDivision)) {
					$("#btn-inv-more-filter").addClass("bg-red");
				}
				else
					$("#btn-inv-more-filter").removeClass("bg-red");
			}
			else
				$("#btn-inv-more-filter").removeClass("bg-red");
			
			this.unbind();
			this.initialize(this.options);
		},

		close: function() {
			this.undelegateEvents();
			this.unbind();
		}
	});	

	return InvFieldReportView;
});
