/**********************************************************************************
 * File:        GmInvPartsUsedView.js
 * Description: 
 * Version:     1.0
 * Author:     	asivaraj
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification','searchview','saleitemmodel', 'saleitemcollection', 'saleitemlistview','gmvSearch'
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification, SearchView, SaleItem, SaleItems, SaleItemListView, GMVSearch ) {
        	
	'use strict';
	var InvPartsUsedView = Backbone.View.extend({

		events : {
			"click .div-inv-parts-used-shipdt-label label, .div-inv-parts-used-shipdt-label i" : "showDatepicker",
			"click .clear-row-pu": "clearRow",
			"change .txt-part-plannedship-date" : "getChangedShipDate",
			"click .div-inv-keypad-values" : "showNumberKeypad",
			"click .plandshipdte-checkbox" : "applyAllPlannedShipDate",
			"click .ra-checkbox" : "updateFlag",
            "click .qty-plus-pu" : "incQty",
            "click .qty-minus-pu" : "decQty",
            "click .div-inv-favourite" : "favpopup",
		},

		/* Load the templates */ 
		template: fnGetTemplate(URL_Inv_Template, "GmInvPartsUsed"),
		template_partdetails: fnGetTemplate(URL_Inv_Template, "GmInvPartsUsedDetails"),
		template_partdetailsMobile: fnGetTemplate(URL_Inv_Template, "GmInvPartsUsedDetailsMobile"),
		template_setpartdetails: fnGetTemplate(URL_Inv_Template, "GmInvSetPartDetails"),
		template_setpartdetailslist: fnGetTemplate(URL_Inv_Template, "GmInvSetPartDetailsList"),
		template_favSearch:fnGetTemplate(URL_Inv_Template, "GmInvAICFavSearch"),
        
		initialize: function(options) {
			this.parent = options.parent;
			this.requesttype = options.requesttype;
			this.render(options);
            this.chkFavPartQty = '';
            this.favIpt = '';
		},

		// Render the initial contents when the View is instantiated
		render: function (options) {
//			showMsgView("011");
			var that = this;
			this.$el.html(this.template());
            
            //Load the parts initially when come from marketing collateral module(PC-2428).
            if (GM.Global.ShipPartsUsedFl == "Y") {
                console.log(JSON.stringify(that.parent.JSONData));
                $("#div-inv-surgery-info").html(lblNm("request_info"));
                $("#div-inv-sets-used").html(lblNm("part_details"));
                $("#div-inv-comments").show();
                $("#div-inv-surgery-info").trigger("click");
                $("#label-inv-surgery-info-surgery-date").html(that.parent.JSONData.reqdt);
                $("#label-inv-surgery-info-plandship-date").html(that.parent.JSONData.reqdt);  
                $(".ul-inv-detail-info").hide();
                $("#div-inv-surgery-info").removeClass("selected-tab");
                $("#div-inv-sets-used").addClass("selected-tab");
                $("#ul-inv-detail-sets-used").show();
                $("#btn-backward").show();
                
                $("#btn-inv-reset").on("click",function(){
                    that.parent.cancelInv();
                });
                
                $("#btn-inv-delete").on("click",function(){
                    that.parent.deleteInv();
                });
                
                $("#btn-back").on("click",function(e){
                    e.stopPropagation();
                    window.location.href = "#collateral";
                });
                
                fnGetSharedDocuments(function (data) {
                    console.log(JSON.stringify(data));
                    var partNumArr = [];
                    _.each(data, function (dt) {
                        partNumArr.push(dt.PartNum);
                        console.log(partNumArr);
                    });
                    setTimeout(function(){
                        var stringPartNum = partNumArr.join("','");
                        fnGetPartsInfo(stringPartNum, function (sList) {
                            console.log(JSON.stringify(sList));
                            _.each(sList,function(item){
                                var rowEl = $("<a id=" + item.ID + " class=item-searchlist name=" + item.Name + " productmaterial=" + item.ProductMaterial + " productfamily=" + item.ProductFamily + " productclass=" + item.ProductClass + "><div class='div-data hide'></div> <i class='fa fa-check'></i></a>");
                                var CurrentRow = {
                                    "ID": item.ID,
                                    "Name": item.Name,
                                    "Element": rowEl
                                };
                                that.getItemsFromSearchView(CurrentRow);
                                GM.Global.ShipPartsUsedFl = "";
                            });
                        });
                    });
                });
            }
			var renderOptions = {
							"el":this.$('#div-inv-parts-used-txtkeyword'),
							"ID" : "txt-keyword",
							"Name" : "txt-search-name",
							"Class" : "txt-keyword-initial",
							"minChar" : "3",
							"autoSearch": true,
							"placeholder": lblNm("msg_character_search"),
							"fnName": fnGetDOParts,
							"usage": 2,
							"template" : 'GmSearchResult',
							"template_URL" : URL_Common_Template,
							"callback": function(sList) {
                                    that.chkFavPartQty = 'N';
                                    that.getItemsFromSearchView(sList);
                                }
							};
			
			var searchview = new SearchView(renderOptions);
			this.displayParts(this.parent.JSONSets);
            this.fnGetDefaultRAfl(function(){});
            this.fnGetFavfl(function(){});
            this.comnMobFunc();
			return this;
		},

		updateFlag: function(e) {
			var parent = $(e.currentTarget).parent().parent();
			$(parent).addClass("part-updated");
			this.updateJSON();
		},

		//Removes the Part Information if clicked on Minus icon
		clearRow: function(e){
			
			if(!this.parent.partsChangeMode) {
				$(e.currentTarget).parent().parent().remove();
				this.updateJSON();
				
			}
			else {
				
				var index = $(e.currentTarget).parent().parent("ul").index();
				var indexValue = $(e.currentTarget).parent().parent().attr('indexValue');
				this.parent.JSONData.arrGmCaseSetInfoVO.splice(index,1);

				this.parent.JSONData.arrGmPartDetailVO = _.reject(this.parent.JSONData.arrGmPartDetailVO, function(arr){ return parseInt(arr.indexValue) == parseInt(indexValue); });
				this.parent.JSONData.gmTxnShipInfoVO = _.reject(this.parent.JSONData.gmTxnShipInfoVO, function(arr){ return parseInt(arr.indexValue) == parseInt(indexValue); });
				this.parent.JSONSets = _.reject(this.parent.JSONSets, function(arr){ return parseInt(arr.indexValue) == parseInt(indexValue); });
				$(e.currentTarget).parent().parent().remove();
				
				
			}
		},

		//Fired when part is selected from the search
		getItemsFromSearchView: function(sList) {
			var that=this;
			var arrSelectedData = new Array();
			if(sList.length==undefined)
				arrSelectedData.push(sList);
			else
				arrSelectedData = sList;

			$("#div-inv-parts-list").show();
			$("#div-inv-parts-used").find("#div-select-all").addClass("hide");
			$(".ul-inv-parts-list-header").show();
			$("#div-itemlist").html("<li>"+lblNm("search_again")+"</li>");
			$("#txt-keyword").val('');
			$("#div-common-search-x").addClass('hide');
			this.loadParts(0,arrSelectedData);
		},
		
		//loads the selected parts in the list.
		//Called Recursively - to fetch the Price for each Part
		loadParts: function(iValue, data) {
            var that = this;
			if(iValue<data.length) {
                if(that.chkFavPartQty == 'Y'){
                    data[iValue].qty = data[0].qty;
                }else{
                    data[iValue].qty = 1;
                }
				console.log($(data[iValue].Element).attr("productfamily"));
				data[iValue].productfamily = $(data[iValue].Element).attr("productfamily");
                data[iValue].productmaterial = $(data[iValue].Element).attr("productmaterial");
                if($(window).width() < 480){
                    this.$("#div-inv-parts-list").append(this.template_partdetailsMobile(data[iValue]));
                } else{
                    this.$("#div-inv-parts-list").append(this.template_partdetails(data[iValue]));
                }
				
				this.loadParts(iValue+1,data);
               

			} else{
			//to set default RA checkbox enabled
                this.fnGetDefaultRAfl(function(data){
                    that.updateJSON();
                });
				
			}
		    this.comnMobFunc();
            
		},
        
        comnMobFunc: function(){
            this.$("#div-inv-part-used").find(".invPartUsedDls").each(function(){
                if($(this).next().is('ul')){
                    $(this).addClass("ul-show-inv-list");
                    $(this).children(".togglebx").children("img").removeClass("onActive");
                }
                else{
                    $(this).removeClass("ul-show-inv-list");
                    $(this).children(".togglebx").children("img").addClass("onActive");
                }
                    
            });
            
            $(".togglebx").unbind('click').bind('click', function () {
                 $(this).parent(".invPartUsedDls").toggleClass('ul-show-inv-list');
                 $(this).children("img").toggleClass("onActive");
             });

             $(".invPartUsedDls").on('swipeleft', function (e, data) {
                 $(this).find(".delete-pu-span").addClass("onShowEl");
             });


             $(".invPartUsedDls").on('tap', function (e, data) {
                 $(this).find(".delete-pu-span").removeClass("onShowEl");
             });
        },
        
        incQty: function(e){
            var that = this;
            var count = parseInt($(e.currentTarget).siblings("#div-inv-qty-box").text());
            var countEl = $(e.currentTarget).siblings("#div-inv-qty-box");
                count++;
                countEl.text(count);
                $(e.currentTarget).parent().parent().find(".inv-qty").text(count);
                if(that.parent.partsChangeMode)
				    $(e.currentTarget).closest('.ul-inv-parts-used-list').addClass('part-updated');
                that.updateJSON();
        },
        
        decQty: function(e){
            var that = this;
            var count = parseInt($(e.currentTarget).siblings("#div-inv-qty-box").text());
            var countEl = $(e.currentTarget).siblings("#div-inv-qty-box");
             if (count > 1) {
                count--;
                countEl.text(count);
                $(e.currentTarget).parent().parent().find(".inv-qty").text(count);
                if(that.parent.partsChangeMode)
				   $(e.currentTarget).closest('.ul-inv-parts-used-list').addClass('part-updated');
                that.updateJSON();
             }  
        },

		//Used to display Part information if loaded from the Booked DO's (Draft)
		displayParts: function(data) {
			var that =this;
			$("#div-inv-parts-list").show();
			$("#div-inv-parts-used").find("#div-select-all").addClass("hide");
			$(".ul-inv-parts-list-header").show();
            if($(window).width() < 480){
                for (var i = 0; i < data.length; i++) {
                    this.$("#div-inv-parts-list").append(this.template_partdetailsMobile(data[i]));
                }
            } else{
                for (var i = 0; i < data.length; i++) {
                    this.$("#div-inv-parts-list").append(this.template_partdetails(data[i]));
                }
            }
			
			
		},

		

		updateJSON :function(e){
			if(!this.parent.partsChangeMode) 
				this.parent.JSONSets =[];
			var MainJSONData = new Array();
			

			if(this.parent.partsChangeMode) 
				MainJSONData = $.parseJSON(JSON.stringify(this.parent.JSONData.arrGmCaseSetInfoVO));
			
			var that = this;
			var id,name,qty,date,reqdt,systype, retnFlag, prodFamily,tissueFlag;
			date = $("#label-inv-surgery-info-plandship-date").text();
			reqdt = $("#label-inv-surgery-info-surgery-date").text();
			this.parent.JSONData.type = this.requesttype;

			if(this.requesttype == 1006507 || this.requesttype == 107286)//Account Item Consignment
				systype = "103644"; // Inv Mgmt item Request

			this.$("#div-inv-parts-list").find(".ul-inv-parts-used-list").each(function(index) {
				id = $(this).attr('id');
				name = $(this).find(".li-inv-parts-used-id").text();
				qty = $(this).find(".div-inv-keypad-values").text().trim();
				retnFlag = "";
				prodFamily = $(this).attr('productfamily');
                tissueFlag = $(this).attr('tissuefl');

				if($(this).find(".li-inv-ra-checkbox").find('input:checked').val()) {
					retnFlag = "Y";
				}

				if(!that.parent.partsChangeMode) { 
					that.parent.JSONSets.push({"ID" :id, "Name" : name, "qty" : qty, "retnFlag": retnFlag, "productfamily" : prodFamily, "planshipdt" : date, "indexValue" : index.toString(),"tissueFlag": tissueFlag});
					MainJSONData.push({"casetid" : "","cainfoid"  : "", "setid" : "", "systp" : systype, "reqdt" : reqdt, "planshipdt" : date,"indexValue" : index.toString()});
					that.parent.JSONData.arrGmCaseSetInfoVO = MainJSONData;
					that.parent.JSONData.arrGmCaseSetInfoVO[index].arrGmPartDetailVO = [];				
					that.parent.JSONData.arrGmCaseSetInfoVO[index].arrGmPartDetailVO.push({"pnum" :id,"qty" : qty, "retnFlag" : retnFlag, "tissueFlag": tissueFlag});
					console.log(MainJSONData);
					$(this).attr('indexValue', index);
				
				}else{
					
					if($(this).hasClass('part-updated')) {
						var indexValue = $(this).attr('indexValue');
						var dupJSONSets = $.parseJSON(JSON.stringify(that.parent.JSONSets));
						var dupJSONParts = $.parseJSON(JSON.stringify(that.parent.JSONData.arrGmCaseSetInfoVO));
						for(var i=0;i<dupJSONSets.length;i++) {
							if(parseInt(dupJSONSets[i].indexValue) == parseInt(indexValue)) {
								that.parent.JSONSets[i].qty = qty;
								that.parent.JSONSets[i].ID = id;
								that.parent.JSONSets[i].Name = name;
								that.parent.JSONSets[i].retnFlag = retnFlag;
								that.parent.JSONSets[i].productfamily = prodFamily;
                                that.parent.JSONSets[i].tissueFlag = tissueFlag;
							}
						}
                        
                        for(var i=0;i<dupJSONParts.length;i++) {
							if(parseInt(dupJSONParts[i].indexValue) == parseInt(indexValue)) {
								that.parent.JSONData.arrGmCaseSetInfoVO[i].casetid = "";
								that.parent.JSONData.arrGmCaseSetInfoVO[i].cainfoid = "";
								that.parent.JSONData.arrGmCaseSetInfoVO[i].systp = systype;
								that.parent.JSONData.arrGmCaseSetInfoVO[i].reqdt = reqdt;
								that.parent.JSONData.arrGmCaseSetInfoVO[i].planshipdt = date;
                                _.each(that.parent.JSONData.arrGmCaseSetInfoVO[i].arrGmPartDetailVO,function(dt){
                                    dt.qty = qty;
                                    dt.retnFlag = retnFlag;
                                });
							}
						}
                        

					} else if(($(this).attr('indexValue')==undefined) || ($(this).attr('indexValue')=="") ) {

					var newIndex;
						if(that.parent.JSONSets.length>0)
							newIndex = parseInt(_.max(_.pluck(that.parent.JSONSets,'indexValue'), function(val){ return parseInt(val); })) + 1;
						else
							newIndex = 0;
						
						that.parent.JSONSets.push({"ID" :id, "Name" : name, "qty" : qty, "retnFlag": retnFlag, "productfamily": prodFamily, "planshipdt" : date, "indexValue" : newIndex.toString(), "tissueFlag": tissueFlag});
						MainJSONData.push({"casetid" : "","cainfoid"  : "", "setid" : "", "systp" : systype, "reqdt" : reqdt, "planshipdt" : date,"indexValue" :  newIndex.toString()});
						that.parent.JSONData.arrGmCaseSetInfoVO.push({"casetid" : "","cainfoid"  : "", "setid" : "", "systp" : systype, "reqdt" : reqdt, "planshipdt" : date,"indexValue" :  newIndex.toString()});


						that.parent.JSONData.arrGmCaseSetInfoVO[that.parent.JSONData.arrGmCaseSetInfoVO.length-1].arrGmPartDetailVO = [];				
						that.parent.JSONData.arrGmCaseSetInfoVO[that.parent.JSONData.arrGmCaseSetInfoVO.length-1].arrGmPartDetailVO.push({"pnum" :id,"qty" : qty, "retnFlag" : retnFlag,"tissueFlag": tissueFlag});
						$(this).attr('indexValue', newIndex);
					}
				}
			});
			
			
			this.$('.part-updated').removeClass('part-updated');
		},

		//Displays the Number keypad 
		showNumberKeypad:function(event){
			
			var that = this;
			this.parent.showNumberKeypad(event, function(val, target) {
				if(event.currentTarget.id == "div-inv-qty-box"){
					if(val == "0" || val == "00" || val == "000"){
					}else {
						if(that.parent.partsChangeMode)
							$(event.currentTarget).closest('.ul-inv-parts-used-list').addClass('part-updated');
					$(event.currentTarget).text(val);
                    $(event.currentTarget).parent().parent().find(".inv-qty").text(val);
					}
				}else {
					if($("#div-qty-entry-text").text() != "")
					$(event.currentTarget).attr("value",val);
					if(that.parent.partsChangeMode)
						$(event.currentTarget).closest('.ul-inv-parts-used-list').addClass('part-updated');
                    $(event.currentTarget).parent().parent().find(".inv-qty").text(val);
				}
				that.updateJSON();
			
			});
		},
        
        // Function is used to enable RA flag as default or not 
        fnGetDefaultRAfl :function(cb){
            var that = this;
            fnGetRuleValueByCompany("ITEM_REQ_RA_FL","CONTAB",localStorage.getItem("cmpid"),function(data){
                
             if (data.length > 0) {
             if(data[0].RULEVALUE=='Y'){
                  that.$(".li-inv-ra-checkbox").each(function() {
                      $(this).find('input').attr("checked","checked");
                      $(this).find('input').attr("disabled","disabled");
                  });
                }
             }
              cb(data);
            }); 
        },
        
        //Get Fav flag based on cmpid and active the star icon if favorite kit available
        fnGetFavfl :function(cb){
            var that = this;
            fnGetRuleValueByCompany("ITEM_REQ_FAV_FL","CONTAB",localStorage.getItem("cmpid"),function(data){
                
             if (data.length > 0) {
                 $("#div-inv-parts-used-fav-icon").removeClass("hide");
             if(data[0].RULEVALUE=='Y'){
                var strOpt = "mykit";
                var userID = localStorage.getItem("userID");
                var partyID = localStorage.getItem("partyID");
                var input = {};
                 
                input.userid = userID;
                input.partyid = partyID;
                input.strOpt = strOpt;
                input.kitnm = "";

                fnGetWebServerData('kit/report', undefined, input, function (rptdata) {
                        if (rptdata.length > 0) {
                            $("#div-inv-parts-used-fav-icon").find("i").addClass("div-inv-favourite");  
                        }
                     });
                 
                  
                }
             }
              cb(data);
            }); 
        },
        
        //Favorite Common Popup
        favpopup: function(){
            var that = this;
            that.showFavpopup("favPopup", "Favourites", "shareContent");
            that.favCommon();
        },
        
        //Show Favorite Popup
        showFavpopup:function (containerDiv, titleText, contentID) {
            var that = this;
                showPopup();
                $("#div-crm-overlay-content-container").addClass(containerDiv);
                $("#div-crm-overlay-content-container").find(".modal-footer").addClass("hide");
                $("#div-crm-overlay-content-container").find('.modal-title').text(lblNm("favourites"));

                $("#div-crm-overlay-content-container .modal-body").html("<div id='" + contentID + "'></div>");
                $("." + containerDiv + " .close").unbind('click').bind('click', function (e) {
                    that.hideFavpopup(containerDiv);
                });
        },
        
        //hide Favorite Popup
        hideFavpopup: function (containerDiv) {
                var that = this;
                hidePopup();
                $("#div-crm-overlay-content-container").removeClass(containerDiv);
                $("#div-crm-overlay-content-container").find(".modal-footer").removeClass("hide");
                $("#div-crm-overlay-content-container").find("#div-crm-popup-msg").html("");
                $("#div-crm-overlay-content-container").find("#div-crm-popup-msg").hide();
        },
        
        //Favorite Common search
         favCommon: function () {
                var that = this;
                $("#shareContent").html(that.template_favSearch());
                that.searchFavs();
        },
        
        //Search Favorites using part kittype ---107923
        searchFavs: function () {
                var that = this;
                var searchOptions = {
                    'el': $("#div-search-fav"),
                    'url': URL_WSServer + 'cmsearch/kitname',
                    'inputParameters': {
                        'token': localStorage.getItem('token'),
                        'userid': localStorage.getItem('userID'),
                        'cmpid': localStorage.getItem('cmpid'),
                        'partyid': localStorage.getItem('partyID'),
						'acid': localStorage.getItem('AcID'),
						'type': "107923", // part kittype --- 107923
                    },
                    'keyParameter': 'srchkey',
                    'IDParameter': 'ID',
                    'NameParameter': 'NAME',
                    'resultVO': undefined,
                    'usage': 2,
                    'minChar': 3,
                    'callback': function (result) {
                        _.each(arrayOf(result), function (data) {
                            var input = {
                                "token": localStorage.getItem("token"),
                                "partyid": localStorage.getItem("partyID"),
                                "kitid": data.ID
                            };
                            that.getKitPartList(input);
                        });
                        
                        $(".favPopup .close").trigger("click");
                        $("#div-search-fav").find("input").val("");
                    }
                };
                var searchView = new GMVSearch(searchOptions);
                $("#div-search-fav").find("input").attr('placeholder', $('<div />').html('&#xF002;  '+lblNm("search_by_kit_name")).text());
        },
        
        //Get Part List from Favorites
        getKitPartList: function (kitinput) {
            var that = this;
            
            fnGetWebServerData("kit/fetchkit", undefined, kitinput, function (data) {
                if (data != null) {
                    that.chkFavPartQty = 'Y';
                    _.each(data.arrkitpartvo, function (item) {
                        var setPartObj = {};
                        setPartObj.ID = item.partnum;
                        setPartObj.Name = item.partnum + " " + item.partdesc;
                        setPartObj.qty = item.qty;
                        that.getItemsFromSearchView(setPartObj);
                    });
                }
            });

        },
		close: function() {
			this.unbind();
			this.undelegateEvents();
		}
		
	});	

	return InvPartsUsedView;
});