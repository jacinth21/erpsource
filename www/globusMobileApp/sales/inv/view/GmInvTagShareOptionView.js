/**************************************************************************************************************************
 * File:        GmInvTagShareOptionView.js
 * Description: Tag Share Option View - select Sharing Options of the Tag available on the field.
 * Version:     1.0
 * Author1:     Karthik Somanathan
 ***************************************************************************************************************************/

define([
        'jquery', 'underscore', 'backbone', 'handlebars',
        'global', 'jqueryui', 'dropdownview', 'daoinv', 'searchview', 'invmorefilterview',  'loaderview'
        ],
    function ($, _, Backbone, Handlebars, Global, JqueryUI, DropDownView, DAOInv, SearchView, MoreFilterView, LoaderView) {

        'use strict';
        var arrData = new Array();
        var InvTagShareOptionView = Backbone.View.extend({

            events: {
                  "click #div-inv-report-load-new": "loadTagShareRptDetails",
            },

           template: fnGetTemplate(URL_Inv_Template, "GmInvSetReqBorrowRpt"), //use the same template of inv system locator report

            initialize: function (options) {
                this.parent = options.parent;
                this.options = options;
                this.tagsysid = "";
			    this.tagsetid = "";
                this.render(options);
            },

            // Render the initial contents when the View is instantiated
            render: function (options) {
                var that = this;
                this.$el.html(this.template());
                this.loadTagShareOptSystemInfo();
                this.loadTagShareOptSetInfo();
                this.loadTagShareOptionMoreFilterVal();
                this.fnhideSystemLocatorScreenElements();
                return this;
            },
            
            //load system info in combo box
            loadTagShareOptSystemInfo : function () {
                var that = this;
                var systemdata = [];
                var comboSystem;
                fnGetSystemSearchInfo(function (data) {
                    if (data != null){
                        systemdata = data;
                    }
                    comboSystem = new dhx.Combobox("#div-inv-set-report-sysnm-list", {
                        multiselection: true,
                        virtual: true,
                        data: systemdata,
                        placeholder: "Search System Name...",
                        css:"search_box_req_borrow"
                    });              
                    comboSystem.events.on("change", function (id) {
                        that.tagsysid=comboSystem.getValue(true);
                    });
                    
                });
            },
            
            //load set info in combo box
            loadTagShareOptSetInfo  : function(){
                var that = this;
                var setdata = [];
                var comboSet;
                fnGetInvSetsSearchInfo(function (data) {                    
                    if (data != null){
                        setdata = data;
                    }
                    comboSet = new dhx.Combobox("#div-inv-set-report-setnm-list", {
                        multiselection: true,
                        virtual: true,
                        data: setdata,
                        placeholder: "Search Set Name...",
                        css:"search_box_req_borrow"
                    });               
                    comboSet.events.on("change", function (id) {
                        that.tagsetid=comboSet.getValue(true);
                    });
                    
                });  
            },
            
            //Since we are using the same template of system locator (borrow) report screen so hide the necessary elements in this report.
            fnhideSystemLocatorScreenElements: function () {
                this.$("#btn-new-inv-more-filter").addClass("hide");
                this.$("#btn-inv-toggle").addClass("hide");
                this.$("#btn-inv-request-borrow").addClass("hide");
            },
            
            
            //Load tag details report in  grid
            loadTagShareRptDetails: function(){
                var that = this;
                var setidStr = "";
                if((that.tagsetid != "" && that.tagsetid != undefined ) || (that.tagsysid != "" && that.tagsysid != undefined ))
                {
                  if((that.tagsetid != "" && that.tagsetid != undefined)){
                     setidStr = "'"+that.tagsetid.join("','")+"'";
                   }
                    
                   this.serverInput = {
                       "token": localStorage.getItem('token'),
                       "companyid": (this.morefilterCompanyid) ? this.morefilterCompanyid : localStorage.getItem("userCompany"),
                       "divids": (this.morefilterDivisionid) ? this.morefilterDivisionid : localStorage.getItem("userDivision"),
                       "zoneids": (this.morefilterZoneid) ? this.morefilterZoneid : "",
                       "regionids": (this.morefilterRegionid) ? this.morefilterRegionid : "",
                       "fsids": (this.morefilterFieldsalesid) ? this.morefilterFieldsalesid : "",
                       "setid": setidStr,
                       "system": that.tagsysid.toString()
                   };

                  console.log(this.serverInput);
                    
                   that.loaderview = new LoaderView({
                        text: "Loading Tag Details..."
                   });
                    
                   $("#invSetReqBorrowList .dhx_header-wrapper").css('z-index','0');
                    
                  fnGetWebServerData('invTagShareOption/loadTagDtl', undefined, this.serverInput, function (data) {
                    that.loaderview.close();
                    $("#invSetReqBorrowList .dhx_header-wrapper").css('z-index','11');
                      
                    if(data != null){
                        $(".dhx_grid.dhx_widget").remove();
                        $(".inv-no-data-text").remove();
                       
                        var grid = new dhx.Grid("invSetReqBorrowList", {
                            columns: [
                                { width: 120, id: "tagid", header: [{ text: "Tag ID"},{content: "inputFilter"}]},
                                { width: 250, id: "distnm", header: [{ text: "Field Sales Name"},{ content: "inputFilter" }]},
                                { width: 100, id: "setid", header: [{ text: "Set ID"},{ content: "inputFilter" }]},
                                { width: 250, id: "setnm", header: [{ text: "Set Name"},{ content: "inputFilter" }]},
                                { width: 80,  id: "invshare", class: "invshare", header: [{ text: "Share Options", colspan: 3 },{ text: "Share"}], type: "boolean"},
                                { width: 120, id: "invpromoteshare", class: "invshare", header: ["",{ text: "Promote Share"}], type: "boolean"},
                                { width: 90,  id: "invdontshare", class: "invshare", header: ["",{ text: "Don't Share"}], type: "boolean"},
                            ],
                            data: data,
                            adjust: true,
                            editable: true,
//                            splitAt:3,  
                            height: 400
                            });
                        
                        //Tag Share Options will update to Server when we click the check box
                        grid.events.on("cellclick", function (row, column, conf) {
                            if (column.id === "invshare") {
                                if (row.invshare == 0) {
                                    grid.data.update(row.id, {
                                        invpromoteshare: 0
                                    });

                                    grid.data.update(row.id, {
                                        invdontshare: 0
                                    });
                                    that.updateTagShareOptions(row.tagid,111141);
                                } else {
                                     dhtmlx.alert({
                                        title: "Alert",
                                        type: "alert-error",
                                        text: "you must select at least one share option(check box) for a tag"
                                    }); 
                                    grid.data.update(row.id, {
                                        invshare: 1
                                    });
                                }
                            }
                            
                            if (column.id === "invpromoteshare") {
                                if (row.invpromoteshare == 0) {
                                    grid.data.update(row.id, {
                                        invshare: 0
                                    });

                                    grid.data.update(row.id, {
                                        invdontshare: 0
                                    });
                                    that.updateTagShareOptions(row.tagid,111140);
                                }else{
                                     dhtmlx.alert({
                                        title: "Alert",
                                        type: "alert-error",
                                        text: "you must select at least one share option(check box) for a tag"
                                    });
                                }
                            }

                            if (column.id === "invdontshare") {
                                if (row.invdontshare == 0) {
                                    grid.data.update(row.id, {
                                        invshare: 0
                                    });

                                    grid.data.update(row.id, {
                                        invpromoteshare: 0
                                    });
                                    that.updateTagShareOptions(row.tagid,111142);
                                }else{
                                     dhtmlx.alert({
                                        title: "Alert",
                                        type: "alert-error",
                                        text: "you must select at least one share option(check box) for a tag"
                                    });
                                }
                            }
                        });
                          
                    } else {
                        $("#invSetReqBorrowList").html("<span class='inv-no-data-text gmTextCenter gmAlignVertical'>No Data Found</span>");
                    }       
                });
             }else{
                 dhtmlx.alert({
                        title: "Alert",
                        type: "alert-error",
                        text: "Please choose atleast one filter to load"
                    }); 
             }
            },
            
            loadTagShareOptionMoreFilterVal : function(){
                 var that = this;
                this.input = {
                    "token": localStorage.getItem('token')
                };
                fnGetServerData("salesfilter/morefilter", undefined, this.input, function (data) {
                    //seperates the data
                    that.comapanyData = that.processInitTagShareMoreFilterData(data.listGmCompanyListVO, "company");
                    that.divisionData = that.processInitTagShareMoreFilterData(data.listGmSlsDivisionListVO, "division");
                    that.zoneData = that.processInitTagShareMoreFilterData(data.listGmSlsZoneListVO, "zone");
                    that.regionData = that.processInitTagShareMoreFilterData(data.listGmSlsRegionListVO, "region");
                    that.fieldsalesData = that.processInitTagShareMoreFilterData(data.listGmFieldSalesListVO, "fieldsales");
                    that.morefilterZoneid = that.zoneData[0].ID;
                    that.morefilterRegionid = that.regionData[0].ID;
                    that.morefilterFieldsalesid = that.fieldsalesData[0].ID;
                    that.morefilterCompanyid = that.comapanyData[0].ID;
                    that.morefilterDivisionid = that.divisionData[0].ID;
                });
            },
            
            //process the given data to form proper ID and Name
            processInitTagShareMoreFilterData: function (data, key) {
                var strData = JSON.stringify(data);
                var finalData = new Array();
                var processedData = JSON.parse(strData, function (k, v) {
                    if (k === (key + "id"))
                        this.ID = v;
                    else if (k === (key + "nm"))
                        this.Name = v;
                    else
                        return v;
                });

                if (processedData.length == undefined) {
                    finalData.push(processedData);
                    return finalData;
                } else
                    return processedData;
            },
            
            //Function used to update the tag share option code id for the tag to the server.
            updateTagShareOptions: function(tagid,tagshareoptioncodeid){
                if(tagid != "" && tagshareoptioncodeid != "" )
                {
                    this.serverInput = {
                       "token": localStorage.getItem('token'),
                       "tagid": tagid,
                       "tagshareoptionid": tagshareoptioncodeid,
                       "userid": localStorage.getItem("userID")
                   };
                    
                    fnGetWebServerData('invTagShareOption/updateTagShareOption', undefined, this.serverInput, function (data) {
                        if(data != null){
                            console.log(" Return data== "+JSON.stringify(data));
                        }
                    });
                    
                }
                
            },
            
        });
        return InvTagShareOptionView;
    });