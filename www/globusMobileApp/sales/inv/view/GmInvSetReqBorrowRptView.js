/**************************************************************************************************************************
 * File:        GmInvSetReqBorrowRptView.js
 * Description: Set Request Borrow Report View - This is used for sales Rep can request the Sets from other Sales rep
 * Version:     1.0
 * Author1:     Karthik Somanathan
 * Author2:     tmuthusamy & MSelvamani
 ***************************************************************************************************************************/

define([
        'jquery', 'underscore', 'backbone', 'handlebars',
        'global', 'jqueryui', 'dropdownview', 'daoinv', 'searchview', 'invmorefilterview', 'loaderview'
        ],
    function ($, _, Backbone, Handlebars, Global, JqueryUI, DropDownView, DAOInv, SearchView, MoreFilterView, LoaderView) {

        'use strict';
        var arrData = new Array();
        var InvReqBorrowReportView = Backbone.View.extend({

            events: {
                "click #btn-new-inv-more-filter": "openMoreFilter",
                "click #div-inv-more-filter-container-popup-close-icon": "closeMoreFilterPopup",
                "keyup #div-inv-set-report-sysnm-list input": "clearSystem",
                "click #div-inv-report-load-new": "loadSetReport",
		      "click #btn-inv-request-borrow" : "showEmail"
            },

            template: fnGetTemplate(URL_Inv_Template, "GmInvSetReqBorrowRpt"),
            template_borrowmail: fnGetTemplate(URL_Inv_Template, "GmInvSetReqBorrowEmail"),

            initialize: function (options) {
                this.parent = options.parent;
                this.options = options;
                this.system = "";
			    this.setid = "";
                this.render(options);
                this.gmInvSetReqBorrowEmailVO = {};
                this.gmInvSetReqBorrowEmailVO.arrReceiverEmailVO = [];
                this.emailRequestLength = "";
            },

            // Render the initial contents when the View is instantiated
            render: function (options) {
                var that = this;
                this.$el.html(this.template());
                this.loadSalesRepAddressandEmailContent();
                this.loadSystemInfo();
                this.loadSetInfo();
                
                //Get the Email Body from Rule Entry
                fnGetRuleValues('EMAIL_BORROW_BODY', 'REQBRWEMAIL', localStorage.getItem("cmpid"), function (data) {
                     if (data != null) {
                         that.emailbodyVal = data.rulevalue; 
                     }
                });
                
                //Get the no of emails to send from the Request Borrow Email ,we are throwing validation if it exceeds this limit
                fnGetRuleValues('NO_OF_EMAIL_REQ', 'REQBRWEMAIL', localStorage.getItem("cmpid"), function (dt) {
                     if (dt != null) {
                         that.emailRequestLength = dt.rulevalue; 
                     }
                });

                if(!((navigator.onLine) && (!intOfflineMode))) {
                    this.$("#div-inv-report-load-new").hide();
                    this.$("#btn-new-inv-more-filter").hide();
                    this.$("#div-inv-nointernet-connection").show();
                    this.$("#div-inv-nointernet-connection").html(" <div> <i class='fa fa-exclamation-triangle fa-lg'></i>"+lblNm("msg_no_internet_connection")+"</div>"+"Please Connect to WiFi or Cellular Network to use Borrow Report");
                }
                this.loadMoreFilterInitialValues();

                return this;
            },
            
            loadMoreFilterInitialValues: function(){
                var that = this;
                this.input = {
                    "token": localStorage.getItem('token')
                };
                fnGetServerData("salesfilter/morefilter", undefined, this.input, function (data) {
                    //seperates the data
                    that.comapanyData = that.processInitialMoreFilterData(data.listGmCompanyListVO, "company");
                    that.divisionData = that.processInitialMoreFilterData(data.listGmSlsDivisionListVO, "division");
                    that.zoneData = that.processInitialMoreFilterData(data.listGmSlsZoneListVO, "zone");
                    that.regionData = that.processInitialMoreFilterData(data.listGmSlsRegionListVO, "region");
                    that.fieldsalesData = that.processInitialMoreFilterData(data.listGmFieldSalesListVO, "fieldsales");
                    that.morefilterZoneid = that.zoneData[0].ID;
                    that.morefilterRegionid = that.regionData[0].ID;
                    that.morefilterFieldsalesid = that.fieldsalesData[0].ID;
                    that.morefilterCompanyid = that.comapanyData[0].ID;
                    that.morefilterDivisionid = that.divisionData[0].ID;
                });
            },
            
            //process the given data to form proper ID and Name
            processInitialMoreFilterData: function (data, key) {
                var strData = JSON.stringify(data);
                var finalData = new Array();
                var processedData = JSON.parse(strData, function (k, v) {
                    if (k === (key + "id"))
                        this.ID = v;
                    else if (k === (key + "nm"))
                        this.Name = v;
                    else
                        return v;
                });

                if (processedData.length == undefined) {
                    finalData.push(processedData);
                    return finalData;
                } else
                    return processedData;
            },
            
            //Initial Load, get sales rep address
            loadSalesRepAddressandEmailContent: function () {
                var that = this;
                fnGetUserDOID(localStorage.getItem("partyID"), function (dt) {
                    fnGetRepAddressByID(dt.C703_SALES_REP_ID, function (data) {
                        console.log("loadSalesRepAddressandEmailContent="+JSON.stringify(data));
                        if(data != null){
                            _.each(data, function (item) {
                                if (item.PrimaryFL == 'Y') {
                                    fnGetCodeNamebyId(item.StateID, function (state) {
                                        fnGetCodeNamebyId(item.Country, function (country) {
                                            that.gmInvSetReqBorrowEmailVO.requestorrepid = dt.C703_SALES_REP_ID;
                                            that.gmInvSetReqBorrowEmailVO.requestorrepnm = item.Name;
                                            that.requestorrepdefltaddress = (item.Add1 ?  item.Add1 + "," : "") + (item.Add2 ? item.Add2 + "," : "") + (item.City ? item.City + "," : "") +(state.Name ? state.Name + "," : "") + (country.Name ? country.Name + "," : "") + item.Zip;
                                        });    
                                    });
                                }
                            });
                        }
                    });
                });
            },
            
            //load search system info
            loadSystemInfo: function () {
                var that = this;
                var systemdata = [];
                var comboSystem;
                fnGetSystemSearchInfo(function (data) {
                    if (data != null){
                        systemdata = data;
                    }
                    comboSystem = new dhx.Combobox("#div-inv-set-report-sysnm-list", {
                        multiselection: true,
                        virtual: true,
                        data: systemdata,
                        placeholder: "Search System Name...",
                        css:"search_box_req_borrow"
                    });              
                    comboSystem.events.on("change", function (id) {
                        that.system=comboSystem.getValue(true);
                    });
                    
                });
            },
            
            
            //load search set info
            loadSetInfo: function () {
                var that = this;
                var setdata = [];
                var comboSet;
                fnGetInvSetsSearchInfo(function (data) {                    
                    if (data != null){
                        setdata = data;
                    }
                    comboSet = new dhx.Combobox("#div-inv-set-report-setnm-list", {
                        multiselection: true,
                        virtual: true,
                        data: setdata,
                        placeholder: "Search Set Name...",
                        css:"search_box_req_borrow"
                    });               
                    comboSet.events.on("change", function (id) {
                        that.setid=comboSet.getValue(true);
                    });
                    
                });  
            },
            
            //clear system window variable
            clearSystem: function () {
                //window.invSystem = "";
            },
            
            //open more filter view 
            openMoreFilter: function () {
                var that = this;
            $("#invSetReqBorrowList .dhx_header-wrapper").css('z-index','0');
            if(this.moreFilterView)
				this.moreFilterView.close();
			this.moreFilterView = new MoreFilterView({'el' : this.$('#div-inv-more-filter-container-popup-content'), 'parent': this.parent.currentView, input: this.parent.moreFilterInput});
			this.$('#div-inv-more-filter-container-popup').show();	
            },
            
            //close filter view popup
            closeMoreFilterPopup: function () {
                this.$('#div-inv-more-filter-container-popup').hide();
            },
            
            //Load set Report
            loadSetReport: function () {
                var that = this;
                if(this.gmInvSetReqBorrowEmailVO.arrReceiverEmailVO.length > 0){
                    dhtmlx.confirm({
                        title: "Confirm",
                        ok: "Yes",
                        cancel: "No",
                        type: "confirm",
                        text: "Your Previous Set selections will be removed. Are you ok to proceed?",
                        callback: function (result) {
                            if (result == true) {
                                that.gmInvSetReqBorrowEmailVO.arrReceiverEmailVO = [];
                                that.loadSetReportGrid();
                            }
                        }
                    });
                }
                else{
                    that.loadSetReportGrid();
                }
            },
            
            //Load set report grid and its conditions
            loadSetReportGrid: function(){
                var that = this;
                var setidStr = "";
                $("#btn-inv-request-borrow").addClass("hide");
                $("#btn-inv-toggle").removeClass("hide");
                if((that.setid != "" && that.setid != undefined ) || (that.system != "" && that.system != undefined ))
                {
                  if((that.setid != "" && that.setid != undefined)){
                     setidStr = "'"+that.setid.join("','")+"'";
                   }
                   this.serverInput = {
                    "token": localStorage.getItem('token'),
                    "companyid": (this.morefilterCompanyid)?this.morefilterCompanyid:localStorage.getItem("userCompany"),
					"divids": (this.morefilterDivisionid)?this.morefilterDivisionid:localStorage.getItem("userDivision"),
					"zoneids": (this.morefilterZoneid)?this.morefilterZoneid:"",
					"regionids": (this.morefilterRegionid)?this.morefilterRegionid:"",
					"fsids": (this.morefilterFieldsalesid)?this.morefilterFieldsalesid:"",
                    "setid": setidStr,
                    "system": that.system.toString()
                  };
                  console.log(this.serverInput);
                   that.loaderview = new LoaderView({
                        text: "Loading Set Details..."
                   });
                   $("#invSetReqBorrowList .dhx_header-wrapper").css('z-index','0');
                  fnGetWebServerData('InvSetReqBorrow/setReqBorrowDetail', undefined, this.serverInput, function (data) {
                    that.loaderview.close();
                    $("#invSetReqBorrowList .dhx_header-wrapper").css('z-index','11');
                    if(data != null){
                        $(".dhx_grid.dhx_widget").remove();
                        $(".inv-no-data-text").remove();
                        $("#btn-inv-toggle").removeClass("hide");
                        $(".inv-check-borrow-add").removeClass("hide");
                       
                        var grid = new dhx.Grid("invSetReqBorrowList", {
                            columns: [
                                { width: 150, id: "invCheckSetBorrow", header: [{ text: "Request set"},{ text: "<input id =\"checkAllMaster\" class=\"chckMaster\" type=\"checkbox\" style='background: #0288d1 !important;'> " }], type: "boolean",sortable:false},
                                { width: 100, id: "setid", header: [{ text: "Set ID"},{ content: "inputFilter" }]},
                                { width: 250, id: "setnm", header: [{ text: "Set Name"},{ content: "inputFilter" }]},
                                { width: 250, id: "distnm", header: [{ text: "Field Sales Name"},{ content: "inputFilter" }]},
                                { width: 225, id: "emailid", header: [{ text: "Email"},{ content: "inputFilter" }]},
                                { width: 150, id: "repphone", header: [{ text: "Phone#"},{ content: "inputFilter" }]},
                                { width: 150, id: "adnm", header: [{ text: "AD Name"},{ content: "inputFilter" }]},
                                { width: 150, id: "regionnm", header: [{ text: "Region Name"},{ content: "inputFilter" }]},
                                { width: 150, id: "distemailid", header: [{ text: "Dist. Email"},{ content: "inputFilter" }]}
                            ],
                            data : data,
                            adjust: true,  
                            editable:true,
                            height: 400,
                            rowCss: function (row) {
                                if(row.tagshareoptionid === 111140){    //Promote Share
                                        return  "promote_share_row";    // CSS for Green color
                                }
                                if(row.tagshareoptionid === 111141){   //Share
                                        return "share_row";            //CSS for Yellow color
                                };
                                if(row.tagshareoptionid === 111142){   // Do not Share
                                        return "do_not_share_row";     // CSS for Red color
                                };
                            }
                        });
                        
                        grid.hideColumn('distemailid');
                        grid.events.on("cellclick", function (row, column, conf) {
                            if (column.id === "invCheckSetBorrow") {
                                if(row.invCheckSetBorrow == undefined || row.invCheckSetBorrow == false){
                                    if (that.gmInvSetReqBorrowEmailVO.arrReceiverEmailVO.length > 0) {
                                        var emailExist = _.findWhere(that.gmInvSetReqBorrowEmailVO.arrReceiverEmailVO, {
                                            receiveremail: row.emailid
                                        });
                                        that.setRqstBorrowJson(row, emailExist);
                                    } else {
                                        that.setRqstBorrowJson(row);
                                    }
                                }
                                else if(row.invCheckSetBorrow == true){
                                    that.removeRqstBorrowJson(row);
                                }
                            }
                            // console.log( "JSON VALUE=====" + JSON.stringify(that.gmInvSetReqBorrowEmailVO));
                        });
                        
                        $(".chckMaster").change(function () {
                            var chkflval = this.checked;
                            if (grid.data.getLength() < that.emailRequestLength) {
                                if (grid.data.getLength() > 0 && chkflval == true) {
                                    $(".dhx_checkbox__input").prop("checked", chkflval);

                                    grid.data.forEach(function (element, index, array) {
                                        if (that.gmInvSetReqBorrowEmailVO.arrReceiverEmailVO.length > 0) {
                                            var emailExist = _.findWhere(that.gmInvSetReqBorrowEmailVO.arrReceiverEmailVO, {
                                                receiveremail: element.emailid
                                            });
                                            that.setRqstBorrowJson(element, emailExist);
                                        } else {
                                            that.setRqstBorrowJson(element);
                                        }

                                        grid.data.update(element.id, {
                                            invCheckSetBorrow: chkflval
                                        });
                                    });

                                } else if (grid.data.getLength() > 0 && chkflval == false) {
                                    $(".dhx_checkbox__input").prop("checked", chkflval);
                                    grid.data.forEach(function (element, index, array) {
                                        that.removeRqstBorrowJson(element);
                                        grid.data.update(element.id, {
                                            invCheckSetBorrow: chkflval
                                        });
                                    });

                                }
                            } else {
                                dhtmlx.message({
                                    title: "Set Request",
                                    type: "alert",
                                    text: "Can't select more than " + that.emailRequestLength + " emails"
                                });
                            }
                        });
                        
                        if ($("#btn-inv-toggle").hasClass("toggle-button-selected")) {
                            grid.showColumn('adnm');
                            grid.showColumn('regionnm');
                            grid.showColumn('repphone');
                        } else {
                            grid.hideColumn('adnm');
                            grid.hideColumn('regionnm');
                            grid.hideColumn('repphone');
                        }
                        $("#btn-inv-toggle").unbind('click').bind('click', function (e) {
                            if(!$(e.currentTarget).hasClass("toggle-button-selected")){
                                $(this).addClass("toggle-button-selected");
                                grid.showColumn('adnm');
                                grid.showColumn('regionnm');
                                grid.showColumn('repphone');
                            }
                            else{
                                $(this).removeClass("toggle-button-selected");
                                grid.hideColumn('adnm');
                                grid.hideColumn('regionnm');
                                grid.hideColumn('repphone');
                            }
                        });
                    } else {
                        $("#invSetReqBorrowList").html("<span class='inv-no-data-text gmTextCenter gmAlignVertical'>No Data Found</span>");
                    }       
                });
             }else{
                 //fnShowStatus("Please choose any filters to load");
                 dhtmlx.alert({
                        title: "Alert",
                        type: "alert-error",
                        text: "Please choose any filters to load"
                    }); 
             }
            },
            
            //Set Request borrow Report Json
            setRqstBorrowJson: function(row,emailExist){
                var that = this;
                var selectedSetDtl = {};
                selectedSetDtl.setid = row.setid;
                selectedSetDtl.setnm = row.setnm;
                if(emailExist == undefined){
                    if(that.gmInvSetReqBorrowEmailVO.arrReceiverEmailVO.length < that.emailRequestLength ){
                        var selectedSetBorrow = {};
                        selectedSetBorrow.receiveremail = row.emailid;
                        selectedSetBorrow.distemailid = row.distemailid;
                        selectedSetBorrow.emailbody = that.emailbodyVal;
                        selectedSetBorrow.requestorrepdefltaddress = that.requestorrepdefltaddress;
                        selectedSetBorrow.arrSetDtl = [];
                        selectedSetBorrow.arrSetDtl.push(selectedSetDtl);
                        that.gmInvSetReqBorrowEmailVO.arrReceiverEmailVO.push(selectedSetBorrow);
                        //console.log(JSON.stringify(that.gmInvSetReqBorrowEmailVO));
                        $("#btn-inv-request-borrow").removeClass("hide");
                        $("#span-borrow-cnt").html(that.gmInvSetReqBorrowEmailVO.arrReceiverEmailVO.length);
                    }else{           
                       dhtmlx.message({
                            title: "Set Request",
                            type: "alert",
                            text: "Can't send more than "+that.emailRequestLength+" emails"
                        });
                    }
                }
                else{
                    _.each(that.gmInvSetReqBorrowEmailVO.arrReceiverEmailVO,function(item){
                        if(item.receiveremail == row.emailid){
                            var setExist = _.findWhere(item.arrSetDtl,{setid: row.setid});
                            if(setExist == undefined){
                                item.arrSetDtl.push(selectedSetDtl);
//                                console.log(JSON.stringify(that.gmInvSetReqBorrowEmailVO));
                            }
                        }
                    });
                }
            },
            
            //Remove Request borrow Report Json
            removeRqstBorrowJson: function(row){
                var that = this;
                _.each(that.gmInvSetReqBorrowEmailVO.arrReceiverEmailVO, function (dt) {
                    if (dt.receiveremail == row.emailid) {
                        dt.arrSetDtl = _.reject(dt.arrSetDtl, function (dl) {
                            return dl.setid == row.setid;
                        });

                        if (dt.arrSetDtl.length == 0) {
                            that.gmInvSetReqBorrowEmailVO.arrReceiverEmailVO = _.reject(that.gmInvSetReqBorrowEmailVO.arrReceiverEmailVO, function (ds) {
                                return ds.receiveremail == row.emailid;
                            });
                             $("#span-borrow-cnt").html(that.gmInvSetReqBorrowEmailVO.arrReceiverEmailVO.length);
                        }
                    }
                });
                if (that.gmInvSetReqBorrowEmailVO.arrReceiverEmailVO.length == 0) {
                    $("#btn-inv-request-borrow").addClass("hide");
                    $(".chckMaster").prop("checked",false);
                }
//                console.log(JSON.stringify(that.gmInvSetReqBorrowEmailVO));
            },
            
            //get More fiter inputs 
            moreFilterModified: function () {
                var savedZone = (this.morefilterZoneid) ? this.morefilterZoneid : "",
                    savedRegion = (this.morefilterRegionid) ? this.morefilterRegionid : "",
                    savedFieldSales = (this.morefilterFieldsalesid) ? this.morefilterFieldsalesid : "";
                var savedCompany = (this.morefilterCompanyid) ? this.morefilterCompanyid : "",
                    savedDivision = (this.morefilterDivisionid) ? this.morefilterDivisionid : "";
                var userCompany = localStorage.getItem("userCompany"),
                    userDivision = localStorage.getItem("userDivision");

                if ((savedZone != "") || (savedRegion != "") || (savedFieldSales != "")) {
                    this.$("#btn-new-inv-more-filter").addClass("bg-red");

                } else if ((savedCompany != "") || (savedDivision != "")) {
                    if ((savedCompany != userCompany) || (savedDivision != userDivision)) {
                        this.$("#btn-new-inv-more-filter").addClass("bg-red");
                    } else
                        this.$("#btn-new-inv-more-filter").removeClass("bg-red");
                } else
                    this.$("#btn-new-inv-more-filter").removeClass("bg-red");

                this.unbind();
                //this.render(this.options);
            },

            // function used to show borrow email popup
            showEmail: function () {
                var that = this;
                var maxEmailBodylength = 500;
                var maxEMailShipAddrlength = 200;
                
                //This function will open the Borrow Set Request popup screen
                that.showPopupReqBorrowmail();
                console.log("showEmail gmInvSetReqBorrowEmailVO ==" + JSON.stringify(that.gmInvSetReqBorrowEmailVO));
                
                //Remder the Request to Borrow details on Popup
                $("#div-crm-overlay-content-container2 .modal-body").append(that.template_borrowmail(that.gmInvSetReqBorrowEmailVO));
                $("#div-footer").addClass("hide");

                 // This will update the remaining characters in text area for Notes text box
                $('.rqBorrowEmailMsg').keyup(function (event) {
                        var text = event.currentTarget.value;
                        if (text.length >= maxEmailBodylength) {
                            event.currentTarget.value = text.substring(0, maxEmailBodylength);
                            text = event.currentTarget.value;
                        }
			            //$("#spn-borrow-notes-limit").text(maxEmailBodylength - text.length);
                        $(event.currentTarget).parent().find('#spn-borrow-notes-limit').text(maxEmailBodylength - text.length);
                });
                
                 // This will update the current EMAIL Json email body (notes)
                $('.rqBorrowEmailMsg').focusout(function (event) {
                        that.updateReceiverJSON(event);
                });
                
                  // This will update the remaining characters in text area for shipping text box
                $('.rqBorrowEmailShipAddr').keyup(function (event) {
                        var text = event.currentTarget.value;
                        if (text.length >= maxEMailShipAddrlength) {
                            event.currentTarget.value = text.substring(0, maxEMailShipAddrlength);
                            text = event.currentTarget.value;
                        }
			            //$("#spn-borrow-shipadr-limit").text(maxEMailShipAddrlength - text.length);
                        $(event.currentTarget).parent().find('#spn-borrow-shipadr-limit').text(maxEMailShipAddrlength - text.length);
                });
                
                 // This will update the current EMAIL Json ship address
                $('.rqBorrowEmailShipAddr').focusout(function (event) {
                        that.updateReceiverJSON(event);
                });
                
                // NOTES Copy icon will update the entire Json email body
                 $("#div-crm-overlay-content-container2").find('#notesId1').unbind('click').bind('click', function (event) {
                    that.updateApplyNotes(event);
                 });
                
                // Ship Address Copy icon will update the entire Json ship address
                $("#div-crm-overlay-content-container2").find('#notesId2').unbind('click').bind('click', function (event) {
                    that.updateApplyShiptoAddresss(event);
                 });
                
                 // This function will call webservice and send Email
                 $("#div-crm-overlay-content-container2").find('.send-borrow-email').unbind('click').bind('click', function () {
                    that.sendRequestToBorrowEmail();
                });
            },

            //This function will open the Borrow Set Request popup screen
            showPopupReqBorrowmail: function () {
                var that = this;
                $('.div-overlay').show();
                $("#div-crm-overlay-content-container2").addClass("borrowmailShowPop");
                $('.borrowmailShowPop').find('.modal-body').empty();
                $('.borrowmailShowPop').find('.modal-title').empty();
                $(".borrowmailShowPop .modal-title").append('<div><div class="req-borrow-header">Request To Borrow Email</div><div class="send-borrow-email"><span class="borrow-email-send-icon">Send <i class="fa fa-paper-plane fa-lg" aria-hidden="true"></span></div></div>');
                $("#div-crm-overlay-content-container2").removeClass("hide");
                $('#div-crm-overlay-content-container2 button[data-dismiss="modal"]').each(function () {
                    $(this).unbind('click').bind('click', function () {
                        $('.div-overlay').hide();
                        $('#div-crm-overlay-content-container2').addClass('hide');
                        $("#div-crm-overlay-content-container2").removeClass("borrowmailShowPop");
                        $('.hideDefaultBtn').addClass('hide');
                        $("#invSetReqBorrowList .dhx_header-wrapper").css('z-index','11');
                    });
                });
                $("#invSetReqBorrowList .dhx_header-wrapper").css('z-index','0');
            },
            
            //This function will call webservice and send Email
            sendRequestToBorrowEmail: function () {
                var that = this;
                var count = 0;
                $('#ship-to-validation-msg').hide();
                console.log("that.gmInvSetReqBorrowEmailVO ==" + JSON.stringify(that.gmInvSetReqBorrowEmailVO));
                _.each(that.gmInvSetReqBorrowEmailVO.arrReceiverEmailVO, function (data) {
                    if (data.requestorrepdefltaddress == "" || data.emailbody == "") {
                        count = ++count;
                    }
                });
                if (count == 0) {
                   dhtmlx.confirm({
                        title: "Confirm - Send EMail",
                        ok: "Yes",
                        cancel: "No",
                        type: "confirm",
                        text: "Are you sure you want to proceed?",
                        callback: function (result) {
                            if (result == true) {                   
                                fnGetWebServerData('InvSetReqBorrow/sendRequestToBorrowEmail', undefined, that.gmInvSetReqBorrowEmailVO, function (data, model, response, errorReport) {
                                    console.log("data==" + JSON.stringify(data));
                                    if ( (data.strReturnMessage == null) || (data.strReturnMessage == "") )  {
                                        dhtmlx.message({
                                            title: "Success",
                                            type: "alert",
                                            text: "EMail(s) sent successfully..."
                                        });

                                    } else {
                                        dhtmlx.alert({
                                            title: "Alert",
                                            type: "alert-error",
                                            text: "EMail(s) not delivered ,Please try again or contact system administrator.Error:"+ data.strReturnMessage+""
                                        }); 
                                    }
                                    $('.div-overlay').hide();
                                }, true);
                            }
                        }
                    });
                } else {
                     dhtmlx.alert({
                        title: "Alert",
                        type: "alert-error",
                        text: "Please enter Email Notes/ShipTo Address to proceed"
                    }); 
                }
            },

             // This will update the current EMAIL Json email body (notes) & Ship Address
            updateReceiverJSON: function (e) {
                var that = this;
                var parent = $(e.currentTarget).parent();
                var email = $(e.currentTarget).attr("data-email");
                var emailbody = $(parent).parent().find("#notes-div").find('textarea').val();
                var shiptoaddr = $(parent).parent().find("#shiptoaddr-div").find('textarea').val();
                _.each(that.gmInvSetReqBorrowEmailVO.arrReceiverEmailVO, function (data) {
                    if (data.receiveremail == email) {
                        data.emailbody = emailbody;
                        data.requestorrepdefltaddress = shiptoaddr;
                    }
                });
                console.log('Updated value --' + JSON.stringify(that.gmInvSetReqBorrowEmailVO.arrReceiverEmailVO));
            },

            // NOTES- Copy icon will update the entire Json email body
            updateApplyNotes: function (e) {
                var that = this;
                if(that.gmInvSetReqBorrowEmailVO.arrReceiverEmailVO.length > 0){
                    dhtmlx.confirm({
                        title: "Confirm - Apply Notes",
                        ok: "Yes",
                        cancel: "No",
                        type: "confirm",
                        text: "This will replace all the below notes. Are you ok to proceed?",
                        callback: function (result) {
                            if (result == true) {
                                var parent = $(e.currentTarget).parent();
                                var emailbody = $(parent).parent().find("#notes-div").find('textarea').val();
                                $('.rqBorrowEmailMsg').val(emailbody);
                                _.each(that.gmInvSetReqBorrowEmailVO.arrReceiverEmailVO, function (data) {
                                    data.emailbody = emailbody;
                                });
                                console.log("Apply Notes-->" + JSON.stringify(that.gmInvSetReqBorrowEmailVO.arrReceiverEmailVO));
                            }
                        }
                    });
                }
            },
            
             // Ship Address - Copy icon will update the entire Json ship address
            updateApplyShiptoAddresss: function (e) {
                var that = this;
                if(that.gmInvSetReqBorrowEmailVO.arrReceiverEmailVO.length > 0){
                    dhtmlx.confirm({
                        title: "Confirm - Apply ShipTo Address",
                        ok: "Yes",
                        cancel: "No",
                        type: "confirm",
                        text: "This will replace all the below ShipTo Address. Are you ok to proceed?",
                        callback: function (result) {
                            if (result == true) {
                                var parent = $(e.currentTarget).parent();
                                var shiptoaddr = $(parent).parent().find("#shiptoaddr-div").find('textarea').val();
                                $('.rqBorrowEmailShipAddr').val(shiptoaddr);
                                _.each(that.gmInvSetReqBorrowEmailVO.arrReceiverEmailVO, function (data) {
                                    data.requestorrepdefltaddress = shiptoaddr;
                                });

                                console.log("Apply ShiptoAdd-->" + JSON.stringify(that.gmInvSetReqBorrowEmailVO.arrReceiverEmailVO));
                            }
                        }
                    });
                }
            },

        });

        return InvReqBorrowReportView;
    });