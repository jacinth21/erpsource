/**********************************************************************************
 * File:        GmInvSummaryview.js
 * Description: 
 * Version:     1.0
 * Author:     	asivaraj
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification','datasync','saleitemmodel', 'saleitemcollection', 'saleitemlistview','itemview', 'invshipaddressview','dropdownview'
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification,DataSync, SaleItem, SaleItems, SaleItemListView,ItemView, InvShipAddressView,DropDownView) {

	'use strict';
	var InvSummaryView = Backbone.View.extend({

		/* Load the templates */ 
		template: fnGetTemplate(URL_Inv_Template, "GmInvSummary"),
		template_setdetails: fnGetTemplate(URL_Inv_Template, "GmInvSummaryDetails"),
		
		initialize: function(options) {
			this.parent = options.parent;
			
			this.requesttype = options.requesttype;
			this.render();	
		},

		// Render the initial contents when the View is instantiated
		render: function () {
			
//			showMsgView("011");
			
			this.$el.html(this.template(this.parent.JSONSets));
			$("#btn-inv-delete").prop("disabled",false);
			$("#div-inv-view-summary-set-list").append(this.template_setdetails(this.parent.JSONSets));
			if(this.requesttype != "1006505"){
				$("#div-inv-view-summary-account-id").hide();
				$("#div-inv-view-summary-info-account-name").hide();
				$(".div-inv-view-summary-label-header").html("Required Date : ");
				$("#div-inv-view-summary-info-assocrep").hide();
			}
			if(this.requesttype == "1006507" || this.requesttype == "107286"){ // Account Item Consignment
				$("#div-inv-view-summary-set-id-header").html("Parts")
			}
			this.$("#div-inv-view-summary-acid").html($("#div-inv-surgery-info-Account-list .div-dropdown-main .div-dropdown-text").text());
			this.$("#div-inv-view-summary-acname").html($("#div-inv-surgery-info-SalesRep-list").text());
			this.$("#div-inv-view-summary-sname").html($("#label-inv-surgery-info-surgeon-name").val());
			this.$("#div-inv-view-summary-sdate").html($("#label-inv-surgery-info-surgery-date").text());
                this.$("#div-inv-view-summary-assocrep").html($("#div-inv-surgery-assocsalesinfoname .div-dropdown-main .div-dropdown-text").text());
            if ($(window).width() < 480) {
                $('#div-inv-view-summary-set-header').addClass('hide');
                $('.inv-summary-set').removeClass('hide');
                $('.inv-summary-qty').removeClass('hide');
                if(this.requesttype == "1006507" || this.requesttype == "107286"){ // Account Item Consignment
				$(".inv-summary-set").html(lblNm('parts'));
			}
                else{
                  $(".inv-summary-set").html(lblNm('set'));  
                }
                
            }
            
			
			return this;
		},

		close: function() {
			this.unbind();
			this.undelegateEvents();
		}
	});	

	return InvSummaryView;
});