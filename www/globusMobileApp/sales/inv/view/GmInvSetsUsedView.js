/**********************************************************************************
 * File:        GmInvSetsUsedView.js
 * Description: 
 * Version:     1.0
 * Author:     	tsekaran
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification','searchview','saleitemmodel', 'saleitemcollection', 'saleitemlistview'
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification, SearchView, SaleItem, SaleItems, SaleItemListView ) {
        	
	'use strict';
	
	var InvSetsUsedView = Backbone.View.extend({

		events : {
			// "click .div-inv-sets-used-shipdt-label label, .div-inv-sets-used-shipdt-label i" : "showDatepicker",
			"click .clear-row-pu": "clearRowSets",
			"change .txt-plannedship-date" : "getChangedShipDate",
			"click .div-inv-keypad-values" : "showNumberKeypad",
			"click .li-inv-parts-icon" : "showPartDetails",
			"click  #div-inv-setpart-details-close-icon" : "closePartDetails",
			"click .plandshipdte-checkbox" : "applyAllPlannedShipDate",
            "click .qty-plus-pu" : "incQty",
            "click .qty-minus-pu" : "decQty",
		},

		/* Load the templates */ 
		template: fnGetTemplate(URL_Inv_Template, "GmInvSetsUsed"),
		template_setdetails: fnGetTemplate(URL_Inv_Template, "GmInvSetsUsedDetails"),
		template_setpartdetails: fnGetTemplate(URL_Inv_Template, "GmInvSetPartDetails"),
		template_setpartdetailslist: fnGetTemplate(URL_Inv_Template, "GmInvSetPartDetailsList"),
		template_setdetailsMobile: fnGetTemplate(URL_Inv_Template, "GmInvSetsUsedDetailsMobile"),
		
		initialize: function(options) {
			this.parent = options.parent;
			this.requesttype = options.requesttype;
			this.render(options);
		},

		// Render the initial contents when the Catalog View is instantiated
		render: function (options) {
//			showMsgView("011");
			var that = this;
			this.$el.html(this.template());
			if(this.requesttype == "1006505"){
				var renderOptions = {
							"el":this.$('#div-inv-sets-used-txtkeyword'),
							"ID" : "txt-keyword",
							"Name" : "txt-search-name",
							"Class" : "txt-keyword-initial",
							"minChar" : "3",
							"autoSearch": true,
							"placeholder": lblNm("msg_character_search"),
							"fnName": fnGetIMLoanerSets,
							"usage": 2,
							"template" : 'GmSearchResult',
							"template_URL" : URL_Common_Template,
							"callback": function(sList) {that.getSetsFromSearchView(sList);}
							};
						} else{
			var renderOptions = {
							"el":this.$('#div-inv-sets-used-txtkeyword'),
							"ID" : "txt-keyword",
							"Name" : "txt-search-name",
							"Class" : "txt-keyword-initial",
							"minChar" : "3",
							"autoSearch": true,
							"placeholder": lblNm("msg_character_search"),
							"fnName": fnGetIMSets,
							"usage": 2,
							"template" : 'GmSearchResult',
							"template_URL" : URL_Common_Template,
							"callback": function(sList) {that.getSetsFromSearchView(sList);}
							};
						}
			
			var searchview = new SearchView(renderOptions);
			this.displaySets(this.parent.SetsJSONData);
			this.comnMobFunc();
            
            /*if the Sets are selected from the CREO Set Ordering Guide , then load the data in the Inventory Management ->Product Loaner screen .
            Get the set id to and create the row element for the Set ID and Set Name and Pass it to the getSetsFromSearchView function*/
        if(this.requesttype == "1006505"){   //Product Loaner 
            var creoData = "";
			if (parseNull(localStorage.getItem("creoData")) != "" ){
                creoData = $.parseJSON(localStorage.getItem("creoData"));
                window.localStorage.setItem("oldcreoData",parseNull(localStorage.getItem("creoData")));            
                for(var i=0;i<creoData.length;i++){
                     var setnm =creoData[i].setid;
                     var setid=setnm.split(' ');
                     
                    var rowEl = $("<a id=" + setid[0] + " class=item-searchlist name=" + setnm + "><div class='div-data hide'></div> <i class='fa fa-check'></i></a>");
                     var CurrentRow = {
                        "ID": setid[0],
                        "Name": setnm,
                        "Element": rowEl
                        };
                      that.getSetsFromSearchView(CurrentRow);  
                    }
            }
            window.localStorage.setItem("creoData","");
            }

		return this;
		},

		//Fired when part is selected from the search
		getSetsFromSearchView: function(sList) {
			var that=this;
			var arrSelectedData = new Array();
			if(sList.length==undefined)
				arrSelectedData.push(sList);
			else
				arrSelectedData = sList;
            
			$("#div-inv-sets-list").show();
			$("#div-inv-sets-used").find("#div-select-all").addClass("hide");
			$(".ul-inv-sets-list-header").show();
			$(".div-inv-sets-list-footer").show();
			$("#div-itemlist").html("<li>"+lblNm("search_again")+"!</li>");
			$("#txt-keyword").val('');
			$("#div-common-search-x").addClass('hide');
			this.loadSets(0,arrSelectedData);
		},
		
		//loads the selected parts in the list.
		//Called Recursively - to fetch the Price for each Part
		loadSets: function(iValue, data) {
			var that = this;
			if(iValue<data.length) {
				data[iValue].qty = 1;
                if ($(window).width() < 480) {
                    this.$("#div-inv-sets-list").append(this.template_setdetailsMobile(data[iValue]));
                   
                } else {
                    this.$("#div-inv-sets-list").append(this.template_setdetails(data[iValue]));
                }
				
				this.loadSets(iValue+1,data);
			}else{
				this.updateJSON();
			}
            
             
			this.comnMobFunc();
            
		},
        
        comnMobFunc: function(){
            this.$("#div-inv-sets-list").find(".invDetaillMbl").each(function(){
                if($(this).next().is('ul')){
                    $(this).addClass("ul-show-inv-list");
                    $(this).children(".togglebx").children("img").removeClass("onActive");
                }
                else{
                    $(this).removeClass("ul-show-inv-list");
                    $(this).children(".togglebx").children("img").addClass("onActive");
                }
                    
            });
            
            $(".togglebx").unbind('click').bind('click', function () {
                 $(this).parent(".invDetaillMbl").toggleClass('ul-show-inv-list');
                 $(this).children("img").toggleClass("onActive");
             });

             $(".invDetaillMbl").on('swipeleft', function (e, data) {
                 $(this).find(".delete-pu-span").addClass("onShowEl");
             });


             $(".invDetaillMbl").on('tap', function (e, data) {
                 $(this).find(".delete-pu-span").removeClass("onShowEl");
             });
        },
        
        incQty: function(e){
            var that = this;
            var count = parseInt($(e.currentTarget).siblings("#div-qty-box").text());
            var countEl = $(e.currentTarget).siblings("#div-qty-box");
                count++;
                countEl.text(count);
             $(e.currentTarget).parent().parent().find(".inv-qty").text(count);
                if(that.parent.partsChangeMode)
				   $(target).closest('.ul-sets-used-list').addClass('part-updated');
                that.updateJSON();
        },
        
        decQty: function(e){
            var that = this;
            var count = parseInt($(e.currentTarget).siblings("#div-qty-box").text());
            var countEl = $(e.currentTarget).siblings("#div-qty-box");
             if (count > 1) {
                count--;
                countEl.text(count);
                $(e.currentTarget).parent().parent().find(".inv-qty").text(count);
                if(that.parent.partsChangeMode)
				   $(target).closest('.ul-sets-used-list').addClass('part-updated');
                that.updateJSON();
             }  
        },

		//Used to display Part information if loaded from the Booked DO's (Draft)
		displaySets: function(data) {
			
			var that =this;
			$("#div-inv-sets-list").show();
			$("#div-inv-sets-used").find("#div-select-all").addClass("hide");
			$(".ul-inv-sets-list-header").show();
			
             if ($(window).width() < 480) {
                 for (var i = 0; i < data.length; i++) {
                     this.$("#div-inv-sets-list").append(this.template_setdetailsMobile(data[i]));
                 }
             } else{
                 for(var i=0;i<data.length;i++) {
                     this.$("#div-inv-sets-list").append(this.template_setdetails(data[i]));
			     }
             }
		},

		clearRowSets: function(event){
			
			if(!this.parent.partsChangeMode) {
				
				$(event.currentTarget).parent().parent().remove();
				 this.updateJSON();
				
			}
			else {
				
				var index = $(event.currentTarget).parent().parent("ul").index();
				var indexValue = $(event.currentTarget).parent().parent().attr('indexValue');
				var dupJSONData = $.parseJSON(JSON.stringify(this.parent.JSONData.arrGmCaseSetInfoVO));

				for(var i=0;i<=dupJSONData.length;i++) {
					if(this.parent.JSONData.arrGmCaseSetInfoVO[i]!=undefined && this.parent.JSONData.arrGmCaseSetInfoVO[i].indexValue==indexValue) {
						this.parent.JSONData.arrGmCaseSetInfoVO.splice(i,1);
						i--;
					}						
				}
					
				this.parent.JSONData.gmTxnShipInfoVO = _.reject(this.parent.JSONData.gmTxnShipInfoVO, function(arr){ console.log( parseInt(arr.indexValue) == parseInt(indexValue));
					
				return parseInt(arr.indexValue) == parseInt(indexValue); });
				this.parent.JSONSets = _.reject(this.parent.JSONSets, function(arr){ return parseInt(arr.indexValue) == parseInt(indexValue); });
				this.parent.SetsJSONData = _.reject(this.parent.SetsJSONData, function(arr){ return parseInt(arr.indexValue) == parseInt(indexValue); });
				$(event.currentTarget).parent().parent().remove();				
			}
		},

		
		updateJSON :function(e){
			if(!this.parent.partsChangeMode)
				{
				this.parent.JSONSets =[];
			    this.parent.SetsJSONData =[];
				}
			var MainJSONData = new Array();

			if(this.parent.partsChangeMode) 
				MainJSONData = $.parseJSON(JSON.stringify(this.parent.JSONData.arrGmCaseSetInfoVO));
	
			
			var that = this;
			var id,name,qty,date,reqdt,systype;
			date = $("#label-inv-surgery-info-plandship-date").text();
			reqdt = $("#label-inv-surgery-info-surgery-date").text();
			
			this.parent.JSONData.type = this.requesttype;
			if(this.requesttype == 1006505) 
				systype = "103642"; //Inv Mgmt Prod Loaner Request
			if(this.requesttype == 1006506)
				systype = "103643"; // Inv Mgmt set Request
			

			this.$("#div-inv-sets-list").find(".ul-sets-used-list").each(function(index) {
				id = $(this).attr('id');
				name = $(this).find(".li-inv-setnm-used-list").text();
				qty = $(this).find(".div-inv-keypad-values").text().trim();
			
				if(!that.parent.partsChangeMode) { 
					that.parent.SetsJSONData.push({"ID" :id, "Name" : name, "qty" : qty,"planshipdt" : date, "indexValue" : index.toString()});
					if(qty > 1)
						{
						for(var i =0 ; i<qty ;i++){
							that.parent.JSONSets.push({"ID" :id, "Name" : name, "qty" : 1,"planshipdt" : date, "indexValue" : index.toString()});
							MainJSONData.push({"casetid" : "","cainfoid"  : "", "setid" : id, "systp" : systype, "qty" : 1, "reqdt" : reqdt, "planshipdt" : date,"indexValue" : index.toString()});
							that.parent.JSONData.arrGmCaseSetInfoVO = MainJSONData;
						}					
					}else{
							that.parent.JSONSets.push({"ID" :id, "Name" : name, "qty" : qty,"planshipdt" : date, "indexValue" : index.toString()});
							MainJSONData.push({"casetid" : "","cainfoid"  : "", "setid" : id, "systp" : systype, "qty" : qty, "reqdt" : reqdt, "planshipdt" : date,"indexValue" : index.toString()});
							that.parent.JSONData.arrGmCaseSetInfoVO = MainJSONData;
					}
					$(this).attr('indexValue', index);
				}else {
					
					 
							if($(this).hasClass('part-updated')) {
								var indexValue = $(this).attr('indexValue');				
								that.parent.SetsJSONData[index].ID = id;
								that.parent.SetsJSONData[index].Name = name;
								that.parent.SetsJSONData[index].qty = qty;
								that.parent.SetsJSONData[index].planshipdt = date;
								that.parent.SetsJSONData[index].indexValue = indexValue;
															
								var dupJSONSets = $.parseJSON(JSON.stringify(that.parent.JSONSets));
								var dupQty = _.countBy(dupJSONSets, function(num) {
								  return num.indexValue.toString();
								});

								for(var i=0;i<dupJSONSets.length;i++) {
									
									if(parseInt(dupJSONSets[i].indexValue) == parseInt(indexValue)) {
										
										var set_qty = that.parent.JSONSets[i].qty;
										that.parent.JSONSets[i].ID = id;
										that.parent.JSONSets[i].Name = name;
										
										var set_qty = dupQty[indexValue];

										
										if(set_qty<qty) {
											var diff = qty - set_qty;

											for(var d=0; d<diff; d++) {
												that.parent.JSONSets.push({"ID" :id, "Name" : name, "qty" : 1,"planshipdt" : date, "indexValue" : indexValue});
												MainJSONData.push({"casetid" : "","cainfoid"  : "", "setid" : id, "systp" : systype, "qty" : 1, "reqdt" : reqdt, "planshipdt" : date,"indexValue" : indexValue});
																						
											}
										}
										else if(qty<set_qty) {
											var diff = set_qty - qty ;

											for(var m=0;m<MainJSONData.length;m++) {
												if(MainJSONData[m].indexValue==dupJSONSets[i].indexValue) {
													var dupMainJSONDataQty = _.countBy(MainJSONData, function(num) {
														  return num.indexValue.toString();
														});
													dupMainJSONDataQty = dupMainJSONDataQty[MainJSONData[m].indexValue];
													if(dupMainJSONDataQty>qty) {
														MainJSONData.splice(m,1);
														m--;
													}								
												}
											}
											for(var m=0;m<that.parent.JSONSets.length;m++) {
												if(that.parent.JSONSets[m].indexValue==dupJSONSets[i].indexValue) {
													var dupJSONSetsQty = _.countBy(that.parent.JSONSets, function(num) {
														  return num.indexValue.toString();
														});
													dupJSONSetsQty = dupJSONSetsQty[that.parent.JSONSets[m].indexValue];
													if(dupJSONSetsQty>qty) {
														that.parent.JSONSets.splice(m,1);
														m--;
													}												
												}
											}
											
										}
										break;
									}

								}
								
							}else if(($(this).attr('indexValue')==undefined) || ($(this).attr('indexValue')=="") ) {
								var newIndex;
								
								if(that.parent.JSONSets.length>0)
									newIndex = parseInt(_.max(_.pluck(that.parent.JSONSets,'indexValue'), function(val){ return parseInt(val); })) + 1;
								else
									newIndex = 0;
								if(_.findWhere(that.parent.SetsJSONData,{"ID" :id, "Name" : name, "qty" : qty,"planshipdt" : date, "indexValue" : newIndex})==undefined) 
									that.parent.SetsJSONData.push({"ID" :id, "Name" : name, "qty" : qty,"planshipdt" : date, "indexValue" : newIndex});
								
								if(qty > 1)
									{
									for(var i =0 ; i<qty ;i++){
										that.parent.JSONSets.push({"ID" :id, "Name" : name, "qty" : 1, "planshipdt" : date, "indexValue" : newIndex.toString()});
										MainJSONData.push({"casetid" : "","cainfoid"  : "", "setid" : id, "systp" : systype, "qty" : 1, "reqdt" : reqdt, "planshipdt" : date,"indexValue" : index.toString()});
										that.parent.JSONData.arrGmCaseSetInfoVO.push({ "casetid" : "","cainfoid"  : "", "setid" : id, "systp" : systype, "qty" : 1, "reqdt" : reqdt, "planshipdt" : date, "indexValue" : newIndex.toString() });

									}
								}else{
									that.parent.JSONSets.push({"ID" :id, "Name" : name, "qty" : qty, "planshipdt" : date, "indexValue" : newIndex.toString()});
									MainJSONData.push({"casetid" : "","cainfoid"  : "", "setid" : id, "systp" : systype, "qty" : 1, "reqdt" : reqdt, "planshipdt" : date,"indexValue" : index.toString()});
									that.parent.JSONData.arrGmCaseSetInfoVO.push({ "casetid" : "","cainfoid"  : "", "setid" : id, "systp" : systype, "qty" : 1, "reqdt" : reqdt, "planshipdt" : date, "indexValue" : newIndex.toString() });

								}
								
								$(this).attr('indexValue', newIndex);
							}
						
					}

				
			});
			that.parent.JSONData.arrGmCaseSetInfoVO = MainJSONData;
			this.$('.part-updated').removeClass('part-updated');
		},


		closePartDetails : function(){
			this.$("#div-inv-setpart-details").hide();
			
		},

		showPartDetails : function(event){
          
			var setid = $(event.currentTarget).parent().attr("id");
			this.$("#div-inv-setpart-details").show();			
			this.$("#div-inv-setpart-details").html(this.template_setpartdetails());
			var that = this;
			
			fnGetPartsForSet("",setid,function(data){
			if(data.length >0){
				var items = new SaleItems(data);				
				that.showItemListView(that.$("#div-inv-setpart-details-part-list"), items, "GmInvSetPartDetailsList", URL_Inv_Template);
			}else{
				$("#div-inv-setpart-details-part-content").html(" <div id='div-parts-not-available'>"+lblNm("msg_no_parts")+"</div>");
			}	
				
				
			});
		
		},

		showItemListView: function(controlID, collection, itemtemplate, template_URL) {
			this.listView = new SaleItemListView({ 
				el: controlID, 
				collection: collection, 
				itemtemplate: itemtemplate,
				template_URL: template_URL,
				columnheader: 0
			});
			return this.listView;
		},

		//Displays the Number keypad 
		showNumberKeypad:function(event){
			var that = this;
			this.parent.showNumberKeypad(event, function(val, target) {
				if(event.currentTarget.id == "div-qty-box"){
					if(val == "0" || val == "00" || val == "000"){
					}else{
						if(that.parent.partsChangeMode)
							$(target).closest('.ul-sets-used-list').addClass('part-updated');
						$(target).text(val);
                        $(target).parent().parent().find(".inv-qty").text(val);
					}
				}else {
					if($("#div-qty-entry-text").text() != "")
					$(event.currentTarget).attr("value",val);
					if(that.parent.partsChangeMode)
						$(target).closest('.ul-sets-used-list').addClass('part-updated');
                    $(target).parent().parent().find(".inv-qty").text(val);
				}
			that.updateJSON();
			});
		},

		close: function() {
			this.unbind();
			this.undelegateEvents();
		}
		
	});	

	return InvSetsUsedView;
});