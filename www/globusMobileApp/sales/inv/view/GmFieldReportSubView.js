/**********************************************************************************
 * File:        GmFieldReportSubView.js
 * Description: 
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification','datasync','saleitemmodel', 'saleitemcollection', 
        'saleitemlistview','itemview', 'dropdownview','daoinv','searchview'
        ], 
	function ($, _, Backbone, Handlebars, Global, Device, Notification, DataSync, SaleItem, SaleItems, SaleItemListView, ItemView, DropDownView, DAOInv, SearchView) {

	'use strict';
	
	var FieldReportSubView = Backbone.View.extend({
		
		events : {
			"click .div-inv-sub-report-settype-label" : "toggleContent",
			"click #li-inv-sub-report-showall i" : "showAiData",
			"click #div-inv-sub-report-load" : "loadData",
			"click .td-index-0 span" : "loadDrillDown",
			"click #ul-inv-sub-report-input i" : "checkBox"
			
		},

		template: fnGetTemplate(URL_Inv_Template, "GmInvFieldSubReport"),

		template_list: fnGetTemplate(URL_Inv_Template, "GmInvFieldReportList"),
		
		tplSpinner: fnGetTemplate(URL_Common_Template, "GmProgressSpinner"),
		
		initialize: function(options) {

			this.parent = options.parent;
			this.triggeredEvent = options.triggeredEvent;
			this.serverInput = $.parseJSON(JSON.stringify(this.parent.currServerInput));
			
			this.skipCalculations = this.parent.skipCalculations;	
			this.skipDivide = this.parent.skipDivide;
			this.roundOffValues = this.parent.roundOffValues;
			this.breadCrumb = $.parseJSON(JSON.stringify(this.parent.breadCrumb));
			this.showPercent = false, this.showArrow = false; this.hideValue = false;
			
			this.monthIndex = ["jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec"]
			
			this.render();
		},

		// Render the initial contents when the View is instantiated
		render: function () {
			var that = this;
//			showMsgView("011");
			console.log(this.$el.get(0));
			this.$el.html(this.template());
			this.loadInputOptions();
			this.loadSystemInfo();
			return this;
		},
		
		loadSystemInfo :function(){
			var that = this;
			var renderOptions = {
					"el":this.$('#div-inv-sub-report-sysnm-list'),
					"ID" : "inv-sub-report-system",
            		"Name" : "txt-search-name",
            		"Class" : "txt-keyword-account",
            		"minChar" : "3",
            		"autoSearch": true,
            		"placeholder": lblNm("msg_character_search"),
            		"fnName": fnGetSystemInfo,
            		"usage": 1,
            		"template" : 'GmSearchResult',
					"template_URL" : URL_Common_Template,
					"callback": function(rowValue) {that.getSystemFromSearchView(rowValue);}
            };
			var searchview = new SearchView(renderOptions);
			
		},
		
		getSystemFromSearchView: function(rowValue){
			$("#inv-sub-report-system").val(rowValue.Name);
			this.serverInput.setnum = rowValue.ID;
			this.currServerInput.setnum = rowValue.ID;
			this.arrServerInput.push($.parseJSON(JSON.stringify(this.currServerInput)));
		},

		loadInputOptions: function () {
			var that = this;
			var elem = this.triggeredEvent.currentTarget;

			fnGetInvAppData('MONTH', function(data) {


				if($(elem).hasClass("spn-inv-field-report-icon-rep")) 
					that.serverInput.action = "LoadRep";


				var strdata = JSON.stringify(data);

				strdata = strdata.replace(/ID/g,'CodeID');
				strdata = strdata.replace(/ALT/g,'ID');

				data = $.parseJSON(strdata);	

				that.fromMonthDropDown = new DropDownView({
					el : that.$("#div-inv-sub-report-from-month"),
					data : data,
					DefaultID : that.serverInput.frmmonth
				});
				
				that.$("#div-inv-sub-report-from-month").bind('onchange', function (e, selectedId,SelectedName) {
					that.serverInput.frmmonth = selectedId;
					that.currServerInput.frmmonth = selectedId;
				});

				that.toMonthDropDown = new DropDownView({
					el : that.$("#div-inv-sub-report-to-month"),
					data : data,
					DefaultID : that.serverInput.tomonth
				});
				
				that.$("#div-inv-sub-report-to-month").bind('onchange', function (e, selectedId,SelectedName) {
					that.serverInput.tomonth = selectedId;
					that.currServerInput.tomonth = selectedId;
				});

				fnGetInvAppData('YEAR', function(data) {

					that.fromYearDropDown = new DropDownView({
						el : that.$("#div-inv-sub-report-from-year"),
						data : data,
						DefaultName : that.serverInput.frmyear
					});
					
					that.$("#div-inv-sub-report-from-year").bind('onchange', function (e, selectedId,SelectedName) {
						that.serverInput.frmyear = SelectedName;
						that.currServerInput.frmyear = SelectedName;
					});

					that.toMonthDropDown = new DropDownView({
						el : that.$("#div-inv-sub-report-to-year"),
						data : data,
						DefaultName : that.serverInput.toyear
					});
					
					that.$("#div-inv-sub-report-to-year").bind('onchange', function (e, selectedId,SelectedName) {
						that.serverInput.toyear = SelectedName;
						that.currServerInput.toyear = SelectedName;
					});

					var typeData = [{"ID" : "0", "Name": "Unit"},{"ID" : "1", "Name" : "Dollar"}];

					that.typedrpDown = new DropDownView({
						el : that.$("#div-inv-sub-report-type-drp"),
						data : typeData,
						DefaultName : "Dollar"
					});
					
					that.serverInput.type = "1";

					that.$("#div-inv-sub-report-type-drp").bind('onchange', function (e, selectedId,SelectedName) {
						that.serverInput.type = selectedId;
						that.currServerInput.type = selectedId;
					});
					
					that.serverInput.contype = "50400"; 

					that.currServerInput = $.parseJSON(JSON.stringify(that.serverInput));

					that.arrServerInput = new Array();

					that.arrServerInput.push(that.currServerInput)

					that.toggleContent();
				});

			});
		},

		loadData: function(event) {
			var that = this;
			var input;
			
//			if((event!=undefined)&&(event.currentTarget.id=="div-inv-sub-report-load")) {
//				input = this.serverInput;
//				this.breadCrumb = $.parseJSON(JSON.stringify(this.parent.breadCrumb));
//				if(event.currentTarget.id=="div-inv-sub-report-load")
//					this.currServerInput = $.parseJSON(JSON.stringify(this.serverInput));
//			}
//			else
				input = this.arrServerInput[this.arrServerInput.length-1];
			
			
			
			this.$("#ul-inv-sub-report-list").html(this.tplSpinner({"message":"Fetching data ..."}));

			fnGetServerData('reports/fetchfieldinventoryrpt', undefined, input, function(data) {
				
				that.$("#div-inv-report-breadcrumb").html(that.breadCrumb.join(" > "));
				
				console.log(data);

				var arrHeaders = data.header[0].split(',');
				var header = new Array();
				var details = data.details;
				var footer = data.footer;
				
				console.log(JSON.stringify(data));

				details = _.sortBy(details,'Name');
				
				var headerColors = {
						"Name" :  {
							"bgcolor" : "#c3c5b8",
							"color"	: "#000"
						},
						
						"Baseline Sets" : {
							"bgcolor" : "",
							"color"	: ""
						},
						
						"Total <BR>$Value" : {
							"bgcolor" : "",
							"color"	: ""
						},
						
						"#Cases<BR> per Month" : {
							"bgcolor" : "",
							"color"	: ""
						},
						
						"#Cases<BR> per M/Set" : {
							"bgcolor" : "",
							"color"	: ""
						},
						
						"Loaner case in period" : {
							"bgcolor" : "#c3c5b8",
							"color"	: "#000"
						},
						
						"Loaner <BR>Utilization %" : {
							"bgcolor" : "#c3c5b8",
							"color"	: "#000"
						},
						
						"AAS" : {
							"bgcolor" : "#feb469",
							"color"	: "#000"
						},
						
						"AAI" : {
							"bgcolor" : "#feb469",
							"color"	: "#000"
						},
						
						"STD" : {
							"bgcolor" : "#feb469",
							"color"	: "#000"
						},
						
						"% Additional" : {
							"bgcolor" : "#feb469",
							"color"	: "#000"
						},
						
						"Budget" : {
							"bgcolor" : "#c3c5b8",
							"color"	: "#000"
						},
						
						"AI Usage<BR>(Quarter)" : {
							"bgcolor" : "#c3c5b8",
							"color"	: "#000"
						},
						
						"% to <BR>AI Budget" : {
							"bgcolor" : "#c3c5b8",
							"color"	: "#000"
						},
						
				};
				
				var detailColors = {
						"Name" :  {
							"bgcolor" : "#dee0d2",
							"color"	: "#000"
						},
						
						"Baseline Sets" : {
							"bgcolor" : "#e4e6f2",
							"color"	: ""
						},
						
						"Total <BR>$Value" : {
							"bgcolor" : "#f1f2f7",
							"color"	: "#000"
						},
						
						"#Cases<BR> per Month" : {
							"bgcolor" : "#E3F3FF",
							"color"	: "#000"
						},
						
						"#Cases<BR> per M/Set" : {
							"bgcolor" : "#D1DFF1",
							"color"	: "#000"
						},
						
						"Loaner case in period" : {
							"bgcolor" : "#c3c5b8",
							"color"	: "#000"
						},
						
						"Loaner <BR>Utilization %" : {
							"bgcolor" : "#c3c5b8",
							"color"	: "#000"
						},
						
						"AAS" : {
							"bgcolor" : "#fef2cc",
							"color"	: "#000"
						},
						
						"AAI" : {
							"bgcolor" : "#fef2cc",
							"color"	: "#000"
						},
						
						"STD" : {
							"bgcolor" : "#ffecb9",
							"color"	: "#000"
						},
						
						"% Additional" : {
							"bgcolor" : "#ffecb9",
							"color"	: "#000"
						},
						
						"Budget" : {
							"bgcolor" : "#c3c5b8",
							"color"	: "#000"
						},
						
						"AI Usage<BR>(Quarter)" : {
							"bgcolor" : "#c3c5b8",
							"color"	: "#000"
						},
						
						"% to <BR>AI Budget" : {
							"bgcolor" : "#c3c5b8",
							"color"	: "#000"
						},
						
				};

				for(var i=0;i<arrHeaders.length;i++) {
					var headerJSON = {}
					headerJSON["Label"] = arrHeaders[i];
					headerJSON["Color"] = (headerColors[arrHeaders[i]])?headerColors[arrHeaders[i]]["color"]:"";
					headerJSON["BgColor"] = (headerColors[arrHeaders[i]])?headerColors[arrHeaders[i]]["bgcolor"]:"";
					header.push(headerJSON);
				}
				
				var arrDetail = new Array();
				
				var headerPostions = new Array();
				
				for(var j=0;j<details.length;j++) {
					var detail = new Array();
					var prevValue = 0;
					for(var k=0;k<arrHeaders.length;k++) {
						var customDetail = {};
						customDetail["Color"] = (detailColors[arrHeaders[k]])?detailColors[arrHeaders[k]]["color"]:"";
						customDetail["BgColor"] = (detailColors[arrHeaders[k]])?detailColors[arrHeaders[k]]["bgcolor"]:"";
						var label = details[j][arrHeaders[k]];
						var currValue, percent = "", prevPercent = 0, percentColor;

						if(label==undefined || parseFloat(label)==0 || label == "") {
							label = "-";
							if((that.showPercent || that.showArrow) && (_.contains(that.monthIndex,(arrHeaders[k]).split(' ')[0].toLowerCase()))) {
								if(!_.contains(headerPostions,k))
									headerPostions.push(k);
								percent = "0%";
							}
								
						}
						else if(!_.contains(that.skipCalculations, arrHeaders[k])) {
							if(that.serverInput.contype=="50400") {
								if(!_.contains(that.skipDivide, arrHeaders[k] )) {
									if(that.currServerInput.type!="0")
										label = label/1000;
									currValue = label;
									if(that.roundOffValues[arrHeaders[k]] != undefined) {
										label = label.toFixed(that.roundOffValues[arrHeaders[k]]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
									}
									else if(that.currServerInput.type!="0")
										label = label.toFixed(1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
								}
							}
							else
								label = parseFloat(label).toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
							if(_.contains(that.monthIndex,(arrHeaders[k]).split(' ')[0].toLowerCase())) {
								if(that.showPercent || that.showArrow) {

									percent = (currValue - prevValue) / prevValue;
									prevValue = currValue;
									if(!percent || !isFinite(percent))
										percent = 0;
									
									percent = percent*100;
									percent = (percent==0)?"0":(percent).toFixed(2);
									var actualPercent = parseFloat(percent);
									
									
									if(percent<0)
										percent = "(" + Math.abs(percent) + ")";
									percent += "%";
									
									if(actualPercent<prevPercent)
										percentColor = "#800";
									else if(actualPercent>prevPercent)
										percentColor = "green";
									
									console.log(percentColor);
									
									if((that.showArrow) && (parseInt(actualPercent)!=0) ) {
										var arrowIcon;
										if(actualPercent<prevPercent) 
											arrowIcon = "<i class='fa fa-caret-down'></i>";
										else
											arrowIcon = '<i class="fa fa-caret-up"></i>';
										percent += (" " + arrowIcon);
										if(!that.showPercent)
											label += " " +arrowIcon;
									}
									
									prevPercent = actualPercent;
									
									if(!_.contains(headerPostions,k))
										headerPostions.push(k);
								}
							}
						}
						
						customDetail["Label"] = label;
						if((percent!="") && (that.showPercent)) {
							if(that.hideValue) {
								customDetail["Label"] = percent;
								if(percent!="0%")
									customDetail["Color"] = percentColor;
							}
							else {
								customDetail["Percent"] = percent;
								if(percent!="0%")
									customDetail["PercentColor"] = percentColor;
							}	
						}
						else if((percent!="") && (that.showArrow)) {
							customDetail["Label"] = label;
							if(percent!="0%")
								customDetail["Color"] = percentColor;
						}
						
						detail.push(customDetail);
					}
					arrDetail.push({"ID" : details[j]['ID'],"detail" : detail});
				}

				if((that.showPercent) && !(that.hideValue)) {
					headerPostions = _.sortBy(headerPostions);
					for(var i=0; i<headerPostions.length;i++) 
						header.splice(headerPostions[i]+i+1, 0, '');
				}
				
				var arrFooter = new Array();
				footer[0]['Name'] = "Total";
				
				for(var j=0;j<footer.length;j++) {
					var detail = new Array();
					var prevValue = 0;
					for(var k=0;k<arrHeaders.length;k++) {
						var customDetail = {};
						customDetail["Color"] = "#fff";
						customDetail["BgColor"] = "#000";
						customDetail["PercentColor"] = "#fff";
						var label = footer[j][arrHeaders[k]];
						var currValue, percent = "", prevPercent = 0, percentColor;

						if(label==undefined || parseFloat(label)==0 || label == "") {
							label = "-";
							if((that.showPercent || that.showArrow) && (_.contains(that.monthIndex,(arrHeaders[k]).split(' ')[0].toLowerCase()))) {
								if(!_.contains(headerPostions,k))
									headerPostions.push(k);
								percent = "0%";
							}
								
						}
						else if(!_.contains(that.skipCalculations, arrHeaders[k])) {
							if(that.serverInput.contype=="50400") {
								if(!_.contains(that.skipDivide, arrHeaders[k] )) {
									if(that.currServerInput.type!="0")
										label = label/1000;
									currValue = label;
									if(that.roundOffValues[arrHeaders[k]] != undefined) {
										label = label.toFixed(that.roundOffValues[arrHeaders[k]]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
									}
									else if(that.currServerInput.type!="0")
										label = label.toFixed(1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
								}
							}
							else
								label = parseFloat(label).toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
							if(_.contains(that.monthIndex,(arrHeaders[k]).split(' ')[0].toLowerCase())) {
								if(that.showPercent || that.showArrow) {

									percent = (currValue - prevValue) / prevValue;
									prevValue = currValue;
									if(!percent || !isFinite(percent))
										percent = 0;
									
									percent = percent*100;
									percent = (percent==0)?"0":(percent).toFixed(2);
									var actualPercent = parseFloat(percent);
									
									
									if(percent<0)
										percent = "(" + Math.abs(percent) + ")";
									percent += "%";
									
									if(actualPercent<prevPercent)
										percentColor = "#800";
									else if(actualPercent>prevPercent)
										percentColor = "green";
									
									
									if((that.showArrow) && (parseInt(actualPercent)!=0) ) {
										var arrowIcon;
										if(actualPercent<prevPercent) 
											arrowIcon = "<i class='fa fa-caret-down'></i>";
										else
											arrowIcon = '<i class="fa fa-caret-up"></i>';
										percent += (" " + arrowIcon);
										if(!that.showPercent)
											label += " " +arrowIcon;
									}
									
									prevPercent = actualPercent;
									
									if(!_.contains(headerPostions,k))
										headerPostions.push(k);
								}
							}
						}
						
						customDetail["Label"] = label;
						console.log(percent);
						if((percent!="") && that.showPercent) {
							if(that.hideValue) {
								customDetail["Label"] = percent;
								console.log("percent>>>>" + percent);
								if(percent!="0%")
									customDetail["Color"] = percentColor;
							}
							else {
								customDetail["Percent"] = percent;
								if(percent!="0%")
									customDetail["PercentColor"] = percentColor;
							}
								
						}
						else if((percent!="") && (that.showArrow)) {
							customDetail["Label"] = label;
							if(percent!="0%")
								customDetail["Color"] = percentColor;
						}
						
						detail.push(customDetail);
					}
					arrFooter.push({"ID" : footer[j]['ID'],"detail" : detail});
				}
				
				var viewJSON = {};
				viewJSON.header = header;
				viewJSON.details = arrDetail;
				viewJSON.footer = arrFooter;
				
				console.log(viewJSON);

				that.$("#ul-inv-sub-report-list").html(that.template_list(viewJSON));
				
				that.showIcons();

			});
		},

		showIcons: function() {

			var arrDrillDownIcons = new Array();

			console.log(this.currServerInput.action);

			switch(this.currServerInput.action) {
				case "LoadRep":
					arrDrillDownIcons.push("grp");
					arrDrillDownIcons.push("part");
					break;

				case "LoadPart":
					arrDrillDownIcons.push("grp");
					arrDrillDownIcons.push("rep");
					break;

				case "LoadGCONS":
					arrDrillDownIcons.push("fs");
					arrDrillDownIcons.push("cons");
					arrDrillDownIcons.push("rep");
					break;

				case "LoadDCONS":
					arrDrillDownIcons.push("grp");
					arrDrillDownIcons.push("cons");
					arrDrillDownIcons.push("rep");
					break;

				case "LoadGroup":
					arrDrillDownIcons.push("dist");
					arrDrillDownIcons.push("part");
					arrDrillDownIcons.push("rep");
					break;

				case "LoadDistributor":
					arrDrillDownIcons.push("grp");
					arrDrillDownIcons.push("part");
					arrDrillDownIcons.push("rep");
					break;
			}

			var that = this;

			_.each(arrDrillDownIcons,function(icon) {

				that.$(".td-index-0 .spn-inv-field-report-icon-" + icon).removeClass("inv-hide");

			});

			

		},

		loadDrillDown: function(event) {
			event.stopImmediatePropagation();
			var icon = event.currentTarget;


			switch(this.currServerInput.action) {
				case "LoadVP":
					this.currServerInput.vpid = $(icon).parent().parent().attr('id');
					this.breadCrumb.push($(icon).parent().text().trim().split('\n').slice(-1)[0].trim() + "(VP)");
					break;

				case "LoadAD":
					this.currServerInput.adid = $(icon).parent().parent().attr('id');
					this.breadCrumb.push($(icon).parent().text().trim().split('\n').slice(-1)[0].trim() + "(AD)");
					break;

				case "LoadDCONS":
				case "LoadDistributor":
					this.currServerInput.did = $(icon).parent().parent().attr('id');
					this.breadCrumb.push($(icon).parent().text().trim().split('\n').slice(-1)[0].trim() + "(FS)");
					break;

				case "LoadGCONS":
				case "LoadGroup":
					this.currServerInput.setnum = $(icon).parent().parent().attr('id');
					this.breadCrumb.push($(icon).parent().text().trim().split('\n').slice(-1)[0].trim() + "(S)");
					break;

				case "LoadRep":
					this.currServerInput.repid = $(icon).parent().parent().attr('id');
					this.breadCrumb.push($(icon).parent().text().trim().split('\n').slice(-1)[0].trim() + "(R)");
					break;

				case "LoadPart":
					this.currServerInput.pnum = $(icon).parent().parent().attr('id');
					this.breadCrumb.push($(icon).parent().text().trim().split('\n').slice(-1)[0].trim() + "(P)");
					break;
			}
			
			if($(icon).hasClass("spn-inv-field-report-icon-cons")) {
				var that = this;
				input = $.parseJSON(JSON.stringify(this.currServerInput));
				input.action = "LoadCons";
				
				this.parent.$("#div-inv-consignment-detail-main").show();
				this.parent.$("#div-inv-consignment-detail-list").html(this.tplSpinner({"message":lblNm("msg_data_fetching")}));
				fnGetServerData('reports/fetchconsigndetails', 'listGmConsignDetailVO',  input, function(data) {
					var viewData = new Array();
					that.breadCrumb.pop();
					if(data.length==undefined)
						viewData.push(data);
					else
						viewData = data;
					
					that.parent.$("#div-inv-consignment-detail-list").html(that.parent.template_consignmentlist(viewData));
				});
			}
			else {
				if($(icon).hasClass("spn-inv-field-report-icon-ad")) 
					this.currServerInput.action = "LoadAD";
				else if($(icon).hasClass("spn-inv-field-report-icon-fs")) 
					this.currServerInput.action = "LoadDCONS";
				else if($(icon).hasClass("spn-inv-field-report-icon-sys")) 
					this.currServerInput.action = "LoadGCONS";
				else if($(icon).hasClass("spn-inv-field-report-icon-rep")) 
					this.currServerInput.action = "LoadRep";
				else if($(icon).hasClass("spn-inv-field-report-icon-part")) 
					this.currServerInput.action = "LoadPart";
				else if($(icon).hasClass("spn-inv-field-report-icon-grp")) 
					this.currServerInput.action = "LoadGroup";
				else if($(icon).hasClass("spn-inv-field-report-icon-dist")) 
					this.currServerInput.action = "LoadDistributor";

				
				this.arrServerInput.push($.parseJSON(JSON.stringify(this.currServerInput)));

				this.loadData();
			}
			
					
		},

		showAiData: function(event) {
			var elem = event.currentTarget;
			if($(elem).hasClass("fa-square-o"))
				this.serverInput.showai = "Y";
			else
				this.serverInput.showai = "";
			$(elem).toggleClass("fa-square-o fa-check-square-o");
		},

		display: function(key) {
			
			if(key.currentTarget==undefined) {
				this.key = key;
			}else{
				this.key = "consign-info";
			}
			this.$(".div-inv-sub-report-settype-label").each(function() {
				$(this).removeClass("selected-tab");
			});

			this.$("#li-inv-sub-report-showall").show();

			if(key=="loaner-info")
				this.$("#li-inv-sub-report-showall").hide();
			
			this.$("#div-inv-sub-report-" + this.key).addClass("selected-tab");
			this.$(".ul-inv-sub-report-info").html("");
			this.$("#ul-inv-sub-report-" + this.key).show();

		},

		toggleContent: function (event) {
			var tabid;
			if(!event) 
				tabid = "div-inv-sub-report-consign-info";
			else
				tabid = (event.currentTarget.id);
			  
			switch(tabid) {
				case "div-inv-sub-report-consign-info":
					this.display("consign-info");
					this.serverInput.salestype = "50300";
					this.loadData();
					break;	

				case "div-inv-sub-report-loaner-info":
					this.display("loaner-info");
					this.serverInput.salestype = "50301";
					this.loadData();
				break;
						
				case "div-inv-sub-report-all-info":
					this.display("all-info");
					this.serverInput.salestype = "50302";
					this.loadData();
					break;

				}

		},

		checkBox: function(event) {
			$(event.currentTarget).toggleClass("fa-square-o fa-check-square-o");
			var value = true;
			if($(event.currentTarget).hasClass('fa-square-o'))
				value = false;
			switch($(event.currentTarget).parent().attr("id")) {
				case "li-inv-sub-report-showgrowthpercent":
					this.showPercent = value;
					break;
					
				case "li-inv-sub-report-showgrowtharrow":
					this.showArrow = value;
					break;
					
				case "li-inv-sub-report-hidevalue":
					this.hideValue = value;
					break;
			}
		},

		close: function() {
			this.undelegateEvents();
			this.unbind();
		}
	});	

	return FieldReportSubView;
});
