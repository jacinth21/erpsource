/**********************************************************************************
 * File:        GmInvShipToInfoView.js
 * Description: 
 * Version:     1.0
 * Author:     	tsekaran
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification','datasync','saleitemmodel', 'saleitemcollection', 'saleitemlistview','itemview', 'invshipaddressview','dropdownview'
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification,DataSync, SaleItem, SaleItems, SaleItemListView,ItemView, InvShipAddressView,DropDownView) {

	'use strict';
	var InvShipToInfoView = Backbone.View.extend({

		events : {
			"click .li-inv-shiptoinfo-checkbox-header": "selectMultiplebox",
			"click .li-inv-shiptoinfo-checkbox": "removeCheck",
			"click #btn-address" : "getaddress",
            "change .sel-address-carrier": "getDefaultCarrierMode",
            "change .sel-address-mode": "getDefaultMode",
            "change .sel-address-carrier": "getCarrier"
		},
		
		/* Load the templates */ 
		template: fnGetTemplate(URL_Inv_Template, "GmInvShipToInfo"),
		templateMobile: fnGetTemplate(URL_Inv_Template, "GmInvShipToInfoMobile"),
		
		initialize: function (options) {
		    var that = this;
		    this.parent = options.parent;

		    GM.Global.DefaultShipto = "";
		    GM.Global.DefaultCarrier = "";
		    GM.Global.DefaultMode = "";
		    GM.Global.defaultAddress = "";
		    this.dfltAcct = "";
		    this.dfltRep = "";
		    this.dfltDist = "";
		    this.dfltEmployee = "";
		    this.dfltShippingAddress = "";
		    this.dfltDealer = "";

		    fnGetDftShipById(function (data) {
		        if (GM.Global.DefaultShipto == "4122") {
		            that.shipToID = that.parent.JSONData.acctid;
		        } else if (GM.Global.DefaultShipto == "4121") {
		            that.shipToID = that.parent.JSONData.repid;
		        } else if (GM.Global.DefaultShipto == "107801") {
                    fnGetDealerIDByAccId(that.parent.JSONData.acctid, function (ddt) {
                       that.shipToID = ddt[0].Dealerid; 
                    });
		        } else {
                    GM.Global.DefaultShipto = "4120";
		            that.shipToID = that.parent.JSONData.did;
		        }

		        fnGetDflCarrier(function (cdt) {
		            fnGetDflMode(function (mdt) {
		                if ((that.parent.JSONData.gmTxnShipInfoVO.length == undefined) || (that.parent.JSONData.gmTxnShipInfoVO.length == 0))
		                    that.initializeAddress();
		                else
		                    that.reInitialize();
		            });
		        });
		    });

		},
		
		initializeAddress: function() {
			
			var that = this;
 //based on the shipto Id , get the address
			fnGetAddressByID(this.shipToID, GM.Global.DefaultShipto, function(data) {
				if(data.length>0) {
					var data = data[0];
					var JSONArray = new Array();
					that.viewJSON = new Array();
					
					for(var i=0;i<that.parent.JSONSets.length;i++) {

							var JSONShip = {};
							JSONShip.source = that.getSource();
							JSONShip.addressid = data.AddressID;
							JSONShip.attnto = data.Name;
							JSONShip.ship_to_id = that.shipToID;
							JSONShip.shipto = GM.Global.DefaultShipto;
							JSONShip.shipcarrier = GM.Global.DefaultCarrier;
							JSONShip.shipmode = GM.Global.DefaultMode;
							JSONShip.shipinstruction = "";
                            JSONShip.addresstype = parseNull(data.AddressType);
							JSONArray.push({"indexValue" : that.parent.JSONSets[i].indexValue, "ShipInfo" : $.parseJSON(JSON.stringify(JSONShip)) });
							
							that.parent.JSONSets[i].bgColor = "#800";
							that.parent.JSONSets[i].shipTo = JSONShip;
							that.parent.JSONSets[i].shipTo.carrierName = GM.Global.DefaultCarrierName;
							that.parent.JSONSets[i].shipTo.modeName = GM.Global.DefaultModeName;
							that.parent.JSONSets[i].AddressData = data;
							that.viewJSON.push($.parseJSON(JSON.stringify(that.parent.JSONSets[i])));
							
						 if(!(that.parent.requesttype == 1006507 || that.parent.requesttype == 107286))
							that.parent.JSONData.arrGmCaseSetInfoVO[i].arrGmPartDetailVO = [];
						 else
							that.parent.JSONData.arrGmCaseSetInfoVO[i].addressid = data.AddressID;
							that.parent.JSONData.arrGmCaseSetInfoVO[i].gmTxnShipInfoVO = {};								
							that.parent.JSONData.arrGmCaseSetInfoVO[i].gmTxnShipInfoVO = JSONShip;
							that.parent.JSONData.gmTxnShipInfoVO = $.parseJSON(JSON.stringify(JSONArray));
					}
					that.render(that.viewJSON); 
					
				}
				else
					that.render(that.parent.JSONSets);	
			});
				
			
		},

		reInitialize: function() {
			
			var that = this;
			if(this.invshipaddressview) 
						this.invshipaddressview.close();
			
			fnGetAddressByID(this.shipToID, GM.Global.DefaultShipto, function(data) {

				if(data.length>0) {
					var data = data[0];
					var JSONArray = new Array();
					that.viewJSON = new Array();
					for(var i=0;i<that.parent.JSONSets.length;i++) {
						
						if(that.parent.JSONSets[i].shipTo==undefined) {
								
								var JSONShip = {};
								JSONShip.source = that.getSource();
								JSONShip.addressid = data.AddressID;
								JSONShip.attnto = data.Name;
								JSONShip.ship_to_id = that.shipToID;
								JSONShip.shipto = GM.Global.DefaultShipto;
								JSONShip.shipcarrier = GM.Global.DefaultCarrier;
								JSONShip.shipmode = GM.Global.DefaultMode;
								JSONShip.shipinstruction = "";
                                JSONShip.addresstype = parseNull(data.AddressType);
								that.parent.JSONData.gmTxnShipInfoVO.push({"indexValue" : that.parent.JSONSets[i].indexValue, "ShipInfo" : JSONShip});
								that.parent.JSONSets[i].bgColor = "#800";
								that.parent.JSONSets[i].shipTo = JSONShip;
								that.parent.JSONSets[i].AddressData = data;
								that.parent.JSONSets[i].shipTo.carrierName = GM.Global.DefaultCarrierName;
								that.parent.JSONSets[i].shipTo.modeName = GM.Global.DefaultModeName;
								that.viewJSON.push($.parseJSON(JSON.stringify(that.parent.JSONSets[i])));
								
							if(!(that.parent.requesttype == 1006507 || that.parent.requesttype == 107286))
									that.parent.JSONData.arrGmCaseSetInfoVO[i].arrGmPartDetailVO = [];
							else
								that.parent.JSONData.arrGmCaseSetInfoVO[i].addressid = data.AddressID;
								that.parent.JSONData.arrGmCaseSetInfoVO[i].gmTxnShipInfoVO = {};								
								that.parent.JSONData.arrGmCaseSetInfoVO[i].gmTxnShipInfoVO = JSONShip;
							}
							else{
								
								 that.viewJSON.push($.parseJSON(JSON.stringify(that.parent.JSONSets[i])));
								
							}
				}
				that.render(that.viewJSON);
					
				}else
					that.render(that.parent.JSONSets);
				
				
			});
			
			
		},
		
		getSource :function(){
			var source = "";
			if(this.parent.requesttype == 1006505) 
				source = "50182";
			if(this.parent.requesttype == 1006506 || this.parent.requesttype == 1006507 || this.parent.requesttype == 107286)
				source = "50181";
			return source;
		},

		// Render the initial contents when the Catalog View is instantiated
		render: function (viewJSON) {
//			showMsgView("011");
            if ($(window).width() < 480) {
                  this.$el.html(this.templateMobile(viewJSON));
                $(".invShipInfoWrap ul").addClass("ul-show-inv-list");
                $(".invShipInfoWrap .togglebx i").removeClass("onActive");
                $(".invShipInfoWrap ul:last-child").addClass("showCheckInfo");
                $(".invShipInfoWrap ul.showCheckInfo").removeClass("ul-show-inv-list");
                $(".invShipInfoWrap ul.showCheckInfo").find(".togglebx img").addClass("onActive");

                $(".togglebx").unbind('click').bind('click', function() {
                    $(this).parent("ul").toggleClass('ul-show-inv-list');
                    $(this).children("img").toggleClass("onActive");
                });
            } else {
                  this.$el.html(this.template(viewJSON));
            }
			this.indicateAddressType();
			var that = this;
			if(this.parent.JSONData.repid == undefined || this.parent.JSONData.repid == "") {
				var partyid = window.localStorage.getItem("partyID");
				fnGetUserDOID(partyid,function(result){
						that.repid = result.C703_SALES_REP_ID;
						
						fnGetAssocSalesRep(result.C703_SALES_REP_ID,function(assList){
							if(assList.length > 0)
								{
									
									that.repid =  assList[0].repid;
								}
						});
				});
				
			}
			return this;
		},

		indicateAddressType: function () {
			this.$(".ul-inv-shipinfo-parts-list").each(function() {
				switch($(this).attr("data-shipto")) {
					case "4121":
                        //added for PC-938 - To indicate address type as H if shipping address selected as Hospital in Sales Rep
                        var el = $(this);
                        fnGetAddressType(el.attr("data-address-id"),function(data){
                        	if(data != ""){
                        		if(data[0].ADDRTYPE == 26240675){
                        			el.find(".li-inv-shiptoinfo-address-indicator").text("H");
                                    el.find(".sinfo-type").text("H");
                        		}
                        		else{
                        			el.find(".li-inv-shiptoinfo-address-indicator").text("S");
                                    el.find(".sinfo-type").text("S");
                        		}
                        	}
                        });
						break;
					case "4122":
						$(this).find(".li-inv-shiptoinfo-address-indicator").text("H");
                        $(this).find(".sinfo-type").text("H");
						break;
					case "4120":
						$(this).find(".li-inv-shiptoinfo-address-indicator").text("D");
                        $(this).find(".sinfo-type").text("D");
						break;
                    case "107801":
						$(this).find(".li-inv-shiptoinfo-address-indicator").text("DE");
                        $(this).find(".sinfo-type").text("DE");
						break;
				}
			});
		},

		selectMultiplebox: function(event) {
			var elem = event.currentTarget;
			if($(elem).hasClass('fa fa-check selected')){
				$(elem).removeClass('fa fa-check selected');
				this.$(".li-inv-shiptoinfo-checkbox").removeClass("fa fa-check selected");
			}
			else {
				$(elem).addClass('fa fa-check selected');
				this.$(".li-inv-shiptoinfo-checkbox").addClass("fa fa-check selected");
			}
		},

		removeCheck:function(event){
			event.preventDefault();
			var elem = event.currentTarget;
			
			if($(elem).hasClass("selected")) {
				this.$(elem).removeClass("fa fa-check selected");
				$(".li-inv-shiptoinfo-checkbox-header").removeClass("fa fa-check selected");
			}
			else {
				this.$(elem).addClass("fa fa-check selected");
			}
			if ($(".li-inv-shiptoinfo-checkbox").length == $(".li-inv-shiptoinfo-checkbox.selected").length) {
				$(".li-inv-shiptoinfo-checkbox-header").addClass("fa fa-check selected");
			}
		},

		getaddress: function () {

		    this.$(".li-inv-shiptoinfo-checkbox").toggleClass("force-bg-light-grey");
		    if (this.$("#div-shipping-address").html().trim() == "" || this.$("#div-shipping-address").css("display") == "none") {
		        this.$("#div-shipping-address").show();
		        var that = this;
		        fnGetDistInfo(this.parent.JSONData.did, function (data) {
		            fnGetDealerIDByAccId(that.parent.JSONData.acctid, function (data) {
		                var dealid = "";
		                if (that.invshipaddressview)
		                    that.invshipaddressview.close();
		                if (data.length > 0) {
		                    dealid = data[0].Dealerid
		                }
		                that.invshipaddressview = new InvShipAddressView({
		                    el: "#div-shipping-address",
		                    dfltAcct: that.parent.JSONData.acctid,
		                    dfltRep: (that.parent.JSONData.repid) ? that.parent.JSONData.repid : that.repid,
		                    dfltDist: that.parent.JSONData.did,
		                    dfltDealer: dealid,
		                    onListRender: function () {
		                        that.onListRender();
		                    }
		                });
		                that.$("#div-shipping-address").bind("onselect", function (event, id, elem) {
		                    that.getAddressColors(id, elem);
		                });
		            });
		        })

		    } else {
		        this.$("#div-shipping-address").hide();
		        this.$(".li-inv-shiptoinfo-checkbox").removeClass("fa fa-check selected");
		        this.$(".li-inv-shiptoinfo-checkbox-header").removeClass('fa fa-check selected');
		    }
		},

		onListRender: function() {
			var that = this;
            
             $('.sel-address-carrier').each(function () {
                 fnCreateOptions(this, GM.Global.totalCarrier);
                 $(this).val($(this).attr("value"));
             });
             GM.Global.totalMode = _.filter(GM.Global.totalMode, function (data) {
                 return data.ID != 0;
             })
             GM.Global.totalMode.push({
                 "ID": 0,
                 "Name": "Choose one"
             });

             $(".sel-address-mode").each(function () {
                 fnCreateOptions(this, GM.Global.totalMode);
                 $(this).val($(this).attr("value"));
             });

             $(".sel-address-carrier").each(function () {
                 if ($(this).val() == undefined || $(this).val() == "" || $(this).val() == null)
                     $(this).val(GM.Global.DefaultCarrier);
             });
             $('.sel-address-mode').each(function () {
                 if ($(this).val() == undefined || $(this).val() == "" || $(this).val() == null)
                     $(this).val(GM.Global.DefaultMode);
             });
            
			var ShipInfos = _.pluck(this.parent.JSONData.gmTxnShipInfoVO,"ShipInfo");
			var idList = _.pluck(ShipInfos,"addressid");
			$(".div-address-row").each(function() {
				var id = this.id.replace("div-address-","");
			if(_.contains(idList,id)) {
					var JSONAddress = _.findWhere(ShipInfos,{addressid: id});
					var carrier = JSONAddress.shipcarrier;
					var mode = JSONAddress.shipmode;
					var attnto = JSONAddress.attnto;
					var shipinstruction = JSONAddress.shipinstruction;
					$(this).find(".sel-address-carrier").val(carrier.toString());
					$(this).find(".sel-address-mode").val(mode.toString());
					$(this).find(".spn-attn-to").text(attnto.toString());
					$(this).find(".fa-info-circle").attr("data-instruction", shipinstruction.toString());
				}
			});
		},

		getAddressColors:function(id, elem){
			
			var that = this;
			var carrier = $(elem).find('.sel-address-carrier').val();
			var mode = $(elem).find('.sel-address-mode').val();
			var attnto = $(elem).find('.spn-attn-to').text();
			var shipInstruction = $(elem).find(".fa-info-circle").attr("data-instruction");
			var carrierName = $(elem).find('.sel-address-carrier option:selected').text();
			var modeName = $(elem).find('.sel-address-mode option:selected').text();
            var addrtype = $(elem).attr("data-address-type");
			
			this.$('#div-inv-shiptoinfo-sets-list').find(".ul-inv-shipinfo-parts-list").each( function() {
				if($(this).find(".li-inv-shiptoinfo-checkbox").hasClass("selected")) {
					$(this).attr("data-address-id",id);
					$(this).attr("data-ship-to-id",that.invshipaddressview.getShipInfo().shipToID);
					$(this).attr("data-shipto",that.invshipaddressview.getShipInfo().shipTo);
					$(this).attr("data-attn-to",attnto);
					$(this).attr("data-instruction",shipInstruction);
                    $(this).attr("data-address-type",addrtype);
					
					var x = $(elem).css("background-color");
					var div = document.createElement('div');
					div.innerHTML = $(elem).find(".div-address").html();
					$(div).attr("class",id);
					$(div).find("i").remove();
					$(this).find(".li-shiptoinfo-address").html(div);
					$(this).find(".selected").css("background-color",x);

					$(this).find(".li-carrier-name").text(carrierName);
					$(this).find(".li-mode-name").text(modeName);
				}
				else if($(this).attr("data-address-id").toString()==id.toString()) {
					$(this).find(".li-carrier-name").text(carrierName);
					$(this).find(".li-mode-name").text(modeName);
					$(this).find(".spn-attn-to").text(attnto);
					$(this).attr("data-attn-to",attnto);
					$(this).attr("data-instruction",shipInstruction);
				}
			});
			 that.indicateAddressType();
			 that.updateJSON(id, carrier, mode);
		},

		updateJSON: function (id, carrier, mode) {

		        var JSONArray = new Array();
		        var that = this;
		        this.$('#div-inv-shiptoinfo-sets-list').find(".ul-inv-shipinfo-parts-list").each(function (i) {
		            // var index = $(this).attr("data-index");
		            var JSONShip = {};

		            JSONShip.source = that.getSource();
		            JSONShip.addressid = $(this).attr("data-address-id");
		            JSONShip.attnto = $(this).attr("data-attn-to");
		            JSONShip.ship_to_id = $(this).attr("data-ship-to-id");
		            JSONShip.shipto = $(this).attr("data-shipto");
		            JSONShip.shipinstruction = $(this).attr("data-instruction");
                    JSONShip.addresstype = $(this).attr("data-address-type");

		            if (JSONShip.addressid == id) {
		                JSONShip.shipcarrier = carrier;
		                JSONShip.shipmode = mode;
		                $(this).attr("data-carrier", carrier);
		                $(this).attr("data-mode", mode);
		            } else {
		                JSONShip.shipcarrier = $(this).attr("data-carrier");
		                JSONShip.shipmode = $(this).attr("data-mode");
		            }

		            var addrText = $(this).find('.li-shiptoinfo-address').find('ul');


		            that.parent.JSONSets[i].AddressData = {};
		            that.parent.JSONSets[i].AddressData.Name = $(addrText).find('li:nth-child(1)').text().trim();
		            that.parent.JSONSets[i].AddressData.Add1 = $(addrText).find('li:nth-child(2)').text().trim();
		            that.parent.JSONSets[i].AddressData.Add2 = $(addrText).find('li:nth-child(3)').text().trim();
		            that.parent.JSONSets[i].AddressData.City = $(addrText).find('li:nth-child(4)').text().trim().split(",")[0];
		            that.parent.JSONSets[i].AddressData.State = $(addrText).find('li:nth-child(4)').text().trim().split(",")[1];
		            that.parent.JSONSets[i].AddressData.Zip = $(addrText).find('li:nth-child(5)').text().trim();
		            that.parent.JSONSets[i].bgColor = $(this).find(".li-inv-shiptoinfo-checkbox").css("background-color");

		            that.parent.JSONSets[i].shipTo = JSONShip;
		            that.parent.JSONSets[i].shipTo.carrierName = $(this).find(".li-carrier-name").text().trim();
		            that.parent.JSONSets[i].shipTo.modeName = $(this).find(".li-mode-name").text().trim();
		            JSONArray.push({
		                "indexValue": $(this).attr("data-index"),
		                "ShipInfo": JSONShip
		            });

		            if (!(that.parent.requesttype == 1006507 || that.parent.requesttype == 107286))
		                that.parent.JSONData.arrGmCaseSetInfoVO[i].arrGmPartDetailVO = [];
		            else
		                that.parent.JSONData.arrGmCaseSetInfoVO[i].addressid = JSONShip.addressid;

		            that.parent.JSONData.arrGmCaseSetInfoVO[i].gmTxnShipInfoVO = {};
		            that.parent.JSONData.arrGmCaseSetInfoVO[i].gmTxnShipInfoVO = JSONShip;

		        });

		        this.parent.JSONData.gmTxnShipInfoVO = $.parseJSON(JSON.stringify(JSONArray));
		    },
        
		    //get Default Ship Mode		
		    getDefaultMode: function (e) {
		        $(e.currentTarget).attr("mode", $(e.currentTarget).val());
		        $(e.currentTarget).parent().trigger("click");
		    },

		    ////get Default Ship Carrier	
		    getCarrier: function (e) {
		        $(e.currentTarget).parent().find(".sel-address-mode").attr("mode", "");
		        $(e.currentTarget).parent().trigger("click");
		        this.getDefaultCarrierMode(e)
		    },

		    ////get Default Ship Carrier	Mode     
		    getDefaultCarrierMode: function (e) {
		        var that = this;
		        var carrierID = $(e.currentTarget).val();
		        fnGetModeList(carrierID, localStorage.getItem("cmpid"), function (data) {

		            _.each(data, function (model) {
		                fnGetCodeNamebyId(model.ID, function (modelist) {
		                    model.Name = modelist.Name;
		                    $(e.currentTarget).parent().find(".sel-address-mode").html("");
		                    $(e.currentTarget).parent().find(".sel-address-mode").each(function () {
		                        fnCreateOptions(this, data);
		                        $(this).val($(this).attr("value"));
		                    });
		                    fnGetRuleValueByCompany(carrierID, "SHIPCARDEFUALTMODE", localStorage.getItem("cmpid"), function (data) {
		                        if (data.length > 0) $(e.currentTarget).parent().find(".sel-address-mode").val(data[0].RULEVALUE);
		                        else $(e.currentTarget).parent().find(".sel-address-mode").val(0);
		                        if ($(e.currentTarget).parent().find(".sel-address-mode").attr("mode") != "" && $(e.currentTarget).parent().find(".sel-address-mode").attr("mode") != undefined) {
		                            $(e.currentTarget).parent().find(".sel-address-mode").val($(e.currentTarget).parent().find(".sel-address-mode").attr("mode"));
		                        }
		                        $(e.currentTarget).parent().trigger("click");
		                    });
		                });
		            });
		        });
		    },


		    close: function () {

		        this.unbind();
		        this.undelegateEvents();
		    }
	});	

	return InvShipToInfoView;
});
