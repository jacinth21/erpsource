/**********************************************************************************
 * File:        GmInvMainView.js
 * Description: Initial Inventory Management View to load other sub views
 * Version:     1.0
 * Author:     	asivaraj
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification','invdetailview','invsentrequestinfoview','invreportview','invlotonhandview','dropdownview','dashmorefilterview', 'daocase','../sales/inv/view/GmInvSetReqBorrowRptView', '../sales/inv/view/GmInvTagShareOptionView'
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification, InvDetailView, InvReqInfoView, InvReportView, InvLotOnHandView, DropDownView,DashMoreFilterView,DAOCase,InvReqBorrowReportView,InvTagShareOptionView ) {

	'use strict';
	var InvMainView = Backbone.View.extend({

		events : {
			"click .div-inv-keypad-values" : "showNumericKeypad",
			"click .div-keys" : "getKeyValues",
			"click .div-num-symbol" : "deleteKeys",
			"click #div-keypad-close" : "closeKeypad",
			"click #div-btn-done" : "displayQuantity",
			"click #btn-case-link" : "openLink"			
		},

		/* Load the templates */ 
		template: fnGetTemplate(URL_Inv_Template, "GmInvMain"),
		template_numberkeypad: fnGetTemplate(URL_Inv_Template, "GmInvNumberKeypad"),

		initialize: function(options) {
			$("body").removeClass("background-1");
			$("#div-app-title").html(lblNm("inventory_dashboard"));
			this.requesttypeid = "";
			this.render(options);
		},

		// Render the initial contents when the View is instantiated
		render: function (options) {
//			showMsgView("011");
			this.$el.html(this.template());
			var that =this;
             if(GM.Global.DoValidation != undefined ){
                showError(GM.Global.DoValidation);
                GM.Global.DoValidation = undefined;
            }
            //to hide the "Report" based on the company
          fnGetRuleValueByCompany ("INV_RPT_TAB","APPHID",localStorage.getItem("cmpid"),function(data){
          if(data.length > 0) {              
             if(data[0].RULEVALUE=='Y'){
                 $("#btn-case-report").hide();
                 $(".div-inv-detail-navigation .btn-detail").css("width","50%");
                }
          }
            });
			//Show the app name in the Menu
			$('#div-app-title').removeClass('gmVisibilityHidden').addClass('gmVisibilityVisible');
       //$("#div-app-title").removeClass("gmMediaVisibilityH").addClass("gmMediaVisibilityV");

                this.strViewType = options[0];
                if (!this.strViewType) {
                    this.strViewType = "request";
                }
                switch (this.strViewType) {
                    case "request":

                        if (this.invdetailview)
                            this.invdetailview.close();
                        this.invdetailview = new InvDetailView({
                            'el': this.$('#div-inv-main-content'),
                            'parent': this,
                            'requesttype': this.requesttypeid
                        });
                        this.$("#div-inv-title").html(lblNm("request") + " - ");
                        this.display("request");
                        break;

                    case "approvals":
                        this.invsentrequestinfoview = new InvReqInfoView({
                            'el': this.$('#div-inv-main-content'),
                            'parent': this,
                            'requesttype': this.requesttypeid
                        });
                        this.$("#div-inv-title").html(lblNm("approvals_item_consignment"));
                        this.display("approvals");
                        break;

                    case "report":
                        this.invreportview = new InvReportView({
                            'el': this.$('#div-inv-main-content'),
                            'parent': this,
                            'requesttype': this.requesttypeid
                        });
                        this.$("#div-inv-title").html(lblNm("report") + " - ");
                        this.display("report");
                        break;

                    case "lotonhand":
                        this.invlotonhandview = new InvLotOnHandView({
                            'el': this.$('#div-inv-main-content'),
                            'parent': this,
                            'requesttype': this.requesttypeid
                        });
                        this.display("lotonhand");
                        break;
                    case "morefilter":
                        moreFilterHash = "#inv/report";
                        this.morefilter = new DashMoreFilterView({
                            'el': this.$('#div-inv-main-content'),
                            'parent': this
                        });
                        this.$("#div-inv-title").html(lblNm("more_filter"));
                        this.display("report");
                        break;
                    case "invreport":
                        this.invSetReqBorrowRpt = new InvReqBorrowReportView({
                            'el': this.$('#div-inv-main-content'),
                            'parent': this
                        });
                        this.$("#div-inv-title").html("SYSTEM LOCATOR REPORT");
                        this.display("invreport");
                        this.$("#btn-new-inv-more-filter").removeClass("bg-red");
						this.currentView = this.invSetReqBorrowRpt;
						this.moreFilterInput = {"token" : localStorage.getItem('token'), "passname" : "1"};
                        break;
                    case "invtagshareOption":
                        this.InvTagShareOptionView = new InvTagShareOptionView({
                            'el': this.$('#div-inv-main-content'),
                            'parent': this
                        });
                        this.$("#div-inv-title").html("TAG SHARING RPT");
                        this.display("invtagshareOption");
                        this.$("#btn-new-inv-more-filter").removeClass("bg-red");
						this.currentView = this.invTagShareOptionRpt;
						//this.moreFilterInput = {"token" : localStorage.getItem('token'), "passname" : "1"};
                        break;

                }
                
                fnGetExcSetFL(function(excSetFL){});
            
            return this;
            
		},

		// Toggles the view
		display: function(key) {
                $("#ui-datepicker-div").hide();
                if (key.currentTarget == undefined) {
                    this.key = key;
                } else {
                    this.key = "detail";

                }
                this.$(".btn-detail").each(function () {
                    $(this).removeClass("btn-selected");
                });

			this.$("#div-case-" + this.key).show();
			this.$("#btn-case-" + this.key).addClass("btn-selected");
            console.log("button"+ this.$("#btn-case-" + this.key).addClass("btn-selected"));
		},
		
		/*Quantity keypad Functions*/
		
		showNumericKeypad:function(event, callback){
			if(callback!=undefined)
				this.callback = callback;
				this.target = $(event.currentTarget);
				var that=this;
			 	$('#overlay-back').fadeIn(100,function(){
            		that.$("#div-show-num-keypad").html(that.template_numberkeypad());
            		var attrval = $(event.currentTarget).attr("value");
            		if(attrval == "quantity"){
						$("#div-keypad-title").html(lblNm("quantity")+"</div>");
						$("#div-keypad-close").show();
						$("#div-num-decimal").hide();
						$(".div-num-symbol").css("width","71px").css("height","17px");
					}
         		});
			
			
		},
		
		getKeyValues:function(event){
			
			var targetid = event.currentTarget.id;
			var targettext = $("#"+targetid).text();
			if ( targetid == "div-num-decimal" && $("#div-qty-entry-text").text() == "") {
				targettext = "0.";
			}
			if( $("#div-qty-entry-text").text() != "" ) {
				if( targetid == "div-num-decimal" && $("#div-qty-entry-text").text().indexOf(".") >= 0 ) {
					targettext = "";
				}
			}
			$("#div-qty-entry-text").append(targettext);
		},
		
		deleteKeys:function(event){
			event.stopPropagation();
			var number_value = $("#div-qty-entry-text").text();
			var str = number_value.substring(0, number_value.length-1);
			$("#div-qty-entry-text").text(str);
			
		},

		displayQuantity:function(event){
			var qty_val;
			
			if(($("#div-qty-entry-text").text()) == ""){
				showMsgView("052");
			}
			if(($("#div-qty-entry-text").text()) != "" && ($("#div-qty-entry-text").text()) == "0"){
				showMsgView("053");
			}
			if($("#div-qty-entry-text").text() != "" && $("#div-qty-entry-text").text() != "0"){
//				showMsgView("011");
			}
			if($("#div-qty-entry-text").text() != "")
				qty_val = $("#div-qty-entry-text").text();
			
			if(this.callback!=undefined)
				this.callback(qty_val, this.target);
			$("#div-num-keypad").hide();
			$('#overlay-back').fadeOut(100);
		},

		closeKeypad:function(event){
			$('#overlay-back').fadeOut(100);
			$("#div-num-keypad").hide();
		},
		
		openLink: function() {
            //to show module access based links
              _.each($(".in-app"),function(sdata){
                    _.each(localStorage.getItem("moduleaccess").split(','),function(data){
                            if($(sdata).attr("href").replace("#","") == data.replace("btn-",""))
                                $(sdata).addClass("hide");
                        });
                     
                });
			$("#btn-case-link").each(function(){	
	            	$(this).toggleClass("show-app");
			})
				$(".btn-app-link").toggle("fast");
		},

		// Close the view and unbind all the events
		close: function(e) {
			this.unbind();
			this.remove();
		}
		
	});	

	return InvMainView;
});
