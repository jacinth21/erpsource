/**********************************************************************************
 * File:        GmSetInvReportView.js
 * Description: 
 * Version:     1.0
 * Author:     asivaraj
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification','datasync','saleitemmodel', 'saleitemcollection','searchview','jqueryui','dropdownview'
        ], 
	function ($, _, Backbone, Handlebars, Global, Device, Notification,DataSync,SaleItem, SaleItems,SearchView,JqueryUI,DropDownView) {

	'use strict';
	var arrData = new Array(), currBottomLength = 0, currTopLength = 0;
	var SetInvReportView = Backbone.View.extend({
		
		events : {
			"click .div-drilldown-header" : "toggleListDetail",
			"click #div-inv-set-report-load" : "loadSetReport",
			"keyup #div-inv-set-report-sysnm-list input" : "clearSystem",
			
		},

	template: fnGetTemplate(URL_Inv_Template, "GmInvSetReport"),
	template_list: fnGetTemplate(URL_Inv_Template, "GmInvSetReportList"),
	tplSpinner: fnGetTemplate(URL_Common_Template, "GmProgressSpinner"),
	
		
		initialize: function(options) {
			this.parent = options.parent;
			this.distid = "";
			this.type = "";
			this.system = "";
			this.setid = "";
			this.options = options;
			this.render();
			window.invSystem = "";
//			showMsgView("011");
		},

		// Render the initial contents when the View is instantiated
		render: function () {
			this.$el.html(this.template());
			this.loadSystemInfo();
			this.loadSetInfo();
			this.loadFieldSalesInfo();
			this.loadTypeInfo();
			// this.processInputData();
			this.$("#div-saleitemlist").html("<div id = 'div-inv-report-no-data-message'>"+lblNm("msg_select_filter_view")+"</div>");
			return this;
			
		},
		loadSystemInfo :function(){
			var that = this;
			var renderOptions = {
					"el":this.$('#div-inv-set-report-sysnm-list'),
					"ID" : "txt-report-system",
            		"Name" : "txt-search-name",
            		"Class" : "txt-keyword-account",
            		"minChar" : "3",
            		"autoSearch": true,
            		"placeholder": lblNm("msg_character_search"),
            		"fnName": fnGetSystemInfo,
            		"usage": 1,
            		"template" : 'GmSearchResult',
					"template_URL" : URL_Common_Template,
					"callback": function(rowValue) {that.getSystemFromSearchView(rowValue);}
            };
			var searchview = new SearchView(renderOptions);
			
		},
		loadFieldSalesInfo :function(){
				var that = this;
				if(!this.arrFieldSales) {
					var input = {'token' : localStorage.getItem('token'),'passname':'1'};
					fnGetServerData("salesfilter/morefilter",undefined,input,function (data) {
						console.log(data);
						if(data.listGmFieldSalesListVO.length != undefined)
							data.listGmFieldSalesListVO = _.where(data.listGmFieldSalesListVO,{"companyid" :"100800"})
						that.fieldsalesData = that.processData(data.listGmFieldSalesListVO,"fieldsales");
						if(that.fsDropDownView)
							that.fsDropDownView.close();
					if(that.fieldsalesData.length > 0)
						that.fieldsalesData.push({"ID":"","Name":"Choose One"});
					that.fsDropDownView = new DropDownView({
						el : that.$("#div-inv-set-report-fs-dropdown"),
						data : that.fieldsalesData,
						DefaultID : ""
					});
					that.distid = "";
					});	
				}
				else {
					if(that.fsDropDownView)
						that.fsDropDownView.close();
				if(that.arrFieldSales.length > 0)
					that.arrFieldSales.push({"ID":"","Name":"Choose One"});
				that.fsDropDownView = new DropDownView({
					el : that.$("#div-inv-set-report-fs-dropdown"),
					data : that.arrFieldSales,
					DefaultID : ""
				});
				that.distid = "";
				}
						
				this.$("#div-inv-set-report-fs-dropdown").bind('onchange', function (e, selectedId,SelectedName) {
					that.distid = selectedId.toString();
					$("#txt-report-fieldsales").val(SelectedName);
				});			

		},
		//process the given data to form proper ID and Name
		processData: function(data,key) {
			var strData = JSON.stringify(data);
			var finalData = new Array();
			var processedData = JSON.parse(strData,function (k, v) {
				if (k === (key+"id")) 
			        this.ID = v;
			    else if (k === (key+"nm"))
			        this.Name = v;
			    else
			        return v;
			});

			if(processedData.length==undefined) {
				finalData.push(processedData);
				return finalData; 
			}
			else
				return processedData;
		},
		loadSetInfo:function(){
			var that = this;
			var renderOptions = {
					"el":this.$('#div-inv-set-report-setnm-list'),
					"ID" : "txt-report-setname",
            		"Name" : "txt-search-name",
            		"Class" : "txt-keyword-account",
            		"minChar" : "3",
            		"autoSearch": true,
            		"placeholder": lblNm("msg_character_search"),
            		"fnName": fnGetInvSetsInfo,
            		"usage": 1,
            		"template" : 'GmSearchResult',
					"template_URL" : URL_Common_Template,
					"callback": function(rowValue) {that.getSetFromSearchView(rowValue);}
            };
			var searchview = new SearchView(renderOptions);
			
		},
		loadTypeInfo:function(){
			var that = this;
			var InvType = {"arrInvType" : [{"ID":"","Name":"All Status"},{"ID":"4070","Name":"Consignment"}
	          ,{"ID":"4074","Name":"Loaner"}]};
				this.reqDropDownView = new DropDownView({
					el : this.$("#div-inv-set-report-type-dropdown"),
					data : InvType.arrInvType,
					DefaultID : ""
				});
				that.type= "";
				this.$("#div-inv-set-report-type-dropdown").bind('onchange', function (e, selectedId,SelectedName) {
					that.type = selectedId.toString();
				});
			
		},
		getSystemFromSearchView: function(rowValue){
			this.system = rowValue.ID.toString();
			window.invSystem = this.system;
			$("#txt-report-system").val(rowValue.Name);
		},
		clearSystem : function(){
			window.invSystem = "";
			
		},
		getSetFromSearchView: function(rowValue){
			this.setid = rowValue.ID.toString();
			$("#txt-report-setname").val(rowValue.Name);
		},
		loadSetReport :function(){
			console.log(this.system);
			if($("#txt-report-setname").val() == "")
				this.setid = "";
			if($("#txt-report-system").val() == "")
				this.system = "";
			
			this.processInputData();		
		},
		toggleListDetail: function(event) {
            var elem = event.currentTarget;
            var parent = $(elem).parent();
            $(".div-drilldown-header-opened").removeClass("div-drilldown-header-opened");
            $(".div-drilldown-list").slideUp("fast");
            if($(elem).find("i").hasClass("fa-caret-right")) {
                $(".fa-caret-down").removeClass("fa-caret-down").addClass("fa-caret-right");
                $(elem).find("i").addClass("fa-caret-down").removeClass("fa-caret-right");
                $(parent).find(".div-drilldown-list").slideDown("fast");
                $(elem).addClass("div-drilldown-header-opened");
                this.$("#div-inv-set-report-list").animate({
                    scrollTop: this.$("#div-inv-set-report-list").scrollTop() + $(parent).position().top - 160
                }, "fast");
            }
            else {
                $(elem).find("i").addClass("fa-caret-right").removeClass("fa-caret-down");
                $(parent).find(".div-drilldown-list").slideUp("fast");
                $(elem).removeClass("div-drilldown-header-opened");
            }
	    },
		processInputData: function() {

			var that = this;
			console.log(this.system);
			this.serverInput = {
					"token": localStorage.getItem('token'), 
					"companyid": (this.morefilterCompanyid)?this.morefilterCompanyid:localStorage.getItem("userCompany"),
					"divids": (this.morefilterDivisionid)?this.morefilterDivisionid:localStorage.getItem("userDivision"),
					"zoneids": (this.morefilterZoneid)?this.morefilterZoneid:"",
					"regionids": (this.morefilterRegionid)?this.morefilterRegionid:"",
					"fsids": (this.morefilterFieldsalesid)?this.morefilterFieldsalesid:"",
					"setid": this.setid,
					"system": this.system,
					"distid": this.distid,
					"type": this.type	
				};
			console.log(this.serverInput);
			this.$(".div-inv-page-loader").show();
			this.$(".div-inv-page-loader").html(this.tplSpinner({"message":lblNm("msg_data_fetching")}));
			this.$("#div-saleitemlist").html("");
			fnGetWebServiceData("setinvrpt/report", "listGmSetInvRptVO", this.serverInput, function(status,data){
				if(status){
					that.viewData = new Array();
						if(data.length == undefined)
							that.viewData.push(data)
							else
							 that.viewData = data;
						that.processOutputData(that.viewData.slice(0,50),1);						
				}
				else
				{
					that.$(".div-inv-page-loader").show();
					that.$(".div-inv-page-loader").html(lblNm("msg_no_data_display"));	
				}	
				
			});
		},


		processOutputData: function(data,doRender) {
					var strData = JSON.stringify(data);
						arrData = $.parseJSON(strData);
					this.collection = new SaleItems(arrData);	
					this.currCollection = this.collection;
					if(doRender) {
						this.populateList();
						this.processOutputData(this.viewData,0);
					}
		},
		populateList: function() {
			var that = this;
			console.log(this.currCollection.toJSON().slice(0,50));
			this.$("#div-saleitemlist").html(this.template_list(this.currCollection.toJSON().slice(0,50)));
			console.log(this.$("#div-saleitemlist"));
			currBottomLength = 50;
			currTopLength = 0;
			$("#div-saleitemlist").on("scroll",function() {
				var scrollPercentage = 100 * this.scrollTop / (this.scrollHeight-this.clientHeight);
				if((scrollPercentage>98) && (that.currCollection.toJSON().length > currBottomLength)) {
					that.initializeUnlimitedScroll(1);
				}
				else if((scrollPercentage<10) && (currTopLength > 0)) {
					that.initializeUnlimitedScroll(0);
				}
			});
			this.$("#div-inv-set-report-list").show();
			this.$(".div-inv-page-loader").hide();

		},

		initializeUnlimitedScroll: function(scrollType) {
			var elem = $(".div-drilldown").slice(-1);
			if(scrollType) {
				if(currBottomLength+10<=this.currCollection.toJSON().length) {
					this.$("#div-saleitemlist").append(this.template_list(this.currCollection.toJSON().slice(currBottomLength,currBottomLength+10)));
					this.$(".div-inv-page-loader").hide();
					currBottomLength = currBottomLength+10;
				}
				else {
					this.$("#div-saleitemlist").append(this.template_list(this.currCollection.toJSON().slice(currBottomLength,this.currCollection.toJSON().length)));
					currBottomLength = this.currCollection.toJSON().length;
				}
				if(currBottomLength>200) {
					this.$("#div-saleitemlist").find(".div-drilldown").slice(0,10).remove();
					currTopLength = currTopLength+10;
				}
			}
			else {
				if(currTopLength-10>=0) {
					this.$("#div-saleitemlist").prepend(this.template_list(this.currCollection.toJSON().slice(currTopLength-10,currTopLength)));
					currTopLength = currTopLength-10;
						
					this.$("#div-saleitemlist").find(".div-drilldown").slice(-10).remove();
					currBottomLength = currBottomLength-10;

				}
			}
			this.$("#div-saleitemlist").scrollTo(elem);
		},
		
		moreFilterModified: function() {
							
			var savedZone = (this.morefilterZoneid)?this.morefilterZoneid:"", savedRegion = (this.morefilterRegionid)?this.morefilterRegionid:"", savedFieldSales = (this.morefilterFieldsalesid)?this.morefilterFieldsalesid:"";
			var savedCompany = (this.morefilterCompanyid)?this.morefilterCompanyid:"", savedDivision = (this.morefilterDivisionid)?this.morefilterDivisionid:"";
			var userCompany = localStorage.getItem("userCompany"), userDivision = localStorage.getItem("userDivision");
			
			
			if((savedZone!="") || (savedRegion!="") || (savedFieldSales!="")) {	
				$("#btn-inv-more-filter").addClass("bg-red");
				
			}
			else if((savedCompany!="") || (savedDivision!="")) {
				if((savedCompany!=userCompany) || (savedDivision!=userDivision)) {
					$("#btn-inv-more-filter").addClass("bg-red");
				}
				else
					$("#btn-inv-more-filter").removeClass("bg-red");
			}
			else
				$("#btn-inv-more-filter").removeClass("bg-red");
			
			this.unbind();
			this.initialize(this.options);
		},
		
		close: function() {
			this.undelegateEvents();
			this.unbind();
		}
		
	});	

	return SetInvReportView;
});
