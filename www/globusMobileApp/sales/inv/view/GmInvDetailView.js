/**********************************************************************************
 * File:        GmInvDetailView.js
 * Description: 
 * Version:     1.0
 * Author:     	asivaraj
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification','dropdownview','invsurgeryinfoview','invsetsusedview','invshiptoinfoview','invcommentsview', 'invsummaryview', 'invpartsusedview' , 'loaderview'
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification,DropDownView, InvSurgeryInfoView,InvSetsUsedView,InvShipToInfoView,InvCommentsView, InvSummaryView, InvPartsUsedView, LoaderView) {

	'use strict';
	var tabid;
	var InvDetailView = Backbone.View.extend({

		events : {
			"click .div-inv-detail-label" : "toggleContent",
			"click .div-inv-keypad-values" : "showNumberKeypad",
			"click .div-keys" : "getKeyValues",
			"click .div-num-symbol" : "deleteKeyValues",
			"click #div-keypad-close" : "closeKeypad",
			"click #div-btn-done" : "displayQuantity",
			"click .div-inv-detail-button" : "changeLabel",
			"click #btn-inv-delete" : "deleteInv",
			"click #btn-inv-reset" : "cancelInv",
			"click #btn-inv-request" : "sendRequestInfo"
			
		},

		/* Load the templates */ 
		template: fnGetTemplate(URL_Inv_Template, "GmInvDetail"),
		template_numberkeypad: fnGetTemplate(URL_Inv_Template, "GmInvNumberKeypad"),
		
		initialize: function(options) {
			var that = this;
			$("body").removeClass();
			$("#btn-home").show();
			$("#btn-back").show();
			this.options = options;
			this.partsChangeMode = false;
			this.requesttype = "";
            GM.Global.ShipPartsUsedFl = "";  //Used Flag to check condition in invpartusedview for Parts loaded from marketing collateral module(PC-2428)
            GM.Global.clearCollateralFl = ""; //Used Flag to check condition in this view for sendrequestinfo,cancelInv and deleteInv operations (PC-2428)
            this.initialiseJSON();
            this.render(options);
		},
		initialiseJSON:function(){
			this.JSONSets = [];
			this.JSONParts = {};
			this.JSONData = {};
			this.SetsJSONData = {};
			this.reqid = "";
			this.distnm = "";
			this.reqtypenm = "";
			this.JSONData.caseid = "";
			this.JSONData.caseinfoid = "";
			this.JSONData.surdt = "";
			this.JSONData.reqdt = "";
			this.JSONData.did = ""; 
                this.JSONData.dnm = "";
			this.JSONData.repid = "";
			this.JSONData.asrepid = "";
			this.JSONData.acctid = "";
			this.JSONData.voidfl = "";
			this.JSONData.prnotes = "";
			this.JSONData.type = "";
			this.JSONData.status = "";
			this.JSONData.prtcaid = "";
			this.JSONData.skipvalidationfl = "";
			this.JSONData.arrGmCaseAttributeVO = "";
			this.JSONData.arrGmCaseSetInfoVO = "";
			this.JSONData.arrGmPartDetailVO = new Array();
			this.JSONData.gmTxnShipInfoVO = new Array();
			
		},

		// Render the initial contents when the View is instantiated
		render: function (options) {
			var that = this;
//			showMsgView("011");
			this.$el.html(this.template());
			this.$("#div-inv-comments").hide();

			this.$("#btn-inv-reset").prop("disabled",true);
			this.$("#btn-inv-delete").prop("disabled",true);
             var reqType = [];
           //show drop down based on the below flag

            if (GM.Global.AICFL == "Y") {
                reqType = {
                    "arrreqType": [{ "ID": "1006505", "Name": "Product Loaner" } , { "ID": "107286", "Name": "Account Item Consignment" }]
                };
            } else {
                reqType = {
                    "arrreqType": [{ "ID": "1006505", "Name": "Product Loaner" } , { "ID": "1006506", "Name": "Set Consignment" } , { "ID": "1006507", "Name": "Item Consignment" }]
                };
            }
            
            //To Set as Item consignment default dropdown value,when come from marketing collateral module(PC-2428)
            if (GM.Global.DocShipFl == "Y") {
                var reqtypeId = "1006507";
                if (GM.Global.AICFL == "Y")
                    var reqtypeId = "107286";
            }
            else{
                var reqtypeId = "1006505";
            }
            
            that.requesttypeid = reqtypeId;
            that.reqDropDownView = new DropDownView({
                el: that.$("#div-inv-request-type-dropdown"),
                data: reqType.arrreqType,
                DefaultID: reqtypeId //Product Loaner
            });
            that.requesttype = reqtypeId;
            

			this.$("#div-inv-request-type-dropdown").bind('onchange', function (e, selectedId,SelectedName) {
				that.requesttype = selectedId ;	
				that.partsChangeMode = false;
				that.requestTypeChanged(function(){
					
					if(that.requesttype != 1006505){
						$("#div-inv-surgery-info").html(lblNm("request_info"));
						if(that.requesttype == 1006506){
							$("#div-inv-sets-used").html(lblNm("set_detail"));
							$("#div-inv-comments").show();
						} else{
							$("#div-inv-sets-used").html(lblNm("part_details"));
							$("#div-inv-comments").show();
						}
					} else{
						$("#div-inv-surgery-info").html(lblNm("surgery_info"));
						$("#div-inv-sets-used").html(lblNm("loaner_details"));
						$("#div-inv-comments").hide();
					}
				
					that.toggleContent();		
					
				});
				
			});
            
            //Load Item consignment view,if it come from marketing collateral module(PC-2428)
            if (GM.Global.DocShipFl == "Y") {
                that.setItemConsignDefaults();
            }
            else{
                that.toggleContent();
            }
            
			
				
            
			
			return this;
		},
        
        //Load Item consignment by default when come from marketing collateral module(PC-2428)
        setItemConsignDefaults: function () {
            var that = this;
            //Used Flag to check condition in this view for load part used view when come from marketing collateral module
            if (GM.Global.DocShipFl == "Y") {
                that.JSONData.arrGmCaseAttributeVO=[];
                that.requesttype = "1006507";
                var today = new Date(); 
                var plannedShipDate = that.fnGetFormatDate(today);
                var fmtPlanDate = formattedDate(plannedShipDate, localStorage.getItem("cmpdfmt"));
                //set required date as today date
                that.JSONData.reqdt = fmtPlanDate;
                fnGetDistByAccess(function (data) {
                    if (data.length == 1) {
                        $("#div-inv-surgery-fieldsalesinfoname").html(data[0].Name);
                        $("#div-inv-surgery-fieldsalesinfoid").html(data[0].ID);
                        that.JSONData.distid =  data[0].ID;
                        that.JSONData.did =  data[0].ID;
                        that.JSONData.dnm =  data[0].Name;
                    } else if (data.length == 0) {
                        var partyid = localStorage.getItem("partyID");
                        fnGetUserDOID(partyid, function (repList) {
                            that.repid = repList.C703_SALES_REP_ID;
                            that.repname = repList.C703_SALES_REP_NAME;
                            fnGetDistributorByRep(that.repid, function (data) {
                                $("#div-inv-surgery-fieldsalesinfoname").html(data[0].Name);
                                $("#div-inv-surgery-fieldsalesinfoid").html(data[0].ID);
                                that.JSONData.distid =  data[0].ID;
                                that.JSONData.did =  data[0].ID;
                                that.JSONData.dnm =  data[0].Name;
                            });
                        });
                    } else {
                        that.getDistributorName(data, "");
                    }
                    that.toggleContent();
                });
            }
        },
        
        //date format script for set required date(PC-2428)
        fnGetFormatDate: function (selectedDate) {
            var fdate = new Date(selectedDate);
            var smm = ("0" + (fdate.getMonth() + 1)).slice(-2),
                sdd = ("0" + fdate.getDate()).slice(-2),
                syy = fdate.getFullYear();
            syy = syy.toString();
            var formatDate = smm + "/" + sdd + "/" + syy;
            return formatDate;
        },
		
		requestTypeChanged: function(callback){
						
			if(this.invsurgeryinfoview) {
				this.invsurgeryinfoview.close();
				this.invsurgeryinfoview = null;
			}
							
			if(this.invsetsusedview){
				this.invsetsusedview.close();
				this.invsetsusedview = null;
			}
							
			if(this.invpartsusedview){
				this.invpartsusedview.close();
				this.invpartsusedview = null;
			}
							
			if(this.invshiptoinfoview){
				this.invshiptoinfoview.close();
				this.invshiptoinfoview = null;
			}
							
			if(this.invsummaryview){
				this.invsummaryview.close();
				this.invsummaryview = null;
			}
							
			this.initialiseJSON();
			callback();	
		},

		// Toggles the view
		display: function(key) {
			
			if(key.currentTarget==undefined) {
				this.key = key;
			}else{
				this.key = "surgery-info";
			}
			this.$(".div-inv-detail-label").each(function() {
				$(this).removeClass("selected-tab");
			});
			
			this.$("#div-inv-" + this.key).addClass("selected-tab");
			this.$(".ul-inv-detail-info").hide();
			this.$("#ul-inv-detail-" + this.key).show();
			this.$(".div-inv-detail-button").show();
			if(this.key == "surgery-info") {
				this.$("#btn-backward").hide();
			}
			if(this.key == "summary") {
					this.$("#btn-forward").hide();
			}
		},
		
		/*Quantity keypad Functions*/

		showNumberKeypad:function(event, callback){
		
			if(callback!=undefined)
				this.callback = callback;
				this.target = $(event.currentTarget);
				var that=this;
				$('#overlay-back').fadeIn(100,function(){
	            	that.$("#div-show-num-keypad").html(that.template_numberkeypad());
	            	var attrval = $(event.currentTarget).attr("value");
	            	if(attrval == "quantity"){
						$("#div-keypad-title").html(lblNm("quantity")+"</div>");
						$("#div-keypad-close").show();
						$("#div-num-decimal").hide();
						$(".div-num-symbol").css("width","71px").css("height","17px");
					}
	         	});
		},

		changeLabel: function (event) {

				var that = this;
				this.$(".div-inv-detail-label").each(function() {
					if($(this).hasClass("selected-tab")) {
						if(event.currentTarget.id.indexOf("backward") >= 0){
							if(that.requesttype != "1006507" && that.requesttype != "1006506" ) {
								if(this.id == "div-inv-summary") {
									$("#div-inv-ship-info").trigger("click");
									}
								else {
									$(this).prev().trigger("click");
									}
								}
								else{
									$(this).prev().trigger("click");
									}
								}else {
									if(that.requesttype != "1006507" && that.requesttype != "1006506" ) {
										if(this.id == "div-inv-ship-info") {
											that.$("#div-inv-summary").trigger("click");
										}
										else {
											$(this).next().trigger("click");
											return false;
										}
									}
									else {
										$(this).next().trigger("click");
										return false;
									}
								}
							}

				});
				},


		getKeyValues:function(event){
			
			var targetid = event.currentTarget.id;
			var targettext = $("#"+targetid).text();
				
			if ( targetid == "div-num-decimal" && $(".div-inv-qty-box").text() == "") {
				targettext = "0.";
			}
			
			if( $(".div-inv-qty-box").text() != "" ) {
				if( targetid == "div-num-decimal" && $(".div-inv-qty-box").text().indexOf(".") >= 0 ) {
					targettext = "";
				}
			}
			this.$(".div-inv-qty-box").append(targettext);
		},

		deleteKeyValues:function(event){
				event.stopPropagation();
			var number_value = $("#div-qty-entry-text").text();
			var str = number_value.substring(0, number_value.length-1);
			$("#div-qty-entry-text").text(str);
			
		},

		displayQuantity:function(event){
			var qty_val;
			if(($("#div-qty-entry-text").text()) == ""){
				showMsgView("052");
			}
			if(($("#div-qty-entry-text").text()) != "" && ($("#div-qty-entry-text").text()) == "0"){
				showMsgView("053");
			}
			if($("#div-qty-entry-text").text() != "" && $("#div-qty-entry-text").text() != "0"){
//				showMsgView("011");
			}
			if($("#div-qty-entry-text").text() != "")
				qty_val = $("#div-qty-entry-text").text();
			
			if(this.callback!=undefined)
				this.callback(qty_val, this.target);
			$("#div-num-keypad").hide();
			$('#overlay-back').fadeOut(100);
		},

		closeKeypad:function(event){
			$('#overlay-back').fadeOut(100);
			$("#div-num-keypad").hide();
		},

		toggleContent: function (event) {
            var that = this;
            //Load Part used view by default when come from marketing collateral module
            if (GM.Global.DocShipFl == "Y") {
                tabid = "div-inv-sets-used";
                GM.Global.DocShipFl = "";
                GM.Global.ShipPartsUsedFl = "Y";
                GM.Global.clearCollateralFl = "Y";
            }
            else if (!event){
                tabid = "div-inv-surgery-info";
            }
            else{
                tabid = (event.currentTarget.id);
            }
			console.log(JSON.stringify(that.JSONData));
			switch(tabid) {
				case "div-inv-surgery-info":
					if(!this.invsurgeryinfoview)
						this.invsurgeryinfoview = new InvSurgeryInfoView({'el' :this.$('#ul-inv-detail-surgery-info'), 'parent' : this,'requesttype' : this.requesttype});
						this.display("surgery-info");
				break;	

				case "div-inv-sets-used":
				 	
					if( (this.requesttype == "1006505" || this.requesttype == "107286") && (this.JSONData.acctid == "")){ //107286--Account Item Consignment
                        fnShowStatus(lblNm("msg_select_account_info"));
				  		}else if((this.requesttype == "1006505") && (this.JSONData.surdt == "")){
				  			fnShowStatus(lblNm("msg_select_surgery_info"));
				 		}else if((this.requesttype != "1006505") && (this.JSONData.reqdt == "")){
				 			fnShowStatus(lblNm("msg_select_date_info"));
				 		}else if (!(this.requesttype == "1006505" || this.requesttype == "107286") && ((this.JSONData.did == "" || this.JSONData.did == undefined) && (this.JSONData.distid== "" ||this.JSONData.distid == undefined )) ) {
                            console.log(this.JSONData.dnm);
                            fnShowStatus(lblNm("msg_seletct_field_info"));
				 		} else {
				 		if(this.requesttype == "1006507" || this.requesttype == "107286"){
							
								if(this.invpartsusedview){
										this.invpartsusedview.close();
								}
								this.invpartsusedview = new InvPartsUsedView({'el' :this.$('#ul-inv-detail-sets-used'), 'parent' : this,'requesttype' : this.requesttype});
						}else{
							
								if(this.invsetsusedview){
										this.invsetsusedview.close();
								}
							this.invsetsusedview = new InvSetsUsedView({'el' :this.$('#ul-inv-detail-sets-used'), 'parent' : this,'requesttype' : this.requesttype});
						}
							this.display("sets-used");
				 	}
				break;
						
				case "div-inv-ship-info":
				 
				 	if((this.requesttype == "1006507" || this.requesttype == "107286") && (this.JSONSets.length==0)){
					  		fnShowStatus(lblNm("msg_choose_parts_info"));
				 	} else if(this.JSONSets.length==0){
					 	fnShowStatus(lblNm("msg_choose_shipping_info"));
					} else{
						this.partsChangeMode = true;
					 	if(this.invshiptoinfoview)
						 	this.invshiptoinfoview.close();
						this.invshiptoinfoview = new InvShipToInfoView({'el' :this.$('#ul-inv-detail-ship-info'), 'parent' : this,'requesttype' : this.requesttype});
						this.display("ship-info");		
					 }		
					break;

				case "div-inv-comments":
			
					if((this.requesttype == 1006507 || this.requesttype == 1006506 || this.requesttype == 107286) && (!this.JSONData.gmTxnShipInfoVO.length)) {
						fnShowStatus(lblNm("msg_select_data_comment"));
					}
                    else{
                        if(this.requesttype == 1006507){
                             this.validateTissueShipAddr(this.JSONData, function(tissueShipValid, partsetID){
                            if (tissueShipValid == 'Y') {
                                if (that.invcommentsview)
                                    that.invcommentsview.close();

                                that.invcommentsview = new InvCommentsView({
                                    'el': that.$('#ul-inv-detail-comments'), 'parent': that, 'requesttype': that.requesttype
                                });
                                that.display("comments");
                            } else {
                                fnShowStatus(lblNm("msg_order_tissue_part_validation") + partsetID );
                            }
                            });
                        } else {
                            if (this.invcommentsview)
                                    this.invcommentsview.close();

                                this.invcommentsview = new InvCommentsView({
                                    'el': this.$('#ul-inv-detail-comments'), 'parent': this, 'requesttype': this.requesttype
                                });
                                this.display("comments");
                        }

                        }
						
					break;

				case "div-inv-summary":

					if((this.requesttype == "1006507" || this.requesttype == "107286") && (this.JSONSets.length==0)){
					  		fnShowStatus(lblNm("msg_choose_parts_info"));
					}else if(this.JSONSets.length==0){
						 			fnShowStatus(lblNm("msg_choose_sets_parts_summary"));
					 }
                    else{
                        if(this.requesttype == 1006507){
                             this.validateTissueShipAddr(this.JSONData, function(tissueShipValid, partsetID){
                            if (tissueShipValid == 'Y') {
                                if (that.invsummaryview)
                                    that.invsummaryview.close();

                                that.invsummaryview = new InvSummaryView({'el' :that.$('#ul-inv-detail-summary'), 'parent' : that,'requesttype' : that.requesttype});
								that.display("summary");
                            } else {
                                fnShowStatus(partsetID  + lblNm("msg_select_valid_tissue_ship_addr"));
                            }
                            });
                        }
                        else{
							 if(this.invsummaryview)
								this.invsummaryview.close();
								this.invsummaryview = new InvSummaryView({'el' :this.$('#ul-inv-detail-summary'), 'parent' : this,'requesttype' : this.requesttype});
								this.display("summary");	
					}
                    }
				break;
				
			}

		},


		validate: function() {
		  
		  if((this.requesttype != 107286) && this.JSONData.did=="") {
				fnShowStatus(lblNm("msg_provide_surgery_information"));
			    $("#btn-inv-request").prop('disabled',false);
				return false;
			} else if(this.JSONSets.length==0) {
				fnShowStatus(lblNm("msg_provide_sets_parts"));
				return false;
			}else if((!this.JSONData.gmTxnShipInfoVO.length)) {
				fnShowStatus(lblNm("msg_provide_shipping_information"));
				return false;
			}else if((this.requesttype == 1006507 || this.requesttype == 107286) && (this.JSONData.prnotes.length=="")) {		//Happens only for Item Request
				showNativeAlert(lblNm("msg_enter_comments"),lblNm("msg_comment_required"));
				return false;
			}
			else if((this.requesttype == 1006506) && (this.JSONData.prnotes.length=="")) {		//Happens only for Set Consignment
				fnShowStatus(lblNm("msg_provide_consignment_support"));
				return false;
			}
			else{
				return true;
			}
		},


		sendRequestInfo: function(){
		
		   var that = this;	
		   if((navigator.onLine) && (!intOfflineMode)) {
			$("#btn-inv-request").prop('disabled',true);
			   if (this.validate()) {
			       that.validateSetTissueShipAddr(function (validfl) {            
			           if (validfl == 'Y') {
						   that.loaderview = new LoaderView({text : lblNm("msg_submitting")});
			               $("#btn-inv-request").text(lblNm("msg_submitting"));
			               sendRequest((JSON.stringify(that.JSONData)), function (status, data) {
			                   if (status) {
			                       showNativeAlert(lblNm("case_id") + " - " + data.caseid + " " + lblNm("msg_created_successfully"), lblNm("msg_success"), lblNm("msg_ok"), function () {
									   $("#btn-inv-request").prop('disabled',false);
			                           that.$("#btn-inv-request").text(lblNm("submit"));
			                           that.invsurgeryinfoview = null;
			                           that.invsetsusedview = null;
			                           that.invpartsusedview = null;
			                           that.invshiptoinfoview = null;
			                           that.invcommentsview = null;
			                           that.invsummaryview = null;
			                           that.options = new Array();
			                           that.initialize(that.options);
			                           that.toggleContent();
			                       });
								   that.loaderview.close();

                                   if (GM.Global.clearCollateralFl == "Y") {
                                       GM.Global.clearCollateralFl = "";
                                       fnDeleteShareList(function () {
                                           window.location.href = "#collateral";
                                       });
                                   }
			                   } else {
								   that.loaderview.close();
			                       showNativeAlert(lblNm("msg_server_down_try_after"), lblNm("msg_error_message"));
			                       $("#btn-inv-request").text(lblNm("submit"));
								   $("#btn-inv-request").prop('disabled',false);
			                   }

			               });
			           }
					    else{
							$("#btn-inv-request").prop('disabled',false);
						}
			       });
			   }
            
		    }else{

		    	if(intOfflineMode) 
							showNativeAlert(lblNm("msg_request_will_create"),lblNm("msg_offline_mode_restriction"));
		    	else
				 $("#btn-inv-request").prop('disabled',true);
		   		 this.saveRequest();
		   		 showNativeAlert(lblNm("msg_request_created_automatically"),lblNm("msg_alert_message"));	
		   		 that.invsurgeryinfoview = null;
				 that.invsetsusedview = null;
				 that.invpartsusedview = null;
				 that.invshiptoinfoview = null;
				 that.invsummaryview = null;
                 that.invcommentsview = null;
				 that.options = new Array();
				 that.initialize(that.options);
				 that.toggleContent();
		    }

		},
		
		saveRequest: function() {

			var that = this;
			var comments = $("#div-inv-surgery-info-additional input").val();
      			
            fnSaveRequest(JSON.stringify(this.JSONData),function(status){
                if(status){
                    if (GM.Global.clearCollateralFl == "Y") {
                        GM.Global.clearCollateralFl = "";
                        fnDeleteShareList(function () {
                            window.location.href = "#collateral";
                        });
                    }
                }
            });
		},

		deleteInv: function() {
			var that = this;
            if(GM.Global.clearCollateralFl == "Y"){
                GM.Global.clearCollateralFl = "";
                fnDeleteShareList(function(){
                    window.location.href = "#collateral";
                });
            }
            else{
//			showNativeConfirm(lblNm("msg_delete_request_information"),lblNm("delete"),[lblNm("msg_yes"),lblNm("msg_no")], function(index) {
				if(index==1) {
					fnDeleteDO(that.JSONData.orderid)	
						that.invsurgeryinfoview = null;
						that.invsetsusedview = null;
						that.invpartsusedview = null;
						that.invshiptoinfoview = null;
						that.invsummaryview = null;
                        that.invcommentsview = null;
						that.options = new Array();
						that.initialize(that.options);
                        that.render(that.options);
				}
            }
				
//			});
            
		},

		cancelInv: function() {
            if(GM.Global.clearCollateralFl == "Y"){
                GM.Global.clearCollateralFl = "";
                window.location.href = "#collateral";
            }
            else{
			switch(tabid) {
				
				case "div-inv-surgery-info":
				var that = this;
				
				showNativeConfirm(lblNm("msg_cancelling_want_proceed"),lblNm("msg_confirm"),[lblNm("msg_yes"),lblNm("msg_no")], function(index) {
					if(index==1) {
						that.invsurgeryinfoview = null;
						that.invsetsusedview = null;
						that.invpartsusedview = null;
						that.invshiptoinfoview = null;
						that.invsummaryview = null;
                        that.invcommentsview = null;
						that.options = new Array();
						that.initialize(that.options);
                        that.render(that.options);
					}
						
				});
				break;

				case "div-inv-sets-used":
					this.invsetsusedview = null;
					this.partsChangeMode = false;
					this.JSONSets = [];
					this.SetsJSONData = {};
					this.JSONData.arrGmCaseAttributeVO = new Array();
					this.JSONData.arrGmCaseSetInfoVO = new Array();
					this.JSONData.arrGmPartDetailVO = new Array();
					this.JSONData.gmTxnShipInfoVO = new Array();
					
					
				break;
				case "div-inv-ship-info":
					for(var i=0;i<this.JSONSets.length;i++) {
						delete this.JSONSets[i]['shipTo'];
						delete this.JSONSets[i]['AddressData'];
					}
					this.JSONData.gmTxnShipInfoVO = new Array();
				break;
				case "div-inv-summary":
					this.invsummaryview.render();
				break;
                case "div-inv-comments":
                    this.JSONData.prnotes = "";
                break;
				
			}
			
			 $(".selected-tab").trigger("click");
            }

		},
		// Close the view and unbind all the events
		close: function() {

		        this.unbind();
		        this.undelegateEvents();
		    },
        
validateTissueShipAddr: function (data, callback) {
    var dtLength = data.arrGmCaseSetInfoVO.length;
    var tissueShipValid = 'N';
    var setparttissueFl = "";
    var setpartnum ="";
    var datasets ="";

    var partsetID = ''; // to assign the not valid ID
    for (var i = 0; i < dtLength; i++) {
         datasets = data.arrGmCaseSetInfoVO[i];
        if (this.requesttype == 1006507) { // Item Consignment                                
            setparttissueFl = datasets.arrGmPartDetailVO[0].tissueFlag;
            setpartnum = datasets.arrGmPartDetailVO[0].pnum;
        }
        // product loaner or set consignment
        else if ((this.requesttype == 1006505) || (this.requesttype == 1006506)) {
            setparttissueFl = datasets.tissueFlag;
            setpartnum = datasets.setid;
        }

    // tissuefl is y and shipto is not hospital
    if (setparttissueFl == 'Y' && datasets.gmTxnShipInfoVO.shipto != '4122') {
        // ship to salesrep and address type is hospital
        if (datasets.gmTxnShipInfoVO.shipto == '4121' && datasets.gmTxnShipInfoVO.addresstype == '26240675') {
            if (partsetID == '')
                tissueShipValid = 'Y';
        } else {
            tissueShipValid = 'N';
            partsetID += setpartnum + ',';
        }
    } 
    else {
        if (partsetID == '')
            tissueShipValid = 'Y';
    }
    }
     partsetID = partsetID.replace(/,(\s+)?$/,'');
    callback(tissueShipValid, partsetID); // return validfl and partset ID
   
},
        
 validateSetTissueShipAddr: function (callback) {
     var that = this;
     var valid = '';
     if ((this.requesttype == 1006505) || (this.requesttype == 1006506)) {
         var tempJSONData = this.JSONData.arrGmCaseSetInfoVO;
         var strSetID = _(tempJSONData).pluck("setid");
         strSetID = strSetID.toString();
         var input = {
             "token": localStorage.getItem("token"),
             "setid": strSetID
         }
        
         fnGetWebServerData("setinvrpt/fetchsettfl", undefined, input, function (data) {
             if (data != null) {
                  _.each(that.JSONData.arrGmCaseSetInfoVO, function (dt) {
             dt.tissueFlag = '';
         });
                 _.each(data, function (wsdata) {
                     _.each(that.JSONData.arrGmCaseSetInfoVO, function (dt) {
                         if (dt.setid == wsdata.setid)
                             dt.tissueFlag = parseNull(wsdata.tissuefl);
                     });
                 });
          
             that.validateTissueShipAddr(that.JSONData, function (tissueShipValid, partsetID) {
                 if (tissueShipValid != 'Y') {
                     fnShowStatus(lblNm("msg_order_tissue_part_validation") + partsetID);
                     valid = 'N';
                 } else {
                     valid = 'Y';
                 }
             });
             callback(valid);
                    }
             else {
         valid = 'Y';
         callback(valid);
     }
         });
     } else {
         valid = 'Y';
         callback(valid);
     }

 }                                 
	});	

	return InvDetailView;
});
