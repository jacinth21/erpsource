/**********************************************************************************
 * File:        GmInvReportView.js
 * Description: 
 * Version:     1.0
 * Author:     asivaraj
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification','datasync','searchview','jqueryui','dropdownview',
        'invfieldreportview','invsetreportview','invmorefilterview'
        ], 
	function ($, _, Backbone, Handlebars, Global, Device, Notification,DataSync,SearchView,JqueryUI,DropDownView,InvFieldReportView,InvSetReportView,MoreFilterView) {

	'use strict';
	
	var InvReportView = Backbone.View.extend({

	template: fnGetTemplate(URL_Inv_Template, "GmInvReport"),
	
		events: {
			"click #btn-inv-more-filter" : "openMoreFilter",
			"click #div-inv-more-filter-container-popup-close-icon" : "closeMoreFilterPopup"
		},
		
		initialize: function(options) {
			this.render(options);
		},

		// Render the initial contents when the View is instantiated
		render: function (options) {
			var that = this;
//			showMsgView("011");
			this.$el.html(this.template());
			var InvType = {"arrInvType" : [{"ID":"1","Name":"Field Inventory"}
			,{"ID":"2","Name":"Set Inventory"}]};
			this.reqDropDownView = new DropDownView({
				el : this.$("#div-report-type-dropdown"),
				data : InvType.arrInvType,
				DefaultID : "1"
			});
			that.openReport(options,1);
			this.$("#div-report-type-dropdown").bind('onchange', function (e, selectedId,SelectedName) {
				that.openReport(options,selectedId);
			});
			
			var savedZone = (localStorage.getItem("zoneid"))?localStorage.getItem("zoneid"):"", savedRegion = (localStorage.getItem("regionid"))?localStorage.getItem("regionid"):"", savedFieldSales = (localStorage.getItem("fieldsalesid"))?localStorage.getItem("fieldsalesid"):"";
			var savedCompany = (localStorage.getItem("companyid"))?localStorage.getItem("companyid"):"", savedDivision = (localStorage.getItem("divisionid"))?localStorage.getItem("divisionid"):"";
			var userCompany = localStorage.getItem("userCompany"), userDivision = localStorage.getItem("userDivision");
			
			if(!((navigator.onLine) && (!intOfflineMode))) {
				this.$(".div-detail").hide();
				this.$("#btn-inv-more-filter").hide();
				this.$("#div-inv-nointernet-connection").show();
				this.$("#div-inv-nointernet-connection").html(" <div> <i class='fa fa-exclamation-triangle fa-lg'></i>"+lblNm("msg_no_internet_connection")+"</div>"+lblNm("msg_connect_wifi_or_cellular_inventory"));
			}
			return this;
		},

		getAccountFromSearchView: function(rowValue){
			console.log(rowValue);
			$("#div-itemlist").hide();
			$("#txt-inv-account").val(rowValue.Name);	
			this.accid = rowValue.ID;
		},
		openReport: function(options,reportType){
			this.strViewType = options[0];
				console.log(reportType);
			if((navigator.onLine) && (!intOfflineMode)) {
				switch(parseInt(reportType)) {
				
					case 1:
						if(this.invfieldreportview)
							this.invfieldreportview.close();
						this.invfieldreportview = new InvFieldReportView({'el' :this.$('#div-inv-report-main-content'), 'parent' : this});
						this.$("#btn-inv-more-filter").removeClass("bg-red");
						this.currentView = this.invfieldreportview;
						this.moreFilterInput = {"token" : localStorage.getItem('token')};
						break;	
					
					case 2:
						if(this.invsetreportview)
							this.invsetreportview.close();
						this.invsetreportview = new InvSetReportView({'el' :this.$('#div-inv-report-main-content'), 'parent' : this});
						this.$("#btn-inv-more-filter").removeClass("bg-red");
						this.currentView = this.invsetreportview;
						this.moreFilterInput = {"token" : localStorage.getItem('token'), "passname" : "1"};
						break;	
					
					default:
					
				}
			}
			else{
				
				this.$(".div-detail").hide();
				this.$("#btn-inv-more-filter").hide();
				this.$("#div-inv-nointernet-connection").show();
				this.$("#div-inv-nointernet-connection").html(" <div> <i class='fa fa-exclamation-triangle fa-lg'></i>"+lblNm("msg_no_internet_connection")+"</div>"+lblNm("msg_connect_wifi_or_cellular_inventory"));
				
			}
			
			
			return this;
		},
		
		openMoreFilter: function() {
			if(this.moreFilterView)
				this.moreFilterView.close();
			this.moreFilterView = new MoreFilterView({'el' : this.$('#div-inv-more-filter-container-popup-content'), 'parent': this.currentView, input: this.moreFilterInput});
			this.$('#div-inv-more-filter-container-popup').show();			
		},
		
		closeMoreFilterPopup: function() {
			this.$('#div-inv-more-filter-container-popup').hide();
		},
		
		close:function()
		{
			this.unbind();
			this.undelegateEvents();
		}
		
	});	

	return InvReportView;
});
