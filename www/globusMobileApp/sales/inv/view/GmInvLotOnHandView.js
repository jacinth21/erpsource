/**********************************************************************************
 * File:        GmInvLotOnHandView.js
 * Description: 
 * Version:     1.0
 * Author:     Vinoth Petchimuthu
 **********************************************************************************/
    define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars', 'jqueryui',

    // Common
    'global',


    // Pre-Compiled Template
    'gmtTemplate', 'gmvSearch', 'commonutil', 'gmcItem', 'gmvItemList'

    //Model

],

        function ($, _, Backbone, Handlebars, JqueryUI,
            Global, GMTTemplate, GMVSearch, Commonutil, GMCItem, GMVItemList) {
    
    'use strict';
        
        
    
    var InvLotOnHandView = Backbone.View.extend({
        
    el: "#div-main",    
        
    template: fnGetTemplate(URL_Inv_Template, "GmLotOnHand"),
    template_Loh_Details: fnGetTemplate(URL_Inv_Template, "GmLotOnHandDetails"),
    template_details: fnGetTemplate(URL_Inv_Template, "GmLothand"),
        
        events: {

              "click .count": "loadLotDetails",
              "click .lot-part-count": "loadLotPartDetails"
            
                
            },
        
        initialize: function(options) {
			this.render(options);
            this.chartArr = [];
        },
            render: function (options) {
			var that = this;

			this.$el.html(this.template());
             if((navigator.onLine) && (!intOfflineMode)) {
                this.loadOnHandQty("lot/lotonhand", this.$("#div-lot-on-hand-overview"), "GmLothand", "GmLotOnHandVO");
                this.load30Days("lot/lotexpbydays", this.$("#div-lot-30-days-overview"), "GmLothand", "GmLotOnHandVO");
                this.load60Days("lot/lotexpbydays", this.$("#div-lot-60-days-overview"), "GmLothand", "GmLotOnHandVO");
                this.load90Days("lot/lotexpbydays", this.$("#div-lot-90-days-overview"), "GmLothand", "GmLotOnHandVO");
                this.loadExpired("lot/expiredlots", this.$("#div-lot-expired-overview"), "GmLothand", "GmLotOnHandVO");
                this.loadExpDaysGraph("lot/expirygraph", this.$("#chart-contain"), "GmLotOnHand", "GmLotOnHandVO");
            }
            else{
                this.$("#div-lot-view").hide();
				this.$("#div-inv-nointernet-connection").show();
				this.$("#div-inv-nointernet-connection").html(" <div> <i class='fa fa-exclamation-triangle fa-lg'></i>"+lblNm("msg_no_internet_connection")+"</div>"+lblNm("msg_server_not_reachable"));
    }
            },

			//load on hand part number and its count
            loadOnHandQty: function (servername, targetname, targettemplate, voname) {
                var that = this;

                fnGetServerData(servername, voname, {}, function (data) {

                    if (data.partInfo != "")
                        data = JSON.parse(data.partInfo);
                    else
                        data = data.partInfo;

                    var onHandCount = 0;

                    _.each(data, function (item) {
                        onHandCount = onHandCount + item.COUNT;
                    });

                    if (data.partInfo != "") {
                        data["widget"] = "onhandqty";
                    }
                    var pnumArry = [];
                    _.each(data, function (datapnum) {
                        pnumArry.push(datapnum.PARTNUM);
                    });
                    var pnumArryStr = pnumArry.toString();
                    $("#id-lot-on-hand").attr("data-parts", pnumArryStr);

                    $("#div-lot-on-hand-overview").html(that.template_details(data));
                    $(".div-dash-widget-header #id-lot-on-hand").text(onHandCount);

                });
            },

			//load expiring in 30 days part number and its count
            load30Days: function (servername, targetname, targettemplate, voname) {
                var that = this;
                var serverinput = {};
                serverinput.expDays = 30;

                fnGetServerData(servername, voname, serverinput, function (data) {

                    if (data.partInfo != "")
                        data = JSON.parse(data.partInfo);
                    else
                        data = data.partInfo;
                    var thirtyDays = 0;
                    _.each(data, function (item) {
                        thirtyDays = thirtyDays + item.COUNT;
                    });
                    if (data.partInfo != "") {
                        data["widget"] = "lotqty30";
                    }
                    var pnumArry = [];
                    _.each(data, function (datapnum) {
                        pnumArry.push(datapnum.PARTNUM);
                    });
                    var pnumArryStr = pnumArry.toString();
                    $("#id-lot-30-days").attr("data-parts", pnumArryStr);

                    $("#div-lot-30-days-overview").html(that.template_details(data));
                    $(".div-dash-widget-header #id-lot-30-days").text(thirtyDays);

                });
            },

			//load expiring in 60 days part number and its count
            load60Days: function (servername, targetname, targettemplate, voname) {
                var that = this;
                var serverinput = {};
                serverinput.expDays = 60;

                fnGetServerData(servername, voname, serverinput, function (data) {

                    if (data.partInfo != "")
                        data = JSON.parse(data.partInfo);
                    else
                        data = data.partInfo;

                    var sixtyDays = 0;
                    _.each(data, function (item) {
                        sixtyDays = sixtyDays + item.COUNT;
                    });
                    if (data.partInfo != "") {
                        data["widget"] = "lotqty60";
                    }

                    var pnumArry = [];
                    _.each(data, function (datapnum) {
                        pnumArry.push(datapnum.PARTNUM);
                    });
                    var pnumArryStr = pnumArry.toString();
                    $("#id-lot-60-days").attr("data-parts", pnumArryStr);

                    $("#div-lot-60-days-overview").html(that.template_details(data));
                    $(".div-dash-widget-header #id-lot-60-days").text(sixtyDays);

                });
            },
			
			//load expiring in 90 days part number and its count
            load90Days: function (servername, targetname, targettemplate, voname) {
                var that = this;
                var serverinput = {};
                serverinput.expDays = 90;
                fnGetServerData(servername, voname, serverinput, function (data) {

                    if (data.partInfo != "")
                        data = JSON.parse(data.partInfo);
                    else
                        data = data.partInfo;
                    var nintyDays = 0;
                    _.each(data, function (item) {
                        nintyDays = nintyDays + item.COUNT;
                    });
                    if (data.partInfo != "") {
                        data["widget"] = "lotqty90";
                    }

                    var pnumArry = [];
                    _.each(data, function (datapnum) {
                        pnumArry.push(datapnum.PARTNUM);
                    });
                    var pnumArryStr = pnumArry.toString();
                    $("#id-lot-90-days").attr("data-parts", pnumArryStr);

                    $("#div-lot-90-days-overview").html(that.template_details(data));
                    $(".div-dash-widget-header #id-lot-90-days").text(nintyDays);

                });
            },
			
			//load expired part number and its count
            loadExpired: function (servername, targetname, targettemplate, voname) {
                var that = this;

                fnGetServerData(servername, voname, {}, function (data) {

                    if (data.partInfo != "")
                        data = JSON.parse(data.partInfo);
                    else
                        data = data.partInfo;

                    var expired = 0;
                    _.each(data, function (item) {
                        expired = expired + item.COUNT;
                    });
                    if (data.partInfo != "") {
                        data["widget"] = "lotqtyexpired";
                    }


                    var pnumArry = [];
                    _.each(data, function (datapnum) {
                        pnumArry.push(datapnum.PARTNUM);
                    });
                    var pnumArryStr = pnumArry.toString();
                    $("#id-lot-expired").attr("data-parts", pnumArryStr);

                    $("#div-lot-expired-overview").html(that.template_details(data));
                    $(".div-dash-widget-header #id-lot-expired").text(expired);
                });
            },
			
			//load data on popup while clicking sum of count on top 
            loadLotDetails: function (e) {
                var that = this;
                var targetid;
                targetid = $(e.target).attr('id');
                var partarray = $(e.target).attr('data-parts');

                var servername, serverinput = {};
                serverinput.partNum = partarray.toString(); //sending the input

                var title = "Lot Details";
                var voname = "GmLotOnHandVO";

                switch (targetid) {
                    case "id-lot-on-hand":
                        servername = "lot/expiryLotDetails";
                        serverinput.expiryFlag = 0;
                        break;
                    case "id-lot-30-days":
                        servername = "lot/expiryLotDetailsByDays";
                        serverinput.expDays = 30;
                        break;
                    case "id-lot-60-days":
                        servername = "lot/expiryLotDetailsByDays";
                        serverinput.expDays = 60;
                        break;
                    case "id-lot-90-days":
                        servername = "lot/expiryLotDetailsByDays";
                        serverinput.expDays = 90;
                        break;
                    case "id-lot-expired":
                        servername = "lot/expiryLotDetails";
                        serverinput.expiryFlag = 1;
                        break;
                }

                showPopupLotReport();
                $("#div-crm-overlay-content-container .modal-title").text(title);
                $("#div-crm-overlay-content-container .modal-body").html(that.template_Loh_Details());
                getLoader("#div_lot_details");
                this.gridObj = "";

                that.loadLotGridData(servername, voname, serverinput);

            },

			//load data on popup while clicking part number count
            loadLotPartDetails: function (e) {
                var that = this;
                var partnumid = $(e.currentTarget).data("partid"); //part number as input
                var widgetTyp = $(e.currentTarget).parent().parent().parent().parent().find("#span-inv-widget-type").attr("data-widget"); //to identify the parent div using the widget
                var servername, serverinput = {};
                var title = "Lot Details";
                var voname = "GmLotOnHandVO";

                serverinput.partNum = partnumid;


                switch (widgetTyp) {
                    case "onhandqty":
                        servername = "lot/expiryLotDetails";
                        serverinput.expiryFlag = 0;
                        break;
                    case "lotqty30":
                        servername = "lot/expiryLotDetailsByDays";
                        serverinput.expDays = 30;
                        break;
                    case "lotqty60":
                        servername = "lot/expiryLotDetailsByDays";
                        serverinput.expDays = 60;
                        break;
                    case "lotqty90":
                        servername = "lot/expiryLotDetailsByDays";
                        serverinput.expDays = 90;
                        break;
                    case "lotqtyexpired":
                        servername = "lot/expiryLotDetails";
                        serverinput.expiryFlag = 1;
                        break;
                }

                showPopupLotReport();
                $("#div-crm-overlay-content-container .modal-title").text(title);
                $("#div-crm-overlay-content-container .modal-body").html(that.template_Loh_Details());
                getLoader("#div_lot_details");
                this.gridObj = "";

                that.loadLotGridData(servername, voname, serverinput);

            },
			//To load data on Grid
            loadLotGridData: function (servername, voname, serverinput) {

                var that = this;
                fnGetWebServerData(servername, voname, serverinput, function (data) {
                    var acid = localStorage.getItem("acid");
                    console.log("acid"+acid)
                    if (data.partDetails != "")
                        data = JSON.parse(data.partDetails);
                    else
                        data = data.partDetails;

                    if (data != "") {
                        var rows = [],
                            i = 0;
                        var lotexpDate;
                        _.each(data, function (griddt) {
                            var rowid = "loh" + i;
                            rows[i] = {};
                            rows[i].id = i;
                            rows[i].data = [];

                            rows[i].data[0] = griddt.RepName;
                            rows[i].data[1] = griddt.PartNum;
                            rows[i].data[2] = griddt.PartDesc;
                            rows[i].data[3] = griddt.LotNum;
                            rows[i].data[4] = formattedDate(griddt.Expdate, localStorage.getItem("cmpdfmt"));
                            rows[i].data[5] = griddt.Location;
                            rows[i].data[6] = griddt.TransID;
                            rows[i].data[7] = formattedDate(griddt.TransDate, localStorage.getItem("cmpdfmt"));
                            rows[i].data[8] = griddt.guconsid;
                            
                            i++;
                        });

                        var dataHost = {};
                        var setInitWidths = "0,90,90,90,80,*,130,120,0";
                        dataHost.rows = rows;
                        if(acid>2){
                        var setInitWidths = "140,90,90,90,80,170,120,100,0";
                        }
                        var setColAlign = "left,left,left,left,left,left,left,left";
                        var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro";
                        var setColSorting = "str,str,str,str,date,str,sortLinkID,date,str";
                        var setHeader = ["FieldSales Name", "Part Number", "Part Description", "Lot Number", "Expiry Date", "Location", "Transaction ID", "Transaction Date",""];
                        var setFilter = ["#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", ""];
                        var enableTooltips = "true,true,true,true,true,true,true,true,false";
                        var footerArry = [];
                        var gridHeight = "";
                        var footerStyles = [];
                        var footerExportFL = true;
                        that.gridObj = loadDHTMLXGrid('div_lot_details', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, setFilter, footerArry, footerStyles);

                        for (var i = 0; i < that.gridObj.getRowsNum(); i++) {
                           that.gridObj.setColumnHidden(8, true);
                            that.gridObj.cells(i, 6).cell.className = 'gmReportLnk';
                        }

                        that.gridObj.attachEvent("onRowSelect", function (id, index) {
                            var chkVal,guid;
                            
                            if (index == 6) {
                                chkVal = that.gridObj.cells(id, 6).getValue();
                                guid = that.gridObj.cells(id, 8).getValue();  
                                openLotOrderSummary(chkVal,guid);
                                
                            }
                        });
                    } else
                        $("#div_lot_details").html("No Data Found");
                });

            },
			
			//load graph based on count 
            loadExpDaysGraph: function (servername, targetname, targettemplate, voname) {
                var that = this;


                fnGetServerData(servername, voname, {}, function (data) {

                    data = JSON.parse(data.lotExpDays);
                    var thirtycount = 0;
                    var sixtycount = 0;
                    var nintycount = 0;
                    var nintyplus = 0;
                    var expired = 0;

                    _.each(data, function (item) {
                        thirtycount = item.THIRTY;
                        sixtycount = item.SIXTY;
                        nintycount = item.NINTY;
                        nintyplus = item.NINTYPLUS;
                        expired = item.EXPIRED;
                    });

                    
                    var thirtyc = {
                        "Label": "0-30 Days",
                        "Value": thirtycount,
                        "color": "#FFFF00"
                    };
                    that.chartArr.push(thirtyc);
                    var sixtyc = {
                        "Label": "31-60 Days",
                        "Value": sixtycount,
                        "color": "#12EC2C"
                    };
                    that.chartArr.push(sixtyc);
                    var nintyc = {
                        "Label": "61-90 Days",
                        "Value": nintycount,
                        "color": "#291DDA"
                    };
                    that.chartArr.push(nintyc);
                    var nintyp = {
                        "Label": "91+ Days",
                        "Value": nintyplus,
                        "color": "#F63209"
                    };
                    that.chartArr.push(nintyp);
                    var expiredc = {
                        "Label": "Expired",
                        "Value": expired,
                        "color": "#ad0a15"
                    };
                    that.chartArr.push(expiredc);
                    that.loadGraph();

                });
            },
			// To load graph
            loadGraph: function () {
                var that = this;
                var chartOptions = {

                    "showBorder": "0",
                    "showShadow": "0",
                    "showCanvasBorder": "0",
                    "bgColor": "#ffffff",
                    "aligncaptionwithcanvas": "0",
                    "captionpadding": "0",
                    "decimals": "0",
                    "startingAngle": "90",
                    "showLabels": "0",
                    "theme": "fusion",
                    "showLegend": "1",
                    "showLegendBorder": "0",
                    "showValues": "0",
                    "drawcustomlegendicon": "1",
                    "legendBorderColor": "#FFFFFF",
                    "chartBottomMargin": "1",
                    "chartTopMargin": "1",
                    "chartRightMargin": "1",
                    "chartLeftMargin": "1",
                    "legendShadow": false,
                    "use3DLighting":"0"
                }

                FusionCharts.ready(function () {
                    var myChart = new FusionCharts({
                        type: "doughnut2d",
                        renderAt: "chart-contain",
                        width: "298",
                        height: "249",
                        dataFormat: "json",
                        dataSource: {
                            "chart": chartOptions,
                            "data": that.chartArr
                        },
                    }).render();
                });
            },

        });

        return InvLotOnHandView;

    });
//function to show popup
function showPopupLotReport() {
    var that = this;
    $('.div-overlay').show();
    $('.scheduler-main-container').attr('style', 'z-index:0');
    $('.div-crm-overlay-content-container').addClass('hide');
    $('#div-crm-overlay-content-container').addClass('popupHostList');
    $('#div-crm-overlay-content-container .modal-footer').addClass('gmINVTextCenter'); //Close button will appear in center
    $('#div-crm-overlay-content-container').removeClass('hide');
    if ($('#div-crm-overlay-content-container .modal-footer').hasClass("hide"))
        $('#div-crm-overlay-content-container .modal-footer').removeClass('hide');

    $("#div-crm-overlay-content-container .modal-footer button").each(function (i) {
        if (!$(this).hasClass("hide"))
            $(this).addClass("hide");
    });
    $(".cancel-model").text("Close");
    $(".cancel-model").removeClass("hide");


    $('#div-crm-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hidePopupLotReport();
        });
    });
}

//On clicking the close icon to hide the popup and remove the popupDOList class from the modal popup
function hidePopupLotReport() {
    $('.div-overlay').hide();
    $('.scheduler-main-container').attr('style', '');
    $('#div-crm-overlay-content-container .modal-footer').removeClass('gmINVTextCenter');
    $('#div-crm-overlay-content-container').removeClass('popupHostList');
    $('#div-crm-overlay-content-container').removeClass('popupHostMain');
    $(".load-model").removeClass("hide");
    $('.div-crm-overlay-content-container').addClass('hide');
    $('.hideDefaultBtn').addClass('hide');
    $(".cancel-model").text("Close");
    $("#div-crm-overlay-msg").addClass('hide');
}

//Function to open Order Summary
function openLotOrderSummary(chkVal,guid) {
    
    console.log("Inside Open Lot summary function event is " + event);
    if ($("#div-dash-drilldown-sortby-header").css("display") != "none") {
        //event.stopImmediatePropagation();
        var that = this,id;
        var URL;
        //				if($(event.currentTarget).attr("data-url")!="") 
        //					id = $(event.currentTarget).attr("data-url");
        //				else
        //					id = $(event.currentTarget).text();
        if (chkVal != null) {
            id = chkVal;
            var companyInfo = {};
            companyInfo.cmpid = localStorage.getItem("cmpid");
            companyInfo.plantid = localStorage.getItem("plantid");
            var StringifyCompanyInfo = JSON.stringify(companyInfo);
            
            if(id.startsWith("GM-CN"))
                {
                  console.log("It is an consignment")  ;
                this.secondURL = "gmShipmentPackSlip.do?method=loadConsignmentPackSlip&skipSessionCheck=Y&hGUId=";
                var toEncode = this.secondURL + guid; 
                URL = URL_Portal + toEncode;
                }
            
            else{
                this.secondURL = "/GmEditOrderServlet?hMode=PrintPrice&hParantForm=SALESDASHBOARD&hOrdId=";
               var toEncode = this.secondURL + id + "&companyInfo="+StringifyCompanyInfo;
               URL = URL_Portal + "GmLogonServlet?token=" + localStorage.getItem("token") + "&return_to=" + encodeURIComponent(toEncode) + "&randomid=" + Math.random();
            }

            console.log("URL is.... "+ URL);
            var summaryWindow = window.open(URL, '_blank', 'location=no');
            summaryWindow.addEventListener('loadstop', function () {
                summaryWindow.insertCSS({
                    code: "body {font-family: 'Trebuchet MS'!important; -webkit-overflow-scrolling: touch;} a{display: none;} .RightDashBoardHeader {background:#999;} .ShadeRightTableCaption {background:#ccc;} TR.ShadeBlueBk {background:#eee}" + that.cssOptions
                });
                summaryWindow.executeScript({
                    code: "var meta = document.createElement('meta'); meta.name = 'format-detection'; meta.content = 'telephone=no'; document.getElementsByTagName('head')[0].appendChild(meta); " + that.scriptOptions
                });
            });
            summaryWindow.addEventListener('loaderror', function () {
                showNativeAlert(lblNm("msg_error_loading"), lblNm("error"), lblNm("msg_ok"), function (index) {
                    summaryWindow.close();
                });
            });

        }
    }
}
