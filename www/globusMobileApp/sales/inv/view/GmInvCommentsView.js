

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification'
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification) {

	'use strict';
	var InvCommentsView = Backbone.View.extend({

		events : {
			"keyup #txtarea-inv-comments" : "updateJSON"
			
		},


		template: fnGetTemplate(URL_Inv_Template, "GmInvComments"),

		initialize: function(options) {
			this.parent = options.parent;
			this.render();
		},

		render: function () {
			this.$el.html(this.template({comments: this.parent.JSONData.prnotes}));
			var maxlength = 500;
			var text = $("#txtarea-inv-comments").val();

			this.$("#spn-inv-comments-limit").text(maxlength - text.length);
			return this;
		},

		updateJSON: function(event) {
			var maxlength = 500;
			var text = event.currentTarget.value;

            text = text.trim();
			if (text.length >= maxlength) {
		        event.currentTarget.value = text.substring(0, maxlength);
		        text = event.currentTarget.value;
                text = text.trim();
		    }

			this.$("#spn-inv-comments-limit").text(maxlength - text.length);
			this.parent.JSONData.prnotes = text;
		},

		close: function() {
			this.unbind();
			this.undelegateEvents();
		}
		

	});	

	return InvCommentsView;
});