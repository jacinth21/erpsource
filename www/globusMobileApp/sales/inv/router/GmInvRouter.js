/**********************************************************************************
 * File:        GmIMRouter.js
 * Description: Router specifically meant to route views within the 
 *              Inventory Management application
 * Version:     1.0
 * Author:      asivaraj
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',
        'invmainview'
         
      ], 
    
    function ($, _, Backbone, InvMainView) {

	'use strict';

	var InvRouter = Backbone.Router.extend({

		routes: {
			"inv": "inv",
			"inv/:viewType": "inv"
		},
		inv: function (viewType) {
		    $("#div-messagebar").show();
		    GM.Global.currentModule = "INV";
		    GM.Global.AICFL = "";
		    hideMessages();
		    this.closeView(invmainview);
		    fnGetRuleValueByCompany("EXC_REQ_TYPES", "CONTAB", localStorage.getItem("cmpid"), function (dt) {
		        if (dt.length > 0) {
		            if (dt[0].RULEVALUE == "Y") {
		                GM.Global.AICFL = "Y"; // Account Item Consignment Flag
		            }
		        }
		        var option = [];
		        option.push(viewType);
		        invmainview = new InvMainView(option);
		        $("#div-main").html(invmainview.el);

		    });
		},
		
		
		closeView: function(view) {
			if(view!=undefined) {
				view.unbind();
				view.remove();
				view = undefined;
				$("#btn-back").unbind();
			}

		}
	});

	return InvRouter;
});
