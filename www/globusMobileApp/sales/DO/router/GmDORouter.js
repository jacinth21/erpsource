/**********************************************************************************
 * File:        GmDORouter.js
 * Description: Router specifically meant to route views within the 
 *              DO application
 * Version:     1.0
 * Author:      tvenkateswarlu
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',
        'domainview'
         
      ], 
    
    function ($, _, Backbone, DOMainView) {

	'use strict';

	var DORouter = Backbone.Router.extend({

		routes: {
			"do": "do",
			"do/BookDO/:DOID" : "do",
			"do/:viewType": "do"
		},

		do: function(viewType){
            var SyncType ="";
            if (GM.Global.CodelookupCount > 0 && GM.Global.CodelookupExclusionCount > 0 && GM.Global.CodelookupAccessCount > 0 && GM.Global.RulesCount > 0){
                if (GM.Global.PartRFSCount > 0) 
                    this.openDO(viewType);
                else{
                    GM.Global.DoValidation = lblNm("msg_sync_part");
                      window.history.back();
                    
//                   showError("Please sync Part to Proceed");
              
                }
            }
            else{
               SyncType =" APP ";
                if (GM.Global.PartRFSCount == 0) 
                  SyncType = SyncType + " and  Part" ;
                 GM.Global.DoValidation = lblNm("msg_sync")+ SyncType +lblNm("msg_proceed");
                window.history.back();
//                showError("Please sync "+ SyncType +" to Proceed"); 
                 
            }   
		},
		
        openDO: function(viewType){
            GM.Global.currentModule ="DO"
            $("#div-messagebar").show();
            hideMessages();
			this.closeView(domainview);
			var option = [];
			option.push(viewType);
			domainview = new DOMainView(option);
			$("#div-main").html(domainview.el);
			
			//if any Tag is not mapped to set then we need to sync the Tags again.
            /*fnTagSetMapCountValidation(function(tagsetmaplen){
              if(tagsetmaplen > 0){
                  fnDeleteTagRecord(function(callback){
                      if(callback == "success") {
                          fnsyncTagDataInAppStartup(); 
                      }
                  });  
               }  
            });*/
			
            
        },
		
		closeView: function(view) {
			if(view!=undefined) {
				view.unbind();
				view.remove();
				view = undefined;
				$("#btn-back").unbind();
			}

		}
	});

	return DORouter;
});
