/**********************************************************************************
 * File:        GmDOShipToInfoView.js
 * Description: 
 * Version:     1.0
 * Author:     	tvenkateswarlu
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification','datasync','saleitemmodel', 'saleitemcollection', 'saleitemlistview','itemview', 'shipaddressview','dropdownview'
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification,DataSync, SaleItem, SaleItems, SaleItemListView,ItemView, ShipAddressView,DropDownView) {

	'use strict';
	var DOShipToInfoView = Backbone.View.extend({

		events : {
			"click .li-do-shiptoinfo-checkbox-header": "selectMultiplebox",
			"click .li-do-shiptoinfo-checkbox": "removeCheck",
			"click #btn-address": "getaddress", 
            "change .sel-address-carrier": "getDefaultCarrierMode",
            "change .sel-address-mode": "getDefaultMode",
            "change .sel-address-carrier": "getCarrier"
			}
			, /* Load the templates */
			template: fnGetTemplate(URL_DO_Template, "GmDOShipToInfo"),
			template_shipInfoMbl: fnGetTemplate(URL_DO_Template, "GmDOShipToInfoMobile"),
        
        
        initialize: function (options) {
			        this.parent = options.parent;
            var that =this;
            GM.Global.DefaultShipto = "";
			        GM.Global.DefaultCarrier = "";
			        GM.Global.DefaultMode = "";
                    GM.Global.defaultAddress="";
                    this.dfltAcct="";
                    this.dfltRep=""; 
                    this.dfltDist="";
                    this.dfltEmployee="";
                    this.dfltShippingAddress="";
                    this.dfltDealer="";
			        this.fngetdefaulShipto(function(data){
                         that.fngetdefaulcarrier(function(data){
                             that.fngetdefaulmode(function(data){
                                 that.fngetDefaultValues(function(success){
                                 that.setDefAddress(function(data){
                                    if ((that.parent.JSONData.gmTxnShipInfoVO.length == undefined) || (that.parent.JSONData.gmTxnShipInfoVO.length == 0)) 
                                        that.initializeAddress();
                                    else 
                                        that.reInitialize();  
                                      });
                            }); 
                             });  
                         });
                    });
			    },
        
		initializeAddress: function() {
			console.log("initializeAddress");
			var that = this;
			fnSalesRepDfltAddress(this.parent.JSONData.repid,function(data) {
				that.parent.repPrimaryAddress = {};
				if(data.length>0) {
					var data = data[0];
					that.parent.repPrimaryAddress = $.parseJSON(JSON.stringify(data));
					var JSONArray = new Array();
					that.viewJSON = new Array();
					for(var i=0;i<that.parent.JSONParts.length;i++) {
						if((that.parent.JSONParts[i].type == "C") || (that.parent.JSONParts[i].repl != "") ) {
							var JSONShip = {};
                            console.log("GM.Global.defaultAddress");
                            console.log(GM.Global.defaultAddress);
							JSONShip.source = "50180";
							JSONShip.addressid = GM.Global.defaultAddress[0].AddressID;
							JSONShip.attnto = GM.Global.defaultAddress[0].Name;
//							JSONShip.ship_to_id = that.parent.JSONData.repid;
							JSONShip.ship_to_id = GM.Global.ShipToId;
//							JSONShip.shipto = "4121";
							JSONShip.shipto = GM.Global.DefaultShipto;
//							JSONShip.shipcarrier = "5001";
							JSONShip.shipcarrier =GM.Global.DefaultCarrier; 
//							JSONShip.shipmode = "5032";
							JSONShip.shipmode = GM.Global.DefaultMode;
							JSONShip.shipinstruction = "";
							JSONArray.push({"indexValue" : that.parent.JSONParts[i].indexValue, "ShipInfo" : $.parseJSON(JSON.stringify(JSONShip)) });
							that.parent.JSONParts[i].bgColor = "#800";
							that.parent.JSONParts[i].shipTo = JSONShip;
							that.parent.JSONParts[i].shipTo.carrierName = GM.Global.DefaultCarrierName;
							that.parent.JSONParts[i].shipTo.modeName = GM.Global.DefaultModeName;
							that.parent.JSONParts[i].AddressData = GM.Global.defaultAddress[0];
							that.viewJSON.push($.parseJSON(JSON.stringify(that.parent.JSONParts[i])));
						}
					}
					that.parent.JSONData.gmTxnShipInfoVO = $.parseJSON(JSON.stringify(JSONArray));
					that.render(that.viewJSON);
				}
				else
					that.render(that.parent.JSONParts);		
				that.parent.saveDO();
			});
		},
		reInitialize: function() {
			var that = this;
			fnSalesRepDfltAddress(this.parent.JSONData.repid,function(data) {
				if(data.length>0) {
					var data = data[0];
					var JSONArray = new Array();
					that.viewJSON = new Array();
				    for(var i=0;i<that.parent.JSONParts.length;i++) {
                            if((that.parent.JSONParts[i].type == "C") || (that.parent.JSONParts[i].repl != "") ) {
                                console.log("ShipTo : " + typeof(that.parent.JSONParts[i].shipTo));
                                if(that.parent.JSONParts[i].shipTo==undefined) {
                                    var JSONShip = {};
                                    JSONShip.source = "50180";
                                    JSONShip.addressid = GM.Global.defaultAddress[0].AddressID;
                                    JSONShip.attnto = GM.Global.defaultAddress[0].Name;
                                    JSONShip.ship_to_id = GM.Global.ShipToId;
                                    JSONShip.shipto = GM.Global.DefaultShipto;
                                    JSONShip.shipcarrier = GM.Global.DefaultCarrier;
                                    JSONShip.shipmode = GM.Global.DefaultMode;
                                    JSONShip.shipinstruction = "";
                                    that.parent.JSONData.gmTxnShipInfoVO.push({"indexValue" : that.parent.JSONParts[i].indexValue, "ShipInfo" : JSONShip});
                                    that.parent.JSONParts[i].bgColor = "#800";
                                    that.parent.JSONParts[i].shipTo = JSONShip;
                                    that.parent.JSONParts[i].AddressData = GM.Global.defaultAddress[0];
                                    that.parent.JSONParts[i].shipTo.carrierName = GM.Global.DefaultCarrierName;
                                    that.parent.JSONParts[i].shipTo.modeName = GM.Global.DefaultModeName;
                                    that.viewJSON.push($.parseJSON(JSON.stringify(that.parent.JSONParts[i])));
                                }
                                else
                                    that.viewJSON.push($.parseJSON(JSON.stringify(that.parent.JSONParts[i])));
                                console.log(that.viewJSON[i])
						}
					}
					 // = $.parseJSON(JSON.stringify(JSONArray));
					that.render(that.viewJSON);
				}
				else
					that.render(that.parent.JSONParts);
				that.parent.saveDO();
			});
		},

		// Render the initial contents when the Catalog View is instantiated
		render: function (viewJSON) {
            var that = this;
//			showMsgView("011");
            GM.Global.totalMode="";
             fnGetModeList(GM.Global.DefaultCarrier,localStorage.getItem("cmpid"),function (data) {
                GM.Global.totalMode = _.each(data,function(model){
                    fnGetCodeNamebyId(model.ID,function(modelist){
                         model.Name = modelist.Name;  
                        }); 
                    
                }); 
                 console.log(GM.Global.totalMode)
            });
            console.log("SHIP INFO " + JSON.stringify(viewJSON));
            if ($(window).width() < 480) {
                that.$el.html(that.template_shipInfoMbl(viewJSON));
                $(".ship-info-mbl ul").addClass("ul-show-part");
                $(".ship-info-mbl .togglebx i").removeClass("onActive");
                $(".ship-info-mbl ul:last-child").addClass("showCheckInfo");
                $(".ship-info-mbl ul.showCheckInfo").removeClass("ul-show-part");
                $(".ship-info-mbl ul.showCheckInfo").find(".togglebx img").addClass("onActive");

                $(".togglebx").unbind('click').bind('click', function() {
                    $(this).parent("ul").toggleClass('ul-show-part');
                    $(this).children("img").toggleClass("onActive");
                });
            }
            else{
                that.$el.html(that.template(viewJSON));
            }
			this.indicateAddressType();
			return this;
		},
        fngetDefaultValues: function (callback) {
            var that =this;
            fnGetRepInfo(this.parent.JSONData.repid, function (data) {
                    fnGetpartyIDByEmployeeID(data.C101_PARTY_ID,function(empid){
							that.dfltAcct = that.parent.JSONData.acctid;
							that.dfltRep = that.parent.JSONData.repid; 
							that.dfltDist = data.C701_DISTRIBUTOR_ID;
                            that.dfltEmployee =empid[0];
                            that.dfltDealer = that.parent.JSONData.dealid;
                            that.dfltShippingAddress = data.C101_PARTY_ID;
                        callback("setted")
						});
                    });
		},
        setDefAddress: function (callback) {
            var that =this;
        switch(GM.Global.DefaultShipto) {
				case "4122"://Hospital
                    if (that.dfltAcct == undefined || that.dfltAcct == "") 
                       getAcctAddressFromRep(that.dfltRep,function(data){
                           GM.Global.ShipToId =that.dfltRep;
                           callback("success")
                       });
                    else 
                        getAcctAddress(that.dfltAcct,function(data){
                            GM.Global.ShipToId=that.dfltAcct;
                            callback("success")
                        });
                    break;
                case "4121"://Rep
                    getRepAddress(that.dfltRep,function(data){
                        GM.Global.ShipToId=that.dfltRep;
                        callback("success")
                    });
                    break;
                case "4120"://Distributor
                    getDistAddress(that.dfltDist,function(data){
                        GM.Global.ShipToId=that.dfltDist;
                        callback("success")
                    });
                    break;
                case "4123"://Employee
                    getEmployeeAddress(that.dfltEmployee,function(data){
                       GM.Global.ShipToId=that.dfltEmployee;
                        callback("success")
                    })
                    break;
                case "107801"://Dealer
                    getPartyAddress (that.dfltDealer,function(data){
                        GM.Global.ShipToId=that.dfltDealer;
                        callback("success")
                    })
                     break;
                case "4000642"://ShippingAddress
                    getshippingAccountAddress(that.dfltShippingAddress,function(data){
                        GM.Global.ShipToId=that.dfltShippingAddress;
                        callback("success")
                    });
				}
		},
		indicateAddressType: function () {
			this.$(".ul-do-shipinfo-parts-list").each(function() {
				switch($(this).attr("data-shipto")) {
					case "4121":
                        //added for PC-938 - To indicate address type as H if shipping address selected as Hospital in Sales Rep
                        var el = $(this);
                        fnGetAddressType(el.attr("data-address-id"),function(data){
                        	if(data != "") {
                        		if(data[0].ADDRTYPE == 26240675){
                        			el.find(".li-do-shiptoinfo-address-indicator").text("H");
                        			el.find(".sinfo-type").text("H");
                        		}
                        		else {
                        			el.find(".li-do-shiptoinfo-address-indicator").text("S");
                                    el.find(".sinfo-type").text("S");
                        		}
                        	}
                        });
						break;
					case "4122":
						$(this).find(".li-do-shiptoinfo-address-indicator").text("H");
                        $(this).find(".sinfo-type").text("H");
						break;
					case "4120":
						$(this).find(".li-do-shiptoinfo-address-indicator").text("D");
                        $(this).find(".sinfo-type").text("D");
						break;
                    case "4123":
						$(this).find(".li-do-shiptoinfo-address-indicator").text("E");
                        $(this).find(".sinfo-type").text("E");
						break;
                    case "4000642":
						$(this).find(".li-do-shiptoinfo-address-indicator").text("SA");
                        $(this).find(".sinfo-type").text("SA");
						break;
                    case "107801":
						$(this).find(".li-do-shiptoinfo-address-indicator").text("DE");
                        $(this).find(".sinfo-type").text("DE");
						break;
				}
			});
		},

		selectMultiplebox: function(event) {
			var elem = event.currentTarget;
			if($(elem).hasClass('fa fa-check selected')){
				$(elem).removeClass('fa fa-check selected');
				this.$(".li-do-shiptoinfo-checkbox").removeClass("fa fa-check selected");
			}
			else {
				$(elem).addClass('fa fa-check selected');
				this.$(".li-do-shiptoinfo-checkbox").addClass("fa fa-check selected");
			}
		},
		removeCheck:function(event){
			event.preventDefault();
			var elem = event.currentTarget;
			// $(elem).addClass("whiteBackground");
			if($(elem).hasClass("selected")) {
				this.$(elem).removeClass("fa fa-check selected");
				$(".li-do-shiptoinfo-checkbox-header").removeClass("fa fa-check selected");
			}
			else {
				this.$(elem).addClass("fa fa-check selected");
			}
			if ($(".li-do-shiptoinfo-checkbox").length == $(".li-do-shiptoinfo-checkbox.selected").length) {
				$(".li-do-shiptoinfo-checkbox-header").addClass("fa fa-check selected");
			}
		},

		getaddress: function() {
			this.$(".li-do-shiptoinfo-checkbox").toggleClass("force-bg-light-grey");
			if(this.$("#div-shipping-address").html().trim()=="" || this.$("#div-shipping-address").css("display") == "none") {
				this.$("#div-shipping-address").show();	
				var that = this;
				fnGetRepInfo(this.parent.JSONData.repid, function (data) {
					if(that.shipaddressview) 
						that.shipaddressview.close();
                    fnGetpartyIDByEmployeeID(data.C101_PARTY_ID,function(empid){
                        that.shipaddressview = new ShipAddressView(
						{ 
							el: "#div-shipping-address", 
							dfltAcct: that.parent.JSONData.acctid, 
							dfltRep: that.parent.JSONData.repid, 
							dfltDist: data.C701_DISTRIBUTOR_ID,
                            dfltEmployee:empid[0],
                            dfltShippingAddress: data.C101_PARTY_ID,
                            dfltDealer: that.parent.JSONData.dealid,
							onListRender: function() {that.onListRender();}
						});
                    
					that.$("#div-shipping-address").bind("onselect", function(event, id, elem) {
						that.getAddressColors(id, elem);
					});   
                    })  
				})
			}
			else {
				this.$("#div-shipping-address").hide();
				this.$(".li-do-shiptoinfo-checkbox").removeClass("fa fa-check selected");
				this.$(".li-do-shiptoinfo-checkbox-header").removeClass('fa fa-check selected');
			}
		},

		onListRender: function() {
			var that = this;
                     $('.sel-address-carrier').each(function () {
                         fnCreateOptions(this, GM.Global.totalCarrier);
                         $(this).val($(this).attr("value"));
            });
              GM.Global.totalMode = _.filter(GM.Global.totalMode, function (data) {
                  return data.ID != 0;
              })
              GM.Global.totalMode.push({
                  "ID": 0
                  , "Name": "Choose one"
              });
              
          $(".sel-address-mode").each(function () {
                            fnCreateOptions(this, GM.Global.totalMode);
                                  $(this).val($(this).attr("value"));   
                            });
              
//            $(".sel-address-carrier").val(GM.Global.DefaultCarrier);
            $(".sel-address-carrier").each(function() {
					if($(this).val() == undefined || $(this).val() == "" ||$(this).val() == null)
                        $(this).val(GM.Global.DefaultCarrier);
			});
//             $('.sel-address-mode').attr("mode",GM.Global.DefaultMode);
             $('.sel-address-mode').each(function() {
					if($(this).val() == undefined || $(this).val() == "" ||$(this).val() == null)
                        $(this).val(GM.Global.DefaultMode);
			});
            
            
            
			var ShipInfos = _.pluck(this.parent.JSONData.gmTxnShipInfoVO,"ShipInfo");
			var idList = _.pluck(ShipInfos,"addressid");
			$(".div-address-row").each(function() {
				var id = this.id.replace("div-address-","");
				console.log(id);
				console.log(_.contains(idList,id));
				if(_.contains(idList,id)) {
					var JSONAddress = _.findWhere(ShipInfos,{addressid: id});
					console.log(JSONAddress);
					var carrier = JSONAddress.shipcarrier;
					var mode = JSONAddress.shipmode;
					var attnto = JSONAddress.attnto;
					var shipinstruction = JSONAddress.shipinstruction;
					$(this).find(".sel-address-carrier").val(carrier.toString()).trigger("change");
                    $(this).find(".sel-address-mode").attr("mode",mode.toString())
					$(this).find(".sel-address-mode").val(mode.toString());
					$(this).find(".spn-attn-to").text(attnto.toString());
					$(this).find(".fa-info-circle").attr("data-instruction", shipinstruction.toString());
				}
			});
              
            
		},

		getAddressColors:function(id, elem){
			var that = this;

			var carrier = $(elem).find('.sel-address-carrier').val();
			var mode = $(elem).find('.sel-address-mode').val();
			var attnto = $(elem).find('.spn-attn-to').text();
			var shipInstruction = $(elem).find(".fa-info-circle").attr("data-instruction");
               var carrierName =  $(elem).find('.sel-address-carrier option:selected').text();
               var modeName =  $(elem).find('.sel-address-mode option:selected').text();

			this.$('#div-do-shiptoinfo-parts-list').find(".ul-do-shipinfo-parts-list").each( function() {
				if($(this).find(".li-do-shiptoinfo-checkbox").hasClass("selected")) {

					$(this).attr("data-address-id",id);
					$(this).attr("data-ship-to-id",that.shipaddressview.getShipInfo().shipToID);
					$(this).attr("data-shipto",that.shipaddressview.getShipInfo().shipTo);
					$(this).attr("data-attn-to",attnto);
					$(this).attr("data-instruction",shipInstruction);

					var x = $(elem).css("background-color");
					var div = document.createElement('div');
					div.innerHTML = $(elem).find(".div-address").html();
					$(div).attr("class",id);
					$(div).find("i").remove();
					$(this).find(".li-shiptoinfo-address").html(div);
					$(this).find(".selected").css("background-color",x);

					$(this).find(".li-carrier-name").text(carrierName);
					$(this).find(".li-mode-name").text(modeName);
				}
				else if($(this).attr("data-address-id").toString()==id.toString()) {
					$(this).find(".li-carrier-name").text(carrierName);
					$(this).find(".li-mode-name").text(modeName);
					$(this).find(".spn-attn-to").text(attnto);
					$(this).attr("data-attn-to",attnto);
					$(this).attr("data-instruction",shipInstruction);
				}
			});
			that.indicateAddressType();
			that.updateJSON(id, carrier, mode);
		},

		updateJSON: function(id, carrier, mode) {
			
			var JSONArray = new Array();
			var that = this;
			this.$('#div-do-shiptoinfo-parts-list').find(".ul-do-shipinfo-parts-list").each( function() {
				var index = $(this).attr("data-index");
				var JSONShip = {};
				JSONShip.source = "50180";
				JSONShip.addressid = $(this).attr("data-address-id");
				JSONShip.attnto = $(this).attr("data-attn-to");
				JSONShip.ship_to_id = $(this).attr("data-ship-to-id");
				JSONShip.shipto = $(this).attr("data-shipto");
				JSONShip.shipinstruction = $(this).attr("data-instruction");
				console.log(JSONShip.addressid)
				console.log(id);
				if(JSONShip.addressid==id) {
					JSONShip.shipcarrier = carrier;
					JSONShip.shipmode = mode;
					$(this).attr("data-carrier",carrier);
					$(this).attr("data-mode",mode);
				}
				else {
					console.log("Else");
					console.log($(this).attr("data-carrier"));
					JSONShip.shipcarrier = $(this).attr("data-carrier");
					JSONShip.shipmode = $(this).attr("data-mode");
				}

				var addrText = $(this).find('.li-shiptoinfo-address').find('ul');
				var i = 0;

				for(var j=0;j<that.parent.JSONParts.length;j++) {
					if(that.parent.JSONParts[j].indexValue == index) {
						i=j;
						break;
					}
				}
				
				that.parent.JSONParts[i].AddressData = {};
				that.parent.JSONParts[i].AddressData.Name = $(addrText).find('li:nth-child(1)').text().trim();

				that.parent.JSONParts[i].AddressData.Add1 = $(addrText).find('li:nth-child(2)').text().trim();
				that.parent.JSONParts[i].AddressData.Add2 = $(addrText).find('li:nth-child(3)').text().trim();
				that.parent.JSONParts[i].AddressData.City = $(addrText).find('li:nth-child(4)').text().trim().split(",")[0];
				that.parent.JSONParts[i].AddressData.State = $(addrText).find('li:nth-child(4)').text().trim().split(",")[1];
				that.parent.JSONParts[i].AddressData.Zip = $(addrText).find('li:nth-child(5)').text().trim();
                that.parent.JSONParts[i].AddressData.AddressType = $(addrText).find('li:nth-child(6)').text().trim();

				that.parent.JSONParts[i].bgColor = $(this).find(".li-do-shiptoinfo-checkbox").css("background-color");
				that.parent.JSONParts[i].shipTo = JSONShip;
				that.parent.JSONParts[i].shipTo.carrierName = $(this).find(".li-carrier-name").text().trim();
				that.parent.JSONParts[i].shipTo.modeName = $(this).find(".li-mode-name").text().trim();
				JSONArray.push({"indexValue" : $(this).attr("data-index"), "ShipInfo" : JSONShip});
			});

			this.parent.JSONData.gmTxnShipInfoVO = $.parseJSON(JSON.stringify(JSONArray));
			this.parent.saveDO();
		},

        /*fngetdefaulShipto() : Function to get Default shipto in the shipping section based on the loggin company
        */
        fngetdefaulShipto :function(callback){
            fnGetRuleValueByCompany ("SHIP_TO","SHIP_CARR_MODE_VAL",localStorage.getItem("cmpid"),function(data){
            	if (data.length>0) {
              		GM.Global.DefaultShipto = data[0].RULEVALUE; 
          		}
                 callback("success");
            });
        } ,
        /*fngetdefaulcarrier() : Function to get Default Carrier in the shipping section based on the loggin company
        */
        fngetdefaulcarrier:function  (callback){
            fnGetRuleValueByCompany ("SHIP_CARRIER","SHIP_CARR_MODE_VAL",localStorage.getItem("cmpid"),function(data){
                GM.Global.DefaultCarrier = data[0].RULEVALUE;
                
                fnGetCodeNamebyId(GM.Global.DefaultCarrier,function(data){
                    GM.Global.DefaultCarrierName = data.Name;
                    callback("success");
                })
                
            });
        } ,
        /*fngetdefaulmode() : Function to get Default Mode in the shipping section based on the loggin company
        */
        fngetdefaulmode: function (callback) {
             fnGetRuleValueByCompany("SHIP_MODE", "SHIP_CARR_MODE_VAL", localStorage.getItem("cmpid"), function (data) {
                 GM.Global.DefaultMode = data[0].RULEVALUE;
                 fnGetCodeNamebyId(GM.Global.DefaultMode, function (data) {
                     GM.Global.DefaultModeName = data.Name;
                     callback("success");
                 })
             });
         },
        getDefaultMode:function(e){
             $(e.currentTarget).attr("mode",$(e.currentTarget).val()); 
            $(e.currentTarget).parent().trigger("click");
        },
        getCarrier:function(e){
             $(e.currentTarget).parent().find(".sel-address-mode").attr("mode",""); 
            $(e.currentTarget).parent().trigger("click");
            this.getDefaultCarrierMode(e)
        },
      
           getDefaultCarrierMode: function (e) {
             var that = this;
             var carrierID = $(e.currentTarget).val();
             fnGetModeList(carrierID, localStorage.getItem("cmpid"), function (data) {
          
                 _.each(data, function (model) {
                     fnGetCodeNamebyId(model.ID, function (modelist) {
                         model.Name = modelist.Name;
                         $(e.currentTarget).parent().find(".sel-address-mode").html("");
                         $(e.currentTarget).parent().find(".sel-address-mode").each(function () {
                             fnCreateOptions(this, data);
                             $(this).val($(this).attr("value"));
                         });
                         fnGetRuleValueByCompany(carrierID, "SHIPCARDEFUALTMODE", localStorage.getItem("cmpid"), function (data) {
                             if (data.length > 0) $(e.currentTarget).parent().find(".sel-address-mode").val(data[0].RULEVALUE);
                             else $(e.currentTarget).parent().find(".sel-address-mode").val(0);
                             if ($(e.currentTarget).parent().find(".sel-address-mode").attr("mode") != "" && $(e.currentTarget).parent().find(".sel-address-mode").attr("mode") != undefined) {
                                 $(e.currentTarget).parent().find(".sel-address-mode").val($(e.currentTarget).parent().find(".sel-address-mode").attr("mode"));
                             }
                             $(e.currentTarget).parent().trigger("click");
                         });
                     });
                 });
             });
         },
		close: function() {
			this.unbind();
			this.undelegateEvents();
		}
 
	});	

	return DOShipToInfoView;
});