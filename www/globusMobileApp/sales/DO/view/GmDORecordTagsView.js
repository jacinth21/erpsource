/**********************************************************************************
 * File:        GmDORecordTagsView.js
 * Description: This View is used to load the tags recorded for the DO
 * Author:     	karthik
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone', 'handlebars',
        'global', 'device', 'notification', 'searchview', 'dropdownview', 'loaderview', 'imagecropview', 'daodo', 'saleitemmodel', 'saleitemcollection', 'saleitemlistview', 'dorecordtagssearchview'
        ],

    function ($, _, Backbone, Handlebars, Global, Device, Notification, SearchView, DropDownView, LoaderView, CropView, DAODO, SaleItem, SaleItems, SaleItemListView, DoRecordTagsSearchView) {

        'use strict';
        var GmDORecordTagsView = Backbone.View.extend({

            events: {
                "click #doScanTagDetails": "getBarcodeValue",
                "click .li-do-record-tag .clear-row-pu": "deleteRecordTags",
                "click .repl-checkbox": "usedTagList",
                "click .clear-row-pu": "deleteRecordTags",
                "click .rctag-setlist-pop": "addPartSold",
                "click #div-open-tag-favorite" : "fnGetUserFavorites",
                "click #div-open-tag-img" : "fnProcessTagFromImage",
                "click #btn-add-favorite" : "fnCreateFavorites",
            },
            /* Load the templates 
             */
            template: fnGetTemplate(URL_DO_Template, "GmDORecordTags"),
            template_recordtagdetails: fnGetTemplate(URL_DO_Template, "GmDORecordTagDetails"),
            template_recordtagdetailsMobile: fnGetTemplate(URL_DO_Template, "GmDORecordTagDetailsMobile"),
            template_recordsetPartsPop: fnGetTemplate(URL_DO_Template, "GmDORecordSetPartsTab"),
	    	template_tagFavorites: fnGetTemplate(URL_DO_Template, "GmDOTagFavorites"),
            template_tag_Images_list: fnGetTemplate(URL_DO_Template, "GmDOReadTextFromImg"),
            template_tag_Images_list_mobile: fnGetTemplate(URL_DO_Template, "GmDOReadTextFromImgMobile"),

            /*Initialize the function */
            initialize: function (options) {
                var that = this;
                this.parent = options.parent;
                this.render(options);
                this.setGrpid = [];
                this.firebaseFavCnt = 0;
                this.FavoriteName = "";
		        this.gmTagFromFirebaseVO = {};
                this.arrDoTagImageDetails = [];
                this.arrDORecTagFrmImgVO = [];
                fnGetLocalFileSystemPath(function (localpath) {
                    that.localpath = localpath;
                    //that.localpath ="";
                });
            },

            /*Render  function:Load the tag and set id Dropdowns 
             */
            render: function (options) {
                var that = this;
                this.$el.html(this.template());
                $('#tagDetails #div-search-result-main').removeClass('hide');
                /*Tag Search
                 */
                var renderOptions = {
                    "el": this.$('#tagDetails'),
                    "ID": "txt-billing-account",
                    "Name": "txt-search-name",
                    "Class": "txt-keyword-account",
                    "minChar": "3",
                    "autoSearch": true,
                    "placeholder": lblNm("msg_tag_search"),
                    "fnName": fnGetDOTagInfo,
                    "usage": 2,
                    "template": 'GmSearchResult',
                    "template_URL": URL_Common_Template,
                    "callback": function (rowValue) {
                        that.loadTagDetailsFromView(rowValue);
                    }

                };

                var searchview = new SearchView(renderOptions);

                /* Set ID/Set name  Search
                 */
                var renderOptions = {
                    "el": this.$('#setDetails'),
                    "ID": "txt-billing-account",
                    "Name": "txt-search-name",
                    "Class": "txt-keyword-account",
                    "minChar": "3",
                    "autoSearch": true,
                    "placeholder": lblNm("msg_set_search"),
                    "fnName": fnGetSetsDetails,
                    "usage": 2,
                    "template": 'GmSearchResult',
                    "template_URL": URL_Common_Template,
                    "callback": function (rowValue) {
                        that.loadSetDetailsFromView(rowValue);
                    }

                };
                var searchview = new SearchView(renderOptions);

                /*Render the tag details in Record tags screen if the User is coming from Draft/Rejected Screen*/
                if (this.parent.DO != undefined) {
                    $(".ul-do-do-tag-list-header").show();
                    this.displayTagDetailsFromDraft(this.parent.JSONData.arrDORecordTagIDVO);
                }
                
                /*Save Geo Location details*/
                if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function (position) {
                            var latitude = position.coords.latitude;
                            var longitude = position.coords.longitude;
                            console.log("latitude Record " + position.coords.latitude);
                            console.log("longitude Record" + position.coords.longitude);
                            GM.Global.latitude = latitude;
                            GM.Global.longitude = longitude;
                        });
                    }
                
                this.fnLoadFavInfo(); //check document in the fire db
                
                return this;
            },

            /*displayTagDetails: While coming from draft ,the screen should render from the local table*/
            displayTagDetailsFromDraft: function (data) {
                var that = this;

                //to hide add favorite button when tag data count is zero
                 if (firedb != undefined) {
                    if (data.length == 0) {
                        $("#btn-add-favorite").addClass("hide");
                    } else {
                        $("#btn-add-favorite").removeClass("hide");
                    }
                 }

                for (var i = 0; i < data.length; i++) {
                    if ($(window).width() < 480) {
                        this.$("#div-do-record-tag-list").append(this.template_recordtagdetailsMobile(data[i]));
                        this.comnMobFunc();
                        that.enableUsedTagByParts();
                    } else {
                        this.$("#div-do-record-tag-list").append(this.template_recordtagdetails(data[i]));
                        that.enableUsedTagByParts();
                    }
                }
            },
            
            /*displayTagDetails: While coming from draft ,the screen should render from the local table*/
            displaydraftRecordTag: function (data, cb) {
                var that = this;
                for (var i = 0; i < data.length; i++) {
                    if ($(window).width() < 480) {
                        this.$("#div-do-record-tag-list").append(this.template_recordtagdetailsMobile(data[i]));
                        this.comnMobFunc();
                        that.enableUsedTagByParts();
                    } else {
                        this.$("#div-do-record-tag-list").append(this.template_recordtagdetails(data[i]));
                        that.enableUsedTagByParts();
                    }
                }
                cb(true);
            },
			
            loadTagDetailsFromScan: function (data) {
                var that = this;
                var tagid = data;
                that.loadTagDetails(tagid);
            },

            loadTagDetails: function (tagid) {
                var that = this;
                $('#tagDetails #txt-billing-account').val('');
              //  $('#tagDetails #div-search-result-main').addClass('hide');
                $('#tagDetails #div-common-search-x').addClass('hide');
                var counttagduplicate = 0;
                if (tagid != "") {
                    if (tagid.length <= 7) {
                        while (tagid.length < 7) {
                            tagid = '0' + tagid;
                        }
                        var token = localStorage.getItem("token");
                        var userID = localStorage.getItem("userID");
                        var searchTagId = tagid;
                        var cmpid = localStorage.getItem("cmpid");
                        var plantid = localStorage.getItem("plantid");
                        var data = {
                            "tagid": searchTagId,
                            "setid": '',
                            "setnm": '',
                            "tagusagefl": ''
                        }
                        fnGetDOTagScan(tagid, function (data) {
                            console.log("data json" + JSON.stringify(data));
                            if (data != "") {
                                _.each(that.parent.JSONData.arrDORecordTagIDVO, function (item) {
                                    if (tagid == item.tagid) {
                                        counttagduplicate = 1;
                                        showError(lblNm("tagid_exist"));
                                    }
                                });
                                if (counttagduplicate == 0) {
                                    $(".ul-do-do-tag-list-header").show();

                                    if ($(window).width() < 480) {
                                        that.$("#div-do-record-tag-list").append(that.template_recordtagdetailsMobile(data[0]));
                                        that.comnMobFunc();
                                        that.enableUsedTagByParts();
                                    } else {
                                        that.$("#div-do-record-tag-list").append(that.template_recordtagdetails(data[0]));
                                        that.enableUsedTagByParts();
                                    }
                                    that.updateJSON(data[0]);
                                }
                            } else {
                                showError(lblNm("no_data_found"));
                            }
                        });
                    } else {
                        var data = {
                            "tagid": tagid,
                            "setid": '',
                            "setnm": '',
                            "tagusagefl": ''
                        }
                        if (data != "") {
                                _.each(that.parent.JSONData.arrDORecordTagIDVO, function (item) {
                                    if (tagid == item.tagid) {
                                        counttagduplicate = 1;
                                        showError(lblNm("tagid_exist"));
                                    }
                                });
                                if (counttagduplicate == 0) {
                        if ($(window).width() < 480) {
                            that.$("#div-do-record-tag-list").append(that.template_recordtagdetailsMobile(data));
                            that.comnMobFunc();
                            that.enableUsedTagByParts();
                        } else {
                            that.$("#div-do-record-tag-list").append(that.template_recordtagdetails(data));
                            that.enableUsedTagByParts();
                        }
                        that.updateJSON(data);
                        showError(lblNm("msg_invalid_tagid"));
                    }
                }
            }
         }
    },

            //Based on the used flag check parts sold btn enable and prepare the array for setid group
            enableUsedTagByParts: function () {
                var that = this;
                $(".li-do-record-tag input").on("change", function () {
                    $("#div-do-record-tag-list .ul-record-tag-list").each(function () {
                        if ($(this).children(".li-do-record-tag").find("input").is(":checked")) {
                            if($(window).width() < 480){
                               var slistid = $(this).find("#do-record-short-setid .rctag-setlist-pop").text();
                            }
                            else{
                                var slistid = $(this).find(".rctag-setlist-pop").text();
                            }
                            that.setGrpid.push(slistid);
                            that.setGrpid = _.uniq(that.setGrpid);
                        } else {
                            if($(window).width() < 480){
                               var slistid = $(this).find("#do-record-short-setid .rctag-setlist-pop").text();
                            }
                            else{
                                var slistid = $(this).find(".rctag-setlist-pop").text();
                            }
                            that.setGrpid = that.setGrpid.filter(function (a) {
                                return a !== slistid;
                            });
                            that.setGrpid = _.uniq(that.setGrpid);
                        }

                        if ($(".li-do-record-tag").find("input:checked").length > 0) {
                            $("#btn-add-parts-sold").removeClass("hide");
                        } else {
                            $("#btn-add-parts-sold").addClass("hide");
                        }
                    });
                    console.log("that.setGrpid>>" + that.setGrpid);
                });



                $("#btn-add-parts-sold").on("click", function (e) {
                    that.isCheckPart = 'Y';
                    that.setPopList(e);
                });


            },

            //On click 'parts sold btn' open the popup
            addPartSold: function (e) {
                this.isCheckPart = "";
                this.setPopList(e);
            },

            //Set list popup function
            setPopList: function (e) {
                var that = this;
                if (that.isCheckPart == 'Y') {
                    that.multiArr = [];
                    _.each(that.setGrpid,function(item){
                        var textPush = "'" + item + "'";
                        that.multiArr.push(textPush);
                    });
                    var setid = that.multiArr;
                } else {
                    var setid = "'" + $(e.currentTarget).text() + "'";
                }
                fnGetSetListDataBySetId(setid, function (data) {
//                    console.log(JSON.stringify(data));
                    data = _.each(data, function (item) {
                        item.titleset = item.setid + " " + item.setnm;
                    });
                    that.groupByDtSet = _.groupBy(data, function (dt) {
                        return dt.titleset;
                    });
                });
                showPopup();

                $("#div-crm-overlay-content-main").addClass("setPopWrap");
                $(".modal-footer").addClass("hide");
                $("#div-crm-overlay-content-container").find('.modal-title').text("Set List");
                getLoader("#div-crm-overlay-content-container .modal-body");
                setTimeout(function () {
                    $("#div-crm-overlay-content-container .modal-body").html(that.template_recordsetPartsPop(that.groupByDtSet));
                    $(".recPartsFilter input").on("keyup", function () {
                        var value = $(this).val().toLowerCase();
                        $(".recdo-toggle-content ul li").filter(function () {
                            $(this).closest(".recdo-toggle-content").removeClass("hide");
                            $(this).closest(".recdo-toggle-content").siblings(".recdo-toggle-head").find("i").removeClass("fa-plus");
                            $(this).closest(".recdo-toggle-content").siblings(".recdo-toggle-head").find("i").addClass("fa-minus");
                            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
                        });
                        
                        $(".recdo-toggle-content ul").filter(function () {
                            if($(this).text().toLowerCase().indexOf(value) == -1){
                                $(this).closest(".recdo-toggle-content").addClass("hide");
                                $(this).closest(".recdo-toggle-content").siblings(".recdo-toggle-head").find("i").addClass("fa-plus");
                                $(this).closest(".recdo-toggle-content").siblings(".recdo-toggle-head").find("i").removeClass("fa-minus");
                            }
                        });
                    });
                    $(".record-parts-list-qty .qty-plus-pu").on("click", function () {
                        var count = parseInt($(this).siblings("#div-qty-box").text());
                        var countEl = $(this).siblings("#div-qty-box");
                        count++;
                        countEl.text(count);
                        $(".btn-parts-sold").removeClass("hide");
                        $(this).closest("li").attr("qty", count);
                        that.partsSoldBtnEnable();
                        $("#div-show-num-keypad #div-keypad-close").trigger("click");
                    });

                    $(".record-parts-list-qty .qty-minus-pu").on("click", function () {
                        var count = parseInt($(this).siblings("#div-qty-box").text());
                        var countEl = $(this).siblings("#div-qty-box");
                        if (count > 0) {
                            count--;
                            countEl.text(count);
                        }
                        $(this).closest("li").attr("qty", count);
                        that.partsSoldBtnEnable();
                        $("#div-show-num-keypad #div-keypad-close").trigger("click");
                        if(count == 0){
                            that.passId = $(this).siblings("#div-qty-box").closest("li").attr("partid");
                            that.parent.JSONData.arrDORecordPartVO = _.filter(that.parent.JSONData.arrDORecordPartVO,function(partdtItem){
                                return partdtItem.partid !== that.passId;
                            });
                            that.parent.JSONData.arrDORecordappendData = _.filter(that.parent.JSONData.arrDORecordappendData,function(partApndItem){
                                return partApndItem.ID !== that.passId;
                            });
                            if ((that.parent.JSONData.orderid).replace("--", "") != "") {
                                that.parent.saveDO();
                            }
                            that.passId = "";
                            
                        }
                    });

                    $(".recdo-toggle-head").click(function () {
                        $(".recDoSetListItem").removeClass("openedBox");
                        $(this).closest(".recDoSetListItem").addClass("openedBox");
                        $(".recDoSetListItem").each(function () {
                            if (!($(this).hasClass('openedBox'))) {
                                $(this).find(".recdo-toggle-content").addClass("hide");
                                $(this).find(".recdo-toggle-head i").removeClass("fa-minus");
                                $(this).find(".recdo-toggle-head i").addClass("fa-plus");
                            }
                        });
                        $(this).siblings(".recdo-toggle-content").toggleClass("hide");
                        $(this).children("i").toggleClass("fa-plus");
                        $(this).children("i").toggleClass("fa-minus");
                    });

                    $(".expandAll-recTags").click(function () {
                        $(".recDoSetListItem").toggleClass("collapseBox");
                        $(".recDoSetListItem").each(function () {
                            if($(this).hasClass("collapseBox")){
                               $(this).find(".recdo-toggle-content").removeClass("hide");
                               $(this).find(".recdo-toggle-head i").removeClass("fa-plus");
                               $(this).find(".recdo-toggle-head i").addClass("fa-minus");
                            }
                            else{
                                $(this).find(".recdo-toggle-content").addClass("hide");
                                $(this).find(".recdo-toggle-head i").removeClass("fa-minus");
                                $(this).find(".recdo-toggle-head i").addClass("fa-plus");
                            }
                        });
                        $(this).children("i").toggleClass("fa-plus");
                        $(this).children("i").toggleClass("fa-minus");
                    });

                    $("#div-crm-overlay-content-main .close").on("click", function () {
                        $("#div-crm-overlay-content-main").removeClass("setPopWrap");
                        $("#div-show-num-keypad #div-keypad-close").trigger("click");
                        that.usedFlCheck(that.parent.JSONData.arrDORecordPartVO);
                    });

                    $("#add-parts-popup-btn").on("click", function () {
                        $(".recDoSetListItem").each(function () {
                            $(this).find("li").each(function () {
                                if (parseInt($(this).find("#div-qty-box").text()) > 0) {
                                    that.parent.JSONData.arrDORecordPartVO.push({
                                        "setid": $(this).attr("setid"),
                                        "partid": $(this).attr("partid"),
                                        "partnm": $(this).attr("partnm"),
                                        "qty": $(this).attr("qty"),
                                        "lotnum": $(this).attr("lotno")
                                    });
                                }
                            });
                        });
                        
                        
                        $("#div-crm-overlay-content-main .close").trigger("click");
                        
                        _.each(that.parent.JSONData.arrDORecordPartVO, function (item) {
                            var txtSearch = item.partid;
                            fnGetDOPartsforTagList("", txtSearch, function (data) {
                                that.parent.JSONData.arrDORecordappendData.push(data);
                            });
                        });

                        that.usedFlCheck(that.parent.JSONData.arrDORecordPartVO);
                        if ((that.parent.JSONData.orderid).replace("--", "") != "")
                            that.parent.saveDO();
                    });

                    $(".recDoSetListItem").each(function () {
                        $(".recdo-toggle-content ul li").each(function(){
                            var setPartId = $(this).attr("partid");
                            var elem = $(this).find("#div-qty-box");
                            that.parent.JSONData.arrDORecordPartVO = _.each(that.parent.JSONData.arrDORecordPartVO,function(item){
                                if(item.partid == setPartId){
                                    elem.text(item.qty);
                                    elem.closest("li").attr("qty",item.qty);
                                    elem.closest("li").attr("lotno",item.lotnum);
                                }
                            });
                        });
                    });

                    $(".div-do-keypad-values").on("click", function (e) {
                        that.showNumberKeypad(e);
                        $("#div-btn-done").on("click", function (e) {
                            setTimeout(function () {
                                that.partsSoldBtnEnable();
                            }, 50);
                        });
                    });
                    
                    if ($(".recDoSetListItem").length == 1) {
                        $(".recDoSetListItem").find(".recdo-toggle-content").removeClass("hide");
                        $(".recDoSetListItem").find(".recdo-toggle-head i").toggleClass("fa-plus");
                        $(".recDoSetListItem").find(".recdo-toggle-head i").toggleClass("fa-minus");
                        $(".recDoSetListItem").addClass("collapseBox");
                        if ($(".recDoSetListItem").hasClass("collapseBox")) {
                            $(".recDoSetListItem").closest(".recDoSetList").find(".expandAll-recTags i").removeClass("fa-plus");
                            $(".recDoSetListItem").closest(".recDoSetList").find(".expandAll-recTags i").addClass("fa-minus");
                        }
                    }
                    
                    

                }, 1000);

            },


            //Based on parts selected in setlist popup used flag enable
            usedFlCheck: function (data) {
                var that = this;
                that.arrUsedFl = _.uniq(data, function (item) {
                    return item.setid;
                });
                $(".ul-record-tag-list").find('.li-do-record-tag input').prop('checked', false);
                that.partsArr = _.groupBy(data, function(dt){
                    return dt.setid;
                });
                
                $("#btn-add-parts-sold").addClass("hide");
                that.setGrpid = [];
                _.each(that.arrUsedFl, function (item) {
                    $(".ul-record-tag-list").each(function () {
                        if ($(this).attr("data-setid") == item.setid) {
                            $(this).find('.li-do-record-tag input').prop('checked', true);
                            $("#btn-add-parts-sold").removeClass("hide");
                            if($(window).width() < 480){
                               var slistid = $(this).find("#do-record-short-setid .rctag-setlist-pop").text();
                            }
                            else{
                                var slistid = $(this).find(".rctag-setlist-pop").text();
                            }
                            that.setGrpid.push(slistid);
                            that.setGrpid = _.uniq(that.setGrpid);
                        }
                    });
                });
            },
            
            //Parts sold btn enable function
            partsSoldBtnEnable: function(){
                var that = this;
                that.btnSoldEnable = "";
                $(".recDoSetListItem").each(function () {
                    $(this).find(".recdo-toggle-content ul li").each(function () {
                        if (parseInt($(this).find("#div-qty-box").text()) > 0) {
                            that.btnSoldEnable = "Y";
                        }
                    });
                });
                if (that.btnSoldEnable == "Y") {
                    $("#add-parts-popup-btn").removeClass("hide");
                } else {
                    $("#add-parts-popup-btn").addClass("hide");
                }
            },

            //Displays the Number keypad 
            showNumberKeypad: function (event) {
                var that = this;
                this.parent.showNumberKeypad(event, function (val, target) {
                    var currSymbol = localStorage.getItem("cmpcurrsmb");
                    if (event.currentTarget.id == "div-qty-box") {
                        if (val == "0" || val == "00" || val == "000") {
                            $("#div-qty-box").text();
                            showMsgView("053");
                        } else {
                            $(target).text(val);
                            $(target).closest("li").attr("qty", val);
                        }
                    } else {
                        if ($("#div-qty-entry-text").text() != "")
                            $(target).text(currSymbol + "" + parseFloat(val).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                        $(event.currentTarget).attr("value", val);
                        $(event.currentTarget).closest("li").attr("qty", val);
                    }
                });
            },

            usedTagList: function (e) {
                var check = $(e.currentTarget).is(':checked');
                var that = this;
                var tagid = '';
                var setid = '';
                if ($(window).width() < 480) {
                    tagid = $(e.currentTarget).parent().parent().find("#li-do-records-tagid-list").text();
                } else {
                    tagid = $(e.currentTarget).parent().parent().find(".li-do-rec-do-tag-dtl").text();
                }
                if (tagid != 'MISSING') {
                    if (check == true) {
                        _.each(that.parent.JSONData.arrDORecordTagIDVO, function (obj) {
                            if (obj.tagid == tagid) {
                                obj.tagusagefl = 'Y'
                            }
                        });
                    } else {
                        _.each(that.parent.JSONData.arrDORecordTagIDVO, function (obj) {
                            if (obj.tagid == tagid) {
                                obj.tagusagefl = undefined
                            }
                        });

                    }
                } else {
                    if ($(window).width() < 480) {
                        setid = $(e.currentTarget).parent().parent().find("#li-do-records-setid").text();
                    } else {
                        setid = $(e.currentTarget).parent().parent().find(".li-do-rec-do-setid-dtl").text();
                    }
                    if (check == true) {
                        _.each(that.parent.JSONData.arrDORecordTagIDVO, function (obj) {
                            if (obj.setid == setid) {
                                obj.tagusagefl = 'Y'
                            }
                        });
                    } else {
                        _.each(that.parent.JSONData.arrDORecordTagIDVO, function (obj) {
                            if (obj.setid == setid) {
                                obj.tagusagefl = ''
                            }
                        });
                    }
                }
                if ((that.parent.JSONData.orderid).replace("--", "") != "") {
                    that.parent.saveDO();
                }

            },

            /*loadSetDetailsFromView(): Display set details for the missing tag
             */
            loadSetDetailsFromView: function (rowValue) {
                var that = this;
                var arrSelectedData = new Array();
                if (rowValue.length == undefined)
                    arrSelectedData.push(rowValue);
                else
                    arrSelectedData = rowValue;

                $('#setDetails #txt-billing-account').val('');
                //$('#setDetails #div-search-result-main').addClass('hide');
                $('#setDetails #div-common-search-x').addClass('hide');

                //If the multiple sets are selected from the Set dropdown then loop the set and render the data and save the details in the Local table
                this.loadSetDetails(0, arrSelectedData);
            },

            /*loadSetDetails(): function to list the multiple set details
             */
            loadSetDetails: function (iValue, data) {
                var that = this;
                if (iValue < data.length) {
                    var setid = data[iValue].ID;
                    var setName = data[iValue].Name;
                    var misingTagId = 'MISSING';

                    data[iValue].tagid = misingTagId;
                    data[iValue].setid = setid;
                    data[iValue].setnm = setName;

                    if (data[iValue].setid != undefined) {
                        $(".ul-do-do-tag-list-header").show();
                        if ($(window).width() < 480) {
                            that.$("#div-do-record-tag-list").append(this.template_recordtagdetailsMobile(data[iValue]));
                            that.comnMobFunc();
                            that.enableUsedTagByParts();
                        } else {
                            that.$("#div-do-record-tag-list").append(this.template_recordtagdetails(data[iValue]));
                            that.enableUsedTagByParts();
                        }
                    }
                    that.loadSetDetails(iValue + 1, data);
                    this.updateJSON(data[iValue]);
                }
            },

            loadTagDetailsFromView: function (rowValue) {
                var that = this;
                var arrSelectedData = new Array();
                if (rowValue.length == undefined)
                    arrSelectedData.push(rowValue);
                else
                    arrSelectedData = rowValue;
                
                //to show add favorite button when tag details render on screen
                 if (firedb != undefined) {
                    if (rowValue.length == undefined || rowValue.length > 0) {
                        $("#btn-add-favorite").removeClass("hide");
                    }
                 }

                $('#tagDetails #txt-billing-account').val('');
                $('#tagDetails #div-common-search-x').addClass('hide');

                //If the multiple tags are selected from the Tag dropdown then loop the tag and render the data and save the details in the Local table
                this.loadTagInfo(0, arrSelectedData);
            },

            /*loadTagInfo(): function to list the multiple Tag details
             */
            loadTagInfo: function (iValue, data) {

                var that = this;
                if (iValue < data.length) {
                    var tagid = data[iValue].Name;
                    var setArray = data[iValue].ID.split("*");
                    var setid = setArray[0];
                    var setname = setArray[1];
                    var counttagduplicate = 0;
                    data[iValue].tagid = tagid;
                    data[iValue].setid = setid;
                    data[iValue].setnm = setname;
                    if (data[iValue].tagid != undefined) {
                        this.tagidCheck = _.each(that.parent.JSONData.arrDORecordTagIDVO, function (item) {
                            if (tagid == item.tagid) {
                                counttagduplicate = 1;
                                 dhtmlx.message({
                                    title: "Tag Exists",
                                    type: "alert",
                                    text: lblNm("tagid_exist"),
                                     callback: function() {$('.dhtmlx-alert').addClass('hide');}
                                });
                            }
                        });
                        if (counttagduplicate == 0) {
                            $(".ul-do-do-tag-list-header").show();

                            if ($(window).width() < 480) {
                                that.$("#div-do-record-tag-list").append(that.template_recordtagdetailsMobile(data[iValue]));
                                that.comnMobFunc();
                                that.enableUsedTagByParts();
                            } else {
                                that.$("#div-do-record-tag-list").append(that.template_recordtagdetails(data[iValue]));
                                that.enableUsedTagByParts();
                            }
                            that.loadTagInfo(iValue + 1, data);
                            this.updateJSON(data[iValue]);
                        }else{
                            that.loadTagInfo(iValue + 1, data);
                        }
                    }
                }
            },

            comnMobFunc: function () {
                this.$("#div-do-record-tag-list").find(".ul-parts-used-list-mobile").each(function () {
                    console.log($(this).next().is('ul'));
                    if ($(this).next().is('ul')) {
                        $(this).addClass("ul-show-part");
                        $(this).children(".togglebx").children("img").addClass("onActive");

                    } else {
                        $(this).removeClass("ul-show-part");
                        $(this).children(".togglebx").children("img").removeClass("onActive");
                    }

                });

                $(".togglebx").unbind('click').bind('click', function () {
                    //                $(".togglebx").on("click", function () {
                    $(this).parent(".ul-parts-used-list-mobile").toggleClass('ul-show-part');
                    $(this).children("img").toggleClass("onActive");
                });

                $(".ul-parts-used-list-mobile").on('swipeleft', function (e, data) {
                    $(this).find(".delete-pu-span").addClass("onShowEl");
                    $(this).find(".copy-pu-span").removeClass("onShowEl");
                });

                $(".ul-parts-used-list-mobile").on('swiperight', function (e, data) {
                    $(this).find(".copy-pu-span").addClass("onShowEl");
                    $(this).find(".delete-pu-span").removeClass("onShowEl");
                });

                $(".ul-parts-used-list-mobile").on('tap', function (e, data) {
                    $(this).find(".delete-pu-span").removeClass("onShowEl");
                    $(this).find(".copy-pu-span").removeClass("onShowEl");
                });
            },

            /*Barcode Scan*/
            getBarcodeValue: function (event) {
                var that = this;
                var parent = $(event.currentTarget).parent();
                cordova.plugins.barcodeScanner.scan(
                    function (result) {
                        $(parent).find("#tagDetails input").val(result.text);
                        that.loadTagDetailsFromScan(result.text);
                    },
                    function (error) {
                        console.log("Scanning failed: " + error);
                    }
                );
            },
            /*End Barcode Scan*/

            /*get tagid details for online*/
            loadSetDetailsOfflineSavedTag: function (data) {
                var that = this;
                var token = localStorage.getItem("token");
                var userID = localStorage.getItem("userID");
                var searchTagId = data.tagid;
                var cmpid = localStorage.getItem("cmpid");
                var plantid = localStorage.getItem("plantid");
                var input = {
                    "token": token,
                    "userid": userID,
                    "tagid": searchTagId,
                    "cmpid": cmpid,
                    "plantid": plantid,
                };
                fnGetWebServerData("DeliveredOrder/getDOTagList/", "gmDORecordTagIDVO", input, function (dataTagDetails) {
                    if (dataTagDetails.setid != undefined && dataTagDetails.setid != '') {
                        that.editJsonOfflineToOnline(dataTagDetails);
                    } else {
                        that.parent.JSONData.arrDORecordTagIDVO = _.without(that.parent.JSONData.arrDORecordTagIDVO, _.findWhere(that.parent.JSONData.arrDORecordTagIDVO, {
                            "tagid": searchTagId
                        }));
                        if ((that.parent.JSONData.orderid).replace("--", "") != "") {
                            that.parent.saveDO();
                        }
                        $('#div-do-record-tag-list').empty();
                        console.log(that.parent.JSONData);
                        that.displayTagDetailsFromDraft(that.parent.JSONData.arrDORecordTagIDVO);
                    }
                });
            },

            /* Edit Setid When user comes from offline to online */
            editJsonOfflineToOnline: function (data) {
                var that = this;
                var jsondata = that.parent.JSONData;
                var passtagid = JSON.stringify(data.tagid);
                if (passtagid != undefined) {
                    jsondata.arrDORecordTagIDVO = _.without(jsondata.arrDORecordTagIDVO, _.findWhere(jsondata.arrDORecordTagIDVO, {
                        "tagid": data.tagid
                    }));
                    this.updateJSON(data);
                    $('#div-do-record-tag-list').empty();
                    this.displayTagDetailsFromDraft(jsondata.arrDORecordTagIDVO);
                    console.log('delete After' + JSON.stringify(jsondata));
                }

            },

            /* End Edit Setid When user comes from offline to onlie */

            /*Delete Record Tags Id*/

            deleteRecordTags: function (event) {
                var that = this;
                var parent = $(event.currentTarget).parent().parent();
                var tagid;
                var setlistid;
                if ($(window).width() < 480) {
                    tagid = $(parent).find("#li-do-records-tagid-list").text();
                    setlistid = $(event.currentTarget).parent().parent().find("#li-do-records-setid").text();
                } else {
                    tagid = $(parent).find(".li-do-rec-do-tag-dtl").text();
                    setlistid = $(event.currentTarget).parent().parent().find(".li-do-rec-do-setid-dtl").text();
                }
                var jsondata = that.parent.JSONData;
                if (tagid != 'MISSING') {
                    jsondata.arrDORecordTagIDVO = _.without(jsondata.arrDORecordTagIDVO, _.findWhere(jsondata.arrDORecordTagIDVO, {
                        "tagid": tagid
                    }));
                } else {
                    var setid = '';
                    if ($(window).width() < 480) {
                        setid = $(event.currentTarget).parent().parent().find("#li-do-records-setid").text();
                    } else {
                        setid = $(event.currentTarget).parent().parent().find(".li-do-rec-do-setid-dtl").text();
                    }
                    jsondata.arrDORecordTagIDVO = _.without(jsondata.arrDORecordTagIDVO, _.findWhere(jsondata.arrDORecordTagIDVO, {
                        "setid": setid
                    }));
                }
                this.parent.JSONData.arrDORecordTagIDVO = jsondata.arrDORecordTagIDVO;
                console.log('Delete After' + JSON.stringify(this.parent.JSONData));
                $('#div-do-record-tag-list').empty();
                this.displayTagDetailsFromDraft(jsondata.arrDORecordTagIDVO);
                
                /*Commenting the code : if we remove the tag from Record Tags screen then the parts already added in the Parts used tab should not remove*/
                /*for (var key in that.partsArr) {
                    if (that.partsArr.hasOwnProperty(key)) {
                        if(key == setlistid){
                            _.each(that.partsArr[key],function(dl){
                                that.parent.JSONData.arrDORecordPartVO = _.filter(that.parent.JSONData.arrDORecordPartVO, function (partdtItem) {
                                    return partdtItem.partid !== dl.partid;
                                });
                                that.parent.JSONData.arrDORecordappendData = _.filter(that.parent.JSONData.arrDORecordappendData, function (partApndItem) {
                                    return partApndItem.ID !== dl.partid;
                                });
                            });
                        }
                    }
                }*/
                that.usedFlCheck(that.parent.JSONData.arrDORecordPartVO);

                if ((this.parent.JSONData.orderid).replace("--", "") != "") {
                    this.parent.saveDO();
                    //delete the tag details for this do to reinsert the new tags
                    fnDelTagsForProcessImage(this.parent.JSONData.orderid);
                    //to save do tag details
                     if (that.parent.JSONData.arrDORecordTagIDVO.length > 0) {

                       _.each(that.parent.JSONData.arrDORecordTagIDVO, function (item) {
                      fnSaveTagsForProcessImage(that.parent.JSONData.orderid,item.tagid,item.setid,item.setnm,GM.Global.latitude,GM.Global.longitude,'');
                        });
                     }
                    
                }

            },
            
            /* Updates the JSON based on the current value in the screen 
             */
            updateJSON: function (data) {
                var that = this;
                if ($("#div-do-billing-acid").text().length != "0") {

                    /*Get the Location of the tag and set it into Global Variable and save the lattitude / longtitude details in the arrDORecordTagIDVO array
                     */

                    that.parent.JSONData.arrDORecordTagIDVO.push({
                        "tagid": data.tagid,
                        "setid": data.setid,
                        "setnm": data.setnm,
                        "tagusagefl": data.tagusagefl,
                        "latitude": GM.Global.latitude,
                        "longitude": GM.Global.longitude
                    });
                    if ((that.parent.JSONData.orderid).replace("--", "") != ""){
                        that.parent.saveDO();
                        fnSaveTagsForProcessImage(that.parent.JSONData.orderid,data.tagid,data.setid,data.setnm,GM.Global.latitude,GM.Global.longitude,'');
                    }
                        
                }
            },
            
            /*  Beginning of Code for the DO Tag Favorites 
                Author1: Karthik Somanathan
                Author2: Paveethra
             */
            
            /*Load Favorite Tag details from Firebase for the user*/
            fnLoadFavInfo: function () {
                var that = this;
                //Get the Tag Favorites Count for Logged in User
                that.fnGetFavoritesCountForUser(function (favCnt) {
                    that.firebaseFavCnt = favCnt;
                    //Show favorites icon based on the count
                    if (that.firebaseFavCnt > 0) {
                        $("#div-open-tag-favorite").removeClass("favtag-hide");

                    }
                });
            },

            /*Get Favorite Tags Count from Firebase for logged in user*/
            fnGetFavoritesCountForUser: function (callback) {
                var that = this;                
                var favDocIDs = this.parent.DOFavoriteMultipleDocumentID;
                _.each(favDocIDs, function(favDocumentId){
                    //get live data from firestore for the user
                    if (firedb != undefined) {
                        firedb.collection("TAG_FAVORITES").doc(favDocumentId.REPID.toString()).onSnapshot(function (doc) {
                            var favCount = 0;
                            if ((doc.exists) && (doc.data().arrFavVO != undefined)) {
                                favCount = doc.data().arrFavVO.length;
                                callback(favCount);
                            } else {
                                console.log("No such document!");
                                callback(favCount);
                            }
                        });
                    }
                });
            },
            
            /*
            * Get User Tag Favorites from Google Firebase
            * Author: karthik Somanathan
            */
            fnGetUserFavorites: function () {
                var that = this;
                that.fnShowPopupTagFavorites();
                var favDocIDs = this.parent.DOFavoriteMultipleDocumentID;
                console.log("favDocIDs==" + JSON.stringify(favDocIDs));
                this.favListArray = [];
                this.renderTemplateFL = 1;
                $("#div-crm-overlay-content-container2 .modal-body").empty();
                
                _.each(favDocIDs, function(favDocumentId){                
                    //get live data from Firebase
                    if (firedb != undefined) {
                        firedb.collection("TAG_FAVORITES").doc(favDocumentId.REPID.toString()).onSnapshot(function (doc) {
                            if (doc.exists) {
                                
                                //Set the current data for manipulation of the data
                                var localArr  = doc.data();
                                that.favListArray[favDocumentId.REPID.toString()] = doc.data();
                                $("#div-tag-fav-list").show();
                                
                                //Render the Popup screen 
                                if(that.renderTemplateFL == 1 && that.deleteRecordTagsFl == undefined) {                                        
                                    $("#div-crm-overlay-content-container2 .modal-body").append(that.template_tagFavorites(doc.data()));
                                }
                                $("#div-footer").addClass("hide");
                                
                                that.favTagToggleFunc();

                                // This function will add the tag to the record tag screen from Favorite Popup.
                                $("#div-crm-overlay-content-container2").find('.use-tag-favorite').unbind('click').bind('click', function (event) {
                                    that.fnAddTagFavoriteToRecordTagScreen(event);
                                });
                                
                                // This function will delete the selected Favorite from Favorite Popup.
                                $("#div-crm-overlay-content-container2").find('.span-delete-tag-favorites').unbind('click').bind('click', function (event) {
                                    that.fnDeleteTagFavoritesfromPopup(event,favDocumentId.REPID.toString());
                                });
                                
                                // This function will delete the selected tag for the favorite from Favorite Popup.
                                $("#div-crm-overlay-content-container2").find('.span-delete-single-tag-fav').unbind('click').bind('click', function (event) {
                                    that.fnDeleteSingleTagFromFavorites(event,favDocumentId.REPID.toString());
                                });
                                
                                // TO set flag
                                $("#div-crm-overlay-content-container2 .modal-dialog .close").unbind('click').bind('click', function (event) {
                                    that.fnclosePopup(event);
                                });
                                
                            } else {
                                console.log("No such document!");
                            }
                        });
                    }
                    });
                },
            
                fnclosePopup: function(event) {
                     this.deleteRecordTagsFl = undefined;
                     $('#div-crm-overlay-content-container2').removeClass('favtagShowPop');
                     $('#div-crm-overlay-content-container2').removeClass('readTextFromImagePopUp');
                     $("#div-crm-overlay-content-container2 .modal-body").empty();
                     hidePopup();
                },
                /*Show Tag Favorites and its details in Popup*/
                fnShowPopupTagFavorites: function () {
                    var that = this;
                    $('.div-overlay').show();
                    $("#div-crm-overlay-content-container2").addClass("favtagShowPop");
                    $('.favtagShowPop').find('.modal-body').empty();
                    $('.favtagShowPop').find('.modal-title').empty();
                    $(".favtagShowPop .modal-title").append('<div class="fav-tag-header">'+lblNm("tag_favorites")+'</div>');

                    $("#div-crm-overlay-content-container2").removeClass("hide");
                    $('#div-crm-overlay-content-container2 button[data-dismiss="modal"]').each(function () {
                        $(this).unbind('click').bind('click', function () {
                            $('.div-overlay').hide();
                            $('#div-crm-overlay-content-container2').addClass('hide');
                            $("#div-crm-overlay-content-container2").removeClass("favtagShowPop");
                        });
                    });

                },
            
                /*Add Tag(s) from the selected Favorites and its details to Record Tags Screen */
                fnAddTagFavoriteToRecordTagScreen: function (event) {
                    var that = this;
                    var i;
                    var tagid, setid, setnm, rowEl, currentRow, setdtl;
                    this.currentFavDocid = $(event.currentTarget).attr("favdocid");
                    this.currentFavName = $(event.currentTarget).attr("favname");
                    
                 _.each(this.favListArray, function (key, value) {
                    if (value == that.currentFavDocid) {
                        var tagdtls = _.findWhere(key.arrFavVO, {
                            favname: that.currentFavName
                        });

                        if (tagdtls != undefined) {
                            for (i = 0; i < tagdtls.favitems.length; i++) {
                                tagid = tagdtls.favitems[i].tagid;
                                setid = tagdtls.favitems[i].setid;
                                setnm = tagdtls.favitems[i].setnm;
                                setdtl = setid + '*' + setnm;

                                //Form the Row Element     
                                rowEl = $("<a id=" + setid * setnm + " class=item-searchlist name=" + tagid + " setid=" + setid + " setnm=" + setnm + " tagid=" + tagid + " ><div class='div-data hide'></div> <i class='fa fa-check'></i></a>");

                                currentRow = {
                                    "ID": setdtl,
                                    "Name": tagid,
                                    "setid": setid,
                                    "setnm": setnm,
                                    "Element": rowEl
                                };
                                //Load Tag Details 
                                that.loadTagDetailsFromView(currentRow);
                            }
                        }
                    }
                 });
                },
            
                //Remove the Entire Favorites
                fnDeleteTagFavoritesfromPopup: function (event, favDocumentId) {
                    var that = this;
                    this.deleteRecordTagsFl = 0;
                    //Get the Favorite name that you want to delete
                    this.currentFavDocid = $(event.currentTarget).attr("favdocid");
                    this.currentFavName = $(event.currentTarget).attr("favname");
                    this.currentCreatedBy = $(event.currentTarget).attr("favcreateby");
                    
                    if (this.currentCreatedBy == localStorage.getItem("userID"))
                        {
                        _.each(this.favListArray, function (key, value) {
                            if (value == that.currentFavDocid) {

                                //Get the array for the favorites
                                var tagdtls = _.findWhere(key.arrFavVO, {
                                    favname: that.currentFavName
                                });

                                if (firedb != undefined) {
                                    that.renderTemplateFL = 0;
                                    firedb.collection("TAG_FAVORITES").doc(that.currentFavDocid).update({
                                        "arrFavVO": firebase.firestore.FieldValue.arrayRemove(tagdtls)
                                    });

                                     //Remove entire selected area (favorites) from popup
                                     $(event.currentTarget).parent().parent().parent().remove();
                                }
                            }
                         });
                        }else{
                            dhtmlx.message({
                                    title: "Alert",
                                    type: "alert",
                                    text: "No access to delete this favorite!",
                                     callback: function() {$('.dhtmlx-alert').addClass('hide');}
                                });
                        }

                },
            
                // This function will delete the selected tag for the favorite from Favorite Popup.
                fnDeleteSingleTagFromFavorites: function (event,favDocumentId){
                     var that= this;
                     var arrFavoriteVO = [];
                     var arrFavInnerVO = [];
                     var arrfavitems = {};
                     var favNewObjJson = {};
                    this.deleteRecordTagsFl = 0;
                    //Get the Favorite name that you want to delete
                    this.currentFavDocid = $(event.currentTarget).parent().parent().parent().parent().parent().attr("favdocid");
                    this.currentFavName = $(event.currentTarget).parent().parent().parent().parent().parent().attr("favname"); 
                    this.currentCreatedBy = $(event.currentTarget).parent().parent().parent().parent().parent().attr("favcreateby");
                    var currentTagID = $(event.currentTarget).attr("tagid");
                    
                    if (this.currentCreatedBy == localStorage.getItem("userID"))
                        {
                            _.each(this.favListArray, function (key, value) {
                             if (value == that.currentFavDocid) {
                                //Get the whole array for the selected favorites from the selected tag
                                 var tagdtls = _.findWhere(key.arrFavVO, {
                                    favname: that.currentFavName
                                });

                                  //Remove the selected tag from the array
                                 var rmAttr = _.without(tagdtls.favitems, _.findWhere(tagdtls.favitems, {
                                        tagid: currentTagID
                                    })
                                  );

                                  favNewObjJson.favname = that.currentFavName;
                                  favNewObjJson.favcreateby = localStorage.getItem("userID");
                                  favNewObjJson.favcreatedate = new Date();
                                  favNewObjJson.favdocid = that.currentFavDocid;
                                  favNewObjJson.favitems = rmAttr;

                                 if (firedb != undefined) {
                                      that.renderTemplateFL = 0;
                                     //Remove the entire array first
                                      firedb.collection("TAG_FAVORITES").doc(that.currentFavDocid).update({
                                       "arrFavVO": firebase.firestore.FieldValue.arrayRemove(tagdtls)
                                     });

                                     //This will push the remaining tag as new array for the same favorite name
                                     firedb.collection("TAG_FAVORITES").doc(that.currentFavDocid).update({
                                       "arrFavVO": firebase.firestore.FieldValue.arrayUnion(favNewObjJson)
                                     });
                                    //Remove entire selected row from popup
                                      $(event.currentTarget).parent().parent().remove();
                                 }  
                             }
                            });
                        }else{
                            dhtmlx.message({
                                    title: "Alert",
                                    type: "alert",
                                    text: "No access to delete the Tag from this Favorite!",
                                     callback: function() {$('.dhtmlx-alert').addClass('hide');}
                                });
                        }
                    
                },
            
                /*Get Favorites Name entered by the user and get confirmed*/
                fnCreateFavorites: function () {
                    var that = this;
                    var favDocumentId = this.parent.DOFavoriteDocumentID;
                    var box = dhtmlx.modalbox({
                        type:"myFavBox",
                        title: "Enter Tag Favorite Name",
                        text: "<div class='dhtmlx_popup_text'><label>"+ lblNm("msg_fav_tags_add") +"</label></div><div></div><div><input id='enteredFavNm' class='inform' type='text'></div>   <div  class='dhtmlx_popup_controls'><div id='save_btn_dhtmlx'><input id='sav_button_dhtmx' type='button' class='dhtmlx_popup_button'  value='Save'></div><div id='cancel_btn_dhtmlx'><input  class='dhtmlx_popup_button dhtmlx_cancel_button' type='button' value='Cancel'></div></div>",
                        width: "350px",

                    });
                    dhtmlx.message.keyboard = false;
                    $(".dhtmlx_popup_text").find('#sav_button_dhtmx').unbind('click').bind('click', function (e) {

                        that.FavoriteName = $("#enteredFavNm").val().trim();
                        if (that.FavoriteName != '') {
                            dhtmlx.confirm({
                                title: "Tag Favorites",
                                ok: "Yes",
                                cancel: "No",
                                type: "confirm",
                                text: lblNm("msg_save_favorites"),
                                callback: function (result) {
                                    if (result) {
                                        //Once confirmed save Favorites in firebase
                                        that.fnCreateUserFavoritesInFirebase(event,favDocumentId);
                                    }

                                }
                            });
                        }
                        else
                            {
                                      dhtmlx.message({
                                        title: "Tag Favorites",
                                        type: "alert",
                                        text: lblNm("msg_valid_favorites")
                                    });
                            }

                    });

                },

                /*Function to save the Tag details on the firebase*/
                fnCreateUserFavoritesInFirebase: function (event,favDocumentId) {
                    var that = this;
                    var arrDOTag = [];
                    var favNewObj = {};
                    var arrFavoriteVO = [];
                    var arrFavInnerVO = [];
                    var favNewObjJson = {};
                    //var FavoriteName = "";
                    if (that.parent.JSONData.arrDORecordTagIDVO.length > 0) {
                        arrDOTag = that.parent.JSONData.arrDORecordTagIDVO;
                        console.log("arrDOTag ==" + arrDOTag[0]);
                        _.each(that.parent.JSONData.arrDORecordTagIDVO, function (item) {
                            if (item.tagid != 'MISSING') {
                                var arrfavitems = {}
                                arrfavitems.tagid = item.tagid;
                                arrfavitems.setid = item.setid;
                                arrfavitems.setnm = item.setnm;
                                arrFavInnerVO.push(arrfavitems);
                            }
                        });
                        favNewObjJson.favname = that.FavoriteName;
                        favNewObjJson.favcreateby = localStorage.getItem("userID");
                        favNewObjJson.favcreatedate = new Date();
                        favNewObjJson.favdocid = favDocumentId;
                        favNewObjJson.favitems = arrFavInnerVO;
                        arrFavoriteVO.push(favNewObjJson);

                        favNewObj.arrFavVO = arrFavoriteVO;

                        console.log("favNewObjJson Tag Favorite==>" + JSON.stringify(favNewObjJson));
                      if (firedb != undefined) {
                        //Add or Update Data
                        firedb.collection("TAG_FAVORITES").doc(favDocumentId).get().then(function (doc) {
                            if (doc.data() == undefined) {
                                //1st time save for the document
                                firedb.collection("TAG_FAVORITES").doc(favDocumentId).set(
                                    favNewObj, {
                                        merge: true
                                    }
                                ).then(function () {
                                    console.log("Favorite created");
                                });
                            } else {
                                var existFavNameCnt = 0;
                                that.gmTagFromFirebaseVO = doc.data();
                                var existFavNameList = that.gmTagFromFirebaseVO.arrFavVO;

                                for (var i = 0; i < existFavNameList.length; i++) {
                                    if (existFavNameList[i].favname.trim().toUpperCase() == that.FavoriteName.trim().toUpperCase()) {
                                        existFavNameCnt++;
                                        break;
                                    }
                                }
                                if (existFavNameCnt > 0) {
                                     //Replace the Tags for the entered favorites if it's already available in the google  firebase
                                     dhtmlx.confirm({
                                        title: "Tag Favorites",
                                        ok: "Yes",
                                        cancel: "No",
                                        type: "confirm",
                                        text:  "'" + that.FavoriteName + "'" + lblNm("msg_fav_name_exist"),
                                        callback: function (result) {
                                            if (result) {
                                                    var tagdtls;
                                                        //Get the array for the favorites
                                                        tagdtls = _.findWhere(that.gmTagFromFirebaseVO.arrFavVO, {
                                                            favname: that.FavoriteName
                                                        });

                                                        firedb.collection("TAG_FAVORITES").doc(favDocumentId).update({
                                                           "arrFavVO": firebase.firestore.FieldValue.arrayRemove(tagdtls)
                                                         });

                                                        firedb.collection("TAG_FAVORITES").doc(favDocumentId).update({
                                                            arrFavVO: firebase.firestore.FieldValue.arrayUnion(favNewObjJson)
                                                        });
                                                        
                                            }
                                        }
                                     });
                                    
                                } else {
                                    // save the Favorites for the  existing document
                                    firedb.collection("TAG_FAVORITES").doc(favDocumentId).update({
                                        arrFavVO: firebase.firestore.FieldValue.arrayUnion(favNewObjJson)
                                    });
                                    console.log("Favorite Pushed");
                                }
                            }
                        });
                     }
                    } else {
                        dhtmlx.message({
                            title: "Tag Favorite",
                            type: "alert",
                            text: lblNm("msg_fav_name_available")
                        });
                    }

                },

                favTagToggleFunc: function () {
                    $("#div-tag-fav-list").find(".ul-fav-used-list").each(function () {
                        if ($(this).next().is('ul')) {
                            $(this).removeClass("ul-show-fav");
                            $(this).children(".favtagtogglebx").children("img").removeClass("onActive");

                        } else {
                            $(this).addClass("ul-show-fav");
                            $(this).children(".favtagtogglebx").children("img").addClass("onActive");
                        }

                    });

                    $(".favtagtogglebx").unbind('click').bind('click', function () {
                        //                $(".togglebx").on("click", function () {
                        $(this).parent(".ul-fav-used-list").toggleClass('ul-show-fav');
                        $(this).children("img").toggleClass("onActive");
                    });
                },
            /*  End of Code for the DO Tag Favorites */
            
            /*
             * This function is used to process the chosen image to get tags 
             * Author: karthik Somanathan
             */
            /*  Start of Code for the DO Read Text From Image */
            fnProcessTagFromImage: function () {

                var that = this;
                //get the tag details which is processed from image for the DO id
                fnGetDOTagImageDtls(that.parent.JSONData.orderid, function (data) {
                    that.arrDORecTagFrmImgVO = data;
                    console.log("data fetch===" + JSON.stringify(data));
                    //FOR HBS FILE
                    if(data.length>0){
                            data = _.each(data, function (item) {
                                item['path']=that.localpath;
                            });
                        }
                    //get the unprocessed tag image details
                    fnGetDOUnprocessedTagImgDtls(that.parent.JSONData.orderid, function (unProcessedData) {
                        //get the processed tag count
                       fnDOReadableTagsFromImageCnt(that.parent.JSONData.orderid, function (processedDataCnt){
                        that.fnShowProcessTagPopup();

                        $("#div-crm-overlay-content-container2 .modal-body").empty();
                        $("#div-footer").addClass("hide");
                       
                        if ($(window).width() < 480) {
                             $("#div-crm-overlay-content-container2 .modal-body").html(that.template_tag_Images_list_mobile(data));    
                        }else{
                            $("#div-crm-overlay-content-container2 .modal-body").html(that.template_tag_Images_list(data));    
                        }
                        //remove offline message when online
                        if(!navigator.onLine){
                           $(".div-do-display-offline-image-msg").removeClass("hide");
                         }
                        
                        if (that.arrDORecTagFrmImgVO.length > 0) {
                            $(".div-do-img-delete-btn").fadeIn(2000);
                            $("#div-do-add-to-rec-tags-btn").fadeIn(2000);
                        } else {
                            $(".div-do-img-delete-btn").fadeOut(2000);
                            $("#div-do-add-to-rec-tags-btn").fadeOut(2000);
                        }


                        if (that.arrDORecTagFrmImgVO.length <= 0) {
                            $("#div-do-process-img-btn").fadeOut(2000);
                        } else if (unProcessedData.length > 0) {
                            if(navigator.onLine){
                                $("#div-do-process-img-btn").fadeIn(2000);
                            }else{
                                $("#div-do-process-img-btn").fadeOut(2000); 
                            }
                        } else if (unProcessedData.length <= 0) {
                            $("#div-do-process-img-btn").fadeOut(2000);
                            
                        }
                        if(processedDataCnt > 0 && processedDataCnt != undefined && processedDataCnt != ""){
                            $("#div-do-add-to-rec-tags-btn").fadeIn(2000);      
                        }else{
                            $("#div-do-add-to-rec-tags-btn").fadeOut(2000);
                        } 
                        $("#div-crm-overlay-content-container2 .modal-dialog .close").unbind('click').bind('click', function (event) {
                            that.fnAddProcessedTagFromImage('Y');
                            that.fnclosePopup(event);
                        });
                        //load image on popup which is chosen
                        $("#div-crm-overlay-content-container2").find('#input-tag-img').unbind('change').bind('change', function (event) {
                            that.fnLoadImageOnPopup(event);

                        });
                        $("#div-crm-overlay-content-container2 .modal-body").find("#div-do-tag-img-upld-check-main").unbind('click').bind('click', function (e) {

                            if ($("#div-crm-overlay-content-container2 .modal-body .ul-do-tag-img-details").find("#li-select-tag-image").find(".check-btn").prop('checked') == false)
                                $(".check-btn").prop('checked', true);
                            else
                                $(".check-btn").prop('checked', false);

                        });
                        //Remove image from popup
                        $("#div-crm-overlay-content-container2 .modal-body").find("#div-do-tag-img-delete-btn").unbind('click').bind('click', function () {
                            if($(".ul-do-tag-img-details").find("input:checked").length > 0){
                            $(".ul-do-tag-img-details").each(function () {
                                if ($(this).find("input:checked").length > 0) {
                                    var tagItemDtlId = $(this).attr("imgrow");
                                    fnDelDOTagImageDtls(tagItemDtlId, that.parent.JSONData.orderid);
                                    $(this).remove();
                                    that.fnProcessTagFromImage();
                                }
                            })
                            }else{
                                    dhtmlx.message({
                                    title: "Process Image",
                                    type: "alert",
                                    text: "Please select image to remove",
                                     callback: function() {$('.dhtmlx-alert').addClass('hide');}
                                });
                              }
                        });
                        // add all the processed tags to the record tags screen
                        $("#div-crm-overlay-content-container2 .modal-body").find(".div-do-add-to-rec-tags-btn").unbind('click').bind('click', function () {
                            that.fnAddProcessedTagFromImage('N');
                            that.fnclosePopup(event);
                        });
                        //process the image
                        $("#div-crm-overlay-content-container2 .modal-body").find(".div-do-process-img-btn").unbind('click').bind('click', function () {
                            that.loaderview = new LoaderView({
                                text: "Processing Image..."
                            });
                            fnReadTextFrmImageThruAPI(that.parent.JSONData.orderid, 'N');
                            setTimeout(function () {
                                that.fnProcessTagFromImage();
                                that.loaderview.close();
                            }, 20000);
                        });
                        //Zoom Image in iPad
                        $("#div-crm-overlay-content-container2 .modal-body .ul-do-tag-img-details").find("#li-read-tag-image-name").unbind('click').bind('click', function () {                     	
                        	if($(this).attr("src") != undefined){
                                openImageInFileOpener2Plugin($(this).attr("src"));
                            }
                        }); 
                           
                        //Zoom Image in mobile   
                       $("#div-crm-overlay-content-container2 .modal-body .ul-do-tag-img-details").find("#li-read-tag-image-name-mob").unbind('click').bind('click', function ()  {
                            if($(this).attr("src") != undefined){
                                openImageInFileOpener2Plugin($(this).attr("src"));
                            }  
                        }); 
                    });
                    });
                });

            },

                /*Show Process Image and its details in Popup*/
                fnShowProcessTagPopup: function () {
                    // var that = this;
                    $('.div-overlay').show();
                    $("#div-crm-overlay-content-container2").addClass("readTextFromImagePopUp");
                    $('.readTextFromImagePopUp').find('.modal-body').empty();
                    $('.readTextFromImagePopUp').find('.modal-title').empty();
                    $(".readTextFromImagePopUp .modal-title").html('Process Image :' + this.parent.JSONData.orderid);

                    $("#div-crm-overlay-content-container2").removeClass("hide");

                    $('#div-crm-overlay-content-container2 button[data-dismiss="modal"]').each(function () {
                        $(this).unbind('click').bind('click', function () {
                            $('.div-overlay').hide();
                            $('#div-crm-overlay-content-container2').addClass('hide');
                            $("#div-crm-overlay-content-container2").removeClass("hide");
                        });
                    });

                },
            /*Load the image which is chosen to process image*/
                 fnLoadImageOnPopup: function (event) {
                     console.log(" inside fnLoadImageOnPopup ====");
                     var that = this;
                     var file = event.target.files[0];
                     //var fileName = event.target.files[0].name;
                     var imgData = "";
                     var imageSavePath = "";
                     //console.log("fileName===" + fileName);

                     var reader = new FileReader();
                     //to get tag image detail count
                     fnGetTagImageDtlsCount(function (tagImgDtlsCount) {
                         console.log(tagImgDtlsCount);
                         var fileName = tagImgDtlsCount + "_" + event.target.files[0].name;
                         console.log("fileName===" + fileName);
                         fnCreateDirectoryForTagImage(function (fileDir) {
                             console.log("fileDir===" + fileDir);

                             reader.onload = function (e) {
                                 imgData = e.target.result.split(',')[1];
                                 
                                 fnWriteImage("Globus/DO/TAGIMAGE", fileName, imgData, "image/jpeg", function (data) {
                                     imageSavePath = data;
                                     console.log("imageSavePath===" + JSON.stringify(imageSavePath));
                                     //Function to save the Tag Image Path for the Order in in TBL_DO_TAG_IMG_DTL table.
                                     //Save only the DO/TAGIMAGE/Filename.jpg as a path
                                     imageSavePath = imageSavePath.substr(imageSavePath.indexOf("/DO")+1);
                                     fnSaveDOTagImageDetails(that.parent.JSONData.orderid, fileName, imageSavePath, "", "");
                                     //CAll the below function again to refresh the screen
                                     that.fnProcessTagFromImage();


                                 });
                             };
                             reader.readAsDataURL(file);
                         });
                     });
                 },

            /*Add the processed tags to the record tags screen*/
                fnAddProcessedTagFromImage: function(validatefl) {

                    var that = this;
                    var tagid, setid, setnm, rowEl, currentRow, setdtl;

                    //get DO processed tag image details
                    fnGetDOProcessedTagImgDtls(that.parent.JSONData.orderid, function (data) {     
                        for(var i = 0;i < data.length; i++) {
                            var processedTagIDs = data[i].PROCESSED_TAGS;   
                            var arrImgTagIDs = processedTagIDs.split(',');

                                        for(var j=0;j<arrImgTagIDs.length;j++){
                                               
                                                fnGetDOTagScan(arrImgTagIDs[j], function (tagdtls) {          
                                                            
                                                     if (tagdtls != "") {
                                                        for (var k = 0; k < tagdtls.length; k++) {
                                                            tagid = tagdtls[k].tagid;
                                                            setid = tagdtls[k].setid;
                                                            setnm = tagdtls[k].setnm;
                                                            setdtl = setid + '*' + setnm;

                                                            //Form the Row Element     
                                                            rowEl = $("<a id=" + setid * setnm + " class=item-searchlist name=" + tagid + " setid=" + setid + " setnm=" + setnm + " tagid=" + tagid + " ><div class='div-data hide'></div> <i class='fa fa-check'></i></a>");

                                                            currentRow = {
                                                                "ID": setdtl,
                                                                "Name": tagid,
                                                                "setid": setid,
                                                                "setnm": setnm,
                                                                "Element": rowEl
                                                            };
                                                            var tagdtls = _.findWhere(that.parent.JSONData.arrDORecordTagIDVO, {
                                                                    "tagid": tagid
                                                            });
                                                            if(validatefl != 'Y'){
                                                               //Load Tag Details 
                                                                that.loadTagDetailsFromView(currentRow);
                                                               }else if(tagdtls == undefined && validatefl == 'Y'){
                                                                 that.loadTagDetailsFromView(currentRow);     
                                                               }
                                                            
                                                        }

                                                     } 
                                                });                 
                                        }                   

                                }
                            });


                    },

            /*  End of Code for the DO Read Text From Image */

            });
    
    

        return GmDORecordTagsView;
    });