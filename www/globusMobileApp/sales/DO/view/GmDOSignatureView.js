/**********************************************************************************
 * File:        GmDOSignatureView.js
 * Description: GmDOSignatureView
 * Version:     1.0
 * Author:     	praja
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification','signatureview','saleitemmodel', 'saleitemcollection', 'saleitemlistview', 'loaderview', 'numberkeypadview'
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification, SignatureView, SaleItem, SaleItems, SaleItemListView, LoaderView, NumberKeypadView) {

	'use strict';
	var DOSignatureView = Backbone.View.extend({

		
		/* Load the templates */ 
		template: fnGetTemplate(URL_DO_Template, "GmDOMainSignature"),
		template_signature: fnGetTemplate(URL_DO_Template, "GmDOSignature"),
		template_partdetails: fnGetTemplate(URL_DO_Template, "GmDOViewSummaryPartDetails"),

		events : {
			"click #li-do-sign-display-area" : "openSignature",
			"click .li-do-sign-date label, .li-do-sign-date i" : "showDatepicker",
			"click .li-do-detai-sign-tab .fa-plus-circle" : "splitSignatureTab",
			"click .li-do-detai-sign-tab div" : "signatureTab",
			"click #btn-sign-submit" : "submitInfo",
			"click #btn-sign-email-price" : "emailWithPrice",
			"click #btn-sign-email" : "emailWithOutPrice",
			"click #btn-sign-view" : "viewSummary",
			"click #div-do-view-summary-close-icon" : "closeViewSummary",
			"change #txt-do-sign-date" : "getSignatureDate",
			"click #btn-fax-pdf-price" : "faxPdfWithPrice",
			"click #btn-fax-pdf" : "faxPdfWithOutPrice",
			"keyup input" : "updateJSON"
		},
		
		initialize: function(options) {
			this.pdfCreatedAttempt = 0;		//For LOGO Inconsistency
			this.parent = options.parent;
			this.render();
		},

		// Render the initial contents when the Catalog View is instantiated
		render: function () {
			var that = this;
			this.$el.html(this.template());


			var date = new Date();
			var dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
			this.$("#txt-do-sign-date").val(mm+"/"+dd+"/"+yyyy);
			this.$("#label-do-signature-date").html(mm+"/"+dd+"/"+yyyy);
			this.signatureTab();
            fnProcessTagImageForPendingSyncDO(); //To Process the DO TAG Images for DO's that has un processed Images if they are in online
			return this;
		},
		
		signatureTab: function (event) {
			this.$("#div-do-signature-main .selected-tab").removeClass("selected-tab")
			if(event!=undefined)
				var signtabid = $(event.currentTarget).parent().attr("id");			
			else
				var signtabid = "li-do-detai-sign-tab1";
			this.$("#"+signtabid).find("div").addClass("selected-tab");
			
			switch(signtabid) {
				case "li-do-detai-sign-tab1":
					if(this.$("#ul-do-detai-sign-tab1").html().trim()=="")
						this.$("#ul-do-detai-sign-tab1").html(this.template_signature({data : this.parent.signaturesrc[0]}));					
					this.$(".ul-do-sign-label-header").hide();
					this.$("#ul-do-detai-sign-tab1").show();
					this.targetsignaturetab = this.$("#ul-do-detai-sign-tab1");
					break;
				case "li-do-detai-sign-tab2":
					if(this.$("#ul-do-detai-sign-tab2").html().trim()=="")
						this.$("#ul-do-detai-sign-tab2").html(this.template_signature({data : this.parent.signaturesrc[1]}));
					this.$(".ul-do-sign-label-header").hide();
					this.$("#ul-do-detai-sign-tab2").show();
					this.targetsignaturetab = this.$("#ul-do-detai-sign-tab2");
					break;
				case "li-do-detai-sign-tab3":
					if(this.$("#ul-do-detai-sign-tab3").html().trim()=="")
						this.$("#ul-do-detai-sign-tab3").html(this.template_signature({data : this.parent.signaturesrc[2]}));
					this.$(".ul-do-sign-label-header").hide();
					this.$("#ul-do-detai-sign-tab3").show();
					this.targetsignaturetab = this.$("#ul-do-detai-sign-tab3");
					break;

			}
			
		},

		openSignature: function() {
			var that = this;
			this.signatureView = new SignatureView({"callback" : function(img) {
				that.populateSignature(img);
			}});
			this.$("#div-do-signature-area").show().html(this.signatureView.el);
		},

		populateSignature: function(img) {
			this.$("#div-do-signature-area").hide();
			this.signatureView.close();
			this.targetsignaturetab.find("#img-do-sign").show().attr("src",img);
			if(img !="") {
				//console.log("populate");
				this.parent.signaturesrc = img;
				this.parent.signatureMode = true;
				this.parent.saveDO();
			}
//			this.$("#img-do-sign").show().attr("src",img);
		},
		
		showDatepicker: function (event) {
			$(".li-do-sign-date input").datepicker( {maxDate: 0,  duration: 'fast'});
			var visible = $('#ui-datepicker-div').is(':visible');
			   //console.log(visible);
			$(".li-do-sign-date input").datepicker(visible? 'hide' : 'show');	
			
			event.stopPropagation();
			$(window).click(function (event) {
			    if(document.getElementById("ui-datepicker-div").style.display != "none") {
			    	
			    	var elem = event.target;
			       if(!($(elem).hasClass("ui-datepicker-prev") || $(elem).hasClass("ui-datepicker-next") || $(elem).parent().hasClass("ui-datepicker-prev") || $(elem).parent().hasClass("ui-datepicker-next"))) {
			    	   var visible = $('#ui-datepicker-div').is(':visible');
			    	   $(".li-do-sign-date input").datepicker(visible? 'hide' : 'show');	
//			    	   $('#ui-datepicker-div').hide();
			       }
			       }
			   });
		},
		
		getSignatureDate: function () {
			this.$("#label-do-signature-date").html($("#txt-do-sign-date").val());
		},
		
		splitSignatureTab: function (event) {
			var signaturetablength = this.$(".li-do-detai-sign-tab").length;
			var addsignaturecount = parseInt(signaturetablength)+1;
			var id = "li-do-detai-sign-tab"+addsignaturecount;
			$(event.currentTarget).parent().find("i").remove();
			if( parseInt(signaturetablength) == 2) {
				this.$("#ul-do-detai-sign-tab").append("<li class='li-do-detai-sign-tab' id="+id+"><div>"+lblNm("signature")+" "+addsignaturecount+"</div></li>");
			}
			
			else {
				this.$("#ul-do-detai-sign-tab").append("<li class='li-do-detai-sign-tab' id="+id+"><div>"+lblNm("signature")+" "+addsignaturecount+"</div> <i class='fa  fa-plus-circle fa-lg'></i> </li>");
			}
		},

		validate: function () {
		    var PartTypes = _.contains(_.uniq(_.pluck(this.parent.JSONParts, "type")), 'C');
		    var repl = _.contains(_.pluck(this.parent.JSONParts, "repl"), true);
            var nullPartTypes =  _.where(this.parent.JSONParts, {"type": ''});
		    var lotNoValidation = false;

		    var igValidate = true;
		    if (this.parent.JSONData.ordertype == "2532") {
		        igValidate = false;
		    }

		    fnGetRuleValueByCompany("DO_APP_LOT_FL", "DO_ORDER", localStorage.getItem("cmpid"), function (data) {
		        if (data.length == 0) {
		            $(".txt-lot-no-box").each(function () {
		                if ($(this).css("display") != "none" && this.value == "")
		                    lotNoValidation = true;
		            });
		        }
		    });

		    if (this.parent.JSONData.orderid == "") {
		        fnShowStatus(lblNm("msg_account_details"));
		        return false;
		    } else if (this.parent.JSONParts.length == 0) {
		        fnShowStatus(lblNm("msg_used_part"));
		        return false;
		    } else if (lotNoValidation) {
		        fnShowStatus(lblNm("msg_provide_lot_no"));
		        return false;
		    } else if (!this.parent.signatureMode) {
		        fnShowStatus(lblNm("msg_provide_signature"));
		        return false;
		    }
            else if (nullPartTypes.length != 0)  {
                var nullPartList = _.pluck(nullPartTypes, 'ID');
                fnShowStatus("Please select part type for below part(s) "+ nullPartList);
                return false;
            }
            else if ((PartTypes || repl) && (!this.parent.JSONData.gmTxnShipInfoVO.length)) {
		        if (igValidate) {
		            fnShowStatus(lblNm("msg_ship_details"));
		            return false;
		        }
		        else {
		        	return true;
		        }

		    }
            else {
		        fnShowStatus(lblNm("msg_generating_pdf_c"));
		        return true;
		    }

		},
		
		submitInfo: function(callback) {

			var that=this;
            var surgeonName = "";
            var lastchar = "";

			try{
                if(this.validate()) {
                that.template_PDFWithOutPrice = fnGetTemplate(URL_DO_Template, "GmDOPDFLayoutWithOutPrice"+localStorage.getItem("cmpid"));
                that.template_PDF = fnGetTemplate(URL_DO_Template, "GmDOPDFLayout"+localStorage.getItem("cmpid"));
				if(that.parent.otherInfo.HCAAccount) {
					that.template_PDF = fnGetTemplate(URL_DO_Template, "GmDOHCALayout");
					that.template_PDFWithOutPrice = fnGetTemplate(URL_DO_Template, "GmDOHCALayoutWithOutPrice");
				}

				this.loaderview = new LoaderView({text : lblNm("msg_generating_pdf_c")});
				this.parent.signaturesubmit = true;
				//console.log("ok  clicked");
				var JSONDataPDF = {};
				var todaydate = new Date();
				var dd = ("0" + todaydate.getDate()).slice(-2), mm = ("0" + (todaydate.getMonth() + 1)).slice(-2), yyyy = todaydate.getFullYear();
				//JSONDataPDF.generateddate = mm+"/"+dd+"/"+yyyy;
                var dateformat = mm+"/"+dd+"/"+yyyy;
                JSONDataPDF.generateddate = formattedDate(dateformat,localStorage.getItem("cmpdfmt"));
                
                if(that.parent.otherInfo.repContact!=undefined) {
                    JSONDataPDF.email = that.parent.otherInfo.repContact.email;
                    JSONDataPDF.phone = that.parent.otherInfo.repContact.phone;
                }
                JSONDataPDF.FirstName = that.parent.repName.split(" ")[0];
				JSONDataPDF.LastName = that.parent.repName.split(" ")[1];
				JSONDataPDF.RepID = that.parent.JSONData.repid;
                
				JSONDataPDF.AccountName = $("#div-do-billing-acname").text().replace(/\d+/g, '');
                //Passing the company Address .
                var companyaddress =localStorage.getItem("strcmpadress");
                JSONDataPDF.strcmpadress = companyaddress.replace(/\N. /, ", ").replace(/\/ /g, "<br>").replace(/\//g, "<br>"); 
                //Italy companyAddress.
                //for each country except Germany.
                //for Germany only.
				JSONDataPDF.strcompfax = localStorage.getItem("strcompfax");
				var companyname =localStorage.getItem("strcompname");  
				JSONDataPDF.strcompname = companyname.replace(/\//g, "\n").replace(/[()]/g, '');
                //for Germany
                //removing () from (pty) for Australia & south africa. 
				JSONDataPDF.strcompphn = localStorage.getItem("strcompphn");
                if(localStorage.getItem("strremitadress") != "" )
				    JSONDataPDF.strremitadress = localStorage.getItem("strremitadress");
				JSONDataPDF.AccountID = that.parent.JSONData.acctid;
				JSONDataPDF.DOID = that.parent.JSONData.orderid;
				JSONDataPDF.src = $("#img-do-sign").attr("src");
       			var imgSrc = $("#div-do-billing-info-additional").css("background-image").replace('url(','').replace(')','');
				console.log("typeof image URL device.version>>>>>>>>>>" +  device.version);
				
                if(parseInt(device.version) >= 12)
					JSONDataPDF.notesrc = $.parseJSON(imgSrc); 
				else
					JSONDataPDF.notesrc =  imgSrc.replace(/"/g, '');

				JSONDataPDF.printedname = $("#txt-do-sign-printed-name").val();
				JSONDataPDF.title = $("#txt-do-sign-title").val();
				JSONDataPDF.date = $("#txt-do-sign-date").val();
				JSONDataPDF.totalprice = $("#div-do-parts-footer-title").find("span").text();
				JSONDataPDF.PurchaseOrder = $("#div-do-billing-info-customer-input").val();
                if (that.parent.JSONData.arrDoNPIDetailVO != undefined && that.parent.JSONData.arrDoNPIDetailVO != ""){
                      _.each(that.parent.JSONData.arrDoNPIDetailVO, function (data) {
                            surgeonName = surgeonName+data.name;
                            surgeonName = surgeonName.trim() +",";
                            surgeonName = surgeonName.replace(/[0-9]/g, ''); //Removing Numbers from the surgeon name
                            surgeonName = surgeonName.replace(/-/g, ''); //Removing hyphens from the surgeon name
                      });
                    lastchar = surgeonName[surgeonName.length -1];
                    if(lastchar == ","){
                        surgeonName = surgeonName.replace(/.$/,'');
                    }
                    surgeonName = surgeonName.replace(/[,]+/g,','); //Replace commas if one or more matches available
                }    
                JSONDataPDF.SurgeonName = surgeonName;
                if(that.parent.JSONData.accDivCompanyFl== "Y"){
                        JSONDataPDF.SurgeryLevel = $("#div-do-billing-info-div-surglvl-list").find(".div-dropdown-text").text();
                        JSONDataPDF.SurgeryType = $("#div-do-billing-info-div-surgtyp-list").find(".div-dropdown-text").text();     
                   } else{
                        JSONDataPDF.SurgeryLevel = $("#div-do-billing-info-surgerylevel-list").find(".div-dropdown-text").text();
                        JSONDataPDF.SurgeryType = $("#div-do-billing-info-surgerytype-list").find(".div-dropdown-text").text();
                   }
                JSONDataPDF.SurgeryLevel = (JSONDataPDF.SurgeryLevel=="Choose One")?"":JSONDataPDF.SurgeryLevel;
                JSONDataPDF.SurgeryType = (JSONDataPDF.SurgeryType=="Choose One")?"":JSONDataPDF.SurgeryType;
                JSONDataPDF.EGPSType = $("#div-do-billing-info-egpuse-list").find(".div-dropdown-text").text();
				JSONDataPDF.EGPSType = (JSONDataPDF.EGPSType=="Choose One")?"":JSONDataPDF.EGPSType;                                
				JSONDataPDF.SurgeryDate = formattedDate($("#label-do-billing-info-surgery-date").text(),localStorage.getItem("cmpdfmt")) ;
				JSONDataPDF.comments = that.parent.JSONData.ordercomments;
				JSONDataPDF.logo = "data:image/gif;base64,R0lGODlhUATgAYcAAAAAEQABOAQUUAAQawgkVgE6dyc6WCM7agBCXgVBeC1BXi5MdEZZd05kfGdwfgATggA+ggJNjgFcogVkmQFtry5XhzJplS52qgJ7wyN7wUdchVBrjUx5pmh6kml8og2Eui+Itz2ivRGHyAuP4S2Uzy6c4zih2Dek5F+KlE+MtFWium6Dmm+PrXKkvEqZykOf4VCm11iu42ubxG+v0mu45XXB3HfG7JovIrQREb0bIq4tHqo1HrckC7ooGrY1BbkyG6spKqw3K6s6MrwqJbktM7k1Kbo4NJxBPqpCObtBKblDObdQPaNJRqxdXbtKRrxIUbpUSLpZVb1mTbxlW61raaxvc691crxoY79vcLxzarp6c8kMAcYIGckZAccaGdEBAdUCHdIXANcZF8cEIMoaI9YAI9QZJcsjCscpGsUzHNMnDtMsF9Q1HMcpJsgrMcw5KMY3MtYrKNUqMdU2Ktc1MuAAG+AuH+MtKuMtM+EyKeI0Msg6Q9I8Q9FBHsZAKMRCONRBKdRDN8VIRslLUcZUScZZVdVJRtdMUtVUS9JbWsxcZMVkWstwW9diWMhnZMttcclzacp5dNRpZ9N1atR6dOBXS+VlXeRya6x/hM58gtl9guB9gLWBfs+Fb8qDetSFeuCMf4aOmYSZspOmuLuPjbaOkr2cn6OsuKGxu4GcwY6yzoq746KtxaS2x6a91rK8ybS90qa+4pLF25DS75fm+6vDy6jD2KjS3LXDyrfI1rnT263W7q7n+8qKhs+LkMuSicyal9SLh9mUjNqZk9ieo8+rmt+gj9ukmsmoqtuno9yyqdy5tOOKieuPkOKXjeSal/OMgvKbm+Wlm+qqp+mus+i0quq4tPKpp/S3q/O6te2+wfS/wd/GveTBq+rHvfTEufjYv8DEy8PL2MTT2tnIxdHZ28LO4sfa5sTc9NPb59Dg3dXs9erLxevL1OvSyO3U0vXIxPrN0PfVy/jZ1Pjc4fjiy/vl2v3y3eLp7OLs8+ny7en4+/zr5/nt8/726/7+/iH5BAEAAP8ALAAAAABQBOABAAj/AP8JHEiwoMGDCBMqXMiwocOHECNKnEixosWLGDNq3Mixo8ePGfcJFMnR30GT/lLymzcvHjxt2pYtI/Ys2KdJjhYVcqKk5x8lSYLCCbTnDRw4bwLRmcO0KdM+c940zZNnTlU2TrHGIdOma5AiQYQIQXLkCJMmVKxwImVqmb5+IOPKnUu3rt27ePPq3cu3r9+/gAMLHky4sFyRJB0i/pe4oMl/KP3xu2ePJbxqyYb18qQlyyJDgt6kQbOlS5cza9rQcQOHjmvXcZbOeU2baR7ZUd/ojorV9pw4efS4lkOGDJ9EkzYxa/Zs2rRs2eC9o0fPHj1+1/n14/fYsPfv4MOL/x9Pvrz58+jTq1+/d3HHxykhS54nz9syZcOYUUo0iMiQIW608UYbaKDRBhsIJshGHwhCNdttVUVIVYRLLcVGVVJZRdVSu9mBoW4DtuHGHpQQY40283AH2YoorThQdyY1xt6MNNZo44045qjjjjz26GNCKN3Djz3vfHNNM5tQ4kghgsDRhhdmtBGHHHI4NRUgVTEVR1NYIRhVblJB1ccbWYL4BlaAYJWhVVbRERxTdLQxhBFTeHIMPELC9+OefPbp55+ABirooIT6FZk/9rhkTTXObLLIH0QUMURXUsYRW2xz6NGmlV3OcSEdeIBKh3C3WQnIl1KlCsgbfrwxJlZ59P+2aoV5mGFgEUpMkow39sTXXaHABivssMQWa+yxyE4ko0Db1VNPN8X8EskigtBhBhhgmHHHbFEB4kerVjaVJpvhYiUcbbW56Ru3TO32qoZUNYXUDz0IAkkw08CT4q/J9uvvvwAHLPDABH/UYkomDamNNcn4IklRlBoYLlMXVpzHmCA6uKabS1VVYW6rmrmbx7dV6JpvEc52MqV/NCJMN/L0qidByxZs880456zzzjyHR1JklLmTjTSeZMHkk2aAmscdwcXbFFQZdhnrhFmCKbJuSO3m6dZcNpXhGyY/SDUdR/0niCTBWDPPPQjHyFjPcMct99x01203QvN9U80wlBD/UgQaPaBxZlYpXzXx4YfjFq7ivnGMoZpO4RZhgTpAEUw14HDHb0E13+3556CHLvro6CVM5DTBRBLFH0+2UaXTni644NVfIg6nmZHHW7iGskmVbrldzwYHGkZAIsw39bDtIunMN+/889BHz5BImxuUGKLwWDPMJIKcUSAatdsu/vjk284gnOrO4SCchrNhoBGS7Kqi9PTXb//9+CPbeedACjTfNtKIxBUE4QUvbKl8CEygAinWlN7pxlvoI1sP/AAJY7hDefnLoAY3yMEOsmcf1VMIXF6UEnvAAxvDkAQfckAGN9DhdbBboAxn6BSnaa1dbwDEatpAhEUMQxuaC6EH/4dIxCIa8YgecU9DJMMPd0zjE4z4AxrUsCU6KIWGWMxiA0+WwwzhAQ+acgMaCJE2ejymHyThHxLXyMY2uvGNjvnHPeBBjWEswgiTagNV7ECxM+VwTGPSoiARCIcv9SFeeJBDlBohDXkIEY6QjKQkJym9xiBKHtgIxhS6Z0CwsSmGgwzlAuFwqjzwMSpDGEQwtqG5txVkhJSMpSxnSUuAqbEf9tDGMSZhBB6wYQ1YWV+bSjUxYorymBNLHxqKIIVjgMNtC2lRLadJzWpak09Bkkc0KBEFJ7UhU3ao2NZyiJSjtMtKjEOmOkvWBiNEohoyU6N7HnnNetrznvgMTAmtcf8MSRShUsCDnW78kIRWpQpVN1QnMpfSAyNoAohnRMg+Jvo2NebzohjNqEYxgp1qBKMQbTjDlgC5pqiQrTYo0xC5mHIqharzDEoAxTf4MRBLLm+jOM2pTjMKI4agJB5P/MOkPFmVMa0qcr7zJMpS5tIFprOBgdCN09Dwh2GkaKdYzapW83m9g8DSf/eQxzSwEIgtnKGpaC3fU5nSmkxV5QyCEIY8vrrVutr1rrVMDBoRBo9kPCJAbviNDtNK2MPdsGoOclMe1lAEYYADg3iNrGQni8SJWvIe7qgGJP7gBdWstbCgPefhqIIGOHzCHdCkrGpXy9rQ0dMx/phHNTqRhB//SEkPH2ppaHe7rvBpCQ2U6EZKKNra4hr3uHYjrnzmkY1MPGEI4BvVhKLiB95aN4ZGAdsQGAHPkSD3u+ANb8AWcz1/3OMbzDgEGcwgvqpZd7d5iMO59oArafSKIHRNz+b49Vrx+ve/ADavPKQBCSex64DU1Y1i37vb3egBD3yQwx56MY9ffbW/grGoQjAM4A57OLL9QFQ1QME6z+ZmTKXCHYN5m0OV5UASrLRwTT9M4xrbWD2SecczCiFFOVgxal30mNdWzFtNzaEIS0jGfROi4cGQlzGWjXJ+b0zlKmPVJPfQxi+UMIQBzeGoR4VXDcnVGyKrU3FLa0MkvBGf9SAm/x/pEEctXoGKU5xiFKLIMwtEsec9i2IUdj7FK16hC3Sso8lWTrSi3fizsE5jEcODk5lB695w+Q63tRtCFK4xP0TrhV/5WIc6zBGLVLBgAxpgwAIOQAACCEAAAxgArGUta1i/mtaxHkABFlCBDWxgBaJwhS7OsY51cI6E0ly0spfdvO40ponCgMIPwOe0z046lJWeisqspIc3JCES8uj0X2ZGs33kQxeoGMUKNECAWhOgAAc4gAY2kIIWyGAGMlDFLUw9jnGIw9/kGMcrRqGKUbBgBR1IdQIK8IACtDsBC9gAoMNxaOsx++IYH93PEPUNTzihCHD4Q29Leu20lvQ1nv86ZB7a8ARlsI3DGakewqwH51yIYgMLQAABAjCABGwgFKIQdKH3sQ507GIWM0iBC1ZBC16gQ7k1nWix0WEOVcwA3xWIQAUgEAEIPODdCmgAC1pBbOUq0dMZT7vaB1W969mjGpP42xt8nL4Glvy6opIKgnhAiXf4z7t1CaGz93GOVoiiAQIIwLsToAEZCPsc+UjMPtDRChWAYAIToIDmKXABFdiAF7zYRSt0EeVbqEIFF8A85m1QgwlIgAISiL0EIrAACBRA5wTQgAdGkYvGpHHtwA9+sWBJ3NheYxFtSEMOm2a4uxP5TVFJwx+OYY+o82Vz6zDH4RfwagJYgAWjcIX/oQ1dbKLnAx3oUAUINo+B9rt/8yCYAS2QjoJdEN0WLUhBCEBAgtTTQgUR4Hqb93qxNwG2x3gesGcdsAKwgA9oJ3wQGIE3wj/+MGCO4Dq9M13qUzvW5nxnRi5FcAXsAHMxtxDmZguiUAHdtwAr4AplJxJoRHQ1MAM1UAO3UGygNwusBwMgkAHtRwEYQAEg4AIzMAugF3qGBnq0sITzZ4SzQAMuAAIXUICyNwFLJwu74HSxwAE6dwArgArpMGVxJIFkWIbq0Rj9MA/O4AQ90FZ15zVa84YeiFYZMiZD4Av0QBhEBwseUAGuZgAdMArmAHUkARe8AALtNwOHtg68QIOz/8CE80cD92YDSzgLsoBvNcALs8ACF3ABIPCJSqcKmjgLj0gLq5B0nlgDlVgDKWABrpd6EbBwBBBxrJAOy5JsZpiLuvgdlgUZ8TAMgyAGQ8Y+HAIiU5Ftc3hmR4UGSqAMNEUQJHgRkrcP4iAKGvBqEdcK5XCLUOYP1MgCFBACs1BssyADH7B5QkiEu8CIvLAOtAADU7h5LvCIMYCOGDABKkALmAcCKaACNWCETccLqmABERCAsQd7sDd7XbdwBeABr+CA+HVTuziRFNkXCCMPw+AEroNp6rMmmnJQychiOZQGjjCCfpFG6cAKDXAAAXAAHoAKT/d7roQY6IB0KtACt/9AdLvQAiHgAi5AAiSQAet3AbIwfzIAA6tQiuW4fpqnirOwfudIAfKXeZgXASDwiJa4hEmnegg5ewpZkAuncwsgCoP4gBV5lmipESkhD2vYA77zMVzTGx8jhyF5TEylG2fQCRUmkXmxDzaXAAOQe6pwDhRlWYtYbIxYdOuwC0wIeoyYhZBYiY8IejOgAinQiURIifM3A5cHAkvoApung5uXAZk4CymAeZ3nhK1oASDAAUonAykgAyrAAinAARZQAQvgcB3ACsaWlr75mx6REvOQkd/EQGATNcM4l3VJQxvTW4wDB0ZgDNXHl3Zhbq6wAQggAAfAAuO4iPiQD9+ZD6H/Ro4y4JNRmAIpwH+fyI/7l5496QKexwtNWIryOQszeHWzsAtHh3qqCAOaZ5+ZBwOfOQER8HroWYRHGHqPeAuk2KCkmIW7EAuqwAIdIHGECZwYmqH9gxDzIA1Q0GUqtZyFNTIcczJ0YBSlFBV7YA3POBf7sw6tsAECQAAVIAOxgINEtw/6YHbrMAMU8AEfIAIjIAJESqRBeqRFigFA+gE2YANR+QGfSIQ1IAsA+aDoIJ81EAuNSAFIdwGUWAOY+Y+TOQurUAMysAqh14pV6XoT0IkgwAIykApUGnquMAqjEIYzpqF6qqEg9A/0MA2FoAN/UEgrJaK71Tsf4zttoAjf/xCNFwEXMYIPqMAAircBrBCTaORKNLMOsoCIQyoCQVqkItB+o+p+7Rek8UcLsPmTQEkCPaieLgADMTALt5CFR3ilRch0AvmPTVgD8Ih5sicBKUCKLsABN8kBAeh6XlmQEaAKj7kCCCAKF/oPYrin1mqGiGFe1dAIIkI2HClahhpaDyQV7OQIjuSiByF1o7AAiteCT6epMrYPt6ACS3qkQMp/PimrNLAKNuCgDwp6R4eVS5igCnp0t+B0ikmw7MiOtCALVHoLDHoLZWqmM3CEh9aIHECgBBh7EWABN4gOorAABBCt5UB813qyafdIJHEP1rAIaAAroBSuhFU1ImMbq/+hCUt2F4+RD6pwjQTAArZQfszCIpyTClCqAipwdTXQr5FZifNZplc3A/qpCqsQC/npdO1IsE1HC/p5pYmJmIipn7JAtTOoCmZLpQ7qmGDLqRlboMyaABHAAR+7ri0ZCrYIeH+Hsnp7cdezDZQwBFuCIBrIBrolsy5FTCTXFG3ADNVnlhiRDzGqax2gC4h5PYhRmFJ3dLYqmatAA1enAj7Jfx8glCSgAjTQr0YYegrqsPeJtC6wfyAQAv44C+TXjgmLg+gAsbJQA5Z5AQl5kEIIAjLgdLqwC+gntqsgA7YZi7eXClIXstp5p3s7vWuXEu6wCXDgYwxUMSFjuGl1Ml//ciFCZgTHcA/Uqqlx4Y2MkQsdEGseIA5SZ3aMcZj4kA7iyYirAAMwwKoiQAKgmgGjO4RXl5RKiJU08IghEAL+W68/igEkAAP/aLxdi4S3WnRJaLFHOH8zKAOwWJAVywEVYAGeCANFeAuViG8cAAErYItEJwoIEAAMwAr5QDPQSL02TGMhNg8kFlhg5BSAgDHG2FveK0ocUkpMsxp/8AxtVhcjhA4sUAACsAG18GaHNlHiCZ6Vuw9wYVm3kAGtWqRR6ogDq8FX95MZAKoCyoNQCqoiEAIwYAP6GXqXiKBIJwuNuAoOe4RYyAsz+I9HuAtYKJm8ywH2WaAIWZASILwz/6CZZJoKhDlR6rACBhDFsOC4N3zJqxVi9iANhCAgJ7MhVpNQyDjEWeRAppQHgQAHTmANIYa+c5EPrAABA1ABzmpZoYYPuHzFhXkQ6KACJWACR0vCk7mENkADMKDAQNm/rdqqlBilS6eUp1ibKoCVlqeKNUABtJB0fGyESbuErHgBM4CmvDC2RymmAYlvqResHCsBE8ABRailVTxRusABgWm3SoTJ+Axe9mAN/qQy3VsmHMguZUbKg+RAbVIETlANEWXJELEP5tC+B5AKMYmY6hB5UJcQ6NACsfvGw/yEMfACrVoCyzzSrjqE80eJoHeKaiwCMICVPHgBmRgDH3AB7/9IAaqYAnYMjzHgzRkwASAgC495CzPgpsPbdPmZv53otl8ZgPUWC+RHdK2gggvQCgydz1atU/7gDpGQBGkQGycTMhmSJbdxB9+auARNQwa9Gk6ADeZbiHjrEfgwClC8AZQ7eRG6CjHpyjXMGOiAnwD7hPu7zKxK0iMNwXZ8pbPwk6ILAi09fzAAqh9AA7xAA0Cajy8wurTQetbcpvKXzapwgraQhKvAAld5mS5AA3NqnxzslQRakFx3AaqwjqHmxCzZAeZw1bhtVzJSQhk5Ml+CG4lLl2ctQ1lyoifKUhnSBovAZnhBjQ4gADWag5WZAlya1971M1AGZfKpiTQQA1H/2KomQALhLd5AGd7jTQKyq4r1OQM0MH8mAKul+NglXbFOCqotQAv+iwHtTd3t7aRWWLkjYZjsqALBegEpsMgnvLwFyrEQYAG1XGyygJ0EcAozzBj7ldsYjk/Xcw/VEAVdXajDPbM0C4ftUgiNiq4DkQ9yXQAcsAqrEAMgMKpA6gLz966MIQst0JsjUXSa6KuDXd5Abt4mMN4/GasoHYnfDQIv4M0yUJS04N1eHJT0fcYj8AE1sAskQKqZrXkC6qQWUMW47HtEx4jl+LvsbAEpwKuzoAqtiMhgyQGkR3ToIAtP7AB3axLVmuF6Tk2QCg+eIARJYBtoEuIrdlR60AZR/3DictEi+zAO7RsBFxCFS0qkPaiKILAKiNHF2Kyj/cCOyfuJQW7e4v3LQ17eJhCr7S2fncuqPdmTJDCPR7gK3/3qjD1/MU4CVV6UFGCZKXCwu5sCAmqJxAWeFb7j+QCxmjjUBLqm7TwDdmyfKcCsXVcBrhDPi8kCDHAKsOSoe97tbGQS9DAMStAG1QUI4eQpZk3oouReYZ0HuqEHciAI1oCLG6EnE9UKCxcBGbDvoSqqnrkK/Me16me6hna/NRCFP1kCCo/erz7kJkDqp/6eB2zUM2CeDi/eJqB0Uqu6T4jwrz5/rToCJPABlpgCMWlukbcOFgDUxkZe9WtZ+KAPjP9xDrVwpZkd7cD6uxZQhPqZCheAyBDAAucQ4IspCqFQcfrwK9zu7UyfQShhDYrgQmziJeouruMivl8mB4ZADUsfTTWMDytQAAkwAWcsqqL6AQJqeaCOoPG8D7sAAkMO0q0Kn/vrAg5/6j4Zzlvr47H6Ai/wk6VuAi1dkxU/q6ruk/P34xkgjkZIXPvggIfoCuaGDzMmEutA7L8ndYxoAymgzuxckBygCka4CqdpexWQC3/HiPs2rU3f+pPkD/XgCUEgB7hlSotT9YcLJjDLVn/A9W8NErnAAAUQARRQ9mbvvyPf3q1Jg/VpuxPVy0ROA2YK1ERXjvkaq6sAoUiXryH/EPd+v78v8MZNd/B2/5Pif4QzEAJR7qopkNntuCLmtg+y8AGyIBIx34uaf/kSldo4r6zBGgEAkULWrl0zLBQogOrfwn3rduXagGrfvoUVLV7EmFHjRo4dPX4EGVLkSJIlTZ5EmVLlSpYtXb6EGVPmTJo1bd7EidLeNChD5szJE/TnUDp08NAZmlTpUqZNnT6FGlXqVKJI5/jpM4dNnjlF4STzZ5Fiy7AV97FaUCBChg8i3LZ1S0JEBhAgbMyiMYsXrRkwQMyY0WKGqhQhQpgIIWviOoqN16GTMcMGL16zVsFwAUPzC86aTcB44WLGrsowTJww4SLECxMkXNCiXBmv/4sUM2ih26VilD6G+dbdMsH43z7fE/Mdn9jw+MV+/3KNkrV3VooJEyRIoHBdQoQEG1oQrGHhAQvhDVURECAK3/CFzXO+hx9f/nz69e3fx59f/37+/Tn2g4eSHOLQio2l4CgqQaoWZLBBBx8Eao43JgSKjjf+ICIafvwZaybiRCkAAgkucKtEuOQioa7XKJsFMxBIgBFGGWSRZQbDdumwLIb2IY2XVfrSLEjNYhAShhlW4WWXGlxw4TTWYAghsxp4maG2XdahjBZasOTxHLHWkQUExRZbp8yJhmtsH3zy6VAsdFrQa7rttLtuggggsEAFvQyKQBXGGnJFAwE6SMcs//8ORTRRRRdltFFHH4U0Upaau0eaPeTQgw42ANFqqDcktMoqCEcltVSqkHoDkK3yeAMOIZzhhyGaGvJArQsyKDFXEeT6AAQYZoFtFhdIGIGEGWYhoYQSTDBnuHVUqGEsf3QczrddVCnyhRiI5BYGIkVjMYbMYFDBNBhIgCGFFiqLEgRe9kFnhhpWubJMe/Upk8Ur1UHOzIyIw6cxi/pZB4UIVCBtFQ4ioHNOCSCoQIVbqLRAA4ka2sWDABgQp01JPwY5ZJFHJrlkk0+uqZ9vFuHhJ65+ikMPprIy0FSbb8ZZZgrzyHSIT2LVyGOU9jlnAwIioEAEuEqUKwO68qJlFtv/agChBBiBJaG1WcbKJYVbKJoWTeKOA1OFGbbd1ltvuSXyWMqA7FYGGWogKLp1ZjHhBRJSYcyWwvxMcx999NmHsq/bHBvsiyZa06KwGhqlutFoqeGCCKzT7nLuIEiBtBkq6ICxx1IZwABW2EM5ddVXZ71111+H/SR+iPljjTeyGirToiL8ycA+asY5eOEbtOqNoO4ggpJ5UKcpl7QiuGBppnF1IQYtW6xrFhtgDI1cw1wQ7p980DmToXsdgxtt9bmlmxcbiAwyBm2vTG6vVWhw4QRaVNCFR15o7JDi4HWLPy0OH+sISwLNQpzwlWUiu7gTBWyzixZUIAEM2w4G71QB/xbohQXkacg6WFEAApxCaLFDYQpVuEIWttCFJTmhP7YxCTIMz4Y3tOFRbpeHO5DhCvSYVUVagZAJsEVpuuqVbdwHg125RnuHcU2UMOMCxRiqN8VZ3C5ksD71zYAGSPLR2uCXtlnoYiHjm0He8jYLFaigRj1aRzrGIbB/PGsyM5DFOv4RtomoQ4+LY09xPJaPWkBHBheYUm40aKfMJaACMuCFLVigCzPVQgECYIF7XrhJTnbSk58EZaM6RA9pOCENn8JhKlUJod31bg5OcIcmPTTEBBRRVyUCAQ20ZAMXvChGJLABLYblAjfyogZRkkU+FpcPfATshPlQhfpogDYvxv/pbGkDTF+2JQMcEedNpBHMKlSxi1W4YEbkFEib1pEKF/CiBaoYiyaVg5EOlUmZgNzHLLaDAr3Y4AIXjIAGA3onC0RHFaMgH9EEFYp7htKhD4VoRCU6UZD0Ix6UaMOE+oDKVXbUo1D5VFDogIRuUEsm+xgFASBgS7eMYC50iQGwbGACusSoNSSYkgrG+add1OIciFOTMzlCpW1N04uSocyPigoYdJRpF7JwAQ14YS+x1aKKDRnMY2Bjr3Qok0cguMUsACer5BRwISYN6lik9Q8Q2WkCK2qBWwc6VwgkgAMEQQc6dIEOc3AgAISiaGAFO1jCFlZ1/KAGIdqgKUBs9KP/j4VsUn6nBzzkoBhhk+VL9nEKAtQSV7ly2q8qM6xf3hQxMEjSmRy4VjXlgzcdmUiNjBqDvOylBl7cFlPrSUEwnoki51DFT+sYmMeo4hayUEULWtAKdEDVWLQIgQySqwpZzEKsE2ugRpip1j364zf/s0ACEFmZFEAAAhGwIEC540hIooMFBFAFOjwggA3gI7OGxW9+9btf/tLEH/QIhhtcZqCXRdbAHb0dG/RghmAArTknVMlZVEqBDGAAtC4IJvZ8SawY6S0EKpjqWcMiS32sqXwegZe8aCCZYHlxmrSdxR91tI90kKYGTfUjOloUmTKtQq9/Skc6yqoKYsGAFjT4/0CSlZxkCU4VwjsSMmuT4wrNpQBYg7lFc8M71wQUYAOkYYEAPLCK+da3v2dGc5rVnOYcZWMRRXBlhQ48Z1W+YStxUMTyYjJjVgwAehZ2C1tyqaUYkCADv4SR1fSGmBjrUYFm2e6T/7UOVXgRjKuoAQ00rekaZNcs6FhFDAKj3GvGQGKpLdNrxcILE5QABNaFkQiKhcsPzMDJGJnxPK3oXQtgRwITsPWf4GWBud6pABygjCocuQoW/LVQa4Z2tKU97U1Kix/J+EMPbqcVrhiFzt9O5R/g0Z6TotTPSQM0W15DORJ8QC4laGKyshaaEMyAMQFbprBJso9b1EAvPgLM/f80jccTswdjRp3RKmygLSPNwGyy0OsohG2vmbra1qQlFq4MLeg8Sto4+K7I41LQSA7YYh1s4rd6A9rlWOxiFrvgQAJy0WwHFErS1MZ5znW+c0VNax6/KAKrNkrgPNABQeBGOs4C0YZpmBQm+zgP0kRg4Q/QJcMhqAuximW1GC0rNS6A5z76gW+KkK3gIsEY+dBxC1cMxAY1gPuYknOmfM1rFS0oEy221QICUgkGV6LSUVfMpGXRxpgwKlas5SICEIRgSzcHmMf2MfJf26kCt2CmM1mglrkWoAWyqACSUlGBXbx3AzbneepVv3rWx2da34CCEYwnIU4BhSscTXruWTn/BGFsaFaqCBGFMaBkI+u9V/EuLQlY8/WtjcWZyrk5is0UuMKpeBUvf+o4GTiRqa7jp+uwgbxUocx10CCPZVp4aEIjA9BsyRawVnxLefUBEJ+dnmoK4C2uk53tpCI5+CoTCzivgKorZLMAAUiFa+EAVfAAAtiAhmq9CJTACaTAkLi2JaADAnkDpKCDl1EV4NG9EGSQNsizx5mJVlCpIlIyEZiMctI45COtElA/E0gNHKkIffANfxkaLFocMNEeTIO7Y0k1VTML2XKyhkhAxoCMWWgur4GXG3sgYYoRENgVl5K1JLO3GBILHtwHFkAaOrkAFjicEGqBAlAvR+IFFKiA/1oIhV2IBQ6QgQogABaAwAq0wzvEw5yTFgAzgsW6va6IGQnpFBEkxKb4FKsAhAkRhHGbMc16BT8jkQ+IHsfTO7qowl8KARhpNTX6uipiiHQIn6ExMceBNCyZBbh7OyZch/V4tDpSqo5rCLvRIr6BlxbwkobAhXJQB1WgHBAwAV+Ki1y5ACQhH+9THLFwlntaB8rjv4fhgFuoFyxJAfU6r1lggRbYB3XYgIHwgAhYAALwAC3Mw3Ekx3KEKLTaI3mQhMUqxHZcEDuQkD/IwDxggz8YAmm4r5fAhQOAABeoMEnUJV5yt10hyNJqjbzhjM+AgbHKwVDcN/FZjo8AkxqwAf8b2IWmOgciZIhY0DRRizF32hMYWIV+mAhbVCt9SIeJmYWs+6VcQRHtUYEUuABdCJt/aaaJuIXLyQ7rsJOA4oAW0CleUAXzIsAa2QCKyAUFiC8W2AA5HAXuWoh7cDpzpMqqtEoV4gdtiIIicINQwT13BMukuD044JTbcwSgOalySAvV+AAKeDXjOz4UQTQSwDrVAI3OEA17+RN0aKaTQA6RuJtVWIXpExoe2bQVGwwZ0IttWYXGaByy4j52i5EmypUPeI1VmICx2oiGCJh14ADr4D/rGKgEIM11mYXwIs1KGwBWmIh0YAFR0AVXcIUDGIBWMLirxM3c1E2Tkadp4Yf/aVACIwiEpPjKsDTOCKEQO4iDRZxKlQibfNiAAZgbCqAAGYgaGHBLubxErtOlVaCcFCiXuYnGBzKuOJKVkIi8kXig6CAf7ZKFTDtMwLiyGGjMkJu0wvGlEbAagrTCWuMFblqMJ1MTxpgBzMGcXwuonuQcYEkBBEiAW5ABhJC4iTgHe8mFAyAAWIi+3eTQDvXQ/QAbewCFHPiDORhOoijO4zROxyKDaGjOSVmBAeAAWgCBCZiS7aEAQ2M8ueQ6GFGBrXKILDMTYbsFVXiXEMoRFDugDbVPhjAch6yIdbgfGzjMFYua3LJBpyuLzqwSxSMWpumVnLqtFFiPf6kWh6iO/4apkwQNKAtAkhZAgFmQgboCx25ylnVoBQFYAC+hlhf90D8F1ECNCbCZB0pIA1RJClFRUeNEJXqcgzSgBCDykFEYgAo4xQmQhSMLRhRBPk0cJrMhoBB6DHu5BXk5wsewv3+JSJGYMYeoAXWAysVQuE2jUk2LmrNZMSi9iMHhPmF5kU3FlbaIGuq8gPJgHrXCks/8NTVlGM0JKCOllxQwwy6rAA8IBVGohRAinQZQpj4V1G8F13Admj2CB0VoA1fCPUVdVHcMhE+xAzt4gyVwBz8tiRlzBZWCJCvjBRgAtE2dy4NkkoBVAThJEsEEjKUajeZqquhLHJR4jFmQBQIaUv9QBJOKpNJapYEWxC1Rs8GKkCX8Q5NdSAF3iz+3wIDXsIHq1NXFoQzqqJOGcdaV4yZe4AAHNS8IGAAZZQAUyJcOGAAP4A0TFNehJdqiBaQ9YodCsAo2+BTc4Z11DcvkzIM2GAZ6TQldIM0KiBqXc4G2sEJORbRMpEHWGBe3mQVRMxK04TTAUAUkSdUdeUy/bMhIqq5buAVd0IXKoMi3u9iMrYyDxVLEGdWGUKsUq4tfXTylsQ1zCsXmbIVMrdEDpZOYvRNI2gUBvBM6baoV0IB8qYABeMo9MtrRJd1wnZZqUAI4iJlEFESoXVeOagNHsAfmcQni0IBaCgEW4YB+9Vf/RGuSsX0BzJAqytmibKpSTatIGrjIlXUWjVRPkJ27p+qmWGxbwUTFGji/38AtGuCmT5OBFBhMf1ErCqqLE3kpYLkAT/yHfKyjBZiSFLgcnpwTNiVAcooODiDKBCCAr1mHFfAA37iFBRCAXKCj0jXgA85Nf6iUPQhEjlJX1zXO2ksCa9ARq1XPFTgaG/0f6dnUTj1INcIMayq1FTvei63IehEaQUqJVRRciENW3DAH3HjP0dCjB3KxGIAnhgASc2JCdGCTBYqX8k1cEKAFWUjft2UIVbCAGqAF+E3TNWUkn6QFDgg9XpDWECkADUgoB3jKdSIABvASBBbjMTZHfjiG/yKIgzvQA0Dwg64ouqLzFAgGS65AA0+IleawYLRjhc4CNvfh3YLsVE2kwbzJDNiQhS1anyo14Yy9C9EhwkhjUu0COXnihTAmK/MhiJMzi4LYFhwJi/KTH3GxtRlgAeEC4pX8AAsbAfqjBeyEws3sQkxt4svJnPnljuoiTQgw0jI0ryz2knIIgGxdxgFYAbEj42NG5p07O3+wh2AwggeW40Js1KGova2gAyVYnkhGu3NYAEeyNV5wAQrwWpdEPqvxOiYxm72ggSFRW9qi1Yxl5Ir0McZBOR4UxRMq4KAhDsGFxgA6G9DQDFpYBYEQ3cc5E3OQBSZa5QxgYhBwy05LUv+DQwfo0YuF4T9aXjlkm8YLKgBImgGESIABWABXWAejEWZBGYVkVumVpjY8nodPGILiSdFozj0CI87eAYQ4eIYHIwvz2QAz/OZZaAsj6t1Ey5pzdgEkkRoxKip3Rt54tlgbmOdk1MEVrsOSALmzQp0H24caGJIQ3oWkljyzWAUMcKleoQUbSGUKwEZkNItfs4CXu4DMEShy4o47iYCOFkqQLoABuJYAKAC+0QUDMIBwoF2WRuzEJixNeuln7kAJmWmaDss0KIR5YF+ToJZRcFBbw161nouWokK5tKmsYRIYmIylLjXacmp4juqoti7fshdtDhrmRc8fbtK1Wge4wYz/KXGBraEnNJmFyjSyGBDnCbCFufPY4ZCBX7sAl7uAJ94OCLC18KqA0UyAFPifLiOhGchT9NgAERIAblXs8SbviXIPf5iHSIhpObiD24tsyda9aZ6DH2i6mRAHAiAA66yBCaAS3p21f503JoENGxCSdlbk1rZY7Ykx42CTk5PtZUJikABZsTix3L4mtbGN0cjnbCQcWdC4tsCAYEoB6vwAvivMZ7GO5r5czAko65QBlbsgCDg2H0kABCiABaiAArBxTFqH+Urp8gbyIOek854HSIADTGlveHRa+A5BmfmJaZ5aSJjdlyiLfHAAAUgBynHLfbWwzwJbRPu64ltntTHw/8M04QSvSAU/0vBJKxia8Hu+iHWwhYEornYO3h+F2H8RBVRYB1pwmhLBACJeBXH+gAko1vXFiHWoUQlANsz8Nc6hnAHksszlAIjVbgJACAwtAONKgAPIhSYV8lAXdZSJp4qoB0hwg5jJgzhwb5dh8tyjgzuwEApxdSPYhjwuiVEIgBmtAep0PBfw7xT5VyYZ3hnIDLZp6gN37bu4C+tC4S108POUvge3ycWJF3mRBaVaHxjgu10wh8LUh1ZggUJrt5bCABDjVwxQ9xRoEwXaBdF0l1ngDhCrgWIbTQL0sv9BiH1HCAIYgANIC8DS6lEn+IL/mHYv8q4ssE1p1AJ79f9vg+OrcHVAaINImF1q5wiKMIcDOADrGj4KsB4qHAEvD+TWiKpbjR9pWm0ET3MFz7BbWxyAuerN/EuVWI4AyifsZYxdeDFqKsby6Qc+Gg4rZpq3+AAmnjp1P/eO3SOp3AdZ8DUXQKci7smAyqC7zlwvK2IZ53cSynQCCF2DF/uxl5R6eAQy0IOXSRVaf9qH/7bHZnujK4JtuOyUyIcOIIBjyQBCzwvp+e+wjaq9KG2mVm1Fhmpmt67EP0IzDao6xOestnvnNQ/h4pEWIPNdOKOqzjXP1tG3IGIbUHoMEOd1Was6asvtgCRjuuuqXzkCvBP9vatV0PGu93qOt2Syx/3/3NeP8zZ7N0j7T5mQ4Hd1t086VFEVkSqCn5mJPa50Egh9Jk6audDOXxqWyeCl+CHzaTLzw3d56womXmjPzWSImxTQuFWJdXDe4bAFS94HXdAmGEAHZ4mFzwv/BVqHECDIz5466wF20Rd9CgCICbL2EVy3bx2ICRIurOLFq0WCCBMiRJBA8WKEiBEhFLDAa1WBAgQSHBAZ8gABDwT/sWzp8iXMmDJn0qxp8ybOnDp38uzp8yfQoEKHEi1q9CjSpEqXMm3q9Kc/e5Ta4JkDqM+bOXOyau3q9SvYsGLHki1r9izatGrfvOljJ08eOH+yvdz3z99QfAwQzIBBAQNgDDRo/00QQUKEiBGHSTBm7MIGLxsuYMB4EQNGjBmZY9Do3NkG6NCgZ9mYZXqWQ7sv8db9RxBfvnyqX+bDZxTf7Jbrdsla5/LgqhgxZMlqYdqFCxm8Yu6bkfjwh8OATVP48CEwBQkqZM24IENWhQgqUKeowUtFgokWKUbMuDECBI4beKkiEPK+yQoCaj3t7/8/gAEKOCCBBRp4IIIJKmjUPZnAQYdWaQBCFldqWXghhhlqeCEbc+QxRxuR2ANTbkCNIkAKfgUmgmCzWIfYYo05ZgMtNCBHmWWbaUYDZ5/RIBppQZ5GCy/olGgTQbXJ5hpL++DGpFBO2qWPbPmgs8oqs6x0kP9B6GgGwwwq1EBLDC+4MINvTeKDSjozZHCYCCAgxqILtNQwQWCAUUDBBNlNAIEFDdlggUSy0MJBRetFEJ5773GEAAf02WcfSgUckEoBDeSzIKedevopqKGKOiqppS5YYlSfDNFVhRu6+iqssWJIB4Rs9DFHEtYcxdo/5xRQAQzX5SkYLRbAeFgJMj7GSw3IvfACZpgJt6Nnn4lWWmmnOcSLQT7tow9sBO0T25HeHtRSPrusUtoqvRHkpSo2rDIDj5eZd5mZM9xyjrgPgQDnnHNiMCYIeVa35wQJz0DLLC5UpNAEpk2EEUYbxQdBAiKxwIsMAiAwUkgE7MLCAKPYVa7/qSmrvDLLLbv8Mswx3yOMEQ9uNaGsOeu8c6xv0PHhHGykMcmIRrG2jwcFXADCXysSe8EHionwpmImkABZs5NBG61wPHrt2bXYCkkkL7sYCVSS66i9ElGxtXQQaaC1uw4vsjTE7AyaxSDDDLtIFoIJIYCgQgu7qM1LDI0ZFjAGINBig8F7UnABDLTQ0l2iEkx0AS2yTEwxRRBkdHHGBSjHwqQhCdDKLgsckA7KMcs+O+2123477qQeiZc/zwABhx5As8Uz8cUbj1Yeb/xQDa9HifPrBU0PO5icIxgmIwiz2EnCZC5Ae5lwXVf744+hBZkt2bvMsm/sOBWUTmz6tJ/T/7hvR3a+vDWsYvg6NtBLQ95ugY5dFO4WzQqBCwZXgxmEYDHWC5gIKBADXrjgA5KrzsJqBAKJSCA7mptICuzUHolcxFHxSUDGBKAKXmxgJPYRwAr2IQoBhGI2/cgdDnOowx3ysIc+RNk9qmEEWgVvK606HhKTSDy4pGER9mieUI62AgL0SVhOq1wKEnM9EoAgezUiQWXCyLUd9chHQMqWaXbhEO2V7Ww/OZlsxoWPcJVofrTxzUHUF7e4gYYGq2BY3tQ4Ay3tAx3dYpYLSNDAfy0Gghl45HWoM4EPpGAwNbpAwjqokA5qziIwOA8EFHUR0VmsdCFRxS4q4EICLOAc6P9wXTmg5MNZ0rKWtrwlLvtTLn9YYwlz+Fke4sAGQBxRicY8JobYkJU80KEN0oBiUZ4XAQpkQHqAEcEH6gSDxCjmXyHQXg24Z4IXmIAy4Avf16x1LSGhpmwM24Xh7GgT2EBJjusIV08MUkg94k80NVhgZ3hxuVskaTb9g0EDZdTIOVkHAxQIYd60N4vudBBhe+KkBDI6gQSYJwWiq1joLnbC+8wiFgkgAAIKAMN9qGIAMcwlTGMq05nSlKb+8EYhiqCVD3UImT796YVupRVBzEMp++hAR6wJmOt8AAS8mAEGFJOBf42pBiAwAVafRRmucaaM5TvjadqpPl6wsVtoe1L/k5iUpDnmQ34nqwmXeHELiWYrbDT4Z7XyZpdu4aUgT02kjJpKgoZ+IAMuoIFDHhcCC17woh7MqOYoUgHtEYpipIRPAuJzHwJUYBeqOCkBDiCAUaBDAwQwR01Tq9rVsra1o1LNPHKqlQndqkJAAypuc+uVDrVhGNAkSi4KAIE9DQsD2IzTLGrwnC6OiQYgQE5Wy2lOMIkPbF8djbbKNgv10QKebvwJPWVysnHVhq1uleV46cbdsIbNBnetlnBaYCSz/gYdz20MCGwAg6WdiUYMqwE6VNEnhzb2ghn1IAktwDDQZXZ0It1spFIhAJOsrqUruKE8XavhDXO4wx6miT0i/1EECLGlxLo98YlxthUjsOO3UUpaB4t7HWx+oDSLAUFVuUcCE3hva+ZEZ7Xaa5rSbGu7liOS2TIsXtvQ7zX3VJK41IoOsu6CYQw7jV3hC8Bb/ENtMVnHDAbLGBXQDTWWmwUNFjuBGfCCaRYkcGMhq1H1RCAFT/3oCEWqWY4U4KQy2IUHJkwAVt7CtKj9MKITrehFq3Yf/uCHMIYAIa0MryvCQzGmlUiHNzAxC/dQii4QEIFqyvi4GFhYCOK0MBvomMfk3Kq09PY194YtSEWusva6azi0uc1cVFISuQ5X5SufJkvYCk0N4DsDVxSkjvtgFmmIpLZ1zEIFBcMAUz9Qo/8BExjO2eGTnDNKETbLoACjzAh8HrxZAqBSAwP4WABS0QoBiELJjL43vvOtb93dZRpuoFUxb1vMTBNcZ2y5Qx56wLykHLUA3F4qto37ATqR6QOfnIUid0xOM231nF4lX63ZqN13Mqxs9ibRkoYirtisAx1T3ta2zsxe0MjCn7YwiD5xfs/DmaYGLdBFQVKQgYmzCDAxoEUIDubtb38w3BNryAXaI7r4pNuUIUnpAWIhCwp3oLQDSMe+wy72sZNdQY7OhiDycIebjWXgBX97rPQQByjUQynmiMhfrJntOWXANCFgGCOthtXJdLxrXbXuOk2zRu0Nm2xGOvnb0IoUYcP/XKCVv3xit7WOfKgtNrJx60FakIILoACPB1EB1DKAGGyTpml5v2CfNArZzyl4FhTTiLrvg4ABWAAdqRjAoBewDlEMIBQucXHZk6/85TMfJ+AgRBzeQky3w736PMtDG36BFwwHZTYyNLdSmRow1S+MrDBozODH2XFpffy62crS4q1c5fl/lye1WQrny2xlifIfy+taxT8t0ONRyUw8GwvwR5McxAxYh7BQgArQggnoSWP1ySaFW0VQhJ3NwAihUGZdjHDhx6BtjAcMGgHoAjoUAAN8F/I1Hwu2oAvuGz14wqp0CPVZnw3KyofAga4kRToswHCR2rDM2JyAgJ3RgrUx/0YJWM2r+di0HJ4ZmQ/8CZREzR+SFcmm8MRe6cNS5JwUEtv5xM26JFtnBFBB1VEC0pdrEARUFZ0FtV63wR7TddIFWgQEsJlHwUe6kQ5+4IcKtQ4BBIAqrAMLBEArtMQKviAiJqIitpY/OMO/tQVb3NYNTmLOBJMT7QpLjAL4KVXEBUxTmUeb/UsS7hiPldP3hE/7gRWRXNn+CVSVlU2a2N8VMlwcPduw8d/j4A8Ajo++4BxN5AavHAQNDIsLUBBxGdhjOR1FrIcs8EIFSF0eZtYe2kdI3EJ9hNY62MIBLADkLaI3fiM44pBd8NIeyEHy+IEfeIgkUiI7ukobPMMhev9LA6QHEBaXJ7JZM84AIyVhVsEaKqbTdQXJKpqGlXXXKjoEOsyiTkjJUtTPu9zCLTKM2GSZ18jALKxNaxwfc6yDCwDGCABGaUTP6zkWJ23SHF6gR9QACoUUxmDMHobMAMxHoAmAB6DDBgwALrTEDYUjT/akT+bO88kBwrFFh6xjOx4lhrgBPChFLiDAcLnesLAIYhiWnXjRVZEAP7qAKXIVQNKa3BAZWQ2J/JENt3RjeBkVWu2GHnlhP7nXj8SADXBG36DhasQE7+yDDEjPQ53HwUiOJjmdRYiSeJyHuYWUNIaE1ZWEAKTCLjCAx+SCLazUT04mZVbmyjxaJPyAHqz/nVAZJVJ+JlrEgSMUjSxFSSg4HFRG5cRFB+CBQOU4l9VkJROiUxl5JfqE5f5Zzvw5xK5hoeQlxf01STrE3MxRZL0AEC+wDU+MgyqcX/RMQGlIDpxxkgdtEp2tRwIYigXgWQdanUl8zEh4lgAIgCugAwMcgC5YpnquJ3siyKMlQw/8kqXBRQ2Cpn1+RRocw06WZlCkQ59JgEMV1zUVVo3RQgX9i/YAFlap3/dISzrZJpYM5JAwXneVjUPEok64TTfiRK+lobjQzVxhGR+5ZWd01Vx6SxqqjSw8l4H2CRxilElaxOdEgAWoy8VEgHAd5jRmTEzywgqQZyxImMm0J5EW/6mRJoU/+AM8OMEReeZ9PmlYwIE3KAUriBpxBWievMgHjAkMvMnUsBlsLigMlJPHHZ5bAokUSlRBVqgarc3JMSRTpFxrFBKWGafXwIC7YCGJ8ELEzIJI+qUHgZscrgd22lm5NVhLvqQLnVQfHoAqtIAHLAA3HimlVqql5kRUOAIcxIFV+MyHVBqUhupXxMEifNp+EsU+OABq6omAWlCd1EB0bBGNnJ9Wqt90OSHYmM/iTahuWmg8QV6HLsWTAOPbUNuQjSiJ1gue0qVOJCmGrUMuTJkMuCjCYJTsyegFjtL+WACi6igImgQr7UIqqIIohFYB5MKlpqu6pqs+3MMxSP8aiQHCz8yBHyyTqN7rHKwBKMRjT5jDAiRA9ARo+BWWacjJ4nBRnUhGP24NKjqhbe4qQebabqrR490F/fymUpjVbDRPHiFrH8EXDNwCszYrlOAFtVEr01WnuGHr5yRABdRNxnjgNG7WxwwAC5gNCwiAfdTQuvasz7KnP3xDOopFfeKrDUIIIAxThwSBN/ArT6ACAUzTMbbqmFSQYSgGY+DYUwGOrTbs+NhADcgLkQwZK1KoGlFsb2aoQqKlHRUEb9jNuowPDXwHRhrFs7lAgSmEn8xetlbMxhxq7n3rpBBALKxDoIWEBoDdzy4u4/JkPUgBIHCq0RotV7CB5c6BExT/ldPmhD5sQHpIJydiwCct4MHKSAqgxpi6Wpl2ZQ2ght9EG42o6W4i5MgWYBw1xbk0GYgCIIlmiUHIaVGswypM4GNZ5wXOaOg0RAUIl8xuVgF8TMgIwHwM4gAcAALkwoY2rvZur6L5wzHoQDo66eSCZq0k7RygwSfww+bmhH8mwAVhaZ44jg1YB5zECBdVziyEgBihIrVYy91gV+yO5cSWpdo+Re42GUHUDfn0zZasrcqtQwpUlLXK3kneXgJwgLqExJ7tYaVQmAagQywM2knVG/eWsAmLnV3AgyC8Qb3awfiK6qQFTR4AwhYow/rmxChErUgKbJYSzFSWLmOYAAgg/5ZkaI3XVss/jW2WjI1EuWLl1R9cYaxScF5PnExyjWG3JAlSAIeLHlgyalTfVozp8EIK5CifhYxJFED1EoAAdECRDGJoLYADnzAd13GH2cMjuMFW9IH4vjBovgWITOkWJ42LdpvBVI4MGNfBblEQL4uNYEaObAYSo8YsrAsTF+TsUiwUF6AUA+cc2yWJ7IIYMnCTfHJQ0E0IdHEyQkwFV0xEmAafeafzigS9uQJjipb1oqsd7zIvs5ajVQMcyIEedEhP+TFoZgWJqeMcREFRIUU6aEAova81iYCdXIAWla5iJIvVuIAaPXL4wMDX/BNk2InYhJUAWyjtypOTaOFT1P/uTeyDuniGKqgGnKKqLqSAGqmAYyXjw7Qye+BopJTbS4IrGquQKizAyAQASohCLze0Q8eUP9RDFLQBreTBMBmzfdorW9jBHbhBJNwwTsDCAETADucdllIP0cEIEGszclgODUQLODuheUjkaKBRbp4tbxaJcv5iJyeFO/NnXaCDGAKQG/W0T9hXBKDGDGjSRXVSZCHvuWXMClnAQAsuAqADCwCAKsQCeHqAKT80WIe17fgDKPyApQUNRn/mG0wIiXGaHgyBbyFFP2jigcGZQ1mQCVxOdUCQ/QYxj02G5fSF3ngVJYetkNBIABukGsHcJsvEWTrFT9OEycYtZwyEawD/b/cJ2ARwy1InTAVScGCCDil1hLoQwBk77+B6QOsIwAKgQ6AdwAGcA0uAtFjXtm1/yjf8QYeoWNGm9dtx2ofUax68NTsApwZUwJ/ycBu6yDXNSV/vGCmaiSX1xXF2RkNUMlhFrJU58cs1NolEdlEcRPa6xi7Qi1ymSbCizT4UzATwyyzwiShwgFMrxPGGMR5GRAG0AC9wADUKrkgEgCvIgs4u5i28ECzcNoInOMvwAyWgAVrPQSD0tm8THFvMKzoy0x4081GkAwFYgAtUlHQ61NF1ZFTxNSOTYla5wHX3BZARCQCuCwCXrWKTpcvJ0+YBdVJQyYayhvCW6Aygg5rg/3iT/QM6/IUE8Mtu8Ivn/OWggg5LopC6yHJKvVBnBRobN8AucMCEvRRtK7iXf3lT+EM3rIr5bkUMTzg7eiq9ctodOAI/JAUsoAgMgLjrOc6dCKFKP7fgnQC0uIB5oFnXDFJk8K752LTs5nSSvbNRIwUB+sTRCG/e9I2a2FuSuoZaQuQqpICRp6eXuQbHSESM2nfouAcBbEwK9HfITLkAFM4C6Cwbb3X1MsA4gjmt17p/qMZO1gMipEExSTiaZ9o5vsEdmIH2bTHx0UAKUKsEojSeO/eJL6hWUQZiXU7e/HlAlrOazjhvYqjtLvpRrAM7dx88q8IopMm4KFml78OKjv9k00jALlj6XdiFLhBKJgXmeswo6XCELOwCYqKxpdhHkLo6G2/ALrRQbNs6wid8QxqiNPgAqP46+WqFH7DBHXCBMmDiPmzAATRMIeuJ48zAnrxIwDSSjAiemTTotAcSLzxhTYeV7B6kd9eFt9stZr/ReOmGvT3bAhZX3qWA6c3is3GASU4EVE/dSEVKCwyAKa3SzaoSCbJxLFxjK4y3wle91b/EPCxCDxCTh3iFikH8JAb3HNxBG3jDqRLFOSyABuxCQoAuBdCIBQkLBAEx+o1TtO8NZJDVRyTbOqGRdusakoH3P9QG1S+k4A95Kauzp+OtgDrUBNhCk3S6uMwACUH/NUaQ0gmxWyqhugiL59P/4c0uAAHw7NWXvum7z11IwxB8Pdh/5qSl4x0YgjwkRS0MAAZj0utNgDZVB7ZJpbP39YJqZRhdhgwgVtkAUNi6HxNTaBWqc80zxQFH0Wy7RHrDxNGcQwq4ZpdakZ5MwLsn8OO5AiqlAKhfRNHnoWZ5RAtQo9IPmgygg36E1tMj9Ah2wFefPv6bvj/MwyC0iiT6OkDMETiQYEGDBxEmVLiQYUOHDyEKfDMnziJ+/zBm1LiRY8eMowZ4mHVhAgYKJinUmHXyAwaXImDGJCGCRM2aJki8MPHiBQyfMGbImMWL16wZNGrYULp0lo1ZT2fRijpr/xevXev2edy4L51Wr1+/7lsHlqxGfxr37cvn9ey/fedYtFo3d6VLuxQo7BKrykINrKISRJAwWEKECREQJ04MIUJgCBAKEFi1KwGCApcJVNjVQgABBARAgw4gKxaBBV3Lpla9mnVr169hx5Y9m3Zt27dx59a9m3dv3793+xvWJo/A4sUjJle+nHlz5w3pDJy4JlJb2/s8FEix6wJevBNS0Jpx0i6GmDBn2rRpgr0JFz19zoCxqujQWTVo2MjPFOpTqbR4oWUXdLICax98CgTOrbF4E0sr6/IZRZeM9tmFvBFMMgkEFSw4LAWrKphgAsIkOEwwxRBj7LEVK6uAl1QEuP8sslViiSy0GwPwIBYNCBBHwR+BDFLIIYks0sgjkUxSySU98mceKOJ4TsopqazSyoPi+MS62vLRQLtdQMDLpAmeKsmuls6Tiab1SGCvJxNgiAEoWsSbYShaVkFqKaecmmoqAfVK0KsDBf3NwQYZfPCffLD65yxbQGjJJQok9Y6CCS6YpQXDBqNgMMEKQzHFxlaETIAZdlngMgFY4GWDGAmw8UYCdkklgFaYzFXXXXnt1ddfgQ1W2I74eYYHQJC7UtllmVU2WYHakGZL2vApAAIOZklBgu/Cc4G88tKkaU312tvpp/l4sWGGGGigT6qkVuGzKaj+q4pAsvbRJ59Cfcv/Z60G9zXQLYz2uSWEkzAU01JPJYiBlwpIHOywwkJdLAIIEljR2gI02EUG0DjwGNZZQwtAFVUGEIXfYVlu2eWXYY5Z5pljK3CeJ97Ig41mee7Z54iKeyO6OdDIZtrZzEEAggtocWHbS1X64FtJPwg3PXLZcwHOnmKQal0aaIihhqrwTEqp/vyLSi9HDUQwyEbxzWrlQQMGqx8KxRrvrg9I8m4CCjKYRYYRJTYMsYoVUzGBjB+7TJVdClhgF1k8A01WHFGIZYEN5qbZ889BD1300UmPrZ9piJijj51/bt3115/Nowd3jpatlQIisGAWGLadAATxvj2z6vPSG7cEnNz8/ymqGdZlF2yViJol3nnTlmqWe2vfSB+3gYSb7nN0warzjtQaX6O7HT2LlzAnVYGXW7rzToJdVpm4sMNMFPXixxa39rEBNsALVcRiFguonI0uR4AAKGBHCzBf6SAYQQlOkIIVFJKTogCHN2yQDh30YAfz0EE9gFCEJKTDCOkQwhOaEIUqbGEJUwhDF7KQhjFcoQ1feMMZ4rCGO/ShDIH4QToITYhCM+IQkfgGQEzEDgIJxDzQZxsW4I5MNYjApWZAi0uVBwOSstq42MQTOM0gXTEwo/OQYiei4EleTamX9+j2QNyULyzrkMUs7niVgeHLX6qxzj5UQCmTtCAtqnjapf8+dAFQfepwKArMI0t1mQQQQBVWeVVkDhArkhFAALfawAHQYUFRjpKUpTTlKTfiD2mkQWhziE4HXflKWcaSlrC05SxvWUtc7lKXvczlL3kJTF8Gk5jDrOUcJuJKQNABEAIZokCaOYdoSjOZecgDHAoxj7PIsSz7WAEVZ2FITO2CBhOQGhe7aB41qacmxzPB8WDggqE0z4xhAxtSVjGUotyxT1O5BTcJFSQ6eiQt7/OPU2Kxtm7iY49f8ccfbdG3CNQiLbE4pASYJgNOMRJxjWnM4kqFMRupYh0tCABmEjirALAgO7lA5UthGlOZznRX/rDHIuCQhzvgAZl92KBP3wD/VKH+lKhBLepQjZpUpC71qE1VqlOZ+lSpRpWqUIXqQFjHujmwwZp5sIM1v+rVWKLQCJCwh1u4ia8NFGBENVhFidyXAe+gs4tpKl472+QmE5CxBvI5Ixrx6RSi8OKO9JrFOdJClnX8C0gHIp9Y0LGLXTyFKTagD0AZyhr0oeMctwjUOlLwN7wMJp+LvN+JEvNIUjUuAZdBQCV3UZrIIHCTAhDAAlgwAFzRlLe99e1vgbuafZxlGTcoghfi4IU1nIG5zXXuc6EbXelOl7rVte51sZtd7TI3DFsIwxfAG17xjje8YCgDGMAwBhxkgh64WYcGEnAYGXCninMtD9XsCsab/+TEPXSSQZzOaE+wKQVe+rQeVBIVFu4BqY9bWQc6buEnyjZFKbL4V/YotGCBbSUtYlGBaBk2ARfQggOLbKQjV4ux1jqOFx4IoAc0GeOUCsAAuRVFcHGcYx3v2JTW4YYyqiGNaQyZyEU28pGRnGQlL5nJTXbyk6EcZSNXoxraYMeVscwOd2yZy13Gsju0zI53bNM251gABEaUyG5tK0NcRFN+1VOCNr3ABTSgRQ1kIKcAA3Y/NoBe9J7CC27mg6FppU1aMquRdeRiQHNZBx4pq5RV+GWhqcnKNsVHIVlcUX4SmwUNTEwxxQTmYvtbcQImwwHPxEIWA/hMSkPTGQJ0IP9lPLb1rXGda5o9lNe6JtJZMCwbcVARo7RIQT7tS1d1rpOdyBtx03zyV3ahUT9LWcUqAESUq3ATH4xlsLcJ1mFxzwUdhaUKZg3tlltYQNCOqlDfRuspCkRAJSUStf5UxCIEVIAqFqhcq3iEyRjPqjMVKIAH0u1rhS+c4Q2nTVsS7vDWyK03uCAAp6r4adFmKHhoWra42FmTOtOCBi6Ithm/ds/89Dmf+cx2YtsWcdkE1NKO7vBcxE3QupFFH2JJgQUatY6I/g3EhAEPLRQ5ItQmjlQRKEBrNyCVCmgycrXqDKxB0xkBHKACGwC3xMEedrGPneyle8XFJUZGXsTvJMH/OxOc9euCEc/C5FwD8LRVXm3+PMXR+gBLg4WkYbOwjfAL4gVi9+GKVFzFjrqAOYV27tB/rMMGElgFRtIhg4U9bVuensHh7o0iFT9dBrxYhQELYBkCDOBxmZxtbTlJgANoADVlt/3tcZ/7sFsn2EFqBdoHAwLTg5jjZ3LJ8JjNTjvPoO7RjlPz8t5npshCfPhYh98J6jaZw8axigI2wSZ3snFM/hYymPQMWkDSQuUD+wY6BwxcEgu3pEICKLEU0UsUATshZmL6y1hrK4A+Pib1ZCRWNIAXWEDWYC/2Iqf2dO8BITACJXACV0MULo5hPkCADgkl0KlqPg7k1GPuUuA9/3hCTu5OwAZsT5hiLW5uX/il+4SE5tgC/AhsBhxv8mSBnoAilChEX8pCF76lBQirO1rCUnrHUzyExEAF30RqvmTB32TE9WyE1Sxn4G5E1g6gAMqBArmwC73Qc3rvC2NQFHCHMGCAsEBsrqbmJYgHBGti+UZQJ+BDz+op+lTwKSiuw7pt/VgwBhNtUNwCHcxGJRhkH3YnwFpA/jQC8LxiHVTgJaTmnNQw3ggHf4wC9BwJMiyAPlJhxWQkCguAVXiBRzYp6xjwAMxBDFVxFVmxFX0LO8rwMGqAKOCt+NzsA++qJqRC7twEPu4u5e5JBc8mwTKC0BZLbuCoe+RIbm4hXv9WIdPcYhXM6AyhEfIKJUHuhjskJZ3azv6M8FJK5DD2z8QQAwBbgBZkgQM+UUZUz0Y0IwGt0BS17gAGwEdc8R7xMR/1UXTyITtGBFOMwijS0BbBBflAUASyKAZcgL90AgZegA7rCbCWQiWUghcISi0c7esUZKDK4tFoQKEw4hxaYF1QReeukfDWwQW2sTzabmEYJmIiIJGWjlSEYhdU4DL4Zx1dKzIE4HEUkOBiT/YGwKX2sSiN8iiRkkn6sQxjksTsREQUxu3s4ovS4ynkLq/MxflQDu8GLD/cSilAkiPSwuaIJBnD4j7aDSP6wRY8SwZmwCzdIvIwotfWAQTQqRv/XfJSkJBEKoAWZIAxDiNjUmAoVMHgnm5jdFKTPiOANqAKrzD2tu4BYCEpKbMyLfMyb0Mf1spwnnLN1LDNwCVcaAIEuiYGQqBN5gxOfgIiVW7l/IwpzEcsFmsPG0suFWUd8iQs0yEtbuEGPcIse20f9OYuvRH//pFEIiCfGCMwjs30LsB/IskTZ8v1PGMXRiEAYG8edQszubM7vfM7F7EC4qsw7sMwlCINu5GLRAADMoB4QCBwaIEEQAB5kmcO/woFB8xsnMJ8Mu1A8KHbNtI2veIW4OUkH68jzLJA/CEtZMHtWvIbI6ZwVIAWKiAmh+IJWyvfGkcnUSqTBGDxMolk/7QONOgRFcDzRFE0RfMRHxbgcCzALw1jQkGA+KTyA2MCBFyAF2hgTejTPVbz7rhy5fTTBvhlSxqlQBBtDxNk+waFGMNCFvCjBjQSLJLxGndhJR80LzsvHBvjRV1uFThgcUit1DZ0HQlgkmZLADpgFH/SFLOuAAaAFZhURem0Tu0U11g0MCQgPEhiMO4DKhWGrsLlPWugacilTbTGJ7hG2vBTP2dhfPRlZdKCURAESW+DIw3EFbzSSRVrSTPibsRCFkJgJTnOJSM0MVJA7fhCxTxKQyGDQ6mORHdBFNoUMjmpRO80V3V1VxuORTlzFkZk3rKoFtPzvtbzRkmAFmwgA/9sQs7a4z3OhVFRsAbMZhXGhxE5TC0Ijf0ojjawVSvkhpxoYAZ4cDUYcUvQoTuIM0vljXAEwwJUYCryaXEqtFX3JyfN9PVspCd3oU1HNDQeABXmlFcJtmANFoLyFDGk54p6508/UyoN8gO6xgX0q0cdUivv03nwQz8eFVzhUizzhdAqtaFewwVVAzdngKQmLlHa4hzmogYQhovY9Tvw5wJkwAakggY44Fp4QR2ZTnFeVSezMFYJwAN2oQL8Nesi00QPtmmd9mlFCR/EUzA0JVgLQ0YtBTSNr2o+oD2VFQTSQ872yz22ZjX3TOU21lovklMBESOPkUkFdFDQgUYuTbj//nAf0hUEYOADPO4uJuX+DOMCYEAW6EQVOMRFBWdxLEZMpfMTY4U6beQAdoEFsDPWbLVEBxZqNXdzORdJWJQxLoAXUsAwQqwpNo48pPJYM2DEYuADagKM6BNa7Y5R+UwWOudj48Zf/tNkVQNTJQ+tZO5QCEYWtpFvHbTTLiAFVEIqbEAF6vVEJAACZGEVxjRF/q9/OFRfYy0WVKFygzIoEeABTqFzybd8zbdXuoQxOGDtCOc7Riy08FKQphIDRqBr8wk99Asr6cwXgfRr0MitWBYtcLd3tXUuXPCBZBA4ONIQ+VYEWoJUvSN5YyBtaiAFFAk5PwUCUEX0WCRoExMz/zTpAAJAFHbBAGTtX23rACBgFM63hV34hYNEMx+jBWbBAki38yagUE/XJM7pJcwDA4SPBlw3f1GTbHtiDoHUntilr9JyI/wlc/FmUrf1gEGWbQEGSRn4Lk9Cb2cAZ/dJBkjCcOwNOScgAbAlTMvR1ETqg7WXJzcAHRqgVm3LACpgACYThvE4j/V4NvZhAx5jBmSBcDrvUphmBk5XTOgqi0YVf0OuXKDVbGPAf9HWWq8xbmcOeEV2invu5g6UYHRjLJe0BQTpA0AgBGBAJaLiXVQgBS7lHwX5VBNjF2bAWtL4/zw4X+NRcinXVkWDAThgAMJhj4V5mInZK/QhO5JzFf84Rd7CEQZoIUwC1e349im4NhfZxIhhQDUB7AQBKywXBIrjJl+6zdFwrsNI9pMfbx2YJynohE7wiPm6o1PibUtPNVRQbRUe414x5pYdF6U8Y18HqHJHtJM6QAaAuZgROqGFeR+mKALCybRwmAKcYoc7znVpoXXb0w0PFZuRWNq4ElWW9PoEamBubrHW4T+7zV8O2Jy7dW48NQ/TIh1M+sECRCliQAVcQK7ykplfsp7tWSgqNJ/3uXFRqgCoU6VSwRxsi2RWCkbsUaGhOqrNlwwTgBc0ijCYuUQKeeMm5fg+YALsrD0zmpGbDVHNRTUhMkiRArEIZoAVOPI6TF9UWmT///Okra+u7/qk7Xod1OGuUXqc31YWQAAE1pMg78JUQ6xwQs+MezafGYef+1nGNEkAOoYBlvqEezK3tlCqObuznXYfRqEANOOqSYSZJ2BCvQWRv4VSlKJqxtqa1yNrzKWjMxZsUrYQ3dpQBNRS5UbcxK3nNjlffNuc9+hta4BvlW1mR8veXFmMl47fZAA6+QeyY+X1PmNWdBk7FVAAFAAdsmP8PDu8xVtXX2EALGAXSixCTdspSEJrTSIEaAEGKEAExhrk8vdZZ/vkAEyJbdvCJjVJBA8QSXqPLHXAX/CJhZNSqCZ+709E/oZElA5/TqwxppeWWSR7h/b1Kmd7r/N7/0eYMirAAcd7xEncO1thAELGAiQmYpiZaWzAvlA3i/i2vsm6rGUbPh4yTrjZnsjVd4cEBhuEw8YCHfj2gSORUti1dyKUMFALcQgAVfZ5uhGzAP05k657X1PBFS4762KBFQhgA0S8xMV8zI8SFgogxSmGuQf5UjiA5AJ1Uu6jyKky5IrYPY5YK5/vo/9pHdAHnFkD0fy85vZBFfpWi3eanjF4whNgO1IMsqk8HktGR0y4ZN64MTlgSsk80080ilCU02VjHA5gMG14xUu7XSeUd5BcQ+JbkI4V7ja6XO48rSMyBmThXpDkWz/ZLPphHW6hRkt1p3s60TsqAc5bHaN8yv9nq403KQA6AB0sO/YAYIBWT2U0vdpzNQyLEttZIx1waxf6T72fRkTsTFsUnAK6JgOQ20Zngoh7lGwVVcfPCPp6HEn+O0gWdB3Wh66Ue56DXdQ6qtQ8BicZB1b9OYRxRAPQYQME2gAmdwAGIBSsPeLBU1B6bS7Z5vsw/uI1PuM5fuM9vuNB/uNFPuRJfuRNvuRRfuS/z+K13dIYYL4wccnbdQJwFgQOySnkt9Xb8GpePXm02aPDpgZsPZVqM9DJAtP2YRXW1Rvlp9/9fcLLsQBWQRWebp8JXsNhjcZiYQW0e4R1obpZWOLFnjsLxB3IwRvQ3hvYIe3Zvu3d/u3hPu7/5X7u6b7u7f7u8T7v6f7KtIEb5sGTa8MBWGAXGINiXnmQB7nm/yYDLlp+u+jN1um+sVJr3gRjp3EWclu3jZ4ttkksDNFBmb7pEX2ME+PfIaMFbgEyVD9fk70KZ0wAXGGXudsc4LEAdmvscd8y/QEYhMAI4AAOksD3f3/4ib/4jf/4kT/5lX/5mb/5nf/5oT/6i78IkqAIjMD3jUAIhsHTZ2MFWoDwv10wBPkbL8ApvlpGb5Frd57nx/bGzyVOcjyLhn7wfsTzFSQ4PX8GZPbXIbSeoXfpAAJChAgFUuyqUABCgYUMGRIo8DAigYkUKQZopSoAgQCidh0QcKBArn8k/0uaPIkypcqVLFu6fAkzpsyZNGvavIkzp86dPHv6/Ak0qNChRIuy7Pfv3RM3etjMeQo1qtSpVKtavYo1q9atXLt6xUonDp02kfoh9SmKxa4EESZEkDBBgty5cinYpSABxCwbFGbM+kABg+APhDGIOIyYhAjFJBg3JmECsgkTLky8gGECBowYnGHM4DUr3z9///aZJG3aaOl1qlXuWzdrgmDBgWtjuGtXAl68c+NKeDswuPAICjfw4sBQYUOIzCk2rzgRgCtbAQQsQMdCY4ID6Vp7/w4+vPjx5MubP48+vfqa/pi1yQPf6df59Ovbv48fq508bODniXMFP0C9otZAv//95ltcceFm1wR6zUJLDbLNdtsHhhmG2GIaPsZhZJNRZhlmmr0QAwyr0DKDaP7ss0tqo5HkIlGvtUZaSWeto0JgFNbG4G50wSWXW78BJxxbAhVQwSwsLNcQAds1BxF0FAnAUSwBBKCKORQdsECM630JZphijklmmWaeieZKqcGzhB/8PSXfHE69ISecdr4hX5531rmnnnXiaWefgc7Jp3x0xkkoonMcOuiifP7pp56MClqoo4pO+micT9khJxTyAJWOB7NYMJCQbf24211xXTALLzJMYOFssWKY4YaLcdiYh5NVdhkML5Cowi60yMDaP+e0IEuxMLa2j2iq1ejiPrz/1KAjbbfdxqBudPkGHILDsUUccQXIIgNDCTi0UETNHSClRR7sEsAG6HSgEQEHbOBlmvruy2+//v4LcMBBmWaaP8D84MccnOYxB8MONwzxwxJHTPHEFleM8cUaZ8zxxh53DPLHG0P81BtvACKfEt70VCM+KBzU7alv+abtXdrKANoFdsk62IW1MnbrrY/pymtml+FsgwvF6gKDDLLka1Q++0Bt1Gu00FCtbT3WDCSQpg4XHLgQnKvKKkwyhACUzkkpwEYd3EKALa4I0PZEolAtcN567813337/HZQ/4CzxBnz5HY544orjRwcdcNDJaQ/K8FTjP/mwcAupwR2o4I+6/9n82SyzgFAthYLVehjQuHYIWWW8vqAKLzMoTRIvm82grHfr6CPePrekYAMNs2m9NV4JIujtkEWGO3YBLcyy0LlNqi0R2xttEIsq6CxQ7wED1AJ4+OKPT3755ut7TzBp4HEVnYu/D3/89jVOx6Z5bEHMWT3tM8ouHARnKs8t6HN4mYUsLkCL0VEAMKazEOpstbpcSWYyv3IBhGCgNNOsgwaeyV1r1oE3oUxNFhOYgA34ci0eZYs3QeoakcDGFiNF4FxqkR66qMeudhGgbQJgADqwo5G2hUQc5yuiEY+IxCQqESammUch4gCVPjyFDnp4ihTlh8UsarEqeahfwuRwCf8B/QQXu2CBQGLWua4RUAKskoFubEALEMDKdBhwIOpUJzTWfegFLkigC1ywCxjdwgUzQEdpSOIP1IjwH/gIIeUSWZp9UMsuNUBhCleoLc/FTHnLS8DYFMKBWSCESQ+hXpQqUrcAXCcjdXsIA7qzxFjKcpa0rKW+CiaNIUBFPvR71BZ/Ccz3MewNfqADHvZgD6CQMRUCcQsaW1iX3KSAFyn4HBxzxEDTiYBWh8nAhiJYAshMEAa8oIELXoC71TStRSWBpCNxso9GemeECwxMJXemQtxwbS5vOdALhSOQGDbPArPgQNrQVsqJROmUqNwh3VqBDgZUR6EE6MA7bYnRjGr/dKMcrUk/5rEIN8AJEFMMpklPmjg65MFkc9CDG7YBKluo4oxEUmPXciMBFdBCZ7mpAYpKR6EPiMCbGcJjBD1EghrwwgW+IuTU9jGLFDztNEVp1jx9V0/A0IAWxMPkj7jlT04ORCABbR4EyjiAdE1voe1qJUfWsT26VUQUHa2rXe+KV46mhh/VKEL9nOKUxqF0sIT1CiDqN4eEuVQaQNlHLWIxnLAGqZ8+quQEcDMBGNDCBtkM6gcyINSifjOPQwuB6M5pmRDoYjUzmMFFe5IPZ1X1NaOblQT8Apge5cZHmvQn2MhKHE96ciG7UIUAplc9trYrAB5ARypi0YB6RaQV/3mtrnWvi13y8SMSaJDTFQsL3vC2bw71o5Me5BCJyvFkH7rYhQXOGEAh0Yw3E5jFDC7LowlYcBYkmONstimC0P7smxHsYw0qUwITpEAVBJuFCjxYlHzwbra8yFHp+sLVfO42k9DcJNjGOsNPSm8VxkVX2tSlw4lQqQHONUAuKsBDe3EnuzSusY1vbKZvGOFxbCDpG+rHMPEKGbx0Ki8g8iCHRdADVP4rQGT7Cc0gWWBYEeiRgy5ol1n9V8CJgaBjSICzGJAgwS7ghYr+IYtgzVNqREHNLnSm5dvMYKe6zSQLJ+tbGBIHAnwW8UJSIYtSqhXF1YOOKs0xNwXsQqJbYv+AbHEM6UhLetJC4QczipAwXpJsyJxGKRwQCzE4wAMo+ODAAma4OQN1a1sIrCYBb1NCCvi0BhWazQh8dkcCc4gGvDBBOBXMi9KQphYgaMHUPvhaeN43zhiQgGZ5ikkWdg44XwNouITLZ4a0IBY3JHQOGxoABuxibqrMhQakiwAWUHrd7G63u0+iXpLIowl+cJ/7Oo3vYK70xyt1Sh66MDmWTa0DBUhABZ7MT7pEQAazuEA0GVRCzSqQ2Q+EoNBEsAv+jqAxLiDWi/KhChVMtWryHApqcmS62vQR2hs2Xgvj0pZ/Lq/PfS44Q1iwigQk9MTKldKhY8E9AiiaAXJlFy7/3o30pCv9uvFGpDV0kO+on3SljWOYv9HgC/31hBUDAFeqJTvZGsQGLqnKll5oEQOgxqrio21MhDgUAr0Q7B/oUIEJzqGaeC7yRfsgHR0poF866/POCpdLzH6L6j6fSyHnetcC0CURhlZEleiIhQKCyIBYaKCVCxjH0j8P+tDTsukksUck3gDFkkl99fEbJh3uMIc+yCcKS/5JLgrudVUfaFsQkIUNyB5r3FzrsnO2gd+DCuABA00EL0BRBlY3i2NL8gOyGE2ya9Ks69OENOsAgbVU2EfS+Yi+vQFSzD+MaiPRfLgLGcDLNMAciCCg0NCxjjmAXi8BbMAcC6hbASza/06iJ4ADSIDn0w/y8Ad00GMlxXoNiDiup3pPMQSjRno3sQ8bUHAfFkCGV18zABcDxCDYYhcpgGVBZRhcliEgoBgj4FMRtBi3QDCqIAI1kHeP5hLnIAs5eA7rwAu7cAvoAEIlkRrQcgHDI4IUkAJxZDO8FWUBhH5ltX7LYRwakFbUQ38BoAG7YHnSFS+WRzfeMwraV4BjSIZlWCb9MA09wB8kJVgO6Ibw0wbOUIE4sSQLQFPDAVYRwCouMBdlF4J38QFbZQMkEBhxFmCHeBgggBhiFzS4YgIgtA8yQALkJBTQsg6ikRpPJYSIVBqzsECE8QIkMBgPpkG8sA+QVBoqMP88WqMXKbBh25JwL+RMMxdi2WZz6WIcG4BiOSR514MOrrAROzQRAdABsYAAbXMu5mCGy8iMzYgm9gAJaZAHJAUHb2iNixMHjnAPQKEluQc2CscBtMABdOGHwlcb+gUhaTcrh1FHiDECAZYBGxchuqYYIHALpSGDJKAChvQT6sVmr7ELstBasqAOmvgi6zADhNFNhwEYM+CDF3ABMCiEsqAjwpcXUfU52kIzhadqiEdzNcckDMALujhovbgRooAOrCSMbRMALGAOdXMAFQBhzkiTNWmT3+EP7qAECpNY1+iThwMHf8AOQLEODJAA3ghA/TQQSWgB5FgzunWOHzBns2D/AkAVYLSSAd5UZjBgGI0xWvfIXo3xAcbWjyaxDnBVAyqgiIrxAcW2C5C4GjWAggEGGKRjISBwDi5yDpeFLSLYiuP3crEoVgMhQx+pEMrhEBWwCx1AUSbZNtqTCkFkaKkALyAxAKkghjepmZvJmS2BS23wFCT1k6NZH2/QBsdQOZmJEvsgChmIfr4lA7RQAdA0fmV3hHYBAnBkfAv0X7QiVBgQA0ulIc+3IR8gkfugAiJQAiEwkzKhXqmJDrygCiCggsp5GCXgTSDgAqtwC7cgkIpYKz6zIypwbKXRAndxhAjEh3e2kWElcyAmUJ90NgSgmChwXLtoEQtgC+iwAQDQ/0pyRSWsYCX2IhKdaaAHiqAxYRrbBZqk6aD00QaTwA/QwhO5QJ9j82G+5Rcz00KpQkDoCaIUoAIQMgMZUDp2tE028Bepw6KKIQLH+RcjQDs/8Roz0AK0kJwksHGNOALvuE28GZ7raDoXEISkcQu6NQEI5EYchmd59mHq95FnAxGKyQKNWX/vYgtBp2IruRGukBETsQH4kKBjSqZjqj/yUAgPqqZe8QZwMA+bqBNTY5QXEFDfGBw1sAqn4jm1aY7DIwFSSQu0wJWEARh29AF7wWUuqhgYwGCloQsf8I6r4CVz+BJTk5A+pYIbt3EkUJ3v6KPu+I4naCHM1pZBCCOuiP9PgDcBu7CexzNtQzKYhRmFiKlWCHAAu+ABFDV5BwCZVLKl/7kRthCZ7HI3ZWqsx9qZ/ZANIrWmzZoVbSANlIp9okAAHFABdYp+q1ADAtE1vsGnfapCILBVVFmohYEBIHA1c6khIgACgfQPj6qCGYAOqpkSrwECH2ADu5CpOwo04ElUiIEhhApU9UQBEgkjJIRZbuFgsAhzvjWY8BmltNokCRALHtA2C0U3G6CFHjBRwvirwhgLKwASBUBEyGqyJ1uG+nOKwaBLzuqyU/EfjyBGPnEOAsABKYChYONMszBTDes5T/mhIEobFBACPmUDJnBZsdJHLsBND5QCILQOLfD/ARw3r/RqEvuADkIlOsRZjynYlgKmkBcSK1pGsGRZErsQfICXAva4pC4Ec870sIlXi4dJSulyALLAAvcJEQFwAKmQktwTY1yqYtbBnyBRAROGsomruKKXGvPgCJrysi7LBnGwB0PJMpaDgTPwXug3ELTQAhDAOTb1rX+4ihTgAnA0CyFAG58BWk1bKx+AmVibqWMJlzmBtRcgAhGiiD4Knj7qTaElqnSUcrVxAXhXEuiQthFwCzzoAhxqfv40i2HDPIaZEFIKERSbt1PyLrhKN8BaNzFGjBH1EGFIVYtrvucraRSak3/wFKkXuc3KBnTgBsSQmjlRI6wgADJQLpxb/wHDwq1QZlNcA5UpRBvouFklgAE1sAujyo6oMwJtWbUhcBgjcAEtYKo2USPr4AIkIKhc1ru3xo4gXEfCay0lLAG2YBJ7iV9w4TSusi3Udn4fVqeexGfstxwotgC4qmIV4AroMAoHIF1bOiWTtwIecQAJgHfSir5LzMTY5Q/VoAQqZXUMA2rve42HRQdyQAj2sCI+kQ4MsAAF5Zq5178poHtCgip1EbQ9QsB2YQKoG5Clk3yvqwK80H27mwEgkE4vwh7/sA4YhKPqmiEXYoIkXJGw5lqpsQ4WsMK8cQsyQG2G56S0uH6MZ71RopgroEr9EwsbIFeC+71cGgCABhErUP8wTYzKqVxj/iAMOaAH/NEHRWbFP9mGbYANh7Re/5C3qWA2MiQcU2bGYAeLAjzAO9OXgeECSrULMZAB17J2vmmimwFaGWKcXvJOlUMa6JABOhUCCvm6DcwzO2KEWoMtq6rIHDBAQTILkPy2h4d+MkTDNnTDBRASEZHDK6AW3PvJQgysUyIAsZAKafUKqkzQBV1d/nAPWNAGVdQHfuAmQTbL1/hjaJAJSkwT5jAAGlsuMSQcF+A/XwdlaeStawyu13IbHAAagXpgWVYYDkRUp4OIAWa2FjgDGEYC3iy2o9ozJPx9twlrFvA0r+FwqnoBbbHOl+VhYiU2e0Zz1Wu9EWH/HbuADruQCh/RVh6L1TxkAGVEABpggwYN1mFdS/5gD4vQBnTgB0ngB3YAexEt0XTwBoIQD0CRDx0wAC3gXjkbHKxCKpIFwLQJtFBpG9iShCmAAX6RQDCQZbxpGCCcfBgAwiBwwTWRtbI2C4gonswWzoOtYZgVAU+Dtob3GYycZsDszk96bfIpsfEH1QWgFilJdNYTuKE8uFnYAAKgCmKt27stS/rgD94wCHDwOH2wH24t0STVBs+gSD1RCw+xCqswNhwdAaHEAc1UKkrpNXtK0iFakX5RhAukAnB0Nd7H2HWEord2a31RnjSxDxSJrzZg3sIbZ2R7SZe0NXCRAkP9/xsWLAsRsM7+rQLnF73wydR+9tTHuAAysAvFhQ7nlmJD/LEsSZkHwADGy9sXjuHn4w/KEMUqBbnGzXo99mPZyMU/kQ8NgACKudGJx9fCAavYfTx2Vo66dRtil6oUAAI+lUAzwFPPbBjNfGsfcAHzOhOZeK+Hakk8zWyHfOOCfWd4EQEWwIMTMEgdOeCESeDCFT3zSQADUAGqsOCpsAAaG9uy7bGtNCUBANACEApWm+FvDudkkkjE4AZ4cAdv8F0g7oBF1gbXYNEzMQoPYRAG5XUEVd1P1i3ylcaBTeMSsBeHjC0fEALivQpqObBrRwHVV+QkgQ6fiCI8bchDa9+C/f85JbTCE5AALbAK9cUCwnHl0vuRWn42ApAAHrAKldcC/UeMi2Y99TfEPNRDq6ABBABLcW7sxw4wSGFpcgDX96bnq9cfUBGhyfQTRbkQqjALM0TDLe7quncqPtsbo6tbKCIBKZdbCxQDELJZMMBAhDqqma6a7o2uXAnqKdfGpN4gMOfqUI7qA7ELkBzD3zJWfLZn8iw9CDAA9KngvBALKVAA3+sBsfBtvj7bQrwRHmBcpozsG8/xZ4LN9PAJQxAIzv7sUtdFjuIH1vDnMjEKCxHGqpCBEGDod3jd7tk5G3lZjI4bGYCjE1LCN04C6R6oNqACzfyJH1BfzimENY0B4Vf/7z3d2cVTQksJ5rswCizQ13BRKkrCubBe4LKOyR4gCz2YChZQEewiACxgjGhu8RD+n2m/AQegjB1P93U/JmR9BWhQxSWPb3RyB3pATHOABp0wszuBFOsAf0iyCzNAAAJRAdRE8HgIw8gzzDqfkSsnGyFq0t+XAoi9WQl5F7NwEzMgGDCwC838d1Bf3/jeFhbQArogfaVxDs2ralQO8E84vVFaAFVo67sgnSww8RIxAKIAjBT/sWcujArgAQPAAqds988P/eARb/4gD49w1otCUhBN8nw/WHfQ7E+hBJa7P//Q8iGB0g4PAUmCs13vntmt3Zg0ZxZA44P9fbg5A+J9/zUhwE4wkZqcJWtcBRAYBGKgQFBgQYQGEVJg2LChBAkpeK1bt0sWOor7/m2smIoFhwoWZqUoEMHkSZMQUkKAkKBAAggFBhSowGLVLpwsGBAQgIDAAQIFCPzkmcpVgKFJeQ4VkLQpU6YCNlQ4YG6jv41ZtW7l2tXrV7BhxY4lW9bsWbRp1a5l29btW7hx5c6lW9fuXbx59Ybt1y4Kmzd+3uQhPOfNG0BzFC9m3NjxY8iRJU+mXNnyZcyK6ShOfIcOkU/3sP7rB3cdgwIQHrTYZaHAAl4tSqpMgDKChJMTbt+GKGGC7969KUgYzlBCC1oOlTNU2Lwhhg8uasyixWvfuv99Gr+OHr1v1wQK0wseVLjc+UKHuFWs+7fvlgWTEy7IqJVd+7586c7xusWiQoHUTKqNJQIBNNACGWLBSZUWLBhgpqAiJEooAgZwpRWkoHrqqaWg6vC/FfYScUQSSzTxRBRTVHFFFlt08UW24CGkiEAUIywPwzLTcUcee/SxRzo2WywPOuBw4huuRlNLo1YAhIAAVWZxbZZRHohAJdtMwk033IC77TfgwBzut+IkUMUG45ZTzjmC0KMgBRl2oQgffbJrbzuOGJqFhvES8rNNhsCbYNAIBn1oghSwW0eWQYMziQMWWFCFvY2uy26dW0SBrzaXaNpgqg1SUAUnizrYAIX/DoI6QChWJXQqFlUy5HBWpzwU4ABcxYFxV1579fVXYIMVdlhie+0nGyXc2OyNOezIg9kfo5V2WmorW5ZZHAMp4hMl4cKvgdQAXGWWCmSZIUDassxNyy+/7A1MiMqUQJYaHlLTvD/9nEAG7PbRBx+K8rEvKzuzWkcFCmaBwU98Ax3Ugg0kVSWFQuONIAVZJtYyONxOKoCFfv/BrtLrVBHKAlV0MWcddGzBCFMODligFnS8SyAoVoFqlUIBzEkFKVo79NBDoUTRrlikk1Z6aaabdvppqJPMxgll52BjjsKq1Xprrnek41lAoH0WjiTkqVSro9FiEoEDYoJgFVlWUYWl/yvVXXe3Lrn8DV6IyJTggllmIA68e+9t7qBAVai5vezyATifdfIROO19VLmAFheIcwjQ9C6owTr79uGFg3iJQ6lRjiu+DQIOREHlOoKvS0WDFl7GB5/J7WMhAJDX0WWDnYeiUKmgYvEgww43HLqppw5AIJ2ropZ+euqrt/567J3uxxolFsO2a/DDF5+xzeg47I2rFUOjl9Lc6jafDlKLaYFZeIkFJrqxtFs3vN11t+/gXIAXMiBOAdNTOPMYBAO+SZSdjta466zjdv2o0y5UwAsQGG4hg5qURu5TuQg8JEwj7B9EUiKAFURQHemAHDpusQuMFIxk67CFBn7ikqQUYP9VwhuKzpKygF1sgHm1EtrQekiAUWikW9ljYhOd+EQoRlGKWvHHNAThBsMkZnxb5GK1rgUtOyjGCezgStrUIo4FyK8ACFDQf1TyxrrtbzdcqhgAgQMREPAiBQAsU5qKg0DmKGRfvPhK6PqRj3+tgxe8oIUNajADFagABBfIQHB0M4HFoW0fqLjNcOxowpPQLT4X4EAKWCAniuzihbBigQaS6MB9oGMUG1iAT4TiE1sGz1VM0UAsFpA8Iw6xQzpkAD6meExkJlOZy2QmivxRDSXAIUjQ6mI1rckjamJtDkMIBj9IM5cVhOtmHOCFLGICkwGdpDbqoiMd78i3FOziAoP/K10fARmo5zwEBJTKSnfuhLaKtKAGsqAOI2kxi1nIIm4zYGi/7FSafbBgAhWYgAVCcgGMktKUMmgB3GRxC1nsojq8sMgq0NECDlgAhxywRb8qUgtRHKApAEKAgQhQ05pCaJdO8cAuDCDMpSiPeEdsRTONelSkJlWpR+2HP6xBtUAI5ppTpaqO8gAHI2SjLulgQEsAVIFd8GIV/4FJBNJpt9MVql0mDI4LWmNHT/bRgPciXD6Hw4FbyBBtW8nOOThQKAtY4AIpgNMMaDCuF1JEgriLHD7QwUiRipSR1JkFTiq7i1nEYhUtaEEKODCVCixABrwA0JUSANZdiGIDDFjA/y8FYCDYrnGNEaJQbYkXgFTsQgBBW17yVtUBfSxVuMMlbnGNi7TtGSENWqxqc507h69BV7rqgwQ96LKPWhAAJg+AAKzGZQECEAiO62Qnu/L2vwjQ4BYhHCE9DfjePwqKAnXV3JhmMLIk/fMf6dAHOvZYQEeZdQK32Ehp+mGfdXTWBS6QgSlT4IJJZuACnowjHAvgAV2cgyIsGK9rAISz2Ib4pqxq1VCHEgBVxAJ5yhOqUwyQgKocV8YzpnGNbey+rThVEGvA0XN9XE1sEQm6bzAfjopAjfYRjC1N3YcHnPQAhRKAoMCjWzo5JccS1jECNpAFb+AFr/nK1Z6aA0994/97gVvws59o00ed1jGDQU0ABIQlLAtScY6CKWkfM4BAb8yKVrQWIK/tWYfJCJSATrGkUyHOpS53mpQAtOJnGrJVrZoClCTeWNOb5nSnb+wPeEShB5yJQ4+J3OMfp1prQmKMHd5QCHsUGC7+2Mc5DgCTAbCAXAVADge0m784App/XfJyBBLa5ztyTDjwZXaY5esQ3bAgzUfjzkbw4cHrjEPDihqYdvzRnfU66pJbsg3/bJMADqRiFLm435UUHa6YhLhVOAze8EwsAFeIAmhEBCZPcFUAB6jZ0wMneMENDsXSaOMKaUCfHnDErCCpWuLhYwMgeGCM0ZRmiUsaxQDSCFb/DswkTjJwUkrqdmUsD9sks6gBsuv4Lpi/F8Bihq9xcMMBVeDZg90RWPTOsg8UmPCSu5kjoAUUkwF0VH7iDZe8YUtbCdl7qAJQwC46sGJ+7zYpGkgjLg7+dbCHXexNAwcjcqAH7xkmmxNnu7SYZQdAKMEdc8FKPoC3qlmoIigIiCefl143LOmvvKoz9gx40yU78o1jcZU54+VF4Qm0wBYObE/Pz4KV7MDHz0P3DaDJi78E1MDQ4l200+udwyNOXQAMQMcG9p31qGgg5CzQSJLHfnvc5173IioNOCjhBjnk6DB+aHvx3Z6YZ0XCm2Z0yzkWsKpUzALRQuHALMb6EkRX/9jo8cnNLFTArt8cPuaKh/l86Wl+38jVzxyQhc4zcifmg+U6FnAUsbe/zpaYdhayYUnTna4zH7ItoDAxpQiADtiFBdA6IpqVAIiYAegAgds9CZxACqxAs+gHe6AEImCW8zE+D/SRsMmDNaCDHqgGWpu1SmGFoSCnDQCxcpGSmkI0OEoXo8MNkaAYoTs8vUk2ZetBjgGzuTKdQmGBVpiI0NGrsKi1CiA3s2oJwcsSlMu/ltg/JzE92mqVAxgAHyJApWgK3okFoQmAABiiDdGAVKAKc4g/C1xDNmzD3bMHZygCZgGErPlAO7yMZWmWOSCEetg4tfG5JgOQjDEQCBiABP+YgV2QgRgUJTjavgjgAFr4K6IjNh+sxE96Jx4snQISkLiBGIhpAV1QwzJqEkRDNEgRBVTcgOw7N8AzLZaoAF5ggQHwqtgqvQ8jMaiTOqDyQlVghTEkADE0gA0wAFkBxgpQBSEaBTdcRmZsRrDTiHuQBjhoAzq4A1S7Q2yUDCI5jGZpg0/wJhTkCBsaFVskAImYgf9YOhp8QtuAgBSgBc0jN2Jzp/DjQTD5Mpgjvx4slFmQAe4ziVeKHj+sNRTYAKPQsNChIZVCiezLP1ekCV7wABAzEHp7uluEugLcLQPYSDEUQwEIAF34GTFUgFEoh/y4OgEgxgVwBVlMIWd8SZj/jEkag0Zr4AM5iLhszEnIIAw/ID470IMesAY/VIvuyAUE0IBdkMXYIgANGBcOKLkICJCzYkd35IUKIDp2yTIv6bz22pswEb+v9MrOuxwc9LMEQIFMkr+B0a9KuQXy+rM3OjSWsIDWkDftKsecKTGeCAAFWAFUMIfJgYVQCIUVSElz2AAAaIBTuLZKwZCPZABYGQANOId/GEqZvEzMzMwp8odtSAQ4gAOdDM3GeDjBYAMcKYR5sL226I5REABZiAWnSwBVoIVUSDR3G684WqcCGK0FKDfzUqt55A3V6TwweTk/I061Cr9HjETxO4ljtDyv8CAq2op1qIDAk0F3u5KX/yiAFNi1WrzFD3MeBRSKjySAvoSFdLiPs2mPU2AAc8CFcTCmrcgFAAgABtiFWACKXGgPy9RM//xPAG2a9tkHf6gHSCA+0UzQxSgSq5kDOKAEcEyLjTvBfegAD+AF4Kmp2BqA6psF4EEX8To5lICAVLDK8nKnEqJH/9lKryS/w7sYWriA4QQl1hm0HCsjr8gHFLjOASG9mJCBW5Atm/qwBBgASBMAFkCFA/BIBgiFXEDCJcoOcciH6RyNUWiAUMAJDRiATMMK1QxQMA1TMeWVjGOcAmUGNFg7BbVDaDGfsDEMI6CGuPg2g4FMWZjIp6uAGaAFf1THsrIyk+A16XNE4P/sGErsn3ZCVGUrOhUgF+7jElCKABZIzxtdzxwFHnfDzqXbzhm4hZkQsQIQAAzTgAaAhXMQmFwIBVYQB/mkIqyotqyQnBwbDcdiGRZAIe3QuDHdVV7tVRipIieAg0Bggz7oA2hBO8hQ0zX1MTZATV2VTrcQhxXYhRTA0yHlAMx6SnWsMixxCdYg1CyRR+DUynGlRMQzqxmYhbuho1CqCTkRRTr1tn9AhwVIJ0WTwf4TiliQhU9dSt4RmA6AHrfYB8bsp3iNoBaQCir1VYZtWIc1EX9gh0JAA6sBDCITEmVdVrYbAk/gh24RxbOoBVWQBdhaFdjyiQgoUVWoAENEJ2D/67/oE1FwPdHk5L5DZcKOSQB6AdeW8ABXCJl1yAXG9Kes0IXwEq+WcIkC8YlENETZGoCm2IAn3YdROIXVrJSC5QqA2SQCWABKfViwDVuxnTV/mAdKaIM4cDgOVDtu1Fjjo4MhkAY6nQuqvTUNdbqg0ABZ4FOawp+4ZIkoaUQBmdmUm0TDJbwEoAU+m9k1WoAVUIVUaAAEgECfI5hUeIlD2063ibcKMJ4Pu6kOGAWd+4dyCAVLXQvYKaPb2QdYcJ4nHVvYjV3ZFQsC9Yd7eAbgu8m2fQNjzZE6dNtUowMj8Ib+RF1R+ImldDpduwVtdZL8gYB0ldluJVyjMze1wkoB/+FTwuWURbsZVTAjjUgHNyoQNapCAsBW1+CJDtCFkVFPkDULWd0KyBEYXViAAXid2c1f/Z3d0ggupyoEanQMxPge4GU7RHiHMmXLP2yPFehau1zKBUDEVfAw542J6LvXVhxc6t1gCUgAC2DO7W3ClUiA+uArWSSQqKzCpWQBXrBfAgiFtfTf00Vd6NwIyekrBhAAZdxfHu7h2JUHTxiCtHU4wrgaLbrGAnYuQDBNM6CEWFMyuMCHVFk0a71IC7iJGQAv5y2A6JXCP/tiMA62DQY0d5QStKJKk6swABEFddAKdahgWvS/NUq6XZApD1jYypxbuYCd+4AcjUCHBhgAo//xYUIuZIaFxmOwySDBgztAu7ZN4lTrAzuIAzgYho+Ni3S4O6GwRVBlv9iogPByiQtOWsBLl8ALpTE2ut0cVIbMkhkUYVSOygAQBa1Ih5rKV5eoqahIClVQBQHw2r2wlAKj1OvY0hXAY0NOZmX+TyXyh2xYBCKAAz3AgziYg96F5FRrUyMQSlor3p/jKh2atxCj4gJgv11AxwcZ2XsN0XQ5K3dO45md3qiMktnQPpNrZ7hE4XfzOoI5Bw14rc89AA2ws1ZQhVBoAAKIhZ8Jh2B2qAlqnA4QgIBdZoquaP/0B3sIBiGQJmR9ZGx+LoiLA0GAhwR+C40Yh1+CsYv8P1b/6dAzTIVbWGek1WcRNuVWbETy0h/BW8UCiJuSuOd8PrmZVjTxGoBQrMxKWYdRkKkC2IBRSM8j3AdWiMgd1osIcpwD04h1iGgPaFWL/mqwZkZorAZC8ALF4EYk/uipYtA80IM2WAR78Oaf2wiUDmftAtW7BZCZsAmcMCdDHOqhdsXs7NGgnso0XkdZ4D/TasJ21mekhYlN9YAI3Id0GAcW0op8EIdwuDbvGIWFlWu2gJz344iI7gB0uBPQDmvVXm2DU6J40IQ9gANoSZ85QNYcUestirjNIJIhiIS45quB/Ye6XhUI2WQN5eSWhqFVeMqggOyZvk1OcULQE+pWtFe4/8zUChiJsorL2wTszHWb1CjSDXiFcjAHy/YKfQiFnQgAGN4Hc6BMpL6LS8EvQittZGZt/M7v29MOeqCGJ3ADZqk4G4m4w+gD3B6fPNQMI/iE34bitxiHnUgjA2m0Kg4KJOWFUZEFFrDf7dzWx07aUiTqV8ZNm44JunzKv/Vu6XZCeAuxTzXS7zWjo7Bw2pvfvLgUdLC80lgHDQiAFYhA/Q5yIT84fwBiOYwDOwijN9Wm2z7wrrkWrNEDrMI4u6g11NghXLTIFeYFBKiA3Mo7BwGQTvHboSZzmk7x5+4/cnINFZfL8I430xvSoAjdXCiHcyiHcZBIHfoJFkjLupjvG/9OMn+W6PseckM/9E1TomgUhDawxopDH6wJElZz8q1ZFuQjEjj4g2mI7y9ti3NowYoEsSqmvl3QAJ9IAF3bhVVIAfu9WzNvc1ifaQI4pQowc9ArkHOKc7wdngDwidfioQKY5cm5rvnODn3IhwFFIwKAwPdFdGd/dqXaOVDzhCKIA/RhA9OsbZyk9EpXO6zJg0Z+AyUQyqzo9LSAqHRIFZMtblG3SKTUYploakTMOw7AJQSIt82Fdb/FH/nxWwCRAZLtd/AGbznWdS0HEKC4mdObZfqOC/yInIQMro3IBfsNhUKHdozP+KPK1W/ih2qIgh/Y7auhAz0IPm7v9hwxzTz/SNs2KIRvOMG6WIcGJucrBE8CADlV+dwKwBj7kQENILHvHGrMdW6i31xxMZl+F3ODN/iJ1C6c+dQr5AlRaPi2aBzcobzIqRRUGIpMkzWN/3qwR6p+mAdhIALGuJq0PvlosW3EoG3ocgKS5vhmJwv8OF4Yy8UrzKVbqNbYAgoj1dOQmoVU0OK8TrQxdxuXVSMn9CoCEPzwWnrIR/iVxtOoSwpWQMIlmd/79od/uZTWNABWCHvRH32k4odteIQ2IDKrYS61nxYhE76weQM9iAM5SASzUaLrGgW7vqWol62EVoV+BfpVeS0OaIHK6kcNuBmcyWulj/wCaFrnX8pOMVkS/8PyCQkKCPEJC98APKv6hw+ZtPEHOrkOD/jlcJh70k9/9bceDJSGQUj9nmz98DkfbCn5QpCHV6Xb9oCFnVD+0yNPgFgVSwCBAgYNFjR4oACBAQgspLo1a1aqFAsKFkxwcCPHjgYX7OKQ0GPHkRtHEtDIcCUBBBgZHiAgk8ABgrrW7funcyfPnjxz5suHD1++nD516is6TsOADeZy9jsqdSrVqlavYs2qdSvXrl6/gg0rdizZsmbPok2rdi3btm7fwi1rlGdUnf7gUUqS5s2cvn7/Ag4seDDhwoYPI06smM4cxn7fAIqTR84ief501jW7b9yGAgtfvnRJoEUsBCtLFv9IgJGAgAIcWMjatWsVCw2qHRpUSZLjhl0VdiP0CDo4S9Usj68uICDAqHX5tu6Lnm8dvuhV9+F0tUDAiqJxv4MPL348+fLmz6NPr349+/ZWL+uM7s9ftURr4szhq3g///7+/w/2Bl955EGHgXTkISAgc+ThRmVq1YUOCzTFNBNyHPgm02kmpXRSQwUskIIqsfAyiyoyWFDAACYBR0AKu6C20EkHdSjjZ8Elp6FoM80UAAGj7BNUVkmt45x3c/0D307R7SPhiqM8t1Nm7lFZpZVXYpmlllty2aWXaV02jzSCoEHHG2zMgWZjfykIoJtvwlmYfoSpSUcbjljGVj6jLHT/o4YzVYCOB60FZ1pBpsUUnJ8DFLABC6vINgttFiygoobCqbILixxxSNxLyCFn4WoEONBKOjpVRxWTQxG1z5RTXYZdLBsIsMArSH6Zq6678tqrr78CG2x6/cQTTBF82WFHX4EANmecz0IbbV8E5teXnZXB92pZuWjgGUMt/XnALhOCmpK5uh1kI0MrHlDBo7bIJksqMnBwG2gDqLLKjMf5iaOn5bKUaI8OzNVPqj9JN12r1r23ZD6pbNfBOTnhKqzFF2OcscYbc9xxW/7ws40kbdxxRx6A+OHsmtVK27LL/g1oIBxT5AnmP/uk44FnGskkmgCqqPInwMMl1KlLrWnA/0ILkMp2C20cVLAAAgLI0oKPNBZg2kEu+VsuRi7tyGOPAsBy881BUhwdq0ExaTZX53jQkijreFy33Xfjnbfee1/ZDz/TEFJEGmywkcdfBs4BB+IvM944YZBJFgcfcBTiznxnXeYPxaxUQNO3DAnAAjo9D73SjaAamjXoPVfAQSqrxMbLbJmqgkIFqglAkNircb27qL6zRtByC4giDpPRFZk82wxzlfnNrlQggAa1tM239ddjn73222NfzzBLnMGGyYbnp59jjqOfvoAM5qEHHXBA8c3lZV0O3z5we8ujBrtswOPnOZZuJak71GpyR4AKOKoFqpBd03YRi1XMywMcaP+ABhhwgAOopiW52+AGA7Ac3RXkggxggAY24IFUoAM7QcFH8nDStoppBT4Sqoko0oEkJXEvhzrcIQ976MPwWMcf8gDFH84whwUFRlnpW6LLGKOgwknGTIKoBg7NoqQ9HQABn2HNLkahu/+BESMCC2AIxVjAAGyAFxo4gAU2IIPXuUIWsZCN7Bj4wDiqIhVA06MeVTEKWbhiFa6Y4y7QYchdmKNIRTmec2AIln2wYjsMoN5RqvjDS2Iyk5rc5A/99g1PJOEHb0AQgdDUByai0mWQcVaC/jANfmjOkV+x3z504YEBXFA5qYjFiv73O6L9TzT+KlqoCOCBXVykIQPYYEr/FlBCE3qABdJMRR/7CLQ85pGaLBAFCzywgg1sYAEMsGFPsKOPsRhlH+YYFAJEoQ5ZcjKe8pwnPevJK0vq5B7dyEIS2oCHOyirDylLJUHfRD4BKWhAk3HDMGKFOSVFpxUMwCUBNoAOWv0JNBldjcA0WrqXhK40QgMeB0tqUg4GwIMp9eByDFAOn2AHnlSBqE7SIYoHDMABuWCetuzp058CNahCJQ/IpuEIN7ghEGx4Qx9UVtCnwmxObNDDZOAQDHtkC3NLwocoZHKAWKRiRcD71qjI6lFgEg1fqiAIS0a1uwoBL64zIQgAQlHF6fQEn6o6GysYUCso+USvQx0sYQtr/9jDSiUqf4sCD+YgGahCVjFIZNBg1JQDSsQDLXOhpS46EIBU7AKuG52JMEUlWrP2jGgyEUAsVOGjcP2urXKdrfBWEI6o2M87YclHLhrQGg+cKio9RSxxi2vc49LTH/aQRiHE8NjIQldOgGEMtVY5Bz3IQRHwcJtXBAvRfbRiBapY42zFhlq3+pImolLOLlLxWrOSNb7l9Z3wWNOAKEGUbjJVlTg6kLsO7HQq+0UugQts4ANrLCdhSkYU0BAH/eRBTX6AA2AAgabCRRe6Z2KfnQiBDX7wRLBjWccpLpK/+cb1vOByq7gGhWIU15c1YgtAKHCFtubFJxdxE8AGboXgH/8DOchC5th8/DEPYUChDQZ6rH6cesQMR1Y/djDcGwJhhCI8gx5JGvBXjLKOUXQOgy9OsVt9VwFujXnMIBTeckRxzp+sY7h53XJ8xLGCqWmgFc/h8pD77Oc/A5qoSZrPPIbxBCUzZspompNj4OAHKEMaDnD4QxEw69CdiNgrFPuyAihUE7Gmeb4COGZNQr27Ncs41TMxgAAYcAp8mE1I8/NJXZS0Dh0TIAALQAVOAu3rXwM72HBBUj8KHYU2SIZabHoDHEYJaShXeZRoWMQ3XsVnrVhnH/gYBQNCeABQmzqun90FCMONah7pjs0yCQAAGiCOfwgF2/lgRQcGMIAGvCL/VRQTNr/77e9/75V5SXqHMRLhBTewry/rezZkEeTEIzorDUaQBohtZhccroMVDSjji8st1wCwIhavlavHVX3q1aJ8rqsNgAFwIaSZxscco1jAMnuc7X+8GeA63znP/T0fe0yjEG1Aw5Mpy3Cokk/hfjEclZPgiXpkeiw4DFIuOhCTBIi2vCUv98/cK7Z0f13lqQZ7ylWO6gCsgG7XWccrPPDtBXjAeBWLes/rbve7G9cffksGI36QBsmUjMpOPnrjkr70wiEuDT4oBDb0Hpe06UIUF9EifdFtecsTxBUseO25xy52soNexmsGuznMcSqY/mMdMv92rU5xDkyHmO54/5897WtPz8wotxqQgIMc8KCH/PhhsoRHOmUBwdS9OEPL4Nl0K6xOofNy3exzFZdnpV/2sofe819fcwBUYY4DMCAUr9ZHP6Ij887UygOwOJLt2+/+9w9WPv+whzUoQYQhBAKhw4f2gtw3BEd4g5y1RXSMQytsnEvA1bnFWPDkWgOgQwP4yNalnAJOYAUuIGsEQAegg2etVAAowAqIggYET49NjNk4D/yhYAqqIN/sm1Q40j24QzA4gRfgx/6lTx+oSYEcVFMtSBzoAXb9wRD8QTIo31qcIEzlwzmIwgaoC03EmLoJTwaiw3aA3QVSoPaR3eXl2gKYg2t90UIs0wFswP8onEOUrOAZomEa5tCUKNg8TIMiuMEaHA6bKJ1TDZ4NBsbgMVVfuI8cmEl+/EAkwIPskQU+qRO9CcwWqRoIoV0siN711VckVuAiopwkBkAFxEIsJIqMDADcocJTzJkaiuIokiLf9AM1TMIfIJpjnI8eTtfK4GGcqIEfGANWwdxbCAUshAITJkQuodtypIIrGNAiSmLYVeEjWqFMJMBrdA5DLMAGhAIsHEwpUmM1WmPH4BYMDoMTuEEcLBmETdeAsInxLVwswkkctIEiWMOlCdhbmF8rhGBBMEqfxMQApMIoRKDYGeMjVqLJLWAuVQhBaIAovMJT7Nu1XWNCKuRCUgn/bhnZNOxeG/RFsihRfpgJdUUYxCHUHZpjYrSP+9CBEgRDnsATQm6FtU0HLIhCB4STvTGKBlRATFRI1pGUyb2VFl1QPRKAM4HgK6SDbsUHQwrlUBJlligYyHiDMwzCEKyBmsyh4VxYOZ5PR/oHYyCOnRSCNNhieQxXkJhDLrTCKHiAiTFKWZ6ETKaETHrLATyAjKgIjrTEAnSAKLACLphDlOAKDglgUfJlX/qlWxQZPVhDJEBBD+AfHfye0uXHKU1LHuCB4VHlfkTbtDRGETyC/JTTeDjP1KnNOqSDK7ACN3UAS1bQAlTKipUWiJhmCY0mCI6CK5wDCwGlVBDiX9rm/23iZiHKQzJQghuYgRz0hfFNJZoUSGJGZn80G4MoGiDAQRy4gSbEQ11s1pVQzHScwzmMgzjgQi5sZ3eGAy6IwziMwzn85M3l5nmiZ3qSRz/QAzwIwyLAQQ+MEna5z2OU43H2RymlibXQgRwIwjPMA025h5LUZlcUqHoiaIKiZwtiBqYplzUwgyDkgBsojmMIiB/8AUfip2DkgcmwzOHYiSBopYAqaIma6InqUBCBzDxoQzBEwbHMyX1uKGLkgbIw2yg5BmPqATouwjTYw16iaJAK6ZD+yhFKxT3UQzd8giCowX1444xKZlM1xvswRuEAgoUxiHMqAjVUXHjUD1rAR/+YGmiSEGmZmmlfZg799UIiuEEbtMGU1aiHls914Ucrrg9faGizFBRHMpqZuIEk+KiYnumgEmqh6oqRWYMzFEIoEV1jJIhTAghkGl9HngEaTIEyBOgsGeqmcmqnGmGaegMwaMES6EAbONsc9AEg7MVk6SCBGA4rShdV0kEcoEEiHEOmSsmSCJyn8mqv+mqXxUeY8oM7KMMnQEEaoAEaNCWWAoarMp3CyeiMuio6DsIwROevYmu2ausj5RU/zEM2CAMkGMIWEN35ONEbpMzCPSuU/gXhgKQbPEEwZEOXbmu92quv8lnmgMw2UAMzPAEREAGyUVbKSOmFQSaU8sVkvU//GxRBI1QDPYiYSd7rxFIsUd7QSbanNGRCIbxBG9zHdQkf4bArm/yhs9AqIQQDPGylrlZsy7pseh4o88yHPHhDMVCCIbQBGUjkH9IhlMqMY1zYfsZBHPyfMsgD7gXlyyrt0hLpPujrPdgDPByDJ0wBHLSBHMiB4QDCVG6o4rBPKSHUq8ZBDhBCL1gD1BUZ06rt2pbpfNCDN1TDJxSCEaBBGJyBHDbrhjrl4qBJYg7BD0gBKCzDj7Jt4RquX8pekdnDPGADM1BCIsABD+wsLFbYYuLphz6OhXqkYhzU+hgIHmDuX6CBEURBMGgDxB5u6qquehKaN/RrIqgiGawBso3S/4IQDuEISFPFqLWsyYEgjpNN5avy7nVVlpvM6hv0gCD8gjVYhohZm12sbvRKbxr6wz1A7TccwydAAiHEJxq4AWLSwfhQ1cHybqsa3Ssq3aTyxUWO0uVCCxs4EcNCQSRIQzzwA4gJ6j8A6fTyb//S3lEO2uJ6gzI8gyQIAhG4gRyIgRjcQX0u2/pgaeE4qw7m4V+or0X6xVW6CQ6mieHggRzEARlQTjBUgzzA0pJ0hcT67wqzcIG5CoPOBz/QwzxYwzBorxQYgWFKpB9aKJ42VVMVSGGQb2A43GNy7X6cEhvA6QTHwRqggSBkwfLawwmTaQtb8RX/b4gNGgyywzI8w/8mJAIfYK0ZiIEZoCPwpUzKNNtGOhlxQqq0sAEHB63xBW3HtgERwIEiMMMyNG8VY/EfA7KQqTDsgQzUygP2/oInOALkkgEZ/OaEpczWfq0FX5iFMetSIVS6vgmebuRALV2BlIwYtIEgQIIzbMMUZ85+DXIgs3IrH1aRKZc8wIM1TIMzTAIhUFoRtIEXLHAc2AF+rO/SJVzSrRJkAEj76illUoui9cXQym8wKIPKzporU3M1G5bE5iWZxrA92IM8ZMM0DEMwKHIh8IEbeIEXtAHC8fDngq/hPNYQJwaFWWX7jA/X/vAe8m7O/kEifMI0fMM8oLK++vGBWnNBG7TeMOj/WOAeLC8uO2gDN1CDM2yCJBSCEyiBEcABAruBzprx0NYg5eans86BJquM/l2XHuDBP5lBOvOBJAhDMnSDN4CD9cKyHx/0TeO0gcFy9drDPcwDOGhDNRwDMPSCJ0SCIiCCIbzBGoTBF3SBGlTqfbzBgyXGBc8JgRwIyzBGDZLj+gqIF5xBG/yBIDSCJDzCJjRDNGSDNrwDPbg1PfSDcOX0XNM1cRHooPHD4s4DPHiDN1iDMkjDMwRDJETCFFT0RV90RiNwEUD1GUB1F5xBZDepGlD2ZDepYylxU9oB4axBGuCgY6/BGlC2GKhBU38BF6B2DuDAagMBEkRBFWBCKZgC/zKQA6zV9W3jNoFlTj+ATF7n9TvAAzx8QzZQwzQYwzAYgzAEQzD0wicEwydMgiQwQnRPt3QTwnUTAiJct3ZnNyEUAiNAwnSL9yREwif0wnkLwzAg9zRMAzZ8AzzMAz3oAz3ww/7m9n3jd3JVsUBvJvSSqXD5zf1O8f166zvMw4EjeIIfuG8TeEDrb646aNradH5TeIX7lCXZ91FkuFZsuIV7+IfzHJDuKoiTeIkLVUKjnqb6N0GbeIu7uA719wkK6oxr836v+IvjeI7r+I7zeI/7+I8DeZAL+ZATeZEb+ZEjeZIr+ZIzeZM7+ZNDeZRL+ZRTeZVb+ZVjeZZr+ZZzef+Xe/mXg3mYi/mYk3mZm/mZo3maq/mas3mbu/mbw3mcy/mc03md2/md43me6/me83mf+/mfA3qgC/qgE3qhG/qhI3qiK/qiM3qjO/qjQ3qkS/qkU3qlW/qlY3qma/qmc3qne/qng3qoi/qok3qVxOxPoDoKq3rSsjp3ubojpS07dhcKpxN3objHIEmup3qr6/qqv3pW1HRYVFGv8/qu/7qvE/uxN4xYxEqyu3p5NPtVGAWLw4pVJPu1G7uzVwW1l/qv9NTx7IM+hHt0iDuTuBDyMFK6m7u6o/u6uzu4Hw/sdbiAiXu9kzu4K5IZ/gq8v3tM9fu5+3u7C3zAEzzAA3z/uY+4hOergzLJIhU8uz/8v0O8wU88kyA8sTE7zk3HvR8Pwpe7oHU8vq+7d+BQbcpU2li8xKv8wFO8uwN89XT79mwaWLLABmjAAuRkzuv8zvN8z/v8z+v8AlgACqSCLYxDr8lFPhCAAuQ80+e8ARwAzneAvnfJvq3DOdjCSoYT0HN913s90C9AA5wQLpyDC/k3WFzGK0D917N92389ArGAKtzEKi/JZYyCpwE9AqCCoOGCAGAd17cTmdI9TKGDLrQCC0CN2ys+1zsT0Rs90sc8QkeHLtSGADDKQUAAcGj+5nM+SUCAbnzIGKbQoInFOnRIR0BA5hsEBAwABwx+eWBH/yp0wFtuROqnfufjfu5zvkswygaogtmTxStcPubrfvEbf254SAWIQgk+kt0LgOoXwO0jf2qoSCuYBy5cvvSTBAGIQmCNaXzsgy2wwHYM/+cf//lnfvqfRO///utHvpc0yYS0hu3Tf/3b//3jf/7r//7Xv0HgkioAhL5/AwkWNHjQ4DoCCQpAcPgwgsOGEApwyIcQY0aNGzly3NdqAYECEx+WNHkSZUqVK1mWZDhyQAVX+zoi9HfQ1ciWO3n23DlS5Kh1NTHS/DdqAM8BrYg2bYor6c9QBI06Pbhv3QYCIhMk8PkVLMoCC0dVtXoWbVq1a9m2dfsWbly5c+nWtXu37v/NfecqcA37F3DLsSzymUWrkCRKhhQ3GMbbluY+FgQQJA58GbPXl6KM3ky7z5ZOzKN5eoXAkCEBDkMHeraK1KFpkw0LMGT1WOOrpLVZFmBBtfVZveb6drVMGnnJsR0uBsf9HHp06dOpV7d+HTtedBoW8oYQkWL48A1R8649sjzF8unPr6e9uP37+N4fDt7n2Gm+yrJPu/RaoLHsrNpHFAFEU2888c5Trz0G/2PPPfgilM898U4yLTVV8NvINYJyGmk+BxuMDzUGTyPRxAhNnO8l9k4cAIWh+kkLtq5spIjEpa6DCr0ezYOPM7f2Cqk/00jsUcIF42OQQvlSJOm9/hr/Aq8AAVa4T8AstdySyy69/BJMyEQZQLOuYjuyPB/RPA/J+dac0D02XRpLFrYUWuxMI18qgLkwEdpHlwIQwPPHN9ncE9Ej20xzzUXpC0+zIgs4AB22QAMxzUx9bHHTTg/1lFM4a2uxv9gGKOufDpuqcVQWa9PROt3aZJNNAn4zSFWi9vHgQIniDNVRYH+dVdT0lCNgpn829JPZZp19Ftpo/Qz0IeP0RNHQTzVVVFht0bTwPAuaS2ud/fLU878Aod2VgGql1Iy8CbPt1ltOue0xtgQfLeBKq1R1pV167R0Y1II/rXdW+mQbKR21YDvxwU9hrY5HQzc7KNeO9slFpFLz/6WV4G6LnffQE0/jjyINWJOW5ZZdfhnmmAcURbQEBn1UsUh1LpLnnX3uucifSy2TtwQGyGWtO/PsVSJ1nz2nsmqP8/jnqoG2GuueI51IZygLOAe4AWEB8eqysz7b7LRRJrsAVHHl6CakwFPJqweYsg4qk34eK0jXMvYIBa5RhvTkoMs8/GSdzbyR8cVXKvE7h5BVVubKLb8c88yzO2cBPEUc3F0bRR+d9NJNJ10siN2ldQW1sIo6T5ck4mDZL0cRTSfQZT+d9959R71roM8cxTmnbvow3515lj25C0vrjwAPat9IbonoLuA2vKPanecCRHELnQW4Js958v/TF33a0v9fv6S56/Nqbn6n15z++u2/H3+Cbmnw/V51b14wJLlRfT6mgX34428cUdr7pNa0+W1JMlHB0/8AeBlrqU9vMAmSWpB3vvNVy0zWwyABlTMbE55kVJGTUvckVQGwoeVhE5RdASZGnbwJkHDw+l4CiRIak1ltIiP0WL6Y564iGnFuoktMulaWPyc+EYpRjJYMFrS6wTGtgo87XwSquLsCVKBSPNRIuZYowCB24IFaykoQMYQyCmaxJ477oMkK4IEmngV55vMYrcYiKBAB5Y99/KNIBhlIQgoSXyv04HgGoIs0HiSGylMf9na0m+QpEgID+N5AHokQVQyAi+QT5RDheKH/JaJmglzThRRZ2UpXvpI6HPijcq5oHD7+cU98zOUucRnIQ1UGh6Mk4CrJtZASNpAiHBDI01L5MRDmspeYiqaadDnNarLNO+QbGogq0DAO5g6TviqABjbggHI2wJzpLOc61dlOdr7TnQ6oQEgwlbbTCABpMCSTySwkpRpO54ar65733DImteGoAA8YiUJpuFCYOLShCX2oRCPK0AHAZCsDOCS84mWaAeACliEV6UhJepYKdNGZzzzA71jaUtENKgERkI3QKPKAWnQyIbA7EzIBhFPsjKNdOyViRxOwUpce9agwbdDa/lOAcqwlj8I7GQE0dB+rXhWrWdXqVq+6jnO4/8IDwNwaJmn4ChpJUI8KoqT2xLMzUW6ScmjZVRDJehoWiOKuecXrXvXaV77+1a+BxSsLWrABnUTpRA0ZgFlL2ljHPlakkIPSHitgi3WsIx2Y1Sw+1qGOdeAjs54FbWc/G9rSkna0ojVtai+bWa92bnQYnMgDUMHJsyzQVzxFI7TEIQAGznAwXv0saldbXOIeV7XIPW1yRXtZWZqHiPwswDigCs5RkoeGN32MUayaCrIBkao+HQhS4GW91f1TOgF1Ztd2uJZ8GBZHV/tiYbhaX/ve16qqwN1MPfoK8UIWwAEWsLOqyDV+UmQBlcrOfcTXoq2pbwDZO4xOh0goZUJrHP8G+i2OdPjfusxVfRgS6gDEUd0Qs7A2BLibdPbx3LqahgCpOGtsJDkeCdvQkkAjnybDdpZ8aICui2zIAqyzD1UEtZ/hicAANDRgJz8ZytFaofWs1RUELOAcHo7LPhSwn0kyyWinGEg/fIpbBAm1p9CyxfY8R+MUp0LLdOFVWkmYyRJ/U4/CzC5BxJiWjH0EnG49TXj1Wd5+/ge90VEvJpsKV7WsQwME9NkXZ1QdViAZIvw8VZyj3GlPf/otRjTf+LCMHc9wGaXqKc8AxNxnjJBxTrrldHRagWTIQew/js7SnHOIzAFQF8+9jpRDBqBdV/sZIbVIiqAxtGl97nTYQVz/K8VyzGyKwNXDP5Y04YY867SwYrIghEkovA1qc58b3RgpYbglUmoB7UN8llGTAFidqsMYs87SvfCzPnlMX3kUzlzidZuj+2sT19U7sDq2W24SmhMrj9BnIa90Z5ho6Cx6kTq0t1ryYYEg9+yL5T7LpevDNQIYqAId8G+6Wd5yl1uFe8cpgLsXLD7lgEijFeCAKloBtoXntIw8dVqzjkwlz0lW1wvmtTYxNCUSH1yPD5JIK0Sukc4MpBYB6zXXnC1xCdaYNjcGaLVZ6BUe2/YwkRbo1irwc7m0AkoiASULUJELdNDE7S/X+96j7ExtitCF1nENgytzUQQwYAOhGMUu/zhbGDvhO7do3i2/A7awf0tkg1oK3MeFmklgp6WDZDWOxe3imVp8t9eSc9tr9kmoiq8YxxbKOI6S7mMgU3mgC6i6ro4iAALEZAMrQEU6HM934x9f7zre5kgCL3iC5EMBC2BBKmqhi3VYNS6wNiKa9+2sfhtnNrKJcZeWjszFGO3OoD+sjm1UANg/xjW20PpYjUQA4j37h6WSttjTS3b6X7vHrELbEkTIKqDSoOPUaqEBRKEWsmz3kA8CI5CVGG2bKCICvGnB8IG+KOcBzUxhgmjomOX7QqitjCTzdm39ogtPni7YhAyEio069iHrcG/YVO+sDG2JKIL0cAPjhI2g8v/OIH7s48aKyKLjJk7N8bhLApeQCaFM+ZqOIZovTA7QLbSv8whl8rxv2QjOc8ZP4PxH/zwq/dAiqpTnIeymOk7PupZnyVbPKSJpeSaJ/xTN/zDp7OLK9rZt1AxQQBCIz5oQEAOxpCiw6dQDA7PjCP0QI4AwISDv0IKo+4huC0mIBL3nAdei/ITq/FhQ/fKMlGBwOkBj/sIJAqjqBinuvN6v/2Rv6wiKLYTwkoiwKpQwrmjRFtHuFvEQOPCuQxhREH8RGF/mCZdGCrPEF4nCCg+MUELQT0ZQ3sRPxr5whGZqIjiRDNfPDBkCFBEQ6zpm9h7io/4rhtJq/yqprcoOANH/Lg8JkNsq4BI1JhjjUR5biRDPZCQuUIo8kBolIgslsXDMiwRj7B3TgtdCJ/KsEY/W8INahSmOEW6KR1mU7fxYSHKIyetw8PXMMZhArr04Tu2ELTx0TzqOUCOocB5PEiVdZhjpqBjfDS+SkT8sbJm08GSesT5qDzsykZ82cQwT8uGGpitqaCANQjJGkRxpjvVoLA5tTCNjkXDuMNtuj9t4pgiNECIDMCWzUiudpR4hpgDwMYr0UeiGUi5G0PL+USClkRVHrCetIvRc8CVWLDIKYkNmESuJkruMLKiiDlJsxcMmzvVSsSnXS0pc0b3ga+tOZAHoy6oY0zEbEzLv4zEl/zMyySyr6FIXt1IzN3NLVhI+WjJ/YNKM+JEs48IZM+gmyQ8M9YTY2tIpynB5+uMBXAEzOdC25vIWlRA3OXAddKEWPKBdwGP2YMIWHAatlhJH5vDi6lDIsC0tYLEeE4ADNsADNmA6q/M6rZM6tRM7tzM7v9M7wzM7py8VRqEWqi/LMpMz15M9paMrywMs1wJL5pM+I6M+59M2d1M/k8YRgy6ZZtIfK1EpJSfgtkQnmwn9Ds4FlUMDOsBBH9RBV+BBJTRCIRRCKbQDMFRDO2ADNOAAKCMmTQkBGuMvW4+uBJOtNpK9Nu45ZenFxsOXYlRGZ5RGfUmjfu+iFsABRCE92//TR38ULzyTm17oM1ghFY4USZNUSZeUSZt0SWUAaRhRNMcSWk6Tlh7CCw00BammGl2zKWDz/17iohrqosqUTM/0TM1UTdF0loTp5hhgHLQMDqONKVPUKXfMOeVqTMAPbfr0oKwGxXJpAFagR4HUUA+VLd6TNuLTX5RlA+jt9yJVoyZVUiuVUi/VUi1VAO7v3vjjA0mzSicRIBfmBAWkIC/pzBDSLQNtKuHSKa0tTOvRTeekAoCtRDESRamNFfPMMIWDkwDzRUfJVYW1VYuVWMcKYqgOUZeVWZtCSKOQSM+CJjoAPRCkRK61aLIVXrZVdeCj1SYsB3kqEkVwC22SQFX/cxrH5zRU9TWx8U8TM1ZnT15B8rBSgwA6oFLibByRcySU8znUy9oMU7w8oxXYbGje1U8TFh2bKgG2QruaFWIj1iAU1RDlk1pLCSUGoLaktD/3kTFKEy7MkhJNEGSb4kB9zUuJIvSI9VjhdWFldRjbz/c6IFnMAggB80Q/Zgcfowej7geR7R/OgQTLa1iD1WiLVl7NhyEW4BAl1mmX9Vm/KFqbwjUuFmNNQmPvUoEo7OjOqGTdwkqRqRSjUUtzkDXtDOre1RNhFWbZNlIiYOccCWNmrHDoNOwG09qg0leVRSqHTWH/FmFzCIMatlSf1nB9VFHvsWkHxGqvFhxrq5g8/zXW/jNU/7EEsbRAteRA1TVBW/Bo2/Zl53VtGQRuV+HuRA5nzUtnVZEOd3WgcJJqleV2gmx0WfZzbddVu0beDmBqD9d3OVNIKQI0nWIfqLVe2RG64ANBknd5EyRrpZRrR/NjKxf8rhTz0HUtNS1lawJMy4YvvzdmKZAvI6Y2PCAX5pIt9pVOkxNv0VFgXWcd4s1tAxdwAzYMHSIiBIAVvvZ3+9fTFJUhGBUtjHd5z29B0kPqkuSHsjWTILdTrVe6+pFcLVdwMJd/O+JU41AMFdRYwxe7wld0Fxa7GpZP5DZ9TVR1/Yl1l9N1V9QhOSnr3DVe6Tdwx3ebFsaO/FeHtf8yeKXWYtnGgBXYSQ5YXxTYaBz4trh2cisCQCe4ejcsSzV3SxGUXb9Uhr1X+Q7WwKZMxLZ4XmNyLCCgydZiTvWvTnXVwP5Pb12HQLTOdr8MjtcnjuH4//4usYykVndYj08SgL9yca2icZ/HeWZKMRr40TqWSvltC88yhCxRLXGILTm4aFdojuXYkuk49WbD/rDkFANzdQdTfHs1dgtimfIhFeLLjB1Xb15VagKYIg7gFvZYloNRSJmvd4mXgHGXcJyHxkRsrLKWXKJX1qiXC0kiilHQbCM52PhyqLZCJJ65j6BZmqOZmqdZmhFA7qo1QTRZFcj4ONe3X9u3jvPUUnb/gVd+D5tqVJ3XOZA4yjsW5iXOZBVmmZ4DkWL9mC0Mi519CaFSiHGEDZgfON9kkphH9iYvmCM2F9qqWGUVUva4KBV0YRzMwRx0wRx2gaIx+qLN4Rw02qMzmqM/eqM1+hzOoRZGgQVO6p1bdeYqxSSJInUPTQdX+F9zjBB/dsvSgRVWYAPEZwB+GqiDWqiHmqiL2qgHgN7ojUzKZIhoQwbqGaqXMGqHtyn2IRQWoAKyWqu3mqu7ep60mmgWNqCT2D+lixnDJGwjb2yxF5K1N22VxzhsKi/xi66vSlmwbx/QYTJY+T9sxTiV0m4pwl95kDmfkpzZQi/uw6tuIRfOExdq/+GxIxuyJ1uyK5uyL9uyLRuycQEXcoEF4mWPOCwFopq0ke+eE+CP8yMf1mG1W5u1X9u1Y5u182G1w4eumHmsrUIsJQ+h1WIEDZpAe1sjMviDVnB7O+ItuQ09tvEPi8dvmptFn/sPsWQdVsCYFhTBUrsjYjoHaYimCbuFCxN2/QyB9EI9Y1DZcIiQC8AFStu9926qbzkU4W1wWzW3nQIml3hcm7Fcwy81H9mIJoihufeKt9VoagE3ynvM/kENbdghBIA28W8ijUQipi320pisDhtzTG+fzkyEKuK9Q7zlEhefnU8vFgABtm1n7rspdtushRstRHZUgxvAl8e4OViYzP+Eue9CVSDNoadkJFhAHFFYpr0blJO2I+HPOTyDyZfcyTeuyaE8OFyjFjSMXxPAAkRcy8+th6k6FBXgn4mVxZFRmHmbmC3Dc0TZVJmmmdB2maXK7BA8wYlSn8tGIgxIwsFOsMXZDjVcc6r8eC+pALJ8ywu90/pYgK0jfv25bMa8JjxQv5uYvyl4QMcWxjHiZBf6uDkiue0YAtBwvuHLE6vlAO4Ipom8u3cWL3r2denCH5SQJK8D0L+Mygbd0G/9ybpcvkMRxQnTgxy9I6YUzc4aTNLaw495zU+0zQccuQt8dVSRJKPd3qSd2qfd6nIZzgtgmdxOfVM5nO0Ui9+X4ez/zSj04YUtBdB3+WNsHdfbHcAQXbtZTAEUJvWAXYEQ2cwVmdIpuHBzcopRFscpefTk3Cr3QdRfTFDwjkW3G9UzEtyLlSGSfNyvIjvSHVXfg9DdXeMH0dq80stjt9pD/k/izSDNx943Ir9jZ3oVuf38myKQPTsGLqVu/M3dSkwJHgFfnYD3Rj22/Q2PE8X2HNyByM8tZR3CgRXwwfmwzspZ8T8yfuOjHpZuukRaOktQrYTo7+THqGNljnZCtYs/psNqPLTXddM3otNjU8WuMi7+TJ91OQGag9uJPOuNnDpuQjdyywVpaLyJFyu8ShVYYAMYAKnNQUBwwbfeZyEhAOql/97xpegJUwjBdp3FGmztimTrM2JK14bYv+TIeIpQGrYFLh0hyg/NyYPZOd3ZSWLH7+Lq9sEBWBVZ477QlBFS7vbu9yHvVc17JZ54/yEfzKEWRAEFGvaQRuLzZN3Kw/xEGv/xnz9/orM+XIj0OWnegWhdkVi3+3PKKAIBLAJaSO4KE4MA+j4Uy0/Efij10f7Hl7fYQNYwitddc1zSayKmG0gbv/sxlC1x4JLvPwMg9o1ywCDBgAEFEhZIAAFCwnP/IkqcSLGixYsYKdYSUABCggQLPTYEWcBCxpMoU6pcybKly5cwY8qcSbOmzZs4c+rcybOmSIYMOzJsGMHhAog9W//uW4BApFCHPyEMQCVzHYKODX8ObZhww76kO1URyApyJNasBUSBhekhIdSzXIEOEDfTldunQB12LDCg1te1+/7+8/dv3wa3efNiTZAvImGWowYAhRvUY0JWazPuqzVg5MeoldPue6x0HwOOWVOnLjAuM8rNqCcDDZrApOvbuHPr3s27t+/fwJM6jQrXKFLSvdcxFQkaqkGqMdcR2JoXKlcEXoOzFEsWa8jUBNRqn9h2b1SzHufWRTycNkgCrXQ/RlfBfOX7Hhu/jDxZdVmQD0DXWyudfaeYUw+I91JgDRSnml6tBbcRYk8JlZBt42Wo4YYcdujhhyCeRFxzXB0lEXL/t5G2lH3tiTRVVVetFldc2AkWYkSsjIXeeUWFx2F55qm2mHoy2cViYkL1hRthjxnp3Igj6ecSf5aR5dxCBWDmG2fWPYmkWiimRNg+GnRU1IMKIYDUbkxGNCFXzM1WAAc31mnnnXjmqWeIVVZoWVAFmGgjaYSeaKhjhw6WaJiFrXgWXs69yGhK0m0V51sJEMDBX4Ui6qmin3YK6qiiTlqRLQJ0h5aQCoa66KuuxkqqoeUNF2eSdBXJnp9bDRDfqGCNiQ4Hd7Xolkf6fKoSlWVZ+ZSvE5UK66yytikRZ8XxKlWrLu3TYHdDYVmAmoJJW+20olK0Dy4IrToUSB+lsOe8//TWa++9+J50pFP/BYpOYIEVJvBXBA9scMEIH6wwwQAv5Z+XDgUI41nvdukQB+skrPHCHG/s8cEBYzROu1WO1GUBMgDc8coft9wxsSEthlZQRMbkJKSzcdWKjSz3vLFEgd1ymJWJvZUfTJHtqCpIvrrstM8DN8zlo80llAqwLTW4GLx/LqRLwE+H7bPUnckoWwLy5qv22my37bZuEWhLG1kMaNBAAxtosMHeevO9d95/9w2434QPbnjedt/dgAbhQlyhK1ivVKmMRpeot+CCG4554JwXvrnnnDPwK0a6pDrjzOZVYLfmfx/+Oeugx856feYV12+ENrOH33cFsBCLK/+u2BJ8LLYUT7zxxdtyvPLJL++88sC7MgoLh/E+nJ9Hv8SKZCVfb1kFnQ/e9/iEky9++X7fnfgCSu/ukCoyGXbsf2XtVYH64ZsP+/6vj6/43RVoHMX+JIO3GfCACEygAi1yAWM5xFJ6UYgEJ0jBClrwggrhSgYhEDf8/CQhtphYat4FQZJg8IQoTKEEpXIKgV1EF5Sh2ELCpcIa2vCE9asYnKCyJpjcrDlccwgBhoiQARDAiEg8ohKTyMQlOtGIfCFAAaRomZAUDVAj4VRLblbCB4rrhmC0YAQ9A7G8DCAWMBkTCo41QpGcaVxhjOMGx8jB75hshgWA3wL3yMc++rH/ThxgDokeRBb6CcWQHkHkRw7JyEQ+hZDneWCzPLKLqkyHaJXzjJwcyclFdlKRoGykJz9CAAFd5ByfISSWqJPIRG5ylKH8pChj+Zn/VI53FUDHerSFR4pB0EomC6YgWSnM7nUymJVxSHZecgvrBCkh9dPKIWdDTVda8yPVxOY1IVlL7wWlh90SxSXb+EuuafOctExnI7n5Lsok4BZ/jKc850lP18xgkl0qGs6AyM8r9pNEFauOWdioASm9xCpw0dY//cnQhTqUZqjgGUXWMbQz6cV7T9pnQzf60KqhLocHMFMBlpm7fbnPlrx04Ekz6lFi/qRHqZAoSgijHA465ABe/+SazA600pT21Fb4YeheENAYmarEFaip4mce6U+NdpSjAbXVn2waKF3U86pYzapWU3ILT84oqCP6qVhZulJ9MmeVWIJAdoyKkslhkqdkjatP5arSjI7iJPsQ5whj5tC5+rWucwXX/My0LZo4Ca5Qfapi92lHqQrAqgsiE2GPxVezKTaxicULvIpVgAawVSXpiJEmIXbHsNJ1rH+tThAdsoJ1bPW1sI2tPCkq2irB1amYvWxd3YNN2hBgFJ9ta23LqtvcGrchLzpJjuSEgNu26LjQ/eeqGruaVyhri8VyLmoBy13ErlIxnyHABgzKEsKIwkHoYeV21xtXsKI1LplSS/9wT0JR9AZzp2Usrn4tNEkBaEm2AA6wgNe2j1Yk1TNOTe1pF5xgfrnzIfPNiFvh1NTuMtjCKS1lcNMRUnhhk6n8VLCILVzaB51pAVKKsEUOm9/ouji/aNXnFHcmk3GYzqLTdZd+j8vYrq0mIZCN3yh0BMn2YvjIZcXZow5QjgE7+clQvpM6OkxOI784ugkeypky5YG/qLgiCG2j3HZM5ivyxZQX2UcqpNhFlD63zHAWJFFGIgDgRo4lP2TviMfqXSsSjQBdjsmYOkBkbpp2zxc2q2xsB+gvZwQd+FSlbVsM5wo3tCEEWIGjo8zpTnuaJ/vgzoOuXOmWXslBQN40mMf/SeFSk7qhBbgrRvzxFXQsZ86UfnWpH6nKQOGjJizWNal73OpEAtq1NMkFNIVUWmF3tMfHsui4KlkTgRRaRt5xtX6lOrOMffrb4A53T9bBAskI9EC1M1q6163udrMbtxbjigBUoWow17Y973a3vvPN730jN6KzbpQtpEjY3fq73wg/OGlNtlkEmKPeFclzoieuYMRm5Y3vSYFrv2KqlISaIx5+GKJHbmqBcoUAqkB2TcgtgHPu0JneS7jM9X2gHxPAFSETt853zvOV7IPcBMevs1885hFKkRU5jw4VdTx0bTOklBkZU2BasQDKvjXXTu8rOUm5gK/ZROJN5/F5Hpkm/wIsoBXevrNSWsFmkrAS3mGH8WreA1yIX4TlzcUjULOeWy+JCwE7K1jPB0/4no/pH+tgBQOGuCuFz/zxdKVsAQRAgA54feX3hrzmHa/wqQS3HxHZxzk6gACE7GXzqOd8up3zngEgoAPnsHvEM5j62rc7NGSfYqBG4e1yzSQwutjAEWkPJ9tvPjSsT8gQNxB7F87kMYFRxQIYD81sqf76CK9QQhDSgYcX/vvgF7c/Dr+PdKCCBfU5iBzXD0b1b6AFuQhZx1UiHfbbf/0DaOFJxg80XYgikKZ3fwKIQgchGRwgCrsANl83gAxoQQVYAR6QCrqgcogyfz63D7YgChbAF/8B2ID3534sEH+cYoEYoUW0Fhjr0AossIEC0IEeKEdFNFISOILhV4M2+GRh8nPjoAuqwAIr0AGtE4RCOIREWIRFiDcbgIQbsAIsIAquoAvlkHY2AXr5cDdGeIVYmIVaSIQNMDossQ/5cA7j0AqiEAorkIRbmIZqWIQswAKj4AricA75IHsYEQ5KuIZ4qIVIiDdM6IS5UA7pIHhgERhhKA5k6IN5mIits4d704etoAtySIcXuA7lkAujIAor4AGKuIl/w4dN6ArjEIU3OIqkGG4Nc4qomIqquIqs2IpJxxP94IqyOIu0WIugB3rx0yi1uIu8uIprEYu9GIzC+Iq9MYz/xuiKwXGMyqiAgliKzviM0EgvXuZ8fNSM2mGN0VgR06hV2Oga0yiJ2RiO4jiOOUGC1UiOdwKON2iO6NiO7kgvj8GOuWEtSSGP0YiL8pQu9RSPsEWP7/iPABmQAjmQBFmQBnmQCJmQCrmQDNmQDvmQEBmREjmRFFmRFnmRGJmRGumQ9riRHvmRIBmSGNmRIlmSJnmSKJmSKrmSLNmSLvmSMBmTMjmTNFmTNnmTOJmTOrmTPNmTPvmTQBmUQjmURFmURnmUSJmUSrmUTNmUTvmUUBmVUjmVVFmVVnmVWJmVWrmVQKmOXPmVmeGVYDmWwaKNQKMTEYaPEyGIYvkSyEGS//LUljvBcRaxjeOhlkuSIXipEnI5l2nWl2QZmDtBGqDnD3u5lzdhgtESergxKJHzloJGEdDnG/yodtWmIYHBfyeimb4hGCojmTnBMHVCl9eVjhGRLNFyeGcpmKypIf5gD/wAm/ZwD7HJDxOBmNU2fvxQm7u5m8gBeoC5lgLDD7TZm/3Qm7t5D8qJnMzZnM5Jm/dwnLDJD6q5JMSZnNiJnMfJD9vJndrJnP5AnZqpIhcBlzQxGtfZm8pJD7ZZjIupm8XJnNvZnfNpnMjpD/FJnYpinvKRntlZmcLpmvSgnPl5D/ZQna2ZoNrhD+4wBVewCI6wCIuQCI4wDPqZmBbhD//foAUOOgUeughRgAx16Rr80ARTIKFT4AiK4AgR+qCLcKInCqESGqEzKqE2SqMy+qIeOgWJUAiOMA/U6BrAUAg2WqRGKgUSiqRJ+qJLqqRSMAmbwAzSYA3e8A4HaiqcqRvuQKSLsKKJgKJY8A6rOY//QA/wQA3DkAlZcAUxWqRI+qZLughKKqc2uqMSqgiUEAzTwA5XmiFGRQ/BoAgQiqOC+gjywCjBGRPz8AiOcKJfaqNX4A7qoqCU6hveoANJAAdz8AZtMAdwQAS9YA+DoY48ww/YEAVEAAdwQAdw8AZwwAOmgJuZQQ+p2qquSgduQAdygKtvEAdz4Kty8Kt0AKz/vzoHwXqsxjoHxeoGfpAEb8AGc2AE8MAbvrAFyuoGyoqryvoGb5AGz/qt3soG4Pqt3xoHcUAGZOAFRCAIkNALxwAPBzqibMIOYwAHu7qrrmoETiCm/CkT42cNkSAHZkAGbuCqb7CpB+ut5Cqu4TquDMutwHqwbgAHPWAEkkAN9+CnFWGYw2CvcpCr9mqvdDAEnxCvIRIPe1CwcpAHwdqtSqAN5FmpMjuP34AGgDAHeZCzygoHQ2CojYKhhjkNcNAGdHAHd5AHyjoHXQAMvmEPuYqzSKusOpu0OcutCGu1WMutWnuwBxu1B2sHgAAH8EBruhEMYZC0aCu1dMCya7ur/2z7tnKgB2tLB3SQtFw7tEPQBkbQCM8ADv2KE97ABXU7B2u7rXtQD2N6G/xADUCwqoWrrNDKBkibB21buZQLt3OLB1CbB9D6Bn1gtW1QBMmgn4m6IIMxDamqB4RLuG9At3HwBkPADKIaIuCwB3Rws3eQtH1gBN8ws77LJt9ABkhrB3PABoBwsHrgBoowtuZpIxzHD8OgBKuas5yrrH2wtH8bE/WArdBqB56Ls8XrB35gtWlbvuZLtdvKtUbgt7vxC2dbvJB7s4Qrt/RLB/V7v7pqv7c7vgebtMfbrT+wBL3gDtlLE+6wBf3bv9D6B/LQG/4QD4Wwqm9wvGxQwdQ7B//4K7d4oL/4u8Fyi7R9wAYh3AdSmwZQIKkbMhr+4A1OEKxXe7xpWwTHMJn/IKu9AQ5/ULchDLl+AAffUMC/G8Qz9Q1m4LXi+wY5iwdy4ATWYJg1MX728AtFUASDy7mey63YmxuCYQ9U/L2RmwfcqgM7sANDUMZD0ANljMZnnMZsjMZqnANm3AM9oANCAA823BNme75zgAZp0AZpwMd+DMh9/MeDHMicmq0XDLZzQMJDoATJIKp3DBbsYK39q6zEy8C90Q/WoAPD+gZ+8MVs0AZo0AZDMMiCLMiBbMp/PARF8AatDAhsYAd2kAd2QAc8oAxAjBP+wA5LAAeqW7x0wK3//wu+gVAE0xDJv1G7SAutUvsGf5ANuSzE0mwRRKwHXBvMXUu5cPAH00CdyBx18/AJPfAGgaDAgNAH46u0wICg3ui027rIOFu3z/AO7vAO9mzP9UzP+LzP93zP9ZzP/vwO7Vm27wu+dRsHleAMCu0Mw7DQDO3QDa3Qz+AMwkAJklAIf0AG5qoHeeCrrYuzb6ADnwCkvsEOX1C3x1u4bPAHJM0mwFAE4Cu1yooI0PAMzxDRD73QOJ3TCt3Qw/AMzSAJrXu0U4sGzGCbpesS8eAIrUq8dmu1g5utTgCzQToe4MAHR0vCOKu6vBvN0/zVEUHEwey48ZvScBAExHChMSEP/4/QA3TAzJCrtTi7BcPAzq5hD3KQu0lLt3CwB03sEl6dE75Q0Mp6s2twCW2S2Pu52EwCxd+gDJGQBD8A1YBAvXrQAz8a2Cph0le7uiuNuLzhD8KwA5VNuXdQt3AwCX2q2KzN2IMxfsSQA0Wbu578q5Mw0ONBD5mwqvA7B4GABpYADeMbzJtKuG5QCPFg18Fx1bR9sPb7BrybZmA93S8RvDdLB5pbvf57sG1Qssp9ltYyGvKgCGhwu5/71pz7uVCbxUltEfbQqat7tXDgDX/b3iyRx2gbzGuwCX87ft7QC0LAB8QNv5RbBJFwD5qNEgdc3B+9qYEgpqEtDEMAv3QbzP9tQAm4nUb/UAxcgLN3IK4HGweOkLEZwg/C0AaautdyMAjywA+/QAQ4+7nXHQeSMA/f7Rvy8Ad73b+s2rvU/eMzsQ1o4Nyqm7PoTLebyrk9AAk2XtUVKBGF6Q1QMARrK649rAd2AOLgu7SWCRb24K3xnbRt8MPnqSH4jbZvgAaUgBP3UA170KoWLL98MATGkOAnseDbSrfiygcNHNrOQAZzAMur+wZrQAnfvH/HILyxPAd+QLhwMAUIPh7+kAxuHbV16wZ7QNX8EAnYSrzke+GzuyE53r948Kw76+NAnuousQ1q4NxdGwiX0Mt6sLLo3MOKkNxOvrH+QA1OAAd+ULf/SFsEl2AJa5AH5420Wcwb/PAD0BrVHd0G3gAT9g0TZ17YdJAG/G0TuqkMRtC1VMuqUNDSuTHJrMsGdJvS4j6PJ97bdNsH+53haUQM9brHXcADbeAGSqAF8J7J1jC9dhsIQyAN7ekP8hAFacDRCLupQ2ChyDHtGZHjgT4Hmtu/PqzqFs8SQu7c4xsHiPANx2AE9ivCw90GhADN+oiLo5EMbhCsN0u5bTAJ32AJ5c3j6mznJ0EPYD64gDC+cPDXYjKpHeK+5Uvoa24THOcPvYAGCK+sHE0HP4DLu4Hni6znb8DnDvzn8IzBm1roh34SyEAEUTAJnyAMyTAN1LANTQ4c/6MBD4Pgq1HrqTkgDCTuGN6gBHGgs1A9BKMr6n8grkje7BVfmhc/+BEh5MaLzXMgCODAD9LgBm0ACLAsv858DUidEfxwDEXgy7RMuUMgCfIwD4mQtsi+zr3x5c6dtJ3rDPCw+qy/DfDg+rD/+vHA+hDuIdWerWqemKPxD/KgqarLrXmgB68bCV1/E+RO3INLzum+JOsuroPO9TZBD1Y6D1faD+Nn8ywhD4uArYssuXscCaIaMilPxTpLwnRrBNiA/S0B8Tdb6lod+IQf/xWR8VC7qYVwqPwwDU6QBnmQu31wvAARx000fv/2/UOYEOG8YG3gvJljx06eIZTm/ZvnyP8NnTkdO/IA5k/hSJIlTZ4kye8HmzkcW87JkweOESNw3MApMlNnzppFihgR0useSqJFjRYNFsZjR0Bz0GwSeVQqP0ttOPqBGHMOFH79pH4l6m6Lxz506LB5w0ceWLYo/TkjMwcQG7Nz3qzZVLCtwoMIo+4FLNWepBwd+/Tp+KbQWn99E9oTNoROTDZN87gR9O3f38CdE8r7A3EOnjcs58DR7Fn1atatXb+GHVv2bNq1bd/GnRvlNjUQ9dg5Xejdv37+qkGJg2cOVjZ69BAJZo/vQX/1KMmRIxcinR7RN9u74ibP0jlnQupWSC9NR5eBYI5/M/mOHUAQ7duVy2b8kE7/Q9FLTYq8ltKgBDZ/mEHDLD/8eG8OJC5CTyyP4ktMrf/8EqaNOdBqiY4+1qDEq9ccM+jChPzxh59gKJwDsZac+Iazkea5Ygg97tjQozamkAdFE0cCTa7R8DsttR+PRDJJJZdkskknn0RyGzQgugPHNwaJxy9/skFkozdKA26OIiwi7kR3FkEjjzv0+JKOIoa5JyrwsiOvi/MutCeN7VrCCpDxGMxDohxZInRDNiaagxJ7/iIxyV+UIu+uAl/rRxov6PgSK5jmEGIeEXNjZyxM6aKjPjog/O+tuFzk6K68Zmv0Qn74GSanjvTLgw4jlBEpVhHBicINPeDLaogsFv30/0fQ0KqLJTpQOxFKaaeltlprr8U2W6KkJHW8ObBUiJ94FjEiEJfsCqSNR+BBEcVtoljPtEBmSsZHhOhRJI7ERLNzM1lXesnbQN+Ao6439M0D4Tn0tcsPN9ygRC8oAyQvkKdkVM0fYxIs9UuY3hCiR4RilU1Cw8xCy8IL/clww6bM+jBEbdsy7o+Cs8oVjmF6dasaJQrOw8WWxlw0ySCbIs1FaGdmummnn4Y6aqilLAs/OQxB1S95ICFiMrvmyqONKLpB0ZpB5MC0vjkwo0ZGehaRg9iO7MQYN/X2DLSPygKBAopB+naCkCUICbxwwft+wpdknaR4KUkNpMQqHDvyVv+JokmWLdSW9kQ3a91UtYtUu/CSuLa6TWdHCK8pj6MIT+IkKVZ/jjFisjw83m+Y0k0ETTTSnF1aauGHJ754448HjNvNF0ZkrZH8mYeSrm9lo7k3luhGGkHkwAMPQPwApA1CsFkco3zzQEu0MO78L0+8W0wjjWnsoYcee+6fx57899ef/vrpoVbjmEIHNEzKNfyQQhz0gAdvfSwK0hkZbkzWIpSlxXmpatmGzgWi3QHGH/rgx+lM5I4pwCEmDZwDIr5xD3rwo4VdmVU/YsiP+8lDEsmBz3vegIR6IeloQxJN8JA3RCIW0YhHlJaUtjMqQwynJJApgtfe4KI3xA8ONxr/Dx3coAh2PTERpWHP3NiHHpU4iyxV9MZq6iZC2gToXKMzIM3KNEd/eEMydNCDS5oShp39Z4IssqCJQLcpBtkhDhHLWDHa0IhOGGMZ3IBH/tjoGV/5ZR6SIAP6IhWISiDCEJ1EBCIOgYhChHKUhCAEIiphLrPUZQ566MgQyBYVzNFmWfHhiLPoEKNaItGXvwRmMIVpFN5sRw/VI8QFnweZKx7TD60iD0eG4Ih4nG4eiYiDaVzSr0nKRj1h1E4f0GAkSo6kl22ElLfqQ6BulqRR+6AOPR6hoXPFAVNrsMaFNDcqs5jKc7k5kIbmcIc3gO8Oh/RPZ/yRjC/E4Qdo6IEQ/5ywiCwAo4O0EYk9KLER9JmmMl8qTUhDdzCRxk80HHlIU26FKSewq52x+SFp5qLFbTQmIeccZk51ulOeNk2Jr7wnIf6JEH384x7SgAPa7HKzpfjhB5OYB4rgSRJ7JKJQdaGbidz3klu9YZzHE2BiQPRSd44MntC7hFUyFRE9yKENkLiobSZYlrMEcmXCMAPl8HPQ1+HUKMSoAyyvMoc2uOEKi5tqbKgzjJsAIg1gM42QBKRSyXpEK0SyA0vqUzBw8S40HfldS/aQjRL11LSnRW1qk4gGXSLzn39B0TQE0YZcPas08DFCJ6LKM6piU5uYyuq/zLipPrQhjcYLa1MKyP+ae3yDEWhQTkH1FpNABEEbZHXNPjkXn6HeRlXjyWHYKDEUvxKFGGAYz7AClYdAOCKEt6FOMtywLz+d0CMcOdfqVhcT5+C3I3a4A/rggx1FYbc1MQ0dHfawDdU22MEPhjBtqGaW27EhmSb5lD+6MQh7YgoxLEHDMKTTK5JdE4xcDe5/7haw272hDTEyUATR8ygBPS4w/uhHPLThiSQUAbyVOVRHiDBG3czVYIHorun8MYxMTuiVCF1NMbhABwYZKg9puEJcY+MPazhBX5UJYxvEPIQhFIHMQ2gDGdBAhjSvOc1iHnOZ04CGgYI3Dy8jQ3QMvJpbNqsjf/AGZ/YcYUL/F9rQPVUeytggiH/2xSvU+UYiNAQRtPzhGVpWSFWzyR6OcBNPemLxUrFxj1mlCMchLE6qcbxqVcswRbOK0z3sAbv/hPVWl0A1q09tan6QelY0pAc4rDEMSiQCDm340lxggihA6MAX773QHyuosv/cI0N00Jfv4nCJePza1anO9bdZDUMaGqMLBb0PWsY7aKk4xh/wiAIc5qKflhhhCs+QRr6TcYxk7Lvf/Pa3v48xcIFLwxmJqN0rI8uHHDgDduXl8x+ElLSOGGHUvr5Hxkm961brutUhjOGhRT5ykkftp3gsDSGcSBQUgWMRP1hYG/4wjZdWtQ0syRVEUkxGgJ3L/1SNiAQlhE6JoBNd6EVH+tGVPnShT4ISk4BEMcpnGxrX2BBLT3rQnf70SUhiEU5QQhB+kIY1xGE8DAMER9b0hiIIA4Im0i6pEENt9MyuMHYRTR75cIlHGN3vSf871ifRCULop034eQMiZQNP6sRDEXCwy3TziITrtsvyIvEK5v2V+c1cHt7CaoN78B4IIkiDlhDvTO9AC0aONGISTr9EJF5PiU4EHfC3x3oktLDykvfe979vUjGB6tqjiKQekDCCGwZhDbJqWptiZLdqvsnV+LxBOUXADnbWsH3ud9/73mdDHJDthjjQGQe9mHptqh6pOXy/+2xAtoYE6l/w6nVCdP/4gxuekAxoj3AsiXGJN0AyQVoGybiPDnmWmyA792PA9yuzPtAKs4AlwpKG8kE9ougLf9Aoq5ALVikCZeA8thC0fugHbSCCQFiQmNisN/iDbPCRCwQMBMs7O2gD7Ii/OIgDOGjAHVwDNiiCG8gn4BPCISRC3Di5s1i0JCsJf6CHbbCGaiqtoqiqE8MqIsuNrTqX+IguNWGgKWIDvSkNvQHDLyTDMOSQN2gKlkgDUkg/2li/SBlDMUSLPhANAaEcrTipOcgOnECCYBAZGfuPfaKguqI79JAHQXiI+7IL/GoKOfRCMHREMqTDpRAU21kOJYgHC2QN6uAHBNGOjtADnPj/hKKZo8Dwin6Ihh74LPQxlTl4ghjhLVv6A2bJJb3CJUwhpEdEC0ncxUjUG0AoAhcswmEkxmLsDKppCTaxMGWSI5tCCedjD50DBn/5jzIKmJa4ojlIguqxw270xi/BlTnoATb8j0epQ28UEE3KEXXktECowzg4A/ERBngoiE+BQdYwme0qxNzYB36IBi+gEPhwxxxBx4K0py+JCdxJAx84BkxjDX/cgy/ECgBzE0gAB5GwqXu8qb6wh09IkP8SkmdZjOhji5iaIsfBo9BJmII0SDTAhlg0xpiUyZlEiG04A98IKiU8CRiExg6Zg53TjRV7I4/wg/TZjiyCCY7IlaRk98qlXAp6QwNSuJA3FBCltEquskNnIRg4+IM/UIIngIRhsIbdYhJBpCu0GEBB4gdqcITasY86NKOllMurnEvqmxA48IMo4BXb8Idr2IO37IiDKgR3IEmT6AvrwEGQZA83gISoug3VG41ztAt7sixFrMvLpEs0EEaa5MzOHMYjRCbem42exEWgvEJQG0qWVM109Ag06IXC3AtbW0105IhKqIRLaIZq8AbHfJJ8pL59/Jx/gAdpuARDIMjEmE3VDARLcAZ2wI15KAQ0oKzEiANngE2iyAY+kMxFHIJnuE6jgMxaTM3kLMhg9MzzRM/0VM/1ZM/2dM/fCwgAOw==";
				JSONDataPDF.hcalogo = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAAA+CAYAAACWX20oAAAKQWlDQ1BJQ0MgUHJvZmlsZQAASA2dlndUU9kWh8+9N73QEiIgJfQaegkg0jtIFQRRiUmAUAKGhCZ2RAVGFBEpVmRUwAFHhyJjRRQLg4Ji1wnyEFDGwVFEReXdjGsJ7601896a/cdZ39nnt9fZZ+9917oAUPyCBMJ0WAGANKFYFO7rwVwSE8vE9wIYEAEOWAHA4WZmBEf4RALU/L09mZmoSMaz9u4ugGS72yy/UCZz1v9/kSI3QyQGAApF1TY8fiYX5QKUU7PFGTL/BMr0lSkyhjEyFqEJoqwi48SvbPan5iu7yZiXJuShGlnOGbw0noy7UN6aJeGjjAShXJgl4GejfAdlvVRJmgDl9yjT0/icTAAwFJlfzOcmoWyJMkUUGe6J8gIACJTEObxyDov5OWieAHimZ+SKBIlJYqYR15hp5ejIZvrxs1P5YjErlMNN4Yh4TM/0tAyOMBeAr2+WRQElWW2ZaJHtrRzt7VnW5mj5v9nfHn5T/T3IevtV8Sbsz55BjJ5Z32zsrC+9FgD2JFqbHbO+lVUAtG0GQOXhrE/vIADyBQC03pzzHoZsXpLE4gwnC4vs7GxzAZ9rLivoN/ufgm/Kv4Y595nL7vtWO6YXP4EjSRUzZUXlpqemS0TMzAwOl89k/fcQ/+PAOWnNycMsnJ/AF/GF6FVR6JQJhIlou4U8gViQLmQKhH/V4X8YNicHGX6daxRodV8AfYU5ULhJB8hvPQBDIwMkbj96An3rWxAxCsi+vGitka9zjzJ6/uf6Hwtcim7hTEEiU+b2DI9kciWiLBmj34RswQISkAd0oAo0gS4wAixgDRyAM3AD3iAAhIBIEAOWAy5IAmlABLJBPtgACkEx2AF2g2pwANSBetAEToI2cAZcBFfADXALDIBHQAqGwUswAd6BaQiC8BAVokGqkBakD5lC1hAbWgh5Q0FQOBQDxUOJkBCSQPnQJqgYKoOqoUNQPfQjdBq6CF2D+qAH0CA0Bv0BfYQRmALTYQ3YALaA2bA7HAhHwsvgRHgVnAcXwNvhSrgWPg63whfhG/AALIVfwpMIQMgIA9FGWAgb8URCkFgkAREha5EipAKpRZqQDqQbuY1IkXHkAwaHoWGYGBbGGeOHWYzhYlZh1mJKMNWYY5hWTBfmNmYQM4H5gqVi1bGmWCesP3YJNhGbjS3EVmCPYFuwl7ED2GHsOxwOx8AZ4hxwfrgYXDJuNa4Etw/XjLuA68MN4SbxeLwq3hTvgg/Bc/BifCG+Cn8cfx7fjx/GvyeQCVoEa4IPIZYgJGwkVBAaCOcI/YQRwjRRgahPdCKGEHnEXGIpsY7YQbxJHCZOkxRJhiQXUiQpmbSBVElqIl0mPSa9IZPJOmRHchhZQF5PriSfIF8lD5I/UJQoJhRPShxFQtlOOUq5QHlAeUOlUg2obtRYqpi6nVpPvUR9Sn0vR5Mzl/OX48mtk6uRa5Xrl3slT5TXl3eXXy6fJ18hf0r+pvy4AlHBQMFTgaOwVqFG4bTCPYVJRZqilWKIYppiiWKD4jXFUSW8koGStxJPqUDpsNIlpSEaQtOledK4tE20Otpl2jAdRzek+9OT6cX0H+i99AllJWVb5SjlHOUa5bPKUgbCMGD4M1IZpYyTjLuMj/M05rnP48/bNq9pXv+8KZX5Km4qfJUilWaVAZWPqkxVb9UU1Z2qbapP1DBqJmphatlq+9Uuq43Pp893ns+dXzT/5PyH6rC6iXq4+mr1w+o96pMamhq+GhkaVRqXNMY1GZpumsma5ZrnNMe0aFoLtQRa5VrntV4wlZnuzFRmJbOLOaGtru2nLdE+pN2rPa1jqLNYZ6NOs84TXZIuWzdBt1y3U3dCT0svWC9fr1HvoT5Rn62fpL9Hv1t/ysDQINpgi0GbwaihiqG/YZ5ho+FjI6qRq9Eqo1qjO8Y4Y7ZxivE+41smsImdSZJJjclNU9jU3lRgus+0zwxr5mgmNKs1u8eisNxZWaxG1qA5wzzIfKN5m/krCz2LWIudFt0WXyztLFMt6ywfWSlZBVhttOqw+sPaxJprXWN9x4Zq42Ozzqbd5rWtqS3fdr/tfTuaXbDdFrtOu8/2DvYi+yb7MQc9h3iHvQ732HR2KLuEfdUR6+jhuM7xjOMHJ3snsdNJp9+dWc4pzg3OowsMF/AX1C0YctFx4bgccpEuZC6MX3hwodRV25XjWuv6zE3Xjed2xG3E3dg92f24+ysPSw+RR4vHlKeT5xrPC16Il69XkVevt5L3Yu9q76c+Oj6JPo0+E752vqt9L/hh/QL9dvrd89fw5/rX+08EOASsCegKpARGBFYHPgsyCRIFdQTDwQHBu4IfL9JfJFzUFgJC/EN2hTwJNQxdFfpzGC4sNKwm7Hm4VXh+eHcELWJFREPEu0iPyNLIR4uNFksWd0bJR8VF1UdNRXtFl0VLl1gsWbPkRoxajCCmPRYfGxV7JHZyqffS3UuH4+ziCuPuLjNclrPs2nK15anLz66QX8FZcSoeGx8d3xD/iRPCqeVMrvRfuXflBNeTu4f7kufGK+eN8V34ZfyRBJeEsoTRRJfEXYljSa5JFUnjAk9BteB1sl/ygeSplJCUoykzqdGpzWmEtPi000IlYYqwK10zPSe9L8M0ozBDuspp1e5VE6JA0ZFMKHNZZruYjv5M9UiMJJslg1kLs2qy3mdHZZ/KUcwR5vTkmuRuyx3J88n7fjVmNXd1Z752/ob8wTXuaw6thdauXNu5Tnddwbrh9b7rj20gbUjZ8MtGy41lG99uit7UUaBRsL5gaLPv5sZCuUJR4b0tzlsObMVsFWzt3WazrWrblyJe0fViy+KK4k8l3JLr31l9V/ndzPaE7b2l9qX7d+B2CHfc3em681iZYlle2dCu4F2t5czyovK3u1fsvlZhW3FgD2mPZI+0MqiyvUqvakfVp+qk6oEaj5rmvep7t+2d2sfb17/fbX/TAY0DxQc+HhQcvH/I91BrrUFtxWHc4azDz+ui6rq/Z39ff0TtSPGRz0eFR6XHwo911TvU1zeoN5Q2wo2SxrHjccdv/eD1Q3sTq+lQM6O5+AQ4ITnx4sf4H++eDDzZeYp9qukn/Z/2ttBailqh1tzWibakNml7THvf6YDTnR3OHS0/m/989Iz2mZqzymdLz5HOFZybOZ93fvJCxoXxi4kXhzpXdD66tOTSna6wrt7LgZevXvG5cqnbvfv8VZerZ645XTt9nX297Yb9jdYeu56WX+x+aem172296XCz/ZbjrY6+BX3n+l37L972un3ljv+dGwOLBvruLr57/17cPel93v3RB6kPXj/Mejj9aP1j7OOiJwpPKp6qP6391fjXZqm99Oyg12DPs4hnj4a4Qy//lfmvT8MFz6nPK0a0RupHrUfPjPmM3Xqx9MXwy4yX0+OFvyn+tveV0auffnf7vWdiycTwa9HrmT9K3qi+OfrW9m3nZOjk03dp76anit6rvj/2gf2h+2P0x5Hp7E/4T5WfjT93fAn88ngmbWbm3/eE8/syOll+AAAACXBIWXMAAAsTAAALEwEAmpwYAAACPmlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNS40LjAiPgogICA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPgogICAgICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgICAgICAgICB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iCiAgICAgICAgICAgIHhtbG5zOnRpZmY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vdGlmZi8xLjAvIj4KICAgICAgICAgPHhtcDpDcmVhdG9yVG9vbD5BZG9iZSBQaG90b3Nob3AgQ0MgMjAxNCBNYWNpbnRvc2g8L3htcDpDcmVhdG9yVG9vbD4KICAgICAgICAgPHRpZmY6WVJlc29sdXRpb24+NzI8L3RpZmY6WVJlc29sdXRpb24+CiAgICAgICAgIDx0aWZmOk9yaWVudGF0aW9uPjE8L3RpZmY6T3JpZW50YXRpb24+CiAgICAgICAgIDx0aWZmOlhSZXNvbHV0aW9uPjcyPC90aWZmOlhSZXNvbHV0aW9uPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KwVaRBAAAIPxJREFUeAHtm3mUnFd55p/aq7qqel+l1traV0uy5R0j72Ys7Jw4GJLBYQmMJ2dOhslkAj7ODGFOQpIBEk4GkwkzBDCDGRhsMHjBjm2wvIAsyZZl7epWq1tbd6v3rn2d33urS5YllUxm5q8cPumrr75bd3nvc9/93vaUufTr6wIEvBeU/LrAIfBrYGowgr9GOcUmYXZ7zqlyznd+cr96SrO/22/n/O5KrUb1Ov+3annlWe3lYiN6zu3GqruuKoVVTeDxVNbYFIPngqHO7+BsJ5XBL/JZE5iyClQvctso3GUGttvrdSUlZlLmd58vMwtfiHo+lctVqqjgob1TYTWHoY31I+WZjY1oANm79eLjDvASsEK7uYyECkllACioVC6qkC/L7w/LOwuOq1glw9pQR24BrWe76blcoclzIYqueQVm9/X8j2on55TTiRuPnyr9eZQvFJVO5xwgZYgvFq2dXfak4CxBrvCiH9ann+oGQpDvAXvnNmDst2qPfK1cswVlfvFCiN9vC2JAMWKprFyuqNI76LBe7K5eF/RY/eHs8xJLaR3NLs/ZZaJotk/jmDwrlct7lYeQYj6vaDQsnzWxOo4O+2KrxTDvQouP/ux27WjrqtvTbvq024nUO/q2QVgsr0c+j0/ZTEHT00kHUDgSUiQcks9fqeM6chx1tgPa1r7eHRgnGta5UedxBBsouWxJ2WwBcLyskE+TEzNqaCiruSUyy0026LugUaXLqlWrzn63EQ0kA6TIbZgZsWDAxdusboNP+Aco2byGR0aVSWcViUSp46GuTyFaef1GO0XG0u6yTqzAdVYpOu+zNjBQVGFPGjse9cCmkER/ReMUQDGWzWbse1mpdFmT4+O812nu/CbqQozjOCPgUiRUfiz5EAwTB8C3f2UmYWJitFsPNqUyLFMuFnln0ug6Exufz+9oOdZ/HJHOqA5QCvmicr6i/D60Fu2DAOe64qOiUyi0Hy5x1QaGhtZJVTkZIKZHS0iGAZLL5d2zkPcDFDLOXUCc+o6MqFjIAU4HLB5kEgXr6ZJ02Bj5Qt4p0oCf/mwwmpSMPYwGJsAIEFDi0wD0Q4cZAY9SibwOHuzTzHRCjU2N0FSiCaA6cPKuvgGIGpI/4ANI2tGhswmXAMaW9aLXWVCgx9HEs8AC5Bk4ny8oCwimW+zO5XjnLrrfPdq185iOHDzFzFhZp0prDuPGLsOKwXxJYbjMk80xHh35sFJwCGqU6ZcQCOMTeNDLWsJVPvu9UGKsXh3tHeY9rnSyDD22cIg4d/VZpB6MBrdZD9XbDV3zozbHGCCzl3Way1XExwbMZLPIcg7FC0iZLL+VlEHxmc4xEfL7wvrlL/YrlUrrso098jAJE40q91X7rfogJhaZ4Rmd2rtX867coEA8BJ/lWWLHJ666KV4zx0UmZ6uenClo9xv9Gjw2pubGFmVTcB2zKQFqiRUqmdwDZpGncUwwWFEN8gRQyBWuqdJxsWfNpTRutg6LBQAxXcJKZgEkm7NnnieixHsun4GDKLfvlKeTvKNzDJxXXtqv3a/3Odb1IhZVIBwhNlHziUxcYJDB7W/of2/9fU2dGOXnoLyMbfxmdFTl0HwkB0qioG0/3603dh5RJBhnAUpKp1i9IvrGFsiJuS2U0ZhD96TRgRneK7QXcDHe7aoJDLAY3k7hFYsAkUk5EcpkTIRgY7uR8wRck0yzukzOw2qW8h4Is9eQSjhdTz2zW798rVcpJpWmz1IJnQPYNu1Tk0mlmHl+eliDz35fER3Q4EsvSExiuhigb4+KrHCCVU9BSwGOmU6U9OJLb+mNPYMK1DVoMsliQEMWtk4b9xaiSmebACSiQgYa04g6hqFUgAuhOUsdZ51Moi5x1QTG4zWvFtmGPQOwoQ+2Nq2fSRmHQEQqNwuQlGVyCbzcKdg/4S0r7Q0wMb9SgZhy0Rb9r0cP6Oev9DpN4XQEojB0akIP/cPjSkyl5IFY/8igeiC0cPiAsaqOnjypp5/ZJo9JFFwTQKHOpAp6/Ge79eyOw/LFm5QpeAElr2QxC/AAZAIId/sBtIToQ6aSPLMFv6M1CTf7Eeug+TaOE2sjU1PHmNY284hkKhAI4KOwcjMZjSZnFAriJwBaamhIHQXYO1jWRD6pZDajHFaimPAqDtFJtHWB9iVPQi9/9XtqT7xXl1+xSdNnJvXw1x7RyJkzin/0NzV0bELjTzyr1qYmHfr561qdSSvqLek//tGfakHqQV1//UYU54xef+FN9b4yoJVzOhWaGFV92ado2KdSBv2VxSizih2hiGJ1IU2icMYzXoVbWrF4Bc1MjGjR/BbVN0ZZoCJuARM081TjqglMNSizduZPYHk1Z26LkijUoVMjisUblJ8a0Vv/8weI0ITq6jwKo28COH7+PESWAm6l4XLl8EAXTk3pzU89r8yt12ni1JCmH/3vWvHJT8sLgV4mVOhaotHTvYrctpCF8CsC698ajmjnv/4z5X7rcoBJ6tS3X9GK5UtVjIYYJyc/XF3wIsPMIk8beUPqDwQ1ygL55q3U0q13yxP3aDo3pSWLW9Xa0aAcbcyqBZ11qoEKxQBXy6IbyzivgQcOkvXBRxYd89aewzp+4rQaAnXK94/o6OM/kv9n39JS1aNZppWhKuS6WMdkteirV6kNvwbx800PqE4dSsxp14mONv3GN/5eDUsXaN+3v6nn7v+kPvj8M+p4zxYdfHmnnr/nfq1ATxWGX2OVpXhHjwqJlFLJ0yqHTTWXVcdgFluZZEwzer+yatv6KfXc8i+UmjtHY96cFve0aeWKLiHhSmIdLXwIG8PQ5nxLSZG7anJMBS7X9CzHmT8QDge0clUPDtWEjg5PqaFntRbdFdWoL6gzzz2pBQsWKlZMObPpNRONvsj7Asp5sAjxkiKtqxVBORM4KPnGC9r3vUd0ze9/UrEFi5Rds1KxrmbNHNyjXZ//vJaPZdXaGVB5wQY4I6+Z5JT88TDisAx9UtGBMWiKoKhnPBElEmU1vu8uzb/pTo03RNU3Oa7V67ugdy7ci1BjHPx4ygZiBUr35aIfNYGp4GldOF5xjc3+m19QR7C4+aoNmnlxl/YcGNCqeFSL33uzhmHrk88+obZwnlTBlAqEyxbn+MotDtCiL6ep/AzWhckVg2rrXqbdf/FZRZvq1byoR43LVsg7eFrbn3pavn98Qs0L1ik9OakkY+Lhs8pYtGIad4DgEPkp4i1jdJRHNIaCmPh7blHz5i3qhZN7B09q01ULtH7tIkSOeSDTHkAxDjZ7+/asLopLbVFyHPMOXCo4m74qYhp9Pp8S03n99Cdvae/+Y1rYEVcEhTi6+zX5+o4ohvbPUsdD3e4TJ9ScnZQnmFECVs/D00FWD1fLATfd1aGWyzZp4sSA5ra2anDbq2r3M3nEdoKly+DKR7A0DVieJOHGWKxNyfp2bKDfpSmyeMP+dRulNWs1GW4kZktqw+aF2rJlnaIRCEb5OsGDppz5TVwBJmjfaolSTR2DFLx9ub4MGLDmB3PWzB/x4mNMTZf1o2d36qV9J9Qej6k8PiZ/IikfK5RBgXrx0dtO79S8g69p7pkJwCgrEwzji/mxJBkF47gBmaTy40Nqapmv6TH8k7Zu1VlYjaJKBUhrYN4jRUCcyWto3nwNrFmn0ZZ5iEVMMXoMWjTd0KbxUlgTqTO6cctc3bplo2KAUmB8vy8EB+BF02URS2s5AlxLN79/OjCGw3kcY2BYkc9QZwCLsv3olqlEUd999GU9jyPX0dmhDE5XBuuUzuK5FkuK5k9pTv9BXd53WIsnBzEKGSUBJmjsh3dawOSGgyQISjlAw9zjXQdIZwRtcTDbBS8BajGsQy3rdWTBKp3unqORSKPC3phaQ3UKIeIBuGHmTFJbbu/QbVvXqx5dWCok0SsWDkQdFEa8BZiViZlQ1eaYS+gY68W1PfvhnDP3ZvBUACrhRTVEfPrAHdfgqGX07AuH1DanG26gORMsAsxkqVnJ1lVYEANpVC1Z3H50ArKmOsxsKR7HYw4oPYWZ7cSZDEWVHU2ZFXbmHN7UUCCu17tXan9Lj+rI/wThoCDclKN/H9r8yNFD+q171mjr1g2KhenT9JLP/C1HquOSynxsUudNrFLlHZ+XAOYd9c57oWMUnlC25UBGBXyWaEtI9/7LW9CQHv3wyUNqb2tTkVxrLpd1qYoR9ES2sUnF5sVwDfoIrmkOJ+QrpHRmYI8z7y31izXVfxQtJMW6lyuDP5QsRzUZaFJfU5sGwzGUP/EPuV4P+Zs0Ub4/GNHI+Ix++8NX6bbbNvKOwwcotUTkvInUfP2/BMb6w8cxfEC/iGgRDyqXymoa79iD7smzogW835Lz8MgJo4iHAu3a0RrUscAEHu2w1uRHlBrZraWf+xNEqE5Hv/tjbfzYH2py8KiO/vVXNb7sWh3yz9Wof55ONTcrQYgRxKNFDxNvCRHCMeQ5ncgqFGrAMbSEvHEzFf4fr0sCY77fpZA3x69YrnMR8smhSX3j6z/R40+d0qoVizU9RYyCv0IoQzCXdRPJEz/1eZq0vS6gLXBSU9+IrvnI72j1Rz+igYEB7fjBTzX/5uu0MHe5Bp58RX2EG69iyk/n6vGH4oginInyD9CnZfzMs7OYqKGuUV/60nOoo4zuet9GaGbRKmH5BfBcaj7nVr4kMGaWTT+aqbNFqAJl5RYmYJ+wLj6NDE3or/7qm3rmHwe06bIVOn1qGMVLEsvSEyi7fC4tH6F/ruTXaYQm5y9oPJ7Xfv1MN6/7bal1rlL7j6hv11ElYb34fKzT+iU68P1HNRBoIJDEXGdTShOzWVRbdgoMY18OkMqMq6Hep+aoV5/77HfxfRO6/Zar3WJVcjJIvKU3jF5EzC57t/tSV01zbY2q0YJ1WO38bDncVOAuISJjk9OaHJugTkTJJD4OuofMgvN3SliXEEFmgeSRD1b3hcw8pxWCncIzU5rT3abGJYuVheNOHj6pOSvnoUxLGtp7QGPkdQpNraorRkg7kCADlyL9+RHd+hjA4L8U0rgQ5FsDWKYSVg3vSQvmdxG71TmajfYqINV52BwMKPvnxRW42HUBMFWuOHDggEZGRlRfX48BCWpmZkb79x/Q3XffpdOnT7vkz+WXX07KIaUDhw5r71sHdfVVV6tn/oKLjePKhsYndLyvV1ds2mTL5soy5ZS2v7xd06NprVu0SuF4QB09c8/2MTI8rKnTY1p62SqCgEoMZlm6t/bsVn//oLZcv0XtLU1n65/75c0333QArFu37tzisw7qOwrPe6kpSjt27NC2bdu0es1q0g0JteKR7tq1C6Di2rNnj/r7julrX/87MnhJ/acH/libL78eWY8rMTGFTxIg1Zlkj2ecBHVcXnyQaSzIVDKhL/7JH+uLX/6yYq2d8sbrlYFrHnn4YS1ZtFK7Xn0RU9+pqwgi/eE6JROTGj5O+nLwmDblZjTBntGVG9ZpcmpYD/+Pv9XK5avVf7hDx9i38uAVR9Bdo+PTampuVxtW8ZFHHtHatWuhhd1SuLuhoUGjo6NaunSpTpLvyeFOXHbZZYQr4Qt0dk1gWlpadOTIEQKzhOL4GcuWLdNXvvIVHT16VEPkYY4fP6EDcNB6CL3j9rvRJ6wnIvz4D3+snsUL9ezTT2r+/LkanxhTktWI1bcpQMxSD8Ff/dp/09HjQ/rrL/2tphHD9as26f5/cz+Zuef0yivb9IUv/JnWrr9BY0Tww6NH1NjerN7+03r1xd168LP/TrfdfqMWwV0+f0QzpDO+/KX/qjmdLSj8CdRVt0uH1uMa7N69W729vers7NTmKzbD2Qfg+v1aumSpDh8+jHm/TatWwaUAU8kpvm3NLi5gTMREZ9GiRdq4cSNcQToTu2iW4+WXX9axY8c0zh5SAg5Ip1Nas2aVAuiO9991p2LRKGYzqNb2Nl1/w3Wa09WpWCCiTUTOx4/2adXy5dq4dr3ijfXol04cMHTUdEoHjxzSjl2va/n6TZq/fIn2HX9TkWY4gXTm6PFBbVi7RvfceydK3fK3Kc3D+52CO//8c1/UyjVrtOXmOxSKNut9d24lwL1Kv/jFL7R582Y99thjbvJr1611XLFi+Qot6Vmim266yXFSLStVExjL2hkwhva111yjKVbmgQce0BNPPKErr7xS9933Ye3dtwd5zWv7zl/q8JHD+vO//AvtPbRfj//kMbV2tfH+nxWuj6q9vUGPPfp9XX3t1WrumqO2uXNJHM1XLBbW2hXopPSI/uZvHlJT2yI84jhiFBNyR6gxza5iHUmxOKJA5h9xqWtAxLIz2r7rFbfze9+/+pCCIa/Gp4m1WgktyN5Z6uMKOMS4/DOf+YzjjqefftrNx+a1kHmZeCWTSadvzlMv7vUC5VutND09TfY9VbFMZmHQ/GamI3URrEwIhyqk8ckJNbY2uM2uFE5WGyBOT89gyr16cdtOEkJFbbnxWj300N/r2vdcS1rzChT6GX3rm9/WCrjsA3ffoWB2SpOnh5XKR9TcSgILj7gwlVYgR844YtYGj5n+LHSAfZTlvYk0x9jIMDsCbOwRfoyMjOL5o2MiAYVDYZf3TTDpILrOR1vb2bBsgBkRUw0d0DlKWtXmYCrjYldNYPKIT9X2m6m2ju3dHCczvdWyGZRqMpnVYfIy6QwTIRdiWyfHBsdxOcps9Pt1AkVX3xxGIXZoHE54480+rSCjtrg1onVNER3Zvo30Ztxl8K/8jVuUGp7Wrmd3KbNyGTQHlWOD3jzFIDTkoKFIMBujzIdCzbITEA6Ti8G3CXBeJJMY1eWb1qsRRVtNjzi6zbXAdNs8qpb3YoBUy2oqX5u4dWAdVb9X/QE/TpZ9z5ujhy8RqfPq1PCIPnTvD9SxsAMfokHd7RGNzRS1f2BaN27I64VDY1JvinHr9Z7bW/XFB7+n+z+4SKkNa5R57UUtufm9eu4bD2nj9as10ntUj/6H+/R3+k3qN3AbmVgOc3UtKmzjkUZRkuOtZxxz7nyBpPp6h/Sdr93F+BFHn9FfpZkWbh4FdKUBZZc9bW4Xu2pyjFWuImvP8y/r2rbXc8h9GQ/Uj6P00st7EJuXVBdrVYKdwSyxkpmqscQ4FgSTxS5lKuNnH6qsvuGT+qNr4lqNM7hpU7NaEK2ffP2r+ugffEKvP/FTHXsjpKci9fr5oRzsHiGSRhRYhDKixH8XJwVIeYRJOwRIe6YyY/rox2/Q7TeuJh3xtnN6Pt3nv1dBOr/84nBRqwpG9Xl+Q4PKiw4JoBMI69ypphuuW6d7792oEzhlwTDxkY9NOlLjsUjE0knES5a1kxpJjZqI+CJBxeY0Ok84jSccIjc88ss3tHfHHi3aej1KNajjw3Y4iIA0lyEuI/iyjSbL9JOnsQ1BqmhodFIf+72bdMvNq+GCtz3d82n+p7zXBOZX6gQO8JXDTBZxIlE02H9G+/cNqKsVywKLBoMehVjpsCeoOLXYyVCUAM/lYEXsEiqrZ9VSndm3T+XpSYWHzuiVP/hDrdl8hTrXL9AJlHt3dyPiHCadQPqSlEIAzgzBMkE/UGNhLBPXhv4yv2piIiE/yvb/x1VTx7A09F8VIXviAiFSlf0mD08kg0x3uYDiI3Y5dHhID3/7GfWfLGA22X8iKcUmLRoBhZ2DWFbYYpwSTl6ZSXa3N2pwOKuOFas1L3yXzhzZKz9+UYwNuVwK/6iQdvFOFgsYwdJ40HWWNTTA/YhUgI2uABxmPNiGV/6z5w+Sgx7XRz50hzrbyNuQwDKMLNQ16qHYfVbmxdd3uWrD67CwztANlvzgxh6520Cx8362BWq5mH1HJvQP33mJveagFi8lzmHy+Hisqm3xEoBiVDysdhF/o2T5SkCKYzqPz1DeHlds/hy99cCnFexZrCu/8F90fGxMowf61dYY0Bl/UpEAIkuzEJ364MAAZjgU8qNf4BxLwYDA0kXLte/NtL7/3Zc1OpwjrWn5XsROJn4ofUTezcUlkWxel75qA3NBO+MWk3cL35FuCA0EPDo6OKUfP7VDYfRFR2crXjL5Wva67bcAe8TgwYRsmxc8EC0Pv2HUMPllLegIyk+stPNHjwrMnPkNhaPq7uhUbnAYrqARiPiJnEN8D5D7NSVskbQdAvLTnw+XgCGItANa2tOjE6D9NLmcJFvFAcTNTmp4LAvuOOaCSdUsqA1MlfPOaWpiZOdfbAfBz6QHB0b1HP6GHyXc1Bh3K2QHAsNoRHOm7A6zshEmEQEhCy6DttLkZNGn6m4K6RRRfDY9puXfekhn6qLkijm2RhYw3DZHES852xKiCqcESWGaXgnRRzhkTz83fdKfLUKZlKeJzZyuLp06MaPnn93htnc8Hk45kCCrmPx355TqdGsD42qAAFxSuWxw820qtv/44Ihe2vY63OBVYz3bJvg1QVYyYkSjHMNGON6q7UObB+qAYmIx9EUYgL3IolmbsdOkNpbOV3BeJ2nLokZ2v0WcllJTD8kqzsGsIeVpORM7oRA2xct4IYAIw2JOsTNOPBpx45lSD6Co21vmauhkWq++eoDTGTAd27yVc71M1+Zzdk5VGC581gZmtrFzYdwH68HTHKLTp8a1a8chOCWo+mgdx0axOpEwANXD7lgiZMW2NMJmPUyG/CgcfI56OCJg2xnQ1oITluR715rlOnngsDIcFPCy4fbUv/2k1tx5sxqXd+nE6JQW4DkHvXi17EHXhQAEYHwAYErOR3ozjGjWsf0Si0FDHLeAsb2IVUdnl4ZPTZEi6eX0FXoG+2URtO2L/SpXTWDgD5BFs9uTT0tv2h6S7fL1951ClCKIT4vjCnPVTfbBYpY7OEZqbE+ZcY1ZoSC5ErM2bps1nwKkskZw56Pz2Ea97f3ynZ5Udu8hXfGpf69V121Wil33aJwA0suWSmGGsVPoEzuqSp+AEUK32B2Ee1CzcAr4A5hxUl2dHQnxEry2kdZI6wCK3E5aWQxXsaw2p0tfNYExZHGonVmueIdeIuyEBo6dRGzYD4o0sDIcxzDugKowt4FjrB50nFIBxRRlQ31IM3i/7R1lPfjg+/Xx371Ce3qPU5/0Jjpp3vJ16n3pVdWd7HcTKiOOple8oaIO7urT/Z+4Qb/38S2kT8fhHME5NpZX0bpwZTzAsXGNm2BehQA9xDaO6bMoYjYN3YPsidsRXDv+6gTg0ri4IKRGFdsYqXCLRdZ2xm70zJizRtFYo7yckMySCvDCVV7ceuOqyqFksxGwq3mnlHlY1WEi9fY2v373w+/FnDdpzoJGff4zk/rOD5+XL5FR/669yrBX3XjPfRo6PKDJ/gEFO5eREBvWp//yDl1z7UrbwiKUuJU2OzWnPaYGImwPi2eK3iLwik4zU47uMjeBRfICQoBnjEVMJNNk7YbV2dUK14Uc59SYuCv2/SnXRSuYjjGbz5XBXZ8mKuZwAUREKMd0+ngCXNkloPlmdflvz+rOhQWgdjAwFs/qdz54nZYtaePQAZtsrOzixd1a0NSgRo4rnNn+qro4PXESBd7c3qHmtmZ5mucC4kLdescmgEefsADd89tIWzZp4OgpgPApHovCAcYpWDpu84zDHGCKcIfQOxFciIriD1BGNA63mSI3y+YORTKDWrGS2bGLXmU2zEweoRVnibwGQESQ+zwKr0zYbyfeghECSDbXLHwpWiIJoAKciPShJIPI8+RkSi31Xm39wHXqxNPNmCXCY7XT3/UQfsvNG5U6OanBlmb6jegUWbwgh4nCJNTjc5t129w2NuoYHyVtTpyPxdpybTdbwgU98+SrJMFwDcJxx6Ah4xyACaLZvZwI96Pj/GYN4Rgz6RU3gUMGeD12/NYyApcSqZrAGJKV2xw5Y70AomPaHQBCQIDfYJxh5aaI7K9OSva7aX0cqiwpyFjUpxtvvQzWb6Ct6StTaXZ219ojqtzh5pjW33GrBvoOssNY0nWf+Jji8+bRRx6RoF+n9AksTDrxmE28N122ELc7px3bexETomu4ARxcoooNcxcy2GFKPxxrf5nig5tMt9gdwLfym98D7Ze6agMzq5ZNoiwY9JhHZhExg1UvL9zjzudZqADhBqSXYDJJliwEO199FXHQoga3/+TntwotpruMAfjEbzGsosu6VZ8a46QUWzWIkWnQonVol1W2vmlsTWwx7PDShk3LOMAU5RB0X8WBjBpXY52Ma4xDLMZClMwLN31TFakQiht7wVWhxr5d7LpkPuZsA0cMBKEA7ehHmgM9WXKmdlo8n+XQMQeObbskx6GelMuj5th2WaSuLtvvQfSYkYUTdrlPJkgJh4pmO8Y3ynN0bWZ0WHGOkQTwjUydm+8DHO40lXFMmZOiHkwyKo9S/gH24UMn3N8SNPF3BFF0jpnkSiwVdH+WEwSoEJbPj14zRezMOwtavWrpmJrAmH6x2yJnm4bpGltEi5FyRM7ulLjbhrUztJy+BphUmrAfi9CzZJ4am+ucuBh/VJjvbWIMqJKJFv9AzHVs5/WMJUoEfvwJAgd87BQ4Jtv+HsGa8nOJQNYY18SgjLhWez5xYghzfBJdEgYMjhIFMOOEHeZchuC+KKc8TSwdHTYkHdqwdtUCpqYoGSVuc9zNp5LvtXmYZresvP0FiAFmq2Y6o8TGfZjU2fwFnQ4UU5gVHWUAW81zLuTB2nHazGUBjUqXWaFN2ZQF75auqPyJzmw76lbE2Z5V2mxv3aN5/KWLxW4jI2NM2hQr/hWK1w44hfCJAk4VnDP+O4g5p/ycr7WBcaxvvghIOHmHUFjeUOfhgDHgfHZqEEJLWCfzhBsJJq2607MmRtS3A4pGi3GwdWvfrfzcu0gD168BMFun+qSqa2QcZiBXOjf9xIv778E/aQecIG4FHjJc5XwbTlWFWCywq1zW4a941RYlDLKtpwPGpuJC98qpASPGRKuAXOVyCRL4xDHEQabwKleVEmN/Dk7N9mKrUAXHSDTAbAS64rgpXDJLt7WuNQdXxVWwBQEc6sJ/fHBgiKJkgqP8nMmxnYNwHRbJLaJ1yO1Gsh5cB1bgOM59Oe+jNsecXU8jm95n301nVLs1qxQM42VaUsoiQ5cYN/AqYmZNjAw7EGgAWDvrqdreTd5erIyKpmDdqzWya/a36tNxy7nlVZrMVLFw5uyZPjGjYKJloLx9Wad2V+dT7fztGud+qwmMSXnlsg5Mj1Q6cp+VIiZJLcJ855fw3fwUSyW48WdbG212PP0sOZVu3K+m120Uq+MAsw+j/Zw6Z79T9g4ucrJqra0Bv1ln9qQoiIPpxMeKZvtzStZ55zbIu181Rendm/7zrvGrwffPG4OLzu7/AK/IGqAyV+zmAAAAAElFTkSuQmCC";


				var arrSignature = new Array();
				$(".div-do-sign-label-header").each(function() {
					var printedName = $(this).find("#txt-do-sign-printed-name").val();
					var title = $(this).find("#txt-do-sign-title").val();
					var signature = $(this).find("#img-do-sign").attr("src");
					arrSignature.push({"printedName" : printedName, "title" : title, "signature" : signature});
				});
				JSONDataPDF.signatures = arrSignature;

				if(_.contains(_.uniq(_.pluck(this.parent.JSONParts,"type")),'C')) {
					JSONDataPDF.Loaner = "No";
				}
				else{
					JSONDataPDF.Loaner = "Yes";
				}

				var MainJSONDataPDF = that.preparePartsData(JSONDataPDF);
				 if(that.template_PDF == undefined )
                    that.template_PDF = fnGetTemplate(URL_DO_Template, "GmDOPDFLayout1000");
                if(that.template_PDFWithOutPrice == undefined )
                    that.template_PDFWithOutPrice = fnGetTemplate(URL_DO_Template, "GmDOPDFLayoutWithOutPrice1000");
                    console.log("MainJSONDataPDF"+MainJSONDataPDF);
				that.$("#div-do-sample-pdf-container").html(that.template_PDF(MainJSONDataPDF)).show();
				var outerHeight = that.$('#div-parts').outerHeight();
				var divtop = that.$('#div-parts').offset().top;
				var pageNos = 1, splitID;
				if(outerHeight>300) {
					that.$('#div-parts').find(".ul-parts-list").each(function(index) {
						if((parseInt(divtop)+parseInt(380))<($(this).offset().top)) {
							if(pageNos==1)
								splitID = index-1;
							pageNos = 2;
						}
					});
				}
				if(outerHeight>300) {
					MainJSONDataPDF = that.preparePartsData(JSONDataPDF, splitID);
					MainJSONDataPDF[0].twoPages = "two";
					that.parent.MainJSONDataPDF = MainJSONDataPDF;
				} 
                
               
                
				that.$("#div-do-sample-pdf-container").hide();
				that.parent.MainJSONDataPDF = MainJSONDataPDF;
				boolUpdate = false;
				createPDFPath(function() {
         					createPDF(that.template_PDF(MainJSONDataPDF),"WP/", JSONDataPDF.DOID +".pdf", // on iOS,
         		               function(status) {
         		                   console.log("status = "+status );
                                     if (parseNull(devicePlatform) == "") {
                                          devicePlatform = "ios";
                                     }
                                     if (parseNull(androidDevicePlatform) == "") {
                                           androidDevicePlatform = "android";
                                     }
                                     if (devicePlatform.toLowerCase().includes(androidDevicePlatform)){
                                         that.wppath = status;
                                     }else{
                                         that.wppath = "file://"+status.match(/\(([^)]+)\)/)[1]; //Check in IOS Method
                                     }
         			                console.log("that.wppath = "+that.wppath );
                                 /* Set the pdf path to GLobal variable in DO Online mode as a temporary fix for PMT-13726
                                 */
                                         GM.Global.WPPdfPath = that.wppath;
                                             console.log("GM.Global.WPPdfPath>"+GM.Global.WPPdfPath);
         			               createPDF(that.template_PDFWithOutPrice(MainJSONDataPDF),"WOP/", JSONDataPDF.DOID +".pdf", // on iOS,
         				               function(status) {
                                               if (parseNull(devicePlatform) == "") {
                                                   devicePlatform = "ios";
                                               }
                                               if (parseNull(androidDevicePlatform) == "") {
                                                   androidDevicePlatform = "android";
                                               }
                                               if (devicePlatform.toLowerCase().includes(androidDevicePlatform)){
                                                    that.woppath = status;
                                               }else{
                                                    that.woppath = "file://"+status.match(/\(([^)]+)\)/)[1];
                                               }
						               fnShowStatus(lblNm("msg_pdf_generated_successfully"));
						               if(callback.currentTarget==undefined) 
							               	callback();
						               else if(that.parent.coveringRepMode) {
						            	   if(that.pdfCreatedAttempt) {
						            		   that.loaderview.close();
						            		   boolUpdate = true;
         								               //var DOCViewer = window.open(that.woppath, '_blank', 'location=no');
         								               openPdfFromLocalFileSystem(that.woppath);
						            	   }
							               
						               }
							           else {
							        	   if(that.pdfCreatedAttempt) {
							        		   that.loaderview.close();
							        		   boolUpdate = true;
							        	   }
							               if(that.parent.hideprice) {
							               	showNativePrompt(lblNm("msg_userid_open_pdf"), lblNm("msg_show_pdf"), [lblNm("cancel"),lblNm("msg_show")], '', function(data) {
												if(data.buttonIndex==2) {
													if(data.input1.toLowerCase()==localStorage.getItem('userName').toLowerCase()) {
														that.parent.hideprice = 0;
														$("#btn-do-hide-price").text(lblNm("hide_price"));
														if(!that.parent.coveringRepMode) {
															$("#li-header-price-ea").show();
															$("#li-header-ext-price").show();
															$("#div-do-parts-footer-title").show();
															$(".li-price-ea-textbox").show();
															$(".li-ext-price-textbox").show();	
                                                            $(".lbl-price-ea").show();
                                                            $(".lbl-ext-price").show();
														}
														if(that.pdfCreatedAttempt)
         															//var DOCViewer = window.open(that.wppath, '_blank', 'location=no');
         															openPdfFromLocalFileSystem(that.wppath);
         													}
         													else {
         														showNativeAlert(lblNm("msg_valid_username"),lblNm("msg_invalid_name"), undefined,function () {
         															//var DOCViewer = window.open(that.woppath, '_blank', 'location=no');
         															openPdfFromLocalFileSystem(that.woppath);
         														});
         													}
         												}
         												else {
         													if(that.pdfCreatedAttempt)
         														//var DOCViewer = window.open(that.woppath, '_blank', 'location=no');
         														openPdfFromLocalFileSystem(that.woppath);
         												}
         											})
         							               }
         							               else
         							            	   if(that.pdfCreatedAttempt)
         							            		  //var DOCViewer = window.open(that.wppath, '_blank', 'location=no');
                                                            openPdfFromLocalFileSystem(that.wppath);
			            	   	   }
						           //For LOGO Inconsistency  
					               if(!that.pdfCreatedAttempt) {
			            	   		   that.pdfCreatedAttempt = 1;
			            	   		  setTimeout( function() {that.$("#btn-sign-submit").trigger("click"); },320);
			            	   	   }
				               }, function(status) {
				               		that.loaderview.close();
				               		boolUpdate = true;
					               //console.log(status);
				           });

		               }, function(status) {
		               	that.loaderview.close();
		               	boolUpdate = true;
			               //console.log(status);
		           });
				});
			}
            }
            catch(err){
                that.loaderview.close();
                showError(lblNm("msg_error_generating"));
                }
			
		 },


		 preparePartsData: function (JSONDataPDF, splitID) {
		 	var that = this
		 	var partsperpage = that.parent.JSONParts.length;
				
			var id,name,qty,type,unitprice, price, replcheck, displayPrice, pfamily, indexValue, lotno, lotimg, pmaterial;
			var MainJSONDataPDF = new Array();

			var AttrJSON = new Array();
			var AttrNextJSON = new Array();
			this.SubJSONParts = [];
			var currJSON = $.parseJSON(JSON.stringify(JSONDataPDF));
			var subtotal = 0;
			var currParts = $.parseJSON(JSON.stringify(that.parent.JSONParts));
            var currSymbol = localStorage.getItem("cmpcurrsmb");
			currJSON.AddressData = that.parent.repPrimaryAddress;
            var index = 1;
			for( var k=0; k < currParts.length ; k++) {
					id = currParts[k].ID;
					name = currParts[k].Name.replace(id + " ", "");
					qty = currParts[k].Qty;
					type = currParts[k].type;
					lotno = currParts[k].lotno;
					lotimg = currParts[k].lotimg;
					unitprice = currParts[k].unitprice;
					unitprice =currSymbol+""+ parseFloat(unitprice).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					price = currParts[k].price;
                    pfamily = currParts[k].pfamily;
                    pmaterial = currParts[k].pmaterial;
					subtotal = parseFloat(subtotal) + parseFloat(price);
					price = currSymbol+""+ parseFloat(price).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					replcheck = currParts[k].repl;
                    if(pfamily != "26240096" || price != currSymbol+"0.00") {
					   indexValue = index++;
					   if((splitID==undefined)||(k<splitID)) {
						  AttrJSON.push({"ID" :id, "Name" : name, "Qty" : qty, "lotno" : lotno, "lotimg" : lotimg, "type" : type, "repl" : replcheck, "unitprice" : unitprice, "price" : price, "pfamily" : pfamily,"pmaterial" : pmaterial,  "indexValue" : indexValue}); 
                       }
					   else {
						  AttrNextJSON.push({"ID" :id, "Name" : name, "Qty" : qty, "lotno" : lotno, "lotimg" : lotimg, "type" : type, "repl" : replcheck, "unitprice" : unitprice, "price" : price, "pfamily" : pfamily,"pmaterial" : pmaterial, "indexValue" : indexValue}); 
                       }
                    }
			}
			subtotal = currSymbol+""+ parseFloat(subtotal).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			currJSON.subtotal = subtotal;
			currJSON.JSONSubParts = $.parseJSON(JSON.stringify(AttrJSON));
			currJSON.JSONNextSubParts = $.parseJSON(JSON.stringify(AttrNextJSON));
		
			MainJSONDataPDF.push(currJSON);

			return MainJSONDataPDF
		 },

		  viewSummary :function(){
			 //console.log(this.parent.JSONData);
			 //console.log(this.parent.JSONData.arrGmOrderItemAttrVO[i]);
			 var currSymbol = localStorage.getItem("cmpcurrsmb");
			 this.$("#div-do-view-summary-part-list").empty();
			 this.$("#div-do-view-summary-tag-list").empty();
			 this.$("#div-do-view-summary-main").show();
			 this.$("#div-do-view-summary-acid").html(this.parent.JSONData.acctid);
			 this.$("#div-do-view-summary-acname").html($("#div-do-billing-acname").text());
			 this.$("#div-do-view-summary-repnm").html(this.parent.repName);
			 this.$("#div-do-view-summary-doid").html(this.parent.JSONData.orderid);
			 this.$("#div-do-view-summary-custpo").html($("#div-do-billing-info-customer-input").val());
			 this.$("#div-do-view-summary-total").html(currSymbol+""+formatCurrency(this.parent.JSONData.total));
			 this.$("#div-do-view-summary-pdoid").html($("#div-do-billing-info-parent-input").val());
			 if(this.parent.dobillinginfoview.caseDrpDown!=undefined && this.parent.dobillinginfoview.caseDrpDown.getCurrentValue()[0] != "0" && this.parent.dobillinginfoview.caseDrpDown.getCurrentValue()[0] != undefined)
				 this.$("#div-do-view-summary-caseid").html(this.parent.dobillinginfoview.caseDrpDown.getCurrentValue()[1]);
			 else
				 this.$("#div-do-view-summary-caseid").html(""); 
             //getting surgeon name and adding in PDF
              var	surgeonName = "";
              var lastchar = "";
              var npidata=this.parent.JSONData.arrDoNPIDetailVO;
              $.each(npidata, function(i, item) {
                        surgeonName = surgeonName + npidata[i].name +",";
                        surgeonName = surgeonName.replace(/[0-9]/g, ''); //Removing Numbers from the surgeon name
                        surgeonName = surgeonName.replace(/-/g, ''); //Removing hyphens from the surgeon name
});
              lastchar    = surgeonName[surgeonName.length -1];
                if(lastchar == ","){
                    surgeonName = surgeonName.replace(/.$/,'')
                }
              surgeonName = surgeonName.replace(/[,]+/g,','); //Replace commas if one or more matches available
			 this.$("#div-do-view-summary-sname").html(surgeonName);
              
              var inputDate = $("#div-do-billing-info-surgery-date").find("input").val();
              if(inputDate !=""){ 
                $("#div-do-view-summary-sdate").html(formattedDate(inputDate,localStorage.getItem("cmpdfmt")));
			   
          }
			 //this.$("#div-do-view-summary-sdate").html($("#div-do-billing-info-surgery-date").find("input").val());
			 
	
			if(this.parent.JSONParts != null)
				{
			//	this.$("#div-do-view-summary-part-list").append($("#div-do-parts-list"));
				var data = $.parseJSON(JSON.stringify(this.parent.JSONParts))
				
				for(var i=0;i<data.length;i++) {
					data[i].price = currSymbol+""+formatCurrency(parseInt(data[i].Qty)* parseFloat(data[i].unitprice));
					data[i].unitprice = currSymbol+""+formatCurrency(data[i].unitprice);
					//this.parent.JSONParts[i].displayPrice = currSymbol+""+ parseFloat(price).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					this.$("#div-do-view-summary-part-list").append(this.template_partdetails(data[i]));
					
				}
//				var items = new SaleItems(this.parent.JSONParts);
//				//console.log(data);
//				this.showItemListView(this.$("#div-do-view-summary-part-list"), items, "GmDOViewSummaryPartDetails", URL_DO_Template);	
				
				}
			if(this.parent.JSONData.arrGmTagUsageVO != null)
				{
				//this.$("#div-do-view-summary-tag-list").append($("#div-do-tagid-parts-list"));
				var items = new SaleItems(this.parent.JSONData.arrGmTagUsageVO);
				
				//console.log(this.parent.JSONData.arrGmTagUsageVO);
				this.showItemListView(this.$("#div-do-view-summary-tag-list"), items, "GmDOViewTagDetails", URL_DO_Template);	
				}
			if((this.parent.coveringRepMode == true) || (this.parent.hideprice)) { 
				this.$("#div-do-view-summary-info-total").hide();
				this.$("#div-do-view-summary-part-price-header").hide();
				this.$("#div-do-view-summary-part-eprice-header").hide();
				this.$(".div-viewsummary-price-ea-value").css("visibility","hidden");
				this.$(".div-viewsummary-ext-price-value").css("visibility","hidden");
			}
			else {
				this.$("#div-do-view-summary-part-price-header").show();
				this.$("#div-do-view-summary-part-eprice-header").show();
			}

		 },
		 showItemListView: function(controlID, collection, itemtemplate, template_URL) {
				
				this.listView = new SaleItemListView(
						{ 
							el: controlID, 
							collection: collection, 
							itemtemplate: itemtemplate,
							template_URL: template_URL,
							columnheader: 0
						}
				);
				return this.listView;
			},
		closeViewSummary :function(){
			this.$("#div-do-view-summary-main").hide();	
		},

		 emailWithPrice: function() {

		 	if(this.wppath!=undefined) {
		 		var that = this;
		 		fnGetLocalFileSystemPath(function(URL) {
		 			GM.Global.PDFPATH = URL;
		 			GM.Global.PDFORDERID = that.parent.JSONData.orderid;
		 			GM.Global.DOPDFFOLDER = "DO/PDF/WP/";
		 			if ($(window).width() < 480) 
                		sendDOPDFEmailViaMobile(function(callback) {
			 			});
            		else
                	 	sendDOPDFEmailViaIpad(function(callback) {
			 			});
			 	});
		 	}
		 	
		 	else {
		 		showNativeAlert(lblNm("msg_generate_pdf_email"),lblNm("generate_pdf"));
		 	}

		 	
		 },

		 emailWithOutPrice: function() {

		 	if(this.woppath!=undefined) {
		 		var that = this;
		 		fnGetLocalFileSystemPath(function(URL) {
		 			GM.Global.PDFPATH = URL;
		 			GM.Global.PDFORDERID = that.parent.JSONData.orderid;
		 			GM.Global.DOPDFFOLDER = "DO/PDF/WOP/";
		 			if ($(window).width() < 480) 
                		sendDOPDFEmailViaMobile(function(callback) {
			 			});
            		else
                	 	sendDOPDFEmailViaIpad(function(callback) {
			 			});
		 		});
		 		
		 	}
		 	
		 	else {
		 		showNativeAlert(lblNm("msg_generate_pdf_email"),lblNm("generate_pdf"));
		 	}
		},
	
		faxPdfWithPrice:function(event) {
			var that = this;
			if(this.wppath!=undefined) {
//				$('#overlay-back').fadeIn(100,function(){
					if(that.keypadview)
						that.keypadview.close();
			 		that.keypadview = new NumberKeypadView({
						el : that.$("#div-sign-show-num-keypad"),
						callback: function(val) {that.preparePDF("WP/", val);}
					});
			 		
			 		$("#div-number-keypad-title").html(lblNm("fax_with_price")+"</div>");
					$("#div-number-keypad-close").show();
					$("#div-number-decimal").hide();
					$("#div-number-keypad-number-symbol").css("width","69px").css("height","17px").css("margin-left","0");
					$("#div-number-keypad-btn-done").html(lblNm("msg_send_fax"));
//				});
			}

		},

		faxPdfWithOutPrice:function(event) {
			var that = this;
			if(this.wppath!=undefined) {
//				$('#overlay-back').fadeIn(100,function(){
					if(that.keypadview)
						that.keypadview.close();
			 		that.keypadview = new NumberKeypadView({
						el : that.$("#div-sign-show-num-keypad"),
						callback: function(val) {that.preparePDF("WOP/", val);}
					});
			 		
			 		$("#div-number-keypad-title").html(lblNm("fax_without_price")+"</div>");
					$("#div-number-keypad-close").show();
					$("#div-number-decimal").hide();
					$("#div-number-keypad-number-symbol").css("width","69px").css("height","17px").css("margin-left","0");
					$("#div-number-keypad-btn-done").html(lblNm("msg_send_fax"));
//				});
			}
		},
		
		preparePDF: function(folder, value) {
			var DOId = this.parent.JSONData.orderid;

			var that = this;
			var pdfPath = "";

			fnGetLocalFileSystemPath(function(URL) {
				pdfPath = URL + 'DO/PDF/' + folder + DOId + '.pdf';

				window.resolveLocalFileSystemURL(pdfPath,function(entry){
			            entry.file(function (file) {
			                var reader = new FileReader();
			                reader.onloadend = function(evt) {
			                    console.log(">>>>>>>>>Read as binary<<<<<<<<");
			                    fnSendFax(evt.target.result,value);
			                };
			                reader.readAsArrayBuffer(file);
			            }, function  (argument) {
			                console.log('error file');
				            })
				            

				        },function() {
				            console.log('error getting file');
				        });
			    },null);
		},

		updateJSON: function() {
			this.parent.saveDO();
		},

		close: function() {
			this.undelegate();
			this.unbind();
			this.remove();
		}
		

	});	

	return DOSignatureView;
});