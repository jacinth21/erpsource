/**********************************************************************************
 * File:        GmDOTagIDView.js
 * Description: 
 * Version:     1.0
 * Author:     	tvenkateswarlu
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification', 'searchview'
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification, SearchView) {

	'use strict';
	var DOTagIDView = Backbone.View.extend({

		events : {
			"click #div-do-tagid-parts-list .fa-plus-circle" : "splitPart",
			"click #div-do-tagid-parts-list .fa-minus-circle" : "removePart",
			"click .div-do-tagid-keypad-values" : "showNumberKeypad",
			"click .li-do-tagid .fa-barcode" : "getBarcode",
			"keyup .li-do-tagid input" : "InsertJSON",
			"change .li-do-tagid input" : "InsertJSON"
			
			
		},
		/* Load the templates */ 
		template: fnGetTemplate(URL_DO_Template, "GmDOTagID"),
		template_numberkeypad: fnGetTemplate(URL_DO_Template, "GmDOPartsNumberKeypad"),
		
		

		initialize: function(options) {
			this.parent = options.parent;
			this.render();
		},

		// Render the initial contents when the Catalog View is instantiated
		render: function () {
//			showMsgView("011");

			console.log(this.parent.JSONParts);

			for(var i=0;i<this.parent.JSONParts.length;i++) {
				console.log(this.parent.JSONParts[i].TagID);
				if(this.parent.JSONParts[i].TagID == undefined) {
					var arrTagInfo = new Array();
					arrTagInfo.push({"ID" : this.parent.JSONParts[i].ID, "Name" : this.parent.JSONParts[i].Name, "indexValue" : this.parent.JSONParts[i].indexValue, "Qty" : this.parent.JSONParts[i].Qty, "TagValue" : ""});
					this.parent.JSONParts[i].TagID = arrTagInfo;
				}
				else {
					var totalQty = parseInt(this.parent.JSONParts[i].Qty);
					var tagQty = _.reduce(_.pluck(this.parent.JSONParts[i].TagID,'Qty'), function(memo, num){ return parseInt(parseInt(memo) + parseInt(num)); }, 0);
					console.log(tagQty);
					console.log(totalQty);
					if(totalQty<tagQty) {
						this.parent.JSONParts[i].TagID.splice(1,(this.parent.JSONParts[i].TagID.length-1));
						console.log(this.parent.JSONParts[i]);
						tagQty = _.reduce(_.pluck(this.parent.JSONParts[i].TagID,'Qty'), function(memo, num){ return parseInt(parseInt(memo) + parseInt(num)); }, 0);
						console.log(totalQty<tagQty);
						if(totalQty<tagQty) {
							this.parent.JSONParts[i].TagID[0].Qty = totalQty;
						}
					}
						
					else if(totalQty>tagQty)
						this.parent.JSONParts[i].TagID[0].Qty = parseInt(this.parent.JSONParts[i].TagID[0].Qty) + (totalQty-tagQty);

				}
			}

			this.viewJSON = new Array();

			var allTags = _.pluck(this.parent.JSONParts,'TagID');

			for(var i=0;i<allTags.length;i++) {
				this.viewJSON = this.viewJSON.concat(allTags[i]);
			}

			console.log(this.viewJSON);

			this.$el.html(this.template(this.viewJSON));	
			this.checkQty();
			return this;
		},
		
		checkQty: function () {
			this.$(".div-do-tagid-keypad-values").each(function() {
				var qty = $(this).text();
				console.log($(this).parent().parent().attr("data-parent"));

				var qtylength = qty.length;
				if(qtylength == 0) {
					$(this).html(1);
					$(this).parent().parent().find(".fa-plus-circle").hide();
				}
				else if(qty < 2) { 
					$(this).parent().parent().find(".fa-plus-circle").hide();
				}
				if(($(this).parent().parent().attr("data-parent")=="")|| ($(this).parent().parent().attr("data-parent") == undefined)) {
					console.log($(this).parent().parent().get(0))
					if(qty > 1) { 
						$(this).parent().parent().find(".fa-plus-circle").show();
					}
					else {
						$(this).parent().parent().find(".fa-plus-circle").hide();
					}
					$(this).parent().parent().find(".fa-minus-circle").hide();
				}
				else {
					$(this).parent().parent().find(".fa-minus-circle").show();
				}
			});
		},
		
		InsertJSON: function (event) {
			
			if(event!=undefined) {
				if( event.type == "change") {
					var tagvalue = $(event.currentTarget).val();
					if(tagvalue.length < 7) {
						
                        fnShowStatus(lblNm("msg_invalid_tagid"));
					}
//					else
//						fnShowStatus("");
				}
			}
			var TagIDJSONData = new Array(), TagJSONParts = new Array();
			var that = this;
			var id, name, qty, tagid, index, parentIndex;
			this.$("#div-do-tagid-parts-list").find("ul").each(function(){
				var id = $(this).attr('id');

				qty = $(this).find(".div-do-tagid-keypad-values").text();
				name = $(this).find(".li-do-tagid-partname").text();
				tagid = $(this).find(".li-do-tagid input").val();
				index = $(this).attr("data-index");
				parentIndex = $(this).attr("data-parent");
				TagIDJSONData.push({"pnum" : id, "qty" : qty, "tagid" : tagid,  "Name": name, "indexValue" : index});

				TagJSONParts.push({"ID" : id, "Qty" : qty, "TagValue" : tagid,  "Name": name, "indexValue" : index, "parentIndex" : parentIndex});

			});

			for(var i=0;i<that.parent.JSONParts.length;i++) {
				that.parent.JSONParts[i].TagID = [];
			}

			for(var i=0;i<TagJSONParts.length;i++) {
				for(var j=0;j<that.parent.JSONParts.length;j++) {
					if(parseInt(that.parent.JSONParts[j].indexValue) == parseInt(TagJSONParts[i].indexValue))
						that.parent.JSONParts[j].TagID.push(TagJSONParts[i]);
				}
			}

			console.log(that.parent.JSONParts);

			that.parent.JSONData.arrGmTagUsageVO = TagIDJSONData;

			this.parent.saveDO();
		},
		
		splitPart: function (e) {
			if($(e.currentTarget).parent().parent().find(".div-do-tagid-keypad-values").text()>1) {
				var ul = document.createElement('ul');
				ul.className = "ul-do-tagid-parts-list ul-do-tagid-child-part";
				var partno = $(e.currentTarget).parent().parent().attr("id");
				$(ul).attr("data-parent",$(e.currentTarget).parent().parent().index());
				$(ul).attr("data-index",$(e.currentTarget).parent().parent().attr("data-index"));
				$(ul).attr("id",partno);
				ul.innerHTML = $(e.currentTarget).parent().parent().html();
				var qtyvalue = $(e.currentTarget).parent().parent().find(".div-do-tagid-keypad-values").text();
				this.$(e.currentTarget).parent().parent().find(".div-do-tagid-keypad-values").html(parseInt(qtyvalue)-1);
				$(ul).find(".li-do-tagid input").val("");
				$(ul).find(".div-do-tagid-keypad-values").html(1);

				var index = $(e.currentTarget).parent().parent().index();
				this.$("#div-do-tagid-parts-list").append(ul);
				this.checkQty();
			}
		},
		
		removePart: function(e){	
			var parentIndex = parseInt($(e.currentTarget).parent().parent().attr("data-parent"));
			var thisQtyValue = $(e.currentTarget).parent().parent().find(".li-do-tagid-qty").text();
			console.log(parentIndex);

			var parentQtyValue = this.$("#div-do-tagid-parts-list").find('ul:nth-child(' + (parentIndex+1) + ')').find(".li-do-tagid-qty").text();
			console.log(parseInt(parentQtyValue), parseInt(thisQtyValue));
			this.$("#div-do-tagid-parts-list").find('ul:nth-child(' + (parentIndex+1) + ')').find(".div-do-tagid-keypad-values").html(parseInt(parentQtyValue)+parseInt(thisQtyValue));

			$(e.currentTarget).parent().parent().remove();
			this.checkQty();
		},
		
		getBarcode: function(event) {
			var that = this;
			var parent = $(event.currentTarget).parent();
			cordova.plugins.barcodeScanner.scan(
				  function (result) {
					  $(parent).find("input").val(result.text);
					  that.InsertJSON();
				  }, 
				  function (error) {
				      console.log("Scanning failed: " + error);
				      }
				   );
		},
		
		showNumberKeypad:function(event){
			var that = this;
			var indexvalue, qtyvalue, partid, childqty, parentqty, parentid, dataparent;
			this.parent.showNumberKeypad(event, function(qty_val, target) {				
				partid = $(this.target).parent().parent().attr("data-parent");
				dataparent = $(this.target).parent().parent().attr("data-parent");
				if(partid == '') {
					childqty=0;
					indexvalue = $(this.target).parent().parent().attr("data-index");
					console.log(_.findWhere(that.parent.JSONParts,{"indexValue": indexvalue}));
					qtyvalue = _.findWhere(that.parent.JSONParts,{"indexValue": indexvalue}).Qty;
					
					$("#div-do-tagid-parts-list").find("ul").each(function(){
						if(($(this).attr("data-parent") != undefined) || ($(this).attr("data-parent") != "")) {
							if( $(this).attr("data-parent") == indexvalue ) {
								childqty = parseInt(childqty) + parseInt($(this).find(".div-do-tagid-keypad-values").text());
							}
						}						
						
					});


					if( parseInt(qtyvalue) >= (parseInt(childqty) +  parseInt(qty_val)) ) {
						if(qty_val == "0" || qty_val == "00" || qty_val == "000"){
							console.log(qty_val)
							$("#div-qty-box").text();
							showMsgView("053");
						} else{
							$(target).text(qty_val);
						}
					}
					
					else {
						fnShowStatus(lblNm("msg_valid_quantity"));
					}
				}
				else {
					indexvalue = $(this.target).parent().parent().index();
					qtyvalue = that.parent.JSONParts[dataparent].Qty;
					parentid = that.parent.JSONParts[dataparent].ID;
					childqty=0, parentqty=0;
					$("#div-do-tagid-parts-list").find("ul").each(function(){
						if( $(this).attr("id") == parentid && $(this).index() == dataparent ) {
							parentqty = parseInt($(this).find(".div-do-tagid-keypad-values").text());
						}
						else if( $(this).attr("data-parent") == dataparent && $(this).index() != indexvalue ) {
							childqty = parseInt(childqty) + parseInt($(this).find(".div-do-tagid-keypad-values").text());
						}											
					});
					
					if( parseInt(qtyvalue) >= parseInt(childqty) +  parseInt(qty_val) + parseInt(parentqty) ) {
						$(target).text(qty_val);
					}
					
					else {
						fnShowStatus(lblNm("msg_valid_quantity"));
					}
					
				}
				
			});
		},

		close: function() {
			this.unbind();
			this.undelegateEvents();
		}
		
		

	});	

	return DOTagIDView;
});