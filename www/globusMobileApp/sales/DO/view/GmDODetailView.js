/**********************************************************************************
 * File:        GmDODetailView.js
 * Description: GmDODetailView
 * Version:     1.0
 * Author:     	tvenkateswarlu
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification','dobillinginfoview', 'dopartsusedview', 'dosignatureview', 'dotagidview', 'doshiptoinfo', 'docommentsview', 'loaderview','dosurgerydetailview','daodo','donpidetailview','dorecordtagsview'
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification, DOBillingInfoView, DOPartsUsedView, DOSignatureView, DOTagIDView, DOShipToInfoView, DOCommentsView, LoaderView,GmDOSurgeryDetailView,DAODO,
                  GmDONPIDetailView,GmDORecordTagsView) {
         
	'use strict';
	var tabid;
	var DOMainView = Backbone.View.extend({

		events : {
			"click .div-do-detail-label" : "toggleContent",
			"click .div-do-detail-button" : "changeLabel",
			"click .div-keys" : "getKeyValues",
			"click .div-do-keypad-values" : "showNumberKeypad",
			"click #div-num-symbol" : "deleteKeyValues",
			"click #div-keypad-close" : "closeKeypad",
			"click #div-btn-done" : "displayQuantity",
			"click #btn-do-bookdo" : "bookDO",
			"click #btn-do-cancel" : "cancelDO",
			"click #btn-do-delete" : "deleteDO"
			
		},

		/* Load the templates */ 
		template: fnGetTemplate(URL_DO_Template, "GmDODetail"),
		template_numberkeypad: fnGetTemplate(URL_DO_Template, "GmDOPartsNumberKeypad"),

		

		initialize: function(options) {
            var that = this;
			$("body").removeClass();
			$("#btn-home").show();
			$("#btn-back").show();
			this.bookMode = true;
			this.coveringRepMode = false;
			this.partsChangeMode = false;
			this.signatureMode = false;
			this.signaturesubmit = false;
			this.signaturesrc = "";
			this.printedName = "";
			this.signatureTitle = "";
			this.additionalInfoSrc = "";
            this.otherInfo = {};
			this.otherInfo.HCAAccount = false;
			this.hideprice = 0;
			this.options =options;
			this.usagelotfl = "";
            this.DOFavoriteDocumentID = '';
            this.DOFavoriteMultipleDocumentID = [];            
            
            /* Get the mapped Sales Rep details for the associate rep based on his user id
               for the Favorites
               Author: Karthik Somanathan    
             */
			var partyid = window.localStorage.getItem("partyID");
			fnGetUserSalesRep(partyid,function(result){
				fnGetAssocSalesRep(result[0].REPID, function  (data) {
					if(data.length>0){
                        //Get Rep Info for the Associate Sales Rep
						that.DOFavoriteMultipleDocumentID = data;
                    }else{
                        //Get logged in Rep Info
                        that.DOFavoriteMultipleDocumentID = result;
                    }
				});
			});

			if(options.DO==undefined) {
				//Initializing DO JSON
				this.JSONParts = [];
				var date = new Date();
				var dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
		
				this.JSONData = {};
				this.JSONData.orderid = "";
				this.JSONData.ordertype = "";
				this.JSONData.orderdate = mm+"/"+dd+"/"+yyyy;
				this.JSONData.repid = "";
				this.JSONData.acctid = "";
				this.JSONData.custpo = "";
				this.JSONData.parentorderid = "";
				this.JSONData.caseinfoid = "";
				this.JSONData.total = "0";
                this.JSONData.dealid = "";
				this.JSONData.userid = localStorage.getItem("userID");
				this.JSONData.skipvalidatefl = "";
				this.JSONData.ordercomments = "";
				this.JSONData.statusfl = "7";
				this.JSONData.stropt = "NEWDO";
				this.JSONData.arrGmOrderAttrVO = new Array();
				this.JSONData.arrGmOrderItemVO = new Array();
				this.JSONData.arrGmOrderItemAttrVO = new Array();
				this.JSONData.gmTxnShipInfoVO = new Array();
				this.JSONData.arrGmTagUsageVO = new Array();
                this.JSONData.arrDoSurgeryDetailVO = new Array();
                this.JSONData.arrDoNPIDetailVO = new Array();
                this.JSONData.arrDORecordTagIDVO = new Array();
                this.JSONData.arrDORecordPartVO = new Array();
                this.JSONData.arrDORecordappendData = new Array();
                
				this.render(options);
			}

			else {

				this.partsChangeMode = true;
				var that = this;
				this.DO = options.DO;
				fnGetDOInfo(options.DO, function(data) {
					that.JSONData = $.parseJSON(data.JSON);
					that.JSONParts = $.parseJSON(data.JSONPARTS);
					that.signaturesrc = $.parseJSON(data.SIGNATURE);
					console.log("coveringrep mode>>>>" + data.COVERINGREPFL);
					that.coveringRepMode = (data.COVERINGREPFL=="true")?true:false;

                    if (parseInt(device.version) >= 12 )
                        that.additionalInfoSrc = $.parseJSON(data.ADDITIONAL_INFO);
                    else
                        that.additionalInfoSrc = data.ADDITIONAL_INFO;

					that.otherInfo = $.parseJSON(data.OTHER_INFO);
					var sign = _.pluck(that.signaturesrc,"signature").join()
					//console.log(sign)
					if(sign!="") {
						that.signatureMode = true;
					}

					fnGetRepInfo(that.JSONData.repid,function(repInfo){
						that.repName = repInfo.C703_SALES_REP_NAME;
						that.render(options);
					});
					
					fnSalesRepDfltAddress(that.JSONData.repid,function(addData) {
						if(addData.length>0)
							that.repPrimaryAddress = $.parseJSON(JSON.stringify(addData[0]));
					});
					
				});
			}

			
		},

		// Render the initial contents when the Catalog View is instantiated
		render: function (options) {
			var that = this;
//			showMsgView("011");
			this.$el.html(this.template());
            
            // To hide NPI tap for japanese users
            _.each(localStorage.getItem("moduleaccess").split(','), function (data) {
                if(data != "")
                    that.$("#"+data).addClass("hide");
            });
                    
                
            //Function To get the Surgery Fields in surgery tab after syncing the Rule Table.
            fnGetRuleForDOSurgery(function(data){
                 var arrSurgeryFields=[]
            _.each(data,function(sData,i){
               arrSurgeryFields.push(sData.RULEVALUE.split("^"));
                arrSurgeryFields[i].push(sData.RULEID);
            });
            var arrValue=[];
          _.each(arrSurgeryFields,function(data){
                  var type = data[0];
                  var name = data[1];
                  var id = data[2];
              
                  arrValue.push({"attrtyp":type ,"attrnm":name ,"ruleid":id,"attrval":""});
          });
                GM.Global.doSurgeryInfo = arrValue; //Set the array list to Global Variable
                if(GM.Global.doSurgeryInfo.length == 0){
                    $("#div-do-surgery-info").remove();
            }
            });
             
			// this.strViewType = options[0];
			var partyid = window.localStorage.getItem("partyID");
			fnGetUserDOID(partyid,function(result){
				fnGetAssocSalesRep(result.C703_SALES_REP_ID, function  (data) {
					if(data.length>0)
						that.userMappedRep = data[0].repid;
					that.userDistributorid = result.C701_DISTRIBUTOR_ID;
				});
				//console.log(that.userDistributorid);
			});
			
			fnGetLotUseFL(function(usageFL){
	               that.usagelotfl=usageFL;
	         });
           
			if(this.DO)
				this.$("#div-do-billing-info").trigger("click");
			else
				this.toggleContent();
			return this;
		},

		// Toggles the view
		display: function(key) {
			$("#ui-datepicker-div").hide();
			if(key.currentTarget==undefined) {
				this.key = key;
			}
			else {
				this.key = "billing-info";
				
			}
			this.$(".div-do-detail-label").each(function() {
				$(this).removeClass("selected-tab");
			});
			

//			this.$("#div-do-" + this.key).show();
			this.$("#div-do-" + this.key).addClass("selected-tab");
			this.$(".ul-do-detail-info").hide();
			this.$("#ul-do-detail-" + this.key).show();
			this.$(".div-do-detail-button").show();
			if(this.key == "billing-info") {
				this.$("#btn-backward").hide();
			}
			if(this.key == "comments") {
				this.$("#btn-forward").hide();
			}

		},
		
		changeLabel: function (event) {
		    this.$(".div-do-detail-label").each(function () {
		        if ($(this).hasClass("selected-tab")) {
		            if (event.currentTarget.id.indexOf("backward") >= 0) {
		                if ($(this).prev().hasClass("hide")) {
                            if ($(this).prev().prev().hasClass("hide")) {
                                $(this).prev().prev().prev().trigger("click");// Hiding the tab when clicking the arrow marks
                            }
                            else {
		                      $(this).prev().prev().trigger("click"); 
                            }
		                } else {
		                    $(this).prev().trigger("click");
		                }
		            } else {
		                if ($(this).next().hasClass("hide")) {
                            if ($(this).next().next().hasClass("hide")) {
                                $(this).next().next().next().trigger("click"); //Hiding the tab when clicking the arrow marks
                            }
                            else {
		                      $(this).next().next().trigger("click"); 
                            }
		                } else {
		                    $(this).next().trigger("click");
		                }
		                return false;
		            }
		        }
		    });
		},

		/*Quantity keypad Functions*/

		showNumberKeypad:function(event, callback){
			if(callback!=undefined)
				this.callback = callback;

			this.price_val = $(event.currentTarget).parent().parent().find("#div-price-ea-box").text();
				this.target = $(event.currentTarget);
				var that=this;
//			 	$('#overlay-back').fadeIn(100,function(){
            		that.$("#div-show-num-keypad").html(that.template_numberkeypad());
            		var attrval = $(event.currentTarget).attr("value");
            		if(attrval == "faxno_withprice"){
            			$("#div-keypad-title").html(lblNm("msg_fax_price")+"</div>");
						$("#div-keypad-close").show();
						$("#div-num-decimal").hide();
						$("#div-num-symbol").css("width","69px").css("height","17px").css("margin-left","0");
						$("#div-btn-done").html(lblNm("msg_send_fax"));
            		}
            		else if(attrval == "faxno_withoutprice"){
            			$("#div-keypad-title").html(lblNm("msg_fax_price_out")+"</div>");
						$("#div-keypad-close").show();
						$("#div-num-decimal").hide();
						$("#div-num-symbol").css("width","69px").css("height","17px").css("margin-left","0");
						$("#div-btn-done").html(lblNm("msg_send_fax"));
            		}
            		else if(attrval == "quantity"){
						$("#div-keypad-title").html(lblNm("quantity")+"</div>");
						$("#div-keypad-close").show();
						$("#div-num-decimal").hide();
						$("#div-num-symbol").css("width","69px").css("height","17px").css("margin-left","0");
					}
//         		});
			
			
		},

		getKeyValues:function(event){
		
			var targetid = event.currentTarget.id;
			var targettext = $("#"+targetid).text();
			if ( targetid == "div-num-decimal" && $("#div-qty-entry-text").text() == "") {
				targettext = "0.";
			}
			
			if( $("#div-qty-entry-text").text() != "" ) {
				if( targetid == "div-num-decimal" && $("#div-qty-entry-text").text().indexOf(".") >= 0 ) {
					targettext = "";
				}
			}
			$("#div-qty-entry-text").append(targettext);
		},

		deleteKeyValues:function(event){
			
			var number_value = $("#div-qty-entry-text").text();
			var str = number_value.substring(0, number_value.length-1);
			$("#div-qty-entry-text").text(str);
			
		},

		displayQuantity:function(event){
			var qty_val;
			//console.log(event.currentTarget.id);
			if(($("#div-qty-entry-text").text()) == ""){
				showMsgView("052");
			}
			if(($("#div-qty-entry-text").text()) != "" && ($("#div-qty-entry-text").text()) == "0"){
				showMsgView("053");
			}
			if($("#div-qty-entry-text").text() != "" && $("#div-qty-entry-text").text() != "0"){
//				showMsgView("011");
			}
			if($("#div-qty-entry-text").text() != "")
				qty_val = $("#div-qty-entry-text").text();
			if(this.callback!=undefined)
				this.callback(qty_val, this.target);
			$("#div-num-keypad").hide();
			$('#overlay-back').fadeOut(100);
		},

		closeKeypad:function(event){
			$('#overlay-back').fadeOut(100);
			$("#div-num-keypad").hide();
		},

		cancelDO:function() {
			
			 //console.log(tabid);
			switch(tabid) {
				
				case "div-do-billing-info":
				var that = this;
				
				showNativeConfirm(lblNm("msg_cancelling_tab"),lblNm("msg_confirm"),[lblNm("msg_yes"),lblNm("msg_no")], function(index) {
					if(index==1) {
						fnDeleteDO(that.JSONData.orderid);
                        //This is related to Read Tag from Image Changes and this code is to Update the Order id to the Tag Image related tables
                        fnDelDOFromTagImgDtls(that.JSONData.orderid);
                        fnDelTagsForProcessImage(that.JSONData.orderid);
						that.dobillinginfoview = null;
						that.dopartsused = null;
						that.doshiptoinfo = null;
						that.dosignature = null;
						that.dotagidview = null;
						that.DO = undefined;
						that.options = new Array();
						that.initialize(that.options);
					}
						
				});
				break;

				case "div-do-parts-used":
					this.dopartsused = null;
					this.JSONParts = [];
					this.partsChangeMode = false;
					this.JSONData.arrGmOrderItemVO = new Array();
					this.JSONData.arrGmOrderItemAttrVO = new Array();
					this.JSONData.arrGmTagUsageVO = new Array();
					this.JSONData.gmTxnShipInfoVO = new Array();
                    this.JSONData.arrDORecordPartVO = new Array();
                    this.JSONData.arrDORecordappendData = new Array();
				break;
				case "div-do-ship-info":
					for(var i=0;i<this.JSONParts.length;i++) {
						delete this.JSONParts[i]['shipTo'];
						delete this.JSONParts[i]['AddressData'];
					}
					this.JSONData.gmTxnShipInfoVO = new Array();
				break;
				case "div-do-signature":
					this.dosignature.render();
				break;
				case "div-do-tag-id":
					for(var i=0;i<this.JSONParts.length;i++) {
						delete this.JSONParts[i]['TagID'];
					}
					this.JSONData.arrGmTagUsageVO = new Array();
				break;
                case "div-do-record-tags":
					this.JSONData.arrDORecordTagIDVO = new Array();
                    this.JSONData.arrDORecordPartVO = new Array();
                    this.JSONData.arrDORecordappendData = new Array();
				break;

			}
			
			 $(".selected-tab").trigger("click");

		},


		toggleContent: function (event) {
            var that = this;
			if(!event) 
				tabid = "div-do-billing-info";
			
			else
				tabid = (event.currentTarget.id);
            var surgInfoValidation = false;
            var surgeryInfoAttrType = "";
            var addInfoValidation = false;
            //to validate the EGPS dropdown mandatory only for GMNA - Spine
			var EGPSUsageValidation = false;
            var lotNumValidation = false;
                if(this.JSONData.accEPGSFl == 'Y'){
                   for(var i=0;i<this.JSONData.arrGmOrderAttrVO.length;i++){
                         if(this.JSONData.arrGmOrderAttrVO[i].attrtype == '26241315' && (this.JSONData.arrGmOrderAttrVO[i].attrvalue == '' || this.JSONData.arrGmOrderAttrVO[i].attrvalue == '0' )){
                                        // fnShowStatus("Please select EGPS Information on Surgery Type before proceeding");
                                        EGPSUsageValidation = true;
                        }
                     }                              
                 }
            if(this.JSONData.accDivCompanyFl == 'Y'){
                   for(var i=0;i<this.JSONData.arrGmOrderAttrVO.length;i++){
                         if((this.JSONData.arrGmOrderAttrVO[i].attrtype == '103603' || this.JSONData.arrGmOrderAttrVO[i].attrtype == '103602') && (this.JSONData.arrGmOrderAttrVO[i].attrvalue == '' || this.JSONData.arrGmOrderAttrVO[i].attrvalue == '0' )){
                                surgeryInfoAttrType =  this.JSONData.arrGmOrderAttrVO[i].attrtype;       
                                surgInfoValidation = true;
                        }
                     } 
                 if($("#div-do-billing-info-additional").css("background-image") != undefined) {
                  var addInfoSrc = $("#div-do-billing-info-additional").css("background-image").replace('url(','').replace(')','');
                           if(addInfoSrc == '' || (addInfoSrc.indexOf("data:image") == -1)){
                                addInfoValidation = true;
                           }
                 }
               
                     $(".txt-lot-no-box").each(function() {
                         if($(this).css("display")!="none"){
                         var lotimgno = $(this).parent().parent().find('.li-img-lot-no img').attr('src');
                         console.log($(this).parent().parent().find('.li-img-lot-no img').attr('src'));
                        if(($(this).attr("validCloudLotfl") == "N" && (lotimgno == '' || lotimgno == undefined))|| this.value=="")
                            lotNumValidation = true;
                             
                         }
                    });
            }
  
			switch(tabid) {
				case "div-do-billing-info":
					if(!this.dobillinginfoview)
						this.dobillinginfoview = new DOBillingInfoView({'el' :this.$('#ul-do-detail-billing-info'), 'parent' : this});
					this.display("billing-info");
					break;	
				case "div-do-parts-used":
                    					//console.log(this.JSONParts)
                    var npiaccess=0;
                      _.each(localStorage.getItem("moduleaccess").split(','), function (data) {
                          if(data == "div-do-NPI-info")
                           npiaccess=++npiaccess;   
                      });
                   this.addNPIFromTextSearch();
					if(this.JSONData.orderid=="") {
                        fnShowStatus(lblNm("msg_select_account_parts"));
					}
                    else if(npiaccess==0 && this.JSONData.arrDoNPIDetailVO.length==0 && localStorage.getItem("cmpid") == "1000") {
                        fnShowStatus("Please provide NPI Details to proceed");
                    }
                    else if(EGPSUsageValidation){
                              fnShowStatus("Please select EGPS Information on Surgery Type before proceeding");                          
                    }
                    else if(surgInfoValidation){
                        if(this.JSONData.arrGmOrderAttrVO[i].attrtype == '103602'){
                                fnShowStatus("Please select Surgery Type before proceeding");  
                           }else{
                                fnShowStatus("Please select Surgery level before proceeding");  
                           }
                            
                    }else if(addInfoValidation){
                              fnShowStatus("Please take a picture before proceeding");  
                    }
					else {
                       var npiidcount=0;
                        for(var i=0;i<this.JSONData.arrDoNPIDetailVO.length;i++){
                           if(!isNaN(this.JSONData.arrDoNPIDetailVO[i].id)){
                               npiidcount=++npiidcount;
                           }
                        }
                        if(npiaccess==0 && npiidcount==0  && localStorage.getItem("cmpid") == "1000"){
                            fnShowStatus("Please provide NPI Details to proceed");
                           } 
                           else{
                               if(!this.dopartsused)
                                   this.dopartsused = new DOPartsUsedView({'el' :this.$('#ul-do-detail-parts-used'), 'parent' : this});
						      this.display("parts-used"); 
                           }
                    }
                    that.dopartsused.doRecPartsAdd();
					break;
				case "div-do-signature":
					  if(EGPSUsageValidation){
                              fnShowStatus("Please select EGPS Information on Surgery Type before proceeding");                          
                    }else if(lotNumValidation){
                              fnShowStatus("Please provide valid lot or take a picture in part level in case of new lot");                          
                    }else if(surgInfoValidation){
                        if(surgeryInfoAttrType == '103602'){
                                fnShowStatus("Please select Surgery Type before proceeding");  
                           }else{
                                fnShowStatus("Please select Surgery level before proceeding");  
                           }
                            
                    }else if(addInfoValidation){
                              fnShowStatus("Please take a picture before proceeding");  
                    }else{
                        	this.validateTissuePartsAddressType();

                            var strMessage = localStorage.getItem("TissueErrorMsg");
                            if (strMessage != "") {
                                 fnShowStatus(strMessage);
                                 break;	
                            }else{
                                if(!this.dosignature)
                                    this.dosignature = new DOSignatureView({'el' :this.$('#ul-do-detail-signature'), 'parent' : this});
                                this.display("signature");
                                break;	
                            }
                    }

				case "div-do-tag-id":
				
					if(this.JSONData.orderid=="") {
						fnShowStatus(lblNm("msg_select_accout_tag"));
					}
					else if(this.JSONParts.length==0) {
						fnShowStatus(lblNm("msg_choose_parts_tag"));
					}
					else {
						this.partsChangeMode = true;
						if(this.dotagidview)
							this.dotagidview.close();
						this.dotagidview = new DOTagIDView({'el' :this.$('#ul-do-detail-tag-id'), 'parent' : this});
						this.display("tag-id");
					}
					break;						
				case "div-do-ship-info":
					
					this.validateTissueQty();			//function added for PC-866
                    var strMessage = localStorage.getItem("TissueQtyMsg");
                    var nullPartTypes =  _.where(this.JSONParts, {"type": ''});
					if(this.JSONData.orderid=="") {
						fnShowStatus(lblNm("msg_select_account_shipping"));
					}
					else if(this.JSONParts.length==0) {
						fnShowStatus(lblNm("msg_choose_parts_shipping"));
					}
					else if (strMessage != "") {
                        fnShowStatus(strMessage);
                    }else if(EGPSUsageValidation){
                              fnShowStatus("Please select EGPS Information on Surgery Type before proceeding");                          
                    }else if(surgInfoValidation){
                        if(surgeryInfoAttrType == '103602'){
                                fnShowStatus("Please select Surgery Type before proceeding");  
                           }else{
                                fnShowStatus("Please select Surgery level before proceeding");  
                           }
                            
                    }else if(addInfoValidation){
                              fnShowStatus("Please take a picture before proceeding");  
                    }else if(lotNumValidation){
                              fnShowStatus("Please provide valid lot or take a picture in part level in case of new lot");       
                    }
                    else if(nullPartTypes.length != 0){
                        var nullPartList = _.pluck(nullPartTypes, 'ID');
                        fnShowStatus("Please select part type for below part(s) "+ nullPartList);
                        $(".ul-parts-used-list").each(function () {
                            if (!$(this).find(".li-type-dropdown span").hasClass("input-checked")) {
                                $(this).addClass("noTypeSelected");
                            }
                            else{
                                $(this).removeClass("noTypeSelected");
                            }
                        });
                    }
					else {
                        $(".ul-parts-used-list").removeClass("noTypeSelected");
						this.partsChangeMode = true;
						 if(this.doshiptoinfo)
						 	this.doshiptoinfo.close();
						this.doshiptoinfo = new DOShipToInfoView({'el' :this.$('#ul-do-detail-ship-info'), 'parent' : this});
						this.display("ship-info");
					}
					break;

				case "div-do-comments":
			
					if(this.JSONData.orderid=="") {
						fnShowStatus(lblNm("msg_select_account_comments"));
					}else if(EGPSUsageValidation){
                              fnShowStatus("Please select EGPS Information on Surgery Type before proceeding");                          
                    }else if(surgInfoValidation){
                        if(this.JSONData.arrGmOrderAttrVO[i].attrtype == '103602'){
                                fnShowStatus("Please select Surgery Type before proceeding");  
                           }else{
                                fnShowStatus("Please select Surgery level before proceeding");  
                           }
                            
                    }else if(addInfoValidation){
                              fnShowStatus("Please take a picture before proceeding");  
                    }else if(lotNumValidation){
                              fnShowStatus("Please provide valid lot or take a picture in part level in case of new lot");       
                    }
					else {
						if(this.docommentsview)
						 	this.docommentsview.close();
						this.docommentsview = new DOCommentsView({'el' :this.$('#ul-do-detail-comments'), 'parent' : this});
						this.display("comments");
					}
					break;
                    
                case "div-do-surgery-info":
                    if(!this.dosurgerydetailview)
				    this.dosurgerydetailview = new GmDOSurgeryDetailView({'el' :this.$('#ul-do-detail-surgery-info'), 'parent' : this});
                    this.display("surgery-info"); 
					break;
                    
                case "div-do-NPI-info":
                    if(this.JSONData.orderid=="") {
						fnShowStatus(lblNm("msg_select_account_npi"));
					} else if(EGPSUsageValidation){
                              fnShowStatus("Please select EGPS Information on Surgery Type before proceeding");                          
                   }else if(surgInfoValidation){
                        if(surgeryInfoAttrType == '103602'){
                                fnShowStatus("Please select Surgery Type before proceeding");  
                           }else{
                                fnShowStatus("Please select Surgery level before proceeding");  
                           }
                            
                    }else if(addInfoValidation){
                              fnShowStatus("Please take a picture before proceeding");  
                    }
                    else {
                        if(!this.donpidetailview)
                        this.donpidetailview = new GmDONPIDetailView({'el' :this.$('#ul-do-detail-NPI-info'), 'parent' : this});
                        this.display("NPI-info");
                        if(localStorage.getItem("cmpid") != "1000"){
                           $("#li-header-NPI").html("NPI#");
                        }
                    }
					break;
                    
                /*Add new tab to record the tags used for the DO*/  
                case "div-do-record-tags":
                    this.DOFavoriteDocumentID = this.JSONData.repid;
                    if(this.JSONData.orderid=="") {
						fnShowStatus(lblNm("msg_select_record_tags_do"));
					}else if(EGPSUsageValidation){
                              fnShowStatus("Please select EGPS Information on Surgery Type before proceeding");                          
                    }else if(surgInfoValidation){
                        if(surgeryInfoAttrType == '103602'){
                                fnShowStatus("Please select Surgery Type before proceeding");  
                           }else{
                                fnShowStatus("Please select Surgery level before proceeding");  
                           }
                            
                    }else if(addInfoValidation){
                              fnShowStatus("Please take a picture before proceeding");  
                    }
                    else {
                        if(!this.dorecordtagsview) {
                            this.dorecordtagsview = new GmDORecordTagsView({'el' :this.$('#ul-do-detail-record-tags'), 'parent' : this}); 
                        }
                        this.display("record-tags");
                   }
                    this.dorecordtagsview.enableUsedTagByParts();
                    this.dorecordtagsview.usedFlCheck(that.JSONData.arrDORecordPartVO); 
                    $('#div-do-record-tag-list').empty();
                    this.$("#div-crm-overlay-content-container2 .modal-body").empty();
                    this.dorecordtagsview.displaydraftRecordTag(this.JSONData.arrDORecordTagIDVO, function(cback){
                        if(cback){
                            that.dorecordtagsview.usedFlCheck(that.JSONData.arrDORecordPartVO);
                        }  
                    }); 
					break;

			}

		},
        
        
     /*   checkTabConn: function (){
            if(this.JSONData.arrDORecordTagIDVO != undefined ) {
                for(var i=0;i<this.JSONData.arrDORecordTagIDVO.length;i++){
                    if(this.JSONData.arrDORecordTagIDVO[i].setid==''){
                        this.dorecordtagsview.loadSetDetailsOfflineSavedTag(this.JSONData.arrDORecordTagIDVO[i]); 
                        console.log('Online Tag Set Get');
                    }
                }
                $('#div-do-record-tag-list').empty();
                this.dorecordtagsview.displayTagDetailsFromDraft(this.JSONData.arrDORecordTagIDVO);
                    }
        },*/
		validate: function() {
			var that = this;

		  	var PartTypes = _.contains(_.uniq(_.pluck(this.JSONParts,"type")),'C');
		  	var repl = _.contains(_.pluck(this.JSONParts,"repl"),true);
		  	var lotNoValidation = false;
            var divisionlotNoValidation = false;
           	
           	var igValidate = true;
		    if (this.JSONData.ordertype == "2532") {
		        igValidate = false;
		    }

                if(this.usagelotfl!='Y'){
                    $(".txt-lot-no-box").each(function() {
                        if($(this).css("display")!="none" && this.value=="")
                        lotNoValidation = true;
                    });
                    
                }
                if(this.JSONData.accDivCompanyFl== "Y"){

                                        for(var i=0;i<this.JSONParts.length;i++){
                                            console.log(this.JSONParts[i].lotno.trim()== "");
                                            if((this.JSONParts[i].lotno.trim() == "" ||(this.JSONParts[i].validCloudLotfl=="N" && this.JSONParts[i].lotimg=="")) && (this.JSONParts[i].pfamily == "4050" && this.JSONParts[i].pclass == "4030")){
                                                divisionlotNoValidation = true;
                                                break;
                                            }
                                    }
                }

		  	
			if(this.JSONData.acctid=="") {
				fnShowStatus(lblNm("msg_account_information"));
				return false;
			}
			else if(this.JSONParts.length==0) {
				fnShowStatus(lblNm("msg_provide_info_parts"));
				return false;
			}
			else if(lotNoValidation) {
				fnShowStatus(lblNm("msg_provide_lot_no"));
				return false;
			}
                       else if(divisionlotNoValidation) {
				fnShowStatus("Please provide valid lot or take a picture in part level in case of new lot");
				return false;
			}
			else if(!this.signaturesubmit) {
				showNativeAlert(lblNm("msg_generate_pdf_do"),lblNm("generate_pdf"));
				return false;
			}
			else if((PartTypes || repl) && (!this.JSONData.gmTxnShipInfoVO.length)) {
				  if (igValidate) {
		            fnShowStatus(lblNm("msg_pls_pro_ship_info"));
		            return false;
		        }
		        else {
		        	return true;
		        }
				// fnShowStatus(lblNm("msg_pls_pro_ship_info"));
				// return false;
			}
			else {
				return true;
			}
		},
        
        //Preparing Parts data same as provided in the PDF (Copy from this.JSONParts)
        prepareSamePDFParts: function() {
          try {
            var arrOrderItem = new Array(), arrItemAttr = new Array();
            var arrPartsInfo = $.parseJSON(JSON.stringify(this.JSONParts));
            var len = arrPartsInfo.length;
            for(var i=0;i<len;i++) {
                arrOrderItem.push({"indexValue" : arrPartsInfo[i].indexValue,
                                                    "pnum" : arrPartsInfo[i].ID,
                                                    "qty" : arrPartsInfo[i].Qty,
                                                    "price" : arrPartsInfo[i].unitprice,
                                                    "type" : "",
                                                    "parttype" : (arrPartsInfo[i].type=="C")?"50300":"50301"});
                if(arrPartsInfo[i].lotno.trim().length>0 || this.usagelotfl=='Y')
                    arrItemAttr.push({"indexValue" : arrPartsInfo[i].indexValue,
                                                            "itemordattrid" : "",
                                                            "pnum" : arrPartsInfo[i].ID,
                                                            "qty" : arrPartsInfo[i].Qty,
                                                            "attrvalue" : arrPartsInfo[i].lotno,
                                                            "attrtype" : "101723"});
                if(arrPartsInfo[i].repl)
                    arrItemAttr.push({"indexValue" : arrPartsInfo[i].indexValue,
                                                            "itemordattrid" : "",
                                                            "pnum" : arrPartsInfo[i].ID,
                                                            "qty" : arrPartsInfo[i].Qty,
                                                            "attrvalue" : "Y",
                                                            "attrtype" : "101732"});
            }
            this.JSONData.arrGmOrderItemVO = $.parseJSON(JSON.stringify(arrOrderItem));
            this.JSONData.arrGmOrderItemAttrVO = $.parseJSON(JSON.stringify(arrItemAttr));
           } catch(err){
                showAppError(err, "GmDODetailView:prepareSamePDFParts(0)");
            }
        },
      //Checks if the PDF is present in DEvice and then if present allows us to BookDO 
		bookDO: function() {
            try {
			var that = this;
			var folder = "WP/";
            var URL = "";
			fnGetDOID(that.JSONData.orderid, function(data) {
                try {
				if(data.length>0) {
                     /* Get the PDF path URL from the global variable */
                         URL = GM.Global.WPPdfPath;
						console.log("PDF Path in bookdo " +URL);
						fnGetImage(URL, function(status) {                           
							if(status!='error')
								{
                                    showNativeConfirm(lblNm("msg_want_book"),lblNm("msg_confirm"),[lblNm("msg_yes"),lblNm("msg_no")], function(index) {
				if(index==1) {
					var addtionalInfo = $("#div-do-billing-info-additional").css("background-image").replace('url(','').replace(')','');
					if(that.validate()) {
						if((navigator.onLine) && (!intOfflineMode)) {
							if(that.bookMode) {
                                var npiaccess=0;
                      _.each(localStorage.getItem("moduleaccess").split(','), function (data) {
                          if(data == "div-do-NPI-info")
                           npiaccess=++npiaccess;   
                      });
                                if(npiaccess==0 && localStorage.getItem("cmpid") == "1000") {
                                    if(that.JSONData.arrDoNPIDetailVO.length){
                                         var npiidcount=0;
                                        for(var i=0;i<that.JSONData.arrDoNPIDetailVO.length;i++){
                                        if(!isNaN(that.JSONData.arrDoNPIDetailVO[i].id)){
                                        npiidcount=++npiidcount;
                                        }
                                        }
                                        if(npiidcount>0){
                                        that.processData();
                                        } 
                                        else{
                                            fnShowStatus("Please provide NPI Details to proceed"); 
                                        }
                                    }
                                    else{
                                         fnShowStatus("Please provide NPI Details to proceed");
                                    }
                                    
                                }
                                else{
                                    that.processData();
                                }
                            }
							
						}
						else {
							fnWriteFile("Globus/DO/Signature", that.JSONData.orderid + ".txt", JSON.stringify(that.MainJSONDataPDF), function() {
								fnUpdateDOStatus(that.JSONData.orderid,'Pending Sync');
                                				//fnUpdateDOStatusinTagDetails(that.JSONData.orderid,'Pending Sync');
								if(intOfflineMode) {
									showNativeAlert(that.JSONData.orderid + lblNm("msg_pending_booked_online"),lblNm("msg_offline_mode_restriction"));
                                }
								else{
                                     if(that.JSONData.arrDoNPIDetailVO.length) {
                                        showNativeAlert(that.JSONData.orderid +lblNm("msg_pending_booked_internet") ,lblNm("msg_pending_sync"));
                                         window.location.href = "#do/booked";
                                     }
                                     else {
                                    showNativeConfirm(lblNm("msg_surgeon_not_listed"),lblNm("msg_confirm"),[lblNm("msg_yes"),lblNm("msg_no")], function(index) {
                                        if(index==1) {
                                           showNativeAlert(that.JSONData.orderid +lblNm("msg_pending_booked_internet"),lblNm("msg_pending_sync"));
                                           window.location.href = "#do/booked";
                                        }
                                    });
                                    }
                                }
							});
							
						}
					}
				}
			});
                                }
							else
								fnShowStatus(lblNm("msg_not_available")+ that.JSONData.orderid) ;
						});
                
				}
				 } catch(err){
                showAppError(err, "GmDODetailView:BookDO(1)");
            }
			});
            } catch(err){
                showAppError(err, "GmDODetailView:BookDO(0)");
            }
			
				
		},
       //If upload PDF ->Success->BooDO else failure->try 3 times totally to BookDO and if failure for three times allow user to BookDO() 
        iteration: function(n){
             try {
            var that =this;
          if((navigator.onLine) && (!intOfflineMode) )
                uploadPDF(that.JSONData.orderid, function(uploadStatus) {
                    console.log("uploadStatus***");
                    console.log("n = "+n);
                    var time = new Date();
                    console.log("time = "+time);
                    console.log(uploadStatus);
                     var pdffailurecnt = eval(5);

                        if(uploadStatus){
                                that.loaderview.close();
                                fnInsertPdfFlagInfo(that.JSONData.orderid, that.JSONData.orderid +".pdf","Y", n);
                                that.JSONData.arrGmOrderAttrVO.push({"orderid" : that.JSONData.orderid, "attrtype" : "26230806", "attrvalue" : "Y"});
                                that.JSONData.arrGmOrderAttrVO.push({"orderid" : that.JSONData.orderid, "attrtype" : "26230805", "attrvalue" : n+1});
                                that.loaderview = new LoaderView({text :lblNm("msg_booking_do_c")});
                                that.OnlineSaveDO ();    
                                }
                        else if(n == pdffailurecnt-1 ){
                                that.loaderview = new LoaderView({text : lblNm("msg_booking_do_c")});
                                that.JSONData.arrGmOrderAttrVO.push({"orderid" : that.JSONData.orderid, "attrtype" : "26230806", "attrvalue" : "N"});
                                that.JSONData.arrGmOrderAttrVO.push({"orderid" : that.JSONData.orderid, "attrtype" : "26230805", "attrvalue" : n+1});
                                fnInsertPdfFlagInfo(that.JSONData.orderid, that.JSONData.orderid +".pdf","N", n+1);
                                that.OnlineSaveDO();
                                    
                                }
                            else
                                that.iteration(n+1)
                                                    }); 
             }catch(err){
                 showAppError(err, "GmDODetailView:iteration()");
             }
        },
        //Booking DO
        OnlineSaveDO: function(){
            try{
            var that =this;
            var addtionalInfo = $("#div-do-billing-info-additional").css("background-image").replace('url(','').replace(')','');
            var signatureData = new Array();
					$(".div-do-sign-label-header").each(function() {
						var printedName = $(this).find("#txt-do-sign-printed-name").val();
						var title = $(this).find("#txt-do-sign-title").val();
						var signature = $(this).find("#img-do-sign").attr("src");
						signatureData.push({"printedName" : printedName, "title" : title, "signature" : signature});
					});
          bookDO($.parseJSON(JSON.stringify(that.JSONData)), function(status, data) {
									if(status) {
                                        that.loaderview.close();
										fnUpdateDOStatus(that.JSONData.orderid,'Pending CS Confirmation');
										arrPendingDOList.push(that.JSONData.orderid); 
                                        console.log("TAG SAVING ONLINE=== "+that.JSONData.orderid);
                                        fnSaveTagsForPendingSyncDO(); //To Add the tag(s) processed from the image to the server for the DO which is not processed manually even when they are in online
												showNativeAlert(lblNm("msg_booked_successfully"), lblNm("msg_success"), lblNm("msg_ok"), function () {
												    window.location.href = "#do/report";
												});
									}
									else {
										that.loaderview.close();
										if(data.indexOf("<html>")==-1) {
											data = $.parseJSON(data);
											if(data.message == "DO already entered in the System") {
												var oldDOID = that.JSONData.orderid;
//												var orderdate =  formattedDate(that.JSONData.orderdate,localStorage.getItem("cmpdfmt"));
                                    var orderdate = that.JSONData.orderdate;
												var serverseq, localseq, seq;
												var DOFormat = oldDOID.split("-")[0]+ "-" + oldDOID.split("-")[1] + "-" ;
												fnCheckDOID(DOFormat, function(DOs) {
													var currSeq = (DOs.length)?(_.max(_.pluck(DOs,'DOID'), function(id){ return parseInt(id.split('-')[2]); })):"0";
													console.log(currSeq);
													localseq = parseInt(currSeq.replace(DOFormat,"")) + 1;
									var prefixordid = oldDOID.split("-")[0] + "-" + oldDOID.split("-")[1];
                                                     var token = "";
                                                     if(localStorage.getItem("token") != undefined ){
                                                        token =  localStorage.getItem("token");
                                                        } 
													var input = {
															"token": token,
															"orderdate": orderdate,		
															"prefixordid": prefixordid
													};
													fnGetWebServiceData("DeliveredOrder/getlatestdo", undefined, input, function(status,data) {
														if(status){
															if(data.orderid!="") {
																var orderid = data.orderid;
																serverseq = orderid.split("-")[2];
																serverseq = parseInt(serverseq) + 1;
															}
															
															else {
																serverseq = 1;
															}

															if(serverseq == localseq)
																seq = serverseq;
															else if(serverseq > localseq)
																seq = serverseq;
															else
																seq = localseq;
														}
														else {
															seq = localseq;
														}

														var newDOID = oldDOID.split("-")[0]+ "-" + oldDOID.split("-")[1] + "-" + seq;
														showNativeConfirm(lblNm("msg_do_id_s")+ oldDOID + lblNm("msg_s_prefer_update")+ newDOID+"?",lblNm("msg_do_id_conflict"),[lblNm("msg_yes"),lblNm("msg_no")], function(index) {
															if(index==1) {
																var exp = new RegExp(oldDOID,'g');
																that.JSONData = $.parseJSON(JSON.stringify(that.JSONData).replace(exp,newDOID));
				
																fnDeleteDO(oldDOID);
																fnSaveDO(newDOID, that.JSONData.acctid, JSON.stringify(that.JSONData), JSON.stringify(that.JSONParts), that.coveringRepMode, JSON.stringify(signatureData), addtionalInfo, JSON.stringify(that.otherInfo));
                                                                fnUpdateOldDoID(newDOID, localStorage.getItem("OLDDOID"));
                                                                //This is related to Read Tag from Image Changes and this code is to Update the Order id to the Tag Image related tables
                                                                fnUpdDOTagImgDtls(oldDOID, newDOID);
                                                                fnUpdDOTagDetails(oldDOID, newDOID);
																that.dosignature.submitInfo(function() {
																	that.bookMode = true;
																	boolUpdate = true;
																	that.bookDO();
																})
				
															}
															else{
																that.$("#btn-do-bookdo").text(lblNm("book_do"));
																that.bookMode = true;
																boolUpdate = true;
															}
																		
														});
													});
												});
											}
											else if(data.message.indexOf("A DO with the same Account-Part-Quantity combination already exists with ID")>=0) {
                                                var replacemessage = data.message.replace("A DO with the same Account-Part-Quantity combination already exists with ID",lblNm("msg_do_with_same_account"));
												showNativeConfirm(replacemessage +lblNm("msg_want_continue_do"),lblNm("msg_duplicate_order"),[lblNm("msg_yes"),lblNm("msg_no")], function(index) {
													if(index==1) {
														that.JSONData.skipvalidatefl = 'Y'
														fnSaveDO(that.JSONData.orderid, that.JSONData.acctid, JSON.stringify(that.JSONData), JSON.stringify(that.JSONParts), that.coveringRepMode, JSON.stringify(signatureData), addtionalInfo, JSON.stringify(that.otherInfo));
                                                        fnUpdateOldDoID(that.JSONData.orderid, localStorage.getItem("OLDDOID"));
														that.bookMode = true;
														boolUpdate = true;
														that.bookDO();
													}
													else {
														fnDeleteDO(that.JSONData.orderid);
                                                        //This is related to Read Tag from Image Changes and this code is to Update the Order id to the Tag Image related tables
                                                        //Delete when they click NO
                                                        fnDelDOFromTagImgDtls(that.JSONData.orderid);
                                                        fnDelTagsForProcessImage(that.JSONData.orderid);
														that.$("#btn-do-bookdo").text(lblNm("book_do"));
														that.bookMode = true;
														boolUpdate = true;
													}
												});
											}
											else if(data.message.indexOf("Control Number")>=0) {
												showNativeAlert(lblNm("msg_provide_valid_lot"),lblNm("msg_invalid_lot"));
												that.$("#btn-do-bookdo").text(lblNm("book_do"));
												that.bookMode = true;
												boolUpdate = true;
											}
											else if(data.message.indexOf("valid tag")>=0) {
												showNativeAlert(lblNm("msg_valid_tagid"),lblNm("msg_invalidtag"));
												that.$("#btn-do-bookdo").text(lblNm("book_do"));
												that.bookMode = true;
												boolUpdate = true;
											}
											else if(data.errorCode == "20643") {
												// showNativeAlert("The account selected for DO is not associated with selected sales rep. Device will update automatically and book","Missing Update");
												that.loaderview = new LoaderView({text : lblNm("msg_repid_mismatch")});
												DataSync_SL('listGmAccountVO','',function(status) {
													that.loaderview = new LoaderView({text : lblNm("msg_updating_do_c")});
													if(status!="success") {
														that.loaderview.close();
														that.$("#btn-do-bookdo").text(lblNm("book_do"));
														that.bookMode = true;
														boolUpdate = true;
														showNativeAlert(lblNm("msg_do_not_associated"),lblNm("msg_error_occurred"));
													}
													else {
														fnGetAccountInfo(that.JSONData.acctid, function(data) {
															//console.log(data);
															var oldrep = that.JSONData.repid;
															that.JSONData.repid = data.REP;
															var oldID = that.JSONData.orderid;
															that.JSONData.orderid = that.JSONData.orderid.replace(oldrep, that.JSONData.repid);
															fnDeleteDO(oldID);
															that.saveDO();
                                                            //This is related to Read Tag from Image Changes and this code is to Update the Order id to the Tag Image related tables (DOUBLE CHECK)
                                                            fnUpdDOTagImgDtls(oldID, that.JSONData.orderid);
                                                            fnUpdDOTagDetails(oldID, that.JSONData.orderid);
															that.dosignature.submitInfo(function() {
																that.loaderview = new LoaderView({text : lblNm("msg_booking_do_c")});
																that.bookMode = true;
																boolUpdate = true;
																that.bookDO();
															});
														});
													}

										        });
											}
											else if((data.message!=undefined) && (data.message!="")) {
												that.$("#btn-do-bookdo").text(lblNm("book_do"));
												that.bookMode = true;
												boolUpdate = true;
                                                var replacemessage = data.message.replace("DO already entered in the System",lblNm("msg_already_entered"));
												showNativeAlert(replacemessage,lblNm("msg_error_in"));
											}
											else {
												that.$("#btn-do-bookdo").text(lblNm("book_do"));
												that.bookMode = true;
												boolUpdate = true;
												showNativeAlert(lblNm("msg_ser_down_not_reach"),lblNm("error"));
											}

										}
										else {
											that.$("#btn-do-bookdo").text(lblNm("book_do"));
											that.bookMode = true;
											boolUpdate = true;
											showNativeAlert(lblNm("msg_ser_down_not_reach"),lblNm("error"));
										}
									}
								}); 
            }catch(err){
                 showAppError(err, "GmDODetailView:OnlineSaveDO()");
             }
        },
        
		saveDO: function() {
			 var that = this;
                try {
			var signatureData = new Array();
			$(".div-do-sign-label-header").each(function() {
				var printedName = $(this).find("#txt-do-sign-printed-name").val();
				var title = $(this).find("#txt-do-sign-title").val();
				var signature = $(this).find("#img-do-sign").attr("src");
				signatureData.push({"printedName" : printedName, "title" : title, "signature" : signature});
			});
			if((signatureData.length==0) && (this.signaturesrc!=""))
				signatureData = this.signaturesrc;
			var addtionalInfo = $("#div-do-billing-info-additional").css("background-image").replace('url(','').replace(')','');
            console.log("DO before local svae");
            console.log(JSON.stringify(this.JSONData));
			if(this.JSONData.acctid!="")
				fnSaveDO(this.JSONData.orderid, this.JSONData.acctid, JSON.stringify(this.JSONData), JSON.stringify(this.JSONParts), this.coveringRepMode, JSON.stringify(signatureData), addtionalInfo, JSON.stringify(this.otherInfo));
                fnUpdateOldDoID(this.JSONData.orderid, localStorage.getItem("OLDDOID"));
                fnGetDOID(that.JSONData.orderid, function(data) {
                        if (data.length == 0 || data.length == null || data.length == undefined  ){
                            showAppError(err, "GmDODetailView:SaveDO():fnGetDOID");
                        }
                    });
                } catch (err) {
                    showAppError(err, "GmDODetailView:SaveDO()");
                }
		},

		deleteDO: function() {
			var that = this;
			showNativeConfirm(lblNm("msg_want_delete_do"),lblNm("delete"),[lblNm("msg_yes"),lblNm("msg_no")], function(index) {
				if(index==1) {
					fnDeleteDO(that.JSONData.orderid);
                    //This is related to Read Tag from Image Changes and this code is to Update the Order id to the Tag Image related tables
                    fnDelDOFromTagImgDtls(that.JSONData.orderid);
                    fnDelTagsForProcessImage(that.JSONData.orderid);
					that.dobillinginfoview = null;
					that.dopartsused = null;
					that.doshiptoinfo = null;
					that.dosignature = null;
					that.dotagidview = null;
					that.DO = undefined;
					that.options = new Array();
					that.initialize(that.options);
				}
			});

		},
        
        processData: function() {
            try{
			var that = this;
                that.loaderview = new LoaderView({
                    text: lblNm("msg_uploading_pdf_c")
                });
           that.$("#btn-do-bookdo").text(lblNm("msg_booking_c"));
            that.bookMode = false;
            boolUpdate = false;
            that.JSONData.arrGmOrderItemVO = new Array();
            that.JSONData.arrGmOrderItemAttrVO = new Array();
            that.prepareSamePDFParts();
            var token = "";
                if (localStorage.getItem("token") != undefined) {
                    token = localStorage.getItem("token");
                }				
				var oldDOID = that.JSONData.orderid;
				var orderdate = that.JSONData.orderdate;
                var input = {
                    "token": token,
                    "orderdate": orderdate,
                    "prefixordid": oldDOID
                };
                fnGetWebServiceData("DeliveredOrder/getlatestdo", undefined, input, function (status, data) {
                    if (status) {
                        if (data.orderid != "" && data.orderid != undefined) {
                            that.OnlineSaveDO();
                        }else{
							that.iteration(0);
						}
                    } else{
						that.iteration(0);
					}
                });                        
            }catch(err){
                showAppError(err, "GmDODetailView:processData()");
            }

		},
            validateOrder: function () {
                console.log("validateOrder");
				var that =this;
                var token = "";
                if (localStorage.getItem("token") != undefined) {
                    token = localStorage.getItem("token");
                }				
				var oldDOID = that.JSONData.orderid;
				var orderdate = that.JSONData.orderdate;
                var input = {
                    "token": token,
                    "orderdate": orderdate,
                    "prefixordid": oldDOID
                };
                fnGetWebServiceData("DeliveredOrder/getlatestdo", undefined, input, function (status, data) {
                    if (status) {
                        if (data.orderid != "") {
                            return false;
                        }else{
							return true;
						}
                    } else{
						return true;
					}
                });
                console.log("validateOrder1");
            },
             addNPIFromTextSearch:function(){
                var that = this;
                if((GM.Global.NPIID!=undefined && GM.Global.NPIID!="")|| (GM.Global.NPINAME!=undefined && GM.Global.NPINAME!="")){
                     var rowEl = $("<a id=" + GM.Global.NPIID + " class=item-searchlist name=" + GM.Global.NPINAME + "><div class='div-data hide'></div> <i class='fa fa-check'></i></a>");
                    var CurrentRow = {
                    "ID": GM.Global.NPIID,
                    "Name": GM.Global.NPINAME,
                    "Element": rowEl
                };
                    $('#surgeonName #div-keyword-search #div-common-search-x').addClass('hide');
                    $('#npiNum #div-keyword-search #div-common-search-x').addClass('hide');
                    $('#surgeonName #div-search-result-main #ul-search-resultlist').addClass('hide');
                    $('#npiNum #div-search-result-main #ul-search-resultlist').addClass('hide');
                 this.donpidetailview.loadNPIDetails(CurrentRow);
                }
               
            },

		/* validateTissuePartsAddressType: Function to validate the part family type for tissue parts and it it's not shippping to Hospital address then throw validation message
		 * Author : Karthik
		 */
            validateTissuePartsAddressType:function(){
            	var that = this;
            	var strErrorMessage = "";
            	var pnumID = "";
            	var pnumType = "";
            	var pnumMaterial = "";
            	var pnumShipTo = "";
            	var pnumAddressType = "";

				if(this.JSONData.acctid != "" && this.JSONData.orderid != ""){

				 var arrPartsInfo = $.parseJSON(JSON.stringify(this.JSONParts)); 
	             var len = arrPartsInfo.length;

            		for(var i=0;i<len;i++) {
            			 console.log("len == "+len);            			 	

            			 if(arrPartsInfo[i] != undefined){
            			 	 pnumID = arrPartsInfo[i].ID;
            			 	 pnumType = arrPartsInfo[i].type;
            			 	 pnumMaterial = arrPartsInfo[i].pmaterial;
            			 }

            			 if(arrPartsInfo[i]['shipTo'] != undefined){
            			 	pnumShipTo = arrPartsInfo[i]['shipTo'].shipto;
            			 }
            			 if(arrPartsInfo[i]['AddressData'] != undefined){
            			 	pnumAddressType = arrPartsInfo[i]['AddressData'].AddressType;
            			 }
            			  

            			  console.log("pnumID == "+pnumID);  
            			  console.log("pnumType == "+pnumType); 
            			  console.log("pnumMaterial == "+pnumMaterial); 
            			  console.log("pnumShipTo == "+pnumShipTo); 
            			  console.log("pnumAddressType == "+pnumAddressType); 

            			 	 if(pnumType != 'L' && pnumMaterial == '100845') {
            			 	 	if(pnumShipTo != '4122') { //Not Hospital

            			 	 		if(pnumShipTo == '4121' && pnumShipTo != "") { //sales rep
            			 	 			
            			 	 			if(pnumAddressType != '26240675'){
            			 	 				strErrorMessage +=  pnumID + ',';
            			 	 			}

            			 	 		} else{
            			 	 				strErrorMessage +=  pnumID + ',';
            			 	 		}
            			 	 	}
            			 	 }
            		}
            		 if (strErrorMessage != "") {
	            		 strErrorMessage = strErrorMessage.replace(/,(\s+)?$/,'');
	            		 strErrorMessage = lblNm("msg_order_tissue_part_validation") + strErrorMessage;
            		 }
           		}	
           		 localStorage.setItem("TissueErrorMsg",strErrorMessage);
			},
			
			/* validateTissueQty: Function to validate the quantity of a tissue parts, if tissue part quantity is more than 1 then throw validation message
			 * PC-866
			 */
			validateTissueQty: function () {
                var that = this;
                var strErrorQty = "";
                var pnumID = "";
                var pnumMaterial = "";
                var qty = "";
                var lotno = "";
                var arrPartsInfo = $.parseJSON(JSON.stringify(this.JSONParts));
                var len = arrPartsInfo.length;

                for (var i = 0; i < len; i++) {

                    if (arrPartsInfo[i] != undefined) {
                        pnumID = arrPartsInfo[i].ID;
                        pnumMaterial = arrPartsInfo[i].pmaterial
                        qty = arrPartsInfo[i].Qty;
                        lotno = arrPartsInfo[i].lotno;
                    }
                    
                    if (pnumMaterial == '100845') {
                        if (qty > 1) {
                            strErrorQty += pnumID + ',';
                        }

                    }

                }
                
                if (strErrorQty != "") {
                    strErrorQty = strErrorQty.replace(/,(\s+)?$/, '');
                    strErrorQty = lblNm("msg_tissue_part_qty_validation") + strErrorQty;
                }
                localStorage.setItem("TissueQtyMsg", strErrorQty);
            }

	});	

	return DOMainView;
});