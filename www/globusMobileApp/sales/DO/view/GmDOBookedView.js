/**********************************************************************************
 * File:        GmDOBookedView.js
 * Description: 
 * Version:     1.0
 * Author:     	tsekaran
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification','saleitemmodel', 'saleitemcollection', 'saleitemlistview','itemview', 'numberkeypadview'
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification, SaleItem, SaleItems, SaleItemListView,ItemView,NumberKeypadView) {

	'use strict';
	var DOBookedView = Backbone.View.extend({

		events : {
			"click .clear-row-pu": "clearRow",
			"keyup #txt-search-booked-do": "searchKeyword",
			"click #btn-do-booked-doid" : "openDO",
			"click .div-do-booked-status" : "displayReason",
			"click .fa-file-pdf-o" : "showPDFOptions"
		},
		/* Load the templates */ 
		template: fnGetTemplate(URL_DO_Template, "GmDOBooked"),
		tplSpinner: fnGetTemplate(URL_Common_Template, "GmProgressSpinner"),
		

		initialize: function(options) {
			
			this.render(options);
			var that = this;
			this.showBookedDOList();
			
		},

		// Render the initial contents when the Catalog View is instantiated
		render: function (options) {
			this.$el.html(this.template());
			return this;
		},
		
		showBookedDOList:function(){
			var that = this;
			this.$("#div-do-booked-content").hide();
			this.$(".div-do-page-loader").html(this.tplSpinner({"message":lblNm("msg_data_fetching")})).show();
			fnGetSearchBooked('',function(data){
				if(data.length>0) {
					data.reverse();
					var currSymbol = localStorage.getItem("cmpcurrsmb");
					for(var i=0;i<data.length;i++) {
						
						var JSONData = $.parseJSON(data[i].JSON);
						data[i].TOTAL = currSymbol+""+formatCurrency(JSONData.total);
						data[i].ORDERDATE = JSONData.orderdate;
						data[i].CUSTPO = JSONData.custpo;
						console.log(data[i].COVERINGREPFL);
					}
	 				
					var items = new SaleItems(data);
                    if ($(window).width() < 480) {
                        that.showItemListView(that.$("#div-do-booked-do-list"), items, "GmDOBookedListMobile", URL_DO_Template);
                        $(".bookListMobile").on('swipeleft', function (e, data) {
                            $(this).find(".delete-pu-span").addClass("onShowEl");
                        });
                        $(".bookListMobile").on('tap', function (e, data) {
                            $(this).find(".delete-pu-span").removeClass("onShowEl");
                        });
                    }
                    else{
                        that.showItemListView(that.$("#div-do-booked-do-list"), items, "GmDOBookedList", URL_DO_Template);	
                    }
					$(".div-do-booked-do-list-item").each(function(){
						if ($(this).attr("coveringrep") == "true") {
							$(this).find(".div-do-booked-total").html("-");
						}
					});
					that.$(".div-do-page-loader").hide();
					that.$("#div-do-booked-content").show();
				}
				else
					{
						that.$(".div-do-page-loader").show();
						that.$(".div-do-page-loader").html(lblNm("msg_no_data_display"));	
					}
							
			});

		},
		openDO:function(event){
				var elem = event.currentTarget;
				console.log(elem);
			   if(this.$(elem).parent().parent().find(".div-do-booked-status").text().replace(/^\s+|\s+$/gm,'').toUpperCase() != "Pending Sync".replace(/^\s+|\s+$/gm,'').toUpperCase())
					window.location.href = "#do/BookDO/" + $(elem).text();
                    fnGetOldDOID($(elem).text(),function(olddoid){
                        localStorage.setItem("OLDDOID", olddoid.OLDDOID); 
                    });
				
		},
		//Search the Collection
		searchKeyword: function (event) {	
					this.listView.searchItemList(event);
		},
		showItemListView: function(controlID, collection, itemtemplate, template_URL) {
			
			this.listView = new SaleItemListView(
					{ 
						el: controlID, 
						collection: collection, 
						itemtemplate: itemtemplate,
						template_URL: template_URL,
						columnheader: 0
					}
			);
			return this.listView;
		},

		clearRow: function(e){
             var DOID="";
			 if ($(window).width() < 480) {
                 DOID=$(e.currentTarget).parent().parent().find('.div-do-booked-doid').find('#btn-do-booked-doid').text();
                  showNativeConfirm(lblNm("msg_want_delete")+ DOID +"?", lblNm("delete"), [lblNm("msg_yes"),lblNm("msg_no")], function (index) {
            	if(index==1) {
	            	fnDeleteDO(DOID);
                    //This is related to Read Tag from Image Changes and this code is to Update the Order id to the Tag Image related tables
                    //Delete when they click MINUS ICON
                    fnDelDOFromTagImgDtls(DOID);
                    fnDelTagsForProcessImage(DOID);
	                $(e.currentTarget).parent().parent().parent().remove();   
            	}  
            });  
             }
            else{
              DOID= $(e.currentTarget).parent().find("#btn-do-booked-doid").text();
            showNativeConfirm(lblNm("msg_want_delete")+ DOID +"?", lblNm("delete"), [lblNm("msg_yes"),lblNm("msg_no")], function (index) {
            	if(index==1) {
	            	fnDeleteDO(DOID);
                    //This is related to Read Tag from Image Changes and this code is to Update the Order id to the Tag Image related tables
                    //Delete when they click MINUS ICON
	            	fnDelDOFromTagImgDtls(DOID);
                    fnDelTagsForProcessImage(DOID);
	                $(e.currentTarget).parent().parent().parent().remove();   
            	}  
            });  
            }
			
		
		},
		
		showPDFOptions: function(event) {
			var that = this;
			var DOId = $(event.currentTarget).parent().parent().find('#btn-do-booked-doid').text();
			var that = this, folder = "WP/";
			if($(event.currentTarget).parent().parent().attr('coveringrep')=="true") {
				folder = "WOP/";
			}
			fnGetLocalFileSystemPath(function(URL) {
				URL = URL + 'DO/PDF/' + folder + DOId + '.pdf';
				console.log(URL);
				fnGetImage(URL, function(status) {
					if(status!='error') {
						var options = [lblNm("msg_open"), lblNm("email_with_price"), lblNm("email_without_price"), lblNm("fax_with_price"), lblNm("fax_without_price"), lblNm("msg_close")]
						showNativeConfirm(lblNm("msg_would_like_pdf"),lblNm("pdf"),options,function(buttonIndex) {
							if(buttonIndex==1) {
								that.openPDF(event);
							}
							else if((buttonIndex == 3) || (buttonIndex == 2)) {
								that.emailPDF(buttonIndex,event);
							}
							else if((buttonIndex == 4) || (buttonIndex == 5)) {
								that.faxPDF(buttonIndex,event);
							}
						});
					}
					else{
						fnShowStatus(lblNm("msg_not_available") + DOId);
					}
				});
			});
			
		},
		
		

		openPDF: function(event) {
			var DOId = $(event.currentTarget).parent().parent().find('#btn-do-booked-doid').text();
			var that = this, folder = "WP/";
			if($(event.currentTarget).parent().parent().attr('coveringrep')=="true") {
				folder = "WOP/";
			}
			fnGetLocalFileSystemPath(function(URL) {
				URL = URL + 'DO/PDF/' + folder + DOId + '.pdf';
				console.log(URL);
				fnGetImage(URL, function(status) {
					//var DOCViewer = window.open(URL, '_blank', 'location=no');
					  openPdfFromLocalFileSystem(URL);
				});
			});
		},
		
		emailPDF: function(buttonIndex, event) {
			var DOId = $(event.currentTarget).parent().parent().find('#btn-do-booked-doid').text();
			var that = this;
			GM.Global.DOPDFFOLDER = "DO/PDF/WP/";
			fnShowStatus(lblNm("msg_fetching_pdf"));
			if(buttonIndex == 3)
				GM.Global.DOPDFFOLDER = "DO/PDF/WOP/";
			fnGetLocalFileSystemPath(function(URL) {
				//URL = URL + 'DO/PDF/' + folder + DOId + '.pdf';
				GM.Global.PDFPATH = URL;
			 	GM.Global.PDFORDERID = DOId;
				var Name = localStorage.getItem('fName') + " " + localStorage.getItem('lName');	
				fnGetImage(URL, function(status) {
//					fnShowStatus('');
					if(status!='error') {
			 			if ($(window).width() < 480) 
	                		sendDOPDFEmailViaMobile(function(callback) {
				 			});
	            		else
	                	 	sendDOPDFEmailViaIpad(function(callback) {
				 			});
					}
					else
						fnShowStatus(lblNm("msg_not_available") + DOId);
				});
			});
				
		},
		
		faxPDF: function(buttonIndex, event) {
			var that = this;
			var DOId = $(event.currentTarget).parent().parent().find('#btn-do-booked-doid').text();
			var folder = "WP/";
			if(buttonIndex==5)
				folder = "WOP/";
//			$('#overlay-back').fadeIn(100,function(){
				if(that.keypadview)
					that.keypadview.close();
		 		that.keypadview = new NumberKeypadView({
					el : that.$("#div-booked-show-num-keypad"),
					callback: function(val) {that.preparePDF(DOId, folder, val);}
				});
		 		if(buttonIndex==5)
		 			$("#div-number-keypad-title").html(lblNm("fax_without_price")+"</div>");
		 		else 
		 			$("#div-number-keypad-title").html(lblNm("fax_with_price")+"</div>");
				$("#div-number-keypad-close").show();
				$("#div-number-decimal").hide();
				$("#div-number-keypad-number-symbol").css("width","69px").css("height","17px").css("margin-left","0");
				$("#div-number-keypad-btn-done").html(lblNm("msg_send_fax"));
//			});
		},
		
		preparePDF: function(DOId, folder, value) {

			var that = this;
			var pdfPath = "";

			fnGetLocalFileSystemPath(function(URL) {
				pdfPath = URL + 'DO/PDF/' + folder + DOId + '.pdf';

				window.resolveLocalFileSystemURL(pdfPath,function(entry){
			            entry.file(function (file) {
			                var reader = new FileReader();
			                reader.onloadend = function(evt) {
			                    console.log(">>>>>>>>>Read as binary<<<<<<<<");
			                    fnSendFax(evt.target.result,value);
			                };
			                reader.readAsArrayBuffer(file);
			            }, function  (argument) {
			                console.log('error file');
				            })
				            

				        },function() {
				            console.log('error getting file');
				        });
			    },null);
		},

		displayReason: function(event) {
			var elem = $(event.currentTarget).find('i');
			if($(elem).css('display')!="none") 
				showNativeAlert($(elem).attr("data-msg"),lblNm("reason"));
		}


	});	

	return DOBookedView;
});