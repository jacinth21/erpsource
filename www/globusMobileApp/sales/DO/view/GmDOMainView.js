/**********************************************************************************
 * File:        GmDOMainView.js
 * Description: Initial DO View to load other sub views
 * Version:     1.0
 * Author:     	tvenkateswarlu
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification', 'dodetailview', 'doreportview', 'dobookedview', 'doudiview'
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification, DODetailView, DOReportView, DOBookedView, DOUDIView) {

	'use strict';
	var DOMainView = Backbone.View.extend({

		events : {
			"click #btn-case-link" : "openLink"
		},

		/* Load the templates */ 
		template: fnGetTemplate(URL_DO_Template, "GmDOMain"),

		initialize: function(options) {
			$("body").removeClass();
			$("#btn-home").show();
			$("#btn-back").show();
			$("#div-app-title").html(lblNm("delivery_dashboard"));
			if(parseInt(localStorage.getItem("AcID"))>2) {
				if(localStorage.getItem("dteFieldSalesList")==null)
					fnFetchFieldSalesList();
				else {
					var dteLastFSSync = localStorage.getItem("dteFieldSalesList");
					var one_day=1000*60*60*24;
					var now = new Date();
					var difference_ms = now - dteLastFSSync;
					if(Math.round(difference_ms/one_day)>0)
						fnFetchFieldSalesList();
				}
			}
			
			this.render(options);
		},

		// Render the initial contents when the Catalog View is instantiated
		render: function (options) {
//			showMsgView("011");
			this.$el.html(this.template());
            
             /* Hiding UDI tab for OUS users */
            var moduleaccess = localStorage.getItem("moduleaccess");
            if ($(window).width() <= 480) {
               var height = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
            this.$('#div-do-view').height(height-49);
            }
            if (moduleaccess != null && moduleaccess.indexOf("btn-case-udi") != -1){
               this.$("#btn-case-udi").hide();
               this.$('div.div-do-detail-navigation').find('a').removeClass("div-do-detail-navigation btn-detail").addClass("a-do-navigation-width btn-detail");
            }
			this.strViewType = options[0];
			if(!this.strViewType) {
				this.strViewType = "detail";
			}
            localStorage.setItem("OLDDOID",'');
				switch(this.strViewType) {
					case "detail":
						this.dodetailview = new DODetailView({'el' :this.$('#div-do-main-content'), 'parent' : this});
						this.$("#div-do-title").html(lblNm("book_do"));
						this.display("book");
						break;	
					case "booked":
						fnCheckDOStatus();
						this.dobookedview = new DOBookedView({'el' :this.$('#div-do-main-content'), 'parent' : this});
						this.$("#div-do-title").html(lblNm("draft_rejected"));
						this.display("booked");
						break;	
					case "report":
						fnCheckDOStatus();
						this.doreportview = new DOReportView({'el' :this.$('#div-do-main-content'), 'parent' : this});
						this.$("#div-do-title").html(lblNm("dos_booked"));
						this.display("report");
						break;	
					case "udi":
						this.doudiview = new DOUDIView({'el' :this.$('#div-do-main-content'), 'parent' : this});
						this.$("#div-do-title").html(lblNm("msg_udi_generator"));
						this.display("udi");
						break;

					default:
						this.dodetailview = new DODetailView({'el' :this.$('#div-do-main-content'), 'parent' : this, 'DO' : this.strViewType});
						this.$("#div-do-title").html(lblNm("book_do"));
						this.display("book");
				}
			$('#div-app-title').removeClass('gmVisibilityHidden').addClass('gmVisibilityVisible');
       //$("#div-app-title").removeClass("gmMediaVisibilityH").addClass("gmMediaVisibilityV");
			return this;
		},
		
		// Toggles the view
		display: function(key) {

			$("#ui-datepicker-div").hide();
			if(key.currentTarget==undefined) {
				this.key = key;
			}
			else {
				this.key = "detail";
				
			}
			this.$(".btn-detail").each(function() {
				$(this).removeClass("btn-selected");
			});

			this.$("#div-case-" + this.key).show();
			this.$("#btn-case-" + this.key).addClass("btn-selected");

		},

		toggleContent: function (event) {
			var id = (event.currentTarget.id).replace("div-do-detail-label-","");
			this.$(".selected-tab").removeClass("selected-tab");
			$(event.currentTarget).addClass("selected-tab");

		},	
		
		openLink: function () {
		    _.each($(".in-app"), function (sdata) {
		        _.each(localStorage.getItem("moduleaccess").split(','), function (data) {
		            if ($(sdata).attr("href").replace("#", "") == data.replace("btn-", ""))
		                $(sdata).addClass("hide");
		        });

		    });
		    $("#btn-case-link").each(function () {
		        $(this).toggleClass("show-app");
		    })
            
		    $(".btn-app-link").toggle("fast");

		},
		
		// Close the view and unbind all the events
		close: function(e) {

			if(this.dodetail)
				this.dodetail.close();
			this.unbind();
			this.remove();

		}

	});	

	return DOMainView;
});
