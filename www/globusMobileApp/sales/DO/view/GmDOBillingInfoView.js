/**********************************************************************************
 * File:        GmDOBillingInfoView.js
 * Description: 
 * Version:     1.0
 * Author:     	tvenkateswarlu
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification', 'searchview', 'dropdownview', 'loaderview', 'imagecropview','date'
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification, SearchView, DropDownView, LoaderView, CropView , Date) {

	'use strict';
	var DOBillingInfoView = Backbone.View.extend({

		events : {
			"click #div-do-billing-info-surgery-date label, #div-do-billing-info-surgery-date i" : "showDatepicker",
			"keyup #txt-keyword": "displayAccounts",
			"change #div-do-billing-info-surgery-date input" : "getSurgeryDate",
			"click #span-do-billing-doid-repid" : "showSalesReps",
			"click .fa-camera" : "getPicture",
			"click #div-do-billing-salesrep-search-x" : "closeSalesrepSearch",
			"change input" : "updateJSON"
		},

		/* Load the templates */ 
		template: fnGetTemplate(URL_DO_Template, "GmDOBillingInfo"),

		initialize: function(options) {
			this.parent = options.parent;
			this.surgerylevelid = "";
			this.surgerytypeid = "";
			this.caseinfoid = "";
            this.ordertype = "";
	        this.dealid="";
            this.epgsid="";  //PC-4658-Add EGPS Usage in DO - Mobile Device
            this.accDivCompanyFl = 'N';
            this.accEPGSFl = 'N';
			this.render();
		},

		// Render the initial contents when the Catalog View is instantiated
		render: function () {
			console.log(this.parent.JSONData);

			var viewJSON  = {};
			var imgsrc = "";
			viewJSON = $.parseJSON(JSON.stringify(this.parent.JSONData));
			if(this.parent.DO != undefined ) {
				console.log(this.parent.JSONData.arrGmOrderAttrVO);
				for(var i=0;i<this.parent.JSONData.arrGmOrderAttrVO.length;i++) {

					switch(this.parent.JSONData.arrGmOrderAttrVO[i].attrtype) {
						case "400146":  //103600
							viewJSON.surgeonName = this.parent.JSONData.arrGmOrderAttrVO[i].attrvalue;
							break;
						case "103602":
							viewJSON.surgeryType = this.parent.JSONData.arrGmOrderAttrVO[i].attrvalue;
							this.surgerytypeid = viewJSON.surgeryType;
							break;
						case "103601":
							viewJSON.surgerydate = this.parent.JSONData.arrGmOrderAttrVO[i].attrvalue;
							break;
						case "103603":
							viewJSON.surgeryLevel = this.parent.JSONData.arrGmOrderAttrVO[i].attrvalue;
							this.surgerylevelid = viewJSON.surgeryLevel;
							break;
                        case "107802": //Order Type
							viewJSON.ordertype = this.parent.JSONData.arrGmOrderAttrVO[i].attrvalue;
							this.ordertype = viewJSON.ordertype;
							break;
                        case "26241315": //PC-4658-Add EGPS Usage in DO - Mobile Device
                            viewJSON.egpuse = this.parent.JSONData.arrGmOrderAttrVO[i].attrvalue;
							this.epgsid = viewJSON.egpuse;
							break;
					}

				}
                this.accDivCompanyFl = this.parent.JSONData.accDivCompanyFl;
                this.accEPGSFl = this.parent.JSONData.accEPGSFl;
				this.caseinfoid = viewJSON.caseinfoid;
				imgsrc = this.parent.additionalInfoSrc;
				imgsrc = imgsrc.replace(/"/g, '');
				viewJSON.imgsrc = imgsrc;
			}
			else {
				//viewJSON.surgerydate = this.parent.JSONData.orderdate;
			}
			
			this.$el.html(this.template(viewJSON));

			if(this.parent.DO==undefined)
				this.$("#img-billing-info-notes").hide();
			
			// var todaydate = new Date();
			var that = this;
			if (this.parent.DO != undefined ) {
				var orderid = this.parent.JSONData.orderid;
				var orderidparts = [];
				orderidparts = orderid.split("-");
				$("#span-do-billing-doid-repid").html(that.fnFormatOrderID(orderidparts[0]));
				$("#span-do-billing-doid-date").html(orderidparts[1]);
				$("#span-do-billing-doid-count").html(orderidparts[2]);
				$("#div-do-billing-doid").css("display","inline-block");
				var accountid = this.parent.JSONData.acctid;
				console.log(accountid);
				fnGetAccountName(accountid,function(data){
					that.$("#div-do-billing-acname").html(data);
					that.$("#div-do-billing-acname").attr("repid",that.parent.JSONData.repid);
                    that.$("#div-do-billing-acname").attr("dealid",that.parent.JSONData.dealid);
                    that.$("#div-do-billing-acname").attr("divcompanyid",that.parent.JSONData.divcompanyid);
				});
				
				this.getDistributor(this.parent.JSONData.repid,true);
				
			}
            if(this.accDivCompanyFl == 'Y'){
              
                 $("#div-do-mand-surg-lvl").css("display","inline-block");
                 $("#div-do-mand-surg-typ").css("display","inline-block");
                 $("#div-do-mand-add-info").css("display","inline-block");
                 $("#div-do-billing-info-div-surglvl-list").removeClass("hide");
                 $("#div-do-billing-info-surgerylevel-list").addClass("hide");
                 $("#div-do-billing-info-div-surgtyp-list").removeClass("hide");
                 $("#div-do-billing-info-surgerytype-list").addClass("hide");
   
             }else{
                 $("#div-do-mand-surg-lvl").css("display","none");
                 $("#div-do-mand-surg-typ").css("display","none");
                 $("#div-do-mand-add-info").css("display","none");
                 $("#div-do-billing-info-surgerylevel-list").removeClass("hide");
                 $("#div-do-billing-info-div-surglvl-list").addClass("hide");
                 $("#div-do-billing-info-surgerytype-list").removeClass("hide");
                 $("#div-do-billing-info-div-surgtyp-list").addClass("hide");
                 
            }
			// var dd = ("0" + todaydate.getDate()).slice(-2), mm = ("0" + (todaydate.getMonth() + 1)).slice(-2), yyyy = todaydate.getFullYear();
			// this.$("#div-do-billing-info-entered-date").html(mm+"/"+dd+"/"+yyyy);
			// this.$("#div-do-billing-info-surgery-date input").val(mm+"/"+dd+"/"+yyyy);
			var renderOptions = {
					"el":this.$('#div-do-billing-account-search'),
					"ID" : "txt-billing-account",
            		"Name" : "txt-search-name",
            		"Class" : "txt-keyword-account",
            		"minChar" : minKeyinChar,
            		"autoSearch": true,
            		"placeholder": lblNm("msg_character_search"),
            		"fnName": fnGetAccounts,
            		"usage": 1,
            		"template" : 'GmSearchResult',
					"template_URL" : URL_Common_Template,
					"callback": function(rowValue) {
                        that.getAccountFromSearchView(rowValue);
                    }
            		
            	};

			var searchview = new SearchView(renderOptions);
			this.getSurgeryLevel();
			this.getSurgeryType();
            this.getEPGSUsage();
			//show Order type based on the company
		        fnGetRuleValueByCompany ("DO_ORDTP_FL","DO_ORDER",localStorage.getItem("cmpid"),function(data){
		            if(data.length > 0){
		               
		             if(data[0].RULEVALUE=='Y'){
		                    that.getOrderType();
                         if ($(window).width() <= 480) {
                         $('#li-do-billing-surgerydate-label').height('80px');
                             $('#div-do-billing-info-surgery-date').height('44%');
                         }
		             }
		             }
		             else
		             {
		                 $("#div-do-billing-info-order-type").hide();
		             }
		            });
			var signAdjust;
			if(this.parent.DO!=undefined)
				signAdjust = false;
			this.getCaseID(signAdjust);
$("#div-footer").addClass("hide") 
			return this;
		},
		
		showDatepicker: function (event) {
			$("#div-do-billing-info-surgery-date input").datepicker( {maxDate: 0,  duration: 'fast'});
			var visible = $('#ui-datepicker-div').is(':visible');
			   console.log(visible);
			$("#div-do-billing-info-surgery-date input").datepicker(visible? 'hide' : 'show');	
			
			if( $("#span-do-billing-doid-repid").text() == ""){
				this.$("#div-do-billing-doid").hide();
			}
			
			else {
				this.$("#div-do-billing-doid").show();
			}
			
			event.stopPropagation();
			$(window).click(function (event) {
			    if(document.getElementById("ui-datepicker-div").style.display != "none") {
			    	var elem = event.target;
			       if(!($(elem).hasClass("ui-datepicker-prev") || $(elem).hasClass("ui-datepicker-next") || $(elem).parent().hasClass("ui-datepicker-prev") || $(elem).parent().hasClass("ui-datepicker-next"))) {
			    	   var visible = $('#ui-datepicker-div').is(':visible');
			    	   $("#div-do-billing-info-surgery-date input").datepicker(visible? 'hide' : 'show');
//			    	   $('#ui-datepicker-div').hide();
			       }
			       }
			   });
		},
		
		getSurgeryLevel: function() {
			var that = this;
			var defaultid;
			if( this.surgerylevelid !=""){
				defaultid = this.surgerylevelid;
			}
			
			else {
				defaultid = 0;
			}

                
             //To load Surgery Level Dropdown based on the account division from Cloud
                fnGetCodeLKpFromCloud("DTRLVL",function(data){
                              if(data != ''){
                         console.log(data);
                            data.push({"ID" : 0, "Name" : "Choose One"});
                            that.dropdownview = new DropDownView({
                                    el : that.$("#div-do-billing-info-div-surglvl-list"),
                                    data : data,
                                    DefaultID : defaultid
                                });

                               that.$("#div-do-billing-info-div-surglvl-list").bind('onchange', function (e, selectedId,SelectedName) {
                                    that.surgerylevelid = selectedId  ;
                                    that.updateJSON();
                                });
                               
                               }

                        });

            //To load Surgery Level Dropdown based on the account division  from Code lookup table    
                        fnGetSurgeryLevel("TRLVL",function(data){
                           
                            data.push({"ID" : 0, "Name" : "Choose One"});
                            that.dropdownview = new DropDownView({
                                    el : that.$("#div-do-billing-info-surgerylevel-list"),
                                    data : data,
                                    DefaultID : defaultid
                                });

                                that.$("#div-do-billing-info-surgerylevel-list").bind('onchange', function (e, selectedId,SelectedName) {
                                    that.surgerylevelid = selectedId  ;
                                    that.updateJSON();
                                });
                        });

		},
		
        getSurgeryType: function() {
			var that = this;
			var defaultid;
			if( this.surgerytypeid !=""){
				defaultid = this.surgerytypeid;
			}
			
			else {
				defaultid = 0;
			}
             //To load Surgery Type Dropdown based on the account division from Cloud
                        fnGetCodeLKpFromCloud("DSURTY",function(data){
                            
                            if(data != ''){
                   
                         		data.push({"ID" : 0, "Name" : "Choose One"});
                                that.dropdownview = new DropDownView({
                                    el : that.$("#div-do-billing-info-div-surgtyp-list"),
                                    data : data,
                                    DefaultID : defaultid
                                });
                                

                                $("#div-do-billing-info-div-surgtyp-list").bind('onchange',  function (e, selectedId,SelectedName) {
                                    that.surgerytypeid = selectedId ;
                                    that.updateJSON();
                                });
                               
                             	
                            }
                         });
             //To load Surgery Type Dropdown based on the account division from Cloud
                    fnGetSurgeryLevel("SURTYP",function(data){
                        data.push({"ID" : 0, "Name" : "Choose One"});
                        that.dropdownview = new DropDownView({
                            el : that.$("#div-do-billing-info-surgerytype-list"),
                            data : data,
                            DefaultID : defaultid
                        });

                        that.$("#div-do-billing-info-surgerytype-list").bind('onchange', function (e, selectedId,SelectedName) {
                            that.surgerytypeid = selectedId ;
                            that.updateJSON();
                        });
                    });

		},
        //PC-4658, PC-5892-Add EGPS Usage in DO - Mobile Device
         getEPGSUsage: function() {
			var that = this;
			var defaultid;
			if( this.epgsid !=""){
				defaultid = this.epgsid;
			}
			else {
				defaultid = 0;
				that.epgsid = defaultid;
				that.updateJSON();
			}
            fnGetCodeLKpFromCloud("EGPUSE",function(edpuseData){
                 edpuseData.push({"ID" : 0, "Name" : "Choose One"});
              
				that.dropdownview = new DropDownView({
					el : that.$("#div-do-billing-info-egpuse-list"),
					data : edpuseData,
					DefaultID : defaultid
				});
				
				that.$("#div-do-billing-info-egpuse-list").bind('onchange', function (e, selectedId,SelectedName) {
					that.epgsid = selectedId ;
					that.updateJSON();
				});
             });	
		},  
        
		
		getAccountFromSearchView: function(rowValue){
			if($(rowValue.Element).attr("synced")=='Y') 
            {
                if( $("#span-do-billing-info-surgery-date").html() != "") {
                    var that = this;
                    var ordfmt;
                    $("#div-itemlist").hide();
                    $("#txt-billing-account").val('');
                    $("#div-do-billing-acid").html(rowValue.ID);
                    $("#div-do-billing-acname").html(rowValue.Name);
                    $("#div-do-billing-acname").attr("repid",$(rowValue.Element).attr("repid"));
                    $("#div-do-billing-acname").attr("dealid",$(rowValue.Element).attr("dealid"));
                    $("#div-do-billing-acname").attr("divcompanyid",$(rowValue.Element).attr("divcompanyid"));
                    $("#div-do-billing-doid").css("display","inline-block");
		            this.dealid=$(rowValue.Element).attr("dealid");
                    if($(rowValue.Element).attr("dealid") != ''){
                        ordfmt="dealid";
                    }else{
                        ordfmt="repid";
                    }
                    //To hide and show the EGPS dropdown based on the division
                    var accDivcompId=$(rowValue.Element).attr("divcompanyid");
                    //Getting data from firebase
                    fnGetRulesFromCloud(accDivcompId,"ACCOUNT_DIV_JR",localStorage.getItem("cmpid"),function(ruleData){
                        if(ruleData != ''&& ruleData[0].RULE_VALUE == 'Y'){
                                that.accDivCompanyFl = 'Y';
                                that.parent.JSONData.accDivCompanyFl=that.accDivCompanyFl;
                                $("#div-do-mand-surg-lvl").css("display","inline-block");
                                $("#div-do-mand-surg-typ").css("display","inline-block");
                                $("#div-do-mand-add-info").css("display","inline-block");
                                $("#div-do-billing-info-div-surglvl-list").removeClass("hide");
                                $("#div-do-billing-info-surgerylevel-list").addClass("hide");
                                $("#div-do-billing-info-div-surgtyp-list").removeClass("hide");
                                $("#div-do-billing-info-surgerytype-list").addClass("hide");

                           }else{
                                that.accDivCompanyFl = 'N';
                                that.parent.JSONData.accDivCompanyFl=that.accDivCompanyFl;
                                $("#div-do-mand-surg-lvl").css("display","none");
                                $("#div-do-mand-surg-typ").css("display","none");
                                $("#div-do-mand-add-info").css("display","none");
                                $("#div-do-billing-info-surgerylevel-list").removeClass("hide");
                                $("#div-do-billing-info-div-surglvl-list").addClass("hide");
                                $("#div-do-billing-info-surgerytype-list").removeClass("hide");
                                $("#div-do-billing-info-div-surgtyp-list").addClass("hide");
                           }
                    });
                    that.surgerylevelid  = "";
                    that.surgerytypeid = "";
                    $("#div-do-billing-info-surgerylevel-list li#0").trigger("click");
                    $("#div-do-billing-info-div-surglvl-list li#0").trigger("click");
                    $("#div-do-billing-info-surgerytype-list li#0").trigger("click");
                    $("#div-do-billing-info-div-surgtyp-list li#0").trigger("click");

                    
                    //Getting data from firebase
                    fnGetRulesFromCloud(accDivcompId,"ACC_DIV_EGPS",localStorage.getItem("cmpid"),function(ruleData){
                        if(ruleData != '' && ruleData[0].RULE_VALUE == 'Y'){
                         that.accEPGSFl = 'Y';
                         that.parent.JSONData.accEPGSFl=that.accEPGSFl;
                        }
                    });
                    $("#span-do-billing-doid-repid").html(that.fnFormatOrderID($(rowValue.Element).attr(ordfmt)));
                    $("#span-do-billing-doid-repid").attr("name",$(rowValue.Element).attr("repname"));

                    var sdate = new Date();
                    var smm = ("0" + (sdate.getMonth() + 1)).slice(-2), sdd = ("0" + sdate.getDate()).slice(-2), syy = sdate.getFullYear();  
                    syy =  syy.toString().substr(2,2);
                    //var surgerydate = smm+"/"+sdd+"/"+syy;
                    var surgerydate =  $("#label-do-billing-info-surgery-date").html();
                        /* To get the date format in DO ID based on the loggined company while selecting the account
                        */
                        fnGetRuleValueByCompany ("DO_APP_ORDERID","DO_ORDER",localStorage.getItem("cmpid"),function(data){
                        var oldDoID = $("#div-do-billing-doid").text();
                        var ORDERDTFMT = data[0].RULEVALUE;  
                            if (ORDERDTFMT == '' || ORDERDTFMT == null || ORDERDTFMT == undefined ){
                                    ORDERDTFMT  = "yyMMdd"; 
                            }
                            /*Delete the Old DO from the TBL_DO if the surgery date was changed*/
                               fnDeleteDO(oldDoID);
                        var orderiddate = formattedDate(surgerydate,ORDERDTFMT);
                        $("#span-do-billing-doid-date").html(orderiddate);
                            
                        that.generateDO(rowValue, "Account");
                        });
                    }
                    else{
                        showNativeAlert(lblNm("msg_surgery_date_account"),lblNm("msg_surgery_date_required"));
                    } 
			}
			else {
				showNativeAlert(lblNm("msg_sync_price"), lblNm("msg_account_price_required"));
			}
		},

				
		getDistributor: function(repid,key) {
			var that = this;
			var salesrepDID;
			fnGetRepInfo(repid,function(data){
				that.parent.repName = data.C703_SALES_REP_NAME;
				salesrepDID = data.C701_DISTRIBUTOR_ID;
				console.log(key);
				if(key==undefined) {
					if(parseInt(localStorage.getItem("AcID"))<=2) {
						if ((salesrepDID == that.parent.userDistributorid) || (repid == that.parent.userMappedRep)) {
							that.parent.coveringRepMode = false;
						}
						else {
							that.parent.coveringRepMode = true;
						}
					}
					else {
						var arrFS = localStorage.getItem('FieldSalesList');
						arrFS = $.parseJSON(arrFS);
						console.log(_.contains(arrFS,salesrepDID));
						if(_.contains(arrFS,salesrepDID))
							that.parent.coveringRepMode = false;
						else
							that.parent.coveringRepMode = true;
					}
				}
			});
            
            fnGetDORepContactInfo(repid, function(status, data) {
                if(status) {
                    that.parent.otherInfo.repContact = {};
                    that.parent.otherInfo.repContact.email = data.email;
                    that.parent.otherInfo.repContact.phone = data.phone;
                    that.parent.saveDO();
                }
            });
		},
        
		getSurgeryDate: function() {            
            var selectedDate = $("#div-do-billing-info-surgery-date input").val();
            var that = this;
//                $("#label-do-billing-info-surgery-date").html(formattedDate(selectedDate,localStorage.getItem("cmpdfmt")));	
                $("#label-do-billing-info-surgery-date").html($("#div-do-billing-info-surgery-date input").val());	
                $("#span-do-billing-info-surgery-date").html(formattedDate(selectedDate,localStorage.getItem("cmpdfmt")));	
                $("#div-do-billing-info-surgery-date").find('input').val(selectedDate);
            
			this.caseinfoid = "";
			this.$("#div-do-billing-info-surgeon-name").find("input").val("");
			this.getCaseID();
            
            var repID = $("#div-do-billing-acname").attr("repid");
            if(repID != undefined) {
                var accID = $("#div-do-billing-acid").text();
                var accNm = $("#div-do-billing-acname").text();
                var repID = $("#div-do-billing-acname").attr("repid");
                var repNM = $("#span-do-billing-doid-repid").attr("name");
                var dealID =$("#div-do-billing-acname").attr("dealid");
                var divcompanyid =$("#div-do-billing-acname").attr("divcompanyid"); 
                //var oldorderid = $("#div-do-billing-doid").text();
                this.oldorderid = $("#div-do-billing-doid").text();
                 console.log(this.oldorderid);
                var oldorderid = localStorage.getItem("OLDDOID");
                localStorage.setItem("OLDDOID", oldorderid +","+this.oldorderid);
                
                var rowEl = $("<a id="+accID+" class=item-searchlist name="+accNm+" productclass='' productmaterial='' productfamily='' repid="+repID+" repname="+repNM+" synced='Y' state='' dealid='"+dealID+"' divcompanyid="+divcompanyid+" ><div class='div-data hide'></div></a>");
                var CurrentRow = {
                    "ID" : accID,
                    "Name" : accNm,
                    "Element" : rowEl
                };
                console.log('CurrentRow');
                console.log(CurrentRow);
                this.getAccountFromSearchView(CurrentRow); 
              
            }
		},
		showSalesReps: function() {
			var that = this;
			this.$("#div-do-billing-salesrep-search").show();
			$("#div-do-billing-salesrep-list").css("display","inline-block");
            this.oldordIdRepSearch = $("#div-do-billing-doid").text();
			var renderOptions = {
					"el":this.$('#div-do-billing-salesrep-list'),
					"ID" : "txt-billing-salesrep",
            		"Name" : "txt-search-name",
            		"Class" : "txt-keyword-salesrep",
            		"minChar" : "3",
            		"autoSearch": true,
            		"placeholder": lblNm("msg_search_c"),
            		"fnName": fnGetSalesRep,
            		"usage": 1,
            		"template" : 'GmSearchResult',
					"template_URL" : URL_Common_Template,
					"callback": function(rowValue){that.getSalesRepFromSearchView(rowValue)}
            		
            	};

			var searchview = new SearchView(renderOptions);
						
		},
		
		getSalesRepFromSearchView: function(rowValue) {
			var that = this;
			$("#div-do-billing-salesrep-search").hide();
			$("#txt-billing-salesrep").val('');
			$("#span-do-billing-doid-repid").html(that.fnFormatOrderID(rowValue.ID));
			var surgerydate = $("#span-do-billing-doid-date").text();
			this.generateDO(rowValue, "Salesrep");
			
		},

		generateDO: function(rowValue, selectType) {
			var that = this;
			var surgerydate = $("#span-do-billing-doid-date").text();
			var dorepid = $("#span-do-billing-doid-repid").text();
			var DOFormat = dorepid + "-" + surgerydate + "-";
			var serverseq, localseq, seq;
			var sdate = new Date();
			var smm = ("0" + (sdate.getMonth() + 1)).slice(-2), sdd = ("0" + sdate.getDate()).slice(-2), syy = sdate.getFullYear();  
			var orderdate;
			if(selectType == "Account") 
				orderdate = smm+"/"+sdd+"/"+syy;
			else
				orderdate = this.parent.JSONData.orderdate;
            var ordIdReadText;

			fnCheckDOID(DOFormat, function(DOs) {
				var currSeq = (DOs.length)?(_.max(_.pluck(DOs,'DOID'), function(id){ return parseInt(id.split('-')[2]); })):"0";
				console.log(currSeq);
				localseq = parseInt(currSeq.replace(DOFormat,"")) + 1;

				if((navigator.onLine) && (!intOfflineMode)) {
					that.loaderview = new LoaderView({text : lblNm("msg_generating_doid")});
					var prefixordid = dorepid+ "-" + surgerydate;
					var input = {
							"token": localStorage.getItem("token"),
							"orderdate": orderdate,		
							"prefixordid": prefixordid
					};

					fnGetWebServiceData("DeliveredOrder/getlatestdo", undefined, input, function(status,data) {
						if(status){
							if(data.orderid!="") {
								var orderid = data.orderid;
								serverseq = orderid.split("-")[2];
								serverseq = parseInt(serverseq) + 1;
							}
							
							else {
								serverseq = 1;
							}

							if(serverseq == localseq)
								seq = serverseq;
							else if(serverseq > localseq)
								seq = serverseq;
							else
								seq = localseq;

							that.$("#span-do-billing-doid-count").html(seq);
							that.loaderview.close();

							if (selectType == "Account") {
								that.$("#div-do-billing-doid").removeClass("do-hide");

								fnGetGPOFor(rowValue.ID,function (PartyID) {
									if(PartyID=="2666") 
										that.parent.otherInfo.HCAAccount = true;
								});
								that.getDistributor($(rowValue.Element).attr("repid"));
								that.caseinfoid = "";
								that.$("#div-do-billing-info-surgeon-name").find("input").val("");
								that.getCaseID();
								that.updateJSON();
							}

							else {
								fnDeleteDO(that.parent.JSONData.orderid)
								that.parent.JSONData.orderid = that.$("#div-do-billing-doid").text();
								that.parent.saveDO();
							}
                            //This is related to Read Tag from Image Changes and this code is to Update the Order id to the Tag Image related tables
                            if (that.oldorderid != undefined || that.oldordIdRepSearch != undefined) {
                                if(that.oldorderid != undefined){
                                     ordIdReadText = that.oldorderid;
                                }else{
                                     ordIdReadText = that.oldordIdRepSearch;
                                    
                                }                               
                                fnUpdDOTagImgDtls(ordIdReadText, $("#div-do-billing-doid").text());
                                fnUpdDOTagDetails(ordIdReadText, $("#div-do-billing-doid").text());
                            }
						}
						else{
							that.loaderview.close();
							showNativeAlert(lblNm("msg_server_down"),lblNm("msg_error_message"));
							that.generateDOOfflineMode(rowValue, localseq, selectType);
						}
					});

				}
				else{
					that.generateDOOfflineMode(rowValue, localseq, selectType);
				}

			});
			
		},

		generateDOOfflineMode: function(rowValue, seq, selectType) {
			var that = this;
            var ordIdReadText;
			fnShowStatus(lblNm("msg_temporary_doid"));
			$("#span-do-billing-doid-count").html(seq);
			if(selectType == "Account") {
				this.$("#div-do-billing-doid").removeClass("do-hide");
				fnGetGPOFor(rowValue.ID,function (PartyID) {
					if(PartyID=="2666") 
						that.parent.otherInfo.HCAAccount = true;
				})
				 
				this.getDistributor($(rowValue.Element).attr("repid"));
				this.caseinfoid = "";
				this.$("#div-do-billing-info-surgeon-name").find("input").val("");
				this.getCaseID();
				this.updateJSON();
                
                //This is related to Read Tag from Image Changes and this code is to Update the Order id to the Tag Image related tables
                if (that.oldorderid != undefined || that.oldordIdRepSearch != undefined) {
                    if (that.oldorderid != undefined) {
                        ordIdReadText = that.oldorderid;
                    } else {
                        ordIdReadText = that.oldordIdRepSearch;

                    }
                    fnUpdDOTagImgDtls(ordIdReadText, $("#div-do-billing-doid").text());
                    fnUpdDOTagDetails(ordIdReadText, $("#div-do-billing-doid").text());
                }
			}
			else {
				fnDeleteDO(that.parent.JSONData.orderid);
				this.parent.JSONData.orderid = that.$("#div-do-billing-doid").text();
				this.parent.saveDO();
                
                //This is related to Read Tag from Image Changes and this code is to Update the Order id to the Tag Image related tables
                if (that.oldorderid != undefined || that.oldordIdRepSearch != undefined) {
                    if (that.oldorderid != undefined) {
                        ordIdReadText = that.oldorderid;
                    } else {
                        ordIdReadText = that.oldordIdRepSearch;

                    }
                    fnUpdDOTagImgDtls(ordIdReadText, $("#div-do-billing-doid").text());
                    fnUpdDOTagDetails(ordIdReadText, $("#div-do-billing-doid").text());
                }
			}
					
		},
		
		getCaseID: function(signAdjust) {
			var that = this;
			var defaultid;

			if( this.caseinfoid !=""){
				defaultid = this.caseinfoid;
			}
			
			else {
				defaultid = 0;
			}

			if(this.$("#span-do-billing-doid-repid").text() != "" && this.$("#div-do-billing-info-surgery-date").find('input').val() !="") {
				var sdate = this.$("#div-do-billing-info-surgery-date").find('input').val();
				var repid = this.$("#span-do-billing-doid-repid").text();
				var acctid = this.$("#div-do-billing-acid").text();
				fnGetCaseID(acctid, repid, sdate, function(data){
					if(data !="") {
						data.push({"ID" : 0, "Name" : "Choose One"});
						if(that.caseDrpDown)
							that.caseDrpDown.close();
						that.caseDrpDown = new DropDownView({
							el : that.$("#div-do-billing-info-caseid-list"),
							data : data,
							DefaultID : defaultid
						});
						
						that.$("#div-do-billing-info-caseid-list").bind('onchange', function (e, selectedId,SelectedName) {
							
							if(selectedId == 0)  {
								that.caseinfoid = "";
								that.$("#div-do-billing-info-surgeon-name").find("input").val("");
								that.updateJSON();
							}
							else {
								that.caseinfoid = selectedId;
								fnGetSurgeon(that.caseinfoid, function(surgeonname){
									that.$("#div-do-billing-info-surgeon-name").find("input").val(surgeonname[0].SNAME);
									that.updateJSON();
								});
							}
						});
					}

					else {
						that.$("#div-do-billing-info-caseid-list").empty();
						that.updateJSON(signAdjust);
					}
					
				});
			}
		},
		
		closeSalesrepSearch: function () {
			this.$("#div-do-billing-salesrep-search").hide();
		},

		updateJSON: function(signAdjust) {
			if(this.$("#div-do-billing-acid").text().length != "0") {
				if(this.parent.signatureMode == true && signAdjust==undefined) {
					this.parent.signatureMode = false;
					this.parent.signaturesrc = "";
					this.parent.signaturesubmit = false;
					$(".img-do-sign").each(function() {
						$(this).attr("src","");
					});
				}
				var prevAcctID = this.parent.JSONData.acctid;
				this.parent.JSONData.acctid = this.$("#div-do-billing-acid").text();
				this.parent.JSONData.repid = this.$("#div-do-billing-acname").attr("repid");
				this.parent.JSONData.orderid = this.$("#div-do-billing-doid").text();
				this.parent.JSONData.custpo = this.$("#div-do-billing-info-parent-customer").find("#div-do-billing-info-customer-input").val();
				this.parent.JSONData.parentorderid = this.$("#div-do-billing-info-parent-customer").find("#div-do-billing-info-parent-input").val();
				this.parent.JSONData.ordertype=this.ordertype;
				this.parent.JSONData.dealid=this.dealid;
                this.parent.JSONData.divcompanyid=this.$("#div-do-billing-acname").attr("divcompanyid");
                this.parent.JSONData.accDivCompanyFl=this.accDivCompanyFl;
                this.parent.JSONData.accEPGSFl=this.accEPGSFl;

                
              //  var surgeryDate = $("#div-do-billing-info-surgery-date").find('input').val();
				var AttrJSON = new Array();
                //getting surgeon name and adding in PDF
				var	surgeonName = "";	
                var lastchar = "";
               $(".do-npi-list-details").each(function() {
                        surgeonName = surgeonName + $(this).find("#span-npi-txt").text().trim() +",";
                        surgeonName = surgeonName.replace(/[0-9]/g, ''); //Removing Numbers from the surgeon name
                        surgeonName = surgeonName.replace(/-/g, ''); //Removing hyphens from the surgeon name
				});
                 lastchar    = surgeonName[surgeonName.length -1];
                if(lastchar == ","){
                    surgeonName = surgeonName.replace(/.$/,'')
                }
                surgeonName = surgeonName.replace(/[,]+/g,','); //Replace commas if one or more matches available

                
				AttrJSON.push({"orderid" : this.parent.JSONData.orderid, "attrtype" : "400146", "attrvalue" : surgeonName});  //Surgeon Name
				AttrJSON.push({"orderid" : this.parent.JSONData.orderid, "attrtype" : "103601", "attrvalue" :$("#div-do-billing-info-surgery-date").find('input').val()});	//Surgery Date
				AttrJSON.push({"orderid" : this.parent.JSONData.orderid, "attrtype" : "103602", "attrvalue" : this.surgerytypeid});	//Surgery Type
				AttrJSON.push({"orderid" : this.parent.JSONData.orderid, "attrtype" : "103603", "attrvalue" : this.surgerylevelid});	//Surgery Level
				AttrJSON.push({"orderid" : this.parent.JSONData.orderid, "attrtype" : "103560", "attrvalue" : "103521"});	//HardCoded value to mention DO creation through GLobus Mobile App
        AttrJSON.push({"orderid" : this.parent.JSONData.orderid, "attrtype" : "26241315", "attrvalue" : this.epgsid}); // EGPS Info
                              
                if(this.ordertype != ""){
				AttrJSON.push({"orderid" : this.parent.JSONData.orderid, "attrtype" : "107802", "attrvalue" : this.ordertype});
               	}
				if(this.parent.JSONData.parentorderid!="") {
					AttrJSON.push({"orderid" : this.parent.JSONData.orderid, "attrtype" : "4000536", "attrvalue" : "4000537" });
					this.caseinfoid = ""
					this.parent.JSONData.caseinfoid = this.caseinfoid;
				}	
				else
					this.parent.JSONData.caseinfoid = this.caseinfoid;
	
				this.parent.JSONData.arrGmOrderAttrVO = AttrJSON;

				if(prevAcctID!=this.parent.JSONData.acctid) {
					this.parent.JSONParts = [];
					$("#div-do-parts-list").empty();
					$("#ul-do-parts-header-title").hide();
					$("#div-do-parts-footer-title").hide();
					
					$("#div-do-shiptoinfo-parts-list").empty(); 
					$("#div-do-tagid-parts-list").empty();
					this.parent.partsChangeMode = false;
					this.parent.JSONData.arrGmOrderItemVO = new Array();
					this.parent.JSONData.arrGmOrderItemAttrVO = new Array();
					this.parent.JSONData.gmTxnShipInfoVO = new Array();
					this.parent.JSONData.arrGmTagUsageVO = new Array();
					if(this.parent.doshiptoinfo) {
						this.parent.doshiptoinfo.close();
						this.parent.doshiptoinfo = null;
					}
					if(this.parent.dotagidview) {
						this.parent.dotagidview.close();
						this.parent.dotagidview = null;
					}
				}
	
				if((this.parent.JSONData.orderid).replace("--","") != "")
					this.parent.saveDO();
                        if(this.oldorderid != undefined ){
				                 var Neworderid = $("#div-do-billing-doid").text();
                                  var oldorderid = localStorage.getItem("OLDDOID");
				                  fnUpdateOldDoID(Neworderid,oldorderid); 
				                  }
			}
		},
		
		getPicture: function() {
			var that = this;

			navigator.camera.getPicture(function (imageURI) {
				var cropview = new CropView({src: "data:image/jpeg;base64,"+ imageURI, callback: function(src) {
					$("#div-do-billing-info-additional").css("background","url('" + src + "') no-repeat");
					that.updateJSON();
				}});
			}, function(){
					console.log("failure");
				}, { destinationType: Camera.DestinationType.DATA_URL, correctOrientation: true });
		},
        
        fnFormatOrderID: function (repid) {
            if (repid.length == "1") { 
                repid = "00"+repid;
            }
            else if (repid.length == "2") { 
                repid = "0"+repid;
            }
            return repid
        },
     //to get the order type list  
        getOrderType: function(){
			

            var that = this;
            if(this.ordertype ==""){
            	
             // default order type from Rule
            	fnGetRuleValueByCompany ("DEFAULT_ORD_TYP","DO_ORDER",localStorage.getItem("cmpid"),function(data){
                if(data.length>0){
                     that.ordertype=data[0].RULEVALUE;
                }else{
                     that.ordertype="2530"; //order type bill only loaner
                }
            });
            }
         
			fnGetRuleByGRP("IPADORDTYP",function(data){
			
				that.dropdownview = new DropDownView({
						el : that.$("#div-do-billing-info-ordertype-list"),
						data : data,
						DefaultID : that.ordertype
                    
					});
				
					that.$("#div-do-billing-info-ordertype-list").bind('onchange', function (e, selectedId,SelectedName) {
						that.ordertype = selectedId;
						that.updateJSON();
					});
			});

        }
		

	});	

	return DOBillingInfoView;
});