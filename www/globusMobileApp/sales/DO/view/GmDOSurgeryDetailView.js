/**********************************************************************************
 * File:        GmDOSurgeryDetailView.js
 * Description: 
 * Version:     1.0
 * Author:     	karthik
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification', 'searchview', 'dropdownview', 'loaderview', 'imagecropview', 'daodo'
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification, SearchView, DropDownView, LoaderView, CropView, DAODO) {

	'use strict';
	var GmDOSurgeryDetailView = Backbone.View.extend({

		events : {
		    "click #div-save-rep-info" : 'saveRep',
            "change input" : "updateJSON",
           "click .fa-calendar-op,.div-do-surgery-detail-tab #ul-do-surg-calender input" : "showDatepicker",
            "change .li-do-surgery-date-details":"getDate" 
		},

		/* Load the templates */ 
		template: fnGetTemplate(URL_DO_Template, "GmDOSurgeryDetail"),
         
         initialize:function (options) {
             this.parent = options.parent;
             this.render(options);
             this.rptval ="";
             this.checkfirst ="";
             
        },
        
        render: function(options){
            
            var viewJSON  = {};
			viewJSON = $.parseJSON(JSON.stringify(this.parent.JSONData));
            var that=this;
              _.each(that.parent.JSONData.arrDoSurgeryDetailVO,function(data){
                  _.each(GM.Global.doSurgeryInfo,function(sdata){
                      if( data.attrtype == sdata.attrtyp)
                          sdata.attrval = data.attrvalue;
                })    
              });
            
			that.$el.html(that.template(GM.Global.doSurgeryInfo));
            
        },
        
        updateJSON: function(e) {
            console.log(this.parent)
			if($("#div-do-billing-acid").text().length != "0") {
               this.parent.JSONData.arrDoSurgeryDetailVO =  _.filter(this.parent.JSONData.arrDoSurgeryDetailVO,function(data){
                    return data.attrtype != $(e.currentTarget).attr("id");
                });
            this.parent.JSONData.arrDoSurgeryDetailVO.push({"orderid" : this.parent.JSONData.orderid, "attrtype" :$(e.currentTarget).attr("id"), "attrvalue":$(e.currentTarget).val()});
                
           if((this.parent.JSONData.orderid).replace("--","") != "")
                this.parent.saveDO();
			}
		},
        
      showDatepicker: function (event) {
			var elem = event.currentTarget;
			$($(elem).parent().find("input")).datepicker( { maxDate: 0, duration: 'fast'});
			var visible = $('#ui-datepicker-div').is(':visible');
			$($(elem).parent().find("input")).datepicker(visible? 'hide' : 'show');	
            
			event.stopPropagation();
		},
        
        getDate: function (event) {
	       var that=this;
            var sDate = $(event.currentTarget).val()
           if(sDate !=""){ 
                that.$(event.currentTarget).val(formattedDate(sDate,localStorage.getItem("cmpdfmt")));
			   
        }
		}	
	});	

	return GmDOSurgeryDetailView;
});