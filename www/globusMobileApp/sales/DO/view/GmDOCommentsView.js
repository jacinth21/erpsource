/**********************************************************************************
 * File:        GmDOCommentsView.js
 * Description: Used for DO Comments / Sub View of GmDODetailView.js
 * Version:     1.0
 * Author:     	praja
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification'
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification) {

	'use strict';
	var DOCommentsView = Backbone.View.extend({

		events : {
			"keyup #txtarea-do-comments" : "updateJSON"
			
		},

		/* Load the templates */ 
		template: fnGetTemplate(URL_DO_Template, "GmDOComments"),

		initialize: function(options) {
			this.parent = options.parent;
			this.surgerylevelid = "";
			this.surgerytypeid = "";
			this.render();
		},

		// Render the initial contents when the Catalog View is instantiated
		render: function () {
			this.$el.html(this.template({comments: this.parent.JSONData.ordercomments}));
			var maxlength = 500;
			var text = $("#txtarea-do-comments").val();

			this.$("#spn-do-comments-limit").text(maxlength - text.length);
			return this;
		},

		updateJSON: function(event) {
			var maxlength = 500;
			var text = event.currentTarget.value;

			if (text.length >= maxlength) {
		        event.currentTarget.value = text.substring(0, maxlength);
		        text = event.currentTarget.value;
		    }

			this.$("#spn-do-comments-limit").text(maxlength - text.length);
			this.parent.JSONData.ordercomments = text;
			this.parent.saveDO();
		},

		close: function() {
			this.unbind();
			this.undelegateEvents();
		}
		

	});	

	return DOCommentsView;
});