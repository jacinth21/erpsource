/**********************************************************************************
 * File:        GmDODOUDIView.js
 * Description: UDI module
 * Version:     1.0
 * Author:     	Matthew/Jreddy 
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification', 'searchview','dropdownview','loaderview', 'daodo'
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification, SearchView,DropDownView,LoaderView, DAODO){

	'use strict';
	var tabid;
	var DOUDIView = Backbone.View.extend({

		events : {
            "click .fa-minus-circle": "clearRow",
            "click #div-do-udi-info-surgery-date label, #div-do-udi-info-surgery-date i" : "showDatepicker",
            "change #div-do-udi-info-surgery-date input" : "getSurgeryDate",
			"click #btn-udi-genpdf" : "generatePDF",
			"click #btn-udi-emailpdf" : "emailUDIDetails",
            "change .li-udi-part-lotno, .li-udi-part-donorno, .li-udi-part-expdate" : "setUDI"
		}, 

		/* Load the templates */ 
		template: fnGetTemplate(URL_DO_Template, "GmDOUDI"),
		template_numberkeypad: fnGetTemplate(URL_DO_Template, "GmDOPartsNumberKeypad"),
        template_PDF: fnGetTemplate(URL_DO_Template, "GmDOUDIPDFLayout"),
        template_part_item: fnGetTemplate(URL_DO_Template, "GmDOUDIParts"),
        template_part_item_mobile: fnGetTemplate(URL_DO_Template, "GmDOUDIPartsMobile"),
		

		initialize: function(options) {
			$("body").removeClass();
			$("#btn-home").show();
            this.surgerylevelid = "";
            this.surgerytypeid = "";
			this.options =options;
            this.parent = options.parent;
            this.JSONData = {};
            this.isPDFCreatd = false;
            this.isValidUdi = true;
            this.UdiNo ={}; 
            this.udiPattern = {};
         
				//Initializing DO JSON
				this.JSONParts = [];

                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1;
                var yyyy = today.getFullYear();
                if (dd < 10)
                    dd = '0' + dd
                if (mm < 10)
                    mm = '0' + mm
                var surgerydt = mm + '/' + dd + '/' + yyyy;
                this.JSONData.surgerydate = surgerydt;
				this.render(options);

		},
		
       
       // Render the initial contents when the Catalog View is instantiated   
		render: function (options) {
			var that = this;
//			showMsgView("011");
            
            //rep details
            that.JSONData.repId = localStorage.getItem('partyID');
            that.JSONData.lastName = localStorage.getItem("lName");
            that.JSONData.firstName = localStorage.getItem("fName");
            that.JSONData.email = localStorage.getItem("emailid");
            that.JSONData.phone = "";
            
            //check if DI_Number is updated
            fnChk_DI_Updated(function(isTrue){
                if(isTrue == false){
                    showNativeAlert(lblNm("msg_generating_udi"),lblNm("msg_data_sync"));
                    window.location.href= "#do";
                }
 
            })
            
            // code to get localfile URL for pdfpath
            fnGetLocalFileSystemPath(function(URL){
                 that.JSONData.fileSystemPath = URL;
            });           
            
			this.$el.html(this.template(this.JSONData));
            
            			var renderOptions = {
							"el":this.$('#div-do-udi-parts-used-txtkeyword'),
							"ID" : "txt-keyword",
							"Name" : "txt-search-name",
							"Class" : "txt-keyword-initial",
							"minChar" : "3",
							"autoSearch": true,
							"placeholder": lblNm("msg_character_search"),
							"fnName": fnGetUDIParts,
							"usage": 2,
							"template" : 'GmSearchResult',
							"template_URL" : URL_Common_Template,
							"callback": function(pList) {
                                that.getItemsFromSearchView(pList);
                            }
							};

			var searchview = new SearchView(renderOptions);
            this.getSurgeryLevel();
			this.getSurgeryType();

			return this;
		},
        
        getItemsFromSearchView: function(data){
            var that = this;

            if(data.length >= 1){
               _.each(data, function(data) {
                   var partName = data.Name.replace(data.ID,"");
            fnGetUDINumber(data.ID, function (diid){
                that.UdiNo = "(UDI Pattern: "+diid[0].pattern +")";
                that.udiPattern = diid[0].pattern;
                if($(window).width() < 480){
                    that.$("#div-do-udi-part-items").append(that.template_part_item_mobile({ID: data.ID, Name: partName, diid: diid[0].diid, UDIpattern:that.UdiNo, UDItemplate:diid[0].pattern, index: $(".ul-udi-parts").length}));
                    $(".togglebx").unbind('click').bind('click', function () {
                        $(this).parent(".udiPartsMobile").toggleClass('showUdiLst');
                        $(this).children("img").toggleClass("onActive");
                    });
                    $(".udiPartsMobile").each(function(){
                       if ($(this).next().is('ul')) {
                            $(this).addClass("showUdiLst");
                            $(this).find(".togglebx").children("img").removeClass("onActive");
                        } else {
                            $(this).removeClass("showUdiLst");
                            $(this).find(".togglebx").children("img").addClass("onActive");
                       }
                   });
                }
                else{
                    that.$("#div-do-udi-part-items").append(that.template_part_item({ID: data.ID, Name: partName, diid: diid[0].diid, UDIpattern:that.UdiNo, UDItemplate:diid[0].pattern, index: $(".ul-udi-parts").length}));
                }
                $("#div-do-udi-parts-used-txtkeyword input").val(""); 
            }); 
            });
            }
                else{
                var partName = data.Name.replace(data.ID,"");
            fnGetUDINumber(data.ID, function (diid){
                that.udiPattern = diid[0].pattern;
                that.UdiNo = "(UDI Pattern: "+diid[0].pattern+")";
                if($(window).width() < 480){
                    that.$("#div-do-udi-part-items").append(that.template_part_item_mobile({ID: data.ID, Name: partName, diid: diid[0].diid, UDIpattern:that.UdiNo, UDItemplate:diid[0].pattern, index: $(".ul-udi-parts").length}));
                    $(".togglebx").unbind('click').bind('click', function () {
                        $(this).parent(".udiPartsMobile").toggleClass('showUdiLst');
                        $(this).children("img").toggleClass("onActive");
                    });
                    $(".udiPartsMobile").each(function(){
                       if ($(this).next().is('ul')) {
                            $(this).addClass("showUdiLst");
                            $(this).find(".togglebx").children("img").removeClass("onActive");
                        } else {
                            $(this).removeClass("showUdiLst");
                            $(this).find(".togglebx").children("img").addClass("onActive");
                       }
                   });
                }
                else{
                    that.$("#div-do-udi-part-items").append(that.template_part_item({ID: data.ID, Name: partName, diid: diid[0].diid, UDIpattern:that.UdiNo, UDItemplate:diid[0].pattern, index: $(".ul-udi-parts").length}));
                }
                 $("#div-do-udi-parts-used-txtkeyword input").val("");
            }); 
            }
        },
        
        //Removes the Part Information if clicked on Minus icon
		clearRow: function(e){
			var that = this;
			var partid = $(e.currentTarget).parent().parent();
            console.log(partid)
            $(e.currentTarget).parent().parent().remove();
        },
        
        //Get surgerydate 
        getSurgeryDate: function() {
			$("#label-do-udi-info-surgery-date").html($("#div-do-udi-info-surgery-date input").val());
		},
        
        showDatepicker: function (event) {
			$("#div-do-udi-info-surgery-date input").datepicker( {maxDate: 0,  duration: 'fast'});
			var visible = $('#ui-datepicker-div').is(':visible');
			   console.log(visible);
			$("#div-do-udi-info-surgery-date input").datepicker(visible? 'hide' : 'show');	
			
			if( $("#span-do-billing-doid-repid").text() == ""){
				this.$("#div-do-billing-doid").hide();
			}
			
			else {
				this.$("#div-do-billing-doid").show();
			}
			
			event.stopPropagation();
			$(window).click(function (event) {
			    if(document.getElementById("ui-datepicker-div").style.display != "none") {
			    	var elem = event.target;
			       if(!($(elem).hasClass("ui-datepicker-prev") || $(elem).hasClass("ui-datepicker-next") || $(elem).parent().hasClass("ui-datepicker-prev") || $(elem).parent().hasClass("ui-datepicker-next"))) {
			    	   var visible = $('#ui-datepicker-div').is(':visible');
			    	   $("#div-do-udi-info-surgery-date input").datepicker(visible? 'hide' : 'show');
//			    	   $('#ui-datepicker-div').hide();
			       }
			       }
			   });
		},
        
        
        getSurgeryLevel: function() {
			var that = this;
			var defaultid;
			if( this.surgerylevelid !=""){
				defaultid = this.surgerylevelid;
			}
			
			else {
				defaultid = 0;
			}
			fnGetSurgeryLevel("TRLVL",function(data){
				data.push({"ID" : 0, "Name" : "Choose One"});
				that.dropdownview = new DropDownView({
						el : that.$("#div-do-udi-info-surgerylevel-list"),
						data : data,
						DefaultID : defaultid
					});
					
					that.$("#div-do-udi-info-surgerylevel-list").bind('onchange', function (e, selectedId,SelectedName) {
						that.surgerylevelid = selectedId  ;
						//that.updateJSON();
					});
			});
		},
		
		getSurgeryType: function() {
			var that = this;
			var defaultid;
			if( this.surgerytypeid !=""){
				defaultid = this.surgerytypeid;
			}
			
			else {
				defaultid = 0;
			}
			fnGetSurgeryLevel("SURTYP",function(data){
				data.push({"ID" : 0, "Name" : "Choose One"});
				that.dropdownview = new DropDownView({
					el : that.$("#div-do-udi-info-surgerytype-list"),
					data : data,
					DefaultID : defaultid
				});
				
				that.$("#div-do-udi-info-surgerytype-list").bind('onchange', function (e, selectedId,SelectedName) {
					that.surgerytypeid = selectedId ;
					//that.updateJSON();
				});
			});
		},

		validate: function() {
		  	var rs = true;
            if($("#label-do-udi-info-surgery-date").html().trim() == ""){
                fnShowStatus(lblNm("msg_surgery_date"));
				rs = false;
            }
            else if($("#div-do-udi-surgname").val().trim() ==""){
                fnShowStatus(lblNm("msg_surgeon_name"));
                document.getElementById("div-do-udi-surgname").focus();
				rs = false;
            }
            else if($("#div-do-udi-info-surgerylevel-list .div-dropdown-text").html().trim() =="" || $("#div-do-udi-info-surgerylevel-list .div-dropdown-text").html().trim() =="Choose One"){
                fnShowStatus(lblNm("msg_surgery_level"));
				rs = false;
            }
             else if($("#div-do-udi-info-surgerytype-list .div-dropdown-text").html().trim() =="" || $("#div-do-udi-info-surgerytype-list .div-dropdown-text").html().trim() =="Choose One"){
                fnShowStatus(lblNm("msg_surgery_type"));
				rs = false;
            }
            else if($(".ul-udi-parts").length < 1){
                fnShowStatus(lblNm("msg_parts_details"));
				rs = false;
            }
            else if(this.isValidUdi == false){
                rs = false;
                this.isValidUdi = true;
            }
            return rs;
		},
        
   
 
//Setting part details to JSON object
 updateUDIparts:function() {
      var Id,Description,DINumber,UDINumber,indexValue;
     var attrJSON = new Array();
                $("#div-do-udi-part-items").find(".ul-udi-parts").each(function(index) {
                    indexValue = index;
                    Id = $(this).find(".li-udi-parts-used-icon").text();
                    Description = $(this).find("#div-do-udi-desc").text().trim();
                    DINumber = $(this).find(".li-do-parts-dino").text().trim();
                    UDINumber = $(this).find(".li-do-parts-udino").text().trim();
                    attrJSON.push({"Id" : Id,"Description" : Description, "DINumber": DINumber, "UDINumber":UDINumber});

                });
     return attrJSON;
 },

// Generate PDF details
	generatePDF: function(callback) {
        
         var that=this;
        //checking validate - matt
			if(this.validate()) {
                that.template_PDF = fnGetTemplate(URL_DO_Template, "GmDOUDIPDFLayout");
                var ItemsOnPage = 18;

				this.loaderview = new LoaderView({text : "Generating PDF..."});
                this.JSONParts = that.updateUDIparts(); //attrJSON;
				this.JSONDataPDF = {};
				var todaydate = new Date();  
				var dd = ("0" + todaydate.getDate()).slice(-2), mm = ("0" + (todaydate.getMonth() + 1)).slice(-2), yyyy = todaydate.getFullYear();
				this.JSONDataPDF.generateddate = mm+"/"+dd+"/"+yyyy;
                this.JSONDataPDF.SurgeonName = $("#div-do-billing-info-surgeon-name").find("input").val();
                this.JSONDataPDF.SurgeryLevel = $("#div-do-udi-info-surgerylevel-list").find(".div-dropdown-text").text();
				this.JSONDataPDF.SurgeryLevel = (that.JSONDataPDF.SurgeryLevel=="Choose One")?"":that.JSONDataPDF.SurgeryLevel;
				this.JSONDataPDF.SurgeryType = $("#div-do-udi-info-surgerytype-list").find(".div-dropdown-text").text();
				this.JSONDataPDF.SurgeryType = (that.JSONDataPDF.SurgeryType=="Choose One")?"":that.JSONDataPDF.SurgeryType;
				this.JSONDataPDF.SurgeryDate = $("#label-do-udi-info-surgery-date").text();
				this.JSONDataPDF.logo = "data:image/gif;base64,R0lGODlhigA8AOYAAAELQ9nu9c4WB+iloCmTzgBmmdxgVpy0xwZKiUlomXucuNXf5scYBtMnGQAycwFwtnGUtIHD4/j9/fLMxq3N3CpQgNc7MeSQiQUnW/Xe2gBmql2Gq2Gw2h5LgEqj1SSBugAVY/jv7qLP5xA2dtUzKCNwqe7BveB+dtdQRs8kF/bY1au7zRGDwiE7awBQlIOtywEaTVl0mSlZjaDe+AM4fHCHrM/W4H7M8zNmmd7n7kWWwfD19/f5++q0rx5Ui1mu2wAJWvbm4xtvqG6+6itik95uZNY4K2yixyOOxQEmauablFKNtjlPdQ1XlcPg66O50NEvIxN4usXR3SZCcc4gEYybtkt1npPX9JGsx8/m8LXF1ubv89leUtUvIrLX6tZEOQADVDp2pzyc1JHI5BOOy////wdDgBBNhgAhav339yFhlwBdnmB8pG212xFnpzOZzDSGuQB3vzRZjhdRhvz28/PQzh9ZkLfh9WqMsThXgAw0aoWjvuPn7RZEdgghVL3O2yH5BAQUAP8ALAAAAACKADwAAAf/gGWCg4SFhoeIiYqGaRkTPUonRVxfDQJUFkVKPXV0i5+goaKjpKMhdZCSXChfXxYkDQ1fmiYZIWm4pbq7vL2KdHUDJygWULENXV0ksJgnE56+0dLToxlKRRbHyUbc3Q1UXD1p1OTl5mXWKFApDSTd79xd4BOGO3wLUitHL1pYR1h/+PA4R7CgiSKwusBbaKSBkR6FeNjQQvEFBw4eOETgoEMHHDgbnmwpSDIanQEo2LljCK+BgSCF/hywwSOLBzEefrTh0GbMmDZtIugQgmfFgpJIR9FRYoSKQpYLG5wYN8gGnCg/RIiI0CZnzq4exnjZwcPJETclFNgYmbTtoTRK/0gIeAoVXooLVMuk2fOAjJieWSTsGPIjgtYITrJoHcnjSYkSGyBoGei28oQvcy3UXSjgBLQyISDEEUPAQxZCO7J42ZIlwhgnXgiw8FKGx5EwS9RsWEG5csk0F2JtXmhBABeYgkIoYEGGAIcxhndIkCBoR4AAVyJkCeCkDYExZZw0QRAmDBvevkkGMZBi5fBuFtrXGbRjD3PnF4fcCMBjYH9CWwTFwxgscJDFCyU00QQRfVRBXXrmqICCQ+/BI5cShDyhgV8/dKjfDDsIwgNZlGWRnVhtsIDER3CUYMYZZ0whBYTlqFBJhZx9kcEgfJTwQE5i/KDfFSCK6F8ZO8xww/8QIvDghQg/sKDBGo/RQEMfNYRIozQZpIQjPF0IcAEhWGzowZlCXnHFHQNNV8gOd9zww2llbDHGBy64UIIaDughhxZbmmQAFe592RQUJtAXRhxvICHGRTeoyWZtEQkSwBgBDCLBCxq4gAAOcyRRQZaB9nKBU8ukquqqrMqFAnJl2FACEkg098MYN0Sa6X+E9LYFW16w0AYFTdBwhgwytJBHDqXuUscXJLQi7bTUVtuKVHkd8MEHcdg6RAQ3zHAkIdNJcEembu7QxgdCuIGAAzhYgQEGgDZbCh0h0KHvvvz26y+/hChwVbdvdCjkabxqOp1qY00nggftutGEGj7IoAf/AFXYay8E28bBghggd3hHbf3xWmIW0kmQBRIaaFDCEjiYkUQHUwBQg8bN7uHGB1Gw8AYBb5w5hpZGlmz0iB6wwC5aZ9CQhB5TwJAxzqIEUYcKWGet9dZcY10HrGU8sYYGHyDBAgsE4OTByIYkLIgXcTwwpQtq2OE0Bi1MsQLVoqhQXKuAtyrAq4NI0cQaQuiARLcEpD1GYOQ+OAgPHDzwwBprIOCDD3pgAAMTTPzBtyg9CBf46YcmKsgWRLiwc9lkMPcGB6shMtAWb7Bw+RouvGgxDC2AztbooFyQAl2GhjnmIDXQ4IYQazxAKxIEkNGGF5JnwVOKbzihwwdu8I6A/5Ud5N2CHwr0RvwnJ7RnKDfF6Vj4HE0I4ULL0nuAxM8iiNhGHACMgwaOsAQ4jGd8NOhAApjgBwBM4SjrE8UJ2vE+bohpchCYGAIwt4Yo8OxsHLDUGzRguQcIAQ5LuI0acICDCriwgRiTXARBoQQoQOF98SEBPQSxgATYAQEIyJMQhNCylh1hICKIwhFysAQ3uCEMWMADHhKgBxjgDQYAYAKzZjgKE3whBTg0DqykIIMz+ACIJXCDCzi4Bjcc4QAzysELwgBEOyThjnoAXQxa0IIZcZEUITgBFSj0pc585g960IMa5qCG8eTpkQoizx52IIUO0EAGNdDCAtgigRX8Qf+GfxTFBFAwSEOlYCqD+EMLgNCHMBDhDGaIZSzrZiU0yEEBNYiBHwexhRXYIJS9MAEpKVghbNEnBgBoQQU60IHNwaiMDojmHf0QgxA9SCZ8AGY0JlCEWCCvLg3gwo4GoQUqWlEPfehDBRJQAXVWgI8HkNwCViAFUGqTF0G4QCWICU4SDKAQNohBFWFgxSn44aAwxIIEtjARKajvntKgQw+w4c26yAMFO+TlCtjAhClEjaAw8EMLEjATPtgTotQIQQ+GsY7jfZMb30CBOOqRAxtIQQo2yMFDUYqUDKzUAF/owjGQkSp5WOAEJggBT4mXBhWkwgCsIEEyYiEAAUCBCxf/6IEKbrFU4oUgA3UwQQ8GoAQlXEASRTjBAHowgSCctKtwjatc50rXutqVRjzIgV73ytfe5HWTi9gCXwc7vNrwYbB8JVqdAGuIexS2DIdFbA7eWhktEIEG0cysZmUAwQVUAAh4SAQfEqDZ0kaTDVpagAxMm1kaYGEQVkBDAh6aAzkAAbWCyAEOWBtNK2xxS2ZwwBnwsAcIGBcCe8DDCEAwNc+CNhFPAEESNrAHBRwXAgqwQhJGUK8F+AAEVjiAeA/whAPIAQx5yKYEYjvbQuRABmDAbRkOAIIO7AELCsjvHvbQhySENlBWksNja7MCLbDFuf89BH2TQIFDrAANI9hb/xm8C4TXFkIBYKgAs9YrW9raVr5YAEECFCsILUhhwL4J8Ba38IQWP+EPmuThZxNsiOgmoV6FeAKEJexd2eZXvwrwARogIAgOt5cQtb2tlg6AhjlsAA8biHKUJUPZtqi4xEkAQWaHLOPnIsLGOM7QjnnoAwdY6cyYTQJni8xeDys5t0TgbTQ3UKorI0kKf8iBFnyQBAV0mcY5lm6YB6HjCJMZCBDYgaJJhAf0jsTIbpZvbRZA6UrrlbRmqDMN5JDNQvBBDn3+cyIW3OAaj3nCPgDCHgyxBzA8sDbsBWVt4xsiHuwBBxIuhBUckOlAlfmSRAi2sIlgBjSsesIVAMEZhv8dbBnQ+cFosAMOmE0EIXOXhx1oMgu3HTMgLEsQMUCDGah9WSDUYCA7SAAI5jBtZgcXB6VawAZ8MId627vePmBDpxcQMzvcu95nsAKSIPDvf8vg2GXIQQLMWPA5WGGXUrACvRtuBQiW4Q+gavgc8NDpZpXr4/YEucgjJ/KTllwRJ0dEyu/KcoKEAGxloEMGPjOIEKigDjivwwSUWogg3BznKsgLIWTO857nHOc0L0QGMiB0Qny16Di7gJioEoQvMEB1g0hDEaiwCbNeYJyDyAA3yhoJFPyzEBOgwgkM0YMUFIGsTCGcIQZAgrs0XRBKYABEqAYcFJxARwMwAEL2nvWlrZd1AEkXhAoaUARCnGo+hJhAZwyhBCpkNANBN4TUJ6ACARTh7koQAOE1loYTGKAMPYBC400AhdHrpQgpYKsJJrBzpUMBBUsPKwoMAPUypH3thRgAFQbg8wwUoQhgAw4UTlDWEyw/6QMQk9V03nsIlf70ZQiCUtvu+tILwBt67zlmlvGNIlT/94aYgBHaswwB8J4QPWAAhgYR/bMPou3s/4broxEIADs=";
                this.JSONDataPDF.firstName = that.JSONData.firstName;
                this.JSONDataPDF.lastName = that.JSONData.lastName;
                this.JSONDataPDF.phone = that.JSONData.phone;
                this.JSONDataPDF.email = that.JSONData.email;
                this.JSONDataPDF.repId = that.JSONData.repId;

				var MainJSONDataPDF = that.preparePartsData(this.JSONDataPDF,this.JSONParts);
				
				that.$("#div-udi-sample-pdf-container").html(that.template_PDF(MainJSONDataPDF)).show();
				var outerHeight = that.$('#div-udi-parts').outerHeight();

				var divtop = that.$('#div-udi-parts').offset().top;
        		var pageNos = 1, splitID;
                if(this.JSONParts.length > ItemsOnPage)
                {
                        splitID = ItemsOnPage;
                        pageNos = 2;
                    }

				//if(outerHeight>250) {
                if(splitID != undefined){
					MainJSONDataPDF = that.preparePartsData(this.JSONDataPDF,this.JSONParts, splitID);
					MainJSONDataPDF[0].twoPages = "two";
					that.parent.MainJSONDataPDF = MainJSONDataPDF;
				} 
				that.$("#div-udi-sample-pdf-container").hide();
                
                var fpath = "~/Documents/Globus/DO/PDF/WP/";
				that.parent.MainJSONDataPDF = MainJSONDataPDF;
				boolUpdate = false;
                this.JSONDataPDF.pdfFileName = that.createFileName();
                
				createPDFPath(function() {
					window.html2pdf.create(that.template_PDF(MainJSONDataPDF),fpath+ that.JSONDataPDF.pdfFileName +".pdf", // on iOS,
		               function(status) {
			               that.udipath = "file://"+status.match(/\(([^)]+)\)/)[1];
                           fnShowStatus(lblNm("msg_udi_pdf_generated"));
                           that.isPDFCreatd = true; 
                           that.loaderview.close();
                           var DOCViewer = window.open(that.udipath, '_blank', 'location=no');

		               }, function(status) {
		               	   //that.loaderview.close();
		               	boolUpdate = true;
			            console.log("status",status);
		           });
				});
            }
		 },

         //Preparing MainJSONDataPDF for pdf creation
		 preparePartsData: function (JSONDataPDF, JSONParts,splitID) {
		 	var that = this
				
			var MainJSONDataPDF = new Array();
    		var AttrJSON = new Array();
			var AttrNextJSON = new Array();

			this.SubJSONParts = [];
			var currJSON = $.parseJSON(JSON.stringify(JSONDataPDF));
			var currParts = $.parseJSON(JSON.stringify(JSONParts));
            var Id,Description,DINumber,UDINumber,indexValue;

             
            for( var k=0; k < currParts.length ; k++) {
					Id = currParts[k].Id;
					Description = currParts[k].Description;
					DINumber = currParts[k].DINumber;
					UDINumber = currParts[k].UDINumber;
					indexValue = k + 1;
					if((splitID==undefined)||(k < splitID))
						AttrJSON.push({"Id" :Id, "Description" : Description, "DINumber" : DINumber, "UDINumber" : UDINumber,"indexValue" : indexValue});
					else 
						AttrNextJSON.push({"Id" :Id, "Description" : Description, "DINumber" : DINumber, "UDINumber" : UDINumber,"indexValue" : indexValue});
			}

			currJSON.JSONSubParts = $.parseJSON(JSON.stringify(AttrJSON));
			currJSON.JSONNextSubParts = {};
            currJSON.JSONNextSubParts = $.parseJSON(JSON.stringify(AttrNextJSON));
		
			MainJSONDataPDF.push(currJSON);

			return MainJSONDataPDF
		 },
        
        createFileName: function(){
            var that = this;                      
            var sdate = new Date(this.JSONDataPDF.SurgeryDate);
            var dd = ("0" + sdate.getDate()).slice(-2), mm = ("0" + (sdate.getMonth() + 1)).slice(-2), yyyy = sdate.getFullYear();

            var yy = yyyy.toString().substr(2,2)
            var fdate = yy+mm+dd
            console.log()
            var localPath=that.JSONData.fileSystemPath+"DO/PDF/WP/"; 
            var fname = "";
            var fileexist = true;
            var cnt = 1;
            do {
                fname = "UDI Details-"+fdate+"-"+cnt;
                var fullPath = localPath+fname+".pdf";
                fileexist = this.fnPdfExists(fullPath);
                if(fileexist == true)
                    cnt++;
            }
            while (fileexist != false);
            return fname;
        },
        
        //Check if specified filed exists in path
        fnPdfExists: function(fullpath){        
            var fileExists = false;
            $.ajax({
              url :fullpath,
              async: false,
            success:function(data){
               fileExists = true;
            },
            error: function(data){
              fileExists = false;  
              console.log(fullpath);
           },                                       
        });  
          return fileExists;
       },        
       
                                          
        //udi details validate/generate based on udi pattern
        setUDI: function(e){
            var index = $(".ul-udi-parts").index($(e.currentTarget).parent().parent());
            var udipattern = $(".li-do-UDI-pattern").eq(index).html();
            var lotno =  $(".li-udi-part-lotno").eq(index).val(); 
            var donorno = $(".li-udi-part-donorno").eq(index).val();
            var expdate = $(".li-udi-part-expdate").eq(index).val();
            var ltStr = "";
            var dnStr = "";
            var expStr="";
            var reqLot = 0, reqDono = 0, reqExpDate = 0; 
            var actLot = 0, actDono = 0, actExpDate = 0;
            this.isValidUdi = true;
            reqLot = udipattern.indexOf("(10)") != -1  ? 10 : 0;
            reqDono = udipattern.indexOf("(21)") != -1 ? 21 : 0;
            reqExpDate = udipattern.indexOf("(17)") != -1  ? 17 : 0;
            var expChkValue = reqLot+reqDono+reqExpDate;
            //lot no

            if(lotno.replace(" ","").length && reqLot == 10){
                ltStr = "(10)"+lotno;
                actLot = 10;
            }else{
                ltStr = "";
                actLot=0;
            }
            
            if(donorno.replace(" ","").length && reqDono==21){
                dnStr = "(21)"+donorno;
                actDono = 21;
            }else{
              
                dnStr = "";
                actDono = 0;
            }
     
            if(expdate.length){
//                var todaydate = new Date(this.today);
                if(this.isValidDate(expdate) == false){
                   actExpDate = 0;
                   showNativeAlert("Invalid date","Expiry date");
                   if(reqExpDate == 17)
                       $(".li-do-parts-udino").eq(index).html(""); 
                    
                    $(".li-udi-part-expdate").eq(index).val("");
                  //  $(".li-udi-part-expdate").eq(index).focus();
                    this.isValidUdi = false;
                    return;
                };
                var todaydate = new Date();
                var expirydate = new Date(expdate);
                  
                 if(expirydate < todaydate){
                     showNativeAlert(lblNm("msg_item_expired"),lblNm("expiry_date"));
                     actExpDate = 0;
                     if(reqExpDate == 17)
                       $(".li-do-parts-udino").eq(index).html(""); 
                    $(".li-udi-part-expdate").eq(index).val("");
                   // $(".li-udi-part-expdate").eq(index).focus(); 
                    this.isValidUdi = false; 
                 } 
                 else
                     {
                         if(reqExpDate == 17){
                                           var dd = ("0" + expirydate.getDate()).slice(-2), mm = ("0" + (expirydate.getMonth() + 1)).slice(-2), yr =(expirydate.getFullYear()).toString();
                         var yy=yr.substr(2,2)
                         expStr = ("(17)")+yy+mm+dd;
                             actExpDate = 17;
                         }
                         else{
                              expStr = "";
                              actExpDate=0;
                         }
                         
                     }
             }
           else{
                expStr = "";
                actExpDate=0;
            }
            
            var fulludi="";
            fulludi = actExpDate==17 ? fulludi+expStr : fulludi;
            fulludi = actLot==10 ? fulludi+ltStr : fulludi;
            fulludi = actDono==21 ? fulludi+dnStr : fulludi;
            
           //check the value to display UDI if all reqd entries are made
           var actChkValue = actLot+actDono+actExpDate;   
           if(actChkValue == expChkValue && udipattern != "") {
               $(".li-do-parts-udino").eq(index).html("(01)"+$(".li-do-parts-dino").eq(index).html()+fulludi);
           }
            else{
                $(".li-do-parts-udino").eq(index).html(""); 
 
            }
         },  //SetUDI
        
          //method to email UDI file.    
          emailUDIDetails: function() {
         	var that = this;       

            if(that.isPDFCreatd != false){
                var fname = that.JSONDataPDF.pdfFileName != undefined ? that.JSONDataPDF.pdfFileName+".pdf" : "";
                var fullPath=that.JSONData.fileSystemPath+"DO/PDF/WP/"+fname;    
                
                 if ($(window).width() < 480) 
			         cordova.plugins.email.open({
					    to:      [],
					    cc:      [],
					    subject: " Globus Medical - " + fname,
					    attachments: [ fullPath],
					    body:    lblNm("msg_attached_pdf")+that.JSONDataPDF.SurgeryLevel + " "+that.JSONDataPDF.SurgeryType 
                        +  lblNm("msg_s_performed_by")+that.JSONDataPDF.SurgeonName +"."
                        + "</b><br/>"+lblNm("msg_let_me_know")+"<br/><br/>"+lblNm("msg_thank_you")+"<br/>" + localStorage.getItem('fName') + " " + localStorage.getItem('lName'),
					    isHtml: true
				      });
                 else
			         window.plugin.email.open({
					    to:      [],
					    cc:      [],
					    subject: " Globus Medical - " + fname,
					    attachments: [ fullPath],
					    body:    lblNm("msg_attached_pdf")+that.JSONDataPDF.SurgeryLevel + " "+that.JSONDataPDF.SurgeryType 
                        +  lblNm("msg_s_performed_by")+that.JSONDataPDF.SurgeonName +"."
                        + "</b><br/>"+lblNm("msg_let_me_know")+"<br/><br/>"+lblNm("msg_thank_you")+"<br/>" + localStorage.getItem('fName') + " " + localStorage.getItem('lName'),
					    isHtml: true
				      });
                }
                else {
		 	      showNativeAlert(lblNm("msg_generate_pdf_email"),lblNm("generate_pdf"));
	 	        };
		 },
        
    
    isValidDate: function(txtDate){
      var currVal = txtDate;
      if(currVal == '')
          return false;
    
      var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
      var dtArray = currVal.match(rxDatePattern); // is format OK?
    
       if (dtArray == null) 
         return false;
        
        var dtMonth=0,dtDay=0,dtYear=0;
        //Checks for mm/dd/yyyy format.
        dtMonth = dtArray[1];
        dtDay= dtArray[3];
        dtYear = dtArray[5];        
    
            if (dtMonth < 1 || dtMonth > 12) 
                return false;
            else if (dtDay < 1 || dtDay> 31) 
                return false;
            else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) 
                return false;
            else if (dtMonth == 2) 
            {
                var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
                if (dtDay> 29 || (dtDay ==29 && !isleap)) 
                        return false;
            }
            return true;
        },


	});	

	return DOUDIView;
});
