/**********************************************************************************
 * File:        GmDOPartsUsedView.js
 * Description: 
 * Version:     1.0
 * Author:     	asivaraj
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification','searchview','saleitemmodel', 'saleitemcollection', 'saleitemlistview'
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification, SearchView, SaleItem, SaleItems, SaleItemListView ) {
        	
	'use strict';
	var DOPartsUsedView = Backbone.View.extend({

		events : {
			"click .clear-row-pu": "clearRow",
			"click .add-row-pu": "addRow",
			"click .li-span-type" : "toggleType",
			"click .div-do-keypad-values, #div-price-ea-box" : "showNumberKeypad",
			"click .li-lot-no-textbox .fa-camera" : "getPicture",
			"click #div-btn-done" : "displayQuantity",
			"change input, .txt-lot-no-box" : "checkChanges",
			"click #btn-do-hide-price" : "togglePrice",
			"click .li-lot-no-textbox .fa-barcode" : "getBarcode",
            "click .qty-plus-pu" : "incQty",
            "click .qty-minus-pu" : "decQty",
            "change .txt-lot-no-box" : "getLotNumRecParts",
            "click #div-do-parts-scan .fa-barcode" : "getPartFrmBarcode",
            "click #parts-help-icon" : "showPartHelpPopup",
		},

		/* Load the templates */ 
		template: fnGetTemplate(URL_DO_Template, "GmDOPartsUsed"),
		template_partdetails: fnGetTemplate(URL_DO_Template, "GmDOPartsUsedDetails"),
		template_partDetailsMobile: fnGetTemplate(URL_DO_Template, "GmDOPartsUsedDetailsMobile"),
		template_numberkeypad: fnGetTemplate(URL_DO_Template, "GmDOPartsNumberKeypad"),
		template_PartFromBarcodeHelp: fnGetTemplate(URL_DO_Template, "GmDOPartFromBarcodeHelp"),
        
              
    

		initialize: function(options) {
			this.parent = options.parent;
			this.render(options);
            this.usagelotfl = ""; 
            this.disableReqTyp = "";
		},

		// Render the initial contents when the Catalog View is instantiated
		render: function (options) {
//			showMsgView("011");
			
			var that = this;
            var part_search;
			this.$el.html(this.template());
            
            /* Parts Used tab - alignment issue fix in Parts used tab*/
             if ($(window).width() < 480) {
                     part_search = lblNm("part_search");
             }else{
                     part_search = lblNm("msg_character_search");
             }
			var renderOptions = {
							"el":this.$('#div-do-parts-used-txtkeyword'),
							"ID" : "txt-keyword",
							"Name" : "txt-search-name",
							"Class" : "txt-keyword-initial",
							"minChar" : "3",
							"autoSearch": true,
							"placeholder": part_search,
							"fnName": fnGetDOParts,
							"usage": 2,
							"template" : 'GmSearchResult',
							"template_URL" : URL_Common_Template,
							"callback": function(pList) {
                                that.partLoadFl = "Y";
                                that.getItemsFromSearchView(pList);}
							};
            

			var searchview = new SearchView(renderOptions);
			if(this.parent.DO != undefined ) {
				 this.displayParts(this.parent.JSONParts);
			}
            fnGetLotUseFL(function(usageFL){
               that.usagelotfl=usageFL;
            });
            //  PMT-43383 :: Disable the option to select Loaner in Type field in Parts Used tab
            fnDisableDOReqType(function(dt){
               that.disableReqTyp=dt;
            });
                if(this.parent.JSONData.accDivCompanyFl== "Y"){
                    this.$("#li-header-exp-date").show();
                    this.$("#div-do-parts-scan").show();
                    this.$("#div-do-parts-help").show();
                    if ($(window).width() < 480) {
                        this.$("btn-do-hide-price").addClass("btn-do-hide-price-div");
                    }
                }else{
                    this.$("#li-header-exp-date").hide();
                    this.$("#div-do-parts-scan").hide();
                    this.$("#div-do-parts-help").hide();
                }
			this.coveringRepMode();
            this.comnMobFunc();
            
			return this;
		},

		//Duplicates the Part Information when Plus icon is clicked
		addRow:function(e){
			var newJSON = {};
			var that = this;
			newJSON.ID = $(e.currentTarget).parent().parent().attr("id");
			newJSON.Name = $(e.currentTarget).parent().parent().find(".li-do-parts-used-list").text().trim();
			newJSON.Price = $(e.currentTarget).parent().parent().find("#div-price-ea-box").attr("value");
			newJSON.Qty = 1;
			newJSON.displayUnitPrice = $(e.currentTarget).parent().parent().find("#div-price-ea-box").text().trim();
			newJSON.displayPrice = $(e.currentTarget).parent().parent().find("#div-price-ea-box").text().trim();
			newJSON.ProductMaterial = $(e.currentTarget).parent().parent().attr("productmaterial");
            newJSON.ProductFamily = $(e.currentTarget).parent().parent().attr("productfamily");
			newJSON.ProductClass = $(e.currentTarget).parent().parent().attr("productclass");
            newJSON.expiryDt = "";
            if ($(window).width() < 480) {
                newJSON.accDivCompnayFl = that.parent.JSONData.accDivCompanyFl;
            }
            else{
                if($("#li-header-exp-date").is(":visible"))
                    newJSON.accDivCompnayFl = "N";
            }
			var index = $(e.currentTarget).parent().parent().index();
            if ($(window).width() < 480) {
                $("#div-do-parts-list > ul:nth-child("+(index+1)+")").after(this.template_partDetailsMobile(newJSON));
            }
            else{
                $("#div-do-parts-list > ul:nth-child("+(index+1)+")").after(this.template_partdetails(newJSON));
            }
			this.coveringRepMode();
			this.calculateTotal();
			this.getLotNoBox();
            this.fnordertypecheck();
			this.updateJSON();
            this.comnMobFunc();
		},

		//Calculates the Extened Price and Total Amount
		calculateTotal: function(callback) {
			var sum = 0;
            var currSymbol = localStorage.getItem("cmpcurrsmb");
			this.$(".ul-parts-used-list").each(function() {
				var value = parseInt($(this).find("#div-qty-box").text())*parseFloat($(this).find("#div-price-ea-box").attr("value"));
                var price = $(this).find("#div-price-ea-box").text();
				$(this).find("#div-ext-price-box").attr("value",value);
				$(this).find("#div-ext-price-box").text(currSymbol+""+parseFloat(value).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                $(this).find(".pu-ext-price").attr("value",value);
                $(this).find(".pu-ext-price").text(currSymbol+""+parseFloat(value).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                $(this).find(".pu-prc").text(price);
				sum += value;
			});
			this.$("#div-do-parts-footer-title").attr("value",sum);
			this.$("#div-do-parts-footer-title").find('span').text(currSymbol+""+ parseFloat(sum).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
			
			if(callback!=undefined) {
				callback();
			}
		},

		//Adds a class to indicate the Parts are changed after dependent Tab's changes before calling updateJSON
		checkChanges: function(event) {
            var that = this;
			if(event.currentTarget.id!="txt-keyword") {
            if ($(window).width() < 480) {
                var partnum= $(event.currentTarget).parent().parent().parent('ul').attr('id')
            }else{
                var partnum= $(event.currentTarget).parent().parent('ul').attr('id');
            }
                
				if(this.parent.partsChangeMode)
					$(event.currentTarget).closest('.ul-parts-used-list').addClass('part-updated');
                if(that.parent.JSONData.accDivCompanyFl== "Y"){
                   
                       fnValidatePartCnumFromCloud(partnum,event.currentTarget.value,function(data){
                        
                          if(data != ''){
                                $(event.currentTarget).attr("validCloudLotfl","Y");                 
                            }else{
                                $(event.currentTarget).attr("validCloudLotfl","N");
                               var lotimgsrc=$(event.currentTarget).parent().parent().find('.li-img-lot-no img').attr('src');
                                if(lotimgsrc == '' || lotimgsrc == undefined){
                                   fnShowStatus("Provided Lot not available in the Lot Master.So please add a picture for the part - "+partnum+" to proceed");
                                   }
                                 
                            }
                           that.updateJSON();
                       });  
                   }
                else
                    this.updateJSON();
			}
			
		},

		//Removes the Part Information if clicked on Minus icon
		clearRow: function(e){
			var that = this;
			this.partid = $(e.currentTarget).parent().parent().attr('id');
            this.usedFl = $(e.currentTarget).parent().parent().attr('usedfl');
			showNativeConfirm(lblNm("msg_want_delete_part") + this.partid +"?", lblNm("delete"), [lblNm("msg_yes"),lblNm("msg_no")], function (index) {
            	if(index==1) {
	            	if(!that.parent.partsChangeMode) {
                        $(e.currentTarget).parent().parent().remove();
                        
                        if(that.usedFl == "Y"){
                            that.parent.JSONData.arrDORecordPartVO = that.parent.JSONData.arrDORecordPartVO.filter(function(item){
                                return item.partid !== that.partid;
                            });

                            that.parent.JSONData.arrDORecordappendData = that.parent.JSONData.arrDORecordappendData.filter(function(apndItem){
                                return apndItem.ID !== that.partid;
                            });
                            that.parent.JSONData.arrGmOrderItemVO = _.filter(that.parent.JSONData.arrGmOrderItemVO, {
                                'pnum': that.partid,
                                'usedFl': "Y"
                            });
                            that.parent.JSONParts = _.filter(that.parent.JSONParts, {
                                'ID': that.partid,
                                'usedFl': "Y"
                            });
                        }
                        
                        that.calculateTotal(function () {
                          that.updateJSON();
                        });
                        
					}
					else {
						if(that.parent.signatureMode == true) {
							that.parent.signatureMode = false;
							that.parent.signaturesubmit = false;
							that.parent.signaturesrc = "";
							$(".img-do-sign").each(function() {
								$(this).attr("src","");
							});
						}
						
						var index = $(e.currentTarget).parent().parent().index();
						var indexValue = $(e.currentTarget).parent().parent().attr('indexValue');
						
						that.parent.JSONData.arrGmOrderItemVO.splice(index,1);

						that.parent.JSONData.arrGmTagUsageVO = _.reject(that.parent.JSONData.arrGmTagUsageVO, function(arr){ return parseInt(arr.indexValue) == parseInt(indexValue); });
						that.parent.JSONData.gmTxnShipInfoVO = _.reject(that.parent.JSONData.gmTxnShipInfoVO, function(arr){ return parseInt(arr.indexValue) == parseInt(indexValue); });
						that.parent.JSONParts = _.reject(that.parent.JSONParts, function(arr){ return parseInt(arr.indexValue) == parseInt(indexValue); });

						var len = that.parent.JSONData.arrGmOrderItemAttrVO.length;

						for(var i=0;i<len;i++)
							that.parent.JSONData.arrGmOrderItemAttrVO = _.difference(that.parent.JSONData.arrGmOrderItemAttrVO,_.findWhere(that.parent.JSONData.arrGmOrderItemAttrVO,{'indexValue' : indexValue}));
                        
                         

                        if(that.usedFl == "Y"){
                            that.parent.JSONData.arrDORecordPartVO = that.parent.JSONData.arrDORecordPartVO.filter(function(item){
                                return item.partid !== that.partid;
                            });

                            that.parent.JSONData.arrDORecordappendData = that.parent.JSONData.arrDORecordappendData.filter(function(apndItem){
                                return apndItem.ID !== that.partid;
                            });
                        }

						$(e.currentTarget).parent().parent().remove();
                        
						that.calculateTotal(function() {
                             that.updateJSON();
                            var currSymbol = localStorage.getItem("cmpcurrsmb");
                that.parent.JSONData.total = parseFloat( $("#div-do-parts-footertitle").find("span").text().replace(",","").replace(currSymbol,"")).toString();
							that.parent.saveDO();
						});
					}
            	}  
            });
		},

		//Hinding the Part Price and Total Amount if the user is a covering sales rep
		coveringRepMode: function() {
			if((this.parent.coveringRepMode == true)) {
				this.$("#btn-do-hide-price").hide();
			}
			if((this.parent.coveringRepMode == true) || (this.parent.hideprice)) {
				this.$("#li-header-price-ea").hide();
				this.$("#li-header-ext-price").hide();
				this.$("#div-do-parts-footer-title").hide();
				this.$(".lbl-price-ea").hide();
                this.$(".li-price-ea-textbox").hide();
                this.$(".lbl-ext-price").hide();
				this.$(".li-ext-price-textbox").hide();	
                //Code to hide the price columns in mobile view
                this.$("#div-price-ea-box").hide();
                this.$("#div-ext-price-box").hide();
                this.$(".pu-prc").hide();
                this.$(".pu-ext-price").hide();
			} 
		},

		togglePrice: function(event) {
			var that = this;
			if(this.parent.hideprice) {
				showNativePrompt(lblNm("msg_enter_userid"), lblNm("msg_show_price"), [lblNm("cancel"),lblNm("msg_show")], '', function(data) {
					if(data.buttonIndex==2) {
						if(data.input1.toLowerCase()==localStorage.getItem('userName').toLowerCase()) {
							that.parent.hideprice = 0;
							$(event.currentTarget).text(lblNm("hide_price"));
							if(!that.parent.coveringRepMode) {
								that.$("#li-header-price-ea").show();
								that.$("#li-header-ext-price").show();
								that.$("#div-do-parts-footer-title").show();
								that.$(".li-price-ea-textbox").show();
								that.$(".li-ext-price-textbox").show();
                                that.$(".lbl-price-ea").show();
                                that.$(".lbl-ext-price").show();
                                //Code to Show the price columns in mobile view
                                that.$("#div-price-ea-box").show();
                                that.$("#div-ext-price-box").show();
                                that.$(".pu-prc").show();
                                that.$(".pu-ext-price").show();
							}
						}
						else {
							showNativeAlert(lblNm("msg_valid_username"),lblNm("msg_invalid_name"));
						}
					}
				})
				
			}
			else {
				this.parent.hideprice = 1;
				$(event.currentTarget).text(lblNm("msg_show_price"));
				this.coveringRepMode();
				
			}
				
		},

		//Used to display Part information if loaded from the Booked DO's (Draft)
		displayParts: function(data) {
            var that = this;
            var currSymbol = localStorage.getItem("cmpcurrsmb");
			$("#div-do-parts-list").show();
			$("#div-do-parts-used").find("#div-select-all").addClass("hide");
			$(".ul-do-parts-list-header").show();
			$(".div-do-parts-list-footer").show();
			for(var i=0;i<data.length;i++) {
               		 var isExpired = false;
				var price = data[i].price;
				var unitprice = data[i].unitprice;
				data[i].displayUnitPrice = currSymbol+"" + parseFloat(unitprice).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				data[i].displayPrice = currSymbol+"" + parseFloat(price).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		                 data[i].accDivCompnayFl = that.parent.JSONData.accDivCompanyFl;
		                    var dateObjExpiryDt = new Date(data[i].expiryDt);
		                    var dateObjCurrDt = new Date();
		                    if(dateObjCurrDt > dateObjExpiryDt){
		                                    data[i].isExpired = true;
		                    }
                data[i].ProductFamily = data[i].pfamily;
				//this.$("#div-do-parts-list").append(this.template_partdetails(data[i]));
                 if ($(window).width() < 480) {
                        this.$("#div-do-parts-list").append(this.template_partDetailsMobile(data[i]));
                    }
                    else{
                       this.$("#div-do-parts-list").append(this.template_partdetails(data[i]));
                    }
                console.log(JSON.stringify(data));
			}
			this.calculateTotal();
			this.displayType();
			// this.updateJSON();
		},
		
		//Enables \ Disables the checkbox for depending upon the given part information
		displayType: function() {
            var that = this;
			this.$(".ul-parts-used-list").each(function() {
				var lotno = $(this).find('.li-lot-no-textbox').find('input').val();
				$(this).find('.li-lot-no-textbox').find('input').val(lotno.trim());
				if($(this).attr("type") == "C") {
					$(this).find("#lbl-loaner").removeClass("input-checked");
					$(this).find("#lbl-consignment").addClass("input-checked");
					$(this).find(".repl-checkbox").prop("disabled", true);
					$(this).find(".repl-checkbox").prop("checked", false);
				}
				else if($(this).attr("type") == "L") {
					$(this).find("#lbl-loaner").addClass("input-checked");
					$(this).find("#lbl-consignment").removeClass("input-checked");
					$(this).find(".repl-checkbox").prop("disabled", false);
				}

				if(lotno!="") {
					$(this).find('.li-lot-no-textbox').find('input').show();
					$(this).find('.li-lot-no-textbox').find('i').show();
					$(this).find(".li-lot-no-textbox").css("background-color","#444");
				}
			});
			this.getLotNoBox();
            if (localStorage.getItem("cmpid") != '1009' && localStorage.getItem("cmpid") != '1011' && localStorage.getItem("cmpid") != '1026') {
                that.setDefaultLoanerPart();
            }
		},

		getPicture: function(event) {
			var that = this;
			var parent = $(event.currentTarget).parent().parent();
			navigator.camera.getPicture(function (imageURI) {
				$(parent).find('.li-img-lot-no').removeClass('do-imp-hide').find('img').attr('src','data:image/jpeg;base64,'+ imageURI);
			    // $("#img-billing-info-notes").attr("src","data:image/jpeg;base64,"+ imageURI).show();
			    that.updateJSON();
			}, function(){
			//console.log("failure");
			}, { destinationType: Camera.DestinationType.DATA_URL });

		},
        
		 getBarcode: function(event) {
			 var that = this;
			 var parent = $(event.currentTarget).parent();
             if(that.parent.JSONData.accDivCompanyFl != "Y"){
                     cordova.plugins.barcodeScanner.scan(
                       function (result) {
                            var partcno = result.text;
                              fnValidateControlNo(partcno,function(controlNo){
                                  $(parent).find("input").val(controlNo);
                                       if(that.parent.partsChangeMode)
                                           $(event.currentTarget).closest('.ul-parts-used-list').addClass('part-updated');
                                            that.updateJSON();
                              });
                       }, 
                       function (error) {
                           console.log("Scanning failed: " + error);
                           }
                        );
                 }else{
                     fnShowStatus("This feature is not applicable.");
                 }
			 },
        getPartFrmBarcode: function(event) {
			 var that = this;
			 var parent = $(event.currentTarget).parent();
			 cordova.plugins.barcodeScanner.scan(
			   function (result) {
			        var partcno = result.text;
			
					partcno = partcno.trim();
				
					partcno = $.trim(partcno);
				    
					partcno = partcno.replace(/[^a-zA-Z0-9\^\.\-]/g, '');
	               console.log(partcno);
                   console.log(partcno.length);
			          fnGetValidPartDtls(partcno,function(partNoData){
                           console.log(partNoData);
	                    if(partNoData != undefined && partNoData.length > 0 && partNoData != ''){
			 
                        for(var i=0; i<partNoData.length;i++){
		
                            var partid = partNoData[i].ID;
                            var partnm = partNoData[i].ID+" "+partNoData[i].pdesc;
                            var productmaterial = partNoData[i].pmat;
                            var productfamily = partNoData[i].pfamily;
                            var productclass = partNoData[i].pclass;
                            var usedFl = "";
                            var Qty = 1;
                            var Lotno = partNoData[i].lotno;
                            var expiryDt = partNoData[i].expirydate;
                            console.log(partid);
                            var rowEl = $("<a id='" + partid + "' class=item-searchlist name='" + partnm + "' productmaterial='" + productmaterial + "' productfamily='" + productfamily + "' productclass='" + productclass + "' usedFl='" + usedFl + "' Qty='" + Qty + "' Lotno='" + Lotno + "' expiryDt='"+expiryDt+"' partdtlsfrmcloudfl=Y ><div class='div-data hide'></div> <i class='fa fa-check'></i></a>");
                            var CurrentRow = {
                                "ID": partid,
                                "Name": partnm,
                                "Element": rowEl
                            };
                            console.log(rowEl);
                             that.partLoadFl = "Y";
                            that.getItemsFromSearchView(CurrentRow);
                        }
                       
                       } else{
                           fnShowStatus("Please scan the valid barcode from Patient label or box label.");
                       }     

			          });
			   }, 
			   function (error) {
			       console.log("Scanning failed: " + error);
			       }
			    );
			 },
        
        //Add JSON Parts into parts used tab from record tag
        doRecPartsAdd: function () {
            var that = this;
            var recordTagData = "";
            this.parent.JSONData.arrDORecordappendData = _.flatten(this.parent.JSONData.arrDORecordappendData);
            this.parent.JSONData.arrDORecordappendData = _.uniq(this.parent.JSONData.arrDORecordappendData, function (item) {
                return item.ID;
            });
            this.parent.JSONData.arrDORecordappendData = _.each(this.parent.JSONData.arrDORecordappendData, function (dt) {
                dt.usedFl = "Y";
                _.each(that.parent.JSONData.arrDORecordPartVO, function (partdt) {
                    if (partdt.partid == dt.ID) {
                        console.log(partdt.qty);
                        dt.Qty = partdt.qty;
                        dt.lotnum = partdt.lotnum;
                    }
                });
            });
            
            /*Commenting the Below code as the Part of PC-2756(Sep2020Release) ,No need to delete the parts (from record tag tab ) and add it again
            */
            /*this.parent.JSONData.arrGmOrderItemVO = this.parent.JSONData.arrGmOrderItemVO.filter(function (orditem) {
                return orditem.usedFl !== "Y";
            });
            this.parent.JSONParts = this.parent.JSONParts.filter(function (dl) {
                return dl.usedFl !== "Y";
            });

            $(".ul-parts-used-list").each(function () {
                if ($(this).attr("usedFl") == "Y") {
                    $(this).remove();
                }
            });
            */
            
            /* Get all the Part numbers on the Part used tab and check if the parts are added from Record Tags (Usedfl ==    'Y') then we need to check those parts on the arrDORecordappendData array and add the parts on the Parts used tab which is not available on the Parts used tab)
            */
            var partnumber = "";
            var arrayRecDotemp = [];
            arrayRecDotemp = this.parent.JSONData.arrDORecordappendData;
            $(".ul-parts-used-list").each(function () {
                if ($(this).attr("usedFl") == "Y") {
                    partnumber = $(this).attr("ID");
                    arrayRecDotemp= _.filter(arrayRecDotemp,function(partApndItem){
                         console.log(" partApndItem=="+partApndItem);
                         console.log(" partnumber=="+partnumber);
                                return partApndItem.ID !== partnumber;
                            });
                }
            });
             console.log("arrayRecDotemp =="+JSON.stringify(arrayRecDotemp));
            
            /*Assign the temp array to another array and loop it and load the new parts to the parts used tab*/
            recordTagData = arrayRecDotemp ;
            if (recordTagData.length > 0) {
                for (var i = 0; i < recordTagData.length; i++) {
                    var partid = recordTagData[i].ID;
                    var partnm = recordTagData[i].Name;
                    var productmaterial = recordTagData[i].ProductMaterial;
                    var productfamily = recordTagData[i].ProductFamily;
                    var productclass = recordTagData[i].ProductClass;
                    var usedFl = recordTagData[i].usedFl;
                    var Qty = recordTagData[i].Qty;
                    var Lotno = recordTagData[i].lotnum;

                    var rowEl = $("<a id=" + partid + " class=item-searchlist name=" + partnm + " productmaterial=" + productmaterial + " productfamily=" + productfamily + " productclass=" + productclass + " usedFl=" + usedFl + " Qty=" + Qty + " Lotno=" + Lotno + " ><div class='div-data hide'></div> <i class='fa fa-check'></i></a>");
                    var CurrentRow = {
                        "ID": partid,
                        "Name": partnm,
                        "Element": rowEl
                    };
                    that.partLoadFl = "";
                    that.getItemsFromSearchView(CurrentRow);
                }
            }

            console.log("JSONParts " + JSON.stringify(this.parent.JSONData.arrDORecordappendData));
        },
			 
		//Fired when part is selected from the search
		getItemsFromSearchView: function(pList) {
			var that=this;
			var arrSelectedData = new Array();
			if(pList.length==undefined)
				arrSelectedData.push(pList);
			else
				arrSelectedData = pList;

			$("#div-do-parts-list").show();
				
			$("#div-do-parts-used").find("#div-select-all").addClass("hide");
			$(".ul-do-parts-list-header").show();
			$(".div-do-parts-list-footer").show();
			$("#div-itemlist").html("<li>"+lblNm("search_again")+"!</li>");
			$("#txt-keyword").val('');
			$("#div-common-search-x").addClass('hide');
			this.loadParts(0,arrSelectedData);
		},

		//Checks if Lot No is mandatory for the part
		getLotNoBox:function(){
            var lotfl;
            var that=this;
            $("#div-do-parts-list").find("ul").each(function() {
            if(that.usagelotfl=='Y'){//default disply usage lot for all parts
                     lotfl='Y';
            }else if(($(this).attr("productclass")=="4030") && ((($(this).attr("productmaterial")) == "100845") || (($(this).attr("productmaterial") == "100844")))){
		     lotfl='Y';
            }else if(($(this).attr("productclass")=="4030") && ($(this).attr("productfamily") == "4050") && (that.parent.JSONData.accDivCompanyFl== "Y")){ 
                     lotfl='Y';
            }else{
                lotfl ='N';
            }
                if(lotfl=='Y'){
                    $(this).find(".txt-lot-no-box").show();
					$(this).find(".fa").show();
					$(this).find(".li-lot-no-textbox").css("background-color","#444");
					var lotno = $(this).find(".txt-lot-no-box").val();
                }
            });
            
		},

		//loads the selected parts in the list.
		//Called Recursively - to fetch the Price for each Part
		loadParts: function(iValue, data) {
			 var that = this;
            var isExpired = false;
			if(iValue<data.length) {
			 
				fnGetPriceFor(data[iValue].ID, this.parent.JSONData.acctid, function(price) {
					price = (price)?price:0;
				    var currSymbol = localStorage.getItem("cmpcurrsmb");
        //Removing the below price validation for PMT-13359
//                    if(price == null || price == undefined ){
//                        showError("Price is not set for this part"); 
//                        $("#div-do-parts-footer-title").hide();
//                        that.loadParts(iValue+1,data);
//                    }
//                    else{
					data[iValue].Price = price;
                    if(that.partLoadFl == ""){
                        data[iValue].Qty = $(data[iValue].Element).attr("Qty");
                    }
                    else{
                        data[iValue].Qty = 1;
                    }
                    data[iValue].ProductMaterial = $(data[iValue].Element).attr("productmaterial");
                    data[iValue].productfamily = $(data[iValue].Element).attr("productfamily");
                    data[iValue].ProductClass = $(data[iValue].Element).attr("productclass");
                    
					data[iValue].displayUnitPrice =currSymbol+"" + parseFloat(price).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					data[iValue].displayPrice = currSymbol+"" + parseFloat(price).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					data[iValue].repl = false;
                    
					//console.log(data[iValue]);
					//console.log(data[iValue].Name);
                    
                    data[iValue].ProductFamily = $(data[iValue].Element).attr("productfamily");
                    data[iValue].usedFl = $(data[iValue].Element).attr("usedFl");
                    data[iValue].lotno = $(data[iValue].Element).attr("Lotno");
                    if(that.parent.JSONData.accDivCompanyFl== "Y"){

                        data[iValue].accDivCompnayFl = that.parent.JSONData.accDivCompanyFl;
                        var lotnumber = $(data[iValue].Element).attr("Lotno");
                        var partDtlsCloudFl = $(data[iValue].Element).attr("partdtlsfrmcloudfl");
                        if(partDtlsCloudFl=='Y'){
                            data[iValue].PartDtlsFrmCloudFl = partDtlsCloudFl;
                            var strexpdt = $(data[iValue].Element).attr("expiryDt");
                            var dateObjExpiryDt = new Date(strexpdt);
                            var dateObjCurrDt = new Date();
                            if(dateObjCurrDt > dateObjExpiryDt){
                                data[iValue].isExpired = true;
                               fnShowStatus("Scanned Product has expired");
                               }
                          var dtfmt = localStorage.getItem("cmpdfmt").toUpperCase();
                          var formatexpdt =moment(strexpdt,'MM/DD/YYYY').format(dtfmt);
                            //data[iValue].expiryDt = $.format.date(strToDate(strexpdt),localStorage.getItem("cmpdfmt"));
                            data[iValue].expiryDt = formatexpdt;
                            if(lotnumber == '' || lotnumber == undefined){
                               data[iValue].validCloudLotfl = 'N'; 
                            }
                            
                        }   
                        else{
                            data[iValue].PartDtlsFrmCloudFl = 'N';
                            data[iValue].expiryDt = "";
                        }       
                       
                    }
//					console.log(data[iValue].ProductFamily);
                    
                    if ($(window).width() < 480) {
                        that.$("#div-do-parts-list").append(that.template_partDetailsMobile(data[iValue]));
                    }
                    else{
                        that.$("#div-do-parts-list").append(that.template_partdetails(data[iValue]));
                    }
					that.loadParts(iValue+1,data);
					that.coveringRepMode();
					that.getLotNoBox();
                    if(localStorage.getItem("cmpid") != '1009' && localStorage.getItem("cmpid") != '1011' && localStorage.getItem("cmpid") != '1026'){
                        that.setDefaultLoanerPart();
                    }
//                    }
				});

			} else{

				this.calculateTotal();
                this.fnordertypecheck();
				this.updateJSON();
				}
            	
                this.comnMobFunc();
            
		},
        
        comnMobFunc: function(){
                this.$("#div-do-parts-list").find(".ul-parts-used-list-mobile").each(function(){
                console.log($(this).next().is('ul'));
                if($(this).next().is('ul')){
                    $(this).addClass("ul-show-part");
                    $(this).children(".togglebx").children("img").removeClass("onActive");
                }
                else{
                    $(this).removeClass("ul-show-part");
                    $(this).children(".togglebx").children("img").addClass("onActive");
                }

//                var puType =  that.$("#div-do-parts-list").find(".ul-parts-used-list-mobile").children('#mobile-expand-div').find(".li-span-type.input-checked").text();
//                    alert(puType);
//                if(puType == 'L'){
//                    $(this).find(".pu-type").text("L");
//                }
//                else{
//                    $(this).find(".pu-type").text("C");
//                }
                    
            });

            $(".togglebx").unbind('click').bind('click', function() {
//                $(".togglebx").on("click", function () {
                $(this).parent(".ul-parts-used-list-mobile").toggleClass('ul-show-part');
                $(this).children("img").toggleClass("onActive");
                 var puType = $(this).parent(".ul-parts-used-list-mobile").children('#mobile-expand-div').find(".li-span-type.input-checked").text();
                if(puType == 'L'){
                    $(this).parent(".ul-parts-used-list-mobile").children('.mobile-short-span-pu').children('.pu-st-span-desc').find(".pu-type").text("L");
                }
                else{
                    $(this).parent(".ul-parts-used-list-mobile").children('.mobile-short-span-pu').children('.pu-st-span-desc').find(".pu-type").text("C");
                }
            });

            $(".ul-parts-used-list-mobile").on('swipeleft', function (e, data) {
                $(this).find(".delete-pu-span").addClass("onShowEl");
                $(this).find('#togglebox').addClass('toggle-delete');
                $(this).find('#mobile-expand-div').removeClass('copy-delete-div-change');
                $(this).find('#mobile-short-div').removeClass('copy-delete-div-change');
                $(this).find('#mobile-expand-div').addClass('delete-div-change');
                $(this).find('#mobile-short-div').addClass('delete-div-change');
                $(this).find(".copy-pu-span").removeClass("onShowEl");
                
            });

            $(".ul-parts-used-list-mobile").on('swiperight', function (e, data) {
                $(this).find(".copy-pu-span").addClass("onShowEl");
                $(this).find('#togglebox').removeClass('toggle-delete');
                $(this).find('#mobile-expand-div').removeClass('delete-div-change');
                $(this).find('#mobile-short-div').removeClass('delete-div-change');
               $(this).find('#mobile-expand-div').addClass('copy-delete-div-change');
                $(this).find('#mobile-short-div').addClass('copy-delete-div-change');
                $(this).find(".delete-pu-span").removeClass("onShowEl");
            });

            $(".ul-parts-used-list-mobile").on('tap', function (e, data) {
                $(this).find(".delete-pu-span").removeClass("onShowEl");
                 $(this).find('#togglebox').removeClass('toggle-delete');
                $(this).find('#mobile-expand-div').removeClass('delete-div-change');
                $(this).find('#mobile-short-div').removeClass('delete-div-change');
                $(this).find('#mobile-expand-div').removeClass('copy-delete-div-change');
                $(this).find('#mobile-short-div').removeClass('copy-delete-div-change');
                $(this).find(".copy-pu-span").removeClass("onShowEl");
            });
        },
        
        incQty: function(e){
            var that = this;
            var count = parseInt($(e.currentTarget).siblings("#div-qty-box").text());
            var countEl = $(e.currentTarget).siblings("#div-qty-box");
                count++;
                countEl.text(count);
            $(e.currentTarget).parent().parent().siblings("#mobile-short-div").children('.pu-st-span-desc').find('.pu-qty').text(count);
                if(that.parent.partsChangeMode)
				    $(e.currentTarget).closest('.ul-parts-used-list').addClass('part-updated');
            
            /*No need to update the Qty in Record Tags screen VO as the Part of PC-2756*/    
            /*var partText = $(e.currentTarget).closest("#mobile-expand-div").siblings(".mobile-short-span-pu").find(".pu-st-span-title").text();
            var partQty = $(e.currentTarget).siblings("#div-qty-box").text();
            var usedFl = $(e.currentTarget).closest('.ul-parts-used-list').attr("usedfl");
            _.each(that.parent.JSONData.arrDORecordPartVO, function (dt) {
                if ((dt.partid + " " + dt.partnm) == partText && usedFl == "Y") {
                    dt.qty = partQty;
                }
            });
            */
                that.calculateTotal();
                that.updateJSON();
        },
        
        decQty: function(e){
            var that = this;
            var count = parseInt($(e.currentTarget).siblings("#div-qty-box").text());
            var countEl = $(e.currentTarget).siblings("#div-qty-box");
             if (count > 1) {
                count--;
                countEl.text(count);
               $(e.currentTarget).parent().parent().siblings("#mobile-short-div").children('.pu-st-span-desc').find('.pu-qty').text(count);
                if(that.parent.partsChangeMode)
				    $(e.currentTarget).closest('.ul-parts-used-list').addClass('part-updated');
                 
             /*No need to update the Qty in Record Tags screen VO as the Part of PC-2756*/    
            /* var partText = $(e.currentTarget).closest("#mobile-expand-div").siblings(".mobile-short-span-pu").find(".pu-st-span-title").text();
             var partQty = $(e.currentTarget).siblings("#div-qty-box").text();
             var usedFl = $(e.currentTarget).closest('.ul-parts-used-list').attr("usedfl");
             _.each(that.parent.JSONData.arrDORecordPartVO, function (dt) {
                 if ((dt.partid + " " + dt.partnm) == partText && usedFl == "Y") {
                     dt.qty = partQty;
                 }
             });
             */
                that.calculateTotal();
                that.updateJSON();
             }  
        },
        

		//Displays the Number keypad 
		showNumberKeypad:function(event){
			var that = this;
			this.parent.showNumberKeypad(event, function(val, target) {
                var currSymbol = localStorage.getItem("cmpcurrsmb");
				if(event.currentTarget.id == "div-qty-box"){
					if(val == "0" || val == "00" || val == "000"){
						// $("#div-qty-box").text();
						showMsgView("053");
					}else {
						if(that.parent.partsChangeMode)
							$(target).closest('.ul-parts-used-list').addClass('part-updated');
						$(target).text(val);
//                        $(target).parent().parent().find(".pu-qty").text(val);
                        $(target).parent().parent().siblings("#mobile-short-div").children('.pu-st-span-desc').find('.pu-qty').text(val);
					}
				}else {
					if($("#div-qty-entry-text").text() != "")
					$(target).text(currSymbol+"" + parseFloat(val).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
					$(event.currentTarget).attr("value",val);
					if(that.parent.partsChangeMode)
							$(target).closest('.ul-parts-used-list').addClass('part-updated');
                     $(target).parent().parent().find(".pu-qty").text(val);
				}
                
              /* No need to update the Qty in Record Tags screen VO as the Part of PC-2756*/    
              /*  var partText = $(event.currentTarget).closest(".li-qty-textbox").siblings(".li-do-parts-used-list").text();
                var partQty = $(event.currentTarget).text();
                var usedFl = $(event.currentTarget).closest('.ul-parts-used-list').attr("usedfl");
                _.each(that.parent.JSONData.arrDORecordPartVO, function (dt) {
                    if ((dt.partid + " " + dt.partnm) == partText && usedFl == "Y") {
                        dt.qty = partQty;
                    }
                });
                */
                
			that.calculateTotal();
			that.updateJSON();
				
			});
            
		},
        
        getLotNumRecParts: function(e){
            console.log(e);
            var lotnum = $(e.currentTarget).val();
            console.log(lotnum);
            var recId = $(e.currentTarget).closest('.ul-parts-used-list').attr("id");
            var usedFl = $(e.currentTarget).closest('.ul-parts-used-list').attr("usedfl");
            console.log(recId);
            this.parent.JSONData.arrDORecordPartVO = _.each(this.parent.JSONData.arrDORecordPartVO,function(item){
                if(item.partid == recId && usedFl == "Y"){
                    item.lotnum = lotnum;
                }
            });
            this.updateJSON();
        },

		//Changes the Type (C/L) Also Enables / disables the Repl Checkbox based on the selceted type
		toggleType:function(event){
            var that = this;
            if(this.disableReqTyp != "Y") {
                if(this.parent.JSONData.ordertype==""){ // to avoid the toggle event if order type has chosen
                var id = (event.currentTarget.id);
//                    $(event.currentTarget).parent().find("#lbl-loaner").toggleClass("input-checked");
//                    $(event.currentTarget).parent().find("#lbl-consignment").toggleClass("input-checked");

                this.toggleid = event.currentTarget.id;
                this.toggleclass = $(event.currentTarget).attr("class");
                $(event.currentTarget).parent().find('.li-span-type').removeClass("input-checked");
                $(event.currentTarget).addClass("input-checked");
                    
                if((this.toggleid == "lbl-consignment")){

                        $(event.currentTarget).parent().parent().find(".repl-checkbox").prop("disabled", true);
                        $(event.currentTarget).parent().parent().find(".repl-checkbox").prop("checked", false);
//                        if(this.toggleclass == "li-span-type"){
//                            $(event.currentTarget).parent().parent().find(".repl-checkbox").prop("disabled", false);
//                        }
                    $(event.currentTarget).parent().parent().siblings('#mobile-short-div').find(".pu-type").text("C");
                } else {

                    // if($(this.toggleclass).hasClass("input-checked")){
                        $(event.currentTarget).parent().parent().find(".repl-checkbox").prop("disabled", false);
                        $(event.currentTarget).parent().parent().find(".repl-checkbox").prop("checked", false);
//                        if(this.toggleclass == "li-span-type"){
//                            $(event.currentTarget).parent().parent().find(".repl-checkbox").prop("disabled", true);
//                        }
                        $(event.currentTarget).parent().parent().siblings('#mobile-short-div').find(".pu-type").text("L");

                }
                if(this.parent.partsChangeMode){
                    if ($(window).width() < 480) {
                        $(event.currentTarget).parent().parent().parent().addClass('part-updated');
                    }
                    $(event.currentTarget).parent().parent().addClass('part-updated');
                }
                this.updateJSON();
                } 
            }
    	},
        
  //function to return lot no as empty
        getLotValFl :function(lotno,partprod,partmat,prodfamily,callback){
            var that = this;
            if((lotno=='') && ( (partprod=="4030") && ((partmat) == "100845") || ((partmat) == "100844"))) { //4030-Sterile,100845-Tissue,100844-Biologics
                lotno = " ";
            }else if(lotno=='' && this.usagelotfl=='Y'){
                lotno = " ";
            }else if((lotno=='') && (partprod=="4030") && (prodfamily == "4050") && (that.parent.JSONData.accDivCompanyFl== "Y")){
                lotno = " ";
            }
            callback(lotno);
        },
		
		//Updates the JSON based on the current value in the screen
		updateJSON :function(e){
            var that=this;
            var currSymbol = localStorage.getItem("cmpcurrsmb");
			if(this.parent.signatureMode == true) {
				this.parent.signatureMode = false;
				this.parent.signaturesubmit = false;
				this.parent.signaturesrc = "";
				$(".img-do-sign").each(function() {
					$(this).attr("src","");
				});
			}
//			 this.parent.JSONParts = [];
			if(!this.parent.partsChangeMode)
                this.parent.JSONParts = [];
			var MainJSONData = new Array();
			var AttrJSON = new Array();
			var that = this;
			var id,name,qty,type,unitprice, price, parttype, replcheck, lotno, pfamily, lotimg ,usedFl,pmaterial,expiryDate,validCldLotfl,pclass;
            
			this.$("#div-do-parts-list").find(".ul-parts-used-list").each(function(index) {
				id = $(this).attr('id').replace("-d","");
				name = $(this).find(".li-do-parts-used-list").text();
				qty = $(this).find(".div-do-keypad-values").text().trim();
				type = $(this).find(".input-checked").text();
				parttype = $(this).find(".input-checked").attr("value");
				unitprice  = $(this).find("#div-price-ea-box").attr("value");
				price = $(this).find("#div-ext-price-box").attr("value");
				replcheck = $(this).find(".repl-checkbox").prop("checked");
				lotimg = $(this).find(".li-img-lot-no").find("img").attr("src");
                pfamily = $(this).attr("productfamily");
                pmaterial = $(this).attr("productmaterial");
                usedFl = $(this).attr("usedFl");
                lotno = $(this).find(".txt-lot-no-box").val();
                expiryDate = $(this).find("#div-part-expiry-date").text().trim();
                validCldLotfl = $(this).find(".li-lot-no-textbox .txt-lot-no-box").attr("validcloudlotfl");
                pclass = $(this).attr("productclass");
					//alert('pfamily---' + pfamily);
                that.getLotValFl(lotno,$(this).attr("productclass"),$(this).attr("productmaterial"),$(this).attr("productfamily"),function (data) {
                        lotno = data;
                });
                
				
                console.log(JSON.stringify(that.parent.JSONData.arrDORecordPartVO));
				
				if(!that.parent.partsChangeMode) { 
					if($(this).find(".li-rep-checkbox").find('input:checked').val()) {
						AttrJSON.push({ "itemordattrid":"", "pnum" : id,"qty" : qty, "attrvalue": "Y", "attrtype":"101732", "voidfl" : "", "indexValue" : index.toString()});
					}

					if($(this).find(".txt-lot-no-box").css("display")!="none" || that.usagelotfl=='Y') {
						AttrJSON.push({ "itemordattrid":"", "pnum" : id,"qty" : qty, "attrvalue": $(this).find(".txt-lot-no-box").val(), "attrtype":"101723", "voidfl" : "", "indexValue" : index.toString()});
					}

					that.parent.JSONParts.push({"ID" :id, "Name" : name, "Qty" : qty, "type" : type, "repl" : replcheck, "lotno" : lotno, "lotimg" : lotimg, "unitprice" : unitprice, "price" : price, "pfamily": pfamily,"pmaterial": pmaterial,"usedFl": usedFl,  "indexValue" : index.toString(), "expiryDt" : expiryDate, "validCloudLotfl" : validCldLotfl,"pclass": pclass});
					MainJSONData.push({"pnum" : id, "qty" : qty, "price" : unitprice, "type" : "", "parttype" : parttype,"usedFl": usedFl, "indexValue" : index.toString() });

					
					that.parent.JSONData.arrGmOrderItemVO = MainJSONData;
					that.parent.JSONData.arrGmOrderItemAttrVO = AttrJSON;

					$(this).attr('indexValue', index);
				}

				else {
					if($(this).hasClass('part-updated')) {
						var indexValue = $(this).attr('indexValue');
						for(var i=0;i<that.parent.JSONParts.length;i++) {
							if(parseInt(that.parent.JSONParts[i].indexValue) == parseInt(indexValue)) {
								
								that.parent.JSONParts[i].ID = id;
								that.parent.JSONParts[i].Name = name;
								that.parent.JSONParts[i].Qty = qty;
								that.parent.JSONParts[i].type = type;
								that.parent.JSONParts[i].repl = replcheck;
								that.parent.JSONParts[i].unitprice = unitprice;
								that.parent.JSONParts[i].price = price;
								that.parent.JSONParts[i].lotno = lotno;
								that.parent.JSONParts[i].lotimg = lotimg;
                                that.parent.JSONParts[i].pfamily = pfamily;
                                that.parent.JSONParts[i].pmaterial = pmaterial; 
                                that.parent.JSONParts[i].usedFl = usedFl;
                                that.parent.JSONParts[i].validCloudLotfl = validCldLotfl;
                                that.parent.JSONParts[i].expiryDt = expiryDate;
                                that.parent.JSONParts[i].pclass = pclass;
                                
								
							}
						}
                        

						for(var i=0;i<that.parent.JSONData.arrGmOrderItemVO.length;i++) {
							if(parseInt(that.parent.JSONData.arrGmOrderItemVO[i].indexValue) == parseInt(indexValue)) {
								that.parent.JSONData.arrGmOrderItemVO[i].pnum = id;
								that.parent.JSONData.arrGmOrderItemVO[i].qty = qty;
								that.parent.JSONData.arrGmOrderItemVO[i].price = unitprice;
								that.parent.JSONData.arrGmOrderItemVO[i].parttype = parttype;
                                that.parent.JSONData.arrGmOrderItemVO[i].usedFl = usedFl;
							}
						}


						for(var i=0;i<that.parent.JSONData.arrGmOrderItemVO.length;i++) {
							if(parseInt(that.parent.JSONData.arrGmOrderItemVO[i].indexValue) == parseInt(indexValue)) {
								that.parent.JSONData.arrGmOrderItemVO[i].pnum = id;
								that.parent.JSONData.arrGmOrderItemVO[i].qty = qty;
								that.parent.JSONData.arrGmOrderItemVO[i].price = unitprice;
								that.parent.JSONData.arrGmOrderItemVO[i].parttype = parttype;
                                that.parent.JSONData.arrGmOrderItemVO[i].usedFl = usedFl;
							}
						}


						if(_.contains( _.pluck(that.parent.JSONData.arrGmOrderItemAttrVO,'indexValue'), indexValue )) {
							var len = that.parent.JSONData.arrGmOrderItemAttrVO.length;
							for(var i=0;i<len;i++) {
								if((that.parent.JSONData.arrGmOrderItemAttrVO[i].indexValue == indexValue) && (that.parent.JSONData.arrGmOrderItemAttrVO[i].attrtype == "101732")) {
									if($(this).find(".li-rep-checkbox").find('input:checked').val()) 
										that.parent.JSONData.arrGmOrderItemAttrVO[i].attrvalue = 'Y';
									else
										that.parent.JSONData.arrGmOrderItemAttrVO.splice(i,1);
								}
								else if((that.parent.JSONData.arrGmOrderItemAttrVO[i].indexValue == indexValue) && (that.parent.JSONData.arrGmOrderItemAttrVO[i].attrtype == "101723")) {
									if(($(this).find(".txt-lot-no-box").css("display")!="none") || that.usagelotfl=='Y')
										that.parent.JSONData.arrGmOrderItemAttrVO[i].attrvalue = $(this).find(".txt-lot-no-box").val();
									else
										that.parent.JSONData.arrGmOrderItemAttrVO.splice(i,1);
								}  
																			
							}
						}
						else {
							if($(this).find(".li-rep-checkbox").find('input:checked').val()) {
								that.parent.JSONData.arrGmOrderItemAttrVO.push({ "itemordattrid":"", "pnum" : id,"qty" : qty, "attrvalue": "Y", "attrtype":"101732", "voidfl" : "", "indexValue" : index.toString()});
							}

							if($(this).find(".txt-lot-no-box").css("display")!="none" || that.usagelotfl=='Y') {
								that.parent.JSONData.arrGmOrderItemAttrVO.push({ "itemordattrid":"", "pnum" : id,"qty" : qty, "attrvalue": $(this).find(".txt-lot-no-box").val(), "attrtype":"101723", "voidfl" : "", "indexValue" : index.toString()});
							}
						}

					}
					else if(($(this).attr('indexValue')==undefined) || ($(this).attr('indexValue')=="") ) {
						var newIndex;
						if(that.parent.JSONParts.length>0)
							newIndex = parseInt(_.max(_.pluck(that.parent.JSONParts,'indexValue'), function(val){ return parseInt(val); })) + 1;
						else
							newIndex = 0;
						if($(this).find(".li-rep-checkbox").find('input:checked').val()) {
							that.parent.JSONData.arrGmOrderItemAttrVO.push({ "itemordattrid":"", "pnum" : id,"qty" : qty, "attrvalue": "Y", "attrtype":"101732", "voidfl" : "", "indexValue" : newIndex.toString()});
						}

						if($(this).find(".txt-lot-no-box").css("display")!="none" || that.usagelotfl=='Y') {
							that.parent.JSONData.arrGmOrderItemAttrVO.push({ "itemordattrid":"", "pnum" : id,"qty" : qty, "attrvalue": $(this).find(".txt-lot-no-box").val(), "attrtype":"101723", "voidfl" : "", "indexValue" : newIndex.toString()});
						}

						that.parent.JSONParts.splice(index,0,{"ID" :id, "Name" : name, "Qty" : qty, "type" : type, "repl" : replcheck, "lotno" : lotno, "lotimg" : lotimg, "unitprice" : unitprice, "price" : price, "pfamily": pfamily,"pmaterial": pmaterial,"usedFl": usedFl, "indexValue" : newIndex.toString(),"expiryDt" : expiryDate, "validCloudLotfl" : validCldLotfl,"pclass": pclass});
						that.parent.JSONData.arrGmOrderItemVO.splice(index,0,{"pnum" : id, "qty" : qty, "price" : unitprice, "type" : "", "parttype" : parttype,"usedFl" : usedFl, "indexValue" : newIndex.toString() });

						$(this).attr('indexValue', newIndex);
					}
					
				}
                          
			});
			this.$('.part-updated').removeClass('part-updated');
			that.parent.JSONData.total = parseFloat($("#div-do-parts-footer-title").find("span").text().replace(",","").replace(currSymbol,"")).toString();
			this.parent.saveDO();
            
            console.log("Test>>> " + JSON.stringify(this.parent.JSONParts));
			
		},
       //based on the order type set default Part Type 
        fnordertypecheck:function(e){
            var that = this;
            if (that.parent.JSONData.ordertype != '') {
                if (that.parent.JSONData.ordertype == "2530") {
                    that.$(".ul-parts-used-list").each(function () {
                        $(this).find("#lbl-loaner").addClass("input-checked");
                        $(this).find("#lbl-consignment").removeClass("input-checked");
                        $(this).find(".repl-checkbox").prop("disabled", true);

                    });
                } else if (that.parent.JSONData.ordertype == "2532") {
                    that.$(".ul-parts-used-list").each(function () {
                        $(this).find("#lbl-loaner").removeClass("input-checked");
                        $(this).find("#lbl-consignment").addClass("input-checked");
                        $(this).find(".repl-checkbox").prop("disabled", true);

                    });
                }
            }
            
            if(localStorage.getItem("cmpid") == '1009' || localStorage.getItem("cmpid") == '1011'){
               that.$(".ul-parts-used-list").each(function () {
                    $(this).find("#lbl-loaner").removeClass("input-checked");
                    $(this).find("#lbl-consignment").addClass("input-checked");
                    $(this).find(".repl-checkbox").prop("disabled", true);
                });
            }
            else{
                if(localStorage.getItem("cmpid") != '1026')
                    that.setDefaultLoanerPart();
            }
            
            
               
            },
        //PC-4476 - To Show help icon Popup
            showPartHelpPopup: function () {
                var that = this;
                showPopup();
                $("#div-crm-overlay-content-container").addClass("borrowTagRptPopup");
                $("#div-crm-overlay-content-container.borrowTagRptPopup .modal-body").html(this.template_PartFromBarcodeHelp());
                $(".borrowTagRptPopup .modal-title").text('Scanning Support Label');
                $(".borrowTagRptPopup .modal-footer").addClass("hide");
            
                //close Popup remove class
                $(".modal-header .close").on('click', function () {
                    $(".borrowTagRptPopup .modal-title").text('');
                    $("#div-crm-overlay-content-container").removeClass("borrowTagRptPopup");
                });
            },
        
        //based on part product family(4000389 - Non Inventory Items,26240096 Usage Code,26240399 Construct Code,26240315 Non Stock), set the Loaner part type by default
        setDefaultLoanerPart: function () {
            var that = this;
            that.$(".ul-parts-used-list").each(function () {
                if ($(this).attr("productfamily") == "4000389" || $(this).attr("productfamily") == "26240096" || $(this).attr("productfamily") == "26240399" || $(this).attr("productfamily") == "26240315") {
                    $(this).find("#lbl-consignment").removeClass("input-checked");
                    $(this).find("#lbl-loaner").addClass("input-checked");
                    $(this).find("#lbl-consignment").css("pointer-events", "none");
                    $(this).find(".repl-checkbox").prop("disabled", false);
                }
            });
        }
      
		
	});	

	return DOPartsUsedView;
});