/**********************************************************************************
 * File:        GmDONPIDetailView.js
 * Description: 
 * Version:     1.0
 * Author:     	karthik
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone', 'handlebars',
        'global', 'device', 'notification', 'searchview', 'dropdownview', 'loaderview', 'imagecropview', 'daodo', 'saleitemmodel', 'saleitemcollection', 'saleitemlistview'
        ],

    function ($, _, Backbone, Handlebars, Global, Device, Notification, SearchView, DropDownView, LoaderView, CropView, DAODO, SaleItem, SaleItems, SaleItemListView) {

        'use strict';
        var GmDONPIDetailView = Backbone.View.extend({

            events: {
                "keyup #txt-keyword": "displayAccounts",
                "click #btn-load-new-npi": "addnewnpi",
                "click .close-sm": "removeNPI",
                "keyup #npiNum input": "searchKey",
                "keyup #surgeonName input": "searchKeyname",
                "click #span-npi-txt": "seperateNPIDtls",
                "click #li-npi-detail": "loadPreviousNPIDetail",
            },
            /* Load the templates 
             */
            template: fnGetTemplate(URL_DO_Template, "GmDONPIDetail"),
            template_npiprevsurgeoninfo: fnGetTemplate(URL_DO_Template, "GmDONPIPrevSurgeonInfo"),

            /*Initialize the function */
            initialize: function (options) {
                this.parent = options.parent;
                this.npiidstring = "";
                this.render(options);                
            },

            /*Render  function:Load the NPI and Surgeon Name Dropdowns 
             */
            render: function (options) {
                var that = this;
                this.$el.html(this.template());
                if (that.parent.JSONData.arrDoNPIDetailVO != "") {
                    _.each(that.parent.JSONData.arrDoNPIDetailVO, function (data) {
                        var npiid = data.id;
                        var npiname = data.name;
                        var addhyphen = "";
                        if (npiid == "0" || npiid == undefined) {
                            npiid = "";
                        }
                        if (npiname == undefined) {
                            npiname = "";
                        }
                        if (isNaN(npiid)) {
                            npiid = "";
                         }
                        if (npiid != "" && npiname != "") {
                            addhyphen = " - ";
                        }
                         var npikey = npiid;
                        if (npiid == ""){
                            npikey = npiname;
                        }
                        that.npiidstring = that.npiidstring + "," + npikey + ","; 
                        /** code to render the NPI id and  surgeon name from Local table**/
                        $("#selectedData").append("<li class=\"do-npi-list-details\" id=\"do-npi-" + npiid + "\" value =" + npiid + "><div> <span id='span-npi-txt'>" + npiid + addhyphen + npiname + "&nbsp;&nbsp;&nbsp;&nbsp;</span><span class=\"close-sm\">&times;</span></div></li>");
                        $("#do-npi-" + npiid).attr("name", npiname);
                                              
                    });
                }

                /*fnGetPreviousNPISurgeonDetails(): Load the Previous NPI and Surgeon Detail based on the account and Rep id
                 */
                fnGetPreviousNPISurgeonDetails(that.parent.JSONData.acctid, that.parent.JSONData.repid, function (data) {
                    console.log(data);
                    if (data != "")
                        that.$("#do-npi-Prev-surgeon-detail").html(that.template_npiprevsurgeoninfo(data));
                });


                /*NPI and Surgeon Name Search
                 */
                var renderOptions = {
                    "el": this.$('#npiNum'),
                    "ID": "txt-billing-account",
                    "Name": "txt-search-name",
                    "Class": "txt-keyword-account",
                    "minChar": "3",
                    "autoSearch": true,
                    "placeholder": lblNm("msg_character_search"),
                    "fnName": fnGetNPIIdList,
                    "usage": 1,
                    "template": 'GmSearchResult',
                    "template_URL": URL_Common_Template,
                    "callback": function (rowValue) {
                        that.getSurgeonNmFromSearchView(rowValue);
                    }

                };
                var searchview = new SearchView(renderOptions);

                var renderOptions = {
                    "el": this.$('#surgeonName'),
                    "ID": "txt-billing-account",
                    "Name": "txt-search-name",
                    "Class": "txt-keyword-account",
                    "minChar": "3",
                    "autoSearch": true,
                    "placeholder": lblNm("msg_character_search"),
                    "fnName": fnGetSurgNameList,
                    "usage": 1,
                    "template": 'GmSearchResult',
                    "template_URL": URL_Common_Template,
                    "callback": function (rowValue) {
                        that.getNPIIdFromSearchView(rowValue);
                    }

                };
                var searchview = new SearchView(renderOptions);
                return this;
            },

            /*searchKey(): while searching the NPI in NPI search box ,Get the vaulue and set the data to GLobal Variable
             */
            searchKey: function (e) {
                var that = this;
                  $('#npiNum #div-search-result-main').removeClass('hide');
                    $('#npiNum #div-keyword-search #div-common-search-x').removeClass('hide');
                var searchData = $(e.currentTarget).val();
                GM.Global.NPIID = searchData;
                var npiInp = $("#surgeonName").find('input').val();
                var nameInp = $("#npiNum").find('input').val();
                if (npiInp != "" || nameInp != "") {
                    $(".btn-signs").removeClass("hide");
                        var re = /[^0-9]/g;
                        var error = re.test(GM.Global.NPIID);
                        if (error == true) {  /*Accept only Digits in NPI# field*/
                            showError("msg_npi_error");
                            $(".btn-signs").addClass("hide"); 
                        }
                    
                } else {
                    $(".btn-signs").addClass("hide");
                }
            },

            /*searchKeyname(): while searching the Surgeon name in Surgeon name search box ,Get the vaulue and set the data to GLobal Variable
             */
            searchKeyname: function (e) {
                var that = this;
                $('#surgeonName #div-keyword-search #div-common-search-x').removeClass('hide');
                    $('#surgeonName #div-search-result-main').removeClass('hide');
                var searchData = $(e.currentTarget).val();
                GM.Global.NPINAME = searchData;
                var npiInp = $("#surgeonName").find('input').val();
                var nameInp = $("#npiNum").find('input').val();
                if (npiInp != "" || nameInp != "") {
                    $(".btn-signs").removeClass("hide");
                        var re = /^[a-zA-Z\. ]*$/;
                        var error = re.test(GM.Global.NPINAME);
                        if (error == false) {  /*Accept only Characters,Space,dot in Surgeon name field*/
                            showError(lblNm("msg_surgeon_name_letter"));
                            $(".btn-signs").addClass("hide"); 
                        }
                    
                } else {
                    $(".btn-signs").addClass("hide");
                }
            },

            /*addnewnpi(): Add the NEW NPI and SurgeonName and append the selected data in the List
             */
            addnewnpi: function (event) {
                var that = this;
                var rowEl = $("<a id=" + GM.Global.NPIID + " class=item-searchlist name=" + GM.Global.NPINAME + "><div class='div-data hide'></div> <i class='fa fa-check'></i></a>");
                var CurrentRow = {
                    "ID": GM.Global.NPIID,
                    "Name": GM.Global.NPINAME,
                    "Element": rowEl
                };
                /*Once the new details are entered then need to remove the searched datas in the input box*/
                var currElem = $(event.currentTarget).parent().find('input');
                $(currElem).val("");
                $(currElem).trigger("keyup");
                $(event.currentTarget).find('input').addClass('hide');
                 $('#npiNum #div-keyword-search #div-common-search-x').addClass('hide');
                $('#surgeonName #div-keyword-search #div-common-search-x').addClass('hide');
                that.loadNPIDetails(CurrentRow);
            },

            loadPreviousNPIDetail: function (e) {

                var that = this;
                var npiid = $(e.currentTarget).find("#div-npiid").text();
                var npiname = $(e.currentTarget).find("#div-surgeonname").text();
                var rowEl = $("<a id=" + npiid + " class=item-searchlist name=" + npiname + "><div class='div-data hide'></div> <i class='fa fa-check'></i></a>");
                var CurrentRow = {
                    "ID": npiid,
                    "Name": npiname,
                    "FromPreviousNPI": "loadPreviousNPIDetail",
                    "Element": rowEl
                };
                that.loadNPIDetails(CurrentRow);

            },
            /*removeNPI(): Remove the Selected NPI and SurgeonName from the list and set the currect data's in the JSON then save it to Local table
             */
            removeNPI: function (event) {
               var that = this;
               var currTarget = $(event.currentTarget).parent().parent();
               currTarget.remove();
               var currVal = currTarget.attr("value");
               var currValName = currTarget.find("#span-npi-txt").text().trim();
               var npikey = currVal;
                if (currVal == undefined || currVal == "") {
                      npikey = currValName;
                 }
               this.parent.JSONData.arrDoNPIDetailVO = _.filter(this.parent.JSONData.arrDoNPIDetailVO, function (data) {
                   return data.id != npikey;
               });
               if (this.npiidstring != "") {
                   this.npiidstring = this.npiidstring.replace("," + npikey + ",", "");
               }

               if ((this.parent.JSONData.orderid).replace("--", "") != "") {
                   this.parent.saveDO();
               }

           },

            /*getNPIDetailsFromSearchViewrchKeyname(): NPI/Surgeon dropdown
             */
            getNPIDetailsFromSearchView: function (rowValue) {
                var that = this;
                that.loadNPIDetails(rowValue);
            },

            /*getSurgeonNmFromSearchView(): Display Surgeon Name while selecting NPI Id
             */
            getSurgeonNmFromSearchView: function (rowValue) {
                var npiId = rowValue.Name;
                var surgeonNm = rowValue.ID;
                $("#surgeonName").find('input').val(surgeonNm);
                $("#npiNum").find('input').val(npiId);
                GM.Global.NPIID = npiId;
                GM.Global.NPINAME = surgeonNm;
                $("#btn-load-new-npi").trigger("click");
            },

            /*getNPIIdFromSearchView(): Display NPI Id while selecting Surgeon Name
             */
            getNPIIdFromSearchView: function (rowValue) {
                var npiId = rowValue.Name;
                var surgeonNm = rowValue.ID;
                $("#npiNum").find('input').val(surgeonNm);
                $("#surgeonName").find('input').val(npiId);
                GM.Global.NPIID = surgeonNm;
                GM.Global.NPINAME = npiId;
                $("#btn-load-new-npi").trigger("click");
            },

            /*loadNPIDetails(): Load the NPI details from Local Table and append the selected data in the List
             */
            loadNPIDetails: function (rowValue) {
                var that = this;
                /* Setting the value to lable which is display as none so that we can take the NPI and surgeon value from that label */
                $('#npi').html(rowValue.ID);
                $('#surgeon').html(rowValue.Name);
                var npiid = $('#npi').html();
                var npiname = $('#surgeon').html();
                
                        var addhyphen = "";
                        //npiname = npiname.replace(/[0-9]/g, ''); // this replaces all numerical characters in the string with nothing.              
                        if (npiid == "0" || npiid == undefined) {
                            npiid = "";
                        }
                        if (npiname == undefined) {
                            npiname = "";
                        }
                        var npikey = npiid;
                        if (npiid == "") {
                            npikey = npiname;
                        }
                        if (this.npiidstring.indexOf("," + npikey + ",") == -1) {
                            this.npiidstring = this.npiidstring + "," + npikey + ",";
                            if (npiid != "" && npiname != "") {
                                addhyphen = " - ";
                            }
                            var divCount = $(".do-npi-list-details").length;
                            $("#selectedData").append("<li class=\"do-npi-list-details\" id=\"do-npi-" + $('#npi').html() + "\" value =" + npiid + " ><div><span id='span-npi-txt'>" + npiid + addhyphen + npiname + "&nbsp;&nbsp;&nbsp;&nbsp;</span><span class=\"close-sm\">&times;</span></div></li>");

                            $("#do-npi-" + npiid).attr("name", npiname);
                            $("#npiNum").find('input').val('');
                            $("#surgeonName").find('input').val('');
                            if (rowValue.FromPreviousNPI != 'loadPreviousNPIDetail') {
                                $("#npiNum").find('input').focus();
                            }
                            this.updateJSON(rowValue);
                        }
                  

            },

            /* Updates the JSON based on the current value in the screen 
             */
            updateJSON: function (rowValue) {

                var that = this;
                var npiid;
                var ID;
                var npiName;
                var index;

                if ($("#div-do-billing-acid").text().length != "0") {
                    ID = rowValue.ID;
                    npiid = $("#do-npi-" + ID).attr('value');
                    npiName = $("#do-npi-" + ID).attr('name');

                    if (npiid == "0" || npiid == undefined) {
                        npiid = "";
                    }
                    if (npiName == undefined) {
                        npiName = rowValue.Name;
                    }
                    if(npiid == ""){
                        npiid = npiName;
                    }

                    that.parent.JSONData.arrDoNPIDetailVO = _.filter(that.parent.JSONData.arrDoNPIDetailVO, function (data) {
                        return data.ID != $("#do-npi-" + ID).val();
                    });
                    that.parent.JSONData.arrDoNPIDetailVO.push({
                        "id": npiid,
                        "name": npiName
                    });
                    if ((that.parent.JSONData.orderid).replace("--", "") != "")
                        that.parent.saveDO();
                }
                GM.Global.NPIID = "";
                GM.Global.NPINAME = "";
                GM.Global.OLDNPIID = "";
            },
            /*seperateNPI(): Seperate NPI# and Surgeon Name to the corresponding fields while clicking on the selected value
             */
            seperateNPIDtls: function (event) {
                var that = this;

                var npiId = $(event.currentTarget).parent().parent().attr("value");
                var surgeonnm = $(event.currentTarget).parent().parent().attr("name");
                $("#npiNum").find('input').val(npiId);
                $("#surgeonName").find('input').val(surgeonnm);
                GM.Global.OLDNPIID = npiId;
                this.removeNPI(event);
                GM.Global.NPIID = npiId;
                GM.Global.NPINAME = surgeonnm;
                if (GM.Global.NPIID != "" || GM.Global.NPINAME != "") {
                    $(".btn-signs").removeClass("hide");
                } else {
                    $(".btn-signs").addClass("hide");
                }
            },

        });

        return GmDONPIDetailView;
    });