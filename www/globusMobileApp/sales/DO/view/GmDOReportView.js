/**********************************************************************************
 * File:        GmDOReportView.js
 * Description: 
 * Version:     1.0
 * Author:     	tvenkateswarlu
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification','datasync','saleitemmodel', 'saleitemcollection', 'saleitemlistview','itemview','searchview','jqueryui','dropdownview', 'numberkeypadview','dorecordtagssearchview','loaderview'
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification,DataSync, SaleItem, SaleItems, SaleItemListView,ItemView,SearchView,JqueryUI,DropDownView,NumberKeypadView,DoRecordTagsSearchView,LoaderView) {

	'use strict';
	var DOReportView = Backbone.View.extend({

		events : {
			"click #li-do-report-from-date label,#li-do-report-from-date i,#li-do-report-to-date label,#li-do-report-to-date i" : "showDatepicker",
			"change #txt-from-date ,#txt-to-date" : "getReportDate",
		   	 "click #div-do-report-load":"loadreport",
		     "click .div-do-report-doid":"openDO",
			"click .fa-file-pdf-o" : "showPDFOptions",
            "click .div-online-pdf" : "viewDocument",
            "click .div-do-report-edit" : "openEditTagReport"
            
           
		},
		/* Load the templates */ 
		template: fnGetTemplate(URL_DO_Template, "GmDOReport"),
		
		tplSpinner: fnGetTemplate(URL_Common_Template, "GmProgressSpinner"),
		
		template_doreport: fnGetTemplate(URL_DO_Template, "GmDOReportList"),
		
		template_date: fnGetTemplate(URL_DO_Template, "GmDODateFormat"),
        
        template_edit_record_tags: fnGetTemplate(URL_DO_Template, "GmDOEditRecordTags"),
        
        template_edit_recordtagdetails: fnGetTemplate(URL_DO_Template, "GmDOEditRecordTagDetails"),

        template_edit_recordtagdetailsMobile: fnGetTemplate(URL_DO_Template, "GmDOEditRecordTagDetailsMobile"),
        
        template_edit_tag_Images_list: fnGetTemplate(URL_DO_Template, "GmDOEditRecTagReadTextFromImg"),
        
        template_edit_tag_Images_listMobile: fnGetTemplate(URL_DO_Template, "GmDOEditRecTagReadTextFromImgMobile"),
        
        template_edit_rec_tag_Favorites: fnGetTemplate(URL_DO_Template, "GmDOEditRecScreenTagFavorites"),

		initialize: function(options) {
            var that = this;
			this.parent = options.parent;
			this.render(options);
            this.arrDORecordTagDtls =[];
            this.selectedOrderId = "";
            this.arrDORecTagFrmImgVO = [];
            this.DOFavoriteDocumentID = '';
            this.DOFavoriteMultipleDocumentID = [];  
            
                        /* Get the mapped Sales Rep details for the associate rep based on his user id
               for the Favorites
               Author: Karthik Somanathan    
             */
			var partyid = window.localStorage.getItem("partyID");
			fnGetUserSalesRep(partyid,function(result){
				fnGetAssocSalesRep(result[0].REPID, function  (data) {
					if(data.length>0){
                        //Get Rep Info for the Associate Sales Rep
						that.DOFavoriteMultipleDocumentID = data;
                    }else{
                        //Get logged in Rep Info
                        that.DOFavoriteMultipleDocumentID = result;
                    }
				});
			});
            
            fnGetLocalFileSystemPath(function (localpath) {
                    that.localpath = localpath;
                    //that.localpath ="";
            });
		},

		// Render the initial contents when the Catalog View is instantiated
		render: function (options) {
//			showMsgView("011");
			this.$el.html(this.template());
			var that = this;
			var renderOptions = {
					"el":this.$('#div-do-report-account-search'),
					"ID" : "txt-billing-account",
            		"Name" : "txt-search-name",
            		"Class" : "txt-keyword-account",
                    "minChar" : minKeyinChar,
            		"autoSearch": true,
            		"placeholder": lblNm("msg_character_search"),
            		"fnName": fnGetAccounts,
            		"usage": 1,
            		"template" : 'GmSearchResult',
					"template_URL" : URL_Common_Template,
					"callback": function(rowValue) {that.getAccountFromSearchView(rowValue);}
            		
            	};

			  this.searchview = new SearchView(renderOptions);
            this.showCompany(); 
			var todaydate = new Date();
			var that = this;
			var fdate = this.fnGetFormatDate(todaydate);
			this.$("#label-do-report-from-date").html(fdate);
			this.$("#span-do-report-from-date").html(this.template_date({"date":fdate}));
			this.$("#label-do-report-to-date").html(fdate);
			this.$("#span-do-report-to-date").html(this.template_date({"date":fdate}));
			var doid = $("#li-do-report-do-id input").val();
			this.showDOReport(fdate,fdate,doid,"","","");
            
             if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function (position) {
                            var latitude = position.coords.latitude;
                            var longitude = position.coords.longitude;
                            console.log("latitude Record " + position.coords.latitude);
                            console.log("longitude Record" + position.coords.longitude);
                            GM.Global.latitude = latitude;
                            GM.Global.longitude = longitude;
                        });
                    }

			return this;
		},
		//Initailize Dropdown view for Company
		showCompany: function() {
			var that = this;
			var comapanyData = {"Company" : [{"ID":"100800","Name":"Globus Medical Inc"},{"ID":"100801","Name":"Algea Therapies"}]};
			
			this.companyDrpdownView = new DropDownView({
				el : this.$("#drp-do-report-company"),
				data : comapanyData.Company,
				DefaultID : 100800
			});

			this.$("#drp-do-report-company").bind('onchange', function (e, selectedId) {
					that.companyid = selectedId;
			});

		},
		
		getReportDate: function (event) {
			var elem = event.currentTarget;
			this.$(elem).parent().find("label").html($(elem).val());
			this.$(elem).parent().find("span").html(this.template_date({"date":$(elem).val()}));
		},
         
        viewDocument: function (event) {
            
         var fileID = $(event.currentTarget).attr("name");
         var companyLocale = localStorage.getItem("compcd");
         var uploadDir = localStorage.getItem("pdffilepath");
            
         /* Need to open the online PDF from FileInputStream
          * Get the FileID [order id ] , Company locale, uploadDir [Constant properties key] and pass to the below servlet
          * it will open the pdf from server */
            var toEncode = "/GmCommonFileOpenServlet?strOpt=IPADORDERS&compLocale=" + companyLocale + "&uploadString=" + uploadDir + "&fileName=" + fileID + ".pdf"
            
            var URL = URL_Portal + "GmLogonServlet?token=" + localStorage.getItem("token") + "&return_to=" + encodeURIComponent(toEncode) + "&randomid=" + Math.random();
            
            console.log(URL);
        if (URL != undefined) 
            window.open(URL, '_blank', 'location=no');
        },
        
		loadreport:function(event){	
			var fromdate,todate,doid,surgeonName,lotNo;
			 fromdate = this.$("#label-do-report-from-date").text();
			 todate   = this.$("#label-do-report-to-date").text();
			 doid = $("#li-do-report-do-id input").val();
            /*
               This is used for DO report based surgeon and Lot number
            */
             lotNo = $("#li-do-report-lot-id input").val();
             surgeonName = $("#li-do-report-surgeon-id input").val();
			 this.$("#div-search-result-main").hide();
			 this.showDOReport(fromdate,todate,doid,this.accid,lotNo,surgeonName);	
			 console.log(this.accid);
		},
		getAccountFromSearchView: function(rowValue){
			console.log(rowValue);
			$("#div-itemlist").hide();
			$("#txt-billing-account").val(rowValue.Name);	
			this.accid = rowValue.ID;
		},
		
	     parseDate:function(str) {
		    var mdy = str.split('/')
		    return new Date(mdy[2], mdy[0]-1, mdy[1]);
		},

		 daydiff:function(first, second) {
		    return (second-first)/(1000*60*60*24)
		},
		openDO:function(event){
			var elem = event.currentTarget;
			if($(elem).text()!="0") {
					if($("#div-dash-drilldown-sortby-header").css("display") != "none" ) {
						event.stopImmediatePropagation();
						var that = this, id;
//						if($(event.currentTarget).attr("data-url")!="") 
//							id = $(event.currentTarget).attr("data-url");
//						else
//							id = $(event.currentTarget).text();
					    var companyInfo = {};
                        companyInfo.cmpid = localStorage.getItem("cmpid");
                        companyInfo.plantid = localStorage.getItem("plantid");
                        companyInfo.token = "";
                        companyInfo.partyid = localStorage.getItem("partyid");
                        var StringifyCompanyInfo = JSON.stringify(companyInfo);
						var toEncode = "/GmEditOrderServlet?hMode=PrintPrice&hParantForm=SALESDASHBOARD&hOrdId=" + $(elem).text() +"&companyInfo="+StringifyCompanyInfo;
//						if(this.secondParameter!=undefined) {
//							toEncode += "&hSetNm=" + $(event.currentTarget).parent().parent().parent().parent().parent().find(".li-second-param").text();
//						}
						var URL = URL_Portal + "GmLogonServlet?token=" + localStorage.getItem("token") + "&return_to=" + encodeURIComponent(toEncode) + "&randomid=" + Math.random();
						
						console.log(URL);
						var summaryWindow = window.open(URL, '_blank', 'location=no');
						summaryWindow.addEventListener('loadstop', function() {
							summaryWindow.insertCSS({
					            code: "body {font-family: 'Trebuchet MS'!important; -webkit-overflow-scrolling: touch;} a{display: none;} .RightDashBoardHeader {background:#999;} .ShadeRightTableCaption {background:#ccc;} TR.ShadeBlueBk {background:#eee}" + " table { margin:auto!important; } .button-red{ display:none; }"  
					        });
					        summaryWindow.executeScript({
					            code: "var meta = document.createElement('meta'); meta.name = 'format-detection'; meta.content = 'telephone=no'; document.getElementsByTagName('head')[0].appendChild(meta); " + that.scriptOptions
					        });
						});
						summaryWindow.addEventListener('loaderror',function() {
							showNativeAlert(lblNm("msg_error_loading"), lblNm("error"),lblNm("msg_ok"), function(index) {
								summaryWindow.close();
							});
						});
					}
			}
			
			
		},
		showDOReport:function(fromdate,todate,doid,accid,lot,surgeon){
			if((navigator.onLine) && (!intOfflineMode)) {
//				fnShowStatus("");
// PC-4865-do-booked-report-extend-date-range
				if($("#txt-billing-account").val() == "")
					 this.accid ="";
				if(this.daydiff(this.parseDate(fromdate),this.parseDate(todate))> 180)
					{
						fnShowStatus(lblNm("msg_records_exceed_select_range"));
						return;
					}
				this.serverInput = {
					"token": localStorage.getItem("token"),
					"stropt": "DOREPORT",
					"companyid": this.companyid,
					"doid": doid,		
					"accountid": this.accid,
					"fromdate": fromdate,
					"todate": todate,
                    "lotnumber": lot,
                    "surgeonname":surgeon
				};
				this.serverInput.salesdbcurrtype = localStorage.getItem("currtype");
                this.serverInput.salesdbcurrsymbol = localStorage.getItem("defcurrncysymbol");
				console.log(this.serverInput);
				var that = this;
				this.$("#div-do-report-content").hide();
				this.$(".div-do-page-loader").html(this.tplSpinner({"message":lblNm("msg_data_fetching")})).show();
				fnGetWebServiceData("orders/submitted","listGmOrdersDrilldownVO", this.serverInput, function(status,data){
					
				if(status){
					
					that.viewData = new Array();
					console.log(data);
//					if(data.listGmOrdersDrilldownVO != undefined)					
//						that.viewData = data.listGmOrdersDrilldownVO;
//					else
//						that.viewData.push(data.listGmOrdersDrilldownVO);
					if(data.length == undefined)
						that.viewData.push(data);					
					else
						that.viewData = data;
				
						that.$(".div-do-page-loader").html(that.tplSpinner({"message":lblNm("msg_loading_data_c")})).show();
					
					var currSymbol = localStorage.getItem("cmpcurrsmb");
					for(var i=0;i<that.viewData.length;i++) {
						that.viewData[i].total = currSymbol+""+formatCurrency(that.viewData[i].total);
//						that.$("#div-do-report-list").append(that.template_doreport(that.viewData[i]));
						
			     	 }
						
					var items = new SaleItems(data);
                    if ($(window).width() < 480) {
					   that.showItemListView(that.$("#div-do-report-list"), items, "GmDOReportListMobile", URL_DO_Template);
                        //edit tag screen access
                        var moduleaccess = localStorage.getItem("moduleaccess");
                        if(navigator.onLine && moduleaccess != null && moduleaccess.indexOf("div-do-report-edit") == -1){
                            $(".div-do-report-edit").removeClass("hide");
                        }
                       $("#div-saleitemlist li").each(function(){
                           if ($(this).next().is('li')) {
                                $(this).children(".reportListMobile").addClass("showRptLst");
                                $(this).find(".togglebx").children("img").removeClass("onActive");
                            } else {
                                $(this).children(".reportListMobile").removeClass("showRptLst");
                                $(this).find(".togglebx").children("img").addClass("onActive");
                           }
                       });
                       $(".togglebx").unbind('click').bind('click', function () {
                           var pdfclass=$(this).parent().attr('class');
                           if(pdfclass=='div-do-report-list-item reportListMobile showRptLst'){
                               $(this).parent().find('.div-do-report-pdf').css("display", "inline-block"); 
                           }
                           else{
                              $(this).parent().find('.div-do-report-pdf').hide(); 
                           }
                            $(this).parent(".reportListMobile").toggleClass('showRptLst');
                            $(this).children("img").toggleClass("onActive");
                       });
                    }
                    else{
                        that.showItemListView(that.$("#div-do-report-list"), items, "GmDOReportList", URL_DO_Template);	
                        //edit tag screen access
                        var moduleaccess = localStorage.getItem("moduleaccess");
                        if(navigator.onLine && moduleaccess != null && moduleaccess.indexOf("div-do-report-edit") == -1){
                            $(".div-do-report-edit").removeClass("hide");
                        }
                    }
                    
					that.$(".div-do-page-loader").hide();
					that.$("#div-do-report-content").show();

					that.$(".div-do-report-pdf").hide();
					fnGetAllDOIDs(function (DOIDs) {
						var allDOs = _.pluck(DOIDs,'DOID');
						console.log(allDOs)
						that.$(".div-do-report-list-item").each(function  () {
                            console.log(_.contains(allDOs,this.id) + " >>>> " + this.id);
                            //disabled pdf in collapse view for mobile
                            if ($(window).width() < 480) {
                           var pdfclass=this.className;
                            if(pdfclass=='div-do-report-list-item reportListMobile showRptLst'){
                               $(this).find(".div-do-report-pdf").hide();
                            }
                            else if(pdfclass=='div-do-report-list-item reportListMobile'){
								$(this).find(".div-do-report-pdf").css("display", "inline-block"); 
                            }
                            }
                            else{
                               if(_.contains(allDOs,this.id))
								$(this).find(".div-do-report-pdf").css("display", "inline-block"); 
                            }
							
							
						});
					});
					

					
				}
				else
				{
					that.$("#div-do-report-content").hide();
					that.$(".div-do-page-loader").show();
					that.$(".div-do-page-loader").html(lblNm("msg_no_data_display"));			
				}
		
				});
			}
			else
				{
				this.$(".div-detail").hide();
				this.$("#div-do-report-content").hide();
				this.$(".div-do-page-loader").show();
				this.$(".div-do-page-loader").html(" <div> <i class='fa fa-exclamation-triangle fa-lg'></i>"+ lblNm("msg_no_internet_connection")+"</div>"+lblNm("msg_connect_wifi_or_cellular"));
				
				}
			
		},

		fnGetFormatDate:function(selectedDate){
			var fdate = new Date(selectedDate);
			  var smm = ("0" + (fdate.getMonth() + 1)).slice(-2), sdd = ("0" + fdate.getDate()).slice(-2), syy = fdate.getFullYear();					  
			  syy =  syy.toString();
			  var formatDate = smm+"/"+sdd+"/"+syy;
			  return formatDate;			
		},

		showItemListView: function(controlID, collection, itemtemplate, template_URL) {
			
			this.listView = new SaleItemListView(
					{ 
						el: controlID, 
						collection: collection, 
						itemtemplate: itemtemplate,
						template_URL: template_URL,
						columnheader: 0
					}
			);
			return this.listView;
		},

		openPDF: function(event) {
			//var DOId = $(event.currentTarget).parent().parent().find('.div-do-report-doid').text();
            		var DOId = $(event.currentTarget).parent().parent().attr('id');
			var that = this;
			var folder = "WP/";
			fnShowStatus(lblNm("msg_fetching_pdf"));
			fnCheckDOID(DOId, function(data) {
				if(data.length>0) {
					if(data[0].COVERINGREPFL == 'true')
						folder = "WOP/"
					fnGetLocalFileSystemPath(function(URL) {
						URL = URL + 'DO/PDF/' + folder + DOId + '.pdf';
						console.log(URL);
						fnGetImage(URL, function(status) {
//							fnShowStatus('');
							if(status!='error')
								//var DOCViewer = window.open(URL, '_blank', 'location=no');
        						  openPdfFromLocalFileSystem(URL);
							else
								fnShowStatus('PDF is not available for ' + DOId);
						});
					});
				}
				else
					fnShowStatus(lblNm("msg_not_available") + DOId);
				
			});
		},

		emailPDF: function(buttonIndex, event) {
			//var DOId = $(event.currentTarget).parent().parent().find('.div-do-report-doid').text();
			var DOId = $(event.currentTarget).parent().parent().attr('id');
			var that = this;
			GM.Global.DOPDFFOLDER = "DO/PDF/WP/";
			fnShowStatus(lblNm("msg_fetching_pdf"));
			if(buttonIndex == 3)
			 GM.Global.DOPDFFOLDER = "DO/PDF/WOP/";
			fnGetLocalFileSystemPath(function(URL) {
				//URL = URL + 'DO/PDF/' + folder + DOId + '.pdf';
				GM.Global.PDFPATH = URL;
			 	GM.Global.PDFORDERID = DOId;
				var Name = localStorage.getItem('fName') + " " + localStorage.getItem('lName');	
				fnGetImage(URL, function(status) {
//					fnShowStatus('');
					if(status!='error') {
			 			if ($(window).width() < 480) 
	                		sendDOPDFEmailViaMobile(function(callback) {
				 			});
	            		else
	                	 	sendDOPDFEmailViaIpad(function(callback) {
				 			});
					}
					else
						fnShowStatus(lblNm("msg_pdf_not_available") + DOId);
				});
			});
				
		},
		
		faxPDF: function(buttonIndex, event) {
			var that = this;
			//var DOId = $(event.currentTarget).parent().parent().find('.div-do-report-doid').text();
            var DOId = $(event.currentTarget).parent().parent().attr('id');
			var folder = "WP/";
			if(buttonIndex==5)
				folder = "WOP/";
//			$('#overlay-back').fadeIn(100,function(){
				if(that.keypadview)
					that.keypadview.close();
		 		that.keypadview = new NumberKeypadView({
					el : that.$("#div-report-show-num-keypad"),
					callback: function(val) {that.preparePDF(DOId, folder, val);}
				});
		 		if(buttonIndex==5)
		 			$("#div-number-keypad-title").html(lblNm("fax_without_price")+"</div>");
		 		else 
		 			$("#div-number-keypad-title").html(lblNm("fax_with_price")+"</div>");
				$("#div-number-keypad-close").show();
				$("#div-number-decimal").hide();
				$("#div-number-keypad-number-symbol").css("width","69px").css("height","17px").css("margin-left","0");
				$("#div-number-keypad-btn-done").html(lblNm("msg_send_fax"));
//			});
		},
		
		preparePDF: function(DOId, folder, value) {

			var that = this;
			var pdfPath = "";

			fnGetLocalFileSystemPath(function(URL) {
				pdfPath = URL + 'DO/PDF/' + folder + DOId + '.pdf';

				window.resolveLocalFileSystemURL(pdfPath,function(entry){
			            entry.file(function (file) {
			                var reader = new FileReader();
			                reader.onloadend = function(evt) {
			                    console.log(">>>>>>>>>Read as binary<<<<<<<<");
			                    fnSendFax(evt.target.result,value);
			                };
			                reader.readAsArrayBuffer(file);
			            }, function  (argument) {
			                console.log('error file');
				            })
				            

				        },function() {
				            console.log('error getting file');
				        });
			    },null);
		},

		showPDFOptions: function(event) {
			var that = this;
			var options = [lblNm("msg_open"), lblNm("email_with_price"), lblNm("email_without_price"), lblNm("fax_with_price"), lblNm("fax_without_price"), lblNm("msg_close")]
			showNativeConfirm(lblNm("msg_would_like_pdf"),lblNm("pdf"),options,function(buttonIndex) {
				if(buttonIndex==1) {
					that.openPDF(event);
				}
				else if((buttonIndex == 3) || (buttonIndex == 2)) {
					that.emailPDF(buttonIndex,event);
				}
				else if((buttonIndex == 4) || (buttonIndex == 5)) {
					that.faxPDF(buttonIndex,event);
				}
			});
		},

		showDatepicker: function (event) {
			var elem = event.currentTarget;
			var parent = $(elem).parent().attr("id");
			var dptext = $("#"+parent +" input").attr("id");
			$("#"+dptext).datepicker( { maxDate: 0, duration: 'fast'});
			var visible = $('#ui-datepicker-div').is(':visible');
			$("#"+dptext).datepicker(visible? 'hide' : 'show');	
			
			event.stopPropagation();
			$(window).click(function (event) {
			    if(document.getElementById("ui-datepicker-div").style.display != "none") {
			    	
			    	var elem = event.target;
			       if(!($(elem).hasClass("ui-datepicker-prev") || $(elem).hasClass("ui-datepicker-next") || $(elem).parent().hasClass("ui-datepicker-prev") || $(elem).parent().hasClass("ui-datepicker-next"))) {
			    	   var visible = $('#ui-datepicker-div').is(':visible');
			    	   $("#"+dptext).datepicker(visible? 'hide' : 'show');	
//			    	   $('#ui-datepicker-div').hide();
			       }
			       }
			   });
		},
        /*open edit tag screen popup*/
        openEditTagReport: function (event) {

            var that = this;
            var openEditEvent = event;
            that.arrDORecordTagDtls=[];
            that.selectedOrderId = $(event.currentTarget).parent().attr("id");
            that.fnShowTagScreenPopup();
            $("#div-crm-overlay-content-container2 .modal-body").empty();
            $("#div-footer").addClass("hide");
            $("#div-crm-overlay-content-container2 .modal-body").html(that.template_edit_record_tags());
            
                /*Tag Search Filter  */
                var renderOptions = {
                    "el": $('#editDOTagDetails'),
                    "ID": "txt-billing-account",
                    "Name": "txt-search-name",
                    "Class": "txt-keyword-account",
                    "minChar": "3",
                    "autoSearch": true,
                    "placeholder": lblNm("msg_tag_search"),
                    "fnName": fnGetDOTagInfo,
                    "usage": 2,
                    "template": 'GmSearchResult',
                    "template_URL": URL_Common_Template,
                    "callback": function (rowValue) {
                        that.loadTagDetailsInEditRecTags(rowValue);
                    }

                };

                var searchview = new SearchView(renderOptions);

                /* Set ID/Set name  Search Filter  */
                var renderOptions = {
                    "el": $('#editDOSetDetails'),
                    "ID": "txt-billing-account",
                    "Name": "txt-search-name",
                    "Class": "txt-keyword-account",
                    "minChar": "3",
                    "autoSearch": true,
                    "placeholder": lblNm("msg_set_search"),
                    "fnName": fnGetSetsDetails,
                    "usage": 2,
                    "template": 'GmSearchResult',
                    "template_URL": URL_Common_Template,
                    "callback": function (rowValue) {
                        that.loadSetDetailsInEditRecTags(rowValue);
                    }

                };
                var searchview = new SearchView(renderOptions);
            //fetch DO tag details
            fnFetchDOTagDetails(that.selectedOrderId, function (data) {

                that.arrDORecordTagDtls = data;
                $(".ul-do-do-tag-list-header").show();
                $('#div-edit-do-record-tag-list').empty();
                console.log("test-----------" + JSON.stringify(data));
                for (var i = 0; i < data.length; i++) {
                    if ($(window).width() < 480) {
                        $("#div-edit-do-record-tag-list").append(that.template_edit_recordtagdetailsMobile(data[i]));
                        that.comnMobFunc();
                    } else {
                        $("#div-edit-do-record-tag-list").append(that.template_edit_recordtagdetails(data[i]));
                    }
                }
                /*Remove Tags from edit Record Tag screen*/
                $("#div-crm-overlay-content-container2 .modal-body #div-do-edit-record-tag-detail #div-edit-do-record-tag-list .ul-record-tag-list .fa-minus-circle").unbind('click').bind('click', function (event) {

                    that.deleteDOEditRecordTags(event);

                });
                /*Remove Tags from edit Record Tag screen mobile*/
                $("#div-crm-overlay-content-container2 .modal-body #div-do-edit-record-tag-detail #div-edit-do-record-tag-list .ul-parts-used-list-mobile .delete-pu-span .clear-row-pu").unbind('click').bind('click', function (event) {

                    that.deleteDOEditRecordTags(event);

                });



            });
            /*Close DO Edit Record Tags Screen Popup*/
            $("#div-crm-overlay-content-container2 .modal-dialog .close").unbind('click').bind('click', function (event) {
                var newlyAddedRec = _.findWhere(that.arrDORecordTagDtls, {newlyAddedTag: "Y"});
                if (newlyAddedRec != undefined && newlyAddedRec != ""){
                        dhtmlx.confirm({
                            title: "Confirm",
                            ok: "Yes",
                            cancel: "No",
                            type: "confirm",
                            text: "Some tags are newly added.Are you sure do you want to discard changes and close? ",
                            callback: function (result) {
                                if(result)
                                    that.fncloseDOEditRecordTagPopup(event);
                            }
                        });
                    }else{
                        that.fncloseDOEditRecordTagPopup(event);
                    }

                
            });
            /*Save Edited DO Record Tags */
            $("#div-crm-overlay-content-container2 .modal-dialog .btn-submit-edit-do-tags").unbind('click').bind('click', function (event) {
                if(that.arrDORecordTagDtls.length <= 0) {
                    dhtmlx.message({
                        title: "Process Tag",
                        type: "alert",
                        text: "Please add tag to submit",
                         callback: function() {$('.dhtmlx-alert').addClass('hide');}
                    });
                }
                else {
                    $("#div-crm-overlay-content-container2 .modal-footer").removeClass("hide");
                    fnSaveTagsDetailsToDO(that.arrDORecordTagDtls);
                    setTimeout(function () {
                            $(".editBookedDOReorcdTagPopUp .modal-footer").html('Record Saved Successfully...');  
                    }, 3000);
                    setTimeout(function () {
                        $("#div-crm-overlay-content-container2 .modal-footer").addClass("hide");   
                        that.openEditTagReport(openEditEvent);
                    }, 5000);
                }
            });
            /*Process Image in Edited DO Record Tags */
            $("#div-crm-overlay-content-container2 .modal-dialog #div-open-tag-img").unbind('click').bind('click', function (event) {
                that.fnDOEditRecProcessTagFromImage();
            });
            /*Get User Tag Favorites*/
            $("#div-crm-overlay-content-container2 .modal-dialog #div-open-tag-favorite").unbind('click').bind('click', function (event) {
                that.fnDOEditRecTagGetUserFavorites();
            });
        },
        
        /*Close DO Edit Record Tags Screen Popup*/
        fncloseDOEditRecordTagPopup: function(event) {
                 $('#div-crm-overlay-content-container2').removeClass('editBookedDOReorcdTagPopUp');
                 $("#div-crm-overlay-content-container2 .modal-body").empty();
                 hidePopup();
        },

        /*Show Process Tag and its details in Popup*/
        fnShowTagScreenPopup: function () {
                var that = this;
                $('.div-overlay').show();
                $("#div-crm-overlay-content-container2").addClass("editBookedDOReorcdTagPopUp");
                $('.editBookedDOReorcdTagPopUp').find('.modal-body').empty();
                $('.editBookedDOReorcdTagPopUp').find('.modal-title').empty();
                $('.editBookedDOReorcdTagPopUp').find('.modal-footer').empty();
                $(".editBookedDOReorcdTagPopUp .modal-title").html('Process Tag :'+that.selectedOrderId);
                $(".editBookedDOReorcdTagPopUp .modal-footer").html('Saving Tag Details...');
                $(".editBookedDOReorcdTagPopUp .modal-footer").addClass("hide");

                $("#div-crm-overlay-content-container2").removeClass("hide");

                $('#div-crm-overlay-content-container2 button[data-dismiss="modal"]').each(function () {
                $(this).unbind('click').bind('click', function () {
                        $('.div-overlay').hide();
                        $('#div-crm-overlay-content-container2').addClass('hide');
                        $("#div-crm-overlay-content-container2").removeClass("hide");
                });
            });

        },
        /*Load Tag Details in DO Edit Record Tags Screen*/
        loadTagDetailsInEditRecTags: function (rowValue) {
                var that = this;
                var arrSelectedData = new Array();
                if (rowValue.length == undefined)
                    arrSelectedData.push(rowValue);
                else
                    arrSelectedData = rowValue;
               
                $('#editDOTagDetails #txt-billing-account').val('');
                $('#editDOTagDetails #div-common-search-x').addClass('hide');

                //If the multiple tags are selected from the Tag dropdown then loop the tag and render the data and save the details in the Local table
                this.loadDOTagInfo(0, arrSelectedData);
       },

      /*loadDOTagInfo(): function to list the multiple Tag details*/
       loadDOTagInfo: function (iValue, data) {
                var that = this;
                if (iValue < data.length) {
                    var tagid = data[iValue].Name;
                    var setArray = data[iValue].ID.split("*");
                    var setid = setArray[0];
                    var setname = setArray[1];
                    var counttagduplicate = 0;
                    data[iValue].tagid = tagid;
                    data[iValue].setid = setid;
                    data[iValue].setnm = setname;
                    if (data[iValue].tagid != undefined) {
                        this.tagidCheck = _.each(that.arrDORecordTagDtls, function (item) {
                            if (tagid == item.tagId) {
                                counttagduplicate = 1;
                                 dhtmlx.message({
                                    title: "Tag Exists",
                                    type: "alert",
                                    text: lblNm("tagid_exist"),
                                     callback: function() {$('.dhtmlx-alert').addClass('hide');}
                                });
                            }
                        });
                        if (counttagduplicate == 0) {
                            $(".ul-do-do-tag-list-header").show();

                            if ($(window).width() < 480) {
                                that.$("#div-edit-do-record-tag-list").append(that.template_edit_recordtagdetailsMobile(data[iValue]));
                                that.comnMobFunc();
                                
                            } else {
                                that.$("#div-edit-do-record-tag-list").append(that.template_edit_recordtagdetails(data[iValue]));
                                
                            }
                             /*Remove Tags from edit Record Tag screen*/
                  $("#div-crm-overlay-content-container2 .modal-body #div-do-edit-record-tag-detail #div-edit-do-record-tag-list .ul-record-tag-list .fa-minus-circle").unbind('click').bind('click', function (event) {
                     
                      that.deleteDOEditRecordTags(event);             
                
                    }); 
                             /*Remove Tags from edit Record Tag screen mobile*/
                $("#div-crm-overlay-content-container2 .modal-body #div-do-edit-record-tag-detail #div-edit-do-record-tag-list .ul-parts-used-list-mobile .delete-pu-span .clear-row-pu").unbind('click').bind('click', function (event) {

                    that.deleteDOEditRecordTags(event);

                });               
                        
                            that.loadDOTagInfo(iValue + 1, data);
                            that.updateDOJSONTagDtls(data[iValue]);
                        }else{
                            that.loadDOTagInfo(iValue + 1, data);
                        }
                    }
                }
            },

         /*update DO Array values based on selection in DO Edit Record Tags Screen*/
             updateDOJSONTagDtls: function (data) {
                var that = this;

                    /*Get the Location of the tag and set it into Global Variable and save the lattitude / longtitude details in the arrDORecordTagDtls array
                     */
                 
                    that.arrDORecordTagDtls.push({
                        "orderId" : that.selectedOrderId,
                        "tagId": data.tagid,
                        "setId": data.setid,
                        "setNm": data.setnm,
                        "latitude": GM.Global.latitude,
                        "longitude": GM.Global.longitude,
                        "newlyAddedTag" : "Y"
                    });
              
                    if(that.arrDORecordTagDtls.length > 0){
                       $("#div-edit-do-record-tag-list").empty();
                       }
                    
                    for (var i = 0; i < that.arrDORecordTagDtls.length; i++) {
                    if ($(window).width() < 480) {
                        $("#div-edit-do-record-tag-list").append(that.template_edit_recordtagdetailsMobile(that.arrDORecordTagDtls[i]));
                        that.comnMobFunc();
                    } else {
                        $("#div-edit-do-record-tag-list").append(that.template_edit_recordtagdetails(that.arrDORecordTagDtls[i]));
                    }
                         /*Remove Tags from edit Record Tag screen*/
                  $("#div-crm-overlay-content-container2 .modal-body #div-do-edit-record-tag-detail #div-edit-do-record-tag-list .ul-record-tag-list .fa-minus-circle").unbind('click').bind('click', function (event) {
                       
                      that.deleteDOEditRecordTags(event);             
                
                    }); 
                    /*Remove Tags from edit Record Tag screen mobile*/    
                $("#div-crm-overlay-content-container2 .modal-body #div-do-edit-record-tag-detail #div-edit-do-record-tag-list .ul-parts-used-list-mobile .delete-pu-span .clear-row-pu").unbind('click').bind('click', function (event) {

                    that.deleteDOEditRecordTags(event);

                });           
                        
                }
                        
              
            },
        /*For Mobile view toggle functionality*/
            comnMobFunc: function () {
                this.$("#div-edit-do-record-tag-list").find(".ul-parts-used-list-mobile").each(function () {
                    console.log($(this).next().is('ul'));
                    if ($(this).next().is('ul')) {
                        $(this).addClass("ul-show-part");
                        $(this).children(".editDORecTagtogglebx").children("img").addClass("onActive");

                    } else {
                        $(this).removeClass("ul-show-part");
                        $(this).children(".editDORecTagtogglebx").children("img").removeClass("onActive");
                    }

                });

                $(".editDORecTagtogglebx").unbind('click').bind('click', function () {
                    //                $(".togglebx").on("click", function () {
                    $(this).parent(".ul-parts-used-list-mobile").toggleClass('ul-show-part');
                    $(this).children("img").toggleClass("onActive");
                });

                $(".ul-parts-used-list-mobile").on('swipeleft', function (e, data) {
                   if($(this).attr("data-newlyaddedTag") == 'Y'){
                        $(this).find(".delete-pu-span").addClass("onShowEl");
                        $(this).find(".copy-pu-span").removeClass("onShowEl");
                      }
                });

                $(".ul-parts-used-list-mobile").on('swiperight', function (e, data) {
                    if($(this).attr("data-newlyaddedTag") == 'Y'){
                        $(this).find(".copy-pu-span").addClass("onShowEl");
                        $(this).find(".delete-pu-span").removeClass("onShowEl");
                    }
                });

                $(".ul-parts-used-list-mobile").on('tap', function (e, data) {
                    if($(this).attr("data-newlyaddedTag") == 'Y'){    
                        $(this).find(".delete-pu-span").removeClass("onShowEl");
                        $(this).find(".copy-pu-span").removeClass("onShowEl");
                    }
                });
            },
        /*loadSetDetailsInEditRecTags(): Display set details for the missing tag
             */
            loadSetDetailsInEditRecTags: function (rowValue) {
                var that = this;
                var arrSelectedData = new Array();
                if (rowValue.length == undefined)
                    arrSelectedData.push(rowValue);
                else
                    arrSelectedData = rowValue;

                $('#editDOSetDetails #txt-billing-account').val('');
                //$('#setDetails #div-search-result-main').addClass('hide');
                $('#editDOSetDetails #div-common-search-x').addClass('hide');

                //If the multiple sets are selected from the Set dropdown then loop the set and render the data and save the details in the Local table
                this.loadSetDetails(0, arrSelectedData);
            },

            /*loadSetDetails(): function to list the multiple set details
             */
            loadSetDetails: function (iValue, data) {
                var that = this;
                if (iValue < data.length) {
                    var setid = data[iValue].ID;
                    var setName = data[iValue].Name;
                    var misingTagId = 'MISSING';

                    data[iValue].tagid = misingTagId;
                    data[iValue].setid = setid;
                    data[iValue].setnm = setName;

                    if (data[iValue].setid != undefined) {
                        $(".ul-do-do-tag-list-header").show();
                        if ($(window).width() < 480) {
                            that.$("#div-edit-do-record-tag-list").append(this.template_edit_recordtagdetailsMobile(data[iValue]));
                            that.comnMobFunc();
                        } else {
                            that.$("#div-edit-do-record-tag-list").append(this.template_edit_recordtagdetails(data[iValue]));
                        }
                         /*Remove Tags from edit Record Tag screen*/
                  $("#div-crm-overlay-content-container2 .modal-body #div-do-edit-record-tag-detail #div-edit-do-record-tag-list .ul-record-tag-list .fa-minus-circle").unbind('click').bind('click', function (event) {
                      that.deleteDOEditRecordTags(event);             
                
                    }); 
                        /*Remove Tags from edit Record Tag screen mobile*/
                    $("#div-crm-overlay-content-container2 .modal-body #div-do-edit-record-tag-detail #div-edit-do-record-tag-list .ul-parts-used-list-mobile .delete-pu-span .clear-row-pu").unbind('click').bind('click', function (event) {

                    that.deleteDOEditRecordTags(event);

                });       
                        
                    }
                    that.loadSetDetails(iValue + 1, data);
                    that.updateDOJSONTagDtls(data[iValue]);
                }
            },
        /*Delete Record Tags From DO Edit Record Tag Screen Cart*/
        deleteDOEditRecordTags: function (event) {
                var that = this;
                var parent = $(event.currentTarget).parent().parent();
                var tagid;
                var setlistid;
                if ($(window).width() < 480) {
                    tagid = $(parent).find("#li-do-records-tagid-list").text();
                    setlistid = $(event.currentTarget).parent().find("#li-do-records-setid").text();
                } else {
                    tagid = $(parent).find(".li-do-rec-do-tag-dtl").text();
                    setlistid = $(event.currentTarget).parent().find(".li-do-rec-do-setid-dtl").text();
                }

                if (tagid != 'MISSING') {
                    that.arrDORecordTagDtls = _.without(that.arrDORecordTagDtls, _.findWhere(that.arrDORecordTagDtls, {
                        "tagId": tagid
                    }));
                } else {
                    var setid = '';
                    if ($(window).width() < 480) {
                        setlistid = $(event.currentTarget).parent().parent().find("#li-do-records-setid").text();
                    } else {
                        setlistid = $(event.currentTarget).parent().parent().find(".li-do-rec-do-setid-dtl").text();
                    }
                    that.arrDORecordTagDtls = _.without(that.arrDORecordTagDtls, _.findWhere(that.arrDORecordTagDtls, {
                        "setId": setlistid
                    }));
                }
                parent.remove();
                console.log('Delete After' + JSON.stringify(that.arrDORecordTagDtls));

        },
        
        /*Process Image in Edited DO Record Tags */
        fnDOEditRecProcessTagFromImage: function () {
            var that = this;

            fnGetDOTagImageDtls(that.selectedOrderId, function (data) {
                console.log(JSON.stringify(data));
                that.arrDORecTagFrmImgVO = data;
                //FOR HBS FILE
                if (data.length > 0) {
                    data = _.each(data, function (item) {
                        item['path'] = that.localpath;
                    });
                }
                fnGetDOUnprocessedTagImgDtls(that.selectedOrderId, function (unProcessedData) {
                   fnDOReadableTagsFromImageCnt(that.selectedOrderId, function (processedDataCnt){
                    that.fnShowProcessTagPopup();

                    $("#div-crm-overlay-content-container4 .modal-body").empty();
                    $("#div-footer").addClass("hide");

                    if ($(window).width() < 480) {
                        $("#div-crm-overlay-content-container4 .modal-body").html(that.template_edit_tag_Images_listMobile(data));
                    } else {
                        $("#div-crm-overlay-content-container4 .modal-body").html(that.template_edit_tag_Images_list(data));
                    }
                    //hide offline message when online   
                    if(!navigator.onLine){
                           $(".div-do-display-offline-image-msg").removeClass("hide");
                         }
                        
                    if (that.arrDORecTagFrmImgVO.length > 0) {
                        $(".div-do-img-delete-btn").fadeIn(2000);
                        $("#div-do-add-to-rec-tags-btn").fadeIn(2000);
                    } else {
                        $(".div-do-img-delete-btn").fadeOut(2000);
                        $("#div-do-add-to-rec-tags-btn").fadeOut(2000);
                    }

                    if (that.arrDORecTagFrmImgVO.length <= 0) {
                        $("#div-do-process-img-btn").fadeOut(2000);
                    } else if (unProcessedData.length > 0) {
                            if(navigator.onLine){
                                $("#div-do-process-img-btn").fadeIn(2000);
                            }else{
                                $("#div-do-process-img-btn").fadeOut(2000); 
                            }
                    } else if (unProcessedData.length <= 0) {
                        $("#div-do-process-img-btn").fadeOut(2000);
                        
                    }
                       
                    if(processedDataCnt > 0 && processedDataCnt != undefined && processedDataCnt != ""){
                        $("#div-do-add-to-rec-tags-btn").fadeIn(2000);      
                    }else{
                        $("#div-do-add-to-rec-tags-btn").fadeOut(2000);
                    }   


                    // TO close popup
                    $("#div-crm-overlay-content-container4 #div-tag-text-image-head-close").unbind('click').bind('click', function (event) {
                        that.fnclosechildPopup(event);
                    });
                    //load image on popup which is chosen
                    $("#div-crm-overlay-content-container4").find('#input-tag-img').unbind('change').bind('change', function (event) {
                        that.fnLoadImageOnDOEditTagPopup(event);

                    });
                    $("#div-crm-overlay-content-container4 .modal-body").find("#div-do-tag-img-upld-check-main").unbind('click').bind('click', function (e) {

                        if ($("#div-crm-overlay-content-container4 .modal-body .ul-do-tag-img-details").find("#li-select-tag-image").find(".check-btn").prop('checked') == false)
                            $(".check-btn").prop('checked', true);
                        else
                            $(".check-btn").prop('checked', false);

                    });
                      //Remove image from popup
                    $("#div-crm-overlay-content-container4 .modal-body").find("#div-do-tag-img-delete-btn").unbind('click').bind('click', function () {
                        if($(".ul-do-tag-img-details").find("input:checked").length > 0){
                            $(".ul-do-tag-img-details").each(function () {
                                if ($(this).find("input:checked").length > 0) {
                                    var tagItemDtlId = $(this).attr("imgrow");
                                fnDelDOTagImageDtls(tagItemDtlId, that.selectedOrderId);
                                $(this).remove();
                                that.fnDOEditRecProcessTagFromImage();
                                }
                            })
                            }else{
                                    dhtmlx.message({
                                    title: "Process Image",
                                    type: "alert",
                                    text: "Please select image to remove",
                                     callback: function() {$('.dhtmlx-alert').addClass('hide');}
                                });
                              }
                    });
                    // add all the processed tags to the record tags screen
                    $("#div-crm-overlay-content-container4 .modal-body").find(".div-do-add-to-rec-tags-btn").unbind('click').bind('click', function () {
                        that.fnAddDOProcessedTagFromImage();
                        that.fnclosechildPopup(event);
                    });
                    //process the image
                     $("#div-crm-overlay-content-container4 .modal-body").find(".div-do-process-img-btn").unbind('click').bind('click', function () {
                            that.loaderview = new LoaderView({
                                text: "Processing Image..."
                            });
                            fnReadTextFrmImageThruAPI(that.selectedOrderId, 'N');
                            setTimeout(function () {
                                that.fnDOEditRecProcessTagFromImage();
                                that.loaderview.close();
                            }, 15000);
                        });
		    //Zoom Image in iPad
                    $("#div-crm-overlay-content-container4 .modal-body .ul-do-tag-img-details").find("#li-read-tag-image-name").unbind('click').bind('click', function () {
                            if($(this).attr("src") != undefined){
                                 openImageInFileOpener2Plugin($(this).attr("src"));
                            }
                    }); 
                     //Zoom Image in mobile   
                    $("#div-crm-overlay-content-container4 .modal-body .ul-do-tag-img-details").find("#li-read-tag-image-name-mob").unbind('click').bind('click', function () {
                            if($(this).attr("src") != undefined){
                                 openImageInFileOpener2Plugin($(this).attr("src"));
                            }
                            
                    });  

                });
                });
            });

        },

        /*Show Process image Tag and its details in Popup*/
        fnShowProcessTagPopup: function () {
            var that = this;
            $('.div-overlay').show();
            $("#div-crm-overlay-content-container4").addClass("editReadTextFromImagePopUp");
            $('.editReadTextFromImagePopUp').find('.modal-body').empty();
            $('.editReadTextFromImagePopUp').find('.modal-title').empty();
            $(".editReadTextFromImagePopUp .modal-title").html('Process Image :' + that.selectedOrderId);

            $("#div-crm-overlay-content-container4").removeClass("hide");

            $('#div-crm-overlay-content-container4 button[data-dismiss="modal"]').each(function () {
                $(this).unbind('click').bind('click', function () {
                    $('.div-overlay').hide();
                    $('#div-crm-overlay-content-container4').addClass('hide');
                    $("#div-crm-overlay-content-container4").removeClass("hide");
                });
            });

        },
         /*Get Image,write in the required path and save to DO */
         fnLoadImageOnDOEditTagPopup: function (event) {
             console.log(" inside fnLoadImageOnDOEditTagPopup ");
             var that = this;
             var file = event.target.files[0];
             //var fileName = event.target.files[0].name;
             var imgData = "";
             var imageSavePath = "";
             //console.log("fileName==="+fileName);

             var reader = new FileReader();
             fnGetTagImageDtlsCount(function (tagImgDtlsCount) {
                 var fileName = tagImgDtlsCount + "_" + event.target.files[0].name;
                 console.log("fileName===" + fileName);
                 fnCreateDirectoryForTagImage(function (fileDir) {
                     console.log("fileDir===" + fileDir);

                     reader.onload = function (e) {
                         imgData = e.target.result.split(',')[1];
                         fnWriteImage("Globus/DO/TAGIMAGE", fileName, imgData, "image/jpeg", function (data) {
                             imageSavePath = data;
                             console.log("imageSavePath===" + JSON.stringify(imageSavePath));
                             //Function to save the Tag Image Path for the Order in in TBL_DO_TAG_IMG_DTL table.
                             //Save only the DO/TAGIMAGE/Filename.jpg as a path
                             imageSavePath = imageSavePath.substr(imageSavePath.indexOf("/DO") + 1);
                             fnSaveDOTagImageDetails(that.selectedOrderId, fileName, imageSavePath, "", "");
                             //CAll the below function again to refresh the screen
                             that.fnDOEditRecProcessTagFromImage();


                         });
                     };
                     reader.readAsDataURL(file);
                 });
             });
         },
        
        /*To close Tag Favorites and Process Image screen popup*/
        fnclosechildPopup: function (event) {
            $('#div-crm-overlay-content-container4').removeClass('editDOTagFavPopUp');
             $('#div-crm-overlay-content-container4').removeClass('editReadTextFromImagePopUp');
            $('#div-crm-overlay-content-container4').addClass('hide');
        },
        
        /*To add processed tags from Image to Edit Record Tag Screen*/
        fnAddDOProcessedTagFromImage: function () {

            var that = this;
            var tagid, setid, setnm, rowEl, currentRow, setdtl;


            fnGetDOProcessedTagImgDtls(that.selectedOrderId, function (data) {
                for (var i = 0; i < data.length; i++) {
                    var processedTagIDs = data[i].PROCESSED_TAGS;
                    var arrImgTagIDs = processedTagIDs.split(',');

                    for (var j = 0; j < arrImgTagIDs.length; j++) {

                        fnGetDOTagScan(arrImgTagIDs[j], function (tagdtls) {

                            if (tagdtls != "") {
                                for (var k = 0; k < tagdtls.length; k++) {
                                    tagid = tagdtls[k].tagid;
                                    setid = tagdtls[k].setid;
                                    setnm = tagdtls[k].setnm;
                                    setdtl = setid + '*' + setnm;

                                    //Form the Row Element     
                                    rowEl = $("<a id=" + setid * setnm + " class=item-searchlist name=" + tagid + " setid=" + setid + " setnm=" + setnm + " tagid=" + tagid + " ><div class='div-data hide'></div> <i class='fa fa-check'></i></a>");

                                    currentRow = {
                                        "ID": setdtl,
                                        "Name": tagid,
                                        "setid": setid,
                                        "setnm": setnm,
                                        "Element": rowEl
                                    };
                                    //Load Tag Details 
                                    that.loadTagDetailsInEditRecTags(currentRow);
                                }

                            }
                        });
                    }

                }
            });


        },
        /*Get Tag Favorites*/
        fnDOEditRecTagGetUserFavorites: function () {
                var that = this;
                that.fnShowPopupTagFavorites();
                var favDocIDs = that.DOFavoriteMultipleDocumentID;
                console.log("favDocIDs==" + JSON.stringify(favDocIDs));
                this.favListArray = [];
                this.renderTemplateFL = 1;
                $("#div-crm-overlay-content-container4 .modal-body").empty();
                
                _.each(favDocIDs, function(favDocumentId){                
                    //get live data from Firebase
                    console.log(favDocumentId.REPID.toString());
                    if (firedb != undefined) {
                        firedb.collection("TAG_FAVORITES").doc(favDocumentId.REPID.toString()).onSnapshot(function (doc) {
                            if (doc.exists) {
                                
                                //Set the current data for manipulation of the data
                                var localArr  = doc.data();
                                that.favListArray[favDocumentId.REPID.toString()] = doc.data();
                                $("#div-tag-fav-list").show();
                                
                                //Render the Popup screen 
                                if(that.renderTemplateFL == 1 && that.deleteRecordTagsFl == undefined) {                                        
                                    $("#div-crm-overlay-content-container4 .modal-body").append(that.template_edit_rec_tag_Favorites(doc.data()));
                                }
                                $("#div-footer").addClass("hide");
                                
                                that.favTagToggleFunc();

                                // This function will add the tag to the record tag screen from Favorite Popup.
                                $("#div-crm-overlay-content-container4").find('.use-tag-favorite').unbind('click').bind('click', function (event) {
                                    that.fnAddTagFavoriteToEditRecordTagScreen(event);
                                });
                                
                                // TO close popup
                                $("#div-crm-overlay-content-container4 #div-tag-fav-head-close").unbind('click').bind('click', function (event) {
                                    that.fnclosechildPopup(event);
                                });
                                
                            } else {
                                console.log("No such document!");
                            }
                        });
                    }
                    });
                },
        /*Toggle Tag Favorites*/
                favTagToggleFunc: function () {
                    $("#div-tag-fav-list").find(".ul-fav-used-list").each(function () {
                        if ($(this).next().is('ul')) {
                            $(this).removeClass("ul-show-fav");
                            $(this).children(".favtagtogglebx").children("img").removeClass("onActive");

                        } else {
                            $(this).addClass("ul-show-fav");
                            $(this).children(".favtagtogglebx").children("img").addClass("onActive");
                        }

                    });

                    $(".favtagtogglebx").unbind('click').bind('click', function () {
                        //                $(".togglebx").on("click", function () {
                        $(this).parent(".ul-fav-used-list").toggleClass('ul-show-fav');
                        $(this).children("img").toggleClass("onActive");
                    });
                },
        
         /*Add Tag(s) from the selected Favorites and its details to Record Tags Screen */
                fnAddTagFavoriteToEditRecordTagScreen: function (event) {
                     var that = this;
                    var i;
                    var tagid, setid, setnm, rowEl, currentRow, setdtl;
                    this.currentFavDocid = $(event.currentTarget).attr("favdocid");
                    this.currentFavName = $(event.currentTarget).attr("favname");
                
                 _.each(this.favListArray, function (key, value) {
                    if (value == that.currentFavDocid) {
                        var tagdtls = _.findWhere(key.arrFavVO, {
                            favname: that.currentFavName
                        });
                        if (tagdtls != undefined) {
                            for (i = 0; i < tagdtls.favitems.length; i++) {
                                tagid = tagdtls.favitems[i].tagid;
                                setid = tagdtls.favitems[i].setid;
                                setnm = tagdtls.favitems[i].setnm;
                                setdtl = setid + '*' + setnm;

                                //Form the Row Element     
                                rowEl = $("<a id=" + setid * setnm + " class=item-searchlist name=" + tagid + " setid=" + setid + " setnm=" + setnm + " tagid=" + tagid + " ><div class='div-data hide'></div> <i class='fa fa-check'></i></a>");

                                currentRow = {
                                    "ID": setdtl,
                                    "Name": tagid,
                                    "setid": setid,
                                    "setnm": setnm,
                                    "Element": rowEl
                                };
                                //Load Tag Details 
                                that.loadTagDetailsInEditRecTags(currentRow);
                            }
                        }
                    }
                 });
                },
                /*Show Tag Favorites and its details in Popup*/
                fnShowPopupTagFavorites: function () {
                    var that = this;
                    $('.div-overlay').show();
                    $("#div-crm-overlay-content-container4").addClass("editDOTagFavPopUp");
                    $('.editDOTagFavPopUp').find('.modal-body').empty();
                    $('.editDOTagFavPopUp').find('.modal-title').empty();
                    $(".editDOTagFavPopUp .modal-title").append('<div class="fav-tag-header">'+lblNm("tag_favorites")+'</div>');

                    $("#div-crm-overlay-content-container4").removeClass("hide");
                    $('#div-crm-overlay-content-container4 button[data-dismiss="modal"]').each(function () {
                        $(this).unbind('click').bind('click', function () {
                            $('.div-overlay').hide();
                            $('#div-crm-overlay-content-container4').addClass('hide');
                            $("#div-crm-overlay-content-container4").removeClass("editDOTagFavPopUp");
                        });
                    });

                },
            
	});	

	return DOReportView;
});