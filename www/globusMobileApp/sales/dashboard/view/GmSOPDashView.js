/**********************************************************************************
 * File:        GmDashDrillDownView.js
 * Description: View which loads the drilldown data
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification','datasync','saleitemmodel', 'saleitemcollection', 'saleitemlistview', 'dropdownview', 'jqueryui', 'setlistview' ,'loaderview'
        
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification, Datasync, SaleItem, SaleItems, SaleItemListView, DropDownView, JqueryUI, SetLisView,LoaderView ) {

	'use strict';
	var arrData = new Array(), currBottomLength = 0, currTopLength = 0;
	var GmSOPDashView = Backbone.View.extend({

		events : {
			"click #count-li" : "fetchSOPCount",
			"click #charge-li" : "fetchSOPCharge",
			"click .countdetail" : "fetchCountDetail",
           	        "click .chargedetailspan" : "fetchChargeDetail",
			"click #div-close" : "closeDetailPage",
		},

		/* Load the templates */ 
		template: fnGetTemplate(URL_Dashboard_Template, "GmSOPDash"),
        templateDetail: fnGetTemplate(URL_Dashboard_Template, "GmSOPDashDetail"),

		tplSpinner: fnGetTemplate(URL_Common_Template, "GmProgressSpinner"),
        /* initialize the template */
		initialize: function(options) {
			this.el = options.el;
			this.parent = options.parent;
			this.render(options);
		},
		 /* render the template */
		render: function (options) {
			var that = this;
			this.$el.html(this.template());
            this.$('#header').text("COUNT TO BE RETURNED");
            this.$(".div-dash-page-loader").html(this.tplSpinner({"message":lblNm("msg_data_fetching")}));
            this.$('#div-col-sop-charge-view').hide();
            this.fetchSOPCount();
            that.currency =localStorage.getItem("salesDbCurrSymbol");
			return this;
		},
         /* fetch SOP Charge */
        fetchSOPCharge: function(){
            var that = this ;
            $('#count-li').removeClass('active');
            $('#charge-li').addClass('active');
            $('#header').text("LATE FEE CHARGES");
            $(".div-dash-page-loader").show();
            $('#div-col-sop-count-view').hide();
            $('#div-col-sop-charge-view').hide();
            that.fetchTotalCharge();
            that.fetchHoldChargeCount();
        },
        /* fetch Accured Charge for SOP */
        fetchTotalCharge: function(){
            var that = this;
            $(".div-dash-page-loader").show();
            $('#div-col-sop-count-view').hide();
            $('#div-col-sop-charge-view').hide();
            	var input = 
                    {
                    "companyid": (localStorage.getItem("companyid"))?localStorage.getItem("companyid"):localStorage.getItem("userCompany"),
                     "division": (localStorage.getItem("divisionid"))?localStorage.getItem("divisionid"):localStorage.getItem("userDivision"),
                     "zone": (localStorage.getItem("zoneid"))?localStorage.getItem("zoneid"):"",
                     "region":(localStorage.getItem("regionid"))?localStorage.getItem("regionid"):"",
                     "distId": (localStorage.getItem("fieldsalesid"))?localStorage.getItem("fieldsalesid"):""};
             fnGetMicroAppServerData("fetchAppSOPChargeAccrued",input, function (data) {
                 $(".div-dash-page-loader").hide();
                 $('#div-col-sop-count-view').hide();
                 $('#div-col-sop-charge-view').show();
                 if(data !=null && data != undefined){
                     if(data[0].strChargeCount == undefined || data[0].strChargeCount ==''){
                         that.accuredcount = 0;
                     }
                     else{
                         that.accuredcount = data[0].strChargeCount;
                     }
                     if(data[0].strCharge == '' || data[0].strCharge == undefined){
                      that.accuredcharge = 0.00;  
                   }
                   else{
                      that.accuredcharge = data[0].strCharge; 
                   }
                 }
                 else{
                      that.accuredcount = 0;
                     that.accuredcharge = 0.00;  
                 }
                  $('#totalcount').html(that.accuredcount);   
                 $('#totalcharge').html(that.currency + " "+formatCurrency(that.accuredcharge));
                 that.finalcount = parseFloat(that.accuredcount) + parseFloat(that.holdcount);
                 that.finalcharge = parseFloat(that.accuredcharge) + parseFloat(that.holdcharge);
                 that.finalcharge =  that.currency + " "+ formatCurrency(that.finalcharge);
                 $('#finalcount').html(that.finalcount);
                 $('#finalcharge').html(that.finalcharge);
             });
        },
        /* fetch Hold Charge for SOP */
        fetchHoldChargeCount:function(){
            var that = this;
            $(".div-dash-page-loader").show();
            $('#div-col-sop-count-view').hide();
            $('#div-col-sop-charge-view').hide();
          var input = 
                    {
                     "companyid": (localStorage.getItem("companyid"))?localStorage.getItem("companyid"):localStorage.getItem("userCompany"),
                     "division": (localStorage.getItem("divisionid"))?localStorage.getItem("divisionid"):localStorage.getItem("userDivision"),
                     "zone": (localStorage.getItem("zoneid"))?localStorage.getItem("zoneid"):"",
                     "region":(localStorage.getItem("regionid"))?localStorage.getItem("regionid"):"",
                     "distId": (localStorage.getItem("fieldsalesid"))?localStorage.getItem("fieldsalesid"):""};
           fnGetMicroAppServerData("fetchAppSOPChargeHold",input, function (data) {
               if(data !=null && data != undefined){
                 $(".div-dash-page-loader").hide();
                 $('#div-col-sop-count-view').hide();
                 $('#div-col-sop-charge-view').show();
               if(data[0].strChargeCount == undefined || data[0].strChargeCount ==''){
                     that.holdcount = 0;
                 }
                 else{
                     that.holdcount = data[0].strChargeCount;
                 }
               if(data[0].strCharge == '' || data[0].strCharge == undefined){
                  that.holdcharge = 0.00;  
               }
               else{
                  that.holdcharge = data[0].strCharge; 
               }
             }
               else{
                   that.holdcount = 0;
                   that.holdcharge = 0.00; 
               }
               $('#holdcount').html(that.holdcount);  
               $('#holdcharge').html(that.currency + " "+formatCurrency(that.holdcharge));
               that.finalcount = parseFloat(that.accuredcount) + parseFloat(that.holdcount);
               that.finalcharge = parseFloat(that.accuredcharge) + parseFloat(that.holdcharge);
               that.finalcharge =  that.currency + " "+ formatCurrency(that.finalcharge);
                $('#finalcount').html(that.finalcount);
                $('#finalcharge').html(that.finalcharge);
             }); 
        },
       
        /* fetch count for SOP */
        fetchSOPCount:function(){
           var that = this;
            $('#count-li').addClass('active');
            $('#charge-li').removeClass('active');
            $('#header').text("COUNT TO BE RETURNED");
            $('#div-col-sop-charge-view').hide();
            $('#div-col-sop-count-view').show();
            var input = 
                    {
                    "companyid": (localStorage.getItem("companyid"))?localStorage.getItem("companyid"):localStorage.getItem("userCompany"),
                     "division": (localStorage.getItem("divisionid"))?localStorage.getItem("divisionid"):localStorage.getItem("userDivision"),
                     "zone": (localStorage.getItem("zoneid"))?localStorage.getItem("zoneid"):"",
                     "region":(localStorage.getItem("regionid"))?localStorage.getItem("regionid"):"",
                     "distId": (localStorage.getItem("fieldsalesid"))?localStorage.getItem("fieldsalesid"):""
                    };
            this.$('#div-col-sop-charge-view').hide();
            this.$('#div-col-sop-count-view').hide();
            var chartArr = [];
             fnGetMicroAppServerData("fetchAppAllSOPCount",input, function (data) {
               
                $(".div-dash-page-loader").hide();
                that.$('#div-col-sop-count-view').show();
                   if(data != null && data !=undefined){
                      that.strOverdueCount = data.strOverdueCount;
                      that.strOverdueCountToday = data.strOverdueCountToday;
                      that.strOverdueCountTomorrow = data.strOverdueCountTomorrow;
                      that.strCount1to7 = data.strCount1to7;
                      that.strCount8to14 = data.strCount8to14;
                      that.strCount15to30 = data.strCount15to30;
                   }
                   else{
                      that.strOverdueCount = 0;
                      that.strOverdueCountToday = 0;
                      that.strOverdueCountTomorrow = 0;
                      that.strCount1to7 = 0;
                      that.strCount8to14 = 0;
                      that.strCount15to30 = 0;
                  }
                     that.$('#Overdue').html(that.strOverdueCount);
                      that.$('#Today').html(that.strOverdueCountToday);
                      that.$('#Tomorrow').html(that.strOverdueCountTomorrow);  
                var cnt1to7 = {
                        "Label": "1-7 Days",
                        "Value": that.strCount1to7,
                        "color": "#d04437"
                    };
                    chartArr.push(cnt1to7);
                    var cnt8to14 = {
                        "Label": "8-14 Days",
                        "Value": that.strCount8to14,
                        "color": "#3B7FC4"
                    };
                    chartArr.push(cnt8to14);
                    var cnt15to30 = {
                        "Label": "15-30 Days",
                        "Value": that.strCount15to30,
                        "color": "#8eb021"
                    };
                    chartArr.push(cnt15to30);
                that.loadChart(chartArr);
             }); 
        },
          // This Function is used to load fusion chart for 1-7,8-14,15-30days
             loadChart: function (data) {
                                var that = this;
               var chartOptions = {
                "showBorder": "0",
                "showShadow": "0",
                "showCanvasBorder": "0",
                "bgColor": "#ffffff",
                "aligncaptionwithcanvas": "0",
                "captionpadding": "0",
                "decimals": "0",
                "startingAngle": "90",
                "showLabels": "0",
                "theme": "fusion",
                "showLegend": "1",
                "showLegendBorder": "0",
                "showValues": "1",
                "drawcustomlegendicon": "1",
                "legendBorderColor": "#FFFFFF",
                "chartBottomMargin": "1",
                "chartTopMargin": "1",
                "chartRightMargin": "1",
                "chartLeftMargin": "1",
                "legendShadow": false,
                "use3DLighting":"0"
            }

            FusionCharts.ready(function () {
                var myChart = new FusionCharts({
                    type: "doughnut2d",
                    renderAt: 'count-chart-container',
                    dataFormat: "json",
                    width: "305",
                    height: "210",
                    dataSource: {
                        "chart": chartOptions,
                        "data": data
                    },
                }).render();
                
                myChart.addEventListener("dataPlotClick", function (e, v) {
                    var selectedStatus = v.toolText;
                    selectedStatus = selectedStatus.split(',');   
                    that.fetchCountDetail(null,selectedStatus[0]);
                });

            });
              
            },
        /* fetch count details for SOP */
        fetchCountDetail: function(e,val){
             var that = this;
            var idval= "";
            that.detail = "count";
            var count = 0;
            if(e == null){
              idval = val; 
              count = 1;
            }
            else{
              idval = e.target.id;
               var text = $('#'+idval).text();
              if(text == '0') {
                  count = 0;
              } 
                else{
                   count = 1;
                }
                
            }
            if(count == 1){
               var input = 
                    {
                     "companyid": (localStorage.getItem("companyid"))?localStorage.getItem("companyid"):localStorage.getItem("userCompany"),
                     "division": (localStorage.getItem("divisionid"))?localStorage.getItem("divisionid"):localStorage.getItem("userDivision"),
                     "zone": (localStorage.getItem("zoneid"))?localStorage.getItem("zoneid"):"",
                     "region":(localStorage.getItem("regionid"))?localStorage.getItem("regionid"):"",
                     "distId": (localStorage.getItem("fieldsalesid"))?localStorage.getItem("fieldsalesid"):"",
                     "strCountDay": idval
                    };
            fnGetMicroAppServerData("fetchSOPAppCountDetail",input, function (data) {
                  $('#div-dash-main-content').html(that.templateDetail());
                            $('#header-detail').html(idval.toUpperCase());

                console.log('data'+JSON.stringify(data));
                var grid = new dhx.Grid("overdue-detail-rpt", {

                        columns: [


                            { width: 100, id: "tagId", header: [{ text:  "Tag #" ,css:"title",align:"center"}, { content: "inputFilter" }], sortable: true, type:"string"},

                            { width: 105, id: "setId", header: [{ text: "Set ID" ,css:"title",align:"center"}, { content: "inputFilter" }] , sortable: true ,type:"string"},

                            { width: 310, id: "setNm", header: [{ text: "Set Description" ,css:"title",align:"center"}, { content: "inputFilter" }] , sortable: true ,type:"string"},
                            
                            { width: 200, id: "distributorNm", header: [{ text: "Field Sales" ,css:"title",align:"center"}, { content: "inputFilter" }] , sortable: true ,type:"string"},
                            
                            { width: 110, id: "returnDate", header: [{ text: "Due By Date" ,css:"title",align:"center"}] , sortable: true },

                            { width: 150, id: "returnTxnId", header: [{ text: "RA #" ,css:"title",align:"center" }, { content: "inputFilter" }],sortable: true ,
                             htmlEnable: true,
                             template: function (text, row, col) {
                                 return "<a class='raClass'>"+ text +"</a>";
                             },align:"center"}
                        ],

                        keyNavigation: true,
                        height: 490,
                        width: 980, 
                        position:"label-right",
                        multiselection:true, 
                        footer: true,
                        selection:"complex",
                        data: data,
                        dragItem: "column",
                        headerRowHeight:50,
                        rowCss: function (row) {
                            return row.strCustomcss
                        },

                    });
                   
    		  grid.events.on("cellclick", function (row, column, conf) {
                  var returnId = row.returnTxnId;
                  console.log("returnId"+returnId);
                  var returnTxnCol =  grid.getColumn("returnTxnId");
               // if GM-RA hyperlink is click, it will load Return Summary Page
                  if(returnTxnCol == column){
                	   if(returnId != ''){
                        that.fnReturnTxn(returnId);
                      }
                  }

          });
              
                 
             });  
            }
            
        },
           /* fetch charge details for SOP */
        fetchChargeDetail: function(e){
             var that = this;
            var resval = '';
            that.detail = "charge";
            var idval = e.target.id;
            var header = '';
            if(idval == 'totalcount' || idval == 'totalcharge'){
               resval = 'totalcharge'; 
                header = 'TOTAL CHARGES';
            }
            else if(idval == 'holdcount' || idval == 'holdcharge'){
               resval = 'hold';
                header = 'HOLD CHARGES';
            }
           var textval= $('#'+idval).text();
            if(textval != '' && textval != '0' && textval !=undefined && textval !='$ 0.00'){
                
            var input = 
                    {
                     "companyid": (localStorage.getItem("companyid"))?localStorage.getItem("companyid"):localStorage.getItem("userCompany"),
                     "division": (localStorage.getItem("divisionid"))?localStorage.getItem("divisionid"):localStorage.getItem("userDivision"),
                     "zone": (localStorage.getItem("zoneid"))?localStorage.getItem("zoneid"):"",
                     "region":(localStorage.getItem("regionid"))?localStorage.getItem("regionid"):"",
                     "distId": (localStorage.getItem("fieldsalesid"))?localStorage.getItem("fieldsalesid"):"",
                     "strChargeNm": resval};
          
             fnGetMicroAppServerData("fetchAppSOPChargeDetail",input, function (data) {
                    $('#div-dash-main-content').html(that.templateDetail());
                   $('#header-detail').html(header);
                    console.log('data'+JSON.stringify(data));
                    var grid = new dhx.Grid("overdue-detail-rpt", {

                         columns: [


                            { width: 80, id: "tagId", header: [{ text:  "Tag #" ,css:"title",align:"center"}, { content: "inputFilter" }], sortable: true, type:"string"},

                            { width: 95, id: "setId", header: [{ text: "Set ID" ,css:"title",align:"center"}, { content: "inputFilter" }] , sortable: true ,type:"string"},

                            { width: 250, id: "setNm", header: [{ text: "Set Description" ,css:"title",align:"center"}, { content: "inputFilter" }] , sortable: true ,type:"string"},

                            { width: 180, id: "distributorNm", header: [{ text: "Field Sales" ,css:"title",align:"center"}, { content: "inputFilter" }] , sortable: true ,type:"string"},
                             
                            { width: 90, id: "strIncidentdate", header: [{ text: "Accrual Date" ,css:"title",align:"center"} ] , sortable: true },
                             
                             { width: 80, id: "strChargeamount", header: [{ text: "Amount" ,css:"title",align:"center"}, { content: "inputFilter" }] , sortable: true ,type:"int"},
                             
                            { width: 90, id: "returnDate", header: [{ text: "Due By Date" ,css:"title",align:"center"}] , sortable: true  },
                            
                            { width: 120, id: "returnTxnId", header: [{ text: "RA #" ,css:"title",align:"center" }, { content: "inputFilter" }],sortable: true ,
                             htmlEnable: true,
                             template: function (text, row, col) {
                                 return "<a class='raClass'>"+ text +"</a>";
                             }}
                           

                        ],


                        keyNavigation: true,
                        height: 490,
                        width: 980, 
                        tooltip: true,
                        position:"label-right",
                        multiselection:true, 
                        footer: true,
                        selection:"complex",
                        data: data,
                        dragItem: "column",
                        headerRowHeight:50,
                        rowCss: function (row) {
                            return row.strCustomcss
                        },

                    });
                    

        grid.events.on("cellclick", function (row, column, conf) {
                  var returnId = row.returnTxnId;
                  console.log("returnId"+returnId);
                  var returnTxnCol =  grid.getColumn("returnTxnId");
               // if GM-RA hyperlink is click, it will load Return Summary Page
                  if(returnTxnCol == column){
                	   if(returnId != ''){
                        that.fnReturnTxn(returnId);
                      }
                  }

          });
              
                 
             }); 
            }
        },
        closeDetailPage: function(){
            var that = this;
             $('#div-dash-main-content').html(that.template());
           
             if(that.detail == "charge"){
             that.fetchSOPCharge();
            }
            else{
               that.fetchSOPCount();
            }
        },
              //Summary Report DO Function
       fnReturnTxn: function (raId) {
    	var strPartyId =  localStorage.getItem("partyid");
        var strCompId  =  localStorage.getItem("cmpid");
        var strLangId  =  localStorage.getItem("lang");
        var strPlantId =  localStorage.getItem("plantid");
        
        var companyInfo = {};
        //form the company info to pass the URL
        companyInfo.cmpid = strCompId;
        companyInfo.plantid = strPlantId;
        companyInfo.token = "";
        companyInfo.partyid = strPartyId;
        companyInfo.cmplangid = strLangId;
           
           var StringifyCompanyInfo = JSON.stringify(companyInfo);
           
                var toEncode = "/GmPrintCreditServlet?hAction=PRINT&hRAID="+raId+ "&companyInfo=" + StringifyCompanyInfo;
                var URL = URL_Portal + "GmLogonServlet?token=" + localStorage.getItem("token") + "&return_to=" + encodeURIComponent(toEncode) + "&randomid=" + Math.random();

                console.log(URL);
                var summaryWindow = window.open(URL, '_blank', 'location=no');
                summaryWindow.addEventListener('loadstop', function () {
                    summaryWindow.insertCSS({
                        code: "body {font-family: 'Trebuchet MS'!important; -webkit-overflow-scrolling: touch;} a{display: none;} .RightDashBoardHeader {background:#999;} .ShadeRightTableCaption {background:#ccc;} TR.ShadeBlueBk {background:#eee}" + " table { margin:auto!important; } .button-red{ display:none; }"
                    });
                    summaryWindow.executeScript({
                        code: "var meta = document.createElement('meta'); meta.name = 'format-detection'; meta.content = 'telephone=no'; document.getElementsByTagName('head')[0].appendChild(meta); " + that.scriptOptions
                    });
                });
                summaryWindow.addEventListener('loaderror', function () {
                    showNativeAlert(lblNm("msg_error_loading"), lblNm("error"), lblNm("msg_ok"), function (index) {
                        summaryWindow.close();
                    });
                });

    },
		// Close the view and unbind all the events
		close: function(e) {
			
			this.unbind();
			this.remove();
		}

	});	

	return GmSOPDashView;
});
