/**********************************************************************************
 * File:        GmDashDrillDownView.js
 * Description: View which loads the drilldown data
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification','datasync','saleitemmodel', 'saleitemcollection', 'saleitemlistview', 'dropdownview', 'jqueryui', 'setlistview' ,'loaderview'
        
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification, Datasync, SaleItem, SaleItems, SaleItemListView, DropDownView, JqueryUI, SetLisView,LoaderView ) {

	'use strict';
	var arrData = new Array(), currBottomLength = 0, currTopLength = 0;
	var DashDrillDownView = Backbone.View.extend({

		events : {
			"click .div-drilldown-header" : "toggleListDetail",
			"click .li-dash-drilldown-link" : "openSummary",
			"keyup .txt-dash-drilldown-search" : "searchKeyword",
			"click .li-dash-drilldown-track-link" : "openTrack",
			"click .btn-req-big" : "reqUpdate",
			"click .li-dash-drilldown-tagid-link" : "openSetList",
            "click .li-dash-pricerequest-link" : "openHeldOrderPriceRequestDetails",
		},

		/* Load the templates */ 
		template: fnGetTemplate(URL_Dashboard_Template, "GmDashDrillDown"),
		template_list: fnGetTemplate(URL_Dashboard_Template, "GmDashDrillDownList"),
		

		tplSpinner: fnGetTemplate(URL_Common_Template, "GmProgressSpinner"),

		initialize: function(options) {
			this.el = options.el;
			this.parent = options.parent;
			//Get the stored filters and token
			this.serverInput = {
				"token": localStorage.getItem("token"),
				"companyid": (localStorage.getItem("companyid"))?localStorage.getItem("companyid"):"",
				"divids": (localStorage.getItem("divisionid"))?localStorage.getItem("divisionid"):"",
				"zoneids": (localStorage.getItem("zoneid"))?localStorage.getItem("zoneid"):"",
				"regionids":(localStorage.getItem("regionid"))?localStorage.getItem("regionid"):"",
				"fsids": (localStorage.getItem("fieldsalesid"))?localStorage.getItem("fieldsalesid"):""
			};


			this.render(options);
		},
		
		render: function (options) {
			var that = this;
			this.$el.html(this.template());
			this.$el.addClass("dept-" + localStorage.getItem('Dept'));
			this.$(".div-dash-page-loader").html(this.tplSpinner({"message":lblNm("msg_data_fetching")}));
			this.serverInput.salesdbcurrtype = localStorage.getItem("salesDbCurrType");
			this.serverInput.salesdbcurrsymbol = localStorage.getItem("salesDbCurrSymbol");
			this.processInputData(options.viewOptions);

			return this;
		},

		reqUpdate: function(event) {
			event.preventDefault();
			var elem = event.currentTarget;
			var that = this;
			var reqDetail = $(elem).attr('name');
            var orderid = $(elem).attr('name');
            var accountnm = $(elem).attr('accountnm');

			if(!$(elem).hasClass('requested')) {
				    arrRequest.push(reqDetail);
                    arrOrder.push(orderid);

				this.collection.each(function(model) {
					if(model.get('reqdtlid') == reqDetail) 
						model.set('loanerReqested',true);
				});

				this.currCollection.each(function(model) {
					if(model.get('reqdtlid') == reqDetail) 
						model.set('loanerReqested',true);
				});
                
                this.collection.each(function(model) {
					if(model.get('txnid') == orderid) 
						model.set('PricingRequested',true);
				});

				this.currCollection.each(function(model) {
					if(model.get('txnid') == orderid) 
						model.set('PricingRequested',true);
				});

				$(elem).addClass('requested');
				$(elem).find('i').toggleClass("fa-check-circle fa-circle-o");
				that.setRequestCount();
			}
			else {
				var index = arrRequest.indexOf(reqDetail);
				if (index > -1) {
					arrRequest.splice(index, 1);
				}
                var indexorder = arrOrder.indexOf(orderid);
				if (indexorder > -1) {
					arrOrder.splice(indexorder, 1);
				}

				this.collection.each(function(model) {
					if(model.get('reqdtlid') == reqDetail) 
						model.set('loanerReqested',false);
				});

				this.currCollection.each(function(model) {
					if(model.get('reqdtlid') == reqDetail) 
						model.set('loanerReqested',false);
				});
                
                this.collection.each(function(model) {
					if(model.get('txnid') == orderid) 
						model.set('PricingRequested',false);
				});

				this.currCollection.each(function(model) {
					if(model.get('txnid') == orderid) 
						model.set('PricingRequested',false);
				});
				
				$(elem).removeClass('requested');
				$(elem).find('i').toggleClass("fa-check-circle fa-circle-o");
				that.setRequestCount();
			}

			if (arrRequest.length > 0 && that.title=="Loaners" ) {
				that.parent.$("#btn-extend-req").show();
				that.parent.$("#btn-extend-req").attr("name",that.serverInput.passname);
				
			} else {
				that.parent.$("#btn-extend-req").hide();
			}
            
			if (arrOrder.length > 0 && that.title=="Orders" && localStorage.getItem("PRICING_APP_ACCESS")=="Y" ) {
				that.parent.$("#btn-generate-pricerequest").show();
				that.parent.$("#btn-generate-pricerequest").attr("name",that.serverInput.passname);
				
			}else {
				that.parent.$("#btn-generate-pricerequest").hide();
			}

            if (that.title=="Orders" ) {
                /*Check box hiding logic in held order dash*/
                $(".div-drilldown").each(function () {
                        var currElement = $(this).find(".div-dash-drilldown-list-value").text();
                            if ((currElement.indexOf(accountnm) != -1)) {
                                $(this).find(".btn-req-big").removeClass("gmVisibilityHidden");
                            } else {
                                $(this).find(".btn-req-big").addClass("gmVisibilityHidden");
                            }

                    });
                if($(".div-drilldown").find(".requested").length == 0){
                    $(".div-drilldown").find(".btn-req-big").removeClass("gmVisibilityHidden");
                }
            }
		},

		setRequestCount: function(count) {
			this.parent.$("#div-extend-count").html(arrRequest.length);
            this.parent.$("#div-pricerequest-count").html(arrOrder.length);
		},

		processInputData: function(options) {

			var that = this;
			var title = options[1];
			var subTitle = options[2];
			var url = (options[3]).replace(/\&/g,"/");
			var jsnTitle = eval("jsnDrillDown."+title);
			this.VO = jsnTitle.VO;
			this.title = jsnTitle.title;
			this.secondURL = jsnTitle.url;
			this.cssOptions = jsnTitle.cssOptions;
			this.scriptOptions = jsnTitle.scriptOptions;

			var passname = eval("jsnDrillDown." + title + "." + subTitle + ".passname");
			var stropt = eval("jsnDrillDown." + title + "." + subTitle + ".stropt");
			var subTitleText = eval("jsnDrillDown." + title + "." + subTitle + ".title");

			if(passname!=undefined)
				this.serverInput.passname = passname;
			if(stropt!=undefined)
				this.serverInput.stropt = stropt;
			if(options[4]!=undefined) {
				this.serverInput.accountid = options[4];
				this.subTitleContent = options[5];
			}
			else
				this.subTitleContent = subTitleText;

			if(!(title.indexOf("consigned")<1 && subTitle=="requested"))
				this.linkKey = eval("jsnDrillDown." + title + ".linkKey");
			else
				this.linkKey = "";
			this.secondParameter = eval("jsnDrillDown." + title + ".secondParameter");

			fnGetServerData(url, this.VO, this.serverInput, function(data){

				if(data.length!=undefined){
                    if (passname == "accrlatefeespastmon" || passname == "accrlatefeescurr"){
                    var currSymbol = that.serverInput.salesdbcurrsymbol;
                    _.each(data, function (item, index, items) {                        
                        if(item.chargeamt < 0) // to check negative value 
                            item.chargeamt =  '<span class="gmFontRed">(' + currSymbol + formatCurrency(Math.abs(item.chargeamt)) + ') </span>';
                        else
                            item.chargeamt =  currSymbol + formatCurrency(item.chargeamt);
                    });
                    }
					that.viewData = data;
                }
				else {
					that.viewData = new Array();
					that.viewData.push(data);
				}

				that.serverData = $.parseJSON(JSON.stringify(that.viewData));

				that.parent.$("#div-dash-title").html(that.title + " - ");
				that.parent.$("#div-dash-subtitle").html(that.subTitleContent);
				that.processOutputData(that.viewData.slice(0,50),1);
				
			});
		},


		processOutputData: function(data,doRender) {
			var that = this;


			if(this.title.indexOf("Consignment")>=0) {
				this.title = "Consignment";
			}

			fnGetDashColumnsFor('',this.title,function(dbData) {
				if(dbData.length>0) {
					
					if(that.title=="Loaners")
						for(var i=0; i<data.length; i++) { 
							data[i].pendingApproval = data[i].pendingappr;
							data[i].loanedOn = data[i].loanedon;
						}
                    
                    if(that.title=="Orders")
						for(var i=0; i<data.length; i++) { 
							data[i].orderdate = data[i].orderdt;
							data[i].orderid = data[i].txnid;
							data[i].accountname = data[i].accountnm;
						}

					var strColumns = JSON.stringify(dbData);
					var strData = JSON.stringify(data);
					var keyTotal = 0;

					for(var x=0; x<dbData.length; x++) {
						strColumns = strColumns.replace("COLUMN_NAME", "label"+(x+1));
						var newCol = "label" + (x+1) + '":"' + dbData[x].COLUMN_NAME + '","'
						var reg = new RegExp('"'+ dbData[x].SHORT_NM + '"', 'g');
						var strClass = "";

						if((dbData[x].SHORT_NM==((that.linkKey!=undefined)?that.linkKey:"txnid"))) {
	             			strClass = "class" + (x+1) + '":"li-dash-drilldown-link","';
	             			var regURL = new RegExp("caseinfoid", 'g');
		             		strData = strData.replace(regURL, "url"+(x+1));
	             		}     					             		

	             		else if(dbData[x].SHORT_NM=="trackingnum") {
	             			strClass = "class" + (x+1) + '":"li-dash-drilldown-track-link","';
	             			var regURL = new RegExp("trackurl", 'g');
		             		strData = strData.replace(regURL, "url"+(x+1));
		             		// console.log(strData);
	             		}

	             		else if(dbData[x].SHORT_NM=="total") {
	             			keyTotal = x+1;
	             			strClass = "class" + (x+1) + '":"li-dash-drilldown-total-identifier","';
	             		}

	             		else if(dbData[x].SHORT_NM=="tagid") {
	             			keyTotal = x+1;
	             			strClass = "class" + (x+1) + '":"li-dash-drilldown-tagid-link","';
	             		}
                        else if((dbData[x].SHORT_NM==((that.linkKey!=undefined)?that.linkKey:"pricereq"))) {
	             			strClass = "class" + (x+1) + '":"li-dash-pricerequest-link","';
	             			var regURL = new RegExp("txnid", 'g');
		             		strData = strData.replace(regURL, "url"+(x+1));
	             		}  

	             		if((that.secondParameter!=undefined) && (dbData[x].SHORT_NM==that.secondParameter)) 
	             			strClass = "class" + (x+1) + '":"li-second-param","';

	             		strData = strData.replace(reg, '"'+ strClass + newCol + "value"+(x+1)+ '"');

					}
					
					// console.log(strData);

					arrData = $.parseJSON(strData);
				
					//Attach keyword for search
					for(var i=0;i<arrData.length;i++) {
						var currRow = arrData[i], keyword="";
						for (var column in currRow) {
							if(column.indexOf("value")>=0) {
								keyword += currRow[column] + " ";
							}
						}
						arrData[i].keyword = keyword;
						if(that.title=="Orders") 
							arrData[i]["value" + keyTotal] = parseFloat(arrData[i]["value" + keyTotal]);
					
					}

					// console.log(arrData);
					that.collection = new SaleItems(arrData);	
					that.currCollection = that.collection;
					if(doRender) {
						that.showSortOptions(dbData);
						that.populateList();
						that.processOutputData(that.viewData,0);
					}
						
					
				}
				else {
					fnGetServerData("configuredrilldown/dddetails","listGmSalesDashConfColumnVO", undefined, function(data){
						that.insertConfigData(0,data);
					});
				}

			});
		},

		insertConfigData: function(iValue, data) {
			if(iValue<data.length) {
				var that = this;
				var currData = data[iValue];
				if(currData.voidfl=="")
					fnInsertDashboardSeq(currData.ddgrpid, currData.ddgrpnm, currData.ddcolumnnm, currData.seq_num, currData.ddcolumnnmshrtnm, function() {
						that.insertConfigData(iValue+1,data);
					});
				else
					that.insertConfigData(iValue+1,data);
			}
			else
				this.processOutputData(this.viewData.slice(0,50),1);
		},

		showSortOptions: function(dbData) {
			var arrDrpDown = new Array();
			var that = this;
			arrDrpDown.push({"ID" : "Choose", "Name" : "Choose One"});
			for(var i=0;i<dbData.length;i++) {
				var jsnDrpDown = new Array();
				jsnDrpDown.ID = "value"+(i+1);
				jsnDrpDown.Name = dbData[i].COLUMN_NAME;
				arrDrpDown.push(jsnDrpDown);
			}

			var drpDownView = new DropDownView({
				el : this.$("#div-dash-drilldown-sortby"),
				data : arrDrpDown,
				DefaultID : "Choose"
			});

			this.$("#div-dash-drilldown-sortby").bind('onchange', function (e, selectedId) {
				if(selectedId!="Choose One")
					that.sort(selectedId);
			});
		},

		sort: function(selectedId) {
			// console.log(selectedId);
			this.collection.sortAsc(selectedId);
			this.currCollection.sortAsc(selectedId);
			this.populateList();
		},

		searchKeyword: function(event) {
			var letters = event.currentTarget.value;
			this.currCollection = new SaleItems(this.collection.search("keyword", letters));

			if(letters.length>2) {
				this.lastSearch = true;
				this.populateList();
				this.$("#ul-dash-drilldown-search-info").removeClass("dash-hide");
				this.$("#li-dash-drilldown-search-retrieved").text(this.currCollection.length);
				this.$("#li-dash-drilldown-search-count").text(this.collection.length);
				this.$("#li-dash-drilldown-search-count-phone").removeClass("dash-hide");
				this.$("#li-dash-drilldown-search-count-phone span").text(this.currCollection.length+"/"+this.collection.length);
                
			}
			else {
				this.$("#ul-dash-drilldown-search-info").addClass("dash-hide");
                this.$("#li-dash-drilldown-search-count-phone").addClass("dash-hide")
				if(this.lastSearch) {
					this.currCollection = this.collection;
					this.populateList();
				}
				this.lastSearch = false;
			}
		},
		
		toggleListDetail: function(event) {
            var elem = event.currentTarget;
            var parent = $(elem).parent();
            $(".div-drilldown-header-opened").removeClass("div-drilldown-header-opened");
            $(".div-drilldown-list").slideUp("fast");
            if($(elem).find("i").hasClass("fa-caret-right")) {
                $(".fa-caret-down").removeClass("fa-caret-down").addClass("fa-caret-right");
                $(elem).find("i").addClass("fa-caret-down").removeClass("fa-caret-right");
                $(parent).find(".div-drilldown-list").slideDown("fast");
                $(elem).addClass("div-drilldown-header-opened");
                this.$("#div-dash-drilldown-content-main").animate({
                    scrollTop: this.$("#div-dash-drilldown-content-main").scrollTop() + $(parent).position().top - 160
                }, "fast");
            }
            else {
                $(elem).find("i").addClass("fa-caret-right").removeClass("fa-caret-down");
                $(parent).find(".div-drilldown-list").slideUp("fast");
                $(elem).removeClass("div-drilldown-header-opened");
            }
	    },


		openSummary: function(event) {
//			if($("#div-dash-drilldown-sortby-header").css("display") != "none" ) {
				event.stopImmediatePropagation();
				var that = this, id;
				if($(event.currentTarget).attr("data-url")!="") 
					id = $(event.currentTarget).attr("data-url");
				else
					id = $(event.currentTarget).text();
			
                 var companyInfo = {};
                        companyInfo.cmpid = localStorage.getItem("cmpid");
                        companyInfo.plantid = localStorage.getItem("plantid");
                        var StringifyCompanyInfo = JSON.stringify(companyInfo);
                
				var toEncode = this.secondURL + id + "&companyInfo="+StringifyCompanyInfo;
				if(this.secondParameter!=undefined) {
					toEncode += "&hSetNm=" + $(event.currentTarget).parent().parent().parent().parent().parent().find(".li-second-param").text();
				}
				var URL = URL_Portal + "GmLogonServlet?token=" + localStorage.getItem("token") + "&return_to=" + encodeURIComponent(toEncode) + "&randomid=" + Math.random();
				
				console.log(URL);
				var summaryWindow = window.open(URL, '_blank', 'location=no');
				summaryWindow.addEventListener('loadstop', function() {
					summaryWindow.insertCSS({
			            code: "body {font-family: 'Trebuchet MS'!important; -webkit-overflow-scrolling: touch;} a{display: none;} .RightDashBoardHeader {background:#999;} .ShadeRightTableCaption {background:#ccc;} TR.ShadeBlueBk {background:#eee}" + that.cssOptions   
			        });
			        summaryWindow.executeScript({
			            code: "var meta = document.createElement('meta'); meta.name = 'format-detection'; meta.content = 'telephone=no'; document.getElementsByTagName('head')[0].appendChild(meta); " + that.scriptOptions
			        });
				});
				summaryWindow.addEventListener('loaderror',function() {
					showNativeAlert(lblNm("msg_error_loading"), lblNm("error"),lblNm("msg_ok"), function(index) {
						summaryWindow.close();
					});
				});
//			}
		},

		openTrack: function(event) {
//			if($("#div-dash-drilldown-sortby-header").css("display") != "none" ) {
				event.stopImmediatePropagation();
				var dataURL = $(event.currentTarget).attr("data-url");
				if(dataURL!=undefined) {
					var URL = dataURL + $(event.currentTarget).text().trim();
					console.log(URL);
					var summaryWindow = window.open(decodeURI(URL), '_blank', 'location=no');
				}
//			}
		},

		populateList: function() {
			var that = this;
			this.$("#div-saleitemlist").html(this.template_list(this.currCollection.toJSON().slice(0,50)));
			currBottomLength = 50;
			currTopLength = 0;
			$("#div-saleitemlist").on("scroll",function() {
				var scrollPercentage = 100 * this.scrollTop / (this.scrollHeight-this.clientHeight);
				if((scrollPercentage>98) && (that.currCollection.toJSON().length > currBottomLength)) {
					that.initializeUnlimitedScroll(1);
				}
				else if((scrollPercentage<10) && (currTopLength > 0)) {
					that.initializeUnlimitedScroll(0);
				}
			});

			this.formatList();

			this.$("#div-dash-drilldown-main").show();
			this.$(".div-dash-page-loader").hide();

			if(that.currCollection.toJSON().length > currBottomLength) {
				var div = document.createElement('div');
				div.id = "div-dash-list-loader";
				div.className = "div-dash-page-loader";
				div.innerHTML = this.tplSpinner({"message":lblNm("msg_loading_data_c")});
				$("#div-saleitemlist").append(div);
			}

		},

		initializeUnlimitedScroll: function(scrollType) {
			var elem = $(".div-drilldown").slice(-1);
			if(scrollType) {
				if(currBottomLength+10<=this.currCollection.toJSON().length) {
					this.$("#div-saleitemlist").append(this.template_list(this.currCollection.toJSON().slice(currBottomLength,currBottomLength+10)));
					this.$(".div-dash-page-loader").hide();
					var div = document.createElement('div');
					div.id = "div-dash-list-loader";
					div.className = "div-dash-page-loader";
					div.innerHTML = this.tplSpinner({"message":lblNm("msg_loading_data_c")});
					$("#div-saleitemlist").append(div);
					currBottomLength = currBottomLength+10;
				}
				else {
					this.$("#div-saleitemlist").append(this.template_list(this.currCollection.toJSON().slice(currBottomLength,this.currCollection.toJSON().length)));
					this.$(".div-dash-page-loader").remove();
					currBottomLength = this.currCollection.toJSON().length;
				}
				if(currBottomLength>200) {
					this.$("#div-saleitemlist").find(".div-drilldown").slice(0,10).remove();
					currTopLength = currTopLength+10;
				}
			}
			else {
				if(currTopLength-10>=0) {
					this.$("#div-saleitemlist").prepend(this.template_list(this.currCollection.toJSON().slice(currTopLength-10,currTopLength)));
					currTopLength = currTopLength-10;
						
					this.$("#div-saleitemlist").find(".div-drilldown").slice(-10).remove();
					currBottomLength = currBottomLength-10;

				}
			}
			this.$("#div-saleitemlist").scrollTo(elem);

			
			this.formatList();

			this.suppressExtn(this.serverInput.passname);
		},

		formatList: function() {
			var subTitle = $("#div-dash-subtitle").text();
			this.$("#div-saleitemlist").find(".li-drilldown-detail").each(function() {
				if($(this).text().trim()=="")
					$(this).remove();
			});

			this.$(".li-dash-drilldown-link").each(function() {
				if($(this).text().trim()=="")
					$(this).remove();
			});
            
            this.$(".li-dash-pricerequest-link").each(function() {
				if($(this).text().trim()=="")
					$(this).remove();
			});

			this.$(".li-dash-drilldown-track-link, .li-dash-drilldown-tagid-link").each(function() {
				if($(this).text().trim()=="")
					$(this).remove();
				else if($(this).attr("data-url")=="")
					$(this).removeClass("li-dash-drilldown-track-link");
			});

			this.$(".li-dash-drilldown-total-identifier").each(function() {
				var value = $(this).text();
				var salesDbCurrSymbol = localStorage.getItem("salesDbCurrSymbol")
				if(value.indexOf("-") >= 0) {
					value = value.replace("-","");
					value = parseFloat(value);
					$(this).html(salesDbCurrSymbol+"("+value.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")+")");
					$(this).addClass("content-red");
				}
				else {
					value = parseFloat(value);
					$(this).html(salesDbCurrSymbol+ value.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ","));
				}
				$(this).removeClass("li-dash-drilldown-total-identifier");
			});

			if(this.title=="Loaners" && subTitle!="Approved" && subTitle != "Requested"){
				this.$(".btn-req-big").removeClass("dash-hide");
            }
            
            if(this.title=="Orders" && subTitle=="Held Orders") {
				this.$(".btn-req-big").removeClass("dash-hide");
            }
			
		},

		openSetList: function(e) {
			var tagid = $(e.currentTarget).text();
			if(setlistview)
				setlistview.close();
						
			setlistview = new SetLisView({'el' :$('#div-setlist-viewer-background'), 'parent' : this, 'tagid' : tagid});
		},
		
        openHeldOrderPriceRequestDetails: function(event){
            event.stopImmediatePropagation();
            var orderid = $(event.currentTarget).parent().parent().attr("orderid");
			var that = this;
                var token = localStorage.getItem("token");
                var userid = localStorage.getItem("userID");
                var input = {
                    "token": token,
                    "orderids": orderid,
                    "userid":userid
                };
            this.loaderview = new LoaderView({
                    text: lblNm("price_request")
                });  
            if(localStorage.getItem("PRICING_APP_ACCESS")== "Y" )
                {
                 fnGetWebServerData("pricingRequest/fetchHeldOrderPriceRequest/", undefined, input, function (data) {
                        that.loaderview.close();
                        if (data != null) {
                            var inputJSNData = JSON.stringify(data.listgmHeldOrderPriceRequestVO);
                            inputJSNData = arrayOf($.parseJSON(inputJSNData));
                            showPopup();
                            $("#div-crm-overlay-content-container").find('.modal-footer').addClass("hide");
                            $("#div-crm-overlay-content-container").find('.modal-title').html(data.orderids);
                             var templateHeldOrderPriceReqDetails = fnGetTemplate(URL_Dashboard_Template, "GmHeldOrderPriceReqDetails");
                            $("#div-crm-overlay-content-container").find('.modal-body').html(templateHeldOrderPriceReqDetails(inputJSNData));
                                 $("#div-crm-overlay-content-container").find('.modal-body #div-heldorder-pr-details').unbind('click').bind('click', function (event) {
                                     var prid = $(event.currentTarget).attr("name");
                                     that.redirectToPricing(prid);
                                     hidePopup();
                                 });
                        } else
                            showError(lblNm("exception_loading"));
                    });
                 }
        },
     
         redirectToPricing:function(prid){
            window.location.href = "#price/pricingSubmittedRequests/"+prid;
		},
        
		// Close the view and unbind all the events
		close: function(e) {
			while(arrRequest.length > 0) {
			    arrRequest.pop();
			};
            while(arrOrder.length > 0) {
			    arrOrder.pop();
			};
			this.unbind();
			this.remove();
		}

	});	

	return DashDrillDownView;
});
