/**********************************************************************************
 * File:        GmDashMainView.js
 * Description: Initial SalesDashboard View to load other sub views
 * Version:     1.0
 * Author:     	praja 
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification','dashoverviewview', 'dashmorefilterview', 'saletodayview', 'salemtdview', 'dashdrilldownview', 'mtddrilldownview','loaderview','../sales/dashboard/view/GmOrderPODashboardView','../sales/dashboard/view/GmSOPDashView'
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification, DashOverviewView, DashMoreFilterView, SaleTodayView, SaleMTDView, DashDrillDownView, MTDDrillDownView,LoaderView,PODashboardView,SOPDashView) {

	'use strict';
	var DashMainView = Backbone.View.extend({

		events : {
//    		"click #div-detail-menu": "showNavigation",
    		"click #btn-extend-req" : "showRequestList",
    		"click #btn-req-submit" : "submitRequestList",
    		"click #btn-req-cancel" : "gotoDashboard",
    		"change #li-days select" : "setDate",
    		"click .li-whole-req" : "toggleWholeReq",
    		"click #btn-case-link" : "openLink",
            "click #btn-generate-pricerequest" : "generatePriceRequest",
            "click .fa-calendar, #surg-date" : "showDatepicker",
            "change .surg-date1": "formatcmpdate"
		},

		/* Load the templates */ 
		template: fnGetTemplate(URL_Dashboard_Template, "GmDashMain"),

		tplReqExtn: fnGetTemplate(URL_Dashboard_Template, "GmReqExtn"),

		initialize: function(options) {
			$("body").removeClass();
			$("#btn-home").show();
			$("#btn-back").show();
			$("#div-app-title").html(lblNm("sales_dashboard"));
            //$("#div-app-title").removeClass("gmMediaVisibilityH").addClass("gmMediaVisibilityV");

			this.render(options);
		},

		// Render the initial contents when the Catalog View is instantiated
		render: function (options) {
			this.$el.html(this.template());
			this.strViewType = options[0];
				
			if(!this.strViewType) {
				this.strViewType = "overview";
			}
            if(GM.Global.DoValidation != undefined ){
                showError(GM.Global.DoValidation);
                GM.Global.DoValidation = undefined;
            }
			var savedZone = (localStorage.getItem("zoneid"))?localStorage.getItem("zoneid"):"", savedRegion = (localStorage.getItem("regionid"))?localStorage.getItem("regionid"):"", savedFieldSales = (localStorage.getItem("fieldsalesid"))?localStorage.getItem("fieldsalesid"):"";
			var savedCompany = (localStorage.getItem("companyid"))?localStorage.getItem("companyid"):"", savedDivision = (localStorage.getItem("divisionid"))?localStorage.getItem("divisionid"):"";
			var userCompany = localStorage.getItem("userCompany"), userDivision = localStorage.getItem("userDivision");

			if((navigator.onLine) && (!intOfflineMode)) {

				if((savedZone!="") || (savedRegion!="") || (savedFieldSales!="")) {	
					this.$("#div-dash-filter").addClass("bg-red");
					this.$("#btn-dash-filter-iphone").addClass("bg-red");
					
				}
				else if((savedCompany!="") || (savedDivision!="")) {
					if((savedCompany!=userCompany) || (savedDivision!=userDivision)) {
						this.$("#div-dash-filter").addClass("bg-red");
						this.$("#btn-dash-filter-iphone").addClass("bg-red");
					}
				}
				console.log(this.strViewType);
				switch(this.strViewType) {
					case "overview":
						this.sdoverview = new DashOverviewView({'el' :this.$('#div-dash-main-content'), 'parent' : this});
						this.$("#div-dash-title").html(lblNm("over_view"));

						this.display("overview");
						break;	

					case "morefilter":
						this.morefilter = new DashMoreFilterView({'el' :this.$('#div-dash-main-content'), 'parent' : this});	
						this.$("#div-dash-title").html(lblNm("more_filter"));

						this.$("#div-dash-filter").hide();
						this.$("#btn-dash-filter").addClass("btn-header-visited");
						if ($(window).width()<600) {
							this.$(".btn-detail").each(function() {
								$(this).removeClass("btn-selected");
							});
							this.$("#btn-dash-filter-iphone").addClass("btn-selected");
						}
						else {
							if(moreFilterHash.indexOf("overview")>0) 
								this.display("overview");
							else if(moreFilterHash.indexOf("saletoday")>0) 
								this.display("todaydetails");
							else if(moreFilterHash.indexOf("salemtd")>0) 
								this.display("mtd");
							else if(moreFilterHash.indexOf("mtdDrillDown")>0) 
								this.display("mtd");
						}
						break;
						
					case "saletoday":
						this.saletoday = new SaleTodayView({'el' :this.$('#div-dash-main-content'), 'parent' : this});	
						this.$("#div-dash-title").html(lblNm("todays_sales"));
						this.display("todaydetails");
						break;
						
					case "salemtd":
						this.salemtd = new SaleMTDView({'el' :this.$('#div-dash-main-content'), 'parent' : this});	
						this.$("#div-dash-title").html(lblNm("msg_month_to_date_sales"));
						this.display("mtd");
						break;

					case "drilldown":
						this.drilldown = new DashDrillDownView({'el' :this.$('#div-dash-main-content'), 'viewOptions': options, 'parent' : this});	
						this.$("#div-dash-filter").hide();
						if(moreFilterHash.indexOf("saletoday")>0) 
							this.display("todaydetails");
						break;

					case "mtdDrillDown":
						this.mtdDrillDown = new MTDDrillDownView({'el' :this.$('#div-dash-main-content'), 'filter' : options[1], 'parent' : this});	
						this.display("mtd");
						break;
                    case "postatus":
                            this.postatus = new PODashboardView ({'el' :this.$('#div-dash-main-content'), 'filter' : options, 'parent' : this});	
                            this.display("postatus");
                        break;
                    case "sop":
                            this.sop = new SOPDashView ({'el' :this.$('#div-dash-main-content'), 'filter' : options, 'parent' : this});	
                            this.$("#div-dash-title").html(lblNm("sop_header"));
                            this.display("sop");
                        break;
				}
			} else {

				this.$(".div-detail").hide();
				this.$("#div-dash-nointernet-connection").show();
				this.$("#div-dash-nointernet-connection").html(" <div> <i class='fa fa-exclamation-triangle fa-lg'></i>"+lblNm("msg_no_internet_connection")+ "</div>"+lblNm("msg_connect_wifi_cellular_dashboard"));
					switch(this.strViewType) {
					case "overview":
						this.$("#div-dash-title").html(lblNm("over_view"));
						this.display("overview");
						break;	

					case "morefilter":
						this.$("#div-dash-title").html(lblNm("more_filters"));
						this.$("#div-dash-filter").hide();
						this.$("#btn-dash-filter").addClass("btn-header-visited");
						if ($(window).width()<600) {
							this.$(".btn-detail").each(function() {
								$(this).removeClass("btn-selected");
							});
							this.$("#btn-dash-filter-iphone").addClass("btn-selected");
						}
						else {
							if(moreFilterHash.indexOf("overview")>0) 
								this.display("overview");
							else if(moreFilterHash.indexOf("saletoday")>0) 
								this.display("todaydetails");
							else if(moreFilterHash.indexOf("salemtd")>0) 
								this.display("mtd");
							else if(moreFilterHash.indexOf("mtdDrillDown")>0) 
								this.display("mtd");
						}
						break;
						
					case "saletoday":
						this.$("#div-dash-title").html(lblNm("todays_sales"));
						this.display("todaydetails");
						break;
						
					case "salemtd":
						this.$("#div-dash-title").html(lblNm("msg_month_to_date_sales"));
						this.display("mtd");
						break;

					case "drilldown":
						this.$("#div-dash-filter").hide();
						break;

					case "mtdDrillDown":
						this.$("#div-dash-title").html(lblNm("msg_month_to_date_sales"));
						this.display("mtd");
						break;
				}
			}
			$("#div-footer").addClass("hide");
			$('#div-app-title').removeClass('gmVisibilityHidden').addClass('gmVisibilityVisible');
			
            $("#div-detail-navigation").show();
			return this;
		},

		gotoDashboard: function() {
			window.location.href = "#dashboard";
		},
		
		sameRequest: function(data, action) {

			$(".div-req-data").each(function() { 

				var txnid = $(this).attr("txnid");
				var reqdtlid = $(this).attr("reqdtlid");
				
				if(reqdtlid !== data.reqdtlid) {
					if(txnid == data.txnid) {

						if(action == "set") {
							
							$(this).find(".li-whole-req").addClass("noWholeReq");
							$(this).find("#li-days select").val(data.newrtdt).trigger("change");
							$(this).find("#li-days select").addClass("noWholeReq");

						} else {
							
							$(this).find(".li-whole-req").removeClass("noWholeReq");
							$(this).find("#li-days select").val("").trigger("change");
							$(this).find("#li-days select").removeClass("noWholeReq");
						}
						
					}
				}
			    
			});
		},

		sameDates: function(newrtdt, txnid, reqdtlid) {

			$(".div-req-data").each(function() { 

				var tid = $(this).attr("txnid");
				var rdtlid = $(this).attr("reqdtlid");
				
				if(rdtlid !== reqdtlid) {
					if(tid == txnid) {

						$(this).find("#li-days select").val(newrtdt).trigger("change");
						
					}
				}
			    
			});
		},

		showRequestList: function(e) {
			var passname = $(e.currentTarget).attr("name");
            
			this.$("#div-dash-subtitle").html(lblNm("msg_extension_request"));
			var strReqList = arrRequest.join(',');

			console.log(strReqList);
			
			var that = this;

			fnGetReqExtn(passname, strReqList, function(data) {
				
				var i = 0, j = 0, xtlen = 0;
				var arrData = new Array();

				console.log(JSON.stringify(data));
				
				if(data.length==undefined) {
					arrData.push(data);	
				} else {
					arrData = data;
				}
				
				
				var l = arrData.length;
				var newextndateArray;

				for(i=0; i<l; i++) {

					var xt = new Array();
					newextndateArray = arrData[i].newextndate.split(",");
					xtlen = newextndateArray.length;

					for (j=0; j<xtlen; j++) {
						var seq = j+1;
						var xtdt = newextndateArray[j]
						var item = {"seq":seq, "xtdt":xtdt};
						xt.push(item);
					}

					arrData[i].newextndate = xt;

				}

				console.log(arrData);

				that.$("#div-request").show();

				that.$("#div-request").html(that.tplReqExtn(arrData));	 
                $(".surg-date1").attr("placeholder",localStorage.getItem("cmpdfmt").toLowerCase());
				that.$("#btn-extend-req").hide(); 
                that.$("#btn-generate-pricerequest").hide(); 
				that.$("#btn-req-submit").show();
				that.$("#btn-req-cancel").show();

			});
			
		},

		submitRequestList: function() {
//			if($("#btn-req-submit").text()=="Submit") {
				var that = this;
				var submit = 1;

				if(arrExtnData.length>0) {
					while(arrExtnData.length > 0) {
					    arrExtnData.pop();
					};	
				}
				

				$(".div-req-data").each(function() { 
					if(!$(this).find("#sel-days").hasClass("noWholeReq")) {
						var surgerydt = $(this).find("#surg-date").children(".surg-date1").val();
						var item = {"txnid" : $(this).attr("txnid"), "reqdtlid" : $(this).attr("reqdtlid"), "newextndate" : $(this).find("#li-new-ret-dt").attr("newretdt"), "consignid" : $(this).attr("consignid"), "extwholereq" : ($(this).find("#li-whole-req").attr("wholeReq"))?$(this).find("#li-whole-req").attr("wholeReq"):"N", "surgdt" : formattedDate(surgerydt, localStorage.getItem("cmpdfmt"))};
					    var newRetDt = $(this).find("#li-new-ret-dt").attr("newretdt");
                        if($(this).find("#surg-date").children(".surg-date1").length == 0)
                            var surgdt =true;
                        else
                            var surgdt = $(this).find("#surg-date").children(".surg-date1").val();
                         if(!surgdt){
                            submit = 2;
			                return false;
                        }
                        if($(this).find("#surg-date").children(".surg-date1").length > 0){
                           var selectedText = surgdt;
                          //var selectedText=formattedDate(selectedText, localStorage.getItem("cmpdfmt"));
                           var selectedDate = new Date(selectedText);
                           var now = new Date();
                           if (selectedDate <= now) {
                                submit = 3;
                                return false;
                            }
                        }
					    if(newRetDt) {
							arrExtnData.push(item);			  
					    } else {
					    	submit = 0;
			                return false;
					    }                      
					}
				});
            console.log(arrExtnData);
				if(submit == 1) {
					if($("#txtarea-req-comments").val().trim()=="") {
						showNativeAlert(lblNm("msg_fill_comments"), lblNm("msg_comments_required"), lblNm("msg_ok"));
					}
					else {
						$("#btn-req-submit").text(lblNm("msg_submitting"));
						var token = localStorage.getItem("token");
					    var input = {"token": token, "txt_logreason": $("#txtarea-req-comments").val(), "gmLoanerExtVO" : arrExtnData};

						fnSubmitReqExtn(input, function(data) {
							showNativeAlert(lblNm("msg_request_submit"), lblNm("msg_loaner_extension"), lblNm("msg_ok"), function (argument) {
								that.gotoDashboard();
				            });
						});	
					}
					
				}
				else if(submit==2)
                    showNativeAlert(lblNm("surg_date_cannot_empty"), lblNm("surg_date_required"), lblNm("msg_ok"));
                else if(submit==3)
                    showNativeAlert(lblNm("surg_date_cannot_past"), lblNm("surg_date_past"), lblNm("msg_ok"));
                else
                    showNativeAlert(lblNm("msg_date_cannot_empty"), lblNm("msg_date_required"), lblNm("msg_ok"));
//			}
		},

		setDate: function(event) {            
			var reqName = event.currentTarget.name;
			var retDt = event.currentTarget.options[event.currentTarget.selectedIndex].value;
            var extCount= event.currentTarget.options[event.currentTarget.selectedIndex].text;
            var surgdt= $(event.currentTarget).parent().parent().find("#surg-date").children('.surg-date1').val();
            var surgerydt = new Date(surgdt);
            var extretrunDt ;
            var day = surgerydt.getDay();
            
            if (extCount == "1" && day != 5 && day != 6)
            	extretrunDt = new Date(surgerydt.setDate(surgerydt.getDate() + 1));
			else if ((extCount == "1" && day == 5)|| (extCount == "3" && day == 1)
					|| (extCount == "3" && day == 2)|| (extCount == "2" && day == 6)|| (extCount == "3" && day == 0))
					extretrunDt = new Date(surgerydt.setDate(surgerydt.getDate() + 3));
			else if ((extCount == "2" && day == 4)|| (extCount == "2" && day == 5)
					|| (extCount == "4" && day == 1)|| (extCount == "4" && day == 0)|| (extCount == "3" && day == 6))
				extretrunDt = new Date(surgerydt.setDate(surgerydt.getDate() + 4));
		    else if ((extCount == "2" && day == 1)|| (extCount == "2" && day == 2)
					|| (extCount == "2" && day == 3)|| (extCount == "1" && day == 6)|| (extCount == "2" && day == 0))
				extretrunDt = new Date(surgerydt.setDate(surgerydt.getDate() + 2));
			else if ((extCount == "3" && day == 3)|| (extCount == "3" && day == 4)|| (extCount == "3" && day == 5)
					|| (extCount == "4" && day == 6)|| (extCount == "5" && day == 0))
				extretrunDt = new Date(surgerydt.setDate(surgerydt.getDate() + 5));
			else if ((extCount == "4" && day != 1 && day != 6)|| (extCount == "5" && day == 6))
				extretrunDt = new Date(surgerydt.setDate(surgerydt.getDate() + 6));
			else if ((extCount == "5")&& (day == 1 || day == 2 || day == 3 || day == 4 || day == 5))
				extretrunDt = new Date(surgerydt.setDate(surgerydt.getDate() + 7));
            
                        
            var day = ("0" + extretrunDt.getDate()).slice(-2);
            var month = ("0" + (extretrunDt.getMonth() + 1)).slice(-2);
            var retDt = (month)+"/"+(day)+"/"+extretrunDt.getFullYear() ;
            

			$(".retdt-" + reqName).html(formattedDate(retDt,localStorage.getItem("cmpdfmt")));
			$(".retdt-" + reqName).attr("newretdt", formattedDate(retDt,localStorage.getItem("cmpdfmt")));

			var txnid = $(event.currentTarget).parent().attr("txnid");
	     	var reqdtlid = $(event.currentTarget).parent().attr("reqdtlid");
	     	
	     	var chkYes = $(event.currentTarget).parent().parent().find("#li-whole-req").find("#lbl-req-yes");
	     	
	     	if(chkYes.hasClass("req-checked")) {
	     		this.sameDates(retDt, txnid, reqdtlid);
	     	}
	     	
		},
		setDatebySurg: function(event) {  
	           var reqName = event.currentTarget.name;
	           var extCount = $(event.currentTarget).parent("#surg-date").next("li").find("#sel-days option:selected").text();
            
	           if(extCount !='0'){
	                var surgdt= $(event.currentTarget).parent().parent().find("#surg-date").children('.surg-date1').val();
	                var surgerydt = new Date(surgdt);
	                var extretrunDt ;
	                var day = surgerydt.getDay();
	 
	           if (extCount == "1" && day != 5 && day != 6)
	                extretrunDt = new Date(surgerydt.setDate(surgerydt.getDate() + 1));
	            else if ((extCount == "1" && day == 5)|| (extCount == "3" && day == 1)
	                    || (extCount == "3" && day == 2)|| (extCount == "2" && day == 6)|| (extCount == "3" && day == 0))
	                    extretrunDt = new Date(surgerydt.setDate(surgerydt.getDate() + 3));
	            else if ((extCount == "2" && day == 4)|| (extCount == "2" && day == 5)
	                    || (extCount == "4" && day == 1)|| (extCount == "4" && day == 0)|| (extCount == "3" && day == 6))
	                extretrunDt = new Date(surgerydt.setDate(surgerydt.getDate() + 4));
	            else if ((extCount == "2" && day == 1)|| (extCount == "2" && day == 2)
	                    || (extCount == "2" && day == 3)|| (extCount == "1" && day == 6)|| (extCount == "2" && day == 0))
	                extretrunDt = new Date(surgerydt.setDate(surgerydt.getDate() + 2));
	            else if ((extCount == "3" && day == 3)|| (extCount == "3" && day == 4)|| (extCount == "3" && day == 5)
	                    || (extCount == "4" && day == 6)|| (extCount == "5" && day == 0))
	                extretrunDt = new Date(surgerydt.setDate(surgerydt.getDate() + 5));
	            else if ((extCount == "4" && day != 1 && day != 6)|| (extCount == "5" && day == 6))
	                extretrunDt = new Date(surgerydt.setDate(surgerydt.getDate() + 6));
	            else if ((extCount == "5")&& (day == 1 || day == 2 || day == 3 || day == 4 || day == 5))
	                extretrunDt = new Date(surgerydt.setDate(surgerydt.getDate() + 7));
	                 
	            var day = ("0" + extretrunDt.getDate()).slice(-2);
	            var month = ("0" + (extretrunDt.getMonth() + 1)).slice(-2);
	            var retDt = (month)+"/"+(day)+"/"+extretrunDt.getFullYear() ;
	         
	            $(".retdt-" + reqName).html(formattedDate(retDt,localStorage.getItem("cmpdfmt")));
	            $(".retdt-" + reqName).attr("newretdt", formattedDate(retDt,localStorage.getItem("cmpdfmt")));
	 
	            var txnid = $(event.currentTarget).parent().attr("txnid");
	             var reqdtlid = $(event.currentTarget).parent().attr("reqdtlid");
	             
	             var chkYes = $(event.currentTarget).parent().parent().find("#li-whole-req").find("#lbl-req-yes");
	             
	             if(chkYes.hasClass("req-checked")) {
	                 this.sameDates(retDt, txnid, reqdtlid);
	             }
	            }       
	        },

		toggleWholeReq:function(event){
		   	
		   	var id = (event.currentTarget.id);
		   	var chkYes = $(event.currentTarget).parent().find("#lbl-req-yes");
	    	var chkNo = $(event.currentTarget).parent().find("#lbl-req-no");

	    	chkYes.toggleClass("req-checked");
	     	chkNo.toggleClass("req-checked");

	     	var txnid = $(event.currentTarget).parent().attr("txnid");
	     	var reqdtlid = $(event.currentTarget).parent().attr("reqdtlid");
	     	var newrtdt = $(event.currentTarget).parent().parent().find("#li-new-ret-dt").attr("newretdt");
	     	var daysExtn = $(event.currentTarget).parent().parent().find("#li-days select option:selected").text();
	     	
	     	var reqDtl = {"txnid" : txnid, "reqdtlid" : reqdtlid, "newrtdt": newrtdt, "daysExtn" : daysExtn};

	     	if(chkYes.hasClass("req-checked")) {
	     		$(event.currentTarget).parent().attr("wholeReq", "Y");	
		     	this.sameRequest(reqDtl, "set");

	     	} else {
	     		$(event.currentTarget).parent().attr("wholeReq", "N");
	     		this.sameRequest(reqDtl, "");

	     	}

		},

		// Toggles the view
		display: function(key) {

			if(key.currentTarget==undefined) {
				this.key = key;
			}
			else {
				this.key = "overview";
				
			}
			this.$(".btn-detail").each(function() {
				$(this).removeClass("btn-selected");
			});

			this.$("#div-case-" + this.key).show();
			this.$("#btn-case-" + this.key).addClass("btn-selected");

		},
		
		// show navigation buttons for phone layout
		showNavigation: function(){			
			$("#div-detail-menu").each(function(){
	            if($(this).hasClass('detail-menu'))	
	            	$(this).removeClass("detail-menu");
	            else
	    	        $("#div-detail-menu").addClass("detail-menu");
	            })
			
			$("#div-detail-navigation" ).slideToggle("fast");
		},
		
		openLink: function() {
			var deptID = localStorage.getItem("Dept");
                _.each($(".in-app"),function(sdata){
                        _.each(localStorage.getItem("moduleaccess").split(','),function(data){
                            if($(sdata).attr("href").replace("#","") == data.replace("btn-",""))
                                $(sdata).addClass("hide");
                        });
                     
                });
			if(deptID == 2005) {
				$("#btn-case-link").each(function(){	
	            	$(this).toggleClass("show-app");
				})
				$(".btn-app-link").toggle("fast");
			}
            else{
                $(".in-app").addClass("hide");
                $(".btn-lnk-scorecard").removeClass("hide");
                $(".btn-app-link").toggle("fast");                
            }
             
			
		},
        
        openPoStatus:function(){
            $("#btn-case-link").each(function(){	
                $(this).toggleClass("show-app");
            })
            $(".btn-app-link").toggle("fast");
        },
        
         showDatepicker: function (event) {
                    var elem = event.currentTarget;
                    $($(elem).parent().find("input")).datepicker({
//                        maxDate: 0,
                        duration: 'fast'
                    });
                    var visible = $('#ui-datepicker-div').is(':visible');
                    $($(elem).parent().find("input")).datepicker(visible ? 'hide' : 'show');

                    event.stopPropagation();
                },
        
        formatcmpdate: function(event){
            var elem = event.currentTarget;
            var dt= $($(elem).parent().find("input")).val();
             console.log(dt);
            var compdtfmt=formattedDate(dt, localStorage.getItem("cmpdfmt"));
            $(elem).siblings(".label-surgery-date").html(compdtfmt);
            this.setDatebySurg(event);
        },
        generatePriceRequest: function() {
			var strOrderList = arrOrder.join(',');
			console.log(strOrderList);
			var that = this;
                var token = localStorage.getItem("token");
                var userid = localStorage.getItem("userID");
                var input = {
                    "token": token,
                    "orderids": strOrderList,
                    "userid":userid
                };
            this.loaderview = new LoaderView({
                    text: lblNm("msg_generate_price")
                });
             fnGetWebServerData("pricingRequest/heldOrderPriceRequest/", undefined, input, function (data) {
                    that.loaderview.close();
                    if (data != null) {
                       that.redirectToPricing(data.pricerequestid);
                    } else
                        showError(lblNm("msg_exception_order"));
                });
		},
     
        redirectToPricing:function(prid){
            window.location.href = "#price/pricingSubmittedRequests/"+prid;
		},
        
		// Close the view and unbind all the events
		close: function(e) {

			if(this.sdoverview)
				this.sdoverview.close();
			if(this.morefilter)
				this.morefilter.close();
			if(this.saletoday)
				this.saletoday.close();
			if(this.salemtd)
				this.salemtd.close();
			if(this.drilldown)
				this.drilldown.close();
			if(this.mtdDrillDown)
				this.mtdDrillDown.close();

			this.unbind();
			this.remove();

		}

	});	

	return DashMainView;
});
