/**********************************************************************************
 * File:        GmDashOverviewView.js
 * Description: View which loads the Overview widgets
 * Version:     1.0
 * Author:      tvenkateshwarlu
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification','datasync','saleitemmodel', 'saleitemcollection', 'saleitemlistview'
        
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification, Datasync, SaleItem, SaleItems, SaleItemListView) {

	'use strict';
	var DashOverviewView = Backbone.View.extend({

		events : {
			"click .div-dash-widget .fa-refresh, .div-widget-refresh": "reload",
			"click .ul-overview-widget-details" : "drillDown",
			"click .div-dash-widget-header" : "showDetails"
		},

		/* Load the templates */ 
		template: fnGetTemplate(URL_Dashboard_Template, "GmDashOverview"),

		tplSpinner: fnGetTemplate(URL_Common_Template, "GmProgressSpinner"),

		 initialize: function(options) {
			 this.el = options.el;

			 //Get the stored filters and token
			 this.serverInput = {
			 "token": localStorage.getItem("token"),
			 "companyid": (localStorage.getItem("companyid"))?localStorage.getItem("companyid"):localStorage.getItem("userCompany"),
			 "divids": (localStorage.getItem("divisionid"))?localStorage.getItem("divisionid"):localStorage.getItem("userDivision"),
			 "zoneids": (localStorage.getItem("zoneid"))?localStorage.getItem("zoneid"):"",
			 "regionids":(localStorage.getItem("regionid"))?localStorage.getItem("regionid"):"",
			 "fsids": (localStorage.getItem("fieldsalesid"))?localStorage.getItem("fieldsalesid"):""
			 };

			 this.render();
			 },
		
		render: function (options) {
			var that = this;
			this.serverInput.salesdbcurrtype = localStorage.getItem("salesDbCurrType");
			this.serverInput.salesdbcurrsymbol = localStorage.getItem("salesDbCurrSymbol");
			this.$el.html(this.template());
            this.fnGetAccItemConfl(function(){});
			this.$(".div-dash-page-loader").html(this.tplSpinner({"message":lblNm("msg_data_fetching")}));
			this.loadoverview("loaners/count", this.$("#div-dash-loaner-overview"), "GmSDItem", "listGmSalesDashResListVO");
			this.loadoverview("orders/count", this.$("#div-dash-order-overview"), "GmSDItem",  "listGmSalesDashResListVO");
			this.loadoverview("returns/count", this.$("#div-dash-return-overview"), "GmSDItem",  "listGmSalesDashResListVO");
			this.loadoverview("casescheduled/count", this.$("#div-dash-case-overview"), "GmSDItem",  "listGmSalesDashResListVO");
			this.loadoverview("consignset/count", this.$("#div-dash-consigned-sets-overview"), "GmSDItem",  "listGmSalesDashResListVO");
			this.loadoverview("consignitem/count", this.$("#div-dash-consigned-items-overview"), "GmSDItem",  "listGmSalesDashResListVO");
			if ($(window).width()<600) {
				this.loadoverview("quota/count", this.$("#div-dash-quota-mtd-overview"), "GmSDQuotaItem",  "listGmSalesDashGrowthResVO");
				this.loadoverview("quota/count", this.$("#div-dash-quota-ytd-overview"), "GmSDQuotaItem",  "listGmSalesDashGrowthResVO");
			}
			else {
				this.loadoverview("quota/count", this.$("#div-dash-quota-overview"), "GmSDQuotaItem",  "listGmSalesDashGrowthResVO");
			}
			
			return this;
		},
		
		reload: function(e) {
			var that= this;
			var targetid;
			
			if($(e.currentTarget).attr('class') == "div-widget-refresh") {
				
				targetid = $(e.target).parent().find(".div-dash-widget-header").attr('id');
			}
			
			else {
				targetid = $(e.target).parent().attr('id');
			}
		
			var servername, targetname, targettemplate, voname;

			switch(targetid) {
				case "widget-loaners":
					that.$("#div-dash-loaner-overview").empty();
					that.$("#div-dash-loaners .div-dash-page-loader").show();
					servername = "loaners/count", targetname=that.$("#div-dash-loaner-overview"), targettemplate="GmSDItem", voname="listGmSalesDashResListVO";	
					break;
				case "widget-orders":
					that.$("#div-dash-order-overview").empty();
					that.$("#div-dash-orders .div-dash-page-loader").show();
					servername = "orders/count", targetname=that.$("#div-dash-order-overview"), targettemplate="GmSDItem", voname="listGmSalesDashResListVO";
					break;
				case "widget-returns":
					that.$("#div-dash-return-overview").empty();
					that.$("#div-dash-returns .div-dash-page-loader").show();
					servername = "returns/count", targetname=that.$("#div-dash-return-overview"), targettemplate="GmSDItem", voname="listGmSalesDashResListVO";
					break;
				case "widget-case":
					that.$("#div-dash-case-overview").empty();
					that.$("#div-dash-case .div-dash-page-loader").show();
					servername = "casescheduled/count", targetname=that.$("#div-dash-case-overview"), targettemplate="GmSDItem", voname="listGmSalesDashResListVO";
					break;
				case "widget-consignset":
					that.$("#div-dash-consigned-sets-overview").empty();
					that.$("#div-dash-consigned-sets .div-dash-page-loader").show();
					servername = "consignset/count", targetname=that.$("#div-dash-consigned-sets-overview"), targettemplate="GmSDItem", voname="listGmSalesDashResListVO";
					break;
				case "widget-consignitem":
					that.$("#div-dash-consigned-items-overview").empty();
					that.$("#div-dash-consigned-items .div-dash-page-loader").show();
					servername = "consignitem/count", targetname=that.$("#div-dash-consigned-items-overview"), targettemplate="GmSDItem", voname="listGmSalesDashResListVO";
					break;
				case "widget-quota":
					if ($(".fa-arrow").css('display') != 'none') {
						targetid = $(e.target).parent().attr('id')
						if(targetid.indexOf("mtd") >= 0) {
							targetname=that.$("#div-dash-quota-mtd-overview");
							that.$("#div-dash-quota-mtd .div-dash-page-loader").show();
							that.$("#div-dash-quota-mtd-overview").empty();	
						}
						else {
							targetname=that.$("#div-dash-quota-ytd-overview");
							that.$("#div-dash-quota-ytd .div-dash-page-loader").show();
							that.$("#div-dash-quota-ytd-overview").empty();	
						}
					}
					else {
						targetname=that.$("#div-dash-quota-overview");
						that.$("#div-dash-quota .div-dash-page-loader").show();
						that.$("#div-dash-quota-overview").empty();	
					}									
					servername = "quota/count",  targettemplate="GmSDQuotaItem", voname="listGmSalesDashGrowthResVO";
					break;
			}
			
			this.loadoverview(servername, targetname, targettemplate, voname);
		},
		
		loadoverview: function(servername, targetname, targettemplate, voname) {
			var that=this;
			
			fnGetServerData(servername,voname, this.serverInput, function(data){

                if (servername == "quota/count") {
                    GM.Global.ToQuota = _.findWhere(data, {"type" : "(%) To Quota "}); 

                }
                if (servername == "loaners/count"){
                    _.each(data, function (item, index, items) {                        
                        if(index == 8 || index == 9)
                            item.count = localStorage.getItem("salesDbCurrSymbol") + formatCurrency(item.count,2);
                        
                    });
                }
				var salesDbCurrSymbol = localStorage.getItem("salesDbCurrSymbol");
				that.$("#span-quota-ov-mtd-cursym").html("("+salesDbCurrSymbol+")");
				that.$("#span-quota-ov-ytd-cursym").html("("+salesDbCurrSymbol+")");
				that.$(targetname).parent().find(".div-dash-widget-details").show();
				that.$(targetname).parent().find(".div-dash-page-loader").hide();
				that.showItemListView(targetname, data, targettemplate, URL_Dashboard_Template);
			});
		},

		drillDown: function (event) {
			var elem = event.currentTarget;
            var countShow = $(elem).find(".li-overview-widget-details-count").text();
            countShow = parseInt(countShow.replace(/[^\d.]/g, ''));
               if(countShow != 0){
				var drillDownUrl = ($(elem).attr("data-url")).replace("/resources/","").replace(/\//g,"&")
				var titleText = (($(elem).parent().parent().parent().attr("id")).replace("div-dash-","")).replace("-overview","").replace(/[^a-zA-Z0-9 ]/g, "");
				var subTitle = $(elem).find(".li-overview-widget-details-type").text().toLowerCase().replace(/ /g,'').replace(/[^a-zA-Z0-9 ]/g, "");

				if(titleText!="quota")
					window.location.href = "#dashboard/drilldown/" + titleText +"/"+ subTitle +"/"+ drillDownUrl;
			}
		},
		
		
		showItemListView: function(controlID, data, itemtemplate, template_URL) {
			var items = new SaleItems(data);	
			this.sditemlistview = new SaleItemListView(
					{ 
						el: controlID, 
						collection: items, 
						itemtemplate: itemtemplate,
						template_URL: template_URL,
						columnheader: 0,
						callbackOnReRender: this.formatQuota
					}
			);
				
		},

		formatQuota: function (option) {
			if(option.itemtemplate == "GmSDQuotaItem") {	
				option.$(".li-overview-widget-details-amount").each(function() {
					var value = $(this).text();
					if(value.indexOf("-") >= 0) {
						value = value.replace("-","");
						value = parseFloat(value);
						$(this).html("("+value.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,")+")");
						$(this).addClass("content-red");
					}
					else {
						value = parseFloat(value);
						$(this).html(value.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,"));
					}
				});
			}
			else if(option.el.id.replace("div-dash-","").replace("-overview","") == "loaner") {
				option.$(".li-overview-widget-details-type").each(function(){
					if($(this).text()=="Overdue") {
						$(this).parent().parent().addClass("bg-dash-brown");
					}
					else if($(this).text()=="Missing Parts") {
						$(this).parent().parent().addClass("bg-dash-brown");
					}
				});
			}
			else if(option.el.id.replace("div-dash-","").replace("-overview","") == "order") {
				option.$(".li-overview-widget-details-type").each(function(){
					if($(this).text()=="Pending PO > 21 days") {
						$(this).parent().parent().addClass("bg-dash-brown");
					}
					else if($(this).text()=="Pending PO 14 - 21 days") {
						$(this).parent().parent().addClass("bg-dash-golden");
					}
				});
			}
		},
		
		//mobile
		showDetails: function(e) {
			if ($(".fa-arrow").css('display') != 'none') {
				var elem = e.currentTarget;
				var parent = $(elem).parent().attr("id");
				this.$(".div-dash-widget-mobile").removeClass("div-dash-widget-mobile");
				this.$(".div-widget-refresh").hide();
				if($(elem).find(".fa-arrow").hasClass("fa-caret-right")) {
					this.$(".fa-arrow").removeClass("fa-caret-down").addClass("fa-caret-right");
					this.$(elem).find(".fa-arrow").removeClass("fa-caret-right").addClass("fa-caret-down");
					$("#"+parent).addClass("div-dash-widget-mobile");
					$("#"+parent).find(".div-widget-refresh").show();					
			        this.$("#div-dash-overview-main").animate({
					     scrollTop: this.$("#div-dash-overview-main").scrollTop() + $("#"+parent).position().top - 105
					}, "fast");
				}
				else {
					this.$(elem).find(".fa-arrow").addClass("fa-caret-right").removeClass("fa-caret-down");
					$("#"+parent).removeClass("div-dash-widget-mobile");
					$("#"+parent).find(".div-widget-refresh").hide();
				}
			}
		},
		// Function is used to get account item Consign display flag 
        fnGetAccItemConfl :function(cb){
            var that = this;
            fnGetRuleValueByCompany("SAL_DASH_ACC_ITM","SALEDASHTAB",localStorage.getItem("cmpid"),function(data){
            
             if (data.length > 0) {
                 
             if(data[0].RULEVALUE=='Y'){
                  
                 $("#widget-consignitem-label").html(lblNm("acc_consignment_items"));

                }
             }
              cb(data);
            }); 
        },
     
		// Close the view and unbind all the events
		close: function(e) {
			this.unbind();
			this.remove();
		}

	});	

	return DashOverviewView;
});
