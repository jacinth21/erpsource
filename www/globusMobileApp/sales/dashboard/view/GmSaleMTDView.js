/**********************************************************************************
 * File:        GmSaleMTDView.js
 * Description: 
 * Version:     1.0
 * Author:      Venkat
 **********************************************************************************/

define([
    'jquery', 'underscore', 'backbone',  'handlebars',
    'global','device','notification','datasync','saleitemmodel', 'saleitemcollection', 'itemview', 'dropdownview'
    
    ], 

    function ($, _, Backbone, Handlebars, Global, Device, Notification, Datasync, SaleItem, SaleItems, ItemView, DropDownView) {

	'use strict';
	var mtdsalesView = Backbone.View.extend({

		events : {
			"click .ul-dash-salemtd" : "drillDown",
			"click .div-dash-mtdsales-tab" : "toggleContent"
		},

		/* Load the templates */ 
		template: fnGetTemplate(URL_Dashboard_Template, "GmSaleMTD"),

		tplSpinner: fnGetTemplate(URL_Common_Template, "GmProgressSpinner"),

		//Initializes the view
		initialize: function(options) {
			this.el = options.el;
			this.parent = options.parent;
			 this.serverInput = {"token": localStorage.getItem("token"),"companyid": (localStorage.getItem("companyid"))?localStorage.getItem("companyid"):"","divids": (localStorage.getItem("divisionid"))?localStorage.getItem("divisionid"):"","zoneids": (localStorage.getItem("zoneid"))?localStorage.getItem("zoneid"):"","regionids":(localStorage.getItem("regionid"))?localStorage.getItem("regionid"):"","fsids": (localStorage.getItem("fieldsalesid"))?localStorage.getItem("fieldsalesid"):""};
			$("body").removeClass();
			this.render();
		},
		
		//Renders the template
		render: function (options) {
			var that = this;
			this.$el.html(this.template());
			this.serverInput.salesdbcurrtype = localStorage.getItem("salesDbCurrType");
			this.serverInput.salesdbcurrsymbol = localStorage.getItem("salesDbCurrSymbol");
			this.$(".div-dash-page-loader").html(this.tplSpinner({"message":lblNm("fetching_data")}));
			fnGetServerData("sales/mtdsales", undefined, this.serverInput, function(data){
				that.viewData = data;
				console.log("Target: " + data.tgtads);

				that.$(".div-dash-page-loader").html(that.tplSpinner({"message":lblNm("msg_loading_data_c")}));
				
				var subTitleAmountValue = that.numberWithCommas(that.viewData.value);
				var salesDbCurrSymbol = localStorage.getItem("salesDbCurrSymbol");
				if(subTitleAmountValue.indexOf("-")>=0) {
					subTitleAmountValue = salesDbCurrSymbol+"(" + subTitleAmountValue.replace("-","") + ")";
					that.parent.$("#div-dash-subtitle").html(" - " + subTitleAmountValue).addClass("content-red");
				}
				else
					that.parent.$("#div-dash-subtitle").html(" - " + salesDbCurrSymbol +""+ subTitleAmountValue);

				if(that.viewData.listGmSalesDashResListVO!=undefined) {
						that.showTable();
						that.showChart();
					}
					else{
                        var salesDbCurrSymbol = localStorage.getItem("salesDbCurrSymbol");
						that.$(".div-dash-page-loader").html(lblNm("msg_no_data"));
						that.parent.$("#div-dash-subtitle").html(" - "+salesDbCurrSymbol+"0.00");
					}
			});
			
			return this;
		},

		numberWithCommas:function(numdata){
	 		var amtdata = parseFloat(numdata).toFixed(2);
		 	return amtdata.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		},

		//Calls the showItemListView to display the data
		showTable: function() {
			var data = new Array();

			if(this.viewData.listGmSalesDashResListVO.length!=undefined)
				data = this.viewData.listGmSalesDashResListVO;
			else
				data.push(this.viewData.listGmSalesDashResListVO);

			var currWeekTotal = 0, currDOF = 0;

			for(var i=0;i<data.length;i++) {
				data[i].displayValue = this.numberWithCommas(data[i].value);
				
				if(currDOF < data[i].dof) {
					currDOF = data[i].dof;
					currWeekTotal = currWeekTotal + parseFloat(data[i].value);
					var model = new SaleItem(data[i]);
					var subView = new ItemView({model:model});
				    this.$("#div-saleitemlist").append(subView.render({template_URL : URL_Dashboard_Template, template: "GmSDMTDSaleItem"}).el);
				}

				else {
					var totalModel = new SaleItem({"label" : "Week", "value" : this.numberWithCommas(currWeekTotal)});
					var totalSubView = new ItemView({model:totalModel});
				    this.$("#div-saleitemlist").append(totalSubView.render({template_URL : URL_Dashboard_Template, template: "GmSaleMTDTotal"}).el);
				    currWeekTotal = parseFloat(data[i].value);
				    currDOF = data[i].dof;
				    var model = new SaleItem(data[i]);
					var subView = new ItemView({model:model});
				    this.$("#div-saleitemlist").append(subView.render({template_URL : URL_Dashboard_Template, template: "GmSDMTDSaleItem"}).el);
				}

			}


			if(currDOF == data[i-1].dof) {
				var weekModel = new SaleItem({"label" : "Week", "value" : this.numberWithCommas(currWeekTotal)});
				var weekSubView = new ItemView({model:weekModel});
			    this.$("#div-saleitemlist").append(weekSubView.render({template_URL : URL_Dashboard_Template, template: "GmSaleMTDTotal"}).el);
			}

			var salesDbCurrSymbol = localStorage.getItem("salesDbCurrSymbol");
	        this.$("#span-dash-mtdsales-amount").html("("+salesDbCurrSymbol+")");
			this.$(".div-dash-mtdsales-footer-value").html(this.numberWithCommas(this.viewData.value));
			this.$(".div-dash-mtdsales-ads-footer-value").html(salesDbCurrSymbol+"" + this.numberWithCommas(this.viewData.ads));


			this.$(".li-dash-mtdsales-amount").each(function() {
				var value = $(this).text();
				if(value.indexOf("-") >= 0) {
					value = value.replace("-","");
					$(this).html("("+value+")");
					$(this).addClass("content-red");
				}
			});

			this.$(".li-dash-mtdsales-total-value").each(function() {
				var value = $(this).text();
				if(value.indexOf("-") >= 0) {
					value = value.replace("-","");
					$(this).html("("+value+")");
					$(this).addClass("content-red");
				}
			});

		},

		//display the chart from the webservice data
		showChart: function() {
            var that = this, chartWidth = "100%", chartHeight = "75%";
            var salesDbCurrSymbol = localStorage.getItem("salesDbCurrSymbol");
            var adsTxt = "ADS\n"+salesDbCurrSymbol+"" + this.numberWithCommas(this.viewData.ads);
            var tgtadsTxt= "To Achieve Quota\n"+salesDbCurrSymbol+"" + this.numberWithCommas(this.viewData.tgtads);
            
            if(screen.availWidth <= 420) {
               chartWidth =  "95%";
               chartHeight =  "100%";
                $("#mtdsales-chart-startval-txt").html(adsTxt);
                $("#mtdsales-chart-endtval-txt").html(tgtadsTxt);
                adsTxt = "";
                tgtadsTxt = "";
            }
			this.chart = new FusionCharts({
				type: "column2d", //type of chart
				renderAt: "div-dash-mtdsales-chart", //chart div area where the chart has to rendered
				width: chartWidth, //width of the chart div area
				height: chartHeight, //height of the chart div area
				dataFormat: "json", //format of data
			});
			
			var contentData = new Array(), chartData = new Array(), highestValue = 0;

			if(this.viewData.listGmSalesDashResListVO.length!=undefined) {
				contentData = this.viewData.listGmSalesDashResListVO; 
            }
			else {
				contentData.push(this.viewData.listGmSalesDashResListVO);
            }
				for(var i=0;i<contentData.length;i++) {
					var date = contentData[i].name;  
					var elem = date.split('/');  
					var day = elem[1]; 
					var mth = elem[0];
					var year = elem[2];
					var columnData = {"Label" : day, "Value" : contentData[i].value, "tooltext": salesDbCurrSymbol+"" +this.numberWithCommas(contentData[i].value)};
					if(highestValue<contentData[i].value)
						highestValue = contentData[i].value;
					chartData.push(columnData);
				}

				if(highestValue<parseInt(that.viewData.tgtads))
					highestValue = that.viewData.tgtads;

				highestValue = parseInt(highestValue);

				highestValue += (highestValue/20);
				console.log(highestValue);
				var chartOptions = {
				showValues: "0", //hide the values
				xAxisName: "Sales By Date of " +mth + "/" +year,
				xAxisNamePadding:"20",
				bgColor: "#FFFFFF",
				showalternatehgridcolor: "0",
				showBorder: "0",
				paletteColors: "#00366e", //hex dec color codes seperated by comma
				plotGradientColor: "",
				yAxisMaxValue: highestValue,
				adjustDiv: "0",
				yAxisMinValue: "0",
				numDivLines: 5,
				theme: "zune" //refers to theme

			};

			var combinedChartData = {"chart" : chartOptions, "data" : chartData,   "trendlines": [
					        {"line": [
					                {
					                    "startvalue": that.viewData.ads.toString(),
					                    "color": "#FBB917",
					                    "displayvalue": adsTxt,
					                    "thickness": "3",
					                    "showontop" : "1",
					                    "valueonright" : "1"
					                },

					                {
					                    "startvalue": that.viewData.tgtads.toString(),
					                    "color": "#FF8C00",
					                    "displayvalue": tgtadsTxt,
					                    "thickness": "3",
					                    "showontop" : "1",
					                    "valueonright" : "1"
					                },
								]}
					    	]
     			};
  
			this.chart.setChartData(combinedChartData, "json");
            this.chart.render();
//			$(window).on('orientationchange', this.renderChart);
			that.$("#div-dash-salemtd-main").show();
			that.$(".div-dash-page-loader").hide();
//			this.renderChart();
		},

		renderChart: function() {
			
			switch(window.orientation) 
		    {  
		      case -90:
		      case 90:
		    	  if(screen.availHeight <= 548 && screen.availWidth == 320) {
				        chart.resizeTo("310","335");
				        chart.render();	 
		    	  }
		    	  else
		    		  {
				     	chart.resizeTo("1000","500");
				        chart.render();		    		  
		    		  }
		        break; 
		      default:
		    	  if(screen.availHeight <= 548 && screen.availWidth == 320) {
				        chart.resizeTo("310","335");
				         chart.render();	 
		    	  }
		    	  else
		    		  {
				        chart.resizeTo("800","800");
				        chart.render();
		    		  }
		        break; 
		    }
			
		},

		drillDown: function(event) {
			var date = $(event.currentTarget).find(".li-dash-mtdsales-date").text();
			if(date!="") 
				window.location.href = "#dashboard/mtddrilldown/" + date.replace(/\//g,'&');
		},

		toggleContent: function (event) {
			var id = (event.currentTarget.id).replace("div-dash-mtdsales-tab-","");
			this.$(".selected-tab").removeClass("selected-tab");
			$(event.currentTarget).addClass("selected-tab");

			if(id=="table") {
				this.$("#div-dash-mtdsales-data").show();
				this.$("#div-dash-mtdsales-chart").hide();
                $("#div-dash-mtdsales-chart-trent-legend").addClass("hide");
                
                    
                        
                
                
				// FusionCharts(this.chart).dispose();

			}
			else {
				this.$("#div-dash-mtdsales-data").hide();
				this.$("#div-dash-mtdsales-chart").show();
//				this.renderChart();
                if(screen.availWidth<=420)
                    {
                        $("#div-dash-mtdsales-chart-trent-legend").removeClass("hide");
                    }
                
			}

		},	

		//Initializes the ItemListView in the given HTML Element with the given collection
		showItemListView: function(controlID, data, itemtemplate, template_URL) {
			var items = new SaleItems(data);
			var count = items.ammountTotal();
			var subTitleAmountValue = count.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
			if(subTitleAmountValue.indexOf("-")>=0) {
				subTitleAmountValue = "$(" + subTitleAmountValue.replace("-","") + ")";
				this.parent.$("#div-dash-subtitle").html(" - " + subTitleAmountValue).addClass("content-red");
			}
			else
				this.parent.$("#div-dash-subtitle").html(" - $" + subTitleAmountValue);

			this.sditemlistview = new SaleItemListView(
					{ 
						el: controlID, 
						collection: items, 
						itemtemplate: itemtemplate,
						template_URL: template_URL,
						columnheader: 0
					}
			);
		},
		
		// Close the view and unbind all the events
		close: function(e) {
			this.unbind();
			this.remove();
		}

	});	

	return mtdsalesView;
});
