/**********************************************************************************
 * File:        GmDashMoreFilterView.js
 * Description: Display More Filters options to save and use in the future
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/

define([
    'jquery', 'underscore', 'backbone',  'handlebars',
    'global','device','notification','datasync','saleitemmodel', 'saleitemcollection', 'saleitemlistview', 'dropdownview'
    
    ], 

    function ($, _, Backbone, Handlebars, Global, Device, Notification, Datasync, SaleItem, SaleItems, SaleItemListView, DropDownView) {

	'use strict';
	var DashMoreFilterView = Backbone.View.extend({

		events : {
			"click .li-div-dash-morefilter-saleitemlist": "select",
			"click #btn-dashboard-morefilter-save" : "saveMoreFilter",
			"click #btn-dashboard-morefilter-reset" : "configInitialFilter",
			"keyup .txt-dash-morefilter-search" : "searchKeyword",
			"click .div-dash-morefilter-search-x": "clearKeyword",
			"click .div-dash-morefilter-content-header": "showMoreFilter"
		},

		/* Load the templates */ 
		template: fnGetTemplate(URL_Dashboard_Template, "GmDashMoreFilter"),

		tplSpinner: fnGetTemplate(URL_Common_Template, "GmProgressSpinner"),

		//Initializes the view
		initialize: function(options) {
			this.el = options.el;
			this.render();
			$("#btn-home").show();
		},
		
		//Renders the template
		render: function (options) {
			var that = this;
			this.$el.html(this.template());
			this.$(".div-dash-page-loader").html(this.tplSpinner({"message":lblNm("msg_fetching_filter_data")}));
			this.showFilter();
			return this;
		},

		//Calls the web service to get the data
		//And also process the retieved data and prepares proper collections
		showFilter: function() {
			var that = this;
			fnGetServerData("salesfilter/morefilter",undefined,undefined,function (data) {
				that.$("#div-dash-morefilter-main").show();
				that.$(".div-dash-page-loader").hide();
				//seperates the data
				that.comapanyData = that.processData(data.listGmCompanyListVO,"company");
				that.divisionData = that.processData(data.listGmSlsDivisionListVO,"division");
				that.zoneData = that.processData(data.listGmSlsZoneListVO,"zone");
				that.regionData = that.processData(data.listGmSlsRegionListVO,"region");
				that.fieldsalesData = that.processData(data.listGmFieldSalesListVO,"fieldsales");

				//creates collection for each data
				that.divisionCollection = new SaleItems(that.divisionData);
				that.zoneCollection = new SaleItems(that.zoneData);
				that.regionCollection = new SaleItems(that.regionData);
				that.fieldsalesCollection = new SaleItems(that.fieldsalesData);

				that.configInitialFilter();				
			})
		},

		//Configures the collections and already stored filters.
		//Also used to reset the more filter
		configInitialFilter: function (event) {

			//Get the Stored Filters
			var storedCompany = (localStorage.getItem("companyid"))?localStorage.getItem("companyid"):localStorage.getItem("userCompany"), storedDivision = (localStorage.getItem("divisionid"))?localStorage.getItem("divisionid"):localStorage.getItem("userDivision"), storedZone = localStorage.getItem("zoneid"), storedRegion = localStorage.getItem("regionid"), storedFieldSales = localStorage.getItem("fieldsalesid");
			var clearSaved = false;
	
			if(event!=undefined) {
				storedCompany = localStorage.getItem("userCompany");
				storedDivision = localStorage.getItem("userDivision");
				storedZone = "";
				storedRegion = "";
				storedFieldSales = "";
				clearSaved = true;

				//creates collection for each data
				this.divisionCollection = new SaleItems(this.divisionData);
				this.zoneCollection = new SaleItems(this.zoneData);
				this.regionCollection = new SaleItems(this.regionData);
				this.fieldsalesCollection = new SaleItems(this.fieldsalesData);
			}			

			if(storedCompany!='100803') {
				this.divisionCollection = new SaleItems(this.divisionCollection.filterBy("companyid", storedCompany));
				this.zoneCollection = new SaleItems(this.zoneCollection.filterBy("companyid", storedCompany));
				this.regionCollection = new SaleItems(this.regionCollection.filterBy("companyid", storedCompany));
				this.fieldsalesCollection = new SaleItems(this.fieldsalesCollection.filterBy("companyid", storedCompany));
			}
			else {
				this.divisionCollection = new SaleItems(this.divisionCollection.getUniqBy('ID'));
				this.zoneCollection = new SaleItems(this.zoneCollection.getUniqBy('ID'));
				this.regionCollection = new SaleItems(this.regionCollection.getUniqBy('ID'));
				this.fieldsalesCollection = new SaleItems(this.fieldsalesCollection.getUniqBy('ID'));
			}

			//Select the Stored Filters
			if(storedDivision!=null) 
				this.divisionCollection.selectOnly(storedDivision.split(","));
			if(storedZone!=null)
				this.zoneCollection.selectOnly(storedZone.split(","));
			if(storedRegion!=null)
				this.regionCollection.selectOnly(storedRegion.split(","));
			if(storedFieldSales!=null)
				this.fieldsalesCollection.selectOnly(storedFieldSales.split(","));

			//Declaring the Current collection as the initial collection
			this.currDivisionCollection = this.divisionCollection;
			this.currZoneCollection = this.zoneCollection;
			this.currRegionCollection = this.regionCollection;
			this.currFieldSalesCollection = this.fieldsalesCollection;

			this.showCompany(storedCompany);
			this.showDivision();

			if(storedDivision!=null) 
				this.updateZone(clearSaved);
			else
				this.showZone();
			if(storedZone!=null)
				this.updateRegion(clearSaved);
			else
				this.showRegion();
			if(storedRegion!=null)
				this.updateFieldSales(clearSaved);
			else
				this.showFieldSales();
			
		},

		//Updates the whole filter to the updates company
		updateFilter: function(companyId) {
			//creates collection for each data
			this.divisionCollection = new SaleItems(this.divisionData);
			this.zoneCollection = new SaleItems(this.zoneData);
			this.regionCollection = new SaleItems(this.regionData);
			this.fieldsalesCollection = new SaleItems(this.fieldsalesData);

			if(companyId!='100803') {
				this.divisionCollection = new SaleItems(this.divisionCollection.filterBy("companyid", companyId));
				this.zoneCollection = new SaleItems(this.zoneCollection.filterBy("companyid", companyId));
				this.regionCollection = new SaleItems(this.regionCollection.filterBy("companyid", companyId));
				this.fieldsalesCollection = new SaleItems(this.fieldsalesCollection.filterBy("companyid", companyId));
			}
			else {
				this.divisionCollection = new SaleItems(this.divisionCollection.getUniqBy('ID'));
				this.zoneCollection = new SaleItems(this.zoneCollection.getUniqBy('ID'));
				this.regionCollection = new SaleItems(this.regionCollection.getUniqBy('ID'));
				this.fieldsalesCollection = new SaleItems(this.fieldsalesCollection.getUniqBy('ID'));
			}

			//Declaring the Current collection as the initial collection
			this.currDivisionCollection = this.divisionCollection;
			this.currZoneCollection = this.zoneCollection;
			this.currRegionCollection = this.regionCollection;
			this.currFieldSalesCollection = this.fieldsalesCollection;

			this.showDivision();
			this.showZone();
			this.showRegion();
			this.showFieldSales();

		},

		//process the given data to form proper ID and Name
		processData: function(data,key) {
			var strData = JSON.stringify(data);
			var finalData = new Array();
			var processedData = JSON.parse(strData,function  (k, v) {
				if (k === (key+"id")) 
			        this.ID = v;
			    else if (k === (key+"nm"))
			        this.Name = v;
			    else
			        return v;
			});

			if(processedData.length==undefined) {
				finalData.push(processedData);
				return finalData; 
			}
			else
				return processedData;
		},

		//Saves the Selected Filter options in the Local Storage
		saveMoreFilter: function() {
			localStorage.setItem("companyid",this.companyDrpdownView.getCurrentValue()[0]);
			localStorage.setItem("divisionid",(this.currDivisionCollection.getSelected()).join(","));
			localStorage.setItem("zoneid",(this.currZoneCollection.getSelected()).join(","));
			localStorage.setItem("regionid",(this.currRegionCollection.getSelected()).join(","));
			localStorage.setItem("fieldsalesid",(this.currFieldSalesCollection.getSelected()).join(","));

			window.location.href = moreFilterHash;
		},

		//Initailize Dropdown view for Company
		showCompany: function(selectedCompany) {
			var that = this;

			this.companyDrpdownView = new DropDownView({
				el : this.$("#drp-dashboard-morefilter-company"),
				data : this.comapanyData,
				DefaultID : selectedCompany
			});

			this.$("#drp-dashboard-morefilter-company").bind('onchange', function (e, selectedId) {
					that.updateFilter(selectedId);
			});

		},

		//Display the Divisions in the current division collection
		showDivision: function () {
			this.divisionItemList = this.showSaleItemListView(this.$(".div-dash-morefilter-division-content"), this.currDivisionCollection);

			//Highlight Selected Area
			var seleced = this.currDivisionCollection.getSelected();
			for(var i=0;i<seleced.length;i++) {
				this.$(".div-dash-morefilter-division-content").find("#"+seleced[i]).parent().addClass("selected");
			}

			//Show / Hide Search Textbox depending on the number of items in the list
			if(this.currDivisionCollection.size()>5) 
				this.$(".div-dash-morefilter-division-header").find(".div-dash-morefilter-search").show();
			else
				this.$(".div-dash-morefilter-division-header").find(".div-dash-morefilter-search").hide();

			this.$(".div-dash-morefilter-division-header").find(".txt-dash-morefilter-search").val("");

		},

		//Display the Zones in the current zone collection
		showZone: function () {
			this.currZoneCollection.sortAsc("Name");
			this.zoneItemList = this.showSaleItemListView(this.$(".div-dash-morefilter-zone-content"), this.currZoneCollection);

			//Highlight Selected Area
			var seleced = this.currZoneCollection.getSelected();
			for(var i=0;i<seleced.length;i++) {
				this.$(".div-dash-morefilter-zone-content").find("#"+seleced[i]).parent().addClass("selected");
			}

			//Show / Hide Search Textbox depending on the number of items in the list
			if(this.currZoneCollection.size()>5) {
				this.$(".div-dash-morefilter-zone-header").find(".div-dash-morefilter-search").show();
				this.$(".div-dash-morefilter-zone").find(".div-dash-morefilter-search").removeClass('dash-hide');
			}
			else {
				this.$(".div-dash-morefilter-zone-header").find(".div-dash-morefilter-search").hide();
				this.$(".div-dash-morefilter-zone").find(".div-dash-morefilter-search").addClass('dash-hide');
			}
				

			this.$(".div-dash-morefilter-zone-header").find(".txt-dash-morefilter-search").val("");

		},

		//Display the Regions in the current region collection
		showRegion: function () {
			this.currRegionCollection.sortAsc("Name");
			this.regionItemList = this.showSaleItemListView(this.$(".div-dash-morefilter-region-content"), this.currRegionCollection);

			//Highlight Selected Area
			var seleced = this.currRegionCollection.getSelected();
			for(var i=0;i<seleced.length;i++) {
				this.$(".div-dash-morefilter-region-content").find("#"+seleced[i]).parent().addClass("selected");
			}

			//Show / Hide Search Textbox depending on the number of items in the list
			if(this.currRegionCollection.size()>5) {
				this.$(".div-dash-morefilter-region-header").find(".div-dash-morefilter-search").show();
				this.$(".div-dash-morefilter-region").find(".div-dash-morefilter-search").removeClass('dash-hide');
			}
				
			else {
				this.$(".div-dash-morefilter-region-header").find(".div-dash-morefilter-search").hide();
				this.$(".div-dash-morefilter-region").find(".div-dash-morefilter-search").addClass('dash-hide');
			}
				

			this.$(".div-dash-morefilter-region-header").find(".txt-dash-morefilter-search").val("");

		},

		//Display the FieldSales in the current Field Sales collection
		showFieldSales: function () {
			this.currFieldSalesCollection.sortAsc("Name");
			this.fieldsalesItemList = this.showSaleItemListView(this.$(".div-dash-morefilter-fieldsales-content"), this.currFieldSalesCollection);

			//Highlight Selected Area
			var seleced = this.currFieldSalesCollection.getSelected();
			for(var i=0;i<seleced.length;i++) {
				this.$(".div-dash-morefilter-fieldsales-content").find("#"+seleced[i]).parent().addClass("selected");
			}

			//Show / Hide Search Textbox depending on the number of items in the list
			if(this.currFieldSalesCollection.size()>5) {
				this.$(".div-dash-morefilter-fieldsales-header").find(".div-dash-morefilter-search").show();
				this.$(".div-dash-morefilter-fieldsales").find(".div-dash-morefilter-search").removeClass('dash-hide');
			}
			else {
				this.$(".div-dash-morefilter-fieldsales-header").find(".div-dash-morefilter-search").hide();
				this.$(".div-dash-morefilter-fieldsales").find(".div-dash-morefilter-search").addClass('dash-hide');
			}
				

			this.$(".div-dash-morefilter-fieldsales-header").find(".txt-dash-morefilter-search").val("");

		},

		//Selects the cliked or touched item and calls the updation of other widgets
		select: function(event) {
			var selectedArea = $(event.currentTarget).parent().parent().parent().parent().attr("data-widget");		//Determines the widget of the target item (division / zone / region / fieldsales)
			var selectedID = $(event.currentTarget).attr("id");
			$(event.currentTarget).parent().toggleClass("selected");
			switch(selectedArea) {
				case "division":
					this.currDivisionCollection.toggleSelect(selectedID);
					this.updateZone(true);
					break;

				case "zone":
					this.currZoneCollection.toggleSelect(selectedID);
					this.updateRegion(true);
					break;
				
				case "region":
					this.currRegionCollection.toggleSelect(selectedID);
					this.updateFieldSales(true);
					break;

				case "fieldsales":
					this.currFieldSalesCollection.toggleSelect(selectedID);
			}

		},

		//Gets the selected Divsions, filters the zone collection based on the selected divisions, updates the current collection
		updateZone: function(clearSaved) {
			var selectedDivisions = this.currDivisionCollection.getSelected();
			if(clearSaved)
				this.zoneCollection.clearAllSelected();
			if(selectedDivisions.length>0)
				var filteredZone = this.zoneCollection.filterBy("divisionid",selectedDivisions);
			else
				var filteredZone = this.zoneCollection.filterBy("divisionid",_.pluck(this.currDivisionCollection.toJSON(),'ID'));
			this.currZoneCollection = new SaleItems(filteredZone);
			this.showZone();
			this.updateRegion(clearSaved);
		},

		//Gets the selected Zones, filters the region collection based on the selected zones, updates the current collection
		updateRegion: function(clearSaved) {
			var selectedZones = this.currZoneCollection.getSelected();
			if(clearSaved)
				this.regionCollection.clearAllSelected();
			if(selectedZones.length>0)
				var filteredRegion = this.regionCollection.filterBy("zoneid",selectedZones);
			else
				var filteredRegion = this.regionCollection.filterBy("zoneid",_.pluck(this.currZoneCollection.toJSON(),'ID'));
			this.currRegionCollection = new SaleItems(filteredRegion);
			this.showRegion();
			this.updateFieldSales(clearSaved);
		},

		//Gets the selected Regions, filters the field sales collection based on the selected regions, updates the current collection
		updateFieldSales: function(clearSaved) {
			var selectedRegions = this.currRegionCollection.getSelected();
			if(clearSaved)
				this.fieldsalesCollection.clearAllSelected();
			if(selectedRegions.length>0)
				var filteredFieldSales = this.fieldsalesCollection.filterBy("regionid",selectedRegions);
			else
				var filteredFieldSales = this.fieldsalesCollection.filterBy("regionid",_.pluck(this.currRegionCollection.toJSON(),'ID'));
			this.currFieldSalesCollection = new SaleItems(filteredFieldSales);
			this.showFieldSales();
		},

		//Search the Collection
		searchKeyword: function (event) {
			var strWidget;
			if ($(".fa-phone-arrow").css('display') != 'none') {
				strWidget = $(event.currentTarget).parent().parent().attr("data-widget").toLowerCase();
			}
			else{
				strWidget = $(event.currentTarget).parent().parent().parent().attr("data-widget").toLowerCase();
			}	
			var searchData = $(event.currentTarget).val();
			var wordLen = searchData.length;
			this.setX(strWidget,wordLen);
			switch(strWidget) {
				case "division":
					this.divisionItemList.searchItemList(event);
					break;
				case "zone":
					this.zoneItemList.searchItemList(event);
					break;
				case "region":
					this.regionItemList.searchItemList(event);
					break;
				case "fieldsales":
					this.fieldsalesItemList.searchItemList(event);
					break;
			}
		},

		//iPhone Layout - Expanding and Contracing
		showMoreFilter: function (e) {
			if ($("#div-dash-morefilter-companycontent").css('display') == 'none') {
				var elem = e.currentTarget;
				var parent = $(elem).parent();
				this.$(".div-dash-morefilter-content-expand").removeClass("div-dash-morefilter-content-expand");
				if($(elem).find("i").hasClass("fa-caret-right")) {
					this.$(".fa-caret-down").removeClass("fa-caret-down").addClass("fa-caret-right");
					this.$(elem).find("i").removeClass("fa-caret-right").addClass("fa-caret-down");
					$(parent).addClass("div-dash-morefilter-content-expand");
					this.$("#div-dash-morefilter-content-phone").animate({
					     scrollTop: this.$("#div-dash-morefilter-content-phone").scrollTop() + $(parent).position().top - 230
					}, "fast");
				}
				else {
					this.$(elem).find("i").addClass("fa-caret-right").removeClass("fa-caret-down");
					$(parent).removeClass("div-dash-morefilter-content-expand");
				}

			}
		},

		//Initializes the SaleItemListView in the given HTML Element with the given collection
		showSaleItemListView: function(controlID, collection) {
			var sditemlistview = new SaleItemListView(
				{ 
					el: controlID, 
					collection: collection, 
					itemtemplate: "GmMoreFilterItem",
					template_URL: URL_Dashboard_Template,
					columnheader: 0
				}
			);

			return sditemlistview;
		},

		setX: function(strWidget,wordLength) {
					var target = ".div-dash-morefilter-"+strWidget;
					if(wordLength == 0) {
						this.$(target).find(".div-dash-morefilter-search-x").hide();
					}
					else {
						this.$(target).find(".div-dash-morefilter-search-x").css("display","inline-block");	
					}
		},
		
		clearKeyword: function(e) {

			var currElem = $(e.currentTarget).parent().find('input');
			console.log(currElem);
			$(currElem).val("");
			$(currElem).trigger("keyup");
			$(e.currentTarget).hide();
		    
		},
		
		// Close the view and unbind all the events
		close: function(e) {
			this.unbind();
			this.remove();
		}

	});	

	return DashMoreFilterView;
});