/**********************************************************************************
 * File:        GmMTDDrillDownView.js
 * Description: 
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/

define([
    'jquery', 'underscore', 'backbone',  'handlebars',
    'global','device','notification','datasync','saleitemmodel', 'saleitemcollection', 'itemview', 'saleitemlistview', 'daterangeview'
    
    ], 

    function ($, _, Backbone, Handlebars, Global, Device, Notification, Datasync, SaleItem, SaleItems, ItemView, SaleItemListView, DateRangeView) {

	'use strict';
	var MTDDrillDownView = Backbone.View.extend({

		events : {
			"click .li-dash-mtddrilldown-list-header" : "processAccountInfo",
			"click .div-dash-mtddrilldown-tab" : "toggleContent"
		},

		/* Load the templates */ 
		template: fnGetTemplate(URL_Dashboard_Template, "GmMTDDrillDown"),
		tplSpinner: fnGetTemplate(URL_Common_Template, "GmProgressSpinner"),
		template_legends: fnGetTemplate(URL_Dashboard_Template, "GmSaleTodayChartLegends"),
		template_date: fnGetTemplate(URL_Dashboard_Template, "GmMTDDrillDownDate"),

		//Initializes the view
		initialize: function(options) {
			this.parent = options.parent;
			this.el = options.el;
			this.serverInput = {"token": localStorage.getItem("token"),"companyid": (localStorage.getItem("companyid"))?localStorage.getItem("companyid"):"","divids": (localStorage.getItem("divisionid"))?localStorage.getItem("divisionid"):"","zoneids": (localStorage.getItem("zoneid"))?localStorage.getItem("zoneid"):"","regionids":(localStorage.getItem("regionid"))?localStorage.getItem("regionid"):"","fsids": (localStorage.getItem("fieldsalesid"))?localStorage.getItem("fieldsalesid"):""};
			this.serverInput.filterby = options.filter.replace(/\&/g,"/");
			this.render(true);
		},
		
		//Renders the template
		render: function (rerendervalue) {
			var that = this;
			this.serverInput.salesdbcurrtype = localStorage.getItem("salesDbCurrType");
			this.serverInput.salesdbcurrsymbol = localStorage.getItem("salesDbCurrSymbol");
			this.parent.$("#div-dash-title").html(this.template_date(this.serverInput));
			this.parent.$("#dte-dash-mtddrilldown").datepicker( {  maxDate: 0,  duration: 'fast'});
			this.parent.$("#dte-dash-mtddrilldown").change(function() {
				console.log(this.value);
				that.serverInput.filterby = this.value;
						that.render(false);
			});
			if(rerendervalue) {
			
				this.parent.$("#div-dash-title").click(function() {
					   var visible = $('#ui-datepicker-div').is(':visible');
					   console.log(visible);
					$("#dte-dash-mtddrilldown").datepicker(visible? 'hide' : 'show');
				});
				this.$el.html(this.template());
			}
			
			this.$("#div-dash-salemtd-main").hide();
			this.$(".div-dash-page-loader").html(this.tplSpinner({"message":lblNm("msg_data_fetching")}));
			fnGetServerData("sales/dailysalesdetail", undefined, this.serverInput, function(data){
				that.$(".div-dash-page-loader").html(that.tplSpinner({"message":lblNm("msg_loading_data_c")}));
				
				that.viewData = new Array();
				if(data.listGmSalesDrilldownVO==undefined) {
					that.$(".div-dash-page-loader").hide();
					that.$(".div-dash-mtd-nodata ").show();
					that.$("#div-dash-mtddrilldown-main").hide();
					$("#div-dash-subtitle").html(" - $0.00");
					that.$(".div-dash-mtd-nodata").html(lblNm("msg_no_data_display"));
					that.dateRange =  new DateRangeView({
						'el' :that.$('#div-dash-mtddrilldown-date'), 
						'dateformat' : 'mm-dd-yyyy', 
						'caption1' : '',
						'date1' : (that.serverInput.filterby).replace(/\//g,"-"),
						'usage' : '1' });
						that.$("#div-dash-mtddrilldown-date").bind("onchange",function(e,date) {
						that.serverInput.filterby = date.replace(/\-/g,"/");
						that.render(false);
				})
					
				}
				else{
					that.$(".div-dash-mtd-nodata ").hide();
					that.$("#div-dash-mtddrilldown-main").show();
					if(data.listGmSalesDrilldownVO.length!=undefined) 
						that.viewData = data.listGmSalesDrilldownVO;
					else
						that.viewData.push(data.listGmSalesDrilldownVO);
					that.processData();
				}
				
			});
			
			return this;
		},
		numberWithCommas:function(x){
			return parseFloat(x).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		},

		processData: function() {
			var colors = ["#006633", "#FFCC33", "#FF00FF", "#0000FF", "#33FF33","#660033","#606060", "#66FFFF","#CC6600", "#666633","#FFB84D","#4D944D","#2952CC","#AD3333","#CC7A00","#85ADFF","#FF66FF","#6B008F","#E6005C","#B2FF99"];
			for(var i=0; i<this.viewData.length; i++) {
				this.viewData[i].displayAmount = this.numberWithCommas(this.viewData[i].amount);
			}
			
			this.mainCollection = new SaleItems(this.viewData);
			var arrFieldSalesID = _.uniq(_.pluck(this.viewData,"fieldsalesid"));
			var arrFieldSalesName = _.uniq(_.pluck(this.viewData,"fieldsalesnm"));
			var arrDisplay = new Array();

			for(var i=0; i<arrFieldSalesID.length; i++) {
				var jsnData = {};
				var collection = new SaleItems(this.mainCollection.filterBy("fieldsalesid",arrFieldSalesID[i]));
				jsnData.fsAmount = collection.ammountTotal("amount");
				jsnData.ID = arrFieldSalesID[i];
				jsnData.Name = arrFieldSalesName[i];
				jsnData.displayAmount = this.numberWithCommas(jsnData.fsAmount);
				
				arrDisplay.push(jsnData);
			}

			arrDisplay = arrDisplay.sort(function(a, b){
				    return  b.fsAmount - a.fsAmount;
				});

			for(var i=0; i<arrFieldSalesID.length; i++) {
				if(colors[i]!=undefined)
					arrDisplay[i].color = colors[i]
				else
					arrDisplay[i].color = "#D80000";
			}
			
			console.log(arrDisplay.length);

			var i=0, sum =0, chartData = new Array();
			for(i =0;((i<arrDisplay.length) && (i<20));i++) {
				var value = arrDisplay[i].fsAmount;
				if(value.indexOf("-")>=0)
					value = 0;
				var columnData = {"Label" : arrDisplay[i].Name, "Value" : value, "color" : arrDisplay[i].color };
				chartData.push(columnData);
			}
			
			for(;i<arrDisplay.length;i++) {
				if(parseFloat(arrDisplay[i].fsAmount)>=0)
					sum = sum + parseFloat(arrDisplay[i].fsAmount);
			}
			if(arrDisplay.length >20) {
				var columnData = {"Label" : "Others", "Value" : sum, "color" : "#D80000"};
				chartData.push(columnData);
			}
				
			console.log(chartData);

			
			this.showChart(chartData);

			this.FSCollection = new SaleItems(arrDisplay);
			this.showItemListView(this.$("#div-dash-mtddrilldown-list"), this.FSCollection, "GmMTDDrillDownFSList");
			var subTitleAmountValue = this.numberWithCommas(this.mainCollection.ammountTotal("amount"));
			var salesDbCurrSymbol = localStorage.getItem("salesDbCurrSymbol");
			this.$("#span-dash-mtddrilldown-amount").html("("+salesDbCurrSymbol+")");
			if(subTitleAmountValue.indexOf("-")>=0) {
				subTitleAmountValue = salesDbCurrSymbol+"(" + subTitleAmountValue.replace("-","") + ")";
				this.parent.$("#div-dash-subtitle").html(" - " + subTitleAmountValue).addClass("content-red");
			}
			else
				this.parent.$("#div-dash-subtitle").html(" - "+salesDbCurrSymbol+"" + subTitleAmountValue);

		},

		processAccountInfo: function(event) {
			
			var fsID = (event.currentTarget.id).replace("li-dash-mtddrilldown-list-header-","");
			var collection = new SaleItems(this.mainCollection.filterBy("fieldsalesid",fsID));

			this.showItemListView($("#li-dash-mtddrilldown-list-content-"+fsID), collection, "GmMTDDrillDownAccList");
			$(".li-dash-mtddrilldown-list-content").each(function() {
				if(this.id != "li-dash-mtddrilldown-list-content-"+fsID)
					$(this).slideUp("fast");
			});
			$("#li-dash-mtddrilldown-list-content-"+fsID).slideToggle("fast");
		},

		//display the chart from the webservice data
		showChart: function(chartData) {
            
            var that = this, chartWidth = "75%", chartHeight = "65%";
            if(screen.availWidth <= 420) {
               chartWidth =  "95%";
               chartHeight =  "50%";
            }
			this.chart = new FusionCharts({
							type: "pie3d",	//type of chart
							renderAt: "div-dash-mtddrilldown-chart", //chart div area where the chart has to rendered
							width: chartWidth, //width of the chart div area
							height: chartHeight, //height of the chart div area
							dataFormat: "json", //format of data
						});
            
			var salesDbCurrSymbol = localStorage.getItem("salesDbCurrSymbol");
			var chartOptions = {
						// legendPosition: "right",
						// legendBorderThickness: "0",
						// showlegend: "1",
						// legendIconScale: "2",
						// legendShadow: "0",
						showLabels: "0", //hide the labels
						showValues: "0", //hide the values
						showpercentintooltip: "0",
						numberprefix: salesDbCurrSymbol,
						theme: "zune" //refers to theme
					};

			
			var combinedChartData = {"chart" : chartOptions, "data" : chartData};
			console.log(chartData);
			var chartCollection = new SaleItems(chartData);
			  this.showItemListView(this.$("#div-dash-mtdsales-chart-legend"), chartCollection, "GmSaleTodayChartLegends", URL_Dashboard_Template);
			this.chart.setChartData(combinedChartData, "json");
			this.chart.render();
			this.$("#div-dash-mtddrilldown-main").show();
			this.$(".div-dash-page-loader").hide();
			this.dateRange =  new DateRangeView({
						'el' :this.$('#div-dash-mtddrilldown-date'), 
						'dateformat' : 'mm-dd-yyyy', 
						'caption1' : '',
						'date1' : (this.serverInput.filterby).replace(/\//g,"-"),
						'usage' : '1' });

			var that = this;
			this.$("#div-dash-mtddrilldown-date").bind("onchange",function(e,date) {
				that.serverInput.filterby = date.replace(/\-/g,"/");
				that.render();
			})
//		this.renderChart();	
		},
    	renderChart: function() {
			switch(window.orientation) 
		    {  
		      case -90:
		      case 90:
	             if(screen.availHeight <= 548 && screen.availWidth == 320) {
				        this.chart.resizeTo("170","310");
				        this.chart.render();	 
		    	  }
		        break; 
		      default:
	             if(screen.availHeight <= 548 && screen.availWidth == 320) {
				        this.chart.resizeTo("310","170");
				        // this.chart.chartTopMargin("-140");
				        // this.chart.chartLeftMargin("-100");
				        // this.chart.chartRightMargin("100");
				        this.chart.render();	 
		    	  }
		        break; 
		    }			
		},

		toggleContent: function (event) {
			var id = (event.currentTarget.id).replace("div-dash-mtddrilldown-tab-","");
			this.$(".selected-tab").removeClass("selected-tab");
			$(event.currentTarget).addClass("selected-tab");

			if(id=="table") {
				this.$("#div-dash-mtddrilldown-data").show();
				this.$("#div-dash-mtddrilldown-chart").hide();
				this.$("#div-dash-mtdsales-chart-legend").hide();
				// this.chart.dispose();
				// FusionCharts(this.chart).dispose();
			}
			else {
				this.$("#div-dash-mtddrilldown-data").hide();
				this.$("#div-dash-mtddrilldown-chart").show();
				this.$("#div-dash-mtdsales-chart-legend").show();
//				this.renderChart();
			}

		},	

		//Initializes the ItemListView in the given HTML Element with the given collection
		showItemListView: function(controlID, collection, itemtemplate) {
			
			this.sditemlistview = new SaleItemListView(
					{ 
						el: controlID, 
						collection: collection, 
						itemtemplate: itemtemplate,
						template_URL: URL_Dashboard_Template,
						columnheader: 0,
						callbackOnReRender: this.formatNegativeAmount
					}
			);
		},

		formatNegativeAmount: function(option) {
			option.$(".div-dash-mtddrilldown-amount").each(function() {
				var value = $(this).text();
				if(value.indexOf("-") >= 0) {
					value = value.replace("-","");
					$(this).html("("+value+")");
					$(this).addClass("content-red");
				}
			});
		},
		
		// Close the view and unbind all the events
		close: function(e) {
			if((document.getElementById("dte-dash-mtddrilldown")!=null) &&(document.getElementById("dte-dash-mtddrilldown").style.display != "none"))
				$("#dte-dash-mtddrilldown").datepicker("destroy");
			$("#ui-datepicker-div").hide();
			this.unbind();
			this.remove();
		}

	});	

	return MTDDrillDownView;
});
