/**********************************************************************************
 * File:        GmOrderPODashboardView.js
 * Description: Pending order report view loading from sales dash main view
 * Version:     1.0
 * Author:      tmuthusamy
 **********************************************************************************/

define([
    'jquery', 'underscore', 'backbone',  'handlebars',
    
     // Common
    'global', 'commonutil','globusMobileApp/common/view/GmCommonFileUpload.js','gmvValidation'
    
    ], 

    function ($, _, Backbone, Handlebars,Global, Commonutil,DhxCommonFileUpload,GMVValidation) {

	'use strict';
	var PODashboardView = Backbone.View.extend({

		events : {
			"click .po-item-dash": "POStatusReport",
            "click .po-report-close": "goBackView",
            "click #updatePO": "showUpdatePOPopup"
		},

		/* Load the templates */ 
		template: fnGetTemplate(URL_Dashboard_Template, "GmOrderPOStatusDash"),
        template_poreport: fnGetTemplate(URL_Dashboard_Template, "GmOrderPOStatusReport"),
        template_updatePO: fnGetTemplate(URL_Dashboard_Template, "GmOrderUpdatePopPO"),
        template_FileListPO: fnGetTemplate(URL_Dashboard_Template, "GmOrderPOFileList"),

		//Initializes the view
		initialize: function(options) {
			this.parent = options.parent;
			this.el = options.el;
			this.options = options.filter;
			this.serverInput = {"token": localStorage.getItem("token"),"companyid": (localStorage.getItem("companyid"))?localStorage.getItem("companyid"):"","divids": (localStorage.getItem("divisionid"))?localStorage.getItem("divisionid"):"","zoneids": (localStorage.getItem("zoneid"))?localStorage.getItem("zoneid"):"","regionids":(localStorage.getItem("regionid"))?localStorage.getItem("regionid"):"","fsids": (localStorage.getItem("fieldsalesid"))?localStorage.getItem("fieldsalesid"):""};
			this.render(true);
		},
		
		//Renders the template
		render: function () {
			var that = this;
            $(this.el).parent("#div-dash-view").find("#div-dash-filter").addClass("hide");
            $(this.el).parent("#div-dash-view").find("#div-dash-title-main").css("height","0");
            this.serverInput.salesdbcurrtype = localStorage.getItem("salesDbCurrType");
			this.serverInput.salesdbcurrsymbol = localStorage.getItem("salesDbCurrSymbol");
            setTimeout(function(){
            if(that.options[1] != undefined){
                if(that.options[1].drilldownurl != "")
                    that.loadStatusReport(that.options[1].drilldownurl);
            }
            else
                that.orderPODash();
            },1000);
		},
        
        //PO status dashboard data fetch function 
        orderPODash: function(){
            var that =this;
            $("#div-dash-title").text("PO Status - Orders Pending PO");
            getLoader(that.$el);
            fnGetWebServerData('po/podash', "gmSalesDashResVO", this.serverInput, function (data) {
                if (data != null) {
                    var currSymbol = data.salesdbcurrsymbol;
                    that.totalCnt = 0;
                    that.totalVal = 0;
                    _.each(data.listGmSalesDashResListVO,function(item){
                        that.totalCnt = that.totalCnt + parseInt(item.count);
                        that.totalVal = that.totalVal + parseInt(item.amt);
                        item.salesdbcurrsymbol = currSymbol;
                    });
                    data.totalCnt = that.totalCnt;
                    data.totalVal = that.totalVal;
                    console.log(JSON.stringify(data));
                    that.$el.html(that.template(data));
                    
                }
            });
        },
        
        POStatusReport: function(e){
            var that = this;
            var passname = $(e.currentTarget).data("passname");
            window.location.href = "#dashboard/postatus/report/" + passname;
        },
        
        goBackView: function(){
            window.history.back();
        },
        
        //call webservice and load report for Pending orders
        loadStatusReport:function(drilldownurl){
            var that = this;
            that.$el.html(that.template_poreport());
            var input = {
                salesdbcurrsymbol: localStorage.getItem("salesDbCurrSymbol"),
                salesdbcurrtype: localStorage.getItem("salesDbCurrType"),
                passname : drilldownurl
            }
            getLoader("#po-status-report");
            fnGetWebServerData('po/poreport', "gmOrdersDrilldownVO", input, function (data) {
                console.log(JSON.stringify(data));
                if(data != null){
                    if( $("#po-status-report").is(":visible")) {
                        if (data.passname == "PO14day") {
                            data.passname = "Pending PO < 14 days";
                        } else if (data.passname == "PO14-21day") {
                            data.passname = "Pending PO 14 - 21 days";
                        } else if (data.passname == "PO21day") {
                            data.passname = "Pending PO > 21 days";
                        }

                        if (data.passname == 'PendingApproval') {
                            data.passname = "Pending Approval";
                            that.loadPOApprovalReport(data);
                        } else if (data.passname == 'PendingDiscrepancy') {
                            data.passname = "Pending Discrepancy";
                            that.loadPODiscrepancyReport(data);
                        }
                        else {
                            that.loadPODaysReport(data);
                        }
                    }
                }
            });
        },
        
        
        //load PO Reports based on status that are po<14 , po 14 - 21, po > 21 orders
        loadPODaysReport: function (data) {
            var that = this;
            $("#div-dash-title").text(data.passname);
            $(".po-report-close").removeClass("hide");
            if (data.listGmOrdersDrilldownVO.length > 0) {
                _.each(data.listGmOrdersDrilldownVO, function (griddt) {
                    if(parseNull(griddt.poamount) == ""){
                        var poamt = "";
                    }
                    else{
                        var poamt = parseFloat(griddt.poamount).toFixed(2);
                        griddt.poamount = formatCurrency(parseFloat(griddt.poamount),2);
                    }
                    if(parseNull(griddt.total) == "")
                        griddt.total = 0;
                    else{
                        griddt.total = parseFloat(griddt.total);
                        griddt.totalCur = formatCurrency(griddt.total,2);
                        griddt.totalCur = griddt.totalCur.replace("-","");
                    }
                    if(parseNull(griddt.shipcost) == "")
                        griddt.shipcost = 0;
                    if(parseNull(griddt.type) == "")
                        griddt.type = "Bill & Ship";
                    else
                        griddt.type = $.parseHTML(griddt.type)[0].textContent;
                    if(parseNull(griddt.holdfl) == "")
                        griddt.holdfl = "";
                    else
                        griddt.holdfl = "<div class='gmTextCenter'><i class='fa fa-exclamation-circle gmAlignHorizontal gmVerticalMiddle'></i></div>";
                    if(parseNull(griddt.orderdt) != ""){
                        griddt.orderdt = formattedDate(griddt.orderdt,localStorage.getItem("cmpdfmt"));
                    }
                    griddt.updatepo = "<a id='updatePO' class='gmAlignHorizontal gmPadding5x0p' data-orderid='"+griddt.txnid+"' data-actnm='"+griddt.accountnm+"' data-orddt='"+griddt.orderdt+"' data-total='"+griddt.total+"' data-ordertype='"+griddt.type+"' data-ponum='"+griddt.ponum+"' data-poamount='"+poamt+"' data-shipcost='"+griddt.shipcost+"' data-postatus='"+griddt.postatus+"' data-dostatus='"+griddt.dostatus+"'  href='javascript:;'><span class='po-update-edit'><i class='fa fa-pencil'></i></span></a>";
                    //PC-4864 Add Online PDF option on Pending PO Screen in Globus App
                    if(griddt.ordmode == 26230683){  //26230683 - IPAD
                        griddt.onlinepdf = "<a class='po-online-pdf' href='javascript:;'>Online PDF</a>";
                    }
                    else{
                        griddt.onlinepdf = "";
                    }
                });
                var grid = new dhx.Grid("po-status-report", {
				columns: [
                        { width: 45, id: "updatepo", header: [{ text: ""},{ content: "" }],htmlEnable: true},
//                        { width: 40, id: "holdfl", header: [{ text: "" }],htmlEnable: true},
                        { width: 130, id: "txnid", header: [{ text: "Order ID" }, { content: "inputFilter" }]},
                        { width: 100, id: "onlinepdf", header: [{ text: "" }],htmlEnable: true}, 
                        { width: 100, id: "orderdt", header: [{ text: "Order Date" },{ content: "selectFilter" }],
                         footer: [
							{ text: '<div class="custom_footer">Total</div>' }
						]},
                        { width: 110, id: "total", header: [{ text: "Order Amount" },{ content: "inputFilter" }], 
                         type:"number", template: function (text, row, col) {
                            if(text == 0){
                              return "0.00"
                            }
                            else if(text < 0){
                              return "(" + row.totalCur + ")"
                            }
                            else{
                                return row.totalCur;
                            } 
                          },
                          mark: function (cell, data, row, col) {
                             if (row.total < 0)
                                 return "min-val-dhxCustom";
                         },
                         footer: [
                            { content: "sum" }
                        ]},
                        { width: 310, id: "accountnm", header: [{ text: "Rep Account" },{ content: "inputFilter" },{ align: "center" }] },
                        { width: 150, id: "donum", header: [{ text: "Parent Order ID" },{ content: "inputFilter" }]},
                        { width: 136, id: "type", header: [{ text: "Order Type" },{ content: "selectFilter" }]}
                        
                    ],
                    data: data.listGmOrdersDrilldownVO,
                    splitAt: 1
			     });
                
                //load the summaryDo function when click order id
                grid.events.on("cellclick", function (row, column, conf) {
                    //when click order id to call the DO summary screen
                    if (column.id === "txnid") {
                        that.fnSummaryDO(row.txnid);
                    }
                    if (column.id === "donum") {
                        that.fnSummaryDO(row.donum);
                    }
                    if(column.id === "onlinepdf"){
                        if(row.onlinepdf != ""){
                            if(row.donum != "")
                                viewOnlinePDF(row.donum);
                            else
                                viewOnlinePDF(row.txnid);
                        }
                    }
                });
                
                //add class on cell based on column
                grid.data.forEach(function (element, index, array) {
                    grid.addCellCss(element.id, "txnid", "gmLinkText");
                    grid.addCellCss(element.id, "donum", "gmLinkText");
                    grid.addCellCss(element.id, "onlinepdf", "onlinepdf-cell");
                });
                
            }
            else {
                $("#po-status-report").html("<span>No Data Found</span>");
            }
        },
        
        //load PO Report based on pending approval Orders
        loadPOApprovalReport: function (data) {
            var that = this;
            var actNmWidth = 310;
            if($(window).width() < 480)
                actNmWidth = 160;
            $("#div-dash-title").text(data.passname);
            $(".po-report-close").removeClass("hide");
            if (data.listGmOrdersDrilldownVO.length > 0) {
                _.each(data.listGmOrdersDrilldownVO, function (griddt) {
                    if(parseNull(griddt.poamount) == ""){
                        var poamt = "";
                        griddt.poamount = 0;
                    }
                    else{
                        var poamt = parseFloat(griddt.poamount).toFixed(2);
                        griddt.poamtCur = formatCurrency(parseFloat(griddt.poamount),2);
                        griddt.poamount = parseFloat(griddt.poamount);
                    }
                    if(parseNull(griddt.total) == "")
                        griddt.total = 0;
                    else{
                        griddt.total = parseFloat(griddt.total);
                        griddt.totalCur = formatCurrency(griddt.total,2);
                        griddt.totalCur = griddt.totalCur.replace("-","");
                    }
                    griddt.diff = griddt.total - griddt.poamount;
                    griddt.diff = formatCurrency(griddt.diff,2);
                    if(parseNull(griddt.shipcost) == "")
                        griddt.shipcost = 0;
                    if(parseNull(griddt.holdfl) == "")
                        griddt.holdfl = "";
                    else
                        griddt.holdfl = "<div class='gmTextCenter'><i class='fa fa-exclamation-circle gmAlignHorizontal gmVerticalMiddle'></i></div>";
                    if(griddt.uploadfl == "Y"){
                        griddt.ficon = "<div class='gmTextCenter'><span class='poreport-fsymbol gmAlignHorizontal gmVerticalMiddle' data-orderid='"+griddt.txnid+"' data-ponum='"+griddt.ponum+"' data-ordertype='"+griddt.type+"'>F</span></div>";
                    }
                    else
                        griddt.ficon = "";
                    if(parseNull(griddt.orderdt) != ""){
                        griddt.orderdt = formattedDate(griddt.orderdt,localStorage.getItem("cmpdfmt"));
                    }
                    if(parseNull(griddt.type) == "")
                        griddt.type = "Bill & Ship";
                    else
                        griddt.type = $.parseHTML(griddt.type)[0].textContent;
                    griddt.updatepo = "<a id='updatePO' class='gmAlignHorizontal gmPadding5x0p' data-orderid='"+griddt.txnid+"' data-total='"+griddt.total+"' data-ordertype='"+griddt.type+"' data-ponum='"+griddt.ponum+"' data-poamount='"+poamt+"' data-shipcost='"+griddt.shipcost+"'  href='javascript:;'><span class='po-update-edit'><i class='fa fa-pencil'></i></span></a>";
                    //PC-4864 Add Online PDF option on Pending PO Screen in Globus App
                    if(griddt.ordmode == 26230683){  //26230683 - IPAD
                        griddt.onlinepdf = "<a class='po-online-pdf' href='javascript:;'>Online PDF</a>";
                    }
                    else{
                        griddt.onlinepdf = "";
                    }
                });
               var grid = new dhx.Grid("po-status-report", {
               columns: [
                    { width: 45, id: "ficon", header: [{ text: ""},{ content: "" }],htmlEnable: true},
//                    { width: 45, id: "updatepo", header: [{ text: ""}],htmlEnable: true},
                    { width: 100, id: "onlinepdf", header: [{ text: "" }],htmlEnable: true}, 
                    { width: actNmWidth, id: "accountnm", header: [{ text: "Rep Account" },{ content: "inputFilter" }]},
                    { width: 190, id: "ponum", header: [{ text: "PO Number" },{ content: "inputFilter" }] },
                    { width: 100, id: "poamount", header: [{ text: "PO Amount" }, { content: "inputFilter" }], 
                     type:"number", template: function (text, row, col) {
                        if(text == 0){
                          return "0.00"
                        }
                        else{
                            return row.poamtCur;
                        } 
                      },
                    footer: [
                        { content: "sum" }
                    ]},
                   { width: 110, id: "total", header: [{ text: "Order Amount" },{ content: "inputFilter" }], 
                     type:"number", template: function (text, row, col) {
                        if(text == 0){
                          return "0.00"
                        }
                        else if (text < 0) {
                             return "(" + row.totalCur + ")"
                        }
                        else{
                            return row.totalCur;
                        } 
                      },
                      mark: function (cell, data, row, col) {
                         if (row.total < 0)
                             return "min-val-dhxCustom";
                      },
                     footer: [
                        { content: "sum" }
                    ]},
                   { width: 150, id: "diff", header: [{ text: "Diff" },{ content: "inputFilter" }]},
//                    { width: 40, id: "holdfl", header: [{ text: "" }],htmlEnable: true},
                    { width: 130, id: "txnid", header: [{ text: "Order ID" },{ content: "inputFilter" }]},
                    { width: 150, id: "donum", header: [{ text: "Parent Order ID" },{ content: "inputFilter" }]},
                    { width: 100, id: "orderdt", header: [{ text: "Order Date" },{ content: "selectFilter" }]},
                    { width: 130, id: "poenterdt", header: [{ text: "PO Entry Date" },{ content: "selectFilter" }]}
                    ],
                    data: data.listGmOrdersDrilldownVO
               });
                
                //load the summaryDo function when click order id
                grid.events.on("cellclick", function (row, column, conf) {
                    //when click order id to call the DO summary screen
                    if (column.id === "txnid") {
                        that.fnSummaryDO(row.txnid);
                    }
                    if (column.id === "donum") {
                        that.fnSummaryDO(row.donum);
                    }
                    if (column.id === "ficon") {
                        if(row.ficon != "")
                            that.showUploadedFileList(row.txnid,row.ponum);
                    }
                    if(column.id === "onlinepdf"){
                        if(row.onlinepdf != ""){
                            if(row.donum != "")
                                viewOnlinePDF(row.donum);
                            else
                                viewOnlinePDF(row.txnid);
                        }
                    }
                });
                
                //add class on cell based on column
                grid.data.forEach(function (element, index, array) {
                    grid.addCellCss(element.id, "txnid", "gmLinkText");
                    grid.addCellCss(element.id, "donum", "gmLinkText");
                    grid.addCellCss(element.id, "onlinepdf", "onlinepdf-cell");
                });
            }
            else {
                $("#po-status-report").html("<span>No Data Found</span>");
            }
        },
        
        //load PO Report based on Discrepancy Orders
        loadPODiscrepancyReport: function (data) {
            var that = this;
            var actNmWidth = 270;
            if($(window).width() < 480)
                actNmWidth = 160;
            $("#div-dash-title").text(data.passname);
            $(".po-report-close").removeClass("hide");
            if (data.listGmOrdersDrilldownVO.length > 0) {
                _.each(data.listGmOrdersDrilldownVO, function (griddt) {
                    if(parseNull(griddt.poamount) == ""){
                        griddt.poamount = 0;
                        var poamt = "";
                    }
                    else{
                        var poamt = parseFloat(griddt.poamount).toFixed(2);
                        griddt.poamtCur = formatCurrency(parseFloat(griddt.poamount),2);
                        griddt.poamount = parseFloat(griddt.poamount);
                    }
                    if(parseNull(griddt.total) == "")
                        griddt.total = 0;
                    else{
                        griddt.total = parseFloat(griddt.total);
                        griddt.totalCur = formatCurrency(griddt.total,2);
                        griddt.totalCur = griddt.totalCur.replace("-","");
                    }
                    griddt.diff = griddt.total - griddt.poamount;
                    griddt.diff = formatCurrency(griddt.diff,2);
                    if(parseNull(griddt.shipcost) == "")
                        griddt.shipcost = 0;
                    if(parseNull(griddt.type) == "")
                        griddt.type = "Bill & Ship";
                    else
                        griddt.type = $.parseHTML(griddt.type)[0].textContent;
                    if(griddt.uploadfl == "Y"){
                        griddt.ficon = "<div class='gmTextCenter'><span class='poreport-fsymbol gmAlignHorizontal gmVerticalMiddle' data-orderid='"+griddt.txnid+"' data-ponum='"+griddt.ponum+"' data-ordertype='"+griddt.type+"'>F</span></div>";
                    }
                    else
                        griddt.ficon = "";
                    griddt.updatepo = "<a id='updatePO' class='gmAlignHorizontal gmPadding5x0p' data-orderid='"+griddt.txnid+"' data-total='"+griddt.total+"' data-ordertype='"+griddt.type+"' data-ponum='"+griddt.ponum+"' data-poamount='"+poamt+"' data-shipcost='"+griddt.shipcost+"'  href='javascript:;'><span class='po-update-edit'><i class='fa fa-pencil'></i></span></a>";
                    if(parseNull(griddt.orderdt) != ""){
                        griddt.orderdt = formattedDate(griddt.orderdt,localStorage.getItem("cmpdfmt"));
                    }
                    //PC-4864 Add Online PDF option on Pending PO Screen in Globus App
                    if(griddt.ordmode == 26230683){  //26230683 - IPAD
                        griddt.onlinepdf = "<a class='po-online-pdf' href='javascript:;'>Online PDF</a>";
                    }
                    else{
                        griddt.onlinepdf = "";
                    }
                });
               var grid = new dhx.Grid("po-status-report", {
               columns: [
                    { width: 45, id: "ficon", header: [{ text: ""},{ content: "" }],htmlEnable: true},
//                    { width: 45, id: "updatepo", header: [{ text: ""}],htmlEnable: true},
                    { width: 100, id: "onlinepdf", header: [{ text: "" }],htmlEnable: true}, 
                    { width: actNmWidth, id: "accountnm", header: [{ text: "Rep Account" },{ content: "inputFilter" }]},
                    { width: 150, id: "ponum", header: [{ text: "PO Number" },{ content: "inputFilter" }] },
                    { width: 100, id: "poamount", header: [{ text: "PO Amount" }, { content: "inputFilter" }], 
                     type:"number", template: function (text, row, col) {
                        if(text == 0){
                          return "0.00"
                        }
                        else{
                            return row.poamtCur;
                        } 
                      },
                    footer: [
                        { content: "sum" }
                    ]},
                    { width: 110, id: "total", header: [{ text: "Order Amount" },{ content: "inputFilter" }], 
                     type:"number", template: function (text, row, col) {
                        if(text == 0){
                          return "0.00"
                        }
                        else if(text < 0){
                              return "(" + row.totalCur + ")"
                        }
                        else{
                            return row.totalCur;
                        } 
                      },
                      mark: function (cell, data, row, col) {
                         if (row.total < 0)
                             return "min-val-dhxCustom";
                      },
                     footer: [
                        { content: "sum" }
                    ]},
                    { width: 95, id: "diff", header: [{ text: "Diff" },{ content: "inputFilter" }]},
                    { width: 130, id: "txnid", header: [{ text: "Order ID" },{ content: "inputFilter" }]},
                    { width: 150, id: "category", header: [{ text: "Category" },{ content: "selectFilter" }]},
                    { width: 130, id: "donum", header: [{ text: "Parent Order ID" },{ content: "inputFilter" }]},
                    { width: 100, id: "orderdt", header: [{ text: "Order Date" },{ content: "selectFilter" }]},
                    { width: 100, id: "poenterdt", header: [{ text: "PO Entry Date" },{ content: "selectFilter" }]}
                    ],
                    data: data.listGmOrdersDrilldownVO
               });
                
                //load the summaryDo function when click order id
                grid.events.on("cellclick", function (row, column, conf) {
                    //when click order id to call the DO summary screen
                    if (column.id === "txnid") {
                        that.fnSummaryDO(row.txnid);
                    }
                    if (column.id === "donum") {
                        that.fnSummaryDO(row.donum);
                    }
                    if (column.id === "ficon") {
                        if(row.ficon != "")
                            that.showUploadedFileList(row.txnid,row.ponum);
                    }
                    if(column.id === "onlinepdf"){
                        if(row.onlinepdf != ""){
                            if(row.donum != "")
                                viewOnlinePDF(row.donum);
                            else
                                viewOnlinePDF(row.txnid);
                        }
                    }
                });
                
                //add class on cell based on column
                grid.data.forEach(function (element, index, array) {
                    grid.addCellCss(element.id, "txnid", "gmLinkText");
                    grid.addCellCss(element.id, "donum", "gmLinkText");
                    grid.addCellCss(element.id, "onlinepdf", "onlinepdf-cell");
                });
            }
            else {
                $("#po-status-report").html("<span>No Data Found</span>");
            }
        },
        
        fnSummaryDO: function (orderid) {
            if (orderid != "0") {
                var that = this,
                    id;
                var companyInfo = {};
                companyInfo.cmpid = localStorage.getItem("cmpid");
                companyInfo.plantid = localStorage.getItem("plantid");
                companyInfo.token = "";
                companyInfo.partyid = localStorage.getItem("partyid");
                var StringifyCompanyInfo = JSON.stringify(companyInfo);
                var toEncode = "/GmEditOrderServlet?hMode=PrintPrice&hParantForm=SALESDASHBOARD&hOrdId=" + orderid + "&companyInfo=" + StringifyCompanyInfo;
                var URL = URL_Portal + "GmLogonServlet?token=" + localStorage.getItem("token") + "&return_to=" + encodeURIComponent(toEncode) + "&randomid=" + Math.random();

                console.log(URL);
                var summaryWindow = window.open(URL, '_blank', 'location=no');
                summaryWindow.addEventListener('loadstop', function () {
                    summaryWindow.insertCSS({
                        code: "body {font-family: 'Trebuchet MS'!important; -webkit-overflow-scrolling: touch;} a{display: none;} .RightDashBoardHeader {background:#999;} .ShadeRightTableCaption {background:#ccc;} TR.ShadeBlueBk {background:#eee}" + " table { margin:auto!important; } .button-red{ display:none; }"
                    });
                    summaryWindow.executeScript({
                        code: "var meta = document.createElement('meta'); meta.name = 'format-detection'; meta.content = 'telephone=no'; document.getElementsByTagName('head')[0].appendChild(meta); " + that.scriptOptions
                    });
                });
                summaryWindow.addEventListener('loaderror', function () {
                    showNativeAlert(lblNm("msg_error_loading"), lblNm("error"), lblNm("msg_ok"), function (index) {
                        summaryWindow.close();
                    });
                });
            }
        },
        
        showUpdatePOPopup: function(e){
            var that = this;
            var orderid = $(e.currentTarget).data("orderid");
            var total = $(e.currentTarget).data("total");
            var poamount = $(e.currentTarget).data("poamount");
            var updatePOJson = {
                orderid : orderid,
                total: total,
                salesdbcurrsymbol: this.serverInput.salesdbcurrsymbol,
                ordertype: $(e.currentTarget).data("ordertype"),
                ponum:  $(e.currentTarget).data("ponum"),
                poamt:  poamount,
                shipcost:  $(e.currentTarget).data("shipcost"),
                actnm:  $(e.currentTarget).data("actnm"),
                orderdt:  $(e.currentTarget).data("orddt"),
                postatus:  $(e.currentTarget).data("postatus"),
                dostatus:  $(e.currentTarget).data("dostatus")
            };
            console.log(JSON.stringify(updatePOJson));
            showPopup();
            $("#div-crm-overlay-content-container").addClass("pendingOrderUpdatePop");
            $(".pendingOrderUpdatePop .modal-title").html("Update PO");
            $(".pendingOrderUpdatePop .cancel-model").removeClass("hide");
            $(".pendingOrderUpdatePop .modal-body").html(this.template_updatePO(updatePOJson));
            $(".pendingOrderUpdatePop .modal-footer").prepend('<button type="button" class="gmBtn gmFont15px hideDefaultBtn gmBGDarkBlue gmFontWhite updatepo-submit-btn">Submit</button>');
            $(".pendingOrderUpdatePop .modal-body").prepend("<div id='div-po-popup-msg' class='gmMarginBottom10p hide'><span></span><i class='fa fa-close gmFloatRight po-pop-alert-close'></i></div>");
            
            $(".pendingOrderUpdatePop .fa-close").on("click",function(){
                $('#div-po-popup-msg').slideUp('fast', function () {
                    $(this).css("display", "block").addClass('hide').removeClass("alertErr alertSuccess");
                });
            });
            
            this.gmvValidation = new GMVValidation({
                "el": $(".pendingOrderUpdatePop")
            });
            
            this.updateFl = "";
            
            this.uploadFile(orderid);
            $(".pendingOrderUpdatePop .updatepo-submit-btn").on("click",function(e){
                var currentPONum = $("#ponumber").val();
                if(currentPONum != ""){
                    that.validatePoNum(currentPONum,orderid,function(valid){
                        if(!valid){
                            dhtmlx.confirm({
                                title: "Entered PO Number : " + currentPONum,
                                ok: "Yes",
                                cancel: "No",
                                type: "confirm",
                                text: "PO Number is already Exists. Do you want to Proceed ?",
                                callback: function (result) {
                                    if (result)
                                        that.updatePO(orderid);
                                }
                            });
                        }
                        else
                            that.updatePO(orderid);
                    });
                }
                else{
                    that.updatePO(orderid);
                }
            });
            $(".pendingOrderUpdatePop .close").on("click",function(){
                that.hideUpdatePOPopup();
                if($(".dhx-file-item").length > that.fileArrLength)
                    that.updateFl = "Y";
                
                if(that.updateFl == "Y")
                    that.loadStatusReport(that.options[1].drilldownurl);
            });
            
            $(".pendingOrderUpdatePop .cancel-model").on("click",function(){
                if(that.updateFl == "Y")
                    that.loadStatusReport(that.options[1].drilldownurl);
            });
        },
        
        updatePO: function(orderid){
            var that = this;
            var input = {};
            input.po = $(".pendingOrderUpdatePop #ponumber").val();
            input.poamt = $(".pendingOrderUpdatePop #poamount").val();
            input.orderid = orderid;
            input.ordercomments = "";
            input.inputstring = "";
            input.copay = "";
            input.capamount = null;
            input.copaycapaction = "";
            input.postatus = $(".pendingOrderUpdatePop .postatus").val();
            input.dostatus = $(".pendingOrderUpdatePop .dostatus").val();
            var errorMsg = "";
            
            if(that.gmvValidation.validate()){
                fnGetWebServerData('po/updatepo', "gmOrdersDrilldownVO", input, function (data) {
                    if(data != null){
                        that.displayPopupMsg("PO Updated Successfully",'alertSuccess');
                        that.updateFl = "Y";
                        $("#existingPoField").addClass("hide");
                    }
                });
            }
            else{
                errorMsg = that.gmvValidation.getErrors();
                that.displayPopupMsg(errorMsg,'alertErr');
            }
            
        },
        
        uploadFile: function(orderid){
            var that = this;
            var locale = localStorage.getItem("locale");
            var divRef = "po-file-upload";
            var uploadURL = URL_WSServer + "po/uploadFile";
            var uploadPath = fileUploadPath;
            var refID = orderid;
            var refGroupType = "26240412";
            var viewMode = "list";
            var refType = "53018";
            var refSubtype = "26230732";
            var fileURL = "fileUpload/deleteUploadedFiles";
            var fetchInfoURL = "fileUpload/fetchFileList";
            var fileUploadDownloadPath = "download-dir/" + locale + "/" + orderid;
            var downloadURL = microappUrlFile+"downloadFile/"+fileUploadDownloadPath+"/";
            var doUpdateFlURL = "po/updateorderfl";
            var divWidth = "630";
            var divHeight = "227";
            var saveInfoURL = "po/saveUploadedFile";
            var strUserid = localStorage.getItem("userID");
            var cmnFileUploadVO = "gmCommonFileUploadVO";
            var AccessFl = "Y";
            
            if($(window).width() < 420)
                divHeight = "200";
            if($(window).width() < 400)
                divHeight = "135";
            
//            var accessFlInput = {
//                userid : localStorage.getItem("partyID"),
//                stropt: 'ORD_PO_ENTRY_ACC'
//            }
            
            var getFileInput = {
				refgroup: "26240412", // DO
				refid: orderid, // Order Id
				reftype: "53018", // DO
				type: "26230732", // DO
                token: localStorage.getItem("token")
			};
            var fileDetArr = [];
            var fileNameArr = [];
            fnGetWebServerData("fileUpload/fetchFileList", "gmCommonFileUploadVO", getFileInput, function (data) {
                if (data != null) {
                    for (var i = 0; i < data.length; i++) {
                        console.log(JSON.stringify(data));
                        var fileObj = data[i];
                        fileDetArr.push({
                            "name": fileObj.filename, // Uploaded file name
                            "size": fileObj.filesize, // Uploaded file size
                            "link": fileObj.filename, // Uploaded file download link
                            "status": "uploaded", // Status of uploaded file
                            "filelistid": fileObj.fileid, // File list of uploaded file
                            "userid": fileObj.userid // userid
                        });
                        fileNameArr.push(fileObj.filename);
                    }
                }
                that.fileArrLength = fileDetArr.length;
                initDHTMLXValut(divRef,uploadURL,uploadPath,saveInfoURL,fileURL,fetchInfoURL,cmnFileUploadVO,downloadURL,doUpdateFlURL,fileDetArr,fileNameArr,AccessFl,refID,refGroupType,refType,refSubtype,viewMode,divWidth,divHeight,strUserid);
//               fnGetWebServerData("po/accessfl", undefined, accessFlInput, function (data) {
//                    if(data != null){
//                            AccessFl = data[0].updfl;
//                    }
//                   
//                }); 
            });
            
        
        },
        
        hideUpdatePOPopup: function(){
            $("#div-crm-overlay-content-container").removeClass("pendingOrderUpdatePop");
            $("#div-crm-overlay-content-container").removeClass("POUploadedFileList");
        },
        
        showUploadedFileList: function(txnid,ponum){
            var that = this;
            this.flistOrderid = txnid;
            showPopup();
            $("#div-crm-overlay-content-container").addClass("POUploadedFileList");
            $(".POUploadedFileList .modal-footer").addClass("hide");
            $(".POUploadedFileList .modal-title").text("PO Attachments: " + this.flistOrderid + "(" + ponum + ")");
            
            this.loadPOAttachments(this.flistOrderid);
            
            $(".POUploadedFileList .close").on("click",function(){
                that.hideUpdatePOPopup();
            });
        },
        
        //load PO Attachments when click File symbol Icon on Report grid
        loadPOAttachments: function (orderid) {
            var that = this;
            $(".POUploadedFileList .modal-body").html(this.template_FileListPO());
            var locale = localStorage.getItem("locale");
            var fileUploadDownloadPath = "download-dir/" + locale + "/" + orderid;
            var input = {
				refgroup: "26240412", // DO
				refid: orderid, // Order Id
				reftype: "53018", // DO
				type: "26230732", // DO
                token: localStorage.getItem("token")
			};
            fnGetWebServerData("fileUpload/fetchFileList", "gmCommonFileUploadVO", input, function (data) {
            console.log(JSON.stringify(data));
            var strUserid = localStorage.getItem("userID");
            if (data != null) {
                if (data.length > 0) {
                    data = _.sortBy(data, function(item) { return item.updateddate; });
                    data = data.reverse();
                    var filesize = "";
                    for(var i = 0; i < data.length; i++){
                        data[i].index = i + 1;
                        filesize = fnGetVaultFileSize(data[i].filesize);
                        if(data[i].userid == strUserid)
                            data[i].deleteElem = '<i class="fa fa-trash" aria-hidden="true"></i>';
                        else
                            data[i].deleteElem = "";

                        data[i].filesize = filesize;
                        var filename = data[i].filename.toLowerCase();
                        var extension = filename.substr((filename.lastIndexOf('.') +1));
                        var fileType = getFileTypeIcon(extension);
                        data[i].fileTypeIcon = '<i class="fa '+ fileType +'" aria-hidden="true"></i>';
//                        '<a href="'+microappUrlFile+'downloadFile/'+fileUploadDownloadPath+'/'+ data[i].filename+'" target="_blank"><i class="fa '+ fileType +'" aria-hidden="true"></i></a>';
//                        data[i].filename = '<a href="'+microappUrlFile+'downloadFile/'+fileUploadDownloadPath+'/'+ data[i].filename+'" target="_blank">'+ data[i].filename +'</a>';
                    }
                    console.log(JSON.stringify(data));
                   var grid = new dhx.Grid("po-attachments", {
                   columns: [
                        { width: 40, id: "index", header: [{ text: ""}],htmlEnable: true},
                        { width: 40, id: "fileTypeIcon", header: [{ text: ""}],htmlEnable: true},
                        { width: 260, id: "filename", header: [{ text: "File Name"}],htmlEnable: true},
                        { width: 100, id: "filesize", header: [{ text: "Size"}]},
                        { width: 160, id: "updateddate", header: [{ text: "Date Attached" }] },
                        { width: 150, id: "updatedby", header: [{ text: "Attached By" }] },
                        { width: 40, id: "deleteElem", header: [{ text: "" }],htmlEnable: true}
                        ],
                        data: data,
                        height: 300
                   });

                   grid.events.on("cellclick", function (row, column, conf) {
                    if (column.id === "deleteElem") {
                        if(row.userid == strUserid){
                            var inputdata = {
                                "gmCommonFileUploadVO": [{
                                    fileid: row.fileid, // int, internal file ID
                                    updatedby: row.userid}]
                            };
                            // your code here, for example

                            dhtmlx.confirm({
                                title: "Confirm",
                                ok: "Yes",
                                cancel: "No",
                                type: "confirm",
                                text: "Are you sure you want to delete " + row.filename + "?",
                                callback: function (result) {
                                    if (result == true) {
                                        fnGetWebServerData("fileUpload/deleteUploadedFiles", undefined, inputdata, function (data) {
                                            console.log(JSON.stringify(data));
                                            that.loadPOAttachments(that.flistOrderid);
                                        });
                                    }
                                }
                            });
                        }
                    }
                    else if(column.id === "fileTypeIcon" || column.id === "filename"){
                        var targetUrl = microappUrlFile+'downloadFile/'+fileUploadDownloadPath+'/'+ row.filename;
                        var windowFeatures = "menubar=yes,location=no,resizable=yes,scrollbars=yes,status=yes";
                        window.open(targetUrl, "_blank", windowFeatures);
                    }

                });
                //add class on cell based on column
                grid.data.forEach(function (element, index, array) {
                    grid.addCellCss(element.id, "filename", "gmLinkText");
                });
            }
            else {
                $("#po-attachments").html("<span>No Data Found</span>");
            }
            }
            
			});
        },
        
        // To display custom msg on popup itself
        displayPopupMsg: function(Message,bgClr) {
            $('#div-po-popup-msg').removeClass('hide').addClass(bgClr).children("span").html(Message.trim().replace(/\n/g, '<br>'));
            setTimeout(function(){
                $('#div-po-popup-msg').slideUp('fast', function () {
                    $(this).css("display", "block").addClass('hide').removeClass("alertErr alertSuccess");
                });
            },8000);
        },
        
        validatePoNum: function(currentPONum,orderid,cb){
            var that = this;
            var input = {
                po : currentPONum,
                orderid: orderid
            }
            fnGetWebServerData("po/checkPoNumber", undefined, input, function (data) {
                console.log(JSON.stringify(data));
                if(data > 0){
                    cb(false);
                    $("#existingPoField").removeClass("hide");
                }
                else
                    cb(true);
            });
            $("#existingPoField .fa-close").unbind('click').bind('click', function () {
                $(this).parent().siblings("input").val("");
                $(this).parent().addClass("hide");
            });

            $("#existingPoField .fa-exclamation-circle").unbind('click').bind('click', function () {
                that.showDuplicatePoOrderList(currentPONum);
            });
            
        },
        
        showDuplicatePoOrderList: function(ponum){
            var that = this;
            var input = {
                    po : ponum
            }
            $("#div-crm-overlay-content-container2").removeClass("hide").addClass("poOrderlistPopCustom");
            $("#div-crm-overlay-content-container2 .modal-title").text("Order Details");
            $("#div-crm-overlay-content-container2 .modal-footer").addClass("hide");
            $("#div-crm-overlay-content-container2 .close").addClass("hide");
            $("#div-crm-overlay-content-container2 .modal-header").prepend('<span class="po-report-close"><i class="fa fa-times" aria-hidden="true"></i></span>');
            $("#div-crm-overlay-content-container2 .modal-body").html("<div id='orderListPO'></div>");
            fnGetWebServerData("po/orderlist", undefined, input, function (data) {
                console.log(JSON.stringify(data));
                if(data != null){
                    var grid = new dhx.Grid("orderListPO", {
                        columns: [
                            { width: 120, id: "orderid", header: [{ text: "Order ID"},{ content: "inputFilter" }]},
                            { width: 310, id: "repacnt", header: [{ text: "Account"},{ content: "inputFilter" }]},
                            { width: 100, id: "orddt", header: [{ text: "Order Date"},{ content: "selectFilter" }]},
                            { width: 100, id: "total", header: [{ text: "Total"},{ content: "inputFilter" }]},
                            { width: 120, id: "ordertype", header: [{ text: "Type" },{ content: "selectFilter" }] },
                            { width: 150, id: "usernm", header: [{ text: "Order By" },{ content: "inputFilter" }] },
                            { width: 100, id: "shipdt", header: [{ text: "Ship Date" },{ content: "selectFilter" }]},
                            { width: 120, id: "shiptrack", header: [{ text: "Tracking#" },{ content: "inputFilter" }]},
                            { width: 190, id: "ponum", header: [{ text: "Customer PO" },{ content: "inputFilter" }]},
                            { width: 120, id: "parentorderid", header: [{ text: "Parent Order Id" },{ content: "inputFilter" }]},
                            { width: 120, id: "invoice", header: [{ text: "Invoice Id" },{ content: "inputFilter" }]}
                        ],
                        data: data,
                        height: 300
                    });
                    
                    //load the summaryDo function when click order id
                    grid.events.on("cellclick", function (row, column, conf) {
                        //when click order id to call the DO summary screen
                        if (column.id === "orderid") {
                            that.fnSummaryDO(row.txnid);
                        }
                        if (column.id === "parentorderid") {
                            that.fnSummaryDO(row.donum);
                        }
                    });
                
                    //add class on cell based on column
                    grid.data.forEach(function (element, index, array) {
                        grid.addCellCss(element.id, "orderid", "gmLinkText");
                        grid.addCellCss(element.id, "parentorderid", "gmLinkText");
                    });
                }
                else{
                    $("#orderListPO").html("<span>No Data Found</span>");
                }
            });
            
            $(".po-report-close").unbind('click').bind('click', function () {
                $(".po-report-close").remove();
                $("#div-crm-overlay-content-container2").addClass("hide").removeClass("poOrderlistPopCustom");
                $("#div-crm-overlay-content-container2 .modal-title").text("");
                $("#div-crm-overlay-content-container2 .modal-body").empty();
            });
        }
		

	});	

	return PODashboardView;
});
