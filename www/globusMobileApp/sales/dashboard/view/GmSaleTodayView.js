/**********************************************************************************
 * File:        GmSaleTodayView.js
 * Description: Display More Filters options to save and use in the future
 * Version:     1.0
 * Author:      anitha
 **********************************************************************************/

define([
    'jquery', 'underscore', 'backbone',  'handlebars',
    'global','device','notification','datasync','saleitemmodel', 'saleitemcollection', 'saleitemlistview', 'dropdownview'
    
    ], 

    function ($, _, Backbone, Handlebars, Global, Device, Notification, Datasync, SaleItem, SaleItems, SaleItemListView, DropDownView) {

	'use strict';
	var SaleTodayView = Backbone.View.extend({

		events: {
			"click .ul-dash-saletoday" : "openDrillDown",
			"click #div-dash-saletoday-drilldown-close-icon" : "closeDrillDown",
			"click .div-dash-saletoday-tab" : "toggleContent",
			"click .div-dash-saletoday-drilldown-list-item" : "openDrillDownView"
		},
		
		/* Load the templates */ 
		template: fnGetTemplate(URL_Dashboard_Template, "GmSaleToday"),

		tplSpinner: fnGetTemplate(URL_Common_Template, "GmProgressSpinner"),

		template_legends: fnGetTemplate(URL_Dashboard_Template, "GmSaleTodayChartLegends"),

		//Initializes the view
		initialize: function(options) {
			this.el = options.el;
			this.parent = options.parent;
			//Get the stored filters and token
			this.serverInput = {"token": localStorage.getItem("token"),"companyid": (localStorage.getItem("companyid"))?localStorage.getItem("companyid"):localStorage.getItem("userCompany"),"divids": (localStorage.getItem("divisionid"))?localStorage.getItem("divisionid"):localStorage.getItem("userDivision"),"zoneids": (localStorage.getItem("zoneid"))?localStorage.getItem("zoneid"):"","regionids":(localStorage.getItem("regionid"))?localStorage.getItem("regionid"):"","fsids": (localStorage.getItem("fieldsalesid"))?localStorage.getItem("fieldsalesid"):""};
			this.render(true);
		},
		
		render: function (rerendervalue) {
				var that = this;
				if(rerendervalue)
					this.$el.html(this.template());
				this.serverInput.salesdbcurrtype = localStorage.getItem("salesDbCurrType");
			    this.serverInput.salesdbcurrsymbol = localStorage.getItem("salesDbCurrSymbol");
				this.$("#div-dash-saletoday-main").hide();
				this.$(".div-dash-page-loader").html(this.tplSpinner({"message":lblNm("msg_data_fetching")})).show();
            
                this.parent.$("#dash-saletoday-toquota").remove();
                if (GM.Global.ToQuota != undefined && screen.availWidth <= 420 ) {
                    this.parent.$("#div-dash-subtitle").after("<section class='gmPaddingTop5p' id='dash-saletoday-toquota'>"+GM.Global.ToQuota.type +" - "+ GM.Global.ToQuota.mtd+"</section>"); 
                }
            
				//Calls the webservice
				fnGetServerData("sales/todayssales", undefined, this.serverInput, function(data){
				
				that.viewData = data;
				if(that.viewData.listGmSalesDashResListVO!=undefined) {
					that.$(".div-dash-page-loader").html(that.tplSpinner({"message":lblNm("msg_loading_data_c")}));
					var arrColorData = new Array();
						if(that.viewData.listGmSalesDashResListVO.length!=undefined)
						arrColorData = that.viewData.listGmSalesDashResListVO;
						else
						arrColorData.push(that.viewData.listGmSalesDashResListVO);

					var colors = new Array();
					colors =  ["#006633", "#FFCC33", "#FF00FF", "#0000FF", "#33FF33","#660033","#606060", "#66FFFF","#CC6600", "#666633","#FFB84D","#4D944D","#2952CC","#AD3333","#CC7A00","#85ADFF","#FF66FF","#6B008F","#E6005C","#B2FF99"];
					
					for(var i=0;i<arrColorData.length;i++) {
						if(i<20)
						arrColorData[i].color = colors[i];
						else
						arrColorData[i].color = "#D80000";
						arrColorData[i].displayvalue = that.numberWithCommas(arrColorData[i].value);
						arrColorData[i].percentage = that.numberWithCommas(arrColorData[i].percentage);
					}

					that.showTable();
					that.showChart();
					if(rerendervalue)
						that.showViewBy();
				}
				else{
                    var salesDbCurrSymbol = localStorage.getItem("salesDbCurrSymbol");
					that.$(".div-dash-page-loader").html(lblNm("msg_no_data"));
					that.parent.$("#div-dash-subtitle").html(" - "+salesDbCurrSymbol+"0.00");
				}
		});

				return this;
		},

	 	numberWithCommas:function(numdata){

	 		var amtdata = parseFloat(numdata).toFixed(2);
		 	return amtdata.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		},

		//Calls the showItemListView to display the data
		showTable: function() {
			this.showItemListView(this.$("#div-dash-saletoday-list"), this.viewData.listGmSalesDashResListVO, "GmSDSaleTodayItem", URL_Dashboard_Template,0);
		},

		showViewBy: function() {

			var viewByData = this.processData(this.viewData.viewByList,"code"), that=this;
			if(this.viewByDropdown) 
				this.viewByDropdown.close();
			this.viewByDropdown = new DropDownView({
				el : this.$("#div-dash-saletoday-viewby"),
				data : viewByData,
				DefaultID : this.viewData.filterby
			});

			this.$("#div-dash-saletoday-viewby").bind('onchange', function (e, selectedId) {
					that.updateView(selectedId);
			});

		},

		processData: function(data,key) {
			var strData = JSON.stringify(data);
			var finalData = new Array();
			var processedData = JSON.parse(strData,function  (k, v) {
				if (k === (key+"nmalt")) 
			        this.ID = v;
			    else if (k === (key+"nm"))
			        this.Name = v;
			    else
			        return v;
			});
			

			if(processedData.length==undefined) {
				finalData.push(processedData);
				return finalData; 
			}
			else
				return processedData;
		},

		updateView: function(id) {
			console.log("selected id :" + id);
			this.serverInput.filterby = id;
			var that = this;
			this.render(false);
		},

		//display the chart from the webservice data
		showChart: function() {
            var that = this, chartWidth = "550", chartHeight = "450";
            if(screen.availWidth <= 420) {
               chartWidth =  "95%";
               chartHeight =  "45%";
            }
			this.chart = new FusionCharts({

							type: "pie3d",	//type of chart
							renderAt: "div-dash-saletoday-chart", //chart div area where the chart has to rendered
							width: chartWidth, //width of the chart div area
							height: chartHeight, //height of the chart div area
							dataFormat: "json", //format of data
							
							
						});

			var chartOptions = {
						showLabels: "0", //hide the labels
						showValues: "0", //hide the values
						showpercentintooltip: "0",
						numberSuffix: "%",
						slicingDistance: "0",
						theme: "zune" //refers to theme
					};
			var contentData = new Array(), chartData = new Array();
			var i =0, sum =0;
			if(this.viewData.listGmSalesDashResListVO.length!=undefined)
				contentData = this.viewData.listGmSalesDashResListVO;
			else
				contentData.push(this.viewData.listGmSalesDashResListVO);
			
			for(i =0;((i<contentData.length) && (i<20));i++) {
				var value = contentData[i].percentage;
				if(value.indexOf("-")>=0)
					value=0;
				var columnData = {"Label" : contentData[i].name, "Value" : value, "color" : contentData[i].color };
				chartData.push(columnData);
			}
			
			
			for(;i<contentData.length;i++) {
				if(parseFloat(contentData[i].percentage)>=0)
					sum = sum + parseFloat(contentData[i].percentage);
			}
			var columnData = {"Label" : "Others", "Value" : sum, "color" : "#D80000"};
			if(sum!=0)
				chartData.push(columnData);
			
			this.showItemListView(this.$("#div-dash-saletoday-chart-legend"), chartData, "GmSaleTodayChartLegends", URL_Dashboard_Template,0);
			var combinedChartData = {"chart" : chartOptions, "data" : chartData};
			this.chart.setChartData(combinedChartData, "json");
			this.chart.render();
			this.$("#div-dash-saletoday-main").show();
			this.$(".div-dash-page-loader").hide();
//			this.renderChart();
		},
		renderChart: function() {
			switch(window.orientation) 
		    {  
		      case -90:
		      case 90:

	             if(screen.availHeight <= 548 && screen.availWidth == 320) {
				        this.chart.resizeTo("170","310");
				        this.chart.render();	 
		    	  }
		        break; 
		      default:
	             if(screen.availHeight <= 548 && screen.availWidth == 320) {
				        this.chart.resizeTo("310","170");
						// this.chart.chartTopMargin("-140");
				        // this.chart.chartLeftMargin("-100");
				        // this.chart.chartRightMargin("100");
				        this.chart.render();	 
		    	  }
		        break; 
		    }
			
		},

		openDrillDown: function(event) {
			console.log(event.currentTarget.id);
			if(event.currentTarget.id!="") {
				this.$("#div-dash-saletoday-drilldown-main").show();
				var inputData = this.serverInput, that = this;
				inputData.stropt = "detail";
				inputData.view = event.currentTarget.id;
				inputData.filterby = this.viewByDropdown.getCurrentValue()[0];
				this.$(".div-dash-widget-loader").html(this.tplSpinner({"message":lblNm("msg_data_fetching")}));

				fnGetServerData("sales/todayssalesdetail", undefined, inputData, function(data){
					that.$(".div-dash-widget-loader").html(that.tplSpinner({"message":lblNm("msg_loading_data_c")}));
					var displayData = new Array();
					(data.listGmSalesDrilldownVO.length!=undefined)?displayData=data.listGmSalesDrilldownVO:displayData.push(data.listGmSalesDrilldownVO);

					for(var i=0;i<displayData.length;i++) {
						var j=1, currRow = displayData[i], keyword="";
						for (var column in currRow) {
							if((column == ("vpnm")) || (column == ("adnm")) || (column == ("fieldsalesnm")) || (column == ("repnm")) || (column == ("accountnm")) || (column == ("amount"))) {
								j++;
								keyword += currRow[column] + " ";
							}
						}
						displayData[i].Name = keyword;
					}


					for(var i=0;i<displayData.length;i++) {
						displayData[i].displayAmount = that.numberWithCommas(displayData[i].amount);
					}
					console.log(displayData);
					that.showItemListView(that.$("#div-dash-saletoday-drilldown-list"), displayData, "GmSaleTodayDrillDownList", URL_Dashboard_Template,1);
					that.$("#div-dash-saletoday-drilldown-list-content-loader").hide();
					that.$("#div-dash-saletoday-drilldown-list").show();
					var grandTotal = that.numberWithCommas(data.value);
					if(grandTotal.indexOf("-")<0)
						that.$("#div-dash-saletoday-drilldown-footer-value").html(grandTotal).removeClass("content-red");
					else {
						grandTotal = grandTotal.replace("-","");
						that.$("#div-dash-saletoday-drilldown-footer-value").html("(" + grandTotal + ")").addClass("content-red");
					}
					
				});
			}
		},

		closeDrillDown: function () {
			this.$("#div-dash-saletoday-drilldown-main").hide();
			this.$("#div-dash-saletoday-drilldown-list-content-loader").show();
			this.$("#div-dash-saletoday-drilldown-list").hide();
		},

		openDrillDownView: function(event) {
			window.location.href = "#dashboard/drilldown/order/today/orders&submitted/"+ event.currentTarget.id + "/" + $(event.currentTarget).find(".div-dash-saletoday-drilldown-account").text();
		},
		
		toggleContent: function (event) {
			var id = (event.currentTarget.id).replace("div-dash-saletoday-tab-","");
			this.$(".selected-tab").removeClass("selected-tab");
			$(event.currentTarget).addClass("selected-tab");

			if(id=="table") {
				this.$("#div-dash-saletoday-data").show();
				this.$("#div-dash-saletoday-chart").hide();
				this.$("#div-dash-saletoday-chart-legend").hide();
				// FusionCharts(this.chart).dispose();
			}
			else {
				this.$("#div-dash-saletoday-data").hide();
				this.$("#div-dash-saletoday-chart-legend").show();
				this.$("#div-dash-saletoday-chart").show();
//				this.renderChart();
			}

		},	
		
		//Initializes the ItemListView in the given HTML Element with the given collection
		showItemListView: function(controlID, data, itemtemplate, template_URL, columnheader) {
			var items = new SaleItems(data);	

			var salesDbCurrSymbol = localStorage.getItem("salesDbCurrSymbol");
            this.$("#span-dash-saletoday-amount").html("("+salesDbCurrSymbol+")");
			this.$("#span-dash-saletoday-drilldown-amount").html("("+salesDbCurrSymbol+")");
			var subTitleAmountValue = this.numberWithCommas(parseFloat(this.viewData.value).toFixed(2));
			if(subTitleAmountValue.indexOf("-")>=0) {
				subTitleAmountValue = salesDbCurrSymbol+"(" + subTitleAmountValue.replace("-","") + ")";
				this.parent.$("#div-dash-subtitle").html(" - " + subTitleAmountValue).addClass("content-red");
			}
			else
				this.parent.$("#div-dash-subtitle").html(" - "+salesDbCurrSymbol+"" + subTitleAmountValue);

			this.sditemlistview = new SaleItemListView(
					{ 
						el: controlID, 
						collection: items, 
						itemtemplate: itemtemplate,
						template_URL: template_URL,
						columnheader: columnheader,
						callbackOnReRender: this.formatNegativeAmount
					}
			);
		},

		formatNegativeAmount: function(option) {
			option.$(".li-dash-saletoday-amount").each(function() {
				var value = $(this).text();
				if(value.indexOf("-") >= 0) {
					value = value.replace("-","");
					$(this).html("("+value+")");
					$(this).addClass("content-red");
				}
			});
			option.$(".div-dash-saletoday-bottom-amount").each(function() {
				var value = $(this).text();
				if(value.indexOf("-") >= 0) {
					value = value.replace("-","");
					$(this).html("("+value+")");
					$(this).addClass("content-red");
				}
			});
			option.$(".div-dash-saletoday-drilldown-amount").each(function() {
				var value = $(this).text();
				if(value.indexOf("-") >= 0) {
					value = value.replace("-","");
					$(this).html("("+value+")");
					$(this).addClass("content-red");
				}
			});
		},
		
		// Close the view and unbind all the events
		close: function(e) {
			this.unbind();
			this.remove();
		}

	});	

	return SaleTodayView;
});
