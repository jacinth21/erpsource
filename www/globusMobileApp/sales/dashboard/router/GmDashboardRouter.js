/**********************************************************************************
 * File:        GmDashboardRouter.js
 * Description: Router specifically meant to route views within the 
 *              dashboard application
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',
        'dashmainview','../sales/dashboard/view/GmOrderPODashboardView'
         
      ], 
    
    function ($, _, Backbone,DashMainView,PODashboardView) {

	'use strict';

	var DashboardRouter = Backbone.Router.extend({

		routes: {
			"dashboard": "dashboard",
			"dashboard/:viewType": "dashboard",
			"dashboard/drilldown/:title/:subTitle/:url" : "drillDown",
			"dashboard/drilldown/:title/:subTitle/:url/:accID/:accName" : "drillDown",
			"dashboard/mtddrilldown/:filter" : "mtdDrillDown",
			"dashboard/postatus/report/:status" : "poStatusReport"
		},

		dashboard: function(viewType){
            $("#div-messagebar").show();
            hideMessages();
			this.closeView(dashmainview);

			var option = [];
			option.push(viewType);
			if(viewType!="morefilter")
				moreFilterHash = window.top.location.hash;
            

            if((navigator.onLine) && (!intOfflineMode)) {
                
                this.serverInput = {
                 "token": localStorage.getItem("token"),
                 "companyid": (localStorage.getItem("companyid"))?localStorage.getItem("companyid"):localStorage.getItem("userCompany"),
                 "divids": (localStorage.getItem("divisionid"))?localStorage.getItem("divisionid"):localStorage.getItem("userDivision"),
                 "zoneids": (localStorage.getItem("zoneid"))?localStorage.getItem("zoneid"):"",
                 "regionids":(localStorage.getItem("regionid"))?localStorage.getItem("regionid"):"",
                 "fsids": (localStorage.getItem("fieldsalesid"))?localStorage.getItem("fieldsalesid"):""
                 };    
                
                 fnGetWebServerData("salesDashBoardCurrency/currencySymbolType/", undefined,  this.serverInput, function(data){
                    localStorage.setItem("salesDbCurrType",data.salesdbcurrtype);
                    //localStorage.setItem("salesDbCurrSymbol",data.salesdbcurrsymbol);
                     console.log(" salesDbCurrSymbol "+data.salesdbcurrsymbol);
                     
                     if (data.salesdbcurrsymbol == "JPY") {
                           localStorage.setItem('salesDbCurrSymbol', String.fromCharCode(165));  
                    } else {
                        localStorage.setItem("salesDbCurrSymbol",data.salesdbcurrsymbol);
                    }

                    dashmainview = new DashMainView(option);
                    $("#div-main").html(dashmainview.el);
                 });
                
            } else {
                dashmainview = new DashMainView(option);
                $("#div-main").html(dashmainview.el);
            }
		},

		drillDown: function(title, subTitle, url, accID, accName) {
			this.closeView(dashmainview);

			var option = [];
			option.push("drilldown");
			option.push(title);
			option.push(subTitle);
			option.push(url);
			option.push(accID);
			option.push(accName);

			dashmainview = new DashMainView(option);
			$("#div-main").html(dashmainview.el);
		},

		mtdDrillDown: function(filter) {
			this.closeView(dashmainview);
			moreFilterHash = window.top.location.hash;

			var option = [];
			option.push("mtdDrillDown");
			option.push(filter);

			dashmainview = new DashMainView(option);
			$("#div-main").html(dashmainview.el);			
		},
        
        poStatusReport: function(status){
            this.closeView(dashmainview);
            
			var option = [];
			option.push("postatus");
			option.push({"drilldownurl":status});

			dashmainview = new DashMainView(option);
			$("#div-main").html(dashmainview.el);	
        },
		
		
		closeView: function(view) {
			if(view!=undefined) {
				view.close();
				view.unbind();
				view.remove();
				view = undefined;
				$("#btn-back").unbind();
			}

		}
	});

	return DashboardRouter;
});
