/**********************************************************************************
 * File:        GmcollateralRouter.js
 * Description: Router specifically meant to route views within the 
 *              collateral application
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',
         'collateraldetailview', 'shareview', 'collateralview', 'sharestatusview','collateralemailtrackview'
      ], 
    
    function ($, _, Backbone, CollateralDetailView, ShareView, CollateralView, ShareStatusView,CollateralEmailTrackView) {

	'use strict';

	var CollateralRouter = Backbone.Router.extend({

		routes: {
			"collateral": "collateral",
			"collateral/:name":"sidePanel",
			"collateral/Technique/:id": "sidePanelTechnique",
			"collateral/detail/:itemType/:itemID": "collateraldetail",
			"collateral/detail/:itemType/:itemID/:viewType": "collateraldetail",
			"collateral/detail/:itemType/:itemID/:viewType/:docID": "collateraldetail",
			"share": "share",
			"oldCollateral": "reloadCollateral",
			"collateralclosedSidePanel" : "collateralclosedSidePanel",
			"sharestatus" : "shareStatus",
            "guide": "guideView"
		},

		initialize: function () {
			var self = this;
		},

		collateral: function(){
            $("#div-messagebar").show();
            hideMessages();
			mchistory = document.URL;
			recentHash = window.top.location.hash;
			this.closeView(collateralInitial);
			this.closeView(collateralDetail);
			collateralInitial = new CollateralView();
			$("#div-main").html(collateralInitial.el);
		},

		share:function(){
			mchistory = document.URL;
			var that = this;
			if(shareView) {
				this.closeView(shareView);
				shareView = new ShareView();
				$("#div-main-detail").html(shareView.el);
			}

			else {
				this.closeView(collateralInitial);
				collateralInitial = new CollateralView(function () {
					collateralInitial.openItemDetail();
					that.closeView(shareView);
					shareView = new ShareView();
					$("#div-main-detail").html(shareView.el);
				});
				$("#div-main").html(collateralInitial.el);
			}
		},

		collateraldetail: function(itemType, itemID, viewType, docID) {
			mchistory = document.URL;
			recentHash = window.top.location.hash;
			var option = [];
			option.push(itemID);
			option.push(itemType);
			option.push(viewType);
			option.push(docID);

            if(GM.Global.EmailTrackFL = "Y") {
                GM.Global.EmailTrackFL = "";
                collateralDetail = undefined;
            }

			if(collateralDetail) {
				this.closeView(collateralDetail);
				collateralDetail = new CollateralDetailView(option);
				$("#div-main-detail").html(collateralDetail.el);
			}

			else {
				this.closeView(collateralInitial);
				collateralInitial = new CollateralView(function () {
					collateralInitial.openItemDetail();
					collateralDetail = new CollateralDetailView(option);
					$("#div-main-detail").html(collateralDetail.el);
				});
				$("#div-main").html(collateralInitial.el);
			}
		},

		sidePanel: function(name) {
			var that = this;
			mchistory = document.URL.replace('/' + name,'');
            var option = [];
            if(name=='Email Tracker'){
				collateralInitial = new CollateralView(function () {
					collateralInitial.openItemDetail();
					that.clearView(collateralEmailTrack);
					collateralEmailTrack = new CollateralEmailTrackView();
					$("#div-main-detail").html(collateralEmailTrack.el);
				});
				$("#div-main").html(collateralInitial.el);
            }
            else{
                if(collateralInitial) {
				collateralInitial.comeBack(['sampleObj']);
				collateralInitial.showSidePanel(name);
			}
            }
			
		},

		reloadCollateral: function() {
			mchistory = document.URL;
			recentHash = window.top.location.hash;
			this.closeView(collateralDetail);
			collateralInitial.reInitialize(true);
			collateralInitial.$("#div-main-collateral").show();
		},
		
		sidePanelTechnique: function(id) {
			var that = this;
			if(collateralInitial) {
				if($("#div-sidepanel").css("display")!="none") {
					collateralInitial.sidepanelview.showSystemsforTechnique(id);
				}
				else {
					collateralInitial.comeBack(['sampleObj']);
					collateralInitial.showSidePanel("Technique",id);
				}
			}
		},
		
		collateralclosedSidePanel: function() {
			window.history.replaceState("",'',recentHash);
		},

		shareStatus: function() {
			mchistory = document.URL;
			recentHash = window.top.location.hash;
			var that = this;
			if(shareStatusScreen) {
				this.closeView(shareStatusScreen);
				shareStatusScreen = new ShareStatusView();
				$("#div-main-detail").html(shareStatusScreen.el);
			}

			else {
				this.closeView(collateralInitial);
				collateralInitial = new CollateralView(function () {
					collateralInitial.openItemDetail();
					that.closeView(shareStatusScreen);
					shareStatusScreen = new ShareStatusView();
					$("#div-main-detail").html(shareStatusScreen.el);
				});
				$("#div-main").html(collateralInitial.el);
			}
		},
        
        guideView: function() {
            try {
                GM.Global.GuideFl = "Y";
                this.closeCollteralViews();
                var rimbGuideID = localStorage.getItem("rimbguide");
                if (parseNull(rimbGuideID) != "") {

                    fnGetFileInfo(rimbGuideID, function(file) {
                        if (file != "")
                            window.location.href = "#collateral/detail/system/" + file.REFID + "/documents";
                        else
                            showError("No data found, Please try later");
                    });
                } else {
                    showError("Please try to logout and login again");
                }
            } catch (err) {
                showAppError(err, "GmCollateralRouter:guideView():rimbguide" + rimbGuideID);
            }
        },
        
        closeCollteralViews: function (view) {

          if (view != undefined) {
              view.unbind();
              view.remove();
              view = undefined;
              $("#btn-back").unbind();
          }

          if (collateralInitial != undefined) {
              collateralInitial.unbind();
              collateralInitial.remove();
              collateralInitial = undefined;
          }

          if (collateralDetail != undefined) {
              collateralDetail.unbind();
              collateralDetail.remove();
              collateralDetail = undefined;
          }

          if (shareView != undefined) {
              shareView.unbind();
              shareView.remove();
              shareView = undefined;
          }

          if (shareStatusScreen != undefined) {
              shareStatusScreen.unbind();
              shareStatusScreen.remove();
              shareStatusScreen = undefined;
          }
            if(collateralEmailTrack != undefined) {
				collateralEmailTrack.unbind();
				collateralEmailTrack.remove();
				collateralEmailTrack = undefined;
			}
      },
		clearView: function (view) {
			if (view) {
				if($.xhrPool.length){
					_.each($.xhrPool, function(xhr){
						if(xhr && xhr.readyState != 4){
							xhr.abort();
						}
					})
					$.xhrPool = [];
				}
				console.log("Close " + view.name);
				view.unbind();
				view.undelegateEvents();
				view = undefined; 
			}
		},

		closeView: function(view) {
			if(view!=undefined) {
				view.unbind();
				view.remove();
				view = undefined;
				$("#btn-back").unbind();
			}

			if(catalogInitial!=undefined) {
				catalogInitial.unbind();
				catalogInitial.remove();
				catalogInitial = undefined;
			}
		
			if(catalogDetail!=undefined) {
				catalogDetail.unbind();
				catalogDetail.remove();
				catalogDetail = undefined;
			}
		}
	});

	return CollateralRouter;
});
