/**********************************************************************************
 * File:        GmContactsView.js
 * Description: View to display the Form for Adding / Editing a Contact information.
 				It is a sub view for GmShareView.js
 * Version:     1.0
 * Author:      praja
 Progbuild verification
 **********************************************************************************/


define([
        'jquery', 'underscore', 'backbone', 'handlebars',
        'global',
        'itemmodel','itemcollection', 'itemlistview','contactsmodel', 'fieldview', 'shareview'
        ], function ($, _, Backbone, Handlebars, Global, ItemModel, ItemCol, ItemListView ,ContactsModel, FieldView, ShareView) {

	'use strict';
	var searchopt;
	var phoneNoContactID;
	
	var ContactsView = Backbone.View.extend({

		events : {
			
			"change #content-type-drop-down" : "showTextnpi",
			"blur .text-new-contacts": "validate",
			"click #add-to-contact": "AddNewcontacts",
			"click #li-contacts-delete" : "deleteContacts"
			
		},
	
		template: fnGetTemplate(URL_Collateral_Template, "GmAddNewContacts"),
		

		initialize: function(event) {
			this.id = $(event.currentTarget).attr('name');
//			showMsgView("011");
		},

		render: function () {
			var that = this;
			this.$el.html(this.template());
			if(this.id!= undefined) {
				fnGetContactsAdded(this.id,function(data){
					fnGetPhoneNO(data.partyid,function(data){
						phoneNoContactID = data.ID;
							if(data.contacttype == "90450"){
								$("#txt-phone").val(data.contactvalue);
							}
							
					});

						
					$("#txt-emailid").val(data.Email);
					$("#txt-fname").val(data.FirstName);
					$("#txt-lname").val(data.LastName);
					$("#txt-mname").val(data.MI);
					$("#txt-npi").val(data.npiid);
					$("#content-type-drop-down").val(data.Type);
					$("#content-type-drop-down").trigger("change");
					
					that.module = data.Module;
					if(that.module == "Local"){
						that.$("#li-contacts-delete").removeClass("li-contacts-delete-hide").addClass("li-contacts-delete-show");

					}
				});

				
			}
			
			return this;
		},
		
		
		addtoDevice:function(event){
			var name = $("#txt-fname").val() + " " + $("#txt-lname").val();
			var email = new ContactField('work', $("#txt-emailid").val(), true);
			var emails = [];
			emails[0] = email;
			var saveContact = navigator.contacts.create({"displayName": name, "emails" : emails});
			saveContact.save();
		},

		showTextnpi:function(e){
			if($("#content-type-drop-down").val() == "7000"){
				$("#div-npi").show();
			}
			else{
				$("#div-npi").hide();
			}
		},

		saveNewContacts:function(e,callback){
		
			//$("#li-contacts-new").show();
			var partyType = $("#content-type-drop-down").val();
			var npiid = $("#txt-npi").val();
			var fname = $("#txt-fname").val();
			var mname = $("#txt-mname").val();
			var lname = $("#txt-lname").val();
			var emailid = $("#txt-emailid").val();
			var phoneNo = $("#txt-phone").val();
			var regnamex = "^[a-zA-Z\s]+$";
            var regemailx = "^(.+)@([^\.].*)\.([a-z]{2,})$";
            
            //$('.myCheckbox').prop('checked', true);
			//$('.myCheckbox').prop('checked', false);
              

			if((fname == "") || (lname == "") || (emailid == "")){
				$(".text-new-contacts").trigger("blur");

			}

			else {
				
				if(this.module!="Local") {
					fnInsertNewParty(partyType,npiid,fname,mname,lname,emailid,phoneNo,function() {
						$(".txt-search-contact").val('');
						$("#div-contacts-list").html('');
                        $(".txt-search-contact").focus();
						showMsgView('045');
					});
				}
				else {
					fnUpdatePartyContact(this.id,partyType,npiid,fname,mname,lname,emailid,phoneNo,phoneNoContactID,function () {

						$(".txt-search-contact").val('');
						$("#div-contacts-list").html('');
                        $(".txt-search-contact").focus();
						showMsgView('045');
					});
				}

				if($('#add-to-contact').find('input').prop('checked')) {
					var name = fname + " " + lname;
					var email = new ContactField('work', emailid, true);
					var emails = [];
					emails[0] = email;
					var phone = new ContactField('work', phoneNo, true);
					var phoneNumbers = [];
					phoneNumbers[0] = phone;
					var saveContact = navigator.contacts.create({"displayName": name, "emails" : emails, "phoneNumbers" : phoneNumbers});
					saveContact.save();
				}
				$("#li-contacts-save").hide();
				$("#li-contacts-cancel").hide();
				$("#li-contacts-new").show();
			
			}
			
       },

       deleteContacts: function(e) {
       		fnDeletePartyContact(this.id, function () {
       			$(".txt-search-contact").val('');
				$("#div-contacts-list").html('');
				$("#li-contacts-btn-search").trigger("click");
				showMsgView('047');
				fnGetFavoriteEmail(function (count) {
					var count = count.length;
					$("#spn-fav-count").html("&nbsp;" + count + "&nbsp;");
				});
       		});
       },
		
       closeView: function(e) {
			$("#li-contacts-new").show();
			if($(".txt-search-contact").val().length<2)
				$("#div-contacts-list").html('');
            $(".txt-search-contact").trigger("click");
            $(".txt-search-contact").focus();          
			this.close();

		},
		
		validate: function() {
	            var contactsValidate = new ContactsModel();  
	            $("input").each(function() {
	            var id = $(this).attr('id');
	            if((id!="txt-phone") && (id!="txt-mname"))
	                new FieldView({el: this, model: contactsValidate});
	            });
		},


		
		close: function() {
			this.$el.undelegate();
			this.$el.unbind();
			//this.$el.remove();
		}

	});

	return ContactsView;
});
