/**********************************************************************************
 * File:        GmColateralSidePanelView.js
 * Description: View to display the side panel, 
                called in the Collateral View after the Home View
 * Version:     1.0
 * Author:      asivaraj
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global',  'notification', 'daosearch',
        'itemmodel', 'itemcollection', 'itemlistview'
        ], function ($, _, Backbone, Handlebars, Global, Notification, DAOSearch, ItemModel, ItemCollection, ItemListView) {

	'use strict';
	var url = "collateral";

	var SidePanelView = Backbone.View.extend({

		events: {

			/* Side Panel's collapsible */
			"click .li-above": "collapseULs",

			/* Back button on Side Panel */
			"click #div-sidepanel-back" : "sidePanelBack"
		},

		template: fnGetTemplate(URL_Collateral_Template,"GmSidePanel"),
		template_recent: fnGetTemplate(URL_Collateral_Template,"GmRecent"),

		initialize: function() {
			this.strAppTitle = $("#div-app-title").text();
			this.strAppTitle = this.strAppTitle.toLowerCase().replace(/ /g, '');
			$("#div-app-title").html(lblNm("marketing_collateral"));
			this.render();
		},

		// Render the initial contents when the app starts
		render: function () {
			
			this.$el.html(this.template());
			this.strShowHide = "show";
			return this;
		},

		setNoDataMessage: function() {
			if((this.recentSystems == 0) && (this.recentSets == 0) && (this.recentParts == 0) && (this.recenGroups ==0)) 
				this.$("#div-sidepanel-list").html("<li class='li-loading'>"+lblNm("no_data_display")+"</li>");
		},

		//Show Side Panel
		showSidePanel: function(ql, callbackid ) {
			var that = this;
			var searchGroup = ql.toUpperCase();
			var that = this;
			this.resetQuickLink();
			this.$("#div-sidepanel-list").html("<li class='li-loading li-info-center'><img/>&nbsp;"+lblNm("loading")+"</li>");

			mcBreadcrumb =[];
			mcBreadcrumb.push("<a href='#collateral/"+ ql + "'>"+searchGroup+"</a>");
			mcRecentBreadCrumbs.push(mcBreadcrumb.slice(0));


			if(this.$(".btn-quicklink").hasClass('show')) {
			 	this.$(".panel").animate({
					left: "-=301"
				},100, function() {
				});
				this.$(".btn-quicklink").removeClass('show').addClass('hide');
			}
			this.$("#div-sidepanel").show();
			this.$("#div-node").html(searchGroup);
			
			switch (searchGroup) {
				
				case "TECHNIQUE":
                    $("#div-sidepanel-back").css({"visibility":"hidden"});
                   
					fnGetTechniqueList(url,function(data) {
						var rplce_value = searchGroup.toLowerCase();
                        
						  showMsgView("005", rplce_value.charAt(0).toUpperCase()+ rplce_value.slice(1), GM.Global.Mobile);

						that.showItemListView(that.$("#div-sidepanel-list"), data, "GmItem", 1, undefined, function() {
							if(callbackid!=undefined)
								that.showSystemsforTechnique(callbackid);
						}, URL_Common_Template);
					});
					break;
				
				case "SYSTEM":
                    $("#div-sidepanel-back").css({"visibility":"hidden"});
                    
					fnGetCollateralSystemList(url,function(data) {
						var rplce_value = searchGroup.toLowerCase();
                        
						  showMsgView("005", rplce_value.charAt(0).toUpperCase()+ rplce_value.slice(1), GM.Global.Mobile);
                        
						that.showItemListView(that.$("#div-sidepanel-list"), data, "GmSubItem", 1, 2, that.addCloseClass, URL_Common_Template);
					});
					break;

				case "RECENT":
					
                    $("#div-sidepanel-back").css({"visibility":"hidden"});
					var rplce_value = searchGroup.toLowerCase();
                    
					   showMsgView("040","", GM.Global.Mobile);
                    
					that.$("#div-sidepanel-list").html(that.template_recent());
					fnGetCollateralRecentSystem(url,function(data) {
							that.showItemListView(that.$("#div-sidepanel-list"), data, "GmItem", 1, undefined , that.addCloseClass, URL_Common_Template);
						});
					
					break;

				case "FAVORITE":
                    $("#div-sidepanel-back").css({"visibility":"hidden"});
					var rplce_value = searchGroup.toLowerCase();
                    
					   showMsgView("018","", GM.Global.Mobile);
                    
					fnGetCollateralFavorites(url,'system',function(data){
						that.showItemListView(that.$("#div-sidepanel-list"), data, "GmSubItem", 1, 2, that.addCloseClass, URL_Common_Template);
					});
					break;
			}

			
		},

		showSystemsforTechnique: function(id) {
			$("#div-sidepanel-back").css({"visibility":"visible"});
			var that = this;
			this.$("#div-sidepanel-list").html("<li class='li-loading'>"+lblNm("loading")+"</li>");

			fnGetTechniqueName(id,function(data){
				mcBreadcrumb.push("<a href='" + document.URL + "'>"+ data +"</a>");
				that.$("#div-node").html(mcBreadcrumb.join(' > '));
				mcRecentBreadCrumbs.push(mcBreadcrumb.slice(0));
				mcBreadcrumb.push("<a href='" + document.URL + "'>SYSTEM</a>");
			});
			
                showMsgView("020","",GM.Global.Mobile);
			 
			
			fnCollatealGetSystemListForTech(url,id,function(data) {
				that.showItemListView(that.$("#div-sidepanel-list"), data, "GmSubItem", 1, 2, that.addCloseClass, URL_Common_Template);
			});
		},


        // Go back on the Side Panel 
		sidePanelBack: function(event) {
            window.location.href = "#collateral/Technique";
        },

        /* Instantiate ItemListView with following arguments
		 * 
		 * controlID: The type of data 
		 * collection: Collection data from appropriate data source depending on the controlID selected
		 * template: Template used to render the results
		 * columnheader: 0 - Hide Columns and Search Textbox / 1 - Hide Columns / 2 - Hide Search Textbox / 3 - Show all 
		 * renderType: 1 - Unlimited Scroll / 2 - Collapsible
		 */
		showItemListView: function(controlID, data, itemtemplate, columnheader, renderType, callback, template_URL) {
			var items = new ItemCollection(data);

			if((this.itemlistview))
				this.itemlistview.close();

			this.itemlistview = new ItemListView(
					{ 
						el: controlID, 
						collection: items,
						columnheader: columnheader, 
						itemtemplate: itemtemplate,
						renderType: renderType,
						template_URL: template_URL
					});

			if(data.length == 0) {
				this.$("#div-sidepanel-list").html("<li class='li-loading'>"+lblNm("no_data_display")+"</li>");
			}

			if(callback!=undefined) 
				callback();

		},

		showItemListViewForRecent: function(controlID, data, itemtemplate, columnheader, renderType, callback, template_URL) {
			var items = new ItemCollection(data);
			
			var itemlistview = new ItemListView(
					{ 
						el: controlID, 
						collection: items,
						columnheader: columnheader, 
						itemtemplate: itemtemplate,
						renderType: renderType,
						template_URL: template_URL
					});

			if(callback!=undefined) 
				callback();
		},
		
		// Collapse the ULs for div-itemlist in Side Panel	
		collapseULs: function(event) {
			var elem = $(event.currentTarget);
			var alphabet = $(elem).text().toLowerCase().replace('+','').replace('-','');
			if($(elem).attr("class").indexOf("li-border-bottom") < 0 ) {
				$(elem).addClass("li-border-bottom");
			}
			else {
				$(elem).removeClass("li-border-bottom");
			}
			$("#ul-under-"+alphabet).slideToggle("fast");
			if($(elem).find('span').text()=="+") {
				$(elem).find('span').text("-");
			}
			else {
				$(elem).find('span').text("+");
			}
		},

		// Prepare to Close Catalog View by adding the appropriate class
		addCloseClass: function() {
			$(".btn-itemlist").each(function() {
				$(this).removeClass("btn-itemlist").addClass("btn-close-catalog");
			});
		},

		// Reset Quicklinks
		resetQuickLink: function() {
			$(".btn-quicklink").removeClass("btn-quicklink-light");
		}

	});

	return SidePanelView;
});
