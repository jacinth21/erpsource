/**********************************************************************************
 * File:        GmCollateralView.js
 * Description: View to display the Collateral's Main Page with various search options
 *              This view loads after the Home View.
 * Version:     1.0
 * Author:      asivaraj
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global',  'notification', 'daosearch','daomarketingcollateral',
        'itemmodel', 'itemcollection', 'itemlistview', 'imageviewerview', 'mmcsidepanelview'
        ], 

        function ($, _, Backbone, Handlebars, Global, Notification, DAOSearch, DAOMarketingCollateral, ItemModel, ItemCollection, ItemListView, ImageViewerView, SidePanelView) {

	'use strict';
	var url="collateral";

	var CollateralView = Backbone.View.extend({


		events : {

			/* Show Search Result */
			"click .btn-catalog-item, .btn-collateral-item": "searchresult",
			
			/* Close Catalog View */
			"click .btn-close-catalog": "openItemDetail",

			/* Hide Side Panel */
			"click #btn-close-panel, #div-filter, #txt-keyword": "hideSidePanel",

			/* Show Systems on Side Panel */
			// "click .btn-itemlist": "showSystems",

			/* Show Side Panel */
			// "click .btn-quicklink, #a-node-1": "showSidePanel",
			
			/* Hide Filter Panel */
			"click #div-catalog-quicklink, #div-catalog-system, #div-sidepanel, #txt-keyword": "hideFilterPanel",
			
			/* Show Filter Panel */
			"click #div-filter": "showFilterPanel",

			/* Back button on Side Panel */
            // "click #div-sidepanel-back" : "sidePanelBack",
            
			/* Clear keyword */
			"click #div-search-x": "clearKeyword",

			/* Set the Filter */
			"click .itm-filter": "setFilter",

            /* Search by keyword */
			"keyup #txt-keyword": "searchKeyword",

			/* Back to Catalog */
			"click .btn-quicklink": "comeBack",
			
			/* open image viewer for search */
			
			"click #btn-search-share": "showCollateralSharePopup",

			"click .div-icon-share" : "editShare"
			
		},

		/* Load the templates */ 
		template: fnGetTemplate(URL_Collateral_Template, "GmCollateral"),
		template_filter: fnGetTemplate(URL_Collateral_Template, "GmSearchFilter"),
		template_quicklink: fnGetTemplate(URL_Collateral_Template, "GmCollateralQuicklink"),
		template_system: fnGetTemplate(URL_Collateral_Template, "GmSystem"),
		template_multisearch: fnGetTemplate(URL_Collateral_Template, "GmCollateralMultiSearch"),
		template_Collateral_Popup : fnGetTemplate(URL_Collateral_Template,"GmCollateralSharePopup"),

		initialize: function(options) {
			$("#div-app-title").html(lblNm("marketing_collateral"));

			if(GM.Global.GuideFl == "Y")
			    $("#div-app-title").html(lblNm("reimbursement_guide"));

          //  $("#div-app-title").removeClass("gmMediaVisibilityH").addClass("gmMediaVisibilityV");
            
			if(options!=undefined)
				this.mainCallBack = options;
			$("body").removeClass("background-1").addClass("background-2").addClass("background-image");
            // $("#div-header").removeClass("background-2").addClass("background-1 background-image");
			$("#div-header").removeClass("iphone-header-small").addClass("iphone-header-big");
			this.$el.html(this.template());
			this.hideContents();
			$(window).on('orientationchange', this.onOrientationChange);
			this.render();
			localStorage.setItem('filter', '4000460');
			this.$("#div-sidepanel").hide();
			this.$("#div-search-result").hide();
			$("#btn-home").show();
			$("#btn-back").hide();
$("#div-footer").addClass("hide") ;
			this.minPartLen = 0;
			mcRecentBreadCrumbs = [];
		},

		// Render the initial contents when the Catalog View is instantiated
		render: function () {
			if(GM.Global.GuideFl != "Y") {
					showMsgView("003", "", GM.Global.Mobile);
			}
			this.sidepanelview = new SidePanelView({el:this.$("#div-sidepanel")});
			this.filters("Keyword");
			this.quicklinks();
            $('#div-app-title').removeClass('gmVisibilityHidden').addClass('gmVisibilityVisible');
			return this;
		},

		// Prepare to Close Catalog View by adding the appropriate class
		addCloseClass: function(controlID) {
			$(controlID).find(".btn-itemlist").each(function() {
				$(this).removeClass("btn-itemlist").addClass("btn-close-catalog");
			});
		},
        
		// Close the view and unbind all the events
		close: function(e) {
			this.unbind();
			this.remove();
		},

		
		comeBack: function(reinit) {
			
//			$("#div-app-title").html("Marketing Collateral");
			if(this.$("#div-catalog-filter").hasClass("in-detail")) {

				if(screen.availHeight <= 548 && screen.availWidth == 320) {

					this.$("#div-filter").hide();
					this.$("#div-catalog-filter").removeClass("div-catalog-filter-detail in-detail").addClass("div-catalog-filter-initial");
					this.$("#ul-catalog li").removeClass("ul-catalog-li-detail");
					this.$(".btn-quicklink").removeClass("btn-quicklink-detail").addClass("btn-quicklink-initial");
					this.$("#div-catalog-quicklink").removeClass("div-catalog-quicklink-detail");
				}

				else{
					
					$("body").removeClass("background-1").addClass("background-2 background-image");
					this.$("#ul-catalog li").addClass("ul-catalog-li-initial").removeClass("ul-catalog-li-detail");
					this.$("#div-catalog-filter").removeClass("div-catalog-filter-detail").addClass("div-collateral-filter-initial");
					this.$("#txt-keyword").removeClass("txt-keyword-detail").addClass("txt-keyword-initial");
					this.$(".btn-quicklink").removeClass("btn-quicklink-detail").addClass("btn-quicklink-initial");
					this.$("#div-catalog-quicklink").removeClass("div-catalog-quicklink-detail").addClass("div-catalog-quicklink-initial");
					this.$("#div-arrow-keyword").show();
					this.$("#div-filter").show();
				}

				this.$("#div-main-detail").hide();
				this.$("#div-catalog-filter").removeClass("in-detail");
				this.$("#txt-keyword").removeClass("in-detail-search-txt");
				this.$("#div-main-catalog").removeClass("in-detail-catalog");
				
				$("#btn-back").hide();

				this.filters("Keyword");
				window.localStorage.setItem("filter", '4000460');
				var strReinitSearch = true;
				if(typeof(reinit) == "object") {
					this.$("#div-catalog-quicklink").show();
					this.$("#div-search-result").hide();

					strReinitSearch = false;
				}

				if(reinit) {
					var wordLength = this.$("#txt-keyword").val().length;
					if((wordLength > 0) && (wordLength < 3))
						window.history.pushState("","","#collateral");
					this.reInitialize(strReinitSearch);
				}
						
			}
			
		},

		openItemDetail: function(e) {
			
			this.$(".btn-quicklink").removeClass("btn-quicklink-initial btn-selected").addClass("btn-quicklink-detail");
			this.$("#div-catalog-quicklink").removeClass("div-catalog-quicklink-initial").addClass("div-catalog-quicklink-detail");
			this.$("#div-catalog-quicklink").show();
			this.$("#ul-catalog li").addClass("ul-catalog-li-detail").removeClass("ul-catalog-li-initial");
			this.$("#txt-keyword").removeClass("txt-keyword-initial").addClass("txt-keyword-detail");
			this.$("#txt-keyword").val("");

			this.$("#div-catalog-filter").removeClass("div-collateral-filter-initial").addClass("div-catalog-filter-detail in-detail");
			//this.$("#txt-keyword").addClass("in-detail-search-txt");
			
			this.$("#div-filter").hide();
			this.$("#div-arrow-keyword").hide();
			this.$("#div-search-x").hide();
			this.$("#div-search-result").hide();
			this.$("#div-sidepanel").hide();
			
			this.$("#div-main-detail").show();     
			
			$("body").removeClass("background-2 background-image");
			this.$("#div-main-catalog").addClass("in-detail-catalog");
		},

		// Clear the keywords
		clearKeyword: function(e) {
			e.preventDefault();
//			showMsgView("011");
			this.$("#txt-keyword").val("");
			this.$("#txt-keyword").trigger("keyup");
//			this.$("#div-main-detail").hide();
		},

		// Prepare Search Filter List
		filters: function(SelectedItem) {
			var that = this;
			var mmcfilter = "MCFLTR";
			fnGetFilters(mmcfilter, SelectedItem, function(filterlist) {
				that.$("#div-filter-list").html(that.template_filter(filterlist));
				that.hideContents();
				that.$("#div-filter-text").html(SelectedItem);
				that.$("#div-filter-list").hide();
			});
            
		},

		// Hide Filter Panel
		hideFilterPanel: function() {
			if(this.$("#div-filter-list").css("display")=="block") {
				if(this.$("#div-arrow-keyword").hasClass("arrow-right")) {
	                this.$("#div-arrow-keyword").removeClass("arrow-right").addClass("arrow-down");    
	            }
	            else {
	                this.$("#div-arrow-keyword").removeClass("arrow-down").addClass("arrow-right");       
	            }
			}
			this.$("#div-filter-list").hide();
		},

		// Hide Side Panel
		hideSidePanel: function(e) {
			e.preventDefault();
            
                showMsgView("003", "", GM.Global.Mobile);
            
			this.resetQuickLink();
			this.bcname = "";
			this.options = undefined;
			if(this.$(".btn-quicklink").hasClass('ctlg-hide')){
				var that = this;
				this.$(".panel").animate ({
					left: "+=301"
				}, 100, function() {
					// Animation complete.
					that.$("#div-sidepanel").hide();
				});
				this.$(".btn-quicklink").removeClass('ctlg-hide').addClass('show');    
			}
			window.location.href = "#collateralclosedSidePanel";
		},

		// Align HTML Controls when the device orientation changes
		onOrientationChange: function() {
			switch(window.orientation) {  
			case -90:
			case 90:
				if(this.$(".btn-quicklink").hasClass("ctlg-hide")) {
					$('#div-sidepanel').css({left: 724});
				}
				else {
					if(this.$(".btn-quicklink").hasClass("show")) {
						$('#div-sidepanel').css({left: 1024}); 
					}
				}
				$("#div-filter-list").css("left","236px");
				break; 

			case -180:
			case 180:
			case 0:
			case 0:
				if(this.$(".btn-quicklink").hasClass("ctlg-hide")) {
					$('#div-sidepanel').css({left: 468});
				}
				else {
					if(this.$(".btn-quicklink").hasClass("show")) {
						$('#div-sidepanel').css({left: 768}); 
					}
				}
				$("#div-filter-list").css("left","108px");
			break; 


			default:
				if(this.$(".btn-quicklink").hasClass("ctlg-hide")) {
					$('#div-sidepanel').css({left: 468});
				}
				else {
					if(this.$(".btn-quicklink").hasClass("show")) {
						$('#div-sidepanel').css({left: 768}); 
					}
				}
				$("#div-filter-list").css("left","108px");
			break; 
			}   
		},
		
		// Prepares Quicklinks
		quicklinks: function() {
			var that = this;
			fnGetQuicklinks(function(quicklinklist) {
				that.$("#div-catalog-quicklink").html(that.template_quicklink(quicklinklist));
				that.hideContents();
				if(that.mainCallBack!=undefined){
					that.mainCallBack();
					that.mainCallBack = undefined;
				}
			});
            
		},
		
		// Change the background and hide the back button.
		reInitialize: function(key){

			$("body").removeClass("background-1").addClass("background-2").addClass("background-image");
            $("#div-header").removeClass("background-2").addClass("background-1 background-image");
	    	$("#btn-back").hide();
	    	this.$("#div-catalog-quicklink").show();
	    	if(key) {
	    		
	    		this.comeBack(false);
	    		this.$("#div-search-result").show();
	    		
	    		this.$("#div-catalog-quicklink").hide();
	    		this.$("#txt-keyword").val(this.letters);
	    		this.setX(this.letters.length);
	    	}

	    	if(window.top.location.hash == "#oldCollateral") {
	    		mcBreadcrumb = [];
	    		mcRecentBreadCrumbs = [];
	    		var tabName = $(".selectedTab").html().split('<')[0];
	        	mcBreadcrumb.push("<a href='#oldCollateral'>"+tabName+"</a>");
	            mcRecentBreadCrumbs.push(mcBreadcrumb.slice(0));
	    	}
		},

		
		// Render Search Results in the Side Panel
		renderResult: function(collection, letters, controlID, LengthControlID, searchField, callback) {

			var its = collection.search(searchField, letters);
			$(LengthControlID).html("&nbsp;" + its.length + "&nbsp;");
			this.showItemListView($(controlID), its, "GmItem", 0, 0,  callback, undefined, URL_Common_Template);
			$(".div-icon-share").each(function() {
								var elem = this;
	   							fnCheckDocuments($(this).attr('name'),function(status){
									if(status=="success") {
										$(elem).addClass('shared');
										$(elem).html('<i class="fa fa-times-circle-o fa-2x"></i>');
									}
									else {
										$(elem).html('<i class="fa fa-share-alt-square fa-2x"></i>');
									}
								});
			});

			if($(controlID).find("#div-itemlist").text().trim()=="") {
				$(controlID).find("#div-itemlist").html("<li>"+lblNm("no_results_found")+"</li>");
			}
		},
		
	    // Reset Quicklinks
		resetQuickLink: function() {
			this.$(".btn-quicklink").removeClass("btn-selected");
		},

		// Search by Keyword entered in txt-keyword
		searchKeyword: function(e) {
			
			this.comeBack();
			var letters = $(e.currentTarget).val();
			var wordLength = letters.length;
			var that = this;
			var resultdiv = "#div-search-result";
			this.letters = letters;
			this.setX(wordLength);
			
			switch (true) {
					case (wordLength < 3):
						this.toggleResult();
						break;
		
					case (wordLength == 3):
						that.$("#div-search-result").html(that.template_multisearch());
						
						fnGetCollateralSystems(url,letters, function(sys) {
							that.systemcol = new ItemCollection(sys);
							$("#spn-no-systems").html("&nbsp;" + sys.length + "&nbsp;");
							that.showItemListView("#div-sub-result-system", sys, "GmItem", 0, 0,  that.addCloseClass,undefined, URL_Common_Template);
							that.toggleResult("show");
						});
						
						fnGetSystemImages(url,letters,function(rsImages) {
							that.imagecol = new ItemCollection(rsImages);
							$("#spn-no-images").html("&nbsp;" + rsImages.length + "&nbsp;");
							
							that.showItemListView("#div-sub-result-image", rsImages, "GmItem", 0,0, that.addCloseClass,undefined, URL_Common_Template);
							
							$(".div-icon-share").each(function() {
								var elem = this;
	   							fnCheckDocuments($(this).attr('name'),function(status){
									if(status=="success") {
										$(elem).addClass('shared');
										$(elem).html('<i class="fa fa-times-circle-o fa-2x"></i>');
									}
									else {
										$(elem).html('<i class="fa fa-share-alt-square fa-2x"></i>');
									}
								});
						});
							that.toggleResult("show");
						});
						
						fnGetDocuments(url,letters ,function(rsDocuments) {
						
							that.documentcol = new ItemCollection(rsDocuments);
		
							$("#spn-no-documents").html("&nbsp;" + rsDocuments.length + "&nbsp;");
							that.showItemListView("#div-sub-result-document", rsDocuments, "GmItem", 0,0, that.addCloseClass,undefined, URL_Common_Template);
							$(".div-icon-share").each(function() {
								var elem = this;
	   							fnCheckDocuments($(this).attr('name'),function(status){
									if(status=="success") {
										$(elem).addClass('shared');
										$(elem).html('<i class="fa fa-times-circle-o fa-2x"></i>');
									}
									else {
										$(elem).html('<i class="fa fa-share-alt-square fa-2x"></i>');
									}
								})
							});

	    					that.toggleResult("show");
						});

							that.shareCount();
						
						fnGetVideos(url,letters,function(rsVideos) {
							that.videocol = new ItemCollection(rsVideos);
							$("#spn-no-videos").html("&nbsp;" + rsVideos.length + "&nbsp;");
							
							that.showItemListView("#div-sub-result-video", rsVideos, "GmItem", 0,0, that.addCloseClass,undefined, URL_Common_Template);
							
							
							$(".div-icon-share").each(function() {
								var elem = this;
	   							fnCheckDocuments($(this).attr('name'),function(status){
									if(status=="success") {
										$(elem).addClass('shared');
										$(elem).html('<i class="fa fa-times-circle-o fa-2x"></i>');
									}
									else {
										$(elem).html('<i class="fa fa-share-alt-square fa-2x"></i>');
									}
								});
						});
							that.toggleResult("show");
						});
							
						break;
						
					case (wordLength > 3):
						that.$("#div-search-result").show();
						that.$("#div-catalog-quicklink").hide();
						that.renderResult(that.systemcol, letters, "#div-sub-result-system", "#spn-no-systems","SearchItemName", that.addCloseClass);
						that.renderResult(that.imagecol, letters, "#div-sub-result-image", "#spn-no-images","SearchItemName",that.addCloseClass);
						that.renderResult(that.documentcol, letters, "#div-sub-result-document", "#spn-no-documents","SearchItemName",that.addCloseClass);
					    that.renderResult(that.videocol, letters, "#div-sub-result-video", "#spn-no-videos","SearchItemName",that.addCloseClass);

						that.toggleResult("show");
						break;
				}
		
			
		},

		editShare: function(event) {
			event.stopPropagation();
			var elem = event.currentTarget;
			var that = this;
			var FileID = $(elem).attr('name');
			if(!$(elem).hasClass('shared')) {
				fnInsertShare(FileID,function(){
					$(elem).addClass('shared');
					$(elem).html('<i class="fa fa-times-circle-o fa-2x"></i>');
					that.shareCount();
				});
			}
			else {
				fnRemoveShare(FileID,function(){
					$(elem).removeClass('shared');
					$(elem).html('<i class="fa fa-share-alt-square fa-2x"></i>');
					that.shareCount();
				});
			}
		},

		shareCount: function() {
			var that = this;
			fnGetShareCount(function(count) {
 				that.$("#spn-share-count").html("&nbsp;" + count + "&nbsp;");
			});
		},

		showShareView: function() {
			var count_val = $("#spn-share-count").text();
 				if(count_val > 0){
 					this.openItemDetail();
 					$('#btn-search-share').attr('href', "#share");
 					$("#btn-back").show();
				}
			},
        
        //popup with share and ship options when click cart icon
        showCollateralSharePopup: function(e){
            var that = this;
            if($(e.currentTarget).find("#spn-share-count").text().trim() != "0"){
                showPopup();
                $("#div-crm-overlay-content-container").addClass("collateral-shipSharePopup");
                $("#div-crm-overlay-content-container .modal-title").text(lblNm("msg_mcollateral_share_ship"));
                $("#div-crm-overlay-content-container .modal-footer").addClass("hide");
                $("#div-crm-overlay-content-container .modal-body").html(this.template_Collateral_Popup());
                $("#div-collateral-share").on("click",function(){
                    that.openItemDetail();
                    window.location.href = "#share";
                    $("#div-crm-overlay-content-container .close").trigger("click");
                    $("#btn-back").show();
                });
                $("#div-collateral-ship").on("click",function(){
                    fnGetSharedDocuments(function(data){
                        if(data.length > 0){
                            GM.Global.DocShipFl = "Y";
                            window.location.href = "#inv";
                            $("#btn-back").show();
                        }
                        else{
                            fnShowStatus(lblNm("msg_choose_docs_info"));
                        }
                        $("#div-crm-overlay-content-container .close").trigger("click");
                    });
                });
                $("#div-crm-overlay-content-container .close").on("click",function(){
                    $("#div-crm-overlay-content-container").removeClass("collateral-shipSharePopup");
                });
            }
        },
		
		
        /* Render Search Result in the Catalog Main View */
		searchresult: function(e) {
			e.preventDefault();
			mcBreadcrumb =[];
			var num = $(e.currentTarget).find('span').text().replace('&nbsp;','');
			var tabName = $(e.currentTarget).html().split('<')[0];
			var tabval = num + tabName;

        	mcBreadcrumb.push("<a href='#oldCollateral'>"+tabName+"</a>");
            mcRecentBreadCrumbs.push(mcBreadcrumb.slice(0));
            window.history.pushState("","","#oldCollateral");

        	$("body").removeClass("background-1").addClass("background-2 background-image");

			if(parseInt(num)!=1) {
				if(tabName == "System"){
				tabval += "s";
			}
				
			}
			
			tabval += ">" + $(e.currentTarget).html().split('<')[0];
			
			 showMsgView("021", tabval, GM.Global.Mobile);
            
            this.$(".div-sub-result").hide();
   
            	switch (e.currentTarget.id) {
            		case "btn-system": 
						this.$("#div-sub-result-system").show();
						break;
		            case "btn-document": 
						this.$("#div-sub-result-document").show();
						break;
					case "btn-image":
						this.$("#div-sub-result-image").show();
						break;
					case "btn-video": 
						this.$("#div-sub-result-video").show();
						break;	
            	}
            	
    			//$(".btn-collateral-item").hide();
				$(".btn-search-item").show();
    			$("#btn-system").show();
           

    		
			$('.selectedTab').removeClass("selectedTab");
			$(e.currentTarget).addClass("selectedTab");
		},
		
		// Set the search criteria
		setFilter: function(e) {
			var x = e.currentTarget;
			var id = x.id;
			var strFilter = $(e.target).text();
            
			 showMsgView("004", strFilter, GM.Global.Mobile);
            
			localStorage.setItem('filter', id);
			localStorage.setItem('filtername', strFilter);
			this.filters(strFilter);
			this.hideFilterPanel();
			if(this.$("#txt-keyword").val().length >= 3) {
				this.toggleResult("show");
			}
		},

		// Display/Hide the X button when at least one character is typed in the keyword
		setX: function(wordLength) {
			if(wordLength == 0) {
				$("#div-search-x").hide();
			}
			else {
				$("#div-search-x").show();
				}
		},

		showSidePanel: function(name, id) {
                 
                  this.sidepanelview.showSidePanel(name, id);
                  $(".btn-selected").removeClass("btn-selected");
                  this.$(".btn-bg-blue").each(function(){
                        if($(this).text() == name)
                              $(this).addClass("btn-selected");
                  })
                  if(this.$(".btn-quicklink").hasClass('show')) {
                        this.$(".panel").animate({
                              left: "-=301"
                        },100, function() {
                        });
                        this.$(".btn-quicklink").removeClass('show').addClass('ctlg-hide');
                  }
                  this.$("#div-sidepanel").show();
        },
 
		
		// Show Filter Panel
		showFilterPanel: function(e) {
			
			var currentval = $(e.currentTarget).text().trim();
		
			if(this.$("#div-arrow-keyword").hasClass("arrow-right")) {
                this.$("#div-arrow-keyword").removeClass("arrow-right").addClass("arrow-down");    
            }
            else {
                this.$("#div-arrow-keyword").removeClass("arrow-down").addClass("arrow-right");       
            }
			this.$("#div-filter-list").toggle();
			
			//hide selected item on filter panel
			var strCurrentFilter = $("#div-filter").text().trim();
			$('.itm-filter').each(function(){
				var strFilterall = $(this).text().trim();
				if(strFilterall == strCurrentFilter ){
					$(this).hide();
				}
			});
		},
		
		/* Instantiate ItemListView with following arguments
		 * 
		 * controlID: The type of data 
		 * collection: Collection data from appropriate data source depending on the controlID selected
		 * template: Template used to render the results
		 * columnheader: 0 - Hide Columns and Search Textbox / 1 - Hide Columns / 2 - Hide Search Textbox / 3 - Show all 
		 * renderType: 1 - Unlimited Scroll / 2 - Collapsible
		 */
		showItemListView: function(controlID, data, itemtemplate, columnheader, renderType, callback, sortoptions, template_URL) {
			var items = new ItemCollection(data);
			this.itemlistview = new ItemListView(
					{ 
						el: controlID, 
						collection: items,
						columnheader: columnheader, 
						itemtemplate: itemtemplate,
						renderType: renderType,
						sortoptions: sortoptions,
						template_URL: template_URL
					});

			if(callback!=undefined) 
				callback(controlID);

				$(".div-sub-result").each(function() {
								if($(this).find("#div-itemlist").text().trim()=="") {
								$(this).find("#div-itemlist").html("<li>"+lblNm("no_results_found")+"</li>");
								}
							});
			
			
		},

		toggleResult: function(option) {
			var filter = localStorage.getItem('filter');
			
			if(option=="show") {
			
				$("#div-search-result").show();
				$("#div-catalog-quicklink").hide();
					
				$(".btn-collateral-item").hide();
				$(".btn-catalog-item").hide();

				if(filter == "103261") {
					$("#btn-document").show();
					$("#btn-document").trigger("click");
				}
				else if(filter == "4000460") {
					$(".btn-collateral-item").show();
					$("#btn-system").trigger("click");
				}
				else if(filter == "103262") {
					$("#btn-image").show();
					$("#btn-image").trigger("click");
				}
				else if(filter == "103263") {
					$("#btn-video").show();
					$("#btn-video").trigger("click");
				}
	
				else if(filter == "103260") {
					$(".btn-catalog-item").show();
					$("#btn-system").trigger("click");
				}
				

				 $("#btn-system").hide();

				if((filter == "4000460") || (filter == "103260")){
					$("#btn-system").show();
					$("#btn-system").trigger("click");
				}
				else {
					$("#btn-system").hide();
				}
			}	
			else {
				$("#div-search-result").hide();
				$("#div-search-result").html("");
				$("#div-catalog-quicklink").show();
			}
		},
        
        hideContents: function (){
            if(GM.Global.GuideFl == "Y") {
                $("#btn-back").hide();
                $("#div-catalog-filter").addClass("gmVisibilityHidden");
                $("#div-catalog-quicklink").addClass("gmVisibilityHidden");
                $("#div-favorite-icon").addClass("gmVisibilityHidden");
                $("#li-detail-name").addClass("hide");
            }else {
                
                $("#div-catalog-filter").removeClass("gmVisibilityHidden");
                $("#div-catalog-quicklink").removeClass("gmVisibilityHidden");
                $("#div-favorite-icon").removeClass("gmVisibilityHidden");
                $("#li-detail-name").removeClass("hide");
            }
        },
		
        // Infinite Scroll
		unlimitedScrollParts: function() {
			var that = this;
			var appendData = [];
			for(var i=that.partadded;i<(that.partadded+10);i++) {
				if(that.partdata[i]!=undefined) {
					appendData.push(that.partdata[i]);
				}
			}
			that.partadded += 10;
			this.showItemListView("#div-sub-result-part", appendData, "GmItem", 0, 1, this.addCloseClass, undefined, URL_Common_Template);
		},


	});
	return CollateralView;
});
