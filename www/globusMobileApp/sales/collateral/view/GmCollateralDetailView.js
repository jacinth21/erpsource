/**********************************************************************************
 * File:        GmCollateralDetailView.js
 * Description: View to display the details of a System, loads after the Collateral View
 * Version:     1.0
 * Author:      asivaraj
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone', 'handlebars',
        'global', 'notification', 'daomarketingcollateral','daosetup',
        'itemmodel', 'itemcollection', 'itemlistview', 'mmcimageview', 'imageviewerview','threesixtyview', 'filesync', 'documentview'
        ],

        function ($, _, Backbone, Handlebars, Global, Notification, DAOMarketingCollateral, DAOSetup, Item, Items, ItemListView, ImageView, ImageViewerView,ThreeSixtyView, FileSync, DocumentView) {

	'use strict';
	var url="collateral";
	var intItemCount= 0;
	var ItemDetailView = Backbone.View.extend({

		events : {

			"click #btn-overview" : "display",
			"click #btn-download" : "download",
			"click #div-favorite-icon" : "processFavorite",
			"click #btn-search, .btn-itemlist" : "close",
			"click .btn-detail" : "deleteBreadCrumb",
			"click .detail-image, .imagelist-image, .img-in-camera-icon" : "openImageViewer",
			"click .div-detail-set-group-part a" : "setBreadCrumb",
			// "click #div-detail-menu": "showNavigation",
			"click #btn-camera": "triggerOpenViewer",
			"click .li-doc-name" : "openPDFViewer",
			"click .li-document-share, .div-icon-share " : "addShare",
			"click #btn-link" : "openLink",
			"click .btn-app-link div, .btn-app-link a" : "changeApp",
			"click .li-video-name" : "displayVideo",
            "click #btn-documents" : "showDocPage",
            "click #btn-share": "showCollateralSharePopup"
		},

		template: fnGetTemplate(URL_Collateral_Template,"GmCollateralItemDetail"),

		template_share: fnGetTemplate(URL_Collateral_Template,"GmShare"),
		
		template_header: fnGetTemplate(URL_Collateral_Template,"GmCollateralItemHeader"),
		
		template_header_image: fnGetTemplate(URL_Collateral_Template,"GmCollateralItemHeaderImage"),
        
		template_Collateral_Popup : fnGetTemplate(URL_Collateral_Template,"GmCollateralSharePopup"),

		initialize: function(itemInfo) {
			
			$("body").removeClass("background-1").removeClass("background-2").removeClass("background-image");
            // $("#div-header").removeClass("background-2").addClass("background-1 background-image");
			$("#div-app-title").html(lblNm("marketing_collateral"));
			$("#div-app-title").attr('title',"Marketing Collateral");
            
            if(GM.Global.GuideFl == "Y")
			     $("#div-app-title").html(lblNm("reimbursement_guide"));
            
			var that = this;
			this.strItemID = itemInfo[0];
			this.strItemType = itemInfo[1];
			this.strViewType = itemInfo[2];
			this.strDocID = itemInfo[3];
			itemInfo.pop();
			if(!this.strViewType) {
				this.strViewType = "overview";
			}
			
			this.confirmDownload = true;
			
			// Show Back and Home buttons
			$("#btn-back").show();
			$("#btn-home").show();
			// Hide Locale button
			$(".div-btn-locale").hide();
			$("#div-detail-navigation").addClass('hide');

			this.$el.attr("id","div-detail-page");
			itemInfo.push(this);

			intItemCount = 0;
			switch(this.strItemType) {
			

			case "system":
				fnGetSystemDetail(this.strItemID,function(data) {
					var Model = new Item(data);

					if(that.strViewType == "overview") {
						that.detailContent = data.Detail;
						that.descContent = data.Desc;
					}

					that.render(Model,itemInfo);
					that.$("#div-detail-overview").addClass("detail-iphone-scroll");
					that.$("#li-detail-image").addClass("li-detail-image-iphone");
					that.$("#div-detail-view").addClass("div-detail-overview-height"); // height of overview won't affect other places

					if(that.strViewType) {
						switch(that.strViewType) {
						
						case "documents":
							 that.showItemByDoc('103114,4000441');
							break;
						case "images":
							 that.showItemByImage('103112');
							break;
						case "videos":
							 that.showItemByVideo('103113');
							break;
						}	
					}
				});
				break;
			
			case "share":
				// var Model = new Item();
				this.$el.html(this.template());
				this.$("#div-detail").html(this.template_share());
			}

			fnInsertRecent(this.strItemType,this.strItemID);

			fnGetLocalFileSystemPath(function(path) {
                that.localpath = path;
            });
		},

		// Render the initial contents when the app starts
		render: function (data,itemInfo) {
            var that = this;
    		this.$el.html(this.template(data.toJSON()));
			this.$("#btn-overview").attr("href", "#collateral/detail/system/" + this.strItemID + "/overview");
            
//            PMT-38838 :: Marketing Collateral Documents Not Opening Android Devices
            if (devicePlatform != "Android") {
			    this.$("#btn-documents").attr("href", "#collateral/detail/system/" + this.strItemID + "/documents");
			}
            
			this.$("#btn-images").attr("href", "#collateral/detail/system/" + this.strItemID + "/images");
			this.$("#btn-videos").attr("href", "#collateral/detail/system/" + this.strItemID + "/videos");
            this.hideContents();
            
			if(this.imageview) {
				this.imageview.close();
			}
			
			switch (this.strViewType) {
        		case "overview": 
					this.$("#div-detail-overview").show();
					this.$("#div-detail-documents").hide();
					this.$("#div-detail-images").hide();
					this.$("#div-detail-videos").hide();
					this.imageview = new ImageView(itemInfo);
					break;
	            case "images": 
					this.$("#div-detail-overview").hide();
					this.$("#div-detail-documents").hide();
					this.$("#div-detail-videos").hide();
					this.imageview = new ImageView(itemInfo);
					break;
				case "documents":
					this.$("#div-detail-overview").hide();
					this.$("#div-detail-images").hide();
					this.$("#div-detail-videos").hide();
					break;
				case "videos": 
					this.$("#div-detail-overview").hide();
					this.$("#div-detail-documents").hide();
					this.$("#div-detail-images").hide();
					break;	
            }

            
      		this.data = data;
			this.$("#div-detail-breadcrumb").html(mcBreadcrumb.join(" > "));
			this.$("#div-detail-breadcrumb").append(" >");
			mcRecentBreadCrumbs.push(mcBreadcrumb.slice(0));
			
			this.hideButtons();
			var title = $("#div-app-title").attr('title');
			
			if( title == "Marketing Collateral") {

				this.$("#btn-share").removeClass("btn-share-hide").addClass("btn-share-show");
				this.$("#div-detail-navigation").addClass("div-detail-collateral-navigation"); 
			}
			
			this.setShareCount();
			return this;
		},
        
        //popup with share and ship options when click cart icon
        showCollateralSharePopup: function(e){
            var that = this;
            if($(e.currentTarget).find("#spn-share-count").text().trim() != "0"){
                showPopup();
                $("#div-crm-overlay-content-container").addClass("collateral-shipSharePopup");
                $("#div-crm-overlay-content-container .modal-title").text(lblNm("msg_mcollateral_share_ship"));
                $("#div-crm-overlay-content-container .modal-footer").addClass("hide");
                $("#div-crm-overlay-content-container .modal-body").html(this.template_Collateral_Popup());
                $("#div-collateral-share").on("click",function(){
                    window.location.href = "#share";
                    $("#div-crm-overlay-content-container .close").trigger("click");
                });
                $("#div-collateral-ship").on("click",function(){
                    fnGetSharedDocuments(function(data){
                        if(data.length > 0){
                            GM.Global.DocShipFl = "Y";
                            window.location.href = "#inv";
                        }
                        else{
                            fnShowStatus(lblNm("msg_choose_docs_info"));
                        }
                        $("#div-crm-overlay-content-container .close").trigger("click");
                    });
                });
                $("#div-crm-overlay-content-container .close").on("click",function(){
                    $("#div-crm-overlay-content-container").removeClass("collateral-shipSharePopup");
                });
            }
        },

		
		openLink: function() {
			$("#btn-link").each(function(){	
	            	$(this).toggleClass("show-app");
			})
				$(".btn-app-link").toggle("fast");
		},
		
		changeApp: function(event) { 
			if($(event.currentTarget).hasClass('in-app')) {
				window.location.href = '#sharestatus';
			}
			else{
				pcBreadcrumb = [];
				pcBreadcrumb.push("<a href='"+ document.URL + "'>Links</a>");
				pcRecentBreadCrumbs.push(pcBreadcrumb.slice(0));
				var url = "#catalog/detail/system/"+this.strItemID ;
				window.location.href = url;
			}	
		},

		// Check for the Favorite icon
		chkFavorite: function() {
			var that = this;
			fnChkFavorite(this.strItemID,this.strItemType,function(status) {
				if(status=="success") {
					that.$("#div-favorite-icon").addClass("favorite-add");
				}
				else {
					that.$("#div-favorite-icon").addClass("favorite-remove");
				}
			});
		},

		// Toggles the Details
		display: function(key) {

			if(key.currentTarget==undefined) {
				this.key = key;
			}
			else {
				this.key = "overview";
				this.$("#btn-download").css("visibility","visible");
			}
			this.$(".btn-detail").each(function() {
				$(this).removeClass("btn-selected");
			});

			this.$("#div-detail-" + this.key).show();
			this.$("#btn-" + this.key).addClass("btn-selected");

		},

		// Triggers DownloadImages Function of ImageView
		download: function(event) {
			this.imageview.downloadImages();
		},

		// Hide and Show Necessary buttons
		hideButtons: function() {
			var that = this;
			switch(that.strItemType) {

			case "system":
				that.$("#btn-part").hide();
				that.$("#div-favorite-icon").show();
				that.chkFavorite();
				break;
			}

			if(!that.detailContent) {
				that.$("#li-detail-detail").hide();
			}

			if(!that.descContent) {
				that.$("#li-detail-description").hide();
			}
		},

		openImageViewer: function(event) {
			var elem = event.currentTarget;
			var that = this;
			if($(elem).hasClass("detail-image")) {
				if(!$(event.currentTarget).hasClass("img-missing")) {
					this.imageViewerView = new ImageViewerView({source: this.$("#div-detail-image-container"), ImageId : $(elem).attr('id'), callback: function  () {
						this.openViewer(event);
					}});
					this.imageViewerView.openViewer(event);
				}
			}
			else if($(elem).hasClass("imagelist-image")) {
				if(!$(event.currentTarget).hasClass("img-missing")) {
					this.imageViewerView = new ImageViewerView({source: this.$("#div-detail-images"), ImageId : $(elem).attr('id'), callback: function  () {
						this.openViewer(event);
					}});
					this.imageViewerView.openViewer(event);
				}
			}
			else {
				this.imageViewerView = new ImageViewerView({source: $(elem).parent(), ImageId : $(elem).attr('id'), callback: function () {
					that.imageViewerView.openViewer(event);
				}});
				this.imageViewerView.openViewer(event);
			}
		},

		// Add or Reomve the Current System as Favorite
		processFavorite: function(event) {
			var icon = $(event.currentTarget);
			var iconClass = $(icon).attr("class");
			var that = this;
			if(iconClass.indexOf("favorite-remove")>-1) {
				fnInsertFavorite(this.strItemID,this.strItemType,function(status) {
					if(status == "success") {
						that.$("#div-favorite-icon").removeClass("favorite-remove").addClass("favorite-add");
					}
				});
			}
			else {
				fnRemoveFavorite(this.strItemID,function(status) {
					if(status == "success") {
						that.$("#div-favorite-icon").removeClass("favorite-add").addClass("favorite-remove");
					}
				});
			}
		},

		//Sets the BreadCrumb
		setBreadCrumb: function(e) {
			e.preventDefault();
			var name = this.data.get("Name");
			var id = this.data.get("ID");
			var bc = mcBreadcrumb.length+1; 
			if(e.currentTarget.href) {
				mcBreadcrumb.push("<a id="+ bc +" href='#collateral/detail/" + this.strItemType + "/" + id + "'>" + name + "</a>");
				var showType = "";
				if(this.strViewType == "overview"){
						mcBreadcrumb.push("<a id="+ bc+" href='#collateral/detail/" + this.strItemType + "/" + id + "/" + this.strViewType + "'>PARTS</a>");
						window.location.href = e.currentTarget.href;
				}
				else {
					mcBreadcrumb.push("<a id="+ bc+" href='#collateral/detail/" + this.strItemType + "/" + id + "/" + this.strViewType + "'>" +this.strViewType + "</a>");
					window.location.href = e.currentTarget.href;
				}
			}
		},

		deleteBreadCrumb: function(e){
			e.preventDefault();
			if(e.currentTarget.href) {
				var changeLink = e.currentTarget.href;
				var chkSame = "";
				if(e.currentTarget.id!="btn-overview"){
					 chkSame = e.currentTarget.id + "s";
				}
				else{
					chkSame = e.currentTarget.id;
				}
				if(chkSame != ("btn-" + this.strViewType)){
					window.location.href = changeLink;
				}
			}
		},

		showItemListView: function(controlID, data, itemtemplate, columnheader, sortoptions, template_URL,callbackOnReRender) {
			var items = new Items(data);	
			this.itemlistview = new ItemListView(
					{ 
						el: controlID, 
						collection: items,
						columnheader: columnheader, 
						itemtemplate: itemtemplate,
						sortoptions: sortoptions,
						template_URL: template_URL,
						callbackOnReRender: callbackOnReRender
					}
			);
			
			if(callbackOnReRender != "undefined") {
				callbackOnReRender();
			}
		},
				
		showItemByVideo: function(itemID) {
			var that = this;
			if(this.itemlistview) {
				that.itemlistview.close();
			}
			
			fnGetItemHeaders( itemID,function(rsHeader) {
				
				if(rsHeader.length == 0){
					that.$("#div-detail-videos").html("<li class='li-no-display'>"+lblNm("videos_not_available")+"</li>");
				}
				else {
					that.$("#div-detail-videos").html(that.template_header(rsHeader));
					that.callshowItemListViewForVideo(0,rsHeader);
				}
			});
		
			this.display("videos");	
		},
		
		
		showItemByDoc: function(itemID) {
			var that = this;
	
			if(this.itemlistview) {
				that.itemlistview.close();
			}
			fnGetItemHeaders( itemID,function(rsHeader) {
				
				if(rsHeader.length == 0){
					that.$("#div-detail-documents").html("<li class='li-no-display'>"+lblNm("documents_not_available")+"</li>");
				}
				else {
					that.$("#div-detail-documents").html(that.template_header(rsHeader));
					that.callshowItemListViewForDoc(0,rsHeader);
				}
			});
		
			this.display("documents");	
		},
		
		showItemByImage: function(itemID) {
			var that = this;
			if(this.itemlistview) {
				that.itemlistview.close();
			}
			
			fnGetItemHeaders( itemID,function(rsHeader) {
				
				if(rsHeader.length == 0){
					that.$("#div-detail-images").html("<li class='li-no-display'>"+lblNm("images_not_available")+"</li>");
				}
				else {
						
						that.$("#div-detail-images").html(that.template_header_image(rsHeader));

						that.callshowItemListViewForImage(0,rsHeader);
					
				}
			});
		
			this.display("images");	
		},
		
		callshowItemListViewForImage: function(iValue,rsHeader){
			var that = this;
			var intHeaderID = rsHeader[iValue].ID;
			var strHeaderName = rsHeader[iValue].Name;

			fnGetFilesBySubType(that.strItemID, intHeaderID, function(rsHeaderContents){

				intItemCount = intItemCount+rsHeaderContents.length;

				if(rsHeaderContents.length > 0){

					$("#div-subtype-"+intHeaderID).find('li').html(strHeaderName);
					$("#div-detail-images").find("#div-subtype-"+intHeaderID).show();

					that.imageview.displayAllImages(iValue,rsHeader,rsHeaderContents,function(){

						if(iValue < (rsHeader.length)-1)
							that.callshowItemListViewForImage(iValue+1,rsHeader);
		
					});	
	
				}
				else {
										
					if(iValue < (rsHeader.length)-1)
						that.callshowItemListViewForImage(iValue+1,rsHeader);
					
					else {

							if(intItemCount == 0)
							$("#div-detail-images").html("<li class='li-no-display'>"+lblNm("images_not_available")+"</li>");

						}
				}


			});	
			
		},


		callshowItemListViewForDoc: function(iValue,rsHeader){
			var that = this;
			var intHeaderID = rsHeader[iValue].ID;
			fnGetFilesBySubType(that.strItemID, intHeaderID, function(rsHeaderContents){
				
				intItemCount = intItemCount+rsHeaderContents.length;
	
				if(rsHeaderContents.length > 0){
					var sortoptions = [{ "DefaultSort" : "Name", "Elements" : [{"ID" : "Name","DispName" : rsHeader[iValue].Name,"ClassName":"li-document-name"}]}];
					that.showItemListView($("#li-subtype-" + rsHeader[iValue].ID) , rsHeaderContents, "GmDocuments", 2, sortoptions, URL_Collateral_Template,that.reRenderedItemList);
					
					$("#div-itemlist li").each(function(){
						if($(this).find('#ul-listitem li').attr('id')== that.strDocID ){
							$(this).find('#ul-listitem li').addClass('li-selected-row');
						}
					});
				}
				
				if(iValue < (rsHeader.length)-1)
					that.callshowItemListViewForDoc(iValue+1,rsHeader);

				else {
					if(intItemCount == 0)
						$("#div-detail-documents").html("<li class='li-no-display'>"+lblNm("documents_not_available")+"</li>");
				}
			});	
            
            this.hideContents();
		},
		
		callshowItemListViewForVideo: function(iValue,rsHeader){
			var that = this;
			var intHeaderID = rsHeader[iValue].ID;

			fnGetFilesBySubType(that.strItemID, intHeaderID, function(rsHeaderContents){

				intItemCount = intItemCount+rsHeaderContents.length;
				
				if(rsHeaderContents.length > 0){
					var sortoptions = [{ "DefaultSort" : "Name", "Elements" : [{"ID" : "Name","DispName" : rsHeader[iValue].Name,"ClassName":"li-document-name"}]}];
					that.showItemListView($("#li-subtype-" + rsHeader[iValue].ID) , rsHeaderContents, "GmVideos", 2, sortoptions, URL_Collateral_Template,that.reRenderedItemList);
					
					$("#div-itemlist li").each(function(){
						if($(this).find('#ul-listitem li').attr('id')== that.strDocID ){
							$(this).find('#ul-listitem li').addClass('li-selected-row');
						}
					});

				}

				if(iValue < (rsHeader.length)-1)
					that.callshowItemListViewForVideo(iValue+1,rsHeader);

				else {
					if(intItemCount == 0)
						$("#div-detail-videos").html("<li class='li-no-display'>"+lblNm("videos_not_available")+"</li>");
				}
	
			});	
		},


		openPDFViewer: function(event) {
            try {
                event.preventDefault();
                if (!$(event.currentTarget).hasClass('li-video-name')) {
                    var docPath = $(event.currentTarget).attr('name'),
                        docId = $(event.currentTarget).attr('id');

                    var view = new DocumentView({'event': event, parent: this, 'path': docPath, 'docId': docId }); 
                }
            } catch (err) {
                showAppError(err, "GmCollateralDetailView:openPDFViewer()");
            }

			// if(!$(event.currentTarget).hasClass('li-video-name'))
			// 	var view = new DocumentView({'event':event,parent:this});
			// var className = event.target.className, docName = $(event.currentTarget).attr('name'), docId = $(event.currentTarget).attr('id');
			// if((window.top.location.hash).indexOf('share')<0) {
			// 	var path = $(event.currentTarget).attr('path');
			// 	fnResolvePath(path, function(status,devicepath) {
			// 		if(status) {
			// 			var ref = window.open(decodeURI(devicepath), '_blank', 'location=no');
			// 		}
			// 		else {
						
			// 			fnDownload(URL_FileServer+path, decodeURI(devicepath+path), function(percent) {
			// 				$('#spn-prg-' + docId).show();
			// 				$('#spn-prg-' + docId).find('progress').val(percent*100);
			// 			},function () {
			// 				$('#spn-prg-' + docId).hide();
			// 				var ref = window.open(decodeURI(devicepath)+path, '_blank', 'location=no');
			// 				ref.addEventListener('loaderror', function(event) { alert('error: ' + event.message); console.log(decodeURI(devicepath+path)) });
			// 			});
			// 		}			
			// 	});
				
			// }
		},

		displayVideo: function(event) {
			event.preventDefault();
			//var view = new DocumentView(event);
			var className = event.target.className, path = $(event.currentTarget).attr('name'), docID = $(event.currentTarget).attr('id');
			var that = this;
			var fileServer = setFileServer();
			if((window.top.location.hash).indexOf('share')<0) {
				fnResolvePath(path, function(status,devicepath) {
					if(status) {
						var ref = window.open(decodeURI(devicepath), '_blank', 'location=no');
					}
					else if(navigator.onLine){
						var connectionType = (navigator.connection.type).toLowerCase();
						if((connectionType.indexOf('cellular')>=0) && that.confirmDownload) {
							showNativeConfirm(lblNm("msg_device_connected_dataplan"),lblNm("msg_warning"),[lblNm("msg_yes"),lblNm("msg_do_not_ask"),lblNm("msg_no")], function(btnIndex) {
								if(btnIndex==1) 
									that.downloadVideo(fileServer, devicepath, path, docID);
								else if(btnIndex==2) {
									that.confirmDownload = false;
									that.downloadVideo(fileServer, devicepath, path, docID);
								}
							})
						}
						else
							that.downloadVideo(fileServer, devicepath, path, docID);
					}
                    else{
                        showMsgView("056");
                    }
				});
				
			}
		},
		
		downloadVideo: function(fileServer, devicepath, path, docID) {
			fnDownload(fileServer+path, devicepath+path, function(percent) {
				$('#spn-prg-' + docID).show();
				$('#spn-prg-' + docID).find('progress').val(percent*100);
			},function () {
				$('#spn-prg-' + docID).hide();
				var ref = window.open(decodeURI(devicepath)+path, '_blank', 'location=no');
			});
		},

		addShare: function(event) {
			event.preventDefault();
			var elem = event.currentTarget;
			var that = this;
			var FileID = $(elem).attr('name');
			if(!$(elem).hasClass('shared')) {
				fnInsertShare(FileID,function(){
					$(elem).addClass('shared');
					$(elem).html('<i class="fa fa-times-circle-o fa-2x"></i>');
					that.setShareCount();
				});
			}
			else {
				fnRemoveShare(FileID,function(){
					$(elem).removeClass('shared');
					$(elem).html('<i class="fa fa-share-alt-square fa-2x"></i>');
					that.setShareCount();
				});
			}
		},


		setShareCount: function() {
			var that = this;
			fnGetShareCount(function(count) {

 				that.$("#spn-share-count").html("&nbsp;" + count + "&nbsp;");
				var count_val = that.$("#spn-share-count").text().trim();
                //Below lines commented for should not link to share page directly(PC-2428)
// 				if(count > 0){

// 					$('#btn-share').attr('href', "#share");
//				}
//				else{
//					$('#btn-share').removeAttr('href');
//				}

			});
		},

		// show navigation buttons for phone layout
		showNavigation: function(){			
			$("#div-detail-menu").each(function(){
	            if($(this).hasClass('detail-menu'))	
	            	$(this).removeClass("detail-menu");
	            else
	    	        $("#div-detail-menu").addClass("detail-menu");
	            })
			
			$("#div-detail-navigation" ).slideToggle("fast");
		},
		
		// trigger open viewer on click camera icon for cellphone
		triggerOpenViewer: function(){
			$(".detail-image").first().trigger("click");
		},
		
		reRenderedItemList: function(currList) {
			
			$(".li-document-share").each(function() {
				var elem = this;
					fnCheckDocuments($(this).attr('name'),function(status){
					if(status=="success") {
						$(elem).addClass('shared');
						$(elem).html('<i class="fa fa-times-circle-o fa-2x"></i>');
					}
					else {
						$(elem).html('<i class="fa fa-share-alt-square fa-2x"></i>');
					}
				})
			});
			
			$(".check-shareable-N").find(".li-document-share").hide();
			$(".check-shareable-NN").find(".li-document-share").hide();
			$(".check-shareable-YN").find(".li-document-share").hide();

			
		},
		
        // Hide div contents based on Mc and Guide module
		hideContents: function (){
            if(GM.Global.GuideFl == "Y") {
                $("#btn-back").hide();
                $("#div-catalog-filter").addClass("gmVisibilityHidden");
                $("#div-catalog-quicklink").addClass("gmVisibilityHidden");
                $("#div-favorite-icon").addClass("gmVisibilityHidden");
                $("#li-detail-name").addClass("gmVisibilityHidden");
                $("#btn-share").removeClass("btn-share-hide").addClass("btn-share-show");
                $("#div-detail-breadcrumb").addClass("hide");
                $("#div-detail-navigation").addClass("hide");
                $(".li-subtype-items #div-column").addClass("hide");
                $(".li-subtype-items div>li").addClass("hide");
                $('.li-subtype-items .li-doc-'+localStorage.getItem("rimbguide")).parent().removeClass("hide");
                $(".div-detail #div-itemlist").css("border", "0px solid #ccc");
                
            }else {
                $("#btn-back").show();
                $("#div-catalog-filter").removeClass("gmVisibilityHidden");
                $("#div-catalog-quicklink").removeClass("gmVisibilityHidden");
                $("#div-favorite-icon").removeClass("gmVisibilityHidden");
                $("#li-detail-name").removeClass("gmVisibilityHidden");
                $("#div-detail-breadcrumb").removeClass("hide");
                $("#div-detail-navigation").removeClass("hide");
                $(".li-subtype-items #div-column").removeClass("hide");
                $(".li-subtype-items div>li").removeClass("hide");
                $('.li-subtype-items .li-doc-'+localStorage.getItem("rimbguide")).parent().removeClass("hide");
                $(".div-detail #div-itemlist").css("border", "1px solid #ccc");
            }
        },
        
        // Function used to show the alert message when click on document tab - only for Android devices
        showDocPage: function () {
            if (devicePlatform == "Android") {
                showNativeAlert(lblNm("msg_doc_tab_not_available"), lblNm("msg_not_available"));
            }
        },
        
		//Closes the View
		close: function(event) {
			this.unbind();
			$("#btn-back").unbind();
			this.remove();
		}

	});

	return ItemDetailView;
});
