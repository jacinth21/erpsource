define([
        'jquery', 'underscore', 'backbone', 'handlebars',
        'global',
        'itemmodel','itemcollection', 'itemlistview', 'contactsview'
        ], function ($, _, Backbone, Handlebars, Global, Itemmodel, ItemCol, ItemListView, ContactsView ) {

	'use strict';
	var arrMain = new Array(), dateSelected = false;
	var ShareStatusView = Backbone.View.extend({

		events : {
			"keyup #input-sharestatus-search" : "searchFromEmail",
			"click .li-document-share" : "removeFile",
			"click .fa-user" : "contactsviewer",
			"keyup #txt-to-email" : "viewToEmailList",
			"click .li-local-contacts": "addContactTo",
			"click .spn-spandiv-close": "removeContactTo",
			"click .div-child-text" : "editContactTo",
			"click #div-contact-close": "closeContactList",
			"click .ul-list-item, #btn-get-status": "sendDataToServer",
			"click #div-date-range" : "selectDateRange",
			"change #input-sharestatus-todate, #input-sharestatus-fromdate" : "updateResultSet"
		},
	
		template: fnGetTemplate(URL_Collateral_Template, "GmShareStatusMain"),
		template_multicontacts: fnGetTemplate(URL_Collateral_Template, "GmCollateralMultiContacts"),
		template_devicecontacts: fnGetTemplate(URL_Collateral_Template, "GmDeviceContacts"),
		template_contacts : fnGetTemplate(URL_Collateral_Template, "GmContacts"),
		template_status : fnGetTemplate(URL_Collateral_Template, "GmStatus"),


		initialize: function() {
//			$("#div-app-title").html("Marketing Collateral");
			$("#btn-back").show();
			this.render();
			this.currView = "searchresult";
		},

		render: function () {
			var that = this;
			this.$el.html(this.template());
			var currentDate = new Date().toISOString().substring(0, 10);
			this.$(".div-date-container").each(function  () {
				$(this).find('input').val(currentDate);
				that.today = $(this).val();
			});	
            
            if(GM.Global.GuideFl == "Y") {
                $("#div-catalog-filter").addClass("gmVisibilityHidden");
                $("#div-catalog-quicklink").addClass("gmVisibilityHidden"); 
            } else {
                $("#div-catalog-filter").removeClass("gmVisibilityHidden");
                $("#div-catalog-quicklink").removeClass("gmVisibilityHidden");
                $("#div-favorite-icon").removeClass("gmVisibilityHidden"); 
            }
			return this;
			
		},

		selectDateRange:function(event){
			var elem = $(event.currentTarget).find('i');
			
			
			if($(elem).hasClass('fa fa-square-o fa-lg')) {
				$(elem).removeClass("fa fa-square-o fa-lg");
				$(elem).addClass("fa fa-check-square-o fa-lg");
				dateSelected = true;
				$("#div-sharestatus-inner-container").addClass("ph-div-sharestatus-inner-container");
				$(".div-date-container").removeClass("div-date-container-hide").addClass("div-date-container-show");
			}
			else{
				$(elem).removeClass("fa fa-check-square-o fa-lg");
				$(elem).addClass("fa fa-square-o fa-lg");
				dateSelected = false;
				$("#div-sharestatus-inner-container").removeClass("ph-div-sharestatus-inner-container");
				$(".div-date-container").addClass("div-date-container-hide").removeClass("div-date-container-show");
			}

		},
	
		sendDataToServer: function (event) {

			var fromVal =  $("#input-sharestatus-fromdate").val();
			var toVal = $("#input-sharestatus-todate").val();
			var today = new Date().toISOString().substring(0, 10);
			var fromDate = fromVal;
			var toDate = toVal;


			var fromDay = fromVal.split('-')[2], fromMonth = fromVal.split('-')[1], fromYear = fromVal.split('-')[0];
			var thisDay = today.split('-')[2], thisMonth = today.split('-')[1], thisYear = today.split('-')[0];

			var monthDiff = Math.abs(thisDay-fromDay);

			if(dateSelected) {
				var boolDiff = false;
				if(fromYear >= (thisYear-1))
					boolDiff = true;
				else {
					if(monthDiff > 6)
						boolDiff = true;
					else if(monthDiff < 6) 
						boolDiff = false;
					else {
						if(!(fromDay >= thisDay))
							boolDiff = true;
					}

				}
			}
			
			

			if(!navigator.onLine) {
				console.log("No Connection")
				showNativeAlert(lblNm("msg_cannot_retrive_status"),lblNm("msg_connection_error"),lblNm("msg_ok"));
			}
			else if(intOfflineMode) 
				showNativeAlert(lblNm("msg_cannot_retrive_status_in_offline"),lblNm("msg_offline_mode_restriction"));
			else if((dateSelected) && (toVal < fromVal)) {
				console.log("to Date < From Date")
				showNativeAlert(lblNm("msg_select_date_greater_than_fromdate"),lblNm("msg_data_range_error"),lblNm("msg_ok"));
			}
			// else if(boolDiff) {
			// 	console.log("Select only before 6 months");
			// }
			else {
				if(typeof(event)=='object')
					var emailID = $(event.currentTarget).find(".li-item-email").find('a').text();
				else
					var emailID = event;

				this.emailID = emailID;
				var arrToDate = toVal.split('-');
				var arrFromDate = fromVal.split('-');
				var strToDate = arrToDate[1] + "/" + arrToDate[2] + "/" + arrToDate[0];
				var strFromDate = arrFromDate[1] + "/" + arrFromDate[2] + "/" + arrFromDate[0];
				var outputJSON = {"token" : localStorage.getItem('token'), "emailid" : emailID, "partytype" : "7000", "emailfromdate" : "", "emailtodate" : "" };
				var msgData = emailID;

				if(dateSelected) {
					outputJSON.emailfromdate = strFromDate;
					outputJSON.emailtodate = strToDate;

					msgData = this.emailID + " in the Selected Date Range";
					if(typeof(event)=='object') {
						if(event.currentTarget.id == "btn-get-status") {
							this.emailID = "";
							delete outputJSON.emailid;
							msgData = "the Date Range";
						}
					}
					else if(event == "") {
						this.emailID = "";
						delete outputJSON.emailid;
						msgData = "the Date Range";
					}

				}

				this.displayStatus(outputJSON, msgData,fromDate, toDate);

			}
			
		},


		displayStatus: function  (outputJSON, emailID,fromDate, toDate) {
			arrMain = [];
				var that = this;
				showMsgView("049");
				fnRetrieveStatus(outputJSON,fromDate, toDate,function (JSONdata) {
					console.log(JSONdata);
					showMsgView("050", emailID);
					that.currView = "status";
					that.$("#div-sharestatus-body").slideUp();
					that.$("#div-status-main").slideDown();
					that.$("#div-status-main").html(that.template_status(JSONdata));

					$(".ul-mc-status-artifacts-list").each(function  (argument) {
						if($(this).text().trim() == "") {
							$(this).parent().hide();
						}
					});
				});
		},

		
		searchFromEmail: function(e){
      		var that = this;
			var intWordLength;
//			showMsgView("011");
			this.$("#div-sharestatus-body").show();
			this.$("#div-status-main").hide();
			var strSearchData = $(e.currentTarget).val();
			var searchopt = $("#search-filter").val();
			strSearchData = strSearchData.trim();
			this.currView = "searchresult";

			if(strSearchData == undefined )
				intWordLength = strSearchData.length-1;
			else 
				intWordLength = strSearchData.length;

			this.setX(intWordLength);

			if((intWordLength > 0 )){
				fnSearchContact(strSearchData ,function(rsEmails) {
					if(rsEmails.length > 0) {
						var sortoptions = [{"DefaultSort" : "Name", "Elements" : [{"ID" : "Name" , "DispName" : "Contact Name","ClassName":"li-item-name"},{"ID" : "Email","DispName" : "E-Mail ID","ClassName":"li-item-email"}]}];
						that.showItemListView(that.$("#div-sharestatus-body"), rsEmails, "GmContactForStatus", 2, sortoptions, URL_Collateral_Template);
					}
					else {
						$("#div-sharestatus-body").html("<li>"+lblNm("msg_no_data_display")+"</li>");
					}
				});
			}
			else {
				if(strSearchData == undefined ) {
					this.$("#div-sharestatus-body").html("<li>"+lblNm("type_word_on_search")+"</li>");
				}
			}
		},

		updateResultSet: function() {
			if(this.currView=="status") {
				$("#input-sharestatus-search").focus();
				this.sendDataToServer(this.emailID);
			}
		},
		
		
		setX: function(intWordLength) {
			if(intWordLength == 0) {
				$("#div-email-search-x").hide();
			}
			else {
				$("#div-email-search-x").show();
			}
		},
		
		clearKeyword: function(e) {
			//showMsgView("011");
			$("#div-email-search-x").hide();
			this.$("#txt-email-search").val("");
			$("#div-email-list").empty();
			$("#span-initial-emailview").show();
		},
		

		showItemListView: function(controlID, data, itemtemplate, columnheader, sortoptions, template_URL) {
			var items = new ItemCol(data);	
			this.itemlistview = new ItemListView(
				{ 
					el: controlID, 
					collection: items,
					columnheader: columnheader, 
					itemtemplate: itemtemplate,
					sortoptions: sortoptions,
					template_URL: template_URL
				}
			);
		},

		
		close: function() {
			this.$el.undelegate();
			this.$el.unbind();
			//this.$el.remove();
		}


	});

	return ShareStatusView;
});
