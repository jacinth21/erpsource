
/**********************************************************************************
 * File:        GmImageView.js
 * Description: Contains the system images downloaded from server.
 *              The View has the functions related to Images in the Details page
 *              Loads after the Catalog View.
 * Version:     1.0
 * Author: 		praja 
 **********************************************************************************/


/*global define*/
define([
    'jquery', 'underscore', 'backbone', 'handlebars',
    'global', 'notification', 'daosetup',  
    'itemcollection', 'filesync'
],

function ($, _, Backbone, Handlebars, Global, Notification, DAOSetup, Items, FileSync) {

	'use strict';
	var images, localpath, imageData = [];
	var ImageView = Backbone.View.extend({

		template: fnGetTemplate(URL_Collateral_Template,"GmImages"),

		initialize: function(itemInfo) {
			var that = this;
 			this.strID = itemInfo[0];
			this.strItemType = itemInfo[1];
			this.strViewType = itemInfo[2];
			this.parentView = itemInfo[3];
			
			fnGetLocalFileSystemPath(function(path) {
                that.localpath = path;
                localpath = path;
            });
			
			var type = eval("img" + this.strItemType.toLowerCase());

			switch(this.strItemType) {
			
          
			case "system":
				fnGetImageof(type,this.strID,function(data) {
				if(data.length>0) {
					that.parentView.$("#div-no-image").html("");
					that.render(data);
				}
				else {
					that.parentView.$("#btn-camera").hide();
					that.parentView.$("#div-no-image").html("System image unavailable");
					that.parentView.$("#div-detail-desc-content").addClass("overview-big-size");
				}
        
				});
				break;

			}
		},

		render: function(data) {
			var that = this;
	        this.images = data;
	        images = data;
	        this.i=0;
	        this.configPath(that.images[that.i]);
		},

        /*This function checks if the images are available in the device, otherwise it takes the images from server */
		configPath: function(image) {
			var that = this;
			var src = "";
			fnGetLocalFileSystemPath(function(path) {
				fnGetImage(path+encodeURI(image["Path"]), function(status) {
	                if(status!="error") {
	                    src = path+encodeURI(image["Path"]);
	                    var strImage = '[{"ID" : "' + image["ID"] + '", "Name" : "' + image["Name"] + '", "Path" : "' + src + '", "Title" : "' + image["Title"] + '"}]';
	 		            that.$el.html(that.template($.parseJSON(strImage)));
			            that.showImage(image["ID"]);
			            that.incrementI();
	                }
	                else if(navigator.onLine) {
	                    src = setFileServer()+encodeURI(image["Path"])+AzureSASToken;

	                    var strImage = '[{"ID" : "' + image["ID"] + '", "Name" : "' + image["Name"] + '", "Path" : "' + src + '", "Title" : "' + image["Title"] + '"}]';

	                   	that.$el.html(that.template($.parseJSON(strImage)));
			            that.showImage(image["ID"]);
			            that.incrementI();
			            that.processError(this); 
			            that.parentView.$("#btn-download").show();
			            that.parentView.$("#btn-download").addClass("show-btn-download-viewer");
			            that.parentView.$(".div-detail-navigation-iphone #btn-download").show();
	                }
//	                else {
//	                	that.parentView.$("#li-detail-image").html("");
//	                	showMsgView("006");
//	                }
	               
	            });
			});
		},

		processError: function(img) {
			var that = this;
			var image = document.getElementsByClassName("detail-image");
			for(var i=0;i<image.length;i++) {
				image[i].onerror = function() {
					console.log(this.src);
					this.src = "globusMobileApp/image/icon/ios/missing.jpeg";

					that.parentView.$("#div-itemlist-count").addClass("set-group-itemlist-count");
					that.parentView.$("#btn-download").hide();
					$("#btn-download-viewer").hide();
					$("#btn-download").removeClass("show-btn-download-viewer");
					this.className = "img-missing";
				}
			}
		},

		showImage: function(id) {
			var imgContainer = this.parentView.$("#div-detail-image-container")
			imgContainer.append(this.$el.html());

			var dtlImage = imgContainer.find('img');
            dtlImage.trigger('create');

		},

		incrementI: function() {
			var that = this;
			this.i++;
	        if(this.i<this.images.length) {
	        	this.configPath(this.images[this.i]);
	        }
		},
        
		/* Download the images from server path */
		downloadImages: function() {
			var that = this;
			var i = 0;
			SuccessCnt = 0;
			if(navigator.onLine) {
				showMsgView("038");
				if (this.strViewType == "images") {
					$(".imagelist-image").each(function() {
						if(!$(this).hasClass('cls-threesixty-default')) {
							var image = this;
							var Source = $(this).attr("src");
                            Source = Source.replace(URL_FileServer,setFileServer());
                            Source = Source.replace(AzureSASToken,"");
							var Destination = that.localpath+$(this).attr("src").replace(URL_FileServer,'').replace(that.localpath,'');

							var connectionType = (navigator.connection.type).toLowerCase();
							if((connectionType.indexOf('cellular')>=0) && that.parent.confirmDownload) {
								showNativeConfirm("This device is connected to internet through Data plan. Do you really want to use Data plan to download the file?","Warning",["Yes","Yes, Don't ask me again","No"], function(btnIndex) {
									if(btnIndex==1) 
										fnDownload(Source,Destination);
									else if(btnIndex==2) {
										that.parent.confirmDownload = false;
										fnDownload(Source,Destination);
									}
								})
							}
							else
								fnDownload(Source,Destination);
							
							that.parentView.$("#btn-download").hide();
							$("#btn-download-viewer").hide();
							$("#btn-download").removeClass("show-btn-download-viewer");
						}
					});
				}

				else {
					for(i=0;i<that.images.length;i++) {
						var image = that.images[i];
						var Source = setFileServer()+image["Path"];
						var Destination = that.localpath+image["Path"];

						var connectionType = (navigator.connection.type).toLowerCase();
						if((connectionType.indexOf('cellular')>=0) && that.parent.confirmDownload) {
							showNativeConfirm("This device is connected to internet through Data plan. Do you really want to use Data plan to download the file?","Warning",["Yes","Yes, Don't ask me again","No"], function(btnIndex) {
								if(btnIndex==1) 
									fnDownload(Source,Destination);
								else if(btnIndex==2) {
									that.parent.confirmDownload = false;
									fnDownload(Source,Destination);
								}
							})
						}
						else
							fnDownload(Source,Destination);
						
						
						that.parentView.$("#btn-download").hide();
						$("#btn-download-viewer").hide();
						$("#btn-download").removeClass("show-btn-download-viewer");
					}
				}
			}
			else {
				showMsgView("007");
			}
		},

		displayAllImages: function(jvalue,rsHeader,rsHeaderContents,callback) {
			var that = this;
			imageData=[];

	        this.getExactPath(0,this, rsHeaderContents, function(data) {
        
//				if(data.length == 0) {
////					that.parentView.$("#div-detail-images").html("<li class='li-no-display'>Images not available</li>");
//                    that.parentView.$("#li-subtype-" + rsHeader[jvalue].ID).html("hahahaha")
//				}
//				else {
                
				if(data.length)
					that.parentView.showItemListView(that.parentView.$("#li-subtype-" + rsHeader[jvalue].ID) , data, "GmImageList", 0, undefined, URL_Collateral_Template,that.parentView.reRenderedItemList);
				else{
                    that.parentView.$("#li-subtype-" + rsHeader[jvalue].ID).html("Images are not yet downloaded!");
                    that.parentView.$("#li-subtype-" + rsHeader[jvalue].ID).css({"padding":"5px"})
                }
                
                $(".div-icon-share").each(function() {
                    var elem = this;
                    fnCheckDocuments($(this).attr('name'),function(status){
                        if(status=="success") {
                            $(elem).addClass('shared');
                            $(elem).html('<i class="fa fa-times-circle-o fa-2x"></i>');
                        }
                        else {
                            $(elem).html('<i class="fa fa-share-alt-square fa-2x"></i>');
                        }
                    });
                });


                $("#div-itemlist li").each(function(){
                    if($(this).find('.div-detail-image-parent img').attr('id') == that.parentView.strDocID ){
                        $(this).find('.div-images-image').addClass('div-selected-image');

                    }
                });

                $(".imagelist-image").each(function() {
                    var elem = this;
                    $(this).load(function(){
                        that.fixImageListImages(this);
                    });
                });
                callback();
//				}
		       });
			},
			
		fixImageListImages: function (elem) {
			var imgwidth = $(elem).width();
			var imgheight = $(elem).height();
			var parentwidth = $(elem).parent().width();
			var parentheight = $(elem).parent().height();
			if (imgwidth > parentwidth) {
				$(elem).css("width", parentwidth + "px" );
				$(elem).css("height", "auto");
				imgheight = $(elem).height();
				if(imgheight>parentheight) {
					$(elem).css("height", parentheight + "px" );
					$(elem).css("width", "auto");
					imgwidth = $(elem).width();
					if (imgwidth > parentwidth) {
						$(elem).css("width", (parentwidth-20) + "px" );
						$(elem).css("height", "auto");
					}
				}
			}

			else if(imgheight>parentheight) {
				$(elem).css("height", parentheight + "px" );
				$(elem).css("width", "auto");
				imgwidth = $(elem).width();
				if (imgwidth > parentwidth) {
					$(elem).css("width", parentwidth + "px" );
					$(elem).css("height", "auto");
				}
			}
		},


		getExactPath: function(iValue,that, allImages, callback) {

			var src = '';
			var image = allImages[iValue];

			fnGetLocalFileSystemPath(function(path) {
                fnGetImage(path+image["Path"], function(status) {
 	                if(status!="error") {
		                if(image["Path"].indexOf('.zip')>=0) {
							src = "globusMobileApp/image/icon/ios/ThreeSixty.png";
							var strImage = '{"ID" : "' + image["ID"] + '", "Name" : "' + image["Name"] + '", "Path" : "' + src + '", "Title" : "' + image["Title"] + '", "Class" : "cls-threesixty-default", "value" : "' + (path+image["Path"]) + '", "ShareVal" : "' + image["ShareValue"] + '" }';
	     	            }
		                else {
		                	
		                    src = path+image["Path"];
		                    var strImage = '{"ID" : "' + image["ID"] + '", "Name" : "' + image["Name"] + '", "Path" : "' + src + '", "Title" : "' + image["Title"] + '", "ShareVal" : "' + image["ShareValue"] + '" }';
		                }
           				imageData.push($.parseJSON(strImage));
                	}
                	 else if(navigator.onLine) { 
    					
	                    if(image["Path"].indexOf('.zip')>=0) {
							src = "globusMobileApp/image/icon/ios/ThreeSixty.png";
							var strImage = '{"ID" : "' + image["ID"] + '", "Name" : "' + image["Name"] + '", "Path" : "' + src + '", "Title" : "' + image["Title"] + '", "Class" : "cls-threesixty-default", "value" : "' + (URL_FileServer+image["Path"]) + '", "ShareVal" : "' + image["ShareValue"] + '"  }';
	                	}
	              		 
	              		else {
	              		  	
		                    src = setFileServer()+encodeURI(image["Path"])+AzureSASToken;
		                    var strImage = '{"ID" : "' + image["ID"] + '", "Name" : "' + image["Name"] + '", "Path" : "' + src + '", "Title" : "' + image["Title"] + '", "ShareVal" : "' + image["ShareValue"] + '" }';
          					that.parentView.$("#btn-download").show();
			            	that.parentView.$("#btn-download").addClass("show-btn-download-viewer");
			            	that.parentView.$(".div-detail-navigation-iphone #btn-download").show();
          				}
         				imageData.push($.parseJSON(strImage));
           				
              		}

//              		else {
//	                	that.parentView.$("#div-detail-images").html("");
//	                	showMsgView("006");
//	                }

	                if(iValue<(allImages.length-1)) {
	                
	              		that.getExactPath(iValue+1,that,allImages,callback);
	                }
	                else                			
               			callback(imageData);
               		
           		});
            });

		},

	    //Closes the View
	    close: function(event) {
	    	this.unbind();
	    	this.remove();
	    }
	});

	return ImageView;
});
