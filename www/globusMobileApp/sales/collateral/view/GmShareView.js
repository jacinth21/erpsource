/**********************************************************************************
 * File:        GmShareView.js
 * Description: View to display the Share Screen to share the artifacts through email
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/


define([
	'jquery', 'underscore', 'backbone', 'handlebars',
	'global',
	'itemmodel', 'itemcollection', 'itemlistview', 'contactsview', 'commonutil'
], function ($, _, Backbone, Handlebars, Global, ItemModel, ItemCol, ItemListView, ContactsView, CommonUtil) {

	'use strict';
	var ShareView = Backbone.View.extend({

		events : {
			"keyup .txt-search-contact" : "showContacts",
			"click #div-contacts-icon, .txt-search-contact" : "showContacts",
			"click .btn-contact-favorite" : "processFavorite",
			"click #li-contacts-btn-search" : "focusSearch",
			"click #ul-contacts-header li": "toggleButtonColor",
			"click .btn-contact-select i" : "addToList",
			"click #div-email-search-x" : "clearKeyword",
			"click .li-document-share" : "removeFile",
			"click .spn-spandiv-close": "removeContactTo",
			"click .div-child-text" : "editContactTo",
			"click #div-contact-close": "closeContactList",
			"click #btn-share-send": "sendDataToServer",
			"click #li-contacts-favorite" : "displayFavorites",
			"click #li-contacts-recent" : "displayRecent",
			"click #li-contacts-new, .btn-contact-edit" : "addNewContact",
			"click .ph-li-contact-name": "showEmails",
			"click #li-contacts-cancel" : "callcloseContactView",
			"click #li-contacts-save" : "callSaveNewContacts",
            "click .list-contact-item" : "addToList",
            
		
		},
	
		template: fnGetTemplate(URL_Collateral_Template, "GmShare"),
		template_GmContactSearchList: fnGetTemplate(URL_Collateral_Template, "GmContactSearchList"),
		template_GmEmailSuccess: fnGetTemplate(URL_Collateral_Template, "GmEmailSuccess"),

		initialize: function() {
			toList = [];
            var oldInput, newInput;
            this.oldInput='';
            this.newInput='';
			this.repemailid = "";
			this.repphoneno = "";
			this.useremailid = '';
			this.globusPhoneNo = "";
			this.emailtemplateid = "";
			this.linkexpirydays = "";

			this.emailSubject = "";
			this.emailMessage = "Thank you for your interest in Globus Medical. Below are the link(s) to the collateral you requested.";
			this.companyAddress = "";
			$("#btn-back").show();
			this.render();
		},

		render: function () {
			
			var that = this;
			this.$el.html(this.template());
            this.renderShareList();
			
            if(GM.Global.GuideFl == "Y") {
                $("#div-catalog-filter").addClass("gmVisibilityHidden");
                $("#div-catalog-quicklink").addClass("gmVisibilityHidden"); 
            } else {
                $("#div-catalog-filter").removeClass("gmVisibilityHidden");
                $("#div-catalog-quicklink").removeClass("gmVisibilityHidden");
                $("#div-favorite-icon").removeClass("gmVisibilityHidden"); 
            }
            
			var partyID = window.localStorage.getItem("partyID");
			var companyID = localStorage.getItem("cmpid");
			var emailContactMode = "90452";
			var phoneContactMode = "90450";
            
			fnGetUserContactInfo(partyID, emailContactMode, function (dt) {
				if (dt != undefined) {
					that.repemailid = parseNull(dt.conval);
				}
			});
			fnGetUserContactInfo(partyID, phoneContactMode, function (dt) {
				if (dt != undefined) {
					that.repphoneno = parseNull(dt.conval);
				}
			});

			this.userId = localStorage.getItem("userID");
            
            fnGetEmailID(this.userId, function (dt) {
                console.log(dt);
                if (dt.length > 0) {
                    that.useremailid = dt[0].ID;
                }
                else {
                    that.useremailid = localStorage.getItem("emailid");
                }
                console.log(that.useremailid);
                $("#txt-email-cc").val(that.useremailid+",");
            });
			
           
            
			fnGetRuleValueByCompany('GLOBUS_CONTACT_NO', 'MARKETINGCOLL', companyID, function (dt) {
				if (dt.length > 0) {
					that.globusPhoneNo = dt[0].RULEVALUE;
				}
				else {
					fnGetRuleValue('DEFAULT_CONTACT_NO', 'MARKETINGCOLL', function (data) {
						console.log(data);
						if (data.length > 0) {
							that.globusPhoneNo = data[0].RULEVALUE;
						}
					});
				}
			});

			fnGetRuleValue('ADDDAYSTOLINKEXPIRY', 'MARKETINGCOLL', function (dt) {
				console.log(dt);
				if (dt.length > 0) {
					that.linkexpirydays = dt[0].RULEVALUE;
				}

			});

			fnGetRuleValueByCompany('SUBJECT_MKT_COLL_EMAIL', 'MARKETINGCOLL', companyID, function (dt) {
				console.log(dt);
				if (dt.length > 0) {
					that.emailSubject = dt[0].RULEVALUE;
					$('#txt-email-subject').val(that.emailSubject);
				}
				else {
					fnGetRuleValue('DEFAULT_SUBJECT_MKT_COLL_EMAIL', 'MARKETINGCOLL', function (data) {
						console.log(data);
						if (data.length > 0) {
							that.emailSubject = data[0].RULEVALUE;
							$('#txt-email-subject').val(that.emailSubject);
						}
					});
				}

			});

			// fnGetRuleValueByCompany('MESSAGE_MKT_COLL_EMAIL', 'MARKETINGCOLL', companyID, function (dt) {
			// 	console.log(dt);
			// 	if (dt.length > 0) {
			// 		that.emailMessage = dt[0].RULEVALUE;
			// 	}
			// 	else {
			// 		fnGetRuleValue('DEFAULT_MESSAGE_MKT_COLL_EMAIL', 'MARKETINGCOLL', function (data) {
			// 			console.log(data);
			// 			if (data.length > 0) {
			// 				that.emailMessage = data[0].RULEVALUE;
			// 			}
			// 		});

			// 	}
				// $("#txt-email-body").val(this.emailMessage);
			// });

			fnGetRuleValueByCompany('COMPANY_ADDRESS', 'MARKETINGCOLL', companyID, function (dt) {
				console.log(dt);
				if (dt.length > 0) {
					that.companyAddress = dt[0].RULEVALUE;
				}
				else {
					fnGetRuleValue('DEFAULT_COMPANY_ADDRESS', 'MARKETINGCOLL', function (data) {
						console.log(data);
						if (data.length > 0) {
							that.companyAddress = data[0].RULEVALUE;
						}
					});
				}
			});

			fnGetRuleValue('EMAIL_TEMPLATE_ID', 'MARKETINGCOLL', function (dt) {
				console.log(dt);
				if (dt.length > 0) {
					that.emailTempID = dt[0].RULEVALUE;
				}

			});

			return this;
		},
		
        // Re render the shared files after removing the files from share list
        renderShareList: function () {
            var that = this;
            fnGetSharedList(function(data) {
				$("#txt-email-body").val(that.emailMessage);
				fnGetSharedDocuments(function(documents) {
					if(documents.length>0) {
						var sortoptions = [{ "DefaultSort":"Name", "Elements":[{"ID":"Title", "DispName":"Documents", "ClassName":"li-document-name"}] }];
						that.showItemListView($("#div-share-document"), documents, "GmDocuments", 2, undefined, undefined, sortoptions, URL_Collateral_Template, that.reRenderedItemList);
						$(".li-document-share").addClass('shared');
						$(".shared").html('<i class="fa fa-times-circle-o fa-2x"></i>');
					}
					else{
						$("#div-share-document").hide();
					}
				});

				fnGetSharedImages(function(images) {
				
					if(images.length>0) {
						var sortoptions = [{ "DefaultSort":"Name", "Elements":[{"ID":"Title", "DispName":"Images", "ClassName":"li-document-name"}] }];
						that.showItemListView($("#div-share-images"), images, "GmDocuments", 2, undefined, undefined, sortoptions, URL_Collateral_Template, that.reRenderedItemList);
						$(".li-document-share").addClass('shared');
						$(".shared").html('<i class="fa fa-times-circle-o fa-2x"></i>');
					}
					else{
						$("#div-share-images").hide();
					}
				});

				fnGetSharedVideos(function(videos) {
				
					if(videos.length>0) {
						var sortoptions = [{ "DefaultSort":"Name", "Elements":[{"ID":"Title", "DispName":"Videos", "ClassName":"li-document-name"}] }];
						that.showItemListView($("#div-share-videos"), videos, "GmDocuments", 2,undefined,undefined, sortoptions,URL_Collateral_Template, that.reRenderedItemList);
						$(".li-document-share").addClass('shared');
						$(".shared").html('<i class="fa fa-times-circle-o fa-2x"></i>');
					}
					else{
						$("#div-share-videos").hide();
					}
				});
				
			});
        },
        
		removeFile:function(event){
			
			event.preventDefault();
			var elem = event.currentTarget;
			var FileID = $(elem).attr('name');
			var that=this;
			fnRemoveShare(FileID,function(){
				that.renderShareList();
			});
		},

		// for iPhone
		showEmails: function(e) {
			var elem = $(e.currentTarget);
			var parentclass = $(elem).parent().attr("class");
			this.$(".ph-div-email-contacts").each(function() {
            	$(this).removeClass("ph-div-email-contacts");
            });

            if(parentclass == "ph-div-email-contacts") {
            	$(elem).parent().removeClass("ph-div-email-contacts");
				$(".ph-div-email-contacts #ph-div-emaillist").slideUp("fast");
            }
            else {
            	$(elem).parent().addClass("ph-div-email-contacts");
				$(".ph-div-email-contacts #ph-div-emaillist").slideDown("fast");
            }
		},

		addNewContact: function (event) {
            event.stopPropagation();
			$("#txt-contacts-search, #div-email-search-x").addClass("hide");
			this.contactView = new ContactsView(event);
			$("#div-contacts-list").html(this.contactView.render().el);
			$("#li-contacts-new").hide();
			$("#li-contacts-save").show();
			$("#li-contacts-cancel").show();
		},

		callcloseContactView: function(e) {
			
			this.contactView.closeView(e);
			$("#li-contacts-save").hide();
			$("#li-contacts-cancel").hide();
	
		},
		
		callSaveNewContacts: function(e){
			this.contactView.saveNewContacts(e);
		},
		
		displayFavorites: function (event) {
			var that=this;
			event.preventDefault();
			$("#txt-contacts-search, #div-email-search-x").addClass("hide");
			var num = $(event.currentTarget).find('#spn-fav-count').text().trim();
		   

			if(num == 0) {
				$("#div-contacts-list").html("<li  class='li-no-data'> No Favorite Contacts to display</li>");
			}
			else{
				fnGetFavoriteEmail(function(data){
					that.showItemListView($("#div-contacts-list"), data, "GmContacts", 0, undefined, undefined, undefined, URL_Collateral_Template, undefined);
					var toIDs = _.pluck(toList,'contactid');
				    $(".fa-check-icon").each(function() {
						var elem = this;
						if(toIDs.indexOf($(elem).attr('id')) >= 0) 
							$(elem).addClass("fa fa-check-square-o fa-lg");
						else
							$(elem).addClass("fa fa-square-o fa-lg");
					});
				});
			}
		},

		displayRecent: function(event){
			var that = this;
			$("#txt-contacts-search, #div-email-search-x").addClass("hide");
			
			fnGetRecentEmail("contacts",function(data){
				if(data.length == 0) {
					$("#div-contacts-list").html("<li  class='li-no-data'> No Recent Contacts to display</li>");
				}
				else {
					that.showItemListView($("#div-contacts-list"), data, "GmContacts", 0, undefined, undefined, undefined, URL_Collateral_Template, undefined);
					var toIDs = _.pluck(toList,'contactid');
				    $(".fa-check-icon").each(function() {
						var elem = this;
						if(toIDs.indexOf($(elem).attr('id')) >= 0) 
							$(elem).addClass("fa fa-check-square-o fa-lg");
						else
							$(elem).addClass("fa fa-square-o fa-lg");
					});
				}
			});
		},

		detectTypedEmail: function (e) {
			var searchDataSpace = e.keyCode;
			var that = this;
			var strSearchData = $(e.currentTarget).val();
			var intWordLength = strSearchData.length;
			var strSearchLastWord = strSearchData.slice(-1);
			
			if ((searchDataSpace == 32) || (strSearchLastWord == ',') || (strSearchLastWord == ';') || (strSearchLastWord == ' ')) {
                console.log(strSearchData);
				strSearchData = strSearchData.replace(',','').replace(';','').replace(' ','');
			    var strEmailID = strSearchData; 
			    var atpos = strEmailID.indexOf("@");
	    	    var dotpos = strEmailID.lastIndexOf(".");
	    	    
		    	    if (atpos < 1 || dotpos <atpos+2 || dotpos+2 >= strEmailID.length) {
		    	        console.log("Not a valid e-mail address");
		    	    }
		    	    else {
		    	    	$("#li-contacts").addClass('hide');
						toList.push({'refid': strEmailID, 'emailid': strEmailID, 'recipienttype' : '103236'});
		    	    	this.createDynamicEmail(strEmailID,strEmailID);
		    	    	$(e.currentTarget).val('');
		    	  }
			}
		},

		contactsviewer: function(event){
			event.preventDefault();
		},
		
		createDynamicEmail: function(targetID, targetValue){
			if(parseNull(targetID)==''){
                var parentDiv = $(document.createElement('div')).attr({'email': targetValue, 'class' :'div-parent-div'});
            }
            else{
			var parentDiv = $(document.createElement('div')).attr({'email': targetID, 'class' :'div-parent-div'});
            }
			var parentDivSpan = $(document.createElement('span')).attr('class' , 'spn-parent-span');
			var spanDivText = $(document.createElement('div')).attr('class', 'div-child-text');
			var spanDivClose = $(document.createElement('div')).attr('class', 'spn-spandiv-close fa fa-times-circle fa-lg');
			$(spanDivText).text(targetValue);
		    $("#spn-dynamic-recipients-list").append(parentDiv);
		    $(parentDiv).append(parentDivSpan);
		    $(parentDivSpan).append(spanDivText);
		    $(parentDivSpan).append(spanDivClose);
		    var widthofParentDiv = $("#div-to-email").width();
		    var widthofSearchText = $("#txt-to-email").width();
		    var widthofContact =  $(".div-parent-div").width();
		    var widthofTextbox = widthofSearchText-widthofContact;
			$("#txt-to-email").width(widthofTextbox);
		},
		
		closeContactList: function(e){
			
			$("#div-popup-to-emails").hide();
		},
		
		
		removeContactTo: function(e){
			var target = $(e.currentTarget).parent().parent().attr('email');             
			if(target.indexOf('@')<0)
                $("#"+target).removeClass("selected").addClass("not-selected");
            else
            $('div[data-idemail="'+target+'"]').removeClass("selected").addClass("not-selected");
            $('.selected').parent('li').css('background', '#ccc');
            $('.not-selected').parent('li').css('background', '#fff');
			var widthofSearchText = $("#txt-to-email").width();
			var widthofContact =  $(".div-parent-div").width();
			$(e.currentTarget).parent().parent().remove();
                toList = _.reject(toList, function(dt){
                      
                    if(target.indexOf('@')<0){return dt.contactid==target; }
                       else{return dt.emailid==target; }
            });
			$("#txt-to-email").width(widthofSearchText+widthofContact);
            this.$("#txt-email-id").focus();
			
		},	
		
		editContactTo: function(e){
			var editText = $(e.currentTarget).text();
			$(e.currentTarget).parent().find(".spn-spandiv-close").trigger("click");
			this.$("#txt-email-id").val(editText);
		},
		
		showContacts: function(e){
            var that = this;
            this.newInput = $("#txt-email-id").val();
            if((this.newInput.length==0)&&(this.oldInput.length==0)&&(e.keyCode==8)){
                 $('#spn-dynamic-recipients-list').children().last().children().children(".spn-spandiv-close").trigger("click");
            }
           this.oldInput=this.newInput;
            
            
			if($("#li-contacts").html().trim()=="") {
				$("#li-contacts").html(this.template_GmContactSearchList);
				this.updateFavoriteCount();	
			}
			else {
				$("#li-contacts").removeClass('hide');

				$("#div-contacts").removeClass("hide");
			}
				

			$("#txt-contacts-search, #div-email-search-x").removeClass("hide");
			

			if(e.currentTarget.id!="div-contacts-icon") {
				if(e.currentTarget.id=="txt-email-id") {
					$("#txt-contacts-search, #div-email-search-x").addClass("hide");
					$("#li-contacts-save").hide();
					$("#li-contacts-cancel").hide();
					$("#li-contacts-new").show();
					this.detectTypedEmail(e);
				}
				this.listContacts(e, "#div-contacts-list");
			}
		},

		listContacts: function(e, containerID) {

			var that = this;
			var strSearchData = $(e.currentTarget).val();
			var intWordLength = strSearchData.length;
			var currID = e.currentTarget.id;            

			if(currID=="txt-contacts-search")
				this.setX(intWordLength);

			$(".btn-selected").removeClass("btn-selected");
			$("#li-contacts-btn-search").addClass("btn-selected");

			switch (true) {

			// case (intWordLength == 0):
			// 	$("#div-contacts-list").html("<li class='li-no-data'>No Results Found!</li>"); 
			// 	break;

			case (intWordLength < 3):
				if($("#txt-contacts-search").hasClass("hide")) {
					$("#div-contacts").addClass("hide");
				}
				break;

			case (intWordLength == 3):
				
				fnSearchEmail(strSearchData, function(rsEmails) {                   
                    rsEmails=_.uniq(rsEmails, false, function(p){ return p.EmailSearchName; });                 
					that.emailcol = new ItemCol(rsEmails);
					if(rsEmails.length > 0) {
						$("#div-contacts").removeClass("hide");
                        
					    that.showItemListView($(containerID), rsEmails, "GmContacts", 0, undefined, undefined, undefined, URL_Collateral_Template, undefined);
					   	var toIDs = _.pluck(toList,'contactid');
                        var toEmails=_.pluck(toList,'emailid');
					    $(".list-contact-item").each(function() {
							var elem = this;
                           if($(elem).attr('id')){
							   if(toIDs.indexOf($(elem).attr('id')) >= 0) 
                                $(elem).addClass("selected");
							    else
                                $(elem).addClass("not-selected");
                               }
                            else{
                                if(toEmails.indexOf($(elem).attr('data-idemail')) >= 0) 
                                $(elem).addClass("selected");
							    else
                                $(elem).addClass("not-selected");
                               }                            
						});
                        $('.selected').parent('li').css('background', '#ccc');
                        $('.not-selected').parent('li').css('background', '#fff');
					}
					else{
						$("#div-contacts-list").html("<li  class='li-no-data'>No Results Found!</li>");
                        
					}
					
				});
				break;
				
			case (intWordLength > 2):
				$("#div-contacts").removeClass("hide");
				
				that.renderResult(that.emailcol, strSearchData, containerID, "#spn-app-count", "EmailSearchName");
                var toIDs = _.pluck(toList,'contactid');
				var toEmails=_.pluck(toList,'emailid');
					    $(".list-contact-item").each(function() {
							var elem = this;
                           if($(elem).attr('id')){
							   if(toIDs.indexOf($(elem).attr('id')) >= 0) 
                                $(elem).addClass("selected");
							    else
                                $(elem).addClass("not-selected");
                               }
                            else{
                                if(toEmails.indexOf($(elem).attr('data-idemail')) >= 0) 
                                $(elem).addClass("selected");
							    else
                                $(elem).addClass("not-selected");
                               }
                            });
                  $('.selected').parent('li').css('background', '#ccc');  
                    $('.not-selected').parent('li').css('background', '#fff');
				break;
			}
		},
 
	addToList: function(event){
			event.preventDefault();
			var elem = event.currentTarget;
			var targetID = $(event.currentTarget).attr('value');
			var targetContact = $(event.currentTarget).attr('id');
			var targetClass = $(event.currentTarget).attr('class');
			var targetValue = $(event.currentTarget).attr('name');
			var strRecipientName = $(event.currentTarget).find("#li-contact-name").text();
 

    		if($(elem).hasClass('not-selected')) {
    			//fnInsertRecent("contacts",targetContact);
				$(elem).removeClass("not-selected").addClass("selected");
                 $('.selected').parent('li').css('background', '#ccc');
                $('.not-selected').parent('li').css('background', '#fff');
				if(targetID!='')
					toList.push({'refid': targetID, 'emailid': targetValue, 'recipienttype' : '103236', 'reftype' : '103238', 'contactid' : targetContact, 'recipientname' : strRecipientName});
				else
					toList.push({'emailid': targetValue, 'recipienttype' : '103236', 'contactid' : targetContact, 'recipientname' : strRecipientName});              
				$("#spn-dynamic-recipients-list").html('');
				for(var i=0;i<toList.length;i++) {
			        this.createDynamicEmail(toList[i].contactid,toList[i].emailid);
				}
    		}
			 else {
                $(elem).removeClass("selected").addClass("not-selected");
                 $('.selected').parent('li').css('background', '#ccc');
                 $('.not-selected').parent('li').css('background', '#fff');        
                toList = _.reject(toList, function(dt){
                if(targetContact!='')
                return dt.contactid==targetContact;    
                else
                return dt.emailid==targetValue;  
                });
			  	$("#spn-dynamic-recipients-list").html('');
			 	for(var i=0;i<toList.length;i++) 
	               this.createDynamicEmail(toList[i].contactid,toList[i].emailid);
			 }    
        this.$("#txt-email-id").val("");
        $("#txt-email-id").trigger("click");
			 this.$("#txt-email-id").focus();
        
		},

		searchContacts: function(e) {
			this.listContacts(e, "#div-contacts-list");
		},

		setX: function(intWordLength) {
			if(intWordLength == 0) {
				$("#div-email-search-x").removeClass("div-email-search-x-show").addClass("div-email-search-x-hide");
			}
			else {
				$("#div-email-search-x").removeClass("div-email-search-x-hide").addClass("div-email-search-x-show");
			}
		},

		focusSearch: function  (event) {

			$("#txt-contacts-search").trigger("click");
			$("#txt-contacts-search").focus();

		},

		sendDataToServer: function (e) {
			var that = this;
			var emailnotificationfl = 'N';
			var userAccessLvl = localStorage.getItem('acid');
			var userFullName = localStorage.getItem('fName') + ' ' + localStorage.getItem('lName');
			this.emailValidation = true;
			if ($('#share-email-check').prop("checked")) {
				emailnotificationfl = 'Y'
			}

			var emailCCValue = $("#txt-email-cc").val();
			var gmSharedEmailCCVO = [];
			if(parseNull(emailCCValue) != "") {
				var emailCCArr = emailCCValue.split(',');
				console.log(emailCCArr);
				_.each(emailCCArr, function(dt){
					console.log(dt);
					console.log(validateEmailID(dt));
					if(!validateEmailID(dt)) {
						showError("Invalid Email Addres " + dt);
						that.emailValidation = false;
					}
					else {
						gmSharedEmailCCVO.push({"emailid":dt, "recipienttype": "103237"});
					}
				});
			}
			console.log(gmSharedEmailCCVO);
			var emailContents = {
				'shextrefid': '',
				'sharetype': 103221,
				'subject': $('#txt-email-subject').val(),
				'msgdetail': $("#txt-email-body").val(),
				'gmSharedEmailVO': '',
				'gmSharedFileVO': '',
				'gmSharedEmailCCVO': gmSharedEmailCCVO,
				'phoneno': this.globusPhoneNo,
				'useremailid': this.useremailid,
				'userfullname': userFullName,
				'expiryday': parseInt(this.linkexpirydays),
				'emailtrackingfl': emailnotificationfl,
				'emailtemplateid' : this.emailtemplateid,
				'companyaddress' : this.companyAddress,
				'createdby' : this.userId,
				'refid' : this.userId,
				'useraccesslevel' : userAccessLvl,
				'dateformat' : localStorage.getItem('cmpdfmt'),
                'companyid' : localStorage.getItem("cmpid"),
                'companytimezone' : localStorage.getItem("cmptzone")
			}

            //console.log ('after set timezone value ' + emailContents.companytimezone);
			
			var textEmail = jQuery.Event("keyup");
			textEmail.keyCode = 32;
			textEmail.which = 32;
			$("#txt-email-id").trigger(textEmail);

			if (userAccessLvl == "1") {
				emailContents.phoneno = this.repphoneno;
				emailContents.useremailid = this.repemailid;
			}

			var objEmailVO = JSON.stringify(toList);
			var objFileVO = "";
            
			if(toList.length >0 && this.emailValidation){
               for(var i=0; i<toList.length;i++)
                   {
                       if(parseNull(toList[i].contactid)==''){fnInsertRecent("emails",toList[i].emailid);}
                       else{fnInsertRecent("contacts",toList[i].contactid);}
                   }

				for(var i=0; i<toList.length;i++)
					delete toList[i].contactid;
				
				this.$("#btn-share-send").removeClass("btn-navigation-80").addClass("btn-navigation-100").html("Sending...");

				 for(var i=0; i<toList.length; i++) {
		           if(toList[i].refid == toList[i].emailid) 
		           	 delete toList[i].refid;
		        }
				fnGetSharedFiles(function(data){
					if(data.length>0) {
						emailContents.gmSharedEmailVO = toList;
						emailContents.gmSharedFileVO = data;
						emailContents.token = localStorage.getItem('token');
						console.log(JSON.stringify(emailContents));

						fnInsertShareHistory(JSON.stringify(emailContents),function (shareId) {
							emailContents.shextrefid = shareId;
							fnUpdateJSON(shareId, JSON.stringify(emailContents), function() {
								if((navigator.onLine) && (!intOfflineMode)) {
									sendEmail(JSON.stringify(emailContents),function(data, res) {
										if(data!='error') {
											fnUpdateStatus(data.shextrefid,data.shareid,data.sharestatus, function  () {
												that.$(".div-share-list").hide();
												that.$("#btn-share-send").hide();
												that.$(".div-from-subject").hide();
												that.$("#ul-share-cc").hide();
												$("#ul-share-body").hide();
												that.$(".div-share-email-check").hide();
												that.$("#ul-share-email").html(that.template_GmEmailSuccess());
											});
										}
										else {                                          
                                            that.$("#btn-share-send").removeClass("btn-navigation-100").addClass("btn-navigation-80").html("Send");
										}
									});
								}  
								else if(intOfflineMode) {
									fnDeleteShareList(function () {
										showNativeAlert("This Email will be automatically sent when you login online","Cannot Send in Offline Mode");
										window.location.href = "#collateral";
									});
								}
								else {
									fnDeleteShareList(function () {
										showNativeAlert('No Internet Connection. This Email will be automatically sent when the internet connection is available','Cannot Send now','OK');
										window.location.href = "#collateral";
									});
								}
							});
							
						}); 
					}
					else{
						showNativeAlert('No artifacts is selected to share','No Artifacts!','OK', function  () {
							window.location.href = "#collateral";
						});
					}
				}); 
			}
			else {
				if(this.emailValidation)
					showNativeAlert('No Email ID specified','Cannot Send','OK'); 
			}
		},


		clearKeyword: function(e) {
			//showMsgView("011");
			this.$("#txt-contacts-search").val("");
			$("#div-email-search-x").removeClass("div-email-search-x-show").addClass("div-email-search-x-hide");
		},

		processFavorite: function(e) {
			var target = $(e.currentTarget).attr('name');
			var that = this;
			var strType= "contacts";
			var targetclass = $(e.currentTarget).attr('class');

			this.emailcol.each(function (model) {
				var modelID = model.get('ID');
				if(target == modelID) {
					var modelFav = model.get('FavClass');
					if(modelFav == 'contacts')
						model.set('FavClass', 'null');
					else
						model.set('FavClass', 'contacts');
				}
			});


			if($(e.currentTarget).find('i').hasClass('str-isFav-contacts')) {
				fnRemoveFavorite(target,function(status){
					if(status == "success") {
						$(e.currentTarget).find('i').removeClass('str-isFav-contacts');
						that.updateFavoriteCount();
					}
				});
			}
			else {
				fnInsertFavorite(target,strType,function(status) {
					if(status == "success") {
						$(e.currentTarget).find('i').addClass('str-isFav-contacts');
						that.updateFavoriteCount();
					}				
				});
			}
		},

		updateFavoriteCount: function () {
			fnGetFavoriteEmail(function (count) {
				var count = count.length;
				$("#spn-fav-count").html("&nbsp;" + count + "&nbsp;");
			});
		},

		showItemListView: function(controlID, data, itemtemplate, columnheader, renderType, callback, sortoptions, template_URL, callbackOnReRender) {
			
			var itemsCol = new ItemCol(data);	
			this.itemlistview = new ItemListView({ 
				el: controlID, 
				collection: itemsCol,
				columnheader: columnheader, 
				itemtemplate: itemtemplate,
				renderType: renderType,
				sortoptions: sortoptions,
				template_URL: template_URL,
				callbackOnReRender: callbackOnReRender
			});
		},

		renderResult: function(collection, letters, controlID, LengthControlID, searchField) {
             
			var its = collection.search(searchField, letters);
			$(LengthControlID).html("&nbsp;" + its.length + "&nbsp;");
			this.showItemListView($(controlID), its, "GmContacts", 0,undefined,  undefined, undefined, URL_Collateral_Template,undefined);
			
			if($(controlID).text().trim() == "x") {
				
				$(controlID).html("<li  class='li-no-data'>No Results Found!</li>");
			}
		},

		reRenderedItemList: function(currList) {
		
			currList.$(".li-document-share").addClass('shared');
			currList.$(".shared").html('<i class="fa fa-times-circle-o fa-2x"></i>');
		},

		toggleButtonColor: function(e) {

			if($(e.currentTarget).attr('id')!="li-contacts-search") {
				$(".btn-selected").removeClass("btn-selected");
				$(e.currentTarget).addClass("btn-selected");

				if(($(e.currentTarget).attr('id')!="li-contacts-new") && ($(e.currentTarget).attr('id')!="li-contacts-save") ) {
					$("#li-contacts-save").hide();
					$("#li-contacts-cancel").hide();
					$("#li-contacts-new").show();
				}
				else {
					$("#li-contacts-save").show();
					$("#li-contacts-cancel").show();
					$("#li-contacts-new").hide();
				}
			}

			
		}

	});

	return ShareView;
});