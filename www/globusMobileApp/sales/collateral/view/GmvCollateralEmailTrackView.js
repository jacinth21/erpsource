/**********************************************************************************
 * File:        GmCollateralEmailTrackView.js
 * Description: 
 * Version:     1.0
 * Author:     Vinoth Petchimuthu
 **********************************************************************************/
define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars', 'jqueryui',

    // Common
    'global',

    // Pre-Compiled Template
    'gmtTemplate',  'commonutil'
    //Model
],
    function ($, _, Backbone, Handlebars, JqueryUI, Global, GMTTemplate, Commonutil,) {

        'use strict';

        var CollateralEmailTrackView = Backbone.View.extend({

            el: "#div-main",

            template: fnGetTemplate(URL_Collateral_Template, "GmtCollateralEmailTrackSummary"),
            template_detail: fnGetTemplate(URL_Collateral_Template, "GmtCollateralEmailTrackDetail"),
            template_detail_timestamp: fnGetTemplate(URL_Collateral_Template, "GmtCollateralEmailTrackDetailTimeStamp"),
            template_mostfrequent: fnGetTemplate(URL_Collateral_Template, "GmtCollateralMostFrequent"),
            template_widget: fnGetTemplate(URL_Collateral_Template, "GmtCollateralEmailTrackWidget"),

            events: {
                "change .collateral-month-filter": "clickDateTabs",
                "click #btn-load-chart": "loadBtnClicked",
                "click #detailView, .link-toredirect": "clickDetailLinkTabs",
                "click #summaryView": "clickSummaryLinkTabs",
                "click #btn-load-detail-report": "loadDetailReport",
                "click .most-shared": "redirectToSystem",
            },


            initialize: function () {
                this.chartDelivered = [];
                this.chartOpens = [];
                this.chartClicks = [];
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                var yyyy = today.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd
                };
                if (mm < 10) {
                    mm = '0' + mm
                };
                this.today = mm + '/' + dd + '/' + yyyy;
                GM.Global.EmailTrackFL = "";
                this.userAccessLvl = localStorage.getItem('acid');
                this.userId = localStorage.getItem("userID");
                this.userDept = localStorage.getItem("deptid");
                this.render();

            },
            render: function () {
                var that = this;
                this.resultJson = {};
                var fromdate = new Date();
                var todate = new Date();
                fromdate.setMonth(fromdate.getMonth() - 0);
                var fromDtdd = ("0" + fromdate.getDate()).slice(-2),
                    fromDtmm = ("0" + (fromdate.getMonth() + 1)).slice(-2),
                    fromDtyyyy = fromdate.getFullYear();
                var toDtdd = ("0" + todate.getDate()).slice(-2),
                    toDtmm = ("0" + (todate.getMonth() + 1)).slice(-2),
                    toDtyyyy = todate.getFullYear();
                fromdate = fromDtyyyy + "" + fromDtmm;
                todate = toDtyyyy + "" + toDtmm;
                that.formatDt(fromdate, "from");
                that.formatDt(todate, "to");
                $("#btn-back").show();
                that.dateRange = '1M';

                // $('#btn-back').on('click', function () {
                //     that.clearDetailReport();
                // });
                // $('#btn-home').on('click', function () {
                //     that.clearDetailReport();
                // });


                var input = {
                    "accessLvl": this.userAccessLvl,
                    "userId": this.userId,
                    "fromDate": this.actstartdt,
                    "toDate": this.actenddt,
                    "action": "",
                    "userDept" : this.userDept
                };
                // $("#div-main").html(that.template());
                $('.dhx_grid').empty();
                yearData();
                monthData();
                $('#btn-load-chart').addClass('hide');
                $('#email-track-detail-report').empty();
                $('#email-track-detail-report').addClass('hide');
                // that.fetchTotalLinkSends(input);
                // that.collateralActivityChart(input);
                this.clickSummaryLinkTabs();
            },

            //function used to load Email Track Detailed View Report when Details link clicked
            clickDetailLinkTabs: function (e) {
                var that = this;
                var id = $(e.currentTarget).attr('id');
                var input = {};
                if (id == 'contacts_email' || id == 'contacts_opened' || id == 'contacts_clicked') {
                    input = {

                        "emailid": $(e.currentTarget).attr('data_recipient'),
                        "eventtype": $(e.currentTarget).attr('data_eventtype'),
                        "accessLvl": this.userAccessLvl,
                        "userId": this.userId,
                        "fromDate": this.actstartdt,
                        "toDate": this.actenddt,
                        "action": "",
                        "userDept" : this.userDept
                    };
                }
                else if (id == 'content_title' || id == 'content_sends' || id == 'content_clicks' || id == 'shared_content_title') {
                    input = {

                        "contenttitle": $(e.currentTarget).attr('data_content'),
                        "eventtype": $(e.currentTarget).attr('data_eventtype'),
                        "accessLvl": this.userAccessLvl,
                        "userId": this.userId,
                        "fromDate": this.actstartdt,
                        "toDate": this.actenddt,
                        "action": "",
                        "userDept" : this.userDept
                    };
                }
                else {
                    input = {
                        "accessLvl": this.userAccessLvl,
                        "userId": this.userId,
                        "fromDate": this.actstartdt,
                        "toDate": this.actenddt,
                        "action": "",
                        "userDept" : this.userDept
                    };
                }

                this.detailsInput = input;
                that.loadEmailTrackDetailReport(input);
                $("#div-main").html(that.template_detail());
                monthData();
                yearData();
                //used to refresh data automatically in every 30mins
                // we no need to refresh screen every mins - comment the code

                /*that.interval =  setInterval(function() {
                         that.loadEmailTrackDetailReport(input);
                     }, 180000);
                 */

                $('.collateral-detail-month-filter').val(that.dateRange);
                if (that.dateRange == 'custom') {
                    $('.SHB-right').removeClass('hide');
                    $('#btn-load-chart').removeClass('hide');
                    $('#btn-load-detail-report').removeClass('hide');
                    $('#summary-details').removeClass('without-load-btn');
                    $('#summary-details').addClass('with-load-btn');
                    $("#select-surg-surgstartmonth option:selected").text(that.fromDtmm);
                    $('#select-surg-surgstartyear').val(that.fromDtyyyy);
                    $('#select-surg-surgendmonth option:selected').text(that.toDtmm);
                    $('#select-surg-surgendyear').val(that.toDtyyyy);
                }
                else {
                    $('#btn-load-detail-report').addClass('hide');

                }
                //function to load month and year in custom date filter
                $('#summaryView').removeClass('active-email-tab');
                $('#detailView').addClass('active-email-tab');
            },


            //function used to load Summary Report when Summary link clicked
            clickSummaryLinkTabs: function () {
                var that = this;
                $("#div-main").html(that.template());
                yearData();
                monthData();

                $('#email-track-detail-report').empty();
                $('#email-track-detail-report').addClass('hide');
                that.clearDetailReport();
                $('#btn-load-chart').addClass('hide');
                var input = {
                    "accessLvl": this.userAccessLvl,
                    "userId": this.userId,
                    "fromDate": this.actstartdt,
                    "toDate": this.actenddt,
                    "action": "",
                    "userDept" : this.userDept
                };

                that.collateralActivityChart(input);
                $('#detailView').removeClass('active-email-tab');

                $('.collateral-month-filter').val(that.dateRange);
                if (that.dateRange == 'custom') {
                    $('.SHB-right').removeClass('hide');
                    $('#btn-load-chart').removeClass('hide');
                    $('#btn-load-detail-report').removeClass('hide');
                    $('#summary-details').removeClass('without-load-btn');
                    $('#summary-details').addClass('with-load-btn');
                    $('#select-surg-surgstartmonth option:selected').text(that.fromDtmm);
                    $('#select-surg-surgstartyear').val(that.fromDtyyyy);
                    $('#select-surg-surgendmonth option:selected').text(that.toDtmm);
                    $('#select-surg-surgendyear').val(that.toDtyyyy);
                }
            },

            //Get Date From Which date Range is Selected in the Filter callback to the called function    
            getDateInputs: function (callback) {
                var dateRange, dr, fromDt, toDt;
                var that = this;
                this.$('.collateral-month-filter').each(function (i) {
                    dateRange = $(this).val();
                });
                switch (dateRange) {
                    case '1M':
                        dr = 0;
                        break;
                    case '3M':
                        dr = 2;
                        break;
                    case '1Y':
                        dr = 11;
                        break;
                    case 'custom':
                        break;
                }
                if (dateRange == 'custom') {
                    var fromDtmm = $("#select-surg-surgstartmonth option:selected").text();
                    that.fromDtmm = fromDtmm;
                    fromDtyyyy = $("#select-surg-surgstartyear option:selected").val();
                    that.fromDtyyyy = fromDtyyyy;
                    var toDtmm = $("#select-surg-surgendmonth option:selected").text();
                    that.toDtmm = toDtmm;
                    toDtyyyy = $("#select-surg-surgendyear option:selected").val();
                    that.toDtyyyy = toDtyyyy;
                    if (fromDtmm == "MM")
                        fromDtmm = "";
                    if (toDtmm == "MM")
                        toDtmm = "";
                    fromDt = fromDtyyyy + "" + fromDtmm;
                    toDt = toDtyyyy + "" + toDtmm;
                    that.formatDt(fromDt,"from");
                    that.formatDt(toDt, "to");
                } else {
                    fromDt = new Date();
                    toDt = new Date();
                    fromDt.setMonth(fromDt.getMonth() - dr);
                    var fromDtdd = ("0" + fromDt.getDate()).slice(-2),
                        fromDtmm = ("0" + (fromDt.getMonth() + 1)).slice(-2),
                        fromDtyyyy = fromDt.getFullYear();
                    var toDtdd = ("0" + toDt.getDate()).slice(-2),
                        toDtmm = ("0" + (toDt.getMonth() + 1)).slice(-2),
                        toDtyyyy = toDt.getFullYear();
                    fromDt = fromDtyyyy + "" + fromDtmm;
                    toDt = toDtyyyy + "" + toDtmm;
                    that.formatDt(fromDt, "from");
                    that.formatDt(toDt, "to");
                }
                callback(fromDt, toDt);
            },


            //  On clicking Date tabs to highlight the tab
            clickDateTabs: function (e) {
                var that = this;
                var dateRange = $(e.currentTarget).val();
                that.dateRange = dateRange;
                $('.surgin-dr-tab').removeClass("gmBGDarkerGrey gmFontWhite selected");
                $('.surgin-dr-tab').removeClass("gmBGGrey");
                $('.surgin-dr-tab').addClass("gmBGGrey");
                $('#' + dateRange).removeClass("gmBGGrey");
                $('#' + dateRange).addClass("gmBGDarkerGrey gmFontWhite selected");
                if (dateRange == 'custom') {
                    $('.SHB-right').removeClass('hide');
                    $('#btn-load-chart').removeClass('hide');
                    $('#btn-load-detail-report').removeClass('hide');
                    $('#summary-details').removeClass('without-load-btn');
                    $('#summary-details').addClass('with-load-btn');
                } else {
                    if (!($('.SHB-right').hasClass('hide')))
                        $('.SHB-right').addClass('hide');
                    $('#select-surg-surgstartmonth').val('');
                    $('#select-surg-surgstartyear').val('');
                    $('#select-surg-surgendmonth').val('');
                    $('#select-surg-surgendyear').val('');
                    $('#btn-load-chart').addClass('hide');
                    $('#btn-load-detail-report').addClass('hide');
                    $('#summary-details').removeClass('with-load-btn');
                    $('#summary-details').addClass('without-load-btn');
                    var id = $(e.currentTarget).attr('id');
                    if ($(e.currentTarget).attr('id') == 'summary-tabs') {
                        $('.dhx_grid').empty();
                        that.loadBtnClicked(dateRange);
                    }
                    else if ($(e.currentTarget).attr('id') == 'detail-tabs') {
                        $('.dhx_grid').empty();
                        that.loadDetailReport();
                    }
                }
            },

            // This function used to format date based on company date format
            formatDt: function (dt, dtabbr) {
                var yr = dt.substring(0, 4);
                var mth = dt.substring(4, 6);
                if (dtabbr == 'from') {
                    this.actstartdt = formattedDate(yr + '-' + mth + '-01', localStorage.getItem("cmpdfmt"));
                }
                else if (dtabbr == 'to') {
                    this.actenddt = formattedDate(yr + '-' + mth + '-' + lastday(yr, mth), localStorage.getItem("cmpdfmt"));
                }
            },

            //This Function used to fetch Activity Chart Report(Sent and Delivered Email) for Sales Rep,Distributor,AD and VP
            collateralActivityChart: function (input) {
                var that = this;
                that.chartDelivered = [];
                // fnGetWebServerData("emailtrack/fetchCollateralActivitityChartDetails", undefined, input, function (data) {

                //   });

                input.accessLvl = this.userAccessLvl;
                input.userId = this.userId;
                input.fromDate = this.actstartdt;
                input.toDate = this.actenddt;
                input.action = "activityChart";
                input.userDept = this.userDept;
                var updInput = JSON.stringify(input);
                var totalcount = 0;
                var deliveredcount = 0;
                var deliveredPercentageVal = 0;
                var totalPercentage = 0;
                var deliveredPercentage = 0 + '%';
                // this.getLoader("#activity-chart-container");
                fnGetWebServerviceCloudData(Azure_Cloud_MC_URL + "emailtrack/fetchcollateraldtls", updInput, function (data, response) {
                    console.log(data);
                    if (response == "error") {
                        showError(lblNm("msg_try_after"));
                    }
                    else {

                        if (data != undefined && data != null) {
                            var findDeliverCount =  _.findWhere(data, {label: "DELIVERED"});
                            var findProcessedCount =  _.findWhere(data, {label: "PROCESSED"});
                            if (findDeliverCount != undefined) {
                                deliveredcount = findDeliverCount.value1; 
                            }
                            if (findProcessedCount != undefined) {
                                totalcount = findProcessedCount.value1;   
                            }
                            that.totallinkssend = totalcount;
                            that.deliveredcount = deliveredcount;
                            deliveredPercentageVal = (deliveredcount / totalcount * 100);
                            deliveredPercentageVal = deliveredPercentageVal.toFixed(2);
                            totalPercentage = 100 - deliveredPercentageVal;
                            if (deliveredPercentageVal != 'NaN') {
                                deliveredPercentage = Math.ceil(deliveredPercentageVal) + '%';
                            }
                            else {
                                deliveredPercentage = '0' + '%';
                            }
                        }
                        // var label1 = "Email Delivered"
                        // var label2 = "% Delivered ";
                        var totalsentdata = {
                            "Label": "Total Sent",
                            "Value": totalPercentage,
                            "color": "#1e90ff"
                        }
                        that.chartDelivered.push(totalsentdata);
                        var delivereddata = {
                            "Label": "Delivered",
                            "Value": deliveredPercentageVal,
                            "color": "#FFA500"
                        }
                        that.chartDelivered.push(delivereddata);
                        var arrChartData = {
                            "title": "Emails Delivered %",
                            "label1": "Total Emails Sent",
                            "count1": totalcount,
                            "label2": "Total Emails Delivered",
                            "count2": deliveredcount,
                            "percentage": deliveredPercentage

                        };
                        // that.loadChart(that.chartDelivered,'activity-chart-container',totalcount,label1,deliveredcount,label2,deliveredPercentage);
                        $("#activity-chart-container").html(that.template_widget(arrChartData));
                        that.collateralOpenedChart(input);
                    }
                });

            },

            //This Function used to fetch Open Chart Report(Open and Unique Open Email) for Sales Rep,Distributor,AD and VP
            collateralOpenedChart: function (input) {
                var that = this;
                that.chartOpens = [];
                input.accessLvl = this.userAccessLvl;
                input.userId = this.userId;
                input.fromDate = this.actstartdt;
                input.toDate = this.actenddt;
                input.action = "openChart";
                input.userDept = this.userDept;
                var updInput = JSON.stringify(input);

                var totalcount = 0;
                var uniquecount = 0;
                var deliveredcount = 0;
                var totalPercentage = 0;
                var uniquePercentageVal = 0;
                var uniquepercentage = 0 + '%';

                fnGetWebServerviceCloudData(Azure_Cloud_MC_URL + "emailtrack/fetchcollateraldtls", updInput, function (data, response) {
                    if (data != undefined) {
                        if (data.length > 0) {
                            totalcount = data[0].value1;
                            uniquecount = data[0].unique_cnt;
                            deliveredcount = that.deliveredcount;
                        }
                        uniquePercentageVal = (uniquecount / deliveredcount * 100);
                        uniquePercentageVal = uniquePercentageVal.toFixed(2);
                        totalPercentage = 100 - uniquePercentageVal;
                        if (uniquePercentageVal != 'NaN' && uniquePercentageVal != '0.00') {
                            uniquepercentage = Math.ceil(uniquePercentageVal) + '%';
                        }
                        else {
                            uniquepercentage = '0' + '%';
                        }
                    }
                    //   var label1 = "Unique Opens"
                    //   var label2 = "Unique Open Rate ";
                    var opendata = {
                        "Label": "Opens",
                        "Value": totalPercentage,
                        "color": "#FFA500"
                    }
                    that.chartOpens.push(opendata);
                    var uniquedata = {
                        "Label": "Unique Opens",
                        "Value": uniquePercentageVal,
                        "color": "#1e90ff"
                    }
                    that.chartOpens.push(uniquedata);
                    var arrChartData = {
                        "title": "Emails Opened %",
                        "label1": "Total Emails Delivered",
                        "count1": totalcount,
                        "label2": "Unique Opens",
                        "count2": uniquecount,
                        "percentage": uniquepercentage

                    };
                    //   that.loadChart(that.chartOpens,'open-chart-container',totalcount,label1,uniquecount,label2,uniquepercentage);
                    $("#open-chart-container").html(that.template_widget(arrChartData));
                    that.collateralClickedChart(input);
                });
            },

            //This Function used to fetch Click Chart Report(Click and Unique Click Email) for Sales Rep,Distributor,AD and VP
            collateralClickedChart: function (input) {
                var that = this;
                that.chartClicks = [];
                var totalcount = 0;
                var uniquecount = 0;
                var uniquepercentage = 0 + '%';
                var uniquePercentageVal = 0;
                var totalPercentage = 0;

                input.accessLvl = this.userAccessLvl;
                input.userId = this.userId;
                input.fromDate = this.actstartdt;
                input.toDate = this.actenddt;
                input.action = "clickChart";
                input.userDept = this.userDept;
                var updInput = JSON.stringify(input);
                fnGetWebServerviceCloudData(Azure_Cloud_MC_URL + "emailtrack/fetchcollateraldtls", updInput, function (data, response) {
                    if (data != undefined) {
                        if (data.length > 0) {
                            totalcount = parseInt(data[0].value1);
                            uniquecount = parseInt(data[0].unique_cnt);
                        }

                        if (uniquecount != 0) {
                            uniquePercentageVal = (uniquecount / totalcount * 100);
                            uniquePercentageVal = uniquePercentageVal.toFixed(2);
                            totalPercentage = 100 - uniquePercentageVal;
                        }
                        if (uniquePercentageVal != 'NaN' && uniquePercentageVal != '0.00') {
                            uniquepercentage = Math.ceil(uniquePercentageVal) + '%';
                        }
                        else {
                            uniquepercentage = '0' + '%';
                        }
                    }
                    var clickdata = {
                        "Label": "Clicks",
                        "Value": totalPercentage,
                        "color": "#FFA500"
                    }
                    that.chartClicks.push(clickdata);
                    var uniqueclick = {
                        "Label": "Unique Clicks",
                        "Value": uniquePercentageVal,
                        "color": "#1e90ff"
                    }
                    that.chartClicks.push(uniqueclick);
                    var arrChartData = {
                        "title": "Links Clicked %",
                        "label1": "Total Links Sent",
                        "count1": totalcount,
                        "label2": "Unique Clicks",
                        "count2": uniquecount,
                        "percentage": uniquepercentage

                    };

                    //  that.loadChart(that.chartClicks,'click-chart-container',totalcount,label1,uniquecount,label2,uniquepercentage);
                    $("#click-chart-container").html(that.template_widget(arrChartData));


                    that.fetchCollateralFrequentContacts(input, function (data) {
                        $('#most-frequent-div').html(that.template_mostfrequent(data));
                    });
                    that.fetchCollateralFrequentCollateral(input, function (data) {
                        $('#most-frequent-div').html(that.template_mostfrequent(data));
                    });
                    that.fetchCollateralMostSharedContent(input, function (data) {
                        $('#most-frequent-div').html(that.template_mostfrequent(data));
                    });
                });
            },

            // This Function is used to load fusion chart for Activity, Open and Click for Email
            loadChart: function (data, divid, count, label1, count1, label2, percentage) {
                var that = this;
                var charWidth = $("#div-col-email-track-view .activity-chart").width();
                var res = [];
                var chartOptions = {
                    "bgColor": "#ffffff",
                    "aligncaptionwithcanvas": "0",
                    "captionpadding": "0",
                    "decimals": "0",
                    "showLabels": "0",
                    "theme": "fusion",
                    "showValues": "0",
                    "enableSmartLabels": "0",
                    "use3DLighting": "0",
                    "startingAngle": "90",
                    "defaultCenterLabel": " " + count
                }

                FusionCharts.ready(function () {
                    var myChart = new FusionCharts({
                        type: "doughnut2d",
                        renderAt: divid,
                        dataFormat: "json",
                        width: charWidth,
                        height: "290",
                        dataSource: {
                            "chart": chartOptions,
                            "data": data
                        },
                    }).render();
                    $("#" + divid + ' .fusioncharts-container').append("<div class='percentage-div'><div class=''><span>" + label1 + "</span><span style='float: right'>" + count1 + "</span></div><div><span>" + label2 + "</span><span style='float: right'>" + percentage + "</span></div></div>");

                });
            },

            //on Load button clicked the function called and it loads the data as given filter date range
            loadBtnClicked: function () {
                var that = this;
                this.getDateInputs(function (fromDt, toDt) {
                    if (fromDt.length == 6 && toDt.length == 6) {
                        if (fromDt <= toDt) {
                            var input = {
                                "accessLvl": that.userAccessLvl,
                                "userId": that.userId,
                                "fromDate": that.actstartdt,
                                "toDate": that.actenddt,
                                "action": "",
                                "userDept" : that.userDept

                            };
                            that.collateralActivityChart(input);
                        } else
                            showError("Please Select From Date is Less than To Date");
                    }
                    else
                        showError("From Date or To Date may be Empty, Please Select the Date Fields");

                });
            },

            //on Load button clicked the function called and it loads the data as given filter date range
            loadDetailReport: function () {
                var that = this;
                this.getDateInputs(function (fromDt, toDt) {
                    if (fromDt.length == 6 && toDt.length == 6) {
                        if (fromDt <= toDt) {
                            var input = that.detailsInput;
                            input["fromDate"] = that.actstartdt;
                            input["toDate"] = that.actenddt;
                            that.loadEmailTrackDetailReport(input);
                        } else
                            showError("Please Select From Date is Less than To Date");
                    }
                    else
                        showError("From Date or To Date may be Empty, Please Select the Date Fields");

                });
            },

            //loadEmailTrackDetailReport - function used to show Email Track Detailed View Report in Grid by AD, by Fieldsales
            loadEmailTrackDetailReport: function (input) {

                var that = this;
                input.accessLvl = this.userAccessLvl;
                input.userId = this.userId;
                input.fromDate = this.actstartdt;
                input.toDate = this.actenddt;
                input.userDept = this.userDept;
                input.action = "emailtrackdetails";
                var updInput = JSON.stringify(input);
                fnGetWebServerviceCloudData(Azure_Cloud_MC_URL + "emailtrack/fetchcollateraldtls", updInput, function (data, response) {
                    $('#email-track-detail-report').empty();
                    $('#email-track-detail-report').addClass('hide');
                    $('#noData').removeClass('hide');
                    if (!data == "" || !data == undefined || !data == null) {

                        $('#email-track-detail-report').removeClass('hide');
                        $('#noData').addClass('hide');

                        var grid = new dhx.Grid("email-track-detail-report", {

                            columns: [


                                { width: 300, id: "recipient", header: [{ text: "Recipient", css: "title" }, { content: "inputFilter" }], sortable: true, type: "string" },
                                // { width: 200, id: "content_title", header: [{ text:  "Content Title",css:"title"} , { content: "inputFilter" }] ,sortable: true,type:"string"},
                                {
                                    width: 350, id: "links", header: [{ text: "Links", css: "title" }, { content: "inputFilter" }], sortable: true,
                                    htmlEnable: true,
                                    template: function (text, row, col) {
                                        // return "<a id='col-link' title='"+data[0].URL+"' target=_blank href='"+data[0].URL+"'>"+ text +"</a>";
                                        return "<a id='col-links'>" + text + "</a>";

                                    }
                                },
                                {
                                    width: 50, id: "total_opens", header: [{ text: "Total Opens", css: "wrap", align: "center" }, { content: "inputFilter" }], sortable: true,
                                    htmlEnable: true,
                                    template: function (text, row, col) {
                                        if (text > 0) {
                                            return "<a id='col-link'>" + text + "</a>";
                                        }
                                        else {
                                            return text;
                                        }

                                    }
                                },
                                {
                                    width: 50, id: "total_clicks", header: [{ text: "Total Clicks", css: "wrap", align: "center" }, { content: "inputFilter" }], sortable: true,
                                    htmlEnable: true,
                                    template: function (text, row, col) {
                                        if (text > 0) {
                                            return "<a id='col-link'>" + text + "</a>";
                                        }
                                        else {
                                            return text;
                                        }

                                    }
                                },
                                { width: 80, id: "status", header: [{ text: "Status", css: "title" }, { content: "inputFilter" }], sortable: true, type: "string" },
                                { width: 180, id: "status_time", header: [{ text: "Status Time", css: "title" }, { content: "inputFilter" }], sortable: true, type: "string" },
                                { width: 0, id: "refid", header: [{ text: "refid" }], sortable: true, hidden: true, type: "string" },
                                { width: 0, id: "event_type", header: [{ text: "EVENT_TYPE" }], sortable: true, hidden: true, type: "string" },
                                { width: 0, id: "emailid", header: [{ text: "emailid" }], sortable: true, hidden: true, type: "string" },
                            ],

                            height: 570,
                            //            	            resizable: true, 
                            //                            autoWidth: true,
                            tooltip: true,
                            data: data

                        });
                        // to enable multi line
                        //grid.enableMultiline(true);

                        grid.events.on("CellClick", function (row, column, e) {
                            if (column.id === "total_opens" || column.id === "total_clicks") {
                                if (column.id === "total_opens") {
                                    var eventtype = "open";     //Opens 110697
                                }
                                else {
                                    var eventtype = "click";     //Clicks 110696
                                }
                                var input = {
                                    "eventtype": eventtype,
                                    "strurl": row.url,
                                    "accessLvl": that.userAccessLvl,
                                    "userId": that.userId,
                                    "fromDate": that.actstartdt,
                                    "toDate": that.actenddt,
                                    "action": "eventtimestamp",
                                    "userDept" : that.userDept,
                                    "emailid" : row.emailid,
                                    "refid" : row.refid,
                                };
                                showTimeStampPopup();
                                $("#div-crm-overlay-content-container").find('.modal-title').text("");
                                $("#div-crm-overlay-content-container .modal-body").html(that.template_detail_timestamp());
                                that.loadTimeStamp(input);

                            }
                            if (column.id === "links") {
                                window.open(row.url, '_blank');
                            }

                        });
                    }
                    else {
                        $('#email-track-detail-report').empty();
                        $('#email-track-detail-report').addClass('hide');
                        $('#noData').removeClass('hide');
                        $('#noData').html("No Data Found");
                    }
                });
            },

            //loadTimeStamp - function to show TimeStamp in Popup when Opens/Clicks count clicked on Email Track Detailed View Report
            loadTimeStamp: function (input) {
                var that = this;
                console.log(input);
                var updInput = JSON.stringify(input);
                //fnGetWebServerData("emailtrack/fetchEventTimeStamp", "GmCollateralEmailTrackVO", input, function (data) {
                    fnGetWebServerviceCloudData(Azure_Cloud_MC_URL + "emailtrack/fetchcollateraldtls", updInput, function (data, response) {
                    if (!data == "" || !data == undefined || !data == null) {
                        var decsOrderData = data.sort(function(a, b) { return a.timeval < b.timeval ? 1 : -1; });
                        
                        var grid = new dhx.Grid("div-time-stamp", {

                            columns: [{ width: 251, id: "TIMESTAMP", header: [{ text: " ", css: "title" }], type: "string" }],

                            height: 300,
                            resizable: true,
                            width: 251,
                            tooltip: true,
                            data: decsOrderData

                        });
                    }
                });
            },
            //This Function is used to fetch top 5 share receipients
            fetchCollateralFrequentContacts: function (input, callback) {
                var that = this;
                input.accessLvl = this.userAccessLvl;
                input.userId = this.userId;
                input.fromDate = this.actstartdt;
                input.toDate = this.actenddt;
                input.userDept = this.userDept;
                input.action = "frequentContacts";
                var updInput = JSON.stringify(input);
                fnGetWebServerviceCloudData(Azure_Cloud_MC_URL + "emailtrack/fetchcollateraldtls", updInput, function (data, response) {
                    if (data != undefined && data != null) {
                        var topRecords = data.sort(function(a, b) { return a.openedcnt < b.openedcnt ? 1 : -1; }).slice(0, 5);
                        that.resultJson.arrFrequentContacts = topRecords;
                    }
                    else {
                        that.resultJson.arrFrequentContacts = null;
                    }
                    callback(that.resultJson);

                });
            },
            //This Function is used to fetch top 5 shared Collateral
            fetchCollateralFrequentCollateral: function (input, callback) {
                var that = this;

                input.accessLvl = this.userAccessLvl;
                input.userId = this.userId;
                input.fromDate = this.actstartdt;
                input.toDate = this.actenddt;
                input.userDept = this.userDept;
                input.action = "frequentDoc";
                var updInput = JSON.stringify(input);
                fnGetWebServerviceCloudData(Azure_Cloud_MC_URL + "emailtrack/fetchcollateraldtls", updInput, function (data, response) {
                    if (data != undefined && data != null) {
                        var topRecords = data.sort(function(a, b) { return a.total_sends < b.total_sends ? 1 : -1; }).slice(0, 10);
                        that.resultJson.arrFrequentCollateral = topRecords;
                    }
                    else {
                        that.resultJson.arrFrequentCollateral = null;
                    }
                    callback(that.resultJson);

                });
            },
            //This Function is used to fetch Most shared contents
            fetchCollateralMostSharedContent: function (input, callback) {
                var that = this;

                input.accessLvl = this.userAccessLvl;
                input.userId = this.userId;
                input.fromDate = this.actstartdt;
                input.toDate = this.actenddt;
                input.userDept = this.userDept;
                input.action = "globalFrequentDoc";
                var updInput = JSON.stringify(input);
                fnGetWebServerviceCloudData(Azure_Cloud_MC_URL + "emailtrack/fetchcollateraldtls", updInput, function (data, response) {
                    console.log(data);
                    if (data != undefined && data != null) {
                        var topRecords = data.sort(function(a, b) { return a.total_sends < b.total_sends ? 1 : -1; }).slice(0, 10);
                        that.resultJson.arrMostSharedContent = topRecords;
                    }
                    else {
                        that.resultJson.arrMostSharedContent = null;
                    }

                    callback(that.resultJson);
                });
            },
            clearDetailReport: function () {
                // var that = this;
                // clearInterval(that.interval);
                // $('.dhx_grid-body').empty();

            },
            //Load Spinner
            getLoader: function (divElem) {
                var template = fnGetTemplate(URL_Common_Template, "gmtSpinner");
                $(divElem).html(template());
            },
            redirectToSystem: function (e) {
                $('#email-track-detail-report').empty();
                $('.dhx_grid').empty();
                var refid = $(e.currentTarget).attr('data-refid');
                var URL = '#collateral/detail/system/' + refid;
                GM.Global.EmailTrackFL = "Y";
                window.location.href = URL;
            }

        });

        return CollateralEmailTrackView;

    });

//function to show Timestamp in Popup when Opens/Clicks count clicked in Email Track Detailed View Report
function showTimeStampPopup() {
    var that = this;
    $('.div-overlay').show();
    $('.div-overlay').addClass('timeStampPopUPMain');
    $('#email-track-detail-report .dhx_header-wrapper').attr('style', 'z-index:0');
    $('.div-crm-overlay-content-container').addClass('hide');
    $('#div-crm-overlay-content-container').addClass('timeStampPopUP');
    $('#div-crm-overlay-content-container').removeClass('hide');
    if ($('#div-crm-overlay-content-container .modal-footer').hasClass("hide"))
        $('#div-crm-overlay-content-container .modal-footer').removeClass('hide');

    $("#div-crm-overlay-content-container .modal-footer button").each(function (i) {
        if (!$(this).hasClass("hide"))
            $(this).addClass("hide");
        $('.div-overlay').removeClass('timeStampPopUPMain');
    });

    $('#div-crm-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hideTimeStampPopup();
            $('.div-overlay').removeClass('timeStampPopUPMain');
        });
    });
}

//On clicking the close icon to hide the popup 
function hideTimeStampPopup() {
    $('.div-overlay').hide();
    $('#email-track-detail-report .dhx_header-wrapper').attr('style', '');
    $('#div-crm-overlay-content-container').removeClass('timeStampPopUP');
    $('#div-crm-overlay-content-container').removeClass('timeStampPopUP');
    $(".load-model").removeClass("hide");
    $('.div-crm-overlay-content-container').addClass('hide');
    $('.hideDefaultBtn').addClass('hide');
    $("#div-crm-overlay-msg").addClass('hide');
}

