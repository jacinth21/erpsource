define([
        'canvas','jquery', 'underscore', 'backbone', 'handlebars','threesixty',
        'global',
],

function (Canvas, $, _, Backbone, Handlebars, ThreeSixty, Global) {

	'use strict';
	
	var ThreeSixtyView = Backbone.View.extend({
		
		events: {
			"click #div-threesixty-close" : "close"
		},

		el: "#div-image-viewer-background",

		template: fnGetTemplate(URL_Collateral_Template,"GmThreeSixty"),
	
	    initialize: function() {
	    	$("#div-image-viewer-background").show();
	    	this.render();
	    },
	    
	    //render threesixty degree view
		render: function () {
			
			//var url="globusMobileApp/image/img/Monument/GMANM23_0114_MONUMENT_Overview_";
			var url="globusMobileApp/image/img/Monument_0108/Monument"
	 		
			this.$el.html(this.template());
	 
			addSpinner();
		    loadImage(url);
			return this;
		},
		
	    //Close the View
	    close: function(event) {
	    	this.unbind();
	    	this.undelegateEvents();
	    	this.$el.unbind();
	    	this.$el.hide();
	    	this.$el.empty();
	    }
			
		});
	
		return ThreeSixtyView;
});
