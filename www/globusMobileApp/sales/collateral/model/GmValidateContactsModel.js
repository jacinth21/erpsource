/**********************************************************************************
 * File:        GmValidateContactsModel.js
 * Description: Contacts Model
 
 **********************************************************************************/
define([
    'underscore', 'backbone',
], 

function (_, Backbone) {
    
    'use strict';
    var Contacts = Backbone.Model.extend({

        //Validate the username and password
        validate: function(attrs) {

            var errors = this.errors = {};
            if (!attrs.contacttype) errors.contacttype = lblNm("vld_contact_type");
            if (!attrs.fname) errors.fname = lblNm("vld_first_name");
            if (!attrs.lname) errors.lname =lblNm("vld_last_name");
            if (!attrs.emailid) errors.emailid = lblNm("vld_email_id");
            if (!attrs.phoneno) errors.phoneno = lblNm("vld_phone_no");
            if (!_.isEmpty(errors)) return errors;
          }
      });

  return Contacts;
});