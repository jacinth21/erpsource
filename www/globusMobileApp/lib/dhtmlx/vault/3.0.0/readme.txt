/*
@license

dhtmlxVault v.3.0.0 Professional
This software is covered by Enterprise License. Usage without proper license is prohibited.

(c) Dinamenta, UAB.
*/


Useful links
-------------

- Online  documentation
	https://docs.dhtmlx.com/