/**********************************************************************************
 * File:        GmDORouter.js
 * Description: Router specifically meant to route views within the 
 *              DO application
 * Version:     1.0
 * Author:      tvenkateswarlu
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',
        'pricemainview'
         
      ], 
    
    function ($, _, Backbone, PriceMainView) {

	'use strict';

	var PriceRouter = Backbone.Router.extend({

		routes: {
			"price": "price",
			"price/:viewType": "price",
			"price/:viewType/:priceRequestId": "price"
		},

		price: function(viewType,priceRequestId){           
			this.closeView(pricemainview);
            
            var input = {
                    "refid": priceRequestId,
                    "type": "26230731"

                };
                fnGetWebServerData("fileUpload/fetchFileList/", "gmCommonFileUploadVO", input, function (data) {
                    console.log("GM.Global.documentData"+data)
                    GM.Global.documentData = data;
                });
             var input = {
                        "token": localStorage.getItem("token"),
                        "partyid":localStorage.getItem("partyID") 
                    };
                fnGetWebServerData("pricingApproval/fetchAccessPermissions", "gmCodeLookUpVO", input, function (data) {
                    console.log("priceapproval")
                    console.log(data)
                    localStorage.setItem("pr_apprvl_access",data.pr_apprvl_access)
                    localStorage.setItem("pr_delete_access",data.pr_delete_access)
                    localStorage.setItem("pr_denied_access",data.pr_denied_access)
                    localStorage.setItem("pr_gen_pricefile",data.pr_gen_pricefile)
                    localStorage.setItem("pr_status_upd_access",data.pr_status_upd_access);
                    localStorage.setItem("pr_pc_access",data.pr_pc_access);
                        });
			var option = [];
			option.push(viewType);
			option.push(priceRequestId);
			pricemainview = new PriceMainView(option);
			$("#div-main").html(pricemainview.el);  
		},
		
		
		closeView: function(view) {
			if(view!=undefined) {
				view.unbind();
				view.remove();
				view = undefined;
				$("#btn-back").unbind();
			}

		}
	});

	return PriceRouter;
});