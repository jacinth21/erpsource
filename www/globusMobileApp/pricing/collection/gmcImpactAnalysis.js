/**********************************************************************************
 * File:        gmcImpactAnalysis.js
 * Description: Impact analyis screen
 * Version:     1.0
 * Author:     	matt b
 **********************************************************************************/
define([
    'underscore',
    'backbone',
    'gmmImpactAnalysis'
],

    function (_, Backbone, GMMImpactAnalysis) {

        'use strict';

        var ImpactAnlyCollection = Backbone.Collection.extend({
            model: GMMImpactAnalysis
        });

        return ImpactAnlyCollection;
    });
