/**********************************************************************************
 * File:        gmcSystem.js
 * Description: Price Initiate Screen
 * Version:     1.0
 * Author:     	treddy
 **********************************************************************************/
define([
    'underscore',
    'backbone',
    'gmmSystem'
], 

function (_, Backbone, GMMSystem) {

	'use strict';

	var SystemCollection = Backbone.Collection.extend({
		model: GMMSystem
	});

	return SystemCollection;
});