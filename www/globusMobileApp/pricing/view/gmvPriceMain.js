/**********************************************************************************
 * File:        GmvPriceMain.js
 * Description: Price Initiate Screen
 * Version:     1.0
 * Author:     	treddy
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',  'handlebars',
        'global','device','notification', 'pricemainview', 'dropdownview','searchview',	    
	    'priceStatusReport','priceRequest','gmtTemplate','pricingApproval'
        ], 

        function ($, _, Backbone, Handlebars, Global, Device, Notification, PriceMainView, DropDownView, SearchView,				
				  PriceStatusReport,PriceRequest,GMTTemplate,pricingApproval) {

	'use strict';
    var tabid;
	var PriceMainView = Backbone.View.extend({
        
        events : {	
          "click .btn-detail-navigation":"tabChange"
         },
        
		/* Load the templates */ 
		templatePrice: fnGetTemplate(URL_Price_Template, "gmtPrice"),
        tempalate_overlay: fnGetTemplate(URL_Price_Template, "gmtPricingOverlay"),
        
        /*Initialize*/
		initialize: function(options) {
			$("body").removeClass();
            if(GM.Global.Browser)
                $("body").addClass("browser");
			$("#btn-home").show();
            $("#btn-back").show();            
            this.render(options);
		},

        // Render the initial contents when the Catalog View is instantiated
		render: function (options) {
			this.$el.html(this.templatePrice());
		      $(".div-pricing-overlay").html(this.tempalate_overlay());
			this.strViewType = options[0];
			if(!this.strViewType) {
				this.strViewType = "initiatePricing";	
		  	}
			switch(this.strViewType) {
					case "initiatePricing":							    
						this.priceRequest = new PriceRequest({'el' :this.$('#div-price-view'), 'parent' : this, 'viewOption': this.strViewType});
						$("#div-app-title").html("Pricing Request");
					    this.display("initiate");
					break;
					case "pricingSubmittedRequests":							    
						this.priceRequest = new PriceRequest({'el' :this.$('#div-price-view'), 'parent' : this, 'viewOption': this.strViewType, 'priceRequestId': options[1]});
						$("#div-app-title").html("Pricing Request");
					    this.display("submit-req");
					break;
					case "report":					   
						this.priceStatusReport = new PriceStatusReport({'el' :this.$('#div-price-view'), 'parent' : this});
						$("#div-app-title").html("Pricing Report");
					    this.display("report");					
					break;	
                case "approval":					   
						this.pricingApproval = new pricingApproval({'el' :this.$('#div-price-view'), 'parent' : this,'reqid':options[1]});
						$("#div-app-title").html("Pricing Approval");
					    this.display("report");					
					break;					
			}
			return this;
		},
        
//        tabChange: function(e) {
//            showNativeConfirm("Do you want to Change Screen..?","Confirm",["Yes","No"], function(idx) {
//                if (idx == 1) {           
//                    e.preventDefault();
//                }
//            });
//        },
		
		// Toggles the view
		display: function(key) {

			if(key.currentTarget==undefined) {
				this.key = key;
			}
			else {
				this.key = "overview";
				
			}
			this.$(".btn-detail").each(function() {
				$(this).removeClass("btn-selected");
			});

			this.$("#btn-price-" + this.key).show();
			this.$("#btn-price-" + this.key).addClass("btn-selected");

		}
                                
	});	

	return PriceMainView;
});
