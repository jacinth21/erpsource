/**********************************************************************************
 * File:        GmvPricingApproval.js
 * Description: Pricing Approval  Screen
 * Version:     1.0
 * Author:     	Akumar
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone', 'handlebars',
        'global', 'device', 'notification', 'dropdownview', 'searchview', 'gmtTemplate', 'loaderview', 'gmvCRMSelectPopup', 'pricingCommon', 'gmcFileUpload'
        ],

    function ($, _, Backbone, Handlebars,
        Global, Device, Notification, DropDownView, SearchView, GMTTemplate, LoaderView, GMVCRMSelectPopup, pricingCommon, GMCFileUpload
    ) {

        'use strict';
        var tabid;
        var pricingApproval = Backbone.View.extend({

            events: {
                "click #fa-times-tick": "openApproval",
                "click .div-p-icon": "showGroupPartPricingDetails",
                "click #fa-price-circle,.div-price-approval-systemname": "collapsePrice",
                "change #price-approval-prop-unit input": "calcChangeExtPrice",
                "click .div-price-approval-btn-questions": "showThresholdQuestions",
                "click .div-price-aproval-account-val": "showPriceAccountDetails",
                "click .ul-approval-status": "checkValue",
                "click .price-approval-collapse": "collapseApproval",
                "change .div-price-approval-ul .ul-approval-status": "checkValidations",
                "change .ul-approval-changetype": "calcCustom",
                "change #price-approval-changevalue input": "calcOfflist",
                "click .price-approval-groupname": "fnGetGroupFullName",
                "click #div-price-approval-save": "saveApproval",
                "click #div-price-approval-cancel": "cancelApproval",
                "change .ul-approval-changetype , .price-approval-change input": "updateString",
                "change .select-price-aproval-system-main-val": "updateSystemStatus",
                "click .div-price-approval-btn-impact": "calcImpactAnaly",
                "click .div-price-approval-btn-document": "showFileUploadPopup",
                "keyup .div-price-approval-content-data input": "validateNumber",
                "click .div-price-approval-sdata select,.div-price-approval-sdata input,.div-p-icon": "highlightSelected",
                "change .select-price-aproval-system-master-val": "masterStatusSelect",
            },

            /* Load the templates */
            //            templateGroupPartPricingDetails: fnGetTemplate(URL_Price_Template, "gmtGroupPartPricingDetails"),
            template_approval_system: fnGetTemplate(URL_Price_Template, "gmtPricingApprovalSystemData"),
            template_approval: fnGetTemplate(URL_Price_Template, "gmtPricingApprovalData"),
            templatePriceThresholdQusetions: fnGetTemplate(URL_Price_Template, "gmtThresholdQuestions"),
            templatePriceThresholdQusetionsPhone: fnGetTemplate(URL_Price_Template, "gmtThresholdQuestionsPhone"),
            templatePriceAccountDetails: fnGetTemplate(URL_Price_Template, "gmtPriceAccountDetails"),
            //            templateShowName: fnGetTemplate(URL_Price_Template, "gmtShowName"),
            templatePriceReport: fnGetTemplate(URL_Price_Template, "gmtPriceReport"),

            initialize: function (options) {
                this.reqid = options.reqid;
                this.el = options.el;
                localStorage.setItem("pricelastupdatedsystem","");
                this.render();
            },

            render: function () {
                this.gmcFileUpload = new GMCFileUpload();
                GM.Global.saveApprovalData = "";
                this.loaderview = new LoaderView({
                    text: "Loading ...!"
                });
                var input = {
                    "token": localStorage.getItem("token"),
                    "codegrp": "PRQSTS"
                };
                fnGetWebServerData("search/codelookup", "gmCodeLookUpVO", input, function (data) {
                    data = arrayOf(data);
                    GM.Global.priceApprovalDpdn = [];
                    var Sdata = _.filter(data, function (model) {
                        return (model.codeid != 52122 && model.codeid != 52189 && model.codeid != 52190 && model.codeid != 52121 && model.codeid != 52188);
                    })
                    _.each(Sdata, function (dssata) {
                        GM.Global.priceApprovalDpdn.push({
                            "ID": dssata.codeid,
                            "Name": dssata.codenm
                        })
                    })
                });
                var input = {
                    "token": localStorage.getItem("token"),
                    "codegrp": "PCHTY"
                };
                fnGetWebServerData("search/codelookup", "gmCodeLookUpVO", input, function (datas) {
                    GM.Global.priceType = []
                    var dsata = arrayOf(datas);
                    _.each(dsata, function (dataa) {
                        GM.Global.priceType.push({
                            "ID": dataa.codeid,
                            "Name": dataa.codenm
                        })
                    })
                });
                var input = {
                    "pricerequestid": this.reqid,
                    "userid": localStorage.getItem("userID"),
                    "deptid": localStorage.getItem("deptid")
                };
                var that = this;
                //Approval scrren Fetch
                fnGetWebServerData("pricingApproval/fetchPriceApprovalDetails/", undefined, input, function (data) {
                    
                    console.log(data);
                    GM.Global.tData = data;
                    localStorage.setItem("advpaccfl", data.listgmPricingApprovalHeaderVO.advpaccfl);
                    data.listGmSystemVO = arrayOf(data.listGmSystemVO);
                    that.loaderview.close();
                    data.listGmSystemVO = _.each(data.listGmSystemVO, function (fata) {
                        fata.sys = fata.systemId.replace(".", "");
                    })
                    that.$el.html(that.template_approval_system(data.listGmSystemVO));
                    var input = {
                    "token": localStorage.getItem("token"),
                    "accid": data.listgmPricingApprovalHeaderVO.accountid,
                    "typeid": data.gmAccountPriceRequestVO.typeid,
                    "repid": localStorage.getItem("userID"),
                    "acid": data.listgmPricingApprovalHeaderVO.accountid,
                    "deptid": localStorage.getItem("deptid"),
                    "userid": localStorage.getItem("userID")
                };
                    //Approval scrren Accountname Fetch
                fnGetWebServerData("pricingRequest/accountName/", "gmPriceAccountTypeVO", input, function (data) {
                    if (data.accname != "" || accID == undefined) {
                        var input = {
                            "token": localStorage.getItem("token"),
                            "accid": $(".div-price-aproval-account-val").attr("name"),
                            "typeid": $(".div-price-aproval-account-val").attr("typeid")
                        };
                        //Approval scrren Account DEtails(GPB,GPO,IDN) Fetch
                        fnGetWebServerData("pricingRequest/accountDetailsList/", undefined, input, function (approvaldata) {
                            if (approvaldata != null) {
                    $(".div-price-gpo-val").html(approvaldata.gmAccountDetailsListVO.gpoaff);
                    $(".div-price-sales-account").html(approvaldata.gmAccountDetailsListVO.accountname+"("+approvaldata.gmAccountDetailsListVO.accid+")");
                    $(".div-price-idn-val").html(approvaldata.gmAccountDetailsListVO.idn);
                    $(".div-price-gpb-val").html(approvaldata.gmAccountDetailsListVO.gpopricebook);
                            } 
                        });
                    } 
                });
                    $(".approval-additional-info").hide();
                    if (GM.Global.documentData != null) {
                        GM.Global.documentData = arrayOf(GM.Global.documentData);
                        $(".document_cnt").html(" (" + GM.Global.documentData.length + ")");
                        $(".document_cnt").css("color", "red");
                    } else {
                        $(".document_cnt").html("");
                        $(".document_cnt").css("color", "black");
                    }
//                    if(localStorage.getItem("PriceApprovalAccountName") !="" && localStorage.getItem("PriceApprovalAccountName")!= undefined){$(".div-price-gpo-val").html(localStorage.getItem("PriceApprovalGpo"));
//                    $(".div-price-sales-account").html(localStorage.getItem("PriceApprovalAccountName")+"("+localStorage.getItem("PriceApprovalAccountID")+")");
//                    $(".div-price-idn-val").html(localStorage.getItem("PriceApprovalIdn"));
//                    $(".div-price-gpb-val").html(localStorage.getItem("PriceApprovalGpb"));
//                    }
                    GM.Global.PricingStatus = data.gmAccountPriceRequestVO.statusid;
                    $(".div-price-status-val").html(data.gmAccountPriceRequestVO.requestStatus)
                    $(".div-price-aproval-account-val").attr("typeid", data.gmAccountPriceRequestVO.typeid)
                    $(".li-price-reqid-val").html(data.listgmPricingApprovalHeaderVO.pricerequestid)
                    $(".div-price-initiated-val").html(data.listgmPricingApprovalHeaderVO.initiatedby)
                    $(".div-price-sales-val").html(data.listgmPricingApprovalHeaderVO.last12monthsales.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"))
                    $(".div-price-date-val").html(data.listgmPricingApprovalHeaderVO.initiateddate)
                    $(".div-price-aproval-account-val").attr("name", data.listgmPricingApprovalHeaderVO.accountid)
                    _.each(data.listGmSystemVO, function (sata,i) {
                        var mod = _.filter(data.listGmGroupVO, function (model) {
                            return (model.systemid == sata.systemId);
                        });
                        mod = _.sortBy(mod, 'setordcnt');
                        var grpConstructId = _.chain(mod).map(function (model) {
                            return model.systemname;
                        }).uniq().value();
                        _.each(grpConstructId, function (name) {
                            var len = _.filter(data.listGmGroupVO, function (model) {
                                return (model.systemname == name && model.systemid == sata.systemId);
                            });
                            if (len.length > 0)
                                $("#approval-" + sata.systemId.replace(".", "")).append(that.template_approval(len));
                            that.loadSelect();
                            _.each(data.listGmGroupVO, function (temp) {
                                if (temp.systemname == name && temp.systemid == sata.systemId) {
                                    $("." + temp.prcdetlid).find(".price-approval-type").val(temp.changetype)
                                    if (temp.changetype == "52020") {
                                        $("." + temp.prcdetlid).find("#price-approval-changevalue input").attr('disabled', 'disabled');
                                        $("." + temp.prcdetlid).find("#price-approval-prop-unit input").attr('disabled', false);
                                    } else {
                                        $("." + temp.prcdetlid).find("#price-approval-prop-unit input").attr('disabled', 'disabled');
                                        $("." + temp.prcdetlid).find("#price-approval-changevalue input").attr('disabled', false);
                                    }
                                    $("." + temp.prcdetlid).find(".ul-approval-status").val(temp.statusid)
                                    if (temp.statusid == "52125") {
                                        $("." + temp.prcdetlid).find("input").attr('disabled', 'disabled');
                                        $("." + temp.prcdetlid).find('select').attr('disabled', 'disabled');
                                    }

                                    $("." + temp.prcdetlid).find(".ul-price-approval-prop-ext").text((temp.quantity * temp.proposedunitprice))
                                    var listext = temp.quantity * temp.listprice;
                                    listext = listext.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
                                    $("." + temp.prcdetlid).find(".price-approval-listext").text(listext)
                                    if ((Math.round(((100 * (temp.listprice - temp.proposedunitprice) / temp.listprice) * 1000) / 10) / 100).toFixed(2) != "NaN")
                                        $("." + temp.prcdetlid).find(".price-approval-offlist").html((Math.round(((100 * (temp.listprice - temp.proposedunitprice) / temp.listprice) * 1000) / 10) / 100).toFixed(2))
                                    if (eval($("." + temp.prcdetlid).find("#price-approval-prop-unit input").val().replace(",", "")) < eval($("." + temp.prcdetlid).find(".price-approval-tripwire").text().replace(",", ""))) {
                                        $("." + temp.prcdetlid).find(".ul-price-approval-color").removeClass("green").addClass("red")
                                    } else {
                                        $("." + temp.prcdetlid).find(".ul-price-approval-color").removeClass("red").addClass("green")
                                    }
                                }
                            });
                            that.calcConstructPrice();
                            if (data.listgmPricingApprovalHeaderVO.statusid == "52189" || data.listgmPricingApprovalHeaderVO.statusid == "52190") {
                                $('.ul-approval-status').attr('disabled', 'disabled');
                                $('.ul-approval-changetype').attr('disabled', 'disabled');
                                $(".div-price-approval-ul input").attr('disabled', 'disabled');
                                $(".div-price-approval-save").hide();
                            }
                            $(".div-price-approval-collapse").hide();
//                            $("#"+localStorage.getItem("pricelastupdatedsystem")).trigger("click");
                        });
                        if(data.listGmSystemVO.length == i+1 ){
                           $("#"+localStorage.getItem("pricelastupdatedsystem")).trigger("click"); 
                            
                        }
                            
                    })
                });
                
                
            },
            showGroupPartPricingDetails: function (e) {
                showGroupPartPricingDetails(e);
            },
            updateSystemStatus: function (e) {
                var that = this;
                setTimeout(function () {
                    if (localStorage.getItem("pr_status_upd_access") != "Y") {
                        e.preventDefault();
                        $(e.currentTarget).val(GM.Global.previousValue);
                        showError("No Status Update Access");
                        $('.ul-approval-status').attr('disabled', 'disabled');
                        $('.ul-approval-changetype').attr('disabled', 'disabled');
                        $(".div-price-approval-ul input").attr('disabled', 'disabled');

                    } else if (GM.Global.previousValue == "52123") {
                        if (localStorage.getItem("pr_pc_access") != "Y") {
                            showError("Not Authorised..");
                            $(e.currentTarget).val(GM.Global.previousValue);
                        } else {
                            that.implementValue(e);
                        }
                    } else {
                        if ($(e.currentTarget).val() == "52125") {
                            if (localStorage.getItem("pr_apprvl_access") != "Y") {
                                showError("Not Authorised..");
                                $(e.currentTarget).val(GM.Global.previousValue);
                            } else {
                                that.implementValue(e);
                            }

                        } else if ($(e.currentTarget).val() == "52120") {

                            if (localStorage.getItem("pr_denied_access") != "Y") {
                                showError("Not Authorised..");

                                $(e.currentTarget).val(GM.Global.previousValue);

                            } else {
                                that.implementValue(e);
                            }
                        } else if ($(e.currentTarget).val() == "52187") {

                            if (localStorage.getItem("advpaccfl") != "Y") {
                                showError("Not Authorised..");

                                $(e.currentTarget).val(GM.Global.previousValue);

                            } else {
                                that.implementValue(e);
                            }
                        } else {
                            that.implementValue(e);
                        }
                    }
                }, 400);
            },
            implementValue: function (e) {
                _.each($(e.currentTarget).parent().parent().next().find(".ul-approval-status"), function (data) {

                    GM.Global.approvedValue = _.filter(GM.Global.tData.listGmGroupVO, function (model) {
                        return (model.prcdetlid == $(data).parent().attr("prcdetlid"));
                    })
                    if ($(data).val() != null) {
                        if (GM.Global.approvedValue[0].statusid != "52125" && $(data).val() != "52123")
                            $(data).val($(e.currentTarget).val()).trigger("change");
                        else if ($(data).val() == "52123") {
                            if (localStorage.getItem("pr_pc_access") == "Y")
                                $(data).val($(e.currentTarget).val()).trigger("change");
                        }
                    }
                });
            },
            collapsePrice: function (e) {
                $(".div-price-approval-collapse").hide();
                if ($(e.currentTarget).hasClass("div-price-approval-systemname"))
                    e.currentTarget = $(e.currentTarget).prev().find("i");
                if ($(e.currentTarget).hasClass("fa-plus-circle")) {
                    $(".div-price-approval-sdata.header").addClass("hide");
                    $(e.currentTarget).parent().parent().parent().removeClass("hide")
                    $(".fa-minus-circle").removeClass("fa-minus-circle").addClass("fa-plus-circle");
                    $(e.currentTarget).removeClass("fa-plus-circle").addClass("fa-minus-circle").css("color:green");
                    $(e.currentTarget).parent().parent().parent().parent().find("#approval-" + $(e.currentTarget).attr("name")).show();
                } else {
                    $(".div-price-approval-sdata.header").removeClass("hide");
                    $(e.currentTarget).removeClass("fa-minus-circle").addClass("fa-plus-circle").css("color:red");
                    $(e.currentTarget).parent().parent().parent().parent().find("#approval-" + $(e.currentTarget).attr("name")).hide();
                }
            },
            calcCustom: function (e) {
                if ($(e.currentTarget).val() == "52020") {
                    $(e.currentTarget).parent().find("#price-approval-changevalue input").val("");
                    $(e.currentTarget).parent().find("#price-approval-changevalue input").attr('disabled', 'disabled');
                    $(e.currentTarget).parent().find("#price-approval-prop-unit input").attr('disabled', false);
                } else {
                    $(e.currentTarget).parent().find("#price-approval-prop-unit input").attr('disabled', 'disabled');
                    
                    $(e.currentTarget).parent().find("#price-approval-changevalue input").attr('disabled', false);
                }

            },
            calcOfflist: function (e) {
                if ($(e.currentTarget).val() != "" && $(e.currentTarget).val() >= 100) {
                    showError("Please Enter %Off List Value less than 100");
                    $(e.currentTarget).val("");
                } else
                if ($(e.currentTarget).parent().parent().find(".ul-approval-changetype").val() == "52021") {
                    $(e.currentTarget).parent().parent().find("#price-approval-prop-unit input").val(Math.round($(e.currentTarget).parent().parent().find(".price-approval-listunit").text().replace(",", "") - (($(e.currentTarget).parent().parent().find(".price-approval-listunit").text().replace(",", "") * $(e.currentTarget).parent().parent().find("#price-approval-changevalue input").val().replace(",", "")) / 100)), 2).trigger("change");
                }
            },
            saveApproval: function () {
                var that = this;
                var input = {
                    "token": localStorage.getItem("token"),
                    "inputstr": GM.Global.saveApprovalData,
                    "pricerequestid": $(".li-price-reqid-val").html(),
                    "implementstring": "",
                    "userid": localStorage.getItem("userID")
                };
                that.loaderview = new LoaderView({
                    text: "Saving....!"
                });
                console.log($(".fa-minus-circle").length)
               if($(".fa-minus-circle").length>0)
                    localStorage.setItem("pricelastupdatedsystem",$(".fa-minus-circle").parent().next().attr("id"))
                console.log(input);
                fnGetWebServerData("pricingApproval/updatePriceApprovalDetails", undefined, input, function (data,model) {
                    console.log(data)
                    console.log(model)
                    if (model == undefined) {
                        that.loaderview.close();
                        console.log(data);
                        showSuccess($(".li-price-reqid-val").html() + " saved Successfully ..!")
                        that.reqid = $(".li-price-reqid-val").html();
                        that.render();
                    } else{
                        showError($.parseJSON(model.responseText).message);
                         that.loaderview.close();
                        that.render();
                        
                    }
                });
            },
            cancelApproval: function () {
                window.history.back();
            },
            updateString: function (e) {
                console.log(GM.Global.saveApprovalData);
                var data;
                switch ($(e.currentTarget).attr("case")) {
                    case "select":
                        data = $(e.currentTarget).parent()
                        break;
                    case "input":
                        data = $(e.currentTarget).parent().parent()
                        break;
                }
                if ($(data).find("#price-approval-prop-unit input").val() != "") {
                    var inputstr = $(data).attr("systemid") + "^" + $(data).find(".ul-approval-status").val() + "^" + $(data).find(".ul-approval-changetype").val() + "^" + $(data).attr("groupid") + "^" + $(data).find("#price-approval-changevalue input").val() + "^" + $(data).find("#price-approval-prop-unit input").val().replace(",", "") + "^" + "N" + "^" + $(data).attr("prcdetlid");
                    if (GM.Global.saveApprovalData != "") {
                        var price = GM.Global.saveApprovalData.split("|");
                        price = _.filter(price, function (model) {
                            return (model.indexOf($(data).attr("prcdetlid")) == -1 && model!="");
                        });
                       GM.Global.saveApprovalData =""
                       _.each(price,function(data){
                           GM.Global.saveApprovalData = GM.Global.saveApprovalData +data+"|";
                       })
                        GM.Global.saveApprovalData = GM.Global.saveApprovalData + inputstr + "|"
                    } else
                        GM.Global.saveApprovalData = inputstr + "|";
                }
                 GM.Global.saveApprovalData = GM.Global.saveApprovalData.replace(",","|");
                console.log(GM.Global.saveApprovalData);

            },
            loadSelect: function (e) {
                _.each($(".approval-construct"), function (data) {
                    $(data).html($(data).parent().prev().attr("id"))
                })
                $('.ul-approval-status').each(function () {
                    if ($(this).val() == null) {
                        $(this).html("");
                        fnCreateOptions(this, GM.Global.priceApprovalDpdn);
                        $(this).val($(this).attr("value"));
                    }

                });
                _.each($('.ul-approval-changetype'), function (hata) {
                    if ($(hata).val() == null) {
                        $(hata).html("");
                        fnCreateOptions(hata, GM.Global.priceType);
                        $(hata).val($(hata).attr("value"));
                    }
                });
            },
            calcConstructPrice: function (e) {
                _.each($(".div-price-aproval-group-name"), function (iter) {
                    var propval = "";
                    var listval = "";
                    _.each($(iter).find("div"), function (tData) {
                        if ($(tData).hasClass("div-price-approval-ul")) {
                            if (propval == "") {
                                propval = eval($(tData).find(".ul-price-approval-prop-ext").html().replace(",", ""));
                                listval = eval($(tData).find(".price-approval-listext").html().replace(",", ""));
                            } else {
                                propval = propval + eval($(tData).find(".ul-price-approval-prop-ext").html().replace(",", ""));
                                listval = eval(listval) + eval($(tData).find(".price-approval-listext").html().replace(",", ""));
                            }
                        }
                        if ($(tData).hasClass("div-price-aproval-contruct")) {
                            $(tData).find(".price-approval-total-prop-ext").html(propval.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"))
                            $(tData).find(".price-approval-total-list-ext").html(listval.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"))
                        }
                    })
                })
            },
            calcChangeExtPrice: function (e) {
                $(e.currentTarget).parent().parent().find(".ul-price-approval-prop-ext").html($(e.currentTarget).val().replace(",", "") * $(e.currentTarget).parent().parent().find(".price-approval-qty").html())
                if ($(e.currentTarget).parent().parent().find("#price-approval-change").val() != "" && $(e.currentTarget).parent().parent().find("#price-approval-change").val() != undefined && $(e.currentTarget).parent().parent().find(".price-approval-tripwire").text() != "" && $(e.currentTarget).parent().parent().find(".price-approval-tripwire").text() != undefined) {
                    if (eval($(e.currentTarget).parent().parent().find("#price-approval-change").val().replace(",", "")) < eval($(e.currentTarget).parent().parent().find(".price-approval-tripwire").text().replace(",", "")))
                        $(e.currentTarget).parent().parent().find(".ul-price-approval-color").removeClass("green").addClass("red")

                    else
                        $(e.currentTarget).parent().parent().find(".ul-price-approval-color").removeClass("red").addClass("green")

                }
                this.calcConstructPrice();

            },

            fnGetGroupFullName: function (e) {
                
                var originalwidth = $(e.currentTarget).removeClass("price-approval-groupname").width();
                $(e.currentTarget).addClass("price-approval-groupname")
                var settedwidth = $(e.currentTarget).width();
               
                $(".highlighted-item").removeClass("highlighted-item");
                $(e.currentTarget).parent().addClass("highlighted-item");
                 if(settedwidth<originalwidth)
                    fnGetGroupFullName(e);
            },
            checkValidations: function (e) {
                var that = this;
                setTimeout(function () {


                    console.log($(e.currentTarget).val());
                    if (localStorage.getItem("pr_status_upd_access") != "Y") {
                        //                    $(e.currentTarget).blur();
                        $(e.currentTarget).val(GM.Global.previousValue);
                        showError("No Status Update Access");
                        //                    $(e.currentTarget).val(GM.Global.previousValue);
                        $('.ul-approval-status').attr('disabled', 'disabled');
                        $('.ul-approval-changetype').attr('disabled', 'disabled');
                        $(".div-price-approval-ul input").attr('disabled', 'disabled');

                    } else if (GM.Global.previousValue == "52123" && $(e.currentTarget).val() != "52187" && $(e.currentTarget).val() != "52120" && $(e.currentTarget).val() != "52125") {
                        if (localStorage.getItem("pr_pc_access") != "Y") {
                            showError("Not Authorised..");
                            $(e.currentTarget).val(GM.Global.previousValue);
                        } else {

                            $(e.currentTarget).parent().find("input").prop('disabled', false);
                            $(e.currentTarget).parent().find('select').prop('disabled', false);
                            $(e.currentTarget).parent().find(".ul-approval-changetype").trigger("change")
                            that.updateString(e);
                        }
                    } else {
                        if ($(e.currentTarget).val() == "52125") {
                            if (localStorage.getItem("pr_apprvl_access") != "Y") {
                                showError("Not Authorised..");
                                $(e.currentTarget).val(GM.Global.previousValue);


                            } else {
                                $(e.currentTarget).parent().find("input").attr('disabled', 'disabled');
                                $(e.currentTarget).parent().find('select').attr('disabled', 'disabled');
                                $(e.currentTarget).prop('disabled', false);
                                //                            $(e.currentTarget).parent().find('select').attr('disabled', 'disabled');
                                that.updateString(e);
                            }

                        } else if ($(e.currentTarget).val() == "52120") {

                            if (localStorage.getItem("pr_denied_access") != "Y") {
                                showError("Not Authorised..");

                                $(e.currentTarget).val(GM.Global.previousValue);

                            } else {
                                $(e.currentTarget).parent().find("input").prop('disabled', false);
                                $(e.currentTarget).parent().find('select').prop('disabled', false);
                                $(e.currentTarget).parent().find(".ul-approval-changetype").trigger("change")
                                that.updateString(e);
                            }
                        } else if ($(e.currentTarget).val() == "52187") {

                            if (localStorage.getItem("advpaccfl") != "Y") {
                                showError("Not Authorised..");

                                $(e.currentTarget).val(GM.Global.previousValue);

                            } else {
                                $(e.currentTarget).parent().find("input").prop('disabled', false);
                                $(e.currentTarget).parent().find('select').prop('disabled', false);
                                that.updateString(e);
                            }
                        } else {

                            $(e.currentTarget).parent().find("input").prop('disabled', false);
                            $(e.currentTarget).parent().find('select').prop('disabled', false);
                            $(e.currentTarget).parent().find(".ul-approval-changetype").trigger("change")
                            that.updateString(e);
                        }
//                        $(".ul-approval-changetype").trigger("change")
                    }

                }, 400);
            },
            checkValue: function (e) {
                var Sdata = _.filter(GM.Global.tData.listGmGroupVO, function (model) {
                    return (model.prcdetlid == $(e.currentTarget).parent().attr("prcdetlid"));
                })
                if (Sdata[0].statusid != undefined)
                    GM.Global.previousValue = Sdata[0].statusid;
                else
                    GM.Global.previousValue = $(e.currentTarget).val();
            },
            showThresholdQuestions: function (e) {
                showThresholdQuestions(e, DropDownView);
            },
            calcImpactAnaly: function (e) {
                var prid = $(".li-price-reqid-val").html();

                calcImpactAnaly(e, this, prid, LoaderView)
            },
            showPriceAccountDetails: function () {
                var input1 = {
                    "token": localStorage.getItem("token"),
                    "accid": $(".div-price-aproval-account-val").attr("name"),
                    "typeid": $(".div-price-aproval-account-val").attr("typeid"),
                    "repid": localStorage.getItem("userID"),
                    "acid": $(".div-price-aproval-account-val").attr("name"),
                    "deptid": localStorage.getItem("deptid"),
                    "userid": localStorage.getItem("userID")
                };
                 var input2 = {
                            "token": localStorage.getItem("token"),
                            "accid": $(".div-price-aproval-account-val").attr("name"),
                            "typeid": $(".div-price-aproval-account-val").attr("typeid")
                        };
                showPriceAccountDetails(input1,input2);
            },
            showFileUploadPopup: function () {
                var prid = $(".li-price-reqid-val").html();
                showPriceFileUploadPopup(this, prid, LoaderView, GMCFileUpload);
            },
            masterStatusSelect: function (e) {
                $(".select-price-aproval-system-main-val").val($(e.currentTarget).val()).trigger("change");
            },
            validateNumber: function (event) {
                var numberPattern = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
                if (!numberPattern.test($(event.currentTarget).val())) {
                    $(event.currentTarget).val("");
                }
            },
            collapseApproval: function (e) {
                console.log(e);
                $(".approval-additional-info").toggle(300);
                setTimeout(function () {
                    if($(".approval-additional-info").css("display")=="none")
                        $(".div-price-approval-content-data").css("height",$(".div-price-approval-content-data").height()+$(".approval-additional-info").height())
                        else
                            
                        $(".div-price-approval-content-data").css("height",(465-$(".approval-additional-info").height()))
                
                },301);
            },
            highlightSelected: function (e) {
                $(".highlighted-item").removeClass("highlighted-item");
                var data;
                if ($(e.currentTarget).attr("case") == "select")
                    data = $(e.currentTarget).parent();
                else if ($(e.currentTarget).attr("case") == "input")
                    data = $(e.currentTarget).parent().parent();
                else if ($(e.currentTarget).hasClass("div-p-icon"))
                    data = $(e.currentTarget).parent().parent();
                $(data).addClass("highlighted-item");
            },
        });
        return pricingApproval;
    });