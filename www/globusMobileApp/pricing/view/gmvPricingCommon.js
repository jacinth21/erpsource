/**********************************************************************************
 * File:        gmvPricingCommon.js
 * Description: Contains global variables and functions commonly used across Pricing
 * Version:     1.0
 * Author:      Akumar
 **********************************************************************************/
function showGroupPartPricingDetails(e) {
    //                var that = this;
    var templateGroupPartPricingDetails = fnGetTemplate(URL_Price_Template, "gmtGroupPartPricingDetails")
    var token = localStorage.getItem("token");
    var grpid = $(e.currentTarget).attr("id");
    var grpname = $(e.currentTarget).attr("name");
    var input = {
        "groupid": grpid,
        "groupname": grpname,
        "token": token
    };
    fnGetWebServerData("pricingRequest/groupPartMapDetailsList/", undefined, input, function (data) {
        var templatedata = JSON.stringify(data.gmGroupPartMappingVO);
        templatedata = arrayOf($.parseJSON(templatedata));
        showGroupPartInfoPopup();
        $("#msg-overlay").show();
        $("#div-price-overlay-content-container7 .modal-body").html(templateGroupPartPricingDetails(templatedata));
        $(".li-pricing-group-part-screen").html(grpname);
        $("#div-price-overlay-content-container7").find('.modal-body .li-group-part-cancel-button, .div-price-group-close-btn').unbind('click').bind('click', function () {
            hideGroupPartInfoPopup();
            $("#msg-overlay").hide();
        });
        $("#div-price-overlay-content-container7").find('.modal-body-priceinfo .mobpartDetails').unbind('click').bind('click', function () {
                    showAccountinfoDetails();
                });
        
    });
}

function showPriceAccountDetails(input1, input2, LoaderView) {
    var that = this;
    var templatePriceAccountDetails = fnGetTemplate(URL_Price_Template, "gmtPriceAccountDetails");
    var templateGPBPriceAccountDetails = fnGetTemplate(URL_Price_Template, "gmtPriceGPBAccountDetails" + GM.Global.Phone );
    fnGetWebServerData("pricingRequest/accountName/", "gmPriceAccountTypeVO", input1, function (data) {
        if (data.accname != "" || accID == undefined) {
            this.loaderview = new LoaderView({
                text: "Loading Account Details."
            });
            fnGetWebServerData("pricingRequest/accountDetailsList/", undefined, input2, function (data) {
                that.loaderview.close();
                if (data != null) {
                    var templatedata = JSON.stringify(data.gmAccountDetailsListVO);
                    showAccountInfoPopup();
                    $("#msg-overlay").show();
                    //903108 := Group Account
                    if(input1.typeid =="903108"){
                        console.log(data.gmAccountDetailsListVO);
                        data = arrayOf(data.gmAccountDetailsListVO);
                        if(data[0].accid == "")
                            data["empty"] ="Y";
                      $("#div-price-overlay-content-container6 .modal-body").html(templateGPBPriceAccountDetails(data));
//                      if(GM.Global.Phone=="Phone"){
//                          $("#div-price-overlay-content-container6 .modal-body").html(templateGPBPriceAccountDetailsMobile(data));
//                      }
                    }else{
                    $("#div-price-overlay-content-container6 .modal-body").html(templatePriceAccountDetails(data.gmAccountDetailsListVO[0]));
                    }
                } else {
                    showError("Invalid Account/Party Id");
                }
                $("#div-price-overlay-content-container6").find('.modal-body .li-group-part-cancel-button, .div-price-group-close-btn').unbind('click').bind('click', function () {
                    hideAccountInfoPopup();
                    $("#msg-overlay").hide();
                });
                 $("#div-price-overlay-content-container6").find('.modal-body .li-group-part-cancel-button, .div-price-group-close-btn').unbind('click').bind('click', function () {
                    hideAccountInfoPopup();
                    $("#msg-overlay").hide();
                });
                                 
                $("#div-price-overlay-content-container6").find('.modal-body #div-gpb-detail-content .fa-chevron-down').unbind('click').bind('click', function (e) {
                    var divclass=$(e.target).parent().parent().parent().attr('class');
                    if(divclass == 'li-gpb-price-acct'){
                     $(e.target).parent().parent().parent().addClass('onActive');
                    $(e.target).parent().parent().parent().find('#div-gpb-dtls-region').removeClass('pricing-mob');
                    $(e.target).parent().parent().parent().find('#div-gpb-dtls-territory').removeClass('pricing-mob');
                     $(e.target).parent().parent().parent().find('#div-gpb-dtls-adname').removeClass('pricing-mob');
                     $(e.target).parent().parent().parent().find('#div-gpb-dtls-vpname').removeClass('pricing-mob');
                    $(e.target).removeClass('fa-chevron-down');
                     $(e.target).addClass('fa-chevron-up');
                    }
                    else{
                     $(e.target).parent().parent().parent().removeClass('onActive'); 
                     $(e.target).parent().parent().parent().find('#div-gpb-dtls-region').addClass('pricing-mob');
                     $(e.target).parent().parent().parent().find('#div-gpb-dtls-territory').addClass('pricing-mob');
                     $(e.target).parent().parent().parent().find('#div-gpb-dtls-adname').addClass('pricing-mob');
                     $(e.target).parent().parent().parent().find('#div-gpb-dtls-vpname').addClass('pricing-mob');
                        $(e.target).addClass('fa-chevron-down');
                     $(e.target).removeClass('fa-chevron-up');
                    }
                });
                
                $("#div-price-overlay-content-container6").find('.modal-body .li-threshold-cancel-button').unbind('click').bind('click', function () {
                    hideAccountInfoPopup();
                    $("#msg-overlay").hide();
                });
            });
        } else {
            showError("Please select a Rep Account");
        }
    });


}

function showPartDetails(e, LoaderView) {
    console.log(e);
    showPartDetailsPopup();
    $("#msg-overlay").show();
    var templatePartDetails = fnGetTemplate(URL_Price_Template, "gmGroupPartPricing");
    var templatePartTotalDetails = fnGetTemplate(URL_Price_Template, "gmtPartPricingDetails");
    var templateGroupTotalDetails = fnGetTemplate(URL_Price_Template, "gmtGroupPricingDetaills");
    $("#div-price-overlay-content-container12 .modal-body").html(templatePartDetails());
    $("#div-price-overlay-content-container12").find('.modal-body #btn-part-load-btn').unbind('click').bind('click', function () {
        if ($("#div-part-value-input").val() != "") {
            var input = {};
            input.partNum = $("#div-part-value-input").val();
            input.search = $(".sel-address-modea").val();
            input.token = localStorage.getItem("token");
            input.userid = localStorage.getItem("userID");

            console.log(input);
            GM.Global.Loader = new LoaderView({
                text: "Loading Part Details."
            });
            fnGetWebServerData("pricingRequest/PartPricing/", undefined, input, function (data) {
                GM.Global.Loader.close();
                console.log(data.listGmPartVO);
                console.log(data.listGmGruouVO);
                if (data.listGmPartVO != undefined || data.listGmGruouVO != undefined) {
                    var templatepartdata = JSON.stringify(data.listGmPartVO);
                    var templategroupdata = JSON.stringify(data.listGmGruouVO);
                    if (templategroupdata != undefined) {
                        templategroupdata = arrayOf($.parseJSON(templategroupdata));
                        $("#div-price-overlay-content-container12 .modal-body").find(".div-part-price-details").html(templateGroupTotalDetails(templategroupdata)).removeClass("textalign");
                    }
                    if (templatepartdata != undefined) {
                        templatepartdata = arrayOf($.parseJSON(templatepartdata));
                        $("#div-price-overlay-content-container12 .modal-body").find(".div-part-price-details").append(templatePartTotalDetails(templatepartdata));
                    }
                    
                } else {
                    $("#div-price-overlay-content-container12 .modal-body").find(".div-part-price-details").html("No Data Found").addClass("textalign");
                }

            });
        } else {
            showError("Please enter the part number");
        }
    });
    $("#div-price-overlay-content-container12").find('.modal-body .price-popup-part-close').unbind('click').bind('click', function () {
        hidePartDetailsPopup();
        $("#msg-overlay").hide();
    });
}

function fnGetGroupFullName(e) {
    var that = this;
    var templateShowName = fnGetTemplate(URL_Price_Template, "gmtShowName");
    var name = $(e.currentTarget).html();
    var partnumber = $(e.currentTarget).attr("partnumber");
    showAccountInfoPopup();
    $("#msg-overlay").show();
    $("#div-price-overlay-content-container6 .modal-body").html(templateShowName());
    if (partnumber != "" && partnumber != undefined)
        $(".li-pricing-group-part-screen").html("Part Description");
    else
        $(".li-pricing-group-part-screen").html("Group Name");
    $(".div-show-construct-name").html(name).css("height", "30px").css("margin", "10px");
    $("#div-price-overlay-content-container6").find('.modal-body .li-group-part-cancel-button, .div-price-group-close-btn').unbind('click').bind('click', function () {
        hideAccountInfoPopup();
        $("#msg-overlay").hide();
    });
}

function showThresholdQuestions(e, parent) {

    //                showThresholdQuestions(e);
    var that = parent;
    var templatePriceThresholdQusetions = fnGetTemplate(URL_Price_Template, "gmtThresholdQuestions" + GM.Global.Phone);
    var input = {
        "token": localStorage.getItem("token"),
        "requestid": $(".li-price-reqid-val").html()
    };
    fnGetWebServerData("pricingRequest/thresholdQuestions/", undefined, input, function (data) {
        showInfoPopup();
        $("#msg-overlay").show();
        $("#div-price-overlay-content-container5 .modal-body").html(templatePriceThresholdQusetions(data.gmThresholdQuestionVO));
        $("#div-threshold-submit").hide();
        /*Consultant drpdwn*/
        getConsultantDrp(data.gmThresholdQuestionVO, parent);
        _.each(data.gmThresholdQuestionVO, function (r) {
            if (r.dropdownid != "")
                that.consultantId = r.dropdownid;
        })
        $("#div-price-overlay-content-container5").find('.modal-body .li-threshold-cancel-button').unbind('click').bind('click', function () {
            hideInfoPopup();
            $("#msg-overlay").hide();
        });
        $("#div-price-overlay-content-container5").find('.modal-body #fa-close-alert').unbind('click').bind('click', function () {
            hideMessages();
        });


    });


}

function getConsultantDrp(data, DropDownView) {
    var that = this;
    var defaultid = 0;
    var defaultname = "Choose One";
    var input = {
        "codeGrp": "YES/NO",
        "token": localStorage.getItem("token")
    };
    fnGetCodeLookUpValues(input, function (drpDwn) {
        drpDwn.push({
            "ID": defaultid,
            "Name": defaultname
        });
        if (data[0].dropdownid != "" && data[0].dropdownvalue != "") {
            defaultid = data[0].dropdownid;
            defaultname = data[0].dropdownvalue;
        }
        var consultantdropdownview = new DropDownView({
            el: $("#div-price-overlay-content-container5 .modal-body #li-pricing-dropdown"),
            data: drpDwn,
            DefaultID: defaultid
        });
        $("#div-price-overlay-content-container5 .modal-body #li-pricing-dropdown").bind('onchange', function (e, selectedId, SelectedName) {
            that.consultantId = selectedId;
            that.consultantName = SelectedName;
        });
    });
}

function calcImpactAnaly(e, parent, prid, LoaderView) {
    var that = parent;
    var requestid = "";
    requestid = prid;
    console.log(requestid);
    var repId = localStorage.getItem("userID");
    var token = localStorage.getItem("token");
    var userid = localStorage.getItem("userID");
    var input = {
        "token": "token",
        "refid": requestid,
        "userid": userid,
    };
    var loaderview = new LoaderView({
        text: "Loading Impact Analysis."
    });
    //webservice call to get impact analysis data
    fnGetWebServerData("impactAnalysis/fetchImpactAnalysis/", "gmImpactAnalysisVO", input, function (data) {
        //        console.log(data);
        if (data == null) {
            loaderview.close();
            showError("No Sales Data Found");
            return;
        }
        data = arrayOf(data);
        loaderview.close();

        //display chart by default
        chkflag = true; //by default impacted checkbox should be checked
        fnImpactAnalysisCht(LoaderView, e, "system", data, requestid, parent, chkflag);
    });
}

//function to generate impact analysis chart
function fnImpactAnalysisCht(LoaderView, e, grpBy, data, requestId, parent, chkflag, order) {
    var that = parent;
    var allData = data;
    var ctitle = "";
    var xAxisName = "";
    var impChkd = false;

    if ($("#div-price-overlay-content-container10").find(".check-btn:checkbox:checked").length > 0 || (chkflag == true))
        impChkd = true;

    if (grpBy == undefined || grpBy == "system") {
        ctitle = "Impact Analysis by System";
        xAxisName = "Systems";
        if (impChkd == false) {
            var chartData = _.filter(data, function (model) {
                return (model.groupingid == 3) //systems
            });
        } else {
            var chartData = _.filter(data, function (model) {
                return (model.groupingid == 3 && model.projimpact != 0) //systems
            });
        }
    } else if (grpBy == "account") {
        ctitle = "Impact Analysis by Facility"
        xAxisName = "Facility";
        if (impChkd == false) {
            var chartData = _.filter(data, function (model) {
                return (model.groupingid == 5) //facility
            });
        } else {
            var chartData = _.filter(data, function (model) {
                return (model.groupingid == 5 && model.projimpact != 0) //systems
            });
        }
    }

    //sort the chartData obj.
    chartData.sort(function (a, b) {
        return parseFloat(b.orderamt) - parseFloat(a.orderamt);
    });
    //prepare data for chart
    var grandTotal = _.filter(allData, function (gtemp) {
        return gtemp.groupingid == '7';
    });

    var category = [];
    var sale = [];
    var neg_projImpact = [];
    var pos_projImpact = [];
    var currencysgn = allData[0].currency;
    var icnt = 0;
    var dataLength = Object.keys(chartData).length
    var othSale = 0;
    var othSalesTot = 0;
    var othImpTot = 0;
    _.each(chartData, function (Sdata) {

        //fill the daaset
        icnt += 1;
        if (icnt < 18) {


            if (grpBy == "system" || grpBy == undefined || grpBy == "impacted") {
                category.push({
                    label: ((Sdata.systemname.length > 20) ? uniToString(Sdata.systemname.substr(0, 20)) + '...' : uniToString(Sdata.systemname))
                });
            } else {
                category.push({
                    label: ((Sdata.accountname.length > 20) ? Sdata.accountname.substr(0, 20) + '...' : Sdata.accountname)
                });
            };
            sale.push({
                value: (parseInt(Sdata.propprice) > parseInt(Sdata.orderamt)) ? Sdata.propprice : Sdata.orderamt
            });

            //calculate percentage to display
            if (Sdata.projimpact != 0) {
                var totValue = ((parseInt(Sdata.propprice) > parseInt(Sdata.orderamt)) ? parseFloat(Sdata.propprice) : parseFloat(Sdata.orderamt)) + parseFloat(Sdata.projimpact);
                var impPercent = (100 / (totValue / Sdata.projimpact)).toFixed(2);
            };

            neg_projImpact.push({
                value: (Sdata.projimpact < 0 ? Math.abs(Sdata.projimpact) : ''),
                displayValue: currencysgn + (Sdata.projimpact < 0 ? formatCurrency(Sdata.projimpact, 0) + " (" + impPercent.toString() + "%)" : '')
            });

            pos_projImpact.push({
                value: (Sdata.projimpact > 0 ? Sdata.projimpact : ''),
                displayValue: currencysgn + (Sdata.projimpact > 0 ? formatCurrency(Sdata.projimpact, 0) + " (" + impPercent.toString() + "%)" : '')
            });
        } else {

            othSale = othSale + ((parseFloat(Sdata.propprice) > parseFloat(Sdata.orderamt)) ? parseFloat(Sdata.propprice) : parseFloat(Sdata.orderamt));

            if (Sdata.projimpact != 0) {
                othSalesTot = othSalesTot + ((parseInt(Sdata.propprice) > parseInt(Sdata.orderamt)) ? parseFloat(Sdata.propprice) : parseFloat(Sdata.orderamt) + Math.abs(parseFloat(Sdata.projimpact)));
                othImpTot = othImpTot + parseFloat(Sdata.projimpact);
            };

            if (icnt == dataLength) {
                category.push({
                    label: "Others"
                });

                sale.push({
                    value: othSale
                });

                var othImpactPerc = (100 / (othSalesTot / othImpTot)).toFixed(2)
                if (othSalesTot != 0) {
                    if (othSalesTot < 0) {
                        neg_projImpact.push({
                            value: othSalesTot,
                            displayValue: currencysgn + (Sdata.projimpact < 0 ? formatCurrency(othImpTot, 0) + " (" + othImpactPerc.toString() + "%)" : '')
                        });
                    } else {
                        pos_projImpact.push({
                            value: (Sdata.projimpact > 0 ? Sdata.projimpact : ''),
                            displayValue: currencysgn + (Sdata.projimpact > 0 ? formatCurrency(Sdata.projimpact, 0) + " (" + othImpactPerc.toString() + "%)" : '')
                        });
                    }
                }


            };
        }


    });
    showImpactPopup();
    $("#msg-overlay").show();
    var templateImpactAnalysis = fnGetTemplate(URL_Price_Template, "gmtImpactAnalysis");
    var templateImpactAnalysisChart = fnGetTemplate(URL_Price_Template, "gmtImpactAnalysisChart");
    $("#div-price-overlay-content-container10 .modal-body").html(templateImpactAnalysis());
    $("#div-price-overlay-content-container10 .modal-body").find(".div-impact-analysis").html(templateImpactAnalysisChart());
    //Adding color to chart tab
    if (impChkd == true)
        $("#li-impacted-heading").prop("checked", true);
    $("#div-impact_by_chart").addClass("active");
    $("#div-impact-by-facility").removeClass("active");
    $("#div-impact_by_sytem").removeClass("active");

    //inside chart buttons hilight
    $("#li-system-heading").addClass("activebtn");
    $("#li-facility-heading").removeClass("activebtn");

    var requestId = allData[0].requestid;
    var orgName = allData[0].orgname;
    var titleRef = "Impact Analysis : " + requestId + " - " + orgName;


    $("#li-prev12-monthsale").find(".li-prev12-monthsale").html(currencysgn + Math.abs(grandTotal[0].orderamt).cformat());
    $("#li-proj-propprice").find(".li-proj-propprice").html(currencysgn + Math.abs(grandTotal[0].propprice).cformat());

    $("#li-projected-impact").find(".li-projected-impact").html(Math.abs(grandTotal[0].projimpact).cformat());
    var gimpactPerc = 0;

    //Projected impact data
    if (grandTotal[0].projimpact < 0) {
        $("#li-projected-impact").find(".li-projected-impact").html("(" + currencysgn + Math.abs(grandTotal[0].projimpact).cformat() +
            ")");
        $("#li-projected-impact").find(".li-projected-impact").css("color", "red");
    } else {
        $("#li-projected-impact").find(".li-projected-impact").html(currencysgn + Math.abs(grandTotal[0].projimpact).cformat());
    }

    //calculate the Project Impact% (Same formula as used to caculate for percent against each bar on chart)
    var baseTotal = ((parseInt(grandTotal[0].propprice) > parseInt(grandTotal[0].orderamt)) ? parseFloat(grandTotal[0].propprice) : parseFloat(grandTotal[0].orderamt)) + parseFloat(grandTotal[0].projimpact);
    gimpactPerc = (parseFloat(grandTotal[0].projimpact) / parseFloat(baseTotal)) * 100;
    gimpactPerc = gimpactPerc.toFixed(2);
    if (gimpactPerc < 0) {
        $("#li-project-impact").find(".li-project-impact").html("(" + Math.abs(gimpactPerc) + "%" + ")");
        $("#li-project-impact").find(".li-project-impact").css("color", "red");
    } else {
        $("#li-project-impact").find(".li-project-impact").html(gimpactPerc + "%");
    }

    $(".titleRef").html(titleRef);
    $(".li-account-name").html(orgName);


    //generate the chart
    var impactChart = new FusionCharts({
        type: 'stackedbar2d',
        renderAt: 'chart-container',
        width: '950',
        height: '465',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": ctitle,
                "xAxisname": xAxisName,
                "yAxisName": "Sales",
                "paletteColors": "#16b1e7,#ff0000,#82b74b",
                "borderColor": "#fff8e4",
                "bgColor": "#fff8e4",
                "borderAlpha": "20",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",
                "divLineDashed": "1",
                "showAlternateVGridColor": "0",
                "subcaptionFontBold": "1",
                "subcaptionFontSize": "16",
                "showHoverEffect": "1",
                "showValues": "0",
                "dataLoadStartMessage": "Loading chart. Please wait...",
                "useEllipsesWhenOverflow": "1",
                "valueFontBold": "1",
                "formatNumberScale": "0",
                "valueFontColor": "#0000ff",
                "canvasBgColor": "#ffffff",
                "numberPrefix": currencysgn,
                "showPercentInToolTip": "1",
                "legendItemFontBold": "1",
                "exportenabled": "1",
                "exportatclient": "1",
            },
            "categories": [
                {
                    "category": category

        }
    ],
            "dataset": [
                {
                    "seriesname": "Sale",
                    "data": sale
                },
                {
                    "seriesname": "-ve Impact",
                    "data": neg_projImpact,
                    "showValues": "1",

                },
                {
                    "seriesname": "+ve Impact",
                    "data": pos_projImpact,
                    "showValues": "1",
                }
            ]
        }
    }).render();


    //Chart navigation bar - System button
    $("#div-price-overlay-content-container10").find('#li-system-heading').unbind('click').bind('click', function (e) {
        fnImpactAnalysisCht(LoaderView, e, "system", data, requestId, parent);
        //inside chart buttons hilight
        $("#li-system-heading").addClass("activebtn");
        $("#li-facility-heading").removeClass("activebtn");
    });

    //Chart navigation bar - account button
    $("#div-price-overlay-content-container10").find('#li-facility-heading').unbind('click').bind('click', function (e) {
        fnImpactAnalysisCht(LoaderView, e, "account", data, requestId, parent);
        //inside chart buttons hilight
        $("#li-system-heading").removeClass("activebtn");
        $("#li-facility-heading").addClass("activebtn");
    });


    //impacted checkbox
    $("#div-price-overlay-content-container10").find('#li-impacted-heading').unbind('click').bind('click', function (e) {
        var selBtn = "system"
        if ($("#li-facility-heading").hasClass("activebtn"))
            selBtn = "account";
        fnImpactAnalysisCht(LoaderView, e, selBtn, data, requestId, parent);
        if (selBtn == "system") {
            $("#li-system-heading").addClass("activebtn");
            $("#li-facility-heading").removeClass("activebtn");
        } else {
            $("#li-system-heading").removeClass("activebtn");
            $("#li-facility-heading").addClass("activebtn");

        }
    });



    //Click on Chart (menu bar)
    $("#div-price-overlay-content-container10").find('#div-impact_by_chart').unbind('click').bind('click', function (e) {
        GM.Global.SortPrice = "ascending";
        fnImpactAnalysisCht(LoaderView, e, "system", data, requestId, parent);
        setTimeout(function () {
            GM.Global.loaderview.close();
            $('.li-impact-pdf-button').addClass("hide")
        }, 500);

    });


    //Click on impact by system
    $("#div-price-overlay-content-container10").find('#div-impact_by_sytem').unbind('click').bind('click', function (e) {
        GM.Global.loaderview = new LoaderView({
            text: "Loading..."
        });
        GM.Global.SortPrice = "ascending";
        fnImpactAnalysisRpt(LoaderView, e, "systemname", data, requestId, parent);
        setTimeout(function () {
            GM.Global.loaderview.close();
            $('.li-impact-pdf-button').removeClass("hide")

        }, 500);

    });

    //Click on impact by facility
    $("#div-price-overlay-content-container10").find('#div-impact-by-facility').unbind('click').bind('click', function (e) {
        GM.Global.loaderview = new LoaderView({
            text: "Loading..."
        });
        GM.Global.SortPrice = "ascending";
        fnImpactAnalysisRpt(LoaderView, e, "accountname", data, requestId, parent);
        setTimeout(function () {
            GM.Global.loaderview.close();
            $('.li-impact-pdf-button').removeClass("hide")
        }, 500);
    });

    //Impact analysys close
    $("#div-price-overlay-content-container10").find('.li-threshold-cancel-button').unbind('click').bind('click', function (e) {
        $("#msg-overlay").hide();
        GM.Global.SortPrice = "ascending";
        hideImpactPopup();
        $(".div-price-impact-btn").css("background-color", "#b2ffb2").css("color", "black");

    });
}

function fnImpactAnalysisRpt(LoaderView, e, grpBy, data, requestId, parent, flag, order) {
    var that = parent;
    var allData = data;
    //Impact analysis all fields now seperating based on groupingid
    if (grpBy == undefined || grpBy == "systemname") {
        var sortData = _.filter(data, function (model) {
            return (model.groupingid == 3)
        });
        var grpGrouped = _.chain(sortData).map(function (model) {
            return model.systemname;
        }).uniq().value();
    } else {
        var sortData = _.filter(data, function (model) {
            return (model.groupingid == 5)
        });
        var grpGrouped = _.chain(sortData).map(function (model) {
            return model.accountname
        }).uniq().value();

    }
    if (GM.Global.Sorting == true) {
        if ($('.price-sort').find(".fa").hasClass("fa-arrow-down"))
            grpGrouped = grpGrouped.reverse()
        GM.Global.Sorting = false;
    }

    var partsJson = [];
    _.each(grpGrouped, function (Sdata) {

        if (grpBy == undefined || grpBy == "systemname") {
            var partsdata = _.filter(data, function (model) {
                return (model.systemname == Sdata && model.groupingid == 2);
            });
            var systemName = uniToString(Sdata);
            var price = _.filter(allData, function (model) {
                return (model.systemname == Sdata)
            });
            price = arrayOf(price);
            var systemidd = price[0].systemid;
            that.active = 'Y';
        } else {
            var partsdata = _.filter(data, function (model) {
                return (model.accountname == Sdata && model.groupingid == 4);
            });
            var systemName = uniToString(Sdata);
            var price = _.filter(allData, function (model) {
                return (model.accountname == Sdata)
            });
            price = arrayOf(price);
            var systemidd = price[0].accountid;
            that.active = 'N';
        }

        partsJson.push({
            name: systemName,
            partNumber: partsdata,
            systemid: systemidd
        });
    })

    var templateImpactAnalysisDetail = fnGetTemplate(URL_Price_Template, "gmtImpactAnalysisDetail");
    $("#div-price-overlay-content-container10 .modal-body").find(".div-impact-analysis").html(templateImpactAnalysisDetail(partsJson));
    if (GM.Global.SortPrice == "descending") {
        $('.price-sort').find(".fa").removeClass("fa-arrow-down").addClass("fa-arrow-up")
    } else if (GM.Global.SortPrice == "ascending") {
        $('.price-sort').find(".fa").removeClass("fa-arrow-up").addClass("fa-arrow-down")
    }

    // Change Button Background color in impact analysis
    if (that.active == 'Y') {
        $('#changeName').text('System Name');
        $('#changeName').attr('name', "systemname");
        $("#div-impact_by_sytem").addClass("active");
        $("#div-impact-by-facility").removeClass("active");
        $("#div-impact_by_chart").removeClass("active");
    } else {
        $('#changeName').text('Account Name');
        $('#changeName').attr('name', "accountname");
        $("#div-impact_by_sytem").removeClass("active");
        $("#div-impact-by-facility").addClass("active");
        $("#div-impact_by_chart").removeClass("active");
    }

    var alength = $(".ul-pricing-impact-content .div-impact-total").length

    _.each($(".ul-pricing-impact-content .div-impact-total"), function (temp) {
        var maindata = data;

        //System level total
        var systemTotal = _.filter(allData, function (a) {
            return a.groupingid == '3' && $(temp).attr("systemid") == a.systemid;
        });
        if (systemTotal.length > 0) {
            $(temp).find(".vtotsale").html(Math.abs(systemTotal[0].orderamt).cformat() + ".00");
            $(temp).find(".vtotprop").html(Math.abs(systemTotal[0].propprice).cformat() + ".00");
            if (systemTotal[0].projimpact < 0) {
                $(temp).find(".vtotproj").html("(" + Math.abs(systemTotal[0].projimpact).cformat() + ".00" + ")");
                $(temp).find(".vtotproj").css("color", "red");
            } else {
                $(temp).find(".vtotproj").css("color", "#000");
                $(temp).find(".vtotproj").html(Math.abs(systemTotal[0].projimpact).cformat() + ".00");
            }
        }
        //Facility account level total
        var facilityTotal = _.filter(allData, function (a) {
            return a.groupingid == '5' && $(temp).attr("systemid") == a.accountid;
        });
        if (facilityTotal.length > 0) {
            $(temp).find(".vtotsale").html(Math.abs(facilityTotal[0].orderamt).cformat() + ".00");
            $(temp).find(".vtotprop").html(Math.abs(facilityTotal[0].propprice).cformat() + ".00");
            if (facilityTotal[0].projimpact < 0) {
                $(temp).find(".vtotproj").html("(" + Math.abs(facilityTotal[0].projimpact).cformat() + ".00" + ")");
                $(temp).find(".vtotproj").css("color", "red");
            } else {
                $(temp).find(".vtotproj").css("color", "#000");
                $(temp).find(".vtotproj").html(Math.abs(facilityTotal[0].projimpact).cformat() + ".00");
            }
        }

        //Checking for positive / negative value to show red color
        _.each($(temp).find(".div-parts-impact-amt"), function (Vdata) {
            var partProjImpact = $(Vdata).find(".totproj").html();
            if (partProjImpact < 0) {
                $(Vdata).find(".totproj").html("(" + Math.abs(partProjImpact).cformat() + ".00" + ")");
                $(Vdata).find(".totproj").css("color", "red");
            } else {
                $(Vdata).find(".totproj").css("color", "#000");
                $(Vdata).find(".totproj").html(Math.abs(partProjImpact).cformat() + ".00");
            }

        });

    });

    //Grand total System/Facility Account
    var grandTotal = _.filter(allData, function (gtemp) {
        return gtemp.groupingid == '7';
    });
    if (grandTotal.length > 0) {
        $(".gmPriceTotalFooter").find(".gtotsale").html(Math.abs(grandTotal[0].orderamt).cformat() + ".00");
        $(".gmPriceTotalFooter").find(".gtotprop").html(Math.abs(grandTotal[0].propprice).cformat() + ".00");
        if (grandTotal[0].projimpact < 0) {
            $(".gmPriceTotalFooter").find(".gtotproj").html("(" + Math.abs(grandTotal[0].projimpact).cformat() + ".00" + ")");
            $(".gmPriceTotalFooter").find(".gtotproj").css("color", "red");
            //Info on header section
            $(".gtotprojs").html("(" + Math.abs(grandTotal[0].projimpact).cformat() + ".00" + ")");
            $(".gtotprojs").css("color", "red");
        } else {
            $(".gmPriceTotalFooter").find(".gtotproj").html(Math.abs(grandTotal[0].projimpact).cformat() + ".00");
            $(".gmPriceTotalFooter").find(".gtotproj").css("color", "white");
            //Info on header section
            $(".gtotprojs").html(Math.abs(grandTotal[0].projimpact).cformat() + ".00");
            $(".gtotprojs").css("color", "#000");
        }
    }
    //Header section information
    $(".gtotsales").html(Math.abs(grandTotal[0].orderamt).cformat() + ".00");
    $(".gtotprops").html(Math.abs(grandTotal[0].propprice).cformat() + ".00");
    var orgName = allData[0].orgname;
    var ADName = allData[0].adname;
    var genDate = strToDate(allData[0].gendate);
    console.log(genDate);

    var requestId = allData[0].requestid;
    var acCurrency = allData[0].currency;
    var titleRef = "Impact Analysis : " + requestId;

    $(".titleRef").html(titleRef);
    $(".orgName").html(orgName);
    $(".acCurrency").html(acCurrency);
    $(".genDate").html(genDate);
    $(".adName").html(ADName);
    var gimpactPerc = 0;

    //calculate the Project Impact % (Same formula as used to caculate for percent against each bar on chart)
    var baseTotal = ((parseFloat(grandTotal[0].propprice) > parseFloat(grandTotal[0].orderamt)) ? parseFloat(grandTotal[0].propprice) : parseFloat(grandTotal[0].orderamt)) + parseFloat(grandTotal[0].projimpact);
    gimpactPerc = (parseFloat(grandTotal[0].projimpact) / parseFloat(baseTotal)) * 100;
    gimpactPerc = gimpactPerc.toFixed(2);
    if (gimpactPerc < 0) {
        $(".impactperc").html("(" + Math.abs(gimpactPerc) + "%" + ")");
        $(".impactperc").css("color", "red");
    } else {
        $(".impactperc").html(gimpactPerc + "%");
    }

    $("#div-price-overlay-content-container10").removeClass("hide");
    $("#div-price-overlay-content-container10").find('.impact-analy-system-name').unbind('click').bind('click', function (e) {
        e.currentTarget = $(e.currentTarget).parent().find(".fa");
        $(".div-impact-content-plus").removeClass("hide")
        $(".div-impact-content-minus").addClass("hide")
        console.log($(e.currentTarget));
        if ($(e.currentTarget).hasClass("fa-plus-circle")) {
            $(e.currentTarget).parent().parent().parent().addClass("hide");
            $(e.currentTarget).parent().parent().parent().next().removeClass("hide");
        } else {
            $(e.currentTarget).parent().parent().parent().addClass("hide");
            $(e.currentTarget).parent().parent().parent().prev().removeClass("hide");
        }
    });

    $("#div-price-overlay-content-container10").find('.price-sort').unbind('click').bind('click', function (e) {
        GM.Global.Sorting = true;
        if ($('.price-sort').find(".fa").hasClass("fa-arrow-down")) {
            GM.Global.SortPrice = "descending";
        } else {
            GM.Global.SortPrice = "ascending";
        }
        fnImpactAnalysisRpt(LoaderView, e, grpBy, data, requestId, parent);
    });
    $("#div-price-overlay-content-container10").find(".li_pdf_document").unbind('click').bind('click', function (e) {
        loadPdfData(allData)
        createPricingPDFPath(function () {
            var prtid = $("#div-pricing-request-input").val();
            console.log(prtid);
            var temp = fnGetTemplate(URL_Price_Template, "gmtPricingImpactPdf");

            var count = Object.keys(GM.Global.pricePDFData).length;
            console.log(count);

            window.html2pdf.create(temp(GM.Global.pricePDFData), "~/Documents/Globus/PRICING/ImpactPDF/PRT-" + prtid + ".pdf", // on iOS,
                function (success) {
                    console.log("success");
                    console.log(success);
                    var finalpath = "file://" + success.match(/\(([^)]+)\)/)[1];
                    var DOCViewer = window.open(finalpath, '_blank', 'location=no');

                },
                function (failure) {
                    console.log("failure");
                    console.log(failure);
                });



        });
    });
    $("#div-price-overlay-content-container10").find(".email-pdf").unbind('click').bind('click', function (e) {
        var PRTID = $("#div-pricing-request-input").val();

        fnShowStatus('Fetching PDF');
        var prtid = $("#div-pricing-request-input").val();
        console.log(prtid);
        var temp = fnGetTemplate(URL_Price_Template, "gmtPricingImpactPdf");
        window.html2pdf.create(temp(GM.Global.pricePDFData), "~/Documents/Globus/PRICING/ImpactPDF/PRT-" + prtid + ".pdf", // on iOS,
            function (success) {
                fnGetLocalFileSystemPath(function (URL) {
                    URL = URL + "PRICING/ImpactPDF/PRT-" + PRTID + ".pdf";

                    var Name = localStorage.getItem('fName') + " " + localStorage.getItem('lName');
                    fnGetImage(URL, function (status) {

                        if (status != 'error') {
                            if ($(window).width() < 480) 
			                  cordova.plugins.email.open({
                                    to: [],
                                    cc: [],
                                    subject: "Globus Medical – Order – " + PRTID,
                                    attachments: [URL],
                                    body: "Please find the attached pdf containing details of Pricing Impact Analysis <b>" + PRTID + "</b><br/>Please let me know if you have any questions / concerns. <br/><br/>Thank you<br/>" + localStorage.getItem('fName') + " " + localStorage.getItem('lName'),
                                    isHtml: true
                               });
                            else
			                 window.plugin.email.open({
                                    to: [],
                                    cc: [],
                                    subject: "Globus Medical – Order – " + PRTID,
                                    attachments: [URL],
                                    body: "Please find the attached pdf containing details of Pricing Impact Analysis <b>" + PRTID + "</b><br/>Please let me know if you have any questions / concerns. <br/><br/>Thank you<br/>" + localStorage.getItem('fName') + " " + localStorage.getItem('lName'),
                                    isHtml: true
                                });
                        } else
                            fnShowStatus('PDF is not available for ' + PRTID);
                    });
                });
            },
            function (failure) {
                console.log("failure");
                console.log(failure);
            });

    });


}


function loadPdfData(allData) {
    var array = [];
    var JSONDataPDF = {};
    JSONDataPDF.logo = "data:image/gif;base64,R0lGODlhUATgAYcAAAAAEQABOAQUUAAQawgkVgE6dyc6WCM7agBCXgVBeC1BXi5MdEZZd05kfGdwfgATggA+ggJNjgFcogVkmQFtry5XhzJplS52qgJ7wyN7wUdchVBrjUx5pmh6kml8og2Eui+Itz2ivRGHyAuP4S2Uzy6c4zih2Dek5F+KlE+MtFWium6Dmm+PrXKkvEqZykOf4VCm11iu42ubxG+v0mu45XXB3HfG7JovIrQREb0bIq4tHqo1HrckC7ooGrY1BbkyG6spKqw3K6s6MrwqJbktM7k1Kbo4NJxBPqpCObtBKblDObdQPaNJRqxdXbtKRrxIUbpUSLpZVb1mTbxlW61raaxvc691crxoY79vcLxzarp6c8kMAcYIGckZAccaGdEBAdUCHdIXANcZF8cEIMoaI9YAI9QZJcsjCscpGsUzHNMnDtMsF9Q1HMcpJsgrMcw5KMY3MtYrKNUqMdU2Ktc1MuAAG+AuH+MtKuMtM+EyKeI0Msg6Q9I8Q9FBHsZAKMRCONRBKdRDN8VIRslLUcZUScZZVdVJRtdMUtVUS9JbWsxcZMVkWstwW9diWMhnZMttcclzacp5dNRpZ9N1atR6dOBXS+VlXeRya6x/hM58gtl9guB9gLWBfs+Fb8qDetSFeuCMf4aOmYSZspOmuLuPjbaOkr2cn6OsuKGxu4GcwY6yzoq746KtxaS2x6a91rK8ybS90qa+4pLF25DS75fm+6vDy6jD2KjS3LXDyrfI1rnT263W7q7n+8qKhs+LkMuSicyal9SLh9mUjNqZk9ieo8+rmt+gj9ukmsmoqtuno9yyqdy5tOOKieuPkOKXjeSal/OMgvKbm+Wlm+qqp+mus+i0quq4tPKpp/S3q/O6te2+wfS/wd/GveTBq+rHvfTEufjYv8DEy8PL2MTT2tnIxdHZ28LO4sfa5sTc9NPb59Dg3dXs9erLxevL1OvSyO3U0vXIxPrN0PfVy/jZ1Pjc4fjiy/vl2v3y3eLp7OLs8+ny7en4+/zr5/nt8/726/7+/iH5BAEAAP8ALAAAAABQBOABAAj/AP8JHEiwoMGDCBMqXMiwocOHECNKnEixosWLGDNq3Mixo8ePGfcJFMnR30GT/lLymzcvHjxt2pYtI/Ys2KdJjhYVcqKk5x8lSYLCCbTnDRw4bwLRmcO0KdM+c940zZNnTlU2TrHGIdOma5AiQYQIQXLkCJMmVKxwImVqmb5+IOPKnUu3rt27ePPq3cu3r9+/gAMLHky4sFyRJB0i/pe4oMl/KP3xu2ePJbxqyYb18qQlyyJDgt6kQbOlS5cza9rQcQOHjmvXcZbOeU2baR7ZUd/ojorV9pw4efS4lkOGDJ9EkzYxa/Zs2rRs2eC9o0fPHj1+1/n14/fYsPfv4MOL/x9Pvrz58+jTq1+/d3HHxykhS54nz9syZcOYUUo0iMiQIW608UYbaKDRBhsIJshGHwhCNdttVUVIVYRLLcVGVVJZRdVSu9mBoW4DtuHGHpQQY40283AH2YoorThQdyY1xt6MNNZo44045qjjjjz26GNCKN3Djz3vfHNNM5tQ4kghgsDRhhdmtBGHHHI4NRUgVTEVR1NYIRhVblJB1ccbWYL4BlaAYJWhVVbRERxTdLQxhBFTeHIMPELC9+OefPbp55+ABirooIT6FZk/9rhkTTXObLLIH0QUMURXUsYRW2xz6NGmlV3OcSEdeIBKh3C3WQnIl1KlCsgbfrwxJlZ59P+2aoV5mGFgEUpMkow39sTXXaHABivssMQWa+yxyE4ko0Db1VNPN8X8EskigtBhBhhgmHHHbFEB4kerVjaVJpvhYiUcbbW56Ru3TO32qoZUNYXUDz0IAkkw08CT4q/J9uvvvwAHLPDABH/UYkomDamNNcn4IklRlBoYLlMXVpzHmCA6uKabS1VVYW6rmrmbx7dV6JpvEc52MqV/NCJMN/L0qidByxZs880456zzzjyHR1JklLmTjTSeZMHkk2aAmscdwcXbFFQZdhnrhFmCKbJuSO3m6dZcNpXhGyY/SDUdR/0niCTBWDPPPQjHyFjPcMct99x01203QvN9U80wlBD/UgQaPaBxZlYpXzXx4YfjFq7ivnGMoZpO4RZhgTpAEUw14HDHb0E13+3556CHLvro6CVM5DTBRBLFH0+2UaXTni644NVfIg6nmZHHW7iGskmVbrldzwYHGkZAIsw39bDtIunMN+/889BHz5BImxuUGKLwWDPMJIKcUSAatdsu/vjk284gnOrO4SCchrNhoBGS7Kqi9PTXb//9+CPbeedACjTfNtKIxBUE4QUvbKl8CEygAinWlN7pxlvoI1sP/AAJY7hDefnLoAY3yMEOsmcf1VMIXF6UEnvAAxvDkAQfckAGN9DhdbBboAxn6BSnaa1dbwDEatpAhEUMQxuaC6EH/4dIxCIa8YgecU9DJMMPd0zjE4z4AxrUsCU6KIWGWMxiA0+WwwzhAQ+acgMaCJE2ejymHyThHxLXyMY2uvGNjvnHPeBBjWEswgiTagNV7ECxM+VwTGPSoiARCIcv9SFeeJBDlBohDXkIEY6QjKQkJym9xiBKHtgIxhS6Z0CwsSmGgwzlAuFwqjzwMSpDGEQwtqG5txVkhJSMpSxnSUuAqbEf9tDGMSZhBB6wYQ1YWV+bSjUxYorymBNLHxqKIIVjgMNtC2lRLadJzWpak09Bkkc0KBEFJ7UhU3ao2NZyiJSjtMtKjEOmOkvWBiNEohoyU6N7HnnNetrznvgMTAmtcf8MSRShUsCDnW78kIRWpQpVN1QnMpfSAyNoAohnRMg+Jvo2NebzohjNqEYxgp1qBKMQbTjDlgC5pqiQrTYo0xC5mHIqharzDEoAxTf4MRBLLm+jOM2pTjMKI4agJB5P/MOkPFmVMa0qcr7zJMpS5tIFprOBgdCN09Dwh2GkaKdYzapW83m9g8DSf/eQxzSwEIgtnKGpaC3fU5nSmkxV5QyCEIY8vrrVutr1rrVMDBoRBo9kPCJAbviNDtNK2MPdsGoOclMe1lAEYYADg3iNrGQni8SJWvIe7qgGJP7gBdWstbCgPefhqIIGOHzCHdCkrGpXy9rQ0dMx/phHNTqRhB//SEkPH2ppaHe7rvBpCQ2U6EZKKNra4hr3uHYjrnzmkY1MPGEI4BvVhKLiB95aN4ZGAdsQGAHPkSD3u+ANb8AWcz1/3OMbzDgEGcwgvqpZd7d5iMO59oArafSKIHRNz+b49Vrx+ve/ADavPKQBCSex64DU1Y1i37vb3egBD3yQwx56MY9ffbW/grGoQjAM4A57OLL9QFQ1QME6z+ZmTKXCHYN5m0OV5UASrLRwTT9M4xrbWD2SecczCiFFOVgxal30mNdWzFtNzaEIS0jGfROi4cGQlzGWjXJ+b0zlKmPVJPfQxi+UMIQBzeGoR4VXDcnVGyKrU3FLa0MkvBGf9SAm/x/pEEctXoGKU5xiFKLIMwtEsec9i2IUdj7FK16hC3Sso8lWTrSi3fizsE5jEcODk5lB695w+Q63tRtCFK4xP0TrhV/5WIc6zBGLVLBgAxpgwAIOQAACCEAAAxgArGUta1i/mtaxHkABFlCBDWxgBaJwhS7OsY51cI6E0ly0spfdvO40ponCgMIPwOe0z046lJWeisqspIc3JCES8uj0X2ZGs33kQxeoGMUKNECAWhOgAAc4gAY2kIIWyGAGMlDFLUw9jnGIw9/kGMcrRqGKUbBgBR1IdQIK8IACtDsBC9gAoMNxaOsx++IYH93PEPUNTzihCHD4Q29Leu20lvQ1nv86ZB7a8ARlsI3DGakewqwH51yIYgMLQAABAjCABGwgFKIQdKH3sQ507GIWM0iBC1ZBC16gQ7k1nWix0WEOVcwA3xWIQAUgEAEIPODdCmgAC1pBbOUq0dMZT7vaB1W969mjGpP42xt8nL4Glvy6opIKgnhAiXf4z7t1CaGz93GOVoiiAQIIwLsToAEZCPsc+UjMPtDRChWAYAIToIDmKXABFdiAF7zYRSt0EeVbqEIFF8A85m1QgwlIgAISiL0EIrAACBRA5wTQgAdGkYvGpHHtwA9+sWBJ3NheYxFtSEMOm2a4uxP5TVFJwx+OYY+o82Vz6zDH4RfwagJYgAWjcIX/oQ1dbKLnAx3oUAUINo+B9rt/8yCYAS2QjoJdEN0WLUhBCEBAgtTTQgUR4Hqb93qxNwG2x3gesGcdsAKwgA9oJ3wQGIE3wj/+MGCO4Dq9M13qUzvW5nxnRi5FcAXsAHMxtxDmZguiUAHdtwAr4AplJxJoRHQ1MAM1UAO3UGygNwusBwMgkAHtRwEYQAEg4AIzMAugF3qGBnq0sITzZ4SzQAMuAAIXUICyNwFLJwu74HSxwAE6dwArgArpMGVxJIFkWIbq0Rj9MA/O4AQ90FZ15zVa84YeiFYZMiZD4Av0QBhEBwseUAGuZgAdMArmAHUkARe8AALtNwOHtg68QIOz/8CE80cD92YDSzgLsoBvNcALs8ACF3ABIPCJSqcKmjgLj0gLq5B0nlgDlVgDKWABrpd6EbBwBBBxrJAOy5JsZpiLuvgdlgUZ8TAMgyAGQ8Y+HAIiU5Ftc3hmR4UGSqAMNEUQJHgRkrcP4iAKGvBqEdcK5XCLUOYP1MgCFBACs1BssyADH7B5QkiEu8CIvLAOtAADU7h5LvCIMYCOGDABKkALmAcCKaACNWCETccLqmABERCAsQd7sDd7XbdwBeABr+CA+HVTuziRFNkXCCMPw+AEroNp6rMmmnJQychiOZQGjjCCfpFG6cAKDXAAAXAAHoAKT/d7roQY6IB0KtACt/9AdLvQAiHgAi5AAiSQAet3AbIwfzIAA6tQiuW4fpqnirOwfudIAfKXeZgXASDwiJa4hEmnegg5ewpZkAuncwsgCoP4gBV5lmipESkhD2vYA77zMVzTGx8jhyF5TEylG2fQCRUmkXmxDzaXAAOQe6pwDhRlWYtYbIxYdOuwC0wIeoyYhZBYiY8IejOgAinQiURIifM3A5cHAkvoApung5uXAZk4CymAeZ3nhK1oASDAAUonAykgAyrAAinAARZQAQvgcB3ACsaWlr75mx6REvOQkd/EQGATNcM4l3VJQxvTW4wDB0ZgDNXHl3Zhbq6wAQggAAfAAuO4iPiQD9+ZD6H/Ro4y4JNRmAIpwH+fyI/7l5496QKexwtNWIryOQszeHWzsAtHh3qqCAOaZ5+ZBwOfOQER8HroWYRHGHqPeAuk2KCkmIW7EAuqwAIdIHGECZwYmqH9gxDzIA1Q0GUqtZyFNTIcczJ0YBSlFBV7YA3POBf7sw6tsAECQAAVIAOxgINEtw/6YHbrMAMU8AEfIAIjIAJESqRBeqRFigFA+gE2YANR+QGfSIQ1IAsA+aDoIJ81EAuNSAFIdwGUWAOY+Y+TOQurUAMysAqh14pV6XoT0IkgwAIykApUGnquMAqjEIYzpqF6qqEg9A/0MA2FoAN/UEgrJaK71Tsf4zttoAjf/xCNFwEXMYIPqMAAircBrBCTaORKNLMOsoCIQyoCQVqkItB+o+p+7Rek8UcLsPmTQEkCPaieLgADMTALt5CFR3ilRch0AvmPTVgD8Ih5sicBKUCKLsABN8kBAeh6XlmQEaAKj7kCCCAKF/oPYrin1mqGiGFe1dAIIkI2HClahhpaDyQV7OQIjuSiByF1o7AAiteCT6epMrYPt6ACS3qkQMp/PimrNLAKNuCgDwp6R4eVS5igCnp0t+B0ikmw7MiOtCALVHoLDHoLZWqmM3CEh9aIHECgBBh7EWABN4gOorAABBCt5UB813qyafdIJHEP1rAIaAAroBSuhFU1ImMbq/+hCUt2F4+RD6pwjQTAArZQfszCIpyTClCqAipwdTXQr5FZifNZplc3A/qpCqsQC/npdO1IsE1HC/p5pYmJmIipn7JAtTOoCmZLpQ7qmGDLqRlboMyaABHAAR+7ri0ZCrYIeH+Hsnp7cdezDZQwBFuCIBrIBrolsy5FTCTXFG3ADNVnlhiRDzGqax2gC4h5PYhRmFJ3dLYqmatAA1enAj7Jfx8glCSgAjTQr0YYegrqsPeJtC6wfyAQAv44C+TXjgmLg+gAsbJQA5Z5AQl5kEIIAjLgdLqwC+gntqsgA7YZi7eXClIXstp5p3s7vWuXEu6wCXDgYwxUMSFjuGl1Ml//ciFCZgTHcA/Uqqlx4Y2MkQsdEGseIA5SZ3aMcZj4kA7iyYirAAMwwKoiQAKgmgGjO4RXl5RKiJU08IghEAL+W68/igEkAAP/aLxdi4S3WnRJaLFHOH8zKAOwWJAVywEVYAGeCANFeAuViG8cAAErYItEJwoIEAAMwAr5QDPQSL02TGMhNg8kFlhg5BSAgDHG2FveK0ocUkpMsxp/8AxtVhcjhA4sUAACsAG18GaHNlHiCZ6Vuw9wYVm3kAGtWqRR6ogDq8FX95MZAKoCyoNQCqoiEAIwYAP6GXqXiKBIJwuNuAoOe4RYyAsz+I9HuAtYKJm8ywH2WaAIWZASILwz/6CZZJoKhDlR6rACBhDFsOC4N3zJqxVi9iANhCAgJ7MhVpNQyDjEWeRAppQHgQAHTmANIYa+c5EPrAABA1ABzmpZoYYPuHzFhXkQ6KACJWACR0vCk7mENkADMKDAQNm/rdqqlBilS6eUp1ibKoCVlqeKNUABtJB0fGyESbuErHgBM4CmvDC2RymmAYlvqResHCsBE8ABRailVTxRusABgWm3SoTJ+Axe9mAN/qQy3VsmHMguZUbKg+RAbVIETlANEWXJELEP5tC+B5AKMYmY6hB5UJcQ6NACsfvGw/yEMfACrVoCyzzSrjqE80eJoHeKaiwCMICVPHgBmRgDH3AB7/9IAaqYAnYMjzHgzRkwASAgC495CzPgpsPbdPmZv53otl8ZgPUWC+RHdK2gggvQCgydz1atU/7gDpGQBGkQGycTMhmSJbdxB9+auARNQwa9Gk6ADeZbiHjrEfgwClC8AZQ7eRG6CjHpyjXMGOiAnwD7hPu7zKxK0iMNwXZ8pbPwk6ILAi09fzAAqh9AA7xAA0Cajy8wurTQetbcpvKXzapwgraQhKvAAld5mS5AA3NqnxzslQRakFx3AaqwjqHmxCzZAeZw1bhtVzJSQhk5Ml+CG4lLl2ctQ1lyoifKUhnSBovAZnhBjQ4gADWag5WZAlya1971M1AGZfKpiTQQA1H/2KomQALhLd5AGd7jTQKyq4r1OQM0MH8mAKul+NglXbFOCqotQAv+iwHtTd3t7aRWWLkjYZjsqALBegEpsMgnvLwFyrEQYAG1XGyygJ0EcAozzBj7ldsYjk/Xcw/VEAVdXajDPbM0C4ftUgiNiq4DkQ9yXQAcsAqrEAMgMKpA6gLz966MIQst0JsjUXSa6KuDXd5Abt4mMN4/GasoHYnfDQIv4M0yUJS04N1eHJT0fcYj8AE1sAskQKqZrXkC6qQWUMW47HtEx4jl+LvsbAEpwKuzoAqtiMhgyQGkR3ToIAtP7AB3axLVmuF6Tk2QCg+eIARJYBtoEuIrdlR60AZR/3DictEi+zAO7RsBFxCFS0qkPaiKILAKiNHF2Kyj/cCOyfuJQW7e4v3LQ17eJhCr7S2fncuqPdmTJDCPR7gK3/3qjD1/MU4CVV6UFGCZKXCwu5sCAmqJxAWeFb7j+QCxmjjUBLqm7TwDdmyfKcCsXVcBrhDPi8kCDHAKsOSoe97tbGQS9DAMStAG1QUI4eQpZk3oouReYZ0HuqEHciAI1oCLG6EnE9UKCxcBGbDvoSqqnrkK/Me16me6hna/NRCFP1kCCo/erz7kJkDqp/6eB2zUM2CeDi/eJqB0Uqu6T4jwrz5/rToCJPABlpgCMWlukbcOFgDUxkZe9WtZ+KAPjP9xDrVwpZkd7cD6uxZQhPqZCheAyBDAAucQ4IspCqFQcfrwK9zu7UyfQShhDYrgQmziJeouruMivl8mB4ZADUsfTTWMDytQAAkwAWcsqqL6AQJqeaCOoPG8D7sAAkMO0q0Kn/vrAg5/6j4Zzlvr47H6Ai/wk6VuAi1dkxU/q6ruk/P34xkgjkZIXPvggIfoCuaGDzMmEutA7L8ndYxoAymgzuxckBygCka4CqdpexWQC3/HiPs2rU3f+pPkD/XgCUEgB7hlSotT9YcLJjDLVn/A9W8NErnAAAUQARRQ9mbvvyPf3q1Jg/VpuxPVy0ROA2YK1ERXjvkaq6sAoUiXryH/EPd+v78v8MZNd/B2/5Pif4QzEAJR7qopkNntuCLmtg+y8AGyIBIx34uaf/kSldo4r6zBGgEAkULWrl0zLBQogOrfwn3rduXagGrfvoUVLV7EmFHjRo4dPX4EGVLkSJIlTZ5EmVLlSpYtXb6EGVPmTJo1bd7EidLeNChD5szJE/TnUDp08NAZmlTpUqZNnT6FGlXqVKJI5/jpM4dNnjlF4STzZ5Fiy7AV97FaUCBChg8i3LZ1S0JEBhAgbMyiMYsXrRkwQMyY0WKGqhQhQpgIIWviOoqN16GTMcMGL16zVsFwAUPzC86aTcB44WLGrsowTJww4SLECxMkXNCiXBmv/4sUM2ih26VilD6G+dbdMsH43z7fE/Mdn9jw+MV+/3KNkrV3VooJEyRIoHBdQoQEG1oQrGHhAQvhDVURECAK3/CFzXO+hx9f/nz69e3fx59f/37+/Tn2g4eSHOLQio2l4CgqQaoWZLBBBx8Eao43JgSKjjf+ICIafvwZaybiRCkAAgkucKtEuOQioa7XKJsFMxBIgBFGGWSRZQbDdumwLIb2IY2XVfrSLEjNYhAShhlW4WWXGlxw4TTWYAghsxp4maG2XdahjBZasOTxHLHWkQUExRZbp8yJhmtsH3zy6VAsdFrQa7rttLtuggggsEAFvQyKQBXGGnJFAwE6SMcs//8ORTRRRRdltFFHH4U0Upaau0eaPeTQgw42ANFqqDcktMoqCEcltVSqkHoDkK3yeAMOIZzhhyGaGvJArQsyKDFXEeT6AAQYZoFtFhdIGIGEGWYhoYQSTDBnuHVUqGEsf3QczrddVCnyhRiI5BYGIkVjMYbMYFDBNBhIgCGFFiqLEgRe9kFnhhpWubJMe/Upk8Ur1UHOzIyIw6cxi/pZB4UIVCBtFQ4ioHNOCSCoQIVbqLRAA4ka2sWDABgQp01JPwY5ZJFHJrlkk0+uqZ9vFuHhJ65+ikMPprIy0FSbb8ZZZgrzyHSIT2LVyGOU9jlnAwIioEAEuEqUKwO68qJlFtv/agChBBiBJaG1WcbKJYVbKJoWTeKOA1OFGbbd1ltvuSXyWMqA7FYGGWogKLp1ZjHhBRJSYcyWwvxMcx999NmHsq/bHBvsiyZa06KwGhqlutFoqeGCCKzT7nLuIEiBtBkq6ICxx1IZwABW2EM5ddVXZ71111+H/SR+iPljjTeyGirToiL8ycA+asY5eOEbtOqNoO4ggpJ5UKcpl7QiuGBppnF1IQYtW6xrFhtgDI1cw1wQ7p980DmToXsdgxtt9bmlmxcbiAwyBm2vTG6vVWhw4QRaVNCFR15o7JDi4HWLPy0OH+sISwLNQpzwlWUiu7gTBWyzixZUIAEM2w4G71QB/xbohQXkacg6WFEAApxCaLFDYQpVuEIWttCFJTmhP7YxCTIMz4Y3tOFRbpeHO5DhCvSYVUVagZAJsEVpuuqVbdwHg125RnuHcU2UMOMCxRiqN8VZ3C5ksD71zYAGSPLR2uCXtlnoYiHjm0He8jYLFaigRj1aRzrGIbB/PGsyM5DFOv4RtomoQ4+LY09xPJaPWkBHBheYUm40aKfMJaACMuCFLVigCzPVQgECYIF7XrhJTnbSk58EZaM6RA9pOCENn8JhKlUJod31bg5OcIcmPTTEBBRRVyUCAQ20ZAMXvChGJLABLYblAjfyogZRkkU+FpcPfATshPlQhfpogDYvxv/pbGkDTF+2JQMcEedNpBHMKlSxi1W4YEbkFEib1pEKF/CiBaoYiyaVg5EOlUmZgNzHLLaDAr3Y4AIXjIAGA3onC0RHFaMgH9EEFYp7htKhD4VoRCU6UZD0Ix6UaMOE+oDKVXbUo1D5VFDogIRuUEsm+xgFASBgS7eMYC50iQGwbGACusSoNSSYkgrG+add1OIciFOTMzlCpW1N04uSocyPigoYdJRpF7JwAQ14YS+x1aKKDRnMY2Bjr3Qok0cguMUsACer5BRwISYN6lik9Q8Q2WkCK2qBWwc6VwgkgAMEQQc6dIEOc3AgAISiaGAFO1jCFlZ1/KAGIdqgKUBs9KP/j4VsUn6nBzzkoBhhk+VL9nEKAtQSV7ly2q8qM6xf3hQxMEjSmRy4VjXlgzcdmUiNjBqDvOylBl7cFlPrSUEwnoki51DFT+sYmMeo4hayUEULWtAKdEDVWLQIgQySqwpZzEKsE2ugRpip1j364zf/s0ACEFmZFEAAAhGwIEC540hIooMFBFAFOjwggA3gI7OGxW9+9btf/tLEH/QIhhtcZqCXRdbAHb0dG/RghmAArTknVMlZVEqBDGAAtC4IJvZ8SawY6S0EKpjqWcMiS32sqXwegZe8aCCZYHlxmrSdxR91tI90kKYGTfUjOloUmTKtQq9/Skc6yqoKYsGAFjT4/0CSlZxkCU4VwjsSMmuT4wrNpQBYg7lFc8M71wQUYAOkYYEAPLCK+da3v2dGc5rVnOYcZWMRRXBlhQ48Z1W+YStxUMTyYjJjVgwAehZ2C1tyqaUYkCADv4SR1fSGmBjrUYFm2e6T/7UOVXgRjKuoAQ00rekaZNcs6FhFDAKj3GvGQGKpLdNrxcILE5QABNaFkQiKhcsPzMDJGJnxPK3oXQtgRwITsPWf4GWBud6pABygjCocuQoW/LVQa4Z2tKU97U1Kix/J+EMPbqcVrhiFzt9O5R/g0Z6TotTPSQM0W15DORJ8QC4laGKyshaaEMyAMQFbprBJso9b1EAvPgLM/f80jccTswdjRp3RKmygLSPNwGyy0OsohG2vmbra1qQlFq4MLeg8Sto4+K7I41LQSA7YYh1s4rd6A9rlWOxiFrvgQAJy0WwHFErS1MZ5znW+c0VNax6/KAKrNkrgPNABQeBGOs4C0YZpmBQm+zgP0kRg4Q/QJcMhqAuximW1GC0rNS6A5z76gW+KkK3gIsEY+dBxC1cMxAY1gPuYknOmfM1rFS0oEy221QICUgkGV6LSUVfMpGXRxpgwKlas5SICEIRgSzcHmMf2MfJf26kCt2CmM1mglrkWoAWyqACSUlGBXbx3AzbneepVv3rWx2da34CCEYwnIU4BhSscTXruWTn/BGFsaFaqCBGFMaBkI+u9V/EuLQlY8/WtjcWZyrk5is0UuMKpeBUvf+o4GTiRqa7jp+uwgbxUocx10CCPZVp4aEIjA9BsyRawVnxLefUBEJ+dnmoK4C2uk53tpCI5+CoTCzivgKorZLMAAUiFa+EAVfAAAtiAhmq9CJTACaTAkLi2JaADAnkDpKCDl1EV4NG9EGSQNsizx5mJVlCpIlIyEZiMctI45COtElA/E0gNHKkIffANfxkaLFocMNEeTIO7Y0k1VTML2XKyhkhAxoCMWWgur4GXG3sgYYoRENgVl5K1JLO3GBILHtwHFkAaOrkAFjicEGqBAlAvR+IFFKiA/1oIhV2IBQ6QgQogABaAwAq0wzvEw5yTFgAzgsW6va6IGQnpFBEkxKb4FKsAhAkRhHGbMc16BT8jkQ+IHsfTO7qowl8KARhpNTX6uipiiHQIn6ExMceBNCyZBbh7OyZch/V4tDpSqo5rCLvRIr6BlxbwkobAhXJQB1WgHBAwAV+Ki1y5ACQhH+9THLFwlntaB8rjv4fhgFuoFyxJAfU6r1lggRbYB3XYgIHwgAhYAALwAC3Mw3Ekx3KEKLTaI3mQhMUqxHZcEDuQkD/IwDxggz8YAmm4r5fAhQOAABeoMEnUJV5yt10hyNJqjbzhjM+AgbHKwVDcN/FZjo8AkxqwAf8b2IWmOgciZIhY0DRRizF32hMYWIV+mAhbVCt9SIeJmYWs+6VcQRHtUYEUuABdCJt/aaaJuIXLyQ7rsJOA4oAW0CleUAXzIsAa2QCKyAUFiC8W2AA5HAXuWoh7cDpzpMqqtEoV4gdtiIIicINQwT13BMukuD044JTbcwSgOalySAvV+AAKeDXjOz4UQTQSwDrVAI3OEA17+RN0aKaTQA6RuJtVWIXpExoe2bQVGwwZ0IttWYXGaByy4j52i5EmypUPeI1VmICx2oiGCJh14ADr4D/rGKgEIM11mYXwIs1KGwBWmIh0YAFR0AVXcIUDGIBWMLirxM3c1E2Tkadp4Yf/aVACIwiEpPjKsDTOCKEQO4iDRZxKlQibfNiAAZgbCqAAGYgaGHBLubxErtOlVaCcFCiXuYnGBzKuOJKVkIi8kXig6CAf7ZKFTDtMwLiyGGjMkJu0wvGlEbAagrTCWuMFblqMJ1MTxpgBzMGcXwuonuQcYEkBBEiAW5ABhJC4iTgHe8mFAyAAWIi+3eTQDvXQ/QAbewCFHPiDORhOoijO4zROxyKDaGjOSVmBAeAAWgCBCZiS7aEAQ2M8ueQ6GFGBrXKILDMTYbsFVXiXEMoRFDugDbVPhjAch6yIdbgfGzjMFYua3LJBpyuLzqwSxSMWpumVnLqtFFiPf6kWh6iO/4apkwQNKAtAkhZAgFmQgboCx25ylnVoBQFYAC+hlhf90D8F1ECNCbCZB0pIA1RJClFRUeNEJXqcgzSgBCDykFEYgAo4xQmQhSMLRhRBPk0cJrMhoBB6DHu5BXk5wsewv3+JSJGYMYeoAXWAysVQuE2jUk2LmrNZMSi9iMHhPmF5kU3FlbaIGuq8gPJgHrXCks/8NTVlGM0JKCOllxQwwy6rAA8IBVGohRAinQZQpj4V1G8F13Admj2CB0VoA1fCPUVdVHcMhE+xAzt4gyVwBz8tiRlzBZWCJCvjBRgAtE2dy4NkkoBVAThJEsEEjKUajeZqquhLHJR4jFmQBQIaUv9QBJOKpNJapYEWxC1Rs8GKkCX8Q5NdSAF3iz+3wIDXsIHq1NXFoQzqqJOGcdaV4yZe4AAHNS8IGAAZZQAUyJcOGAAP4A0TFNehJdqiBaQ9YodCsAo2+BTc4Z11DcvkzIM2GAZ6TQldIM0KiBqXc4G2sEJORbRMpEHWGBe3mQVRMxK04TTAUAUkSdUdeUy/bMhIqq5buAVd0IXKoMi3u9iMrYyDxVLEGdWGUKsUq4tfXTylsQ1zCsXmbIVMrdEDpZOYvRNI2gUBvBM6baoV0IB8qYABeMo9MtrRJd1wnZZqUAI4iJlEFESoXVeOagNHsAfmcQni0IBaCgEW4YB+9Vf/RGuSsX0BzJAqytmibKpSTatIGrjIlXUWjVRPkJ27p+qmWGxbwUTFGji/38AtGuCmT5OBFBhMf1ErCqqLE3kpYLkAT/yHfKyjBZiSFLgcnpwTNiVAcooODiDKBCCAr1mHFfAA37iFBRCAXKCj0jXgA85Nf6iUPQhEjlJX1zXO2ksCa9ARq1XPFTgaG/0f6dnUTj1INcIMayq1FTvei63IehEaQUqJVRRciENW3DAH3HjP0dCjB3KxGIAnhgASc2JCdGCTBYqX8k1cEKAFWUjft2UIVbCAGqAF+E3TNWUkn6QFDgg9XpDWECkADUgoB3jKdSIABvASBBbjMTZHfjiG/yKIgzvQA0Dwg64ouqLzFAgGS65AA0+IleawYLRjhc4CNvfh3YLsVE2kwbzJDNiQhS1anyo14Yy9C9EhwkhjUu0COXnihTAmK/MhiJMzi4LYFhwJi/KTH3GxtRlgAeEC4pX8AAsbAfqjBeyEws3sQkxt4svJnPnljuoiTQgw0jI0ryz2knIIgGxdxgFYAbEj42NG5p07O3+wh2AwggeW40Js1KGova2gAyVYnkhGu3NYAEeyNV5wAQrwWpdEPqvxOiYxm72ggSFRW9qi1Yxl5Ir0McZBOR4UxRMq4KAhDsGFxgA6G9DQDFpYBYEQ3cc5E3OQBSZa5QxgYhBwy05LUv+DQwfo0YuF4T9aXjlkm8YLKgBImgGESIABWABXWAejEWZBGYVkVumVpjY8nodPGILiSdFozj0CI87eAYQ4eIYHIwvz2QAz/OZZaAsj6t1Ey5pzdgEkkRoxKip3Rt54tlgbmOdk1MEVrsOSALmzQp0H24caGJIQ3oWkljyzWAUMcKleoQUbSGUKwEZkNItfs4CXu4DMEShy4o47iYCOFkqQLoABuJYAKAC+0QUDMIBwoF2WRuzEJixNeuln7kAJmWmaDss0KIR5YF+ToJZRcFBbw161nouWokK5tKmsYRIYmIylLjXacmp4juqoti7fshdtDhrmRc8fbtK1Wge4wYz/KXGBraEnNJmFyjSyGBDnCbCFufPY4ZCBX7sAl7uAJ94OCLC18KqA0UyAFPifLiOhGchT9NgAERIAblXs8SbviXIPf5iHSIhpObiD24tsyda9aZ6DH2i6mRAHAiAA66yBCaAS3p21f503JoENGxCSdlbk1rZY7Ykx42CTk5PtZUJikABZsTix3L4mtbGN0cjnbCQcWdC4tsCAYEoB6vwAvivMZ7GO5r5czAko65QBlbsgCDg2H0kABCiABaiAArBxTFqH+Urp8gbyIOek854HSIADTGlveHRa+A5BmfmJaZ5aSJjdlyiLfHAAAUgBynHLfbWwzwJbRPu64ltntTHw/8M04QSvSAU/0vBJKxia8Hu+iHWwhYEornYO3h+F2H8RBVRYB1pwmhLBACJeBXH+gAko1vXFiHWoUQlANsz8Nc6hnAHksszlAIjVbgJACAwtAONKgAPIhSYV8lAXdZSJp4qoB0hwg5jJgzhwb5dh8tyjgzuwEApxdSPYhjwuiVEIgBmtAep0PBfw7xT5VyYZ3hnIDLZp6gN37bu4C+tC4S108POUvge3ycWJF3mRBaVaHxjgu10wh8LUh1ZggUJrt5bCABDjVwxQ9xRoEwXaBdF0l1ngDhCrgWIbTQL0sv9BiH1HCAIYgANIC8DS6lEn+IL/mHYv8q4ssE1p1AJ79f9vg+OrcHVAaINImF1q5wiKMIcDOADrGj4KsB4qHAEvD+TWiKpbjR9pWm0ET3MFz7BbWxyAuerN/EuVWI4AyifsZYxdeDFqKsby6Qc+Gg4rZpq3+AAmnjp1P/eO3SOp3AdZ8DUXQKci7smAyqC7zlwvK2IZ53cSynQCCF2DF/uxl5R6eAQy0IOXSRVaf9qH/7bHZnujK4JtuOyUyIcOIIBjyQBCzwvp+e+wjaq9KG2mVm1Fhmpmt67EP0IzDao6xOestnvnNQ/h4pEWIPNdOKOqzjXP1tG3IGIbUHoMEOd1Was6asvtgCRjuuuqXzkCvBP9vatV0PGu93qOt2Syx/3/3NeP8zZ7N0j7T5mQ4Hd1t086VFEVkSqCn5mJPa50Egh9Jk6audDOXxqWyeCl+CHzaTLzw3d56womXmjPzWSImxTQuFWJdXDe4bAFS94HXdAmGEAHZ4mFzwv/BVqHECDIz5466wF20Rd9CgCICbL2EVy3bx2ICRIurOLFq0WCCBMiRJBA8WKEiBEhFLDAa1WBAgQSHBAZ8gABDwT/sWzp8iXMmDJn0qxp8ybOnDp38uzp8yfQoEKHEi1q9CjSpEqXMm3q9Kc/e5Ta4JkDqM+bOXOyau3q9SvYsGLHki1r9izatGrfvOljJ08eOH+yvdz3z99QfAwQzIBBAQNgDDRo/00QQUKEiBGHSTBm7MIGLxsuYMB4EQNGjBmZY9Do3NkG6NCgZ9mYZXqWQ7sv8db9RxBfvnyqX+bDZxTf7Jbrdsla5/LgqhgxZMlqYdqFCxm8Yu6bkfjwh8OATVP48CEwBQkqZM24IENWhQgqUKeowUtFgokWKUbMuDECBI4beKkiEPK+yQoCaj3t7/8/gAEKOCCBBRp4IIIJKmjUPZnAQYdWaQBCFldqWXghhhlqeCEbc+QxRxuR2ANTbkCNIkAKfgUmgmCzWIfYYo05ZgMtNCBHmWWbaUYDZ5/RIBppQZ5GCy/olGgTQbXJ5hpL++DGpFBO2qWPbPmgs8oqs6x0kP9B6GgGwwwq1EBLDC+4MINvTeKDSjozZHCYCCAgxqILtNQwQWCAUUDBBNlNAIEFDdlggUSy0MJBRetFEJ5773GEAAf02WcfSgUckEoBDeSzIKedevopqKGKOiqppS5YYlSfDNFVhRu6+iqssWJIB4Rs9DFHEtYcxdo/5xRQAQzX5SkYLRbAeFgJMj7GSw3IvfACZpgJt6Nnn4lWWmmnOcSLQT7tow9sBO0T25HeHtRSPrusUtoqvRHkpSo2rDIDj5eZd5mZM9xyjrgPgQDnnHNiMCYIeVa35wQJz0DLLC5UpNAEpk2EEUYbxQdBAiKxwIsMAiAwUkgE7MLCAKPYVa7/qSmrvDLLLbv8Mswx3yOMEQ9uNaGsOeu8c6xv0PHhHGykMcmIRrG2jwcFXADCXysSe8EHionwpmImkABZs5NBG61wPHrt2bXYCkkkL7sYCVSS66i9ElGxtXQQaaC1uw4vsjTE7AyaxSDDDLtIFoIJIYCgQgu7qM1LDI0ZFjAGINBig8F7UnABDLTQ0l2iEkx0AS2yTEwxRRBkdHHGBSjHwqQhCdDKLgsckA7KMcs+O+2123477qQeiZc/zwABhx5As8Uz8cUbj1Yeb/xQDa9HifPrBU0PO5icIxgmIwiz2EnCZC5Ae5lwXVf744+hBZkt2bvMsm/sOBWUTmz6tJ/T/7hvR3a+vDWsYvg6NtBLQ95ugY5dFO4WzQqBCwZXgxmEYDHWC5gIKBADXrjgA5KrzsJqBAKJSCA7mptICuzUHolcxFHxSUDGBKAKXmxgJPYRwAr2IQoBhGI2/cgdDnOowx3ysIc+RNk9qmEEWgVvK606HhKTSDy4pGER9mieUI62AgL0SVhOq1wKEnM9EoAgezUiQWXCyLUd9chHQMqWaXbhEO2V7Ww/OZlsxoWPcJVofrTxzUHUF7e4gYYGq2BY3tQ4Ay3tAx3dYpYLSNDAfy0Gghl45HWoM4EPpGAwNbpAwjqokA5qziIwOA8EFHUR0VmsdCFRxS4q4EICLOAc6P9wXTmg5MNZ0rKWtrwlLvtTLn9YYwlz+Fke4sAGQBxRicY8JobYkJU80KEN0oBiUZ4XAQpkQHqAEcEH6gSDxCjmXyHQXg24Z4IXmIAy4Avf16x1LSGhpmwM24Xh7GgT2EBJjusIV08MUkg94k80NVhgZ3hxuVskaTb9g0EDZdTIOVkHAxQIYd60N4vudBBhe+KkBDI6gQSYJwWiq1joLnbC+8wiFgkgAAIKAMN9qGIAMcwlTGMq05nSlKb+8EYhiqCVD3UImT796YVupRVBzEMp++hAR6wJmOt8AAS8mAEGFJOBf42pBiAwAVafRRmucaaM5TvjadqpPl6wsVtoe1L/k5iUpDnmQ34nqwmXeHELiWYrbDT4Z7XyZpdu4aUgT02kjJpKgoZ+IAMuoIFDHhcCC17woh7MqOYoUgHtEYpipIRPAuJzHwJUYBeqOCkBDiCAUaBDAwQwR01Tq9rVsra1o1LNPHKqlQndqkJAAypuc+uVDrVhGNAkSi4KAIE9DQsD2IzTLGrwnC6OiQYgQE5Wy2lOMIkPbF8djbbKNgv10QKebvwJPWVysnHVhq1uleV46cbdsIbNBnetlnBaYCSz/gYdz20MCGwAg6WdiUYMqwE6VNEnhzb2ghn1IAktwDDQZXZ0It1spFIhAJOsrqUruKE8XavhDXO4wx6miT0i/1EECLGlxLo98YlxthUjsOO3UUpaB4t7HWx+oDSLAUFVuUcCE3hva+ZEZ7Xaa5rSbGu7liOS2TIsXtvQ7zX3VJK41IoOsu6CYQw7jV3hC8Bb/ENtMVnHDAbLGBXQDTWWmwUNFjuBGfCCaRYkcGMhq1H1RCAFT/3oCEWqWY4U4KQy2IUHJkwAVt7CtKj9MKITrehFq3Yf/uCHMIYAIa0MryvCQzGmlUiHNzAxC/dQii4QEIFqyvi4GFhYCOK0MBvomMfk3Kq09PY194YtSEWusva6azi0uc1cVFISuQ5X5SufJkvYCk0N4DsDVxSkjvtgFmmIpLZ1zEIFBcMAUz9Qo/8BExjO2eGTnDNKETbLoACjzAh8HrxZAqBSAwP4WABS0QoBiELJjL43vvOtb93dZRpuoFUxb1vMTBNcZ2y5Qx56wLykHLUA3F4qto37ATqR6QOfnIUid0xOM231nF4lX63ZqN13Mqxs9ibRkoYirtisAx1T3ta2zsxe0MjCn7YwiD5xfs/DmaYGLdBFQVKQgYmzCDAxoEUIDubtb38w3BNryAXaI7r4pNuUIUnpAWIhCwp3oLQDSMe+wy72sZNdQY7OhiDycIebjWXgBX97rPQQByjUQynmiMhfrJntOWXANCFgGCOthtXJdLxrXbXuOk2zRu0Nm2xGOvnb0IoUYcP/XKCVv3xit7WOfKgtNrJx60FakIILoACPB1EB1DKAGGyTpml5v2CfNArZzyl4FhTTiLrvg4ABWAAdqRjAoBewDlEMIBQucXHZk6/85TMfJ+AgRBzeQky3w736PMtDG36BFwwHZTYyNLdSmRow1S+MrDBozODH2XFpffy62crS4q1c5fl/lye1WQrny2xlifIfy+taxT8t0ONRyUw8GwvwR5McxAxYh7BQgArQggnoSWP1ySaFW0VQhJ3NwAihUGZdjHDhx6BtjAcMGgHoAjoUAAN8F/I1Hwu2oAvuGz14wqp0CPVZnw3KyofAga4kRToswHCR2rDM2JyAgJ3RgrUx/0YJWM2r+di0HJ4ZmQ/8CZREzR+SFcmm8MRe6cNS5JwUEtv5xM26JFtnBFBB1VEC0pdrEARUFZ0FtV63wR7TddIFWgQEsJlHwUe6kQ5+4IcKtQ4BBIAqrAMLBEArtMQKviAiJqIitpY/OMO/tQVb3NYNTmLOBJMT7QpLjAL4KVXEBUxTmUeb/UsS7hiPldP3hE/7gRWRXNn+CVSVlU2a2N8VMlwcPduw8d/j4A8Ajo++4BxN5AavHAQNDIsLUBBxGdhjOR1FrIcs8EIFSF0eZtYe2kdI3EJ9hNY62MIBLADkLaI3fiM44pBd8NIeyEHy+IEfeIgkUiI7ukobPMMhev9LA6QHEBaXJ7JZM84AIyVhVsEaKqbTdQXJKpqGlXXXKjoEOsyiTkjJUtTPu9zCLTKM2GSZ18jALKxNaxwfc6yDCwDGCABGaUTP6zkWJ23SHF6gR9QACoUUxmDMHobMAMxHoAmAB6DDBgwALrTEDYUjT/akT+bO88kBwrFFh6xjOx4lhrgBPChFLiDAcLnesLAIYhiWnXjRVZEAP7qAKXIVQNKa3BAZWQ2J/JENt3RjeBkVWu2GHnlhP7nXj8SADXBG36DhasQE7+yDDEjPQ53HwUiOJjmdRYiSeJyHuYWUNIaE1ZWEAKTCLjCAx+SCLazUT04mZVbmyjxaJPyAHqz/nVAZJVJ+JlrEgSMUjSxFSSg4HFRG5cRFB+CBQOU4l9VkJROiUxl5JfqE5f5Zzvw5xK5hoeQlxf01STrE3MxRZL0AEC+wDU+MgyqcX/RMQGlIDpxxkgdtEp2tRwIYigXgWQdanUl8zEh4lgAIgCugAwMcgC5YpnquJ3siyKMlQw/8kqXBRQ2Cpn1+RRocw06WZlCkQ59JgEMV1zUVVo3RQgX9i/YAFlap3/dISzrZJpYM5JAwXneVjUPEok64TTfiRK+lobjQzVxhGR+5ZWd01Vx6SxqqjSw8l4H2CRxilElaxOdEgAWoy8VEgHAd5jRmTEzywgqQZyxImMm0J5EW/6mRJoU/+AM8OMEReeZ9PmlYwIE3KAUriBpxBWievMgHjAkMvMnUsBlsLigMlJPHHZ5bAokUSlRBVqgarc3JMSRTpFxrFBKWGafXwIC7YCGJ8ELEzIJI+qUHgZscrgd22lm5NVhLvqQLnVQfHoAqtIAHLAA3HimlVqql5kRUOAIcxIFV+MyHVBqUhupXxMEifNp+EsU+OABq6omAWlCd1EB0bBGNnJ9Wqt90OSHYmM/iTahuWmg8QV6HLsWTAOPbUNuQjSiJ1gue0qVOJCmGrUMuTJkMuCjCYJTsyegFjtL+WACi6igImgQr7UIqqIIohFYB5MKlpqu6pqs+3MMxSP8aiQHCz8yBHyyTqN7rHKwBKMRjT5jDAiRA9ARo+BWWacjJ4nBRnUhGP24NKjqhbe4qQebabqrR490F/fymUpjVbDRPHiFrH8EXDNwCszYrlOAFtVEr01WnuGHr5yRABdRNxnjgNG7WxwwAC5gNCwiAfdTQuvasz7KnP3xDOopFfeKrDUIIIAxThwSBN/ArT6ACAUzTMbbqmFSQYSgGY+DYUwGOrTbs+NhADcgLkQwZK1KoGlFsb2aoQqKlHRUEb9jNuowPDXwHRhrFs7lAgSmEn8xetlbMxhxq7n3rpBBALKxDoIWEBoDdzy4u4/JkPUgBIHCq0RotV7CB5c6BExT/ldPmhD5sQHpIJydiwCct4MHKSAqgxpi6Wpl2ZQ2ght9EG42o6W4i5MgWYBw1xbk0GYgCIIlmiUHIaVGswypM4GNZ5wXOaOg0RAUIl8xuVgF8TMgIwHwM4gAcAALkwoY2rvZur6L5wzHoQDo66eSCZq0k7RygwSfww+bmhH8mwAVhaZ44jg1YB5zECBdVziyEgBihIrVYy91gV+yO5cSWpdo+Re42GUHUDfn0zZasrcqtQwpUlLXK3kneXgJwgLqExJ7tYaVQmAagQywM2knVG/eWsAmLnV3AgyC8Qb3awfiK6qQFTR4AwhYow/rmxChErUgKbJYSzFSWLmOYAAgg/5ZkaI3XVss/jW2WjI1EuWLl1R9cYaxScF5PnExyjWG3JAlSAIeLHlgyalTfVozp8EIK5CifhYxJFED1EoAAdECRDGJoLYADnzAd13GH2cMjuMFW9IH4vjBovgWITOkWJ42LdpvBVI4MGNfBblEQL4uNYEaObAYSo8YsrAsTF+TsUiwUF6AUA+cc2yWJ7IIYMnCTfHJQ0E0IdHEyQkwFV0xEmAafeafzigS9uQJjipb1oqsd7zIvs5ajVQMcyIEedEhP+TFoZgWJqeMcREFRIUU6aEAova81iYCdXIAWla5iJIvVuIAaPXL4wMDX/BNk2InYhJUAWyjtypOTaOFT1P/uTeyDuniGKqgGnKKqLqSAGqmAYyXjw7Qye+BopJTbS4IrGquQKizAyAQASohCLze0Q8eUP9RDFLQBreTBMBmzfdorW9jBHbhBJNwwTsDCAETADucdllIP0cEIEGszclgODUQLODuheUjkaKBRbp4tbxaJcv5iJyeFO/NnXaCDGAKQG/W0T9hXBKDGDGjSRXVSZCHvuWXMClnAQAsuAqADCwCAKsQCeHqAKT80WIe17fgDKPyApQUNRn/mG0wIiXGaHgyBbyFFP2jigcGZQ1mQCVxOdUCQ/QYxj02G5fSF3ngVJYetkNBIABukGsHcJsvEWTrFT9OEycYtZwyEawD/b/cJ2ARwy1InTAVScGCCDil1hLoQwBk77+B6QOsIwAKgQ6AdwAGcA0uAtFjXtm1/yjf8QYeoWNGm9dtx2ofUax68NTsApwZUwJ/ycBu6yDXNSV/vGCmaiSX1xXF2RkNUMlhFrJU58cs1NolEdlEcRPa6xi7Qi1ymSbCizT4UzATwyyzwiShwgFMrxPGGMR5GRAG0AC9wADUKrkgEgCvIgs4u5i28ECzcNoInOMvwAyWgAVrPQSD0tm8THFvMKzoy0x4081GkAwFYgAtUlHQ61NF1ZFTxNSOTYla5wHX3BZARCQCuCwCXrWKTpcvJ0+YBdVJQyYayhvCW6Aygg5rg/3iT/QM6/IUE8Mtu8Ivn/OWggg5LopC6yHJKvVBnBRobN8AucMCEvRRtK7iXf3lT+EM3rIr5bkUMTzg7eiq9ctodOAI/JAUsoAgMgLjrOc6dCKFKP7fgnQC0uIB5oFnXDFJk8K752LTs5nSSvbNRIwUB+sTRCG/e9I2a2FuSuoZaQuQqpICRp6eXuQbHSESM2nfouAcBbEwK9HfITLkAFM4C6Cwbb3X1MsA4gjmt17p/qMZO1gMipEExSTiaZ9o5vsEdmIH2bTHx0UAKUKsEojSeO/eJL6hWUQZiXU7e/HlAlrOazjhvYqjtLvpRrAM7dx88q8IopMm4KFml78OKjv9k00jALlj6XdiFLhBKJgXmeswo6XCELOwCYqKxpdhHkLo6G2/ALrRQbNs6wid8QxqiNPgAqP46+WqFH7DBHXCBMmDiPmzAATRMIeuJ48zAnrxIwDSSjAiemTTotAcSLzxhTYeV7B6kd9eFt9stZr/ReOmGvT3bAhZX3qWA6c3is3GASU4EVE/dSEVKCwyAKa3SzaoSCbJxLFxjK4y3wle91b/EPCxCDxCTh3iFikH8JAb3HNxBG3jDqRLFOSyABuxCQoAuBdCIBQkLBAEx+o1TtO8NZJDVRyTbOqGRdusakoH3P9QG1S+k4A95Kauzp+OtgDrUBNhCk3S6uMwACUH/NUaQ0gmxWyqhugiL59P/4c0uAAHw7NWXvum7z11IwxB8Pdh/5qSl4x0YgjwkRS0MAAZj0utNgDZVB7ZJpbP39YJqZRhdhgwgVtkAUNi6HxNTaBWqc80zxQFH0Wy7RHrDxNGcQwq4ZpdakZ5MwLsn8OO5AiqlAKhfRNHnoWZ5RAtQo9IPmgygg36E1tMj9Ah2wFefPv6bvj/MwyC0iiT6OkDMETiQYEGDBxEmVLiQYUOHDyEKfDMnziJ+/zBm1LiRY8eMowZ4mHVhAgYKJinUmHXyAwaXImDGJCGCRM2aJki8MPHiBQyfMGbImMWL16wZNGrYULp0lo1ZT2fRijpr/xevXev2edy4L51Wr1+/7lsHlqxGfxr37cvn9ey/fedYtFo3d6VLuxQo7BKrykINrKISRJAwWEKECREQJ04MIUJgCBAKEFi1KwGCApcJVNjVQgABBARAgw4gKxaBBV3Lpla9mnVr169hx5Y9m3Zt27dx59a9m3dv3793+xvWJo/A4sUjJle+nHlz5w3pDJy4JlJb2/s8FEix6wJevBNS0Jpx0i6GmDBn2rRpgr0JFz19zoCxqujQWTVo2MjPFOpTqbR4oWUXdLICax98CgTOrbF4E0sr6/IZRZeM9tmFvBFMMgkEFSw4LAWrKphgAsIkOEwwxRBj7LEVK6uAl1QEuP8sslViiSy0GwPwIBYNCBBHwR+BDFLIIYks0sgjkUxSySU98mceKOJ4TsopqazSyoPi+MS62vLRQLtdQMDLpAmeKsmuls6Tiab1SGCvJxNgiAEoWsSbYShaVkFqKaecmmoqAfVK0KsDBf3NwQYZfPCffLD65yxbQGjJJQok9Y6CCS6YpQXDBqNgMMEKQzHFxlaETIAZdlngMgFY4GWDGAmw8UYCdkklgFaYzFXXXXnt1ddfgQ1W2I74eYYHQJC7UtllmVU2WYHakGZL2vApAAIOZklBgu/Cc4G88tKkaU312tvpp/l4sWGGGGigT6qkVuGzKaj+q4pAsvbRJ59Cfcv/Z60G9zXQLYz2uSWEkzAU01JPJYiBlwpIHOywwkJdLAIIEljR2gI02EUG0DjwGNZZQwtAFVUGEIXfYVlu2eWXYY5Z5pljK3CeJ97Ig41mee7Z54iKeyO6OdDIZtrZzEEAggtocWHbS1X64FtJPwg3PXLZcwHOnmKQal0aaIihhqrwTEqp/vyLSi9HDUQwyEbxzWrlQQMGqx8KxRrvrg9I8m4CCjKYRYYRJTYMsYoVUzGBjB+7TJVdClhgF1k8A01WHFGIZYEN5qbZ889BD1300UmPrZ9piJijj51/bt3115/Nowd3jpatlQIisGAWGLadAATxvj2z6vPSG7cEnNz8/ymqGdZlF2yViJol3nnTlmqWe2vfSB+3gYSb7nN0warzjtQaX6O7HT2LlzAnVYGXW7rzToJdVpm4sMNMFPXixxa39rEBNsALVcRiFguonI0uR4AAKGBHCzBf6SAYQQlOkIIVFJKTogCHN2yQDh30YAfz0EE9gFCEJKTDCOkQwhOaEIUqbGEJUwhDF7KQhjFcoQ1feMMZ4rCGO/ShDIH4QToITYhCM+IQkfgGQEzEDgIJxDzQZxsW4I5MNYjApWZAi0uVBwOSstq42MQTOM0gXTEwo/OQYiei4EleTamX9+j2QNyULyzrkMUs7niVgeHLX6qxzj5UQCmTtCAtqnjapf8+dAFQfepwKArMI0t1mQQQQBVWeVVkDhArkhFAALfawAHQYUFRjpKUpTTlKTfiD2mkQWhziE4HXflKWcaSlrC05SxvWUtc7lKXvczlL3kJTF8Gk5jDrOUcJuJKQNABEAIZokCaOYdoSjOZecgDHAoxj7PIsSz7WAEVZ2FITO2CBhOQGhe7aB41qacmxzPB8WDggqE0z4xhAxtSVjGUotyxT1O5BTcJFSQ6eiQt7/OPU2Kxtm7iY49f8ccfbdG3CNQiLbE4pASYJgNOMRJxjWnM4kqFMRupYh0tCABmEjirALAgO7lA5UthGlOZznRX/rDHIuCQhzvgAZl92KBP3wD/VKH+lKhBLepQjZpUpC71qE1VqlOZ+lSpRpWqUIXqQFjHujmwwZp5sIM1v+rVWKLQCJCwh1u4ia8NFGBENVhFidyXAe+gs4tpKl472+QmE5CxBvI5Ixrx6RSi8OKO9JrFOdJClnX8C0gHIp9Y0LGLXTyFKTagD0AZyhr0oeMctwjUOlLwN7wMJp+LvN+JEvNIUjUuAZdBQCV3UZrIIHCTAhDAAlgwAFzRlLe99e1vgbuafZxlGTcoghfi4IU1nIG5zXXuc6EbXelOl7rVte51sZtd7TI3DFsIwxfAG17xjje8YCgDGMAwBhxkgh64WYcGEnAYGXCninMtD9XsCsab/+TEPXSSQZzOaE+wKQVe+rQeVBIVFu4BqY9bWQc6buEnyjZFKbL4V/YotGCBbSUtYlGBaBk2ARfQggOLbKQjV4ux1jqOFx4IoAc0GeOUCsAAuRVFcHGcYx3v2JTW4YYyqiGNaQyZyEU28pGRnGQlL5nJTXbyk6EcZSNXoxraYMeVscwOd2yZy13Gsju0zI53bNM251gABEaUyG5tK0NcRFN+1VOCNr3ABTSgRQ1kIKcAA3Y/NoBe9J7CC27mg6FppU1aMquRdeRiQHNZBx4pq5RV+GWhqcnKNsVHIVlcUX4SmwUNTEwxxQTmYvtbcQImwwHPxEIWA/hMSkPTGQJ0IP9lPLb1rXGda5o9lNe6JtJZMCwbcVARo7RIQT7tS1d1rpOdyBtx03zyV3ahUT9LWcUqAESUq3ATH4xlsLcJ1mFxzwUdhaUKZg3tlltYQNCOqlDfRuspCkRAJSUStf5UxCIEVIAqFqhcq3iEyRjPqjMVKIAH0u1rhS+c4Q2nTVsS7vDWyK03uCAAp6r4adFmKHhoWra42FmTOtOCBi6Ithm/ds/89Dmf+cx2YtsWcdkE1NKO7vBcxE3QupFFH2JJgQUatY6I/g3EhAEPLRQ5ItQmjlQRKEBrNyCVCmgycrXqDKxB0xkBHKACGwC3xMEedrGPneyle8XFJUZGXsTvJMH/OxOc9euCEc/C5FwD8LRVXm3+PMXR+gBLg4WkYbOwjfAL4gVi9+GKVFzFjrqAOYV27tB/rMMGElgFRtIhg4U9bVuensHh7o0iFT9dBrxYhQELYBkCDOBxmZxtbTlJgANoADVlt/3tcZ/7sFsn2EFqBdoHAwLTg5jjZ3LJ8JjNTjvPoO7RjlPz8t5npshCfPhYh98J6jaZw8axigI2wSZ3snFM/hYymPQMWkDSQuUD+wY6BwxcEgu3pEICKLEU0UsUATshZmL6y1hrK4A+Pib1ZCRWNIAXWEDWYC/2Iqf2dO8BITACJXACV0MULo5hPkCADgkl0KlqPg7k1GPuUuA9/3hCTu5OwAZsT5hiLW5uX/il+4SE5tgC/AhsBhxv8mSBnoAilChEX8pCF76lBQirO1rCUnrHUzyExEAF30RqvmTB32TE9WyE1Sxn4G5E1g6gAMqBArmwC73Qc3rvC2NQFHCHMGCAsEBsrqbmJYgHBGti+UZQJ+BDz+op+lTwKSiuw7pt/VgwBhNtUNwCHcxGJRhkH3YnwFpA/jQC8LxiHVTgJaTmnNQw3ggHf4wC9BwJMiyAPlJhxWQkCguAVXiBRzYp6xjwAMxBDFVxFVmxFX0LO8rwMGqAKOCt+NzsA++qJqRC7twEPu4u5e5JBc8mwTKC0BZLbuCoe+RIbm4hXv9WIdPcYhXM6AyhEfIKJUHuhjskJZ3azv6M8FJK5DD2z8QQAwBbgBZkgQM+UUZUz0Y0IwGt0BS17gAGwEdc8R7xMR/1UXTyITtGBFOMwijS0BbBBflAUASyKAZcgL90AgZegA7rCbCWQiWUghcISi0c7esUZKDK4tFoQKEw4hxaYF1QReeukfDWwQW2sTzabmEYJmIiIJGWjlSEYhdU4DL4Zx1dKzIE4HEUkOBiT/YGwKX2sSiN8iiRkkn6sQxjksTsREQUxu3s4ovS4ynkLq/MxflQDu8GLD/cSilAkiPSwuaIJBnD4j7aDSP6wRY8SwZmwCzdIvIwotfWAQTQqRv/XfJSkJBEKoAWZIAxDiNjUmAoVMHgnm5jdFKTPiOANqAKrzD2tu4BYCEpKbMyLfMyb0Mf1spwnnLN1LDNwCVcaAIEuiYGQqBN5gxOfgIiVW7l/IwpzEcsFmsPG0suFWUd8iQs0yEtbuEGPcIse20f9OYuvRH//pFEIiCfGCMwjs30LsB/IskTZ8v1PGMXRiEAYG8edQszubM7vfM7F7EC4qsw7sMwlCINu5GLRAADMoB4QCBwaIEEQAB5kmcO/woFB8xsnMJ8Mu1A8KHbNtI2veIW4OUkH68jzLJA/CEtZMHtWvIbI6ZwVIAWKiAmh+IJWyvfGkcnUSqTBGDxMolk/7QONOgRFcDzRFE0RfMRHxbgcCzALw1jQkGA+KTyA2MCBFyAF2hgTejTPVbz7rhy5fTTBvhlSxqlQBBtDxNk+waFGMNCFvCjBjQSLJLxGndhJR80LzsvHBvjRV1uFThgcUit1DZ0HQlgkmZLADpgFH/SFLOuAAaAFZhURem0Tu0U11g0MCQgPEhiMO4DKhWGrsLlPWugacilTbTGJ7hG2vBTP2dhfPRlZdKCURAESW+DIw3EFbzSSRVrSTPibsRCFkJgJTnOJSM0MVJA7fhCxTxKQyGDQ6mORHdBFNoUMjmpRO80V3V1VxuORTlzFkZk3rKoFtPzvtbzRkmAFmwgA/9sQs7a4z3OhVFRsAbMZhXGhxE5TC0Ijf0ojjawVSvkhpxoYAZ4cDUYcUvQoTuIM0vljXAEwwJUYCryaXEqtFX3JyfN9PVspCd3oU1HNDQeABXmlFcJtmANFoLyFDGk54p6508/UyoN8gO6xgX0q0cdUivv03nwQz8eFVzhUizzhdAqtaFewwVVAzdngKQmLlHa4hzmogYQhovY9Tvw5wJkwAakggY44Fp4QR2ZTnFeVSezMFYJwAN2oQL8Nesi00QPtmmd9mlFCR/EUzA0JVgLQ0YtBTSNr2o+oD2VFQTSQ872yz22ZjX3TOU21lovklMBESOPkUkFdFDQgUYuTbj//nAf0hUEYOADPO4uJuX+DOMCYEAW6EQVOMRFBWdxLEZMpfMTY4U6beQAdoEFsDPWbLVEBxZqNXdzORdJWJQxLoAXUsAwQqwpNo48pPJYM2DEYuADagKM6BNa7Y5R+UwWOudj48Zf/tNkVQNTJQ+tZO5QCEYWtpFvHbTTLiAFVEIqbEAF6vVEJAACZGEVxjRF/q9/OFRfYy0WVKFygzIoEeABTqFzybd8zbdXuoQxOGDtCOc7Riy08FKQphIDRqBr8wk99Asr6cwXgfRr0MitWBYtcLd3tXUuXPCBZBA4ONIQ+VYEWoJUvSN5YyBtaiAFFAk5PwUCUEX0WCRoExMz/zTpAAJAFHbBAGTtX23rACBgFM63hV34hYNEMx+jBWbBAki38yagUE/XJM7pJcwDA4SPBlw3f1GTbHtiDoHUntilr9JyI/wlc/FmUrf1gEGWbQEGSRn4Lk9Cb2cAZ/dJBkjCcOwNOScgAbAlTMvR1ETqg7WXJzcAHRqgVm3LACpgACYThvE4j/V4NvZhAx5jBmSBcDrvUphmBk5XTOgqi0YVf0OuXKDVbGPAf9HWWq8xbmcOeEV2invu5g6UYHRjLJe0BQTpA0AgBGBAJaLiXVQgBS7lHwX5VBNjF2bAWtL4/zw4X+NRcinXVkWDAThgAMJhj4V5mInZK/QhO5JzFf84Rd7CEQZoIUwC1e349im4NhfZxIhhQDUB7AQBKywXBIrjJl+6zdFwrsNI9pMfbx2YJynohE7wiPm6o1PibUtPNVRQbRUe414x5pYdF6U8Y18HqHJHtJM6QAaAuZgROqGFeR+mKALCybRwmAKcYoc7znVpoXXb0w0PFZuRWNq4ElWW9PoEamBubrHW4T+7zV8O2Jy7dW48NQ/TIh1M+sECRCliQAVcQK7ykplfsp7tWSgqNJ/3uXFRqgCoU6VSwRxsi2RWCkbsUaGhOqrNlwwTgBc0ijCYuUQKeeMm5fg+YALsrD0zmpGbDVHNRTUhMkiRArEIZoAVOPI6TF9UWmT///Okra+u7/qk7Xod1OGuUXqc31YWQAAE1pMg78JUQ6xwQs+MezafGYef+1nGNEkAOoYBlvqEezK3tlCqObuznXYfRqEANOOqSYSZJ2BCvQWRv4VSlKJqxtqa1yNrzKWjMxZsUrYQ3dpQBNRS5UbcxK3nNjlffNuc9+hta4BvlW1mR8veXFmMl47fZAA6+QeyY+X1PmNWdBk7FVAAFAAdsmP8PDu8xVtXX2EALGAXSixCTdspSEJrTSIEaAEGKEAExhrk8vdZZ/vkAEyJbdvCJjVJBA8QSXqPLHXAX/CJhZNSqCZ+709E/oZElA5/TqwxppeWWSR7h/b1Kmd7r/N7/0eYMirAAcd7xEncO1thAELGAiQmYpiZaWzAvlA3i/i2vsm6rGUbPh4yTrjZnsjVd4cEBhuEw8YCHfj2gSORUti1dyKUMFALcQgAVfZ5uhGzAP05k657X1PBFS4762KBFQhgA0S8xMV8zI8SFgogxSmGuQf5UjiA5AJ1Uu6jyKky5IrYPY5YK5/vo/9pHdAHnFkD0fy85vZBFfpWi3eanjF4whNgO1IMsqk8HktGR0y4ZN64MTlgSsk80080ilCU02VjHA5gMG14xUu7XSeUd5BcQ+JbkI4V7ja6XO48rSMyBmThXpDkWz/ZLPphHW6hRkt1p3s60TsqAc5bHaN8yv9nq403KQA6AB0sO/YAYIBWT2U0vdpzNQyLEttZIx1waxf6T72fRkTsTFsUnAK6JgOQ20Zngoh7lGwVVcfPCPp6HEn+O0gWdB3Wh66Ue56DXdQ6qtQ8BicZB1b9OYRxRAPQYQME2gAmdwAGIBSsPeLBU1B6bS7Z5vsw/uI1PuM5fuM9vuNB/uNFPuRJfuRNvuRRfuS/z+K13dIYYL4wccnbdQJwFgQOySnkt9Xb8GpePXm02aPDpgZsPZVqM9DJAtP2YRXW1Rvlp9/9fcLLsQBWQRWebp8JXsNhjcZiYQW0e4R1obpZWOLFnjsLxB3IwRvQ3hvYIe3Zvu3d/u3hPu7/5X7u6b7u7f7u8T7v6f7KtIEb5sGTa8MBWGAXGINiXnmQB7nm/yYDLlp+u+jN1um+sVJr3gRjp3EWclu3jZ4ttkksDNFBmb7pEX2ME+PfIaMFbgEyVD9fk70KZ0wAXGGXudsc4LEAdmvscd8y/QEYhMAI4AAOksD3f3/4ib/4jf/4kT/5lX/5mb/5nf/5oT/6i78IkqAIjMD3jUAIhsHTZ2MFWoDwv10wBPkbL8ApvlpGb5Frd57nx/bGzyVOcjyLhn7wfsTzFSQ4PX8GZPbXIbSeoXfpAAJChAgFUuyqUABCgYUMGRIo8DAigYkUKQZopSoAgQCidh0QcKBArn8k/0uaPIkypcqVLFu6fAkzpsyZNGvavIkzp86dPHv6/Ak0qNChRIuy7Pfv3RM3etjMeQo1qtSpVKtavYo1q9atXLt6xUonDp02kfoh9SmKxa4EESZEkDBBgty5cinYpSABxCwbFGbM+kABg+APhDGIOIyYhAjFJBg3JmECsgkTLky8gGECBowYnGHM4DUr3z9///aZJG3aaOl1qlXuWzdrgmDBgWtjuGtXAl68c+NKeDswuPAICjfw4sBQYUOIzCk2rzgRgCtbAQQsQMdCY4ID6Vp7/w4+vPjx5MubP48+vfqa/pi1yQPf6df59Ovbv48fq508bODniXMFP0C9otZAv//95ltcceFm1wR6zUJLDbLNdtsHhhmG2GIaPsZhZJNRZhlmmr0QAwyr0DKDaP7ss0tqo5HkIlGvtUZaSWeto0JgFNbG4G50wSWXW78BJxxbAhVQwSwsLNcQAds1BxF0FAnAUSwBBKCKORQdsECM630JZphijklmmWaeieZKqcGzhB/8PSXfHE69ISecdr4hX5531rmnnnXiaWefgc7Jp3x0xkkoonMcOuiifP7pp56MClqoo4pO+micT9khJxTyAJWOB7NYMJCQbf24211xXTALLzJMYOFssWKY4YaLcdiYh5NVdhkML5Cowi60yMDaP+e0IEuxMLa2j2iq1ejiPrz/1KAjbbfdxqBudPkGHILDsUUccQXIIgNDCTi0UETNHSClRR7sEsAG6HSgEQEHbOBlmvruy2+//v4LcMBBmWaaP8D84MccnOYxB8MONwzxwxJHTPHEFleM8cUaZ8zxxh53DPLHG0P81BtvACKfEt70VCM+KBzU7alv+abtXdrKANoFdsk62IW1MnbrrY/pymtml+FsgwvF6gKDDLLka1Q++0Bt1Gu00FCtbT3WDCSQpg4XHLgQnKvKKkwyhACUzkkpwEYd3EKALa4I0PZEolAtcN567813337/HZQ/4CzxBnz5HY544orjRwcdcNDJaQ/K8FTjP/mwcAupwR2o4I+6/9n82SyzgFAthYLVehjQuHYIWWW8vqAKLzMoTRIvm82grHfr6CPePrekYAMNs2m9NV4JIujtkEWGO3YBLcyy0LlNqi0R2xttEIsq6CxQ7wED1AJ4+OKPT3755ut7TzBp4HEVnYu/D3/89jVOx6Z5bEHMWT3tM8ouHARnKs8t6HN4mYUsLkCL0VEAMKazEOpstbpcSWYyv3IBhGCgNNOsgwaeyV1r1oE3oUxNFhOYgA34ci0eZYs3QeoakcDGFiNF4FxqkR66qMeudhGgbQJgADqwo5G2hUQc5yuiEY+IxCQqESammUch4gCVPjyFDnp4ihTlh8UsarEqeahfwuRwCf8B/QQXu2CBQGLWua4RUAKskoFubEALEMDKdBhwIOpUJzTWfegFLkigC1ywCxjdwgUzQEdpSOIP1IjwH/gIIeUSWZp9UMsuNUBhCleoLc/FTHnLS8DYFMKBWSCESQ+hXpQqUrcAXCcjdXsIA7qzxFjKcpa0rKW+CiaNIUBFPvR71BZ/Ccz3MewNfqADHvZgD6CQMRUCcQsaW1iX3KSAFyn4HBxzxEDTiYBWh8nAhiJYAshMEAa8oIELXoC71TStRSWBpCNxso9GemeECwxMJXemQtxwbS5vOdALhSOQGDbPArPgQNrQVsqJROmUqNwh3VqBDgZUR6EE6MA7bYnRjGr/dKMcrUk/5rEIN8AJEFMMpklPmjg65MFkc9CDG7YBKluo4oxEUmPXciMBFdBCZ7mpAYpKR6EPiMCbGcJjBD1EghrwwgW+IuTU9jGLFDztNEVp1jx9V0/A0IAWxMPkj7jlT04ORCABbR4EyjiAdE1voe1qJUfWsT26VUQUHa2rXe+KV46mhh/VKEL9nOKUxqF0sIT1CiDqN4eEuVQaQNlHLWIxnLAGqZ8+quQEcDMBGNDCBtkM6gcyINSifjOPQwuB6M5pmRDoYjUzmMFFe5IPZ1X1NaOblQT8Apge5cZHmvQn2MhKHE96ciG7UIUAplc9trYrAB5ARypi0YB6RaQV/3mtrnWvi13y8SMSaJDTFQsL3vC2bw71o5Me5BCJyvFkH7rYhQXOGEAh0Yw3E5jFDC7LowlYcBYkmONstimC0P7smxHsYw0qUwITpEAVBJuFCjxYlHzwbra8yFHp+sLVfO42k9DcJNjGOsNPSm8VxkVX2tSlw4lQqQHONUAuKsBDe3EnuzSusY1vbKZvGOFxbCDpG+rHMPEKGbx0Ki8g8iCHRdADVP4rQGT7Cc0gWWBYEeiRgy5ol1n9V8CJgaBjSICzGJAgwS7ghYr+IYtgzVNqREHNLnSm5dvMYKe6zSQLJ+tbGBIHAnwW8UJSIYtSqhXF1YOOKs0xNwXsQqJbYv+AbHEM6UhLetJC4QczipAwXpJsyJxGKRwQCzE4wAMo+ODAAma4OQN1a1sIrCYBb1NCCvi0BhWazQh8dkcCc4gGvDBBOBXMi9KQphYgaMHUPvhaeN43zhiQgGZ5ikkWdg44XwNouITLZ4a0IBY3JHQOGxoABuxibqrMhQakiwAWUHrd7G63u0+iXpLIowl+cJ/7Oo3vYK70xyt1Sh66MDmWTa0DBUhABZ7MT7pEQAazuEA0GVRCzSqQ2Q+EoNBEsAv+jqAxLiDWi/KhChVMtWryHApqcmS62vQR2hs2Xgvj0pZ/Lq/PfS44Q1iwigQk9MTKldKhY8E9AiiaAXJlFy7/3o30pCv9uvFGpDV0kO+on3SljWOYv9HgC/31hBUDAFeqJTvZGsQGLqnKll5oEQOgxqrio21MhDgUAr0Q7B/oUIEJzqGaeC7yRfsgHR0poF866/POCpdLzH6L6j6fSyHnetcC0CURhlZEleiIhQKCyIBYaKCVCxjH0j8P+tDTsukksUck3gDFkkl99fEbJh3uMIc+yCcKS/5JLgrudVUfaFsQkIUNyB5r3FzrsnO2gd+DCuABA00EL0BRBlY3i2NL8gOyGE2ya9Ks69OENOsAgbVU2EfS+Yi+vQFSzD+MaiPRfLgLGcDLNMAciCCg0NCxjjmAXi8BbMAcC6hbASza/06iJ4ADSIDn0w/y8Ad00GMlxXoNiDiup3pPMQSjRno3sQ8bUHAfFkCGV18zABcDxCDYYhcpgGVBZRhcliEgoBgj4FMRtBi3QDCqIAI1kHeP5hLnIAs5eA7rwAu7cAvoAEIlkRrQcgHDI4IUkAJxZDO8FWUBhH5ltX7LYRwakFbUQ38BoAG7YHnSFS+WRzfeMwraV4BjSIZlWCb9MA09wB8kJVgO6Ibw0wbOUIE4sSQLQFPDAVYRwCouMBdlF4J38QFbZQMkEBhxFmCHeBgggBhiFzS4YgIgtA8yQALkJBTQsg6ikRpPJYSIVBqzsECE8QIkMBgPpkG8sA+QVBoqMP88WqMXKbBh25JwL+RMMxdi2WZz6WIcG4BiOSR514MOrrAROzQRAdABsYAAbXMu5mCGy8iMzYgm9gAJaZAHJAUHb2iNixMHjnAPQKEluQc2CscBtMABdOGHwlcb+gUhaTcrh1FHiDECAZYBGxchuqYYIHALpSGDJKAChvQT6sVmr7ELstBasqAOmvgi6zADhNFNhwEYM+CDF3ABMCiEsqAjwpcXUfU52kIzhadqiEdzNcckDMALujhovbgRooAOrCSMbRMALGAOdXMAFQBhzkiTNWmT3+EP7qAECpNY1+iThwMHf8AOQLEODJAA3ghA/TQQSWgB5FgzunWOHzBns2D/AkAVYLSSAd5UZjBgGI0xWvfIXo3xAcbWjyaxDnBVAyqgiIrxAcW2C5C4GjWAggEGGKRjISBwDi5yDpeFLSLYiuP3crEoVgMhQx+pEMrhEBWwCx1AUSbZNtqTCkFkaKkALyAxAKkghjepmZvJmS2BS23wFCT1k6NZH2/QBsdQOZmJEvsgChmIfr4lA7RQAdA0fmV3hHYBAnBkfAv0X7QiVBgQA0ulIc+3IR8gkfugAiJQAiEwkzKhXqmJDrygCiCggsp5GCXgTSDgAqtwC7cgkIpYKz6zIypwbKXRAndxhAjEh3e2kWElcyAmUJ90NgSgmChwXLtoEQtgC+iwAQDQ/0pyRSWsYCX2IhKdaaAHiqAxYRrbBZqk6aD00QaTwA/QwhO5QJ9j82G+5Rcz00KpQkDoCaIUoAIQMgMZUDp2tE028Bepw6KKIQLH+RcjQDs/8Roz0AK0kJwksHGNOALvuE28GZ7raDoXEISkcQu6NQEI5EYchmd59mHq95FnAxGKyQKNWX/vYgtBp2IruRGukBETsQH4kKBjSqZjqj/yUAgPqqZe8QZwMA+bqBNTY5QXEFDfGBw1sAqn4jm1aY7DIwFSSQu0wJWEARh29AF7wWUuqhgYwGCloQsf8I6r4CVz+BJTk5A+pYIbt3EkUJ3v6KPu+I4naCHM1pZBCCOuiP9PgDcBu7CexzNtQzKYhRmFiKlWCHAAu+ABFDV5BwCZVLKl/7kRthCZ7HI3ZWqsx9qZ/ZANIrWmzZoVbSANlIp9okAAHFABdYp+q1ADAtE1vsGnfapCILBVVFmohYEBIHA1c6khIgACgfQPj6qCGYAOqpkSrwECH2ADu5CpOwo04ElUiIEhhApU9UQBEgkjJIRZbuFgsAhzvjWY8BmltNokCRALHtA2C0U3G6CFHjBRwvirwhgLKwASBUBEyGqyJ1uG+nOKwaBLzuqyU/EfjyBGPnEOAsABKYChYONMszBTDes5T/mhIEobFBACPmUDJnBZsdJHLsBND5QCILQOLfD/ARw3r/RqEvuADkIlOsRZjynYlgKmkBcSK1pGsGRZErsQfICXAva4pC4Ec870sIlXi4dJSulyALLAAvcJEQFwAKmQktwTY1yqYtbBnyBRAROGsomruKKXGvPgCJrysi7LBnGwB0PJMpaDgTPwXug3ELTQAhDAOTb1rX+4ihTgAnA0CyFAG58BWk1bKx+AmVibqWMJlzmBtRcgAhGiiD4Knj7qTaElqnSUcrVxAXhXEuiQthFwCzzoAhxqfv40i2HDPIaZEFIKERSbt1PyLrhKN8BaNzFGjBH1EGFIVYtrvucraRSak3/wFKkXuc3KBnTgBsSQmjlRI6wgADJQLpxb/wHDwq1QZlNcA5UpRBvouFklgAE1sAujyo6oMwJtWbUhcBgjcAEtYKo2USPr4AIkIKhc1ru3xo4gXEfCay0lLAG2YBJ7iV9w4TSusi3Udn4fVqeexGfstxwotgC4qmIV4AroMAoHIF1bOiWTtwIecQAJgHfSir5LzMTY5Q/VoAQqZXUMA2rve42HRQdyQAj2sCI+kQ4MsAAF5Zq5178poHtCgip1EbQ9QsB2YQKoG5Clk3yvqwK80H27mwEgkE4vwh7/sA4YhKPqmiEXYoIkXJGw5lqpsQ4WsMK8cQsyQG2G56S0uH6MZ71RopgroEr9EwsbIFeC+71cGgCABhErUP8wTYzKqVxj/iAMOaAH/NEHRWbFP9mGbYANh7Re/5C3qWA2MiQcU2bGYAeLAjzAO9OXgeECSrULMZAB17J2vmmimwFaGWKcXvJOlUMa6JABOhUCCvm6DcwzO2KEWoMtq6rIHDBAQTILkPy2h4d+MkTDNnTDBRASEZHDK6AW3PvJQgysUyIAsZAKafUKqkzQBV1d/nAPWNAGVdQHfuAmQTbL1/hjaJAJSkwT5jAAGlsuMSQcF+A/XwdlaeStawyu13IbHAAagXpgWVYYDkRUp4OIAWa2FjgDGEYC3iy2o9ozJPx9twlrFvA0r+FwqnoBbbHOl+VhYiU2e0Zz1Wu9EWH/HbuADruQCh/RVh6L1TxkAGVEABpggwYN1mFdS/5gD4vQBnTgB0ngB3YAexEt0XTwBoIQD0CRDx0wAC3gXjkbHKxCKpIFwLQJtFBpG9iShCmAAX6RQDCQZbxpGCCcfBgAwiBwwTWRtbI2C4gonswWzoOtYZgVAU+Dtob3GYycZsDszk96bfIpsfEH1QWgFilJdNYTuKE8uFnYAAKgCmKt27stS/rgD94wCHDwOH2wH24t0STVBs+gSD1RCw+xCqswNhwdAaHEAc1UKkrpNXtK0iFakX5RhAukAnB0Nd7H2HWEord2a31RnjSxDxSJrzZg3sIbZ2R7SZe0NXCRAkP9/xsWLAsRsM7+rQLnF73wydR+9tTHuAAysAvFhQ7nlmJD/LEsSZkHwADGy9sXjuHn4w/KEMUqBbnGzXo99mPZyMU/kQ8NgACKudGJx9fCAavYfTx2Vo66dRtil6oUAAI+lUAzwFPPbBjNfGsfcAHzOhOZeK+Hakk8zWyHfOOCfWd4EQEWwIMTMEgdOeCESeDCFT3zSQADUAGqsOCpsAAaG9uy7bGtNCUBANACEApWm+FvDudkkkjE4AZ4cAdv8F0g7oBF1gbXYNEzMQoPYRAG5XUEVd1P1i3ylcaBTeMSsBeHjC0fEALivQpqObBrRwHVV+QkgQ6fiCI8bchDa9+C/f85JbTCE5AALbAK9cUCwnHl0vuRWn42ApAAHrAKldcC/UeMi2Y99TfEPNRDq6ABBABLcW7sxw4wSGFpcgDX96bnq9cfUBGhyfQTRbkQqjALM0TDLe7quncqPtsbo6tbKCIBKZdbCxQDELJZMMBAhDqqma6a7o2uXAnqKdfGpN4gMOfqUI7qA7ELkBzD3zJWfLZn8iw9CDAA9KngvBALKVAA3+sBsfBtvj7bQrwRHmBcpozsG8/xZ4LN9PAJQxAIzv7sUtdFjuIH1vDnMjEKCxHGqpCBEGDod3jd7tk5G3lZjI4bGYCjE1LCN04C6R6oNqACzfyJH1BfzimENY0B4Vf/7z3d2cVTQksJ5rswCizQ13BRKkrCubBe4LKOyR4gCz2YChZQEewiACxgjGhu8RD+n2m/AQegjB1P93U/JmR9BWhQxSWPb3RyB3pATHOABp0wszuBFOsAf0iyCzNAAAJRAdRE8HgIw8gzzDqfkSsnGyFq0t+XAoi9WQl5F7NwEzMgGDCwC838d1Bf3/jeFhbQArogfaVxDs2ralQO8E84vVFaAFVo67sgnSww8RIxAKIAjBT/sWcujArgAQPAAqds988P/eARb/4gD49w1otCUhBN8nw/WHfQ7E+hBJa7P//Q8iGB0g4PAUmCs13vntmt3Zg0ZxZA44P9fbg5A+J9/zUhwE4wkZqcJWtcBRAYBGKgQFBgQYQGEVJg2LChBAkpeK1bt0sWOor7/m2smIoFhwoWZqUoEMHkSZMQUkKAkKBAAggFBhSowGLVLpwsGBAQgIDAAQIFCPzkmcpVgKFJeQ4VkLQpU6YCNlQ4YG6jv41ZtW7l2tXrV7BhxY4lW9bsWbRp1a5l29btW7hx5c6lW9fuXbx59Ybt1y4Kmzd+3uQhPOfNG0BzFC9m3NjxY8iRJU+mXNnyZcyK6ShOfIcOkU/3sP7rB3cdgwIQHrTYZaHAAl4tSqpMgDKChJMTbt+GKGGC7969KUgYzlBCC1oOlTNU2Lwhhg8uasyixWvfuv99Gr+OHr1v1wQK0wseVLjc+UKHuFWs+7fvlgWTEy7IqJVd+7586c7xusWiQoHUTKqNJQIBNNACGWLBSZUWLBhgpqAiJEooAgZwpRWkoHrqqaWg6vC/FfYScUQSSzTxRBRTVHFFFlt08UW24CGkiEAUIywPwzLTcUcee/SxRzo2WywPOuBw4huuRlNLo1YAhIAAVWZxbZZRHohAJdtMwk033IC77TfgwBzut+IkUMUG45ZTzjmC0KMgBRl2oQgffbJrbzuOGJqFhvES8rNNhsCbYNAIBn1oghSwW0eWQYMziQMWWFCFvY2uy26dW0SBrzaXaNpgqg1SUAUnizrYAIX/DoI6QChWJXQqFlUy5HBWpzwU4ABcxYFxV1579fVXYIMVdlhie+0nGyXc2OyNOezIg9kfo5V2WmorW5ZZHAMp4hMl4cKvgdQAXGWWCmSZIUDassxNyy+/7A1MiMqUQJYaHlLTvD/9nEAG7PbRBx+K8rEvKzuzWkcFCmaBwU98Ax3Ugg0kVSWFQuONIAVZJtYyONxOKoCFfv/BrtLrVBHKAlV0MWcddGzBCFMODligFnS8SyAoVoFqlUIBzEkFKVo79NBDoUTRrlikk1Z6aaabdvppqJPMxgll52BjjsKq1Xprrnek41lAoH0WjiTkqVSro9FiEoEDYoJgFVlWUYWl/yvVXXe3Lrn8DV6IyJTggllmIA68e+9t7qBAVai5vezyATifdfIROO19VLmAFheIcwjQ9C6owTr79uGFg3iJQ6lRjiu+DQIOREHlOoKvS0WDFl7GB5/J7WMhAJDX0WWDnYeiUKmgYvEgww43HLqppw5AIJ2ropZ+euqrt/567J3uxxolFsO2a/DDF5+xzeg47I2rFUOjl9Lc6jafDlKLaYFZeIkFJrqxtFs3vN11t+/gXIAXMiBOAdNTOPMYBAO+SZSdjta466zjdv2o0y5UwAsQGG4hg5qURu5TuQg8JEwj7B9EUiKAFURQHemAHDpusQuMFIxk67CFBn7ikqQUYP9VwhuKzpKygF1sgHm1EtrQekiAUWikW9ljYhOd+EQoRlGKWvHHNAThBsMkZnxb5GK1rgUtOyjGCezgStrUIo4FyK8ACFDQf1TyxrrtbzdcqhgAgQMREPAiBQAsU5qKg0DmKGRfvPhK6PqRj3+tgxe8oIUNajADFagABBfIQHB0M4HFoW0fqLjNcOxowpPQLT4X4EAKWCAniuzihbBigQaS6MB9oGMUG1iAT4TiE1sGz1VM0UAsFpA8Iw6xQzpkAD6meExkJlOZy2QmivxRDSXAIUjQ6mI1rckjamJtDkMIBj9IM5cVhOtmHOCFLGICkwGdpDbqoiMd78i3FOziAoP/K10fARmo5zwEBJTKSnfuhLaKtKAGsqAOI2kxi1nIIm4zYGi/7FSafbBgAhWYgAVCcgGMktKUMmgB3GRxC1nsojq8sMgq0NECDlgAhxywRb8qUgtRHKApAEKAgQhQ05pCaJdO8cAuDCDMpSiPeEdsRTONelSkJlWpR+2HP6xBtUAI5ppTpaqO8gAHI2SjLulgQEsAVIFd8GIV/4FJBNJpt9MVql0mDI4LWmNHT/bRgPciXD6Hw4FbyBBtW8nOOThQKAtY4AIpgNMMaDCuF1JEgriLHD7QwUiRipSR1JkFTiq7i1nEYhUtaEEKODCVCixABrwA0JUSANZdiGIDDFjA/y8FYCDYrnGNEaJQbYkXgFTsQgBBW17yVtUBfSxVuMMlbnGNi7TtGSENWqxqc507h69BV7rqgwQ96LKPWhAAJg+AAKzGZQECEAiO62Qnu/L2vwjQ4BYhHCE9DfjePwqKAnXV3JhmMLIk/fMf6dAHOvZYQEeZdQK32Ehp+mGfdXTWBS6QgSlT4IJJZuACnowjHAvgAV2cgyIsGK9rAISz2Ib4pqxq1VCHEgBVxAJ5yhOqUwyQgKocV8YzpnGNbey+rThVEGvA0XN9XE1sEQm6bzAfjopAjfYRjC1N3YcHnPQAhRKAoMCjWzo5JccS1jECNpAFb+AFr/nK1Z6aA0994/97gVvws59o00ed1jGDQU0ABIQlLAtScY6CKWkfM4BAb8yKVrQWIK/tWYfJCJSATrGkUyHOpS53mpQAtOJnGrJVrZoClCTeWNOb5nSnb+wPeEShB5yJQ4+J3OMfp1prQmKMHd5QCHsUGC7+2Mc5DgCTAbCAXAVADge0m784App/XfJyBBLa5ztyTDjwZXaY5esQ3bAgzUfjzkbw4cHrjEPDihqYdvzRnfU66pJbsg3/bJMADqRiFLm435UUHa6YhLhVOAze8EwsAFeIAmhEBCZPcFUAB6jZ0wMneMENDsXSaOMKaUCfHnDErCCpWuLhYwMgeGCM0ZRmiUsaxQDSCFb/DswkTjJwUkrqdmUsD9sks6gBsuv4Lpi/F8Bihq9xcMMBVeDZg90RWPTOsg8UmPCSu5kjoAUUkwF0VH7iDZe8YUtbCdl7qAJQwC46sGJ+7zYpGkgjLg7+dbCHXexNAwcjcqAH7xkmmxNnu7SYZQdAKMEdc8FKPoC3qlmoIigIiCefl143LOmvvKoz9gx40yU78o1jcZU54+VF4Qm0wBYObE/Pz4KV7MDHz0P3DaDJi78E1MDQ4l200+udwyNOXQAMQMcG9p31qGgg5CzQSJLHfnvc5173IioNOCjhBjnk6DB+aHvx3Z6YZ0XCm2Z0yzkWsKpUzALRQuHALMb6EkRX/9jo8cnNLFTArt8cPuaKh/l86Wl+38jVzxyQhc4zcifmg+U6FnAUsbe/zpaYdhayYUnTna4zH7ItoDAxpQiADtiFBdA6IpqVAIiYAegAgds9CZxACqxAs+gHe6AEImCW8zE+D/SRsMmDNaCDHqgGWpu1SmGFoSCnDQCxcpGSmkI0OEoXo8MNkaAYoTs8vUk2ZetBjgGzuTKdQmGBVpiI0NGrsKi1CiA3s2oJwcsSlMu/ltg/JzE92mqVAxgAHyJApWgK3okFoQmAABiiDdGAVKAKc4g/C1xDNmzD3bMHZygCZgGErPlAO7yMZWmWOSCEetg4tfG5JgOQjDEQCBiABP+YgV2QgRgUJTjavgjgAFr4K6IjNh+sxE96Jx4snQISkLiBGIhpAV1QwzJqEkRDNEgRBVTcgOw7N8AzLZaoAF5ggQHwqtgqvQ8jMaiTOqDyQlVghTEkADE0gA0wAFkBxgpQBSEaBTdcRmZsRrDTiHuQBjhoAzq4A1S7Q2yUDCI5jGZpg0/wJhTkCBsaFVskAImYgf9YOhp8QtuAgBSgBc0jN2Jzp/DjQTD5Mpgjvx4slFmQAe4ziVeKHj+sNRTYAKPQsNChIZVCiezLP1ekCV7wABAzEHp7uluEugLcLQPYSDEUQwEIAF34GTFUgFEoh/y4OgEgxgVwBVlMIWd8SZj/jEkag0Zr4AM5iLhszEnIIAw/ID470IMesAY/VIvuyAUE0IBdkMXYIgANGBcOKLkICJCzYkd35IUKIDp2yTIv6bz22pswEb+v9MrOuxwc9LMEQIFMkr+B0a9KuQXy+rM3OjSWsIDWkDftKsecKTGeCAAFWAFUMIfJgYVQCIUVSElz2AAAaIBTuLZKwZCPZABYGQANOId/GEqZvEzMzMwp8odtSAQ4gAOdDM3GeDjBYAMcKYR5sL226I5REABZiAWnSwBVoIVUSDR3G684WqcCGK0FKDfzUqt55A3V6TwweTk/I061Cr9HjETxO4ljtDyv8CAq2op1qIDAk0F3u5KX/yiAFNi1WrzFD3MeBRSKjySAvoSFdLiPs2mPU2AAc8CFcTCmrcgFAAgABtiFWACKXGgPy9RM//xPAG2a9tkHf6gHSCA+0UzQxSgSq5kDOKAEcEyLjTvBfegAD+AF4Kmp2BqA6psF4EEX8To5lICAVLDK8nKnEqJH/9lKryS/w7sYWriA4QQl1hm0HCsjr8gHFLjOASG9mJCBW5Atm/qwBBgASBMAFkCFA/BIBgiFXEDCJcoOcciH6RyNUWiAUMAJDRiATMMK1QxQMA1TMeWVjGOcAmUGNFg7BbVDaDGfsDEMI6CGuPg2g4FMWZjIp6uAGaAFf1THsrIyk+A16XNE4P/sGErsn3ZCVGUrOhUgF+7jElCKABZIzxtdzxwFHnfDzqXbzhm4hZkQsQIQAAzTgAaAhXMQmFwIBVYQB/mkIqyotqyQnBwbDcdiGRZAIe3QuDHdVV7tVRipIieAg0Bggz7oA2hBO8hQ0zX1MTZATV2VTrcQhxXYhRTA0yHlAMx6SnWsMixxCdYg1CyRR+DUynGlRMQzqxmYhbuho1CqCTkRRTr1tn9AhwVIJ0WTwf4TiliQhU9dSt4RmA6AHrfYB8bsp3iNoBaQCir1VYZtWIc1EX9gh0JAA6sBDCITEmVdVrYbAk/gh24RxbOoBVWQBdhaFdjyiQgoUVWoAENEJ2D/67/oE1FwPdHk5L5DZcKOSQB6AdeW8ABXCJl1yAXG9Kes0IXwEq+WcIkC8YlENETZGoCm2IAn3YdROIXVrJSC5QqA2SQCWABKfViwDVuxnTV/mAdKaIM4cDgOVDtu1Fjjo4MhkAY6nQuqvTUNdbqg0ABZ4FOawp+4ZIkoaUQBmdmUm0TDJbwEoAU+m9k1WoAVUIVUaAAEgECfI5hUeIlD2063ibcKMJ4Pu6kOGAWd+4dyCAVLXQvYKaPb2QdYcJ4nHVvYjV3ZFQsC9Yd7eAbgu8m2fQNjzZE6dNtUowMj8Ib+RF1R+ImldDpduwVtdZL8gYB0ldluJVyjMze1wkoB/+FTwuWURbsZVTAjjUgHNyoQNapCAsBW1+CJDtCFkVFPkDULWd0KyBEYXViAAXid2c1f/Z3d0ggupyoEanQMxPge4GU7RHiHMmXLP2yPFehau1zKBUDEVfAw542J6LvXVhxc6t1gCUgAC2DO7W3ClUiA+uArWSSQqKzCpWQBXrBfAgiFtfTf00Vd6NwIyekrBhAAZdxfHu7h2JUHTxiCtHU4wrgaLbrGAnYuQDBNM6CEWFMyuMCHVFk0a71IC7iJGQAv5y2A6JXCP/tiMA62DQY0d5QStKJKk6swABEFddAKdahgWvS/NUq6XZApD1jYypxbuYCd+4AcjUCHBhgAo//xYUIuZIaFxmOwySDBgztAu7ZN4lTrAzuIAzgYho+Ni3S4O6GwRVBlv9iogPByiQtOWsBLl8ALpTE2ut0cVIbMkhkUYVSOygAQBa1Ih5rKV5eoqahIClVQBQHw2r2wlAKj1OvY0hXAY0NOZmX+TyXyh2xYBCKAAz3AgziYg96F5FRrUyMQSlor3p/jKh2atxCj4gJgv11AxwcZ2XsN0XQ5K3dO45md3qiMktnQPpNrZ7hE4XfzOoI5Bw14rc89AA2ws1ZQhVBoAAKIhZ8Jh2B2qAlqnA4QgIBdZoquaP/0B3sIBiGQJmR9ZGx+LoiLA0GAhwR+C40Yh1+CsYv8P1b/6dAzTIVbWGek1WcRNuVWbETy0h/BW8UCiJuSuOd8PrmZVjTxGoBQrMxKWYdRkKkC2IBRSM8j3AdWiMgd1osIcpwD04h1iGgPaFWL/mqwZkZorAZC8ALF4EYk/uipYtA80IM2WAR78Oaf2wiUDmftAtW7BZCZsAmcMCdDHOqhdsXs7NGgnso0XkdZ4D/TasJ21mekhYlN9YAI3Id0GAcW0op8EIdwuDbvGIWFlWu2gJz344iI7gB0uBPQDmvVXm2DU6J40IQ9gANoSZ85QNYcUestirjNIJIhiIS45quB/Ye6XhUI2WQN5eSWhqFVeMqggOyZvk1OcULQE+pWtFe4/8zUChiJsorL2wTszHWb1CjSDXiFcjAHy/YKfQiFnQgAGN4Hc6BMpL6LS8EvQittZGZt/M7v29MOeqCGJ3ADZqk4G4m4w+gD3B6fPNQMI/iE34bitxiHnUgjA2m0Kg4KJOWFUZEFFrDf7dzWx07aUiTqV8ZNm44JunzKv/Vu6XZCeAuxTzXS7zWjo7Bw2pvfvLgUdLC80lgHDQiAFYhA/Q5yIT84fwBiOYwDOwijN9Wm2z7wrrkWrNEDrMI4u6g11NghXLTIFeYFBKiA3Mo7BwGQTvHboSZzmk7x5+4/cnINFZfL8I430xvSoAjdXCiHcyiHcZBIHfoJFkjLupjvG/9OMn+W6PseckM/9E1TomgUhDawxopDH6wJElZz8q1ZFuQjEjj4g2mI7y9ti3NowYoEsSqmvl3QAJ9IAF3bhVVIAfu9WzNvc1ifaQI4pQowc9ArkHOKc7wdngDwidfioQKY5cm5rvnODn3IhwFFIwKAwPdFdGd/dqXaOVDzhCKIA/RhA9OsbZyk9EpXO6zJg0Z+AyUQyqzo9LSAqHRIFZMtblG3SKTUYploakTMOw7AJQSIt82Fdb/FH/nxWwCRAZLtd/AGbznWdS0HEKC4mdObZfqOC/yInIQMro3IBfsNhUKHdozP+KPK1W/ih2qIgh/Y7auhAz0IPm7v9hwxzTz/SNs2KIRvOMG6WIcGJucrBE8CADlV+dwKwBj7kQENILHvHGrMdW6i31xxMZl+F3ODN/iJ1C6c+dQr5AlRaPi2aBzcobzIqRRUGIpMkzWN/3qwR6p+mAdhIALGuJq0PvlosW3EoG3ocgKS5vhmJwv8OF4Yy8UrzKVbqNbYAgoj1dOQmoVU0OK8TrQxdxuXVSMn9CoCEPzwWnrIR/iVxtOoSwpWQMIlmd/79od/uZTWNABWCHvRH32k4odteIQ2IDKrYS61nxYhE76weQM9iAM5SASzUaLrGgW7vqWol62EVoV+BfpVeS0OaIHK6kcNuBmcyWulj/wCaFrnX8pOMVkS/8PyCQkKCPEJC98APKv6hw+ZtPEHOrkOD/jlcJh70k9/9bceDJSGQUj9nmz98DkfbCn5QpCHV6Xb9oCFnVD+0yNPgFgVSwCBAgYNFjR4oACBAQgspLo1a1aqFAsKFkxwcCPHjgYX7OKQ0GPHkRtHEtDIcCUBBBgZHiAgk8ABgrrW7funcyfPnjxz5suHD1++nD516is6TsOADeZy9jsqdSrVqlavYs2qdSvXrl6/gg0rdizZsmbPok2rdi3btm7fwi1rlGdUnf7gUUqS5s2cvn7/Ag4seDDhwoYPI06smM4cxn7fAIqTR84ief501jW7b9yGAgtfvnRJoEUsBCtLFv9IgJGAgAIcWMjatWsVCw2qHRpUSZLjhl0VdiP0CDo4S9Usj68uICDAqHX5tu6Lnm8dvuhV9+F0tUDAiqJxv4MPL348+fLmz6NPr349+/ZWL+uM7s9ftURr4szhq3g///7+/w/2Bl955EGHgXTkISAgc+ThRmVq1YUOCzTFNBNyHPgm02kmpXRSQwUskIIqsfAyiyoyWFDAACYBR0AKu6C20EkHdSjjZ8Elp6FoM80UAAGj7BNUVkmt45x3c/0D307R7SPhiqM8t1Nm7lFZpZVXYpmlllty2aWXaV02jzSCoEHHG2zMgWZjfykIoJtvwlmYfoSpSUcbjljGVj6jLHT/o4YzVYCOB60FZ1pBpsUUnJ8DFLABC6vINgttFiygoobCqbILixxxSNxLyCFn4WoEONBKOjpVRxWTQxG1z5RTXYZdLBsIsMArSH6Zq6678tqrr78CG2x6/cQTTBF82WFHX4EANmecz0IbbV8E5teXnZXB92pZuWjgGUMt/XnALhOCmpK5uh1kI0MrHlDBo7bIJksqMnBwG2gDqLLKjMf5iaOn5bKUaI8OzNVPqj9JN12r1r23ZD6pbNfBOTnhKqzFF2OcscYbc9xxW/7ws40kbdxxRx6A+OHsmtVK27LL/g1oIBxT5AnmP/uk44FnGskkmgCqqPInwMMl1KlLrWnA/0ILkMp2C20cVLAAAgLI0oKPNBZg2kEu+VsuRi7tyGOPAsBy881BUhwdq0ExaTZX53jQkijreFy33Xfjnbfee1/ZDz/TEFJEGmywkcdfBs4BB+IvM944YZBJFgcfcBTiznxnXeYPxaxUQNO3DAnAAjo9D73SjaAamjXoPVfAQSqrxMbLbJmqgkIFqglAkNircb27qL6zRtByC4giDpPRFZk82wxzlfnNrlQggAa1tM239ddjn73222NfzzBLnMGGyYbnp59jjqOfvoAM5qEHHXBA8c3lZV0O3z5we8ujBrtswOPnOZZuJak71GpyR4AKOKoFqpBd03YRi1XMywMcaP+ABhhwgAOopiW52+AGA7Ac3RXkggxggAY24IFUoAM7QcFH8nDStoppBT4Sqoko0oEkJXEvhzrcIQ976MPwWMcf8gDFH84whwUFRlnpW6LLGKOgwknGTIKoBg7NoqQ9HQABn2HNLkahu/+BESMCC2AIxVjAAGyAFxo4gAU2IIPXuUIWsZCN7Bj4wDiqIhVA06MeVTEKWbhiFa6Y4y7QYchdmKNIRTmec2AIln2wYjsMoN5RqvjDS2Iyk5rc5A/99g1PJOEHb0AQgdDUByai0mWQcVaC/jANfmjOkV+x3z504YEBXFA5qYjFiv73O6L9TzT+KlqoCOCBXVykIQPYYEr/FlBCE3qABdJMRR/7CLQ85pGaLBAFCzywgg1sYAEMsGFPsKOPsRhlH+YYFAJEoQ5ZcjKe8pwnPevJK0vq5B7dyEIS2oCHOyirDylLJUHfRD4BKWhAk3HDMGKFOSVFpxUMwCUBNoAOWv0JNBldjcA0WrqXhK40QgMeB0tqUg4GwIMp9eByDFAOn2AHnlSBqE7SIYoHDMABuWCetuzp058CNahCJQ/IpuEIN7ghEGx4Qx9UVtCnwmxObNDDZOAQDHtkC3NLwocoZHKAWKRiRcD71qjI6lFgEg1fqiAIS0a1uwoBL64zIQgAQlHF6fQEn6o6GysYUCso+USvQx0sYQtr/9jDSiUqf4sCD+YgGahCVjFIZNBg1JQDSsQDLXOhpS46EIBU7AKuG52JMEUlWrP2jGgyEUAsVOGjcP2urXKdrfBWEI6o2M87YclHLhrQGg+cKio9RSxxi2vc49LTH/aQRiHE8NjIQldOgGEMtVY5Bz3IQRHwcJtXBAvRfbRiBapY42zFhlq3+pImolLOLlLxWrOSNb7l9Z3wWNOAKEGUbjJVlTg6kLsO7HQq+0UugQts4ANrLCdhSkYU0BAH/eRBTX6AA2AAgabCRRe6Z2KfnQiBDX7wRLBjWccpLpK/+cb1vOByq7gGhWIU15c1YgtAKHCFtubFJxdxE8AGboXgH/8DOchC5th8/DEPYUChDQZ6rH6cesQMR1Y/djDcGwJhhCI8gx5JGvBXjLKOUXQOgy9OsVt9VwFujXnMIBTeckRxzp+sY7h53XJ8xLGCqWmgFc/h8pD77Oc/A5qoSZrPPIbxBCUzZspompNj4OAHKEMaDnD4QxEw69CdiNgrFPuyAihUE7Gmeb4COGZNQr27Ncs41TMxgAAYcAp8mE1I8/NJXZS0Dh0TIAALQAVOAu3rXwM72HBBUj8KHYU2SIZabHoDHEYJaShXeZRoWMQ3XsVnrVhnH/gYBQNCeABQmzqun90FCMONah7pjs0yCQAAGiCOfwgF2/lgRQcGMIAGvCL/VRQTNr/77e9/75V5SXqHMRLhBTewry/rezZkEeTEIzorDUaQBohtZhccroMVDSjji8st1wCwIhavlavHVX3q1aJ8rqsNgAFwIaSZxscco1jAMnuc7X+8GeA63znP/T0fe0yjEG1Aw5Mpy3Cokk/hfjEclZPgiXpkeiw4DFIuOhCTBIi2vCUv98/cK7Z0f13lqQZ7ylWO6gCsgG7XWccrPPDtBXjAeBWLes/rbve7G9cffksGI36QBsmUjMpOPnrjkr70wiEuDT4oBDb0Hpe06UIUF9EifdFtecsTxBUseO25xy52soNexmsGuznMcSqY/mMdMv92rU5xDkyHmO54/5897WtPz8wotxqQgIMc8KCH/PhhsoRHOmUBwdS9OEPL4Nl0K6xOofNy3exzFZdnpV/2sofe819fcwBUYY4DMCAUr9ZHP6Ij887UygOwOJLt2+/+9w9WPv+whzUoQYQhBAKhw4f2gtw3BEd4g5y1RXSMQytsnEvA1bnFWPDkWgOgQwP4yNalnAJOYAUuIGsEQAegg2etVAAowAqIggYET49NjNk4D/yhYAqqIN/sm1Q40j24QzA4gRfgx/6lTx+oSYEcVFMtSBzoAXb9wRD8QTIo31qcIEzlwzmIwgaoC03EmLoJTwaiw3aA3QVSoPaR3eXl2gKYg2t90UIs0wFswP8onEOUrOAZomEa5tCUKNg8TIMiuMEaHA6bKJ1TDZ4NBsbgMVVfuI8cmEl+/EAkwIPskQU+qRO9CcwWqRoIoV0siN711VckVuAiopwkBkAFxEIsJIqMDADcocJTzJkaiuIokiLf9AM1TMIfIJpjnI8eTtfK4GGcqIEfGANWwdxbCAUshAITJkQuodtypIIrGNAiSmLYVeEjWqFMJMBrdA5DLMAGhAIsHEwpUmM1WmPH4BYMDoMTuEEcLBmETdeAsInxLVwswkkctIEiWMOlCdhbmF8rhGBBMEqfxMQApMIoRKDYGeMjVqLJLWAuVQhBaIAovMJT7Nu1XWNCKuRCUgn/bhnZNOxeG/RFsihRfpgJdUUYxCHUHZpjYrSP+9CBEgRDnsATQm6FtU0HLIhCB4STvTGKBlRATFRI1pGUyb2VFl1QPRKAM4HgK6SDbsUHQwrlUBJlligYyHiDMwzCEKyBmsyh4VxYOZ5PR/oHYyCOnRSCNNhieQxXkJhDLrTCKHiAiTFKWZ6ETKaETHrLATyAjKgIjrTEAnSAKLACLphDlOAKDglgUfJlX/qlWxQZPVhDJEBBD+AfHfye0uXHKU1LHuCB4VHlfkTbtDRGETyC/JTTeDjP1KnNOqSDK7ACN3UAS1bQAlTKipUWiJhmCY0mCI6CK5wDCwGlVBDiX9rm/23iZiHKQzJQghuYgRz0hfFNJZoUSGJGZn80G4MoGiDAQRy4gSbEQ11s1pVQzHScwzmMgzjgQi5sZ3eGAy6IwziMwzn85M3l5nmiZ3qSRz/QAzwIwyLAQQ+MEna5z2OU43H2RymlibXQgRwIwjPMA025h5LUZlcUqHoiaIKiZwtiBqYplzUwgyDkgBsojmMIiB/8AUfip2DkgcmwzOHYiSBopYAqaIma6InqUBCBzDxoQzBEwbHMyX1uKGLkgbIw2yg5BmPqATouwjTYw16iaJAK6ZD+yhFKxT3UQzd8giCowX1444xKZlM1xvswRuEAgoUxiHMqAjVUXHjUD1rAR/+YGmiSEGmZmmlfZg799UIiuEEbtMGU1aiHls914Ucrrg9faGizFBRHMpqZuIEk+KiYnumgEmqh6oqRWYMzFEIoEV1jJIhTAghkGl9HngEaTIEyBOgsGeqmcmqnGmGaegMwaMES6EAbONsc9AEg7MVk6SCBGA4rShdV0kEcoEEiHEOmSsmSCJyn8mqv+mqXxUeY8oM7KMMnQEEaoAEaNCWWAoarMp3CyeiMuio6DsIwROevYmu2ausj5RU/zEM2CAMkGMIWEN35ONEbpMzCPSuU/gXhgKQbPEEwZEOXbmu92quv8lnmgMw2UAMzPAEREAGyUVbKSOmFQSaU8sVkvU//GxRBI1QDPYiYSd7rxFIsUd7QSbanNGRCIbxBG9zHdQkf4bArm/yhs9AqIQQDPGylrlZsy7pseh4o88yHPHhDMVCCIbQBGUjkH9IhlMqMY1zYfsZBHPyfMsgD7gXlyyrt0hLpPujrPdgDPByDJ0wBHLSBHMiB4QDCVG6o4rBPKSHUq8ZBDhBCL1gD1BUZ06rt2pbpfNCDN1TDJxSCEaBBGJyBHDbrhjrl4qBJYg7BD0gBKCzDj7Jt4RquX8pekdnDPGADM1BCIsABD+wsLFbYYuLphz6OhXqkYhzU+hgIHmDuX6CBEURBMGgDxB5u6qquehKaN/RrIqgiGawBso3S/4IQDuEISFPFqLWsyYEgjpNN5avy7nVVlpvM6hv0gCD8gjVYhohZm12sbvRKbxr6wz1A7TccwydAAiHEJxq4AWLSwfhQ1cHybqsa3Ssq3aTyxUWO0uVCCxs4EcNCQSRIQzzwA4gJ6j8A6fTyb//S3lEO2uJ6gzI8gyQIAhG4gRyIgRjcQX0u2/pgaeE4qw7m4V+or0X6xVW6CQ6mieHggRzEARlQTjBUgzzA0pJ0hcT67wqzcIG5CoPOBz/QwzxYwzBorxQYgWFKpB9aKJ42VVMVSGGQb2A43GNy7X6cEhvA6QTHwRqggSBkwfLawwmTaQtb8RX/b4gNGgyywzI8w/8mJAIfYK0ZiIEZoCPwpUzKNNtGOhlxQqq0sAEHB63xBW3HtgERwIEiMMMyNG8VY/EfA7KQqTDsgQzUygP2/oInOALkkgEZ/OaEpczWfq0FX5iFMetSIVS6vgmebuRALV2BlIwYtIEgQIIzbMMUZ85+DXIgs3IrH1aRKZc8wIM1TIMzTAIhUFoRtIEXLHAc2AF+rO/SJVzSrRJkAEj76illUoui9cXQym8wKIPKzporU3M1G5bE5iWZxrA92IM8ZMM0DEMwKHIh8IEbeIEXtAHC8fDngq/hPNYQJwaFWWX7jA/X/vAe8m7O/kEifMI0fMM8oLK++vGBWnNBG7TeMOj/WOAeLC8uO2gDN1CDM2yCJBSCEyiBEcABAruBzprx0NYg5eans86BJquM/l2XHuDBP5lBOvOBJAhDMnSDN4CD9cKyHx/0TeO0gcFy9drDPcwDOGhDNRwDMPSCJ0SCIiCCIbzBGoTBF3SBGlTqfbzBgyXGBc8JgRwIyzBGDZLj+gqIF5xBG/yBIDSCJDzCJjRDNGSDNrwDPbg1PfSDcOX0XNM1cRHooPHD4s4DPHiDN1iDMkjDMwRDJETCFFT0RV90RiNwEUD1GUB1F5xBZDepGlD2ZDepYylxU9oB4axBGuCgY6/BGlC2GKhBU38BF6B2DuDAagMBEkRBFWBCKZgC/zKQA6zV9W3jNoFlTj+ATF7n9TvAAzx8QzZQwzQYwzAYgzAEQzD0wicEwydMgiQwQnRPt3QTwnUTAiJct3ZnNyEUAiNAwnSL9yREwif0wnkLwzAg9zRMAzZ8AzzMAz3oAz3ww/7m9n3jd3JVsUBvJvSSqXD5zf1O8f166zvMw4EjeIIfuG8TeEDrb646aNradH5TeIX7lCXZ91FkuFZsuIV7+IfzHJDuKoiTeIkLVUKjnqb6N0GbeIu7uA719wkK6oxr836v+IvjeI7r+I7zeI/7+I8DeZAL+ZATeZEb+ZEjeZIr+ZIzeZM7+ZNDeZRL+ZRTeZVb+ZVjeZZr+ZZzef+Xe/mXg3mYi/mYk3mZm/mZo3maq/mas3mbu/mbw3mcy/mc03md2/md43me6/me83mf+/mfA3qgC/qgE3qhG/qhI3qiK/qiM3qjO/qjQ3qkS/qkU3qlW/qlY3qma/qmc3qne/qng3qoi/qok3qVxOxPoDoKq3rSsjp3ubojpS07dhcKpxN3objHIEmup3qr6/qqv3pW1HRYVFGv8/qu/7qvE/uxN4xYxEqyu3p5NPtVGAWLw4pVJPu1G7uzVwW1l/qv9NTx7IM+hHt0iDuTuBDyMFK6m7u6o/u6uzu4Hw/sdbiAiXu9kzu4K5IZ/gq8v3tM9fu5+3u7C3zAEzzAA3z/uY+4hOergzLJIhU8uz/8v0O8wU88kyA8sTE7zk3HvR8Pwpe7oHU8vq+7d+BQbcpU2li8xKv8wFO8uwN89XT79mwaWLLABmjAAuRkzuv8zvN8z/v8z+v8AlgACqSCLYxDr8lFPhCAAuQ80+e8ARwAzneAvnfJvq3DOdjCSoYT0HN913s90C9AA5wQLpyDC/k3WFzGK0D917N92389ArGAKtzEKi/JZYyCpwE9AqCCoOGCAGAd17cTmdI9TKGDLrQCC0CN2ys+1zsT0Rs90sc8QkeHLtSGADDKQUAAcGj+5nM+SUCAbnzIGKbQoInFOnRIR0BA5hsEBAwABwx+eWBH/yp0wFtuROqnfufjfu5zvkswygaogtmTxStcPubrfvEbf254SAWIQgk+kt0LgOoXwO0jf2qoSCuYBy5cvvSTBAGIQmCNaXzsgy2wwHYM/+cf//lnfvqfRO///utHvpc0yYS0hu3Tf/3b//3jf/7r//7Xv0HgkioAhL5/AwkWNHjQ4DoCCQpAcPgwgsOGEApwyIcQY0aNGzly3NdqAYECEx+WNHkSZUqVK1mWZDhyQAVX+zoi9HfQ1ciWO3n23DlS5Kh1NTHS/DdqAM8BrYg2bYor6c9QBI06Pbhv3QYCIhMk8PkVLMoCC0dVtXoWbVq1a9m2dfsWbly5c+nWtXu37v/NfecqcA37F3DLsSzymUWrkCRKhhQ3GMbbluY+FgQQJA58GbPXl6KM3ky7z5ZOzKN5eoXAkCEBDkMHeraK1KFpkw0LMGT1WOOrpLVZFmBBtfVZveb6drVMGnnJsR0uBsf9HHp06dOpV7d+HTtedBoW8oYQkWL48A1R8649sjzF8unPr6e9uP37+N4fDt7n2Gm+yrJPu/RaoLHsrNpHFAFEU2888c5Trz0G/2PPPfgilM898U4yLTVV8NvINYJyGmk+BxuMDzUGTyPRxAhNnO8l9k4cAIWh+kkLtq5spIjEpa6DCr0ezYOPM7f2Cqk/00jsUcIF42OQQvlSJOm9/hr/Aq8AAVa4T8AstdySyy69/BJMyEQZQLOuYjuyPB/RPA/J+dac0D02XRpLFrYUWuxMI18qgLkwEdpHlwIQwPPHN9ncE9Ej20xzzUXpC0+zIgs4AB22QAMxzUx9bHHTTg/1lFM4a2uxv9gGKOufDpuqcVQWa9PROt3aZJNNAn4zSFWi9vHgQIniDNVRYH+dVdT0lCNgpn829JPZZp19Ftpo/Qz0IeP0RNHQTzVVVFht0bTwPAuaS2ud/fLU878Aod2VgGql1Iy8CbPt1ltOue0xtgQfLeBKq1R1pV167R0Y1II/rXdW+mQbKR21YDvxwU9hrY5HQzc7KNeO9slFpFLz/6WV4G6LnffQE0/jjyINWJOW5ZZdfhnmmAcURbQEBn1UsUh1LpLnnX3uucifSy2TtwQGyGWtO/PsVSJ1nz2nsmqP8/jnqoG2GuueI51IZygLOAe4AWEB8eqysz7b7LRRJrsAVHHl6CakwFPJqweYsg4qk34eK0jXMvYIBa5RhvTkoMs8/GSdzbyR8cVXKvE7h5BVVubKLb8c88yzO2cBPEUc3F0bRR+d9NJNJ10siN2ldQW1sIo6T5ck4mDZL0cRTSfQZT+d9959R71roM8cxTmnbvow3515lj25C0vrjwAPat9IbonoLuA2vKPanecCRHELnQW4Js958v/TF33a0v9fv6S56/Nqbn6n15z++u2/H3+Cbmnw/V51b14wJLlRfT6mgX34428cUdr7pNa0+W1JMlHB0/8AeBlrqU9vMAmSWpB3vvNVy0zWwyABlTMbE55kVJGTUvckVQGwoeVhE5RdASZGnbwJkHDw+l4CiRIak1ltIiP0WL6Y564iGnFuoktMulaWPyc+EYpRjJYMFrS6wTGtgo87XwSquLsCVKBSPNRIuZYowCB24IFaykoQMYQyCmaxJ477oMkK4IEmngV55vMYrcYiKBAB5Y99/KNIBhlIQgoSXyv04HgGoIs0HiSGylMf9na0m+QpEgID+N5AHokQVQyAi+QT5RDheKH/JaJmglzThRRZ2UpXvpI6HPijcq5oHD7+cU98zOUucRnIQ1UGh6Mk4CrJtZASNpAiHBDI01L5MRDmspeYiqaadDnNarLNO+QbGogq0DAO5g6TviqABjbggHI2wJzpLOc61dlOdr7TnQ6oQEgwlbbTCABpMCSTySwkpRpO54ar65733DImteGoAA8YiUJpuFCYOLShCX2oRCPK0AHAZCsDOCS84mWaAeACliEV6UhJepYKdNGZzzzA71jaUtENKgERkI3QKPKAWnQyIbA7EzIBhFPsjKNdOyViRxOwUpce9agwbdDa/lOAcqwlj8I7GQE0dB+rXhWrWdXqVq+6jnO4/8IDwNwaJmn4ChpJUI8KoqT2xLMzUW6ScmjZVRDJehoWiOKuecXrXvXaV77+1a+BxSsLWrABnUTpRA0ZgFlL2ljHPlakkIPSHitgi3WsIx2Y1Sw+1qGOdeAjs54FbWc/G9rSkna0ojVtai+bWa92bnQYnMgDUMHJsyzQVzxFI7TEIQAGznAwXv0saldbXOIeV7XIPW1yRXtZWZqHiPwswDigCs5RkoeGN32MUayaCrIBkao+HQhS4GW91f1TOgF1Ztd2uJZ8GBZHV/tiYbhaX/ve16qqwN1MPfoK8UIWwAEWsLOqyDV+UmQBlcrOfcTXoq2pbwDZO4xOh0goZUJrHP8G+i2OdPjfusxVfRgS6gDEUd0Qs7A2BLibdPbx3LqahgCpOGtsJDkeCdvQkkAjnybDdpZ8aICui2zIAqyzD1UEtZ/hicAANDRgJz8ZytFaofWs1RUELOAcHo7LPhSwn0kyyWinGEg/fIpbBAm1p9CyxfY8R+MUp0LLdOFVWkmYyRJ/U4/CzC5BxJiWjH0EnG49TXj1Wd5+/ge90VEvJpsKV7WsQwME9NkXZ1QdViAZIvw8VZyj3GlPf/otRjTf+LCMHc9wGaXqKc8AxNxnjJBxTrrldHRagWTIQew/js7SnHOIzAFQF8+9jpRDBqBdV/sZIbVIiqAxtGl97nTYQVz/K8VyzGyKwNXDP5Y04YY867SwYrIghEkovA1qc58b3RgpYbglUmoB7UN8llGTAFidqsMYs87SvfCzPnlMX3kUzlzidZuj+2sT19U7sDq2W24SmhMrj9BnIa90Z5ho6Cx6kTq0t1ryYYEg9+yL5T7LpevDNQIYqAId8G+6Wd5yl1uFe8cpgLsXLD7lgEijFeCAKloBtoXntIw8dVqzjkwlz0lW1wvmtTYxNCUSH1yPD5JIK0Sukc4MpBYB6zXXnC1xCdaYNjcGaLVZ6BUe2/YwkRbo1irwc7m0AkoiASULUJELdNDE7S/X+96j7ExtitCF1nENgytzUQQwYAOhGMUu/zhbGDvhO7do3i2/A7awf0tkg1oK3MeFmklgp6WDZDWOxe3imVp8t9eSc9tr9kmoiq8YxxbKOI6S7mMgU3mgC6i6ro4iAALEZAMrQEU6HM934x9f7zre5kgCL3iC5EMBC2BBKmqhi3VYNS6wNiKa9+2sfhtnNrKJcZeWjszFGO3OoD+sjm1UANg/xjW20PpYjUQA4j37h6WSttjTS3b6X7vHrELbEkTIKqDSoOPUaqEBRKEWsmz3kA8CI5CVGG2bKCICvGnB8IG+KOcBzUxhgmjomOX7QqitjCTzdm39ogtPni7YhAyEio069iHrcG/YVO+sDG2JKIL0cAPjhI2g8v/OIH7s48aKyKLjJk7N8bhLApeQCaFM+ZqOIZovTA7QLbSv8whl8rxv2QjOc8ZP4PxH/zwq/dAiqpTnIeymOk7PupZnyVbPKSJpeSaJ/xTN/zDp7OLK9rZt1AxQQBCIz5oQEAOxpCiw6dQDA7PjCP0QI4AwISDv0IKo+4huC0mIBL3nAdei/ITq/FhQ/fKMlGBwOkBj/sIJAqjqBinuvN6v/2Rv6wiKLYTwkoiwKpQwrmjRFtHuFvEQOPCuQxhREH8RGF/mCZdGCrPEF4nCCg+MUELQT0ZQ3sRPxr5whGZqIjiRDNfPDBkCFBEQ6zpm9h7io/4rhtJq/yqprcoOANH/Lg8JkNsq4BI1JhjjUR5biRDPZCQuUIo8kBolIgslsXDMiwRj7B3TgtdCJ/KsEY/W8INahSmOEW6KR1mU7fxYSHKIyetw8PXMMZhArr04Tu2ELTx0TzqOUCOocB5PEiVdZhjpqBjfDS+SkT8sbJm08GSesT5qDzsykZ82cQwT8uGGpitqaCANQjJGkRxpjvVoLA5tTCNjkXDuMNtuj9t4pgiNECIDMCWzUiudpR4hpgDwMYr0UeiGUi5G0PL+USClkRVHrCetIvRc8CVWLDIKYkNmESuJkruMLKiiDlJsxcMmzvVSsSnXS0pc0b3ga+tOZAHoy6oY0zEbEzLv4zEl/zMyySyr6FIXt1IzN3NLVhI+WjJ/YNKM+JEs48IZM+gmyQ8M9YTY2tIpynB5+uMBXAEzOdC25vIWlRA3OXAddKEWPKBdwGP2YMIWHAatlhJH5vDi6lDIsC0tYLEeE4ADNsADNmA6q/M6rZM6tRM7tzM7v9M7wzM7py8VRqEWqi/LMpMz15M9paMrywMs1wJL5pM+I6M+59M2d1M/k8YRgy6ZZtIfK1EpJSfgtkQnmwn9Ds4FlUMDOsBBH9RBV+BBJTRCIRRCKbQDMFRDO2ADNOAAKCMmTQkBGuMvW4+uBJOtNpK9Nu45ZenFxsOXYlRGZ5RGfUmjfu+iFsABRCE92//TR38ULzyTm17oM1ghFY4USZNUSZeUSZt0SWUAaRhRNMcSWk6Tlh7CCw00BammGl2zKWDz/17iohrqosqUTM/0TM1UTdF0loTp5hhgHLQMDqONKVPUKXfMOeVqTMAPbfr0oKwGxXJpAFagR4HUUA+VLd6TNuLTX5RlA+jt9yJVoyZVUiuVUi/VUi1VAO7v3vjjA0mzSicRIBfmBAWkIC/pzBDSLQNtKuHSKa0tTOvRTeekAoCtRDESRamNFfPMMIWDkwDzRUfJVYW1VYuVWMcKYqgOUZeVWZtCSKOQSM+CJjoAPRCkRK61aLIVXrZVdeCj1SYsB3kqEkVwC22SQFX/cxrH5zRU9TWx8U8TM1ZnT15B8rBSgwA6oFLibByRcySU8znUy9oMU7w8oxXYbGje1U8TFh2bKgG2QruaFWIj1iAU1RDlk1pLCSUGoLaktD/3kTFKEy7MkhJNEGSb4kB9zUuJIvSI9VjhdWFldRjbz/c6IFnMAggB80Q/Zgcfowej7geR7R/OgQTLa1iD1WiLVl7NhyEW4BAl1mmX9Vm/KFqbwjUuFmNNQmPvUoEo7OjOqGTdwkqRqRSjUUtzkDXtDOre1RNhFWbZNlIiYOccCWNmrHDoNOwG09qg0leVRSqHTWH/FmFzCIMatlSf1nB9VFHvsWkHxGqvFhxrq5g8/zXW/jNU/7EEsbRAteRA1TVBW/Bo2/Zl53VtGQRuV+HuRA5nzUtnVZEOd3WgcJJqleV2gmx0WfZzbddVu0beDmBqD9d3OVNIKQI0nWIfqLVe2RG64ANBknd5EyRrpZRrR/NjKxf8rhTz0HUtNS1lawJMy4YvvzdmKZAvI6Y2PCAX5pIt9pVOkxNv0VFgXWcd4s1tAxdwAzYMHSIiBIAVvvZ3+9fTFJUhGBUtjHd5z29B0kPqkuSHsjWTILdTrVe6+pFcLVdwMJd/O+JU41AMFdRYwxe7wld0Fxa7GpZP5DZ9TVR1/Yl1l9N1V9QhOSnr3DVe6Tdwx3ebFsaO/FeHtf8yeKXWYtnGgBXYSQ5YXxTYaBz4trh2cisCQCe4ejcsSzV3SxGUXb9Uhr1X+Q7WwKZMxLZ4XmNyLCCgydZiTvWvTnXVwP5Pb12HQLTOdr8MjtcnjuH4//4usYykVndYj08SgL9yca2icZ/HeWZKMRr40TqWSvltC88yhCxRLXGILTm4aFdojuXYkuk49WbD/rDkFANzdQdTfHs1dgtimfIhFeLLjB1Xb15VagKYIg7gFvZYloNRSJmvd4mXgHGXcJyHxkRsrLKWXKJX1qiXC0kiilHQbCM52PhyqLZCJJ65j6BZmqOZmqdZmhFA7qo1QTRZFcj4ONe3X9u3jvPUUnb/gVd+D5tqVJ3XOZA4yjsW5iXOZBVmmZ4DkWL9mC0Mi519CaFSiHGEDZgfON9kkphH9iYvmCM2F9qqWGUVUva4KBV0YRzMwRx0wRx2gaIx+qLN4Rw02qMzmqM/eqM1+hzOoRZGgQVO6p1bdeYqxSSJInUPTQdX+F9zjBB/dsvSgRVWYAPEZwB+GqiDWqiHmqiL2qgHgN7ojUzKZIhoQwbqGaqXMGqHtyn2IRQWoAKyWqu3mqu7ep60mmgWNqCT2D+lixnDJGwjb2yxF5K1N22VxzhsKi/xi66vSlmwbx/QYTJY+T9sxTiV0m4pwl95kDmfkpzZQi/uw6tuIRfOExdq/+GxIxuyJ1uyK5uyL9uyLRuycQEXcoEF4mWPOCwFopq0ke+eE+CP8yMf1mG1W5u1X9u1Y5u182G1w4eumHmsrUIsJQ+h1WIEDZpAe1sjMviDVnB7O+ItuQ09tvEPi8dvmptFn/sPsWQdVsCYFhTBUrsjYjoHaYimCbuFCxN2/QyB9EI9Y1DZcIiQC8AFStu9926qbzkU4W1wWzW3nQIml3hcm7Fcwy81H9mIJoihufeKt9VoagE3ynvM/kENbdghBIA28W8ijUQipi320pisDhtzTG+fzkyEKuK9Q7zlEhefnU8vFgABtm1n7rspdtushRstRHZUgxvAl8e4OViYzP+Eue9CVSDNoadkJFhAHFFYpr0blJO2I+HPOTyDyZfcyTeuyaE8OFyjFjSMXxPAAkRcy8+th6k6FBXgn4mVxZFRmHmbmC3Dc0TZVJmmmdB2maXK7BA8wYlSn8tGIgxIwsFOsMXZDjVcc6r8eC+pALJ8ywu90/pYgK0jfv25bMa8JjxQv5uYvyl4QMcWxjHiZBf6uDkiue0YAtBwvuHLE6vlAO4Ipom8u3cWL3r2denCH5SQJK8D0L+Mygbd0G/9ybpcvkMRxQnTgxy9I6YUzc4aTNLaw495zU+0zQccuQt8dVSRJKPd3qSd2qfd6nIZzgtgmdxOfVM5nO0Ui9+X4ez/zSj04YUtBdB3+WNsHdfbHcAQXbtZTAEUJvWAXYEQ2cwVmdIpuHBzcopRFscpefTk3Cr3QdRfTFDwjkW3G9UzEtyLlSGSfNyvIjvSHVXfg9DdXeMH0dq80stjt9pD/k/izSDNx943Ir9jZ3oVuf38myKQPTsGLqVu/M3dSkwJHgFfnYD3Rj22/Q2PE8X2HNyByM8tZR3CgRXwwfmwzspZ8T8yfuOjHpZuukRaOktQrYTo7+THqGNljnZCtYs/psNqPLTXddM3otNjU8WuMi7+TJ91OQGag9uJPOuNnDpuQjdyywVpaLyJFyu8ShVYYAMYAKnNQUBwwbfeZyEhAOql/97xpegJUwjBdp3FGmztimTrM2JK14bYv+TIeIpQGrYFLh0hyg/NyYPZOd3ZSWLH7+Lq9sEBWBVZ477QlBFS7vbu9yHvVc17JZ54/yEfzKEWRAEFGvaQRuLzZN3Kw/xEGv/xnz9/orM+XIj0OWnegWhdkVi3+3PKKAIBLAJaSO4KE4MA+j4Uy0/Efij10f7Hl7fYQNYwitddc1zSayKmG0gbv/sxlC1x4JLvPwMg9o1ywCDBgAEFEhZIAAFCwnP/IkqcSLGixYsYKdYSUABCggQLPTYEWcBCxpMoU6pcybKly5cwY8qcSbOmzZs4c+rcybOmSIYMOzJsGMHhAog9W//uW4BApFCHPyEMQCVzHYKODX8ObZhww76kO1URyApyJNasBUSBhekhIdSzXIEOEDfTldunQB12LDCg1te1+/7+8/dv3wa3efNiTZAvImGWowYAhRvUY0JWazPuqzVg5MeoldPue6x0HwOOWVOnLjAuM8rNqCcDDZrApOvbuHPr3s27t+/fwJM6jQrXKFLSvdcxFQkaqkGqMdcR2JoXKlcEXoOzFEsWa8jUBNRqn9h2b1SzHufWRTycNkgCrXQ/RlfBfOX7Hhu/jDxZdVmQD0DXWyudfaeYUw+I91JgDRSnml6tBbcRYk8JlZBt42Wo4YYcdujhhyCeRFxzXB0lEXL/t5G2lH3tiTRVVVetFldc2AkWYkSsjIXeeUWFx2F55qm2mHoy2cViYkL1hRthjxnp3Igj6ecSf5aR5dxCBWDmG2fWPYmkWiimRNg+GnRU1IMKIYDUbkxGNCFXzM1WAAc31mnnnXjmqWeIVVZoWVAFmGgjaYSeaKhjhw6WaJiFrXgWXs69yGhK0m0V51sJEMDBX4Ui6qmin3YK6qiiTlqRLQJ0h5aQCoa66KuuxkqqoeUNF2eSdBXJnp9bDRDfqGCNiQ4Hd7Xolkf6fKoSlWVZ+ZSvE5UK66yytikRZ8XxKlWrLu3TYHdDYVmAmoJJW+20olK0Dy4IrToUSB+lsOe8//TWa++9+J50pFP/BYpOYIEVJvBXBA9scMEIH6wwwQAv5Z+XDgUI41nvdukQB+skrPHCHG/s8cEBYzROu1WO1GUBMgDc8coft9wxsSEthlZQRMbkJKSzcdWKjSz3vLFEgd1ymJWJvZUfTJHtqCpIvrrstM8DN8zlo80llAqwLTW4GLx/LqRLwE+H7bPUnckoWwLy5qv22my37bZuEWhLG1kMaNBAAxtosMHeevO9d95/9w2434QPbnjedt/dgAbhQlyhK1ivVKmMRpeot+CCG4554JwXvrnnnDPwK0a6pDrjzOZVYLfmfx/+Oeugx856feYV12+ENrOH33cFsBCLK/+u2BJ8LLYUT7zxxdtyvPLJL++88sC7MgoLh/E+nJ9Hv8SKZCVfb1kFnQ/e9/iEky9++X7fnfgCSu/ukCoyGXbsf2XtVYH64ZsP+/6vj6/43RVoHMX+JIO3GfCACEygAi1yAWM5xFJ6UYgEJ0jBClrwggrhSgYhEDf8/CQhtphYat4FQZJg8IQoTKEEpXIKgV1EF5Sh2ELCpcIa2vCE9asYnKCyJpjcrDlccwgBhoiQARDAiEg8ohKTyMQlOtGIfCFAAaRomZAUDVAj4VRLblbCB4rrhmC0YAQ9A7G8DCAWMBkTCo41QpGcaVxhjOMGx8jB75hshgWA3wL3yMc++rH/ThxgDokeRBb6CcWQHkHkRw7JyEQ+hZDneWCzPLKLqkyHaJXzjJwcyclFdlKRoGykJz9CAAFd5ByfISSWqJPIRG5ylKH8pChj+Zn/VI53FUDHerSFR4pB0EomC6YgWSnM7nUymJVxSHZecgvrBCkh9dPKIWdDTVda8yPVxOY1IVlL7wWlh90SxSXb+EuuafOctExnI7n5Lsok4BZ/jKc850lP18xgkl0qGs6AyM8r9pNEFauOWdioASm9xCpw0dY//cnQhTqUZqjgGUXWMbQz6cV7T9pnQzf60KqhLocHMFMBlpm7fbnPlrx04Ekz6lFi/qRHqZAoSgijHA465ABe/+SazA600pT21Fb4YeheENAYmarEFaip4mce6U+NdpSjAbXVn2waKF3U86pYzapWU3ILT84oqCP6qVhZulJ9MmeVWIJAdoyKkslhkqdkjatP5arSjI7iJPsQ5whj5tC5+rWucwXX/My0LZo4Ca5Qfapi92lHqQrAqgsiE2GPxVezKTaxicULvIpVgAawVSXpiJEmIXbHsNJ1rH+tThAdsoJ1bPW1sI2tPCkq2irB1amYvWxd3YNN2hBgFJ9ta23LqtvcGrchLzpJjuSEgNu26LjQ/eeqGruaVyhri8VyLmoBy13ErlIxnyHABgzKEsKIwkHoYeV21xtXsKI1LplSS/9wT0JR9AZzp2Usrn4tNEkBaEm2AA6wgNe2j1Yk1TNOTe1pF5xgfrnzIfPNiFvh1NTuMtjCKS1lcNMRUnhhk6n8VLCILVzaB51pAVKKsEUOm9/ouji/aNXnFHcmk3GYzqLTdZd+j8vYrq0mIZCN3yh0BMn2YvjIZcXZow5QjgE7+clQvpM6OkxOI784ugkeypky5YG/qLgiCG2j3HZM5ivyxZQX2UcqpNhFlD63zHAWJFFGIgDgRo4lP2TviMfqXSsSjQBdjsmYOkBkbpp2zxc2q2xsB+gvZwQd+FSlbVsM5wo3tCEEWIGjo8zpTnuaJ/vgzoOuXOmWXslBQN40mMf/SeFSk7qhBbgrRvzxFXQsZ86UfnWpH6nKQOGjJizWNal73OpEAtq1NMkFNIVUWmF3tMfHsui4KlkTgRRaRt5xtX6lOrOMffrb4A53T9bBAskI9EC1M1q6163udrMbtxbjigBUoWow17Y973a3vvPN730jN6KzbpQtpEjY3fq73wg/OGlNtlkEmKPeFclzoieuYMRm5Y3vSYFrv2KqlISaIx5+GKJHbmqBcoUAqkB2TcgtgHPu0JneS7jM9X2gHxPAFSETt853zvOV7IPcBMevs1885hFKkRU5jw4VdTx0bTOklBkZU2BasQDKvjXXTu8rOUm5gK/ZROJN5/F5Hpkm/wIsoBXevrNSWsFmkrAS3mGH8WreA1yIX4TlzcUjULOeWy+JCwE7K1jPB0/4no/pH+tgBQOGuCuFz/zxdKVsAQRAgA54feX3hrzmHa/wqQS3HxHZxzk6gACE7GXzqOd8up3zngEgoAPnsHvEM5j62rc7NGSfYqBG4e1yzSQwutjAEWkPJ9tvPjSsT8gQNxB7F87kMYFRxQIYD81sqf76CK9QQhDSgYcX/vvgF7c/Dr+PdKCCBfU5iBzXD0b1b6AFuQhZx1UiHfbbf/0DaOFJxg80XYgikKZ3fwKIQgchGRwgCrsANl83gAxoQQVYAR6QCrqgcogyfz63D7YgChbAF/8B2ID3534sEH+cYoEYoUW0Fhjr0AossIEC0IEeKEdFNFISOILhV4M2+GRh8nPjoAuqwAIr0AGtE4RCOIREWIRFiDcbgIQbsAIsIAquoAvlkHY2AXr5cDdGeIVYmIVaSIQNMDossQ/5cA7j0AqiEAorkIRbmIZqWIQswAKj4AricA75IHsYEQ5KuIZ4qIVIiDdM6IS5UA7pIHhgERhhKA5k6IN5mIits4d704etoAtySIcXuA7lkAujIAor4AGKuIl/w4dN6ArjEIU3OIqkGG4Nc4qomIqquIqs2IpJxxP94IqyOIu0WIugB3rx0yi1uIu8uIprEYu9GIzC+Iq9MYz/xuiKwXGMyqiAgliKzviM0EgvXuZ8fNSM2mGN0VgR06hV2Oga0yiJ2RiO4jiOOUGC1UiOdwKON2iO6NiO7kgvj8GOuWEtSSGP0YiL8pQu9RSPsEWP7/iPABmQAjmQBFmQBnmQCJmQCrmQDNmQDvmQEBmREjmRFFmRFnmRGJmRGumQ9riRHvmRIBmSGNmRIlmSJnmSKJmSKrmSLNmSLvmSMBmTMjmTNFmTNnmTOJmTOrmTPNmTPvmTQBmUQjmURFmURnmUSJmUSrmUTNmUTvmUUBmVUjmVVFmVVnmVWJmVWrmVQKmOXPmVmeGVYDmWwaKNQKMTEYaPEyGIYvkSyEGS//LUljvBcRaxjeOhlkuSIXipEnI5l2nWl2QZmDtBGqDnD3u5lzdhgtESergxKJHzloJGEdDnG/yodtWmIYHBfyeimb4hGCojmTnBMHVCl9eVjhGRLNFyeGcpmKypIf5gD/wAm/ZwD7HJDxOBmNU2fvxQm7u5m8gBeoC5lgLDD7TZm/3Qm7t5D8qJnMzZnM5Jm/dwnLDJD6q5JMSZnNiJnMfJD9vJndrJnP5AnZqpIhcBlzQxGtfZm8pJD7ZZjIupm8XJnNvZnfNpnMjpD/FJnYpinvKRntlZmcLpmvSgnPl5D/ZQna2ZoNrhD+4wBVewCI6wCIuQCI4wDPqZmBbhD//foAUOOgUeughRgAx16Rr80ARTIKFT4AiK4AgR+qCLcKInCqESGqEzKqE2SqMy+qIeOgWJUAiOMA/U6BrAUAg2WqRGKgUSiqRJ+qJLqqRSMAmbwAzSYA3e8A4HaiqcqRvuQKSLsKKJgKJY8A6rOY//QA/wQA3DkAlZcAUxWqRI+qZLughKKqc2uqMSqgiUEAzTwA5XmiFGRQ/BoAgQiqOC+gjywCjBGRPz8AiOcKJfaqNX4A7qoqCU6hveoANJAAdz8AZtMAdwQAS9YA+DoY48ww/YEAVEAAdwQAdw8AZwwAOmgJuZQQ+p2qquSgduQAdygKtvEAdz4Kty8Kt0AKz/vzoHwXqsxjoHxeoGfpAEb8AGc2AE8MAbvrAFyuoGyoqryvoGb5AGz/qt3soG4Pqt3xoHcUAGZOAFRCAIkNALxwAPBzqibMIOYwAHu7qrrmoETiCm/CkT42cNkSAHZkAGbuCqb7CpB+ut5Cqu4TquDMutwHqwbgAHPWAEkkAN9+CnFWGYw2CvcpCr9mqvdDAEnxCvIRIPe1CwcpAHwdqtSqAN5FmpMjuP34AGgDAHeZCzygoHQ2CojYKhhjkNcNAGdHAHd5AHyjoHXQAMvmEPuYqzSKusOpu0OcutCGu1WMutWnuwBxu1B2sHgAAH8EBruhEMYZC0aCu1dMCya7ur/2z7tnKgB2tLB3SQtFw7tEPQBkbQCM8ADv2KE97ABXU7B2u7rXtQD2N6G/xADUCwqoWrrNDKBkibB21buZQLt3OLB1CbB9D6Bn1gtW1QBMmgn4m6IIMxDamqB4RLuG9At3HwBkPADKIaIuCwB3Rws3eQtH1gBN8ws77LJt9ABkhrB3PABoBwsHrgBoowtuZpIxzHD8OgBKuas5yrrH2wtH8bE/WArdBqB56Ls8XrB35gtWlbvuZLtdvKtUbgt7vxC2dbvJB7s4Qrt/RLB/V7v7pqv7c7vgebtMfbrT+wBL3gDtlLE+6wBf3bv9D6B/LQG/4QD4Wwqm9wvGxQwdQ7B//4K7d4oL/4u8Fyi7R9wAYh3AdSmwZQIKkbMhr+4A1OEKxXe7xpWwTHMJn/IKu9AQ5/ULchDLl+AAffUMC/G8Qz9Q1m4LXi+wY5iwdy4ATWYJg1MX728AtFUASDy7mey63YmxuCYQ9U/L2RmwfcqgM7sANDUMZD0ANljMZnnMZsjMZqnANm3AM9oANCAA823BNme75zgAZp0AZpwMd+DMh9/MeDHMicmq0XDLZzQMJDoATJIKp3DBbsYK39q6zEy8C90Q/WoAPD+gZ+8MVs0AZo0AZDMMiCLMiBbMp/PARF8AatDAhsYAd2kAd2QAc8oAxAjBP+wA5LAAeqW7x0wK3//wu+gVAE0xDJv1G7SAutUvsGf5ANuSzE0mwRRKwHXBvMXUu5cPAH00CdyBx18/AJPfAGgaDAgNAH46u0wICg3ui027rIOFu3z/AO7vAO9mzP9UzP+LzP93zP9ZzP/vwO7Vm27wu+dRsHleAMCu0Mw7DQDO3QDa3Qz+AMwkAJklAIf0AG5qoHeeCrrYuzb6ADnwCkvsEOX1C3x1u4bPAHJM0mwFAE4Cu1yooI0PAMzxDRD73QOJ3TCt3Qw/AMzSAJrXu0U4sGzGCbpesS8eAIrUq8dmu1g5utTgCzQToe4MAHR0vCOKu6vBvN0/zVEUHEwey48ZvScBAExHChMSEP/4/QA3TAzJCrtTi7BcPAzq5hD3KQu0lLt3CwB03sEl6dE75Q0Mp6s2twCW2S2Pu52EwCxd+gDJGQBD8A1YBAvXrQAz8a2Cph0le7uiuNuLzhD8KwA5VNuXdQt3AwCX2q2KzN2IMxfsSQA0Wbu578q5Mw0ONBD5mwqvA7B4GABpYADeMbzJtKuG5QCPFg18Fx1bR9sPb7BrybZmA93S8RvDdLB5pbvf57sG1Qssp9ltYyGvKgCGhwu5/71pz7uVCbxUltEfbQqat7tXDgDX/b3iyRx2gbzGuwCX87ft7QC0LAB8QNv5RbBJFwD5qNEgdc3B+9qYEgpqEtDEMAv3QbzP9tQAm4nUb/UAxcgLN3IK4HGweOkLEZwg/C0AaautdyMAjywA+/QAQ4+7nXHQeSMA/f7Rvy8Ad73b+s2rvU/eMzsQ1o4Nyqm7PoTLebyrk9AAk2XtUVKBGF6Q1QMARrK649rAd2AOLgu7SWCRb24K3xnbRt8MPnqSH4jbZvgAaUgBP3UA170KoWLL98MATGkOAnseDbSrfiygcNHNrOQAZzAMur+wZrQAnfvH/HILyxPAd+QLhwMAUIPh7+kAxuHbV16wZ7QNX8EAnYSrzke+GzuyE53r948Kw76+NAnuousQ1q4NxdGwiX0Mt6sLLo3MOKkNxOvrH+QA1OAAd+ULf/SFsEl2AJa5AH5420Wcwb/PAD0BrVHd0G3gAT9g0TZ17YdJAG/G0TuqkMRtC1VMuqUNDSuTHJrMsGdJvS4j6PJ97bdNsH+53haUQM9brHXcADbeAGSqAF8J7J1jC9dhsIQyAN7ekP8hAFacDRCLupQ2ChyDHtGZHjgT4Hmtu/PqzqFs8SQu7c4xsHiPANx2AE9ivCw90GhADN+oiLo5EMbhCsN0u5bTAJ32AJ5c3j6mznJ0EPYD64gDC+cPDXYjKpHeK+5Uvoa24THOcPvYAGCK+sHE0HP4DLu4Hni6znb8DnDvzn8IzBm1roh34SyEAEUTAJnyAMyTAN1LANTQ4c/6MBD4Pgq1HrqTkgDCTuGN6gBHGgs1A9BKMr6n8grkje7BVfmhc/+BEh5MaLzXMgCODAD9LgBm0ACLAsv858DUidEfxwDEXgy7RMuUMgCfIwD4mQtsi+zr3x5c6dtJ3rDPCw+qy/DfDg+rD/+vHA+hDuIdWerWqemKPxD/KgqarLrXmgB68bCV1/E+RO3INLzum+JOsuroPO9TZBD1Y6D1faD+Nn8ywhD4uArYssuXscCaIaMilPxTpLwnRrBNiA/S0B8Tdb6lod+IQf/xWR8VC7qYVwqPwwDU6QBnmQu31wvAARx000fv/2/UOYEOG8YG3gvJljx06eIZTm/ZvnyP8NnTkdO/IA5k/hSJIlTZ4kye8HmzkcW87JkweOESNw3MApMlNnzppFihgR0useSqJFjRYNFsZjR0Bz0GwSeVQqP0ttOPqBGHMOFH79pH4l6m6Lxz506LB5w0ceWLYo/TkjMwcQG7Nz3qzZVLCtwoMIo+4FLNWepBwd+/Tp+KbQWn99E9oTNoROTDZN87gR9O3f38CdE8r7A3EOnjcs58DR7Fn1atatXb+GHVv2bNq1bd/GnRvlNjUQ9dg5Xejdv37+qkGJg2cOVjZ69BAJZo/vQX/1KMmRIxcinR7RN9u74ibP0jlnQupWSC9NR5eBYI5/M/mOHUAQ7duVy2b8kE7/Q9FLTYq8ltKgBDZ/mEHDLD/8eG8OJC5CTyyP4ktMrf/8EqaNOdBqiY4+1qDEq9ccM+jChPzxh59gKJwDsZac+Iazkea5Ygg97tjQozamkAdFE0cCTa7R8DsttR+PRDJJJZdkskknn0RyGzQgugPHNwaJxy9/skFkozdKA26OIiwi7kR3FkEjjzv0+JKOIoa5JyrwsiOvi/MutCeN7VrCCpDxGMxDohxZInRDNiaagxJ7/iIxyV+UIu+uAl/rRxov6PgSK5jmEGIeEXNjZyxM6aKjPjog/O+tuFzk6K68Zmv0Qn74GSanjvTLgw4jlBEpVhHBicINPeDLaogsFv30/0fQ0KqLJTpQOxFKaaeltlprr8U2W6KkJHW8ObBUiJ94FjEiEJfsCqSNR+BBEcVtoljPtEBmSsZHhOhRJI7ERLNzM1lXesnbQN+Ao6439M0D4Tn0tcsPN9ygRC8oAyQvkKdkVM0fYxIs9UuY3hCiR4RilU1Cw8xCy8IL/clww6bM+jBEbdsy7o+Cs8oVjmF6dasaJQrOw8WWxlw0ySCbIs1FaGdmummnn4Y6aqilLAs/OQxB1S95ICFiMrvmyqONKLpB0ZpB5MC0vjkwo0ZGehaRg9iO7MQYN/X2DLSPygKBAopB+naCkCUICbxwwft+wpdknaR4KUkNpMQqHDvyVv+JokmWLdSW9kQ3a91UtYtUu/CSuLa6TWdHCK8pj6MIT+IkKVZ/jjFisjw83m+Y0k0ETTTSnF1aauGHJ754448HjNvNF0ZkrZH8mYeSrm9lo7k3luhGGkHkwAMPQPwApA1CsFkco3zzQEu0MO78L0+8W0wjjWnsoYcee+6fx57899ef/vrpoVbjmEIHNEzKNfyQQhz0gAdvfSwK0hkZbkzWIpSlxXmpatmGzgWi3QHGH/rgx+lM5I4pwCEmDZwDIr5xD3rwo4VdmVU/YsiP+8lDEsmBz3vegIR6IeloQxJN8JA3RCIW0YhHlJaUtjMqQwynJJApgtfe4KI3xA8ONxr/Dx3coAh2PTERpWHP3NiHHpU4iyxV9MZq6iZC2gToXKMzIM3KNEd/eEMydNCDS5oShp39Z4IssqCJQLcpBtkhDhHLWDHa0IhOGGMZ3IBH/tjoGV/5ZR6SIAP6IhWISiDCEJ1EBCIOgYhChHKUhCAEIiphLrPUZQ566MgQyBYVzNFmWfHhiLPoEKNaItGXvwRmMIVpFN5sRw/VI8QFnweZKx7TD60iD0eG4Ih4nG4eiYiDaVzSr0nKRj1h1E4f0GAkSo6kl22ElLfqQ6BulqRR+6AOPR6hoXPFAVNrsMaFNDcqs5jKc7k5kIbmcIc3gO8Oh/RPZ/yRjC/E4Qdo6IEQ/5ywiCwAo4O0EYk9KLER9JmmMl8qTUhDdzCRxk80HHlIU26FKSewq52x+SFp5qLFbTQmIeccZk51ulOeNk2Jr7wnIf6JEH384x7SgAPa7HKzpfjhB5OYB4rgSRJ7JKJQdaGbidz3klu9YZzHE2BiQPRSd44MntC7hFUyFRE9yKENkLiobSZYlrMEcmXCMAPl8HPQ1+HUKMSoAyyvMoc2uOEKi5tqbKgzjJsAIg1gM42QBKRSyXpEK0SyA0vqUzBw8S40HfldS/aQjRL11LSnRW1qk4gGXSLzn39B0TQE0YZcPas08DFCJ6LKM6piU5uYyuq/zLipPrQhjcYLa1MKyP+ae3yDEWhQTkH1FpNABEEbZHXNPjkXn6HeRlXjyWHYKDEUvxKFGGAYz7AClYdAOCKEt6FOMtywLz+d0CMcOdfqVhcT5+C3I3a4A/rggx1FYbc1MQ0dHfawDdU22MEPhjBtqGaW27EhmSb5lD+6MQh7YgoxLEHDMKTTK5JdE4xcDe5/7haw272hDTEyUATR8ygBPS4w/uhHPLThiSQUAbyVOVRHiDBG3czVYIHorun8MYxMTuiVCF1NMbhABwYZKg9puEJcY+MPazhBX5UJYxvEPIQhFIHMQ2gDGdBAhjSvOc1iHnOZ04CGgYI3Dy8jQ3QMvJpbNqsjf/AGZ/YcYUL/F9rQPVUeytggiH/2xSvU+UYiNAQRtPzhGVpWSFWzyR6OcBNPemLxUrFxj1mlCMchLE6qcbxqVcswRbOK0z3sAbv/hPVWl0A1q09tan6QelY0pAc4rDEMSiQCDm340lxggihA6MAX773QHyuosv/cI0N00Jfv4nCJePza1anO9bdZDUMaGqMLBb0PWsY7aKk4xh/wiAIc5qKflhhhCs+QRr6TcYxk7Lvf/Pa3v48xcIFLwxmJqN0rI8uHHDgDduXl8x+ElLSOGGHUvr5Hxkm961brutUhjOGhRT5ykkftp3gsDSGcSBQUgWMRP1hYG/4wjZdWtQ0syRVEUkxGgJ3L/1SNiAQlhE6JoBNd6EVH+tGVPnShT4ISk4BEMcpnGxrX2BBLT3rQnf70SUhiEU5QQhB+kIY1xGE8DAMER9b0hiIIA4Im0i6pEENt9MyuMHYRTR75cIlHGN3vSf871ifRCULop034eQMiZQNP6sRDEXCwy3TziITrtsvyIvEK5v2V+c1cHt7CaoN78B4IIkiDlhDvTO9AC0aONGISTr9EJF5PiU4EHfC3x3oktLDykvfe979vUjGB6tqjiKQekDCCGwZhDbJqWptiZLdqvsnV+LxBOUXADnbWsH3ud9/73mdDHJDthjjQGQe9mHptqh6pOXy/+2xAtoYE6l/w6nVCdP/4gxuekAxoj3AsiXGJN0AyQVoGybiPDnmWmyA792PA9yuzPtAKs4AlwpKG8kE9ougLf9Aoq5ALVikCZeA8thC0fugHbSCCQFiQmNisN/iDbPCRCwQMBMs7O2gD7Ii/OIgDOGjAHVwDNiiCG8gn4BPCISRC3Di5s1i0JCsJf6CHbbCGaiqtoqiqE8MqIsuNrTqX+IguNWGgKWIDvSkNvQHDLyTDMOSQN2gKlkgDUkg/2li/SBlDMUSLPhANAaEcrTipOcgOnECCYBAZGfuPfaKguqI79JAHQXiI+7IL/GoKOfRCMHREMqTDpRAU21kOJYgHC2QN6uAHBNGOjtADnPj/hKKZo8Dwin6Ihh74LPQxlTl4ghjhLVv6A2bJJb3CJUwhpEdEC0ncxUjUG0AoAhcswmEkxmLsDKppCTaxMGWSI5tCCedjD50DBn/5jzIKmJa4ojlIguqxw270xi/BlTnoATb8j0epQ28UEE3KEXXktECowzg4A/ERBngoiE+BQdYwme0qxNzYB36IBi+gEPhwxxxBx4K0py+JCdxJAx84BkxjDX/cgy/ECgBzE0gAB5GwqXu8qb6wh09IkP8SkmdZjOhji5iaIsfBo9BJmII0SDTAhlg0xpiUyZlEiG04A98IKiU8CRiExg6Zg53TjRV7I4/wg/TZjiyCCY7IlaRk98qlXAp6QwNSuJA3FBCltEquskNnIRg4+IM/UIIngIRhsIbdYhJBpCu0GEBB4gdqcITasY86NKOllMurnEvqmxA48IMo4BXb8Idr2IO37IiDKgR3IEmT6AvrwEGQZA83gISoug3VG41ztAt7sixFrMvLpEs0EEaa5MzOHMYjRCbem42exEWgvEJQG0qWVM109Ag06IXC3AtbW0105IhKqIRLaIZq8AbHfJJ8pL59/Jx/gAdpuARDIMjEmE3VDARLcAZ2wI15KAQ0oKzEiANngE2iyAY+kMxFHIJnuE6jgMxaTM3kLMhg9MzzRM/0VM/1ZM/2dM/fCwgAOw==";
    JSONDataPDF.accountname = $(".orgName").text();
    JSONDataPDF.currency = $(".acCurrency").text();
    JSONDataPDF.prev12monthsale = $(".gtotsales").text();
    JSONDataPDF.projectedproposed = $(".gtotprops").text();
    JSONDataPDF.projectedimpact = $(".gtotprojs").text();
    JSONDataPDF.impactperc = $(".impactperc").text();
    JSONDataPDF.areadirector = $(".adName").text();
    JSONDataPDF.genDate = $(".genDate").text();
    JSONDataPDF.prevsale = $(".gtotsale").text();
    JSONDataPDF.spendpropose = $(".gtotprop").text()
    JSONDataPDF.totpropose = $(".gtotproj").text();
    JSONDataPDF.PriceReqPdf = $("#div-pricing-request-input").val()
    console.log(JSONDataPDF.PriceReqPdf);
    var sortSystemData = _.filter(allData, function (model) {
        return (model.groupingid == 3) //system
    });
    var sortFacilityData = _.filter(allData, function (model) {
        return (model.groupingid == 5) //facility
    });

    var grandTotal = _.filter(allData, function (gtemp) {
        return (gtemp.groupingid == '7');
    });
    JSONDataPDF.grandSales = grandTotal[0].orderamt;
    JSONDataPDF.grandProp = grandTotal[0].propprice;

    JSONDataPDF.twoPages = "two";
    JSONDataPDF.generateddate = new Date();

    JSONDataPDF.sortSystemData = sortSystemData;
    JSONDataPDF.sortSystemData = _.each(JSONDataPDF.sortSystemData, function (allData) {
        if (allData.projimpact < 0) {
            projimpactvalue = allData.projimpact.replace("-", "");
            allData.projimpact = "(" + projimpactvalue + ".00" + ")";
            allData.negative = "true";
        }
    });
    var systemcount = sortSystemData.length;
    console.log(systemcount);
    JSONDataPDF.sortFacilityData = sortFacilityData;
    JSONDataPDF.sortFacilityData = _.each(JSONDataPDF.sortFacilityData, function (allData) {
        if (allData.projimpact < 0) {
            allData.projimpact = allData.projimpact.replace("-", "")
            allData.projimpact = "(" + projimpactvalue + ".00" + ")";
            allData.negative = "true";
        }
    });

    var facilitycount = sortFacilityData.length;
    console.log(facilitycount);

    console.log("JSONDataPDF")
    console.log(JSONDataPDF)
    GM.Global.pricePDFData = JSONDataPDF;
}


function deleteUploadedfile(parent, prid, LoaderView, GMCFileUpload) {
    console.log("global")
    var that = parent;
    var gmcDeletedList = new GMCFileUpload();

    var deletedlist = _.filter(that.gmcFileUpload.toJSON(), function (model) {
        return (model.deletefl == "Y")
    });

    if (deletedlist.length > 0) {
        var jsnData = $.parseJSON(JSON.stringify(deletedlist));
        //var jsnData = JSON.stringify(deletedlist);
        gmcDeletedList.add(jsnData);


        console.log(deletedlist);
        console.log(gmcDeletedList);
        var input = {
            "gmCommonFileUploadVO": gmcDeletedList
        };

        fnGetWebServerData("fileUpload/deleteUploadedFiles", undefined, input, function (data) {
            showPriceFileUploadPopup(parent, prid, LoaderView, GMCFileUpload);

        }, true);
    } else {
        showNativeConfirm("Select files to be removed", "Alert", ["OK"], function (idx) {
            if (idx == 1) {
                return
            }
        });
    }

}

function showPriceFileUploadPopup(parent, prid, LoaderView, GMCFileUpload) {
    var that = parent;
    var refid = "";
    var refid = prid;
    var refid = refid.indexOf("PR-") >= 0 ? refid : "PR-" + refid;
    GM.Global.ID = refid;

    var input = {
        "refid": refid,
        "type": "26230731"

    };
    this.loaderview = new LoaderView({
        text: "Loading Document."
    });

    fnGetWebServerData("fileUpload/fetchFileList/", "gmCommonFileUploadVO", input, function (data) {
        that.loaderview.close();
        var adata = arrayOf(data);
        if (data != null) {
            $(".document_cnt").html(" (" + adata.length + ")");
            $(".document_cnt").css("color", "red");
        } else {
            $(".document_cnt").html("");
            $(".document_cnt").css("color", "black");
        }

        that.gmmFileUpload = adata;
        var jsnData = $.parseJSON(JSON.stringify(adata));
        that.gmcFileUpload.add(jsnData);


        if (!GM.Global.Device) {
            var userPartyId = window.localStorage.getItem("partyID");
            showFileUploadPopup();
            $("#msg-overlay").show();

            var template_picture = fnGetTemplate(URL_Common_Template, "gmtFileUploadDialog");
            if (data == null) {
                $("#div-price-overlay-content-container11").find('.modal-body').html(template_picture({
                    empty: "Y"
                }));
            } else {
                $("#div-price-overlay-content-container11").find('.modal-body').html(template_picture(adata));
            }

            if (localStorage.getItem("acid") < 3 && GM.Global.PricingStatus != "52186") {
                $("#div-price-delete-btn").addClass("hide");
                $("#div-price-delete-btn").parent().find("input").attr('disabled', 'disabled');
            }


            var userurl = $("#mobile-menu-top-user img").attr('src');

            var screenTitle = "Documents : " + refid;
            $(".screenTitle").html(screenTitle);

            $("#div-user-pic").append("<img class = 'image-user-pic'>");
            $(".image-user-pic").attr("src", userurl);

            $("#div-price-overlay-content-container11").find('.modal-body #input-user-picture').unbind('change').bind('change', function (e) {
                var file = e.target.files[0];
                var fileDisplayArea = document.getElementById('div-user-pic');
                var reader = new FileReader();

                reader.onload = function (event) {

                    // The file's text will be printed here
                    $("#div-price-overlay-content-container11").attr("src", event.target.result);
                    fnSelect_File(file, parent, prid, LoaderView, GMCFileUpload);

                };

                reader.readAsDataURL(file);

            });

            $("#div-price-overlay-content-container11 .ul-group-price-details").unbind('click').bind('click', function () {

                var info = [];
                _.each($(".ul-group-price-details"), function (adata) {
                    _.each(that.gmmFileUpload, function (Sdata) {
                        if ($(adata).find("#li-group-price-file-name").html() == Sdata.filename)
                            if ($(adata).find(".check-btn:checkbox:checked").length > 0) {
                                info.push({
                                    "fileid": Sdata.fileid,
                                    "deletefl": "Y",
                                    "updatedby": localStorage.getItem("userID")
                                });
                                console.log(Sdata.fileid);

                                that.gmcFileUpload.findWhere({
                                    "fileid": Sdata.fileid
                                }).set("deletefl", "Y").set("updatedby", localStorage.getItem("userID"));

                            } else {
                                that.gmcFileUpload.findWhere({
                                    "fileid": Sdata.fileid
                                }).set("deletefl", "").set("updatedby", "");
                            }
                    });

                })
            });


            console.log(GM.Global.fileDeleteData);

            $("#div-price-overlay-content-container11 .div-delete-btn").unbind('click').bind('click', function () {
                deleteUploadedfile(parent, prid, LoaderView, GMCFileUpload);
            });

            $("#div-price-overlay-content-container11 #div-price-file-upld-check-main").unbind('click').bind('click', function (e) {
                if ($(".li-group-price-top-header").find(".check-btn").prop('checked') == true)
                    $(".check-btn").prop('checked', true);
                else
                    $(".check-btn").prop('checked', false);
                $("#div-price-overlay-content-container11 .ul-group-price-details").trigger("click")
            });

            $("#div-price-overlay-content-container11").find('.modal-body .li-group-part-cancel-button, .div-price-group-close-btn').unbind('click').bind('click', function () {
                hideFileUploadPopup();

                $("#msg-overlay").hide();
            });

            $("#div-price-overlay-content-container11").find('.modal-body .div-upload-file-details #li-group-price-file-name').unbind('click').bind('click', function (e) {
                var path = $(e.currentTarget).html();
                if (URL_FileServer + "PRT/" + path != undefined && URL_FileServer + "/PRT/" + path != "")
                    window.open(URL_FileServer + "PRT/" + GM.Global.ID + "/" + path, '_blank', 'location=no');

                console.log(URL_FileServer + "PRT/" + GM.Global.ID + "/" + path);

            });
        } else
            showError("Network Connection is Required");

    });

}

/* to select file for uploading*/
function fnSelect_File(picfile, parent, prid, LoaderView, GMCFileUpload) {

    var that = this;

    if (!ValidateFileType(picfile.name)) {
        showNativeConfirm("Invalid upload file format. Cannot upload", "Error", ["Ok"], function (idx) {
            if (idx == 1) {
                showPriceFileUploadPopup(parent, prid, LoaderView, GMCFileUpload);
                return;
            }
        });

    }
    console.log(picfile.name);
    //clicking Upload button
    $("#div-price-overlay-content-container11 #update-image").unbind('click').bind('click', function () {

        var input = {
            "refid": GM.Global.ID,
            "token": localStorage.getItem("token"),
            "filename": picfile.name,
            "reftype": "26240010"
        };

        //checking if file exists
        fnGetWebServerData("fileUpload/checkFileExists/", "gmCommonFileUploadVO", input, function (data) {
            if (data != null && data.message == "Y") {
                showNativeConfirm("File exists. Do you want to overwrite?", "Confirm", ["Yes", "No"], function (idx) {
                    if (idx == 2) {
                        showPriceFileUploadPopup(parent, prid, LoaderView, GMCFileUpload);
                        return
                    } else {
                        fnUploadSelected_File(picfile, parent, prid, LoaderView, GMCFileUpload);
                    }
                });
            } else
                fnUploadSelected_File(picfile, parent, prid, LoaderView, GMCFileUpload);

        });
    });

}

function fnUploadSelected_File(picfile, parent, prid, LoaderView, GMCFileUpload) {
    var that = this;
    var $form = $("#frm-file-upload");
    var params = $form.serializeArray();
    this.picData = new FormData();

    this.picData.append('file', picfile, picfile.name);

    console.log($("#update-image").length);

    if (that.picData != "" && that.picData != undefined) {
        that.picData.append('refid', GM.Global.ID);
        that.picData.append('refgrp', '26240010'); //Documents
        that.picData.append('token', localStorage.getItem('token'));
        that.picData.append('type', '26230731'); //Pricing upload
        that.picData.append('fileid', '');
        that.picData.append('reftype', '26240010'); //documents
        console.log(that.picData);

        that.loaderview = new LoaderView({
            text: "Uploading File.Please wait....."
        });

        $.ajax({
            url: URL_WSServer + 'fileUpload/uploadFileInfo/',
            data: that.picData,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function (result) {
                console.log("fileUpload/uploadFileInfo/ success >>> " + JSON.stringify(result));
                that.loaderview.close();
                showPriceFileUploadPopup(parent, prid, LoaderView, GMCFileUpload);
            },
            error: function (model, response, errorReport) {
                console.log("<<<<< UPLOAD FAILED. Check if URL_DOCUMENTServer is defined in GmServer.js  >>> " + JSON.stringify(model));
                showNativeConfirm("File upload failed. Contact administrator", "Error", ["Ok"], function (idx) {
                    if (idx == 1) {
                        return
                    }
                });
                that.loaderview.close();
            }
        });
    }

    $("#div-price-overlay-content-container11").find('.modal-body #input-file-upload  .li-group-part-cancel-button, .div-price-group-close-btn').unbind('click').bind('click', function () {
        hideFileUploadPopup();

        $("#msg-overlay").hide();
    });
}

/******** Click Sales Analysis button amd load load 12 month sale by default **********************************************************************************************************************/
function generateSalesAnalysis(e, parent, prid, LoaderView) {
    
    var that = parent;
    var requestid = "";
    requestid = prid;
    var repId = localStorage.getItem("userID");
    var token = localStorage.getItem("token");
    var userid = localStorage.getItem("userID");
    var input = {
        "token": "token",
        "refid": requestid,
        "userid": userid,
    };
    var loaderview = new LoaderView({
        text: "Loading Sales Analysis."
    });
    //webservice call to get impact analysis data
    fnGetWebServerData("salesAnalysis/fetchSalesAnalysis/", "gmSalesAnalysisVO", input, function (data) {
            console.log("data in sales");
                console.log(data);
        if (data == null) {
            loaderview.close();
            showError("No 12 month Sales Data Found");
            return;
        }
        data = arrayOf(data);
        loaderview.close();
        fnSalesAnalysisChart(e,parent,LoaderView,data,requestid );
    });
}

function fnSalesAnalysisChart(e,parent,LoaderView,data,requestid ) {
//    var allData = data;
    var selectData;
    var reRenderFl;
    var category = [];
    var sale = [];
    var allData = data[0].gmsalesAnalysisVO;
    var result = [];
    
      allData.reduce(function (res, value) {
            if (!res[value.month]) {
                res[value.month] = {
                    salesamount: 0,
                    month: value.month
                };
                result.push(res[value.month])
            }
          if(res[value.month] != undefined) {
            res[value.month].salesamount += parseInt(value.salesamount);
            res[value.month].monthyear = value.monthyear;
          }
            return res;
        }, {});

        _.each(result, function (dt) {
            category.push({
                label: dt.month
            });

            sale.push({
                label: dt.monthyear,
                value: dt.salesamount
            });
        });
    
    showSalesAnalysisPopup();
    $("#msg-overlay").show();
    var templateSalesAnalysis = fnGetTemplate(URL_Price_Template, "gmtPriceSalesReport");
    var templateSalesAnalysisChart = fnGetTemplate(URL_Price_Template, "gmtPriceSalesChart");
    $("#div-price-overlay-content-container-sales-analysis .modal-body").html(templateSalesAnalysis());
    $("#div-price-overlay-content-container-sales-analysis .modal-body").find(".div-sales-analysis-report").html(templateSalesAnalysisChart());
    var titleRef = "Sales Analysis : " + requestid;
     $(".titleRef").html(titleRef);
    
    var orgName ,acCurrency,last12monthsales,genDate,partynm;
    if(data[0].gmPricingSalesHeaderVO.last12monthsales != undefined){
         orgName = data[0].gmPricingSalesHeaderVO.orgname;
         acCurrency = data[0].gmPricingSalesHeaderVO.accurrency;
         last12monthsales = data[0].gmPricingSalesHeaderVO.last12monthsales;
         last12monthsales = acCurrency +" "+ formatCurrency(last12monthsales);
         genDate = strToDate(data[0].gmPricingSalesHeaderVO.gendate);
         partynm = data[0].gmPricingSalesHeaderVO.partynm;
    }else{
         orgName = data[0].gmPricingSalesHeaderVO[0].orgname;
         acCurrency = data[0].gmPricingSalesHeaderVO[0].accurrency;
         last12monthsales = data[0].gmPricingSalesHeaderVO[0].last12monthsales;
         last12monthsales = acCurrency +" "+ formatCurrency(last12monthsales);
         genDate = strToDate(data[0].gmPricingSalesHeaderVO[0].gendate);
         partynm = data[0].gmPricingSalesHeaderVO[0].partynm;
    }
    $(".orgName").html(orgName);
    $(".acCurrency").html(acCurrency);
    $(".genDate").html(genDate);
    $(".partynm").html(partynm);
    $(".gtotsales").html(last12monthsales);
    
    //generate the chart
     var salesAnalysisChart = new FusionCharts({
        type: 'column3d',
        renderAt: 'sales-analysis-chart-container',
        width: '900',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "subCaption": "",
                "xAxisName": "",
                "yAxisName": "",
                "valueFontColor": "#00000",
                "valueFontBold": "1",
                "valueFontSize": "11",
                "baseFont": "Verdana,Helvetica Neue,Arial",
                "baseFontSize": "11",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "placeValuesInside": "0",
                "rotateValues": "1",
                "showShadow": "0",
                "divlineColor": "#999999",               
                "divLineIsDashed": "1",
                "divlineThickness": "1",
                "divLineDashLen": "1",
                "divLineGapLen": "1",
                "canvasBgColor": "#ffffff",
                "palettecolors": "#0075c2",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5"
            },

             "categories": [
                {
                    "category": category
                }
            ],          
            "dataset": [
                {
                     "seriesname": "12MonthSales",
                     "data" : sale
                }, 
            ]
        }
    }).render();    
     //Click on account 
    $("#div-price-overlay-content-container-sales-analysis").find('#li-account-level').unbind('click').bind('click', function (e) {
         //inside chart buttons hilight
        $("#li-account-level").addClass("activeSalesLevel");
        $("#li-segment-level").removeClass("activeSalesLevel");
        fnSalesAccountLevelChart(e,parent,LoaderView,data,requestid,selectData,reRenderFl);
    });
    
    
    //Click on segment 
    $("#div-price-overlay-content-container-sales-analysis").find('#li-segment-level').unbind('click').bind('click', function (e) {
         //inside chart buttons hilight
        $("#li-segment-level").addClass("activeSalesLevel");
        $("#li-account-level").removeClass("activeSalesLevel");
        generateSegmentSales(e, parent, requestid, LoaderView);
    });
    
        //Sales analysys close
    $("#div-price-overlay-content-container-sales-analysis").find('.li-threshold-cancel-button').unbind('click').bind('click', function (e) {
        $("#msg-overlay").hide();
        hideSalesAnalysisPopup();
        $(".div-price-overlay-content-container-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");
        $(".div-price-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");

    });  
    
    //click on Home button
    $("#div-price-overlay-content-container-sales-analysis").find('.fa-home-icon').unbind('click').bind('click', function (e) {
         //inside chart buttons hilight
        $("#li-segment-level").removeClass("activeSalesLevel");
        $("#li-account-level").addClass("activeSalesLevel");
        generateSalesAnalysis(e, parent, requestid, LoaderView);
    });
    
     //click on Back button
    $("#div-price-overlay-content-container-sales-analysis").find('.fa-arrow-left').unbind('click').bind('click', function (e) {
        $("#msg-overlay").hide();
        hideSalesAnalysisPopup();
        $(".div-price-overlay-content-container-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");
        $(".div-price-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");
    });
}

//loading account level details -Click By account level
function fnSalesAccountLevelChart(e,parent,LoaderView,data,requestid,selectData,reRenderFl) {
    
    var category = [];
    var sale = [];
    var allData = data[0].gmsalesAnalysisVO;
    var result = [];
    if(reRenderFl != undefined) {
        selectData =selectData[0].gmsalesAnalysisVO;
         selectData.reduce(function (res, value) {
                if (!res[value.partyid] && value.partyid != "" ) {
                    res[value.partyid] = {
                        salesamount: 0,
                        partyid: value.partyid
                    };
                    result.push(res[value.partyid])
                }
              if(res[value.partyid] != undefined) {
                res[value.partyid].salesamount += parseInt(value.salesamount);
                res[value.partyid].partynm = value.partynm;
              }
                return res;
            }, {});
    }else{
             allData.reduce(function (res, value) {
            if (!res[value.partyid] && value.partyid != "" ) {
                res[value.partyid] = {
                    salesamount: 0,
                    partyid: value.partyid
                };
                result.push(res[value.partyid])
            }
            if(res[value.partyid] != undefined) {
                res[value.partyid].salesamount += parseInt(value.salesamount);
                res[value.partyid].partynm = value.partynm;
            }
            return res;
        }, {});
    }
    /*if the data is more than 12 then sum all the sales amount and show in others in Graph*/
        var icnt = 0;
        var dataLength = Object.keys(result).length;
        var otherSale = 0;
        //sort the result based on the sales amount.
        result = result.sort(function (a, b) {
            return parseFloat(b.salesamount) - parseFloat(a.salesamount);
        });
    
        _.each(result, function (Sdata) {
            //fill the dataset
            icnt += 1;
            if (icnt < 12) {
                category.push({
                    label: Sdata.partynm
                });

                sale.push({
                    value: Sdata.salesamount
                });

            } else {
                otherSale = otherSale + Sdata.salesamount;
                if (icnt == dataLength) {
                    category.push({
                        label: "Others"
                    });

                    sale.push({
                        value: otherSale
                    });
                }

            }
        });
    
    //for currency formatting in .hbs file
     result = _.each(result, function (data) {
                data.salesamount = data.salesamount.toString();
        });
    
    if(reRenderFl != undefined) {
        var templatedata = JSON.stringify(GM.Global.AccountCharts);
    }else{
         GM.Global.AccountCharts = result;
         var templatedata = JSON.stringify(result);
    }
    templatedata = arrayOf($.parseJSON(templatedata));
    
    showSalesAnalysisPopup();
    $("#msg-overlay").show();
   
   var templateSalesAnalysis = fnGetTemplate(URL_Price_Template, "gmtPriceSalesReport");
    var templateSalesAccountLevelChart = fnGetTemplate(URL_Price_Template, "gmtPriceAccountLevelChart");
    $("#div-price-overlay-content-container-sales-analysis .modal-body").html(templateSalesAnalysis());
    $("#div-price-overlay-content-container-sales-analysis .modal-body").find(".div-sales-account-level").html(templateSalesAccountLevelChart(templatedata));
    
    if (reRenderFl != undefined) {
            $("#div-sales-accountlevel-detail-content #li-acc-div-detail-id").each(function () {
                var partyID = $(this).attr("partyid");
                var currDiv = $(this).find("input");
                _.each(result, function (dt) {
                    if (dt.partyid == partyID) {
                        $(currDiv).prop("checked", true);
                    }
                });
            });
     } else {
            $("#div-sales-accountlevel-detail-content #li-acc-div-detail-id").each(function () {
                $(this).find("input").prop("checked", true);
            });
     }
    
    var titleRef = "Sales Analysis : " + requestid;
     $(".titleRef").html(titleRef);
    var orgName ,acCurrency,last12monthsales,genDate,partynm;
    if(data[0].gmPricingSalesHeaderVO.last12monthsales != undefined){
         orgName = data[0].gmPricingSalesHeaderVO.orgname;
         acCurrency = data[0].gmPricingSalesHeaderVO.accurrency;
         last12monthsales = data[0].gmPricingSalesHeaderVO.last12monthsales;
         last12monthsales = acCurrency +" "+ formatCurrency(last12monthsales);
         genDate = strToDate(data[0].gmPricingSalesHeaderVO.gendate);
         partynm = data[0].gmPricingSalesHeaderVO.partynm;
    }else{
         orgName = data[0].gmPricingSalesHeaderVO[0].orgname;
         acCurrency = data[0].gmPricingSalesHeaderVO[0].accurrency;
         last12monthsales = data[0].gmPricingSalesHeaderVO[0].last12monthsales;
         last12monthsales = acCurrency +" "+ formatCurrency(last12monthsales);
         genDate = strToDate(data[0].gmPricingSalesHeaderVO[0].gendate);
         partynm = data[0].gmPricingSalesHeaderVO[0].partynm;
    }
    $(".orgName").html(orgName);
    $(".acCurrency").html(acCurrency);
    $(".genDate").html(genDate);
    $(".partynm").html(partynm);
    $(".gtotsales").html(last12monthsales);
    var totalsales;
    totalsales = "Total Sales"  + " " + acCurrency;
    $(".li-total-sales").html(totalsales);
    
    //generate the chart
     var SalesAccountLevelChart = new FusionCharts({
        type: 'msbar3d',
        renderAt: 'account-level-chart-container',
        width: '670',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "subCaption": "",
                "yAxisname": "",
                "numberPrefix": acCurrency,
                "paletteColors": "#0075c2,#1aaf5d",
                "bgColor": "#ffffff",
                "legendBorderAlpha": "0",
                "legendBgAlpha": "0",
                "legendShadow": "0",
                "placevaluesInside": "0",
                "valueFontColor": "#00000",
                "valueFontBold": "1",
                "valueFontSize": "11",
                "baseFont": "Verdana,Helvetica Neue,Arial",
                "baseFontSize": "11",
                "alignCaptionWithCanvas": "1",
                "showHoverEffect":"1",
                "canvasBgColor": "#ffffff",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "divlineColor": "#999999",
                "divLineIsDashed": "1",
                "divLineDashLen": "1",
                "divLineGapLen": "1",
                "showAlternateHGridColor": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                 "scrollColor": "#3a4660",
                "scrollHeight": "10",
                "scrollPadding": "5",
                "defaultAnimation":'0' 
            },
             "categories": [
                {
                    "category": category
                }
            ],
            "dataset": [
                {
                     "seriesname": "Accounts",
                     "data" : sale
                }, 
            ]
        }
    }).render();
    $("#li-by-account-level").addClass("activeSalesLevel");
    
      //Click on by rep account in Account level
    $("#div-price-overlay-content-container-sales-analysis").find('#li-by-rep-account-level').unbind('click').bind('click', function (e) {
         //inside chart buttons hilight
        $("#li-by-rep-account-level").addClass("activeSalesLevel");
        $("#li-by-account-level").removeClass("activeSalesLevel");
        fnSalesRepAccountLevelChart(e,parent,LoaderView,data,requestid);
    });
    
        //Sales analysys close
    $("#div-price-overlay-content-container-sales-analysis").find('.li-threshold-cancel-button').unbind('click').bind('click', function (e) {
        $("#msg-overlay").hide();
        hideSalesAnalysisPopup();
        $(".div-price-overlay-content-container-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");
        $(".div-price-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");
    }); 
    
    //Click on GO button
    $("#div-price-overlay-content-container-sales-analysis").find('#li-Go-account-button').unbind('click').bind('click', function (e) {
        var selectedRslt = [];
            $("#div-sales-accountlevel-detail-content #li-acc-div-detail-id").each(function () {
                var partyId = $(this).attr("partyid");
                var salesamt = $(this).attr("salesamount");
                var partynm = $(this).attr("partynm");
                if($(this).find("li input").prop("checked"))
                    selectedRslt.push({"salesamount":salesamt, "partyid" : partyId ,"partynm":partynm});
            });
        var selectData = {"gmsalesAnalysisVO" : selectedRslt};
        var arraydata = {"0" : selectData};
        var reRenderFl = "Y";
        fnSalesAccountLevelChart(e,parent,LoaderView,data,requestid,arraydata,reRenderFl);
    });
    
     //Click on Clear Button
    $("#div-price-overlay-content-container-sales-analysis").find('#li-clear-account-button').unbind('click').bind('click', function (e) {
            $("#div-sales-accountlevel-detail-content #li-acc-div-detail-id").each(function () {
               $(this).find("li input").prop("checked", false);
            });
                var reRenderFl ="Y";
                 var selectedRslt = [];
                 var selectData = {"gmsalesAnalysisVO" : selectedRslt};
                var arraydata =  {"0" : selectData};
                
        fnSalesAccountLevelChart(e,parent,LoaderView,data,requestid,arraydata,reRenderFl);
    });
    
    //click on Home button
    $("#div-price-overlay-content-container-sales-analysis").find('.fa-home-icon').unbind('click').bind('click', function (e) {
         //inside chart buttons hilight
        $("#li-segment-level").removeClass("activeSalesLevel");
        $("#li-account-level").addClass("activeSalesLevel");
        generateSalesAnalysis(e, parent, requestid, LoaderView);
    });
    
    //click on Back button
    $("#div-price-overlay-content-container-sales-analysis").find('.fa-arrow-left').unbind('click').bind('click', function (e) {
         //inside chart buttons hilight
        $("#li-segment-level").removeClass("activeSalesLevel");
        $("#li-account-level").addClass("activeSalesLevel");
        generateSalesAnalysis(e, parent, requestid, LoaderView);
    });
}

//loading rep account level details -Click By rep account level
function fnSalesRepAccountLevelChart(e,parent,LoaderView,data,requestid,selectData,reRenderFl) {
    
    var category = [];
    var sale = [];
    var allData = data[0].gmsalesAnalysisVO;
    var result = [];

    if(reRenderFl != undefined) {
        selectData =selectData[0].gmsalesAnalysisVO;
         selectData.reduce(function (res, value) {
                if (!res[value.accountid]  && value.accountid != "") {
                    res[value.accountid] = {
                        salesamount: 0,
                        accountid: value.accountid
                    };
                    result.push(res[value.accountid])
                }
             if(res[value.accountid] != undefined) {
                    res[value.accountid].salesamount += parseInt(value.salesamount);
                    res[value.accountid].accountnm = value.accountnm;
             }
                return res;
            }, {});
    }else{
             allData.reduce(function (res, value) {
            if (!res[value.accountid] && value.accountid != "") {
                res[value.accountid] = {
                    salesamount: 0,
                    accountid: value.accountid
                };
                result.push(res[value.accountid])
            }
          if(res[value.accountid] != undefined) {
                res[value.accountid].salesamount += parseInt(value.salesamount);
                res[value.accountid].accountnm = value.accountnm;
          }
            return res;
        }, {});
    }
        /*if the data is more than 12 then sum all the sales amount and show in others in Graph*/
        var icnt = 0;
        var dataLength = Object.keys(result).length;
        var otherSale = 0;
        //sort the result based on the sales amount.
        result = result.sort(function (a, b) {
            return parseFloat(b.salesamount) - parseFloat(a.salesamount);
        });
        _.each(result, function (Sdata) {
            //fill the dataset
            icnt += 1;
            if (icnt < 12) {
                category.push({
                    label: Sdata.accountnm
                });

                sale.push({
                    value: Sdata.salesamount
                });

            } else {
                otherSale = otherSale + Sdata.salesamount;
                if (icnt == dataLength) {
                    category.push({
                        label: "Others"
                    });

                    sale.push({
                        value: otherSale
                    });
                }

            }
        });
    
    //for currency formatting in .hbs file
     result = _.each(result, function (data) {
                data.salesamount = data.salesamount.toString();
        });
    
    if(reRenderFl != undefined) {
        var templatedata = JSON.stringify(GM.Global.RepAccountCharts);
    }else{
         GM.Global.RepAccountCharts = result;
         var templatedata = JSON.stringify(result);
    }
     templatedata = arrayOf($.parseJSON(templatedata));
    showSalesAnalysisPopup();
    $("#msg-overlay").show(); 
   var templateSalesAnalysis = fnGetTemplate(URL_Price_Template, "gmtPriceSalesReport");
    var templateSalesRepAccountLevelChart = fnGetTemplate(URL_Price_Template, "gmtPriceRepAccountLevelChart");
    $("#div-price-overlay-content-container-sales-analysis .modal-body").html(templateSalesAnalysis());
    $("#div-price-overlay-content-container-sales-analysis .modal-body").find(".div-sales-rep-account-level").html(templateSalesRepAccountLevelChart(templatedata));
    if (reRenderFl != undefined) {
            $("#div-sales-accountlevel-detail-content #li-acc-div-detail-id").each(function () {
                var accountid = $(this).attr("accountid");
                var currDiv = $(this).find("input");
                _.each(result, function (dt) {
                    if (dt.accountid == accountid) {
                        $(currDiv).prop("checked", true);
                    }
                });
            });
     } else {
            $("#div-sales-accountlevel-detail-content #li-acc-div-detail-id").each(function () {
                $(this).find("input").prop("checked", true);
            });
     }
    
    var titleRef = "Sales Analysis : " + requestid;
     $(".titleRef").html(titleRef);
     var orgName ,acCurrency,last12monthsales,genDate,partynm;
    if(data[0].gmPricingSalesHeaderVO.last12monthsales != undefined){
         orgName = data[0].gmPricingSalesHeaderVO.orgname;
         acCurrency = data[0].gmPricingSalesHeaderVO.accurrency;
         last12monthsales = data[0].gmPricingSalesHeaderVO.last12monthsales;
         last12monthsales = acCurrency +" "+ formatCurrency(last12monthsales);
         genDate = strToDate(data[0].gmPricingSalesHeaderVO.gendate);
         partynm = data[0].gmPricingSalesHeaderVO.partynm;
    }else{
         orgName = data[0].gmPricingSalesHeaderVO[0].orgname;
         acCurrency = data[0].gmPricingSalesHeaderVO[0].accurrency;
         last12monthsales = data[0].gmPricingSalesHeaderVO[0].last12monthsales;
         last12monthsales = acCurrency +" "+ formatCurrency(last12monthsales);
         genDate = strToDate(data[0].gmPricingSalesHeaderVO[0].gendate);
         partynm = data[0].gmPricingSalesHeaderVO[0].partynm;
    }
    $(".orgName").html(orgName);
    $(".acCurrency").html(acCurrency);
    $(".genDate").html(genDate);
    $(".partynm").html(partynm);
    $(".gtotsales").html(last12monthsales);
    var totalsales;
    totalsales = "Total Sales"  + " " + acCurrency;
    $(".li-total-sales").html(totalsales);
    
    //generate the chart
     var SalesAccountLevelChart = new FusionCharts({
        type: 'msbar3d',
        renderAt: 'rep-acc-level-chart-container',
        width: '670',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "subCaption": "",
                "yAxisname": "",
                "numberPrefix": acCurrency,
                "paletteColors": "#0075c2,#1aaf5d",
                "bgColor": "#ffffff",
                "legendBorderAlpha": "0",
                "legendBgAlpha": "0",
                "legendShadow": "0",
                "placevaluesInside": "0",
                "valueFontColor": "#00000",
                "valueFontBold": "1",
                "valueFontSize": "11", 
                "baseFont": "Verdana,Helvetica Neue,Arial",
                "baseFontSize": "11",
                "alignCaptionWithCanvas": "1",
                "showHoverEffect":"1",
                "canvasBgColor": "#ffffff",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "divlineColor": "#999999",
                "divLineIsDashed": "1",
                "divLineDashLen": "1",
                "divLineGapLen": "1",
                "showAlternateHGridColor": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                 "scrollColor": "#3a4660",
                "scrollHeight": "10",
                "scrollPadding": "5",
                "defaultAnimation":'0' 
            },            
             "categories": [
                {
                    "category": category
                }
            ],          
            "dataset": [
                {
                     "seriesname": "RepAccounts",
                     "data" : sale
                }, 
            ]
        }
    }).render();
    $("#li-by-rep-account-level2").addClass("activeSalesLevel");
    
      //Click on account in Account level
    $("#div-price-overlay-content-container-sales-analysis").find('#li-by-account-level2').unbind('click').bind('click', function (e) {
         //inside chart buttons hilight
        $("#li-by-account-level2").addClass("activeSalesLevel");
        $("#li-by-rep-account-level2").removeClass("activeSalesLevel");
        fnSalesAccountLevelChart(e,parent,LoaderView,data,requestid);
    });
    
    //Click on GO button
    $("#div-price-overlay-content-container-sales-analysis").find('#li-Go-repaccount-button').unbind('click').bind('click', function (e) {
        var selectedRslt = [];
            $("#div-sales-accountlevel-detail-content #li-acc-div-detail-id").each(function () {
                var accountid = $(this).attr("accountid");
                var salesamt  = $(this).attr("salesamount");
                var accountnm = $(this).attr("accountnm");
                if($(this).find("li input").prop("checked"))
                    selectedRslt.push({"salesamount":salesamt, "accountid" : accountid ,"accountnm":accountnm});
            });
        var selectData = {"gmsalesAnalysisVO" : selectedRslt};
        var arraydata = {"0" : selectData};
        var reRenderFl = "Y";
        fnSalesRepAccountLevelChart(e,parent,LoaderView,data,requestid,arraydata,reRenderFl);
    });
    
     //Click on Clear Button
    $("#div-price-overlay-content-container-sales-analysis").find('#li-clear-repaccount-button').unbind('click').bind('click', function (e) {
            $("#div-sales-accountlevel-detail-content #li-acc-div-detail-id").each(function () {
               $(this).find("li input").prop("checked", false);
            });
                var reRenderFl ="Y";
                 var selectedRslt = [];
                 var selectData = {"gmsalesAnalysisVO" : selectedRslt};
                var arraydata =  {"0" : selectData};    
        fnSalesRepAccountLevelChart(e,parent,LoaderView,data,requestid,arraydata,reRenderFl);
    });
    
        //Sales analysys close
    $("#div-price-overlay-content-container-sales-analysis").find('.li-threshold-cancel-button').unbind('click').bind('click', function (e) {
        $("#msg-overlay").hide();
        hideSalesAnalysisPopup();
        $(".div-price-overlay-content-container-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");
        $(".div-price-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");

    });  
    
    //click on Home button
    $("#div-price-overlay-content-container-sales-analysis").find('.fa-home-icon').unbind('click').bind('click', function (e) {
         //inside chart buttons hilight
        $("#li-segment-level").removeClass("activeSalesLevel");
        $("#li-account-level").addClass("activeSalesLevel");
       generateSalesAnalysis(e, parent, requestid, LoaderView);
    });
    
    //click on Back button
    $("#div-price-overlay-content-container-sales-analysis").find('.fa-arrow-left').unbind('click').bind('click', function (e) {
         //inside chart buttons hilight
        $("#li-segment-level").removeClass("activeSalesLevel");
        $("#li-account-level").addClass("activeSalesLevel");
        generateSalesAnalysis(e, parent, requestid, LoaderView);
    });
}

/******** Click segment level inside the home sales dash ******************************************************************************************************************************/

function generateSegmentSales(e, parent, prid, LoaderView) {
    
     var that = parent;
    var requestid = "";
    requestid = prid;
    console.log(requestid);
    var repId = localStorage.getItem("userID");
    var token = localStorage.getItem("token");
    var userid = localStorage.getItem("userID");
    var input = {
        "token": "token",
        "refid": requestid,
        "userid": userid,
    };
    var loaderview = new LoaderView({
        text: "Loading Sales Segment."
    });
    //webservice call to get impact analysis data
    fnGetWebServerData("salesAnalysis/fetchSalesSegmentdetails/", "gmSalesAnalysisVO", input, function (data) {
            console.log("data in segment");
            console.log(data);
        if (data == null) {
            loaderview.close();
            showError("No Sales Segment Data Found");
            return;
        }
        data = arrayOf(data);
        loaderview.close();
        fnSegmentSalesChart(e, parent, prid, LoaderView,data);
    }); 
}

function fnSegmentSalesChart(e, parent, prid, LoaderView,data) {
    
    var category = [];
    var sale = [];
    var allData = data[0].gmsalesAnalysisVO;
    var currentSegment;
    var selectData;
    var reRenderFl;

    var result = [];
    allData.reduce(function (res, value) {
        if (!res[value.segmentid]) {
            res[value.segmentid] = {
                salesamount: 0,
                segmentid: value.segmentid
            };
            result.push(res[value.segmentid])
        }
        res[value.segmentid].salesamount += parseInt(value.salesamount);
        res[value.segmentid].segmentnm = value.segmentnm;
        return res;
    }, {});
    
    //sort the result based on the sales amount.
        result = result.sort(function (a, b) {
            return parseFloat(b.salesamount) - parseFloat(a.salesamount);
        });
    
    _.each(result, function (dt) {

        category.push({
            label: dt.segmentnm
        });

        sale.push({
            label: dt.segmentnm,
            value: dt.salesamount
        });
    });
    //for currency formatting in .hbs file
     result = _.each(result, function (data) {
                data.salesamount = data.salesamount.toString();
        });
    
     var templatedata = JSON.stringify(result);
        templatedata = arrayOf($.parseJSON(templatedata));
    showSalesAnalysisPopup();
    $("#msg-overlay").show();
   var templateSalesAnalysis = fnGetTemplate(URL_Price_Template, "gmtPriceSalesReport");
    var templateSalesAnalysisChart = fnGetTemplate(URL_Price_Template, "gmtPriceSegmentlevelSalesChart");
    $("#div-price-overlay-content-container-sales-analysis .modal-body").html(templateSalesAnalysis());
    $("#div-price-overlay-content-container-sales-analysis .modal-body").find(".div-sales-analysis-report").html(templateSalesAnalysisChart(templatedata));
    var titleRef = "Sales Analysis : " + prid;
     $(".titleRef").html(titleRef);
    
     var orgName ,acCurrency,last12monthsales,genDate,partynm;
    if(data[0].gmPricingSalesHeaderVO.last12monthsales != undefined){
         orgName = data[0].gmPricingSalesHeaderVO.orgname;
         acCurrency = data[0].gmPricingSalesHeaderVO.accurrency;
         last12monthsales = data[0].gmPricingSalesHeaderVO.last12monthsales;
         last12monthsales = acCurrency +" "+ formatCurrency(last12monthsales);
         genDate = strToDate(data[0].gmPricingSalesHeaderVO.gendate);
         partynm = data[0].gmPricingSalesHeaderVO.partynm;
    }else{
         orgName = data[0].gmPricingSalesHeaderVO[0].orgname;
         acCurrency = data[0].gmPricingSalesHeaderVO[0].accurrency;
         last12monthsales = data[0].gmPricingSalesHeaderVO[0].last12monthsales;
         last12monthsales = acCurrency +" "+ formatCurrency(last12monthsales);
         genDate = strToDate(data[0].gmPricingSalesHeaderVO[0].gendate);
         partynm = data[0].gmPricingSalesHeaderVO[0].partynm;
    }
    $(".orgName").html(orgName);
    $(".acCurrency").html(acCurrency);
    $(".genDate").html(genDate);
    $(".partynm").html(partynm);
    $(".gtotsales").html(last12monthsales);
    
    var totalsales;
    totalsales = "Revenue"  + " " + acCurrency;
    $(".li-total-sales").html(totalsales);
    //generate the chart
    var segmentSalesChart = new FusionCharts({
        type: 'pie2d',
        renderAt: 'sales-analysis-chart-container',
        width: '700',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "subCaption": "",
                "numbersuffix": "$",
                "paletteColors": "#0075c2,#1aaf5d,#f2c500,#f45b00,#8e0000",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "use3DLighting": "0",
                "showShadow": "0",
                "baseFont": "Verdana,Helvetica Neue,Arial",
                "baseFontSize": "11",
                "enableSmartLabels": "0",
                "startingAngle": "0",
                "showPercentValues": "1",
                "showPercentInTooltip": "0",
                "decimals": "1",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                "showHoverEffect":"1",
                "showLegend": "1",
                "legendBgColor": "#ffffff",
                "legendBorderAlpha": '0',
                "legendShadow": '0',
                "legendItemFontSize": '10',
                "legendItemFontColor": '#666666'
            },
             "categories": [
                {
                    "category": category
                }
            ],          
            "dataset": [
                {
                     "data" : sale
                }, 
            ]
        }
    }).render();
     $("#li-segment-level").addClass("activeSalesLevel");
    
      //Click on account
    $("#div-price-overlay-content-container-sales-analysis").find('#li-account-level').unbind('click').bind('click', function (e) {
         //inside chart buttons hilight
        $("#li-account-level").addClass("activeSalesLevel");
        $("#li-segment-level").removeClass("activeSalesLevel");
        fnSalesAccountLevelChart(e,parent,LoaderView,data,prid);
    });
    
        //click on Segment name in the segment chart container div
    $("#div-price-overlay-content-container-sales-analysis").find('.li-segment-dtl').unbind('click').bind('click', function (e) {
         //inside chart buttons hilight
        $("#li-account-level").addClass("activeSalesLevel");
        $("#li-segment-level").removeClass("activeSalesLevel");
        fnSalesSystemLevelChart(e, parent, prid, LoaderView,data,currentSegment,selectData,reRenderFl);
    });
    

    //Sales analysys close
    $("#div-price-overlay-content-container-sales-analysis").find('.li-threshold-cancel-button').unbind('click').bind('click', function (e) {
        $("#msg-overlay").hide();
        hideSalesAnalysisPopup();
        $(".div-price-overlay-content-container-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");
        $(".div-price-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");
    });
    
    //click on Home button
     $("#div-price-overlay-content-container-sales-analysis").find('.fa-home-icon').unbind('click').bind('click', function (e) {
         //inside chart buttons hilight
        $("#li-segment-level").removeClass("activeSalesLevel");
        $("#li-account-level").addClass("activeSalesLevel");
        generateSalesAnalysis(e, parent, prid, LoaderView);
    });
    
    //click on Back button
    $("#div-price-overlay-content-container-sales-analysis").find('.fa-arrow-left').unbind('click').bind('click', function (e) {
         //inside chart buttons hilight
        $("#li-segment-level").removeClass("activeSalesLevel");
        $("#li-account-level").addClass("activeSalesLevel");
        generateSalesAnalysis(e, parent, prid, LoaderView);
    });

}

//click on system name in segemnt level chart
function fnSalesSystemLevelChart(e, parent, prid, LoaderView,data,currentSegment,selectData,reRenderFl) {
    
    var category = [];
    var sale = [];
    var allData = data[0].gmsalesAnalysisVO;
    if (currentSegment == undefined){
        currentSegment = $(e.currentTarget).find("#div-sales-segmntnm").attr("value");
    }
    var currentsystem;
    var currentsystemname;
    var currentsystemprice;

    var result = [];
    if(reRenderFl != undefined) {
        selectData =selectData[0].gmsalesAnalysisVO;
         selectData.reduce(function (res, value) {
                if (!res[value.setid] ) {
                    res[value.setid] = {
                        salesamount: 0,
                        setid: value.setid
                    };
                    result.push(res[value.setid])
                }
                  if(res[value.setid] != undefined) {
                        res[value.setid].salesamount += parseInt(value.salesamount);
                        res[value.setid].setnm = value.setnm;
                  }
                return res;
            }, {});
    }else{
             allData.reduce(function (res, value) {
            if (!res[value.setid] && currentSegment == value.segmentid) {
                res[value.setid] = {
                    salesamount: 0,
                    setid: value.setid
                };
                result.push(res[value.setid])
            }
                  if(res[value.setid] != undefined) {
                    res[value.setid].salesamount += parseInt(value.salesamount);
                    res[value.setid].setnm = value.setnm;
                  }
            return res;
        }, {});
    }
    
     /*if the data is more than 12 then sum all the sales amount and show in others in Graph*/
        var icnt = 0;
        var dataLength = Object.keys(result).length;
        var otherSale = 0;
    //sort the result based on the sales amount.
        result = result.sort(function (a, b) {
            return parseFloat(b.salesamount) - parseFloat(a.salesamount);
        });
    
        _.each(result, function (Sdata) {
            //fill the dataset
            icnt += 1;
            if (icnt < 12) {
                category.push({
                    label: Sdata.setnm
                });

                sale.push({
                    label: Sdata.setnm,
                    value: Sdata.salesamount
                });

            } else {
                otherSale = otherSale + Sdata.salesamount;
                if (icnt == dataLength) {
                    category.push({
                        label: "Others"
                    });

                    sale.push({
                        label: "Others",
                        value: otherSale
                    });
                }

            }
        });
    
    //for currency formatting in .hbs file
     result = _.each(result, function (data) {
                data.salesamount = data.salesamount.toString();
        });
    
    if(reRenderFl != undefined) {
        var templatedata = JSON.stringify(GM.Global.systemcharts);
    }else{
         GM.Global.systemcharts = result;
         var templatedata = JSON.stringify(result);
    }
        templatedata = arrayOf($.parseJSON(templatedata));
    
    showSalesAnalysisPopup();
    $("#msg-overlay").show();
   var templateSalesAnalysis = fnGetTemplate(URL_Price_Template, "gmtPriceSalesReport");
    var templateSalesAnalysisChart = fnGetTemplate(URL_Price_Template, "gmtPriceSystemlevelSalesChart");
    $("#div-price-overlay-content-container-sales-analysis .modal-body").html(templateSalesAnalysis());
    $("#div-price-overlay-content-container-sales-analysis .modal-body").find(".div-sales-system-segment-level").html(templateSalesAnalysisChart(templatedata));
    var titleRef = "Sales Analysis : " + prid;
     $(".titleRef").html(titleRef);
    
     var orgName ,acCurrency,last12monthsales,genDate,partynm;
    if(data[0].gmPricingSalesHeaderVO.last12monthsales != undefined){
         orgName = data[0].gmPricingSalesHeaderVO.orgname;
         acCurrency = data[0].gmPricingSalesHeaderVO.accurrency;
         last12monthsales = data[0].gmPricingSalesHeaderVO.last12monthsales;
         last12monthsales = acCurrency +" "+ formatCurrency(last12monthsales);
         genDate = strToDate(data[0].gmPricingSalesHeaderVO.gendate);
         partynm = data[0].gmPricingSalesHeaderVO.partynm;
    }else{
         orgName = data[0].gmPricingSalesHeaderVO[0].orgname;
         acCurrency = data[0].gmPricingSalesHeaderVO[0].accurrency;
         last12monthsales = data[0].gmPricingSalesHeaderVO[0].last12monthsales;
         last12monthsales = acCurrency +" "+ formatCurrency(last12monthsales);
         genDate = strToDate(data[0].gmPricingSalesHeaderVO[0].gendate);
         partynm = data[0].gmPricingSalesHeaderVO[0].partynm;
    }
    $(".orgName").html(orgName);
    $(".acCurrency").html(acCurrency);
    $(".genDate").html(genDate);
    $(".partynm").html(partynm);
    $(".gtotsales").html(last12monthsales);
    
    var totalsales;
    totalsales = "Sales" + " " + acCurrency;
    $(".li-total-sales").html(totalsales);
    
    if (reRenderFl != undefined) {
            $("#div-sales-segment-detail-content #li-sales-seg-detail").each(function () {
                var setid = $(this).attr("setid");
                var currDiv = $(this).find("input");
                _.each(result, function (dt) {
                    if (dt.setid == setid) {
                        $(currDiv).prop("checked", true);
                    }
                });
            });
     } else {
            $("#div-sales-segment-detail-content #li-sales-seg-detail").each(function () {
                $(this).find("input").prop("checked", true);
            });
     }

    $(".orgName").html(orgName);
    $(".acCurrency").html(acCurrency);
    $(".genDate").html(genDate);
     $(".partynm").html(partynm);
    //generate the chart
    var segmentSalesChart = new FusionCharts({
        type: 'pie2d',
        renderAt: 'sales-systemlevel-chart-container',
        width: '600',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "subCaption": "",
                "numbersuffix": "$",
                "paletteColors": "#0075c2,#1aaf5d,#f2c500,#f45b00,#8e0000",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "use3DLighting": "0",
                "showShadow": "0",
                "baseFont": "Verdana,Helvetica Neue,Arial",
                "baseFontSize": "11",
                "enableSmartLabels": "0",
                "startingAngle": "0",
                "showPercentValues": "1",
                "showPercentInTooltip": "0",
                "decimals": "1",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                "showHoverEffect":"1",
                "showLegend": "1",
                "legendBgColor": "#ffffff",
                "legendBorderAlpha": '0',
                "legendShadow": '0',
                "legendItemFontSize": '10',
                "legendItemFontColor": '#666666'
            },
            "categories": [
                {
                    "category": category
                }
            ],          
            "dataset": [
                {
                     "data" : sale
                }, 
            ]
        }
    }).render();
    
      $("#li-segment-level").addClass("activeSalesLevel");
     //Click on account
    $("#div-price-overlay-content-container-sales-analysis").find('#li-account-level').unbind('click').bind('click', function (e) {
         //inside chart buttons hilight
        $("#li-account-level").addClass("activeSalesLevel");
        $("#li-segment-level").removeClass("activeSalesLevel");
        fnSalesAccountLevelChart(e,parent,LoaderView,data,prid);
    });
    
      
    //Click on GO button
    $("#div-price-overlay-content-container-sales-analysis").find('#li-Go-system-button').unbind('click').bind('click', function (e) {
        var selectedRslt = [];
            $("#div-sales-segment-detail-content #li-sales-seg-detail").each(function () {
                var setid = $(this).attr("setid");
                var salesamt  = $(this).attr("salesamount");
                var setnm = $(this).attr("setnm");
                if($(this).find("li input").prop("checked"))
                    selectedRslt.push({"salesamount":salesamt, "setid" : setid ,"setnm":setnm});
            });
        var selectData = {"gmsalesAnalysisVO" : selectedRslt};
        var arraydata = {"0" : selectData};
        var reRenderFl = "Y";
        fnSalesSystemLevelChart(e, parent, prid, LoaderView,data,currentSegment,arraydata,reRenderFl)
    });
    
     //Click on Clear Button
    $("#div-price-overlay-content-container-sales-analysis").find('#li-clear-system-button').unbind('click').bind('click', function (e) {
            $("#div-sales-segment-detail-content #li-sales-seg-detail").each(function () {
               $(this).find("li input").prop("checked", false);
            });
                 var reRenderFl ="Y";
                 var selectedRslt = [];
                 var selectData = {"gmsalesAnalysisVO" : selectedRslt};
                 var arraydata =  {"0" : selectData};    
               fnSalesSystemLevelChart(e, parent, prid, LoaderView,data,currentSegment,arraydata,reRenderFl)
    });
    
    //click on Systen name in the segment chart container div
    $("#div-price-overlay-content-container-sales-analysis").find('.div-sysnm').unbind('click').bind('click', function (e) {
         //inside chart buttons hilight
        $("#li-account-level").addClass("activeSalesLevel");
        $("#li-segment-level").removeClass("activeSalesLevel");
        fnSystemAccountLevelChart(e, parent,LoaderView, data,prid,currentsystem,currentSegment,currentsystemname,currentsystemprice);
    });
    
    
    //Sales analysys close
    $("#div-price-overlay-content-container-sales-analysis").find('.li-threshold-cancel-button').unbind('click').bind('click', function (e) {
        $("#msg-overlay").hide();
        hideSalesAnalysisPopup();
        $(".div-price-overlay-content-container-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");
        $(".div-price-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");
    });
    
    //click on Home button
     $("#div-price-overlay-content-container-sales-analysis").find('.fa-home-icon').unbind('click').bind('click', function (e) {
         //inside chart buttons hilight
        $("#li-segment-level").removeClass("activeSalesLevel");
        $("#li-account-level").addClass("activeSalesLevel");
        generateSalesAnalysis(e, parent, prid, LoaderView);
    });
    
    //click on Back button
    $("#div-price-overlay-content-container-sales-analysis").find('.fa-arrow-left').unbind('click').bind('click', function (e) {
         //inside chart buttons hilight
        $("#li-segment-level").removeClass("activeSalesLevel");
        $("#li-account-level").addClass("activeSalesLevel");
        fnSegmentSalesChart(e, parent, prid, LoaderView,data);
    });
}

//loading account level details in system segment
function fnSystemAccountLevelChart(e,parent,LoaderView,data,requestid,currentsystem,currentSegment,currentsystemname,currentsystemprice) {
    
    var category = [];
    var sale = [];  
    var allData = data[0].gmsalesAnalysisVO;
    var currentsystemprice;
    if (currentsystem == undefined){
        currentsystem = $(e.currentTarget).attr("value");
        currentsystemname = $(e.currentTarget).attr("setnm");
        currentsystemprice = $(e.currentTarget).attr("salesamount");
    }
    var result = [];
    allData.reduce(function (res, value) {
        if (!res[value.partyid] && currentsystem == value.setid) {
            res[value.partyid] = {
                salesamount: 0,
                partyid: value.partyid
            };
            result.push(res[value.partyid])
        }
         if(res[value.partyid] != undefined && currentsystem == value.setid) {
            res[value.partyid].salesamount += parseInt(value.salesamount);
            res[value.partyid].partynm = value.partynm;
         }
        return res;
    }, {});
    
    /*if the data is more than 12 then sum all the sales amount and show in others in Graph*/
        var icnt = 0;
        var dataLength = Object.keys(result).length;
        var otherSale = 0;
        //sort the result based on the sales amount.
        result = result.sort(function (a, b) {
            return parseFloat(b.salesamount) - parseFloat(a.salesamount);
        });
        _.each(result, function (Sdata) {
            //fill the dataset
            icnt += 1;
            if (icnt < 12) {
                category.push({
                    label: Sdata.partynm
                });

                sale.push({
                    value: Sdata.salesamount
                });

            } else {
                otherSale = otherSale + Sdata.salesamount;
                if (icnt == dataLength) {
                    category.push({
                        label: "Others"
                    });

                    sale.push({
                        value: otherSale
                    });
                }

            }
        });
    
     var systemresult = [];
     systemresult.push({"salesamount":currentsystemprice, "setnm" : currentsystemname});
    //for currency formatting in .hbs file
     systemresult = _.each(systemresult, function (data) {
                data.salesamount = data.salesamount.toString();
        });
     var templatedata = JSON.stringify(systemresult);
     templatedata = arrayOf($.parseJSON(templatedata));
    showSalesAnalysisPopup();
    $("#msg-overlay").show();
     
   var templateSalesAnalysis = fnGetTemplate(URL_Price_Template, "gmtPriceSalesReport");
    var templateSalesSystemAccountLevelChart = fnGetTemplate(URL_Price_Template, "gmtPriceSystemAccountLevelChart");
    $("#div-price-overlay-content-container-sales-analysis .modal-body").html(templateSalesAnalysis());
    $("#div-price-overlay-content-container-sales-analysis .modal-body").find(".div-sales-account-level").html(templateSalesSystemAccountLevelChart(templatedata));
    var titleRef = "Sales Analysis : " + requestid;
     $(".titleRef").html(titleRef);
    
     var orgName ,acCurrency,last12monthsales,genDate,partynm;
    if(data[0].gmPricingSalesHeaderVO.last12monthsales != undefined){
         orgName = data[0].gmPricingSalesHeaderVO.orgname;
         acCurrency = data[0].gmPricingSalesHeaderVO.accurrency;
         last12monthsales = data[0].gmPricingSalesHeaderVO.last12monthsales;
         last12monthsales = acCurrency +" "+ formatCurrency(last12monthsales);
         genDate = strToDate(data[0].gmPricingSalesHeaderVO.gendate);
         partynm = data[0].gmPricingSalesHeaderVO.partynm;
    }else{
         orgName = data[0].gmPricingSalesHeaderVO[0].orgname;
         acCurrency = data[0].gmPricingSalesHeaderVO[0].accurrency;
         last12monthsales = data[0].gmPricingSalesHeaderVO[0].last12monthsales;
         last12monthsales = acCurrency +" "+ formatCurrency(last12monthsales);
         genDate = strToDate(data[0].gmPricingSalesHeaderVO[0].gendate);
         partynm = data[0].gmPricingSalesHeaderVO[0].partynm;
    }
    
    $(".orgName").html(orgName);
    $(".acCurrency").html(acCurrency);
    $(".genDate").html(genDate);
    $(".partynm").html(partynm);
    $(".gtotsales").html(last12monthsales);
 
     var totalsales;
    totalsales = "Sales" + " " + acCurrency;
    $(".li-total-sales").html(totalsales);
    
    //generate the chart
     var SalesAccountLevelChart = new FusionCharts({
        type: 'msbar3d',
        renderAt: 'account-level-chart-container',
        width: '700',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "subCaption": "",
                "yAxisname": "",
                "numberPrefix": acCurrency,
                "paletteColors": "#0075c2,#1aaf5d",
                "bgColor": "#ffffff",
                "legendBorderAlpha": "0",
                "legendBgAlpha": "0",
                "legendShadow": "0",
                "placevaluesInside": "0",
                "valueFontColor": "#00000",
                "valueFontBold": "1",
                "valueFontSize": "11",
                "baseFont": "Verdana,Helvetica Neue,Arial",
                "baseFontSize": "11",
                "alignCaptionWithCanvas": "1",
                "showHoverEffect":"1",
                "canvasBgColor": "#ffffff",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "divlineColor": "#999999",
                "divLineIsDashed": "1",
                "divLineDashLen": "1",
                "divLineGapLen": "1",
                "showAlternateHGridColor": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                 "scrollColor": "#3a4660",
                "scrollHeight": "10",
                "scrollPadding": "5",
                "defaultAnimation":'0' 
            },
             "categories": [
                {
                    "category": category
                }
            ],
            "dataset": [
                {
                     "seriesname": "Accounts",
                     "data" : sale
                }, 
            ]
        }
    }).render();
    $("#li-by-account-level").addClass("activeSalesLevel");
    
        //Sales analysys close
    $("#div-price-overlay-content-container-sales-analysis").find('.li-threshold-cancel-button').unbind('click').bind('click', function (e) {
        $("#msg-overlay").hide();
        hideSalesAnalysisPopup();
        $(".div-price-overlay-content-container-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");
        $(".div-price-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");

    });  
    
      //Click on by rep account in system level
    $("#div-price-overlay-content-container-sales-analysis").find('#li-by-rep-account-level').unbind('click').bind('click', function (e) {
         //inside chart buttons hilight
        $("#li-by-rep-account-level").addClass("activeSalesLevel");
        $("#li-by-account-level").removeClass("activeSalesLevel");
        fnSystemRepAccountLevelChart(e,parent,LoaderView,data,requestid,currentsystem,currentSegment,currentsystemname,currentsystemprice);
    });
    
    //click on Home button
    $("#div-price-overlay-content-container-sales-analysis").find('.fa-home-icon').unbind('click').bind('click', function (e) {
         //inside chart buttons hilight
        $("#li-segment-level").removeClass("activeSalesLevel");
        $("#li-account-level").addClass("activeSalesLevel");
        generateSalesAnalysis(e, parent, requestid, LoaderView);
    });
     //click on Back button
    $("#div-price-overlay-content-container-sales-analysis").find('.fa-arrow-left').unbind('click').bind('click', function (e) {
         //inside chart buttons hilight
        $("#li-segment-level").removeClass("activeSalesLevel");
        $("#li-account-level").addClass("activeSalesLevel");
        fnSalesSystemLevelChart(e, parent, requestid, LoaderView,data,currentSegment);
    });
      
}


//loading rep account level details in system segment
function fnSystemRepAccountLevelChart(e,parent,LoaderView,data,requestid,currentsystem,currentSegment,currentsystemname,currentsystemprice) {
    
    var category = [];
    var sale = [];
    var allData = data[0].gmsalesAnalysisVO;

    var result = [];
    allData.reduce(function (res, value) {
        if (!res[value.accountid] && currentsystem == value.setid) {
            res[value.accountid] = {
                salesamount: 0,
                accountid: value.accountid
            };
            result.push(res[value.accountid])
        }
        if(res[value.accountid] != undefined && currentsystem == value.setid) {
            res[value.accountid].salesamount += parseInt(value.salesamount);
            res[value.accountid].accountnm = value.accountnm;
        }
        return res;
    }, {});
    
    /*if the data is more than 12 then sum all the sales amount and show in others in Graph*/
        var icnt = 0;
        var dataLength = Object.keys(result).length;
        var otherSale = 0;
        //sort the result based on the sales amount.
        result = result.sort(function (a, b) {
            return parseFloat(b.salesamount) - parseFloat(a.salesamount);
        });
        _.each(result, function (Sdata) {
            //fill the dataset
            icnt += 1;
            if (icnt < 12) {
                category.push({
                    label: Sdata.accountnm
                });

                sale.push({
                    value: Sdata.salesamount
                });

            } else {
                otherSale = otherSale + Sdata.salesamount;
                if (icnt == dataLength) {
                    category.push({
                        label: "Others"
                    });

                    sale.push({
                        value: otherSale
                    });
                }

            }
        });
     var systemresult = [];
     systemresult.push({"salesamount":currentsystemprice, "setnm" : currentsystemname});
     //for currency formatting in .hbs file
     systemresult = _.each(systemresult, function (data) {
                data.salesamount = data.salesamount.toString();
        });
     var templatedata = JSON.stringify(systemresult);
     templatedata = arrayOf($.parseJSON(templatedata));
    showSalesAnalysisPopup();
    $("#msg-overlay").show();
   
   var templateSalesAnalysis = fnGetTemplate(URL_Price_Template, "gmtPriceSalesReport");
    var templateSalesRepAccountLevelChart = fnGetTemplate(URL_Price_Template, "gmtPriceSystemRepAccountLevelChart");
    $("#div-price-overlay-content-container-sales-analysis .modal-body").html(templateSalesAnalysis());
    $("#div-price-overlay-content-container-sales-analysis .modal-body").find(".div-sales-rep-account-level").html(templateSalesRepAccountLevelChart(templatedata));
    var titleRef = "Sales Analysis : " + requestid;
     $(".titleRef").html(titleRef);
    var orgName ,acCurrency,last12monthsales,genDate,partynm;
    if(data[0].gmPricingSalesHeaderVO.last12monthsales != undefined){
         orgName = data[0].gmPricingSalesHeaderVO.orgname;
         acCurrency = data[0].gmPricingSalesHeaderVO.accurrency;
         last12monthsales = data[0].gmPricingSalesHeaderVO.last12monthsales;
         last12monthsales = acCurrency +" "+ formatCurrency(last12monthsales);
         genDate = strToDate(data[0].gmPricingSalesHeaderVO.gendate);
         partynm = data[0].gmPricingSalesHeaderVO.partynm;
    }else{
         orgName = data[0].gmPricingSalesHeaderVO[0].orgname;
         acCurrency = data[0].gmPricingSalesHeaderVO[0].accurrency;
         last12monthsales = data[0].gmPricingSalesHeaderVO[0].last12monthsales;
         last12monthsales = acCurrency +" "+ formatCurrency(last12monthsales);
         genDate = strToDate(data[0].gmPricingSalesHeaderVO[0].gendate);
         partynm = data[0].gmPricingSalesHeaderVO[0].partynm;
    }
    $(".orgName").html(orgName);
    $(".acCurrency").html(acCurrency);
    $(".genDate").html(genDate);
    $(".partynm").html(partynm);
    $(".gtotsales").html(last12monthsales);
    
     var totalsales;
    totalsales = "Sales" + " " + acCurrency;
    $(".li-total-sales").html(totalsales);
    
    //generate the chart
     var SalesAccountLevelChart = new FusionCharts({
        type: 'msbar3d',
        renderAt: 'rep-acc-level-chart-container',
        width: '700',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "subCaption": "",
                "yAxisname": "",
                "numberPrefix": acCurrency,
                "paletteColors": "#0075c2,#1aaf5d",
                "bgColor": "#ffffff",
                "legendBorderAlpha": "0",
                "legendBgAlpha": "0",
                "legendShadow": "0",
                "placevaluesInside": "0",
                "valueFontColor": "#00000",
                "valueFontBold": "1",
                "valueFontSize": "11",
                "baseFont": "Verdana,Helvetica Neue,Arial",
                "baseFontSize": "11",
                "alignCaptionWithCanvas": "1",
                "showHoverEffect":"1",
                "canvasBgColor": "#ffffff",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "divlineColor": "#999999",
                "divLineIsDashed": "1",
                "divLineDashLen": "1",
                "divLineGapLen": "1",
                "showAlternateHGridColor": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                 "scrollColor": "#3a4660",
                "scrollHeight": "10",
                "scrollPadding": "5",
                "defaultAnimation":'0'
            },            
             "categories": [
                {
                    "category": category
                }
            ],          
            "dataset": [
                {
                     "seriesname": "RepAccounts",
                     "data" : sale
                }, 
            ]
        }
    }).render();
    $("#li-by-rep-account-level2").addClass("activeSalesLevel");
    
      //Click on account in system level
    $("#div-price-overlay-content-container-sales-analysis").find('#li-by-account-level2').unbind('click').bind('click', function (e) {
         //inside chart buttons hilight
        $("#li-by-account-level2").addClass("activeSalesLevel");
        $("#li-by-rep-account-level2").removeClass("activeSalesLevel");
        fnSystemAccountLevelChart(e,parent,LoaderView,data,requestid,currentsystem,currentSegment,currentsystemname,currentsystemprice);
    });

        //Sales analysys close
    $("#div-price-overlay-content-container-sales-analysis").find('.li-threshold-cancel-button').unbind('click').bind('click', function (e) {
        $("#msg-overlay").hide();
        hideSalesAnalysisPopup();
        $(".div-price-overlay-content-container-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");
        $(".div-price-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");

    });  
    
    //click on Home button
    $("#div-price-overlay-content-container-sales-analysis").find('.fa-home-icon').unbind('click').bind('click', function (e) {
         //inside chart buttons hilight
        $("#li-segment-level").removeClass("activeSalesLevel");
        $("#li-account-level").addClass("activeSalesLevel");
       generateSalesAnalysis(e, parent, requestid, LoaderView);
    });
    
    //click on Back button
    $("#div-price-overlay-content-container-sales-analysis").find('.fa-arrow-left').unbind('click').bind('click', function (e) {
         //inside chart buttons hilight
        $("#li-segment-level").removeClass("activeSalesLevel");
        $("#li-account-level").addClass("activeSalesLevel");
        fnSalesSystemLevelChart(e, parent, requestid, LoaderView,data,currentSegment);
    });
}
