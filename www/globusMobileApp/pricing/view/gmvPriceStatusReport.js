/**********************************************************************************
 * File:        GmvPriceMain.js
 * Description: Price Initiate Screen
 * Version:     1.0
 * Author:     	jreddy
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone', 'handlebars',
        'global', 'device', 'notification', 'dropdownview', 'searchview', 'gmtTemplate', 'loaderview', 'gmvCRMSelectPopup', 'commonutil'
        ],

    function ($, _, Backbone, Handlebars,
        Global, Device, Notification, DropDownView, SearchView, GMTTemplate, LoaderView, GMVCRMSelectPopup, Commonutil
    ) {

        'use strict';
        var tabid;
        var PriceStatusReport = Backbone.View.extend({

            events: {
                "click #btn-main-save": "saveOrUpdatePricingDetails",
                "click #li-do-report-from-date label, #div-price-info-from-date label, #div-price-info-from-date i,#div-price-info-to-date label, #div-price-info-to-date i": "showDatepicker",
                "click #div-price-report-load-btn": "loadPricereport",
                "click #btn-category-search": "loadPricereport",
                "change #txt-from-date ,#txt-to-date": "getReportDate",
                "click #div-reason-submit-id": "voidPriceRequest",
                "click  .btn-listby": "openPopup",
                "click .sort-list": "sortList"
            },

            /* Load the templates */
            templatePricingReportData: fnGetTemplate(URL_Price_Template, "gmtPricingReportData"),
            templatePriceReport: fnGetTemplate(URL_Price_Template, "gmtPriceReport"),
            templateCancelPriceRequest: fnGetTemplate(URL_Common_Template, "gmtCommonCancel"),
            template: fnGetTemplate(URL_Common_Template, "gmtSpinner"),
            template_selected_items: fnGetTemplate(URL_Common_Template, "gmtPopupSelectedItem"),


            initialize: function (options) {
                this.el = options.el;
                this.voidsuccess = false;
                this.render(true);
                GM.Global.Pricing = {};
                GM.Global.Pricing.FilterID = {};
                GM.Global.Pricing.FilterName = {};

                this.tempJSON = {};
            },

            // Render the initial contents when the Catalog View is instantiated
            render: function (options) {
                console.log("inside pricing status report render");
                var that = this;
                this.fnGetRuleStatusValues(function (status){});
                this.$el.html(this.templatePriceReport());
//                this.$("#btn-category-search").hide();
                this.filterID = "";
                this.getCodeName();
                this.getPriceStatusDrp();
                var todaydate = new Date();
                var fdate = this.fnGetFormatDate(todaydate);
                this.$("#label-price-from-info-date").html();
                this.$("#label-price-to-info-date").html();
                this.statusTypeId = "0";
                this.loadDefaultReport();
                this.getReasonDrp();
                this.initFilter = false;
                return this;
            },

            getPriceStatusDrp: function () {

                var that = this;
                var defaultid;
                var input = {
                    "codeGrp": "PRQSTS"
                };
                fnGetCodeLookUpValues(input, function (drpDwn) {
                    drpDwn.push({
                        "ID": 0,
                        "Name": "All"
                    });
                    that.statusdropdownview = new DropDownView({
                        el: $("#div-pricing-status"),
                        data: drpDwn,
                        DefaultID: 0
                    });
                    that.$("#div-pricing-status").bind('onchange', function (e, selectedId, SelectedName) {
                        that.statusTypeId = selectedId;
                        that.statusTypeName = SelectedName;
                    });
                });

            },

            fnGetFormatDate: function (selectedDate) {
                var fdate = new Date(selectedDate);
                var smm = ("0" + (fdate.getMonth() + 1)).slice(-2),
                    sdd = ("0" + fdate.getDate()).slice(-2),
                    syy = fdate.getFullYear();
                syy = syy.toString();
                var formatDate = smm + "/" + sdd + "/" + syy;
                return formatDate;
            },

            showDatepicker: function (event) {
                var elem = event.currentTarget;
                var parent = $(elem).parent().attr("id");
                var dptext = $("#" + parent + " input").attr("id");
                $("#" + dptext).datepicker({
                    maxDate: 0,
                    duration: 'fast'
                });
                var visible = $('#ui-datepicker-div').is(':visible');
                $("#" + dptext).datepicker(visible ? 'hide' : 'show');

                event.stopPropagation();
                $(window).click(function (event) {
                    if (document.getElementById("ui-datepicker-div").style.display != "none") {

                        var elem = event.target;
                        if (!($(elem).hasClass("ui-datepicker-prev") || $(elem).hasClass("ui-datepicker-next") || $(elem).parent().hasClass("ui-datepicker-prev") || $(elem).parent().hasClass("ui-datepicker-next"))) {
                            var visible = $('#ui-datepicker-div').is(':visible');
                            $("#" + dptext).datepicker(visible ? 'hide' : 'show');
                        }
                    }
                });
            },

            getReportDate: function (event) {
                var elem = event.currentTarget;
                this.$(elem).parent().find("label").html($(elem).val());
            },



            loadDefaultReport: function (event) {
                var that = this;
                var fromdate, todate;
                fromdate = this.$("#label-price-from-info-date").text();
                todate = this.$("#label-price-to-info-date").text();
                var acid = localStorage.getItem("acid");
                var deptid = localStorage.getItem("deptid");
                var userID = localStorage.getItem("userID");

                var input = {
                    "status": GM.Global.FilterIds,
                    "fromdate": fromdate,
                    "todate": todate,
                    "cmpid": localStorage.getItem("cmpid"), //op
                    "token": localStorage.getItem("token"),
                    "acid": acid,
                    "deptid": deptid,
                    "userid": userID
                };
                this.loaderview = new LoaderView({
                    text: "Loading Reports"
                });
		
	//added for PC-902
		$(".div-price-reports").html("<div id='price-report-list'></div>");
                fnGetWebServiceData("pricingRequest/pricingReportScreen", "gmPricingReportVO", input, function (i, data) {
                    that.loaderview.close();
                    that.loadPriceGridReport(data);
                });
            },

            loadPricereport: function (event) {

                var fromdate, todate, status;
                var statuslist;
                fromdate = this.$("#label-price-from-info-date").text();
                todate = this.$("#label-price-to-info-date").text();

                if (this.initFilter)
                    statuslist = GM.Global.Pricing.FilterID.pricingstatus;
                else
                    statuslist = $.parseJSON(GM.Global.Pricing.FilterID).pricingstatus;
                console.log(statuslist)
                $(".div-price-report").removeClass("hide");
                this.showPriceReport(fromdate, todate, statuslist);
            },

            getCodeName: function () {
                var that = this;

                this.filterID = GM.Global.FilterIds; //"52186,52187,52122,52123";
                if (this.filterID != "") {
                    var input = {
                        "token": localStorage.getItem("token"),
                        "codeid": this.filterID
                    };
                    if (GM.Global.Device) {
                        fnGetCodeNMByID(this.filterID, function (data) {
                            data = arrayOf(data);
                            that.processData(data);
                        })
                    } else {
                        fnGetWebServerData("search/codelookup", "gmCodeLookUpVO", input, function (data) {
                            data = arrayOf(data);
                            that.processData(data);
                        });
                    }
                }
            },

            popUpTemplate: function () {
                var that = this;
                var tempArray = new Array();
                $.each(that.tempJSON, function (key, val) {
                    if (val != "") {
                        tempArray.push({
                            "title": key,
                            "value": val
                        });
                    }
                });
                var criteriaValues = $.parseJSON(JSON.stringify(tempArray));
                if (criteriaValues != "")
                    $(".list-category-item").html(that.template_selected_items(criteriaValues));
                $(".div-text-selected-crt").addClass("gmFont" + this.module);
                this.initFilter = true;
                if (this.filterID != undefined) {
                    this.$("#btn-category-search").show();
                    GM.Global.Pricing.FilterID = {
                        "pricingstatus": GM.Global.FilterIds
                    };
                    this.loadPricereport()
                }

            },

            processData: function (data) {
                var that = this;
                var pricePopup = new Array();
                pricePopup.push(this.filterID);
                if (pricePopup != "") {
                    var statusResult = _.filter(data, function (data) {
                        return data.codegrp == "PRQSTS";
                    });
                    var statusList = _.pluck(arrayOf(statusResult), "codenm");
                    var statusID = _.pluck(arrayOf(statusResult), "codeid");
                    that.tempJSON["Status"] = statusList.toString().replace(/,/g, " / ");
                    var countVal = statusResult.length;
                    that.defaultVals = statusID;
                    $(".div-popup-status").attr("data-values", that.defaultVals);
                    $(".div-popup-status").find("span").text("(" + countVal + ")");
                    $(".div-popup-status").css("background", "#FDF5E6");
                }
                that.popUpTemplate();
            },

            //Sort data based on columns
            sortList: function (e) {
                var that = this;
                var data = GM.Global.ReportData;
                var name = $(e.currentTarget).attr("name");
                if (data.length > 0) {
                    data = _.each(data, function (Sdata) {
                        Sdata.Sortprid = Sdata.requestid.replace("PR-", "");
                    })
                    $(e.currentTarget).toggleClass("sort");
                    if ($(e.currentTarget).hasClass("sort")) {
                        if (name == "Sortprid") {
                            data = data.sort(function (a, b) {
                                return a[name] - b[name];
                            });
                            GM.Global.ReportData = data;
                        } else {
                            data = _.sortBy(data, name);
                            GM.Global.ReportData = data;
                        }
                    } else
                        data = GM.Global.ReportData.reverse();
                    var jsnData = $.parseJSON(JSON.stringify(data));
                    $(".div-price-reports").html(that.templatePricingReportData(jsnData)).addClass("div-valid").removeClass("gmPricingTextAll div-inval");
                } else {
                    $(".div-price-reports").html("No data found!").removeClass("div-valid").addClass("gmPricingTextAll div-inval");
                }
            },

            showPriceReport: function (fromdate, todate, statuslist) {
                console.log(statuslist)
                var that = this;
                var fdate = new Date(fromdate);
                var tdate = new Date(todate);
                var diffDays = tdate.getTime() - fdate.getTime();
                if (diffDays < 0) {
                    //fnShowStatus("To date Should be less than From date ");
                    showError("<B>To</B> date should be less than <B>From</B> date");
                } else {
                    /*if(this.statusTypeId == "" )
                      //  this.statusTypeId = "52120";*/
                    if ((navigator.onLine) && (!intOfflineMode)) {

                        if ($("#txt-billing-account").val() == "")
                            this.accid = "";
                        if (this.daydiff(this.parseDate(fromdate), this.parseDate(todate)) > 60) {
                            //fnShowStatus("Records Exceeds More Than 60 days old, Please select within 60 days range ");
                            showError("Records Exceeds More Than 60 days old, Please select within 60 days range");
                            return;
                        }
                        var acid = localStorage.getItem("acid");
                        var deptid = localStorage.getItem("deptid");
                        var userID = localStorage.getItem("userID");

                        if (statuslist.indexOf("52190") > -1) {
                            if (fromdate == "") {
                                showError("From Date should be mandatory  for Approved Implemented status")
                                return;
                            }
                            if (todate == "") {
                                showError("To Date should be mandatory  for Approved Implemented status")
                                return;
                            }
                        }
                        var input = {
                            "status": this.statusTypeId,
                            "fromdate": fromdate,
                            "todate": todate,
                            "cmpid": localStorage.getItem("cmpid"),
                            "cmptzone": localStorage.getItem("cmptzone"),
                            "cmpdfmt": localStorage.getItem("cmpdfmt"),
                            "plantid": localStorage.getItem("plantid"),
                            "token": localStorage.getItem("token"),
                            "acid": acid,
                            "deptid": deptid,
                            "userid": userID,
                            "status": statuslist
                        };
                        var that = this;
                        this.loaderview = new LoaderView({
                            text: "Loading Reports"
                        });
                        
			//added for PC-902
                        $(".div-price-reports").html("<div id='price-report-list'></div>");
                        fnGetWebServiceData("pricingRequest/pricingReportScreen", "gmPricingReportVO", input, function (i, data) {
                            that.loaderview.close();
                            that.loadPriceGridReport(data);
                        });
                    }
                }
            },
            
	    //function added for PC-902
            loadPriceGridReport: function (data) {
                var that = this;
                console.log(data);
                if (data != null && data.length>0) {
                    $('.div-price-report-header-list').addClass("hide");
                    $('.div-price-report').removeClass("hide");
                    var rows = [],
                    i = 0;
                    _.each(data, function (griddt) {
                    rows[i] = {};
                    rows[i].id = i;
                    rows[i].data = [];
                    rows[i].data[0] = "<a class='gmReportLnk' title='" + griddt.requestid + "' href='#price/pricingSubmittedRequests/" + griddt.requestid + "'>" + griddt.requestid + "</a>";
                    rows[i].data[1] = griddt.groupaccname;
                    rows[i].data[2] = griddt.regname;
                    rows[i].data[3] = griddt.initiatedby;
                    rows[i].data[4] = griddt.initiatedon;
                    rows[i].data[5] = griddt.statusupd;
                    if (griddt.statusid == 52186) {
                        rows[i].data[6] = griddt.status + "<div id='fa-times-circle' class='fa fa-pricereport-circle fa-times-circle fa-2em font-red fa-price-circle div-red-close' reqid ='" + griddt.requestid + "'></div>";
                    } else {
                        rows[i].data[6] = griddt.status + "<a class='gmReportLnk' href='#price/approval/" + griddt.requestid + "'><div id='fa-times-tick' class='fa fa-check-circle-o font-green'></div></a>";
                    }
                        i++;
                });
                    var dataHost = {};
                    dataHost.rows = rows;
                    var setInitWidths = "80,198,143,149,90,112,208";
                    var setColAlign = "left,left,left,left,center,left,left";
                    var setColTypes = "ro,ro,ro,ro,ro,ro,ro";
                    var setColSorting = "str,str,str,str,date,date,str";
                    var setHeader = ["Request#", "Account/Group", "Region", "Initiated By", "Initiated On", "Submitted Date", "Status"];
                    var setFilter = ["#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter"];
                    var enableTooltips = "false,false,false,false,false,false,false";
                    var footerArry = [];
                    var gridHeight = "";
                    var footerStyles = [];

                    that.gridObj = loadDHTMLXGrid('price-report-list', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, setFilter, footerArry, footerStyles);
                                
                    $("#price-report-list").addClass("div-valid").removeClass("gmPricingTextAll div-inval");
                                
                    $(".fa-pricereport-circle").unbind("click").bind("click", function (event) {
                        that.cancelPriceRequest(event);
                    });

                } else {
                	$('.div-price-report-header-list').addClass("hide");
                    $("#price-report-list").html("No data found!").removeClass("div-valid").addClass("gmPricingTextAll div-inval");
                    GM.Global.ReportData = "";
                }
            },

            daydiff: function (first, second) {
                return (second - first) / (1000 * 60 * 60 * 24)
            },

            parseDate: function (str) {
                var mdy = str.split('/')
                return new Date(mdy[2], mdy[0] - 1, mdy[1]);
            },

            // Open Single / Multi Select box        
            openPopup: function (event) {
                var that = this;
                that.fnGetRuleStatusValues(function (status) {
                var fnName = "";
                if ($(event.currentTarget).attr("data-title") == "Product Used")
                    fnName = fnGetSystem;
                else
                    fnName = fnGetCodeNMByGRP;

//                if (that.initFilter) {
                    GM.Global.Pricing.FilterID = {
                        "pricingstatus": GM.Global.FilterIds
                    };
                    that.initFilter = false;
//                }
                that.systemSelectOptions = {
                    'title': $(event.currentTarget).attr("data-title"),
                    'storagekey': $(event.currentTarget).attr("data-storagekey"),
                    'webservice': $(event.currentTarget).attr("data-webservice"),
                    'resultVO': $(event.currentTarget).attr("data-resultVO"),
                    'IDParameter': $(event.currentTarget).attr("data-IDParameter"),
                    'NameParameter': $(event.currentTarget).attr("data-NameParameter"),
                    'inputParameters': $(event.currentTarget).attr("data-inputParameters"),
                    'codegrp': $(event.currentTarget).attr("data-codegrp"),
                    'module': that.module,
                    'event': event,
                    'parent': that,
                    'moduleLS': GM.Global.Pricing.FilterID,
                    'moduleCLS': GM.Global.Pricing.FilterName,
                    'fnName': fnName,
                    'callback': function (ID, Name) {
                        GM.Global.Pricing.FilterID = ID;
                        //                    GM.Global.Activity = $.parseJSON(ID)
                        GM.Global.Pricing.FilterName = Name;
                        console.log(ID);
                        console.log(Name)
                    }
                };
                console.log(GM.Global.Pricing.FilterID);
                console.log(GM.Global.Pricing.FilterName);

                that.popupSelectView = new GMVCRMSelectPopup(that.systemSelectOptions);
                });
            },

            cancelPriceRequest: function (e) {
                var that = this;
                var requestId = $(e.currentTarget).attr("reqid"); 
                console.log(requestId);
                var statusId = $(e.currentTarget).attr("name");
                showCancelPriceRequestPopup();
                $("#msg-overlay").show();
                $("#div-price-overlay-content-container8 .modal-body").html(that.templateCancelPriceRequest());
                $("#div-trans-id").html(requestId);
                that.getReasonDrp();
                // X icon click
                $("#div-price-overlay-content-container8").find('.modal-body .li-threshold-cancel-button').unbind('click').bind('click', function () {
                    hideCancelPriceRequestPopup();
                    $("#msg-overlay").hide();
                    if (that.voidsuccess) {
                        that.voidsuccess = false;
                        $("#btn-category-search").trigger("click");
                    }
                });
                //Cancel button click
                $("#div-price-overlay-content-container8").find('.modal-body #div-reason-cancel-id').unbind('click').bind('click', function () {
                    hideCancelPriceRequestPopup();
                    $("#msg-overlay").hide();
                });
                // Submit button click
                $("#div-price-overlay-content-container8").find('.modal-body #div-reason-submit-id').unbind('click').bind('click', function () {

                    var requestId = $("#div-trans-id").html();
                    var cancelReason = that.consultantId;
                    var cancelComment = $("#li-price-req-id textarea").val();
                    if (cancelComment == "") {
                        showError("<B>Comments</B> cannot be empty to perform this action");
                        return;
                    }
                    if (cancelReason == undefined || cancelReason == "0") {
                        showError("Please select at least one value from the <B>Reason</B> to perform this action ");
                        return;
                    }
                    var input = {
                        "token": localStorage.getItem("token"),
                        "userid": localStorage.getItem("userID"),
                        "cancelreasonid": cancelReason,
                        "comments": cancelComment,
                        "txnids": requestId
                    };
                    fnGetWebServerData("pricingRequest/voidPriceRequest/", undefined, input, function (data) {
                        if (data != null) {
                            $(".div-cancel-screen").html("<li class='li-cancel-report-cls' id='li-void-success'>Void Pricing Request Transaction for <u><i>" + data.txnids + "</i></u> performed</li>")
                            that.voidsuccess = true;
                        }
                    });
                });
            },

            getReasonDrp: function () {
                var that = this;
                var defaultid;
                var input = {
                    "codeGrp": "VDPRRS"
                };
                fnGetCodeLookUpValues(input, function (drpDwn) {
                    drpDwn.push({
                        "ID": 0,
                        "Name": "Choose One"
                    });
                    that.typedropdownview = new DropDownView({
                        el: $("#div-reason-drpdwn-id"),
                        data: drpDwn,
                        DefaultID: 0
                    });
                    $("#div-reason-drpdwn-id").bind('onchange', function (e, selectedId, SelectedName) {
                        that.consultantId = selectedId;
                        that.consultantName = SelectedName;
                    });
                });
            },

            /*method to get default Price  Request Status Valeus*/
            fnGetRuleStatusValues: function (callback) {
                if (localStorage.getItem("acid") <= '6' && localStorage.getItem("Dept") != "2026") {
                    var input = {
                        ruleid: "PRICESTATUS",
                        rulegrpid: "SALESDEFAULTSTATUS",
                        token: localStorage.getItem("token"),
                        cmpid: localStorage.getItem("cmpid")
                    };
                } else {
                    var input = {
                        ruleid: "PRICESTATUS",
                        rulegrpid: "DEFAULTSTATUS",
                        token: localStorage.getItem("token"),
                        cmpid: localStorage.getItem("cmpid")
                    };
                }
                fnGetWebServerData("pricingRequest/fetchDefaultStatus/", undefined, input, function (data) {
                    if (data != null) {

                        //                        else
                        GM.Global.FilterIds = data.rulevalue;
                        console.log(GM.Global.FilterIds);
                        callback("true");
                    }
                });
            }

        });

        return PriceStatusReport;
    });
