/**********************************************************************************
 * File:        gmmSystem.js
 * Description: Price Initiate Screen
 * Version:     1.0
 * Author:     	treddy
 **********************************************************************************/
define([
    'underscore',
    'backbone',
],

function (_, Backbone) {

	'use strict';

	var SystemModel = Backbone.Model.extend({

		// Default values of the model
		defaults: {
			systemName:"",
			systemId:"",
            changetype:"",
			changevalue:"",
            changetypename:"",
            userid:""
		}
	});	

	return SystemModel;
});
