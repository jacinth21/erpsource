/**********************************************************************************
 * File:        gmmAccountPriceRequest.js
 * Description: model to hold the data
 * Version:     1.0
 * Author:     	treddy
 **********************************************************************************/
define([
    'underscore',
    'backbone',
],

    function (_, Backbone) {

        'use strict';

        var AccountPriceRequestModel = Backbone.Model.extend({

            // Default values of the model
            defaults: {
                requestType: "",
                requestStatus: "",
                companyId: "",
                last12MonthsSale: "",
                projected12MonthsSale: "",
                gpoReferenceId: "",
                gpoReferenceDetailId: "",
                salesRepId: "",
                adId: "",
                vpAD: "",
                pricerequestid: "",
                partyid: "",
                selectedSystems: "",
                createdBy: "",
                userid: "",
                partyname: "",
                typeid: "",
                questioncnt: "",
                statusid: "",
                documentcount: "",
                impactcount: "",
                stropt: ""
            }
        });

        return AccountPriceRequestModel;
    });
