/**********************************************************************************
 * File:        gmcBusinessQuestions.js
 * Description: BusinessQuestions model File
 * Version:     1.0
 * Author:     	Jreddy
 **********************************************************************************/
define([
    'underscore',
    'backbone',
],

function (_, Backbone) {

	'use strict';

	var businessAnswers = Backbone.Model.extend({

		// Default values of the model
		defaults: {
		questionid:"",
        questionanswer:"", 
        userid:"",
        dropdownid:""
		}
	});	

	return businessAnswers;
});