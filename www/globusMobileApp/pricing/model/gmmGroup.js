/**********************************************************************************
 * File:        gmmGroup.js
 * Description: Price Initiate Screen
 * Version:     1.0
 * Author:     	treddy
 **********************************************************************************/
define([
    'underscore',
    'backbone',
],

    function (_, Backbone) {

        'use strict';

        var GroupModel = Backbone.Model.extend({

            // Default values of the model
            defaults: {
                systemid: "",
                systemname: "",
                groupid: "",
                groupname: "",
                quantity: "",
                proposedunitprice: "",
                proposedextensionprice: "",
                tripwireunitprice: "",
                tripwireextensionprice: "",
                currentprice: "",
                constructlevel: "",
                specialitygroup: "",
                group: "",
                constructname: "",
                principalflag: "",
                listprice: "",
                grouptype: "",
                pricetype: "",
                grouptypedesc: "",
                partnum: "",
                partdesc: "",
                changetype: "",
                changevalue: "",
                constructid: "",
                autopop_fl: "",
                gpoprice: "",
                statusname: "",
                statusid: "",
                checkfl: "",
                parentgroupid: "",
                parentgroupname: "",
                reqdtlid: "",
                systemdivisionid: "",
                individualgrpchangevalue: ""
            }
        });

        return GroupModel;
    });
