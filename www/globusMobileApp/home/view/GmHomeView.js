/**********************************************************************************
 * File:        GmHomeView.js
 * Description: View to display the Home Page where the user starts to enter a
 *              particular application
 * Version:     1.0
 * Author:      ejayabalan
 **********************************************************************************/

define([
	'jquery', 'underscore', 'backbone', 'global',
    'device', 'handlebars','notification','filesync',

    // Pre-Compiled Template
    'gmtTemplate'
], 

function ($, _, Backbone, Global, Device, Handlebars, Notification, FileSync, GMTTemplate ) {

	'use strict';
    
    var HomeView = Backbone.View.extend({
    
	    el: "#div-main",

	    events: {
	    	"click .btn-app, .btn-app-phone": "setAppTitle"
	    },

	    template: fnGetTemplate(URL_Home_Template, "GmHome"),

		initialize: function() {
			$("#div-messagebar").hide();
			$(".div-btn-header-1").hide();
			$(".div-btn-header").show();
			$(".div-btn-locale").hide();
			$("#div-app-title").removeClass("gmVisibilityVisible").addClass("gmVisibilityHidden");
			// $("#div-header").addClass("iphone-header-small").removeClass("iphone-header-big");
 			$("body").removeClass("background-2").addClass("background-1 background-image");
             $("#ul-header-right-pic").removeClass("hide");
            $("#ul-header-right-pic-menubar").removeClass("hide");
				$("#btn-sidebar-menu").show();
if($(window).width() > 480){
$("#btn-sidebar-menu").hide();
}
            $(".mobile-menu-top-user-name").removeClass("hide");
	$("#btn-back").hide();
            hideMessages();
            this.render();
		},

		// Render the initial contents when the app starts
		render: function () {
            var that = this;
			var strUserName = window.localStorage.getItem("strShortName");
			this.$el.html(this.template());
             boolSyncing = false;
            //op changes start
//            getLocaleData();
            _.each(localStorage.getItem("moduleaccess").split(','),function(data){
               $("#"+data).addClass("hide"); 
            });
            fnCodelookupCountValidation();
            fnCodelookupExclusionCountValidation();
            fnCodelookupAccessCountValidation();
            fnRulesCountValidation();
            fnPartRFSCountValidation();
            // PC-5704 - Function call for get firebase pushnotification token
            fnGetFirebasePNSToken(function (data){
               deviceToken = data;
                var oldFcmToken = localStorage.getItem("fcmToken");
               if (parseNull(deviceToken) != parseNull(oldFcmToken)) {
                   that.saveFirebaseToken(deviceToken);
                   console.log("fnGetFirebasePNSToken save() called>>>>>>>>>>>>>>"+ deviceToken);
               }
            });
            
          //To get RFS mappingID of company for part search in Product Catalog module
            fnGetRFSCompanyMapID();
            
            if(GM.Global.DoValidation != undefined ){
                showError(GM.Global.DoValidation);
                GM.Global.DoValidation = undefined;
            }
            
            if(GM.Global.GuideFl == "Y" && GM.Global.GuideErr != undefined) {
                showError(GM.Global.GuideErr);
                GM.Global.GuideErr = undefined;
            }
            GM.Global.GuideFl = "N";
			
            $(".btn-app").each(function () {
                var btnID = $(this).attr("id");
                var btnData = '#' + btnID;
                if (localStorage.getItem(btnID) == null)
                    $(btnData).addClass("hide");
                else
                    $(btnData).removeClass("hide");
            });
				
            if(GM.Global.Device){
               if ($(window).width() <= 480) {
                                   $('.CRMoffline').addClass('hide');
                                }
                                else{
                                   $(".CRMoffline").removeClass("hide"); 
                                }
                $(".CRMonline").addClass("hide");
            }
            else{
                $(".CRMoffline").addClass("hide");
                $(".CRMonline").removeClass("hide");
            }
//			showMsgView("002",strUserName);
            if($(window).width() <= 480){
            	 
//                $("#div-app-title").addClass("gmMediaVisibilityH").removeClass("gmMediaVisibilityV");
                $(".div-moble-menu").addClass("gmMediaVisibilityH").removeClass("gmMediaVisibilityV");
                $("#btn-home").removeClass("hide evlShow");
                $("#btn-back").removeClass("evlShow"); 
                
           }
            $("#iPad-fname").html(localStorage.getItem("fName"));
            $("#iPad-lname").html(localStorage.getItem("lName"));
             $(".li-ipad-user-pic").removeClass("hide");
            $(".ul-language-name").removeClass("hide");
          
            $("#ipad-menu-bar").removeClass("hide");
             if ($(window).width() < 480) {
                    $("#li-update-count").addClass("hide");
               }
//             $("#li-LocalDB").removeClass("hide");
            $("#li-LocalDB").remove();
            
            $("#div-footer").removeClass("hide");
            $("#ul-header-right-pic").removeClass("hide");
            $("#ul-header-right-pic-menubar").removeClass("hide");
            $(".mobile-menu-top-user-name").html(localStorage.getItem("fName")+" "+localStorage.getItem("lName")); 
            var partyID  = window.localStorage.getItem("partyID");
            $("#ul-header-right-pic").find("img").attr("src","");
            $("#ul-header-right-pic-menubar").find("img").attr("src","");
            $("#mobile-menu-top-user").find("img").attr("src","");
            if(GM.Global.Device){
                fnGetCRMDocumentDetail(partyID, 103112, function(fileData){
                  if (fileData.length){
                    fileData = fileData[0];
                    GM.Global.UserPicID = fileData.fileid;
                    $("#ul-header-right-pic").find("img").removeAttr("onerror").css("display","inline-block");
                      $("#ul-header-right-pic-menubar").find("img").removeAttr("onerror").css("display","inline-block");
                    $("#mobile-menu-top-user").find("img").removeAttr("onerror").css("display","inline-block");
                    if(GM.Global.Browser){
                        $("#mobile-menu-top-user").find("img").attr("src",  URL_CRMFileServer + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());

                        $("#ul-header-right-pic").find("img").attr("src",  URL_CRMFileServer + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());
    //                              $("#mobile-menu-top-user-name").html(localStorage.getItem("fname")+" "+localStorage.getItem("lname"));
                        $("#ul-header-right-pic-menubar").find("img").attr("src",  URL_CRMFileServer + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());
                        $("#ul-header-right-pic").find("img").removeClass("hide");
                        $("#ul-header-right-pic").removeClass("hide");
                        $("#ul-header-right-pic-menubar").find("img").removeClass("hide");
                        $("#ul-header-right-pic-menubar").removeClass("hide");
                    }
                    else{
                        fnGetLocalFileSystemPath(function(localpath) {
                            $("#mobile-menu-top-user").find("img").attr("src",  localpath + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());

                            $("#ul-header-right-pic").find("img").attr("src",  localpath + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());
                            $("#ul-header-right-pic-menubar").find("img").attr("src",  localpath + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());
                        });
                        $("#ul-header-right-pic").find("img").removeClass("hide");
                        $("#ul-header-right-pic").removeClass("hide");
                         $("#ul-header-right-pic-menubar").find("img").removeClass("hide");
                        $("#ul-header-right-pic-menubar").removeClass("hide");
                    }
                      
                   }
                   else{
                       $("#ul-header-right-pic").find("img").attr("src","");
                       $("#ul-header-right-pic-menubar").find("img").attr("src","");
                       $("#mobile-menu-top-user").find("img").attr("src","");
                   }
                   $("#ul-header-right-pic").find("img").attr("onerror","this.style.display = 'none'");
                    $("#ul-header-right-pic-menubar").find("img").attr("onerror","this.style.display = 'none'");
                   $("#mobile-menu-top-user").find("img").attr("onerror","this.style.display = 'none'");
                });
            }
            else{
                var input = {"token":localStorage.getItem("token"), "refid":partyID, "deletefl": "", "refgroup" : "103112"};
                fnGetWebServerData("file/get", "gmCRMFileUploadVO", input, function(fileData){
                    if(fileData != null && fileData !="")
                        if(fileData.length >1)
                            fileData = fileData[0];
                  if (fileData != null){

                      GM.Global.UserPicID = fileData.fileid;
                        $("#ul-header-right-pic").find("img").removeAttr("onerror").css("display","inline-block");
                       $("#ul-header-right-pic-menubar").find("img").removeAttr("onerror").css("display","inline-block");
                    $("#mobile-menu-top-user").find("img").removeAttr("onerror").css("display","inline-block");
                      $("#mobile-menu-top-user").find("img").attr("src",  URL_CRMFileServer + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());

                      $("#ul-header-right-pic").find("img").attr("src",  URL_CRMFileServer + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());
    //                              $("#mobile-menu-top-user-name").html(localStorage.getItem("fname")+" "+localStorage.getItem("lname"));
                      $("#ul-header-right-pic").find("img").removeClass("hide");
                      $("#ul-header-right-pic").removeClass("hide");
                      
                      $("#ul-header-right-pic-menubar").find("img").attr("src",  URL_CRMFileServer + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());
    //                              $("#mobile-menu-top-user-name").html(localStorage.getItem("fname")+" "+localStorage.getItem("lname"));
                      $("#ul-header-right-pic-menubar").find("img").removeClass("hide");
                      $("#ul-header-right-pic-menubar").removeClass("hide");
                   }
                   else{
                       $("#ul-header-right-pic").find("img").attr("src","");
                       $("#ul-header-right-pic-menubar").find("img").attr("src","");
                       $("#mobile-menu-top-user").find("img").attr("src","");
                   }
                   $("#ul-header-right-pic").find("img").attr("onerror","this.style.display = 'none'");
                    $("#ul-header-right-pic-menubar").find("img").attr("onerror","this.style.display = 'none'");
                   $("#mobile-menu-top-user").find("img").attr("onerror","this.style.display = 'none'");
               }, true);
            }
            
            $("#ul-header-right-pic").find("img").removeClass("hide");
            $("#ul-header-right-pic").removeClass("hide");
             $("#ul-header-right-pic-menubar").find("img").removeClass("hide");
            $("#ul-header-right-pic-menubar").removeClass("hide");
            $('#div-app-title').removeClass('gmVisibilityVisible').addClass('gmVisibilityHidden');
            $("#div-footer").addClass("hide");

            // PC-5704 - Function call for firebase redirect url 
            appLaunchInit(function (callback) {
                console.log("appLaunchInit callback >>>>>>>>" + callback);
               var fcmAppURL = openFCMURL();
               console.log("Router >>>>>>" + fcmAppURL);
                if(parseNull(fcmAppURL) != "") {
                    setTimeout(function(){ 
                    window.location.href = fcmAppURL; }, 1000);
                }
            });

			return this;
            
		},   

		setAppTitle: function(e) {
			e.preventDefault();
			var strAppName = e.currentTarget.name;
                console.log(strAppName);
            
            
                if($(window).width() < 480 && strAppName == "CRM" ){
                    $("#span-app-title").show().html(strAppName);
                    window.location.href = e.currentTarget.href;
                    }
                else{
                    $("#div-app-title").show().html(strAppName).removeClass("gmVisibilityHidden").addClass("gmVisibilityVisible");
				    window.location.href = e.currentTarget.href;
                }
		$("#mySidenav").removeClass("width-nav");
                $(".gmFloatCenter").addClass("sidenav-block");
			
		},

        // This function used for save firebase token and user details on firestore database
        saveFirebaseToken: function (fbtoken) {
            var userId = localStorage.getItem("userID");
            var userName = localStorage.getItem("fName") + " " + localStorage.getItem("lName");
            var userInfo = {
                "C101_USER_ID": userId,
                "C101_USER_NAME": userName,
                "C9160_DEVICE_FCM_TOKEN": fbtoken,
                "C9160_DEVICE_UUID": deviceUUID,
                "C9160_LAST_UPDATED_BY": userId,
                "C9160_LAST_UPDATED_DATE": new Date(),
                "C9160_VOID_FL": ""

            };
            console.log(userInfo);
            if (firedb != undefined) {
                firedb.collection("FT9160_CLOUD_DEVICE_INFO").doc(deviceUUID).set(userInfo)
                    .then(() => {
                        console.log("Firebase PNS token saved successfully on firestore!!!");
                        localStorage.setItem("fcmToken", deviceToken);

                    })
                    .catch((error) => {
                        console.error("Error on saving Firebase PNS token : ", error);
                    });
            }
        },

		close: function() {
			this.unbind();
			this.undelegateEvents();
		}

	});
	return HomeView;

});
