/**********************************************************************************
 * File:        GmSettingsView.js
 * Description: View to display the app settings
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone', 'handlebars', 'i18n!nls/GmLocale',
        'daosetup', 'daoproductcatalog', 'daomarketingcollateral', 'datasync', 'filesync', 'global', 'notification', 'authentication', 'fieldview', 'usermodel', 'jqueryui', 'jqueryuitouch', 'searchview', 'daodo', 'daoinv'
        ],

    function ($, _, Backbone, Handlebars, Locale, DAOSetup, DAOProductCatalog, DAOMarketingCollateral, FileSync, DataSync, Global, Notification, Authentication, FieldView, UserModel, JqueryUI, JqueryUITouch, SearchView, DAODO, DAOInv) {

        'use strict';

        var SettingsView = Backbone.View.extend({

            events: {

				"click #btn-syncall": "syncAll",
                "click #btn-reset": "resetConfirmation",
                "click #reset-button":"resetConfirmation",
                "click #btn-sync": "startSync",
                "click .check": "selectData",
//changes removed for setting Dynamic locale(PMT-29507)
                "click #div-settings-locale": "showLocalePanel",
                "click .div-sync-label": "toggleSync",
                "click .div-common": "selectItems",
                "click #btn-delete": "startDelete",
                "click .div-lastsync i": "showLastSyncDate",
                "click .li-settings-dash-widget-list": "selectWidget",
                "click #btn-dash-reset": "resetSequence",
                "click #div-settings-menu": "showNavigation",
                "click .fa-arrow-circle-down": "goDown",
                "click .fa-arrow-circle-up": "goUp",
                "click #div-setting-account-price-synchronize": "syncAccountPrice",
                "click #div-setting-account-price-delete": "deleteAccountPrice",
                "collide #div-data-sync": "collisonAvoidance",
                "click .sync-count .span-sync-count": "syncDetail",

            },

            template: fnGetTemplate(URL_Settings_Template, "GmSettings"),
            template_data: fnGetTemplate(URL_Settings_Template, "GmDataSyncList"),
            template_file: fnGetTemplate(URL_Settings_Template, "GmFileSyncList"),
            template_locale: fnGetTemplate(URL_Main_Template, "GmLocale"),
            template_reset: fnGetTemplate(URL_Settings_Template, "GmFactoryReset"),
            template_dashboard: fnGetTemplate(URL_Settings_Template, "GmDashboardList"),
            template_column: fnGetTemplate(URL_Settings_Template, "GmDashColumnList"),
            template_account: fnGetTemplate(URL_Settings_Template, "GmAccountPrice"),
            template_accountsynclist: fnGetTemplate(URL_Settings_Template, "GmAccountSyncList"),
            template_crm: fnGetTemplate(URL_Settings_Template, "GmCRMSyncList"),
            template_detailsynclist: fnGetTemplate(URL_Settings_Template, "GmSettingsPopup"),

            initialize: function () {
                $("#div-messagebar").show();
                //		showMsgView("011");
                $("#btn-home").show();
                $("#btn-back").hide().unbind();
                //        $("#div-footer").addClass("hide");
                if (!$("#div-footer").hasClass("hide")) {
                    $("#div-footer").addClass("hide")
                }
                $("#div-item-type").hide();
                $(".div-btn-locale").hide();
                $("#div-app-title").removeClass("gmVisibilityVisible").addClass("gmVisibilityHidden");
//                $("#div-app-title").removeClass("gmMediaVisibilityH").addClass("gmMediaVisibilityV");
                $("#btn-settings").addClass("btn-header-visited");
                this.syncMode = true;
                $("#btn-updates").removeClass("btn-updates-syncmode");
                this.renderCnt = 0;
                this.render();
            },

            selectItems: function (event) {
                event.preventDefault();
                var target = event.currentTarget;
                var parent = document.getElementById(target.id);
                if (this.syncMode) {
                    if (!$(parent).hasClass("checked-items")) {
                        $(".div-common").removeClass("checked-items");
                        $(parent).addClass("checked-items");
                    } else {
                        $(parent).removeClass("checked-items");
                    }
                }
            },

            // Render the initial contents when the app starts
            render: function () {
                this.$el.html(this.template());
                //changes removed for setting Dynamic locale(PMT-29507)
                this.getLocalePanel();
             if ($(window).width() <= 480) {
               var height = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
            this.$('#div-settings').height(height-74);
            }
                securityEventAccess(".div-sync-label", "id", "SETTINGS");
                $("body").removeClass("background-1 background-2 background-file");
                
                // Hide Enviroinment info in Production
	           if (strEnvironment == "Production") {
                this.$("#div-settings-env").addClass("gmVisibilityHidden");
                }         
                this.$("#span-environment").html(strEnvironment);
                this.$("#spn-version").html(version);
                this.$("#div-settings-sync").html(this.template_data());
                securityEventAccess(".li-data-sync", "name", "SYNC_DATA");
                this.$("#btn-delete").hide();
                this.$("#btn-dash-reset").hide();
                if ($(window).width() <= 480) {
                    $("#link-home").removeClass("hide");
                    $("#link-back").addClass("hide");

                    $("#div-app-title").show();
                    $("#span-app-title").html(lblNm("settings"));
                    $(".div-moble-menu").removeClass("gmMediaVisibilityV").addClass("gmMediaVisibilityH");
                    //                $("#div-app-title #btn-home").removeClass("hide");
                    $("#div-settings-menu").addClass("gmMediaDisplayNone");

                    $("#div-header").addClass("background-1");
                } else
                    $("#div-app-title").removeClass("gmVisibilityVisible").addClass("gmVisibilityHidden");
                $("#div-header").addClass("background-1");

                this.count();
                this.lastSync();
                 $("#div-app-title").hide();
                return this;
            },

            collisonAvoidance: function (event) {
                var that = this;
                if (this.collisonCheck == undefined) {
                    this.collisonCheck = window.setInterval(function () {
                        if (!boolAutoUpdateStatus) {
                            window.clearInterval(that.collisonCheck);
                            that.collisonCheck = undefined;
                            that.render();
                        }
                    }, 1000);
                }

            },

            count: function () {
                var that = this;
                fnGetSystemList("url", function (data) {
                    that.$("#span-file-count").html("&nbsp;" + data.length + "&nbsp;");
                    var datafiles = document.getElementsByClassName('li-data-sync');
                    that.$("#span-data-count").html("&nbsp;" + datafiles.length + "&nbsp;");
                });

            },

            lastSync: function() {
                try {
                    fnGetLastSyncDate(function(date) {
                        try {
                            var voname;
                            var totalseq;
                            var value
                            _.each(date, function(date) { //get the success count of data from Database and Local
                                var name = date.SYNCNAME;
                                _.each($(".li-data-sync"), function(d) {

                                    if ($(d).attr("name") == name && name != "") {
                                        if (name == "ACCOUNT/FIELDSALES")
                                            name = "MASTER";
                                        fnGetCount(name, function(data) {
                                            value = data[0].SUCCESS_CNT;
                                            if (value == null)
                                                value = 0;
                                            voname = $(d).find("input").first().val();
                                            totalseq = eval(voname).totalSeq;
                                            $(d).find(".span-sync-count").html(value + "/" + totalseq);
                                            if (value != totalseq)
                                                $(d).find(".sync-count").css("color", "red");
                                            else
                                                $(d).find(".sync-count").css("color", "black");
                                        });

                                    }
                                });
                            });
                            if (date.length == 0) {
                                $(".div-lastsync span").html(lblNm("no_data_sync"));
                                $(".span-sync-count").html("");
                                var data = [];
                                _.each(jsnUpdateCode, function(key, value) {
                                    var json = {
                                        reftype: eval(key).code,
                                        statusid: "103120",
                                        syncgrp: eval(key).syncGrp,
                                        syncseq: eval(key + ".syncSeq").toString()
                                    }
                                    data.push(json)
                                });
                                updateLocalDB("GmProdCatlReqVO", data, function() {
                                    console.log("GmSettingsView :: lastSync() :: local sync table insert success");
                                });
                            } else {
                                for (var i = 0; i < date.length; i++) {
                                    var arrDate = date[i]["LASTSYNC"].split('(');
                                    var strDate = arrDate[0];
                                    strDate = strDate.substring(0, strDate.length - 1);
                                    if (date[i]["SYNCNAME"].indexOf('300') < 0)
                                        var id = "span-" + date[i]["SYNCNAME"].toLowerCase() + "-lastsync";
                                    else
                                        var id = "span-" + date[i]["SYNCNAME"] + "-lastsync";
                                    if (document.getElementById(id) != null)
                                        document.getElementById(id).innerHTML = strDate;
                                }
                            }
                        } catch (err) {
                            showAppError(err, "GmSettingsView:lastsync(1)");
                        }
                    });
                } catch (err) {
                    showAppError(err, "GmSettingsView:lastsync(0)");
                }
            },
			
			//function to sync PMT#50719 
			syncAll: function (e) {
                            var that = this;
                            var event = {};

                                      this.$(".check").each(function () {
                                event.currentTarget = $(this);
                                $(this).removeClass("checked-sync");
                                if ($(this).is(":visible"))
                                    that.selectData(event);
                                          console.log(event);
                            });
                          that.startSync(e);
                        },

            resetConfirmation: function (event) {
            	//if (this.syncMode) {
                                    if ((navigator.onLine) && (!intOfflineMode)) {
                        var that = this;

                        showNativeConfirm(lblNm("msg_factory_reset"), lblNm("msg_warning"), [lblNm("msg_yes"), lblNm("msg_no")], function (index) {
                            if (index == 1) {
                                that.reset(event);
                            }
                        });
                    } else if (intOfflineMode) {
                    	showNativeAlert(lblNm("msg_reset_offline"), lblNm("msg_offline_mode_restriction"));
                    }
            //}
                 },
                

            showLastSyncDate: function (e) {
                e.stopPropagation();
                var parent = $(e.currentTarget).parent();
                var lastsyncdate = this.$(parent).find("span").text();
                navigator.notification.alert(
                    lastsyncdate, // message
                    undefined,
                    lblNm("last_sync"), // title
                    'OK' // buttonLabels
                );
            },


            reset: function (event) {
                var that = this;

                var ws = $(event.currentTarget).attr("value");
                resetServer("", function () {
                    that.lastSync();
                    that.count();
                    localStorage.removeItem("V700SYNC");
                    //Temporary fix by gopi
                    //			fnRemoveFiles(function() {
                    $("#msg-overlay").fadeOut("fast");
                    $("#div-confirm-reset").fadeOut("fast");
                    showMsgView("028");
                    //			});
                });

            },

            resetSequence: function () {
                var id = $(".li-settings-dash-widget-list-select").attr("class").replace("li-settings-dash-widget-list-select", "").replace("li-settings-dash-widget-list", "").replace("li-grp-", "").trim();
                fnResetDashSeq(id, function () {
                    $(".li-settings-dash-widget-list-select").trigger("click");
                });
            },

            download: function (iValue) {
                var that = this;
                var self = this;
                var intSystemID = $(this.syncElements[iValue]).val();
                showMsgView("024", $(this.syncElements[iValue]).parent().find('label').text().trim());
                var intType = eval("imgsystem");
                var arrTotalFiles = [];
                // syncname = $(this.syncElements[iValue]).parent().find('label').text().trim();
                // if(($(this.syncElements[iValue]).parent()).hasClass("li-file-sync"))
                // 	syncname = $(this.syncElements[iValue]).attr('id');

                fnGetFilesFor(intSystemID, function (arrTotalFiles) {
                    console.log("File to be downloaded");
                    var progressElem = $(that.syncElements[iValue]).parent().find(".spn-progress");
                    if (intSystemID != "") {
                        fnGetFileSize(arrTotalFiles, function (data) {
                            var connectionType = (navigator.connection.type).toLowerCase();
                            if ((data / 1024 / 1024 > intWarnSize) && (connectionType.indexOf('cellular') >= 0)) {
                                navigator.notification.confirm(
                                    lblNm("msg_files_download") + $(that.syncElements[iValue]).parent().find('label').text().trim() + lblNm("msg_is_greater") + intWarnSize + lblNm("msg_nw_cost"), // message
                                    function (buttonIndex) {
                                        if (buttonIndex == '1') {
                                            that.downloadFile(0, arrTotalFiles, progressElem, function () {
                                                that.endOfDownload(iValue, arrTotalFiles, progressElem);
                                            });
                                        } else
                                            that.endOfDownload(iValue, arrTotalFiles, progressElem);
                                    },
                                    lblNm("msg_warning"), // title
                                [lblNm("msg_yes"), lblNm("msg_no")] // buttonLabels
                                );
                            } else {
                                that.downloadFile(0, arrTotalFiles, progressElem, function () {
                                    that.endOfDownload(iValue, arrTotalFiles, progressElem);
                                });
                            }
                        });
                    } else {
                        that.downloadFile(0, arrTotalFiles, progressElem, function () {
                            that.endOfDownload(iValue, arrTotalFiles, progressElem);
                        });
                    }

                });

                //	});

            },

            endOfDownload: function (iValue, arrTotalFiles, progressElem) {
                var that = this;
                var now = new Date();
                var syncid = $(this.syncElements[iValue]).attr('id');
                fnInsertLastsync(syncid, now);

                if ((iValue + 1) < that.syncElements.length) {
                    that.download(iValue + 1);
                } else {
                    var intCount = that.FilesDownloaded + that.Failed;
                    showMsgView("030", intCount + " of " + that.FilesDownloaded);
                    that.lastSync();
                    that.$("#btn-sync").attr("disabled", false);
                    that.$("#btn-sync").html(lblNm("sync"));
                    that.syncMode = true;
                    boolUpdate = true;
                    $("#btn-updates").removeClass("btn-updates-syncmode");
                    that.$(".div-common").removeClass("checked-items");
                    that.$(".checked-sync").each(function () {
                        $(this).removeClass("checked-sync");
                    });
                    that.$(".sync-checked").each(function () {
                        $(this).removeClass("sync-checked");
                    });
                    $(".div-progress-bar").hide();
                    $(".div-sync-label-big").removeClass("div-sync-label-big");
                    $(".div-lastsync").show();
                }

            },

            deleteFiles: function (iValue) {
                var that = this;
                var self = this;
                var arrSubPaths = [];
                var intSystemID = $(this.deleteElements[iValue]).val();
                showMsgView("041", $(this.deleteElements[iValue]).parent().find('label').text().trim());
                showMsgView("042", $(this.deleteElements[iValue]).parent().find('label').text().trim());
                arrSubPaths.push("System/" + intSystemID);

                //Getting the Group Data mapped under the system
                fnGetMappedGroups(undefined, intSystemID, function (groupData) {
                    var intGroupID = [];
                    for (var j = 0; j < groupData.length; j++) {
                        intGroupID.push(groupData[j]["ID"]);
                        arrSubPaths.push("Group/" + groupData[j]["ID"]);
                    }

                    //Getting the Set Data mapped under the system
                    fnGetMappedSets(undefined, intSystemID, function (setData) {
                        var intSetID = [];
                        for (var k = 0; k < setData.length; k++) {
                            intSetID.push(setData[k]["ID"]);
                            arrSubPaths.push("Set/" + setData[k]["ID"]);
                        }
                        var intPartID = [];

                        //Getting the PartID mapped under the set
                        that.getPartsforSystem(0, intSetID, intPartID, fnGetPartsForSet, function (setParts) {
                            //Getting the PartID mapped under the group
                            that.getPartsforSystem(0, intGroupID, setParts, fnGetPartsForGroup, function (groupParts) {
                                intPartID = [];
                                intPartID = groupParts;
                                for (var l = 0; l < intPartID.length; l++) {
                                    arrSubPaths.push("Part_Number/" + intPartID[l]);
                                }
                                that.deleteFolder(0, arrSubPaths, function() {
                                    if ((iValue + 1) < that.deleteElements.length)
                                        that.deleteFiles(iValue + 1);
                                    else {
                                        that.$("#btn-delete").attr("disabled", false);
                                        that.$("#btn-delete").html(lblNm("delete"));
                                        that.$(".div-common").removeClass("checked-items");
                                        that.$(".checked-sync").each(function () {
                                            $(this).removeClass("checked-sync");
                                        });
                                        that.$(".sync-checked").each(function () {
                                            $(this).removeClass("sync-checked");
                                        });
                                    }
                                })
                            });
                        });
                    });
                });
            },

            deleteFolder: function (iValue, arrPaths, callback) {
                var that = this;
                if (arrPaths.length == iValue)
                    callback();
                else {
                    fnDeleteDirectory(this.localFileSystem, arrPaths[iValue], function (status) {
                        if (status == "success") {
                            that.deleteFolder(iValue + 1, arrPaths, callback);
                        }
                    });
                }
            },

            downloadDocument: function (iValue, documents, progressElem, callback) {
                var that = this;

                if (documents.length > 0) {
                    var documen = documents[iValue];
                    var Source = setFileServer() + documen["Path"] + documen["Name"];
                    var Destination = this.localpath + documen["Path"] + documen["Name"];
                    fnDownload(Source, Destination, function (percent) {
                        percent = percent * 100;
                        var percentage = (iValue * (100 / documents.length)) + (percent / documents.length); //Logic for Progress Percentage
                        $(progressElem).css("width", percentage + "%");
                    }, function (FilesDownloaded, Failed) {
                        if ((iValue + 1) < documents.length) {
                            that.downloadDocument(iValue + 1, documents, progressElem, callback);
                        } else {
                            if (FilesDownloaded != undefined) {
                                that.FilesDownloaded = FilesDownloaded; /*Get file count when download succeeds */
                            }
                            if (Failed != undefined) {
                                that.Failed = Failed; /*Get failed count when download fails */
                            }
                            callback();
                        }
                    });
                } else {
                    $(progressElem).css("width", "100%");
                    callback();
                }
            },


            getPartsforSystem: function (iValue, intSetORGroupID, totalParts, fnName, callback) {
                var that = this;
                fnName(undefined, intSetORGroupID[iValue], function (partData) {
                    for (var i = 0; i < partData.length; i++) {
                        if (totalParts.indexOf(partData[i]["ID"]) < 0)
                            totalParts.push(partData[i]["ID"]);
                    }
                    if ((iValue + 1) < intSetORGroupID.length)
                        that.getPartsforSystem(iValue + 1, intSetORGroupID, totalParts, fnName, callback);
                    else
                        callback(totalParts);
                });
            },

            loadFileforDownload: function (iValue, intCodeVal, intItemIDs, totalFiles, callback) {
                var that = this;
                fnGetAllFiles(intCodeVal, intItemIDs[iValue], function (files) {
                    for (var i = 0; i < files.length; i++) {
                        totalFiles.push(files[i]);
                    }
                    if ((iValue + 1) < intItemIDs.length)
                        that.loadFileforDownload(iValue + 1, intCodeVal, intItemIDs, totalFiles, callback);
                    else
                        callback(totalFiles);
                });
            },

            downloadFile: function (iValue, files, progressElem, callback) {
                var that = this;
                var server = setFileServer();
                if (GM.Global.CRMFile)
                    server = URL_CRMFileServer;
                if (files.length > 0) {
                    var file = files[iValue];
                    console.log("Currently Downloaded File");
                    var Source = server + file["Path"];
                    var Destination = this.localpath + file["Path"];
                    fnDownload(Source, Destination, function (percent) {
                        percent = percent * 100;
                        var percentage = (iValue * (100 / files.length)) + (percent / files.length); //Logic for Progress Percentage
                        $(progressElem).css("width", percentage + "%");
                    }, function (FilesDownloaded, Failed) {
                        if ((iValue + 1) < files.length) {
                            that.downloadFile(iValue + 1, files, progressElem, callback);
                        } else {
                            if (FilesDownloaded != undefined) {
                                that.FilesDownloaded = FilesDownloaded; /*Get file count when download succeeds */
                            }
                            if (Failed != undefined) {
                                that.Failed = Failed; /*Get failed count when download fails */
                            }
                            callback();
                        }
                    });
                } else {
                    $(progressElem).css("width", "100%");
                    callback();
                }
            },

            selectData: function (event) {
                //event.preventDefault(); commenting this code for PMT-50719
                var target = event.currentTarget;
                if (this.syncMode) {
                    //			showMsgView("011");
                    if (!$(target).hasClass("checked-sync")) {
                        $(target).addClass("checked-sync");
                        $(target).find('input').each(function () {
                            $(this).attr("class", "sync-checked");
                        });
                    } else {
                        $(target).removeClass("checked-sync");
                        $(target).find('input').each(function () {
                            $(this).attr("class", "");
                        });
                    }
                }
            },

            getLocalePanel: function () {
                var CurrLocale = localStorage.getItem("deviceLng");
                console.log(CurrLocale);
                fnGetLocale(CurrLocale, function (data) {
                    console.log(data);
                    if (data.length > 0) {
                        $("#div-locale-text").html(data[0].LOCALE_NM);
                        $("#div-locale-flag img").attr("src", "globusMobileApp/image/icon/ios/" + data[0].LOCALE_FLAG + ".png");
                    } else {
                        $("#div-locale-text").html(Locale.region);
                        $("#div-locale-flag img").attr("src", "globusMobileApp/image/icon/ios/" + Locale.image.region);
                    }
                });
            },
            
            /* Used when language is changed */
            showLocalePanel: function (event) {
                //		showMsgView("011");
                console.log(event);
                var that = this;
                var CurrLocale = localStorage.getItem("deviceLng");
                console.log(CurrLocale);
                //changes removed for setting Dynamic locale info(PMT-29507)
                fnGetLocaleItems(CurrLocale, function (data) {
                    console.log(data);

                    if (data.length > 0) {
                            $("#div-locale").addClass("div-settings-locale");
                            $("#div-locale").html(that.template_locale(data));
                            $("#div-locale").toggle();
                        }
                    });
            },

            startSync: function(event) {
                try {
                    var that = this;
                    if ((navigator.onLine) && (!intOfflineMode)) {
                        if (this.syncMode) {
                            this.i = 0;
                            this.syncElements = document.getElementsByClassName('sync-checked');
                            this.syncCheckElements = document.getElementsByClassName('checked-items');

                            if (this.syncElements.length > 0) {
                                this.$("#btn-sync").attr("disabled", true);
                                this.$("#btn-sync").html(lblNm("syncing_c"));
                                this.syncMode = false;
                                boolUpdate = false;
                                boolSyncing = true;
                                $("#btn-updates").addClass("btn-updates-syncmode");
                                this.$(".checked-sync").each(function() {
                                    $(this).find(".div-progress-bar").show();
                                    $(this).find("label").addClass("div-sync-label-big");
                                    $(this).find(".div-lastsync").hide();
                                    $(this).find(".spn-progress").css("width", "0");
                                });
                                if (this.$(".selectedTab").attr("name").trim().indexOf("Data") >= 0) {
//                                    window.clearTimeout(UpdateTimer);
//                                    if (!GM.Global.Browser)
//                                        window.clearTimeout(timerSplashScreen);
                                    this.sync(this.syncElements[this.i]);
                                } else {
                                    if (this.syncElements.length > 0) {
                                        if (this.$(".selectedTab").attr("name").trim().indexOf("CRM") >= 0)
                                            GM.Global.CRMFile = true;
                                        else
                                            GM.Global.CRMFile = undefined;
                                        fnGetLocalFileSystemPath(function(path) {
                                            that.localpath = path;
                                            that.FilesDownloaded = 0; /*declared globally in GmfileSync.js*/
                                            that.Failed = 0;
                                            that.download(0);
                                        });
                                    } else {
                                        that.$("#btn-sync").attr("disabled", false);
                                        that.$("#btn-sync").html(lblNm("sync"));
                                        that.syncMode = true;
                                        boolUpdate = true;
                                        boolSyncing = false;
                                        $("#btn-updates").removeClass("btn-updates-syncmode");
                                        that.$(".div-common").removeClass("checked-items");
                                        that.$(".checked-sync").each(function() {
                                            $(this).removeClass("checked-sync");
                                        });
                                        that.$(".sync-checked").each(function() {
                                            $(this).removeClass("sync-checked");
                                        });
                                        $(".div-progress-bar").hide();
                                        $(".div-sync-label-big").removeClass("div-sync-label-big");
                                        $(".div-lastsync").show();
                                        showMsgView("039");
                                    }
                                }
                            } else
                            if ($('.selectedTab').attr('id').indexOf("data") >= 0) {
                                showMsgView("015");
                            } else
                                showMsgView("051");
                        }
                    } else if (intOfflineMode)
                        showNativeAlert(lblNm("msg_offline_mode"), lblNm("msg_offline_mode_restriction"));
                    else
                        showMsgView("014");
                } catch (err) {
                    showAppError(err, "GmSettingsView:startSync()");
                }
            },

            startDelete: function (event) {
                var that = this;

                if (this.syncMode) {
                    this.deleteElements = document.getElementsByClassName('sync-checked');

                    if (this.deleteElements.length > 0) {
                        this.$("#btn-delete").attr("disabled", true);
                        this.$("#btn-delete").html(lblNm("msg_deleteing"));
                        fnGetLocalFileSystem(function (entry) {
                            that.localFileSystem = entry;
                            that.deleteFiles(0);
                        });
                    } else {
                        showMsgView("048");
                    }

                }
            },

        sync: function(element) {
            try {
                var that = this;
                var ws = $(element).val();
                var code = eval(ws + ".code");
                showMsgView("024", $(element).parent().find('.sync-data-label').text().trim());
                var syncname = $(element).parent().find('label').text().trim();
                console.log("syncname >>>>>>>>" + syncname);
                var now = new Date();

                $(".spn-current-progress").removeClass("spn-current-progress");
                $(element).parent().find(".spn-progress").addClass("spn-current-progress");
                /*Check previous sync status from t9151 table.
                 *if status is failure then clear local table and void sync detail in server for current sync type.
                 *Start fresh sync for the sync type.
                 */
                fnSyncStatusCheck(ws, function(syncstatus) {
                    try {
                        if (syncstatus != undefined) {
                            var objVO = eval(ws);
                            objVO.parentStatus = syncstatus;
                        }
                        DataSync_SL(ws, '', function(status) {
                            try {
                                that.i++;
                                fnInsertLastsync(syncname.split(' ').join(''), now);
                                fnClearUpdates(code, function(sts) {
                                    try {
                                        fnGetUpdates(function(length) {
                                            that.lastSync();
                                            if (length > 0) {
                                                $(".divUpdatesNo").html('');
                                                $(".divUpdatesNo").html("&nbsp;" + length + "&nbsp;");
                                                $(".divUpdatesNo").trigger("refresh");
                                                $(".divUpdatesNo").css("display", "inline");
                                            }
                                        });
                                        if (that.i < that.syncElements.length) {
                                            if ($(that.syncElements[that.i]).val() == "listGmSurgeonRepAdVpVO") {
                                                fnClearV700(function() {
                                                    that.sync(that.syncElements[that.i]);
                                                });
                                            } else
                                                that.sync(that.syncElements[that.i]);
                                        } else {
                                            if (status != "success") {
                                                showMsgView("014");
                                            } else {
                                                showMsgView("013");
                                            }
                                            that.$("#btn-sync").attr("disabled", false);
                                            that.$("#btn-sync").html(lblNm("sync"));
                                            that.syncMode = true;
                                            boolUpdate = true;
                                            boolSyncing = false;
                                            $("#btn-updates").removeClass("btn-updates-syncmode");
                                            fnTimerSplashScreen();
//                                            chkUpdate();
                                            that.$(".checked-sync").each(function() {
                                                $(this).removeClass("checked-sync");
                                            });
                                            that.$(".sync-checked").each(function() {
                                                $(this).removeClass("sync-checked");
                                            });
                                            $(".div-progress-bar").hide();
                                            $(".div-sync-label-big").removeClass("div-sync-label-big");
                                            $(".div-lastsync").show();
                                            that.count();
                                        }
                                    } catch (err) {
                                        showAppError(err, "GmSettingsView:sync(3) ");
                                    }
                                });
                            } catch (err) {
                                showAppError(err, "GmSettingsView:sync(2) ");
                            }
                        });
                    } catch (err) {
                        showAppError(err, "GmSettingsView:sync(1) ");
                    }
                });
            } catch (err) {
                showAppError(err, "GmSettingsView:sync(0) ");
            }

        },
            //get the local count and db count from local table
            syncDetail: function (e) {
                var that = this;
                var target = e.currentTarget;
                if ($(target).html() != "")
                    if (this.syncMode) {
                        $(target).parent().trigger("click");
                        var seq = $(target).parent().parent().find("input").first().val()
                        var name = $(target).parent().parent().find("label").html().toUpperCase();
                        var data = eval(seq).syncGrp;
                        fnPopup(data, function (dt) {
                            var n = 0;
                            $("#msg-overlay").show();
                            $("#div-popup").addClass("sync");
                            $("#div-popup").html(that.template_detailsynclist(dt));
                            $("#div-popup h4").html(name)
                            _.each(dt, function (dt) {
                                if (dt.DBCNT == dt.LOCALCNT)
                                    $(".li-count-values-" + n).css("color", "black")
                                else
                                    $(".li-count-values-" + n).css("color", "red")
                                n = n + 1

                            });
                            var i = 0;
                            _.each(dt, function (data2) {
                                if (data2.DBCNT == null) {
                                    $(".li-count-values-" + i).find(".span-db-count").html("-");
                                }
                                if (data2.LOCALCNT == null) {
                                    $(".li-count-values-" + i).find(".span-loc-count").html("-/");
                                }
                                i = i + 1;
                            });
                            $("#div-popup").find('.fa').unbind('click').bind('click', function () {
                                $("#msg-overlay").hide();
                                $("#div-popup").html("");
                                $("#div-popup").removeClass("sync");
                            });

                        });
                    }
            },


            toggleSync: function (event) {
                this.$("#btn-sync").show();
                this.$("#btn-syncall").hide();
                if (this.syncMode) {
                    $(".selectedTab").removeClass("selectedTab");
                    $(event.currentTarget).addClass("selectedTab");
                    if ($("#div-settings-version-lable").css("display") == "none") {
                        $("#div-settings-menu-popup").slideToggle("fast");
                    }
                    this.$("#div-settings-sync").empty();
                    if (event.currentTarget.id.indexOf("data") >= 0) {
                        this.$('#ul-btn-sync').show();
                        this.$("#btn-syncall").show();
                        this.$("#btn-delete").hide();
                        this.$("#btn-dash-reset").hide();
                        this.$("#h1-settings-data-sync").addClass("selectedTab");
                        this.$("#h1-settings-data-sync-menu").addClass("selectedTab");
                        this.$("#div-settings-sync").html(this.template_data());
                        this.$("#div-settings-sync").removeClass("ul-account-price");
                        var height = document.getElementById('div-settings-sync').offsetHeight;
                        var scrollheight = document.getElementById('div-settings-sync').scrollHeight;
                        if (height < scrollheight)
                            showMsgView("043");
                        //				else 
                        //					showMsgView("011");
                        securityEventAccess(".li-data-sync", "name", "SYNC_DATA");
                    } else if (event.currentTarget.id.indexOf("file") >= 0) {
                        var that = this;
                        this.$('#ul-btn-sync').show();
                        this.$("#btn-delete").show();
                        this.$("#btn-dash-reset").hide();
                        this.$("#h1-settings-file-sync").addClass("selectedTab");
                        this.$("#h1-settings-file-sync-menu").addClass("selectedTab");
                        this.$("#div-settings-sync").removeClass("ul-account-price");
                        fnGetSystemList("url", function (data) {
                            that.$("#div-settings-sync").html(that.template_file(data));
                            var height = document.getElementById('div-settings-sync').offsetHeight;
                            var scrollheight = document.getElementById('div-settings-sync').scrollHeight;
                            if (height < scrollheight)
                                showMsgView("043");
                            //					else 
                            //						showMsgView("011");
                        });
                    } else if (event.currentTarget.id.indexOf("account") >= 0) {
                        var that = this;
                        this.$('#ul-btn-sync').hide();
                        this.$("#btn-delete").hide();
                        this.$("#btn-sync").hide();
                        this.$("#btn-dash-reset").hide();
                        this.$("#h1-settings-account-price").addClass("selectedTab");
                        this.$("#h1-settings-account-price-menu").addClass("selectedTab");
                        this.$("#div-settings-sync").html(this.template_account());
                        this.$("#div-settings-sync").addClass("ul-account-price");
                        var renderOptions = {
                            "el": this.$('#div-setting-account-price-search'),
                            "ID": "txt-setting-account-price-search",
                            "Name": "txt-search-name",
                            "Class": "txt-keyword-account",
                            "autoSearch": true,
                            "placeholder": lblNm("search_accounts_c"),
                            "fnName": fnGetAccountsForPrice,
                            "usage": 1,
                            "minChar" : minKeyinChar,
                            "target": this.$(".ul-setting-account-price-search-list"),
                            "template": 'GmAccountList',
                            "template_URL": URL_Settings_Template
                        };
                        var searchview = new SearchView(renderOptions);
                        this.showAccountSyncList();
                    } else if (event.currentTarget.id.indexOf("crm") >= 0) {
                        var that = this;
                        this.$('#ul-btn-sync').show();
                        this.$("#btn-delete").hide();
                        this.$("#btn-sync").show();
                        this.$("#btn-dash-reset").hide();
                        this.$("#h1-settings-crm").addClass("selectedTab");
                        this.$("#h1-settings-crm-menu").addClass("selectedTab");
                        this.$("#div-settings-sync").removeClass("ul-account-price");
                        this.$("#div-settings-sync").html(this.template_crm({
                            "ID": "",
                            "Name": "CRM FILES"
                        }));
                    } else {
                        var that = this;
                        this.$('#ul-btn-sync').show();
                        this.$("#btn-delete").hide();
                        this.$("#btn-sync").hide();
                        this.$("#div-wifi-note").hide();
                        this.$("#div-settings-sync").addClass("div-settings-sync-phone-dashboard");
                        this.$("#h1-settings-dashboard").addClass("selectedTab");
                        this.$("#h1-settings-dashboard-menu").addClass("selectedTab");
                        this.$("#div-settings-sync").removeClass("ul-account-price");
                        this.$("#btn-dash-reset").show();
                        this.showDashConfiguration();

                    }
                    this.lastSync();
                }
            },

            showAccountSyncList: function () {
                var that = this;
                fnGetSyncedAccountPriceNameList(function (data) {
                    that.$(".ul-setting-account-price-sync-list").html(that.template_accountsynclist(data));
                    that.$("#span-total-account-count").html(data.length);
                });
            },

            getAccountFromSearchView: function () {
                console.log("search list");
            },

            showDashConfiguration: function () {
                var that = this;
                if(this.renderCnt == 0) {
                    this.delordergroup(); 
                }
                this.renderCnt++;
                fnGetDashboardGroups(function (data) {
                    if (data.length > 0) {
                        that.$("#div-settings-sync").html(that.template_dashboard(data));
                        that.$(".li-grp-" + data[0].GROUP_ID).trigger("click");
                    } else {
                        fnGetServerData("configuredrilldown/dddetails", "listGmSalesDashConfColumnVO", undefined, function (data) {
                            that.insertConfigData(0, data);
                        });
                    }

                })
            },

              delordergroup:function () {
                    fnDeleteOrderDashColumns();
            },

            insertConfigData: function (iValue, data) {
                if (iValue < data.length) {
                    var that = this;
                    var currData = data[iValue];
                    if (currData.voidfl == "")
                        fnInsertDashboardSeq(currData.ddgrpid, currData.ddgrpnm, currData.ddcolumnnm, currData.seq_num, currData.ddcolumnnmshrtnm, function () {
                            that.insertConfigData(iValue + 1, data);
                        });
                    else
                        that.insertConfigData(iValue + 1, data);
                } else
                    this.showDashConfiguration();
            },

            selectWidget: function (event) {
                var id = (event.currentTarget.className).replace("li-settings-dash-widget-list", "").replace("li-settings-dash-widget-list-select", "").replace("li-grp-", "").trim();
                var that = this;

                if ($("#div-settings-version-lable").css("display") == "none") {
                    if ($(".li-settings-dash-widget-list").hasClass("li-settings-dash-widget-list-phone-sort")) {
                        $(".li-settings-dash-widget-list-phone-sort").removeClass("li-settings-dash-widget-list-phone-sort");
                        this.$(".li-settings-dash-widget-list-select").removeClass("li-settings-dash-widget-list-select");
                        $(".li-grp-" + id).addClass("li-settings-dash-widget-list-select");
                        $(".div-widget-itemlist-iphone").hide();
                        $("#" + id).show();
                    } else {
                        if ($(".li-grp-" + id).hasClass("li-settings-dash-widget-list-select")) {
                            this.$(".li-settings-dash-widget-list-select").removeClass("li-settings-dash-widget-list-select");
                            $(".div-widget-itemlist-iphone").hide();
                        } else {
                            this.$(".li-settings-dash-widget-list-select").removeClass("li-settings-dash-widget-list-select");
                            $(".li-grp-" + id).addClass("li-settings-dash-widget-list-select");
                            $(".div-widget-itemlist-iphone").hide();
                            $("#" + id).show();
                        }
                    }
                } else {
                    this.$(".li-settings-dash-widget-list-select").removeClass("li-settings-dash-widget-list-select");
                    $(".li-grp-" + id).addClass("li-settings-dash-widget-list-select");
                }
                $(".li-settings-widget-itemlist").removeClass("li-settings-widget-itemlist");
                fnGetDashColumnsFor(id, '', function (data) {
                    that.$(".ul-widget-typelist").html(that.template_column(data));
                    that.$("#" + id).find("#ul-widget-typelist-iphone").find("li").slice(3).addClass("li-settings-widget-itemlist");
                    that.$("#" + id).find("#ul-widget-typelist-iphone").find("li").slice(-1).addClass("li-settings-widget-itemlist-last");
                    that.$("#" + id).find("#ul-widget-typelist-iphone").find("li").eq(0).addClass("li-settings-widget-itemlist-first");
                    that.$("#ul-widget-typelist-ipad").find("li").slice(6).addClass("li-settings-widget-itemlist");
                    that.$("#ul-widget-typelist-ipad").find("li").slice(-1).addClass("li-settings-widget-itemlist-last");
                    that.$("#ul-widget-typelist-ipad").find("li").eq(0).addClass("li-settings-widget-itemlist-first");
                    that.$("#ul-widget-typelist-ipad").sortable({
                        start: function (event, ui) {
                            $(ui.helper).addClass("li-settings-dash-widget-list-select");
                        },
                        beforeStop: function (event, ui) {
                            $(ui.helper).removeClass("li-settings-dash-widget-list-select");
                            that.saveDashboardConfig();
                        },
                        stop: function (event, ui) {
                            that.$("#ul-widget-typelist-ipad").find("li").slice(6).addClass("li-settings-widget-itemlist");
                            that.$("#ul-widget-typelist-ipad").find("li").slice(-1).addClass("li-settings-widget-itemlist-last");
                            that.$("#ul-widget-typelist-ipad").find("li").eq(0).addClass("li-settings-widget-itemlist-first");
                        }
                    });
                    that.$("#ul-widget-typelist-ipad").disableSelection();
                });
            },

            saveDashboardConfig: function () {
                this.$(".li-settings-widget-itemlist").removeClass("li-settings-widget-itemlist");
                this.$(".li-settings-widget-itemlist-last").removeClass("li-settings-widget-itemlist-last");
                this.$(".li-settings-widget-itemlist-first").removeClass("li-settings-widget-itemlist-first");
                var arrNewConfig = new Array();
                this.$("#ul-widget-typelist-ipad").find("li").each(function (index) {
                    arrNewConfig.push({
                        "seq_id": (this.id).replace("li-seq-", ""),
                        "seq_no": (index + 1)
                    });
                });
                this.updateSavedConfig(0, arrNewConfig);
            },

            goDown: function (event) {
                var li = $(event.currentTarget).parent();
                var currLiIndex = $(li).index() + 1;
                var targetLiIndex = currLiIndex + 1;
                var arrNewConfig = new Array();

                var currUpdateSeqID = this.$(".ul-widget-typelist").find("li").eq(currLiIndex - 1).attr("id").replace("li-seq-", "");
                arrNewConfig.push({
                    "seq_id": currUpdateSeqID,
                    "seq_no": targetLiIndex
                });

                var targetUpdateSeqID = this.$(".ul-widget-typelist").find("li").eq(targetLiIndex - 1).attr("id").replace("li-seq-", "");
                arrNewConfig.push({
                    "seq_id": targetUpdateSeqID,
                    "seq_no": currLiIndex
                });

                this.$("#ul-widget-typelist-ipad").find("li").each(function (index) {
                    var seq = (this.id).replace("li-seq-", "");
                    if ((seq != targetUpdateSeqID) && (seq != currUpdateSeqID))
                        arrNewConfig.push({
                            "seq_id": seq,
                            "seq_no": (index + 1)
                        });
                });

                this.updateSavedConfig(0, arrNewConfig, function () {
                    $(".li-settings-dash-widget-list-select").trigger("click");
                });


                if ($("#div-settings-version-lable").css("display") == "none") {
                    this.$(".li-settings-dash-widget-list").addClass("li-settings-dash-widget-list-phone-sort");
                }


            },

            goUp: function (event) {
                var li = $(event.currentTarget).parent();
                var currLiIndex = $(li).index() + 1;
                var targetLiIndex = currLiIndex - 1;
                var arrNewConfig = new Array();

                var currUpdateSeqID = this.$(".ul-widget-typelist").find("li").eq(currLiIndex - 1).attr("id").replace("li-seq-", "");

                arrNewConfig.push({
                    "seq_id": currUpdateSeqID,
                    "seq_no": targetLiIndex
                });

                var targetUpdateSeqID = this.$(".ul-widget-typelist").find("li").eq(targetLiIndex - 1).attr("id").replace("li-seq-", "");
                arrNewConfig.push({
                    "seq_id": targetUpdateSeqID,
                    "seq_no": currLiIndex
                });

                this.$("#ul-widget-typelist-ipad").find("li").each(function (index) {
                    var seq = (this.id).replace("li-seq-", "");
                    if ((seq != targetUpdateSeqID) && (seq != currUpdateSeqID))
                        arrNewConfig.push({
                            "seq_id": seq,
                            "seq_no": (index + 1)
                        });
                });

                this.updateSavedConfig(0, arrNewConfig, function () {
                    $(".li-settings-dash-widget-list-select").trigger("click");
                });


                if ($("#div-settings-version-lable").css("display") == "none") {
                    this.$(".li-settings-dash-widget-list").addClass("li-settings-dash-widget-list-phone-sort");
                }
            },

            updateSavedConfig: function (iValue, data, callback) {
                if (iValue < data.length) {
                    var currData = data[iValue],
                        that = this;
                    fnUpdateDashboardSeq(currData.seq_id, currData.seq_no, function () {
                        that.updateSavedConfig(iValue + 1, data, callback);
                    });
                } else {
                    //			fnShowStatus("");
                    if (callback != undefined)
                        callback();
                }
            },

            syncAccountPrice: function() {
                try {
                    if ((navigator.onLine) && (!intOfflineMode)) {
                        if (this.syncMode) {
                            var that = this;
                            var arrAccounts = new Array(),
                                accounts, input = {};
                            var that = this;
                            this.$(".ul-setting-account-price-search-list").find(".sync-checked").each(function() {
                                arrAccounts.push(this.id);
                            });
                            accounts = arrAccounts.join(",");
                            input.acctid = accounts;
                            input.reftype = 4000524;
                            input.syncall = "Y";
                            if(input.acctid.length < 1)
                            showError("Please select an Account");
                            fnShowStatus(lblNm("syncing_c"));
                            $("#div-setting-account-price-info").find(".div-progress-bar").show();
                            $("#div-setting-account-price-info").find(".spn-progress").addClass("spn-current-progress");
                            this.syncMode = false;
                            boolUpdate = false;
                            $("#btn-updates").addClass("btn-updates-syncmode");
                            $("#txt-setting-account-price-search").attr("disabled", true);
                            DataSync_SL('listGmAcctGPOPriceVO', input, function(status) {
                                try {
                                    if (status != "fail") {
                                        input.reftype = 4000526;
                                        DataSync_SL('listGmAcctPartPriceVO', input, function(status) {
                                            try {
                                                if (status.toLowerCase() == "success")
                                                    that.endPriceSync(accounts, 'Y');
                                                else {
                                                    that.syncMode = true;
                                                    boolUpdate = true;
                                                    $("#btn-updates").removeClass("btn-updates-syncmode");
                                                    $("#div-setting-account-price-info").find(".div-progress-bar").hide();
                                                    $("#div-setting-account-price-info").find(".spn-progress").removeClass("spn-current-progress");
                                                    $("#txt-setting-account-price-search").attr("disabled", false);
                                                    showMsgView("014");
                                                }
                                            } catch (err) {
                                                showAppError(err, "GmSettingsView:syncAccountPrice(2)");
                                            }
                                        });
                                    } else {
                                        that.syncMode = true;
                                        boolUpdate = true;
                                        $("#btn-updates").removeClass("btn-updates-syncmode");
                                        $("#div-setting-account-price-info").find(".div-progress-bar").hide();
                                        $("#div-setting-account-price-info").find(".spn-progress").removeClass("spn-current-progress");
                                        $("#txt-setting-account-price-search").attr("disabled", false);
                                        showMsgView("014");
                                    }
                                } catch (err) {
                                    showAppError(err, "GmSettingsView:syncAccountPrice(1)");
                                }
                            });

                        }
                    } else if (intOfflineMode)
                        showNativeAlert(lblNm("msg_offline_mode"), lblNm("msg_offline_mode_restriction"));
                    else
                        showMsgView("014");
                } catch (err) {
                    showAppError(err, "GmSettingsView:syncAccountPrice(0)");
                }
            },

            // End Of Account Price Sync - update sync in Table
            endPriceSync: function(accounts, syncValue) {
                try {
                    this.syncMode = true;
                    boolUpdate = true;
                    $("#btn-updates").removeClass("btn-updates-syncmode");
                    $("#txt-setting-account-price-search").attr("disabled", false);
                    var that = this;
                    this.$("#txt-setting-account-price-search").val("");
                    this.$("#txt-setting-account-price-search").trigger("keyup");
                    $("#div-setting-account-price-info").find(".div-progress-bar").hide();
                    $("#div-setting-account-price-info").find(".spn-progress").removeClass("spn-current-progress");

                    fnUpdatePriceSyncInfo(accounts, syncValue, function() {
                        try {
                            that.showAccountSyncList();
                        } catch (err) {
                            showAppError(err, "GmSettingsView:endPriceSync(1)");
                        }
                        //          fnShowStatus("");
                    });
                } catch (err) {
                    showAppError(err, "GmSettingsView:endPriceSync(0)");
                }
            },

            //Delete Account Price Sync - unless the Mapped GPO is linked to any other Synced account which is not to be deleted
            deleteAccountPrice: function() {
                try {
                    if (this.syncMode) {
                        var that = this,
                            arrAccounts = new Array();
                        this.$(".div-setting-account-price-select-list").find(".sync-checked").each(function() {
                            arrAccounts.push(this.value);
                        });

                        var strAccounts = arrAccounts.join(',');
                        this.syncMode = false;
                        boolUpdate = false;
                        $("#btn-updates").addClass("btn-updates-syncmode");
                        $("#txt-setting-account-price-search").attr("disabled", true);
                        fnGetGOPMapping(strAccounts, function(GPOS) {
                            try {
                                var GPOAccounts = _.pluck(GPOS, "C704_ACCOUNT_ID");
                                GPOS = _.pluck(GPOS, "C101_PARTY_ID");

                                if (GPOS.length > 0) {
                                    var GPOSRestriction = new Array();
                                    that.$(".div-setting-account-price-select-list").find("input").each(function() {
                                        if (!$(this).hasClass("sync-checked"))
                                            GPOSRestriction.push($(this).attr("gpo"));
                                    });
                                    GPOS = _.difference(GPOS, GPOSRestriction);
                                }
                                fnDeleteAccountPrice(arrAccounts, function() {
                                    try {
                                        if (GPOS.length > 0) {
                                            fnDeleteGPOPrice(GPOS, function() {
                                                that.endPriceSync(arrAccounts.join(','), null);
                                            });
                                        } else
                                            that.endPriceSync(arrAccounts.join(','), null);
                                    } catch (err) {
                                        showAppError(err, "GmSettingsView:deleteAccountPrice(2)");
                                    }
                                });
                            } catch (err) {
                                showAppError(err, "GmSettingsView:deleteAccountPrice()1");
                            }
                        });
                    }
                } catch (err) {
                    showAppError(err, "GmSettingsView:deleteAccountPrice(0)");
                }
            },
            showNavigation: function () {
                $("#div-settings-menu").each(function () {
                    if ($(this).hasClass('detail-menu'))
                        $(this).removeClass("detail-menu");
                    else
                        $("#div-settings-menu").addClass("detail-menu");
                })

                $("#div-settings-menu-popup").slideToggle("fast");
            },


            //Closes the View
            close: function (event) {
                $("#btn-settings").removeClass("btn-header-visited");
                this.unbind();
                this.remove();
            }

        });

        return SettingsView;
    });
