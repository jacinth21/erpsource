/**********************************************************************************
 * File:        GmDashView.js
 * Description: View to represent Dashboard for CRM
 * Version:     1.0
 * Author:      cskumar
 **********************************************************************************/
define([

        //External Libraries
        'jquery', 'underscore', 'backbone', 'handlebars',

        //Common
        'global', 'commonutil', 'gmvCRMMain',

        // Pre-Compiled Template
        'gmtTemplate', 'gmcItem', 'gmvItemList',
        ],

    function ($, _, Backbone, Handlebars,
        Global, CommonUtil, GMVCRMMain, GMTTemplate, GMCItem, GMVItemList) {

        'use strict';

        var widgetSize = 1;
        var  tempJSON;
        GM.View.CRMDashboard = Backbone.View.extend({

            el: "#div-crm-module",
            //events
            events: {
                "click .div-dash-module-header": "setTabModule",
                "click #dash-create-merc": "listUpcomingMERC",
                "click .OfflineLink": "goOfflineMsg",
                "click #div-dash-phy-header div": "togglePhy",
                "click .module-content-view": "toggleDashModule",
		        "click .gmlinksalescall": "toggleSalescall",
                "click #div-dash-ps-header div": "togglePS",
                "click .tradeUserIcon": "fetchTradeShowAttendeeRpt"
            },

            name: "CRM Dashboard View",

            /* Load the templates */
            template_mainmodule: fnGetTemplate(URL_Common_Template, "gmtCRMMainModule"),
            template_dashmodule: fnGetTemplate(URL_CRMDash_Template, "gmtCRMDashModule"),
            template_dashwidget: fnGetTemplate(URL_CRMDash_Template, "gmtCRMDashWidget"),
            template_SurgeonMERC: fnGetTemplate(URL_Surgeon_Template, "gmtMERCListPopup"),
            template_preceptorDashList: fnGetTemplate(URL_Preceptor_Template, "gmtPSDashList"),
            template_attendeemail: fnGetTemplate(URL_CRMDash_Template, "gmtCRMTShowAttendeeMail"),

            //Initial configurations
            initialize: function () {
//              $("#span-app-title").html("Dashboard");
                 $("#div-app-title").html(lblNm("crm"));
                this.tabAccess = _.pluck($.parseJSON(localStorage.getItem('userTabs')), "functionID");
                if (!$(".alert").hasClass("alertSuccess"))
                    hideMessages();
//                GM.Global.SCDATA = null;

                if (GM.Global.FieldSales == undefined) {
                    GM.Global.FieldSales = {};
                }
                GM.Global.SalesRep = isRep();
                this.setList = {};
            },

            render: function () {
                var that = this;
                console.log($("#div-crm-module").length);
                this.$el.html(this.template_mainmodule({
                    "module": "dashboard"
                }));
                this.$(".gmPanelHead").hide();

                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1;
                var yyyy = today.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd
                }
                if (mm < 10) {
                    mm = '0' + mm
                }
                this.today = mm + '/' + dd + '/' + yyyy;

                var width = $(window).width();
                $("#btn-back").show();
                if (width <= 480) {
                    onrefresh();
                    $("#mobile-menu-top-user-name").html(localStorage.getItem("fName") + " " + localStorage.getItem("lName"))
                    $(".div-moble-menu").removeClass("hide");
                }
                $(".li-ipad-user-pic").removeClass("hide");

                this.access = $.parseJSON(localStorage.getItem('userAccess'));
                if (!GM.Global.Browser)
                    fnGetLocalFileSystemPath(function (localpath) {
                        that.docPath = localpath;
                    });
                var dashcontent;
                var that = this;

                _.each(that.access, function (item, i) {
                    if (item.funcid.indexOf("CRMDB-") > -1) {
                        dashcontent = item;
                    }
                });

                var dashlayout = _.findWhere(dashConfig, {
                    SecGrp: dashcontent["funcid"].toString()
                });
                var dbmerc = _.findWhere(GM.Global.UserAccess, {
                    funcid: "CRMDB-MERC"
                });
                var dbtraining = _.findWhere(GM.Global.UserAccess, {
                    funcid: "CRMDB-TRAINING"
                });
                var crfinternaluse = _.findWhere(GM.Global.UserAccess, {
                    funcid: "CRMDB-INTUSE"
                });

                this.showDashboardCount();
/*
                if (_.contains(GM.Global.Admin, "crf") && ((dbmerc != undefined) || (dbtraining != undefined)) && (crfinternaluse == undefined)) {
                    if (!_.contains(dashlayout.Secondary, "crf-pending"))
                        dashlayout.Secondary.push("crf-pending");
                    if (!_.contains(dashlayout.Secondary, "crf-reviewed"))
                        dashlayout.Secondary.push("crf-reviewed");
                }

                if (_.contains(GM.Global.Admin, "crf") && (crfinternaluse != undefined)) {
                    if (!_.contains(dashlayout.Secondary, "crf-pending"))
                        dashlayout.Secondary.push("crf-pending");
                    if (!_.contains(dashlayout.Secondary, "crf"))
                        dashlayout.Secondary.push("crf");

                    dashlayout.Primary = ["crf-reviewed"];
                }
                */
                if (dashlayout.Primary)
                    this.createWidget(dashlayout.Primary.toString(), 2);

                var dashVal = dashlayout.Secondary;
               
                var usertabs = GM.Global.UserTabs;
                var dashaccsval=[];
                for (var i = 0; i < dashVal.length; i++) {
                     var hasAccess = _.findWhere(usertabs, {
                    module: dashVal[i]
                });
                if(hasAccess != undefined || dashVal[i] == 'merc' || dashVal[i] == 'quickreference')
                    dashaccsval.push(dashVal[i])
                }
                if($(window).width() > 767)
                    dashaccsval = _.without(dashaccsval,"preceptorship");
                for (var i = 0; i < dashaccsval.length; i++) {
                    if(i%2 == 0)
                    this.createWidget(dashaccsval[i].toString(), 1);
                    else
                    this.createWidget(dashaccsval[i].toString(), 2);
                }
            $('#div-app-title').removeClass('gmVisibilityHidden').addClass('gmVisibilityVisible');
                return this;
                

                
            },
            listUpcomingMERC: function (event) {
                if (!GM.Global.Device) {
                    var that = this;
                    if ($(event.currentTarget).hasClass("act-merc")) {
                        this.surgeonID = $(event.target).parent().parent().attr("data-partyid");
                        this.surgeonName = $(event.target).parent().parent().attr("data-partynm");
                    } else {
                        this.surgeonID = $(event.currentTarget).attr("party");
                        this.surgeonName = $(event.currentTarget).attr("name");
                    }
                    showPopup();
                    $("#div-crm-overlay-content-container .modal-body").html(this.template_SurgeonMERC());
                    $(".modal-title").text("Add '" + this.surgeonName + "' to MERC");
                    $(".modal-footer").addClass("hide");

                    var voname = "gmCRMActivityVO";
                    var sortoptions = [{
                        "DefaultSort": "actstartdate",
                        "Elements": [{
                                "ID": "actnm",
                                "DispName": "Title",
                                "ClassName": "gmDashColHead gmFontmerc gmText150"
                            },
                            {
                                "ID": "actstartdate",
                                "DispName": " From Date",
                                "ClassName": "gmDashColHead gmFontmerc gmText125"
                            },
                            {
                                "ID": "actenddate",
                                "DispName": " To Date",
                                "ClassName": "gmDashColHead gmFontmerc gmText100"
                            },
                            {
                                "ID": "location",
                                "DispName": "Location",
                                "ClassName": "gmDashColHead gmFontmerc gmText125"
                            }]
                    }];
                    var filterOptions = ["actnm", "actstartdate", "location"];
                    var itemtemplate = "gmtSurgeonMERCList";
                    var columnheader = 6;
                    var template_URL = URL_Surgeon_Template;
                    var controlID = "#div-crm-overlay-content-container #div-popup-merc-list";
                    var url = "crmactivity/info";
                    var color = "gmFontmerc";
                    that.search = ["actnm", "actstartdate", "actenddate", "location"];
                    var input = {
                        "token": GM.Global.Token,
                        "actcategory": "105265",
                        "actstartdate": this.today
                    };
                    console.log(JSON.stringify(input))
                    fnGetWebServerData(url, voname, input, function (data) {
                        if (data != null) {
                            data = arrayOf(data.GmCRMListVO.gmCRMActivityVO);
                            _.each(data, function (data) {
                                data.actstartdate = getDateByFormat(data.actstartdate);
                                data.actenddate = getDateByFormat(data.actenddate);
                                if (data.partyid != "" && data.partyid.indexOf(that.surgeonID) >= 0)
                                    data["added"] = "Y";
                                if (data.attrpartyid != "" && data.attrpartyid.indexOf(that.surgeonID) >= 0)
                                    data["reqstd"] = "Y";
                            });
                            that.showItemListView(controlID, data, itemtemplate, columnheader, sortoptions, template_URL, filterOptions, undefined, color);
                            $("#div-crm-overlay-content-container .modal-body .li-add-merc").bind("click", function (event) {
                                that.addParty(event);
                            });
                        } else
                            $(controlID).html("No Data Found");

                    });
                } else {
                    showError("Internet connection is required for this option");
                }

            },

            addParty: function (event) {
                var that = this;

                var input = {
                    "attrid": "",
                    "attrval": this.surgeonID,
                    "attrnm": this.surgeonName,
                    "actid": $(event.currentTarget).attr("actid"),
                    "attrtype": "105589",
                    "voidfl": ""
                };

                console.log("Add Party Input" + JSON.stringify(input));
                hidePopup();
                fnGetWebServerData("crmactivity/addattr", "gmCRMActivityAttrVO", input, function (data) {
                    if (data != null)
                        showSuccess(that.surgeonName + " requested for " + $(event.currentTarget).attr("actnm"), 5000);
                    else
                        showError("Request Failed");
                });
            },



            showItemListView: function (controlID, data, itemtemplate, columnheader, sortoptions, template_URL, filterOptions, csvFileName, color, callbackOnReRender) {
                data = arrayOf(data)
                for (var i = 0; i < data.length; i++) {
                    var row = data[i];
                    data[i].search = "";
                    if (this.search && this.search.length) {
                        _.each(this.search, function (srh) {
                            data[i].search += row[srh] + " ";
                        })
                    }
                }
                var items = new GMCItem(data);
                this.itemlistview = new GMVItemList({
                    el: controlID,
                    collection: items,
                    columnheader: columnheader,
                    itemtemplate: itemtemplate,
                    sortoptions: sortoptions,
                    template_URL: template_URL,
                    filterOptions: filterOptions,
                    csvFileName: csvFileName,
                    color: color,
                    callbackOnReRender: callbackOnReRender
                });


            },
            createWidget: function (module, widgetSize) {

                var approvers = _.findWhere(GM.Global.UserAccess, {
                    funcid: "CRMADMIN-CRFAPPR"
                });
                var userTabs = GM.Global.UserTabs;

                var options = _.findWhere(userTabs, {
                    module: module
                });
                if (module == "merc") {
                	var userData = JSON.parse(localStorage.getItem("userData"));
                	  var merctitle= (userData!=undefined && userData.cmpid!="1000")?"MERC International":"MERC North America ";
                    var options = {
                        "module": "merc",
                        "title": merctitle,
                        "url": "merc/dash"
                    };
                } 
                
                if (module == "quickreference") {
                    var options = {
                        "module": "quickreference",
                        "title": "Quick Reference",
                        "url": "quickref/list"
                    };
                }
                /*
                if (module == "crf") {
                    if (approvers != undefined)
                        var options = {
                            "module": "crf",
                            "title": "CRF - Pending Approval"
                        };
                    else
                        var options = {
                            "module": "crf",
                            "title": "CRF - All"
                        };
                }
                if (module == "crf-pending") {
                    var options = {
                        "module": "crf-pending",
                        "title": "CRF - Pending Approval"
                    };
                }
                if (module == "crf-reviewed") {
                    var options = {
                        "module": "crf-reviewed",
                        "title": "CRF - Pending Review"
                    };
                }
                */

                if (options != undefined) {
                    var info = {
                        "module": options.module,
                        "title": options.title,
                        "size": "size-" + widgetSize
                    };
                    if (module == "physicianvisit" && widgetSize == 2) {
                        info["phy"] = "Y";
                    }
                    this.$("#div-main-content").append(this.template_dashmodule(info));

                    if (options.module == "fieldsales") {
                        $("#div-dash-fieldsales .btn-dash-lnk a").attr("href", "#crm/fieldsales/list/region");
                        $("#div-dash-fieldsales .ul-dash-module-header a").attr("href", "#crm/fieldsales/list/region");
                    }
                    if (options.module == "surgeon") {
                        $("#div-dash-surgeon .btn-dash-lnk a").attr("href", "#crm/surgeon/list/myrep");
                        $("#div-dash-surgeon .ul-dash-module-header a.dashReportLink").attr("href", "#crm/surgeon/list/myrep");

                        var editAccess = _.findWhere(GM.Global.UserAccess, {
                            funcid: "CRM-SURGEON-EDIT"
                        });

                        if (editAccess != undefined) {
                            $(".btn-create-surgeon").removeClass("gmVisibilityVisible");
                        } else {
                            $(".btn-create-surgeon").addClass("gmVisibilityHidden");
                        }
                    }

                    if (_.contains(GM.Global.Admin, options.module) && options.module != "fieldsales" && options.module != "crf" || options.module == "preceptorship") {
                        $(".create-button-" + options.module).css({
                            "visibility": "visible"
                        });
                        $("#div-dash-" + options.module + " .i-dash-new").removeClass("hide");
                    } else
                        $(".create-button-" + options.module).css({
                            "visibility": "hidden"
                        });

                    if (_.contains(GM.Global.Admin, "physicianvisit") || _.contains(GM.Global.Admin, "physvisitrqst")) {
                        $(".create-button-physicianvisit").css({
                            "visibility": "visible"
                        });
                        $("#div-dash-physicianvisit .i-dash-new").removeClass("hide");
                    } else
                        $(".create-button-physicianvisit").css({
                            "visibility": "hidden"
                        });

                    this.loadWidget(options, widgetSize);
                }
            },

            loadWidget: function (options, widgetSize) {
                var that = this;
                //upcoming events
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1;
                var yyyy = today.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd
                }
                if (mm < 10) {
                    mm = '0' + mm
                }
                today = mm + '/' + dd + '/' + yyyy;
                var dbtoday = yyyy + '/' + mm + '/' + dd;
                //                var date = yyyy+'/'+mm+'/'+dd;
                var userid = localStorage.getItem("userID");
                var input = {};
                input.token = localStorage.getItem("token");

                switch (options.module) {
                    case "surgeon":
                        input.repid = localStorage.getItem("partyID");
                        var partyID = localStorage.getItem('partyID');
                        var userID = localStorage.getItem('userID');
                        if (GM.Global.Device) {
                            fnGetMySurgeon(userID, partyID, function (data) {
                                if (data.length) {
                                    data = $.parseJSON(JSON.stringify(data));
                                    var len = data.length;
                                    _.each(data, function (data, i) {
                                        data["module"] = "surgeon";
                                        data.surgeon = "Y";

                                        if (_.contains(GM.Global.Admin, "surgeon"))
                                            data["merc"] = "Y";
                                        if (_.contains(GM.Global.Admin, "physicianvisit") || _.contains(GM.Global.Admin, "physvisitrqst"))
                                            data["phy"] = "Y";
                                        if (_.contains(GM.Global.Admin, "salescall"))
                                            data["scall"] = "Y";

                                        if (data.state == null || data.state.trim() == "")
                                            data.state = "Unassigned";
                                        if (data.city == null || data.city.trim() == "")
                                            data.city = "Unassigned";
                                        if (data.city == "Unassigned" || data.state == "Unassigned")
                                            data.state = "";
                                        data["id"] = data.partyid;
                                        if (data.account == null || data.account.trim() == "")
                                            data.account = "Unassigned";
                                        data["title"] = data.firstnm + (data.midinitial ? (" " + data.midinitial) : "") + " " + data.lastnm;
                                        data["pic"] = "Y";
                                        if (data.picurl != "")
                                            data["server"] = that.docPath;
                                    });
                                    console.log("pic url" + JSON.stringify(data));
                                    that.$("#div-" + options.module + "-dash-content").html(that.template_dashwidget(data));
                                    $("#div-" + options.module + "-db-count").html(data.length);
                                } else {
                                    that.$("#div-" + options.module + "-dash-content").html("<div class = 'gmTextAll gmTextCenter gmPaddingTop10p'>No Data Found</div>");
                                    $("#div-" + options.module + "-db-count").html(0);
                                }
                            });
                        }
                        break;
                    case "salescall":
                        //input.partyid = localStorage.getItem("partyID");
                        input.actcategory = "105267";
                        input.actstartdate = today;
                        input.actstatus = "105761";
                        //input.actstartdate = dateOfnPastDays(salescallDays);                   

                        var d = new Date();
                        d.setDate(d.getDate() - salescallDays);
                        var dd = d.getDate();
                        var mm = d.getMonth() + 1;
                        var yyyy = d.getFullYear();
                        if (dd < 10) {
                            dd = '0' + dd
                        }
                        if (mm < 10) {
                            mm = '0' + mm
                        }
                        this.sctoday = yyyy + '/' + mm + '/' + dd;
                        if (GM.Global.Device) {
                            this.setList.today = this.sctoday;
                            this.setList.actcategory = '105267';
                            console.log("SCSCSC")
                            fnGetListActivity(this.setList, "MYLDSC", function (data) {
                                console.log("SSSSS" + JSON.stringify(data))
                                if (data.length) {
                                    data = $.parseJSON(JSON.stringify(data));
                                    _.each(data, function (data) {
                                        if (data.picurl != "") {
                                            data["server"] = that.docPath;
                                            data["salespic"] = "Y";
                                        }

                                        data["module"] = "salescall";
                                        data["id"] = data.actid;
                                        data["date"] = data.actstartdate;
                                        if (data.date == "")
                                            data.date = "N/A";
                                        else {
                                            //                                            var dd, mm, yyyy;
                                            data.date = getDateByFormat(data.date);
                                        }
                                        data["title"] = data.phynm;

                                        data["pic"] = "Y";
                                        data["salescall"] = "Y";
                                        if (data.result == "" || data.result == null)
                                            data.result = "Unassigned";
                                    });
                                    that.$("#div-" + options.module + "-dash-content").html(that.template_dashwidget(data));
                                    $("#div-" + options.module + "-db-count").html(data.length);
                                } else {
                                    that.$("#div-" + options.module + "-dash-content").html("<div class = 'gmTextAll gmTextCenter gmPaddingTop10p'>No Data Found</div>");
                                    $("#div-" + options.module + "-db-count").html(0);
                                }
                            });
                        }

                        break;
                    case "physicianvisit":
                        input.actcategory = "105266";
                        if (widgetSize == 2)
                            input.actstatus = "105761,105760";
                        else
                            input.actstatus = "105761";
                        input.actstartdate = today;
                        GM.Global.Status = {};
                        GM.Global.Status.listtype = "Scheduled";
                        if (GM.Global.Device) {
                            this.setList.today = dbtoday;
                            this.setList.actcategory = '105266';
                            this.setList.actstat = '105761';
                            fnGetListActivity(this.setList, "UPC", function (data) {
                                console.log("phy")
                                console.log(data);

                                if (data.length) {
                                    _.each(data, function (data, i) {
                                        data.type = "Y";
                                        data["module"] = "physicianvisit";
                                        data["date"] = data.actstartdate;


                                        if (data.date == "")
                                            data.date = "N/A";
                                        else {
                                            data.date = getDateByFormat(data.date);
                                        }

                                        data["type"] = data.acttype;

                                        if (data.type == "VIP Only")
                                            data.type = "vip";
                                        else if (data.type == "Research")
                                            data.type = "research";
                                        else if (data.type == "Design")
                                            data.type = "design";
                                        else if (data.type == "VIP Visit + Lab")
                                            data.type = "viplab";
                                        else if (data.type == "Lab Only")
                                            data.type = "lab";
                                        data[data.type] = "Y";

                                        data["phy"] = "Y";
                                        data["index"] = i + 1;
                                    });

                                    that.$("#div-" + options.module + "-dash-content").html(that.template_dashwidget(data));
                                    $("#div-" + options.module + "-db-count").html(data.length);
                                    resizeFont();
                                } else {
                                    that.$("#div-" + options.module + "-dash-content").html("<div class = 'gmTextAll gmTextCenter gmPaddingTop10p'>No Data Found</div>");
                                    $("#div-" + options.module + "-db-count").html(0);
                                }
                            });
                        }

                        break;
                    case "lead":
                        options.url = "crmactivity/info";
                        options.voname = "gmCRMActivityVO"
                        if (isRep()) {
                            input.partyid = localStorage.getItem("partyID");
                            if (GM.Global.Device) {
                                fnGetMyLead(localStorage.getItem("partyID"), function (data) {
                                    console.log("Lead Data");
                                    console.log(data);
                                    if (data.length) {
                                        that.$("#div-" + options.module + "-dash-content").html(that.template_dashwidget(data));
                                        $("#div-" + options.module + "-db-count").html(data.length);
                                        resizeFont();
                                    } else {
                                        that.$("#div-" + options.module + "-dash-content").html("<div class = 'gmTextAll gmTextCenter gmPaddingTop10p'>No Data Found</div>");
                                        $("#div-" + options.module + "-db-count").html(0);
                                    }
                                });
                            }
                        } else {
                            input.areauserid = localStorage.getItem("userID");
                            if (GM.Global.Device) {
                                fnGetLeadADVP(function (data) {
                                    if (data.length) {
                                        that.$("#div-" + options.module + "-dash-content").html(that.template_dashwidget(data));
                                        $("#div-" + options.module + "-db-count").html(data.length);
                                        resizeFont();
                                    } else {
                                        that.$("#div-" + options.module + "-dash-content").html("<div class = 'gmTextAll gmTextCenter gmPaddingTop10p'>No Data Found</div>");
                                        $("#div-" + options.module + "-db-count").html(0);
                                    }

                                });
                            }
                        }

                        input.actcategory = "105269";
                        break;
                    case "merc":
//                        input.actcategory = "105265";
//                        input.actstartdate = today;
                    	  var userData = JSON.parse(localStorage.getItem("userData"));
                  	      input.regnid= (userData!=undefined && userData.cmpid!="1000")?"100824":"100823";
                        if (GM.Global.Device) {
                            this.setList.today = dbtoday;
                            this.setList.actcategory = '105265';
                             fnGetWebServerData(options.url, options.voname, input, function (data) {
                                 if (data.length) {
                                  _.each(data, function (data, i) {
                                        data["module"] = "merc";
                                        data["mercD"] = "Y";
                                        data["index"] = i + 1;
                                  });
                                  that.$("#div-" + options.module + "-dash-content").html(that.template_dashwidget(data));
                                 $(".merc-dash-link").unbind('click').bind('click', function (e) {
                                     var mercid=$(e.currentTarget).data("id");
                                    that.showMercPDF(mercid);
                                 });
                                 $(".merc-event-link").unbind('click').bind('click', function (e) {
                                     var merclink=$(e.currentTarget).attr("href");
                                     window.open(merclink, '_blank', 'location=no');
                                 });
                                    $("#div-" + options.module + "-db-count").html(data.length);
                                    resizeFont();
                                } else {
                                    that.$("#div-" + options.module + "-dash-content").html("<div class = 'gmTextAll gmTextCenter gmPaddingTop10p'>No Data Found</div>");
                                    $("#div-" + options.module + "-db-count").html(0);
                                }
                             });
                        }
                        break;
                    case "training":
                        input.actcategory = "105268";
                        input.actstartdate = today;
                        options.voname = "gmCRMActivityVO";
                        if (GM.Global.Device) {
                            this.setList.today = dbtoday;
                            this.setList.actcategory = '105268';
                            fnGetListActivity(this.setList, "UPC", function (data) {
                                if (data.length) {
                                    data = $.parseJSON(JSON.stringify(data));
                                    _.each(data, function (data, i) {
                                        data["module"] = "training";
                                        data["trainD"] = "Y";
                                        data["index"] = i + 1;

                                        if (data.location == null || data.location.trim() == "")
                                            data.location = "Unassigned";
                                        data["date"] = data.actstartdate;


                                        if (data.date == "")
                                            data.date = "Unassigned";
                                        else {
                                            data.date = getDateByFormat(data.date);
                                        }
                                        data["topic"] = data.actnm;
                                    });


                                    that.$("#div-" + options.module + "-dash-content").html(that.template_dashwidget(data));
                                    $("#div-" + options.module + "-db-count").html(data.length);
                                    resizeFont();
                                } else {
                                    that.$("#div-" + options.module + "-dash-content").html("<div class = 'gmTextAll gmTextCenter gmPaddingTop10p'>No Data Found</div>");
                                    $("#div-" + options.module + "-db-count").html(0);
                                }
                            });
                        }
                        break;
                    case "crf":
                        options.voname = "gmCRMCRFVO";
                        options.url = "crf/list";

                        if (_.contains(GM.Global.Admin, "crfappr")) {
                            input.apprpartyid = localStorage.getItem("partyID");
                            input.statusid = "105207";
                            if (GM.Global.Device) {
                                var setList = {};
                                setList.crfstatus = "105207";
                                setList.crf = "Y";
                                fnGetListActivity(setList, "", function (data) {
                                    console.log("fnGetPendingcrf" + JSON.stringify(data));
                                    if (data.length) {
                                        data = $.parseJSON(JSON.stringify(data));
                                        _.each(data, function (a, i) {
                                            a["module"] = "crf";
                                            a["crf"] = "Y";
                                            a["id"] = a.actid + "/" + a.partyid;
                                            a["title"] = a.actnm;
                                            a["index"] = i + 1;
                                            if ((i + 1) == data.length) {
                                                console.log(data)
                                                that.$("#div-" + options.module + "-dash-content").html(that.template_dashwidget(data));
                                                $(".OfflineLinkRemover").addClass("hide");
                                                $(".OfflineLink").removeClass("hide");
                                                $("#div-" + options.module + "-db-count").html(data.length);
                                                resizeFont();
                                            }
                                        });
                                    } else {
                                        that.$("#div-" + options.module + "-dash-content").html("<div class = 'gmTextAll gmTextCenter gmPaddingTop10p'>No Data Found</div>");
                                        $("#div-" + options.module + "-db-count").html(0);
                                    }
                                });
                            }

                        } else {
                            input.statusid = "105207,105206,105205,105208";
                            input.createdby = localStorage.getItem("userID");
                            if (GM.Global.Device) {
                                var setList = {};
                                setList.crfstatus = "105207,105206,105205,105208";
                                setList.crf = "Y";
                                fnGetListActivity(setList, "", function (data) {
                                    console.log("fnGetAllcrf" + JSON.stringify(data));
                                    if (data.length) {
                                        data = $.parseJSON(JSON.stringify(data));
                                        var userData = _.filter(data, function (dt) {
                                            return dt.createdby == userid;
                                        });

                                        if (userData.length) {
                                            data = userData;

                                            _.each(data, function (a, i) {
                                                a["module"] = "crf";
                                                a["crf"] = "Y";
                                                a["id"] = a.actid + "/" + a.partyid;
                                                a["title"] = a.actnm;
                                                a["index"] = i + 1;
                                                if ((i + 1) == data.length) {
                                                    console.log(data)
                                                    that.$("#div-" + options.module + "-dash-content").html(that.template_dashwidget(data));
                                                    $(".OfflineLinkRemover").addClass("hide");
                                                    $(".OfflineLink").removeClass("hide");
                                                    $("#div-" + options.module + "-db-count").html(data.length);
                                                    resizeFont();
                                                }
                                            });
                                        } else {
                                            that.$("#div-" + options.module + "-dash-content").html("<div class = 'gmTextAll gmTextCenter gmPaddingTop10p'>No Data Found</div>");
                                            $("#div-" + options.module + "-db-count").html(0);
                                        }
                                    } else {
                                        that.$("#div-" + options.module + "-dash-content").html("<div class = 'gmTextAll gmTextCenter gmPaddingTop10p'>No Data Found</div>");
                                        $("#div-" + options.module + "-db-count").html(0);
                                    }
                                });
                            }

                        }
                        break;
                    case "crf-pending":
                        options.voname = "gmCRMCRFVO";
                        options.url = "crf/list";
                        input.statusid = "105207";
                        input.createdby = localStorage.getItem("userID");
                        if (GM.Global.Device) {
                            var setList = {};
                            setList.crfstatus = "105207";
                            setList.crf = "Y";
                            fnGetListActivity(setList, "", function (data) {
                                console.log("fnGetPendingcrf" + JSON.stringify(data));
                                if (data.length) {
                                    data = $.parseJSON(JSON.stringify(data));
                                    _.each(data, function (a, i) {
                                        a["module"] = "crf";
                                        a["crf"] = "Y";
                                        a["id"] = a.actid + "/" + a.partyid;
                                        a["title"] = a.actnm;
                                        a["index"] = i + 1;
                                        if ((i + 1) == data.length) {
                                            console.log(data)
                                            that.$("#div-" + options.module + "-dash-content").html(that.template_dashwidget(data));
                                            $(".OfflineLinkRemover").addClass("hide");
                                            $(".OfflineLink").removeClass("hide");
                                            $("#div-" + options.module + "-db-count").html(data.length);
                                            resizeFont();
                                        }
                                    });
                                } else {
                                    that.$("#div-" + options.module + "-dash-content").html("<div class = 'gmTextAll gmTextCenter gmPaddingTop10p'>No Data Found</div>");
                                    $("#div-" + options.module + "-db-count").html(0);
                                }
                            });
                        }
                        break;
                    case "crf-reviewed":
                        options.voname = "gmCRMCRFVO";
                        options.url = "crf/list";
                        input.statusid = "105206";
                        input.createdby = localStorage.getItem("userID");
                        if (GM.Global.Device) {
                            var setList = {};
                            setList.crfstatus = "105206";
                            setList.crf = "Y";
                            fnGetListActivity(setList, "", function (data) {
                                console.log("fnGetReviewedcrf" + JSON.stringify(data));
                                if (data.length) {
                                    data = $.parseJSON(JSON.stringify(data));
                                    _.each(data, function (a, i) {
                                        a["module"] = "crf";
                                        a["crf"] = "Y";
                                        a["id"] = a.actid + "/" + a.partyid;
                                        a["title"] = a.actnm;
                                        a["index"] = i + 1;
                                        if ((i + 1) == data.length) {
                                            console.log(data)
                                            that.$("#div-" + options.module + "-dash-content").html(that.template_dashwidget(data));
                                            $(".OfflineLinkRemover").addClass("hide");
                                            $(".OfflineLink").removeClass("hide");
                                            $("#div-" + options.module + "-db-count").html(data.length);
                                            resizeFont();
                                        }
                                    });
                                } else {
                                    that.$("#div-" + options.module + "-dash-content").html("<div class = 'gmTextAll gmTextCenter gmPaddingTop10p'>No Data Found</div>");
                                    $("#div-" + options.module + "-db-count").html(0);
                                }
                            });
                        }

                        break;
                    case "fieldsales":

                        input.repid = localStorage.getItem("userID");
                        input.regid = "y";
                        if (GM.Global.Device) {
                            fnGetDashFS(function (data) {
                                if (data.length) {
                                    that.$("#div-" + options.module + "-dash-content").html(that.template_dashwidget(data));
                                    $("#div-" + options.module + "-db-count").html(data.length);
                                    resizeFont();
                                } else {
                                    that.$("#div-" + options.module + "-dash-content").html("<div class = 'gmTextAll gmTextCenter gmPaddingTop10p'>No Data Found</div>");
                                    $("#div-" + options.module + "-db-count").html(0);
                                }
                            });
                        }
                        break;
                    case "preceptorship":
                        
                         that.psHostCaseAdmin = _.findWhere(GM.Global.UserAccess, {
                             funcnm: "PRECEPTOR-ADMIN"
                         });
                         if (that.psHostCaseAdmin == undefined) {
                                $("#upcoming-host-case").addClass("hide");
                         }
                         that.$("#div-" + options.module + "-dash-content").append("<div class = 'gmTextAll gmTextCenter gmPaddingTop10p'>No Data Found</div>");
                        
                        
                            var input1 = {
                                "actstatusid": "105765,105766",
                                "actcategory": "105272",
                                "actstartdate": this.today
                            };
                            var input2 = {
                                "actstatusid": "105765",
                                "actcategory": "105273",
                                "actstartdate": ''
                            };
                            that.upcomingData = "";
                            that.caseRqstData = "";
                            
                            fnGetWebServerData("psrpt/listhost", "undefined", input1, function (data) {
                                // console.log(JSON.stringify(data));
                                if (data != null) {
                                    _.each(data,function(dt,i){
                                        dt.upcoming = "Y";
                                        dt.index = i + 1;
                                        that.$("#div-" + options.module + "-dash-content").append(that.template_preceptorDashList(dt));
                                    });
                                    that.upcomingData = "Y";
                                    if (that.upcomingData == "Y") {
                                        $("#div-preceptorship-dash-content .gmTextAll.gmTextCenter").css('display', 'none');
                                    } else {
                                        $("#div-preceptorship-dash-content .gmTextAll.gmTextCenter").css('display', 'block');
                                    }
                                }

                            });
                        
                            fnGetWebServerData("psrpt/listcaserequest", undefined, input2, function (data) {
//                                 console.log(JSON.stringify(data));
                                if (data != null) {
                                     _.each(data,function(dt,i){
                                        dt.caserqst = "Y";
                                        dt.index = i + 1;
                                        that.$("#div-" + options.module + "-dash-content").append(that.template_preceptorDashList(dt));
                                    });
                                    that.caseRqstData = "Y";
                                    if (that.psHostCaseAdmin == undefined) {
                                        if (that.caseRqstData == "Y") {
                                            $("#div-preceptorship-dash-content .gmTextAll.gmTextCenter").css('display', 'none');
                                        } else {
                                            $("#div-preceptorship-dash-content .gmTextAll.gmTextCenter").css('display', 'block');
                                        }
                                        $("#open-case-request").addClass("gmFontWhite gmBGDarkerGrey");
                                    }
                                    else{
                                        $(".dashCaseReqst").removeClass("showEl");
                                    }
                                 }
                            });
                        
                            
                           break;
                    case "tradeshow":
                        input.divisionid = localStorage.getItem("userDivision");
                        break;
                    case "quickreference":
                        break;
                }


                console.log(JSON.stringify(input));
                console.log(options.url);
                console.log(options.voname);
                console.log("MODULEEEE" + options.module);
                if((options.module != "preceptorship")){
                    if (!GM.Global.Device)
                        fnGetWebServerData(options.url, options.voname, input, function (data) {

                            if (options.url == "crmactivity/info" && data != undefined)
                                data = data.GmCRMListVO.gmCRMActivityVO;

                            if (data != null) {
                                if (options.module == "fieldsales") {
                                    data = arrayOf(data.gmCRMSalesRepListVO);

                                } else {
                                    data = arrayOf(data);
                                }

                                //                    if ((options.module == "crf-pending" || options.module == "crf-reviewed" || options.module == "crf") && !_.contains(GM.Global.Admin, "crfappr") && _.contains(GM.Global.Admin, "crf")) {
                                //                        
                                //                        var userData = _.filter(data, function(dt){
                                //                            return dt.createdby == userid;
                                //                        });
                                //                        console.log(userData)
                                //                        if(userData != "") {
                                //                            data = arrayOf(userData);
                                //                        }
                                //                    }
                                var req = 0,
                                    upc = 0;
                                _.each(data, function (data, i) {
                                    if (data.picurl != undefined && data.picurl != "") {
                                        data["server"] = URL_CRMFileServer;
                                        data.picurl = data.picurl + "?num=" + Math.random()
                                    }


                                    if (options.module == "lead") {
                                        data["module"] = "lead";
                                        data["lead"] = "Y";
                                        data["index"] = i + 1;
                                        data["gendate"] = data.actstartdate;

                                        if (data.gendate == "")
                                            data.gendate = "N/A";
                                        else {
                                            data.gendate = getDateByFormat(data.gendate);
                                        }

                                    }
                                    if (options.module == "training") {

                                        data["module"] = "training";
                                        data["trainD"] = "Y";
                                        data["index"] = i + 1;

                                        if (data.location == "")
                                            data.location = "N/A";
                                        else {
                                            if (data.statenm != "" && data.zip != "")
                                                data.location = data.location + ", " + data.statenm + ", " + data.zip;
                                            else if (data.statenm != "")
                                                data.location = data.location + ", " + data.statenm;
                                            else if (data.statenm != "")
                                                data.location = data.location + ", " + data.zip;
                                            else
                                                data.location = data.location;
                                        }
                                        data["id"] = data.actid;
                                        data["date"] = data.actstartdate;


                                        if (data.date == "")
                                            data.date = "N/A";
                                        else {
                                            data.date = getDateByFormat(data.date);
                                        }

                                        data["topic"] = data.actnm;
                                    }

                                    if (options.module == "physicianvisit") {
                                        data["module"] = "physicianvisit";
                                        data["id"] = data.actid;
                                        data["date"] = data.actstartdate;


                                        if (data.date == "")
                                            data.date = "N/A";
                                        else {
                                            data.date = getDateByFormat(data.date);
                                        }

                                        data["title"] = data.phynm;
                                        data["type"] = data.acttype;

                                        if (data.type == "VIP Only")
                                            data.type = "vip";
                                        else if (data.type == "Research")
                                            data.type = "research";
                                        else if (data.type == "Design")
                                            data.type = "design";
                                        else if (data.type == "VIP Visit + Lab")
                                            data.type = "viplab";
                                        else if (data.type == "Lab Only")
                                            data.type = "lab";
                                        data[data.type] = "Y";

                                        data["phy"] = "Y";
                                        //                        data["index"] = i + 1;
                                        if (data.actstatus == "Requested") {
                                            req++;
                                            data["index"] = req;
                                        } else if (data.actstatus == "Scheduled") {
                                            upc++;
                                            data["index"] = upc;
                                        }

                                    }

                                    if (options.module == "surgeon") {

                                        data["module"] = "surgeon";
                                        data.surgeon = "Y";

                                        if (_.contains(GM.Global.Admin, "surgeon"))
                                            data["merc"] = "Y";
                                        if (_.contains(GM.Global.Admin, "physicianvisit") || _.contains(GM.Global.Admin, "physvisitrqst"))
                                            data["phy"] = "Y";
                                        if (_.contains(GM.Global.Admin, "salescall"))
                                            data["scall"] = "Y";

                                        if (data.address == "")
                                            data.address = "N/A";
                                        data["id"] = data.partyid;
                                        if (data.account == "")
                                            data.account = "N/A";

                                        data["title"] = parseNull(data.salutenm) + " " + data.firstnm + " " + parseNull(data.midinitial) + " " + data.lastnm;
                                        data["pic"] = "Y";

                                        data = arrayOf(data);
                                    }

                                    if (options.module == "salescall") {

                                        if (data.picurl != "")
                                            data["salespic"] = "Y";

                                        data["module"] = "salescall";
                                        data["id"] = data.actid;
                                        data["date"] = data.actstartdate;

                                        if (data.date == "")
                                            data.date = "N/A";
                                        else {
                                            data.date = getDateByFormat(data.date);
                                        }
                                        data["title"] = data.phynm;

                                        data["pic"] = "Y";
                                        data["salescall"] = "Y";
                                    }

                                    if (options.module == "merc") {

                                        data["module"] = "merc";
                                        data["mercD"] = "Y";
                                        data["index"] = i + 1;
                                    }
                                    if (options.module == "crf") {
                                        data["module"] = "crf";
                                        data["crf"] = "Y";
                                        data["id"] = data.actid + "/" + data.partyid;
                                        data["title"] = data.actnm;
                                        data["index"] = i + 1;
                                    }
                                    if (options.module == "crf-pending") {
                                        data["module"] = "crf";
                                        data["crf"] = "Y";
                                        data["id"] = data.actid + "/" + data.partyid;
                                        data["title"] = data.actnm;
                                        data["index"] = i + 1;
                                    }
                                    if (options.module == "crf-reviewed") {
                                        data["module"] = "crf";
                                        data["crf"] = "Y";
                                        data["id"] = data.actid + "/" + data.partyid;
                                        data["title"] = data.actnm;
                                        data["index"] = i + 1;
                                    }

                                    if (options.module == "fieldsales") {

                                        data["module"] = "fieldsales";
                                        data["id"] = data.regid;
                                        data["title"] = data.regnm;
                                        data["fieldsales"] = "Y";
                                        data["index"] = i + 1;
                                    }
                                    if (options.module == "tradeshow") {
                                        data["module"] = "tradeshow";
                                        data["tradeshow"] = "Y";
                                        data["index"] = i + 1;    
                                    }
                                    if (options.module == "quickreference") {
                                        data["module"] = "quickreference";
                                        data["quickreference"] = "Y";
                                        data["index"] = i + 1;
                                    }
                                });

                                
                                    that.$("#div-" + options.module + "-dash-content").html(that.template_dashwidget(data));
                                if (options.module == "merc") {
                                     $(".merc-dash-link").unbind('click').bind('click', function (e) {
                                         var mercid=$(e.currentTarget).data("id");
                                        that.showMercPDF(mercid);
                                     });
                                     $(".merc-event-link").unbind('click').bind('click', function (e) {
                                         var merclink=$(e.currentTarget).attr("href");
                                         window.open(merclink, '_blank', 'location=no');
                                     });
                                }
                                if (options.module == "quickreference") {
                                    $(".refDashItem a").unbind('click').bind('click', function (e) {
                                        var refDashLink = $(e.currentTarget).data("href");
                                        window.open(refDashLink, '_blank', 'location=no');
                                    });
                                }
                                if (options.module == "physicianvisit") {
                                    $(".div-Requested").addClass("hide");
                                    $("#div-phy-req span").text(req);
                                    $("#div-phy-upc span").text(upc);
                                }
                                $("#div-" + options.module + "-db-count").html(data.length);
                                resizeFont();

                            } else {
                                that.$("#div-" + options.module + "-dash-content").html("<div class = 'gmTextAll gmTextCenter gmPaddingTop10p'>No Data Found</div>");
                                $("#div-" + options.module + "-db-count").html(0);
                                if (options.module == "physicianvisit") {
                                    $("#div-phy-req span").text(0);
                                    $("#div-phy-upc span").text(0);
                                }
                            }
                        });
                }

                var visible = $('#div-dash-phy-header').is(':visible');
                if (visible)
                    $("#div-physicianvisit-dash-content").css({
                        "height": "200px"
                    });
            }, 
            //Load Spinner
            getLoader: function (divElem) {
                var template = fnGetTemplate(URL_Common_Template, "gmtSpinner");
                $(divElem).html(template());
            },
            
            fetchTradeShowAttendeeRpt: function (e) {
                var that = this;
                var input = {};
                this.attendeemail="";
                input.token = localStorage.getItem("token");
                input.tradeid = $(e.currentTarget).data("id");
                input.status = $(e.currentTarget).data("status");
                var tradenm = $(e.currentTarget).data("tradenm");
                this.closedStatus = "";
                      this.tradeShow = {
                            tradeid: $(e.currentTarget).data("id"),
                            tradenm: $(e.currentTarget).data("tradenm"),
                            city: $(e.currentTarget).data("city"),
                    state: $(e.currentTarget).data("state"),
                    country: $(e.currentTarget).data("country"),
                    startdt: $(e.currentTarget).data("startdt"),
                    enddt: $(e.currentTarget).data("enddt"),
                    wrklink: $(e.currentTarget).data("wrklink"),
                    demolink: $(e.currentTarget).data("demolink"),
                    notes: $(e.currentTarget).data("notes"),
                    acronym: $(e.currentTarget).data("acronym"),
                }
                    showPopup();
                    $("#div-crm-overlay-content-container").addClass("tradeShowPop");
                    $(".tradeShowPop .modal-header").append('<button type="button" class="invitesurgeonBtn hide">Invite a Surgeon</button>');
                    $(".modal-footer").addClass("hide");
                    $(".modal-header .modal-title").text(tradenm);
                    $(".modal-header .invitesurgeonBtn").removeClass("hide"); 
                    $(".modal-body").html("<div id='dash-attendee-report-list'></div>");
                    that.getLoader("#dash-attendee-report-list");
                fnGetWebServerData("tradeshow/attendeerpt", undefined, input, function (data) {
                    console.log(JSON.stringify(data));
                    if (!data == "" || !data == undefined || !data == null) {
                        var rows = [],
                            i = 0;
                        _.each(data, function (griddt) {
                            rows[i] = {};
                            rows[i].id = i;
                            rows[i].data = [];
                                if (parseNull(griddt.email) != '')
                                rows[i].data[0] = '<i class="attendeeMail fa fa-envelope-o">';
                            else
                                rows[i].data[0] = '';
                            rows[i].data[1] = griddt.firstnm + " " + griddt.lastnm;
                            rows[i].data[2] = griddt.email;
                            rows[i].data[3] = griddt.phone;
                            rows[i].data[4] = griddt.company;
                            rows[i].data[5] = griddt.city;
                            rows[i].data[6] = griddt.state;
                            if (griddt.status == '109044') {
                                rows[i].data[7] = griddt.sysnotes;
                                rows[i].data[8] = griddt.notes;
                                that.closedStatus = "Y";
                            }

                            i++;
                        });
                        var dataAttendeeRpt = {};
                        dataAttendeeRpt.rows = rows;
                        if (that.closedStatus != "Y") {
                            var setInitWidths = "50,150,227,150,150,130,120";
                            var setColAlign = "center,left,left,left,left,left,left";
                            var setColTypes = "ro,ro,ro,ro,ro,ro,ro";
                            var setColSorting = "str,str,str,str,str,str,str";
                            var setHeader = ["", "Name", "Email Id", "Phone No", "Company", "City", "State"];
                            var setFilter = ["", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#select_filter", "#select_filter"];
                            var enableTooltips = "false,true,true,true,true,true,true";
                        } else {
                            var setInitWidths = "50,150,227,150,150,150,100,180,150";
                            var setColAlign = "center,left,left,left,left,left,left,left,left";
                            var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro";
                            var setColSorting = "str,str,str,str,str,str,str,str,str";
                            var setHeader = ["","Name", "Email Id", "Phone No","Company", "City", "State", "System Notes", "Notes"];
                            var setFilter = ["","#text_filter", "#text_filter", "#text_filter", "#text_filter", "#select_filter", "#select_filter", "#text_filter", "#text_filter"];
                            var enableTooltips = "false,true,true,true,true,true,true,true,true";
                        }
                        var footerArry = [];
                        var gridHeight = "";
                        var footerStyles = [];
                        var footerExportFL = false;
                        var rptTemplate = "";
                        var mobResArg = true;
                        that.AttendeeGridObj = loadDHTMLXGrid('dash-attendee-report-list', gridHeight, dataAttendeeRpt, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL, rptTemplate, mobResArg);
                         
                        that.AttendeeGridObj.attachEvent("onRowSelect", function (id, index) {
                             
                            if (index == 0) {
                                that.attendeemail = that.AttendeeGridObj.cells(id, 2).getValue();
                                 var tradeinfo = JSON.stringify(that.tradeShow);
                                var tradeobj = JSON.parse(tradeinfo);
                                that.eventObj(tradeobj);
                                console.log(tempJSON);
                                if((tempJSON.attendeeemail)!= ''){
                                 that.attendeepopup(tempJSON);
                                }
                                console.log("tradeshow" + JSON.stringify(that.tradeShow));
                            }
                            });
                            $(".invitesurgeonBtn").on("click", function () {
                                that.attendeemail ="";
                                var tradeinfo = JSON.stringify(that.tradeShow);
                                var tradeobj = JSON.parse(tradeinfo);
                                that.eventObj(tradeobj);
                                that.attendeepopup(tempJSON);
                            });
                    } 
                    
                    else {
                        $("#dash-attendee-report-list").html("No data found");
                    }

                });
              $(".tradeShowPop .close").on("click", function () {
                  $(this).closest(".tradeShowPop").find(".modal-footer").removeClass("hide");
                  $(this).closest(".tradeShowPop").find(".modal-header .modal-title").text("");
                  $(this).closest(".tradeShowPop").find(".modal-body").html("");
                  $(".tradeShowPop .modal-header .invitesurgeonBtn").remove();
                  $(this).closest("#div-crm-overlay-content-container").removeClass("tradeShowPop");

              });
            },
            
            eventObj: function (tradeobj) {
                var that = this;
 		var eventLocation = that.getEvntLocation(tradeobj);  
                console.log("Fianl eventLocation=>"+eventLocation);
                tempJSON = {
                    attendeeemail: that.attendeemail,
                    evntNM: tradeobj.tradenm,
                    evntSD: tradeobj.startdt,
                    evntED: tradeobj.enddt,
                    evntLoc : eventLocation,
                    evntWlink: tradeobj.wrklink,
                    evntDlink: tradeobj.demolink,
                    evntNotes: tradeobj.notes,
                    evntAcronym: tradeobj.acronym,
                }
            },
             attendeepopup: function (tempJSON) {
                 var that = this;
                 var emailPattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
                 
                 that.showPopupemail();
                 $("#div-crm-overlay-content-container2 .modal-body").append(that.template_attendeemail(tempJSON));
                 
                 $("#mailresult").on("click", function () {
                     var mailid = $("#attendeemail").val();
                     var sendFl = "";
                      if (mailid == '' || mailid == undefined){
                         $("#email-validation-msg-bar").removeClass("hide");
                         $("#email-validation-msg-bar").html("Please enter valid email id").css({
                        "background-color": "#700",
                        "padding": "10px",
                        "color": "#fff"
                    }).slideDown(500).fadeOut(5000); 
                    }  
                     else if(emailPattern.test(mailid)!=true) {
                         sendFl = "N";
                        $("#email-validation-msg-bar").removeClass("hide");
                         $("#email-validation-msg-bar").html("Please enter valid email id").css({
                        "background-color": "#700",
                        "padding": "10px",
                        "color": "#fff"
                    }).slideDown(500).fadeOut(5000); 
                     } else {
                         sendFl = "Y";
                     }
                     if (sendFl == "Y") {
                         var input = {};
                         input.msg = $("#strMailMsg").val(),
                             input.tradeinfo = $("#tradeInfo").html().replace(/\n/g, ""),
                             input.attendeemail = mailid,
			     input.subject = $("#attendeesub").val(),
                               //PC-2939
                             input.userid = localStorage.getItem("userID");
                             console.log("mailresult:" + JSON.stringify(input));
                             fnGetWebServerData("tradeshow/attendeemail", "gmCRMAttendeeMailVO", input, function (data) {
                              that.showinfoMsg(data);
                             console.log("data" + JSON.stringify(data));
                         });
                     }
                   });
                 },
            showMercPDF: function (refid) {
                var input = {
                    "token": localStorage.getItem("token"),
                    "refid": refid,
                    "reftype": "NULL",
                    "refgroup": "105265"
                };
                console.log(input);
                fnGetWebServerData("file/get", "gmCRMFileUploadVO", input, function (fileData) {
                    console.log(fileData);
                    if (fileData != undefined && fileData.length > 0) {
                        console.log(fileData);
                        var pdfPath = URL_CRMFileServer + fileData[0].refgroup + "/" + fileData[0].refid + "/" + fileData[0].filename;
                        window.open(pdfPath, '_blank', 'location=no');
                    } else {
                        dhtmlx.alert("More Information Coming Soon");

                    }

                });
            },
            
           

            setTabModule: function (e) {
                GM.Global.Module = $(e.currentTarget).attr("data-attr");
            },

            goOfflineMsg: function () {
                showError("Network Connection is required for CRF")
            },

            // to switch physician visit status tabs
            togglePhy: function (e) {
                var id = e.currentTarget.id;
                if (id == "div-phy-req" && $("#div-phy-req span").text().length > 0) {
                    GM.Global.Status.listtype = "Requested";
                    $(".div-Requested").removeClass("hide");
                    $(".div-Scheduled").addClass("hide");
                    $("#div-phy-req").addClass("gmFontWhite gmBGDarkerGrey").removeClass("gmFontBlack gmBGLGrey");
                    $("#div-phy-upc").removeClass("gmFontWhite gmBGDarkerGrey").addClass("gmFontBlack gmBGLGrey");
                } else if (id == "div-phy-upc" && $("#div-phy-upc span").text().length > 0) {
                    GM.Global.Status.listtype = "Scheduled";
                    $(".div-Requested").addClass("hide");
                    $(".div-Scheduled").removeClass("hide");
                    $("#div-phy-upc").addClass("gmFontWhite gmBGDarkerGrey").removeClass("gmFontBlack gmBGLGrey");
                    $("#div-phy-req").removeClass("gmFontWhite gmBGDarkerGrey").addClass("gmFontBlack gmBGLGrey");
                }


            },
             togglePS: function (e) {
                 var that = this;
                var id = e.currentTarget.id;
                if (id == "upcoming-host-case" && $("#upcoming-host-case span").text().length > 0) {
                    $(".dashUpComgHost").addClass("showEl");
                    $(".dashCaseReqst").removeClass("showEl");
                    $("#upcoming-host-case").addClass("gmFontWhite gmBGDarkerGrey").removeClass("gmFontBlack gmBGLGrey");
                    $("#open-case-request").removeClass("gmFontWhite gmBGDarkerGrey").addClass("gmFontBlack gmBGLGrey");
                    if (that.upcomingData == "Y") {
                        $("#div-preceptorship-dash-content .gmTextAll.gmTextCenter").css('display', 'none');
                    } else {
                        $("#div-preceptorship-dash-content .gmTextAll.gmTextCenter").css('display', 'block');
                    }
                           
                } else if (id == "open-case-request" && $("#open-case-request span").text().length > 0) {
                    $(".dashCaseReqst").addClass("showEl");
                    $(".dashUpComgHost").removeClass("showEl");
                    $("#open-case-request").addClass("gmFontWhite gmBGDarkerGrey").removeClass("gmFontBlack gmBGLGrey");
                    $("#upcoming-host-case").removeClass("gmFontWhite gmBGDarkerGrey").addClass("gmFontBlack gmBGLGrey");
                    if (that.caseRqstData == "Y") {
                        $("#div-preceptorship-dash-content .gmTextAll.gmTextCenter").css('display', 'none');
                    } else {
                        $("#div-preceptorship-dash-content .gmTextAll.gmTextCenter").css('display', 'block');
                    }
                }

            },
            
            
             showDashboardCount: function (input) {
            	// input for Upcoming Host case. Input will be override in resource to get count for Open Host case and Open Case request
                var input = {
                    "actstatusid": "105765,105766",
                    "actcategory": "105272",
                    "actstartdate": this.today
                };
                //  getLoader("#open-host-case");
                fnGetWebServerData("psrpt/dashcnt", undefined, input, function (data) {
                    var count = 0;
                    if (!data == "" || !data == undefined || !data == null) {
                        $("#span-ps-host-upc-count").html(data.UPCOMEHOST);
                        $("#span-ps-casereq-open-count").html(data.OPENCASEREQ);
                    } else {
                    	$("#span-ps-host-upc-count").html("0");
                        $("#span-ps-casereq-open-count").html("0");
                    }
                });
            },
            
              toggleDashModule: function (event) {
                  var that = this;
                  var widthWin = $(window).width();
                  if (widthWin <= 480) {
                      var currentid = $(event.currentTarget).parent().parent().attr("id");
                      if ($(event.currentTarget).parent().parent().hasClass('enable')) {
                          $(event.currentTarget).parent().parent().removeClass('enable');
                          $(event.currentTarget).parent().parent().children('div').css('display', 'none');
                      } else {
                          $('#div-main-content').children().each(function () {
                              var divid = $(this).attr('id');
                              if (divid != currentid) {
                                  $(this).removeClass('enable');
                                  $(this).children('div').css('display', 'none');
                              } else {
                                  $(this).addClass('enable');
                                  $(this).children('div').css('display', 'block');
                              }
                          });
                          var checkShowPSTab = $("#upcoming-host-case").hasClass("hide");
                          if (checkShowPSTab == true) {
                              $("#open-case-request").trigger("click");
                          }
                          var checkActivePSTab = $("#upcoming-host-case").hasClass("gmBGDarkerGrey");
                          if (checkActivePSTab == true) {
                              if (that.upcomingData == "Y") {
                                  $("#div-preceptorship-dash-content .gmTextAll.gmTextCenter").css('display', 'none');
                              } else {
                                  $("#div-preceptorship-dash-content .gmTextAll.gmTextCenter").css('display', 'block');
                              }
                          } else {
                              if (that.caseRqstData == "Y") {
                                  $("#div-preceptorship-dash-content .gmTextAll.gmTextCenter").css('display', 'none');
                              } else {
                                  $("#div-preceptorship-dash-content .gmTextAll.gmTextCenter").css('display', 'block');
                              }
                          }
                      }

                  }
              },
	      
	       toggleSalescall: function(){
            if( $('#div-salescall-dash-content').css('display') == 'block' ) {
                    $('#div-salescall-dash-content').css('display','none');
                }
                else{
                    $('#div-salescall-dash-content').css('display','block');  
                }
              
            },
            
            showinfoMsg: function(data){
                $("#email-info-msg-bar").removeClass("hide");
                if(data!= null){
                   $("#email-info-msg-bar").html("Mail Sent Successfully...").css({
                        "background-color": "green",
                        "padding": "10px",
                        "color": "#fff"
                    }).slideDown(500).fadeOut(5000); 
                       setTimeout(function(){
                         $('#div-crm-overlay-content-container2 .close').trigger("click");
                      }, 5000);
                   }
                else{
                    $("#email-info-msg-bar").html("Mail Sent Failed...").css({
                        "background-color": "#700",
                        "padding": "10px",
                        "color": "#fff"
                    }).slideDown(500).fadeOut(5000);
                }
            },
            
            showPopupemail: function () {
                 var that = this;
                 $('.div-overlay').show();
                 $("#div-crm-overlay-content-container2").addClass("tradeemailShowPop");
                 $('.tradeemailShowPop').find('.modal-body').empty();
                 $("#div-crm-overlay-content-container2").removeClass("hide");
                 $(".modal-footer").addClass("hide");
                 $('#div-crm-overlay-content-container2 button[data-dismiss="modal"]').each(function () {
                     $(this).unbind('click').bind('click', function () {
                         $('#div-crm-overlay-content-container2').addClass('hide');
                         $("#div-crm-overlay-content-container2").removeClass("tradeemailShowPop");
                         $('.hideDefaultBtn').addClass('hide');
                     });
                 });
             },
            
	getEvntLocation: function (evntradeobj) {
                var tradeLocation = '';
                if (evntradeobj.city != '') {
                    tradeLocation += evntradeobj.city + ', ';
                }
                if (evntradeobj.state != '') {
                    tradeLocation += evntradeobj.state + ', ';
                }
                if (evntradeobj.country != '') {
                    tradeLocation += evntradeobj.country;
                }
                tradeLocation = tradeLocation.replace(/, \s*$/, "");
                
                return tradeLocation;

            },
        });

        return GM.View.CRMDashboard;
    });
