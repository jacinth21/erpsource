define([
    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars', 'jqueryui',

    // Common
   'global', 'commonutil', 'gmvSearch', 'gmvValidation', 'gmvCRMAddress',

    // View
    'gmvCRMData', 'gmvCRMSelectPopup',
    
    //Model
    'gmmActivity',

    // Pre-Compiled Template
    'gmtTemplate'
],
    function ($, _, Backbone, Handlebars, JqueryUI,
        Global, CommonUtil, GMVSearch, GMVValidation, GMVCRMAddress,
        GMVCRMData, GMVCRMSelectPopup, GMMActivity, GMTTemplate) {

        'use strict';

        GM.View.CRFDetail = Backbone.View.extend({

            el: "#div-crm-module",

            name: "CRM CRF Detail View",
            
            /* Load the templates */
            template_MainModule: fnGetTemplate(URL_Common_Template, "gmtCRMMainModule"),
            template_CRFDetailMain: fnGetTemplate(URL_CRF_Template, "gmtCRFDetailMain"),
            template_CRFDetailContent: fnGetTemplate(URL_CRF_Template, "gmtCRFDetailContent"),
            template: fnGetTemplate(URL_CRF_Template, "gmtCRFEditMain"),
            template_request: fnGetTemplate(URL_CRF_Template, "gmtCRFRequestDetails"),
            template_description: fnGetTemplate(URL_CRF_Template, "gmtCRFServiceDescription"),
            template_qualification: fnGetTemplate(URL_CRF_Template, "gmtCRFHCPQualification"),
            template_hours: fnGetTemplate(URL_CRF_Template, "gmtCRFHoursTime"),
            template_internaluse: fnGetTemplate(URL_CRF_Template, "gmtCRFInternalUse"),
            template_approval: fnGetTemplate(URL_CRF_Template, "gmtCRFApproval"),
            template_approval_list: fnGetTemplate(URL_CRF_Template, "gmtCRFApprovalList"),
            template_feedback: fnGetTemplate(URL_CRF_Template, "gmtCRFFeedback"),
            template_feedback_list: fnGetTemplate(URL_CRF_Template, "gmtCRFFeedbackList"),
            template_merc: fnGetTemplate(URL_MERC_Template, "gmtMERCDetail"),
            template_physician: fnGetTemplate(URL_PhysicianVisit_Template, "gmtPhysicianVisitDetail"),
            template_training: fnGetTemplate(URL_Training_Template, "gmtTrainingDetail"),
            

            events: {
                  "click #li-edit": "editDetails",
                  "click .crf-tabs": "toggleTabs",
                  "click #li-save": "saveDetail",
                  "click #li-cancel": "resetDetail",
                  "click #span-crf-service-desc-comments-clear, #span-crf-hcp-qualify-comments-clear": "cleartxt",
                  "keyup #txt-crf-service-desc-comments": "updateDesc",
                  "keyup #txt-crf-hcp-qualify-comments": "updateNotes",
                  "keyup #txt-crf-service-desc-comments, #txt-crf-hcp-qualify-comments": "getComments",
                  "change input, select, textarea": "updateJson",
                  "keyup input": "updateJson",
                  "keyup #txt-crf-feedback-comments": "getFeedback",
                  "click #span-crf-feedback-comments-cancel": "clearComments",
                  "click .delete-comments": "deleteComments",
                  "click .edit-comments": "editComments",
                  "click .btn-crf-approve": "approveCRF",
                  "click .btn-crf-reject": "rejectCRF",
                  "click #div-crf-details-requestdetail": "toogleRequest",
                  "click #div-crf-details-servicedes": "toogleDescription",
                  "click #div-crf-details-hourstim": "toogleTime",
                  "click #div-crf-details-approval": "toogleApprover",
                  "click #div-crf-details-feedbac": "toogleFeedback",
                  "click .crf-tab": "togglePhoneDisplay",
//                  "click .crf-tabs-edit": "togglePhoneDisplayEdit",
                  "click #crf-send-review": "sendReview",
                  "click #crf-send-approval": "sendApproval",
                  "click .open-crf-doc": "openDoc",
            },

            initialize: function (options) {
               GM.Global["Crf"]=this.options;
                hideMessages();

                this.actid = options.controlid;
                this.partyid = options.partyid;
                
                this.strOptions = GM.Global.Crf;
                this.deleteformfile = new Array();
                this.arrgcrformfilefattrvo = new Array();
                this.crfdata = {};
                this.updatedfeedback = false;
                this.deletedComments = new Array();
                this.crffeedbackdata = new Array();
                this.arrgmcrmlogvo = new Array();
                this.arrgmcrfapprovalvo = new Array();
                this.approvaldata = new Array();
                this.acttype = "";
                this.arrgmcrmphysicianparticipantsvo = new Array();
                this.arrgmcrmphysicianprojectsvo = new Array();
                this.phydata = new Array();
                GM.Global.WorkNo = "";
                
            },
                

            render: function () {
                var that = this;
                $(window).on('orientationchange', this.onOrientationChange);
                this.$el.html(this.template_MainModule({ "module": "crf" }));
                
                if (_.contains(GM.Global.Admin, "crf"))
                    $(".gmBtnGroup #li-edit").removeClass("hide");
                $("#span-app-title").html("CRF")
                var token = localStorage.getItem("token");
                
                var actInput = {"token": token,"actid": this.actid};
                fnGetWebServerData("crmactivity/info", "gmCRMActivityVO", actInput, function (data) {
                    if(data!=null) {
                        data = data.GmCRMListVO.gmCRMActivityVO[0];
                        that.setActData(data); 
                      }
                });
                
                var input = { "token": token, "partyid": this.partyid, "actid": this.actid };
                var partyID = localStorage.getItem('partyID');
                var fName = localStorage.getItem("fName");
                var lName = localStorage.getItem("lName");
                var partyName = fName + " " + lName;
                
                fnGetWebServerData("crf/info", undefined, input, function (data) {
                    that.gmTitle = "CRF - " + parseNull(data.gmCRMSurgeonlistVO.salutenm) +" "+ data.gmCRMSurgeonlistVO.firstnm +" "+ parseNull(data.gmCRMSurgeonlistVO.midinitial)+" "+data.gmCRMSurgeonlistVO.lastnm;
                    $(".gmPanelTitle").html(that.gmTitle);
                    that.crfdata = $.parseJSON(JSON.stringify(data));
                    $("#div-main-content").html(that.template_CRFDetailMain());
                    
                    
                    
                    
                    
                    var now = new Date();
                    if (that.crfdata.reqdate != "")
                        now = new Date(that.crfdata.reqdate);
                    var day = ("0" + now.getDate()).slice(-2);
                    var month = ("0" + (now.getMonth() + 1)).slice(-2);
                    that.crfdata.reqdate = month + "/" + day + "/" + now.getFullYear();

                    $("#div-approver-details-content").html(that.template_CRFDetailContent(that.crfdata));
//                    delete that.crfdata.gmCRMSurgeonlistVO.address;
                    that.toogleDefault();
                     var fname = localStorage.getItem("fName");
                    var lname = localStorage.getItem("lName");
                    var ReqName = fname + " " + lname;
                    if(that.crfdata.gmCRMSurgeonlistVO.arrgmsurgeoncontactsvo)
                        _.each(that.crfdata.gmCRMSurgeonlistVO.arrgmsurgeoncontactsvo,function(data){

                            if(data.conmode == "90450"){
                                if(data.contype == "Assistant" && data.convalue!= "" )
                                    GM.Global.WorkNo = data.convalue;
                                else if(data.contype == "Business" && data.convalue!= ""  )
                                    GM.Global.WorkNo = data.convalue;
                                }});
                    

                    if(GM.Global.WorkNo)
                      $(".span-crf-work").html(GM.Global.WorkNo).attr('href', 'tel:'+GM.Global.WorkNo );
                    else
                       $(".span-crf-work").html("Unavailable").addClass("gmFontItalics"); 

                    if(that.crfdata.gmCRMSurgeonlistVO.arrgmsurgeonsaddressvo != "") {
                      that.crfdata.gmCRMSurgeonlistVO.arrgmsurgeonsaddressvo = that.crfdata.gmCRMSurgeonlistVO.arrgmsurgeonsaddressvo[0];
                    if(that.crfdata.gmCRMSurgeonlistVO.arrgmsurgeonsaddressvo.length >1){
                        
                        _.each( that.crfdata.gmCRMSurgeonlistVO.arrgmsurgeonsaddressvo ,function(data){
                           if(data.primaryfl == "y")
                                that.crfdata.gmCRMSurgeonlistVO.arrgmsurgeonsaddressvo = arrayOf(data);
                         });
                      }
                        $("#div-crf-address").text(that.crfdata.gmCRMSurgeonlistVO.arrgmsurgeonsaddressvo.add1 + "," +
                            that.crfdata.gmCRMSurgeonlistVO.arrgmsurgeonsaddressvo.city + "," +
                            that.crfdata.gmCRMSurgeonlistVO.arrgmsurgeonsaddressvo.statenm + " " +
                            that.crfdata.gmCRMSurgeonlistVO.arrgmsurgeonsaddressvo.zip);
                    }
                    else
                        $("#div-crf-address").html("<span class='gmFontItalic'>Unavailable</span>")
                      that.getTotal();   
                    
                    $("#span-crf-requestor").html(ReqName);
                    that.getFromFiles(data.partyid)
                    if (data.arrgmcrmlogvo != null)
                        that.arrgmcrmlogvo = data.arrgmcrmlogvo;

                    if (that.updatedfeedback == false) {
                        that.updatedfeedback = true;
                        if (that.arrgmcrmlogvo.length == undefined) {
                            that.crffeedbackdata.push({"logid": that.arrgmcrmlogvo.logid,"refid": that.arrgmcrmlogvo.refid,"logtypeid": that.arrgmcrmlogvo.logtypeid,"logtype": that.arrgmcrmlogvo.logtype,"logcomment": that.arrgmcrmlogvo.logcomment,"updatedby": that.arrgmcrmlogvo.updatedby,"updateddate": that.arrgmcrmlogvo.updateddate,"voidfl": that.arrgmcrmlogvo.voidfl,"updatedbyid": that.arrgmcrmlogvo.updatedbyid});

                        } else if (that.arrgmcrmlogvo.length != 0) {
                            for (var i = 0; i < that.arrgmcrmlogvo.length; i++) {
                                that.crffeedbackdata.push({"logid": that.arrgmcrmlogvo[i].logid,"refid": that.arrgmcrmlogvo[i].refid,"logtypeid": that.arrgmcrmlogvo[i].logtypeid,"logtype": that.arrgmcrmlogvo[i].logtype,"logcomment": that.arrgmcrmlogvo[i].logcomment,"updatedby": that.arrgmcrmlogvo[i].updatedby,"updateddate": that.arrgmcrmlogvo[i].updateddate,"voidfl": that.arrgmcrmlogvo[i].voidfl,"updatedbyid": that.arrgmcrmlogvo[i].updatedbyid});
                            }
                        }
                    }

                    if (that.crffeedbackdata.length != 0) {
                        that.crffeedbackdata.sort(function (a, b) {
                            return (a.logid - b.logid);
                        });
                        that.crffeedbackdata.reverse();
                        if (that.crffeedbackdata) {
                            $(".approver-comments-content").removeClass("hide");
                            that.$("#div-approver-feedback-comments-lists").html(that.template_feedback_list(that.crffeedbackdata));
                            $(".edit-comments").addClass('hide');
                            $(".delete-comments").addClass('hide');
                        }
                    } else {
                        $("#div-approver-feedback-comments-data").removeClass("hide");
                    }


                    var partyID = localStorage.getItem('partyID');
                    if(data.arrgmcrfapprovalvo != undefined) {
                        that.$(".div-approver-approval-list-content").html(that.template_approval_list(data.arrgmcrfapprovalvo));
                        var userID = localStorage.getItem("userID");


                        if (_.contains(GM.Global.Admin, "crfappr"))
                            if(that.crfdata.statusid > "105206") {
                                $("#div-approver-"+partyID).each(function () {
                                    if ($(this).find(".div-crf-approval-detail").attr("statusid") != "105208") {
                                        $(this).find(".div-crf-approval-buttons").removeClass("hide");
                                        if ($(this).find(".div-crf-approval-detail").attr("statusid") == "105209") {
                                            $(this).find(".btn-crf-reject").addClass("hide");
                                        }
                                    }
                                }); 
                            }
                    }
                    else {
                        $(".approvers-list").addClass("hide");
                    } 

                });
        
                resizeHeadWidth('.gmPanelHead', '.gmBtnGroup', '#panelhead-left');
            },
            
            toogleDefault: function (event) {
           $(".requestdetail").addClass("gmMediaDisplayNone");
             $(".servicedes").addClass("gmMediaDisplayNone");
             $(".hourstim").addClass("gmMediaDisplayNone");                                 
             $(".approval").addClass("gmMediaDisplayNone");
             $(".feedbac").addClass("gmMediaDisplayNone");
            },
             toogleRequest: function (event) {
             $(".requestdetail").removeClass("gmMediaDisplayNone");
             $(".servicedes").addClass("gmMediaDisplayNone");
             $(".hourstim").addClass("gmMediaDisplayNone");                                 
             $(".approval").addClass("gmMediaDisplayNone");
             $(".feedbac").addClass("gmMediaDisplayNone");
                 
            },
            toogleDescription: function (event) {
             $(".servicedes").removeClass("gmMediaDisplayNone");
             $(".requestdetail").addClass("gmMediaDisplayNone");
             $(".hourstim").addClass("gmMediaDisplayNone");                                 
             $(".approval").addClass("gmMediaDisplayNone");
             $(".feedbac").addClass("gmMediaDisplayNone");
                 
            },
            toogleTime: function (event) {
             $(".hourstim").removeClass("gmMediaDisplayNone");
             $(".servicedes").addClass("gmMediaDisplayNone");
             $(".requestdetail").addClass("gmMediaDisplayNone");
             $(".approval").addClass("gmMediaDisplayNone");
             $(".feedbac").addClass("gmMediaDisplayNone");
                 
            },
            toogleApprover: function (event) {
             $(".hourstim").addClass("gmMediaDisplayNone");
             $(".servicedes").addClass("gmMediaDisplayNone");
             $(".requestdetail").addClass("gmMediaDisplayNone");
             $(".approval").removeClass("gmMediaDisplayNone");
             $(".feedbac").addClass("gmMediaDisplayNone");
                 
            },
            toogleFeedback: function (event) {
             $(".hourstim").addClass("gmMediaDisplayNone");
             $(".servicedes").addClass("gmMediaDisplayNone");
             $(".requestdetail").addClass("gmMediaDisplayNone");
             $(".approval").addClass("gmMediaDisplayNone");
             $(".feedbac").removeClass("gmMediaDisplayNone");
                 
            },
            editDetails: function () {
                var that = this;
                var input = { "token": localStorage.getItem("token"), "refid": this.actid, "refgroup": "107994" };
                fnGetWebServerData("file/get", "gmCRMFileUploadVO", input, function (file) {

                    if(file != null ){
                        that.filetitle  = file.filetitle;
                    $(".a-service-desc-agenda-file-name").text(file.filetitle);
                    $(".a-service-desc-agenda-file-name").attr("href", URL_CRMFileServer + file.refgroup + "/" + file.refid + "/" + file.filename);
                    $(".a-service-desc-agenda-file-name").attr("data-fileid", file.fileid);
                    $(".a-service-desc-agenda-file-name").addClass("open-crf-doc");
                    }
                });

                resizeHeadWidth('.gmPanelHead', '.gmBtnGroup', '#panelhead-left');
                this.$el.html(this.template_MainModule({"module": "crf" }));
                $("#crm-btn-back").removeClass("hide");
                this.$(".gmBtnGroup #li-save").removeClass("hide");
                this.$(".gmBtnGroup #li-cancel").removeClass("hide");
                $("#div-main-content").html(this.template());
                $(".gmPanelTitle").html(this.gmTitle);
                 var event = {};
                event.currentTarget = $("#div-crf-details-objective");
                this.toggleTabs(event);
                    if(this.crfdata.statusid == "105205")   // 105205,'Initiated'
                        $("#crf-send-review").removeClass("hide");
                    else if(this.crfdata.statusid == "105206")   // 105206,'Pending Review'
                        $("#crf-send-approval").removeClass("hide");
                
                if(this.crfdata.crfid == "")
                    $("#div-crf-details-feedback").addClass("hide"); // comments tab hide when CRF is create time
                var userAccess = $.parseJSON(localStorage.getItem("userAccess"));
                
                if(_.contains(_.pluck(userAccess,'funcid'),'CRM-INTERNALUSE'))
                    this.$("#div-crf-details-internaluse").removeClass("hide");
                },
            
            setActData: function(data){
                var that = this;
                that.actModel = new GMMActivity(data);
                that.actModel.set("arrgmactivitypartyvo", arrayOf(data.arrgmactivitypartyvo));
                that.actModel.set("arrgmactivityattrvo", arrayOf(data.arrgmactivityattrvo));
                that.actModel.set("arrgmcrmlogvo", arrayOf(data.arrgmcrmlogvo));
                that.actModel.set("arrgmcrmaddressvo", arrayOf(data.arrgmcrmaddressvo));
                that.actModel.set("frmdt",that.actModel.get("actstartdate"));
                that.actModel.set("todt",that.actModel.get("actenddate"));
                if (that.actModel.get('arrgmcrmaddressvo')[0] == null || that.actModel.get('arrgmcrmaddressvo')[0] == undefined)
                    that.actModel.set('arrgmcrmaddressvo', []);
                if (that.actModel.get('arrgmcrmlogvo')[0] == null || that.actModel.get('arrgmcrmlogvo')[0] == undefined)
                    that.actModel.set('arrgmcrmlogvo', []);
                if (that.actModel.get('arrgmactivityattrvo')[0] == null || that.actModel.get('arrgmactivityattrvo')[0] == undefined)
                    that.actModel.set('arrgmactivityattrvo', []);
                if (that.actModel.get('arrgmactivitypartyvo')[0] == null || that.actModel.get('arrgmactivitypartyvo')[0] == undefined)
                    that.actModel.set('arrgmactivitypartyvo', []);

//                $(".gmPanelTitle").html("CRF - " + that.actModel.get("actnm"));

                var input = {"token": localStorage.getItem("token"),"refid": that.actid,"refgroup": "107994"};
                fnGetWebServerData("file/get", "gmCRMFileUploadVO", input, function (file) {
                    if (file != null) {
                        that.agenda = file;
                        that.filetitle = that.agenda.filetitle;
                        that.$(".a-service-desc-agenda-file-name").text(that.agenda.filetitle);
                        that.$(".a-physicianvisit-agenda-file-name").text(that.agenda.filetitle);
                        that.$(".a-service-desc-agenda-file-name").attr("href", URL_CRMFileServer + that.agenda.refgroup + "/" + that.agenda.refid + "/" + that.agenda.filename);
                    }
                });
            },
                
             enableEdit: function () {

              resizeHeadWidth('.gmPanelHead', '.gmBtnGroup', '#panelhead-left');
            
                if(this.crfdata.crfid == "")
                    $("#div-crf-details-feedback").addClass("hide"); // comments tab hide when CRF is create time
                
                 
               var that = this;
//                $("#div-crf-details-content .div-merc-title").addClass("hide");
                
                var userAccess = $.parseJSON(localStorage.getItem("userAccess"));
                
                if(_.contains(_.pluck(userAccess,'funcid'),'CRM-INTERNALUSE'))
                    this.$("#div-crf-details-internaluse").removeClass("hide");

                this.$("#div-crf-details-objective").trigger("click");
                     
                $("#div-crf-tabs").show();

                $("#div-merc-details-main .div-edit").addClass("hide");
//                this.togglePhoneDisplayEdit({"currentTarget":"#div-crf-details-requestdetais"});    
                return this;
            },

              togglePhoneDisplay: function(e){

            var tab = $(e.currentTarget).attr("id");
             if($(e.currentTarget).hasClass("selected"))          
                  { 
                      $(e.currentTarget).removeClass("selected");
                    this.$(".crf-tab").toggle();
                    this.$(e.currentTarget).toggle();
                      $(".hourstim").addClass("gmMediaDisplayNone");
                        $(".servicedes").addClass("gmMediaDisplayNone");
                         $(".requestdetail").addClass("gmMediaDisplayNone");
                         $(".approval").addClass("gmMediaDisplayNone");
                         $(".feedbac").addClass("gmMediaDisplayNone");
                      this.$(e.currentTarget).find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-right");
                      
                      
                      
                  }
                  else{
                      
                     this.$(".crf-tab").toggle();
                  this.$(e.currentTarget).toggle();
                  this.$(e.currentTarget).addClass("selected");
                      $(e.currentTarget).find(".fa-chevron-right").removeClass("fa-chevron-right").addClass("fa-chevron-down");
                  }
        },
            togglePhoneDisplayEdit: function(e){
            var tab = $(e.currentTarget).attr("id");
             if($(e.currentTarget).hasClass("selected"))          
                  { 

                      $(e.currentTarget).removeClass("selected");
                    this.$(".crf-tabs-edit").toggle();
                    this.$(e.currentTarget).toggle();
                    this.$(e.currentTarget).find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-right");
                    $("#div-crf-details-content").hide();  
                  }
                  else{
                     this.$(".crf-tabs-edit").toggle();
                  this.$(e.currentTarget).toggle();
                  this.$(e.currentTarget).addClass("selected");
                      $(e.currentTarget).find(".fa-chevron-right").removeClass("fa-chevron-right").addClass("fa-chevron-down");
                      $("#div-crf-details-content").show();
                  }
                  
                  
        }, 
        updateDesc: function (event) {
                this.crfdata.servicedesc = $(event.currentTarget).val();
            },

            updateNotes: function (event) {
                this.crfdata.hcpqualification = $(event.currentTarget).val();
            },

            toggleTabs: function (e) {
                var that = this;
                colorControl(e, "crf-tabs", "gmBGLGrey", "gmBGDarkerGrey gmFontWhite");

                if ($(e.currentTarget).find(".fa").hasClass("fa-chevron-right")) {
                    $(e.currentTarget).find(".fa-chevron-right").removeClass("fa-chevron-right").addClass("fa-chevron-down");
                    $(".crf-tabs").addClass("gmMediaDisplayNone");
                    $("#crf-send-buttons").addClass("gmMediaDisplayNone");
                    $(e.currentTarget).removeClass("gmMediaDisplayNone");
                    $("#div-crf-details-content").removeClass("gmMediaDisplayNone");
                }
                else {
                    $(e.currentTarget).find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-right");
                    $(".crf-tabs").removeClass("gmMediaDisplayNone");
                    $("#crf-send-buttons").removeClass("gmMediaDisplayNone");
                    $("#div-crf-details-content").addClass("gmMediaDisplayNone")
                }

                var targetID = $(e.currentTarget).attr("id");
                var userID = localStorage.getItem("userID");
                if (targetID == "div-crf-details-objective") {

                    if (this.actModel.get("actcategory") == "105265") { //  actcategory == "105265" ref to MERC
                            
                            this.$("#div-crf-details-content").html(this.template_merc(this.actModel.toJSON()));
                            $(".div-merc-title").addClass('hide');
                            $("#div-merc-header-label-print").addClass('hide');
                            $(".participants-list").addClass('hide');
                            $(".comments-list").addClass('hide');
                            if (this.actModel.get("acttype") == "4000925") { //check merc type is "Preceptorship"(4000925); and hide tabs
                                $("#div-crf-details-servicedesc").addClass('hide');
                                $("#div-crf-details-hcpqualification").addClass('hide');
                            }
                        } else if (this.actModel.get("actcategory") == "105266") { //   actcategory == "105266" ref to Physician Visit

                            this.$("#div-crf-details-content").html(this.template_physician(this.actModel.toJSON()));
                            $("#span-physicianvisit-details-detailview-physician-name").text();
                            if (this.actModel.get("acttype") != "") {
                                if (this.actModel.get("acttype") == "105226") {
                                    $(".ul-physicianvisit-display-projects").removeClass("hide");
                                    $(".ul-physicianvisit-display-product").addClass("hide");
                                } else {
                                    $(".ul-physicianvisit-display-projects").addClass("hide");
                                    $(".ul-physicianvisit-display-product").removeClass("hide");
                                }
                            }
                        } else if (this.actModel.get("actcategory") == "105268") { //  actcategory == "105268"    ref to Training

                            this.$("#div-crf-details-content").html(this.template_training(this.actModel.toJSON()));
                            $(".div-training-title").addClass('hide');
                            $(".div-training-surgeons-details-content").addClass('hide');
                            $(".div-training-header-label-comment").addClass('hide');
                            $(".training-comments-list").addClass('hide');
                            $("#div-training-header-label-print").addClass('hide');
                        }
                    
                    $(".a-service-desc-agenda-file-name").text(this.filetitle);
                }
                else if (targetID == "div-crf-details-requestdetails") {
                    this.$("#div-crf-details-requestdetails").addClass("btn-crf-tab-select");
                    this.$("#div-crf-details-content").html(this.template_request(this.crfdata));
                    
                    if(this.crfdata.priorityfl == "Y")
                        $("#input-crf-priorityfl").prop( "checked", true );
                    else
                        $("#input-crf-priorityfl").prop( "checked", false);
                    
                    if(this.crfdata.reqtypenm == "Resubmission")
                        $("#div-crf-type-of-req").val("105211");
                    else
                         $("#div-crf-type-of-req").val(this.crfdata.reqtypeid);
                    
                    if (this.crfdata.reqtypeid == "105211") {
                        $("#input-type-of-req").removeClass("hide");
                    } else {
                        $("#input-type-of-req").addClass('hide');
                    }
                    
                    if(this.crfdata.reqtypeid == "")
                        $("#div-crf-type-of-req").val("105210");
                        

                    var fname = localStorage.getItem("fName");
                    var lname = localStorage.getItem("lName");
                    var ReqName = fname + " " + lname;
                    $("#span-crf-requestor").html(ReqName);
                    var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth()+1;
                var yyyy = today.getFullYear();
                if(dd<10) { dd='0'+dd }
                if(mm<10) { mm='0'+mm }
                today = mm+'/'+dd+'/'+yyyy;
                    $("#spn-crf-req-date").text(today);

                    $("#div-crf-type-of-req").on("change", function () {
                        var selected = $('#div-crf-type-of-req option:selected').val();
                        if (selected == "105211") {
                            $("#input-type-of-req").removeClass("hide");
                        } else {
                            $("#input-type-of-req").addClass('hide');
                        }
                    });

                    if (arrayOf(that.crfdata.gmCRMSurgeonlistVO != null)) {
                        if (that.crfdata.gmCRMSurgeonlistVO.length == undefined) {
                            if (that.crfdata.gmCRMSurgeonlistVO.arrgmsurgeoncontactsvo != (null || undefined)) {

                                var surgeonCont = arrayOf(that.crfdata.gmCRMSurgeonlistVO.arrgmsurgeoncontactsvo);

                                var contMail = _.findWhere(surgeonCont, {
                                    contype: "",
                                    conmode: "90452"
                                });
                                if (contMail)
                                    $("#span-crf-email").text(contMail.convalue);

                                var contCell = _.findWhere(surgeonCont, {
                                    contype: "",
                                    conmode: "90450"
                                });
                               if (contCell)
                                    $("#span-crf-cell").text(contCell.convalue);

//                                var contWork = _.findWhere(surgeonCont, {
//                                    contype: "Business",
//                                    conmode: "90450"
//                                });
//                                if (contWork)
//                                    $("#span-crf-work").html(contWork.convalue);
                                if(GM.Global.WorkNo !="" && GM.Global.WorkNo!= undefined)
                                    $(".span-crf-work").html(GM.Global.WorkNo)
                                else
                                    $(".span-crf-work").html("Unavailable").addClass("gmFontItalic");
                            } 

                            if (arrayOf(that.crfdata.gmCRMSurgeonlistVO.arrgmsurgeonsaddressvo != (null || undefined))) {
                                var surgeonAddr = that.crfdata.gmCRMSurgeonlistVO.arrgmsurgeonsaddressvo;
                                var addrData = _.findWhere(surgeonAddr, {
                                    primaryfl: "Y"
                                });
                                if (addrData) {

                                    $("#span-crf-address-add1-add2").text(addrData.add1 + "," + addrData.add2);
                                    $("#span-crf-address-city").text(addrData.city);
                                    $("#span-crf-address-stat-zip").text(addrData.statenm + "," + addrData.zip);
                                } else if (!addrData) {
                                    if(surgeonAddr) {
                                        $("#span-crf-address-add1-add2").text(surgeonAddr.add1 + "," + surgeonAddr.add2);
                                        $("#span-crf-address-city").text(surgeonAddr.city);
                                        $("#span-crf-address-stat-zip").text(surgeonAddr.statenm + "," + surgeonAddr.zip); 
                                    }
                                } else {
                                    $("#span-crf-address-add1-add2").text(surgeonAddr.add1 + "," + surgeonAddr.add2);
                                    $("#span-crf-address-city").text(surgeonAddr.city);
                                    $("#span-crf-address-stat-zip").text(surgeonAddr.statenm + "," + surgeonAddr.zip);
                                }
                            }
                            
                            this.getFromFiles(that.crfdata.partyid);
                        }
                    }
                } else if (targetID == "div-crf-details-servicedesc") {
                    $(".btn-crf-tab-select").removeClass("btn-crf-tab-select");
                    $("#div-crf-details-servicedesc").addClass("btn-crf-tab-select");
                    $("#div-crf-details-content").html(this.template_description(this.crfdata));

                    this.$("#txt-crf-service-desc-comments").trigger("keyup");

                } else if (targetID == "div-crf-details-hcpqualification") {
                    $(".btn-crf-tab-select").removeClass("btn-crf-tab-select");
                    $("#div-crf-details-hcpqualification").addClass("btn-crf-tab-select");
                    $("#div-crf-details-content").html(this.template_qualification(this.crfdata));
                    this.$("#txt-crf-hcp-qualify-comments").trigger("keyup");

                } else if (targetID == "div-crf-details-hourstime") {
                    $(".btn-crf-tab-select").removeClass("btn-crf-tab-select");
                    $("#div-crf-details-hourstime").addClass("btn-crf-tab-select");
                    $("#div-crf-details-content").html(this.template_hours(this.crfdata));
                    that.$("select").each(function () {
                        this.value = $(this).attr("data-value");
                    });

                } else if (targetID == "div-crf-details-feedback") {
                    $(".btn-crf-tab-select").removeClass("btn-crf-tab-select");
                    $("#div-crf-details-feedback").addClass("btn-crf-tab-select");
                    $("#div-crf-details-content").html(this.template_feedback());
                    this.$("#span-crf-feedback-comments-count").html(500);
                    this.$("#txt-crf-feedback-comments").trigger("keyup");
                    if (this.crffeedbackdata.length != 0) {
                        this.crffeedbackdata.sort(function (a, b) {
                            return (a.logid - b.logid);
                        });
                        this.crffeedbackdata.reverse();
                        this.$("#div-crf-feedback-comments-lists").html(this.template_feedback_list(this.crffeedbackdata));
                        this.$(".div-crf-feedback-comments-list").each(function () {
                            if ($(this).find(".span-crf-feedback-comments-list-header").attr("updatedbyid") != userID) {
                                $(this).find(".edit-comments").addClass('hide');
                                $(this).find(".delete-comments").addClass('hide');
                            }
                        });
                    } else {
                        $("#div-crf-feedback-comments-data").removeClass("hide");
                    }

                    if (this.viewmode == "detail") {
                        $("#div-crf-feedback-comments").addClass('hide');
                        $(".edit-comments").addClass('hide');
                        $(".delete-comments").addClass('hide');
                        $("#div-crf-feedback-comments-count").addClass('hide');
                        $("#div-crf-feedback-department").addClass('hide');
                    }

                } else if (targetID == "div-crf-details-internaluse") {
                    $(".btn-crf-tab-select").removeClass("btn-crf-tab-select");
                    $("#div-crf-details-internaluse").addClass("btn-crf-tab-select");
                    $("#div-crf-details-content").html(this.template_internaluse(this.crfdata));
                    if (that.acttype == "4000925") { //check merc type is "Preceptorship";
                        $("#div-crf-intr-use-buttons").addClass('hide');
                    }

                    this.getFromFiles(that.crfdata.partyid);
                    
                    that.$("select").each(function () {
                        this.value = $(this).attr("data-value");
                    });

                    this.getTotal();
                    $("#input-internal-use-travel-hr, #input-internal-use-work-hr, #input-internal-use-prep-time-hr, #input-internal-use-hourly-amt").on("keydown keyup", function () {
                        that.getTotal();
                    });
                    if ($("#div-crf-honoraria-status").attr('data-value') == "0") {
                        $("#input-internal-use-honoraria-comment").removeClass("hide");
                        $(".iuse-status-attendee").addClass('hide');
                    } else if ($("#div-crf-honoraria-status").attr('data-value') == "1") {
                        $(".iuse-status-attendee").removeClass("hide");
                        $("#input-internal-use-honoraria-comment").addClass('hide');
                    } else {
                        $(".iuse-status-attendee").addClass('hide');
                        $("#input-internal-use-honoraria-comment").addClass('hide');
                    }

                    $("#div-crf-honoraria-status").change(function () {
                        if ($(this).val() == "1") {
                            $("#input-internal-use-honoraria-comment").addClass('hide');
                            $(".iuse-status-attendee").removeClass("hide");
                        } else if ($(this).val() == "0") {
                            $("#input-internal-use-honoraria-comment").removeClass("hide");
                            $(".iuse-status-attendee").addClass('hide');
                        } else {
                            $(".iuse-status-attendee").addClass('hide');
                            $("#input-internal-use-honoraria-comment").addClass('hide');
                        }
                    });
                } 
                this.updateJson();

            },

            getTotal: function () {
                if ($("#input-internal-use-travel-hr").val().length == 0) {
                    $("#input-internal-use-travel-hr").val("0");
                } else
                    var travelhr = document.getElementById('input-internal-use-travel-hr').value;

                if ($("#input-internal-use-work-hr").val().length == 0) {
                    $("#input-internal-use-work-hr").val("0");
                } else
                    var workhr = document.getElementById('input-internal-use-work-hr').value;

                if ($("#input-internal-use-prep-time-hr").val().length == 0) {
                    $("#input-internal-use-prep-time-hr").val("0");
                } else
                    var preptimehr = document.getElementById('input-internal-use-prep-time-hr').value;

                if ($("#input-internal-use-hourly-amt").val().length == 0) {
                    $("#input-internal-use-hourly-amt").val("0");
                }
                var hourlyamt = document.getElementById('input-internal-use-hourly-amt').value;

                var totalhrs = parseInt(travelhr) + parseInt(workhr) + parseInt(preptimehr);
                var totalamt = parseInt(totalhrs) * parseInt(hourlyamt);
                if (!isNaN(totalhrs)) {
                    this.$('#input-internal-use-tot-hr').text(totalhrs);
                    this.$('#input-internal-use-tot-amt').text(totalamt);
                }
            },

            getComments: function (event) {
                var maxlength = 1000;
                var text = event.currentTarget.value;

                if (text.length >= maxlength) {
                    event.currentTarget.value = text.substring(0, maxlength);
                    text = event.currentTarget.value;
                }

                this.$("#span-crf-service-desc-comments-count").text(maxlength - text.length);
                this.$("#span-crf-hcp-qualify-comments-count").text(maxlength - text.length);
            },
            getFromFiles: function (data) {
                    var input = { "token": localStorage.getItem("token"), "refid": data};
                    fnGetWebServerData("surgeon/infodocs", "gmCRMFileUploadVO", input, function(data){
                       var data =arrayOf(data);

                        if (data != "") {
                            _.each((data),function(data){
                                if(data.reftype =="105220"){
                                    $(".a-type-of-req-cv-file").attr("href", URL_CRMFileServer + '107994/' + data.refid + '/' + data.filename).text("CV");
                                    $(".a-internal-use-forms-in-file").attr("href", URL_CRMFileServer + '107994/' + data.refid + '/' + data.filename).text("CV");
                                    $(".forms-in-file-CV").attr("href", URL_CRMFileServer + '107994/' + data.refid + '/' + data.filename).text("CV");
                               }
                               if(data.reftype =="105221")  {
                                    $(".forms-in-file-RC").attr("href", URL_CRMFileServer + '107994/' + data.refid + '/' + data.filename).text("Rate Card");
                                   }
                                if(data.reftype =="105222") {
                                    $(".forms-in-file-W9").attr("href", URL_CRMFileServer + '107994/' + data.refid + '/' + data.filename).text("W9 Form");
                                }
                            });
                        } 
                    });
            },
            updateFiFAttrVO: function () {
                var that = this;
                $(".spn-fif-txt").each(function () {
                    var attrvalue = $(this).text();
                    var attrid = $(this).attr("codeid");
                    that.arrgcrformfilefattrvo.push({"attrid": attrid, "attrvalue": attrvalue,"voidfl": ""});
                    that.crfdata.arrgcrformfilefattrvo = $.parseJSON(JSON.stringify(that.arrgcrformfilefattrvo));
                });
            },
            
            cleartxt: function (e) {
                var targetID = $(e.currentTarget).attr("id");

                if (targetID == "span-crf-service-desc-comments-clear") {
                    $("#txt-crf-service-desc-comments").val('');
                    $("#span-crf-service-desc-comments-count").html(1000);
                } else {
                    $("#txt-crf-hcp-qualify-comments").val('');
                    $("#span-crf-hcp-qualify-comments-count").html(1000);
                }
            },

            updateJson: function () {
                var tab = this.$(".btn-crf-tab-select").attr('name');

                switch (tab) {
                    case "Objective":
                        this.crfdata.reqdate = $("#spn-crf-req-date").text();
                        
                    case "ReqDetail":
                        this.crfdata.reason = $("#input-type-of-req").val();
                        this.crfdata.reqtypeid = $("#div-crf-type-of-req").val();
                        this.crfdata.reqdate = $("#spn-crf-req-date").text();
                        
                        if($("#input-crf-priorityfl").prop('checked'))
                           $("#input-crf-priorityfl").val("Y");
                        else
                           $("#input-crf-priorityfl").val("");
                        
                        this.crfdata.priorityfl = $("#input-crf-priorityfl").val();
                        this.crfdata.servicetyp = "105215";
                        break;
                    case "ServiceDesc":
                        this.crfdata.servicedesc = $("#txt-crf-service-desc-comments").val();
                        break;
                    case "HCPQual":
                        this.crfdata.hcpqualification = $("#txt-crf-hcp-qualify-comments").val();
                        break;
                    case "HoursTime":
                        this.crfdata.preptime = this.$("#input-crf-tot-prep-time").val();
                        this.crfdata.workhrs = this.$("#input-crf-tot-work-hours").val();
                        this.crfdata.timefl = this.$("#div-crf-travel-time-option").val();
                        break;
                    case "Feedback":
                        //                    this.currentcomment.logcomment = this.$("#txt-crf-feedback-comments").val();
                        this.commentsData = this.$("#txt-crf-feedback-comments").val();
                        break;

                    case "InternalUse":
                        this.crfdata.agrfl = $("#div-crf-current-agrfl").val();
                        this.crfdata.trvlfrom = $("#input-internal-use-travel-from").val();
                        this.crfdata.trvlto = $("#input-internal-use-tracel-to").val();
                        this.crfdata.trvlmile = $("#input-internal-use-to-miles").val();
                        this.crfdata.trvltime = $("#input-internal-use-travel-hr").val();
                        this.crfdata.workhrs = $("#input-internal-use-work-hr").val();
                        this.crfdata.hrrate = $("#input-internal-use-hourly-amt").val();

                        this.crfdata.honstatus = $("#div-crf-honoraria-status").val();
                        this.crfdata.honprephrs = $("#input-internal-use-honoraria-tot-hr").val();
                        this.crfdata.honworkhrs = $("#input-internal-use-honoraria-tot-whr").val();
                        this.crfdata.honcomment = $("#input-internal-use-honoraria-comment").val();
                        break;
                }
            },
            resetDetail: function () {
                Backbone.history.loadUrl(Backbone.history.fragment);
            },

            approve: function () {
                this.statusid = "105208";// CRF status save in "Approve" status
            },
            reject: function () {
                this.statusid = "105209"; 
            },
            saveDetail: function () {
                if(this.crfdata.crfid == "")
                    this.crfdata.statusid = "105205"; //  CRF status save in "Pending Approve" status
                else {
                    this.crfdata.statusid = "105206"; //  Edit CRF status save in "Pending Review" status 
                    this.crfdata.statusnm = "Pending Review"; //  Edit CRF status save in "Pending Review" status 
                }
                
                if (this.acttype == "4000925") { //check merc type is "Preceptorship"; with save crf "approve"
                    this.crfdata.statusid = "105208";
                    this.saveCRF();
                }
                else {
                        this.saveCRF();
                }
            },
            saveCRF: function () {
                var text = $("#txt-crf-feedback-comments").val();
                var that = this;
                var token = localStorage.getItem("token");
                delete this.crfdata["userid"];
                delete this.crfdata["stropt"];
                delete this.crfdata["gmCRMSurgeonlistVO"];
                this.crfdata.token = token;
                this.feedbackid = "105181";
                var logtype = $("#txt-select-feedbacktype  option:selected").text();
                this.$("#txt-crf-feedback-comments").val("");
                this.$("#span-crf-feedback-comments-count").html(500);
                var date = new Date();
                var dd = ("0" + date.getDate()).slice(-2);
                var mm = ("0" + (date.getMonth() + 1)).slice(-2);
                var yyyy = date.getFullYear();

                var updateddate = mm + "/" + dd + "/" + yyyy;
                var fname = localStorage.getItem("fName");
                var lname = localStorage.getItem("lName");
                var updatedby = fname + " " + lname;
                this.currentcomment = new Array();
                if (this.editmode == true) {
                    this.editmode = false;
                    this.crffeedbackdata.splice(this.editindex, 1);

                    if(text != "" && text != "undefined") {
                        this.crffeedbackdata.push({"logid": this.tempeditcomment.logid,"refid": this.tempeditcomment.refid,"logtype": this.tempeditcomment.logtype,"logtypeid": this.tempeditcomment.logtypeid,"logcomment": text,"updatedby": updatedby,"updateddate": updateddate,"voidfl": ""});

                        this.currentcomment.push({"logid": this.tempeditcomment.logid,"refid": this.tempeditcomment.refid,"logtype": this.tempeditcomment.logtype,"logtypeid": this.tempeditcomment.logtypeid,"logcomment": text,"updatedby": updatedby,"updateddate": updateddate,"voidfl": ""}); 

                    }

                    this.tempeditcomment = new Array();
                } else {

                    if(that.commentsData != "" && that.commentsData != undefined ) {
                        this.crffeedbackdata.push({"logid": "","refid": "","logtype": logtype,"logtypeid": this.feedbackid,"logcomment": that.commentsData,"updatedby": updatedby,"updateddate": updateddate,"voidfl": ""});
                        this.currentcomment.push({"logid": "","refid": "","logtype": logtype,"logtypeid": this.feedbackid,"logcomment": that.commentsData,"updatedby": updatedby,"updateddate": updateddate,"voidfl": ""}); 
                    }
                    else{

                    }
                }
                var arrgmcrmlogvo = new Array();
                if (this.crfdata.crfid && this.crfdata.crfid != 0) {
                   if(this.currentcomment != "" && this.currentcomment != undefined){
                    if(this.currentcomment[0].logcomment != "" && this.currentcomment[0].logcomment != undefined)
                        arrgmcrmlogvo.push({"refid": this.crfdata.crfid,"logid": this.currentcomment[0].logid,"logtypeid": this.currentcomment[0].logtypeid,"logtype": this.currentcomment[0].logtype,"logcomment": this.currentcomment[0].logcomment,"voidfl": this.currentcomment[0].voidfl});}
                } else {
                    if(that.commentsData != "" && that.commentsData != undefined )
                        arrgmcrmlogvo.push({"refid" : "", "logid" : "", "logtypeid" : "105181", "logcomment" : that.commentsData, "voidfl" : ""});  
                }
                this.crfdata.arrgmcrmlogvo = arrgmcrmlogvo;
                var arrgmcrfapprovalvo = new Array();
                this.crfdata.arrgmcrfapprovalvo = arrgmcrfapprovalvo;

                fnGetWebServerData("crf/save", undefined, this.crfdata, function (data) {

                    showSuccess("CRF Document Detail created / updated");

                    window.history.back();
                });
                
            },

            getFeedback: function (event) {
                var maxlength = 500;
                var text = event.currentTarget.value;

                if (text.length >= maxlength) {
                    event.currentTarget.value = text.substring(0, maxlength);
                    text = event.currentTarget.value;
                }

                this.$("#span-crf-feedback-comments-count").text(maxlength - text.length);
            },

            clearComments: function () {
                this.$("#txt-crf-feedback-comments").val("");
                this.$("#span-crf-feedback-comments-count").html(500);
                if (this.editmode == true) {
                    this.tempeditcomment = new Array();
                    this.editmode = false;
                    this.$("#div-crf-feedback-comments-lists").html(this.template_feedback_list(this.crffeedbackdata));
                }
            },

            deleteComments: function (e) {
                var that = this;
                var userID = localStorage.getItem("userID");
                showNativeConfirm("Do you want to delete this comment","Confirm",["Yes","No"], function(idx) {
                    if (idx == 1) {
                        var index = $(e.currentTarget).attr("indexvalue");
                        that.tempDeleteComment = that.crffeedbackdata[index];
                        that.crffeedbackdata.splice(index, 1);
                        if (that.tempDeleteComment.logid != "") {
                            that.tempDeleteComment.voidfl = 'Y';
                            that.deletedComments.push(that.tempDeleteComment);
                            var token = localStorage.getItem("token");
                            var input = {
                                "token": token,
                                "refid": that.crfdata.crfid,
                                "logid": that.tempDeleteComment.logid,
                                "logtypeid": that.tempDeleteComment.logtypeid,
                                "logtype": that.tempDeleteComment.logtype,
                                "logcomment": that.tempDeleteComment.logcomment,
                                "voidfl": that.tempDeleteComment.voidfl
                            };
                            fnGetWebServerData("crmactivity/savelog", undefined, input, function (data) {
                                that.tempDeleteComment = new Array();
                                that.crffeedbackdata = new Array();
                                input = {
                                    "token": token,
                                    "partyid": that.partyid,
                                    "crfid": that.crfdata.crfid
                                };
                                fnGetWebServerData("crf/info", undefined, input, function (data) {
                                    if (data.arrgmcrmlogvo != null) {

                                        that.arrgmcrmlogvo = data.arrgmcrmlogvo;

                                        if (that.arrgmcrmlogvo.length == undefined) {
                                            that.crffeedbackdata.push({"logid": that.arrgmcrmlogvo.logid,"refid": that.arrgmcrmlogvo.refid,"logtype": that.arrgmcrmlogvo.logtype,"logtypeid": that.arrgmcrmlogvo.logtypeid,"logcomment": that.arrgmcrmlogvo.logcomment,"updatedby": that.arrgmcrmlogvo.updatedby,"updateddate": that.arrgmcrmlogvo.updateddate,"voidfl": that.arrgmcrmlogvo.voidfl,"updatedbyid": that.arrgmcrmlogvo.updatedbyid});
                                            if (that.crffeedbackdata.length != 0) {
                                                that.crffeedbackdata.sort(function (a, b) {
                                                    return (a.logid - b.logid);
                                                });
                                                that.crffeedbackdata.reverse();
                                                that.$("#div-crf-feedback-comments-lists").html(that.template_feedback_list(that.crffeedbackdata));
                                                that.$(".div-crf-feedback-comments-list").each(function () {
                                                    if ($(that).find(".span-crf-feedback-comments-list-header").attr("updatedbyid") != userID) {
                                                        $(that).find(".edit-comments").addClass('hide');
                                                        $(that).find(".delete-comments").addClass('hide');
                                                    }
                                                });
                                            }
                                        } else if (that.arrgmcrmlogvo.length != 0) {
                                            for (var i = 0; i < that.arrgmcrmlogvo.length; i++) {
                                                that.crffeedbackdata.push({"logid": that.arrgmcrmlogvo[i].logid,"refid": that.arrgmcrmlogvo[i].refid,"logtype": that.arrgmcrmlogvo[i].logtype,"logtypeid": that.arrgmcrmlogvo[i].logtypeid,"logcomment": that.arrgmcrmlogvo[i].logcomment,"updatedby": that.arrgmcrmlogvo[i].updatedby,"updateddate": that.arrgmcrmlogvo[i].updateddate,"voidfl": that.arrgmcrmlogvo[i].voidfl,"updatedbyid": that.arrgmcrmlogvo[i].updatedbyid});
                                                if (that.crffeedbackdata.length != 0) {
                                                    that.crffeedbackdata.sort(function (a, b) {
                                                        return (a.logid - b.logid);
                                                    });
                                                    that.crffeedbackdata.reverse();
                                                    that.$("#div-crf-feedback-comments-lists").html(that.template_feedback_list(that.crffeedbackdata));
                                                    that.$(".div-crf-feedback-comments-list").each(function () {
                                                        if ($(that).find(".span-crf-feedback-comments-list-header").attr("updatedbyid") != userID) {
                                                            $(that).find(".edit-comments").addClass('hide');
                                                            $(that).find(".delete-comments").addClass('hide');
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                    } else {
                                        that.$("#div-crf-feedback-comments-lists").html(that.template_feedback_list(that.crffeedbackdata));
                                        $("#div-crf-feedback-comments-data").removeClass("hide");
                                    }

                                });
                            });

                        } else {
                            that.$("#div-crf-feedback-comments-lists").html(that.template_feedback_list(that.crffeedbackdata));
                        }
                    }
                });
                
            },

            editComments: function (e) {
                var logid = $(e.currentTarget).parent().parent().parent().attr("logid");
                this.editindex = $(e.currentTarget).attr("indexvalue");
                this.editmode = true;
                this.tempeditcomment = this.crffeedbackdata[this.editindex];
                this.$("#txt-crf-feedback-comments").val(this.tempeditcomment.logcomment).trigger("keyup").focus();
                $("#txt-select-feedbacktype").val(this.tempeditcomment.logtypeid).trigger("change");
                this.$(".div-crf-feedback-comments-list[logid='" + logid + "']").remove();
            },
            
            
            
            sendReview: function(e) {
                    this.crfdata.statusid = "105206";
                    this.saveCRF();
            },
            sendApproval: function(e) {
                    this.crfdata.statusid = "105207";
                    this.saveCRF();
            },
            approveCRF: function(e) {
                var that = this;
                var partyID = localStorage.getItem('partyID');
                if($("#div-approver-" + partyID + " .btn-crf-approve").get(0) == e.currentTarget) {
                    var elem = this.$("#div-approver-" + partyID);
                    var input = {crfapprid : $(elem).attr("crfapprid"),
                                 comments : $(elem).find(".input-approval-comment").val(),
                                 crfid : that.crfdata.crfid,
                                 statusid : '105208'
                                };


                    fnGetWebServerData("crf/setstatus",undefined,input,function(data) {

                       if(data!=null) {
                           $(elem).find(".div-crf-approval-buttons").addClass("hide");
                           $(elem).find(".div-crf-appr-status").text("Approved");
                           $(elem).find(".div-crf-appr-comments").text(input.comments);
                       } 
                    });
                    
                }
            },
            rejectCRF: function(e) {
                var that = this;
                var partyID = localStorage.getItem('partyID');
                if($("#div-approver-" + partyID + " .btn-crf-reject").get(0) == e.currentTarget) {
                    var elem = this.$("#div-approver-" + partyID);
                    $(elem).find(".crf-approval-validation").addClass("hide")
                        var input = {crfapprid : $(elem).attr("crfapprid"),
                                    comments : $(elem).find(".input-approval-comment").val(),
                                    crfid : that.crfdata.crfid,
                                    statusid : '105209'
                                    };  

                    fnGetWebServerData("crf/setstatus",undefined,input,function(data) {
                       if(data!=null) {
                           $(elem).find(".btn-crf-reject").addClass("hide");
                           $(elem).find(".div-crf-appr-status").text("Denied");
                           $(elem).find(".div-crf-appr-comments").text(input.comments);
                       } 
                    });
                }
            },
            
            openDoc: function(event) {
                var className = event.target.className, path = $(event.currentTarget).attr('href') ;

                if(path != undefined)
                    window.open(path, '_blank', 'location=no');
            },
            
            //Handles the orietaion change in the iPad
            onOrientationChange: function(event) {
                resizeHeadWidth('.gmPanelHead', '.gmBtnGroup', '#panelhead-left');
            },

        });

        return GM.View.CRFDetail;
    });
