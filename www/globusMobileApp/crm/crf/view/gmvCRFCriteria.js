define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars', 'jqueryui',

    // Common
    'global',
    
    // View
    'gmvCRMData','gmvCRMSelectPopup',

    // Pre-Compiled Template
    'gmtTemplate'
    
    //Model
    
], 

function ($, _, Backbone, Handlebars, JqueryUI,
           Global,
           GMVCRMData, GMVCRMSelectPopup, GMTTemplate) {

    'use strict'; 
    
    GM.View.CRFCriteria = Backbone.View.extend({

        el: "#div-crm-module",

        name: "CRM CRF Criteria View",

        /* Load the templates */ 
        template_MainModule: fnGetTemplate(URL_Common_Template, "gmtCRMMainModule"),
        template_CRFCriteria: fnGetTemplate(URL_CRF_Template, "gmtCRFCriteria"),
        template_FilterButtons: fnGetTemplate(URL_Common_Template, "gmtFilterButtons"),

        events: {
            "click .btn-upcoming": "upComingCRF",
            "click #btn-search-title": "lookupByTitle",
            "keypress input": "clickDefaultButton",
            "click  .btn-listby": "openPopup",
            "click .sub-tab"                                      : "toggleTabs",
            "click #btn-category-search": "searchCategory",
            "click .gmBtnCrDelete": "removeFavorite",
            "click .div-text-selected-crt":  "toggleSelectedItems"
        },
        
        initialize: function(options) {
            GM.Global.CRFID = null;
            GM.Global.CRFCriteria = null;
            GM.Global.CRF = null;
            GM.Global.myFilter = null;
        },

        render: function() {
            var that = this;
            $(window).on('orientationchange', this.onOrientationChange);
            this.$el.html(this.template_MainModule({"module": "crf"}));
            this.$("#li-create").removeClass("hide");
            this.$("#div-main-content").html(this.template_CRFCriteria());
            this.$(".gmPanelTitle").html("Lookup CRF Programs");
            this.$("#crm-btn-back").hide();

            $("#crf-frmdate").datepicker({
                    onSelect: function (selected) {
                        var dt = new Date(selected);
                        dt.setDate(dt.getDate());
                        $("#crf-todate").datepicker("option", "minDate", dt);
                        $("#crf-frmdate").trigger("blur");
                    }
            });
            $("#crf-todate").datepicker({
                 onSelect: function (selected) {
                    var dt = new Date(selected);
                    dt.setDate(dt.getDate());
                    $("#crf-todate").datepicker("option", dt);
                    $("#crf-todate").trigger("blur");
                }
            });
            
            var input = {"token":localStorage.getItem("token"), "criteriatyp": "crf"};
            fnGetWebServerData("criteria/list", undefined, input, function(data){
                if(data!=null) {
                    that.$(".gmMyFav li").removeClass('hide');
                    that.$(".gmCRFFav").html(that.template_FilterButtons(arrayOf(data.gmCriteriaVO)));
                    that.$(".gmBtnCr").addClass("gmBGcrf");
                }
                else
                    that.$("#li-crf-criteria-filter-content").addClass('hide');
            });
            
            var event = {};
            event.currentTarget = this.$("#crf-tab-search");
            this.toggleTabs(event);
        },

        // List out the upcoming CRF program list
        upComingCRF: function() {
            GM.Global.CRF = { RepID: localStorage.getItem("userID") };
            window.location.href = "#crf/list/0";
        },
        
        //option to load data on Enter key
        clickDefaultButton: function(event) {
            var that = this;
            if(event.keyCode==13) {
                var txtInput = $(event.target).attr('data-id');
                
                console.log(txtInput);
                switch(txtInput) {
                    case "crf-actid":
                        that.lookupByActID(event);
                        break;
                    case "crf-title":
                        that.lookupByTitle(event);
                        break;
                    case "crf-fname":
                    case "crf-lname":
                        that.lookupByName(event);
                        break;
                }    
            }
        },
        
        // Lookup CRF Details by providing NPI
        lookupByActID: function (event) {
            var actid = this.$(".crf-actid").val();
            var token = localStorage.getItem("token");
            if (actid != "") {
                var input = { "token": token, "actid": actid, "actcategory": "105265" }; //  105265 - CRF actcategory id
                fnGetWebServerData("crmactivity/info", "gmCRMActivityVO", input, function (data) {
                    if (data.actid == "") {
                        showError("Control ID is not available");
                    } else if (data.actid != undefined) {
                        window.location.href = "#crf/id/" + actid;
                    }
                });
            } else {
                showError("Please Enter the Valid Control ID");
            }
        },

        // Lookup CRF by providing CRF Name
        lookupByName: function(event) {

            var firstnm = $(event.currentTarget).parent().find(".crf-fname").val();
            var lastnm = $(event.currentTarget).parent().find(".crf-lname").val();

            GM.Global.CRF = { firstnm: firstnm, lastnm: lastnm };

            firstnm = (firstnm !== "") ? firstnm : "undefined";
            lastnm = (lastnm !== "") ? lastnm : "undefined";
            
            window.location.href = "#crf/name/" + firstnm + "/" + lastnm;    

        },
        
        // Lookup CRF by providing Keyword
        lookupByTitle: function(event) {
            var actnm = $(event.currentTarget).parent().find(".crf-title").val();
           if(actnm !=""){ 
               GM.Global.CRF = { actnm: actnm};
            window.location.href = "#crf/title/" + actnm;
           }
        },
        
        // Open Single / Multi Select box        
        openPopup: function(event){
            var that = this;
            console.log(GM.Global.CRFID);
            that.systemSelectOptions = {                                                                                                              
                'title': $(event.currentTarget).attr("data-title"),
                'storagekey': $(event.currentTarget).attr("data-storagekey"),
                'webservice': $(event.currentTarget).attr("data-webservice"),   
                'resultVO': $(event.currentTarget).attr("data-resultVO"),
                'IDParameter': $(event.currentTarget).attr("data-IDParameter"),
                'NameParameter': $(event.currentTarget).attr("data-NameParameter"),
                'inputParameters': $(event.currentTarget).attr("data-inputParameters"),
                'codegrp': $(event.currentTarget).attr("data-codegrp"),
                'module': "crf",
                'event': event,
                'parent': this,
                'moduleLS': GM.Global.CRFID,
                'moduleCLS': GM.Global.CRFCriteria,
                'fnName': fnGetCodeNMByGRP,
                'callback': function(ID, Name){
                    GM.Global.CRFID = ID;
                    GM.Global.CRFCriteria = Name;
                    console.log(ID);
                    console.log(Name)
                }
            };
            
            that.systemSelectView = new GMVCRMSelectPopup(that.systemSelectOptions);
        },
        
        // Lookup CRF by Activity Date
        activityDate: function () {
            var that = this;
            var fromdate = $("#crf-frmdate").val();
            var todate = $("#crf-todate").val();
            
            if(fromdate !="" && todate !="") {
//                var dd, mm, yyyy, date;
//                date = new Date(fromdate);
//
//                dd = ("0" + date.getDate()).slice(-2);
//                mm = ("0" + (date.getMonth() + 1)).slice(-2);
//                yyyy = date.getFullYear();
                
                if (GM.Global.CRF.Filter == undefined)
                    GM.Global.CRF.Filter = {};
                if (GM.Global.CRF.FilterName == undefined)
                    GM.Global.CRF.FilterName = {};

//                GM.Global.CRF.Filter["crtfrmdate"] = mm + "/" + dd + "/" + yyyy;
//                GM.Global.CRF.FilterName["From Date"] = mm + "/" + dd + "/" + yyyy;
//                
                GM.Global.CRF.Filter["crtfrmdate"] = getDateByFormat(fromdate);
                GM.Global.CRF.FilterName["From Date"] = getDateByFormat(fromdate);
                
//                date = new Date(todate);
//                dd = ("0" + date.getDate()).slice(-2);
//                mm = ("0" + (date.getMonth() + 1)).slice(-2);
//                yyyy = date.getFullYear();
//
//                GM.Global.CRF.Filter["crttodate"] = mm + "/" + dd + "/" + yyyy;
//                GM.Global.CRF.FilterName["To Date"] = mm + "/" + dd + "/" + yyyy;

                GM.Global.CRF.Filter["crttodate"] = getDateByFormat(todate);
                GM.Global.CRF.FilterName["To Date"] = getDateByFormat(todate);
            }
        },    
        
        // Lookup CRF by Filters
        searchCategory: function (event) {
            
            GM.Global.CRFID = $.parseJSON(GM.Global.CRFID);
            GM.Global.CRFCriteria = $.parseJSON(GM.Global.CRFCriteria);
            if(GM.Global.CRF==undefined)
                GM.Global.CRF = {"Filter":""};
            GM.Global.CRF["Filter"] = GM.Global.CRFID;
            GM.Global.CRF["FilterName"] = GM.Global.CRFCriteria;
            if($("#crf-frmdate").val()!="")
                this.activityDate();
            if($("#crf-frmdate").val()!=""|| GM.Global.CRFID!=null) {
                window.location.href = '#crf/list/0';
            }
        },
        
        // Delete the favorite item from the criteria page
        removeFavorite: function(e){
            showNativeConfirm("Do you want to delete this favorite","Confirm",["Yes","No"], function(index) {
                if (index == 1) {
                    var input = {};
                    input.token = localStorage.getItem('token');
                    input.criteriaid = $(e.currentTarget).attr('criteriaid');
                    input.criterianm = $(e.currentTarget).parent().find('a').text();
                    input.criteriatyp = "crf";
                    input.voidfl = "Y";
                    input.listGmCriteriaAttrVO = [];
                    console.log(input);
                    fnGetWebServerData("criteria/save", undefined, input, function(data){
                        if(data!=null) {
                            $(e.currentTarget).parent().parent().remove();
                        }
                    });
                }
            });
            
        },
        
        toggleTabs: function(event) {
            console.log($(event.currentTarget));
            colorControl(event, "sub-tab", "gmBGEmpty", "gmBGDarkerGrey gmFontWhite");
            $(".crf-sub-content").addClass("gmMediaDisplayNone");
            var target = $(event.currentTarget).attr("id");
            
            if(target == "crf-tab-search") {
          
                $(".crf-search-content").removeClass("gmMediaDisplayNone");
                $(".crf-criteria-phone-style").addClass("gmMediaDisplayNone");
                 $(".crf-favorite-criteria").addClass("gmMediaDisplayNone");
            }
            else if(target == "crf-tab-category") {
          
                $(".crf-criteria-phone-style").removeClass("gmMediaDisplayNone");
                $(".crf-search-content").addClass("gmMediaDisplayNone");
                 $(".crf-favorite-criteria").addClass("gmMediaDisplayNone");
            }
            else if(target == "crf-tab-favorite") {
              
                $(".crf-favorite-criteria").removeClass("gmMediaDisplayNone");
                 $(".crf-criteria-phone-style").addClass("gmMediaDisplayNone");
                $(".crf-search-content").addClass("gmMediaDisplayNone");
            }
            else {
                $(".crf-search-content").removeClass("gmMediaDisplayNone");
            }
        },
        
        toggleSelectedItems: function (event) {
            if ($("#criteria-selected-itmes").hasClass("gmMediaDisplayNone")) {
                $("#criteria-selected-itmes").removeClass("gmMediaDisplayNone");
                $("#selected-criteria-icon").find(".fa-chevron-down").addClass("fa-chevron-up").removeClass("fa-chevron-down");
            } else {
                $("#criteria-selected-itmes").addClass("gmMediaDisplayNone");
                $("#selected-criteria-icon").find(".fa-chevron-up").addClass("fa-chevron-down").removeClass("fa-chevron-up");
            }
        },
        
        //Handles the orietaion change in the iPad
        onOrientationChange: function(event) {
            resizeHeadWidth('.gmPanelHead', '.gmBtnGroup', '#panelhead-left');
        },

    });
    return GM.View.CRFCriteria;
}); 
