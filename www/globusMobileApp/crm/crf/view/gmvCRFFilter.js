define([
     // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars', 'jqueryui',
    
     //Common
    'global',
    
    //View
    'gmvCRMSelectPopup',

    // Pre-Compiled Template
    'gmtTemplate'
], 

function ($, _, Backbone, Handlebars, JqueryUI,
           Global,
           GMVCRMSelectPopup, GMTTemplate) {

    'use strict';
    
    GM.View.TrainingFilter = Backbone.View.extend({

        el: "#div-crm-module",

        //Load the templates
        template_MainModule: fnGetTemplate(URL_Common_Template, "gmtCRMMainModule"),
        template_filter: fnGetTemplate(URL_Training_Template, "gmtTrainingFilter"),
        template_selected_items: fnGetTemplate(URL_Common_Template, "gmtPopupSelectedItem"),
        
        events: {
            "click #btn-main-save": "saveFilter",
            "click .spn-name-x": "removeName",
            "change #txt-training-fltr-fdate, #txt-training-fltr-tdate": "getDate",
            "click #btn-main-cancel": "close",
            "click  .btn-listby"             : "openPopup",
            "click #btn-category-search": "searchFilter",
            "click #btn-filter-selected-criteria": "showSelectedCriteria",
            "click .div-text-selected-crt":  "toggleSelectedItems"
        },

	    initialize: function(options) {
            $(window).on('orientationchange', this.onOrientationChange);
            this.filterID = options.filterid;
            hideMessages();
            this.arrSelectedList = new Array();
            this.tempJSON = {};
        },
        
         // Render the initial contents when the Training Filter View is instantiated
        render: function() {
            this.$el.html(this.template_MainModule({"module":"training"}));
            this.$(".gmPanelHead").addClass("gmBGtraining");
            this.$(".gmPanelTitle").html("Create a new Filter");
            this.$("#crm-btn-back").removeClass("hide");
            this.$(".gmBtnGroup #li-save").removeClass("hide");
            this.$(".gmBtnGroup #li-cancel").removeClass("hide");

            this.$("#div-main-content").html(this.template_filter());
            
            $("#training-frmdate").datepicker({
                    onSelect: function (selected) {
                        var dt = new Date(selected);
                        dt.setDate(dt.getDate());
                        $("#training-todate").datepicker("option", "minDate", dt);
                        $("#training-frmdate").trigger("blur");
                    }
            });
            $("#training-todate").datepicker({
                 onSelect: function (selected) {
                    var dt = new Date(selected);
                    dt.setDate(dt.getDate());
                    $("#training-todate").datepicker("option", dt);
                    $("#training-todate").trigger("blur");
                }
            });

            this.jsnData = {};
            this.jsnData.criteriatyp = "training";
            this.jsnData.token = localStorage.getItem('token');

            this.stateList = new Array();
            this.productsList = new Array();

            if(this.filterID !=0)
                this.getFilterData();
            
            return this;
        },
        
        //load the data if edit into the view
        getFilterData: function () {
            this.arrAttribute = new Array();
            
            var that = this;
            var input = { "token": localStorage.getItem("token"), "criteriaid": this.filterID };
            if(GM.Global.Device){
                fnGetOfflineCriteriaSavedData(this.filterID, function(existData){
                    console.log("existData of CRF Filter");
                    console.log(existData);
                    if(existData.length){
                       console.log(existData)
                        existData = $.parseJSON(existData[0].data);
                        that.criteriaData(existData);
                    }
                    else{
                        fnGetCriteriaAttr(that.filterID,that.module, function(data){
                            if(data.length){
                                console.log(data);
                                data = data[0]
                                that.criteriaData(data);
                            }
                        });
                    }
                });
            }
            else {
                fnGetWebServerData("criteria/get", undefined, input, function (data) {
                    if(data!=null)
                        that.criteriaData(data);
                });
            }
        },
        
        criteriaData: function(data){
            var that = this;
            that.$(".gmPanelTitle").html(data.criterianm);
            that.$("#div-training-phone-title").html(data.criterianm);
            that.$(".txt-training-fltr-name").val(data.criterianm);
            GM.Global.myFilter = null;
            GM.Global.myFilter= JSON.stringify({"Name":data.criterianm,"ID":data.criteriaid});
            that.jsnData.criteriaid = data.criteriaid;
            data.listGmCriteriaAttrVO = arrayOf(data.listGmCriteriaAttrVO);
            GM.Global.criteriaAttr = [];
            GM.Global.criteriaAttr = data.listGmCriteriaAttrVO;
            _.each(data.listGmCriteriaAttrVO, function (data) {

                if (data.key == "crtfrmdate") {
                    that.frmdate = data.value;
                    console.log(that.frmdate);
                    that.$("#training-frmdate").val(data.value);

                    var frmdate = new Date(data.value);
                    var day = ("0" + frmdate.getDate()).slice(-2);
                    var month = ("0" + (frmdate.getMonth() + 1)).slice(-2);
                    var crtfrmdate = frmdate.getFullYear()+"-"+(month)+"-"+(day) ;

                    that.$("#div-training-phone-content .training-frmdate").val(crtfrmdate);
                    that.tempJSON["From Date"] = that.frmdate;
                }
                if (data.key == "crttodate") {
                    that.todate = data.value;
                    that.$("#training-todate").val(data.value);

                    var todate = new Date(data.value);
                    var day = ("0" + todate.getDate()).slice(-2);
                    var month = ("0" + (todate.getMonth() + 1)).slice(-2);
                    var crttomdate = todate.getFullYear()+"-"+(month)+"-"+(day) ;

                    that.$("#div-training-phone-content .training-todate").val(crttomdate);
                    that.tempJSON["To Date"] = that.todate;
                }

                if (data.key == "state") {
                    that.stateList.push(data.value);
                }
                if (data.key == "procedure") {
                    that.techqList.push(data.value);
                }
                that.newArray = _.union(that.stateList);
            });

            var saveLocalJSON = {};
            if(that.productsList.toString()!="")
                saveLocalJSON["systemid"] = that.productsList.toString();
            if(that.stateList.toString()!="")
                saveLocalJSON["state"] = that.stateList.toString();

            if(that.frmdate!=undefined && that.frmdate.toString()!="")
                saveLocalJSON["crtfrmdate"] = that.frmdate;
            if(that.todate!=undefined && that.frmdate.toString()!="")
                saveLocalJSON["crttodate"] = that.todate;
            if(GM.Global.Training==undefined)
                GM.Global.Training = {"Filter":""};

            GM.Global.TrainingID = saveLocalJSON;
            console.log(saveLocalJSON)
            GM.Global.Training.Filter =  saveLocalJSON;

            if(that.newArray != "")
                that.getCodeName(that.newArray);
            else if(that.productsList != "")
                that.getSystemName(that.productsList);
            else
                that.popUpTemplate();

            _.each(data.listGmCriteriaAttrVO, function(data){
                delete data.userid;
                delete data.token;
                delete data.stropt;
            });
        },
        
        getSystemName: function (sysid) {
            var that = this;
            var sysid = sysid.toString();
            var systemInput = { "token": localStorage.getItem("token"), "sysid": sysid };
            fnGetWebServerData("search/system", "gmSystemBasicVO", systemInput, function (data) {
                data = arrayOf(data);
                if (sysid != undefined) {
                    var systemList = _.pluck(data, "sysnm");
                    var systemID = _.pluck(data, "sysid");
                    that.tempJSON["Product Used"] = systemList.toString().replace(/,/g," / ");

                    if (systemList != null) {
                        var countVal = systemList.length;
                        that.defaultVals = systemID;
                        $(".div-surg-popup-system").attr("data-values", that.defaultVals);
                        $(".div-surg-popup-system").find("span").text("(" + countVal + ")");
                        $(".div-surg-popup-system").css("background", "#FDF5E6");
                    }
                }
                that.popUpTemplate();
            });          
        },
        getCodeName: function (codeid) {
            var that = this;
            var codeid = codeid.toString();
            if(codeid != "") {
                var input = { "token": localStorage.getItem("token"), "codeid": codeid };
                fnGetWebServerData("search/codelookup", "gmCodeLookUpVO", input, function (data) {

                    data = arrayOf(data);
                    if (that.stateList != "") {
                        var stateResult = _.filter(data, function (data) {
                            return data.codegrp == "STATE";
                        });
                        var stateList = _.pluck(arrayOf(stateResult), "codenm");
                        console.log(stateList.toString().replace(/,/g," / "));
                        var stateID = _.pluck(arrayOf(stateResult), "codeid");
                        that.tempJSON["Location"] = stateList.toString().replace(/,/g," / ");
                        var countVal = stateResult.length;
                        that.defaultVals = stateID;
                        $(".div-surg-popup-state").attr("data-values", that.defaultVals);
                        $(".div-surg-popup-state").find("span").text("(" + countVal + ")");
                        $(".div-surg-popup-state").css("background", "#FDF5E6");

                    }
                    if (that.techqList != "") {
                        var techqResult = _.filter(data, function (data) {
                            return data.codegrp == "TECHQ";
                        });
                        var techqList = _.pluck(arrayOf(techqResult), "codenm");
                        var techqID = _.pluck(arrayOf(techqResult), "codeid");
                        that.tempJSON["Technique"] = techqList.toString().replace(/,/g," / ");
                        var countVal = techqResult.length;
                        that.defaultVals = techqID;
                        $(".div-surg-popup-technique").attr("data-values", that.defaultVals);
                        $(".div-surg-popup-technique").find("span").text("(" + countVal + ")");
                        $(".div-surg-popup-technique").css("background", "#FDF5E6");
                    }
                    if (that.skillList != "") {
                        var specalityResult = _.filter(data, function (data) {
                            return data.codegrp == "SGSPC";
                        });
                        var specalityList = _.pluck(arrayOf(specalityResult), "codenm");
                        var specalityID = _.pluck(arrayOf(specalityResult), "codeid");
                        that.tempJSON["Skillset"] = specalityList.toString().replace(/,/g," / ");
                        var countVal = specalityResult.length;
                        that.defaultVals = specalityID;
                        $(".div-surg-popup-skills").attr("data-values", that.defaultVals);
                        $(".div-surg-popup-skills").find("span").text("(" + countVal + ")");
                        $(".div-surg-popup-skills").css("background", "#FDF5E6");
                    }
                    if (that.roleList != "") {
                        var roleResult = _.filter(data, function (data) {
                            return data.codegrp == "ACTROL";
                        });
                        var roleList = _.pluck(arrayOf(roleResult), "codenm");
                        var roleID = _.pluck(arrayOf(roleResult), "codeid");
                        that.tempJSON["Role"] = roleList.toString().replace(/,/g," / ");
                        var countVal = roleResult.length;
                        that.defaultVals = roleID;
                        $(".div-surg-popup-role").attr("data-values", that.defaultVals);
                        $(".div-surg-popup-role").find("span").text("(" + countVal + ")");
                        $(".div-surg-popup-role").css("background", "#FDF5E6");
                    }
                    if (that.activityList != "") {
                        var acttypResult = _.filter(data, function (data) {
                            return data.codegrp == "SGACTP";
                        });
                        var acttypList = _.pluck(arrayOf(acttypResult), "codenm");
                        var acttypID = _.pluck(arrayOf(acttypResult), "codeid");
                        that.tempJSON["Activity Type"] = acttypList.toString().replace(/,/g," / ");
                        var countVal = acttypResult.length;
                        that.defaultVals = acttypID;
                        $(".div-surg-popup-activity").attr("data-values", that.defaultVals);
                        $(".div-surg-popup-activity").find("span").text("(" + countVal + ")");
                        $(".div-surg-popup-activity").css("background", "#FDF5E6");
                    }
                    if (that.productsList != ""){
                        that.getSystemName(that.productsList);
                    }
                    else {
                        that.popUpTemplate();
                    }
                }); 
            }
            else if (that.productsList != ""){
                constant.log(that.productsList);
                that.getSystemName(that.productsList);
            }
        },
        
        popUpTemplate: function () {
            var that = this;
            var tempArray = new Array();
            GM.Global.Training.FilterName = JSON.stringify(that.tempJSON); 
            console.log(GM.Global.Training.FilterName)
            console.log(JSON.stringify(that.tempJSON))
            $.each(that.tempJSON, function (key, val) {
                if (val != "") {
                    tempArray.push({ "title": key, "value": val });
                }
            });
            var criteriaValues = $.parseJSON(JSON.stringify(tempArray));
            if(criteriaValues != "")
                $(".list-category-item").html(that.template_selected_items(criteriaValues));
            $(".div-text-selected-crt").addClass("gmFonttraining");
          
        },

        //cancels the filterview and redirects to criteria view
        close: function() {
            window.history.back();
        },
        openPopup: function(event){
            var that = this;
            
            that.systemSelectOptions = {                                                                                                              
                'title': $(event.currentTarget).attr("data-title"),
                'storagekey': $(event.currentTarget).attr("data-storagekey"),
                'webservice': $(event.currentTarget).attr("data-webservice"),   
                'resultVO': $(event.currentTarget).attr("data-resultVO"),
                'IDParameter': $(event.currentTarget).attr("data-IDParameter"),
                'NameParameter': $(event.currentTarget).attr("data-NameParameter"),
                'inputParameters': $(event.currentTarget).attr("data-inputParameters"),
                'codegrp': $(event.currentTarget).attr("data-codegrp"),
                'module': "training",
                'event': event,
                'parent': this,
                'moduleLS': GM.Global.TrainingID,
                'moduleCLS': GM.Global.Training.FilterName,
                'fnName': fnGetCodeNMByGRP,
                'callback': function(ID, Name){
                    GM.Global.TrainingID = ID;
                    GM.Global.Training.FilterName = Name;
                    console.log(ID);
                    console.log(Name)
                }
            };
            
            that.systemSelectView = new GMVCRMSelectPopup(that.systemSelectOptions);
        },
         // search by activity date
        activityDate: function () {
            var that = this;
            var fromdate = $("#training-frmdate").val();
            var todate = $("#training-todate").val();
            
            if(fromdate !="" && todate !="") {
//                var dd, mm, yyyy, date;
//                date = new Date(fromdate);
//
//                dd = ("0" + date.getDate()).slice(-2);
//                mm = ("0" + (date.getMonth() + 1)).slice(-2);
//                yyyy = date.getFullYear();
                
                if (GM.Global.TrainingID == undefined)
                    GM.Global.TrainingID = {};
                
                if(GM.Global.Training.FilterName == undefined)
                    GM.Global.Training.FilterName = {};
                
                if(GM.Global.Training.FilterName!="" && typeof(GM.Global.Training.FilterName)!="object")
                    GM.Global.Training.FilterName = $.parseJSON(GM.Global.Training.FilterName);
                if(GM.Global.TrainingID!="" && typeof(GM.Global.TrainingID)!="object")
                    GM.Global.TrainingID = $.parseJSON(GM.Global.TrainingID);
//
//                GM.Global.TrainingID["crtfrmdate"] = mm + "/" + dd + "/" + yyyy;
//                GM.Global.Training.FilterName["From Date"] = mm + "/" + dd + "/" + yyyy;
                GM.Global.TrainingID["crtfrmdate"] = getDateByFormat(fromdate);
                GM.Global.Training.FilterName["From Date"] = getDateByFormat(fromdate);
//                
//                date = new Date(todate);
//                dd = ("0" + date.getDate()).slice(-2);
//                mm = ("0" + (date.getMonth() + 1)).slice(-2);
//                yyyy = date.getFullYear();
//
//                GM.Global.TrainingID["crttodate"] = mm + "/" + dd + "/" + yyyy;
//                GM.Global.Training.FilterName["To Date"] = mm + "/" + dd + "/" + yyyy;
                GM.Global.TrainingID["crttodate"] = getDateByFormat(todate);
                GM.Global.Training.FilterName["To Date"] = getDateByFormat(todate);
            }
        },    
        
        // Save currently selected data as favorite criteria
        saveFilter: function (event) {
            this.activityDate();
            var that =this;
            
            console.log(GM.Global.TrainingID);
            console.log($.parseJSON(GM.Global.TrainingID));
            this.favoriteData = GM.Global.TrainingID;
            console.log($.parseJSON(JSON.stringify(this.favoriteData)));
            that.jsnData = {};
            var token = localStorage.getItem("token");
            var criterianm = this.$(".txt-training-fltr-name").val();
            if (criterianm != "") {
                
                that.jsnData.token = token;
                that.jsnData.criteriaid = that.filterID;
                that.jsnData.criterianm = criterianm;
                var key, value;
                $.each(this.favoriteData, function (key, value) {
                    if(value != "" && key != "token")
                        that.arrAttribute.push({"key": key, "value": value, "attrid": "", "criteriaid": that.jsnData.criteriaid, "voidfl": ""});
                });
                
                var temp1 = [];
                _.each(GM.Global.criteriaAttr, function(data){
                    
                    var chk = "";
                    _.each(that.arrAttribute, function(dt, i){
                        if(dt.key == data.key){
                            dt.attrid = data.attrid;
                            chk = "Y";
                        }
                    });
                    if(chk == ""){
                        data.voidfl = "Y";
                        temp1.push(data);
                    }
                    
                    delete data.cmpid;
                    delete data.cmptzone;
                    delete data.cmpdfmt;
                    delete data.plantid;
                    delete data.partyid;
                });
                if(temp1.length){
                    _.each(temp1, function(data){
                        that.arrAttribute.push(data);
                    });
                }  
                that.jsnData.criteriatyp = "crf";
                that.jsnData.token =  GM.Global.Token;
                that.jsnData.listGmCriteriaAttrVO = $.parseJSON(JSON.stringify(that.arrAttribute));
                if(GM.Global.Device){
                    console.log(that.jsnData.criteriaid);
                    fnSaveCriteria(that.jsnData.criteriaid, that.jsnData, function(data, id){
                        if(data == "success"){
                            GM.Global.myFilter = null;
                            GM.Global.myFilter= JSON.stringify({"Name":that.jsnData.criterianm,"ID":id});
                            $(".li-new-favorite").addClass("hide");
                            $(".li-exist-favorite").addClass("hide");
                            $("#div-favorite-title").html(criterianm);
                            showSuccess("Favorite is updated successfully");
                            window.location.href = '#crm/crf/list/'+that.filterID;
                            GM.Global.Filtersave = "true";
                            GM.Global.FilterName = that.jsnData.criterianm;
                            hideMessages();
                        }
                        else
                            showError("Favorite save is failed!");
                    });
                }
                else{
                    fnGetWebServerData("criteria/save", undefined, that.jsnData, function (data) {
                        if (data != null) {
                            GM.Global.myFilter = null;
                            GM.Global.myFilter= JSON.stringify({"Name":data.criterianm,"ID":data.criteriaid});
                            $(".li-new-favorite").addClass("hide");
                            $(".li-exist-favorite").addClass("hide");
                            $("#div-favorite-title").html(criterianm);
                            GM.Global.FilterName = data.criterianm;
                             GM.Global.Filtersave = "true";
                            showSuccess("Favorite is updated successfully");
                            window.location.href = '#crm/crf/list/'+that.filterID;
                            hideMessages();
                            fnUpdateCriteria();
                        } else
                            showError("Favorite save is failed!");
                    });
                }
            } else
                showError("Please enter the Favorite Name");

        },
        // Search category vise
        searchFilter: function () {
            this.activityDate();
            window.location.href = '#training/list/0';
        },
        showSelectedCriteria: function (event) {
            if($(event.currentTarget).hasClass("details-opened")) {
                $(".details-opened").removeClass("details-opened");
               $(".list-category-item").addClass("hide"); 
            }
            else {
                $(event.currentTarget).addClass("details-opened");
                $(".list-category-item").removeClass("hide"); 
            }
        },
        toggleSelectedItems: function (event) {
            if ($("#criteria-selected-itmes").hasClass("gmMediaDisplayNone")) {
                $("#criteria-selected-itmes").removeClass("gmMediaDisplayNone");
                $("#selected-criteria-icon").find(".fa-chevron-down").addClass("fa-chevron-up").removeClass("fa-chevron-down");
            } else {
                $("#criteria-selected-itmes").addClass("gmMediaDisplayNone");
                $("#selected-criteria-icon").find(".fa-chevron-up").addClass("fa-chevron-down").removeClass("fa-chevron-up");
            }
        },
        //Handles the orietaion change in the iPad
        onOrientationChange: function(event) {
            resizeHeadWidth('.gmPanelHead', '.gmBtnGroup', '#panelhead-left');
        },
    });

	return GM.View.TrainingFilter;
});
