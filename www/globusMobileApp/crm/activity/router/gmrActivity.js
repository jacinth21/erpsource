/**********************************************************************************
 * File:        gmrPhysicianvisit.js
 * Description: Router for all of Physicianvisit
 * Version:     1.0
 * Author:      cskumar
 **********************************************************************************/
define([

  // External Libraries
  'jquery', 'underscore', 'backbone',

  //Common
'gmvCRMMain',

  //Views
  'gmvCRMData', 'gmvActivityCriteria', 'gmvActivityDetail', 'gmvActivityFilter', 'gmvCRMDashboard', 'gmvSurgeonDetail', 'gmvCRFDetail',

    //local db
    'crmdb'
],

    function ($, _, Backbone,
        GMVCRMMain, GMVCRMData, GMVActivityCriteria, GMVActivityDetail, GMVActivityFilter, GMVCRMDashboard, GMVSurgeonDetail, GMVCRFDetail, CRMDB) {

        'use strict';

        GM.Router.Activity = Backbone.Router.extend({

            //routes to its respective web page 
            routes: {
                "crm/:module": "viewModule",
                "crm/:module/:actid/:id/:firstnm/:lastnm": "viewModule",
                "crm/:module/list": "listActivities",
                "crm/:module/filter/:filterid": "showFilter",
                "crm/:module/id/:actid": "activityDetail",
                "crm/:module/list/:criteriaid": "listActivity"
            },

            //things to execute before the routing
            execute: function (callback, args, name) {
                var lastEl = GM.Global.Url[GM.Global.Url.length-2];
            if(lastEl.indexOf("#crm") == -1)
                GM.Global.CRMMain=undefined;
                if (GM.Global.CRMMain == undefined) {
                    commonSetUp();
                    GM.Global.CRMMain = true;
                    var gmvCRMMain = new GMVCRMMain();
                    gmvApp.showView(gmvCRMMain);
                }
                $('.gmTab').removeClass().addClass('gmTab');
                $("#" + args[0]).addClass("gmTab gmFontWhite tab" + args[0]);

                if (args[0] == "crf-pending")
                    $("#crf").addClass("gmTab tabcrf-pending gmFontWhite");
                if (args[0] == "crf-reviewed")
                    $("#crf").addClass("gmTab tabcrf-reviewed gmFontWhite");

                GM.Global.FileUploadData = undefined;

                if (GM.Global.AlertMsg) {
                    showError(GM.Global.AlertMsg);
                    GM.Global.AlertMsg = undefined;
                } else {
                    toErr = setTimeout(function () {
                        hideMessages();
                    }, 2500)
                }


                if (callback) callback.apply(this, args);
                //$("#div-app-title").removeClass("gmMediaVisibilityH").addClass("gmMediaVisibilityV");
                $(".div-moble-menu").removeClass("gmMediaVisibilityH").addClass("gmMediaVisibilityV");
            },

            initialize: function () {
                GM.Global.Activity = null;
                $("#div-app-title").show();
                $("#span-app-title").html(this.title);
            },

            // Display detail by id
            activityDetail: function (module, actid) {
                console.log("Activity Detail Module " + module);
                if (GM.Global.Activity == undefined)
                    GM.Global.Activity = null;
                var input = {
                    "token": localStorage.getItem("token"),
                    "actid": actid
                };
                if (actid != 0) {
                    var template = fnGetTemplate(URL_Common_Template, "gmtSpinner");
                    $("#div-crm-module").html(template());
                    if (GM.Global.Device) {
                        activityInfo(actid, function (data) {
                            var gmvActivityDetail = new GMVActivityDetail({
                                module: module,
                                "actid": actid,
                                "data": data
                            });
                            gmvApp.showView(gmvActivityDetail);
                        }, module, "gmvActivity");
                    } else {
                        console.log("Input is " + JSON.stringify(input));
                        fnGetWebServerData("crmactivity/info", "gmCRMActivityVO", input, function (data) {
                            if (data != null && data.actid != "") {
                                data = data.GmCRMListVO.gmCRMActivityVO[0];
                                data.arrgmactivitypartyvo = arrayOf(data.arrgmactivitypartyvo);
                                data.arrgmactivityattrvo = arrayOf(data.arrgmactivityattrvo);
                                data.arrgmcrmaddressvo = arrayOf(data.arrgmcrmaddressvo);
                                data.arrgmcrmlogvo = arrayOf(data.arrgmcrmlogvo);
                                console.log("activity info data " + JSON.stringify(data));
                                var gmvActivityDetail = new GMVActivityDetail({
                                    module: module,
                                    "actid": actid,
                                    "data": data
                                });
                                gmvApp.showView(gmvActivityDetail);
                            } else {
                                GM.Global.AlertMsg = "No Such Activity";
                                window.location.href = "#crm/" + module;
                            }

                        });
                    }
                } else {


                    if ((!navigator.onLine) && ((module == "training") || (module == "merc"))) {
                        showError("You need Internet to create Activity");
                    } else {
                        var gmvActivityDetail = new GMVActivityDetail({
                            module: module,
                            "actid": 0,
                            "data": {}
                        });
                        gmvApp.showView(gmvActivityDetail);
                    }
                }

            },

            CRFDetail: function (controlID, partyID) {
                var gmvCRFDetail = new GMVCRFDetail({
                    controlid: controlID,
                    partyid: partyID
                });
                gmvApp.showView(gmvCRFDetail);
            },

            listActivities: function (module) {
                if (GM.Global.Activity == undefined) {
                    GM.Global.Activity = {}
                    GM.Global.Activity.Filter = null;
                }

                var gmvCRMData = new GMVCRMData({
                    "module": module
                });
                gmvApp.showView(gmvCRMData);
            },

            listActivity: function (module, criteriaid) {
                if (GM.Global.Activity == undefined) {
                    GM.Global.Activity = {}
                    GM.Global.Activity.Filter = null;
                }
                if (GM.Global.FieldSales == undefined)
                    GM.Global.FieldSales = {}

                if (GM.Global.SalesRep && criteriaid == 0)
                    GM.Global.Activity["criteriaid"] = "0";
                else if (module == "fieldsales") {
                    GM.Global.FieldSales["criteriaid"] = criteriaid;
                } else
                    GM.Global.Activity["criteriaid"] = criteriaid;
                var gmvCRMData = new GMVCRMData({
                    "module": module
                });
                gmvApp.showView(gmvCRMData);
            },

            viewModule: function (module, actid, id, firstnm, lastnm) {
                if (module == "dashboard") {
                    console.log(GM.Global.CRMMain)
                    if (GM.Global.CRMMain == undefined) {
                        commonSetUp();
                        GM.Global.CRMMain = true;
                        var gmvCRMMain = new GMVCRMMain();
                        gmvApp.showView(gmvCRMMain);
                    }
                    var gmvCRMDashboard = new GMVCRMDashboard(GM.Global.UserTabs);
                    gmvApp.showView(gmvCRMDashboard);
                } else if (module == "tradeshow") {
                    //                $(".gmTab").removeClass()
                    $("#lead").addClass("tablead");
                    if (GM.Global.Activity && GM.Global.Activity.Mode == "view")
                        GM.Global.Activity.Mode = undefined;
                    var gmvActivityDetail = new GMVActivityDetail({
                        module: module,
                        "actid": undefined,
                        "data": {}
                    });
                    gmvApp.showView(gmvActivityDetail);
                } else if (id) {
                    var surgeonid = id;
                    var firstnm = firstnm;
                    var lastnm = lastnm;
                    var sourceid;
                    GM.Global.ActSurgInfo = {
                        sourceid: sourceid,
                        surgeonid: surgeonid,
                        firstnm: firstnm,
                        lastnm: lastnm
                    };

                    firstnm = (firstnm !== "") ? firstnm : "undefined";
                    lastnm = (lastnm !== "") ? lastnm : "undefined";

                    var gmvActivityDetail = new GMVActivityDetail({
                        module: module
                    });
                    gmvApp.showView(gmvActivityDetail);
                } else {
                    if (isRep() && (module == "lead")) {
                        GM.Global.Activity = {
                            RepID: localStorage.getItem("partyID")
                        };
                        this.listActivity(module);
                    } else {
                        var gmvActivityCriteria = new GMVActivityCriteria({
                            "module": module
                        });
                        gmvApp.showView(gmvActivityCriteria);
                    }
                }
            },

            showFilter: function (module, filterid) {
                var gmvActivityFilter = new GMVActivityFilter({
                    module: module,
                    filterid: filterid
                });
                gmvApp.showView(gmvActivityFilter);
            },
        });
        return GM.Router.Activity;
    });
