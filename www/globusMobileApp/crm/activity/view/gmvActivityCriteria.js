define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars', 'jqueryui',

    // Common
    'global', 'gmvSearch',

    // View
    'gmvCRMData', 'gmvCRMSelectPopup', 'gmvActivityDetail','gmvPSUtil',

    // Pre-Compiled Template
    'gmtTemplate'

    //Model

],

    function ($, _, Backbone, Handlebars, JqueryUI, Global, GMVSearch, GMVCRMData, GMVCRMSelectPopup, GMVActivityDetail, GMTTemplate, GMVPSUtill) {

        'use strict';

        GM.View.ActivityCriteria = Backbone.View.extend({

            el: "#div-crm-module",

            name: "CRM ActivityCriteria View",

            /* Load the templates */
            template_MainModule: fnGetTemplate(URL_Common_Template, "gmtCRMMainModule"),
            template_FieldSalesCriteria: fnGetTemplate(URL_FieldSales_Template, "gmtFieldSalesCriteria"),
            template_LeadCriteria: fnGetTemplate(URL_Lead_Template, "gmtLeadCriteria"),
            template_MERCCriteria: fnGetTemplate(URL_MERC_Template, "gmtMERCCriteria"),
            template_PhysicianVisitCriteria: fnGetTemplate(URL_PhysicianVisit_Template, "gmtPhysicianVisitCriteria"),
            template_visitSchedule: fnGetTemplate(URL_PhysicianVisit_Template, "gmtVisitsScheduler"),
            template_SchedulerInfo: fnGetTemplate(URL_PhysicianVisit_Template, "gmtSchedulerInfo"),
            template_SchedulerEventInfo: fnGetTemplate(URL_PhysicianVisit_Template, "gmtSchedulerEventInfo"),
            template_SalesCallCriteria: fnGetTemplate(URL_SalesCall_Template, "gmtSalesCallCriteria"),
            template_TrainingCriteria: fnGetTemplate(URL_Training_Template, "gmtTrainingCriteria"),
            template_FilterButtons: fnGetTemplate(URL_Common_Template, "gmtFilterButtons"),
            template_CRFCriteria: fnGetTemplate(URL_CRF_Template, "gmtCRFCriteria"),
            template_selected_items: fnGetTemplate(URL_Common_Template, "gmtPopupSelectedItem"),

            events: {
                "click #btn-main-create": "createActivity",
                "click .btn-upcoming": "showUpcoming",
                "click .btn-requested": "showRequested",
                "click #btn-search-name": "lookupByName",
                "click #btn-search-title": "lookupByTitle",
                "click #btn-search-keyword": "lookupByKeyword",
                "click #btn-search-npid": "lookupByNPI",
                "keypress input": "clickDefaultButton",
                "click  .btn-listby": "openPopup",
                "click .sub-tab": "toggleTabs",
                "click #btn-category-search": "searchCategory",
                "click .gmBtnCrDelete": "removeFavorite",
                "click #btn-search-actid": "lookupByActID",
                "change #activity-frmdate, #activity-todate": "setMinDate",
                "click .div-text-selected-crt": "toggleSelectedItems",
                "click .btn-visitSchedule": "showVisitSchedule",
                "click #btn-main-close": "closeVisitSchedule",
                "click .a-view-docs": "viewDocument",
                "click #scheduler-legend-icon": "showLegend",
                "click #span-activity-frmdate,#span-activity-todate": "loadDatePicker",
                "change #activity-frmdate,#activity-todate": "displaySelctdDt"
            },

            initialize: function (options) {
                var that = this;
                GM.Global.myFilter = null;
//                GM.Global.SCDATA = null;
                GM.Global.Activity = {};
                GM.Global.Status = {};
                GM.Global.Activity.FilterID = {};
                GM.Global.Activity.FilterName = {};
                GM.Global.SchedulerFL = "";
                $(window).on('orientationchange', this.onOrientationChange);
                this.module = options.module;
                $("#div-app-title").show();
                $('#div-app-title').removeClass('gmVisibilityHidden').addClass('gmVisibilityVisible');
                $("#div-app-title").html("CRM");
            },

            render: function () {
                var that = this;
                this.$el.html(this.template_MainModule({
                    "module": this.module
                }));

                if (_.contains(GM.Global.Admin, this.module) && this.module != "crf")
                    this.$("#li-create").removeClass("hide");

                if (_.contains(GM.Global.Admin, "physvisitrqst") && this.module == "physicianvisit")
                    this.$("#li-create").removeClass("hide");

                switch (this.module) {
                    case "fieldsales":
                        this.template = this.template_FieldSalesCriteria();
                        $("#span-app-title").html("Field Sales");
                        this.$(".gmPanelTitle").html("Lookup FieldSales");
                        break;
                    case "lead":
                        this.template = this.template_LeadCriteria();
                        $("#span-app-title").html("Lead");
                        this.$(".gmPanelTitle").html("Lookup Lead");
                        $("#li-tradeshow").removeClass("hide");
                        break;
                    case "merc":
                        this.template = this.template_MERCCriteria();
                        $("#span-app-title").html("MERC");
                        this.$(".gmPanelTitle").html("Lookup MERC");
                        break;
                    case "physicianvisit":
                        this.template = this.template_PhysicianVisitCriteria();
                        $("#span-app-title").html("Physician Visit");
                        this.$(".gmPanelTitle").html("Lookup Physician Visit");
                        break;
                    case "salescall":
                        this.template = this.template_SalesCallCriteria();
                        $("#span-app-title").html("Sales Call");
                        this.$(".gmPanelTitle").html("Lookup Sales Call");
                        break;

                    case "training":
                        this.template = this.template_TrainingCriteria();
                        $("#span-app-title").html("Training");
                        this.$(".gmPanelTitle").html("Lookup Training");
                        break;
                    case "crf":
                        this.template = this.template_CRFCriteria();
                        $("#span-app-title").html("CRF");
                        this.$(".gmPanelTitle").html("Lookup CRF");
                        break;
                }

                
                this.$("#div-main-content").html(this.template);

                var scheduleObj = {};
                getCodeLookUpVal("", "", "DIVLST", "", "", function (divisionDt) {
                getCodeLookUpVal("", "", "SGACTP", "PHYVST", "", function (typeDt) {
                getCodeLookUpVal("", "", "ACTSTS", "", "105266", function (statusDt) {
                     scheduleObj = {
                                "module": that.module,
                                "division": divisionDt,
                                "type": typeDt,
                                "status": statusDt,
                                };
                that.$("#scheduler-main-container").html(that.template_visitSchedule(scheduleObj));
                        });
                    });
                });

                if (this.module == "salescall") {
                    $("#scheduler-legend").css({"left": "300px"});
                    $("#filters_wrapper").addClass("hide");
                    $("#dhtmlx-filter-division").addClass("hide");
                    $("#salescall-scheduler-rep-search").removeClass("hide");
                    if(!isRep())
                        this.searchRep();
                } else {
                    $("#filters_wrapper").removeClass("hide");
                    $("#dhtmlx-filter-division").removeClass("hide");
                    $("#salescall-scheduler-rep-search").addClass("hide");
                }
                this.$("#crm-btn-back").hide();

                var input = {
                    "token": localStorage.getItem("token"),
                    "criteriatyp": this.module
                };
                console.log(JSON.stringify(input));
                fnGetWebServerData("criteria/list", undefined, input, function (data) {
                    console.log(data)
                    if (data != null) {
                        that.$(".gmMyFav li").removeClass('hide');
                        that.$(".gmMyFavContent").html(that.template_FilterButtons(arrayOf(data.gmCriteriaVO)));
                        that.$(".gmBtnCr").addClass("gmBG" + that.module);
                    } else
                        that.$(".gmMyFav").addClass('hide');
                    if (GM.Global.SwitchUser) {
                        $(".gmBtnCrDelete").addClass("hide");
                        $(".gmBtnCrEdit").addClass("hide");
                        $(".module-title").removeClass("gmCriteriaName");
                    }
                });



                var event = {};
                event.currentTarget = this.$(".search-tab-criteria");
                this.toggleTabs(event);

                $("#btn-category-search").hide();

            },
            
            //Load common datepicker function
            loadDatePicker: function(event){
                var elemId = "#" + $(event.target).data("id");
                loadDatepicker(event,elemId,"","mm/dd/yy","","");
            },
            
            displaySelctdDt: function(event){
                var elemId = "#" + $(event.target).attr("id");
                var elemVal = $(elemId).val();
                if(elemVal == "")
                   $(event.target).siblings("span").text("mm/dd/yyyy");
                else
                    $(event.target).siblings("span").text(elemVal);               
            },

            // Fnc to search by Activity ID (eg. MERC and Training modules)
            lookupByActID: function () {

                var actid = this.$("#activity-actid").val();
                var actcat;
                var module;
                if (this.module == "merc") {
                    actcat = "105265";
                    module = "merc";
                }
                if (this.module == "training") {
                    actcat = "105268";
                    module = "training";
                }
                var input = {
                    "token": localStorage.getItem("token"),
                    "actid": actid,
                    "actcategory": actcat
                };
                if (actid != "") {
                    fnGetWebServerData("crmactivity/info", "gmCRMActivityVO", input, function (data) {
                        if (data != "") {
                            data = data.GmCRMListVO.gmCRMActivityVO[0];
                            if (data.actid == "") {
                                GM.Global.ValidationActid = "false";
                                showError("No such ControlID");
                                $("#activity-actid").val("");
                            } else {
                                window.location.href = "#crm/" + module + "/id/" + data.actid;
                            }
                        }
                    });
                }
            },

            // Create new activity
            createActivity: function () {
                var that = this;
                if ((!navigator.onLine) && ((this.module == "training") || (this.module == "merc"))) {
                    showError("You need Internet to create Activity");
                } else {
                    var template = fnGetTemplate(URL_Common_Template, "gmtSpinner");
                    $("#div-crm-module").html(template());
                    GM.Global.Activity.Mode = "create";
                    window.location.href = "#crm/" + this.module + "/id/0";
                }

            },

            // Display list of upcoming activities
            showUpcoming: function () {
                GM.Global.Activity = {};

                if (this.module != "lead" && this.module != "tradeshow") {
                    if (this.module == "physicianvisit")
                        GM.Global.Status = {
                            listtype: "Scheduled"
                        };
                    else
                        GM.Global.Status = {
                            listtype: "Upcoming"
                        };
                }
                window.location.href = "#crm/" + this.module + "/list";
            },

            // Display list of requested activities
            showRequested: function () {
                if (this.module != "lead" && this.module != "tradeshow")
                    GM.Global.Status = {
                        listtype: "Requested"
                    };
                window.location.href = "#crm/" + this.module + "/list";
            },

            //option to load data on Enter key
            clickDefaultButton: function (event) {
                var that = this;
                if (event.keyCode == 13) {
                    var txtInput = $(event.target).attr('data-id');

                    console.log(txtInput);
                    switch (txtInput) {
                        case this.module + "-fname":
                        case this.module + "-lname":
                            that.lookupByName(event);
                            break;
                        case this.module + "-title":
                            that.lookupByTitle(event);
                            break;
                        case this.module + "-keyword":
                            that.lookupByKeyword(event);
                            break;
                        case this.module + "-actid":
                            that.lookupByActID(event);
                            break;
                        case "surgeon-npid":
                            that.lookupByNPI(event);
                            break;
                    }
                }
            },

            // Lookup Activities by providing Surgeon Name
            lookupByName: function (event) {


                var firstnm = $(event.currentTarget).parent().find("." + this.module + "-fname").val();
                var lastnm = $(event.currentTarget).parent().find("." + this.module + "-lname").val();
                firstnm = (firstnm.indexOf("’")!=-1)?firstnm.replace("’","'"):firstnm;
                lastnm  = (lastnm.indexOf("’")!=-1)?lastnm.replace("’","'"):lastnm;
                var name = {
                    firstnm: firstnm,
                    lastnm: lastnm
                };
                if (firstnm != "" || lastnm != "") {
                    GM.Global.Activity = name;
                    firstnm = (firstnm !== "") ? firstnm : "undefined";
                    lastnm = (lastnm !== "") ? lastnm : "undefined";

                    window.location.href = "#crm/" + this.module + "/list";
                }


            },

            // Lookup list by providing title
            lookupByTitle: function (event) {
                var title = $(event.currentTarget).parent().find("." + this.module + "-title").val();
                console.log(title);
                if (title.trim() != "") {
                    var titleSearch = {
                        actnm: title
                    };
                    GM.Global.Activity = titleSearch;
                    if (this.module == "lead") //set up for tradeshow search
                        GM.Global.TradeShowList = "Y";
                    else
                        GM.Global.TradeShowList = undefined;
                    window.location.href = "#crm/" + this.module + "/list";
                }

            },

            // Lookup Surgeons by providing Keyword
            lookupByKeyword: function (event) {
                var keywords = $(event.currentTarget).parent().find(".surgeon-keyword").val();
                if (keywords != "") {
                    GM.Global.Activity = {
                        keywords: keywords
                    };
                    window.location.href = "#crm/" + this.module + "/list";
                }
            },

            // Lookup Surgeon Details by providing NPI
            lookupByNPI: function (event) {
                var npi = $(event.currentTarget).parent().find(".surgeon-npid").val();
                if (npi != "") {
                    if ($.isNumeric(npi)) { //check if npi is a number only
                        getSurgeonPartyInfo(npi, function (surgeonData) {
                            if (surgeonData != undefined) {
                                if (surgeonData.partyid != null)
                                    window.location.href = "#crm/surgeon/id/" + surgeonData.partyid;
                            } else
                                showError("No such surgeon");
                        });
                    } else
                        showError("NPID should be Numeric!")
                }
            },

            // Open Single / Multi Select box        
            openPopup: function (event) {
                var that = this;
                var fnName = "";
                that.systemSelectOptions = {
                    'title': $(event.currentTarget).attr("data-title"),
                    'storagekey': $(event.currentTarget).attr("data-storagekey"),
                    'webservice': $(event.currentTarget).attr("data-webservice"),
                    'resultVO': $(event.currentTarget).attr("data-resultVO"),
                    'IDParameter': $(event.currentTarget).attr("data-IDParameter"),
                    'NameParameter': $(event.currentTarget).attr("data-NameParameter"),
                    'inputParameters': $(event.currentTarget).attr("data-inputParameters"),
                    'codegrp': $(event.currentTarget).attr("data-codegrp"),
                    'sortby': $(event.currentTarget).attr("data-sortby"),
                    'module': this.module,
                    'event': event,
                    'parent': this,
                    'moduleLS': GM.Global.Activity.FilterID,
                    'moduleCLS': GM.Global.Activity.FilterName,
                    'callback': function (ID, Name) {
                        GM.Global.Activity.FilterID = ID;
                        GM.Global.Activity.FilterName = Name;
                    }
                };

                that.popupSelectView = new GMVCRMSelectPopup(that.systemSelectOptions);

            },

            // Lookup Activities by Activity Date
            activityDate: function () {
                var that = this;
                var fromDate = $("#activity-frmdate").val();
                var toDate = $("#activity-todate").val();
                if (fromDate != "" && toDate != "") {

                    if (GM.Global.Activity.FilterID == undefined)
                        GM.Global.Activity.FilterID = {};
                    if (GM.Global.Activity.FilterName == undefined)
                        GM.Global.Activity.FilterName = {};

                    if (this.module == "crf") {
                        GM.Global.Activity.FilterID["crffrmdate"] = fromDate;
                        GM.Global.Activity.FilterID["crftodate"] = toDate;
                        GM.Global.Activity.FilterName["From Date"] = fromDate;
                        GM.Global.Activity.FilterName["To Date"] = toDate;
                    } else {
                        GM.Global.Activity.FilterID["actstartdate"] = fromDate;
                        GM.Global.Activity.FilterID["actenddate"] = toDate;
                        GM.Global.Activity.FilterName["From Date"] = fromDate;
                        GM.Global.Activity.FilterName["To Date"] = toDate;
                    }
                }
            },

            // Lookup Activities by Filters
            searchCategory: function (event) {
                if (typeof (GM.Global.Activity.FilterID) != "object")
                    GM.Global.Activity.FilterID = $.parseJSON(GM.Global.Activity.FilterID);
                if (typeof (GM.Global.Activity.FilterID) != "object")
                    GM.Global.Activity.FilterName = $.parseJSON(GM.Global.Activity.FilterName);

                if ($("#" + this.module + "-frmdate") != "")
                    this.activityDate();
                if ($("#" + this.module + "-frmdate") != "" || !$.isEmptyObject(GM.Global.Activity.FilterID)) {
                    GM.Global.Activity["FilterID"] = GM.Global.Activity.FilterID;
                    GM.Global.Activity["FilterName"] = GM.Global.Activity.FilterName;
                    window.location.href = "#crm/" + this.module + "/list";
                }

            },

            // Delete the favorite item from the criteria page
            removeFavorite: function (e) {
                var that = this;
                showNativeConfirm("Do you want to delete this favorite", "Confirm", ["Yes", "No"], function (index) {
                    if (index == 1) {
                        var input = {};
                        input.token = localStorage.getItem('token');
                        input.criteriaid = $(e.currentTarget).attr('criteriaid');
                        input.criterianm = $(e.currentTarget).parent().find('a').text();
                    input.criteriatyp = that.module;
                        input.voidfl = "Y";
                        input.listGmCriteriaAttrVO = [];
                        console.log(input);

                        fnGetWebServerData("criteria/save", undefined, input, function (data) {
                            if (data != null)
                                $(e.currentTarget).parent().parent().remove();
                            else
                                showError("Favorite delete has failed!");

                        });
                    }
                });
            },
            //for phone all modules toogle
            toggleTabs: function (event) {
                colorControl(event, "sub-tab", "gmBGEmpty", "gmBGDarkerGrey gmFontWhite");
                var target = $(event.currentTarget).attr("tabname");
                if (target == "search-tab-criteria") {
                    $(".search-criteria-content").removeClass("gmMediaDisplayNone");
                    $(".favorite-criteria-content").addClass("gmMediaDisplayNone");
                    $(".filter-criteria-content").addClass("gmMediaDisplayNone");
                } else if (target == "filter-tab-criteria") {
                    $(".filter-criteria-content").removeClass("gmMediaDisplayNone");
                    $(".search-criteria-content").addClass("gmMediaDisplayNone");
                    $(".favorite-criteria-content").addClass("gmMediaDisplayNone");
                } else if (target == "favorite-tab-criteria") {
                    $(".favorite-criteria-content").removeClass("gmMediaDisplayNone");
                    $(".search-criteria-content").addClass("gmMediaDisplayNone");
                    $(".filter-criteria-content").addClass("gmMediaDisplayNone");
                }
            },

            setMinDate: function (e) {
                var fromdt = $("#activity-frmdate").val();
                var todt = $("#activity-todate").val();
                var fromdtFmt = new Date(fromdt);
                var todtFmt = new Date(todt);
                if ($("#activity-todate").val() != "" && $("#activity-todate").val() != undefined && $("#activity-frmdate").val() != undefined && fromdtFmt > todtFmt)
                    $("#activity-todate").val($("#activity-frmdate").val()).trigger("change");
                var fromDate = $("#activity-frmdate").val();
                var toDate = $("#activity-todate").val();

                if ($(e.currentTarget).attr("id") == "activity-frmdate")
                    $("#activity-todate").attr("min", fromDate);
                if (GM.Global.Activity.FilterID == undefined)
                    GM.Global.Activity.FilterID = {};
                if (GM.Global.Activity.FilterName == undefined || GM.Global.Activity.FilterName == undefined)
                    GM.Global.Activity.FilterName = {};
                else if (typeof (GM.Global.Activity.FilterID) != "object")
                    GM.Global.Activity.FilterID = $.parseJSON(GM.Global.Activity.FilterID);
                if (typeof (GM.Global.Activity.FilterName) != "object")
                    GM.Global.Activity.FilterName = $.parseJSON(GM.Global.Activity.FilterName);
                if (toDate != "" && toDate != "aN/aN/NaN" && fromDate != "" && fromDate != "aN/aN/NaN") {
                    if (this.module == "crf") {
                        GM.Global.Activity.FilterName["From Date"] = fromDate;
                        GM.Global.Activity.FilterID["crffrmdate"] = fromDate;
                        GM.Global.Activity.FilterID["crftodate"] = toDate;
                        GM.Global.Activity.FilterName["To Date"] = toDate;
                    } else {
                        GM.Global.Activity.FilterName["From Date"] = fromDate;
                        GM.Global.Activity.FilterID["actstartdate"] = fromDate;
                        GM.Global.Activity.FilterID["actenddate"] = toDate;
                        GM.Global.Activity.FilterName["To Date"] = toDate;
                    }
                } else {
                    if (this.module == "crf") {
                        GM.Global.Activity.FilterName["From Date"] = "";
                        GM.Global.Activity.FilterID["crffrmdate"] = "";
                        GM.Global.Activity.FilterID["crftodate"] = "";
                        GM.Global.Activity.FilterName["To Date"] = "";
                    } else {
                        GM.Global.Activity.FilterName["From Date"] = "";
                        GM.Global.Activity.FilterID["actstartdate"] = "";
                        GM.Global.Activity.FilterID["actenddate"] = "";
                        GM.Global.Activity.FilterName["To Date"] = "";
                    }
                }
                var tempArray = new Array();
                var result = $.parseJSON(JSON.stringify(GM.Global.Activity.FilterName));
                $.each(result, function (key, val) {
                    if (val != "" && val != "aN/aN/NaN")
                        tempArray.push({
                            "title": key,
                            "value": val
                        });
                });
                var finalJSON = $.parseJSON(JSON.stringify(tempArray));
                if (finalJSON != "") {
                    this.$(".list-category-item").html(this.template_selected_items(finalJSON));
                    this.$("#btn-category-search").show();
                } else {
                    this.$(".list-category-item").empty();
                    this.$("#btn-category-search").hide();
                }

                $(".div-criteria").addClass("gmFont" + this.module);
            },
            toggleSelectedItems: function (event) {
                if ($("#criteria-selected-itmes").hasClass("gmMediaDisplayNone")) {
                    $("#criteria-selected-itmes").removeClass("gmMediaDisplayNone");
                    $("#selected-criteria-icon").find(".fa-chevron-down").addClass("fa-chevron-up").removeClass("fa-chevron-down");
                } else {
                    $("#criteria-selected-itmes").addClass("gmMediaDisplayNone");
                    $("#selected-criteria-icon").find(".fa-chevron-up").addClass("fa-chevron-down").removeClass("fa-chevron-up");
                }
            },

            // Display DHTMLX Scheduler view
            showVisitSchedule: function () {
                var that = this;
                this.eventArray = [];
                this.partyFilters = [];
                this.partyID =  localStorage.getItem("partyID");
                this.createdById =  localStorage.getItem("userID");
                this.PVScheduler = Scheduler.getSchedulerInstance();
                $(".scheduler-main-container").removeClass("hide");
                $(".gmBtnGroup #li-close").removeClass("hide");
                that.PVScheduler.clearAll();
                that.PVScheduler.config.hour_date = "%h:%i:%A";
                that.PVScheduler.config.first_hour = 8;
                that.PVScheduler.config.last_hour = 19;
                this.$(".gmPanelTitle").html("Visit Schedule");

                that.PVScheduler.templates.event_bar_text = function (start, end, event) {
                    that.PVScheduler.templates.event_bar_date = function (start, end, event) {
                        return "";
                    };
                    return "• <b>" + that.PVScheduler.templates.event_date(start) + "</b> <span class='dhx_cal_event-txt" + event.accessfl + "'>" + event.text + "</span> ";
                };
                
                that.PVScheduler.attachEvent("onViewChange", function (new_mode , new_date){
                      setCookie('CRMSchedulerMode', new_mode,null)
                    
                });

                var schedulerMode= getCookie('CRMSchedulerMode');
                if (parseNull(schedulerMode) != "") {  
                    that.PVScheduler.init('scheduler_here', new Date(),schedulerMode);

                } else { 
                if(this.module == "salescall")
                    that.PVScheduler.init('scheduler_here', new Date(), "week");
                else
                    that.PVScheduler.init('scheduler_here', new Date(), "month");
                 }
                
                that.PVScheduler.locale.labels.year_tab = "Year";

                that.PVScheduler.config.readonly = true;
               
                // default values for filters
                // Divisions: 2000	Spine, 2004	I-N-Robotics, 2005	Trauma
                if(this.module == "physicianvisit") {
                    console.log(this.module);
                    if (_.contains(GM.Global.Admin, this.module)) {
                        $("#filter-typ-others").addClass("hide");
                        var filters = {
                            type105225: true,
                            type105226: true,
                            type105227: true,
                            type26240330: true,
                            type26240331: true,
                            type26240596: true,
                            typeOthers: false,
                            statusAll: true,
                            status105760: true,
                            status105761: true,
                            status105762: false,
                            division2000: true,
                            division2004: true,
                            division2005: true,
                            division2007: true,
                            division : true
                        };
                    } else {
                        $("#filter-typ-others").removeClass("hide");
                        $("#dhtmlx-filter-division").addClass("hide");
                        var filters = {
                            type105225: true,
                            type105226: false,
                            type105227: false,
                            type26240330: true,
                            type26240331: true,
                            type26240596: true,
                            typeOthers: true,
                            statusAll: true,
                            status105760: true,
                            status105761: true,
                            status105762: false,
                            division2000: false,
                            division2004: false,
                            division2005: false,
                            division2007: false,
                            division : true
                        };
                        filters["division" + localStorage.getItem("repDivision")] = true;
                    }

                    var filter_inputs = $(".filters_wrapper").find("input");
                    console.log("filter_input");
                    console.log(filter_input);
                    
                    for (var i = 0; i < filter_inputs.length; i++) {
                        var filter_input = filter_inputs[i];

                        // set initial input value based on filters settings
                        filter_input.checked = filters[filter_input.name];

                        // attach event handler to update filters object and refresh view (so filters will be applied)
                        filter_input.onchange = function () {
                            filters[this.name] = !!this.checked;
                            that.PVScheduler.updateView();
                        }
                    }
                }
                    // here we are using single function for all filters but we can have different logic for each view
                    that.PVScheduler.filter_month = that.PVScheduler.filter_day = that.PVScheduler.filter_week = function (id, event) {
                        // display event only if its type is set to true in filters obj
                        // or it was not defined yet - for newly created event
                        console.log(that.module);
                        if(that.module == "physicianvisit") {
                            var statusCnt = $("#dhtmlx-filter-status input:checked").length;
                            var divisionCnt = $("#dhtmlx-filter-division input:checked").length;
                            var typeCnt = $(".dhtml-filter-type input:checked").length;

                            // Three filters are selected
                            if (statusCnt > 0 && typeCnt > 0 && divisionCnt > 0)
                                if (filters[event.type] && filters[event.status] && filters[event.division] || event.type == that.PVScheduler.undefined)
                                    return true;
                            // Single filter is selected
                            if (statusCnt > 0 && typeCnt == 0 && divisionCnt == 0)
                                if (filters[event.status])
                                    return true;

                            if (statusCnt == 0 && typeCnt > 0 && divisionCnt == 0)
                                if (filters[event.type])
                                    return true;

                            if (statusCnt == 0 && typeCnt == 0 && divisionCnt > 0)
                                if (filters[event.division])
                                    return true;

                            // Two filter is selected
                            if (statusCnt > 0 && typeCnt > 0 && divisionCnt == 0)
                                if (filters[event.status] && filters[event.type])
                                    return true;

                            if (statusCnt == 0 && typeCnt > 0 && divisionCnt > 0)
                                if (filters[event.type] && filters[event.division])
                                    return true;

                            if (statusCnt > 0 && typeCnt == 0 && divisionCnt > 0)
                                if (filters[event.status] && filters[event.division])
                                    return true;

                            // default, do not display event
                            return false; 
                        }
                        else {
                            return true; 
                        }
                    };
                
                
                if(GM.Global.SchdulerEvents != undefined) {
                    that.PVScheduler.detachEvent(GM.Global.SchdulerEvents); 
                }
                GM.Global.SchdulerEvents = that.PVScheduler.attachEvent("onClick", function (id, e) {
                    var evs = that.PVScheduler.getEvent(id);

                    if (evs.accessfl == "Y" || that.module == "salescall") {
                        hideMessages();
                        var input = { "token": localStorage.getItem("token"), "actid": id };
                        fnGetWebServerData("crmactivity/info", "gmCRMActivityVO", input, function (data) {
                            console.log(data);
                            data = data.GmCRMListVO.gmCRMActivityVO;
                            if (data != undefined && data.length > 0) {
                                var dataObj = data[0];
                                _.each(dataObj.arrgmactivitypartyvo, function (data) {
                                    //Code_ID-103112,	Code_Nm-Images	Code_GRP-FTYPE
                                    data["imgurl"] = URL_WSServer+"file/src?refgroup=103112&refid=" + data.partyid + "&reftype=null&num=" + Math.random();
                                });
                                
                                var headInfobgcolor = "";
                                if(that.module == "physicianvisit")
                                    headInfobgcolor = (dataObj.acttype == "26240330") ? "bgSkyBlue" : (dataObj.acttype == "105225") ? "bgOrange" : (dataObj.acttype == "105227") ? "bgGreen" : (dataObj.acttype == "105226") ? "bgGray" : (dataObj.acttype == "26240596") ? "bgLRed" : "bgBlue";
                                if(that.module == "salescall") {
                                    $("#scheduler-info-container").addClass('salescallpopup');
                                    console.log(dataObj);
                                    headInfobgcolor = (dataObj.actstatus == "105762") ? "bgLRed" : (dataObj.actstatus == "105761") ? "bgLSkyBlue" : "bgLOrange"; 
                                    if(dataObj.createdbyid == that.createdById){
                                        console.log(headInfobgcolor);
                                        headInfobgcolor = "bgLOrange";
                                    }
                                }
                                
                                that.showCalendarInfo(dataObj);
                                
                                $("#scheduler-info-main-header").addClass(headInfobgcolor);
                            } else {
                                showError("No access to view this Event");
                                that.closeCalendarInfo();
                                return true;
                            }
                        });
                    } else {
                        showError("No access to view this Event");
                        that.closeCalendarInfo();
                        return true;
                    }
                    return true;
                    
                });
                
                
                
                var input = {};
                if(this.module == "salescall") {
                    console.log(this.partyFilters);
                    if(this.partyFilters == "")
                        this.partyFilters = "";
                    if(isRep())
                        this.partyFilters = this.partyID;
                    input = {
                        "acttypeid": "",
                        "token": localStorage.getItem("token"),
                        "actcategory": "105267",
                        "stropt": "visitschedule",
                        "partyid" : this.partyFilters
                    };  
                }
                else {    
                //105225-VIP Only, 105226-Design, 105227-Research, 26240331-Lab Only, 26240330-VIP Visit + Lab, 105266-Physician Visit
                    input = {
                        "acttypeid": "105226,105227,105225,26240330,26240331,26240596",
                        "token": localStorage.getItem("token"),
                        "actcategory": "105266",
                        "stropt": "visitschedule"
                    }; 
                }
                //105760-Requested, 105761-Scheduled, 105762-Cancelled, 105763-In Progress, 105764-Completed
                console.log(JSON.stringify(input));
                fnGetWebServerData("crmactivity/scheduler", "gmCRMActivityVO", input, function (data) {
                    console.log(data);
                    
                    that.PVScheduler.clearAll();
                    
                    if (data != undefined) {
                        data = data.GmCRMListVO.gmCRMActivityVO;
                        _.each(data, function (val, index) {

                            if (_.contains(GM.Global.Admin, "physicianvisit"))
                                val.accessfl = "Y";
                            if(val.acttypeid=='26240596')
                                val.accessfl = "Y";
                            
                            if (that.module == "physicianvisit") {
                                if (val.actstatusid == "105760")
                                    that.bgColor = (val.accessfl == "Y") ? (val.acttypeid == "26240330") ? 'repeating-linear-gradient(135deg, #fff 9px, #fff 11px, #ffeebf 10px, #ffeebf 15px)' : (val.acttypeid == "105225") ? 'repeating-linear-gradient(135deg, #fff 9px, #fff 11px, #ccefff 10px, #ccefff 15px)' : (val.acttypeid == "105227") ? 'repeating-linear-gradient(135deg, #fff 9px, #fff 11px, #fddae1 10px, #fddae1 15px)' : (val.acttypeid == "105226") ? 'repeating-linear-gradient(135deg, #fff 9px, #fff 11px, #fbe4d1 10px, #fbe4d1 15px)' : (val.acttypeid == "26240596") ? 'repeating-linear-gradient(135deg, #fff 9px, #fff 11px, #ff8d83 10px, #ff8d83 15px)' : 'repeating-linear-gradient(135deg, #fff 9px, #fff 11px, #eaeaa6 10px, #eaeaa6 15px)' : "#dfdce0";
                                else if (val.actstatusid == "105762" && that.module == "physicianvisit") 
                                    that.bgColor = (val.accessfl == "Y") ? "#ff8d83" : "#dfdce0"; 
                                else
                                    that.bgColor = (val.accessfl == "Y") ? (val.acttypeid == "26240330") ? "#fdcc45" : (val.acttypeid == "105225") ? "#40C4FF" : (val.acttypeid == "105227") ? "#FB607F" : (val.acttypeid == "105226") ? "#f57403" : (val.acttypeid == "26240596") ? "#ff8d83" : "#E3E804" : "#dfdce0";
                                
                            }
                            else if (that.module == "salescall") {
                                
                                if(val.createdby == that.createdById) 
                                    val.accessfl = "Y";
                                
                                if (val.actstatusid == "105762") 
                                    that.bgColor = (val.accessfl == "Y") ? "#ff8d83" : "#ff8d83"; 
                                else if (val.createdby == that.createdById)
                                    that.bgColor = (val.accessfl == "Y") ? "#f6b453" : "#9fe8ff";
                                else if (val.actstatusid == "105761")
                                    that.bgColor = (val.accessfl == "Y") ? "#9fe8ff" : "#9fe8ff";
                            }
                            
                            that.eventArray.push({
                                "id": val.actid,
                                "text": (val.accessfl == "Y") ? val.actnm : "",
                                "start_date": getDateByFormat(val.actstartdate) + " " + val.actstarttime,
                                "end_date": getDateByFormat(val.actenddate) + " " + val.actendtime,
                                "type": (val.accessfl == "Y") ? "type" + val.acttypeid : "typeOthers",
                                "status": (val.accessfl == "Y") ? "status" + val.actstatusid : "statusAll",
                                "color": that.bgColor,
                                "textColor": "#000",
                                "accessfl": val.accessfl,
                                "division": "division" + val.division
                            }); 
                            
                        });
                        console.log(that.eventArray);
                        that.PVScheduler.parse(that.eventArray, "json"); //takes the name and format of the data source
                    }
                });

            },

            // show calendar popinfo
            showCalendarInfo: function (data) {
                var that = this;
                var userID = localStorage.getItem("userID");
                that.closeCalendarInfo();
                data["module"] = this.module;
                
                this.$("#scheduler-info-container").html(that.template_SchedulerInfo(data));
                this.$("#scheduler-info-container").removeClass("hide");
                if (_.contains(GM.Global.Admin, this.module) && data.createdbyid == userID) {
                    $(".gmBtnGroup #scheduler-button-edit").removeClass("gmVisibilityHidden");
                }
                if (_.contains(GM.Global.Admin, "physvisitrqst") && data.createdbyid == userID && data.actstatus == "105760") {
                    $(".gmBtnGroup #scheduler-button-edit").removeClass("gmVisibilityHidden");
                }
                if (_.contains(GM.Global.Admin, "physicianvisit")) { // PV Admin 
                    $(".gmBtnGroup #scheduler-button-edit").removeClass("gmVisibilityHidden");
                }

                if (data.actstatus == "105762" && data.createdbyid != userID)
                    $(".gmBtnGroup #scheduler-button-edit").addClass("gmVisibilityHidden");


                $("#scheduler-button-view").unbind("click").bind("click", function () {
                    if(that.module == "physicianvisit")
                        that.viewCalendarInfo(data);
                    else if (that.module == "salescall")
                        window.location.href = "#crm/salescall/id/"+data.actid;
                });
                $("#scheduler-button-edit").unbind("click").bind("click", function () {
                    GM.Global.SchedulerFL = "Y";
                });
                $("#scheduler-button-close").unbind("click").bind("click", function () {
                    that.closeCalendarInfo();
                });
            },

            viewCalendarInfo: function (data) {
                var that = this;

                this.$("#scheduler-info-view").removeClass("hide");

                if (data.acttype == "105226")
                    data["projFl"] = "Y";
                else
                    data["prodFl"] = "Y";

                this.$("#scheduler-info-view").html(this.template_SchedulerEventInfo(data));

                _.each(data.arrgmactivitypartyvo, function (data) {

                    if ($("#cv-links-" + data.partyid).length > 0) {
                        $("#cv-links-" + data.partyid + " span").attr("href", URL_CRMFileServer + data.surgeoncv)
                    }
                });

                if (isRep()) {
                    $(".visit-edit-division").addClass("hide");
                }
                if (data.actstatus == "105762")
                    $("#div-visit-cancel-reson").removeClass("hide");

                var input = {
                    "token": localStorage.getItem("token"),
                    "refid": data.actid,
                    "refgroup": "107994"
                };
                fnGetWebServerData("file/get", "gmCRMFileUploadVO", input, function (file) {
                    if (file != null) {
                        file = arrayOf(file);
                        $(".upload-files-list").html("");
                        _.each(file, function (v) {
                            $(".upload-files-list").append("<div class = 'gmLnk gmUnderline gmPaddingButtom10p'><span class='a-view-docs' href = '" + URL_CRMFileServer + v.refgroup + "/" + v.refid + "/" + v.filename + "'>" + v.filetitle + " - " + v.updatedby + " on " + v.updateddate + "</span></div>");
                        });
                    }
                });
                $("#event-info-btn-close").unbind("click").bind("click", function () {
                    that.closeEventInfo();
                });
            },

            closeEventInfo: function () {
                var that = this;
                this.$("#scheduler-info-view").addClass("hide");
            },

            // close calendar popinfo
            closeCalendarInfo: function () {
                var that = this;
                this.$("#scheduler-info-container").addClass("hide");
                this.closeEventInfo();
            },

            // close calendar view in criteria page
            closeVisitSchedule: function () {
                var that = this;
                $(".scheduler-main-container").addClass("hide");
                $(".gmBtnGroup #li-close").addClass("hide");
                this.closeCalendarInfo();
                this.closeEventInfo();
                this.$(".gmPanelTitle").html("Lookup Physician Visit");
            },

            viewDocument: function (event) {
                var path = $(event.currentTarget).attr('href');
                if (path != undefined)
                    window.open(path, '_blank', 'location=no');
            },

            //Handles the orietaion change in the iPad
            onOrientationChange: function (event) {
                resizeHeadWidth('.gmPanelHead', '.gmBtnGroup', '#panelhead-left');
            },
            
             // sales rep search 
            searchRep: function () {
                var that = this;
                var searchOptions = {
                    'el': $("#salescall-scheduler-rep-search"),
                    'url': URL_WSServer + 'search/rep',
                    'inputParameters': {
                        'token': localStorage.getItem('token')
                    },
                    'keyParameter': 'repname',
                    'IDParameter': 'repid',
                    'NameParameter': 'repname',
                    'resultVO': 'gmSalesRepQuickVO',
                    'fnName': "",
                    'minChar': 3,
                    'usage' : 2,
                    'callback': function (data) {
                        console.log(data);
                        var partyIDs = [];
                        _.each(arrayOf(data), function(dt) {
                                partyIDs.push(dt.partyid);
                        });

                        that.partyFilters = partyIDs.toString();

                        that.showVisitSchedule(); // callback same funcion 
                    }
                };
                this.searchView5 = new GMVSearch(searchOptions);
            },
            
            showLegend: function () {
                $("#scheduler-legend-info").toggle();
            }
        });
        return GM.View.ActivityCriteria;
    });
