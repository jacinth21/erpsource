/********************************************************************************************************************
 * File:                gmvActivityDetail.js
 * Description:         View which displays the details of the respective activities
 * Version:             1.0
 * Author:              jgurunathan	 
 ********************************************************************************************************************/
define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars', 'jqueryui',

    // Common
   'global', 'commonutil', 'gmvSearch', 'gmvValidation', 'gmvCRMAddress', 'gmvCRMFileUpload', 'gmvMultiFileUpload','dropdownview',

    // View
    'gmvCRMData', 'gmvCRMSelectPopup',

    //Model
    'gmmActivity',

    //local db
    'crmdb',

    // Pre-Compiled Template
    'gmtTemplate', 'gmcItem', 'gmvItemList',

],

    function ($, _, Backbone, Handlebars, JqueryUI,
        Global, CommonUtil, GMVSearch, GMVValidation, GMVCRMAddress, GMVCRMFileUpload, GMVMultiFileUpload,DropDownView,
        GMVCRMData, GMVCRMSelectPopup,
        GMMActivity, CRMDB, GMTTemplate, GMCItem, GMVItemList) {

        'use strict';

        GM.View.ActivityDetail = Backbone.View.extend({

            el: "#div-crm-module",

            name: "CRM Activity Detail View",

            /* Load the templates */
            template_MainModule: fnGetTemplate(URL_Common_Template, "gmtCRMMainModule"),
            template_SalesCallDetail: fnGetTemplate(URL_SalesCall_Template, "gmtSalesCallDetail"),
            template_PhysicianVisitDetail: fnGetTemplate(URL_PhysicianVisit_Template, "gmtPhysicianVisitDetail"),
            template_VisitCancelReq: fnGetTemplate(URL_PhysicianVisit_Template, "gmtCancelRequest"),
            template_PhysicianVisitGlobusExperience: fnGetTemplate(URL_PhysicianVisit_Template, "gmtGlobusExperience"),
            template_SelectAddress: fnGetTemplate(URL_SalesCall_Template, "gmtSalesCallSelectAddress"),
            template_FilterButtons: fnGetTemplate(URL_Common_Template, "gmtFilterButtons"),
            template_CreateAddress: fnGetTemplate(URL_Common_Template, "gmtCreateAddress"),
            template_LeadDetail: fnGetTemplate(URL_Lead_Template, "gmtLeadDetail"),
            template_MERCDetail: fnGetTemplate(URL_MERC_Template, "gmtMERCDetail"),
            template_TrainingDetail: fnGetTemplate(URL_Training_Template, "gmtTrainingDetail"),
            template_TradeshowDetail: fnGetTemplate(URL_Lead_Template, "gmtLeadTradeshow"),
            template_CreateSurgeon: fnGetTemplate(URL_Common_Template, "gmtCreateSurgeon"),
            template_SurgeonMERC: fnGetTemplate(URL_Surgeon_Template, "gmtMERCListPopup"),

            events: {
                "change input, textarea": "updateModel",
                "change select": "chkSelect",
                "click #btn-main-edit": "editActivity",
                "click #btn-main-void": "confirmVoid",
                "click #btn-main-save": "saveActivity",
                "click #li-cancel": "backToPage",
                // "click #li-activity-select-address": "getSurgeonAddress",
                "click #li-salescall-edit-address": "editAddres",
                "click .phyvisit-tab": "togglePVTabs",
                "click .merc-tab": "toggleMERCTabs",
                "click .training-tab": "toggleTrainingTabs",
                "click #training-select-address, #merc-select-address, #tradeshow-select-address, #lead-select-address": "addNewAddress",
                "click #create-surgeon": "createSurgeon",
                "click .OfflineLink": "goOfflineMsg",
                "click #tab-contact, #btn-category-done": "toogleContact",
                "click #tab-interest": "toogleInterest",
                "click #lead-contacts-input-address": "toogleAddress",
                "click #tab-techniques ": "toogleTechniques",
                "click #tab-contact-detail ": "toogleDetailContact",
                "click #lead-edit-set-default": "setDefaultTradeshow",
                "click .span-remove-extend": "cancelAttribute",
                "click .gmBtnGroup #li-create": "createActivity",
                "click #li-phyvsit-add-more-document .fa-plus-circle": "addMoreDoc",
                "click #li-phyvsit-add-more-document .fa-minus-circle": "deleteDoc",
                "click .li-surgeon-crf-doc a": "saveBeforeCRF",
                "click #fa-surgeon-detail-delete": "deleteSurgeon",
                "click .div-edit .li-mobile-edit-toogle": "toogleRoleStatus",
                "click .div-lead-surg-hospital-new": "addNewHosp",
                "click .remove-participants": "removeParticipants",
                "click .comments-cancel": "cancelComment",
                "click .comments-save": "saveComment",
                "click .comments-edit": "editComments",
                "click .comments-delete": "deleteComments",
//                "change #select-salescall-result": "changeResult",
                "click #div-lead-file-upload-btn": "leadUpload",
                "click #button-print": "printSummary",
                "click #div-physicianvisit-confirm": "confirmVisit",
                "click #div-physicianvisit-cancel, #div-salescall-cancel": "cancelVisit",
                "click .li-phone-globus-exp-btn": "toogleGlobusExperience",
                "click .a-view-docs": "viewDocument",
                "blur #activity-controlid-merc, #activity-controlid-training": "checkCtrlNo",
//                "click #create-merc-activity ": "listUpcomingMERC",
                "blur #input-lead-contacts-npid": "checkNpid",
//                "click #button-sc-result-create": "createResult",
                "click .merc-party-checkbox, #lbl-accept,#lbl-reject, #lbl-acceptAll,#lbl-rejectAll": "mercParty",
                "click .div-multi-fileupld-btn": "multipleFileUpld",
                "click #btn-add-other-visitor": "addOtherVisitor",
                "change #input-physicianvisit-other-visitor-title, #input-physicianvisit-other-visitor": "setVisitorNm",
                "change #acttype": "updateAttrVal",
                "click #span-edit-title": "changeTitle",
                "click #span-reset-title": "resetTitle",
                "click #span-set-title": "setTitle",
                "click #span-cancel-title": "cancelTitle",
                "click .btn-add-projsys": "addProjSystem",
                "click #checkbox-sc-calander-sync": "toggleSyncCal",
                "click #div-physicianvisit-travel": "activityDetail"

            },

            initialize: function (options) {
                var that = this;
                this.module = options.module;
                this.actid = options.actid;
                this.data = options.data;
                this.restrictFl = '';
                this.validationText = undefined;
                this.curPosTgt = 0;
                $(window).on('orientationchange', this.onOrientationChange);
                if (GM.Global.Activity == undefined)
                    GM.Global.Activity = {};
                if (this.actid && this.actid != 0) {
                    this.actModel = new GMMActivity(this.data);
                    this.actModel.set("arrgmactivitypartyvo", arrayOf(this.data.arrgmactivitypartyvo));
                    this.actModel.set("arrgmactivityattrvo", arrayOf(this.data.arrgmactivityattrvo));
                    this.actModel.set("arrgmcrmlogvo", arrayOf(this.data.arrgmcrmlogvo));
                    this.actModel.set("arrgmcrmaddressvo", arrayOf(this.data.arrgmcrmaddressvo));

                    if (this.actModel.get('arrgmcrmaddressvo')[0] == null || this.actModel.get('arrgmcrmaddressvo')[0] == undefined)
                        this.actModel.set('arrgmcrmaddressvo', []);
                    if (this.actModel.get('arrgmcrmlogvo')[0] == null || this.actModel.get('arrgmcrmlogvo')[0] == undefined)
                        this.actModel.set('arrgmcrmlogvo', []);
                    if (this.actModel.get('arrgmactivityattrvo')[0] == null || this.actModel.get('arrgmactivityattrvo')[0] == undefined)
                        this.actModel.set('arrgmactivityattrvo', []);
                    if (this.actModel.get('arrgmactivitypartyvo')[0] == null || this.actModel.get('arrgmactivitypartyvo')[0] == undefined)
                        this.actModel.set('arrgmactivitypartyvo', []);
                    this.actModel.set($.parseJSON(JSON.stringify(this.actModel)));
                    this.attIndex = 0;
                    this.addVisitorFl = true;
                    var that = this;
                    _.each(this.actModel.get("arrgmactivityattrvo"), function (data) {
                        if ($.isNumeric(data.attrtype))
                            data.attrtype = data.attrtype.toString();
                        if (data.attrtype == "105590")
                            data.index = that.attIndex++;
                    });
                    _.each(this.actModel.get("arrgmactivitypartyvo"), function (data) {
                        if ($.isNumeric(data.roleid))
                            data.roleid = data.roleid.toString();
                        if ($.isNumeric(data.partytype))
                            data.partytype = data.partytype.toString();
                    });

                    if ($.isNumeric(this.actModel.get("actstatus")))
                        this.actModel.set("actstatus", this.actModel.get("actstatus").toString());

                    GM.Global.Activity.Mode = "view";
                } else if (this.actid == 0 || this.actid == undefined) {
                    this.actModel = new GMMActivity();
                    if (this.module != "tradeshow") {
                        GM.Global.Activity.Mode = "create";
                    }
                }

                this.void = "";
                this.userTabs = _.pluck($.parseJSON(localStorage.getItem('userTabs')), "functionID");

                GM.Global.Activity.ActStep = "";
                GM.Global.file = null;
                GM.Global.FormData = null;
                GM.Global.PopupID = null; // Popup single select values
                GM.Global.PopupName = null;

                this.nameFile = true;
                this.interest = false;
                switch (this.module) {
                    case "salescall":
                        this.template = this.template_SalesCallDetail;
                        this.tabAccess = fnGetAccessInfo("SALESCALL");
                        this.title = "Sales Call";
                        this.actcategory = "105267";
                        break;
                    case "lead":
                        this.template = this.template_LeadDetail;
                        this.tabAccess = fnGetAccessInfo("LEAD");
                        this.title = "Lead";
                        this.actcategory = "105269";
                        break;
                    case "merc":
                        this.template = this.template_MERCDetail;
                        this.tabAccess = fnGetAccessInfo("MERC");
                        this.title = "MERC";
                        this.actcategory = "105265";
                        break;
                    case "physicianvisit":
                        this.template = this.template_PhysicianVisitDetail;
                        this.tabAccess = fnGetAccessInfo("PHYSICIANVISIT");
                        this.title = "Physician Visit";
                        this.actcategory = "105266";
                        break;
                    case "training":
                        this.template = this.template_TrainingDetail;
                        this.tabAccess = fnGetAccessInfo("TRAINING");
                        this.title = "Training";
                        this.actcategory = "105268";
                        break;
                    case "tradeshow":
                        this.template = this.template_TradeshowDetail;
                        this.tabAccess = fnGetAccessInfo("LEAD");
                        this.title = "Tradeshow";
                        this.actcategory = "105270";
                        break;
                }

                this.actstatus = this.actModel.get("actstatusnm");

                this.shuffleTrade = []; // Holds TradeShow Name in case of shuffling its' select when the attrid!=null
                this.tradeID = "";
                this.tradeName = "";
                this.newSurgeon = "";
                this.pvFiles = [];
                this.actModel.on('change', this.render, this);

                if (this.module == "physicianvisit" && this.actstatus == "Scheduled") {
                    GM.Global.PVDATA = {};
                    GM.Global.PVDATA = this.actModel;
                }


            },

            render: function () {

                var that = this;
                var userID = localStorage.getItem("userID");
                $("#span-app-title").html(this.title);

                if (this.module == "tradeshow") {
                    this.module = "lead";
                    var mod = true;
                }

                this.newSurgeon = "false";
                that.chkNPI = 0;
                this.$el.html(this.template_MainModule({
                    "module": this.module
                }));
                if (mod) { // handling tradeshow template main module like as done for leads.
                    this.module = "tradeshow";
                }
                $("#div-main-content").empty();


                if (this.actModel.get("actstartdate") != "") {
                    var frmdt = "";
                    frmdt = this.actModel.get("actstartdate");
                    if (frmdt != "" && frmdt != null) {
                        if (frmdt.indexOf("-") > 0) {
                            frmdt = frmdt.split("-");
                            this.actModel.set("actstartdate", frmdt[1] + "/" + frmdt[2] + "/" + frmdt[0]);
                        } 
                        else {
                            this.actModel.set("actstartdate", frmdt);
                        }
                    }
                }

                if (this.actModel.get("actenddate") != "") {
                    var todt = "";
                    todt = this.actModel.get("actenddate");
                    if (todt != "" && todt != null) {
                        if (todt.indexOf("-") > 0) {
                            todt = todt.split("-");
                            this.actModel.set("actenddate", todt[1] + "/" + todt[2] + "/" + todt[0]);
                        } 
                        else {
                            this.actModel.set("actenddate", todt);
                        }
                    }
                }
                
                
                if (this.module == "lead") {
                    var tech = "",
                        pdt = "",
                        notes = "",
                        relation = "",
                        npi = "",
                        email = "",
                        phone = "";
                    _.each(this.actModel.get("arrgmactivitypartyvo"), function (model) {
                        if (model.roleid == "105564" && model.partynpid != null && model.partynpid.trim() != "")
                            npi = "Y";
                    });
                    _.each(this.actModel.get("arrgmactivityattrvo"), function (model) {
                        if (model.attrtype == "105588" && model.attrnm != null && model.attrnm.trim() != "")
                            tech = "Y";
                        if (model.attrtype == "105582" && model.attrnm != null && model.attrnm.trim() != "")
                            pdt = "Y";
                        if (model.attrtype == "105585" && model.attrval != null && model.attrval.trim() != "")
                            notes = "Y";
                        if (model.attrtype == "7781" && model.attrval != null && model.attrval.trim() != "")
                            relation = "Y";
                        if (model.attrtype == "7801" && model.attrval != null && model.attrval.trim() != "")
                            phone = "Y";
                        if (model.attrtype == "7802" && model.attrval != null && model.attrval.trim() != "")
                            email = "Y"
                    });

                    that.actModel.set("technm", tech);
                    that.actModel.set("prod", pdt);
                    that.actModel.set("notes", notes);
                    that.actModel.set("relation", relation);
                    that.actModel.set("phone", phone);
                    that.actModel.set("email", email);
                    that.actModel.set("npi", npi);
                }
                $("#div-main-content").html(this.template(this.actModel.toJSON()));
                
                $("#span-" + this.module + "-actstartdate").on("click",function(event){
                    that.loadDatePicker(event);
                });
                $("#span-" + this.module + "-actenddate").on("click",function(event){
                    that.loadDatePicker(event);
                });
                
                $("#" + this.module + "-actstartdate").on("change",function(event){
                    that.displaySelctdDt(event);
                });
                $("#" + this.module + "-actenddate").on("change",function(event){
                    that.displaySelctdDt(event);
                });
                
                loadTimepicker("#" + this.module + "-actstarttime",this.actModel.get("actstarttime"));
                loadTimepicker("#" + this.module + "-actendtime",this.actModel.get("actendtime"));
                //                GM.Global.Device = 1;
                if (this.module == "tradeshow") {
                    $('.gmTab').removeClass().addClass('gmTab');
                    $("#lead").addClass("tablead gmFontWhite");
                }
                if (GM.Global.Device && (this.module == "merc" || this.module == "physicianvisit" || this.module == "training")) {
                    $(".OfflineLinkRemover").addClass("hide");
                    $(".OfflineLink").removeClass("hide");
                }

                if (this.module == "lead" && GM.Global.Activity.Mode != "view") {
                    if (this.sData == undefined && this.actid == 0)
                        $(".lead-surgeon-info").addClass("hide");
                    else
                        $(".lead-surgeon-info").removeClass("hide");
                }
                var editAccess = _.findWhere(GM.Global.UserAccess, {
                    funcid: "CRM-SURGEON-EDIT"
                });

                if (editAccess != undefined) {

                    $(".new-surgeon-create").removeClass("hide")
                } else {
                    $(".new-surgeon-create").addClass("hide");
                }


                if ((this.module == "merc") || (this.module == "physicianvisit") || (this.module == "training")) {

                    $(".comments-list").each(function () {
                        if ($(this).find(".comments-updatedby").attr("updatedbyid") != userID) {
                            $(this).find(".comments-edit").addClass('hide');
                            $(this).find(".comments-delete").addClass('hide');
                        }
                    });
                    if (this.actModel.get("arrgmactivitypartyvo") != "") {
                        $.each(this.actModel.get("arrgmactivitypartyvo"), function (index, item) {
                            $(".actpartyrole-" + item.partyid).val(item.roleid);
                            $(".actpartystatus-" + item.partyid).val(item.statusid);
                        });
                    }
                }

                var checkDevice = navigator.userAgent.match(/(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i) != null;
                if (checkDevice) {

                    $("#div-physicianvisit-closure").remove();
                }
                if (GM.Global.Device)
                    $("#create-surgeon").addClass("hide");
                else
                    $("#create-surgeon").removeClass("hide");

                if (GM.Global.SchedulerFL == "Y") {
                    GM.Global.Activity.Mode = "edit";
                }

                if (this.module == "physicianvisit") {
                    if (this.actModel.get("acttype") == '26240596') {
                        $(".restrictedDiv").css("display", "none");
                        $(".hideTimeRestrict").addClass("hide");
                      //  $(".visit-edit-division").addClass("gmVisibilityHidden");
                        $(".phyvisit-content .gmSection .iPhone.gmVerticalMiddle:nth-child(4) .div-legend").css("display", "none");
                         $("#ul-physicianvisit-detail-left-top-right .gmSection.iPhone").addClass("hide");
                    }
                    
                    
                    
                    if(this.actModel.get("actstatus") != '105761' ){
                        $('#div-physicianvisit-travel').addClass('hide');
                    }       
                    
                    if ($(window).width() < 768) {
                        $("#div-physicianvisit-edit-objective").scrollTop(that.curPosTgt);
                    }
                }
                switch (GM.Global.Activity.Mode) {
                    case "create":
                        $(".div-edit").removeClass("hide");
                        $(".gmBtnGroup #li-void").addClass("hide");

                        $(".div-view").addClass("hide");
                        $(".gmBtnGroup #li-cancel").removeClass("hide");
                        $(".gmBtnGroup #li-save").removeClass("hide");


                        if (this.module == "physicianvisit") {
                            var event = {};
                            event.currentTarget = $("#div-physicianvisit-objective");
                            $("#div-physicianvisit-closure").addClass("hide");
                            $("#div-physicianvisit-feedback").addClass("hide");
                            $("#div-physicianvisit-globus").addClass("hide");
                            $("#div-physicianvisit-travel").addClass("hide");
                            this.togglePVTabs(event);
                        
                        //PC-4002 - Block Sales Reps from booking Physician Visit on Restricted Days
                        if (this.actModel.get("acttype") == '26240596') {
                                setTimeout(function () {
                                    $('#divtyp option:first').text('All Division');
                                    $('#divtyp').val(''); // To set All Division as selected value
                                }, 2000);
                                 
                             }
                            else{
                                 setTimeout(function () {
                            $('#divtyp option:first').text('Select');
                              },2000);
                            }
                             
                        }

                        if (this.module == "merc") {

                            var event = {};
                            event.currentTarget = $("#div-merc-program");
                            $("#div-merc-comments").addClass("hide");
                            $("#div-merc-pending-surgeons").addClass("hide");
                            this.toggleMERCTabs(event);
                        }

                        if (this.module == "training") {
                            var event = {};
                            event.currentTarget = $("#div-training-program");
                            $("#div-training-comments").addClass("hide");
                            this.toggleTrainingTabs(event);
                        }
                        //
                        //                    if (this.module == "lead") {
                        //                        this.toogleContact();
                        //                    }
                        break;
                    case "edit":
                        $(".div-edit").removeClass("hide");
                        $(".div-view").addClass("hide");
                        $(".gmBtnGroup #li-edit").addClass("hide");
                        $(".gmBtnGroup #li-cancel").removeClass("hide");
                        $(".gmBtnGroup #li-save").removeClass("hide");
                        $(".gmBtnGroup #li-void").removeClass("hide");

                        if (this.module == "physicianvisit") {
                            var event = {};
                            event.currentTarget = $("#div-physicianvisit-objective");
                            $("#div-physicianvisit-feedback").addClass("hide");
                            $("#div-physicianvisit-closure").addClass("hide");
                            $("#div-physicianvisit-globus").addClass("hide");
                            $("#div-physicianvisit-travel").addClass("hide");
                            this.togglePVTabs(event);   
                            
                        //PC-4002 - Block Sales Reps from booking Physician Visit on Restricted Days
                         if (this.actModel.get("acttype") == '26240596') {
                                 setTimeout(function () {
                                     $('#divtyp option:first').text('All Division');
                                }, 2000);
                                      
                             }
                            
                        }

                        if (this.module == "merc") {
                            var event = {};
                            event.currentTarget = $("#div-merc-program");
                            $("#div-merc-comments").addClass("hide");
                            $("#div-merc-pending-surgeons").removeClass("hide");
                            this.toggleMERCTabs(event);
                        }

                        if (this.module == "training") {
                            var event = {};
                            event.currentTarget = $("#div-training-program");
                            $("#div-training-comments").addClass("hide");
                            this.toggleTrainingTabs(event);
                        }
                        if (this.module == "lead") {

                            this.toogleDetailContact();
                        }



                        break;
                    case "view":
                        $(".div-edit").addClass("hide");
                        $(".div-view").removeClass("hide");

                        var spinnerFl = true;
                        if (this.module == "physicianvisit") {
                            var event = {};
                            event.currentTarget = $("#div-physicianvisit-objective");
                            this.togglePVTabs(event);
                            spinnerFl = false;
                        }

                        if (this.module == "merc") {
                            var event = {};
                            event.currentTarget = $("#div-merc-program");
                            $("#div-merc-comments").removeClass("hide");
                            $("#div-merc-pending-surgeons").addClass("hide");
                            this.toggleMERCTabs(event);
                            spinnerFl = false;
                        }

                        if (this.module == "training") {
                            var event = {};
                            event.currentTarget = $("#div-training-program");
                            $("#div-training-comments").removeClass("hide");
                            this.toggleTrainingTabs(event);
                            spinnerFl = false;
                        }
                        if (this.module == "lead") {

                            this.toogleDetailContact();
                        }

                        if (_.contains(GM.Global.Admin, this.module) && spinnerFl == true && this.module != "physicianvisit") {
                            $(".gmBtnGroup #li-edit").removeClass("hide");
                        }

                        if (spinnerFl == false) {
                            $(".gmBtnGroup #li-spinner").removeClass("hide");
                        }

                        break;
                    default:
                        if (this.module == "tradeshow" && _.contains(GM.Global.Admin, this.module))
                            $(".gmBtnGroup #li-create").removeClass("hide");
                }

                $(".gmPanelTitle").html(this.actModel.get("actnm"));


                switch (this.module) {
                    case "salescall":
                        this.searchSurgeon();
                        this.searchProduct();
                        break;
                    case "lead":
                        this.searchSurgeon();
                        this.searchTradeShow();
                        this.searchProduct();
                        this.searchTechnique();
                        $('#select-leads-source').val("");
                        $('#select-leads-relationship').val("");
                        var src = _.filter(arrayOf(this.actModel.get("arrgmactivityattrvo")), function (data) {
                            return data.attrtype == 105580;
                        });
                        if (src.length > 0)
                            src = src[0].attrval;

                        if (GM.Global.Activity.Mode == "create" && src == "") {
                            var dftTrade = $.parseJSON(localStorage.getItem("DftTradeshow"));
                            if (dftTrade != null) {
                                $("#lead-edit-set-default").html("By Default");
                                this.getTradeShowInfo(dftTrade.ID, dftTrade.Name);
                            }
                        }
                        if (GM.Global.Activity.Mode == "view" && this.actid != undefined && this.actid != 0) {
                            var party = _.filter(this.actModel.get("arrgmactivitypartyvo"), function (data) {
                                return data.roleid == 105564;
                            });
                        }
                        break;
                    case "merc":
                        this.getUploadFiles();
                        this.searchSurgeon();
                        this.getCRFStatus();
                        this.fileUpload();
                        this.fileUploadFL = "Y";
                        break;
                    case "physicianvisit":
                        getCLValToDiv("#divtyp", "", "", "DIVLST", "", "", "codenmalt", "division", true, true, function(divID, attrVal){
                            that.setSelect(divID, that.actModel.get(attrVal));
                        });
                        getCLValToDiv(".select-comments-logtype", "", "", "CRMLG", "PHYSLG", "", "codeid", "logtype", false, true, function(divID, attrVal){
                            that.setSelect(divID, that.actModel.get(attrVal));
                        });
                        var chkAdmin = _.findWhere(GM.Global.UserAccess, {
                                    funcnm: "PHYSVISIT-ADMIN"
                        }); 
                        if(chkAdmin == undefined){
                            getCLValToDiv("#acttype", "", "", "SGACTP", "PHYVST", "2005", "codeid", "acttype", true, true, function(divID, attrVal){
                                that.setSelect(divID, that.actModel.get(attrVal));
                            });
                        }
                        else{
                            getCLValToDiv("#acttype", "", "", "SGACTP", "PHYVST", "", "codeid", "acttype", true, true, function(divID, attrVal){
                                that.setSelect(divID, that.actModel.get(attrVal));
                            });
                        }
                        this.searchSurgeon();
                        this.getCRFStatus();
                        this.searchProduct();
                        this.searchProject();
                        this.searchParticipant();
                        this.searchProductLab();
                        this.fileUpload();
                        this.checkDivVisible();
                        this.fileUploadFL = "Y";
                        setTimeout(function () {
                            that.getUploadFiles();                           
                      });
                        
                        break;
                    case "training":
                        this.getUploadFiles();
                        this.searchSurgeon();
                        this.getCRFStatus();
                        this.fileUpload();
                        this.fileUploadFL = "Y";
                        break;
                    case "tradeshow":
                        this.searchTradeShow();
                        this.getUploadFiles();
                        if (GM.Global.Activity.Mode == "view")
                            this.fileUpload();
                        this.fileUploadFL = "Y";
                        break;
                }

                if (this.actModel.get("actnm") == "")
                    $("#ul-salescall-title").hide();
                    $("#ul-salescall-surgeon").hide();

                $.each(this.actModel.get("arrgmactivityattrvo"), function (index, item) {
                    if (item != null) {

                        if (item.attrtype == 105580)
                            that.setSelect("#select-leads-source", item.attrval);
                        if (item.attrtype == 7781)
                            that.setSelect("#select-leads-relationship", item.attrval);

                    }
                });

                if (this.module == "lead") {
                    // refer with cskumar if changing this code
                    var dftTrade = $.parseJSON(localStorage.getItem("DftTradeshow"));
                    var ts = _.filter(this.actModel.get("arrgmactivityattrvo"), function (data) {
                        return data.attrtype == 7779;
                    });
                    if (ts.length)
                        ts = ts[0].attrval;
                    else
                        ts = ""
                    var src = $('#select-leads-source').val();
                    if (GM.Global.Activity.Mode == "create" && dftTrade != null && (src == null || dftTrade.ID == ts)) {
                        if (dftTrade != null) {
                            $("#lead-edit-set-default").html("By Default");
                            this.getTradeShowInfo(dftTrade.ID, dftTrade.Name);
                        }
                    }
                }


                if (this.module == "lead" && $("#select-leads-source").val() != null && $("#select-leads-source").val() != 105349)
                    $("#ul-lead-tradeshow").addClass("hide");

                var that = this;
                var preRst = _.filter(this.actModel.get("arrgmactivityattrvo"), function (data) {
                    return data.attrtype == "105581";
                });

                if (this.actid == undefined && this.module != "tradeshow") {
                    if (this.actModel.get("arrgmactivitypartyvo") == "") {
                        if (GM.Global.ActSurgInfo) {
                            var partyid = GM.Global.ActSurgInfo.surgeonid;
                            var name = parseNull(GM.Global.ActSurgInfo.salutenm) + " " + GM.Global.ActSurgInfo.firstnm + " " + parseNull(GM.Global.ActSurgInfo.midinitial) + " " + GM.Global.ActSurgInfo.lastnm;
                            var partynm = name;
                            var partyinfo = {
                                "ID": partyid,
                                "Name": partynm
                            };
                            this.setPartyInfo(partyinfo, "7000", "105564");
                        }
                    }
                }

                if (this.actid == 0 || this.actid == undefined) {
                    var newParticipants = _.findWhere(this.actModel.get("arrgmactivitypartyvo"), {
                        roleid: "105561"
                    });
                    if (newParticipants == undefined) {
                        var partyinfo = {
                            "ID": localStorage.getItem("partyID"),
                            "Name": localStorage.getItem("fName") + " " + localStorage.getItem("lName"),
                        };
                        this.setPartyInfo(partyinfo, "7006", "105561");
                    }

                    $("#li-physicianvisit-title").parent().addClass("hide");
                    var getDivision = this.actModel.get("division");
                    if (!this.actModel.get("acttype")=='26240596'){ // PC-4002
                        if (getDivision == "")
                        this.actModel.set("division", 2000); 
                    }
                }

                if (this.actid > 0) {
                    _.each(this.actModel.get("arrgmactivitypartyvo"), function (data) {

                        if ($("#cv-links-" + data.partyid).length > 0) {
                            $("#cv-links-" + data.partyid + " span").attr("href", URL_CRMFileServer + data.surgeoncv);
                        }
                    });
                }

                if (this.actstatus == 'Cancelled')
                    $("#ul-visit-cancel-reson").removeClass("hide");

                $(".div-actstatus").html(this.actstatus);

                if (this.actstatus == 'Scheduled')
                    $("#div-salescall-cancel").removeClass("hide");

                if (this.actModel.get("arrgmactivitypartyvo")[0] != undefined)
                    GM.Global.SurgeonPartyId = this.actModel.get("arrgmactivitypartyvo")[0].partyid;
                if (that.module == "tradeshow" || that.module == "lead") {
                    $(".span-doc-act-edit").html("( File Type - .xlsx,.xls )");
                } else
                    $(".span-doc-act-edit").html("( File Type - .doc,.docx,.pdf,.txt ) ");

                if (GM.Global.Activity.Mode == "create") {
                    $("#span-activity-ctrl-num").addClass("hide");
                    $("#activity-controlid-training").removeClass("hide");
                    $("#activity-controlid-merc").removeClass("hide");

                } else if (GM.Global.Activity.Mode == "edit") {
                    $("#span-activity-ctrl-num").removeClass("hide");
                    $("#activity-controlid-training").addClass("hide");
                    $("#activity-controlid-merc").addClass("hide");
                }

                if (this.interest)
                    this.toogleInterest();
                else
                    this.toogleContact();

                if (that.module == "merc")
                    that.checkSelected();

                if (this.renderCount != undefined && this.module == "physicianvisit" && GM.Global.Activity.Mode != "view" && this.renderCount > 0) {
                    for (var i = 0; i < this.renderCount; i++) {
                        this.addOtherVisitor();
                    }
                }
                if ((GM.Global.SchedulerFL == "Y") && (this.module == "physicianvisit" || this.module == "salescall")) {
                    GM.Global.SchedulerFL = "";
                    setTimeout(function () {
                        $("#btn-main-edit").trigger("click");
                    });
                }

                if (this.actModel.get("createdbyid") != userID && this.module == "salescall" && parseNull(this.actid) != 0) {
                    GM.Global.Activity.Mode = "view";
                    $(".gmBtnGroup #li-edit").addClass("hide");
                }


                this.gmvValidation = new GMVValidation({
                    "el": this.$(".gmPanelContent")
                });
                resizeHeadWidth('.gmPanelHead', '.gmBtnGroup', '#panelhead-left');
                return this;
            },
             
            setActTitle: function () {
                var that = this;
                this.actSurgeon = "";
                this.actParticipants = "";
                this.actOtherVisitors = "";
                var actTypeNm = "";
                _.each(this.actModel.get("arrgmactivitypartyvo"), function (data) {
                    if (data.roleid != 105561 && data.voidfl != "Y")
                        that.actSurgeon += data.partynm + ", ";
                    if (data.roleid == 105561 && data.voidfl != "Y")
                        that.actParticipants += data.partynm + ", ";

                });
                _.each(this.actModel.get("arrgmactivityattrvo"), function (data) {
                    if (data.attrtype == 105590 && data.attrnm != "" && data.attrval != "" && data.voidfl != "Y")
                        that.actOtherVisitors += data.attrval + ", ";
                });

                var titleVsitor = this.actSurgeon + " " + this.actOtherVisitors;
                titleVsitor = titleVsitor.replace(/,(?=[^,]+$)/, '');
                titleVsitor = titleVsitor.replace(/,(?=[^,]+$)/, ' and');

                var titleParty = this.actParticipants.replace(/,(?=[^,]+$)/, '');
                titleParty = titleParty.replace(/,(?=[^,]+$)/, ' and');

                
                if (this.actModel.get("acttype")=='26240596'){
                    var divisionName = $("#divtyp option:selected").text();
                    $("#input-physicianvisit-title").val(divisionName.toUpperCase() + " : Restricted Days");// PC-4002
                    $("#li-physicianvisit-title").text(divisionName.toUpperCase() + " : Restricted Days");// PC-4002
                    $(".gmPanelTitle").text("Restricted Days");
                }
                else{
                    var divisionName = $("#divtyp option:selected").text();

                    if (this.actModel.get("acttype") != "")
                        actTypeNm = $("#acttype option:selected").text() + " w/ ";

                    $("#input-physicianvisit-title").val(divisionName.toUpperCase() + ": " + parseNull(titleVsitor) + " " + parseNull(actTypeNm) + " " + parseNull(titleParty));
                    $("#li-physicianvisit-title").text(divisionName.toUpperCase() + ": " + parseNull(titleVsitor) + " " + parseNull(actTypeNm) + " " + parseNull(titleParty));
                    $(".gmPanelTitle").text(divisionName.toUpperCase() + ": " + parseNull(titleVsitor) + " " + parseNull(actTypeNm) + " " + parseNull(titleParty));
                }


                this.actModel.set("actnm", $("#input-physicianvisit-title").val());
            },

            // Opens the Multiple File Upload popup
            multipleFileUpld: function () {
                if (this.mfu)
                    this.mfu.render();
                else
                    this.mfu = new GMVMultiFileUpload({
                        arrAcceptetFormat: ['doc', 'docx', 'pdf', 'txt', 'xlsx', 'xls', 'ppt', 'pptx'],
                        parent: this,
                        parentID: this.actid,
                        viewMode: GM.Global.Activity.Mode,
                        refgroup: 107994,
                        fileData: this.pvFiles
                    });
            },
            
            //Load common datepicker function
            loadDatePicker: function(event){
                var elemId = "#" + $(event.target).data("id");
                var propName = event.currentTarget.name;
                loadDatepicker(event,elemId,this.actModel.get(propName),"mm/dd/yy",new Date(),"");
            },
            
            displaySelctdDt: function(event){
                var elemId = "#" + $(event.target).attr("id");
                var elemVal = $(elemId).val();
                if(elemVal == "")
                   $(event.target).siblings("span").text("mm/dd/yyyy");
                else
                    $(event.target).siblings("span").text(elemVal);               
            },

            //validation for npid
            checkNpid: function (e) {
                var that = this;
                var val;
                $("#img-npid-spinner").toggleClass('hide');
                $(e.currentTarget).toggleClass('hide');
                if (this.newSurgeon == "true") {
                    val = $("#input-new-surgeon-npid").val();
                    this.newSurgeon = "false"
                } else
                    val = $("#input-lead-contacts-npid").val();
                var re = /[^0-9]/g;
                var error = re.test(val);
                //            hideMessages();
                if (val != "") {
                    if (error == true) { //npid should be numberic and with max length of 10
                        $("#popup-create-surgeon-validation").html("NPI Should be Numeric");
                        $("#img-npid-spinner").toggleClass('hide');
                        $(e.currentTarget).toggleClass('hide');
                        $(e.currentTarget).val("").focus();
                    } else {
                        $("#popup-create-surgeon-validation").html("");
                        getSurgeonPartyInfo(val, function (surgeonData) {
                            var partyid;
                            if (that.newSurgeon == "true")
                                partyid = 0;
                            else
                                partyid = GM.Global.SurgeonPartyId; //calling surgeon's party information of typed in npid
                            //that.chkNPI == 1 already allocated;  that.chkNPI == 0 npid verified
                            if (surgeonData != undefined) {
                                surgeonData = surgeonData[0];
                                if (partyid == 0) {
                                    $(".npi-detection.fa-check-circle-o").remove();
                                    if ($(".npi-detection.fa-times-circle").length == 0)
                                        $(".li-popup-npid-create").append("<span class='fa npi-detection fa-times-circle'></span>");
                                    that.chkNPI = 1;
                                } else {
                                    if (surgeonData.partyid == partyid) {
                                        $(".npi-detection.fa-times-circle").remove();
                                        if ($(".npi-detection.fa-check-circle-o").length == 0)
                                            $(".li-popup-npid-create").append("<span class='fa npi-detection fa-check-circle-o'></span>");
                                        that.chkNPI = 0;
                                    } else {
                                        $(".npi-detection.fa-check-circle-o").remove();
                                        if ($(".npi-detection.fa-times-circle").length == 0)
                                            $(".li-popup-npid-create").append("<span class='fa npi-detection fa-times-circle'></span>");
                                        that.chkNPI = 1;
                                    }
                                }
                            } else {
                                $(".npi-detection.fa-times-circle").remove();
                                if ($(".npi-detection.fa-check-circle-o").length == 0)
                                    $(".li-popup-npid-create").append("<span class='fa npi-detection fa-check-circle-o'></span>");
                                that.chkNPI = 0;
                            }
                            $("#img-npid-spinner").toggleClass('hide');
                            $(e.currentTarget).toggleClass('hide');
                        });
                    }

                } else { //empty
                    $("#popup-create-surgeon-validation").html("");
                    if ($(".npi-detection.fa-times-circle").length != 0)
                        $(".npi-detection.fa-times-circle").remove();
                    if ($(".npi-detection.fa-check-circle-o").length != 0)
                        $(".npi-detection.fa-check-circle-o").remove();
                    $("#img-npid-spinner").toggleClass('hide');
                    $(e.currentTarget).toggleClass('hide');
                    this.chkNPI = 0;
                }

            },



            // Check the Control number is avaible or not
            checkCtrlNo: function (e) {
                var that = this;
                if (GM.Global.Activity.Mode == "create") {
                    $("#check-ctrlno").empty();
                    var ctrlNo;
                    if (that.module == "merc")
                        ctrlNo = $("#activity-controlid-merc").val();
                    if (that.module == "training")
                        ctrlNo = $("#activity-controlid-training").val();
                    var numberPattern = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
                    var textPattern = /^[a-zA-Z]+$/;
                    if (numberPattern.test(ctrlNo) == false) {
                        var token = localStorage.getItem("token");
                        if (ctrlNo != "") {
                            var actcat;
                            if (that.module == "merc")
                                actcat = "105265";
                            if (that.module == "training")
                                actcat = "105268";
                            var input = {
                                "token": token,
                                "actid": ctrlNo,
                                "actcategory": actcat
                            };
                            fnGetWebServerData("crmactivity/info", "gmCRMActivityVO", input, function (data) {
                                if (data != undefined && data != "") {
                                    data = data.GmCRMListVO.gmCRMActivityVO[0];
                                    if (data.actid != "") {
                                        if (GM.Global.Activity.Mode == "edit") {
                                            that.validateControlNo = "true";
                                        } else {
                                            showError("Control Number already exists,Please change");
                                            that.validateControlNo = "false";
                                        }
                                    } else {
                                        that.validateControlNo = "true";
                                        $(".check-ctrlno").remove();
                                        $(".li-ctrl-numb").append("<i id='check-ctrlno' class='fa check-ctrlno  fa-check gmFontRed' aria-hidden='true'></i>")
                                    }
                                } else {
                                    showError("Please enter the Contol Number");
                                    that.validateControlNo = "false";
                                }

                            });
                        } else {
                            showError("Please enter the Contol Number");
                            that.validateControlNo = "false";
                        }
                    } else {
                        showError("Please enter Control number in format as displayed in Placeholder");
                        that.validateControlNo = "false";
                    }
                }
            },

            clearAddress: function () {

                $("#popup-address").val("");
                $("#popup-city").val("");
                $("#popup-zipcode").val("");
                $("#popup-state").val("");
                $("#popup-address2").val("");
                $("#popup-country").val("");
                $("#div-address-validation").html("");
            },
            selectDate: function (e) {
                var that = this;
                var target = $(e.currentTarget).attr("id");
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1;
                var yyyy = today.getFullYear();
                if (dd < 10)
                    dd = '0' + dd
                if (mm < 10)
                    mm = '0' + mm
                var activityMdl=that.actModel.toJSON();
                var hours = today.getHours();
                var minutes = today.getMinutes();
                var currntTime = hours + ':' + minutes;
                today = mm + '/' + dd + '/' + yyyy;
                
                var todayFmt = new Date(today);
                var startdate = $("#" + this.module + "-actstartdate").val();
                var enddate = $("#" + this.module + "-actenddate").val();
                var starttime = $("#" + this.module + "-actstarttime").val();
                var endtime = $("#" + this.module + "-actendtime").val();
                var startDateFmt = new Date(startdate);
                var toDateFmt = new Date(enddate);
                if (isRep()) {
                    if (startdate != "" && todayFmt > startDateFmt)
                        $("#" + that.module + "-actstartdate").val(today).trigger("change");
                }

                if ((startdate != "" && enddate == "" && target == that.module + "-actstartdate") || (startdate != "" && enddate != "" && toDateFmt < startDateFmt)) {
                    $("#" + that.module + "-actenddate").val(startdate).trigger("change");
                }


                if ((activityMdl.acttype == '26240596') && (startdate != "" && starttime != "00:00" && endtime != "23:59")) {
//                    $("#" + that.module + "-actstarttime").val("00:00").trigger("change");
//                    $("#" + that.module + "-actendtime").val("23:59").trigger("change");
                    $("#" + that.module + "-actstarttime").mdtimepicker('setValue','00:00');
                    $("#" + that.module + "-actendtime").mdtimepicker('setValue','23:59');
                    $("#" + that.module + "-actstarttime").trigger("change");
                    $("#" + that.module + "-actendtime").trigger("change");
                } else if (startdate != "" && starttime == "" && endtime == "") {
//                    $("#" + that.module + "-actstarttime").val("09:00").trigger("change");
//                    $("#" + that.module + "-actendtime").val("15:00").trigger("change");
                    $("#" + that.module + "-actstarttime").mdtimepicker('setValue', '09:00');
                    $("#" + that.module + "-actendtime").mdtimepicker('setValue', '15:00');
                    $("#" + that.module + "-actstarttime").trigger("change");
                    $("#" + that.module + "-actendtime").trigger("change");
                }
                
                if (startdate == enddate) {
                    if (starttime != "" && endtime != "" && endtime < starttime){
//                        $("#" + that.module + "-actendtime").val(starttime).trigger("change");
                        $("#" + that.module + "-actendtime").mdtimepicker('setValue', starttime);
                        $("#" + that.module + "-actendtime").trigger("change");
                    }
                }
                else{
                    if (startdate != "" && starttime == "" && endtime != "") {
                        $("#" + that.module + "-actstarttime").mdtimepicker('setValue', endtime);
                        $("#" + that.module + "-actstarttime").trigger("change");
                    } else if ((starttime != "" && endtime != "" && endtime < starttime) || (startdate != "" && starttime != "" && endtime == "")) {
                        $("#" + that.module + "-actendtime").mdtimepicker('setValue', starttime);
                        $("#" + that.module + "-actendtime").trigger("change");
                    }
                }
                 //PC - 4002 - Block Sales Reps from booking Physician Visit on Restricted Days
//                if(that.module == "physicianvisit"){
//                    if ((target == that.module + "-actstartdate" || target == that.module + "-actenddate") && startdate != "" && enddate != "") {
//                        var input = {
//                            "token": localStorage.getItem("token"),
//                            "actstartdate": $("#" + this.module + "-actstartdate").val(),
//                            "actenddate": $("#" + this.module + "-actenddate").val(),
//                            "actid": activityMdl.actid
//                        }
//                        fnGetWebServerData("crmactivity/chkblocked", "GmCRMActivityVO", input, function (data) {
//                            console.log(data);
//                            if (data.result != "0") {
//                                that.restrictFl = 'Y';
//                            } else {
//                                that.restrictFl = '';
//                            }
//
//                        });
//                    }
//                }
                
            },
            getUploadFiles: function () {
                var that = this;
                if ((GM.Global.Activity.Mode == "view" && this.nameFile) || (GM.Global.Activity.Mode == "edit" && this.module == "physicianvisit" || this.module == "merc" || this.module == "training")) {
                    if (this.module == "tradeshow") {
                        var tempArray = _.filter(that.actModel.get("arrgmactivityattrvo"), function (data) {
                            return data.attrtype == 7805;
                        });
                        var fileid = "";
                        if (tempArray.length > 0)
                            fileid = tempArray[0].attrval;

                        GM.Global.Fileid = fileid;
                        var input = {
                            "token": localStorage.getItem("token"),
                            "refid": this.actid,
                            "refgroup": "107994",
                            "fileid": fileid
                        };
                    } else
                        var input = {
                            "token": localStorage.getItem("token"),
                            "refid": this.actid,
                            "refgroup": "107994"
                        };


                    if (!GM.Global.Device) {
                        fnGetWebServerData("file/get", "gmCRMFileUploadVO", input, function (file) {
                            //                            console.log("gmCRMFileUploadVO result" + JSON.stringify(file));
                            if (GM.Global.Activity.Mode == "view") {
                                var todayDate = new Date();
                                todayDate.setHours(0, 0, 0, 0);
                                var visitSDate = new Date(that.actModel.get("actstartdate"));
                                visitSDate.setHours(0, 0, 0, 0);
                                var userID = localStorage.getItem("userID");
                                if (_.contains(GM.Global.Admin, "physvisitrqst") && that.actModel.get("createdbyid") == userID && that.actModel.get("actstatus") == "105760") {
                                    $(".gmBtnGroup #li-edit").removeClass("hide");
                                }


                                if ((that.module == "physicianvisit") || (that.module == "merc") || (that.module == "training")) {
                                    $(".gmBtnGroup #li-spinner").addClass("hide");
                                }

                                if (_.contains(GM.Global.Admin, "physvisitrqst") && that.actstatus == "Scheduled" && that.module == "physicianvisit") {
                                    $(".gmBtnGroup #li-edit").addClass("hide");
                                }
                                if (that.module == "physicianvisit" && GM.Global.Device && that.actModel.get("pvEdit") == "Y")
                                    $(".gmBtnGroup #li-edit").removeClass("hide");
                                if (_.contains(GM.Global.Admin, that.module)) { // PV Admin 
                                    $(".gmBtnGroup #li-edit").removeClass("hide");
                                }
                                if (that.actstatus == "Cancelled" || that.actModel.get("actstatus") == "105762") { // Cancelled visit 
                                    $(".gmBtnGroup #li-edit").addClass("hide");
                                }

                                resizeHeadWidth('.gmPanelHead', '.gmBtnGroup', '#panelhead-left');
                            }
                            if (file != null) {
                                file = arrayOf(file);
                                if ((that.module == "physicianvisit") || (that.module == "merc") || (that.module == "training")) {

                                    that.pvFiles = file;
                                    $(".upload-files-list").html("");
                                    _.each(file, function (v) {
                                        var docLink = URL_CRMFileServer + v.refgroup + "/" + v.refid + "/" + v.filename;
                                        $(".upload-files-list").append("<div class = 'a-view-docs gmLnk gmUnderline gmPaddingButtom10p'><span class='hide hidden-file'>" + docLink + "</span> <span class='' href = '" + URL_CRMFileServer + v.refgroup + "/" + v.refid + "/" + v.filename + "'>" + v.filetitle + " - " + v.updatedby + " on " + v.updateddate + "</span></div>");
                                    });
                                } else {
                                    file = file[0];
                                    GM.Global.Fileid = file.fileid;

                                    $(".a-service-desc-agenda-file-name").text(file.filetitle);
                                    $(".a-service-desc-agenda-file-name").attr("href", URL_CRMFileServer + file.refgroup + "/" + file.refid + "/" + file.filename);
                                    $(".a-service-desc-agenda-file-name").attr("data-fileid", file.fileid);
                                }
                                if (that.module == "tradeshow") {
                                    $("#file-upload-link").attr("href", URL_CRMFileServer + file.refgroup + "/" + file.refid + "/" + file.filename);
                                    $("#span-fileupload-browse-filenm").html(file.filename);
                                    $("#ul-tradeshow-file-detail").removeClass("hide");
                                    $("#span-lead-browse-lastupdated-by").html(file.updatedby);
                                    $("#span-lead-browse-lastupdated-on").html(file.updateddate);
                                } else if ((that.module != "physicianvisit") || (that.module != "merc") || (that.module != "training"))
                                    $("#span-fileupload-browse-filenm").html(file.filetitle);
                            } else {
                                $(".a-service-desc-agenda-file-name").html("Unavailable").addClass("gmFontItalic");
                                if ((that.module == "physicianvisit") || (that.module == "merc") || (that.module == "training")) {
                                    if (GM.Global.Activity.Mode == "view")
                                        that.pvFiles = [];
                                    $(".upload-files-list").html("Unavailable").addClass("gmFontItalic");
                                }
                            }
                        });
                    } else {
                        fnGetCRMDocumentDetail(this.actid, 107994, function (file) {
                            //                            console.log(file)
                            if (file.length) {
                                GM.Global.Fileid = file.fileid;
                                file = arrayOf(file);
                                fnGetLocalFileSystemPath(function (localpath) {
                                    _.each(file, function (v) {
                                        $(".upload-files-list").append("<div class = 'gmLnk gmUnderline gmPaddingButtom10p'><span class='a-view-docs' href = '" + localpath + file.refgroup + "/" + file.refid + "/" + file.filename + "' target='_blank'>" + file.filetitle + " - " + file.updatedby + " on " + file.updateddate + "</span></div>");
                                    });
                                });

                            } else {
                                $(".upload-files-list").html("Unavailable").addClass("gmFontItalic");
                            }
                        });
                    }
                }
            },
            setSelect: function (controlid, selectedvalue) {
                $(controlid).val(selectedvalue);
            },
            toogleContact: function () {
                this.interest = false;
                $("#tab-interest").removeClass("gmBGDarkerGrey").removeClass("gmFontWhite");
                $("#tab-contact").addClass("gmBGDarkerGrey").addClass("gmFontWhite");
                $(".lead-edit-tech-mobile").addClass("lead-style-phone");
                $(".lead-mobile-toogle-contact").removeClass("lead-style-phone");
                $(".ul-lead-toogle-address").addClass("lead-style-phone");
                $(".lead-select-address").removeClass("lead-style-phone");
                $(".lead-contacts-input-address-edit").removeClass("lead-style-phone");
            },
            toogleInterest: function () {
                this.interest = true;
                $("#tab-contact").removeClass("gmBGDarkerGrey").removeClass("gmFontWhite");
                $("#tab-interest").addClass("gmBGDarkerGrey").addClass("gmFontWhite");
                $(".lead-edit-tech-mobile").removeClass("lead-style-phone");
                $(".lead-mobile-toogle-contact").addClass("lead-style-phone");
                $(".ul-lead-toogle-address").addClass("lead-style-phone");
                $(".lead-select-address").addClass("lead-style-phone");
            },
            toogleAddress: function () {
                $(".lead-edit-tech-mobile").addClass("lead-style-phone");
                $(".lead-mobile-toogle-contact").addClass("lead-style-phone");
                $(".ul-lead-toogle-address").removeClass("lead-style-phone");
                $(".lead-contacts-input-address-edit").addClass("lead-style-phone");
            },
            toogleTechniques: function () {

                $("#tab-contact-detail").removeClass("gmBGDarkerGrey").removeClass("gmFontWhite");
                $("#tab-techniques").addClass("gmBGDarkerGrey").addClass("gmFontWhite");
                $(".lead-tab-tradeshow").removeClass("lead-style-phone");
                $(".lead-tab-contact-detail").addClass("lead-style-phone");

            },
            toogleDetailContact: function () {

                $("#tab-techniques").removeClass("gmBGDarkerGrey").removeClass("gmFontWhite");
                $("#tab-contact-detail").addClass("gmBGDarkerGrey").addClass("gmFontWhite");
                $(".lead-tab-tradeshow").addClass("lead-style-phone");
                $(".lead-tab-contact-detail").removeClass("lead-style-phone");
            },
            toogleRoleStatus: function (e) {
                if ($(e.currentTarget).parent().parent().hasClass("selected")) {
                    $(".selected").find(".div-surgeon-list-sub-data").addClass("gmMediaDisplayNone");
                    $(e.currentTarget).parent().parent().removeClass("selected");
                    $(".ul-merc-surgeon-list-details").removeClass("selected");
                } else {
                    $(".div-surgeon-list-sub-data").addClass("gmMediaDisplayNone");
                    $(".ul-merc-surgeon-list-details").removeClass("selected");
                    $(e.currentTarget).parent().parent().addClass("selected");
                    $(".selected").find(".div-surgeon-list-sub-data").removeClass("gmMediaDisplayNone");
                }
            },

            // Functin for edit the activity 
            editActivity: function (model) {
                var that = this;
                GM.Global.Activity.Mode = "edit";
                if (GM.Global.Device && this.actModel.get("actstarttime") && this.actModel.get("actstarttime").toString().indexOf("-") >= 0) {
                    this.actModel.set("actstarttime", fmtTime(this.actModel.get("actstarttime")));
                    this.actModel.set("actendtime", fmtTime(this.actModel.get("actendtime")));
                }

                $("#span-activity-ctrl-num").removeClass("hide");
                $("#activity-controlid-training").addClass("hide");
                $("#activity-controlid-merc").addClass("hide");

                $(".gmBtnGroup #li-cancel").removeClass("hide");
                $(".gmBtnGroup #li-save").removeClass("hide");

                if (_.contains(GM.Global.Admin, this.module))
                    $(".gmBtnGroup #li-void").removeClass("hide");

                var todayDate = new Date();
                todayDate.setHours(0, 0, 0, 0);
                var visitSDate = new Date(this.actModel.get("actstartdate"));
                visitSDate.setHours(0, 0, 0, 0);

                var userID = localStorage.getItem("userID");

                if (_.contains(GM.Global.Admin, "physvisitrqst") && todayDate <= visitSDate && this.actModel.get("createdbyid") == userID && this.actModel.get("actstatus") == "105760") {
                    $(".gmBtnGroup #li-void").removeClass("hide");
                }

                $(".gmBtnGroup #li-edit").addClass("hide");
                $(".gmBtnGroup #li-create").addClass("hide");
                $(".div-edit").removeClass("hide");
                $(".div-view").addClass("hide");
                _.each(this.actModel.get("arrgmactivityattrvo"), function (data) {

                    if (data.attrtype == "7779") {
                        that.tradeID = data.attrval;
                        that.tradeName = data.attrnm
                    }
                });
                if (this.module == "physicianvisit") {
                    var event = {};
                    event.currentTarget = $("#div-physicianvisit-objective");
                    $("#div-physicianvisit-feedback").addClass("hide");

                    $("#div-physicianvisit-globus").addClass("hide");
                    $("#div-physicianvisit-closure").addClass("hide");
                    $("#div-physicianvisit-travel").addClass("hide");
                    this.togglePVTabs(event);
                    if (that.actModel.get("acttype") == '26240596') {
                        $("#acttype").attr('disabled' ,'disabled');
                        $('#divtyp option:first').text('All Division');
                    }else{
                          $('#divtyp option:first').text('Select');
                    }
                }

                if (this.module == "merc") {
                    var event = {};
                    event.currentTarget = $("#div-merc-program");
                    $("#div-merc-comments").addClass("hide");
                    $("#div-merc-pending-surgeons").removeClass("hide");
                    this.toggleMERCTabs(event);
                }
                if (this.module == "training") {
                    var event = {};
                    event.currentTarget = $("#div-training-program");
                    $("#div-training-comments").addClass("hide");
                    this.toggleTrainingTabs(event);
                }

                this.checkDivVisible();
                resizeHeadWidth('.gmPanelHead', '.gmBtnGroup', '#panelhead-left');
            },

            // Switch between to the tab in the physicianvisit module
            togglePVTabs: function (e) {
                var that = this;
                //                hideMessages();
                colorControl(e, "phyvisit-tab", "gmBGLGrey", "gmBGDarkerGrey gmFontWhite");
                var target = $(e.currentTarget).attr("id");
//                if ($(e.currentTarget).hasClass("tab-opened")) {
//                    $(".phyvisit-content").addClass("gmMediaDisplayNone");
//                    $(".phyvisit-tab").removeClass("tab-closed");
//                    $(e.currentTarget).removeClass("tab-opened tab-closed");
//                    $("#div-physicianvisit-sub-content").addClass("gmMediaDisplayNone");
//                    $(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-right");
//                    $("#physicianvisit-status-btns").removeClass("gmMediaDisplayNone");
//                } else {
//                    $(".phyvisit-content").removeClass("gmMediaDisplayNone");
//                    $(".phyvisit-tab").removeClass("tab-opened").addClass("tab-closed");
//                    $(e.currentTarget).addClass("tab-opened").removeClass("tab-closed");
//                    $("#div-physicianvisit-sub-content").removeClass("gmMediaDisplayNone");
//                    $(".fa-chevron-right").removeClass("fa-chevron-right").addClass("fa-chevron-down");
//                    $("#physicianvisit-status-btns").addClass("gmMediaDisplayNone");
//                }

                if (GM.Global.Activity.Mode == "view") {

                    if (target == "div-physicianvisit-objective") {
                        $("#div-physicianvisit-view-objective").removeClass("hide");
                        $("#div-physicianvisit-view-surgeons").addClass("hide");
                        $("#div-physicianvisit-edit-feedback").addClass("hide");
                        $("#div-physicianvisit-view-closure").addClass("hide");
                        $("#div-physicianvisit-view-experience").addClass("hide");
                         $("#div-physicianvisit-view-travel").addClass("hide");
                    } else if (target == 'div-physicianvisit-surgeons') {
                        $("#div-physicianvisit-view-surgeons").removeClass("hide");
                        $("#div-physicianvisit-view-objective").addClass("hide");
                        $("#div-physicianvisit-edit-feedback").addClass("hide");
                        $("#div-physicianvisit-view-closure").addClass("hide");
                        $("#div-physicianvisit-view-experience").addClass("hide");
                         $("#div-physicianvisit-view-travel").addClass("hide");
                    } else if (target == "div-physicianvisit-feedback") {
                        $("#div-physicianvisit-edit-feedback").removeClass("hide");
                        $("#div-physicianvisit-view-surgeons").addClass("hide");
                        $("#div-physicianvisit-view-objective").addClass("hide");
                        $("#div-physicianvisit-view-closure").addClass("hide");
                        $("#div-physicianvisit-view-experience").addClass("hide");
                         $("#div-physicianvisit-view-travel").addClass("hide");
                        $(".selectbox-icon").show();
                    } else if (target == "div-physicianvisit-closure") {
                        $("#div-physicianvisit-view-closure").removeClass("hide");
                        $("#div-physicianvisit-view-surgeons").addClass("hide");
                        $("#div-physicianvisit-view-objective").addClass("hide");
                        $("#div-physicianvisit-edit-feedback").addClass("hide");
                        $("#div-physicianvisit-view-experience").addClass("hide");
                         $("#div-physicianvisit-view-travel").addClass("hide");
                        $(".selectbox-icon").show();
                    } else if (target == "div-physicianvisit-globus") {
                        $("#div-physicianvisit-view-closure").addClass("hide");
                        $("#div-physicianvisit-view-surgeons").addClass("hide");
                        $("#div-physicianvisit-view-objective").addClass("hide");
                        $("#div-physicianvisit-edit-feedback").addClass("hide");
                        $("#div-physicianvisit-view-experience").removeClass("hide");
                         $("#div-physicianvisit-view-travel").addClass("hide");
                        $(".selectbox-icon").show();
                    }else if (target == "div-physicianvisit-travel") {
                        $("#div-physicianvisit-view-travel").removeClass("hide");
                        $("#div-physicianvisit-view-objective").addClass("hide");
                        $("#div-physicianvisit-view-surgeons").addClass("hide");
                        $("#div-physicianvisit-edit-feedback").addClass("hide");
                        $("#div-physicianvisit-view-closure").addClass("hide");
                        $("#div-physicianvisit-view-experience").addClass("hide");
                    } 
                } else {
                    if (target == "div-physicianvisit-objective") {
                        $("#div-physicianvisit-edit-objective").removeClass("hide");
                        $("#div-physicianvisit-edit-surgeons").addClass("hide");
                        $("#div-physicianvisit-edit-feedback").addClass("hide");
                        $("#div-physicianvisit-view-closure").addClass("hide");

                    } else if (target == 'div-physicianvisit-surgeons') {
                        $("#div-physicianvisit-edit-surgeons").removeClass("hide");
                        $("#div-physicianvisit-edit-objective").addClass("hide");
                        $("#div-physicianvisit-view-closure").addClass("hide");
                    }
                }
            },

            toggleMERCTabs: function (e) {
                var that = this;
                //                hideMessages();

                colorControl(e, "merc-tab", "gmBGLGrey", "gmBGDarkerGrey gmFontWhite");
                var target = $(e.currentTarget).attr("id");
                if ($(e.currentTarget).hasClass("tab-opened")) {
                    $(".merc-content").addClass("gmMediaDisplayNone");
                    $(".merc-tab").removeClass("tab-closed");
                    $(e.currentTarget).removeClass("tab-opened tab-closed");
                    $("#div-merc-sub-content").addClass("gmMediaDisplayNone");
                    $(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-right");
                } else {
                    $(".merc-content").removeClass("gmMediaDisplayNone");
                    $(".merc-tab").removeClass("tab-opened").addClass("tab-closed");
                    $(e.currentTarget).addClass("tab-opened").removeClass("tab-closed");
                    $("#div-merc-sub-content").removeClass("gmMediaDisplayNone");
                    $(".fa-chevron-right").removeClass("fa-chevron-right").addClass("fa-chevron-down");
                }


                if (GM.Global.Activity.Mode == "view") {
                    if (target == "div-merc-program") {
                        $("#div-merc-view-program").removeClass("hide");
                        $("#div-merc-view-surgeons").addClass("hide");
                        $("#div-merc-view-comments").addClass("hide");

                    } else if (target == 'div-merc-surgeons') {
                        $("#div-merc-view-surgeons").removeClass("hide");
                        $("#div-merc-view-program").addClass("hide");
                        $("#div-merc-view-comments").addClass("hide");
                    } else if (target == "div-merc-comments") {
                        $("#div-merc-view-comments").removeClass("hide");
                        $("#div-merc-view-surgeons").addClass("hide");
                        $("#div-merc-view-program").addClass("hide");
                        $(".selectbox-icon").show();
                    }
                } else {
                    if (target == "div-merc-program") {
                        $("#div-merc-edit-program").removeClass("hide");
                        $("#div-merc-edit-surgeons").addClass("hide");
                        $("#div-merc-edit-comments").addClass("hide");
                        $("#merc-pending-surgeons").addClass("hide");

                    } else if (target == 'div-merc-surgeons') {
                        $("#div-merc-edit-surgeons").removeClass("hide");
                        $("#div-merc-edit-program").addClass("hide");
                        $("#div-merc-edit-comments").addClass("hide");
                        $("#merc-pending-surgeons").addClass("hide");
                    } else if (target == 'div-merc-pending-surgeons') {
                        $("#merc-pending-surgeons").removeClass("hide");
                        $("#div-merc-edit-program").addClass("hide");
                        $("#div-merc-edit-comments").addClass("hide");
                        $("#div-merc-edit-surgeons").addClass("hide");
                        if ($("#ul-pending-surgeon").is(":visible") == false) {
                            $("#ul-pending-surgeon-content").addClass("hide");
                            $("#div-no-data").removeClass("hide");
                        } else {
                            $("#ul-pending-surgeon-content").removeClass("hide");
                            if ($("#ul-pending-surgeon li").length > 3) {
                                $("#div-btn-contents").removeClass("hide");
                            } else
                                $("#div-btn-contents").addClass("hide");
                        }
                        var result = _.filter(that.actModel.get("arrgmactivityattrvo"), function (model) {
                            return model.attrtype == 105589;
                        });
                        if (result.length <= 1)
                            $("#li-multiple-select-merc").addClass("hide")
                    }
                }


            },

            toggleTrainingTabs: function (e) {

                var that = this;
                //                hideMessages();

                colorControl(e, "training-tab", "gmBGLGrey", "gmBGDarkerGrey gmFontWhite");
                var target = $(e.currentTarget).attr("id");
                if ($(e.currentTarget).hasClass("tab-opened")) {
                    $(".training-content").addClass("gmMediaDisplayNone");
                    $(".training-tab").removeClass("tab-closed");
                    $(e.currentTarget).removeClass("tab-opened tab-closed");
                    $("#div-training-sub-content").addClass("gmMediaDisplayNone");
                    $(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-right");
                } else {
                    $(".training-content").removeClass("gmMediaDisplayNone");
                    $(".training-tab").removeClass("tab-opened").addClass("tab-closed");
                    $(e.currentTarget).addClass("tab-opened").removeClass("tab-closed");
                    $("#div-training-sub-content").removeClass("gmMediaDisplayNone");
                    $(".fa-chevron-right").removeClass("fa-chevron-right").addClass("fa-chevron-down");
                }


                if (GM.Global.Activity.Mode == "view") {
                    if (target == "div-training-program") {
                        $("#div-training-view-program").removeClass("hide");
                        $("#div-training-view-surgeons").addClass("hide");
                        $("#div-training-view-comments").addClass("hide");

                    } else if (target == 'div-training-surgeons') {
                        $("#div-training-view-surgeons").removeClass("hide");
                        $("#div-training-view-program").addClass("hide");
                        $("#div-training-view-comments").addClass("hide");
                    } else if (target == "div-training-comments") {
                        $("#div-training-view-comments").removeClass("hide");
                        $("#div-training-view-surgeons").addClass("hide");
                        $("#div-training-view-program").addClass("hide");
                        $(".selectbox-icon").show();
                        $("#div-training-view-comments").removeClass("hide");
                    }
                } else {
                    if (target == "div-training-program") {
                        $("#div-training-edit-program").removeClass("hide");
                        $("#div-training-edit-surgeons").addClass("hide");
                        $("#div-training-edit-comments").addClass("hide");

                    } else if (target == 'div-training-surgeons') {
                        $("#div-training-edit-surgeons").removeClass("hide");
                        $("#div-training-edit-program").addClass("hide");
                        $("#div-training-edit-comments").addClass("hide");
                    }
                }
            },

            //proving search option to surgeon name
            searchSurgeon: function () {
                var that = this;
                var fnName = "";
                if (GM.Global.Device)
                    fnName = fnSearchSurgeon;
                var searchOptions = {
                    'el': $("#" + this.module + "-surgeon-search"),
                    'url': URL_WSServer + 'search/rsparty',
                    'inputParameters': {
                        'token': localStorage.getItem('token'),
                        "partytype": "7000"
                    },
                    'placeholder': 'Key in at least 3 characters to search',
                    'keyParameter': 'partynm',
                    'IDParameter': 'id',
                    'NameParameter': 'name',
                    'resultVO': 'gmPartyBasicVO',
                    'fnName': fnName,
                    'minChar': 3,
                    'callback': function (result) {
                        that.curPosTgt = $("#div-physicianvisit-edit-objective").scrollTop();
                        that.setPartyInfo(result, "7000", "105564"); //Attendee
                        $("#ul-salescall-title").show();
                        $("#ul-salescall-surgeon").show();
                        
                    }
                };
                var searchView = new GMVSearch(searchOptions);
                var name = "";

                if (this.module == "lead") {
                    if (this.sData != undefined)
                        name = parseNull(this.sData.salutenm) + " " + this.sData.firstnm + " " + parseNull(this.sData.midinitial) + " " + this.sData.lastnm;
                    else if (this.actModel.get("arrgmactivitypartyvo").length > 0) {
                        name = _.filter(this.actModel.get("arrgmactivitypartyvo"), function (data) {
                            return data.roleid == 105564;
                        })
                        name = name[0].partynm;
                    }

                    if (name != "")
                        $("#" + this.module + "-surgeon-search input").val(name);
                    $("#" + this.module + "-surgeon-search input").attr("validate", "required");
                    $("#" + this.module + "-surgeon-search input").attr("message", "Provide the Surgeon Name");
                }

            },

            setPartyInfo: function (data, partytype, roleid) {
                var that = this;

                $("#" + this.module + "-participants-search").find("input").val("");
                $("#" + this.module + "-hpartytype").val(partytype);
                $("#" + this.module + "-hpartyrole").val(roleid);
                $("#" + this.module + "-hpartynm").val(data.Name);
                $("#" + this.module + "-hpartynm").attr("partyid", data.ID);
                $("#" + this.module + "-hpartytype").attr("partyid", data.ID);
                $("#" + this.module + "-hpartyrole").attr("partyid", data.ID);
                $("#" + this.module + "-hpartyrole").trigger("change");
                $("#" + this.module + "-hpartynm").trigger("change");
                $("#" + this.module + "-hpartytype").trigger("change");


                $("#" + this.module + "-hpartyid").val(data.ID); //Need to replace partyid also for single attendee entery in leads
                $("#" + this.module + "-hpartyid").attr("partyid", data.ID);
                $("#" + this.module + "-hpartyid").trigger("change");
                if (this.module == "lead")
                    this.actModel.set("actnm", data.Name);

                if (data.npid != "") { // Updating  partynpid for the attendee in partyVO
                    $("#" + this.module + "-assigneenm-partynpid").val(data.npid);
                    $("#" + this.module + "-assigneenm-partynpid").attr("partyid", data.ID);
                    $("#" + this.module + "-assigneenm-partynpid").trigger("change");
                }

                if (this.module == "salescall") {
                    var actnm = $(".gmPanelTitle").html();
                    this.partyid = data.ID;
                    if (roleid == "105564") {

                        if (actnm == "") {
                            actnm = this.title + " - " + data.Name;
                        } else {
                            if (this.module !== "salescall")
                                actnm += " / " + data.Name;
                            else
                                actnm = this.title + " - " + data.Name;
                        }
                        $("#" + this.module + "-hactnm").val(actnm);
                        $("#" + this.module + "-hactnm").trigger("change");
                    }
                }
                var assignee = $("#" + this.module + "-assigneenm-repnm").val();

                var partyid = data.ID;
                var partyID = partyid;
                if (this.module == "lead") {
                    if (GM.Global.Device) {
                        surgeonInfo(partyID, function (sData) {
                            //                            console.log("Surgeon Rep Info Offline")
                            //                            console.log(sData);
                            $("#" + that.module + "-surgeon-search input").val(sData.firstnm + (sData.midinitial ? (" " + sData.midinitial) : "") + " " + sData.lastnm);
                            fnGetSurgeonRepDetail(partyID, function (data) {
                                //                                console.log(data);
                                if (data.length) {
                                    data = data[0];
                                    that.getRepInfo(data.repnm, data.repid, data.adnm, data.vpnm, data.reppartyid);
                                } else
                                    that.actModel.set("partyid", "");
                                that.setUpAddr(sData);
                            });


                        });
                    } else {
                        var input = {
                            "token": localStorage.getItem("token"),
                            "partyid": data.ID
                        };
                        fnGetWebServerData("surgeon/info", "gmCRMSurgeonlistVO", input, function (sData) {
                            if (sData != null) {
                                sData = sData[0];
                                $("#" + that.module + "-surgeon-search input").val(sData.firstnm + " " + sData.lastnm);
                                var input = {
                                    "token": localStorage.getItem('token'),
                                    "partyid": partyid
                                };
                                fnGetWebServerData("surgeon/sales", "gmCRMSurgeonlistVO", input, function (data) { // commented for now to put in assignee into partyid of activity table
                                    if (data != null)
                                        that.getRepInfo(data.repnm, data.repid, data.adnm, data.vpnm, data.partyid);
                                    else
                                        that.actModel.set("partyid", "");
                                    that.setUpAddr(sData);
                                });
                            }
                        });
                    }
                } else {
                    if (assignee == "") {
                        var input = {
                            "token": localStorage.getItem('token'),
                            "partyid": data.ID
                        };
                        if (GM.Global.Device) {
                            fnGetSurgeonRepDetail(partyID, function (data) {
                                //                                console.log(data);
                                if (data.length) {
                                    data = data[0];
                                    that.getRepInfo(data.repnm, data.repid, data.adnm, data.vpnm, data.reppartyid);
                                } else
                                    that.actModel.set("partyid", "");
                            });
                        } else {
                            fnGetWebServerData("surgeon/sales", "gmCRMSurgeonlistVO", input, function (data) {
                                if (data != null)
                                    that.getRepInfo(data.repnm, data.repid, data.adnm, data.vpnm, data.partyid);
                                else
                                    that.actModel.set("partyid", "");
                            });
                        }

                    }
                    this.render();
                }

                if (GM.Global.SalesRep) {
                    $("#" + this.module + "-salesrep-search input").val(GM.Global.UserName);
                    $("#" + this.module + "-salesrep-search input").trigger("keyup");
                }


            },

            searchTradeShow: function () {
                var that = this;
                var fnName = "";
                if (GM.Global.Device)
                    fnName = fnSearchTS;
                var searchOptions = {
                    'el': that.$("#" + this.module + "-tradeshow-search"),
                    'url': URL_WSServer + 'crmactivity/info',
                    'inputParameters': {
                        'token': localStorage.getItem('token'),
                        'actcategory': 105270
                    },
                    'keyParameter': 'actnm',
                    'IDParameter': 'actid',
                    'NameParameter': 'actnm',
                    'resultVO': 'gmCRMActivityVO',
                    'fnName': fnName,
                    'minChar': 2,
                    'callback': function (result) {
                        that.getTradeShowInfo(result.ID, result.Name, result.Element);
                    }
                };
                this.searchView4 = new GMVSearch(searchOptions);
                if (this.module == "lead") {
                    _.each(that.actModel.get('arrgmactivityattrvo'), function (data) {
                        if (data.attrtype == 7779)
                            $("#" + that.module + "-tradeshow-search input").val(data.attrnm);
                    })
                }
            },

            getTradeShowInfo: function (ID, Name) {
                var that = this;
                if (this.module == "lead") {
                    $("#htradeshow").val(ID);
                    $("#htradeshow").attr("attrnm", Name);
                    $("#htradeshow").trigger("change");
                    $("#" + this.module + "-tradeshow-search input").val(Name);
                    this.tradeID = ID;
                    this.tradeName = Name;
                    $("#select-leads-source").val(105349).trigger("change");
                } else if (this.module == "tradeshow")
                    window.location.href = "#crm/tradeshow/id/" + ID;

            },

            searchHospital: function () {
                var that = this;
                var fnName = "";
                if (GM.Global.Device)
                    fnName = fnSearchAccnt;
                var searchOptions = {
                    'el': $("#" + this.module + "-hospital-search"),
                    'url': URL_WSServer + 'search/account',
                    'inputParameters': {
                        'token': localStorage.getItem('token')
                    },
                    'keyParameter': 'accnm',
                    'IDParameter': 'accid',
                    'NameParameter': 'accnm',
                    'resultVO': 'gmAccountBasicVO',
                    'fnName': fnName,
                    'minChar': 2,
                    'callback': function (result) {
                        that.getAccntInfo(result.ID, result.Name, result.Element);
                    }
                };
                this.search3 = new GMVSearch(searchOptions);
                var name = "";
                if (this.leadnm == undefined)
                    this.leadnm = "";
                if (this.sData != undefined && this.primeAcc != undefined && this.leadnm == "") { // leadnm signifies that lead hospital change is not eflected back to surgeon hospital info
                    name = this.primeAcc[0];
                } else if (this.actModel.get("arrgmactivityattrvo").length > 0) {
                    name = _.filter(this.actModel.get("arrgmactivityattrvo"), function (data) {
                        return data.attrtype == 7800;
                    });
                    if (name.length > 0) {
                        name = name[0].attrnm;
                        this.leadnm = name;
                    }
                }
                if (this.module == "lead" && name != "")
                    $("#" + this.module + "-hospital-search input").val(name);

                $("#" + this.module + "-hospital-search input").on("keyup", function (e) {
                    if (e.keyCode != undefined && e.keyCode != 13) {
                        $("#lead-hhospital").val("");
                        $("#lead-hhospital").attr("attrnm", "");
                        $("#lead-hhospital").trigger("change");
                    }
                });

            },

            getAccntInfo: function (ID, Name, Element) {
                $("#" + this.module + "-hospital-search input").val(Name);
                $("#lead-hhospital").attr("attrnm", Name);
                $("#lead-hhospital").val(ID);
                $("#lead-hhospital").trigger("change");
                this.leadnm = Name;
            },

            searchRep: function () {
                var that = this;
                var fnName = "";
                if (GM.Global.Device)
                    fnName = fnSearchRep;
                var searchOptions = {
                    'el': $("#" + this.module + "-salesrep-search"),
                    'url': URL_WSServer + 'search/rep',
                    'inputParameters': {
                        'token': localStorage.getItem('token')
                    },
                    'keyParameter': 'repname',
                    'IDParameter': 'repid',
                    'NameParameter': 'repname',
                    'resultVO': 'gmSalesRepQuickVO',
                    'fnName': fnName,
                    'minChar': 2,
                    'callback': function (data) {
                        that.getRepInfo(data.Name, data.ID, data.AD, data.VP, data.partyid);
                    }
                };
                this.searchView5 = new GMVSearch(searchOptions);
            },

            getRepInfo: function (repnm, repid, adnm, vpnm, partyid) {
                this.actModel.set("partyid", partyid);
                this.actModel.set("partynm", repnm);
                //                this.render();
            },

            fileUpload: function () {
                var that = this;
                var frmt = ['doc', 'docx', 'pdf', 'txt'];
                var fileid = "";
                if (this.module == "tradeshow" || this.module == "lead")
                    frmt = ['xlsx', 'xls'];
                if (GM.Global.Fileid)
                    fileid = GM.Global.Fileid;
                else if (this.$(".a-service-desc-agenda-file-name").attr("data-fileid"))
                    fileid = this.$(".a-service-desc-agenda-file-name").attr("data-fileid");
                var actid = this.actid;
                if (this.module == "lead")
                    actid = "105269";

                var fileUploadOptions = {
                    'el': $("#" + this.module + "-file-upload"),
                    'url': "crmtraing/savetraining",
                    'parent': that,
                    'voname': "arrGmOrderVO",
                    'arrAcceptetFormat': frmt,
                    'refid': actid,
                    'fileid': fileid,
                    'filetitle': (this.$(".a-service-desc-agenda-file-name").text().trim() != "") ? this.$(".a-service-desc-agenda-file-name").text() : "Agenda - " + $(".gmPanelTitle ").text(),
                    'refgrp': '107994',
                    'displayEl': this.$(".a-service-desc-agenda-file-name"),
                    'validation': function (file) {
                        that.fileName = file.name;
                        //                        console.log("file" + JSON.stringify(file))
                        if (that.module == "tradeshow" || that.module == "lead") {
                            var format = file.type.toLowerCase();
                            var reader = new FileReader();
                            reader.onload = function (progressEvent) {
                                //                                console.log("result" + JSON.stringify(this.result))
                                var lines = this.result.split('\n');
                                lines = _.filter(lines, function (line) {
                                    return line != "";
                                });
                                //                                console.log("lines" + JSON.stringify(lines))
                                that.prepareData(lines, 1);
                            };
                            reader.readAsText(file);
                        } else
                            $(".data-filetitle").val("Agenda");
                    },
                    callback: function (id) {
                        that.saveFileId = id;
                        // Saving Surgeon Data First to recieve partyid of new surgeons and update exisiting info of globus surgeons
                        if (that.module == "lead" || that.module == "tradeshow")
                            that.saveActivitySurgByCSV(that.csvData[0].data, 0);
                    }
                }
                that.fileUploadView = new GMVCRMFileUpload(fileUploadOptions);
                that.fileUploadView.dispData = {
                    "deletefl": "",
                    "fileid": fileid,
                    "filename": "",
                    "filetitle": "",
                    "refgroup": "107994",
                    "refid": actid,
                    "reftype": "",
                    "reftypenm": "",
                    "type": ""
                };

                if (that.fileName != undefined) {
                    $("#span-fileupload-browse-filenm").html(that.fileName);
                }

                GM.Global.uploadAgendaFile = that.fileUploadView;
            },

            prepareData: function (lines, iValue) {
                var that = this;
                var headers = lines[0].split(",");
                var content = _.filter(lines, function (data, i) {
                    return i != 0;
                });
                if (content.length) {
                    showSuccess("Please wait while the data is processing");
                    $("#btn-fileupload-popup-browse").addClass("hide");
                    $("#li-fileupload-upload").addClass("hide");
                    var obj = {};
                    var currentline = lines[iValue].split(",");
                    var error = false;
                    var finalArray = [];
                    _.each(content, function (ct) {
                        var dataJsn = {};
                        var cnt = ct.split(",");
                        _.each(headers, function (hd, i) {
                            dataJsn[hd] = cnt[i];
                        });
                        finalArray.push(dataJsn);
                    });
                    //                    console.log(finalArray);
                    this.fLength = finalArray.length;
                    this.csvData = finalArray;

                    //                    this.preSurg = [];
                    //                    this.newSurg = [];
                    this.count = 0;
                    this.leadData = [];
                    that.resolveCSVData(finalArray[0], 0);
                } else
                    showNativeAlert("No Data Provided in the File!", "Empty File");

            },

            resolveCSVData: function (data, iVal) {
                var that = this;
                if (iVal < this.fLength) {
                    var input = {
                        "token": localStorage.getItem("token"),
                        "firstname": data.Surgeon_First_Name,
                        "middleinitial": data.Surgeon_Mid_Initial,
                        "lastname": data.Surgeon_Last_Name
                    };
                    fnGetWebServerData("surgeon/info/", "gmCRMSurgeonlistVO", input, function (data) {
                        //                        console.log("Surgeon Data" + JSON.stringify(data));
                        if (data != null) {
                            getSurgeonPartyInfo(that.csvData[iVal].Surgeon_NPID, function (npiData) {
                                if (npiData != null) {
                                    if (npiData.npid != null && npiData.partyid != data.partyid) // check if another surgeon has same npid
                                        that.csvData[iVal].Surgeon_NPID = "";
                                }
                                that.csvData[iVal].new = "N";
                                that.csvData[iVal].partyid = data.partyid;
                                var input = {
                                    "token": localStorage.getItem('token'),
                                    "partyid": data.partyid
                                };
                                fnGetWebServerData("surgeon/sales", "gmCRMSurgeonlistVO", input, function (data) {
                                    //                                    console.log(data);
                                    if (data != null) {
                                        that.csvData[iVal].repid = data.repid;
                                        that.csvData[iVal].reppartyid = data.partyid;
                                        that.csvData[iVal].adnm = data.adnm;
                                        that.csvData[iVal].vpnm = data.vpnm;
                                        that.csvData[iVal].repnm = data.repnm;
                                    } else {
                                        that.csvData[iVal].repid = "";
                                        that.csvData[iVal].reppartyid = "";
                                        that.csvData[iVal].adnm = "";
                                        that.csvData[iVal].vpnm = "";
                                        that.csvData[iVal].repnm = "";
                                    }
                                    that.resolveCSVData(that.csvData[iVal + 1], iVal + 1);
                                }, true);

                            }, true);
                        } else {
                            getSurgeonPartyInfo(that.csvData[iVal].Surgeon_NPID, function (npiData) {
                                //                                console.log(npiData)
                                if (npiData != null)
                                    that.csvData[iVal].Surgeon_NPID = "";
                                that.csvData[iVal].new = "Y";
                                that.csvData[iVal].partyid = "";
                                that.csvData[iVal].repid = "";
                                that.csvData[iVal].reppartyid = "";
                                that.csvData[iVal].adnm = "";
                                that.csvData[iVal].vpnm = "";
                                that.csvData[iVal].repnm = "";
                                that.resolveCSVData(that.csvData[iVal + 1], iVal + 1);
                            });
                        }
                    }, true);
                } else {
                    //                    console.log(this.csvData);
                    _.each(this.csvData, function (data, i) {
                        that.formSurgData(data, {}, i);
                    });
                }
            },

            formSurgData: function (csvData, surgeon, iVal) {
                var that = this;
                this.count++;
                surgeon.arrgmsurgeonsaddressvo = [];
                surgeon.arrgmcrmsurgeonaffiliationvo = [];
                surgeon.arrgmsurgeoncontactsvo = [];
                surgeon.arrgmcrmsurgeonsurgicalvo = [];
                surgeon.arrgmcrmsurgeonactivityvo = [];
                surgeon.arrgmcrmsurgeonattrvo = [];
                surgeon.arrgmcrmfileuploadvo = [];
                surgeon.asstnm = "";
                surgeon.mngrnm = "";
                surgeon.midinitial = csvData.Surgeon_Mid_Initial;
                surgeon.firstnm = csvData.Surgeon_First_Name;
                surgeon.lastnm = csvData.Surgeon_Last_Name;
                surgeon.partynm = parseNull(surgeon.salutenm) + " " + surgeon.firstnm + " " + parseNull(surgeon.midinitial) + " " + surgeon.lastnm;
                surgeon.partyid = csvData.partyid;
                surgeon.npid = csvData.Surgeon_NPID;
                surgeon.voidfl = "";
                surgeon.activefl = "";
                this.csvData[iVal]["data"] = surgeon;
                if (this.count == this.csvData.length) {
                    $(".alert").addClass("hide");
                    $("#li-fileupload-upload").removeClass("hide");
                    $("#btn-fileupload-popup-browse").removeClass("hide");
                }
            },

            createActivityData: function () {

                var that = this;
                if (this.module == "tradeshow") {
                    this.tradeJsn = that.actModel.toJSON();
                    _.each(this.csvData, function (sData, i) {
                        if (that.tradeJsn.arrgmactivitypartyvo[0] != undefined) {
                            that.tradeJsn.arrgmactivitypartyvo = arrayOf(that.tradeJsn.arrgmactivitypartyvo);
                            var temp = _.filter(that.tradeJsn.arrgmactivitypartyvo, function (partyData) {
                                return partyData.roleid == "105564" && partyData.partyid == sData.data.partyid;
                            });
                            that.tradeJsn.arrgmactivitypartyvo = _.filter(that.tradeJsn.arrgmactivitypartyvo, function (partyData) {
                                return partyData.roleid == "105564" && partyData.partyid != sData.data.partyid;
                            });
                            if (temp.length) {
                                _.each(that.tradeJsn.arrgmactivitypartyvo, function (partyData) {
                                    if (partyData.roleid == "105564" && partyData.partyid == sData.data.partyid) {
                                        partyData.partynpid = sData.data.npid;
                                        partyData.partynm = parseNull(sData.data.salutenm) + " " + sData.data.firstnm + " " + parseNull(sData.data.midinitial) + " " + sData.data.lastnm;
                                    }
                                });
                            } else {
                                that.tradeJsn.arrgmactivitypartyvo.push({
                                    "actpartyid": "",
                                    "actid": that.tradeJsn.actid,
                                    "partyid": sData.data.partyid,
                                    "partynpid": sData.data.npid,
                                    "partynm": sData.data.firstnm + (sData.data.midinitial ? (" " + sData.data.midinitial) : "") + " " + sData.data.lastnm,
                                    "partytype": "7000",
                                    "roleid": "105564",
                                    "rolenm": "Attendee",
                                    "document": "",
                                    "statusid": "",
                                    "statusnm": "",
                                    "voidfl": ""
                                });
                            }
                        } else {
                            that.tradeJsn.arrgmactivitypartyvo = [];
                            that.tradeJsn.arrgmactivitypartyvo.push({
                                "actpartyid": "",
                                "actid": that.tradeJsn.actid,
                                "partyid": sData.data.partyid,
                                "partynpid": sData.data.npid,
                                "partynm": sData.data.firstnm + (sData.data.midinitial ? (" " + sData.data.midinitial) : "") + " " + sData.data.lastnm,
                                "partytype": "7000",
                                "roleid": "105564",
                                "rolenm": "Attendee",
                                "document": "",
                                "statusid": "",
                                "statusnm": "",
                                "voidfl": ""
                            });
                        }
                        if (i == (that.fLength - 1)) {
                            if (that.tradeJsn.arrgmcrmaddressvo[0] != undefined)
                                that.tradeJsn.arrgmcrmaddressvo = arrayOf(that.tradeJsn.arrgmcrmaddressvo);
                            else
                                that.tradeJsn.arrgmcrmaddressvo = [];
                            if (that.tradeJsn.arrgmactivityattrvo[0] != undefined)
                                that.tradeJsn.arrgmactivityattrvo = arrayOf(that.tradeJsn.arrgmactivityattrvo);
                            else
                                that.tradeJsn.arrgmactivityattrvo = [];
                            if (that.tradeJsn.arrgmcrmlogvo[0] != undefined)
                                that.tradeJsn.arrgmcrmlogvo = arrayOf(that.tradeJsn.arrgmcrmlogvo);
                            else
                                that.tradeJsn.arrgmcrmlogvo = [];

                            var temp = _.filter(that.tradeJsn.arrgmactivityattrvo, function (attrData) {
                                return attrData.attrtype == 7805;
                            });
                            that.tradeJsn.arrgmactivityattrvo = _.filter(that.tradeJsn.arrgmactivityattrvo, function (attrData) {
                                return attrData.attrtype != 7805;
                            });

                            if (temp.length > 0) {
                                temp[0].attrval = that.saveFileId;
                                that.tradeJsn.arrgmactivityattrvo.push(temp[0]);
                            } else
                                that.tradeJsn.arrgmactivityattrvo.push({
                                    "attrid": "",
                                    "attrtype": 7805,
                                    "attrval": that.saveFileId,
                                    "attrnm": "FileID",
                                    "voidfl": ""
                                });

                            var date = getDateByFormat(that.tradeJsn.actstartdate);
                            if (date && date != "aN/aN/NaN")
                                that.tradeJsn.actstartdate = date;

                            var endDate = getDateByFormat(that.tradeJsn.actenddate);
                            if (endDate && endDate != "aN/aN/NaN")
                                that.tradeJsn.actenddate = endDate;

                            that.tradeJsn.token = GM.Global.Token;
                            that.saveActivityByCSV();
                        }
                    });
                } else if (this.module == "lead") {
                    this.lData = [];
                    _.each(this.csvData, function (sData, i) {
                        var leadJsn = new GMMActivity();
                        leadJsn = leadJsn.toJSON();
                        leadJsn.actcategory = "105269";
                        leadJsn.token = GM.Global.Token;
                        leadJsn.actnm = parseNull(sData.data.salutenm) + " " + sData.data.firstnm + " " + parseNull(sData.data.midinitial) + " " + sData.data.lastnm;
                        leadJsn.arrgmcrmaddressvo = [{
                            "add1": sData.Address,
                            "add2": "",
                            "city": sData.City,
                            "zip": sData.Zip_Code,
                            "stateid": sData.State_ID,
                            "statenm": sData.State,
                            "countryid": sData.Country_ID,
                            "countrynm": sData.Country,
                            "addid": ""
                        }];
                        leadJsn.partyid = sData.reppartyid;
                        leadJsn.partynm = sData.repnm;
                        var d = new Date();
                        var month = d.getMonth() + 1;
                        var day = d.getDate();
                        var output = (month < 10 ? '0' : '') + month + '/' + (day < 10 ? '0' : '') + day + '/' + d.getFullYear();
                        leadJsn.actstartdate = output;
                        leadJsn.actenddate = output;
                        if (sData.Relationship != "") {
                            var relID = "";
                            if (sData.Relationship == "Phone")
                                relID = 105356;
                            else if (sData.Relationship == "Office Visit")
                                relID = 105355;
                            leadJsn.arrgmactivityattrvo.push({
                                "attrid": "",
                                "attrtype": "7781",
                                "attrval": relID,
                                "attrnm": sData.Relationship,
                                "voidfl": ""
                            }); // Relationship
                        }
                        if (sData.Source != "") {
                            var srcID = "",
                                ts = "";
                            if (sData.Source == "Web")
                                srcID = 105345;
                            else if (sData.Source == "Email")
                                srcID = 105346;
                            else if (sData.Source == "Phone")
                                srcID = 105347;
                            else if (sData.Source == "Tradeshow") {
                                srcID = 105349;
                                //                                ts = "Y";
                            }
                            leadJsn.arrgmactivityattrvo.push({
                                "attrid": "",
                                "attrtype": "105580",
                                "attrval": srcID,
                                "attrnm": sData.Source,
                                "voidfl": ""
                            }); // Source
                            //                            if(ts == "Y")
                            //                                leadJsn.arrgmactivityattrvo.push({"attrid": "","attrtype": "7779","attrval": sData.Tradeshow_ID,"attrnm": sData.Tradeshow_ID,"voidfl": ""}); // Tradeshow

                        }
                        leadJsn.arrgmactivitypartyvo.push({
                            "actpartyid": "",
                            "actid": "",
                            "partyid": sData.data.partyid,
                            "partynpid": sData.data.npid,
                            "partynm": parseNull(sData.data.salutenm) + " " + sData.data.firstnm + " " + parseNull(sData.data.midinitial) + " " + sData.data.lastnm,
                            "partytype": "7000",
                            "roleid": "105564",
                            "rolenm": "Assignee",
                            "document": "",
                            "statusid": "",
                            "statusnm": "",
                            "voidfl": ""
                        });
                        that.lData.push(leadJsn);
                        if (i == (that.fLength - 1))
                            that.saveActivityByCSV(that.lData[0], 0);
                    });
                }
            },

            saveActivityByCSV: function (lead, iVal) {
                var that = this;
                if (that.module == "tradeshow") {
                    delete that.tradeJsn.file;
                    fnGetWebServerData("crmactivity/save", undefined, that.tradeJsn, function (data) {
                        if (data != null) {
                            showSuccess("Activity saved successfully");
                            GM.Global.Activity.Mode = "view";
                            Backbone.history.loadUrl("#crm/tradeshow/id/" + that.tradeJsn.actid);
                        } else
                            showError("Activity saved Failed");
                    }, true);
                }
                if (that.module == "lead") {
                    if (iVal < this.fLength) {
                        lead.arrgmactivityattrvo.push({
                            "attrid": "",
                            "attrtype": 7805,
                            "attrval": that.saveFileId,
                            "attrnm": "FileID",
                            "voidfl": ""
                        });
                        fnGetWebServerData("crmactivity/save", undefined, lead, function (data) {
                            that.saveActivityByCSV(that.lData[iVal + 1], iVal + 1);
                        }, true);
                    } else
                        Backbone.history.loadUrl("#crm/lead/id/0");
                }
            },

            saveActivitySurgByCSV: function (sData, iVal) {
                var that = this;
                if (iVal < this.csvData.length) {
                    if (sData.partyid == "") {
                        sData.token = GM.Global.Token;
                        fnGetWebServerData("surgeon/save", undefined, sData, function (data) {
                            that.csvData[iVal]["data"] = data;
                            iVal++;
                            if (iVal < that.csvData.length)
                                that.saveActivitySurgByCSV(that.csvData[iVal].data, iVal);
                            else
                                that.createActivityData();
                        }, true);
                    } else {
                        iVal++;
                        if (iVal < this.csvData.length)
                            that.saveActivitySurgByCSV(that.csvData[iVal].data, iVal);
                        else
                            that.createActivityData();
                    }
                } else
                    that.createActivityData();

            },
            showItemListView: function (controlID, data, itemtemplate, columnheader, sortoptions, template_URL, filterOptions, csvFileName, color, callbackOnReRender) {
                data = arrayOf(data)
                for (var i = 0; i < data.length; i++) {
                    var row = data[i];
                    data[i].search = "";
                    if (this.search && this.search.length) {
                        _.each(this.search, function (srh) {
                            data[i].search += row[srh] + " ";
                        })
                    }
                }
                var items = new GMCItem(data);
                this.itemlistview = new GMVItemList({
                    el: controlID,
                    collection: items,
                    columnheader: columnheader,
                    itemtemplate: itemtemplate,
                    sortoptions: sortoptions,
                    template_URL: template_URL,
                    filterOptions: filterOptions,
                    csvFileName: csvFileName,
                    color: color,
                    callbackOnReRender: callbackOnReRender
                });


            },

            //to get surgeon details using their partyid
//            getSurgeonAddress: function () {
//                var that = this;
//                var token = localStorage.getItem("token");
//                var input = { "token": token, "partyid": this.partyid };
//                fnGetWebServerData("surgeon/address", undefined, input, function (data) { //getting surgeon's address info
//                    if (data != null)
//                        that.pickAddress(data);
//                    else
//                        that.addNewAddress();
//                });
//            },

            // Add project or System to attributeVO from the inputs
            addProjSystem: function (e) {
                var that = this;
                var attrDivCount = this.actModel.get("arrgmactivityattrvo").length;
                attrDivCount++;
                var attrType = $(e.currentTarget).attr("attrtype");
                var attrId = attrDivCount;
                var attrName = $(e.currentTarget).parent().find("input").val();
                var labelVal = $(e.currentTarget).attr("labelnm");
                if (parseNull(attrName) != "") {
                    switch (attrType) {
                        case "105582":
                            attrName = $("#physicianvisit-product-input").val();
                            that.curPosTgt = $("#div-physicianvisit-edit-objective").scrollTop();
                            this.setProductInfo({
                                ID: attrId,
                                Name: attrName
                            });
                            break;
                        case "105583":
                            attrName = $("#physicianvisit-project-input").val();
                            that.curPosTgt = $("#div-physicianvisit-edit-objective").scrollTop();
                            this.setProJectInfo({
                                ID: attrId,
                                Name: attrName
                            });
                            break;
                        case "105586":
                            attrName = $("#physicianvisit-product-input-lab").val();
                            that.curPosTgt = $("#div-physicianvisit-edit-objective").scrollTop();
                            this.setProductInfoLab({
                                ID: attrId,
                                Name: attrName
                            });
                            break;
                    }
                } else
                    showError("Please enter the " + labelVal);
            },

            //proving search option for Products
            searchProduct: function () {
                var that = this;

                var input = {
                    'token': localStorage.getItem('token')
                };
                if (this.module == "physicianvisit")
                    input["stropt"] = "physicianvisit";

                var searchOptions1 = {
                    'el': $("#" + this.module + "-product-search"),
                    'url': URL_WSServer + 'search/system',
                    'inputParameters': input,
                    'keyParameter': 'sysnm',
                    'IDParameter': 'sysid',
                    'NameParameter': 'sysnm',
                    'resultVO': 'gmSystemBasicVO',
                    'minChar': 2,
                    'callback': function (val) {
                        that.curPosTgt = $("#div-physicianvisit-edit-objective").scrollTop();
                        that.setProductInfo(val);
                    }
                };

                var searchView4 = new GMVSearch(searchOptions1);
                $("#" + this.module + "-product-search input").on("keyup", function () {
                    $("#" + this.module + "-product-search").attr("data-pdt", "");
                });
            },


            //proving search option for Products to Use in Lab
            searchProductLab: function () {
                var that = this;
                var input = {
                    'token': localStorage.getItem('token')
                };
                if (this.module == "physicianvisit")
                    input["stropt"] = "physicianvisit";
                if (isRep())
                    input["syslab"] = "syslab";

                var searchOptions1 = {
                    'el': $("#physicianvisit-product-search-lab"),
                    'url': URL_WSServer + 'search/system',
                    'inputParameters': input,
                    'keyParameter': 'sysnm',
                    'IDParameter': 'sysid',
                    'NameParameter': 'sysnm',
                    'resultVO': 'gmSystemBasicVO',
                    'minChar': 2,
                    'callback': function (val) {
                        that.curPosTgt = $("#div-physicianvisit-edit-objective").scrollTop();
                        that.setProductInfoLab(val);
                    }
                };

                var searchView4 = new GMVSearch(searchOptions1);
                $("#physicianvisit-product-search-lab input").on("keyup", function () {
                    $("#physicianvisit-product-search-lab").attr("data-pdt", "");
                });
            },

            setProductInfo: function (data) {
                var that = this;
                $("#" + this.module + "-product-search").find("input").val("");
                $("#" + this.module + "-hproduct").attr("attrtype", 105582);
                $("#" + this.module + "-hproduct").val(data.ID);
                $("#" + this.module + "-hproduct").attr("attrnm", data.Name);

                $("#" + this.module + "-hproduct").trigger("change");
                this.render();
            },

            setProductInfoLab: function (data) {
                var that = this;
                $("#physicianvisit-product-search-lab").find("input").val("");
                $("#physicianvisit-hproduct-lab").attr("attrtype", 105586);
                $("#physicianvisit-hproduct-lab").val(data.ID);
                $("#physicianvisit-hproduct-lab").attr("attrnm", data.Name);

                $("#physicianvisit-hproduct-lab").trigger("change");
                this.render();
            },

            //proving search option for Technique
            searchTechnique: function () {
                var that = this;

                var searchOptions1 = {
                    'el': $("#" + this.module + "-technique-search"),
                    'url': URL_WSServer + 'search/codelookup',
                    'inputParameters': {
                        'token': localStorage.getItem('token'),
                        'codegrp': "TECHQ"
                    },
                    'keyParameter': 'codenm',
                    'IDParameter': 'codeid',
                    'NameParameter': 'codenm',
                    'resultVO': 'gmCodeLookUpVO',
                    'minChar': 2,
                    'callback': function (val) {
                        that.setTechInfo(val);
                    }
                };

                var searchView4 = new GMVSearch(searchOptions1);
                $("#" + this.module + "-technique-search input").on("keyup", function () {
                    $("#" + this.module + "-technique-search").attr("data-pdt", "");
                });
            },

            setTechInfo: function (data) {
                var that = this;
                $("#" + this.module + "-technique-search").find("input").val("");
                $("#" + this.module + "-htechnique").attr("attrtype", "105588");
                $("#" + this.module + "-htechnique").val(data.ID);
                $("#" + this.module + "-htechnique").attr("attrnm", data.Name);

                $("#" + this.module + "-htechnique").trigger("change");
                this.render();
            },

            //proving search option for Project
            searchProject: function () {
                var that = this;

                var searchOptions1 = {
                    'el': $("#" + this.module + "-project-search"),
                    'url': URL_WSServer + 'search/project',
                    'inputParameters': {
                        'token': localStorage.getItem('token')
                    },
                    'keyParameter': 'projectname',
                    'IDParameter': 'projectid',
                    'NameParameter': 'projectname',
                    'resultVO': 'gmProjectBasicVO',
                    'minChar': 2,
                    'callback': function (val) {
                        that.setProJectInfo(val);
                    }
                };

                var searchView = new GMVSearch(searchOptions1);
                $("#" + this.module + "-product-search input").on("keyup", function () {
                    $("#" + this.module + "-product-search").attr("data-pdt", "");
                });
            },


            setProJectInfo: function (data) {
                var that = this;
                $("#" + this.module + "-project-search").find("input").val("");
                $("#" + this.module + "-hproject").attr("attrtype", 105583);
                $("#" + this.module + "-hproject").val(data.ID);
                $("#" + this.module + "-hproject").attr("attrnm", data.Name);

                $("#" + this.module + "-hproject").trigger("change");
                this.render();
            },

            // Search participants from the participants searchbox
            searchParticipant: function () {
                var that = this;
                var searchOptions3 = {
                    'el': $("#" + this.module + "-participants-search"),
                    'url': URL_WSServer + 'search/rsparty',
                    'inputParameters': {
                        'token': localStorage.getItem('token'),
                        "partytype": "7005,7006"
                    },
                    'placeholder': 'Key in at least 3 characters to search',
                    'keyParameter': 'partynm',
                    'IDParameter': 'id',
                    'NameParameter': 'name',
                    'resultVO': 'gmPartyBasicVO',
                    'minChar': 3,
                    'callback': function (result) {
                        that.curPosTgt = $("#div-physicianvisit-edit-objective").scrollTop();
                        that.setPartyInfo(result, "7006", "105561"); // 105561 - Participant
                    }
                };

                var searchView = new GMVSearch(searchOptions3);
            },

            addOtherVisitor: function () {
                var divCount = $(".div-sc-extn #input-physicianvisit-other-visitor").length;
                $("#btn-add-other-visitor-more").append("<div class='div-other-visitor div-sc-extn span-extend'  data-attrval='' data-attrtype='105590' data-attrid=''><label class='gmMarginRight10p'>Name</label><input type='text' id='input-physicianvisit-other-visitor' class='gmInputText requiredInput" + divCount + "' value='' attrnm='' placeholder='Enter visitor name'  attrtype='105590' VO='arrgmactivityattrvo' index=" + divCount + " attrid='' validate='required' message='Please include name for all Other Visitors' maxlength='100'><label class='gmMarginRight10p gmText75 gmAlignHorizontal gmTextRight'>Title</label><input type='text' id='input-physicianvisit-other-visitor-title' class='gmInputText requiredInput" + divCount + "' value='' placeholder='Enter visitor title' attrtype='105590' VO='arrgmactivityattrvo'  updAfl='Y' attrval='' attrnm='' index=" + divCount + " attrid='' validate='required' message='Please include title for all Other Visitors' maxlength='100'><span class='span-remove-extend extend-close' removeFl='Y'> - </span></div>");
            },

            addMoreDoc: function (e) {
                var that = this;
                this.count += 1;
                $("#li-phyvsit-add-more-document").prepend('<div class="gmAlignHorizontal div-phyvsit-more-doc"><li id="physicianvisit-file-upload gmAlignHorizontal agendaupload"></li><i class="fa fa-minus-circle fa-3x gmFontDarkerGrey"></i></div>');
                $("#physicianvisit-file-upload").attr("id", 'physicianvisit-file-upload' + this.count);
                this.fileUpload();
            },

            deleteDoc: function (e) {

                $(e.currentTarget).closest("div").remove();

                var that = this;
                var position = $(e.currentTarget).closest("div");

                if (that.fileUploadView[position] != undefined && that.fileUploadView[position].dispData != undefined && that.fileUploadView[position].dispData.filename != "") {
                    showNativeConfirm("Do you want delete the Document?", "Confirm", ["Yes", "No"], function (index) {
                        if (index == 1) {
                            if (that.fileUploadView[position].dispData.fileid) {
                                that.fileUploadView[position].dispData.deletefl = "Y";
                                var input = that.fileUploadView[position].dispData;
                                input["token"] = localStorage.getItem("token");
                                input['fileid'] = that.fileUploadView[position].dispData.fileid;
                                that.fileUploadView[position].dispData = "";
                                fnGetWebServerData("file/savefile", undefined, input, function (fileData) {
                                    if (fileData != null) {
                                        that.resetSurgeon();
                                    }
                                });
                            } else {
                                that.fileUploadView[position].dispData = "";
                                if (that.fileUploadView[position].formData)
                                    that.fileUploadView[position].formData = undefined;
                                $(e.currentTarget).closest("div").remove();
                            }
                        }
                    });
                }
            },

            //calls web-service to obtain and display crf status
            getCRFStatus: function () {
                var that = this;
                if (GM.Global.Activity.Mode != "create") {
                    var input = {
                        "token": localStorage.getItem("token"),
                        "actid": this.actid
                    };
                    fnGetWebServerData("crf/getstatus", "gmCRMCRFVO", input, function (data) {

                        if (data != undefined) {
                            data = arrayOf(data);
                            _.each(data, function (data) {
                                $(".surgeon-crf-status[data-partyid='" + data.partyid + "']").text(data.statusnm);
                                $(".surgeon-crf-status-mobile[data-partyid='" + data.partyid + "']").text(data.statusnm);
                                $(".surgeon-crf-status[data-partyid='" + data.partyid + "']").addClass("surgeon-crf-status-" + data.statusid);
                                $(".surgeon-crf-status[data-partyid='" + data.partyid + "']").addClass("surgeon-crf-status-" + data.statusid);
                                $(".surgeon-crf-status-mobile[data-partyid='" + data.partyid + "']").addClass("surgeon-crf-status-" + data.statusid);
                                $(".span-merc-surgeon-crf-status[data-partyid='" + data.partyid + "']").addClass("surgeon-crf-status-" + data.statusid);
                            });
                        }
                    });
                }
            },

            backToPage: function () {

                if (GM.Global.Activity.Mode == "edit")
                    Backbone.history.loadUrl(Backbone.history.fragment);
                else
                    window.history.back();
            },
            
            addNewAddress: function () {
                var that = this;
                showAddressPopup();
                $("#div-crm-overlay-content-container3").find('.load-model').addClass("hide");
                $("#div-crm-overlay-content-container3").find('.done-model').addClass("hide");
                $("#div-crm-overlay-content-container3").find('.select-model').addClass("hide");
                $("#div-crm-overlay-content-container3").find('.addnew-model').addClass("hide");
                $("#div-crm-overlay-content-container3").find('.save-model').removeClass("hide");
                //                $("#div-crm-overlay-content-container3").find('.clear-model').removeClass("hide");
                $("#div-crm-overlay-content-container3").find('.modal-title').text("Key in a new address");
                if (this.module == "salescall" && GM.Global.AddNewAddress == "false") {
                    GM.Global.AddNewAddress == "false"
                    $("#div-crm-overlay-content-container3 .modal-body").html(this.template_CreateAddress());
                } else if (this.actModel.toJSON().arrgmcrmaddressvo != "") {
                    $("#div-crm-overlay-content-container3 .modal-body").html(this.template_CreateAddress(this.actModel.toJSON()));
                } else {
                    GM.Global.AddNewAddress == "false"
                    $("#div-crm-overlay-content-container3 .modal-body").html(this.template_CreateAddress());
                }
                if (this.module == "lead") {
                    $("#div-crm-overlay-content-container3").find('.modal-body input').addClass("leadsurg-upd");
                    $("#div-crm-overlay-content-container3").find('.modal-body select').addClass("leadsurg-upd");
                }
                $.get("globusMobileApp/datafiles/state.json", function (data) {
                    if (typeof (data) != "object")
                        data = $.parseJSON(data);
                    var arrSelectOpt = new Array();
                    arrSelectOpt.push({
                        "ID": "",
                        "Name": "Select"
                    });
                    data = arrSelectOpt.concat(_.sortBy(data, function (data) {
                        return data.codenm;
                    }));
                    var strSelectOptions = JSON.stringify(data).replace(/codeid/g, 'ID').replace(/codenm/g, 'Name');
                    that.statenm = $.parseJSON(strSelectOptions);
                    $('.select-address-state').each(function () {
                        fnCreateOptions(this, that.statenm);
                        $(this).val($(this).attr("value"));
                    });

                });


                $.get("globusMobileApp/datafiles/country.json", function (data) {
                    if (typeof (data) != "object")
                        data = $.parseJSON(data);
                    var arrSelectOpt = new Array();
                    arrSelectOpt.push({
                        "ID": "",
                        "Name": "Select"
                    });
                    //   GM.Global.Country = arrSelectOpt.concat(GM.Global.Country);
                    data = arrSelectOpt.concat(_.sortBy(data, function (data) {
                        return data.codenm;
                    }));
                    var strSelectOptions = JSON.stringify(data).replace(/codeid/g, 'ID').replace(/codenm/g, 'Name');
                    that.countrynm = $.parseJSON(strSelectOptions);
                    $('.select-address-country').each(function () {
                        fnCreateOptions(this, that.countrynm);
                        $(this).val($(this).attr("value"));
                    });
                });

                $("#div-crm-overlay-content-container3").find('.modal-body #popup-address').unbind('blur').bind('blur', function (e) {

                    if ($("#popup-address").val() == "")
                        $("#popup-address").addClass("gmValidationError");
                    else
                        $("#popup-address").removeClass("gmValidationError");
                    $("#div-address-validation").html("");
                });
                $("#div-crm-overlay-content-container3").find('.modal-body #popup-city').unbind('blur').bind('blur', function (e) {
                    if ($("#popup-city").val() == "")
                        $("#popup-city").addClass("gmValidationError");
                    else
                        $("#popup-address").removeClass("gmValidationError");
                    $("#div-address-validation").html("");

                });
                $("#div-crm-overlay-content-container3").find('.modal-body #popup-state').unbind('blur').bind('blur', function (e) {
                    if ($("#popup-state").val() == "")
                        $("#popup-state").addClass("gmValidationError");
                    else
                        $("#popup-address").removeClass("gmValidationError");
                    $("#div-address-validation").html("");
                });
                $("#div-crm-overlay-content-container3").find('.modal-body #popup-zipcode').unbind('blur').bind('blur', function (e) {
                    if ($("#popup-zipcode").val() == "")
                        $("#popup-zipcode").addClass("gmValidationError");
                    else if ($("#popup-zipcode").val().trim().length > 10)
                        $("#popup-zipcode").addClass("gmValidationError");
                    else
                        $("#popup-address").removeClass("gmValidationError");
                    $("#div-address-validation").html("");
                });
                $("#div-crm-overlay-content-container3").find('.modal-body #popup-country').unbind('blur').bind('blur', function (e) {
                    if ($("#popup-country").val() == "")
                        $("#popup-country").addClass("gmValidationError");
                    else
                        $("#popup-address").removeClass("gmValidationError");
                    $("#div-address-validation").html("");
                });

                $("#div-crm-overlay-content-container3").find('.save-model').unbind('click').bind('click', function () {

                    that.validation = "false"
                    that.validateAddress();
                    if (that.validation == "true") {
                        var addid = ""
                        var temp = _.filter(that.actModel.get('arrgmcrmaddressvo'), function (data) {
                            return data.addid != "";
                        });
                        if (temp.length)
                            addid = temp[0].addid;
                        var addrData = {
                            "add1": $("#div-crm-overlay-content-container3").find('.modal-body #popup-address').val(),
                            "add2": $("#div-crm-overlay-content-container3").find('.modal-body #popup-address2').val(),
                            "city": $("#div-crm-overlay-content-container3").find('.modal-body #popup-city').val(),
                            "zip": $("#div-crm-overlay-content-container3").find('.modal-body #popup-zipcode').val(),
                            "stateid": $("#div-crm-overlay-content-container3").find('.modal-body #popup-state option:selected').val(),
                            "statenm": $("#div-crm-overlay-content-container3").find('.modal-body #popup-state option:selected').text(),
                            "countryid": $("#div-crm-overlay-content-container3").find('.modal-body #popup-country option:selected').val(),
                            "countrynm": $("#div-crm-overlay-content-container3").find('.modal-body #popup-country option:selected').text(),
                            "addid": addid
                        }

                        that.actModel.set('arrgmcrmaddressvo', []);
                        that.actModel.set("arrgmcrmaddressvo", arrayOf(addrData));
                        $("#div-crm-overlay-content-container3").find('.modal-title').text("");
                        hideAddressPopup();
                        that.render();
                    } else {
                        $("#div-address-validation").html(that.validationText);
                        that.validationText = undefined;
                        that.validation = "false";
                    }
                });
            },

            validateAddress: function (id) {
                var that = this;
                var str = "";
                var numberPattern = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
                if ($("#popup-address").val() == "")
                    str = "Address 1";
                if ($("#popup-city").val() == "")
                    str = (str.trim().length) ? (str + " / " + "City") : "City";
                if ($("#popup-state").val() == "")
                    str = (str.trim().length) ? (str + " / " + "State") : "State";
                if ($("#popup-zipcode").val() == "")
                    str = (str.trim().length) ? (str + " / " + "Zip Code") : "Zip Code";
                if ($("#popup-country").val() == "")
                    str = (str.trim().length) ? (str + " / " + "Country") : "Country";
                if ($("#popup-zipcode").val().trim().length > 10)
                    str = (str.trim().length) ? (str + " / " + "Zip can have maximun of 10 characters") : "Zip can have maximun of 10 characters";

                this.validationText = "Mandatory Fields : " + str;

                if ($("#popup-address").val() != "" && $("#popup-city").val() != "" && $("#popup-state").val() != "" && $("#popup-zipcode").val() != "" && $("#popup-country").val() != "" && ($("#popup-zipcode").val().trim() != "" && numberPattern.test($("#popup-zipcode").val().trim())))
                    that.validation = "true";
            },


            updatePartyVO: function (partyid, attribute, value) {

                var that = this;

                var partyVO = [];
                var partyVOItem;

                if (this.module == "salescall") {
                    var actpartyid = $("#salescall-hactpartyid").val();
                } else
                    var actpartyid = "";

                if (this.module == "lead") {
                    partyVOItem = _.findWhere(this.actModel.get("arrgmactivitypartyvo"), function (data) {
                        return data.roleid == 105564;
                    });
                } else {
                    partyVOItem = _.findWhere(this.actModel.get("arrgmactivitypartyvo"), {
                        "partyid": partyid
                    });
                }

                var obj = _.findWhere(this.actModel.get("arrgmactivitypartyvo"), {
                    "partyid": partyid,
                    "voidfl": "Y"
                });

                if (!partyVOItem && !obj) {
                    partyVOItem = {
                        "actpartyid": actpartyid,
                        "actid": "",
                        "partyid": partyid,
                        "partynpid": "",
                        "partynm": "",
                        "partytype": "",
                        "roleid": "",
                        "rolenm": "",
                        "document": "",
                        "statusid": "",
                        "statusnm": "",
                        "voidfl": ""
                    };
                } else if (obj) {
                    _.each(this.actModel.get("arrgmactivitypartyvo"), function (a) {
                        if (a.partyid == partyid)
                            a.voidfl = "";
                    })
                }
                if (this.module == "lead") {
                    partyVO = partyVOItem;
                    partyVO.partyid = partyid;
                    partyVO[attribute] = value;
                } else {
                    partyVO = _.without(this.actModel.get("arrgmactivitypartyvo"), partyVOItem);
                    partyVOItem[attribute] = value;
                    partyVO.push(partyVOItem);
                }

                this.actModel.get("arrgmactivitypartyvo").length = 0;
                partyVO = arrayOf(partyVO);
                $.each(partyVO, function (index, item) {
                    that.actModel.get("arrgmactivitypartyvo").push(item);
                });


                if (this.module == "salescall" && !GM.Global.ActSurgInfo) {
                    var rmAttr = _.reject(this.actModel.get("arrgmactivitypartyvo"), function (data) {
                        return data.roleid == 105564 && data.partyid != partyid;
                    });
                    this.actModel.set("arrgmactivitypartyvo", rmAttr);
                }
            },

            updateAttrVO: function (attrid, attrtype, attrval, attrnm, updAfl, index) {

                var that = this;
                var attrVO = [];
                var attrVOItem;
                this.actModel.set("arrgmactivityattrvo", arrayOf(this.actModel.get("arrgmactivityattrvo")));

                switch (attrtype) {
                    case "105584": //actn steps
                    case "105581": //rslt
                    case "7779":
                    case "105580":
                    case "7781":
                    case "105585": //notes 
                    case "105541":
                    case "7800":
                    case "7801":
                    case "7802":
                    case "7777":
                    case "26240350": // Cancel comment
                    case "26240352": // Cancel Reason
                        attrVOItem = _.findWhere(this.actModel.get("arrgmactivityattrvo"), {
                            attrtype: attrtype
                        });
                        break;
                    case "105588": // tech
                    case "105582": //pdt
                    case "105583": //projects
                    case "105589": //merc party
                    case "105586": // product lab
                        attrVOItem = _.findWhere(this.actModel.get("arrgmactivityattrvo"), {
                            attrval: attrval,
                            attrtype: attrtype
                        });
                        break;

                    case "105590": // visitors

                        if (attrid != "")
                            attrVOItem = _.findWhere(this.actModel.get("arrgmactivityattrvo"), {
                                attrid: attrid
                            });
                        else
                            attrVOItem = _.findWhere(this.actModel.get("arrgmactivityattrvo"), {
                                index: index
                            });
                        break;
                }

                if (attrVOItem) {
                    attrVO = _.without(this.actModel.get("arrgmactivityattrvo"), attrVOItem);

                    attrVOItem["attrnm"] = attrnm;
                    attrVOItem["attrtype"] = attrtype;

                    if (parseNull(updAfl) == "")
                        attrVOItem["attrval"] = attrval;

                    attrVO.push(attrVOItem);

                    this.actModel.get("arrgmactivityattrvo").length = 0;

                    $.each(attrVO, function (index, item) {
                        that.actModel.get("arrgmactivityattrvo").push(item);
                    });

                } else {
                    this.actModel.get("arrgmactivityattrvo").push({
                        "attrid": "",
                        "attrnm": attrnm,
                        "attrval": attrval,
                        "attrtype": attrtype,
                        "voidfl": "",
                        "index": index
                    });
                }
                var rmAttr = _.reject(this.actModel.get("arrgmactivityattrvo"), function (data) {
                    return data.attrval == "" && data.attrnm == "" && data.attrid == "";
                });
                this.actModel.set("arrgmactivityattrvo", rmAttr);
                $(".div-other-visitor").find('.gmInputText').attr("validate", "required")


            },
            updateLogVO: function (logid, attribute, value) {
                var that = this;
                var logVO = [];
                var logVOItem;
                this.actModel.set("arrgmcrmlogvo", arrayOf(this.actModel.get("arrgmcrmlogvo")));

                logVOItem = _.findWhere(this.actModel.get("arrgmcrmlogvo"), {
                    "logid": logid
                });

                console.log("logVOItem " + JSON.stringify(logVOItem));

                if (!logVOItem) {
                    logVOItem = {
                        "refid": this.actid,
                        "logid": logid,
                        "logtypeid": "",
                        "logtype": "",
                        "logcomment": "",
                        "updatedbyid": localStorage.getItem("userID"),
                        "updatedby": localStorage.getItem("fName") + " " + localStorage.getItem("lName"),
                        "voidfl": ""
                    };
                }

                logVO = _.without(this.actModel.get("arrgmcrmlogvo"), logVOItem);
                logVOItem[attribute] = value;
                logVO.push(logVOItem);
                this.actModel.get("arrgmcrmlogvo").length = 0;
                $.each(logVO, function (index, item) {
                    that.actModel.get("arrgmcrmlogvo").push(item);
                });

                console.log("Updated log VO " + JSON.stringify(this.actModel.get("arrgmcrmlogvo")));
            },

            updateModel: function (e) {
                var that = this;
                if (!$("lead-file-upload").hasClass("hide")) {
                    var leadFile = true;
                    this.actModel.unbind("change");
                }

                if (this.stopRerender) {
                    this.stopRerender = undefined;
                    this.actModel.on('change', this.render, this);
                }

                if (this.module == "tradeshow" && $(e.currentTarget).attr("name") == "actnm")
                    $(".gmPanelTitle").html($(e.currentTarget).val());

                var control;
                if (e.currentTarget.tagName == "SELECT") {
                    control = $(e.currentTarget).find("option:selected");
                } else {
                    control = $(e.currentTarget);
                }

                var attribute = control.attr("name");
                var partyid = control.attr("partyid");
                var VO = control.attr("VO");
                var value = control.val();
                var attrval = control.val();
                var attrtype = control.attr("attrtype");
                var attrnm = control.attr("attrnm");
                var attrid = control.attr("attrid");
                var logid = control.attr("data-logid");
                var updAfl = control.attr("updAfl");
                var index = control.attr("index");

                if (updAfl == "Y") {
                    attrnm = attrval;
                    attrval = control.attr("attrval");
                }

                if ((attribute != undefined) || (value != undefined)) {
                    if (VO == undefined) {
                        this.actModel.set(attribute, value);
                    } else {
                        if (VO == "arrgmactivitypartyvo") {
                            this.updatePartyVO(partyid, attribute, value);
                        }
                        if (VO == "arrgmactivityattrvo") {
                            this.updateAttrVO(attrid, attrtype, attrval, attrnm, updAfl, index);
                        }

                        if (VO == "arrgmcrmlogvo") {

                            this.updateLogVO(logid, attribute, value);

                        }

                        if (VO == "arrgmcrmaddressvo") {
                            if (this.actModel.get(VO)[0] == undefined) {
                                var arrJson = {};
                                arrJson[attribute] = value;
                                this.actModel.get("arrgmcrmaddressvo").push(arrJson);
                            } else {
                                this.actModel.get(VO)[0][attribute] = value;
                            }
                        }
                    }
                }
                if($(window).width() > 767){
                    if (leadFile)
                        this.actModel.on('change', this.render, this);
                }

                this.checkDivVisible();

                if (GM.Global.Activity.Mode == "create") {
                    $("#span-activity-ctrl-num").addClass("hide");
                    $("#activity-controlid-training").removeClass("hide");
                    $("#activity-controlid-merc").removeClass("hide");
                } else {
                    $("#span-activity-ctrl-num").removeClass("hide");
                    $("#activity-controlid-training").addClass("hide");
                    $("#activity-controlid-merc").addClass("hide");
                }
                console.log("Model is >>>>>>>>>>>>>>>>" + JSON.stringify(this.actModel));
                this.selectDate(e);
            },

            saveValidation: function (input) {
                var that = this;
                this.savevalid = "true";

                var message = "";
                if (GM.Global.Activity.Mode == "edit")
                    this.validateControlNo = "true";
                if ((this.module == "training" || this.module == "merc") && this.validateControlNo == "false" && GM.Global.Activity.Mode == "create") {
                    message += "Control Number already exists,Please change \n ";
                    this.savevalid = "false";
                }

                if (this.module == "lead" && that.chkNPI && that.chkNPI != 0) {
                    message += "NPI number already exists \n ";
                    this.savevalid = "false";
                }
                if (this.module == "lead" && $("#select-leads-source").val() == "") {
                    message += "Enter Source \n ";
                    this.savevalid = "false";
                }
                if (this.module == "lead" && $("#lead-surgeon-search #div-keyword-search #txt-search-keyword").val() == "") {
                    message += "Select surgeon to save \n ";
                    this.savevalid = "false";
                }
//                if(this.module == "physicianvisit" && this.restrictFl == 'Y'){
//                    message += "We are not able to process this request. Please select another date or contact vip@globusmedical.com \n ";
//                    this.savevalid = "false";
//                }
                if (this.savevalid == "false") { // Correct way to do validations if done in front end!
                    showError(message);
                }
                
                if(this.actModel.get("actstartdate") == "")
                    $("#span-" + this.module + "-actstartdate").addClass("validateError");
                
                if(this.actModel.get("actenddate") == "")
                    $("#span-" + this.module + "-actenddate").addClass("validateError");
            },
            //saves the sales call to the database
            saveActivity: function (event) {
                this.stopRerender = true;
                this.actModel.unbind("change");
                var that = this;
                var startDate = this.actModel.get("actstartdate");

                if (this.module == "salescall") {
                    this.actModel.set("actenddate", startDate);
                    var scDeviceFl = this.actModel.get("devicefl");
                    if (parseNull(scDeviceFl) == "")
                        this.actModel.set("devicefl", "Y");
                }

                var fromDate = this.actModel.get("actstartdate");
                var toDate = this.actModel.get("actenddate");
                var fromTime = this.actModel.get("actstarttime");
                var toTime = this.actModel.get("actendtime");

                if (this.module == "lead") {
                    this.actModel.unset("technm");
                    this.actModel.unset("prod");
                    this.actModel.unset("notes");
                    this.actModel.unset("relation");
                    this.actModel.unset("phone");
                    this.actModel.unset("email");
                    this.actModel.unset("npi");

                }
                $("#address-validation").val("");
                if (this.actModel.get("arrgmcrmaddressvo").length != 0 && this.module != "physicianvisit") {
                    $("#address-validation").val("Validation is Correct");
                }
//                GM.Global.AddMerc = "false";
                var date = fromDate;
                if (date && date != "aN/aN/NaN")
                    this.actModel.set("actstartdate", date);

                var date2 = toDate;
                if (date2 && date2 != "aN/aN/NaN")
                    this.actModel.set("actenddate", date2);

                if (this.module == "salescall") {
                    this.actModel.set("actenddate", this.actModel.get("actstartdate"));
                }
                if (this.module == "lead" && GM.Global.Activity.Mode == "create") {
                    var d = new Date();
                    var month = d.getMonth() + 1;
                    var day = d.getDate();
                    var output = (month < 10 ? '0' : '') + month + '/' + (day < 10 ? '0' : '') + day + '/' + d.getFullYear();
                    this.actModel.set("actstartdate", output);
                    this.actModel.set("actenddate", output);
                }
                if (this.module == "lead" && GM.Global.Activity.Mode == "edit") {
                    this.actModel.set("actstarttime", "");
                    this.actModel.set("actendtime", "");
                    this.actModel.set("partyid", localStorage.getItem("partyID"));
                }
                if (this.module == "physicianvisit" && GM.Global.Activity.Mode == "edit" && this.actModel.get("actstatus") != "105762"){
                    if(this.actModel.get("acttype") == '26240596')
                        this.actModel.set("actstatus", "105761");
                }
                else if (this.module == "physicianvisit" && GM.Global.Activity.Mode == "create"){
                    if(this.actModel.get("acttype") == '26240596'){
                        this.actModel.set("actstatus", "105761");
                    }
                    else
                        this.actModel.set("actstatus", "105760");
                }
                else if (this.module == "salescall" && GM.Global.Activity.Mode == "create"){
                    this.actModel.set("actstatus", "105761")
                }

                var phyStatus = this.actModel.get("actstatus");
                if (phyStatus == "105760" && parseNull(this.actid) == 0)
                    this.mailStatus = 1; // request

                if (parseNull(this.actid) > 0 && event.currentTarget.id == "btn-main-save") {
                    this.mailStatus = 2; // update
                }

                if (this.module == "merc") {
                    this.selectChecked();
                }
                //                var input = _.clone(this.actModel);    //Not Working so using unbind for render call on chane of actmodel <above>
                var input = this.actModel;



                input.set("token", localStorage.getItem("token"));
                input.set("actcategory", this.actcategory);
                if (input.get('arrgmcrmaddressvo')[0] == null || input.get('arrgmcrmaddressvo')[0] == undefined)
                    input.set('arrgmcrmaddressvo', []);
                if (input.get('arrgmcrmlogvo')[0] == null || input.get('arrgmcrmlogvo')[0] == undefined)
                    input.set('arrgmcrmlogvo', []);
                if (input.get('arrgmactivityattrvo')[0] == null || input.get('arrgmactivityattrvo')[0] == undefined)
                    input.set('arrgmactivityattrvo', []);
                if (input.get('arrgmactivitypartyvo')[0] == null || input.get('arrgmactivitypartyvo')[0] == undefined)
                    input.set('arrgmactivitypartyvo', []);

                if (input.get('arrgmactivitypartyvo')[0] != undefined)
                    _.each(input.get('arrgmactivitypartyvo'), function (data) {
                        delete data.ad;
                        delete data.vp;
                    });
                if (input.get('arrgmactivityattrvo')[0] != undefined)
                    _.each(input.get('arrgmactivityattrvo'), function (data) {
                        delete data.checkedfl;
                    });
                if (this.module == "lead") {
                    var tradeShow = _.findWhere(input.get("arrgmactivityattrvo"), {
                        attrval: "105349"
                    });
                    var tradeShowName = _.findWhere(input.get("arrgmactivityattrvo"), {
                        attrtype: "7779"
                    });
                    if (tradeShow != undefined) {
                        if (tradeShowName == undefined)
                            this.tradeSave = "";
                        else
                            this.tradeSave = "Y";
                    } else
                        this.tradeSave = "Y";
                }
                input.unset("frmdt");
                input.unset("todt");
                input.unset("file");

                console.log("Acitivty Save INPUT" + JSON.stringify(input));
                if (this.sData != undefined)
                    delete this.sData.accnm;
                if (input.get("acttype")=='26240596'){ //If it is Restricted Type empty the Division
                    //   input.set("division","");  //PC-4002
                    $("#divtyp").removeClass("gmValidationError");
                    $("#divtyp").attr("validate","");
                }
                that.saveValidation(input);

                if (this.module == "physicianvisit") {
                    this.rmVisitorVal();

                    if (parseNull(this.actid) == 0 || this.actModel.get("acttype") == '26240596')
                        this.setActTitle();
                }

                 
                
                if (that.gmvValidation.validate()) {
                    if (this.savevalid == "true") {
                        if (GM.Global.Device) { // Saving Activity in local table
                            this.activityLocalTBLSave(input, event);
                        } else {
                            if (this.module == "lead") {
                                if (this.tradeSave != "")
                                    this.leadSave(input);
                                else
                                    showError("Please select Tradeshow");


                            } else {
                                if (this.module == "physicianvisit")
                                    if (input.get('arrgmactivityattrvo')[0] != undefined)
                                        _.each(input.get('arrgmactivityattrvo'), function (data) {
                                            delete data.index;
                                        });
                                fnGetWebServerData("crmactivity/save", undefined, input, function (data) {
                                    console.log("Acitivty Save OUTPUT" + JSON.stringify(data));
                                        var id = data.actid;
                                        GM.Global.Activityid = data.actid;
                                    
                                    if (data != null) {
                                       // var id = data.actid;
                                    //    GM.Global.Activityid = data.actid;
                                        // fnUpdateActivity();
                                        if (data.actcategory == "105266") {
                                            if (GM.Global.Activity.Mode == "create" && that.fileUploadFL == "Y" && that.mfu != undefined) {
                                                var filesCnt = 0;
                                                if (that.mfu.fileData != "") {
                                                    that.mfu.uploadFile(data.actid, function (dt) {
                                                        filesCnt++;

                                                        if (that.mfu.fileData.length == filesCnt) {
                                                            window.location.href = "#crm/" + that.module + "/id/" + id;
                                                            that.sendVIPMail(id, "N", "Y", "Y");
                                                        }
                                                    });
                                                }
                                            } else {
                                                if(data.acttype != '26240596'){
                                                    if (event.currentTarget.id != "btn-main-void" && that.module == "physicianvisit") {
                                                        if (phyStatus == '105761') {
                                                            if (that.mailStatus != 4) {
                                                                showNativeConfirm("Do you want send email notification?", "Confirm", ["Yes", "No"], function (index) {
                                                                    var notifyfl = (index == 1) ? "Y" : "N";
                                                                    that.sendVIPMail(id, notifyfl);
                                                                });
                                                            } else {
                                                                that.sendVIPMail(id, "N", "Y", "Y");
                                                            }
                                                        } else {
                                                            that.sendVIPMail(id, "N");
                                                        }
                                                    }
                                                }

                                                window.location.href = "#crm/" + that.module + "/id/" + id;
                                                Backbone.history.loadUrl(Backbone.history.fragment);
                                            }
                                        }
                                        else if (data.actcategory =="105267" && data.devicefl == "Y") {
                                            that.sendVIPMail(id, "Y", "", "Y");
                                        } else {
                                            if (that.actid == undefined || that.actid == 0)
                                                window.location.href = "#crm/" + that.module + "/id/" + id;
                                            else
                                                Backbone.history.loadUrl(Backbone.history.fragment);
                                        }

                                        if (event.currentTarget.id == "btn-main-void") {
                                            window.location.href = "#crm/" + that.module;
                                        }

                                        if (that.actModel.get("devicefl") == "Y" && that.module == "salescall") {
                                            if (GM.Global.Activity.Mode == "edit") {
                                                that.data.voidfl = "Y"; // remove the exsting event from the device calendar
                                                createEvent(that.data);
                                            }
                                            createEvent(data);
                                        }

                                        if ($(event.currentTarget).parent().hasClass("li-surgeon-crf-doc")) {
                                            window.location.href = "#crf/" + data.actid + "/" + $(event.currentTarget).attr("href").split("/")[2];
                                        }

                                        GM.Global.Activity.Mode = "view";
                                        showSuccess("Activity saved successfully");

                                    } else
                                        showError("Activity saved Failed");

                                });
                            }
                        }
                    }
                } else
                    showError(this.gmvValidation.getErrors());
            },
            
            sendVIPMail: function (actid, notifyfl) {
                var that = this;
                var input = {
                    "token": GM.Global.Token,
                    "content": "",
                    "toaddr": "",
                    "ccaddr": "",
                    "subj": "",
                    "actid": actid,
                    "isevent": "y",
                    "mailstatus": this.mailStatus,
                    "notifyfl": notifyfl,
                    "includesurgeon": parseNull(this.includeSurgeonFl)
                };
                console.log(JSON.stringify(input));
                fnGetWebServerData("crmactivity/mail", undefined, input, function (data) {
                    console.log(JSON.stringify(data));
                    if (data.mstatus == "success") {
                        var msg = (notifyfl == "Y") ? "Invite Sent!" : "Event updated in the calendar";
                        showSuccess(msg);
                    } else
                        showError("Failed sending invitation. Please try again later");
                }, true);
            },


            activityLocalTBLSave: function (actData, event) {
                var that = this;
                actData = actData.toJSON();
                if (this.module == "lead") {
                    var that = this;
                    var surgInpt = _.filter(this.actModel.get("arrgmactivitypartyvo"), function (data) {
                        return data.roleid == 105564;
                    })
                    surgInpt = surgInpt[0];
                    //Tradeshow assigning
                    var tradeshow = _.filter(this.actModel.get("arrgmactivityattrvo"), function (data) {
                        return data.attrtype == 7779 && data.voidfl != 'Y';
                    })
                    if (tradeshow.length > 0) {
                        var actid = tradeshow[0].attrval;
                        activityInfo(actid, function (data) {
                            //                            console.log(data);
                            that.saveTSLead(data, surgInpt, actData);
                        }, "tradeshow");
                    } else {
                        fnSaveActivity(actData, function (finalData, ID) {
                            if (finalData == "success") {
                                GM.Global.Activity.Mode = "view";
                                if (that.actid == undefined || that.actid == 0)
                                    window.location.href = "#crm/" + that.module + "/id/" + ID;
                                else
                                    Backbone.history.loadUrl(Backbone.history.fragment);
                            } else
                                showError("Save Failed")
                        });
                    }
                } else {

                    var tempID = lastOf(GM.Global.Url).split('/');
                    var size = tempID.length; // taking id from url 'coz we use temp ids for create type data in Offline Mode
                    var id = tempID[size - 1];

                    var todayDate = new Date();
                    todayDate.setHours(0, 0, 0, 0);
                    var visitSDate = new Date(actData.actstartdate);
                    visitSDate.setHours(0, 0, 0, 0);
                    if (_.contains(GM.Global.Admin, "physvisitrqst") && todayDate <= visitSDate && actData.actstatus == "105760")
                        actData.pvEdit = "Y";
                    fnSaveActivity(actData, function (data, ID) {
                        if (data == "success") {
                            showSuccess("Activity Saved")
                            GM.Global.Activity.Mode = "view";
                            if (id == 0 || id == "tradeshow")
                                window.location.href = "#crm/" + that.module + "/id/" + ID;
                            else
                                Backbone.history.loadUrl("#crm/" + that.module + "/id/" + ID);
                        } else
                            showError("Save Failed")
                    });
                }
            },

            saveTSLead: function (data, surgInpt, input) {
                var that = this;

                if (data != null) {
                    var tmp = "";
                    if (data.arrgmactivitypartyvo != undefined) {
                        data.arrgmactivitypartyvo = arrayOf(data.arrgmactivitypartyvo);

                        tmp = _.filter(data.arrgmactivitypartyvo, function (partyData) {
                            return partyData.roleid == 105564 && partyData.partyid == surgInpt.partyid;
                        });
                        data.arrgmactivitypartyvo = _.filter(data.arrgmactivitypartyvo, function (partyData) {
                            return partyData.roleid == 105564 && partyData.partyid != surgInpt.partyid;
                        });
                        if (tmp.length)
                            tmp = tmp[0].actpartyid;
                        else
                            tmp = "";
                    } else
                        data.arrgmactivitypartyvo = [];
                    var name = parseNull(surgInpt.salutenm) + " " + surgInpt.firstnm + " " + parseNull(surgInpt.midinitial) + " " + surgInpt.lastnm;
                    data.arrgmactivitypartyvo.push({
                        "actpartyid": tmp,
                        "actid": data.actid,
                        "partyid": surgInpt.partyid,
                        "partynpid": surgInpt.npid,
                        "partynm": name,
                        "partytype": "7000",
                        "roleid": "105564",
                        "rolenm": "Attendee",
                        "document": "",
                        "statusid": "",
                        "statusnm": "",
                        "voidfl": ""
                    });

                    if (data.arrgmcrmaddressvo != undefined)
                        data.arrgmcrmaddressvo = arrayOf(data.arrgmcrmaddressvo);
                    else
                        data.arrgmcrmaddressvo = [];
                    if (data.arrgmactivityattrvo != undefined)
                        data.arrgmactivityattrvo = arrayOf(data.arrgmactivityattrvo);
                    else
                        data.arrgmactivityattrvo = [];
                    if (data.arrgmcrmlogvo != undefined)
                        data.arrgmcrmlogvo = arrayOf(data.arrgmcrmlogvo);
                    else
                        data.arrgmcrmlogvo = [];
                    if (input.actid == undefined || input.actid == 0)
                        input.actid = "";
                    data.token = GM.Global.Token;
                    fnSaveActivity(data, function (tradeData) {
                        if (tradeData == "success") {
                            fnSaveActivity(input, function (finalData, ID) {
                                if (finalData == "success") {
                                    GM.Global.Activity.Mode = "view";
                                    if (that.actid == undefined || that.actid == 0)
                                        window.location.href = "#crm/" + that.module + "/id/" + ID;
                                    else
                                        Backbone.history.loadUrl(Backbone.history.fragment);
                                } else
                                    showError("Save Failed")
                            });
                        }
                    });

                }
            },

            // Save Details before when click CRF 
            saveBeforeCRF: function (event) {
                event.preventDefault();
                if (!this.$(".div-edit").hasClass("hide")) {
                    this.saveActivity(event);
                } else
                    window.location.href = $(event.currentTarget).attr("href");
            },

            //  To delele details from the list
            confirmVoid: function (event) {
                var that = this;
                if (GM.Global.Browser) {
                    this.validateControlNo = "true";
                    this.savevalid = "true";
                    that.actModel.set("voidfl", "Y");
                    that.void = "Y";
                    if (that.module == "lead" || that.module == "tradeshow") {

                        var input = that.actModel.toJSON();

                        var date = getDateByFormat(that.actModel.get("actstartdate"));
                        if (date && date != "aN/aN/NaN")
                            input.actstartdate = date;
                        var date = getDateByFormat(that.actModel.get("actenddate"));
                        if (date && date != "aN/aN/NaN")
                            input.actenddate = date;

                        if (that.module == "lead") {
                            delete input.technm;
                            delete input.prod;
                            delete input.notes;
                            delete input.relation;
                            delete input.phone;
                            delete input.email;
                            delete input.npi;

                        }
                        input["token"] = GM.Global.Token;
                        if (!GM.Global.Device) {
                            fnGetWebServerData("crmactivity/save", undefined, input, function (data) {
                                if (data != null) {
                                    //                                    console.log(data);
                                    GM.Global.Activity.Mode = undefined;
                                    showSuccess("Activity Voided");
                                    window.location.href = "#crm/" + that.module;
                                } else
                                    showError("Activity Void Failed");
                            });
                        } else {
                            fnSaveActivity(input, function (data, ID) {
                                if (data == "success") {
                                    showSuccess("Activity Voided ")
                                    GM.Global.Activity.Mode = undefined;
                                    window.location.href = "#crm/" + that.module;
                                } else
                                    showError("Acitivity Void Failed")
                            });
                        }
                    } else {
                        that.updateModel(event);
                        that.saveActivity(event);
                    }
                } else
                    showNativeConfirm("Do you want to void this details?", "Confirm", ["Yes", "No"], function (index) {
                        //                        console.log(index)
                        if (index == 1) {
                            GM.Global.ValidateControlNo = "true";
                            GM.Global.SaveValidation = "true";
                            that.actModel.set("voidfl", "Y");
                            that.void = "Y";
                            if (that.module == "lead" || that.module == "tradeshow") {

                                var input = that.actModel.toJSON();

                                var date = getDateByFormat(that.actModel.get("actstartdate"));
                                if (date && date != "aN/aN/NaN")
                                    input.actstartdate = date;
                                var date = getDateByFormat(that.actModel.get("actenddate"));
                                if (date && date != "aN/aN/NaN")
                                    input.actenddate = date;

                                if (that.module == "lead") {
                                    delete input.technm;
                                    delete input.prod;
                                    delete input.notes;
                                    delete input.relation;
                                    delete input.phone;
                                    delete input.email;
                                    delete input.npi;

                                }
                                input["token"] = GM.Global.Token;
                                if (!GM.Global.Device) {
                                    fnGetWebServerData("crmactivity/save", undefined, input, function (data) {
                                        if (data != null) {
                                            //                                            console.log(data);
                                            GM.Global.Activity.Mode = undefined;
                                            showSuccess("Activity Voided");
                                            window.location.href = "#crm/" + that.module;
                                        } else
                                            showError("Activity Void Failed");
                                    });
                                } else {
                                    fnSaveActivity(input, function (data, ID) {
                                        if (data == "success") {
                                            showSuccess("Activity Voided ")
                                            GM.Global.Activity.Mode = undefined;
                                            window.location.href = "#crm/" + that.module;
                                        } else
                                            showError("Acitivity Void Failed")
                                    });
                                }
                            } else {
                                that.updateModel(event);
                                that.saveActivity(event);
                            }
                        } else
                            that.void = "";
                    });


            },

            //validation for npid
            checkNumber: function (e) {
                if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
                    $(e.currentTarget).val("");
                }
            },
                     
            // New Method to allow only the valid characters like Numbers, + -,()
            checkPhoneNumber: function(e){  
                if((e.which == 32 || e.which == 40 || e.which == 41 || e.which == 43 || e.which == 45)||((e.which > 47) && (e.which < 58))){
                        return true;
                }
                else{
                    e.preventDefault();
                }
            },

            //To Add/ edit the popup address
            createSurgeon: function () {
                var that = this;
                var newSurgeonValidation = "true";
                if (!GM.Global.Device) {
                    showPopup();
                    $("#div-crm-overlay-content-container").find('.load-model').addClass("hide");
                    $("#div-crm-overlay-content-container").find('.done-model').addClass("hide");
                    $("#div-crm-overlay-content-container").find('.save-model').removeClass("hide");
                    $("#div-crm-overlay-content-container").find('.modal-title').text("Key in a new Surgeon");
                    $("#div-crm-overlay-content-container .modal-body").html(this.template_CreateSurgeon());

                    //getSelectState();
                    getCLValToDiv(".select-address-state", "", "", "STATE", "", "", "codeid", "", false, "Y", function (divID, attrVal) {});
                    
                    //getSelectCountry();
                    getCLValToDiv(".select-address-country", "", "", "CNTY", "", "", "codeid", "", false, "Y", function (divID, attrVal) {});
                    
                    var userData = JSON.parse(localStorage.getItem("userData"));
                    //Input for fetching company List for company dropdown
                    var input = {
                        'token': localStorage.getItem('token'),
                        'companyid': '',
                        'partyid': localStorage.getItem('partyID')
                    };
                    var defaultid = userData.cmpid;
                    var elemAdmin = "#surg-company-dropdown";

                    companyDropdown(input,defaultid,"",DropDownView,elemAdmin,function(selectedId){
                        that.companyid = selectedId;
                    });
                    
                    $("#div-crm-overlay-content-container .close").on('click', function () {
                        $("#div-crm-overlay-content-container").find(".modal-body").empty();
                    });
                    
                    $("#div-crm-overlay-content-container").find('#input-new-surgeon-npid').unbind('blur').bind('blur', function (e) {
                        that.newSurgeon = "true";
                        that.checkNpid(e);
                    });
                    $("#div-crm-overlay-content-container").find('.input-number-validation').unbind('keyup').bind('keyup', function (e) {
                        that.checkNumber(e);
                    });
                    $("#div-crm-overlay-content-container").find('.input-phonenumber-validation').unbind('keypress').bind('keypress', function (e) {
                    that.checkPhoneNumber(e);
                         
                    });
                    $("#div-crm-overlay-content-container").find('.save-model').unbind('click').bind('click', function () {
                        if ($("#img-npid-spinner").hasClass('hide') && that.chkNPI == 0) { // Denying save when NPID is validating

                            var str = "";
                            var emailPattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
                            var numberPattern = new RegExp(/^[0-9- +()]*$/);
                            var newSurgNPI = $("#input-new-surgeon-npid").val();

                            if ($("#input-new-surgeon-firstnm").val().trim() == "")
                                str = "Firstname";
                            if ($("#input-new-surgeon-lastnm").val().trim() == "")
                                str = (str.trim().length) ? (str + " / " + "Lastname") : "Lastname";
                            if (that.companyid == "undefined" || that.companyid == "0")
                                str = (str.trim().length) ? (str + " / " + "Company") : "Company";
                            if (newSurgNPI.trim() != "" && newSurgNPI.length != 10)
                                str = (str.trim().length) ? (str + " / " + "NPI should contain numeric characters only and should be 10-digit number") : "NPI should contain numeric characters only and should be 10-digit number";
                            if ($("#input-new-surgeon-email").val().trim() != "" && !emailPattern.test($("#input-new-surgeon-email").val().trim()))
                                str = (str.trim().length) ? (str + " / " + "Wrong Email Fomat") : "Wrong Email Fomat";
                            var newSurgPhNo = $("#input-new-surgeon-phone").val();
                            if (newSurgPhNo.trim() != "" && !numberPattern.test($("#input-new-surgeon-phone").val().trim()) &&  newSurgPhNo.length > 25)
                                str = (str.trim().length) ? (str + " / " + "Please enter Phone Number and should be 10-digit number") : "Please enter Phone Number and should be 10-digit number";
                            var addr = "";
                            if ($("#input-new-surgeon-add1").val().trim() != "" || $("#input-new-surgeon-city").val() != "" || $("#div-new-surgeon-content .select-address-state").val().trim() != "" || $("#input-new-surgeon-zip").val().trim() != "" || $("#div-new-surgeon-content .select-address-country").val().trim() != "") {
                                var confirm = true;
                                if ($("#input-new-surgeon-add1").val().trim() == "")
                                    addr = (addr.trim().length) ? (addr + " / " + "Address1") : "Address1";
                                if ($("#input-new-surgeon-city").val().trim() == "")
                                    addr = (addr.trim().length) ? (addr + " / " + "City") : "City";
                                if($("#div-new-surgeon-content .select-address-country").val().trim() == '1101'){
                            if ($("#div-new-surgeon-content .select-address-state").val().trim() == "")
                                addr = (addr.trim().length) ? (addr + " / " + "State") : "State";
                            if ($("#input-new-surgeon-zip").val().trim() == "")
                                addr = (addr.trim().length) ? (addr + " / " + "Zip Code") : "Zip Code";
                            if ($("#input-new-surgeon-zip").val().trim() != "" && $("#input-new-surgeon-zip").val().trim().length > 10)
                                addr = (addr.trim().length) ? (addr + " / " + "Zip can have maximun of 10 characters") : "Zip can have maximun of 10 characters";
                            }
                                if ($("#div-new-surgeon-content .select-address-country").val().trim() == "")
                                    addr = (addr.trim().length) ? (addr + " / " + "Country") : "Country";
                            }
                            if (str.length)
                                str = (addr.length && addr.length) ? (str + " / " + addr) : str;
                            else if (addr.length)
                                str = addr;
                            var surgstr = " Mandatory Fields: " + str;
                            var firstName = $("#input-new-surgeon-firstnm").val().trim();
                            var lastName = $("#input-new-surgeon-lastnm").val().trim();
                            var midName = $("#input-new-surgeon-midinitial").val().trim();
                            if (firstName != "" && lastName != "") {
                                var input = {
                                    "token": localStorage.getItem("token"),
                                    "firstname": firstName,
                                    "lastname": lastName,
                                    "middleinitial": midName
                                };
                                fnGetWebServerData("surgeon/info/", "gmCRMSurgeonlistVO", input, function (data) {
                                    //                                    console.log("Surgeon Data >>>>>>>>" + JSON.stringify(data));
                                    if (data != undefined && data.length > 0)
                                        $("#popup-create-surgeon-validation").text(firstName + " " + lastName + (midName ? (" " + midName) : "") + "'s information already exists");
                                    else if (str.length)
                                        $("#popup-create-surgeon-validation").text(surgstr);
                                    else {
                                        var surgeonInput = {
                                            "token": localStorage.getItem("token"),
                                            "partyid": "",
                                            "npid": newSurgNPI,
                                            "firstnm": $("#input-new-surgeon-firstnm").val(),
                                            "lastnm": $("#input-new-surgeon-lastnm").val(),
                                            "partynm": "",
                                            "midinitial": $("#input-new-surgeon-midinitial").val(),
                                            "asstnm": "",
                                            "mngrnm": "",
                                            "companyid": that.companyid,

                                            "arrgmsurgeonsaddressvo": [],
                                            "arrgmsurgeoncontactsvo": [],
                                            "arrgmcrmsurgeonaffiliationvo": [],
                                            "arrgmcrmsurgeonsurgicalvo": [],
                                            "arrgmcrmsurgeonactivityvo": [],
                                            "arrgmcrmsurgeonattrvo": [],
                                            "arrgmcrmfileuploadvo": []
                                        };
                                        if (isRep()) {
                                            surgeonInput.arrgmcrmsurgeonattrvo.push({
                                                "attrtype": 11509,
                                                "attrvalue": localStorage.getItem("partyID"),
                                                "attrnm": localStorage.getItem("fName") + " " + localStorage.getItem("lName"),
                                                "attrid": "",
                                                "voidfl": ""
                                            });
                                        }

                                        if (addr == "" && confirm == true) {
                                            surgeonInput.arrgmsurgeonsaddressvo.push({
                                                "addrtyp": "90400",
                                                "addid": "",
                                                "add1": $("#input-new-surgeon-add1").val(),
                                                "add2": $("#input-new-surgeon-add2").val(),
                                                "city": $("#input-new-surgeon-city").val(),
                                                "statenm": $(".select-address-state option:selected").text(),
                                                "stateid": $(".select-address-state option:selected").val(),
                                                "zip": $("#input-new-surgeon-zip").val(),
                                                "countrynm": $(".select-address-country option:selected").text(),
                                                "countryid": $(".select-address-country option:selected").val(),
                                                "primaryfl": "Y"
                                            });
                                        }
                                    if(newSurgPhNo != ""){
                                        surgeonInput.arrgmsurgeoncontactsvo.push({
                                                "contype": "Home",
                                                "conmode": "90450",
                                                "convalue": newSurgPhNo,
                                                "conid": "",
                                                "primaryfl": "Y",
                                                "voidfl": ""
                                        });
                                    }
                                    if($("#input-new-surgeon-email").val() != ""){
                                        surgeonInput.arrgmsurgeoncontactsvo.push({
                                                "contype": "Home",
                                                "conmode": "90452",
                                                "convalue": $("#input-new-surgeon-email").val(),
                                                "conid": "",
                                                "primaryfl": "Y",
                                                "voidfl": ""
                                        });
                                    }
                                        hidePopup();
                                        //                                        console.log(JSON.stringify(surgeonInput));
                                        fnGetWebServerData("surgeon/save", undefined, surgeonInput, function (data) {
                                            if (data != null) {
                                                data.Name = parseNull(data.salutenm) + " " + data.firstnm + " " + parseNull(data.midinitial) + " " + data.lastnm;
                                                data.ID = data.partyid;
                                                showSuccess("Surgeon Detail created successfully");
                                                if (that.actModel.get("arrgmactivityattrvo")[0] != undefined) {
                                                    _.each(that.actModel.get("arrgmactivityattrvo"), function (data) {
                                                        if (data.attrtype == 7800)
                                                            data.attrval = "";
                                                    })
                                                    _.each(that.actModel.get("arrgmactivitypartyvo"), function (pData) {
                                                        if (pData.roleid == 105564)
                                                            pData.partynpid = "";
                                                    })
                                                }
                                                that.setPartyInfo(data, "7000", "105564");
                                            } else
                                                showError("Save Failed");
                                        });

                                        $("#div-crm-overlay-content-container").find('.modal-title').text("");
                                        $("#div-crm-overlay-content-container").find(".modal-body").empty();
                                    }
                                });
                            } else
                                $("#popup-create-surgeon-validation").text(surgstr);
                        } else if (that.chkNPI != 0)
                            $("#popup-create-surgeon-validation").text("Enter valid NPID");
                    });
                }
            },


            setUpAddr: function (data) {
                var that = this;
                if (data.arrgmsurgeonsaddressvo != undefined)
                    data.arrgmsurgeonsaddressvo = arrayOf(data.arrgmsurgeonsaddressvo);
                else
                    data.arrgmsurgeonsaddressvo = [];
                if (data.arrgmcrmsurgeonaffiliationvo != undefined)
                    data.arrgmcrmsurgeonaffiliationvo = arrayOf(data.arrgmcrmsurgeonaffiliationvo);
                else
                    data.arrgmcrmsurgeonaffiliationvo = [];
                if (data.arrgmsurgeoncontactsvo != undefined)
                    data.arrgmsurgeoncontactsvo = arrayOf(data.arrgmsurgeoncontactsvo);
                else
                    data.arrgmsurgeoncontactsvo = [];
                if (data.arrgmcrmsurgeonsurgicalvo != undefined)
                    data.arrgmcrmsurgeonsurgicalvo = arrayOf(data.arrgmcrmsurgeonsurgicalvo);
                else
                    data.arrgmcrmsurgeonsurgicalvo = [];
                if (data.arrgmcrmsurgeonactivityvo != undefined)
                    data.arrgmcrmsurgeonactivityvo = arrayOf(data.arrgmcrmsurgeonactivityvo);
                else
                    data.arrgmcrmsurgeonactivityvo = [];
                if (data.arrgmcrmsurgeonattrvo != undefined)
                    data.arrgmcrmsurgeonattrvo = arrayOf(data.arrgmcrmsurgeonattrvo);
                else
                    data.arrgmcrmsurgeonattrvo = [];
                if (data.arrgmcrmfileuploadvo != undefined)
                    data.arrgmcrmfileuploadvo = arrayOf(data.arrgmcrmfileuploadvo);
                else
                    data.arrgmcrmfileuploadvo = [];

                this.sData = data;
                if (this.sData.arrgmsurgeonsaddressvo.length) {
                    var address = _.filter(this.sData.arrgmsurgeonsaddressvo, function (data, i) {
                        return (data.primaryfl == "Y" || i == 0);
                    });
                    if (that.actModel.get("arrgmcrmaddressvo").length)
                        address[0].addid = that.actModel.get("arrgmcrmaddressvo")[0].addid;

                    that.actModel.set("arrgmcrmaddressvo", address);
                } else
                    that.actModel.set("arrgmcrmaddressvo", []);

                if (this.sData.arrgmsurgeoncontactsvo.length) {
                    _.each(this.sData.arrgmsurgeoncontactsvo, function (data) {
                        if (data.contype == "" && data.conmode == 90450 && data.primaryfl == "Y") {
                            $("#lead-hphone").val(data.convalue);
                            $("#lead-hphone").trigger("change");
                        }
                        if (data.contype == "" && data.conmode == 90452 && data.primaryfl == "Y") {
                            $("#lead-hemail").val(data.convalue);
                            $("#lead-hemail").trigger("change");
                        }
                    });
                }

                this.updateLeadSurg();

            },

            setDefaultTradeshow: function (e) {
                var exist = localStorage.getItem("DftTradeshow");
                if (exist != null && exist != undefined) { // Preset already
                    $(e.currentTarget).html("Set as Default");
                    localStorage.removeItem("DftTradeshow");
                } else if ($("#lead-tradeshow-search input").val().trim() != "" && $("#select-leads-source option:selected").text() == "Tradeshow" && this.tradeID != "" && this.tradeID != undefined) {
                    $(e.currentTarget).html("By Default");
                    localStorage.setItem("DftTradeshow", JSON.stringify({
                        "ID": this.tradeID,
                        "Name": this.tradeName
                    }));
                }
            },

            chkSelect: function (e) {
                this.curPosTgt = $("#div-physicianvisit-edit-objective").scrollTop();
                if ($(e.currentTarget).attr("id") == "select-leads-source") {
                    var sourceid = $("#select-leads-source option:selected").val();
                    var sourcenm = $("#select-leads-source option:selected").text();


                    if (sourceid != 105349) {
                        this.updateModel(e);
                        this.shuffleTrade = [];

                        var val = _.filter(this.actModel.get("arrgmactivityattrvo"), function (data) {
                            return data.attrtype == 7779;
                        });

                        if (val.length > 0) { // Removing the tradeshow object that slips out of the actModel when unselected
                            if (val[0].attrid != "") {
                                _.each(this.actModel.get("arrgmactivityattrvo"), function (data) {
                                    if (data.attrtype == 7779)
                                        data.voidfl = "Y";
                                });
                                this.shuffleTrade.push($("#lead-tradeshow-search input").val());
                            } else {
                                var trade = _.filter(this.actModel.get("arrgmactivityattrvo"), function (data) {
                                    return data.attrtype != 7779;
                                });
                                this.actModel.set("arrgmactivityattrvo", trade);
                            }

                        }
                        $("#lead-tradeshow-search input").val("");
                        $("#ul-lead-tradeshow").addClass("hide");
                    } else {
                        _.each(this.actModel.get("arrgmactivityattrvo"), function (data) {
                            if (data.attrtype == 7779 && data.voidfl == "Y")
                                data.voidfl = "";
                        });
                        if (this.shuffleTrade.length > 0)
                            $("#lead-tradeshow-search input").val(this.shuffleTrade[0]);
                        $("#ul-lead-tradeshow").removeClass("hide");
                        this.updateModel(e);
                    }
                } else
                    this.updateModel(e);
            },

            cancelAttribute: function (event) {
                var that = this;
                var attrid = $(event.currentTarget).parent('.div-sc-extn').attr("data-attrid");
                var attrtype = $(event.currentTarget).parent('.div-sc-extn').attr("data-attrtype");
                var attrval = $(event.currentTarget).parent('.div-sc-extn').attr("data-attrval");
                var removeFl = $(event.currentTarget).attr("removeFl");
                var indexVal = $(event.currentTarget).parent().find('input').attr("index");


                this.renderCount = 0;
                $(".div-other-visitor").each(function () {
                    var visitorNm = $(this).find("#input-physicianvisit-other-visitor").val();
                    var visitorTitle = $(this).find("#input-physicianvisit-other-visitor-title").val();
                    if (parseNull(visitorNm) == "" && parseNull(visitorTitle) == "") {
                        that.renderCount++;
                    }
                });
                if (attrid != "") {
                    _.each(this.actModel.get("arrgmactivityattrvo"), function (data) {
                        if (data.attrid == attrid)
                            data.voidfl = "Y";
                    });
                } else {
                    var voidAttr = _.reject(this.actModel.get("arrgmactivityattrvo"), function (data) {
                        if (parseNull(indexVal) != "") {
                            return data.attrtype == attrtype && data.index == indexVal;
                        } else {
                            return data.attrtype == attrtype && data.attrval == attrval;
                        }
                    });
                    this.actModel.set("arrgmactivityattrvo", voidAttr);
                }
                this.curPosTgt = $("#div-physicianvisit-edit-objective").scrollTop();
                if (removeFl == "Y")
                    $(event.currentTarget).parent('.div-sc-extn').remove();
                else
                    this.render();
            },

            createActivity: function () {
                var that = this;
                GM.Global.Activity.Mode = "create";
                this.renderCount = 0;
                this.render();
            },

            removeParticipants: function (event) {
                var partyid = $(event.currentTarget).parent().attr("data-partyid");
                var roleid = $(event.currentTarget).parent().attr("data-roleid");
                var actpartyid = $(event.currentTarget).parent().attr("data-actpartyid");

                if (parseNull(actpartyid) != "") {
                    _.each(this.actModel.get("arrgmactivitypartyvo"), function (data) {
                        if (data.actpartyid == actpartyid)
                            data.voidfl = "Y";
                    });
                } else {
                    var voidParty = _.reject(this.actModel.get("arrgmactivitypartyvo"), function (data) {
                        return data.partyid == partyid && data.roleid == roleid;
                    });
                    this.actModel.set("arrgmactivitypartyvo", voidParty);
                }
                this.curPosTgt = $("#div-physicianvisit-edit-objective").scrollTop();
                this.render();
            },

            // Delelte the surgeon from the list of participants 
            deleteSurgeon: function (event) {
                var that = this;
                var token = localStorage.getItem("token");
                var actpartyid = $(event.currentTarget).parent().find("li").attr("data-actpartyid");
                var partyid = $(event.currentTarget).parent().find("li").attr("data-partyid");
                var partynm = $(event.currentTarget).parent().find("li").attr("data-partynm");
                var actpartyid = $(event.currentTarget).parent().find("li").attr("data-actpartyid");
                var roleid = $(event.currentTarget).parent().find("div").attr("data-roleid");
                var statusid = $(event.currentTarget).parent().find("div").attr("data-statusid");

                this.voidThis = false;
                if (actpartyid == "") {
                    var voidParty = _.filter(this.actModel.get("arrgmactivitypartyvo"), function (data) {
                        return data.partyid != partyid && data.roleid == roleid;
                    });
                    this.actModel.set("arrgmactivitypartyvo", voidParty);
                } else {

                    _.each(this.actModel.get("arrgmactivitypartyvo"), function (data) {
                        if (data.actpartyid == actpartyid)
                            data.voidfl = "Y";
                    });
                    showNativeConfirm("Do you want to delete this Surgeon?", "Confirm", ["Yes", "No"], function (index) {
                        if (index == 1) {
                            that.arrgmactivitypartyvo = {
                                "token": token,
                                "actpartyid": actpartyid,
                                "actid": that.actid,
                                "partyid": partyid,
                                "partynm": partynm,
                                "roleid": roleid,
                                "rolenm": "",
                                "stateid": statusid,
                                "statusnm": "",
                                "voidfl": "Y"
                            };
                            var element = event.currentTarget.parentNode;
                            element.remove();

                            fnGetWebServerData("crmactivity/addparty", undefined, that.arrgmactivitypartyvo, function (data) {
                                showSuccess("Surgeon list is removed / updated");
                                that.render();
                            });
                        }
                    });

                }
            },

            leadSave: function (input) {
                var that = this;
                var surgInpt = _.filter(this.actModel.get("arrgmactivitypartyvo"), function (data) {
                    return data.roleid == 105564;
                })
                surgInpt = surgInpt[0];
                //Tradeshow assigning
                var tradeshow = _.filter(this.actModel.get("arrgmactivityattrvo"), function (data) {
                    return data.attrtype == 7779;
                })
                if (tradeshow.length > 0) {
                    var ipt = {
                        "token": localStorage.getItem('token'),
                        "actid": tradeshow[0].attrval,
                        "actcategory": 105270
                    }
                    fnGetWebServerData("crmactivity/info", "gmCRMActivityVO", ipt, function (data) {

                        if (data != null) {
                            var tmp = "";
                            if (data.arrgmactivitypartyvo != undefined) {
                                data.arrgmactivitypartyvo = arrayOf(data.arrgmactivitypartyvo);

                                tmp = _.filter(data.arrgmactivitypartyvo, function (partyData) {
                                    return partyData.roleid == 105564 && partyData.partyid == surgInpt.partyid;
                                });
                                data.arrgmactivitypartyvo = _.filter(data.arrgmactivitypartyvo, function (partyData) {
                                    return partyData.roleid == 105564 && partyData.partyid != surgInpt.partyid;
                                });
                                if (tmp.length)
                                    tmp = tmp[0].actpartyid;
                                else
                                    tmp = "";

                            } else
                                data.arrgmactivitypartyvo = [];

                            var name = parseNull(surgInpt.salutenm) + " " + surgInpt.firstnm + " " + parseNull(surgInpt.midinitial) + " " + surgInpt.lastnm;
                            data.arrgmactivitypartyvo.push({
                                "actpartyid": tmp,
                                "actid": tradeshow[0].attrval,
                                "partyid": surgInpt.partyid,
                                "partynpid": surgInpt.npid,
                                "partynm": name,
                                "partytype": "7000",
                                "roleid": "105564",
                                "rolenm": "Attendee",
                                "document": "",
                                "statusid": "",
                                "statusnm": "",
                                "voidfl": ""
                            });

                            if (data.arrgmcrmaddressvo != undefined)
                                data.arrgmcrmaddressvo = arrayOf(data.arrgmcrmaddressvo);
                            else
                                data.arrgmcrmaddressvo = [];
                            if (data.arrgmactivityattrvo != undefined)
                                data.arrgmactivityattrvo = arrayOf(data.arrgmactivityattrvo);
                            else
                                data.arrgmactivityattrvo = [];
                            if (data.arrgmcrmlogvo != undefined)
                                data.arrgmcrmlogvo = arrayOf(data.arrgmcrmlogvo);
                            else
                                data.arrgmcrmlogvo = [];

                            data.token = GM.Global.Token;


                            fnGetWebServerData("crmactivity/save", undefined, data, function (tradeData) {
                                if (tradeData != null) {
                                    fnGetWebServerData("crmactivity/save", undefined, input, function (finalData) {
                                        if (finalData != null) {
                                            showSuccess("Activity saved successfully");
                                            if (that.actid == 0)
                                                window.location.href = "#crm/" + that.module + "/id/" + finalData.actid;
                                            else
                                                Backbone.history.loadUrl(Backbone.history.fragment);
                                            // fnUpdateActivity(); // app code. Not for GlobusCRM code

                                        } else
                                            showError("Activity saved Failed");
                                    });
                                }
                            });
                        }
                    });
                } else {
                    fnGetWebServerData("crmactivity/save", undefined, input, function (data) {
                        if (data != null) {
                            showSuccess("Activity saved successfully");
                            if (that.actid == 0)
                                window.location.href = "#crm/" + that.module + "/id/" + data.actid;
                            else
                                Backbone.history.loadUrl(Backbone.history.fragment);
                            // fnUpdateActivity(); // app code. Not for GlobusCRM code
                        } else
                            showError("Activity saved Failed");
                    });
                }

            },

            updateLeadSurg: function (e) {
                var that = this;
                //NPID
                $("#" + this.module + "-assigneenm-partynpid").val(this.sData.npid);
                $("#" + this.module + "-assigneenm-partynpid").attr("partyid", this.sData.partyid);
                $("#" + this.module + "-assigneenm-partynpid").trigger("change");

                //Phone & email
                if (this.sData.arrgmsurgeoncontactsvo.length) {
                    var phone = _.filter(this.sData.arrgmsurgeoncontactsvo, function (data) {
                        return data.contype == "" && data.conmode == 90450 && data.primaryfl == "Y";
                    })

                    if (phone.length) {
                        _.each(this.sData.arrgmsurgeoncontactsvo, function (data) {
                            if (data.contype == "" && data.conmode == 90450 && data.primaryfl == "Y") {
                                $("#lead-hphone").val(data.convalue);
                                $("#lead-hphone").trigger("change");
                            }
                        });
                    } else {
                        $("#lead-hphone").val("");
                        $("#lead-hphone").trigger("change");
                    }

                    var email = _.filter(this.sData.arrgmsurgeoncontactsvo, function (data) {
                        return data.contype == "" && data.conmode == 90452 && data.primaryfl == "Y";
                    })

                    if (email.length) {
                        _.each(this.sData.arrgmsurgeoncontactsvo, function (data) {
                            if (data.contype == "" && data.conmode == 90452 && data.primaryfl == "Y") {
                                $("#lead-hemail").val(data.convalue);
                                $("#lead-hemail").trigger("change");
                            }
                        });
                    } else {
                        $("#lead-hemail").val("");
                        $("#lead-hemail").trigger("change");
                    }
                } else {
                    $("#lead-hphone").val("");
                    $("#lead-hphone").trigger("change");
                    $("#lead-hemail").val("");
                    $("#lead-hemail").trigger("change");
                }
                if (GM.Global.Activity.Mode != "view") // After setting surgeon values, we need to rerender the contents into the screen
                    this.render();

            },

            // Cancel Comments
            cancelComment: function () {
                var that = this;
                $("#" + this.module + "-comments-textarea").val("");
                $(".comment-edit-data").removeClass("hide");
                $("#" + this.module + "-comments-textarea").attr("data-logid", "");
            },

            //Add/ Update Comments data
            saveComment: function (e) {
                if (!GM.Global.Device) {
                    var that = this;
                    var logtypeid = $(".select-comments-logtype").find("option:selected").val();
                    var logtype = $(".select-comments-logtype").find("option:selected").text();
                    var logid = $("#" + this.module + "-comments-textarea").attr("data-logid");
                    var commentData = $("#" + this.module + "-comments-textarea").val();
                    var dd, mm, yyyy;
                    var date = new Date();
                    dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
                    var todayDate = yyyy + "-" + mm + "-" + dd;

                    if (commentData != "" && logtypeid != "") {
                        var token = localStorage.getItem("token");
                        var input = {
                            "token": token,
                            "refid": this.actid,
                            "logid": logid,
                            "logtypeid": logtypeid,
                            "logtype": logtype,
                            "logcomment": commentData,
                            "updatedbyid": localStorage.getItem("userID"),
                            "updatedby": localStorage.getItem("fName") + " " + localStorage.getItem("lName"),
                            "updateddate": todayDate,
                            "voidfl": ""
                        };
                        //                        console.log(JSON.stringify(input));
                        fnGetWebServerData("crmactivity/savelog", undefined, input, function (data) {
                            if (data != null) {
                                showSuccess("Comments saved successfully");
                                $("#" + that.module + "-comments-textarea").val("");
                                if (GM.Global.Editcomment == true) {
                                    that.actModel.set("arrgmcrmlogvo", new Array());
                                    GM.Global.Editcomment = false;
                                }
                                if (that.editComment != "" && that.editComment != undefined)
                                    that.actModel.set("arrgmcrmlogvo", that.editComment);

                                if (that.actModel.get("arrgmcrmlogvo") != undefined) {
                                    that.actModel.get("arrgmcrmlogvo").unshift(data);
                                } else {

                                    that.actModel.set("arrgmcrmlogvo", [])
                                    that.actModel.set("arrgmcrmlogvo", arrayOf(data));
                                }
                                that.render();
                                if (that.module == "physicianvisit") {
                                    var event = {};
                                    event.currentTarget = $("#div-physicianvisit-feedback");
                                    that.togglePVTabs(event);
                                }

                                if (that.module == "merc") {
                                    var event = {};
                                    event.currentTarget = $("#div-merc-comments");
                                    that.toggleMERCTabs(event);
                                }
                                if (that.module == "training") {
                                    var event = {};
                                    event.currentTarget = $("#div-training-comments");
                                    that.toggleTrainingTabs(event);
                                }
                            } else
                                showError("Comments saved Failed");
                        });
                    } else {
                        if (logtypeid == "")
                            showError("Please select department before click on save");
                        else if (commentData == "")
                            showError("Please enter your comments before click on save");
                    }
                } else
                    showError("Network Connection is Required");


            },

            // Edit the comments
            editComments: function (e) {

                if (!GM.Global.Device) {
                    var logid = $(e.currentTarget).parent().parent().parent().attr("logid");
                    var logtypeid = $(e.currentTarget).parent().parent().parent().attr("logtypeid");
                    var commentData = $(e.currentTarget).parent().parent().parent().find(".comments-data-area").text();
                    var editindex = $(e.currentTarget).attr("indexvalue");
                    $("#" + this.module + "-comments-textarea").attr("data-logid", logid);
                    $(".comment-edit-data").removeClass("hide");
                    var element = $(e.currentTarget).parent().parent().parent();
                    element.addClass("comment-edit-data hide");

                    $("#" + this.module + "-comments-textarea").val(commentData).trigger("keyup").focus();
                    $(".select-comments-logtype").val(logtypeid).trigger("change");
                    $("#" + this.module + "-hlogtypeid").val(logid);

                    this.editComment = _.filter(this.actModel.get("arrgmcrmlogvo"), function (data) {
                        return data.logid != logid;
                    });
                    GM.Global.Editcomment = true;
                } else
                    showError("Network Connection is Required");

            },

            //Delete the comment from the comments list and save and display remaining data of comments
            deleteComments: function (e) {

                if (!GM.Global.Device) {
                    var that = this;
                    var logid = $(e.currentTarget).parent().parent().parent().attr("logid");
                    var logtype = $(e.currentTarget).parent().parent().parent().attr("logtype");
                    var logtypeid = $(e.currentTarget).parent().parent().parent().attr("logtypeid");
                    var commentData = $(e.currentTarget).parent().parent().parent().find(".comments-data-area").text();

                    if (logid == "") {
                        var voidComment = _.filter(this.actModel.get("arrgmcrmlogvo"), function (data) {
                            return data.logid == logid;
                        });
                        this.actModel.set("arrgmcrmlogvo", voidComment);
                    } else {
                        _.each(this.actModel.get("arrgmcrmlogvo"), function (data) {
                            if (data.logid == logid)
                                data.voidfl = "Y";
                        });
                        showNativeConfirm("Do you want to delete this comment?", "Confirm", ["Yes", "No"], function (index) {
                            if (index == 1 && that.actid != 0) {
                                var token = localStorage.getItem("token");
                                var input = {
                                    "token": token,
                                    "refid": that.actid,
                                    "logid": logid,
                                    "logtypeid": logtypeid,
                                    "logtype": logtype,
                                    "logcomment": commentData,
                                    "voidfl": "Y"
                                };
                                var element = $(e.currentTarget).parent().parent().parent();
                                element.remove();

                                fnGetWebServerData("crmactivity/savelog", undefined, input, function (data) {});
                            }
                        });

                    }
                } else
                    showError("Network Connection is Required");

            },

            leadUpload: function () {
                $(".gmPanelTitle").html("Upload Lead Data Using Excel");
                $(".div-edit").addClass("hide");
                $("#div-lead-file-upload-btn").addClass("hide");
                $("#div-lead-file-upload-content").addClass("hide");
                $("#li-save").addClass("hide");
                this.fileUpload();
                $("#div-lead-file-upload .lb-spn-file-open").addClass("hide");
                $("#div-lead-file-upload #li-fileupload-fileinfo").addClass("gmAlignHorizontal");
                $("#div-lead-file-upload #span-fileupload-browse-filenm").addClass("tradeshow-file-btn");
                $("#div-lead-file-upload #btn-fileupload-upload").removeClass("hide");
            },

            // Print the current page detail
            printSummary: function () {
                var closure = window.open();
                var data = document.getElementById("div-" + this.module + "-view-closure").innerHTML;
                closure.document.write("<body onload='window.print()' ><div style='width:100%; display:inline-block; text-align:center; font-weight:bold; font-size:20px; margin-bottom:10px'>Physician Visit Details</div>" + data + "</body>");
                closure.document.write('<link rel="stylesheet" href="globusMobileApp/css/app/GmReset.css" type="text/css" />');
                closure.document.write('<link rel="stylesheet" href="globusMobileApp/css/app/GmCRMPrint.css" type="text/css" />');
                closure.document.close();
                closure.focus();
            },

            confirmVisit: function (e) {
                var that = this;

                var todayDate = new Date();
                todayDate.setHours(0, 0, 0, 0);
                var fromdate = new Date(this.actModel.get("actstartdate"));
                fromdate.setHours(0, 0, 0, 0);
                var todate = new Date(this.actModel.get("actenddate"));
                todate.setHours(0, 0, 0, 0);

                if (!GM.Global.Device) {
                    if (this.actid > 0) {
                        if (this.module == "physicianvisit" && GM.Global.Activity.Mode == "edit") {
                            if (todayDate > todate) {
                                this.actModel.set("actstatus", "105764"); // 105764 Completed
                                this.includeSurgeonFl = "No";
                            } else {
                                this.actModel.set("actstatus", "105761"); // 105761 Scheduled 
                                this.includeSurgeonFl = "";
                            }

                            this.mailStatus = 3; // busy
                            this.saveActivity(e);
                        }
                    }
                } else {
                    showError("Internet Connection is Required")
                }

            },

            cancelVisit: function (e) {
                var that = this;
                if (!GM.Global.Device) {
                    if (this.actid > 0) {
                        showPopup();
                        $("#div-crm-overlay-content-container").find('.submit-model').removeClass("hide");
                        $("#div-crm-overlay-content-container").find('.cancel-model').removeClass("hide");
                        $("#div-crm-overlay-content-container").find('.done-model').addClass("hide");
                        $("#div-crm-overlay-content-container").find('.load-model').addClass("hide");
                        $("#div-crm-overlay-content-container").find('.modal-title').text("REASON FOR CANCELLATION");
                        $("#div-crm-overlay-content-container .modal-body").html(this.template_VisitCancelReq());
                        getCLValToDiv("#canreson", "", "", "VSTCAN", "","", "codeid", "canreson", false, true, function(divID, attrVal){});
                        if (this.module == "salescall") {
                            $("#canreson option[value='106501']").attr('disabled', 'disabled').addClass("hide");
                        } else {
                            $("#canreson option[value='106501']").removeClass("hide");
                        }
                        $("#cancel-req-error").css("display", "none");
                        $("#div-crm-overlay-content-container").find('.submit-model').unbind('click').bind('click', function () {
                            var reasonval = $("#canreson option:selected").val();
                            var commentVal = $("#textarea-visit-cancel-req").val();
                            if (parseNull(reasonval) != "" && parseNull(commentVal) != "") {
                                $("#div-crm-overlay-content-container").find('.submit-model').addClass("hide");
                                $("#div-crm-overlay-content-container").find('.cancel-model').addClass("hide");
                                var reasonNm = $("#canreson option:selected").text();
                                var reasonID = $("#span-visit-reason").attr("data_attrid");
                        if (that.module == "salescall") {
                                    that.updateAttrVO(reasonID, "107100", reasonval, reasonNm);
                                } else {
                                    that.updateAttrVO(reasonID, "26240352", reasonval, reasonNm);
                                }
                                var commentID = $("#span-visit-comment").attr("data_attrid");
                                that.updateAttrVO(commentID, "26240350", commentVal, "cancmt");
                                hidePopup();
                                that.actModel.set("actstatus", "105762");
                                that.mailStatus = 4; // cancel
                                that.saveActivity(e);
                            } else if (parseNull(reasonval) == "")
                                $("#cancel-req-error").html("Please select reason").slideDown('fast').css("display", "block").delay(4000).slideUp('slow');
                            else if (parseNull(commentVal) == "")
                                $("#cancel-req-error").html("Please enter the comment").slideDown('fast').css("display", "block").delay(4000).slideUp('slow');
                        });
                        $("#div-crm-overlay-content-container").find('.cancel-model').unbind('click').bind('click', function () {
                            $("#div-crm-overlay-content-container").find('.submit-model').addClass("hide");
                            $("#div-crm-overlay-content-container").find('.cancel-model').addClass("hide");
                            hidePopup();
                            that.backToPage();
                        });

                        //                        }
                    }

                } else {
                    showError("Internet Connection is Required")
                }

            },

            viewDocument: function (event) {
                var path = $(event.currentTarget).find('.hidden-file').text();
                if (parseNull(path) == "")
                    path = $(event.currentTarget).attr("href");

                if (path != undefined)
                    window.open(path, '_blank', 'location=no');
            },


            // MERC program accept party
            mercParty: function (event) {
                var that = this;
                if (event.currentTarget.id == "acceptAll-btn") {
                    if ($("#acceptAll-btn").prop("checked")) {
                        $(".accept-btn").each(function () {
                            this.checked = true;
                        });
                        $(".reject-btn").each(function () {
                            this.checked = false;
                        });
                    } else {
                        $(".accept-btn").each(function () {
                            this.checked = false;
                        });
                    }
                }
                if (event.currentTarget.id == "rejectAll-btn") {
                    if ($("#rejectAll-btn").prop("checked")) {
                        $(".reject-btn").each(function () {
                            this.checked = true;
                        });
                        $(".accept-btn").each(function () {
                            this.checked = false;
                        });
                    } else {
                        $(".reject-btn").each(function () {
                            this.checked = false;
                        });
                    }
                }

                if (event.currentTarget.id == "accept-btn") {
                    $(event.currentTarget).parent().parent().find(".reject-btn").prop("checked", false);
                }
                if (event.currentTarget.id == "reject-btn") {
                    $(event.currentTarget).parent().parent().find(".accept-btn").prop("checked", false);
                }

                if ($(".accept-btn:checked").length == $(".accept-btn").length) {
                    $("#acceptAll-btn").prop("checked", true);
                } else {
                    $("#acceptAll-btn").prop("checked", false);
                }

                if ($(".reject-btn:checked").length == $(".reject-btn").length) {
                    $("#rejectAll-btn").prop("checked", true);
                } else {
                    $("#rejectAll-btn").prop("checked", false);
                }

                $(".merc-party-checkbox:checkbox:not(:checked)").each(function () {
                    var attrid = $(this).attr("data-attrid");
                    _.each(that.actModel.get("arrgmactivityattrvo"), function (data) {
                        if (data.attrid == attrid)
                            data["checkedfl"] = "";
                    });
                });
                $(".accept-btn:checkbox:checked").each(function () {
                    var attrid = $(this).attr("data-attrid");
                    _.each(that.actModel.get("arrgmactivityattrvo"), function (data) {
                        if (data.attrid == attrid)
                            data["checkedfl"] = "Y";
                    });
                });
                $(".reject-btn:checkbox:checked").each(function () {
                    var attrid = $(this).attr("data-attrid");
                    _.each(that.actModel.get("arrgmactivityattrvo"), function (data) {
                        if (data.attrid == attrid)
                            data["checkedfl"] = "N";
                    });
                });
            },

            // Render page to checked the selected checkboxs
            checkSelected: function () {
                $(".accept-btn").each(function () {
                    var checkedfl = $(this).attr("data-checkedfl");
                    if (checkedfl == 'Y') {
                        $(this).prop("checked", true);
                    } else
                        $(this).prop("checked", false);
                });
                $(".reject-btn").each(function () {
                    var checkedfl = $(this).attr("data-checkedfl");
                    if (checkedfl == 'N') {
                        $(this).prop("checked", true);
                    } else
                        $(this).prop("checked", false);
                });

                if ($(".accept-btn:checked").length == $(".accept-btn").length) {
                    $("#acceptAll-btn").prop("checked", true);
                } else {
                    $("#acceptAll-btn").prop("checked", false);
                }

                if ($(".reject-btn:checked").length == $(".reject-btn").length) {
                    $("#rejectAll-btn").prop("checked", true);
                } else {
                    $("#rejectAll-btn").prop("checked", false);
                }
            },

            // get all checked items
            selectChecked: function () {
                var that = this;
                $(".accept-btn:checkbox:checked").each(function () {
                    var partyid = $(this).attr("data-partyid");
                    var attrid = $(this).attr("data-attrid");
                    var actid = $("#activity-controlid-merc").val();
                    var roleid = "105564";
                    var partynm = $(this).attr("data-partynm");

                    var inputParty = {
                        "ID": partyid,
                        "Name": partynm
                    };
                    that.setPartyInfo(inputParty, "7000", "105564");

                    var attrtype = "105589";
                    var attrval = partyid;
                    if (attrid != "") {
                        _.each(that.actModel.get("arrgmactivityattrvo"), function (data) {
                            if (data.attrid == attrid)
                                data.voidfl = "Y";
                        });
                    } else {
                        var voidAttr = _.filter(that.actModel.get("arrgmactivityattrvo"), function (data) {
                            return data.attrval != attrval;
                        });
                        that.actModel.set("arrgmactivityattrvo", voidAttr);
                    }
                });
                $(".reject-btn:checkbox:checked").each(function () {
                    var attrtype = "105589";
                    var attrval = $(this).attr("data-partyid");
                    var attrid = $(this).attr("data-attrid");
                    if (attrid != "") {
                        _.each(that.actModel.get("arrgmactivityattrvo"), function (data) {
                            if (data.attrid == attrid)
                                data.voidfl = "Y";
                        });
                    } else {
                        var voidAttr = _.filter(that.actModel.get("arrgmactivityattrvo"), function (data) {
                            return data.attrval != attrval;
                        });
                        that.actModel.set("arrgmactivityattrvo", voidAttr);
                    }
                });

            },
            setVisitorNm: function (e) {
                var that = this;
                e.preventDefault();
                var target = $(e.currentTarget).attr("id");
                var currAttrVal = $(e.currentTarget).val();

                if (target == "input-physicianvisit-other-visitor") {
                    $(e.currentTarget).parent().find('#input-physicianvisit-other-visitor-title').attr("attrval", currAttrVal);
                }
                if (target == "input-physicianvisit-other-visitor-title") {
                    $(e.currentTarget).parent().find('#input-physicianvisit-other-visitor').attr("attrnm", currAttrVal);
                }

            },
            checkDivVisible: function () {
                var actstatus = this.actModel.get("actstatus");
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1;
                var yyyy = today.getFullYear();
                if (dd < 10)
                    dd = '0' + dd
                if (mm < 10)
                    mm = '0' + mm
                today = yyyy + '-' + mm + '-' + dd;
                
                if (this.module == "physicianvisit") {
                    var actTyp = this.actModel.get("acttype");
                    if (actTyp != "") {
                        if (actTyp == "105226") { //105226-Design , 105227-Research
                            $(".ul-physicianvisit-display-projects").removeClass("hide");
                            $(".ul-physicianvisit-display-product").addClass("hide");
                        } else {
                            $(".ul-physicianvisit-display-projects").addClass("hide");
                            $(".ul-physicianvisit-display-product").removeClass("hide");
                        }
                        if (actTyp == "105226" || actTyp == "105227") {
                            $("#physicianvisit-product-search").addClass("hide");
                            $("#physicianvisit-product-search-lab").addClass("hide");
                            $("#physicianvisit-project-search").addClass("hide");
                            $("#physicianvisit-product-input-section").removeClass("hide");
                            $("#physicianvisit-product-input-lab-section").removeClass("hide");
                            $("#physicianvisit-project-input-section").removeClass("hide");
                        } else {
                            $("#physicianvisit-product-input-lab-section").addClass("hide");
                            $("#physicianvisit-product-input-section").addClass("hide");
                            $("#physicianvisit-project-input-section").addClass("hide");
                            $("#physicianvisit-product-search").removeClass("hide");
                            $("#physicianvisit-product-search-lab").removeClass("hide");
                            $("#physicianvisit-project-search").removeClass("hide");
                        }

                        if (actTyp != "105225") {
                            $("#physicianvisit-product-search-lab").find('input').removeAttr('disabled');
                            $("#textarea-visit-other-instr").removeAttr('disabled');
                        } else {
                            $("#physicianvisit-product-search-lab").find('input').attr('disabled', 'disabled');
                            $("#textarea-visit-other-instr").attr('disabled', 'disabled');
                        }
                    }

                    if (localStorage.getItem("deptid").toUpperCase() == "S")
                        $("#acttype option[value='105226']").attr('disabled', 'disabled').addClass("hide");

                    var getDivision = this.actModel.get("division");
                    if (getDivision == "")
                        this.actModel.set("division", localStorage.getItem("repDivision"));

                    if (isRep()) {
                        $("#li-physicianvisit-title").removeClass("hide");
                        $("#li-sc-title").addClass("hide");

                        $(".visit-edit-division").addClass("hide");
                    } else {
                        if (!$("#li-sc-title").hasClass("hide")) {
                            $("#span-edit-title").addClass("hide");
                            $("#li-physicianvisit-title").addClass("hide");
                        } else {
                            $("#span-edit-title").removeClass("hide");
                            $("#li-physicianvisit-title").removeClass("hide");
                        }
                    }


                    if (_.contains(GM.Global.Admin, this.module) && (GM.Global.Activity.Mode == "edit")) {
                        $("#div-physicianvisit-confirm").removeClass("hide");
                        $("#div-physicianvisit-cancel").removeClass("hide");
                    } else {
                        $("#div-physicianvisit-confirm").addClass("hide");
                        $("#div-physicianvisit-cancel").addClass("hide");
                        $("#" + this.module + "-actstartdate").attr("min", today);
                        $("#" + this.module + "-actenddate").attr("min", today);
                    }


                    if (actstatus == "105761") {
                        $("#div-physicianvisit-confirm").addClass("hide");
                    }
                    if (actstatus == "105762") {
                        $("#div-physicianvisit-cancel").addClass("hide");
                        $("#div-physicianvisit-confirm").addClass("hide");
                    }

                    if (parseNull(this.actid) == 0)
                        $("#ul-visit-title").addClass("hide");
                }

            },

            changeTitle: function () {
                $("#li-sc-title").removeClass("hide");
                $("#li-physicianvisit-title").addClass("hide");
                $("#span-edit-title").addClass("hide");
                $("#li-title-set-btn").removeClass("hide");
            },

            setTitle: function () {
                var that = this;
                $("#li-sc-title").addClass("hide");
                $("#span-edit-title").removeClass("hide");
                $("#li-physicianvisit-title").removeClass("hide");
                $("#li-title-set-btn").addClass("hide");

                var inputText = $("#input-physicianvisit-title").val();
                $("#li-physicianvisit-title").text(inputText);
                $(".gmPanelTitle").text(inputText);
                this.actModel.set("actnm", inputText);
            },
            resetTitle: function () {
                this.setActTitle();
            },

            cancelTitle: function () {

                $("#li-physicianvisit-title").removeClass("hide");
                $("#li-sc-title").addClass("hide");
                $("#span-edit-title").removeClass("hide");
                $("#li-title-set-btn").addClass("hide");
            },

            updateAttrVal: function () {
                if ($(".ul-physicianvisit-display-product").hasClass("hide")) {
                    _.each(this.actModel.get("arrgmactivityattrvo"), function (data) {
                        if (data.attrtype == "105583" && data.attrid != "")
                            data.voidfl = 'Y';
                    });
                    var rmAttr = _.reject(this.actModel.get("arrgmactivityattrvo"), function (data) {
                        return data.attrtype == "105583" && data.attrid == "";
                    });
                    this.actModel.set("arrgmactivityattrvo", rmAttr);
                }
                if ($(".ul-physicianvisit-display-projects").hasClass("hide")) {
                    _.each(this.actModel.get("arrgmactivityattrvo"), function (data) {
                        if (data.attrtype == "105582" && data.attrid != "")
                            data.voidfl = 'Y';
                    });
                    var rmAttr = _.reject(this.actModel.get("arrgmactivityattrvo"), function (data) {
                        return data.attrtype == "105582" && data.attrid == "";
                    });
                    this.actModel.set("arrgmactivityattrvo", rmAttr);
                }
                if ($(".ul-physicianvisit-display-product-lab").find('input').attr('disabled') == "disabled") {
                    _.each(this.actModel.get("arrgmactivityattrvo"), function (data) {
                        if (data.attrtype == "105586" && data.attrid != "")
                            data.voidfl = 'Y';
                    });
                    var rmAttr = _.reject(this.actModel.get("arrgmactivityattrvo"), function (data) {
                        return data.attrtype == "105586" && data.attrid == "";
                    });
                    this.actModel.set("otherinstr", "");
                    this.actModel.set("arrgmactivityattrvo", rmAttr);
                }
                
                //PC-4002 - Block Sales Reps from booking Physician Visit on Restricted Days
                if (this.module == "physicianvisit") {
                    if (this.actModel.get("acttype") == "26240596") {
                        $('#divtyp option:first').text('All Division');
                        $('#divtyp').val('');
                        }
                    else
                          $('#divtyp option:first').text('Select');
                          }
                
                this.render();
            },

            rmVisitorVal: function () {
                $(".div-sc-extn").each(function () {
                    var visitorNm = $(this).find("#input-physicianvisit-other-visitor").val();
                    var visitorTitle = $(this).find("#input-physicianvisit-other-visitor-title").val();
                    var visitorTitle = $(this).find("#input-physicianvisit-other-visitor-title").val();
                    var visitorIndex = $(this).find("input").attr("index");
                    if (parseNull(visitorNm) == "" && parseNull(visitorTitle) == "") {
                        $(this).find('.requiredInput' + visitorIndex).removeAttr('validate').removeClass('gmValidationError');
                    }

                });
            },

             toggleSyncCal: function (e) {
                
               
                if($(window).width() < 768) {
                    if ($(e.currentTarget).find("input").is(":checked"))
                         this.actModel.set("devicefl", "Y"); // used for backend create event
                else
                    this.actModel.set("devicefl", "N");
                }
                else{
                    
                $(e.currentTarget).find("input").trigger("click");

                if ($(e.currentTarget).find("input").is(":checked"))
                    this.actModel.set("devicefl", "Y"); // used for backend create event
                else
                    this.actModel.set("devicefl", "N");
            }
                                                      
            },
            
            activityDetail :function(){
                 var that = this;
                 var actTyp = this.actModel.get("acttype");
                $(".upload-file-list").html("");
             
                    
                var input = {
                        "token": localStorage.getItem('token'),
                        "actid": that.actid,
                        "actcategory": actTyp
                    };
                // here we are fetch the activity details
                 fnGetWebServerData("crmactivity/info", "gmCRMActivityVO", input, function (data) {
                     
                            });
                    var fileinput = {
                                "token": localStorage.getItem("token"),
                                "refid": that.actid,
                            "refgroup": "110762" //travel doc group id
                            };
                // here we are fetch the Travel document details
                    fnGetWebServerData("file/get", "gmCRMFileUploadVO", fileinput, function (file) {
                        if (file != null) {
                            file = arrayOf(file);

                            _.each(file, function (v) {
                                var docLink = URL_FileServer+"traveldocupload"+ "/" + v.refgroup + "/" +v.reftype + "/" + v.refid + "/" + v.filename;

                                //here we are getting file server path from gmserver.js

                                $(".upload-file-list").append("<div class = 'a-view-docs gmLnk gmUnderline gmPaddingButtom10p'><span class='hide hidden-file'>" + docLink + "</span> <span class='' href = '" + URL_FileServer + v.refgroup + "/" + v.refid + "/" + v.filename + "'>" + v.filetitle + " - " + v.updatedby + " on " + v.updateddate + "</span></div>");
                            });
                        }
                        else {
                                $(".a-service-desc-agenda-file-name").html("Unavailable").addClass("gmFontItalic");
                                if ((that.module == "physicianvisit") || (that.module == "merc") || (that.module == "training")) {
                                    if (GM.Global.Activity.Mode == "view")
                                        that.pvFiles = [];
                                    $(".upload-file-list").html("Unavailable").addClass("gmFontItalic");
                                }
                            }
                    });
            }
        });
        return GM.View.ActivityDetail;
    });
