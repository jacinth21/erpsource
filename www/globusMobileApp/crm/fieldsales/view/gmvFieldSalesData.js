define([
        'jquery', 'underscore', 'backbone', 'handlebars',
        'global', 'commonutil',
        'gmcItem', 'gmvItemList', 'gmvSurgeonDetail', 'gmvSearch',

        // Pre-Compiled Template
        'gmtTemplate'
 
        ], 
        function ($, _, Backbone, Handlebars, Global, CommonUtil, GMCItem, GMVItemList, GMVSurgeonDetail,GMVSearch, GMTTemplate) {

	'use strict';

	GM.View.FieldSalesData = Backbone.View.extend({


//		el: "#div-rep-module",

		name: "Field Sales Data View",
        template_SurgeonMERC: fnGetTemplate(URL_Surgeon_Template, "gmtMERCListPopup"),
        template_LeadAssignee: fnGetTemplate(URL_Lead_Template, "gmtLeadAssginee"),
        /* Templates */
        events : {
            "click .li-surg-add-merc": "listUpcomingMERC",
             "click .li-lead-assignto"                   : "assignee",
            "click #div-assignee-tap-close"             : "closeOverlay",
             "click .li-surgeon-calendar"                : "createSurActivity",
            "click .li-lead-calendar"                   : "createleadActivity",
		},

        initialize: function(options) {
            console.log("Inside Field Sales Data");
            this.tabAccess = _.pluck($.parseJSON(localStorage.getItem('userTabs')), "functionID");
            this.info = options.info;
            this.repid = options.repid;
            this.module = options.module;
            this.render();
            //this.render(options);
        },

        render: function(options) {
            
            
            
            GM.Global.FieldSales.Module = this.module;

            var that = this;
             $(window).on('orientationchange', this.onOrientationChange);
            console.log(this.module);
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();
            if(dd<10) { dd='0'+dd };
            if(mm<10) { mm='0'+mm };
            this.dbtoday = yyyy+'/'+mm+'/'+dd;
            this.today = mm+'/'+dd+'/'+yyyy;
            
            switch (this.module) {
                case "surgeon":
                    that.listSurgeons();
                    break;
                case "physicianvisit":
                    that.listPhysicianvisit();
                    break;
                case "lead":
                    that.listLead()
                    break;
                case "salescall":
                    that.listSalesCall()
                    break;
            }
            
            return this;
        },
          createSurActivity: function (e) {
            console.log(e.currentTarget);
            if($(e.currentTarget).hasClass("active-tab")) {
                $(e.currentTarget).removeClass("active-tab");
                $(".li-surg-create-activity").addClass("hide");
            }
            else {
                $(".active-tab").removeClass("active-tab");
                $(".li-surg-create-activity").addClass("hide");
                $(e.currentTarget).addClass("active-tab");
                $(e.currentTarget).parent().find(".li-surg-create-activity").removeClass("hide"); 
            }
        },
            createleadActivity: function (e) {
            console.log(e.currentTarget);
            if($(e.currentTarget).hasClass("active-tab")) {
                $(e.currentTarget).removeClass("active-tab");
                $(".li-lead-create-activity").addClass("hide");
            }
            else {
                $(".active-tab").removeClass("active-tab");
                $(".li-lead-create-activity").addClass("hide");
                $(e.currentTarget).addClass("active-tab");
                $(e.currentTarget).parent().find(".li-lead-create-activity").removeClass("hide"); 
            }
        },
	 getRepInfo: function(ID, Name, Element, partyid) {
        	var that = this;
            var input = {"token": GM.Global.Token, "actid": this.leadid, "actcategory": 105269};
            fnGetWebServerData("crmactivity/info", "gmCRMActivityVO", input, function(data){
                console.log(data);
                if(data!=null){
                    if(data.arrgmactivitypartyvo!=undefined)
                        data.arrgmactivitypartyvo = arrayOf(data.arrgmactivitypartyvo);
                    else
                        data.arrgmactivitypartyvo = [];
                    if(data.arrgmcrmaddressvo!=undefined)
                        data.arrgmcrmaddressvo = arrayOf(data.arrgmcrmaddressvo);
                    else
                        data.arrgmcrmaddressvo = [];
                    if(data.arrgmactivityattrvo!=undefined)
                        data.arrgmactivityattrvo = arrayOf(data.arrgmactivityattrvo);
                    else
                        data.arrgmactivityattrvo = [];
                    if(data.arrgmcrmlogvo!=undefined)
                        data.arrgmcrmlogvo = arrayOf(data.arrgmcrmlogvo);
                    else
                        data.arrgmcrmlogvo = [];
                    data.partyid = partyid;
                    data.partynm = Name;
                    data.token = GM.Global.Token;
                    var nm = Name.replace(" ","");
                    nm = nm.toLowerCase();
                    nm = nm+"@globusmedical.com";
                    nm = "cskumar@globusmedical.com";
                    fnGetWebServerData("crmactivity/save", undefined, data, function (saveData) {
                        console.log(saveData);
                        if(saveData!=null)
                            Backbone.history.loadUrl(Backbone.history.fragment);
                    });
                }
                
            });

        },
        listUpcomingMERC: function(event) {
            if(!GM.Global.Device){
                var that = this;
                if($(event.currentTarget).hasClass("act-merc")) {
                    this.surgeonID = $(event.target).parent().parent().attr("data-partyid");
                    this.surgeonName = $(event.target).parent().parent().attr("data-partynm");    
                } 
                else {
                    this.surgeonID = $(event.currentTarget).attr("data-partyid");
                    this.surgeonName = $(event.currentTarget).attr("data-partynm"); 
                }
                showPopup();
                $("#div-crm-overlay-content-container .modal-body").html(this.template_SurgeonMERC());
                $(".modal-title").text($(event.currentTarget).attr("title"));
                $(".modal-footer").addClass("hide");

                var voname = "gmCRMActivityVO";
                var sortoptions = [{ "DefaultSort" : "actstartdate",
                            "Elements" : [{"ID" : "actnm" , "DispName" : "Title","ClassName":"gmDashColHead gmFontmerc gmText150"}, 
                                                  {"ID" : "actstartdate" , "DispName" : " From Date","ClassName":"gmDashColHead gmFontmerc gmText125"}, 
                                                  {"ID" : "actenddate" , "DispName" : " To Date","ClassName":"gmDashColHead gmFontmerc gmText100"}, 
                                                  {"ID" : "location" , "DispName" : "Location","ClassName":"gmDashColHead gmFontmerc gmText125"} ]}]; 
                var filterOptions = ["actnm", "actstartdate", "location", "actstatus"];
                var itemtemplate = "gmtSurgeonMERCList";
                var columnheader = 6;
                var template_URL = URL_Surgeon_Template;
                var controlID = "#div-crm-overlay-content-container #div-popup-merc-list";
                var url = "crmactivity/info";
                that.search =["actnm","actstartdate","actenddate","location"]
                var color= "gmFontmerc";
                var input = { "token" : GM.Global.Token,"actcategory" : "105265", "actstartdate": this.today};
                console.log(JSON.stringify(input))
                fnGetWebServerData(url, voname, input, function(data){
                        if(data!=null) {
                            data = arrayOf(data);
                             _.each(data,function(data){
                               data.actstartdate=getDateByFormat(data.actstartdate);
                               data.actenddate=getDateByFormat(data.actenddate);
                                if(data.partyid!="" && data.partyid.indexOf(that.surgeonID) >= 0)
                                    data["added"] = "Y";
                                 if(data.attrpartyid!="" && data.attrpartyid.indexOf(that.surgeonID) >= 0)
                                     data["reqstd"] = "Y";
                            });
                            that.showItemListView(controlID, data, itemtemplate, columnheader, sortoptions, template_URL, filterOptions,undefined, color);
                             $("#div-crm-overlay-content-container .modal-body .li-add-merc").bind("click",function(event) {
                               that.addParty(event); 
                            });
                        }
                        else 
                             $(controlID).html("No Data Found");

                });
            }
            else{
                showError("Internet connection is required for this option");
            }
            
        },
        onOrientationChange: function(event) {
                resizeHeadWidth('.gmPanelHead', '.gmBtnGroup', '#panelhead-left');
            },
        addParty: function(event) {
            var that = this;
            
            var input = {
                "attrid": "",
                "attrval": this.surgeonID,
                "attrnm": this.surgeonName,
                "actid": $(event.currentTarget).attr("actid"),
                "attrtype": "105589",
                "voidfl": ""
            };
            console.log("Add Party Input"+ JSON.stringify(input));
            hidePopup();
            fnGetWebServerData("crmactivity/addattr", "gmCRMActivityAttrVO", input, function(data) {
                if(data!=null)
                    showSuccess(that.surgeonName + " requested for " + $(event.currentTarget).attr("actnm"),5000);
                else
                    showError("Request Failed");
            });
            
        },
        searchrep: function () {
            var that = this;
            var searchOptions = {
                'el' : $("#div-assign-to-search"),
                'url' : URL_WSServer + 'search/rep',
                'inputParameters' : {'token' : localStorage.getItem('token')},
                'keyParameter' : 'repname',
                'IDParameter' : 'repid',
                'NameParameter' : 'repname',
                'resultVO' : 'gmSalesRepQuickVO',
                'minChar' : 2,
                'callback' : function(result) {
                    that.getRepInfo(result.ID, result.Name, result.Element, result.partyid);
                }
             };
            var searchView = new GMVSearch(searchOptions);
        },

          
        listSurgeons: function() {
            var that = this;
            this.input = { "token" : GM.Global.Token };
            this.voname = "gmCRMSurgeonlistVO";
            this.sortoptions = [{ "DefaultSort" : "firstnm", 
                             "Elements" : [{"ID" : "firstnm" , "DispName" : "First Name","ClassName":"gmDashColHead gmFontsurgeon gmText200 sur-liheader-fname"},
                                           {"ID" : "midinitial" , "DispName" : "M.I","ClassName":"gmDashColHead gmFontsurgeon gmText50 sur-liheader-minitial"},
                                           {"ID" : "lastnm" , "DispName" : "Last Name","ClassName":"gmDashColHead gmFontsurgeon gmText200 sur-liheader-lname"},
                                           {"ID" : "city" , "DispName" : "City","ClassName":"gmDashColHead gmFontsurgeon gmText125 sur-liheader-city"},
                                           {"ID" : "state" , "DispName" : "State","ClassName":"gmDashColHead gmFontsurgeon gmText125 sur-liheader-state"}
                                          ]
                          }];
            this.filterOptions = ["firstnm","lastnm"];
            this.columnheader = 4;
            this.template_URL = URL_Surgeon_Template;
            this.itemtemplate = "gmtSurgeonList";
            this.controlID = "#div-rep-module-surgeon";
            this.url = "surgeon/info";
            this.color = "gmFontsurgeon";
            this.callbackOnReRender = this.checkSurgeonAccess();

            this.dispList(this.info);
        },

        listPhysicianvisit: function() {
            var that = this;
            this.input = { "token" : GM.Global.Token, "actcategory" : "105266" };
            this.voname = "gmCRMActivityVO";
            this.sortoptions =[{ "DefaultSort" : "phynm", 
                                "Elements" : [{"ID" : "acttype","DispName" : "Type of Visit","ClassName":"gmDashColHead gmFontphysicianvisit gmText200"},
                                {"ID" : "actstartdate" , "DispName" : "Date of Visit","ClassName":"gmDashColHead gmFontphysicianvisit gmText200"},
                                {"ID" : "phynm" , "DispName" : "Physician(s) Name","ClassName":"gmDashColHead gmFontphysicianvisit gmText250"},
                                {"ID" : "actstatus" , "DispName" : "Status","ClassName":"gmDashColHead gmFontphysicianvisit gmText200"}]}];
            this.filterOptions = ["acttype","phynm"];
            this.columnheader = 4;
            this.template_URL = URL_PhysicianVisit_Template;
            this.itemtemplate = "gmtPhysicianVisitList";
            this.controlID = "#div-rep-module-physicianvisit";
            this.url = "crmactivity/info";
            this.color = "gmFontphysicianvisit";
            this.dispList(this.info);
        },

        listSalesCall: function() {
            var that = this;
            this.input = { "token" : GM.Global.Token, "actcategory" : "105267" };
            this.voname = "gmCRMActivityVO";
             this.sortoptions =[{ "DefaultSort" : "actstartdate", 
                                "Elements" : [{"ID" : "phynm","DispName" : "Surgeon","ClassName":"gmDashColHead gmFontsalescall gmText200"},
                                {"ID" : "actstartdate" , "DispName" : "Date & Time","ClassName":"gmDashColHead gmFontsalescall gmText225"},
                                {"ID" : "products" , "DispName" : "Product","ClassName":"gmDashColHead gmFontsalescall gmText350"}]}];
            this.filterOptions = ["phynm","products"];
            this.columnheader = 4;
            this.template_URL = URL_SalesCall_Template;
            this.itemtemplate = "gmtSalesCallList";
            this.controlID =  "#div-rep-module-salescall";
            this.url = "crmactivity/info";
            this.color = "gmFontsalescall";
            this.dispList(this.info);
        },

        // To list the leads
        listLead: function() {
            
            var that = this;
            this.voname = "gmCRMActivityVO";
            this.input = {"token":localStorage.getItem("token"),"partyid":GM.Global.FieldSales.RepPartyID,"actcategory": "105269"};
            this.sortoptions = [{ "DefaultSort" : "actstartdate", "Elements" : [
								{"ID" : "actstartdate","DispName" : "Generated On","ClassName":"gmDashColHead gmFontlead gmText125 lead-date"},
						 		{"ID" : "phynm" , "DispName" : "Name","ClassName":"gmDashColHead gmFontlead gmText150  lead-phynm"},
								{"ID" : "source" , "DispName" : "Source","ClassName":"gmDashColHead gmFontlead gmText125 lead-source"},
								{"ID" : "products" , "DispName" : "Products","ClassName":"gmDashColHead gmFontlead gmText250 lead-products"},
								{"ID" : "assignee" , "DispName" : "Assignee","ClassName":"gmDashColHead gmFontlead gmText150 lead-rep"}
								]}];
            this.filterOptions = ["phynm"];
            this.columnheader = 4;
            this.template_URL = URL_Lead_Template;
            this.itemtemplate = "gmtLeadList";
            this.controlID = "#div-rep-module-lead";
            this.url = "crmactivity/info";
            this.color = "gmFontlead";
            this.dispList(this.info);
        },

        assignee: function (e) {
            var that=this;

            this.$("#div-main-content").children().bind('click', false);

//            $("#div-lead-popup-assignee").html(this.template_assignee());
            this.$el.append(that.template_LeadAssignee())
            console.log($(e.currentTarget));
            this.leadid = $(e.currentTarget).find('div').attr('data-leadid');
            console.log(this.leadid);

            this.searchRep();
        },

        searchRep: function () {
            var that = this;
            var searchOptions = {
                'el' : $("#div-assign-to-search"),
                'url' : URL_WSServer + 'search/rep',
                'inputParameters' : {'token' : localStorage.getItem('token')},
                'keyParameter' : 'repname',
                'IDParameter' : 'repid',
                'NameParameter' : 'repname',
                'resultVO' : 'gmSalesRepQuickVO',
                'minChar' : 2,
                'callback' : function(result) {
                    that.getRep(result.ID, result.Name, result.Element);
                }
             };

            var searchView = new GMVSearch(searchOptions);
             
        },
       getRep: function(ID, Name, Element) {
        	var that = this;
            this.repid = ID;
            this.repname = Name;
            that.$("#div-assign-to-search input").val(Name); 
            // var repData = ({"ID" : this.repid, "Name" : this.repname});
             console.log(ID);
           	var token = localStorage.getItem("token");
           	var input = {"token":token, "repid": this.repid, "leadid" : this.leadid};
			console.log(input);

			fnGetWebServerData("lead/assign", undefined, input, function(data){
                if(data!=null){
                    console.log(data)
                    var data = arrayOf(data);
				showSuccess("Assignee is saved successfully");
                console.log(that.leadData);
//                    that.leadData = _.filter(arrayOf(that.leadData), function(data){      // reps only get list and they can't assign. s0 no need
//                        return data.leadid!= that.leadid;
//                    });
                        for (var i = 0; i < that.leadData.length; i++) {
						// console.log(data.repname);
						if (data[0].leadid == that.leadData[i].leadid) {
							console.log(data[0].leadid, that.leadData[i].leadid);
							that.leadData[i].repnm = data[0].repnm;
							console.log(data[0].repnm);
							console.log(that.leadData[0].repnm);
				            that.sortoptions = [{ "DefaultSort" : "generatedon", "Elements" : [
								{"ID" : "generatedon","DispName" : "Generated On","ClassName":"gmDashColHead gmFontlead gmText125"},
						 		{"ID" : "partynm" , "DispName" : "Name","ClassName":"gmDashColHead gmFontlead gmText150"},
								{"ID" : "phone" , "DispName" : "Phone","ClassName":"gmDashColHead gmFontlead gmText125"},
								{"ID" : "email" , "DispName" : "Email","ClassName":"gmDashColHead gmFontlead gmText175"},
								{"ID" : "repnm" , "DispName" : "Assignee","ClassName":"gmDashColHead gmFontlead gmText150"}]}];
							that.filterOptions = ["generatedon","partynm","repnm"];
							that.itemtemplate = "gmtLeadList";
							that.columnheader = 5;
							that.controlID = "#div-main-content";
							that.template_URL = URL_Lead_Template;
                            that.color = "gmFontlead";
							that.showItemListView(that.controlID, that.leadData, that.itemtemplate, that.columnheader, that.sortoptions, that.template_URL, that.filterOptions, that.color);
							that.closeOverlay();
						}
						else {
							that.closeOverlay();
						}
					}
                }
                else
                    showError("Lead Assign Failed");
				
                
			});
            hideMessages();
        },
        closeOverlay: function () {
//            $("#div-lead-assginee-main").hide();
            $("#div-lead-assginee-main").remove();
            this.$("#div-main-content").children().unbind('click', false);
        },
         assignMyself: function () {
        	var that = this;
			var token = localStorage.getItem("token");
			var userID = localStorage.getItem("userID");

			var input = {"token":token, "repid": userID, "leadid" : this.leadid};
			console.log(input);

			fnGetWebServerData("lead/assign", undefined, input, function(data){
                if(data!=null){
                    console.log(data)
                    var data = arrayOf(data);
                    console.log("Assignee is saved successfully");
                   console.log(that.leadData);
                        for (var i = 0; i <that.leadData.length; i++) {
                        // console.log(data.repname);
                        if (data[0].leadid == that.leadData[i].leadid) {
                            console.log(data[0].leadid, that.leadData[i].leadid);
                            that.leadData[i].repnm = data[0].repnm;
                            console.log(data[0].repnm);
                            console.log(that.leadData[0].repnm);
                            that.sortoptions = [{ "DefaultSort" : "generatedon", "Elements" : [
                                {"ID" : "generatedon","DispName" : "Generated On","ClassName":"gmDashColHead gmFontlead gmText125"},
                                {"ID" : "partynm" , "DispName" : "Name","ClassName":"gmDashColHead gmFontlead gmText150"},
                                {"ID" : "phone" , "DispName" : "Phone","ClassName":"gmDashColHead gmFontlead gmText125"},
                                {"ID" : "email" , "DispName" : "Email","ClassName":"gmDashColHead gmFontlead gmText175"},
                                {"ID" : "repnm" , "DispName" : "Assignee","ClassName":"gmDashColHead gmFontlead gmText150"}]}];
                            that.filterOptions = ["generatedon","partynm","repnm"];
                            that.itemtemplate = "gmtLeadList";
                            that.columnheader = 5;
                            that.controlID = "#div-main-content";
                            that.template_URL = URL_Lead_Template;
                            that.color = "gmFontlead";
                            console.log(that.color);
                            that.showItemListView(that.controlID, that.leadData, that.itemtemplate, that.columnheader, that.sortoptions, that.template_URL, that.filterOptions,that.color);
                            that.closeOverlay();
                        }
                        else {
                            that.closeOverlay();
                        }
                    }
                }
				
			});
             hideMessages();
        },
        
        dispList: function(data){
            var that = this;
            console.log("datadatadatadata")
            console.log(data)
            if(data && data != null && data.length > 0) {
                data = $.parseJSON(JSON.stringify(data));
                if(GM.Global.Module=="lead")
                    that.leadData= arrayOf(data);
                var userTabs = _.pluck($.parseJSON(localStorage.getItem('userTabs')),"functionID");
                data  = arrayOf(data);
                _.each(data, function (data, i) {
                    if(_.contains(userTabs, 'CRM-PHYSICIANVISIT'))
                        data.phy = "Y";
                    if(_.contains(userTabs, 'CRM-SURGEON'))
                        data.title = parseNull(data.salutenm)+" "+data.firstnm +" "+ parseNull(data.midinitial)+" "+data.lastnm;
                    if(_.contains(userTabs, 'CRM-MERC'))
                        data.merc = "Y";
                    if(_.contains(userTabs, 'CRM-SALESCALL'))
                        data.scall = "Y";
                    data["index"] = i + 1;
                    data.server = URL_CRMFileServer;
                    
                     var dd, mm, yyyy;
                    if(data.actstartdate && data.actstartdate!=""){
                        var date = new Date(data.actstartdate);
                        dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
                        data.actstartdate = mm + "/" + dd + "/" + yyyy;
                    }
                    if(data.actenddate && data.actenddate!=""){
                        var date = new Date(data.actenddate);
                        dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
                        data.actenddate = mm + "/" + dd + "/" + yyyy;
                    }
                });
                for(var i=0;i<data.length;i++) {         // This Code is put back to avoid writing it's web service for all lists
                    var row = data[i];
                    data[i].search = ""
                    if(this.search && this.search.length){
                        _.each(this.search, function(srh){
                            data[i].search += row[srh] + " ";
                        })
                    }
                    else{
                        for (var column in row) {
                            if(column != "actid")
                                data[i].search += row[column] + " ";
                            }	
                    }
                }
                console.log(data);
                that.collection = new GMCItem(data);

                console.log(that.collection);
                var gmvItemList = new GMVItemList({ 
                    el: that.controlID, 
                    collection: that.collection,
                    columnheader: that.columnheader, 
                    itemtemplate: that.itemtemplate,
                    sortoptions: that.sortoptions,
                    template_URL: that.template_URL,
                    filterOptions: that.filterOptions,
                    csvFileName: that.csvFileName,
                    color: that.color,
                    callbackOnReRender: that.callbackOnReRender
                });
                console.log(that.gmvItemList);

//                if(GM.Global.Module == "lead" && isRep())
//                    $(".li-lead-assignto").remove();

//                    $("#div-rep-module").removeClass();

            } else 
                $("#div-rep-module-"+this.module).html("No Data Found").addClass("gmTextAll gmTextCenter gmPaddingTop10p");
        },

        checkSurgeonAccess: function() {
            var userTabs = _.pluck($.parseJSON(localStorage.getItem('userTabs')),"functionID");
            if(!_.contains(userTabs,'CRM-MERC'))
                this.$(".li-surg-add-merc").remove();
            if(!_.contains(userTabs,'CRM-SALESCALL'))
                this.$(".li-surg-salescall").remove();
        },		
        
        showFilterName: function (data) {
            var that = this;
            var tempArray = new Array();
            $.each(data, function(key, val) {
                if(val != "")
                    tempArray.push({"title":key, "value":val});
            });
            var criteriaValues = $.parseJSON(JSON.stringify(tempArray));
            this.$(".div-category-items").html(this.template_selected_items(criteriaValues));
            this.$(".div-text-selected-crt").addClass("gmFont"+GM.Global.Module);
            $(".li-new-favorite").removeClass("hide");
            console.log(GM.Global.myFilter)
            if(GM.Global.myFilter!=null && typeof(GM.Global.myFilter)!="object")
                GM.Global.myFilter = $.parseJSON(GM.Global.myFilter);
            
            
            var FilterJson = GM.Global.myFilter;
                if(FilterJson!=null){
                    $("#div-module-main #input-add-favorite").val(FilterJson.Name);
                    that.filterID = FilterJson.ID;
                    GM.Global.myFilter = null;
                }
        },
        getCriteriaData: function (criteriaid) {
            var that = this;
            this.criteriaID = criteriaid;
            var tempArray = new Array();
            var input = {"token": GM.Global.Token, "criteriaid": this.criteriaID};
            fnGetWebServerData("criteria/get", undefined, input, function(data){

                data.listGmCriteriaAttrVO = arrayOf(data.listGmCriteriaAttrVO);
                GM.Global.criteriaAttr = [];
                GM.Global.criteriaAttr = data.listGmCriteriaAttrVO;
                that.criterianm = data.criterianm;
                _.each(data.listGmCriteriaAttrVO, function (data) {
                    
                    if (data.key == "crtfrmdate") {
                         that.frmdate = data.value;
                         that.tempJSON["From Date"] = that.frmdate;
                     }
                     if (data.key == "crttodate") {
                         that.todate = data.value;
                         that.tempJSON["To Date"] = that.todate;
                     }
                    if (data.key == "actfrmdate") {
                        that.frmdate = data.value;
                        that.tempJSON["From Date"] = that.frmdate;
                    }
                    if (data.key == "acttodate") {
                        that.todate = data.value;
                        that.tempJSON["To Date"] = that.todate;
                    }

                    if (data.key == "systemid") {
                        that.productsList.push(data.value);
                    }

                    if (data.key == "state") {
                        that.stateList.push(data.value);
                    }
                    if (data.key == "procedure") {
                        that.techqList.push(data.value);
                    }
                    if (data.key == "specality") {
                        that.skillList.push(data.value);
                    }
                    if (data.key == "acttyp") {
                        that.activityList.push(data.value);
                    }
                    if (data.key == "acttypeid") {
                        that.activityList.push(data.value);
                    }
                    if (data.key == "role") {
                        that.roleList.push(data.value);
                    }
                    that.newArray = _.union(that.stateList, that.techqList, that.skillList, that.activityList, that.roleList);
                });
            if(that.newArray != "")
                that.getCodeName(that.newArray);
            else if(that.productsList != "")
                that.getSystemName(that.productsList);
            else
                that.popUpTemplate();
            });
        },
                
        // Edit favoriter selected data
        editFavorite: function () {
            var that = this;
            console.log(that.filterID);
            if(that.filterID != "")
                window.location.href = "#"+GM.Global.Module+"/"+"filter"+"/"+that.filterID;
            else
                window.location.href = "#"+GM.Global.Module+"/"+"filter"+"/"+that.criteriaID;
        },

        // Save as favorite current view list
        addFavorite: function (event) {
            var that =this;
            var criterianm
            that.jsnData = {};
            
            criterianm = $("#input-add-favorite").val();
            
            if (criterianm != "") {
                that.jsnData.criteriatyp = GM.Global.Module;
                that.jsnData.token =  GM.Global.Token;
                that.jsnData.criterianm = criterianm;
                if(that.filterID!="")
                    that.jsnData.criteriaid = that.filterID;
                var key, value;
                $.each(this.favoriteData, function (key, value) {
                    if(value != "" && key != "token")
                        that.arrAttribute.push({"key": key, "value": value, "attrid": "", "criteriaid": that.jsnData.criteriaid, "voidfl": ""});
                });
                
                var temp1 = [];
                _.each(GM.Global.criteriaAttr, function(data){
                    
                    var chk = "";
                    _.each(that.arrAttribute, function(dt, i){
                        if(dt.key == data.key){
                            dt.attrid = data.attrid;
                            chk = "Y";
                        }
                    });
                    if(chk == ""){
                        data.voidfl = "Y";
                        temp1.push(data);
                    }
                    
                    delete data.cmpid;
                    delete data.cmptzone;
                    delete data.cmpdfmt;
                    delete data.plantid;
                    delete data.partyid;
                });
                if(temp1.length){
                    _.each(temp1, function(data){
                        that.arrAttribute.push(data);
                    });
                }  
                that.jsnData.criteriatyp = "fieldsales";
                that.jsnData.token =  GM.Global.Token;
                that.jsnData.listGmCriteriaAttrVO = $.parseJSON(JSON.stringify(that.arrAttribute));
                fnGetWebServerData("criteria/save", undefined, that.jsnData, function (data) {
                    if (data != null) {
                        $(".li-exist-favorite").removeClass("hide");
                        $(".li-edit-favorite").removeClass("hide");
                        $(".li-new-favorite").addClass("hide");
                        $("#div-favorite-title").html(data.criterianm);
                        showSuccess("Favorite is added successfully");
                        hideMessages();
                    } else
                        showError("Favorite save is failed!");
                });
            } else
                showError("Please enter the Favorite Name");
        },

        // To Get system name for to process data in popup template
        getSystemName: function (sysid) {
            var that = this;
            var sysid = sysid.toString();
            var systemInput = { "token":  GM.Global.Token, "sysid": sysid };
            fnGetWebServerData("search/system", "gmSystemBasicVO", systemInput, function (data) {
                data = arrayOf(data);
                if (sysid != undefined) {
                    var systemList = _.pluck(data, "sysnm");
                    var systemID = _.pluck(data, "sysid");
                    that.tempJSON["Product Used"] = systemList.toString().replace(/,/g," / ");

                    if (systemList != null) {
                        var countVal = systemList.length;
                        that.defaultVals = systemID;
                        $(".div-surg-popup-system").attr("data-values", that.defaultVals);
                        $(".div-surg-popup-system").find("span").text("(" + countVal + ")");
                        $(".div-surg-popup-system").css("background", "#FDF5E6");
                    }
                }
                that.popUpTemplate();
            });          
        },

        // get CodeLookUp names for to process data in popup template
        getCodeName: function (codeid) {
            var that = this;
            var codeid = codeid.toString();
            if(codeid != "") {
                var input = { "token":  GM.Global.Token, "codeid": codeid };
                fnGetWebServerData("search/codelookup", "gmCodeLookUpVO", input, function (data) {

                    data = arrayOf(data);
                    if (that.stateList != "") {
                        var stateResult = _.filter(data, function (data) {
                            return data.codegrp == "STATE";
                        });
                        var stateList = _.pluck(arrayOf(stateResult), "codenm");
                        that.tempJSON["State"] = stateList.toString().replace(/,/g," / ");

                    }
                    if (that.techqList != "") {
                        var techqResult = _.filter(data, function (data) {
                            return data.codegrp == "TECHQ";
                        });
                        var techqList = _.pluck(arrayOf(techqResult), "codenm");
                        that.tempJSON["Technique"] = techqList.toString().replace(/,/g," / ");
                    }
                    if (that.skillList != "") {
                        var specalityResult = _.filter(data, function (data) {
                            return data.codegrp == "SGSPC";
                        });
                        var specalityList = _.pluck(arrayOf(specalityResult), "codenm");
                        that.tempJSON["Skillset"] = specalityList.toString().replace(/,/g," / ");
                    }
                    if (that.roleList != "") {
                        var roleResult = _.filter(data, function (data) {
                            return data.codegrp == "ACTROL";
                        });
                        var roleList = _.pluck(arrayOf(roleResult), "codenm");
                        that.tempJSON["Role"] = roleList.toString().replace(/,/g," / ");
                    }
                    if (that.activityList != "") {
                        var acttypResult = _.filter(data, function (data) {
                            return data.codegrp == "SGACTP";
                        });
                        var acttypList = _.pluck(arrayOf(acttypResult), "codenm");
                        that.tempJSON["Activity Type"] = acttypList.toString().replace(/,/g," / ");
                    }
                    if (that.productsList != ""){
                        that.getSystemName(that.productsList);
                    }
                    else {
                        that.popUpTemplate();
                    }
                }); 
            }
            else if (that.productsList != ""){
                that.getSystemName(that.productsList);
            }
        },
        showItemListView: function(controlID, data, itemtemplate, columnheader, sortoptions, template_URL, filterOptions, csvFileName, color, callbackOnReRender) {
			data = arrayOf(data)
			for(var i=0;i<data.length;i++) {         // This Code is put back to avoid writing it's web service for all lists
				var row = data[i];
                data[i].search = "";
				if(this.search && this.search.length){
                    _.each(this.search, function(srh){
                        data[i].search += row[srh] + " ";
                    })
                }
			}
			var items = new GMCItem(data);	
			this.itemlistview = new GMVItemList({ 
				el: controlID, 
				collection: items,
				columnheader: columnheader, 
				itemtemplate: itemtemplate,
				sortoptions: sortoptions,
				template_URL: template_URL,
				filterOptions: filterOptions,
                csvFileName: csvFileName,
                color: color,
                callbackOnReRender : callbackOnReRender
			});
            

		},

        // To show and append selected criteria data in the template
        popUpTemplate: function () {
            var that = this;
            var tempArray = new Array();
            $.each(that.tempJSON, function (key, val) {
                if (val != "") {
                    tempArray.push({ "title": key, "value": val });
                }
            });
            var criteriaValues = $.parseJSON(JSON.stringify(tempArray));
            $(".div-category-items").html(that.template_selected_items(criteriaValues));
            $(".div-text-selected-crt").addClass("gmFont"+GM.Global.Module);
            $(".li-new-favorite").addClass("hide");
            $(".li-exist-favorite").removeClass("hide");
            $(".li-edit-favorite").removeClass("hide");
            $("#div-favorite-title").html(that.criterianm);
        },
        
        // Toggle slide for to create surgoen activity from surgeon list 
        createSurActivity: function (e) {
            console.log(e.currentTarget);
            if($(e.currentTarget).hasClass("active-tab")) {
                $(e.currentTarget).removeClass("active-tab");
                $(".li-surg-create-activity").addClass("hide");
            }
            else {
                $(".active-tab").removeClass("active-tab");
                $(".li-surg-create-activity").addClass("hide");
                $(e.currentTarget).addClass("active-tab");
                $(e.currentTarget).parent().find(".li-surg-create-activity").removeClass("hide"); 
            }
        },

	});

	return GM.View.FieldSalesData;
});
