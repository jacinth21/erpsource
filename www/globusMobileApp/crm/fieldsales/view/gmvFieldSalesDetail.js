/********************************************************************************************************************
 * File:                gmvFieldSalesDetail.js
 * Description:         View which loads the view with all activities of Sales Rep
 * Versionkey:          1.0
 * Author:              jgurunathan	 
 ********************************************************************************************************************/
define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars', 'jqueryui',

    // Common
    'global', 'gmvSearch',

    // View
    'gmvCRMData', 'gmvCRMSelectPopup', 'gmvFieldSalesData',

    // Model
    'gmmActivity',

    // Pre-Compiled Template
    'gmtTemplate'

],

    function ($, _, Backbone, Handlebars, JqueryUI,
        Global, GMVSearch, 
        GMVCRMData, GMVCRMSelectPopup, GMVFieldSalesData,
        GMMActivity, GMTTemplate) {

        'use strict';

        GM.View.FieldSalesDetail = Backbone.View.extend({

            el: "#div-crm-module",

            name: "CRM FieldSales Detail View",

            /* Load the templates */
            template_MainModule: fnGetTemplate(URL_Common_Template, "gmtCRMMainModule"),
            template_FieldSalesDetail: fnGetTemplate(URL_FieldSales_Template, "gmtFieldSalesDetail"),
            template_FilterButtons: fnGetTemplate(URL_Common_Template, "gmtFilterButtons"),
            
            events: {
                "click  .btn-listby": "openPopup",
                "click .gmRepTab li" : "showModule",
                "click .fieldsales-tab" : "toogleTabs",
            },

            initialize: function (options) {
                console.log(options);
                var that = this;
                this.repid = options.repid;
                this.userTabs = _.pluck($.parseJSON(localStorage.getItem('userTabs')), "functionID");
                $("#div-app-title").show();
                $("#span-app-title").html("Field Sales");
                this.tabAccess = fnGetAccessInfo("FIELDSALES");
                if(GM.Global.FSViews && GM.Global.FSViews.length){
                    _.each(GM.Global.FSViews, function(v){
                        that.close(v);
                    })
                }
                GM.Global.FSViews = [];
                hideMessages();
                this.productsList = new Array();
            },

            render: function () {

                var that = this;
                this.$el.html(this.template_MainModule({ "module": "fieldsales" }));
                    if (!GM.Global.FieldSales)
                            GM.Global.FieldSales = {};
               this.$("#li-save").addClass("hide");
                 $(window).on('orientationchange', this.onOrientationChange);
                if (that.repid != 0) { //in case of edit/detail option
                    var token = localStorage.getItem("token");
                    var input = { "token": token, "repid": that.repid };
                    
                    console.log(input);
                    if(GM.Global.Device){
                        console.log(that.repid)
                        fnGetFSRepInfo(that.repid, function(data){
                            console.log("FSDATA")
                                console.log(data)
                            if(data.length){
                                data = data[0];
                                that.fieldSalesData(data);
                                fnGetCRMDocumentDetail(that.repid, 103112, function(fileData){
                                    console.log("fnGetCRMDocumentDetail fileData" + JSON.stringify(fileData));
                                  if (fileData.length){
                                      fileData = fileData[0];
                                      var url = GM.Global.LocalFilePath+"/"+fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename;
                                      console.log(url);
                                      $(".div-rep-pic-container").find("img").attr("src",url);
                                   }
                                });
                            }
                        });
                    }
                    else{
                        fnGetWebServerData("fieldsales/repinfo", "gmCRMSalesRepListVO", input, function (data) { //obtaining fieldsales data from server
                            if(data!=null){
                                console.log("FSDATA")
                                console.log(data)
                                that.fieldSalesData(data);
                                var userID = data.repid;
                                getImageDoc(userID, function (data){
                                    console.log("getImageDoc" + JSON.stringify(data));
                                    $(".div-rep-pic-container").find("img").attr("src",data);
                                });
                            }
                        });
                    }
                }
                     
            },
            
            fieldSalesData: function(data){
                var that = this;
                console.log("Sales Rep Info is " + JSON.stringify(data));
                GM.Global.FieldSales.RepPartyID=data.partyid;
                that.$("#div-main-content").html(that.template_FieldSalesDetail(data));
                $(".gmPanelTitle").html(data.firstnm + (data.midinitial ? (" " + data.midinitial) : "") + " " + data.lastnm);
                var today = new Date();
                today.setDate(today.getDate() - salescallDays);
                var dd = today.getDate();
                var mm = today.getMonth()+1; //January is 0!
                var yyyy = today.getFullYear();
                if(dd<10) { dd='0'+dd }
                if(mm<10) { mm='0'+mm }
                today = mm+'/'+dd+'/'+yyyy;
                var dbtoday = yyyy+'/'+mm+'/'+dd;
                GM.Global.Today=today;
                console.log(GM.Global.FieldSales.RepPartyID);
                var surgeoninput={"token":localStorage.getItem("token"),"repid":GM.Global.FieldSales.RepPartyID}
                var scinput={"token":localStorage.getItem("token"),"partyid":GM.Global.FieldSales.RepPartyID,"actcategory": "105267" ,"actstartdate":today}
                var pvinput={"token":localStorage.getItem("token"),"actcategory": "105266","actstartdate":today,actstatus: "105761"}
                console.log(surgeoninput);
                var leadinput={"token":localStorage.getItem("token"),"partyid":GM.Global.FieldSales.RepPartyID,"actcategory": "105269"}
                    console.log(surgeoninput);
                if(GM.Global.Device){
                    fnGetMySurgeon(data.repid, GM.Global.FieldSales.RepPartyID, function(scData){
                        console.log(scData)
                        if(scData.length)
                            $("#span-surgcount").html(scData.length);
                        else
                            $("#span-surgcount").html(0);
                        var gmvFieldSalesData = new GMVFieldSalesData({ "module": "surgeon", "repid": data.partyid, "info": scData});
                        GM.Global.FSViews.push(gmvFieldSalesData)
                    });
                }
                else{
                    fnGetWebServerData("surgeon/info", "gmCRMSurgeonlistVO", surgeoninput, function (sData){
                        if(sData != null){
                            sData=arrayOf(sData);
                            $("#span-surgcount").html(sData.length);
                        }
                        else
                            $("#span-surgcount").html(0);
                        var gmvFieldSalesData = new GMVFieldSalesData({ "module": "surgeon", "repid": data.partyid, "info": sData});
                        GM.Global.FSViews.push(gmvFieldSalesData)

                    }, true);
                }

                    console.log(scinput);
                if(GM.Global.Device){
                    var d = new Date();
                    d.setDate(d.getDate() - salescallDays);
                    var dd = d.getDate();
                    var mm = d.getMonth()+1;
                    var yyyy = d.getFullYear();
                    if(dd<10) { dd='0'+dd }
                    if(mm<10) { mm='0'+mm }
                    that.sctoday = yyyy+'/'+mm+'/'+dd;
                    var ipt = {actcategory : "105267", today : that.sctoday, repid: data.partyid}
                    fnGetListActivity(ipt, "MYLDSC", function(scData){
                        console.log("fnGetMySC"+ JSON.stringify(scData));
                        if(scData.length)
                            $("#span-sccounts").html(scData.length);
                        else
                            $("#span-sccounts").html(0);
                        var gmvFieldSalesData = new GMVFieldSalesData({ "module": "salescall", "repid": data.partyid, "info": scData});
                        GM.Global.FSViews.push(gmvFieldSalesData)
                    });
                }
                else{
                    fnGetWebServerData("crmactivity/info", "gmCRMActivityVO", scinput, function (scData){
                      if(scData != null){
                            scData=arrayOf(scData);
                            $("#span-sccounts").html(scData.length);
                      }
                        else
                            $("#span-sccounts").html(0);
                        var gmvFieldSalesData = new GMVFieldSalesData({ "module": "salescall", "repid": data.partyid, "info": scData});
                        GM.Global.FSViews.push(gmvFieldSalesData)
                    }, true);
                }

                     console.log(pvinput);
                if(GM.Global.Device){
                    var setList = {};
                    setList.today = dbtoday;
                    setList.actcategory = '105266';
                    setList.actstat = '105761';
                    fnGetListActivity(setList,"UPC", function(pvData){
                        console.log(pvData)
                        if(pvData.length)
                            $("#span-pvcount").html(pvData.length);
                        else
                            $("#span-pvcount").html(0);
                        var gmvFieldSalesData = new GMVFieldSalesData({ "module": "physicianvisit", "repid": data.partyid, "info": pvData});
                        GM.Global.FSViews.push(gmvFieldSalesData)
                    });
                }
                else{
                    fnGetWebServerData("crmactivity/info", "gmCRMActivityVO", pvinput, function (pvData){
                      if(pvData != null){
                            pvData=arrayOf(pvData);
                            $("#span-pvcount").html(pvData.length);
                      }
                        else
                            $("#span-pvcount").html(0);
                        var gmvFieldSalesData = new GMVFieldSalesData({ "module": "physicianvisit", "repid": data.partyid, "info": pvData});
                        GM.Global.FSViews.push(gmvFieldSalesData)
                    }, true);
                }
                if(GM.Global.Device){
                    fnGetMyLead(GM.Global.FieldSales.RepPartyID, function(leadData){
                        console.log(leadData)
                        if(leadData.length)
                            $("#span-leadcount").html(leadData.length);
                        else
                            $("#span-leadcount").html(0);
                        var gmvFieldSalesData = new GMVFieldSalesData({ "module": "lead", "repid": data.partyid, "info": leadData});
                        GM.Global.FSViews.push(gmvFieldSalesData)
                    });
                }
                else{
                    fnGetWebServerData("crmactivity/info", "gmCRMActivityVO", leadinput, function (leadData){
                      if(leadData != null){
                            leadData = arrayOf(leadData);
                            $("#span-leadcount").html(leadData.length);
                      }
                        else
                            $("#span-leadcount").html(0);
                        var gmvFieldSalesData = new GMVFieldSalesData({ "module": "lead", "repid": data.partyid, "info": leadData});
                        GM.Global.FSViews.push(gmvFieldSalesData)
                    }, true);
                }
                this.$("#li-save").addClass("hide");
                $(".div-rep-module").addClass("gmMediaDisplayNone");
               
            },

            showModule: function(e) {
                var that = this;
                var modulename = $(e.currentTarget).attr("name");
//                var gmvFieldSalesData = new GMVFieldSalesData({ module: modulename, repid: that.repid});
                $(".div-rep-module").addClass("hide");
                $("#div-rep-module-"+modulename).removeClass("hide");
                
            
            },
             onOrientationChange: function(event) {
                resizeHeadWidth('.gmPanelHead', '.gmBtnGroup', '#panelhead-left');
            },
            toogleTabs: function(e) {
//                colorControl(e, "fieldsales-tab", "gmBGLGrey", "gmBGDarkerGrey gmFontWhite");
                var target = $(e.currentTarget).attr("name");
               if ($(e.currentTarget).hasClass("opened")) {
                   $(".div-rep-module").addClass("gmMediaDisplayNone");
                    $(".opened .fa-chevron-right").removeClass("hide");
                    $(".opened .fa-chevron-left ").addClass("hide");
                  $(e.currentTarget).removeClass("opened");
                   $(".fieldsales-tab").removeClass("gmMediaDisplayNone");
                   $(".ul-fs-datail-top").removeClass("gmMediaDisplayNone");
                   $("#div-rep-module").addClass("gmMediaDisplayNone");
               }
                else{
                    $(".div-rep-module").removeClass("gmMediaDisplayNone");
                   $(e.currentTarget).addClass("opened");
                    $(".fieldsales-tab").addClass("gmMediaDisplayNone");
                    $(e.currentTarget).removeClass("gmMediaDisplayNone");
                    $(".ul-fs-datail-top").addClass("gmMediaDisplayNone");
                    $("#div-rep-module").removeClass("gmMediaDisplayNone");
                    $(".opened .fa-chevron-right").addClass("hide");
                    $(".opened .fa-chevron-left ").removeClass("hide");
                    
                }
                
            
            },

            backToPage: function () {
                if (this.repid == 0)
                    window.location.href = "#crm/fieldsales";
                else {
                    Backbone.history.loadUrl("#crm/fieldsales/id/" + this.repid);
                }

            },
            
            close: function(view){
                view.unbind();
                view.undelegateEvents();
                view = undefined;
            }

        });
        return GM.View.FieldSalesDetail;
    });