define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars', 'jqueryui',

    // Common
    'global',
    
    // View
    'gmvCRMData','gmvCRMSelectPopup',

    // Pre-Compiled Template
    'gmtTemplate'
    
    //Model
    
], 

function ($, _, Backbone, Handlebars, JqueryUI,
           Global,
           GMVCRMData, GMVCRMSelectPopup, GMTTemplate) {

    'use strict'; 
    
    GM.View.FieldSalesCriteria = Backbone.View.extend({

        el: "#div-crm-module",

        name: "CRM FieldSalesCriteria View",

        /* Load the templates */ 
        template_MainModule: fnGetTemplate(URL_Common_Template, "gmtCRMMainModule"),
        template_FieldSalesCriteria: fnGetTemplate(URL_FieldSales_Template, "gmtFieldSalesCriteria"),
        template_FilterButtons: fnGetTemplate(URL_Common_Template, "gmtFilterButtons"),

        events: {
            "click .div-upcum-salescall-btn": "showUpcoming",
            "click #btn-search-repid": "lookupByRepID",
            "click #btn-search-name": "lookupByName",
            "keypress input": "clickDefaultButton",
            "click  .btn-listby": "openPopup",
            "click .sub-tab"                                      : "toggleTabs",
            "click #btn-category-search": "searchCategory",
            "click .gmBtnCrDelete": "removeFavorite",
             "click .div-text-selected-crt":  "toggleSelectedItems"
            
        },
        
        initialize: function() {
            GM.Global.FieldSales = {};
            GM.Global.FieldSalesID = null;
            GM.Global.FieldSalesCriteria = null;
            GM.Global.FieldSales = null;
            GM.Global.myFilter = null;
            console.log("Initialized");
            GM.Global.FieldSalesRegion = null;
        },

        render: function() {
            var that = this;
            $("#div-app-title").show();
            $(window).on('orientationchange', this.onOrientationChange);
            $("#span-app-title").html("Field Sales");
            //console.log("UserTabs " + JSON.stringify(GM.Global.UserTabs));
            this.$el.html(this.template_MainModule({"module": "fieldsales"}));
            this.$("#div-main-content").html(this.template_FieldSalesCriteria({userID: localStorage.getItem("userID")}));
            this.$(".gmPanelTitle").html("Lookup Field Sales");
            this.$("#crm-btn-back").hide();
            if(GM.Global.Device){
                fnGetCriteria('fieldsales', function(data){
                    if(data.length){
                        that.$(".gmMyFav li").removeClass('hide');
                        that.$(".gmMyFavContent").html(that.template_FilterButtons(data));
                        that.$(".gmBtnCr").addClass("gmBGfieldsales");
                    }
                    else
                        that.$(".gmMyFav").addClass('hide');
                });
            }
            else{
                var input = {"token":localStorage.getItem("token"), "criteriatyp": "fieldsales"};
                console.log(JSON.stringify(input));
                fnGetWebServerData("criteria/list", undefined, input, function(data){
                    if(data!=null) {
                        that.$(".gmMyFav li").removeClass('hide');
                        that.$(".gmSalesCallFav").html(that.template_FilterButtons(arrayOf(data.gmCriteriaVO)));
                        that.$(".gmBtnCr").addClass("gmBGfieldsales");
                    }
                    else
                        that.$("#li-fieldsales-criteria-filter-content").addClass('hide');
                });
            }
            
            
            this.toggleTabs({"currentTarget":"#tab-npiname"});

            $("#btn-category-search").hide();
        },

        
        //option to load data on Enter key
        clickDefaultButton: function(event) {
            var that = this;
            if(event.keyCode==13) {
                var txtInput = $(event.target).attr('data-id');
                
                console.log(txtInput);
                switch(txtInput) {
                    case "repid":
                        that.lookupByRepID(event);
                        break;
                    case "fieldsales-fname":
                    case "fieldsales-lname":
                        that.lookupByName(event);
                        break;
                }    
            }
        },

        // Lookup Surgeon Details by providing NPI
        lookupByRepID: function(event) {
            var repid = $(event.currentTarget).parent().find(".fieldsales-repid").val();
            if(repid !=""){if($.isNumeric(repid)){
                if(GM.Global.Device){
                    fnGetFSRepInfo(repid, function(data){
                        console.log("FSDATA")
                            console.log(data)
                        if(data.length){
                            window.location.href = "#crm/fieldsales/id/" + repid;
                        }
                        else{
                            showError("Enter valid RepID");
                            $(event.currentTarget).parent().find(".fieldsales-repid").val("");
                        }
                    });
                }
                else{
                    var input = { "token": localStorage.getItem("token"), "repid": repid };
                    fnGetWebServerData("fieldsales/repinfo", "gmCRMSalesRepListVO", input, function (data) { //obtaining fieldsales data from server
                        console.log(data);
                        if(data!=null)
                            window.location.href = "#crm/fieldsales/id/" + repid;
                        else{
                            showError("Enter valid RepID");
                            $(event.currentTarget).parent().find(".fieldsales-repid").val("");
                        }

                    });
                }
            }
            else
                showError("Rep ID should be Numeric!")}
        },

        // Lookup Sales Rep by providing Rep's First or Last Name
        lookupByName: function(event) {

            var firstnm = $(event.currentTarget).parent().find(".fieldsales-fname").val();
            var lastnm = $(event.currentTarget).parent().find(".fieldsales-lname").val();
            if(firstnm != ""|| lastnm !="")
           { GM.Global.FieldSales = { firstnm: firstnm, lastnm: lastnm };

            firstnm = (firstnm !== "") ? firstnm : "undefined";
            lastnm = (lastnm !== "") ? lastnm : "undefined";
            
            window.location.href = "#crm/fieldsales/name/" + firstnm + "/" + lastnm;}    

        },
        onOrientationChange: function(event) {
                resizeHeadWidth('.gmPanelHead', '.gmBtnGroup', '#panelhead-left');
            },
        
        // Open Single / Multi Select box        
        openPopup: function(event){
            var that = this;
            var fnName = "";
            if($(event.currentTarget).attr("data-title") == "Region")
                fnName = fnGetFSRegion;
            else
                fnName = fnGetFSDistributor;
            that.systemSelectOptions = {                                                                                                              
                'title': $(event.currentTarget).attr("data-title"),
                'storagekey': $(event.currentTarget).attr("data-storagekey"),
                'webservice': $(event.currentTarget).attr("data-webservice"),   
                'resultVO': $(event.currentTarget).attr("data-resultVO"),
                'IDParameter': $(event.currentTarget).attr("data-IDParameter"),
                'NameParameter': $(event.currentTarget).attr("data-NameParameter"),
                'inputParameters': $(event.currentTarget).attr("data-inputParameters"),
                'module': "fieldsales",
                'event': event,
                'parent': this,
                'moduleLS': GM.Global.FieldSalesID,
                'moduleCLS': GM.Global.FieldSalesCriteria,
                'fnName': fnName,
                'callback': function(ID, Name){
                    GM.Global.FieldSalesID = ID;
                    GM.Global.FieldSalesCriteria = Name;
                    console.log(ID);
                    console.log(Name)
                }
            };
            
            that.systemSelectView = new GMVCRMSelectPopup(that.systemSelectOptions);
        },
           // Lookup SalesCalls by Filters
        searchCategory: function (event) {
            
           
            GM.Global.FieldSalesID = $.parseJSON(GM.Global.FieldSalesID);
            GM.Global.FieldSalesCriteria = $.parseJSON(GM.Global.FieldSalesCriteria);
            if(GM.Global.FieldSales==undefined)
                GM.Global.FieldSales = {"Filter":""};
            GM.Global.FieldSales["FilterID"] = GM.Global.FieldSalesID;
            
            
            GM.Global.FieldSales["FilterName"] = GM.Global.FieldSalesCriteria;
                window.location.href = '#crm/fieldsales/list/0';

        },
        
        // Delete the favorite item from the criteria page
        removeFavorite: function(e){
            showNativeConfirm("Do you want to delete this favorite","Confirm",["Yes","No"], function(index) {
                if (index == 1) {
                    var input = {};
                    input.token = localStorage.getItem('token');
                    input.criteriaid = $(e.currentTarget).attr('criteriaid');
                    input.criterianm = $(e.currentTarget).parent().find('a').text();
                    input.criteriatyp = "fieldsales";
                    input.voidfl = "Y";
                    input.listGmCriteriaAttrVO = [];
                    console.log(input);
                    fnGetWebServerData("criteria/save", undefined, input, function(data){
                        if(data!=null) {
                            $(e.currentTarget).parent().parent().remove();
                        }
                    });
                }
            });
            
        },
        toggleSelectedItems: function (event) {
            if ($("#criteria-selected-itmes").hasClass("gmMediaDisplayNone")) {
                $("#criteria-selected-itmes").removeClass("gmMediaDisplayNone");
                $("#selected-criteria-icon").find(".fa-chevron-down").addClass("fa-chevron-up").removeClass("fa-chevron-down");
            } else {
                $("#criteria-selected-itmes").addClass("gmMediaDisplayNone");
                $("#selected-criteria-icon").find(".fa-chevron-up").addClass("fa-chevron-down").removeClass("fa-chevron-up");
            }
        },
 toggleTabs: function (event) {
            colorControl(event, "sub-tab", "gmBGEmpty", "gmBGDarkerGrey gmFontWhite");
            var target = $(event.currentTarget).attr("tabname");
                if (target == "search-tab-criteria") {
                    $(".search-criteria-content").removeClass("gmMediaDisplayNone");
                    $(".favorite-criteria-content").addClass("gmMediaDisplayNone");
                    $(".filter-criteria-content").addClass("gmMediaDisplayNone");
                } else if (target == "filter-tab-criteria") {
                    $(".filter-criteria-content").removeClass("gmMediaDisplayNone");
                    $(".search-criteria-content").addClass("gmMediaDisplayNone");
                    $(".favorite-criteria-content").addClass("gmMediaDisplayNone");
                } else if (target == "favorite-tab-criteria") {
                    $(".favorite-criteria-content").removeClass("gmMediaDisplayNone");
                    $(".search-criteria-content").addClass("gmMediaDisplayNone");
                    $(".filter-criteria-content").addClass("gmMediaDisplayNone");
                }
            }

    });
    return GM.View.FieldSalesCriteria;
}); 
