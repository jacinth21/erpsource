/**********************************************************************************
 * File:        gmrFieldSales.js
 * Description: Router for all of Field Sales
 * Version:     1.0
 * Author:      cskumar
 **********************************************************************************/
define([

  // External Libraries
  'jquery', 'underscore', 'backbone',

  //Common
  'gmvCRMMain',
  
  //Views
  'gmvCRMData', 'gmvFieldSalesCriteria', 'gmvFieldSalesDetail', 'gmvFieldSalesFilter', 'gmvCRMMap'
], 
    
function ($, _, Backbone,
           GMVCRMMain, GMVCRMData, GMVFieldSalesCriteria, GMVFieldSalesDetail, GMVFieldSalesFilter, GMVCRMMap) {

    'use strict';
    
    GM.Router.FieldSales = Backbone.Router.extend({
        
        // routes to its respective web page 
        routes: {
            "crm/fieldsales"                                     : "viewFieldSales",
            "crm/fieldsales/filter/:filterID"                     : "showFilter",
//            "crm/fieldsales/list/:criteriaID"                     : "listFieldSales",
            "crm/fieldsales/id/:repid"                           : "fieldsalesDetail",
            "crm/fieldsales/name/:fieldsalesFName/:fieldsalesLName" : "listFieldSalesbyName",
            "crm/fieldsales/map/:repID"                          : "showMap",
            "crm/fieldsales/list/region"                          : "showListbyRegion",
            "crm/fieldsales/list/:regID"                          : "showListbyRegion"
            
        },
                        
        initialize: function() {
            GM.Global.FieldSales = {};
            GM.Global.FieldSales.Module = null;
            console.log("Initialized Global FieldSales");
          $("#div-app-title").show();
            $("#span-app-title").html("Field Sales");
        },

        // things to execute before the routing
        execute: function(callback, args, name) {
            console.log(GM.Global.CRMMain)
            if(GM.Global.CRMMain==undefined){
                    commonSetUp();
                    GM.Global.Module = "fieldsales"
                    GM.Global.CRMMain = true;
                    var gmvCRMMain = new GMVCRMMain();
                    gmvApp.showView(gmvCRMMain);
                    
            }
            $('.gmTab').removeClass().addClass('gmTab');
//            var color = $(e.currentTarget).attr("color");
            $("#fieldsales").addClass("tabfieldsales gmFontWhite");
                
            if(GM.Global.AlertMsg){
                showError(GM.Global.AlertMsg);
                GM.Global.AlertMsg = undefined;
            }
            else{
                toErr = setTimeout(function() {
                    hideMessages();
                }, 2500)   
            }
            if (callback) callback.apply(this, args);
        },
        

        // Show Field Sales Details by RepID
        fieldsalesDetail: function(repid) {
            console.log(repid);
           
            
            var gmvFieldSalesDetail = new GMVFieldSalesDetail({repid: repid});
            gmvApp.showView(gmvFieldSalesDetail);
        },

        // List Field salesa by First Name and/or Last Name
        listFieldSalesbyName: function(){
          var gmvCRMData = new GMVCRMData({module: "fieldsales"});
          gmvApp.showView(gmvCRMData);
          
        },

        // List Region by Reps
        showListbyRegion: function(regid){
            console.log(regid);
            
            console.log(GM.Global.FieldSales);
            if(regid!=null){
                GM.Global.FieldSalesRegion = "N"
                if(GM.Global.FieldSales == undefined){
                    GM.Global.FieldSales = {};
                }
                else {
                    GM.Global.FieldSales["regid"] = regid;
                } 
            }
            else{
                GM.Global.FieldSalesRegion = "Y";
            }
            
            
          var gmvCRMData = new GMVCRMData({module: "fieldsales", regid: regid});
          gmvApp.showView(gmvCRMData);
        },
        
        // List Physicianvisits by selected filter values
        listFieldSales: function(criteriaid) {
            if(GM.Global.FieldSales == undefined){
                GM.Global.FieldSales = {}
                GM.Global.FieldSales.Filter = null;
            }
            if (GM.Global.SalesRep)
                GM.Global.FieldSales["criteriaid"] = "0";
            else
                GM.Global.FieldSales["criteriaid"] = criteriaid;
            
            var gmvCRMData = new GMVCRMData({ "module": "fieldsales"});
            gmvApp.showView(gmvCRMData);  
            
        },

        //  View Field Sales
        viewFieldSales: function() {
            var gmvFieldSalesCriteria = new GMVFieldSalesCriteria();
            gmvApp.showView(gmvFieldSalesCriteria);
        },
        
        showFilter: function(FilterID) {
            var gmvFieldSalesFilter = new GMVFieldSalesFilter({filterid: FilterID});
            gmvApp.showView(gmvFieldSalesFilter);
            console.log(gmvApp)
        },

        //to open and display the map to locate reps
        showMap: function(repID) {
            $(".gmPanelTitle").html("Field Sales Map View");
//            if(repID!=0)
//                $("#li-list").removeClass("hide"); 
            
            var gmvCRMMap = new GMVCRMMap({"el": $(".gmPanelContent"), "repID" : repID});
            gmvApp.showView(gmvCRMMap);
        }
  });
    return GM.Router.FieldSales;
});
