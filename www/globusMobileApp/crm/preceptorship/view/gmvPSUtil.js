/**********************************************************************************
 * File             :        gmvPSUtil.js
 * Description      :        View to represent Common Functions of Preceptorship
 * Path             :        /web/GlobusCRM/crm/preceptorship/view/gmvPSUtil.js
 * Author           :        mahendrand
 **********************************************************************************/

//Load Spinner
function getLoader(divElem) {
    var template = fnGetTemplate(URL_Common_Template, "gmtSpinner");
    $(divElem).html(template());
}

//Load Spinner Icon Without Loader Text
function getLoaderIcon(divElem) {
    $(divElem).html('<img class="img-spinner gmText25" src="globusMobileApp/image/icon/ios/spinner.gif">');
}

//To Show DO List In Popup on clicking the system dataplot in the chart and shows the popup and add the class popupDOList
function showPopupPSReport() {
    var that = this;
    $('.div-overlay').show();
    $('.scheduler-main-container').attr('style', 'z-index:0');
    $('.div-crm-overlay-content-container').addClass('hide');
    $('#div-crm-overlay-content-container').addClass('popupHostList');
    $('#div-crm-overlay-content-container').removeClass('hide');  
    if($('#div-crm-overlay-content-container .modal-footer').hasClass("hide"))
        $('#div-crm-overlay-content-container .modal-footer').removeClass('hide');  
    $( "#div-crm-overlay-content-container .modal-footer button" ).each(function( i ) {
        if(!$(this).hasClass("hide"))
            $(this).addClass("hide");
    });

    $('#div-crm-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hidePopupPSReport();
        });
    });
}

//On clicking the close icon to hide the popup and remove the popupDOList class from the modal popup
function hidePopupPSReport() {
    $('.div-overlay').hide();
    $('.scheduler-main-container').attr('style', '');
    $('#div-crm-overlay-content-container').removeClass('popupHostList');
    $('#div-crm-overlay-content-container').removeClass('popupHostMain');
    $(".load-model").removeClass("hide");
    $('.div-crm-overlay-content-container').addClass('hide');
    $('.hideDefaultBtn').addClass('hide');
    $("#div-crm-overlay-msg").addClass('hide');
}

//validation for npid
function checkNumber(e) {
    if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
        $(e.currentTarget).val("");
    }
}

  // New Method to allow only the valid characters like Numbers, + -,()
   function checkPhoneNumber(e){
                if((e.which == 32 || e.which == 40 || e.which == 41 || e.which == 43 || e.which == 45)||((e.which > 47) && (e.which < 58))){
                        return true;
                }
                else{
                    e.preventDefault();
                }
            }
                
//validation for npid
function checkNPI(e) {
    var that = this;
    var val;
    $("#img-npid-spinner").toggleClass('hide');
    $(e.currentTarget).toggleClass('hide');
    if (this.newSurgeon == "true") {
        val = $("#input-new-surgeon-npid").val();
        this.newSurgeon = "false"
    } else
        val = $("#input-lead-contacts-npid").val();
    var re = /[^0-9]/g;
    var error = re.test(val);
    //            hideMessages();
    if (val != "") {
        if (error == true) { //npid should be numberic and with max length of 10
            $("#popup-create-surgeon-validation").html("NPI Should be Numeric");
            $("#img-npid-spinner").toggleClass('hide');
            $(e.currentTarget).toggleClass('hide');
            $(e.currentTarget).val("").focus();
        } else {
            $("#popup-create-surgeon-validation").html("");
            getSurgeonPartyInfo(val, function (surgeonData) {
                var partyid;
                if (that.newSurgeon == "true")
                    partyid = 0;
                else
                    partyid = GM.Global.SurgeonPartyId; //calling surgeon's party information of typed in npid                                                                                                                                                        //that.chkNPI == 1 already allocated;  that.chkNPI == 0 npid verified

                if (surgeonData != undefined) {
                    surgeonData = surgeonData[0];
                    if (partyid == 0) {
                        $(".npi-detection.fa-check-circle-o").remove();
                        if ($(".npi-detection.fa-times-circle").length == 0)
                            $(".li-popup-npid-create").append("<span class='fa npi-detection fa-times-circle'></span>");
                        that.chkNPI = 1;
                    } else {
                        if (surgeonData.partyid == partyid) {
                            $(".npi-detection.fa-times-circle").remove();
                            if ($(".npi-detection.fa-check-circle-o").length == 0)
                                $(".li-popup-npid-create").append("<span class='fa npi-detection fa-check-circle-o'></span>");
                            that.chkNPI = 0;
                        } else {
                            $(".npi-detection.fa-check-circle-o").remove();
                            if ($(".npi-detection.fa-times-circle").length == 0)
                                $(".li-popup-npid-create").append("<span class='fa npi-detection fa-times-circle'></span>");
                            that.chkNPI = 1;
                        }
                    }
                } else {
                    $(".npi-detection.fa-times-circle").remove();
                    if ($(".npi-detection.fa-check-circle-o").length == 0)
                        $(".li-popup-npid-create").append("<span class='fa npi-detection fa-check-circle-o'></span>");
                    that.chkNPI = 0;
                }
                $("#img-npid-spinner").toggleClass('hide');
                $(e.currentTarget).toggleClass('hide');
            });
        }

    } else { //empty
        $("#popup-create-surgeon-validation").html("");
        if ($(".npi-detection.fa-times-circle").length != 0)
            $(".npi-detection.fa-times-circle").remove();
        if ($(".npi-detection.fa-check-circle-o").length != 0)
            $(".npi-detection.fa-check-circle-o").remove();
        $("#img-npid-spinner").toggleClass('hide');
        $(e.currentTarget).toggleClass('hide');
        that.chkNPI = 0;
    }
}

//Create New Surgeon Function
function popCreateNewSurgeon(template_var,DropDownView) {
    var that = this;
    var newSurgeonValidation = "true";
    showPopup();
    $("#div-crm-overlay-content-container").find('.load-model').addClass("hide");
    $("#div-crm-overlay-content-container").find('.done-model').addClass("hide");
    $("#div-crm-overlay-content-container").find('.save-model').removeClass("hide");
    $("#div-crm-overlay-content-container").addClass("caseNewSurgeonPopup");
    $(".yes-model").addClass("hide");
    $(".no-model").addClass("hide");
    $("#div-crm-overlay-content-container").find('.modal-title').text("Key in a new Surgeon");
    $("#div-crm-overlay-content-container .modal-body").html(template_var);
    
    browseCVFile();
    //getSelectState();
    getCLValToDiv(".select-address-state", "", "", "STATE", "", "", "codeid", "", false, "Y", function (divID, attrVal) {});
    
    //getSelectCountry();
    getCLValToDiv(".select-address-country", "", "", "CNTY", "", "", "codeid", "", false, "Y", function (divID, attrVal) {});
    
    var userData = JSON.parse(localStorage.getItem("userData"));
    //Input for fetching company List for company dropdown
    var input = {
        'token': localStorage.getItem('token'),
        'companyid': '',
        'partyid': localStorage.getItem('partyID')
    };
    var defaultid = userData.cmpid;
    var elemAdmin = "#surg-company-dropdown";

    companyDropdown(input,defaultid,"",DropDownView,elemAdmin,function(selectedId){
        that.companyid = selectedId;
    });
    
    var PSAdmin = _.findWhere(GM.Global.UserAccess, {
           funcnm: "PRECEPTOR-ADMIN"
     });
    //PSadmin - Preceptor Admin access
    if(PSAdmin == undefined){  
        $("#surgeon-label-email").text("*" + " " + "EMAIL");
        $("#input-new-surgeon-email").attr("placeholder","Email*");
        $("#new-surg-cv-file").text("*" + " " + "CV FILE");
    }
    
    $(".caseNewSurgeonPopup .close").on('click', function () {
        $(this).closest("#div-crm-overlay-content-container").removeClass("caseNewSurgeonPopup");
        $(this).closest("#div-crm-overlay-content-container").find(".modal-body").empty();
    });
    
    $("#div-crm-overlay-content-container").find('#input-new-surgeon-npid').unbind('blur').bind('blur', function (e) {
        that.newSurgeon = "true";
        checkNPI(e);
    });
    $("#div-crm-overlay-content-container").find('.input-number-validation').unbind('keyup').bind('keyup', function (e) {
        checkNumber(e);
    });
    $("#div-crm-overlay-content-container").find('.input-phonenumber-validation').unbind('keypress').bind('keypress', function (e) {
        checkPhoneNumber(e);
    });
    $("#div-crm-overlay-content-container").find('.save-model').unbind('click').bind('click', function () {
        if ($("#img-npid-spinner").hasClass('hide')) { // Denying save when NPID is validating

            var str = "";
            var emailPattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
             var numberPattern = new RegExp(/^[0-9- +()]*$/);
            var newSurgNPI = $("#input-new-surgeon-npid").val();
            
            if ($("#input-new-surgeon-firstnm").val().trim() == "")
                str = "Firstname";
            if ($("#input-new-surgeon-lastnm").val().trim() == "")
                str = (str.trim().length) ? (str + " / " + "Lastname") : "Lastname";
            if (that.companyid == "undefined" || that.companyid == "0")
                str = (str.trim().length) ? (str + " / " + "Company") : "Company";
            if (newSurgNPI.trim() != "" && newSurgNPI.length != 10)
                str = (str.trim().length) ? (str + " / " + "NPI should contain numeric characters only and should be 10-digit number") : "NPI should contain numeric characters only and should be 10-digit number";
            if ($("#input-new-surgeon-email").val().trim() != "" && !emailPattern.test($("#input-new-surgeon-email").val().trim()))
                str = (str.trim().length) ? (str + " / " + "Wrong Email Fomat") : "Wrong Email Fomat";
            if (PSAdmin == undefined) {
                if ($("#input-new-surgeon-email").val().trim() == "")
                    str = (str.trim().length) ? (str + " / " + "Email") : "Email";
            }
            var cvFile = $("#txt-fileupload").val();
            if(PSAdmin == undefined && cvFile == ''){
                str = (str.trim().length) ? (str + " / " + "CV File") : "CV File";
            }
            var newSurgPhNo = $("#input-new-surgeon-phone").val();
            if (newSurgPhNo.trim() != "" && !numberPattern.test($("#input-new-surgeon-phone").val().trim()) && newSurgPhNo.length > 25)
                str = (str.trim().length) ? (str + " / " + "Please enter Phone Number and should be 10-digit number") : "Please enter Phone Number and should be 10-digit number";
            var addr = "";
            if ($("#input-new-surgeon-add1").val().trim() != "" || $("#input-new-surgeon-city").val() != "" || $("#div-new-surgeon-content .select-address-state").val().trim() != "" || $("#input-new-surgeon-zip").val().trim() != "" || $("#div-new-surgeon-content .select-address-country").val().trim() != "") {
                var confirm = true;
                if ($("#input-new-surgeon-add1").val().trim() == "")
                    addr = (addr.trim().length) ? (addr + " / " + "Address1") : "Address1";
                if ($("#input-new-surgeon-city").val().trim() == "")
                    addr = (addr.trim().length) ? (addr + " / " + "City") : "City";
                if($("#div-new-surgeon-content .select-address-country").val().trim() == '1101'){
                            if ($("#div-new-surgeon-content .select-address-state").val().trim() == "")
                                addr = (addr.trim().length) ? (addr + " / " + "State") : "State";
                            if ($("#input-new-surgeon-zip").val().trim() == "")
                                addr = (addr.trim().length) ? (addr + " / " + "Zip Code") : "Zip Code";
                            if ($("#input-new-surgeon-zip").val().trim() != "" && $("#input-new-surgeon-zip").val().trim().length > 10)
                                addr = (addr.trim().length) ? (addr + " / " + "Zip can have maximun of 10 characters") : "Zip can have maximun of 10 characters";
                            }
                if ($("#div-new-surgeon-content .select-address-country").val().trim() == "")
                    addr = (addr.trim().length) ? (addr + " / " + "Country") : "Country";
            }
            if (str.length)
                str = (addr.length && addr.length) ? (str + " / " + addr) : str;
            else if (addr.length)
                str = addr;
            var surgstr = " Mandatory Fields: " + str;
            var firstName = $("#input-new-surgeon-firstnm").val().trim();
            var lastName = $("#input-new-surgeon-lastnm").val().trim();
            var midName = $("#input-new-surgeon-midinitial").val().trim();
            if (firstName != "" && lastName != "") {
                var input = {
                    "token": localStorage.getItem("token"),
                    "firstname": firstName,
                    "lastname": lastName,
                    "middleinitial": midName
                };
                fnGetWebServerData("surgeon/info", "gmCRMSurgeonlistVO", input, function (data) {
                    console.log("Surgeon Data >>>>>>>>" + JSON.stringify(data));
                    if (data != undefined && data.length > 0)
                        $("#popup-create-surgeon-validation").text(firstName + " " + lastName + (midName ? (" " + midName) : "") + "'s information already exists");
                    else if (str.length)
                        $("#popup-create-surgeon-validation").text(surgstr);
                    else {
                        var surgeonInput = {
                            "token": localStorage.getItem("token"),
                            "partyid": "",
                            "npid": newSurgNPI,
                            "firstnm": $("#input-new-surgeon-firstnm").val(),
                            "lastnm": $("#input-new-surgeon-lastnm").val(),
                            "partynm": "",
                            "midinitial": $("#input-new-surgeon-midinitial").val(),
                            "asstnm": "",
                            "mngrnm": "",
                            "companyid": that.companyid,

                            "arrgmsurgeonsaddressvo": [],
                            "arrgmsurgeoncontactsvo": [],
                            "arrgmcrmsurgeonaffiliationvo": [],
                            "arrgmcrmsurgeonsurgicalvo": [],
                            "arrgmcrmsurgeonactivityvo": [],
                            "arrgmcrmsurgeonattrvo": [],
                            "arrgmcrmfileuploadvo": []
                        };

                        if (addr == "" && confirm == true) {
                            surgeonInput.arrgmsurgeonsaddressvo.push({
                                "addrtyp": "90400",
                                "addid": "",
                                "add1": $("#input-new-surgeon-add1").val(),
                                "add2": $("#input-new-surgeon-add2").val(),
                                "city": $("#input-new-surgeon-city").val(),
                                "statenm": $(".select-address-state option:selected").text(),
                                "stateid": $(".select-address-state option:selected").val(),
                                "zip": $("#input-new-surgeon-zip").val(),
                                "countrynm": $(".select-address-country option:selected").text(),
                                "countryid": $(".select-address-country option:selected").val(),
                                "primaryfl": "Y"
                            });
                        }
                        
                        if (newSurgPhNo != "") {
                            surgeonInput.arrgmsurgeoncontactsvo.push({
                                "contype": "Home",
                                "conmode": "90450",
                                "convalue": newSurgPhNo,
                                "conid": "",
                                "primaryfl": "Y",
                                "voidfl": ""
                            });
                        }
                        if ($("#input-new-surgeon-email").val() != "") {
                            surgeonInput.arrgmsurgeoncontactsvo.push({
                                "contype": "Home",
                                "conmode": "90452",
                                "convalue": $("#input-new-surgeon-email").val(),
                                "conid": "",
                                "primaryfl": "Y",
                                "voidfl": ""
                            });
                        }
                        console.log(JSON.stringify(surgeonInput));
                        
                        var formData = new FormData();
                        var format='doc';
                        // Get form
                        var $form = $("#txt-fileupload").closest(".frm-documents");
                        var files = $form.find('[name="file"]')[0].files;
                        this.arrAcceptetFormat = ['doc', 'docx', 'pdf', 'txt'];
                        if($("#txt-fileupload").val()!='')
                            format = _.last(files[0].name.split('.'));
                        

                        if (!_.contains(that.arrAcceptetFormat, format.toLowerCase())) {
                            $("#txt-fileupload").val("");
                            $("#span-fileupload-browse-filenm").text("");
                            displayPopupMsg("Invalid File Format. Please select any ." + that.arrAcceptetFormat.join(' or .') + " file");
                        }
                        else {
                            fnGetWebServerData("surgeon/save", undefined, surgeonInput, function (data) {
                                if (data != null) {
                                    console.log(data);
                                    showSuccess("Surgeon Detail created successfully");
                                    var surgType = 'Y';
                                    uploadCVFile(data.partyid, surgType);
                                    data.Name = data.firstnm + " " + data.lastnm;
                                    data.ID = data.partyid;
                                    GM.Global.CRNewSurgeon="Y";
                                    setSurgeon(data);
                                    $("#div-crm-overlay-content-container").find(".modal-body").empty();

                                } else
                                    showError("Save Failed");
                            });
                            $("#frm-documents").removeClass("cr-form-cv");
                            hidePopup();
                        }

                    }
                });
            } else
                $("#popup-create-surgeon-validation").text(surgstr);
        }
    });
}
function getSurgeonCv(partyid) {
    var surgeonInput = {
        "refgroup": "103112",
        "reftype": "105220",
        "refid": partyid,
        "token": localStorage.getItem("token")
    }
    console.log(surgeonInput);
    fnGetWebServerData("file/get", "gmCRMFileUploadVO", surgeonInput, function (fileObj) {
        $("#surgeon-cv-link").attr('data-surgid', partyid);
        $("#surgeon-cv-link").attr("data-refgrp", "103112");
        if (fileObj != undefined) {
            console.log(fileObj);
            $("#surgeon-cv-link").removeClass("gmVisibilityHidden");
            $("#surgeon-cv-link").removeClass("inactive").addClass("active");
            $("#surgcv").val(fileObj[0].filename);
            $("#surgeon-cv-link").attr("data-filenm", fileObj[0].filename);
            $("#surgcv").removeAttr("validate");
        } else {
            $("#surgeon-cv-link").removeClass("gmVisibilityHidden");
            $("#surgeon-cv-link").removeClass("active").addClass("inactive");
            $("#surgcv").val("");
            $("#surgeon-cv-link").attr("data-filenm", "");
            $("#surgcv").attr("validate", "required");
        }
        var PSAdmin = _.findWhere(GM.Global.UserAccess, {
           funcnm: "PRECEPTOR-ADMIN"
        });
        //PSadmin - Preceptor Admin access
        if(PSAdmin != undefined){  
            $("#surgcv").removeAttr("validate");
            $("#surgcv").removeClass("gmValidationError");
        } else
            $("#surgcv").attr("validate", "required");
        
    });
}

function setSurgeon(data) {

    getSurgeonCv(data.ID);
    $("#preceptorship-surgeon-search input").val("");
    $("#preceptorship-hsurgeon").val("");
    $("#preceptorship-hsurgeonnm").val("");
    $("#preceptorship-surgeon-search input").val(data.Name);
    
    $("#preceptorship-hsurgeon").val(data.ID);
    $("#preceptorship-hsurgeonnm").val(data.Name);
    $("#preceptorship-hsurgeon").trigger("change");
    $("#preceptorship-hsurgeonnm").trigger("change");
    $("#preceptorship-surgeon-search input").removeClass("gmValidationError");
    $("#preceptorship-surgeon-search").attr("data-surgid",data.ID);
}

function lastday(y, m) {
   return new Date(y, m, 0).getDate();
}

//To Show Segment and System List
function showPopupSegSysList() {
    var that = this;
    $('.div-overlay').show();
    $('.scheduler-main-container').attr('style', 'z-index:0');
    $('.div-crm-overlay-content-container').addClass('hide');
    $('#div-crm-overlay-content-container').addClass('popupsegSysLsit');
    $('#div-crm-overlay-content-container').removeClass('hide');  
    $( "#div-crm-overlay-content-container .modal-footer" ).addClass("hide");
    $('#div-crm-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hidePopupSegSysList();
        });
    });
}

//On clicking the close icon to hide the popup
function hidePopupSegSysList() {
    $('.div-overlay').hide();
    $('.scheduler-main-container').attr('style', '');
    $('#div-crm-overlay-content-container').removeClass('popupsegSysLsit');
    $(".load-model").removeClass("hide");
    $('.div-crm-overlay-content-container').addClass('hide');
    $('.hideDefaultBtn').addClass('hide');
}

function filterStsDvsn(filterObj){
        if (_.contains(GM.Global.Admin, this.module)) {
                    var filters = filterObj;
        } else {
            $("#filter-typ-others").removeClass("hide");
            $("#dhtmlx-filter-division").removeClass("hide");
            var filters = filterObj;
            filters["division" + localStorage.getItem("repDivision")] = true;
        }
}


function browseCVFile(){
    $("#btn-fileupload-popup-browse").click(function () {
        $("#txt-fileupload").trigger("click");
        $('input[type="file"]').change(function () {
            var file = $("#txt-fileupload").val().replace(/C:\\fakepath\\/i, '');
            $("#span-fileupload-browse-filenm").text(file);
        });

    });
}

function uploadCVFile(partyid,surgType){
    var that = this;
    this.surgid = partyid;
    this.chkNewSurgeon = surgType;
    if($("#txt-fileupload").val()!=''){
    var formData = new FormData();
    // Get form
    var $form = $("#txt-fileupload").closest(".frm-documents");
    var files = $form.find('[name="file"]')[0].files;
    this.arrAcceptetFormat =  ['doc', 'docx', 'pdf', 'txt'];
    var format = _.last(files[0].name.split('.'));
    
        if (!_.contains(that.arrAcceptetFormat, format.toLowerCase()) && that.chkNewSurgeon == 'N') {
            $("#txt-fileupload").val("");
            $("#span-fileupload-browse-filenm").text("");
            displayPopupMsg("Invalid File Format. Please select any ." + that.arrAcceptetFormat.join(' or .') + " file");
        }
        else{
            $.each(files, function (i, file) {
            formData.append('file', file);
            });
            // Create an FormData object 
            if (formData != "" && formData != undefined) {
                formData.append('refid', that.surgid);
                formData.append('reftype', '105220');
                formData.append('refgrp', '103112');
                formData.append('token', localStorage.getItem('token'));
            }
            $.ajax({
                url: URL_WSServer + 'file/crmfileupload',
                type: 'POST',
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                enctype: 'multipart/form-data',
                success: function (data) {
                    getSurgeonCv(that.surgid);
                }
            });
            $("#div-crm-overlay-content-container").find('.modal-header button').trigger("click");
        }
    }
}
function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
}

function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            return unescape(y);
        }

    }
}

