/**********************************************************************************
 * File             :        gmvPSCriteria.js
 * Description      :        View to represent Preceptorship Criteria
 * Path             :        /web/GlobusCRM/crm/preceptorship/view/gmvPSCriteria.js
 * Author           :        tmuthusamy
 **********************************************************************************/
define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars',

    // Common
    'global', 'commonutil', 'gmvSearch', 'gmvValidation'
],

    function ($, _, Backbone, Handlebars,
        Global, Commonutil, GMVSearch, GMVValidation) {

        'use strict';

        GM.View.PSCriteria = Backbone.View.extend({

            name: "CRM Preceptorship",

            el: "#div-crm-module",

            /* Load the templates */
            template_MainModule: fnGetTemplate(URL_Common_Template, "gmtCRMMainModule"),
            template_preceptorCriteria: fnGetTemplate(URL_Preceptor_Template, "gmtPSCriteria"),


            events: {
                "click #createHostCase": "CreateHostCase",
                "click #li-report, #li-create": "showDropdown",
                "click #li-cancel": "backToPage",
                "click #li-close": "backToPage",
                "click #createCaseRequest": "createCaseRequest",
                "click #hostCaseSchedule, #li-schedule": "hostCaseSchedule",
                "click #upcmgHostCases, #upcoming-host-case": "upcmgHostCases",
                "click #openHostCases, #open-host-case": "openHostCases",
                "click #openCaseRequests, #open-case-request": "openCaseRequests",
                "click #allHCDetails": "allHCDetails",
                "click #allCRDetails": "allCRDetails",
                "click #upcmgHCDetails": "upcmgHCDetails",
                "click #btn-search-hc": "lookupByName",
                "click #btn-view-hc": "lookupByDate",
                "click #btn-search-cr": "lookupReqByName",
                "click #btn-view-cr": "lookupReqByDate",
                "click .mobHostCase" : "mobHostShow",
                "click .mobCaseReq"  : "mobCaseReqShow",
                "click .btnHostGrp li,.btnCaseGrp li": "checkChartDivision",
                "click #span-activity-frmdate, #span-activity-todate, #span-activity-cr-frmdate, #span-activity-cr-todate": "loadDatePicker",
                "change #activity-frmdate, #activity-todate,#activity-cr-frmdate,#activity-cr-todate":"displaySelctdDt",

            },

            initialize: function (options) {
                $(window).on('orientationchange', this.onOrientationChange);
                this.view = options.view;
                GM.Global.PSHost = {};
                GM.Global.PSHost.Mode = "";
                GM.Global.SchedulerFL = "";
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                var yyyy = today.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd
                };
                if (mm < 10) {
                    mm = '0' + mm
                };
                this.today = mm + '/' + dd + '/' + yyyy;
                $("#div-app-title").show();
                $('#div-app-title').removeClass('gmVisibilityHidden').addClass('gmVisibilityVisible');
                $("#div-app-title").html("CRM");

                
            },

            render: function () {
                var that = this;
                var upcominginput = "";
                var openinput = "";
                var requestinput = "";
                this.$el.html(this.template_MainModule({
                    "module": "preceptorship"
                }));
                $("#preceptorship").addClass("tabpreceptorship");
                this.$(".gmPanelTitle").html("Lookup Preceptorship");
                this.$('#crm-btn-back').addClass('hide');
                this.$("#div-main-content").html(this.template_preceptorCriteria());
               
                var psCaseReqAdmin = _.findWhere(GM.Global.UserAccess, {
                    funcnm: "PRECEPTOR-ADMIN"
                });
                
                 var psHostCreate = _.findWhere(GM.Global.UserAccess, {
                    funcnm: "PRECEPTOR-HOST"
                });
                
                if ((psHostCreate == undefined ) && (psCaseReqAdmin == undefined )) {
                  $("#createHostCase").addClass("hide");  
                }

                if (psCaseReqAdmin != undefined) {
                    console.log(psCaseReqAdmin);
                    $("#upcmgHCDetails").removeClass("hide");
                    $("#allHCDetails").removeClass("hide");
                    $("#allCRDetails").removeClass("hide");
                    
                    $(".chartPSwrap").removeClass("hide");
                    var input = {
                        "token": localStorage.getItem("token")
                    }
                    fnGetWebServerData("psrpt/dashchart", undefined, input, function (data) {
                        console.log(JSON.stringify(data));
                        if (!data == "" || !data == undefined || !data == null) {
                            that.chartDt = data;
                            var hostChartActiveTab = getCookie('hostChartActiveTab');
                            var caseChartActiveTab = getCookie('caseChartActiveTab');
                            if (parseNull(hostChartActiveTab) == "" || parseNull(hostChartActiveTab) == 'spine')
                                $(".btnHostGrp .btn-hcspine").trigger("click");
                            else
                                $(".btnHostGrp .btn-hcinr").trigger("click");
                            if (parseNull(caseChartActiveTab) == "" || parseNull(caseChartActiveTab) == 'spine')
                                $(".btnCaseGrp .btn-hcspine").trigger("click");
                            else
                                $(".btnCaseGrp .btn-hcinr").trigger("click");
                        }
                    });
                }
                


                // Functions for count the host,request and Upcoming cases
                this.showDashboardCount();
                
                $("#li-report").removeClass("hide");
                $("#li-create").removeClass("hide");
                $("#li-schedule").removeClass("hide");
            },
            
            //Load common datepicker function
            loadDatePicker: function(event){
                var elemId = "#" + $(event.target).data("id");
                loadDatepicker(event,elemId,"","mm/dd/yy","","");
            },
            
            displaySelctdDt: function(event){
                var elemId = "#" + $(event.target).attr("id");
                var elemVal = $(elemId).val();
                if(elemVal == "")
                   $(event.target).siblings("span").text("mm/dd/yyyy");
                else
                    $(event.target).siblings("span").text(elemVal);               
            },
            
            checkChartDivision: function(e){
                var that = this;
                switch($(e.currentTarget).data("tabval")){
                    case "hostspine":
                         setCookie('hostChartActiveTab', "spine" ,null);
                         that.statusPSChart('hcchart-container',that.chartDt.hostspine);
                         $(".btnHostGrp .btn-hcspine").addClass("gmBGDarkGrey gmFontWhite");
                         $(".btnHostGrp .btn-hcinr").removeClass("gmBGDarkGrey gmFontWhite");
                         break;
                    case "hostinr":
                        setCookie('hostChartActiveTab', "inr" ,null);
                        that.statusPSChart('hcchart-container',that.chartDt.hostinr);
                        $(".btnHostGrp .btn-hcinr").addClass("gmBGDarkGrey gmFontWhite");
                        $(".btnHostGrp .btn-hcspine").removeClass("gmBGDarkGrey gmFontWhite");
                        break;
                    case "casespine":
                        setCookie('caseChartActiveTab', "spine" ,null);
                        that.statusPSChart('crchart-container',that.chartDt.casespine);
                        $(".btnCaseGrp .btn-hcspine").addClass("gmBGDarkGrey gmFontWhite");
                        $(".btnCaseGrp .btn-hcinr").removeClass("gmBGDarkGrey gmFontWhite");
                        break;
                    case "caseinr":
                        setCookie('caseChartActiveTab', "inr" ,null);
                        that.statusPSChart('crchart-container',that.chartDt.caseinr);
                        $(".btnCaseGrp .btn-hcinr").addClass("gmBGDarkGrey gmFontWhite");
                        $(".btnCaseGrp .btn-hcspine").removeClass("gmBGDarkGrey gmFontWhite");
                        break;
                }
            },
            
            statusPSChart: function(ElemId,chartData){
               var colorArr = ['#9bb960', '#FD625E', '#3599B8', '#F2C80F']; 
               if($(window).width() < 400)
                   var chartWidth = '300';
               else
                  var chartWidth = '400';
                   var visitcbarChart = new FusionCharts({
                       type: 'bar2d',
                       renderAt: ElemId,
                       width: chartWidth,
                       height: '200',
                       dataFormat: 'json',
                       dataSource: {
                           "chart": {
                               "outCnvBaseFontSize": "12",
                               "subCaption": "",
                               "labeldisplay": "rotate",
                               "slantlabels": "1",
                               "useellipseswhenoverflow": "1",
                               "maxLabelHeight": "80",
                               "paletteColors": colorArr.toString(),
                               "bgColor": "#ffffff",
                               "usePlotGradientColor": "0",
                               "showBorder": "0",
                               "plotBorderAlpha": "10",
                               "placevaluesInside": "0",
                               "rotatevalues": "0",
                               "showXAxisLine": "1",
                               "xAxisLineColor": "#999999",
                               "divlineColor": "#ccc",
                               "divLineDashed": "0",
                               "showAlternateHGridColor": "0",
                               "showAlternateVGridColor": "0",
                               "subcaptionFontSize": "18"
                           },
                           "data": chartData
                       }
                   }).render();
                
            },


            showDashboardCount: function (input) {
            	// input for Upcoming Host case. Input will be override in resource to get count for Open Host case and Open Case request
                var input = {
                    "actstatusid": "105765,105766",
                    "actcategory": "105272",
                    "actstartdate": this.today
                };
                getLoaderIcon("#span-ps-host-upc-count");
                getLoaderIcon("#span-ps-host-open-count");
                getLoaderIcon("#span-ps-casereq-open-count");
                fnGetWebServerData("psrpt/dashcnt", undefined, input, function (data) {
                    var count = 0;
                    if (!data == "" || !data == undefined || !data == null) {
                        $("#span-ps-host-upc-count").html(data.UPCOMEHOST);
                        $("#span-ps-host-open-count").html(data.OPENHOST);
                        $("#span-ps-casereq-open-count").html(data.OPENCASEREQ);
                    } else {
                    	$("#span-ps-host-upc-count").html("0");
                        $("#span-ps-host-open-count").html("0");
                        $("#span-ps-casereq-open-count").html("0");
                    }
                });
            },


            lookupByName: function (event) {
                var firstnm = $(".hostCase-fname").val();
                firstnm = (firstnm.indexOf("’")!=-1)?firstnm.replace("’","'"):firstnm;
                
                var name = {
                    firstnm: firstnm
                };
                if (firstnm != "") {
                    GM.Global.Activity = name;
                    firstnm = (firstnm !== "") ? firstnm : "undefined";
                    window.location.href = "#crm/preceptorship/host/search/" + firstnm + "";
                } else {
                    showError("Please provide the Name");
                }
            },

            lookupReqByName: function (event) {
                var searchnm = $(".caseReq-name").val();
                searchnm = (searchnm.indexOf("’")!=-1)?searchnm.replace("’","'"):searchnm;
                console.log(searchnm);
                var name = {
                    searchnm: searchnm
                };
                if (searchnm != "") {
                    GM.Global.Activity = name;
                    searchnm = (searchnm !== "") ? searchnm : "undefined";
                    window.location.href = "#crm/preceptorship/request/search/" + searchnm + "";
                } else {
                    showError("Please provide the Name");
                }
            },
            lookupByDate: function (event) {
                var startdate = $("#activity-frmdate").val();
                var enddate = $("#activity-todate").val();
                console.log(startdate);
                console.log(enddate);
                if (startdate != "" && startdate != null) {
                    if (startdate.indexOf("/") > 0) {
                        startdate = startdate.split("/");
                        startdate = startdate[2] + "-" + startdate[0] + "-" + startdate[1];
                    }
                }
                if (enddate != "" && enddate != null) {
                    if (enddate.indexOf("/") > 0) {
                        enddate = enddate.split("/");
                        enddate = enddate[2] + "-" + enddate[0] + "-" + enddate[1];
                    }
                }
                var name = {
                    startdate: startdate,
                    enddate: enddate
                };
                if (startdate != "" && enddate != "") {
                    if (startdate <= enddate) {
                        GM.Global.Activity = name;
                        startdate = (startdate !== "") ? startdate : "undefined";
                        enddate = (enddate !== "") ? enddate : "undefined";
                        window.location.href = "#crm/preceptorship/host/report/" + startdate + "/" + enddate;
                    } else {
                        showError("Please Select From Date is Less than To Date");
                    }

                } else {
                    showError("Please select the valid date");
                }
            },
            lookupReqByDate: function (event) {
                var startdate = $("#activity-cr-frmdate").val();
                var enddate = $("#activity-cr-todate").val();
                console.log(startdate);
                console.log(enddate);
                if (startdate != "" && startdate != null) {
                    if (startdate.indexOf("/") > 0) {
                        startdate = startdate.split("/");
                        startdate = startdate[2] + "-" + startdate[0] + "-" + startdate[1];
                    }
                }
                if (enddate != "" && enddate != null) {
                    if (enddate.indexOf("/") > 0) {
                        enddate = enddate.split("/");
                        enddate = enddate[2] + "-" + enddate[0] + "-" + enddate[1];
                    }
                }
                var name = {
                    startdate: startdate,
                    enddate: enddate
                };
                if (startdate != "" && enddate != "") {
                    if (startdate <= enddate) {
                        GM.Global.Activity = name;
                        startdate = (startdate !== "") ? startdate : "undefined";
                        enddate = (enddate !== "") ? enddate : "undefined";
                        window.location.href = "#crm/preceptorship/request/report/" + startdate + "/" + enddate;
                    } else {
                        showError("Please Select From Date is Less than To Date");
                    }
                } else {
                    showError("Please select the valid date");
                }
            },


            //Show Dropdown for Top Menus and hide other open dropdown
            showDropdown: function (e) {
                $(e.currentTarget).css('position', 'relative');
                $(".dropdown").css("display", "none");
                if ($(e.currentTarget).hasClass("open")) {
                    $(e.currentTarget).removeClass("open");
                    $(e.currentTarget).children('.dropdown').css("display", "none");
                } else {
                    $(".dropdown").parent().removeClass("open");
                    $(e.currentTarget).addClass("open");
                    $(e.currentTarget).children('.dropdown').css("display", "block");
                }
            },

            //Create Host Case View Change Function
            CreateHostCase: function () {
                window.location.href = "#crm/preceptorship/host/id/0";
            },


            //Create Case Request Function
            createCaseRequest: function () {
                window.location.href = "#crm/preceptorship/case/id/0";
            },
            // Host Case View schedule Function
            hostCaseSchedule: function () {
                window.location.href = "#crm/preceptorship/schedule";
            },

            // Back to Screen after clicking the back icon
            backToPage: function () {
                window.history.back();
            },
            upcmgHostCases: function () {
                window.location.href = "#crm/preceptorship/host/report/upcoming";
            },
            openHostCases: function () {
                window.location.href = "#crm/preceptorship/host/report/open";
            },
            allHCDetails: function () {
                window.location.href = "#crm/preceptorship/host/report/allhost";
            },
            allCRDetails: function () {
                window.location.href = "#crm/preceptorship/case/report/allcase";
            },

            upcmgHCDetails: function () {
                window.location.href = "#crm/preceptorship/host/report/upcmghcdetails";
            },
            openCaseRequests: function () {
                window.location.href = "#crm/preceptorship/case/report/opencase";
            },
            
            mobHostShow: function(e){
                     var width = $(window).width();
                  if (width <= 480) {
                    $("#host-case-search").slideToggle();
                  }
                },
            mobCaseReqShow: function(e){
                     var width = $(window).width();
                  if (width <= 480) {
                    $("#case-request-search").slideToggle();
                  }
                },
        });
        return GM.View.PSCriteria;
    });
