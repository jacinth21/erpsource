/**********************************************************************************
 * File             :        gmvPSCase.js
 * Description      :        View to represent Preceptor Case
 * Path             :        /web/GlobusCRM/crm/preceptorship/view/gmvPSCase.js
 * Author           :        tmuthusamy
 **********************************************************************************/
define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars',

    // Common
    'global', 'commonutil', 'gmvSearch', 'gmvValidation', 'gmvMultiFileUpload','dropdownview',

    // View
     'gmvCRMSelectPopup', 'gmvItemList',

    //Model
     'gmmPS',

    //Collection
    'gmcItem',

],

    function ($, _, Backbone, Handlebars,
        Global, Commonutil, GMVSearch, GMVValidation, GMVMultiFileUpload,DropDownView,
        GMVCRMSelectPopup, GMVItemList, GMMPS, GMCItem) {

        'use strict';

        GM.View.PSCase = Backbone.View.extend({

            name: "CRM Preceptorship Case",

            el: "#div-crm-module",

            /* Load the templates */
            template_MainModule: fnGetTemplate(URL_Common_Template, "gmtCRMMainModule"),
            template_PreceptorCaseEdit: fnGetTemplate(URL_Preceptor_Template, "gmtPSCaseEdit"),
            template_PreceptorCaseInfo: fnGetTemplate(URL_Preceptor_Template, "gmtPSCaseInfo"),
            template_CreateSurgeon: fnGetTemplate(URL_Common_Template, "gmtCvNewSurgeon"),
            template_VisitCancelReq: fnGetTemplate(URL_PhysicianVisit_Template, "gmtCancelRequest"),
            template_FileuploadDoc: fnGetTemplate(URL_Common_Template, "gmtCRMFileUpload"),
            template_DhxReportMobile: fnGetTemplate(URL_Common_Template, "gmtDhxReportMobile"),


            events: {
                "change input,select,textarea": "updateModel",
                "click #li-save": "saveCase",
                "click #li-edit": "editCase",
                "click #li-void": "voidCase",
                "click #li-cancel": "backToPage",
                "click #cancel-case-request": "cancelCase",
                "click #lookupHostCase": "showlkpHostCase",
                "click #rd-fileupld": "rdFileUpload",
                "click #admin-fileupld": "adminFileUpload",
                "click #preceptorship-case-surgeon": "createSurgeon",
                "click .tabPS": "togglePSCRTab",
                "click .remove-participants": "removeParty",
                "click .span-remove-pdct": "removeAttr",
                "blur #input-lead-contacts-npid": "checkNPID",
                "change #divtyp": "changeDivType",
                "click #btn-add-other-visitor": "addOtherVisitor",
                "change #input-preceptorship-other-visitor-title, #input-preceptorship-other-visitor": "setVisitorNm",
                "click .span-remove-attr": "cancelAttribute",
                "change #pcptyp": "changePSType",
                "click #surgeon-cv-link": "uploadCVSurgeon",
                "blur #preceptorship-surgeon-search input": "chkSurgeon",
                "click #segSysList" : "fetchSegSysList",
                "click .a-file-open": "viewDocument",
                "click .a-view-docs": "viewUploadDocs",
                "blur #preceptorship-procedure input": "chkProcedure",
                "click #preceptorTypeNfn": "openNotifyPSType",
                "click #btn-add-new-acnt": "addNewAcnt",
                "change #regionSelect": "changeZoneVal",
                "change #atndtyp": "changeAtndType",
                "click #span-preceptorship-frmdt, #span-preceptorship-todt": "loadDatePicker",
                "change #preceptorship-frmdt, #preceptorship-todt":"displaySelctdDt",
            },

            initialize: function (options) {
                this.curPosTgt = 0;
                if (options.data != "") {
                    this.data = options.data.GmCRMListVO.gmCRMPSCaseVO[0];
                } else {
                    this.data = "";
                }
                this.casereqid = options.casereqid;
                var that = this;
                this.mfu;
                this.afu;
                this.psTypCheck = "";
                this.divTypCheck = "";
                this.module = "preceptorship";
                 GM.Global.CRNewSurgeon="";
                if (GM.Global.preceptorship == undefined)
                    GM.Global.preceptorship = {};

                if (this.casereqid && this.casereqid != 0) {
                    this.PSModel = new GMMPS(this.data);
                    this.PSModel.set("arrcrmpscaseassvo", arrayOf(this.data.arrcrmpscaseassvo));
                    this.PSModel.set("arrcrmpscasereqvo", arrayOf(this.data.arrcrmpscasereqvo));
                    this.PSModel.set("arrcrmpscaseattrvo", arrayOf(this.data.arrcrmpscaseattrvo));

                    if (this.PSModel.get('arrcrmpscaseassvo')[0] == null || this.PSModel.get('arrcrmpscaseassvo')[0] == undefined)
                        this.PSModel.set('arrcrmpscaseassvo', []);
                    if (this.PSModel.get('arrcrmpscasereqvo')[0] == null || this.PSModel.get('arrcrmpscasereqvo')[0] == undefined)
                        this.PSModel.set('arrcrmpscasereqvo', []);
                    if (this.PSModel.get('arrcrmpscaseattrvo')[0] == null || this.PSModel.get('arrcrmpscaseattrvo')[0] == undefined)
                        this.PSModel.set('arrcrmpscaseattrvo', []);
                    this.PSModel.set($.parseJSON(JSON.stringify(this.PSModel)));
                    GM.Global.preceptorship.Mode = "view";
                } else if (this.casereqid == 0 || this.casereqid == undefined) {
                    this.PSModel = new GMMPS();
                    GM.Global.preceptorship.Mode = "create";


                }
            },

            render: function () {
                var that = this;
                this.rdFiles = [];
                this.adFiles = [];
                
                this.legendtype = [];
                 $.getJSON( "globusMobileApp/datafiles/pstypelegend.json", function( data ) {
                        that.legendtype = data;
                    });
                this.psCaseReqAdmin = _.findWhere(GM.Global.UserAccess, {
                        funcnm: "PRECEPTOR-ADMIN"
                });
                
                var defDivision = localStorage.getItem("repDivision");
                var defRegion = localStorage.getItem("userDivision");
                if(parseNull(defDivision) == "")
                   defDivision = "2000";
                if(parseNull(defRegion) != "100824")
                   defRegion = "100823";
                if (GM.Global.preceptorship.Mode == "create") {
                    that.$el.html(this.template_MainModule({
                        "module": "preceptorship"
                    }));
                    if(parseNull(GM.Global.SchedulerFL)=="Y"){
                        that.PSModel.set("division", GM.Global.PSHostActivity.division);
                        that.PSModel.set("pstype", GM.Global.PSHostActivity.acttype);
                        that.PSModel.set("segid", GM.Global.PSHostActivity.segid);
                        that.PSModel.set("procnm", GM.Global.PSHostActivity.segnm);
                        //Set Product values
                        if(GM.Global.PSHostActivity.arrgmactivityattrvo !=null || GM.Global.PSHostActivity.arrgmactivityattrvo !=undefined){
                            _.each(GM.Global.PSHostActivity.arrgmactivityattrvo, function (dt) {
                                console.log(that.PSModel.get("arrcrmpscasereqvo"));
                                console.log(dt);
                                if (dt.attrtype == "105582"){
                                    var attrVOItem;
                                    attrVOItem = _.findWhere(that.PSModel.get("arrcrmpscasereqvo"), {
                                            setid: dt.attrval
                                    });
                                    if(!attrVOItem)
                                    that.PSModel.get("arrcrmpscasereqvo").push({
                                        "crdid": "",
                                        "setid": dt.attrval,
                                        "setnm": dt.attrnm,
                                        "voidfl": "",
                                    });
                                }
                            });
                        }
                        //Date Values
                        var startdt = that.formatdt(GM.Global.PSHostActivity.actstartdate)
                        var enddt = that.formatdt(GM.Global.PSHostActivity.actenddate)
                         that.PSModel.set("frmdt", startdt);    
                         that.PSModel.set("todt", enddt);    
                         that.PSModel.set("workflow", GM.Global.PSHostActivity.workflow);    
                            
                    }
                    var changeFmtData = that.PSModel.toJSON();
                    this.casereqid = changeFmtData.casereqid;
                    $(".gmPanelContent").html(that.template_PreceptorCaseEdit(changeFmtData));
                    this.gmvValidation = new GMVValidation({
                        "el": that.$(".gmPanelContent")
                    });
                    $(".gmPanelTitle").html("Create Case Request");
                    $("#preceptorship").addClass("tabpreceptorship");
                    $(".file-upload-content").addClass("hide");    // File Upload not Working on Create Case
                    this.searchSurgeon();
                    this.searchProduct();
                    this.searchFieldSales();
                    this.searchProcedure();
                    this.loadHospitalAffiliationData();
                    this.changeActNm();
                    this.renderCount = 0;
                    if (this.renderCount != undefined && this.module == "preceptorship" && GM.Global.preceptorship.Mode != "view" && this.renderCount > 0) {
                        for (var i = 0; i < this.renderCount; i++) {
                            this.addOtherVisitor();
                        }
                    }
                    getCLValToDiv("#crfStatus", "", "", "PSCSTS", "", "", "codeid", "pcprcrf", false, false, function (divID, attrVal) {
                        var curVal = that.PSModel.get(attrVal);
                        if (parseNull(curVal) == "") {
                            that.setSelect(divID, "107342");
                            $(divID).trigger("change");
                        } else
                            that.setSelect(divID, that.PSModel.get(attrVal));
                    });

                    getCLValToDiv("#trfstatis", "", "", "PSCSTS", "", "", "codeid", "trfstatis", false, false, function (divID, attrVal) {
                        var curVal = that.PSModel.get(attrVal);
                        if (parseNull(curVal) == "") {
                            that.setSelect(divID, "107342");
                            $(divID).trigger("change");
                        } else
                            that.setSelect(divID, that.PSModel.get(attrVal));
                    });

                    getCLValToDiv("#divtyp", "", "", "DIVLST", "", "105272", "codenmalt", "division", false, true, function (divID, attrVal) {
                        var curVal = that.PSModel.get(attrVal);
                        if (parseNull(curVal) == "") {
                            that.setSelect(divID, defDivision);  // Default Set to login users rep division
                            $(divID).trigger("change");
                        } else {
                            that.setSelect(divID, that.PSModel.get(attrVal));
                        }
                        if (parseNull(curVal) == "2004") {
                            $('.workflowItem').removeClass("hide");
                            $('.pcptItem').removeClass("hide");
                        }
                        else if(parseNull(curVal) == "2000" || parseNull(curVal) == "2005"){
                           $('.pcptItem').removeClass("hide");
                           $('.workflowItem').addClass("hide");
                        }

                    });
                    
                    getCLValToDiv("#regionSelect", "", "", "DIV", "", "", "codeid", "region", false, false, function (divID, attrVal) {
                        if (that.PSModel.get(attrVal) != "")
                            that.setSelect(divID, that.PSModel.get(attrVal));
                        else {
                            that.setSelect(divID, defRegion);
                            $(divID).trigger("change");
                        }
                    });

                    getCLValToDiv("#workflw", "", "", "PRWKFL", "", "", "codeid", "workflow", false, true, function (divID, attrVal) {
                        var curVal = that.PSModel.get(attrVal);
                        if (parseNull(curVal) == "")
                            that.setSelect(divID, curVal);
                        else
                            that.setSelect(divID, that.PSModel.get(attrVal));
                    });
                    
                    if (that.PSModel.get("division") == "2004"){
                        getCLValToDiv("#pcptyp", "", "", "PCTSTY", "INR", "", "codeid", "pstype", false, true, function (divID, attrVal) {
                            var curVal = that.PSModel.get(attrVal);
                            if (parseNull(curVal) == "") {
                                that.setSelect(divID, curVal);
                                $('.PSWebinarFL').addClass("hide");
                            } else {
                                that.setSelect(divID, that.PSModel.get(attrVal));
                            }
                            if (parseNull(curVal) == "107264") {
                                $('.clobjItem').removeClass("hide");
                            }
                        });
                    }
                    else{
                         getCLValToDiv("#pcptyp", "", "", "PCTSTY", "NINR", "", "codeid", "pstype", false, true, function (divID, attrVal) {
                            var curVal = that.PSModel.get(attrVal);
                            if (parseNull(curVal) == "") {
                                that.setSelect(divID, curVal);
                                $('.PSWebinarFL').addClass("hide");
                            } else {
                                that.setSelect(divID, that.PSModel.get(attrVal));
                            }
                        });
                    }
                   that.getZoneVal(defRegion,true);

                    // Admin Status
                    getCLValToDiv("#admin-status", "", "", "PCTADS", "", "", "codeid", "adminsts", false, true, function (divID, attrVal) {
                        if (that.PSModel.get(attrVal) != "")
                            that.setSelect(divID, that.PSModel.get(attrVal));
                        else {
                            that.setSelect(divID, ""); //set default value
                            $(divID).trigger("change");
                        }
                    });
                    getCLValToDiv("#atndtyp", "", "", " PCTATY", "", "", "codeid", "atndtyp", false, false, function (divID, attrVal) {
                        if (that.PSModel.get(attrVal) != "")
                            that.setSelect(divID, that.PSModel.get(attrVal));
                        else {
                            that.setSelect(divID, "109000"); //default codeid for surgeon
                            $(divID).trigger("change");
                        }
                    });

                    

                    $("#li-save").removeClass("hide");
                    $("#li-cancel").removeClass("hide");
                    if (this.casereqid == 0 || this.casereqid == undefined) {
                        var newParticipants = _.findWhere(this.PSModel.get("arrcrmpscaseassvo"), {
                            role: "105561"
                        });
                        if (newParticipants == undefined) {
                            var partyinfo = {
                                "ID": localStorage.getItem("partyID"),
                                "Name": localStorage.getItem("fName") + " " + localStorage.getItem("lName"),
                            };
                            that.setFieldSales(partyinfo, "105561");
                        }
                    }
                    
                    if (that.PSModel.get("pstype") == "109342" || that.PSModel.get("pstype") == "109340")
                        $('.PSWebinarFL').removeClass("hide");
                    else
                        $('.PSWebinarFL').addClass("hide");
                    
                }
                if (GM.Global.preceptorship.Mode == "view") {
                    that.$el.html(that.template_MainModule({
                        "module": "preceptorship"
                    }));
                    
                    var viewData = that.PSModel.toJSON();
                    _.each(viewData.arrcrmpscaseassvo,function(item){
                        if(item.adnm != null){
                            item.adnm = 'AD:' + " " + item.adnm;
                        }
                        if(item.vpnm != null){
                            item.vpnm = 'VP:' + " " + item.vpnm;
                        }
                    });
                    viewData.server = URL_CRMFileServer;
                    
                    $(".gmPanelContent").html(that.template_PreceptorCaseInfo(viewData));
                    
                    $(".gmPanelContent").attr("crid", that.PSModel.casereqid);
                    $("#preceptorship").addClass("tabpreceptorship");
                    $("#li-save").addClass("hide");
                    $("#li-cancel").addClass("hide");
                    if (that.psCaseReqAdmin != undefined) 
                            $("#lookupHostCase").removeClass("hide");                   
                    else
                            $("#lookupHostCase").addClass("hide");
                       
//                    else if(GM.Global.UserID == that.PSModel.get("createdbyid")){
//                            $("#li-edit").removeClass("hide");
//                            $("#lookupHostCase").addClass("hide");
//                    }
//                    else{
//                        $("#li-edit").addClass("hide");
//                        $("#lookupHostCase").addClass("hide");
//                     }
                    
                    
                    if(viewData.status == '105762'){
                        $("#lookupHostCase").addClass("hide");
                        if (that.psCaseReqAdmin != undefined)
                        $("#li-edit").removeClass("hide");
                        else
                        $("#li-edit").addClass("hide");
                    }
                    else                        
                        $("#li-edit").removeClass("hide");
                    
                    $("#cv-links-" + viewData.surgid + " a").attr("href", URL_CRMFileServer + viewData.surgeoncv);

                    setTimeout(function () {
                        that.getUploadFiles(that.casereqid);
                    });
                    setTimeout(function () {
                        that.getAdminUploadFiles(that.casereqid);
                    });
                    if(that.PSModel.get("atndtyp") != 109000 && that.PSModel.get("surgid") == "")
                        $(".gmPanelTitle").html("Case Request - " + that.PSModel.toJSON().atndnm);
                    else
                        $(".gmPanelTitle").html("Case Request - " + that.PSModel.toJSON().surgnm);

                }
                if (GM.Global.preceptorship.Mode == "edit") {
                    var that = this;
                    this.gmvValidation = new GMVValidation({
                        "el": that.$(".gmPanelContent")
                    });
                    $("#li-save").removeClass("hide");
                    $("#li-cancel").removeClass("hide");
                    $("#li-edit").addClass("hide");
                    var editData = that.PSModel.toJSON();
                    $(".gmPanelContent").attr("crid", editData.casereqid);
                    var frmdt = this.PSModel.get("frmdt");
                    var todt = this.PSModel.get("todt");
                    
                    if (frmdt != "" && frmdt != null) {
                            if (frmdt.indexOf("-") > 0) {
                                  var fmtFromDt = compDateFmt(that.PSModel.get("frmdt"));
                                  that.PSModel.set("frmdt",fmtFromDt);
                            } 
                            else {
                                frmdt = frmdt;
                            }
                    }
                    if (todt != "" && todt != null) {
                            if (todt.indexOf("-") > 0) {
                                  var fmtToDt = compDateFmt(that.PSModel.get("todt"));
                                  that.PSModel.set("todt",fmtToDt);
                            } 
                            else {
                                todt = todt;
                            }
                    }
                    $(".gmPanelContent").html(that.template_PreceptorCaseEdit(that.PSModel.toJSON()));
                    $("#cancel-case-request").removeClass("hide");
                    that.searchSurgeon();
                    that.searchProduct();
                    that.searchFieldSales();
                    that.searchProcedure();
                    that.changeActNm();
                    that.loadHospitalAffiliationData();
                    if(editData.status == '105762'){ //107620 - case cancelled for comment field, so changed to 105762 - cancelled status
                       $("#cancel-case-request").addClass("hide");
                    }
                    if(that.ptsCaseTab == "admin"){
                        $("#preceptorship-admin").trigger("click");
                    }
                    else{
                        that.ptsCaseTab = "request";
                    }
                    
                    getCLValToDiv("#crfStatus", "", "", "PSCSTS", "","", "codeid", "pcprcrf", false, false, function (divID, attrVal) {
                        var attrVal = parseInt(editData.pcprcrf);
                        that.setSelect(divID, attrVal);
                    });

                    getCLValToDiv("#trfstatis", "", "", "PSCSTS", "", "", "codeid", "trfstatis", false, false, function (divID, attrVal) {
                        var attrVal = parseInt(editData.trfstatis);
                        that.setSelect(divID, attrVal);
                    });

                    getCLValToDiv("#divtyp", "", "", "DIVLST", "", "105272", "codenmalt", "division", false, true, function (divID, attrVal) {
                        var attrVal = parseInt(editData.division);
                        that.setSelect(divID, attrVal);
                        if (attrVal == "2004") {
                            $('.workflowItem').removeClass("hide");
                            $('.pcptItem').removeClass("hide");
                        }
                        else if(attrVal == "2000" || attrVal == "2005"){
                           $('.pcptItem').removeClass("hide");
                        }
                    });
                    
                    getCLValToDiv("#regionSelect", "", "", "DIV", "", "", "codeid", "region", false, false, function (divID, attrVal) {
                        var attrVal = parseInt(editData.region);
                        that.setSelect(divID, attrVal);
                    });

                    getCLValToDiv("#workflw", "", "", "PRWKFL", "", "", "codeid", "workflow", false, true, function (divID, attrVal) {
                         var curVal = that.PSModel.get(attrVal);
                        if (parseNull(curVal) == "")
                            that.setSelect(divID, curVal);
                        else
                            that.setSelect(divID, parseInt(editData.workflow));
                    });
                    
                    if (that.PSModel.get("division") == "2004"){
                        getCLValToDiv("#pcptyp", "", "", "PCTSTY", "INR", "", "codeid", "pstype", false, true, function (divID, attrVal) {
                            var attrVal = editData.pstype;
                            if(parseNull(attrVal) == ""){
                                that.setSelect(divID, attrVal);
                                $('.PSWebinarFL').addClass("hide");
                            }
                            else
                                that.setSelect(divID, parseInt(editData.pstype));
                            if (attrVal == "107264") {
                                $('.clobjItem').removeClass("hide");
                            }
                        });
                    }
                    else{
                        getCLValToDiv("#pcptyp", "", "", "PCTSTY", "NINR", "", "codeid", "pstype", false, true, function (divID, attrVal) {
                            var attrVal = that.PSModel.get(attrVal);
                            if(parseNull(attrVal) == ""){
                                that.setSelect(divID, attrVal);
                                $('.PSWebinarFL').addClass("hide");
                            }
                            else
                                that.setSelect(divID, parseInt(editData.pstype));
                        });
                    }
                    
                    that.getZoneVal(defRegion,true);

                    // Admin Status
                    getCLValToDiv("#admin-status", "", "", "PCTADS", "", "", "codeid", "adminsts", false, true, function (divID, attrVal) {
                        if (that.PSModel.get(attrVal) != "")
                            that.setSelect(divID, that.PSModel.get(attrVal));
                        else {
                            that.setSelect(divID, ""); //set default value
                            $(divID).trigger("change");
                        }
                    });
                    
                    getCLValToDiv("#atndtyp", "", "", " PCTATY", "", "", "codeid", "atndtyp", false, false, function (divID, attrVal) {
                        if (that.PSModel.get(attrVal) != "")
                            that.setSelect(divID, that.PSModel.get(attrVal));
                        else {
                            that.setSelect(divID, "109000"); //default codeid for surgeon
                            $(divID).trigger("change");
                        }
                    });

                    setTimeout(function () {
                        that.getUploadFiles(that.casereqid);
                    });

                    setTimeout(function () {
                        that.getAdminUploadFiles(that.casereqid);
                    });
                    
                    if (that.PSModel.get("pstype") == "109342" || that.PSModel.get("pstype") == "109340")
                        $('.PSWebinarFL').removeClass("hide");
                    else
                        $('.PSWebinarFL').addClass("hide");
                    

                }
                
                if (that.psCaseReqAdmin != undefined) {
                    $("#preceptorship-admin").removeClass("hide");
                    $(".adminStatus").removeClass("hide");
                } else {
                    $("#preceptorship-admin").addClass("hide");
                }
//                this.checkDivVisible();
                
                $("#nextRqstDls").on("click",function(){
                    $("#preceptorship-request").trigger("click");
                });
                
                if(that.ptsCaseTab == "request"){
                    $("#preceptorship-request").trigger("click");
                }
                else if(that.ptsCaseTab == "admin"){
                    $("#preceptorship-admin").trigger("click");
                }
                else{
                    $("#preceptorship-INR-info").trigger("click");
                }
                
                if(that.PSModel.get("atndtyp") != 109000){
                   $('.ulSurgeonField').addClass('hide');
                    $('.ulAttendeeField').removeClass('hide');
                }
                else{                       
                    $('.ulAttendeeField').addClass('hide');
                    $('.ulSurgeonField').removeClass('hide');  
                }
                // For Future to show the Info tab
//                var caseInfo = $(".caseInfoDls");
//                $.getJSON( "globusMobileApp/datafiles/preceptorship.json", function( data ) {
//                    caseInfo.empty();
//                     _.each(data, function (x) {
//                        caseInfo.append(x.title + x.body);
//                    });
//                });
                if ($(window).width() < 480) {
                    $('#pscaseedits').removeClass("tab-closed");
                    $('#pscaseedits').addClass("tab-opened");
                }
                if ($(window).width() < 768) {
                        $(".gmPanelContent").scrollTop(that.curPosTgt);
                    }
            },
            
            //Load common datepicker function
            loadDatePicker: function(event){
                var elemId = "#" + $(event.target).data("id");
                var propName = $(elemId).attr("name");
                loadDatepicker(event,elemId,this.PSModel.get(propName),"mm/dd/yy",new Date(),"");
            },
            
            displaySelctdDt: function(event){
                var elemId = "#" + $(event.target).attr("id");
                var elemVal = $(elemId).val();
                if(elemVal == "")
                   $(event.target).siblings("span").text("mm/dd/yyyy");
                else
                    $(event.target).siblings("span").text(elemVal);               
            },
            
            formatdt: function(dt){
               var d = new Date(dt);
                var curr_date = ("0" + d.getDate()).slice(-2);
                var curr_month = ("0" + (d.getMonth() + 1)).slice(-2); //Months are zero based
                var curr_year = d.getFullYear();
                return curr_year + "-" + curr_month + "-" + curr_date;  
            },

            //On Change Division
            changeDivType: function (e) {
                var that = this;
                var newDivision = $(e.currentTarget).find("option:selected").val();
                that.divTypCheck = newDivision;                
                    $(".preceptTypeNfnBx").css("display","none");
                if (newDivision == "2004") {
                    $("#pcptyp").val("");
                    $("#pcptyp").trigger("change");
                    $('.workflowItem').removeClass("hide");
                    $('.pcptItem').removeClass("hide");
                    getCLValToDiv("#workflw", "", "", "PRWKFL", "", "", "codeid", "workflow", false, true, function (divID, attrVal) {
                        var curVal = that.PSModel.get(attrVal);
                        if (parseNull(curVal) == "")
                            that.setSelect(divID, that.PSModel.get(attrVal));
                    });
                    getCLValToDiv("#pcptyp", "", "", "PCTSTY", "INR", "", "codeid", "pstype", false, true, function (divID, attrVal) {
                        var curVal = that.PSModel.get(attrVal);
                        if (parseNull(curVal) == "")
                            that.setSelect(divID, that.PSModel.get(attrVal));
                    });
                    var dpdct = {};
                    dpdct.ID = '300.336';
                    dpdct.Name = 'Excelsius GPS';
                    that.setProductInfo(dpdct);
                    
                }else if(newDivision == "2000" || newDivision == "2005"){
                    $("#pcptyp").val("");
                    $("#pcptyp").trigger("change");
                    $('.pcptItem').removeClass("hide");   
                    $('.workflowItem').addClass("hide");
                    getCLValToDiv("#pcptyp", "", "", "PCTSTY", "NINR", "", "codeid", "pstype", false, true, function (divID, attrVal) {
                        var curVal = that.PSModel.get(attrVal);
                        if (parseNull(curVal) == "")
                            that.setSelect(divID, that.PSModel.get(attrVal));
                    });
                 }
                 else {
                    $('.workflowItem').addClass("hide");
                    $('.pcptItem').addClass("hide");
                    $('.clobjItem').addClass("hide");
                }
                
                if(newDivision != "2004"){
                    $(".gmPStypeLbl").html("PRECEPTORSHIP TYPE");
                    $(".productItems .div-sc-extn").each(function(){
                        if($(this).attr("data-setid") == '300.336'){
                            $(this).find(".span-remove-pdct").trigger("click");
                        }
                    });
                }
                else{
                    $(".gmPStypeLbl").html("<span class='gmFontValidation'>*</span>PRECEPTORSHIP TYPE");
                }
            },
            
            viewDocument: function (event) {
                var path = $(event.currentTarget).attr('href-url');
                if (parseNull(path) != "")
                    window.open(path, '_blank', 'location=no');
            },
            
            viewUploadDocs: function (event) {

                var path = $(event.currentTarget).find('.hidden-file').text();
                if (parseNull(path) == "")
                    path = $(event.currentTarget).attr("href");

                if (path != undefined)
                    window.open(path, '_blank', 'location=no');
            },

            //On Change Division
            changePSType: function (e) {
                var that = this;
                var newPSType = $(e.currentTarget).find("option:selected").val();
                that.psTypCheck = newPSType;
                if (newPSType == "107264") {
                    $('.clobjItem').removeClass("hide");
                } else {
                    $('.clobjItem').addClass("hide");
                }
                
                if (newPSType == "109342" || newPSType == "109340")
                    $('.PSWebinarFL').removeClass("hide");
                else {
                    $('.PSWebinarFL').addClass("hide");
                    if ($('#webinarfl').is(":checked")) {
                        $("#webinarfl").prop("checked", false);
                        $("#webinarfl").trigger("change");
                        $("#webinarfltxt").val("");
                    }
                }
            },

            //Tab Toggle Function
            togglePSCRTab: function (e) {
                var that = this;
                colorControl(e, "tabPS", "gmBGLGrey gmFontBlack", "gmBGDarkerGrey gmFontWhite");
                $(".div-casereq-sub-content").addClass("hide");
                if($(e.currentTarget).attr("name") == 'casereqinfo'){
                     that.ptsCaseTab = "caseinfo";
                     $("#div-casereq-INR-info").removeClass("hide");
                     $("#div-preceptorship-request").addClass("hide");
                 }
                 else if ($(e.currentTarget).attr("name") == 'requestdetails') {
                    that.ptsCaseTab = "request";
                    if (that.ptsCaseTab == "request") {
                        $("#div-preceptorship-request").removeClass("hide");
                        $("#div-preceptorCaseRqstInfo").removeClass("hide");
                    }
                } else if ($(e.currentTarget).attr("name") == 'admindetails') {
                    that.ptsCaseTab = "admin";
                    if (that.ptsCaseTab == "admin") {
                        $("#div-preceptorship-admin").removeClass("hide");
                        $("#div-preceptorCaseAdminInfo").removeClass("hide");
                    }
                }
            },


            //Update the Model 
            updateModel: function (e) {
                var that = this;
                this.selectDate(e);
                var control;
                if (e.currentTarget.tagName == "SELECT") {
                    control = $(e.currentTarget).find("option:selected");
                } else {
                    control = $(e.currentTarget);
                }

                if (this.stopRerender) {
                    this.stopRerender = undefined;
                    this.PSModel.on('change', this.render, this);
                }

                var attribute = control.attr("name");
                var VO = control.attr("VO");
                var value = control.val();
                var attrval = control.val();
                var attrtype = control.attr("attrtype");
                var attrnm = control.attr("attrnm");
                var attrid = control.attr("attrid");
                var updAfl = control.attr("updAfl");
                var index = control.attr("index");
                var partyid = control.attr("partyid");
                if (control.attr("type") == 'checkbox') {
                    if (e.currentTarget.checked)
                        value = "Y";
                    else
                        value = "";
                }

                if (updAfl == "Y") {
                    attrnm = attrval;
                    attrval = control.attr("attrval");
                }

                if ((attribute != undefined) || (value != undefined)) {
                    if (VO == undefined) {
                        this.PSModel.set(attribute, value);
                    } else {
                        if (VO == "arrcrmpscaseassvo") {
                            this.updateCaseReqAss(partyid, attribute, attrnm, value);
                        }
                        if (VO == "arrcrmpscasereqvo") {
                            this.updateCaseReq(attrid, attrtype, attrval, attrnm, updAfl, index);
                        }
                        if (VO == "arrcrmpscaseattrvo") {
                            this.updateAttrVO(attrid, attrtype, attrval, attrnm, updAfl, index);
                        }
                    }
                }
                else {                    
                    that.render();
                }
                
                
            },




            //proving search option to surgeon name
            searchSurgeon: function (e) {
                var that = this;
                var fnName = "";
                var searchOptions1 = {
                    'el': $("#preceptorship-surgeon-search"),
                    'url': URL_WSServer + 'search/rsparty',
                    'inputParameters': {
                        'token': localStorage.getItem('token'),
                        "partytype": "7000"
                    },
                    'placeholder': 'Key in at least 3 characters to search',
                    'keyParameter': 'partynm',
                    'IDParameter': 'id',
                    'NameParameter': 'name',
                    'resultVO': 'gmPartyBasicVO',
                    'fnName': fnName,
                    'minChar': 3,
                    'callback': function (result) {
                         GM.Global.CRNewSurgeon="";
                        setSurgeon(result);
                        if(result != null){
                            $("#preceptorship-surgeon-search").attr("data-surgid",result.ID);
                            that.loadHospitalAffiliationData();
                        }
                    }
                };
                var searchView = new GMVSearch(searchOptions1);
                $("#preceptorship-surgeon-search input").attr("validate", "required");
                var surgeonID = $("#preceptorship-surgeon-search").attr("data-surgid");
                var surgeonName = $("#preceptorship-surgeon-search").attr("data-surgnm");
                if (parseNull(surgeonName) != "") {
                    $("#preceptorship-surgeon-search input").val(surgeonName);
                    getSurgeonCv(surgeonID);                   
                    
                }
            },
            
            
            chkSurgeon: function(){
                var that = this;
                var surgeonID = $("#preceptorship-surgeon-search").attr("data-surgid");
                var inputNameCheck = $("#preceptorship-surgeon-search input").val();
                if (surgeonID != "" && inputNameCheck != '') {
                    $("#preceptorship-surgeon-search input").attr("validate", "");
                    $("#preceptorship-surgeon-search input").attr("message", "");
                    $("#preceptorship-surgeon-search input").removeClass("gmValidationError");
                }
            },
            
            changeAtndType: function(e){
                var that = this;
                var newAttendee = $(e.currentTarget).find("option:selected").val();
                if(newAttendee != 109000){          // If it is not surgeon attendee type           
                    $('.ulSurgeonField').addClass('hide');
                    $('.ulAttendeeField').removeClass('hide');
                    $("#actnmlist").empty();
                    $("#actnmlist").append('<option value="">Select</option>');
                    $("#actnmlist").append('<option value="newAcnt">Add New</option>');
                }
                else{                       
                    $('.ulAttendeeField').addClass('hide');
                    $('.ulSurgeonField').removeClass('hide');  
                    that.loadHospitalAffiliationData();
                }
            },


            //Cancel Other Visitor Function
            cancelAttribute: function (event) {
                var that = this;
                var attrid = $(event.currentTarget).parent('.div-sc-extn').attr("data-attrid");
                var attrtype = $(event.currentTarget).parent('.div-sc-extn').attr("data-attrtype");
                var attrval = $(event.currentTarget).parent('.div-sc-extn').attr("data-attrval");
                var removeFl = $(event.currentTarget).attr("removeFl");
                var indexVal = $(event.currentTarget).parent().find('input').attr("index");
                
                if($(event).parent().attr('data-attrtype') == '107101'){
                    that.newHblAcntChk = 'Y';
                }

                this.rmVisitorVal();

                if (attrid != "") {
                    _.each(this.PSModel.get("arrcrmpscaseattrvo"), function (data) {
                        if (data.attrid == attrid)
                            data.voidfl = "Y";
                    });
                } 
                else if(that.newHblAcntChk == 'Y'){
                   _.each(this.PSModel.get("arrcrmpscaseattrvo"), function (data) {
                        if (data.attrval == attrval)
                            data.voidfl = "Y";
                    });   
                }
                else {
                    var voidAttr = _.reject(this.PSModel.get("arrcrmpscaseattrvo"), function (data) {
                        if (parseNull(indexVal) != "") {
                            return data.attrtype == attrtype && data.index == indexVal;
                        } else {
                            return data.attrtype == attrtype && data.attrval == attrval;
                        }
                    });
                    this.PSModel.set("arrcrmpscaseattrvo", voidAttr);
                }
                if (removeFl == "Y"){
                    $(event.currentTarget).parent('.div-sc-extn').remove();
                    that.newHblAcntChk = '';
                }
                else{
                    this.render();
                }
                    
            },

            //Add other Visitor Function
            addOtherVisitor: function () {
                var that = this;
                if(that.divCount == undefined){
                    that.divCount = 0;
                }
                else{
                    that.divCount = that.divCount + 1;
                }
//                this.divCount = $(".div-sc-extn #input-preceptorship-other-visitor").length;
                $("#btn-add-other-visitor-more").append("<div class='div-other-visitor div-sc-extn span-extend'  data-attrval='{{attrval}}' data-attrtype='{{attrtype}}' data-attrid='{{attrid}}'><label class='gmMarginRight10p'>Name</label><input type='text' id='input-preceptorship-other-visitor' class='gmInputText requiredInput" + that.divCount + "' value='' attrnm='' placeholder='Enter visitor name'  attrtype='107500' VO='arrcrmpscaseattrvo' index=" + that.divCount + " attrid='' validate='required' message='Please include name for all Other Visitors' maxlength='100'><label class='gmMarginRight10p gmText75 gmAlignHorizontal gmTextRight'>Title</label><input type='text' id='input-preceptorship-other-visitor-title' class='gmInputText requiredInput" + that.divCount + "' value='' placeholder='Enter visitor title' attrtype='107500' VO='arrcrmpscaseattrvo'  updAfl='Y' attrval='' attrnm='' index=" + that.divCount + " attrid='' validate='required' message='Please include title for all Other Visitors' maxlength='100'><span class='span-remove-attr extend-close' removeFl='Y'> - </span></div>");
                
            },

            //set visitor Name
            setVisitorNm: function (e) {
                var that = this;
                e.preventDefault();
                var target = $(e.currentTarget).attr("id");
                var currAttrVal = $(e.currentTarget).val();

                if (target == "input-preceptorship-other-visitor") {
                    $(e.currentTarget).parent().find('#input-preceptorship-other-visitor-title').attr("attrval", currAttrVal);
                }
                if (target == "input-preceptorship-other-visitor-title") {
                    $(e.currentTarget).parent().find('#input-preceptorship-other-visitor').attr("attrnm", currAttrVal);
                }
                that.newHblAcntChk = '';
            },

            //update Product values in the model
            updateAttrVO: function (attrid, attrtype, attrval, attrnm, updAfl, index) {
                var that = this;
                var attrVO = [];
                var attrVOItem;
                var obj;
                this.PSModel.set("arrcrmpscaseattrvo", arrayOf(this.PSModel.get("arrcrmpscaseattrvo")));
                if(!(isNaN(parseInt(attrval)))){
                   obj = _.findWhere(this.PSModel.get("arrcrmpscaseattrvo"), {
                        attrval: attrval,
                        voidfl: "Y"
                    }); 
                }
                else if (attrid != ""){
                    attrVOItem = _.findWhere(this.PSModel.get("arrcrmpscaseattrvo"), {
                        attrid: attrid
                    });
                }
                else{
                    attrVOItem = _.findWhere(this.PSModel.get("arrcrmpscaseattrvo"), {
                        index: index
                    });
                }
                if (attrVOItem) {
                    attrVO = _.without(this.PSModel.get("arrcrmpscaseattrvo"), attrVOItem);

                    attrVOItem["attrnm"] = attrnm;
                    attrVOItem["attrtype"] = attrtype;

                    if (parseNull(updAfl) == "")
                        attrVOItem["attrval"] = attrval;

                    attrVO.push(attrVOItem);

                    this.PSModel.get("arrcrmpscaseattrvo").length = 0;

                    $.each(attrVO, function (index, item) {
                        that.PSModel.get("arrcrmpscaseattrvo").push(item);
                    });

                } else if (obj) {
                    attrVO = _.without(this.PSModel.get("arrcrmpscaseattrvo"), obj);

                    obj["attrnm"] = attrnm;
                    obj["attrtype"] = attrtype;
                    obj["attrval"] = attrval;
                    
                    obj["voidfl"] = '';
                    
                }else {
                    this.PSModel.get("arrcrmpscaseattrvo").push({
                        "attrid": "",
                        "attrnm": attrnm,
                        "attrval": attrval,
                        "attrtype": attrtype,
                        "index": index,
                        "voidfl": ""
                    });
                }
                var rmAttr = _.reject(this.PSModel.get("arrcrmpscaseattrvo"), function (data) {
                    return data.attrval == "" && data.attrnm == "" && data.attrid == "";
                });
                this.PSModel.set("arrcrmpscaseattrvo", rmAttr);
                $(".div-other-visitor").find('.gmInputText').attr("validate", "required");
                this.rmVisitorVal();
            },

            rmVisitorVal: function () {
                var that = this;
                this.renderCount = 0;
                $(".div-other-visitor").each(function () {
                    var visitorNm = $(this).find("#input-preceptorship-other-visitor").val();
                    var visitorTitle = $(this).find("#input-preceptorship-other-visitor-title").val();
                    var visitorTitle = $(this).find("#input-preceptorship-other-visitor-title").val();
                    var visitorIndex = $(this).find("input").attr("index");
                    if (parseNull(visitorNm) == "" && parseNull(visitorTitle) == "") {
                        that.renderCount++;
                        $(this).find('.requiredInput' + visitorIndex).removeAttr('validate').removeClass('gmValidationError');
                    }

                });
            },


            //providing search option for Products
            searchProduct: function () {
                var that = this;
                var input = {
                    'token': localStorage.getItem('token')
                };
                var searchOptions2 = {
                    'el': $("#preceptorship-products"),
                    'url': URL_WSServer + 'search/system',
                    'inputParameters': input,
                    'keyParameter': 'sysnm',
                    'IDParameter': 'sysid',
                    'NameParameter': 'sysnm',
                    'resultVO': 'gmSystemBasicVO',
                    'minChar': 2,
                    'callback': function (val) {
                        that.setProductInfo(val);
                    }
                };
                var searchView = new GMVSearch(searchOptions2);

            },

            //Set Products value in the model
            setProductInfo: function (data) {
                var that = this;
                $("#" + this.module + "-product-search").find("input").val("");
                $("#" + this.module + "-hproduct").attr("attrtype", 105582);
                $("#" + this.module + "-hproduct").attr("attrid", data.ID);
                $("#" + this.module + "-hproduct").attr("attrnm", data.Name);
                $("#" + this.module + "-hproduct").trigger("change");
            },

            //update Product values in the model
            updateCaseReq: function (attrid, attrtype, attrval, attrnm, updAfl, index) {
                var that = this;
                var attrVO = [];
                var attrVOItem;

                attrVOItem = _.findWhere(this.PSModel.get("arrcrmpscasereqvo"), {
                    setid: attrid
                });

                var obj = _.findWhere(this.PSModel.get("arrcrmpscasereqvo"), {
                    "setid": attrid,
                    "voidfl": "Y"
                });

                if (!attrVOItem && !obj) {
                    this.PSModel.get("arrcrmpscasereqvo").push({
                        "crdid": "",
                        "setid": attrid,
                        "setnm": attrnm,
                        "voidfl": "",
                    });
                } else if (obj) {
                    _.each(this.PSModel.get("arrcrmpscasereqvo"), function (a) {
                        if (a.setid == attrid)
                            a.voidfl = "";
                    })
                }
                this.render();
            },

            //Remove Selected Products 
            removeAttr: function (event) {
                var that = this;
                var setid = $(event.currentTarget).parent('.div-sc-extn').attr("data-setid");

                if (setid != "") {
                    _.each(this.PSModel.get("arrcrmpscasereqvo"), function (data) {
                        if (data.setid == setid)
                            data.voidfl = "Y";
                    });
                } else {
                    var voidAttr = _.reject(this.PSModel.get("arrcrmpscasereqvo"), function (data) {
                        return data.setid == setid;
                    });

                    this.PSModel.set("arrcrmpscasereqvo", voidAttr);
                }
                this.render();
                this.updateModel(event);
            },



            //providing search option for field sales
            searchFieldSales: function () {
                var that = this;
                var searchOptions3 = {
                    'el': $("#" + this.module + "-field-sales"),
                    'url': URL_WSServer + 'search/partyrep',
                    'inputParameters': {
                        'token': localStorage.getItem('token')
                    },
                    'keyParameter': 'partynm',
                    'IDParameter': 'partyid',
                    'NameParameter': 'partynm',
                    'resultVO': undefined,
                    'minChar': 2,
                    'callback': function (result) {
                        that.setFieldSales(result, "105561"); // 105561 - Participant
                    }
                };
                var searchView = new GMVSearch(searchOptions3);
            },

            //update field sales value in the model
            setFieldSales: function (data, role) {
                var that = this;
                
                if(data.AD != null || data.AD != undefined){
                    data.AD = 'AD:' + " " + data.AD;
                }
                if(data.VP != null || data.VP != undefined){
                    data.VP = 'VP:' + " " + data.VP;
                }
                
                $("#preceptorship-participants-search").find("input").val("");
                $("#preceptorship-hpartyrole").val(role);
                $("#preceptorship-hpartynm").val(data.Name);
                $("#preceptorship-hpartynm").attr("partyid", data.ID);
                $("#preceptorship-hpartyrole").attr("partyid", data.ID);
                $("#preceptorship-adname").attr("partyid", data.ID);
                $("#preceptorship-adname").val(data.AD);
                $("#preceptorship-vpname").attr("partyid", data.ID);
                $("#preceptorship-vpname").val(data.VP);
                $("#preceptorship-hpartyrole").trigger("change");
                $("#preceptorship-hpartynm").trigger("change");
                $("#preceptorship-hpartytype").trigger("change");
                $("#preceptorship-adname").trigger("change");
                $("#preceptorship-vpname").trigger("change");


                $("#preceptorship-hpartyid").val(data.ID); //Need to replace partyid also for single attendee entery in leads
                $("#preceptorship-hpartyid").attr("partyid", data.ID);
                $("#preceptorship-hpartyid").trigger("change");
                this.render();
            },

            //update field sales
            updateCaseReqAss: function (partyid, attribute, attrnm, value) {
                var that = this;
                var partyVO = [];
                var partyVOItem;

                partyVOItem = _.findWhere(this.PSModel.get("arrcrmpscaseassvo"), {
                    "partyid": partyid
                });

                var obj = _.findWhere(this.PSModel.get("arrcrmpscaseassvo"), {
                    "partyid": partyid,
                    "voidfl": "Y"
                });

                if (!partyVOItem && !obj) {
                    partyVOItem = {
                        "craid": "",
                        "partyid": partyid,
                        "fieldnm": attrnm,
                        "adnm": "",
                        "vpnm": "",
                        "role": "105561",
                        "voidfl": ""
                    };
                } else if (obj) {
                    _.each(this.PSModel.get("arrcrmpscaseassvo"), function (a) {
                        if (a.partyid == partyid)
                            a.voidfl = "";
                    })
                }

                partyVO = _.without(this.PSModel.get("arrcrmpscaseassvo"), partyVOItem);
                partyVOItem[attribute] = value;
                partyVO.push(partyVOItem);

                this.PSModel.get("arrcrmpscaseassvo").length = 0;
                partyVO = arrayOf(partyVO);
                $.each(partyVO, function (index, item) {
                    that.PSModel.get("arrcrmpscaseassvo").push(item);
                });

            },

            //remove field sales 
            removeParty: function (event) {
                var partyid = $(event.currentTarget).parent().attr("data-partyid");
                var roleid = $(event.currentTarget).parent().attr("data-roleid");
                var craid = $(event.currentTarget).parent().attr("data-craid");

                if (parseNull(craid) != "") {
                    _.each(this.PSModel.get("arrcrmpscaseassvo"), function (data) {
                        if (data.craid == craid)
                            data.voidfl = "Y";
                    });
                } else {
                    var voidParty = _.reject(this.PSModel.get("arrcrmpscaseassvo"), function (data) {
                        return data.partyid == partyid && data.roleid == roleid;
                    });

                    this.PSModel.set("arrcrmpscaseassvo", voidParty);
                }
                this.render();
                this.updateModel(event);
            },

            //providing search option for Procedure
            searchProcedure: function () {
                var that = this;
                var input = {
                    'token': localStorage.getItem('token')
                };
                var searchOptions4 = {
                    'el': $("#preceptorship-procedure"),
                    'url': URL_WSServer + 'search/segment',
                    'inputParameters': input,
                    'keyParameter': 'segnm',
                    'IDParameter': 'segid',
                    'NameParameter': 'segnm',
                    'resultVO': 'gmSegmentBasicVO',
                    'minChar': 2,
                    'callback': function (result) {
                        that.setProcedureInfo(result);
                    }
                };
                var searchView = new GMVSearch(searchOptions4);
                var segID = $("#preceptorship-procedure").attr("data-segid");
                var procName = $("#preceptorship-procedure").attr("data-procnm");
                if (parseNull(procName) != "") {
                    $("#preceptorship-procedure input").val(procName);
                }
                $("#preceptorship-procedure input").attr("validate", "required");
                $("#preceptorship-procedure input").attr("message", "Please provide Procedure");
            },

            //Procedure Value update in the model
            setProcedureInfo: function (data) {
                var that = this;
                if(data != null){
                    $("#preceptorship-procedure").attr("data-segid",data.ID);                
                    $("#preceptorship-procedure input").val(data.Name);
                    $("#preceptorship-hsegmentid").val(data.ID);
                    $("#preceptorship-hsegmentid").trigger("change");
                    $("#preceptorship-hprocedurenm").val(data.Name);
                    $("#preceptorship-hprocedurenm").trigger("change");
                    $("#preceptorship-procedure input").removeClass("gmValidationError");
                }
            },
            
            
            chkProcedure: function(){
                var that = this;
                var segID = $("#preceptorship-procedure").attr("data-segid");
                var procedureNmChk = $("#preceptorship-procedure input").val();
                if (segID != "" && procedureNmChk != '') {
                    $("#preceptorship-procedure input").attr("validate", "");
                    $("#preceptorship-procedure input").attr("message", "");
                    $("#preceptorship-procedure input").removeClass("gmValidationError");
                }
                else{
                    $("#preceptorship-procedure input").attr("validate", "required");
                    $("#preceptorship-procedure input").attr("message", "Please provide Procedure");
                    $("#preceptorship-procedure input").addClass("gmValidationError");
                }
            },


            //status select function
            setSelect: function (controlid, selectedvalue) {
                $(controlid).val(selectedvalue);
            },
            
            // Load Hospital Affiliation
            loadHospitalAffiliationData : function(){
                var that = this;
                var modelData = that.PSModel.toJSON();
                var input = {
                        "token": localStorage.getItem("token"),
                        "partyid": modelData.surgid
                }
                fnGetWebServerData("surgeon/infohospaffl",'gmCRMSurgeonAffilVO', input, function (data) {
                    if(data != null){
                        $("#actnmlist").empty();
                        $("#actnmlist").append('<option value="">Select</option>');
                        if(data.gmCRMSurgeonAffiliationVO.length != 0) {
                            that.hosbitalList = data.gmCRMSurgeonAffiliationVO;
                            _.each(data.gmCRMSurgeonAffiliationVO,function(item){
                                $("#actnmlist").append('<option value='+ item.accid +'>' + item.accnm + '</option>');

                            });
                        }
                        $("#actnmlist").append('<option value="newAcnt">Add New</option>');
                    }
                });
            },
            
            
            // on change hospital Affiliation
            changeActNm: function(e){
                var that = this;
                this.eqlActNm = "";
                this.ActFlChk = '';
                var surgdata = this.PSModel.toJSON();
                $('#actnmlist').change(function () {
                    $(this).find(":selected").each(function () {
                        that.attrActVal = $(this).val();
                        that.attrActNm = $(this).text();
                        
                        _.each(surgdata.arrcrmpscaseattrvo, function (item) {
                            if (item.attrval == that.attrActVal && item.voidfl == '') {
                                that.eqlActNm = "Y";
                            }
                            if (item.attrval == that.attrActVal && item.voidfl == 'Y') {
                                that.ActFlChk = "Y";
                            }
                        });
                        if(that.attrActVal != 'newAcnt' && that.attrActNm != 'Select'){
                            if(that.eqlActNm == "" || that.ActFlChk == "Y"){
                                that.addNewAcnt();
                            }
                            $("#btn-add-new-acnt").addClass("hide");
                            $("#preceptorship-new-account-search").addClass("hide");
                            
                        }
                        else if(that.attrActNm == 'Select'){
                            $("#btn-add-new-acnt").addClass("hide");
                            $("#preceptorship-new-account-search").addClass("hide");
                        }
                        else if(that.attrActVal == 'newAcnt'){
                            that.attrActVal = '';
                            $("#btn-add-new-acnt").removeClass("hide");
                            $("#preceptorship-new-account-search").removeClass("hide");
                        }
                    });
                });
            },
            
            
            addNewAcnt: function(){
                var that = this;
                this.newAcntName = $("#preceptorship-new-account-search").val();
                
                if (that.newAcntName != '' || that.attrActVal != '') {
                     if (that.divCount == undefined) {
                         that.divCount = 0;
                     } else {
                         that.divCount = that.divCount + 1;
                     }
                }
                
                if(that.attrActVal != ''){
                    $("#preceptorship-hattrid").val(that.attrActVal);
                    $("#preceptorship-hattrid").attr('attrid','');
                    $("#preceptorship-hattrid").attr('attrnm', that.attrActNm);
                    $("#preceptorship-hattrid").attr('attrtype','107101');
                    $("#preceptorship-hattrid").attr('index', that.divCount);

                    $("#preceptorship-hattrid").trigger("change");
                }
                else if(that.newAcntName != ''){
                    $("#preceptorship-hattrid").val('');
                    $("#preceptorship-hattrid").attr('attrid','');
                    $("#preceptorship-hattrid").attr('attrnm', that.newAcntName);
                    $("#preceptorship-hattrid").attr('attrtype','107101');
                    $("#preceptorship-hattrid").attr('index', that.divCount);

                    $("#preceptorship-hattrid").trigger("change");
                }
                that.curPosTgt = $(".gmPanelContent").scrollTop();
                this.newHblAcntChk = 'Y';
                $("#preceptorship-new-account-search").val("");
                that.attrActVal = '';
                that.attrActNm = '';
                
                this.render();
            },
            

            //Preceptorship Type Validation
            psTypeValidation: function () {
                var that = this;
                var productChilds = $(".productItems").children().length;
                var hbAcntChilds = $(".hbAcntitems").children().length;
                var surgNmInpt = $("#preceptorship-surgeon-search input").val();
                var surgNm = $("#preceptorship-hsurgeonnm").val();
                var procNmInpt = $("#preceptorship-procedure input").val();
                var procNm = $("#preceptorship-hprocedurenm").val();
                
                if ($("#pcptyp").val() == "107264") {
                    if ($("#preceptorship-clobj").val() == "") {
                        $("#preceptorship-clobj").attr("validate", "required");
                        $("#preceptorship-clobj").attr("message", "Please provide Call Objective");
                        $("#preceptorship-clobj").addClass("validateError");
                    } else {
                        $("#preceptorship-clobj").removeClass("validateError");
                        $("#preceptorship-clobj").removeClass("gmValidationError");
                        $("#preceptorship-clobj").attr("validate", "");
                    }
                }
                if($("#divtyp").val() == "2004"){
                    if ($("#pcptyp").val() == "") {
                        $("#pcptyp").attr("validate", "required");
                        $("#pcptyp").attr("message", "Please provide Preceptorship Type");
                        $("#pcptyp").addClass("gmValidationError");
                    }
                }
                else {
                        $("#pcptyp").removeClass("gmValidationError");
                        $("#pcptyp").attr("validate", "");
                }
                
                if ($("#pcptyp").val() != "107264") {
                    $("#preceptorship-clobj").removeClass("validateError");
                    $("#preceptorship-clobj").removeClass("gmValidationError");
                    $("#preceptorship-clobj").attr("validate", "");
                }
                
                if($("#divtyp").val() == "2000" || $("#divtyp").val() == "2004"){
                    if (productChilds == 0) {
                        $("#preceptorship-products input").addClass("gmValidationError");
                        $("#preceptorship-products input").attr("message", "Please provide Product");
                    } else {
                        $("#preceptorship-products input").removeClass("gmValidationError");
                    }
                }
                else{
                    $("#preceptorship-products input").removeClass("gmValidationError");
                }
                
                
                if (hbAcntChilds == 0) {
                        $("#actnmlist").attr("validate", "required");
                        $("#actnmlist").addClass("gmValidationError");
                } else {
                        $("#actnmlist").attr("validate", "");
                        $("#actnmlist").removeClass("gmValidationError");
                }
                
                if($("#atndtyp option:selected").val() != 109000){          // If it is not surgeon attendee type
                    $("#attendeenm").attr("message","Please provide Attendee Name");
                    $("#attendeenm").attr("validate","required");  
                    $("#attendeenm").addClass("gmValidationError");
                    $("#preceptorship-surgeon-search input").attr("message","");
                    $("#preceptorship-surgeon-search input").attr("validate","");
                    $("#preceptorship-surgeon-search input").removeClass("gmValidationError");
                    $("#preceptorship-hsurgeon").attr("validate", "");
                    $("#preceptorship-hsurgeon").attr("message", "");
                    $("#preceptorship-surgeon-search input").val("");
                    $("#surgcv").removeAttr("validate");
                    $("#surgcv").removeClass("gmValidationError");
                    $("#surgeon-cv-link").addClass("gmVisibilityHidden");
                }
                else{                        
                    $("#preceptorship-surgeon-search input").attr("message","Please provide Surgeon");
                    $("#preceptorship-surgeon-search input").attr("validate","required");
                    $("#preceptorship-surgeon-search input").addClass("gmValidationError");
                    $("#attendeenm").attr("message","");
                    $("#attendeenm").attr("validate","");  
                    $("#attendeenm").removeClass("gmValidationError");
                    if (surgNmInpt != '') {
                        $("#preceptorship-hsurgeon").attr("validate", "required");
                        $("#preceptorship-hsurgeon").attr("message", "Please provide Surgeon");

                        if (surgNmInpt != surgNm){
                            $("#preceptorship-hsurgeon").val('');
                            $("#surgcv").removeAttr("validate");
                            $("#surgcv").removeClass("gmValidationError");
                            $("#surgeon-cv-link").addClass("gmVisibilityHidden");
                        }
                    }
                }

                if (procNmInpt != '') {
                    $("#preceptorship-hsegmentid").attr("validate", "required");
                    $("#preceptorship-hsegmentid").attr("message", "Please provide Procedure");

                    if (procNmInpt != procNm)
                        $("#preceptorship-hsegmentid").val('');
                }
                
                
                if ($("#pcptyp").val() == "109342" || $("#pcptyp").val() == "109340") {
                    if ($('#webinarfl').is(":checked")) {
                        $("#webinarfltxt").val("Y");
                        $("#webinarfltxt").removeClass("validateError");
                        $("#webinarfltxt").removeClass("gmValidationError");
                        $("#webinarfltxt").attr("validate", "");
                    } else {
                        $("#webinarfltxt").val("");
                        $("#webinarfltxt").attr("validate", "required");
                        $("#webinarfltxt").attr("message", "Please agree to the webinar terms and conditions");
                        $("#webinarfltxt").addClass("validateError");
                    }
                } else {
                    $("#webinarfltxt").val("");
                    $("#webinarfltxt").removeClass("validateError");
                    $("#webinarfltxt").removeClass("gmValidationError");
                    $("#webinarfltxt").attr("validate", "");
                }
                
                if(this.PSModel.get("frmdt") == "")
                    $("#span-preceptorship-frmdt").addClass("validateError");
                else
                    $("#span-preceptorship-frmdt").removeClass("validateError");
                
                if(this.PSModel.get("todt") == "")
                    $("#span-preceptorship-todt").addClass("validateError");
                else
                    $("#span-preceptorship-todt").removeClass("validateError");
            },

            //Select Date min-max Condition Check
            selectDate: function (e) {
                var target = $(e.currentTarget).attr("id");
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1;
                var yyyy = today.getFullYear();
                if (dd < 10)
                    dd = '0' + dd
                if (mm < 10)
                    mm = '0' + mm

                var hours = today.getHours();
                var minutes = today.getMinutes();
                var currntTime = hours + ':' + minutes;
                today = mm + '/' + dd + '/' + yyyy;
                
                var todayFmt = new Date(today);
                var startdate = $("#" + this.module + "-frmdt").val();
                var startDateFmt = new Date(startdate);
                var enddate = $("#" + this.module + "-todt").val();
                var toDateFmt = new Date(enddate);
                
                console.log(startdate + ">>> " + enddate);
                if (startdate != "" && target == this.module + "-frmdt") {
                    $("#" + this.module + "-todt").val(startdate).trigger("change");
                    $("#" + this.module + "-todt").datepicker("setDate" , startdate);
                    $("#" + this.module + "-todt").trigger("change");
                }

                if (startdate != "" && enddate != "" && toDateFmt < startDateFmt) {
                    $("#" + this.module + "-todt").val(startdate).trigger("change");
                    $("#" + this.module + "-todt").datepicker("setDate" , startdate);
                    $("#" + this.module + "-todt").trigger("change");
                }
            },
            
            checkDivVisible: function () {
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1;
                var yyyy = today.getFullYear();
                if (dd < 10)
                    dd = '0' + dd
                if (mm < 10)
                    mm = '0' + mm
                today = yyyy + '-' + mm + '-' + dd;
                
                $("#" + this.module + "-frmdt").attr("min", today);
                $("#" + this.module + "-todt").attr("min", today);

            },


            //Save Case Function
            saveCase: function () {
                var that = this;

                if (this.casereqid == 0) {
                    this.PSModel.set("status", "105765");
                    this.mailStatus = 1; // request
                } else if (parseNull(this.casereqid) > 0 && event.currentTarget.id == "btn-main-save") {
                    this.mailStatus = 2; // update
                }
                this.PSModel.set("type", "105273"); // Case Request 
    
                this.psTypeValidation();
                if (that.gmvValidation.validate()) {
                    var input = that.PSModel.toJSON();
                    if (input.trfstatis != '') {
                        input.trfstatis = parseInt(input.trfstatis);
                    }
                    if (input.surgid != '') {
                        input.surgid = parseInt(input.surgid);
                    }
                    if (input.segid != '') {
                        input.segid = parseInt(input.segid);
                    }
                    if (input.pcprcrf != '') {
                        input.pcprcrf = parseInt(input.pcprcrf);
                    }
                    if (input.pstype != '') {
                        input.pstype = parseInt(input.pstype);
                    }
//                    if (input.frmdt != '') {
//                        input.frmdt = compDateFmt(input.frmdt);
//                    }
//                    if (input.todt != '') {
//                        input.todt = compDateFmt(input.todt);
//                    }
                    if (input.status != '') {
                        input.status = parseInt(input.status);
                    }
                    if (input.type != '') {
                        input.type = parseInt(input.type);
                    }
                    if (input.region != '') {
                        input.region = parseInt(input.region);
                    }
                    var cr_id_check = $("#crId").val();
                    if (cr_id_check != '') {
                        input.casereqid = $("#crId").val();
                    } else {
                        input.casereqid = '';
                    }
                    var divisionCheck = input.division;
                    if (divisionCheck == "2005" || divisionCheck == "2000") {
                        input.workflow = '';
                    }

                    var psTypeCheck = input.pstype;
                    if (psTypeCheck != "107264") {
                        input.clobj = '';
                    }

                    if (input.workflow != '') {
                        input.workflow = parseInt(input.workflow);
                    }
                    if (input.division != '') {
                        input.division = parseInt(input.division);
                    }
                    
                    if (input.atndtyp != "109000") {
                        input.surgid = "";
                    }
                    else{
                        input.atndnm = "";
                    }
                    
                    input.arrcrmpscaseassvo = _.each(input.arrcrmpscaseassvo,function(item){
                        delete item['vpnm'];
                        delete item['adnm'];
                    });
                    

                    if ($("#preceptorship-procedure input").val() != "") {
                        input.segid = input.segid;
                        input.procnm = input.procnm;
                    } else {
                        input.segid = '';
                        input.procnm = '';
                    }
                    
                    fnGetWebServerData('pstxn/casereq', 'gmCRMPSCaseVO', input, function (data) {
                        if (data != null) {
                            var caseReqID = data.casereqid;
                            showSuccess("Case Request Saved Successfully");
                            window.location.href = "#crm/preceptorship/case/id/" + caseReqID;
                            if(parseNull(GM.Global.SchedulerFL)=="Y"){
                                that.casereqid=caseReqID;
                                that.hcid=GM.Global.PSHostActivity.actid;
                                GM.Global.SchedulerFL="";
                                GM.Global.PSHostActivity="";
                                GM.Global.preceptorship.Mode = "view";
                                that.selectedDone();
                            }
                            else{
                            GM.Global.preceptorship.Mode = "view";
                            Backbone.history.loadUrl(Backbone.history.fragment);
                            }
                                

                            if (GM.Global.preceptorship.Mode == "create" && that.mfu != undefined) {
                                var filesCnt = 0;
                                if (that.mfu.fileData != "") {
                                    that.mfu.uploadFile(caseReqID, function (dt) {
                                        filesCnt++;
                                        if (filesCnt == that.mfu.fileData.length) {
                                            that.getUploadFiles(caseReqID);
                                          //  that.sendCaseReqEmail(caseReqID);
                                        }
                                    });
                                }
                            }
  /*                          else if (GM.Global.preceptorship.Mode == "create") {
                                that.sendCaseReqEmail(caseReqID);
                            }*/
                                     
                            if (GM.Global.preceptorship.Mode == "create" && that.afu != undefined) {
                                var filesCnt = 0;
                                if (that.afu.fileData != "") {
                                    that.afu.uploadFile(caseReqID, function (dt) {
                                        filesCnt++;
                                        if (filesCnt == that.afu.fileData.length) {
                                            that.getAdminUploadFiles(caseReqID);
                                        }
                                    });
                                }
                            }


                        } else {
                            showError("Save Failed");
                        }
                    });
                } else {
                    var caseErr = that.gmvValidation.getErrorsTag();
                    dhtmlx.alert({
                        title: "Alert",
                        type: "alert-error",
                        text: caseErr
                    }); 
                }
            },

            //Fetch the Uploaded Request Files
            getUploadFiles: function (caseReqID) {
                var that = this;
                if ((GM.Global.preceptorship.Mode == "view") || (GM.Global.preceptorship.Mode == "edit") || (GM.Global.preceptorship.Mode == "create")) {

                    var input = {
                        "token": localStorage.getItem("token"),
                        "refid": caseReqID,
                        "refgroup": "107361"
                    };

                    if (parseNull(caseReqID) != "")
                        input["refid"] = caseReqID;
                    console.log("getUploadFiles" + JSON.stringify(input));
                    fnGetWebServerData("file/get", "gmCRMFileUploadVO", input, function (file) {
                        console.log("getUploadFiles files" + JSON.stringify(file));
                        if (file != null) {
                            file = arrayOf(file);
                            that.rdFiles = file;
                            $(".upload-files-list").html("");
                            _.each(file, function (v) {
                                var docLink = URL_CRMFileServer + v.refgroup + "/" + v.refid + "/" + v.filename;
                                $(".upload-files-list").append("<div class = 'a-view-docs gmLnk gmUnderline gmPaddingButtom10p'><span class='hide hidden-file'>" + docLink + "</span> <a class='' href = '" + URL_CRMFileServer + v.refgroup + "/" + v.refid + "/" + v.filename + "'  target='_blank'>" + v.filetitle + " - " + v.updatedby + " on " + v.updateddate + "</a></div>");
                            });
                        }
                    });
                }
            },

            //Fetch Uploaded Admin files
            getAdminUploadFiles: function (caseReqID) {
                var that = this;
                if ((GM.Global.preceptorship.Mode == "view") || (GM.Global.preceptorship.Mode == "edit") || (GM.Global.preceptorship.Mode == "create")) {

                    var input = {
                        "token": localStorage.getItem("token"),
                        "refid": caseReqID,
                        "refgroup": "107360"
                    };

                    if (parseNull(caseReqID) != "")
                        input["refid"] = caseReqID;

                    console.log("getAdminUploadFiles" + JSON.stringify(input));
                    fnGetWebServerData("file/get", "gmCRMFileUploadVO", input, function (file) {
                        console.log("getAdminUploadFiles files" + JSON.stringify(file));
                        if (file != null) {
                            file = arrayOf(file);
                            that.adFiles = file;
                            $(".upload-admin-list").html("");
                            _.each(file, function (v) {
                                var docLink = URL_CRMFileServer + v.refgroup + "/" + v.refid + "/" + v.filename;
                                $(".upload-admin-list").append("<div class = 'a-view-docs gmLnk gmUnderline gmPaddingButtom10p'><span class='hide hidden-file'>" + docLink + "</span> <a class='' href = '" + URL_CRMFileServer + v.refgroup + "/" + v.refid + "/" + v.filename + "' target='_blank'>" + v.filetitle + " - " + v.updatedby + " on " + v.updateddate + "</a></div>");
                            });
                        }
                    });
                }
            },
            
            // Function used for send email notification while create the case request
            sendCaseReqEmail: function(caseReqID) {
                var that = this;
                var input = {
                        "token": localStorage.getItem("token"),
                        "casereqid": caseReqID,
                    };
                console.log("Case Request Email Input" + JSON.stringify(input));
                fnGetWebServerData("pstxn/casereqmail", undefined, input, function (emailcb) {
                    console.log("Case Request Email Output" + JSON.stringify(emailcb));
                });
            },
            
            //Edit Case Function
            editCase: function () {
                var that = this;
                GM.Global.preceptorship.Mode = "edit";
                that.render();
            },

            //validation for npid
            checkNPID: function (e) {
                checkNPI(e);
            },


            //Create New Surgeon
            createSurgeon: function () {
                var that = this;
                var template_var = this.template_CreateSurgeon();
                popCreateNewSurgeon(template_var,DropDownView);
                $("#frm-documents").addClass("cr-form-cv");
            },

            //back to Page Function
            backToPage: function () {
                if (GM.Global.preceptorship.Mode == "edit")
                    Backbone.history.loadUrl(Backbone.history.fragment);
                else
                    window.history.back();
            },

            //cancel the case when edit case
            cancelCase: function (e) {
                var that = this;
                showPopup();
                $("#div-crm-overlay-content-container").find('.submit-model').removeClass("hide");
                $("#div-crm-overlay-content-container").find('.cancel-model').removeClass("hide");
                $("#div-crm-overlay-content-container").find('.done-model').addClass("hide");
                $("#div-crm-overlay-content-container").find('.load-model').addClass("hide");
                $("#div-crm-overlay-content-container").find('.modal-title').text("REASON FOR CANCELLATION");
                $("#div-crm-overlay-content-container .modal-body").html(this.template_VisitCancelReq());
                getCLValToDiv("#canreson", "", "", "CASCAN", "", "", "codeid", "canreson", false, true, function (divID, attrVal) {});

                $("#cancel-req-error").css("display", "none");
                $("#div-crm-overlay-content-container").find('.submit-model').unbind('click').bind('click', function () {
                    var reasonval = $("#canreson option:selected").val();
                    var commentVal = $("#textarea-visit-cancel-req").val();
                    if (parseNull(reasonval) != "" && parseNull(commentVal) != "") {
                        $("#div-crm-overlay-content-container").find('.submit-model').addClass("hide");
                        $("#div-crm-overlay-content-container").find('.cancel-model').addClass("hide");
                        var reasonNm = $("#canreson option:selected").text();
                        var reasonID = $("#span-visit-reason").attr("data_attrid");

                        that.updateAttrVO(reasonID, "107583", reasonval, reasonNm); // 107583 - Case request comments type 

                        var commentID = $("#span-visit-comment").attr("data_attrid");
                        that.updateAttrVO(commentID, "26240350", commentVal, "cancmt"); // 26240350	- Cancel Comment
                        hidePopup();
                        that.PSModel.set("status", "105762"); // 105762 - Cancelled
                        that.mailStatus = 4; // cancel
                        that.saveCase(e);
                    } else if (parseNull(reasonval) == "")
                        $("#cancel-req-error").html("Please select reason").slideDown('fast').css("display", "block").delay(4000).slideUp('slow');
                    else if (parseNull(commentVal) == "")
                        $("#cancel-req-error").html("Please enter the comment").slideDown('fast').css("display", "block").delay(4000).slideUp('slow');
                });
                $("#div-crm-overlay-content-container").find('.cancel-model').unbind('click').bind('click', function () {
                    $("#div-crm-overlay-content-container").find('.submit-model').addClass("hide");
                    $("#div-crm-overlay-content-container").find('.cancel-model').addClass("hide");
                    hidePopup();
                    that.backToPage();
                });
            },
            
            //Request DetailsFile Upload Function
            rdFileUpload: function (event) {
                var that = this;
                if (this.mfu)
                    this.mfu.render();
                else
                    this.mfu = new GMVMultiFileUpload({
                        arrAcceptetFormat: ['doc', 'docx', 'pdf', 'txt', 'xlsx', 'xls', 'ppt', 'pptx'],
                        parent: this,
                        parentID: this.casereqid,
                        viewMode: GM.Global.preceptorship.Mode,
                        refgroup: 107361,
                        fileData: that.rdFiles
                    });

            },

            //Admin Details File Upload Function
            adminFileUpload: function (event) {
                var that = this;
                if (this.afu)
                    this.afu.render();
                else
                    this.afu = new GMVMultiFileUpload({
                        arrAcceptetFormat: ['doc', 'docx', 'pdf', 'txt', 'xlsx', 'xls', 'ppt', 'pptx'],
                        parent: this,
                        parentID: this.casereqid,
                        viewMode: GM.Global.preceptorship.Mode,
                        refgroup: 107360,
                        fileData: that.adFiles
                    });

            },

        selectedDone: function(){
                var that=this;
                var input = {
                    'token': localStorage.getItem('token')
                };
                
                if(parseNull(that.hcid)=="" && parseNull(that.PSModel.get("actid")) == ""){
                	displayPopupMsg("Select Atleast One Host Case Before Clicking Done Button");   
                }else if(parseNull(that.hcid) == parseNull(that.PSModel.get("actid"))){
                	 displayPopupMsg("Nothing changed to save");
                }else{
                	
	                if(parseNull(that.hcid)!=""){
	                	input.actid=that.hcid;// New Host case
	                    input.linkreqs=[this.casereqid]; //Scheduled
	                    input.unlinkreqs=[];
	                }else{
	                	input.actid=parseNull(that.PSModel.get("actid")); //old Host case
	                	input.linkreqs=[];
	                    input.unlinkreqs=[this.casereqid];  //Open
	                }
	                
	                fnGetWebServerData("pstxn/casereqmap",undefined, input, function (data) {     
	                    if(data != null){
	                    hidePopupPSReport();
                            var msg="";                    
	                    if(data.linkreqs.length>0)    
	                       msg = that.casereqid + " Successfully Assigned to " + data.actid;
	                    if(data.unlinkreqs.length>0)
                           msg = that.casereqid + " Successfully Unlinked from " + data.actid;
                        showSuccess(msg);                            
	                    Backbone.history.loadUrl();
	                    }
	                    else{
	                     showError("Save Failed");   
	                    }
	                    
	                });
                }
                
            },
            
            // Show lookup host case popup
            showlkpHostCase: function () {
                var that = this;
                
                
                var lkinput = {
                    'token': localStorage.getItem('token')
                };
                lkinput.segid=that.PSModel.get("segid");
                lkinput.actcategory="105272";
                
                var title=that.PSModel.get("casereqid")+" - "+that.PSModel.get("surgnm");
                this.reqid = that.PSModel.get("casereqid");
                this.hcid= that.PSModel.get("actid");
                
                
                showPopupPSReport();
                $("#div-crm-overlay-content-container .modal-title").text(title);
                $("#div-crm-overlay-content-container .modal-body").html("<div id='gridPreceptorRpt'></div>");
                getLoader("#gridPreceptorRpt");
                $("#div-crm-overlay-content-container").find('.done-model').unbind('click').bind('click', function () {
                that.selectedDone();
                });
                
                this.gridObj = "";
                fnGetWebServerData("psrpt/fetchlkuphcase",undefined, lkinput, function (data) {
                    if(data != null){
                var rows = [], i = 0;
                   
                            _.each(data, function (griddt) {
                                var partyidarr =  parseNull(griddt.partyid).split(" / ");
                                var partynmarr =  parseNull(griddt.partynm).split(" / ");
                                var partyData = "";
                                for (j = 0; j < partyidarr.length; j++) {
                                    if (partyData != "")
                                        partyData += " / ";
                                    partyData += "<a class='gmReportLnk' href='#crm/surgeon/id/" + partyidarr[j] + "'>" + partynmarr[j] + "</a>";
                                }
                                rows[i] = {};
                                rows[i].id = i;
                                rows[i].data = [];
                                if($(window).width() > 767){
                                if(griddt.actid==that.PSModel.get("actid")){
                                    rows[i].data[0] = true;
                                    that.hcid=griddt.actid;
                                }
                                else
                                    rows[i].data[0] = false;
                                }
                                else{
                                    if(griddt.actid==that.PSModel.get("actid"))
                                        rows[i].data[0] = '<input type="checkbox" checked value="1">';
                                    else
                                        rows[i].data[0] = '<input type="checkbox" value="0">';
                                }
                                    
//                                rows[i].data[1] = '<span data-surgid="'+griddt.partyid+'">'+griddt.partynm+'</span>';
                                rows[i].data[1] = partyData;
                                rows[i].data[2] = griddt.segnm;
                                rows[i].data[3] = griddt.products;
                                rows[i].data[4] = griddt.actid;
                                rows[i].data[5] = griddt.statusnm;
                                rows[i].data[6] = compDateFmt(griddt.actstartdate);
                                i++;
                            });
                            var dataHost = {};
                            dataHost.rows = rows;
                $(".done-model").removeClass("hide");
                $(".cancel-model").removeClass("hide");
                var setInitWidths = "30,200,150,250,100,*,150";
                var setColAlign = "center,left,left,left,left,center";
                var setColTypes = "ch,ro,ro,ro,ro,ro,ro";
                var setColSorting = "str,str,str,str,str,str,str";
                var setHeader = ["", "Surgeon Name", "Procedure", "Product(s)", "Host-ID","Status", "Case Date"];
                var setFilter = ["", "#text_filter", "#text_filter", "#text_filter", "#text_filter","#text_filter","#text_filter"];
                var enableTooltips = "true,true,true,true,true,true,true";
                var footerArry = [];
                var gridHeight = "";
                var setFilter = "";
                var footerStyles = [];
                var footerExportFL = true;
                var mobResArg=false;
                dhxReportDataFmt(rows,setHeader, function(dt){
                    that.rptTemplate = that.template_DhxReportMobile(dt);
                });
                that.gridObj = loadDHTMLXGrid('gridPreceptorRpt', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL, that.rptTemplate,mobResArg);
                if ($(window).width() > 767) {
                    for (var i = 0; i < that.gridObj.getRowsNum(); i++) {
                        that.gridObj.cells(i, 1).cell.className = 'not_m_line';
                        that.gridObj.cells(i, 2).cell.className = 'gmEllipses';
                        that.gridObj.cells(i, 3).cell.className = 'gmEllipses';
                        that.gridObj.cells(i, 4).cell.className = 'gmReportLnk gmEllipses';
                    }
                    that.gridObj.attachEvent("onRowSelect", function (id, index) {
                        if (index == 0) {
                            var chkVal = that.gridObj.cells(id, 0).getValue();
                            if (chkVal == 1) {
                                that.gridObj.cells(id, 0).setValue(false);
                                that.hcid = "";
                            } else {
                                that.hcid = that.gridObj.cells(id, 4).getValue();
                                for (var i = 0; i < that.gridObj.getRowsNum(); i++) {
                                    that.gridObj.cells(i, 0).setValue(false);
                                }
                                that.gridObj.cells(id, 0).setValue(true);
                            }
                        }
                        if (index == 1) {
                            //                             var element = that.gridObj.cells(id, 1).getValue();
                            hidePopupPSReport();
                            //                             window.location.href = '#crm/surgeon/id/' + $(element).data('surgid');
                        } else if (index == 4) {
                            hidePopupPSReport();
                            window.location.href = '#crm/preceptorship/host/id/' + that.gridObj.cells(id, 4).getValue();
                        }
                    });
                    that.gridObj.attachEvent("onCheck", function (rId, cInd, state) {
                        if (state == true) {
                            that.hcid = that.gridObj.cells(rId, 4).getValue();
                            for (var i = 0; i < that.gridObj.getRowsNum(); i++) {
                                that.gridObj.cells(i, cInd).setValue(false);
                            }
                            that.gridObj.cells(rId, cInd).setValue(true);
                        } else
                            that.hcid = "";
                    });
                    }
                    else{
                        $("#gridPreceptorRpt").find("input").change(function(){
                                $("#gridPreceptorRpt .div-dhx-report-mbl").each(function(){
                                    $(this).children(".div-dhx-report-mbl-header").find("input").prop("checked", false);
                                });
                                $(this).prop("checked", true);
                                if($(this).is(":checked")){
                                    that.hcid = $(this).closest(".div-dhx-report-mbl-header").siblings(".div-dhx-report-mbl-body").children(".ul-dhx-details:nth-child(1)").find(".div-dhx-report-value").text();
                                    console.log(that.hcid);
                                }
                                else{
                                    that.hcid = "";
                                }
                            });
                        
                        $(".mob-act-id").on("click", function () {
                            var actid = $(this).data("actid");
                            window.location.href = '#crm/preceptorship/host/id/' + actid;
                            hidePopupPSReport();
                        });
                        
                    }
                        
                    } else 
                        $("#gridPreceptorRpt").html("No Data Found");
                    
                  });
                
            },
            
            
            //Toggle Preceptorship Type Info
            openNotifyPSType: function () {
                
                var that = this;
                    $("#rpt-legend-detail").html(that.legendtype[0].spine);
                    $("#rpt-legend-detail-inr").html(that.legendtype[0].inr);
                if (this.PSModel.get("division") == "2004") {
                    $("#rpt-legend-detail-inr").slideToggle();
                } else {
                    $("#rpt-legend-detail").slideToggle();
                }
            },
            
            // upload Cv for Surgeon
            uploadCVSurgeon: function(e){
                var that  = this;
                if($(e.currentTarget).hasClass("inactive")){
                    var surgid=$(e.currentTarget).data("surgid");
                    that.renderDocList();
                    $("#frm-documents").addClass("display-flex cr-form-cv");
                }
                if($(e.currentTarget).hasClass("active")){
                   var surgid=$(e.currentTarget).attr("data-surgid");
                   var refgrp=$(e.currentTarget).attr("data-refgrp");
                   var filenm=$(e.currentTarget).attr("data-filenm");                   
                   var docLink = URL_CRMFileServer + refgrp + "/" + surgid + "/" + filenm;
                   window.open(docLink, '_blank', 'location=no');                    
                }
            },
            
            // Document Render Function
            renderDocList: function(){
                var that = this;
                var surgData = that.PSModel.toJSON(); 
                 showPopup();
                 $("#div-crm-overlay-content-container").find('.modal-footer').addClass("hide");
                 $("#div-crm-overlay-content-container").find('.modal-title').text("CV File Upload");
                 $("#div-crm-overlay-content-container").find('.modal-body').html(that.template_FileuploadDoc());
                
                browseCVFile();
                
                $("#span-fileupload-browse-filenm").addClass("gmEllipses");
                // upload CV with data save Function
                $("#li-fileupload-upload").click(function(){
                    if($("#txt-fileupload").val() != ''){
                        var surgType = 'N';
                        uploadCVFile(surgData.surgid,surgType);
                        $("#frm-documents").removeClass("cr-form-cv");
                    }
                    else{
                        displayPopupMsg("Please select a File");
                    }
                });
            },
            
            fetchSegSysList: function() {
                var that = this;
                showPopupSegSysList();
                $("#div-crm-overlay-content-container .modal-title").text("Procedure Product Lookup");
                $("#div-crm-overlay-content-container .modal-body").html("<div id='gridSegSysRpt'></div>");
                getLoader("#gridSegSysRpt");
                
                var sysinput = {
                    'token': localStorage.getItem('token')
                };
                
                this.gridObj = "";
                fnGetWebServerData("psrpt/fetchsegsys",undefined, sysinput, function (data) {
                    if(data != null){
                        var rows = [], i = 0;
                            _.each(data, function (griddt) {
                                rows[i] = {};
                                rows[i].id = i;
                                rows[i].data = [];  
                                rows[i].data[0] = '<span data-segid="'+griddt.segid+'"  data-segnm="'+griddt.segnm+'">'+griddt.segnm+'</span>';
                                rows[i].data[1] = griddt.sysnm;
                                i++;
                            });
                            var dataSys = {};
                            dataSys.rows = rows;
                        
                        if($(window).width() > 767){
                                  var setInitWidths = "250,*";
                             }
                        else{
                            var setInitWidths = "130,*";
                        }
                           // var setInitWidths = "250,*";
                            var setColAlign = "left,left";
                            var setColTypes = "ro,ro";
                            var setColSorting = "str,str";
                            var setHeader = ["Procedure", "Product"];
                            var setFilter = ["#text_filter", "#text_filter"];
                            var enableTooltips = "true,true";
                            var footerArry = [];
                            var gridHeight = "";
                            var footerStyles = [];
                            var footerExportFL = true;
                            var mobResArg=true;                       
                        
                        
                        dhxReportDataFmt(rows,setHeader, function(dt){
                           that.rptTemplate = that.template_DhxReportMobile(dt);
                       });
                        
                        
                            that.gridObj = loadDHTMLXGrid('gridSegSysRpt', gridHeight, dataSys, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, setFilter, footerArry, footerExportFL, footerStyles, that.rptTemplate, mobResArg);  
                    }
                    for (var i = 0; i < that.gridObj.getRowsNum(); i++) {
                         that.gridObj.cells(i, 0).cell.className = 'gmEllipses';
                         that.gridObj.cells(i, 1).cell.className = 'gmEllipses';
                    }
                    that.gridObj.attachEvent("onRowSelect", function (id, index) {
                        var chkVal = that.gridObj.cells(id, 0).getValue();
                        var setSegid = $(chkVal).data('segid');
                        var setSegnm = $(chkVal).data('segnm');
                        var pcrData = {
                            "ID" : setSegid,
                            "Name" : setSegnm
                        }
                        that.setProcedureInfo(pcrData);
                        hidePopupSegSysList();
                    });
                });
            },
           getZoneVal: function (region,trig) {
                var that = this;
                getCLValToDiv("#zone", "", "", "ZONE", "", region, "codeid", "zone", false, true, function (divID, attrVal) {
                    if (that.PSModel.get(attrVal) != "")
                         that.setSelect(divID, that.PSModel.get(attrVal));  
                    else {
                        if (trig) {  
                            var input = {
                                "token": localStorage.getItem("token")
                            };
                            fnGetWebServerData("psrpt/fetchsrepzone", undefined, input, function (data) { // To get the sales rep zone
                                if (data != null) {
                                    that.setSelect(divID, data.ID);
                                    if(!$("#zone option").is(":selected"))
                                        that.setSelect(divID, "");
                                    $(divID).trigger("change");
                                }  
                                else {
                                    that.setSelect(divID, ""); //set default value
                                    $(divID).trigger("change");
                                }
                            });
                        }
                        else {
                            that.setSelect(divID, ""); //set default value
                            $(divID).trigger("change");
                        } 
                    }
                });
            },
            
           changeZoneVal: function(e){
                var that=this;
                var selRegion = $(e.currentTarget).find("option:selected").val();
                that.setSelect("#zone", ""); //set default value
                $("#zone").trigger("change");
                that.getZoneVal(selRegion,false);
            },

        });
        return GM.View.PSCase;
    });
