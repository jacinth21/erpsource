/**********************************************************************************
 * File:        gmrPreceptor.js
 * Description: Router for all of Preceptorship
 * Version:     1.0
 * Author:      mahendrand
 **********************************************************************************/
define([

  // External Libraries
  'jquery', 'underscore', 'backbone',

  //Common
    'gmvCRMMain', 'gmvCRMData',

  //Views
   'gmvPSCriteria', 'gmvPSHost', 'gmvPSCase', 'gmvPSSchedule', 'gmvPSList'
],

    function ($, _, Backbone,
        GMVCRMMain, GMVCRMData,
        GMVPSCriteria, GMVPSHost, GMVPSCase, GMVPSSchedule, GMVPSList) {

        'use strict';

        GM.Router.PS = Backbone.Router.extend({

            //routes to its respective web page 
            routes: {
                "crm/preceptorship": "viewPreceptorship",
                "crm/preceptorship/host/id/:actid": "hostDetail",
                "crm/preceptorship/case/id/:casereqid": "caseDetail",
                "crm/preceptorship/schedule": "viewCaseSchedule",
                "crm/preceptorship/host/report/:viewtype": "hostReport",
                "crm/preceptorship/case/report/:viewtype": "hostReport",
                "crm/preceptorship/host/search/:firstnm": "lookupByName",
                "crm/preceptorship/request/search/:searchnm": "lookupReqByName",
                "crm/preceptorship/host/report/:sdate/:edate": "lookupByDate",
                "crm/preceptorship/request/report/:sdate/:edate": "lookupReqByDate",
                "crm/preceptorship/:casetype/:divisionid/:statusid": "lookupReqByChartPlot",

            },

            //things to execute before the routing
            execute: function (callback, args, name) {
                console.log(GM.Global.CRMMain)
                var lastEl = GM.Global.Url[GM.Global.Url.length-2];
                   if(lastEl.indexOf("#crm") == -1)
                GM.Global.CRMMain=undefined;
                if (GM.Global.CRMMain == undefined) {
                    commonSetUp();
                    GM.Global.Module = "preceptor";
                    GM.Global.CRMMain = true;
                    var gmvCRMMain = new GMVCRMMain();
                    gmvApp.showView(gmvCRMMain);
                }
                $('.gmTab').removeClass().addClass('gmTab');
                $("#preceptor").addClass("gmTab tabpreceptorship gmFontWhite");
                if (!$(".alert").hasClass("alertSuccess")) {
                    if (GM.Global.AlertMsg) {
                        showError(GM.Global.AlertMsg);
                        GM.Global.AlertMsg = undefined;
                    } else
                        hideMessages();
                }
                if (callback) callback.apply(this, args);
            },
            initialize: function () {
                $("#div-app-title").show();
                $("#span-app-title").html("Preceptor");
                $(".li-icon-back").show();
                GM.Global.PSHost = null;
                $('#div-app-title').removeClass('gmVisibilityHidden').addClass('gmVisibilityVisible');
            },
            //View Preceptorship
            viewPreceptorship: function () {
                GM.Global.PSScheduler = undefined;
                var gmvPSCriteria = new GMVPSCriteria({
                    "view": ""
                });
                gmvApp.showView(gmvPSCriteria);
            },
            //View Case Schedule
            viewCaseSchedule: function () {
                var gmvPSSchedule = new GMVPSSchedule({
                    "view": ""
                });
                gmvApp.showView(gmvPSSchedule);
            },
            hostReport: function (viewtype) {

                console.log(viewtype);
                var gmvPSList = new GMVPSList({
                    "view": viewtype
                });
                gmvApp.showView(gmvPSList);

            },
            lookupByName: function (firstnm) {
                var gmvPSList = new GMVPSList({
                    "firstnm": firstnm
                });
                gmvApp.showView(gmvPSList);

            },
            lookupReqByName: function (searchnm) {
                console.log(searchnm);
                var gmvPSList = new GMVPSList({
                    "name": searchnm
                });
                gmvApp.showView(gmvPSList);

            },
            lookupByDate: function (sdate, edate) {
                var gmvPSList = new GMVPSList({
                    "sdate": sdate,
                    "edate": edate
                });
                gmvApp.showView(gmvPSList);
            },
            lookupReqByDate: function (reqsdate, reqedate) {
                var gmvPSList = new GMVPSList({
                    "reqsdate": reqsdate,
                    "reqedate": reqedate
                });
                gmvApp.showView(gmvPSList);
            },


            // Host Case Detail on create, view and Edit
            hostDetail: function (actid) {
                console.log("Activity Detail Mode Preceptorship Host Case");
                if (GM.Global.Preceptorship == undefined)
                    GM.Global.Preceptorship = null;
                var input = {
                    "token": localStorage.getItem("token"),
                    "actid": actid
                };

                if (actid != 0) {
                    var template = fnGetTemplate(URL_Common_Template, "gmtSpinner");
                    $("#div-crm-module").html(template());

                    console.log("Input is " + JSON.stringify(input));
                    fnGetWebServerData("crmactivity/info", "gmCRMActivityVO", input, function (data) {
                        if (data != null && data.actid != "") {
                            data = data.GmCRMListVO.gmCRMActivityVO[0];
                            data.arrgmactivitypartyvo = arrayOf(data.arrgmactivitypartyvo);
                            data.arrgmactivityattrvo = arrayOf(data.arrgmactivityattrvo);
                            data.arrgmcrmaddressvo = arrayOf(data.arrgmcrmaddressvo);
                            data.arrgmcrmlogvo = arrayOf(data.arrgmcrmlogvo);
                            console.log("Activity Info Data >>>>> " + JSON.stringify(data));
                            var gmvPSHost = new GMVPSHost({
                                module: "preceptorship",
                                "actid": actid,
                                "data": data
                            });
                            gmvApp.showView(gmvPSHost);
                        } else {
                            GM.Global.AlertMsg = "No Such Activity";
                            window.location.href = "#crm/preceptorship";
                        }
                    });

                } else {
                    var gmvPSHost = new GMVPSHost({
                        module: "preceptorship",
                        "actid": 0,
                        "data": {}
                    });
                    gmvApp.showView(gmvPSHost);

                }
            },


            caseDetail: function (casereqID) {
                if (casereqID != 0) {

                    var input = {
                        "token": localStorage.getItem("token"),
                        "casereqid": casereqID
                    };
                    getLoader(".gmPanelContent");
                    fnGetWebServerData("psrpt/caseinfo", 'gmCRMPSCaseVO', input, function (data) {
                        if (data != null) {
                            var gmvPSCase = new GMVPSCase({
                                "Mode": 'view',
                                "casereqid": casereqID,
                                "data": data
                            });
                            gmvApp.showView(gmvPSCase);
                        } else {
                            GM.Global.AlertMsg = "No Such Case Request";
                            window.location.href = "#crm/preceptorship";

                        }
                    }, true);
                } else {
                    var gmvPSCase = new GMVPSCase({
                        "casereqid": 0,
                        "data": ""
                    });
                    gmvApp.showView(gmvPSCase);
                }
            },
            
            lookupReqByChartPlot:function(casetype,divisionid,statusid){
                var gmvPSList = new GMVPSList({
                    "divisionid": divisionid,
                    "statusid": statusid,
                    "view": casetype
                });
                gmvApp.showView(gmvPSList);
            },

        });
        return GM.Router.PS;
    });
