    /**********************************************************************************
     * File             :        gmvMercReport.js
     * Description      :        View to represent Merc Attendee Report List
     * Version          :        1.0
     * Author           :        mahendrand
     **********************************************************************************/
    define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars', 'jqueryui',

    // Common
    'global', 'commonutil',

    // View

    // Pre-Compiled Template
    'gmtTemplate'

    //Model

],

        function ($, _, Backbone, Handlebars, JqueryUI, Global, Commonutil, GMTTemplate) {

            'use strict';

            GM.View.MercReport = Backbone.View.extend({

                el: "#div-crm-module",

                name: "CRM Merc Report View",

                /* Load the templates */
                template_MainModule: fnGetTemplate(URL_Common_Template, "gmtCRMMainModule"),
                template_MercReport: fnGetTemplate(URL_CRMMerc_Template, "gmtMercReportMain"),
                template_MercList: fnGetTemplate(URL_CRMMerc_Template, "gmtMercPrgmList"),
                template_MercReportList: fnGetTemplate(URL_CRMMerc_Template, "gmtMercReportList"),
                template_MERCReportMain: fnGetTemplate(URL_CRMMerc_Template, "gmtMERCReport"),

                events: {
                    "click #select-merc": "showMercList",
                    "click .ulMercList": "selectMerc",
                    "click #report-btn-mercsurgrpt": "viewAttendeeRpt",
                    "click #rpt-legend-icon": "showLegend",
                    "click .mercsurgrpt": "toggleMercSurgeonRpt"
                },

                initialize: function (options) {
                    console.log(options);
                    this.viewby = options.reportby;
                    this.module = options.module;
                    this.options = options;
                    $(window).on('orientationchange', this.onOrientationChange);
                    if (GM.Global.Merc == undefined)
                        GM.Global.Merc = {};
                    if (GM.Global.Merc.FilterID == undefined)
                        GM.Global.Merc.FilterID = {};
                    if (GM.Global.Merc.FilterName == undefined)
                        GM.Global.Merc.FilterName = {};
                    if (typeof (GM.Global.Merc.FilterID) != 'object')
                        GM.Global.Merc.FilterID = JSON.parse(GM.Global.Merc.FilterID);
                    if (typeof (GM.Global.Merc.FilterName) != 'object')
                        GM.Global.Merc.FilterName = JSON.parse(GM.Global.Merc.FilterName);
                },

                render: function () {
                    var that = this;
                    this.$el.html(this.template_MainModule({
                        "module": this.module
                    }));
                    $("#crm-btn-back").show();
                    $("#div-app-title").show();
                    $("#div-crm-panel-title").html("MERC Attendee Report");  

                    GM.Global.Merc.FilterID["reporttype"] = this.viewby;
                    this.$("#div-main-content").html(this.template_MercReport(GM.Global.Merc.FilterID));
                    
                    
                    switch(this.viewby){
                        case "mercsurgrpt":
                            $(".gmPanelTitle").html("MERC Attendee Report");
                            this.renderMercSelect();
                            if (parseNull(GM.Global.Merc.FilterID["mercid"]) != '')
                                that.viewAttendeeRpt();
                            break;
                        case "rptbyregion":
                            var input = {
                                "token": localStorage.getItem("token"),
                                "regnid": that.options.regnid
                            }
                            that.loadMercReport(input);
                            break;
                        case "rptbyname":
                            var input = {
                                "token": localStorage.getItem("token"),
                                "title": that.options.title
                            }
                            that.loadMercReport(input);
                            break;   
                        case "rptbysurgname":
                            var input = {
                                "token": localStorage.getItem("token"),
                                "firstnm": GM.Global.Merc["firstnm"],
                                "lastnm": GM.Global.Merc["lastnm"]
                            }
                            that.loadMercReport(input);
                            break;                            
                        case "rptbyfilter":
                            var input = {
                                "token": localStorage.getItem("token"),
                                "state": GM.Global.Merc.FilterID["state"],
                                "country": GM.Global.Merc.FilterID["country"],                                
                                "fromdt": GM.Global.Merc.FilterID["mercstartdate"],
                                "todt": GM.Global.Merc.FilterID["mercenddate"]
                            }
                            that.loadMercReport(input);
                            break;
                        case "rptbywebinar":
                            var startDtYr = "01/01/" + (new Date()).getFullYear();
                            var endDtYr = "12/31/" + (new Date()).getFullYear();
                            var input = {                             
                                    "fromdt": startDtYr,
                                    "todt": endDtYr,
                                    "type": "26241144" // codelookup id of webinar MERCTY
                                         }
                             that.loadMercReport(input);
                             break;
                        }


                },

                renderMercSelect: function () {
                    var that = this;
                    var userData = JSON.parse(localStorage.getItem("userData"));
                    var input = {
                        'token': localStorage.getItem('token'),
                        'attendeefl': 'Y'  // for getting Merc Program which has surgeon attendees
                    };
                    input.regnid = (userData != undefined && userData.cmpid != "1000") ? "100824" : "100823";

                    fnGetWebServerData("merc/dash", undefined, input, function (mercdt) {
                        console.log(mercdt);
                        that.$(".search-result-merc").html(that.template_MercList(mercdt));
                    });

                },

                showMercList: function () {
                    if ($('.search-result-merc').hasClass('hide'))
                        $('.search-result-merc').removeClass('hide');
                    else
                        $('.search-result-merc').addClass('hide');
                },

                selectMerc: function (e) {
                    var mercID = $(e.currentTarget).data('mercid');
                    var mercNM = $(e.currentTarget).data('mercnm');
                    $('#select-merc').text(mercNM);
                    $('#select-merc').attr('data-mercid', mercID);
                    GM.Global.Merc.FilterName["mercid"] = parseNull(mercID);
                    GM.Global.Merc.FilterID["mercid"] = parseNull(mercID);
                    GM.Global.Merc.FilterName["mercnm"] = parseNull(mercNM);
                    GM.Global.Merc.FilterID["mercnm"] = parseNull(mercNM);
                    if (!$('.search-result-merc').hasClass('hide'))
                        $('.search-result-merc').addClass('hide');
                },

                viewAttendeeRpt: function () {
                    var that = this;
                    if (parseNull(GM.Global.Merc.FilterID["mercid"]) == "")
                        showError("Please select atleast one filter");
                    else {
                        var input = {
                            'token': localStorage.getItem('token')
                        };
                        input.mercid = GM.Global.Merc.FilterID["mercid"];
                        var tempSpinner = fnGetTemplate(URL_Common_Template, "gmtSpinner");
                            that.$el.html(that.template_MainModule({
                                "module": "merc"
                            }));
                            $(".gmPanelContent").html(tempSpinner);
                            //$("#surgeon-report-section").html(tempSpinner);
                        fnGetWebServerData("merc/surgeonrpt", undefined, input, function (surgdt) {
                            console.log(surgdt);
                            if (surgdt != undefined) {
                                _.each(surgdt, function(dt){
                                dt["picurl"] = URL_WSServer+"file/src?refgroup=103112&refid=" + dt.partyid + "&reftype=null&num=" + Math.random();
                                });
                                console.log(surgdt);
                                    $(".gmPanelContent").html(that.template_MercReportList(surgdt));
                            } else {
                                $("#surgeon-report-section").html("<section class = 'gmTextCenter gmMarginTop20p gmPaddingTop10p'>No Data Found</section>");
                            }
                        });
                    }
                },
                
                toggleMercSurgeonRpt: function(e){
                     var width = $(window).width();
                  if (width <= 480) {
                    $("#mobreportView").slideToggle();
                  }
                },
                
                //Merc Report
                loadMercReport: function (input) {
                    var that = this;
                    if(input.regnid != undefined && input.regnid == "100823")
                        $(".gmPanelTitle").text("North America MERC Programs");
                    else if(input.regnid != undefined && input.regnid == "100824")
                        $(".gmPanelTitle").text("International MERC Programs");
                    else if(input.title != undefined)
                        $(".gmPanelTitle").text("MERC Programs list selected by '" + input.title + "'");
                    else if(input.firstnm != undefined || input.lastnm != undefined)
                        $(".gmPanelTitle").text("MERC Programs list selected by '" + input.firstnm + " "+ input.lastnm +"'");
                    else if(input.type != undefined && input.type == "26241144")
                        $(".gmPanelTitle").text("Webinar MERC Programs For this Year");
                    else if((input.fromdt != undefined && input.todt != undefined)|| input.state !=undefined || input.country != undefined )
                        $(".gmPanelTitle").text("MERC Programs list selected by filters");

                    
                    $("#div-main-content").html(this.template_MERCReportMain());
                    console.log(">>>> " + JSON.stringify(input));
                    fnGetWebServerData("merc/report", undefined, input, function (data) {
                        console.log(data);
                        if (!data == "" || !data == undefined || !data == null) {
                            var rows = [],
                                i = 0;
                            _.each(data, function (griddt) {
                                rows[i] = {};
                                rows[i].id = i;
                                rows[i].data = [];
                                if(griddt.attendeefl == 'Y'){
                                  rows[i].data[0] = "<a class='attendeeRptLink'  data-mercid = '"+griddt.id+"' href='#crm/merc/report' title='"+ griddt.id + "'><i class='fa fa-user-md fa-2x gmFontGreen' aria-hidden='true'></i></a>";   
                                }
                                else 
                                   rows[i].data[0] = ""; 
                                rows[i].data[1] = "<a class='gmReportLnk' title='"+ griddt.name+ "' href='#crm/merc/id/"+griddt.id+"'>" + griddt.name + "</a>";
                                rows[i].data[2] = griddt.mtype;
                                rows[i].data[3] = griddt.mstatus;
                                rows[i].data[4] = griddt.fromdt;
                                rows[i].data[5] = griddt.todt;
                                rows[i].data[6] = griddt.city;
                                rows[i].data[7] = griddt.lang;
                                if(griddt.regnid == '100823'){
                                     rows[i].data[8] = "";
                                     rows[i].data[9] = griddt.statenm;
                                }
                                else{
                                    rows[i].data[8] = griddt.opento; 
                                    rows[i].data[9] = griddt.countrynm;
                                }
                                i++;
                            });
                            var dataHost = {};
                            dataHost.rows = rows;
                            var setInitWidths = "60,180,101,75,80,80,130,91,90,*";
                            var setColAlign = "center,left,left,left,left,left,left,left,left,left";
                            var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro,ro";
                            var setColSorting = "str,str,str,str,date,date,str,str,str,str";
                            var setHeader = ["","Title", "Type", "Status", "From Date", "To Date", "Location", "Language", "Open To", "State/Country"];
                            var setFilter =["","#text_filter","#select_filter_strict","#select_filter_strict","","","#text_filter","#select_filter_strict","#text_filter","#select_filter_strict"] ;
                            var enableTooltips = "false,false,false,false,false,false,false,false,false,false";
                            var footerArry = [];
                            var gridHeight = "";
                            var footerStyles = [];
                            var footerExportFL = true;

                            that.gridObj = loadDHTMLXGrid('merc-report-list', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL);
                            for (var i = 0; i < that.gridObj.getRowsNum(); i++) {
                                that.gridObj.cells(i, 1).cell.className = 'gmEllipses';
                                that.gridObj.cells(i, 9).cell.className = 'gmEllipses';
                            }

                            if (footerExportFL) {
                                var deleteIndexArr = []; //include the index of the column in the array if u want to export                 
                                $("#merc-report-list-export").unbind("click").bind("click", function (event) {
                                    exportExcel(event, that.gridObj, deleteIndexArr);
                                });
                            }

                            $(".attendeeRptLink").on("click", function (e) {
                                var attendeename = $(this).parent().siblings().find('a').text();
                                var attendeeInput = {
                                    'token': localStorage.getItem('token')
                                };
                                attendeeInput.mercid = $(e.currentTarget).data("mercid");
                                var tempSpinner = fnGetTemplate(URL_Common_Template, "gmtSpinner");
                                that.$el.html(that.template_MainModule({
                                    "module": "merc"
                                }));
                                $(".gmPanelContent").html(tempSpinner);
                                $(".gmPanelContent").html(that.template_MercReportList());
                                $(".gmPanelTitle").html(attendeename);
                                fnGetWebServerData("merc/surgeonrpt", undefined, attendeeInput, function (surgdt) {
                                    console.log(surgdt);
                                    if (surgdt != undefined) {
                                        _.each(surgdt, function (dt) {
                                            dt["picurl"] = URL_WSServer + "file/src?refgroup=103112&refid=" + dt.partyid + "&reftype=null&num=" + Math.random();
                                        });
                                        console.log(surgdt);
                                        $(".gmPanelContent").html(that.template_MercReportList(surgdt));
                                    }
                                });
                            });


                        } else {
                            $("#merc-report-list").html("No data found");
                        }

                    });



                },

                showLegend: function () {
                    $("#rpt-legend-detail").slideToggle();
                },


                // Close view
                closeView: function (view) {
                    if (view != undefined) {
                        view.unbind();
                        view.undelegateEvents();
                        view.$el.unbind();
                        view.$el.empty();
                        view = undefined;
                        $("#btn-back").unbind();
                    }
                },


                //Handles the orietaion change in the iPad
                onOrientationChange: function (event) {
                    resizeHeadWidth('.gmPanelHead', '.gmBtnGroup', '#panelhead-left');
                }
            });
            return GM.View.MercReport;
        });