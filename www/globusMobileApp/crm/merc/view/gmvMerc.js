/**********************************************************************************
 * File             :        gmvMerc.js
 * Description      :        View to create Merc and its detail
 * Path             :        /web/GlobusCRM/crm/merc/view/gmvMerc.js
 * Author           :        tmuthusamy
 **********************************************************************************/
define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars',

    // Common
    'global', 'commonutil', 'gmvSearch', 'gmvValidation','gmvMultiFileUpload','dropdownview',

    // View

    //Model
     'gmmMerc',

    //Collection
],

    function ($, _, Backbone, Handlebars,Global, Commonutil, GMVSearch, GMVValidation,GMVMultiFileUpload,DropDownView,GMMMerc) {

        'use strict';

        GM.View.Merc = Backbone.View.extend({

            name: "CRM Merc View",

            el: "#div-crm-module",

            /* Load the templates */
            template_MainModule: fnGetTemplate(URL_Common_Template, "gmtCRMMainModule"),
            template_MercEdit: fnGetTemplate(URL_CRMMerc_Template, "gmtMercEdit"),
            template_MercDetail: fnGetTemplate(URL_CRMMerc_Template, "gmtDetailMerc"),
            template_MercFileUpload: fnGetTemplate(URL_Common_Template, "gmtCRMFileUpload"),
            template_MercAttendee: fnGetTemplate(URL_CRMMerc_Template, "gmtMercAttendeeDtls"),
            template_CreateSurgeon: fnGetTemplate(URL_Common_Template, "gmtCreateSurgeon"),


            events: {
                "change input,select,textarea": "updateModel",
                "click #li-edit": "editMerc",
                "click #li-cancel": "backToPage",
                "click #mercUploadFile": "sourceFileUpload",
                "click #btn-main-save": "saveMerc",
                "change #regionSelect": "onChangeRegion",
                "click #btn-main-void": "confirmVoid",
                "click .tabMerc": "toggleMercTab",
                "change .attendee-role,.attendee-status": "onChangeAttendeeSelect",
                "click .remove-attendee-list": "removeAttendeeList",
                "click #merc-attendee-new-surgeon": "createSurgeon",
                "click .a-view-docs": "viewUploadDocs",
                "click #span-merc-frmdt,#span-merc-todt": "loadDatePicker",
                "change #merc-frmdt,#merc-todt": "displaySelctdDt"
            },

            initialize: function (options) {
                if (options.data != "") {
                    this.data = options.data;
                } else {
                    this.data = "";
                }
                this.mercid = options.mercid;
                var that = this;
                this.module = "merc";
                if (GM.Global.Merc == undefined)
                    GM.Global.Merc = {};

                if (this.mercid && this.mercid != 0) {
                    this.MercModel = new GMMMerc(this.data);
                    this.MercModel.set("arrgmmercpartyvo", arrayOf(this.data.arrgmmercpartyvo));

                    if (this.MercModel.get('arrgmmercpartyvo')[0] == null || this.MercModel.get('arrgmmercpartyvo')[0] == undefined)
                        this.MercModel.set('arrgmmercpartyvo', []);
                    this.MercModel.set($.parseJSON(JSON.stringify(this.MercModel)));
                    GM.Global.Merc.Mode = "view";
                } else if (this.mercid == 0 || this.mercid == undefined) {
                    this.MercModel = new GMMMerc();
                    GM.Global.Merc.Mode = "create";
                }
            },

            render: function () {
                var that = this;
                this.mercFiles = [];
                _.each(this.MercModel.get('arrgmmercpartyvo'),function(item,index){
                    item.roleElId = "attendee-role" + index;
                    item.statusElId = "attendee-status" + index;
                    item.partyid = item.partyid.toString();
                });
                if (GM.Global.Merc.Mode == "create") {
                    that.$el.html(this.template_MainModule({
                        "module": "merc"
                    }));
                    var mercDt = that.MercModel.toJSON();
                    $(".gmPanelContent").html(that.template_MercEdit(mercDt));
                    $("#merc-attendee-details").html(that.template_MercAttendee(mercDt));
                    $(".gmPanelTitle").html("Create Merc");
                    $("#li-save").removeClass("hide");
                    $("#li-cancel").removeClass("hide");
                    that.gmvValidation = new GMVValidation({
                        "el": that.$(".gmPanelContent")
                    });
                    that.searchSurgeon();
                    that.codeLookUpSelect();
                    that.attendeeCodeLookUpValues();
                }
                if (GM.Global.Merc.Mode == "view") {
                    that.$el.html(that.template_MainModule({
                        "module": "merc"
                    }));
                    
                    var viewData = that.MercModel.toJSON();
                    $(".gmPanelContent").html(that.template_MercDetail(viewData));
                    
                    $(".gmPanelContent").attr("mercid", viewData.mercid);
                    $("#li-save").addClass("hide");
                    $("#li-cancel").addClass("hide");
                    $("#li-edit").removeClass("hide");
                    $(".gmPanelTitle").html(viewData.eventnm);
                    that.getUploadFiles(viewData.mercid);
                }
                if (GM.Global.Merc.Mode == "edit") {
                    that.fileExist = "N";
                    this.gmvValidation = new GMVValidation({
                        "el": that.$(".gmPanelContent")
                    });
                    $("#li-void").removeClass("hide");
                    $("#li-save").removeClass("hide");
                    $("#li-cancel").removeClass("hide");
                    $("#li-edit").addClass("hide");
                    var editData = that.MercModel.toJSON();
                    $(".gmPanelContent").attr("mercid", editData.mercid);
                    if (this.MercModel.get("fromdt") != "") {
                        var fmtFromDt = compDateFmt(that.MercModel.get("fromdt"));
                        that.MercModel.set("fromdt",fmtFromDt);
                    }
                    if(this.MercModel.get("todt") != ""){
                        var fmtToDt = compDateFmt(that.MercModel.get("todt"));
                        that.MercModel.set("todt",fmtToDt);    
                    }
                    $(".gmPanelContent").html(that.template_MercEdit(that.MercModel.toJSON()));
                    $("#merc-attendee-details").html(that.template_MercAttendee(that.MercModel.toJSON()));
                    that.codeLookUpSelect();
                    that.attendeeCodeLookUpValues();
                    that.searchSurgeon();
                    if(editData.status == "108080")
                        $("#publishMerc").prop("checked",true);
                    else
                        $("#publishMerc").prop("checked",false);
                    that.getUploadFiles(editData.mercid);
                    if(this.MercModel.get("region") == "100824")
                        $(".mercState").text("STATE : ");
                    if(this.MercModel.get("region") == "100823"){
                        $(".mercOpenTo").addClass("hide");
                    }
                    else{
                        $(".mercOpenTo").removeClass("hide");
                    }
                }
                
                if(that.mercTabVal == "eventTab"){
                    $("#merc-eventsetup").trigger("click");
                }
                else if(that.mercTabVal == "attendeeTab"){
                    $("#merc-attendeedtl").trigger("click");
                }
            },
            
            codeLookUpSelect: function () {
                var that = this;
                getCLValToDiv("#typeSelect", "", "", "MERCTY", "", "", "codeid", "type", false, false, function (divID, attrVal) {
                    if (that.MercModel.get(attrVal) == "") {
                        that.setSelect(divID, "108061");
                        $(divID).trigger("change");
                    } else
                        that.setSelect(divID, that.MercModel.get(attrVal));
                });
                getCLValToDiv("#languageSelect", "", "", "MLANG", "", "", "codeid", "language", false, false, function (divID, attrVal) {
                    if (that.MercModel.get(attrVal) == "") {
                        that.setSelect(divID, "103097");
                        $(divID).trigger("change");
                    } else
                        that.setSelect(divID, that.MercModel.get(attrVal));
                });
                getCLValToDiv("#regionSelect", "", "", "DIV", "", "", "codeid", "region", false, true, function (divID, attrVal) {
                    if (that.MercModel.get(attrVal) != "")
                        that.setSelect(divID, that.MercModel.get(attrVal));
                });
                getCLValToDiv(".select-address-state", "", "", "STATE", "", "", "codeid", "state", false, "Y", function (divID, attrVal) {
                    if (that.MercModel.get(attrVal) != "")
                        that.setSelect(divID, that.MercModel.get(attrVal));
                });
                getCLValToDiv(".select-address-country", "", "", "CNTY", "", "", "codeid", "country", false, "Y", function (divID, attrVal) {
                    if (that.MercModel.get(attrVal) != "")
                        that.setSelect(divID, that.MercModel.get(attrVal));
                });
                
            },
            
            //attendee code lookup values -- Role and status
            attendeeCodeLookUpValues: function(){
                var that = this;
                _.each(that.MercModel.get('arrgmmercpartyvo'), function (item, index) {
                    var roleElId = "#"+item.roleElId;
                    var statusElId = "#"+item.statusElId;
                    getCLValToDiv(roleElId, "", "", "MCATRO", "", "", "codeid", "roleid", false, 'Y', function (divID, attrVal) {
                        if (item.roleid != "")
                            that.setSelect(divID, item.roleid);
                    });
                    getCLValToDiv(statusElId, "", "", "MCATST", "", "", "codeid", "statusid", false, 'Y', function (divID, attrVal) {
                        if (item.statusid == "") {
                            that.setSelect(divID, "108782");  // Default Set as "Checked In" on Status select
                            $(divID).trigger("change");
                        }
                        else
                            that.setSelect(divID, item.statusid);
                    });
                });
            },
            
            //Tab Toggle Function
            toggleMercTab: function (e) {
                var that = this;
                colorControl(e, "tabMerc", "gmBGLGrey gmFontBlack", "gmBGDarkerGrey gmFontWhite");
                $(".crm-merc-edit").addClass("hide");
                $(".div-merc-sub-content").addClass("hide");
                if ($(e.currentTarget).attr("name") == 'eventsetup') {
                    that.mercTabVal = "eventTab";
                    if (that.mercTabVal == "eventTab") {
                        $("#merc-event-setup").removeClass("hide");
                        $("#div-mercDetail").removeClass("hide");
                    }
                } else if ($(e.currentTarget).attr("name") == 'attendeedetails') {
                    that.mercTabVal = "attendeeTab";
                    if (that.mercTabVal == "attendeeTab") {
                        $("#merc-attendee-details").removeClass("hide");
                        $("#div-attendeeDetail").removeClass("hide");
                    }
                }
            },
            
            //Load common datepicker function
            loadDatePicker: function(event){
                var elemId = "#" + $(event.target).data("id");
                var propName = event.currentTarget.name;
                loadDatepicker(event,elemId,this.MercModel.get(propName),"mm/dd/yy","","");
            },
            
            displaySelctdDt: function(event){
                var elemId = "#" + $(event.target).attr("id");
                var elemVal = $(elemId).val();
                if(elemVal == "")
                   $(event.target).siblings("span").text("mm/dd/yyyy");
                else
                    $(event.target).siblings("span").text(elemVal);               
            },
            
            //proving search option to surgeon name
            searchSurgeon: function (e) {
                var that = this;
                var fnName = "";
                var searchOptions1 = {
                    'el': $("#merc-attendee-surgeon-search"),
                    'url': URL_WSServer + 'search/rsparty',
                    'inputParameters': {
                        'token': localStorage.getItem('token'),
                        "partytype": "7000"
                    },
                    'placeholder': 'Key in at least 3 characters to search', 
                    'keyParameter': 'partynm',
                    'IDParameter': 'id',
                    'NameParameter': 'name',
                    'resultVO': 'gmPartyBasicVO',
                    'fnName': fnName,
                    'minChar': 3,
                    'usage': 2,
                    'callback': function (result) {
                        if(result != null){
                           that.addAttendeeList(result);
                        }
                    }
                };
                var searchView = new GMVSearch(searchOptions1);
            },
            
            //Add attendee List
            addAttendeeList: function(datas){
                var that = this;
                _.each(arrayOf(datas), function (data) {
                $("#merc-attendee-surgeon-search").find("input").val("");
                $("#merc-hpartyid").val(data.ID);
                $("#merc-hpartyid").attr("partyid", data.ID);
                $("#merc-hpartyid").trigger("change");
                $("#merc-hpartynm").val(data.Name);
                $("#merc-hpartynm").attr("partyid", data.ID);
                $("#merc-hpartynm").trigger("change");
                });
                this.attendeeRenderTemplate();
            },
            
            //On change attendee select of role and status
            onChangeAttendeeSelect: function(e){
                var selectVal = $(e.currentTarget).val();
                var attrnm = $(e.currentTarget).attr("attrnm");
                var partyid = $(e.currentTarget).data("id");
                if(attrnm == 'role'){
                    $("#merc-hrole").val(selectVal);
                    $("#merc-hrole").attr("partyid", partyid);
                    $("#merc-hrole").trigger("change");
                }
                else if(attrnm == 'status'){
                    $("#merc-hstatus").val(selectVal);
                    $("#merc-hstatus").attr("partyid", partyid);
                    $("#merc-hstatus").trigger("change");
                }
            },
            
            //remove attendee from attendee list
            removeAttendeeList: function(e){
                var that = this;
                var attendeeid = $(e.currentTarget).data("id");
                var partyid = $(e.currentTarget).data("partyid");

                if (attendeeid != "") {
                    _.each(this.MercModel.get("arrgmmercpartyvo"), function (data) {
                        if (data.partyid == partyid)
                            data.voidfl = "Y";
                    });
                } else {
                    var voidAttr = _.reject(this.MercModel.get("arrgmmercpartyvo"), function (data) {
                        return data.partyid == partyid;
                    });

                    this.MercModel.set("arrgmmercpartyvo", voidAttr);
                }
                this.attendeeRenderTemplate();
                this.updateModel(e);
            },
            
            //render attendee sureon template for attendee Tab when add surgeon
            attendeeRenderTemplate: function(){
                _.each(this.MercModel.get('arrgmmercpartyvo'),function(item,index){
                    item.roleElId = "attendee-role" + index;
                    item.statusElId = "attendee-status" + index;
                    item.partyid = item.partyid.toString();
                });
                $("#merc-attendee-details").html(this.template_MercAttendee(this.MercModel.toJSON()));
                this.searchSurgeon();
                this.codeLookUpSelect();
                this.attendeeCodeLookUpValues();
            },
            
            
            //view uploaded file
            viewUploadDocs: function (event) {

                var path = $(event.currentTarget).data("href");
                
                if (path != undefined)
                    window.open(path, '_blank', 'location=no');
            },
            
            //On Change Region
            onChangeRegion: function(e){
                if(this.MercModel.get("region") == "100824")
                    $(".mercState").text("STATE : ");
                else
                    $(".mercState").text("* STATE : ");
                
                if(this.MercModel.get("region") == "100824" && this.MercModel.get("region") != ""){
                    $(".mercOpenTo").removeClass("hide");
                }
                else{
                    $(".mercOpenTo").addClass("hide");
                }
                    
            },
            
            //Update the Model 
            updateModel: function (e) {
                var that = this;
                this.selectDate(e);
                var control;
                if (e.currentTarget.tagName == "SELECT") {
                    control = $(e.currentTarget).find("option:selected");
                } else {
                    control = $(e.currentTarget);
                }

                var attribute = control.attr("name");
                var VO = control.attr("VO");
                var value = control.val();
                var attrval = control.val();
                var attrtype = control.attr("attrtype");
                var attrnm = control.attr("attrnm");
                var attrid = control.attr("attrid");
                var updAfl = control.attr("updAfl");
                var index = control.attr("index");
                var partyid = control.attr("partyid");
                if (control.attr("type") == 'checkbox') {
                    if (e.currentTarget.checked)
                        value = "Y";
                    else
                        value = "";
                }

                if (updAfl == "Y") {
                    attrnm  = attrval;
                    attrval = control.attr("attrval");
                }

                if ((attribute != undefined) || (value != undefined)) {
                    if (VO == undefined) {
                        this.MercModel.set(attribute, value);
                    } else {
                        if (VO == "arrgmmercpartyvo") {
                            this.updateMercPartyVO(partyid, attribute,value);
                        }
                    }
                }
                else {                    
                    that.render();
                }
                console.log(this.MercModel.toJSON());
            },
            
            // update Attendee list
            updateMercPartyVO: function (partyid, attribute,value) {
                var that = this;
                var partyVO = [];
                var partyVOItem;

                partyVOItem = _.findWhere(this.MercModel.get("arrgmmercpartyvo"), {
                    "partyid": partyid
                });

                var obj = _.findWhere(this.MercModel.get("arrgmmercpartyvo"), {
                    "partyid": partyid,
                    "voidfl": "Y"
                });

                if (!partyVOItem && !obj) {
                    partyVOItem = {
                        "partyid": partyid,
                        "mercid": "",
                        "partynm": "",
                        "attendeeid":"",
                        "roleid": "",
                        "statusid": "",
                        "voidfl": ""
                    };
                } else if (obj) {
                    _.each(this.MercModel.get("arrgmmercpartyvo"), function (a) {
                        if (a.partyid == partyid)
                            a.voidfl = "";
                    })
                }

                partyVO = _.without(this.MercModel.get("arrgmmercpartyvo"), partyVOItem);
                partyVOItem[attribute] = value;
                partyVO.push(partyVOItem);

                this.MercModel.get("arrgmmercpartyvo").length = 0;
                partyVO = arrayOf(partyVO);
                $.each(partyVO, function (index, item) {
                    that.MercModel.get("arrgmmercpartyvo").push(item);
                });
            },
            
            //Select Date min-max Condition Check
            selectDate: function (e) {
                var target = $(e.currentTarget).attr("id");
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1;
                var yyyy = today.getFullYear();
                if (dd < 10)
                    dd = '0' + dd
                if (mm < 10)
                    mm = '0' + mm

                var hours = today.getHours();
                var minutes = today.getMinutes();
                var currntTime = hours + ':' + minutes;
                today = yyyy + '-' + mm + '-' + dd;
                
                var startdate = $("#" + this.module + "-frmdt").val();
                var enddate = $("#" + this.module + "-todt").val();
                var startDateFmt = new Date(startdate);
                var toDateFmt = new Date(enddate);
                if (startdate != "" && target == this.module + "-frmdt") {
                    $("#" + this.module + "-todt").val(startdate).trigger("change");
                }

                if (startdate != "" && enddate != "" && toDateFmt < startDateFmt) {
                    $("#" + this.module + "-todt").val(startdate).trigger("change");
                }
            },
            
            //Edit Merc Function
            editMerc: function () {
                var that = this;
                GM.Global.Merc.Mode = "edit";
                that.render();
            },
            
            //status select function
            setSelect: function (controlid, selectedvalue) {
                $(controlid).val(selectedvalue);
            },
            
            //back to Page Function
            backToPage: function () {
                if (GM.Global.Merc.Mode == "edit")
                    Backbone.history.loadUrl(Backbone.history.fragment);
                else
                    window.history.back();
            },
            
            // Validations for Search Fields
            searchValidations: function () {
                var that = this;
                this.checkValidation = false;
                $("#countrySelect").attr("validate", "required");
                $("#countrySelect").attr("message", "Please provide Country");
                $("#countrySelect").addClass("gmValidationError");
                $("#stateSelect").attr("validate", "required");
                $("#stateSelect").attr("message", "Please provide State");
                $("#stateSelect").addClass("gmValidationError");
                $("#regionSelect").attr("validate", "required");
                $("#regionSelect").attr("message", "Please provide Region");
                $("#regionSelect").addClass("gmValidationError");
                
                if(this.MercModel.get("region") == "100823"){
                    if($("#countrySelect").val() != "" && $("#stateSelect").val() != "")
                            this.checkValidation = true;
                }
                
                if(this.MercModel.get("region") == "100824"){
                    $("#stateSelect").removeClass("gmValidationError");
                    $("#stateSelect").attr("validate","");
                    if($("#countrySelect").val() != "")
                        this.checkValidation = true;
                }
                else{
                    $("#stateSelect").addClass("gmValidationError");
                    $("#stateSelect").attr("validate","required");
                }
                
                if(this.MercModel.get("fromdt") == "")
                    $("#span-merc-frmdt").addClass("validateError");
                else
                    $("#span-merc-frmdt").removeClass("validateError");
                if(this.MercModel.get("todt") == "")
                    $("#span-merc-todt").addClass("validateError");
                else
                    $("#span-merc-todt").removeClass("validateError");
                
                that.checkUploadFl = "";
                 if (this.MercModel.get("file") != undefined) {
                    // Get form
                    var $form = $("#txt-fileupload").closest(".frm-documents");
                    that.files = $form.find('[name="file"]')[0].files;
                    this.arrAcceptetFormat = ['pdf'];
                    var filenm = that.files[0].name.slice(0,(that.files[0].name.length - 4));
                    var format = _.last(that.files[0].name.split('.'));

                    if (!_.contains(that.arrAcceptetFormat, format.toLowerCase()) || filenm.indexOf(" ") >= 0 || filenm.indexOf(".") >= 0 || filenm.indexOf("/") >= 0) {
                        that.checkValidation = false;
                        $("#txt-fileupload").val("");
                        $("#span-fileupload-browse-filenm").text("");
                        $(".fileUploadErr").attr("validate","required");
                        this.MercModel.set("file", undefined);
                        $("#li-fileupload-fileinfo").hide();
                        $(".fileUploadErr").attr("message","Invalid File Format. Please select any ." + that.arrAcceptetFormat.join(' or .') + " file");
                    }
                    else {
                        that.checkUploadFl = "Y";
                        $(".fileUploadErr").attr("validate","");
                        $(".fileUploadErr").attr("message","");
                        that.checkValidation = true;
                    }
                 }
                else{
                    $(".fileUploadErr").attr("validate","");
                    $(".fileUploadErr").attr("message","");
                    that.checkValidation = true;
                }
                     
                
            },
            // To delete the merc event from the details
            confirmVoid: function (event) {
                var that = this;
                dhtmlx.confirm({
                    title: "Confirm",
                    ok: "Yes",
                    cancel: "No",
                    type: "confirm",
                    text: "Do you want to void this details? ",
                    callback: function (result) {
                        if (result) {
                            that.saveMerc(event);
                        }
                    }
                });
            },
            
            // Save Merc Event Activity
            saveMerc: function (event) {
                this.MercModel.unbind("change");
                var that = this;
                var input =  this.MercModel.toJSON();
                input.token = localStorage.getItem("token");
                input.fromdt = input.fromdt;
                input.todt = input.todt;
                if(input.countrynm != undefined)
                    delete input.countrynm;
                if(input.statenm != undefined)
                    delete input.statenm;
                if(input.langnm != undefined)
                    delete input.langnm;
                if(input.regionnm != undefined)
                    delete input.regionnm;
                if(input.typenm != undefined)
                    delete input.typenm;
                if(input.statusnm != undefined)
                    delete input.statusnm;
                if(input.file != undefined)
                    delete input.file;
                if(input.status == "Y" || input.status == 108080)
                    input.status = 108080;
                else
                    input.status = 108081;
                if (event.currentTarget.id == "btn-main-void"){
                    input.voidfl = "Y";
                }
		            if(input.arrgmmercpartyvo.length != 0){
                    _.each(input.arrgmmercpartyvo,function(item){
                        delete item.partynm;
                        delete item.roleElId;
                        delete item.statusElId;
                        delete item.rolenm;
                        delete item.statusnm;
                    });
                }
                delete input.roleid;
                delete input.statusid;
                console.log("input >> " + JSON.stringify(input));
                this.searchValidations();
                if (this.gmvValidation.validate() && this.checkValidation) {
                    if (that.checkUploadFl == "Y"){
                         $(".alert").removeClass().addClass('alert alertWarning').html('File Upload In Progress');
                    }
                    that.getLoader(".crm-merc-edit");
                    fnGetWebServerData("merc/save", undefined, input, function (data) {
                        console.log("MERC Save OUTPUT" + JSON.stringify(data));

                    if (data != null) {
                            if (event.currentTarget.id == "btn-main-void"){
                                showSuccess("MERC Event Voided Successfully");
                                window.location.href = "#crm/" + that.module;
                            }
                            else{
                                var mercID = data.mercid;
                                if (that.checkUploadFl == "Y"){
                                    that.uploadFile(mercID,function(uploaded){
                                        if(uploaded){
                                            showSuccess("MERC Event saved successfully");
                                            if (that.mercid == undefined || that.mercid == 0)
                                                window.location.href = "#crm/" + that.module + "/id/" + mercID;
                                            else
                                                Backbone.history.loadUrl(Backbone.history.fragment);

                                            GM.Global.Merc.Mode = "view";
                                        }
                                    });
                                }
                                else{
                                    showSuccess("MERC Event saved successfully");
                                    if (that.mercid == undefined || that.mercid == 0)
                                        window.location.href = "#crm/" + that.module + "/id/" + mercID;
                                    else
                                        Backbone.history.loadUrl(Backbone.history.fragment);

                                    GM.Global.Merc.Mode = "view";
                                }
                            }
                            
                                
                        } else
                            showError("Merc saved Failed");
                    });
                } else
                    showError(this.gmvValidation.getErrors());

            },
            
            //Request DetailsFile Upload Function
            sourceFileUpload: function (event) {
                var that = this;
                if(this.fileExist == "Y"){
                        showError("Please remove uploaded file");
                }
                else{
                    this.renderDocList();
                }
            },
            
            browseFile:function () {
                $("#btn-fileupload-popup-browse").click(function () {
                    $("#txt-fileupload").trigger("click");
                    $('input[type="file"]').change(function () {
                        var file = $("#txt-fileupload").val().replace(/C:\\fakepath\\/i, '');
                        $("#span-fileupload-browse-filenm").text(file);
                    });

                });
            },
            
            uploadFile: function(mercID,cb) {
                var that = this;
                var formData = new FormData();
                $.each(that.files, function (i, file) {
                    formData.append('file', file);
                });
                // Create an FormData object 
                if (formData != "" && formData != undefined) {
                    formData.append('refid', mercID);
                    formData.append('reftype', '');
                    formData.append('refgrp', '105265');
                    formData.append('token', localStorage.getItem('token'));
                }
                $.ajax({
                    url: URL_WSServer + 'file/crmfileupload',
                    type: 'POST',
                    data: formData,
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    enctype: 'multipart/form-data',
                    success: function (data) {
                        cb(true);
                    },
                    error: function (model, response, errorReport) {
                        showError(errorReport);
                        cb(false);
                    }
                });
            },
            
            // Document Render Function
            renderDocList: function(){
                var that = this;
                var mercData = that.MercModel.toJSON(); 
                $(".merc-fileupld-edit").html(that.template_MercFileUpload());
                $(".span-doc-act-edit").text("File Type - .pdf");
                $("#btn-fileupload-upload").addClass("hide");
                this.browseFile();
                
                $("#span-fileupload-browse-filenm").addClass("gmEllipses");
            },
            
            
            //Fetch the Uploaded Request Files
            getUploadFiles: function (mercID) {
                var that = this;
                if ((GM.Global.Merc.Mode == "view") || (GM.Global.Merc.Mode == "edit") || (GM.Global.Merc.Mode == "create")) {

                    var input = {
                        "token": localStorage.getItem("token"),
                        "refid": mercID,
                        "refgroup": "105265"
                    };

                    if (parseNull(mercID) != "")
                        input["refid"] = mercID;
                    console.log("getUploadFiles" + JSON.stringify(input));
                    fnGetWebServerData("file/get", "gmCRMFileUploadVO", input, function (file) {
                        console.log("getUploadFiles files" + JSON.stringify(file));
                        if (file != null) {
                            file = arrayOf(file);
                            that.mercFiles = file;
                            $(".merc-fileupld-view").html("");
                            _.each(file, function (v) {
                                var docLink = URL_CRMFileServer + v.refgroup + "/" + v.refid + "/" + v.filename;
                                if (GM.Global.Merc.Mode == "view") {
                                    $(".merc-fileupld-view").append("<div class = 'gmLnk gmUnderline gmPaddingButtom10p'><span class='hide hidden-file'>" + docLink + "</span> <a class='a-view-docs' data-href = '" + URL_CRMFileServer + v.refgroup + "/" + v.refid + "/" + v.filename + "'  >" + v.filename + " - " + v.updatedby + " on " + v.updateddate + "</a></div>");
                                } else {
                                    that.fileExist = "Y";
                                    $(".merc-fileupld-view").append("<div class = 'gmLnk gmUnderline gmPaddingButtom10p'><i data-fileid='" + v.fileid + "' class='fa fa-minus-circle merc-file-remove gmBtnLarge gmFontBlack gmMarginLeft5p'></i><span class='hide hidden-file'>" + docLink + "</span> <a class='a-view-docs' data-href = '" + URL_CRMFileServer + v.refgroup + "/" + v.refid + "/" + v.filename + "'  >" + v.filename + " - " + v.updatedby + " on " + v.updateddate + "</a></div>");

                                    $(".merc-file-remove").on("click", function (e) {
                                        var fileid = $(e.currentTarget).data("fileid");
                                        dhtmlx.confirm({
                                            title: "Delete File",
                                            ok: "Yes",
                                            cancel: "No",
                                            type: "confirm",
                                            text: "File will be removed.Would you like to continue? ",
                                            callback: function (result) {
                                                if(result){
                                                    var deleteObj = [{
                                                        "token": localStorage.getItem("token"),
                                                        "fileid": fileid
                                                    }];
                                                    var input = {
                                                        "gmCommonFileUploadVO": deleteObj
                                                    }
                                                    fnGetWebServerData("fileUpload/deleteUploadedFiles", undefined, input, function (data) {
                                                        that.render();
                                                        $("#li-edit").trigger("click");
                                                    }, true);
                                                }
                                            }
                                        });
                                        
                                    });
                                }
                            });
                        }
                    });

                }
            },
            
            //Create New Surgeon Function
            createSurgeon: function () {
                var that = this;
                var newSurgeonValidation = "true";
                showPopup();
                $("#div-crm-overlay-content-container").find('.load-model').addClass("hide");
                $("#div-crm-overlay-content-container").find('.done-model').addClass("hide");
                $("#div-crm-overlay-content-container").find('.save-model').removeClass("hide");
                $("#div-crm-overlay-content-container").find('.modal-title').text("Key in a new Surgeon");
                $("#div-crm-overlay-content-container .modal-body").html(this.template_CreateSurgeon());

                //getSelectState();
                getCLValToDiv(".select-address-state", "", "", "STATE", "", "", "codeid", "", false, "Y", function (divID, attrVal) {});
                
                //getSelectCountry();
                getCLValToDiv(".select-address-country", "", "", "CNTY", "", "", "codeid", "", false, "Y", function (divID, attrVal) {});
                
                var userData = JSON.parse(localStorage.getItem("userData"));
                //Input for fetching company List for company dropdown
                var input = {
                    'token': localStorage.getItem('token'),
                    'companyid': '',
                    'partyid': localStorage.getItem('partyID')
                };
                var defaultid = userData.cmpid;
                var elemAdmin = "#surg-company-dropdown";

                companyDropdown(input,defaultid,"",DropDownView,elemAdmin,function(selectedId){
                    that.companyid = selectedId;
                });
                
                $("#div-crm-overlay-content-container .close").on('click', function () {
                    $("#div-crm-overlay-content-container").find(".modal-body").empty();
                });
                
                $("#div-crm-overlay-content-container").find('#input-new-surgeon-npid').unbind('blur').bind('blur', function (e) {
                    that.newSurgeon = "true";
                    that.checkNpid(e);
                });
                $("#div-crm-overlay-content-container").find('.input-number-validation').unbind('keyup').bind('keyup', function (e) {
                    that.checkNumber(e);
                });
                $("#div-crm-overlay-content-container").find('.input-phonenumber-validation').unbind('keypress').bind('keypress', function (e) {
                    that.checkPhoneNumber(e);
                });
                $("#div-crm-overlay-content-container").find('.save-model').unbind('click').bind('click', function () {

                        var str = "";
                        var emailPattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
                        var numberPattern = new RegExp(/^[0-9- +()]*$/);
                        var newSurgNPI = $("#input-new-surgeon-npid").val();

                        if ($("#input-new-surgeon-firstnm").val().trim() == "")
                            str = "Firstname";
                        if ($("#input-new-surgeon-lastnm").val().trim() == "")
                            str = (str.trim().length) ? (str + " / " + "Lastname") : "Lastname";
                        if (that.companyid == "undefined" || that.companyid == "0")
                            str = (str.trim().length) ? (str + " / " + "Company") : "Company";
                        if (newSurgNPI.trim() != "" && newSurgNPI.length != 10)
                            str = (str.trim().length) ? (str + " / " + "NPI should contain numeric characters only and should be 10-digit number") : "NPI should contain numeric characters only and should be 10-digit number";
                        if ($("#input-new-surgeon-email").val().trim() != "" && !emailPattern.test($("#input-new-surgeon-email").val().trim()))
                            str = (str.trim().length) ? (str + " / " + "Wrong Email Fomat") : "Wrong Email Fomat";
                        var newSurgPhNo = $("#input-new-surgeon-phone").val();
                        if (newSurgPhNo.trim() != "" && !numberPattern.test($("#input-new-surgeon-phone").val().trim()) && newSurgPhNo.length > 25)
                            str = (str.trim().length) ? (str + " / " + "Please enter Phone Number and should be 10-digit number") : "Please enter Phone Number and should be 10-digit number";
                        var addr = "";
                        if ($("#input-new-surgeon-add1").val().trim() != "" || $("#input-new-surgeon-city").val() != "" || $("#div-new-surgeon-content .select-address-state").val().trim() != "" || $("#input-new-surgeon-zip").val().trim() != "" || $("#div-new-surgeon-content .select-address-country").val().trim() != "") {
                            var confirm = true;
                            if ($("#input-new-surgeon-add1").val().trim() == "")
                                addr = (addr.trim().length) ? (addr + " / " + "Address1") : "Address1";
                            if ($("#input-new-surgeon-city").val().trim() == "")
                                addr = (addr.trim().length) ? (addr + " / " + "City") : "City";
                            if($("#div-new-surgeon-content .select-address-country").val().trim() == '1101'){
                            if ($("#div-new-surgeon-content .select-address-state").val().trim() == "")
                                addr = (addr.trim().length) ? (addr + " / " + "State") : "State";
                            if ($("#input-new-surgeon-zip").val().trim() == "")
                                addr = (addr.trim().length) ? (addr + " / " + "Zip Code") : "Zip Code";
                            if ($("#input-new-surgeon-zip").val().trim() != "" && $("#input-new-surgeon-zip").val().trim().length > 10)
                                addr = (addr.trim().length) ? (addr + " / " + "Zip can have maximun of 10 characters") : "Zip can have maximun of 10 characters";
                            }
                            if ($("#div-new-surgeon-content .select-address-country").val().trim() == "")
                                addr = (addr.trim().length) ? (addr + " / " + "Country") : "Country";
                        }
                        if (str.length)
                            str = (addr.length && addr.length) ? (str + " / " + addr) : str;
                        else if (addr.length)
                            str = addr;
                        var surgstr = " Mandatory Fields: " + str;
                        var firstName = $("#input-new-surgeon-firstnm").val().trim();
                        var lastName = $("#input-new-surgeon-lastnm").val().trim();
                        var midName = $("#input-new-surgeon-midinitial").val().trim();
                        if (firstName != "" && lastName != "") {
                            var input = {
                                "token": localStorage.getItem("token"),
                                "firstname": firstName,
                                "lastname": lastName,
                                "middleinitial": midName
                            };
                            fnGetWebServerData("surgeon/info", "gmCRMSurgeonlistVO", input, function (data) {
                                console.log("Surgeon Data >>>>>>>>" + JSON.stringify(data));
                                if (data != undefined && data.length > 0)
                                    $("#popup-create-surgeon-validation").text(firstName + " " + lastName + (midName ? (" " + midName) : "") + "'s information already exists");
                                else if (str.length)
                                    $("#popup-create-surgeon-validation").text(surgstr);
                                else {
                                    var surgeonInput = {
                                        "token": localStorage.getItem("token"),
                                        "partyid": "",
                                        "npid": newSurgNPI,
                                        "firstnm": $("#input-new-surgeon-firstnm").val(),
                                        "lastnm": $("#input-new-surgeon-lastnm").val(),
                                        "partynm": "",
                                        "midinitial": $("#input-new-surgeon-midinitial").val(),
                                        "asstnm": "",
                                        "mngrnm": "",
                                        "companyid": that.companyid,

                                        "arrgmsurgeonsaddressvo": [],
                                        "arrgmsurgeoncontactsvo": [],
                                        "arrgmcrmsurgeonaffiliationvo": [],
                                        "arrgmcrmsurgeonsurgicalvo": [],
                                        "arrgmcrmsurgeonactivityvo": [],
                                        "arrgmcrmsurgeonattrvo": [],
                                        "arrgmcrmfileuploadvo": []
                                    };
                                    if (isRep()) {
                                        surgeonInput.arrgmcrmsurgeonattrvo.push({
                                            "attrtype": 11509,
                                            "attrvalue": localStorage.getItem("partyID"),
                                            "attrnm": localStorage.getItem("fName") + " " + localStorage.getItem("lName"),
                                            "attrid": "",
                                            "voidfl": ""
                                        });
                                    }

                                    if (addr == "" && confirm == true) {
                                        surgeonInput.arrgmsurgeonsaddressvo.push({
                                            "addrtyp": "90400",
                                            "addid": "",
                                            "add1": $("#input-new-surgeon-add1").val(),
                                            "add2": $("#input-new-surgeon-add2").val(),
                                            "city": $("#input-new-surgeon-city").val(),
                                            "statenm": $(".select-address-state option:selected").text(),
                                            "stateid": $(".select-address-state option:selected").val(),
                                            "zip": $("#input-new-surgeon-zip").val(),
                                            "countrynm": $(".select-address-country option:selected").text(),
                                            "countryid": $(".select-address-country option:selected").val(),
                                            "primaryfl": "Y"
                                        });
                                    }
                                    if (newSurgPhNo != "") {
                                        surgeonInput.arrgmsurgeoncontactsvo.push({
                                            "contype": "Home",
                                            "conmode": "90450",
                                            "convalue": newSurgPhNo,
                                            "conid": "",
                                            "primaryfl": "Y",
                                            "voidfl": ""
                                        });
                                    }
                                    if ($("#input-new-surgeon-email").val() != "") {
                                        surgeonInput.arrgmsurgeoncontactsvo.push({
                                            "contype": "Home",
                                            "conmode": "90452",
                                            "convalue": $("#input-new-surgeon-email").val(),
                                            "conid": "",
                                            "primaryfl": "Y",
                                            "voidfl": ""
                                        });
                                    }
                                    hidePopup();
                                    console.log(JSON.stringify(surgeonInput));
                                    fnGetWebServerData("surgeon/save", undefined, surgeonInput, function (data) {
                                        if (data != null) {
                                            showSuccess("Surgeon Detail created successfully");
                                            data.Name = data.firstnm + " " + data.lastnm;
                                            data.ID = data.partyid;
                                            that.addAttendeeList(data);
                                        } else
                                            showError("Save Failed");
                                    });

                                    $("#div-crm-overlay-content-container").find('.modal-title').text("");
                                    $("#div-crm-overlay-content-container").find(".modal-body").empty();
                                }
                            });
                        } else
                            $("#popup-create-surgeon-validation").text(surgstr);
                });
            },
            
            //validation for npid
            checkNpid: function (e) {
                var that = this;
                var val;
                $("#img-npid-spinner").toggleClass('hide');
                $(e.currentTarget).toggleClass('hide');
                if (this.newSurgeon == "true") {
                    val = $("#input-new-surgeon-npid").val();
                    this.newSurgeon = "false"
                } else
                    val = $("#input-lead-contacts-npid").val();
                var re = /[^0-9]/g;
                var error = re.test(val);
                //            hideMessages();
                if (val != "") {
                    if (error == true) { //npid should be numberic and with max length of 10
                        $("#popup-create-surgeon-validation").html("NPI Should be Numeric");
                        $("#img-npid-spinner").toggleClass('hide');
                        $(e.currentTarget).toggleClass('hide');
                        $(e.currentTarget).val("").focus();
                    } else {
                        $("#popup-create-surgeon-validation").html("");
                        getSurgeonPartyInfo(val, function (surgeonData) {
                            var partyid;
                            if (that.newSurgeon == "true")
                                partyid = 0;
                            else
                                partyid = GM.Global.SurgeonPartyId; //calling surgeon's party information of typed in npid                                                                                                                                                        //that.chkNPI == 1 already allocated;  that.chkNPI == 0 npid verified

                            if (surgeonData != undefined) {
                                surgeonData = surgeonData[0];
                                if (partyid == 0) {
                                    $(".npi-detection.fa-check-circle-o").remove();
                                    if ($(".npi-detection.fa-times-circle").length == 0)
                                        $(".li-popup-npid-create").append("<span class='fa npi-detection fa-times-circle'></span>");
                                    that.chkNPI = 1;
                                } else {
                                    if (surgeonData.partyid == partyid) {
                                        $(".npi-detection.fa-times-circle").remove();
                                        if ($(".npi-detection.fa-check-circle-o").length == 0)
                                            $(".li-popup-npid-create").append("<span class='fa npi-detection fa-check-circle-o'></span>");
                                        that.chkNPI = 0;
                                    } else {
                                        $(".npi-detection.fa-check-circle-o").remove();
                                        if ($(".npi-detection.fa-times-circle").length == 0)
                                            $(".li-popup-npid-create").append("<span class='fa npi-detection fa-times-circle'></span>");
                                        that.chkNPI = 1;
                                    }
                                }
                            } else {
                                $(".npi-detection.fa-times-circle").remove();
                                if ($(".npi-detection.fa-check-circle-o").length == 0)
                                    $(".li-popup-npid-create").append("<span class='fa npi-detection fa-check-circle-o'></span>");
                                that.chkNPI = 0;
                            }
                            $("#img-npid-spinner").toggleClass('hide');
                            $(e.currentTarget).toggleClass('hide');
                        });
                    }

                } else { //empty
                    $("#popup-create-surgeon-validation").html("");
                    if ($(".npi-detection.fa-times-circle").length != 0)
                        $(".npi-detection.fa-times-circle").remove();
                    if ($(".npi-detection.fa-check-circle-o").length != 0)
                        $(".npi-detection.fa-check-circle-o").remove();
                    $("#img-npid-spinner").toggleClass('hide');
                    $(e.currentTarget).toggleClass('hide');
                    that.chkNPI = 0;
                }
            },

            //validation for npid
            checkNumber: function (e) {
                if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
                    $(e.currentTarget).val("");
                }
            },
                      
             // New Method to allow only the valid characters like Numbers, + -,()
            checkPhoneNumber: function(e){ 
                if((e.which == 32 || e.which == 40 || e.which == 41 || e.which == 43 || e.which == 45)||((e.which > 47) && (e.which < 58))){
                   return true;       
                }
                else{
                    e.preventDefault();
                }
            },
            
            //Load Spinner
            getLoader: function (divElem) {
                var template = fnGetTemplate(URL_Common_Template, "gmtSpinner");
                $(divElem).html(template());
            }
            
        });
        return GM.View.Merc;
    });
