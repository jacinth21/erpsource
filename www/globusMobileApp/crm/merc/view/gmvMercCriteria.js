define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars', 'jqueryui',

    // Common
    'global',

    // View
    'gmvCRMSelectPopup',

    // Pre-Compiled Template
    'gmtTemplate'

    //Model

],

    function ($, _, Backbone, Handlebars, JqueryUI, Global, GMVCRMSelectPopup, GMTTemplate) {

        'use strict';

        GM.View.MercCriteria = Backbone.View.extend({

            el: "#div-crm-module",

            name: "CRM MERC Criteria View",

            /* Load the templates */
            template_MainModule: fnGetTemplate(URL_Common_Template, "gmtCRMMainModule"),
            template_MercCriteria: fnGetTemplate(URL_CRMMerc_Template, "gmtCriteriaMerc"),
            template_selected_items: fnGetTemplate(URL_Common_Template, "gmtPopupSelectedItem"),
            template_MercReportList: fnGetTemplate(URL_CRMMerc_Template, "gmtMercReportList"),

            events: {
                "click #btn-main-create": "createMerc",
                "click #btn-category-search": "searchCategory",
                "change #merc-frmdate, #merc-todate": "setMinDate",
                "click .li-upcum-merc": "searchReportByRegion",
                "click #btn-search-title": "searchReportByTitle",
                "click  .btn-listby": "openPopup",
                "click #btn-search-name": "searchReportByName",
                "click .li-webinar-merc": "showReportByWebinar",
                "click #span-merc-frmdate,#span-merc-todate": "loadDatePicker",
                "change #merc-frmdate,#merc-todate": "displaySelctdDt"
            },

            initialize: function (options) {
                $(window).on('orientationchange', this.onOrientationChange);
                this.view = options.view;
                GM.Global.Merc = {};
                GM.Global.Merc.FilterID = {};
                GM.Global.Merc.FilterName = {};
            },

            render: function () {
                var that = this;
                this.$el.html(this.template_MainModule({
                    "module": "merc"
                }));
                this.mercAdmin = _.findWhere(GM.Global.UserAccess, {
                    funcnm: "MERC-ADMIN"
                });
                this.$("#li-create").removeClass("hide");
                
                $("#crm-btn-back").addClass("hide");
                    

                $(".gmPanelTitle").text("Lookup MERC");
                $("#div-main-content").html(this.template_MercCriteria());
                this.$("#btn-category-search").hide();
            },

            createMerc: function () {
                window.location.href = "#crm/merc/id/" + 0 + "";
            },
            
            //Load common datepicker function
            loadDatePicker: function(event){
                var elemId = "#" + $(event.target).data("id");
                loadDatepicker(event,elemId,"","mm/dd/yy","","");
            },
            
            displaySelctdDt: function(event){
                var elemId = "#" + $(event.target).attr("id");
                var elemVal = $(elemId).val();
                if(elemVal == "")
                   $(event.target).siblings("span").text("mm/dd/yyyy");
                else
                    $(event.target).siblings("span").text(elemVal);               
            },

            //set min date and button enable based on date selected
            setMinDate: function (e) {
                var that = this;
                var fromdt = $("#merc-frmdate").val();
                var todt = $("#merc-todate").val();
                var fromdtFmt = new Date(fromdt);
                var todtFmt = new Date(todt);
                if ($("#merc-todate").val() != "" && $("#merc-todate").val() != undefined && $("#merc-frmdate").val() != undefined && fromdtFmt > todtFmt)
                    $("#merc-todate").val($("#merc-frmdate").val()).trigger("change");
                var fromDate = $("#merc-frmdate").val();
                var toDate = $("#merc-todate").val();
                
                
                 if ($(e.currentTarget).attr("id") == "merc-frmdate")
                    $("#merc-todate").attr("min", fromDate);
                if (GM.Global.Merc.FilterID == undefined)
                    GM.Global.Merc.FilterID = {};
                if (GM.Global.Merc.FilterName == undefined || GM.Global.Merc.FilterName == undefined)
                    GM.Global.Merc.FilterName = {};
                else if (typeof (GM.Global.Merc.FilterID) != "object")
                    GM.Global.Merc.FilterID = $.parseJSON(GM.Global.Merc.FilterID);
                if (typeof (GM.Global.Merc.FilterName) != "object")
                    GM.Global.Merc.FilterName = $.parseJSON(GM.Global.Merc.FilterName);
                if (toDate != "" && toDate != "aN/aN/NaN" && fromDate != "" && fromDate != "aN/aN/NaN") {                    
                        GM.Global.Merc.FilterName["From Date"] = fromDate;
                        GM.Global.Merc.FilterID["mercstartdate"] = fromDate;
                        GM.Global.Merc.FilterID["mercenddate"] = toDate;
                        GM.Global.Merc.FilterName["To Date"] = toDate;
                    
                } else {                   
                        GM.Global.Merc.FilterName["From Date"] = "";
                        GM.Global.Merc.FilterID["mercstartdate"] = "";
                        GM.Global.Merc.FilterID["mercenddate"] = "";
                        GM.Global.Merc.FilterName["To Date"] = "";
                    
                }           
                var tempArray = new Array();
                var result = $.parseJSON(JSON.stringify(GM.Global.Merc.FilterName));
                $.each(result, function (key, val) {
                    if (val != "" && val != "aN/aN/NaN")
                        tempArray.push({
                            "title": key,
                            "value": val
                        });
                });
                var finalJSON = $.parseJSON(JSON.stringify(tempArray));
                if (finalJSON != "") {
                    this.$(".list-category-item").html(this.template_selected_items(finalJSON));
                    this.$("#btn-category-search").show();
                } else {
                    this.$("#btn-category-search").hide();
                }

            },
            
             // Open Single / Multi Select box        
            openPopup: function (event) {
                var that = this;
                var fnName = "";
                that.systemSelectOptions = {
                    'title': $(event.currentTarget).attr("data-title"),
                    'storagekey': $(event.currentTarget).attr("data-storagekey"),
                    'webservice': $(event.currentTarget).attr("data-webservice"),
                    'resultVO': $(event.currentTarget).attr("data-resultVO"),
                    'IDParameter': $(event.currentTarget).attr("data-IDParameter"),
                    'NameParameter': $(event.currentTarget).attr("data-NameParameter"),
                    'inputParameters': $(event.currentTarget).attr("data-inputParameters"),
                    'codegrp': $(event.currentTarget).attr("data-codegrp"),
                    'sortby': $(event.currentTarget).attr("data-sortby"),
                    'module': this.module,
                    'event': event,
                    'parent': this,
                    'moduleLS': GM.Global.Merc.FilterID,
                    'moduleCLS': GM.Global.Merc.FilterName,
                    'callback': function (ID, Name) {
                        GM.Global.Merc.FilterID = ID;
                        GM.Global.Merc.FilterName = Name;
                    }
                };

                that.popupSelectView = new GMVCRMSelectPopup(that.systemSelectOptions);

            },

            // view report based on selected criteria
            searchCategory: function (event) {
                window.location.href = "#crm/merc/report/list/rptbyfilter";
            },
            
             // view report based on webinar type
            showReportByWebinar: function (event) {
                window.location.href = "#crm/merc/reportby/webinar";
            },
            
            searchReportByName: function (event) {
                var mercFName = $(".merc-fname").val();
                var mercLName = $(".merc-lname").val();
                
                GM.Global.Merc["firstnm"] = mercFName;
                GM.Global.Merc["lastnm"] = mercLName;
                if(mercFName !="" || mercLName !="")
                window.location.href = "#crm/merc/report/list/rptbysurgname";   
                else {
                    showError("Please enter first name or last name to search");
                }
            },

            searchReportByRegion: function (e) {
                var regionID = $(e.currentTarget).data("region");
                if(regionID == '100823')
                    window.location.href = "#crm/merc/report/US";
                else
                   window.location.href = "#crm/merc/report/OUS"; 
            },


            searchReportByTitle: function () {
                var mercTitle = $(".merc-title").val();
                if (mercTitle != "") {
                    window.location.href = "#crm/merc/report/search/" + mercTitle;
                } else {
                    showError("Please enter title to search");
                }
            },

            

        });
        return GM.View.MercCriteria;
    });