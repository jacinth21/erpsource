/**********************************************************************************
 * File:        gmrMerc.js
 * Description: Router for all of Merc
 * Version:     1.0
 * Author:      tmuthusamy
 **********************************************************************************/
define([

  // External Libraries
  'jquery', 'underscore', 'backbone',

  //Common
    'gmvCRMMain',

  //Views
   'gmvMercCriteria','gmvMerc','gmvMercReport'
],

    function ($, _, Backbone,GMVCRMMain,GMVMercCriteria,GMVMerc,GMVMercReport) {

        'use strict';

        GM.Router.Merc = Backbone.Router.extend({

            //routes to its respective web page 
            routes: {
                "crm/merc": "viewMercCriteria",
                "crm/merc/id/:mercid": "mercDetail",
                "crm/merc/report/:region": "lookupByRegion",
                "crm/merc/report/search/:searchnm": "lookupByName",
                "crm/merc/report/list/:rpttyp": "lookupByFilter",
                "crm/merc/reportby/:rptType" : "lookupByRptType",
            },

            //things to execute before the routing
            execute: function (callback, args, name) {
                console.log(GM.Global.CRMMain)
                if (GM.Global.CRMMain == undefined) {
                    commonSetUp();
                    GM.Global.Module = "merc";
                    GM.Global.CRMMain = true;
                    var gmvCRMMain = new GMVCRMMain();
                    gmvApp.showView(gmvCRMMain);
                }
                $('.gmTab').removeClass().addClass('gmTab');
                $("#merc").addClass("gmTab tabmerc gmFontWhite");
                if (!$(".alert").hasClass("alertSuccess")) {
                    if (GM.Global.AlertMsg) {
                        showError(GM.Global.AlertMsg);
                        GM.Global.AlertMsg = undefined;
                    } else
                        hideMessages();
                }
                if (callback) callback.apply(this, args);
            },
            initialize: function () {
                $("#div-app-title").show();
                $("#span-app-title").html("Merc");
                $(".li-icon-back").show();
                GM.Global.Merc = null;
            },

            //Show Criteria screen
            viewMercCriteria: function(){
                var gmvMercCriteria = new GMVMercCriteria({"view": ""});
                gmvApp.showView(gmvMercCriteria);
            },
            
            //View Merc Report by region search
            lookupByRegion: function (region) {
                if(region == 'US')
                    var regnid = '100823';
                else
                    var regnid = '100824';
                var gmvMercReport = new GMVMercReport({
                    "reportby": 'rptbyregion',
                    "module": 'merc',
                    "regnid": regnid
                });
                gmvApp.showView(gmvMercReport);

            },
            //View Merc Report by webinar
            lookupByRptType: function (rptType) {
                if(rptType == 'webinar'){
                var gmvMercReport = new GMVMercReport({
                    "reportby": 'rptbywebinar',
                    "module": 'merc'
                });
                gmvApp.showView(gmvMercReport);
                }
           
            },
            //View Merc Report by Title search
            lookupByName: function (searchnm) {
                var gmvMercReport = new GMVMercReport({
                    "reportby": 'rptbyname',
                    "module": 'merc',
                    "title": searchnm
                });
                gmvApp.showView(gmvMercReport);

            },
            
            //View Merc Report by Filter search            
            lookupByFilter: function(reporttyp){
                var gmvMercReport = new GMVMercReport({
                    "reportby": reporttyp,
                    "module": 'merc'
                });
                gmvApp.showView(gmvMercReport);
            },
            
            // Merc Detail on create, view and Edit
            mercDetail: function(mercid){
                console.log("Merc Detail Mode");
                if (GM.Global.Merc == undefined)
                    GM.Global.Merc = null;
                var input = {
                    "token": localStorage.getItem("token"),
                    "mercid": mercid
                };

                if (mercid != 0) {
                    var template = fnGetTemplate(URL_Common_Template, "gmtSpinner");
                    $("#div-crm-module").html(template());
                    var input = {
                        "token": localStorage.getItem("token"),
                        "mercid": mercid
                    }
                    console.log("Input is " + JSON.stringify(input));
                    fnGetWebServerData("merc/info", undefined, input, function (data) {
                        if (data != null && data.mercid != "") {
                            console.log("Activity Info Data >>>>> " + JSON.stringify(data));
                            var gmvMerc = new GMVMerc({
                                module: "merc",
                                "mercid": mercid,
                                "data": data
                            });
                            gmvApp.showView(gmvMerc);
                        } else {
                            GM.Global.AlertMsg = "No Such Activity";
                            window.location.href = "#crm/merc";
                        }
                    });

                } else {
                    var gmvMerc = new GMVMerc({
                        module: "merc",
                        "mercid": 0,
                        "data": {}
                    });
                    gmvApp.showView(gmvMerc);

                }
            },


        });
        return GM.Router.Merc;
    });