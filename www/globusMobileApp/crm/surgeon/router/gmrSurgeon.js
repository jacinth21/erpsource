/**********************************************************************************
 * File:        gmrSurgeon.js
 * Description: Router for all of Surgeon
 * Version:     1.0
 * Author:      cskumar
 **********************************************************************************/
define([

  // External Libraries
  'jquery', 'underscore', 'backbone',

  //Common
    'gmvCRMMain', 'gmvCRMData',
  //'gmvCRMData', 'gmvCRMMain','gmvMain',
  
  //Views
   'gmvSurgeonDetail', 'gmvSurgeonCriteria', 'gmvSurgeonFilter', 'gmvSurgeonEducation', 'gmvSurgeonReport','gmvAccountSurgeonRpt','gmvMercReport', 'gmvAccountSurgicalActivity',
    
    //local db
    'crmdb'
], 
    
function ($, _, Backbone,
           GMVCRMMain, GMVCRMData,
           GMVSurgeonDetail, GMVSurgeonCriteria, GMVSurgeonFilter, GMVSurgeonEducation, GMVSurgeonReport, GMVAccountSurgeonRpt, GMVMercReport, GMVAccountSurgicalActivity, CRMDB) {

    'use strict';
    
    GM.Router.Surgeon = Backbone.Router.extend({
        
        //routes to its respective web page 
        routes: {
            "crm/surgeon":                                      "viewSurgeon",
            "crm/surgeon/filter/:FilterID":                     "showFilter",
            "crm/surgeon/id/:partyid":                          "surgeonDetail",
            "crm/surgeon/id/:partyid/:viewTab":                 "surgeonDetail",
            "crm/surgeon/name/:surgeonFName/:surgeonLName":     "listSurgeonbyName",
            "crm/surgeon/repid/:repid":                         "listSurgeonbyName",
            "crm/surgeon/list/:criteriaID":                     "listSurgeonbyFilter",
            "crm/surgeon/list":                                 "listSurgeonbyFilter",
            "crm/surgeon/keywords/:keywords":                   "listSurgeonbyKeywords",
            "crm/surgeon/filter/report/:reportby":              "listSurgeonReport",
            "crm/surgeon/filter/report/:reportby/:acslvl/:refid":             "listSummaryReport",
            "crm/surgeon/filter/report/:reportby/:grid":        "listRevenueReport",
            "crm/surgeon/account/id/:accid":    "accountSurgeonRpt",
            "crm/surgeon/account/surgactivity/id/:accid":    "accountSurgActivity"
        },
        
        //things to execute before the routing
        execute: function(callback, args, name) {
            console.log(GM.Global.CRMMain)
            var lastEl = GM.Global.Url[GM.Global.Url.length-2];
            if(lastEl.indexOf("#crm") == -1)
                GM.Global.CRMMain=undefined;
            if(GM.Global.CRMMain==undefined){
                commonSetUp();
                GM.Global.Module = "surgeon"
                GM.Global.CRMMain = true;
                var gmvCRMMain = new GMVCRMMain();
                gmvApp.showView(gmvCRMMain);
            }
            $('.gmTab').removeClass().addClass('gmTab');
            $("#surgeon").addClass("gmTab tabsurgeon gmFontWhite");
            if(GM.Global.AlertMsg){
                showError(GM.Global.AlertMsg);
                GM.Global.AlertMsg = undefined;
            }
            else
            {
                toErr = setTimeout(function() {
                    hideMessages();
                }, 2500)
            }
            
            
            if (callback) callback.apply(this, args);
        },

        initialize: function() {
            GM.Global.Surgeon =  null;
            $("#div-app-title").show();
            $("#span-app-title").html("Surgeon");
            $(".li-icon-back").show();
        },
        
        //Show Surgeon Details by PartyID
        surgeonDetail: function(partyID) {
            if(partyID!=0){
	           var template = fnGetTemplate(URL_Common_Template, "gmtSpinner");
                $("#div-crm-module").html(template());
                if(GM.Global.Device){
                    surgeonInfo(partyID, function(data){
                        var gmvSurgeonDetail = new GMVSurgeonDetail({"partyid": partyID, "data": data});
                        gmvApp.showView(gmvSurgeonDetail);
                    }, "gmvSurgeon");
                }
                else{
                    var input = { "token": localStorage.getItem("token"), "partyid": partyID};
                    fnGetWebServerData("surgeon/info/", undefined, input, function(data) {
                        if(data != null){
                            console.log("Surgeon Data"+ JSON.stringify(data));
                            var gmvSurgeonDetail = new GMVSurgeonDetail({"partyid": partyID, "data": data});
                            gmvApp.showView(gmvSurgeonDetail);
                        }
                        else{
                            GM.Global.AlertMsg = "No Such Surgeon";
                            window.location.href = "#crm/surgeon";
                        }
                    }, true);
                }
            }
            else{
                var gmvSurgeonDetail = new GMVSurgeonDetail({"partyid": 0, "data": {}});
                gmvApp.showView(gmvSurgeonDetail);
            }
        },

        // List Surgeons by First Name and/or Last Name
        listSurgeonbyName: function(){
          
          var gmvCRMData = new GMVCRMData({module:"surgeon"});
          gmvApp.showView(gmvCRMData);
          
        },
        
        // List Surgeons by Keywords
        listSurgeonbyKeywords: function(keywords) {
            GM.Global.Surgeon["keywords"] = keywords;
            var gmvCRMData = new GMVCRMData({module:"surgeon"});
            gmvApp.showView(gmvCRMData);
        },
        
        // List Surgeons by selected filter values
        listSurgeonbyFilter: function(criteriaID) {
            console.log("listSurgeon");
            if(GM.Global.Surgeon == undefined){
                GM.Global.Surgeon = {}
                GM.Global.Surgeon.Filter = null;
            }
//            if (GM.Global.SalesRep)
//                GM.Global.Surgeon["criteriaid"] = "0";
//            else
                GM.Global.Surgeon["criteriaid"] = criteriaID;
            
            if(GM.Global.PreUrl == "crm/dashboard" && GM.Global.Surgeon.keywords)
                GM.Global.Surgeon.keywords = undefined;
            var gmvCRMData = new GMVCRMData({module:"surgeon"});
            gmvApp.showView(gmvCRMData);            
        },

        //View Surgeon
        viewSurgeon: function() {
            
//            if (isRep()) {
//                GM.Global.Surgeon = {};
//                this.listSurgeonbyFilter();
//            } else {
                GM.Global.Merc={};
                GM.Global.CurrentTab="";
                var gmvSurgeonCriteria = new GMVSurgeonCriteria();
                gmvApp.showView(gmvSurgeonCriteria);
//            }
        },
        
		// load Surgeon Report View
        listSurgeonReport: function (reportby) {
            GM.Global.CurrentTab="";
            if(reportby=="mercsurgrpt"){                
                var gmvMercReport = new GMVMercReport({module: 'surgeon',reportby: reportby});
                gmvApp.showView(gmvMercReport); 
            }else{
                var gmvSurgeonReport = new GMVSurgeonReport({reportby: reportby});
                gmvApp.showView(gmvSurgeonReport); 
            }
        },
        
        listSummaryReport: function (reportby, acslvl, refid) {
            GM.Global.Surgeon.Revenue=undefined;            
            var gmvSurgeonReport = new GMVSurgeonReport({reportby: reportby, acslvl: acslvl, refid: refid});
            gmvApp.showView(gmvSurgeonReport); 
        },
        
        showFilter: function(FilterID) {
            var gmvSurgeonFilter = new GMVSurgeonFilter({filterid: FilterID});
            gmvApp.showView(gmvSurgeonFilter);
            console.log(gmvApp)
        },
        
        listRevenueReport: function(reportby, grid) {
                var gmvSurgeonReport = new GMVSurgeonReport({reportby: reportby, grid: grid});
                gmvApp.showView(gmvSurgeonReport);
        },
        
        accountSurgeonRpt: function(accid){
            var gmvAccountSurgeonRpt = new GMVAccountSurgeonRpt({accid: accid});
            gmvApp.showView(gmvAccountSurgeonRpt);
        },
        
        accountSurgActivity: function(accid){
            var gmvAccountSurgicalActivity = new GMVAccountSurgicalActivity({accid: accid });
            gmvApp.showView(gmvAccountSurgicalActivity);
        }
        
  });
    return GM.Router.Surgeon;
});


