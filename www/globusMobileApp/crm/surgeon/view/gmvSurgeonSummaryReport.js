    /**********************************************************************************
     * File             :        gmvSurgeonSummaryReport.js
     * Description      :        View to represent Surgeon Summary Report List and Charts
     * Version          :        1.0
     * Author           :        mahendrand
     **********************************************************************************/
    define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars', 'jqueryui',

    // Common
    'global',

    // View
    'gmvSurgeonSurgicalUtil',

    // Pre-Compiled Template
    'gmtTemplate', 'gmvSearch', 'commonutil', 'gmcItem', 'gmvItemList'

    //Model

],

        function ($, _, Backbone, Handlebars, JqueryUI,
            Global,
            GMVSurgeonSurgicalUtil, GMTTemplate, GMVSearch, Commonutil, GMCItem, GMVItemList) {

            'use strict';

            GM.View.SurgeonSummaryReport = Backbone.View.extend({

                el: "#surgeon-report-section",

                name: "CRM Surgeon Summary Report View",

                /* Load the templates */
                template_SummaryReportMain: fnGetTemplate(URL_Surgeon_Template, "gmtSummaryReportMain"),
                template_SummaryReportList: fnGetTemplate(URL_Surgeon_Template, "gmtSummaryReportList"),
                template_DhxReportMobile: fnGetTemplate(URL_Common_Template, "gmtDhxReportMobile"),

                events: {
                    "click .li-chart-btn": "ToggleChart",  
                },

                initialize: function (options) {
                    this.reportData = [];
                    GM.Global.Surgeon.Summary={};
                    if (options.acslvl)
                    GM.Global.Surgeon.Summary["AccessLvl"]=options.acslvl;
                    else
                    GM.Global.Surgeon.Summary["AccessLvl"]="VP";                    
                    this.render(options);                    
                },

                render: function (options) {
                    var that = this;   
                    if(options.grid)
                        var templopt="revenue";
                    else
                        var templopt="summary";
                    
                    this.$("#surgeon-report-section").html(this.template_SummaryReportMain({"templopt" : templopt}));
                    
                    if(options.grid){
                        $(".gmPanelTitle").html("Surgeon's Revenue (In 000's)");
                        console.log(">>>>>>>>>>>>>GRID>>"+GM.Global.Surgeon.Revenue);
                        monthData();
                        yearData();
                        $(".surgin-dr-tab").bind("click", function (event) {
                            that.clickDateTabs(event);
                        }); 
                        $("#btn-load-chart").bind("click", function (event) {
                            that.listSurgeonRevenueWFilter();
                        });     
                         that.getSixMDiff(function(fromDt, toDt){          
                             GM.Global.Surgeon.Revenue["FromDt"]=fromDt;                    
                             GM.Global.Surgeon.Revenue["ToDt"]=toDt; 
                             that.renderRevenueList();
                         });
                    } else  if (options.acslvl) {                        
                        $(".gmPanelTitle").html("<a class='backReport' href='javascript:window.history.back();'>"+options.viewby + " / " + this.acsLvlAbr(options.acslvl)+"</a>");
                        var input = {};
                        input.token = localStorage.getItem("token");
                        input.stropt = options.acslvl;
                        input.ref_id = options.refid; 
                        console.log("Render INPUT>>>>>>>>" + JSON.stringify(input));                        
                        this.renderReportChart(input);
                    } else
                        that.viewSummary();
                },

                
                //Returns the Abbreviation of Access Levels
                acsLvlAbr: function (acslvl) {
                    var nso;
                    switch (acslvl) {

                        case "VP":
                            nso = "Vice President";
                            break;
                        case "AD":
                            nso = "Area Director";
                            break;
                        case "D":
                            nso = "Distributor";
                            break;
                        case "REP":
                            nso = "Sales Representative";
                            break;
                        case "MYREP":
                            nso = "Sales Representative";
                            break;                            
                    }
                    return nso;
                },
                
                //Return the Next Level of Access Levels
                acsLvlfn: function (acslvl) {
                    var nso;
                    switch (acslvl) {
                        case "VP":
                            nso = "AD";
                            break;
                        case "AD":
                            nso = "D";
                            break;
                        case "D":
                            nso = "REP";
                            break;
                    }
                    return nso;
                },
                
                //Triggers the appropriate charts for which icon i have clicked
                ToggleChart: function (e) {
                    var that = this;
                    var chartName = $(e.currentTarget).data('chartname');
                    $(".li-chart-btn").removeClass('active');
                    $("." + chartName + "-btn").addClass('active');
                    this.showReportChart(chartName, that.reportData);
                },
                
                //To Display Chart for a given chartdata and chartname
                showReportChart: function (chartNm, reportdata) {
                    var url = window.location.href.replace(/\/$/, '');
                    var array = url.split('/');
                    var acslvlSeg = array[array.length - 2];
                    if (acslvlSeg == 'report')
                        acslvlSeg = 'VP'
                    acslvlSeg = acslvlSeg.toLowerCase();
                    $(".borderListChart").removeClass("hide");
                    $(".ul-chart-btn").removeClass("hide");
                    if (chartNm == 'piechart')
                        summaryreportPChart(acslvlSeg, reportdata);
                    else
                        summaryStackchartData(acslvlSeg, reportdata, function (reportchartdata) {
                            summaryreportSBchart(reportchartdata);
                        });
                },
                
                //to show the report based on the users access level
                viewSummary: function () {
                    var that = this;
                    var Dept = localStorage.getItem('Dept');

                    if (Dept != '2005') {   //if not sales admin
                        var input = {};
                        input.token = localStorage.getItem("token");
                        input.stropt = "VP";
                        this.renderReportChart(input);
                    } else {                        
                        var acsid = localStorage.getItem('acid');
                        var refid = localStorage.getItem('userID');
                        switch(acsid){
                            case "1": //Sales Rep 
                               window.location.href = '#crm/surgeon/filter/report/summary/MYREP/' + refid; 
                               break;
                            case "2": //Distributor
                               window.location.href = '#crm/surgeon/filter/report/summary/REP/' + refid; 
                               break;
                            case "3": //AD
                               window.location.href = '#crm/surgeon/filter/report/summary/D/' + refid; 
                               break;
                            case "4": //VP
                               window.location.href = '#crm/surgeon/filter/report/summary/AD/' + refid; 
                               break;
                            case "6": //Other AD's
                               window.location.href = '#crm/surgeon/filter/report/summary/D/' + refid; 
                               break;
                            case "7": //ASS(Sales Rep) // pass repid from json                                
                                var userdata=$.parseJSON(localStorage.getItem('userData'));
                               window.location.href = '#crm/surgeon/filter/report/summary/MYREP/' + userdata.repid; 
                               break;                                
                        }
                    }
                },
                
                //To show the report list for the given input
                renderReportChart: function (input) {
                    var that = this;
                    var tempSpinner = fnGetTemplate(URL_Common_Template, "gmtSpinner");
                    $("#summary-report-list").html(tempSpinner);

                    summaryReportData(input, function (data) {
                        that.reportData = data;
                        if (data != null) {
                            if(input.stropt == 'MYREP'){                                
                            data = changePropName(data, 'rep_id', 'nrefid');
                            data = changePropName(data, 'rep_name', 'nrefnm');
                            }
                            else{
                            data = changePropName(data, input.stropt.toLowerCase() + '_id', 'nrefid');
                            data = changePropName(data, input.stropt.toLowerCase() + '_name', 'nrefnm');
                            }
                            _.each(data, function (dt) {
                                dt.acsLvl = that.acsLvlfn(input.stropt);
                                if(input.stropt == 'MYREP')
                                    dt.acsLvl=input.stropt;
                                dt.acsLink = 1;
                                if (input.stropt == 'REP' || input.stropt=='MYREP')
                                    dt.acsLink = 0;
                            });
                            console.log("DATA>>>>>>" + JSON.stringify(data));
                            var rows = [],
                                i = 0;
                            _.each(data, function (griddt) {
                                rows[i] = {};
                                rows[i].id = i;
                                rows[i].data = [];
                                if (griddt.acsLink == 0)
                                    rows[i].data[0] = "<a data-acslink='" + griddt.acsLink + "' data-refid='" + griddt.nrefid + "' data-accesslvl='" + griddt.acsLvl + "'>" + griddt.nrefnm + "</a>";
                                else
                                    rows[i].data[0] = "<a class='gmReportLnk'  data-acslink='" + griddt.acsLink + "' data-refid='" + griddt.nrefid + "' data-accesslvl='" + griddt.acsLvl + "'>" + griddt.nrefnm + "</a>";
                                rows[i].data[1] = "<a class='revenueList' data-refid='" + griddt.nrefid + "' data-accesslvl='" + griddt.acsLvl + "' title='Revenue List'><i class='gmFontsurgeon fa fa-user-md fa-2x' aria-hidden='true'></i></a>";

                                rows[i].data[2] = griddt.active;
                                rows[i].data[3] = griddt.inprogress;
                                rows[i].data[4] = griddt.open;
                                rows[i].data[5] = griddt.total;
                                i++;
                            });
                            var dataDO = {};
                            dataDO.rows = rows;
                           if($(window).width() > 767){
                                  var setGridHeight = 350;
                                    var setInitWidths = "*,50,100,100,100,100";
                             }
                            else if($(window).width() < 400){
                                  var setGridHeight = 380;
                                    var setInitWidths = "100,33,49,65,48,50";
                             }
                            else{
                                  var setGridHeight = 380;
                                    var setInitWidths = "125,30,53,65,53,53";
                             }
                            var setColAlign = "left,center,right,right,right,right";
                            var setColTypes = "ro,ro,ro,ro,ro,ro";
                            var setColSorting = "str,na,int,int,int,int";
                            var setHeader = [that.acsLvlAbr(input.stropt) + "(s)", "", "Active", "In-Progress", "Open", "Total"];
                            var enableTooltips = "false,false,false,false,false,false";
                            var attachFilter = "#text_filter,,#numeric_filter,#numeric_filter,#numeric_filter,#numeric_filter";
                            var footerArr = ["Total", "<a class='allrevenueList' title='Revenue List'><i class='gmFontsurgeon fa fa-user-md fa-2x' aria-hidden='true'></i></a>", "<div id='totalc2' class='gmReportLnk showAllSurgeons' data-status='400901'></div>", "<div id='totalc3' class='gmReportLnk showAllSurgeons' data-status='400902'></div>", "<div id='totalc4' class='gmReportLnk showAllSurgeons' data-status='400903'></div>", "<div id='totalc5' class='gmReportLnk showAllSurgeons' data-status='total'></div>"];
                            var footerArrStyles = ["text-align:right;", "text-align:center;", "text-align:right;", "text-align:right;", "text-align:right;", "text-align:right;"]
                            var footerExportFL = true;
                            var mobResArg=true;
                            dhxReportDataFmt(rows,setHeader, function(dt){
                            that.rptTemplate = that.template_DhxReportMobile(dt);
                             });
                            that.gridObj = loadDHTMLXGrid('summary-report-list', setGridHeight, dataDO, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, attachFilter, footerArr, footerArrStyles, footerExportFL, that.rptTemplate, mobResArg);
                            var rowCount = that.gridObj.getRowsNum();
                            var colCount = that.gridObj.getColumnsNum();
                            console.log(colCount);
                            var refidArr = [];
                            for (var i = 0; i < rowCount; i++) {
                                var firstColVals = that.gridObj.cells(i, 0).getValue();
                                var colRefids = $(firstColVals).data('refid');
                                refidArr[i] = colRefids;
                                for (j = 2; j < 6; j++)
                                    that.gridObj.cells(i, j).cell.className = 'gmReportLnk';
                            }

                            console.log(refidArr);
                            var refidStr = eval(refidArr).join(",");
                            console.log(refidStr);
                            for (var l = 0; l < colCount; l++) {
                                if (l > 1)
                                    document.getElementById("totalc" + l).innerHTML = that.sumColumn(l, that.gridObj);
                            }
                            $(".showAllSurgeons").bind("click", function (event) {
                                var allStatus = $(event.currentTarget).data('status');
                                console.log(allStatus);
                                that.listSurgeonReport(allStatus, refidStr);

                            });

                            $(".allrevenueList").bind("click", function (event) {

                                that.listSurgeonRevenue(refidStr);
                            });
                            if (footerExportFL) {
                                var deleteIndexArr = []; //include the index of the column in the array if u want to export                 
                                $("#summary-report-list-export").unbind("click").bind("click", function (event) {
                                    exportExcel(event, that.gridObj, deleteIndexArr);
                                });
                            }
                            that.gridObj.attachEvent("onRowSelect", function (id, index) {
                                var firstColVal = that.gridObj.cells(id, 0).getValue();
                                var colRefid = $(firstColVal).data('refid');
                                var colAccessLvl = $(firstColVal).data('accesslvl');
                                var colAccsessLink = $(firstColVal).data('acslink');
                                var colStatus = "";
                                if (index == 0) {
                                    if (colAccsessLink == 1)
                                        window.location.href = '#crm/surgeon/filter/report/summary/' + colAccessLvl + '/' + colRefid;
                                } else if (index == 1)
                                    that.listSurgeonRevenue(colRefid);
                                else if (index == 2)
                                    colStatus = "400901";
                                else if (index == 3)
                                    colStatus = "400902";
                                else if (index == 4)
                                    colStatus = "400903";
                                else if (index == 5)
                                    colStatus = "total";
                                if (index > 1)
                                    that.listSurgeonReport(colStatus, colRefid);

                            });


                            var chartNm = checkChartNm();
                        $('.li-chart-btn').removeClass('active');
                        $("." + chartNm + "-btn").addClass('active');
                        that.showReportChart(chartNm, that.reportData);
                        } else{
                            $(controlID).html("No Data Found");
                            $("#summary-report-section").css("margin-top","-20px");
                        }                       

                    });
                },
                
                //Render the list view
                showItemListView: function (controlID,
                    data,
                    itemtemplate,
                    columnheader,
                    sortoptions,
                    template_URL,
                    filterOptions,
                    csvFileName,
                    color,
                    callbackOnReRender) {

                    data = arrayOf(data)
                    for (var i = 0; i < data.length; i++) {
                        var row = data[i];
                        data[i].search = "";
                        if (this.search && this.search.length) {
                            _.each(this.search, function (srh) {
                                data[i].search += row[srh] + " ";
                            })
                        }
                    }
                    var items = new GMCItem(data);
                    this.itemlistview = new GMVItemList({
                        el: controlID,
                        collection: items,
                        columnheader: columnheader,
                        itemtemplate: itemtemplate,
                        sortoptions: sortoptions,
                        template_URL: template_URL,
                        filterOptions: filterOptions,
                        csvFileName: csvFileName,
                        color: color,
                        callbackOnReRender: callbackOnReRender
                    });
                },  
                
            //To List Surgeon For the clicking counts in the Summary Report
                listSurgeonReport: function(status,refid){
                    GM.Global.Surgeon.Revenue=undefined;
                    GM.Global.Surgeon.Summary["Status"]=status;
                    GM.Global.Surgeon.Summary["RefID"]=refid;
                    window.location.href = '#crm/surgeon/list/0';                
                },
              
            //Get Date From Which date Range is Selected in the Filter callback to the called function    
            getDateInputs: function(callback){               
                var dateRange, dr, fromDt, toDt;
                this.$('.surg-datadr .surgin-dr-tab').each(function (i) {
                    if ($(this).hasClass("selected")) {
                        dateRange = $(this).attr('id');
                    }                 
                });
                    switch (dateRange) {
                        case '3M':
                            dr = 2;
                            break;
                        case '6M':
                            dr = 5;
                            break;
                        case '1Y':
                            dr = 11;
                            break;
                        case 'custom':
                            break;
                    }                    
                if (dateRange == 'custom') {
                    var fromDtmm = $("#select-surg-surgstartmonth option:selected").text();
                    fromDtyyyy = $("#select-surg-surgstartyear option:selected").val();
                    var toDtmm = $("#select-surg-surgendmonth option:selected").text();
                    toDtyyyy = $("#select-surg-surgendyear option:selected").val();
                    if(fromDtmm=="MM")
                        fromDtmm="";
                    if(toDtmm=="MM")
                        toDtmm="";
                    fromDt = fromDtyyyy + "" + fromDtmm;
                    toDt = toDtyyyy + "" + toDtmm;
                } else {
                    fromDt = new Date();
                    toDt = new Date();
                    fromDt.setMonth(fromDt.getMonth() - dr);
                    var fromDtdd = ("0" + fromDt.getDate()).slice(-2),
                        fromDtmm = ("0" + (fromDt.getMonth() + 1)).slice(-2),
                        fromDtyyyy = fromDt.getFullYear();
                    var toDtdd = ("0" + toDt.getDate()).slice(-2),
                        toDtmm = ("0" + (toDt.getMonth() + 1)).slice(-2),
                        toDtyyyy = toDt.getFullYear();
                    fromDt = fromDtyyyy + "" + fromDtmm;
                    toDt = toDtyyyy + "" + toDtmm;
                }
                callback(fromDt, toDt);
            },
                
            //  On clicking Date tabs to highlight the tab
            clickDateTabs: function (e) {
                var dateRange = $(e.currentTarget).attr('id');
                $('.surgin-dr-tab').removeClass("gmBGDarkerGrey gmFontWhite selected");
                $('.surgin-dr-tab').removeClass("gmBGGrey");
                $('.surgin-dr-tab').addClass("gmBGGrey");
                $('#' + dateRange).removeClass("gmBGGrey");
                $('#' + dateRange).addClass("gmBGDarkerGrey gmFontWhite selected");
                if (dateRange == 'custom') {
                    $('.SHB-right').removeClass('hide');
                } else {
                    if (!($('.SHB-right').hasClass('hide')))
                        $('.SHB-right').addClass('hide');
                }
            },
                
            //On loading the Revenue Screen gets six months data by using this function
                getSixMDiff: function(callback){  
                    var fromDt, toDt;
                fromDt = new Date();
                    toDt = new Date();
                    fromDt.setMonth(fromDt.getMonth() - 5);
                    var fromDtdd = ("0" + fromDt.getDate()).slice(-2),
                        fromDtmm = ("0" + (fromDt.getMonth() + 1)).slice(-2),
                        fromDtyyyy = fromDt.getFullYear();
                    var toDtdd = ("0" + toDt.getDate()).slice(-2),
                        toDtmm = ("0" + (toDt.getMonth() + 1)).slice(-2),
                        toDtyyyy = toDt.getFullYear();
                    fromDt = fromDtyyyy + "" + fromDtmm;
                    toDt = toDtyyyy + "" + toDtmm;
                    callback(fromDt, toDt);
                },
                
                //on Load button clicked the function called and it loads the data as given filter date range
                listSurgeonRevenueWFilter: function(){
                    var that=this;
                    this.getDateInputs(function(fromDt, toDt){
                    if( fromDt.length==6 && toDt.length==6){
                        if (fromDt <= toDt ) {              
                    GM.Global.Surgeon.Revenue["FromDt"]=fromDt;                    
                    GM.Global.Surgeon.Revenue["ToDt"]=toDt; 
                            that.renderRevenueList();
                       } else 
                    showError("Please Select From Date is Less than To Date");                                   
                }
                 else
                    showError("From Date or To Date may be Empty, Please Select the Date Fields");
                    
                });      
                },
                
                //To List Surgeon Revenue For the clicking the surgeon icon in the Summary Report
                listSurgeonRevenue: function(refid){                      
                    this.getSixMDiff(function(fromDt, toDt){                                        
                    GM.Global.Surgeon.Revenue={};
                    GM.Global.Surgeon.Revenue["RefID"]=refid;
                    GM.Global.Surgeon.Revenue["AccessLvl"]=GM.Global.Surgeon.Summary["AccessLvl"];
                    GM.Global.Surgeon.Revenue["FromDt"]=fromDt;                    
                    GM.Global.Surgeon.Revenue["ToDt"]=toDt;                    
                    window.location.href = "#crm/surgeon/filter/report/summary/revenuelist" ;   
                });      
                },
                
                //To display Revenue Report for Surgeons
                renderRevenueList: function () {
                          var that = this;
                          this.gridObj = "";
                          this.rows = [];
                          var input = {};
                          input.stropt = "revenuelist";
                          input.refid = GM.Global.Surgeon.Revenue["RefID"];
                          input.accesstype = GM.Global.Surgeon.Revenue["AccessLvl"];
                          input.fromdt = GM.Global.Surgeon.Revenue["FromDt"];
                          input.todt = GM.Global.Surgeon.Revenue["ToDt"];
                          var tempSpinner = fnGetTemplate(URL_Common_Template, "gmtSpinner");
                          $("#summary-revenue-list").html(tempSpinner);

                          summaryRevenueData(input, function (data) {
                              console.log(data);
                              if(data.gmCRMRevenueDetailVO.length > 0){
                              var revheader = data.gmCRMRevenueHeaderVO;
                              var revdetail = data.gmCRMRevenueDetailVO;
                              var hdMonthLength = revheader.length;
                              var gridData = [];
                                  
                              _.each(revdetail, function (detail) {
                                  var gridItem = {};
                                  var indexs = partyExist(gridData, detail.partyid);
                                  var detailValue = detail.value/1000;
                                  if (indexs == -1) {
                                      gridItem.partyid = detail.partyid;
                                      gridItem.partynm = detail.partynm;
                                      gridItem.partylink = "<a href='#crm/surgeon/id/" + detail.partyid + "' title='info'>i</a>";   
                                      gridItem.partyimg = URL_WSServer+"file/src?refgroup=103112&refid=" + detail.partyid + "&reftype=null&num=" + Math.random();
                                      gridItem[detail.yrmonth] = parseFloat(detailValue).toFixed(1);
                                      gridItem.total = parseFloat(detailValue).toFixed(1);
                                      gridData.push(gridItem);
                                  } else {
                                      gridData[indexs][detail.yrmonth] = parseFloat(detailValue).toFixed(1);
                                      gridData[indexs]["total"] = parseFloat(gridData[indexs]["total"]) + parseFloat(detailValue);
                                      gridData[indexs]["total"] = parseFloat(gridData[indexs]["total"]).toFixed(1);
                                  }
                              });                                  


                              var gridTemp = [];
                              for (var i = 0; i < gridData.length; i++) {
                                  gridTemp[i] = {};
                                  _.each(revheader, function (header) {
                                      gridTemp[i][header.yrmonth] = parseFloat(0.0).toFixed(1);
                                  });
                              }

                              var gridObj = [];
                              for (var i = 0; i < gridTemp.length; i++) {
                                  gridObj[i] = {};
                                  gridObj[i] = Object.assign(gridTemp[i], gridData[i]);
                              }

                              console.log(JSON.stringify(gridObj));
                              var rows = [],
                                  i = 0;
                              _.each(gridObj, function (griddt) {
                                  rows[i] = {};
                                  rows[i].id = griddt.partyid;
                                  rows[i].data = [];
                                  rows[i].data[0] = griddt.partyimg;
                                  rows[i].data[1] = "<a class='gmReportLnk' href='#crm/surgeon/id/" + griddt.partyid + "'>"+griddt.partynm+"</a>";
                                  if($(window).width() < 768){
                                    rows[i].data[2] = griddt.total;
                                    var j = 4;
                                  }
                                  else{
                                      var j = 2;
                                  }
                                  _.each(revheader, function (header) {
                                      rows[i].data[j] = griddt[header.yrmonth];
                                      j++;
                                  });
                                  
                                  if($(window).width() > 767){
                                      rows[i].data[j] = griddt.total;
                                  }
                                  i++;
                              });
                              var data = {};
                              data.rows = rows;

                              var headerTitle = [],
                                  headerWidths = [],
                                  headerTypes = [],
                                  headerSorting = [],
                                  headerColAlign=[],
                                  enableTooltips=[],
                                  headerFilter=[],
                                  footerDiv = [];
                              for(var s=0; s<data.rows[0].data.length;s++){
                                  headerTypes[s]="ro" ;
                                  headerWidths[s]="80";
                                  enableTooltips[s]="true";
                                if(s<2){
                                 headerColAlign[s] = "center";
                                 headerSorting[s]="na";
                                 headerFilter[s]=""
                                    if(s==1){
                                        headerFilter[s]="#text_filter";
                                        headerSorting[s]="str";
                                     headerColAlign[s] = "left";                                         
                                     if(data.rows[0].data.length<12)
                                     headerWidths[s] = "*";
                                     else
                                     headerWidths[s] = "150";    
                                    }
                                    if(s==0){
                                        headerTypes[s]="img";
                                        enableTooltips[s]="false";
                                         headerWidths[s]="50";
                                    }
                                }
                                else{
                                    headerFilter[s]="#numeric_filter"
                                 headerColAlign[s] = "right";   
                                 headerSorting[s] = "int";
                                    headerWidths[s] = "80";
                                    footerDiv[s] = "<div id='totalc" + s + "'></div>";
//                                    footerDiv[s] = "#stat_total";
                                }                                 
                              }
                              headerTitle[0] = ""; footerDiv[0] = "Total";
                              headerTitle[1] = "Surgeon"; footerDiv[1] = "#cspan";
                              
                              if($(window).width() < 768){
                               headerTitle[2] = "Total";
                                var k = 4;
                              }
                              else{
                                  var k = 2;
                              }
                              _.each(revheader, function (header) {
                                  headerTitle[k] = header.monthvalue.toLowerCase();
                                  headerTitle[k] = header.monthvalue.charAt(0)+ headerTitle[k].slice(1);
                                  k++;
                              });
                              if($(window).width() > 767){
                                headerTitle[k] = "Total";
                              }
                              
                              var strheaderWidths = eval(headerWidths).join(",");
                              var strheaderTypes = eval(headerTypes).join(",");
                              var strheaderSorting = eval(headerSorting).join(",");
                              var strenableTooltips = eval(enableTooltips).join(",");
                              var strheaderColAlign = eval(headerColAlign).join(",");
                                  var footerExportFL=true;
                                  var mobResArg=false;
                                  console.log(data.rows);
                                  if($(window).width() < 768){
                                  _.each(data.rows, function (dt) {
                                      console.log(dt.data[0]);
                                      dt.data[0]="<img class='gmRevReportImg' src='"+dt.data[0]+"'/>";
                                  });
                                  }
                            dhxReportDataFmt(data.rows,headerTitle, function(dt){
                            that.rptTemplate = that.template_DhxReportMobile(dt);
                             });
                                  
                              that.gridObj = loadDHTMLXGrid('summary-revenue-list', '550',data, headerTitle, strheaderWidths, strheaderColAlign, strheaderTypes, strheaderSorting, strenableTooltips,headerFilter, footerDiv,[],footerExportFL, that.rptTemplate,mobResArg);
                                  
                                  if ($(window).width() < 768) {
                                  $("#summary-revenue-list .div-dhx-report-mbl").each(function () {
                                      var imageUrl = $(this).find("img").attr("src");
                                      var imageTag = $(this).find("img");
                                      imageExists(imageUrl, imageTag, function (exists) {
                                          //Show the result
                                          console.log('Fileexists=' + exists);
                                          if (!exists) {
                                              imageTag.attr("src", "");
                                              imageTag.after('<i class="fa fa-user avatar-pfl-img" aria-hidden="true"></i>');
                                          }
                                      });
                                  });
                              } else {
                                  $('.objbox tr').each(function () {
                                      var imageUrl = $(this).find("img").attr("src");
                                      var imageTag = $(this).find("img");
                                      imageExists(imageUrl, imageTag, function (exists) {
                                          //Show the result
                                          console.log('Fileexists=' + exists);
                                          if (!exists) {
                                              imageTag.attr("src", "");
                                              imageTag.after('<i class="fa fa-user avatar-pfl-img" aria-hidden="true"></i>');
                                              imageTag.remove();
                                          }
                                      });
                                  });
                              }
                             
                                if($(window).width() > 767){
                    var rowCount=that.gridObj.getRowsNum();                    

                    for(var i=0; i<rowCount; i++){
                        that.gridObj.cells(i, 0).cell.className = 'viewDO gmReportLnk';
                    }    
                                  
                              document.getElementById("totalc" + l).innerHTML = parseFloat(that.sumColumn(l, that.gridObj)).toFixed(1);
                                  $('.objbox > table > tbody  > tr').each(function (tr) {
                                      $(this).find('td').each(function () {  
                                          var hasImage = $('img', this).length > 0;
                                          if (hasImage) {
                                              // image exist
                                              $(this).attr("class","partyimg");
                                              $('img', this).attr("alt","");
                                          }
                                      });
                                  }); 
                                   that.gridObj.attachEvent("onRowSelect", function(id,index){
                                  var surgeonInfo=that.gridObj.cells(id,1).getValue() ;
                                       console.log($(surgeonInfo).attr('href'));
                                       window.location.href = $(surgeonInfo).attr('href');
                                   });
                                    
                                $(".viewDO").bind("click", function (event) {
                        that.openDO(event);
                    })
                                  
                                  if(footerExportFL){
                        var deleteIndexArr=["0"];       //include the index of the column in the array if u want to export                 
                    $("#summary-revenue-list-export").unbind("click").bind("click", function (event) {                        
                        exportExcel(event, that.gridObj, deleteIndexArr);
                    });               
                    }
                                    }else{
                                        
                        
                                       
                $("#summary-revenue-list .div-dhx-report-mbl-header ul li:nth-child(2) ul li:nth-child(2)").addClass("viewDO ReportLnk-mob cellselected");
                                                 
                    $(".viewDO").bind("click", function (event) {
                        that.openDO(event);
                    });
                                        
                      
                     }
                              }
                              else
                                  {
                                      $("#summary-revenue-list").html("No Data Found");
                                  }
                          });
            },
                
//         To manipulate Column total
		sumColumn :function(ind, myGrid){
			var out = 0;
			for(var i=0;i< myGrid.getRowsNum();i++){
				out+= parseFloat(myGrid.cells2(i,ind).getValue());
			}
			return out;
		},


      
               
            });
            return GM.View.SurgeonSummaryReport;
        });