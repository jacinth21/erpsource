    /**********************************************************************************
     * File             :        GmSurgeonDetailView.js
     * Description :        View to represent Surgeon Details
     * Version       :        1.0
     * Author        :        cskumar
     **********************************************************************************/
    define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars', 'jqueryui',

    // Common
    'global', 'gmvSearch',

    // View
    'gmvCRMData', 'gmvCRMSelectPopup',

    // Pre-Compiled Template
    'gmtTemplate'

    //Model

],

        function ($, _, Backbone, Handlebars, JqueryUI,
            Global, GMVSearch,
            GMVCRMData, GMVCRMSelectPopup, GMTTemplate) {

            'use strict';

            GM.View.SurgeonCriteria = Backbone.View.extend({

                el: "#div-crm-module",

                name: "CRM Surgeon Criteria View",

                /* Load the templates */
                template_MainModule: fnGetTemplate(URL_Common_Template, "gmtCRMMainModule"),
                template_SurgeonCriteria: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonCriteria"),
                template_FilterButtons: fnGetTemplate(URL_Common_Template, "gmtFilterButtons"),
                template_selected_items: fnGetTemplate(URL_Common_Template, "gmtPopupSelectedItem"),

                events: {
                    "click #btn-search-npid": "lookupByNPI",
                    "click #btn-search-name": "lookupByName",
                    "click #btn-search-srep": "lookupBySRep",
                    "keypress input": "clickDefaultButton",
                    "click  .btn-listby": "openPopup",
                    "click .sub-tab": "toggleTabs",
                    "click #btn-category-search": "searchCategory",
                    "click .gmBtnCrDelete": "removeFavorite",
                    "click #btn-main-create": "createSurgeon",
                    "change #surgeon-frmdate, #surgeon-todate": "setMinDate",
                    "click .div-text-selected-crt": "toggleSelectedItems",
                    "click .btn-showby": "surgeonReport"
                },

                initialize: function (options) {
                    $(window).on('orientationchange', this.onOrientationChange);
                    GM.Global.Surgeon = {};
                    GM.Global.Surgeon.FilterID = {};
                    GM.Global.Surgeon.FilterName = {};
                    GM.Global.myFilter = null;
                    $("#div-app-title").show();
                    $('#div-app-title').removeClass('gmVisibilityHidden').addClass('gmVisibilityVisible');
                    $("#div-app-title").html("CRM");
                },

                render: function () {
                    var that = this;
                    this.$el.html(this.template_MainModule({
                        "module": "surgeon"
                    }));
                    if (_.contains(GM.Global.Admin, "surgeon"))
                        this.$("#li-create").removeClass("hide");
                    this.$("#div-main-content").html(this.template_SurgeonCriteria());
                    this.$(".gmPanelTitle").html("Lookup Surgeons");

                    var editAccess = _.findWhere(GM.Global.UserAccess, {
                        funcid: "CRM-SURGEON-EDIT"
                    });

                    if (editAccess != undefined) {
                        $("#li-create").removeClass("hide");
                    } else {
                        $("#li-create").addClass("hide");
                    }

                    var surgConsultantFl = _.findWhere(GM.Global.UserAccess, {
                        funcid: "CRM-SURGEON-CONSULT"
                    });
                    if (surgConsultantFl != undefined)
                        this.$("#surgeon-consultant-view").removeClass("hide");
                    else
                        this.$("#surgeon-consultant-view").addClass("hide");

                    this.$("#crm-btn-back").hide();
                    $("#npi-name-key-search").removeClass("sur-criteria-phone-style");
                    if (GM.Global.Device) {
                        fnGetCriteria("surgeon", function (data) {
                            if (data.length) {
                                data = data[0];
                                that.$(".gmMyFav li").removeClass('hide');
                                that.$(".gmSurgeonFav").html(that.template_FilterButtons(arrayOf(data)));
                                that.$(".gmBtnCr").addClass("gmBGsurgeon");
                                that.$(".gmCriteria").addClass("gmBGsurgeon");
                            } else
                                that.$("#li-surgeon-criteria-filter-content").addClass('hide');
                        });
                    } else {
                        var input = {
                            "token": localStorage.getItem("token"),
                            "criteriatyp": "surgeon"
                        };
                        fnGetWebServerData("criteria/list", undefined, input, function (data) {
                            if (data != null) {
                                that.$(".gmMyFav li").removeClass('hide');
                                that.$(".gmSurgeonFav").html(that.template_FilterButtons(arrayOf(data.gmCriteriaVO)));
                                that.$(".gmBtnCr").addClass("gmBGsurgeon");
                                that.$(".gmCriteria").addClass("gmBGsurgeon");
                            } else
                                that.$("#li-surgeon-criteria-filter-content").addClass('hide');
                        });
                    }

                    $("#btn-category-search").hide();
                    this.toggleTabs({
                        "currentTarget": "#tab-npiname"
                    });
                    that.searchRep();
                },

                setMinDate: function (e) {
                    if ($("#surgeon-todate").val() != "" && $("#surgeon-todate").val() != undefined && $("#surgeon-frmdate").val() != undefined && $("#surgeon-frmdate").val() > $("#surgeon-todate").val())
                        $("#surgeon-todate").val($("#surgeon-frmdate").val());
                    var fromDate = $("#surgeon-frmdate").val();
                    var toDate = $("#surgeon-todate").val();

                    if ($(e.currentTarget).attr("id") == "surgeon-frmdate")
                        $("#surgeon-todate").attr("min", fromDate);
                    var dd, mm, yyyy;
                    var date = new Date(fromDate);
                    dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
                    var startDate = mm + "/" + dd + "/" + yyyy;
                    fromDate = startDate;

                    var date = new Date(toDate);
                    dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
                    var endDate = mm + "/" + dd + "/" + yyyy;
                    toDate = endDate;
                    if (GM.Global.Surgeon == undefined)
                        GM.Global.Surgeon = {};
                    if (GM.Global.Surgeon.FilterID == undefined)
                        GM.Global.Surgeon.FilterID = {};
                    if (GM.Global.Surgeon.FilterName == undefined || GM.Global.Surgeon.FilterName == undefined)
                        GM.Global.Surgeon.FilterName = {};
                    else if (typeof (GM.Global.Surgeon.FilterID) != "object")
                        GM.Global.Surgeon.FilterID = $.parseJSON(GM.Global.Surgeon.FilterID);
                    if (typeof (GM.Global.Surgeon.FilterName) != "object")
                        GM.Global.Surgeon.FilterName = $.parseJSON(GM.Global.Surgeon.FilterName);

                    if (toDate != "" && toDate != "aN/aN/NaN" && fromDate != "" && fromDate != "aN/aN/NaN") {
                        GM.Global.Surgeon.FilterName["From Date"] = fromDate;
                        GM.Global.Surgeon.FilterName["To Date"] = toDate;
                        GM.Global.Surgeon.FilterID["actfrmdate"] = fromDate;
                        GM.Global.Surgeon.FilterID["acttodate"] = toDate;
                    } else {
                        GM.Global.Surgeon.FilterName["From Date"] = "";
                        GM.Global.Surgeon.FilterName["To Date"] = "";
                        GM.Global.Surgeon.FilterID["actfrmdate"] = "";
                        GM.Global.Surgeon.FilterID["acttodate"] = "";
                    }

                    var tempArray = new Array();
                    var result = $.parseJSON(JSON.stringify(GM.Global.Surgeon.FilterName));
                    $.each(result, function (key, val) {
                        if (val != "" && val != "aN/aN/NaN")
                            tempArray.push({
                                "title": key,
                                "value": val
                            });
                    });
                    var finalJSON = $.parseJSON(JSON.stringify(tempArray));
                    if (finalJSON.length > 0) {
                        this.$(".list-category-item").html(this.template_selected_items(finalJSON));
                        this.$("#btn-category-search").show();
                    } else {
                        this.$(".list-category-item").empty();
                        this.$("#btn-category-search").hide();
                    }

                    $(".div-criteria").addClass("gmFontsurgeon");

                },

                //option to load data on Enter key
                clickDefaultButton: function (event) {
                    var that = this;
                    if (event.keyCode == 13) {
                        var txtInput = $(event.target).attr('data-id');

                        switch (txtInput) {
                            case "surgeon-npid":
                                that.lookupByNPI(event);
                                break;
                            case "surgeon-fname":
                            case "surgeon-lname":
                                that.lookupByName(event);
                                break;
                        }
                    }
                },

                // Lookup Surgeon Details by providing NPI
                lookupByNPI: function (event) {
                    var npi = $(event.currentTarget).parent().find(".surgeon-npid").val();
                    if (npi.trim() != "")
                        if ($.isNumeric(npi)) { //check if npi is a number only
                            if (GM.Global.Device) {
                                fnGetPartyByNPI(npi, function (data) {
                                    if (data.length) {
                                        data = data[0];
                                        if (data.partyid != null)
                                            window.location.href = "#crm/surgeon/id/" + data.partyid;
                                    } else
                                        showError("No such surgeon");
                                });
                            } else {
                                getSurgeonPartyInfo(npi, function (surgeonData) {
                                    if (surgeonData != undefined) {
                                        surgeonData = surgeonData[0];
                                        if (surgeonData.partyid != null)
                                            window.location.href = "#crm/surgeon/id/" + surgeonData.partyid;
                                    } else
                                        showError("No such surgeon");
                                });
                            }

                        }
                    else
                        showError("NPID should be Numeric!")
                },

                // Lookup Surgeons by providing Surgeon Name
                lookupByName: function (event) {

                    var firstnm = $(event.currentTarget).parent().find(".surgeon-fname").val();
                    var lastnm = $(event.currentTarget).parent().find(".surgeon-lname").val();
                    
		            firstnm = (firstnm.indexOf("’")!=-1)?firstnm.replace("’","'"):firstnm;
                    lastnm  = (lastnm.indexOf("’")!=-1)?lastnm.replace("’","'"):lastnm;

                    GM.Global.Surgeon = {
                        firstnm: firstnm,
                        lastnm: lastnm
                    };

                    firstnm = (firstnm !== "") ? firstnm : "undefined";
                    lastnm = (lastnm !== "") ? lastnm : "undefined";

                    if (!(firstnm == "undefined" && lastnm == "undefined"))
                        window.location.href = "#crm/surgeon/name/" + firstnm + "/" + lastnm;

                },

                // Open Single / Multi Select box        
                openPopup: function (event) {
                    var that = this;
                    var fnName = "";
                    if ($(event.currentTarget).attr("data-title") == "Product Used")
                        fnName = fnGetSystem;
                    else
                        fnName = fnGetCodeNMByGRP;
                    that.systemSelectOptions = {
                        'title': $(event.currentTarget).attr("data-title"),
                        'storagekey': $(event.currentTarget).attr("data-storagekey"),
                        'webservice': $(event.currentTarget).attr("data-webservice"),
                        'resultVO': $(event.currentTarget).attr("data-resultVO"),
                        'IDParameter': $(event.currentTarget).attr("data-IDParameter"),
                        'NameParameter': $(event.currentTarget).attr("data-NameParameter"),
                        'inputParameters': $(event.currentTarget).attr("data-inputParameters"),
                        'codegrp': $(event.currentTarget).attr("data-codegrp"),
                        'module': "surgeon",
                        'event': event,
                        'parent': this,
                        'moduleLS': GM.Global.Surgeon.FilterID,
                        'moduleCLS': GM.Global.Surgeon.FilterName,
                        'fnName': fnName,
                        'callback': function (ID, Name) {
                            GM.Global.Surgeon.FilterID = ID;
                            GM.Global.Surgeon.FilterName = Name;
                            if (GM.Global.Surgeon == undefined)
                                GM.Global.Surgeon = {};
                            GM.Global.Surgeon.FilterID = ID;
                        }
                    };

                    that.systemSelectView = new GMVCRMSelectPopup(that.systemSelectOptions);
                },

                // Lookup Surgeons by Activity Date
                activityDate: function () {
                    var that = this;
                    var fromdate = $("#surgeon-frmdate").val();
                    var todate = $("#surgeon-todate").val();

                    if (fromdate != "" && fromdate != "aN/aN/NaN" && todate != "" && todate != "aN/aN/NaN") {
                        var dd, mm, yyyy, date;
                        date = new Date(fromdate);

                        dd = ("0" + date.getDate()).slice(-2);
                        mm = ("0" + (date.getMonth() + 1)).slice(-2);
                        yyyy = date.getFullYear();

                        if (GM.Global.Surgeon.Filter == undefined)
                            GM.Global.Surgeon.Filter = {};
                        if (GM.Global.Surgeon.FilterName == undefined)
                            GM.Global.Surgeon.FilterName = {};

                        GM.Global.Surgeon.Filter["actfrmdate"] = getDateByFormat(fromdate);
                        GM.Global.Surgeon.FilterName["From Date"] = getDateByFormat(fromdate);

                        date = new Date(todate);
                        dd = ("0" + date.getDate()).slice(-2);
                        mm = ("0" + (date.getMonth() + 1)).slice(-2);
                        yyyy = date.getFullYear();

                        GM.Global.Surgeon.Filter["acttodate"] = getDateByFormat(todate);
                        GM.Global.Surgeon.FilterName["To Date"] = getDateByFormat(todate);
                    }
                },

                // Lookup Surgeons by Filters
                searchCategory: function (event) {
                    var that = this;

                    if (typeof (GM.Global.Surgeon.FilterID) != "object")
                        GM.Global.Surgeon.FilterID = $.parseJSON(GM.Global.Surgeon.FilterID);
                    if (typeof (GM.Global.Surgeon.FilterName) != "object")
                        GM.Global.Surgeon.FilterName = $.parseJSON(GM.Global.Surgeon.FilterName);
                    if (GM.Global.Surgeon == undefined)
                        GM.Global.Surgeon = {
                            "Filter": ""
                        };
                    GM.Global.Surgeon["Filter"] = GM.Global.Surgeon.FilterID;
                    GM.Global.Surgeon["FilterName"] = GM.Global.Surgeon.FilterName;
                    if ($("#surgeon-frmdate").val() != "" && $("#surgeon-frmdate").val() != undefined)
                        this.activityDate();

                    if (GM.Global.Surgeon.FilterID != null || GM.Global.Surgeon.FilterName != null) {
                        window.location.href = '#crm/surgeon/list/0';
                    }

                },

                // Delete the favorite item from the criteria page
                removeFavorite: function (e) {
                    showNativeConfirm("Do you want to delete this favorite", "Confirm", ["Yes", "No"], function (index) {
                        if (index == 1) {
                            var input = {};
                            input.token = localStorage.getItem('token');
                            input.criteriaid = $(e.currentTarget).attr('criteriaid');
                            input.criterianm = $(e.currentTarget).parent().find('a').text();
                            input.criteriatyp = "surgeon";
                            input.voidfl = "Y";
                            input.listGmCriteriaAttrVO = [];
                            fnGetWebServerData("criteria/save", undefined, input, function (data) {
                                if (data != null) {
                                    $(e.currentTarget).parent().parent().remove();
                                }
                            });
                        }
                    });

                },

                toggleTabs: function (event) {
                    colorControl(event, "sub-tab", "gmBGEmpty", "gmBGDarkerGrey gmFontWhite");
                    var target = $(event.currentTarget).attr("id");

                    if (target == "tab-npiname") {
                        $(".surgeon-search-content").removeClass("gmMediaDisplayNone");
                        $(".surgeon-category-content").addClass("gmMediaDisplayNone");
                        $(".surgeon-favorite-criteria").addClass("gmMediaDisplayNone");
                    } else if (target == "tab-category") {
                        $(".surgeon-category-content").removeClass("gmMediaDisplayNone");
                        $(".surgeon-favorite-criteria").addClass("gmMediaDisplayNone");
                        $(".surgeon-search-content").addClass("gmMediaDisplayNone");
                    } else if (target == "tab-favorite") {
                        $(".surgeon-favorite-criteria").removeClass("gmMediaDisplayNone");
                        $(".surgeon-search-content").addClass("gmMediaDisplayNone");
                        $(".surgeon-category-content").addClass("gmMediaDisplayNone");
                    } else {
                        $(".surgeon-search-content").removeClass("gmMediaDisplayNone");
                    }
                },

                createSurgeon: function () {
                    window.location.href = "#crm/surgeon/id/0"
                },

                toggleSelectedItems: function (event) {
                    if ($("#criteria-selected-itmes").hasClass("gmMediaDisplayNone")) {
                        $("#criteria-selected-itmes").removeClass("gmMediaDisplayNone");
                        $("#selected-criteria-icon").find(".fa-chevron-down").addClass("fa-chevron-up").removeClass("fa-chevron-down");
                    } else {
                        $("#criteria-selected-itmes").addClass("gmMediaDisplayNone");
                        $("#selected-criteria-icon").find(".fa-chevron-up").addClass("fa-chevron-down").removeClass("fa-chevron-up");
                    }
                },

                // Load Surgoen Report Page
                surgeonReport: function (e) {
                    var that = this;
                    var currName = $(e.currentTarget).attr("name");
                    window.location.href = "#crm/surgeon/filter/report/" + currName;
                },
                
                 // Load Sales rep on search in the search field
                searchRep: function () {
                    var userData = localStorage.getItem("userData");
                    userData = JSON.parse(userData);
                    var companyid = userData.cmpid;
                    var searchOptions = {
                        'el': $("#div-surgeon-search-rep"),
                        'url': URL_WSServer + 'cmsearch/party',
                        'inputParameters': {
                        'token': localStorage.getItem('token'),
                        'userid': localStorage.getItem('userID'),
                        'cmpid': companyid
                    },
                    'placeholder': 'Key in at least 3 characters to search',
                    'keyParameter': 'srchkey',
                    'IDParameter': 'ID',
                    'NameParameter': 'NAME',
                    'resultVO': undefined,
                        'minChar': 3,
                        'callback': function (result) {
                            $("#div-surgeon-search-rep input").val(result.Name);
                            $("#div-surgeon-search-rep").attr('data-repid', result.ID);
                        }
                    };
                    this.searchView = new GMVSearch(searchOptions);
                },

                // Lookup Surgeons by providing Sales rep id
                lookupBySRep: function (event) {
                    var repid = $(event.currentTarget).parent().find("#div-surgeon-search-rep").data('repid');
                    var repnm = $(event.currentTarget).parent().find("#div-surgeon-search-rep input").val();
                    GM.Global.Surgeon = {
                        repid: repid,
                        repnm: repnm
                    };
                    repid = (repid !== "") ? repid : "undefined";
                    if (repid != undefined)
                        window.location.href = "#crm/surgeon/repid/" + repid;
                },

                //Handles the orietaion change in the iPad
                onOrientationChange: function (event) {
                    resizeHeadWidth('.gmPanelHead', '.gmBtnGroup', '#panelhead-left');
                },
            });
            return GM.View.SurgeonCriteria;
        });
