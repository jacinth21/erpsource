REM +--------------------------------------------------------------------------+
REM | VALIDATE COMMAND-LINE PARAMETERS                                         |
REM +--------------------------------------------------------------------------+

if (%1)==() goto USAGE
if (%2)==() goto USAGE
if (%3)==() goto USAGE
if (%4)==() goto USAGE
if (%5)==() goto USAGE
if (%6)==() goto USAGE

REM +--------------------------------------------------------------------------+
REM | DECLARE ALL VARIABLES.                                            |
REM +--------------------------------------------------------------------------+

FOR /F "TOKENS=1* DELIMS= " %%A IN ('DATE/T') DO SET CDATE=%%B
FOR /F "TOKENS=1,2 eol=/ DELIMS=/ " %%A IN ('DATE/T') DO SET mm=%%B
FOR /F "TOKENS=1,2 DELIMS=/ eol=/" %%A IN ('echo %CDATE%') DO SET dd=%%B
FOR /F "TOKENS=2,3 DELIMS=/ " %%A IN ('echo %CDATE%') DO SET yyyy=%%B
set DB_USERNAME=%1%
set DB_PASSWORD=%2%
set TNS_ALIAS=%3%
set ORABACKUP=%4
set exportuser=%5

set BACKUPROOT=D:\Oracle_Databackup
set ORALOG=%BACKUPROOT%\%yyyy%%mm%\LOG
set ORABACKUP=%BACKUPROOT%\%yyyy%%mm%
set FILENAME=%yyyy%%mm%%dd%_%TNS_ALIAS%_%exportuser%
set PARFILE=%BACKUPROOT%\%FILENAME%.parfile
set LOGFILE=%ORALOG%\%FILENAME%.log
set BACKUPDIR=\\gmiasrv04\File_Server\IT\Database\backup
set BACKUPFILENAME=oracle_backup.zip
set connection=%DB_USERNAME%/%DB_PASSWORD%@%TNS_ALIAS%


REM +--------------------------------------------------------------------------+
REM | CREATE DIRECTORIES IF THEY ARE NOT PRESENT                               |
REM +--------------------------------------------------------------------------+

if not exist %ORALOG% ( 
	echo creating folder %ORALOG%
	md %ORALOG%
	)

REM +--------------------------------------------------------------------------+
REM | SETTING EMAIL PARAMETERS			                               |
REM +--------------------------------------------------------------------------+

FOR /F "TOKENS=1* DELIMS= " %%A IN ('DATE/T') DO SET CDATE=%%B
FOR /F "TOKENS=1,2 eol=/ DELIMS=/ " %%A IN ('DATE/T') DO SET mm=%%B
FOR /F "TOKENS=1,2 DELIMS=/ eol=/" %%A IN ('echo %CDATE%') DO SET dd=%%B
FOR /F "TOKENS=2,3 DELIMS=/ " %%A IN ('echo %CDATE%') DO SET yyyy=%%B
SET date_formatted=%mm%_%dd%_%yyyy% 

set eMail=%6%
set fromId="notification@globusmedical.com"
set subj=-s "Export_on"_%date_formatted%
set server=-server 192.168.1.45
set debug=-debug -log %ORALOG%\export_email.log -timestamp 
set body="Export successful"

REM +--------------------------------------------------------------------------+
REM | REMOVE OLD LOG AND PARAMETER FILE(S).                                    |
REM +--------------------------------------------------------------------------+

del /q %PARFILE%
del /q %LOGFILE%
del /q %BACKUPROOT%

REM +--------------------------------------------------------------------------+
REM | WRITE EXPORT PARAMETER FILE.                                             |
REM +--------------------------------------------------------------------------+

echo userid="%connection%" > %PARFILE%
echo DUMPFILE="%FILENAME%.dmp"  >> %PARFILE%
echo LOGFILE="%FILENAME%.log" >> %PARFILE%
echo DIRECTORY=DM_EXPORT_DIR >> %PARFILE%
echo SCHEMAS=%exportuser% >> %PARFILE%
echo CONTENT=ALL >> %PARFILE%
echo STATUS=0 >> %PARFILE%
echo VERSION=10.1.0.2.0 >> %PARFILE%


REM +--------------------------------------------------------------------------+
REM | PERFORM EXPORT.                                                          |
REM +--------------------------------------------------------------------------+

expdp parfile=%PARFILE%

REM +--------------------------------------------------------------------------+
REM | COPY LOG FILE 							       |
REM +--------------------------------------------------------------------------+
copy %BACKUPROOT%\%FILENAME%.log %ORALOG%

REM +--------------------------------------------------------------------------+
REM | PERFORM COMPRESSION.                                                     |
REM +--------------------------------------------------------------------------+
set zipfilename=%FILENAME%.zip
cd %BACKUPROOT%
rar a %ORABACKUP%\%zipfilename% %FILENAME%.dmp 

REM +--------------------------------------------------------------------------+
REM | MOVE EXPORT (DUMP) FILE TO BACKUP DIRECTORY                              |
REM +--------------------------------------------------------------------------+

copy %ORABACKUP%\%zipfilename% %BACKUPDIR%\%BACKUPFILENAME% /Y
if not errorlevel 1 goto COPYSUCCESS
	set copystatus= ERROR While Copying to backup IT drive %BACKUPDIR%\%BACKUPFILENAME%
	goto ANALYZE
		
:COPYSUCCESS
	set copystatus= Successfully copied to backup IT drive %BACKUPDIR%\%BACKUPFILENAME%
	goto ANALYZE

REM +--------------------------------------------------------------------------+
REM | SCAN THE EXPORT LOGFILE FOR ERRORS.                                      |
REM +--------------------------------------------------------------------------+

:ANALYZE
echo ...
echo Analyzing log file for EXP- errors...
findstr /I /C:"EXP-" %LOGFILE%
if errorlevel 0 if not errorlevel 1 echo EXP- Errors:  %FILENAME% %TNS_ALIAS% %COMPUTERNAME% %DATE% %TIME% %LOGFILE%

echo ...
echo Analyzing log file for ORA- errors...
findstr /I /C:"ORA-" %LOGFILE%
if errorlevel 0 if not errorlevel 1 echo ORA- Errors:  %FILENAME% %TNS_ALIAS% %COMPUTERNAME% %DATE% %TIME% %LOGFILE%

echo ...
echo Analyzing log file for warnings...
findstr /I /C:"Export terminated successfully with warnings" %LOGFILE%
if errorlevel 0 if not errorlevel 1 echo WARNING: %FILENAME% %TNS_ALIAS% %COMPUTERNAME% %DATE% %TIME% %LOGFILE%

echo ...
echo Analyzing log file for errors...
findstr /I /C:"successfully completed" %LOGFILE%
if errorlevel 1 goto UNSUCCESSERROR
if errorlevel 0 goto SUCCESS

:EOFREPORT
echo ...
echo END OF FILE REPORT
echo Filename      : %FILENAME%
echo Database      : %TNS_ALIAS%
echo Hostname      : %COMPUTERNAME%
echo Date          : %DATE%
echo Time          : %TIME%
echo EXP Log File  : %LOGFILE%
GOTO END

:USAGE
echo Usage: "Export_User_Backup <User> <Password> <TNS_ALIAS> <Destination Folder> <User to export> <Comma Separated Email Ids for Notification>"
GOTO END

:SUCCESS

echo SUCCESS: %FILENAME% %TNS_ALIAS% %COMPUTERNAME% %DATE% %TIME% %LOGFILE%
set body="Export successful Export file name - %FILENAME% located at %ORABACKUP% during %TIME% %copystatus%"
set subj=-s "Export successful on "%date_formatted%
blat -body %body%  -to %eMail% -f %fromId% %subj% %server% %debug%
cd %BACKUPDIR%
GOTO EOFREPORT

:UNSUCCESSERROR 
set body="Export UNsuccessful at time "%TIME% 
set subj=-s "Export UNsuccessful on "%date_formatted%
echo ERROR: %FILENAME% %TNS_ALIAS% %COMPUTERNAME% %DATE% %TIME% %LOGFILE%
blat -body %body%  -to %eMail% -f %fromId% %subj% %server% %debug%
GOTO EOFREPORT

:END
