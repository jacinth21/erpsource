REM +--------------------------------------------------------------------------+
REM | VALIDATE COMMAND-LINE PARAMETERS                                         |
REM +--------------------------------------------------------------------------+

if (%1)==() goto USAGE
if (%2)==() goto USAGE
if (%3)==() goto USAGE
if (%4)==() goto USAGE
if (%5)==() goto USAGE
if (%6)==() goto USAGE
if (%7)==() goto USAGE
if (%8)==() goto USAGE



REM +--------------------------------------------------------------------------+
REM | DECLARE ALL VARIABLES.                                            |
REM +--------------------------------------------------------------------------+


set SYS_USER_NAME=%1%
set SYS_USER_PWD=%2%
set DEST_DB=%3%
set SOURCE_SYS_USER_NAME=%4
set DEST_SYS_USER_NAME=%5
set DEST_DB_USER_PWD=%6
set yyyymm=%7
set dd=%8
set eMail="psrinivasan@globusmedical.com,richardk@globusmedical.com,sshyam@globusmedical.com,djames@globusmedical.com,rajeshwaran@globusmedical.com,djayaraj@globusmedical.com,gvaithiyanathan@globusmedical.com,ykannan@globusmedical.com"
set esms="6129634488@txt.att.net,4847444410@txt.att.net,2674383912@txt.att.net,4846825889@txt.att.net"

echo %DEST_DB%
echo %DEST_SYS_USER_NAME%
echo %DEST_DB_USER_PWD%


SHIFT
SHIFT
SHIFT
SHIFT
SHIFT
SHIFT
SHIFT
SHIFT
SHIFT


REM +--------------------------------------------------------------------------+
REM | SETTING EMAIL PARAMETERS			                               |
REM +--------------------------------------------------------------------------+

set fromId="notification@globusmedical.com"
set subj=-s "Import_on"_%date_formatted%
set server=-server 192.168.1.124
set debug=-debug -log export_email.log -timestamp 
set body="Import successful"

REM - check if we want to perform import for today
IF FILENAME == "today" set FILENAME=exp_%exportuser%_GMIPROD_%date:~4,2%-%date:~7,2%-%date:~10%.dmp

set BACKUPROOT=D:\DataBackup
REM set ORACLE_DMP_ROOT=D:\DataBackup
set LOG_DIR=%BACKUPROOT%\%yyyymm%\LOG
REM set SOURCE_DB_FILE_DIR=\\gmiep\Oracle_Databackup
set SOURCE_DB_FILE_DIR=D:\dmpshare


set FILENAME=%yyyymm%%dd%_GMIPROD_US_%SOURCE_SYS_USER_NAME%
set ZIP_FILENAME=%FILENAME%.zip
set LOGFILE=%LOG_DIR%\%FILENAME%.log
set PARFILE=%LOG_DIR%\%FILENAME%.parfile

set connection=%SYS_USER_NAME%/%SYS_USER_PWD%@%DEST_DB% as sysdba

echo %PARFILE%
echo %FILENAME%

REM +--------------------------------------------------------------------------+
REM | CREATE DIRECTORIES IF THEY ARE NOT PRESENT                               |
REM +--------------------------------------------------------------------------+

if not exist %LOG_DIR% ( 
	echo creating folder %LOG_DIR%
	md %LOG_DIR%
	)

REM +--------------------------------------------------------------------------+
REM | Check if the Dmp file is available. If not available, do not proceed further.
REM | This will avoid the BO Load Failure.
REM +--------------------------------------------------------------------------+

 if not exist %SOURCE_DB_FILE_DIR%\%FILENAME%.dmp (

    echo file Not exists %SOURCE_DB_FILE_DIR%\%FILENAME%.dmp
	GOTO FAILDMPFILE

) 
	
REM +--------------------------------------------------------------------------+
REM | REMOVE OLD LOG AND PARAMETER FILE(S).                                    |
REM +--------------------------------------------------------------------------+

del D:\Database\DW_BI\dbscripts\spool\drop.sql

del /q %PARFILE%
del /q %LOGFILE%


REM +--------------------------------------------------------------------------+
REM | WRITE EXPORT PARAMETER FILE.                                             |
REM +--------------------------------------------------------------------------+

echo USERID="%connection%" > %PARFILE%
echo DUMPFILE=%FILENAME% >> %PARFILE%
echo LOGFILE=%FILENAME%.log >> %PARFILE%
echo DIRECTORY=DUMP_IMP_DIRECTORY >> %PARFILE%
echo SCHEMAS=%SOURCE_SYS_USER_NAME% >> %PARFILE%
echo REMAP_SCHEMA=%SOURCE_SYS_USER_NAME%:%DEST_SYS_USER_NAME% >> %PARFILE%
echo REMAP_TABLESPACE=GLOBUS_PROD_PERM:GLOBUS_DWSTG_PERM >> %PARFILE%
echo CONTENT=ALL >> %PARFILE%
echo TABLE_EXISTS_ACTION=REPLACE >> %PARFILE%
REM echo EXCLUDE=GRANT,REFRESH_GROUP,JOB,DOMAIN_INDEX,ROLE_GRANT,USER,CONSTRAINT,INDEX_STATISTICS >> %PARFILE%
echo EXCLUDE=GRANT,JOB,ROLE_GRANT,USER,CONSTRAINT >> %PARFILE%
echo EXCLUDE=TABLE:"IN ('T941_AUDIT_TRAIL_LOG','T730T_VIRTUAL_CONSIGNMENT','T251_INVENTORY_LOCK_DETAIL', 'T4042_DEMAND_SHEET_DETAIL',  'T4043_DEMAND_SHEET_GROWTH', 'T4042_DEMAND_SHEET_DETAIL_BACK', 'T705C_PRICE_INCREASE', 'T731B_VIRTUAL_CONSIGN_ITEM', 'T253B_ITEM_CONSIGNMENT_LOCK', 'T251_INV_LOCK_DETAIL_BACK','T5011_TAG_LOG', 'T902_LOG','T907_CANCEL_LOG','T905_STATUS_DETAILS','T810_INVENTORY_TXN','T810_CONSIGNMENT_TXN','T810_OTHER_TXN','T501B_ORDER_BY_SYSTEM_LOG','T2551_PART_CONTROL_NUMBER_LOG','T821_COSTING_TRACK_LOG','T5055A_CONTROL_NUMBER_LOG','T253D_REQUEST_DETAIL_LOCK','T502A_ITEM_ORDER_LOG','T4043_DEMAND_SHEET_GROWTH_BACK','T810_AP_TXN','TEMPA_PATIENT_ANSWER_HISTORY','TEMP_PATIENT_ANSWER_LIST','TEMP_T705_ACC_PRICING_DEL_ROWS','T733_AI_QUOTA_LOG','T253B_ITEM_CONS_LOCK_BACK','T253D_REQUEST_DETAIL_LOCK_BACK','T253C_REQUEST_LOCK_BACK','T4053_TTP_PART_DETAIL_BACK','T4044_DS_REQUEST_BACK','T253A_CONSIGNMENT_LOCK_BACK','T4041_DS_MAPPING_BACK','T907_SHIPPING_INFO','T820_COSTING_ARCHIVE','T501B_ORDER_BY_SYSTEM','T820_COSTING','T2052_PART_PRICE_MAPPING','T731_VIRTUAL_CONSIGN_ITEM','T504A_LOANER_TRANSACTION','T2060_DI_PART_MAPPING','T704A_ACCOUNT_ATTRIBUTE','T709_QUOTA_BREAKUP','T705_ACCOUNT_PRICING','T810_POSTING_TXN_ARCHIVE','T810_POSTING_TXN','T5054_INV_LOCATION_LOG','T4060_PART_FORECAST_QTY','T5053_LOCATION_PART_MAPPING')" >> %PARFILE%
echo STATUS=0 >> %PARFILE%


REM +--------------------------------------------------------------------------+
REM | CLEAN old copy and COPY new DB DUMP FROM PRODDB                                                   |
REM +--------------------------------------------------------------------------+

del /q %ORACLE_DMP_ROOT%
del /q %BACKUPROOT%

REM copy %SOURCE_DB_FILE_DIR%\%yyyymm%\%ZIP_FILENAME% %BACKUPROOT%

move %SOURCE_DB_FILE_DIR%\%FILENAME%.dmp %BACKUPROOT%


REM This Script was added on 03/08/17,to troubleshoot BO Import Failures.
REM timeout changed to 900 secs (15mins) on 3/14/17.
REM Timeout commented on 4/20/17.

REM TIMEOUT 900

REM +--------------------------------------------------------------------------+
REM | To check space more than 15GB               |
REM +--------------------------------------------------------------------------+
Echo Drive   FreeSpace MB
Echo -----   ------------
for /f "usebackq delims== tokens=2" %%x in (`wmic logicaldisk where "DeviceID='D:'" get FreeSpace /format:value`) do set FreeSpace=%%x
IF !FreeSpace! GTR 16522202496  (GOTO FAILSPACEBACKUP)

REM +--------------------------------------------------------------------------+
REM | PERFORM UNCOMPRESSION. It'll automatically go into D:\DataBackup\Oracle_Databackup               |
REM +--------------------------------------------------------------------------+
 
REM unrar x %BACKUPROOT%/%ZIP_FILENAME% %BACKUPROOT%

REM +--------------------------------------------------------------------------+
REM | File exist check, if not then send failure              |
REM +--------------------------------------------------------------------------+
REM if not exist %BACKUPROOT%/%ZIP_FILENAME% GOTO FAILSPACEBACKUP

REM +--------------------------------------------------------------------------+
REM | PERFORM IMPORT.							       |
REM +--------------------------------------------------------------------------+


sqlplus %DEST_SYS_USER_NAME%/%DEST_DB_USER_PWD%@%DEST_DB% @"D:\Database\DW_BI\dbscripts\preimport_%DEST_SYS_USER_NAME%.sql" torem
impdp parfile=%PARFILE%

REM +--------------------------------------------------------------------------+
REM | POST IMPORT LOAD							       |
REM +--------------------------------------------------------------------------+
copy %BACKUPROOT%\%FILENAME%.log %LOG_DIR%

sqlplus %DEST_SYS_USER_NAME%/%DEST_DB_USER_PWD%@%DEST_DB% @"D:\Database\DW_BI\scripts\load\daily_post_globus_app_load.sql" 
REM sqlplus globus_dw/dw303sdw@%DEST_DB% @"D:\Database\DW_BI\scripts\load\procedure_and_static_data_Load.sql" old
sqlplus globus_dw/dw303sdw@%DEST_DB% @"D:\Database\DW_BI\scripts\load\Daily_load.sql" 
REM sqlplus globus_bi_realtime/bir303sbir@%DEST_DB% @"D:\Database\DW_BI\scripts\load\clinical_load.sql" old

REM +--------------------------------------------------------------------------+
REM | SCAN THE EXPORT LOGFILE FOR ERRORS.                                      |
REM +--------------------------------------------------------------------------+

echo ...
echo Analyzing log file for EXP- errors...
findstr /I /C:"EXP-" %LOGFILE%
if errorlevel 0 if not errorlevel 1 echo EXP- Errors:  %FILENAME% %DEST_DB% %COMPUTERNAME% %DATE% %TIME% %LOGFILE%

echo ...
echo Analyzing log file for ORA- errors...
findstr /I /C:"ORA-" %LOGFILE%
if errorlevel 0 if not errorlevel 1 echo ORA- Errors:  %FILENAME% %DEST_DB% %COMPUTERNAME% %DATE% %TIME% %LOGFILE%

echo ...
echo Analyzing log file for warnings...
findstr /I /C:"Import terminated successfully with warnings" %LOGFILE%
if errorlevel 0 if not errorlevel 1 echo WARNING: %FILENAME% %DEST_DB% %COMPUTERNAME% %DATE% %TIME% %LOGFILE%

echo ...
echo Analyzing log file for errors...
findstr /I /C:"Import terminated successfully" %LOGFILE%
if errorlevel 1 goto UNSUCCESSERROR
if errorlevel 0 goto SUCCESS

:CLEANUP
copy %BACKUPROOT%\%ZIP_FILENAME% %BACKUPROOT%\%yyyymm%
del /q %BACKUPROOT%
GOTO EOFREPORT

:EOFREPORT
echo ...
echo END OF FILE REPORT
echo Filename      : %FILENAME%
echo Database      : %DEST_DB%
echo Hostname      : %COMPUTERNAME%
echo Date          : %DATE%
echo Time          : %TIME%
echo EXP Log File  : %LOGFILE%
GOTO END

:USAGE
echo Usage: "Import_User_Backup <User> <Password> <DEST_DB> <Source Folder> <Source_File> <From User> <To User> <To User password> <Comma Separated Email Ids for Notification> <Date For Import MM/DD/YYYY>"

:SUCCESS
echo SUCCESS: %FILENAME% %DEST_DB% %COMPUTERNAME% 
rem %DATE% %TIME% %LOGFILE%
set body="Import successful from file - %FILENAME% located at %BACKUPROOT% during %TIME% from User %SOURCE_SYS_USER_NAME% To %DEST_SYS_USER_NAME%"
set subj=-s "Imported successful into %DEST_DB%/%DEST_SYS_USER_NAME% on %dd% %mm% %yyyy%"
blat -body %body%  -to %eMail% -f %fromId% %subj% %server% %debug% -attacht %LOGFILE%
GOTO CLEANUP

:UNSUCCESSERROR 
set body="Import UNsuccessful at time "%TIME%
set subj=-s "Import UNsuccessful into %DEST_DB%/%DEST_SYS_USER_NAME% on %dd% %mm% %yyyy%"
echo ERROR: %FILENAME% %DEST_DB% %COMPUTERNAME% %DATE% %TIME% %LOGFILE%
blat -body %body%  -to %eMail% -f %fromId% %subj% %server% %debug% -attacht %LOGFILE%
GOTO CLEANUP

:FAILSPACEBACKUP
set body="Import UNsuccessful due to space or backupfailure"
set subj=-s "Import UNsuccessful into %DEST_DB%/%DEST_SYS_USER_NAME% on %dd% %mm% %yyyy%"
rem echo ERROR: %FILENAME% %DEST_DB% %COMPUTERNAME% %DATE% %TIME% %LOGFILE%
blat -body %body%  -to %esms% -f %fromId% %subj% 
rem %server% %debug% -attacht %LOGFILE%
GOTO END

:FAILDMPFILE
set body="**Import UNsuccessful due to missing Dump File**"-%FILENAME%
set subj=-s "Missing DMP File -Import UNsuccessful into %DEST_DB%/%DEST_SYS_USER_NAME% on %dd% %mm% %yyyy%"
rem echo ERROR: %FILENAME% %DEST_DB% %COMPUTERNAME% %DATE% %TIME% %LOGFILE%
blat -body %body%  -to %esms% -f %fromId% %subj% 
rem %server% %debug% -attacht %LOGFILE%
GOTO END

:END