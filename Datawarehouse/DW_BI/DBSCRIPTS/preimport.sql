spool D:/Database/DW_BI/dbscripts/spool/drop.sql
set head off feed off verify off
set linesize 5000;

select 'drop view GLOBUS_DW_STG.'||object_name|| ';' from all_objects where owner='GLOBUS_DW_STG' and object_type = 'VIEW';

select 'drop MATERIALIZED view GLOBUS_DW_STG.'||object_name|| ';' from all_objects where owner='GLOBUS_DW_STG' and object_type = 'MATERIALIZED VIEW' and object_name like '%FACT%';

select 'drop MATERIALIZED view GLOBUS_DW_STG.'||object_name|| ';' from all_objects where owner='GLOBUS_DW_STG' and object_type = 'MATERIALIZED VIEW';

select 'drop FUNCTION GLOBUS_DW_STG.'||object_name|| ';' from all_objects where owner='GLOBUS_DW_STG' and object_type = 'FUNCTION';
select 'drop PACKAGE GLOBUS_DW_STG.'||object_name|| ';' from all_objects where owner='GLOBUS_DW_STG' and object_type = 'PACKAGE';
select 'drop PACKAGE BODY GLOBUS_DW_STG.'||object_name|| ';' from all_objects where owner='GLOBUS_DW_STG' and object_type = 'PACKAGE BODY';
select 'drop PROCEDURE GLOBUS_DW_STG.'||object_name|| ';' from all_objects where owner='GLOBUS_DW_STG' and object_type = 'PROCEDURE';

select 'drop TABLE GLOBUS_DW_STG.'||object_name ||' CASCADE CONSTRAINTS PURGE;' from all_objects where owner='GLOBUS_DW_STG' and object_type = 'TABLE';

select 'drop SEQUENCE GLOBUS_DW_STG.'||object_name|| ';' from all_objects where owner='GLOBUS_DW_STG' and object_type = 'SEQUENCE';

spool off;
@"D:/Database/DW_BI/dbscripts/spool/drop.sql";
commit;
exit;