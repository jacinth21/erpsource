FOR /F "TOKENS=1* DELIMS= " %%A IN ('DATE/T') DO SET CDATE=%%B
FOR /F "TOKENS=1,2 eol=/ DELIMS=/ " %%A IN ('DATE/T') DO SET mm=%%B
FOR /F "TOKENS=1,2 DELIMS=/ eol=/" %%A IN ('echo %CDATE%') DO SET dd=%%B
FOR /F "TOKENS=2,3 DELIMS=/ " %%A IN ('echo %CDATE%') DO SET yyyy=%%B

d:
cd D:\database\DW_BI\dbscripts

REM IF NOT EXIST \\gmiasrv04\File_Server\IT\Database\backup\export.lock GOTO ERRORL
goto run

:ERRORL
REM +--------------------------------------------------------------------------+
REM | SETTING EMAIL PARAMETERS			                               |
REM +--------------------------------------------------------------------------+

set fromId="#IT-Helpdesk@gminc.local"
set subj=-s "Import_on"_%yyyy%%mm%%dd%
set server=-server 192.168.1.45
set debug=-debug -log export_email.log -timestamp 
set body="Export not finished before import"
set eMail="jkumar@globusmedical.com"

blat -body %body%  -to %eMail% -f %fromId% %subj% %server% %debug%
GOTO END

:RUN
REM set default values
set YEAR_MONTH=%yyyy%%mm%
set DAY=%dd%
REM set DAY=22

REM set parameter override
IF NOT "a%1"=="a" (set YEAR_MONTH=%1)
IF NOT "a%2"=="a" (set DAY=%2)
echo %YEAR_MONTH

set TNS_ALIAS=GMIPRODN
set SYS_USER_NAME=SYS
set SYS_USER_PWD=
set SOURCE_DB_USERNAME=globus_app
set DEST_DB_USERNAME=GLOBUS_DW
set DEST_DB_USER_PWD=
set DEST_DB=GMIDWPROD
set DEST_TABLESPACE=GLOBUS_DW_PERM

set path=D:\oracle\product\10.2.0\db_1\BIN\;%PATH%;

Import_User_Backup %SYS_USER_NAME% %SYS_USER_PWD% %DEST_DB% %SOURCE_DB_USERNAME% %DEST_DB_USERNAME% %DEST_DB_USER_PWD% %YEAR_MONTH% %DAY% %DEST_TABLESPACE%

REM del \\gmiasrv04\File_Server\IT\Database\backup\export.lock
GOTO END

:END