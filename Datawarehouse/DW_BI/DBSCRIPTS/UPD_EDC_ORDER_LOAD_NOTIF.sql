-- Updated some name in T906 table for  EDC ORDER LOAD Job 

UPDATE T906_RULES SET c906_rule_value='spineithelp@globusmedical.com,psrinivasan@globusmedical.com,kramasamy@globusmedical.com,skumarasamy@globusmedical.com',c906_last_updated_by='839135',c906_last_updated_date=SYSDATE 
WHERE C906_RULE_ID ='ENT_EDC_ETL_FAILURE' AND c906_rule_grp_id = 'DATA_LOAD_NOTIF';

UPDATE T906_RULES SET c906_rule_value='skumarasamy@globusmedical.com,psrinivasan@globusmedical.com,kramasamy@globusmedical.com',c906_last_updated_by='839135',c906_last_updated_date=SYSDATE 
WHERE C906_RULE_ID ='ENT_EDC_ETL_SUCCESS' AND c906_rule_grp_id = 'DATA_LOAD_NOTIF';

UPDATE T906_RULES SET c906_rule_value='aesser@globusmedical.com,psrinivasan@globusmedical.com,kramasamy@globusmedical.com',c906_last_updated_by='839135',c906_last_updated_date=SYSDATE 
WHERE C906_RULE_ID ='ENT_EDC_ORDER_ALTER' AND c906_rule_grp_id = 'DATA_LOAD_NOTIF';