SELECT prim_key
    , study_id
    , form_name
    , xml_tag
    , translate(answer,'X' || CHR (9) || CHR (10) || CHR (13),'X')  answer
    ,NVL(question_seqno,100000) + 10|| trim(to_char( (answer_seqno+ 10)/100000, '999.99999')) || trim(to_char((v600.answer_grp_id / 100000), '999.99999'))  seqno
    , question_seqno
    , answer_seqno answer_seqno
    , v600.answer_grp_id  answer_grp_id
FROM globus_app.v600_xml_data v600
where STUDY_LIST_ID = '{?Study List Id}'
UNION ALL
SELECT prim_key
    , study_id
    , form_name
    , other_xml_tag
    , translate(other_answer,'X' || CHR (9) || CHR (10) || CHR (13),'X') other_answer
  , NVL(question_seqno,100000) + 10|| trim(to_char( (answer_seqno+ 10)/100000, '.99999')) || trim(to_char((v600.answer_grp_id / 100000), '.99999'))  || trim(to_char((v600.answer_list_id / 100000), '.99999')) seqno
    , question_seqno
    ,  (answer_seqno+ 1) answer_seqno
    , v600.answer_grp_id  answer_grp_id
FROM globus_app.v600_xml_data v600
where STUDY_LIST_ID = '{?Study List Id}'
AND other_xml_tag IS NOT NULL
UNION ALL
SELECT prim_key
     , study_id
    , form_name
    , 'PATIENT' xml_tag
    , ide answer
    , '01' seqno
    , 0 question_seqno
    , 1 answer_seqno
    , 1  answer_grp_id
FROM globus_app.v600_xml_data v600
where STUDY_LIST_ID =  '{?Study List Id}'
UNION ALL
SELECT prim_key
     , study_id
    , form_name
    , 'TRTMT' xml_tag
    , TRTMT answer
    , '02' seqno
    , 0 question_seqno
    , 2 answer_seqno
    , 2  answer_grp_id
FROM globus_app.v600_xml_data v600
where STUDY_LIST_ID =  '{?Study List Id}'
UNION ALL
SELECT prim_key
     , study_id
    , form_name
    , 'VSDT' xml_tag
    , SUBSTR(vdate,0,10) answer
    , '03' seqno
    , 0 question_seqno
    , 3 answer_seqno
    , 3 answer_grp_id
FROM globus_app.v600_xml_data v600
where STUDY_LIST_ID = '{?Study List Id}'
UNION ALL
SELECT prim_key
     , study_id
    , form_name
    , 'CRDT' xml_tag
    , SUBSTR(cdate,0,10) answer
    , '04' seqno
    , 0 question_seqno
    , 4 answer_seqno
    , 4 answer_grp_id
FROM globus_app.v600_xml_data v600
where STUDY_LIST_ID = '{?Study List Id}'
UNION ALL
SELECT prim_key
     , study_id
    , form_name
    , 'PERIOD' xml_tag
    , period answer
    , '05' seqno
    , 0 question_seqno
    , 5 answer_seqno
   , 5 answer_grp_id
FROM globus_app.v600_xml_data v600
where STUDY_LIST_ID = '{?Study List Id}'
ORDER BY prim_key,question_seqno, answer_seqno, answer_grp_id