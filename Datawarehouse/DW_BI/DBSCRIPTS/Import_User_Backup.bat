REM +--------------------------------------------------------------------------+
REM | VALIDATE COMMAND-LINE PARAMETERS                                         |
REM +--------------------------------------------------------------------------+

if (%1)==() goto USAGE
if (%2)==() goto USAGE
if (%3)==() goto USAGE
if (%4)==() goto USAGE
if (%5)==() goto USAGE
if (%6)==() goto USAGE
if (%7)==() goto USAGE
if (%8)==() goto USAGE



REM +--------------------------------------------------------------------------+
REM | DECLARE ALL VARIABLES.                                            |
REM +--------------------------------------------------------------------------+


set SYS_USER_NAME=%1%
set SYS_USER_PWD=%2%
set DEST_DB=%3%
set SOURCE_SYS_USER_NAME=%4
set DEST_SYS_USER_NAME=%5
set DEST_DB_USER_PWD=%6
set yyyymm=%7
set dd=%8
set to_tablespace=%9
set eMail="jkumar@globusmedical.com,rvaradarajan@globusmedical.com"

echo %DEST_DB%
echo %DEST_SYS_USER_NAME%
echo %DEST_DB_USER_PWD%


SHIFT
SHIFT
SHIFT
SHIFT
SHIFT
SHIFT
SHIFT
SHIFT
SHIFT


REM +--------------------------------------------------------------------------+
REM | SETTING EMAIL PARAMETERS			                               |
REM +--------------------------------------------------------------------------+

set fromId="notification@globusmedical.com"
set subj=-s "Import_on"_%date_formatted%
set server=-server 192.168.1.45
set debug=-debug -log export_email.log -timestamp 
set body="Import successful"

REM - check if we want to perform import for today
IF FILENAME == "today" set FILENAME=exp_%exportuser%_GMIPROD_%date:~4,2%-%date:~7,2%-%date:~10%.dmp

set BACKUPROOT=D:\DataBackup
set ORACLE_DMP_ROOT=D:\DataBackup
set LOG_DIR=%BACKUPROOT%\%yyyymm%\LOG
set SOURCE_DB_FILE_DIR=\\gmiep\Oracle_Databackup

set FILENAME=%yyyymm%%dd%_GMIPROD_%SOURCE_SYS_USER_NAME%
set ZIP_FILENAME=%FILENAME%.zip
set LOGFILE=%LOG_DIR%\%FILENAME%.log
set PARFILE=%LOG_DIR%\%FILENAME%.parfile

set connection=%SYS_USER_NAME%/%SYS_USER_PWD%@%DEST_DB% as sysdba

echo %PARFILE%
echo %FILENAME%

REM +--------------------------------------------------------------------------+
REM | CREATE DIRECTORIES IF THEY ARE NOT PRESENT                               |
REM +--------------------------------------------------------------------------+

if not exist %LOG_DIR% ( 
	echo creating folder %LOG_DIR%
	md %LOG_DIR%
	)

REM +--------------------------------------------------------------------------+
REM | REMOVE OLD LOG AND PARAMETER FILE(S).                                    |
REM +--------------------------------------------------------------------------+

del D:\Database\DW_BI\dbscripts\spool\drop.sql

del /q %PARFILE%
del /q %LOGFILE%


REM +--------------------------------------------------------------------------+
REM | WRITE EXPORT PARAMETER FILE.                                             |
REM +--------------------------------------------------------------------------+

echo USERID="%connection%" > %PARFILE%
echo DUMPFILE=%FILENAME% >> %PARFILE%
echo LOGFILE=%FILENAME%.log >> %PARFILE%
echo DIRECTORY=DUMP_IMP_DIRECTORY >> %PARFILE%
echo SCHEMAS=%SOURCE_SYS_USER_NAME% >> %PARFILE%
echo REMAP_SCHEMA=%SOURCE_SYS_USER_NAME%:%DEST_SYS_USER_NAME% >> %PARFILE%
echo REMAP_TABLESPACE=GLOBUS_PROD_PERM:%to_tablespace% >> %PARFILE%
echo CONTENT=ALL >> %PARFILE%
echo TABLE_EXISTS_ACTION=REPLACE >> %PARFILE%
echo STATUS=0 >> %PARFILE%


REM +--------------------------------------------------------------------------+
REM | CLEAN old copy and COPY new DB DUMP FROM PRODDB                                                   |
REM +--------------------------------------------------------------------------+

del /q %ORACLE_DMP_ROOT%
copy %SOURCE_DB_FILE_DIR%\%yyyymm%\%ZIP_FILENAME% %BACKUPROOT%

REM +--------------------------------------------------------------------------+
REM | PERFORM UNCOMPRESSION. It'll automatically go into D:\DataBackup\Oracle_Databackup               |
REM +--------------------------------------------------------------------------+

unrar x %BACKUPROOT%/%ZIP_FILENAME% %BACKUPROOT%

REM +--------------------------------------------------------------------------+
REM | PERFORM IMPORT.							       |
REM +--------------------------------------------------------------------------+

sqlplus %DEST_SYS_USER_NAME%/%DEST_DB_USER_PWD%@%DEST_DB% @"D:\Database\DW_BI\dbscripts\preimport.sql"
impdp parfile=%PARFILE%

REM +--------------------------------------------------------------------------+
REM | POST IMPORT LOAD							       |
REM +--------------------------------------------------------------------------+
copy %ORACLE_DMP_ROOT%\%FILENAME%.log %LOG_DIR%

REM +--------------------------------------------------------------------------+
REM | SCAN THE EXPORT LOGFILE FOR ERRORS.                                      |
REM +--------------------------------------------------------------------------+

echo ...
echo Analyzing log file for EXP- errors...
findstr /I /C:"EXP-" %LOGFILE%
if errorlevel 0 if not errorlevel 1 echo EXP- Errors:  %FILENAME% %DEST_DB% %COMPUTERNAME% %DATE% %TIME% %LOGFILE%

echo ...
echo Analyzing log file for ORA- errors...
findstr /I /C:"ORA-" %LOGFILE%
if errorlevel 0 if not errorlevel 1 echo ORA- Errors:  %FILENAME% %DEST_DB% %COMPUTERNAME% %DATE% %TIME% %LOGFILE%

echo ...
echo Analyzing log file for warnings...
findstr /I /C:"Import terminated successfully with warnings" %LOGFILE%
if errorlevel 0 if not errorlevel 1 echo WARNING: %FILENAME% %DEST_DB% %COMPUTERNAME% %DATE% %TIME% %LOGFILE%

echo ...
echo Analyzing log file for errors...
findstr /I /C:"Import terminated successfully" %LOGFILE%
if errorlevel 1 goto UNSUCCESSERROR
if errorlevel 0 goto SUCCESS

:CLEANUP
copy %BACKUPROOT%\%ZIP_FILENAME% %BACKUPROOT%\%yyyymm%
del /q %BACKUPROOT%
GOTO EOFREPORT

:EOFREPORT
echo ...
echo END OF FILE REPORT
echo Filename      : %FILENAME%
echo Database      : %DEST_DB%
echo Hostname      : %COMPUTERNAME%
echo Date          : %DATE%
echo Time          : %TIME%
echo EXP Log File  : %LOGFILE%
GOTO END

:USAGE
echo Usage: "Import_User_Backup <User> <Password> <DEST_DB> <Source Folder> <Source_File> <From User> <To User> <To User password> <Comma Separated Email Ids for Notification> <Date For Import MM/DD/YYYY>"

:SUCCESS
echo SUCCESS: %FILENAME% %DEST_DB% %COMPUTERNAME% %DATE% %TIME% %LOGFILE%
set body="Import successful from file - %FILENAME% located at %BACKUPROOT% during %TIME% from User %SOURCE_SYS_USER_NAME% To %DEST_SYS_USER_NAME%"
set subj=-s "Imported successful into %DEST_SYS_USER_NAME% on %dd% %mm% %yyyy%"
blat -body %body%  -to %eMail% -f %fromId% %subj% %server% %debug% -attacht %LOGFILE%
GOTO CLEANUP

:UNSUCCESSERROR 
set body="Import UNsuccessful at time "%TIME%
set subj=-s "Import UNsuccessful into %DEST_SYS_USER_NAME% on %dd% %mm% %yyyy%"
echo ERROR: %FILENAME% %DEST_DB% %COMPUTERNAME% %DATE% %TIME% %LOGFILE%
blat -body %body%  -to %eMail% -f %fromId% %subj% %server% %debug% -attacht %LOGFILE%
GOTO CLEANUP

:END