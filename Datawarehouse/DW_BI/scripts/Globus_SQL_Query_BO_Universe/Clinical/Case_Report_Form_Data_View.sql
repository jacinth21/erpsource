-- @"C:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Clinical\Case_Report_Form_Data_View.sql";
     

CREATE OR REPLACE VIEW Case_Report_Form_Data_View AS
	SELECT PRIM_KEY,STUDY_ID, STUDYNAME, FORMDS, FORM_NAME,XML_TAG ,ANSWER  ,SEQNO,QUESTION_SEQNO ,ANSWER_SEQNO,ANSWER_GRP_ID  ,STUDY_LIST_ID FROM (
		SELECT prim_key
		    , study_id
		    , get_study_name(study_id) studyname
            , form_name||' - '||get_form_name(FORM_ID) formds 
		    , form_name
		    , xml_tag
		    , translate(answer,'X' || CHR (9) || CHR (10) || CHR (13),'X')  answer
		    ,NVL(question_seqno,100000) + 10|| trim(to_char( (answer_seqno+ 10)/100000, '999.99999')) || trim(to_char((v600.answer_grp_id / 100000), '999.99999'))  seqno
		    , question_seqno
		    , answer_seqno answer_seqno
		    , v600.answer_grp_id  answer_grp_id
            ,STUDY_LIST_ID study_list_id
		FROM globus_app.v600_xml_data v600
		--where STUDY_LIST_ID = '7701'
		UNION ALL
		SELECT prim_key
		    , study_id
		    , get_study_name(study_id) studyname
            , form_name||' - '||get_form_name(FORM_ID) formds 
		    , form_name
		    , other_xml_tag
		    , translate(other_answer,'X' || CHR (9) || CHR (10) || CHR (13),'X') other_answer
		  , NVL(question_seqno,100000) + 10|| trim(to_char( (answer_seqno+ 10)/100000, '.99999')) || trim(to_char((v600.answer_grp_id / 100000), '.99999'))  || trim(to_char((v600.answer_list_id / 100000), '.99999')) seqno
		    , question_seqno
		    ,  (answer_seqno+ 1) answer_seqno
		    , v600.answer_grp_id  answer_grp_id
            ,STUDY_LIST_ID study_list_id
		FROM globus_app.v600_xml_data v600
		where other_xml_tag IS NOT NULL
		--and STUDY_LIST_ID = '7701'
		UNION ALL
		SELECT prim_key
		     , study_id
		     , get_study_name(study_id) studyname
            , form_name||' - '||get_form_name(FORM_ID) formds 
		    , form_name
		    , 'PATIENT' xml_tag
		    , ide answer
		    , '01' seqno
		    , 0 question_seqno
		    , 1 answer_seqno
		    , 1  answer_grp_id
            ,STUDY_LIST_ID study_list_id
		FROM globus_app.v600_xml_data v600
		--where STUDY_LIST_ID = '7701'
		UNION ALL
		SELECT prim_key
		     , study_id
		     , get_study_name(study_id) studyname
            , form_name||' - '||get_form_name(FORM_ID) formds 
		    , form_name
		    , 'VSDT' xml_tag
		    , SUBSTR(vdate,0,10) answer
		    , '02' seqno
		    , 0 question_seqno
		    , 2 answer_seqno
		    , 2 answer_grp_id
            ,STUDY_LIST_ID study_list_id
		FROM globus_app.v600_xml_data v600
		--where STUDY_LIST_ID = '7701'
		UNION ALL
		SELECT prim_key
		     , study_id
		     , get_study_name(study_id) studyname
            , form_name||' - '||get_form_name(FORM_ID) formds 
		    , form_name
		    , 'PERIOD' xml_tag
		    , period answer
		    , '03' seqno
		    , 0 question_seqno
		    , 3 answer_seqno
		   , 3 answer_grp_id
           ,STUDY_LIST_ID study_list_id
		FROM globus_app.v600_xml_data v600
	)
	--where STUDY_LIST_ID = '7701'
	ORDER BY prim_key,question_seqno, answer_seqno, answer_grp_id;
/