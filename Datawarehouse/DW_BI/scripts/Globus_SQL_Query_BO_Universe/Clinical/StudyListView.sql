-- @"C:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Clinical\StudyListView.sql";
     
CREATE OR REPLACE VIEW StudyListView AS
	SELECT  C612_STUDY_LIST_ID studylistid,C601_FORM_ID formid
	,C612_STUDY_FORM_DS ||' - '||get_form_name(C601_FORM_ID) formname,
	C612_STUDY_FORM_DS formdesc,C611_STUDY_ID studyid,c612_seq seq
	,get_study_name(C611_STUDY_ID) studyname
  FROM t612_study_form_list
  ORDER BY C611_STUDY_ID,c612_seq;
  
/