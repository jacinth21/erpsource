select  t502b.C502B_USAGE_LOT_NUM as "control number", t205.c205_part_number_id as "Part Number", t205.c205_part_num_desc as "Part Description"
, get_project_name(t205.c202_project_id) as "Project Name", count(*) as "duplicate count"
from  T502B_ITEM_ORDER_USAGE t502b, t205_part_number t205, t501_order t501
where  t502b.c205_part_number_id = t205.c205_part_number_id
and t502b.c501_order_id = t501.c501_order_id
and t501.c501_void_fl is null
and t502b.c502b_void_fl is null
group by C502B_USAGE_LOT_NUM, t205.c205_part_number_id, t205.c205_part_num_desc, t205.c202_project_id
having count(C502B_USAGE_LOT_NUM) > 1
ORDER BY "Project Name",  "duplicate count" desc;