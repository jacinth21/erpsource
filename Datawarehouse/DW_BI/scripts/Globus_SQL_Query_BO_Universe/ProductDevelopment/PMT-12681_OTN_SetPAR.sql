-- OTN
 select c205_part_number_id part_id,
  c4023_qty one_time_need_qty,
  c4023_last_updated_date updated_date,
  t4020.c4020_demand_nm,
  t901_demand_type.c901_code_nm demand_type ,
  t101.c101_user_sh_name primary_user,
  t901_company_id.c901_code_nm company
from globus_dm_operation.t4023_demand_otn_detail t4023,
  t4020_demand_master t4020,
  t901_code_lookup t901_demand_type,
  t901_code_lookup t901_company_id,
  t101_user t101
where t4020.c4020_demand_master_id = t4023.c4020_demand_master_id
  and t4020.c901_demand_type         = t901_demand_type.c901_code_id (+)
  and t4020.c901_company_id          = t901_company_id.c901_code_id (+)
  and t4020.c4020_primary_user       = t101.c101_user_id
  and t4023.c4023_void_fl           is null
  and t4020.c4020_void_fl           is null
  and t4020.c4020_inactive_fl       is null;

--SetPar
SELECT t4022.c207_set_id set_id,t208.c205_part_number_id part_id,
  t4022.c4022_par_value * t208.c208_set_qty par_value,
  c4022_updated_date updated_date,
  t901_par_type.c901_code_nm par_type
FROM globus_dm_operation.t4022_demand_par_detail t4022,
  t901_code_lookup t901_par_type, t208_set_details t208
WHERE t4022.c901_par_type = t901_par_type.c901_code_id (+)
AND t4022.c207_set_id = t208.c207_set_id
AND t208.c208_set_qty >0
AND t4022.c207_set_id IS NOT NULL;
