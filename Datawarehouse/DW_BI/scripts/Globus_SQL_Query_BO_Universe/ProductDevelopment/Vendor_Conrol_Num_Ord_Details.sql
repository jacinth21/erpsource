select distinct 
sf.do_id as order_id, sf.order_dt as order_date
, pd.part_id as part_number , pd.PART_NUM_DESC  as part_description
, sf.item_qty as item_qty, t502b.c502b_usage_lot_num AS control_number
,  regexp_replace(t502b.c502b_usage_lot_num, '\-') as No_hyp_control_number
, get_set_name(sf.system_id) as system_name
, case when previous_day.cal_date IS NULL
     then 'N' Else 'Y' end as previous_day 
--, sf.item_order_key_id itemkey -- added to eliminate duplicates when there is same part, order in t502a
, sf.order_type as order_type
, sf.order_id as original_order_id
, t502b.c502b_item_qty as control_num_qty
, ac.account_id account_id
, ac.account_nm account_name
, case when nvl(duplicate_control_num.dupl_cnt,0) > 1 then 'Y' else 'N' end as Is_Duplicate
from sales_fact sf, (select * from t502b_item_order_usage where  c502b_void_fl IS NULL) t502b,
 (select max(CAL_DATE) cal_date
    from date_dim
    where BUS_DAY_IND = 'Y'
    and CAL_DATE <= (sysdate-1)) previous_day
, part_dim pd, t906_rules 
, account_dim ac
, (select  regexp_replace(c502b_usage_lot_num, '\-') cnum, count(*) dupl_cnt
from  t502b_item_order_usage t502b, t205_part_number t205, t501_order t501
where  t502b.c205_part_number_id = t205.c205_part_number_id
and t502b.c501_order_id = t501.c501_order_id
and t501.c501_void_fl is null
and t502b.c502b_void_fl is null
group by regexp_replace(c502b_usage_lot_num, '\-')
having count(regexp_replace(c502b_usage_lot_num, '\-')) > 1
) duplicate_control_num
WHERE sf.account_key_id = ac.account_key_id
AND t502b.c501_order_id (+) = sf.do_id
AND t502b.c205_part_number_id (+) = sf.part_id
AND sf.part_key_id = pd.part_key_id
and previous_day.cal_date (+) = sf.order_dt
AND duplicate_control_num.cnum (+) = regexp_replace(t502b.c502b_usage_lot_num, '\-')
AND c906_rule_value = pd.project_id
AND c906_rule_grp_id = 'ORCNUM'
AND c906_active_fl = 'Y'
AND c906_void_fl IS NULL;