SELECT
  DIST_ID ,
  GET_DISTRIBUTOR_NAME( DIST_ID) DIST_NAME ,
  PNUM ,
  CNUM ,
  MAX(CONSIGNED) CONSIGNED ,
  MAX(DIRECT_SALE) DIRECT_SALE ,
  MAX(RETURNED) RETURNED ,
  MAX(USED) USED ,
  MAX(LOT_TRACK) LOT_TRACK
FROM
  (  
    SELECT
      t504.c701_distributor_id DIST_ID ,
      t505.c205_part_number_id PNUM ,
      t505.c505_control_number CNUM ,
      MAX(t504.c504_consignment_id) CONSIGNED ,
      '' DIRECT_SALE ,
      '' RETURNED ,
      '' USED ,
      '' LOT_TRACK
    FROM
      t504_consignment t504 ,
      t505_item_consignment t505
    WHERE
      t504.c504_consignment_id = t505.c504_consignment_id
    AND t504.c504_void_fl     IS NULL
    AND t504.c504_status_fl    = 4
    AND t504.c5040_plant_id    = 3013
    AND c701_distributor_id   IS NOT NULL
    GROUP BY
      t504.c701_distributor_id ,
      t505.c205_part_number_id ,
      t505.c505_control_number
    UNION ALL
    SELECT
      t703.c701_distributor_id DIST_ID ,
      t502.c205_part_number_id PNUM ,
      t502.c502_control_number CNUM ,
      '' CONSIGNED ,
      MAX(t502.c501_order_id) DIRECT_SALE ,
      '' RETURNED ,
      '' USED ,
      '' LOT_TRACK
    FROM
      t501_order t501 ,
      t502_item_order t502 ,
      t703_sales_rep t703
    WHERE
      t501.c501_order_id       = t502.c501_order_id
    AND t501.c703_sales_rep_id = t703.c703_sales_rep_id
    AND t501.c5040_plant_id    = 3013
    AND t501.c901_order_type  IS NULL
    AND t501.c501_void_fl     IS NULL
    AND t501.c501_delete_fl   IS NULL
    GROUP BY
      t703.c701_distributor_id ,
      t502.c205_part_number_id ,
      t502.c502_control_number
    UNION ALL
    SELECT
      DIST_ID ,
      PNUM ,
      CNUM ,
      '' CONSIGNED ,
      '' DIRECT_SALE ,
      RETURNED ,
      '' USED ,
      '' LOT_TRACK
    FROM
      (
        SELECT
          t506.c701_distributor_id DIST_ID ,
          t507.c205_part_number_id PNUM ,
          t507.c507_control_number CNUM ,
          MAX(t507.c506_rma_id) RETURNED
        FROM
          t506_returns t506,
          t507_returns_item t507
        WHERE
          t506.c506_rma_id       = t507.c506_rma_id
        AND t506.c506_void_fl   IS NULL
        AND t506.c5040_plant_id  = 3013
        AND t506.c506_status_fl >=2
        AND t506.c501_order_id  IS NULL
        GROUP BY
          t506.c701_distributor_id ,
          t507.c205_part_number_id ,
          t507.c507_control_number
        UNION ALL
        SELECT
          t703.c701_distributor_id DIST_ID ,
          t507.c205_part_number_id PNUM ,
          t507.c507_control_number CNUM ,
          MAX(t507.c506_rma_id) RETURNED
        FROM
          t506_returns t506,
          t507_returns_item t507,
          t501_order t501,
          t703_sales_rep t703
        WHERE
          t506.c506_rma_id         = t507.c506_rma_id
        AND t501.c501_order_id     = t506.c501_order_id
        AND t501.c703_sales_rep_id = t703.c703_sales_rep_id
        AND t506.c501_order_id    IS NOT NULL
        AND t506.c506_void_fl     IS NULL
        AND t506.c5040_plant_id    = 3013
        AND t506.c506_status_fl   >=2
        GROUP BY
          t703.c701_distributor_id ,
          t507.c205_part_number_id ,
          t507.c507_control_number
      )
    UNION ALL
    SELECT
      t703.c701_distributor_id dist_id ,
      t502b.c205_part_number_id PNUM ,
      t502b.c502b_usage_lot_num CNUM ,
      '' CONSIGNED ,
      '' DIRECT_SALE ,
      '' RETURNED ,
      MAX(t502b.c501_order_id) USED ,
      '' LOT_TRACK
    FROM
      t501_order t501 ,
      t502b_item_order_usage t502b ,
      t703_sales_rep t703
    WHERE
      t501.c501_order_id       = t502b.c501_order_id
    AND t501.c703_sales_rep_id = t703.c703_sales_rep_id
    AND t501.c5040_plant_id    = 3013
    AND t501.c501_void_fl     IS NULL
    AND t501.c501_delete_fl   IS NULL
    GROUP BY
      c701_distributor_id ,
      t502b.c205_part_number_id ,
      t502b.c502b_usage_lot_num
    UNION ALL
    SELECT
      t701.c701_distributor_id DIST_ID ,
      T2550.C205_PART_NUMBER_ID partnum ,
      T2550.C2550_CONTROL_NUMBER ctrlnum ,
      '' CONSIGNED ,
      '' DIRECT_SALE ,
      '' RETURNED ,
      '' USED ,
      'Y' LOT_TRACK
    FROM
      T2550_PART_CONTROL_NUMBER T2550,
      T5060_CONTROL_NUMBER_INV T5060,
      T701_DISTRIBUTOR T701,
      T901_CODE_LOOKUP T901
    WHERE
      T2550.C205_PART_NUMBER_ID    = T5060.C205_PART_NUMBER_ID
    AND t5060.C901_WAREHOUSE_TYPE  = t901.c901_code_id (+)
    AND T5060.C5060_CONTROL_NUMBER = T2550.C2550_CONTROL_NUMBER
    AND T5060.C5060_REF_ID         = T701.C701_DISTRIBUTOR_ID (+)
    AND NVL(T5060.C5060_QTY,'0')   > 0
    AND t5060.C901_WAREHOUSE_TYPE  = 4000339
    AND T2550.C1900_COMPANY_ID     = 1000
    AND T2550.C5040_PLANT_ID       = 3013
    GROUP BY
      t701.c701_distributor_id ,
      T2550.C205_PART_NUMBER_ID ,
      T2550.C2550_CONTROL_NUMBER
  )
WHERE
  PNUM IN ( '8137.0001S', '8137.0005S', '8137.0010S' )
GROUP BY
  DIST_ID,
  PNUM,
  CNUM
ORDER BY
  DIST_ID,
  PNUM,
  CNUM
