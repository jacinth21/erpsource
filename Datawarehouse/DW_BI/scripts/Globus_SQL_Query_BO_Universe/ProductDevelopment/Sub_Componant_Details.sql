SELECT t205.c202_project_id as project_id,
  get_project_name(t205.c202_project_id) as  project_name,
  T205A.C205_FROM_PART_NUMBER_ID as parent_part_id,
  T205A.C205_TO_PART_NUMBER_ID as child_part_id,
  T205.C205_PART_NUM_DESC as part_description ,
  t205.c205_part_drawing as part_drawing,
  get_code_name(t205.c205_material_spec) as  material_specification,
  T205A.C205A_QTY as Qty,
  T205.C205_REV_NUM as Rev_Level,
  t205.c205_min_accpt_rev_lel as minimum_accept_rev_level,
  t205.c205_inspection_rev as inspection_rev_level
FROM T205A_PART_MAPPING T205A,
  T205_PART_NUMBER T205
WHERE  T205A.C205_TO_PART_NUMBER_ID     = T205.C205_PART_NUMBER_ID
AND t205a.c901_type = 30031 -- subcomponent
UNION ALL
SELECT t205.c202_project_id as project_id,
  get_project_name(t205.c202_project_id) as  project_name,
  T205.C205_PART_NUMBER_ID as parent_part_id,
  T205.C205_PART_NUMBER_ID as child_part_id,
  T205.C205_PART_NUM_DESC as part_description ,
  t205.c205_part_drawing as part_drawing,
  get_code_name(t205.c205_material_spec) as  material_specification,
  0 as Qty,
  T205.C205_REV_NUM as Rev_Level,
  t205.c205_min_accpt_rev_lel as minimum_accept_rev_level,
  t205.c205_inspection_rev as inspection_rev_level
FROM  T205_PART_NUMBER T205;

