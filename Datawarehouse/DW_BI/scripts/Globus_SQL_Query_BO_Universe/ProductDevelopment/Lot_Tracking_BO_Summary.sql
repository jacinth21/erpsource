SELECT
  t.*,
  DECODE(IN_HAND_CNT,LOT_TRACK_COUNT,'N','Y') ISSUE
FROM
  (
  
    SELECT
      DIST_ID ,
      get_distributor_name(DIST_ID) DIST_NAME ,
      PNUM ,
      MAX(CONSIGNMENT_SHIPPED_COUNT) CONSIGNMENT_SHIPPED_COUNT ,
      MAX(DIRECT_SALE_CNT) DIRECT_SALE_CNT ,
      (MAX(CONSIGNMENT_SHIPPED_COUNT) + MAX(DIRECT_SALE_CNT))
      TOTAL_SHIPPED_COUNT ,
      MAX(RETURNED_COUNT) RETURNED_COUNT ,
      MAX(USED_IN_SURGERY_COUNT) USED_IN_SURGERY_COUNT ,
      (MAX(RETURNED_COUNT) + MAX(USED_IN_SURGERY_COUNT))
      TOTAL_USED_AND_RETURNED ,
      MAX(CONSIGNMENT_SHIPPED_COUNT) - (MAX(RETURNED_COUNT) + MAX(
      USED_IN_SURGERY_COUNT)) IN_HAND_CNT ,
      MAX(LOT_TRACK_COUNT) LOT_TRACK_COUNT
    FROM
      (
        SELECT
          t504.c701_distributor_id DIST_ID ,
          t505.c205_part_number_id PNUM ,
          COUNT(1) CONSIGNMENT_SHIPPED_COUNT ,
          0 DIRECT_SALE_CNT ,
          0 RETURNED_COUNT ,
          0 USED_IN_SURGERY_COUNT ,
          0 LOT_TRACK_COUNT
        FROM
          t504_consignment t504 ,
          t505_item_consignment t505
        WHERE
          t504.c504_consignment_id = t505.c504_consignment_id
        AND t504.c504_void_fl     IS NULL
        AND t504.c504_status_fl    = 4
        AND t504.c5040_plant_id    = 3013
        AND c701_distributor_id   IS NOT NULL
        GROUP BY
          t504.c701_distributor_id ,
          t505.c205_part_number_id
        UNION ALL
        SELECT
          t703.c701_distributor_id DIST_ID ,
          t502.c205_part_number_id PNUM ,
          0 CONSIGNMENT_SHIPPED_COUNT ,
          COUNT(1) DIRECT_SALE_CNT ,
          0 RETURNED_COUNT ,
          0 USED_IN_SURGERY_COUNT ,
          0 LOT_TRACK_COUNT
        FROM
          t501_order t501 ,
          t502_item_order t502 ,
          t703_sales_rep t703
        WHERE
          t501.c501_order_id       = t502.c501_order_id
        AND t501.c703_sales_rep_id = t703.c703_sales_rep_id
        AND t501.c5040_plant_id    = 3013
        AND t501.c901_order_type  IS NULL
        AND t501.c501_void_fl     IS NULL
        AND t501.c501_delete_fl   IS NULL
        GROUP BY
          t703.c701_distributor_id ,
          t502.c205_part_number_id
        UNION ALL
        SELECT
          DIST_ID ,
          PNUM ,
          0 CONSIGNMENT_SHIPPED_COUNT ,
          0 DIRECT_SALE_CNT ,
          SUM(RETURNED_COUNT) RETURNED_COUNT ,
          0 USED_IN_SURGERY_COUNT ,
          0 LOT_TRACK_COUNT
        FROM
          (
            SELECT
              t506.c701_distributor_id DIST_ID ,
              t507.c205_part_number_id PNUM ,
              COUNT(1) RETURNED_COUNT
            FROM
              t506_returns t506,
              t507_returns_item t507
            WHERE
              t506.c506_rma_id       = t507.c506_rma_id
            AND t506.c506_void_fl   IS NULL
            AND t506.c5040_plant_id  = 3013
            AND t506.c506_status_fl >=2
            AND t506.c501_order_id  IS NULL
            GROUP BY
              t506.c701_distributor_id ,
              c205_part_number_id
            UNION ALL
            SELECT
              t703.c701_distributor_id DIST_ID ,
              t507.c205_part_number_id ,
              COUNT(1) RETURNED_COUNT
            FROM
              t506_returns t506,
              t507_returns_item t507,
              t501_order t501,
              t703_sales_rep t703
            WHERE
              t506.c506_rma_id         = t507.c506_rma_id
            AND t501.c501_order_id     = t506.c501_order_id
            AND t501.c703_sales_rep_id = t703.c703_sales_rep_id
            AND t506.c501_order_id    IS NOT NULL
            AND t506.c506_void_fl     IS NULL
            AND t506.c5040_plant_id    = 3013
            AND t506.c506_status_fl   >=2
            GROUP BY
              t703.c701_distributor_id ,
              t507.c205_part_number_id
          )
        GROUP BY
          DIST_ID,
          PNUM
        UNION ALL
        SELECT
          t703.c701_distributor_id dist_id ,
          t502b.c205_part_number_id ,
          0 CONSIGNMENT_SHIPPED_COUNT ,
          0 DIRECT_SALE_CNT ,
          0 RETURNED_COUNT ,
          COUNT(1) USED_IN_SURGERY_COUNT ,
          0 LOT_TRACK_COUNT
        FROM
          t501_order t501 ,
          t502b_item_order_usage t502b ,
          t703_sales_rep t703
        WHERE
          t501.c501_order_id               = t502b.c501_order_id
        AND t501.c703_sales_rep_id         = t703.c703_sales_rep_id
        AND t501.c5040_plant_id            = 3013
        AND t501.c501_void_fl             IS NULL
        AND t501.c501_delete_fl           IS NULL
        AND t502b.c502b_usage_lot_num NOT IN
          (
            SELECT
              t502.c502_control_number
            FROM
              t502_item_order t502 ,
              t501_order t501
            WHERE
              t502.c205_part_number_id IN ( '8137.0001S', '8137.0005S',
              '8137.0010S' )
            AND t501.c501_order_id    = t502.c501_order_id
            AND t501.c901_order_type IS NULL
            AND t501.c5040_plant_id   = 3013
            AND t501.c501_void_fl    IS NULL
            AND t501.c501_delete_fl  IS NULL
          )
        GROUP BY
          c701_distributor_id ,
          t502b.c205_part_number_id
        UNION ALL
        SELECT
          t701.c701_distributor_id DIST_ID ,
          T2550.C205_PART_NUMBER_ID partnum ,
          0 CONSIGNMENT_SHIPPED_COUNT ,
          0 DIRECT_SALE_CNT ,
          0 RETURNED_COUNT ,
          0 USED_IN_SURGERY_COUNT ,
          COUNT(1) LOT_TRACK_COUNT
        FROM
          T2550_PART_CONTROL_NUMBER T2550,
          T5060_CONTROL_NUMBER_INV T5060,
          T701_DISTRIBUTOR T701,
          T901_CODE_LOOKUP T901
        WHERE
          T2550.C205_PART_NUMBER_ID    = T5060.C205_PART_NUMBER_ID
        AND t5060.C901_WAREHOUSE_TYPE  = t901.c901_code_id (+)
        AND T5060.C5060_CONTROL_NUMBER = T2550.C2550_CONTROL_NUMBER
        AND T5060.C5060_REF_ID         = T701.C701_DISTRIBUTOR_ID (+)
        AND NVL(T5060.C5060_QTY,'0')   > 0
        AND t5060.C901_WAREHOUSE_TYPE  = 4000339
        AND T2550.C1900_COMPANY_ID     = 1000
        AND T2550.C5040_PLANT_ID       = 3013
        GROUP BY
          t701.c701_distributor_id ,
          t701.c701_distributor_name,
          T2550.C205_PART_NUMBER_ID
      )
    WHERE
      PNUM IN ( '8137.0001S', '8137.0005S', '8137.0010S')
    GROUP BY
      DIST_ID,
      PNUM
  )
  t
ORDER BY
  DIST_ID,
  PNUM
