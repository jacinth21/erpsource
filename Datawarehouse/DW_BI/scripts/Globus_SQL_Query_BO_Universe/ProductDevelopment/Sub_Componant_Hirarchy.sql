SELECT t205a.c205_from_part_number_id as parent_part, t205a.c205_to_part_number_id as child_part ,t205a.c205a_qty as qty, level as lvl
, SYS_CONNECT_BY_PATH(c205_from_part_number_id, '/') as Path
FROM T205A_PART_MAPPING T205A
CONNECT BY NOCYCLE PRIOR t205a.c205_to_part_number_id = t205a.c205_from_part_number_id;