select distinct sf.do_id as order_id, sf.order_dt as order_date
, pd.part_id as part_number , pd.PART_NUM_DESC  as part_description
, sf.item_qty as item_qty, t502b.C502B_USAGE_LOT_NUM AS control_number
, get_set_name(sf.system_id) as system_name
, case when previous_day.cal_date IS NULL
     then 'N' Else 'Y' end as previous_day 
, sf.sales_rep_id as sales_rep_id
, dd.distributor_id as distributor_id
, dd.distributor_name as distributor_name
--, sf.item_order_key_id itemkey -- added to eliminate duplicates when there is same part, order in t502a
, sf.order_type as order_type
, sf.order_id as original_order_id
, t502b.C502B_ITEM_QTY as control_num_qty
from sales_fact sf, T502B_ITEM_ORDER_USAGE t502b,
 (select max(CAL_DATE) cal_date
    from date_dim
    where BUS_DAY_IND = 'Y'
    and CAL_DATE <= (sysdate-1)) previous_day
, part_dim pd, t906_rules 
, sales_rep_dim salesrep, distributor_dim dd
WHERE sf.sales_rep_id = salesrep.sales_rep_id
AND salesrep.distributor_key_id = dd.distributor_key_id
AND t502b.c501_order_id (+) = sf.order_id
AND t502b.c205_part_number_id (+) = sf.part_id
AND sf.part_key_id = pd.part_key_id
and previous_day.cal_date (+) = sf.order_dt
AND c906_rule_value = pd.project_id
AND c906_rule_grp_id = 'ORCNUM'
AND c906_active_fl = 'Y'
AND c906_void_fl IS NULL
AND t502b.c502b_void_fl IS NULL