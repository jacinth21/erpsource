SELECT T5010.C207_SET_ID Set_Id, 
   T207.C207_SET_NM Set_Name, 
   T5010.C5010_LAST_UPDATED_TRANS_ID Transaction_Id, 
   T5096.C5010_TAG_ID Tag_Id, 
   C504A_ETCH_ID Etch_Id, 
   T5095.C5091_WASHER_MASTER_ID Washer_Master_Id, 
   T5095.C5095_CYCLE_NUMBER Cycle_Number, 
   T5095.C5095_CYCLE_DATE Cycle_Date, 
   T5095.LAST_UPDATED_BY Last_Updated_By, 
   T5095.LAST_UPDATED_DATE Last_Updated_Date 
 FROM T5095_WASHER_TXN T5095, 
   T5096_WASHER_TXN_DETAIL T5096, 
   T5010_TAG T5010, 
   T504A_CONSIGNMENT_LOANER T504A, 
   T207_SET_MASTER T207 
 WHERE t5095.c5095_washer_txn_id = T5096.C5095_WASHER_TXN_ID
 AND T5096.C5010_TAG_ID =T5010.C5010_TAG_ID (+) 
 AND T5010.C5010_LAST_UPDATED_TRANS_ID=T504A.C504_CONSIGNMENT_ID (+) 
 AND T207.C207_SET_ID = T5010.C207_SET_ID 
 AND T207.C207_VOID_FL IS NULL 
 AND T5095.C5095_VOID_FL IS NULL 
 AND T5010.C5010_VOID_FL IS NULL 
 AND T5095.C5095_CYCLE_DATE >= to_date(@prompt('From date','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss') 
 AND T5095.C5095_CYCLE_DATE <= to_date(@prompt('To date','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss') 
 ORDER BY T5095.C5095_CYCLE_DATE DESC;
