--PC-2907
--PC Owner - Yoga Balakrishnan


select request.c207_set_id SET_ID
      , t207.c207_set_nm SET_NAME
      , t5010.c5010_tag_id TAG_ID
      , get_code_name(t5010.c901_status) TAG_STATUS
      , get_code_name(t207.c207_category) SET_CATEGORY
      , get_code_name(request.c901_request_for) REQUEST_FOR
      , get_code_name(t207.c901_set_grp_type) SET_GROUP
      , t504a.c504_consignment_id CN_ID
      , gm_pkg_op_loaner.get_loaner_status(t504acl.c504a_status_fl) STATUS
      , t504acl.c504a_etch_id ETCH_ID
      , t504a.c504a_expected_return_dt EXPECTED_RETURN_DATE
      , t504a.c504a_loaner_dt LOANED_DATE
      , TRUNC(SYSDATE) - TRUNC(t504a.c504a_loaner_dt) ELAPSED_DAYS
      , TRUNC(SYSDATE) - TRUNC(t504a.c504a_expected_return_dt) OVERDUE_DAYS
      , GET_CODE_NAME(t504a.c901_consigned_to) CONSIGNED_TO
      , get_distributor_name(t504a.c504a_consigned_to_id) DIST_NAME
      , get_rep_name(request.c703_sales_rep_id) REP_NAME
      , request.c525_surgery_date SURGERY_DATE -- Add this
      , t504a.C504A_IS_LOANER_EXTENDED IS_EXTENDED
      , t504a.C504A_IS_REPLENISHED IS_REPLENISHED
      , t504a.C526_PRODUCT_REQUEST_DETAIL_ID PRODUCT_REQUEST_DETAIL_ID
      , request.c525_product_request_id PRODUCT_REQUEST_ID
   from t504a_loaner_transaction t504a
      , t504a_consignment_loaner t504acl
      , t5010_tag t5010
      , t207_set_master t207
      , (select t5010.c5010_last_updated_trans_id c504_consignment_id, MAX(t5010.c5010_tag_id) tag_id
           from t5010_tag t5010
          where t5010.c5010_void_fl is null
            and t5010.c5010_last_updated_trans_id is not null
       GROUP BY t5010.c5010_last_updated_trans_id) tag
      , (select t525.c525_product_request_id,t526.c526_product_request_detail_id, t525.c901_request_for, t526.c207_set_id, t525.c703_sales_rep_id
      ,t525.c525_surgery_date -- Add this
          from t525_product_request t525
             , t526_product_request_detail t526
         where t525.c525_product_request_id = t526.c525_product_request_id
           and t525.c901_request_for IS NOT NULL
           and t525.c901_request_for = 4127
         ) request
  where t504a.c526_product_request_detail_id = request.c526_product_request_detail_id(+)
    and t504a.c504_consignment_id = t504acl.c504_consignment_id
    and t504a.c504_consignment_id = tag.c504_consignment_id(+)
    and tag.tag_id = t5010.c5010_tag_id
    and request.c207_set_id = t207.c207_set_id
    and t504a.c504a_return_dt is null
    and t504a.c504a_void_fl is null
    and t504a.c1900_company_id = 1000
    and t504a.c5040_plant_id = 3000
    and t504a.c504_consignment_id is not null
    and request.c901_request_for is not null
;



