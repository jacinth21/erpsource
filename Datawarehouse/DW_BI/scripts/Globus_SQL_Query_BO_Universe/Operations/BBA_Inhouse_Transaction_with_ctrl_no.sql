SELECT *
   FROM
    (
         SELECT t412.C412_INHOUSE_TRANS_ID Inhouse_Trans_Id, t412.C412_CREATED_DATE Created_Date,
            t413.C205_PART_NUMBER_ID pnum, t413.C413_CONTROL_NUMBER cnum, t2540.C2540_DONOR_NUMBER donor_number
          , t412.C412_COMMENTS Trans_Comments, get_User_name (t412.C412_VERIFIED_BY) Verified_By,
            t412.C412_VERIFIED_DATE Verified_Date, t412.C412_VERIFY_FL Verify_Flag, DECODE (t412.C412_STATUS_FL, 0,
            'Initiated/Back Order', 2, 'Pending Control', 3, 'Pending Verification', '3.30', 'Packing In Progress',
            '3.60', 'Ready For Pickup', 4, 'Completed') Inhouse_trans_Status, t412.C412_VOID_FL Void_Flag,
            Get_User_Name (t412.C412_CREATED_BY) Created_By, t412.C412_LAST_UPDATED_DATE Last_Updated_Date,
            Get_User_Name (t412.C412_LAST_UPDATED_BY) Last_Updated_By, t412.C412_REF_ID Ref_Id
           FROM T412_INHOUSE_TRANSACTIONS t412, T413_INHOUSE_TRANS_ITEMS t413, T2550_PART_CONTROL_NUMBER t2550
          , T2540_DONOR_MASTER t2540
          WHERE t412.C412_INHOUSE_TRANS_ID  = t413.C412_INHOUSE_TRANS_ID
            AND t2540.C2540_DONOR_ID        = t2550.C2540_DONOR_ID
            AND t2550.C2550_CONTROL_NUMBER  = t413.C413_CONTROL_NUMBER
            AND t2550.C205_PART_NUMBER_ID   = t413.C205_PART_NUMBER_ID
            AND t412.C412_VOID_FL          IS NULL
            AND t413.C413_VOID_FL          IS NULL
            AND t2540.C2540_VOID_FL        IS NULL
            AND t412.C1900_COMPANY_ID       = 1001
            AND t2540.C1900_COMPANY_ID      = 1001
            AND TRUNC (C412_VERIFIED_DATE) >= to_date(@prompt('From date','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss')
            AND TRUNC (C412_VERIFIED_DATE) <= to_date(@prompt('To date','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss')
          UNION
         SELECT t504.C504_CONSIGNMENT_ID Inhouse_Trans_Id, t504.C504_CREATED_DATE Created_Date,
            t505.C205_PART_NUMBER_ID pnum, t505.C505_CONTROL_NUMBER cnum, t2540.C2540_DONOR_NUMBER donor_number
          , t504.C504_COMMENTS Trans_Comments, get_User_name (t504.C504_VERIFIED_BY) Verified_By,
            t504.C504_VERIFIED_DATE Verified_Date, t504.C504_VERIFY_FL Verify_Flag, DECODE (t504.C504_STATUS_FL, 0,
            'Initiated/Back Order', 2, 'Pending Control', 3, 'Pending Verification', '3.30', 'Packing In Progress',
            '3.60', 'Ready For Pickup', 4, 'Completed') Inhouse_trans_Status, t504.c504_VOID_FL Void_Flag,
            Get_User_Name (t504.C504_CREATED_BY) Created_By, t504.C504_LAST_UPDATED_DATE Last_Updated_Date,
            Get_User_Name (t504.C504_LAST_UPDATED_BY) Last_Updated_By, t504.C504_REF_ID Ref_Id
           FROM T504_CONSIGNMENT t504, T505_ITEM_CONSIGNMENT t505, T2550_PART_CONTROL_NUMBER t2550
          , T2540_DONOR_MASTER t2540
          WHERE t504.C504_CONSIGNMENT_ID   = t505.C504_CONSIGNMENT_ID
            AND t2540.C2540_DONOR_ID       = t2550.C2540_DONOR_ID
            AND t2550.C2550_CONTROL_NUMBER = t505.C505_CONTROL_NUMBER
            AND t2550.C205_PART_NUMBER_ID  = t505.C205_PART_NUMBER_ID
            AND t504.C504_VOID_FL         IS NULL
            AND t505.C505_VOID_FL         IS NULL
            AND t2540.C2540_VOID_FL       IS NULL
            AND t504.C1900_COMPANY_ID      = 1001
            AND t2540.C1900_COMPANY_ID     = 1001
            AND TRUNC (C504_VERIFIED_DATE) >= to_date(@prompt('From date','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss')
            AND TRUNC (C504_VERIFIED_DATE) <= to_date(@prompt('To date','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss')
    )
ORDER BY Inhouse_Trans_Id, Verified_Date;