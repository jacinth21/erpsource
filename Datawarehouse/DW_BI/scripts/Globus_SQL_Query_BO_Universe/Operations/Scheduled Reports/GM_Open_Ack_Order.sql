SELECT * FROM ( 
SELECT pnum, 
    get_partnum_desc (pnum) as pnumdesc, 
    accid, 
    get_account_name (accid) as accname, 
    custpo, 
    ordid, 
    shelfqty, 
    ordqty, 
    get_released_qty (pnum, ordid, price) shipqty, 
    (ordqty - get_released_qty (pnum, ordid, price)) pendqty, 
    get_order_attribute_value(ordid,'103926') requiredate, 
    comp_name, 
	  TO_CHAR(order_date,'MM/DD/YYYY') ORDER_DATE 
FROM 
    (SELECT t502.c205_part_number_id pnum, 
        t501.c704_account_id accid, 
        t501.c501_customer_po custpo , 
        t501.c501_order_id ordid, 
        SUM(t502.c502_item_qty) ordqty, 
        t502.c502_item_price price, 
        get_company_name(t501.c1900_company_id) comp_name, 
		NVL(shelfqty,0) shelfqty, 
		t501.c501_order_date order_date 
    FROM t501_order t501, 
        t502_item_order t502, 
		( 
		   SELECT t205c.c205_part_number_id,  
		          ( NVL (t205c.c205_available_qty, 0) -  NVL (v205c.c205_allocated_qty, 0)	) shelfqty	  
			 FROM globus_app.t205c_part_qty t205c 
				, globus_app.v205c_part_allocated_qty v205c 
	        WHERE t205c.c901_transaction_Type  = 90800  -- Inventory Qty
	          AND t205c.c205_part_number_id = v205c.c205_part_number_id(+) 
	          AND v205c.c901_transaction_type(+) = '60001' -- FG Inventory
	          AND t205c.c5040_plant_id 	= 	@prompt('Enter Plant ID','A',,mono,free,persistent)
	          AND t205c.c5040_plant_id     =   v205c.C5040_PLANT_ID(+) 
	   ) shelf 
    WHERE t502.c501_order_id = t501.c501_order_id 
	AND t502.c205_part_number_id = shelf.c205_part_number_id (+)
    AND t501.c901_order_type = '101260' 
    AND t501.c501_status_fl <> '3' 
    AND t501.c501_void_fl   IS NULL 
    AND t502.c502_void_fl   IS NULL 
    AND t501.c5040_plant_id = @prompt('Enter Plant ID','A',,mono,free,persistent)
    GROUP BY t502.c205_part_number_id, 
        t501.c704_account_id, 
        t501.c501_customer_po , 
        t501.c501_order_id, 
        t502.c502_item_price, 
        t501.c1900_company_id, 
        t501.c501_order_date,  
        shelfqty 
    ) 
   ) 
 where pendqty > 0    
ORDER BY pnum, order_date