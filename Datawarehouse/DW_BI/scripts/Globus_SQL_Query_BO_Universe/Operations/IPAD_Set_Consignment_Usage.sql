SELECT t7100.c7100_case_id AS ID
     , v700.gp_name AS ZONE
     , TO_CHAR(t7100.c7100_created_date, 'MM/YYYY') AS MONTH_YEAR
     , 0 AS PORTAL_COUNT
     , 1 AS IPAD_COUNT
  from t7100_case_information t7100 
     , ( select v700.d_id, v700.gp_name
           from v700_territory_mapping_detail v700 
       GROUP BY v700.d_id, v700.gp_name ) v700
WHERE t7100.c901_type in (1006506)     
  AND t7100.c1900_company_id = 1000
  AND t7100.c901_case_status IN (11091,19524)
  AND t7100.c7100_void_fl IS NULL
  AND TRUNC (t7100.c7100_created_date) >= to_date(@prompt('From date','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss')
  AND TRUNC (t7100.c7100_created_date)  <= to_date(@prompt('To date','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss')
  AND v700.d_id = t7100.c701_distributor_id
UNION ALL  
select t520.c520_request_id AS ID     
     , v700.gp_name AS ZONE
     , TO_CHAR(t520.c520_request_date, 'MM/YYYY') AS MONTH_YEAR
     , 1 AS PORTAL_COUNT
     , 0 AS IPAD_COUNT
from   
  ( select *
  from t520_request t520 
  where t520.c207_set_id is not null 
  and t520.c520_void_fl is null
  and t520.c520_request_date >= to_date(@prompt('From date','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss')
  and t520.c520_request_date <= to_date(@prompt('To date','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss')
  and t520.c520_request_to is not null
  and t520.c1900_company_id = 1000
  ) t520 
   , ( select distinct v700.d_id, v700.gp_name
           from v700_territory_mapping_detail v700 
       GROUP BY v700.d_id, v700.gp_name ) v700
where t520.c520_request_to = v700.d_id
