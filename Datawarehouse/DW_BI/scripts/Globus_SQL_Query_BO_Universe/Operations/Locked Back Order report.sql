--PC-2849
--PC Owner - Yoga Balakrishnan


SELECT t2591.c2590_back_order_lock_id lock_id, 
   t2590.c2590_back_order_lock_date lock_date, 
   t2591.c205_part_number_id part_number, 
   t2591.c2591_qty qty, 
   t901.c901_code_nm back_order_type,
   rank.rank 
 FROM Globus_app. t2591_back_order_details t2591, 
  Globus_app. t2590_back_order_lock t2590, 
   t901_code_lookup t901, 
   (SELECT t205.c205_part_number_id part_number, 
     t901_curr_mon.c901_code_nm rank 
   FROM t205_part_number t205, 
     globus_dm_operation.t4071_purchase_part_rank t4071, 
     globus_dm_operation.t4072_purchase_part_rank_detail t4072, 
     t901_code_lookup t901_curr_mon 
   WHERE t4071.c4071_purchase_part_rank_id = t4072.c4071_purchase_part_rank_id 
   AND t205.c205_part_number_id = t4072.c205_part_number_id 
   AND t4072.c4072_rank = t901_curr_mon.c901_code_id (+) 
   AND t4071.c4071_void_fl IS NULL 
   AND to_char(t4071.c4071_load_date,'MM/YYYY') = to_char(SYSDATE,'MM/YYYY') 
   ) rank 
 WHERE t2590.c2590_back_order_lock_id = t2591.c2590_back_order_lock_id 
 AND t2591.c2591_back_order_type = t901.c901_code_id (+) 
 AND t2591.c205_part_number_id = rank.part_number (+) 
 AND t2590.c2590_back_order_lock_date  in ( select max(c2590_back_order_lock_date) from Globus_app.t2590_back_order_lock)



