SELECT t7100.c7100_case_id AS ID
     , v700.gp_name AS ZONE
     , TO_CHAR(t7100.c7100_created_date, 'MM/YYYY') AS MONTH_YEAR
     , 0 AS PORTAL_COUNT
     , 1 AS IPAD_COUNT
  from t7100_case_information t7100 
     , ( select v700.d_id, v700.gp_name
           from v700_territory_mapping_detail v700 
       GROUP BY v700.d_id, v700.gp_name ) v700
WHERE t7100.c901_type in (1006505)     
  AND t7100.c1900_company_id = 1000
  AND t7100.c901_case_status IN (11091,19524)
  AND t7100.c7100_void_fl IS NULL
  AND TRUNC (t7100.c7100_created_date) >= to_date(@prompt('From date','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss')
  AND TRUNC (t7100.c7100_created_date)  <= to_date(@prompt('To date','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss')
  AND v700.d_id = t7100.c701_distributor_id
UNION ALL
select t525.c525_product_request_id AS ID
     , v700.gp_name AS ZONE
     , TO_CHAR(t525.c525_created_date,'MM/YYYY') AS MONTH_YEAR
     , 1 AS PORTAL_COUNT
     , 0 AS IPAD_COUNT
from t525_product_request t525
    , ( select v700.d_id, v700.gp_name
           from v700_territory_mapping_detail v700 
       GROUP BY v700.d_id, v700.gp_name ) v700
where t525.c525_request_for_id = v700.d_id
and t525.c525_product_request_id NOT IN 
(
select DISTINCT t525.c525_product_request_id
from t7100_case_information t7100 ,
   t7104_case_set_information t7104,
   t526_product_request_detail t526,
   t525_product_request t525
where t7104.c7100_case_info_id = t7100.c7100_case_info_id
AND t7104.C526_PRODUCT_REQUEST_DETAIL_ID = t526.C526_PRODUCT_REQUEST_DETAIL_ID
AND t526.c525_product_request_id = t525.c525_product_request_id
AND t7100.c901_type in (1006505,1006506,1006507) 
AND TRUNC (t7100.c7100_created_date) >= to_date(@prompt('From date','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss')
AND TRUNC (t7100.c7100_created_date)  <= to_date(@prompt('To date','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss')
AND t7100.c901_case_status IN (11091,19524)
AND t7100.c7100_void_fl IS NULL
AND t7104.C526_PRODUCT_REQUEST_DETAIL_ID IS NOT NULL
)
and TRUNC(t525.c525_created_date) >= (To_Date('01/01/2017','MM/dd/yyyy'))
and t525.c901_request_for = 4127
and c901_request_by_type = '50626'
order by month_year
