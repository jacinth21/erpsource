SELECT  ROWNUM sno,wo_data.vendor_name, part_data.*
FROM
  (SELECT 
    pa.part_key_id part_number,
    part.part_num_desc part_desc,
    part.project_name,
    part.product_family,
    pa.brand_name brand_name,
    pa.gmdn_code gmdn_code,
    gm_pkg_pd_rpt_udi.get_part_di_number(pa.part_key_id) di_number,
    rfs_dim.rfs_flag rfs_flag,
    fda_submission_no_1 ,
    fda_submission_no_2 ,
    fda_submission_no_3 ,
    fda_submission_no_4 ,
    fda_submission_no_5 ,
    fda_submission_no_6 ,
    fda_submission_no_7 ,
    fda_submission_no_8 ,
    fda_submission_no_9 ,
    fda_submission_no_10 ,
    fda_product_code_1 ,
    fda_product_code_2 ,
    fda_product_code_3 ,
    fda_product_code_4 ,
    fda_product_code_5 ,
    fda_product_code_6 ,
    fda_product_code_7 ,
    fda_product_code_8 ,
    fda_product_code_9 ,
    fda_product_code_10,
    pma_supplement_no_1 ,
    pma_supplement_no_2
  FROM globus_dw.part_dim part,
    globus_dw.part_attribute_dim pa,
    globus_dw.release_for_sales_dim rfs_dim
  WHERE part.part_key_id      = pa.part_key_id
  AND rfs_dim.part_key_id     =pa.part_key_id
  AND rfs_dim.country_code_id = 80120 --US
  ) part_data,
  (SELECT vendor.vendor_name vendor_name,
    wo.part_key_id part_number
  FROM globus_dw.work_order_fact wo,
    globus_dw.vendor_dim vendor,
    globus_dw.date_dim date_dim
  WHERE vendor.vendor_key_id = wo.vendor_key_id
  AND wo.date_key_id         =date_dim.date_key_id
   AND ( date_dim.cal_date BETWEEN add_months (TRUNC (SYSDATE, 'MONTH'), (-1*(@prompt('Enter No','N',{'3','6','9','12','24'},mono,free,persistent)))) AND last_day(add_months (TRUNC (SYSDATE, 'MONTH'), -1)))
   GROUP BY vendor.vendor_name, 
     wo.part_key_id 
   ) wo_data 
 WHERE part_data.part_number = wo_data.part_number;
 
 
 
 
 