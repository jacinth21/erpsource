SELECT GP_NAME, YEAR||'/'||month as year_month,
  COUNT(pricing_req_id) as req_count
FROM
  (SELECT c7500_account_price_request_id pricing_req_id,  GP_NAME, 
    TO_CHAR(c7500_created_date,'MM') month,
    TO_CHAR(c7500_created_date,'YYYY') YEAR
  FROM t7500_account_price_request,
   ( select  
 rep_id, GP_NAME 
from v700_territory_mapping_detail group by rep_id, GP_NAME ) v_zone 
  WHERE c7500_void_fl IS NULL and c703_sales_rep_id is not null and rep_id=c703_sales_rep_id
  )
GROUP BY GP_NAME,YEAR, month
ORDER BY GP_NAME,YEAR,month;