
--@"C:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Pricing\V_GROUP_PART_PRICING.VW";
/*********************************************************************************************
 * Author : Velu
 * Description : This view taking all the Account Grouping Price information and 	
 * 				 columns are Group ID,PNUM, ACC ID, GPOID from T7051 to Account Pricing Report for BO. 
 *  
 ********************************************************************************************** 
 */
CREATE OR REPLACE VIEW V_GROUP_PART_PRICING
AS
     SELECT T7051.C704_ACCOUNT_ID, T7051.C101_GPO_ID, T4010.C4010_GROUP_ID
      , T4011.C205_PART_NUMBER_ID
       FROM T4011_GROUP_DETAIL T4011, T4010_GROUP T4010, T7051_ACCOUNT_GROUP_PRICING T7051
      WHERE T4010.C4010_GROUP_ID         = T7051.C7051_REF_ID
        AND t4010.c4010_group_id         = t4011.c4010_group_id
        AND T4010.C901_TYPE              = 40045 --ForeCast / Demand Group
        AND T4011.C901_PART_PRICING_TYPE = 52080 -- Primary Group
        AND T7051.C901_REF_TYPE          = 52000 -- Group Type
        AND c7051_void_fl               IS NULL
        AND t4010.c4010_void_fl         IS NULL
        AND c7051_active_fl              = 'Y'
  UNION ALL
     SELECT T7051.C704_ACCOUNT_ID, T7051.C101_GPO_ID, NULL C4010_GROUP_ID
      ,T7051.C7051_REF_ID C205_PART_NUMBER_ID
       FROM T7051_ACCOUNT_GROUP_PRICING T7051
       WHERE t7051.C901_REF_TYPE = 52001
       AND c7051_void_fl      IS NULL
       AND c7051_active_fl     = 'Y';
       