- Part price master details
SELECT c205_part_number_id PART_ID,
    c2052_equity_price EQUITY_PRICE,
    c2052_loaner_price LOANER_PRICE,
    c2052_consignment_price CONSIGNMENT_PRICE,
    c2052_list_price LIST_PRICE
FROM t2052_part_price_mapping
WHERE c1900_company_id=1000
AND c2052_void_fl    IS NULL