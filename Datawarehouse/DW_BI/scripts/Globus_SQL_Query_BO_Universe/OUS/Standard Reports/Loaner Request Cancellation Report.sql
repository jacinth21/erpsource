--PC-2904
--PC Owner - Yoga Balakrishnan


SELECT c526_product_request_detail_id Request_Detail_Id,
t525.C525_PRODUCT_REQUEST_ID Parent_Request_Id,
t526.c207_set_id set_Id, 
get_set_name(t526.c207_set_id) Set_Name,
get_distributor_name(C525_REQUEST_FOR_ID) Distributor_Name,
c525_requested_date Requested_Date, 
c526_qty_requested Qty_Requested,
get_code_name(t907.c901_cancel_cd) Cancelled_Reason,
c907_created_date Cancelled_Date,
get_user_name(c907_created_by) Cancelled_By, 
get_company_name(t526.c1900_company_id) Company_Name 
FROM t526_product_request_detail t526, 
t907_cancel_log t907,
T525_PRODUCT_REQUEST t525 
WHERE c907_ref_id = c526_product_request_detail_id 
AND t525.C525_PRODUCT_REQUEST_ID =t526.C525_PRODUCT_REQUEST_ID 
AND trunc(c907_created_date)= to_date(@prompt('Enter From Date:','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss')
AND trunc(c907_created_date)= to_date(@prompt('Enter To Date:','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss')
AND t907.c901_type = 90198 
AND t525.C901_REQUEST_FOR = 4127 
AND t526.c5040_plant_id = 3003
;