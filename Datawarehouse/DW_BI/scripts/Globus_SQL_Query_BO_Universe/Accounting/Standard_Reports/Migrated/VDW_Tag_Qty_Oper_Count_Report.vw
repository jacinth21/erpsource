-- @"D:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Standard_Reports\Migrated\VDW_Tag_Qty_Oper_Count_Report.vw";

CREATE OR REPLACE VIEW VDW_Tag_Qty_Oper_Count_Report AS
	SELECT   d_id, get_distributor_name (d_id) d_name, p_num, c_qty, r_qty, f_value
	FROM     t205_part_number t205,
	         t205d_part_attribute t205d, 
	         (SELECT consign.d_id, consign.p_num, consign.c_qty,
	                 NVL (retur.r_qty, 0) r_qty,
	                 (consign.c_qty - NVL (retur.r_qty, 0)) f_value
	            FROM (SELECT   t504.c701_distributor_id d_id,
	                           t505.c205_part_number_id p_num,
	                           SUM (t505.c505_item_qty) c_qty
	                      FROM t504_consignment t504, t505_item_consignment t505
	                     WHERE t504.c701_distributor_id IS NOT NULL
	                       AND t504.c504_consignment_id = t505.c504_consignment_id
	                       AND t504.c504_status_fl = 4
	                       AND t504.c504_type IN (4110)
	                       AND TRIM (t505.c505_control_number) IS NOT NULL
	                       AND c504_void_fl IS NULL
						    AND t504.c1900_company_id  =1000
							AND t504.c5040_plant_id    =3000
	                  --AND c701_distributor_id IN (7,8,9)
	                  GROUP BY t504.c701_distributor_id, t505.c205_part_number_id) consign,
	                 (SELECT   t506.c701_distributor_id d_id,
	                           t507.c205_part_number_id p_num,
	                           SUM (t507.c507_item_qty) r_qty
	                      FROM t506_returns t506, t507_returns_item t507
	                     WHERE t506.c701_distributor_id IS NOT NULL
	                       AND t506.c506_status_fl = 2
	                       AND t506.c506_rma_id = t507.c506_rma_id
	                       AND t507.c507_status_fl IN ('C', 'W')
	                       AND t506.c506_void_fl IS NULL
						    AND t506.c1900_company_id  =1000
							AND t506.c5040_plant_id    =3000
	                  GROUP BY t506.c701_distributor_id, t507.c205_part_number_id) retur
	           WHERE consign.d_id = retur.d_id(+) AND consign.p_num = retur.p_num(+)) temp
	   WHERE f_value > 0
	     AND t205.c205_part_number_id = temp.p_num
	     AND t205.c205_part_number_id = t205d.c205_part_number_id
	     AND t205d.c901_attribute_type = 92340
	   AND t205d.C205D_ATTRIBUTE_VALUE = 'Y'
	UNION ALL
	SELECT   d_id, get_distributor_name (d_id), p_num, c_qty, r_qty, f_value
	FROM     t205_part_number t205,
	         t205d_part_attribute t205d,
	         (SELECT retur.d_id, retur.p_num, NVL (consign.c_qty, 0) c_qty,
	                 NVL (retur.r_qty, 0) r_qty,
	                 (NVL (consign.c_qty, 0) - NVL (retur.r_qty, 0)) f_value
	            FROM (SELECT   t504.c701_distributor_id d_id,
	                           t505.c205_part_number_id p_num,
	                           SUM (t505.c505_item_qty) c_qty
	                      FROM t504_consignment t504, t505_item_consignment t505
	                     WHERE t504.c701_distributor_id IS NOT NULL
	                       AND t504.c504_consignment_id = t505.c504_consignment_id
	                       AND t504.c504_status_fl = 4
	                       AND t504.c504_type IN (4110, 4129)
	                       AND TRIM (t505.c505_control_number) IS NOT NULL
	                       AND c504_void_fl IS NULL
						   AND t504.c1900_company_id  =1000
							AND t504.c5040_plant_id    =3000
	                  --   AND c701_distributor_id IN (7, 8, 9)
	                  GROUP BY t504.c701_distributor_id, t505.c205_part_number_id) consign,
	                 (SELECT   t506.c701_distributor_id d_id,
	                           t507.c205_part_number_id p_num,
	                           SUM (t507.c507_item_qty) r_qty
	                      FROM t506_returns t506, t507_returns_item t507
	                     WHERE t506.c701_distributor_id IS NOT NULL
	                       AND t506.c506_status_fl = 2
	                       AND t506.c506_rma_id = t507.c506_rma_id
	                       AND t507.c507_status_fl IN ('C', 'W')
	                       AND t506.c506_void_fl IS NULL
						   AND t506.c1900_company_id  =1000
							AND t506.c5040_plant_id    =3000
	                  --   AND c701_distributor_id IN (7, 8, 9)
	                  GROUP BY t506.c701_distributor_id, t507.c205_part_number_id) retur
	           WHERE retur.d_id = consign.d_id(+) AND retur.p_num = consign.p_num(+)) temp
	   WHERE f_value < 0
	     AND t205.c205_part_number_id = temp.p_num
	     --AND t205.c205_part_number_id = costid.pc_num(+)
	     AND t205.c205_part_number_id = t205d.c205_part_number_id
	     AND t205d.c901_attribute_type = 92340
	     AND t205d.C205D_ATTRIBUTE_VALUE = 'Y'
	ORDER BY d_id, p_num;


