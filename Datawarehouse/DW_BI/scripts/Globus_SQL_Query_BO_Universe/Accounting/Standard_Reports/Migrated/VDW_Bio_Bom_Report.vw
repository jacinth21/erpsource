-- @"D:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Standard_Reports\Migrated\VDW_Bio_Bom_Report.vw";

CREATE OR REPLACE VIEW VDW_Bio_Bom_Report AS
		SELECT   t207.c207_set_id bomid, t207.c207_set_nm bomname,  t208.c205_part_number_id partnum
		       , globus_app.get_partnum_desc (t208.c205_part_number_id) partdesc, NVL (t208.c208_set_qty, 0) bomqty
		, 0 AS cogprice --NVL (globus_app.get_ac_cogs_value (t208.c205_part_number_id, 4909), 0) cogprice
		    FROM t207_set_master t207, t208_set_details t208
		   WHERE t207.c901_set_grp_type = '1602'
		     AND t208.c207_set_id = t207.c207_set_id
		     AND t208.C208_INSET_FL = 'Y'
		     AND t207.c207_void_fl IS NULL
		     AND t208.c208_void_fl IS NULL
			 --AND t207.c207_set_id = 'BM.001' 
		ORDER BY t207.c207_set_id, t208.c205_part_number_id,
		t208.c208_critical_fl DESC,
		t208.c208_inset_fl DESC;


