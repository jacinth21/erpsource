-- @"D:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Standard_Reports\Migrated\VDW_WklyInHouseAcc_OperQtyRpt.vw";

CREATE OR REPLACE VIEW VDW_WklyInHouseAcc_OperQtyRpt AS
	SELECT	 pnum.c205_part_number_id pnum, pnum.c205_part_num_desc pdesc, NVL (oper_qty, 0) operqty
		   , NVL (account_qty, 0) accountqty, (NVL (oper_qty, 0) - NVL (account_qty, 0)) diff
		FROM t205_part_number pnum, T2023_PART_COMPANY_MAPPING t2023
		   , (SELECT consign.p_num, (consign.c_qty - NVL (retur.r_qty, 0)) oper_qty
				FROM (SELECT   t505.c205_part_number_id p_num, SUM (t505.c505_item_qty) c_qty
						  FROM t504_consignment t504, t505_item_consignment t505
						 WHERE t504.c504_consignment_id = t505.c504_consignment_id
						   AND t504.c504_status_fl = 4
						   AND (   (t504.c504_type IN (4112) AND NVL (t504.c504_inhouse_purpose, 1) = 4131)
								OR (t504.c504_type IN (4119) AND NVL (t504.c504_inhouse_purpose, 1) NOT IN
																										 (4134, 4137, 4136)
								   )
							   )
						   AND TRIM (t505.c505_control_number) IS NOT NULL
						   AND c504_void_fl IS NULL
						   AND t504.c1900_company_id  =1000
							AND t504.c5040_plant_id    =3000
						   AND t505.c505_void_fl IS NULL-- Added by Mihir-- causes difference
						   AND t504.c504_consignment_id NOT LIKE 'GM-EX-%'
					  GROUP BY t505.c205_part_number_id) consign
				   , (SELECT   t507.c205_part_number_id p_num, SUM (t507.c507_item_qty) r_qty
						  FROM t506_returns t506, t507_returns_item t507
						 WHERE t506.c704_account_id = '02'
						   AND t506.c506_status_fl = 2
						   AND t506.c506_rma_id = t507.c506_rma_id
						   AND t507.c507_status_fl IN ('C', 'W')
						   AND t506.c506_void_fl IS NULL
						   AND t506.c1900_company_id  =1000
							AND t506.c5040_plant_id    =3000
						   AND t506.c506_update_inv_fl IS NOT NULL
					  GROUP BY t507.c205_part_number_id) retur
			   WHERE consign.p_num = retur.p_num(+)) operqty
		   , (SELECT   x.c205_part_number_id, SUM (NVL (x.c820_qty_on_hand, 0)) account_qty
				  FROM t820_costing x
				 WHERE x.c901_status IN (4800, 4801) AND x.c901_costing_type = 4903
			  GROUP BY x.c205_part_number_id) accountqty
	   WHERE pnum.c205_part_number_id = operqty.p_num(+)
		 AND pnum.c205_part_number_id = accountqty.c205_part_number_id(+)
		 and t2023.c205_part_number_id = pnum.c205_part_number_id
		 and t2023.c2023_void_fl is null
		 AND t2023.C1900_COMPANY_ID = 1000
		 AND EXISTS
								   (
										 SELECT t205c.c205_part_number_id
										   FROM t205c_part_qty t205c
										  WHERE t205c.c205_part_number_id   = pnum.c205_part_number_id
											AND t205c.c205_available_qty   IS NOT NULL
											AND t205c.c901_transaction_type = '90800' --FG qty
											AND t205c.c5040_plant_id    =3000
									)
	--	 AND INSTR (pnum.c205_part_number_id, '.', 5, 1) = 0  -- Commented by Richard/Mihir Causes Diff
	ORDER BY pnum.c205_part_number_id;


