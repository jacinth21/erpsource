-- @"D:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Standard_Reports\Migrated\VDW_Loaner_Convert_To_Consign.vw";

CREATE OR REPLACE VIEW VDW_Loaner_Convert_To_Consign AS
	SELECT DISTINCT loanerswap.c504_consignment_id "Loaner ID", loanerswap.c504a_etch_id "Etch ID",
	       t504.c504_consignment_id "Consignment ID",
	       get_distributor_name (t504.c701_distributor_id) "Distributor Name", tlog.c5010_tag_id "Tag ID",
	       t504.C207_SET_ID "Set ID",GET_SET_NAME(t504.C207_SET_ID ) "Set Name",   
	       TO_CHAR (t504.c504_created_date, 'MM/DD/YYYY') "Created_Date",
	       get_user_name (t504.c504_created_by) "Created By"
	  FROM t504_consignment t504,
	       (SELECT x.c504_consignment_id, x.c504a_etch_id, y.c504d_ref_id
	          FROM t504a_consignment_loaner x, t504b_loaner_swap_log y
	         WHERE x.c504a_status_fl = '60'
			   AND x.c5040_plant_id    =3000
	           AND x.c504_consignment_id = y.c504_consignment_id
	           AND y.c901_type = 51101) loanerswap,
	       t5011_tag_log tlog
	 WHERE t504.c504_consignment_id = loanerswap.c504d_ref_id
	   AND t504.c504_consignment_id = tlog.c5011_last_updated_trans_id(+)
	   AND t504.c1900_company_id  =1000
		AND t504.c5040_plant_id    =3000
	ORDER BY "Distributor Name";


