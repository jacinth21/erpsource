-- @"D:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Standard_Reports\Migrated\VDW_WklyBuildSetAccvsOperRpt.vw";
CREATE OR REPLACE VIEW VDW_WklyBuildSetAccvsOperRpt AS 
SELECT   operqty.c205_part_number_id pnum, operqty.c205_part_num_desc pdesc, NVL (oper_qty, 0) operqty
       , NVL (account_qty, 0) accountqty, (NVL (oper_qty, 0) - NVL (account_qty, 0)) diff
    FROM (SELECT t205.c205_part_number_id, c205_part_num_desc , (NVL(oper_qty,0) + NVL (operqty2, 0)) oper_qty
            FROM t205_part_number t205 , T2023_PART_COMPANY_MAPPING t2023,  (SELECT   y.c205_part_number_id, SUM (y.c505_item_qty) oper_qty
                      FROM t504_consignment x, t505_item_consignment y
                     WHERE x.c504_void_fl IS NULL
                       AND x.c207_set_id IS NOT NULL
                       AND x.c504_status_fl IN (1.20,2,2.20, 3)--added status 2.20 Pending PIC, 1.20 Pending FG
                       AND x.c504_verify_fl = 1
					   AND x.c1900_company_id  =1000
						AND x.c5040_plant_id    =3000
                       AND x.c504_consignment_id = y.c504_consignment_id
                       AND TRIM (y.c505_control_number) IS NOT NULL
--  AND TRIM(X.C504_TRACKING_NUMBER) IS NULL
--  AND X.C504_TYPE =
                  GROUP BY y.c205_part_number_id) operqty1
               , (SELECT   t413.c205_part_number_id, SUM (t413.c413_item_qty) operqty2
                      FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413, t504_consignment t504
                     WHERE t412.c412_type IN (50161, 50162)
                       AND t412.c412_status_fl < 4
                       AND t412.c412_verify_fl IS NULL
                       AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
                       AND t412.c412_ref_id = t504.c504_consignment_id
                       AND t504.c207_set_id IS NOT NULL
                       AND t413.c413_control_number <> 'TBE'
					    AND t504.c1900_company_id  =1000
						AND t504.c5040_plant_id    =3000
                       AND TRUNC (t412.c412_created_date) > TO_DATE ('10/26/2008', 'mm/dd/yyyy')
                       AND t412.C412_VOID_FL IS NULL -- Added by Mihir b'coz to exclude  voided BS txns created by Physical Inventory
                  GROUP BY t413.c205_part_number_id) operqty2
           WHERE t205.c205_part_number_id = operqty2.c205_part_number_id(+)
		   and t2023.c205_part_number_id = t205.c205_part_number_id
			and t2023.c2023_void_fl is null
			AND t2023.C1900_COMPANY_ID = 1000
                                   AND   t205.c205_part_number_id = operqty1.c205_part_number_id (+)
                                   AND EXISTS
								   (
										 SELECT t205c.c205_part_number_id
										   FROM t205c_part_qty t205c
										  WHERE t205c.c205_part_number_id   = t205.c205_part_number_id
											AND t205c.c205_available_qty   IS NOT NULL
											AND t205c.c901_transaction_type = '90800' --FG qty
											AND t205c.c5040_plant_id    =3000
									)) operqty
       , (SELECT   x.c205_part_number_id, SUM (NVL (x.c820_qty_on_hand, 0)) account_qty
              FROM t820_costing x
             WHERE x.c901_status IN (4800, 4801) AND x.c901_costing_type = 4901
          GROUP BY x.c205_part_number_id) accountqty
   WHERE operqty.c205_part_number_id = accountqty.c205_part_number_id(+);
    -- AND INSTR (operqty.c205_part_number_id , '.', 5, 1) = 0 --Commented by Mihir currently causes difference of 1 part