-- @"D:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Standard_Reports\Migrated\VDW_Bio_DHR_Report.vw";

CREATE OR REPLACE VIEW VDW_Bio_DHR_Report AS
	SELECT   t408.c408_control_number lotnum
	             , t408.C205_PART_NUMBER_ID finishedpartnum 
	             , t303.C303_MATERIAL_REQUEST_ID request_id
	             , TO_CHAR (t303.c303_material_request_dt, 'mm/dd/yyyy') ldate
	             , TRUNC(t303.c303_material_request_dt) matdate
	            ,  T304.c205_part_number_id sub_pnum,
	              get_partnum_desc (T304.c205_part_number_id) sub_pdesc       
	         , to_number(GET_MFG_MATREQ_PARTNUM_QTY(t303.c303_material_request_id,t304.c205_part_number_id)) QTYreq
	         , T304.C304_ITEM_QTY          
	       , DECODE (t303.c303_status_fl, 0, 'Initiated', 1, 'In Progress', 2, 'Closed') sfl
	       , DECODE (t402.c402_status_fl, 1, 'Open', 2, 'Closed') wostatus
	    FROM t401_purchase_order t401, t402_work_order t402, t408_dhr t408, 
	             t303_material_request t303, t304_material_request_item T304
	   WHERE t402.c402_work_order_id = t408.c402_work_order_id
	     AND t408.c408_dhr_id = t303.c408_dhr_id(+)
	     AND t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
	       AND t303.c303_material_request_id = t304.c303_material_request_id
	     AND t401.c401_type IN (3104, 3105)
		 AND t408.c1900_company_id  =1000
		AND t408.c5040_plant_id    =3000
	     --AND TRUNC(t303.c303_material_request_dt) >= {From Date}
	     --AND TRUNC(t303.c303_material_request_dt) <= {To Date}
	ORDER BY t408.C205_PART_NUMBER_ID, t408.c408_control_number;