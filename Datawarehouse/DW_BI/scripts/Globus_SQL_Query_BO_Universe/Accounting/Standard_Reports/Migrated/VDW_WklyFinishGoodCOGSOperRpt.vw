-- @"D:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Standard_Reports\Migrated\VDW_WklyFinishGoodCOGSOperRpt.vw";

CREATE OR REPLACE VIEW VDW_WklyFinishGoodCOGSOperRpt AS
SELECT t205.c205_part_number_id, t205.c205_part_num_desc, nvl (cogs_value, 0) cogs_value
	  ,(nvl (FG.fg_qty, 0) --FG
      + nvl (bulk.bulk_qty, 0)  --Bulk                                                  
      + nvl (Repackage.repack_qty, 0)  --Repackage                                                  
      + nvl (Returns.return_qty, 0)  --Returns
      ) inv_value
   FROM t205_part_number t205 , T2023_PART_COMPANY_MAPPING t2023,
   (
   SELECT t205c.C205_PART_NUMBER_ID , t205c.C205_AVAILABLE_QTY fg_qty
	FROM t205c_part_qty t205c
	WHERE C901_TRANSACTION_TYPE = 90800
	AND t205c.c5040_plant_id    =3000
   )FG,
   (
   SELECT t205c.C205_PART_NUMBER_ID , t205c.C205_AVAILABLE_QTY bulk_qty
	FROM t205c_part_qty t205c
	WHERE C901_TRANSACTION_TYPE = 90814
	AND t205c.c5040_plant_id    =3000
   )bulk,
   (
   SELECT t205c.C205_PART_NUMBER_ID , t205c.C205_AVAILABLE_QTY repack_qty
	FROM t205c_part_qty t205c
	WHERE C901_TRANSACTION_TYPE = 90815
	AND t205c.c5040_plant_id    =3000
   )Repackage,
   (
   SELECT t205c.C205_PART_NUMBER_ID , t205c.C205_AVAILABLE_QTY return_qty
	FROM t205c_part_qty t205c
	WHERE C901_TRANSACTION_TYPE = 90812
	AND t205c.c5040_plant_id    =3000
   )Returns,(SELECT   c205_part_number_id, SUM (c820_qty_on_hand) cogs_value
	            FROM t820_costing
	           WHERE c901_costing_type = 4900 AND c901_status <> 4802
	           AND c1900_company_id = 1000
	        group by c205_part_number_id) icogg
	 WHERE t205.c205_part_number_id = icogg.c205_part_number_id(+)
	 and t205.c205_part_number_id = bulk.c205_part_number_id(+)
	 and t205.c205_part_number_id = Repackage.c205_part_number_id(+)
	 and t205.c205_part_number_id = FG.c205_part_number_id(+)
	 and t205.c205_part_number_id = Returns.c205_part_number_id(+)
	 and t2023.c205_part_number_id = t205.c205_part_number_id
	 and t2023.c2023_void_fl is null
 	 AND t2023.C1900_COMPANY_ID = 1000;