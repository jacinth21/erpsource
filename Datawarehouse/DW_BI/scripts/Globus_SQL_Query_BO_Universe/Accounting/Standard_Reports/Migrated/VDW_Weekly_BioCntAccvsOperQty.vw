-- @"D:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Standard_Reports\Migrated\VDW_Weekly_BioCntAccvsOperQty.vw";

CREATE OR REPLACE VIEW VDW_Weekly_BioCntAccvsOperQty AS
		SELECT * FROM(
			SELECT T205.C205_PART_NUMBER_ID PARTID, T205.C205_PART_NUM_DESC PARTDESC, OPERVALUE.C205_AVAILABLE_QTY AVAILATY,  COGSVALUE.account_qty COSGQTY FROM 
				T205_PART_NUMBER T205, T2023_PART_COMPANY_MAPPING t2023, (SELECT t205c.C205_PART_NUMBER_ID , t205c.C205_AVAILABLE_QTY 
				FROM t205c_part_qty t205c
			WHERE C901_TRANSACTION_TYPE = 90802
			AND t205c.c5040_plant_id    =3000) OPERVALUE, 
				(SELECT   x.c205_part_number_id, SUM (NVL (x.c820_qty_on_hand, 0)) account_qty
		             FROM t820_costing x
	    	         WHERE x.c901_status IN (4800, 4801) AND x.c901_costing_type = 4909
	        	  GROUP BY x.c205_part_number_id
			) COGSVALUE
			WHERE  T205.C205_PART_NUMBER_ID = OPERVALUE.C205_PART_NUMBER_ID (+)
			AND T205.C205_PART_NUMBER_ID = COGSVALUE.C205_PART_NUMBER_ID (+)     
			and t2023.c205_part_number_id = t205.c205_part_number_id
			and t2023.c2023_void_fl is null
			AND t2023.C1900_COMPANY_ID = 1000
		)
		WHERE (AVAILATY IS NOT NULL AND COSGQTY IS NOT NULL);


