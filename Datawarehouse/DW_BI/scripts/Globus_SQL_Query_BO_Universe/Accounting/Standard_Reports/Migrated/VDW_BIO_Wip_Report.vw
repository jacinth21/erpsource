-- @"D:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Standard_Reports\Migrated\VDW_BIO_Wip_Report.vw";

CREATE OR REPLACE VIEW VDW_BIO_Wip_Report AS
	SELECT t408.c408_dhr_id dhr_id, t408.c205_part_number_id partnum,
	       t408.c301_vendor_id vid,
	       globus_app.get_partnum_desc (t408.c205_part_number_id) pdesc,
	       globus_app.get_vendor_name (t408.c301_vendor_id) vname,
	       TO_CHAR (c408_created_date, 'mm/dd/yyyy') cdate,
	       t408.c408_control_number lot_number, t408.c408_qty_received
	manu_qty,TO_CHAR (t408.C408_MANF_DATE) MANF_DATE,
	       t408.c408_qty_inspected qty_accepted,TO_CHAR (t408.C408_INSPECTED_TS)
	inspected_date,
	       t408.c408_qty_rejected rejected_qty,
	       t408.c408_shipped_qty initiated_qty,TO_CHAR (t408.C408_PACKAGED_TS) Packed_Date,
	       ((t405.c405_cost_price)) standard_cost,
	       (CASE
	           WHEN c408_status_fl = 0
	           AND globus_app.get_mf_wo_matreq_status (t408.c408_dhr_id) != 2
	              THEN 'Pending Supply Release'
	           WHEN c408_status_fl = 0
	           AND globus_app.get_mf_wo_matreq_status (t408.c408_dhr_id) = 2
	              THEN 'Pending Supply Recon.'
	           WHEN c408_status_fl = 1
	              THEN 'Pending QC Inspect'
	           --when c408_status_fl =2  then 'Pending Supply Recon.'
	        WHEN c408_status_fl = 3
	              THEN 'Inventory Transfer'
	        END
	       ) status,
	       (SELECT SUM
	                  ( -- globus_app.get_ac_cogs_value (t304.c205_part_number_id,
	                                  --                 4909
	                                  --                )
	                  0 * t304.c304_item_qty
	                  ) COST
	          FROM globus_app.t303_material_request t303,
	               globus_app.t304_material_request_item t304
	         WHERE globus_app.t303.c303_material_request_id =
	                                                 t304.c303_material_request_id
	           AND globus_app.t303.c303_material_request_id IN (
	                                 SELECT t303.c303_material_request_id
	                                                                     matreqid
	                                   FROM globus_app.t303_material_request t303
	                                  WHERE t303.c408_dhr_id = t408.c408_dhr_id))
	                                                                         COST
	  FROM globus_app.t408_dhr t408,
	       globus_app.t402_work_order t402,
	       globus_app.t401_purchase_order t401,
	       globus_app.t405_vendor_pricing_details t405
	 WHERE c408_status_fl < 4
	   AND t408.c408_void_fl IS NULL
	   AND t408.c1900_company_id  =1000
	   AND t408.c5040_plant_id    =3000
	   AND t402.c402_work_order_id = t408.c402_work_order_id
	   AND t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
	   AND t408.c205_part_number_id = t405.c205_part_number_id
	   AND t401.c401_type IN (3104, 3105);


