-- @"D:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Standard_Reports\Migrated\VDW_Acc_vs_Oper_Rework_Inv_Rpt.vw";

CREATE OR REPLACE VIEW VDW_Acc_vs_Oper_Rework_Inv_Rpt AS
	SELECT icogg.c205_part_number_id pnum, nvl(tinv.pdesc, GET_PARTNUM_DESC(icogg.c205_part_number_id)) pdesc, NVL (inv_value, 0) inv_value, NVL (cogs_value, 0) cogs_value, (nvl(inv_value,0) - nvl(cogs_value,0) ) diff  
	  FROM ( SELECT	 t402.c205_part_number_id pnum, get_partnum_desc (t402.c205_part_number_id) pdesc
		   , SUM (get_wo_pend_qty (t402.c402_work_order_id, t402.c402_qty_ordered)) inv_value
		FROM t401_purchase_order t401, t402_work_order t402
	   WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
		 AND t401.c401_type = 3101
		 AND t401.c401_void_fl IS NULL
		 AND t402.c402_void_fl IS NULL
		 AND t401.c1900_company_id  =1000
	                 AND t402.c402_status_fl < 3 
	GROUP BY t402.c205_part_number_id  ) tinv
	     , (SELECT   c205_part_number_id, SUM (c820_qty_on_hand) cogs_value
	            FROM t820_costing
	           WHERE c901_costing_type = 4905 AND c901_status <> 4802
	        GROUP BY c205_part_number_id) icogg
	 WHERE tinv.pnum(+) = icogg.c205_part_number_id
	 ORDER BY icogg.c205_part_number_id;


