-- @"D:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Standard_Reports\Migrated\VDW_BOM_Pricing_Report.vw";

CREATE OR REPLACE VIEW VDW_BOM_Pricing_Report AS
	SELECT   t207.c207_set_id bomid, t207.c207_set_nm bomname,  t208.c205_part_number_id partnum
	       , get_partnum_desc (t208.c205_part_number_id) partdesc, NVL (t208.c208_set_qty, 0) bomqty
	,  0 AS cogprice --NVL (get_ac_cogs_value (t208.c205_part_number_id, 4901), 0) cogprice
	    FROM t207_set_master t207, t208_set_details t208
	   WHERE t207.c901_set_grp_type = '1601'
	     AND t208.c207_set_id = t207.c207_set_id
	     AND t208.C208_INSET_FL = 'Y'
	     AND t207.c207_void_fl IS NULL
	     AND t208.c208_void_fl IS NULL
		 AND t207.c207_set_id LIKE '%BM' 
	--	 AND t207.c207_set_id = '910.901BM'
		 --AND t208.c205_part_number_id  = '110.228' 
	ORDER BY t207.c207_set_id, t208.c205_part_number_id,
		t208.c208_critical_fl DESC,
		t208.c208_inset_fl DESC;


   


