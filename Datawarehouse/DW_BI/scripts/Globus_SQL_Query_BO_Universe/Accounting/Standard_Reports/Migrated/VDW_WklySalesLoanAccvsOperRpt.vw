-- @"D:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Standard_Reports\Migrated\VDW_WklySalesLoanAccvsOperRpt.vw";

CREATE OR REPLACE VIEW VDW_WklySalesLoanAccvsOperRpt AS
	SELECT Pnum.C205_PART_NUMBER_ID pnum, Pnum.C205_PART_NUM_DESC pdesc, nvl(oper_qty,0) operqty, nvl(account_qty,0) acctqty
	  ,(nvl(oper_qty,0) - nvl(account_qty,0) ) diff
	FROM T205_PART_NUMBER Pnum , T2023_PART_COMPANY_MAPPING t2023, 
	(SELECT   t505.c205_part_number_id p_num, SUM (t505.c505_item_qty) oper_qty
	          FROM t505_item_consignment t505, T504_CONSIGNMENT t504      
	          WHERE t505.C504_CONSIGNMENT_ID = t504.C504_CONSIGNMENT_ID
	          AND t504.C504_TYPE = 4127
	          AND t504.C504_VOID_FL IS NULL
              AND t504.c1900_company_id  =1000
			  AND t504.c5040_plant_id    =3000			  
	          GROUP BY t505.c205_part_number_id 
	) operqty , 
	( SELECT X.C205_PART_NUMBER_ID , SUM(NVL(X.C820_QTY_ON_HAND ,0)) account_qty 
			FROM T820_COSTING X  
			WHERE X.C901_STATUS IN (4800,4801)
			AND X.C901_COSTING_TYPE = 4906
			GROUP BY X.C205_PART_NUMBER_ID
	) accountqty
	WHERE Pnum.C205_PART_NUMBER_ID = operqty.p_num (+)    
	AND  Pnum.C205_PART_NUMBER_ID = accountqty.C205_PART_NUMBER_ID (+)
	and t2023.c205_part_number_id = pnum.c205_part_number_id
	and t2023.c2023_void_fl is null
	AND t2023.C1900_COMPANY_ID = 1000
	AND EXISTS(
										 SELECT t205c.c205_part_number_id
										   FROM t205c_part_qty t205c
										  WHERE t205c.c205_part_number_id   = pnum.c205_part_number_id
											AND t205c.c205_available_qty   IS NOT NULL
											AND t205c.c901_transaction_type = '90800' --FG qty
											AND t205c.c5040_plant_id    =3000
									)
	AND INSTR(Pnum.C205_PART_NUMBER_ID,'.',5,1) = 0
	ORDER BY Pnum.C205_PART_NUMBER_ID;


