SELECT t501.c704_account_id AS Account_Id,
  t704.C704_ACCOUNT_NM AS Account_Name,
  t501.c501_order_id AS Order_Id ,
  t703.C703_SALES_REP_NAME  AS Rep_Name,
  t501.c501_order_date AS Order_Date ,
  t701.C701_DISTRIBUTOR_NAME  AS Distributor_Name,
  v700.region_name Region_Name,
  -- PC-272: Updated Revenue Tracking information in BO
  -- To get the Rev. information from new table)
  NVL (get_code_name (t5003.c901_po_dtls), '') AS Hardcopy_PO,
  NVL (get_code_name (t5003.c901_do_dtls), '') AS Signed_DO,
  NVL (t5003.c5003_do_created_date, t5003.c5003_po_created_date) AS Updated_Date,
  --
  to_number(DECODE (DECODE(t704d.C901_ADMIN_FLAG,7003,GET_ACCOUNT_GPO_ID (t501.C704_ACCOUNT_ID),7011,NVL(t704d.c101_gpo,0),0),0, GET_TOTAL_DO_AMT(t501.C501_ORDER_ID, ''),DECODE(t704d.C704D_ADMIN_FEE,NULL,GET_TOTAL_DO_AMT (t501.C501_ORDER_ID, ''), GET_TOTAL_EMAIL_DO_AMT (t501.C501_ORDER_ID)))) AS Total_DO_Amount,
  '' AS Check_Flag ,
  t503.c503_invoice_id AS Invoice_Id,
  t503.c503_invoice_date AS Invoice_Date ,
  NVL(GET_INVOICE_PAID_AMOUNT(t503.c503_invoice_id),0) AS Received_Amount,
  DECODE(t503.c503_status_fl,2,'Closed','Pending') AS Status ,
  get_code_name(t501.c501_receive_mode) AS Mode_Of_Order,
  T501.C501_CUSTOMER_PO Customer_PO,
  NVL(T501.C501_PO_AMOUNT,0) AS PO_Amount ,
  get_code_name (t501.c901_contract) AS Contract_flag,
  get_company_name(T501.c1900_company_id) AS Company_Name
FROM t501_order t501 ,
  t704_account t704,
  t704d_account_affln t704d , 
  -- PC-272: Updated Revenue Tracking information in BO
  t5003_order_revenue_sample_dtls t5003,
  t703_sales_rep t703 ,
  t701_distributor t701 ,
  (SELECT DISTINCT ad_id,
    ad_name,
    region_id,
    region_name,
    d_id,
    d_name,
    rep_id
  FROM v700_territory_mapping_detail
  ) v700,
  t503_invoice t503
WHERE t501.c703_sales_rep_id   = t703.c703_sales_rep_id
--AND t704.C901_CURRENCY         in ('1')
AND T501.c1900_company_id      =1000
AND T501.C704_ACCOUNT_ID       = t704.C704_ACCOUNT_ID
AND t703.c701_distributor_id   = t701.c701_distributor_id
AND t501.c703_sales_rep_id     = v700.rep_id
AND t501.c501_order_id         = t5003.c501_order_id (+)
AND t501.c503_invoice_id       = t503.c503_invoice_id(+)
AND t501.C704_ACCOUNT_ID       = t704d.C704_ACCOUNT_ID (+)
AND t704d.C704D_VOID_FL (+)   IS NULL
AND t501.c501_parent_order_id IS NULL
AND t501.c501_void_fl         IS NULL
AND t701.c701_void_fl         IS NULL
AND t703.c703_void_fl         IS NULL
AND c501_delete_fl            IS NULL
AND (t701.c901_ext_country_id IS NULL
OR t701.c901_ext_country_id   IN
  (SELECT country_id FROM v901_country_codes
  ))
AND (t703.c901_ext_country_id IS NULL
OR t703.c901_ext_country_id   IN
  (SELECT country_id FROM v901_country_codes
  ))
AND (t501.c901_ext_country_id IS NULL
OR t501.c901_ext_country_id   IN
  (SELECT country_id FROM v901_country_codes
  ))
AND NVL(t501.c901_order_type,0)      <> 2533
AND NVL (c901_order_type, -9999) NOT IN
  (SELECT t906.c906_rule_value
  FROM t906_rules t906
  WHERE t906.c906_rule_grp_id = 'ORDTYPE'
  AND c906_rule_id            = 'EXCLUDE'
  )
AND t501.c501_order_date   >=  trunc (to_date(@prompt('From_Order_Date','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss'))
AND t501.c501_order_date   <=  trunc (to_date(@prompt('To_Order_Date','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss'))
AND T501.C703_SALES_REP_ID IN
  (SELECT DISTINCT REP_ID
  FROM V700_TERRITORY_MAPPING_DETAIL
  WHERE REP_COMPID NOT IN
    (SELECT C906_RULE_ID
    FROM T906_RULES
    WHERE C906_RULE_GRP_ID = 'ADDNL_COMP_IN_SALES'
    AND C906_VOID_FL      IS NULL
    )
  )
AND t501.c704_account_id IN
  (SELECT ac_id
  FROM v700_territory_mapping_detail
  WHERE (COMPID                = DECODE(100800,100803, NULL,100800)
  OR DECODE(100800,100803,1,2) = 1)
  )
AND t501.c703_sales_rep_id IN
  (SELECT REP_ID FROM v700_territory_mapping_detail WHERE divid =100823
  );
