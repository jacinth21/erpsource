-- @"C:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Standard_Reports\VDW_Inventory_Not_Inspected.vw";
CREATE OR REPLACE VIEW VDW_Inventory_Not_Inspected
AS
SELECT     *
    FROM
        (SELECT     pnum                                                                                       , pdesc         , txn_id
              , wo_id                                                                                          , po_id         , qty_received
              , received_date                                                                                  , qty_rejected  , rejected_date
              , closeout_qty                                                                                   , close_out_date, (NVL (qty_to_split, 0)) qty_to_split
              , verified_date                                                                                  , vendor_name   , potype
              , uom                                                                                            , DECODE (potypeid, '3101', 0 --GET_AC_COGS_VALUE (pnum, 4908)
			  , TO_NUMBER ( (
                get_currency_conversion (get_vendor_currency (vendor_id), 1, SYSDATE, cost_price)))) cost_price, DECODE
                (potypeid, '3101', 0 --GET_AC_COGS_VALUE (pnum, 4908)
				, TO_NUMBER ( (get_currency_conversion (
                get_vendor_currency (vendor_id), 1, SYSDATE, unit_price)))) unit_price, ( (NVL (qty_to_split, 0)) * NVL
                (DECODE (potypeid, '3101', 0 --GET_AC_COGS_VALUE (pnum, 4908)
				, TO_NUMBER ( (get_currency_conversion (
                get_vendor_currency (vendor_id), 1, SYSDATE, unit_price)))), 0)) extended_price
                /*    ,TO_NUMBER ((get_currency_conversion (get_vendor_currency (vendor_id), 1, SYSDATE, cost_price))
                )cost_price
                , TO_NUMBER ((get_currency_conversion (get_vendor_currency (vendor_id), 1, SYSDATE, unit_price))
                ) unit_price
                , (  (NVL (qty_to_split, 0))
                * NVL (get_currency_conversion (get_vendor_currency (vendor_id), 1, SYSDATE, unit_price), 0)
                ) extended_price*/
            FROM (
                (SELECT     t408.c205_part_number_id pnum                                                        , get_partnum_desc (t408.c205_part_number_id) pdesc,
                        t408.c408_dhr_id txn_id                                                                  , t402.c402_work_order_id wo_id                    ,
                        t402.c401_purchase_ord_id po_id                                                          , NVL (t408.c408_qty_received, 0) qty_received     , NVL (
                        t408.c408_received_date, '') received_date                                               , NVL (t408.c408_qty_rejected, 0) qty_rejected     ,
                        NVL (t409.c409_eval_date, '') rejected_date                                              , NVL (t409.c409_closeout_qty, 0) closeout_qty     ,
                        NVL (t409.c409_closeout_date, '') close_out_date                                         , NVL (ihqty, 0) qty_to_split                      ,
                        NVL (TO_CHAR (t408.c408_verified_ts, 'MM/DD/YYYY'), 'DHR not Verified Yet') verified_date,
                        get_vendor_name (t408.c301_vendor_id) vendor_name                                        ,
                        get_code_name (t401.c401_type) potype                                                    ,
                        t405.c405_uom_qty uom                                                                    ,
                        t401.c401_type potypeid                                                                  , (
                            CASE
                                WHEN t401.c401_type IN (3104, 3105)
                                THEN (NVL (t205.c205_cost, DECODE (t408.c901_type, 80191, t402.c402_split_cost, DECODE
                                    (t402.c402_rm_inv_fl, 'Y', t402.c402_cost_price - t402.c402_split_cost,
                                    t402.c402_cost_price))))
                                ELSE (NVL (DECODE (t401.c401_type, 3104, t205.c205_cost, t402.c402_cost_price),
                                    t402.c402_cost_price))
                            END) cost_price, (
                            CASE
                                WHEN t401.c401_type IN (3104, 3105)
                                THEN (NVL (t205.c205_cost, DECODE (t408.c901_type, 80191, t402.c402_split_cost, DECODE
                                    (t402.c402_rm_inv_fl, 'Y', t402.c402_cost_price - t402.c402_split_cost,
                                    t402.c402_cost_price))) / t405.c405_uom_qty)
                                ELSE ( (NVL (DECODE (t401.c401_type, 3104, t205.c205_cost, t402.c402_cost_price),
                                    t402.c402_cost_price)) / t405.c405_uom_qty)
                            END) unit_price, t401.c301_vendor_id vendor_id
                    FROM
                        (SELECT     *
                            FROM t408_dhr t408
                            WHERE t408.c408_status_fl = 4
                                AND t408.c1900_company_id = 1000
                                AND t408.c5040_plant_id = 3000
                        ) t408                                 , t402_work_order t402    , t405_vendor_pricing_details t405
                      , t409_ncmr t409                         , t401_purchase_order t401, t205_part_number t205
                      , (SELECT     t412.c412_ref_id           , SUM (t413.c413_item_qty) ihqty
                            FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
                            WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
                                AND t412.c412_void_fl IS NULL
                                AND t412.c412_ref_id IS NOT NULL
                                AND t412.c412_status_fl < 4
                                AND t412.c412_inhouse_purpose = 41278
                                AND T413.C413_VOID_FL IS NULL
                                AND t412.c1900_company_id = 1000
                                AND t412.c5040_plant_id = 3000
                            GROUP BY c412_ref_id
                        ) t412
                    WHERE t408.c402_work_order_id = t402.c402_work_order_id
                        AND t402.c401_purchase_ord_id = t401.c401_purchase_ord_id
                        AND t408.c408_dhr_id = t409.c408_dhr_id(+)
                        AND t408.c408_dhr_id = t412.c412_ref_id
                        AND t408.c205_part_number_id = t205.c205_part_number_id
                        AND t405.c405_pricing_id = t402.c405_pricing_id
                        AND t402.c402_void_fl IS NULL
                        AND t408.c408_void_fl IS NULL
                        AND t409.c409_void_fl IS NULL
                        AND t401.c401_void_fl IS NULL
                )
            UNION
                    (SELECT     t408.c205_part_number_id pnum               , get_partnum_desc (t408.c205_part_number_id) pdesc,
                            t408.c408_dhr_id txn_id                         , t402.c402_work_order_id wo_id                    ,
                            t402.c401_purchase_ord_id po_id                 , NVL (t408.c408_qty_received, 0) qty_received     , NVL (
                            t408.c408_received_date, '') received_date      , NVL (t408.c408_qty_rejected, 0) qty_rejected     ,
                            NVL (t409.c409_eval_date, '') rejected_date     , NVL (t409.c409_closeout_qty, 0) closeout_qty     ,
                            NVL (t409.c409_closeout_date, '') close_out_date, (
                                CASE
                                    WHEN
                                        (
                                            t401.c401_type IN (3104, 3105)
                                            AND t408.c408_status_fl < 1
                                        )
                                    THEN 0
                                    ELSE (NVL (t408.c408_qty_received, 0) - (
                                            CASE
                                                WHEN t409.c409_status_fl >= 2
                                                THEN NVL (t408.c408_qty_rejected, 0)
                                                ELSE 0
                                            END) + NVL (t409.c409_closeout_qty, 0))
                                END) qty_to_split                , NVL (TO_CHAR (t408.c408_verified_ts, 'MM/DD/YYYY'),
                            'DHR not Verified Yet') verified_date, get_vendor_name (t408.c301_vendor_id) vendor_name,
                            get_code_name (t401.c401_type) potype, t405.c405_uom_qty uom                            ,
                            t401.c401_type potypeid              , (
                                CASE
                                    WHEN t401.c401_type IN (3104, 3105)
                                    THEN (NVL (t205.c205_cost, DECODE (t408.c901_type, 80191, t402.c402_split_cost,
                                        DECODE (t402.c402_rm_inv_fl, 'Y', t402.c402_cost_price - t402.c402_split_cost,
                                        t402.c402_cost_price))))
                                    ELSE (NVL (DECODE (t401.c401_type, 3104, t205.c205_cost, t402.c402_cost_price),
                                        t402.c402_cost_price))
                                END) cost_price, (
                                CASE
                                    WHEN t401.c401_type IN (3104, 3105)
                                    THEN (NVL (t205.c205_cost, DECODE (t408.c901_type, 80191, t402.c402_split_cost,
                                        DECODE (t402.c402_rm_inv_fl, 'Y', t402.c402_cost_price - t402.c402_split_cost,
                                        t402.c402_cost_price))) / t405.c405_uom_qty)
                                    ELSE ( (NVL (DECODE (t401.c401_type, 3104, t205.c205_cost, t402.c402_cost_price),
                                        t402.c402_cost_price)) / t405.c405_uom_qty)
                                END) unit_price, t401.c301_vendor_id vendor_id
                        FROM
                            (SELECT     *
                                FROM t408_dhr t408
                                WHERE t408.c408_status_fl < 4
                                    AND t408.c1900_company_id = 1000
                                    AND t408.c5040_plant_id = 3000
                            ) t408        , t402_work_order t402    , t405_vendor_pricing_details t405
                          , t409_ncmr t409, t401_purchase_order t401, t205_part_number t205
                        WHERE t408.c402_work_order_id = t402.c402_work_order_id
                            AND t402.c401_purchase_ord_id = t401.c401_purchase_ord_id
                            AND t408.c408_dhr_id = t409.c408_dhr_id(+)
                            AND t408.c205_part_number_id = t205.c205_part_number_id
                            AND t405.c405_pricing_id = t402.c405_pricing_id
                            AND t402.c402_void_fl IS NULL
                            AND t408.c408_void_fl IS NULL
                            AND t409.c409_void_fl IS NULL
                            AND t401.c401_void_fl IS NULL
                    ))
        )
    WHERE qty_to_split > 0;