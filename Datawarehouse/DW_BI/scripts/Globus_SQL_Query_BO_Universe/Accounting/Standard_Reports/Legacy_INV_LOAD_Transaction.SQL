SELECT c701_distributor_nm,
    ce5054_trans_dt,
    c205_part_number_id,
	c205_part_desc,
    ce5054_orig_qty,
    ce5054_txn_qty,
    ce5054_new_qty,
    ce5054_txn_id ,
    ce5054_posted_by ,
    c701_ext_dist_id,
    c1900_company_id,
    c1900_company_name
FROM tb5054_inv_load_legacy
   