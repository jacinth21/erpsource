SELECT Inhouse_Trans_Id, Transaction_Comments, Inhouse_trans_Status,  Release_To_Employee, Release_To_Rep_Name, Inhouse_Trans_Purpose_Name,Inhouse_Trans_Type_Name,Update_Inv_Flag,
Verified_Date, Verified_By, Verify_Flag, Created_By, Created_Date, Last_Updated_Date, Last_Updated_By, Ref_Id, Loaner_Transaction_Id, Expected_Return_Date, Company_Name
FROM
(SELECT c412_inhouse_trans_id inhouse_trans_id ,
    c412_comments Transaction_Comments ,
    DECODE(c412_status_fl, 0,'Initiated/Back Order',2,'Pending Control',3,'Pending Verification', '3.30','Packing In Progress','3.60','Ready For Pickup',4,'Completed') inhouse_trans_status ,
    --c412_void_fl void_flag ,
    DECODE(c412_release_to,50625,t101_release_to_employee.c101_user_f_name
    ||' '
    ||t101_release_to_employee.c101_user_l_name,0,t101_release_to_employee.c101_user_f_name
    ||' '
    ||t101_release_to_employee.c101_user_l_name) release_to_employee ,
    DECODE(c412_release_to,4121,get_rep_name(c412_release_to_id)) release_to_rep_name ,
    NVL(t901_inhouse_trans_purpose.c901_code_nm,' ') inhouse_trans_purpose_name ,
    NVL(t901_inhouse_trans_type.c901_code_nm,' ') inhouse_trans_type_name ,
    c412_update_inv_fl update_inv_flag ,
    c412_verified_date verified_date ,
    NVL(t101_verified_by.c101_user_f_name
    ||' '
    ||t101_verified_by.c101_user_l_name,' ') verified_by ,
    c412_verify_fl verify_flag ,
    NVL(t101_created_by.c101_user_f_name
    ||' '
    ||t101_created_by.c101_user_l_name,' ') created_by ,
    c412_created_date created_date ,
    c412_last_updated_date last_updated_date ,
    NVL(t101_last_updated_by.c101_user_f_name
    ||' '
    ||t101_last_updated_by.c101_user_l_name,' ') last_updated_by ,
    c412_ref_id ref_id ,
    c504a_loaner_transaction_id loaner_transaction_id ,
    c412_expected_return_date expected_return_date,
    company.c1900_company_name Company_Name
  FROM t412_inhouse_transactions t412,
    t101_user t101_release_to_employee,
    t101_user t101_verified_by,
    t101_user t101_created_by,
    t101_user t101_last_updated_by,
    t901_code_lookup t901_inhouse_trans_purpose ,
    t901_code_lookup t901_inhouse_trans_type,
    t1900_company company
  WHERE t412.c1900_company_id <> 1000 -- 1000:globus north america
    AND c412_release_to_id      =t101_release_to_employee.c101_user_id(+)
    AND t412.c412_verified_by        =t101_verified_by.c101_user_id(+)
    AND t412.c412_created_by         =t101_created_by.c101_user_id(+)
    AND t412.c412_last_updated_by    =t101_last_updated_by.c101_user_id(+)
    AND t412. c412_inhouse_purpose    =t901_inhouse_trans_purpose.c901_code_id(+)
    AND t412.c412_type               =t901_inhouse_trans_type.c901_code_id(+)
    AND t412.c1900_company_id        =company.c1900_company_id(+)
    AND t412.C412_CREATED_DATE   >=  trunc (to_date(@prompt('Transaction Date From','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss'))
    AND  t412.C412_CREATED_DATE   <=  trunc (to_date(@prompt('Transaction Date To','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss'))
    AND  t412.C412_VOID_FL IS NULL
  UNION ALL
  SELECT T504.C504_CONSIGNMENT_ID Inhouse_Trans_Id
    , T504.C504_COMMENTS Transaction_Comments
    , DECODE (t504.c504_status_fl,'0', 'Initiated', '1', 'WIP', '1.10','Completed set', '1.20', 'Pending Putaway','2', 'Built set','2.20','Pending Pick', '3', 'Pending Shipping', '3.30', 'Packing In Progress', '3.60', 'Ready For Pickup',4, 'shipped',6,'Transfer Not Verified', t504.c504_status_fl) Inhouse_trans_Status
    --, T504.C504_VOID_FL Void_Flag
    , NULL Release_To_Employee
    , NULL Release_To_Rep_Name
    , NVL(t901_consignment_inhouse.c901_code_nm,' ') Inhouse_Trans_Purpose_Name
    , NVL(t901_consignment_type_name.c901_code_nm,' ') Inhouse_Trans_Type_Name
    , NULL Update_Inv_Flag
    , T504.C504_VERIFIED_DATE Verified_Date
    , NVL(t101_consignment_verified_by.c101_user_f_name
    ||' '
    ||t101_consignment_verified_by.c101_user_l_name,' ') Verified_By
    , T504.c504_verify_fl Verify_Flag
    , NVL(t101_consignment_created_by.c101_user_f_name
    ||' '
    ||t101_consignment_created_by.c101_user_l_name,' ') Created_By
    , T504.C504_CREATED_DATE Created_Date
    , T504.C504_LAST_UPDATED_DATE Last_Updated_Date
    , NVL(t101_consignment_updated_by.c101_user_f_name
    ||' '
    ||t101_consignment_updated_by.c101_user_l_name,' ') Last_Updated_By
    , T504.C504_MASTER_CONSIGNMENT_ID Ref_Id
    , NULL Loaner_Transaction_Id
    , NULL Expected_Return_Date
    , company.c1900_company_name Company_Name
    FROM t504_consignment t504,
    t101_user t101_consignment_created_by,
    t101_user t101_consignment_updated_by,
    t101_user t101_consignment_verified_by ,
    t901_code_lookup t901_consignment_inhouse,
    t901_code_lookup t901_consignment_type_name,
    t1900_company company
    WHERE  t504.C1900_COMPANY_ID <> 1000 -- 1000:Globus North America
      AND t504.c504_created_by     =t101_consignment_created_by.c101_user_id(+)
      AND t504.c504_last_updated_by=t101_consignment_updated_by.c101_user_id(+)
      AND t504.c504_verified_by    =t101_consignment_verified_by.c101_user_id(+)
      AND t504.c504_inhouse_purpose=t901_consignment_inhouse.c901_code_id(+)
      AND t504.c504_type           =t901_consignment_type_name.c901_code_id(+)
      AND t504.c1900_company_id         =company.c1900_company_id(+)
      AND t504.C504_TYPE IN (4113,4114,4115,4116,9110,56028,104622,400058,400059,400060,400062,400063,400064,400076,400086)
      AND t504.C504_CREATED_DATE  >=  trunc (to_date(@prompt('Transaction Date From','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss'))
      AND t504.C504_CREATED_DATE   <=  trunc (to_date(@prompt('Transaction Date To','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss'))
      AND T504.C504_VOID_FL IS NULL
    -- 4113:shelf to quarantine,4114:shelf to repack,4115:quarantine to scrap,4116:quarantine to shelf,9110:ih loaner item,
    -- 56028:ps to rw inventory,104622:qty to quarantine,400058:returns hold to repack,400059:bulk to quarantine,
    -- 400060:bulk to repack,400062:bulk to inv adjustment,400063:repack to shelf,400064:returns hold to quarantine,
    -- 400076:quarantine to bulk,400086:shelf to returns
    -- consignment_master_dim is already filtered on c1900_company_id = 1000
  )