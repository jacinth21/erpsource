SELECT t504.c504_consignment_id As Consignment_Id,
c504a_etch_id As etch_id, 
t504.c207_set_id As setid,
get_set_name (t504.c207_set_id) As setname ,
c907_created_date As TRANSDT, 
get_code_name (t907.c901_cancel_cd) As TRNSREASON,
t504.c504_comments As Comments
           FROM t504a_consignment_loaner t504a, t504_consignment t504 ,t907_cancel_log t907 
          WHERE t504.c504_consignment_id = t504a.c504_consignment_id
            AND t504a.c504a_status_fl = 60
            AND t907.c907_ref_id = t504a.c504_consignment_id
            AND c504a_void_fl IS NULL
           AND t907.c907_void_fl IS NULL
           AND t907.c901_type = 93076