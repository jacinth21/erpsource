-- WIP Consignments

 SELECT wipinv.c504_consignment_id as "Consignment ID"
, wipinv.c701_distributor_id as "Distributor ID"
, get_distributor_name(wipinv.c701_distributor_id) as "Distributor Name"
, wipinv.c504_status_fl as "Status Flag"
, wipinv.c504_void_fl as "Void Flag"
, wipinv.c704_account_id as "Account ID"
, wipinv.c207_set_id as  "Set ID"
, wipinv.c504_type as "Type"
, wipinv.c504_verify_fl as "Verify Flag"
, wipinv.c504_verified_date as "Verified Date"
, wipinv.c504_verified_by as "Verified By"
, wipinv.c520_request_id as "Request ID"
, wipinv.c504_master_consignment_id as "Master Consignment ID"
, wipinv.c504_last_updated_by as "Last Updated By"
, get_user_name(wipinv.c504_last_updated_by) as "Last Updated User"
, wipinv.c504_last_updated_date as "Last Updated Date"
, 20467 as "Location Type"
, NVL(taggedcns.invtagid,get_tagid_from_txn_id(wipinv.c504_consignment_id)) as "Inv Tag ID"
, taggedcns.invlocid as "Inv Loc ID"
, (select c5052_location_cd from t5052_location_master t5052 where t5052.c5052_location_id = taggedcns.invlocid) as "Inv Loc Name"
, get_company_name(wipinv.c1900_company_id) as "Company Name"
, get_plant_name(wipinv.C5040_PLANT_ID) as "Plant Name"
FROM 
 ( 
 SELECT t504.c504_consignment_id, t504.c701_distributor_id, t504.c504_status_fl 
 , t504.c504_void_fl, t504.c704_account_id, t504.c207_set_id 
 , t504.c504_type, t504.c504_verify_fl, t504.c504_verified_date 
 , t504.c504_verified_by, t504.c520_request_id, t504.c504_master_consignment_id 
 , t504.c504_last_updated_by, t504.c504_last_updated_date 
 ,t504.c1900_company_id, t504.c5040_plant_id
 FROM t504_consignment t504, t906_rules t906 
 WHERE t504.c504_status_fl = t906.c906_rule_value --IN ('1','1.10','1.20') -- 1 WIP status /1.10 Completed Set 
 -- status/1.20 Pending Put status 
 AND t504.c504_verify_fl = '0' 
 AND t504.c504_void_fl IS NULL 
 AND t504.c207_set_id IS NOT NULL 
 AND t906.c906_rule_id = 'PI_WIPSETSTATUS' 
 AND t906.c906_rule_grp_id = 'PI_LOCK' 
 AND t906.c906_void_fl IS NULL 
 --AND c504_consignment_id = 'GM-CN-1143837' 
 ) 
 wipinv, 
( 
 SELECT T5010.c5010_last_updated_trans_id lastupdtransid, T5053.C5010_TAG_ID invtagid, t5053.c5052_location_id 
 invlocid 
 FROM t5010_tag t5010, t5051_inv_warehouse t5051, t5052_location_master t5052 
 , t5053_location_part_mapping T5053 
 WHERE t5010.c5010_tag_id = t5053.c5010_tag_id 
 AND t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id 
 AND t5052.c5052_location_id = t5053.c5052_location_id 
 AND T5052.c901_location_type IN (93501) --Consignment 
 AND t5052.c5052_void_fl IS NULL 
 AND t5010.c5010_void_fl IS NULL 
 AND t5051.c901_status_id = 1 
 AND t5051.c901_warehouse_type = 90800 --FG Warehouse 
 ) 
 taggedcns 
 WHERE wipinv.c504_consignment_id = taggedcns.lastupdtransid(+)