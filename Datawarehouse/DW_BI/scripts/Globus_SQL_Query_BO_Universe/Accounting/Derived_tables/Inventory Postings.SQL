 
 SELECT t810.C205_PART_NUMBER_ID AS "Part Number"
  , t205.C205_PART_NUM_DESC AS "Part Number Description"
  , t202.c202_project_nm AS "Project Name"
  , get_code_name( t205.C205_PRODUCT_FAMILY) AS "Part Family"
  , t810.C810_QTY AS "QTY"
  , t5040.C5040_PLANT_NAME AS "Plant Name"
  , t1900_owner.C1900_COMPANY_NAME AS "Owner Company Name"
  , t1900_txn_comp.C1900_COMPANY_NAME AS "Transactions Company Name"
  , t1900_dest_comp.C1900_COMPANY_NAME AS "Destination Company Name"
  , t801.C801_NAME AS "Account Name", t901_acc.C901_CODE_NM AS "Txn Type"
  , t810.C810_TXN_ID AS "Txn ID"
  , TO_CHAR (t810.C810_TXN_DATE, 'MM/dd/yyyy') AS "Txn Date (MM/dd/yyyy)"
  , TO_CHAR (t810.C810_CREATED_DATE, 'hh24:mi AM') AS "Txn Time"
  , t101.C101_USER_F_NAME ||' '|| t101.C101_USER_L_NAME AS "Created By"
  , DECODE (t810.C810_DR_AMT, 0, 0, NVL (t810.C810_DR_AMT * t810.C810_QTY, 0)) AS "DR Amount"
  , DECODE (t810.C810_CR_AMT, 0, 0, NVL (t810.C810_CR_AMT * t810.C810_QTY, 0)) AS "CR Amount"
  , t901_own_curr.C902_CODE_NM_ALT AS "Owner Currency"
  , NVL (C810_local_company_DR_AMT * t810.C810_QTY, 0) AS "Local DR Amount"
  , NVL (C810_local_company_CR_AMT * t810.C810_QTY, 0) AS "Local CR Amount"
  , t901_loc_curr.C902_CODE_NM_ALT AS "Local Currency"
  , t1900.C1900_COMPANY_NAME AS "Portal Company Name"
  , t810.C810_ACCOUNT_TXN_ID AS "Account Txn ID"
   FROM t810_posting_txn t810, t1900_company t1900
   , T5040_PLANT_MASTER t5040, T1900_COMPANY t1900_owner
   , T901_CODE_LOOKUP t901_loc_curr, T901_CODE_LOOKUP t901_own_curr
  , T101_USER t101, T205_PART_NUMBER t205
  , t202_project t202
  , T901_CODE_LOOKUP t901_acc, T801_ACCOUNT_ELEMENT t801
  , T1900_COMPANY t1900_txn_comp, T1900_COMPANY t1900_dest_comp
  WHERE C810_DELETE_FL                   <> 'Y'
  	AND t202.c202_project_id = t205.c202_project_id
    AND t1900.C1900_COMPANY_ID            = t810.C1900_COMPANY_ID
    AND t810.C5040_PLANT_ID               = t5040.C5040_PLANT_ID (+)
    AND t810.C1900_OWNER_COMPANY_ID       = t1900_owner.c1900_company_id (+)
    AND t810.C901_LOCAL_COMPANY_CURRENCY  = t901_loc_curr.C901_CODE_ID (+)
    AND t810.C901_OWNER_CURRENCY          = t901_own_curr.C901_CODE_ID (+)
    AND t810.c810_created_by              = t101.C101_USER_ID (+)
    AND t810.C205_PART_NUMBER_ID          = t205.C205_PART_NUMBER_ID (+)
    AND t810.C901_ACCOUNT_TXN_TYPE        = t901_acc.C901_CODE_ID
    AND t810.C801_ACCOUNT_ELEMENT_ID      = t801.C801_ACCOUNT_ELEMENT_ID
    AND t810.C1900_TXN_COMPANY_ID         = t1900_txn_comp.C1900_COMPANY_ID (+)
    AND t810.C1900_DESTINATION_COMPANY_ID = t1900_dest_comp.C1900_COMPANY_ID (+)
    AND t1900.c1900_void_fl              IS NULL
    AND t1900_owner.c1900_void_fl        IS NULL
    AND t1900_txn_comp.c1900_void_fl     IS NULL
    AND t1900_dest_comp.c1900_void_fl    IS NULL
    AND t5040.c5040_void_fl           IS NULL
    AND C810_TXN_DATE                    >= to_date (@prompt ('From date', 'D',, mono,, persistent), 'dd-mm-yyyy hh24:mi:ss')
    AND C810_TXN_DATE        <= to_date (@prompt ('To date', 'D',, mono,, persistent), 'dd-mm-yyyy hh24:mi:ss')
    --AND t810.C810_TXN_ID LIKE '%FGQN-74749'
    --AND t810.C810_TXN_ID In( 'GM-MA-12120' )
    --AND t810.C205_PART_NUMBER_ID IN ('124.000')
    --AND t1900.C1900_COMPANY_ID IN (1000, 1001)
ORDER BY t810.C810_ACCOUNT_TXN_ID
