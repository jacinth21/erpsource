--@"D:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\VDW_Invoice_Portal_Price_Comp.sql";

CREATE OR REPLACE VIEW VDW_Invoice_Portal_Price_Comp AS
	SELECT *
	  FROM (SELECT ROWNUM, t501.c704_account_id account_id, get_account_name (t501.c704_account_id) account_name
				 , t501.c503_invoice_id invoice, DECODE (c501_update_inv_fl, '1', 'Pending', '2', 'Closed') invoice_status
				 , t501.c501_order_id order_id, c205_part_number_id part
				 , get_partnum_desc (c205_part_number_id) part_description, c502_item_qty quantity
				 , c502_item_price invoiced_price
				 , get_account_part_pricing (t501.c704_account_id
										   , c205_part_number_id
										   , get_account_gpo_id (t501.c704_account_id)
											) price_on_portal
			   , get_account_part_price_effdate (t501.c704_account_id , c205_part_number_id 
			   , get_account_gpo_id (t501.c704_account_id)) effective_date
			   , trunc(t503.c503_invoice_date) invoicedate
			FROM t502_item_order t502, t501_order t501, t503_invoice t503
			WHERE t501.c501_order_id = t502.c501_order_id
			AND t501.c503_invoice_id = t503.c503_invoice_id
		  --AND trunc(t503.c503_invoice_date)  >= {?From Date}
	      --AND trunc(t503.c503_invoice_date) <= {?To Date}
	        AND t502.c502_delete_fl IS NULL
	 AND NVL (t501.c901_order_type, -9999) NOT IN (2535, 2524,2533))
	 WHERE invoiced_price <> NVL (price_on_portal, 0) 
  	 ORDER BY account_name;


