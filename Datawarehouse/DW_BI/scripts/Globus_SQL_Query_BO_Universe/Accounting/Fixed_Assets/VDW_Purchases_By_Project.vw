-- @"C:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Fixed_Assets\VDW_Purchases_By_Project.vw";

CREATE OR REPLACE VIEW VDW_Purchases_By_Project AS
	SELECT   SUM (NVL (v810.c810_qty, 0) * NVL (v810.c810_dr_amt, 0)) dr_amt
	       , SUM (NVL (v810.c810_qty, 0) * NVL (v810.c810_cr_amt, 0)) cr_amt, MAX(v810.c810_txn_date) txt_date, t202.c202_project_nm
	    FROM v810_posting_txn v810, t205_part_number t205, t202_project t202
	   WHERE t205.c205_part_number_id(+) = v810.c205_part_number_id
	     AND t205.c202_project_id = t202.c202_project_id
	     AND c901_account_txn_type IN ('4810','48131','48132','48133','48134','48135','48141')
	     AND c810_delete_fl <> 'Y'
	    -- AND v810.c810_txn_date >= {From Date}
	    -- AND v810.c810_txn_date <= {To Date}
	     AND v810.c801_account_element_id IN (SELECT t801.c801_account_element_id
	                                            FROM t801_account_element t801
	                                           WHERE t801.c800_account_type_id = 3)
	GROUP BY t202.c202_project_nm
	ORDER BY t202.c202_project_nm;