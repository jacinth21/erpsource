-- @"D:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Fixed_Assets\VDW_Purchase_By_Proj_Detail.vw";

CREATE OR REPLACE VIEW VDW_Purchase_By_Proj_Detail AS
	SELECT  t202.C202_PROJECT_ID projectid, t202.C202_PROJECT_NM projectname, t205.c205_part_number_id pnum
		  ,c205_part_num_desc pdesc ,  v810.c810_account_txn_id acttxnid, trunc (c810_txn_date) txndate
	      , t801.C801_DESC account_name
	       , c810_desc txndesc, TO_CHAR (c810_acct_date, 'mm/dd/yyyy') actdate
	       , DECODE (c810_dr_amt, 0, 0, NVL (c810_dr_amt * c810_qty, 0)) dramt
	       , DECODE (c810_cr_amt, 0, 0, NVL (c810_cr_amt * c810_qty, 0)) cramt, c810_qty qty
	     , c803_period_id perid
	       , c810_party_id partyid, NVL (c820_costing_id, 0) costid, TO_CHAR (c810_created_date, 'MM/DD/YYYY hh24:mi AM') txntime
	       , c810_txn_id txnid, get_user_name (c810_created_by) createdby, c901_account_txn_type txntypeid
	       , v810.c801_account_element_id account_element_id
		FROM (
			  SELECT c810_account_txn_id ,
			  c901_account_txn_type ,
			  c810_txn_date ,
			  c810_acct_date ,
			  c810_desc ,
			  c810_dr_amt ,
			  c810_cr_amt ,
			  c810_qty ,
			  c810_party_id ,
			  c810_txn_id ,
			  c820_costing_id ,
			  c803_period_id ,
			  c205_part_number_id ,
			  c801_account_element_id ,
			  c810_delete_fl ,
			  c901_country_id ,
			  c901_company_id ,
			  c810_created_by ,
			  c810_created_date ,
			  c1900_company_id
			FROM T810_POSTING_TXN 
			WHERE c1900_company_id = 1000 -- US
			AND c810_delete_fl <> 'Y'
			UNION ALL
			SELECT c810_account_txn_id ,
			  c901_account_txn_type ,
			  c810_txn_date ,
			  c810_acct_date ,
			  c810_desc ,
			  c810_dr_amt ,
			  c810_cr_amt ,
			  c810_qty ,
			  c810_party_id ,
			  c810_txn_id ,
			  c820_costing_id ,
			  c803_period_id ,
			  c205_part_number_id ,
			  c801_account_element_id ,
			  c810_delete_fl ,
			  c901_country_id ,
			  c901_company_id ,
			  c810_created_by ,
			  c810_created_date ,
			  c1900_company_id
			FROM T810_POSTING_TXN_ARCHIVE
			WHERE c1900_company_id = 1000 -- US
			AND c810_delete_fl <> 'Y'
		) v810, t205_part_number t205, t202_project t202, t801_account_element t801
	   WHERE t205.C205_PART_NUMBER_ID  = v810.C205_PART_NUMBER_ID 
	   AND t205.C202_PROJECT_ID = T202.C202_PROJECT_ID
	AND v810.C801_ACCOUNT_ELEMENT_ID  = t801.C801_ACCOUNT_ELEMENT_ID
	   AND c901_account_txn_type IN ('4810','48131','48132','48133','48134','48135','48141')
	    AND t801.C800_ACCOUNT_TYPE_ID = 3 
	   -- AND v810.c810_txn_date >= {From date}
	   -- AND  v810.c810_txn_date <= {To Date}
	ORDER BY c810_txn_date, v810.c801_account_element_id, c810_account_txn_id;


