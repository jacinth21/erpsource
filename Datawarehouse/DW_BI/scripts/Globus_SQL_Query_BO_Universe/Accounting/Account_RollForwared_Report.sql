SELECT ' Opening Balance' txn_name
       , NVL (SUM (v810.c810_qty * v810.c810_dr_amt) - SUM (v810.c810_qty * v810.c810_cr_amt), 0) amt
       , get_project_name(t205.C202_PROJECT_ID) project_name, t801.C801_NAME a_name
       , get_code_name(t801.c901_location_type) location
       , t801.C801_ACCOUNT_ELEMENT_ID
       , 1 Sort_Order
    FROM (SELECT c205_part_number_id , c801_account_element_id ,  c810_qty  , c810_dr_amt, c810_cr_amt FROM t810_posting_txn  t810txn
          WHERE t810txn.c1900_company_id = 1000
          AND c810_delete_fl <> 'Y'
          AND c810_txn_date < to_date(@prompt('From date','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss') 
          UNION ALL 
          SELECT c205_part_number_id , c801_account_element_id ,  c810_qty  , c810_dr_amt, c810_cr_amt  FROM T810_POSTING_TXN_ARCHIVE T810arc
          WHERE T810arc.c1900_company_id = 1000 
          AND c810_delete_fl <> 'Y'
          ) v810  
    , t205_part_number t205, t801_account_element t801 
   WHERE t205.C205_PART_NUMBER_ID (+) = v810.C205_PART_NUMBER_ID
   AND t801.C801_ACCOUNT_ELEMENT_ID = v810.C801_ACCOUNT_ELEMENT_ID
   AND  t801.C800_ACCOUNT_TYPE_ID = 3 
GROUP BY t205.C202_PROJECT_ID, t801.C801_NAME,t801.c901_location_type, t801.C801_ACCOUNT_ELEMENT_ID
UNION ALL 
   SELECT trim(get_code_name (v810.c901_account_txn_type)) txn_name
       , NVL (SUM (v810.c810_qty * v810.c810_dr_amt) - SUM (v810.c810_qty * v810.c810_cr_amt), 0) amt
        , get_project_name(t205.C202_PROJECT_ID) project_name, t801.C801_NAME a_name
       , get_code_name(t801.c901_location_type) location_val
       , t801.C801_ACCOUNT_ELEMENT_ID
        , 2 Sort_Order
    FROM t810_posting_txn v810, t205_part_number t205, t801_account_element t801 
   WHERE t205.C205_PART_NUMBER_ID (+) = v810.C205_PART_NUMBER_ID
   AND t801.C801_ACCOUNT_ELEMENT_ID = v810.C801_ACCOUNT_ELEMENT_ID
   AND  t801.C800_ACCOUNT_TYPE_ID = 3 
   AND v810.c810_txn_date >= to_date(@prompt('From date','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss')
   AND v810.c810_txn_date <= to_date(@prompt('To date','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss')
   AND v810.c1900_company_id = 1000 
   AND c810_delete_fl <> 'Y'
GROUP BY t205.C202_PROJECT_ID, t801.C801_NAME,t801.c901_location_type,v810.c901_account_txn_type, t801.C801_ACCOUNT_ELEMENT_ID
ORDER BY txn_name,project_name