--@"C:\Datawarehouse\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Scheduled_Reports\Month_End_Reports\VDW_Acc_Ops_CN_only_NEDC.vw"
CREATE OR REPLACE VIEW VDW_Acc_Ops_CN_only_NEDC
AS
     SELECT t205.c205_part_number_id pnum, t205.c205_part_num_desc p_desc, get_code_name (t205.c205_product_family)
        prod_family, NVL (tmp_con.field_ws_qty, 0) fs_warehouse_qty, NVL (tmp_con.acc_ws_qty, 0) acc_warehouse_qty
      , (NVL (tmp_con.field_ws_qty, 0) + NVL (tmp_con.acc_ws_qty, 0)) ops_qty, NVL (tmp_con.account_qty, 0) cogs_qty,
        t1900.C1900_COMPANY_NAME company_name
       FROM t205_part_number t205, T1900_COMPANY t1900, (
             SELECT pnum, SUM (account_qty) account_qty, SUM (field_ws_qty) field_ws_qty
              , SUM (acc_ws_qty) acc_ws_qty, comp_id
               FROM
                (
                    -- getting the accounting count - US sales consignment bucket
                     SELECT t820.c205_part_number_id pnum, SUM (NVL (t820.c820_qty_on_hand, 0)) account_qty, SUM (0)
                        field_ws_qty, SUM (0) acc_ws_qty, t820.C1900_COMPANY_ID comp_id
                       FROM t820_costing t820
                      WHERE t820.c901_status      IN (4800, 4801) -- 4800 Pending Progress --4801 Active
                        AND t820.c901_costing_type = 4904 -- US sales consignment
                        AND t820.c1900_company_id IN (1010, 1008, 1017)
                   GROUP BY t820.c205_part_number_id, t820.C1900_COMPANY_ID
                  UNION ALL
                    -- getting the field sales warehouse count
                     SELECT t5053.c205_part_number_id pnum, SUM (0) account_qty, SUM (NVL (t5053.c5053_curr_qty, 0))
                        field_ws_qty, SUM (0) acc_ws_qty, 1010 comp_id
                       FROM t5053_location_part_mapping t5053, t5051_inv_warehouse t5051, t5052_location_master t5052
                      WHERE t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id
                        AND t5053.c5052_location_id      = t5052.c5052_location_id
                        AND t5051.c5051_inv_warehouse_id = 3 -- FS warehouse
                        AND t5052.c5052_void_fl         IS NULL
                        AND t5052.c1900_company_id      IN
                        (
                             SELECT c1900_company_id
                               FROM t5041_plant_company_mapping
                              WHERE c5040_plant_id = 3003 AND C5041_PRIMARY_FL = 'Y' AND c5041_void_fl IS NULL
                        )
                   GROUP BY t5053.c205_part_number_id
                  UNION ALL
                    -- getting the account warehouse count
                     SELECT t5053.c205_part_number_id pnum, SUM (0) account_qty, SUM (0) field_ws_qty
                      , SUM (NVL (t5053.c5053_curr_qty, 0)) acc_ws_qty, 1010 comp_id
                       FROM t5053_location_part_mapping t5053, t5051_inv_warehouse t5051, t5052_location_master t5052
                      WHERE t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id
                        AND t5053.c5052_location_id      = t5052.c5052_location_id
                        AND t5051.c5051_inv_warehouse_id = 5 -- Account warehouse
                        AND t5052.c5052_void_fl         IS NULL
                        AND t5052.c1900_company_id      IN
                        (
                             SELECT c1900_company_id
                               FROM t5041_plant_company_mapping
                              WHERE c5040_plant_id = 3003 AND C5041_PRIMARY_FL = 'Y' AND c5041_void_fl IS NULL
                        )
                   GROUP BY t5053.c205_part_number_id
                )
           GROUP BY pnum, comp_id
        )
        tmp_con
      WHERE t205.c205_part_number_id = tmp_con.pnum
        AND t1900.C1900_COMPANY_ID   = tmp_con.comp_id
        AND (tmp_con.field_ws_qty   <> 0
        OR tmp_con.acc_ws_qty       <> 0
        OR tmp_con.account_qty      <> 0)
   ORDER BY company_name, pnum;