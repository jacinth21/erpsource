--@"D:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Scheduled_Reports\Month_End_Reports\VDW_ICT_Returns_Monthly.vw";

CREATE OR REPLACE VIEW VDW_ICT_Returns_Monthly AS
SELECT v810.c810_txn_id txnid,globus_app.get_distributor_name(t506.C701_DISTRIBUTOR_ID) DIST_NAME,
         c205_part_number_id pnum,globus_app.get_partnum_desc (c205_part_number_id) pdesc,
c810_qty qty, 
                         DECODE (c810_dr_amt, 0, 0, NVL (c810_dr_amt * c810_qty, 0)) dramt, 
         DECODE (c810_cr_amt, 0, 0, NVL (c810_cr_amt * c810_qty, 0)) cramt,
            globus_app.get_acctelement_desc (c801_account_element_id) 
         || '/' 
         || globus_app.get_code_name (c901_account_txn_type) txntype, 
         globus_app.get_code_name(c901_account_txn_type) txntypeid,
           TO_CHAR (c810_txn_date, 'mm/dd/yyyy') txndate  
    FROM v810_posting_txn v810, t506_returns t506
   WHERE c810_delete_fl <> 'Y' 
     AND c901_account_txn_type = '48097' 
     AND v810.C810_TXN_ID = t506.C506_RMA_ID
	AND t506.c1900_company_id  =1000
	AND t506.c5040_plant_id    =3000
     AND c810_txn_date BETWEEN trunc(LAST_DAY (ADD_MONTHS (SYSDATE,-2)) + 1 )
     AND trunc(LAST_DAY (ADD_MONTHS (SYSDATE,-1)))
ORDER BY c810_txn_date, c801_account_element_id, c810_account_txn_id;
