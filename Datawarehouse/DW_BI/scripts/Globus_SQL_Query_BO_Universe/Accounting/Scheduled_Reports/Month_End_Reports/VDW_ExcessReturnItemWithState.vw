--@"D:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Scheduled_Reports\Month_End_Reports\VDW_ExcessReturnItemWithState.vw";

CREATE OR REPLACE VIEW VDW_ExcessReturnItemWithState AS
	SELECT DISTRIBUTOR_DIM.DISTRIBUTOR_NAME name 
		, DISTRIBUTOR_DIM.BILLING_STATE BILL_STATE
	    , P_NUM 
		, PART_DIM.product_family pfamily
	    , F_VALUE 
	 FROM
	 (  
	 SELECT retur.d_id, retur.p_num, NVL (consign.c_qty,0) c_qty,
	      NVL (retur.r_qty, 0) r_qty,
	      (NVL(consign.c_qty,0) - NVL (retur.r_qty, 0)) f_value
	   FROM (SELECT   t504.c701_distributor_id d_id,
	              t505.c205_part_number_id p_num,
	              SUM (t505.c505_item_qty) c_qty
	           FROM t504_consignment t504, t505_item_consignment t505
	          WHERE t504.c701_distributor_id IS NOT NULL
	            AND t504.c504_consignment_id = t505.c504_consignment_id
	            AND t504.c504_status_fl = 4
	            AND t504.c504_type IN  (4110)
	              --AND t504.c504_consignment_id NOT LIKE 'GM-EX-%'
	              -- AND t504.c504_consignment_id NOT IN (SELECT T504A.c504_consignment_id FROM T504A_CONSIGNMENT_EXCESS T504A WHERE T504A.C504A_STATUS_FL = 0)
	            AND TRIM (t505.c505_control_number) IS NOT NULL
	            AND c504_void_fl IS NULL
	            AND trim(t504.C207_SET_ID) IS NULL
				AND t504.c1900_company_id  =1000
				AND t504.c5040_plant_id    =3000
	       GROUP BY t504.c701_distributor_id, t505.c205_part_number_id) consign,
	      (SELECT   t506.c701_distributor_id d_id,
	              t507.c205_part_number_id p_num,
	              SUM (t507.c507_item_qty) r_qty
	           FROM t506_returns t506, t507_returns_item t507
	          WHERE t506.c701_distributor_id IS NOT NULL
	            AND t506.c506_status_fl = 2
	            AND t506.c506_rma_id = t507.c506_rma_id
	            AND t507.c507_status_fl IN('C','W')
	            AND t506.c506_void_fl IS NULL
	            AND trim(t506.C207_SET_ID) IS NULL
				AND t506.c1900_company_id  =1000
				AND t506.c5040_plant_id    =3000
	              --AND t506.C506_RMA_ID NOT IN  ( SELECT X.C921_REF_ID FROM T921_TRANSFER_DETAIL X
	               --         WHERE X.C920_TRANSFER_ID IN ( SELECT T921.C920_TRANSFER_ID FROM T504A_CONSIGNMENT_EXCESS T504A, T921_TRANSFER_DETAIL T921
	                --WHERE T921.C921_REF_ID = T504A.C504_CONSIGNMENT_ID AND T504A.C504A_STATUS_FL = 1 ) AND X.C901_LINK_TYPE =  90361 )
	       GROUP BY t506.c701_distributor_id, t507.c205_part_number_id) retur
	  WHERE retur.d_id  = consign.d_id (+)   
	        AND retur.p_num  = consign.p_num (+)
	 ) TEMP 
	, DISTRIBUTOR_DIM DISTRIBUTOR_DIM 
	, PART_DIM PART_DIM
	WHERE TEMP.f_value < 0 
        AND DISTRIBUTOR_DIM.distributor_id = TEMP.d_id
	AND PART_DIM.PART_ID = TEMP.p_num 
	--AND T205.C205_PART_NUMBER_ID = '101.613' 
	ORDER BY name, p_num;


