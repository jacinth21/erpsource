--@"D:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Scheduled_Reports\Month_End_Reports\VDW_Part_AcctQty_SalesConsgn.vw";

CREATE OR REPLACE VIEW VDW_Part_AcctQty_SalesConsgn AS
	SELECT X.C205_PART_NUMBER_ID , SUM(NVL(X.C820_QTY_ON_HAND ,0)) account_qty 
	FROM T820_COSTING X  
	WHERE X.C901_STATUS IN (4800,4801)
	AND X.C901_COSTING_TYPE = 4904
	GROUP BY X.C205_PART_NUMBER_ID;


