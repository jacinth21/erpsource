--@"D:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Scheduled_Reports\Month_End_Reports\VDW_NetConsByDistBySetRet_Cnt.vw";

CREATE OR REPLACE VIEW VDW_NetConsByDistBySetRet_Cnt AS
	SELECT globus_app.get_distributor_name(t506.C701_DISTRIBUTOR_ID) name,t506.C207_SET_ID SETID
	,globus_app.get_set_name(t506.C207_SET_ID) SETNAME
	,globus_app.get_code_name(t506.C506_type) RETURNTYPE
	,globus_app.get_code_name(t506.C506_REASON) REASON,  COUNT(1)  RETURN
	FROM t506_returns t506
	WHERE t506.c701_distributor_id IS NOT NULL
	AND t506.c506_status_fl = 2
	AND t506.c506_void_fl IS NULL
	AND trim(t506.C207_SET_ID) IS NOT NULL
	AND t506.c1900_company_id  =1000
	AND t506.c5040_plant_id    =3000
	GROUP BY t506.C701_DISTRIBUTOR_ID, t506.C207_SET_ID,t506.C506_REASON,t506.C506_type
	ORDER BY name, t506.C207_SET_ID;


