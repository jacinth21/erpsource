
--@"D:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Scheduled_Reports\Month_End_Reports\VDW_Demo_RA_Pending_Reprocess.vw";

CREATE OR REPLACE VIEW VDW_Demo_RA_Pending_Reprocess AS
SELECT get_company_name(setdtl.compid) compname, setdtl.raid , setdtl.distnm  , setdtl.repname 
  , ra.creditdt , setdtl.setid  , setdtl.setname 
  , ra.tagid , setdtl.pnum , get_partnum_desc (setdtl.pnum) partdesc
  , setdtl.setqty , NVL (ra.QTY_Crd, 0) qtycrd, ra.control_no 
   FROM
    (
         SELECT t506.c506_rma_id RAid, t506.c207_set_id setid, t507.c5010_tag_id tagid
          , t506.C506_CREDIT_DATE creditdt, t507.c205_part_number_id PNUM, c507_item_qty QTY_Crd
          , t507.c507_control_number control_no
           FROM t506_returns t506, t507_returns_item t507
          WHERE t506.c506_rma_id      = t507.c506_rma_id
            AND t506.c506_status_fl   = 2
            --AND t506.c1900_company_id = 1000
            AND t507.c507_status_fl   = 'C'
            AND c506_type            IN (3301,3302)
			AND t506.c506_void_fl IS NULL
            AND t506.C506_CREDIT_DATE BETWEEN TRUNC (sysdate, 'mm') AND SYSDATE
    )
    RA, (
         SELECT t506.c1900_company_id compid, t207.c207_set_id setid, t207.c207_set_nm setname, t208.c205_part_number_id pnum
          , t208.c208_set_qty setqty, t506.c506_rma_id RAid, get_distributor_name (t506.c701_distributor_id) distnm
          , get_rep_name (t506.C703_SALES_REP_ID) repname
           FROM t207_set_master t207, t208_set_details t208, t506_returns t506
          WHERE t207.c207_set_id      = t208.c207_set_id
            AND t506.c207_set_id      = t207.c207_set_id
            AND t506.c506_status_fl   = 2
            --AND t506.c1900_company_id = 1000
            AND c506_type            IN (3301,3302)
            AND t506.C506_CREDIT_DATE BETWEEN TRUNC (sysdate, 'mm') AND SYSDATE
            AND t207.c207_set_id IN
            (
                 SELECT c207_set_id
                   FROM t207_set_master
                  WHERE c901_set_grp_type = 1601
                    AND c207_type         = 4078 --Demo
                    AND C207_VOID_FL     IS NULL
            )
            AND c208_void_fl IS NULL
    )
    setdtl
  WHERE setdtl.setid = ra.setid(+)
    AND setdtl.raid  = ra.RAid(+)
    AND setdtl.pnum  = ra.pnum(+)
ORDER BY compname, SETID, raid, pnum;						
						
						
						
						
						
						
						
