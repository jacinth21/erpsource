
--@"D:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Scheduled_Reports\Month_End_Reports\VDW_ICT_RA_Reprocess.vw";

CREATE OR REPLACE VIEW VDW_ICT_RA_Reprocess AS
select get_company_name(t506.c1900_company_id) compname, t506.c506_rma_id  RAID
, get_distributor_name(t506.c701_distributor_id) distnm
, get_rep_name( t506.c703_sales_rep_id) repnm
, t506.c506_ref_id refid
, get_code_name(t506.c506_type) typenm
, t506.C506_CREDIT_DATE CREDIT_DT
, t506.C207_SET_ID SETID
, get_set_name(t506.C207_SET_ID) SETNM
, GET_TAG_ID( t506.c506_rma_id ) TAGID
, t507.c205_part_number_id pnum
, get_partnum_desc(t507.c205_part_number_id) partdesc
, NVL2(t506.C207_SET_ID, get_set_qty(t506.C207_SET_ID,t507.c205_part_number_id),'') setqty
, t507.c507_item_qty qtycredit
, t507.c507_control_number control
from t506_returns  t506, t507_returns_item t507 
where t506.c506_rma_id = t507.c506_rma_id 
AND t506.c506_type in ('26240177','3307') 
AND t506.c506_status_fl =1
AND t507.c507_status_fl   ='R'
--and t506.c1900_company_id  =1000
AND t506.c506_void_fl IS NULL order by  compname,distnm,t506.c506_rma_id,t507.c205_part_number_id;	


				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				

				
				
				
				
				
				
				
				
				
				
				
				
				
