--@"D:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Scheduled_Reports\Month_End_Reports\VDW_NetConsbyDistBySetNetCnt.vw";

CREATE OR REPLACE VIEW VDW_NetConsbyDistBySetNetCnt AS
	SELECT globus_app.get_distributor_name(a.C701_DISTRIBUTOR_ID) name , T207.C207_SET_ID , T207.C207_SET_NM ,  a.cons ,  B.RETURN 
	, (a.cons -  NVL(B.RETURN,0)) total_consign,  globus_app.GET_CODE_NAME(T207.C207_CATEGORY) Cat
	 FROM T207_SET_MASTER T207, ( 
	SELECT T504.C701_DISTRIBUTOR_ID, T504.C207_SET_ID, COUNT(1) cons FROM T504_CONSIGNMENT T504 
	WHERE  t504.c701_distributor_id IS NOT NULL 
	AND t504.c504_status_fl = 4 
	AND t504.c504_type IN  (4110) 
	AND c504_void_fl IS NULL 
	AND trim(t504.C207_SET_ID) IS NOT NULL
	AND T504.c1900_company_id  =1000
	AND T504.c5040_plant_id    =3000		
	GROUP BY T504.C701_DISTRIBUTOR_ID, T504.C207_SET_ID ) A, 
	( SELECT t506.C701_DISTRIBUTOR_ID, t506.C207_SET_ID, COUNT(1)  RETURN 
	FROM t506_returns t506 
	WHERE t506.c701_distributor_id IS NOT NULL 
	AND t506.c506_status_fl = 2 
	AND t506.c506_void_fl IS NULL 
	AND trim(t506.C207_SET_ID) IS NOT NULL 
	AND t506.c1900_company_id  =1000
	AND t506.c5040_plant_id    =3000
	GROUP BY t506.C701_DISTRIBUTOR_ID, t506.C207_SET_ID ) B 
	WHERE A.C701_DISTRIBUTOR_ID = B.C701_DISTRIBUTOR_ID (+) 
	AND A.C207_SET_ID = B.C207_SET_ID (+) 
	AND T207.C207_SET_ID = A.C207_SET_ID 
	ORDER BY name, T207.C207_SET_ID;


