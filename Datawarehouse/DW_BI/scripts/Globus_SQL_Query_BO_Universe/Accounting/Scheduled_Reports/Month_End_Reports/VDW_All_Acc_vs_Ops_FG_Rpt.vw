--@"C:\Datawarehouse\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Scheduled_Reports\Month_End_Reports\VDW_All_Acc_vs_Ops_FG_Rpt.vw"
CREATE OR REPLACE VIEW VDW_All_Acc_vs_Ops_FG_Rpt
AS
     SELECT pnum, t205.c205_part_num_desc pdesc, inv_value inv_value
      , cogs_value cogs_value, get_company_name (t5040.C1900_PARENT_COMPANY_ID) company_name
       FROM t205_part_number t205, t5040_plant_master t5040, (
             SELECT pnum, SUM (ops_qty) inv_value, SUM (acc_qty) cogs_value
              , plant
               FROM
                (
                    -- getting the FG bucket value
                     SELECT t205c.C205_PART_NUMBER_ID pnum, SUM (t205c.C205_AVAILABLE_QTY) ops_qty, 0 acc_Qty
                      , t205c.c5040_plant_id plant
                       FROM t205c_part_qty t205c
                      WHERE C901_TRANSACTION_TYPE = 90800 -- Inventory Qty
                   GROUP BY t205c.C205_PART_NUMBER_ID, t205c.c5040_plant_id
                  UNION ALL
                    -- getting the Bulk Qty value
                     SELECT t205c.C205_PART_NUMBER_ID pnum, SUM (t205c.C205_AVAILABLE_QTY) ops_qty, 0 acc_Qty
                      , t205c.c5040_plant_id plant
                       FROM t205c_part_qty t205c
                      WHERE C901_TRANSACTION_TYPE = 90814 -- Bulk Qty
                   GROUP BY t205c.C205_PART_NUMBER_ID, t205c.c5040_plant_id
                  UNION ALL
                    -- getting the repackage qty value
                     SELECT t205c.C205_PART_NUMBER_ID pnum, SUM (t205c.C205_AVAILABLE_QTY) ops_qty, 0 acc_Qty
                      , t205c.c5040_plant_id plant
                       FROM t205c_part_qty t205c
                      WHERE C901_TRANSACTION_TYPE = 90815 -- Repackage Qty
                   GROUP BY t205c.C205_PART_NUMBER_ID, t205c.c5040_plant_id
                  UNION ALL
                    -- getting the returns hold inventory
                     SELECT t205c.C205_PART_NUMBER_ID pnum, SUM (t205c.C205_AVAILABLE_QTY) ops_qty, 0 acc_Qty
                      , t205c.c5040_plant_id plant
                       FROM t205c_part_qty t205c
                      WHERE C901_TRANSACTION_TYPE = 90812 -- Returns Hold Inventory
                   GROUP BY t205c.C205_PART_NUMBER_ID, t205c.c5040_plant_id
                  UNION ALL
                    -- geeting the FG costing layer Qty
                     SELECT c205_part_number_id pnum, 0 ops_qty, SUM (c820_qty_on_hand) cogs_value
                      , C5040_PLANT_ID
                       FROM t820_costing
                      WHERE c901_costing_type = 4900 -- FG Inventory
                        AND c901_status      <> 4802 -- Closed
                   GROUP BY c205_part_number_id, C5040_PLANT_ID
                )
           GROUP BY pnum, plant
        )
        tmp_fg
      WHERE t205.c205_part_number_id = tmp_fg.pnum
        AND t5040.c5040_plant_id     = tmp_fg.plant
        AND (inv_value              <> 0
        OR cogs_value               <> 0)
   ORDER BY t5040.C1900_PARENT_COMPANY_ID, pnum;