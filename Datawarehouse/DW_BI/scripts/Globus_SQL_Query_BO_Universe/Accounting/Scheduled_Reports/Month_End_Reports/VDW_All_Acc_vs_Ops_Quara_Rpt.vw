--@"C:\Datawarehouse\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Scheduled_Reports\Month_End_Reports\VDW_All_Acc_vs_Ops_Quara_Rpt.vw";
CREATE OR REPLACE VIEW VDW_All_Acc_vs_Ops_Quara_Rpt
AS
     SELECT pnum, t205.c205_part_num_desc pdesc, inv_value inv_value
      , cogs_value cogs_value, get_company_name (t5040.C1900_PARENT_COMPANY_ID) company_name
       FROM t205_part_number t205, t5040_plant_master t5040, (
             SELECT pnum, SUM (ops_qty) inv_value, SUM (acc_qty) cogs_value
              , plant
               FROM
                (
                    -- getting the operation qty
                     SELECT t205c.C205_PART_NUMBER_ID pnum, SUM (t205c.C205_AVAILABLE_QTY) ops_qty, 0 acc_qty
                      , t205c.C5040_PLANT_ID plant
                       FROM t205c_part_qty t205c
                      WHERE C901_TRANSACTION_TYPE = 90813 -- Quarantine Inventory
                   GROUP BY t205c.C5040_PLANT_ID, t205c.C205_PART_NUMBER_ID
                  UNION ALL
                    -- getting the accounting qty
                     SELECT t820.c205_part_number_id pnum, 0 ops_qty, SUM (t820.c820_qty_on_hand) acc_qty
                      , t820.c5040_plant_id plant
                       FROM t820_costing t820
                      WHERE c901_costing_type = 4902 -- Quarantine Inventory
                        AND c901_status      <> 4802 -- Closed
                   GROUP BY c205_part_number_id, t820.c5040_plant_id
                )
           GROUP BY pnum, plant
        )
        tmp_quarantine
      WHERE t205.c205_part_number_id = tmp_quarantine.pnum
        AND t5040.c5040_plant_id     = tmp_quarantine.plant
        AND (inv_value              <> 0
        OR cogs_value               <> 0)
   ORDER BY t5040.C1900_PARENT_COMPANY_ID, pnum;