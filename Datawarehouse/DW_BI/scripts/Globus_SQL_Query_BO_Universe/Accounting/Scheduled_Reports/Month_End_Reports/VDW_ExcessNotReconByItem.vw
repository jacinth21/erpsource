--@"D:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Scheduled_Reports\Month_End_Reports\VDW_ExcessNotReconByItem.vw";

CREATE OR REPLACE VIEW VDW_ExcessNotReconByItem AS
  SELECT T701.C701_DISTRIBUTOR_NAME name, GET_CODE_NAME(T701.C701_BILL_STATE) BILL_STATE     
	, t505.c205_part_number_id p_num,get_code_name(C205_PRODUCT_FAMILY) C205_Product_Family
    ,SUM (t505.c505_item_qty*-1) c_qty 
  FROM t504_consignment t504, t505_item_consignment t505 , T504A_CONSIGNMENT_EXCESS T504A
       , T701_DISTRIBUTOR T701, T205_PART_NUMBER T205 
  WHERE t504.c701_distributor_id IS NOT NULL 
    AND t504.c504_consignment_id = t505.c504_consignment_id 
    AND t504.c504_consignment_id = T504A.C504_CONSIGNMENT_ID  
    AND t504.c504_status_fl = 4 
    AND t504.c504_type IN  (4110)
    AND T504A.C504A_STATUS_FL = 0
    AND TRIM (t505.c505_control_number) IS NOT NULL
    AND c504_void_fl IS NULL
    AND T701.C701_DISTRIBUTOR_ID = t504.c701_distributor_id
    AND T205.C205_PART_NUMBER_ID = t505.c205_part_number_id
    AND t504.c1900_company_id  =1000
    AND t504.c5040_plant_id    =3000
	GROUP BY T701.C701_DISTRIBUTOR_NAME,T701.C701_BILL_STATE,t505.c205_part_number_id,C205_PRODUCT_FAMILY;


