/* Formatted on 2011/06/07 18:54 (Formatter Plus v4.8.0) */

--@"D:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Scheduled_Reports\Month_End_Reports\VDW_SalesConsAfterCorrectRet.vw";

CREATE OR REPLACE VIEW VDW_SalesConsAfterCorrectRet AS
	SELECT     *
    FROM
        (SELECT     t205.c205_part_number_id p_num                                        , t205.c205_part_num_desc p_desc              , get_code_name (
                t205.c205_product_family) prod_family                                     , NVL (not_reconciled.c_qty, 0) no_recon_qty  , NVL (
                op_total.f_value, 0) op_total                                             , NVL (accounting_qty.account_qty, 0) acct_qty, NVL (
                ictica2010returns.r_qty, 0) ictica2010returns                             , NVL (itccons.c_qty, 0) itcconsvalue         , NVL (
                indiasalescon2008.c_qty, 0) indiasalescon2008                             , NVL (indiacon2010.c_qty, 0) indiacon2010    , NVL (
                cyprussalescons2008.c_qty, 0) cyprussalescons2008                         , NVL (ukcon2010.c_qty, 0) ukcon2010          , NVL (
                uksalescon2008.c_qty, 0) uksalescon2008                                   , NVL (belgiumcon2010.c_qty, 0) belgiumcon2010, NVL (
                germanycon2010.c_qty, 0) germanycon2010                                   , NVL (globuscarecon2010today.c_qty, 0)
                globuscarecon2010today                                                    , NVL (orthoconmayjune2010.c_qty, 0)
                orthoconmayjune2010                                                       , NVL (drdanielallcon.c_qty, 0) drdanielallcon, NVL (
                polandallcon.c_qty, 0) polandallcon                                       , NVL (swissallcon.c_qty, 0) swissallcon      , NVL (
                southafricaallcon.c_qty, 0) southafricaallcon                             , NVL (edc.c_qty, 0) edc                      , NVL (
                ictreturnsstart.r_qty, 0) ictreturnsstart                                 , NVL (icticareturnsstart.r_qty, 0) icticareturnsstart
                , NVL (ictexcess2010_not_reconciled.c_qty, 0) ictexcess2010_not_reconciled, NVL (
                indiareturnbefore2009.r_qty, 0) indiareturnbefore2009                     , NVL (
                indiareturnjulytoday2010.r_qty, 0) indiareturnjulytoday2010               , NVL (
                ukreturnsbefore2009.r_qty, 0) ukreturnsbefore2009                         , NVL (
                globuscarereturnsales2010.r_qty, 0) globuscarereturnsales2010             , NVL (
                orthoreturnsales2010.r_qty, 0) orthoreturnsales2010
              , NVL (cogserror.cogs_qty, 0) cogserror                       , NVL (ictexbef2010_not_reconciled.c_qty, 0)
                ictexbef2010_not_reconciled                                 , NVL (globuscarebackoffdistcon2009.c_qty, 0)
                globuscarebackoffdistcon2009                                , NVL (globuscarertnload2009.r_qty, 0) globuscarertnload2009, NVL
                (indiaconsnotposted.c_qty, 0) indiaconsnotposted            , NVL (ukcorrection.r_qty, 0) ukcorrection                  ,
                NVL (belgiumcorrection.r_qty, 0) belgiumcorrection          , NVL (germanycorrection.r_qty, 0) germanycorrection        ,
                NVL (indiacorrectiondtrange.r_qty, 0) indiacorrectiondtrange, NVL (glcarecorrection.r_qty, 0)
                glcarecorrection                                            , NVL (drdancorrection.r_qty, 0)
                drdancorrection                                             , NVL (orthocorrection.r_qty, 0)
                orthocorrection                                             , NVL (fisher.r_qty, 0) fisher, NVL (
                globuscarertn2009range.r_qty, 0) globuscarertn2009range     , NVL (cogsaprnow2011.cogs_qty, 0)
                cogsaprnow2011
            FROM t205_part_number t205
                -- ICT Rreturns added by Richard
              , (SELECT     t507.c205_part_number_id p_num, 'ictica2010returns', SUM (t507.c507_item_qty) r_qty
                    FROM t506_returns t506                , t507_returns_item t507
                    WHERE t506.c701_distributor_id IS NOT NULL
                        AND t506.c506_status_fl = 2
                        AND t506.c506_rma_id = t507.c506_rma_id
                        AND t507.c507_status_fl IN ('C', 'W')
                        AND t506.c506_void_fl IS NULL
                        AND t506.c1900_company_id = 1000
                        AND t506.c5040_plant_id = 3000
                        AND t506.c506_credit_date >= TO_DATE ('01/11/2010', 'MM/DD/YYYY')
                        AND t506.c701_distributor_id IN
                        (SELECT     c701_distributor_id
                            FROM t701_distributor x
                            WHERE x.c901_distributor_type IN (70106, 70103)
                        )
                    GROUP BY t507.c205_part_number_id
                ) ictica2010returns
                --All ICT Consignment from '1/11/2010' added by Richard
              , (SELECT     t505.c205_part_number_id p_num, 'ICTC', SUM (t505.c505_item_qty) c_qty
                    FROM t504_consignment t504            , t505_item_consignment t505
                    WHERE t504.c701_distributor_id IS NOT NULL
                        AND t504.c504_consignment_id = t505.c504_consignment_id
                        AND t504.c504_status_fl = 4
                        AND t504.c504_type IN (40057)
                        AND TRIM (t505.c505_control_number) IS NOT NULL
                        AND t504.c1900_company_id = 1000
                        AND t504.c5040_plant_id = 3000
                        AND t504.c504_ship_date >= TO_DATE ('1/11/2010', 'MM/DD/YYYY')
                        AND t504.c701_distributor_id IN
                        (SELECT     c701_distributor_id
                            FROM t701_distributor x
                            WHERE x.c901_distributor_type IN (70106, 70103)
                        )
                        AND c504_void_fl IS NULL
                    GROUP BY t505.c205_part_number_id
                ) itccons
                --Accounting Sales Consignment Quantity
              , (SELECT     x.c205_part_number_id p_num, SUM (NVL (x.c820_qty_on_hand, 0)) account_qty
                    FROM t820_costing x
                    WHERE x.c901_status IN (4800, 4801)
                        AND x.c901_costing_type = 4904
                    GROUP BY x.c205_part_number_id
                ) accounting_qty
                -- Excess Not Reconciled Includes US + ICT (ICT Excess are never reconciled.There is an exception for 1
                -- case where Excess is reconciled
              , (SELECT     t505.c205_part_number_id p_num, SUM (t505.c505_item_qty) * - 1 c_qty
                    FROM t504_consignment t504            , t505_item_consignment t505, t504a_consignment_excess t504a
                      , t205_part_number t205
                    WHERE t504.c701_distributor_id IS NOT NULL
                        AND t504.c504_consignment_id = t505.c504_consignment_id
                        AND t504.c504_consignment_id = t504a.c504_consignment_id
                        AND t504.c504_status_fl = 4
                        AND t504.c504_type IN (4110)
                        AND t504a.c504a_status_fl = 0
                        AND t504.c1900_company_id = 1000
                        AND t504.c5040_plant_id = 3000
                        AND TRIM (t505.c505_control_number) IS NOT NULL
                        AND c504_void_fl IS NULL
                        AND t205.c205_part_number_id = t505.c205_part_number_id
                    GROUP BY t505.c205_part_number_id
                ) not_reconciled,
                --Net Consignment Operation side. The total does not Include Not Reconciled for US and ICT
                (
                SELECT     consign.p_num, NVL (consign.c_qty, 0) c_qty, NVL (retur.r_qty, 0) r_qty
                      , (NVL (consign.c_qty, 0) - NVL (retur.r_qty, 0)) f_value
                    FROM
                        (SELECT     t505.c205_part_number_id p_num, SUM (t505.c505_item_qty) c_qty
                            FROM t504_consignment t504            , t505_item_consignment t505
                            WHERE t504.c701_distributor_id IS NOT NULL
                                AND t504.c504_consignment_id = t505.c504_consignment_id
                                AND t504.c504_status_fl = 4
                                AND t504.c504_type IN (4110)
                                AND TRIM (t505.c505_control_number) IS NOT NULL
                                -- AND TRIM (t504.c207_set_id) IS NULL
                                --AND t504.c701_distributor_id NOT IN (456,480,375,652,632,121,427,506,373,569)
                                AND c504_void_fl IS NULL
                                AND t504.c1900_company_id = 1000
                                AND t504.c5040_plant_id = 3000
                            GROUP BY t505.c205_part_number_id
                        ) consign                 , (SELECT     t507.c205_part_number_id p_num, SUM (t507.c507_item_qty) r_qty
                            FROM t506_returns t506, t507_returns_item t507
                            WHERE t506.c701_distributor_id IS NOT NULL
                                AND t506.c506_status_fl = 2
                                AND t506.c506_rma_id = t507.c506_rma_id
                                AND t507.c507_status_fl IN ('C', 'W')
                                AND t506.c1900_company_id = 1000
                                AND t506.c5040_plant_id = 3000
                                -- AND TRIM (t506.c207_set_id) IS NULL
                                --AND t506.c701_distributor_id NOT IN (456,480,375,652,632,121,427,506,373,569)
                                AND t506.c506_void_fl IS NULL
                            GROUP BY t507.c205_part_number_id
                        ) retur
                    WHERE consign.p_num = retur.p_num(+)
                ) op_total,
                --India Consign on/before 09/10/2008 for  type 40057
                (
                SELECT     t505.c205_part_number_id p_num, 'indiasalescon2008', SUM (t505.c505_item_qty) c_qty
                    FROM t504_consignment t504           , t505_item_consignment t505
                    WHERE t504.c701_distributor_id IS NOT NULL
                        AND t504.c504_consignment_id = t505.c504_consignment_id
                        AND t504.c504_status_fl = 4
                        AND t504.c504_type IN (40057)
                        AND TRIM (t505.c505_control_number) IS NOT NULL
                        AND t504.c504_ship_date <= TO_DATE ('09/10/2008', 'MM/DD/YYYY')
                        AND c504_void_fl IS NULL
                        AND t504.c504_consignment_id NOT LIKE 'GM-EX-%'
                        AND t504.c701_distributor_id IN (121)
                        AND t504.c1900_company_id = 1000
                        AND t504.c5040_plant_id = 3000
                    GROUP BY t505.c205_part_number_id
                ) indiasalescon2008,
                --India Consign on/after 12-JAN-10
                (
                SELECT     t505.c205_part_number_id p_num, 'indiacon2010', SUM (t505.c505_item_qty) c_qty
                    FROM t504_consignment t504           , t505_item_consignment t505
                    WHERE t504.c701_distributor_id IS NOT NULL
                        AND t504.c504_consignment_id = t505.c504_consignment_id
                        AND t504.c504_status_fl = 4
                        AND t504.c504_type IN (40057)
                        AND TRIM (t505.c505_control_number) IS NOT NULL
                        AND t504.c504_ship_date >= TO_DATE ('01/12/2010', 'MM/DD/YYYY')
                        AND c504_void_fl IS NULL
                        AND t504.c1900_company_id = 1000
                        AND t504.c5040_plant_id = 3000
                        AND t504.c504_consignment_id NOT LIKE 'GM-EX-%'
                        AND t504.c701_distributor_id IN (121)
                    GROUP BY t505.c205_part_number_id
                ) indiacon2010,
                --India Cons Not Posted
                (
                SELECT     t505.c205_part_number_id p_num, 'indiaconsnotposted', SUM (t505.c505_item_qty) c_qty
                    FROM t504_consignment t504           , t505_item_consignment t505
                    WHERE t504.c701_distributor_id IS NOT NULL
                        AND t504.c504_consignment_id = t505.c504_consignment_id
                        AND t504.c504_status_fl = 4
                        AND t504.c504_type IN (4110)
                        AND TRIM (t505.c505_control_number) IS NOT NULL
                        --AND TRIM (t504.c207_set_id) IS NULL
                        AND c504_void_fl IS NULL
                        AND t504.c1900_company_id = 1000
                        AND t504.c5040_plant_id = 3000
                        --AND t505.c205_part_number_id = '916.002'
                        --AND t505.c504_consignment_id  like 'GM-CN-56601'
                        AND t504.c701_distributor_id IN (121)
                        AND t504.c504_consignment_id IN ('GM-CN-3941', 'GM-CN-3888', 'GM-CN-7906', 'GM-CN-7946',
                        'GM-CN-8080')
                    GROUP BY t505.c205_part_number_id
                ) indiaconsnotposted,
                --Cyprus sales consign on before 8/13/2008 for type 40057
                (
                SELECT     t505.c205_part_number_id p_num, 'cyprussalescons2008', SUM (t505.c505_item_qty) c_qty
                    FROM t504_consignment t504           , t505_item_consignment t505
                    WHERE t504.c701_distributor_id IS NOT NULL
                        AND t504.c504_consignment_id = t505.c504_consignment_id
                        AND t504.c504_status_fl = 4
                        AND t504.c504_type IN (40057)
                        AND TRIM (t505.c505_control_number) IS NOT NULL
                        AND t504.c504_ship_date <= TO_DATE ('08/13/2008', 'MM/DD/YYYY')
                        AND c504_void_fl IS NULL
                        AND t504.c1900_company_id = 1000
                        AND t504.c5040_plant_id = 3000
                        AND t504.c504_consignment_id NOT LIKE 'GM-EX-%'
                        AND t504.c701_distributor_id IN (373)
                    GROUP BY t505.c205_part_number_id
                ) cyprussalescons2008,
                --UK Consign on/after 1/14/2010
                (
                SELECT     t505.c205_part_number_id p_num, 'ukcon2010', SUM (t505.c505_item_qty) c_qty
                    FROM t504_consignment t504           , t505_item_consignment t505
                    WHERE t504.c701_distributor_id IS NOT NULL
                        AND t504.c504_consignment_id = t505.c504_consignment_id
                        AND t504.c504_status_fl = 4
                        AND t504.c504_type IN (40057)
                        AND t504.c1900_company_id = 1000
                        AND t504.c5040_plant_id = 3000
                        AND TRIM (t505.c505_control_number) IS NOT NULL
                        AND t504.c504_ship_date >= TO_DATE ('01/14/2010', 'MM/DD/YYYY')
                        AND c504_void_fl IS NULL
                        AND t504.c504_consignment_id NOT LIKE 'GM-EX-%'
                        AND t504.c701_distributor_id IN (375)
                    GROUP BY t505.c205_part_number_id
                ) ukcon2010,
                --UK Consign sales consign on/before 9/12/2008
                (
                SELECT     t505.c205_part_number_id p_num, 'uksalescon2008', SUM (t505.c505_item_qty) c_qty
                    FROM t504_consignment t504           , t505_item_consignment t505
                    WHERE t504.c701_distributor_id IS NOT NULL
                        AND t504.c504_consignment_id = t505.c504_consignment_id
                        AND t504.c504_status_fl = 4
                        AND t504.c504_type IN (40057)
                        AND TRIM (t505.c505_control_number) IS NOT NULL
                        AND t504.c504_ship_date <= TO_DATE ('09/12/2008', 'MM/DD/YYYY')
                        AND c504_void_fl IS NULL
                        AND t504.c1900_company_id = 1000
                        AND t504.c5040_plant_id = 3000
                        AND t504.c504_consignment_id NOT LIKE 'GM-EX-%'
                        AND t504.c701_distributor_id IN (375)
                    GROUP BY t505.c205_part_number_id
                ) uksalescon2008,
                --Belgiun Consign on/after 14-Jan-10
                (
                SELECT     t505.c205_part_number_id p_num, 'belgiumcon2010', SUM (t505.c505_item_qty) c_qty
                    FROM t504_consignment t504           , t505_item_consignment t505
                    WHERE t504.c701_distributor_id IS NOT NULL
                        AND t504.c504_consignment_id = t505.c504_consignment_id
                        AND t504.c504_status_fl = 4
                        AND t504.c504_type IN (40057)
                        AND TRIM (t505.c505_control_number) IS NOT NULL
                        AND t504.c504_ship_date >= TO_DATE ('01/14/2010', 'MM/DD/YYYY')
                        AND c504_void_fl IS NULL
                        AND t504.c1900_company_id = 1000
                        AND t504.c5040_plant_id = 3000
                        AND t504.c504_consignment_id NOT LIKE 'GM-EX-%'
                        AND t504.c701_distributor_id IN (456)
                    GROUP BY t505.c205_part_number_id
                ) belgiumcon2010,
                --Germany Consign on/after 12-Jan-10
                (
                SELECT     t505.c205_part_number_id p_num, 'germanycon2010', SUM (t505.c505_item_qty) c_qty
                    FROM t504_consignment t504           , t505_item_consignment t505
                    WHERE t504.c701_distributor_id IS NOT NULL
                        AND t504.c504_consignment_id = t505.c504_consignment_id
                        AND t504.c504_status_fl = 4
                        AND t504.c504_type IN (40057)
                        AND TRIM (t505.c505_control_number) IS NOT NULL
                        AND t504.c504_ship_date >= TO_DATE ('01/12/2010', 'MM/DD/YYYY')
                        AND c504_void_fl IS NULL
                        AND t504.c1900_company_id = 1000
                        AND t504.c5040_plant_id = 3000
                        AND t504.c504_consignment_id NOT LIKE 'GM-EX-%'
                        AND t504.c701_distributor_id IN (480)
                    GROUP BY t505.c205_part_number_id
                ) germanycon2010,
                --Globus Cares Consign on after 10/25/2010 --
                (
                SELECT     t505.c205_part_number_id p_num, 'globuscarecon2010today', SUM (t505.c505_item_qty) c_qty
                    FROM t504_consignment t504           , t505_item_consignment t505
                    WHERE t504.c701_distributor_id IS NOT NULL
                        AND t504.c504_consignment_id = t505.c504_consignment_id
                        AND t504.c504_status_fl = 4
                        AND t504.c504_type IN (40057)
                        AND TRIM (t505.c505_control_number) IS NOT NULL
                        AND t504.c504_ship_date >= TO_DATE ('10/25/2010', 'MM/DD/YYYY')
                        AND c504_void_fl IS NULL
                        AND t504.c1900_company_id = 1000
                        AND t504.c5040_plant_id = 3000
                        AND t504.c504_consignment_id NOT LIKE 'GM-EX-%'
                        AND t504.c701_distributor_id IN (506)
                    GROUP BY t505.c205_part_number_id
                ) globuscarecon2010today,
                --Globus Cares Consign for 2009 between 06/26/2009 and 10/07/2009
                --Back off Globus Cares Consign for 2009 between 6/26/2009 and 10/7/2009 because they posted to ICT
                -- DIST and are in the SQL for Type = 4110.
                --We only need consignment between 10/16/2009 and till date that has type 4110(In Main SQL) & 40057(
                -- Right above) and posted to sales consignment.
                (
                SELECT     t505.c205_part_number_id p_num, 'globuscarebackoffdistcon2009', SUM (t505.c505_item_qty)
                        c_qty
                    FROM t504_consignment t504, t505_item_consignment t505
                    WHERE t504.c701_distributor_id IS NOT NULL
                        AND t504.c504_consignment_id = t505.c504_consignment_id
                        AND t504.c504_status_fl = 4
                        AND t504.c504_type IN (4110)
                        AND TRIM (t505.c505_control_number) IS NOT NULL
                        AND t504.c504_ship_date >= TO_DATE ('06/26/2009', 'MM/DD/YYYY')
                        AND t504.c504_ship_date <= TO_DATE ('10/07/2009', 'MM/DD/YYYY')
                        AND c504_void_fl IS NULL
                        AND t504.c504_consignment_id NOT LIKE 'GM-EX-%'
                        AND t504.c701_distributor_id IN (506)
                        AND t504.c1900_company_id = 1000
                        AND t504.c5040_plant_id = 3000
                    GROUP BY t505.c205_part_number_id
                ) globuscarebackoffdistcon2009,
                --Ortho Consign between 11-May-10 and 16-Jun-10
                (
                SELECT     t505.c205_part_number_id p_num, 'orthoconmayjune2010', SUM (t505.c505_item_qty) c_qty
                    FROM t504_consignment t504           , t505_item_consignment t505
                    WHERE t504.c701_distributor_id IS NOT NULL
                        AND t504.c504_consignment_id = t505.c504_consignment_id
                        AND t504.c504_status_fl = 4
                        AND t504.c504_type IN (40057)
                        AND TRIM (t505.c505_control_number) IS NOT NULL
                        AND t504.c504_ship_date >= TO_DATE ('05/11/2010', 'MM/DD/YYYY')
                        AND t504.c504_ship_date <= TO_DATE ('06/16/2010', 'MM/DD/YYYY')
                        AND c504_void_fl IS NULL
                        AND t504.c1900_company_id = 1000
                        AND t504.c5040_plant_id = 3000
                        AND t504.c504_consignment_id NOT LIKE 'GM-EX-%'
                        AND t504.c701_distributor_id IN (569)
                    GROUP BY t505.c205_part_number_id
                ) orthoconmayjune2010,
                --Dr. Daniel Serban all Consign
                (
                SELECT     t505.c205_part_number_id p_num, 'drdanielallcon', SUM (t505.c505_item_qty) c_qty
                    FROM t504_consignment t504           , t505_item_consignment t505
                    WHERE t504.c701_distributor_id IS NOT NULL
                        AND t504.c504_consignment_id = t505.c504_consignment_id
                        AND t504.c504_status_fl = 4
                        AND t504.c504_type IN (40057)
                        AND TRIM (t505.c505_control_number) IS NOT NULL
                        AND c504_void_fl IS NULL
                        AND t504.c1900_company_id = 1000
                        AND t504.c5040_plant_id = 3000
                        AND t504.c504_consignment_id NOT LIKE 'GM-EX-%'
                        AND t504.c701_distributor_id IN (632)
                    GROUP BY t505.c205_part_number_id
                ) drdanielallcon,
                --Poland all Consign
                (
                SELECT     t505.c205_part_number_id p_num, 'polandallcon', SUM (t505.c505_item_qty) c_qty
                    FROM t504_consignment t504           , t505_item_consignment t505
                    WHERE t504.c701_distributor_id IS NOT NULL
                        AND t504.c504_consignment_id = t505.c504_consignment_id
                        AND t504.c504_status_fl = 4
                        AND t504.c504_type IN (40057)
                        AND TRIM (t505.c505_control_number) IS NOT NULL
                        AND c504_void_fl IS NULL
                        AND t504.c1900_company_id = 1000
                        AND t504.c5040_plant_id = 3000
                        AND t504.c504_consignment_id NOT LIKE 'GM-EX-%'
                        AND t504.c701_distributor_id IN (652)
                    GROUP BY t505.c205_part_number_id
                ) polandallcon
                --New Dist Swiss Added 2011 --all
              , (SELECT     t505.c205_part_number_id p_num, 'swissallcon', SUM (t505.c505_item_qty) c_qty
                    FROM t504_consignment t504            , t505_item_consignment t505
                    WHERE t504.c701_distributor_id IS NOT NULL
                        AND t504.c504_consignment_id = t505.c504_consignment_id
                        AND t504.c504_status_fl = 4
                        AND t504.c504_type IN (40057)
                        AND TRIM (t505.c505_control_number) IS NOT NULL
                        --AND t504.c504_ship_date >= TO_DATE ('01/12/2010', 'MM/DD/YYYY')
                        AND c504_void_fl IS NULL
                        AND t504.c1900_company_id = 1000
                        AND t504.c5040_plant_id = 3000
                        AND t504.c504_consignment_id NOT LIKE 'GM-EX-%'
                        AND t504.c701_distributor_id IN (668)
                    GROUP BY t505.c205_part_number_id
                ) swissallcon
                --New Dist South Africa Added
              , (SELECT     t505.c205_part_number_id p_num, 'southafricaallcon', SUM (t505.c505_item_qty) c_qty
                    FROM t504_consignment t504            , t505_item_consignment t505
                    WHERE t504.c701_distributor_id IS NOT NULL
                        AND t504.c504_consignment_id = t505.c504_consignment_id
                        AND t504.c504_status_fl = 4
                        AND t504.c504_type IN (40057)
                        AND TRIM (t505.c505_control_number) IS NOT NULL
                        AND t504.c504_ship_date >= TO_DATE ('01/12/2010', 'MM/DD/YYYY')
                        AND c504_void_fl IS NULL
                        AND t504.c1900_company_id = 1000
                        AND t504.c5040_plant_id = 3000
                        AND t504.c504_consignment_id NOT LIKE 'GM-EX-%'
                        AND t504.c701_distributor_id IN (676)
                    GROUP BY t505.c205_part_number_id
                ) southafricaallcon
                --edc Added
              , (SELECT     t505.c205_part_number_id p_num, 'edc', SUM (t505.c505_item_qty) c_qty
                    FROM t504_consignment t504            , t505_item_consignment t505
                    WHERE t504.c701_distributor_id IS NOT NULL
                        AND t504.c504_consignment_id = t505.c504_consignment_id
                        AND t504.c504_status_fl = 4
                        AND t504.c504_type IN (40057)
                        AND TRIM (t505.c505_control_number) IS NOT NULL
                        -- AND t504.c504_ship_date >= TO_DATE ('01/12/2010', 'MM/DD/YYYY')
                        AND c504_void_fl IS NULL
                        AND t504.c1900_company_id = 1000
                        AND t504.c5040_plant_id = 3000
                        AND t504.c504_consignment_id NOT LIKE 'GM-EX-%'
                        AND t504.c701_distributor_id IN (689)
                    GROUP BY t505.c205_part_number_id
                ) edc,
                --All ICT Returns  from start
                (
                SELECT     t507.c205_part_number_id p_num, 'ictreturnsstart', SUM (t507.c507_item_qty) r_qty
                    FROM t506_returns t506               , t507_returns_item t507
                    WHERE t506.c701_distributor_id IS NOT NULL
                        AND t506.c506_status_fl = 2
                        AND t506.c506_rma_id = t507.c506_rma_id
                        AND t507.c507_status_fl IN ('C', 'W')
                        AND t506.c506_void_fl IS NULL
                        AND t506.c1900_company_id = 1000
                        AND t506.c5040_plant_id = 3000
                        -- AND t506.c506_credit_date < TO_DATE ('6/30/2010', 'MM/DD/YYYY')
                        -- AND t506.c506_credit_date >= TO_DATE ('01/12/2010', 'MM/DD/YYYY')
                        AND t506.c701_distributor_id IN
                        (SELECT     c701_distributor_id
                            FROM t701_distributor x
                            WHERE x.c901_distributor_type IN (70106, 70103, 70105)
                        )
                    GROUP BY t507.c205_part_number_id
                ) ictreturnsstart,
                --All 70106, 70103 type returns
                (
                SELECT     t507.c205_part_number_id p_num, 'icticareturnsstart', SUM (t507.c507_item_qty) r_qty
                    FROM t506_returns t506               , t507_returns_item t507
                    WHERE t506.c701_distributor_id IS NOT NULL
                        AND t506.c506_status_fl = 2
                        AND t506.c506_rma_id = t507.c506_rma_id
                        AND t507.c507_status_fl IN ('C', 'W')
                        AND t506.c506_void_fl IS NULL
                        AND t506.c1900_company_id = 1000
                        AND t506.c5040_plant_id = 3000
                        -- AND t506.c506_credit_date < TO_DATE ('6/30/2010', 'MM/DD/YYYY')
                        -- AND t506.c506_credit_date >= TO_DATE ('01/12/2010', 'MM/DD/YYYY')
                        AND t506.c701_distributor_id IN
                        (SELECT     c701_distributor_id
                            FROM t701_distributor x
                            WHERE x.c901_distributor_type IN (70106, 70103)
                        )
                    GROUP BY t507.c205_part_number_id
                ) icticareturnsstart,
                --ICT Excess 2010 not reconciled on/after 01/12/2010
                (
                SELECT     t505.c205_part_number_id p_num, 'ICTEXCESS2010_NOT_RECONCILED', SUM (t505.c505_item_qty) * -
                        1 c_qty
                    FROM t504_consignment t504, t505_item_consignment t505, t504a_consignment_excess t504a
                      , t205_part_number t205
                    WHERE t504.c701_distributor_id IS NOT NULL
                        AND t504.c504_consignment_id = t505.c504_consignment_id
                        AND t504.c504_consignment_id = t504a.c504_consignment_id
                        AND t504.c504_ship_date >= TO_DATE ('01/12/2010', 'MM/DD/YYYY')
                        AND t504.c504_status_fl = 4
                        AND t504.c504_type IN (4110)
                        AND t504a.c504a_status_fl = 0
                        AND TRIM (t505.c505_control_number) IS NOT NULL
                        AND c504_void_fl IS NULL
                        AND t504.c1900_company_id = 1000
                        AND t504.c5040_plant_id = 3000
                        --AND t504.c701_distributor_id NOT IN (506,373,569)
                        AND t504.c701_distributor_id IN
                        (SELECT     c701_distributor_id
                            FROM t701_distributor x
                            WHERE x.c901_distributor_type IN (70106, 70103, 70105)
                        )
                        AND t205.c205_part_number_id = t505.c205_part_number_id
                    GROUP BY t505.c205_part_number_id
                ) ictexcess2010_not_reconciled,
                --ICT Excess  not reconciled on/before 01/11/2010
                (
                SELECT     t505.c205_part_number_id p_num, 'ictexbef2010_not_reconciled', SUM (t505.c505_item_qty) * -
                        1 c_qty
                    FROM t504_consignment t504, t505_item_consignment t505, t504a_consignment_excess t504a
                      , t205_part_number t205
                    WHERE t504.c701_distributor_id IS NOT NULL
                        AND t504.c504_consignment_id = t505.c504_consignment_id
                        AND t504.c504_consignment_id = t504a.c504_consignment_id
                        AND t504.c504_ship_date <= TO_DATE ('01/11/2010', 'MM/DD/YYYY')
                        AND t504.c504_status_fl = 4
                        AND t504.c504_type IN (4110)
                        AND t504a.c504a_status_fl = 0
                        AND TRIM (t505.c505_control_number) IS NOT NULL
                        AND c504_void_fl IS NULL
                        AND t504.c1900_company_id = 1000
                        AND t504.c5040_plant_id = 3000
                        --AND t504.c701_distributor_id NOT IN (506,373,569)
                        AND t504.c701_distributor_id IN
                        (SELECT     c701_distributor_id
                            FROM t701_distributor x
                            WHERE x.c901_distributor_type IN (70106, 70103, 70105)
                        )
                        AND t205.c205_part_number_id = t505.c205_part_number_id
                    GROUP BY t505.c205_part_number_id
                ) ictexbef2010_not_reconciled,
                --India Returns sales consing prior to on before 01/01/2009
                (
                SELECT     t507.c205_part_number_id p_num, 'indiareturnbefore2009', SUM (t507.c507_item_qty) r_qty
                    FROM t506_returns t506               , t507_returns_item t507
                    WHERE t506.c701_distributor_id IS NOT NULL
                        AND t506.c506_status_fl = 2
                        AND t506.c506_rma_id = t507.c506_rma_id
                        AND t507.c507_status_fl IN ('C', 'W')
                        AND t506.c506_void_fl IS NULL
                        AND t506.c1900_company_id = 1000
                        AND t506.c5040_plant_id = 3000
                        -- AND t506.c506_credit_date <= TO_DATE ('07/07/2010', 'MM/DD/YYYY')
                        AND t506.c506_credit_date <= TO_DATE ('01/01/2009', 'MM/DD/YYYY')
                        AND t506.c701_distributor_id IN (121)
                    GROUP BY t507.c205_part_number_id
                ) indiareturnbefore2009,
                --India Returns sales consing between  7-Jul-10 and tilldate
                (
                SELECT     t507.c205_part_number_id p_num, 'indiareturnjulytoday2010', SUM (t507.c507_item_qty) r_qty
                    FROM t506_returns t506               , t507_returns_item t507
                    WHERE t506.c701_distributor_id IS NOT NULL
                        AND t506.c506_status_fl = 2
                        AND t506.c506_rma_id = t507.c506_rma_id
                        AND t507.c507_status_fl IN ('C', 'W')
                        AND t506.c506_void_fl IS NULL
                        AND t506.c1900_company_id = 1000
                        AND t506.c5040_plant_id = 3000
                        -- AND t506.c506_credit_date <= TO_DATE ('07/07/2010', 'MM/DD/YYYY')
                        AND t506.c506_credit_date >= TO_DATE ('07/07/2010', 'MM/DD/YYYY')
                        AND t506.c701_distributor_id IN (121)
                    GROUP BY t507.c205_part_number_id
                ) indiareturnjulytoday2010,
                --UK Returns sales consing    before 12/16/2008
                (
                SELECT     t507.c205_part_number_id p_num, 'ukreturnsbefore2009', SUM (t507.c507_item_qty) r_qty
                    FROM t506_returns t506               , t507_returns_item t507
                    WHERE t506.c701_distributor_id IS NOT NULL
                        AND t506.c506_status_fl = 2
                        AND t506.c506_rma_id = t507.c506_rma_id
                        AND t507.c507_status_fl IN ('C', 'W')
                        AND t506.c506_void_fl IS NULL
                        AND t506.c1900_company_id = 1000
                        AND t506.c5040_plant_id = 3000
                        -- AND t506.c506_credit_date <= TO_DATE ('07/07/2010', 'MM/DD/YYYY')
                        AND t506.c506_credit_date < TO_DATE ('12/16/2008', 'MM/DD/YYYY')
                        --and t506.c506_credit_date IS NOT NULL
                        --AND t506.c701_distributor_id IN (121,569)   --- made changes
                        AND t506.c701_distributor_id IN (375)
                    GROUP BY t507.c205_part_number_id
                ) ukreturnsbefore2009,
                --globus care sales consing returns between 10/5/2009 and 10/4/2010
                (
                SELECT     t507.c205_part_number_id p_num, 'globuscarereturnsales2010', SUM (t507.c507_item_qty) r_qty
                    FROM t506_returns t506               , t507_returns_item t507
                    WHERE t506.c701_distributor_id IS NOT NULL
                        AND t506.c506_status_fl = 2
                        AND t506.c506_rma_id = t507.c506_rma_id
                        AND t507.c507_status_fl IN ('C', 'W')
                        AND t506.c506_void_fl IS NULL
                        AND t506.c1900_company_id = 1000
                        AND t506.c5040_plant_id = 3000
                        AND t506.c506_credit_date >= TO_DATE ('10/05/2009', 'MM/DD/YYYY')
                        AND t506.c506_credit_date <= TO_DATE ('10/04/2010', 'MM/DD/YYYY')
                        AND t506.c701_distributor_id IN (506)
                    GROUP BY t507.c205_part_number_id
                ) globuscarereturnsales2010,
                --globus care sales consing returns  on before 09/30/2009 posting through Manual Load 16450000 -
                -- Modules and Cases - Consigned/Sales Consignment to ICT COGS //////70350000 - ICT COGS/Sales
                -- Consignment to ICT COGS
                (
                SELECT     t507.c205_part_number_id p_num, 'globuscarertnload2009', SUM (t507.c507_item_qty) r_qty
                    FROM t506_returns t506               , t507_returns_item t507
                    WHERE t506.c701_distributor_id IS NOT NULL
                        AND t506.c506_status_fl = 2
                        AND t506.c506_rma_id = t507.c506_rma_id
                        AND t507.c507_status_fl IN ('C', 'W')
                        AND t506.c506_void_fl IS NULL
                        AND t506.c1900_company_id = 1000
                        AND t506.c5040_plant_id = 3000
                        AND t506.c506_credit_date <= TO_DATE ('09/30/2009', 'MM/DD/YYYY')
                        AND t506.c701_distributor_id IN (506)
                    GROUP BY t507.c205_part_number_id
                ) globuscarertnload2009,
                --Ortho sales consign returns after 02-JUL-10  till date/first return on 02-JUL-10
                (
                SELECT     t507.c205_part_number_id p_num, 'orthoreturnsales2010', SUM (t507.c507_item_qty) r_qty
                    FROM t506_returns t506               , t507_returns_item t507
                    WHERE t506.c701_distributor_id IS NOT NULL
                        AND t506.c506_status_fl = 2
                        AND t506.c506_rma_id = t507.c506_rma_id
                        AND t507.c507_status_fl IN ('C', 'W')
                        AND t506.c506_void_fl IS NULL
                        AND t506.c1900_company_id = 1000
                        AND t506.c5040_plant_id = 3000
                        AND t506.c506_credit_date >= TO_DATE ('01/12/2010', 'MM/DD/YYYY')
                        AND t506.c701_distributor_id IN (569)
                    GROUP BY t507.c205_part_number_id
                ) orthoreturnsales2010
                --All sales consignment Cogs Error from dec 2009
              , (SELECT     c205_part_number_id p_num, SUM (c822_qty) cogs_qty
                    FROM t822_costing_error_log
                    WHERE c822_costing_error_log_id IS NOT NULL
                        AND c822_created_date >= TO_DATE ('12/01/2009', 'mm/dd/yyyy')
                        AND
                        (
                            c822_from_costing_type = '4904'
                            OR c822_to_costing_type = '4904'
                        )
                        -- AND c822_flag_fix_status IS NULL
                    GROUP BY c205_part_number_id
                ) cogserror,
                ----UK Correction temp_ac_correct_posting(375,NULL,'01/14/2010',NULL,48098,'303497');
                (
                SELECT     t507.c205_part_number_id p_num, 'ukcorrection', SUM (t507.c507_item_qty) r_qty
                    FROM t506_returns t506               , t507_returns_item t507
                    WHERE t506.c701_distributor_id IS NOT NULL
                        AND t506.c506_status_fl = 2
                        AND t506.c506_rma_id = t507.c506_rma_id
                        AND t507.c507_status_fl IN ('C', 'W')
                        AND t506.c506_void_fl IS NULL
                        AND t506.c1900_company_id = 1000
                        AND t506.c5040_plant_id = 3000
                        AND t506.c506_credit_date >= TO_DATE ('01/14/2010', 'MM/DD/YYYY')
                        AND t506.c506_credit_date <= TO_DATE ('05/26/2011', 'MM/DD/YYYY')
                        AND t506.c701_distributor_id IN (375)
                    GROUP BY t507.c205_part_number_id
                ) ukcorrection,
                --Belgium Correction temp_ac_correct_posting(456,NULL,'01/14/2010',NULL,48098,'303497');
                (
                SELECT     t507.c205_part_number_id p_num, 'belgiumcorrection', SUM (t507.c507_item_qty) r_qty
                    FROM t506_returns t506               , t507_returns_item t507
                    WHERE t506.c701_distributor_id IS NOT NULL
                        AND t506.c506_status_fl = 2
                        AND t506.c506_rma_id = t507.c506_rma_id
                        AND t507.c507_status_fl IN ('C', 'W')
                        AND t506.c506_void_fl IS NULL
                        AND t506.c1900_company_id = 1000
                        AND t506.c5040_plant_id = 3000
                        AND t506.c506_credit_date >= TO_DATE ('01/14/2010', 'MM/DD/YYYY')
                        AND t506.c506_credit_date <= TO_DATE ('05/26/2011', 'MM/DD/YYYY')
                        AND t506.c701_distributor_id IN (456)
                    GROUP BY t507.c205_part_number_id
                ) belgiumcorrection,
                ----Germany Correction temp_ac_correct_posting(480,NULL,'01/14/2010',NULL,48098,'303497');
                (
                SELECT     t507.c205_part_number_id p_num, 'germanycorrection', SUM (t507.c507_item_qty) r_qty
                    FROM t506_returns t506               , t507_returns_item t507
                    WHERE t506.c701_distributor_id IS NOT NULL
                        AND t506.c506_status_fl = 2
                        AND t506.c506_rma_id = t507.c506_rma_id
                        AND t507.c507_status_fl IN ('C', 'W')
                        AND t506.c506_void_fl IS NULL
                        AND t506.c1900_company_id = 1000
                        AND t506.c5040_plant_id = 3000
                        AND t506.c506_credit_date >= TO_DATE ('01/14/2010', 'MM/DD/YYYY')
                        AND t506.c506_credit_date <= TO_DATE ('05/26/2011', 'MM/DD/YYYY')
                        AND t506.c701_distributor_id IN (480)
                    GROUP BY t507.c205_part_number_id
                ) germanycorrection,
                ------India Correction temp_ac_correct_posting(121,NULL,'01/14/2010','05/06/2010',48098,'303497');
                (
                SELECT     t507.c205_part_number_id p_num, 'indiacorrectiondtrange', SUM (t507.c507_item_qty) r_qty
                    FROM t506_returns t506               , t507_returns_item t507
                    WHERE t506.c701_distributor_id IS NOT NULL
                        AND t506.c506_status_fl = 2
                        AND t506.c506_rma_id = t507.c506_rma_id
                        AND t507.c507_status_fl IN ('C', 'W')
                        AND t506.c506_void_fl IS NULL
                        AND t506.c1900_company_id = 1000
                        AND t506.c5040_plant_id = 3000
                        AND t506.c506_credit_date >= TO_DATE ('01/14/2010', 'MM/DD/YYYY')
                        AND t506.c506_credit_date <= TO_DATE ('05/06/2010', 'MM/DD/YYYY')
                        AND t506.c701_distributor_id IN (121)
                    GROUP BY t507.c205_part_number_id
                ) indiacorrectiondtrange,
                --Globus Care temp_ac_correct_posting(506,NULL,'10/06/2010',NULL,48097,'303497');
                (
                SELECT     t507.c205_part_number_id p_num, 'glcarecorrection', SUM (t507.c507_item_qty) r_qty
                    FROM t506_returns t506               , t507_returns_item t507
                    WHERE t506.c701_distributor_id IS NOT NULL
                        AND t506.c506_status_fl = 2
                        AND t506.c506_rma_id = t507.c506_rma_id
                        AND t507.c507_status_fl IN ('C', 'W')
                        AND t506.c506_void_fl IS NULL
                        AND t506.c506_credit_date >= TO_DATE ('10/06/2010', 'MM/DD/YYYY')
                        AND t506.c506_credit_date <= TO_DATE ('05/26/2011', 'MM/DD/YYYY')
                        AND t506.c701_distributor_id IN (506)
                        AND t506.c1900_company_id = 1000
                        AND t506.c5040_plant_id = 3000
                    GROUP BY t507.c205_part_number_id
                ) glcarecorrection,
                --Dr. Daniel Serban temp_ac_correct_posting(632,NULL,NULL,NULL,48097,'303497');
                (
                SELECT     t507.c205_part_number_id p_num, 'drdancorrection', SUM (t507.c507_item_qty) r_qty
                    FROM t506_returns t506               , t507_returns_item t507
                    WHERE t506.c701_distributor_id IS NOT NULL
                        AND t506.c506_status_fl = 2
                        AND t506.c506_rma_id = t507.c506_rma_id
                        AND t507.c507_status_fl IN ('C', 'W')
                        AND t506.c506_void_fl IS NULL
                        AND t506.c1900_company_id = 1000
                        AND t506.c5040_plant_id = 3000
                        AND t506.c506_credit_date <= TO_DATE ('05/26/2011', 'MM/DD/YYYY')
                        AND t506.c701_distributor_id IN (632)
                    GROUP BY t507.c205_part_number_id
                ) drdancorrection,
                --ortho medical correctionComment temp_gm_pkg_op_ict.gm_po_sav_ict_returns (ortho_ra.rmaid,
                -- ortho_ra.did, '303497', ortho_ra.dtype);
                (
                SELECT     t507.c205_part_number_id p_num, 'orthocorrection', SUM (t507.c507_item_qty) r_qty
                    FROM t506_returns t506               , t507_returns_item t507
                    WHERE t506.c701_distributor_id IS NOT NULL
                        AND t506.c506_status_fl = 2
                        AND t506.c506_rma_id = t507.c506_rma_id
                        AND t507.c507_status_fl IN ('C', 'W')
                        AND t506.c506_void_fl IS NULL
                        AND t506.c1900_company_id = 1000
                        AND t506.c5040_plant_id = 3000
                        AND t506.c506_credit_date >= TO_DATE ('01/11/2010', 'MM/DD/YYYY')
                        AND t506.c506_credit_date <= TO_DATE ('05/26/2011', 'MM/DD/YYYY')
                        AND t506.c701_distributor_id IN
                        (SELECT     c701_distributor_id
                            FROM t701_distributor x
                            WHERE x.c701_distributor_id IN (569)
                        )
                    GROUP BY t507.c205_part_number_id
                ) orthocorrection, --fisher back of --- returns are included in main US query and ALL ICT start coulns
                -- hence back off  from any one(main US query or ALL ICT start coulns)
                        (SELECT     t507.c205_part_number_id p_num, 'fisher', SUM (t507.c507_item_qty) r_qty
                            FROM t506_returns t506                , t507_returns_item t507
                            WHERE t506.c701_distributor_id IS NOT NULL
                                AND t506.c506_status_fl = 2
                                AND t506.c506_rma_id = t507.c506_rma_id
                                AND t507.c507_status_fl IN ('C', 'W')
                                AND t506.c506_void_fl IS NULL
                                AND t506.c1900_company_id = 1000
                                AND t506.c5040_plant_id = 3000
                                --AND t506.c506_credit_date >= TO_DATE ('01/11/2010', 'MM/DD/YYYY')
                                --AND t506.c506_credit_date <= TO_DATE ('05/26/2011', 'MM/DD/YYYY')
                                AND t506.c701_distributor_id IN
                                (SELECT     c701_distributor_id
                                    FROM t701_distributor x
                                    WHERE x.c701_distributor_id IN (648)
                                )
                            GROUP BY t507.c205_part_number_id
                        ) fisher,
                        --globus care sales consign exclusively  used in the formula ( new formula after correction all
                        -- returns) between
                        (
                        SELECT     t507.c205_part_number_id p_num, 'globuscarertn2009range', SUM (t507.c507_item_qty)
                                r_qty
                            FROM t506_returns t506, t507_returns_item t507
                            WHERE t506.c701_distributor_id IS NOT NULL
                                AND t506.c506_status_fl = 2
                                AND t506.c506_rma_id = t507.c506_rma_id
                                AND t507.c507_status_fl IN ('C', 'W')
                                AND t506.c506_void_fl IS NULL
                                AND t506.c1900_company_id = 1000
                                AND t506.c5040_plant_id = 3000
                                AND t506.c506_credit_date >= TO_DATE ('10/01/2009', 'MM/DD/YYYY')
                                AND t506.c506_credit_date <= TO_DATE ('01/10/2010', 'MM/DD/YYYY') -- 1 day less than
                                -- the Start date if ICT changes in 2010
                                AND t506.c701_distributor_id IN (506)
                            GROUP BY t507.c205_part_number_id
                        ) globuscarertn2009range
                        --All sales consignment Cogs Error from April 2011 after patty corrction
                      , (SELECT     c205_part_number_id p_num, 'cogsaprnow2011', SUM (c822_qty) cogs_qty
                            FROM t822_costing_error_log
                            WHERE c822_costing_error_log_id IS NOT NULL
                                AND c822_created_date >= TO_DATE ('04/19/2011', 'mm/dd/yyyy')
                                AND
                                (
                                    c822_from_costing_type = '4904'
                                    OR c822_to_costing_type = '4904'
                                )
                                -- AND c822_flag_fix_status IS NULL
                            GROUP BY c205_part_number_id
                        ) cogsaprnow2011
                    WHERE t205.c205_part_number_id = accounting_qty.p_num(+)
                        AND t205.c205_part_number_id = not_reconciled.p_num(+)
                        AND t205.c205_part_number_id = op_total.p_num(+)
                        AND t205.c205_part_number_id = itccons.p_num(+)
                        AND t205.c205_part_number_id = ictica2010returns.p_num(+)
                        AND t205.c205_part_number_id = indiasalescon2008.p_num(+)
                        AND t205.c205_part_number_id = indiacon2010.p_num(+)
                        AND t205.c205_part_number_id = indiaconsnotposted.p_num(+)
                        AND t205.c205_part_number_id = cyprussalescons2008.p_num(+)
                        AND t205.c205_part_number_id = ukcon2010.p_num(+)
                        AND t205.c205_part_number_id = uksalescon2008.p_num(+)
                        AND t205.c205_part_number_id = belgiumcon2010.p_num(+)
                        AND t205.c205_part_number_id = germanycon2010.p_num(+)
                        AND t205.c205_part_number_id = globuscarecon2010today.p_num(+)
                        AND t205.c205_part_number_id = globuscarebackoffdistcon2009.p_num(+)
                        AND t205.c205_part_number_id = globuscarertnload2009.p_num(+)
                        AND t205.c205_part_number_id = orthoconmayjune2010.p_num(+)
                        AND t205.c205_part_number_id = drdanielallcon.p_num(+)
                        AND t205.c205_part_number_id = polandallcon.p_num(+)
                        AND t205.c205_part_number_id = swissallcon.p_num(+)
                        AND t205.c205_part_number_id = southafricaallcon.p_num(+)
                        AND t205.c205_part_number_id = edc.p_num(+)
                        AND t205.c205_part_number_id = ictreturnsstart.p_num(+)
                        AND t205.c205_part_number_id = icticareturnsstart.p_num(+)
                        AND t205.c205_part_number_id = ictexcess2010_not_reconciled.p_num(+)
                        AND t205.c205_part_number_id = indiareturnbefore2009.p_num(+)
                        AND t205.c205_part_number_id = indiareturnjulytoday2010.p_num(+)
                        AND t205.c205_part_number_id = ukreturnsbefore2009.p_num(+)
                        AND t205.c205_part_number_id = globuscarereturnsales2010.p_num(+)
                        AND t205.c205_part_number_id = orthoreturnsales2010.p_num(+)
                        AND t205.c205_part_number_id = cogserror.p_num(+)
                        AND t205.c205_part_number_id = ictexbef2010_not_reconciled.p_num(+)
                        AND t205.c205_part_number_id = ukcorrection.p_num(+)
                        AND t205.c205_part_number_id = belgiumcorrection.p_num(+)
                        AND t205.c205_part_number_id = germanycorrection.p_num(+)
                        AND t205.c205_part_number_id = indiacorrectiondtrange.p_num(+)
                        AND t205.c205_part_number_id = glcarecorrection.p_num(+)
                        AND t205.c205_part_number_id = drdancorrection.p_num(+)
                        AND t205.c205_part_number_id = orthocorrection.p_num(+)
                        AND t205.c205_part_number_id = fisher.p_num(+)
                        AND t205.c205_part_number_id = globuscarertn2009range.p_num(+)
                        AND t205.c205_part_number_id = cogsaprnow2011.p_num(+)
        ) ;

