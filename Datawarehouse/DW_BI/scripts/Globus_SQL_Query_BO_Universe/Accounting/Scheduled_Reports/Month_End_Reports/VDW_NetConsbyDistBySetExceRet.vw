--@"D:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Scheduled_Reports\Month_End_Reports\VDW_NetConsbyDistBySetExceRet.vw";

CREATE OR REPLACE VIEW VDW_NetConsbyDistBySetExceRet AS
	SELECT * FROM  
		(SELECT   globus_app.get_distributor_name (b.c701_distributor_id) NAME, t207.c207_set_id SETID, t207.c207_set_nm SETNAME, NVL(a.cons,0) cons
		       , b.RETURN, (NVL(a.cons,0) - NVL (b.RETURN, 0)) total_consign, globus_app.get_code_name (t207.c207_category) CATEGORY
		    FROM t207_set_master t207
		       , (SELECT   t504.c701_distributor_id, t504.c207_set_id, COUNT (1) cons
		              FROM t504_consignment t504
		             WHERE t504.c701_distributor_id IS NOT NULL
		               AND t504.c504_status_fl = 4
		               AND t504.c504_type IN (4110)
		               AND c504_void_fl IS NULL
					   AND t504.c1900_company_id  =1000
						AND t504.c5040_plant_id    =3000	
		               AND TRIM (t504.c207_set_id) IS NOT NULL
		          GROUP BY t504.c701_distributor_id, t504.c207_set_id) a
		       , (SELECT   t506.c701_distributor_id, t506.c207_set_id, COUNT (1) RETURN
		              FROM t506_returns t506
		             WHERE t506.c701_distributor_id IS NOT NULL
		               AND t506.c506_status_fl = 2
		               AND t506.c506_void_fl IS NULL
		               AND TRIM (t506.c207_set_id) IS NOT NULL
					   AND t506.c1900_company_id  =1000
					   AND t506.c5040_plant_id    =3000	
		        	GROUP BY t506.c701_distributor_id, t506.c207_set_id
		        ) b
		   		WHERE b.c701_distributor_id  = a.c701_distributor_id (+)  AND b.c207_set_id  = a.c207_set_id (+)
		        AND t207.c207_set_id = b.c207_set_id
				ORDER BY NAME, t207.c207_set_id
		)WHERE total_consign < 0 AND cons = 0;


