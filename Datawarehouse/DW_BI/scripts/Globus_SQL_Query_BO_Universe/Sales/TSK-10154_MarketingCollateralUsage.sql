SELECT v700.rep_compid COMPANY_ID,
  COMPANY_NAME,
  v700.divid DIV_ID,
  v700.divname DIVISION_NAME,
  v700.GP_ID ZONE_ID,
  v700.GP_NAME ZONE_NAME,
  v700.REGION_ID REGION_ID,
  v700.REGION_NAME REGION_NAME,
  v700.AD_ID AD_ID,
  v700.AD_NAME AD_NAME,
  v700.D_ID DIST_ID,
  v700.D_NAME DISTRIBUTOR_NAME,
  v700.rep_id REP_ID,
  v700.rep_name rep_name,
  T1018.C1018_Share_File_Id FILE_ID, 
  t1018.c1018_last_updated_date CREATED_DATE
FROM globus_app.T1018_Share_File T1018,
  globus_app.T1016_Share T1016,
  t101_user t101,
  ( SELECT DISTINCT v700.rep_compid,
  get_company_name(v700.rep_compid)  COMPANY_NAME,
    v700.divid,
    v700.divname,
    v700.GP_ID,
    v700.GP_NAME,
    v700.REGION_ID,
    v700.REGION_NAME,
    v700.AD_ID,
    v700.AD_NAME,
    v700.D_ID,
    v700.D_NAME,
    v700.rep_id,
    v700.rep_name
  FROM v700_territory_mapping_detail v700
  )v700
WHERE T1018.C1016_Share_Id   = T1016.C1016_Share_Id
AND t101.c101_user_id        = T1016.C1016_Ref_Id
AND t101.c901_dept_id        =2005 
AND t101.c701_distributor_id = v700.d_id
AND T1016.C1016_Void_Fl     IS NULL
AND T1018.C1018_Void_Fl     IS NULL;
