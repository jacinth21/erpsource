SELECT EVENT.EVENT_ID as event_id, EVENT.EVENT_NAME as event_name, EVENT.SET_ID as SETID, t207.c207_set_nm as setname, get_code_name (t207.c207_type) as set_type
, EVENT.SET_QTY as REQUESTED_QTY, EVENT.SHIP_DATE as ship_date, EVENT.RETURN_DATE as return_date, LOANERS.AVAILABLE as loaner_pool_qty
  FROM 
(SELECT T504.c207_set_id SET_ID, t504.c504_type TYP,  COUNT(1) AVAILABLE
FROM t504a_consignment_loaner t504a, t504_consignment t504
WHERE t504.c504_consignment_id = t504a.c504_consignment_id
AND t504.c504_void_fl IS NULL
AND t504a.c504a_void_fl IS NULL
AND t504a.c504a_status_fl NOT IN(23,30,60) --60 IN ACTIVE 23- Deployed 30-WIP
AND t504.c207_set_id IS NOT NULL
AND t504.c504_type IN(4119, 4127)
GROUP BY T504.c207_set_id, t504.c504_type ) LOANERS
, (SELECT 
  t7100.c7100_case_info_id EVENT_ID
, t7103.c7103_name EVENT_NAME
, t7104.c207_set_id SET_ID
, COUNT(1) SET_QTY 
, t526.c526_required_date SHIP_DATE 
, t526.c526_exp_return_date RETURN_DATE
, t526.c901_request_type TYP
FROM t7100_case_information T7100
, t7103_schedule_info T7103
, t7104_case_set_information t7104
, t526_product_request_detail t526
Where t7100.c7100_case_info_id = t7103.c7100_case_info_id
and t7100.c7100_case_info_id = t7104.c7100_case_info_id
AND t7104.c526_product_request_detail_id = t526.c526_product_request_detail_id
AND t7100.c7100_void_fl IS NULL
and T7100.c901_case_status = 19524 -- ACTIVE
and t7100.c901_type  = 1006504 -- EVENT
AND t7104.c7104_void_fl is NULL
and t7104.c207_set_id IS NOT NULL
AND t526.c526_void_fl IS NULL
AND t526.c526_status_fl != 40 -- REJECTED
AND t526.c526_required_date IS NOT NULL
AND t526.c526_exp_return_date IS NOT NULL
GROUP BY t7100.c7100_case_info_id
, t7103.c7103_name
, t7104.c207_set_id
, t526.c526_required_date
, t526.c526_exp_return_date
, t526.c901_request_type
) EVENT
, T207_SET_MASTER T207
WHERE EVENT.SET_ID = LOANERS.SET_ID
AND EVENT.TYP = LOANERS.TYP
AND T207.C207_SET_ID = EVENT.SET_ID
ORDER BY  EVENT.EVENT_ID, EVENT.EVENT_NAME, EVENT.SET_ID;