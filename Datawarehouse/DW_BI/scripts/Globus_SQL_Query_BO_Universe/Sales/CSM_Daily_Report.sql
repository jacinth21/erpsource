SELECT   t101.NAME "REP/AD NAME", REP_category, t101.distributor, NVL(AD_NAME,'') AD_NAME, NVL(VP_NAME,'') VP_NAME,
         NVL (cas.cas_count, 0) total_case_booked_using_csm,
         NVL (case_today.cas_count, 0) case_booked_using_csm_today,
         NVL (DO.do_count, 0) total_do_using_csm,
         NVL (do_today.do_count, 0) do_using_csm_today,
         NVL (do_out.do_out_count, 0) total_do_outside_csm,
         NVL (do_out_today.do_out_count, 0) do_outside_csm_today,
         TO_CHAR (NVL (login.last_login, ''), 'MM/DD/YYYY') last_login,
         NVL (login_count.login_cnt_last_5_days, 0) login_cnt_last_5_days,
         NVL (loaner_set_requested, 0) loaner_set_requested,
         NVL (area_set_requested, 0) area_set_requested,
         NVL (outside_loaners_count, 0) outside_loaners_count
    FROM (SELECT   c101_user_id user_id,
                   COUNT (c103_login_ts) login_cnt_last_5_days
              FROM t103_user_login_track t103,
                   (SELECT user_id
                      FROM (SELECT DISTINCT t101p.NAME,
                                            t102.c101_user_id user_id
                                       FROM t102_user_login t102,
                                            (SELECT t101.c101_party_id,
                                                    (   t101.c101_first_nm
                                                     || ' '
                                                     || t101.c101_last_nm
                                                    ) NAME,
                                                    LOWER
                                                       (   SUBSTR
                                                              (t101.c101_first_nm,
                                                               1,
                                                               1
                                                              )
                                                        || t101.c101_last_nm
                                                       ) user_name
                                               FROM t1501_group_mapping t1501,
                                                    t101_party t101
                                              WHERE t1501.c1500_group_id =
                                                                   '100000027'
                                                AND t1501.c101_mapped_party_id =
                                                            t101.c101_party_id) t101p
                                      WHERE t101p.user_name = t102.c102_login_username(+)
                            UNION
                            SELECT    t101.c101_user_f_name
                                   || ' '
                                   || t101.c101_user_l_name NAME,
                                   t101.c101_user_id user_id
                              FROM t101_user t101
                             WHERE t101.c101_user_id IN (947, 858, 671, 670, 404 , 914 ))
                     WHERE user_id IS NOT NULL) t101
             WHERE t103.c101_user_id = t101.user_id
               AND TRUNC (t103.c103_login_ts) >= TRUNC (SYSDATE) - 5
          GROUP BY c101_user_id) login_count,
         (SELECT   c101_user_id user_id, MAX (c103_login_ts) last_login
              FROM t103_user_login_track t103,
                   (SELECT user_id
                      FROM (SELECT DISTINCT t101p.NAME,
                                            t102.c101_user_id user_id
                                       FROM t102_user_login t102,
                                            (SELECT t101.c101_party_id,
                                                    (   t101.c101_first_nm
                                                     || ' '
                                                     || t101.c101_last_nm
                                                    ) NAME,
                                                    LOWER
                                                       (   SUBSTR
                                                              (t101.c101_first_nm,
                                                               1,
                                                               1
                                                              )
                                                        || t101.c101_last_nm
                                                       ) user_name
                                               FROM t1501_group_mapping t1501,
                                                    t101_party t101
                                              WHERE t1501.c1500_group_id =
                                                                   '100000027'
                                                AND t1501.c101_mapped_party_id =
                                                            t101.c101_party_id) t101p
                                      WHERE t101p.user_name = t102.c102_login_username(+)
                            UNION
                            SELECT    t101.c101_user_f_name
                                   || ' '
                                   || t101.c101_user_l_name NAME,
                                   t101.c101_user_id user_id
                              FROM t101_user t101
                             WHERE t101.c101_user_id IN (947, 858, 671, 670, 404 , 914 ))
                     WHERE user_id IS NOT NULL) t101
             WHERE t103.c101_user_id = t101.user_id
          GROUP BY c101_user_id) login,
         (SELECT   t525.c703_sales_rep_id user_id,
                   COUNT (t504a.c504_consignment_id) outside_loaners_count
              FROM t504a_loaner_transaction t504a,
                   t526_product_request_detail t526,
                   t525_product_request t525
             WHERE TRUNC (t504a.c504a_created_date) >=
                                          TO_DATE ('01/02/2012', 'MM/DD/YYYY')
               AND t526.c526_product_request_detail_id =
                                          t504a.c526_product_request_detail_id
               AND t525.c525_product_request_id = t526.c525_product_request_id
               AND c504a_void_fl IS NULL
               AND t525.c525_void_fl IS NULL
               AND t526.c526_void_fl IS NULL
          GROUP BY t525.c703_sales_rep_id) osets,
         (SELECT   t7100.c703_sales_rep_id user_id,
                   SUM (DECODE (c901_set_location_type, 11381, 1, 0)
                       ) loaner_set_requested,
                   SUM (DECODE (c901_set_location_type, 11378, 1, 0)
                       ) area_set_requested
              FROM globus_app.t7104_case_set_information t7104,
                   globus_app.t7100_case_information t7100
             WHERE t7100.c7100_case_info_id = t7104.c7100_case_info_id
               AND t7104.c7104_void_fl IS NULL
               AND t7100.c7100_void_fl IS NULL
          GROUP BY t7100.c703_sales_rep_id) csets,
         (SELECT   t501.c703_sales_rep_id user_id, COUNT (1) do_out_count
              FROM t501_order t501, t102_user_login t102
             WHERE t501.c501_void_fl IS NULL
               AND TRUNC (t501.c501_created_date) >
                                          TO_DATE ('01/02/2012', 'MM/DD/YYYY')
               AND t501.c7100_case_info_id IS NULL
               AND t501.c703_sales_rep_id = t102.c101_user_id
               AND t501.c501_void_fl IS NULL
               AND t501.c501_parent_order_id IS NULL
          GROUP BY t501.c703_sales_rep_id) do_out,
         (SELECT   t501.c703_sales_rep_id user_id, COUNT (1) do_out_count
              FROM t501_order t501, t102_user_login t102
             WHERE t501.c501_void_fl IS NULL
               AND t501.c7100_case_info_id IS NULL
               AND t501.c501_void_fl IS NULL
               AND t501.c703_sales_rep_id = t102.c101_user_id
               AND t501.c501_parent_order_id IS NULL
               AND TRUNC (t501.c501_created_date) = TRUNC (SYSDATE)
          GROUP BY t501.c703_sales_rep_id) do_out_today,
         (SELECT   t501.c501_created_by user_id, COUNT (1) do_count
              FROM t501_order t501
             WHERE t501.c7100_case_info_id IS NOT NULL
               AND t501.c501_void_fl IS NULL
               AND t501.c501_parent_order_id IS NULL
          GROUP BY t501.c501_created_by) DO,
         (SELECT   t501.c501_created_by user_id, COUNT (1) do_count
              FROM t501_order t501
             WHERE t501.c7100_case_info_id IS NOT NULL
               AND t501.c501_void_fl IS NULL
               AND t501.c501_parent_order_id IS NULL
               AND TRUNC (t501.c501_created_date) = TRUNC (SYSDATE)
          GROUP BY t501.c501_created_by) do_today,
         (SELECT   t7100.c7100_created_by user_id, COUNT (1) cas_count
              FROM globus_app.t7100_case_information t7100
             WHERE t7100.c7100_void_fl IS NULL
          GROUP BY t7100.c7100_created_by) cas,
         (SELECT   t7100.c7100_created_by user_id, COUNT (1) cas_count
              FROM globus_app.t7100_case_information t7100
             WHERE t7100.c7100_void_fl IS NULL
               AND TRUNC (t7100.c7100_created_date) = TRUNC (SYSDATE)
          GROUP BY t7100.c7100_created_by) case_today,
         (SELECT t101.*, GET_CODE_NAME(t703.c703_rep_category) REP_category
                 , get_distributor_name (t703.c701_distributor_id) distributor, v700.AD_NAME, V700.VP_NAME
            FROM t703_sales_rep t703, (SELECT DISTINCT   AD_NAME, VP_NAME, rep_id  FROM v700_territory_mapping_detail v700) v700
                 ,(SELECT DISTINCT t101p.NAME, t102.c101_user_id user_id
                             FROM t102_user_login t102,
                                  (SELECT t101.c101_party_id,
                                          (   t101.c101_first_nm
                                           || ' '
                                           || t101.c101_last_nm
                                          ) NAME,
                                          LOWER
                                             (   SUBSTR (t101.c101_first_nm,
                                                         1,
                                                         1
                                                        )
                                              || t101.c101_last_nm
                                             ) user_name
                                     FROM t1501_group_mapping t1501,
                                          t101_party t101
                                    WHERE t1501.c1500_group_id = '100000027'
                                      AND t1501.c101_mapped_party_id =
                                                            t101.c101_party_id) t101p
                            WHERE t101p.user_name = t102.c102_login_username(+)
                  UNION
                  SELECT    t101.c101_user_f_name
                         || ' '
                         || t101.c101_user_l_name NAME,
                         t101.c101_user_id user_id
                    FROM t101_user t101
                   WHERE t101.c101_user_id IN (947, 858, 671, 670,404 , 914 )) t101
           WHERE user_id IS NOT NULL 
           AND user_id = t703.c703_sales_rep_id(+)           
           AND user_id = v700.rep_id(+)
           ) t101
   WHERE t101.user_id = cas.user_id(+)
     AND t101.user_id = case_today.user_id(+)
     AND t101.user_id = DO.user_id(+)
     AND t101.user_id = do_today.user_id(+)
     AND t101.user_id = do_out.user_id(+)
     AND t101.user_id = do_out_today.user_id(+)
     AND t101.user_id = login.user_id(+)
     AND t101.user_id = login_count.user_id(+)
     AND t101.user_id = csets.user_id(+)
     AND t101.user_id = osets.user_id(+)
     AND NAME != 'Michael Melchionni'
     AND NAME != 'Inactive - Terry McLeod'
     AND t101.user_id not in (404 , 914)
ORDER BY distributor, NAME;