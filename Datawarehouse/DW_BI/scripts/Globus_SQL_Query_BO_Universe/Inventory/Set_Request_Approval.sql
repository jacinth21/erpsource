SELECT *
FROM (
  (SELECT t7100.c7100_created_date REQUESTED_DATE,
    t7104.c526_product_request_detail_id REQUEST_ID,
    t526.c525_product_request_id PARENT_REQUEST_ID ,
    NVL(T7104.C207_Set_Id,T7104.C205_Part_Number_Id) SET_ID,
    DECODE(T7104.C207_Set_Id, NULL,get_partnum_desc (T7104.c205_part_number_id),get_set_name (t7104.c207_set_id)) SET_NAME,
    GET_DISTRIBUTOR_NAME(t525.c525_request_for_id) FIELD_SALES ,
     DECODE(400087, 400088, get_user_name(t7100.c7100_created_by),400087,get_user_name(t7100.c7100_created_by) ,get_rep_name (t7100.c703_sales_rep_id)) REQUESTOR_NAME,
    get_latest_log_comments(c7100_case_id,26240283) COMMENTS 
  FROM t7100_case_information t7100,
    t7104_case_set_information t7104,
    t526_product_request_detail t526 ,
    t7103_schedule_info t7103,
    t901_code_lookup t901,
    t525_product_request t525 ,
    (SELECT DISTINCT v700.d_id
    FROM v700_territory_mapping_detail v700
    WHERE ((COMPID               = DECODE(100803,100803, NULL,100803)
    OR DECODE(100803,100803,1,2) = 1))
    AND DIVID = 100823
    AND V700.REP_COMPID NOT     IN
      (SELECT C906_RULE_ID
      FROM T906_RULES
      WHERE C906_RULE_GRP_ID = 'ADDNL_COMP_IN_SALES'
      AND C906_VOID_FL      IS NULL
      )
    ) v700
  WHERE t7104.c7100_case_info_id            = t7100.c7100_case_info_id
  AND t7104.c526_product_request_detail_id  = t526.c526_product_request_detail_id
  AND t525.c525_product_request_id          = t526.c525_product_request_id
  AND t7104.c526_product_request_detail_id IS NOT NULL
  AND t7103.c7100_case_info_id (+)          = t7100.c7100_case_info_id
  AND t526.c901_request_type                =400087
  AND t7104.c7104_shipped_del_fl            IS NULL
  AND t7100.c901_case_status                IN (11091,19524)
  AND t526.c526_status_fl                   ='5'
  AND t526.c526_void_fl                     IS NULL
  AND t7100.c7100_void_fl                   IS NULL
  AND t7104.c7104_void_fl                   IS NULL
  AND t525.C1900_company_id                 = 1000
  AND t7100.c901_type                       = t901.c901_code_id
  AND t7100.c901_type                       =1006506
  AND t7100.c701_distributor_id             = v700.d_id
  ) )
WHERE REQUEST_ID IS NOT NULL
ORDER BY REQUESTED_DATE,REQUEST_ID,SET_ID