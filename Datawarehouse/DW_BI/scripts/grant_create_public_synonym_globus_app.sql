DECLARE
    CURSOR cur_objects
    IS
       SELECT object_name, owner,  object_type
          FROM all_objects
         WHERE owner IN ('GLOBUS_APP') AND object_type IN ('PROCEDURE','PACKAGE','PACKAGE BODY','MATERIALIZED VIEW','TABLE','FUNCTION','VIEW','SEQUENCE');

	/* this script will prompt you for the schema owner*/
BEGIN
    FOR rec_objects IN cur_objects
    LOOP
        BEGIN
          --  DBMS_OUTPUT.put_line (rec_objects.object_name);

            EXECUTE IMMEDIATE (   'create or replace  public synonym '
                               || rec_objects.object_name
                               || ' for '
                               || rec_objects.owner
                               || '.'
                               || rec_objects.object_name
                              );
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;
    END LOOP;
END;
/
