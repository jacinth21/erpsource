SELECT T101.C101_USER_F_NAME           AS "First Name" ,
  T101.C101_USER_L_NAME                AS "Last Name",
  T101.C101_USER_ID                    AS "User Id",
  T101.C101_CREATED_DATE               AS "Created Date",
  TO_CHAR( T101.C101_CREATED_DATE,'MM/YYYY') "Month_year",
  T102.C102_LOGIN_USERNAME             AS "Login User Name",
  GET_CODE_NAME(T102.C901_AUTH_TYPE)   AS "Authentication type",
  GET_CODE_NAME(T101.C901_USER_STATUS) AS "Status",
  T906C.C906C_LDAP_NAME                AS "LDAP Name",
  T101.c1900_def_company_id            AS "Company"
FROM T101_USER T101 ,
  T102_USER_LOGIN T102 ,
  T906C_LDAP_MASTER T906C
WHERE T101.C101_USER_ID       = T102.C101_USER_ID
AND T101.C101_CREATED_DATE  >= to_date(@prompt('Enter From Date For User:','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss')
AND T101.C101_CREATED_DATE  <= to_date(@prompt('Enter To Date For User:','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss')
AND T102.C906C_LDAP_MASTER_ID = T906C.C906C_LDAP_MASTER_ID
ORDER BY T101.C101_CREATED_DATE DESC;