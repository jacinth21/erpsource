ALTER TABLE T602_QUESTION_MASTER ADD C602_QUESTION_NM VARCHAR2(4000); 

UPDATE T602_QUESTION_MASTER SET C602_QUESTION_NM = REGEXP_REPLACE (C602_QUESTION_DS, '<font color=#FF9900> -  |<font color=#FF9900> |<font color="#FF0000"> |<font color=#FF0000>|</font>| </font>|<B><U>|</U></B>|<B>|</B>| \(Last Name only\)| \(specify\)| \(specify\)| \(specify\)| \(mark only one\)|  \(mark only one\) |  \(mark only one\) |  \(mark only one\)');

COMMIT;