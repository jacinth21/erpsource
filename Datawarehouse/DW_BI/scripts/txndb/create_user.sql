spool E:/Database/DW_BI/dbscripts/spool/createuser.sql
set head off feed off verify off
set linesize 5000;

select 'create user globus_bi_realtime identified by birtbirt default tablespace GLOBUS_PROD_PERM temporary tablespace GLOBUS_PROD_TEMP;' from dual;
select 'grant connect, resource to globus_bi_realtime;' from dual;
select 'grant EXECUTE ANY PROCEDURE to globus_bi_realtime;' from dual;
select 'grant SELECT ANY TABLE to globus_bi_realtime;' from dual;

spool off;
@"E:/Database/DW_BI/dbscripts/spool/createuser.sql";
commit;
