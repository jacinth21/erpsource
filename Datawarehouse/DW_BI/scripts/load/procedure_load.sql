-- @sqlplusw user/pwd@GMIDWSTG @"C:/Database/DW_BI/scripts/load/procedure_and_static_data_Load.sql"

spool E:\Database\DW_BI\Log\procedure_Load.log
set head off feed off verify off
set linesize 5000;

@"E:\Database\DW_BI\packages\realtime\gm_dw_pkg_sm_salesfact.pkg";
@"E:\Database\DW_BI\packages\realtime\gm_dw_pkg_sm_salesfact.bdy";
@"E:\Database\DW_BI\packages\realtime\gm_dw_pkg_pd_partdata.pkg";
@"E:\Database\DW_BI\packages\realtime\gm_dw_pkg_pd_partdata.bdy";
@"E:\Database\DW_BI\packages\realtime\gm_dw_pkg_util.pkg";
@"E:\Database\DW_BI\packages\realtime\gm_dw_pkg_util.bdy";
@"E:\Database\DW_BI\packages\realtime\gm_dw_pkg_clinical_util.pkg";
@"E:\Database\DW_BI\packages\realtime\gm_dw_pkg_clinical_util.bdy";


@"E:\Database\DW_BI\packages\realtime\gm_pkg_system_set_hierarchy.pkg";
@"E:\Database\DW_BI\packages\realtime\gm_pkg_system_set_hierarchy.bdy";

spool off;
