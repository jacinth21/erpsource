spool D:\DataBackup\Log\Product_Development_Load.log
SET HEADING   OFF 
SET ECHO      ON
set feedback on
set linesize 5000
set TIMING ON

@"D:\Database\DW_BI\MaterializedViews\Product Development\PART_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Product Development\PART_ATTRIBUTE_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Product Development\SET_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Product Development\SET_PART_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Product Development\SYSTEM_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Product Development\GROUP_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Product Development\GROUP_PART_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Product Development\PART_COMPANY_MAPPING_DIM.VW";
exec gm_pkg_system_set_hierarchy.load_set_hierarchy;

spool off;
