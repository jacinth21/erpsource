spool D:\DataBackup\Log\Operations_Load.log
SET HEADING   OFF 
SET ECHO      ON
set feedback on
set linesize 5000
set TIMING ON

--DIM
@"D:\Database\DW_BI\MaterializedViews\Operations\DHR_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Operations\NCMR_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Operations\PURCHASE_ORDER_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Operations\VENDOR_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Operations\WORK_ORDER_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Operations\INVENTORY_LOCATION_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Operations\Shipping_Dim.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\TAG_DIM.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\Request_Dim.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\Request_Detail_Dim.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\Consignment_Master_Dim.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\ITEM_CONSIGNMENT_MASTER_DIM.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\Consignment_Dim.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\Item_Consignment_Dim.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\PRODUCT_REQUEST_DIM.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\PRODUCT_REQUEST_DETAIL_DIM.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\LOANER_EXTENSION_DETAIL_DIM.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\INHOUSE_TRANSACTION_DIM.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\INHOUSE_TRANSACTION_ITEM_DIM.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\CONSIGNMENT_LOANER_DIM.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\VENDOR_PRICING_DETAILS_DIM.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\CONSIGNMENT_EXCESS_DIM.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\PART_CONTROL_NUMBER_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Operations\RETURNS_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Operations\RETURNS_ITEM_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Operations\COSTING_DIM.vw";


--FACT
@"D:\Database\DW_BI\MaterializedViews\Operations\WORK_ORDER_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Operations\DHR_NCMR_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Operations\INVENTORY_LOCATION_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Operations\INVENTORY_LOCATION_LOG_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Operations\TAG_FACT.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\TAG_LOG_FACT.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\Request_Fact.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\Consignment_Fact.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\PRODUCT_REQUEST_FACT.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\CONSIGNMENT_LOANER_FACT.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\INHOUSE_TRANSACTION_FACT.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\VENDOR_PRICING_FACT.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\RETURNS_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Operations\RETURNS_REPROCESS_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Sales_FieldInventory\LOANER_INVENTORY_FACT.vw";

GRANT SELECT ON OUS_PART_INVENTORY_FACT TO GLOBUS_DM_OPERATION;

spool off;
