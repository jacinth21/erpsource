--  @"D:\database\DW_BI\scripts\load\mview_refresh.sql";
-- exec dbms_mview.REFRESH('currency_rule_dim');
spool D:\DataBackup\Log\BBA_partinventory.log
SET HEADING   OFF 
SET ECHO      ON
set feedback on
set linesize 5000
set TIMING ON

-- Load part inventory fact
@"D:\database\DW_BI\MaterializedViews\Operations\PART_INVENTORY_FACT.vw";

spool off;
exit;
