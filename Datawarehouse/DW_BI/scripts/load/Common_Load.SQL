spool D:\DataBackup\Log\Common_Load.log
SET HEADING   OFF 
SET ECHO      ON
set feedback on
set linesize 5000
set TIMING ON

@"D:\Database\DW_BI\MaterializedViews\Common\Void_Dim.VW";
@"D:\Database\DW_BI\MaterializedViews\Common\CURRENCY_RULE_DIM.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\Shipping_Dim.VW";
@"D:\Database\DW_BI\MaterializedViews\Common\COMPANY_DIM.VW";
@"D:\Database\DW_BI\MaterializedViews\Common\DEALER_DIM.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\PLANT_COMPANY_MAPPING_DIM.VW";
@"D:\Database\DW_BI\MaterializedViews\Operations\PLANT_DIM.VW";

spool off;