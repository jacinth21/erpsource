spool D:\DataBackup\Log\Data_Validation.log
SET HEADING   OFF 
SET ECHO      ON
set feedback on
set linesize 5000
set TIMING ON

--@"C:\Database\DW_BI\scripts\T980_LOAD_DW_LOG.sql";
@"C:\Database\DW_BI\packages\gm_dw_pkg_cm_data_validation.pkg";
@"C:\Database\DW_BI\packages\gm_dw_pkg_cm_data_validation.bdy";


exec gm_dw_pkg_cm_data_validation.gm_cm_validate_main;
commit;

spool off;