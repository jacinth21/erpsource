--@"e:\Database\DW_BI\scripts\load\Dashboard_Load.sql"

spool e:\Database\DW_BI\Log\Dashboard_Load.sql
set head off feed off verify off
set linesize 5000;

-- @"e:\Database\DW_BI\Dashboard\Drop_fact_tables.sql";
-- @"e:\Database\DW_BI\scripts\load\procedure_and_static_data_Load.sql";
exec globus_bi_realtime.gm_pkg_system_set_hierarchy.load_set_hierarchy;

@"e:\Database\DW_BI\Dashboard\DATE_DIM.SQL";
@"e:\Database\DW_BI\Dashboard\ins_date_dim.sql";
@"e:\Database\DW_BI\Dashboard\YEAR_DIM.sql";
@"e:\Database\DW_BI\Dashboard\QUARTER_DIM.sql";
@"e:\Database\DW_BI\Dashboard\MONTH_DIM.sql";
@"e:\Database\DW_BI\Dashboard\WEEK_DIM.sql";

@"e:\Database\DW_BI\Dashboard\ACCOUNT_DIM.vw";
@"e:\Database\DW_BI\Dashboard\ORDER_DIM.vw";
@"e:\Database\DW_BI\Dashboard\ITEM_ORDER_DIM.vw";
@"e:\Database\DW_BI\Dashboard\SET_DIM.VW";
@"e:\Database\DW_BI\Dashboard\PART_DIM.vw";
@"e:\Database\DW_BI\Dashboard\SET_PART_DIM.vw";
@"e:\Database\DW_BI\Dashboard\SYSTEM_DIM.vw";
@"e:\Database\DW_BI\Dashboard\LOANER_DIM.vw";

@"e:\Database\DW_BI\Dashboard\SALES_REP_DIM.VW";
@"e:\Database\DW_BI\Dashboard\DISTRIBUTOR_DIM.VW";

@"e:\Database\DW_BI\Dashboard\SALES_TERRITORY_DIM.VW";
@"e:\Database\DW_BI\Dashboard\SALES_REGION_DIM.VW";
@"e:\Database\DW_BI\Dashboard\SALES_ZONE_DIM.VW";

@"e:\Database\DW_BI\Dashboard\SALES_FACT.vw";
@"e:\Database\DW_BI\Dashboard\DAILY_SALES_FACT.vw";
	
@"e:\Database\DW_BI\Dashboard\QUOTA_FACT.vw";
@"e:\Database\DW_BI\Dashboard\FIELD_CONSIGN_INVENTORY_FACT.vw";
@"e:\Database\DW_BI\Dashboard\LOANER_INVENTORY_FACT.vw";

@"e:\Database\DW_BI\Dashboard\TOP_N_ACCOUNTS_DAILY.vw";
@"e:\Database\DW_BI\Dashboard\TOP_N_DIRECT_SALES_REP_DAILY.vw";
@"e:\Database\DW_BI\Dashboard\TOP_N_DISTRIBUTORS_DAILY.vw";
@"e:\Database\DW_BI\Dashboard\TOP_N_SYSTEMS_DAILY.vw";

@"e:\Database\DW_BI\Dashboard\TOP_N_ACCOUNTS_WEEKLY.vw";
@"e:\Database\DW_BI\Dashboard\TOP_N_DIRECT_SALES_REP_WEEKLY.vw";
@"e:\Database\DW_BI\Dashboard\TOP_N_DISTRIBUTORS_WEEKLY.vw";
@"e:\Database\DW_BI\Dashboard\TOP_N_SYSTEMS_WEEKLY.vw";

@"e:\Database\DW_BI\Dashboard\TOP_N_ACCOUNTS_MONTHLY.vw";
@"e:\Database\DW_BI\Dashboard\TOP_N_DIRECT_SALES_REP_MONTHLY.vw";
@"e:\Database\DW_BI\Dashboard\TOP_N_DISTRIBUTORS_MONTHLY.vw";
@"e:\Database\DW_BI\Dashboard\TOP_N_SYSTEMS_MONTHLY.vw";

@"e:\Database\DW_BI\Dashboard\TOP_N_ACCOUNTS_QUARTERLY.vw";
@"e:\Database\DW_BI\Dashboard\TOP_N_DIRECT_SALES_REP_QRTLY.vw";
@"e:\Database\DW_BI\Dashboard\TOP_N_DISTRIBUTORS_QUARTERLY.vw";
@"e:\Database\DW_BI\Dashboard\TOP_N_SYSTEMS_QUARTERLY.vw";

@"e:\Database\DW_BI\Dashboard\TOP_N_ACCOUNTS_YEARLY.vw";
@"e:\Database\DW_BI\Dashboard\TOP_N_DIRECT_SALES_REP_YEARLY.vw";
@"e:\Database\DW_BI\Dashboard\TOP_N_DISTRIBUTORS_YEARLY.vw";
@"e:\Database\DW_BI\Dashboard\TOP_N_SYSTEMS_YEARLY.vw";
@"e:\Database\DW_BI\Dashboard\CURRENT_INFO.sql";

spool off;

