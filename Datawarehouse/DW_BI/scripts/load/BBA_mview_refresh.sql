--  @"D:\database\DW_BI\scripts\load\mview_refresh.sql";
-- exec dbms_mview.REFRESH('currency_rule_dim');
spool D:\DataBackup\Log\BBA_mviewrefesh.log
SET HEADING   OFF 
SET ECHO      ON
set feedback on
set linesize 5000
set TIMING ON

-- drop fact tables so that the constraint doesnt kick out
@"D:\Database\DW_BI\MaterializedViews\Sales_FieldInventory\Drop_fact_tables.sql";


exec dbms_mview.REFRESH('ADDRESS_DIM.VW');
exec dbms_mview.REFRESH('COMMENTS_DIM.VW');
exec dbms_mview.REFRESH('COMMENTS_FACT.VW');
exec dbms_mview.REFRESH('COMMENTS_LATEST_FACT.VW');
exec dbms_mview.REFRESH('CURRENCY_RULE_DIM.VW');
exec dbms_mview.REFRESH('STATUS_DETAIL_MASTER_DIM.VW');
exec dbms_mview.REFRESH('STATUS_FACT.VW');
exec dbms_mview.REFRESH('Void_Dim.VW');
exec dbms_mview.REFRESH('PART_DIM.vw');
exec dbms_mview.REFRESH('SET_DIM.vw');
exec dbms_mview.REFRESH('SET_PART_DIM.vw');
exec dbms_mview.REFRESH('SYSTEM_DIM.vw');
exec dbms_mview.REFRESH('ACCOUNT_DIM.vw');
exec dbms_mview.REFRESH('DISTRIBUTOR_DIM.vw');
exec dbms_mview.REFRESH('ITEM_ORDER_DIM.vw');
exec dbms_mview.REFRESH('ORDER_DIM.vw');
exec dbms_mview.REFRESH('QUOTA_FACT.VW');
exec dbms_mview.REFRESH('RELEASE_FOR_SALES_DIM.VW');
exec dbms_mview.REFRESH('SALES_COMPANY_DIM.vw');
exec dbms_mview.REFRESH('SALES_DIVISION_DIM.vw');
exec dbms_mview.REFRESH('SALES_REGION_DIM.vw');
exec dbms_mview.REFRESH('SALES_REP_DIM.vw');
exec dbms_mview.REFRESH('SALES_SECURITY_DIM.VW');
exec dbms_mview.REFRESH('SALES_TERRITORY_DIM.vw');
exec dbms_mview.REFRESH('SALES_ZONE_DIM.vw');
exec dbms_mview.REFRESH('ACCOUNT_REP_MAPPING_DIM.vw');
exec dbms_mview.REFRESH('SHIPPING_DIM.vw');

@"D:\Database\DW_BI\MaterializedViews\Sales_FieldInventory\UPD_DATE_DIM_FOR_SALES.sql";
@"D:\Database\DW_BI\MaterializedViews\Sales_FieldInventory\SALES_FACT_TILL_PREVIOUS_YEAR.vw"; -- should be removed in future
@"D:\Database\DW_BI\MaterializedViews\Sales_FieldInventory\SALES_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Sales_FieldInventory\DO_CLASSIFICATION_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Sales_FieldInventory\BBA_SALES_COMMISION_FACT.vw";

spool off;
exit;
