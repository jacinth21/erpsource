--@"D:\Database\DW_BI\scripts\load\Account_Load_Pricing.sql";
spool D:\DataBackup\Log\Account_Load_Pricing.log
SET HEADING   OFF 
SET ECHO      ON
set feedback on
set linesize 5000
set TIMING ON

exec gm_pkg_account_part_pricing.gm_upd_last_updated_date;
commit;

exec gm_pkg_account_part_pricing.gm_save_acc_part_price;
commit;

spool off;
exit;
