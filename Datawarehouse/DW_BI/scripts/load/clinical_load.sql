-- @sqlplusw user/pwd@GMIDWSTG @"C:/Database/DW_BI/scripts/load/clinical_load.sql"
spool D:\DataBackup\Log\Clinical_Load.log
SET HEADING   OFF 
SET ECHO      ON
set feedback on
set linesize 5000

@"D:\Database\DW_BI\MaterializedViews\Clinical\ANSWER_GROUP_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\STUDY_PERIOD_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\STUDY_FORM_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\SURGEON_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\FORM_MASTER_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\CLINICAL_STUDY_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\QUESTION_MASTER_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\PATIENT_MASTER_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\PATIENT_ANSWER_LIST_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\PATIENT_DETAILS_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\FORM_QUESTION_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\STUDY_SITE_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\ANSWER_LIST_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\CLINICAL_STUDY_PATIENT_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\PATIENT_STUDY_QUALIFIC_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\PATIENT_ADVERSE_EVENT_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\PATIENT_VAS_SCORE_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\NECK_DISABILITY_INDEX_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\PATIENT_PREOPERATIVE_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\PATIENT_PREOPERAT_SURVEY_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\PATIENT_HEALTH_STATUS_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\NEUROLOGICAL_STATUS_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\PATIENT_RADIOLOGIC_ASSESS_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\PATIENT_QUALIFICATION_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\OSWESTRY_DISABILITY_INDEX_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\ZURICH_CLAUDICATION_QUEST_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\POST_OPERAT_PATIENT_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\POST_OPERAT_PATIENT_SURVEY_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\CALCULATED_SCORES_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\PATIENT_OPERATIVE_DATA_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\HOSPITAL_DISCHARGE_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\PATIENT_TWO_YEAR_STUDY_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\PATIENT_RADIOGRAPH_UPLOAD_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\Clinical\SURGICAL_TECHNIQUE_FACT.vw";

spool off;
