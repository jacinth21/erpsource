--@"D:\Database\DW_BI\scripts\load\Pricing.sql"

spool D:\DataBackup\Log\Pricing.log
SET HEADING   OFF 
SET ECHO      ON
set feedback on
set linesize 5000
set TIMING ON


@"D:\Database\DW_BI\MaterializedViews\Pricing\ICT_PRICING_FACT.VW";
--@"D:\Database\DW_BI\MaterializedViews\Pricing\ACCOUNT_PRICING_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\Pricing\PART_PRICE_MAPPING.VW";
@"D:\Database\DW_BI\MaterializedViews\Pricing\ACCOUNT_GPB_MAPP.vw";

spool off;

