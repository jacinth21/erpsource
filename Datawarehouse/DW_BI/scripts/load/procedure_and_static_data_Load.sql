-- @sqlplusw user/pwd@GMIDWSTG @"C:/Database/DW_BI/scripts/load/procedure_and_static_data_Load.sql"

spool E:\Database\DW_BI\Log\procedure_and_static_data_Load.log
set head off feed off verify off
set linesize 5000;

@"E:\Database\DW_BI\packages\gm_dw_pkg_sm_salesfact.pkg";
@"E:\Database\DW_BI\packages\gm_dw_pkg_sm_salesfact.bdy";
@"E:\Database\DW_BI\packages\gm_dw_pkg_pd_partdata.pkg";
@"E:\Database\DW_BI\packages\gm_dw_pkg_pd_partdata.bdy";
@"E:\Database\DW_BI\packages\gm_dw_pkg_util.pkg";
@"E:\Database\DW_BI\packages\gm_dw_pkg_util.bdy";
@"E:\Database\DW_BI\packages\GET_CS_REP_LOANER_BILL_NM_ADD.fnc";

@"E:\Database\DW_BI\packages\gm_pkg_system_set_hierarchy.pkg";
@"E:\Database\DW_BI\packages\gm_pkg_system_set_hierarchy.bdy";

@"E:\Database\DW_BI\MaterializedViews\Common\DATE_DIM.SQL";
@"E:\Database\DW_BI\MaterializedViews\Common\ins_date_dim.sql";
@"E:\Database\DW_BI\MaterializedViews\Common\YEAR_DIM.sql";
@"E:\Database\DW_BI\MaterializedViews\Common\QUARTER_DIM.sql";
@"E:\Database\DW_BI\MaterializedViews\Common\MONTH_DIM.sql";
@"E:\Database\DW_BI\MaterializedViews\Common\WEEK_DIM.sql";


spool off;
exit;
