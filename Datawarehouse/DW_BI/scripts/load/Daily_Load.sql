--@"D:\Database\DW_BI\MaterializedViews\Sales_FieldInventory\Daily_Load.sql"
spool D:\DataBackup\Log\Daily_Load.log
SET HEADING   OFF 
SET ECHO      ON
set feedback on
set linesize 5000
set TIMING ON

@"D:\Database\DW_BI\MaterializedViews\Sales_FieldInventory\Drop_fact_tables.sql";

@"D:\Database\DW_BI\scripts\load\Common_Load.sql";
@"D:\Database\DW_BI\scripts\load\Product_Development_Load.sql";
@"D:\Database\DW_BI\scripts\load\Sales_FieldInventory_Load.sql";
@"D:\Database\DW_BI\scripts\load\Operations_Load.sql";
@"D:\Database\DW_BI\scripts\load\Quality_Load.sql";
@"D:\Database\DW_BI\scripts\load\Pricing.sql";
@"D:\Database\DW_BI\scripts\load\Accounting_Load.sql";
@"D:\Database\DW_BI\scripts\load\Index_Creation.sql";
--@"D:\Database\DW_BI\scripts\load\Data_Validation.sql";


spool off;
exit;
