/* Get tag information */
SELECT t5010.C5010_TAG_ID "Tag Id"
, t5010.C5010_LAST_UPDATED_TRANS_ID "Consignment Id"
, t5010.C207_SET_ID "Set Id"
, cnln.C504A_ETCH_ID "Etch Id"
, get_code_name(cnln.C901_CONSIGNED_TO) "Consignment Type"
, DECODE(cnln.C901_CONSIGNED_TO 
    ,50170,get_distributor_name(cnln.C504A_CONSIGNED_TO_ID) 
    ,50172,get_account_name(cnln.C504A_CONSIGNED_TO_ID) 
    ,50171,get_user_name(cnln.C504A_CONSIGNED_TO_ID)) "Consigned To Name"
, get_rep_name(cnln.C703_SALES_REP_ID) "Sales Rep Name"
, get_account_name(cnln.C704_ACCOUNT_ID) "Account Name"
FROM GLOBUS_APP.T5010_TAG t5010
 -- Combine consignment information with the most recent transaction history available
, (SELECT CN.C504_CONSIGNMENT_ID
  , cn.C504A_ETCH_ID
  , lntx.C901_CONSIGNED_TO
  , lntx.C504A_CONSIGNED_TO_ID
  , lntx.C703_SALES_REP_ID
  , lntx.C704_ACCOUNT_ID
  FROM
  -- Get consignment ID and Etch id if applicable
    (SELECT T504.C504_CONSIGNMENT_ID
    , T504A.C504A_ETCH_ID
    FROM GLOBUS_APP.T504_CONSIGNMENT t504
    , GLOBUS_APP.T504A_CONSIGNMENT_LOANER t504a
    WHERE T504.C504_CONSIGNMENT_ID = t504a.C504_CONSIGNMENT_ID(+)
    ) cn
  -- Get the last transaction associated with each CN (used for finding the last owner of the set)
  , (SELECT t504b.C504_CONSIGNMENT_ID
    , t504b.C504A_CREATED_DATE
    , t504b.C504A_LOANER_DT
    , t504b.C504A_EXPECTED_RETURN_DT
    , t504b.C504A_RETURN_DT
    , t504b.C901_CONSIGNED_TO
    , t504b.C504A_CONSIGNED_TO_ID
    , t504b.C703_SALES_REP_ID
    , t504b.C704_ACCOUNT_ID
    FROM T504A_LOANER_TRANSACTION t504b
    ,(SELECT t504c.C504_CONSIGNMENT_ID
      , MAX(t504c.C504A_CREATED_DATE) C504A_CREATED_DATE
      FROM T504A_LOANER_TRANSACTION t504c
      WHERE t504c.C504A_VOID_FL IS NULL
      GROUP BY t504c.C504_CONSIGNMENT_ID
      ) maxdt
    WHERE t504b.C504_CONSIGNMENT_ID = maxdt.C504_CONSIGNMENT_ID
    AND t504b.C504A_CREATED_DATE    = maxdt.C504A_CREATED_DATE
    AND t504b.C504A_VOID_FL        IS NULL
    ) lntx
  WHERE cn.C504_CONSIGNMENT_ID = lntx.C504_CONSIGNMENT_ID(+)
  ) cnln
,GLOBUS_APP.T207_SET_MASTER t207
WHERE T5010.C5010_LAST_UPDATED_TRANS_ID = cnln.C504_CONSIGNMENT_ID
AND T5010.C207_SET_ID                   = T207.C207_SET_ID
AND t5010.C5010_VOID_FL                IS NULL;