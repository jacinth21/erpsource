SELECT
 TO_NUMBER (c504a_loaner_transaction_id) as loaner_id
, T504A.C504A_CONSIGNED_TO_ID as DISTRIBUTOR_ID
, get_distributor_name(t504a.c504a_consigned_to_id) as distributor_name
, T504A.C703_SALES_REP_ID as  SALES_REP_ID
, get_rep_name(t504a.c703_sales_rep_id) as  sales_Rep_name
, t504a.c704_account_id as ACCOUNT_KEY_ID
, T504A.C704_ACCOUNT_ID as ACCOUNT_ID
, geT_account_name(T504A.C704_ACCOUNT_ID)  as account_name
, T504.C207_SET_ID as SET_ID
, GET_SET_NAME(T504.C207_SET_ID)  as SET_NAME
, V207A.C207_SET_ID as SYSTEM_ID
, get_set_name(V207A.C207_SET_ID)  as system_name
, LOAN_DATE_DIM.DATE_KEY_ID as LOAN_DATE_KEY_ID
, LOAN_DATE_DIM.CAL_DATE as LOAN_DATE
, SUM(T505.C505_ITEM_PRICE * T505.C505_ITEM_QTY)  as TOTAL_LOANER_VALUE
, SUM(T505.C505_ITEM_QTY) as item_qty
FROM t504a_loaner_transaction t504a
       , GLOBUS_APP.T504_CONSIGNMENT T504
       , GLOBUS_APP.T504A_CONSIGNMENT_LOANER T504B
       , GLOBUS_APP.T505_ITEM_CONSIGNMENT T505
       , GLOBUS_APP.DATE_DIM LOAN_DATE_DIM
       , v207a_set_consign_link v207a
WHERE
	t504a.c504_consignment_id = t504.c504_consignment_id
     AND t504a.c504_consignment_id = t504b.c504_consignment_id
     AND t504a.c504_consignment_id = t505.C504_CONSIGNMENT_ID
     and LOAN_DATE_DIM.CAL_DATE = T504A.C504A_LOANER_DT
     AND V207A.C207_ACTUAL_SET_ID (+) = T504.C207_SET_ID
     AND t504.C504_TYPE = '4127'
     AND t504a.C901_CONSIGNED_TO = 50170
     AND t504a.c504a_void_fl IS NULL
     AND t504.C504_VOID_FL IS NULL
     and T505.C505_VOID_FL is null
group by C504A_LOANER_TRANSACTION_ID, C504A_CONSIGNED_TO_ID, LOAN_DATE_DIM.DATE_KEY_ID, t504.C207_SET_ID, 
c504a_return_dt, t504a.c504a_loaner_dt,t504a.c703_sales_rep_id, t504a.c704_account_id,LOAN_DATE_DIM.CAL_DATE,V207A.C207_SET_ID;