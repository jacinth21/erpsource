INSERT INTO 
T207A_SET_LINK T207A (T207A.T207A_SET_LINK_ID, C207_MAIN_SET_ID, C207_LINK_SET_ID, C901_TYPE)
SELECT S207A_SET_LINK.nextval,'UNS.101',  INN.setid,20002
FROM 
 (SELECT DISTINCT T730.C207_SET_ID setid
FROM t730_virtual_consignment t730
MINUS
SELECT distinct c207_actual_set_id 
FROM v207a_set_consign_link v207) INN;

COMMIT;

DROP MATERIALIZED VIEW	v207a_set_consign_link;

CREATE MATERIALIZED VIEW v207a_set_consign_link
	AS (SELECT linkdata.m_set_id c207_set_id, get_set_name(linkdata.m_set_id) c207_set_nm, linkdata.c207_link_set_id c207_link_set_id, t207.c901_cons_rpt_id c901_link_type, linkdata.actual_set_id c207_actual_set_id 
    , get_set_sequence(linkdata.m_set_id) c207_seq_no, linkdata.ss c207a_shared_status, linkdata.SETTYP c207a_type
    , t207.C901_HIERARCHY C901_HIERARCHY, DECODE(t207.C901_HIERARCHY,20703,20705,t207.C901_HIERARCHY) C901_CUSTOM_HIERARCHY
FROM t207_set_master t207,
( SELECT   t207a.c207_main_set_id m_set_id,t207a.c207_link_set_id c207_link_set_id,CONNECT_BY_ROOT t207a.c207_link_set_id actual_set_id,t207a.C901_SHARED_STATUS ss, CONNECT_BY_ROOT t207a.C901_TYPE SETTYP
              FROM t207a_set_link t207a
             WHERE t207a.c207_main_set_id IN (SELECT t207.c207_set_id
                                                FROM t207_set_master t207
                                               WHERE t207.c901_set_grp_type = 1600
                                               AND t207.c207_void_fl IS NULL
                                                 )
        CONNECT BY PRIOR c207_main_set_id = t207a.c207_link_set_id
        AND CONNECT_BY_ROOT t207a.C901_TYPE  <> 20004
)               linkdata
WHERE linkdata.actual_set_id = t207.c207_set_id 
 AND NVL(linkdata.ss,20740) NOT IN (20741) -- for removing all secondary shared set
)
/

