--PC-3380
--Updated By : Paddy Srinivasan
SELECT t251.c205_part_number_id as Part_Number,t205.C205_PART_NUM_DESC as Part_Desc ,t202.c202_Project_nm as Project_Name, get_code_name(c205_product_family) as Product_Family, C251_TPR as TPR_Qty      
   FROM globus_dm_operation.t251_inventory_lock_details t251, t205_part_number t205, t202_project t202  
   WHERE t205.c205_part_number_id = t251.c205_part_number_id
   AND t205.c202_project_id = t202.c202_project_id
    AND t251.c250_inventory_lock_id=(SELECT t250.c250_inventory_lock_id
			FROM globus_dm_operation.t250_inventory_lock t250
			 WHERE t250.c901_lock_type = 20430
             --20430 Main Inventory Lock
	         AND t250.c4016_global_inventory_map_id  = 1
	         AND t250.c250_void_fl IS NULL
             AND TO_CHAR(C250_LOCK_DATE,'MM/YYYY')= @prompt('Enter MM/YYYY','A',,mono,free,persistent))
    AND C251_TPR                 > 0       
    AND t205.c205_part_number_id = t251.c205_part_number_id