--PC-375_Item_Forecast_By_Country
--Created By : Padmanathan Srinivasan
SELECT countryname.c901_code_nm  AS country_name,
    t4020.c4020_demand_master_id AS demand_master_id,
    t4020.c4020_demand_nm        AS demand_name ,
    dmd_type.c901_code_nm        AS demand_type,
    t4040.c4040_demand_period_dt AS order_planning_load_Dt,
    t1910.c1910_division_name division_name ,
    t205.c202_project_id      AS project_id,
    t202.c202_project_nm      AS project_name,
    prod_famly.c901_code_nm   AS product_family,
    t4042.c205_part_number_id AS part_number ,
    t4042.c4042_qty           AS qty,
    t4042.c4042_period        AS period
FROM globus_dm_operation.t4042_demand_sheet_detail t4042,
    globus_dm_operation.t4040_demand_sheet t4040,
    globus_dm_operation.t4020_demand_master t4020,
    t205_part_number t205,
    t202_project t202,
    t901_code_lookup countryname,
    t901_code_lookup dmd_type,
    t901_code_lookup prod_famly,
    t1910_division t1910
WHERE t4040.c4040_demand_period_dt = @prompt('Enter GOP Load Month MM/DD/YYYY','D',,mono,,persistent)
AND t4040.c4040_demand_sheet_id    = t4042.c4040_demand_sheet_id
AND t4040.c4020_demand_master_id   = t4020.c4020_demand_master_id
AND t4042.c205_part_number_id      = t205.c205_part_number_id
AND t205.c202_project_id           = t202.c202_project_id
AND t4042.c901_type                = 50563
AND c4042_qty                     != 0
AND c901_ref_type                 IN (102660,102661,102662)
AND t4040.c901_access_type         = 102623
AND t4040.c4040_void_fl           IS NULL
AND t4020.c4020_void_fl           IS NULL
AND t4040.c901_level_value         = countryname.c901_code_id 
AND t4020.c901_demand_type         = dmd_type.c901_code_id 
AND t205.c205_product_family       = prod_famly.c901_code_id 
AND t202.c1910_division_id         =t1910.c1910_division_id
AND countryname.c901_void_fl IS NULL
AND dmd_type.c901_void_fl IS NULL
AND prod_famly.c901_void_fl IS NULL
-- and T205.C205_PART_NUMBER_ID='124.000'
ORDER BY t4040.c901_level_value,
    t4020.c901_demand_type,
    t4042.c4042_period ,
    t4042.c205_part_number_id