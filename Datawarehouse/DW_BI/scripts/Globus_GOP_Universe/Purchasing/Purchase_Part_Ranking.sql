
SELECT 
t205.c205_part_number_id  AS "Part Number Id",
t205.c205_part_num_desc  AS "Part Desc" ,
t202.c202_project_id AS "Project Id",
t202.c202_project_nm AS "Project name" ,
t1910.c1910_division_name AS "Division Name",
t4072.c4072_sales_qty AS "Unit_Qty",
t4072.c4072_sales_percent AS "Sales Percent",
t4072.c4072_sales_per_overall AS "Sales Percent Overall",
t901_curr_mon.c901_code_nm AS  "Rank classification" ,
t901_last_mon.c901_code_nm AS "Last Month Rank",
t901_last_prev_mon.c901_code_nm AS "Previous Month Rank",
To_char(T4071.C4071_Load_date,'MM/YYYY') AS "period_date",
T4071.C4071_updated_date AS "Updated Date"
FROM  t205_part_number t205,
		  t202_project t202, 
		  t1910_division t1910,
		  GLOBUS_DM_OPERATION.t4071_purchase_part_rank t4071, 
          GLOBUS_DM_OPERATION.t4072_purchase_part_rank_detail t4072, 
          t901_code_lookup t901_curr_mon, 
          t901_code_lookup t901_last_mon, 
          t901_code_lookup t901_last_prev_mon
	WHERE t4071.c4071_purchase_part_rank_id = t4072.c4071_purchase_part_rank_id
    AND t205.c205_part_number_id        = t4072.c205_part_number_id
   	AND t202.c202_project_id            = t205.c202_project_id
   	AND t1910.c1910_division_id         = t202.c1910_division_id
    AND t4072.c4072_rank = t901_curr_mon.c901_code_id (+)
    AND t4072.c4072_last_mon_rank = t901_last_mon.c901_code_id (+) 
    AND t4072.c4072_prev_last_mon_rank = t901_last_prev_mon.c901_code_id (+) 
   	AND t4071.c4071_void_fl IS NULL
   	AND t202.C202_void_fl IS NULL
   	AND t1910.C1910_void_fl IS NULL
   ORDER BY t4072.c4072_sales_per_overall,t205.c205_part_number_id;
