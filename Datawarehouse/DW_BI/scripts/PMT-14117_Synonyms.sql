-- create synonym for Power BI views
CREATE OR REPLACE PUBLIC SYNONYM pbi_company_dim FOR GLOBUS_APP.pbi_company_dim;

CREATE OR REPLACE PUBLIC SYNONYM pbi_sales_hierarchy_dim FOR GLOBUS_APP.pbi_sales_hierarchy_dim;

CREATE OR REPLACE PUBLIC SYNONYM pbi_division_dim FOR GLOBUS_APP.pbi_division_dim;

CREATE OR REPLACE PUBLIC SYNONYM pbi_region_company_dim FOR GLOBUS_APP.pbi_region_company_dim;

CREATE OR REPLACE PUBLIC SYNONYM pbi_territory_dim FOR GLOBUS_APP.pbi_territory_dim;

CREATE OR REPLACE PUBLIC SYNONYM pbi_account_dim FOR GLOBUS_APP.pbi_account_dim;

CREATE OR REPLACE PUBLIC SYNONYM pbi_distributor_dim FOR GLOBUS_APP.pbi_distributor_dim;

CREATE OR REPLACE PUBLIC SYNONYM pbi_sales_rep_dim FOR GLOBUS_APP.pbi_sales_rep_dim;

CREATE OR REPLACE PUBLIC SYNONYM pbi_realtime_date_dim FOR GLOBUS_APP.pbi_realtime_date_dim;

CREATE OR REPLACE PUBLIC SYNONYM pbi_realtime_month_dim FOR GLOBUS_APP.pbi_realtime_month_dim;

CREATE OR REPLACE PUBLIC SYNONYM pbi_system_dim FOR GLOBUS_APP.pbi_system_dim;

CREATE OR REPLACE PUBLIC SYNONYM pbi_set_part_dim FOR GLOBUS_APP.pbi_set_part_dim;

CREATE OR REPLACE PUBLIC SYNONYM pbi_realtime_quota_fact FOR GLOBUS_APP.pbi_realtime_quota_fact;

CREATE OR REPLACE PUBLIC SYNONYM pbi_realtime_sales_fact FOR GLOBUS_APP.pbi_realtime_sales_fact;