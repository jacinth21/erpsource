/*Run In Globus_Dw_Stage*/

CREATE TABLE T905_STATUS_DETAILS
(
C905_STATUS_DETAILS                 NUMBER   NOT NULL PRIMARY KEY,                   
C905_REF_ID                         VARCHAR2(20) ,               
C905_STATUS_FL                      CHAR(3) ,                    
C905_UPDATED_BY                     VARCHAR2(10) ,               
C905_DELETE_FL                      CHAR(1),                     
C905_UPDATED_DATE                   DATE ,                       
C901_TYPE                           NUMBER ,                     
C901_SOURCE                         NUMBER,                      
C905_LOANER_TURN_NO                 NUMBER ,                     
C1900_COMPANY_ID                    NUMBER,                      
C905_LAST_UPDATED_DATE_UTC          TIMESTAMP(6) WITH TIME ZONE, 
C905_ETL_CREATED_DATE               DATE,                        
C905_ETL_UPDATED_DATE               DATE);