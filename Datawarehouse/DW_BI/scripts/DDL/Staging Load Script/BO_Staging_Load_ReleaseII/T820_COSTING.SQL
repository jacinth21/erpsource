/*Run In Globus_Dw_Stage*/

CREATE TABLE T820_COSTING (
C820_COSTING_ID                 NUMBER NOT NULL PRIMARY KEY,      
C205_PART_NUMBER_ID             VARCHAR2(20), 
C820_PART_RECEIVED_DT           DATE,         
C820_QTY_RECEIVED               NUMBER,       
C820_PURCHASE_AMT               NUMBER(15,4) NOT NULL,
C820_TXN_ID                     VARCHAR2(20), 
C820_QTY_ON_HAND                NUMBER,       
C820_QTY_ADDED                  NUMBER,       
C901_STATUS                     NUMBER,       
C820_DELETE_FL                  CHAR(1),      
C820_CREATED_BY                 VARCHAR2(10), 
C820_CREATED_DATE               DATE,         
C820_LAST_UPDATED_BY            VARCHAR2(10), 
C820_LAST_UPDATED_DATE          DATE,         
C901_COSTING_TYPE               NUMBER,       
C820_PURCHASE_AMT_TEMP          NUMBER(15,4), 
C1900_COMPANY_ID                NUMBER,
C820_ETL_CREATED_DATE           DATE,
C820_ETL_UPDATED_DATE           DATE);