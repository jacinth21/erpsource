/*Run In Globus_Dw_Stage*/

CREATE TABLE T901A_LOOKUP_ACCESS (
C901A_CODE_ACCESS_ID                 NUMBER  NOT NULL PRIMARY KEY,                    
C901_ACCESS_CODE_ID                  NUMBER,                     
C901_CODE_ID                         NUMBER,                      
C1900_COMPANY_ID                     NUMBER,                      
C901A_LAST_UPDATED_DATE_UTC          TIMESTAMP(6) WITH TIME ZONE,
C901A_ETL_CREATED_DATE               DATE); 