/*Run In Globus_Dw_Stage*/

CREATE TABLE T2052_PART_PRICE_MAPPING (
C2052_PART_PRC_MAP_ID                NUMBER  NOT NULL PRIMARY KEY,                    
C205_PART_NUMBER_ID                  VARCHAR2(20),                
C2052_EQUITY_PRICE                   NUMBER,                     
C2052_LOANER_PRICE                   NUMBER,                      
C2052_CONSIGNMENT_PRICE              NUMBER,                      
C2052_LIST_PRICE                     NUMBER,                      
C1900_COMPANY_ID                     NUMBER,                      
C2052_UPDATED_DATE                   DATE,                        
C2052_UPDATED_DATE_UTC               TIMESTAMP(6) WITH TIME ZONE, 
C2052_UPDATED_BY                     VARCHAR2(20),                
C2052_VOID_FL                        VARCHAR2(1),                 
C2052_EQT_PRICE_HISTORY_FL           VARCHAR2(1),                 
C2052_LIST_PRICE_HISTORY_FL          VARCHAR2(1),                 
C2052_LAST_UPDATED_BY                VARCHAR2(20),                
C2052_LAST_UPDATED_DATE              DATE,                        
C2052_LAST_UPDATED_DATE_UTC          TIMESTAMP(6) WITH TIME ZONE, 
C2052_PRICE_COMMENTS                 VARCHAR2(4000),
C2052_ETL_CREATED_DATE               DATE,
C2052_ETL_UPDATED_DATE               DATE); 