/*Run In Globus_Dw_Stage*/

CREATE TABLE T107_CONTACT(
C107_CONTACT_ID                     NUMBER NOT NULL PRIMARY KEY,                      
C101_PARTY_ID                       NUMBER,                      
C901_MODE                           NUMBER,                     
C107_CONTACT_TYPE                   VARCHAR2(100),               
C107_CONTACT_VALUE                  VARCHAR2(200),               
C107_SEQ_NO                         NUMBER(4),                   
C107_INACTIVE_FL                    CHAR(1),                     
C107_VOID_FL                        VARCHAR2(1),                 
C107_CREATED_BY                     VARCHAR2(10),                
C107_CREATED_DATE                   DATE,                        
C107_LAST_UPDATED_BY                VARCHAR2(10),                
C107_LAST_UPDATED_DATE              DATE,                        
C107_PRIMARY_FL                     VARCHAR2(1),                 
C107_LAST_UPDATED_DATE_UTC          TIMESTAMP(6) WITH TIME ZONE,
C107_ETL_CREATED_DATE               DATE,
C107_ETL_UPDATED_DATE               DATE);  