/*Run In Globus_Dw_Stage*/

CREATE TABLE T704B_ACCOUNT_CPC_MAPPING (
C704B_ACCOUNT_CPC_MAP_ID             NUMBER,                     
C704_ACCOUNT_ID                      VARCHAR2(20),                
C704B_CPC_NAME                       VARCHAR2(200),               
C704B_CPC_ADDRESS                    VARCHAR2(1000),              
C704B_CPC_BARCODE_LBL_VER            VARCHAR2(100),               
C704B_CPC_FINAL_CPC_VER              VARCHAR2(100),               
C704B_ACTIVE_FL                      VARCHAR2(1),                 
C704B_VOID_FL                        VARCHAR2(1),                 
C704B_LAST_UPDATED_BY                VARCHAR2(10),                
C704A_LAST_UPDATED_DATE              DATE,                        
C1900_COMPANY_ID                     NUMBER,                      
C5040_PLANT_ID                       NUMBER,                      
C704B_LAST_UPDATED_DATE_UTC          TIMESTAMP(6) WITH TIME ZONE,
C704B_ETL_CREATED_DATE               DATE,
C704B_ETL_UPDATED_DATE               DATE);