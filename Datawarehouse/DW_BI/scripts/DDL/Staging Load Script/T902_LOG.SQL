/*Run In Globus_Dw_Stage*/

CREATE TABLE T902_LOG
(
C902_LOG_ID                         NUMBER NOT NULL PRIMARY KEY,                     
C902_REF_ID                         VARCHAR2(20),                
C902_COMMENTS                       VARCHAR2(4000),              
C902_TYPE                           NUMBER,                      
C902_CREATED_BY                     VARCHAR2(10),                
C902_CREATED_DATE                   DATE,                        
C902_LAST_UPDATED_BY                VARCHAR2(10),                
C902_LAST_UPDATED_DATE              DATE,                        
C902_VOID_FL                        VARCHAR2(1),                 
C1900_COMPANY_ID                    NUMBER,                      
C902_LAST_UPDATED_DATE_UTC          TIMESTAMP(6) WITH TIME ZONE, 
C902_ETL_CREATED_DATE               DATE,                        
C902_ETL_UPDATED_DATE               DATE); 