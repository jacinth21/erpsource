SELECT t504.c504_consignment_id Consignment_ID, t701.c701_distributor_name Distributor_Name, t505.c205_part_number_id Part_Number,t205.C205_PART_NUM_DESC Part_Desc
      , t505.c505_item_qty Quantity, t505.c505_control_number Control_Number
      , ROUND (get_account_part_pricing ('', t505.c205_part_number_id, t701.c101_party_intra_map_id)) as ICT_Price
   FROM t504_consignment t504, t505_item_consignment t505, t701_distributor t701, t205_part_number t205
  WHERE t504.c504_consignment_id = t505.c504_consignment_id
  AND t505.c205_part_number_id=T205.c205_part_number_id
    AND t505.c504_consignment_id in @prompt('Enter CN','A',,multi,free,persistent)
    AND t701.c701_distributor_id = t504.c701_distributor_id;