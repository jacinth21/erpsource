/* Formatted on 2012/04/26 11:31 (Formatter Plus v4.8.8) */
SELECT   t202.c202_project_nm as project, t205.c205_part_number_id As partnumber,
         t205.c205_part_num_desc As partdesc, t205.c205_list_price As listprice,
         DECODE (t705_gpo.uprice_gpo,
                 NULL, NVL (t705.uprice, 0),
                 t705_gpo.uprice_gpo
                ) as uprice,
         ROUND (  DECODE (DECODE (t705_gpo.uprice_gpo,
                                  NULL, NVL (t705.uprice, 0),
                                  t705_gpo.uprice_gpo
                                 ),
                          0, 0,
                            1
                          - (  DECODE (t705_gpo.uprice_gpo,
                                       NULL, NVL (t705.uprice, 0),
                                       t705_gpo.uprice_gpo
                                      )
                             / t205.c205_list_price
                            )
                         )
                * 100,
                0
               ) doffered,
         DECODE (t705_gpo.uprice_gpo, NULL, '', '*') As gpo, t705.account_id As Account_Id
    FROM t205_part_number t205,
         t202_project t202,
         (SELECT c205_part_number_id, t705.c705_unit_price uprice,
                 t705.c705_discount_offered doffered, t705.c704_account_id AS Account_Id
            FROM t705_account_pricing t705
           WHERE t705.c704_account_id = @prompt('Enter Account ID','A',,mono,free,persistent) AND t705.c705_void_fl IS NULL
          UNION ALL
          SELECT t4011.c205_part_number_id, t7051.c7051_price uprice,
                 0 doffered,t7051.c704_account_id Account_Id
            FROM t7051_account_group_pricing t7051, t4011_group_detail t4011
           WHERE t7051.c704_account_id = @prompt('Enter Account ID','A',,mono,free,persistent)
             AND t7051.c901_ref_type = 52000
             AND t7051.c7051_ref_id = t4011.c4010_group_id
             AND t4011.c4011_primary_part_lock_fl = 'Y') t705,
         (SELECT c205_part_number_id, t705.c705_unit_price uprice_gpo
            FROM t705_account_pricing t705
           WHERE c101_party_id = get_account_gpo_id (@prompt('Enter Account ID','A',,mono,free,persistent))
             AND t705.c705_void_fl IS NULL
          UNION ALL
          SELECT t4011.c205_part_number_id, t7051.c7051_price uprice_gpo
            FROM t7051_account_group_pricing t7051, t4011_group_detail t4011
           WHERE t7051.c101_gpo_id = get_account_gpo_id (@prompt('Enter Account ID','A',,mono,free,persistent))
             AND t7051.c901_ref_type = 52000
             AND t7051.c7051_ref_id = t4011.c4010_group_id
             AND t4011.c4011_primary_part_lock_fl = 'Y') t705_gpo,
         v205d_part_attr
   WHERE t205.c205_part_number_id = t705.c205_part_number_id(+)
     AND t205.c205_part_number_id = t705_gpo.c205_part_number_id(+)
     AND t205.c205_list_price IS NOT NULL
     AND t205.c205_list_price <> 0
     AND t202.c901_status_id = '20302'
     AND t205.c205_part_number_id = v205d_part_attr.pnum
     AND t202.c202_project_id = t205.c202_project_id
     AND (t705.uprice <> 0 OR t705_gpo.uprice_gpo <> 0)
     AND t202.c202_project_id = t205.c202_project_id
     AND t705.account_id = @prompt('Enter Account ID','A',,mono,free,persistent)
ORDER BY t202.c202_project_nm, t205.c205_part_number_id