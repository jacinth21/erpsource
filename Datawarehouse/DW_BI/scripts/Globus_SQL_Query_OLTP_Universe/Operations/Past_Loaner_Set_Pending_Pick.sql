Select T5010.C5010_Tag_Id Tagid, T504.C207_Set_Id Setid,Get_Set_Name(T504.C207_Set_Id) as SetDesc, T504a.C504_Consignment_Id Cnid
                  , t525.c525_product_request_id reqid , Gm_Pkg_Op_Set_Pick_Rpt.Get_Loc_Cd_From_Id (T5053.C5052_Location_Id) Locationcd
                  ,   t504a.c504a_etch_id etchid, T504a.C504a_Planned_Ship_Date planship_date
                   FROM t504a_consignment_loaner t504a, t504_consignment t504, t504a_loaner_transaction t504a1, t526_product_request_detail t526
                  , t525_product_request t525, t5010_tag t5010, t5053_location_part_mapping t5053
                  WHERE t504a.c504_consignment_id = t504.c504_consignment_id
        AND t504a.c504_consignment_id = t504a1.c504_consignment_id
                    AND t504a.c504_consignment_id = t5010.c5010_last_updated_trans_id(+)
                    AND t5010.c5010_tag_id                = t5053.c5010_tag_id (+)
                    AND t504a.c504a_status_fl             = 7 -- Pending Pick
                    AND t525.c525_product_request_id        = t526.c525_product_request_id
                   -- AND t504.c504_consignment_id = t5050.c5050_ref_id(+)
                    And T526.C526_Product_Request_Detail_Id = T504a1.C526_Product_Request_Detail_Id
                      AND t504.c504_consignment_id = t504a.c504_consignment_id  
                    AND t525.c525_void_fl                  IS NULL
                    AND t504a.c504a_void_fl                IS NULL
                    AND T504A1.C504A_VOID_FL               IS NULL
                    AND t525.c1900_company_id = 1000 
                    AND t5010.c1900_company_id = 1000
                     AND T504a.C504a_Planned_Ship_Date < trunc(sysdate)
                    AND T526.C526_Void_Fl                  IS NULL
                    AND t504a1.c504a_return_dt IS NULL       
                    And T5010.C5010_Void_Fl(+)             Is Null
                Order By to_number(Reqid) Desc, T504.C207_Set_Id
