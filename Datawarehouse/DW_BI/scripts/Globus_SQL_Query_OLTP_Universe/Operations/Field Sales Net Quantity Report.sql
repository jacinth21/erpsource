PC-2436 - Field Sales Net Quantity Report
By Suganthi Sharmila


SELECT t1900.c1900_company_name comp_name,
  t5052.c5052_ref_id Dist_iD,
  t703.c703_sales_rep_id Sales_rep,globus_app.get_rep_login_name(t703.c703_sales_rep_id) Rep_name,
  t701.c701_distributor_name location,
  location_type.c901_code_nm loctype,
  t5053.c205_part_number_id pnum,
  t205.c205_part_num_desc pdesc ,
  t5053.c5053_curr_qty qty,
  bill_state.c901_code_nm bill_state,
  product_family.c901_code_nm product_family
FROM t5053_location_part_mapping t5053,
  t5051_inv_warehouse t5051,
  t5052_location_master t5052 ,
  t205_part_number t205,
  t701_distributor t701,t703_sales_rep t703,
  t901_code_lookup location_type,
  t901_code_lookup bill_state,
  t901_code_lookup product_family,
  t1900_company t1900
WHERE t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id
AND t205.c205_part_number_id       = t5053.c205_part_number_id
AND t5053.c5052_location_id        = t5052.c5052_location_id
AND t5052.c5052_ref_id             = t701.c701_distributor_id
and t703.c701_distributor_id         = t701.c701_distributor_id
AND t5052.c901_location_type       = location_type.c901_code_id(+)
AND t701.c701_bill_state           = bill_state.c901_code_id(+)
AND t205.c205_product_family       = product_family.c901_code_id(+)
AND t5051.c5051_inv_warehouse_id   = 3
AND t5052.c1900_company_id         =t1900.c1900_company_id
AND t5052.c5052_void_fl           IS NULL
AND t1900.c1900_void_fl           IS NULL
AND t701.C1900_COMPANY_ID          =1026
AND t5053.c5053_curr_qty          <>0
ORDER BY t1900.c1900_company_name,
  t701.c701_distributor_name