Select T504.C207_Set_Id Setid, get_set_name(T504.C207_Set_Id) as Setdesc , T5010.C5010_Tag_Id Tagid,  T504a.C504_Consignment_Id Cnid
                  , T525.C525_Product_Request_Id Reqid , Gm_Pkg_Op_Set_Pick_Rpt.Get_Loc_Cd_From_Id (T5053.C5052_Location_Id) Locationcd
                  ,   t504a.c504a_etch_id etchid, T504a.C504a_Planned_Ship_Date planship_date,'pending Pick' status, Gm_Pkg_Op_Set_Put_Rpt.Get_Loaner_Future_Status (T5010.C5010_Tag_Id,T504a.C504_Consignment_Id) Nextstatus
                   FROM t504a_consignment_loaner t504a, t504_consignment t504, t504a_loaner_transaction t504a1, t526_product_request_detail t526
                  , t525_product_request t525, t5010_tag t5010, t5053_location_part_mapping t5053
                  WHERE t504a.c504_consignment_id = t504.c504_consignment_id
        AND t504a.c504_consignment_id = t504a1.c504_consignment_id
                    AND t504a.c504_consignment_id = t5010.c5010_last_updated_trans_id(+)
                    AND t5010.c5010_tag_id                = t5053.c5010_tag_id (+)
                    AND t504a.c504a_status_fl             = 7 -- Pending Pick
                    AND t525.c525_product_request_id        = t526.c525_product_request_id 
                    And T526.C526_Product_Request_Detail_Id = T504a1.C526_Product_Request_Detail_Id
                      AND t504.c504_consignment_id = t504a.c504_consignment_id  
                    AND t525.c525_void_fl                  IS NULL
                    AND t504a.c504a_void_fl                IS NULL
                    And T504a1.C504a_Void_Fl               Is Null
                    AND ( T504a.C504a_Planned_Ship_Date = trunc(sysdate) OR T504a.C504a_Planned_Ship_Date = Trunc(Sysdate)+1)
                    AND T526.C526_Void_Fl                  IS NULL
                    And T504a1.C504a_Return_Dt Is Null      
                    and t526.C901_REQUEST_TYPE = 4127
                    And T5010.C5010_Void_Fl(+)             Is Null
                    AND T504A.C5040_PLANT_ID = 3000
                    AND T504.C1900_COMPANY_ID = 1000 
                    AND t525.C1900_COMPANY_ID = 1000
             --   Order By Reqid  Desc
                UNION ALL
            Select T504.C207_Set_Id Setid,   get_set_name(T504.C207_Set_Id) as Setdesc , T5010.C5010_Tag_Id Tagid, T504.C504_Consignment_Id Cnid,'' Reqid, '' Locationcd, T504a.C504a_Etch_Id Etchid
              ,     T504a.C504a_Planned_Ship_Date planship_date, 'pending Putaway' status, Gm_Pkg_Op_Set_Put_Rpt.Get_Loaner_Future_Status (T5010.C5010_Tag_Id,T504a.C504_Consignment_Id) Nextstatus
              FROM t504a_consignment_loaner t504a, t504_consignment t504, t5010_tag t5010
              WHERE t504a.c504_consignment_id = t5010.c5010_last_updated_trans_id (+) 
                AND t504a.c504_consignment_id         = t504.c504_consignment_id
                AND t504.c504_type                    = 4127
                AND t504a.c504a_status_fl             = 58 --pending FG
                AND t504a.c504a_void_fl              IS NULL
                AND t504.c504_void_fl                IS NULL
                And T5010.C5010_Void_Fl(+)              Is Null
                AND T504A.C5040_PLANT_ID = 3000
                AND T504.C1900_COMPANY_ID = 1000
                AND t5010.C1900_COMPANY_ID = 1000