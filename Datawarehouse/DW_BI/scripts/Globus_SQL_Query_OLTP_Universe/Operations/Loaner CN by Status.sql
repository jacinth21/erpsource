select c504_consignment_id as "consignment id"
,GET_SET_ID_FROM_CN(c504_consignment_id) as "Set id"
,get_set_name(GET_SET_ID_FROM_CN(c504_consignment_id))as "Set name"
,GET_TAG_ID(c504_consignment_id) as "Tag Id"
,gm_pkg_op_loaner.get_loaner_status(C504A_STATUS_FL) as "Loaner Flag"
,gm_pkg_op_loaner.get_loaner_request_id((gm_pkg_op_loaner.get_loaner_trans_id(c504_consignment_id))) as pdtid
from t504a_consignment_loaner 
where C504A_STATUS_FL !=60
and c504_consignment_id like 'GM-CN-'||@prompt('Enter GM-CN-%','A',,mono,free,persistent)
AND c1900_company_id = 1000