--PC-3806 -vendor payments details Report Japan
--By MatthewBalraj




 select get_code_name(t101pin.C901_INVOICE_CLOSING_DATE) AS  "INVOICE_CLOSING_DATE",
  t101.C101_PARTY_ID AS PARTY_ID, 
  t101.C101_PARTY_NM AS "PARTY_NM",
  t101.C101_PARTY_NM_EN AS "PARTY_NM_EN",
  t503.C503_INVOICE_ID AS  "INVOICE_ID" , 
  C503_INVOICE_DATE AS "INVOICE_DATE" ,
  get_code_name(t503.C901_INVOICE_TYPE) AS "INVOICE_TYPE", 
  NVL(C503B_UNPAID_INVOICE_AMT,0) AS "opening_balance",
  NVL(C503B_PAID_INVOICE_AMT,0)  AS "payment", 
 NVL(C503B_TOTAL_SALES_AMT,0) AS "sales", 
 NVL(C503B_CONSUMPTION_TAX_AMOUNT,0) AS "tax", 
 NVL(C503B_TOTAL_SALES_AMT,0)+ NVL(C503B_CONSUMPTION_TAX_AMOUNT,0) AS "Sales_and_tax", 
 NVL(C503B_ENDING_AR_BAL,0)  AS "ending_balance"
  FROM T503_INVOICE t503, T503B_INVOICE_ATTRIBUTE t503b,T101_PARTY_INVOICE_DETAILS t101pin, t101_party t101 where t101.c101_party_id in ( 
  SELECT t101.C101_PARTY_ID 
  FROM T101_PARTY_INVOICE_DETAILS  a , t101_party t101  
  WHERE   t101.c101_party_id = a.c101_party_id  
  AND a.C101_VOID_FL is null AND t101.C101_VOID_FL is null 
  ) 
  and C503_INVOICE_DATE(+) >=  to_date(@prompt('From Date:','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss')
  and C503_INVOICE_DATE(+) <=  to_date(@prompt('To  Date:','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss')
  and t503.C503_VOID_FL(+) is null 
  and t503b.C503B_VOID_FL is null 
  and t503.C503_INVOICE_ID = t503b.C503_INVOICE_ID(+) 
  and  t101pin.C101_PARTY_ID = t101.C101_PARTY_ID 
  and t101.c101_party_id = t503.C101_DEALER_ID(+) 
  order by get_code_name(t101pin.C901_INVOICE_CLOSING_DATE),upper(t101.C101_PARTY_NM_EN)