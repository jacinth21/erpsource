SELECT *
FROM
  (SELECT t408.c408_dhr_id AS "DHR ID",
    t408.c205_part_number_id "PART#",
    T408.C408_Control_Number "LOT#",
    Get_Mfg_Qty_To_Mfg(t408.C408_Dhr_Id) "QTY",
    t5052.c5052_location_cd "LOCATION",
    TRUNC(T408.C408_Location_Scan_Dt)             AS "DATE",
    TO_CHAR(t408.c408_location_scan_dt,'HH24:MI') AS "TIME"
  FROM t408_dhr t408,
    T5052_Location_Master T5052
  WHERE t408.c5052_location_id           = t5052.c5052_location_id
  AND t5052.c901_location_type           = '26241098'
  AND t5052.c901_status                  = '93310'
  And T408.C5052_Location_Id            Is Not Null
  AND TRUNC(T408.C408_Location_Scan_Dt) >= TO_DATE('09/15/2020','mm/dd/yyyy') --starting day of this project
  AND T408.C408_Void_Fl                 IS NULL
  AND T5052.C5052_Void_Fl               IS NULL
  UNION ALL
  SELECT T408b.C408_Dhr_Id AS "DHR ID",
    T408b.C205_Part_Number_Id "PART#",
    T408b.C408_Control_Number "LOT#",
    Get_Mfg_Qty_To_Mfg(T408b.C408_Dhr_Id) "QTY",
    Gm_Pkg_Op_Set_Pick_Rpt.Get_Loc_Cd_From_Id(C5052_Location_Id) "LOCATION",
    TRUNC(T408b.C408_Location_Scan_Dt)             AS "DATE",
    TO_CHAR(T408b.c408_location_scan_dt,'HH24:MI') AS "TIME"
  FROM T408b_Dhr_Loc_Log T408b
  WHERE T408b.C408b_Void_Fl              IS NULL
  AND TRUNC(T408b.C408_Location_Scan_Dt) >= TO_DATE('09/15/2020','mm/dd/yyyy') --starting day of this project
  AND T408b.C5052_Location_Id            IS NOT NULL
  )
ORDER BY "LOT#",
  "DATE",
  "TIME";
