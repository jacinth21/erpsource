PC-226 - Lot Expiry in 30 days Report
Created By - Arokia Prasath


SELECT globus_app.gm_pkg_op_loaner.get_loaner_status(T504A.C504A_STATUS_FL) AS "LOANSFL" ,
  t504a.c504a_etch_id AS "ETCHID",
  a.CONSIGNMENT_ID AS "CONSIGNMENT_ID",
  a.PNUM AS "PNUM",
  a.PDESC AS "PDESC",
  a.CNUM AS "CNUM",
  a.QTY AS "QTY",
  a.expdate AS "EXPDATE",
  a.setid AS "SETID",
  a.setname AS "SETNAME"
FROM
  (SELECT t5070.C901_TXN_ID AS "CONSIGNMENT_ID",
    t5072.C205_PART_NUMBER_ID AS "PNUM",
    get_partdesc_by_company(t5072.c205_part_number_id) AS "PDESC",
    t5072.c5072_control_number AS "CNUM",
    SUM(t5072.c5072_curr_qty) AS "QTY" ,
    GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(t5072.C205_PART_NUMBER_ID, t5072.c5072_control_number) AS "EXPDATE",
    get_set_id_from_cn(t5070.C901_TXN_ID) AS "SETID",
    get_set_name_from_cn(t5070.C901_TXN_ID) AS "SETNAME"
  FROM t5070_set_lot_master t5070,
    t5071_set_part_qty t5071,
    t5072_set_part_lot_qty t5072
  WHERE t5070.C5070_SET_LOT_MASTER_ID = t5071.C5070_SET_LOT_MASTER_ID
  AND t5071.C5071_SET_PART_QTY_ID     = t5072.C5071_SET_PART_QTY_ID
  AND t5070.C901_TXN_ID              IN
    (SELECT c504_consignment_id
    FROM GLOBUS_APP.t5073_set_lot_received_qty
    WHERE TRUNC(c5073_created_date)>= to_date('03/23/2020','mm/dd/yyyy')
    GROUP BY c504_consignment_id
    )
  AND t5070.c5070_void_fl IS NULL
  AND t5071.c5071_void_fl IS NULL
  AND t5072.c5072_void_fl IS NULL
  AND t5072.c5072_curr_qty >0
  GROUP BY t5070.C901_TXN_ID ,
    t5072.C205_PART_NUMBER_ID,
    t5072.c5072_control_number
  ) a,
  t504a_consignment_loaner t504a
WHERE a.CONSIGNMENT_ID   = t504a.c504_consignment_id
AND c504a_status_fl     !=60
AND t504a.c504a_void_fl IS NULL
AND expdate             >= sysdate
AND expdate             <= sysdate+30
ORDER BY CONSIGNMENT_ID ,
  PNUM,
  CNUM; 
