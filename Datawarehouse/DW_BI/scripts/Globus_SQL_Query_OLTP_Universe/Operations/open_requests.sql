SELECT OPEN_REQ.c525_product_request_id AS "Request Id"
     , OPEN_REQ.c526_product_request_detail_id As "Request Detail Id"
     , OPEN_REQ.REQUEST_DATE AS "Requested Date"
     , OPEN_REQ.c526_approve_reject_date AS "Approved Date"
     , OPEN_REQ.REQUEST_STATUS AS "Request Status"
     , OPEN_LOANERS.CN_STATUS AS "CN Status"
     , OPEN_REQ.REQUEST_TYPE AS "Request Type"
     , OPEN_REQ.c207_set_id AS "Set Id"
     , get_set_name(OPEN_REQ.c207_set_id) AS "Set Description"
     , OPEN_REQ.c526_required_date AS "Planned Shipped Date"
     , OPEN_LOANERS.c504_consignment_id AS "Consignment Id"
     , OPEN_LOANERS.c504a_etch_id AS "Etch Id"
     , OPEN_LOANERS.c5010_tag_id AS "Tag Id"
     , OPEN_LOANERS.Locationcd AS "Inventory Location"  
     , '' AS "Shipping Location"
     , '' AS "Shipping Tote"
     , NULL AS "Loaner Last Updated Date"
     , NULL AS "Time Lapsed"
FROM
(
SELECT t526.c525_product_request_id
     , t526.c526_product_request_detail_id
     , NVL(t526.c526_last_updated_date, t525.c525_created_date) REQUEST_DATE
     , t526.c526_approve_reject_date
     , DECODE(t526.c526_status_fl, 5, 'Pending Approval', 10, 'Open') REQUEST_STATUS
     , GET_CODE_NAME(t526.c901_request_type) REQUEST_TYPE
     , t526.c207_set_id 
     , t526.c526_required_date
from t525_product_request t525
   , t526_product_request_detail t526
WHERE t525.c525_product_request_id = t526.c525_product_request_id
  AND t526.c526_status_fl IN ( 5, 10 )
  AND t525.c525_void_fl IS NULL
  AND t526.c526_void_fl IS NULL
  AND t525.c1900_company_id = 1000
) OPEN_REQ
,(SELECT t504a.c504_consignment_id 
     , t504a.c504a_etch_id 
     , t5010.c5010_tag_id 
     , gm_pkg_op_loaner.get_loaner_status(t504a.c504a_status_fl) CN_STATUS
     , t504.c207_set_id
     ,  Gm_Pkg_Op_Set_Pick_Rpt.Get_Loc_Cd_From_Id (T5053.C5052_Location_Id) Locationcd
FROM t504a_consignment_loaner t504a
   , t5010_tag t5010
   , t504_consignment t504
   , t5053_location_part_mapping t5053
where t504a.c504a_status_fl IN (0, 25, 30, 40, 50, 55, 58 )
AND t504a.c504a_void_fl IS NULL
AND t504a.c504_consignment_id = t5010.c5010_last_updated_trans_id (+)
AND t5010.c5010_void_fl (+) is NULL
AND t504.c504_consignment_id = t504a.c504_consignment_id
AND t504.c504_void_fl IS NULL
AND T504.C1900_COMPANY_ID = 1000
AND T504A.C5040_PLANT_ID = 3000
AND t5010.c1900_company_id = 1000
AND t5010.c5010_tag_id = t5053.c5010_tag_id (+)
) OPEN_LOANERS
WHERE OPEN_REQ.c207_set_id = OPEN_LOANERS.c207_set_id (+)
UNION ALL
select t525.c525_product_request_id AS "Request Id"
     , t526.c526_product_request_detail_id As "Request Detail Id"
     ,  NVL(t526.c526_last_updated_date, t525.c525_created_date) AS "Requested Date" 
     , t526.c526_approve_reject_date AS "Approved Date"
     , gm_pkg_op_loaner.get_loaner_status(t504b.c504a_status_fl) AS "Request Status"
     , gm_pkg_op_loaner.get_loaner_status(t504b.c504a_status_fl) AS "CN Status"
     , GET_CODE_NAME(t526.c901_request_type) AS "Request Type" 
     , t526.c207_set_id AS "Set Id"
     , get_set_name(t526.c207_set_id) AS "Set Description"
     , t526.c526_required_date AS "Planned Shipped Date"
     , t504a.c504_consignment_id AS "Consignment Id"
     , t504b.c504a_etch_id AS "Etch Id"
     , t5010.c5010_tag_id AS "Tag Id"
     ,  Gm_Pkg_Op_Set_Pick_Rpt.Get_Loc_Cd_From_Id (T5053.C5052_Location_Id) AS "Inventory Location" 
     , t907.c907_pack_station_id AS "Shipping Location"
     , t907.c907_ship_tote_id AS "Shipping Tote"     
     , t504b.c504a_last_updated_date AS "Loaner Last Updated Date"
     , to_char(to_date('00:00:00','HH24:MI:SS') + (sysdate - t504b.c504a_last_updated_date), 'HH24:MI:SS') AS "Time Lapsed"
from 
  t525_product_request t525
, t526_product_request_detail t526
, t504a_loaner_transaction t504a
, t504a_consignment_loaner t504b
, t907_shipping_info t907
, t5010_tag t5010
, t5053_location_part_mapping t5053
where t525.c525_product_request_id = t526.c525_product_request_id
AND t526.c526_void_fl IS NULL
AND t525.c525_void_fl IS NULL
AND t526.c526_status_fl = 20 -- Allocated
and t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id
and t504a.c504a_void_fl IS NULL
and t504a.c504_consignment_id = t504b.c504_consignment_id
and t504b.c504a_void_fl IS NULL
and t504b.c504a_status_fl IN (5,7,10,13,16,58)
AND t525.c525_product_request_id = t907.c525_product_request_id
and ( t504b.c504_consignment_id  = t907.c907_ref_id or TO_CHAR(t526.c526_product_request_detail_id)  = t907.c907_ref_id ) 
and t907.c907_void_fl IS NULL
AND t504a.c504_consignment_id = t5010.c5010_last_updated_trans_id (+)
AND t5010.c5010_void_fl (+) is NULL
AND t5010.c5010_tag_id = t5053.c5010_tag_id (+)
AND t525.c1900_company_id = 1000
AND t526.c1900_company_id = 1000
AND T504A.C1900_COMPANY_ID = 1000
AND T504b.C5040_PLANT_ID = 3000
AND t5010.c1900_company_id = 1000
UNION ALL
select t525.c525_product_request_id AS "Request Id"
     , t526.c526_product_request_detail_id As "Request Detail Id"
     ,  NVL(t526.c526_last_updated_date, t525.c525_created_date) AS "Requested Date" 
     , t526.c526_approve_reject_date AS "Approved Date"
     , 'Completed' AS "Request Status"
     , 'Completed' AS "CN Status"
     , GET_CODE_NAME(t526.c901_request_type) AS "Request Type" 
     , t526.c207_set_id AS "Set Id"
     , get_set_name(t526.c207_set_id) AS "Set Description"
     , t526.c526_required_date AS "Planned Shipped Date"
     , t504a.c504_consignment_id AS "Consignment Id"
     , t504b.c504a_etch_id AS "Etch Id"
     , t5010.c5010_tag_id AS "Tag Id"
     ,  Gm_Pkg_Op_Set_Pick_Rpt.Get_Loc_Cd_From_Id (T5053.C5052_Location_Id) AS "Inventory Location" 
     , t907.c907_pack_station_id AS "Shipping Location"
     , t907.c907_ship_tote_id AS "Shipping Tote"     
     , t504b.c504a_last_updated_date AS "Loaner Last Updated Date"
     , to_char(to_date('00:00:00','HH24:MI:SS') + (sysdate - t504b.c504a_last_updated_date), 'HH24:MI:SS') AS "Time Lapsed"
from 
  t525_product_request t525
, t526_product_request_detail t526
, t504a_loaner_transaction t504a
, t504a_consignment_loaner t504b
, t907_shipping_info t907
, t5010_tag t5010
, t5053_location_part_mapping t5053
where t525.c525_product_request_id = t526.c525_product_request_id
AND t526.c526_void_fl IS NULL
AND t525.c525_void_fl IS NULL
AND t525.c1900_company_id = 1000
AND t526.c1900_company_id = 1000
AND T504A.C1900_COMPANY_ID = 1000
AND T504b.C5040_PLANT_ID = 3000
AND t5010.c1900_company_id = 1000
AND t526.c526_status_fl = 30 -- Allocated
and t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id
and t504a.c504a_void_fl IS NULL
and t504a.c504_consignment_id = t504b.c504_consignment_id
and t504b.c504a_void_fl IS NULL
and t504b.c504a_status_fl IN (20)
AND t525.c525_product_request_id = t907.c525_product_request_id
and ( t504b.c504_consignment_id  = t907.c907_ref_id or TO_CHAR(t526.c526_product_request_detail_id)  = t907.c907_ref_id ) 
and t907.c907_void_fl IS NULL
AND t504a.c504_consignment_id = t5010.c5010_last_updated_trans_id (+)
AND t5010.c5010_void_fl (+) is NULL
AND t5010.c5010_tag_id = t5053.c5010_tag_id (+)
AND TRUNC(T526.C526_REQUIRED_DATE) = TRUNC(SYSDATE)
