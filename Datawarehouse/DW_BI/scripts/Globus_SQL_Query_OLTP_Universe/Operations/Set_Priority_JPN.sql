/* Added prompt for Company id and plant Id 
* Ref: TSK-10896
*/
SELECT c504_consignment_id as "Consignment Id"
     , RETURNED.set_id as "Set Id"
     , GET_SET_NAME(RETURNED.set_id) as "Set Name"
     , C504a_etch_id as "Etch ID"
     , Status as "Status"
     , DET_ID as "Detail ID"
     , GET_CODE_NAME(set_type) as "Request Type"
     , Requested_Date as "Requested Date"
     , Approved_date as "Approved Date"
     , Priority as "Priority"
     , RET_DATE as "Set Returned Date"
 FROM
 (SELECT t504.c504_consignment_id
           , c207_set_id set_id
           , t504a.c504a_etch_id  
           , c504a_status_fl
           , gm_pkg_op_loaner.get_loaner_status(t504a.c504a_status_fl) STATUS
           , RET_DATE
        FROM t504a_consignment_loaner t504a
           , t504_consignment t504
           ,  ( select t504a.c504_consignment_id, MAX(c504a_return_dt) RET_DATE
                  from t504a_loaner_transaction t504a 
                 where t504a.c504a_void_fl IS NULL
                   AND T504A.C504A_RETURN_DT IS NOT NULL
                   AND t504a.C1900_COMPANY_ID = 1026
                   AND t504a.c526_product_request_detail_id IS NOT NULL
              GROUP BY  t504a.c504_consignment_id ) t504b
      WHERE t504.c504_consignment_id = t504a.c504_consignment_id
         AND t504.c504_consignment_id = t504b.c504_consignment_id
         AND t504a.c504a_void_fl IS NULL
         AND t504.c504_void_fl IS NULL
         AND t504.c207_set_id IS NOT NULL
         AND T504A.C5040_PLANT_ID = 3017
         AND t504.C1900_COMPANY_ID = 1026
         AND c504_type IN (4119, 4127)
         AND t504a.c504a_status_fl IN ( 25 , 30, 40, 50, 55 )) RETURNED
    , ( SELECT TO_CHAR(t526.c525_product_request_id) rid 
           , TO_CHAR(t526.c526_product_request_detail_id)  det_id   
           , t526.c207_set_id set_id
           , 1 QTY
           , t526.c901_request_type set_type
           , TO_CHAR(t525.c525_created_date, 'MM/DD/YYYY HH:MI:SS AM') Requested_Date
           , TO_CHAR(t526.c526_approve_reject_date, 'MM/DD/YYYY HH:MI:SS AM') Approved_date
           , 'A' Priority
        FROM t526_product_request_detail t526
           , t525_product_request t525
      WHERE t525.c525_product_request_id = t526.c525_product_request_id
         AND t526.c526_status_fl = 10 -- Approved and not allocated ( Open )
         AND C526_VOID_FL IS NULL 
         AND t525.C1900_COMPANY_ID = 1026
         AND TRUNC(c526_required_date) = TRUNC(SYSDATE)
         AND c901_request_type IN ( 4119, 4127 )
         AND C207_SET_ID IS NOT NULL 
   UNION ALL
     SELECT TO_CHAR(t526.c525_product_request_id) rid
           , TO_CHAR(t526.c526_product_request_detail_id)  det_id   
           , t526.c207_set_id set_id
           , 1 QTY
           , t526.c901_request_type set_type
           , TO_CHAR(t525.c525_created_date, 'MM/DD/YYYY HH:MI:SS AM') Requested_Date
           ,  TO_CHAR(t526.c526_approve_reject_date,'MM/DD/YYYY HH:MI:SS AM') Approved_date
           , 'B' Priority
        FROM t526_product_request_detail t526
           , t525_product_request t525
       WHERE t525.c525_product_request_id = t526.c525_product_request_id
         AND t526.c526_status_fl = 5 -- Pending Approval 
         AND c526_void_fl IS NULL 
         AND t525.C1900_COMPANY_ID = 1026
         AND TRUNC(c526_required_date) = TRUNC(SYSDATE)
         AND c901_request_type IN ( 4119, 4127 )
         AND c207_set_id IS NOT NULL
   UNION ALL     
   SELECT 'N/A' rid, 'N/A' det_id
           , t504.c207_set_id set_id
           , SUM( DECODE(t504a.c504a_status_fl,0,1,0)) QTY -- Sum up the sets that are in available status
           , TO_NUMBER(t504.c504_type) set_type
           , '' Requested_Date
           , '' Approved_date
           , 'C' Priority
        FROM t504a_consignment_loaner t504a
           , t504_consignment t504
      WHERE t504.c504_consignment_id = t504a.c504_consignment_id
         AND t504a.c504a_void_fl IS NULL
         AND t504.c504_void_fl IS NULL
         AND T504.C207_SET_ID IS NOT NULL
         AND T504A.C5040_PLANT_ID = 3017
         AND t504.C1900_COMPANY_ID = 1026
         AND c504_type IN (4119, 4127)
         AND t504a.c504a_status_fl != 60 -- ( In Active )
   GROUP BY c207_set_id, c504_type
      HAVING SUM( DECODE(t504a.c504a_status_fl,0,1,0)) = 0
      ) ACTUAL_NEED 
  WHERE RETURNED.SET_ID = ACTUAL_NEED.SET_ID