SELECT t526.c525_product_request_id as "Request Id"
    , GET_CODE_NAME(C901_REQUEST_FOR) as "Requested for"
     , get_distributor_name(C525_REQUEST_FOR_ID) as "Distributor Name"
     , t526.c207_set_id  as "Set ID"
     , get_set_name (t526.c207_set_id) as "Set Description" 
     , DECODE(t526.c526_status_fl,5,'Pending Approval',10, 'Open', 20, 'Allocated', 30, 'Closed') as  "Request Status"
     , t526.c526_product_request_detail_id as  "Request Detail ID"
     , t525.c525_created_date as "Request created date"
     , t526.c526_approve_reject_date  AS "Approved Date" 
     , t504a.c504_consignment_id AS "Consignment ID"
     , DECODE(c526_status_fl,30,'Pending Return.', gm_pkg_op_loaner.get_loaner_status (t504ac.c504a_status_fl)) as "Loaner_status"
from t525_product_request t525
   , t526_product_request_detail t526
   , t504a_loaner_transaction t504a
   , t504a_consignment_loaner t504ac
where t525.c525_product_request_id = t526.c525_product_request_id
AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
AND t504a.c504_consignment_id = t504ac.c504_consignment_id
AND TRUNC(t526.c526_required_date) = TRUNC(SYSDATE) 
AND t526.c526_void_fl is null
AND t525.c525_void_fl is null
AND t526.c207_set_id is not null
AND t504a.c504a_void_fl is null
AND T504AC.C504A_VOID_FL IS NULL
AND t525.c1900_company_id = 1000 
AND t504a.c1900_company_id = 1000
AND t504ac.C5040_PLANT_ID = 3000
AND t526.c526_status_fl IN (20,30) -- allocated
UNION ALL
SELECT t526.c525_product_request_id as "Request Id"
    , GET_CODE_NAME(C901_REQUEST_FOR) as "Requested for"
    , get_distributor_name(C525_REQUEST_FOR_ID) as "Distributor Name"
     , t526.c207_set_id  as "Set ID"
     , get_set_name (t526.c207_set_id) as "Set Description" 
     , DECODE(t526.c526_status_fl,5,'Pending Approval',10, 'Open', 20, 'Allocated', 30, 'Closed') as  "Request Status"
     , t526.c526_product_request_detail_id as  "Request Detail ID"
     , t525.c525_created_date as "Request created date"
     , t526.c526_approve_reject_date  AS "Approved Date" 
     , t504.c504_consignment_id AS "Consignment ID"
     , gm_pkg_op_loaner.get_loaner_status (t504ac.c504a_status_fl) as "Loaner_status"
from t525_product_request t525
   , t526_product_request_detail t526
   , t504a_consignment_loaner t504ac
   , t504_consignment t504
where t525.c525_product_request_id = t526.c525_product_request_id
AND t526.c207_set_id = t504.c207_set_id
AND t504ac.c504_consignment_id = t504.c504_consignment_id
AND TRUNC(t526.c526_required_date) = TRUNC(SYSDATE) 
AND t526.c526_void_fl is null
AND t525.c525_void_fl is null
AND t526.c207_set_id is not null
AND t504ac.c504a_void_fl is null
AND t504.c504_void_fl is null
AND t525.c1900_company_id = 1000 
AND t504.c1900_company_id = 1000
AND t504ac.C5040_PLANT_ID = 3000
AND t526.c526_status_fl IN (5,10) -- Pending approval and Open
AND t504ac.c504a_status_fl NOT IN ('5','7','10','20','22','23','60')