--PC-2398 -Traceability Report
--By Suganthi Sharmila

select  T5072.C205_PART_NUMBER_ID  AS "PART", 
    t205.c205_part_num_desc AS "PartDESC",
    T5072.C5072_CONTROL_NUMBER AS "LOT",  
    C5072_CURR_QTY AS "QTY", 
     T704.C101_DEALER_ID AS "Dealer_ID",
  t101.c101_party_nm AS "Dealer_nm", 
  T504.c704_account_ID AS "ACCT_ID", 
  t704.c704_account_nm AS "ACCT_NAME", 
    t504.c207_set_id AS  "SET_ID" ,
  GET_SET_NAME(t504.c207_set_id) AS "set_name", 
  t907.C907_SHIPPED_DT  AS "shipped_date",
  t5070.c901_txn_id  AS "Trans_id",  
  t2550.c2550_expiry_date AS "Expiry_date", 
  get_cs_fch_loaner_etchid(t5070.c901_txn_id) AS "Etch_id"
FROM T5072_SET_PART_LOT_QTY T5072,
  t5071_set_part_qty t5071,
  T5070_SET_LOT_MASTER T5070,
  T5052_LOCATION_MASTER T5052,
  t205_part_number t205,
  t2550_part_control_number t2550,
  t504_consignment t504,
  T704_ACCOUNT T704,
  T101_party T101 ,t907_shipping_info t907
where t5072.c205_part_number_id    = t205.c205_part_number_id
and t2550.c205_part_number_id    = t205.c205_part_number_id
and t2550.c2550_control_number   = T5072.C5072_CONTROL_NUMBER
and t5052.c5052_location_id      =t5070.c5052_location_id
and t5070.c5070_set_lot_master_id=t5072.c5070_set_lot_master_id
and t5070.C5070_SET_LOT_MASTER_ID = t5071.C5070_SET_LOT_MASTER_ID
AND t5071.C5071_SET_PART_QTY_ID     = t5072.C5071_SET_PART_QTY_ID
AND T704.C704_ACCOUNT_ID         = T504.c704_account_ID 
and t101.c101_party_id           = t704.c101_dealer_id(+)
and t5070.c901_txn_id            = t907.c907_ref_id
AND t5052.c1900_company_id      = 1026
and t5052.c5051_inv_warehouse_id = 6  --- Loaner
AND T704.C704_ACCOUNT_ID         = T907.C907_SHIP_TO_ID
and t504.c504_consignment_id = t5070.c901_txn_id
AND t5052.c5052_void_fl IS NULL
and t5072.c5072_void_fl is null
and t5070.c5070_void_fl  is null
and t704.c704_void_fl is null
and t907.c907_void_fl           is null
and t5072.c205_part_number_id = t205.c205_part_number_id
and T5072.C5072_CONTROL_NUMBER   = T5072.C5072_CONTROL_NUMBER
union all
SELECT T5072.C205_PART_NUMBER_ID AS "PART", 
    t205.c205_part_num_desc AS "PartDESC",
    T5072.C5072_CONTROL_NUMBER AS "LOT",  
  C5072_CURR_QTY AS "QTY",
  T704.C101_DEALER_ID AS "Dealer_ID",
  t101.c101_party_nm AS "Dealer_nm", 
  T5070.C901_TXN_ID AS "ACCT_ID", 
  t704.c704_account_nm AS "ACCT_NAME",
  t207.c207_set_id AS "SET_ID",
  t207.c207_set_nm AS "set_name",
  t907.C907_SHIPPED_DT AS "shipped_date",
  T5072.C5072_LAST_UPDATED_TXN_ID AS "Trans_id",
  t2550.c2550_expiry_date AS "Expiry_date",
  get_cs_fch_loaner_etchid(T5072.C5072_LAST_UPDATED_TXN_ID) AS "Etch_id"
FROM T5072_SET_PART_LOT_QTY T5072,
  T5070_SET_LOT_MASTER T5070,
  T5052_LOCATION_MASTER T5052,
  T704_ACCOUNT T704,
  T101_party T101,
  t208_set_details T208,
  t207_set_master t207,
  t205_part_number t205,t2550_part_control_number t2550 ,t907_shipping_info t907
where  t5072.c205_part_number_id = t205.c205_part_number_id
and T5072.C5072_CONTROL_NUMBER   = T5072.C5072_CONTROL_NUMBER
AND t207.c207_set_id             = T208.c207_set_id
AND t207.c901_set_grp_type       = '1600' -- 1600 System type
AND t5072.c205_part_number_id    = T208.c205_part_number_id
AND t5072.c205_part_number_id    = t205.c205_part_number_id
and t2550.c205_part_number_id    = t205.c205_part_number_id
and t2550.c2550_control_number   = T5072.C5072_CONTROL_NUMBER
and t5052.c5052_location_id      =t5070.c5052_location_id
and t5070.c5070_set_lot_master_id=t5072.c5070_set_lot_master_id
AND T704.C704_ACCOUNT_ID         =T5070.C901_TXN_ID (+)
AND T704.C704_ACCOUNT_ID         =T5052.C5052_REF_ID
and t101.c101_party_id           = t704.c101_dealer_id(+)
and t5070.c901_txn_id            = t907.c907_ref_id
AND t5052.c1900_company_id      = 1026
AND t207.c207_void_fl           IS NULL
and t208.c208_void_fl           is null
and t5052.c5052_void_fl         is null
and t5072.c5072_void_fl         is null
and t5070.c5070_void_fl         is null
and t907.c907_void_fl           is null
and t101.C101_VOID_FL       is null
and t5052.c5051_inv_warehouse_id = 5  --- AIC
and t704.c901_account_type       = 26230710 -- p_location_type  1119.0034  1119.0010
ORDER BY ACCT_NAME,
  PART,
  lot,
  QTY -- (AIC final)


