--PC-2912
--PC Owner - Karthik Somanathan

SELECT *
FROM
  (SELECT t408.c408_dhr_id "DHR #",
    t408.c205_part_number_id "PART #",
    t408.c408_control_number "LOT #",
    t5052.c5052_location_cd "CURRENT LOCATION",
    T408.C5052_Location_Id "LOCATION ID",
    t408.c408_location_scan_dt "UPDATED DATE",
   t408.c408_location_scan_dt "UPDATED DATE WITH TIME"
  FROM t408_dhr t408,
    t5052_Location_Master T5052
  WHERE t408.c5052_location_id    = t5052.c5052_location_id
  AND t5052.c901_location_type    = '26241098'
  AND t5052.c901_status           = '93310'
  AND T408.C5052_Location_Id     IS NOT NULL
  AND T408.C408_Void_Fl          IS NULL
  AND T5052.C5052_Void_Fl        IS NULL
  UNION ALL
  SELECT T408b.C408_Dhr_Id "DHR #",
    t408b.C205_Part_Number_Id "PART #",
    t408b.C408_Control_Number "LOT #",
    Gm_Pkg_Op_Set_Pick_Rpt.Get_Loc_Cd_From_Id(C5052_Location_Id) "CURRENT LOCATION",
    t408b.C5052_Location_Id "Location_id",
    t408b.c408_location_scan_dt "UPDATED DATE",
    t408b.c408_location_scan_dt "UPDATED DATE WITH TIME"
  FROM t408b_Dhr_Loc_Log T408b
  WHERE t408b.C408b_Void_Fl       IS NULL
  AND t408b.C5052_Location_Id     IS NOT NULL
  )
ORDER BY "DHR #" ,
  "UPDATED DATE" DESC

