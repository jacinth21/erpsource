PC-3540 -Open Loaner Request_14 Days
By Yogabalakrishnan


SELECT c504_consignment_id AS "consignment id" , actual_need.set_id AS "set id" ,
  get_set_name(actual_need.set_id) AS "set name" ,
  c504a_etch_id AS "etch id" ,
  status AS "status" ,
  det_id AS "detail id" ,
  get_code_name(set_type) AS "request type" ,
  requested_date AS "requested date" ,
  approved_date AS "approved date" ,
  priority AS "priority" ,
  ret_date AS "set returned date" ,
  t701.c701_distributor_name_en AS "distributor name" ,
  t703.c703_sales_rep_name_en AS "rep name" ,
  surgery_date AS "surgery date" ,
  parent_request_id AS "parent request id" ,
  planned_ship_date AS "planned ship date" ,
  t901.c901_code_nm AS "region"
FROM
  (SELECT t504.c504_consignment_id ,
    c207_set_id set_id ,
    t504a.c504a_etch_id ,
    c504a_status_fl ,
    gm_pkg_op_loaner.get_loaner_status(t504a.c504a_status_fl) status ,
    ret_date
  FROM t504a_consignment_loaner t504a ,
    t504_consignment t504 ,
    (SELECT t504a.c504_consignment_id,
      MAX(c504a_return_dt) ret_date
    FROM t504a_loaner_transaction t504a
    WHERE t504a.c504a_void_fl IS NULL
    AND t504a.c504a_return_dt IS NOT NULL
    AND t504a.c1900_company_id = 1000
    AND t504a.c526_product_request_detail_id IS NOT NULL
    GROUP BY t504a.c504_consignment_id
    ) t504b
  WHERE t504.c504_consignment_id = t504a.c504_consignment_id
  AND t504.c504_consignment_id = t504b.c504_consignment_id
  AND t504a.c504a_void_fl IS NULL
  AND t504.c504_void_fl IS NULL
  AND t504.c207_set_id IS NOT NULL
  AND t504a.c5040_plant_id = 3000
  AND t504.c1900_company_id = 1000
  AND c504_type IN (4119, 4127)
  AND t504a.c504a_status_fl IN ( 25 , 30, 40, 50, 55 )
  ) returned ,
  (SELECT TO_CHAR(t526.c525_product_request_id) rid ,
    TO_CHAR(t526.c526_product_request_detail_id) det_id ,
    t526.c207_set_id set_id ,
    1 qty ,
    t526.c901_request_type set_type ,
    TO_CHAR(t525.c525_created_date, 'mm/dd/yyyy hh:mi:ss am') requested_date ,
    TO_CHAR(t526.c526_approve_reject_date, 'mm/dd/yyyy hh:mi:ss am') approved_date ,
    'A' priority ,
    t525.c525_request_for_id did ,
    c703_sales_rep_id rep_id ,
    c525_surgery_date surgery_date ,
    t526.c525_product_request_id parent_request_id ,
    t526.c526_required_date planned_ship_date
  FROM t526_product_request_detail t526 ,
    t525_product_request t525
  WHERE t525.c525_product_request_id = t526.c525_product_request_id
  AND t526.c526_status_fl = 10 -- approved and not allocated ( open )
  AND c526_void_fl IS NULL
  AND t525.c1900_company_id = 1000
  AND TRUNC(c526_required_date) >=trunc(SYSDATE)
AND TRUNC(c526_required_date) <=trunc(SYSDATE)+14
  AND c901_request_type IN ( 4119, 4127 )
  AND c207_set_id IS NOT NULL
  UNION ALL
  SELECT TO_CHAR(t526.c525_product_request_id) rid ,
    TO_CHAR(t526.c526_product_request_detail_id) det_id ,
    t526.c207_set_id set_id ,
    1 qty ,
    t526.c901_request_type set_type ,
    TO_CHAR(t525.c525_created_date, 'mm/dd/yyyy hh:mi:ss am') requested_date ,
    TO_CHAR(t526.c526_approve_reject_date,'mm/dd/yyyy hh:mi:ss am') approved_date ,
    'B' priority ,
    t525.c525_request_for_id did ,
    c703_sales_rep_id rep_id ,
    c525_surgery_date surgery_date ,
    t526.c525_product_request_id parent_request_id ,
    t526.c526_required_date planned_ship_date
  FROM t526_product_request_detail t526 ,
    t525_product_request t525
  WHERE t525.c525_product_request_id = t526.c525_product_request_id
  AND t526.c526_status_fl = 5 -- pending approval
  AND c526_void_fl IS NULL
  AND t525.c1900_company_id = 1000
    AND TRUNC(c526_required_date) >=trunc(SYSDATE)
AND TRUNC(c526_required_date) <=trunc(SYSDATE)+14
  AND c901_request_type IN ( 4119, 4127 )
  AND c207_set_id IS NOT NULL
  ) actual_need ,
  t701_distributor t701 ,
  t703_sales_rep t703 ,
  t901_code_lookup t901
WHERE returned.set_id (+) = actual_need.set_id
AND t701.c701_distributor_id (+)=did
AND t703.c703_sales_rep_id (+) = rep_id
AND c701_region =t901.c901_code_id(+)