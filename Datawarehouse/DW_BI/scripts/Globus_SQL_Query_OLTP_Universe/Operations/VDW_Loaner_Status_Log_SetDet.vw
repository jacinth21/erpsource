--@"C:\Database\DW_BI\scripts\Globus_SQL_Query_OLTP_Universe\Operations\VDW_Loaner_Status_Log_SetDet.vw";

CREATE OR REPLACE VIEW globus_bi_realtime.VDW_Loaner_Status_Log_SetDet AS
		SELECT c905_ref_id, c905_loaner_turn_no
			,  MAX(DECODE(c905_status_fl,100,c905_updated_by,''))accepted_by
			, MAX( DECODE(c905_status_fl,100,TRUNC(c905_updated_date),''))accepted_date
			, MAX( DECODE(c905_status_fl,100,TO_CHAR(c905_updated_date,'HH:MI AM'),''))accepted_time
			,  MAX(DECODE(c905_status_fl,200,c905_updated_by,'')) CHECKED_by
			,  MAX(DECODE(c905_status_fl,200,TRUNC(c905_updated_date),'')) CHECKED_date
			,  MAX(DECODE(c905_status_fl,200,TO_CHAR(c905_updated_date,'HH:MI AM'),'')) CHECKED_time
			,  MAX(DECODE(c905_status_fl,250,c905_updated_by,'')) Processed_by
			,  MAX(DECODE(c905_status_fl,250,TRUNC(c905_updated_date),''))  Processed_date
			, MAX( DECODE(c905_status_fl,250,TO_CHAR(c905_updated_date,'HH:MI AM'),'')) Processed_time
			,  MAX(DECODE(c905_status_fl,300,c905_updated_by,'')) Picture_taken_by
			,  MAX(DECODE(c905_status_fl,300,TRUNC(c905_updated_date),'')) Picture_taken_date
			,  MAX(DECODE(c905_status_fl,300,TO_CHAR(c905_updated_date,'HH:MI AM'),'')) Picture_taken_time
			, MAX(c905_status_fl) current_status, MAX(TRUNC(c905_updated_date)) updated_date
		FROM T905_STATUS_DETAILS t905 
		,t208_set_details t208, t504_consignment t504  
		WHERE c901_source = 91107 
		AND t208.c208_void_fl IS NULL
		AND t208.c208_inset_fl = 'Y'
		AND t504.c207_set_id = t208.c207_set_id 
		AND t504.c504_void_fl IS NULL 
		AND t504.c504_consignment_id =t905.c905_ref_id 
		AND c905_status_fl IN ('100','200','250','300') 
		AND t504.C1900_COMPANY_ID = 1000
		AND t905.C1900_COMPANY_ID = 1000
		GROUP BY c905_ref_id ,c905_loaner_turn_no;
/