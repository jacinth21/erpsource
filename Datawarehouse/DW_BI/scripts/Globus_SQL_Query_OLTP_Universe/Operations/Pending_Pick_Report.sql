Select * From (  
       SELECT get_code_name (t504.c504_type) TYPE, t504.c504_type type_id, t504.c504_consignment_id ,  t5010.c5010_tag_id , t5052.c5052_location_cd,           
                  DECODE('LN','IN',gm_pkg_sm_eventbook_txn.get_event_name(t525.c525_product_request_id), t525.c525_product_request_id) 
                  req_eventid, t525.c525_product_request_id reqid,  '' field_sales, '' fid 
                  , MIN( DECODE(t5050.c901_status, 93003, 'A', 93004, 'I',93005,'W', 'O')) status 
                  , MIN(t5050.c101_allocated_user_id) user_id 
                  , GET_USER_SH_NAME(MIN(t5050.c101_allocated_user_id)) Short_nm 
                  , MIN(TO_CHAR(t526.c526_required_date,'MM/DD/YYYY')) planshipdt 
        From T504a_Consignment_Loaner T504a, T504a_Loaner_Transaction T504a1, T5050_Invpick_Assign_Detail T5050, 
            T525_Product_Request T525, T526_Product_Request_Detail T526, T504_Consignment T504 , T5010_Tag T5010, T5053_Location_Part_Mapping T5053, 
            t5052_location_master t5052 
        WHERE t525.c525_product_request_id = t526.c525_product_request_id  
        And T504a.C504_Consignment_Id =   T504a1.C504_Consignment_Id  
        and T504a.C504_Consignment_Id  = t5010.C5010_LAST_UPDATED_TRANS_ID 
        And T504.C504_Consignment_Id = T504a.C504_Consignment_Id   
        And T5053.C5010_Tag_Id = T5010.C5010_Tag_Id 
        and T5053.c5052_location_id = T5052.c5052_location_id 
        AND t504a1.c526_product_request_detail_id = t526.c526_product_request_detail_id  
        And T504a.C504a_Status_Fl=  7 
		AND t525.c1900_company_id = 1000 
        AND t5010.c1900_company_id = 1000 
        AND t504.c504_type  = DECODE ('LN', 'LN', 4127, 'CNLN', 4127, 'IN', 4119, - 9999) 
        AND t504.c504_consignment_id = t5050.c5050_ref_id(+) 
        AND t5050.c5050_void_fl IS NULL 
        AND t504a1.c504a_return_dt IS NULL        
          AND t504a1.c504a_void_fl IS NULL  
        AND t504a.c504a_void_fl IS NULL  
        AND t525.c525_void_fl IS NULL  
        And Nvl(T5050.C901_Status ,-9999) In (93003,93004,93005,-9999) 
    GROUP BY t525.c525_product_request_id, t504.c504_type,t504.c504_consignment_id ,t5010.c5010_tag_id,t5052.c5052_location_cd 
    Order By T525.C525_Product_Request_Id Desc)