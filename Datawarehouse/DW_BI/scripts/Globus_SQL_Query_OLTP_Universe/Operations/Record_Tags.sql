PC-2294 - Record Tags
Created By: Karthik Somanathan


SELECT T501.C501_Order_Id                                      AS "Order Id" ,
  get_account_name(t501.c704_account_id)                       AS "Account Name",
  T501d.C5010_Tag_Id                                           AS "Tag Id",
  T501d.C207_Set_Id                                            AS  "Set Id",
  DECODE(T501d.C5010_Tag_Id,NULL,'Y',NULL )                    AS  "Tag Missing in DO",
  get_code_name(T501d.c901_tag_status)                         AS "DO Tag Status",
  Get_Set_Name(T501d.C207_Set_Id)                              AS "Set Name",
  DECODE(T501.C501_Receive_Mode , '26230683','IPAD','SpineIT') AS  "Order Receive Mode",
  Get_Set_Name(Get_System_From_Setid( T501d.C207_Set_Id))      AS "System Name",
  get_rep_name(t501.c703_sales_rep_id)                         AS "Rep Name" ,
  get_dist_rep_name(t501.c703_sales_rep_id)                    AS  "Distributor Name" ,
  get_distributor_name(t501d.c501d_tag_owner_id)               AS "Tag Owner",
  TO_CHAR(T501.c501_order_date,'mm/dd/yyyy HH:MM:SS AM')       AS "Order Date"
FROM T501d_Order_Tag_Usage_Details T501d ,
  T501_Order T501
WHERE T501.C501_Order_Id                = T501d.C501_Order_Id(+)
AND t501.c501_parent_order_id          IS NULL
AND T501.C1900_Company_Id               = 1000
AND NVL(t501.c901_order_type,-999) NOT IN ( 2529,2522,2535,2520,101260,26240233,26240232 )
AND T501d.C501d_Void_Fl(+)               IS NULL
AND T501.C501_Void_Fl                  IS NULL
AND t501.c501_order_date               >= to_date(@prompt('Enter From Date:','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss')
AND t501.c501_order_date               <= to_date(@prompt('Enter To Date:','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss')
ORDER BY T501.C703_Sales_Rep_Id ,
T501.C704_Account_Id ,
Get_Set_Name(T501d.C207_Set_Id)