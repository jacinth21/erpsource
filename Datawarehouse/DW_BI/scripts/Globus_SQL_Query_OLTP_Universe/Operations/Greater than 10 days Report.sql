PC-190 - Aging  Report(Greater than 10 days report)
Created By: Karthik Somanathan



SELECT t408.c408_dhr_id "DHR #",
  t408.c205_part_number_id "PART #",
  t408.c408_control_number "LOT #",
  t5052.c5052_location_cd "CURRENT LOCATION",
  t408.c5052_location_id  "Location_id",
  t408.c408_location_scan_dt "UPDATED DATE"
FROM t408_dhr t408,
  t5052_location_master t5052
WHERE t408.c5052_location_id     = t5052.c5052_location_id
AND t5052.c901_location_type     = '26241098'
AND t5052.c901_status            = '93310'
AND TRUNC(t408.c408_location_scan_dt) <= TRUNC (SYSDATE) - 10
AND t408.c5052_location_id      IS NOT NULL
AND t408.c408_void_fl           IS NULL
AND t5052.c5052_void_fl         IS NULL
