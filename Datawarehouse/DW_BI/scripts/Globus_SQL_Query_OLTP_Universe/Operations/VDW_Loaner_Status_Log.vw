--@"C:\Database\DW_BI\scripts\Globus_SQL_Query_OLTP_Universe\Operations\VDW_Loaner_Status_Log.vw";

CREATE OR REPLACE VIEW globus_bi_realtime.VDW_Loaner_Status_Log AS
	SELECT t504.c207_set_id setid, substr(get_set_name(t504.c207_set_id),0,32) setname,t504a.C504A_ETCH_ID etchid,t504.c504_consignment_id
		FROM t504_consignment t504, t504a_consignment_loaner t504a 
		WHERE t504.c504_consignment_id = t504a.C504_CONSIGNMENT_ID 
		AND T504.C1900_COMPANY_ID = 1000
		AND t504a.C5040_PLANT_ID = 3000
		AND t504.c504_void_fl IS NULL;
/