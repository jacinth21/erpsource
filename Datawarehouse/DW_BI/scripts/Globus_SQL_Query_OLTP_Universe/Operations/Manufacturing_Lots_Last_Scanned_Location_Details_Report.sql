SELECT T408.C408_Dhr_Id AS "DHR ID",
  T408.C205_Part_Number_Id "PART#",
  T408.C408_Control_Number "LOT#",
  T408.C408_Qty_Received "QTY",
  T5052.C5052_Location_Cd                       AS "LAST SCANNED LOCATION",
  TRUNC(T408.C408_Location_Scan_Dt)             AS "DATE",
  TO_CHAR(t408.c408_location_scan_dt,'HH24:MI') AS "TIME"
FROM t408_dhr t408,
  T5052_Location_Master T5052
WHERE t408.c5052_location_id          = t5052.c5052_location_id
AND t5052.c901_location_type          = '26241098'
AND t5052.c901_status                 = '93310'
AND T408.C5052_Location_Id           IS NOT NULL
AND TRUNC(T408.C408_Location_Scan_Dt) = TRUNC(CURRENT_DATE)-1
AND T408.C408_Void_Fl                IS NULL
AND T5052.C5052_Void_Fl              IS NULL
ORDER BY TO_CHAR(t408.c408_location_scan_dt,'HH24:MI') ASC;
