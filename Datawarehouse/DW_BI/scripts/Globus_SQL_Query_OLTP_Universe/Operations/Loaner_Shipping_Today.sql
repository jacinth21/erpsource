SELECT  REQ_CN.c525_product_request_id AS "Request Id"
     , REQ_CN.c526_product_request_detail_id AS "Request Detail ID"
     , REQ_CN.c207_set_id as "Set ID" 
     , get_set_name (REQ_CN.c207_set_id) as "Set Description" 
     , REQ_CN.c504_consignment_id AS "Consignment ID" 
     , REQ_CN.c5010_tag_id As "Tag ID"    
     , REQ_CN.Req_STATUS AS " Request Status"
     , REQ_CN.Loaner_status AS "Loaner Status"
     , REQ_CN.c525_created_date AS "Requested Date"
     , REQ_CN.c526_approve_reject_date AS "Approved Date"    
     , SETS_LOC.loc_cd AS "Inventory Location"
     FROM
(select REQ.c525_product_request_id 
     , REQ.c526_product_request_detail_id
     , REQ.c207_set_id 
     , SETS_AVL.c504_consignment_id 
     , SETS_AVL.c5010_tag_id 
     , REQ.Req_STATUS 
     , SETS_AVL.Loaner_status 
     , REQ.c525_created_date 
     , REQ.c526_approve_reject_date 
FROM        
(
SELECT t526.c525_product_request_id
     , t526.c207_set_id
     , DECODE(t526.c526_status_fl,5,'Pending Approval',10, 'Open', 20, 'Allocated', 30, 'Closed') Req_STATUS
     , t526.c526_product_request_detail_id 
     , t525.c525_created_date
     , t526.c526_approve_reject_date
from t525_product_request t525
   , t526_product_request_detail t526
where t525.c525_product_request_id = t526.c525_product_request_id
AND TRUNC(T526.C526_REQUIRED_DATE) = TRUNC(SYSDATE) 
AND t525.c1900_company_id = 1000 
AND t526.c526_void_fl is null
AND t525.c525_void_fl is null
) REQ
,(SELECT t504a.c504_consignment_id
     , t504.c207_set_id
     , gm_pkg_op_loaner.get_loaner_status (t504a.c504a_status_fl) Loaner_status
     , t5010.c5010_tag_id
from t504a_consignment_loaner t504a
   , t504_consignment t504
   , t5010_tag t5010
where t504a.c504_consignment_id = t504.c504_consignment_id
AND t504a.c504_consignment_id = t5010.c5010_last_updated_trans_id (+)
AND t504a.c504a_status_fl != 60
AND t504.c1900_company_id = 1000
AND t504a.C5040_PLANT_ID = 3000
AND t5010.c1900_company_id = 1000
and t504a.c504a_void_fl is NULL
AND t504.c504_void_fl is null) SETS_AVL
WHERE REQ.c207_set_id = SETS_AVL.c207_set_id (+)) REQ_CN
,(select t5010.c5010_last_updated_trans_id cn_iD
     , t5052.c5052_location_cd loc_cd
  from t5053_location_part_mapping t5053
     , t5010_tag t5010
     , t5052_location_master t5052
where t5010.c5010_tag_id = t5053.c5010_tag_id
  and t5052.c5052_location_id = t5053.c5052_location_id
  AND t5010.c1900_company_id = 1000
  AND t5052.c1900_company_id = 1000  
  and t5010.c5010_void_fl IS NULL
  AND t5052.c5052_void_fl IS NULL) SETS_LOC
 WHERE  REQ_CN.c504_consignment_id = SETS_LOC.cn_iD (+)
