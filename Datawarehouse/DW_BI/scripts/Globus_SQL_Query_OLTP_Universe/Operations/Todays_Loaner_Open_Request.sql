SELECT T526.C525_Product_Request_Id Pdtreqid, NVL (T526.C207_Set_Id, T526.C205_Part_Number_Id) as Setid,
    Get_Distributor_Name (T525.C525_Request_For_Id) as Dname, Get_Code_Name (T525.C901_Ship_To) as Shiptonm, Get_User_Name (
    T525.C525_Created_By) as Create_User, TO_CHAR ( T525.C525_Created_Date, 'mm/dd/yyyy HH:MI AM') Create_Date,
    T526.C526_Qty_Requested Qty, TO_CHAR (T526.C526_Required_Date, 'mm/dd/yyyy') Reqired_date, TO_CHAR (
    T525.C525_Surgery_Date, 'mm/dd/yyyy') Surgery_date, T526.C526_Product_Request_Detail_Id Reqdetailid,
    Get_Loaner_Request_Status ( T526.C526_Product_Request_Detail_Id) Stsfl
   FROM T526_Product_Request_Detail T526, T525_Product_Request T525, T907_Shipping_Info T907
  , T504a_Consignment_Loaner T504acl, (
         SELECT T504a.C504_Consignment_Id, C526_Product_Request_Detail_Id
           FROM T504a_Consignment_Loaner T504a, (
                 SELECT T504alt.C504_Consignment_Id, C526_Product_Request_Detail_Id
                   FROM T504a_Loaner_Transaction T504alt
                  WHERE T504alt.C526_Product_Request_Detail_Id IS NOT NULL
				    AND T504alt.C1900_COMPANY_ID = 1000
                    AND T504alt.C504a_Void_Fl                  IS NULL
               GROUP BY T504alt.C504_Consignment_Id, C526_Product_Request_Detail_Id
            )
            T504alt
          WHERE T504a.C504_Consignment_Id = T504alt.C504_Consignment_Id
            AND T504a.C504a_Status_Fl    != 60
            AND T504a.C504a_Void_Fl      IS NULL
    )
    T504alt
  WHERE C901_Request_Type                   = 4127
    AND T907.C901_Source                    = 50185
    AND T526.C525_Product_Request_Id        = T525.C525_Product_Request_Id
    AND T525.C525_Product_Request_Id        = T907.C907_Ref_Id(+)
    AND T525.C525_Void_Fl                  IS NULL
    AND T526.C526_Void_Fl                  IS NULL
    AND T526.C526_Product_Request_Detail_Id = T504alt.C526_Product_Request_Detail_Id(+)
    AND T504alt.C504_Consignment_Id         = T504acl.C504_Consignment_Id(+)
   AND T907.C907_Void_Fl(+)               IS NULL
    And T526.C526_Status_Fl                In (10)
    AND T526.C526_REQUIRED_DATE             = TRUNC (SYSDATE)
    AND T525.C1900_COMPANY_ID = 1000 
    AND T907.C1900_COMPANY_ID = 1000
    AND T504acl.C5040_PLANT_ID = 3000
    Order By T526.C525_Product_Request_Id