select order_id,rep_name,Account_name,Receive_mode,order_date,NPI_NUMBER,SURGEON_NAME,Valid from (
SELECT NPI_Detail.*, DECODE(NPI_MASTER_NUMBER,NULL,'No', 'Yes') Valid from (  
SELECT T501.*, NPI.NPI_NUMBER, NPI.SURGEON_NAME 
FROM  
( SELECT t501.c501_order_id order_id 
     , get_rep_name(t501.c703_sales_rep_id) rep_name 
     , get_account_name(t501.c704_account_id) Account_name 
     , get_code_name(t501.C501_RECEIVE_MODE) Receive_mode 
     , t501.c501_order_date order_date
     , t501.c1900_company_id
  FROM t501_order t501 
 WHERE t501.c501_void_fl      IS NULL    
  AND to_char(t501.c703_sales_rep_id) IN ( 
    SELECT to_char(nvl(c703_sales_rep_id,'-999'))  
    FROM t6645_npi_account
    WHERE c6645_void_fl IS NULL group by c703_sales_rep_id)
    
   AND TRUNC(t501.c501_order_date) = trunc( to_date(@prompt('Date','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss'))    
   AND t501.c501_parent_order_id is null ) T501 
  FULL OUTER JOIN 
  ( select t6640.c6640_ref_id REF_ID 
       , t6640.c6600_surgeon_npi_id NPI_NUMBER 
       , t6640.c6640_surgeon_name SURGEON_NAME 
   from t6640_npi_transaction t6640 where c6640_void_fl is null and c6640_transaction_fl = 'Y'  AND TRUNC(t6640.c6640_created_date) >= trunc( to_date(@prompt('Date','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss'))    
   ) NPI 
  ON t501.order_id = NPI.REF_ID  )NPI_Detail 
   , 
   ( select c6600_surgeon_npi_id NPI_MASTER_NUMBER    
     from t6600_party_surgeon ) NPI_MAster 
where NVL(NPI_Detail.NPI_NUMBER, -999) = NPI_MAster.NPI_MASTER_NUMBER (+)
and NPI_Detail.c1900_company_id = 1000
) sub1 order by 6 asc;