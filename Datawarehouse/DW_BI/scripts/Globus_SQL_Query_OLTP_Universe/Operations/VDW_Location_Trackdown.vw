--@"C:\Database\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Operations\VDW_Location_Trackdown.vw";

CREATE OR REPLACE VIEW globus_bi_realtime.VDW_Location_Trackdown AS
	SELECT t5054.c5054_txn_id TXN_ID,
	  t5054.c205_part_number_id pnum,
	  NVL(t5054.c5054_orig_qty,0) orgqty,
	  NVL(t5054.c5054_txn_qty,0) txnqty,
	  NVL(t5054.c5054_new_qty,0) newqty,
	  t5052.c5052_location_cd lcnid,
	  t5054.c5054_created_date created_date
	FROM t5054_inv_location_log t5054, t5052_location_master t5052
	WHERE t5052.c5052_location_id = t5054.c5052_location_id
	AND t5054.C1900_COMPANY_ID = 1000
	AND t5052.C1900_COMPANY_ID = 1000;
/