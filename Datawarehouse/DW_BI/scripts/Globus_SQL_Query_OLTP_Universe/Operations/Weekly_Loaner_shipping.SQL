SELECT  c907_shipping_id as Shipping_id,
        c907_ref_id as refid,
        gm_pkg_op_loaner.get_loaner_etch_id(c907_ref_id) as etchid,
        get_code_name (c901_source) as SOURCE,
        c901_source as  sourceid,
        get_cs_ship_name (c901_ship_to, c907_ship_to_id) as shiptonm,
        get_code_name (c901_delivery_mode) as ship_mode,
        c901_delivery_mode as ship_mode_code,
        get_code_name (c901_delivery_carrier) as Delivery_Carrier,
        DECODE (c907_tracking_number, NULL, 'Y', c907_tracking_number ) as track,
        c907_shipped_dt as shipped_date,
        DECODE (c907_status_fl, 15,gm_pkg_cm_shipping_info.get_pendcontrol_status(c907_ref_id,c901_source), c907_status_fl) as status,
        get_set_id_from_cn(c907_ref_id) as setid,
        get_set_name_from_cn(c907_ref_id) as setname
    FROM t907_shipping_info t907
    WHERE c907_void_fl  IS NULL
    AND C907_ACTIVE_FL  IS NULL
	AND t907.C1900_COMPANY_ID = 1000 
    AND c901_source      = 50182 --Loaner
    AND c907_status_fl=40 -- completed
    AND TRUNC(C907_SHIPPED_DT) >= TO_CHAR(sysdate-8,'DD/MON/YYYY')
    AND TRUNC(C907_SHIPPED_DT) <= TO_CHAR(sysdate,'DD/MON/YYYY')