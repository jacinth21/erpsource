 SELECT T501.C501_Order_Id                        AS Order_Id,
  T501.C704_Account_Id                           AS Account_Id,
  Get_Account_Name(T501.C704_Account_Id)         AS Account_Name,
  T501.C501_Order_Date                           AS Current_Order_Date,
  T941.C941_Value                                AS Previous_Order_Date,
  SUM( (C502_Item_Qty *C502_Corp_Item_Price))    AS Order_Amount,
  T501.C1900_Company_Id                          AS Company_Id,
  Get_Company_Name (T501.C1900_Company_Id)       AS Company_Name,
  T501.C501_Order_Date_Updated_Date              AS Order_Date_Updated_Date,
  T501.C501_Order_Date_Updated_By                AS Updated_By_Id,
  Get_User_Name(T501.C501_Order_Date_Updated_By) AS Updated_By_Name
FROM T501_Order T501,
  T941_Audit_Trail_Log T941,
  T502_Item_Order t502,
  (SELECT C941_Ref_Id Order_Id,
    MAX(C941_Updated_Date) Last_Updated_Date
  FROM T941_Audit_Trail_Log
  GROUP BY C941_Ref_Id
  )T941_Date
WHERE T941.C941_Ref_Id          =T941_Date.Order_Id
AND T941_Date.Last_Updated_Date =T941.C941_Updated_Date
AND T501.C501_Order_Id          = T941.C941_Ref_Id
AND t502.C501_Order_Id          = T501.C501_Order_Id
AND T941.C940_Audit_Trail_Id    ='304'
AND T501.C501_Void_Fl          IS NULL
AND (T501.C501_Order_Date Between Trunc( To_Date(@Prompt('From_Order_Date','D',,Mono,,Persistent),'dd-mm-yyyy hh24:mi:ss'))
AND Trunc( To_Date(@Prompt('To_Order_Date','D',,Mono,,Persistent),'dd-mm-yyyy hh24:mi:ss')))
GROUP BY T501.C501_Order_Id ,
  T501.C704_Account_Id ,
  Get_Account_Name(T501.C704_Account_Id) ,
  T501.C501_Order_Date ,
  T941.C941_Value ,
  T501.C1900_Company_Id ,
  Get_Company_Name (T501.C1900_Company_Id) ,
  T501.C501_Order_Date_Updated_Date ,
  T501.C501_Order_Date_Updated_By ,
  Get_User_Name(T501.C501_Order_Date_Updated_By)