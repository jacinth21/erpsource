SELECT t526.C525_PRODUCT_REQUEST_ID as "Request id"
,T504_CL.C504_CONSIGNMENT_ID as "Consignment_Id"
,t526.C207_SET_ID as "Set_Id"
,t207.C207_SET_DESC as "Set_Desc"
,T504_CL.C504A_ETCH_ID as "Etch_Id"
,DECODE(T504_CL.C504A_STATUS_FL,0, 'Available',5, 'Allocated',7, 'Pending Pick',10, 'Pending Shipping',13, 'Packing In Progress',16, 'Ready For Pickup',20, 'Pending Return',22, 'Missing',23, 'Deployed',25, 'Pending Check',30, 'WIP - Loaners',40, 'Pending Verification',50, 'Pending Process',55, 'Pending Picture',58, 'Pending Putaway',60, 'Inactive') as "Loaner_Status"
FROM T504A_LOANER_TRANSACTION T504A_LT,t526_product_request_detail t526, T504A_CONSIGNMENT_LOANER T504_CL,t207_set_master T207
WHERE T504A_LT.c526_product_request_detail_id=t526.c526_product_request_detail_id
AND T504A_LT.c504_consignment_id=T504_CL.c504_consignment_id
AND t526.c207_set_id=t207.c207_set_id
AND TRUNC(C526_REQUIRED_DATE)=TRUNC(SYSDATE)
AND t504a_lt.c504a_void_fl IS NULL
AND t526.c526_void_fl IS NULL
AND t504_cl.c504a_void_fl IS NULL
AND T207.C207_VOID_FL IS NULL
AND T504A_LT.c1900_company_id = 1000
AND t526.c1900_company_id = 1000
AND T504_cl.C5040_PLANT_ID = 3000
AND T207.C1900_COMPANY_ID = 1000 
ORDER BY t526.C525_PRODUCT_REQUEST_ID 