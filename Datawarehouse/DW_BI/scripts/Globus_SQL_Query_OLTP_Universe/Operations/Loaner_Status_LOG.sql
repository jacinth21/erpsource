select t504.c207_set_id as setid, substr(get_set_name(t504.c207_set_id),0,32) as setname,t504a.C504A_ETCH_ID as etchid, stat.c905_ref_id as refid, stat.c905_loaner_turn_no, 
get_short_user_name(get_user_name(stat.accepted_by))  as accepted_by, stat.accepted_date, stat.accepted_time, 
get_short_user_name(get_user_name(stat.CHECKED_by)) as CHECKED_by, stat.CHECKED_date, stat.CHECKED_time, 
get_short_user_name(get_user_name(stat.Processed_by)) as Processed_by,stat.Processed_date, stat.Processed_time, 
get_short_user_name(get_user_name(stat.Picture_taken_by)) as Picture_taken_by, stat.Picture_taken_date,stat.Picture_taken_time 
, current_status,trunc(stat.Updated_Date) as Updated_date,get_user_name(stat.Updated_By) as Updated_By
from t504_consignment t504, (
select c905_ref_id, c905_loaner_turn_no
,  MAX(DECODE(c905_status_fl,100,c905_updated_by,''))accepted_by
, MAX( DECODE(c905_status_fl,100,TRUNC(c905_updated_date),''))accepted_date
, MAX( DECODE(c905_status_fl,100,TO_CHAR(c905_updated_date,'HH:MI AM'),''))accepted_time
,  MAX(DECODE(c905_status_fl,200,c905_updated_by,'')) CHECKED_by
,  MAX(DECODE(c905_status_fl,200,TRUNC(c905_updated_date),'')) CHECKED_date
,  MAX(DECODE(c905_status_fl,200,TO_CHAR(c905_updated_date,'HH:MI AM'),'')) CHECKED_time
,  MAX(DECODE(c905_status_fl,250,c905_updated_by,'')) Processed_by
,  MAX(DECODE(c905_status_fl,250,TRUNC(c905_updated_date),''))  Processed_date
, MAX( DECODE(c905_status_fl,250,TO_CHAR(c905_updated_date,'HH:MI AM'),'')) Processed_time
,  MAX(DECODE(c905_status_fl,300,c905_updated_by,'')) Picture_taken_by
,  MAX(DECODE(c905_status_fl,300,TRUNC(c905_updated_date),'')) Picture_taken_date
,  MAX(DECODE(c905_status_fl,300,TO_CHAR(c905_updated_date,'HH:MI AM'),'')) Picture_taken_time
, MAX(c905_status_fl) current_status, MAX(c905_updated_date) Updated_Date, MAX(c905_updated_by) as Updated_By
from T905_STATUS_DETAILS t905 
,t208_set_details t208, t504_consignment t504  
where c901_source = 91107 
AND t208.c208_void_fl IS NULL
AND t208.c208_inset_fl = 'Y'
AND t504.c207_set_id = t208.c207_set_id 
AND T504.C1900_COMPANY_ID = 1000
AND t905.c1900_company_id = 1000
AND t504.c504_void_fl IS NULL 
AND t504.c504_consignment_id =t905.c905_ref_id 
AND c905_status_fl IN ('100','200','250','300') 
AND trunc(c905_updated_date) >= trunc( to_date(@prompt('From date','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss'))
AND trunc(c905_updated_date) <= trunc(to_date(@prompt('To date','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss'))
GROUP BY c905_ref_id ,c905_loaner_turn_no
) stat , t504a_consignment_loaner t504a 
WHERE 
t504.c504_consignment_id=stat.c905_ref_id 
AND t504a.C504_CONSIGNMENT_ID = stat.c905_ref_id 
AND T504.C504_VOID_FL IS NULL
AND t504a.c1900_company_id = 1000