PC-190 - 
Created By: Karthik Somanathan



SELECT locationid AS  "Location_Id",
  location AS  "Location",
  SUM (dhr_sum) AS  "Quantity",
  SUM (greater_5_days) AS ">5 DAYS" ,
  SUM (greater_10_days) AS ">10 DAYS"
FROM
  (SELECT locationid,
    location,
    (
    CASE
      WHEN dt <= TRUNC (SYSDATE) - 5
      AND dt   > TRUNC (SYSDATE) - 10
      THEN COUNT (1)
      ELSE 0
    END) greater_5_days,
    (
    CASE
      WHEN dt <= TRUNC (SYSDATE) - 10
      THEN COUNT (1)
      ELSE 0
    END) greater_10_days,
    COUNT (1) location_cnt ,
    SUM (dhr_cnt) dhr_sum
  FROM
    (SELECT COUNT (t408.c408_dhr_id) dhr_cnt,
      t408.c5052_location_id locationid,
      t5052.c5052_location_cd location,
   TRUNC (t408.c408_location_scan_dt) dt
    FROM t408_dhr t408,
      t5052_location_master t5052
    WHERE t408.c5052_location_id = t5052.c5052_location_id
    AND t5052.c901_location_type = '26241098'
    AND t5052.c901_status        = '93310'
    AND t5052.c5052_void_fl     IS NULL
    AND t408.c408_void_fl       IS NULL
    AND t408.c5052_location_id  IS NOT NULL
    GROUP BY t408.c5052_location_id,
      t5052.c5052_location_cd,
      TRUNC (t408.c408_location_scan_dt) ,
      t408.c408_dhr_id
    )
  GROUP BY locationid,
    location,
    dt
  )
GROUP BY locationid,
  location

