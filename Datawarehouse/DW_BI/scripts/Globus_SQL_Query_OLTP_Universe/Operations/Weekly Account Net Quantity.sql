PC-3342 - Weekly Account Net Quantity
By Suganthi Sharmila

SELECT DECODE (t5051.c5051_inv_warehouse_id, 3, get_distributor_name (t5052.c5052_ref_id), 5, get_account_name ( t5052.c5052_ref_id), t5052.c5052_location_cd) location,
  get_code_name (t5052.c901_location_type) loctype,
  t5052.c5052_ref_id acc_id,t704.c704_account_nm,
  get_code_name (t5052.c901_status) status,
  t5053.c205_part_number_id pnum,
  t205.c205_part_num_desc pdesc ,
  t5053.c5053_curr_qty qty,
  t5052.c5052_location_id locationid,
  t5052.c5052_location_cd locationcd ,
  t5052.c901_status status_id,
  t5051.c5051_inv_warehouse_id warehouse_id,
  t5051.c5051_inv_warehouse_nm warehouse_nm ,
  t5051.C901_WAREHOUSE_TYPE warehouse_type,
  t5051.C901_STATUS_ID warehouse_status_id,
  get_code_name ( t5051.C901_STATUS_ID) warehouse_status_nm,
  t5053.C5053_MAX_QTY maxqty,
  t5053.C5053_MIN_QTY minqty ,
  t5053.C5053_LAST_UPDATE_TRANS_ID last_trans_id,
  NVL(t5053.C5053_LAST_UPDATED_DATE,t5053.C5053_CREATED_DATE),
  t5053.C901_LAST_TRANSACTION_TYPE last_trans_type,
  get_code_name ( C901_LAST_TRANSACTION_TYPE) last_trans_type_nm,
  t5053.C901_TYPE map_type,
  t5053.C901_ACTION map_action ,
  get_code_name (t5053.C901_TYPE) map_type_nm,
  get_code_name (t5053.C901_ACTION) map_action_nm ,
  get_company_name (t5052.c1900_company_id) company_name ,
  get_code_name(C205_PRODUCT_FAMILY) Product_family
FROM t5053_location_part_mapping t5053,
  t5051_inv_warehouse t5051,
  t5052_location_master t5052 ,
  t205_part_number t205 , t704_account t704
WHERE t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id
AND t205.c205_part_number_id = t5053.c205_part_number_id
AND t5053.c5052_location_id = t5052.c5052_location_id
AND regexp_like (t5053.c205_part_number_id, NVL (NULL, t5053.c205_part_number_id))
AND t5052.c5052_location_id = NVL (NULL, t5052.c5052_location_id)
AND t5052.c5052_ref_id = DECODE ('0', '0', t5052.c5052_ref_id, '0')
AND t5052.c901_location_type = DECODE ('0', '0', t5052.c901_location_type, '0')
and t704.c704_account_id = t5052.c5052_ref_id
AND t5051.c5051_inv_warehouse_id = 5
AND t5052.c1900_company_id =1026
AND t5052.c5052_void_fl IS NULL
ORDER BY t5052.c1900_company_id,
  location,
  t5053.c205_part_number_id;
 
