PC-227 - Insert Available Quantity in Shelf
Created By: Karthik Somanathan

Description: Plant Id= 3000 Audobon

SELECT t205c.c205_part_number_id AS  "Part Number",
  get_partnum_desc(t205c.c205_part_number_id) AS "Part Desc",
  t205c.C5040_PLANT_ID AS PLANT_ID,
  NVL (t205c.c205_available_qty, 0)  AS "FG QTY"
FROM GLOBUS_APP.t205c_part_qty t205c ,
  GLOBUS_APP.v205c_part_allocated_qty v205c
WHERE t205c.c205_part_number_id IN
  (SELECT c205_insert_id FROM t205j_part_insert WHERE c205j_void_fl IS NULL
  )
AND t205c.C901_Transaction_Type        = 90800 --90800 - Inventory Qty
AND t205c.c205_part_number_id          = v205c.c205_part_number_id(+)
AND v205c.c901_transaction_type(+)     = '60001' -- FG Inventory
AND t205c.C5040_PLANT_ID               = v205c.C5040_PLANT_ID(+)
AND NVL (t205c.c205_available_qty, 0) <= 100


