SELECT t504.c207_set_id setid,Get_Set_Name(t504.c207_set_id) as Set_Desc,  t5010.c5010_tag_id tagid, t504a.c504a_etch_id etchid
  , Get_Code_Name (T504.C504_Type) as Settype, Gm_Pkg_Op_Set_Put_Rpt.Get_Loaner_Future_Status (T5010.C5010_Tag_Id,T504a.C504_Consignment_Id) as Nextstatus
  , t504.c504_type type, t504.c504_consignment_id REFID, T504a.C504a_Planned_Ship_Date planship_date
   FROM t504a_consignment_loaner t504a, t504_consignment t504, t5010_tag t5010
  WHERE t504a.c504_consignment_id = t5010.c5010_last_updated_trans_id (+) 
    AND t504a.c504_consignment_id         = t504.c504_consignment_id
    AND t504.c504_type                    = 4127
    AND t504a.c504a_status_fl             = 58 --pending FG
    AND t504a.c504a_void_fl              IS NULL
    AND t504.c504_void_fl                IS NULL
    AND T5010.C5010_VOID_FL(+)              IS NULL
    AND t504a.C5040_PLANT_ID = 3000
    AND t504.C1900_COMPANY_ID = 1000
    AND t5010.c1900_company_id = 1000
    AND T504a.C504a_Planned_Ship_Date = trunc(sysdate)