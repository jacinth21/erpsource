PC-3407 -DOs pending PO for GM Italy
By Andrews Stanley





SELECT   t501.c501_customer_po  AS "CustomerPO", 
t501.c501_customer_po_date AS  "PO_Date", 
t501.c704_account_id AS "customer_id",                
t704.C704_ACCOUNT_NM   AS "customer_name",
       t501.c501_order_id AS "DOID", 
       t501.c501_parent_order_id  AS "ParentDOID" ,           
       DECODE (t501.c501_customer_po, NULL, 'N', 'Y')  AS "Readytoinvoice", 
       t501.c501_order_date AS  "do_date",
       ROUND (CURRENT_DATE - t501.c501_order_date) AS  "AGING",            
       t502.c205_part_number_id  AS "product_id", 
      T205.C205_PART_NUM_DESC AS  "product_desc",              
       t502b.usedqty  AS "qty", 
       t502.c502_item_price AS  "Item_price" ,                
       t502.c502_item_price * t502b.usedqty AS "total_price"  ,              
       t502b.ddt AS   "ddt" ,                            
       t502b.usedlot AS "usedlot",                           
       (select t5034.c5034_proforma_no       
           from t5034_proforma t5034, t5035_proforma_order_map t5035       
           where t5034.c5034_proforma_id = t5035.c5034_proforma_id       
           and t5035.c501_order_id = t501.c501_order_id       
           and t5035.c5035_void_fl is null      
           AND ROWNUM = 1) AS "Vendita #",       
       t501a.SurgeryDate  AS "SurgeryDate",               
       t501a.ClinicalReport AS "ClinicalReport",                
       t501a.Surgeonn  AS "Surgeon",              
       t501a.PatientInitial AS "PatientInitial",            
         v700.AD_NAME AS "TerritoryManagerNm",    
        t703.C703_SALES_REP_NAME AS "SalesRep",           
       t501.c501_comments AS "Comments"                
    FROM t501_order t501, t502_item_order t502,t704_Account t704, T703_SALES_REP T703,t205_part_number t205,V700_TERRITORY_MAPPING_DETAIL V700, (select c501_order_id orderid             
, MAX (DECODE (c901_attribute_type, 400151, c501a_attribute_value, NULL)) SurgeryDate                
 , MAX (DECODE (c901_attribute_type, 400153, c501a_attribute_value, NULL)) ClinicalReport                
, MAX (DECODE (c901_attribute_type, 400146, c501a_attribute_value, NULL)) Surgeonn                
, MAX (DECODE (c901_attribute_type, 400145, c501a_attribute_value, NULL)) PatientInitial               
from t501a_order_attribute              
GROUP BY c501_order_id)t501a ,t500_order_info t500,     
 (SELECT c501_order_id usageordid, c205_part_number_id pnum,c502b_item_qty usedqty,c502b_ref_id ddt,c502b_usage_lot_num usedlot              
            FROM t502b_item_order_usage                
            where c502b_void_fl is null) t502b     
     WHERE t501.c503_invoice_id IS NULL                
     AND t501.c1900_company_id = 1020                
     AND t501.c501_delete_fl IS NULL                
     AND t501.c501_void_fl IS NULL                
     AND t501.c501_order_id = t502.c501_order_id  
     AND t501.C704_ACCOUNT_ID= t704.C704_ACCOUNT_ID(+)
     AND t501.C703_SALES_REP_ID = t703.C703_SALES_REP_ID(+)
     AND t502.c205_part_number_id = t205.c205_part_number_id(+)
    AND T501.C704_ACCOUNT_ID = v700.AC_ID(+)
     AND t500.c500_order_info_id = t502.c500_order_info_id     
     and t501.c501_order_id = t501a.orderid(+)     
     and t501.c501_order_id = t502b.usageordid      
     AND t502b.pnum = t502.c205_part_number_id          
     AND NVL (c901_order_type, -9999) NOT IN (102080, 101260)      
     AND NVL (c901_order_type, -9999) NOT IN (SELECT t906.c906_rule_value                
     FROM t906_rules t906                
     WHERE t906.c906_rule_grp_id = 'ORDTYPE' AND c906_rule_id = 'EXCLUDE')                
    GROUP BY t501.c501_customer_po                
       , t501.c501_customer_po_date                
       , t501.c704_account_id    
       ,t704.C704_ACCOUNT_NM  
      ,t703.C703_SALES_REP_NAME
      , T205.C205_PART_NUM_DESC 
      ,V700.AD_NAME
       , t501.c501_order_id
       , t501.c501_parent_order_id
       , t501.c501_order_date                               
       , t501.c501_ad_id                
       , t502.c205_part_number_id                
       , t502.c502_item_price                
       , t501.c501_comments  
       ,t502b.usedqty  
       , t502b.ddt                               
       , t502b.usedlot      
       , t501a.SurgeryDate             
       , t501a.ClinicalReport                
       , t501a.Surgeonn                
       , t501a.PatientInitial             
ORDER BY "do_date","SalesRep", "customer_id","DOID";