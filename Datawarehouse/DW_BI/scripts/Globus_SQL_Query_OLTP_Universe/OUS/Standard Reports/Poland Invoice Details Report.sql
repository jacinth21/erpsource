PC-3409 -Poland Invoice Details Report
By Andrews Stanley


SELECT t501.c503_invoice_id AS "INVID", 
  T503.C503_INVOICE_DATE AS "INVDT", 
  t501.c501_order_date AS "SALESDT", 
  T502.C205_PART_NUMBER_ID AS "Part Number Id", 
   	(SELECT TRIM(REGEXP_REPLACE(t2000.c2000_ref_desc ,'<[^>]*>',' ')) 
	   	   FROM t2000_master_metadata t2000 
         WHERE t2000.c2000_ref_id = T502.C205_PART_NUMBER_ID 
           AND c901_language_id = 10306092 
           AND c901_ref_type = 103090 
           AND t2000.c2000_void_fl IS NULL) AS PDESC, 
  T502.C502_CONTROL_NUMBER CNUM, 
  DECODE(get_partnum_uom(T502.C205_PART_NUMBER_ID),20511,'szt.',get_code_name((get_partnum_uom(T502.C205_PART_NUMBER_ID)))) uom, 
  T502.C502_ITEM_QTY QTY, 
  NVL(c502_unit_price,0) unitprice, 
  NVL(T502.C502_ITEM_QTY * c502_unit_price,0)NETVAL, 
  NVL(T502.c502_vat_rate,0) VAT , 
  NVL(T502.c502_tax_amt, 0) tax_cost, 
  NVL(T502.C502_ITEM_QTY * c502_unit_price,0) + NVL(T502.c502_tax_amt, 0) grossval, 
  get_account_name(t503.c704_account_id) custname, 
  c704_ship_add1 ||' '|| c704_ship_zip_code ||' '|| c704_ship_city custadd, 
  GET_ACCOUNT_ATTRB_VALUE (t503.c704_account_id,'101185') CUSTNIP# 
FROM T502_ITEM_ORDER T502, T501_ORDER T501, T503_INVOICE T503, t704_account t704 
WHERE TRUNC(c503_INVOICE_DATE) >= Trunc(SYSDATE,'Month') 
AND Trunc(c503_INVOICE_DATE)  <= Trunc(Last_day(SYSDATE))
AND T502.C502_VOID_FL    IS NULL 
AND T501.c503_invoice_id  = T503.c503_invoice_id 
and t704.c704_account_id = t503.c704_account_id 
and t704.c704_account_id = t501.c704_account_id 
AND T502.C502_DELETE_FL  IS NULL 
AND T502.C501_ORDER_ID    = T501.C501_ORDER_ID 
AND T501.C1900_COMPANY_ID = '1014' 
AND T501.c501_void_fl    IS NULL 
ORDER BY T502.C205_PART_NUMBER_ID
