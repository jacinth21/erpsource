SELECT   Order_Date, Order_ID,Acct_ID,Acct_Name,Rep_Name,Price_Hold,Order_Value,Collector_Name,Customer_PO,Cust_PO_Date,AR_Currency_Symb,Ship_Cost,PO_Amount,Invoice_ID,Invoice_Date
, get_code_name(t501a.c501a_attribute_value) as Ipad_Order, t501a.c501a_attribute_value

    FROM (SELECT   order_date as Order_Date, order_id as Order_ID, acct_id as Acct_ID, acct_name as Acct_Name, get_rep_name (c703_sales_rep_id) as Rep_Name
                 , c501_hold_fl as Price_Hold, order_value as Order_Value, get_code_name (collector_name) as Collector_Name, Customer_PO
                 , Cust_PO_Date, get_code_name (arcurrencysymb) as AR_Currency_Symb, SUM (ship_cost)as Ship_Cost
                 , SUM (tot_cost) as PO_Amount, c503_invoice_id as Invoice_ID, c503_invoice_date as Invoice_Date
              FROM (SELECT t501.c501_order_date as Order_Date, NVL (t501.c501_parent_order_id
                                                               , t501.c501_order_id)as order_id
                         , t501.c704_account_id as acct_id, t501.c703_sales_rep_id, t704.c704_account_nm as acct_name
                         , NVL (t501.c501_total_cost, 0) order_value, NVL (t501.c501_ship_cost, 0) as ship_cost
                         , NVL (t501.c501_total_cost, 0) + NVL (t501.c501_ship_cost, 0) as tot_cost
                         , t501.c501_customer_po as Customer_PO, t501.c501_customer_po_date as Cust_PO_Date
                         , t704a.collectorid as collector_name, t704.c704_payment_terms as payment_terms
                         , t704.c901_currency as arcurrencysymb, t501.c501_hold_fl, t501.c503_invoice_id
                         , t503.c503_invoice_date
                      FROM t501_order t501
                         , (SELECT   t704a.c704_account_id
                                   , MAX (DECODE (c901_attribute_type, 103050, c704a_attribute_value)) as collectorid
                                   , MAX (DECODE (c901_attribute_type, '101184', c704a_attribute_value)) as credittype
                                FROM t704a_account_attribute t704a
                               WHERE c704a_void_fl IS NULL
                                 AND c901_attribute_type IN (
                                            SELECT t906.c906_rule_value
                                              FROM t906_rules t906
                                             WHERE t906.c906_rule_grp_id = 'ACCNT_ATTR'
                                                   AND c906_rule_id = 'INCLUDE_ATTR')
                            GROUP BY c704_account_id) t704a
                         , t704_account t704
                         , t503_invoice t503
                     WHERE t501.c501_void_fl IS NULL
                     
                       AND t501.c704_account_id = t704.c704_account_id
                       AND t501.c501_order_date >= TO_DATE ('01/01/2017', 'MM/DD/YYYY')
                       AND t501.c501_delete_fl IS NULL
                       AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id
                                                                                               FROM v901_country_codes))
                       AND NVL (t501.c901_order_type, -9999) <> 2535
                       AND t501.c503_invoice_id = t503.c503_invoice_id(+)
                       AND NVL (c901_order_type, -9999) NOT IN (
                                                    SELECT t906.c906_rule_value
                                                      FROM t906_rules t906
                                                     WHERE t906.c906_rule_grp_id = 'ORDTYPE'
                                                           AND c906_rule_id = 'EXCLUDE')
                       AND NVL (c901_order_type, -9999) NOT IN (
                                                  SELECT t906.c906_rule_value
                                                    FROM t906_rules t906
                                                   WHERE t906.c906_rule_grp_id = 'ORDERTYPE'
                                                         AND c906_rule_id = 'EXCLUDE')
                       AND t501.c704_account_id = t704a.c704_account_id(+)
                       AND t501.c1900_company_id = '1000'
                       AND t704.c901_currency = 1) 
          GROUP BY order_date
                 , order_id
                 , acct_id
                 , acct_name
                 , c703_sales_rep_id
                 , c501_hold_fl
                 , order_value
                 , collector_name
                 , customer_po
                 , cust_po_date
                 , arcurrencysymb
                 , c503_invoice_id
                 , c503_invoice_date
                 ),
                         t501a_order_attribute t501a
   WHERE 
     t501a.c501_order_id=order_id
     and t501a.c901_attribute_type = 103560 
    and t501a.c501a_attribute_value = 103521  

ORDER BY order_date, order_id