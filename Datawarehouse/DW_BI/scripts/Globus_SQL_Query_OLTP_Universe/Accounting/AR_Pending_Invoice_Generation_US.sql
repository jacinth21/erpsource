-- PC-2041 Add PO Status
-- Modified By : Rajeshwaran Varatharajan
SELECT Order_Date, Order_ID, Acct_ID
  , Acct_Name, get_rep_name (c703_sales_rep_id) AS Rep_Name, C501_Hold_Fl AS Price_Hold
  , get_code_name (collector_name) AS Collector_Name, Customer_PO, CUST_PO_DATE
  , GET_CODE_NAME (ARCURRENCYSYMB) AS AR_CURRENCY_SYMB, SHIP_COST AS FREIGHT, (tot_cost) AS tot_cost
  , ORDER_VALUE AS Order_Value, PO_Amt AS PO_AMT, Order_Mode
  , Invoice_ID, Invoice_Date , PO_status
   FROM
    (
         SELECT t501.c501_order_date AS Order_Date, NVL (t501.c501_parent_order_id, t501.c501_order_id) AS Order_ID,
            T501.C704_ACCOUNT_ID AS ACCT_ID, T501.C703_SALES_REP_ID, T704.C704_ACCOUNT_NM AS ACCT_NAME
          , (NVL (T501.C501_TOTAL_COST, 0) + NVL (T501.C501_SHIP_COST, 0) + NVL (
            (
                 SELECT SUM (C501_TOTAL_COST)
                   FROM T501_ORDER
                  WHERE C501_PARENT_ORDER_ID           = T501.C501_ORDER_ID
                    AND NVL (C901_ORDER_TYPE, - 9999) <> 2535
                    AND C501_VOID_FL                  IS NULL
            )
            , 0)) AS TOT_COST, (NVL (T501.C501_TOTAL_COST, 0) + NVL (
            (
                 SELECT SUM (C501_TOTAL_COST)
                   FROM T501_ORDER
                  WHERE C501_PARENT_ORDER_ID           = T501.C501_ORDER_ID
                    AND NVL (C901_ORDER_TYPE, - 9999) <> 2535
                    AND C501_VOID_FL                  IS NULL
            )
            , 0)) AS ORDER_VALUE, NVL (T501.C501_SHIP_COST, 0) AS SHIP_COST, t501.c501_po_amount PO_Amt
          , get_code_name (t501.C501_RECEIVE_MODE) Order_Mode, t501.c501_customer_po AS Customer_PO,
            t501.c501_customer_po_date AS Cust_PO_Date, t704a.collectorid AS collector_name, t704.c704_payment_terms AS
            Payment_Terms, t704.c901_currency AS arcurrencysymb, T501.C501_Hold_Fl
          , DECODE (T501.C501_Parent_Order_Id, NULL, 'N', 'Y') Child_Order_Fl, V700.D_Name AS Dist_name,
            t503.C503_INVOICE_ID Invoice_ID, t503.C503_INVOICE_DATE Invoice_Date
			, get_code_name(t5001.C901_CUST_PO_STATUS) PO_status 
           FROM t501_order t501, T503_INVOICE t503, (
                 SELECT t704a.c704_account_id, MAX (DECODE (c901_attribute_type, 103050, c704a_attribute_value)) AS
                    collectorid, MAX (DECODE (c901_attribute_type, '101184', c704a_attribute_value)) AS credittype
                   FROM t704a_account_attribute t704a
                  WHERE c704a_void_fl       IS NULL
                    AND c901_attribute_type IN
                    (
                         SELECT t906.c906_rule_value
                           FROM t906_rules t906
                          WHERE t906.c906_rule_grp_id = 'ACCNT_ATTR'
                            AND c906_rule_id          = 'INCLUDE_ATTR'
                    )
               GROUP BY C704_Account_Id
            )
            T704a, t704_account t704, V700_Territory_Mapping_Detail v700 ,  T5001_ORDER_PO_DTLS t5001 
          WHERE 
		  
			t501.c501_order_id = t5001.C501_ORDER_ID(+) 
			AND t5001.C5001_VOID_FL(+) IS NULL
			and t501.C503_INVOICE_ID       = t503.C503_INVOICE_ID(+)
            AND t503.c503_void_fl         IS NULL
            AND t501.c501_void_fl         IS NULL
            AND T501.C704_Account_Id       = T704.C704_Account_Id
            AND V700.Ac_Id                 = T704.C704_Account_Id
            AND t501.c503_invoice_id      IS NULL
            AND T501.C501_CUSTOMER_PO     IS NOT NULL
            AND T501.C901_Parent_Order_Id IS NULL
            AND t501.c501_delete_fl       IS NULL
            AND (t501.c901_ext_country_id IS NULL
            OR t501.c901_ext_country_id   IN
            (
                 SELECT country_id FROM v901_country_codes
            ))
            AND NVL (t501.c901_order_type, - 9999)     <> 2535
            AND NVL (c901_order_type,      - 9999) NOT IN
            (
                 SELECT t906.c906_rule_value
                   FROM t906_rules t906
                  WHERE t906.c906_rule_grp_id = 'ORDTYPE'
                    AND c906_rule_id          = 'EXCLUDE'
            )
            AND NVL (c901_order_type, - 9999) NOT IN
            (
                 SELECT t906.c906_rule_value
                   FROM t906_rules t906
                  WHERE t906.c906_rule_grp_id = 'ORDERTYPE'
                    AND c906_rule_id          = 'EXCLUDE'
            )
            AND t501.c704_account_id  = t704a.c704_account_id(+)
            AND t501.c1900_company_id = '1000'
            AND T704.C901_Currency    = 1
    )
  WHERE CHILD_ORDER_FL = 'N'
GROUP BY ORDER_DATE, ORDER_ID, ACCT_ID
  , ACCT_NAME, C703_SALES_REP_ID, C501_HOLD_FL
  , COLLECTOR_NAME, CUSTOMER_PO, CUST_PO_DATE
  , ARCURRENCYSYMB, TOT_COST, SHIP_COST
  , Order_Value, PO_AMT, Order_Mode
  , Invoice_ID, Invoice_Date, po_status
ORDER BY Order_Date, Order_Id
