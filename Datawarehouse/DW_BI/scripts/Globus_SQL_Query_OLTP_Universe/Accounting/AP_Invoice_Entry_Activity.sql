SELECT   t406.c406_created_by as Created_By, t101.c101_user_f_name || ' ' || t101.c101_user_l_name as name, t406.c406_entered_dt as Entered_date
       , COUNT (1) as Entry_count, SUM(c406_payment_amount) as Payment_Amount
    FROM t406_payment t406, t101_user t101
   WHERE t406.c406_entered_dt BETWEEN @prompt('Enter From Entered Date mm/dd/yyyy','D',,mono,free,persistent) AND @prompt('Enter To Entered Date mm/dd/yyyy','D',,mono,free,persistent)
     AND t101.c101_user_id = t406.c406_created_by
     AND t406.c406_void_fl IS NULL 
GROUP BY t406.c406_created_by, t406.c406_entered_dt, t101.c101_user_f_name , t101.c101_user_l_name
ORDER BY t406.c406_entered_dt, t406.c406_created_by;