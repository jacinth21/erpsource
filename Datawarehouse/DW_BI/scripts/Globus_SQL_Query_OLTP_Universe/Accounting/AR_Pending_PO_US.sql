-- PC-2887 : Revenue tracking information not showing pending PO screen.
-- Owner : mmuthusamy 
 
SELECT Order_Date, Order_Id, Acct_Id
  , acct_name, get_rep_name (c703_sales_rep_id) AS Rep_Name, Dist_Name
  , Signed_DO.DO_Tracking_Option Revenue_Tracking_DO_Value, iPad_order_flag.val iPad_order_flag, Contract_Flag.Val
    Contract_Flag, Order_Value, Get_Code_Name (Collector_Name) Collector_Name
  , GET_CODE_NAME (PAYMENT_TERMS) AS PAYMENT_TERMS, GET_CODE_NAME (ARCURRENCYSYMB) AS AR_CURRENCY_SYMB, SUM (SHIP_COST)
    AS Ship_Cost, Tot_Cost AS Tot_Cost, COMP_ID
   FROM
    (
         SELECT T501.C501_Order_Date AS Order_Date, NVL (T501.C501_Parent_Order_Id, T501.C501_Order_Id) AS Order_Id,
            T501.C704_ACCOUNT_ID AS ACCT_ID, T501.C703_SALES_REP_ID, T704.C704_ACCOUNT_NM AS ACCT_NAME
          , (NVL (T501.C501_TOTAL_COST, 0) + NVL (T501.C501_SHIP_COST, 0) + NVL (
            (
                 SELECT SUM (C501_TOTAL_COST)
                   FROM T501_ORDER
                  WHERE C501_PARENT_ORDER_ID           = T501.C501_ORDER_ID
                    AND NVL (C901_ORDER_TYPE, - 9999) <> 2535
                    AND C501_VOID_FL                  IS NULL
            )
            , 0)) AS TOT_COST, (NVL (T501.C501_TOTAL_COST, 0) + NVL (
            (
                 SELECT SUM (C501_TOTAL_COST)
                   FROM T501_ORDER
                  WHERE C501_PARENT_ORDER_ID           = T501.C501_ORDER_ID
                    AND NVL (C901_ORDER_TYPE, - 9999) <> 2535
                    AND C501_VOID_FL                  IS NULL
            )
            , 0)) AS ORDER_VALUE, NVL (T501.C501_SHIP_COST, 0) AS SHIP_COST, t704a.collectorid AS collector_name
          , t704.c704_payment_terms AS payment_terms, t704.c901_currency AS ARCURRENCYSYMB, DECODE (
            T501.C501_Parent_Order_Id, NULL, 'N', 'Y') Child_Order_Fl, V700.D_Name AS Dist_name, V700.COMPID COMP_ID
           FROM t501_order t501, T701_Distributor T701, T703_Sales_Rep T703
          , T101_User T101, (
                 SELECT t704a.c704_account_id, MAX (DECODE (c901_attribute_type, 103050, c704a_attribute_value)) AS
                    Collectorid, MAX (DECODE (C901_Attribute_Type, '101184', C704a_Attribute_Value)) AS Credittype
                   FROM t704a_account_attribute t704a, T901_Code_Lookup T901
                  WHERE C704a_Void_Fl              IS NULL
                    AND T704a.C704a_Attribute_Value = T901.C901_Code_Id
                    AND T704a.C901_Attribute_Type   = '103050'
                    AND c901_attribute_type        IN
                    (
                         SELECT t906.c906_rule_value
                           FROM t906_rules t906
                          WHERE t906.c906_rule_grp_id = 'ACCNT_ATTR'
                            AND c906_rule_id          = 'INCLUDE_ATTR'
                    )
               GROUP BY c704_account_id
            )
            T704a, T704_Account T704, V700_Territory_Mapping_Detail v700
          WHERE T501.C501_Void_Fl         IS NULL
            AND T501.C704_ACCOUNT_ID       = T704.C704_ACCOUNT_ID
            AND V700.Ac_Id                 = T704.C704_Account_Id
            AND T704.C901_Currency         = '1'
            AND t501.c503_invoice_id      IS NULL
            AND T501.C501_Customer_Po     IS NULL
            AND T501.C501_Status_Fl       IS NOT NULL
            AND T501.C703_Sales_Rep_Id     = T703.C703_Sales_Rep_Id
            AND T701.C701_Distributor_Id   = T703.C701_Distributor_Id
            AND T501.C704_Account_Id       = T704a.C704_Account_Id (+)
            AND T501.C901_PARENT_ORDER_ID IS NULL
            AND T501.C1900_Company_Id      = '1000'
            AND T501.C501_Delete_Fl       IS NULL
            AND T501.C501_Created_By       = T101.C101_User_Id
            AND (T501.C901_Ext_Country_Id IS NULL
            OR t501.c901_ext_country_id   IN
            (
                 SELECT country_id FROM v901_country_codes
            ))
            AND T501.C703_Sales_Rep_Id IN
            (
                 SELECT Rep_Id FROM V700_Territory_Mapping_Detail WHERE Divid IN (100823)
            )
            AND NVL (t501.c901_order_type, - 9999)     <> 2535
            AND NVL (c901_order_type,      - 9999) NOT IN
            (
                 SELECT t906.c906_rule_value
                   FROM t906_rules t906
                  WHERE t906.c906_rule_grp_id = 'ORDTYPE'
                    AND c906_rule_id          = 'EXCLUDE'
            )
            AND NVL (c901_order_type, - 9999) NOT IN
            (
                 SELECT t906.c906_rule_value
                   FROM t906_rules t906
                  WHERE t906.c906_rule_grp_id = 'ORDERTYPE'
                    AND C906_RULE_ID          = 'EXCLUDE'
            )
            AND T501.C704_ACCOUNT_ID  = T704A.C704_ACCOUNT_ID(+)
            AND t501.c1900_company_id = '1000'
            AND t704.c901_currency    = 1
    )
    t501, (
    	-- PC-2887: Revenue sampling information not showing pending PO report
    	-- instead of order attribute get the data from t5003 table
         SELECT get_code_name (t5003.c901_do_dtls) DO_Tracking_Option, t5003.c501_order_id ord_id 
          FROM t5003_order_revenue_sample_dtls t5003
             WHERE t5003.c5003_void_fl IS NULL 
    )
    Signed_DO, (
         SELECT get_code_name (t501a.c501a_attribute_value) val, t501a.c501_order_id ord_id
           FROM t501a_order_attribute t501a
          WHERE T501a.C901_Attribute_Type = 103560
            AND t501a.c501a_void_fl      IS NULL
    )
    iPad_order_flag,
    /*
    (
    SELECT get_code_name (c704a_attribute_value) val, t704a.c704_account_id tmp_acc
    FROM t704a_account_attribute t704a
    WHERE t704a.c901_attribute_type = 903103
    and t704a.c704a_attribute_value not in '0'
    And T704a.C704a_Void_Fl      Is Null
    )
    Contract_Flag    */
    (
         SELECT get_code_name (c901_contract) val, t704d.c704_account_id tmp_acc
           FROM t704d_account_affln t704d
          WHERE t704d.c901_contract NOT IN '0'
            AND T704d.C704d_Void_Fl     IS NULL
    )
    Contract_Flag
  WHERE t501.ORDER_ID       = SIGNED_DO.ORD_ID (+)
    AND t501.Order_Id       = Ipad_Order_Flag.Ord_Id (+)
    AND T501.CHILD_ORDER_FL = 'N'
    AND t501.acct_id        = Contract_Flag.tmp_acc (+)
GROUP BY order_date, order_id, acct_id
  , Acct_Name, C703_Sales_Rep_Id, Order_Value
  , COLLECTOR_NAME, PAYMENT_TERMS, ARCURRENCYSYMB
  , DIST_NAME, SIGNED_DO.DO_TRACKING_OPTION, IPAD_ORDER_FLAG.VAL
  , CONTRACT_FLAG.VAL, TOT_COST, COMP_ID
ORDER BY ORDER_DATE, ORDER_ID;