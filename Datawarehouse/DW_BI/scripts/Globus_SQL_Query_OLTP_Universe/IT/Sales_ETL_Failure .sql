SELECT
 CEV501_SALES_ETL_F_ID        
, ETL_SALES_LOAD_DATE          
, ETL_SALES_LOAD_RUN_ID        
, ETL_SALES_COUNTRY_ID         
, C501_ORDER_ID                
, C501_TOTAL_COST              
, C502_ITEM_QTY                
, C502_ITEM_PRICE              
, C501_ORDER_ID_DEST           
, C501_EXT_TOTAL_COST          
, C502_ITEM_QTY_DEST           
, C502_EXT_ITEM_PRICE          
, C501_TOTAL_COST_DEST         
, C501_TOTAL_COST_BY_ITEM_DEST 
, DI_ERRORACTION               
, DI_ERRORCOLUMNS              
FROM  globus_etl.tev501_sales_etl_f;