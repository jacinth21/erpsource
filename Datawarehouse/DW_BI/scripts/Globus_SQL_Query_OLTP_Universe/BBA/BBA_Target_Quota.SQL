SELECT Get_ad_rep_name(t709.c703_sales_rep_id) as area_director_name,
       t703.c703_sales_rep_name as rep_name, 
       c709_quota_breakup_amt as quota
FROM   t709_quota_breakup t709,t703_sales_rep t703 
WHERE  t709.c703_sales_rep_id= t703.c703_sales_rep_id
AND c901_type = 20750 
AND c1900_company_id=1001
AND C703_VOID_FL IS NULL
AND trunc (to_date(@prompt('From_Order_Date','D',,mono,,persistent),'dd-mm-yyyy hh24:mi:ss')) BETWEEN c709_start_date AND c709_end_date 