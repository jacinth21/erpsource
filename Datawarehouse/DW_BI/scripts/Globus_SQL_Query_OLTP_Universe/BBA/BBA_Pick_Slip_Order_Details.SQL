SELECT t501.c501_order_id orderid, t501.c704_account_id AS "Account ID"
     , get_account_name (t501.c704_account_id) AS "Account Name", t501.c501_customer_po
     , t502.c205_part_number_id productno, get_partnum_desc (t502.c205_part_number_id) AS proddescrption
     , TO_CHAR (t2550.c2550_expiry_date, get_rule_value ('DATEFMT', 'DATEFORMAT')) AS prodexpiry
     , c502_control_number allograftno, get_part_size (t502.c205_part_number_id
                                                     , t2550.c2550_control_number) AS prodsize
     , t2550.c2540_donor_number proddonor, t502.c502_item_qty iqty, t502.c502_item_price price, c2550_mfg_date mfgdate
     , intl_use, research
  FROM (SELECT t2550.c205_part_number_id, t2550.c2550_control_number, t2550.c2550_expiry_date, t2550.c2550_custom_size
             , t2540.c2540_donor_number, t2550.c2540_donor_id, t2550.c2550_mfg_date
             , get_code_name (t2540.c901_international_use) AS intl_use, get_code_name (t2540.c901_research)
                                                                                                            AS research
          FROM t2550_part_control_number t2550, t2540_donor_master t2540 , t502_item_order t502
         WHERE NVL (t2550.c2540_donor_id, -999) = t2540.c2540_donor_id(+)
               AND t2550.c205_part_number_id  = t502.c205_part_number_id
               AND t502.C501_ORDER_ID  = @prompt('Enter Order ID','A',,mono,free,persistent)
               AND t502.c502_control_number = t2550.c2550_control_number ) t2550
     , t502_item_order t502
     , t501_order t501
WHERE t501.C501_ORDER_ID  = @prompt('Enter Order ID','A',,mono,free,persistent)
   AND t501.c501_order_id = t502.c501_order_id
   AND t502.c205_part_number_id = t2550.c205_part_number_id(+)
   AND t502.c502_control_number = t2550.c2550_control_number(+)
   AND t502.c502_void_fl IS NULL
   AND t501.c1900_company_id=1001