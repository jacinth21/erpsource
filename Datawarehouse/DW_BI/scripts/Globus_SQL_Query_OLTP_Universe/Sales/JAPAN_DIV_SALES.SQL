SELECT t1910.c1910_division_name division,
  NVL(t704.c704_account_nm_en,t704.c704_account_nm) account_name,
  NVL(t703.c703_sales_rep_name_en,t703.c703_sales_rep_name) sales_rep_name ,
  t501.c501_order_id order_id,
  t502.c205_part_number_id part_number,
  (t502.c502_item_price*t502.c502_item_qty) sales_amount
FROM t501_order t501,
  t502_item_order t502,
  t703a_salesrep_div_mapping t703a,
  t1910_division t1910,
  t703_sales_rep t703,
  t704_account t704
WHERE t703a.c703_sales_rep_id=t501.c703_sales_rep_id
AND t501.c501_order_id       =t502.c501_order_id
AND t1910.c1910_division_id  =t703a.c1910_division_id
AND t703.c703_sales_rep_id   =t703a.c703_sales_rep_id
AND t501.c704_account_id     =t704.c704_account_id
AND t501.c1900_company_id    =1026
AND t501.c501_void_fl       IS NULL
AND t502.c502_void_fl       IS NULL;