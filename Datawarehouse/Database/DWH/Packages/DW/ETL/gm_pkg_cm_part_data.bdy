 --@"C:\PMT\db\Packages\DWH\DW\ETL\gm_pkg_cm_part_data.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_part_data
IS
 PROCEDURE gm_cm_upd_part_num_desc (
 P_Job_Id  IN tei205_quality_valid_part.ce9001_job_id%TYPE 
 )
 AS
  CURSOR etl_part
        IS
          SELECT t205.c205_part_number_id part_no,
                 t205.c205_part_num_desc part_desc,
 				 t205.c205_etl_part_num_desc part_desc_etl
		  FROM globus_etl.tei205_quality_valid_part tei,
 				globus_app.t205_part_number t205
			WHERE t205.c205_part_number_id=tei.c205_part_number_id
			 AND  tei.ce9001_job_id=P_Job_Id;
 BEGIN
	FOR part_desc IN etl_part
        LOOP
        UPDATE globus_app.t205_part_number
		SET C205_part_num_desc   =NVL(part_desc.part_desc_etl,part_desc.part_desc)
		WHERE c205_part_number_id=part_desc.part_no;
	END LOOP;

END gm_cm_upd_part_num_desc;
END gm_pkg_cm_part_data;
