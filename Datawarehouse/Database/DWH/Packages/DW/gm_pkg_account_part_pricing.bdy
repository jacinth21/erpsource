--@"D:\Database\DW_BI\packages\gm_pkg_account_part_pricing.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_account_part_pricing
IS
PROCEDURE gm_upd_last_updated_date
AS
    /***************************************************************************************************
    * This procedure is to update t705_puf last updated date  and getting the data from Excel.
    * Author : Velu
    * Date    : 12/19/14
    ***************************************************************************************************/
    v_group_id t4010_group.c4010_group_id%TYPE;
    v_count  NUMBER;
    v_ref_id VARCHAR2 (20) ;
    v_c7051_last_updated_date DATE := NULL;
    v_c705_last_updated_date DATE;
    v_etl_table_update_time DATE;
    -- Load the all parts for last updated date from T705 and T7051 tables.
    CURSOR part_details
    IS
         SELECT C205_PART_NUMBER_ID p_num
           FROM TDW705_PUF_PARTS_DIM
          WHERE CDW705_ACTIVE_FL = 'Y';
BEGIN
    FOR currindex1 IN part_details
    LOOP
        BEGIN
            -- Individual Parts count only will take in T7051 table.
             SELECT COUNT (1)
               INTO v_count
               FROM t7051_account_group_pricing T7051
              WHERE c7051_ref_id    = currindex1.p_num
                AND c901_ref_type   = 52001 -- part
                AND c7051_void_fl  IS NULL
                AND c7051_active_fl = 'Y';
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_count := 0;
        END;
        -- Individual Parts last updated date will take in T7051 table.
        IF v_count <> 0 THEN
            BEGIN
                 SELECT MAX (C7051_LAST_UPDATED_DATE)
                   INTO v_c7051_last_updated_date
                   FROM t7051_account_group_pricing T7051
                  WHERE c7051_ref_id    = currindex1.p_num
                    AND c901_ref_type   = 52001 -- part
                    AND c7051_void_fl  IS NULL
                    AND c7051_active_fl = 'Y';
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_c7051_last_updated_date := NULL;
            END;
        END IF;
        -- Getting Group ID from T7051 table.
        IF v_count = 0 AND v_c7051_last_updated_date IS NULL THEN
            BEGIN
                 SELECT t4010.c4010_group_id
                   INTO v_group_id
                   FROM t4011_group_detail t4011, t4010_group t4010
                  WHERE t4010.c4010_group_id         = t4011.c4010_group_id
                    AND t4010.c4010_void_fl         IS NULL
                    AND t4010.c901_type              = 40045
                    AND t4011.c205_part_number_id    = currindex1.p_num
                    AND t4011.c901_part_pricing_type = 52080; -- primary part
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_group_id := 0;
            END;
            BEGIN
                -- Getting Group Last Updated Date from T7051 table.
                 SELECT MAX (C7051_LAST_UPDATED_DATE)
                   INTO v_c7051_last_updated_date
                   FROM t7051_account_group_pricing T7051
                  WHERE c7051_void_fl           IS NULL
                    AND t7051.c7051_ref_id       = TO_CHAR (v_group_id)
                    AND C7051_LAST_UPDATED_DATE IS NOT NULL
                    AND c7051_active_fl          = 'Y';
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_c7051_last_updated_date := NULL;
            END;
        END IF;
        -- To get Last Updated Date will take in T705 table.
        BEGIN
             SELECT MAX (C705_LAST_UPDATED_DATE)
               INTO v_c705_last_updated_date
               FROM t705_account_pricing t705
              WHERE c205_part_number_id = currindex1.p_num
                AND t705.c705_void_fl  IS NULL;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_c705_last_updated_date := NULL;
        END;
        -- update the last updated date in TDW705_PUF_PARTS_DIM table.
         UPDATE TDW705_PUF_PARTS_DIM
        SET C7051_LAST_UPDATED_DATE = v_c7051_last_updated_date, C705_LAST_UPDATED_DATE = v_c705_last_updated_date
          WHERE c205_part_number_id = currindex1.p_num;
    END LOOP;
END gm_upd_last_updated_date;
---
/**********************************************************************************************************************
* This procedure is to insert the price in TDW7051_ACC_PUFPART_PRICE_FACT and get the parts in TDW705_PUF_PARTS_DIM table
* Author : Velu
* Date    : 12/19/14
**********************************************************************************************************************
*/
PROCEDURE gm_save_acc_part_price
AS
    v_etl_table_update_time DATE;
    CURSOR puf_part_info
    IS
         SELECT C205_PART_NUMBER_ID pnum
           FROM TDW705_PUF_PARTS_DIM TDW705
          WHERE CDW705_ACTIVE_FL                = 'Y'
            AND TDW705.c7051_last_updated_date >= v_etl_table_update_time
            OR TDW705.c705_last_updated_date   >= v_etl_table_update_time;
BEGIN
     SELECT TRUNC (ETL_TABLE_UPDATE_TIME)
       INTO v_etl_table_update_time
       FROM GLOBUS_ETL.ETL_JOB_STATUS
      WHERE ETL_JOB_NAME   = 'GM_Account_Pricing_Job'
        AND ETL_TABLE_NAME = 'ACCOUNT_PRICING_SYNC_TIME';
    FOR puf_part_index    IN puf_part_info
    LOOP
         DELETE
           FROM TDW7051_ACC_PUFPART_PRICE_FACT
          WHERE C205_PART_NUMBER_ID = puf_part_index.pnum;
         INSERT INTO TDW7051_ACC_PUFPART_PRICE_FACT
         SELECT SE7051_PUF_PART_PRICE.NEXTVAL, ACCID, PNUM
          , GPOID, account_dim.sales_rep_id AS sales_rep_id, CASE
                WHEN LAG (ACCPRICE.GPOID, 1) over (order by ACCPRICE.PNUM, ACCPRICE.GPOID) = ACCPRICE.GPOID
                THEN FIRST_VALUE (ACCPRICE.PRICEVAL) over (partition BY ACCPRICE.GPOID, ACCPRICE.PNUM order by
                    ACCPRICE.PRICEVAL DESC)
                ELSE accprice.PRICEval
            END PRICE, SYSDATE
           FROM
            (
                 SELECT C704_ACCOUNT_ID AS accid, C101_GPO_ID AS gpoid, C205_PART_NUMBER_ID AS pnum
                  , CASE
                        WHEN LAG (C101_GPO_ID, 1) over (order by C205_PART_NUMBER_ID, C101_GPO_ID) = C101_GPO_ID
                        THEN - 999
                        ELSE NVL (GET_ACCOUNT_PART_PRICING (C704_ACCOUNT_ID, C205_PART_NUMBER_ID, C101_GPO_ID), 0)
                    END AS priceval
                   FROM MV_ACC_GPO_PART_MAP
                  WHERE C205_PART_NUMBER_ID IN (puf_part_index.pnum)
            )
            accprice, account_dim
          WHERE accprice.accid = account_dim.account_key_id;
         UPDATE TDW705_PUF_PARTS_DIM
        SET CDW705_LAST_UPDATED_DATE = SYSDATE
          WHERE c205_part_number_id  = puf_part_index.pnum;
    END LOOP;
    -- when update the price and it should update sysdate in this columns ETL_TABLE_UPDATE_TIME,ETL_JOB_DATE_TIME.
    -- Because after the data will load from the date
     UPDATE GLOBUS_ETL.ETL_JOB_STATUS
    SET ETL_TABLE_UPDATE_TIME = SYSDATE, ETL_JOB_DATE_TIME = SYSDATE
      WHERE ETL_COUNTRY_ID    = 1101 -- US Country ID
        AND ETL_JOB_NAME      = 'GM_Account_Pricing_Job'
        AND ETL_TABLE_NAME    = 'ACCOUNT_PRICING_SYNC_TIME';
END gm_save_acc_part_price;
END gm_pkg_account_part_pricing;