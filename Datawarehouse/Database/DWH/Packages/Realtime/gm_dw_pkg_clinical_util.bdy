/* Formatted on 2010/09/27 17:55 (Formatter Plus v4.8.0) */
-- @"C:\database\DW_BI\packages\gm_dw_pkg_clinical_util.bdy";

CREATE OR REPLACE PACKAGE BODY globus_bi_realtime.gm_dw_pkg_clinical_util
IS
   FUNCTION get_patient_death_date (
      p_patient_id   t621_patient_master.c621_patient_id%TYPE
   )
      RETURN VARCHAR2
   IS
/*****************************************************************************
 * Description     : This function is used to identify if the patient is still alive
 * Parameters   :p_patient_id
 ****************************************************************************/
      v_patient_death_date   t623_patient_answer_list.c904_answer_ds%TYPE;
--
   BEGIN
      --
      SELECT c904_answer_ds
        INTO v_patient_death_date
        FROM t623_patient_answer_list t623
       WHERE t623.c601_form_id IN (14, 213)
         AND t623.c603_answer_grp_id = 600 -- <B>  Resolution Date </B>  (mm/dd/yyyy) 
         AND t623.c623_delete_fl = 'N'
         AND t623.c621_patient_id IN (
                SELECT c621_patient_id
                  FROM t623_patient_answer_list
                 WHERE c621_patient_id = p_patient_id
                   AND c601_form_id IN (14, 213)
                   AND c605_answer_list_id IN (271, 248) -- Death
                   AND c623_delete_fl = 'N'
                   );

      --
      RETURN v_patient_death_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         --
         RETURN '';
   --
   END get_patient_death_date;
END gm_dw_pkg_clinical_util;
/
