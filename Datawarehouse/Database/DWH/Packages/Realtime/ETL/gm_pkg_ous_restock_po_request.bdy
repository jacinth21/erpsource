/* Formatted on 2013/07/12 14:41 (Formatter Plus v4.8.8) */
-- @"C:\Database\SQL\ETL\OUS_Restock_PO\201306\gm_pkg_ous_restock_po_request.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_ous_restock_po_request
IS
--
    PROCEDURE gm_sav_restock_po_request (
        p_request_id          IN   t520_request.c520_request_id%TYPE
      , p_po_id               IN   t520_request.c520_request_id%TYPE
      , p_required_date       IN   DATE
      , p_request_source      IN   t520_request.c901_request_source%TYPE
      , p_request_txn_id      IN   t520_request.c520_request_txn_id%TYPE
      , p_request_for         IN   t520_request.c520_request_for%TYPE
      , p_request_to          IN   t520_request.c520_request_to%TYPE
      , p_request_by_type     IN   t520_request.c901_request_by_type%TYPE
      , p_request_by          IN   t520_request.c520_request_by%TYPE
      , p_ship_to             IN   t520_request.c901_ship_to%TYPE
      , p_ship_to_id          IN   t520_request.c520_ship_to_id%TYPE
      , p_master_request_id   IN   t520_request.c520_master_request_id%TYPE
      , p_status_fl           IN   t520_request.c520_status_fl%TYPE
      , p_userid              IN   t520_request.c520_created_by%TYPE
      , p_inputstring         IN   VARCHAR2
      , p_purpose             IN   t520_request.c901_purpose%TYPE
      , p_inpstr              IN   VARCHAR2
      , p_planshipdate        IN   t520_request.c520_planned_ship_date%TYPE
      , p_job_run_id          IN   NUMBER
    )
    AS
        v_out_request_id t520_request.c520_request_id%TYPE;
        v_out_consignment_id t504_consignment.c504_consignment_id%TYPE;
        v_etl_user_id  t520_request.c520_created_by%TYPE := '649291';
    BEGIN
-- Initiate RQ / CN
        gm_pkg_op_process_prod_request.gm_sav_initiate_request (p_request_id
                                                              , p_required_date
                                                              , p_request_source
                                                              , p_request_txn_id
                                                              , NULL   -- set_id
                                                              , p_request_for
                                                              , p_request_to
                                                              , p_request_by_type
                                                              , v_etl_user_id
                                                              , p_ship_to
                                                              , p_ship_to_id
                                                              , p_master_request_id
                                                              , p_status_fl
                                                              , p_userid
                                                              , p_inputstring
                                                              , p_purpose
                                                              , NULL   -- p_rep_id
                                                              , NULL   -- p_acc_id
                                                              , p_inpstr
                                                              , p_planshipdate
							      , NULL
                                                              , v_out_request_id
                                                              , v_out_consignment_id
							      , NULL
                                                               );
-- create shipping records for RQ and CN
        gm_pkg_ous_restock_po_request.gm_etl_op_sav_rq_cn_ship_info (v_out_request_id, v_out_consignment_id, p_request_to);

        UPDATE te520_ous_restock_po_request
           SET ce520_rq_processed_fl = 'Y'
             , ce520_rq_processed_date = SYSDATE
         WHERE c401_purchase_ord_id = p_po_id;

-- 50184 - request
-- 50181 - CN
        INSERT INTO te520a_ous_restock_po_rq_map
                    (ce520a_ous_po_rq_map_id, c401_ous_purchase_ord_id, c901_txn_type, ce520a_txn_id, ce520a_load_date
                   , ce520a_job_run_id)
            SELECT sce_5020_sales_job_run_id.NEXTVAL, p_po_id, 50184, t520.c520_request_id, SYSDATE, p_job_run_id
              FROM t520_request t520
             WHERE c901_request_source = 4000121 AND t520.c520_master_request_id = v_out_request_id
                OR t520.c520_request_id = v_out_request_id;

        INSERT INTO te520a_ous_restock_po_rq_map (ce520a_ous_po_rq_map_id,c401_ous_purchase_ord_id,c901_txn_type,ce520a_txn_id,ce520a_load_date,ce520a_job_run_id)
             VALUES (sce_5020_sales_job_run_id.NEXTVAL, p_po_id, 50181, v_out_consignment_id, SYSDATE, p_job_run_id);
    END gm_sav_restock_po_request;

/****************************************************************************************************************************************
 * This procedure is to dealloc given qty (p_qty) for the given part (p_part_number_id) in all backorder RQ first and then WIP CN.
 * In WIP CN, first dealloc from FG and then from RW
 *
*****************************************************************************************************************************************/
-- Needs more documentation on the deallocation logic and may be some way of automated testing
-- whoever is going to read this code, plz refer to the xls in the same folder in SVN. It has step-by-step data flow logic of the deallocation logic
    PROCEDURE gm_etl_op_dealloc_rq_cn_qty (
        p_ous_distributor_id   IN   t520_request.c520_request_to%TYPE
      , p_ext_country_id       IN   te520a_ous_restock_po_upd_void.c901_ext_country_id%TYPE
      , p_part_number_id       IN   te520a_ous_restock_po_upd_void.c205_part_number_id%TYPE
      , p_qty                  IN   te520a_ous_restock_po_upd_void.c214_trans_qty%TYPE
      , p_job_run_id           IN   NUMBER
    )
    AS
        -- Fetch all open WO for that part and sort by latest and then do the dealloc
        CURSOR cur_backorder_rq
        IS
            SELECT   t521.c520_request_id request_id, t521.c205_part_number_id pnum, t521.c521_qty qty
                FROM t520_request t520, t521_request_detail t521
               WHERE t520.c520_request_id = t521.c520_request_id
                 AND t520.c901_request_source = 4000121   -- ETL sales replenishment
                 AND t520.c520_status_fl = 10   -- 10 is backorder
                 AND t521.c521_qty > 0
                 AND t520.c520_void_fl IS NULL
                 AND t520.c520_request_to = p_ous_distributor_id   -- 375 is UK
                 AND t521.c205_part_number_id = p_part_number_id
            ORDER BY t520.c520_request_date desc;

        CURSOR cur_wip_cn
        IS
            SELECT   t505.c504_consignment_id consignment_id, t505.c205_part_number_id pnum, t505.c505_item_qty qty
                   , c901_warehouse_type warehouse_type, t505.c505_control_number control_num
                   , t505.c505_item_consignment_id item_cn_id
                FROM t520_request t520, t504_consignment t504, t505_item_consignment t505
               WHERE t520.c520_request_id = t504.c520_request_id
                 AND t504.c504_consignment_id = t505.c504_consignment_id
                 AND t504.c504_status_fl < 3   -- initiated. 1 is WIP and we shouldnt modify anything after 1.
                 AND t520.c901_request_source = 4000121   -- ETL sales replenishment
                 AND t520.c520_void_fl IS NULL
                 AND t504.c504_void_fl IS NULL
                 AND t505.c505_void_fl IS NULL
                 AND t520.c520_request_to = p_ous_distributor_id
                 AND t505.c205_part_number_id = p_part_number_id
                 AND t505.c505_item_qty > 0
                 AND NVL (t505.c505_control_number, 'TBE') =
                                        'TBE'   -- If a pick happens, then it'll have control # even though the status is 2
            ORDER BY c901_warehouse_type DESC NULLS LAST, t504.c504_created_date desc;   -- so we first dealloc FG (90800) from ALL CN and then look at 56001 (Restricted WH) of each CN

        v_txn_id       t520_request.c520_request_id%TYPE;
        v_negative_fl  BOOLEAN := FALSE;
        v_new_qty      t521_request_detail.c521_qty%TYPE;
        v_original_qty t521_request_detail.c521_qty%TYPE;
        v_input_qty    t521_request_detail.c521_qty%TYPE;
        v_user_id      t520_request.c520_last_updated_by%TYPE := '649291';
        v_message      VARCHAR2 (4000);
        v_item_cn_id   t505_item_consignment.c505_item_consignment_id%TYPE;
    BEGIN
        v_input_qty := p_qty;   -- number should already come as positive.

        FOR backorder_rq IN cur_backorder_rq
        LOOP
            v_new_qty   := backorder_rq.qty;
            v_original_qty := backorder_rq.qty;   -- just for logging purpose
            v_txn_id    := backorder_rq.request_id;

            IF v_input_qty - v_new_qty < 0
            THEN
                v_new_qty   := v_input_qty;
            END IF;

            v_input_qty := v_input_qty - v_new_qty;

            IF v_negative_fl = FALSE
            THEN
                UPDATE t521_request_detail
                   SET c521_qty = c521_qty - v_new_qty
                 WHERE c520_request_id = v_txn_id AND c205_part_number_id = p_part_number_id;

                UPDATE t520_request
                   SET c520_last_updated_by = v_user_id
                     , c520_last_updated_date = SYSDATE
                 WHERE c520_request_id = v_txn_id;

                -- add comments 1235
                gm_save_log (v_txn_id ,    'Request qty for ' || p_part_number_id || ' reduced from original qty of ' || v_original_qty || ' by '|| v_new_qty|| ' to make it '|| (v_original_qty - v_new_qty) || ' due to an update / void happened in OUS '
			   , 1235
                           , v_user_id
                           , NULL
                           , v_message
                            );
            END IF;

            IF v_input_qty <= 0
            THEN
                v_negative_fl := TRUE;
            END IF;
        END LOOP;

        FOR wip_cn IN cur_wip_cn
        LOOP
            v_new_qty   := wip_cn.qty;
            v_item_cn_id := wip_cn.item_cn_id;
            v_original_qty := wip_cn.qty;   -- just for logging purpose
            v_txn_id    := wip_cn.consignment_id;

            IF v_input_qty - v_new_qty < 0
            THEN
                v_new_qty   := v_input_qty;
            END IF;

            v_input_qty := v_input_qty - v_new_qty;

            IF v_negative_fl = FALSE
            THEN
                UPDATE t505_item_consignment
                   SET c505_item_qty = c505_item_qty - v_new_qty
                 WHERE c505_item_consignment_id = v_item_cn_id;

                UPDATE t504_consignment
                   SET c504_last_updated_by = v_user_id
                     , c504_last_updated_date = SYSDATE
                 --, c504_comments = 'Consignment qty reduced from original qty of ' || v_original_qty|| ' qty by '||v_new_qty|| ' due to an update / void happened in OUS '
                WHERE  c504_consignment_id = v_txn_id;

                -- add comments 1235
                gm_save_log (v_txn_id
                           ,    'Consignment qty for '|| p_part_number_id|| ' reduced from original qty of '|| v_original_qty|| ' by ' || v_new_qty || ' to make it '|| (v_original_qty - v_new_qty)|| ' due to an update / void happened in OUS '
			   , 1236   -- consignment comments
                           , v_user_id
                           , NULL
                           , v_message
                            );
            END IF;

            IF v_input_qty <= 0
            THEN
                v_negative_fl := TRUE;
            END IF;
        END LOOP;

	gm_pkg_ous_restock_po_request.gm_etl_op_po_rq_upd_info( p_ous_distributor_id
	      , p_ext_country_id
	      , p_part_number_id
	      , p_job_run_id
	 );

        -- Update the status etc of the modified RQ and CN
        gm_pkg_ous_restock_po_request.gm_etl_op_upd_rq_cn_status (p_ous_distributor_id, p_part_number_id);

	-- void the restock CN and RQ if all their details are voided
	gm_pkg_ous_restock_po_request.gm_void_ous_restock_rq_cn (p_ous_distributor_id);

        -- Finally update the table to specify that the processing is complete so that ETL will skip these records in the next run
        UPDATE te520a_ous_restock_po_upd_void
           SET ce520a_etl_processed_fl = 'Y'
             , ce520a_etl_processed_date = SYSDATE
         WHERE c205_part_number_id = p_part_number_id
           AND c901_ext_country_id = p_ext_country_id
           AND ce520a_etl_processed_fl IS NULL;
    END gm_etl_op_dealloc_rq_cn_qty;

/****************************************************************************************************************************************
 * This procedure will update the RQ and CN item status after all modifications happen to them due to returns et al.
 *
*****************************************************************************************************************************************/
    PROCEDURE gm_etl_op_upd_rq_cn_status (
        p_ous_distributor_id   IN   t520_request.c520_request_to%TYPE
      , p_part_number_id       IN   te520a_ous_restock_po_upd_void.c205_part_number_id%TYPE
    )
    AS
    BEGIN
        DELETE FROM t521_request_detail
              WHERE c521_request_detail_id IN (
                        SELECT c521_request_detail_id
                          FROM t520_request t520, t521_request_detail t521
                         WHERE t520.c520_request_id = t521.c520_request_id
                           AND t520.c901_request_source = 4000121
                           --AND t520.c520_status_fl = 10
                           AND t520.c520_request_to = p_ous_distributor_id   -- '375' is UK
                           AND t520.c520_void_fl IS NULL
                           AND t521.c521_qty = 0
                           AND t521.c205_part_number_id = p_part_number_id);

        UPDATE t505_item_consignment
           SET c505_void_fl = 'Y'
         WHERE c505_item_consignment_id IN (
                   SELECT t505.c505_item_consignment_id item_cn_id
                     FROM t520_request t520, t504_consignment t504, t505_item_consignment t505
                    WHERE t520.c520_request_id = t504.c520_request_id
                      AND t504.c504_consignment_id = t505.c504_consignment_id
                      AND t504.c504_status_fl < 3   -- inititaed. 1 is WIP and we shouldnt modify anything after 1.
                      AND t520.c901_request_source = 4000121
                      AND t520.c520_void_fl IS NULL
                      AND t504.c504_void_fl IS NULL
                      AND t520.c520_request_to = p_ous_distributor_id
                      AND NVL (t505.c505_control_number, 'TBE') = 'TBE'   -- If a pick happens, then it'll have control # even though the status is 2
                      AND c205_part_number_id = p_part_number_id
                      AND t505.c505_item_qty = 0); -- add void fl is null
    END gm_etl_op_upd_rq_cn_status;

/****************************************************************************************************************************************
 * This procedure will update TE520B_PO_RQ_UPD_QTY_INFO
 *
*****************************************************************************************************************************************/
PROCEDURE gm_etl_op_po_rq_upd_info (
    p_ous_distributor_id   IN   t520_request.c520_request_to%TYPE
  , p_ext_country_id       IN   te520a_ous_restock_po_upd_void.c901_ext_country_id%TYPE
  , p_part_number_id       IN   te520a_ous_restock_po_upd_void.c205_part_number_id%TYPE
  , p_job_run_id           IN   NUMBER
)
AS
BEGIN
    INSERT INTO te520b_po_rq_upd_qty_info
                (ce520b_po_rq_upd_qty_info_id, c901_ext_country_id, c401_ous_purchase_ord_id, c205_part_number_id
               , c520_request_id, ce520b_total_rq_qty, ce520b_etl_load_date, ce520b_etl_job_run_id)
        SELECT sce520b_po_rq_upd_qty_info_id.NEXTVAL, p_ext_country_id
             , gm_pkg_op_request_master.get_request_attribute (request_id, 1006420) -- PO Id attribute type
	     , p_part_number_id, request_id, qty
             , SYSDATE, p_job_run_id
          FROM (
	  SELECT request_id, sum(qty) qty
	  FROM (
	  SELECT   t520.c520_master_request_id request_id, SUM (t521.c521_qty) qty
                    FROM t520_request t520, t521_request_detail t521
                   WHERE t520.c520_request_id = t521.c520_request_id
                     AND t520.c901_request_source = 4000121 -- ETL sales replenishment
                     AND t520.c520_status_fl = 10 -- backorder
                     AND t520.c520_request_to = p_ous_distributor_id   -- '375' -- is UK
                     AND t520.c520_void_fl IS NULL
                     AND t521.c205_part_number_id = p_part_number_id   -- '124.000'
                GROUP BY t520.c520_master_request_id   -- If there are multiple Backorder, then total the qty
                UNION ALL
                SELECT   t520.c520_request_id request_id, SUM (t505.c505_item_qty) qty
                    FROM t520_request t520, t504_consignment t504, t505_item_consignment t505
                   WHERE t520.c520_request_id = t504.c520_request_id
                     AND t504.c504_consignment_id = t505.c504_consignment_id
                     AND t504.c504_status_fl < 4   -- Not shipped
                     AND t504.c504_ext_load_date IS NULL   -- Not loaded in OUS
                     AND t520.c901_request_source = 4000121   -- ETL sales replenishment
                     AND t520.c520_void_fl IS NULL
                     AND t504.c504_void_fl IS NULL
                     AND t505.c505_void_fl IS NULL
                     AND t520.c520_request_to = p_ous_distributor_id   -- '375'
                     AND t505.c205_part_number_id = p_part_number_id   --'124.000'
                GROUP BY t520.c520_request_id)
	GROUP BY request_id );
END gm_etl_op_po_rq_upd_info;


/****************************************************************************************************************************************
 * This procedure will create shipping records for the RQ and CN that have been created
 *
*****************************************************************************************************************************************/
    PROCEDURE gm_etl_op_sav_rq_cn_ship_info (
        p_request_id       IN   t520_request.c520_request_id%TYPE
      , p_consignment_id   IN   t504_consignment.c504_consignment_id%TYPE
      , p_distributor_id   IN   t520_request.c520_request_to%TYPE
    )
    AS
        v_source_as_request t907_shipping_info.c901_source%TYPE := 50184;   -- request
        v_source_as_consignment t907_shipping_info.c901_source%TYPE := 50181;   -- consignment
        v_shipto       t907_shipping_info.c901_ship_to%TYPE := 4120;   -- distributor
        v_shipcarr     t907_shipping_info.c901_delivery_carrier%TYPE := 5001;   -- fedex
        v_shipmode     t907_shipping_info.c901_delivery_mode%TYPE := 5004;   -- priority overnight
        v_etl_user_id  t520_request.c520_created_by%TYPE := '649291';
        v_ship_id      VARCHAR2 (20) := NULL;

        CURSOR cur_request_list
        IS
            SELECT t520.c520_request_id request_id
              FROM t520_request t520
             WHERE c901_request_source = 4000121 AND t520.c520_master_request_id = p_request_id
                OR t520.c520_request_id = p_request_id;
    BEGIN
        FOR request_list IN cur_request_list
        LOOP
-- Shipping record for RQ
            gm_pkg_cm_shipping_trans.gm_sav_shipping (request_list.request_id
                                                    , v_source_as_request   -- p_source : request
                                                    , v_shipto   -- p_shipto :distributor
                                                    , p_distributor_id   -- p_shiptoid : distributor id
                                                    , v_shipcarr   -- p_shipcarr : fedex
                                                    , v_shipmode   -- p_shipmode : priority overnight
                                                    , NULL   -- p_trackno
                                                    , 0   -- p_addressid
                                                    , NULL   -- p_freightamt
                                                    , v_etl_user_id   -- p_userid
                                                    , v_ship_id   -- p_shipid
                                                     );
        END LOOP;

-- Shipping record for CN, if CN was created
/*        IF p_consignment_id IS NOT NULL
        THEN
            gm_pkg_cm_shipping_trans.gm_sav_shipping (p_consignment_id
                                                    , v_source_as_consignment   -- p_source : request
                                                    , v_shipto   -- p_shipto :distributor
                                                    , p_distributor_id   -- p_shiptoid : distributor id
                                                    , v_shipcarr   -- p_shipcarr : fedex
                                                    , v_shipmode   -- p_shipmode : priority overnight
                                                    , NULL   -- p_trackno
                                                    , 0   -- p_addressid
                                                    , NULL   -- p_freightamt
                                                    , v_etl_user_id   -- p_userid
                                                    , v_ship_id   -- p_shipid
                                                     );
        END IF; */
    END gm_etl_op_sav_rq_cn_ship_info;

/****************************************************************************************
 * Purpose: To void all the request and consignment which are voided because of returns happening in OUS
 ***************************************************************************************/
/* Formatted on 2013/08/13 15:58 (Formatter Plus v4.8.8) */
PROCEDURE gm_void_ous_restock_rq_cn (
    p_ous_distributor_id   IN   t520_request.c520_request_to%TYPE
)
AS
    v_rq_cancelreason t907_cancel_log.c901_cancel_cd%TYPE := 90754;   -- Others
    v_cn_cancelreason t907_cancel_log.c901_cancel_cd%TYPE := 90239;
    v_comments     t907_cancel_log.c907_comments%TYPE := 'ETL Restock transaction voided';
    v_userid       t102_user_login.c102_user_login_id%TYPE := '649291';
    v_master_request_id t520_request.c520_request_id%TYPE;

    -- Request to be voided
    CURSOR cur_backorder_request_info
    IS
        SELECT   t520.c520_request_id requestid, COUNT (t521.c521_request_detail_id) details_cnt
            FROM t520_request t520, t521_request_detail t521
           WHERE c901_request_source = 4000121
             AND t520.c520_request_id = t521.c520_request_id(+)
             AND t521.c520_request_id IS NULL   -- selects only RQ that are in t520
             AND t520.c520_status_fl = 10   -- 10 : Backorder
             AND t520.c520_request_to =
                          p_ous_distributor_id   -- since itz for 4000121, request_to will always be the ous distributor id
             AND t520.c520_void_fl IS NULL
        GROUP BY t520.c520_request_id;

    -- Consignment to be voided
    CURSOR cur_consign_info
    IS
        SELECT   t520.c520_request_id requestid, t504.c504_consignment_id consignid
               , COUNT (t505.c504_consignment_id) details_cnt
            FROM t504_consignment t504
               , (SELECT c504_consignment_id, c505_control_number
                    FROM t505_item_consignment
                   WHERE c505_void_fl IS NULL) t505
               , t520_request t520
           WHERE t505.c504_consignment_id(+) = t504.c504_consignment_id
             AND t520.c520_request_id = t504.c520_request_id
             AND t504.c504_status_fl < 3   -- inititaed. 1 is WIP and we shouldnt modify anything after 1.
             AND t520.c901_request_source = 4000121
             AND t520.c520_void_fl IS NULL
             AND t504.c504_void_fl IS NULL
             AND NVL (t505.c505_control_number, 'TBE') = 'TBE'
             AND t505.c504_consignment_id IS NULL
             AND t504.c701_distributor_id = p_ous_distributor_id
        GROUP BY t520.c520_request_id, t504.c504_consignment_id;

    -- dont need distributor filter since we are querying by request itself
    CURSOR cur_master_request_info
    IS
        SELECT DISTINCT mainn.reqid requestid, NVL (cn.cncnt, 0) + NVL (req.rqcnt, 0) totalcnt
                   FROM (SELECT c520_request_id reqid
                           FROM t520_request t520
                          WHERE c520_request_id = v_master_request_id
                            AND c520_void_fl IS NULL   -- so we dont process already voided request
                                                    ) mainn
                      , (SELECT   c520_request_id reqid, SUM (t505.c505_item_qty) cncnt
                             FROM t504_consignment t504, t505_item_consignment t505
                            WHERE t505.c504_consignment_id = t504.c504_consignment_id
                              AND c520_request_id = v_master_request_id
                              AND c504_void_fl IS NULL
                         GROUP BY c520_request_id) cn   -- Get the total item qty of child CN's that are not voided
                      , (SELECT   c520_master_request_id reqid, SUM (t521.c521_qty) rqcnt
                             FROM t520_request t520, t521_request_detail t521
                            WHERE t521.c520_request_id = t520.c520_request_id
                              AND (   t520.c520_master_request_id = v_master_request_id
                                   OR t520.c520_request_id = v_master_request_id
                                  )
                              AND t520.c520_void_fl IS NULL
                         GROUP BY c520_master_request_id) req   -- Get the total item qty of child master / child RQ's that are not voided
                  WHERE mainn.reqid = cn.reqid(+) AND mainn.reqid = req.reqid(+)
                        AND NVL (cn.cncnt, 0) + NVL (req.rqcnt, 0) = 0;   -- Qty Sum of non-voided RQ / CN should be 0
BEGIN
    FOR backorder_request_info IN cur_backorder_request_info
    LOOP
        gm_pkg_common_cancel.gm_cm_sav_cancelrow (backorder_request_info.requestid
                                                , v_rq_cancelreason
                                                , 'VDREQ'
                                                , v_comments
                                                , v_userid
                                                 );
    END LOOP;

    FOR consign_info IN cur_consign_info
    LOOP
        gm_pkg_common_cancel.gm_cm_sav_cancelrow (consign_info.consignid
                                                , v_cn_cancelreason
                                                , 'VODCN'
                                                , v_comments
                                                , v_userid
                                                 );
        v_master_request_id := consign_info.requestid;

        FOR master_request_info IN cur_master_request_info
        LOOP
            gm_pkg_common_cancel.gm_cm_sav_cancelrow (master_request_info.requestid
                                                    , v_rq_cancelreason
                                                    , 'VDREQ'
                                                    , v_comments
                                                    , v_userid
                                                     );
        END LOOP;
    END LOOP;
END gm_void_ous_restock_rq_cn;


END gm_pkg_ous_restock_po_request;
/