/* Formatted on 2013/08/13 15:58 (Formatter Plus v4.8.8) */
-- @"C:\Database\SQL\ETL\OUS_Restock_PO\201306\gm_pkg_intl_auto_sales_restock.bdy"
-- # call below proc to create WO
-- exec gm_pkg_intl_auto_sales_restock.gm_etl_op_process_restock_po

create or replace PACKAGE BODY gm_pkg_intl_auto_sales_restock
IS
--
 /*******************************************************
   * Description : To call existing procedure to update t205c_part_qty  through ETL
   *******************************************************/
--
    PROCEDURE gm_etl_cm_sav_partqty (
        p_part_number_id           IN   t205c_part_qty.c205_part_number_id%TYPE
      , p_update_qty               IN   t205c_part_qty.c205_available_qty%TYPE
      , p_trans_id                 IN   t205c_part_qty.c205_last_update_trans_id%TYPE
      , p_updated_by               IN   t205c_part_qty.c205_last_updated_by%TYPE
      , p_transaction_type         IN   t205c_part_qty.c901_transaction_type%TYPE
      , p_action                   IN   t205c_part_qty.c901_action%TYPE
      , p_type                     IN   t205c_part_qty.c901_type%TYPE
      , p_order_replenishment_id   IN   te501_order_replenishment_part.ce501_order_replenishment_id%TYPE
      , p_company_id		   IN   t1900_company.c1900_company_id%TYPE
    )
    AS
    
    BEGIN
       
        gm_cm_sav_partrepl_qty (p_part_number_id
                         , p_update_qty
                         , p_trans_id
                         , p_updated_by
                         , p_transaction_type
                         , p_action
                         , p_type
                         , p_company_id
                         );

        UPDATE te501_order_replenishment_part
           SET ce501_part_qty_update_fl = 'Y'
             , ce501_part_qty_update_date = SYSDATE
         WHERE ce501_order_replenishment_id = p_order_replenishment_id
	 AND c1900_company_id=p_company_id;
    END gm_etl_cm_sav_partqty;

/****************************************************************************************************************************************
 * This is the main procedure called from ETL
 *
*****************************************************************************************************************************************/
    PROCEDURE gm_etl_op_process_restock_po(
	 p_company_id		   IN   t1900_company.c1900_company_id%TYPE
    ,p_vendor_id            IN   t301_vendor.C301_VENDOR_ID%TYPE
    )
    AS
        v_us_time_zone t901_code_lookup.c901_code_nm%TYPE;
        v_us_time_format t901_code_lookup.c901_code_nm%TYPE;
        
    BEGIN
    
    SELECT GET_CODE_NAME(C901_TIMEZONE), GET_CODE_NAME(C901_DATE_FORMAT)
            INTO v_us_time_zone,v_us_time_format
        FROM t1900_company
        WHERE C1900_COMPANY_ID=p_company_id;
            
                     
            my_context.set_app_double_inlist_ctx('COMPANY_ID',p_company_id);
            my_context.set_app_double_inlist_ctx('COMP_TIME_ZONE',v_us_time_zone);
	        my_context.set_app_double_inlist_ctx('COMP_DATE_FMT',v_us_time_format);
        -- cleanup update void table for new information
        DELETE FROM te402_po_to_rq_update_void;

        gm_etl_op_new_wo_from_po(p_company_id,p_vendor_id);
        gm_etl_op_upd_wo_of_po(p_company_id);
        -- void is handled from ETL
        gm_etl_op_upd_po_tot_cost(p_company_id);   -- updates the total cost of PO
    END gm_etl_op_process_restock_po;

/****************************************************************************************************************************************
 * This procedure is to create new WO.
 *
*****************************************************************************************************************************************/
    PROCEDURE gm_etl_op_new_wo_from_po(
	 p_company_id		   IN   t1900_company.c1900_company_id%TYPE
    ,p_vendor_id            IN   t301_vendor.C301_VENDOR_ID%TYPE
    )
    AS
        v_emailids     VARCHAR2 (4000);
        v_wo_count     NUMBER := 0;
        v_po_id        t401_purchase_order.c401_purchase_ord_id%TYPE;

-- Cursor to fetch all PO's to process
        CURSOR cur_po
        IS
            SELECT DISTINCT te501.c401_purchase_ord_id poid, c1900_part_company_id source_company
                       FROM te501_order_replenishment_part te501
                      WHERE te501.ce501_should_need_po = 'Y'
                        AND ce501_ous_processed_fl IS NULL   -- so that if job runs after partial failure, duplicate WO is not created
			AND c1900_company_id=p_company_id
                   ORDER BY te501.c401_purchase_ord_id;
    BEGIN
-- Loop through the PO's and create WO's
        BEGIN
            SELECT get_rule_value ('102543', 'ALERT_NOTIFICATION')
              INTO v_emailids
              FROM DUAL;
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                v_emailids  := '';
        END;

        FOR poinfo IN cur_po
        LOOP
            -- create restock WO
            v_po_id     := poinfo.poid;
            -- to update the source company id
             UPDATE t401_purchase_order t401
			SET t401.c1900_source_company_id  = poinfo.source_company
			  WHERE t401.c401_purchase_ord_id = v_po_id;
			--  
            gm_pkg_intl_auto_sales_restock.gm_etl_op_process_restock_wo (v_po_id, p_company_id,p_vendor_id,v_wo_count);

            -- maybe log the PO and count

            -- handle alert notification
            -- if suppose the PO has all negative qty parts, then no WO will be created adn so no notification is needed
            IF v_wo_count > 0
            THEN
                gm_etl_op_new_po_notify (poinfo.poid, v_emailids);
            ELSE
                -- since there are no WO's being created, clean up the PO
                DELETE FROM t401_purchase_order
                      WHERE c401_purchase_ord_id = v_po_id
		      AND c1900_company_id=p_company_id;
            /* Dont need this since we are already making them as null in gm_etl_op_process_restock_wo
                    UPDATE te501_order_replenishment_part
                       SET c401_purchase_ord_id = NULL
                         , ce501_po_created_date = NULL
                     WHERE c401_purchase_ord_id = v_po_id
		     AND C1900_COMPANY_ID=p_company_id;
            */
            END IF;
        END LOOP;
    END gm_etl_op_new_wo_from_po;

/****************************************************************************************************************************************
 * This procedure is to update existing WO of return orders (marked by ce501_should_need_po = 'N')
 *
*****************************************************************************************************************************************/
    PROCEDURE gm_etl_op_upd_wo_of_po(
	p_company_id		   IN   t1900_company.c1900_company_id%TYPE
    )
    AS
        CURSOR cur_wo_upd_details
        IS
            SELECT   te501.c205_part_number_id pnum, SUM (te501.c502_item_qty) qty
                FROM te501_order_replenishment_part te501
               WHERE te501.ce501_should_need_po = 'N'   -- so we only take sales adjustment and credit orders for updation
                 AND te501.ce501_ous_processed_fl IS NULL   -- so we know this record hasnt been processed yet
		 AND c1900_company_id=p_company_id
            GROUP BY te501.c205_part_number_id;

        CURSOR cur_no_open_wo
        IS
            SELECT ce501_order_replenishment_id key_id, te501.c205_part_number_id pnum, te501.c502_item_qty qty
                 , DECODE (te501.c901_ext_country_id, 1148, te501.c501_order_id, te501.c501_ext_ref_id) order_id   -- except EDC, the orderid should be the native order id (ext_ref_if)
              FROM te501_order_replenishment_part te501
                 , (SELECT DISTINCT t402.c205_part_number_id pnum
                               FROM t402_work_order t402, t401_purchase_order t401
                              WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
                                AND t402.c402_status_fl < 3   -- only open WO
                                AND t402.c402_void_fl IS NULL
                                AND t401.c401_type = 4000095   -- restock PO
				AND t402.c1900_company_id=p_company_id
				AND t401.c1900_company_id=p_company_id
                                                            ) wo_info
             WHERE te501.c205_part_number_id = wo_info.pnum(+)
               AND te501.ce501_should_need_po = 'N'
               AND te501.ce501_ous_processed_fl IS NULL
               AND wo_info.pnum IS NULL   -- outer join condition to choose parts that dont have entry for open WO
	       AND te501.c1900_company_id=p_company_id;
    BEGIN
        FOR woinfo IN cur_wo_upd_details
        LOOP
            -- even if we pass all parts, those that dont have open WO wont be dealloc'ed
           gm_etl_op_dealloc_wo_qty (woinfo.pnum, woinfo.qty,p_company_id);
        END LOOP;

        FOR no_open_wo IN cur_no_open_wo
        LOOP
            -- these parts would have already added negative balance in part_replenish_log_DF. so just mark them as processed and operation as nop
            UPDATE te501_order_replenishment_part
               SET ce501_ous_processed_fl = 'Y'
                 , ce501_ous_processed_date = SYSDATE
                 , ce501_wo_operation = 'nop_no_open_wo'   -- no operation.
             WHERE ce501_order_replenishment_id = no_open_wo.key_id AND ce501_ous_processed_fl IS NULL AND c1900_company_id=p_company_id;
        END LOOP;
    END gm_etl_op_upd_wo_of_po;

/****************************************************************************************************************************************
 * This procedure is to dealloc given qty (p_qty) for the given part (p_part_number_id) in all open WO
 *
*****************************************************************************************************************************************/
-- Needs more documentation on the deallocation logic and may be some way of automated testing
    PROCEDURE gm_etl_op_dealloc_wo_qty (
        p_part_number_id   IN   t205c_part_qty.c205_part_number_id%TYPE   -- change this to rite column
      , p_qty              IN   t205c_part_qty.c205_available_qty%TYPE   -- change this to rite column
	  , p_company_id		   IN   t1900_company.c1900_company_id%TYPE
    )
    AS
          
            -- Fetch all open WO for that part and sort by latest and then do the dealloc
        CURSOR cur_wos_for_part
        IS
            SELECT   t402.c205_part_number_id pnum, t402.c402_work_order_id work_order_id
                                                                                         --, GET_WO_PEND_QTY(t402.c402_work_order_id,t402.c402_qty_ordered) qty
                     , t402.c402_qty_ordered qty, t402.c401_purchase_ord_id po_id
                FROM t402_work_order t402, t401_purchase_order t401
               WHERE t402.c401_purchase_ord_id = t401.c401_purchase_ord_id
                 AND t401.c401_type = 4000095
                 AND t402.c402_status_fl < 3
                 AND t402.c402_void_fl IS NULL
                 AND t402.c205_part_number_id = p_part_number_id
                 AND t402.c402_qty_ordered > 0
		 AND t402.c1900_company_id=p_company_id
		 AND t401.c1900_company_id=p_company_id
            ORDER BY t402.c402_created_date DESC;

        v_work_order_id t402_work_order.c402_work_order_id%TYPE;
        v_negative_fl  BOOLEAN := FALSE;
        v_new_qty      t205c_part_qty.c205_available_qty%TYPE;
        v_input_qty    t205c_part_qty.c205_available_qty%TYPE;
        v_user_id      t402_work_order.c402_last_updated_by%TYPE := '649291';
        v_po_id        t401_purchase_order.c401_purchase_ord_id%TYPE;
        
             
    BEGIN
        v_input_qty := p_qty * -1;   -- converting to positive number for the below logic
            
        FOR wos_info IN cur_wos_for_part
        LOOP
            v_new_qty   := wos_info.qty;
            v_work_order_id := wos_info.work_order_id;
            v_po_id     := wos_info.po_id;

            IF v_input_qty - v_new_qty < 0
            THEN
                v_new_qty   := v_input_qty;
            END IF;

            v_input_qty := v_input_qty - v_new_qty;

            IF v_negative_fl = FALSE
            THEN
                UPDATE t402_work_order
                   SET c402_qty_ordered = c402_qty_ordered - v_new_qty
                     , c402_last_updated_by = v_user_id
                     , c402_last_updated_date = CURRENT_DATE
                     --, c402_status_fl = DECODE( GET_WO_PEND_QTY(c402_work_order_id,c402_qty_ordered), v_new_qty,3, c402_status_fl) -- If qty becomes 0 ie c402_qty_ordered == v_new_qty, then close the WO
                ,      c402_status_fl =
                           DECODE
                               (c402_qty_ordered
                              , v_new_qty, 3
                              , c402_status_fl
                               )   -- If qty becomes 0 ie c402_qty_ordered == v_new_qty, then close the WO
                 WHERE c402_work_order_id = v_work_order_id
		 AND c1900_company_id=p_company_id;
     
                /*
                we send a positive value since the -ve qty was already posted by the Return order.
                We dont want these returns to impact the c205c_available_qty since if the shipment happens, the overrage will be posted as negative balance.
                */
                gm_cm_sav_partrepl_qty(p_part_number_id
                                 , v_new_qty   -- will be a positive value
                                 , v_work_order_id
                                 , v_user_id
                                 , 4000100   -- Sales replenishment
                                 , 4301   -- plus
                                 , 4319   -- Work order
                                 , p_company_id) ;
                   -- save this update info in TE402_PO_TO_RQ_UPDATE_VOID so that it can be processed further in US
                gm_pkg_intl_auto_sales_restock.gm_etl_sav_wo_upd_qty (v_po_id
                                                               , p_part_number_id
                                                               , 'update'
                                                               , v_new_qty
                                                               , v_work_order_id
                                                               , NULL
															   , p_company_id
                                                                );

                -- If this WO wasnt processed (inserted as RQ/CN) in US, then mark it as upsert. These records have to be used to create RQ / CN in US.
                -- If this WO was processed in US, then mark it as update. With this, we'll know that the qty has to be reduced in US
                UPDATE te501_order_replenishment_part
                   SET ce501_ous_processed_fl = 'Y'
                     , ce501_ous_processed_date = SYSDATE
                     , ce501_us_processed_fl =
                           NULL   -- because whatever the scenario, we need to remove the flag so that it'll be processed in US
                     , ce501_wo_operation =
                           DECODE (ce501_us_processed_fl
                                 , NULL, 'upsert'
                                 , 'update'
                                  )   -- also can be interpreted as deallocated
                 WHERE c402_work_order_id = v_work_order_id
		 AND c1900_company_id=p_company_id;

                UPDATE te501_order_replenishment_part
                   SET ce501_ous_processed_fl = 'Y'
                     , ce501_ous_processed_date = SYSDATE
                     , ce501_wo_operation =
                           'nop_update'   -- no-operation. qty used during deallocated and this doesnt have to be processed
                 WHERE ce501_should_need_po = 'N'
                   AND ce501_ous_processed_fl IS NULL   -- so that if job runs after partial failure, duplicate WO is not created
                   AND c205_part_number_id = wos_info.pnum
		   AND c1900_company_id=p_company_id;
            --AND NVL (c901_order_type, -999) NOT IN (2528, 2529);   -- important condition. not needed. it has to update only for return orders.
            END IF;

            IF v_input_qty <= 0
            THEN
                v_negative_fl := TRUE;
            END IF;
        END LOOP;
    END gm_etl_op_dealloc_wo_qty;

/****************************************************************************************************************************************
 * This procedure is to update part qty information in the new t205c_part_qty table
 *
*****************************************************************************************************************************************/
    PROCEDURE gm_etl_op_new_po_notify (
        p_po_id      t401_purchase_order.c401_purchase_ord_id%TYPE
      , p_user_ids   t906_rules.c906_rule_value%TYPE
    )
    AS
        v_user_id      t402_work_order.c402_created_by%TYPE := '649291';   -- this is for "created by" in alerts table
    BEGIN
        globus_app.gm_pkg_alt_alerts.gm_sav_alerts (102560   -- low priority
                                                  , 102542   -- Notification
                                                  , 102543   -- Restock PO creation
                                                  , CURRENT_DATE
                                                  , 'New PO creation : ' || p_po_id
                                                  , 'New PO ' || p_po_id || ' created at ' || TRUNC (CURRENT_DATE)
                                                  , 102545   -- NO
                                                  , v_user_id
                                                  , p_user_ids
                                                   );
    END gm_etl_op_new_po_notify;

/****************************************************************************************************************************************
 * This procedure is to create restock WO for each of the restock PO
 *
*****************************************************************************************************************************************/
    PROCEDURE gm_etl_op_process_restock_wo (
        p_po_id            t401_purchase_order.c401_purchase_ord_id%TYPE
	  , p_company_id		   IN   t1900_company.c1900_company_id%TYPE
      , p_vendor_id            IN   t301_vendor.C301_VENDOR_ID%TYPE
      , p_wo_count   OUT   NUMBER
    )
    AS
-- Cursor to fetch all Part, qty, cost from TE501_ORDER_REPLENISHMENT_PART and T205C_PART_QTY with qty > 0
        v_vendor_id    t402_work_order.c301_vendor_id%TYPE;-- := 8;
        v_user_id      t402_work_order.c402_created_by%TYPE := '649291';
        v_wo_id        t402_work_order.c402_work_order_id%TYPE;   -- out param
        v_part_id      t402_work_order.c205_part_number_id%TYPE;   -- assigned from cursor
        v_qty          t402_work_order.c402_qty_ordered%TYPE;
        v_wo_count     NUMBER := 0;
        
        CURSOR cur_wo_details
        IS
            -- get all parts that needs a PO.
            SELECT   t205c.c205_part_number_id part_id, t205c.c205_available_qty qty
                   , part_replenish.cost_price costprice
                FROM te205c_repl_part_qty t205c
                   , (SELECT DISTINCT te501.c205_part_number_id pnum, te501.c405_cost_price cost_price
                                 FROM te501_order_replenishment_part te501
                                WHERE te501.ce501_should_need_po = 'Y'
                                  AND ce501_ous_processed_fl IS NULL   -- so that if job runs after partial failure, duplicate WO is not created
                                  AND te501.c401_purchase_ord_id = p_po_id
				  AND te501.c1900_company_id=p_company_id) part_replenish
               WHERE part_replenish.pnum = t205c.c205_part_number_id AND c901_transaction_type = 4000100
               and t205c.c1900_company_id=p_company_id
            ORDER BY qty DESC NULLS LAST;
--               AND t205c.c205_available_qty > 0;
    BEGIN
            
        FOR wo_info IN cur_wo_details
        LOOP
            v_part_id   := wo_info.part_id;
            v_qty       := wo_info.qty;

            -- only if the available qty in t205c has +ve value will we need a new WO
            IF v_qty > 0
            THEN
                v_wo_count  := v_wo_count + 1;
                gm_pkg_intl_auto_sales_restock.gm_sav_work_order (p_po_id
                                                           , p_vendor_id
                                                           , v_part_id
                                                           , v_qty
                                                           , wo_info.costprice
                                                           , v_user_id
                                                           , p_company_id
                                                           , v_wo_id
														   );

                UPDATE te501_order_replenishment_part
                   SET c402_work_order_id = v_wo_id
                     , ce501_wo_created_date = SYSDATE
                     , ce501_ous_processed_fl = 'Y'
                     , ce501_ous_processed_date = SYSDATE
                     , ce501_wo_operation =
                           DECODE
                               (ce501_should_need_po
                              , 'Y', 'insert'
                              , 'N', 'nop_wo'
                               )   -- nop_wo means that the qty was already considered in WO qty calculation.
                 WHERE ce501_ous_processed_fl IS NULL   -- so that if job runs after partial failure, duplicate WO is not created
                   AND c205_part_number_id = v_part_id
		   AND c1900_company_id=p_company_id;

--               AND NVL (c901_order_type, -999) NOT IN (2528, 2529);   -- important condition.
                gm_cm_sav_partrepl_qty (v_part_id
                                 , v_qty * -1
                                 , v_wo_id
                                 , v_user_id
                                 , 4000100   -- Sales replenishment
                                 , 4302   -- minus
                                 , 4319   -- Work order
                                 , p_company_id
                                 );
                -- if available qty < 0 then make the operation as nop_total_qty_lto
            -- since this doesnt increment the WO counter. If for a specific PO, the wo count returns 0, we delete the PO.
            -- we dont mark the records (which has ce501_should_need_po as N) to processed because it will have to be processed for reducing the WO values in gm_etl_op_upd_wo_of_po
            ELSE
                UPDATE te501_order_replenishment_part
                   SET c401_purchase_ord_id =
                                             NULL   -- this could have been earlier set to a value and should be NULLed out
                     , ce501_po_created_date = NULL
                     , ce501_ous_processed_fl = DECODE (ce501_should_need_po, 'Y', 'Y', ce501_ous_processed_fl)
                     , ce501_ous_processed_date = DECODE (ce501_should_need_po, 'Y', SYSDATE, ce501_ous_processed_date)
                     , ce501_wo_operation = 'nop_total_qty_lto'   -- total qty is <= 0
                 WHERE ce501_ous_processed_fl IS NULL   -- so that if job runs after partial failure, duplicate WO is not created
                   AND c205_part_number_id = v_part_id
		   AND c1900_company_id=p_company_id;
            END IF;
        END LOOP;

        p_wo_count  := v_wo_count;
    END gm_etl_op_process_restock_wo;

/****************************************************************************************************************************************
 * This procedure is to update part qty information in the new t205c_part_qty table
 *
*****************************************************************************************************************************************/
    PROCEDURE gm_sav_work_order (
        p_po_id       IN       t401_purchase_order.c401_purchase_ord_id%TYPE
      , p_vendor_id   IN       t301_vendor.c301_vendor_id%TYPE
      , p_part_num    IN       t205_part_number.c205_part_number_id%TYPE
      , p_quantity    IN       NUMBER
      , p_cost        IN       t402_work_order.c402_cost_price%TYPE
      , p_userid      IN       t401_purchase_order.c401_created_by%TYPE
      , p_company_id  IN       t1900_company.c1900_company_id%TYPE
      , p_wo_id       OUT      t402_work_order.c402_work_order_id%TYPE
       )
    AS
        v_wo_id        t402_work_order.c402_work_order_id%TYPE;
    BEGIN
        SELECT 'GM-WO-' || s402_work.NEXTVAL
          INTO v_wo_id
          FROM DUAL;

        INSERT INTO t402_work_order
                    (c402_work_order_id, c401_purchase_ord_id, c205_part_number_id, c301_vendor_id, c402_created_date
                   , c402_status_fl, c402_cost_price, c402_created_by, c402_last_updated_date, c402_last_updated_by
                   , c402_qty_ordered, c402_rev_num, c402_critical_fl, c402_sub_component_fl, c402_sterilization_fl
                   , c408_dhr_id, c402_far_fl, c402_void_fl, c405_pricing_id, c402_completed_date, c402_qty_history_fl
                   , c402_price_history_fl, c402_split_percentage, c402_posting_fl, c402_rc_fl, c402_split_cost
                   , c402_rm_inv_fl,c1900_company_id
                    )
             VALUES (v_wo_id, p_po_id, p_part_num, p_vendor_id, CURRENT_DATE
                   , '1', p_cost, p_userid, NULL, NULL
                   , p_quantity, 'A', NULL, NULL, NULL
                   , NULL, NULL, NULL, NULL, NULL, NULL
                   , NULL, NULL, NULL, NULL, NULL
                   , NULL,p_company_id
                    );

        p_wo_id     := v_wo_id;
    END gm_sav_work_order;

/****************************************************************************************************************************************
 * This procedure is to update total WO cost to the PO cost column
 *
*****************************************************************************************************************************************/
    PROCEDURE gm_etl_op_upd_po_tot_cost(
	p_company_id		   IN   t1900_company.c1900_company_id%TYPE
    )
    AS
    BEGIN
        UPDATE t401_purchase_order t401
           SET c401_po_total_amount =
                            (SELECT NVL (SUM (NVL (c402_cost_price, 0) * NVL (c402_qty_ordered, 0)), 0)
                               FROM t402_work_order t402
                              WHERE t402.c401_purchase_ord_id = t401.c401_purchase_ord_id AND t402.c402_void_fl IS NULL AND t402.c1900_company_id=p_company_id)
             , t401.c401_last_updated_by = '649291'
             , t401.c401_last_updated_date = CURRENT_DATE
         WHERE t401.c401_purchase_ord_id IN (SELECT c401_purchase_ord_id
                                               FROM te501_order_replenishment_part
                                              WHERE te501_order_replenishment_part.ce501_us_processed_fl IS NULL AND c1900_company_id=p_company_id)
											  AND t401.c1900_company_id=p_company_id;
    END gm_etl_op_upd_po_tot_cost;

/****************************************************************************************************************************************
 * This procedure is to sync the RQ / CN qty with the WO quantity
 *
*****************************************************************************************************************************************/
    PROCEDURE gm_etl_po_rq_sync(
	p_company_id		   IN   t1900_company.c1900_company_id%TYPE
    )
    AS
        CURSOR cur_open_wo_to_sync
        IS
            SELECT c402_work_order_id woid, c205_part_number_id pnum, ce520b_diff_qty diffqty
              FROM te520b_wo_rq_upd_qty_info
             WHERE ce520b_ous_processed_fl IS NULL AND ce520b_diff_qty <> 0
             AND c1900_company_id=p_company_id;

        v_wo_diff_qty  te520b_wo_rq_upd_qty_info.ce520b_diff_qty%TYPE;
        v_user_id      VARCHAR2 (20) := '649291';
        v_action_sign  t205c_part_qty.c901_action%TYPE := 4302;   -- minus
        v_wo_id        te520b_wo_rq_upd_qty_info.c402_work_order_id%TYPE;
    BEGIN
        UPDATE te520b_wo_rq_upd_qty_info
           SET ce520b_ous_processed_fl = 'Y'
         WHERE ce520b_ous_processed_fl IS NULL AND ce520b_diff_qty = 0
         AND c1900_company_id=p_company_id;

        FOR open_wo_to_sync IN cur_open_wo_to_sync
        LOOP
            v_wo_diff_qty := open_wo_to_sync.diffqty;
            v_wo_id     := open_wo_to_sync.woid;

            IF v_wo_diff_qty > 0
            THEN
                v_action_sign := 4301;
            END IF;

            UPDATE t402_work_order
               SET c402_qty_ordered = c402_qty_ordered + (v_wo_diff_qty * -1)   -- reverse the diff qty
                 , c402_last_updated_date = CURRENT_DATE
                 , c402_last_updated_by = v_user_id
                 , c402_status_fl = 2
             WHERE c402_work_order_id = v_wo_id
             AND c1900_company_id=p_company_id;

            gm_cm_sav_partrepl_qty (open_wo_to_sync.pnum
                             , v_wo_diff_qty   -- could be +ve or -ve
                             , v_wo_id
                             , v_user_id
                             , 4000100   -- Sales replenishment
                             , v_action_sign   -- plus or minus
                             , 4000126   -- OUS Restock RQ to WO sync
                             , p_company_id
                            );

            UPDATE te520b_wo_rq_upd_qty_info
               SET ce520b_ous_processed_fl = 'Y'
                 , ce520b_ous_processed_date = SYSDATE
                 , ce520b_wo_processed_fl = 'Y'
                 , ce520b_wo_processed_date = SYSDATE
             WHERE ce520b_ous_processed_fl IS NULL AND c402_work_order_id = v_wo_id
             AND c1900_company_id=p_company_id;
        END LOOP;
    END gm_etl_po_rq_sync;

    PROCEDURE gm_etl_sav_wo_upd_qty (
        p_purchase_ord_id   IN   t401_purchase_order.c401_purchase_ord_id%TYPE
      , p_part_number_id    IN   t205_part_number.c205_part_number_id%TYPE
      , p_wo_operation      IN   VARCHAR2
      , p_trans_qty         IN   t205c_part_qty.c205_available_qty%TYPE
      , p_work_order_id     IN   t402_work_order.c402_work_order_id%TYPE
      , p_etl_job_run_id    IN   NUMBER
	  , p_company_id		   IN   t1900_company.c1900_company_id%TYPE
    )
    AS
    BEGIN

        INSERT INTO te402_po_to_rq_update_void
                    (c401_purchase_ord_id, c205_part_number_id, ce501_wo_operation, c214_trans_qty
                   , c402_work_order_id, ce402_etl_load_date, ce402_etl_job_run_id,c1900_company_id
                    )
             VALUES (p_purchase_ord_id, p_part_number_id, p_wo_operation, p_trans_qty
                   , p_work_order_id, SYSDATE, p_etl_job_run_id,p_company_id
                    );
    END gm_etl_sav_wo_upd_qty;

    /****************************************************************************************************************************************
 *  Description  :  Procedure to call PO creationg with split of PO
 *
*****************************************************************************************************************************************/
    PROCEDURE gm_sav_initiate_repl_po (
		p_src_comp IN t1900_company.c1900_company_id%TYPE
	    ,p_src_plant IN t5040_plant_master.c5040_plant_id%TYPE
	    ,p_src_vendor IN t301_vendor.c301_vendor_id%TYPE
	    ,p_po_type   IN t401_purchase_order.c401_type%TYPE
	    ,p_userId IN t401_purchase_order.c401_created_by%TYPE
        )
    AS
   	v_wo_split_cnt NUMBER;  -- can be 50
	v_total_line_item_cnt  NUMBER;
	v_po_count             NUMBER;
	v_start_value          NUMBER;
	v_wo_split_val         NUMBER;
	v_msg VARCHAR2(500):='Restock PO created by ETL';
	v_po_company NUMBER;
	v_po_id        t401_purchase_order.C401_PURCHASE_ORD_ID%TYPE;
	v_DateReq      t401_purchase_order.c401_date_required%TYPE;
	v_po_prefix VARCHAR2(10);
	pcount NUMBER;
	v_test_cnt number;

        CURSOR ttp_dtls_cur
        IS
            /* To get by System and part company */
        
                SELECT C207_SYSTEM_ID sys_id,C1900_PART_COMPANY_ID po_comp  FROM TE501_ORDER_REPLENISHMENT_PART
			    WHERE C401_PURCHASE_ORD_ID IS NULL
                AND CE501_SHOULD_NEED_PO = 'Y'
                AND CE501_PART_QTY_UPDATE_FL = 'Y'
                AND CE501_OUS_PROCESSED_FL IS NULL
                AND C1900_COMPANY_ID=p_src_comp   -- input variable
                AND C1900_PART_COMPANY_ID =v_po_company
                --and C207_SYSTEM_ID =-1
                GROUP BY C207_SYSTEM_ID,C1900_PART_COMPANY_ID;
                
        CURSOR po_company
        IS
               /* To run for each Replenishment company like GMANA and ATEC */
        
                SELECT C1900_PART_COMPANY_ID po_comp_id 
                FROM TE501_ORDER_REPLENISHMENT_PART
			    WHERE C401_PURCHASE_ORD_ID IS NULL
                AND CE501_SHOULD_NEED_PO = 'Y'
                AND CE501_PART_QTY_UPDATE_FL = 'Y'
                AND CE501_OUS_PROCESSED_FL IS NULL
                AND C1900_COMPANY_ID=p_src_comp -- input variable
                GROUP BY C1900_PART_COMPANY_ID;

BEGIN
			BEGIN
	         SELECT GET_CODE_NAME_ALT(C906_RULE_ID)||'-PO-'  INTO v_po_prefix
	            FROM T906_RULES 
	            WHERE C906_RULE_GRP_ID='OUS_CNTRY_COMPNY_MAP' 
	            AND C906_RULE_VALUE=p_src_comp;  -- input from cur
	           EXCEPTION WHEN OTHERS
		  		 THEN
		   		v_po_prefix := 'GM-PO-';
		   	END;
	   	
	   		BEGIN
			  		   	
			   	select get_business_day(next_day(sysdate,(SELECT ce214_shipment_day 
			   		from globus_etl.te214_repl_master WHERE  C5040_PLANT_ID=p_src_plant AND ce214_active_fl='Y')),
						(SELECT CE214_TRANSIT_DAYS from globus_etl.te214_repl_master 
							WHERE  C5040_PLANT_ID=p_src_plant)) into v_DateReq  from dual;
				
				/* if NED then Thursday  + 3 Business Day as Required Date */
				
	   		EXCEPTION WHEN OTHERS
		  		 THEN
		   		v_DateReq := SYSDATE;
		   	END;
			
		SELECT NVL(GET_RULE_VALUE('REPL_POWO_CNT','REPL_PO_WO_CNT'), '50') INTO v_wo_split_cnt
		FROM dual;
                                            
    FOR po_comp_dtl IN po_company
        LOOP
     
        v_po_company := po_comp_dtl.po_comp_id;
     --   dbms_output.put_line (' v_po_company  :'|| v_po_company) ;
        
                FOR ttp_dtl IN ttp_dtls_cur
                LOOP
                        SELECT COUNT(1) INTO v_total_line_item_cnt
                        FROM
                            (SELECT C205_PART_NUMBER_ID PNUM  FROM TE501_ORDER_REPLENISHMENT_PART
                                WHERE C401_PURCHASE_ORD_ID IS NULL
                                AND CE501_SHOULD_NEED_PO = 'Y'
                                AND CE501_PART_QTY_UPDATE_FL = 'Y'
                                AND CE501_OUS_PROCESSED_FL IS NULL
                                and C1900_COMPANY_ID=p_src_comp   -- input variable from pkg
                                and C1900_PART_COMPANY_ID =ttp_dtl.po_comp --input from cur
                                and C207_SYSTEM_ID =ttp_dtl.sys_id  -- input from cur
                                GROUP BY C205_PART_NUMBER_ID);
                                    
                                        v_po_count                    := CEIL(v_total_line_item_cnt/v_wo_split_cnt);
                                        v_start_value                 :=0;
                                        v_wo_split_val                :=v_wo_split_cnt;
                                        
                                     --  dbms_output.put_line (' SEQ v_po_count  :'|| v_po_count) ;
                                     --  dbms_output.put_line (' SEQ v_total_line_item_cnt  :'|| v_total_line_item_cnt) ;
                                   -- dbms_output.put_line (' v_po_count  :'|| v_po_count) ;
                            WHILE(v_po_count               > 0)
                            LOOP
            --select *from my_temp_part_list;
            
                                delete from my_temp_part_list;
											
                               insert into my_temp_part_list  (C205_PART_NUMBER_ID) 
                               (SELECT PNUM 
                               FROM
                                (SELECT C205_PART_NUMBER_ID PNUM  FROM TE501_ORDER_REPLENISHMENT_PART
                                    WHERE C401_PURCHASE_ORD_ID IS NULL
                                    AND CE501_SHOULD_NEED_PO = 'Y'
                                    AND CE501_PART_QTY_UPDATE_FL = 'Y'
                                    AND CE501_OUS_PROCESSED_FL IS NULL
                                    and C1900_COMPANY_ID=p_src_comp   -- input variable from pkg
                                    and C1900_PART_COMPANY_ID =ttp_dtl.po_comp --input from cur
                                    and C207_SYSTEM_ID =ttp_dtl.sys_id  -- input from cur
                                    GROUP BY C205_PART_NUMBER_ID) Offset  v_start_value 
                                    ROWS  FETCH NEXT v_wo_split_cnt ROWS ONLY);
                                           -- dbms_output.put_line (' PO String  :'|| v_po_str) ;
                                            --gm_pkg_op_purchasing.gm_sav_initiate_po (136,3100,v_po_str,'1764361',p_poid,p_msg);
                                            
                                            -- dbms_output.put_line (' v_start_value  :'|| v_start_value) ;
                                            -- dbms_output.put_line (' v_wo_split_val  :'|| v_wo_split_val) ;
                                            -- dbms_output.put_line (' v_wo_split_cnt  :'|| v_wo_split_cnt) ;
                                          
                                          
                                            v_start_value := 0;
                                            v_wo_split_val:=v_wo_split_val +v_wo_split_cnt;
                                            v_po_count    :=v_po_count     -1;
                                          --  dbms_output.put_line (' v_po_count  :'|| v_po_count) ;
											
                                                 SELECT v_po_prefix || s401_purchase.nextval
                                              		INTO v_po_id
                                              		FROM DUAL;
											
											INSERT INTO t401_purchase_order(
												c401_purchase_ord_id
												,c301_vendor_id
												,c401_purchase_ord_date
												,c401_date_required
												,c401_po_total_amount
												,c401_type
												,c401_created_by
												,c401_created_date
												,c1900_company_id
												,c5040_plant_id
												,c1900_source_company_id
												,c901_status
												, c401_po_notes
												) VALUES (
												v_po_id
												,p_src_vendor  -- input p_VendId
												,TRUNC(CURRENT_DATE)
												,v_DateReq
												,0
												,p_po_type --p_Type
												,p_userId
												,CURRENT_DATE
												,p_src_comp   --v_company_id
												,p_src_plant   --v_plant_id
												,ttp_dtl.po_comp  --p_source_company_id
												,NULL
												,v_msg
											);
										--	dbms_output.put_line (' New PO Id created  :'|| v_po_id) ;
                                           
							UPDATE globus_etl.TE501_ORDER_REPLENISHMENT_PART 
							SET C401_PURCHASE_ORD_ID=v_po_id,CE501_PO_CREATED_DATE=CURRENT_DATE
								where C1900_COMPANY_ID=p_src_comp  -- input variable from pkg
                                AND C401_PURCHASE_ORD_ID IS NULL
								and CE501_US_PROCESSED_FL is null and CE501_SHOULD_NEED_PO='Y'-- and CE501_WO_OPERATION<>'nop_total_qty_lto'
								AND C205_PART_NUMBER_ID IN (SELECT C205_PART_NUMBER_ID FROM my_temp_part_list)
								and C1900_PART_COMPANY_ID =ttp_dtl.po_comp --input from cur
								and C207_SYSTEM_ID =ttp_dtl.sys_id ; -- input from cur     
                            
                            
                                                                
                        END LOOP; --While LOOP end 
                    END LOOP; --for loop end
                    
                    -- v_po_company := NULL;
          END LOOP; --end loop for po_company            
	END gm_sav_initiate_repl_po;
    
END gm_pkg_intl_auto_sales_restock;
/