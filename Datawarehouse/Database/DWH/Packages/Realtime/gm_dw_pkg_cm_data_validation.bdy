/* Formatted on 2010/04/12 10:30 (Formatter Plus v4.8.0) */
-- @"C:\database\DW_BI\packages\gm_dw_pkg_cm_data_validation.bdy";

CREATE OR REPLACE PACKAGE BODY globus_bi_realtime.gm_dw_pkg_cm_data_validation
IS
	PROCEDURE gm_cm_validate_main
	AS
	BEGIN
		DBMS_MVIEW.REFRESH ('v207a_set_consign_link');
		gm_cm_territory_sales;
		gm_cm_historical_sales;
		gm_cm_total_baseline_set;
		gm_cm_total_set_count;
		gm_cm_total_consignment_cost;
		gm_cm_total_csg_cost_sys;
		gm_cm_total_procedure;
		gm_cm_total_procedure_csg;
		gm_cm_total_procedure_loaner;
		gm_cm_total_loaner_util;
		gm_cm_quota_2009;
	END gm_cm_validate_main;

	PROCEDURE gm_cm_territory_sales
	AS
		v_portal_total_sales t502_item_order.c502_item_price%TYPE;
		v_dw_total_sales t502_item_order.c502_item_price%TYPE;
		v_txn_query    t980_load_dw_log.c980_txn_query%TYPE;
		v_dw_query	   t980_load_dw_log.c980_dw_query%TYPE;
		v_measure	   t980_load_dw_log.c980_measure%TYPE;
		v_txn_val	   t980_load_dw_log.c980_txn_val%TYPE;
		v_dw_val	   t980_load_dw_log.c980_dw_val%TYPE;
		v_status	   t980_load_dw_log.c980_status%TYPE;
		v_check_date   t980_load_dw_log.c980_check_date%TYPE;
	BEGIN
		SELECT SUM (t502.c502_item_price * t502.c502_item_qty) amount
		  INTO v_portal_total_sales
		  FROM t501_order t501, t502_item_order t502, v700_territory_mapping_detail v700
		 WHERE t502.c501_order_id = t501.c501_order_id
		   AND NVL (t501.c901_order_type, -9999) <> '2533'
		   AND t501.c501_void_fl IS NULL
		   AND t501.c501_delete_fl IS NULL
		   AND v700.ac_id = t501.c704_account_id
		   AND t502.c205_part_number_id IN (SELECT c205_part_number_id
											  FROM t208_set_details t208, t207_set_master t207
											 WHERE t208.c207_set_id = t207.c207_set_id AND t207.c901_set_grp_type = 1600)
		   AND t501.c501_order_date >= ADD_MONTHS (TRUNC (SYSDATE, 'MONTH'), -11)
		   AND t501.c501_order_date <= SYSDATE;

		SELECT SUM (sales_fact.item_sales)
		  INTO v_dw_total_sales
		  FROM account_dim, sales_fact, date_dim, sales_rep_dim
		 WHERE sales_fact.account_key_id = account_dim.account_key_id
		   AND account_dim.sales_rep_id = sales_rep_dim.sales_rep_key_id
		   AND (sales_fact.date_key_id(+) = date_dim.date_key_id)
		   AND date_dim.cal_date >= ADD_MONTHS (TRUNC (SYSDATE, 'MONTH'), -11)
		   AND date_dim.cal_date <= SYSDATE;

		v_status	:= get_validation_status (v_portal_total_sales, v_dw_total_sales);
		gm_cm_log_validation ('gm_cm_territory_sales'
							, 'gm_cm_territory_sales'
							, 'sales by current org structure'
							, v_portal_total_sales
							, v_dw_total_sales
							, v_status
							, TRUNC (SYSDATE)
							 );
	END gm_cm_territory_sales;

--
	PROCEDURE gm_cm_historical_sales
	AS
		v_portal_total_sales t502_item_order.c502_item_price%TYPE;
		v_dw_total_sales t502_item_order.c502_item_price%TYPE;
		v_txn_query    t980_load_dw_log.c980_txn_query%TYPE;
		v_dw_query	   t980_load_dw_log.c980_dw_query%TYPE;
		v_measure	   t980_load_dw_log.c980_measure%TYPE;
		v_txn_val	   t980_load_dw_log.c980_txn_val%TYPE;
		v_dw_val	   t980_load_dw_log.c980_dw_val%TYPE;
		v_status	   t980_load_dw_log.c980_status%TYPE;
		v_check_date   t980_load_dw_log.c980_check_date%TYPE;
	BEGIN
		SELECT SUM (amount) amount
		  INTO v_portal_total_sales
		  FROM (SELECT t703.c703_sales_rep_id, t701.c701_distributor_id distributor_id, t701.c701_distributor_name NAME
				  FROM t703_sales_rep t703, t701_distributor t701
				 WHERE t703.c701_distributor_id = t701.c701_distributor_id) rep_disp_details
			 , (SELECT	 t501.c703_sales_rep_id, TO_CHAR (c501_order_date, 'Mon YY') r_date
					   , SUM (t502.c502_item_price * t502.c502_item_qty) amount
					FROM t501_order t501, t502_item_order t502
				   WHERE t502.c501_order_id = t501.c501_order_id
					 AND t501.c501_void_fl IS NULL
					 AND t501.c501_delete_fl IS NULL
					 AND NVL (t501.c901_order_type, -9999) <> '2533'
					 AND t502.c205_part_number_id IN (
											 SELECT c205_part_number_id
											   FROM t208_set_details t208, t207_set_master t207
											  WHERE t208.c207_set_id = t207.c207_set_id
													AND t207.c901_set_grp_type = 1600)
					 AND t501.c501_order_date >= ADD_MONTHS (TRUNC (SYSDATE, 'MONTH'), -11)
					 AND t501.c501_order_date <= SYSDATE
				GROUP BY t501.c703_sales_rep_id, TO_CHAR (c501_order_date, 'Mon YY'), 'MON YY') ord_detail
		 WHERE rep_disp_details.c703_sales_rep_id = ord_detail.c703_sales_rep_id;

		SELECT SUM (sales_fact.item_sales)
		  INTO v_dw_total_sales
		  FROM sales_fact, sales_rep_dim, date_dim
		 WHERE (sales_rep_dim.sales_rep_key_id = sales_fact.sales_rep_key_id)
		   AND (sales_fact.date_key_id(+) = date_dim.date_key_id)
		   AND date_dim.cal_date >= ADD_MONTHS (TRUNC (SYSDATE, 'MONTH'), -11)
		   AND date_dim.cal_date <= SYSDATE;

		v_status	:= get_validation_status (v_portal_total_sales, v_dw_total_sales);
		gm_cm_log_validation ('gm_cm_historical_sales'
							, 'gm_cm_historical_sales'
							, 'sales by history'
							, v_portal_total_sales
							, v_dw_total_sales
							, v_status
							, TRUNC (SYSDATE)
							 );
	END gm_cm_historical_sales;

	--
	PROCEDURE gm_cm_total_set_count
	AS
		v_portal_set_count NUMBER;
		v_dw_set_count NUMBER;
		v_txn_query    t980_load_dw_log.c980_txn_query%TYPE;
		v_dw_query	   t980_load_dw_log.c980_dw_query%TYPE;
		v_measure	   t980_load_dw_log.c980_measure%TYPE;
		v_txn_val	   t980_load_dw_log.c980_txn_val%TYPE;
		v_dw_val	   t980_load_dw_log.c980_dw_val%TYPE;
		v_status	   t980_load_dw_log.c980_status%TYPE;
		v_check_date   t980_load_dw_log.c980_check_date%TYPE;
	BEGIN
		SELECT COUNT (t730.c730_virtual_consignment_id) vcount
		  INTO v_portal_set_count
		  FROM t730_virtual_consignment t730;

		SELECT COUNT (DISTINCT field_consign_inventory_fact.logical_consignment_id) baselinecnt
		  INTO v_dw_set_count
		  FROM field_consign_inventory_fact;

		v_status	:= get_validation_status (v_portal_set_count, v_dw_set_count);
		gm_cm_log_validation ('gm_cm_total_set_count'
							, 'gm_cm_total_set_count'
							, 'total set count'
							, v_portal_set_count
							, v_dw_set_count
							, v_status
							, TRUNC (SYSDATE)
							 );
	END gm_cm_total_set_count;

--
	PROCEDURE gm_cm_total_baseline_set
	AS
		v_portal_baseline_set NUMBER;
		v_dw_baseline_set NUMBER;
		v_txn_query    t980_load_dw_log.c980_txn_query%TYPE;
		v_dw_query	   t980_load_dw_log.c980_dw_query%TYPE;
		v_measure	   t980_load_dw_log.c980_measure%TYPE;
		v_txn_val	   t980_load_dw_log.c980_txn_val%TYPE;
		v_dw_val	   t980_load_dw_log.c980_dw_val%TYPE;
		v_status	   t980_load_dw_log.c980_status%TYPE;
		v_check_date   t980_load_dw_log.c980_check_date%TYPE;
	BEGIN
		SELECT SUM (DECODE (v207.c901_link_type, 20100, 1, 0)) vcount
		  INTO v_portal_baseline_set
		  FROM t730_virtual_consignment t730, v207a_set_consign_link v207
		WHERE  t730.c207_set_id = v207.c207_actual_set_id;	 -- AND v700.d_id = t730.c701_distributor_id;

		SELECT COUNT (DISTINCT DECODE (set_dim.consignment_report_id
									 , 'Full Set', field_consign_inventory_fact.logical_consignment_id
									 , NULL
									  )
					 ) baselinecnt
		  INTO v_dw_baseline_set
		  FROM field_consign_inventory_fact, set_dim
		 WHERE (field_consign_inventory_fact.set_id = set_dim.set_id);

		v_status	:= get_validation_status (v_portal_baseline_set, v_dw_baseline_set);
		gm_cm_log_validation ('gm_cm_total_baseline_set'
							, 'gm_cm_total_baseline_set'
							, 'total baseline set'
							, v_portal_baseline_set
							, v_dw_baseline_set
							, v_status
							, TRUNC (SYSDATE)
							 );
	END gm_cm_total_baseline_set;

--
	PROCEDURE gm_cm_total_consignment_cost
	AS
		v_portal_total_csg_cost NUMBER;
		v_dw_total_csg_cost NUMBER;
		v_txn_query    t980_load_dw_log.c980_txn_query%TYPE;
		v_dw_query	   t980_load_dw_log.c980_dw_query%TYPE;
		v_measure	   t980_load_dw_log.c980_measure%TYPE;
		v_txn_val	   t980_load_dw_log.c980_txn_val%TYPE;
		v_dw_val	   t980_load_dw_log.c980_dw_val%TYPE;
		v_status	   t980_load_dw_log.c980_status%TYPE;
		v_check_date   t980_load_dw_log.c980_check_date%TYPE;
	BEGIN
		SELECT SUM (t731.c731_item_qty * t731.c731_item_price) total_csg_cost
		  INTO v_portal_total_csg_cost
		  FROM t731_virtual_consign_item t731;

		SELECT COUNT (DISTINCT field_consign_inventory_fact.logical_consignment_id) baselinecnt
		  INTO v_dw_total_csg_cost
		  FROM field_consign_inventory_fact;

		SELECT SUM (field_consign_inventory_fact.qty * field_consign_inventory_fact.part_price) baselinecnt
		  INTO v_dw_total_csg_cost
		  FROM field_consign_inventory_fact;

		v_status	:= get_validation_status (v_portal_total_csg_cost, v_dw_total_csg_cost);
		gm_cm_log_validation ('gm_cm_total_consignment_cost'
							, 'gm_cm_total_consignment_cost'
							, 'total consignment cost'
							, v_portal_total_csg_cost
							, v_dw_total_csg_cost
							, v_status
							, TRUNC (SYSDATE)
							 );
	END gm_cm_total_consignment_cost;

	--
	--
	PROCEDURE gm_cm_total_csg_cost_sys
	AS
		v_portal_total_csg_cost NUMBER;
		v_missing_dist_csg_cost NUMBER;
		v_dw_total_csg_cost NUMBER;
		v_txn_query    t980_load_dw_log.c980_txn_query%TYPE;
		v_dw_query	   t980_load_dw_log.c980_dw_query%TYPE;
		v_measure	   t980_load_dw_log.c980_measure%TYPE;
		v_txn_val	   t980_load_dw_log.c980_txn_val%TYPE;
		v_dw_val	   t980_load_dw_log.c980_dw_val%TYPE;
		v_status	   t980_load_dw_log.c980_status%TYPE;
		v_check_date   t980_load_dw_log.c980_check_date%TYPE;
	BEGIN
		SELECT SUM (t730.c730_total_cost) tot
		  INTO v_portal_total_csg_cost
		  FROM t730_virtual_consignment t730
			 , v207a_set_consign_link v207
			 , (SELECT DISTINCT ad_id, ad_name, region_id, region_name, d_id, d_name, vp_id, vp_name, v700.d_active_fl
						   FROM v700_territory_mapping_detail v700) v700
		 WHERE t730.c207_set_id = v207.c207_actual_set_id AND v700.d_id = t730.c701_distributor_id;

		SELECT SUM (part_price * qty)
		  INTO v_missing_dist_csg_cost
		  FROM field_consign_inventory_fact fif
		 WHERE fif.distributor_id IN (SELECT dd.distributor_id
										FROM distributor_dim dd
									   WHERE dd.region_key_id NOT IN (SELECT v700.region_id
																		FROM v700_territory_mapping_detail v700));

		SELECT SUM (field_consign_inventory_fact.part_price * field_consign_inventory_fact.qty) tot
		  INTO v_dw_total_csg_cost
		  FROM system_dim, field_consign_inventory_fact, system_set_hierarchy_dim
		 WHERE (field_consign_inventory_fact.set_id = system_set_hierarchy_dim.actual_set_id)
		   AND (system_set_hierarchy_dim.system_id = system_dim.system_id);

		v_status	:= get_validation_status ((v_portal_total_csg_cost + v_missing_dist_csg_cost), v_dw_total_csg_cost);
		gm_cm_log_validation ('gm_cm_total_csg_cost_sys'
							, 'gm_cm_total_csg_cost_sys'
							, 'total consignment cost by system'
							, v_portal_total_csg_cost + v_missing_dist_csg_cost
							, v_dw_total_csg_cost
							, v_status
							, TRUNC (SYSDATE)
							 );
	END gm_cm_total_csg_cost_sys;

	--
	PROCEDURE gm_cm_total_procedure
	AS
		v_portal_total_procedures NUMBER;
		v_dw_total_procedures NUMBER;
		v_txn_query    t980_load_dw_log.c980_txn_query%TYPE;
		v_dw_query	   t980_load_dw_log.c980_dw_query%TYPE;
		v_measure	   t980_load_dw_log.c980_measure%TYPE;
		v_txn_val	   t980_load_dw_log.c980_txn_val%TYPE;
		v_dw_val	   t980_load_dw_log.c980_dw_val%TYPE;
		v_status	   t980_load_dw_log.c980_status%TYPE;
		v_check_date   t980_load_dw_log.c980_check_date%TYPE;
	BEGIN
		SELECT ROUND (SUM (amount)) amount
		  INTO v_portal_total_procedures
		  FROM (SELECT	 v207.c207_set_id ID, v207.c207_set_nm NAME, c207_seq_no
					   , TO_CHAR (c501_order_date, 'Mon YY') r_date
					   , DECODE (v207.c204_value, 0, 0, (SUM (t502.c502_item_qty) / v207.c204_value)) amount
					FROM t501_order t501
					   , t502_item_order t502
					   , v207b_set_part_details v207
					   , v700_territory_mapping_detail v700
				   WHERE v207.c205_part_number_id = t502.c205_part_number_id
					 AND NVL (t501.c901_order_type, -9999)NOT IN ('2533','2524')
					 AND v700.ac_id = t501.c704_account_id
					 AND t502.c501_order_id = t501.c501_order_id
					 AND t501.c501_void_fl IS NULL
					 AND t501.c501_delete_fl IS NULL
					 AND v207.c205_tracking_implant_fl = 'Y'
					 -- AND t502.c901_type = 50300
					 AND t501.c501_order_date BETWEEN ADD_MONTHS (TRUNC (SYSDATE, 'MONTH'), -3)
												  AND LAST_DAY (ADD_MONTHS (TRUNC (SYSDATE, 'MONTH'), -1))
				GROUP BY v207.c207_set_id
					   , v207.c207_set_nm
					   , v207.c207_seq_no
					   , v207.c204_value
					   , TO_CHAR (c501_order_date, 'Mon YY')
					   , 'MON YY');

		SELECT ROUND (SUM (  DECODE (system_dim.baseline_case
								   , 0, NULL
								   , DECODE (part_dim.tracking_implant_fl, 'Y', sales_fact.item_qty, NULL)
									)
						   / DECODE (system_dim.baseline_case, 0, NULL, system_dim.baseline_case)
						  )
					 ) noofproc
		  INTO v_dw_total_procedures
		  FROM part_dim, sales_fact, system_dim, date_dim
		 WHERE (sales_fact.date_key_id = date_dim.date_key_id)
		   AND (sales_fact.system_id = system_dim.system_id)
		   AND (sales_fact.part_id = part_dim.part_id)
		   AND (date_dim.cal_date BETWEEN ADD_MONTHS (TRUNC (SYSDATE, 'MONTH'), -3)
									  AND LAST_DAY (ADD_MONTHS (TRUNC (SYSDATE, 'MONTH'), -1))
			   );

		v_status	:= get_validation_status (v_portal_total_procedures, v_dw_total_procedures);
		gm_cm_log_validation ('gm_cm_total_procedure'
							, 'gm_cm_total_procedure'
							, 'total procedure'
							, v_portal_total_procedures
							, v_dw_total_procedures
							, v_status
							, TRUNC (SYSDATE)
							 );
	END gm_cm_total_procedure;

	--
	PROCEDURE gm_cm_total_procedure_csg
	AS
		v_portal_total_procedures NUMBER;
		v_dw_total_procedures NUMBER;
		v_txn_query    t980_load_dw_log.c980_txn_query%TYPE;
		v_dw_query	   t980_load_dw_log.c980_dw_query%TYPE;
		v_measure	   t980_load_dw_log.c980_measure%TYPE;
		v_txn_val	   t980_load_dw_log.c980_txn_val%TYPE;
		v_dw_val	   t980_load_dw_log.c980_dw_val%TYPE;
		v_status	   t980_load_dw_log.c980_status%TYPE;
		v_check_date   t980_load_dw_log.c980_check_date%TYPE;
	BEGIN
		SELECT ROUND (SUM (amount)) amount
		  INTO v_portal_total_procedures
		  FROM (SELECT	 v207.c207_set_id ID, v207.c207_set_nm NAME, c207_seq_no
					   , TO_CHAR (c501_order_date, 'Mon YY') r_date
					   , DECODE (v207.c204_value, 0, 0, (SUM (t502.c502_item_qty) / v207.c204_value)) amount
					FROM t501_order t501
					   , t502_item_order t502
					   , v207b_set_part_details v207
					   , v700_territory_mapping_detail v700
				   WHERE v207.c205_part_number_id = t502.c205_part_number_id
					 AND NVL (t501.c901_order_type, -9999)NOT IN ('2533','2524')
					 AND v700.ac_id = t501.c704_account_id
					 AND t502.c501_order_id = t501.c501_order_id
					 AND t501.c501_void_fl IS NULL
					 AND t501.c501_delete_fl IS NULL
					 AND v207.c205_tracking_implant_fl = 'Y'
					 AND t502.c901_type = 50300
					 AND t501.c501_order_date BETWEEN ADD_MONTHS (TRUNC (SYSDATE, 'MONTH'), -3)
												  AND LAST_DAY (ADD_MONTHS (TRUNC (SYSDATE, 'MONTH'), -1))
				GROUP BY v207.c207_set_id
					   , v207.c207_set_nm
					   , v207.c207_seq_no
					   , v207.c204_value
					   , TO_CHAR (c501_order_date, 'Mon YY')
					   , 'MON YY');

		SELECT ROUND (SUM (  DECODE (system_dim.baseline_case
								   , 0, NULL
								   , DECODE (part_dim.tracking_implant_fl, 'Y', sales_fact.item_qty, NULL)
									)
						   / DECODE (system_dim.baseline_case, 0, NULL, system_dim.baseline_case)
						  )
					 ) noofproc
		  INTO v_dw_total_procedures
		  FROM part_dim, sales_fact, system_dim, date_dim, item_order_dim
		 WHERE (sales_fact.date_key_id = date_dim.date_key_id)
		   AND sales_fact.item_order_key_id = item_order_dim.item_order_key_id
		   AND (sales_fact.system_id = system_dim.system_id)
		   AND (sales_fact.part_id = part_dim.part_id)
		   AND item_order_dim.order_part_type = 'Sales Consignment'
		   AND (date_dim.cal_date BETWEEN ADD_MONTHS (TRUNC (SYSDATE, 'MONTH'), -3)
									  AND LAST_DAY (ADD_MONTHS (TRUNC (SYSDATE, 'MONTH'), -1))
			   );

		v_status	:= get_validation_status (v_portal_total_procedures, v_dw_total_procedures);
		gm_cm_log_validation ('gm_cm_total_procedure_csg'
							, 'gm_cm_total_procedure_csg'
							, 'total consignment procedure'
							, v_portal_total_procedures
							, v_dw_total_procedures
							, v_status
							, TRUNC (SYSDATE)
							 );
	END gm_cm_total_procedure_csg;

	--
	PROCEDURE gm_cm_total_procedure_loaner
	AS
		v_portal_total_procedures NUMBER;
		v_dw_total_procedures NUMBER;
		v_txn_query    t980_load_dw_log.c980_txn_query%TYPE;
		v_dw_query	   t980_load_dw_log.c980_dw_query%TYPE;
		v_measure	   t980_load_dw_log.c980_measure%TYPE;
		v_txn_val	   t980_load_dw_log.c980_txn_val%TYPE;
		v_dw_val	   t980_load_dw_log.c980_dw_val%TYPE;
		v_status	   t980_load_dw_log.c980_status%TYPE;
		v_check_date   t980_load_dw_log.c980_check_date%TYPE;
	BEGIN
		SELECT ROUND (SUM (amount)) amount
		  INTO v_portal_total_procedures
		  FROM (SELECT	 v207.c207_set_id ID, v207.c207_set_nm NAME, c207_seq_no
					   , TO_CHAR (c501_order_date, 'Mon YY') r_date
					   , DECODE (v207.c204_value, 0, 0, (SUM (t502.c502_item_qty) / v207.c204_value)) amount
					FROM t501_order t501
					   , t502_item_order t502
					   , v207b_set_part_details v207
					   , v700_territory_mapping_detail v700
				   WHERE v207.c205_part_number_id = t502.c205_part_number_id
					 AND NVL (t501.c901_order_type, -9999) NOT IN ('2533','2524')
					 AND v700.ac_id = t501.c704_account_id
					 AND t502.c501_order_id = t501.c501_order_id
					 AND t501.c501_void_fl IS NULL
					 AND t501.c501_delete_fl IS NULL
					 AND v207.c205_tracking_implant_fl = 'Y'
					 AND t502.c901_type = 50301
					 AND t501.c501_order_date BETWEEN ADD_MONTHS (TRUNC (SYSDATE, 'MONTH'), -3)
												  AND LAST_DAY (ADD_MONTHS (TRUNC (SYSDATE, 'MONTH'), -1))
				GROUP BY v207.c207_set_id
					   , v207.c207_set_nm
					   , v207.c207_seq_no
					   , v207.c204_value
					   , TO_CHAR (c501_order_date, 'Mon YY')
					   , 'MON YY');

		SELECT ROUND (SUM (  DECODE (system_dim.baseline_case
								   , 0, NULL
								   , DECODE (part_dim.tracking_implant_fl, 'Y', sales_fact.item_qty, NULL)
									)
						   / DECODE (system_dim.baseline_case, 0, NULL, system_dim.baseline_case)
						  )
					 ) noofproc
		  INTO v_dw_total_procedures
		  FROM part_dim, sales_fact, system_dim, date_dim, item_order_dim
		 WHERE (sales_fact.date_key_id = date_dim.date_key_id)
		   AND sales_fact.item_order_key_id = item_order_dim.item_order_key_id
		   AND (sales_fact.system_id = system_dim.system_id)
		   AND (sales_fact.part_id = part_dim.part_id)
		   AND item_order_dim.order_part_type = 'Loaner'
		   AND (date_dim.cal_date BETWEEN ADD_MONTHS (TRUNC (SYSDATE, 'MONTH'), -3)
									  AND LAST_DAY (ADD_MONTHS (TRUNC (SYSDATE, 'MONTH'), -1))
			   );

		v_status	:= get_validation_status (v_portal_total_procedures, v_dw_total_procedures);
		gm_cm_log_validation ('gm_cm_total_procedure_loaner'
							, 'gm_cm_total_procedure_loaner'
							, 'total loaner procedure'
							, v_portal_total_procedures
							, v_dw_total_procedures
							, v_status
							, TRUNC (SYSDATE)
							 );
	END gm_cm_total_procedure_loaner;

	--
	PROCEDURE gm_cm_total_loaner_util
	AS
		v_portal_total_loaner_util NUMBER;
		v_dw_total_loaner_util NUMBER;
		v_txn_query    t980_load_dw_log.c980_txn_query%TYPE;
		v_dw_query	   t980_load_dw_log.c980_dw_query%TYPE;
		v_measure	   t980_load_dw_log.c980_measure%TYPE;
		v_txn_val	   t980_load_dw_log.c980_txn_val%TYPE;
		v_dw_val	   t980_load_dw_log.c980_dw_val%TYPE;
		v_status	   t980_load_dw_log.c980_status%TYPE;
		v_check_date   t980_load_dw_log.c980_check_date%TYPE;
	BEGIN
		SELECT COUNT (distinct t504a.C504A_LOANER_TRANSACTION_ID) vcount
		  INTO v_portal_total_loaner_util
		  FROM t504_consignment t504
			 , t504a_loaner_transaction t504a
			 , v207a_set_consign_link v207
			 , (SELECT DISTINCT ad_id, ad_name, region_id, region_name, d_id, d_name, vp_id, vp_name, v700.d_active_fl
						   FROM v700_territory_mapping_detail v700) v700
		 WHERE t504.c207_set_id = v207.c207_actual_set_id
		   AND t504.c504_consignment_id = t504a.c504_consignment_id
		   AND t504a.c504a_consigned_to_id = v700.d_id
		   AND TRUNC (t504a.c504a_loaner_dt) BETWEEN ADD_MONTHS (TRUNC (SYSDATE, 'MONTH'), -3)
												  AND LAST_DAY (ADD_MONTHS (TRUNC (SYSDATE, 'MONTH'), -1))
		   AND t504.c504_type = 4127
		   AND v207.c901_link_type = 20100
		   AND t504.c504_void_fl IS NULL
		   AND t504a.c504a_void_fl IS NULL
		   AND t504.c207_set_id IS NOT NULL;

		SELECT COUNT (lif.LOANER_KEY_ID)
		  INTO v_dw_total_loaner_util
		  FROM loaner_inventory_fact lif, loaner_dim ld, distributor_dim dd, set_dim sd
		 WHERE lif.loaner_key_id = ld.loaner_key_id
		   AND TRUNC (ld.loaner_date) BETWEEN ADD_MONTHS (TRUNC (SYSDATE, 'MONTH'), -3)
												  AND LAST_DAY (ADD_MONTHS (TRUNC (SYSDATE, 'MONTH'), -1))
		   AND lif.distributor_key_id = dd.distributor_key_id
		   AND lif.set_key_id = sd.set_key_id
		   AND sd.consignment_report_id = 'Full Set';

		v_status	:= get_validation_status (v_portal_total_loaner_util, v_dw_total_loaner_util);
		gm_cm_log_validation ('gm_cm_total_loaner_util'
							, 'gm_cm_total_loaner_util'
							, 'total loaner utilization'
							, v_portal_total_loaner_util
							, v_dw_total_loaner_util
							, v_status
							, TRUNC (SYSDATE)
							 );
	END gm_cm_total_loaner_util;

--
	PROCEDURE gm_cm_quota_2009
	AS
		v_portal_quota_2009 NUMBER;
		v_dw_quota_2009 NUMBER;
		v_txn_query    t980_load_dw_log.c980_txn_query%TYPE;
		v_dw_query	   t980_load_dw_log.c980_dw_query%TYPE;
		v_measure	   t980_load_dw_log.c980_measure%TYPE;
		v_txn_val	   t980_load_dw_log.c980_txn_val%TYPE;
		v_dw_val	   t980_load_dw_log.c980_dw_val%TYPE;
		v_status	   t980_load_dw_log.c980_status%TYPE;
		v_check_date   t980_load_dw_log.c980_check_date%TYPE;
	BEGIN
		SELECT SUM (NVL (t709.c709_quota_breakup_amt, 0)) amount
		  INTO v_portal_quota_2009
		  FROM t709_quota_breakup t709
			 , (SELECT DISTINCT ad_id, ad_name, region_id, region_name, d_id, d_name, ter_id, rep_id, rep_name
							  , t901.c901_code_id vp_id, get_code_name_alt (t901.c901_code_id) vp_name
						   FROM v700_territory_mapping_detail v700, t901_code_lookup t901
						  WHERE v700.region_id = t901.c901_code_id) v700
		 WHERE t709.c709_reference_id = v700.ter_id
		   AND t709.c901_type = 20750
		   AND t709.c709_start_date >= TO_DATE ('01/2009', 'MM/YYYY')
		   AND t709.c709_end_date <= LAST_DAY (TO_DATE ('12/2009', 'MM/YYYY'));

		SELECT SUM (quota_fact.amount) QUOTA
		  INTO v_dw_quota_2009
		  FROM quota_fact, date_dim
		 WHERE (date_dim.cal_date = quota_fact.start_date)
		   AND quota_fact.quota_type = 'Territory'
		   AND date_dim.cal_date BETWEEN TO_DATE ('1/2009', 'MM/YYYY') AND LAST_DAY (TO_DATE ('12/2009', 'MM/YYYY'));

		v_status	:= get_validation_status (v_portal_quota_2009, v_dw_quota_2009);
		gm_cm_log_validation ('gm_cm_quota_2009'
							, 'gm_cm_quota_2009'
							, 'Total sales quota for 2009'
							, v_portal_quota_2009
							, v_dw_quota_2009
							, v_status
							, TRUNC (SYSDATE)
							 );
	END gm_cm_quota_2009;

	--
	PROCEDURE gm_cm_log_validation (
		p_txn_query    t980_load_dw_log.c980_txn_query%TYPE
	  , p_dw_query	   t980_load_dw_log.c980_dw_query%TYPE
	  , p_measure	   t980_load_dw_log.c980_measure%TYPE
	  , p_txn_val	   t980_load_dw_log.c980_txn_val%TYPE
	  , p_dw_val	   t980_load_dw_log.c980_dw_val%TYPE
	  , p_status	   t980_load_dw_log.c980_status%TYPE
	  , p_check_date   t980_load_dw_log.c980_check_date%TYPE
	)
	AS
	BEGIN
		INSERT INTO t980_load_dw_log
					(c980_load_dw_log_id, c980_txn_query, c980_dw_query, c980_measure, c980_txn_val, c980_dw_val
				   , c980_status, c980_check_date
					)
			 VALUES (s980_load_dw_log.NEXTVAL, p_txn_query, p_dw_query, p_measure, p_txn_val, p_dw_val
				   , p_status, p_check_date
					);

		COMMIT;
	END gm_cm_log_validation;

	/*
	* Function to get the Order Planning Group Name for a part
	*/
	FUNCTION get_validation_status (
		p_val_1   NUMBER
	  , p_val_2   NUMBER
	)
		RETURN VARCHAR2
	IS
		v_status	   VARCHAR2 (20) := 'FAILED';
	BEGIN
		IF p_val_1 <> p_val_2
		THEN
			v_status	:= 'FAILED';
		ELSIF p_val_1 = p_val_2
		THEN
			v_status	:= 'SUCCESS';
		END IF;

		RETURN v_status;
	END get_validation_status;
END gm_dw_pkg_cm_data_validation;
/
