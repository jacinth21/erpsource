CREATE OR REPLACE FORCE VIEW GLOBUS_BI_REALTIME.SET_DIM
AS
  (SELECT t207.c207_set_id set_key_id
  , t207.c207_set_id set_id
  , t207.c207_set_nm set_name
  , t207.c207_set_desc set_description
  , get_code_name(t207.c207_type) TYPE
  , t207.c202_project_id project_id
  , get_code_name(t207.c207_category) set_category
  , get_code_name(t207.c901_set_grp_type) set_group_type
  , t207.c901_set_grp_type set_group_type_Id
  , get_code_name(t207.c901_cons_rpt_id) consignment_report_id
  , get_code_name(t207.c901_minset_logic) minset_logic
  , get_code_name(t207.c901_status_id) status
  , t207.c207_rev_lvl revision_level
  FROM t207_set_master t207
  WHERE t207.c207_void_fl   IS NULL
  AND t207.c207_obsolete_fl IS NULL
  UNION
  SELECT '-1' set_key_id
  , '-1' set_id
  , 'Unknown' set_name
  , 'Unknown' set_description
  , 'Unknown' TYPE
  , '-1' project_id
  , 'Unknown' set_category
  , 'Unknown' set_group_type
  , -1 set_group_type_Id
  , 'Unknown' consignment_report_id
  , 'Unknown' minset_logic
  , 'Unknown' status
  , 'Unknown' revision_level
  FROM Dual
  );
