CREATE OR REPLACE VIEW globus_bi_realtime.SALES_FACT
AS 
(
SELECT Order_Dim.order_key_id ,
        Order_Dim.order_id ,
        gm_dw_pkg_sm_salesfact.get_do_id(Order_Dim.order_id) do_id ,
        Item_Order_Dim.item_order_key_id ,
        Item_Order_Dim.item_order_id ,
        Order_Dim.account_key_id ,
        Order_Dim.account_id ,
        Order_Dim.sales_rep_key_id ,
        Order_Dim.sales_rep_id ,
        Item_Order_Dim.part_id part_key_id ,
        Item_Order_Dim.part_id ,
        Set_Dim.set_key_id system_key_id ,
        Set_Dim.set_key_id system_id ,
        gm_dw_pkg_pd_partdata.get_main_set_id_for_part(Item_Order_Dim.part_id, Set_Dim.set_key_id) set_key_id ,
        gm_dw_pkg_pd_partdata.get_main_set_id_for_part(Item_Order_Dim.part_id, Set_Dim.set_key_id) set_id ,
        date_dim.date_key_id date_key_id ,
		To_Number(DECODE(order_type_Id, 2524, decode(order_dim.company_key_id,1026,Item_Order_Dim.ITEM_QTY,NULL), Item_Order_Dim.ITEM_QTY)) item_qty ,
        TRUNC(Order_Dim.order_dt) order_dt ,
        Item_Order_Dim.item_price ,
        Item_Order_Dim.Ext_Item_Price ,
	Item_Order_Dim.unit_price,
        Item_Order_Dim.item_price     * Item_Order_Dim.item_qty item_sales ,
        Item_Order_Dim.Ext_Item_Price * Item_Order_Dim.item_qty EXT_item_sales ,
        NVL(Item_Order_Dim.ext_vat_rate,0) EXT_VAT_RATE ,
        Item_Order_Dim.Ext_Item_Price * Item_Order_Dim.item_qty +(Item_Order_Dim.Ext_Item_Price * Item_Order_Dim.item_qty * (NVL(Item_Order_Dim.ext_vat_rate,0)/100)) EXT_item_sales_WithVAT ,
		Item_Order_Dim.Ext_Item_Price * Item_Order_Dim.item_qty +(Item_Order_Dim.Ext_Item_Price * Item_Order_Dim.item_qty * (NVL(Item_Order_Dim.ext_gst_rate,0)/100)) EXT_item_sales_WithGST ,
        Order_Dim.status ,
        Order_Dim.Order_Type_Id order_type
        --, Order_Dim.c501_total_cost total_sales -- not required
        ,
        Order_Dim.ship_cost ,
        Order_dim.Ext_Ship_cost ,
        GET_AC_COGS_VALUE(Item_Order_Dim.part_id,4900) COGS_Cost ,
        To_Number(GET_PART_PRICE(Set_Part_Dim.PART_NUMBER_ID,'L')) List_Price
        --, DECODE(gm_dw_pkg_sm_salesfact.get_first_order_for_acc(Order_Dim.c704_account_id), Order_Dim.c501_order_id, 'Y','N') first_sale_flag
    FROM Order_dim Order_dim ,
        Item_Order_Dim Item_Order_Dim ,
        Set_Dim Set_Dim ,
        SET_PART_DIM Set_Part_Dim ,
        date_dim date_dim		
    WHERE Item_Order_Dim.order_id   = Order_Dim.order_id
    AND Set_Dim.set_id              = Set_Part_Dim.SET_ID
    AND Set_Part_Dim.PART_NUMBER_ID = Item_Order_Dim.part_id
        --AND c207_void_fl IS NULL
        --AND Order_Dim.c501_void_fl IS NULL
        --AND Order_Dim.c501_delete_fl IS NULL   All void fl validated in Dim
        --AND Item_Order_Dim.c502_void_fl IS NULL
        --AND Item_Order_Dim.c502_delete_fl IS NULL
    AND Set_Dim.set_group_type_Id = 1600
        --AND NVL (Order_Dim.order_type, -9999) NOT IN ('2533','2518','2519')
    AND date_dim.cal_date   = TRUNC(Order_Dim.Order_Dt)
	--    AND Order_Dim.order_dt >= TRUNC(SYSDATE, 'MONTH')) -- Order_Dim already has a 2 month filter
);