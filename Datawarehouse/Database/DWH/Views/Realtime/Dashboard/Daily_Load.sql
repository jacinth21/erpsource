--@"D:\Database\DW_BI\MaterializedViews\Daily_Load.sql"

spool D:\DataBackup\Log\Daily_Load.log
set head off feed off verify off
set linesize 5000;
define source_schema=GLOBUS_DW

@"D:\Database\DW_BI\scripts\set_mapping.sql";
@"D:\Database\DW_BI\scripts\SystemName_Special_Char_Cleanup.sql";

@"D:\Database\DW_BI\packages\gm_dw_pkg_sm_salesfact.pkg";
@"D:\Database\DW_BI\packages\gm_dw_pkg_sm_salesfact.bdy";
@"D:\Database\DW_BI\packages\gm_dw_pkg_pd_partdata.pkg";
@"D:\Database\DW_BI\packages\gm_dw_pkg_pd_partdata.bdy";
@"D:\Database\DW_BI\packages\GET_CS_REP_LOANER_BILL_NM_ADD.fnc";

@"D:\Database\DW_BI\MaterializedViews\Drop_fact_tables.sql";

@"D:\Database\DW_BI\MaterializedViews\DATE_DIM.SQL";
@"D:\Database\DW_BI\MaterializedViews\ins_date_dim.sql";
@"D:\Database\DW_BI\MaterializedViews\YEAR_DIM.sql";
@"D:\Database\DW_BI\MaterializedViews\QUARTER_DIM.sql";
@"D:\Database\DW_BI\MaterializedViews\MONTH_DIM.sql";
@"D:\Database\DW_BI\MaterializedViews\WEEK_DIM.sql";

@"D:\Database\DW_BI\MaterializedViews\ACCOUNT_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\ORDER_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\ITEM_ORDER_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\SET_DIM.VW";
@"D:\Database\DW_BI\MaterializedViews\PART_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\SET_PART_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\SYSTEM_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\LOANER_DIM.vw";

@"D:\Database\DW_BI\MaterializedViews\SALES_REP_DIM.VW";
@"D:\Database\DW_BI\MaterializedViews\DISTRIBUTOR_DIM.VW";

@"D:\Database\DW_BI\MaterializedViews\SALES_TERRITORY_DIM.VW";
@"D:\Database\DW_BI\MaterializedViews\SALES_REGION_DIM.VW";
@"D:\Database\DW_BI\MaterializedViews\SALES_ZONE_DIM.VW";

@"D:\Database\DW_BI\MaterializedViews\SYSTEM_SET_HIERARCHY_TABLE.sql";
@"D:\Database\DW_BI\packages\gm_pkg_system_set_hierarchy.pkg";
@"D:\Database\DW_BI\packages\gm_pkg_system_set_hierarchy.bdy";
exec gm_pkg_system_set_hierarchy.load_set_hierarchy;

@"D:\Database\DW_BI\MaterializedViews\SALES_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\DAILY_SALES_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\UPD_DATE_DIM_FOR_SALES.sql";
@"D:\Database\DW_BI\MaterializedViews\QUOTA_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\FIELD_CONSIGN_INVENTORY_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\LOANER_INVENTORY_FACT.vw";

@"D:\Database\DW_BI\MaterializedViews\TOP_N_ACCOUNTS_DAILY.vw";
@"D:\Database\DW_BI\MaterializedViews\TOP_N_DIRECT_SALES_REP_DAILY.vw";
@"D:\Database\DW_BI\MaterializedViews\TOP_N_DISTRIBUTORS_DAILY.vw";
@"D:\Database\DW_BI\MaterializedViews\TOP_N_SYSTEMS_DAILY.vw";

@"D:\Database\DW_BI\MaterializedViews\TOP_N_ACCOUNTS_WEEKLY.vw";
@"D:\Database\DW_BI\MaterializedViews\TOP_N_DIRECT_SALES_REP_WEEKLY.vw";
@"D:\Database\DW_BI\MaterializedViews\TOP_N_DISTRIBUTORS_WEEKLY.vw";
@"D:\Database\DW_BI\MaterializedViews\TOP_N_SYSTEMS_WEEKLY.vw";

@"D:\Database\DW_BI\MaterializedViews\TOP_N_ACCOUNTS_MONTHLY.vw";
@"D:\Database\DW_BI\MaterializedViews\TOP_N_DIRECT_SALES_REP_MONTHLY.vw";
@"D:\Database\DW_BI\MaterializedViews\TOP_N_DISTRIBUTORS_MONTHLY.vw";
@"D:\Database\DW_BI\MaterializedViews\TOP_N_SYSTEMS_MONTHLY.vw";

@"D:\Database\DW_BI\MaterializedViews\TOP_N_ACCOUNTS_QUARTERLY.vw";
@"D:\Database\DW_BI\MaterializedViews\TOP_N_DIRECT_SALES_REP_QRTLY.vw";
@"D:\Database\DW_BI\MaterializedViews\TOP_N_DISTRIBUTORS_QUARTERLY.vw";
@"D:\Database\DW_BI\MaterializedViews\TOP_N_SYSTEMS_QUARTERLY.vw";

@"D:\Database\DW_BI\MaterializedViews\TOP_N_ACCOUNTS_YEARLY.vw";
@"D:\Database\DW_BI\MaterializedViews\TOP_N_DIRECT_SALES_REP_YEARLY.vw";
@"D:\Database\DW_BI\MaterializedViews\TOP_N_DISTRIBUTORS_YEARLY.vw";
@"D:\Database\DW_BI\MaterializedViews\TOP_N_SYSTEMS_YEARLY.vw";
@"D:\Database\DW_BI\MaterializedViews\CURRENT_INFO.sql";

@"D:\Database\DW_BI\scripts\T980_LOAD_DW_LOG.sql";
@"D:\Database\DW_BI\packages\gm_dw_pkg_cm_data_validation.pkg";
@"D:\Database\DW_BI\packages\gm_dw_pkg_cm_data_validation.bdy";

exec gm_dw_pkg_cm_data_validation.gm_cm_validate_main;
commit;

@"D:\Database\DW_BI\MaterializedViews\ANSWER_GROUP_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\STUDY_PERIOD_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\STUDY_FORM_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\SURGEON_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\FORM_MASTER_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\CLINICAL_STUDY_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\QUESTION_MASTER_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\PATIENT_MASTER_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\PATIENT_ANSWER_LIST_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\PATIENT_DETAILS_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\FORM_QUESTION_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\STUDY_SITE_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\ANSWER_LIST_DIM.vw";
@"D:\Database\DW_BI\MaterializedViews\CLINICAL_STUDY_PATIENT_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\PATIENT_STUDY_QUALIFIC_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\PATIENT_ADVERSE_EVENT_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\PATIENT_VAS_SCORE_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\NECK_DISABILITY_INDEX_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\PATIENT_PREOPERATIVE_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\PATIENT_HEALTH_STATUS_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\NEUROLOGICAL_STATUS_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\PATIENT_RADIOLOGIC_ASSESS_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\PATIENT_QUALIFICATION_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\OSWESTRY_DISABILITY_INDEX_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\ZURICH_CLAUDICATION_QUEST_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\POST_OPERAT_PATIENT_SURVEY_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\CALCULATED_SCORES_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\PATIENT_OPERATIVE_DATA_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\HOSPITAL_DISCHARGE_FACT.vw";
@"D:\Database\DW_BI\MaterializedViews\PATIENT_PREOPERAT_SURVEY_FACT.vw";


spool off;
exit;