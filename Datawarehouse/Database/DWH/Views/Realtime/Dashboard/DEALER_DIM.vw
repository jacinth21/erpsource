/* View to fetch dealer related informations*/

CREATE OR REPLACE VIEW globus_bi_realtime.dealer_dim
AS
  (SELECT t101.c101_party_id dealer_key_id,
    t101.c101_party_id dealer_id,
    NVL (c101_party_nm_en,(c101_first_nm
    || ' '
    ||c101_last_nm)) dealer_name,
    c101_active_fl active_fl,
    t901_closing_dt.c901_code_nm closing_date
  FROM t101_party t101,
    t901_code_lookup t901,
    t101_party_invoice_details t101_inv,
    t901_code_lookup t901_closing_dt
  WHERE c101_delete_fl    IS NULL
  AND t101.c101_void_fl   IS NULL
  AND t101.c901_party_type =t901.c901_code_id (+)
  AND c901_party_type      =26230725 -- Type dealer
  AND t101.c101_party_id   =t101_inv.c101_party_id (+)
  AND C901_INVOICE_CLOSING_DATE=t901_closing_dt.c901_code_id (+)
  );