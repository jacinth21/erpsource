CREATE OR REPLACE VIEW globus_bi_realtime.distributor_dim
AS
  ( SELECT DISTINCT t701.c701_distributor_id distributor_key_id ,
    t701.c701_distributor_id distributor_id ,
    nvl(t701.c701_distributor_name_en,t701.c701_distributor_name) distributor_name ,
    t701.c701_region region_key_id ,
    t701.c701_contract_start_date contract_start_date ,
    t701.c701_contract_end_date contract_end_date ,
    c701_active_fl active_fl ,
    t901_region_name.c901_code_nm region_name ,
    v700.ad_id ad_key_id ,
    v700.ad_id ad_id ,
    v700.ad_name ad_name ,
    t701.c701_ship_add1 ship_address1 ,
    t701.c701_ship_add2 ship_address2 ,
    t701.c701_ship_zip_code ship_zip_code ,
    t701.c701_ship_city ship_city ,
    t901_ship_state.c901_code_nm ship_state ,
    t901_ship_country.c901_code_nm ship_country ,
    t901_type.c901_code_nm type ,
    c701_bill_add1
    || c701_bill_add2 billing_address ,
    c701_bill_city billing_city ,
    t901_billing_state.c901_code_nm billing_state ,
    t901_billing_country.c901_code_nm billing_country ,
    c1900_company_id company_key_id
  FROM t701_distributor t701 ,
    v700_territory_mapping_detail v700 ,
    t901_code_lookup t901_region_name ,
    t901_code_lookup t901_ship_state ,
    t901_code_lookup t901_ship_country ,
    t901_code_lookup t901_type ,
    t901_code_lookup t901_billing_state ,
    t901_code_lookup t901_billing_country
  WHERE t701.c701_distributor_id = v700.d_id (+)
  AND t701.c701_void_fl         IS NULL
  AND t701.c701_region           =t901_region_name.c901_code_id(+)
  AND t701.c701_ship_state       =t901_ship_state.c901_code_id(+)
  AND t701.c701_ship_country     =t901_ship_country.c901_code_id(+)
  AND t701.c901_distributor_type =t901_type.c901_code_id(+)
  AND t701.c701_bill_state       =t901_billing_state.c901_code_id(+)
  AND t701.c701_bill_country     =t901_billing_country.c901_code_id(+)
  );
