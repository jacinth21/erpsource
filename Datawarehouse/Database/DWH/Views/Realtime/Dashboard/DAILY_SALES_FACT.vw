/* Formatted on 2009/11/11 14:09 (Formatter Plus v4.8.0) */

CREATE OR REPLACE VIEW globus_bi_realtime.DAILY_SALES_FACT
AS 
(SELECT t501.c704_account_id account_key_id ,
        t501.c704_account_id account_id ,
        t501.c703_sales_rep_id sales_rep_key_id ,
        t501.c703_sales_rep_id sales_rep_id ,
        t207.c207_set_id system_key_id ,
        t207.c207_set_id system_id ,
        date_dim.date_key_id date_key_id ,
        SUM(DECODE(c901_order_type, 2524, NULL, t502.c502_item_qty)) item_qty ,
        SUM(t502.c502_item_price * t502.c502_item_qty) day_sales ,
        AVG(t502.c502_item_price) Avg_item_price ,
        AVG(t501.c501_ship_cost) avg_ship_cost ,
        AVG(GET_AC_COGS_VALUE(T502.C205_PART_NUMBER_ID,4900)) avg_COGS_Cost ,
        AVG(GET_PART_PRICE(t208.c205_part_number_id,'L')) Avg_List_Price
    FROM t501_order t501 ,
        t502_item_order t502 ,
        t207_set_master t207 ,
        t208_set_details t208 ,
        date_dim date_dim
    WHERE t502.c501_order_id                   = t501.c501_order_id
    AND t207.c207_set_id                       = t208.c207_set_id
    AND t208.c205_part_number_id               = t502.c205_part_number_id
    AND c207_void_fl                          IS NULL
    AND t501.c501_void_fl                     IS NULL
    AND t501.c501_delete_fl                   IS NULL
    AND t502.c502_void_fl                     IS NULL
    AND t502.c502_delete_fl                   IS NULL
    AND t207.c901_set_grp_type                 = 1600
    AND NVL (t501.c901_order_type, -9999) NOT IN ('2533','2518','2519')
    AND date_dim.cal_date                      = t501.c501_order_date
    AND TRUNC(t501.c501_order_date)            = TRUNC(SYSDATE)
    GROUP BY t501.c704_account_id ,
        t501.c703_sales_rep_id ,
        t207.c207_set_id ,
        date_dim.date_key_id
   );