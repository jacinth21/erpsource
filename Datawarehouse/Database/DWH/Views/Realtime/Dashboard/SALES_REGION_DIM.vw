CREATE OR REPLACE VIEW globus_bi_realtime.sales_region_dim
AS
  (SELECT DISTINCT region_id region_key_id ,
    region_id ,
    ad_id area_director_key_id ,
    ad_id area_director_id ,
    ad_name area_director_name ,
    region_name ,
    gp_id zone_key_id ,
    vp_id vice_president_key_id ,
    t101_ad_email_id.c101_email_id ad_email_id
  FROM v700_territory_mapping_detail,
    t101_user t101_ad_email_id
  WHERE ad_id =t101_ad_email_id.c101_user_id(+)
  );