CREATE OR REPLACE VIEW globus_bi_realtime.sales_rep_dim
AS
  (SELECT c703_sales_rep_id sales_rep_key_id ,
    c703_sales_rep_id sales_rep_id ,
    nvl(c703_sales_rep_name_en,c703_sales_rep_name) sales_rep_name ,
    c701_distributor_id distributor_key_id ,
    c701_distributor_id distributor_id ,
    c703_phone_number phone_number ,
    t901_rep_category.c901_code_nm rep_category ,
    c703_ship_add1 ship_add1 ,
    c703_ship_add2 ship_add2 ,
    c703_pager_number pager_number ,
    c703_email_id email_id ,
    c703_ship_city ship_city ,
    t901_ship_state.c901_code_nm ship_state ,
    t901_ship_country.c901_code_nm ship_country ,
    c703_ship_zip_code ship_zip_code ,
    c703_sales_rep_sh_name sales_rep_sh_name ,
    t703.c702_territory_id territory_key_id ,
    t703.c702_territory_id territory_id ,
    t702.c702_territory_name territory_name ,
    c703_active_fl active_fl ,
    c703_sales_fl sales_fl ,
    t901_designation.c901_code_nm designation ,
    c703_start_date start_date ,
    c703_end_date end_date ,
    NVL(c703_ext_ref_id,c703_sales_rep_id) ext_ref_id ,
    t703.c1900_company_id company_key_id ,
    c1910_division_id division_key_id
  FROM t703_sales_rep t703,
    t901_code_lookup t901_rep_category,
    t901_code_lookup t901_ship_country,
    t901_code_lookup t901_ship_state,
    t901_code_lookup t901_designation,
    t702_territory t702
  WHERE c703_void_fl       IS NULL
  AND c703_rep_category     =t901_rep_category.c901_code_id(+)
  AND c703_ship_state       =t901_ship_country.c901_code_id(+)
  AND c703_ship_country     =t901_ship_state.c901_code_id(+)
  AND c901_designation      =t901_designation.c901_code_id(+)
  AND t703.c702_territory_id=t702.c702_territory_id(+)
  );