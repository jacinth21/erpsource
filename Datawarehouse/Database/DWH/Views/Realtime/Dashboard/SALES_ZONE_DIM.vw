CREATE OR REPLACE VIEW globus_bi_realtime.sales_zone_dim
AS
  ( SELECT DISTINCT gp_id zone_key_id ,
    gp_name zone_name ,
    vp_id vice_president_key_id ,
    vp_id vice_president_id ,
    vp_name vice_president_name ,
    divid division_key_id ,
    t101_vp_email_id.c101_email_id vp_email_id ,
    t710.zone_company_id company_key_id ,
    t901_company_name.c901_code_nm company_name
  FROM v700_territory_mapping_detail,
    ( SELECT DISTINCT c901_zone_id zone_key_id ,
      c901_company_id zone_company_id
    FROM t710_sales_hierarchy
    ) t710,
    t101_user t101_vp_email_id,
    t901_code_lookup t901_company_name
  WHERE t710.zone_key_id  = v700_territory_mapping_detail.gp_id
  AND vp_id               =t101_vp_email_id.c101_user_id(+)
  AND t710.zone_company_id=t901_company_name.c901_code_id(+)
  );
