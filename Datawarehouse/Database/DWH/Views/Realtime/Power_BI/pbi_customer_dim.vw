/*
File name: pbi_customer_dim.vw
Created By: RVeerasamy
PMT # :- PMT-35169
Refresh frequency : 1 hour
Description: Fetch all voided & non-voided account data. filtering only party type account & dealer
	party Type 4000877 - Account 
  party Type 26230725 - Dealer
*/

DROP MATERIALIZED VIEW pbi_customer_dim;
CREATE MATERIALIZED VIEW pbi_customer_dim 
NOLOGGING
REFRESH COMPLETE START WITH (SYSDATE) NEXT  (SYSDATE+60/1440) WITH ROWID
AS
SELECT t101.c101_party_id parent_acc_id, NVL (t101.c101_party_nm_en, t101.c101_party_nm) parent_acc_name
     , NVL (NVL (t101.c101_party_nm_en, t101.c101_party_nm) , NVL (t704.c704_account_nm_en, t704.c704_account_nm)) parrent_account
     , dealer.c101_party_id dealer_id
     , NVL (dealer.c101_party_nm_en, dealer.c101_party_nm) dealer_name
     , to_number (t704.c704_account_id) account_id
     , NVL (t704.c704_account_nm_en, t704.c704_account_nm) || '' account_name
     , t704.c703_sales_rep_id sales_rep_id
     , t704.c901_account_type account_type_id
     , t704.c1900_company_id company_id
     , t704.c901_company_id division_id
     , t704.c704_active_fl active_fl
     , t901_contract.c901_code_nm contract
     , t901_state.c901_code_nm state
     , t901_country.c901_code_nm country
     , t901_country.c901_code_nm || ',' || c704_bill_city city  
     , c704_ship_city ship_city
     , t901_ship_state.c901_code_nm ship_state
     , t901_currency.c901_code_nm currency
     , c704_credit_rating credit_rating
     , t901_payment_terms.c901_code_nm payment_terms
     , t901_payment_terms.c902_code_nm_alt term_days
     , to_number(v700.ad_id) ad_id
     , to_number(v700.vp_id) vp_id
     , to_number(v700.divid) market_id
     , to_number(v700.gp_id) zone_id
     , to_number(v700.region_id) region_id
     , to_number(v700.d_id) distributor_id
     , to_number(v700.ter_id) territory_id
  FROM t101_party t101
     , t704_account t704
     , v700_territory_mapping_detail v700
     , t901_code_lookup t901_state
     , t901_code_lookup t901_country
     , t901_code_lookup t901_ship_state
     , t101_party dealer
     , t901_code_lookup t901_currency
     , t901_code_lookup t901_payment_terms 
     , t704d_account_affln t704d
     , t901_code_lookup t901_contract
 WHERE t704.c101_party_id = t101.c101_party_id(+)
   AND t704.c704_account_id = v700.ac_id (+)
   AND t704.c704_account_id = t704d.c704_account_id(+)
   AND t101.c901_party_type(+) = 4000877   -- Party Type Account
   AND dealer.c901_party_type(+) = 26230725   -- Party Type Dealer
   AND t704.c704_void_fl  IS NULL
   AND t704d.c704d_void_fl IS NULL
   AND c704_bill_state = t901_state.c901_code_id(+)
   AND c704_bill_country = t901_country.c901_code_id(+)
   AND c704_ship_state = t901_ship_state.c901_code_id(+)
   AND t704.c101_dealer_id = dealer.c101_party_id(+)
   AND c901_currency = t901_currency.c901_code_id(+)
   AND c704_payment_terms = t901_payment_terms.c901_code_id(+)
   AND t704d.c901_contract = t901_contract.c901_code_id(+);