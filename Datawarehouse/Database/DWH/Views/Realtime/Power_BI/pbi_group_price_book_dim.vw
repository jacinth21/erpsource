/*
File name: pbi_group_price_book_dim.vw
Created By: RVeerasamy
PMT # :- PMT-43311
Description: Fetch all the group price book names based on account id
*/

DROP VIEW pbi_group_price_book_dim;
DROP MATERIALIZED VIEW pbi_group_price_book_dim;
CREATE MATERIALIZED VIEW pbi_group_price_book_dim
NOLOGGING
REFRESH COMPLETE START WITH (SYSDATE) NEXT  (SYSDATE+60/1440) WITH ROWID
AS
   SELECT to_number (t740.c704_account_id) account_id
     , to_number (t101.c101_party_id) gpb_id
     , DECODE ('103097','103097',NVL(t101.c101_party_nm_en,DECODE(t101.c101_party_nm, NULL, t101.c101_first_nm || '' || t101.c101_last_nm || '' || t101.c101_middle_initial, t101.c101_party_nm)), 
         DECODE(t101.c101_party_nm, NULL, t101.c101_first_nm || '' || t101.c101_last_nm || '' || t101.c101_middle_initial, t101.c101_party_nm)) group_price_book
   FROM   t740_gpo_account_mapping  t740
     , t101_party t101
   WHERE  t740.c101_party_id  = t101.c101_party_id
   AND t101.c901_party_type = 7003
   AND t101.c101_void_fl IS NULL
   AND t740.C740_void_fl IS NULL;
   
   
