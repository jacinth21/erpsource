/*
File name: pbi_distributor_dim.vw
Created By: YKannan
Description: Fetch non voided distributor data
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_distributor_dim.vw";
*/

DROP VIEW pbi_distributor_dim;
DROP MATERIALIZED VIEW pbi_distributor_dim;
CREATE MATERIALIZED VIEW pbi_distributor_dim 
NOLOGGING
REFRESH COMPLETE START WITH (SYSDATE) NEXT  (SYSDATE+60/1440) WITH ROWID
AS
  (SELECT to_number(t701.c701_distributor_id) distributor_id ,
	NVL(t701.c701_distributor_name_en, t701.c701_distributor_name) distributor_name, --For Japan use English name
	c901_code_nm distributor_type , 
	to_number(t701.c701_region) region_id ,
	c701_active_fl active_fl ,
	c1900_company_id company_id
  FROM t701_distributor t701 , t901_code_lookup t901_dist_type
  WHERE t701.c901_distributor_type = t901_dist_type.c901_code_id (+)
  AND t701.c701_void_fl IS NULL
  );
  
CREATE INDEX idx_distributor_id ON pbi_distributor_dim (distributor_id);
CREATE INDEX idx_dist_region_id ON pbi_distributor_dim (region_id);
CREATE INDEX idx_dist_company_id ON pbi_distributor_dim (company_id);
