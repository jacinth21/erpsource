/*
File name: pbi_realtime_quota_fact.vw
Created By: YKannan
Description: Fetch current month quota amount for each sales rep
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_realtime_quota_fact.vw";
*/
DROP VIEW pbi_realtime_quota_fact;
DROP MATERIALIZED VIEW pbi_realtime_quota_fact;
CREATE MATERIALIZED VIEW  pbi_realtime_quota_fact 
NOLOGGING
REFRESH COMPLETE START WITH (SYSDATE) NEXT  (SYSDATE+60/1440) WITH ROWID
AS
   (SELECT 
      to_number(t709.c703_sales_rep_id) sales_rep_id,
      to_number(t710.c901_division_id) market_id,
      to_number(t710.c901_company_id) division_id,
      to_number(t710.c901_zone_id) zone_id,
      to_number(t710.c901_area_id) region_id,
      to_number(t701.c701_distributor_id) distributor_id,
      t709.c709_start_date start_date,
      t709.c709_end_date end_date,
      (nvl(t709.c709_quota_breakup_amt_base, 0)) base_amount ,
      (nvl(t709.c709_quota_breakup_amt, 0)) amount 
	FROM t709_quota_breakup t709, 
	  t710_sales_hierarchy t710, 
	  t701_distributor t701, 
	  t703_sales_rep t703
    WHERE t709.c703_sales_rep_id = t703.c703_sales_rep_id
      AND t703.c701_distributor_id = t701.c701_distributor_id
      AND t710.c901_area_id = t701.c701_region
      AND t709.c901_type       = 20750
      AND c709_start_date = trunc(SYSDATE, 'MONTH')
      AND t710.c710_active_fl = 'Y'
	  AND t701.c701_void_fl       IS NULL
      AND t703.c703_void_fl       IS NULL
   );
 
CREATE INDEX idx_rt_Quota_rep_id ON pbi_realtime_quota_fact (sales_rep_id);
CREATE INDEX idx_rt_Quota_market_id ON pbi_realtime_quota_fact (market_id);
CREATE INDEX idx_rt_Quota_division_id ON pbi_realtime_quota_fact (division_id);
CREATE INDEX idx_rt_Quota_zone_id ON pbi_realtime_quota_fact (zone_id);
CREATE INDEX idx_rt_Quota_region_id ON pbi_realtime_quota_fact (region_id);
CREATE INDEX idx_rt_Quota_distributor_id ON pbi_realtime_quota_fact (distributor_id);
CREATE INDEX idx_rt_Quota_start_date ON pbi_realtime_quota_fact (start_date);
CREATE INDEX idx_rt_Quota_end_date ON pbi_realtime_quota_fact (end_date);