/*
File name: pbi_sales_rep_dim.vw
Created By: YKannan
Description: Fetch non voided sales rep data along with VP id, email, region id and region name
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_sales_rep_dim.vw";
*/

DROP VIEW pbi_sales_rep_dim; 
DROP MATERIALIZED VIEW pbi_sales_rep_dim; 
CREATE MATERIALIZED VIEW pbi_sales_rep_dim 
NOLOGGING
REFRESH COMPLETE START WITH (SYSDATE) NEXT  (SYSDATE+60/1440) WITH ROWID
AS
  (SELECT to_number(t703.c703_sales_rep_id) sales_rep_id,
    NVL(t703.c703_sales_rep_name_en, t703.c703_sales_rep_name) sales_rep_name, --For Japan use English name
    to_number(t703.c701_distributor_id) distributor_id ,
    t703.c702_territory_id territory_id ,
    t703.c703_active_fl active_fl ,
    t703.c703_sales_fl sales_fl ,
    t703.c1900_company_id company_id,
    dist_vp.vp_id vp_id,
    dist_vp.vp_name,
    Upper(dist_vp.c101_email_id) vp_email ,
    ad.ad_id,
    ad.ad_name,
    ad.ad_email,
    dist_vp.region_id region_id,
    dist_vp.region_name region_name
  FROM t703_sales_rep t703,
    (SELECT t708.c101_user_id vp_id,
      t101.c101_User_f_name
      || ' '
      || t101.c101_User_l_name vp_name,
      t701.c701_distributor_id dist_id,
      t101.c101_email_id,
      t708.c901_region_id region_id,
      t901_region.c901_code_nm region_name
    FROM t701_distributor t701,
      t708_region_asd_mapping t708,
      t101_user t101 ,
      t901_code_lookup t901_region
    WHERE t708.c901_region_id    = t701.c701_region
    AND t101.c101_user_id        = t708.c101_user_id
    AND t708.c901_region_id      = t901_region.c901_code_id (+)
    AND c708_delete_fl          IS NULL
    AND t708.c901_user_role_type =8001 --Role Type VP
    ) dist_vp,
    (SELECT t708.c101_user_id ad_id,
      t101.c101_User_f_name
      || ' '
      || t101.c101_User_l_name ad_name,
      t701.c701_distributor_id dist_id,
      Upper(t101.c101_email_id) ad_email
    FROM t701_distributor t701,
      t708_region_asd_mapping t708,
      t101_user t101 ,
      t901_code_lookup t901_region
    WHERE t708.c901_region_id    = t701.c701_region
    AND t101.c101_user_id        = t708.c101_user_id
    AND t708.c901_region_id      = t901_region.c901_code_id (+)
    AND c708_delete_fl          IS NULL
    AND t708.c901_user_role_type =8000 --Role Type AD
    ) ad
  WHERE t703.c701_distributor_id = dist_vp.dist_id (+)
  AND t703.c701_distributor_id   = ad.dist_id (+)
  AND t703.c703_void_fl         IS NULL
  );


CREATE INDEX idx_rep_distributor_id ON pbi_sales_rep_dim (distributor_id);
CREATE INDEX idx_sales_rep_id ON pbi_sales_rep_dim (sales_rep_id);
CREATE INDEX idx_rep_territory_id ON pbi_sales_rep_dim (territory_id);
CREATE INDEX idx_rep_company_id ON pbi_sales_rep_dim (company_id);
CREATE INDEX idx_rep_vp_id ON pbi_sales_rep_dim (vp_id);
CREATE INDEX idx_rep_ad_id ON pbi_sales_rep_dim (ad_id);
CREATE INDEX idx_rep_region_id ON pbi_sales_rep_dim (region_id);
