/*
File name: pbi_division_dim.vw
Created By: YKannan
Description: Fetch non voided Division data
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_division_dim.vw";
*/

DROP VIEW pbi_division_dim;  
DROP MATERIALIZED VIEW pbi_division_dim ;
CREATE MATERIALIZED VIEW pbi_division_dim 
NOLOGGING
REFRESH COMPLETE START WITH (SYSDATE) NEXT  (SYSDATE+60/1440) WITH ROWID
AS
  (SELECT to_number(c901_code_id) division_id,
    nvl(c902_code_nm_alt,c901_code_nm) division_name,
    c901_code_seq_no seq_number	
  FROM t901_code_lookup
  WHERE c901_code_grp='COMP'
    AND c901_void_fl is null
    AND c901_active_fl =1
    AND c901_code_id <>100803 -- Division "All"
  );

CREATE INDEX idx_division_id ON pbi_division_dim (division_id);
