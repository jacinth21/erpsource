/*
File name: pbi_account_attribute_dim.vw
Created By: RVeerasamy
PMT # :- PMT-35169
Refresh frequency : 1 hour
Description: Fetch all the account related details including collector Name, credit Limit and type;
*/

DROP MATERIALIZED VIEW pbi_account_attribute_dim;
CREATE MATERIALIZED VIEW pbi_account_attribute_dim 
NOLOGGING
REFRESH COMPLETE START WITH (SYSDATE) NEXT  (SYSDATE+60/1440) WITH ROWID
AS
SELECT account_id,
  creditlimit,
  get_code_name(collectorid) COLLECTOR_NAME,
  get_code_name(credittype) CREDITTYPE
FROM 
  (SELECT c704_account_id ACCOUNT_ID,
    MAX(DECODE (c901_attribute_type, 103050,c704a_attribute_value)) COLLECTORID,
    MAX (DECODE (c901_attribute_type, '101182', c704a_attribute_value)) CREDITLIMIT,
    MAX (DECODE (c901_attribute_type, '101184' , c704a_attribute_value)) credittype
  FROM t704a_account_attribute
  WHERE c704a_void_fl IS NULL
  GROUP BY c704_account_id
  );