/*
File name: pbi_account_dim.vw
Created By: YKannan
Description: Fetch all non voided account data with GPO and IDN. (c1900_company_id=1018 OR c901_account_type =70114) -> filters for ICS accounts
	Account Type 70114 - InterCompany Sale(ICS) 
	Company id 1018 - Globus Medical International HQ
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_account_dim.vw";
*/

DROP MATERIALIZED VIEW pbi_account_dim;
CREATE MATERIALIZED VIEW pbi_account_dim 
NOLOGGING
REFRESH COMPLETE START WITH (SYSDATE) NEXT  (SYSDATE+60/1440) WITH ROWID
AS
(SELECT t101.c101_void_fl,
  t101.c901_party_type Party_type_id,
  get_code_name(t101.c901_party_type) Party_type,
  t101.c101_party_id Parent_acc_id,
  NVL(t101.c101_party_nm_en,t101.c101_party_nm) Parent_acc_name,
  dealer.c101_party_id dealer_id,
  NVL(dealer.c101_party_nm_en,dealer.c101_party_nm) dealer_name,
  to_number(t704.c704_account_id) account_id ,
  NVL(t704.c704_account_nm_en, t704.c704_account_nm)
  || '' account_name,
  t704.c703_sales_rep_id sales_rep_id ,
  t704.c901_account_type account_type_id ,
  t704.c1900_company_id company_id ,
  t704.c901_company_id division_id ,
  t704.c704_active_fl active_fl,
  t704.c704_bill_zip_code zip_code,
  NVL(t704.c704_bill_add1, t704.c704_bill_add2) address,
  t901_country.c901_code_nm
  ||','
  ||c704_bill_city city,
  t704.c704_bill_city bill_city,
  t901_state.c901_code_nm state,
  t901_country.c901_code_nm country,
  t704.c704_bill_latitude lattitude,
  t704.c704_bill_longitude longitude,
  t704.c704_ship_zip_code ship_zip_code,
  NVL(t704.c704_ship_add1, t704.c704_ship_add2) ship_address,
  t704.c704_ship_city ship_city,
  t901_ship_state.c901_code_nm ship_state,
  t901_ship_country.c901_code_nm ship_country,
  t704.c704_ship_latitude ship_lattitude,
  t704.c704_ship_longitude ship_longitude,
  t901_currency.c901_code_id currency_id,
  t901_currency.c901_code_nm currency,
  affln.c101_gpo gpo_id,--,get_party_name(c101_gpo),
  affln.c101_idn idn_id, --, get_party_name(affln.c101_idn)
  affln.c101_health_system health_system_id,
  affln.c101_rpc rpc_id
FROM t101_party t101,
  t704_account t704,
  t704d_account_affln Affln,
  t101_party dealer,
  (SELECT c901_code_id,
    c901_code_nm
  FROM t901_code_lookup
  WHERE c901_code_grp='CURRN'
  AND c901_void_fl  IS NULL
  ) t901_currency,
  (SELECT c901_code_id,
    c901_code_nm
  FROM t901_code_lookup
  WHERE c901_code_grp='STATE'
  AND c901_void_fl  IS NULL
  ) t901_state,
  (SELECT c901_code_id,
    c901_code_nm
  FROM t901_code_lookup
  WHERE c901_code_grp='CNTY'
  AND c901_void_fl  IS NULL
  ) t901_country,
  (SELECT c901_code_id,
    c901_code_nm
  FROM t901_code_lookup
  WHERE c901_code_grp='STATE'
  AND c901_void_fl  IS NULL
  ) t901_ship_state,
  (SELECT c901_code_id,
    c901_code_nm
  FROM t901_code_lookup
  WHERE c901_code_grp='CNTY'
  AND c901_void_fl  IS NULL
  ) t901_ship_country
WHERE t704.c101_party_id          =t101.c101_party_id(+)
AND t704.c704_account_id          = affln.c704_account_id(+) -- For GPO and IDN
AND t101.c901_party_type (+)      =4000877                   -- Party Type Account
AND dealer.c901_party_type (+)    =26230725                  -- Party Type Dealer
AND t704.c704_void_fl            IS NULL
AND Affln.c704d_void_fl          IS NULL
AND t704.c101_dealer_id           =dealer.c101_party_id(+)
AND t901_currency.c901_code_id    =t704.c901_currency
AND t901_state.c901_code_id       =t704.c704_bill_state
AND t901_country.c901_code_id     =t704.c704_bill_country
AND t901_ship_state.c901_code_id  =t704.c704_ship_state
AND t901_ship_country.c901_code_id=t704.c704_ship_country
); 
    
CREATE INDEX idx_account_id ON pbi_account_dim (account_id);
CREATE INDEX idx_account_rep_id ON pbi_account_dim (sales_rep_id);
CREATE INDEX idx_account_division_id ON pbi_account_dim (division_id);
CREATE INDEX idx_account_dealer_id ON pbi_account_dim (dealer_id);
CREATE INDEX idx_account_company_id ON pbi_account_dim (company_id);
CREATE INDEX idx_account_parent_acc_id ON pbi_account_dim (parent_acc_id);
CREATE INDEX idx_acc_currency_id ON pbi_account_dim (currency_id);
CREATE INDEX idx_acc_gpo_id ON pbi_account_dim (gpo_id);
CREATE INDEX idx_acc_idn_id ON pbi_account_dim (idn_id);
CREATE INDEX idx_acc_health_system_id ON pbi_account_dim (health_system_id);
CREATE INDEX idx_acc_rpc_id ON pbi_account_dim (rpc_id);