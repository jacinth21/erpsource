/*
File name: pbi_mv_system_dim.vw
Created By: YKannan
Description: Get the system data in materialized view with fast refresh option to improve performance.
			 For this, need to create log on t207_set_master and t208_set_details for specific fields when those data changes materialized view will refresh automatically
			 Also create index on PART_ID to make fast data retrieval
			 COUNT(*) must always be present to guarantee all types of fast refresh. Otherwise, you may be limited to fast refresh after inserts only
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_mv_system_dim.vw";
*/

DROP materialized VIEW log ON t207_set_master;
DROP materialized VIEW log ON t208_set_details;
DROP materialized VIEW pbi_mv_system_dim;    
CREATE MATERIALIZED VIEW pbi_mv_system_dim REFRESH COMPLETE START WITH  (
    SYSDATE  )  NEXT (SYSDATE+240/1440) WITH ROWID
  AS    
    SELECT t207.c207_set_id System_Id, t207.c207_set_nm system_name, t208.c205_part_number_id Part_Id, COUNT (1) row_count
    FROM t208_set_details t208, t207_set_master t207
    WHERE t207.c207_set_id = t208.c207_set_id
    AND t207.c901_set_grp_type = 1600 --Sales Report Grp
    AND t208.c208_void_fl  IS NULL
    AND t207.c207_void_fl  IS NULL
    GROUP BY t207.c207_set_id , t208.c205_part_number_id, t207.c207_set_nm;
CREATE INDEX XIF1PBI_SYSTEM_PART ON pbi_mv_system_dim
  (
    PART_ID ASC
  );

