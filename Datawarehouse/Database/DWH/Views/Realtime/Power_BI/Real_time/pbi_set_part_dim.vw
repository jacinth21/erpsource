/*
File name: pbi_set_part_dim.vw
Created By: YKannan
Description: Fetch non voided set details data by joining system view for only Sales Report Group
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_set_part_dim.vw";
*/

CREATE OR REPLACE VIEW pbi_set_part_dim
  AS
  (SELECT c208_set_details_id set_details_id ,
    c207_set_id set_id ,
    c205_part_number_id part_number_id ,
    c208_set_qty set_part_qty
  FROM t208_set_details t208, pbi_system_dim pbi_system
  WHERE PBI_SYSTEM.SYSTEM_ID = T208.C207_SET_ID
    AND t208.c208_void_fl IS NULL
  );