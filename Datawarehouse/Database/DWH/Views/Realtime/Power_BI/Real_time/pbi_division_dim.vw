/*
File name: pbi_division_dim.vw
Created By: YKannan
Description: Fetch non voided Division data
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_division_dim.vw";
*/
CREATE OR REPLACE VIEW pbi_division_dim AS
  (SELECT to_number(c901_code_id) division_id,
    nvl(c902_code_nm_alt,c901_code_nm) division_name,
    c901_code_seq_no seq_number	
  FROM t901_code_lookup
  WHERE c901_code_grp='COMP'
    AND c901_void_fl is null
    AND c901_active_fl =1
  );