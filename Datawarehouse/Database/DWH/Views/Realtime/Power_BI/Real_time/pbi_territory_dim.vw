/*
File name: pbi_territory_dim.vw
Created By: YKannan
Description: Fetch non voided (not marked as deleted) territory data
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_territory_dim.vw";
*/

CREATE OR REPLACE VIEW pbi_territory_dim
AS
  (SELECT c702_territory_id territory_id,
    c702_territory_name territory_name,
    c901_territory_type territory_type,
    c1900_company_id company_id,
    c702_active_fl active_fl
    FROM t702_territory
  WHERE c702_delete_fl IS NULL
  );