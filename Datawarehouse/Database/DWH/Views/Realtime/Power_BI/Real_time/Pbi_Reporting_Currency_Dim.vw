/*
File name: PBI_REPORTING_CURRENCY_DIM.vw
Created By: YKannan
Description: Fetch Reporting currencies
Location: @"Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_currency_dim.vw";
*/


CREATE OR REPLACE VIEW PBI_REPORTING_CURRENCY_DIM AS 
  (select 'Base' REPORTING_CURRENCY  from dual
    union all
   select 'Original' REPORTING_CURRENCY from dual
  );
