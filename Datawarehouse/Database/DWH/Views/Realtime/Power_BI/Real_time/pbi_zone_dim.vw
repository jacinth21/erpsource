/*
File name: pbi_zone_dim.vw
Created By: YKannan
Description: Get the Zone master data from t901_code_lookup
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_zone_dim.vw";
*/

CREATE OR REPLACE VIEW pbi_zone_dim
AS
  SELECT to_number(c901_code_id) zone_id,
   nvl(c902_code_nm_alt,c901_code_nm) zone_name,
   c901_active_fl active_fl,
   c901_code_seq_no seq_number
  FROM t901_code_lookup
  WHERE c901_code_grp='ZONE'
    AND c901_void_fl  IS NULL
  ORDER BY c901_code_seq_no ;
  