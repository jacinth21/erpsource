/*
File name: pbi_realtime_month_dim.vw
Created By: YKannan
Description: Fetch current month total business days and sales  business days as of current date
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_realtime_month_dim.vw";
*/

CREATE OR REPLACE VIEW pbi_realtime_month_dim AS
(
	SELECT date_dim_all.month_nm_year month_year,
	  date_dim_all.business_days_this_month ,
	  date_dim_until.sales_business_days_this_month
	FROM
	  (SELECT year_month,
		month_nm_year,
		COUNT (globus_bi_realtime.date_dim.day_number) sales_business_days_this_month
	  FROM globus_bi_realtime.date_dim
	  WHERE cal_date <=
		(SELECT TRUNC (SYSDATE) FROM dual
		)
	  AND bus_day_ind = 'Y'
	  AND year_month  = TO_CHAR (SYSDATE, 'YYYY/MM')
	  GROUP BY year_month,
		month_nm_year
	  ) date_dim_until ,
	  (SELECT year_month,
		month_nm_year,
		COUNT (globus_bi_realtime.date_dim.day_number) business_days_this_month
	  FROM globus_bi_realtime.date_dim
	  WHERE bus_day_ind = 'Y'
	  AND year_month    = TO_CHAR (SYSDATE, 'YYYY/MM')
	  GROUP BY year_month,
		month_nm_year
	  ) date_dim_all
	WHERE date_dim_until.year_month = date_dim_all.year_month 
  );