/*
File name: pbi_sales_rep_dim.vw
Created By: YKannan
Description: Fetch non voided sales rep data
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_sales_rep_dim.vw";
*/

CREATE OR REPLACE VIEW pbi_sales_rep_dim
AS
  (SELECT to_number(c703_sales_rep_id) sales_rep_id,
    nvl(c703_sales_rep_name_en, c703_sales_rep_name) sales_rep_name, --For Japan use English name
    to_number(c701_distributor_id) distributor_id ,
    c702_territory_id territory_id ,
    c703_active_fl active_fl ,
    c703_sales_fl sales_fl ,
    c1900_company_id company_id
  FROM t703_sales_rep
  WHERE c703_void_fl IS NULL
);