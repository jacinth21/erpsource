/*
File name: pbi_part_dim.vw
Created By: YKannan
Description: Fetch non voided part data
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_part_dim.vw";
*/

CREATE OR REPLACE VIEW pbi_part_dim
AS
  (SELECT t205.c205_part_number_id part_id ,
    t205.c205_part_num_desc part_num_desc ,
    get_code_name(t205.c205_product_family) product_family ,
    get_code_name(t205.c205_product_class) product_class ,
    t205.c202_project_id project_id ,
    get_project_name(t205.c202_project_id) project_name ,
    get_code_name(c205_product_material) product_material
  FROM t205_part_number t205
  );