/*
File name: pbi_ics_account_dim.vw
Created By: YKannan
Description: Fetch non voided account data with filter : c1900_company_id=1018 (Globus Medical International HQ) OR c901_account_type   =70114 (InterCompany Sale(ICS))
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_ics_account_dim.vw";
*/

CREATE OR REPLACE VIEW pbi_ics_account_dim
AS
  (SELECT c704_account_id account_id ,
    nvl(c704_account_nm_en,c704_account_nm)  account_nm ,
    c703_sales_rep_id sales_rep_id ,
    c704_bill_add1 add1 ,
    c704_bill_name bill_name ,
    c704_bill_add2 add2 ,
    c704_bill_city city ,
    get_code_name(c704_bill_state) bill_state ,
    get_code_name(c704_bill_country) bill_country ,
    c704_bill_zip_code zip_code ,
    get_code_name(c704_payment_terms) payment_terms ,
    c704_inception_date inception_date ,
    c704_ship_name ship_name ,
    c704_active_fl active_fl ,
    c704_ship_add1 ship_add1 ,
    c704_ship_add2 ship_add2 ,
    c704_ship_city ship_city ,
    get_code_name(c704_ship_state) ship_state ,
    get_code_name(c704_ship_country) ship_country ,
    c704_ship_zip_code ship_zip_code ,
    c704_email_id email_id ,
    c704_price_cat_fl price_cat_fl ,
    c704_void_fl void_fl ,
    c901_company_id company_div_id ,
    get_code_name (c901_company_id) company_name ,
    c901_account_type account_type_id ,
    70114 account_type_id_fdsr ,
    get_code_name (c901_account_type) account_type ,
    t704.c1900_company_id company_id
  FROM t704_account t704
  WHERE (c1900_company_id=1018
  OR c901_account_type   =70114)
  AND c704_void_fl      IS NULL
  );