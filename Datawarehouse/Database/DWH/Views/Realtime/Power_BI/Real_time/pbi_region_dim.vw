/*
File name: pbi_region_dim.vw
Created By: YKannan
Description: Fetch non voided region master data from t901_code_lookup
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_region_dim.vw";
*/

CREATE OR REPLACE VIEW pbi_region_dim
AS
  (SELECT c901_code_id region_id,
    c901_code_nm region_name,
    c902_code_nm_alt region_alt_name,
    c901_active_fl active_fl
  FROM t901_code_lookup
  WHERE c901_code_grp='REGN'
  AND c901_void_fl is null
  );