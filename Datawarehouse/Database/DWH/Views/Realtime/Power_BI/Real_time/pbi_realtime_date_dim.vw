/*
File name: pbi_realtime_date_dim.vw
Created By: YKannan
Description: Fetch current month total business days and sales  business days as of current date
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_realtime_date_dim.vw";
*/

CREATE OR REPLACE VIEW pbi_realtime_date_dim
AS
  ( SELECT 
	  cal_date, 
      year_month,
      holiday_ind, 
      bus_day_ind,
      day_number,
      decode(cal_date,trunc(SYSDATE), 1,null) today
    FROM date_dim 
	WHERE year_month = to_char(sysdate,'yyyy/mm')
  );
