/*
File name: pbi_account_dim.vw
Created By: YKannan
Description: Fetch all non voided account data. (c1900_company_id=1018 OR c901_account_type =70114) -> filters for ICS accounts
	Account Type 70114 - InterCompany Sale(ICS) 
	Company id 1018 - Globus Medical International HQ
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_account_dim.vw";
*/


CREATE OR REPLACE VIEW pbi_account_dim AS
    (SELECT  t101.c101_party_id Parent_acc_id, 
        c101_party_nm Parent_acc_name,
        to_number(t704.c704_account_id) account_id ,
        nvl(t704.c704_account_nm_en, t704.c704_account_nm) || '' account_name,
        t704.c703_sales_rep_id sales_rep_id ,
        t704.c901_account_type account_type_id ,
        t704.c1900_company_id company_id ,
        t704.c901_company_id division_id ,
        t704.c704_active_fl active_fl, 
        t901_state.c901_code_nm state,
        t901_country.c901_code_nm Country,
        t901_country.c901_code_nm
        ||','
        ||c704_bill_city city,
        c704_bill_latitude lattitude,
        c704_bill_longitude longitude,
        c704_ship_city ship_city,
        t901_ship_state.c901_code_nm ship_state
    FROM t101_party t101,t704_account t704,t901_code_lookup t901_state,t901_code_lookup t901_country,t901_code_lookup t901_ship_state
    WHERE t704.c101_party_id=t101.c101_party_id(+)
    AND c901_party_type     (+)=4000877
    AND t704.c704_void_fl  IS NULL
    AND c704_bill_state=t901_state.c901_code_id(+)
    AND c704_bill_country=t901_country.c901_code_id(+)
    AND c704_ship_state=t901_ship_state.c901_code_id(+)
    );