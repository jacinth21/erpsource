/*
File name: pbi_system_dim.vw
Created By: YKannan
Description: Fetch non voided system data for only Sales Report Group
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_system_dim.vw";
*/

CREATE OR REPLACE VIEW pbi_system_dim AS
  (SELECT t207.c207_set_id system_id ,
      regexp_replace (t207.c207_set_nm, '.?trade;|.?reg;|.?\(.*\)') system_nm ,
      t207.c207_set_desc set_desc
    FROM t207_set_master t207
    WHERE t207.c901_set_grp_type = 1600 --Sales Report Grp
    AND t207.c207_void_fl       IS NULL
  );