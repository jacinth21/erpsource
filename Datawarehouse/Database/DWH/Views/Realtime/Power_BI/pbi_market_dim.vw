/*
File name: pbi_market_dim.vw
Created By: YKannan
Description: Get the market master data (US/OUS) from t901_code_lookup
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_market_dim.vw";
*/

DROP VIEW pbi_market_dim;
DROP MATERIALIZED VIEW pbi_market_dim;
CREATE MATERIALIZED VIEW  pbi_market_dim
NOLOGGING
REFRESH COMPLETE START WITH (SYSDATE) NEXT  (SYSDATE+60/1440) WITH ROWID
AS
  SELECT to_number(c901_code_id) market_id,
   nvl(c902_code_nm_alt,c901_code_nm) market_name,
   c901_active_fl active_fl,
   c901_code_seq_no seq_number
  FROM t901_code_lookup
  WHERE c901_code_grp='DIV'
    AND c901_void_fl  IS NULL
  ORDER BY c901_code_seq_no 
  ;

CREATE INDEX idx_market_id ON pbi_market_dim (market_id);
  