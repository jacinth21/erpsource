/*
File name: pbi_currency_dim.vw
Created By: YKannan
Description: Fetch all non voided Currencies with Active Flag
Location: @"Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_currency_dim.vw";
*/


DROP MATERIALIZED VIEW PBI_CURRENCY_DIM;
CREATE MATERIALIZED VIEW PBI_CURRENCY_DIM 
NOLOGGING
REFRESH COMPLETE START WITH (SYSDATE) NEXT  (SYSDATE+60/1440) WITH ROWID
AS
  (
  SELECT c901_code_id currency_id,
    c901_code_nm currency_name,
    c901_active_fl
  FROM t901_code_lookup
  WHERE c901_code_grp = 'CURRN'
  AND c901_void_fl   IS NULL
  );
  
create index idx_currency_id  ON PBI_CURRENCY_DIM (currency_id);
create index idx_currency_name  ON PBI_CURRENCY_DIM (currency_name);