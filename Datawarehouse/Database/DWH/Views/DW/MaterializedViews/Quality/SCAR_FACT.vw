-- @"c:\database\dw_bi\materializedviews\Quality\SCAR_FACT.vw"

DROP MATERIALIZED VIEW SCAR_FACT;

CREATE MATERIALIZED VIEW SCAR_FACT
NOCACHE
LOGGING
NOCOMPRESS
NOPARALLEL
REFRESH FORCE ON DEMAND
AS
(
SELECT scar_dim.scar_key_id, vendor_dim.vendor_key_id, date_dim.date_key_id
     , DECODE (scar_dim.status_id
             , '102780', GET_WEEKDAYSDIFF(TRUNC(scar_dim.assigned_date),TRUNC(scar_dim.last_updated_date)) 
             , '102781', GET_WEEKDAYSDIFF(TRUNC(scar_dim.assigned_date),TRUNC(scar_dim.close_date)) 
             , NULL
              ) daystoclose
     , DECODE(STATUS_ID
     ,'102779',GET_WEEKDAYSDIFF(TRUNC(ASSIGNED_DATE),TRUNC(Sysdate))
     ,'102780',GET_WEEKDAYSDIFF(TRUNC(ASSIGNED_DATE),TRUNC(LAST_UPDATED_DATE))
     ,'102781',GET_WEEKDAYSDIFF(TRUNC(ASSIGNED_DATE),TRUNC(CLOSE_DATE))
     ,0) aging
  FROM scar_dim, date_dim, vendor_dim
 WHERE TRUNC (scar_dim.assigned_date) = date_dim.cal_date(+) AND scar_dim.ASSIGNED_TO_VENDOR_KEY_ID = vendor_dim.vendor_key_id(+)
);
