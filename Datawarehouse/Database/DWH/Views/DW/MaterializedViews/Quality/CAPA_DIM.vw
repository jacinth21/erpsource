-- @"c:\database\dw_bi\materializedviews\Quality\CAPA_DIM.vw"

DROP MATERIALIZED VIEW CAPA_DIM;

CREATE MATERIALIZED VIEW CAPA_DIM
AS
(
SELECT c3150_capa_id capa_key_id
, c3150_capa_id capa_id
, c901_type capa_type_id
, get_code_name (c901_type) capa_type
, c3150_ncmr_com_ref ncmr_references
, get_user_name (c101_issued_by) issued_by
, get_user_name (c101_requested_by) requested_by
, c3150_project_ref c3150_project_ref
, c901_status status_id    
, get_code_name (c901_status) status
, c3150_capa_ref capa_reference
, c3150_lot_number lot_number    
, c3150_catagory capa_category
, c3150_capa_desc capa_description
, c3150_previous_capa previous_capa    
, c3150_submitted_dt submitted_date
, c3150_response_dt response_date
, c901_priority_level priority_level_id    
, get_code_name (c901_priority_level) priority_level
, c3150_close_dt close_date
, c3150_response response    
, c3150_void_fl void_flag
, get_user_name (c3150_created_by) created_by
, c3150_created_date created_date    
, get_user_name (c3150_last_updated_by) last_updated_by
, c3150_last_updated_date last_updated_date 
FROM globus_app.t3150_capa
);

