-- @"c:\database\dw_bi\materializedviews\Quality\CAPA_FACT.vw"

DROP MATERIALIZED VIEW CAPA_FACT;

CREATE MATERIALIZED VIEW CAPA_FACT
NOCACHE
LOGGING
NOCOMPRESS
NOPARALLEL
REFRESH FORCE ON DEMAND
AS
(
SELECT capa_dim.capa_key_id, date_dim.date_key_id
     , DECODE (capa_dim.status_id
             , '102770', GET_WEEKDAYSDIFF(TRUNC(capa_dim.submitted_date),TRUNC(capa_dim.last_updated_date)) 
             , '102771', GET_WEEKDAYSDIFF(TRUNC(capa_dim.submitted_date),TRUNC(capa_dim.close_date))
             , 0
              ) daystoclose
     , DECODE (capa_dim.status_id
             , '102769', GET_WEEKDAYSDIFF(TRUNC(capa_dim.submitted_date),TRUNC(SYSDATE))
             , '102770', GET_WEEKDAYSDIFF(TRUNC(capa_dim.submitted_date),TRUNC(capa_dim.last_updated_date))
             , '102771', GET_WEEKDAYSDIFF(TRUNC(capa_dim.submitted_date),TRUNC(capa_dim.close_date))
             , 0
              ) aging
  FROM capa_dim, date_dim
 WHERE TRUNC (capa_dim.submitted_date) = date_dim.cal_date(+)
);
