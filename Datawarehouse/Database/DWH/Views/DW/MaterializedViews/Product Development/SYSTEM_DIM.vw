DROP MATERIALIZED VIEW system_dim;

/*
This dimension will store all system related information.
*/

CREATE MATERIALIZED VIEW system_dim 
NOCACHE
NOLOGGING
NOCOMPRESS
NOPARALLEL
REFRESH FORCE ON DEMAND
AS
(
SELECT  
  t207.c207_set_id system_key_id
, t207.c207_set_id system_id
, regexp_replace (t207.c207_set_nm, '.?trade;|.?reg;|.?\(.*\)') system_nm
, t207.c207_set_desc set_desc
, t207.c207_seq_no seq_no
, MAX(DECODE(t240.c901_type , 20175,t240.c240_value,0 )) baseline_case
, MAX(DECODE(t240.c901_type, 20176,t240.c240_value,0 )) organization_list_turns
, MAX(DECODE(t240.c901_type, 20177, t240.c240_value,0 )) organization_baseline_case
, MAX(DECODE(t240.c901_type, 20178, t240.c240_value,0 )) baseline_cogs_setup
, MAX(DECODE(attribute_type_id,'101501',to_number(attribute_value_id),NULL)) BOD_SEGMENT_PRIORITY
, MAX(DECODE(attribute_type_id,'101500',attribute_value_id,NULL)) BOD_SEGMENT_ID
, MAX(DECODE(attribute_type_id,'101500',get_code_name(attribute_value_id),NULL)) BOD_SEGMENT_NAME
FROM t207_set_master t207, t240_baseline_value t240,
(
SELECT C207_SET_ID setid, C207C_ATTRIBUTE_VALUE attribute_value_id, C901_ATTRIBUTE_TYPE attribute_type_id
FROM GLOBUS_APP.T207C_SET_ATTRIBUTE T207C
WHERE T207C.C207C_VOID_FL IS NULL
AND C901_ATTRIBUTE_TYPE IN (101500,101501)
) T207C
WHERE t207.c901_set_grp_type = 1600 AND t207.c207_void_fl IS NULL 
      AND t207.c207_set_id = t240.c207_set_id (+)
      AND T207C.setid (+)= t207.c207_set_id
GROUP BY t207.c207_set_id, c207_set_nm, c207_set_desc, c207_seq_no
UNION
SELECT  
  '-1' system_key_id
, '-1' system_id
, 'No System' system_nm
, 'N/A' set_desc
, -1 seq_no
, Null baseline_case
, Null organization_list_turns
, Null organization_baseline_case
, Null baseline_cogs_setup
, NULL BOD_SEGMENT_PRIORITY
, NULL BOD_SEGMENT_ID
, NULL BOD_SEGMENT_NAME
FROM Dual
);


ALTER TABLE SYSTEM_DIM ADD 
CONSTRAINT SYSTEM_DIM_PK
 PRIMARY KEY (SYSTEM_KEY_ID)
 ENABLE VALIDATE; 