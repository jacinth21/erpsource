DROP materialized VIEW group_dim;
CREATE materialized VIEW group_dim nocache nologging nocompress noparallel refresh force ON demand
AS
  (SELECT t4010.c4010_group_id group_key_id,
    t4010.c4010_group_id group_id,
    t4010.c4010_group_nm group_name,
    t4010.c901_group_type group_type_id,
    t901.c901_code_nm group_type_name,
	t4010.c901_type type,
	t901_type.c901_code_nm type_name,
	t4010.c4010_group_desc group_desc
  FROM t4010_group t4010,
    t901_code_lookup t901,
	t901_code_lookup t901_type
  WHERE t4010.c901_group_type = t901.c901_code_id(+)
  AND t4010.c901_type =t901_type.C901_Code_Id(+)
  AND t4010.c4010_void_fl    IS NULL
 );
  ALTER TABLE group_dim ADD CONSTRAINT group_dim_pk PRIMARY KEY (group_key_id) Enable VALIDATE;
  /