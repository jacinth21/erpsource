CREATE OR REPLACE VIEW globus_bi_realtime.PATIENT_QUALIFICATION_FACT
AS 
SELECT 
PATIENT_KEY_ID 
,PATIENT_ID 
,DATE_KEY_ID 
,PATIENT_DETAILS_ID 
,PATIENT_DETAILS_KEY_ID 
,PATIENT_DETAILS_DATE 
,FORM_ID 
,FORM_KEY_ID 
,STUDY_LIST_ID
,STUDY_PERIOD_ID
,Max(Knwn_Alrgy_titnm_cblt_chrm) 	Knwn_Alrgy_titnm_cblt_chrm 
,Max(Has_NeuroMuscular_disorder) 	Has_NeuroMuscular_disorder
,Max(Has_Active_Malignancy) 		Has_Active_Malignancy
,Max(Active_Systm_Locl_Infction)   	Active_Systm_Locl_Infction
,Max(Diag_Acut_Mentl_Ill) 		Diag_Acut_Mentl_Ill
,Max(Diag_osteoporosis_bone_disease) 	Diag_osteoporosis_bone_disease
,Max(Clinic_Comprom_Vertbral_bodies) 	Clinic_Comprom_Vertbral_bodies
,Max(Has_Prior_Surgery) 		Has_Prior_Surgery
,Max(Prior_Fusion_Surgery) 		Prior_Fusion_Surgery
,Max(Mrk_crvcl_instbil_m_than_11)	Mrk_crvcl_instbil_m_than_11
,Max(Mrk_crvcl_instbil_g_than_3mm)	Mrk_crvcl_instbil_g_than_3mm
,Max(Mark_cervicl_instbil)		Mark_cervicl_instbil
,Max(Vertebral_level_req_treat)		Vertebral_level_req_treat
,Max(Neck_Arm_Pain_Ukwn_Etiolog)	Neck_Arm_Pain_Ukwn_Etiolog
,Max(anthr_InvstgDev_Drgcling30dSrg)	anthr_InvstgDev_Drgcling30dSrg
,Max(Rdiograp_conf_degen_hypertroph)	Rdiograp_conf_degen_hypertroph
,Max(Rheumat_Arth_Autoimmun_Diseas)	Rheumat_Arth_Autoimmun_Diseas
,Max(Svre_Spondy_disc_ht_gr_50pct)  	Svre_Spondy_disc_ht_gr_50pct
,Max(Svre_Spondy_absen_of_motion)	Svre_Spondy_absen_of_motion
,MaX(Svre_Spondy_bridg_osteophytes)	Svre_Spondy_bridg_osteophytes
,Max(Svre_Spondylosis_be_treated)	Svre_Spondylosis_be_treated
,Max(systemic_diseas_aids_hiv_Hepat)	systemic_diseas_aids_hiv_Hepat
,Max(Bone_grwth_stimul_past_30days)	Bone_grwth_stimul_past_30days
,Max(Is_A_Prisoner)			Is_A_Prisoner
,Max(Preg_or_interst_becom_nxt2yrs)	Preg_or_interst_becom_nxt2yrs
,Max(Medn_drg_intf_bonsoft_tsu_heal)	Medn_drg_intf_bonsoft_tsu_heal
,Max(Fail_least_6wks_consrv_treat)	Fail_least_6wks_consrv_treat 
,Max(preopr_NDI_Scr_Dbl_Expr_Prcnt)	preopr_NDI_Scr_Dbl_Expr_Prcnt
,Max(SCDD_One_Vertebral_lvl_C3_C7)	SCDD_One_Vertebral_lvl_C3_C7
,Max(SCDD_Hrniat_Nucls_PulposC3_C7)	SCDD_Hrniat_Nucls_PulposC3_C7
,Max(SCDD_Spondylosis_lvl_C3_C7)	SCDD_Spondylosis_lvl_C3_C7
,MAx(SCDD_Radiculopathy_Myelopathy)	SCDD_Radiculopathy_Myelopathy
,Max(SCDD_Loss_of_disc_Height)  	SCDD_Loss_of_disc_Height
,Max(Age_Btwn_18_60_yr_time_surg)	Age_Btwn_18_60_yr_time_surg
,Max(Age_50yrs_or_older_enrollment)	Age_50yrs_or_older_enrollment
,Max(Follow_Post_Operat_Mgmt_Prg)	Follow_Post_Operat_Mgmt_Prg
,MAx(Meet_Proposed_Follow_up_Sched)	Meet_Proposed_Follow_up_Sched
,MAx(Unstd_Study_sign_consentform)	Unstd_Study_sign_consentform
,Max(Psy_Phy_Ment_Cmply_StdyPrtcol)	Psy_Phy_Ment_Cmply_StdyPrtcol
,Max(Knwn_Alergy_Titn_Tantlm_PEEK)	Knwn_Alergy_Titn_Tantlm_PEEK
,Max(Cannot_Sit_50Mins_Wo_Pain)		Cannot_Sit_50Mins_Wo_Pain
,Max(Cannot_walk_more_than_50ft)	Cannot_walk_more_than_50ft
,Max(Has_fixed_motor_deficit)		Has_fixed_motor_deficit
,Max(Acut_Denerv_2ndry_radiculp_emg)	Acut_Denerv_2ndry_radiculp_emg
,Max(Has_Angina_Actv_Rheum_Arth)	Has_Angina_Actv_Rheum_Arth
,Max(AxlBckpain_wo_leg_Btck_GroinPn)	AxlBckpain_wo_leg_Btck_GroinPn
,Max(Cauda_EquinSynd_Neural_Comp)	Cauda_EquinSynd_Neural_Comp
,Max(Had_Surgery_Lumbar_Spine)		Had_Surgery_Lumbar_Spine
,Max(Osteop_Dexa_Bndens_Lthanng2_5)	Osteop_Dexa_Bndens_Lthanng2_5
,Max(Severe_Facet_Hypertrophy)		Severe_Facet_Hypertrophy
,Max(SvreSym_Lmbarspn_Sten_Mthn_2Lv)	SvreSym_Lmbarspn_Sten_Mthn_2Lv
,Max(signif_Instab_lumbar_spine)	signif_Instab_lumbar_spine
,Max(Sgf_PPhrLvasclr_Diseas_DimDors)	Sgf_PPhrLvasclr_Diseas_DimDors
,Max(Signf_Scoliosis_Cobb_Angle)	Signf_Scoliosis_Cobb_Angle
,Max(Spinal_Metastasis_Vertebrae)	Spinal_Metastasis_Vertebrae
,MAx(spondylol_GThan_Grd1_Afct_lvl)	spondylol_GThan_Grd1_Afct_lvl
,Max(SStain_Pthlgic_Fract_Spine_Hip)	SStain_Pthlgic_Fract_Spine_Hip
,Max(UnRemitt_Pain_In_Spinal_Pos)	UnRemitt_Pain_In_Spinal_Pos
,Max(Imun_SuprRcd_Strd_dl_gth1_12m) 	Imun_SuprRcd_Strd_dl_gth1_12m
,Max(morb_obes_BMI_GT40wght100lbs)	morb_obes_BMI_GT40wght100lbs
,Max(ZCQ_62_61_1_5_PF_62_61_1_5_SS)	ZCQ_62_61_1_5_PF_62_61_1_5_SS
,Max(Completed_6mth_conserv_treat)	Completed_6mth_conserv_treat
,Max(lumbars_stenos_leg_btk_groinpn)	lumbars_stenos_leg_btk_groinpn
,Max(Narrow_spin_canl_nrve_rootcanl)	Narrow_spin_canl_nrve_rootcanl
,Max(meet_prp_flwupschd6w3_6_12_24m)	meet_prp_flwupschd6w3_6_12_24m
,Max(Able_sit_for_50mins_wo_pain)	Able_sit_for_50mins_wo_pain
,Max(Able_walk_for_50_feet_or_more)	Able_walk_for_50_feet_or_more
,Max(ddd_1vertlevel_l1_s1)		ddd_1vertlevel_l1_s1
,Max(loss_of_disc_ht_gt_2mm)		loss_of_disc_ht_gt_2mm
,Max(herniated_nucleus_pulposus)	herniated_nucleus_pulposus
,Max(modic_changes)			modic_changes
,Max(scarring_thickening_ligamentum)	scarring_thickening_ligamentum
,Max(vacuum_phenomenon)			vacuum_phenomenon
,Max(between_18_65)			between_18_65
,Max(VAS_back_leg_atleast_30)		VAS_back_leg_atleast_30
,Max(ODI_atleast_30)			ODI_atleast_30
,Max(back_leg_pain_etiology)		back_leg_pain_etiology
,Max(acute_chronic_mental_illness)	acute_chronic_mental_illness
,Max(bilateral_leg_pain)		bilateral_leg_pain
,Max(disc_ht_lesst_5mm)			disc_ht_lesst_5mm
,Max(six_mon_conservative_treatment)	six_mon_conservative_treatment
,Max(laminectomy_or_laminotomy)		laminectomy_or_laminotomy
,Max(prior_fusion_surgery_lumbar)	prior_fusion_surgery_lumbar
,Max(instability_gt_3mm)		instability_gt_3mm
,Max(gt1_lumbar_level_with_DDD)		gt1_lumbar_level_with_DDD
,Max(osteoporosis_or_osteopenia)	osteoporosis_or_osteopenia
,Max(SCOLIOSIS_gt_10)			scoliosis_gt_10
,Max(PATHOLOGY_FACET_JOIN)		pathology_facet_join
,Max(SPONDYLOSIS_gt1_grade)		spondylosis_gt1_grade
,Max(pathologic_fractures_spine_hip)	pathologic_fractures_spine_hip
,Max(understand_sign_consent_form)	understand_sign_consent_form
,Max(medication_bone_soft_tissue)	medication_bone_soft_tissue
FROM ( 
SELECT 
C623_PATIENT_ANS_LIST_ID PATIENT_ANS_LIST_ID 
,C623_PATIENT_ANS_LIST_ID PATIENT_ANS_LST_KEY_ID 
,T623_PATIENT_ANSWER_LIST.C622_PATIENT_LIST_ID PATIENT_DETAILS_ID 
,T623_PATIENT_ANSWER_LIST.C622_PATIENT_LIST_ID PATIENT_DETAILS_KEY_ID 
,DATE_DIM.DATE_KEY_ID 
,T623_PATIENT_ANSWER_LIST.C621_PATIENT_ID PATIENT_ID 
,T623_PATIENT_ANSWER_LIST.C621_PATIENT_ID PATIENT_KEY_ID 
,T612_STUDY_FORM_LIST.C601_FORM_ID FORM_ID 
,T612_STUDY_FORM_LIST.C601_FORM_ID FORM_KEY_ID 
,T612_STUDY_FORM_LIST.C612_STUDY_LIST_ID STUDY_LIST_ID
,T613_STUDY_PERIOD.C613_STUDY_PERIOD_ID STUDY_PERIOD_ID
,T623_PATIENT_ANSWER_LIST.C602_QUESTION_ID QUESTION_ID 
,T623_PATIENT_ANSWER_LIST.C602_QUESTION_ID QUESTION_KEY_ID 
,T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID ANSWER_LIST_ID 
,T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID ANSWER_LIST_KEY_ID 
,T603_ANSWER_GRP.C603_ANSWER_GRP_ID ANSWER_GRP_ID 
,T603_ANSWER_GRP.C603_ANSWER_GRP_ID ANSWER_GRP_KEY_ID 
,T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID ANS_GRP_DW_REF_ID 
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC12', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Knwn_Alrgy_titnm_cblt_chrm 
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'NEUROMASCULAR_DISORDER_STAT_TR' OR T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'EXC17'  then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  Has_NeuroMuscular_disorder
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'MALIGNANCY_STAT_TR' OR T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'EXC16'  then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  Has_Active_Malignancy
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'INFECTION_STAT_TR' OR T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'EXC18'  then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end Active_Systm_Locl_Infction  
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC25', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Diag_Acut_Mentl_Ill
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC9', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Diag_osteoporosis_bone_disease
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'CLINICAL_COMP_TR' OR T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'EXC4'  then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  Clinic_Comprom_Vertbral_bodies
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC3', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Has_Prior_Surgery
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC2', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Prior_Fusion_Surgery
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC6.1',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Mrk_crvcl_instbil_m_than_11
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC6.2',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Mrk_crvcl_instbil_g_than_3mm
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC6',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Mark_cervicl_instbil
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC1',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Vertebral_level_req_treat 
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC8',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Neck_Arm_Pain_Ukwn_Etiolog
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'DEVICE_PRG_STAT_TR' OR T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'EXC24'  then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end anthr_InvstgDev_Drgcling30dSrg
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC5', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Rdiograp_conf_degen_hypertroph
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC14',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Rheumat_Arth_Autoimmun_Diseas
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC7.3', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Svre_Spondy_disc_ht_gr_50pct  
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC7.1', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Svre_Spondy_absen_of_motion
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC7.2', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Svre_Spondy_bridg_osteophytes
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC7',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Svre_Spondylosis_be_treated
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'DISEASE_STAT_TR' OR T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'EXC17_SD'  then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  systemic_diseas_aids_hiv_Hepat
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'SIMULATOR_STAT_TR' OR T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'EXC19'  then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  Bone_grwth_stimul_past_30days
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'PRISONER_STAT_TR' OR T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'EXC27'  then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end Is_A_Prisoner
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'PREGNANCY_STAT_TR' OR T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'EXC26'  then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end Preg_or_interst_becom_nxt2yrs
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC13',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Medn_drg_intf_bonsoft_tsu_heal
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'INC3',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Fail_least_6wks_consrv_treat 
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'INC4',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) preopr_NDI_Scr_Dbl_Expr_Prcnt
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'INC1.1', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) SCDD_Hrniat_Nucls_PulposC3_C7
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'INC1.2', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) SCDD_Spondylosis_lvl_C3_C7
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'INC1.3',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) SCDD_Radiculopathy_Myelopathy
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'INC1.4', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) SCDD_Loss_of_disc_Height  
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'INC1', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) SCDD_One_Vertebral_lvl_C3_C7
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'POST_OP_MPRG_TR' OR T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'INC11'  then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  Follow_Post_Operat_Mgmt_Prg
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'INC6', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Completed_6mth_conserv_treat
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'INC7', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Meet_Proposed_Follow_up_Sched
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'INC8', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Unstd_Study_sign_consentform
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'INC2', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Age_Btwn_18_60_yr_time_surg
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'INC5', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Age_50yrs_or_older_enrollment
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'FOLLOW_UP_TR' OR T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'INC9'  then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  Psy_Phy_Ment_Cmply_StdyPrtcol
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'ALLERGY_STAT_TR' OR T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'EXC22'  then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end Knwn_Alergy_Titn_Tantlm_PEEK
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC1_SD',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Cannot_Sit_50Mins_Wo_Pain
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC2_SD',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Cannot_walk_more_than_50ft
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC5_SD',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Has_fixed_motor_deficit
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC10',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Acut_Denerv_2ndry_radiculp_emg
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC19_SD',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Has_Angina_Actv_Rheum_Arth
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC4_SD',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) AxlBckpain_wo_leg_Btck_GroinPn
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'CAUDA_EQUINA_STAT_TR' OR T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'EXC6_SD'  then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  Cauda_EquinSynd_Neural_Comp
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC9_SD',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Had_Surgery_Lumbar_Spine
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC20',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Osteop_Dexa_Bndens_Lthanng2_5
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC15',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Severe_Facet_Hypertrophy
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC7_SD',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) SvreSym_Lmbarspn_Sten_Mthn_2Lv
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC8_SD',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) signif_Instab_lumbar_spine
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC12_SD',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Sgf_PPhrLvasclr_Diseas_DimDors
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC11',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Signf_Scoliosis_Cobb_Angle
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC21',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Spinal_Metastasis_Vertebrae
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC13_SD',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) spondylol_GThan_Grd1_Afct_lvl
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC14_SD',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) SStain_Pthlgic_Fract_Spine_Hip
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC3_SD',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) UnRemitt_Pain_In_Spinal_Pos
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'EXC23',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Imun_SuprRcd_Strd_dl_gth1_12m
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'MORBIDLY_OBESE_TR' OR T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'EXC16_SD'  then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  morb_obes_BMI_GT40wght100lbs
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'INC7_SD',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) ZCQ_62_61_1_5_PF_62_61_1_5_SS
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'INC1_SD',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) lumbars_stenos_leg_btk_groinpn
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'INC2_SD',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Narrow_spin_canl_nrve_rootcanl
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'PROP_FOLLOW_UP_TR' OR T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'INC10'  then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end   meet_prp_flwupschd6w3_6_12_24m
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'INC3_SD',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Able_sit_for_50mins_wo_pain
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'INC4_SD',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Able_walk_for_50_feet_or_more
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'INC1_PST'then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end ddd_1vertlevel_l1_s1
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'INC2_PST'then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end loss_of_disc_ht_gt_2mm
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'INC3_PST'then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end herniated_nucleus_pulposus
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'INC4_PST'then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end modic_changes
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'INC5_PST'then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end scarring_thickening_ligamentum
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'INC6_PST'then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  vacuum_phenomenon
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'INC7_PST'then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  between_18_65
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'VAS_STATUS_TR'then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end VAS_back_leg_atleast_30
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'ODI_STATUS_TR'then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end ODI_atleast_30
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'PAIN_ETILOLOGY_TR'then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end back_leg_pain_etiology
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'ACUTE_DIAGNOSE_ST_TR'then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end acute_chronic_mental_illness
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'LEGPAIN_STAT_TR'then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  bilateral_leg_pain
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'DISC_HEIGHT_LVL_TR'then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  disc_ht_lesst_5mm
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'CONS_TREATMENT_TR'then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end six_mon_conservative_treatment
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'LAMINECTOMY_ST_TR'then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end laminectomy_or_laminotomy 
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'FUSION_SURG_TR'then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end prior_fusion_surgery_lumbar
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'STABILITY_TR'then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end instability_gt_3mm
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'LUMBAR_LVL_TR'then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end gt1_lumbar_level_with_DDD
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'OSTEOPHOROSIS_STAT_TR'then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  osteoporosis_or_osteopenia
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'SCOLIOSIS_STAT_TR'then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end SCOLIOSIS_gt_10
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'PATHOLOGY_ST_TR'then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end PATHOLOGY_FACET_JOIN
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'SPONDYLOSIS_STAT_TR'then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end SPONDYLOSIS_gt1_grade
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'PATHOLOGIC_FRACTURES_STAT_TR'then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end pathologic_fractures_spine_hip
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'CONSONENT_FORM_TR'then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end understand_sign_consent_form
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'DRUG_STAT_TR'then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  medication_bone_soft_tissue
,T622_PATIENT_LIST.C622_DATE PATIENT_DETAILS_DATE 
,C623_HISTORY_FLAG HISTORY_FLAG 
,GET_USER_NAME(C623_CREATED_BY) CREATED_BY 
,C623_CREATED_DATE CREATED_DATE 
,C623_LAST_UPDATED_DATE LAST_UPDATED_DATE 
,GET_USER_NAME(C623_LAST_UPDATED_BY) LAST_UPDATED_BY 
FROM T612_STUDY_FORM_LIST, T613_STUDY_PERIOD, T623_PATIENT_ANSWER_LIST, T622_PATIENT_LIST, T603_ANSWER_GRP, T602_QUESTION_MASTER, DATE_DIM 
WHERE 
T612_STUDY_FORM_LIST.C601_FORM_ID IN (1, 21, 41) 
AND T602_QUESTION_MASTER.C602_QUESTION_ID = T603_ANSWER_GRP.C602_QUESTION_ID 
AND T612_STUDY_FORM_LIST.C612_STUDY_LIST_ID = T613_STUDY_PERIOD.C612_STUDY_LIST_ID
AND T623_PATIENT_ANSWER_LIST.C601_FORM_ID = T612_STUDY_FORM_LIST.C601_FORM_ID 
AND T622_PATIENT_LIST.C622_DATE = DATE_DIM.CAL_DATE 
AND T623_PATIENT_ANSWER_LIST.C622_PATIENT_LIST_ID =  T622_PATIENT_LIST.C622_PATIENT_LIST_ID 
AND T623_PATIENT_ANSWER_LIST.C602_QUESTION_ID = T602_QUESTION_MASTER.C602_QUESTION_ID 
AND T623_PATIENT_ANSWER_LIST.C603_ANSWER_GRP_ID = T603_ANSWER_GRP.C603_ANSWER_GRP_ID 
AND (T623_PATIENT_ANSWER_LIST.C623_DELETE_FL IS NULL OR T623_PATIENT_ANSWER_LIST.C623_DELETE_FL = 'N') 
AND (T602_QUESTION_MASTER.C602_DELETE_FL IS NULL OR T602_QUESTION_MASTER.C602_DELETE_FL = 'N') 
) GROUP BY 
PATIENT_KEY_ID 
,PATIENT_ID 
,PATIENT_DETAILS_ID 
,PATIENT_DETAILS_KEY_ID 
,DATE_KEY_ID 
,PATIENT_DETAILS_DATE 
,FORM_ID 
,FORM_KEY_ID
,STUDY_LIST_ID
,STUDY_PERIOD_ID; 