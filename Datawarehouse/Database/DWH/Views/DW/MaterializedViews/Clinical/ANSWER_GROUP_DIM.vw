CREATE OR REPLACE VIEW globus_bi_realtime.ANSWER_GROUP_DIM
AS
SELECT 
C603_ANSWER_GRP_ID ANSWER_GRP_ID
,C603_ANSWER_GRP_ID ANSWER_GRP_KEY_ID	
,C603_ANSWER_GRP_DS ANSWER_GRP_DESC	
,C904_ANSWER_GRP ANSWER_GRP	
,C602_QUESTION_ID QUESTION_ID
,C602_QUESTION_ID QUESTION_KEY_ID	
,C603_ANSWER_SEQ_NO ANSWER_SEQ_NO	
,C603_CONTROL_LENGTH CONTROL_LENGTH	
,C603_COLUMN_LENGTH COLUMN_LENGTH	
,C603_MANDATORY_FLAG MANDATORY_FLAG	
,GET_CODE_NAME(C901_CONTROL_TYPE) CONTROL_TYPE
,C603_XML_REQUIRED_FL XML_REQUIRED_FL
,C603_XML_TAG_ID XML_TAG_ID	
,C603_DEFAULT_VALUE DEFAULT_VALUE
,C603_VALIDATE_FN VALIDATE_FN
,GET_USER_NAME(C603_CREATED_BY) CREATED_BY	
,C603_CREATED_DATE CREATED_DATE 	
,C603_LAST_UPDATED_DATE LAST_UPDATED_DATE	
,GET_USER_NAME(C603_LAST_UPDATED_BY) LAST_UPDATED_BY	
FROM T603_ANSWER_GRP
WHERE (C603_DELETE_FL IS NULL OR C603_DELETE_FL = 'N');