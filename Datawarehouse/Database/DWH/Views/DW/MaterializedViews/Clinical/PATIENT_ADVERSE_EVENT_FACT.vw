CREATE OR REPLACE VIEW globus_bi_realtime.PATIENT_ADVERSE_EVENT_FACT
AS 
SELECT 
PATIENT_KEY_ID 
,PATIENT_ID 
,DATE_KEY_ID 
,PATIENT_DETAILS_ID 
,PATIENT_DETAILS_KEY_ID 
,PATIENT_DETAILS_DATE 
,FORM_ID 
,FORM_KEY_ID 
,STUDY_LIST_ID
,STUDY_PERIOD_ID
,MAX(Continuing_Y_N) Continuing_Y_N 
,MAX(OutCome) OutCome 
,MAX(Hospitalization) Hospitalization 
,MAX(Relnp_To_Implant) Relationship_To_Implant 
,MAX(Intensity) Intensity 
,MAX(UADE_Y_N) UADE_Y_N 
,MAX(Action_Taken) Action_Taken
,MAX(globus_dw.gm_dw_pkg_util.valid_to_date(Onset_Date,'MM/DD/YYYY')) Onset_Date 
,MAX(globus_dw.gm_dw_pkg_util.valid_to_date(Resolution_Date,'MM/DD/YYYY')) Resolution_Date 
,MAX(Relationship_to_Device) Relationship_to_Device 
,MAX(Adverse_Event_Comments) Adverse_Event_Comments 
,MAX(Adverse_Event_Code) Adverse_Event_Code 
,MAX(Adverse_Event) Adverse_Event 
FROM ( 
SELECT 
C623_PATIENT_ANS_LIST_ID PATIENT_ANS_LIST_ID 
,C623_PATIENT_ANS_LIST_ID PATIENT_ANS_LST_KEY_ID 
,T623_PATIENT_ANSWER_LIST.C622_PATIENT_LIST_ID PATIENT_DETAILS_ID 
,T623_PATIENT_ANSWER_LIST.C622_PATIENT_LIST_ID PATIENT_DETAILS_KEY_ID 
,DATE_DIM.DATE_KEY_ID 
,T623_PATIENT_ANSWER_LIST.C621_PATIENT_ID PATIENT_ID 
,T623_PATIENT_ANSWER_LIST.C621_PATIENT_ID PATIENT_KEY_ID 
,T612_STUDY_FORM_LIST.C601_FORM_ID FORM_ID 
,T612_STUDY_FORM_LIST.C601_FORM_ID FORM_KEY_ID 
,T612_STUDY_FORM_LIST.C612_STUDY_LIST_ID STUDY_LIST_ID
,T613_STUDY_PERIOD.C613_STUDY_PERIOD_ID STUDY_PERIOD_ID
,T623_PATIENT_ANSWER_LIST.C602_QUESTION_ID QUESTION_ID 
,T623_PATIENT_ANSWER_LIST.C602_QUESTION_ID QUESTION_KEY_ID 
,T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID ANSWER_LIST_ID 
,T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID ANSWER_LIST_KEY_ID 
,T603_ANSWER_GRP.C603_ANSWER_GRP_ID ANSWER_GRP_ID 
,T603_ANSWER_GRP.C603_ANSWER_GRP_ID ANSWER_GRP_KEY_ID 
,T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID ANS_GRP_DW_REF_ID 
, CASE 
  WHEN  T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID IN ('AE1', 'AE1_SD','AE4_TR') THEN DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)
  END Adverse_Event 
, CASE 
  WHEN  T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID IN ('AECODE', 'AECODE_SD','TRIAECODE') THEN DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)
  END Adverse_Event_Code 
, CASE 
  WHEN  T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID IN ('CONT1', 'CONT1_SD') THEN DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)
  END Continuing_Y_N 
, CASE 
  WHEN  T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID IN ('OUTCOME1', 'OUTCOME1_SD','OUTCOME4_TR') THEN DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)
  END OutCome 
, CASE 
  WHEN  T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID IN ('HOSP1', 'HOSP1_SD','HOSP4_TR') THEN DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)
  END Hospitalization 
, CASE 
  WHEN  T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID IN ('RELAT1', 'RELAT1_SD','RELAT4_TR') THEN DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)
  END Relnp_To_Implant 
, CASE 
  WHEN  T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID IN ('INTENS1', 'INTENS1_SD','INTENS4_TR') THEN DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)
  END Intensity 
, CASE 
  WHEN  T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID IN ('UADE1', 'UADE1_SD','UADE4_TR') THEN DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)
  END UADE_Y_N 
, CASE 
  WHEN  T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID IN ('ACTION1', 'ACTION1_SD','ACTION4_TR') THEN DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)
  END Action_Taken
, CASE 
  WHEN  T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID IN ('AEONDT1', 'AEONDT1_SD','AEONDT4_TR') THEN DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)
  END Onset_Date 
, CASE 
  WHEN  T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID IN ('AERESDT1', 'AERESDT_SD','AERESDT4_TR') THEN DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)
  END Resolution_Date 
, CASE 
  WHEN  T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID IN ('RELAT1', 'RELAT1_SD','RELAT4_TR') THEN DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)
  END Relationship_to_Device 
, CASE 
  WHEN  T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID IN ('AECOMM', 'AECOMM_SD','COMM_TR') THEN DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)
  END Adverse_Event_Comments 
,T622_PATIENT_LIST.C622_DATE PATIENT_DETAILS_DATE 
,C623_HISTORY_FLAG HISTORY_FLAG 
,GET_USER_NAME(C623_CREATED_BY) CREATED_BY 
,C623_CREATED_DATE CREATED_DATE 
,C623_LAST_UPDATED_DATE LAST_UPDATED_DATE 
,GET_USER_NAME(C623_LAST_UPDATED_BY) LAST_UPDATED_BY 
FROM T612_STUDY_FORM_LIST, T613_STUDY_PERIOD, T623_PATIENT_ANSWER_LIST, T622_PATIENT_LIST, T603_ANSWER_GRP, T602_QUESTION_MASTER, DATE_DIM 
WHERE 
T612_STUDY_FORM_LIST.C601_FORM_ID IN (14, 213,50) 
AND T602_QUESTION_MASTER.C602_QUESTION_ID = T603_ANSWER_GRP.C602_QUESTION_ID 
AND T612_STUDY_FORM_LIST.C612_STUDY_LIST_ID = T613_STUDY_PERIOD.C612_STUDY_LIST_ID
AND T623_PATIENT_ANSWER_LIST.C601_FORM_ID = T612_STUDY_FORM_LIST.C601_FORM_ID 
AND T622_PATIENT_LIST.C622_DATE = DATE_DIM.CAL_DATE 
AND T623_PATIENT_ANSWER_LIST.C622_PATIENT_LIST_ID =  T622_PATIENT_LIST.C622_PATIENT_LIST_ID 
AND T623_PATIENT_ANSWER_LIST.C602_QUESTION_ID = T602_QUESTION_MASTER.C602_QUESTION_ID 
AND T623_PATIENT_ANSWER_LIST.C603_ANSWER_GRP_ID = T603_ANSWER_GRP.C603_ANSWER_GRP_ID 
AND (T623_PATIENT_ANSWER_LIST.C623_DELETE_FL IS NULL OR T623_PATIENT_ANSWER_LIST.C623_DELETE_FL = 'N') 
AND (T602_QUESTION_MASTER.C602_DELETE_FL IS NULL OR T602_QUESTION_MASTER.C602_DELETE_FL = 'N') 
) GROUP BY 
PATIENT_KEY_ID 
,PATIENT_ID 
,PATIENT_DETAILS_ID 
,PATIENT_DETAILS_KEY_ID 
,DATE_KEY_ID 
,PATIENT_DETAILS_DATE 
,FORM_ID 
,FORM_KEY_ID
,STUDY_LIST_ID
,STUDY_PERIOD_ID;