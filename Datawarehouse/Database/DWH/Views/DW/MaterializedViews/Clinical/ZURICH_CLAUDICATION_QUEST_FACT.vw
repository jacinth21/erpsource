CREATE OR REPLACE VIEW globus_bi_realtime.ZURICH_CLAUDICATION_QUEST_FACT
AS 
SELECT 
PATIENT_KEY_ID 
,PATIENT_ID 
,DATE_KEY_ID 
,PATIENT_DETAILS_ID 
,PATIENT_DETAILS_KEY_ID 
,PATIENT_DETAILS_DATE 
,FORM_ID 
,FORM_KEY_ID 
,STUDY_LIST_ID
,STUDY_PERIOD_ID
,Max(Been_shop_grocries_oth_items) Been_shop_grocries_oth_items
,Max(Taken_walks_outdrs_or_malls) Taken_walks_outdrs_or_malls
,Max(Walk_arnd_diff_rooms_house_apt) Walk_arnd_diff_rooms_house_apt
,Max(Walked_frm_bedrm_bathrm) Walked_frm_bedrm_bathrm
,Max(How_far_been_able_to_walk) How_far_been_able_to_walk
,Max(How_oftn_back_buttock_leg_pain) How_oftn_back_buttock_leg_pain
,Max(Weakness_legs_or_feet) Weakness_legs_or_feet
,Max(Numbness_tingl_legs_or_feet) Numbness_tingl_legs_or_feet
,Max(Physical_Function_Score) Physical_Function_Score
,Max(Problems_w_your_balance) Problems_w_your_balance
,Max(Relief_of_pain_flw_operation) Relief_of_pain_flw_operation
,Max(Satisfication_Score) Satisfication_Score
,Max(Symptom_Severity_Score) Symptom_Severity_Score
,Max(Pain_in_your_legs_feet) Pain_in_your_legs_feet
,Max(Result_of_back_opertion) Result_of_back_opertion
,Max(Pain_in_your_back_or_buttcks) Pain_in_your_back_or_buttcks
,Max(Pain_avg_incl_bck_butck_leg) Pain_avg_incl_bck_butck_leg
,Max(Ably_do_hous_yard_wrk_jb_oprtn) Ably_do_hous_yard_wrk_jb_oprtn
,Max(Ably_to_walk_follw_operation) Ably_to_walk_follw_operation
,Max(Bal_or_steadiness_on_your_feet) Bal_or_steadiness_on_your_feet
,Max(Strength_in_thighs_legs_feet) Strength_in_thighs_legs_feet
FROM ( 
SELECT 
C623_PATIENT_ANS_LIST_ID PATIENT_ANS_LIST_ID 
,C623_PATIENT_ANS_LIST_ID PATIENT_ANS_LST_KEY_ID 
,T623_PATIENT_ANSWER_LIST.C622_PATIENT_LIST_ID PATIENT_DETAILS_ID 
,T623_PATIENT_ANSWER_LIST.C622_PATIENT_LIST_ID PATIENT_DETAILS_KEY_ID 
,DATE_DIM.DATE_KEY_ID 
,T623_PATIENT_ANSWER_LIST.C621_PATIENT_ID PATIENT_ID 
,T623_PATIENT_ANSWER_LIST.C621_PATIENT_ID PATIENT_KEY_ID 
,T612_STUDY_FORM_LIST.C601_FORM_ID FORM_ID 
,T612_STUDY_FORM_LIST.C601_FORM_ID FORM_KEY_ID 
,T612_STUDY_FORM_LIST.C612_STUDY_LIST_ID STUDY_LIST_ID
,T613_STUDY_PERIOD.C613_STUDY_PERIOD_ID STUDY_PERIOD_ID
,T623_PATIENT_ANSWER_LIST.C602_QUESTION_ID QUESTION_ID 
,T623_PATIENT_ANSWER_LIST.C602_QUESTION_ID QUESTION_KEY_ID 
,T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID ANSWER_LIST_ID 
,T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID ANSWER_LIST_KEY_ID 
,T603_ANSWER_GRP.C603_ANSWER_GRP_ID ANSWER_GRP_ID 
,T603_ANSWER_GRP.C603_ANSWER_GRP_ID ANSWER_GRP_KEY_ID 
,T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID ANS_GRP_DW_REF_ID 
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'ZCQQ10', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Been_shop_grocries_oth_items
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'ZCQQ9', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Taken_walks_outdrs_or_malls
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'ZCQQ11',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Walk_arnd_diff_rooms_house_apt
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'ZCQQ12', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Walked_frm_bedrm_bathrm
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'ZCQQ8', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) How_far_been_able_to_walk
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'ZCQQ2', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) How_oftn_back_buttock_leg_pain
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'ZCQQ6',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Weakness_legs_or_feet
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'ZCQQ5',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Numbness_tingl_legs_or_feet
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'PFSCR', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Physical_Function_Score
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'ZCQQ7', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Problems_w_your_balance
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'ZCQQ14',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Relief_of_pain_flw_operation
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'SATSCR',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Satisfication_Score
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'SSSCR',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) SYMPTOM_SEVERITY_SCORE
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'ZCQQ4',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) pain_in_your_legs_feet
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'ZCQQ13',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Result_of_back_opertion
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'ZCQQ3',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Pain_in_your_back_or_buttcks
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'ZCQQ1',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) pain_avg_incl_bck_butck_leg
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'ZCQQ16',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Ably_do_hous_yard_wrk_jb_oprtn
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'ZCQQ15',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Ably_to_walk_follw_operation
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'ZCQQ18',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Bal_or_steadiness_on_your_feet
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'ZCQQ17',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Strength_in_thighs_legs_feet
,T622_PATIENT_LIST.C622_DATE PATIENT_DETAILS_DATE 
,C623_HISTORY_FLAG HISTORY_FLAG 
,GET_USER_NAME(C623_CREATED_BY) CREATED_BY 
,C623_CREATED_DATE CREATED_DATE 
,C623_LAST_UPDATED_DATE LAST_UPDATED_DATE 
,GET_USER_NAME(C623_LAST_UPDATED_BY) LAST_UPDATED_BY 
FROM T612_STUDY_FORM_LIST, T613_STUDY_PERIOD, T623_PATIENT_ANSWER_LIST, T622_PATIENT_LIST, T603_ANSWER_GRP, T602_QUESTION_MASTER, DATE_DIM 
WHERE 
T612_STUDY_FORM_LIST.C601_FORM_ID IN (25) 
AND T602_QUESTION_MASTER.C602_QUESTION_ID = T603_ANSWER_GRP.C602_QUESTION_ID 
AND T612_STUDY_FORM_LIST.C612_STUDY_LIST_ID = T613_STUDY_PERIOD.C612_STUDY_LIST_ID
AND T623_PATIENT_ANSWER_LIST.C601_FORM_ID = T612_STUDY_FORM_LIST.C601_FORM_ID 
AND T622_PATIENT_LIST.C622_DATE = DATE_DIM.CAL_DATE 
AND T623_PATIENT_ANSWER_LIST.C622_PATIENT_LIST_ID =  T622_PATIENT_LIST.C622_PATIENT_LIST_ID 
AND T623_PATIENT_ANSWER_LIST.C602_QUESTION_ID = T602_QUESTION_MASTER.C602_QUESTION_ID 
AND T623_PATIENT_ANSWER_LIST.C603_ANSWER_GRP_ID = T603_ANSWER_GRP.C603_ANSWER_GRP_ID 
AND (T623_PATIENT_ANSWER_LIST.C623_DELETE_FL IS NULL OR T623_PATIENT_ANSWER_LIST.C623_DELETE_FL = 'N') 
AND (T602_QUESTION_MASTER.C602_DELETE_FL IS NULL OR T602_QUESTION_MASTER.C602_DELETE_FL = 'N') 
) GROUP BY 
PATIENT_KEY_ID 
,PATIENT_ID 
,PATIENT_DETAILS_ID 
,PATIENT_DETAILS_KEY_ID 
,DATE_KEY_ID 
,PATIENT_DETAILS_DATE 
,FORM_ID 
,FORM_KEY_ID
,STUDY_LIST_ID
,STUDY_PERIOD_ID;