/*
File name: SALES_FACT.vw
Created By: gvaithiyanathan
Description: This is a view combines both SALES_HISTORICAL_FACT and SALES_CURRENT_FACT
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Sales_FieldInventory\SALES_FACT.vw";
*/

CREATE OR REPLACE VIEW SALES_FACT
AS
  ( SELECT * FROM SALES_HISTORICAL_FACT
   UNION ALL
  SELECT * FROM SALES_CURRENT_FACT
  );
  /