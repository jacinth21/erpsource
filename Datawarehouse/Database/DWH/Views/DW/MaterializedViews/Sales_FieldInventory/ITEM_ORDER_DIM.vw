/*
File name: ITEM_ORDER_DIM.vw
Created By: gvaithiyanathan
Description: This is a view combines both ITEM_ORDER_HISTORICAL_DIM and ITEM_ORDER_CURRENT_DIM
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Sales_FieldInventory\ITEM_ORDER_DIM.vw";
*/
CREATE OR REPLACE VIEW ITEM_ORDER_DIM
AS
  (SELECT * FROM ITEM_ORDER_HISTORICAL_DIM
   UNION ALL
  SELECT * FROM ITEM_ORDER_CURRENT_DIM
  );
  
  /