/*
File name: LOANER_DIM.vw
Created By: gvaithiyanathan
Description: This is a view combines both LOANER_HISTORICAL_DIM and LOANER_CURRENT_DIM
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Sales_FieldInventory\LOANER_DIM.vw";
*/
CREATE OR REPLACE VIEW LOANER_DIM
AS
  ( SELECT * FROM LOANER_CURRENT_DIM
   UNION ALL
  SELECT * FROM LOANER_HISTORICAL_DIM
  );


/