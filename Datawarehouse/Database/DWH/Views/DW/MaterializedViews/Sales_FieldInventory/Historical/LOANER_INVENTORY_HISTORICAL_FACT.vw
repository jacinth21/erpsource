/*
File name: LOANER_INVENTORY_HISTORICAL_FACT.vw
Created By: gvaithiyanathan
Description: This Materialized view contains loaner data since beginning to last but before year. 
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Sales_FieldInventory\Historical\LOANER_INVENTORY_HISTORICAL_FACT.vw";
*/
DROP MATERIALIZED VIEW LOANER_INVENTORY_HISTORICAL_FACT;
CREATE MATERIALIZED VIEW LOANER_INVENTORY_HISTORICAL_FACT NOCACHE NOLOGGING NOCOMPRESS NOPARALLEL REFRESH FORCE ON DEMAND
AS
  (  
SELECT loaner_dim.loaner_key_id,
  loaner_dim.consignment_id consignment_key_id,
  loaner_dim.consigned_distributor_id distributor_key_id,
  loaner_dim.consigned_distributor_id distributor_id,
  loaner_dim.sales_rep_id sales_rep_key_id,
  loaner_dim.sales_rep_id sales_rep_id,
  loaner_dim.consigned_account_id account_key_id,
  loaner_dim.consigned_account_id account_id,
  consign_master_dim.consignment_set_id set_key_id,
  consign_master_dim.consignment_set_id set_id,
  date_dim.date_key_id loan_date_key_id,
  loaner_shipping_dim.shipping_id shipping_key_id,
  product_req_dtl_dim.product_request_detail_key_id,
  consign_master_dim.consignment_type_id,
  loaner_dim.consigned_to,
  loaner_dim.consigned_to_code_id,
  NVL(loaner_dim.return_date, TRUNC(sysdate))           - loaner_dim.loaner_date usage_days,
  SUM(DECODE(loaner_dim.return_date, NULL,TRUNC(sysdate)- loaner_dim.expected_return_date,0)) days_overdue,
  SUM(consign_master_dim.item_price                     * consign_master_dim.consignment_qty) total_loaner_value,
  loaner_dim.company_key_id company_key_id,
  loaner_dim.plant_key_id plant_key_id
FROM loaner_historical_dim loaner_dim,
  date_dim date_dim,
  (SELECT consignment_master_dim.consignment_id,
    consignment_master_dim.consignment_set_id,
    item_consignment_dim.part_key_id,
    item_consignment_dim.item_price,
    item_consignment_dim.consignment_qty,
    consignment_master_dim.consignment_type_id
  FROM consignment_master_historical_dim consignment_master_dim,
    item_consignment_historical_dim item_consignment_dim
  WHERE consignment_master_dim.consignment_id = item_consignment_dim.consignment_key_id(+)
  AND consignment_type_id                    IN (4127,4119)
  AND consignment_master_dim.void_fl         IS NULL
  )consign_master_dim ,
  (SELECT shipping_id,
    ref_id
  FROM shipping_dim
  WHERE source_code_id   IN (50182,50128,50185,50183)
  AND NVL(active_fl,'Y') != 'N'
  ) loaner_shipping_dim,
  product_request_detail_dim product_req_dtl_dim
  --50182 loaners,50128 loaner extension with replenish,50185 productloaner,50183 loaner extn
WHERE loaner_dim.consignment_id          = consign_master_dim.consignment_id
AND loaner_dim.loaner_date               = date_dim.cal_date
AND TO_CHAR(loaner_dim.loaner_key_id)    = loaner_shipping_dim.ref_id(+)
AND loaner_dim.product_request_detail_id = product_req_dtl_dim.product_request_detail_key_id(+)
GROUP BY loaner_dim.loaner_key_id,
  loaner_dim.consignment_id,
  loaner_dim.consigned_distributor_id,
  loaner_dim.sales_rep_id,
  loaner_dim.consigned_account_id,
  consign_master_dim.consignment_set_id,
  date_dim.date_key_id,
  loaner_shipping_dim.shipping_id,
  product_req_dtl_dim.product_request_detail_key_id,
  consign_master_dim.consignment_type_id,
  loaner_dim.consigned_to,
  loaner_dim.consigned_to_code_id,
  loaner_dim.return_date,
  loaner_dim.loaner_date,
  loaner_dim.company_key_id,
  loaner_dim.plant_key_id );
--------------- constraints
ALTER TABLE loaner_inventory_historical_fact ADD CONSTRAINT LOANER_INVENTORY_HISTORICAL_FACT_PK PRIMARY KEY (loaner_key_id) enable VALIDATE;