/*
File name: DO_CLASSIFICATION_HISTORICAL_FACT.vw
Created By: gvaithiyanathan
Description: •	This Materialized view contains T501B_ORDER_BY_SYSTEM data upto last 2years. 
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Sales_FieldInventory\Historical\DO_CLASSIFICATION_HISTORICAL_FACT.vw";
*/
DROP MATERIALIZED VIEW DO_CLASSIFICATION_HISTORICAL_FACT ;
CREATE MATERIALIZED VIEW DO_CLASSIFICATION_HISTORICAL_FACT NOCACHE LOGGING NOCOMPRESS NOPARALLEL REFRESH FORCE ON DEMAND
AS
  (SELECT T501B.C501_ORDER_ID ORDER_ID ,
    T501B.C501_ORDER_ID ORDER_KEY_ID ,
    T501B.C207_SYSTEM_ID SYSTEM_ID ,
    T501B.C207_SYSTEM_ID SYSTEM_KEY_ID ,
    ORDER_DIM.ACCOUNT_KEY_ID ACCOUNT_KEY_ID ,
    ORDER_DIM.SALES_REP_KEY_ID SALES_REP_KEY_ID ,
    DATE_DIM.DATE_KEY_ID DATE_KEY_ID ,
    T501B.C501B_QTY DO_QTY ,
    T501B.C901_TYPE DO_INVENTORY_TYPE_ID ,
    get_code_name(T501B.C901_TYPE) DO_INVENTORY_TYPE ,
    C501B_LAST_UPDATED_BY last_updated_by ,
    C501B_LAST_UPDATED_DATE last_updated_date ,
    C501B_IT_UPDATE_FL IT_UPDATE_FL ,
    DECODE( rank() over( partition BY T501B.C501_ORDER_ID, SYSTEM_DIM.BOD_SEGMENT_ID order by SYSTEM_DIM.BOD_SEGMENT_PRIORITY ),1,SYSTEM_DIM.BOD_SEGMENT_ID,NULL) BOD_SEGMENT_RANK_ID ,
    DECODE( rank() over( partition BY T501B.C501_ORDER_ID, SYSTEM_DIM.BOD_SEGMENT_ID order by SYSTEM_DIM.BOD_SEGMENT_PRIORITY ),1,SYSTEM_DIM.BOD_SEGMENT_NAME,NULL) BOD_SEGMENT
  FROM T501B_ORDER_BY_SYSTEM T501B,
    ORDER_HISTORICAL_DIM ORDER_DIM,
    DATE_DIM DATE_DIM,
    SYSTEM_DIM SYSTEM_DIM
  WHERE T501B.C501_ORDER_ID = ORDER_DIM.ORDER_KEY_ID
  AND date_dim.cal_date     = ORDER_DIM.order_dt
  AND T501B.C207_SYSTEM_ID  = SYSTEM_DIM.SYSTEM_KEY_ID
  AND T501B.C501B_VOID_FL  IS NULL
  );

 