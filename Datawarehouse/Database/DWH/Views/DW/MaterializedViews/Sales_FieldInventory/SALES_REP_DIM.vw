DROP MATERIALIZED VIEW sales_rep_dim;
CREATE MATERIALIZED VIEW sales_rep_dim
AS
  (SELECT c703_sales_rep_id sales_rep_key_id
  , c703_sales_rep_id sales_rep_id
  , c703_sales_rep_name sales_rep_name
  , c703_sales_rep_name_en sales_rep_name_en
  , c701_distributor_id distributor_key_id
  , c701_distributor_id distributor_id
  , phone_number
  , get_code_name (c703_rep_category) rep_category
  , c703_ship_add1 ship_add1
  , c703_ship_add2 ship_add2
  , c703_pager_number pager_number
  , email_Id
  , c703_ship_city ship_city
  , get_code_name (c703_ship_state) ship_state
  , get_code_name (c703_ship_country) ship_country
  , c703_ship_zip_code ship_zip_code
  , c703_sales_rep_sh_name sales_rep_sh_name
  , c702_territory_id territory_key_id
  , c702_territory_id territory_id
  , GET_TERRITORY_NAME (c702_territory_id) territory_name
  , c703_active_fl active_fl
  , c703_sales_fl sales_fl
  , get_code_name (c901_designation) designation
  , c703_start_date start_date
  , c703_end_date end_date
  , nvl(C703_EXT_REF_ID,c703_sales_rep_id) ext_ref_id
  , C901_EXT_COUNTRY_ID ext_country_id
  , get_code_name (C901_EXT_COUNTRY_ID) ext_country_name
  , C1900_COMPANY_ID Company_Key_Id
  , C1910_DIVISION_ID Division_Key_Id
  FROM t703_sales_rep t703
  , (SELECT Party_Id
    , MAX (DECODE (Contact_value_Id, '90452', Contact_Value)) Email_Id
    , MAX (DECODE ( Contact_value_Id, '90450', Contact_Value)) phone_number
    FROM
      (SELECT c101_party_id Party_Id
      , c107_contact_value Contact_Value
      , c901_mode Contact_value_Id
      FROM t107_contact t107
      WHERE c901_mode      IN (90452, 90450) --90452: Email, 90450: Business phone
      AND c107_void_fl     IS NULL
      AND c107_primary_fl   = 'Y'
      AND c107_inactive_fl IS NULL
      )
    GROUP BY Party_Id
    ) party
  WHERE c703_void_fl    IS NULL
  AND t703.c101_party_id = party.Party_Id (+)
  );
  ALTER TABLE SALES_REP_DIM ADD CONSTRAINT SALES_REP_DIM_PK PRIMARY KEY (SALES_REP_ID) ENABLE VALIDATE;