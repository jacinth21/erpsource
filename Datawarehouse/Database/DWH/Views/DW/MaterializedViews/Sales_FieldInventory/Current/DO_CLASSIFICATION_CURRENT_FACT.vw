/*
File name: DO_CLASSIFICATION_CURRENT_FACT.vw
Created By: gvaithiyanathan
Description: This Materialized view contains T501B_ORDER_BY_SYSTEMdata for last year and current year  
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Sales_FieldInventory\Current\DO_CLASSIFICATION_CURRENT_FACT.vw";
*/
DROP MATERIALIZED VIEW DO_CLASSIFICATION_CURRENT_FACT;
CREATE MATERIALIZED VIEW DO_CLASSIFICATION_CURRENT_FACT nocache logging nocompress noparallel refresh force ON demand
AS
  SELECT t501b.c501_order_id order_id ,
    t501b.c501_order_id order_key_id ,
    t501b.c207_system_id system_id ,
    t501b.c207_system_id system_key_id ,
    order_dim.account_key_id account_key_id ,
    order_dim.sales_rep_key_id sales_rep_key_id ,
    date_dim.date_key_id date_key_id ,
    t501b.c501b_qty do_qty ,
    t501b.c901_type do_inventory_type_id ,
    get_code_name(t501b.c901_type) do_inventory_type ,
    c501b_last_updated_by last_updated_by ,
    c501b_last_updated_date last_updated_date ,
    c501b_it_update_fl it_update_fl ,
    DECODE( rank() over( partition BY t501b.c501_order_id, system_dim.bod_segment_id order by system_dim.bod_segment_priority ),1,system_dim.bod_segment_id,NULL) bod_segment_rank_id ,
    DECODE( rank() over( partition BY t501b.c501_order_id, system_dim.bod_segment_id order by system_dim.bod_segment_priority ),1,system_dim.bod_segment_name,NULL) bod_segment
  FROM t501b_order_by_system t501b,
    order_current_dim order_dim,
    date_dim date_dim,
    system_dim system_dim
  WHERE t501b.c501_order_id = order_dim.order_key_id
  AND date_dim.cal_date     = order_dim.order_dt
  AND t501b.c207_system_id  = system_dim.system_key_id
  AND t501b.c501b_void_fl  IS NULL;