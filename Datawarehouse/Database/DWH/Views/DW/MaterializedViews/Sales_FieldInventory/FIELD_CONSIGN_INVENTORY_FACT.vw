/* Formatted on 2009/11/09 12:55 (Formatter Plus v4.8.0) */

CREATE MATERIALIZED VIEW FIELD_CONSIGN_INVENTORY_FACT 
NOCACHE
NOLOGGING
NOCOMPRESS
NOPARALLEL
REFRESH FORCE ON DEMAND
AS 
(SELECT 
  t730.c207_set_id set_key_id
, t730.c207_set_id set_id 
, t730.c701_distributor_id distributor_key_id
, t730.c701_distributor_id distributor_id 
, t731.c205_part_number_id part_key_id
, t731.c205_part_number_id part_id 
, DATE_DIM.date_key_id date_key_id
, DATE_DIM.date_key_id date_id 
, t731.c731_item_cogs_price cogs_cost
, t731.c731_item_price part_price
, t731.c731_item_qty qty 
, t730.C730_VIRTUAL_CONSIGNMENT_ID Logical_Consignment_Id 
FROM t730_virtual_consignment t730, t731_virtual_consign_item t731, DATE_DIM 
WHERE t730.c730_virtual_consignment_id = t731.c730_virtual_consignment_id 
AND DATE_DIM.cal_date = TRUNC(c730_last_load_dt) 
   );


--- Constraints

ALTER TABLE FIELD_CONSIGN_INVENTORY_FACT ADD 
CONSTRAINT FIELD_CNSGN_INV_FACT_PK
 PRIMARY KEY (Logical_Consignment_Id, PART_KEY_ID, DISTRIBUTOR_KEY_ID, DATE_KEY_ID)
 ENABLE VALIDATE;


