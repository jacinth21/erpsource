/*
File name: INVENTORY_LOCATION_LOG_CURRENT_FACT.vw
Created By: gvaithiyanathan
Description: This Materialized view contains t5054_inv_location_log data for last year and current year 
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Operations\Historical\INVENTORY_LOCATION_LOG_CURRENT_FACT.vw";
*/
DROP MATERIALIZED VIEW INVENTORY_LOCATION_LOG_CURRENT_FACT;
CREATE MATERIALIZED VIEW INVENTORY_LOCATION_LOG_CURRENT_FACT
AS
  (SELECT inv_loc_dim.location_key_id,
    part_dim.part_key_id part_key_id,
    date_dim.date_key_id date_key_id,
    DECODE (inv_loc_dim.warehouse_id, 3, inv_loc_dim.ref_id) distributor_key_id,
    t5054.c5054_txn_id transaction_id,
    (
    CASE
      WHEN t901.c901_code_grp='INVPT'
      THEN 'PICK'
      WHEN t901.c901_code_grp='INVPW'
      THEN 'PUT'
      WHEN t901.c901_code_grp='CONTY'
      THEN 'REPLAN'
      ELSE t901.c901_code_grp
    END) transaction_type,
    t901.c901_code_nm trans_type_detail,
    t5054.c5054_orig_qty original_qty,
    t5054.c5054_txn_qty transaction_qty,
    t5054.c5054_new_qty new_qty
  FROM inventory_location_dim inv_loc_dim,
    t5054_inv_location_log t5054,
    t901_code_lookup t901,
    part_dim,
    date_dim
  WHERE inv_loc_dim.location_key_id  =t5054.c5052_location_id
  AND t5054.c901_action              = t901.c901_code_id(+)
  AND t5054.c205_part_number_id      =part_dim.part_key_id
  AND TRUNC(t5054.c5054_created_date)=date_dim.cal_date
  AND t5054.c5054_created_date       >= TRUNC(ADD_MONTHS(SYSDATE,-12),'MM')
  ) ;