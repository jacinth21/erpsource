/*
File name: RETURNS_CURRENT_FACT.vw
Created By: gvaithiyanathan
Description: This Materialized view contains Returns data for last year and current year 
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Operations\Current\RETURNS_CURRENT_FACT.vw";
*/
DROP materialized VIEW RETURNS_CURRENT_FACT;
CREATE MATERIALIZED VIEW RETURNS_CURRENT_FACT NOCACHE LOGGING NOCOMPRESS NOPARALLEL REFRESH FORCE ON DEMAND
AS
  SELECT returns_item.returns_item_key_id ,
    returns.returns_key_id returns_key_id ,
    returns.account_key_id account_key_id ,
    returns.sales_rep_id sales_rep_key_id ,
    returns.distributor_key_id distributor_key_id ,
    returns.set_key_id set_key_id ,
    returns.order_key_id order_key_id ,
    datedim.date_key_id returns_date_key_id ,
    returns_item.part_key_id part_key_id ,
    returns_item.item_price item_price ,
    returns_item.item_qty item_qty ,
    (returns_item.item_qty * returns_item.item_price) total_price
  FROM returns_current_dim returns,
    returns_item_current_dim returns_item,
    date_dim datedim
  WHERE returns.returns_key_id      = returns_item.returns_key_id
  AND TRUNC(returns.reprocess_date) = datedim.cal_date (+);