/*
File name: RETURNS_CURRENT_DIM.vw
Created By: gvaithiyanathan
Description: This Materialized view contains Returns data for last year and current year 
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Operations\Current\RETURNS_CURRENT_DIM.vw";
*/
DROP MATERIALIZED VIEW RETURNS_CURRENT_DIM;
CREATE MATERIALIZED VIEW RETURNS_CURRENT_DIM
AS
  (SELECT t506.c506_rma_id returns_id ,
    t506.c506_rma_id returns_key_id ,
    t506.c704_account_id account_key_id ,
    t506.c703_sales_rep_id sales_rep_id ,
    t506.c701_distributor_id distributor_key_id ,
    t506.c207_set_id set_key_id ,
    t506.c501_order_id order_key_id ,
    t506.c503_invoice_id invoice_id ,
    t506.c506_comments comments ,
    t506.c506_created_date created_date ,
    t506.c506_type returns_type_id ,
    get_code_name(t506.c506_type) returns_type ,
    t506.c506_reason returns_reason_id ,
    get_code_name(t506.c506_reason) returns_reason ,
    DECODE(T506.C506_STATUS_FL,'0','Initiated','1','Pending Credit','2','Reprocessed') returns_status ,
    get_user_name(t506.c506_created_by) created_by ,
    t506.c506_credit_memo_id credit_memo_id ,
    t506.c506_expected_date returns_expected_date ,
    t506.c506_last_updated_date last_updated_date ,
    get_user_name(t506.c506_last_updated_by) last_updated_by ,
    t506.c506_credit_date credit_date ,
    t506.c506_return_date return_date_time ,
    TRUNC(t506.c506_return_date) return_date ,
    t506.c506_update_inv_fl update_inv_fl ,
    t506.qb_consignment_txnid qb_consignment_txnid ,
    t506.c501_reprocess_date reprocess_date ,
    t506.c101_user_id user_id ,
    get_user_name(t506.c101_user_id) user_name ,
    t506.c506_ac_credit_dt ac_credit_date ,
    t506.c506_void_fl void_fl ,
    t506.c506_parent_rma_id parent_rma_id ,
    t506.c506_associated_rma_id associated_rma_id ,
    t506.c1900_company_id company_key_id
  FROM t506_returns t506
  WHERE c1900_company_id      = 1000 -- 1000:globus north america
  AND t506.c506_created_date >= TRUNC(ADD_MONTHS(SYSDATE,-12),'MM')
  );
  /
  ALTER TABLE RETURNS_CURRENT_DIM ADD CONSTRAINT RETURNS_CURRENT_DIM_PK PRIMARY KEY (returns_key_id) enable VALIDATE; 