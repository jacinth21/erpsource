/*
File name: ITEM_CONSIGNMENT_MASTER_CURRENT_DIM.vw
Created By: gvaithiyanathan
Description: This Materialized view contains Item_Consignment data for last year and current year 
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Operations\Current\ITEM_CONSIGNMENT_MASTER_CURRENT_DIM.vw";
*/
DROP MATERIALIZED VIEW ITEM_CONSIGNMENT_MASTER_CURRENT_DIM;
CREATE MATERIALIZED VIEW ITEM_CONSIGNMENT_MASTER_CURRENT_DIM nocache logging nocompress noparallel refresh force ON demand
AS
  (SELECT t505.c505_item_consignment_id item_consignment_key_id,
    t505.c504_consignment_id consignment_key_id,
    t505.c505_control_number control_number,
    t505.c505_item_qty consignment_qty,
    t505.c505_item_price item_price,
    t505.c505_void_fl void_flag,
    t505.c205_part_number_id part_key_id,
    t505.c901_type item_consignment_type_id,
    NVL(c901_code_nm,' ') item_consignment_type_name,
    t505.c505_ref_id item_consignment_ref_id,
    t504.c504_type consignment_type_id,
    t504.c5040_plant_id plant_id
  FROM t505_item_consignment t505,
    t504_consignment t504 ,
    t901_code_lookup
  WHERE t505.c504_consignment_id=t504.c504_consignment_id
  AND t505.c901_type            =c901_code_id(+)
  AND t504.c504_created_date    >= TRUNC(ADD_MONTHS(SYSDATE,-12),'MM')
  );