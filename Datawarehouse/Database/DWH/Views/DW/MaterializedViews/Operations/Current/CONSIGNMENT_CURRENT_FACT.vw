/*
File name: CONSIGNMENT_CURRENT_FACT.vw
Created By: gvaithiyanathan
Description: This Materialized view contains Consignment data for last year and current year 
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Operations\Current\CONSIGNMENT_CURRENT_FACT.vw";
*/
DROP MATERIALIZED VIEW CONSIGNMENT_CURRENT_FACT;
CREATE MATERIALIZED VIEW CONSIGNMENT_CURRENT_FACT NOCACHE NOLOGGING NOCOMPRESS NOPARALLEL REFRESH FORCE ON DEMAND
AS
  (SELECT item_consign_dim.item_consignment_key_id item_consignment_key_id,
    consign_dim.consignment_id consignment_key_id ,
    consign_dim.request_id request_key_id ,
    DECODE(item_consign_dim.item_consignment_key_id,NULL,-1,date_dim.date_key_id) date_key_id ,
    DECODE(item_consign_dim.item_consignment_key_id,NULL,-1,consign_shipping_dim.shipping_id) shipping_key_id ,
    DECODE(item_consign_dim.item_consignment_key_id,NULL,NULL,consign_dim.consignment_distributor_id) distributor_key_id,
    consign_dim.consignment_set_id set_key_id,
    item_consign_dim.part_key_id part_key_id ,
    item_consign_dim.consignment_qty,
    item_consign_dim.item_price Consignment_Price,
    item_consign_dim.set_part_qty ,
    consign_dim.company_key_id company_key_id ,
    consign_dim.plant_key_id plant_key_id ,
    consign_dim.division_key_id division_key_id
  FROM consignment_current_dim consign_dim ,
    item_consignment_current_dim item_consign_dim ,
    date_dim ,
    (SELECT shipping_id,
      ref_id,
      Shipped_Date
    FROM shipping_dim
    WHERE source_code_id      = 50181
    AND NVL (active_fl, 'Y') != 'N'
    ) consign_shipping_dim
  WHERE consign_dim.consignment_id             = item_consign_dim.consignment_key_id(+)
  AND TRUNC(consign_dim.consignment_ship_date) = date_dim.cal_date (+)
  AND consign_dim.consignment_id               = consign_shipping_dim.ref_id(+)
  );