/*
File name: REQUEST_DIM.vw
Created By: gvaithiyanathan
Description: This is a view combines both REQUEST_HISTORICAL_DIM and REQUEST_CURRENT_DIM
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Opreations\REQUEST_DIM.vw";
*/

CREATE OR REPLACE VIEW REQUEST_DIM
AS
  (SELECT * FROM REQUEST_HISTORICAL_DIM
  UNION ALL
  SELECT * FROM REQUEST_CURRENT_DIM
  );
  
  /