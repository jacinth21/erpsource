/*
This dim will have the part with control number and its expiry information
*/
DROP MATERIALIZED VIEW part_control_number_dim;

CREATE MATERIALIZED VIEW part_control_number_dim
NOCACHE
LOGGING
NOCOMPRESS
NOPARALLEL
REFRESH FORCE ON DEMAND
AS
(
SELECT t2550.c2550_part_control_number_id part_control_number_key_id, part_dim.part_key_id part_key_id
, t2550.c2550_control_number control_number, date_dim.date_key_id expiry_date_key_id, date_dim.cal_date expiry_date
, t2550.c2550_last_updated_date last_updated_date, get_user_name(t2550.c2550_last_updated_by) last_updated_by
FROM t2550_part_control_number t2550, part_dim part_dim, date_dim date_dim
WHERE t2550.c205_part_number_id = part_dim.part_key_id
AND t2550.c2550_expiry_date = date_dim.cal_date
);