DROP MATERIALIZED VIEW TAG_LOG_FACT;


CREATE MATERIALIZED VIEW TAG_LOG_FACT
NOCACHE
NOLOGGING
NOCOMPRESS
NOPARALLEL
REFRESH FORCE ON DEMAND
AS
(SELECT
TAG.Tag_Key_Id Tag_Key_Id,
T5011.c205_part_number_id Part_Key_Id,
Date_Dim.date_key_id Date_Key_id,
T5011.c207_set_id Set_Key_ID,
Tag_Void_dim.void_log_id Void_Log_Key_Id,
CASE WHEN (T5011.c901_location_type = 4120 AND T5011.c5011_location_id != 'In-House') THEN T5011.c5011_location_id END Consigned_Distributor_Key_id,
Decode(T5011.c901_location_type,4123,T5011.c5011_location_id) Consigned_Employee_Key_id,
Decode(T5011.c901_location_type,40033,T5011.c5011_location_id) Consigned_Inhouse_Key_id,
T5011.c5011_location_id location_id,
T5011.c5011_Sub_location_id sub_location_id,
Decode(T5011.c901_sub_location_type,11301,T5011.c5011_Sub_location_id) Location_Accout_Key_id,
Decode(T5011.c901_Sub_location_type,50221,T5011.c5011_Sub_location_id) Location_Distributor_Key_id,
Decode(T5011.c901_Sub_location_type,50222,T5011.c5011_Sub_location_id) Location_Sales_Rep_Key_Id,
gm_dw_pkg_op_field_operations.get_addr_key_id(T5011.c5011_location_id,T5011.c901_location_type,c106_Address_Id) Address_Key_ID,
T5011.c5011_last_updated_trans_id Transaction_ID,
t5011.c5011_control_number Control_Number,
get_user_name(t5011.c5011_Created_By) Created_By,
t5011.c5011_Created_Date Created_Date,
t5011.c5011_Void_Fl Void_Fl,
get_code_name(t5011.c901_Status) Status,
t5011.c5011_Lock_Fl Lock_Fl,
get_code_name(t5011.c901_trans_type) Transaction_Type,
get_code_name(T5011.c901_location_type) Consigned_Type,
get_user_name(Decode(c901_location_type,4123,c5011_location_id)) Consigned_Employee_Name,
get_code_name(Decode(c901_location_type,40033,c5011_location_id)) Consigned_Inhouse_Type,
get_code_name(c901_sub_location_type) Location_Type,
get_code_name(Decode(T5011.c901_sub_location_type,11301,T5011.c5011_Sub_location_id)) Location_Account_Name,
get_rep_name(DECODE(T5011.c901_Sub_location_type,50222,T5011.c5011_Sub_location_id)) Location_Sales_Rep_Name,
get_distributor_name(DECODE(T5011.c901_Sub_location_type,50221,T5011.c5011_Sub_location_id)) Location_Distributor_Name,
T5011.c704_account_id Account_id,
get_account_name(T5011.c704_account_id) Account_Name,
T5011.c703_sales_rep_id Sales_Rep_ID,
get_rep_name(T5011.c703_sales_rep_id) Sales_Rep_Name,
T5011.c5011_created_date Transaction_Date
FROM
TAG_DIM TAG, T5011_TAG_LOG T5011,Date_Dim Date_Dim,(select void_log_id,void_type_id,void_ref_id from void_dim where void_type_id in(90896,91961)) Tag_Void_dim
WHERE TAG.tag_key_id = T5011.c5010_tag_id
AND trunc(T5011.c5011_created_date) = Date_Dim.cal_date(+)
AND T5011.c5010_tag_id = Tag_Void_dim.void_ref_id(+));


