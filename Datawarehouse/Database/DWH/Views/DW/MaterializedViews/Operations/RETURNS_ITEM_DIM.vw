/*
File name: RETURNS_ITEM_DIM.vw
Created By: gvaithiyanathan
Description: This is a view combines both RETURNS_ITEM_HISTORICAL_DIM and RETURNS_ITEM_CURRENT_DIM
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Opreations\RETURNS_ITEM_DIM.vw";
*/

CREATE OR REPLACE VIEW RETURNS_ITEM_DIM
AS
  (SELECT * FROM RETURNS_ITEM_HISTORICAL_DIM
  UNION ALL
  SELECT * FROM RETURNS_ITEM_CURRENT_DIM
  );
  
  /