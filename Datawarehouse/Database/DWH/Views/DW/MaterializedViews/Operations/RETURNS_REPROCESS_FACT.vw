DROP MATERIALIZED VIEW RETURNS_REPROCESS_FACT;

/*
This will hold the returns fact information
*/
CREATE MATERIALIZED VIEW RETURNS_REPROCESS_FACT
NOCACHE
LOGGING
NOCOMPRESS
NOPARALLEL
REFRESH FORCE ON DEMAND
AS
SELECT item_csg.item_consignment_key_id
    , consignment.consignment_key_id reprocess_id
    , returns.returns_key_id
    , consignment.consignment_created_date reprocess_created_date
    , datedim.date_key_id reprocess_created_date_key_id
    , item_csg.part_key_id part_key_id
    , item_csg.ITEM_PRICE item_price
    , item_csg.consignment_qty item_qty
    , (item_csg.consignment_qty * item_csg.item_price) total_price
  FROM returns_dim returns, consignment_dim consignment, item_consignment_dim item_csg, date_dim datedim
 WHERE consignment.consignment_reprocess_id = returns.returns_id
   AND item_csg.consignment_key_id = consignment.consignment_key_id
   AND datedim.cal_date = consignment.consignment_created_date
   AND consignment.consignment_type_id IN (4114, 4113, 4110);