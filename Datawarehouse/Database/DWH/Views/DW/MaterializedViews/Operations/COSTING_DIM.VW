DROP materialized VIEW costing_dim;
CREATE materialized VIEW costing_dim
AS
  (SELECT c820_costing_id costing_key_id,
    c820_costing_id costing_id,
    c205_part_number_id part_key_id,
    c820_part_received_dt part_received_date,
    c820_qty_received qty_received,
    c820_purchase_amt purchase_amount,
    c820_txn_id transaction_id,
    c820_qty_on_hand qty_on_hand,
    c820_qty_added qty_added,
    NVL(t901_status.c901_code_nm,' ') status,
    NVL(t101_created_by.c101_user_f_name
    ||' '
    ||t101_created_by.c101_user_l_name,' ') created_by,
    c820_created_date created_date,
    NVL(t101_updated_by.c101_user_f_name
    ||' '
    ||t101_updated_by.c101_user_l_name,' ') updated_by,
    c820_last_updated_date last_updated_date,
    c901_costing_type costing_type_id,
    NVL(t901_costing_type_name.c901_code_nm,' ') costing_type_name,
    c820_purchase_amt_temp purchase_amt_temp
  FROM t820_costing,
    t901_code_lookup t901_status,
    t901_code_lookup t901_costing_type_name,
    t101_user t101_created_by,
    t101_user t101_updated_by
  WHERE c901_status      IN (4801,4800) --active,pending progress
  AND c901_status         =t901_status.c901_code_id(+)
  AND c901_costing_type   =t901_costing_type_name.c901_code_id(+)
  AND c820_created_by     =t101_created_by.c101_user_id(+)
  AND c820_last_updated_by=t101_updated_by.c101_user_id(+)
  );
  ALTER TABLE costing_dim ADD CONSTRAINT costing_dim_pk PRIMARY KEY (costing_key_id) enable VALIDATE;
  /