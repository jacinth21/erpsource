/*
File name: REQUEST_HISTORICAL_DIM.vw
Created By: gvaithiyanathan
Description: This Materialized view contains Request data upto last 2years. 
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Operations\Historical\REQUEST_HISTORICAL_DIM.vw";
*/
DROP MATERIALIZED VIEW REQUEST_HISTORICAL_DIM;
CREATE MATERIALIZED VIEW REQUEST_HISTORICAL_DIM
AS
  (SELECT t520.c520_request_id request_key_id ,
    t520.c520_request_id request_id ,
   t520.c5403_request_priority priority_id,
    TRUNC(t520.c520_request_date) request_date ,
    TRUNC(t520.c520_required_date) required_date ,
    NVL(t901_request_for.c901_code_nm,' ') request_for ,
    t520.c520_request_for request_for_id ,
    t520.c520_request_to request_to_id ,
    t520.c901_ship_to request_ship_to ,
    t520.c520_ship_to_id request_ship_to_id ,
    CASE
      WHEN t520.c901_request_source IN (50616,4000122)
      THEN NVL(c4020_demand_nm,0)
      ELSE t520.c520_request_txn_id
    END request_transaction_name ,
    CASE
      WHEN t520.c520_request_for =40021
      THEN get_distributor_name(t520.c520_request_to)
      WHEN t520.c520_request_for =40022
      THEN t101_request_to_name.c101_user_f_name
        || ' '
        || t101_request_to_name.c101_user_l_name
      ELSE t520.c520_request_to
    END request_to_name ,
    NVL(t901_ship_to_type.c901_code_nm,' ') ship_to_type , -- dist/rep
    NVL(t101_request_by.c101_user_f_name
    || ' '
    || t101_request_by.c101_user_l_name,' ') request_by ,
    t520.c520_void_fl void_fl ,
    NVL(t101_created_by.c101_user_f_name
    || ' '
    || t101_created_by.c101_user_l_name,' ') created_by ,
    DECODE(t520.c520_status_fl,0,'Requested', 10,'Back Order', 15,'Back Log', 20,'Ready To Consign', 30,'Ready To Ship', 40,'Completed') request_status ,
    TRUNC(t520.c520_created_date) create_date ,
    NVL(t101_last_updated_by.c101_user_f_name
    || ' '
    || t101_last_updated_by.c101_user_l_name,' ') last_updated_by ,
    TRUNC(t520.c520_last_updated_date) last_updated_date ,
    t520.c520_master_request_id master_request_id ,
    NVL(t901_request_source.c901_code_nm,' ') request_source ,
    NVL(t901_request_by_type.c901_code_nm,' ') request_by_type ,
    t520.c207_set_id set_id ,
    NVL(t901_request_purpose.c901_code_nm,' ') request_purpose ,
    DECODE(t520p.c520_status_fl,0,'Requested', 10,'Back Order', 15,'Back Log', 20,'Ready To Consign', 30,'Ready To Ship', 40,'Completed') parent_request_status ,
    t520p.c207_set_id parent_set_id ,
    t520.c1900_company_id company_key_id ,
    t520.c5040_plant_id plant_key_id
  FROM t520_request t520 ,
    t520_request t520p ,
    t901_code_lookup t901_request_for ,
    t901_code_lookup t901_ship_to_type ,
    t901_code_lookup t901_request_source ,
    t901_code_lookup t901_request_by_type ,
    t901_code_lookup t901_request_purpose ,
    t101_user t101_request_to_name ,
    t101_user t101_request_by ,
    t101_user t101_created_by ,
    t101_user t101_last_updated_by ,
    t4020_demand_master t4020
  WHERE t520.c520_master_request_id = t520p.c520_request_id (+)
  AND t520.c1900_company_id         = 1000 -- 1000:globus north america
  AND t520.c520_request_for         =t901_request_for.c901_code_id (+)
  AND t520.c901_ship_to             =t901_ship_to_type.c901_code_id (+)
  AND t520.c901_request_source      =t901_request_source.c901_code_id (+)
  AND t520.c901_request_by_type     =t901_request_by_type.c901_code_id (+)
  AND t520.c901_purpose             =t901_request_purpose.c901_code_id (+)
  AND t520.c520_request_to          =t101_request_to_name.c101_user_id (+)
  AND t520.c520_request_by          =t101_request_by.c101_user_id (+)
  AND t520.c520_created_by          =t101_created_by.c101_user_id (+)
  AND t520.c520_last_updated_by     =t101_last_updated_by.c101_user_id (+)
  AND t520.c520_request_txn_id      =t4020.c4020_demand_master_id(+)
  AND t520.c520_created_date        <= LAST_DAY(ADD_MONTHS(SYSDATE,-13))
  );
  /
  ALTER TABLE REQUEST_HISTORICAL_DIM ADD CONSTRAINT REQUEST_HISTORICAL_DIM_PK PRIMARY KEY (request_key_id) enable VALIDATE;  