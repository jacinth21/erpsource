/*
File name: REQUEST_DETAIL_HISTORICAL_DIM.vw
Created By: gvaithiyanathan
Description: This Materialized view contains Request_details data upto last 2years. 
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Operations\Historical\REQUEST_DETAIL_HISTORICAL_DIM.vw";
*/
DROP MATERIALIZED VIEW REQUEST_DETAIL_HISTORICAL_DIM;
CREATE MATERIALIZED VIEW REQUEST_DETAIL_HISTORICAL_DIM nocache logging nocompress noparallel refresh force ON demand
AS
  (SELECT t521.c521_request_detail_id request_detail_key_id,
    t521.c520_request_id request_key_id,
    t521.c205_part_number_id part_key_id,
    t521.c521_qty quantity
  FROM t521_request_detail t521,
    t520_request t520
  WHERE t521.c520_request_id =t520.c520_request_id
  AND t520.c520_created_date <= LAST_DAY(ADD_MONTHS(SYSDATE,-13))
  );