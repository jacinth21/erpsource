DROP MATERIALIZED VIEW TAG_DIM;
CREATE MATERIALIZED VIEW TAG_DIM
AS
  (SELECT T5010.C5010_TAG_ID Tag_Key_ID
  , T5010.C5010_TAG_ID Tag_Id
  , T5010.C5010_VOID_FL Void_Flag
  , T5010.C5010_HISTORY_FL History_Flag
  , T5010.C5010_LOCK_FL Lock_Flag
  , T5010.c5010_last_updated_trans_id Transaction_ID
  , t5010.c5010_control_number Control_Number
  , get_code_name(T5010.c901_location_type) Consigned_Type
  , get_user_name(DECODE(T5010.c901_location_type,4123,T5010.c5010_location_id)) Consigned_Employee_Name
  , get_code_name(DECODE(T5010.c901_location_type,40033,T5010.c5010_location_id)) Consigned_Inhouse_Type
  , get_code_name(t5010.c901_sub_location_type) Location_Type
  , get_account_name(DECODE(T5010.c901_sub_location_type,11301,T5010.c5010_Sub_location_id)) Location_Account_Name
  , get_rep_name(DECODE(T5010.c901_Sub_location_type,50222,T5010.c5010_Sub_location_id)) Location_Sales_Rep_Name
  , get_distributor_name(DECODE(T5010.c901_Sub_location_type,50221,T5010.c5010_Sub_location_id)) Location_Distributor_Name
  , get_rep_name(T5010.c703_sales_rep_id) Sales_Rep_Name
  , DECODE(T5010.c5010_last_updated_date,NULL,T5010.c5010_created_date,T5010.C5010_LAST_updated_Date) Transaction_Date
  , get_account_name(T5010.c704_account_id) Account_Name
  , get_user_name(T5010.C5010_CREATED_BY) Created_By
  , T5010.C5010_CREATED_DATE Created_Date
  , get_user_name(T5010.C5010_LAST_UPDATED_BY) Last_Updated_By
  , T5010.C5010_LAST_UPDATED_Date Last_Updated_Date
  , get_code_name(T5010.C901_STATUS) Tag_Status
  , get_code_name(C901_INVENTORY_TYPE) Tag_Inventory_Type_Name
  , C901_INVENTORY_TYPE Tag_Inventory_Type_Id
  , C1900_COMPANY_ID Company_Key_Id
  , C5040_PLANT_ID Plant_Key_Id
  FROM T5010_TAG T5010
  --WHERE C1900_COMPANY_ID = 1000 -- 1000:Globus North America
  );
  ALTER TABLE TAG_DIM ADD CONSTRAINT TAG_DIM_PK PRIMARY KEY (TAG_KEY_ID) ENABLE VALIDATE;
  /