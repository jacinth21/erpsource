DROP MATERIALIZED VIEW PRODUCT_REQUEST_DIM;
CREATE MATERIALIZED VIEW PRODUCT_REQUEST_DIM
AS
  (SELECT C525_PRODUCT_REQUEST_ID Product_Request_Key_ID
  , C525_PRODUCT_REQUEST_ID Product_Request_Id
  , C525_REQUESTED_DATE Product_Request_Date
  , C901_REQUEST_FOR Prodcut_Request_For_Code_Id
  , GET_CODE_NAME(C901_REQUEST_FOR) Product_Request_For_Code_Name
  , C525_REQUEST_FOR_ID Distributor_Id
  , C901_REQUEST_BY_TYPE Product_Request_By_ID
  , DECODE(C901_REQUEST_BY_TYPE,50626,'Sales Rep',50625,'Employee') Product_Request_By_Type
  , GET_USER_NAME(C525_REQUEST_BY_ID) REQUEST_BY_Name
  , C901_SHIP_TO Shipto_Code
  , Get_code_name(C901_SHIP_TO) Ship_To_Type
  , --EMP, REP, DIST OR HOSPITAL
    C525_SHIP_TO_ID Ship_To_ID
  , DECODE(C901_SHIP_TO,4121, get_rep_name(C525_SHIP_TO_ID),4122, get_account_name(C525_SHIP_TO_ID),4120, get_distributor_name(C525_SHIP_TO_ID),4123, get_user_name(C525_SHIP_TO_ID),50625,get_user_name(C525_SHIP_TO_ID)) shiptoname
  , DECODE(C525_STATUS_FL,10,'Open',40,'Closed') Product_Request_Status
  , C525_VOID_FL Void_Flag
  , get_user_name(C525_CREATED_BY) Created_BY
  , C525_CREATED_DATE Created_Date
  , get_user_name(C525_LAST_UPDATED_BY) Last_Updated_By
  , C525_LAST_UPDATED_DATE Last_Updated_Date
  , C704_ACCOUNT_ID Account_Id
  , C703_SALES_REP_ID Sales_Rep_Id
  , C525_SURGERY_DATE Surgery_Date
  , C525_USAGE_DATE Usage_Date
  , DECODE(C525_USAGE_STATUS,10,'Open',20,'Non Usage',30,'Usage') Usage_Status
  , C525_CHARGE_DATE Charge_Date
  , C525_CHARGE_STATUS Charge_Status
  , C525_EMAIL_COUNT Email_Count
  , C525_EMAIL_DATE Email_Date
  , C1900_COMPANY_ID Company_Key_Id
  , C5040_PLANT_ID Plant_Key_Id
  FROM T525_PRODUCT_REQUEST
  WHERE C1900_COMPANY_ID = 1000 -- 1000:Globus North America
  );
  ALTER TABLE PRODUCT_REQUEST_DIM ADD CONSTRAINT PRODUCT_REQUEST_DIM_PK PRIMARY KEY (Product_Request_Key_ID) ENABLE VALIDATE;
  /