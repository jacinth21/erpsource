--@"C:\Database\DW_BI\MaterializedViews\Operations\PART_FORECAST_FACT.vw";
/************************************************************************
* Description   : This Fact table will store all Forecast Key details
* Author        : Dave Lindner
************************************************************************/
DROP MATERIALIZED VIEW PART_FORECAST_FACT;
CREATE MATERIALIZED VIEW PART_FORECAST_FACT
AS
  (SELECT PART_FORECAST_DIM.PART_FORECAST_KEY_ID
  , DATE_DIM.DATE_KEY_ID
  , PART_FORECAST_DIM.PART_NUMBER AS PART_KEY_ID
  , PART_FORECAST_DIM.QUANTITY
  FROM DATE_DIM
  , PART_FORECAST_DIM
  WHERE PART_FORECAST_DIM.FORECAST_DATE = DATE_DIM.CAL_DATE
  );
  /