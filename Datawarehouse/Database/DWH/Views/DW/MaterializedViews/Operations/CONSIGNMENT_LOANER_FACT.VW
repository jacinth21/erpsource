DROP MATERIALIZED VIEW CONSIGNMENT_LOANER_FACT;

CREATE MATERIALIZED VIEW CONSIGNMENT_LOANER_FACT
NOCACHE
NOLOGGING
NOCOMPRESS
NOPARALLEL
REFRESH FORCE ON DEMAND
AS
(
SELECT
Consign_Loaner_Dim.Consignment_Key_Id,
Consign_Master_Dim.consignment_distributor_id Distributor_key_Id,
Consign_Master_Dim.consignment_set_id Set_Key_Id,
Date_Dim.date_key_id Date_Key_Id,
Consign_Loaner_Dim.Plant_Key_Id
FROM CONSIGNMENT_LOANER_DIM Consign_Loaner_Dim, CONSIGNMENT_MASTER_DIM Consign_Master_Dim, Date_dim Date_Dim
where Consign_Loaner_Dim.Consignment_Key_Id = Consign_Master_Dim.Consignment_Key_Id
and Trunc(Consign_Loaner_Dim.Loaner_Created_Date) = Date_Dim.cal_date(+));


