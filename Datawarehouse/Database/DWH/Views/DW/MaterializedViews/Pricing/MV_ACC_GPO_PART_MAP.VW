-- @"D:\Database\DW_BI\MaterializedViews\Pricing\MV_ACC_GPO_PART_MAP.VW";
/*********************************************************************************************
 * Author	   : Velu
 * Description : This Materialized view taking all the Group Part Pricing 
 * 				 and GPO Mapping and Account Group Pricing  and Account pricing informations.
 * 				 The columns are PNUM, ACC ID, GPOID from this view to Account Pricing Report for BO. 
 * ********************************************************************************************** 
 */
DROP MATERIALIZED VIEW MV_ACC_GPO_PART_MAP;
---- group to part level pricing of accounts and GPO
CREATE MATERIALIZED VIEW MV_ACC_GPO_PART_MAP
AS
	SELECT C704_ACCOUNT_ID, MAX(C101_GPO_ID) C101_GPO_ID, C205_PART_NUMBER_ID FROM (        
		     SELECT C704_ACCOUNT_ID, C101_GPO_ID, C205_PART_NUMBER_ID
		       FROM V_GROUP_PART_PRICING
		      WHERE C101_GPO_ID IS NULL
		  UNION ALL
		     SELECT VAGM.accid, vgpp.C101_GPO_ID, vgpp.C205_PART_NUMBER_ID
		       FROM V_GROUP_PART_PRICING VGPP, V_ACC_GPO_PARTY_MAP VAGM
		      WHERE VGPP.C101_GPO_ID  = VAGM.GPOID
		        AND VGPP.C101_GPO_ID IS NOT NULL
		  UNION ALL
		    -- part level pricing of accounts and gpo
		     SELECT C704_ACCOUNT_ID, C101_PARTY_ID, C205_PART_NUMBER_ID
		       FROM T705_ACCOUNT_PRICING
		      WHERE C101_PARTY_ID IS NULL
		        AND C705_VOID_FL  IS NULL
		  UNION ALL
		     SELECT VAGM.ACCID, T705.C101_PARTY_ID, T705.C205_PART_NUMBER_ID
		       FROM T705_ACCOUNT_PRICING T705, V_ACC_GPO_PARTY_MAP VAGM
		      WHERE T705.C101_PARTY_ID  = VAGM.GPOID
		        AND T705.C101_PARTY_ID IS NOT NULL
		        AND C705_VOID_FL       IS NULL
		)PRICING 
	GROUP BY C704_ACCOUNT_ID,C205_PART_NUMBER_ID;