DROP MATERIALIZED VIEW ACCOUNT_PRICING_DIM;
CREATE materialized VIEW ACCOUNT_PRICING_DIM
AS
  (SELECT t704.c704_account_id account_key_id,
  t740.c101_party_id party_key_id,
  c205_part_number_id part_key_id,
  c705_unit_price unit_price,
  DECODE((t740.c101_party_id) , NULL, 'Account' , 'GPB' ) price_source,
  t704.c704_active_fl active_fl
FROM t704_account t704,
  t740_gpo_account_mapping t740,
  t705_account_pricing t705,
  t101_user t101_created_by,
  t101_user t101_last_updated_by
WHERE t704.c704_account_id   =t740.c704_account_id(+)
AND t704.c101_party_id      = t705.c101_party_id
AND t704.c704_void_fl       IS NULL
AND t705.c705_void_fl       IS NULL
AND t740.c740_void_fl       IS NULL
AND t705.c705_created_by     =t101_created_by.c101_user_id(+)
AND t705.c705_last_updated_by=t101_last_updated_by.c101_user_id(+)
  );