 -- @"c:\database\dw_bi\materializedviews\common\address_dim.vw"
DROP MATERIALIZED VIEW ADDRESS_DIM;

CREATE MATERIALIZED VIEW ADDRESS_DIM
NOCACHE
NOLOGGING
NOCOMPRESS
NOPARALLEL
REFRESH FORCE ON DEMAND
AS
(
SELECT s106_address_Key.nextval ADDRESS_KEY_ID,
  ADDRESS_ID,
  ADDR1 ADDRESS1,
  ADDR2 ADDRESS2,
  CITY,
  STATE,
  COUNTRY,
  zip ZIPCODE,
  PARTY_KEY_ID,
  REF_ID,
  REF_TYPE,
  REF_TYPE_ID,
  ADDRESS_TYPE,
  PRIMARY_FL
FROM
  (SELECT C106_ADDRESS_ID Address_Id,
    C106_ADD1 Addr1,
    C106_ADD2 Addr2,
    C106_CITY City,
    get_code_name(C901_STATE) State,
    get_code_name(C901_COUNTRY) Country,
    C106_ZIP_CODE Zip,
    T101.C101_PARTY_ID Party_Key_Id,
    DECODE(T101.c901_Party_type,7005,T101.c101_Party_ID,NULL) Ref_ID,
    DECODE(T101.c901_Party_type,7005,get_code_name(T101.c901_Party_type),NULL) Ref_Type,
    DECODE(T101.c901_Party_type,7005,T101.c901_Party_type,NULL) Ref_Type_Id,
    get_code_name(C901_ADDRESS_TYPE) Address_Type,
    C106_PRIMARY_FL Primary_Fl
  FROM T106_ADDRESS T106,
    T101_PARTY T101
  WHERE T106.c101_party_id = T101.c101_party_id
  AND T106.C106_VOID_FL   IS NULL
  AND T101.C101_VOID_FL   IS NULL
  UNION ALL
  SELECT NULL Address_id,
    Billing_Address Addr1,
    NULL Addr2,
    BILLING_CITY City,
    BILLING_STATE State,
    BILLING_COUNTRY Country,
    BILLING_ZIP_CODE Zip,
    NULL Party_Key_Id,
    To_Number(Distributor_Id) Ref_Id,
    GET_CODE_NAME(4120) Ref_Type,
    4120 Ref_Type_Id,
    get_code_name(101020) Address_Type,
    NULL Primary_Fl
  FROM DISTRIBUTOR_DIM
  UNION ALL
  SELECT NULL Address_id,
    SHIP_Address1 Addr1,
    SHIP_ADDRESS2 Addr2,
    SHIP_CITY City,
    SHIP_STATE State,
    SHIP_COUNTRY Country,
    SHIP_ZIP_CODE Zip,
    NULL Party_Key_Id,
    To_Number(Distributor_Id) Ref_Id,
    GET_CODE_NAME(4120) Ref_Type,
    4120 Ref_Type_Id,
    get_code_name(101021) Address_Type,
    'Y' Primary_Fl
  FROM DISTRIBUTOR_DIM
  UNION ALL
  SELECT NULL Address_id,
    Add1 Addr1,
    Add2 Addr2,
    CITY City,
    BILL_STATE State,
    BILL_COUNTRY Country,
    ZIP_Code Zip,
    NULL Party_Key_Id,
    To_Number(Account_Id) Ref_Id,
    GET_CODE_NAME(11301) Ref_Type,
    11301 Ref_Type_Id,
    get_code_name(101020) Address_Type,
    NULL Primary_Fl
  FROM ACCOUNT_DIM
  UNION ALL
  SELECT NULL Address_id,
    SHIP_ADD1 Addr1,
    SHIP_ADD2 Addr2,
    SHIP_CITY City,
    SHIP_STATE State,
    SHIP_COUNTRY Country,
    SHIP_ZIP_CODE Zip,
    NULL Party_Key_Id,
    To_Number(Account_Id) Ref_Id,
    GET_CODE_NAME(11301) Ref_Type,
    11301 Ref_Type_Id,
    get_code_name(101021) Address_Type,
    'Y' Primary_Fl
  FROM ACCOUNT_DIM
  ));
  
ALTER TABLE ADDRESS_DIM ADD 
CONSTRAINT ADDRESS_DIM_PK
 PRIMARY KEY  (ADDRESS_KEY_ID)
 ENABLE VALIDATE;