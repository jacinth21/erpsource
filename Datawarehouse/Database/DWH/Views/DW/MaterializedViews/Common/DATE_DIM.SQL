/* Formatted on 2009/11/11 14:05 (Formatter Plus v4.8.0) */
DROP TABLE date_dim;

CREATE TABLE date_dim
(
  date_key_id			NUMBER,
  cal_date			DATE,
  date_MM_DD_YYYY		CHAR(10),
  month_year		VARCHAR2(12),
  year_month		VARCHAR2(12),
  day_number		NUMBER,
  week_number		NUMBER,
  week_mth_number		NUMBER, 
  week_yr_number		NUMBER,
  Quarter_number		NUMBER,
  month_number		NUMBER,
  year_number		NUMBER,
  year_quarter		VARCHAR2(6),
  week_day_number		NUMBER,
  holiday_ind		CHAR(1 BYTE),
  bus_day_ind		CHAR(1 BYTE),
  bus_days_for_month	NUMBER,
  month_name		VARCHAR2(10 BYTE),
  month_short_name	CHAR(3 BYTE),
  week_year			varchar2(8),
  MONTH_NM_YEAR         varchar2(15)
);

ALTER TABLE DATE_DIM ADD 
CONSTRAINT DATE_DIM_PK
 PRIMARY KEY (DATE_KEY_ID)
 ENABLE VALIDATE;

CREATE UNIQUE INDEX DATE_DIM_CAL_DT_IDX ON DATE_DIM
(CAL_DATE)
LOGGING
NOPARALLEL;

CREATE UNIQUE INDEX DATE_DIM_DT_CHR_IDX ON DATE_DIM
(date_MM_DD_YYYY)
LOGGING
NOPARALLEL;