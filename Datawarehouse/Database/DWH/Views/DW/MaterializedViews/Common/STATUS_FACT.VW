DROP MATERIALIZED VIEW STATUS_FACT;

CREATE MATERIALIZED VIEW STATUS_FACT
NOCACHE
LOGGING
NOCOMPRESS
NOPARALLEL
REFRESH FORCE ON DEMAND
AS
(
select
STATUS_DETAILS_KEY_ID,
Ref_Id,
Decode(Status_source_code_ID,91107,Ref_Id) Consignment_loaner_key_id,
Decode(Status_source_code_Id,91102,Ref_Id) Order_key_id,
Decode(Status_source_code_Id,91104,Ref_Id) Inhouse_Trans_Key_Id,
Decode(Status_source_code_Id,91100,Ref_Id) Returns_Key_Id,
Decode(Status_source_code_Id,91101,Ref_Id) CONSIGNMENT_KEY_ID,
Decode(Status_source_code_Id,91106,Ref_Id) Allocation_Ref_ID, 
DATE_DIM.DATE_KEY_ID
from
STATUS_DETAIL_MASTER_DIM, DATE_DIM
WHERE TRUNC(STATUS_DETAIL_MASTER_DIM.updated_date)=DATE_DIM.cal_date(+)
AND Status_source_code_ID in(91107,91102,91104,91100,91101,91106));
