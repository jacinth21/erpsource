
/*
File name: pbi_pending_po_invoice_fact.vw
Created By: RVeerasamy
PMT # :- PMT-35169
Description: Fetch all the orders related information; only orders whose po number not received and order’s whose po’s are received but the invoice’s are not yet to be raised.
	Excluding order type - Ack order for sale, ST order, ICS, Quote, exclude order type from A/R dashboard, Invoice ous distributor
*/

DROP VIEW pbi_pending_po_invoice_fact;
CREATE OR REPLACE VIEW pbi_pending_po_invoice_fact
AS  
SELECT 
  t501.c501_order_id order_id,
  t501.c501_parent_order_id parent_order_id,
  t501.c501_customer_po customer_po, 
  TRUNC(t501.c501_order_date) order_date, 
  TRUNC(t501.c501_customer_po_date) customer_po_date,
  TRUNC(t501.c501_surgery_date) surgery_date,
  DECODE (MIN (t501.c501_status_fl)
                                    , 0, 'Back Order'
                                    , 1, 'Pending Control Number'
                                    , 2, 'Pending Shipment'
                                    , 2.30, 'Packing In Progress'
                                    , 2.60, 'Ready For Pickup'
                                    , 3, 'Shipped'
                                    , 4, 'Shipped'
                                    , 7, 'Pending Release'
                                    , 8, 'Pending CS Confirmation'
                                    ) ordstatus,
  t501.c501_hold_fl holdfl,
  t501.c704_account_id account_id, 
  t501.c703_sales_rep_id sales_rep_id,
  TO_NUMBER (t501.c501_ad_id) ad_id, 
  TO_NUMBER (t501.c501_vp_id) vp_id,
  TO_NUMBER (t501.c1900_company_id) company_id,
  NVL (DECODE (c501_customer_po, NULL, 'Pending PO', DECODE (c503_invoice_id, NULL, 'Pending Invoice')), 'Invoice Generated') category,
  t501.c501_ship_cost ship_cost,
  SUM (t502.c502_item_price * t502.c502_item_qty) sales_amount,
  SUM (t502.c502_corp_item_price * t502.c502_item_qty) corp_sales_amount,
  NVL(SUM(t501.c501_ship_cost),0)+SUM (t502.c502_item_price * t502.c502_item_qty) Total
  FROM t501_order t501, t502_item_order t502
  WHERE t502.c501_order_id = t501.c501_order_id
  AND (t501.c501_customer_po IS NULL OR t501.c503_invoice_id IS NULL)
           ----Some orders don't have PO But invoice id available
  AND t501.c503_invoice_id IS NULL
  AND (t501.c901_ext_country_id is NULL
                    OR t501.c901_ext_country_id  in (select country_id from v901_country_codes))
  AND t501.c501_delete_fl IS NULL
  AND t501.c501_void_fl IS NULL
  AND NVL(T501.C901_order_type,-9999) NOT IN 
   (SELECT  t906.c906_rule_value
   FROM t906_rules t906
   WHERE t906.c906_rule_grp_id IN ('SKIP_PO_INVOICE')
   AND c906_rule_id IN ('ORDTYPE'))
GROUP BY t501.c501_order_id
       , t501.c501_parent_order_id
       , t501.c501_order_date
       , t501.c501_customer_po_date
       , t501.c501_surgery_date
       , t501.c501_status_fl
       , t501.c501_hold_fl
       , t501.c704_account_id
       , t501.c703_sales_rep_id
       , t501.c501_ad_id
       , t501.c501_vp_id
       , t501.c1900_company_id
       , c501_customer_po
       , c503_invoice_id
       , t501.c501_ship_cost;