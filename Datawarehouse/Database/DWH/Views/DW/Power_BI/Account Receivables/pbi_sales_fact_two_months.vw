  /*
File name: pbi_sales_fact_two_months.vw
Created By: RVeerasamy
PMT # :- PMT-37834
Description: Fetch non voided sales data from t501_order, t502_item_order tables and pbi_mv_system_dim for last two month by 
	order type excluding following order types, 
	2519:Pending CS Confirmation, 2518:Draft, 2533:ICS, 101260:Acknowledgement Order,2520:Quote,2534:ICT returns Order,102364:Stock Transfer
*/
DROP VIEW pbi_sales_fact_two_months;
CREATE OR REPLACE VIEW pbi_sales_fact_two_months
AS          
  SELECT account_id,
          order_date,
         company_id,
         SUM(sales_amount) sales_amount, 
         SUM(corp_sales_amount) corp_sales_amount
  FROM pbi_sales_fact_two_yrs
 WHERE  order_date  >= trunc(add_months( sysdate, -12 ),'MONTH')
  GROUP BY account_id,
           order_date,
           company_id;