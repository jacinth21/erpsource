﻿
/*
File name: pbi_pending_po_invoice_fact.vw
Created By: RVeerasamy
PMT # :- PC-1551
Description: Fetch all the data's related to the payment received and the details of the invoices.
*/

DROP VIEW pbi_receivables_fact;
CREATE OR REPLACE VIEW pbi_receivables_fact
AS
  SELECT t503.c704_account_id account_id,
  t508.c503_invoice_id invoice_id,
  t503.c503_invoice_date invoice_date,
  t901.c901_code_nm received_mode,
  t508.c508_received_date payment_received_date,
  SUM(t508.c508_payment_amt) payment_amount
  FROM t508_receivables t508, t503_invoice t503, t901_code_lookup t901
  WHERE t503.c503_invoice_id = t508.c503_invoice_id
  AND t508.c508_received_mode = t901.c901_code_id
  AND t508.c508_received_date >= trunc(add_months( SYSDATE, -12 ),'MONTH')
  GROUP BY t503.c704_account_id,t508.c503_invoice_id,t503.c503_invoice_date,t901.c901_code_nm,t508.c508_received_date;