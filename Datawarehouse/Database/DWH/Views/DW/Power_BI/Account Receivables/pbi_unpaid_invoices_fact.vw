
/*
File name: pbi_unpaid_invoices_fact.vw
Created By: RVeerasamy
PMT # :- PMT-35169
Description: Fetch all the accounts unpaided invoices data from t503_invoice table.
	invoice_source excluding following invoice_source types
  50253 - Inter-Company Sale, 50256 - Inter-Company Transfer, 50257 - Inter-Company Account, 26240213 - Company Dealers
  Filtering only - Invoice Type Stock Transfer & invoice statement type in AR
*/

DROP VIEW pbi_unpaid_invoices_fact;
CREATE OR REPLACE VIEW pbi_unpaid_invoices_fact
AS
DROP VIEW pbi_unpaid_invoices_fact;
CREATE OR REPLACE VIEW pbi_unpaid_invoices_fact
AS
 SELECT t503.c704_account_id account_id, 
        t503.c503_invoice_id invoice_id,
        TRUNC(t503.c503_invoice_date) invoice_date,
        t503.c503_customer_po customer_po,
        t503.c503_inv_amt_wo_tax inv_amt_wo_tax,
        t503.c503_inv_tax_amt inv_tax_amt,
        t503.c503_inv_ship_cost inv_ship_cost,
        t503.c503_inv_amt inv_amnt, 
        t503.c503_inv_pymnt_amt inv_pymnt_amt,
        inv_src.c901_code_nm invoice_source,
        inv_type.c901_code_nm invoice_type,
        t101.c101_user_f_name ||' ' || c101_user_l_name created_by,
        t503.c503_created_date created_date,
        t503.c1900_company_id company_id
 FROM t503_invoice t503, t901_code_lookup inv_src,t901_code_lookup inv_type,t101_user t101 
 WHERE t503.c901_invoice_source = inv_src.c901_code_id
 AND t503.c901_invoice_type = inv_type.c901_code_id
 AND t101.c101_user_id = t503.c503_created_by
 AND t503.c503_void_fl IS NULL
 AND t503.c901_ext_country_id IS NULL
 AND (t503.c704_account_id IS NOT NULL OR t503.c701_distributor_id IS NOT NULL OR t503.c101_dealer_id IS NOT NULL)
 AND t503.c901_invoice_source NOT IN (50256,50257,50253,26240213)
  --- filtering only company customers
 AND t503.c503_status_fl < 2  
 AND NVL (c901_invoice_type, -9999) NOT IN
 (SELECT t906.c906_rule_value
  FROM t906_rules t906
  WHERE t906.c906_rule_grp_id = 'INVOICETYPE'
  AND c906_rule_id             = 'EXCLUDE'
  AND c906_void_fl           IS NULL
   --Filtering - Invoice Type Stock Transfer & invoice statement type in AR
  );