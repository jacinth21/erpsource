/*
File name: pbi_date_dim_two_yrs.vw
Created By: djayaraj
Updated BY: Yoga
Description: Fetch last 3 years bussiness days upto end of this year in materialized view
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\Power_BI\Two_Years_Data\pbi_date_dim_two_yrs.vw";
*/

DROP MATERIALIZED VIEW pbi_date_dim_two_yrs;
CREATE MATERIALIZED VIEW pbi_date_dim_two_yrs 
refresh complete start with (sysdate) next  (sysdate)+23/24 with rowid
AS ( SELECT 
      day,
      cal_date, 
      year_month,
      to_char(cal_date,'yy/mm') yy_mm,
      holiday_ind, 
      bus_day_ind,
      day_number,
      decode(cal_date,trunc(SYSDATE), 1,null) today,
  CASE
    WHEN year_month = to_char(SYSDATE,'yyyy/mm')
    THEN 1
  END Current_Month,
  CASE
    WHEN year_quarter = to_char(sysdate, 'Q') || 'Q'  || to_char(SYSDATE,'yyyy')
    THEN 1
  END Current_Quarter,
  CASE
    WHEN YEAR_NUMBER = to_char(SYSDATE,'yyyy') 
    THEN 1
  END Current_Year,
  CASE
    WHEN CAL_DATE  BETWEEN trunc(sysdate, 'MONTH') and sysdate
    THEN 1
  END MTD,
  CASE
    WHEN cal_date BETWEEN trunc(sysdate,'q') and sysdate
    THEN 1
  END QuarterToDate,
  CASE
    WHEN CAL_DATE BETWEEN trunc(sysdate, 'YEAR') and sysdate
    THEN 1
  END YearToDate,
  CASE
    WHEN cal_date BETWEEN trunc(ADD_MONTHS(SYSDATE,  -12), 'MONTH') and ADD_MONTHS(SYSDATE,  -12)
    THEN 1
  END Prior_Year_MTD,
  CASE
    WHEN cal_date BETWEEN trunc(ADD_MONTHS(SYSDATE,  -12),'q') and ADD_MONTHS(SYSDATE,  -12)
    THEN 1
  END Prior_Year_QTD,
  CASE
    WHEN CAL_DATE BETWEEN trunc(ADD_MONTHS(SYSDATE,  -12), 'YEAR') and ADD_MONTHS(SYSDATE,  -12)
    THEN 1
  END Prior_Year_YTD,
  -- For Backward compatability
  0 Last_Business_Day,
   CASE
    WHEN (cal_date > ADD_MONTHS((LAST_DAY(SYSDATE)),-2)
    AND cal_date  <= ADD_MONTHS((LAST_DAY(SYSDATE)),-1))
    THEN 1
  END Last_Month,
  CASE
    WHEN (cal_date >= ADD_MONTHS((LAST_DAY(SYSDATE)),-2)
    AND cal_date   <= ADD_MONTHS((SYSDATE),          -1))
    THEN 1
  END Last_MTD,
   CASE
    WHEN (cal_date >= TRUNC(TO_DATE(SYSDATE),'YEAR')
    AND cal_date   <= ADD_MONTHS((SYSDATE),0))
    THEN 1
  END Year_To_Date,
  DECODE(YEAR_NUMBER, TO_CHAR(ADD_MONTHS((SYSDATE),-12),'YYYY'), 1,NULL) Last_Year,
  CASE
    WHEN (cal_date >= Add_months(TRUNC(TO_DATE(SYSDATE),'Year'),-12)
    AND cal_date   <= ADD_MONTHS((SYSDATE),                     -12))
    THEN 1
  END Last_YTD,
  DECODE(YEAR_NUMBER, TO_CHAR(ADD_MONTHS((SYSDATE),-(12*2)),'YYYY'), 1,NULL) Last_2_Year,
  CASE
    WHEN (cal_date >= Add_months(TRUNC(TO_DATE(SYSDATE),'Year'),-(12*2))
    AND cal_date   <= ADD_MONTHS((SYSDATE),                     -(12*2)))
    THEN 1
  END Last_2_YTD,
  DECODE(YEAR_NUMBER, TO_CHAR(ADD_MONTHS((SYSDATE),-(12*3)),'YYYY'), 1,NULL) Last_3_Year,
  CASE
    WHEN (cal_date >= Add_months(TRUNC(TO_DATE(SYSDATE),'Year'),-(12*3))
    AND cal_date   <= ADD_MONTHS((SYSDATE),                     -(12*3)))
    THEN 1
  END Last_3_YTD,
  DECODE(YEAR_NUMBER, TO_CHAR(ADD_MONTHS((SYSDATE),-(12*4)),'YYYY'), 1,NULL) Last_4_Year,
  CASE
    WHEN (cal_date >= Add_months(TRUNC(TO_DATE(SYSDATE),'Year'),-(12*4))
    AND cal_date   <= ADD_MONTHS((SYSDATE),                     -(12*4)))
    THEN 1
  END Last_4_YTD,
  DECODE(YEAR_NUMBER, TO_CHAR(ADD_MONTHS((SYSDATE),-(12*5)),'YYYY'), 1,NULL) Last_5_Year,
  CASE
    WHEN (cal_date >= Add_months(TRUNC(TO_DATE(SYSDATE),'Year'),-(12*5))
    AND cal_date   <= ADD_MONTHS((SYSDATE),                     -(12*5)))
    THEN 1
  END Last_5_YTD
    FROM date_dim 
	WHERE year_number >= TO_CHAR(TRUNC(add_months( sysdate, -12*3 ),'YEAR'),'yyyy')
    AND year_number   <= TO_CHAR(TRUNC(sysdate,'YEAR'),'yyyy')
  );
  
CREATE INDEX idx_pbi_date_id ON PBI_DATE_DIM_TWO_YRS (cal_date);
CREATE INDEX idx_pbi_today ON PBI_DATE_DIM_TWO_YRS (today);
CREATE INDEX idx_pbi_current_month ON PBI_DATE_DIM_TWO_YRS (Current_Month);
CREATE INDEX idx_pbi_current_qtr ON PBI_DATE_DIM_TWO_YRS (Current_Quarter);
CREATE INDEX idx_pbi_current_year ON PBI_DATE_DIM_TWO_YRS (Current_Year);
/