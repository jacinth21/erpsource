DROP MATERIALIZED VIEW pbi_gpo_dim;
CREATE MATERIALIZED VIEW pbi_gpo_dim REFRESH COMPLETE START WITH
  (
    SYSDATE
  )
  NEXT (SYSDATE+60/1440)
WITH ROWID AS
   ( SELECT  t101.c101_party_id gpo_id, t101.c101_party_nm gpo_name,t101.c1900_company_id gpo_company_id, c101_active_fl active_fl  FROM 
t101_party t101
WHERE c901_party_type=7011
AND t101.c101_void_fl IS NULL
); 

CREATE INDEX idx_pbi_gpo_party_id ON pbi_gpo_dim (gpo_id);
CREATE INDEX idx_pbi_gpo_company_id ON pbi_gpo_dim (gpo_company_id);

CREATE OR REPLACE PUBLIC SYNONYM pbi_gpo_dim FOR globus_app.pbi_gpo_dim ;
