/*
File name: pbi_rpc_dim.vw
Created By: RVeerasamy
PC # :- PC-2839
Description: Fetch the RPC informations from party table
Location: C:\PMT_Bit\Datawarehouse\Database\DWH\Views\DW\Power_BI\Two_Years_Data\pbi_rpc_dim.vw
*/

DROP MATERIALIZED VIEW pbi_rpc_dim;
CREATE MATERIALIZED VIEW pbi_rpc_dim 
NOLOGGING
REFRESH COMPLETE START WITH (SYSDATE) NEXT  (SYSDATE+60/1440) WITH ROWID
AS  
( SELECT
    t101.c101_party_id      rpc_id,
    t101.c101_party_nm      rpc_name,
    t101.c1900_company_id   rpc_company_id,
    c101_active_fl          active_fl
FROM
    t101_party t101
WHERE
    c901_party_type = 26241117 --RPC
    AND t101.c101_void_fl IS NULL
);

CREATE INDEX idx_pbi_rpc_party_id ON pbi_rpc_dim (rpc_id);
CREATE INDEX idx_pbi_rpc_company_id ON pbi_rpc_dim (rpc_company_id);

CREATE OR REPLACE PUBLIC SYNONYM pbi_rpc_dim FOR globus_app.pbi_rpc_dim;
/