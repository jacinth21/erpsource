/*
File name: pbi_inv_consignment_tag_history.vw
Created By: Yoga
Description: Fetch Consignment Tags expected usage data for given period for Score Card
Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\2 Years Data\Inventory\pbi_inv_consignment_tag_history.vw";
*/

DROP MATERIALIZED VIEW pbi_inv_consignment_tag_history;
CREATE MATERIALIZED VIEW pbi_inv_consignment_tag_history REFRESH COMPLETE START WITH
  (
    SYSDATE
  )
  NEXT (SYSDATE+60/1440)
WITH ROWID AS
SELECT baseline_sets.critical_system_id,
  baseline_sets.critical_system_name,
  baseline_sets.critical_system_type_name,
  t701.c701_distributor_name distributor_name,
  t504g_adj.tag_id,
  baseline_sets.set_id,
  baseline_sets.set_name,
  t504g_adj.placement_date,
  t504g_adj.return_date,
  t504g_adj.adjusted_placement_date,
  t504g_adj.adjusted_return_date,
  round((t504g_adj.adjusted_return_date-t504g_adj.adjusted_placement_date),0) days_kept,
  t504h.c504h_month_turn,
  t504h.c504h_turn_rate turn_rate
FROM
  (SELECT t504g.c701_distributor_id distributor_id,
    t504g.c5010_tag_id tag_id,
    t504g.c207_set_id set_id,
    t504g.c504g_placement_date placement_date,
    CASE
      WHEN trunc(t504g.c504g_placement_date) >= trunc(add_months(SYSDATE, -3),'MONTH')
      THEN trunc(t504g.c504g_placement_date)
      WHEN trunc(t504g.c504g_placement_date) < trunc(add_months(SYSDATE, -3),'MONTH')
      THEN trunc(add_months(SYSDATE,                                     -3),'MONTH')
    END AS adjusted_placement_date,
    t504g.c504g_return_date return_date,
    nvl(t504g.c504g_return_date, last_day(add_months(SYSDATE, -1))) adjusted_return_date,
    t504g.c504g_txn_id txn_id,
    t504g.c504g_active_fl active_fl,
    t504g.c504g_missing_since
  FROM t504g_consignment_tag_history t504g
  ) t504g_adj,
  t701_distributor t701,
  (SELECT t207_system.c207_set_id critical_system_id,
    t207_system.c207_set_nm critical_system_name,
    t207_system.c901_critical_system_type critical_system_type_id,
    get_code_name(t207_system.c901_critical_system_type) critical_system_type_name,
    t207_set.c207_set_id set_id,
    t207_set.c207_set_nm set_name
  FROM t207_set_master t207_system,
    t207_set_master t207_set
  WHERE t207_set.c207_set_sales_system_id = t207_system.c207_set_id
  AND t207_system.c207_critical_system_fl ='Y'
  AND t207_set.c901_cons_rpt_id           = 20100
  AND ( t207_set.c207_set_id NOT LIKE unistr('%LN')
  AND t207_set.c207_set_id NOT LIKE unistr('%EURLN')
  AND t207_set.c207_set_id NOT LIKE unistr('%EUR')
  AND t207_set.c207_set_id NOT LIKE unistr('%JPNLN')
  AND t207_set.c207_set_id NOT LIKE unistr('%JPN')
  AND t207_set.c207_set_id NOT LIKE unistr('%ED')
  AND t207_set.c207_set_id NOT LIKE unistr('%DE') )
  ) baseline_sets,
  t504h_consignment_turn_rate t504h
WHERE t504g_adj.set_id                = baseline_sets.set_id
  AND t504g_adj.distributor_id          = t701.c701_distributor_id
  AND t504h.c901_system_type            = baseline_sets.critical_system_type_id
  AND t504h.c504h_month_turn            = trunc((t504g_adj.adjusted_return_date - t504g_adj.adjusted_placement_date)/30)
  AND (t504g_adj.adjusted_return_date  >=trunc(add_months(SYSDATE,              -3),'MONTH')
    OR t504g_adj.adjusted_return_date    IS NULL)                             
  AND t504g_adj.adjusted_placement_date < last_day(add_months(SYSDATE, -1)) ;
/
