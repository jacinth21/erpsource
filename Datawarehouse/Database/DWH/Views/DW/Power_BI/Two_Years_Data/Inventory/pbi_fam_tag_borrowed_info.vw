/*
File name: pbi_inv_consignment_tag_history.vw
Created By: Richard
Description: Fetch Tag Borrowed Details  for Score Card
Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\2 Years Data\Inventory\pbi_fam_tag_borrowed_info.vw";
*/

DROP MATERIALIZED VIEW pbi_fam_tag_borrowed_info;
CREATE MATERIALIZED VIEW pbi_fam_tag_borrowed_info REFRESH COMPLETE START WITH
  (
    SYSDATE
  )
  NEXT (SYSDATE+60/1440)
WITH ROWID AS
SELECT last2mon.start_date ,
      baseline_sets.critical_system_id ,
      baseline_sets.critical_system_type_id critical_system_type_id ,
      t501d.c5010_tag_id tag_id,
      t501d.c207_set_id set_id,
      t501d.c501_order_id order_id,
      t501.c501_order_date order_date,
      t501d.c501d_order_owner_id order_owner_id,
      t701_order_owner.c701_distributor_name order_owner,
      t501d.c501d_tag_owner_id tag_owner_id,
      t701_tag_owner.c701_distributor_name tag_owner,
      t501d.c501d_credit_owner_id procedure_credit_owner,     
	  t501d.c501d_credit_owner_id distributor_id,
	  baseline_sets.critical_system_name,
      baseline_sets.critical_system_type_name,
      baseline_sets.set_name
    FROM t501d_order_tag_usage_details t501d,
      t501_order t501 ,
      pbi_fam_baseline_set_dim baseline_sets,
      t207_set_master t207,
      t701_distributor t701_order_owner,
      t701_distributor t701_tag_owner,
      (SELECT trunc(add_months(SYSDATE, -2),'MONTH') start_date FROM dual
      ) last2mon
    WHERE t501d.c501_order_id      = t501.c501_order_id
    AND t501d.c207_set_id          = baseline_sets.set_id
    AND t501d.c207_set_id          = t207.c207_set_id
    AND t501d.c501d_order_owner_id = t701_order_owner.c701_distributor_id(+)
    AND t501d.c501d_tag_owner_id   = t701_tag_owner.c701_distributor_id(+)
    AND t207.c207_void_fl         IS NULL
    AND t501.c501_parent_order_id IS NULL -- Remove Child Orders
    AND t501d.c501d_order_owner_id <> t701_tag_owner.c701_distributor_id
    AND t501.c501_order_date      >=
      (SELECT trunc(add_months(SYSDATE, -3),'MONTH') FROM dual
      ) ;