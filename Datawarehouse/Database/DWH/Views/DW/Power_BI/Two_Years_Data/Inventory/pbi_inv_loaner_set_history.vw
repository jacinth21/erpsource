/*
File name: pbi_inv_loaner_set_history.vw
Created By: Yoga
Description: Fetch Consignment expected usage data for given period for Score Card
Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\2 Years Data\Inventory\pbi_inv_loaner_set_history.vw";
Code Ids:
60 -- Inactive
20100 -- Baseline sets
70106/70103/70105 -- ICT/ICA/ICS
*/

DROP MATERIALIZED VIEW pbi_inv_Loaner_set_history;
CREATE MATERIALIZED VIEW pbi_inv_Loaner_set_history REFRESH COMPLETE START WITH
  (
    SYSDATE
  )
  NEXT (SYSDATE+240/1440)
WITH ROWID AS
SELECT SC_Loaners.*,
  SC_Loaners.Adjusted_Return_Date-SC_Loaners.Adjusted_Placement_Date No_of_Occurances,
  decode(SC_Loaners.status_fl, 22,0,
  21,decode(Actual_Return_Date, null, 0,ceil((SC_Loaners.Adjusted_Return_Date-SC_Loaners.Adjusted_Placement_Date)/7)), 
  ceil((SC_Loaners.Adjusted_Return_Date-SC_Loaners.Adjusted_Placement_Date)/7)) exp_usage
FROM
  (SELECT t207.C207_SET_SALES_SYSTEM_ID Critical_System_Id,
    critical_systems.CRITICAL_SYSTEM_Name ,
    critical_systems.CRITICAL_SYSTEM_TYPE,
    t701.c701_distributor_name Distributor_name,
    T504.C207_Set_Id Set_Id,
    t207.C207_SET_NM Set_Name,
    TRUNC(T504a.C504a_Loaner_Dt) Actual_Placement_date,
    CASE
      WHEN TRUNC(T504a.C504a_Loaner_Dt) >= TRUNC(ADD_MONTHS(SYSDATE, -3),'MONTH')
      THEN TRUNC(T504a.C504a_Loaner_Dt)
      WHEN TRUNC(T504a.C504a_Loaner_Dt) <= TRUNC(ADD_MONTHS(SYSDATE, -3),'MONTH')
      THEN TRUNC(ADD_MONTHS(SYSDATE, -3),'MONTH')
    END AS Adjusted_Placement_Date,
    T504a.C504a_Return_Dt Actual_Return_Date,
    NVL(T504a.C504a_Return_Dt, last_day(ADD_MONTHS(SYSDATE, -1))) Adjusted_Return_Date,
    T504a.c504_consignment_id,
    T504a.C504A_LOANER_TRANSACTION_ID Loaner_Id,
    t504acl.c504a_etch_id,
    t504acl.c504a_status_fl status_fl,
    DECODE (t504acl.c504a_status_fl, 0, 'Available', 5, 'Allocated', 7, 'Pending Pick', 10, 'Pending Shipping', 13, 'Packing In Progress', 16, 'Ready For Pickup', 
      20, 'Pending Return',21, 'Disputed', 22, 'Missing', 23, 'Deployed', 25, 'Pending Check', 30, 'WIP - Loaners', 40, 'Pending Verification', 50, 'Pending Process', 
      55, 'Pending Picture', 58, 'Pending Putaway', 60, 'Inactive', 24, 'Pending Acceptance') Status
  FROM T504a_Loaner_Transaction T504a,
    T504_Consignment t504,
    T207_SET_MASTER t207,
    T504A_CONSIGNMENT_LOANER t504acl,
    t703_sales_rep t703,
    t701_distributor t701,
    (SELECT T207.c207_set_id Set_id,
      T207.c207_set_nm CRITICAL_SYSTEM_Name,
      sys_type.C901_CODE_NM CRITICAL_SYSTEM_TYPE
    FROM t207_set_master T207,
      t901_code_lookup sys_type
    WHERE t207.C901_CRITICAL_SYSTEM_TYPE = sys_type.C901_CODE_ID (+)
    AND t207.C207_CRITICAL_SYSTEM_FL     ='Y'
    AND c207_void_fl                    IS NULL
    ) critical_systems
  WHERE T504a.c504_consignment_id   = t504.c504_consignment_id
    AND t207.c207_set_id              = T504.C207_Set_Id
    AND T207.C207_SET_SALES_SYSTEM_ID = critical_systems.Set_id
    AND t504acl.c504_consignment_id   = t504.c504_consignment_id -- For Etch Id
    AND T504a.c703_sales_rep_id       = t703.c703_sales_rep_id
    AND t703.c701_distributor_id      = t701.c701_distributor_id
    AND (T504a.C504a_Return_Dt       >= TRUNC(ADD_MONTHS(SYSDATE, -3),'MONTH')
      OR T504a.C504a_Return_Dt         IS NULL)
      --and ( T504a.C504a_Loaner_Dt>='01-MAR-2019' and T504a.C504a_Loaner_Dt<='31-MAY-2019'
    AND T504a.C504a_Void_Fl     IS NULL
    AND T504acl.C504a_Void_Fl   IS NULL
    AND T207.C207_Void_Fl       IS NULL
    AND t207.C901_CONS_RPT_ID    = 20100
    AND t504.c1900_company_id    =1000
    AND c504_type                = 4127
    AND T504acl.C504A_STATUS_FL !=60
  
  ORDER BY t701.c701_distributor_id
  ) SC_Loaners 
  
  WHERE (SC_Loaners.Adjusted_Return_Date >=TRUNC(ADD_MONTHS(SYSDATE, -3),'MONTH')
      OR SC_Loaners.Adjusted_Return_Date   IS NULL)
    AND SC_Loaners.Adjusted_Placement_Date < last_day(ADD_MONTHS(SYSDATE, -1));
/