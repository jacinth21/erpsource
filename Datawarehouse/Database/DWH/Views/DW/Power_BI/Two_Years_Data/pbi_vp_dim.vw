/*
File name: pbi_vp_dim.vw
Created By: djayaraj
Description: Fetch Vice President Details from t101_user and v700_terrirory_mapping_detail
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\2 Years Data\pbi_vp_dim.vw";
*/
CREATE OR REPLACE VIEW pbi_vp_dim
AS
  (SELECT DISTINCT t101.c101_user_id vp_id,
    c101_user_sh_name vp_short_name,
    c101_user_f_name
    ||' '
    ||c101_user_l_name vp_name,
    c101_email_id email_id,
    SUBSTR(c101_email_id,1,INSTR(c101_email_id,'@',1,1)-1) user_name,
    c102_login_username login_user_name,
    c101_dept_id dept_id,
    c101_access_level_id access_level_id,
    c101_hire_dt hire_dt,
    c101_termination_date termination_date,
    c901_user_status user_status
   FROM t101_user t101,
    (SELECT DISTINCT vp_id FROM v700_territory_mapping_detail
    ) v700,
    t102_user_login t102
  WHERE t101.c101_user_id=vp_id
  AND t101.c101_user_id  =t102.c101_user_id(+)
  );