/*
File name: pbi_inr_fleet_info.vw
Created By: Naveen
Description: To show the number of installs and the other related details
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\2 Years Data\pbi_inr_fleet_info.vw";
*/
  CREATE OR REPLACE VIEW pbi_inr_fleet_info
  AS
  select t5050.c5050_fleet_info_id,
    t5050.c5050_egps_serial_no,
    t5050.c5050_install_date,
    t5050.c704_account_id,
    t704.C704_ACCOUNT_NM,
    t5050.c901_account_type,
    get_code_name(t5050.c901_account_type)Account_Type_Name,
    t5050.c5050_sys_sales_price,
    t5050.c5050_sys_stock_ord_price,
    t5050.c5050_total_revenue,
    t5050.c5050_warranty,
    t5050.c5050_service_contract,
    t5050.c5050_service_price,
    t5050.c5050_service_contract_renewal_date,
    t5050.c5050_first_case_date,
    (select count(*)
    from t5052_fleet_capital_purchase cap
    where cap.c5050_fleet_info_id=t5050.c5050_fleet_info_id and cap.c5052_void_fl is null
    ) as num_cap,
    (select count(*)
    from t5053_fleet_instrument_sets ins
    where ins.c5050_fleet_info_id=t5050.c5050_fleet_info_id and ins.c5053_void_fl is null
    ) as num_inst,
    (select count(*)
    from t5055_fleet_trained_surgeons surg,t6645_npi_account npi
    where npi.c101_party_surgeon_id=surg.c101_party_id and npi.c704_account_id=t5050.c704_account_id and surg.c5055_void_fl is null and npi.C6645_VOID_FL is null
    ) as num_trsurg,
    t5050.c5050_hospital_imaging_systems,
    t5050.c901_status,
    decode(t5050.c901_status,'110270','new site','110271','stopped cases','110272','first priority','110273','no current sugg.','110274','needs work','110275','just started, going well','110276','running smooth') Status_value,
    t5050.c901_case_coverage,
    decode(t5050.c901_case_coverage,'110280','complete independence','110281','new/complex cases ','110282','new surgeons','110283','certain surg. full cover','110284','full coverage','110285','has not started cases') Coverage_value
  from t5050_fleet_info t5050,
    t704_account t704
  where t5050.c704_account_id = t704.c704_account_id 
  and t704.c704_void_fl is null
  and t5050.c5050_void_fl is null;