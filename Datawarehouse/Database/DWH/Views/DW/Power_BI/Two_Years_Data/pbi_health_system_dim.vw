/*
File name: pbi_health_system_dim.vw
Created By: RVeerasamy
PC # :- PC-2839
Description: Fetch the Health System informations from party table
Location: C:\PMT_Bit\Datawarehouse\Database\DWH\Views\DW\Power_BI\Two_Years_Data\pbi_health_system_dim.vw
*/

DROP MATERIALIZED VIEW pbi_health_system_dim;
CREATE MATERIALIZED VIEW pbi_health_system_dim
NOLOGGING
REFRESH COMPLETE START WITH (SYSDATE) NEXT  (SYSDATE+60/1440) WITH ROWID
AS      
( SELECT
    t101.c101_party_id      health_system_id,
    t101.c101_party_nm      health_system_name,
    t101.c1900_company_id   hs_company_id,
    c101_active_fl          active_fl
FROM
    t101_party t101
WHERE
    c901_party_type = 26241118 --Health System
    AND t101.c101_void_fl IS NULL
);

CREATE INDEX idx_pbi_health_system_party_id ON pbi_health_system_dim (health_system_id);
CREATE INDEX idx_pbi_health_system_company_id ON pbi_health_system_dim (hs_company_id);

CREATE OR REPLACE PUBLIC SYNONYM pbi_health_system_dim FOR globus_app.pbi_health_system_dim;
/
