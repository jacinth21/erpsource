DROP MATERIALIZED VIEW pbi_idn_dim;
CREATE MATERIALIZED VIEW pbi_idn_dim REFRESH COMPLETE START WITH
  (
    SYSDATE
  )
  NEXT (SYSDATE+60/1440)
WITH ROWID AS
   ( SELECT  t101.c101_party_id idn_id, t101.c101_party_nm idn_name,t101.c1900_company_id idn_company_id, c101_active_fl active_fl  FROM 
t101_party t101
WHERE c901_party_type=7010
AND t101.c101_void_fl IS NULL
); 

CREATE INDEX idx_pbi_idn_party_id ON pbi_idn_dim (idn_id);
CREATE INDEX idx_pbi_idn_company_id ON pbi_idn_dim (idn_company_id);

CREATE OR REPLACE PUBLIC SYNONYM pbi_idn_dim FOR globus_app.pbi_idn_dim ;
