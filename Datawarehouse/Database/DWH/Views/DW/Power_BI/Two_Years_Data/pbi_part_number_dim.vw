/*
File name: pbi_part_number_dim.vw
Created By: Anuradha
Description: Fetch Part Id, Part description, Product family, Project Id and Project Name
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\2 Years Data\pbi_part_number_dim.vw";
*/


CREATE OR REPLACE VIEW pbi_part_number_dim AS
SELECT t205.c205_part_number_id part_number_id,
           t205.c205_part_num_desc part_num_desc,
           t205.c202_project_id project_id,
           t202.c202_project_nm project_nm,
           t205.c205_active_fl part_active_fl,
           t205.c205_product_family product_family_id,
           t901.c901_code_nm product_family
     FROM t205_part_number t205,t202_project t202,t901_code_lookup t901
    WHERE t205.c202_project_id = t202.c202_project_id
    AND t205.c205_product_family  = t901.c901_code_id(+)
    AND t205.c205_product_family NOT IN (26240315,4056,4000389,4054,4055,4057,26240467,4053)
    AND t205.c205_active_fl ! = 'N';