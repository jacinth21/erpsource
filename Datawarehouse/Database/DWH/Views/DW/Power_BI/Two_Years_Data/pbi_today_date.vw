/*
File name: pbi_today_date.vw
Created By: djayaraj
Description: Fetch today date
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\2 Years Data\pbi_today_date.vw";
*/


CREATE OR REPLACE VIEW pbi_today_date AS
  (select sysdate today from dual);