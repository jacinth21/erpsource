DROP MATERIALIZED VIEW pbi_surgeon_dim;
CREATE MATERIALIZED VIEW pbi_surgeon_dim REFRESH COMPLETE START WITH
  (
    SYSDATE
  )
  NEXT (SYSDATE+60/1440)
WITH ROWID AS
  (SELECT t101.c101_party_id surgeon_party_id,
    t6600.c6600_surgeon_npi_id npi,
    t101.c101_first_nm first_name,
    t101.c101_last_nm last_name,
     t101.c101_first_nm
    || decode(t101.c101_middle_initial,NULL,' ',' '
    || t101.c101_middle_initial
    || ' ' )
    || t101.c101_last_nm full_name
  FROM t6600_party_surgeon t6600,
    t101_party t101
  WHERE t101.c101_party_id = t6600.c101_party_surgeon_id(+)
  AND t101.c901_party_type = 7000
  AND t6600.c6600_void_fl IS NULL
  AND t101.c101_void_fl   IS NULL
  ) ;
CREATE INDEX idx_pbi_surgeon_party_id ON pbi_surgeon_dim (surgeon_party_id);
CREATE INDEX idx_pbi_surgeon_npi ON pbi_surgeon_dim (npi);

 

CREATE OR REPLACE PUBLIC SYNONYM pbi_surgeon_dim FOR globus_app.pbi_surgeon_dim ;