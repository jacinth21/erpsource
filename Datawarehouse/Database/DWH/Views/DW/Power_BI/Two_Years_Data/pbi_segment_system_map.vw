/*
File name: pbi_segment_system_map.vw
Created By: djayaraj
Description: Fetch mapping details between segment and system
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\2 Years Data\pbi_segment_system_map.vw";
*/

CREATE OR REPLACE VIEW pbi_segment_system_map
AS
   (SELECT t2090.c2090_segment_id Segment_id,
           t2090.c2090_segment_name segment_name,
           t207.c207_set_id system_id,
		   t207.c207_set_nm system_nm
      FROM t2090_segment t2090,
           T2091_segment_system_map t2091,
           t207_set_master t207
     WHERE     t2090.c2090_segment_id = t2091.c2090_segment_id
           AND t2091.c207_set_id = t207.c207_set_id
           AND t207.c901_set_grp_type = 1600
           AND t207.c207_void_fl IS NULL
           AND t2090.c2090_void_fl IS NULL
           AND t2091.c2091_void_fl IS NULL);