//*
File name: pbi_sales_fact_two_yrs.vw
Created By: djayaraj, Anuradha
Description: Fetch non voided sales data from t501_order, t502_item_order tables and pbi_mv_system_dim for last two years by 
	order type excluding following order types, 
	2519:Pending CS Confirmation, 2518:Draft, 2533:ICS, 101260:Acknowledgement Order,2520:Quote,2534:ICT returns Order,102364:Stock Transfer
                        exclude following order types-2524--Price Adjustment,2535---Sales Discount)
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\2 Years Data\pbi_sales_fact_two_yrs.vw";
*//


CREATE OR REPLACE VIEW pbi_sales_fact_two_yrs AS
(SELECT t501.c501_order_id order_id,
       t501.c501_parent_order_id parent_order_id,
        DECODE(t501.c501_receive_mode,0,'IPAD',UPPER(t901_ordermode.c901_code_nm)) received_mode, --Received Mode 0 as IPAD        Order
        DECODE(t501.c501_receive_mode,26230683,'IPAD',0,'IPAD','NON-IPAD') ipad_order,
        NVL(t901_ordertype.c901_code_nm,'Bill and Ship') Order_type,
        t501.c501_order_date order_date,
        t501.c501_surgery_date surgery_date,
         TO_CHAR(t501.c501_created_date, 'HH AM') order_hour,
         t501.c704_account_id account_id ,t501.c501_created_date order_date_time,
         t501.c703_sales_rep_id sales_rep_id ,
         to_number(v700.ad_id) ad_id,
         to_number(v700.vp_id) vp_id,
         system_dim.system_id system_id,
         system_dim.system_name system_name,
         t502.c205_part_number_id part_id ,
         to_number(t501.c1900_company_id) company_id,
         to_number(v700.divid) market_id,
         to_number(v700.compid) division_id,
         to_number(v700.gp_id) zone_id,
         to_number(v700.region_id) region_id,
        to_number(v700.d_id) distributor_id,
        to_number(v700.ter_id) territory_id,
         to_number(decode(t501.c901_order_type,2524,0,2535,0,t502.c502_item_qty))item_qty ,---(2524--Price Adjustment,2535-Sales Discount)
         t502.c502_item_price item_price,
         t502.c502_corp_item_price corp_item_price,
         t502.c502_item_price * t502.c502_item_qty sales_amount ,
         t502.c502_corp_item_price * t502.c502_item_qty corp_sales_amount              
     FROM t501_order t501 ,
         t502_item_order t502,
         pbi_mv_system_dim system_dim,
         v700_territory_mapping_detail v700,
        (select c901_code_id,c901_code_nm from t901_code_lookup where c901_code_grp IN ('ORDMO','ORDMOD') AND c901_void_fl IS      NULL) t901_ordermode, 
        (select c901_code_id,c901_code_nm from t901_code_lookup where c901_code_grp='ODTYP' AND c901_void_fl IS NULL AND      c901_active_fl='1') t901_ordertype
    WHERE t502.c501_order_id = t501.c501_order_id
        AND t502.c205_part_number_id = system_dim.Part_Id (+)
       AND v700.ac_id = t501.c704_account_id
       AND t501.c501_receive_mode= t901_ordermode.c901_code_id(+)
       AND t501.C901_Order_Type =t901_ordertype.c901_code_id(+)
       AND t501.c704_account_id != 14040 -- Exclude Globus Medical Inc. BBA
       AND NVL (t501.c901_order_type, -9999)     NOT IN (select C906_RULE_VALUE from GLOBUS_APP.v901_order_type_grp) 
       AND t501.c501_order_date                  >= trunc(add_months( sysdate, -12*3 ),'YEAR')
       AND t502.c502_void_fl IS NULL
       AND t501.c501_void_fl IS NULL
       AND t501.c501_delete_fl IS NULL
         ) ;