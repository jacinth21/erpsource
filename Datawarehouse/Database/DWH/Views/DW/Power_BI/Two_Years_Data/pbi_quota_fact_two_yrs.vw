/*
File name: pbi_quota_fact_two_yrs.vw
Created By: djayaraj
Description: Fetch last two years monthly quota amount for each sales rep
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\2 Years Data\pbi_quota_fact_two_yrs.vw";
*/

CREATE OR REPLACE VIEW pbi_quota_fact_two_yrs AS
   (SELECT 
      to_number(t709.c703_sales_rep_id) sales_rep_id,
      to_number(t710.c901_division_id) market_id,
      to_number(t710.c901_company_id) division_id,
      to_number(t710.c901_zone_id) zone_id,
      to_number(t710.c901_area_id) region_id,
      to_number(t701.c701_distributor_id) distributor_id,
      t709.c709_start_date start_date,
      t709.c709_end_date end_date,
      (nvl(t709.c709_quota_breakup_amt_base, 0)) base_amount ,
      (nvl(t709.c709_quota_breakup_amt, 0)) amount 
        FROM t709_quota_breakup t709, 
          t710_sales_hierarchy t710, 
          t701_distributor t701, 
          t703_sales_rep t703
    WHERE t709.c703_sales_rep_id = t703.c703_sales_rep_id
      AND t703.c701_distributor_id = t701.c701_distributor_id
      AND t710.c901_area_id = t701.c701_region
      AND t709.c901_type       = 20750
      AND c709_start_date >=  add_months(Trunc(Sysdate,'YEAR'), -12*3 )
      AND t710.c710_active_fl = 'Y'
          AND t701.c701_void_fl       IS NULL
      And T703.C703_Void_Fl       Is Null
   );