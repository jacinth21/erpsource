/*
File name: pbi_surgeon_sales_dim.vw
Created By: Yoga, Anuradha
Description: To show the order id by the surgeon's npi id and party id
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\2 Years Data\pbi_surgeon_sales_dim.vw";
*/
DROP MATERIALIZED VIEW pbi_surgeon_sales_dim;
CREATE MATERIALIZED VIEW pbi_surgeon_sales_dim refresh complete START WITH
  (
    SYSDATE
  )
  NEXT (SYSDATE+60/1440)
WITH ROWID AS
(SELECT  DISTINCT t6640.c6640_ref_id order_id,
    t6640.c6600_surgeon_npi_id npi,
     t6640.c6640_valid_fl valid_fl,
    t6640.c101_party_surgeon_id Surgeon_Party_Id
  FROM t6640_npi_transaction t6640,
          (select DISTINCT(order_id) from globus_app.pbi_sales_fact_two_yrs)sales
  WHERE t6640.c6640_ref_id                    = sales.order_id
  AND t6640.c6640_transaction_fl ='Y'
  AND t6640.c6640_void_fl       IS NULL
   );
    
CREATE INDEX idx_pbi_surgeon_sales_party_id ON pbi_surgeon_sales_dim (Surgeon_Party_Id);
CREATE INDEX idx_pbi_surgeon_sales_NPI ON pbi_surgeon_sales_dim (NPI);
 
CREATE OR REPLACE PUBLIC SYNONYM pbi_surgeon_sales_dim FOR GLOBUS_APP.pbi_surgeon_sales_dim ;


 

