/**********************************************************************
*Purpose: Materialized View to get LN SC month split data
Author: Yoga
PC: 2781
**********************************************************************/
DROP MATERIALIZED VIEW pbi_fam_ln_mon_split_fact ;
CREATE MATERIALIZED VIEW pbi_fam_ln_mon_split_fact REFRESH COMPLETE START WITH
(
SYSDATE
)
NEXT (SYSDATE+480/1440)
WITH ROWID AS
SELECT t504i.c504a_loaner_transaction_id loaner_id,
  t504i.c701_distributor_id distributor_id,
  t504i.c207_set_id set_id,
  c504i_days_kept days_kept,
  c504i_placement_date placement_date,
  c504i_return_date return_date,
  t504acl.c504a_etch_id etch_id,
  t504acl.c504a_status_fl status_fl,
  decode (t504acl.c504a_status_fl, 0, '', 5, '', 7, '', 10, '', 13, '', 16, '', 20, '',21, 'Disputed', 22, 'Missing', 23, '', 25, '', 30, '', 40, '', 50, '', 55, '', 58, '', 60, '', 24, '') status ,
  t504.c504_consignment_id consignment_id,
  to_char( c504i_month_start, 'MM' ) split_month,
  to_char( c504i_month_start, 'YYYY/MM' ) year_month,
  CASE
    WHEN t504acl.c504a_status_fl =21 THEN 0 -- Disputed
    WHEN t504acl.c504a_status_fl =22 THEN 0 -- Missing
    ELSE
      decode(to_char(c504i_month_start,'MM'), to_char(SYSDATE,'MM'),0,t504i.c504i_exp_usage)
  END AS cmpl_exp_usage,
  CASE
    WHEN t504acl.c504a_status_fl =21 THEN 0 -- Disputed
    WHEN t504acl.c504a_status_fl =22 THEN 0 -- Missing
    ELSE
      decode(to_char(c504i_month_start,'MM'), to_char(SYSDATE,'MM'),t504i.c504i_exp_usage,0) 
  END AS proj_exp_usage,
  t504i.c504i_exp_usage exp_usage,
  t207.c207_set_sales_system_id critical_system_id,
  critical_systems.critical_system_type_id,
    request.rep_id rep_id,
  request.rep_name rep_name,
  request.ass_rep_id ass_rep_id,
  request.ass_rep_name ass_rep_name ,
  DECODE(floor((trunc(sysdate) - trunc(sysdate,'MONTH'))/7) , 0 , 1 ,
floor((trunc(sysdate) - trunc(sysdate,'MONTH'))/7) )proj_week_number,
3.5 loaner_proj_month_usage
FROM t504i_loaner_month_history t504i ,
t504_consignment t504,
        t504a_consignment_loaner t504acl,
        pbi_fam_critical_system_dim critical_systems,
        t207_set_master t207,
        (SELECT t525.c525_product_request_id,t526.c526_product_request_detail_id, t525.c901_request_for, t526.c207_set_id, t525.c703_sales_rep_id
      ,t525.c525_surgery_date, t525.c525_request_for_id, t525.c525_request_by_id, 
      t525.c703_sales_rep_id rep_id,t703_rep.c703_sales_rep_name rep_name,
      t525.c703_ass_rep_id ass_rep_id,t703_assoc_rep.c703_sales_rep_name ass_rep_name
          FROM t525_product_request t525
             , t526_product_request_detail t526 , t703_sales_rep t703_rep, t703_sales_rep t703_assoc_rep
          WHERE t525.c525_product_request_id = t526.c525_product_request_id
           AND t525.c901_request_for IS NOT NULL
           AND t525.c703_sales_rep_id = t703_rep.c703_sales_rep_id (+)
           AND t525.c703_ass_rep_id = t703_assoc_rep.c703_sales_rep_id (+)
           AND t525.c901_request_for = 4127
         ) request, t504a_loaner_transaction t504a
WHERE t504a.c526_product_request_detail_id = request.c526_product_request_detail_id(+)
      AND t504i.c504a_loaner_transaction_id =   t504a.c504a_loaner_transaction_id
      AND t504i.c504_consignment_id = t504.c504_consignment_id
      AND t504acl.c504_consignment_id     = t504.c504_consignment_id -- For Etch Id
       AND t207.c207_set_id                = t504.c207_set_id
      AND t207.c207_set_sales_system_id   = critical_systems.critical_system_id
;
