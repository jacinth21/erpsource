/**********************************************************************
Purpose: Materialized view with Loaner Tags expected usage data in field for the Score Card Window period.
Author: Yoga
PC: 342,2322,2941
Location: Datawarehouse\Database\DWH\Views\DW\Power_BI\Operations\FAM\pbi_fam_loaner_usage_fact.vw
Code Lookup: 
**********************************************************************/

DROP MATERIALIZED VIEW pbi_fam_loaner_usage_fact;
CREATE MATERIALIZED VIEW pbi_fam_loaner_usage_fact REFRESH COMPLETE START WITH
  (
    SYSDATE
  )
  NEXT (SYSDATE+60/1440)
WITH ROWID AS
(SELECT loaner_proj.*,
	-- PC-3805: Holiday exclusion changes
          CASE
            WHEN loaner_proj.status_fl =21
            THEN 0 -- Disputed
            WHEN loaner_proj.status_fl =22
            THEN 0 -- Missing
              --WHEN loaners.status_fl =60 THEN 0 -- Inactive
            WHEN (no_of_occurances - (holiday_no_of_occur + fedex_waive_days)) <=3
            THEN 0
            WHEN loaner_proj.critical_system_type_id   = 109845
            AND ceil((no_of_occurances - (holiday_no_of_occur + fedex_waive_days)) /7)>7.5
            THEN 7.5
            WHEN loaner_proj.critical_system_type_id   = 109846
            AND ceil((no_of_occurances - (holiday_no_of_occur + fedex_waive_days)) /7)>6
            THEN 6
            ELSE round(ceil((no_of_occurances - (holiday_no_of_occur + fedex_waive_days)) /7),0)
          END AS exp_usage,
      DECODE(floor((trunc(sysdate) -  trunc(sysdate,'MONTH'))/7) , 0 , 1 , 
                                                floor((trunc(sysdate) -  trunc(sysdate,'MONTH'))/7) )proj_week_number,
      3.5 loaner_proj_month_usage,
      CASE
        WHEN loaner_proj.status_fl =21
        THEN 0 -- Disputed
        WHEN loaner_proj.status_fl =22
        THEN 0 -- Missing
          --WHEN loaners.status_fl =60 THEN 0 -- Inactive
        WHEN ((no_of_occurances - (holiday_no_of_occur + fedex_waive_days)) )<=3
        THEN 0
        WHEN ((loaner_proj.last2mon_no_of_occurances - (holiday_last2mon_cnt + fedex_waive_days)))<=3
        THEN 0
        WHEN loaner_proj.critical_system_type_id         = 109845
        AND ceil((loaner_proj.last2mon_no_of_occurances - (holiday_last2mon_cnt + fedex_waive_days)) /7)>7.5
        THEN 7.5
        WHEN loaner_proj.critical_system_type_id         = 109846
        AND ceil((loaner_proj.last2mon_no_of_occurances - (holiday_last2mon_cnt + fedex_waive_days)) /7)>6
        THEN 6
        ELSE round(ceil((loaner_proj.last2mon_no_of_occurances - (holiday_last2mon_cnt + fedex_waive_days)) /7),0)
      END AS last2_mon_exp_usage,
      CASE
        WHEN loaner_proj.status_fl =21
        THEN 0 -- Disputed
        WHEN loaner_proj.status_fl =22
        THEN 0 -- Missing
          --WHEN loaners.status_fl =60 THEN 0 -- Inactive
        WHEN ((loaner_proj.proj_no_loaned_days - (holiday_proj_loaned_cnt + fedex_waive_days)))<=3
        THEN 0
        WHEN loaner_proj.critical_system_type_id   = 109845
        AND ceil((loaner_proj.proj_no_loaned_days - (holiday_proj_loaned_cnt + fedex_waive_days)) /7)>7.5
        THEN 7.5
        WHEN loaner_proj.critical_system_type_id   = 109846
        AND ceil((loaner_proj.proj_no_loaned_days - (holiday_proj_loaned_cnt + fedex_waive_days)) /7)>6
        THEN 6
        ELSE round(ceil((loaner_proj.proj_no_loaned_days - (holiday_proj_loaned_cnt + fedex_waive_days))/7),0)
      END AS proj_exp_usage
    FROM
      (SELECT all_loaners.* ,
        --gm_pkg_sm_tag_order_history.get_number_of_business_day(all_loaners.proj_adj_placement_date,all_loaners.proj_adj_return_date) proj_no_loaned_days
        Floor(all_loaners.proj_adj_return_date-all_loaners.proj_adj_placement_date) proj_no_loaned_days,
        get_comp_holiday_cnt (all_loaners.proj_adj_placement_date, all_loaners.proj_adj_return_date, 1000) holiday_proj_loaned_cnt
      FROM
        (SELECT loaners.critical_system_id,
          loaners.critical_system_name,
          loaners.critical_system_type_id,
          loaners.critical_system_type_name,
          loaners.distributor_id,
          loaners.set_id,
          loaners.set_name,
          loaners.consignment_id,
          loaners.loaner_id,
          loaners.etch_id,
          loaners.status_fl,
          loaners.status,
          loaners.placement_date,
          loaners.return_date,
          loaners.adjusted_placement_date,
          loaners.adjusted_return_date,
          loaners.last2_mon_adj_placement_date,
          loaners.last2_mon_adj_return_date,
          loaners.request_by_id,
          loaners.requestor_name,
          loaners.rep_id,
          loaners.rep_name,
          loaners.ass_rep_id,
          loaners.ass_rep_name,
          CASE
            WHEN trunc(placement_date)>trunc(proj_start_date)
            THEN placement_date
            ELSE proj_start_date
          END AS proj_adj_placement_date,
          CASE
            WHEN (trunc(return_date) < (SYSDATE))
            THEN return_date
            ELSE nvl(return_date,(SYSDATE))
          END AS proj_adj_return_date,
          ceil(loaners.adjusted_return_date-loaners.adjusted_placement_date) no_of_occurances,
          -- PC-3805: Holiday Exclusion
          holiday_no_of_occur,
          ceil(loaners.adjusted_return_date-loaners.last2_mon_adj_placement_date) last2mon_no_of_occurances,
          holiday_last2mon_cnt,
          -- PC_3928: Fedex waive days exclude - getting the product request dtls id
    	  loaners.product_request_detail_id,
		  loaners.product_request_id,
		  loaners.ln_tagid,
		  loaners.fedex_waive_days
        FROM pbi_fam_distributor_loaner_comb_excl_dim loaners
        ) all_loaners
      )loaner_proj
  );
