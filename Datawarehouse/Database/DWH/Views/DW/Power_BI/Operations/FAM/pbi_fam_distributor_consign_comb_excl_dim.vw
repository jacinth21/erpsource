/**********************************************************************
Purpose: Materialized view having data with exclusion applied in expected usage calulation up on  pbi_fam_distributor_consign_dim. Also add last 2 mont cut off data
		Ex: Jun need to be executed from 3M data
Author: Yoga
PC: 342, 2322
Location: Datawarehouse\Database\DWH\Views\DW\Power_BI\Operations\FAM\pbi_fam_distributor_consign_comb_excl_dim.vw
Code Lookup: 
**********************************************************************/

DROP MATERIALIZED VIEW pbi_fam_distributor_consign_comb_excl_dim;
CREATE MATERIALIZED VIEW pbi_fam_distributor_consign_comb_excl_dim REFRESH COMPLETE START WITH
  (
    SYSDATE
  )
  NEXT (SYSDATE+60/1440)
WITH ROWID AS
  (SELECT dscn.distributor_id,
      dscn.tag_id,
      dscn.set_id,
      dscn.placement_date,
      dscn.adjusted_placement_date,
      CASE
        WHEN trunc(adjusted_placement_date)>trunc(last2mon.start_date)
        THEN adjusted_placement_date
        ELSE last2mon.start_date
      END AS last2_mon_adjusted_placement_date,
      dscn.return_date,
      dscn.adjusted_return_date,
      dscn.consignment_id,
      dscn.active_fl,
      dscn.missing_fl
    FROM pbi_fam_distributor_consign_exclusion_dim dscn,
      (SELECT trunc(add_months(SYSDATE, -2),'MONTH') start_date FROM dual
      ) last2mon
  );