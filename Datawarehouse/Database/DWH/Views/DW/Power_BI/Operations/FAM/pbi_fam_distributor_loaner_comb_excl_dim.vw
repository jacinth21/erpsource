/**********************************************************************
Purpose: Materialized view with Loaner Tags expected usage data in field for the Score Card Window period and last2 month cut off data
Author: Yoga
PC: 342,2322,2941
Location: Datawarehouse\Database\DWH\Views\DW\Power_BI\Operations\FAM\pbi_fam_distributor_loaner_comb_excl_dim.vw
Code Lookup: 
**********************************************************************/

DROP MATERIALIZED VIEW pbi_fam_distributor_loaner_comb_excl_dim;
CREATE MATERIALIZED VIEW pbi_fam_distributor_loaner_comb_excl_dim REFRESH COMPLETE START WITH
  (
    SYSDATE
  )
  NEXT (SYSDATE+60/1440)
WITH ROWID AS
(
	-- PC-Scorecard holiday exclusion changes
	SELECT loaner_dim.* ,
	gm_pkg_sm_loaner_history.get_sc_holiday_exclusion_cnt (loaner_dim.last2_mon_adj_placement_date
		, loaner_dim.adjusted_return_date, 1000) holiday_last2mon_cnt
	FROM(
		SELECT loaners.critical_system_id,
			cs.critical_system_name,
			loaners.critical_system_type_id,
			cs.critical_system_type_name,
			loaners.set_id,
			loaners.request_by_id,
			loaners.requestor_name,
			t207.c207_set_nm set_name,
			distributor_id,    
			placement_date,
			return_date,
			adjusted_placement_date,
			adjusted_return_date,
			-- PC-Scorecard holiday exclusion changes
			gm_pkg_sm_loaner_history.get_sc_holiday_exclusion_cnt (loaners.adjusted_placement_date, loaners.adjusted_return_date, 1000) holiday_no_of_occur,
			CASE
			  WHEN trunc(placement_date)>trunc(last2mon.start_date)
			  THEN placement_date
			  ELSE last2mon.start_date
			END AS last2_mon_adj_placement_date,
			adjusted_return_date last2_mon_adj_return_date,
			gm_pkg_sm_tag_order_history.get_last_nth_business_day(SYSDATE,5) proj_start_date,
			CASE
			  WHEN return_date < last_day(SYSDATE)
			  THEN return_date
			  ELSE nvl(return_date,last_day(SYSDATE))
			END AS proj_end_date,
			consignment_id,
			loaner_id,
			etch_id,
			status_fl,
			status,
			loaners.rep_id,
			loaners.rep_name,
			loaners.ass_rep_id,
			loaners.ass_rep_name,
			-- PC-3928: Fedex waive days exclude - getting the product request dtls id
			loaners.product_request_detail_id,
			product_request_id,
			ln_tagid,
			loaners.fedex_waive_days
		  FROM pbi_fam_distributor_loaner_exclusion_dim loaners, t207_set_master t207, pbi_fam_critical_system_dim cs,
			(SELECT trunc(add_months(SYSDATE, -2),'MONTH') start_date FROM dual
			) last2mon
		  WHERE t207.c207_set_id = loaners.set_id
			AND cs.critical_system_id = loaners.critical_system_id
		) loaner_dim
	);
