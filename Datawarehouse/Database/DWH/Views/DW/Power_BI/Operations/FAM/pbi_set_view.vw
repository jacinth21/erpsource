/**********************************************************************
Purpose: Materialized view with Set ID,  System ID and their Hierarchy name.
Author: Naveen
PC: 2104
Location: Datawarehouse\Database\DWH\Views\DW\Power_BI\Operations\FAM\pbi_set_view.vw
**********************************************************************/

DROP MATERIALIZED VIEW pbi_set_view;
CREATE MATERIALIZED VIEW pbi_set_view REFRESH COMPLETE START WITH
  (
    SYSDATE
  )
  NEXT (SYSDATE+60/1440)
WITH ROWID AS
     SELECT t207_set.c207_set_id set_id, t207_set.c207_set_nm set_name, t207_set.c207_set_sales_system_id system_id
     , t207_set.c207_category set_category, t901_type.c901_code_nm set_category_name
     , t207_set.c901_hierarchy hierarchy_id, t901_cons_rpt_id.c901_code_nm hierarchy_name
     , t207_set.c207_type set_type_id
     , t901_set_type.c901_code_nm set_type_nm
     , 'to_code_for_international_set' international_set_flg
  FROM t207_set_master t207_set, t901_code_lookup t901_type, t901_code_lookup t901_cons_rpt_id
        , t901_code_lookup t901_set_type 
 WHERE t207_set.c207_void_fl IS NULL
   AND t207_set.c901_set_grp_type = 1601
   AND t207_set.c207_category = t901_type.c901_code_id(+)
   AND t207_set.c901_hierarchy = t901_cons_rpt_id.c901_code_id(+)
   AND t207_set.c207_type = t901_set_type.c901_code_id(+);