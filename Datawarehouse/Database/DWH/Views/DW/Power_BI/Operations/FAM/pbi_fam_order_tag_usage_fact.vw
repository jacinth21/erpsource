/**********************************************************************
Purpose: Materialized view with DO and Tag used in the tag along with Order and Tag Owner
Author: Yoga
PC: 342,2322
Location: Datawarehouse\Database\DWH\Views\DW\Power_BI\Operations\FAM\pbi_fam_order_tag_usage_fact.vw
Code Lookup: 4127 - Product Loaner, 20100 - Full Set, 70106/70103/70105 -- ICT/ICA/ICS, 1000 - GMNA, 60-In active
**********************************************************************/

DROP MATERIALIZED VIEW pbi_fam_order_tag_usage_fact;
CREATE MATERIALIZED VIEW pbi_fam_order_tag_usage_fact REFRESH COMPLETE START WITH
  (
    SYSDATE
  )
  NEXT (SYSDATE+60/1440)
WITH ROWID AS
(SELECT last2mon.start_date ,
      baseline_sets.critical_system_id ,
      baseline_sets.critical_system_type_id critical_system_type_id ,
      t501d.c5010_tag_id tag_id,
      t501d.c207_set_id set_id,
      t501d.c501_order_id order_id,
      t501.c501_order_date order_date,
      t501d.c501d_order_owner_id order_owner_id,
      t701_order_owner.c701_distributor_name order_owner,
      t501d.c501d_tag_owner_id tag_owner_id,
      t701_tag_owner.c701_distributor_name tag_owner,
      t501d.c501d_credit_owner_id procedure_credit_owner,
      trunc(sysdate) -  trunc(sysdate,'MONTH') mtd_days,
       case  
        when month_dim.sales_business_days <6 then
        5
        else month_dim.sales_business_days
        end as completed_bus_days,
      month_dim.bus_days_for_month cur_month_no_of_bus_days,
      --decode(c501d_invald_share,'Y',0, 1) procedure,
      CASE
        WHEN trunc(t501.c501_order_date)<=trunc(last_day(add_months(SYSDATE, -1)))
        THEN decode(t501d.c501d_invald_share,'Y',0, c501d_system_usage)
      END AS PROCEDURE,
      t501d.c501d_invald_share invald_share,
      t501d.t501d_comments comments,
      CASE
        WHEN (trunc(t501.c501_order_date) > = last2mon.start_date
        AND (trunc(t501.c501_order_date) <=trunc(last_day(add_months(SYSDATE, -1)))))
        THEN decode(t501d.c501d_invald_share,'Y',0, c501d_system_usage)
      END AS last2mon_procedure,
      CASE
        WHEN ( (trunc(t501.c501_order_date) >=act_proj_period.proj_start_date)
        AND (trunc(t501.c501_order_date)     <trunc(SYSDATE)))
        THEN decode(t501d.c501d_invald_share,'Y',0, c501d_system_usage)
      END AS proj_procedure,
      t501d.c501d_credit_owner_id distributor_id,
      baseline_sets.critical_system_name,
      baseline_sets.critical_system_type_name,
      baseline_sets.set_name,
      t501d.c901_order_category order_category_id,
      t901_ord_ctg.c901_code_nm order_category,
      t501d.c207_system_id system_id,
      t501d.c501d_system_usage system_usage 
    FROM t501d_order_tag_usage_details t501d,
      t501_order t501 ,
      pbi_fam_baseline_set_dim baseline_sets,
      t207_set_master t207,
      t701_distributor t701_order_owner,
      t701_distributor t701_tag_owner,
      t901_code_lookup t901_ord_ctg,
      (SELECT trunc(add_months(SYSDATE, -2),'MONTH') start_date FROM dual
      ) last2mon ,  
       (SELECT SYSDATE curret_day, trunc(min(
       CASE
        WHEN cal_date>TRUNC(SYSDATE,'MONTH')
        THEN TRUNC(SYSDATE,'MONTH')
        ELSE cal_date
      END)) AS proj_start_date 
      FROM
        (SELECT cal_date
        FROM date_dim
        WHERE BUS_DAY_IND    ='Y'
        AND TRUNC(cal_date) <= TRUNC(SYSDATE)
        ORDER BY cal_date DESC
        )
      WHERE rownum<=5+1  -- 5 days previos month data until current month reaches 5 business days
      )  act_proj_period,
      (
      SELECT cal_date bus_date, BUS_DAY_IND, HOLIDAY_IND --DECODE(COUNT(1), 0,1,COUNT(1) ) cmpl_bus_days
        FROM date_dim
      WHERE BUS_DAY_IND ='Y'
      AND cal_date> trunc(add_months(SYSDATE, -1),'MONTH')
       AND cal_date< trunc(SYSDATE)
      ) lastMon,  
      ( select bus_days_for_month, sales_business_days, remain_bus_days_for_month 
      from globus_bi_realtime.month_dim where month_year = to_char(SYSDATE,'MM/YYYY')) month_dim
    WHERE (t501d.c901_order_category IS NULL OR t501d.c901_order_category = 111160)
    AND t501d.c501_order_id      = t501.c501_order_id
    AND t501d.c207_set_id          = baseline_sets.set_id
    AND t501d.c207_set_id          = t207.c207_set_id
    AND t501d.c501d_order_owner_id = t701_order_owner.c701_distributor_id(+)
    AND t501d.c501d_tag_owner_id   = t701_tag_owner.c701_distributor_id(+)
    AND t501d.c901_order_category      = t901_ord_ctg.c901_code_id(+)
    AND t207.c207_void_fl         IS NULL
    AND t501d.c501d_void_fl       IS NULL
    AND t501.c501_void_fl         IS NULL
    AND t501.c501_parent_order_id IS NULL -- Remove Child Orders
    AND t501.c501_order_date      >=
      (SELECT trunc(add_months(SYSDATE, -3),'MONTH') from dual    )
    AND  t501.c501_order_date = lastMon.bus_date(+)
  ) 
  UNION ALL
  (SELECT last2mon.start_date ,
      crsys.critical_system_id ,
      crsys.critical_system_type_id critical_system_type_id ,
      t501d.c5010_tag_id tag_id,
      t501d.c207_set_id set_id,
      t501d.c501_order_id order_id,
      t501.c501_order_date order_date,
      t501d.c501d_order_owner_id order_owner_id,
      t701_order_owner.c701_distributor_name order_owner,
      t501d.c501d_tag_owner_id tag_owner_id,
      t701_tag_owner.c701_distributor_name tag_owner,
      t501d.c501d_credit_owner_id procedure_credit_owner,
      trunc(sysdate) -  trunc(sysdate,'MONTH') mtd_days,
       case  
        when month_dim.sales_business_days <6 then
        5
        else month_dim.sales_business_days
        end as completed_bus_days,
      month_dim.bus_days_for_month cur_month_no_of_bus_days,
      --decode(c501d_invald_share,'Y',0, 1) procedure,
      CASE
        WHEN trunc(t501.c501_order_date)<=trunc(last_day(add_months(SYSDATE, -1)))
        THEN decode(t501d.c501d_invald_share,'Y',0, c501d_system_usage)
      END AS PROCEDURE,
      t501d.c501d_invald_share invald_share,
      t501d.t501d_comments comments,
      CASE
        WHEN (trunc(t501.c501_order_date) > = last2mon.start_date
        AND (trunc(t501.c501_order_date) <=trunc(last_day(add_months(SYSDATE, -1)))))
        THEN decode(t501d.c501d_invald_share,'Y',0, c501d_system_usage)
      END AS last2mon_procedure,
      CASE
        WHEN ( (trunc(t501.c501_order_date) >=trunc(gm_pkg_sm_tag_order_history.get_last_nth_business_day(SYSDATE,5)))
        AND (trunc(t501.c501_order_date)     <trunc(SYSDATE)))
        THEN decode(t501d.c501d_invald_share,'Y',0, c501d_system_usage)
      END AS proj_procedure,
      t501d.c501d_credit_owner_id distributor_id,
      crsys.critical_system_name,
      crsys.critical_system_type_name,
      '' set_name,
      t501d.c901_order_category order_category_id,
      t901_ord_ctg.c901_code_nm order_category,
      t501d.c207_system_id system_id,
      t501d.c501d_system_usage system_usage 
    FROM t501d_order_tag_usage_details t501d,
      t501_order t501 ,
      pbi_fam_critical_system_dim crsys,
      t701_distributor t701_order_owner,
      t701_distributor t701_tag_owner,
      t901_code_lookup t901_ord_ctg,
      (SELECT trunc(add_months(SYSDATE, -2),'MONTH') start_date FROM dual
      ) last2mon ,  
       (SELECT SYSDATE curret_day, trunc(min(
       CASE
        WHEN cal_date>TRUNC(SYSDATE,'MONTH')
        THEN TRUNC(SYSDATE,'MONTH')
        ELSE cal_date
      END)) AS proj_start_date 
      FROM
        (SELECT cal_date
        FROM date_dim
        WHERE BUS_DAY_IND    ='Y'
        AND TRUNC(cal_date) <= TRUNC(SYSDATE)
        ORDER BY cal_date DESC
        )
      WHERE rownum<=5+1  -- 5 days previos month data until current month reaches 5 business days
      )  act_proj_period,
      (
      SELECT cal_date bus_date, BUS_DAY_IND, HOLIDAY_IND --DECODE(COUNT(1), 0,1,COUNT(1) ) cmpl_bus_days
        FROM date_dim
      WHERE BUS_DAY_IND ='Y'
      AND cal_date> trunc(add_months(SYSDATE, -1),'MONTH')
       AND cal_date< trunc(SYSDATE)
      ) lastMon,  
      ( select bus_days_for_month, sales_business_days, remain_bus_days_for_month 
      from globus_bi_realtime.month_dim where month_year = to_char(SYSDATE,'MM/YYYY')) month_dim
    WHERE  t501d.c901_order_category = 111161 -- Bulk DO
    AND t501d.c501_order_id      = t501.c501_order_id
    AND t501d.c207_system_id       = crsys.critical_system_id
    AND t501d.c501d_order_owner_id = t701_order_owner.c701_distributor_id(+)
    AND t501d.c501d_tag_owner_id   = t701_tag_owner.c701_distributor_id(+)
    AND t501d.c901_order_category      = t901_ord_ctg.c901_code_id(+)
    AND t501d.c501d_void_fl       IS NULL
    AND t501.c501_void_fl         IS NULL
    AND t501.c501_parent_order_id IS NULL -- Remove Child Orders
    AND t501.c501_order_date      >=
      (SELECT trunc(add_months(SYSDATE, -3),'MONTH') from dual    )
    AND  t501.c501_order_date = lastMon.bus_date(+)
  );