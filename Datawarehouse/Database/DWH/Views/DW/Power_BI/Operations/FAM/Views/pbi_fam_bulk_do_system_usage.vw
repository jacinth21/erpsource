/**********************************************************************
Purpose: View to get bulk DO data, primarily used as a cursor in gm_sc_fetch_bulk_order. Because Bulk DO can be re updated, check only for DO orders to check for duplicate
Author: Yoga
PC: 3534
Location: Datawarehouse\Database\DWH\Views\DW\Power_BI\Operations\FAM\views\pbi_fam_bulk_do_system_usage.vw
Code Lookup: 1600 -       Sales Report Grp, 20175 -             Baseline Case, 111161 - Bulk DO
**********************************************************************/
CREATE OR REPLACE VIEW pbi_fam_bulk_do_system_usage
AS
  SELECT do_data.account_id,
    do_data.distributor_id ,
    do_data.sys_id,
    do_data.qty,
    sys_wtg.sys_wt,
    do_data.order_id,
    order_date,
    round(decode(sys_wtg.sys_wt,0,0,do_data.qty/sys_wtg.sys_wt),2) system_usage
  FROM
    (SELECT t207.c207_set_id sys_id,t207.c207_set_nm,
      t501.c704_account_id account_id,
      t501.c501_order_id order_id,
      t501.c501_order_date order_date,
      NVL(t501.c901_order_type, 2521) order_type,
      t501.c501_distributor_id distributor_id,
      SUM(t502.c502_item_qty) qty
    FROM t207_set_master t207,
      t208_set_details t208,
      t205_part_number t205,
      t502_item_order t502,
      t501_order t501
    WHERE t207.c207_set_id            = t208.c207_set_id
      AND t205.c205_part_number_id      = t208.c205_part_number_id
      AND t205.c205_part_number_id      = t502.c205_part_number_id
      AND t501.c501_order_id            = t502.c501_order_id
     AND t208.c208_void_fl            IS NULL
      AND t207.c901_set_grp_type        =1600
      AND t501.c501_bulk_do_process_fl IS NULL
      AND t205.c205_tracking_implant_fl = 'Y'
      AND t501.c501_order_id NOT       IN
        (SELECT c501_order_id 
        FROM t501d_order_tag_usage_details
        WHERE c501d_void_fl IS NULL
        AND (c901_order_category is null or C901_ORDER_CATEGORY=111160) --DO
        )
    AND t501.c704_account_id IN
      (SELECT c704_account_id
      FROM t704a_account_attribute
      WHERE c901_attribute_type = 111161
        AND c704a_attribute_value ='Y'
        AND c704a_void_fl IS NULL
      )
    GROUP BY t207.c207_set_id,t207.c207_set_nm,
      t501.c501_order_id,
      t501.c501_order_date,
      t501.c901_order_type,
      t501.c704_account_id,
      t501.c501_distributor_id
    ) do_data,    
    (SELECT t207.c207_set_id sys_id,
      t207.c207_set_nm sys_name,
      bvalue.c240_value sys_wt
    FROM t207_set_master t207,
      (SELECT c207_set_id,
        c240_value
      FROM t240_baseline_value t240
      WHERE t240.c901_type = 20175
        AND c240_void_fl    IS NULL
      ) bvalue
    WHERE t207.c901_set_grp_type = 1600
    AND t207.c207_obsolete_fl   IS NULL
    AND t207.c207_void_fl       IS NULL
    AND t207.c207_set_id         = bvalue.c207_set_id(+)
    ) sys_wtg,    
    (SELECT critical_system_id
    FROM PBI_FAM_CRITICAL_SYSTEM_DIM) crsys
  WHERE do_data.sys_id           = sys_wtg.sys_id (+)
    AND do_data.sys_id           = crsys.critical_system_id
    AND do_data.order_type NOT    IN
    ( SELECT c906_rule_value FROM v901_order_type_grp    )
    AND do_data.order_date >=
    (SELECT trunc(add_months(SYSDATE, -3),'MONTH') FROM dual
    );