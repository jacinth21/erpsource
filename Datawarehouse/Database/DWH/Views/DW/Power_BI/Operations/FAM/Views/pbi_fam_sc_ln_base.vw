/**********************************************************************
Purpose: View to get the base for 1. Split months for Loaners, 2. Return updated not processed 3.Not returned LN sets to add to new month
from t504a_loaner transaction by exposing the split process flag, Return process flag
Author: Yoga
PC: 2781
Location: Datawarehouse\Database\DWH\Views\DW\Power_BI\Operations\FAM\pbi_fam_sc_ln_base.vw
Code Lookup: 20100 - Full Set, 4127-Product Loaner, 70106/70103/70105 -- ICT/ICA/ICS
**********************************************************************/

CREATE OR REPLACE VIEW PBI_FAM_SC_LN_BASE
AS
  SELECT ln_fact.critical_system_id,
  ln_fact.critical_system_type_id,
  ln_fact.distributor_id,
  ln_fact.set_id,
  ln_fact.loaner_id,
  ln_fact.start_date,
  ln_fact.end_date,
  ln_fact.total_days_kept,
  ln_fact.consignment_id ,
  ln_fact.split_process_fl,
  ln_fact.return_dt,
  ln_fact.return_process_fl
FROM
  (SELECT critical_system.critical_system_id critical_system_id,
    critical_system.critical_system_name critical_system_name,
    critical_system.critical_system_type_id,
    t701.c701_distributor_id distributor_id,
    t504.c207_set_id set_id,
    t504a.c504a_loaner_transaction_id loaner_id,
    CASE
      WHEN trunc(t504a.c504a_loaner_dt) >= sc_start_date
      THEN trunc(t504a.c504a_loaner_dt)
      ELSE sc_start_date
    END AS start_date,
    nvl(t504a.c504a_return_dt,last_day( SYSDATE)) end_date,
    round(nvl(t504a.c504a_return_dt,last_day( SYSDATE)) - t504a.c504a_loaner_dt,0) total_days_kept,
    t504.c504_consignment_id consignment_id,
    t504a.c504a_split_process_fl split_process_fl,
    t504a.c504a_return_dt return_dt,
    t504a.c504a_return_process_fl return_process_fl,
    t504acl.c504a_etch_id etch_id,
    t504acl.c504a_status_fl status_fl,
    decode (t504acl.c504a_status_fl, 0, '', 5, '', 7, '', 10, '', 13, '', 16, '', 20, '',21, 'Disputed', 22, 'Missing', 23, '', 25, '', 30, '', 40, '', 50, '', 55, '', 58, '', 60, '', 24, '') status
  FROM t504_consignment t504,
    pbi_fam_baseline_set_dim baseset,
    pbi_fam_critical_system_dim critical_system,
    t504a_loaner_transaction t504a ,
    t504a_consignment_loaner t504acl,
    t703_sales_rep t703,
    t701_distributor t701,
    (SELECT to_date(c906_rule_value,'MM/DD/YYYY') sc_start_date
    FROM t906_rules
    WHERE c906_rule_grp_id = 'SC_START_DATE'
    ) sc_rule
  WHERE (trunc(c504a_return_dt)      >= sc_rule.sc_start_date
      OR c504a_return_dt                 IS NULL)
    AND t504a.c504_consignment_id       =t504.c504_consignment_id
    AND baseset.set_id                  = t504.c207_set_id
    AND baseset.critical_system_id      = critical_system.critical_system_id
    AND t504acl.c504_consignment_id     = t504.c504_consignment_id -- For Etch Id
    AND t504a.c703_sales_rep_id         = t703.c703_sales_rep_id
    AND t703.c701_distributor_id        = t701.c701_distributor_id
    AND NOT (t504acl.c504a_status_fl    = 60
    AND t504a.c504a_return_dt          IS NULL)
    AND t504.c1900_company_id           =1000
    AND c504_type                       = 4127
    AND t701.c1900_company_id           =1000
    AND t701.c901_distributor_type NOT IN (70106,70103,70105) -- ICT/ICA/ICS
    AND t504a.c504a_void_fl            IS NULL
    AND t504.c504_void_fl              IS NULL
    AND t701.c701_void_fl              IS NULL
    AND t703.c703_void_fl              IS NULL
    AND t504acl.c504a_void_fl          IS NULL
    AND ((t504a.c504a_split_process_fl IS NULL)
      OR (t504a.c504a_return_dt          IS NOT NULL
        AND t504a.c504a_return_process_fl  IS NULL)
      OR (t504a.c504a_return_dt          IS NULL))
  ) ln_fact ;