/**********************************************************************
Purpose: Materialized view with DO and Tag used in the tag along with Order and Tag Owner
Author: Yoga
PC: 3213
Location: Datawarehouse\Database\DWH\Views\DW\Power_BI\Operations\FAM\pbi_fam_order_tag_usage_fact.vw
Code Lookup: 4127 - Product Loaner, 20100 - Full Set, 70106/70103/70105 -- ICT/ICA/ICS, 1000 - GMNA, 60-In active
**********************************************************************/

DROP MATERIALIZED VIEW pbi_fam_order_mon_split_tag_usage_fact;
CREATE MATERIALIZED VIEW pbi_fam_order_mon_split_tag_usage_fact REFRESH COMPLETE START WITH
  (
    SYSDATE
  )
  NEXT (SYSDATE+60/1440)
WITH ROWID AS
(SELECT 
      baseline_sets.critical_system_id ,
      baseline_sets.critical_system_type_id critical_system_type_id ,
      t501d.c5010_tag_id tag_id,
      t501d.c207_set_id set_id,
      t501d.c501_order_id order_id,
      t501.c501_order_date order_date,      
      TO_CHAR( t501.c501_order_date, 'YYYY/MM' ) Year_Month,
      t501d.c501d_order_owner_id order_owner_id,
      t701_order_owner.c701_distributor_name order_owner,
      t501d.c501d_tag_owner_id tag_owner_id,
      t701_tag_owner.c701_distributor_name tag_owner,
      t501d.c501d_credit_owner_id procedure_credit_owner,
      trunc(sysdate) -  trunc(sysdate,'MONTH') mtd_days,
      gm_pkg_sm_tag_order_history.get_number_of_business_day(
          globus_app.gm_pkg_sm_tag_order_history.get_last_nth_business_day(sysdate,5),(sysdate)) completed_bus_days,
      gm_pkg_sm_tag_order_history.get_number_of_business_day(trunc(sysdate,'MONTH'), last_day(sysdate)) cur_month_no_of_bus_days,
      --decode(c501d_invald_share,'Y',0, 1) procedure,
      CASE
        WHEN trunc(t501.c501_order_date)<=trunc(last_day(add_months(SYSDATE, -1)))
        THEN decode(t501d.c501d_invald_share,'Y',0, 1)
      END AS PROCEDURE,
      t501d.c501d_invald_share invald_share,
      t501d.t501d_comments comments,
      CASE
        WHEN ( (trunc(t501.c501_order_date) >=trunc(gm_pkg_sm_tag_order_history.get_last_nth_business_day(SYSDATE,5)))
        AND (trunc(t501.c501_order_date)     <trunc(SYSDATE)))
        THEN decode(t501d.c501d_invald_share,'Y',0, 1)
      END AS proj_procedure,
      decode(t501d.c501d_invald_share,'Y',0, 1) all_procedure,
 	  t501d.c501d_credit_owner_id distributor_id
    FROM t501d_order_tag_usage_details t501d,
      t501_order t501 ,
      pbi_fam_baseline_set_dim baseline_sets,
      t207_set_master t207,
      t701_distributor t701_order_owner,
      t701_distributor t701_tag_owner
  WHERE t501d.c501_order_id      = t501.c501_order_id
    AND t501d.c207_set_id          = baseline_sets.set_id
    AND t501d.c207_set_id          = t207.c207_set_id
    AND t501d.c501d_order_owner_id = t701_order_owner.c701_distributor_id(+)
    AND t501d.c501d_tag_owner_id   = t701_tag_owner.c701_distributor_id(+)
    AND t207.c207_void_fl         IS NULL
    AND t501.c501_parent_order_id IS NULL 
    AND t501.c501_order_date      >=(SELECT to_date(c906_rule_value,'MM/DD/YYYY')
      FROM t906_rules
      WHERE c906_rule_grp_id = 'SC_START_DATE'
      AND c906_void_fl      IS NULL
      )
  );