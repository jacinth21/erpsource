/**********************************************************************
Purpose: Materialized view having US Consignment Tags expected usage data in field for the Score Card Window period 
Author: Yoga
PC: 342,2322
Location:  Datawarehouse\Database\DWH\Views\DW\Power_BI\Operations\FAM\pbi_fam_consignment_usage_fact.vw
Code Lookup: 70106/70103/70105 -- ICT/ICA/ICS, 1000 - GMNA
**********************************************************************/

DROP MATERIALIZED VIEW pbi_fam_consignment_usage_fact;
CREATE MATERIALIZED VIEW pbi_fam_consignment_usage_fact REFRESH COMPLETE START WITH
  (
    SYSDATE
  )
  NEXT (SYSDATE+60/1440)
WITH ROWID AS
(SELECT baseline_sets.critical_system_id,
  baseline_sets.critical_system_name,
  baseline_sets.critical_system_type_id,
  baseline_sets.critical_system_type_name,
  comp_combined_usage.distributor_id,
  comp_combined_usage.tag_id,
  baseline_sets.set_id,
  baseline_sets.set_name,
  comp_combined_usage.consignment_id,
  comp_combined_usage.missing_fl,
  comp_combined_usage.placement_date,
  comp_combined_usage.return_date,
  comp_combined_usage.adjusted_placement_date,
  comp_combined_usage.adjusted_return_date,
  comp_combined_usage.last2mon_adjusted_placement_date,
  comp_combined_usage.last2mon_adjusted_return_date,
  comp_combined_usage.proj_adj_placement_date,
  comp_combined_usage.proj_adj_return_date,
  comp_combined_usage.days_kept,
  comp_combined_usage.last2mon_days_kept,
  round((comp_combined_usage.proj_adj_return_date          - comp_combined_usage.last2mon_adjusted_placement_date),0) proj_days_kept ,
  round((comp_combined_usage.adjusted_return_date          - comp_combined_usage.adjusted_placement_date)/29,0) completed_month_kept ,
  round((comp_combined_usage.last2mon_adjusted_return_date - last2mon_adjusted_placement_date)/29,0) last2mon_month_kept ,
  round((comp_combined_usage.proj_adj_return_date          - comp_combined_usage.last2mon_adjusted_placement_date)/29,0) proj_month_kept ,
  comp_combined_usage.completed_month_turn,
  comp_combined_usage.last2mon_month_turn,
  t504h_proj.c504h_month_turn proj_month_turn,
  comp_combined_usage.turn_rate turn_rate,
  comp_combined_usage.last2mon_turn_rate last2mon_exp_usage,
  decode(comp_combined_usage.missing_fl,1,0,t504h_proj.c504h_turn_rate) tot_proj_exp_usage
FROM
  (SELECT baseline_sets.critical_system_id,
    baseline_sets.critical_system_type_id,
    t701.c701_distributor_id distributor_id,
    dscnexc.tag_id,
    baseline_sets.set_id,
    dscnexc.consignment_id,
    dscnexc.missing_fl,
    dscnexc.placement_date,
    dscnexc.return_date,
    dscnexc.adjusted_placement_date,
    dscnexc.adjusted_return_date,
    dscnexc.last2_mon_adjusted_placement_date last2mon_adjusted_placement_date,
    dscnexc.adjusted_return_date last2mon_adjusted_return_date,
    CASE
      WHEN trunc(dscnexc.placement_date)>trunc(SYSDATE,'MONTH') 
      THEN dscnexc.placement_date
      ELSE trunc(SYSDATE,'MONTH')
    END AS proj_adj_placement_date,
    CASE
      WHEN dscnexc.return_date < last_day(SYSDATE)
      THEN dscnexc.return_date
      ELSE nvl(dscnexc.return_date,last_day(SYSDATE))
    END AS proj_adj_return_date,
    round((dscnexc.adjusted_return_date-dscnexc.adjusted_placement_date),0) days_kept,
    round((dscnexc.adjusted_return_date-dscnexc.last2_mon_adjusted_placement_date),0) last2mon_days_kept,
    t504h.c504h_month_turn completed_month_turn,
    t504h_2mon.c504h_month_turn last2mon_month_turn,
    decode(dscnexc.missing_fl,1,0,t504h.c504h_turn_rate) turn_rate,
    decode(dscnexc.missing_fl,1,0,t504h_2mon.c504h_turn_rate) last2mon_turn_rate
  FROM pbi_fam_distributor_consign_comb_excl_dim dscnexc,
    t701_distributor t701,
    (SELECT critical_system_id,
      critical_system_name,
      critical_system_type_id,
      critical_system_type_name,
      set_id,
      set_name,
      critical_system_fl
    FROM pbi_fam_baseline_set_dim
    WHERE critical_system_fl = 'Y'
    ) baseline_sets,
    t504h_consignment_turn_rate t504h,
    t504h_consignment_turn_rate t504h_2mon
  WHERE dscnexc.set_id                = baseline_sets.set_id(+)
    AND dscnexc.distributor_id          = t701.c701_distributor_id
    AND t504h.c901_system_type          = baseline_sets.critical_system_type_id
    AND t504h_2mon.c901_system_type     = baseline_sets.critical_system_type_id
    AND t504h.c504h_month_turn          = trunc((dscnexc.adjusted_return_date - dscnexc.adjusted_placement_date)/29)
    AND t504h_2mon.c504h_month_turn     = trunc((dscnexc.adjusted_return_date - dscnexc.last2_mon_adjusted_placement_date)/29)
    AND t701.c1900_company_id           =1000
    AND t701.c901_distributor_type NOT                            IN (70106,70103,70105) -- ICT/ICA/ICS
    AND (dscnexc.adjusted_return_date  >=trunc(add_months(SYSDATE, -3),'MONTH')
    OR dscnexc.adjusted_return_date    IS NULL)
    --AND dscnexc.adjusted_placement_date < last_day(add_months(SYSDATE, -1))
  ) comp_combined_usage, --3891
  (SELECT critical_system_id,
    critical_system_name,
    critical_system_type_id,
    critical_system_type_name,
    set_id,
    set_name,
    critical_system_fl
  FROM pbi_fam_baseline_set_dim
  WHERE critical_system_fl = 'Y'
  ) baseline_sets,
  t504h_consignment_turn_rate t504h_proj
WHERE comp_combined_usage.set_id = baseline_sets.set_id(+)
AND t504h_proj.c901_system_type  = baseline_sets.critical_system_type_id
AND t504h_proj.c504h_month_turn  = trunc((comp_combined_usage.proj_adj_return_date - comp_combined_usage.last2mon_adjusted_placement_date)/29)
);
