/**********************************************************************
Purpose: Materialized view with Set ID and Tag ID and their usages.
Author: Naveen
PC: 2104
Location: Datawarehouse\Database\DWH\Views\DW\Power_BI\Operations\FAM\pbi_active_front_loaders.vw
**********************************************************************/

DROP MATERIALIZED VIEW pbi_active_front_loaders;
CREATE MATERIALIZED VIEW pbi_active_front_loaders REFRESH COMPLETE START WITH
  (
    SYSDATE
  )
  NEXT (SYSDATE+60/1440)
WITH ROWID AS
   SELECT t5010e.c5010_tag_id,
    t5010e.c5010e_placement_date,
    t5010e.c5010e_expiry_date ,
    t207.c207_set_id,
    t525.c525_request_for_id,
    t7110.c7110_m1 ,
    t7110.c7110_m2,
    t7110.c7110_m3,
    t7110.c7110_m4,
    t7110.c7110_m5,
    t7110.c7110_m6,
    t7110.c7110_avg_3months,
    t7110.c7110_hq_notes_on_request hq_notes 
    -- set ID, Field sales ID
  FROM globus_app.t5010e_tag_req_fam t5010e ,
    globus_app.t7110_case_req_fam_details t7110,
    t207_set_master t207,
    t526_product_request_detail t526,
    t525_product_request t525
  WHERE t5010e.c5010e_active_fl            = 1
  AND t7110.c526_product_request_detail_id = t5010e.c5010e_last_trans_id
  AND t526.c526_product_request_detail_id = t7110.c526_product_request_detail_id
  AND t526.c207_set_id = t207.c207_set_id
  AND t525.C525_product_request_id = t526.c525_product_request_id
  AND t7110.c7110_request_type = 2;
  