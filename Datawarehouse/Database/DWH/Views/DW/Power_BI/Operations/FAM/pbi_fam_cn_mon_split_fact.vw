/**********************************************************************
*Purpose: Materialized View to get CN SC month split data
Author: Yoga
PC: 2781
**********************************************************************/
DROP MATERIALIZED VIEW pbi_fam_cn_mon_split_fact ;
CREATE MATERIALIZED VIEW pbi_fam_cn_mon_split_fact REFRESH COMPLETE START WITH
  (
    SYSDATE
  )
  NEXT (SYSDATE+480/1440)
WITH ROWID AS
SELECT baseline_sets.critical_system_id,
  baseline_sets.critical_system_type_id,
  t504h.c504g_tag_transaction_id txn_id,
  t504h.c5010_tag_id tag_id,
  t701.c701_distributor_id distributor_id,
  t504h.c207_set_id set_id,
  (c504h_month_end - c504h_month_start)+1 days_kept,
  decode(to_char(t504h.c504h_month_start,'MM'), to_char(SYSDATE,'MM'),0,t504h.c504h_exp_usage) cmpl_exp_usage, 
  decode(to_char(t504h.c504h_month_start,'MM'), to_char(SYSDATE,'MM'),t504h.c504h_exp_usage,0) proj_Exp_usage,
  t504h.c504h_exp_usage exp_usage,
  c504h_month_end split_end_date,
  c504h_month_start split_start_date,
  c504h_placement_date placement_date,
  c504h_return_date return_date,
  t504g.c504g_txn_id consignment_id,
  to_char( c504h_month_start, 'MM' ) split_month,
  to_char( c504h_month_start, 'YYYY/MM' ) year_month
FROM t504h_cons_tag_month_history t504h , t504g_consignment_tag_history t504g,
  pbi_fam_critical_system_dim critical_system,
  t701_distributor t701,
  pbi_fam_baseline_set_dim baseline_sets
WHERE t504g.c504g_tag_transaction_id = t504h.c504g_tag_transaction_id
  AND t504h.c207_set_id              = baseline_sets.set_id
  AND t701.c701_distributor_id         = t504h.c701_distributor_id
  AND t701.c1900_company_id            = 1000
  AND t701.c901_distributor_type NOT  IN (70106,70103,70105) -- ICT/ICA/ICS
  AND baseline_sets.critical_system_id = critical_system.critical_system_id
  AND baseline_sets.critical_system_fl = 'Y';
