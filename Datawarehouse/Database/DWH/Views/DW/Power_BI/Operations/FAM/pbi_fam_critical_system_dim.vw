/**********************************************************************
Purpose: Materialized view having Critical Systems which is based on the flag in t207_Set_Master atble
Author: Yoga
PC: 342
Location: Datawarehouse\Database\DWH\Views\DW\Power_BI\Operations\FAM\pbi_fam_critical_system_dim.vw
Code Lookup: 
**********************************************************************/

DROP MATERIALIZED VIEW pbi_fam_critical_system_dim;
CREATE MATERIALIZED VIEW pbi_fam_critical_system_dim REFRESH COMPLETE START WITH
  (
    SYSDATE
  )
  NEXT (SYSDATE+60/1440)
WITH ROWID AS
(SELECT t207.c207_set_id critical_system_id,
      t207.c901_critical_system_type critical_system_type_id,
      t207.c207_set_nm critical_system_name,
      sys_type.c901_code_nm critical_system_type_name
    FROM t207_set_master t207,
      t901_code_lookup sys_type
    WHERE t207.c901_critical_system_type = sys_type.c901_code_id (+)
    AND t207.c207_critical_system_fl     ='Y'
    AND c207_void_fl                    IS NULL
  );
  /