/**********************************************************************
Purpose: Materialized view with System ID, System Name and Critical System Name.
Author: Naveen
PC: 2104
Location: Datawarehouse\Database\DWH\Views\DW\Power_BI\Operations\FAM\pbi_system_view.vw
**********************************************************************/

DROP MATERIALIZED VIEW pbi_system_view;
CREATE MATERIALIZED VIEW pbi_system_view REFRESH COMPLETE START WITH
  (
    SYSDATE
  )
  NEXT (SYSDATE+60/1440)
WITH ROWID AS
  SELECT t207.c207_set_id system_id, t207.c207_set_nm system_name, t207.c207_critical_system_fl critical_system_fl
     , sys_type.c901_code_nm critical_system_type_name
  FROM t207_set_master t207, t901_code_lookup sys_type
 WHERE t207.c901_critical_system_type = sys_type.c901_code_id(+)
   AND t207.c901_set_grp_type = 1600 
   AND c207_void_fl IS NULL;
  