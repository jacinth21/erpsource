/**********************************************************************
Purpose: Materialized view to keep System set relation for Baseline (Full Set) and Supporting Set
Author: Yoga
PC: 3534
Location: Datawarehouse\Database\DWH\Views\DW\Power_BI\Operations\FAM\pbi_fam_system_set_map_dim.vw
Code Lookup:  20100 - Full Set, 20101	Supporting set
**********************************************************************/

DROP MATERIALIZED VIEW pbi_fam_system_set_map_dim;
CREATE MATERIALIZED VIEW pbi_fam_system_set_map_dim REFRESH COMPLETE START WITH
  (
    SYSDATE
  )
  NEXT (SYSDATE+60/1440)
WITH ROWID AS
(SELECT t207_system.c207_set_id critical_system_id,
  t207_system.c207_set_nm critical_system_name,
  t207_system.c901_critical_system_type critical_system_type_id,
  t901_type.c901_code_nm critical_system_type_name,
  t207_set.c207_set_id set_id,
  t207_set.c207_set_nm set_name,
  t207_system.c207_critical_system_fl critical_system_fl,
  t207_set.c901_cons_rpt_id set_Type_id, get_code_name(t207_set.c901_cons_rpt_id) set_Type
FROM t207_set_master t207_system,
  t207_set_master t207_set,
  t901_code_lookup t901_type
WHERE t207_set.c207_set_sales_system_id = t207_system.c207_set_id
  AND t207_system.c901_critical_system_type = t901_type.c901_code_id(+)
  AND t207_set.c207_set_id NOT LIKE unistr('%EUR')
  AND t207_set.c207_set_id NOT LIKE unistr('%EURLN')
  AND t207_set.c207_set_id NOT LIKE unistr('%JPN')
  AND t207_set.c207_set_id NOT LIKE unistr('%JPNLN')
  AND t207_set.c207_set_id NOT LIKE unistr('%ED')
  AND t207_set.c207_set_id NOT LIKE unistr('%DS')
  AND t207_set.c207_set_id NOT LIKE unistr('%GCARE')
  AND t207_set.c207_set_id NOT LIKE unistr('%ITA')
  AND t207_set.c207_set_id NOT LIKE unistr('%DS')
  AND t207_set.c207_set_id NOT LIKE unistr('%LS')
);