/**********************************************************************
Purpose: View to show 3 month Expected Vs Actual usage for consignment and Loaner along with efficiency calculation
Author: Yoga
PC: 342
Location: \Database\Views\Sales\v_score_card_efficiency.vw
Code Lookup: 
**********************************************************************/

CREATE OR REPLACE VIEW v_score_card_efficiency AS
(SELECT t701.c701_distributor_id distributor_id,
  t701.c701_distributor_name distributor_name,
  cs.critical_system_id,
  cs.critical_system_name,
  consignment.cons_system_count sys_count,
  consignment.missing_count,
  consignment.expected_usage CN_Completed,
  consignment.LAST2MON_EXP_USAGE CN_Last2_month,
  consignment.TOT_PROJ_EXP_USAGE CN_tot_proj,
  loaner.expected_usage LN_Completed,
  loaner.last2mon_exp_usage LN_Last2_month,
  loaner.MTD_Proj LN_MTD_Proj,
  loaner.Month_Proj LN_Projected,
  NVL(loaner.last2mon_exp_usage, 0) + NVL(Month_Proj, 0) LN_Total_Projected,
  NVL(consignment.expected_usage, 0) + NVL(loaner.expected_usage, 0) Exp_Usage_Completed,
  NVL(consignment.tot_proj_exp_usage ,0)+(NVL(loaner.last2mon_exp_usage, 0) + NVL(Month_Proj, 0)) Exp_Usage_Projected,
  do_tag.procedure_completed Proc_Completed,
  do_tag.procedure_Last2Month Proc_Last2_month,
  do_tag.procedure_MTD_Projected Proc_MTD_Projected,
  round(do_tag.Procedure_Projected, 1) Proc_Projected,
  --PC-3528: Projection score decimal issue fix
  (NVL(do_tag.procedure_Last2Month, 0) + NVL(do_tag.Procedure_Projected, 0)) Proc_Total_Projected,
  round((NVL(do_tag.procedure_completed, 0)) - ( NVL(consignment.expected_usage, 0) + NVL(loaner.expected_usage, 0)), 1) Efficiency_Completed,
  --PC-3528: Projection score decimal issue fix
  (NVL(do_tag.procedure_Last2Month, 0) + NVL(do_tag.Procedure_Projected, 0)) - (NVL(consignment.tot_proj_exp_usage, 0) + (NVL(loaner.last2mon_exp_usage, 0) + NVL(Month_Proj, 0))) Efficiency_Projected,
  decode((consignment.expected_usage+loaner.expected_usage),0,0,round((do_tag.procedure_completed) /(consignment.expected_usage+loaner.expected_usage)*100,1)) Efficiency_Score_Completed,
  decode((consignment.tot_proj_exp_usage+(loaner.last2mon_exp_usage+Month_Proj)),0,0,round((do_tag.procedure_Last2Month+do_tag.Procedure_Projected) /(consignment.tot_proj_exp_usage+(loaner.last2mon_exp_usage+Month_Proj))*100,1)) Efficiency_Score_Projected 
FROM
  (SELECT loan.distributor_id,
    loan.critical_system_id,
    sum(loan.exp_usage) expected_usage,
    sum(loan.LAST2_MON_EXP_USAGE) last2mon_exp_usage,
    sum(loan.PROJ_EXP_USAGE) MTD_Proj,
    sum(loan.LOANER_PROJ_MONTH_USAGE) PROJ_MONTH_USAGE,    
    loan.PROJ_WEEK_NUMBER,
    round((sum(loan.PROJ_EXP_USAGE)/(loan.PROJ_WEEK_NUMBER))*(loan.LOANER_PROJ_MONTH_USAGE),1)  Month_Proj
  FROM pbi_fam_loaner_usage_fact loan 
  GROUP BY loan.distributor_id,
    loan.critical_system_id,
    loan.LOANER_PROJ_MONTH_USAGE,
    loan.PROJ_WEEK_NUMBER
  ) loaner,
  (SELECT cons.distributor_id,
    cons.critical_system_id,
    count(1) cons_system_count,
    count(cons.missing_fl) missing_count,
    sum(cons.turn_rate) expected_usage,
    sum(cons.last2mon_exp_usage) last2mon_exp_usage,
    sum(cons.tot_proj_exp_usage) tot_proj_exp_usage
  FROM pbi_fam_consignment_usage_fact cons
  GROUP BY cons.distributor_id,
    critical_system_id
  ) consignment,
  (SELECT dotag.critical_system_id,
    dotag.procedure_credit_owner,
    sum(PROCEDURE) procedure_completed,
    sum(LAST2MON_PROCEDURE) procedure_Last2Month,
    sum(PROJ_PROCEDURE) procedure_MTD_Projected,
    (sum(dotag.PROJ_PROCEDURE)/(dotag.completed_bus_days) *(dotag.CUR_MONTH_NO_OF_BUS_DAYS)) Procedure_Projected
  FROM pbi_fam_order_tag_usage_fact dotag
  -- PC-5101: DO Classification changes
  WHERE dotag.allowed_tag IN (SELECT token FROM v_in_list)
  GROUP BY  critical_system_id,procedure_credit_owner,dotag.completed_bus_days,dotag.CUR_MONTH_NO_OF_BUS_DAYS
  ) do_tag,  pbi_fam_critical_system_dim cs, t701_distributor t701
  -- PC-3676: Bulk DO changes (to changes set id to system id)
WHERE cs.CRITICAL_SYSTEM_ID = consignment.critical_system_id (+)
AND cs.CRITICAL_SYSTEM_ID = loaner.critical_system_id (+)
AND cs.CRITICAL_SYSTEM_ID = do_tag.critical_system_id (+)
AND t701.C701_DISTRIBUTOR_ID  =  consignment.distributor_id (+)
AND t701.C701_DISTRIBUTOR_ID = loaner.distributor_id  (+)
AND t701.C701_DISTRIBUTOR_ID  =  do_tag.procedure_credit_owner (+)
) order by t701.c701_distributor_name, cs.critical_system_name;
/