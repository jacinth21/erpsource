/**********************************************************************
Purpose: Materialized view having data with exclusion applied in expected usage calulation up on  pbi_fam_distributor_consign_dim. 
		Ex: Jun need to be executed from 3M data
Author: Yoga
PC: 342,2322
Location: Datawarehouse\Database\DWH\Views\DW\Power_BI\Operations\FAM\pbi_fam_distributor_consign_exclusion_dim.vw
Code Lookup: 
**********************************************************************/

DROP MATERIALIZED VIEW pbi_fam_distributor_consign_exclusion_dim;
CREATE MATERIALIZED VIEW pbi_fam_distributor_consign_exclusion_dim REFRESH COMPLETE START WITH
  (
    SYSDATE
  )
  NEXT (SYSDATE+60/1440)
WITH ROWID AS
(SELECT dscn.distributor_id,
  dscn.tag_id,
  dscn.set_id,
  dscn.placement_date,
  CASE
    WHEN trunc(adjusted_placement_date)>trunc(start_date)
    THEN adjusted_placement_date
    ELSE nvl(start_date,adjusted_placement_date)
  END AS adjusted_placement_date,
  dscn.return_date,
  dscn.adjusted_return_date,
  dscn.consignment_id,
  dscn.active_fl,
  dscn.missing_fl
FROM pbi_fam_distributor_consign_dim dscn
WHERE (dscn.adjusted_return_date >=trunc(add_months(SYSDATE, -3),'MONTH')
    OR dscn.adjusted_return_date   IS NULL) 
);