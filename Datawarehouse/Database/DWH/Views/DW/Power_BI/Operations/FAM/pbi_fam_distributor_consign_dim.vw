/**********************************************************************
Purpose: Materialized view having data with Adjustment Placement and return date based on scorecard window period
Author: Yoga
PC: 342
Location: Datawarehouse\Database\DWH\Views\DW\Power_BI\Operations\FAM\pbi_fam_distributor_consign_dim.vw
Code Lookup: 
**********************************************************************/

DROP MATERIALIZED VIEW pbi_fam_distributor_consign_dim;
CREATE MATERIALIZED VIEW pbi_fam_distributor_consign_dim REFRESH COMPLETE START WITH
  (
    SYSDATE
  )
  NEXT (SYSDATE+60/1440)
WITH ROWID AS
(SELECT dt.st_dt start_date,
  t504g.c701_distributor_id distributor_id,
  t504g.c5010_tag_id tag_id,
  t504g.c207_set_id set_id,
  t504g.c504g_placement_date placement_date,
  CASE
    WHEN TRUNC(t504g.c504g_placement_date) >= TRUNC(add_months(SYSDATE, -3),'MONTH')
    THEN TRUNC(t504g.c504g_placement_date)
    WHEN TRUNC(t504g.c504g_placement_date) < TRUNC(add_months(SYSDATE, -3),'MONTH')
    THEN TRUNC(add_months(SYSDATE,                                     -3),'MONTH')
  END AS adjusted_placement_date,
  t504g.c504g_return_date return_date,
  CASE
    WHEN TRUNC(t504g.c504g_return_date) >= last_day(add_months(SYSDATE, -1))
    THEN last_day(add_months(SYSDATE,                                   -1))
    ELSE NVL(t504g.c504g_return_date, last_day(add_months(SYSDATE,      -1)))
  END AS adjusted_return_date,
  t504g.c504g_txn_id Consignment_id,
  t504g.c504g_active_fl active_fl,
  t504g.c504g_missing_fl missing_fl
FROM t504g_consignment_tag_history t504g,
  (SELECT to_date(c906_rule_value,'MM/DD/YYYY') st_dt
  FROM t906_rules
  WHERE c906_rule_grp_id = 'SC_START_DATE'
    AND c906_void_fl IS NULL
  ) dt
WHERE c504g_void_fl IS NULL
);
/