/**********************************************************************
Purpose: Materialized view having distributor data with sales hierarchy
Author: Yoga
PC: 343
Location: Datawarehouse\Database\DWH\Views\DW\Power_BI\Operations\FAM\pbi_fam_distributor_hierachy.vw
Code Lookup:8001 - VP, 8000 - AD
**********************************************************************/

DROP MATERIALIZED VIEW pbi_fam_distributor_hierachy;
CREATE MATERIALIZED VIEW pbi_fam_distributor_hierachy REFRESH COMPLETE START WITH
  (
    SYSDATE
  )
  NEXT (SYSDATE+60/1440)
WITH ROWID AS 
(SELECT to_number(t701.c701_distributor_id) distributor_id ,
  t701.c701_distributor_name distributor_Name,
  dist_vp.vp_id vp_id,
  dist_vp.vp_name vp_name,
  dist_ad.ad_id ad_id,
  dist_ad.ad_name ad_name,
  Upper(dist_vp.c101_email_id) vp_email ,
  Upper(dist_ad.c101_email_id) ad_email,
  dist_ad.region_id region_id,
  dist_ad.region_name region_name,
  dist_vp.Market_id Market_Id,
  dist_vp.Division_Id Division_Id,
  dist_vp.ZONE_ID ZONE_ID
FROM t701_distributor t701,
  (SELECT t708.c101_user_id vp_id,
    c101_user_f_name
    || ' '
    || c101_user_l_name vp_name,
    t701.c701_distributor_id distributor_id,
    t101.c101_email_id,
    t708.c901_region_id region_id,
    t901_region.c901_code_nm region_name,
    t710.C901_ZONE_ID Zone_Id,
    t710.C901_DIVISION_ID Market_id,
    t710.C901_COMPANY_ID Division_Id
  FROM t701_distributor t701,
    t708_region_asd_mapping t708,
    t101_user t101 ,
    t901_code_lookup t901_region,
    T710_SALES_HIERARCHY t710
  WHERE t708.c901_region_id   = t701.c701_region
  AND t101.c101_user_id       = t708.c101_user_id
  AND t708.c901_region_id     = t901_region.c901_code_id (+)
  and t708.c901_region_id  = t710.C901_AREA_ID(+)
  AND c708_delete_fl         IS NULL
  AND t708.c901_user_role_type=8001
  AND t710.c710_void_fl IS NULL
  and t710.c710_active_fl = 'Y'
  ) dist_vp, 
  (SELECT t708.c101_user_id ad_id,
    c101_user_f_name
    || ' '
    || c101_user_l_name ad_name,
    t701.c701_distributor_id distributor_id,
    t101.c101_email_id,
    t708.c901_region_id region_id,
    t901_region.c901_code_nm region_name
  FROM t701_distributor t701,
    t708_region_asd_mapping t708,
    t101_user t101 ,
    t901_code_lookup t901_region
  WHERE t708.c901_region_id   = t701.c701_region
  AND t101.c101_user_id       = t708.c101_user_id
  AND t708.c901_region_id     = t901_region.c901_code_id (+)
  AND c708_delete_fl         IS NULL
  AND t708.c901_user_role_type=8000
  ) dist_ad
WHERE t701.c701_distributor_id = dist_vp.distributor_id (+) 
  AND t701.c701_distributor_id = dist_ad.distributor_id (+)
);