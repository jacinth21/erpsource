/**********************************************************************
Purpose: Materialized view with Loaner Tags expected usage data in field for the Score Card Window period
Author: Yoga
PC: 342,2322,2941
Location: Datawarehouse\Database\DWH\Views\DW\Power_BI\Operations\FAM\pbi_fam_distributor_loaner_exclusion_dim.vw
Code Lookup: 
**********************************************************************/

DROP MATERIALIZED VIEW pbi_fam_distributor_loaner_exclusion_dim;
CREATE MATERIALIZED VIEW pbi_fam_distributor_loaner_exclusion_dim REFRESH COMPLETE START WITH
  (
    SYSDATE
  )
  NEXT (SYSDATE+60/1440)
WITH ROWID AS
(SELECT critical_system_id,
    critical_system_type_id,
                distributor_id,
    set_id,
                placement_date,
    CASE
      WHEN trunc(adjusted_placement_date)>trunc(start_date)
      THEN adjusted_placement_date
      ELSE nvl(start_date,adjusted_placement_date)
    END AS adjusted_placement_date,
    return_date,
    adjusted_return_date,
    consignment_id,
    loaner_id,
    etch_id,
    status_fl,
    status,
    request_by_id,
    requestor_name,
    rep_id,
    rep_name,
    ass_rep_id,
    ass_rep_name,
   -- PC-3928: Fedex waive days exclude
  	product_request_detail_id,
	product_request_id,
	ln_tagid,
	fedex_waive_days
  FROM pbi_fam_distributor_loaner_dim loaners
  WHERE NOT (status_fl = 60 AND return_date is NULL)
);

