/**********************************************************************
Purpose: Materialized view with Loaner data in field. Extracted Loaner Transaction data with adjusted placement date and return date
Author: Yoga
PC: 342,2322,2941
Location: Datawarehouse\Database\DWH\Views\DW\Power_BI\Operations\FAM\pbi_fam_distributor_loaner_dim.vw
Code Lookup: 4127 - Product Loaner, 20100 - Full Set, 70106/70103/70105 -- ICT/ICA/ICS, 1000 - GMNA, 60-In active
**********************************************************************/

DROP MATERIALIZED VIEW pbi_fam_distributor_loaner_dim;
CREATE MATERIALIZED VIEW pbi_fam_distributor_loaner_dim REFRESH COMPLETE START WITH
  (
    SYSDATE
  )
  NEXT (SYSDATE+60/1440)
WITH ROWID AS
(SELECT dt.st_dt  start_date,t207.c207_set_sales_system_id critical_system_id,
  critical_systems.critical_system_name ,
  critical_systems.critical_system_type_id,
  t701.c701_distributor_id distributor_id,
  t701.c701_distributor_name distributor_name,
  t504.c207_set_id set_id,
  t207.c207_set_nm set_name,
  trunc(t504a.c504a_loaner_dt) placement_date,
  CASE
    WHEN trunc(t504a.c504a_loaner_dt) >= trunc(add_months(SYSDATE, -3),'MONTH')
    THEN trunc(t504a.c504a_loaner_dt)
    WHEN trunc(t504a.c504a_loaner_dt) <= trunc(add_months(SYSDATE, -3),'MONTH')
    THEN trunc(add_months(SYSDATE,                                 -3),'MONTH')
  END AS adjusted_placement_date,
  t504a.c504a_return_dt return_date,
  CASE
    WHEN trunc(t504a.c504a_return_dt) >= last_day(add_months(SYSDATE, -1))
    THEN last_day(add_months(SYSDATE,                                 -1))
    WHEN trunc(t504a.c504a_return_dt) <= last_day(add_months(SYSDATE, -1))
    THEN trunc(t504a.c504a_return_dt)
    ELSE nvl(t504a.c504a_return_dt, last_day(add_months(SYSDATE, -1)))
  END AS adjusted_return_date,
  t504a.c504_consignment_id consignment_id,
  t504a.c504a_loaner_transaction_id loaner_id,
  t504acl.c504a_etch_id etch_id,
  t504acl.c504a_status_fl status_fl,
  decode (t504acl.c504a_status_fl, 0, '', 5, '', 7, '', 10, '', 13, '', 16, '', 20, '',21, 'Disputed', 22, 'Missing', 23, '', 25, '', 30, '', 40, '', 50, '', 55, '', 58, '', 60, '', 24, '') status,
  request.C525_REQUEST_BY_ID request_by_id,
  (t101.C101_USER_F_NAME || ' ' || t101.C101_USER_L_NAME) requestor_name,
  request.rep_id rep_id,
  request.rep_name rep_name,
  request.ass_rep_id ass_rep_id,
  request.ass_rep_name ass_rep_name,
  -- PC-3928: Fedex waive days exclude
  t504a.c526_product_request_detail_id product_request_detail_id,
  request.c525_product_request_id product_request_id,
  GET_TAG_ID(t504a.c504_consignment_id) ln_tagid,
  gm_pkg_sm_loaner_history.get_fedex_waive_days_count (t504a.c526_product_request_detail_id,
	t504a.c504a_expected_return_dt, NVL (t504a.c504a_return_dt,CURRENT_DATE), 1000) fedex_waive_days
FROM t504a_loaner_transaction t504a,
  t504_consignment t504,
  t207_set_master t207,
  t504a_consignment_loaner t504acl,
  t703_sales_rep t703,
  t101_user t101,
  t701_distributor t701,
  pbi_fam_critical_system_dim critical_systems,
  (SELECT to_date(c906_rule_value,'MM/DD/YYYY') st_dt      
    FROM t906_rules
    WHERE c906_rule_grp_id = 'SC_START_DATE') dt,
    (SELECT t525.c525_product_request_id,t526.c526_product_request_detail_id, t525.c901_request_for, t526.c207_set_id, t525.c703_sales_rep_id
      ,t525.c525_surgery_date, t525.c525_request_for_id, t525.c525_request_by_id, 
      t525.c703_sales_rep_id rep_id,t703_rep.c703_sales_rep_name rep_name,
      t525.c703_ass_rep_id ass_rep_id,t703_assoc_rep.c703_sales_rep_name ass_rep_name
          FROM t525_product_request t525
             , t526_product_request_detail t526 , t703_sales_rep t703_rep, t703_sales_rep t703_assoc_rep
          WHERE t525.c525_product_request_id = t526.c525_product_request_id
           AND t525.c901_request_for IS NOT NULL
           AND t525.c703_sales_rep_id = t703_rep.c703_sales_rep_id (+)
           AND t525.c703_ass_rep_id = t703_assoc_rep.c703_sales_rep_id (+)
           and t525.c901_request_for = 4127
         ) request
WHERE t504a.c526_product_request_detail_id = request.c526_product_request_detail_id(+)
and t504a.c504_consignment_id   = t504.c504_consignment_id
  AND t207.c207_set_id              = t504.c207_set_id
  AND t207.c207_set_sales_system_id = critical_systems.critical_system_id
  AND t504acl.c504_consignment_id   = t504.c504_consignment_id -- For Etch Id
  AND t504a.c703_sales_rep_id       = t703.c703_sales_rep_id
  AND t703.c701_distributor_id      = t701.c701_distributor_id
  AND (t504a.c504a_return_dt       >= trunc(add_months(SYSDATE, -3),'MONTH')
    OR t504a.c504a_return_dt         IS NULL)
  AND t504a.c504a_void_fl            IS NULL
  AND t504acl.c504a_void_fl          IS NULL
  AND t207.c207_void_fl              IS NULL
  AND t207.c901_cons_rpt_id           = 20100
  AND t504.c1900_company_id           =1000
  AND c504_type                       = 4127
  AND t701.c1900_company_id           =1000
  AND t701.c901_distributor_type NOT IN (70106,70103,70105) -- ICT/ICA/ICS
  AND request.C525_REQUEST_BY_ID=t101.C101_USER_ID(+)
); --5418
