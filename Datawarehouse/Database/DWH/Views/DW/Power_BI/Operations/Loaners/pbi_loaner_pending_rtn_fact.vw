/*
File name: pbi_loaner_pending_rtn_fact.vw
Created By: RVeerasamy
PC # :- PC-1013
Description: It contains loaner transation data's, it will fetch only the pending returns.
Location: "C:\PMT_Bit\Datawarehouse\Database\DWH\Views\DW\Power_BI\Operations\Loaners\pbi_loaner_pending_rtn_fact.vw"
*/

DROP VIEW pbi_loaner_pending_rtn_fact;
CREATE OR REPLACE VIEW pbi_loaner_pending_rtn_fact
AS
SELECT
    request.c207_set_id              set_id,
    t504a.c504_consignment_id        consignment_id,
    t504a.c504a_loaner_transaction_id loaner_transaction_id,
    t504a.c504a_loaner_dt            loaned_date,
    t504a.c504a_expected_return_dt   expected_return_date,
    t901_consigned_to.codenm         consigned_to,
    t504a.c504a_consigned_to_id      dist_id,
    t504a.c703_sales_rep_id   rep_id,
    t504a.c1900_company_id           company_id,
    t504a.c5040_plant_id              plant_id
FROM
    t504a_loaner_transaction      t504a,
    (
        SELECT
            t526.c526_product_request_detail_id,
            t525.c901_request_for,
            t526.c207_set_id
        FROM
            t525_product_request          t525,
            t526_product_request_detail   t526
        WHERE
            t525.c525_product_request_id = t526.c525_product_request_id
            AND t525.c901_request_for IS NOT NULL
            AND t525.c901_request_for = 4127
            AND t525.c525_void_fl IS NULL
            AND t526.c207_set_id is NOT NULL
    ) request,
    (
        SELECT
            c901_code_id   codeid,
            c901_code_nm   codenm
        FROM
            t901_code_lookup
        WHERE
            c901_code_grp = 'LONTO'
            AND c901_active_fl = 1
            AND c901_void_fl IS NULL
    ) t901_consigned_to
WHERE
    t504a.c526_product_request_detail_id = request.c526_product_request_detail_id (+)
    AND t504a.c901_consigned_to = t901_consigned_to.codeid
    AND t504a.c504a_return_dt IS NULL
    AND t504a.c504a_void_fl IS NULL
    AND t504a.c504_consignment_id IS NOT NULL
    AND request.c901_request_for IS NOT NULL;
    /