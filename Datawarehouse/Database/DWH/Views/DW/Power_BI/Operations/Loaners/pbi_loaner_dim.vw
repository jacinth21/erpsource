/*
File name: pbi_loaner_dim.vw
Created By: RVeerasamy
PC # :- PC-1013
Description: It contains loaner informtions, it will fetch only the product loaners data's.
Location: "C:\PMT_Bit\Datawarehouse\Database\DWH\Views\DW\Power_BI\Operations\Loaners\pbi_loaner_dim.vw"
*/

DROP MATERIALIZED VIEW pbi_loaner_dim;
CREATE MATERIALIZED VIEW pbi_loaner_dim 
NOLOGGING
REFRESH COMPLETE START WITH (SYSDATE) NEXT  (SYSDATE+60/1440) WITH ROWID
AS
SELECT 
    t504.c504_consignment_id         consignment_id,
    t504.c207_set_id                 set_id,
    t504a.c504a_etch_id              etch_id,
    t5010.c5010_tag_id               tag_id,
    t901_tag_status.codenm           tag_status,
    t901_status.codenm                status,
    t504.c1900_company_id           company_id,
    t504.c5040_plant_id             plant_id
FROM
    t504_consignment           t504,
    t504a_consignment_loaner   t504a,
    t5010_tag                  t5010,
    (SELECT
            c901_code_id   codeid,
            c901_code_nm   codenm,
            c902_code_nm_alt codenmalt
        FROM
            t901_code_lookup
        WHERE
            c901_code_grp = 'LOANS'
            AND c901_void_fl IS NULL)           t901_status,
     (SELECT      t5010.c5010_last_updated_trans_id c504_consignment_id,
            MAX(t5010.c5010_tag_id) tag_id
        FROM
            t5010_tag t5010, t504a_consignment_loaner t504a
        WHERE
            t5010.c5010_last_updated_trans_id = t504a.c504_consignment_id
         AND    t5010.c5010_void_fl IS NULL
         AND t504a.c504a_void_fl is null
            AND t5010.c5010_last_updated_trans_id IS NOT NULL
        GROUP BY
            t5010.c5010_last_updated_trans_id) tag,
        (SELECT
            c901_code_id   codeid,
            c901_code_nm   codenm
        FROM
            t901_code_lookup
        WHERE
            c901_code_grp = 'TAGST'
            AND c901_active_fl = 1
            AND c901_void_fl IS NULL
        ) t901_tag_status
WHERE
    t504.c504_consignment_id = t504a.c504_consignment_id
    AND t504a.c504a_status_fl = t901_status.codenmalt
    AND t504a.c504_consignment_id = tag.c504_consignment_id (+)
    AND tag.tag_id = t5010.c5010_tag_id
    AND t5010.c901_status = t901_tag_status.codeid
    AND t504.c504_type = '4127' --product loaner
    AND t504.c504_void_fl is NULL
    AND t504a.c504a_void_fl IS NULL;