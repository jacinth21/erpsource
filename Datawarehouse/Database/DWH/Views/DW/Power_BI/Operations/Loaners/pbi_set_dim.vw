/*
File name: pbi_set_dim.vw
Created By: RVeerasamy
PC # :- PC-1013
Description: It contains set informtions, it will fetch only the set report group data's.
Location: "C:\PMT_Bit\Datawarehouse\Database\DWH\Views\DW\Power_BI\Operations\Loaners\pbi_set_dim.vw"
*/

DROP MATERIALIZED VIEW pbi_set_dim;
CREATE MATERIALIZED VIEW pbi_set_dim 
NOLOGGING
REFRESH COMPLETE START WITH (SYSDATE) NEXT  (SYSDATE+60/1440) WITH ROWID
AS
(SELECT
    t207.c207_set_id                 set_id,
    t207.c207_set_nm                 set_name,
    t207.c207_set_sales_system_id sales_system_id, 
    t901_set_category.c901_code_nm   set_category,
    t901_set_type.c901_code_nm       set_type,
    t901_staus.c901_code_nm          set_status
FROM
    t207_set_master t207,
    (
        SELECT
            c901_code_id,
            c901_code_nm
        FROM
            t901_code_lookup
        WHERE
            c901_code_grp = 'SETCA'
            AND c901_void_fl IS NULL
    ) t901_set_category,
    (
        SELECT
            c901_code_id,
            c901_code_nm
        FROM
            t901_code_lookup
        WHERE
            c901_code_grp = 'SETTY'
            AND c901_void_fl IS NULL
    ) t901_set_type,
    (
        SELECT
            c901_code_id,
            c901_code_nm
        FROM
            t901_code_lookup
        WHERE
            c901_code_grp = 'PSTAT'
            AND c901_void_fl IS NULL
    ) t901_staus
WHERE t207.c207_category = t901_set_category.c901_code_id (+)
    AND t207.c207_type = t901_set_type.c901_code_id (+)
    AND t207.c901_status_id = t901_staus.c901_code_id (+)
    AND t207.c207_void_fl IS NULL
    AND t207.c901_set_grp_type = '1601'--Set Report Group
    );
