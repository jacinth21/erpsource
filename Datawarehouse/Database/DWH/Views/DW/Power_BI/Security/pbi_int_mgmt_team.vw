/*
File name: pbi_int_mgmt_team.vw
Created By: RVeerasamy
PMT # :- PMT-46422
Description: Fetch all PBI_INT_MGMT_TEAM Security Group users details And Individual company access users details. Using for Pbi report company level user access.            
*/

DROP VIEW pbi_int_mgmt_team;
CREATE OR REPLACE VIEW pbi_int_mgmt_team 
AS
SELECT t101p.c101_party_id party_id,
       c101_first_nm ||' '|| c101_last_nm  user_name,
       Upper(t101u.c101_email_id) email_id,
       t1019.c1900_company_id company_id
FROm t101_party t101p,
     t1019_party_company_map t1019,
     t101_user t101u
WHERE t101p.c101_party_id=t1019.c101_party_id
AND t101u.c101_party_id = t101p.c101_party_id
AND t101p.c101_void_fl IS NULL
AND t1019.c1019_void_fl IS NULL
AND t101p.c101_party_id IN 
(SELECT  c101_mapped_party_id FROm t1501_group_mapping t1501
WHERE t1501.c1500_group_id IN
(select c906_rule_value from t906_rules
where c906_rule_id = 'ACCESSID'
AND c906_rule_grp_id = 'PBI_INT_MGMT_ACCESS' 
AND c906_void_fl IS NULL)
AND t1501.c1501_void_fl IS NULL)
UNION ALL
---Individual company access from t1021_party_sales_company_map table
SELECT t101p.c101_party_id party_id, 
       c101_first_nm || ' ' || c101_last_nm user_name, 
       Upper(t101u.c101_email_id) email_id,
       t1021.c1900_company_id company_id
FROM t101_party t101p, t1021_party_sales_company_map t1021, t101_user t101u
WHERE t101p.c101_party_id = t1021.c101_party_id
AND t101u.c101_party_id = t101p.c101_party_id
AND t101p.c101_void_fl IS NULL
AND t1021.c1021_void_fl IS NULL;
/