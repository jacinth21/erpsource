/*
File name: pbi_ad_plus_team.vw
Created By: RVeerasamy
PMT # :- PMT-46422
Description: Fetch all the AD plus user's details and fetch Active user's only. Using for Pbi report AD plus user access.
*/

DROP VIEW pbi_ad_plus_team;
CREATE OR REPLACE VIEW pbi_ad_plus_team
AS
SELECT t101p.c101_party_id party_id, c101_first_nm || ' ' || c101_last_nm user_name, Upper(t101u.c101_email_id) C101_EMAIL_ID
  ,t708.c901_region_id region_id
 FROM t101_party t101p, t101_user t101u, t708_region_asd_mapping t708
 WHERE t101u.c101_party_id = t101p.c101_party_id
 AND t101u.c101_user_id = t708.c101_user_id
 AND t708.c901_user_role_type = 8002   --AD+
 AND t101p.c101_void_fl IS NULL
 AND t708.c708_delete_fl IS NULL
 AND t101u.c901_user_status = 311;
/

