-- @"C:\Datawarehouse\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Scheduled_Reports\Month_End_Reports\GMJP\VDW_GMJP_Acc_vs_Ops_Quara_Rpt.vw";

CREATE OR REPLACE VIEW VDW_GMJP_Acc_vs_Ops_Quara_Rpt
AS
     SELECT pnum, pdesc, cogs_value
      , inv_value
       FROM
        (
             SELECT t205.c205_part_number_id pnum, t205.c205_part_num_desc pdesc, NVL (cogs_value, 0) cogs_value
              , NVL (Qurantaine.quar_qty, 0) inv_value
               FROM t205_part_number t205, T2023_PART_COMPANY_MAPPING t2023, (
                     SELECT t205c.C205_PART_NUMBER_ID C205_PART_NUMBER_ID, t205c.C205_AVAILABLE_QTY quar_qty
                       FROM t205c_part_qty t205c
                      WHERE C901_TRANSACTION_TYPE = 90813 -- Quarantine Inventory
                        AND t205c.c5040_plant_id  = 3017 
                )
                Qurantaine, (
                     SELECT c205_part_number_id, SUM (c820_qty_on_hand) cogs_value
                       FROM t820_costing
                      WHERE c901_costing_type = 4902 -- Quarantine Inventory
                        AND c901_status      <> 4802 -- Closed
                        AND c1900_company_id  = 1026 
                        AND c820_delete_fl                   = 'N'
                   GROUP BY c205_part_number_id
                )
                icogg
              WHERE t205. c205_part_number_id = icogg.c205_part_number_id (+)
                AND t205.c205_part_number_id  = Qurantaine.c205_part_number_id (+)
                AND t2023.c205_part_number_id = t205.c205_part_number_id
                AND t2023.c2023_void_fl      IS NULL
                AND t2023.C1900_COMPANY_ID    = 1026 
        )
      WHERE (cogs_value <> 0
        OR inv_value    <> 0)
/