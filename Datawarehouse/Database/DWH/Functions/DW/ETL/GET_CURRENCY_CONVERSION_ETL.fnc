create or replace FUNCTION GET_CURRENCY_CONVERSION_ETL(
      p_rep_id t703_sales_rep.c703_sales_rep_id%TYPE ,
      p_amount NUMBER ,
      p_convtp t907_currency_conv.c901_conversion_type%TYPE DEFAULT 101103 ,
      P_date date)
    RETURN NUMBER
  IS
    v_curr_rate    NUMBER;
    v_currency     NUMBER;
    v_company      NUMBER;
    v_comptxn_curr NUMBER;
    v_rept_curr    NUMBER;
  BEGIN
    BEGIN
      SELECT C1900_COMPANY_ID
      INTO v_company
      FROM T703_SALES_REP
      WHERE C703_SALES_REP_ID=p_rep_id;
      SELECT c901_txn_currency,
        c901_reporting_currency
      INTO v_comptxn_curr,
        v_rept_curr
      FROM t1900_company
      WHERE c1900_company_id = v_company;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_comptxn_curr := '1';
      v_rept_curr    := '1';
    END;

    SELECT GET_CURRENCY_CONVERSION (v_comptxn_curr,v_rept_curr,to_date(P_date),1,p_convtp)
    INTO v_curr_rate
    FROM DUAL;
    RETURN v_curr_rate * p_amount;

  END;
 /