/*This procedure is calling by ETL Job, If there is any changes then please coordinate with DWH team during design phase */
CREATE OR REPLACE PROCEDURE gm_cm_sav_partrepl_qty(
        p_part_number_id   IN t205c_part_qty.c205_part_number_id%TYPE ,
        p_update_qty       IN t205c_part_qty.c205_available_qty%TYPE ,
        p_trans_id         IN t205c_part_qty.c205_last_update_trans_id%TYPE ,
        p_updated_by       IN t205c_part_qty.c205_last_updated_by%TYPE ,
        p_transaction_type IN t205c_part_qty.c901_transaction_type%TYPE ,
        p_action           IN t205c_part_qty.c901_action%TYPE ,
        p_type             IN t205c_part_qty.c901_type%TYPE ,
        p_company_id       IN T1900_COMPANY.C1900_COMPANY_ID%TYPE )
AS
    /****************************************************************************************************************************************
    * This procedure is to update part qty information in the new t205c_part_qty table
    *
    *****************************************************************************************************************************************/
BEGIN
    --
    UPDATE te205c_repl_part_qty
    SET c205_available_qty        = NVL(c205_available_qty,0) + p_update_qty ,
        c205_last_update_trans_id = p_trans_id ,
        c205_last_updated_by      = p_updated_by ,
        c901_action               = p_action ,
        c901_type                 = p_type
    WHERE c205_part_number_id     = p_part_number_id
    AND c901_transaction_type     = p_transaction_type
    AND c1900_company_id          = p_company_id ;
    IF (SQL%ROWCOUNT              = 0) THEN
        INSERT
        INTO te205c_repl_part_qty
            (
                c205c_repl_part_qty_id,
                c205_part_number_id,
                c205_available_qty,
                c205_last_update_trans_id ,
                c205_last_updated_by,
                c205_last_updated_date,
                c901_transaction_type,
                c901_action,
                c901_type,
                c1900_company_id
            )
            VALUES
            (
                se205c_repl_part_qty.NEXTVAL,
                p_part_number_id,
                p_update_qty,
                p_trans_id ,
                p_updated_by,
                SYSDATE,
                p_transaction_type,
                p_action,
                p_type,
                p_company_id
            );
    END IF;
END gm_cm_sav_partrepl_qty;
/