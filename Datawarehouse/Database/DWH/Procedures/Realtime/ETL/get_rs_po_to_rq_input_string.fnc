/*This procedure is calling by ETL Job, If there is any changes then please coordinate with DWH team during design phase */
create or replace FUNCTION get_rs_po_to_rq_input_string(
   p_po_id IN t401_purchase_order.c401_purchase_ord_id%type )
   RETURN VARCHAR2
IS
   string_info VARCHAR2(4000);

   cursor poitems is
     SELECT ce402_input_string_concat string_concat,
        c401_purchase_ord_id id
    FROM te402_po_to_rq_insert_concat
    WHERE c401_purchase_ord_id = p_po_id;
BEGIN

   string_info := NULL;

   FOR string_concat in poitems
   LOOP
      string_info := string_info ||'|'|| string_concat.string_concat;
   END LOOP;

   RETURN substr(string_info,2)||'|';

END;
/