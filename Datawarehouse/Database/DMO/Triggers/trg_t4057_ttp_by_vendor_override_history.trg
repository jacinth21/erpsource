
--@"C:\Datawarehouse\Database\DMO\Triggers\trg_t4057_ttp_by_vendor_override_history.trg";

CREATE OR REPLACE TRIGGER trg_t4057_ttp_by_vendor_override_history 
	BEFORE UPDATE OF c4057_override_split_per
	ON t4057_ttp_vendor_capacity_summary_dtls
	FOR EACH ROW
DECLARE
	
BEGIN
--
IF (NVL(:OLD.c4057_override_split_per,-999) <> NVL(:NEW.c4057_override_split_per,-999)) THEN
	IF (:OLD.c4057_override_split_hist_fl IS NULL)
	 THEN
	 	-- primary key, value, audit id (t940), user id, date
	 	
		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.C4057_TTP_VENDOR_CAPA_DTLS_ID, :OLD.c4057_override_split_per, 315,
        :OLD.c4057_last_updated_by, :OLD.c4057_last_updated_date) ;
        
        -- to update the historical flag
        
		:NEW.c4057_override_split_hist_fl := 'Y';
		--
	END IF;
	--
		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (:OLD.C4057_TTP_VENDOR_CAPA_DTLS_ID, :NEW.c4057_override_split_per, 315,
        :NEW.c4057_last_updated_by, :NEW.c4057_last_updated_date) ;
	--								 
END IF;									 
--
END trg_t4057_ttp_by_vendor_override_history;
/
