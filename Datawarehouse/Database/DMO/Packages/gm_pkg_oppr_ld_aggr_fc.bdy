--@"c:\database\packages\operations\purchasing\gm_pkg_oppr_ld_aggr_fc.bdy";

-- exec gm_pkg_oppr_ld_aggr_fc.gm_ld_forecast

CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ld_aggr_fc
IS
--
--
	/**********************************************************
 	* Purpose: This procedure is to aggregate the forecast
 	* 		   and load the details in temp tables so that
 	*          reports can make use of it, instead of joining
 	*          with demand sheet tables directly
 	* Created By VPrasath
 	***********************************************************/
	PROCEDURE gm_ld_forecast
	AS
	v_load_date	   DATE;
	
	BEGIN
		
		-- PMT-38602 , Calling below procedure to update zero qty forecast value as previous month qty
		--gm_pkg_oppr_ld_mend_inventory.gm_op_fch_load_date(v_load_date);
		
		gm_ld_part_forecast_reset();
		
		gm_ld_item_forecast();
		
		gm_ld_set_forecast();
		
		 UPDATE t4052_ttp_detail t4052a
		   SET t4052a.c4052_fc_sum_fl = 'Y'
		 WHERE t4052a.c4052_ttp_detail_id  = (SELECT t4052.c4052_ttp_detail_id
		    								   FROM t4052_ttp_detail t4052 
		   									  WHERE t4052.c901_status = 26240574 -- only LOCKED ttp's
		     									AND t4052.c4052_fc_sum_fl IS NULL -- Take only the sheets that are not used by the job already
		     									AND t4052.c4052_void_fl IS NULL
		     									AND t4052.C4052_TTP_LINK_DATE = TRUNC(SYSDATE,'mm')
		     									AND t4052a.c4052_ttp_detail_id = t4052.c4052_ttp_detail_id);
		
	END gm_ld_forecast;
--
--
	/**********************************************************
 	* Purpose: This procedure is to aggregate the forecast
 	* 		   and load the item details in a table so that trend
 	*          report can make use of it, instead of joining
 	*          with demand sheet tables directly
 	* Created By VPrasath
 	***********************************************************/
	PROCEDURE gm_ld_item_forecast
	AS	
		v_cur_mon DATE;
		v_inventory_id      t250_inventory_lock.c250_inventory_lock_id%TYPE;

		CURSOR v_fc_cur IS   
		  SELECT t4042.c205_part_number_id PNUM
		       , t4042.c4042_period PERIOD
		       , t4040.c901_demand_type DTYPE
		       , SUM(t4042.c4042_qty) QTY
		       , t4040.c901_level_value LVL_VAL
		    FROM t4052_ttp_detail t4052 
		       , t4040_demand_sheet t4040
		       , t4042_demand_sheet_detail t4042
		   WHERE t4052.c4052_ttp_detail_id = t4040.c4052_ttp_detail_id
		     AND t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
		     AND t4052.c901_status = 26240574 -- only LOCKED ttp's
		     AND t4052.c4052_fc_sum_fl IS NULL -- Take only the sheets that are not used by the job already
		     AND t4052.c4052_void_fl IS NULL
		     AND t4040.c4040_void_fl IS NULL
		     AND t4040.c901_level_id IN (102581, 102580) -- Only COMPANY,Division Level Sheets
		     AND t4042.c901_type = 50563 -- FORECAST
		     AND c901_ref_type IN (102660,102661,102662) -- Group Net Total,Grand Total,Item Part Net Total
		     AND t4040.c250_inventory_lock_id = v_inventory_id 
		     AND c4042_qty != 0
		     AND t4042.c205_part_number_id IS NOT NULL
		GROUP BY t4042.c205_part_number_id
		       , t4042.c4042_period
		       , t4040.c901_demand_type, t4040.c901_level_value;
		       
		BEGIN
		-- Below code to get current inventory value
        -- Should always pick US (Company level Inventory) 
        -- Thats the reason global inventory hardcoded to 1 
		SELECT MAX (t250.c250_inventory_lock_id)
		  INTO v_inventory_id
		  FROM t250_inventory_lock t250
		 WHERE t250.c901_lock_type = 20430
		 AND c250_void_fl IS NULL
         AND t250.c4016_global_inventory_map_id  = 1;
           
		FOR v_index IN v_fc_cur  LOOP
		
		 UPDATE t4060_part_forecast_qty
		    SET c4060_qty = v_index.qty
		      , c4060_last_updated_by = 30301
		      , c4060_last_updated_date = sysdate
		  WHERE c205_part_number_id = v_index.pnum
		    AND c901_forecast_type = v_index.dtype
		    AND c4060_period = v_index.period
		    AND c901_level_value =v_index.LVL_VAL ;
		
		IF SQL%ROWCOUNT = 0  
		THEN
		
		INSERT INTO t4060_part_forecast_qty 
		            (c4060_part_forecast_id , c205_part_number_id
		          , c901_forecast_type, c4060_qty, c4060_period
		          , c4060_last_updated_by, c4060_last_updated_date,c901_level_value)
		     VALUES(s4060_part_forecast_qty.NEXTVAL, v_index.pnum
		     	  , v_index.dtype, v_index.qty, v_index.period
		          , 30301, SYSDATE,v_index.LVL_VAL);
		       
		END IF;
		
		END LOOP;
		     									
END gm_ld_item_forecast;
--
--
	/**********************************************************
 	* Purpose: This procedure is to aggregate the forecast
 	* 		   and load the set details in a table so that set
 	*          overview report can make use of it, instead of 
 	*          joining with demand sheet tables directly
 	* Created By VPrasath
 	***********************************************************/
	PROCEDURE gm_ld_set_forecast
	AS	
		v_cur_mon DATE;
		v_inventory_id      t250_inventory_lock.c250_inventory_lock_id%TYPE;

		CURSOR v_fc_cur IS   
		 SELECT * FROM (
			SELECT t4042.c4042_ref_id set_id
			       , t4042.c4042_period period
			       , t4040.c901_demand_type dtype
	  	       	   , SUM(t4042.c4042_qty) qty
			    FROM t4052_ttp_detail t4052 
			       , t4040_demand_sheet t4040
			       , t4042_demand_sheet_detail t4042
			   WHERE t4052.c4052_ttp_detail_id = t4040.c4052_ttp_detail_id
			     AND t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
			     AND t4052.c901_status = 26240574 -- only LOCKED ttp's
			     AND t4052.c4052_fc_sum_fl IS NULL -- Take only the sheets that are not used by the job already
			     AND t4052.c4052_void_fl IS NULL
			     AND t4040.c4040_void_fl IS NULL
			     AND t4040.c901_level_id = 102580 -- Only COMPANY Level Sheets
			     AND t4042.c901_type = 50563 -- FORECAST
			     AND t4040.c250_inventory_lock_id = v_inventory_id 
			     AND c4042_qty != 0
	         	 AND t4042.c901_ref_type = 102663 -- Set
			     AND t4042.c205_part_number_id IS NULL
			GROUP BY  t4042.c4042_ref_id 
			       , t4042.c4042_period
			       , t4040.c901_demand_type
			 UNION ALL		
				  SELECT t4042.c4042_ref_id set_id
				       , t4042.c4042_period period
				       , t4042.c901_type dtype
		  	       	   , SUM(t4042.c4042_qty) qty
				    FROM t4052_ttp_detail t4052 
				       , t4040_demand_sheet t4040
				       , t4042_demand_sheet_detail t4042
				   WHERE t4052.c4052_ttp_detail_id = t4040.c4052_ttp_detail_id
				     AND t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
				     AND t4052.c901_status = 26240574 -- only LOCKED ttp's
				     AND t4052.c4052_fc_sum_fl IS NULL -- Take only the sheets that are not used by the job already
				     AND t4052.c4052_void_fl IS NULL
				     AND t4040.c4040_void_fl IS NULL
				     AND t4040.c901_level_id = 102580 -- Only COMPANY Level Sheets
				     AND t4042.c901_type=4000113 -- FORECAST
				     AND t4040.c250_inventory_lock_id = v_inventory_id
				     AND c4042_qty != 0
		         	 AND t4042.c901_ref_type = 102663 -- Set
				     AND t4042.c205_part_number_id IS NULL		            
				GROUP BY  t4042.c4042_ref_id 
				       , t4042.c4042_period
				       , t4040.c901_demand_type, t4042.c901_type)
		ORDER BY period,set_id;
		       
		BEGIN
			
		-- Below code to get current inventory value
        -- Should always pick US (Company level Inventory) 
        -- Thats the reason global inventory hardcoded to 1 
		SELECT MAX (t250.c250_inventory_lock_id)
		  INTO v_inventory_id
		  FROM t250_inventory_lock t250
		 WHERE t250.c901_lock_type = 20430
		 AND c250_void_fl IS NULL
           AND t250.c4016_global_inventory_map_id  = 1;
		
		FOR v_index IN v_fc_cur  LOOP
		
		 UPDATE t4061_set_forecast_qty 
		    SET c4061_qty = v_index.qty
		      , c4061_last_updated_by = 30301
		      , c4061_last_updated_date = sysdate
		  WHERE c207_set_id = v_index.set_id
		    AND c901_forecast_type = v_index.dtype
		    AND c4061_period = v_index.period;
		
		IF SQL%ROWCOUNT = 0 
		THEN
		
		INSERT INTO t4061_set_forecast_qty 
		            (c4061_set_forecast_id , c207_set_id
		          , c901_forecast_type, c4061_qty, c4061_period
		          , c4061_last_updated_by, c4061_last_updated_date)
		     VALUES(s4061_set_forecast_qty.NEXTVAL, v_index.set_id
		     	  , v_index.dtype, v_index.qty, v_index.period
		          , 30301, SYSDATE);
		       
		END IF;
		
		END LOOP;
		
		
END gm_ld_set_forecast;


	/**********************************************************
 	* Purpose: This procedure is to update zero forecast
 	* 		   qty as previous month forecast qty	
 	* Created By Mahavishnu
 	***********************************************************/
PROCEDURE gm_ld_part_forecast_reset
	AS	
	
	type pnum_array IS     TABLE OF  T205_PART_NUMBER.c205_part_number_id%TYPE;
	type period_array IS    TABLE OF  t4042_demand_sheet_detail.c4042_period%TYPE ;
	type dmd_type_array IS    TABLE OF  t4040_demand_sheet.c901_demand_type%TYPE ;
	type lvl_value_array IS    TABLE OF  t4040_demand_sheet.c901_level_value%TYPE ;

	arr_pnum 		pnum_array;
	arr_period 		period_array;
	arr_dmd_type 	dmd_type_array;
	arr_lvl_value	lvl_value_array;
	v_load_date	   DATE;
	
	v_cur_mon DATE;
	v_inventory_id      t250_inventory_lock.c250_inventory_lock_id%TYPE;

		CURSOR cur_forecast_by_month IS   
		  SELECT t4042.c205_part_number_id PNUM
		       , t4042.c4042_period PERIOD
		       , t4040.c901_demand_type DTYPE
		       , t4040.c901_level_value LVL_VAL
		    FROM t4052_ttp_detail t4052 
		       , t4040_demand_sheet t4040
		       , t4042_demand_sheet_detail t4042
		   WHERE t4052.c4052_ttp_detail_id = t4040.c4052_ttp_detail_id
		     AND t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
		     AND t4052.c901_status = 26240574 -- only LOCKED ttp's
		     AND t4052.c4052_fc_sum_fl IS NULL -- Take only the sheets that are not used by the job already
		     AND t4052.c4052_void_fl IS NULL
		     AND t4040.c4040_void_fl IS NULL
		     AND t4040.c901_level_id IN (102581, 102580) -- Only COMPANY,Division Level Sheets
		     AND t4042.c901_type = 50563 -- FORECAST
		     AND c901_ref_type IN (102660,102661,102662) -- Group Net Total,Grand Total,Item Part Net Total
		     AND t4040.c250_inventory_lock_id = v_inventory_id 
		     AND c4042_qty = 0
		     AND t4042.c205_part_number_id IS NOT NULL
		GROUP BY t4042.c205_part_number_id
		       , t4042.c4042_period
		       , t4040.c901_demand_type, t4040.c901_level_value;
		       


	BEGIN
		
	
		-- PMT-38602 , Calling below procedure to update zero qty forecast value as previous month qty
		--gm_pkg_oppr_ld_mend_inventory.gm_op_fch_load_date(v_load_date);
		
	SELECT MAX (t250.c250_inventory_lock_id)
		  INTO v_inventory_id
		  FROM t250_inventory_lock t250
		 WHERE t250.c901_lock_type = 20430
		   AND C250_VOID_FL IS NULL
		   AND t250.C250_LOCK_DATE=TRUNC(SYSDATE,'mm')
           AND t250.c4016_global_inventory_map_id  = 1;
		   
		OPEN cur_forecast_by_month;
		LOOP
		FETCH cur_forecast_by_month BULK COLLECT INTO arr_pnum,arr_period,arr_dmd_type,arr_lvl_value  LIMIT 500;

		FORALL i in 1.. arr_pnum.COUNT
		
			 UPDATE t4060_part_forecast_qty
		    SET c4060_qty = 0
		      , c4060_last_updated_by = 30301
		      , c4060_last_updated_date = CURRENT_DATE
		  WHERE c205_part_number_id = arr_pnum(i)
		    AND c901_forecast_type = arr_dmd_type(i)
		    AND c4060_period = arr_period(i)
		    AND c901_level_value = arr_lvl_value(i) ;
	
		EXIT WHEN cur_forecast_by_month%NOTFOUND;
	    END LOOP;

	END gm_ld_part_forecast_reset;
END gm_pkg_oppr_ld_aggr_fc;
/