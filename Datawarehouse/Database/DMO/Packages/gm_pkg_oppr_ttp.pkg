/* Formatted on 2009/11/18 10:08 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\operations\Purchasing\gm_pkg_oppr_ttp.pkg"

CREATE OR REPLACE PACKAGE gm_pkg_oppr_ttp
IS
--
	PROCEDURE gm_fc_fch_ttp_dstemplate_map (
		p_ttpid 		  IN	   t4050_ttp.c4050_ttp_id%TYPE
	  , p_outunselected   OUT	   TYPES.cursor_type
	  , p_outselected	  OUT	   TYPES.cursor_type
	);

/*******************************************************
   * Description : Procedure to save  / update the demand sheets associated to a TTP
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_sav_ttpdsmap (
		p_ttpid 	 IN 	  t4050_ttp.c4050_ttp_id%TYPE
	  , p_ttpname	 IN 	  t4050_ttp.c4050_ttp_nm%TYPE
	  , p_primuser	 IN 	  t4050_ttp.c4050_primary_user%TYPE
	  , p_userid	 IN 	  t4050_ttp.c4050_created_by%TYPE
	  , p_inputstr	 IN 	  VARCHAR2
	  , p_category	 IN 	  t4050_ttp.c901_ttp_category%TYPE--PMT-49826 -Save category id in t4050table
	  , p_outttpid	 OUT	  t4050_ttp.c4050_ttp_id%TYPE
	);

  /*******************************************************
   * Description : Procedure to fetch ttp report
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_ttp_report (
		p_report   OUT	 TYPES.cursor_type
	);

--
	/*******************************************************
   * Description : Procedure to fetch monthly sheet report
   * Author 	 : D James
   *******************************************************/
--
	PROCEDURE gm_fc_fch_monthsheet_report (
		p_sheetnmregex	 IN 	  VARCHAR2
	  , p_status		 IN 	  t4040_demand_sheet.c901_status%TYPE
	  , p_sheettype 	 IN 	  t4020_demand_master.c901_demand_type%TYPE
	  , p_month 		 IN 	  VARCHAR2
	  , p_year			 IN 	  VARCHAR2
	  , p_ttpid 		 IN 	  t4050_ttp.c4050_ttp_id%TYPE
	  , p_primary_user	 IN 	  t4040_demand_sheet.c4040_primary_user%TYPE
	  , p_report		 OUT	  TYPES.cursor_type
	);

--
	/*******************************************************
   * Description : Procedure to fetch demand sheet for that TTP
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_ttp_dsmonthly (
		p_ttp_id 	 IN		 t4050_ttp.c4050_ttp_id%TYPE
	  , p_monyear	 IN		 VARCHAR2
	  , p_level_id	 IN		 t901_code_lookup.c901_code_id%TYPE
	  , p_company_id IN	     t901_code_lookup.c901_code_id%TYPE
	  , p_output	OUT 	 TYPES.cursor_type
	);

--
	/*******************************************************
   * Description : Procedure to fetch all approved monthly demand sheet
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_ttp_approvedds (
		p_ttpid 	IN		 t4050_ttp.c4050_ttp_id%TYPE
	  , p_monyear	IN		 VARCHAR2
	  , p_output	OUT 	 TYPES.cursor_type
	);

--
	/*******************************************************
   * Description : Procedure to fetch monthly sheet report
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_inventoryidlist (
		p_ttp_id 	       IN	 t4050_ttp.c4050_ttp_id%TYPE
	  , p_lock_type 	   IN	 t250_inventory_lock.c901_lock_type%TYPE
	  , p_level_id	       IN	 t901_code_lookup.c901_code_id%TYPE
	  , p_company_id       IN	 t901_code_lookup.c901_code_id%TYPE
	  , p_inventoryidlist  OUT 	 TYPES.cursor_type
	);

	/*******************************************************
   * Description : Procedure to fetch TTP List
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_ttplist (
		p_output   OUT	 TYPES.cursor_type
	);
--
  /*******************************************************
   * Description : Procedure to fetch set par Details
   * Author 	 : VPrasath
   *******************************************************/
--
	PROCEDURE gm_fc_fch_setpar_details (
		p_demandsheetids IN  VARCHAR2
	  , p_out_setpar     OUT TYPES.cursor_type
	);
--	
--
  /*******************************************************
   * Description : Procedure to fetch Growth Details
   * Author 	 : VPrasath
   *******************************************************/
--
	PROCEDURE gm_fc_fch_request_details (
		p_demandsheetids IN  VARCHAR2
	  , p_out_req        OUT TYPES.cursor_type
	);
--
	/*******************************************************
   * Description : Procedure to save monthly TTP grouping
   * Author 	 : Joe P Kumar
   Algorithm:
	1. Create a new entry onto t4052_ttp_detail
	2. Update the v_ttp_detail_id created into t4040_demand_sheet for all the selected demand sheets
	3. Load part, qty, historyfl information into t4053_ttp_part_detail for the selected demand sheets from t4042_demand_sheet_detail
	4. Insert into t252_inventory_lock_ref with v_ttp_detail_id and p_inventorydate
   *******************************************************/
--
		PROCEDURE gm_fc_sav_ttp_dsmonthly (
		p_ttpid 		   IN		t4050_ttp.c4050_ttp_id%TYPE
	  , p_demandsheetids   IN		VARCHAR2
	  , p_forecastmonths   IN		VARCHAR2
	  , p_inventoryid	   IN		t252_inventory_lock_ref.c250_inventory_lock_id%TYPE
	  , p_userid		   IN		t4052_ttp_detail.c4052_created_by%TYPE
	  , p_string		   IN		VARCHAR2
	  , p_cnt			   IN		VARCHAR2
	  , p_ttp_dtl_id	   IN OUT	t4052_ttp_detail.c4052_ttp_detail_id%TYPE	   	
	  , p_ttp_type   	   OUT		t4050_ttp.c901_ttp_type%TYPE	 
	);
--	
   /*******************************************************
   * Description : Procedure to fetch TTP Level List
   * Author 	 : Elango
   *******************************************************/
   PROCEDURE gm_fc_fch_ttplevel (
		 p_company_id IN t4020_demand_master.C901_COMPANY_ID%TYPE
		,p_output   OUT	 TYPES.cursor_type
	);
	
	/*****************************************************************
	* Description : Procedure to get company id for the TTP ID
	* Author 	 : Elango
	*****************************************************************/
	FUNCTION get_ttp_company_id (
		p_ttp_id   IN   t4050_ttp.C4050_TTP_ID%TYPE
	)
		RETURN VARCHAR2;
		
END gm_pkg_oppr_ttp;
/
