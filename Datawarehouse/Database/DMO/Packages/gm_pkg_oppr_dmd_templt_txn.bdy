/* Formatted on 2009/12/08 19:47 (Formatter Plus v4.8.0) */
-- @"C:\database\Packages\operations\purchasing\gm_pkg_oppr_dmd_templt_txn.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_oppr_dmd_templt_txn
IS
    --
    /*******************************************************
    * Purpose: This Procedure is used to save demand template
    *******************************************************/
    --
PROCEDURE gm_sav_demand_template (
        p_dmd_templt_id     IN OUT t4020_demand_master.c4020_demand_master_id%TYPE,
        p_dmd_templt_nm     IN t4020_demand_master.c4020_demand_nm%TYPE,
        p_dmd_period        IN t4020_demand_master.c4020_demand_period%TYPE,
        p_forecast_period   IN t4020_demand_master.c4020_forecast_period%TYPE,
        p_setbuild_forecast IN t4020_demand_master.c4020_request_period%TYPE,
        p_company_id        IN t4020_demand_master.c901_company_id%TYPE,
        p_sheetowner_id     IN t4020_demand_master.c4020_primary_user%TYPE,
        p_inactive_fl       IN t4020_demand_master.c4020_inactive_fl%TYPE,
        p_comments		    IN t902_log.c902_comments%TYPE,
        p_user_id           IN t4020_demand_master.c4020_created_by%TYPE)
AS
	v_old_name 			t4020_demand_master.c4020_demand_nm%TYPE;
	v_old_inactive_fl	t4020_demand_master.c4020_inactive_fl%TYPE;
	v_sheet_cnt 		NUMBER;
	v_msg 			VARCHAR2(100);
BEGIN
	
	IF p_dmd_templt_id IS NOT NULL THEN
		BEGIN
			SELECT c4020_demand_nm, c4020_inactive_fl INTO v_old_name, v_old_inactive_fl
			  FROM t4020_demand_master
			 WHERE c4020_demand_master_id  = p_dmd_templt_id
			   AND c4020_void_fl IS NULL;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				v_old_name := NULL;
		END;
	END IF;
	
     UPDATE t4020_demand_master
        SET c4020_demand_nm         = p_dmd_templt_nm
          , c4020_demand_period     = p_dmd_period
          , c4020_forecast_period   = p_forecast_period
          , c4020_request_period    = p_setbuild_forecast
          , c901_company_id         = p_company_id
          , c4020_last_updated_by   = p_user_id
          , c4020_last_updated_date = SYSDATE
          , c4020_primary_user      = p_sheetowner_id
      WHERE c4020_demand_master_id  = p_dmd_templt_id;
      
    IF (SQL%ROWCOUNT  = 0) THEN
         SELECT s4020_demand_master.NEXTVAL 
           INTO p_dmd_templt_id 
           FROM DUAL;
         
         INSERT
           INTO t4020_demand_master
            (
                c4020_demand_master_id, c4020_demand_nm, c4020_demand_period
              , c4020_forecast_period, c4020_request_period, c4020_inactive_fl
              , c901_company_id, c4020_created_by, c4020_created_date
              , c4020_primary_user, c901_demand_type, c901_sheet_status
            )
            VALUES
            (
                p_dmd_templt_id, p_dmd_templt_nm, p_dmd_period
              , p_forecast_period, p_setbuild_forecast, p_inactive_fl
              , p_company_id, p_user_id, SYSDATE
              , p_sheetowner_id, 4000103, 102640
            ) ; -- 4000103 - Demand Sheet Template , 102640 - Draft status
    END IF;
    
    SELECT COUNT(1) INTO v_sheet_cnt
      FROM t4020_demand_master
     WHERE c4020_parent_demand_master_id = p_dmd_templt_id
       AND c901_demand_type != 4000103
       AND c4020_void_fl IS NULL;
       
	IF v_sheet_cnt > 0 THEN
	     UPDATE t4020_demand_master
	        SET c4020_demand_period     = p_dmd_period
	          , c4020_forecast_period   = p_forecast_period
	          , c4020_request_period    = DECODE(c901_demand_type,40020,NULL,p_setbuild_forecast) --40020 SALES
	          , c4020_primary_user      = p_sheetowner_id
	          , c4020_last_updated_by   = p_user_id
	          , c4020_last_updated_date = SYSDATE
	      WHERE c4020_parent_demand_master_id  = p_dmd_templt_id;	
	      
		IF TRIM(v_old_name) <> TRIM(p_dmd_templt_nm) THEN
			gm_upd_dmd_sheet_name(p_dmd_templt_id,p_user_id);
		END IF;
		
	END IF;
	
	IF NVL(v_old_inactive_fl,'N') <> NVL(p_inactive_fl,'N') THEN
		-- active/inactive demand template and sheets
		gm_sav_inactive_template(p_dmd_templt_id,p_inactive_fl,p_comments,p_user_id);
	ELSIF p_comments IS NOT NULL THEN
		--Create the comments for the template
		gm_update_log(p_dmd_templt_id,p_comments,'1227',p_user_id,v_msg);-- 1227 - DemandSheet Log type
	END IF;
	EXCEPTION
		WHEN DUP_VAL_ON_INDEX
		THEN
			--The Template name already exists.
			raise_application_error (-20597, '');	
END gm_sav_demand_template;
    --
    /*******************************************************
    * Purpose: This Procedure is used to update demand sheet 
    * based on demand template
    *******************************************************/
    --
PROCEDURE gm_upd_dmd_sheet_name (
        p_dmd_templ_id IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_user_id      IN t4020_demand_master.c4020_created_by%TYPE)
AS
	CURSOR v_cur_dmd_sht
    	IS
         SELECT c4020_demand_master_id sheetid, get_code_name_alt(C901_Demand_Type) typenm
           FROM t4020_demand_master
          WHERE c4020_parent_demand_master_id = p_dmd_templ_id
            AND c901_demand_type != 4000103
            AND c4020_void_fl IS NULL;
	v_dmd_sheet_name 	t4020_demand_master.c4020_demand_nm%TYPE;
	v_regshrtnm 		VARCHAR2(50);
	v_continue VARCHAR2(10);
BEGIN
	
	BEGIN
		SELECT c4020_demand_nm INTO v_dmd_sheet_name
		  FROM t4020_demand_master
		 WHERE c4020_demand_master_id  = p_dmd_templ_id
		   AND c4020_void_fl IS NULL;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			RETURN;
	END;

	FOR cur_sht IN v_cur_dmd_sht
	LOOP
		BEGIN
			 SELECT DECODE(c901_level_id,'102584',GET_RULE_VALUE(c901_level_value,'REG_SHRT_CODE'),get_code_name(c901_level_value))
			   INTO v_regshrtnm
			   FROM t4020_demand_master t4020, t4015_global_sheet_mapping t4015
			  WHERE t4020.c4015_global_sheet_map_id = t4015.c4015_global_sheet_map_id
			    AND c4020_demand_master_id          = cur_sht.sheetid
			    AND t4020.c4020_void_fl            IS NULL
			    AND t4015.c4015_void_fl            IS NULL;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				v_continue := '';
		END;
		
	     UPDATE t4020_demand_master
	        SET c4020_demand_nm         = v_dmd_sheet_name ||' '|| v_regshrtnm ||' '|| cur_sht.typenm
	          , c4020_last_updated_by   = p_user_id
	          , c4020_last_updated_date = SYSDATE
	      WHERE C4020_Demand_Master_Id  = cur_sht.sheetid;		
		    
	END LOOP;
END gm_upd_dmd_sheet_name;
    --
    /*******************************************************
    * Purpose: This Procedure is used to save grouping 
    * information for a demand template
    *******************************************************/
    --
PROCEDURE gm_sav_dmd_tmplt_mapping (
        p_dmd_templ_id IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_input_string IN CLOB,
        p_user_id      IN t4020_demand_master.c4020_created_by%TYPE)
AS
    v_input_string CLOB:= p_input_string;
    v_sub_string   CLOB;
    v_refids_string  CLOB;
    v_reftype 	   VARCHAR2 (50) ;
BEGIN
     DELETE
       FROM T4021_Demand_Mapping
      WHERE c4020_demand_master_id     = p_dmd_templ_id;

   WHILE INSTR (v_input_string, '|') <> 0
    LOOP
        v_sub_string   := SUBSTR (v_input_string, 1, INSTR (v_input_string, '|') - 1) ;
        v_input_string := SUBSTR (v_input_string, INSTR (v_input_string, '|')    + 1) ;
        v_reftype      := SUBSTR (v_sub_string, 1, INSTR (v_sub_string, '^')      - 1) ;
        v_refids_string  := SUBSTR (v_sub_string, INSTR (v_sub_string, '^')         + 1) ;

        gm_pkg_oppr_sheet.gm_fc_sav_demandsheetmap (p_dmd_templ_id, 4000103, v_reftype, v_refids_string) ;
    END LOOP;
    gm_sav_populate_templ_map(p_dmd_templ_id,p_user_id);
    
    -- When Mapping is updated, we should update the Demand Master for ETL To sync to OLTP.
    UPDATE T4020_DEMAND_MASTER 
      SET  C4020_LAST_UPDATED_BY=p_user_id
		  ,C4020_LAST_UPDATED_DATE = sysdate
     WHERE c4020_parent_demand_master_id = p_dmd_templ_id
       AND c901_demand_type != 4000103 -- Demand Sheet Template
       AND c4020_void_fl IS NULL;
     
    UPDATE T4020_DEMAND_MASTER 
      SET C4020_LAST_UPDATED_BY=p_user_id
		  ,C4020_LAST_UPDATED_DATE = sysdate
     WHERE C4020_DEMAND_MASTER_ID = p_dmd_templ_id
      AND c4020_void_fl IS NULL;
            
END gm_sav_dmd_tmplt_mapping;
    --
    /*******************************************************
    * Purpose: This Procedure is used to save demand 
    * sheet from the demand template
    *******************************************************/
    --
PROCEDURE gm_sav_gen_demand_sheet (
        p_dmd_templ_id IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_user_id      IN t4020_demand_master.c4020_created_by%TYPE)
AS
    v_cur_templt 		TYPES.cursor_type;
    v_cur_glblmpgdtl 	TYPES.cursor_type;
    v_demand_master_id 	t4020_demand_master.c4020_demand_master_id%TYPE;
    v_demand_nm 		t4020_demand_master.c4020_demand_nm%TYPE;
    v_demand_period 	t4020_demand_master.c4020_demand_period%TYPE;
    v_forecast_period 	t4020_demand_master.c4020_forecast_period%TYPE;
    v_request_period 	t4020_demand_master.c4020_request_period%TYPE;
    v_inactive_fl 		t4020_demand_master.c4020_inactive_fl%TYPE;
    v_company_id 		t4020_demand_master.c901_company_id%TYPE;
    v_primary_user 		t4020_demand_master.c4020_primary_user%TYPE;
    v_outdemsheetid 	t4020_demand_master.c4020_demand_master_id%TYPE;
    v_sheet_status_id	t4020_demand_master.c901_sheet_status%TYPE;
    v_company_nm		VARCHAR2(50);
    v_sheet_status		VARCHAR2(50);
    v_primary_user_nm	VARCHAR2(200);
 	CURSOR v_cur_glbl_map IS  
	     SELECT c4015_global_sheet_map_id glbl_map_id, DECODE(c901_level_id,'102584',GET_RULE_VALUE(c901_level_value,'REG_SHRT_CODE'),get_code_name(c901_level_value)) lvlnm
	          , c4015_sales_sheet_fl salesfl, c4015_consignment_sheet_fl consignfl, c4015_inhouse_sheet_fl inhousfl
	   	   FROM t4015_global_sheet_mapping
	      WHERE c901_company_id = v_company_id
	        AND c4015_void_fl  IS NULL;
BEGIN
    --Based on Template Id, get the template information
    gm_pkg_oppr_dmd_templt_rpt.gm_fch_demand_template (p_dmd_templ_id, v_cur_templt) ;
    LOOP
        FETCH v_cur_templt
           INTO v_demand_master_id, v_demand_nm, v_demand_period
          , v_forecast_period, v_request_period, v_inactive_fl
          , v_company_id, v_primary_user, v_company_nm
          , v_primary_user_nm,v_sheet_status_id,v_sheet_status;
        EXIT WHEN v_cur_templt%NOTFOUND;
    END LOOP;
    
    --Cursor pulling the global sheet mapping information based on Company Id. 
	FOR v_cur IN v_cur_glbl_map
	LOOP
	    
	    IF NVL(v_cur.salesfl,'N') = 'Y' THEN
	    
	        gm_pkg_oppr_sheet.gm_fc_sav_demsheet (NULL
	        									, v_demand_nm||' '||v_cur.lvlnm||' '|| GET_CODE_NAME_ALT(40020)
	        									, v_demand_period
	        									, v_forecast_period
	        									, NULL
	        									, NULL
	        									, v_inactive_fl
	        									, p_user_id
	        									, v_primary_user
	        									, 40020 --sales
	        									, NULL
	        									, p_dmd_templ_id
	        									, v_cur.glbl_map_id
	        									, v_company_id
	        									, NULL
	        									, v_outdemsheetid);
	    END IF;    
		IF NVL(v_cur.consignfl,'N') = 'Y' THEN
	    
	        gm_pkg_oppr_sheet.gm_fc_sav_demsheet (NULL
	        									, v_demand_nm||' '||v_cur.lvlnm||' '|| GET_CODE_NAME_ALT(40021)
	        									, v_demand_period
	        									, v_forecast_period
	        									, NULL
	        									, NULL
	        									, v_inactive_fl
	        									, p_user_id
	        									, v_primary_user
	        									, 40021 --consignment
	        									, v_request_period
	        									, p_dmd_templ_id
	        									, v_cur.glbl_map_id
	        									, v_company_id
	        									, NULL
	        									, v_outdemsheetid);
	        									
	    END IF;    
		IF NVL(v_cur.inhousfl,'N') = 'Y' THEN
	    
	        gm_pkg_oppr_sheet.gm_fc_sav_demsheet (NULL
	        									, v_demand_nm||' '||v_cur.lvlnm||' '|| GET_CODE_NAME_ALT(40022)
	        									, v_demand_period
	        									, v_forecast_period
	        									, NULL
	        									, NULL
	        									, v_inactive_fl
	        									, p_user_id
	        									, v_primary_user
	        									, 40022 -- Inhouse
	        									, v_request_period
	        									, p_dmd_templ_id
	        									, v_cur.glbl_map_id
	        									, v_company_id
	        									, NULL
	        									, v_outdemsheetid);
	        
	    END IF;
    END LOOP;
    gm_sav_populate_templ_map(p_dmd_templ_id,p_user_id);
    
    --upate template status to active
     UPDATE t4020_demand_master
	    SET c901_sheet_status       = 102641
          , c4020_last_updated_by   = p_user_id
          , c4020_last_updated_date = SYSDATE
      WHERE c4020_demand_master_id  = p_dmd_templ_id;    
END gm_sav_gen_demand_sheet;
    --
    /*******************************************************
    * Purpose: This Procedure is used to save demand 
    * sheet mapping details from the demand template
    *******************************************************/
    --
PROCEDURE gm_sav_populate_templ_map (
        p_dmd_templ_id IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_user_id      IN t4020_demand_master.c4020_created_by%TYPE)
AS
    CURSOR v_cur_dmd_sht
    IS
         SELECT c4020_demand_master_id sheetid, c901_demand_type demandtype
           FROM t4020_demand_master
          WHERE c4020_parent_demand_master_id = p_dmd_templ_id
            AND c901_demand_type != 4000103 -- Demand Sheet Template
            AND c4020_void_fl IS NULL;
BEGIN
    FOR v_cur IN v_cur_dmd_sht
    LOOP
         DELETE FROM T4021_Demand_Mapping WHERE c4020_demand_master_id = v_cur.sheetid;
         
         INSERT
           INTO t4021_demand_mapping
            (
                c4021_demand_mapping_id, c4020_demand_master_id, c901_ref_type
              , c4021_ref_id, c901_action_type
            )
         SELECT s4021_demand_mapping.NEXTVAL, v_cur.sheetid, t4021.c901_ref_type
          , t4021.c4021_ref_id, t4021.c901_action_type
           FROM t4020_demand_master t4020, t4021_demand_mapping t4021
          WHERE t4020.c4020_demand_master_id = p_dmd_templ_id
            AND t4020.c4020_demand_master_id = t4021.c4020_demand_master_id
            AND t4020.c901_demand_type = 4000103 -- Demand Sheet Template
            AND t4020.c4020_inactive_fl IS NULL
            AND t4020.c4020_void_fl IS NULL
            AND t4021.c901_ref_type = DECODE(v_cur.demandtype,40020,40030,40021,40031,40022,4000109);
            /* Sales Sheet(40020) -> Load Only Groups (40030). 
			 * Consignment Sheet (40021) -> Load 40031 
			 * In-House Sheet (40022) -> Load 4000109 
             */
    END LOOP;
END gm_sav_populate_templ_map;
    --
    /*******************************************************
    * Purpose: This Procedure is used to inactive demand template
    *******************************************************/
    --
PROCEDURE gm_sav_inactive_template (
        p_dmd_templt_id     IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_inactive_fl       IN t4020_demand_master.c4020_inactive_fl%TYPE,
        p_comments		    IN t902_log.c902_comments%TYPE,        
        p_user_id           IN t4020_demand_master.c4020_created_by%TYPE)
AS

	CURSOR v_cur_dmd_sht
    	IS
         SELECT c4020_demand_master_id sheetid
           FROM t4020_demand_master
          WHERE c4020_parent_demand_master_id = p_dmd_templt_id
            AND c901_demand_type != 4000103
            AND c4020_void_fl IS NULL;
           
	CURSOR v_cur_dmd_sht_map (p_sheet_id VARCHAR2)
    	IS          
   		 SELECT t4021.c4021_ref_id refid, NVL (t4021.c4021_stack_month, 0) months, t4021.c901_ref_type reftype
		   FROM t4020_demand_master t4020, t4021_demand_mapping t4021
		  WHERE t4020.c4020_demand_master_id = p_sheet_id
		    AND t4020.c4020_demand_master_id = t4021.c4020_demand_master_id
            AND t4020.c901_demand_type != 4000103 -- Demand Sheet Template
            AND t4020.c4020_void_fl IS NULL;
		   
		  
	v_msg 			VARCHAR2(100);
	v_inv_lock_id 	t250_inventory_lock.c250_inventory_lock_id%TYPE;
	v_status		t4040_demand_sheet.c901_status%TYPE;
BEGIN
		
	 SELECT MAX (c250_inventory_lock_id)
	   INTO v_inv_lock_id
	   FROM t250_inventory_lock
	  WHERE c250_void_fl IS NULL;
	
	-- Cursor get sheets based on template
    FOR cur_sht IN v_cur_dmd_sht
    LOOP
		BEGIN
			 SELECT c901_status
			   INTO v_status
			   FROM t4040_demand_sheet
			  WHERE c250_inventory_lock_id = v_inv_lock_id
			    AND c4020_demand_master_id = cur_sht.sheetid
			    AND c4040_void_fl IS NULL and rownum=1;
	    		EXCEPTION 
				WHEN NO_DATA_FOUND THEN
					 v_status := NULL;
		END;
		
		IF v_status = 50550 OR v_status IS NULL THEN -- 50550 - Open status
		
			-- Cursor get sheet mapping detail based on sheed id
			--Commenting this as the functionality is not required.It will be handled during month end job.
			/*
			FOR cur_map IN v_cur_dmd_sht_map(cur_sht.sheetid)
    		LOOP
    			--To move req to common pool and email users
    			
				 gm_pkg_oppr_ld_no_stack.gm_op_nostack_move (cur_sht.sheetid, cur_map.refid, 'S', p_user_id, cur_map.months, cur_map.reftype);
			END LOOP;
			*/
			UPDATE t4020_demand_master
			   SET c4020_inactive_fl       = p_inactive_fl
			     , c4020_last_updated_by   = p_user_id
			     , c4020_last_updated_date = SYSDATE
			 WHERE c4020_demand_master_id  = cur_sht.sheetid;

			--Create the comments for the demand sheet
			IF p_comments IS NOT NULL THEN
				gm_update_log(cur_sht.sheetid,p_comments,'1227',p_user_id,v_msg);-- 1227 - DemandSheet Log type
			END IF;
		ELSE
			--Cannot make the template Inactive, the associated Demand Sheets are in Approved status.
			--Please contact the Sheet Owner.
			raise_application_error ('-20591', '') ;
		END IF;
    END LOOP;
    
	 UPDATE t4020_demand_master
        SET c4020_inactive_fl       = p_inactive_fl
          , c901_sheet_status       = DECODE(p_inactive_fl,'Y',102642,102641) --102642-Inactive, 102641 - Active
          , c4020_last_updated_by   = p_user_id
          , c4020_last_updated_date = SYSDATE
      WHERE c4020_demand_master_id  = p_dmd_templt_id;
      
      IF p_comments IS NOT NULL THEN
      	gm_update_log(p_dmd_templt_id,p_comments,'1227',p_user_id,v_msg); -- 1227 - DemandSheet Log type
      END IF;
    
    DELETE FROM t4021_demand_mapping WHERE c4020_demand_master_id = p_dmd_templt_id;
    
	gm_sav_populate_templ_map(p_dmd_templt_id,p_user_id);
	
END gm_sav_inactive_template;
END gm_pkg_oppr_dmd_templt_txn;
/