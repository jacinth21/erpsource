/* Formatted on 2009/11/25 13:32 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\operations\Purchasing\gm_pkg_oppr_ttp.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ttp
IS
--
	PROCEDURE gm_fc_fch_ttp_dstemplate_map (
		p_ttpid 		  IN	   t4050_ttp.c4050_ttp_id%TYPE
	  , p_outunselected   OUT	   TYPES.cursor_type
	  , p_outselected	  OUT	   TYPES.cursor_type
	)
	AS
	/*******************************************************
	  * Description : Procedure to fetch ttp group mapping information
	  * Author				: Joe P Kumar
	  *******************************************************/
	BEGIN
		OPEN p_outunselected
		 FOR
			 SELECT   t4020.c4020_demand_master_id ID, t4020.c4020_demand_nm NAME
				 FROM t4020_demand_master t4020
				WHERE t4020.c4020_demand_master_id NOT IN (SELECT NVL (t4051.c4020_demand_master_id, 1)
															 FROM t4051_ttp_demand_mapping t4051)
				  AND t4020.c4020_void_fl IS NULL
				  AND t4020.c901_demand_type  in (4000103,40023) -- Demand Sheet Template type
			 ORDER BY t4020.c4020_demand_nm;

		OPEN p_outselected
		 FOR
			 SELECT   t4020.c4020_demand_master_id ID, t4020.c4020_demand_nm NAME
				 FROM t4051_ttp_demand_mapping t4051, t4020_demand_master t4020
				WHERE t4051.c4020_demand_master_id = t4020.c4020_demand_master_id(+) 
				  AND t4051.c4050_ttp_id = p_ttpid
				  AND t4020.c4020_void_fl IS NULL
				  AND t4020.c901_demand_type  in (4000103,40023) -- Demand Sheet Template type
			 ORDER BY t4020.c4020_demand_nm;
	END gm_fc_fch_ttp_dstemplate_map;

--
	PROCEDURE gm_fc_sav_ttpdsmap (
		p_ttpid 	 IN 	  t4050_ttp.c4050_ttp_id%TYPE
	  , p_ttpname	 IN 	  t4050_ttp.c4050_ttp_nm%TYPE
	  , p_primuser	 IN 	  t4050_ttp.c4050_primary_user%TYPE
	  , p_userid	 IN 	  t4050_ttp.c4050_created_by%TYPE
	  , p_inputstr	 IN 	  VARCHAR2
	  , p_category	 IN 	  t4050_ttp.c901_ttp_category%TYPE --PMT-49826 -Save category id in t4050table
	  , p_outttpid	 OUT	  t4050_ttp.c4050_ttp_id%TYPE
	)
	AS
	/*******************************************************
	 * Description : Procedure to save		  / update the demand sheets associated to a TTP
	 * Author		  : Joe P Kumar
	  *******************************************************/
--
		v_ttpid 	   t4050_ttp.c4050_ttp_id%TYPE;
	BEGIN
		-- The input String in the form of 5,8 will be set into this temp view.
		my_context.set_my_inlist_ctx (p_inputstr);
		p_outttpid	:= p_ttpid;

		UPDATE t4050_ttp
		   SET c4050_ttp_nm = p_ttpname
			 , c4050_primary_user = p_primuser
			 , c901_ttp_category = p_category
			 , c4050_last_updated_by = p_userid
			 , c4050_last_updated_date = SYSDATE
		 WHERE c4050_ttp_id = p_ttpid;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s4050_ttp.NEXTVAL
			  INTO p_outttpid
			  FROM DUAL;

			INSERT INTO t4050_ttp
						(c4050_ttp_id, c4050_ttp_nm, c4050_primary_user, c4050_created_by, c4050_created_date,c901_ttp_category
						)
				 VALUES (p_outttpid, p_ttpname, p_primuser, p_userid, SYSDATE,p_category
						);
		END IF;

		DELETE FROM t4051_ttp_demand_mapping t4051
			  WHERE t4051.c4050_ttp_id = p_outttpid;

		INSERT INTO t4051_ttp_demand_mapping t4051
					(t4051.c4051_po_ttp_map_id, c4050_ttp_id, c4020_demand_master_id)
			SELECT s4051_ttp_demand_mapping.NEXTVAL, p_outttpid, tmptable.refid
			  FROM (SELECT token refid
					  FROM v_in_list) tmptable;
	END gm_fc_sav_ttpdsmap;

--
	PROCEDURE gm_fc_fch_ttp_report (
		p_report   OUT	 TYPES.cursor_type
	)
	 /*******************************************************
	* Description : Procedure to fetch ttp report
	* Author		: Joe P Kumar
	*******************************************************/
	AS
	BEGIN
		OPEN p_report
		 FOR
			 SELECT   t4050.c4050_ttp_id ttpid, t4050.c4050_ttp_nm ttpnm, t4020.c4020_demand_nm dmname
					, get_code_name (t4021.c901_ref_type) maptype
					, DECODE (t4021.c901_ref_type
							, 40030, get_group_name (t4021.c4021_ref_id)
							, 40031, get_set_name (t4021.c4021_ref_id)
							, 40032, get_code_name (t4021.c4021_ref_id)
							, 40033, get_code_name (t4021.c4021_ref_id)
							 ) mapdata
					, get_code_name (c901_action_type) stackfl
				 FROM t4050_ttp t4050
					, t4051_ttp_demand_mapping t4051
					, t4020_demand_master t4020
					, t4021_demand_mapping t4021
				WHERE t4050.c4050_ttp_id = t4051.c4050_ttp_id
				  AND t4020.c4020_demand_master_id = t4021.c4020_demand_master_id
				  AND t4051.c4020_demand_master_id = t4020.c4020_demand_master_id
				  AND t4050.c4050_void_fl IS NULL
				  AND t4020.c4020_void_fl IS NULL
			 GROUP BY t4050.c4050_ttp_id
					, t4050.c4050_ttp_nm
					, t4020.c4020_demand_nm
					, t4021.c901_ref_type
					, t4021.c4021_ref_id
					, c901_action_type
			 ORDER BY ttpnm, dmname, maptype;
	END gm_fc_fch_ttp_report;

--
	PROCEDURE gm_fc_fch_monthsheet_report (
		p_sheetnmregex	 IN 	  VARCHAR2
	  , p_status		 IN 	  t4040_demand_sheet.c901_status%TYPE
	  , p_sheettype 	 IN 	  t4020_demand_master.c901_demand_type%TYPE
	  , p_month 		 IN 	  VARCHAR2
	  , p_year			 IN 	  VARCHAR2
	  , p_ttpid 		 IN 	  t4050_ttp.c4050_ttp_id%TYPE
	  , p_primary_user	 IN 	  t4040_demand_sheet.c4040_primary_user%TYPE
	  , p_report		 OUT	  TYPES.cursor_type
	)
	 /*******************************************************
	* Description : Procedure to fetch monthly sheet report
	* Author		: D James
	*******************************************************/
	AS
	BEGIN
		OPEN p_report
		 FOR
			 SELECT   t4020.c4020_demand_master_id dmid, t4020.c4020_demand_nm demnm
					, get_code_name (t4020.c901_demand_type) dtype, get_user_name (t4040.c4040_primary_user) cuser
					, TO_CHAR (c4040_load_dt, 'mm/dd/yyyy') ldate, c4040_demand_period dper
					, get_code_name (c901_status) dstat, c4040_forecast_period fper
					, get_user_name (c4040_last_updated_by) luser, t4040.c4040_demand_sheet_id dmsheetid
					, TO_CHAR (c4040_load_dt, 'MM') ldtmonth, TO_CHAR (c4040_load_dt, 'YYYY') ldtyear
					, TO_CHAR (t4040.c4040_demand_period_dt, 'mm/dd/yyyy') perioddate, TO_CHAR (c901_status) statusid
				 FROM t4040_demand_sheet t4040, t4020_demand_master t4020
				WHERE t4040.c4020_demand_master_id = t4020.c4020_demand_master_id
				  AND t4040.c4040_void_fl IS NULL
				  AND t4020.c4020_void_fl IS NULL
				  AND t4020.c4020_demand_nm LIKE DECODE (p_sheetnmregex, '', t4020.c4020_demand_nm, p_sheetnmregex)
				  AND c901_status = DECODE (p_status, '0', c901_status, p_status)
				  AND t4020.c901_demand_type = DECODE (p_sheettype, '0', t4020.c901_demand_type, p_sheettype)
				  AND t4040.c4040_demand_period_dt =
						  DECODE (p_month
								, '0', t4040.c4040_demand_period_dt
								, TO_DATE (p_month || '/' || p_year, 'MM/YYYY')
								 )
				  AND NVL (c4052_ttp_detail_id, 1) = DECODE (p_ttpid, '0', NVL (c4052_ttp_detail_id, 1), p_ttpid)
				  AND t4040.c4040_primary_user = DECODE (p_primary_user, '0', t4040.c4040_primary_user, p_primary_user)
			 ORDER BY demnm;
	END gm_fc_fch_monthsheet_report;

--
	/*******************************************************
   * Description : Procedure to fetch demand sheet for that TTP
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_ttp_dsmonthly (
		p_ttp_id 	 IN		 t4050_ttp.c4050_ttp_id%TYPE
	  , p_monyear	 IN		 VARCHAR2
	  , p_level_id	 IN		 t901_code_lookup.c901_code_id%TYPE
	  , p_company_id IN	     t901_code_lookup.c901_code_id%TYPE
	  , p_output	OUT 	 TYPES.cursor_type
	)
	AS
	v_demand_type 	   NUMBER;
	BEGIN
		
		BEGIN	
			 SELECT MAX(T4020.c901_demand_type) 
			   INTO v_demand_type
			   FROM t4051_ttp_demand_mapping T4051
			      , t4020_demand_master t4020
			      , t4050_ttp t4050
			  WHERE t4051.c4020_demand_master_id = t4020.c4020_demand_master_id
			    AND t4050.c4050_ttp_id = t4051.c4050_ttp_id
			    AND t4050.c4050_ttp_id = p_ttp_id
			    AND t4020.c4020_void_fl IS NULL
			    AND t4050.c4050_void_fl IS NULL
			ORDER BY t4050.c4050_ttp_nm;
		EXCEPTION 
		WHEN NO_DATA_FOUND
		THEN
			v_demand_type 	:= '';
		END;	

		IF v_demand_type = '40023' ---For multi part Type
		THEN
	        OPEN p_output
			 FOR
			SELECT   t4040.c4040_demand_sheet_id ID
						,	 NVL (t4040.c4040_demand_sheet_id, 'No Id')
						  || ' : '
						  || gm_pkg_oppr_sheet.get_demandsheet_name (t4051.c4020_demand_master_id)
						  || ' - '
						  || DECODE (t4040.c901_status, NULL, 'Not Loaded', get_code_name (t4040.c901_status)) NAME
						, DECODE (t4040.c901_status,50551, 'false','true') status
						, t4040.c901_status statusid
						, gm_pkg_oppr_sheet.get_demandsheet_name (t4051.c4020_demand_master_id) DMNM
					 FROM t4040_demand_sheet t4040, t4051_ttp_demand_mapping t4051
					WHERE t4051.c4020_demand_master_id = t4040.c4020_demand_master_id(+)
					  AND t4051.c4050_ttp_id = p_ttp_id
					  AND TO_CHAR (t4040.c4040_load_dt, 'MM/YYYY') = p_monyear
					  AND t4040.c4040_void_fl IS NULL
				 UNION
				 SELECT   t4040.c4040_demand_sheet_id ID
						,	 NVL (t4040.c4040_demand_sheet_id, 'No Id')
						  || ' : '
						  || gm_pkg_oppr_sheet.get_demandsheet_name (t4040.c4020_demand_master_id)
						  || ' - '
						  || DECODE (t4040.c901_status, NULL, 'Not Loaded', get_code_name (t4040.c901_status)) NAME
						, DECODE (t4040.c901_status, 50551, 'false', 'true') status
						, t4040.c901_status statusid
						, gm_pkg_oppr_sheet.get_demandsheet_name (t4040.c4020_demand_master_id) DMNM
					 FROM t4040_demand_sheet t4040
					WHERE t4040.c4052_ttp_detail_id IN (SELECT c4052_ttp_detail_id
														  FROM t4052_ttp_detail
														 WHERE c4050_ttp_id = p_ttp_id)
					  AND TO_CHAR (t4040.c4040_load_dt, 'MM/YYYY') = p_monyear
					  AND t4040.c4040_void_fl IS NULL
				 ORDER BY DMNM;
		ELSE		
		 OPEN p_output
		 FOR
			  SELECT t4040.c4040_demand_sheet_id ID
			      , NVL (t4040.c4040_demand_sheet_id, 'No Id') || ' : ' || gm_pkg_oppr_sheet.get_demandsheet_name (t4020.c4020_demand_master_id) || ' - ' || 
			      	DECODE (t4040.c901_status, NULL,'Not Loaded', get_code_name (t4040.c901_status)) NAME
				  , DECODE (t4040.c901_status,50551, 'false','true') status
				  , t4040.c901_status statusid
				  , gm_pkg_oppr_sheet.get_demandsheet_name (t4020.c4020_demand_master_id) DMNM
			  FROM t4051_ttp_demand_mapping t4051, t4020_demand_master t4020, t4040_demand_sheet t4040
		     WHERE t4051.c4020_demand_master_id    = t4020.c4020_parent_demand_master_id
			   AND t4020.c4020_demand_master_id    = t4040.c4020_demand_master_id (+)
			   AND t4020.c4015_global_sheet_map_id =
				    (
				         SELECT c4015_global_sheet_map_id
				           FROM t4015_global_sheet_mapping t4015
				          WHERE NVL(t4015.c901_level_value,'-9999') = NVL(p_level_id,'-9999')
				            AND NVL(t4015.c901_company_id,'-9999')  = NVL(p_company_id,'-9999')
				            AND t4015.c4015_void_fl   IS NULL
				    )
			   AND t4051.c4050_ttp_id                       = p_ttp_id
			   AND TO_CHAR (t4040.c4040_load_dt, 'MM/YYYY') = p_monyear
			   AND t4040.c4040_void_fl IS NULL
			 UNION
			SELECT t4040.c4040_demand_sheet_id ID
			     , NVL (t4040.c4040_demand_sheet_id, 'No Id') || ' : ' || gm_pkg_oppr_sheet.get_demandsheet_name (t4020.c4020_demand_master_id) || ' - ' || 
			       DECODE (t4040.c901_status, NULL,'Not Loaded', get_code_name (t4040.c901_status)) NAME
			     , DECODE (t4040.c901_status, 50551, 'false', 'true') status
			     , t4040.c901_status statusid
				 , gm_pkg_oppr_sheet.get_demandsheet_name (t4020.c4020_demand_master_id) DMNM
			  FROM t4040_demand_sheet t4040, t4020_demand_master t4020
		     WHERE t4020.c4020_demand_master_id = t4040.c4020_demand_master_id
			   AND t4040.c4052_ttp_detail_id   IN
				    (
				         SELECT c4052_ttp_detail_id
				           FROM t4052_ttp_detail
				          WHERE c4050_ttp_id = p_ttp_id
				    )
			   AND t4020.c4015_global_sheet_map_id =
				    (
				         SELECT c4015_global_sheet_map_id
				           FROM t4015_global_sheet_mapping t4015
				          WHERE NVL(t4015.c901_level_value,'-9999') = NVL(p_level_id,'-9999')
				            AND NVL(t4015.c901_company_id,'-9999')  = NVL(p_company_id,'-9999')
				            AND t4015.c4015_void_fl   IS NULL
				    )
			   AND TO_CHAR (t4040.c4040_load_dt, 'MM/YYYY') = p_monyear
			   AND t4040.c4040_void_fl IS NULL
		  ORDER BY DMNM;
		  END IF;
END gm_fc_fch_ttp_dsmonthly;

--
	/*******************************************************
   * Description : Procedure to fetch all approved monthly demand sheet
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_ttp_approvedds (
		p_ttpid 	IN		 t4050_ttp.c4050_ttp_id%TYPE
	  , p_monyear	IN		 VARCHAR2
	  , p_output	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_output
		 FOR
			 SELECT   t4040.c4040_demand_sheet_id ID
					, gm_pkg_oppr_sheet.get_demandsheet_name (t4040.c4020_demand_master_id) NAME
				 FROM t4040_demand_sheet t4040
				WHERE t4040.c901_status = 50551   -- approved status
				  AND t4040.c4020_demand_master_id NOT IN (SELECT t4051.c4020_demand_master_id
															 FROM t4051_ttp_demand_mapping t4051
															WHERE t4051.c4050_ttp_id = p_ttpid)
				  AND TO_CHAR (t4040.c4040_load_dt, 'MM/YYYY') = p_monyear
				  AND t4040.c4040_void_fl IS NULL
				  AND t4040.c4040_demand_sheet_id NOT IN (
						  SELECT t4040.c4040_demand_sheet_id ID
							FROM t4040_demand_sheet t4040
						   WHERE t4040.c4052_ttp_detail_id IN (SELECT c4052_ttp_detail_id
																 FROM t4052_ttp_detail
																WHERE c4050_ttp_id = p_ttpid)
							 AND TO_CHAR (t4040.c4040_load_dt, 'MM/YYYY') = p_monyear
							 AND t4040.c4040_void_fl IS NULL)
			 ORDER BY NAME;
	END gm_fc_fch_ttp_approvedds;

	--
	/*******************************************************
   * Description : Procedure to fetch monthly sheet report
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_inventoryidlist (
	    p_ttp_id 	       IN	 t4050_ttp.c4050_ttp_id%TYPE
	  ,	p_lock_type 	   IN	 t250_inventory_lock.c901_lock_type%TYPE
	  , p_level_id	       IN	 t901_code_lookup.c901_code_id%TYPE
	  , p_company_id       IN	 t901_code_lookup.c901_code_id%TYPE
	  , p_inventoryidlist  OUT 	 TYPES.cursor_type
	)
	AS
	v_demand_type 	   NUMBER;
	BEGIN
		
		BEGIN	
			 SELECT MAX(T4020.c901_demand_type) 
			   INTO v_demand_type
			   FROM t4051_ttp_demand_mapping T4051
			      , t4020_demand_master t4020
			      , t4050_ttp t4050
			  WHERE t4051.c4020_demand_master_id = t4020.c4020_demand_master_id
			    AND t4050.c4050_ttp_id = t4051.c4050_ttp_id
			    AND t4050.c4050_ttp_id = p_ttp_id
			    AND t4020.c4020_void_fl IS NULL
			    AND t4050.c4050_void_fl IS NULL
			ORDER BY t4050.c4050_ttp_nm;
		EXCEPTION 
		WHEN NO_DATA_FOUND
		THEN
			v_demand_type 	:= '';
		END;	

		IF v_demand_type = '40023' -- If multipart, always fetch the US Inventory
		THEN
			OPEN p_inventoryidlist
			 FOR
			   SELECT t250.c250_inventory_lock_id ID, TO_CHAR (t250.c250_lock_date, 'MM/DD/YYYY') NAME, TRUNC (t250.c250_lock_date) dt
			     FROM t250_inventory_lock t250
			           ,  t4016_global_inventory_mapping t4016            
			    WHERE t250.c250_void_fl IS NULL 
			          AND t250.c901_lock_type = p_lock_type --main inventory lock  20430
			          AND t4016.c4016_global_inventory_map_id = t250.c4016_global_inventory_map_id
			           AND NVL(t4016.c4016_global_inventory_map_id,'-9999')= NVL(( SELECT  t4015.c4016_global_inventory_map_id 
						                                        FROM T4015_GLOBAL_SHEET_MAPPING T4015
						                                       WHERE NVL(T4015.C901_LEVEL_VALUE,'-9999') = 100823 -- Hard code to US for Multi Part
						                                         AND NVL(T4015.C901_COMPANY_ID,'-9999') = 100800 -- Hard code to Globus for Multi Part
						                                         AND T4015.c4015_void_fl IS NULL
		                                        			 ),'-9999')
	         ORDER BY dt DESC;
		ELSE
			OPEN p_inventoryidlist
			 FOR
				SELECT t250.c250_inventory_lock_id ID, TO_CHAR (t250.c250_lock_date, 'MM/DD/YYYY') NAME, TRUNC (t250.c250_lock_date) dt
			     FROM t250_inventory_lock t250
			           ,  t4016_global_inventory_mapping t4016            
			    WHERE t250.c250_void_fl IS NULL 
			          AND t250.c901_lock_type = p_lock_type --main inventory lock  20430
			          AND t4016.c4016_global_inventory_map_id = t250.c4016_global_inventory_map_id
			           AND NVL(t4016.c4016_global_inventory_map_id,'-9999')= NVL(( SELECT  t4015.c4016_global_inventory_map_id 
						                                        FROM T4015_GLOBAL_SHEET_MAPPING T4015
						                                       WHERE NVL(T4015.C901_LEVEL_VALUE,'-9999') = NVL(p_level_id,'-9999')-- Level Selected
						                                         AND NVL(T4015.C901_COMPANY_ID,'-9999') = NVL(p_company_id,'-9999') -- Company Based on the TTP Selected
						                                         AND T4015.c4015_void_fl IS NULL
		                                        			 ),'-9999')
	         ORDER BY dt DESC;
	     END IF;
	END gm_fc_fch_inventoryidlist;

	/*******************************************************
   * Description : Procedure to fetch TTP List
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_ttplist (
		p_output   OUT	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_output
		FOR
		     SELECT t4050.c4050_ttp_id codeid
                   , t4050.c4050_ttp_nm codenm
                   , gm_pkg_oppr_ttp.get_ttp_company_id (t4050.c4050_ttp_id) company_id
               FROM t4050_ttp t4050
              WHERE t4050.c4050_void_fl IS NULL
            ORDER BY UPPER(t4050.c4050_ttp_nm);
	END gm_fc_fch_ttplist;
	
--
  /*******************************************************
   * Description : Procedure to fetch set par Details
   * Author 	 : VPrasath
   *******************************************************/
--
	PROCEDURE gm_fc_fch_setpar_details (
		p_demandsheetids IN  VARCHAR2
	  , p_out_setpar     OUT TYPES.cursor_type
	)
	AS
	BEGIN
		my_context.set_my_inlist_ctx (p_demandsheetids);	
		
	  OPEN p_out_setpar 
	   FOR
	SELECT t4040.c4040_demand_sheet_id id
	     , t4020.c4020_demand_master_id masterid
	     , t4022.c207_set_id set_id
	     , t4040.c901_demand_type dtype
	     , t4040.c4040_primary_user puser
	     , sum(c4022_par_value) qty
      FROM t4022_demand_par_detail t4022
	     , t4020_demand_master t4020
	     , t4040_demand_sheet t4040
	     , v_in_list vlist
	 WHERE t4022.c207_set_id IS NOT NULL
	   AND t4020.c4020_demand_master_id = t4022.c4020_demand_master_id
	   AND t4020.c4020_demand_master_id = t4040.c4020_demand_master_id
	   AND t4040.c4040_demand_sheet_id = vlist.token
	   AND t4040.c901_demand_type NOT IN (40023, 4000103)
  GROUP BY t4040.c4040_demand_sheet_id
	     , t4020.c4020_demand_master_id
	     , t4022.c207_set_id
	     , t4040.c901_demand_type
	     , t4040.c4040_primary_user;
  
  END gm_fc_fch_setpar_details;
--
  /*******************************************************
   * Description : Procedure to fetch Growth Details
   * Author 	 : VPrasath
   *******************************************************/
--
	PROCEDURE gm_fc_fch_request_details (
		p_demandsheetids IN  VARCHAR2
	  , p_out_req        OUT TYPES.cursor_type
	)
	AS
	BEGIN
		my_context.set_my_inlist_ctx (p_demandsheetids);
			
		OPEN p_out_req FOR
		SELECT id, set_id, period, puser, dtype, masterid,  SUM(qty) qty
		FROM
		(SELECT worldwide.c4040_demand_sheet_id ID
		      , worldwide.c4042_ref_id SET_ID
		      , (worldwide.c4042_qty - NVL(childforecast.c4043_value, 0)) QTY 
		      , worldwide.c4042_period PERIOD
		      , worldwide.c4040_primary_user PUSER
		      , worldwide.c901_demand_type DTYPE
		      , worldwide.c4020_demand_master_id MASTERID
		FROM 
		( SELECT t4040.c4020_demand_master_id
		      , t4042.c4042_ref_id
		      , t4042.c4042_qty
		      , t4042.c4042_period
		      , t4040.c4040_demand_sheet_id
		      , t4040.c4040_primary_user
		      , t4040.c901_demand_type
		  FROM t4040_demand_sheet t4040
		     , t4042_demand_sheet_detail t4042
		WHERE t4040.c901_demand_type IN (40021, 40022)
		   AND t4040.c901_level_id = 102580   -- (Only compnay level sheet)
		   AND t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
		   AND t4042.c205_part_number_id IS NULL
		   AND t4042.c901_type = 50563   -- > Only Forecast
		   AND t4042.c4042_qty <> 0
		   AND t4040.c4040_demand_sheet_id IN (SELECT * FROM v_in_list)
		   AND t4042.c4042_period <= ADD_MONTHS (SYSDATE, t4040.c4040_request_period - 1) 
		   ) worldwide
		   , (SELECT t4046.c4040_demand_sheet_id
		     	   , t4043.c4030_grth_ref_id
		     	   , SUM(t4043.c4043_value) c4043_value
		     	   , t4043.c4043_start_date
		        FROM t4046_demand_sheet_assoc_map t4046
		           , t4043_demand_sheet_growth t4043
		           , t4040_demand_sheet t4040
		       WHERE t4046.c4040_demand_sheet_id IN (SELECT * FROM v_in_list)
		         AND t4046.c4040_demand_sheet_assoc_id = t4043.c4040_demand_sheet_id
		         AND t4046.c4040_demand_sheet_assoc_id = t4040.c4040_demand_sheet_id 
		         AND t4043.c901_grth_ref_type = 20296 -- SET
		         AND t4040.c901_demand_type IN (40021, 40022)
             	 AND t4043.c4043_start_date <=  ADD_MONTHS (SYSDATE, t4040.c4040_request_period - 1) 
		         AND t4043.c4043_value != 0
		    GROUP BY t4046.c4040_demand_sheet_id, t4043.c4030_grth_ref_id, t4043.c4043_start_date
		         ) childforecast
		WHERE worldwide.c4040_demand_sheet_id = childforecast.c4040_demand_sheet_id (+)
		   AND worldwide.c4042_ref_id = childforecast.c4030_grth_ref_id (+)
		   AND worldwide.c4042_period = childforecast.c4043_start_date (+)
		UNION ALL
		SELECT t4046.c4040_demand_sheet_assoc_id ID
		     , t4043.c4030_grth_ref_id REF_ID
		     , t4043.c4043_value QTY
		     , t4043.c4043_start_date PERIOD
		     , t4040.c4040_primary_user PUSER
		     , t4040.c901_demand_type DTYPE
		     , t4040.c4020_demand_master_id MASTERID
		 FROM t4046_demand_sheet_assoc_map t4046
		 	, t4043_demand_sheet_growth t4043
		 	, t4040_demand_sheet t4040
		WHERE t4046.c4040_demand_sheet_id IN (SELECT * FROM v_in_list)
		  AND t4040.c4040_demand_sheet_id = t4046.c4040_demand_sheet_assoc_id
		  AND t4046.c4040_demand_sheet_assoc_id = t4043.c4040_demand_sheet_id
		  AND t4040.c901_demand_type IN (40021, 40022)
		  AND t4043.c901_grth_ref_type = 20296 -- SET
		  AND t4043.c4043_start_date <=  ADD_MONTHS (SYSDATE, t4040.c4040_request_period - 1) 
		  AND t4043.c4043_value != 0
		)
		GROUP BY id, set_id, period, puser, dtype, masterid;
		  
	END gm_fc_fch_request_details;	
		--
	/*******************************************************
   * Description : Procedure to save monthly TTP grouping
   * Author 	 : Joe P Kumar
   Algorithm:
	0. Check if the finalizing is already done for that month. If so, throw an exception
	1. Create a new entry onto t4052_ttp_detail
	2. Update the v_ttp_detail_id created into t4040_demand_sheet for all the selected demand sheets
	3. Load part, qty, historyfl information into t4053_ttp_part_detail for the selected demand sheets from t4042_demand_sheet_detail
	4. Insert into t252_inventory_lock_ref with v_ttp_detail_id and p_inventorydate
   *******************************************************/
--
	PROCEDURE gm_fc_sav_ttp_dsmonthly (
		p_ttpid 		   IN		t4050_ttp.c4050_ttp_id%TYPE
	  , p_demandsheetids   IN		VARCHAR2
	  , p_forecastmonths   IN		VARCHAR2
	  , p_inventoryid	   IN		t252_inventory_lock_ref.c250_inventory_lock_id%TYPE
	  , p_userid		   IN		t4052_ttp_detail.c4052_created_by%TYPE
	  , p_string		   IN		VARCHAR2
	  , p_cnt			   IN		VARCHAR2
	  , p_ttp_dtl_id	   IN OUT	t4052_ttp_detail.c4052_ttp_detail_id%TYPE	   	
	  , p_ttp_type   	   OUT		t4050_ttp.c901_ttp_type%TYPE	 
	)
	AS
		v_ttp_detail_id 	t4052_ttp_detail.c4052_ttp_detail_id%TYPE;
		v_fc_start_date 	DATE;
		v_cnt_link_date 	NUMBER;
		v_string	   		VARCHAR2 (4000) := p_string;
		v_substring    		VARCHAR2 (4000);
		v_partnumber   		VARCHAR2 (20);
		v_qty				VARCHAR2 (10);
		v_id		   		NUMBER;
		v_price 	   		NUMBER;
		
	BEGIN
		-- The input String in the form of 5,8 will be set into this temp view.
		my_context.set_my_inlist_ctx (p_demandsheetids);

		--
		/*
		 	In Java code if string is more then 4000 chars then this proc will be called more then once.
			So for 2nd time it should not call the update and insert. so the following code is written in if.
    	 */
		--
		--PMT-28048-TTP summary approval new screen 
		--1)Get TTP id if already approved from ttp summary screen 
		--2)Update the ttpdetail id in t4040_demand_sheet 
		
		BEGIN
			SELECT c4052_ttp_detail_id INTO v_ttp_detail_id FROM t4052_ttp_detail 
			WHERE c4050_ttp_id = p_ttpid AND c4052_ttp_link_date = trunc(sysdate,'month')
			AND c4052_void_fl is null;
			EXCEPTION WHEN NO_DATA_FOUND THEN
			v_ttp_detail_id:=NULL;
		END;
			
		--1) If ttpdetailid is null i.e approved from ttp_finalizing screen
		--2) Insert into t4052_ttp_detail and update in t4040_demand_sheet.
		IF v_ttp_detail_id IS NULL
		THEN
			SELECT t4050.c4050_ttp_id
				  INTO v_id
				  FROM t4050_ttp t4050
				 WHERE t4050.c4050_ttp_id = p_ttpid
			FOR UPDATE;

			SELECT s4052_ttp_detail.NEXTVAL
			  INTO v_ttp_detail_id
			  FROM DUAL;

			-- 1. Create a new entry onto t4052_ttp_detail
			INSERT INTO t4052_ttp_detail
						(c4052_ttp_detail_id, c4050_ttp_id, c4052_ttp_link_date, c901_status, c4052_created_by
					   , c4052_created_date, c4052_forecast_period
						)
				 VALUES (v_ttp_detail_id, p_ttpid, TO_DATE (TO_CHAR (SYSDATE, 'MM/YYYY'), 'MM/YYYY'), 50581, p_userid
					   , SYSDATE, p_forecastmonths
						);
		 END IF;
			-- 2. Update the v_ttp_detail_id created into t4040_demand_sheet for all the selected demand sheets
			UPDATE t4040_demand_sheet t4040
			   SET t4040.c4052_ttp_detail_id = v_ttp_detail_id
				 , t4040.c901_status = 50552
				 , t4040.c4040_last_updated_by = p_userid
				 , t4040.c4040_last_updated_date = SYSDATE
			 WHERE t4040.c4040_demand_sheet_id IN (SELECT c4040_demand_sheet_id
													 FROM t4020_demand_master t4020, t4040_demand_sheet t4040
														, ( SELECT DISTINCT t4020.c4020_parent_demand_master_id
														      FROM t4020_demand_master t4020, t4040_demand_sheet t4040
														     WHERE t4020.c4020_demand_master_id = t4040.c4020_demand_master_id
														       AND t4040.c4040_demand_sheet_id IN (SELECT * FROM v_in_list)) t4020a
													WHERE t4020.c4020_parent_demand_master_id = t4020a.c4020_parent_demand_master_id
													  AND t4020.c4020_demand_master_id = t4040.c4020_demand_master_id
													  AND t4040.c250_inventory_lock_id = p_inventoryid
													  -- As Multipart TTP does not have a parent D.Master, we are using this condition
													  UNION 
													  SELECT c4040_demand_sheet_id FROM t4040_demand_sheet t4040
													  where c4040_demand_sheet_id IN (SELECT * FROM v_in_list)
													  AND t4040.C901_DEMAND_TYPE = '40023' --Multi-Part
													  );
				p_ttp_dtl_id := v_ttp_detail_id;
	

		BEGIN				
		 SELECT t4050.c901_ttp_type
		   INTO p_ttp_type
		   FROM t4050_ttp t4050
		  WHERE t4050.c4050_ttp_id = p_ttpid
		    AND C4050_void_fl IS NULL;
		 EXCEPTION WHEN NO_DATA_FOUND THEN
		 	p_ttp_type := NULL;
		 END;
				 
		SELECT COUNT (1)
		  INTO v_cnt_link_date
		  FROM t4052_ttp_detail
		 WHERE c4050_ttp_id = p_ttpid 
		   AND c4052_ttp_detail_id = p_ttp_dtl_id 
		   AND c4052_ttp_link_date = TO_DATE (TO_CHAR (SYSDATE, 'MM/YYYY'), 'MM/YYYY');

		IF (v_cnt_link_date > 1)
		THEN
			raise_application_error (-20067, '');	-- TTP sheet for this month has already been finalized
		END IF;

		-- 3. Load part, qty, historyfl information into t4053_ttp_part_detail for the selected demand sheets from t4042_demand_sheet_detail
		-- lock the inventory information
		SELECT MIN (c4042_period)	-- '8/1/2007'
		  INTO v_fc_start_date
		  FROM t4042_demand_sheet_detail t4042
		 WHERE t4042.c901_type = 50563 
		   AND t4042.c4040_demand_sheet_id IN (SELECT *
												 FROM v_in_list);

		WHILE INSTR (v_string, '|') <> 0
		LOOP
--
			v_partnumber := NULL;
			v_qty		:= NULL;
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_partnumber := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_qty		:= SUBSTR (v_substring, INSTR (v_substring, '^') + 1);

			BEGIN
				SELECT c255_unit_cost 
				  INTO v_price
				  FROM t255_part_attribute_lock 
				 WHERE c250_inventory_lock_id = p_inventoryid 
				   AND c205_part_number_id = v_partnumber;
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					v_price 	:= 0;
			END;

			INSERT INTO t4053_ttp_part_detail t4053
						(c4053_ttp_part_id, c4052_ttp_detail_id, c205_part_number_id, c4053_qty, c4053_cost_price
					   , c4053_history_fl, c4053_created_by, c4053_created_date
						)
				 VALUES (s4053_ttp_part_detail.NEXTVAL, p_ttp_dtl_id, v_partnumber, v_qty, v_price
					   , 'N', p_userid, SYSDATE
						);
		END LOOP;

		IF p_cnt = '0'
		THEN
			-- 4. Insert into t252_inventory_lock_ref with v_ttp_detail_id and p_inventorydate
			INSERT INTO t252_inventory_lock_ref
						(c252_lock_ref_id, c250_inventory_lock_id, c901_ref_type, c252_ref_id
						)
				 VALUES (s252_inventory_lock_ref.NEXTVAL, p_inventoryid, 20480, v_ttp_detail_id
						);

			
		 IF p_ttp_type IS NULL
		THEN	
			v_string	:= gm_pkg_oppr_sheet.get_demand_sheet_ids (v_ttp_detail_id);
			gm_pkg_oppr_sheet.gm_op_sav_multipartsheet (v_string, p_userid,'N');
		END IF;

		--updated date, by
		UPDATE t4052_ttp_detail
		   SET c901_status = 50582
			 , c4052_last_updated_by = p_userid
			 , c4052_last_updated_date = TRUNC (SYSDATE)
		 WHERE c4052_ttp_detail_id = v_ttp_detail_id;
		
		END IF;
	END gm_fc_sav_ttp_dsmonthly;
--
  /*******************************************************
   * Description : Procedure to fetch TTP Level List
   * Author 	 : Elango
   *******************************************************/
    PROCEDURE gm_fc_fch_ttplevel (
    	 p_company_id IN t4020_demand_master.C901_COMPANY_ID%TYPE
		,p_output   OUT	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_output
		 FOR
			SELECT t4015.c901_level_value lvl_id
	            , get_code_name(t4015.c901_level_value) lvl_name
	            , get_code_name(t4016.c901_level_value) inventory
	            , t4015.c901_level_id lvl_hierarchy 
	            , Decode(t4015.c901_level_id,102580,1,102581,Decode(t4016.c901_level_value,100823,2,3),4) seq
	        FROM t4015_global_sheet_mapping t4015
	           , t4016_global_inventory_mapping T4016 
	       WHERE t4015.c901_company_id = p_company_id -- Globus/Algea
	         AND t4015.c4015_void_fl IS NULL
	         AND t4015.c4016_global_inventory_map_id = t4016.c4016_global_inventory_map_id
	         AND t4016.c4016_void_fl IS NULL 
	    ORDER BY seq,UPPER(lvl_name);
END gm_fc_fch_ttplevel;
--
/*****************************************************************
 * Description : Procedure to get company id for the TTP ID
 * Author 	 : Elango
 *****************************************************************/

FUNCTION get_ttp_company_id (
		p_ttp_id   IN   t4050_ttp.C4050_TTP_ID%TYPE
	)
		RETURN VARCHAR2
	IS
		v_company_id  VARCHAR2(20);
	BEGIN
		SELECT C901_COMPANY_ID 
		  INTO v_company_id
		  FROM t4020_demand_master  
         WHERE c901_demand_type  IN (4000103,40023)
           AND c4020_demand_master_id IN ( 
          								    SELECT C4020_DEMAND_MASTER_ID 
	                                          FROM t4051_ttp_demand_mapping 
	                                         WHERE C4050_TTP_ID = p_ttp_id 
	                                           AND ROWNUM=1 )
           AND c4020_void_fl IS NULL;
		RETURN v_company_id;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END get_ttp_company_id;
	
END gm_pkg_oppr_ttp;
/
