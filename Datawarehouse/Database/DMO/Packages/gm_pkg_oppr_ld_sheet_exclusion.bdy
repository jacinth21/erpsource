--@"c:\database\packages\operations\purchasing\gm_pkg_oppr_ld_sheet_exclusion.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ld_sheet_exclusion
IS
    /*******************************************************
   * Description : Procedure used to check if the Demand Sheet
   * is having any Demand or Forecast and mark the Sheet for Exclusion for main order planning job.
   * This job runs at 10PM on last day of the month.
   *******************************************************/
PROCEDURE gm_op_ld_orderplanning_main
AS
--
   CURSOR template_cursor
   IS
        SELECT t4020.c4020_demand_master_id, t4020.c4020_demand_nm 
          FROM t4020_demand_master t4020
         WHERE t4020.c4020_parent_demand_master_id IS NULL
		   AND t4020.c4020_inactive_fl IS NULL
           AND t4020.c4020_inactive_fl IS NULL; 
        
	CURSOR dmaster_cursor(v_demand_master_id t4020_demand_master.c4020_demand_master_id%TYPE )
	IS
		SELECT c4020_demand_master_id dmd_mast_id, C901_DEMAND_TYPE sheet_type  
		, c4020_demand_period dmd_period , C901_LEVEL_ID level_id, C901_LEVEL_VALUE level_val
		, C4020_FORECAST_PERIOD fcst_period
		  FROM t4020_demand_master t4020 , t4015_global_sheet_mapping t4015
		 WHERE t4020.c4020_void_fl IS NULL AND t4020.c4020_inactive_fl IS NULL
		   AND t4015.C4015_GLOBAL_SHEET_MAP_ID = t4020.C4015_GLOBAL_SHEET_MAP_ID		   
		   AND t4015.c901_access_type = '102623'-- Editable Sheet
		   AND t4015.c901_level_value <> '100823' -- Exclude US Sheet.
           AND t4020.c4020_parent_demand_master_id = v_demand_master_id ;

	--
	v_demandmasterid VARCHAR2 (20);
	v_start_time   DATE;
	v_count 	   NUMBER;
    v_daily_gop_job  VARCHAR2(10);
    v_sheet_type   t4020_demand_master.C901_DEMAND_TYPE%TYPE;
    v_dmd_from_period  DATE;
	v_dmd_to_period    DATE;
	v_fcst_from_period  DATE;
	v_fcst_to_period    DATE;
	v_dmd_period   t4020_demand_master.c4020_demand_period%type;	
	v_level_id	   t4015_global_sheet_mapping.C901_LEVEL_ID%TYPE;
	v_level_val	   t4015_global_sheet_mapping.C901_LEVEL_VALUE%TYPE;
	v_fcst_period  t4020_demand_master.C4020_FORECAST_PERIOD%TYPE;
	
	v_sales_demand 		NUMBER;
	v_sales_forecast 	NUMBER;
	v_consign_demand	NUMBER;
	V_CONSIGN_FORECAST	NUMBER;
	v_open_request		NUMBER;
	v_inhouse_demand	NUMBER;
	v_exclude_dmd_master VARCHAR2(1);
	v_demand_sheet_id    t4040_demand_sheet.C4040_DEMAND_SHEET_ID%TYPE;
	v_sheet_sum			NUMBER;
	v_positive_cnt NUMBER :=0;
	
--
BEGIN
--
	v_start_time 		:= SYSDATE;
	
	UPDATE T4020_DEMAND_MASTER SET c4020_exclude_ld_fl = '',C4020_LAST_UPDATED_DATE=SYSDATE WHERE c4020_exclude_ld_fl IS NOT NULL;
    
	--SELECT s4040_demand_sheet.NEXTVAL INTO v_demand_sheet_id FROM DUAL;
	v_demand_sheet_id :='EXCLUDE';
	
	FOR template_val IN template_cursor
	LOOP    	
		
    	FOR dmaster_val IN dmaster_cursor(template_val.c4020_demand_master_id)
    	LOOP
    		--
    		v_demandmasterid := dmaster_val.dmd_mast_id;
    		v_sheet_type 	:= dmaster_val.sheet_type;
    		v_dmd_period	:= dmaster_val.dmd_period;
    		v_fcst_period   := dmaster_val.fcst_period;
    		v_level_id	    := dmaster_val.level_id;
    		v_level_val	    := dmaster_val.level_val;
    		
    		
    		gm_pkg_oppr_ld_demand.gm_op_calc_period (v_dmd_period, 'Y', 'D', v_dmd_from_period, v_dmd_to_period);
    		gm_pkg_oppr_ld_demand.gm_op_calc_period (v_fcst_period, '', 'F', v_fcst_from_period, v_fcst_to_period);
    		
    		
    		v_exclude_dmd_master := '';
    		
    		delete from MY_TEMP_LIST;    	
    		gm_pkg_oppr_ld_demand.gm_op_sav_sheet_main (v_demandmasterid, v_demand_sheet_id, '', '', '', '', '', '', '', '', '', '', '' , '', '','30301','');
    				
    		IF v_sheet_type = '40020' -- sales
    		THEN
    			
    			gm_pkg_oppr_ld_sales_demand.gm_op_ld_account_info(v_demandmasterid);
    			
    			--Demand
    			gm_pkg_oppr_ld_sales_demand.gm_op_ld_demand (v_demandmasterid, v_demand_sheet_id, v_dmd_from_period, v_dmd_to_period);
    			
    			--Forecast
	   			gm_pkg_oppr_ld_sales_demand.gm_op_ld_forecast (v_demandmasterid, v_demand_sheet_id, v_fcst_from_period, v_fcst_to_period);
    			
    		ELSIF v_sheet_type='40021' -- Consign
    		THEN
    			
    			gm_pkg_oppr_ld_consign_demand.gm_op_ld_set_info(v_demandmasterid, v_level_id, v_level_val);
				gm_pkg_oppr_ld_consign_demand.gm_op_ld_distributor_info(v_demandmasterid,v_level_id, v_level_val );
				
				--Demand
				gm_pkg_oppr_ld_consign_demand.gm_op_ld_demand_fa_set (v_demandmasterid, v_demand_sheet_id, v_dmd_from_period, v_dmd_to_period);
        		gm_pkg_oppr_ld_consign_demand.gm_op_ld_demand_fa_item (v_demandmasterid, v_demand_sheet_id, v_dmd_from_period, v_dmd_to_period);
        		
                GM_PKG_OPPR_LD_DEMAND.GM_OP_LD_DEMAND_CALC_AVG (V_DEMAND_SHEET_ID, V_DMD_FROM_PERIOD);
                
        		--Forecast
        		gm_pkg_oppr_ld_consign_demand.gm_op_ld_forecast (v_demandmasterid, v_demand_sheet_id, v_fcst_from_period, v_fcst_to_period) ;
        		
        		--To Enable it later,(after the inventory load is done),so we will have the latest inventory id to run this procedure.
        		--gm_pkg_oppr_ld_consign_demand.gm_op_ld_pending_request (v_demandmasterid, v_demand_sheet_id, v_fcst_from_period, p_inventory_id);
        		gm_pkg_oppr_ld_consign_demand.gm_op_ld_otn(v_demandmasterid, v_demand_sheet_id, v_fcst_from_period) ;

			ELSE  --Inhouse
			
				--Demand
				gm_pkg_oppr_ld_consign_demand.gm_op_ld_demand_inhosue_set (v_demandmasterid, v_demand_sheet_id, v_dmd_from_period, v_dmd_to_period);
        		gm_pkg_oppr_ld_consign_demand.gm_op_ld_demand_inhouse_item (v_demandmasterid, v_demand_sheet_id, v_dmd_from_period, v_dmd_to_period);
        		gm_pkg_oppr_ld_consign_demand.gm_op_ld_demand_lnr_writeoff(v_demandmasterid,v_demand_sheet_id,v_dmd_from_period,v_dmd_to_period);
        		
        		--Forecast
        		gm_pkg_oppr_ld_consign_demand.gm_op_ld_forecast (v_demandmasterid, v_demand_sheet_id, v_fcst_from_period, v_fcst_to_period) ;
    			
    		END IF;
    		
    		SELECT COUNT(1) INTO v_positive_cnt
			FROM t4020_demand_master t4020 , t4040_demand_sheet t4040, my_temp_demand_sheet_detail t4042
			WHERE t4020.c4020_demand_master_id = t4040.c4020_demand_master_id
			AND t4040.c4040_demand_sheet_id =t4042.c4040_demand_sheet_id
			AND t4042.c4042_qty <> 0
			AND t4040.c4040_demand_SHEET_id=v_demand_sheet_id;
    		
    		
    		SELECT sum(t4042.c4042_qty) INTO v_sheet_sum
			FROM t4020_demand_master t4020 , t4040_demand_sheet t4040, my_temp_demand_sheet_detail t4042
			WHERE t4020.c4020_demand_master_id = t4040.c4020_demand_master_id
			AND t4040.c4040_demand_sheet_id =t4042.c4040_demand_sheet_id
			AND t4040.c4040_demand_SHEET_id=v_demand_sheet_id;
	   			
				IF (v_sheet_sum = 0 AND v_positive_cnt = 0)
    			THEN
    				v_exclude_dmd_master := 'Y';
    			END IF;
    			
    		DELETE FROM t4040_demand_sheet WHERE c4040_demand_SHEET_id=v_demand_sheet_id;
			DELETE FROM my_temp_demand_sheet_detail WHERE c4040_demand_SHEET_id=v_demand_sheet_id;

    		
    		IF v_exclude_dmd_master='Y'
    		THEN
    		
    			UPDATE T4020_DEMAND_MASTER SET c4020_exclude_ld_fl = 'Y',C4020_LAST_UPDATED_DATE=SYSDATE WHERE c4020_demand_master_id=v_demandmasterid;
    			v_exclude_dmd_master := '';
    			
    			INSERT INTO T4024_DMD_MASTER_LD_EXCLUSION (C4024_EXCLUSION_ID,C4020_DEMAND_MASTER_ID,C4020_CREATED_BY,C4020_CREATED_DATE)
    			VALUES (S4024_DMD_MASTER_LD_EXCLUSION.NEXTVAL,v_demandmasterid,'30301',SYSDATE);
    		END IF;
	    	commit;	
    	--
    	END LOOP;
          
    END LOOP;
		
	--
	COMMIT;
	EXCEPTION	WHEN OTHERS
	THEN
		ROLLBACK;
END gm_op_ld_orderplanning_main;


	/*************************************************************************
	 * Purpose: This Function will check if there is any sales for the Specificied 
	 * period for the demand sheet.
	 *************************************************************************/
	FUNCTION gm_chk_sales_demand (
	    p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_from_period		IN	 DATE
	  , p_to_period 		IN	 DATE
	) RETURN NUMBER
	AS
		v_sales_cnt NUMBER;
	BEGIN
		--
			SELECT	 COUNT(1) INTO v_sales_cnt
				FROM (SELECT t4011.c4010_group_id, t4011.c205_part_number_id, v9001.month_value, t4011.c901_part_pricing_type price_type
						FROM t4010_group t4010
						   , t4021_demand_mapping t4021
						   , t4011_group_detail t4011
						   , v9001_month_list v9001
					   WHERE t4021.c4020_demand_master_id = p_demand_id
						 AND t4021.c901_ref_type = 40030
						 AND t4021.c4021_ref_id = t4010.c4010_group_id
						 AND t4010.c4010_group_id = t4011.c4010_group_id
						 AND v9001.month_value >= to_date('05/01/2018','MM/DD/YYYY')
						 AND v9001.month_value <= p_to_period
						 ) partgrpinfo
				   , (SELECT TRUNC(t501.c501_order_date,'month') order_date
						   , t502.c205_part_number_id, t502.c502_item_qty
						FROM my_temp_list mtl
						   , t501_order t501
						   , t502_item_order t502
					   WHERE t501.c704_account_id = mtl.my_temp_txn_id
						 AND t501.c501_order_date >= p_from_period
						 AND t501.c501_order_date <= p_to_period
						 AND t501.c501_order_id = t502.c501_order_id
						 AND t501.c501_void_fl IS NULL
						 AND NVL (t501.c901_order_type, -999) <> 2524
						 AND NVL (c901_order_type, -9999) NOT IN (SELECT TO_NUMBER(c906_rule_value) FROM v901_order_type_grp)
					   ) partorderinfo
			   WHERE partgrpinfo.month_value = partorderinfo.order_date
			   AND partgrpinfo.c205_part_number_id = partorderinfo.c205_part_number_id;

	
	RETURN v_sales_cnt;

	END gm_chk_sales_demand;
	
	
	/*************************************************************************
	 * Purpose: This function will check if the Demand Master is having any growth or not
	 *************************************************************************/
	FUNCTION gm_chk_sales_forecast (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_from_period		IN	 DATE
	  , p_to_period 		IN	 DATE
	) RETURN NUMBER
	IS
		--
		v_history_fl            CHAR (1);
        v_comments_fl           CHAR (1);
        v_weighted_avg_diff     t4042_demand_sheet_detail.c4042_weighted_avg_diff_qty%TYPE;
        v_current_qty           t4042_demand_sheet_detail.c4042_qty%TYPE;
	v_sales_part_forecast_cnt    NUMBER;
	v_sales_grp_forecast_cnt    NUMBER;
    	--
	BEGIN

			SELECT COUNT(1) INTO v_sales_grp_forecast_cnt    
				FROM (SELECT t4030.c4030_ref_id GROUP_ID, t4031.c4031_start_date, t4031.c4031_end_date
						   , t4031.c901_ref_type, t4031.c4031_value
                           , NULL comments_fl
						FROM t4020_demand_master t4020, t4030_demand_growth_mapping t4030, t4031_growth_details t4031
					   WHERE t4020.c4020_demand_master_id = p_demand_id
						 AND t4020.c4020_demand_master_id = t4030.c4020_demand_master_id
						 AND t4031.c4030_demand_growth_id = t4030.c4030_demand_growth_id
						 AND t4030.c901_ref_type = 20297
						 AND t4031.c4031_start_date >= p_from_period
						 AND t4031.c4031_end_date <= p_to_period
						 AND t4030.c4030_void_fl IS NULL
                         AND t4031.c4031_void_fl IS NULL
						 AND t4031.c4031_value IS NOT NULL) ;
						 
                   -- Fetch Part Level override info      
				SELECT COUNT(1) INTO v_sales_part_forecast_cnt    
				FROM	   
				(SELECT t4030.c4030_ref_id part_number, t4031.c4031_start_date, t4031.c4031_end_date
						   , t4031.c901_ref_type, t4031.c4031_value
                           , NULL comments_fl
						FROM t4020_demand_master t4020, t4030_demand_growth_mapping t4030, t4031_growth_details t4031
					   WHERE t4020.c4020_demand_master_id = p_demand_id
						 AND t4020.c4020_demand_master_id = t4030.c4020_demand_master_id
						 AND t4031.c4030_demand_growth_id = t4030.c4030_demand_growth_id
						 AND t4030.c901_ref_type = 20295
						 AND t4031.c4031_start_date >= p_from_period
						 AND t4031.c4031_end_date <= p_to_period
						 AND t4030.c4030_void_fl IS NULL
                         AND t4031.c4031_void_fl IS NULL
						 AND t4031.c4031_value IS NOT NULL) ;
	RETURN v_sales_part_forecast_cnt+v_sales_grp_forecast_cnt;
		
	END gm_chk_sales_forecast;
	
	/*************************************************************************
	 * Purpose: This Function will check if there is any Consignment demand for the Specificied 
	 * period for the demand sheet.
	 *************************************************************************/
	
	FUNCTION gm_chk_consign_demand (
        p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_from_period     IN DATE,
        p_to_period       IN DATE) RETURN NUMBER
	IS
	
	v_consign_set_demand NUMBER;
	v_consign_item_demand NUMBER;
	
	
	BEGIN
		
		    SELECT COUNT(1) INTO v_consign_set_demand   
			FROM (
        SELECT partinfo.setid, partinfo.month_value, NVL (c_qty, 0) -  NVL (r_qty, 0) cons_qty
      FROM (SELECT setlist.my_temp_txn_id setid, v9001.month_value
              FROM my_temp_key_value setlist, v9001_month_list v9001
             WHERE  setlist.my_temp_txn_key = 'SET'
               AND v9001.month_value >= p_from_period
               AND v9001.month_value <= p_to_period
                ORDER BY setlist.my_temp_txn_id,v9001.month_value
               ) partinfo,
           (SELECT   t504.c207_set_id setid,
                     TRUNC(t504.c504_ship_date,'month')  sdate,
                     COUNT (1) c_qty
                FROM t504_consignment t504,
                     my_temp_list distlist,
                     my_temp_key_value setlist
               WHERE t504.c701_distributor_id IS NOT NULL
                 AND t504.c701_distributor_id = distlist.my_temp_txn_id
                 AND t504.c207_set_id IS NOT NULL
                 AND t504.C5040_plant_id IN  (
			                SELECT my_temp_txn_id FROM my_temp_key_value
			                 WHERE my_temp_txn_key = 'RGN_COMP_PLANT_FLAG')
                 AND t504.c207_set_id = setlist.my_temp_txn_id
                 AND setlist.my_temp_txn_key = 'SET'
                 -- Remove the consignment transfers
                 AND NOT EXISTS (
                        SELECT c921_ref_id
                          FROM t920_transfer t920, t921_transfer_detail t921
                         WHERE t920.c920_transfer_id = t921.c920_transfer_id
                           AND c921_ref_id = t504.c504_consignment_id
                           AND t920.c920_void_fl IS NULL
                           AND t920.c920_transfer_date >= p_from_period
                           AND t920.c920_transfer_date <= p_to_period
                           AND t921.c901_link_type = 90360)
                 AND t504.c504_ship_date >= p_from_period
                 AND t504.c504_ship_date <= p_to_period
                 AND t504.c504_status_fl = '4'
                 AND t504.c504_type = 4110
                 AND t504.c504_void_fl IS NULL
            GROUP BY t504.c207_set_id, TRUNC(t504.c504_ship_date,'month')) consinfo,
           (SELECT   t506.c207_set_id setid,
                     TRUNC(t506.c506_credit_date,'month') sdate,
                     COUNT (1) r_qty
                FROM t506_returns t506,
                     my_temp_list distlist,
                     my_temp_key_value setlist
               WHERE t506.c701_distributor_id IS NOT NULL
                 AND t506.c701_distributor_id = distlist.my_temp_txn_id
                 AND t506.c506_void_fl IS NULL
                 AND t506.c506_status_fl = '2'
                 AND t506.c207_set_id IS NOT NULL
                 AND t506.c207_set_id = setlist.my_temp_txn_id
                 AND setlist.my_temp_txn_key = 'SET'
                 AND t506.C5040_plant_id IN  (
			                SELECT my_temp_txn_id FROM my_temp_key_value
			                 WHERE my_temp_txn_key = 'RGN_COMP_PLANT_FLAG')
                 AND NOT EXISTS (
                        SELECT c921_ref_id
                          FROM t920_transfer t920, t921_transfer_detail t921
                         WHERE t920.c920_transfer_id = t921.c920_transfer_id
                           AND c921_ref_id = t506.c506_rma_id
                           AND t920.c920_void_fl IS NULL
                           AND t920.c920_transfer_date >= p_from_period
                           AND t920.c920_transfer_date <= p_to_period
                           AND t921.c901_link_type = 90361)
                 AND t506.c506_credit_date >= p_from_period
                 AND t506.c506_credit_date <= p_to_period
            GROUP BY t506.c207_set_id, TRUNC(t506.c506_credit_date,'month')) returninfo
     WHERE partinfo.month_value = consinfo.sdate
       AND partinfo.setid = consinfo.setid
       AND partinfo.month_value = returninfo.sdate
       AND partinfo.setid = returninfo.setid );
    
    -- ******************************
    -- Only Set Part Details 
    -- ******************************
   SELECT COUNT(1) INTO v_consign_item_demand   
		FROM (
			SELECT partinfo.p_num, partinfo.month_value,  NVL (c_qty, 0) -  NVL (r_qty, 0) cons_qty
          FROM (SELECT DISTINCT setpartlist.my_temp_txn_id p_num, v9001.month_value
                  FROM my_temp_key_value setpartlist, v9001_month_list v9001
                 WHERE setpartlist.my_temp_txn_key = 'ITEM'
                   AND v9001.month_value >= p_from_period
                   AND v9001.month_value <= p_to_period
                   ORDER BY setpartlist.my_temp_txn_id,v9001.month_value
                   ) partinfo,
               (SELECT   t505.c205_part_number_id p_num,
                         TRUNC(t504.c504_ship_date,'month')sdate,
                         SUM (t505.c505_item_qty) c_qty
                    FROM t504_consignment t504,
                         t505_item_consignment t505, 
                         my_temp_list distlist
                   WHERE t504.c701_distributor_id IS NOT NULL
                     AND t504.c701_distributor_id = distlist.my_temp_txn_id
                     AND t504.c504_consignment_id  = t505.c504_consignment_id
                     AND t504.c207_set_id IS NULL
                     -- Remove excess from item consignent 
                     AND NOT EXISTS
                        (
                             SELECT a.c504_consignment_id a
                               FROM t504a_consignment_excess a
                              WHERE a.c504_consignment_id = t504.c504_consignment_id
                        )             
                     -- Remove the consignment transfers
                     AND NOT EXISTS (
                            SELECT c921_ref_id
                              FROM t920_transfer t920, t921_transfer_detail t921
                             WHERE t920.c920_transfer_id = t921.c920_transfer_id
                               AND c921_ref_id = t504.c504_consignment_id
                               AND t920.c920_void_fl IS NULL
                               AND t920.c920_transfer_date >= p_from_period
                               AND t920.c920_transfer_date <= p_to_period
                               AND t921.c901_link_type = 90360)
                     AND t504.c504_ship_date >= p_from_period
                     AND t504.c504_ship_date <= p_to_period
                     AND t504.c504_status_fl = '4'
                     AND t504.c504_type = 4110
                     AND t504.C5040_plant_id IN  (
			                SELECT my_temp_txn_id FROM my_temp_key_value
			                 WHERE my_temp_txn_key = 'RGN_COMP_PLANT_FLAG')                     
                     AND t504.c504_void_fl IS NULL
                     AND TRIM (t505.c505_control_number) IS NOT NULL
                     AND (t505.c901_type   IS NULL
                        OR t505.c901_type                   <> 100880)
                GROUP BY t505.c205_part_number_id, TRUNC(t504.c504_ship_date,'month')) consinfo
                , (SELECT   t507.c205_part_number_id p_num, TRUNC(t506.c506_credit_date,'month') sdate,                          
                         SUM (t507.c507_item_qty )  r_qty
                    FROM t506_returns t506,
                         t507_returns_item t507, 
                         my_temp_list distlist
                   WHERE t506.c701_distributor_id IS NOT NULL
                     AND t506.c701_distributor_id = distlist.my_temp_txn_id
                     AND t506.c506_void_fl IS NULL
                     AND t506.c506_status_fl = '2'
                     AND t506.c207_set_id IS NULL
                     AND t506.c506_rma_id = t507.c506_rma_id
                     --Need to take the RA that is Credited (Exclude Write-off)
                     --AND t507.c507_status_fl IN ('C', 'W')
                     AND t507.c507_status_fl = 'C'
                     --Exclude RA Created from FA.
                     AND (t506.C901_TXN_SOURCE is NULL OR t506.C901_TXN_SOURCE <>'102680')
                     AND t506.C5040_plant_id IN  (
			                SELECT my_temp_txn_id FROM my_temp_key_value
			                 WHERE my_temp_txn_key = 'RGN_COMP_PLANT_FLAG')                     
                     AND NOT EXISTS (
                            SELECT c921_ref_id
                              FROM t920_transfer t920, t921_transfer_detail t921
                             WHERE t920.c920_transfer_id = t921.c920_transfer_id
                               AND c921_ref_id = t506.c506_rma_id
                               AND t920.c920_void_fl IS NULL
                               AND t920.c920_transfer_date >= p_from_period
                               AND t920.c920_transfer_date <= p_to_period
                               AND t921.c901_link_type = 90361)
                     AND t506.c506_credit_date >= p_from_period
                     AND t506.c506_credit_date <= p_to_period
                GROUP BY t507.c205_part_number_id, TRUNC(t506.c506_credit_date,'month')) returninfo
         WHERE partinfo.month_value = consinfo.sdate
           AND partinfo.p_num = consinfo.p_num 
           AND partinfo.month_value = returninfo.sdate
           AND partinfo.p_num = returninfo.p_num);
			
	        RETURN v_consign_item_demand + v_consign_set_demand;
	    --
	END gm_chk_consign_demand;
	
	/*************************************************************************
	 * Purpose: This Function will check if there is any In-House Demand for the Specificied 
	 * period for the demand sheet.
	 *************************************************************************/
	FUNCTION gm_chk_inhouse_demand (
        p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_from_period     IN DATE,
        p_to_period       IN DATE)RETURN NUMBER
	IS
	 	v_inhouse_set_demand NUMBER;
		v_inhouse_item_demand NUMBER;
	BEGIN
	
		SELECT COUNT(1) INTO v_inhouse_set_demand FROM (
	         SELECT partinfo.c207_set_id, partinfo.c205_part_number_id, partinfo.month_value
	          , NVL (c_qty, 0) item_qty
	           FROM
	            (
	                SELECT DISTINCT t208.c207_set_id, t208.c205_part_number_id, v9001.month_value
	                   FROM t208_set_details t208, t4021_demand_mapping t4021, v9001_month_list v9001
	                  WHERE t4021.c4020_demand_master_id = p_demand_id
	                    AND t4021.c901_ref_type          = 40031
	                    AND t208.c208_inset_fl           = 'Y'
	                    AND t4021.c4021_ref_id           = t208.c207_set_id
	                    AND v9001.month_value           >= p_from_period
	                    AND v9001.month_value           <= p_to_period
	                    AND t208.c208_void_fl           IS NULL
	            )
	        partinfo, (
	             SELECT t504.c207_set_id setid, TRUNC(t504.c504_ship_date,'month')
	                sdate, t505.c205_part_number_id p_num, SUM (t505.c505_item_qty) c_qty
	               FROM t504_consignment t504, t505_item_consignment t505, (
	                     SELECT t4021.c4021_ref_id
	                       FROM t4021_demand_mapping t4021
	                      WHERE t4021.c4020_demand_master_id = p_demand_id
	                        AND t4021.c901_ref_type          = 40031
	                )
	                setlist
	              WHERE t504.c701_distributor_id IS NULL
	                AND t504.c504_consignment_id  = t505.c504_consignment_id
	                AND t504.c704_account_id      = '01'
	                AND t504.c207_set_id         IS NOT NULL
	                AND t504.c207_set_id          = setlist.c4021_ref_id
	                -- Remove the consignment transfers
	                AND NOT EXISTS
	                (
	                     SELECT c921_ref_id
	                       FROM t920_transfer t920, t921_transfer_detail t921
	                      WHERE t920.c920_transfer_id    = t921.c920_transfer_id
	                        AND c921_ref_id              = t504.c504_consignment_id
	                        AND t920.c920_void_fl       IS NULL
	                        AND t920.c920_transfer_date >= p_from_period
	                        AND t920.c920_transfer_date <= p_to_period
	                        AND t921.c901_link_type      = 90360
	                )
	                AND t504.c504_ship_date             >= p_from_period
	                AND t504.c504_ship_date             <= p_to_period
	                AND t504.c504_status_fl              = '4'
	                AND TRIM (t505.c505_control_number) IS NOT NULL
	                AND t504.c504_void_fl               IS NULL
	           GROUP BY t504.c207_set_id, t505.c205_part_number_id, TRUNC(t504.c504_ship_date,'month')
	        )
	        consinfo
	      WHERE partinfo.c205_part_number_id = consinfo.p_num
	        AND partinfo.month_value         = consinfo.sdate
	        AND partinfo.c207_set_id         = consinfo.setid
	        );
	        
	   SELECT COUNT(1) INTO v_inhouse_item_demand FROM (
	   SELECT partinfo.c205_part_number_id, partinfo.month_value, NVL (c_qty, 0) item_qty
	           FROM
	            (
	                SELECT DISTINCT t208.c205_part_number_id, v9001.month_value
	                   FROM t208_set_details t208, t4021_demand_mapping t4021, v9001_month_list v9001
	                  WHERE t4021.c4020_demand_master_id = p_demand_id
	                    AND t4021.c901_ref_type          = 40031
	                    AND t4021.c4021_ref_id           = t208.c207_set_id
	                    AND v9001.month_value           >= p_from_period
	                    AND v9001.month_value           <= p_to_period
	                    AND t208.c208_void_fl           IS NULL
	            )
	        partinfo, (
	             SELECT TRUNC(t504.c504_ship_date,'month') sdate,
	                t505.c205_part_number_id p_num, SUM (t505.c505_item_qty) c_qty
	               FROM t504_consignment t504, t505_item_consignment t505
	              WHERE t504.c701_distributor_id IS NULL
	                AND t504.c504_consignment_id  = t505.c504_consignment_id
	                AND t504.c704_account_id      = '01'
	                AND t504.c207_set_id         IS NULL
	                -- Remove the consignment transfers
	                AND NOT EXISTS
	                (
	                     SELECT c921_ref_id
	                       FROM t920_transfer t920, t921_transfer_detail t921
	                      WHERE t920.c920_transfer_id    = t921.c920_transfer_id
	                        AND c921_ref_id              = t504.c504_consignment_id
	                        AND t920.c920_void_fl       IS NULL
	                        AND t920.c920_transfer_date >= p_from_period
	                        AND t920.c920_transfer_date <= p_to_period
	                        AND t921.c901_link_type      = 90360
	                )
	                AND t504.c504_ship_date             >= p_from_period
	                AND t504.c504_ship_date             <= p_to_period
	                AND t504.c504_status_fl              = '4'
	                AND TRIM (t505.c505_control_number) IS NOT NULL
	                AND t504.c504_void_fl               IS NULL
	           GROUP BY t505.c205_part_number_id, TRUNC(t504.c504_ship_date,'month')
	        )
	        consinfo
	      WHERE partinfo.c205_part_number_id = consinfo.p_num
	        AND partinfo.month_value         = consinfo.sdate);
		
	      RETURN v_inhouse_item_demand + v_inhouse_set_demand;
	END gm_chk_inhouse_demand;
	
		/*************************************************************************
	 * Purpose: This Function will check if there is any Growth (Set) for the demand Master.
	 *************************************************************************/
	FUNCTION gm_chk_consign_forecast (
        p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_from_period     IN DATE,
        p_to_period       IN DATE) RETURN NUMBER
	IS
		v_set_frcst_cnt NUMBER;
		v_item_frcst_cnt NUMBER;
		v_par_cnt 		 NUMBER;
	BEGIN
	    --
	    SELECT COUNT(1) INTO v_set_frcst_cnt FROM
	    (SELECT partinfo.c207_set_id groupid, partinfo.c205_part_number_id, partinfo.month_value
	          , (partinfo.c208_set_qty * setinfo.c4031_value) item_qty
	           FROM
	            (
	                 SELECT t208.c207_set_id, t208.c205_part_number_id, v9001.month_value
	                  , t208.c208_set_qty
	                   FROM t208_set_details t208, t4021_demand_mapping t4021, v9001_month_list v9001
	                  WHERE t4021.c4020_demand_master_id = p_demand_id
	                    AND t4021.c901_ref_type          = 40031
	                    AND t4021.c4021_ref_id           = t208.c207_set_id
	                    AND t208.c208_inset_fl           = 'Y'
	                    AND t208.c208_set_qty           <> 0
	                    AND v9001.month_value           >= p_from_period
	                    AND v9001.month_value           <= p_to_period
	                    AND t208.c208_void_fl           IS NULL
	            )
	        partinfo, (
	            SELECT DISTINCT t4030.c4030_ref_id set_id, t4031.c4031_start_date, t4031.c4031_end_date
	              , t4031.c901_ref_type, t4031.c4031_value
	               FROM t4020_demand_master t4020, t4030_demand_growth_mapping t4030, t4031_growth_details t4031
	              WHERE t4020.c4020_demand_master_id = p_demand_id
	                AND t4020.c4020_demand_master_id = t4030.c4020_demand_master_id
	                AND t4031.c4030_demand_growth_id = t4030.c4030_demand_growth_id
	                AND t4030.c901_ref_type          = 20296
	                AND t4031.c4031_start_date      >= p_from_period
	                AND t4031.c4031_end_date        <= p_to_period
	                AND t4030.c4030_void_fl         IS NULL
	                AND t4031.c4031_value           IS NOT NULL
	        )
	        setinfo
	      WHERE partinfo.c207_set_id = setinfo.set_id
	        AND partinfo.month_value = setinfo.c4031_start_date);
	        
	    SELECT COUNT(1) INTO v_item_frcst_cnt FROM
	    (
	    	SELECT DISTINCT t4030.c4030_ref_id part_id, v9001.month_value
	           FROM t4020_demand_master t4020, t4030_demand_growth_mapping t4030, t4031_growth_details t4031
	          , v9001_month_list v9001
	          WHERE t4020.c4020_demand_master_id = p_demand_id
	            AND t4030.c901_ref_type          = 20298
	            AND t4020.c4020_demand_master_id = t4030.c4020_demand_master_id
	            AND t4031.c4030_demand_growth_id = t4030.c4030_demand_growth_id
	            AND v9001.month_value           >= p_from_period
	            AND v9001.month_value           <= p_to_period
	            AND v9001.month_value            = t4031.c4031_start_date
	            AND t4030.c4030_void_fl         IS NULL
	            AND t4031.c4031_value           IS NOT NULL
	    );
	    
	    SELECT COUNT(1) INTO v_par_cnt FROM
	    (SELECT DISTINCT c205_part_number_id part_id
	           FROM t4022_demand_par_detail t4022, v9001_month_list v9001
	          WHERE c4020_demand_master_id         = p_demand_id
	            AND v9001.month_value             >= p_from_period
	            AND v9001.month_value             <= p_to_period
		);
		
		RETURN v_set_frcst_cnt+v_item_frcst_cnt+v_par_cnt ;
		
	END gm_chk_consign_forecast;
	
		/*************************************************************************
	 * Purpose: This Function will check if there is any Request, OTN for the demand Master.
	 *************************************************************************/
	FUNCTION gm_chk_open_request (
        p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_from_period     IN DATE) RETURN NUMBER
	IS
		v_set_qty NUMBER;
		v_otn_qty NUMBER;
	BEGIN
		
		 SELECT COUNT(1) INTO v_set_qty 
           FROM t520_request t520
          WHERE t520.c520_request_txn_id     = p_demand_id
            AND t520.c901_request_source     = '50616'
            AND t520.c520_void_fl          IS NULL
            AND t520.c520_status_fl          > 0
            AND t520.c520_status_fl          < 40
            AND t520.c520_required_date      < p_from_period
            AND t520.c520_master_request_id IS NULL;
            
            SELECT count(1) into v_otn_qty
		    FROM t4023_demand_otn_detail t4023
		    WHERE t4023.c4020_demand_master_id = p_demand_id
		    AND C4023_VOID_FL IS NULL;
		    
		    return v_set_qty+v_otn_qty;
		
	END gm_chk_open_request;
END gm_pkg_oppr_ld_sheet_exclusion;
/