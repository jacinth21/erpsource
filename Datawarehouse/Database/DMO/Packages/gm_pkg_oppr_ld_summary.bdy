create or replace PACKAGE BODY gm_pkg_oppr_ld_summary
IS

 PROCEDURE gm_op_ld_summary_main (
         p_ttp_detail_id 	IN  T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE
       , p_demand_mas_id 	IN	t4020_demand_master.c4020_demand_master_id%TYPE
       , p_request_by   	IN	t4020_demand_master.c4020_created_by%TYPE
    )
    AS

    v_delete_ttp_summary VARCHAR2(10);
    v_ttp_detail_id T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE;
    v_demand_type t4040_demand_sheet.c901_demand_type%TYPE;

    Cursor cur_ttp_dmdsheet
    IS

		SELECT (CASE WHEN C901_DEMAND_TYPE=40023 THEN 102580 ELSE c901_level_id END) lvl_id,
		(CASE WHEN C901_DEMAND_TYPE=40023 THEN nvl(c901_level_value,100800) ELSE c901_level_value END) lvl_value ,
		C250_INVENTORY_LOCK_ID inv_lck_id,
		C4040_DEMAND_PERIOD_DT load_dt
		FROM T4040_DEMAND_SHEET
		WHERE C4052_TTP_DETAIL_ID=v_ttp_detail_id
		GROUP BY (CASE WHEN C901_DEMAND_TYPE=40023 THEN 102580 ELSE c901_level_id END),
		(CASE WHEN C901_DEMAND_TYPE=40023 THEN nvl(c901_level_value,100800) ELSE c901_level_value END),
		C4040_DEMAND_PERIOD_DT,C250_INVENTORY_LOCK_ID
		ORDER BY lvl_id DESC;

    BEGIN

	    if p_ttp_detail_id IS NOT NULL
	    THEN
	    	v_ttp_detail_id := p_ttp_detail_id;
	    ELSE
	    	BEGIN
		    	SELECT C4052_TTP_DETAIL_ID
		    	INTO v_ttp_detail_id
		    	FROM T4040_DEMAND_SHEET
		    	WHERE
		    	C4040_DEMAND_PERIOD_DT = TO_DATE (TO_CHAR (CURRENT_DATE, 'MM/YYYY'), 'MM/YYYY')
		    	AND c4020_demand_master_id = p_demand_mas_id;
		    EXCEPTION WHEN OTHERS
		    THEN
		    	v_ttp_detail_id :='';
		    END;
	    END IF;

	    DBMS_OUTPUT.PUT_LINE('v_ttp_detail_id:'||v_ttp_detail_id);

	    UPDATE T4055_TTP_SUMMARY SET C4055_VOID_FL='Y',C4055_UPDATED_DATE=CURRENT_DATE,
				C4055_UPDATED_BY=p_request_by
		WHERE C4052_TTP_DETAIL_ID = v_ttp_detail_id
		AND C4055_VOID_FL IS NULL;

		SELECT GET_RULE_VALUE('DELETE_TTP_SUMMARY','ORDERPLANNING')
		INTO v_delete_ttp_summary
		FROM DUAL;

		IF v_delete_ttp_summary='Y'
		THEN
			DELETE FROM T4055_TTP_SUMMARY
			WHERE C4052_TTP_DETAIL_ID = v_ttp_detail_id;
		END IF;

		
		-- To get sheet type
		SELECT MAX(C901_DEMAND_TYPE)
				INTO v_demand_type
			FROM T4040_DEMAND_SHEET
			WHERE C4052_TTP_DETAIL_ID=p_ttp_detail_id
			AND C4040_VOID_FL       IS NULL;

				
		--DBMS_OUTPUT.PUT_LINE('gm_op_ld_summary_main :1'|| TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS')  );

	    FOR cur_ttp_dmdsheet_val IN cur_ttp_dmdsheet
		LOOP

			--DBMS_OUTPUT.PUT_LINE('Part Info b4 :'|| TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS')  );

			gm_pkg_oppr_ld_summary.gm_ld_part_info(v_ttp_detail_id,cur_ttp_dmdsheet_val.lvl_id,cur_ttp_dmdsheet_val.lvl_value );
		 	gm_pkg_oppr_ld_summary.gm_sav_part_info(v_ttp_detail_id,cur_ttp_dmdsheet_val.lvl_id,cur_ttp_dmdsheet_val.lvl_value, cur_ttp_dmdsheet_val.inv_lck_id
												  ,cur_ttp_dmdsheet_val.load_dt,p_request_by);

			DBMS_OUTPUT.PUT_LINE('Part Info after :'|| TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS')  );

			--DBMS_OUTPUT.PUT_LINE(' 1 Forecast Info before  :'|| TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS')  );
			gm_pkg_oppr_ld_summary.gm_ld_demand_sheet_id(v_ttp_detail_id,cur_ttp_dmdsheet_val.lvl_id,cur_ttp_dmdsheet_val.lvl_value);

			IF (v_demand_type=40023)
			THEN
				gm_pkg_oppr_ld_multipart_ttp.gm_ld_mp_forecast(v_ttp_detail_id,cur_ttp_dmdsheet_val.lvl_value, cur_ttp_dmdsheet_val.inv_lck_id
													 ,cur_ttp_dmdsheet_val.load_dt,p_request_by);
				ELSE									 
				gm_pkg_oppr_ld_summary.gm_ld_forecast(v_ttp_detail_id,cur_ttp_dmdsheet_val.lvl_value, cur_ttp_dmdsheet_val.inv_lck_id
													 ,cur_ttp_dmdsheet_val.load_dt,p_request_by);
			END IF;									 
			--DBMS_OUTPUT.PUT_LINE(' 2 Forecast Info after :'|| TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS')  );




			-- If Level ID is at company,load below values which is applicable for Company Level Only.
			if (cur_ttp_dmdsheet_val.lvl_id = 102580)
			THEN

				--TTP SUmmary Calculation for WW Level Only.


			--	DBMS_OUTPUT.PUT_LINE('Calling Grouping :1'|| TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS')  );
				gm_pkg_oppr_ld_summary.gm_ld_forecast_grouping(v_ttp_detail_id,cur_ttp_dmdsheet_val.lvl_value , cur_ttp_dmdsheet_val.inv_lck_id
												 ,cur_ttp_dmdsheet_val.load_dt,p_request_by);
			--	DBMS_OUTPUT.PUT_LINE('After Grouping :1'|| TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS')  );



				/* -- PAR is not required as the forecast is already including the PAR,so this will double the PAR value in the final calculation.
				gm_pkg_oppr_ld_summary.gm_upd_PAR(v_ttp_detail_id,cur_ttp_dmdsheet_val.lvl_value , cur_ttp_dmdsheet_val.inv_lck_id
												 ,cur_ttp_dmdsheet_val.load_dt,p_request_by);
				*/

				IF (v_demand_type=40023) 
				--40023 Mult part
				THEN
					/*  New Table Map */
					    gm_pkg_oppr_ld_multipart_ttp.gm_ld_mp_forecast_grouping(v_ttp_detail_id,cur_ttp_dmdsheet_val.lvl_value , cur_ttp_dmdsheet_val.inv_lck_id
												 ,cur_ttp_dmdsheet_val.load_dt,p_request_by);						
						-- This code for SET PAR update in TTP summary table only for MP
						gm_pkg_oppr_ld_summary.gm_ld_mp_SetPAR(v_ttp_detail_id,cur_ttp_dmdsheet_val.lvl_value , cur_ttp_dmdsheet_val.inv_lck_id
												 ,cur_ttp_dmdsheet_val.load_dt,p_request_by);
						-- This code for OTN update in TTP summary table only for MP
						gm_pkg_oppr_ld_summary.gm_ld_mp_OTN(v_ttp_detail_id,cur_ttp_dmdsheet_val.lvl_value , cur_ttp_dmdsheet_val.inv_lck_id
												 ,cur_ttp_dmdsheet_val.load_dt,p_request_by);
												 
						gm_pkg_oppr_ld_summary.gm_ld_mp_safety_stock(v_ttp_detail_id,cur_ttp_dmdsheet_val.lvl_value);
												 
						-- This procedure to handle PAR value <FCST qty
						gm_pkg_oppr_ld_summary.gm_upd_forecast_qty(v_ttp_detail_id,cur_ttp_dmdsheet_val.load_dt,p_request_by);
  												 
				ELSE
				
					gm_pkg_oppr_ld_summary.gm_ld_SetPAR(v_ttp_detail_id,cur_ttp_dmdsheet_val.lvl_value , cur_ttp_dmdsheet_val.inv_lck_id
													 ,cur_ttp_dmdsheet_val.load_dt,p_request_by);
				--	DBMS_OUTPUT.PUT_LINE('After Set Par :1'|| TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS')  );
	
	
					gm_pkg_oppr_ld_summary.gm_ld_OTN(v_ttp_detail_id,cur_ttp_dmdsheet_val.lvl_value , cur_ttp_dmdsheet_val.inv_lck_id
													 ,cur_ttp_dmdsheet_val.load_dt,p_request_by);
													 
					
					gm_pkg_oppr_ld_summary.gm_ld_safety_stock(v_ttp_detail_id,cur_ttp_dmdsheet_val.lvl_value);
					
				END IF;

			--	DBMS_OUTPUT.PUT_LINE('After OTN :1'|| TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS')  );

        		gm_pkg_oppr_ttp_summary_calc.gm_ttp_summary_calc_main(v_ttp_detail_id,cur_ttp_dmdsheet_val.lvl_id,cur_ttp_dmdsheet_val.inv_lck_id,cur_ttp_dmdsheet_val.lvl_value);

			--	DBMS_OUTPUT.PUT_LINE('b4  tot req:1'||cur_ttp_dmdsheet_val.inv_lck_id||':'||cur_ttp_dmdsheet_val.lvl_value);
				gm_pkg_oppr_ld_summary.gm_ld_total_required(v_ttp_detail_id,cur_ttp_dmdsheet_val.lvl_value , cur_ttp_dmdsheet_val.inv_lck_id
												 ,cur_ttp_dmdsheet_val.load_dt,p_request_by);
			--	DBMS_OUTPUT.PUT_LINE('After Tot Req :1'|| TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS')  );
				--gm_pkg_oppr_ld_summary.gm_ld_orderQty(v_ttp_detail_id,cur_ttp_dmdsheet_val.lvl_value , cur_ttp_dmdsheet_val.inv_lck_id
				--								 ,cur_ttp_dmdsheet_val.load_dt,p_request_by);

				-- To load into t4056_ttp_mon_summary
				gm_sav_ttp_mon_summary(v_ttp_detail_id, cur_ttp_dmdsheet_val.lvl_id, cur_ttp_dmdsheet_val.lvl_value, p_request_by);
			END IF;
			
			
			
		END LOOP;
		

	  --  DBMS_OUTPUT.PUT_LINE('final time'|| TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS')  );

	END gm_op_ld_summary_main;


	PROCEDURE gm_ld_part_info (
		p_ttp_detail_id 	IN T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE ,
		p_lvl_id           IN T4040_DEMAND_SHEET.c901_level_id%TYPE,
		p_lvl_value         IN T4040_DEMAND_SHEET.c901_level_value%TYPE
	)
	AS
	v_demand_type t4040_demand_sheet.c901_demand_type%TYPE;
	BEGIN

		DBMS_OUTPUT.PUT_LINE(':gm_ld_part_info:'||p_ttp_detail_id||':'||p_lvl_value );
		DELETE FROM my_temp_part_list;

		SELECT MAX(c901_demand_type)
				INTO v_demand_type
			FROM t4040_demand_sheet
			WHERE c4052_ttp_detail_id=p_ttp_detail_id
			AND c4040_void_fl       IS NULL;

			IF (v_demand_type=40023)
		-- Multi Part
			THEN
				INSERT INTO my_temp_part_list(c205_part_number_id)
			        SELECT  C205_PART_NUMBER_ID
			        FROM T4040_DEMAND_SHEET T4040, T4042_MP_DEMAND_SHEET_DETAIL T4042
			        /*  New Table Map */
			        WHERE T4040.C4040_DEMAND_SHEET_ID=T4042.C4040_DEMAND_SHEET_ID
			        AND T4040.C4052_TTP_DETAIL_ID=p_ttp_detail_id
			        AND T4042.C205_PART_NUMBER_ID IS NOT NULL
			        GROUP BY C205_PART_NUMBER_ID ;

			ELSE
				INSERT INTO my_temp_part_list(c205_part_number_id)
			        SELECT  C205_PART_NUMBER_ID
			        FROM T4040_DEMAND_SHEET T4040, T4042_DEMAND_SHEET_DETAIL T4042
			        WHERE T4040.C4040_DEMAND_SHEET_ID=T4042.C4040_DEMAND_SHEET_ID
			        AND T4040.C4052_TTP_DETAIL_ID=p_ttp_detail_id
			        AND T4042.C205_PART_NUMBER_ID IS NOT NULL
			        AND c901_level_id = p_lvl_id
			        AND c901_level_value = p_lvl_value
			        GROUP BY C205_PART_NUMBER_ID ;
			END IF;

	END gm_ld_part_info;


	PROCEDURE gm_sav_part_info (
		p_ttp_detail_id 	IN T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE ,
		p_lvl_id	         IN T4040_DEMAND_SHEET.c901_level_id%TYPE ,
		p_lvl_value         IN T4040_DEMAND_SHEET.c901_level_value%TYPE ,
		p_inv_lck_id        IN T4040_DEMAND_SHEET.C250_INVENTORY_LOCK_ID%TYPE ,
		p_load_dt	        IN T4055_TTP_SUMMARY.C4055_LOAD_DATE%TYPE ,
		p_request_by   		IN	t4020_demand_master.c4020_created_by%TYPE
	)
	AS
	BEGIN
		INSERT INTO T4055_TTP_SUMMARY (C4055_TTP_SUMMARY_ID,c205_part_number_id,C205_PART_NUM_DESC,c901_level_value,c901_level_id ,C255_LEAD_TIME,C255_UNIT_COST,C4055_LOAD_DATE,
		C4052_TTP_DETAIL_ID,C4055_CREATED_DATE,C4055_CREATED_BY)

		SELECT S4055_TTP_SUMMARY_ID.nextval, t205.c205_part_number_id,t205.C205_PART_NUM_DESC,p_lvl_value,p_lvl_id,
		--Vendor Cost and Lead Time applicable only in WW Level.
		DECODE(p_lvl_id,102580,NVL(C255_LEAD_TIME,C255_CALC_LEAD_TIME),''),
		DECODE(p_lvl_id,102580,C255_UNIT_COST,'') ,p_load_dt,p_ttp_detail_id,current_date,p_request_by
		FROM
		my_temp_part_list temp,T255_PART_ATTRIBUTE_LOCK t255,T205_PART_NUMBER t205
		WHERE temp.c205_part_number_id = t205.c205_part_number_id
		AND temp.c205_part_number_id = t255.c205_part_number_id
		AND T255.C250_INVENTORY_LOCK_ID = p_inv_lck_id;


		
	END gm_sav_part_info;

	PROCEDURE gm_ld_demand_sheet_id(
		p_ttp_detail_id 	IN T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE ,
		p_lvl_id         IN T4040_DEMAND_SHEET.c901_level_id%TYPE,
		p_lvl_value         IN T4040_DEMAND_SHEET.c901_level_value%TYPE
	)
	AS
	v_demand_type t4040_demand_sheet.c901_demand_type%TYPE;

	BEGIN

		DELETE FROM my_temp_list;

			SELECT MAX(C901_DEMAND_TYPE)
				INTO v_demand_type
			FROM T4040_DEMAND_SHEET
			WHERE C4052_TTP_DETAIL_ID=p_ttp_detail_id
			AND C4040_VOID_FL       IS NULL;

		IF (v_demand_type=40023)
		-- Multi Part
		THEN

			INSERT INTO my_temp_list
	                   (my_temp_txn_id)
	        SELECT C4040_DEMAND_SHEET_ID
	        FROM T4040_DEMAND_SHEET
	        WHERE C4052_TTP_DETAIL_ID=p_ttp_detail_id
	        AND C4040_VOID_FL       IS NULL;

        ELSE

			INSERT INTO my_temp_list
	                   (my_temp_txn_id)
	        SELECT C4040_DEMAND_SHEET_ID
	        FROM T4040_DEMAND_SHEET
	        WHERE C4052_TTP_DETAIL_ID=p_ttp_detail_id
	        AND C901_LEVEL_ID = p_lvl_ID
	        AND c901_level_value = p_lvl_value
	        AND C4040_VOID_FL       IS NULL;

	   END IF;
		-- Multi Part Handling, Level id null for Multi part Spine, level_valu has value only for Multi Part Trauma
	END gm_ld_demand_sheet_id;


	PROCEDURE gm_ld_forecast (
		p_ttp_detail_id 	IN T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE ,
		p_lvl_value         IN T4040_DEMAND_SHEET.c901_level_value%TYPE ,
		p_inv_lck_id        IN T4040_DEMAND_SHEET.C250_INVENTORY_LOCK_ID%TYPE ,
		p_load_dt	        IN T4055_TTP_SUMMARY.C4055_LOAD_DATE%TYPE ,
		p_request_by   		IN	t4020_demand_master.c4020_created_by%TYPE
	)
	AS
	v_forecast_month NUMBER;

	type pnum_array IS     TABLE OF  T205_PART_NUMBER.c205_part_number_id%TYPE;
	type month_array IS    TABLE OF  T4055_TTP_SUMMARY.C4055_MONTH_01%TYPE ;

	arr_pnum pnum_array;
	arr_m1 month_array;
	arr_m2 month_array;
	arr_m3 month_array;
	arr_m4 month_array;
	arr_m5 month_array;
	arr_m6 month_array;
	arr_m7 month_array;
	arr_m8 month_array;
	arr_m9 month_array;
	arr_m10 month_array;
	arr_m11 month_array;
	arr_m12 month_array;
	arr_par month_array;

	Cursor cur_forecast_by_month
	IS
	--12 Months Forecast
--DEV Independence MIS Apr'2019

		select  pnum
		      , MAX(Month_1) Month_1
		      , MAX(Month_2) Month_2
		      , MAX(Month_3) Month_3
		      , MAX(Month_4) Month_4
		      , MAX(Month_5) Month_5
		      , MAX(Month_6) Month_6
		      , MAX(Month_7) Month_7
		      , MAX(Month_8) Month_8
		      , MAX(Month_9) Month_9
		      , MAX(Month_10) Month_10
		      , MAX(Month_11) Month_11
		      , MAX(Month_12) Month_12
		FROM (
		select   period,pnum
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(p_load_dt , 'month'),0) THEN qty ELSE 0 END) Month_1
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(p_load_dt , 'month'),1)  THEN qty ELSE 0 END) Month_2
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(p_load_dt , 'month'),2)  THEN qty ELSE 0 END) Month_3
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(p_load_dt , 'month'),3)  THEN qty ELSE 0 END) Month_4
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(p_load_dt , 'month'),4)  THEN qty ELSE 0 END) Month_5
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(p_load_dt , 'month'),5)  THEN qty ELSE 0 END) Month_6
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(p_load_dt , 'month'),6)  THEN qty ELSE 0 END) Month_7
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(p_load_dt , 'month'),7)  THEN qty ELSE 0 END) Month_8
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(p_load_dt , 'month'),8)  THEN qty ELSE 0 END) Month_9
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(p_load_dt , 'month'),9)  THEN qty ELSE 0 END) Month_10
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(p_load_dt , 'month'),10)  THEN qty ELSE 0 END) Month_11
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(p_load_dt , 'month'),11) THEN qty ELSE 0 END) Month_12
		from
		(SELECT t4042.c205_part_number_id pnum,
		c4042_period period,
		--SUM (DECODE (refid, NULL, DECODE (t4042.c901_type, 50563, c4042_qty, 0), 0))  qty
		SUM((CASE WHEN refid IS NULL THEN DECODE (t4042.c901_type, 50563, c4042_qty, 0) ELSE 0 END)) qty
		 FROM
		    -- Below code to fetch demand sheet based on par value
		    (
		         SELECT t4042.c4040_demand_sheet_id, t4042.c205_part_number_id, t4042.c4042_period
		          ,T4042.C901_TYPE, NVL (DECODE (t4042.c901_type, 50563, DECODE (
		            parvalue.c205_part_number_id, NULL, t4042.c4042_qty, DECODE (parvalue.parperiod, t4042.c4042_period,
		            parvalue.parvalue, 0)), 0), 0) C4042_QTY,t4042.c4042_parent_ref_id refid
		           FROM T4042_DEMAND_SHEET_DETAIL t4042, my_temp_list my_temp_dsid, (
		                 SELECT c4040_demand_sheet_id, c205_part_number_id, parperiod
		                  , demandvalue, usdemand, ousdemand
		                  , parvalue, (
		                    CASE
		                        WHEN (parvalue - ousdemand) > 0
		                        THEN (parvalue - ousdemand)
		                        ELSE 0
		                    END) usparvalue, ousdemand ousparvalue
		                   FROM
		                    (
		                         SELECT t4042.c4040_demand_sheet_id, t4042.c205_part_number_id, MIN (t4042.c4042_period)
		                            parperiod, SUM (DECODE (t4042.c901_type, 50563, t4042.c4042_qty, 0)) demandvalue, SUM (
		                            DECODE (t4042.c901_type, 4000110, t4042.c4042_qty, 0)) usdemand, SUM (DECODE (
		                            t4042.c901_type, 4000111, t4042.c4042_qty, 0)) ousdemand, SUM (DECODE (t4042.c901_type,
		                            50566, t4042.c4042_qty, 0)) parvalue
		                           FROM T4042_DEMAND_SHEET_DETAIL T4042 , my_temp_list my_temp_dsid
		                          WHERE t4042.c4040_demand_sheet_id  = my_temp_dsid.MY_TEMP_TXN_ID
		                            AND t4042.c4042_period BETWEEN p_load_dt AND ADD_MONTHS (p_load_dt, (v_forecast_month- 1)) -- INPUT current month
		                            AND t4042.c901_type IN (50563, 50566, 4000110, 4000111)
		                       GROUP BY t4042.c4040_demand_sheet_id, t4042.c205_part_number_id
		                    )
		                  WHERE parvalue > demandvalue
		            )
		            parvalue -- Above to fetch part which has value more than par
		          WHERE T4042.C4040_DEMAND_SHEET_ID  = my_temp_dsid.MY_TEMP_TXN_ID
		            AND t4042.c901_type              = 50563--, 4000110, 4000111)
		            AND t4042.c4042_period         <= ADD_MONTHS (p_load_dt, (12 - 1))   -- INPUT current month
		            AND t4042.c4040_demand_sheet_id = parvalue.c4040_demand_sheet_id(+)
		            AND t4042.c205_part_number_id   = parvalue.c205_part_number_id(+)
		            )
		    t4042,  T205_PART_NUMBER T205
		  WHERE t4042.c205_part_number_id  = t205.c205_part_number_id
		    AND t4042.c205_part_number_id IS NOT NULL
		 --and t4042.c205_part_number_id='3135.0313'
		  GROUP BY c4042_period ,T4042.C205_PART_NUMBER_ID
		)
		)
		group by PNUM;

	BEGIN
		SELECT get_rule_value('FORECASTMONTHS','ORDERPLANNING')
		INTO 	v_forecast_month
		FROM 	DUAL;


		OPEN cur_forecast_by_month;
		LOOP
		FETCH cur_forecast_by_month BULK COLLECT INTO arr_pnum,arr_m1,arr_m2,arr_m3,arr_m4,arr_m5,arr_m6,arr_m7,arr_m8,arr_m9,arr_m10,arr_m11,arr_m12  LIMIT 500;

		FORALL i in 1.. arr_pnum.COUNT
			UPDATE T4055_TTP_SUMMARY SET
			C4055_MONTH_01 = arr_m1(i),C4055_MONTH_02=arr_m2(i),C4055_MONTH_03=arr_m3(i),C4055_MONTH_04=arr_m4(i),C4055_MONTH_05=arr_m5(i),C4055_MONTH_06=arr_m6(i),
			C4055_MONTH_07=arr_m7(i),C4055_MONTH_08=arr_m8(i),C4055_MONTH_09=arr_m9(i),C4055_MONTH_10=arr_m10(i),C4055_MONTH_11=arr_m11(i),C4055_MONTH_12=arr_m12(i),
			C4055_FORECAST_QTY = (arr_m1(i)+arr_m2(i)+arr_m3(i)+arr_m4(i))
           -- ,			C4055_PAR = arr_par(i)
			WHERE C4052_TTP_DETAIL_ID = p_ttp_detail_id
			AND c205_part_number_id = arr_pnum(i)
			AND nvl(c901_level_value,1) = nvl(p_lvl_value,1)
			AND C4055_VOID_FL IS NULL;
			--AND (nvl(c901_level_id,1) = nvl(p_lvl_id,1) OR c901_level_value = p_lvl_value);
			-- Multi Part Handling, Level id null for Multi part Spine, level_valu has value only for Multi Part Trauma

		EXIT WHEN cur_forecast_by_month%NOTFOUND;
	    END LOOP;
	    CLOSE cur_forecast_by_month;


	END gm_ld_forecast;


	PROCEDURE gm_ld_forecast_grouping (
		p_ttp_detail_id 	IN T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE ,
		p_lvl_value         IN T4040_DEMAND_SHEET.c901_level_value%TYPE ,
		p_inv_lck_id        IN T4040_DEMAND_SHEET.C250_INVENTORY_LOCK_ID%TYPE ,
		p_load_dt	        IN T4055_TTP_SUMMARY.C4055_LOAD_DATE%TYPE ,
		p_request_by   		IN	t4020_demand_master.c4020_created_by%TYPE
	)
	AS
	v_forecast_month NUMBER;
	v_multipart 	NUMBER;

	type pnum_array IS     TABLE OF  T205_PART_NUMBER.c205_part_number_id%TYPE;
	type tpr_fl_array IS     TABLE OF  T4055_TTP_SUMMARY.C4055_TPR_FL%TYPE;
	type usforecast_array IS    TABLE OF  T4055_TTP_SUMMARY.C4055_US_FORECAST_QTY%TYPE ;
	type ousforecast_array IS    TABLE OF  T4055_TTP_SUMMARY.C4055_OUS_FORECAST_QTY%TYPE ;


	arr_pnum pnum_array;
	arr_tpr_fl tpr_fl_array;
	arr_usforecast usforecast_array;
	arr_ousforecast ousforecast_array;
	arr_wwforecast usforecast_array;


	Cursor cur_forecast_grouping
	IS
		 SELECT t205.c205_part_number_id pnum,
		 -- If TPR_FL = 1 then take TPR from t251, else 0
        SUM(CASE WHEN refid IS NULL THEN 1 ELSE 0 END) TPR_FL,
 		SUM(CASE WHEN refid IS NULL THEN c4042_qty ELSE 0 END) WW_Forecast,
 		SUM(CASE WHEN refid IS NULL THEN c4042_US_qty ELSE 0 END) US_Forecast,
 		SUM(CASE WHEN refid IS NULL THEN c4042_OUS_qty ELSE 0 END) OUS_Forecast
   FROM
   -- Below code to fetch demand sheet based on par value
    (
         SELECT t4042.c205_part_number_id, SUM (DECODE (t4042.c901_type, 50563, DECODE (parvalue.c205_part_number_id,
            NULL, t4042.c4042_qty, DECODE (parvalue.parperiod, t4042.c4042_period, parvalue.parvalue, 0)), 0))
            c4042_qty, SUM (DECODE (t4042.c901_type, 4000110, DECODE (parvalue.c205_part_number_id, NULL,
            t4042.c4042_qty, DECODE (parvalue.parperiod, t4042.c4042_period, parvalue.usparvalue, 0)), 0)) c4042_US_qty
            , SUM (DECODE (t4042.c901_type, 4000111, DECODE (parvalue.c205_part_number_id, NULL, t4042.c4042_qty,
            DECODE (parvalue.parperiod, t4042.c4042_period, parvalue.ousparvalue, 0)), 0)) c4042_OUS_qty,
            t4042.c4042_parent_ref_id refid
           FROM t4042_demand_sheet_detail t4042, my_temp_list my_temp_dsid,  (
                 SELECT c4040_demand_sheet_id, c205_part_number_id, parperiod
                  , demandvalue, usdemand, ousdemand
                  , parvalue, (
                    CASE
                        WHEN (parvalue - ousdemand) > 0
                        THEN (parvalue - ousdemand)
                        ELSE 0
                    END) usparvalue, ousdemand ousparvalue
                   FROM
                    (
                         SELECT t4042.c4040_demand_sheet_id, t4042.c205_part_number_id, MIN (t4042.c4042_period)
                            parperiod, SUM (DECODE (t4042.c901_type, 50563, t4042.c4042_qty, 0)) demandvalue, SUM (
                            DECODE (t4042.c901_type, 4000110, t4042.c4042_qty, 0)) usdemand, SUM (DECODE (
                            t4042.c901_type, 4000111, t4042.c4042_qty, 0)) ousdemand, SUM (DECODE (t4042.c901_type,
                            50566, t4042.c4042_qty, 0)) parvalue
                           FROM t4042_demand_sheet_detail t4042, my_temp_list my_temp_dsid
                          WHERE t4042.c4040_demand_sheet_id = my_temp_dsid.MY_TEMP_TXN_ID
                            AND t4042.c4042_period <= ADD_MONTHS (p_load_dt, (v_forecast_month - 1))
                            AND t4042.c901_type IN (50563, 4000110, 4000111, 50566)
                       GROUP BY t4042.c4040_demand_sheet_id, t4042.c205_part_number_id
                    )
                  WHERE parvalue > demandvalue
            )
            parvalue -- Above to fetch part which has value more than par
          WHERE t4042.c4040_demand_sheet_id = my_temp_dsid.MY_TEMP_TXN_ID
            AND t4042.c901_type                                                                             IN (50563, 4000106, 4000110, 4000111)
            AND t4042.c4042_period <= ADD_MONTHS (p_load_dt, (v_forecast_month - 1))
            AND t4042.c4040_demand_sheet_id = parvalue.c4040_demand_sheet_id(+)
            AND t4042.c205_part_number_id   = parvalue.c205_part_number_id(+)
       GROUP BY t4042.c205_part_number_id, t4042.c4042_parent_ref_id
    )
    forecast,t205_part_number t205
  WHERE t205.c205_part_number_id  = forecast.c205_part_number_id
     AND t205.c205_part_number_id IS NOT NULL
    --and t205.c205_part_number_id='693.001'
GROUP BY T205.C205_Part_Number_Id
;

	BEGIN
		SELECT get_rule_value('FORECASTMONTHS','ORDERPLANNING')
		INTO 	v_forecast_month
		FROM 	DUAL;

	 	SELECT count(1)
	 	INTO v_multipart
	    FROM t4040_demand_sheet
	    WHERE c4052_ttp_detail_id=p_ttp_detail_id
	    AND c901_demand_type=40023
	    AND c4040_void_fl IS NULL;
		--40023 multi part

		OPEN cur_forecast_grouping;
		LOOP
		FETCH cur_forecast_grouping BULK COLLECT INTO arr_pnum,arr_tpr_fl,arr_wwforecast,arr_usforecast,arr_ousforecast LIMIT 500;

		FORALL i in 1.. arr_pnum.COUNT

			UPDATE T4055_TTP_SUMMARY SET
					C4055_US_FORECAST_QTY =arr_usforecast(i),
					C4055_OUS_FORECAST_QTY=arr_ousforecast(i),
					C4055_FORECAST_QTY= arr_wwforecast(i),
					C4055_TPR_FL= decode(v_multipart,1,1,arr_tpr_fl(i))
			WHERE C4052_TTP_DETAIL_ID = p_ttp_detail_id
			AND c205_part_number_id = arr_pnum(i)
			AND c901_level_value = p_lvl_value
			AND C4055_VOID_FL IS NULL;

		EXIT WHEN cur_forecast_grouping%NOTFOUND;
	    END LOOP;
	    CLOSE cur_forecast_grouping;


	END gm_ld_forecast_grouping;


	PROCEDURE gm_upd_PAR (
		p_ttp_detail_id 	IN T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE ,
		p_lvl_value         IN T4040_DEMAND_SHEET.c901_level_value%TYPE ,
		p_inv_lck_id        IN T4040_DEMAND_SHEET.C250_INVENTORY_LOCK_ID%TYPE ,
		p_load_dt	        IN T4055_TTP_SUMMARY.C4055_LOAD_DATE%TYPE ,
		p_request_by   		IN	t4020_demand_master.c4020_created_by%TYPE
	)
	AS
	v_forecast_month NUMBER;

	type pnum_array IS     TABLE OF  T205_PART_NUMBER.c205_part_number_id%TYPE;
	type par_array IS    TABLE OF  T4055_TTP_SUMMARY.C4055_PAR%TYPE ;
/*	type usforecast_array IS    TABLE OF  T4055_TTP_SUMMARY.C4055_US_FORECAST_QTY%TYPE ;
	type ousforecast_array IS    TABLE OF  T4055_TTP_SUMMARY.C4055_OUS_FORECAST_QTY%TYPE ;
	type pkey_array IS    TABLE OF  T4055_TTP_SUMMARY.C4055_TTP_SUMMARY_ID%TYPE ;
	*/



	/*arr_pnum pnum_array;
	arr_usforecast usforecast_array;
	arr_wwforecast usforecast_array;
	arr_pkey pkey_array;
    */
	arr_pnum pnum_array;
	arr_par par_array;

	Cursor cur_PAR
	IS

/*
		 SELECT t4055.C205_PART_NUMBER_ID pnum, (NVL(t4055.C4055_PAR,0)+NVL(C4055_US_FORECAST_QTY,0)) US_FCST
				, (NVL(t4055.C4055_PAR,0)+NVL(C4055_FORECAST_QTY,0)) WW_FCST ,C4055_TTP_SUMMARY_ID pkey
		FROM T4055_TTP_SUMMARY t4055
		WHERE t4055.C4055_VOID_FL is null
		and t4055.C4052_TTP_DETAIL_ID=p_ttp_detail_id
		and t4055.C901_LEVEL_VALUE=p_lvl_value
		--and t4055.C205_PART_NUMBER_ID in ('1135.0020','1135.0025','1135.0027','1135.0030')
		--and C205_PART_NUMBER_ID=''
		;*/

		 SELECT t4042.c205_part_number_id pnum, SUM (DECODE (t4042.c901_type,
                            50566, t4042.c4042_qty, 0)) parvalue
                           FROM t4042_demand_sheet_detail t4042, my_temp_list my_temp_dsid
                          WHERE t4042.c4040_demand_sheet_id = my_temp_dsid.MY_TEMP_TXN_ID
                            AND t4042.c4042_period <= ADD_MONTHS (p_load_dt, (v_forecast_month - 1))
                            AND t4042.c901_type=50566
                       GROUP BY t4042.c4040_demand_sheet_id, t4042.c205_part_number_id;

	BEGIN

		SELECT get_rule_value('FORECASTMONTHS','ORDERPLANNING')
		INTO 	v_forecast_month
		FROM 	DUAL;

		OPEN cur_PAR;
		LOOP
		FETCH cur_PAR BULK COLLECT INTO arr_pnum,arr_par LIMIT 500;

		FORALL i in 1.. arr_pnum.COUNT

			UPDATE 	T4055_TTP_SUMMARY
    		SET
		    		C4055_PAR = arr_par(i)
			WHERE C4052_TTP_DETAIL_ID = p_ttp_detail_id
			AND c205_part_number_id = arr_pnum(i)
			AND c901_level_value = p_lvl_value
			AND C4055_VOID_FL IS NULL;

		EXIT WHEN cur_PAR%NOTFOUND;
	    END LOOP;
	    CLOSE cur_PAR;


	END gm_upd_PAR;


	PROCEDURE gm_ld_SetPAR (
		p_ttp_detail_id 	IN T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE ,
		p_lvl_value         IN T4040_DEMAND_SHEET.c901_level_value%TYPE ,
		p_inv_lck_id        IN T4040_DEMAND_SHEET.C250_INVENTORY_LOCK_ID%TYPE ,
		p_load_dt	        IN T4055_TTP_SUMMARY.C4055_LOAD_DATE%TYPE ,
		p_request_by   		IN	t4020_demand_master.c4020_created_by%TYPE
	)
	AS
	v_forecast_month NUMBER;

	type pnum_array IS     TABLE OF  T205_PART_NUMBER.c205_part_number_id%TYPE;
	type setpar_array IS    TABLE OF  T4055_TTP_SUMMARY.C4055_SET_PAR%TYPE ;


	arr_pnum pnum_array;
	arr_setpar setpar_array;


	Cursor cur_setpar
	IS
		SELECT  c205_part_number_id, SUM(t4042.c4042_qty) SET_PAR
           FROM T4042_DEMAND_SHEET_DETAIL t4042,T4040_DEMAND_SHEET T4040
          WHERE T4040.C4040_DEMAND_SHEET_ID  = T4042.C4040_DEMAND_SHEET_ID
            AND T4040.C4052_TTP_DETAIL_ID=p_ttp_detail_id
            AND T4040.C4040_VOID_FL IS NULL
            AND c901_level_id=102580 --102580 Company
             AND C4040_VOID_FL IS NULL
            AND t4042.c901_type= 4000113 --  SET PAR
            AND t4042.c4042_period = p_load_dt
            and  t4042.c205_part_number_id IS NOT NULL
           	and c4042_qty >0
           	GROUP BY c205_part_number_id
            ;

	BEGIN
		SELECT get_rule_value('FORECASTMONTHS','ORDERPLANNING')
		INTO 	v_forecast_month
		FROM 	DUAL;


		OPEN cur_setpar;
		LOOP
		FETCH cur_setpar BULK COLLECT INTO arr_pnum,arr_setpar LIMIT 500;

		FORALL i in 1.. arr_pnum.COUNT

			UPDATE T4055_TTP_SUMMARY SET
					C4055_SET_PAR = arr_setpar(i)
			WHERE C4052_TTP_DETAIL_ID = p_ttp_detail_id
			AND c205_part_number_id = arr_pnum(i)
			AND c901_level_value = p_lvl_value
			AND C4055_VOID_FL IS NULL;

		EXIT WHEN cur_setpar%NOTFOUND;
	    END LOOP;
	    CLOSE cur_setpar;


	END gm_ld_SetPAR;



	PROCEDURE gm_ld_OTN (
		p_ttp_detail_id 	IN T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE ,
		p_lvl_value         IN T4040_DEMAND_SHEET.c901_level_value%TYPE ,
		p_inv_lck_id        IN T4040_DEMAND_SHEET.C250_INVENTORY_LOCK_ID%TYPE ,
		p_load_dt	        IN T4055_TTP_SUMMARY.C4055_LOAD_DATE%TYPE ,
		p_request_by   		IN	t4020_demand_master.c4020_created_by%TYPE
	)
	AS
	v_forecast_month NUMBER;

	type pnum_array IS     	TABLE OF  T205_PART_NUMBER.c205_part_number_id%TYPE;
	type otn_array IS    	TABLE OF  T4055_TTP_SUMMARY.C4055_SET_PAR%TYPE ;


	arr_pnum pnum_array;
	arr_ww_otn otn_array;
	arr_us_otn otn_array;
	arr_ous_otn otn_array;


	Cursor cur_OTN
	IS

		 SELECT *FROM
		(
		SELECT  part_id, level_id, OTN_QTY
		FROM
		    (select 'WW' Level_Id,
		  t4023.c205_part_number_id part_id,
		  TRUNC(SYSDATE , 'Month') Load_Date,
		  SUM(c4023_qty)  otn_qty
		  from t4023_demand_otn_detail t4023,
		  t4020_demand_master t4020,my_temp_part_list part_list,T4040_DEMAND_SHEET t4040
		 where t4020.c4020_demand_master_id = t4023.c4020_demand_master_id
		  and part_list.c205_part_number_id=t4023.c205_part_number_id
		   and t4020.C4020_DEMAND_MASTER_ID = t4040.C4020_DEMAND_MASTER_ID
		   and t4040.C4052_TTP_DETAIL_ID = p_ttp_detail_id
		  and t4023.c4023_void_fl           is null
		  and t4020.c4020_void_fl           is null
		  and t4020.c4020_inactive_fl       is null
		 -- and t4023.c205_part_number_id ='193.001'
		  GROUP BY t4023.c205_part_number_id
		  UNION ALL
		  --US
		   select 'US' Level_Id,
		  t4023.c205_part_number_id part_id,
		  TRUNC(SYSDATE , 'Month') Load_Date,
		  SUM(c4023_qty)  otn_qty
		  from t4023_demand_otn_detail t4023,
		  t4020_demand_master t4020,my_temp_part_list part_list,T4040_DEMAND_SHEET t4040
		 where t4020.c4020_demand_master_id = t4023.c4020_demand_master_id
		  and part_list.c205_part_number_id=t4023.c205_part_number_id
		  and t4020.C4020_DEMAND_MASTER_ID = t4040.C4020_DEMAND_MASTER_ID
		   and t4040.C4052_TTP_DETAIL_ID = p_ttp_detail_id
		  and t4023.c4023_void_fl           is null
		  and t4020.c4020_void_fl           is null
		  and t4020.c4020_inactive_fl       is null
		  and t4020.C4015_GLOBAL_SHEET_MAP_ID=3
		 -- and t4023.c205_part_number_id ='193.001'
		  GROUP BY t4023.c205_part_number_id
		    UNION ALL
		    --OUS
		   select 'OUS' Level_Id,
		  t4023.c205_part_number_id part_id,
		  TRUNC(SYSDATE , 'Month') Load_Date,
		  SUM(c4023_qty)  otn_qty
		  from t4023_demand_otn_detail t4023,
		  t4020_demand_master t4020,my_temp_part_list part_list,T4040_DEMAND_SHEET t4040
		 where t4020.c4020_demand_master_id = t4023.c4020_demand_master_id
		  and part_list.c205_part_number_id=t4023.c205_part_number_id
		  and t4020.C4020_DEMAND_MASTER_ID = t4040.C4020_DEMAND_MASTER_ID
		   and t4040.C4052_TTP_DETAIL_ID = p_ttp_detail_id
		  and t4023.c4023_void_fl           is null
		  and t4020.c4020_void_fl           is null
		  and t4020.c4020_inactive_fl       is null
		  and t4020.C4015_GLOBAL_SHEET_MAP_ID<>3
		  --and t4023.c205_part_number_id ='193.001'
		  GROUP BY t4023.c205_part_number_id)
		  )
		  pivot
		  (max(OTN_QTY) FOR level_id in ('WW' as "WW",'US' as "US",'OUS' as "OUS"));


	BEGIN

		OPEN cur_OTN;
		LOOP
		FETCH cur_OTN BULK COLLECT INTO arr_pnum,arr_ww_otn,arr_us_otn,arr_ous_otn LIMIT 500;

		FORALL i in 1.. arr_pnum.COUNT

			UPDATE T4055_TTP_SUMMARY SET
					C4023_OTN = arr_ww_otn(i),
					C4023_US_OTN = arr_us_otn(i),
					C4023_OUS_OTN = arr_ous_otn(i)
			WHERE C4052_TTP_DETAIL_ID = p_ttp_detail_id
			AND c205_part_number_id = arr_pnum(i)
			AND c901_level_value = p_lvl_value
			AND C4055_VOID_FL IS NULL;

		EXIT WHEN cur_OTN%NOTFOUND;
	    END LOOP;
	    CLOSE cur_OTN;


	END gm_ld_OTN;


	PROCEDURE gm_ld_total_required (
		p_ttp_detail_id 	IN T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE ,
		p_lvl_value         IN T4040_DEMAND_SHEET.c901_level_value%TYPE ,
		p_inv_lck_id        IN T4040_DEMAND_SHEET.C250_INVENTORY_LOCK_ID%TYPE ,
		p_load_dt	        IN T4055_TTP_SUMMARY.C4055_LOAD_DATE%TYPE ,
		p_request_by   		IN	t4020_demand_master.c4020_created_by%TYPE
	)
	AS
	v_forecast_month NUMBER;

	v_ous_fcst 			NUMBER;
	v_ous_need	 		NUMBER :=0;
	v_rw_inv 			NUMBER;
	v_ous_tpr			NUMBER;
	v_toal_required 	NUMBER;
	v_ous_otn			NUMBER;
	v_inventory 		NUMBER;
	v_order_qty			NUMBER;
	v_us_need			NUMBER;
	v_parent_need		NUMBER;
	v_lta				NUMBER;
	v_par				NUMBER;
	v_OUS_LTA   NUMBER;
    v_US_LTA   NUMBER;

	Cursor cur_inventory
	IS

		 SELECT t4055.C205_PART_NUMBER_ID pnum,NVL(t4055.C4055_FORECAST_QTY,0) WW_FCST,NVL(C4055_US_FORECAST_QTY,0) US_FCST
				,NVL(C4055_OUS_FORECAST_QTY,0) OUS_FCST , (CASE WHEN t4055.C4055_TPR_FL=1 THEN C251_TPR ELSE 0 END)  WW_TPR,(CASE WHEN t4055.C4055_TPR_FL=1 THEN C251_TPR_US ELSE 0 END)TPR_US,
				(CASE WHEN t4055.C4055_TPR_FL=1 THEN C251_TPR_OUS ELSE 0 END) TPR_OUS,NVL(C4023_US_OTN,0) US_OTN,NVL(C4023_OUS_OTN,0) OUS_OTN,NVL(C4055_SET_PAR,0) SET_PAR,
		 		(NVL(t251.C251_IN_STOCK,0)+NVL(t251.C251_OPEN_PO,0)+NVL(t251.C251_OPEN_DHR,0)+NVL(t251.C251_BUILD_SET,0)) TOT_INV,
				NVL(t251.C251_RW_INVENTORY,0) RW_INV , NVL(C255_UNIT_COST,0) part_cost,C4055_TTP_SUMMARY_ID pkey,
				NVL(C255_PARENT_NEED,0) PNEED,NVL(C255_LTA,0) LTA ,NVL(C255_PARENT_US_LTA,0) PARENT_US_LTA,NVL(C255_US_LTA,0) LTA_US, NVL(C255_PARENT_OUS_LTA,0) PARENT_OUS_LTA,
				NVL(C255_OUS_LTA,0) OUS_LTA,NVL(t4055.C4055_PAR,0) PAR,
				NVL(C255_PARENT_NEED_US,0) PNEED_US,
				NVL(C255_PARENT_NEED_OUS,0) PNEED_OUS,
				NVL(C4025_SAFETY_STOCK_QTY,0) SSTOCK_QTY
		FROM T251_INVENTORY_LOCK_DETAILS t251,T4055_TTP_SUMMARY t4055
		WHERE C250_INVENTORY_LOCK_ID= p_inv_lck_id
		and t4055.C205_PART_NUMBER_ID = t251.C205_PART_NUMBER_ID
		and t4055.C4055_VOID_FL is null
		and t4055.C4052_TTP_DETAIL_ID=p_ttp_detail_id
		and t4055.C901_LEVEL_VALUE=p_lvl_value
		--and t4055.C205_PART_NUMBER_ID in ('1135.0020','1135.0025','1135.0027','1135.0030')
		--and C205_PART_NUMBER_ID=''
    ORDER BY t4055.C205_PART_NUMBER_ID
		;



	BEGIN

		FOR cur_inv_val IN cur_inventory
    	LOOP
    		v_ous_need := 0;
    		v_us_need :=0;
    		v_toal_required :=0;
    		v_order_qty :=0;
    		v_parent_need :=0;
    		v_lta :=0;
    		v_OUS_LTA :=0;
            v_US_LTA :=0;
            v_par :=0;

    		v_ous_fcst := cur_inv_val.OUS_FCST;
    		v_rw_inv := cur_inv_val.RW_INV;
    		v_ous_tpr := cur_inv_val.TPR_OUS;
    		v_ous_otn := cur_inv_val.OUS_OTN;
    	--	v_ous_need := v_ous_fcst+v_ous_tpr+v_ous_otn+ cur_inv_val.PNEED_OUS+ cur_inv_val.OUS_LTA;
       		v_OUS_LTA := cur_inv_val.OUS_LTA;
       		v_par	  :=cur_inv_val.PAR;

    	/*SELECT DECODE(cur_inv_val.PNEED_OUS ,0,cur_inv_val.OUS_LTA,0)
       INTO v_OUS_LTA
       FROM DUAL; */


      v_ous_need := v_ous_fcst+v_ous_tpr+v_ous_otn+ cur_inv_val.PNEED_OUS +v_OUS_LTA;
    		v_parent_need := cur_inv_val.PNEED;
    		v_lta := cur_inv_val.LTA;

    		v_inventory := cur_inv_val.TOT_INV;
    		--v_us_need := cur_inv_val.US_FCST+cur_inv_val.TPR_US+cur_inv_val.SET_PAR+cur_inv_val.US_OTN + cur_inv_val.PNEED_US+ cur_inv_val.LTA_US + cur_inv_val.SSTOCK_QTY;

    		v_US_LTA := cur_inv_val.LTA_US;

      /* SELECT DECODE(cur_inv_val.PNEED_US ,0,cur_inv_val.LTA_US,0)
       INTO v_US_LTA
       FROM DUAL;  */


        v_us_need := cur_inv_val.US_FCST+cur_inv_val.TPR_US+cur_inv_val.SET_PAR+cur_inv_val.US_OTN + cur_inv_val.PNEED_US+v_US_LTA+ cur_inv_val.SSTOCK_QTY;

    		--v_toal_required := GREATEST(0,v_us_need+v_ous_need+v_parent_need+v_lta - (cur_inv_val.PNEED_OUS+cur_inv_val.OUS_LTA+cur_inv_val.PNEED_US+ cur_inv_val.LTA_US));
    		-- AS per the output, the LTA is not used in the total required formula,hence removing it.
    	--	v_toal_required := GREATEST(0,((v_us_need+v_ous_need) -(cur_inv_val.OUS_LTA+cur_inv_val.LTA_US) ));
      v_toal_required := (v_us_need+v_ous_need) ;

    	--	DBMS_OUTPUT.PUT_LINE('1:'||cur_inv_val.pnum||':v_us_need: '||v_us_need||' :v_ous_need: '||v_ous_need||' :v_toal_required: '||v_toal_required||' :v_inventory: '||v_inventory);

    		-- OUS Forecast is postive and having inventory in RW.
    		IF (cur_inv_val.RW_INV>0)
    		THEN
    			IF ( v_ous_need >= cur_inv_val.RW_INV)
    			THEN
    				v_ous_need := v_ous_need-cur_inv_val.RW_INV;
    			ELSE
    				--As all OUS needs are satisified with RW, we do not need any OUS inventory needs.
    				v_ous_need := 0;
    			END IF;
    		END IF;

    	--	DBMS_OUTPUT.PUT_LINE('2:'||cur_inv_val.pnum||':v_ous_need:'||v_ous_need||':v_toal_required:'||v_toal_required||':v_inventory:'||v_inventory);

    		-- CHeck if the Total Need is more than than the available inventory,so we call identify the order qty for the part.
    		IF ((v_us_need+v_ous_need) >v_inventory)
    		THEN
    			v_order_qty := (v_us_need+v_ous_need) -v_inventory;
    		ELSE
    			-- As we have enough inventory ,we do not need to order.
    			v_order_qty :=0;
    		END IF;

    	--	DBMS_OUTPUT.PUT_LINE('3:'||cur_inv_val.pnum||':v_us_need:'||v_us_need||':v_ous_need:'||v_ous_need||':v_toal_required:'||v_toal_required||':v_order_qty:'||v_order_qty);

    		UPDATE 	T4055_TTP_SUMMARY
    		SET
		    		C4055_TOTAL_REQUIRED_QTY = v_toal_required,
		    		C4055_ORG_TOTAL_REQUIRED_QTY = v_toal_required,
		    		C4055_TOTAL_ORDER_QTY = v_order_qty,
		    		C4055_ORG_ORDER_QTY=v_order_qty,
		    		C4055_TOTAL_ORDER_COST = v_order_qty * cur_inv_val.part_cost,
					C4055_ORG_ORDER_COST =  v_order_qty * cur_inv_val.part_cost

			WHERE C4055_TTP_SUMMARY_ID = cur_inv_val.pkey
			AND C4055_VOID_FL IS NULL;

    	END LOOP;

	END gm_ld_total_required;

	  /*******************************************************
   * Description : Procedure to fetch US OUS forecast for parent need calculation
   * Author 	 : Paddy
   *******************************************************/
	PROCEDURE gm_ld_us_ous_forecast
	(
		p_ttp_detail_id 	IN T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE ,
		p_lvl_id         IN T4040_DEMAND_SHEET.c901_level_id%TYPE,
		p_lvl_value         IN T4040_DEMAND_SHEET.c901_level_value%TYPE
	)
	AS
	v_forecast_month NUMBER;
	v_ttp_detail_id T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE;
	v_load_dt	    T4055_TTP_SUMMARY.C4055_LOAD_DATE%TYPE;

	type pnum_array IS     TABLE OF  T205_PART_NUMBER.c205_part_number_id%TYPE;
	type month_array IS    TABLE OF  T4055_TTP_SUMMARY.C4055_MONTH_01%TYPE ;

	arr_pnum pnum_array;
	arr_m1 month_array;
	arr_m2 month_array;
	arr_m3 month_array;
	arr_m4 month_array;
	arr_m5 month_array;
	arr_m6 month_array;
	arr_m7 month_array;
	arr_m8 month_array;
	arr_m9 month_array;
	arr_m10 month_array;
	arr_m11 month_array;
	arr_m12 month_array;
	arr_ous_m1 month_array;
	arr_ous_m2 month_array;
	arr_ous_m3 month_array;
	arr_ous_m4 month_array;
	arr_ous_m5 month_array;
	arr_ous_m6 month_array;
	arr_ous_m7 month_array;
	arr_ous_m8 month_array;
	arr_ous_m9 month_array;
	arr_ous_m10 month_array;
	arr_ous_m11 month_array;
	arr_ous_m12 month_array;


	Cursor cur_usous_fcst_by_month
	IS

		SELECT  pnum

		      , MAX(Month_1_US) US_FCST_1
		      , MAX(Month_2_US) US_FCST_2
		      , MAX(Month_3_US) US_FCST_3
		      , MAX(Month_4_US) US_FCST_4
		      , MAX(Month_5_US) US_FCST_5
		      , MAX(Month_6_US) US_FCST_6
		      , MAX(Month_7_US) US_FCST_7
		      , MAX(Month_8_US) US_FCST_8
		      , MAX(Month_9_US) US_FCST_9
		      , MAX(Month_10_US) US_FCST_10
		      , MAX(Month_11_US) US_FCST_11
		      , MAX(Month_12_US) US_FCST_12
              , MAX(Month_1_OUS) OUS_FCST_1
              , MAX(Month_2_OUS) OUS_FCST_2
              , MAX(Month_3_OUS) OUS_FCST_3
              , MAX(Month_4_OUS) OUS_FCST_4
              , MAX(Month_5_OUS) OUS_FCST_5
              , MAX(Month_6_OUS) OUS_FCST_6
              , MAX(Month_7_OUS) OUS_FCST_7
              , MAX(Month_8_OUS) OUS_FCST_8
              , MAX(Month_9_OUS) OUS_FCST_9
              , MAX(Month_10_OUS) OUS_FCST_10
              , MAX(Month_11_OUS) OUS_FCST_11
              , MAX(Month_12_OUS) OUS_FCST_12

		FROM (
		select   period,pnum
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(v_load_dt , 'month'),0) THEN US_qty ELSE 0 END) Month_1_US
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(v_load_dt , 'month'),1)  THEN US_qty ELSE 0 END) Month_2_US
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(v_load_dt , 'month'),2)  THEN US_qty ELSE 0 END) Month_3_US
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(v_load_dt , 'month'),3)  THEN US_qty ELSE 0 END) Month_4_US
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(v_load_dt , 'month'),4)  THEN US_qty ELSE 0 END) Month_5_US
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(v_load_dt , 'month'),5)  THEN US_qty ELSE 0 END) Month_6_US
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(v_load_dt , 'month'),6)  THEN US_qty ELSE 0 END) Month_7_US
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(v_load_dt , 'month'),7)  THEN US_qty ELSE 0 END) Month_8_US
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(v_load_dt , 'month'),8)  THEN US_qty ELSE 0 END) Month_9_US
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(v_load_dt , 'month'),9)  THEN US_qty ELSE 0 END) Month_10_US
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(v_load_dt , 'month'),10)  THEN US_qty ELSE 0 END) Month_11_US
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(v_load_dt , 'month'),11) THEN US_qty ELSE 0 END) Month_12_US
               , (CASE WHEN period = ADD_MONTHS(TRUNC(v_load_dt , 'month'),0) THEN OUS_qty ELSE 0 END) Month_1_OUS
               , (CASE WHEN period = ADD_MONTHS(TRUNC(v_load_dt , 'month'),1) THEN OUS_qty ELSE 0 END) Month_2_OUS
               , (CASE WHEN period = ADD_MONTHS(TRUNC(v_load_dt , 'month'),2) THEN OUS_qty ELSE 0 END) Month_3_OUS
               , (CASE WHEN period = ADD_MONTHS(TRUNC(v_load_dt , 'month'),3) THEN OUS_qty ELSE 0 END) Month_4_OUS
               , (CASE WHEN period = ADD_MONTHS(TRUNC(v_load_dt , 'month'),4) THEN OUS_qty ELSE 0 END) Month_5_OUS
               , (CASE WHEN period = ADD_MONTHS(TRUNC(v_load_dt , 'month'),5) THEN OUS_qty ELSE 0 END) Month_6_OUS
               , (CASE WHEN period = ADD_MONTHS(TRUNC(v_load_dt , 'month'),6) THEN OUS_qty ELSE 0 END) Month_7_OUS
               , (CASE WHEN period = ADD_MONTHS(TRUNC(v_load_dt , 'month'),7) THEN OUS_qty ELSE 0 END) Month_8_OUS
               , (CASE WHEN period = ADD_MONTHS(TRUNC(v_load_dt , 'month'),8) THEN OUS_qty ELSE 0 END) Month_9_OUS
               , (CASE WHEN period = ADD_MONTHS(TRUNC(v_load_dt , 'month'),9) THEN OUS_qty ELSE 0 END) Month_10_OUS
               , (CASE WHEN period = ADD_MONTHS(TRUNC(v_load_dt , 'month'),10) THEN OUS_qty ELSE 0 END) Month_11_OUS
               , (CASE WHEN period = ADD_MONTHS(TRUNC(v_load_dt , 'month'),11) THEN OUS_qty ELSE 0 END) Month_12_OUS
		from
		(SELECT t4042.c205_part_number_id pnum,
		c4042_period period,
  		 SUM((CASE WHEN refid IS NULL THEN DECODE (t4042.c901_type, 4000110, US_QTY, 0) ELSE 0 END))US_qty,
         SUM((CASE WHEN refid IS NULL THEN DECODE (t4042.c901_type, 4000111, OUS_QTY, 0) ELSE 0 END))OUS_qty
		 FROM
		    -- 4000110 US
		    -- 4000111 OUS
		    -- 50563   Forecast
		    -- Below code to fetch demand sheet based on par value
		    (
		         SELECT t4042.c4040_demand_sheet_id, t4042.c205_part_number_id, t4042.c4042_period
		          ,T4042.C901_TYPE, NVL (DECODE (t4042.c901_type, 4000110, DECODE (
		            parvalue.c205_part_number_id, NULL, t4042.c4042_qty, DECODE (parvalue.parperiod, t4042.c4042_period,
		            parvalue.parvalue, 0)), 0), 0) US_QTY,NVL (DECODE (t4042.c901_type, 4000111, DECODE (
		            parvalue.c205_part_number_id, NULL, t4042.c4042_qty, DECODE (parvalue.parperiod, t4042.c4042_period,
		            parvalue.parvalue, 0)), 0), 0) OUS_QTY,t4042.c4042_parent_ref_id refid
		           FROM T4042_DEMAND_SHEET_DETAIL t4042, my_temp_list my_temp_dsid,
                   (
		                 SELECT c4040_demand_sheet_id, c205_part_number_id, parperiod
		                  , demandvalue, usdemand, ousdemand
		                  , parvalue, (
		                    CASE
		                        WHEN (parvalue - ousdemand) > 0
		                        THEN (parvalue - ousdemand)
		                        ELSE 0
		                    END) usparvalue, ousdemand ousparvalue
		                   FROM
		                    (
		                         SELECT t4042.c4040_demand_sheet_id, t4042.c205_part_number_id, MIN (t4042.c4042_period)
		                            parperiod, SUM (DECODE (t4042.c901_type, 50563, t4042.c4042_qty, 0)) demandvalue, SUM (
		                            DECODE (t4042.c901_type, 4000110, t4042.c4042_qty, 0)) usdemand, SUM (DECODE (
		                            t4042.c901_type, 4000111, t4042.c4042_qty, 0)) ousdemand, SUM (DECODE (t4042.c901_type,
		                            50566, t4042.c4042_qty, 0)) parvalue
		                           FROM T4042_DEMAND_SHEET_DETAIL T4042, my_temp_list my_temp_dsid
		                          WHERE t4042.c4040_demand_sheet_id  = my_temp_dsid.MY_TEMP_TXN_ID
		                            AND t4042.c4042_period BETWEEN v_load_dt AND ADD_MONTHS (v_load_dt, (v_forecast_month- 1)) -- INPUT current month
		                            AND t4042.c901_type IN (50563, 50566, 4000110, 4000111)
		                       GROUP BY t4042.c4040_demand_sheet_id, t4042.c205_part_number_id
		                    )
		                  WHERE parvalue > demandvalue
		            )
		            parvalue -- Above to fetch part which has value more than par
		          WHERE T4042.C4040_DEMAND_SHEET_ID = my_temp_dsid.MY_TEMP_TXN_ID
		            AND t4042.c901_type            IN (50563, 4000110, 4000111)
		            AND t4042.c4042_period         <= ADD_MONTHS (v_load_dt, (12 - 1))   -- INPUT current month
		            AND t4042.c4040_demand_sheet_id = parvalue.c4040_demand_sheet_id(+)
		            AND t4042.c205_part_number_id   = parvalue.c205_part_number_id(+)
		            )
		    t4042,  T205_PART_NUMBER T205
		  WHERE t4042.c205_part_number_id  = t205.c205_part_number_id
		    AND t4042.c205_part_number_id IS NOT NULL
		    --AND t4042.c205_part_number_id='682.108S'
		  GROUP BY c4042_period ,T4042.C205_PART_NUMBER_ID
		)
		)
        group by PNUM;

	BEGIN

		v_ttp_detail_id := p_ttp_detail_id;
		
		DELETE FROM  TEMP_US_OUS_FORECAST;
		
		BEGIN
			SELECT
			MAX(C4040_DEMAND_PERIOD_DT) INTO v_load_dt
			FROM T4040_DEMAND_SHEET
			WHERE C4052_TTP_DETAIL_ID=v_ttp_detail_id
	        AND C4040_VOID_FL IS NULL
			ORDER BY c901_level_id DESC;

			EXCEPTION WHEN OTHERS
			    THEN
			    	RETURN;
		END;

		SELECT get_rule_value('FORECASTMONTHS','ORDERPLANNING')
		INTO 	v_forecast_month
		FROM 	DUAL;

		gm_pkg_oppr_ld_summary.gm_ld_demand_sheet_id(p_ttp_detail_id,p_lvl_id,p_lvl_value);

	/*	SELECT get_rule_value('FORECASTMONTHS','ORDERPLANNING')
		INTO 	v_forecast_month
		FROM 	DUAL;
		*/

		OPEN cur_usous_fcst_by_month;
		LOOP
		FETCH cur_usous_fcst_by_month BULK COLLECT INTO arr_pnum,arr_m1,arr_m2,arr_m3,arr_m4,arr_m5,arr_m6,arr_m7,arr_m8,arr_m9,arr_m10,arr_m11,arr_m12,
		arr_ous_m1,arr_ous_m2,arr_ous_m3,arr_ous_m4,arr_ous_m5,arr_ous_m6,arr_ous_m7,arr_ous_m8,arr_ous_m9,arr_ous_m10,arr_ous_m11,arr_ous_m12 LIMIT 500;

		FORALL i in 1.. arr_pnum.COUNT

		INSERT INTO TEMP_US_OUS_FORECAST (C205_PART_NUMBER_ID,C4052_TTP_DETAIL_ID,C901_LEVEL_id,US_FCST_1,US_FCST_2 ,US_FCST_3,US_FCST_4,US_FCST_5,
		US_FCST_6,US_FCST_7,US_FCST_8,US_FCST_9,US_FCST_10,US_FCST_11,US_FCST_12,OUS_FCST_1,OUS_FCST_2,OUS_FCST_3,OUS_FCST_4,OUS_FCST_5,OUS_FCST_6,OUS_FCST_7
		,OUS_FCST_8,OUS_FCST_9,OUS_FCST_10,OUS_FCST_11,OUS_FCST_12) VALUES (arr_pnum(i),p_ttp_detail_id,p_lvl_id,arr_m1(i),arr_m2(i),arr_m3(i),arr_m4(i),arr_m5(i),
		arr_m6(i),arr_m7(i),arr_m8(i),arr_m9(i),arr_m10(i),arr_m11(i),arr_m12(i),arr_ous_m1(i),arr_ous_m2(i),arr_ous_m3(i),arr_ous_m4(i),arr_ous_m5(i),
		arr_ous_m6(i),arr_ous_m7(i),arr_ous_m8(i),arr_ous_m9(i),arr_ous_m10(i),arr_ous_m11(i),arr_ous_m12(i));


		EXIT WHEN cur_usous_fcst_by_month%NOTFOUND;
	    END LOOP;
	    CLOSE cur_usous_fcst_by_month;


	END gm_ld_us_ous_forecast;

	  /*******************************************************
   * Description : Function to get specifc month forecast for parent need calculation
   * Author 	 : Paddy
   *******************************************************/
	FUNCTION get_oppr_mon_fcst_qty
	(
		p_ttp_detail_id 	T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE ,
		p_lvl_id         T4040_DEMAND_SHEET.C901_LEVEL_id%TYPE  ,
        p_pnum              T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE ,
        p_fcst_mon          VARCHAR2,
        p_type            VARCHAR2 DEFAULT NULL
	)
    RETURN NUMBER
	IS
	/*  Description     : THIS FUNCTION RETURNS FORECAST QTY FOR SPECIFIC MONTH
	*/
	    v_qty          NUMBER;
	    v_fcst_mon     VARCHAR2(50);
	    v_str VARCHAR2 (2000);

	BEGIN

	  v_fcst_mon := p_fcst_mon;

	    BEGIN
	    v_str := 'SELECT NVL ('||p_fcst_mon||', 0)  FROM TEMP_US_OUS_FORECAST  WHERE C4052_TTP_DETAIL_ID= '|| p_ttp_detail_id ||' AND  C901_LEVEL_ID='||p_lvl_id||' AND  C205_PART_NUMBER_ID='''||p_pnum||'''';

	    IF p_type = 'WW_FCST'
	    THEN
	      v_str := 'SELECT NVL ('||p_fcst_mon||', 0)  FROM T4055_TTP_SUMMARY  WHERE C4052_TTP_DETAIL_ID= '|| p_ttp_detail_id ||' AND C4055_VOID_FL IS NULL AND C901_LEVEL_ID='||p_lvl_id||' AND  C205_PART_NUMBER_ID='''||p_pnum||'''';
	    END IF;
	  --  DBMS_OUTPUT.PUT_LINE(':v_str:'||v_str);
	    EXECUTE IMMEDIATE v_str INTO v_qty;


    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            RETURN 0;
    END;

    RETURN v_qty;
	END get_oppr_mon_fcst_qty;
	
   /*******************************************************
   * Description : This procedure for to load monthly summary into t4056_ttp_mon_summary
   * Author 	 : Paddy
   *******************************************************/
	
	PROCEDURE gm_sav_ttp_mon_summary (
        p_ttp_dtls_id IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE,        
        p_level_id    IN t4056_ttp_mon_summary.c901_level_id%TYPE,
        p_level_value IN t4056_ttp_mon_summary.c901_level_value%TYPE,
        p_user_id     IN t4056_ttp_mon_summary.c4056_updated_by%TYPE
        )
AS
    v_order_qty t4056_ttp_mon_summary.c4056_order_qty%TYPE;
    v_order_cost t4056_ttp_mon_summary.c4056_order_cost%TYPE;
    --
    v_last_3_avg_qty NUMBER;
    v_last_3_avg_cost NUMBER;
    --
    v_ttp_link_date DATE;
BEGIN
    --1.  to get the order qty and cost
     SELECT SUM (NVL(c4055_total_order_qty, 0)), ROUND(SUM (NVL(c4055_total_order_cost,0)))
       INTO v_order_qty, v_order_cost
       FROM t4055_ttp_summary
      WHERE c4052_ttp_detail_id = p_ttp_dtls_id
      	AND c901_level_id       = p_level_id
      	AND c901_level_value    = p_level_value
        AND c4055_void_fl      IS NULL;
    
    -- 2. to get the load date
    SELECT  c4052_ttp_link_date
    	INTO v_ttp_link_date
      FROM t4052_ttp_detail
      WHERE c4052_ttp_detail_id = p_ttp_dtls_id
      AND c4052_void_fl     IS NULL;
      
    -- to update the monthly summary
    
     UPDATE t4056_ttp_mon_summary
    SET c4056_order_qty         = v_order_qty, c4056_order_cost = v_order_cost, c4056_updated_by = p_user_id
      , c4056_updated_date      = CURRENT_DATE
      WHERE c4052_ttp_detail_id = p_ttp_dtls_id
      	AND c901_level_id       = p_level_id
      	AND c901_level_value    = p_level_value
        AND c4056_void_fl IS NULL;
        
    -- If no data then, insert the new records
    
        
    IF (SQL%ROWCOUNT       = 0) THEN
         INSERT
           INTO t4056_ttp_mon_summary
            (
                c4056_ttp_mon_summary_id, c901_level_id, c901_level_value
              , c4052_ttp_detail_id, c4056_load_date, c4056_order_qty
              , c4056_order_cost, c4056_created_by, c4056_created_date
            )
            VALUES
            (
                s4056_ttp_mon_summary_id.NEXTVAL, p_level_id, p_level_value
              , p_ttp_dtls_id, v_ttp_link_date, v_order_qty
              , v_order_cost, p_user_id, CURRENT_DATE
            ) ;
    END IF;
    
END gm_sav_ttp_mon_summary;

	/*******************************************************
	* Description : This procedure used to void the TTP month summary details
	* Author   : Mani
	*******************************************************/
	PROCEDURE gm_void_ttp_mon_summary (
	        p_ttp_mon_sum_id IN t4056_ttp_mon_summary.c4056_ttp_mon_summary_id%TYPE,
	        p_user_id        IN t4056_ttp_mon_summary.c4056_updated_by%TYPE)
	AS
	    v_cnt NUMBER;
	BEGIN
		-- to get the count
		
	     SELECT COUNT (1)
	       INTO v_cnt
	       FROM t4056_ttp_mon_summary
	      WHERE c4056_ttp_mon_summary_id = p_ttp_mon_sum_id
	        AND c4056_void_fl           IS NULL;
	    -- validate    
	    IF v_cnt                         = 0 THEN
	        raise_application_error ('-20999', 'TTP Montly Summary Invalid/already voided ') ;
	    END IF;
	    
	    
	     UPDATE t4056_ttp_mon_summary
	    SET c4056_void_fl                = 'Y', C4056_UPDATED_BY = p_user_id, C4056_UPDATED_DATE = CURRENT_DATE
	      WHERE c4056_ttp_mon_summary_id = p_ttp_mon_sum_id
	        AND c4056_void_fl           IS NULL;
	        
	END gm_void_ttp_mon_summary;
	/*******************************************************
	* Description : This procedure used to update SET PAR for multipart
	*******************************************************/
		PROCEDURE gm_ld_mp_SetPAR (
		p_ttp_detail_id 	IN T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE ,
		p_lvl_value         IN T4040_DEMAND_SHEET.c901_level_value%TYPE ,
		p_inv_lck_id        IN T4040_DEMAND_SHEET.C250_INVENTORY_LOCK_ID%TYPE ,
		p_load_dt	        IN T4055_TTP_SUMMARY.C4055_LOAD_DATE%TYPE ,
		p_request_by   		IN	t4020_demand_master.c4020_created_by%TYPE
	)
	AS
	v_forecast_month NUMBER;

	type pnum_array IS     TABLE OF  T205_PART_NUMBER.c205_part_number_id%TYPE;
	type setpar_array IS    TABLE OF  T4055_TTP_SUMMARY.C4055_SET_PAR%TYPE ;


	arr_pnum pnum_array;
	arr_setpar setpar_array;


	Cursor cur_setpar
	IS
		/* New Table Map */
		 SELECT t4042.c205_part_number_id pnum, t4042.c4042_qty qty 
           FROM t4040_demand_sheet t4040, t4042_mp_demand_sheet_detail t4042
          WHERE t4040.c4040_demand_sheet_id=t4042.c4040_demand_sheet_id
          AND c4052_ttp_detail_id=p_ttp_detail_id
          AND t4042.c901_type=4000113
          --set par
          AND t4040.c4040_void_fl IS NULL
          AND t4042.c4042_period= p_load_dt;

	BEGIN
		SELECT get_rule_value('FORECASTMONTHS','ORDERPLANNING')
		INTO 	v_forecast_month
		FROM 	DUAL;


		OPEN cur_setpar;
		LOOP
		FETCH cur_setpar BULK COLLECT INTO arr_pnum,arr_setpar LIMIT 500;

		FORALL i in 1.. arr_pnum.COUNT

			UPDATE t4055_ttp_summary SET
					c4055_set_par = arr_setpar(i)
			WHERE c4052_ttp_detail_id = p_ttp_detail_id
			AND c205_part_number_id = arr_pnum(i)
			AND c901_level_value = p_lvl_value
			AND c4055_void_fl IS NULL;

		EXIT WHEN cur_setpar%NOTFOUND;
	    END LOOP;
	    CLOSE cur_setpar;


	END gm_ld_mp_SetPAR;
	
	/*******************************************************
	* Description : This procedure used to update OTN for multipart
	*******************************************************/
		PROCEDURE gm_ld_mp_OTN (
		p_ttp_detail_id 	IN T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE ,
		p_lvl_value         IN T4040_DEMAND_SHEET.c901_level_value%TYPE ,
		p_inv_lck_id        IN T4040_DEMAND_SHEET.C250_INVENTORY_LOCK_ID%TYPE ,
		p_load_dt	        IN T4055_TTP_SUMMARY.C4055_LOAD_DATE%TYPE ,
		p_request_by   		IN	t4020_demand_master.c4020_created_by%TYPE
	)
	AS
	v_forecast_month NUMBER;

	type pnum_array IS     	TABLE OF  T205_PART_NUMBER.c205_part_number_id%TYPE;
	type otn_array IS    	TABLE OF  T4055_TTP_SUMMARY.C4055_SET_PAR%TYPE ;


	arr_pnum pnum_array;
	arr_ww_otn otn_array;
	arr_us_otn otn_array;
	arr_ous_otn otn_array;


	Cursor cur_OTN
	IS

		  SELECT *FROM
		(
		SELECT  part_id, level_id, otn_qty
		FROM    
        (SELECT 'WW' Level_Id, t4042.c205_part_number_id part_id, t4042.c4042_qty otn_qty
           FROM t4040_demand_sheet t4040, t4042_mp_demand_sheet_detail t4042
          WHERE t4040.c4040_demand_sheet_id=t4042.c4040_demand_sheet_id
          AND c4052_ttp_detail_id=p_ttp_detail_id
          AND t4042.c901_type=4000112
          AND t4040.c4040_void_fl IS NULL
          AND t4042.c4042_period= p_load_dt
        UNION ALL
        SELECT 'US' Level_Id, t4042.c205_part_number_id part_id, t4042.c4042_qty otn_qty
           FROM t4040_demand_sheet t4040, t4042_mp_demand_sheet_detail t4042
          WHERE t4040.c4040_demand_sheet_id=t4042.c4040_demand_sheet_id
          AND c4052_ttp_detail_id=p_ttp_detail_id
          AND t4042.c901_type=4000119
          AND t4040.c4040_void_fl IS NULL
          AND t4042.c4042_period= p_load_dt
          UNION ALL
           SELECT 'OUS' Level_Id, t4042.c205_part_number_id part_id, t4042.c4042_qty otn_qty
           FROM t4040_demand_sheet t4040, t4042_mp_demand_sheet_detail t4042
          WHERE t4040.c4040_demand_sheet_id=t4042.c4040_demand_sheet_id
          AND c4052_ttp_detail_id=p_ttp_detail_id
          AND t4042.c901_type=4000120
          AND t4040.c4040_void_fl IS NULL
          AND t4042.c4042_period= p_load_dt)
          )
		  pivot
		  (max(otn_qty) FOR level_id in ('WW' as "WW",'US' as "US",'OUS' as "OUS"));


	BEGIN

		OPEN cur_OTN;
		LOOP
		FETCH cur_OTN BULK COLLECT INTO arr_pnum,arr_ww_otn,arr_us_otn,arr_ous_otn LIMIT 500;

		FORALL i in 1.. arr_pnum.COUNT

			UPDATE t4055_ttp_summary SET
					c4023_otn = arr_ww_otn(i),
					c4023_us_otn = arr_us_otn(i),
					c4023_ous_otn = arr_ous_otn(i)
			WHERE c4052_ttp_detail_id = p_ttp_detail_id
			AND c205_part_number_id = arr_pnum(i)
			AND c901_level_value = p_lvl_value
			AND c4055_void_fl IS NULL;

		EXIT WHEN cur_OTN%NOTFOUND;
	    END LOOP;
	    CLOSE cur_OTN;


	END gm_ld_mp_OTN;
	
	/*******************************************************
	* Description : This procedure used to handle PAR value <FCST qty
	*******************************************************/
	
	PROCEDURE gm_upd_forecast_qty (
		p_ttp_detail_id 	IN T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE ,
		p_load_dt	        IN T4055_TTP_SUMMARY.C4055_LOAD_DATE%TYPE ,
		p_request_by   		IN	t4020_demand_master.c4020_created_by%TYPE
	)
	AS
	
	
	CURSOR Forecast_diff
		IS
			 select * from (
        		select  
        			c205_part_number_id, 
        			(NVL(c4055_forecast_qty,0) -(NVL(c4055_us_forecast_qty,0)+ NVL(c4055_ous_forecast_qty,0))) fcst_diff
        		from 
        			t4055_ttp_summary 
        		where 
        			c4052_ttp_detail_id=p_ttp_detail_id
        			and c4055_void_fl is null
        		) where fcst_diff > 0; 
        		
	BEGIN
		
		
		
        	FOR cur_diff in Forecast_diff
        	LOOP
        		
        		UPDATE t4055_ttp_summary
        		SET	   c4055_us_forecast_qty = nvl(c4055_us_forecast_qty,0) + nvl(cur_diff.fcst_diff,0),
        			   c4055_updated_date = current_date,
					   c4055_updated_by = p_request_by        			
        		WHERE c4052_ttp_detail_id = p_ttp_detail_id
        		AND   c4055_void_fl IS NULL
        		AND   c901_level_id=102580
        		AND   c4055_load_date = p_load_dt
        		AND   c205_part_number_id = cur_diff.c205_part_number_id; 
        	
        	END LOOP;
		
	END gm_upd_forecast_qty;
	


/* Procedure to load the Safety Stock Qty for the World Wide Sheet.
*  The Cursor will get the Primary Demand Template for the parts available in the TTP.
* This will avoid double counting of Safety Stock Qty in non primary sheet.
*  Author : Rajeshwaran.v
*/ 
PROCEDURE gm_ld_safety_stock (
		p_ttp_detail_id 	IN T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE ,
		p_lvl_value         IN T4040_DEMAND_SHEET.c901_level_value%TYPE  
	)
	AS
	v_forecast_month NUMBER;

	type pnum_array IS     	TABLE OF  T205_PART_NUMBER.c205_part_number_id%TYPE;
	type ss_array IS    	TABLE OF  T4055_TTP_SUMMARY.C4025_SAFETY_STOCK_QTY%TYPE ;


	arr_pnum pnum_array;
	arr_ss_qty ss_array;


	Cursor cur_SStock
	IS

		 
		SELECT 
			
			t4011.c205_part_number_id ,
			t4025.c4025_qty
			FROM t4020_demand_master t4020,
				t4021_demand_mapping t4021,
				t4051_ttp_demand_mapping t4051,
				t4011_group_detail t4011,
				t4010_group t4010,
				t4050_ttp t4050,
				t4052_ttp_detail t4052,
				t4025_safety_stock t4025
			WHERE t4020.c4020_parent_demand_master_id IS NULL
			AND t4010.c4010_group_id                   = t4011.c4010_group_id
			AND t4021.c4020_demand_master_id           = t4020.c4020_demand_master_id
			AND t4020.c4020_demand_master_id           = t4051.c4020_demand_master_id
			AND t4051.c4050_ttp_id                     = t4050.c4050_ttp_id
			AND t4050.c4050_ttp_id                     = t4052.c4050_ttp_id
			AND t4011.c205_part_number_id              = t4025.c205_part_number_id
			AND t4010.c4010_void_fl                   IS NULL
			AND t4050.c4050_void_fl                   IS NULL
			AND t4052.c4052_void_fl                   IS NULL
			AND t4020.c4020_void_fl                   IS NULL
			AND t4025.c4025_void_fl                   IS NULL
			AND t4011.c901_part_pricing_type           =52080
			AND t4021.c4021_ref_id                     = t4010.c4010_group_id
			AND t4021.c901_ref_type                    = 40030
			AND t4020.c901_demand_type                 =4000103	
			AND t4052.c4052_ttp_detail_id              = p_ttp_detail_id
			ORDER BY t4011.c205_part_number_id;


	BEGIN

		OPEN cur_SStock;
		LOOP
		FETCH cur_SStock BULK COLLECT INTO arr_pnum,arr_ss_qty LIMIT 500;

		FORALL i in 1.. arr_pnum.COUNT

			UPDATE T4055_TTP_SUMMARY SET
					C4025_SAFETY_STOCK_QTY = arr_ss_qty(i)
			WHERE C4052_TTP_DETAIL_ID = p_ttp_detail_id
			AND c205_part_number_id = arr_pnum(i)
			AND c901_level_value = p_lvl_value
			AND c4055_void_fl IS NULL;

		EXIT WHEN cur_SStock%NOTFOUND;
	    END LOOP;
	    CLOSE cur_SStock;


	END gm_ld_safety_stock;	
	
	
	/* Procedure to load the Safety Stock Qty for the Multi Part Sheet.
	*  Author : Rajeshwaran.v
	*/ 
PROCEDURE gm_ld_mp_safety_stock (
		p_ttp_detail_id 	IN T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE ,
		p_lvl_value         IN T4040_DEMAND_SHEET.c901_level_value%TYPE
	)
	AS
	v_forecast_month NUMBER;

	type pnum_array IS     	TABLE OF  T205_PART_NUMBER.c205_part_number_id%TYPE;
	type ss_array IS    	TABLE OF  T4055_TTP_SUMMARY.C4025_SAFETY_STOCK_QTY%TYPE ;


	arr_pnum pnum_array;
	arr_ss_qty ss_array;

	
	
	-- AS the Multi Part will have the TTP Summary data, we will be using this table to get the SS qty for MultiPart Parts.
	Cursor cur_SStock
	IS

		 
		SELECT t4025.C205_PART_NUMBER_ID,t4025.c4025_qty 
		FROM T4055_TTP_SUMMARY t4055,T4025_SAFETY_STOCK t4025
		WHERE t4055.C205_PART_NUMBER_ID = t4025.C205_PART_NUMBER_ID
		AND t4055.C4055_VOID_FL is null
		AND t4025.c4025_void_fl is null
		AND t4055.C4052_TTP_DETAIL_ID = p_ttp_detail_id;


	BEGIN

		OPEN cur_SStock;
		LOOP
		FETCH cur_SStock BULK COLLECT INTO arr_pnum,arr_ss_qty LIMIT 500;

		FORALL i in 1.. arr_pnum.COUNT

			UPDATE T4055_TTP_SUMMARY SET
					C4025_SAFETY_STOCK_QTY = arr_ss_qty(i)
			WHERE C4052_TTP_DETAIL_ID = p_ttp_detail_id
			AND c205_part_number_id = arr_pnum(i)
			AND c901_level_value = p_lvl_value
			AND C4055_VOID_FL IS NULL;

		EXIT WHEN cur_SStock%NOTFOUND;
	    END LOOP;
	    CLOSE cur_SStock;


	END gm_ld_mp_safety_stock;
	
END gm_pkg_oppr_ld_summary;
/