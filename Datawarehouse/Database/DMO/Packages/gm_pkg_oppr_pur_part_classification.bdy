CREATE or REPLACE PACKAGE BODY gm_pkg_oppr_pur_part_classification
IS

/**********************************************************************************************
* description      : Calculate the Purchasing A/B/C Classification for a Part based on the every month
* 					 forecast sales 
* author           : Prabhu vigneshwaran M D 
***********************************************************************************************/
PROCEDURE GM_OP_PART_USAGE_MAIN 

AS 
v_multi_sheet_id t4070_division_mp_mapping.c4020_demand_master_id%type;
v_division_id    t4070_division_mp_mapping.c1910_division_id%type;
v_load_date      t4071_purchase_part_rank.c4071_load_date%type;
v_pur_part_rank_id        t4071_purchase_part_rank.c4071_purchase_part_rank_id%type;
subject 	   VARCHAR2 (100);
to_mail 	   t906_rules.c906_rule_value%TYPE;
mail_body	   VARCHAR2 (3000);

CURSOR multi_part_curr IS 
--Read and get the status of multi part sheet from T4070_Division_mp_mapping and T4040_demand_sheet
SELECT t4070.c4020_demand_master_id mul_sheet_id,t4040.c901_status status 
    FROM t4040_demand_sheet t4040,
          t4070_division_mp_mapping t4070
     WHERE t4040.c4020_demand_master_id = t4070.c4020_demand_master_id
       AND t4040.c901_status = 50552--Locked
       AND t4040.c4040_demand_period_dt = trunc(sysdate, 'mm')
       AND t4040.c4040_void_fl is null
       AND t4070.c4070_void_fl is null
       GROUP BY t4070.c4020_demand_master_id,t4040.c901_status;
       
CURSOR  division_curr  IS  
  SELECT t4070.c1910_division_id division_id, t1910.c1910_division_name division_nm
    FROM t4070_division_mp_mapping t4070,t1910_division t1910
   WHERE t4070.c1910_division_id  = t1910.c1910_division_id 
   AND t4070.c4020_demand_master_id = v_multi_sheet_id
   AND t4070.c1910_division_id not in 
   (SELECT t4071.c1910_division_id 
           FROM t4071_purchase_part_rank t4071 
          WHERE t4071.c4071_load_date = trunc(sysdate, 'mm')
           AND t4071.c4071_process_fl='Y'
           AND t4071.c4071_void_fl is null)
   AND t4070.c4070_void_fl is null;
   
BEGIN
	
	SELECT trunc(sysdate, 'mm') INTO v_load_date FROM dual;--
      FOR mpc IN multi_part_curr				
      LOOP				
	       v_multi_sheet_id := mpc.mul_sheet_id;
	       
	        FOR mp_div IN division_curr			
	        LOOP	
	        
		       v_division_id := mp_div.division_id;
		       v_pur_part_rank_id := s4071_purchase_part_rank_id.nextval;
		       
               --INSERT in T4071_GOP_PART_RANK with process fl null	
                gm_sav_purchase_part_rank (v_pur_part_rank_id, v_division_id, v_load_date);
                
		       --call gm_pkg_oppr_ld_summary.gm_op_process_part_ranking(division_id,v_rank_id)
                gm_op_process_part_ranking(v_division_id,v_pur_part_rank_id);
                
              --UPDATE T4071_GOP_PART_RANK with process fl='Y'
                gm_sav_purchase_part_rank (v_pur_part_rank_id, v_division_id, v_load_date);
                
              --Send Mail When part ranking completed                
                subject := mp_div.division_nm || ' Part ranking completed';
                mail_body := mp_div.division_nm || ' Part ranking completed';
                to_mail := get_rule_value ('PURCHASE_PART_RANK', 'EMAIL');
                gm_com_send_email_prc (to_mail, subject, mail_body);

           END LOOP;			
      END LOOP;
      
END GM_OP_PART_USAGE_MAIN;

/**********************************************************************************************
* description      : Calculate the Purchasing A/B/C Classification for a Part based on the every month
* 					 forecast sales 
* author           : Prabhu vigneshwaran M D 
***********************************************************************************************/
PROCEDURE gm_op_process_part_ranking(
      p_division_id  IN  t4070_division_mp_mapping.c1910_division_id%TYPE,
      p_rank_id      IN  t4071_purchase_part_rank.c4071_purchase_part_rank_id%TYPE
      )
      
      AS
      --Get Partnumber and qty from t4060_part_forecast_qty
      CURSOR cur_part_sale
      IS
      SELECT  t4060.c205_part_number_id pnum,sum(t4060.c4060_qty )qty
	     FROM t205_part_number t205,t202_project t202,t4060_part_forecast_qty t4060
	     WHERE t202.c202_project_id = t205.c202_project_id
	       AND t205.c205_part_number_id = t4060.c205_part_number_id
	       AND t4060.c4060_period >= trunc(last_day(current_date)-1, 'mm')
	       AND t4060.c4060_period <=  trunc(last_day(current_date)-1, 'mm')+365
	       AND t4060.c901_forecast_type = '40020'--Sales
	       AND t202.c1910_division_id = p_division_id
	       AND t4060.c901_level_value in (select c906_rule_id from t906_rules where c906_rule_grp_id='GOP_PART_RANK_LEVEL' and c906_void_fl is null)
	       AND t202.c202_void_fl is null
	       GROUP BY t4060.c205_part_number_id
	       ORDER BY 2 desc;
	
	v_pnum t502_item_order.C205_PART_NUMBER_ID%TYPE;
    v_percentage NUMBER(20,4);
    v_total_sales t4072_purchase_part_rank_detail.c4072_sales_percent%TYPE;--PMT-46404(BUG FIX)
    v_part_percent t4072_purchase_part_rank_detail.c4072_sales_percent%TYPE;--PMT-46404(BUG FIX)
    v_part_cumul_per t4072_purchase_part_rank_detail.c4072_sales_per_overall%TYPE;--PMT-46404(BUG FIX)
    v_rank NUMBER;
    v_Arank_Percent  NUMBER(5,2);
    v_Brank_percent  NUMBER(5,2);
  
    v_last_month_rank t4072_purchase_part_rank_detail.c4072_rank%TYPE;
    v_previous_last_mon_rank t4072_purchase_part_rank_detail.c4072_rank%TYPE;  
    --Get partnumber,qty ,rank id from T4072_PURCHASE_PART_RANK_DETAIL
    Cursor cur_part_sale_temp
    IS
     SELECT t4072.c205_part_number_id pnum, 
     		t4072.c4072_sales_qty qty ,
     		t4072.c4071_purchase_part_rank_id rank_id,
     		t4071.c1910_division_id division_id
      FROM t4072_purchase_part_rank_detail t4072,t4071_purchase_part_rank t4071
      WHERE t4071.c4071_purchase_part_rank_id = t4072.c4071_purchase_part_rank_id
      AND t4071.c4071_load_date = trunc(sysdate, 'mm')
      AND t4072.c4071_purchase_part_rank_id = p_rank_id
      ORDER BY t4072.c4072_sales_qty desc;
	
     BEGIN
	     
	        SELECT c4070c_to_percent INTO v_Arank_Percent FROM t4070c_rank_config  WHERE c901_rank_cd ='108320';
	        SELECT c4070c_to_percent INTO v_Brank_percent FROM t4070c_rank_config  WHERE c901_rank_cd ='108321';  
		
	        -- calculate v_total_sales from cur_part_sale cursor and Insert into T4072_PURCHASE_PART_RANK_DETAIL
	         v_total_sales := 0;
    		 FOR part_sale IN cur_part_sale
       		 LOOP
				 v_total_sales := v_total_sales + part_sale.qty; 
				 gm_sav_purchase_part_rank_detail (p_rank_id, part_sale.pnum, part_sale.qty);	 
      		 END LOOP;
      		
      		
      		DBMS_OUTPUT.PUT_LINE('>> Total:'||v_total_sales);
      		
      		
--          Designate A/B/C Classification for each Part per the following based on the running total of percentage of sales                    
	 		v_part_cumul_per := 0;
	 		FOR part_sale_temp IN cur_part_sale_temp
	 		LOOP
	 			IF(v_total_sales <> 0)
				THEN
				v_part_percent := ROUND((part_sale_temp.qty/ v_total_sales)*100,6);--Update round v_part_percent value to 6-PMT-46404(BUG FIX)
				v_part_cumul_per := v_part_cumul_per + v_part_percent ;
				
				if(v_part_cumul_per <= v_Arank_Percent )
				THEN
					v_rank := '108320';--A Rank
					ELSIF ( v_part_cumul_per > v_Arank_Percent AND v_part_cumul_per <= v_Brank_percent )
				THEN
					v_rank := '108321';--B Rank
				ELSE
					v_rank := '108322';--C Rank
				END IF;	
		     --After current month Rank insert then read from T4072_MON_PART_RANK_DETAIL 
		     --and get the last and previous month rank for each part and update in T4072_MON_PART_RANK_DETAIL	
		    v_last_month_rank        := get_last_month_rank (part_sale_temp.division_id,part_sale_temp.pnum, -1);
            v_previous_last_mon_rank := get_last_month_rank (part_sale_temp.division_id,part_sale_temp.pnum, -2);
	
		      DBMS_OUTPUT.PUT_LINE('>>part_sale_temp.pnum:'||part_sale_temp.pnum||'>> part_sale_temp.qty:'||part_sale_temp.qty||':v_part_percent:'||v_part_percent||':v_part_cumul_per:'||v_part_cumul_per||':v_rank:'||v_rank);       
				
		      UPDATE t4072_purchase_part_rank_detail 
				SET c4072_rank = v_rank,
				    c4072_sales_percent = v_part_percent,
					c4072_sales_per_overall = v_part_cumul_per,
					c4072_last_mon_rank =v_last_month_rank ,
        			c4072_prev_last_mon_rank =v_previous_last_mon_rank 
				WHERE c205_part_number_id = part_sale_temp.pnum
				AND c4071_purchase_part_rank_id = part_sale_temp.rank_id;
				 
				END IF;	
			
		END LOOP;
		COMMIT;
END gm_op_process_part_ranking;	
 /**********************************************************************************************
* description      : This procedure used to insert t4071_purchase_part_rank table
* 					  
* author           : Prabhu vigneshwaran M D 
***********************************************************************************************/

PROCEDURE gm_sav_purchase_part_rank(
 p_pur_part_rank_id    IN    t4071_purchase_part_rank.c4071_purchase_part_rank_id%type,
 p_division_id         IN     t4070_division_mp_mapping.c1910_division_id%type,
 p_load_date           IN     t4071_purchase_part_rank.c4071_load_date%type
)
AS
BEGIN
	            UPDATE t4071_purchase_part_rank t4071
                SET   
                t4071.c4071_process_fl ='Y',
                t4071.c4071_updated_date = sysdate		
                WHERE t4071.c1910_division_id = p_division_id
                AND t4071.c4071_load_date = p_load_date
                and t4071.c4071_purchase_part_rank_id = p_pur_part_rank_id
                AND t4071.c4071_void_fl is NULL;

	IF (SQL%ROWCOUNT = 0) THEN
			    --INSERT in T4071_GOP_PART_RANK with process fl null	
		       INSERT INTO t4071_purchase_part_rank(c4071_purchase_part_rank_id,c1910_division_id, c4071_load_date, c4071_process_fl)
               VALUES(p_pur_part_rank_id,p_division_id,p_load_date, NULL); 	
	END IF;
	
END gm_sav_purchase_part_rank;
 /**********************************************************************************************
* description      : This procedure used to insert t4072_purchase_part_rank_detail table
* 					  
* author           : Prabhu vigneshwaran M D 
***********************************************************************************************/
	
	
PROCEDURE gm_sav_purchase_part_rank_detail(
 p_rank_id      IN  t4071_purchase_part_rank.c4071_purchase_part_rank_id%TYPE,
 p_part_num     IN  t4060_part_forecast_qty.c205_part_number_id%TYPE,
 p_sale_qty     IN  t4060_part_forecast_qty.c4060_qty%TYPE
)
AS
BEGIN
	             INSERT 
                 INTO t4072_purchase_part_rank_detail 
                 		(c4072_part_rank_detail_id,
                	     c4071_purchase_part_rank_id,
                 		 c205_part_number_id,
                		 c4072_sales_qty)
                 VALUES(s4072_part_rank_detail_id.nextval, 
                 		 p_rank_id,
                         p_part_num, 
                         p_sale_qty); 
END gm_sav_purchase_part_rank_detail;
/*********************************************************************
* description      : This function used to get last month and last previous 
					 month record				  
* author           : Prabhu vigneshwaran M D 
*********************************************************************/	
FUNCTION get_last_month_rank (
	  p_division_id        IN    t4071_purchase_part_rank.c1910_division_id%type,
	  p_part_number        IN    t4072_purchase_part_rank_detail.c205_part_number_id%type,
      p_mon_num          IN     NUMBER
      )
	RETURN NUMBER
	IS
	v_rank       t4072_purchase_part_rank_detail.c4072_rank%TYPE;
	BEGIN

	   	  BEGIN
				  	SELECT c4072_rank 
				  	INTO v_rank
				  	FROM t4072_purchase_part_rank_detail t4072,
				  	     t4071_purchase_part_rank t4071 
				 	 WHERE t4071.c4071_purchase_part_rank_id = t4072.c4071_purchase_part_rank_id 
                  	 AND t4072.c205_part_number_id =  p_part_number
                  	 AND t4071.c4071_load_date = trunc(add_months(sysdate, p_mon_num),'mm')
                  	 AND t4071.c1910_division_id = p_division_id;

                 EXCEPTION
		           WHEN NO_DATA_FOUND THEN
			          v_rank:= NULL;
		         END;
		RETURN 	v_rank;
END Get_last_month_rank;

/*******************************************************
    * Author  : Prabhu vigneshwaran M D 
    * Description: This procedure used to fetch the 
                   ABC details to new table 
                   (T4071, T4072, T205, T202, T1910).
    *******************************************************/	
	PROCEDURE gm_fch_part_classification_rpt(
 		p_part_number_id IN VARCHAR2,
		p_project_id 	 IN  t202_project.c202_project_id%TYPE,
		p_division_id    IN t1910_division.c1910_division_id%TYPE,
		p_rank_id        IN t4072_purchase_part_rank_detail.c4072_rank%TYPE,
		p_load_date      IN  VARCHAR2,
		p_out_abc_rpt_dtls OUT CLOB
)

AS
 v_load_date DATE;
 v_date_fmt VARCHAR2 (20) ;
BEGIN
	
--	  SELECT  get_compdtfmt_frm_cntx ()
--       INTO v_date_fmt
--       FROM dual;
	
	   SELECT to_date(p_load_date || '/01' , 'MM/yyyy/dd')
     	 INTO v_load_date
         FROM DUAL;
	--JSON QUERY
   SELECT JSON_ARRAYAGG(JSON_OBJECT(
		'pnum' value t205.c205_part_number_id,
		'pdesc' value REGEXP_REPLACE(t205.c205_part_num_desc,'[^0-9A-Za-z]', ' '),
		'projnm' value t202.c202_project_nm,
		'divnm' value t1910.c1910_division_name,
		'salesqty' value t4072.c4072_sales_qty,
		'salespercent' value t4072.c4072_sales_percent||'',
		'salesperoverall' value t4072.c4072_sales_per_overall,
		'rank' value t901_curr_mon.c901_code_nm,
		'lastmonrank' value t901_last_mon.c901_code_nm,
		'previouslastmonrank' value t901_last_prev_mon.c901_code_nm
		)  ORDER BY t1910.c1910_division_name,t4072.c4072_sales_per_overall,t205.c205_part_number_id
  RETURNING CLOB) INTO P_OUT_ABC_RPT_DTLS 
	FROM  t205_part_number t205,
		  t202_project t202, 
		  t1910_division t1910,
		  t4071_purchase_part_rank t4071, 
          t4072_purchase_part_rank_detail t4072, 
          t901_code_lookup t901_curr_mon, 
          t901_code_lookup t901_last_mon, 
          t901_code_lookup t901_last_prev_mon
	WHERE t4071.c4071_purchase_part_rank_id = t4072.c4071_purchase_part_rank_id
    AND t205.c205_part_number_id        = t4072.c205_part_number_id
   	AND t202.c202_project_id            = t205.c202_project_id
   	AND t1910.c1910_division_id         = t202.c1910_division_id
    AND t4072.c4072_rank = t901_curr_mon.c901_code_id (+)
    AND t4072.c4072_last_mon_rank = t901_last_mon.c901_code_id (+) 
    AND t4072.c4072_prev_last_mon_rank = t901_last_prev_mon.c901_code_id (+) 
   	AND REGEXP_LIKE(NVL(t4072.c205_part_number_id,'-999') , NVL(REGEXP_REPLACE(p_part_number_id,'[+]','\+'),NVL(t4072.c205_part_number_id,'-999')))
   	AND t205.c202_project_id = DECODE (p_project_id,NULL,t205.c202_project_id, p_project_id)
   	AND t1910.c1910_division_id = decode(p_division_id,0,t1910.c1910_division_id,p_division_id)
   	AND t4072.c4072_rank = decode(p_rank_id,0,t4072.c4072_rank,p_rank_id)
    AND t4071.c4071_load_date = v_load_date
   	AND t4071.c4071_void_fl IS NULL
   	AND t202.C202_VOID_FL is NULL
   	AND t1910.C1910_VOID_FL is NULL;	
END gm_fch_part_classification_rpt;


END gm_pkg_oppr_pur_part_classification;

/