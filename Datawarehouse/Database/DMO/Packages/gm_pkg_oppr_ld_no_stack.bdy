/* Formatted on 2009/04/29 18:23 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Operations\purchasing\gm_pkg_oppr_ld_no_stack.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ld_no_stack
IS
	PROCEDURE gm_op_nostack_main
	AS
		v_demandmasterid t4020_demand_master.c4020_demand_master_id%TYPE;
		v_refid 	   t4021_demand_mapping.c4021_ref_id%TYPE;
		v_ref_type	   t4021_demand_mapping.c901_ref_type%TYPE;
		v_start_dt	   DATE;
		v_month 	   NUMBER;
		v_date		   DATE;
		v_plant_id     T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;

		-- SQL modified to only fetch world wide sheet 
        -- All the country sheet will be in stack 
		-- Below SQL is a temp fix need to reverify the same 
		CURSOR cur_sheet_details
		IS
			SELECT t4020.c4020_demand_master_id demandmasterid
					, t4020.c4020_demand_nm
						, t4021.c207_set_id refid
						 , 0 months
						 , 40031 ref_type
					  FROM t4020_demand_master t4020, t4015_global_sheet_mapping t4015 
					  ,  ( SELECT DISTINCT t520.c207_set_id, t520.c520_request_txn_id
				   FROM t520_request t520
				  WHERE t520.c520_status_fl < 30
					AND t520.c520_void_fl IS NULL
					AND t520.c520_master_request_id IS NULL
					AND t520.c901_request_source = '50616'   --order planning
					AND t520.c520_request_to IS NULL
					AND t520.c1900_company_id = 1000
					AND TRUNC (t520.c520_required_date) < v_date
					AND t520.c520_request_txn_id IN (
							SELECT t4020.c4020_demand_master_id 
							FROM   t4020_demand_master t4020, t4015_global_sheet_mapping t4015
							WHERE  t4020.c901_demand_type IN (40022, 40021)   --consignment, in house consignment
							   AND t4020.c4015_global_sheet_map_id = t4015.c4015_global_sheet_map_id
							   AND t4015.c901_level_id = 102580
							   AND t4020.c4020_void_fl IS NULL
							   AND t4020.c4020_inactive_fl IS NULL) ) t4021 
					 WHERE t4020.c901_demand_type IN (40022, 40021)   --consignment, in house consignment
					   AND t4020.c4015_global_sheet_map_id = t4015.c4015_global_sheet_map_id
					   AND t4015.c901_level_id = 102580
					   AND t4020.c4020_void_fl IS NULL
					   AND t4020.c4020_inactive_fl IS NULL
					   AND t4020.c4020_demand_master_id = t4021.c520_request_txn_id;

            /*SELECT t4020.c4020_demand_master_id demandmasterid, t4020.c4020_demand_nm, t4021.c4021_ref_id refid
                 , NVL (t4021.c4021_stack_month, 0) months, t4021.c901_ref_type ref_type
              FROM t4020_demand_master t4020, t4015_global_sheet_mapping t4015 , t4021_demand_mapping t4021 
             WHERE t4020.c901_demand_type IN (40022, 40021)   --consignment, in house consignment
               AND t4020.c4015_global_sheet_map_id = t4015.c4015_global_sheet_map_id
               AND t4015.c901_level_id = 102580   
               AND t4020.c4020_demand_master_id IN (296,297)
               AND t4020.c4020_void_fl IS NULL
               AND t4020.c4020_inactive_fl IS NULL
               AND t4020.c4020_demand_master_id = t4021.c4020_demand_master_id
               AND t4021.c901_ref_type IN (40031, 40034, 4000109);   */
	BEGIN
		v_start_dt	:= CURRENT_DATE;

		SELECT  get_rule_value('DEFAULT_PLANT','ONEPORTAL') INTO v_plant_id FROM DUAL;

		-- Change for last day [if last date then run for next month)
		IF  ( TRUNC(LAST_DAY(CURRENT_DATE)) = TRUNC(CURRENT_DATE) )
		THEN 
			v_date	:= TRUNC (CURRENT_DATE + 1, 'MON');
		ELSE
			v_date	:= TRUNC (CURRENT_DATE, 'MON');
		END IF;


		FOR var_sheet_detail IN cur_sheet_details
		LOOP
			v_demandmasterid := var_sheet_detail.demandmasterid;
			--v_setid	:= var_sheet_detail.setid;
			v_refid 	:= var_sheet_detail.refid;
			v_month 	:= var_sheet_detail.months;
			v_ref_type	:= var_sheet_detail.ref_type;
			gm_pkg_oppr_ld_no_stack.gm_op_nostack_move (v_demandmasterid, v_refid, 'L', '30301', v_month, v_ref_type);
			COMMIT;
		END LOOP;

    	-- Making order qty  to zero  (If not ordered)
    	-- To be a seperate procedure in OLAP US DB 
        UPDATE t205c_part_qty x
          SET x.c205_available_qty = 0
         , c205_last_updated_by = 303043
         , c901_action = 4302
         , c901_type = 4316
         , c205_last_updated_date = CURRENT_DATE
           , C205_LAST_UPDATE_TRANS_ID = 'OP Load Clean' 
        WHERE x.c205_available_qty > 0 AND x.c901_transaction_type = 90801 AND x.C5040_PLANT_ID 	= 	v_plant_id;
		
        -- LOG THE STATUS
		gm_pkg_cm_job.gm_cm_notify_load_info (v_start_dt, CURRENT_DATE, 'S', 'SUCCESS', 'gm_pkg_oppr_ld_no_stack');
		COMMIT;
	EXCEPTION
		WHEN OTHERS
		THEN
			ROLLBACK;
			--
			gm_pkg_cm_job.gm_cm_notify_load_info (v_start_dt
												, CURRENT_DATE
												, 'E'
												,	 SQLERRM
												  || ' Set Id '
												  || v_refid
												  || ' Demand Master ID '
												  || v_demandmasterid
												, 'gm_pkg_oppr_ld_no_stack'
												 );
			COMMIT;
	END gm_op_nostack_main;

	/***********************************************************
	* Description : Procedure for no stack move
	L means Load, S means from the screen
	************************************************************/
	PROCEDURE gm_op_nostack_move (
		p_demandmasterid   IN	t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_refid 		   IN	t4021_demand_mapping.c4021_ref_id%TYPE
	  , p_type			   IN	VARCHAR2
	  , p_userid		   IN	t101_user.c101_user_id%TYPE
	  , p_months		   IN	NUMBER
	  , p_ref_type		   IN	VARCHAR2
	  , p_request_id       IN   t520_request.c520_request_id%TYPE DEFAULT NULL
	)
	AS
		v_count 	   NUMBER;
		v_growth_count NUMBER;
		v_straction    VARCHAR2 (200);
		v_str		   VARCHAR2 (3500);
		v_date		   DATE := ADD_MONTHS (TRUNC (CURRENT_DATE, 'MON'), p_months * -1);
		v_reqid 	   t520_request.c520_request_id%TYPE;
		v_required_dt	DATE;
		crlf		   VARCHAR2 (2) := CHR (13) || CHR (10);

		CURSOR cur_request_details
		IS
			SELECT	 c520_request_id reqid , t520.c520_required_date required_dt
				FROM t520_request t520
			   WHERE t520.c520_request_txn_id = p_demandmasterid
			     AND c520_request_id = NVL(p_request_id,c520_request_id)
				 --AND t520.c207_set_id = p_setid
				 AND NVL (t520.c207_set_id, '-9999') = NVL (p_refid, '-9999')
				 AND t520.c520_status_fl < 30
				 AND t520.c520_void_fl IS NULL
				 AND t520.c520_master_request_id IS NULL
				 AND t520.c901_request_source = '50616'   --order planning
				 AND t520.c520_request_to IS NULL
			ORDER BY t520.c520_required_date;
	BEGIN

		-- Change for last day [if last date then run for next month)
		IF  ( TRUNC(LAST_DAY(CURRENT_DATE)) = TRUNC(CURRENT_DATE) )
		THEN 
			v_date	:= ADD_MONTHS (TRUNC (CURRENT_DATE + 1, 'MON'), p_months * -1);
		ELSE
			v_date	:= ADD_MONTHS (TRUNC (CURRENT_DATE, 'MON'), p_months * -1);
		END IF;

		--	Check if any open request need to be moved to next month
		SELECT COUNT (1)
		  INTO v_count
		  FROM t520_request t520
		 WHERE t520.c520_request_txn_id = p_demandmasterid
		   AND NVL (t520.c207_set_id, '-9999') = NVL (p_refid, '-9999')
		   AND t520.c520_status_fl < 30   --10, 'Back Order' , 15, 'Back Log' , 20, 'Ready to Consign'
		   AND t520.c520_request_to IS NULL
		   AND t520.c520_void_fl IS NULL
		   AND t520.c520_master_request_id IS NULL
		   AND t520.c901_request_source = '50616'	--order planning
		   -- AND TRUNC (t520.c520_required_date) < TRUNC (CURRENT_DATE);
		   AND TRUNC (t520.c520_required_date) < v_date;

		IF v_count > 0
		THEN
			--	Check if the sheet has growth forecast associated for selected sheet and set
			SELECT	 COUNT (1)
				INTO v_growth_count
				FROM t4030_demand_growth_mapping t4030, t4031_growth_details t4031
			   WHERE t4031.c4030_demand_growth_id = t4030.c4030_demand_growth_id
				 AND t4030.c4020_demand_master_id = p_demandmasterid
				 AND t4030.c4030_ref_id = DECODE (p_ref_type, 40031, p_refid, t4030.c4030_ref_id)
				 --   AND t4030.c901_ref_type = 20296
				 AND t4030.c901_ref_type =
										  DECODE (p_ref_type
												, 40031, 20296
												, 20298
												 )	 ---20296, setid,  20298, part# consignment
				 AND NVL (t4031.c4031_value, 0) <> 0
				 AND t4031.c4031_start_date >= v_date
				 --AND t4031.c4031_start_date >= TRUNC (CURRENT_DATE, 'MON')
				 AND t4030.c4030_void_fl IS NULL
				 AND t4031.c4031_void_fl IS NULL
			ORDER BY t4031.c4031_start_date;

			IF (v_growth_count = 0)   -- no growth forecast
			THEN
				FOR var_request_detail IN cur_request_details
				LOOP
					v_reqid 	:= var_request_detail.reqid;
					v_required_dt := var_request_detail.required_dt;
					
					-- Below proc has to be called for RQ that has passed the required date.
					if v_required_dt < v_date
					THEN
						gm_pkg_oppr_ld_no_stack.gm_op_request_action (v_reqid, p_userid, v_straction);
						
						 -- Code added becoz it was failing for vstr more than 4000 chars and it has been refered from this same package.
						  BEGIN 
							v_str		:= v_str || v_straction || crlf;
						  EXCEPTION 
						  WHEN OTHERS
						  THEN
							NULL;
						  END;
					END IF;
					 				
				END LOOP;
				
					gm_pkg_oppr_ld_no_stack.gm_op_nostack_email (p_demandmasterid, p_userid, p_refid, v_str);
	                
				RETURN;
			END IF;

			--gm_pkg_oppr_ld_no_stack.gm_op_update_required_date (p_demandmasterid, p_setid, p_type, p_userid);
			IF (p_ref_type = 40031)   --set
			THEN
				gm_pkg_oppr_ld_no_stack.gm_op_upt_required_date_set (p_demandmasterid
																 , p_refid
																 , p_type
																 , p_userid
																 , p_months
																 , p_request_id
																  );
			END IF;

			IF (p_ref_type = 40034)   --item
			THEN
				gm_pkg_oppr_ld_no_stack.gm_op_upt_required_date_item (p_demandmasterid
																  , p_refid
																  , p_type
																  , p_userid
																  , p_months
																   );
			END IF;
		END IF;
	END gm_op_nostack_move;

	/*****************************************************************
	* Description : Procedure to update the required date for unused sets.
	**********************************************************************/
	PROCEDURE gm_op_upt_required_date_set (
		p_demandmasterid   IN	t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_setid 		   IN	t4022_demand_par_detail.c205_part_number_id%TYPE
	  , p_type			   IN	VARCHAR2
	  , p_userid		   IN	t101_user.c101_user_id%TYPE
	  , p_months		   IN	NUMBER
	  , p_request_id       IN   t520_request.c520_request_id%TYPE DEFAULT NULL
	)
	AS
		v_start_date   DATE;
		v_end_date	   DATE;
		v_growth_value NUMBER := 0;
		v_nvalue	   NUMBER := 0;
		v_open_req_count NUMBER := 0;
		v_nodata	   BOOLEAN := FALSE;
		--	v_filter_date  DATE := TRUNC (CURRENT_DATE, 'MON');
		v_filter_date  DATE := ADD_MONTHS (TRUNC (CURRENT_DATE, 'MON'), p_months * -1);
		v_reqid 	   t520_request.c520_request_id%TYPE;
		v_str		   VARCHAR2 (3500);
		v_old_req_date t520_request.c520_required_date%TYPE;
		v_straction    VARCHAR2 (200);
		crlf		   VARCHAR2 (2) := CHR (13) || CHR (10);
		v_type				t906_rules.c906_rule_value%TYPE;
		CURSOR cur_request_details
		IS
			SELECT	 c520_request_id reqid, c520_required_date old_reqdate
				FROM t520_request t520
			   WHERE t520.c520_request_txn_id = p_demandmasterid
			     AND t520.c520_request_id = NVL(p_request_id,t520.c520_request_id)
				 AND t520.c207_set_id = p_setid
				 AND t520.c520_status_fl < 30
				 AND t520.c520_void_fl IS NULL
				 AND t520.c520_master_request_id IS NULL
				 AND t520.c901_request_source = '50616'   --order planning
				 AND t520.c520_request_to IS NULL
			ORDER BY t520.c520_required_date;
	BEGIN
		FOR var_request_detail IN cur_request_details
		LOOP
			v_reqid 	:= var_request_detail.reqid;
			v_old_req_date := var_request_detail.old_reqdate;

			IF (v_growth_value = v_nvalue AND v_nodata = FALSE)
			THEN
				BEGIN
					SELECT tmptable.sdate, tmptable.edate, tmptable.gvalue
					  INTO v_start_date, v_end_date, v_growth_value
					  FROM (SELECT	 t4031.c4031_start_date sdate, t4031.c4031_end_date edate, t4031.c4031_value gvalue
								FROM t4030_demand_growth_mapping t4030, t4031_growth_details t4031
							   WHERE t4031.c4030_demand_growth_id = t4030.c4030_demand_growth_id
								 AND t4030.c4020_demand_master_id = p_demandmasterid
								 AND t4030.c4030_ref_id = p_setid
								 AND t4030.c901_ref_type = 20296
								 AND NVL (t4031.c4031_value, 0) <> 0
								 AND t4031.c4031_start_date >= v_filter_date
								 AND t4030.c4030_void_fl IS NULL
								 AND t4031.c4031_void_fl IS NULL
							ORDER BY c4031_start_date ASC) tmptable
					 WHERE ROWNUM = 1;

					SELECT COUNT (1)
					  INTO v_open_req_count
					  FROM t520_request t520
					 WHERE t520.c520_request_txn_id = p_demandmasterid
					   AND t520.c207_set_id = p_setid
					   AND t520.c520_status_fl < 30
					   AND t520.c520_request_to IS NOT NULL
					   AND t520.c520_void_fl IS NULL
					   AND t520.c520_master_request_id IS NULL
					   AND t520.c520_required_date BETWEEN v_start_date AND v_end_date;

					v_filter_date := LAST_DAY (v_end_date) + 1;
				EXCEPTION
					WHEN NO_DATA_FOUND
					THEN
						v_nodata	:= TRUE;
				END;

				v_growth_value := v_growth_value - v_open_req_count;
				v_nvalue	:= 0;
			END IF;

			IF (v_nodata = FALSE)
			THEN
				UPDATE t520_request
				   SET c520_required_date = v_end_date
					 , c520_last_updated_by = p_userid
					 , c520_last_updated_date = CURRENT_DATE
				 WHERE (c520_request_id = v_reqid OR c520_master_request_id = v_reqid);

				v_nvalue	:= v_nvalue + 1;
		   END IF;

			IF (v_nodata = TRUE)
			THEN
				--	v_strreqid	:= v_strreqid || v_reqid || ', ';
				gm_pkg_oppr_ld_no_stack.gm_op_request_action (v_reqid, p_userid, v_straction);

				-- Below begin and End statement added to 4000 plus character 
				-- 
				BEGIN 
					v_str		:= v_str || v_straction || crlf;
				EXCEPTION 
				WHEN OTHERS
				THEN
					NULL;
				END;
			END IF;
		END LOOP;

		IF (v_nodata = TRUE)
		THEN
			gm_pkg_oppr_ld_no_stack.gm_op_nostack_email (p_demandmasterid, p_userid, p_setid, v_str);
		END IF;
	END gm_op_upt_required_date_set;

	--

	/*****************************************************************
	  * Description : Procedure to update the required date for unused sets.
	  **********************************************************************/
	PROCEDURE gm_op_upt_required_date_item (
		p_demandmasterid   IN	t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_refid 		   IN	t4022_demand_par_detail.c205_part_number_id%TYPE
	  , p_type			   IN	VARCHAR2
	  , p_userid		   IN	t101_user.c101_user_id%TYPE
	  , p_months		   IN	NUMBER
	)
	AS
		v_start_date   DATE;
		v_end_date	   DATE;
		v_growth_value NUMBER := 0;
		v_nvalue	   NUMBER := 0;
		v_open_req_count NUMBER := 0;
		v_nodata	   BOOLEAN := FALSE;
		--	v_filter_date  DATE := TRUNC (CURRENT_DATE, 'MON');
		v_filter_date  DATE := ADD_MONTHS (TRUNC (CURRENT_DATE, 'MON'), p_months * -1);
		v_pnum		   t521_request_detail.c205_part_number_id%TYPE;
		v_reqid 	   t520_request.c520_request_id%TYPE;
		v_str		   VARCHAR2 (3500);
		v_old_req_date t520_request.c520_required_date%TYPE;
		v_straction    VARCHAR2 (200);
		crlf		   VARCHAR2 (2) := CHR (13) || CHR (10);

		CURSOR cur_request_details
		IS
			SELECT	 t520.c520_request_id reqid, t520.c520_required_date old_reqdate, t521.c205_part_number_id pnum
				   , t521.c521_qty
				FROM t520_request t520, t521_request_detail t521
			   WHERE t520.c520_request_txn_id = p_demandmasterid
				 AND t520.c520_request_id = t521.c520_request_id
				 --  AND t520.c207_set_id = p_setid
				 AND t520.c207_set_id IS NULL
				 AND t520.c520_status_fl = 10
				 AND t520.c520_void_fl IS NULL
				 AND t520.c520_master_request_id IS NULL
				 AND t520.c901_request_source = '50616'   --order planning
				 AND t520.c520_request_to IS NULL
				 AND TRUNC (t520.c520_required_date) < v_filter_date
			ORDER BY t520.c520_required_date;
	BEGIN
		FOR var_request_detail IN cur_request_details
		LOOP
			v_nodata	:= TRUE;

			v_reqid 	:= var_request_detail.reqid;
			v_old_req_date := var_request_detail.old_reqdate;
			v_pnum		:= var_request_detail.pnum;
			--	v_strreqid	:= v_strreqid || v_reqid || ', ';
			gm_pkg_oppr_ld_no_stack.gm_op_request_action (v_reqid, p_userid, v_straction);

			-- Below begin and End statement added to 4000 plus character 
			-- 
			BEGIN 
				v_str		:= v_str || v_straction || crlf;
			EXCEPTION 
			WHEN OTHERS
			THEN
				NULL;
			END;

		END LOOP;

		IF (v_nodata = TRUE)
		THEN
			gm_pkg_oppr_ld_no_stack.gm_op_nostack_email (p_demandmasterid, p_userid, p_refid, v_str);
		END IF;
	END gm_op_upt_required_date_item;

	/***********************************************************
	   * Description : Procedure to take corresponding request action
	   by different status
	   ************************************************************/
	PROCEDURE gm_op_request_action (
		p_reqid 	   IN		t520_request.c520_request_id%TYPE
	  , p_userid	   IN		t101_user.c101_user_id%TYPE
	  , p_str_action   OUT		VARCHAR2
	)
	AS
		v_status_fl    NUMBER;
		v_status	   VARCHAR2 (20);
	BEGIN
		SELECT t520.c520_status_fl
		  INTO v_status_fl
		  FROM t520_request t520
		 WHERE t520.c520_request_id = p_reqid;

		--	Check if the sheet has growth forecast associated for selected sheet and set
		IF v_status_fl = 10   --Back Order
		THEN
			gm_pkg_common_cancel.gm_cm_sav_cancelrow (p_reqid
													, 90753
													, 'VDREQ'
													, 'Request Voided by No Stack Process'
													, p_userid
													 );   --90753 request calcelled
			p_str_action := p_reqid || ' was Voided ';
		END IF;

		IF v_status_fl = 15   --Back Log
		THEN
			BEGIN
				SELECT t504.c504_status_fl
				  INTO v_status
				  FROM t504_consignment t504
				 WHERE t504.c520_request_id = p_reqid;
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					v_status := '0';
			END;

			IF v_status = '0'	--Initiated (TBE)
			THEN
				gm_pkg_common_cancel.gm_cm_sav_cancelrow (p_reqid
														, 90753
														, 'VDREQ'
														, 'Request Voided by No Stack Process'
														, p_userid
														 );   --90753 request calcelled
				p_str_action := p_reqid || ' was Voided ';
			ELSE   --WIP
				UPDATE t520_request t520
				   SET t520.c520_request_txn_id = NULL
					 , t520.c520_required_date = NULL
					 , t520.c520_last_updated_by = p_userid
					 , t520.c520_last_updated_date = CURRENT_DATE
				 WHERE (c520_request_id = p_reqid OR c520_master_request_id = p_reqid);

				p_str_action := p_reqid || ' Moved to common Pool ';
			END IF;
		END IF;

		IF v_status_fl = 20   --Ready to Consign
		THEN
			UPDATE t520_request t520
			   SET t520.c520_request_txn_id = NULL
				 , t520.c520_required_date = NULL
				 , t520.c520_last_updated_by = p_userid
				 , t520.c520_last_updated_date = CURRENT_DATE
			 WHERE (c520_request_id = p_reqid OR c520_master_request_id = p_reqid);

			p_str_action := p_reqid || ' Moved to common Pool ';
		END IF;
	END gm_op_request_action;

		/***********************************************************
	* Description : Procedure to send Email when no growth forcast
	for selected set and for selected sheet
	************************************************************/
	PROCEDURE gm_op_nostack_email (
		p_master_id   IN   t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_userid	  IN   t101_user.c101_user_id%TYPE
	  , p_setid 	  IN   t207_set_master.c207_set_id%TYPE
	  , p_strreq	  IN   VARCHAR2
	)
	AS
		subject 	   VARCHAR2 (200);
		to_mail 	   VARCHAR2 (400);
		to_priusrmail  t101_user.c101_email_id%TYPE;
		mail_body	   VARCHAR2 (4000);
		v_username	   VARCHAR2 (100);
		v_priuserid    t101_user.c101_user_id%TYPE;
		v_priusername  VARCHAR2 (100);
		v_sheetname    t4020_demand_master.c4020_demand_nm%TYPE;
		v_usrmail	   t101_user.c101_email_id%TYPE;
		crlf		   VARCHAR2 (2) := CHR (13) || CHR (10);
		v_owner 	   VARCHAR2 (200);
		v_setname	   t207_set_master.c207_set_nm%TYPE;
	BEGIN
		SELECT t4020.c4020_primary_user, t4020.c4020_demand_nm
		  INTO v_priuserid, v_sheetname
		  FROM t4020_demand_master t4020
		 WHERE t4020.c4020_demand_master_id = p_master_id;

		--DBMS_OUTPUT.put_line ('Inside posting ****' );
		v_priusername := get_user_name (v_priuserid);
		to_priusrmail := get_user_emailid (v_priuserid);
		v_owner 	:= 'The owner of ' || v_sheetname || ' sheet is, ' || v_priusername;
		v_setname	:= get_set_name (p_setid);
		-- Person who change action set
		v_username	:= get_user_name (p_userid);
		v_usrmail	:= get_user_emailid (p_userid);
		-- Email Area
		to_mail 	:= get_rule_value ('NOSTACK', 'EMAIL') || ',' || v_usrmail || ',' || to_priusrmail;
		--DBMS_OUTPUT.put_line ('Inside posting ****' || to_mail);
		subject 	:= ' No Stack, ' || v_sheetname || ', ' || p_setid || ', ' || v_setname;
		--	  'No forecast but request available for set ' || p_setid || ', set associated with ' || v_sheetname;
		--	mail_body	:= 'No forecast but request available for set ' || p_setid || crlf || crlf || v_owner;
		
		SELECT DECODE(v_username,NULL,'',' by : '||v_username)  INTO v_username FROM DUAL;
		
		mail_body	:=
			'Following inventory has been voided/moved to common pool '|| v_username || crlf || crlf || p_strreq || crlf || crlf
			|| v_owner;
		gm_com_send_email_prc (to_mail, subject, mail_body);
	END gm_op_nostack_email;
END gm_pkg_oppr_ld_no_stack;
/
