CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ld_multipart
IS
/*******************************************************
 * Purpose: This pkg used for load GOP monthend Load
 *
 * modification history
 * ====================
 * Person		 Date	Comments
 * ---------   ------	------------------------------------------
 * psrinivasan    20191112  Initial Version
 *******************************************************/
    /*******************************************************
	* Purpose: 1. This procedure will load Multi part qty for each template
	*******************************************************/
	PROCEDURE gm_op_multipart_qty_load_by_ttp(
	 p_load_date	IN  DATE
	,p_inventory_id			IN t250_inventory_lock.c250_inventory_lock_id%TYPE
	)
	AS
	
	v_string	   		CLOB;
	v_ttp_dtl_id		NUMBER;
	
	CURSOR cur_ttp_dtl
		IS
			   SELECT gm_pkg_oppr_sheet.get_demand_sheet_ids (c4052_ttp_detail_id) v_string
				FROM t4052_ttp_detail t4052, t4050_ttp t4050
				WHERE t4050.c4050_ttp_id = t4052.c4050_ttp_id
                AND c4052_ttp_link_date=p_load_date   -- should come from paramter??
				AND nvl(c901_ttp_type,-9999) <>50310
				AND t4052.c4052_void_fl IS NULL
				AND t4050.c4050_void_fl IS NULL;
                --50310 Multi part
				-- To process all ttp dtl id except Multi Part Spine,Multi Part Trauma
				
	CURSOR cur_mpttp_dtl
		IS			
			 SELECT c4052_ttp_detail_id v_ttp_dtl_id
				FROM t4052_ttp_detail t4052 , t4050_ttp t4050
				WHERE  t4050.c4050_ttp_id = t4052.c4050_ttp_id
                AND c4052_ttp_link_date=p_load_date
				AND nvl(c901_ttp_type,-9999) = 50310
				AND t4052.c4052_void_fl IS NULL
				AND t4050.c4050_void_fl IS NULL;
				-- To get all multi part ttp detail ids
	BEGIN
		
		  -- To cleanup temp table before start loading multi part qty lock as part of Monthend load												  
		  	execute immediate 'TRUNCATE TABLE TS4042_TTP_MULTIPART_MONTH'; 
		  -- 	v_string example '3350517,3229736,3285500'
		
		FOR cur_val IN cur_ttp_dtl
		LOOP
			gm_pkg_oppr_sheet.gm_op_sav_multipartsheet (cur_val.v_string, '30301',NULL,p_load_date,p_inventory_id);
			COMMIT;
			--Here commit is requied to know the progress of inserting record in TS4042_TTP_MULTIPART_MONTH
			-- otherwise there is no clue the progress of 2 hrs loding MP.
		END LOOP;
		
		-- For Multi part detail load in summary
		FOR cur_mp_val IN cur_mpttp_dtl
		LOOP
			gm_pkg_oppr_ld_summary.gm_op_ld_summary_main(cur_mp_val.v_ttp_dtl_id,'','30301');
			COMMIT;
		END LOOP;

			
	-- DELETE current month data from below table as this will have an entry from each template lock 
		DELETE FROM t4045_demand_sheet_assoc WHERE c4045_demand_period_dt=TRUNC (p_load_date, 'MM');
	
  END gm_op_multipart_qty_load_by_ttp;
  
     /*******************************************************
	* Purpose: 2. This procedure will load Multi part qty fetch for Staging table
	*******************************************************/
	PROCEDURE gm_op_multipart_fetch_qty_by_ttp(
	 p_demand_sheetids	IN	 VARCHAR2,
	 p_multi_sheet_id 	IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	 )
	AS
	
	v_multi_sheet_id t4040_demand_sheet.c4040_demand_sheet_id%TYPE;
	
	CURSOR cur_ttp_dtl_fetch
		IS
  
  			SELECT ttp_dtl_id,pnum, period, fctype, SUM(qty) qty FROM (
			SELECT c4052_ttp_detail_id ttp_dtl_id,t4042.c205_part_number_id pnum
			     , DECODE(t4042.c4042_parent_ref_id,NULL,t4042.c4042_qty,0) qty
			     , t4042.c4042_period period
			     --if PAR type then added in to Forecast type
			     , DECODE(t4042.c901_type,50566,50563,t4042.c901_type) fctype
			  FROM t4040_demand_sheet t4040
			     , t4042_demand_sheet_detail t4042
			     , t205_part_number t205
			  WHERE t4040.c4020_demand_master_id NOT IN (SELECT t4045.c4020_demand_master_assoc_id
														  FROM t4045_demand_sheet_assoc t4045
														 WHERE c4040_demand_sheet_id = v_multi_sheet_id)
			   AND t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
			  -- AND t4040.c250_inventory_lock_id = v_inventory_id   -- This join is not required 
			   AND t205.c205_part_number_id = t4042.c205_part_number_id
			   AND t205.c205_crossover_fl IS NOT NULL
			   AND t4042.c901_type IN (50563,  4000110, 4000111,50566)		
			   AND t4042.c205_part_number_id IS NOT NULL
			   AND t4040.c4040_demand_sheet_id IN (SELECT * FROM v_in_list)
		 UNION ALL
			SELECT c4052_ttp_detail_id ttp_dtl_id,t4042.c205_part_number_id pnum
			     , t4042.c4042_qty qty
			     , t4042.c4042_period period
			     , t4042.c901_type fctype
			  FROM t4040_demand_sheet t4040
			     , t4042_demand_sheet_detail t4042
			     , t205_part_number t205
			  WHERE t4040.c4020_demand_master_id NOT IN (SELECT t4045.c4020_demand_master_assoc_id
														  FROM t4045_demand_sheet_assoc t4045
														 WHERE c4040_demand_sheet_id = v_multi_sheet_id)
			   AND t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
			  -- AND t4040.c250_inventory_lock_id = v_inventory_id
			   AND t205.c205_part_number_id = t4042.c205_part_number_id
			   AND t205.c205_crossover_fl IS NOT NULL
			   AND t4042.c901_type IN (4000112, 4000113, 4000119, 4000120, 50570)			  
			   AND t4042.c205_part_number_id IS NOT NULL
			   AND t4040.c4040_demand_sheet_id IN (SELECT * FROM v_in_list)
			   )
			 GROUP BY ttp_dtl_id,pnum, period, fctype;  
		
	BEGIN
		
		my_context.set_my_inlist_ctx (p_demand_sheetids);
		
		FOR cur_val_fetch IN cur_ttp_dtl_fetch
		LOOP
			gm_op_multipart_qty_save_stg (cur_val_fetch.ttp_dtl_id, cur_val_fetch.pnum, cur_val_fetch.period, cur_val_fetch.fctype, cur_val_fetch.qty);
			 
		END LOOP;
		
	END gm_op_multipart_fetch_qty_by_ttp;
	
	/*******************************************************
	* Purpose: 3. This procedure will load Multi part qty in Staging table
	*******************************************************/
	
	PROCEDURE gm_op_multipart_qty_save_stg(
		p_ttp_dtl_id   	   IN	t4052_ttp_detail.c4052_ttp_detail_id%TYPE
	  , p_pnum			   IN	t4042_demand_sheet_detail.c205_part_number_id%TYPE
	  , p_period		   IN   t4042_demand_sheet_detail.c4042_period%TYPE
	  , p_type			   IN	t4042_demand_sheet_detail.c901_type%TYPE
	  , p_qty		       IN	t4042_demand_sheet_detail.c4042_qty%TYPE
	)
	AS
		v_detail_id    t4042_demand_sheet_detail.c4042_demand_sheet_detail_id%TYPE;
		v_qty NUMBER;
	BEGIN
	       v_qty := ROUND(p_qty,0);
		
			UPDATE ts4042_ttp_multipart_month
			   SET c4042_qty = v_qty,
			       c4042_upd_flag='Y'
			 WHERE  c4052_ttp_detail_id = p_ttp_dtl_id
			   AND c205_part_number_id = p_pnum
			   AND c4042_period = p_period
			   AND c901_type = p_type;
			   
		IF (SQL%ROWCOUNT = 0)
		THEN
			
				INSERT INTO ts4042_ttp_multipart_month
							(c4052_ttp_detail_id, c205_part_number_id, c4042_period, c901_type
						   , c4042_qty, c4042_upd_flag
							)
					 VALUES ( p_ttp_dtl_id, p_pnum, p_period,p_type,v_qty,'Y'
							);
		END IF;
	END gm_op_multipart_qty_save_stg;
	
END gm_pkg_oppr_ld_multipart;
/