--@"c:\database\packages\operations\purchasing\gm_pkg_oppr_orderplan_load.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_orderplan_load
IS
    /*******************************************************
   * Description : Procedure used to execute all the required
   * load functionality, If will executed at 1.00  AM
   *******************************************************/
PROCEDURE gm_op_ld_orderplanning_main
AS
--
   CURSOR template_cursor
   IS
        SELECT t4020.c4020_demand_master_id, t4020.c4020_demand_nm 
          FROM t4020_demand_master t4020
         WHERE t4020.c4020_parent_demand_master_id IS NULL
		   AND t4020.c4020_inactive_fl IS NULL
		ORDER BY t4020.c4020_demand_nm ; 
    
	-- only fetch editable sheet 
	CURSOR dmaster_cursor(v_demand_master_id t4020_demand_master.c4020_demand_master_id%TYPE )
	IS
		SELECT c4020_demand_master_id
		  FROM t4020_demand_master t4020, t4015_global_sheet_mapping t4015
		 WHERE t4020.c4020_void_fl IS NULL 
		   AND t4020.c4020_inactive_fl IS NULL
           AND t4020.c4020_parent_demand_master_id = v_demand_master_id 
		   AND t4020.c901_demand_type IN ( 40021,40020,40022 ) 
		   AND t4020.c4015_global_sheet_map_id = t4015.c4015_global_sheet_map_id
		   AND t4015.c901_access_type = 102623 ;

	--
	v_demandsheetid VARCHAR2 (20);
	v_start_time   DATE;
	v_count 	   NUMBER;
    v_daily_gop_job  VARCHAR2(10);
--
BEGIN
--
	v_start_time := SYSDATE;
    -- To be a stand alone package executed seperately 
	--gm_pkg_oppr_ld_no_stack.gm_op_nostack_main;
	gm_pkg_oppr_ld_inventory.gm_op_ld_demand_main (20430);
    
    SELECT get_rule_value('1', 'GOP_MONTH_END')
      INTO v_daily_gop_job
      FROM dual; 
	
    -- Disable the job to execute regular month end load 
	BEGIN 
		DBMS_JOB.BROKEN(v_daily_gop_job, TRUE);
		COMMIT;
		EXCEPTION
	WHEN OTHERS 
	THEN
		NULL;
	END;

	--
    FOR template_val IN template_cursor
	LOOP
		-- Executed editable sheet 
    	FOR dmaster_val IN dmaster_cursor(template_val.c4020_demand_master_id)
    	LOOP
    		--
    		v_demandsheetid := dmaster_val.c4020_demand_master_id;
    		gm_pkg_oppr_ld_demand.gm_op_ld_demand_main (v_demandsheetid);
    	--
    	END LOOP;
        -- To Load the readonly sheet
        gm_pkg_oppr_ld_demand.gm_op_exec_job_detail ();
          
    END LOOP;
	--
	--
	--
	--
	BEGIN 
		DBMS_JOB.BROKEN(v_daily_gop_job, FALSE);
		COMMIT;
	EXCEPTION
	WHEN OTHERS 
	THEN
		NULL;
	END;
	--
	SELECT COUNT (1)
	  INTO v_count
	  FROM t4040_demand_sheet t4040
	 WHERE t4040.c4040_demand_period_dt = TO_DATE ('01/' || TO_CHAR (SYSDATE, 'MM/YYYY'), 'DD/MM/YYYY');

	gm_pkg_cm_job.gm_cm_notify_load_info (v_start_time
										, SYSDATE
										, 'S'
										, 'SUCCESS'
										, v_count || ' Demand Sheet Data Created *** '
										 );
	--
	COMMIT;
EXCEPTION
	WHEN OTHERS
	THEN
		ROLLBACK;
		-- Final log insert statement (Error Message)
		gm_pkg_cm_job.gm_cm_notify_load_info (v_start_time, SYSDATE, 'E', SQLERRM, 'gm_op_ld_orderplanning_main');
		COMMIT;
END gm_op_ld_orderplanning_main;
END gm_pkg_oppr_orderplan_load;
/