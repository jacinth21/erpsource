CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ld_mend_stage
IS
/*******************************************************
 * Purpose: This pkg used for to load GOP monthend Load staging tables which will have only six months data
 *
 * modification history
 * ====================
 * Person		 Date	Comments
 * ---------   ------	------------------------------------------
 * psrinivasan    20190920  Initial Version
 * exec gm_pkg_oppr_ld_mend_Stage.gm_op_ld_stg_main();
 *******************************************************/
	PROCEDURE gm_op_ld_stg_main 
	AS
		--
		v_dmd_from_period  DATE;
		v_dmd_to_period    DATE;
		v_fct_from_period  DATE;
		v_fct_to_period    DATE;
		v_load_date	   DATE;
	--
	BEGIN
		
		gm_pkg_oppr_ld_mend_inventory.gm_op_fch_load_date(v_load_date);
		
		-- To get the demand period
		gm_pkg_oppr_ld_mend_demand.gm_op_calc_period (v_load_date,6, '', 'D', v_dmd_from_period, v_dmd_to_period);
		
		
		DBMS_OUTPUT.put_line ('v_dmd_from_period: '|| v_dmd_from_period);
		DBMS_OUTPUT.put_line ('v_dmd_to_period: '|| v_dmd_to_period);
		
		/* To truncate all Staging table before reload data */		  
		execute immediate 'TRUNCATE TABLE ST501_ORDER_BY_PART';
		execute immediate 'TRUNCATE TABLE ST504_CONSIGNMENT_SET';
		execute immediate 'TRUNCATE TABLE ST504_CONSIGNMENT_SET_DTL';
		execute immediate 'TRUNCATE TABLE ST505_CONSIGNMENT_ITEM';
		execute immediate 'TRUNCATE TABLE ST504_INHOUSE_SET';
		execute immediate 'TRUNCATE TABLE ST504_INHOUSE_SET_DTL';
		execute immediate 'TRUNCATE TABLE ST505_INHOUSE_ITEM';
		execute immediate 'TRUNCATE TABLE ST412_WRITEOFF';
		execute immediate 'TRUNCATE TABLE ST4021_FCT_CONSIGN_INHOUSE_SET';
		execute immediate 'TRUNCATE TABLE ST4030_FCT_SET_ITEM_GROWTH';
		execute immediate 'TRUNCATE TABLE ST253C_REQUEST_LOCK';
		execute immediate 'TRUNCATE TABLE ST253D_REQUEST_DTL_LOCK';
		execute immediate 'TRUNCATE TABLE ST253A_CONSIGNMENT_LOCK';
				
		DBMS_OUTPUT.put_line ('Staging tables are truncated');
		
		gm_op_ld_stg_sales_demand(v_dmd_from_period,v_dmd_to_period);
		DBMS_OUTPUT.put_line ('Sales staging table reloaded on: '||TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
		
		gm_op_ld_stg_set_consign_demand(v_dmd_from_period,v_dmd_to_period);
		DBMS_OUTPUT.put_line ('Set Consignment staging table reloaded on: '||TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
		
		gm_op_ld_stg_setdtl_consign_demand(v_dmd_from_period,v_dmd_to_period);
		DBMS_OUTPUT.put_line ('Set Detail Consignment staging table reloaded on: '||TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
		
		gm_op_ld_stg_item_consign_demand(v_dmd_from_period,v_dmd_to_period);
		DBMS_OUTPUT.put_line ('Item Consignment staging table reloaded on: '||TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
		
		gm_op_ld_stg_set_inhouse_demand(v_dmd_from_period,v_dmd_to_period);
	    DBMS_OUTPUT.put_line ('Set Inhouse staging table reloaded on: '||TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
	    
	    gm_op_ld_stg_setdtl_inhouse_demand(v_dmd_from_period,v_dmd_to_period);
	    DBMS_OUTPUT.put_line ('Set Detail Inhouse staging table relaoded on: '||TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
	    
	    gm_op_ld_stg_item_inhouse_demand(v_dmd_from_period,v_dmd_to_period);
	    DBMS_OUTPUT.put_line ('Item Inhouse staging table reloaded on: '||TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
	   
	    gm_op_ld_stg_loaner_writeoff_demand(v_dmd_from_period,v_dmd_to_period);
	    DBMS_OUTPUT.put_line ('Loaner writeoff staging table reloaded on: '||TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
	    
	    -- To get the Forecast period
	    gm_pkg_oppr_ld_mend_demand.gm_op_calc_period (v_load_date,12, '', 'F', v_fct_from_period, v_fct_to_period);
	    DBMS_OUTPUT.put_line ('v_fct_from_period: '|| v_fct_from_period);
		DBMS_OUTPUT.put_line ('v_fct_to_period: '|| v_fct_to_period);
	    
	    gm_op_ld_stg_fct_consign_inhouse_set(v_fct_from_period,v_fct_to_period);
	    DBMS_OUTPUT.put_line ('Forecast consingment and inhouse set master staging table reloaded on: '||TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
	    
	    gm_op_ld_stg_fct_override_set_item(v_fct_from_period,v_fct_to_period);
	    DBMS_OUTPUT.put_line ('Forecast override set and item staging table reloaded on: '||TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
	    
	    gm_op_ld_stg_request_lock(v_fct_from_period);
	    DBMS_OUTPUT.put_line ('Pending request lock staging table reloaded on: '||TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
	    
	    gm_op_ld_stg_consignment_lock(v_fct_from_period);
	    DBMS_OUTPUT.put_line ('Pending request lock staging table reloaded on: '||TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
	    
	    
	END gm_op_ld_stg_main;
	
	/*******************************************************
	* Purpose: This procedure will load GOP Monthend Staging table
	*	Six months order information
	* Accepts demand id and loads demand  and forecast information
	*******************************************************/
	PROCEDURE gm_op_ld_stg_sales_demand (
		p_from_period		IN	 DATE
	  , p_to_period 		IN	 DATE
	)
	AS
	
	BEGIN 
			INSERT INTO st501_order_by_part (c704_account_id,c501_order_date,c205_part_number_id,c502_item_qty)
		        SELECT c704_account_id,trunc(t501.c501_order_date,'month') order_date
								   , t502.c205_part_number_id, SUM(t502.c502_item_qty)
								FROM  t501_order t501
								   , t502_item_order t502
							   WHERE t501.c501_order_date >= p_from_period
								 AND t501.c501_order_date <= p_to_period
								 AND t501.c501_order_id = t502.c501_order_id
								 AND t501.c501_void_fl IS NULL
								 AND nvl (t501.c901_order_type, -999) <> 2524
								 AND nvl (c901_order_type, -9999) NOT IN (SELECT to_number(c906_rule_value) FROM v901_order_type_grp)
								 GROUP BY c704_account_id,trunc(t501.c501_order_date,'month'), t502.c205_part_number_id;
	
    END gm_op_ld_stg_sales_demand;

	/*******************************************************
	* Purpose: This procedure will load GOP Monthend Staging table
	*	Six months Set Consignment information
	*******************************************************/
	PROCEDURE gm_op_ld_stg_set_consign_demand (
		p_from_period		IN	 DATE
	  , p_to_period 		IN	 DATE
	)
	AS
	
	BEGIN 

		INSERT INTO st504_consignment_set (c5040_plant_id,c701_distributor_id,c207_set_id,c504_ship_date,c504_consign_qty)
			SELECT NVL(consinfo.plant_id,returninfo.plant_id),NVL(consinfo.dist_id,returninfo.dist_id),NVL(consinfo.setid,returninfo.setid),
			NVL(consinfo.sdate,returninfo.sdate),NVL (consinfo.c_qty, 0) -  NVL (r_qty, 0) cons_qty
			FROM
				(SELECT  t504.c5040_plant_id plant_id,t504.c701_distributor_id dist_id,t504.c207_set_id setid,
				TRUNC(t504.c504_ship_date,'month') sdate,
				COUNT (1) c_qty
				FROM t504_consignment t504
				WHERE t504.c701_distributor_id IS NOT NULL
				AND t504.c207_set_id IS NOT NULL
				-- Remove the consignment transfers
				AND NOT EXISTS (
					SELECT c921_ref_id
					FROM t920_transfer t920, t921_transfer_detail t921
					WHERE t920.c920_transfer_id = t921.c920_transfer_id
					AND c921_ref_id = t504.c504_consignment_id
					AND t920.c920_void_fl IS NULL
					AND t920.c920_transfer_date >= p_from_period
					AND t920.c920_transfer_date <= p_to_period
					AND t921.c901_link_type = 90360)
				AND t504.c504_ship_date >= p_from_period
				AND t504.c504_ship_date <= p_to_period
				AND t504.c504_status_fl = '4'
				AND t504.c504_type = 4110
				AND t504.c504_void_fl IS NULL
				GROUP BY t504.c5040_plant_id,t504.c701_distributor_id,t504.c207_set_id, TRUNC(t504.c504_ship_date,'month')) consinfo
			FULL OUTER JOIN
				(SELECT   t506.c5040_plant_id plant_id,t506.c701_distributor_id dist_id,t506.c207_set_id setid,
				TRUNC(t506.c506_credit_date,'month') sdate,
				COUNT (1) r_qty
				FROM t506_returns t506
				WHERE t506.c701_distributor_id IS NOT NULL
				AND t506.c506_void_fl IS NULL
				AND t506.c506_status_fl = '2'
				AND t506.c207_set_id IS NOT NULL
				AND NOT EXISTS (
					SELECT c921_ref_id
					FROM t920_transfer t920, t921_transfer_detail t921
					WHERE t920.c920_transfer_id = t921.c920_transfer_id
					AND c921_ref_id = t506.c506_rma_id
					AND t920.c920_void_fl IS NULL
					AND t920.c920_transfer_date >= p_from_period
					AND t920.c920_transfer_date <= p_to_period
					AND t921.c901_link_type = 90361)
				AND t506.c506_credit_date >= p_from_period
				AND t506.c506_credit_date <= p_to_period
				GROUP BY t506.c5040_plant_id,t506.c701_distributor_id,t506.c207_set_id, TRUNC(t506.c506_credit_date,'month'))returninfo
			ON  consinfo.setid = returninfo.setid
			AND consinfo.dist_id=returninfo.dist_id
			AND consinfo.sdate=returninfo.sdate;

	
    END gm_op_ld_stg_set_consign_demand;
    
    /*******************************************************
	* Purpose: This procedure will load GOP Monthend Staging table
	*	Six months Set detail Consignment information
	*******************************************************/
	PROCEDURE gm_op_ld_stg_setdtl_consign_demand (
		p_from_period		IN	 DATE
	  , p_to_period 		IN	 DATE
	)
	AS
	
	BEGIN 
	
		INSERT INTO st504_consignment_set_dtl (c5040_plant_id,c701_distributor_id,c207_set_id,c205_part_number_id,c504_ship_date,c504_consign_qty)
			SELECT NVL(consinfo.plant_id,returninfo.plant_id),NVL(consinfo.dist_id,returninfo.dist_id),NVL(consinfo.setid,returninfo.setid),
			NVL(consinfo.p_num,returninfo.p_num),NVL(consinfo.sdate,returninfo.sdate),NVL (consinfo.c_qty, 0) -  NVL (r_qty, 0) cons_qty
			FROM
				(SELECT  t504.c5040_plant_id plant_id,t504.c701_distributor_id dist_id,t504.c207_set_id setid,t505.c205_part_number_id p_num,
				TRUNC(t504.c504_ship_date,'month') sdate,
				SUM (t505.c505_item_qty) c_qty
				FROM t504_consignment t504,
				t505_item_consignment t505
				WHERE t504.c701_distributor_id IS NOT NULL
				AND t504.c504_consignment_id  = t505.c504_consignment_id
				AND t504.c207_set_id IS NOT NULL
				-- Remove the consignment transfers
				AND NOT EXISTS (
					SELECT c921_ref_id
					FROM t920_transfer t920, t921_transfer_detail t921
					WHERE t920.c920_transfer_id = t921.c920_transfer_id
					AND c921_ref_id = t504.c504_consignment_id
					AND t920.c920_void_fl IS NULL
					AND t920.c920_transfer_date >= p_from_period
					AND t920.c920_transfer_date <= p_to_period
					AND t921.c901_link_type = 90360)
				AND t504.c504_ship_date >= p_from_period
				AND t504.c504_ship_date <= p_to_period
				AND t504.c504_status_fl = '4'
				AND t504.c504_type = 4110
				AND t504.c504_void_fl IS NULL
				GROUP BY t504.c5040_plant_id,t504.c701_distributor_id,t504.c207_set_id,t505.c205_part_number_id, TRUNC(t504.c504_ship_date,'month')) consinfo
			FULL OUTER JOIN
				(SELECT   t506.c5040_plant_id plant_id,t506.c701_distributor_id dist_id,t506.c207_set_id setid,t507.c205_part_number_id p_num,
				TRUNC(t506.c506_credit_date,'month') sdate,
				SUM (t507.c507_item_qty ) r_qty
				FROM t506_returns t506,
				t507_returns_item t507
				WHERE t506.c701_distributor_id IS NOT NULL
				AND t506.c506_rma_id = t507.c506_rma_id
				AND t506.c506_void_fl IS NULL
				AND t506.c506_status_fl = '2'
				AND t506.c207_set_id IS NOT NULL
				AND t507.c507_status_fl IN ('C', 'W')
				AND NOT EXISTS (
					SELECT c921_ref_id
					FROM t920_transfer t920, t921_transfer_detail t921
					WHERE t920.c920_transfer_id = t921.c920_transfer_id
					AND c921_ref_id = t506.c506_rma_id
					AND t920.c920_void_fl IS NULL
					AND t920.c920_transfer_date >= p_from_period
					AND t920.c920_transfer_date <= p_to_period
					AND t921.c901_link_type = 90361)
				AND t506.c506_credit_date >= p_from_period
				AND t506.c506_credit_date <= p_to_period
				GROUP BY t506.c5040_plant_id,t506.c701_distributor_id,t506.c207_set_id,t507.c205_part_number_id,TRUNC(t506.c506_credit_date,'month'))returninfo
			ON  consinfo.setid = returninfo.setid
			AND consinfo.dist_id=returninfo.dist_id
			AND consinfo.p_num=returninfo.p_num
			AND consinfo.sdate=returninfo.sdate;
					
    END gm_op_ld_stg_setdtl_consign_demand;
    /*******************************************************
	* Purpose: This procedure will load GOP Monthend Staging table
	*	Six months item Consignment information
	*******************************************************/
	PROCEDURE gm_op_ld_stg_item_consign_demand (
		p_from_period		IN	 DATE
	  , p_to_period 		IN	 DATE
	)
	AS
	
	BEGIN 
	
		INSERT INTO st505_consignment_item (c5040_plant_id,c701_distributor_id,c205_part_number_id,c504_ship_date,c504_consign_qty)
			SELECT NVL(consinfo.plant_id,returninfo.plant_id),NVL(consinfo.dist_id,returninfo.dist_id),NVL(consinfo.p_num,returninfo.p_num),
			NVL(consinfo.sdate,returninfo.sdate),NVL (consinfo.c_qty, 0) -  NVL (r_qty, 0) cons_qty
			FROM
				(SELECT   t504.C5040_plant_id plant_id,t504.c701_distributor_id dist_id,t505.c205_part_number_id p_num,
				TRUNC(t504.c504_ship_date,'month') sdate,
				SUM (t505.c505_item_qty) c_qty
				FROM t504_consignment t504,
				t505_item_consignment t505
				WHERE t504.c701_distributor_id IS NOT NULL
				AND t504.c504_consignment_id  = t505.c504_consignment_id
				AND t504.c207_set_id IS NULL
				-- Remove excess from item consignent
				AND NOT EXISTS
					(
					SELECT a.c504_consignment_id a
					FROM t504a_consignment_excess a
					WHERE a.c504_consignment_id = t504.c504_consignment_id
					)
				-- Remove the consignment transfers
				AND NOT EXISTS (
					SELECT c921_ref_id
					FROM t920_transfer t920, t921_transfer_detail t921
					WHERE t920.c920_transfer_id = t921.c920_transfer_id
					AND c921_ref_id = t504.c504_consignment_id
					AND t920.c920_void_fl IS NULL
					AND t920.c920_transfer_date >= p_from_period
					AND t920.c920_transfer_date <= p_to_period
					AND t921.c901_link_type = 90360)
				AND t504.c504_ship_date >= p_from_period
				AND t504.c504_ship_date <= p_to_period
				AND t504.c504_status_fl = '4'
				AND t504.c504_type = 4110
				AND t504.c504_void_fl IS NULL
				AND TRIM (t505.c505_control_number) IS NOT NULL
				AND (t505.c901_type   IS NULL
				OR t505.c901_type                   <> 100880)
				GROUP BY t504.C5040_plant_id,t504.c701_distributor_id,t505.c205_part_number_id, TRUNC(t504.c504_ship_date,'month')) consinfo
			FULL OUTER JOIN
				(SELECT   t506.C5040_plant_id plant_id,t506.c701_distributor_id dist_id,t507.c205_part_number_id p_num, TRUNC(t506.c506_credit_date,'month') sdate,
				SUM (t507.c507_item_qty )  r_qty
				FROM t506_returns t506,
				t507_returns_item t507
				WHERE t506.c701_distributor_id IS NOT NULL
				AND t506.c506_void_fl IS NULL
				AND t506.c506_status_fl = '2'
				AND t506.c207_set_id IS NULL
				AND t506.c506_rma_id = t507.c506_rma_id
				AND t507.c507_status_fl='C'
				--Exclude RA Created from FA.
				AND (t506.C901_TXN_SOURCE is NULL OR t506.C901_TXN_SOURCE <>'102680')
				AND NOT EXISTS (
					SELECT c921_ref_id
					FROM t920_transfer t920, t921_transfer_detail t921
					WHERE t920.c920_transfer_id = t921.c920_transfer_id
					AND c921_ref_id = t506.c506_rma_id
					AND t920.c920_void_fl IS NULL
					AND t920.c920_transfer_date >= p_from_period
					AND t920.c920_transfer_date <= p_to_period
					AND t921.c901_link_type = 90361)
				AND t506.c506_credit_date >= p_from_period
				AND t506.c506_credit_date <= p_to_period
				GROUP BY t506.C5040_plant_id,t506.c701_distributor_id,t507.c205_part_number_id, TRUNC(t506.c506_credit_date,'month')) returninfo
			ON  consinfo.p_num = returninfo.p_num
			and consinfo.dist_id=returninfo.dist_id
			and consinfo.sdate=returninfo.sdate;
					
    END gm_op_ld_stg_item_consign_demand;
    
    /*******************************************************
	* Purpose: This procedure will load GOP Monthend Staging table
	*	Six months Inhouse Set Consignment information
	*******************************************************/
	PROCEDURE gm_op_ld_stg_set_inhouse_demand (
		p_from_period		IN	 DATE
	  , p_to_period 		IN	 DATE
	)
	AS
	
	BEGIN 
		
	INSERT INTO st504_inhouse_set(c5040_plant_id,c207_set_id,c504_ship_date,c504_consign_qty)
		SELECT NVL(consinfo.plant_id,returninfo.plant_id),NVL(consinfo.setid,returninfo.setid),NVL(consinfo.sdate,returninfo.sdate),NVL (consinfo.c_qty, 0) -  NVL (r_qty, 0) cons_qty
		FROM
			(SELECT   t504.C5040_plant_id plant_id, t504.c207_set_id setid,
			TRUNC(t504.c504_ship_date,'month') sdate,
			COUNT (1) c_qty
			FROM t504_consignment t504
			WHERE t504.c701_distributor_id IS  NULL
			AND t504.c704_account_id = '01'
			AND t504.c207_set_id IS NOT NULL
			AND NOT EXISTS (
			SELECT c921_ref_id
				FROM t920_transfer t920, t921_transfer_detail t921
				WHERE t920.c920_transfer_id = t921.c920_transfer_id
				AND c921_ref_id = t504.c504_consignment_id
				AND t920.c920_void_fl IS NULL
				AND t920.c920_transfer_date >= p_from_period
				AND t920.c920_transfer_date <= p_to_period
				AND t921.c901_link_type = 90360)
			AND t504.c504_ship_date >= p_from_period
			AND t504.c504_ship_date <= p_to_period
			AND t504.c504_status_fl = '4'
			AND t504.c504_void_fl IS NULL
			GROUP BY t504.C5040_plant_id,t504.c207_set_id, TRUNC(t504.c504_ship_date,'month')) consinfo
		FULL OUTER JOIN
			(SELECT t506.C5040_plant_id plant_id,t506.c207_set_id setid,
			TRUNC(t506.c506_credit_date,'month') sdate,
			COUNT (1) r_qty
			FROM t506_returns t506
			WHERE t506.c701_distributor_id IS NULL
			AND t506.c704_account_id = '01'
			AND t506.c506_void_fl IS NULL
			AND t506.c506_status_fl = '2'
			AND t506.c207_set_id IS NOT NULL
			AND NOT EXISTS (
				SELECT c921_ref_id
				FROM t920_transfer t920, t921_transfer_detail t921
				WHERE t920.c920_transfer_id = t921.c920_transfer_id
				AND c921_ref_id = t506.c506_rma_id
				AND t920.c920_void_fl IS NULL
				AND t920.c920_transfer_date >= p_from_period
				AND t920.c920_transfer_date <= p_to_period
				AND t921.c901_link_type = 90361)
			AND t506.c506_credit_date >= p_from_period
			AND t506.c506_credit_date <= p_to_period
			GROUP BY t506.C5040_plant_id,t506.c207_set_id, TRUNC(t506.c506_credit_date,'month')) returninfo
			ON consinfo.sdate = returninfo.sdate
			AND consinfo.setid = returninfo.setid
			AND consinfo.plant_id = returninfo.plant_id;
					
    END gm_op_ld_stg_set_inhouse_demand;
    
     /*******************************************************
	* Purpose: This procedure will load GOP Monthend Staging table
	*	Six months Inhouse Set Consignment detail information
	*******************************************************/
	PROCEDURE gm_op_ld_stg_setdtl_inhouse_demand (
		p_from_period		IN	 DATE
	  , p_to_period 		IN	 DATE
	)
	AS
	
	BEGIN 
		
		INSERT INTO st504_inhouse_set_dtl(c5040_plant_id,c207_set_id,c205_part_number_id,c504_ship_date,c504_consign_qty)
			SELECT NVL(consinfo.plant_id,returninfo.plant_id),NVL(consinfo.setid,returninfo.setid),NVL(consinfo.p_num,returninfo.p_num),NVL(consinfo.sdate,returninfo.sdate),NVL (consinfo.c_qty, 0) -  NVL (r_qty, 0) cons_qty
			FROM
				(SELECT   t504.c5040_plant_id plant_id,t504.c207_set_id setid, t505.c205_part_number_id p_num,
				TRUNC(t504.c504_ship_date,'month') sdate,
				SUM (t505.c505_item_qty) c_qty
				FROM t504_consignment t504,
				t505_item_consignment t505
				WHERE t504.c701_distributor_id IS NULL
				AND t504.c704_account_id      = '01'
				AND t504.c504_consignment_id  = t505.c504_consignment_id
				AND t504.c207_set_id IS NOT NULL
				-- Remove the consignment transfers
				AND NOT EXISTS (
					SELECT c921_ref_id
					FROM t920_transfer t920, t921_transfer_detail t921
					WHERE t920.c920_transfer_id = t921.c920_transfer_id
					AND c921_ref_id = t504.c504_consignment_id
					AND t920.c920_void_fl IS NULL
					AND t920.c920_transfer_date >= p_from_period
					AND t920.c920_transfer_date <= p_to_period
					AND t921.c901_link_type = 90360)
				AND t504.c504_ship_date >= p_from_period
				AND t504.c504_ship_date <= p_to_period
				AND t504.c504_status_fl = '4'
				AND t504.c504_void_fl IS NULL
				AND TRIM (t505.c505_control_number) IS NOT NULL
				AND (t505.c901_type                 IS NULL
				OR t505.c901_type                   <> 100880)
				GROUP BY t504.c5040_plant_id,t504.c207_set_id, t505.c205_part_number_id, TRUNC(t504.c504_ship_date,'month')) consinfo
			FULL OUTER JOIN
				(SELECT   t506.c5040_plant_id plant_id,t506.c207_set_id setid,
				TRUNC(t506.c506_credit_date,'month') sdate,
				t507.c205_part_number_id p_num, SUM (t507.c507_item_qty )  r_qty
				FROM t506_returns t506,
				t507_returns_item t507
				WHERE t506.c701_distributor_id IS NULL
				AND t506.c704_account_id = '01'
				AND t506.c506_void_fl IS NULL
				AND t506.c506_status_fl = '2'
				AND t506.c207_set_id IS NOT NULL
				AND t506.c506_rma_id = t507.c506_rma_id
				AND t507.c507_status_fl IN ('C', 'W')
				AND NOT EXISTS (
					SELECT c921_ref_id
					FROM t920_transfer t920, t921_transfer_detail t921
					WHERE t920.c920_transfer_id = t921.c920_transfer_id
					AND c921_ref_id = t506.c506_rma_id
					AND t920.c920_void_fl IS NULL
					AND t920.c920_transfer_date >= p_from_period
					AND t920.c920_transfer_date <= p_to_period
					AND t921.c901_link_type = 90361)
				AND t506.c506_credit_date >= p_from_period
				AND t506.c506_credit_date <= p_to_period
				GROUP BY t506.c5040_plant_id,t506.c207_set_id, t507.c205_part_number_id, TRUNC(t506.c506_credit_date,'month')) returninfo
			ON consinfo.sdate = returninfo.sdate
			AND consinfo.setid = returninfo.setid
			AND consinfo.p_num = returninfo.p_num
			AND consinfo.plant_id = returninfo.plant_id;
					
    END gm_op_ld_stg_setdtl_inhouse_demand;
     /*******************************************************
	* Purpose: This procedure will load GOP Monthend Staging table
	*	Six months Inhouse item Consignment information
	*******************************************************/
	PROCEDURE gm_op_ld_stg_item_inhouse_demand (
		p_from_period		IN	 DATE
	  , p_to_period 		IN	 DATE
	)
	AS
	
	BEGIN 
	
		INSERT INTO st505_inhouse_item(c5040_plant_id,c205_part_number_id,c504_ship_date,c504_consign_qty)
			SELECT NVL(consinfo.plant_id,returninfo.plant_id),NVL(consinfo.p_num,returninfo.p_num),NVL(consinfo.sdate,returninfo.sdate),NVL (consinfo.c_qty, 0) -  NVL (r_qty, 0) cons_qty
			FROM
				(SELECT t504.C5040_plant_id plant_id,t505.c205_part_number_id p_num,
				TRUNC(t504.c504_ship_date,'month') sdate,
				SUM (t505.c505_item_qty) c_qty
				FROM t504_consignment t504,
				t505_item_consignment t505
				WHERE t504.c701_distributor_id IS NULL
				AND t504.c704_account_id = '01'
				AND t504.c504_consignment_id  = t505.c504_consignment_id
				AND t504.c207_set_id IS NULL
				-- Remove excess from item consignent
				AND NOT EXISTS
				(
				SELECT a.c504_consignment_id a
				FROM t504a_consignment_excess a
				WHERE a.c504_consignment_id = t504.c504_consignment_id
				)
				-- Remove the consignment transfers
				AND NOT EXISTS (
					SELECT c921_ref_id
					FROM t920_transfer t920, t921_transfer_detail t921
					WHERE t920.c920_transfer_id = t921.c920_transfer_id
					AND c921_ref_id = t504.c504_consignment_id
					AND t920.c920_void_fl IS NULL
					AND t920.c920_transfer_date >= p_from_period
					AND t920.c920_transfer_date <= p_to_period
					AND t921.c901_link_type = 90360)
				AND t504.c504_ship_date >= p_from_period
				AND t504.c504_ship_date <= p_to_period
				AND t504.c504_status_fl = '4'
				AND t504.c504_void_fl IS NULL
				AND TRIM (t505.c505_control_number) IS NOT NULL
				AND (t505.c901_type   IS NULL
				OR t505.c901_type                   <> 100880)
				GROUP BY  t504.C5040_plant_id,t505.c205_part_number_id, TRUNC(t504.c504_ship_date,'month')) consinfo
			FULL OUTER JOIN
				(SELECT t506.C5040_plant_id plant_id, t507.c205_part_number_id p_num, TRUNC(t506.c506_credit_date,'month') sdate,
				SUM (t507.c507_item_qty )  r_qty
				FROM t506_returns t506,
				t507_returns_item t507
				WHERE t506.c701_distributor_id IS NULL
				AND t506.c704_account_id = '01'
				AND t506.c506_void_fl IS NULL
				AND t506.c506_status_fl = '2'
				AND t506.c207_set_id IS NULL
				AND t506.c506_rma_id = t507.c506_rma_id
				AND t507.c507_status_fl IN ('C', 'W')
				AND NOT EXISTS (
					SELECT c921_ref_id
					FROM t920_transfer t920, t921_transfer_detail t921
					WHERE t920.c920_transfer_id = t921.c920_transfer_id
					AND c921_ref_id = t506.c506_rma_id
					AND t920.c920_void_fl IS NULL
					AND t920.c920_transfer_date >= p_from_period
					AND t920.c920_transfer_date <= p_to_period
					AND t921.c901_link_type = 90361)
				AND t506.c506_credit_date >= p_from_period
				AND t506.c506_credit_date <= p_to_period
				GROUP BY t506.C5040_plant_id,t507.c205_part_number_id, TRUNC(t506.c506_credit_date,'month')) returninfo
			ON consinfo.sdate = returninfo.sdate
			AND consinfo.p_num = returninfo.p_num
			AND consinfo.plant_id = returninfo.plant_id;
			
					
    END gm_op_ld_stg_item_inhouse_demand;

      /*******************************************************
	* Purpose: This procedure will load GOP Monthend Staging table
	*	Six months Loaner Writeoff information
	*******************************************************/
	PROCEDURE gm_op_ld_stg_loaner_writeoff_demand (
		p_from_period		IN	 DATE
	  , p_to_period 		IN	 DATE
	)
	AS
	
	BEGIN 
		
		INSERT INTO st412_writeoff(c5040_plant_id,c413_created_date,c205_part_number_id,c413_item_qty)
			SELECT t412.C5040_plant_id plant_id, TRUNC(t413.c413_created_date,'month') sdate,
			t413.c205_part_number_id p_num, SUM (t413.c413_item_qty) cons_qty
			FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
			WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
			AND t412.c412_status_fl        = '4'
			AND t412.c412_void_fl         IS NULL
			AND t413.c413_status_fl        = 'W'
			AND t413.c413_void_fl         IS NULL
			AND TRUNC(t413.c413_created_date) >= p_from_period
			AND TRUNC(t413.c413_created_date) <= p_to_period
			GROUP BY t412.C5040_plant_id,t413.c205_part_number_id, TRUNC(t413.c413_created_date,'month');
					
    END gm_op_ld_stg_loaner_writeoff_demand;
    
    /*******************************************************
	* Purpose: This procedure will load GOP Monthend Staging table
	*	Forecast consignment and inhouse set information
	*******************************************************/
	PROCEDURE gm_op_ld_stg_fct_consign_inhouse_set (
		p_from_period		IN	 DATE
	  , p_to_period 		IN	 DATE
	)
	AS  
	
	BEGIN
		
		INSERT INTO st4021_fct_consign_inhouse_set (c4020_demand_master_id,c207_set_id,month_value)
			SELECT t4021.c4020_demand_master_id,t4021.c4021_ref_id setid,v9001.month_value
			FROM t4021_demand_mapping t4021,
			    v9001_month_list v9001
			WHERE t4021.c901_ref_type IN (40031, 4000109)
			AND v9001.month_value   >= p_from_period
			AND v9001.month_value   <= p_to_period;
			--(40031, 4000109) Consignment Set, Inhouse Set
    
 	END gm_op_ld_stg_fct_consign_inhouse_set;
 	
 	 /*******************************************************
	* Purpose: This procedure will load GOP Monthend Staging table
	*	Forecast override set information
	*******************************************************/
	PROCEDURE gm_op_ld_stg_fct_override_set_item (
		p_from_period		IN	 DATE
	  , p_to_period 		IN	 DATE
	)
	AS  
	
	BEGIN
		
		-- For Set
		INSERT INTO st4030_fct_set_item_growth(c4020_demand_master_id,c4030_ref_id,c901_ref_type,c4031_value,c4031_start_date,c901_growth_mapping_type)
			SELECT t4020.c4020_demand_master_id,t4030.c4030_ref_id, t4031.c901_ref_type, t4031.c4031_value,t4031.c4031_start_date,t4030.c901_ref_type
                        FROM t4020_demand_master t4020,
                             t4030_demand_growth_mapping t4030,
                             t4031_growth_details t4031
                       WHERE t4020.c4020_demand_master_id = t4030.c4020_demand_master_id
                         AND t4031.c4030_demand_growth_id = t4030.c4030_demand_growth_id
                         AND t4030.c901_ref_type = 20296
                         AND t4031.c4031_start_date >= p_from_period
                         AND t4031.c4031_end_date <= p_to_period
                         AND t4030.c4030_void_fl IS NULL
                         AND t4031.c4031_void_fl IS NULL
                         AND t4031.c4031_value IS NOT NULL;
                         --20296 SET ID
                         --20298 Part # - Consignment
		--For Item
       INSERT INTO st4030_fct_set_item_growth(c4020_demand_master_id,c4030_ref_id,c901_ref_type,c4031_value,c4031_start_date,c901_growth_mapping_type)                  
       		SELECT t4020.c4020_demand_master_id, t4030.c4030_ref_id, t4031.c901_ref_type, t4031.c4031_value,t4031.c4031_start_date,t4030.c901_ref_type
                        FROM t4020_demand_master t4020,
                             t4030_demand_growth_mapping t4030,
                             t4031_growth_details t4031
                       WHERE t4020.c4020_demand_master_id = t4030.c4020_demand_master_id
                         AND t4031.c4030_demand_growth_id = t4030.c4030_demand_growth_id
                         AND t4030.c901_ref_type = 20298
                         AND t4031.c4031_start_date >= p_from_period
                         AND t4031.c4031_end_date <= p_to_period
                         AND t4030.c4030_void_fl IS NULL
                         AND t4031.c4031_void_fl IS NULL
                         AND t4031.c4031_value IS NOT NULL;
    
 	END gm_op_ld_stg_fct_override_set_item;
 	
 	 /*******************************************************
	* Purpose: This procedure will load GOP Monthend Staging table
	*	To load request lock detail
	*******************************************************/
	PROCEDURE gm_op_ld_stg_request_lock (
		p_from_period		IN	 DATE
	)
	AS  
	
	BEGIN
		
		INSERT INTO st253c_request_lock(c520_request_id, c520_request_txn_id,c520_master_request_id, c520_status_fl,c207_set_id,c520_required_date)	
			 SELECT t520.c520_request_id, t520.c520_request_txn_id,c520_master_request_id,t520.c520_status_fl, t520.c207_set_id,c520_required_date
	           FROM t520_request t520
	          WHERE t520.c520_void_fl   IS NULL
	            AND t520.c520_delete_fl IS NULL
	            AND t520.c520_status_fl  < 40
	            AND t520.c1900_company_id  = 1000
	            AND t520.c901_request_source    = 50616;
    			--50616 Order Planning
    			
	            
	    INSERT INTO st253d_request_dtl_lock(c520_request_id, c520_request_txn_id,c520_master_request_id, c520_status_fl,c207_set_id, c205_part_number_id, c521_qty)	
			 SELECT t520.c520_request_id, t520.c520_request_txn_id,c520_master_request_id,t520.c520_status_fl, t520.c207_set_id, t521.c205_part_number_id, t521.c521_qty
	           FROM t520_request t520, t521_request_detail t521
	          WHERE t520.c520_request_id = t521.c520_request_id
	            AND t520.c520_void_fl   IS NULL
	            AND t520.c520_delete_fl IS NULL
	            AND t520.c520_status_fl  < 40
	            AND t520.c1900_company_id  = 1000
	            AND t520.c901_request_source  = 50616
	            AND t520.c520_required_date < p_from_period;
 	END gm_op_ld_stg_request_lock;
 	
 	 /*******************************************************
	* Purpose: This procedure will load GOP Monthend Staging table
	*	To load consingment lock detail
	*******************************************************/
	PROCEDURE gm_op_ld_stg_consignment_lock (
		p_from_period		IN	 DATE
	)
	AS  
	
	BEGIN
		
		INSERT INTO st253a_consignment_lock(c504_consignment_id, c520_request_id,c520_request_txn_id,c520_status_fl, c207_set_id, c205_part_number_id, c505_item_qty)	
			SELECT t504.c504_consignment_id,t520.c520_request_id,t520.c520_request_txn_id,t520.c520_status_fl,t520.c207_set_id, t505.c205_part_number_id, t505.c505_item_qty
	           FROM t520_request t520, t504_consignment t504, t505_item_consignment t505
	          WHERE t520.c520_request_id     = t504.c520_request_id
	            AND t504.c504_consignment_id = t505.c504_consignment_id
	            AND t520.c520_void_fl       IS NULL
	            AND t520.c520_delete_fl     IS NULL
	            AND t520.c520_status_fl      < 40
	            AND t520.c1900_company_id  = 1000 
	            AND t520.c901_request_source  = 50616
	            AND t520.c520_required_date < p_from_period;
    
 	END gm_op_ld_stg_consignment_lock;
    
END gm_pkg_oppr_ld_mend_stage;
/