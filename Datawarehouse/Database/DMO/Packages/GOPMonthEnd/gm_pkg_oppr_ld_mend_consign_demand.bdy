/* Formatted on 2009/12/28 18:08 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\purchasing\gm_pkg_oppr_ld_mend_consign_demand.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ld_mend_consign_demand
IS
    /*************************************************************************
    * Purpose: Procedure used to load sales demand, calculated value and
    * Forecast information
    *************************************************************************/
PROCEDURE gm_op_ld_main (
        p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
        p_demand_type     IN t4020_demand_master.c901_demand_type%TYPE,
        p_demand_period   IN t4020_demand_master.c4020_demand_period%TYPE,
        p_forecast_period IN t4020_demand_master.c4020_forecast_period%TYPE,
        p_inc_current_fl  IN t4020_demand_master.c4020_include_current_fl%TYPE,
        p_inventory_id    IN t250_inventory_lock.c250_inventory_lock_id%TYPE,
        p_dmd_from_period	IN  DATE,
		p_dmd_to_period		IN  DATE,
		p_fct_from_period	IN  DATE,
		p_fct_to_period		IN  DATE
        )
AS
	v_from_period 	DATE;
	v_to_period		DATE;
    v_level_id      t4015_global_sheet_mapping.c901_level_id%TYPE;
    v_level_value   t4015_global_sheet_mapping.c901_level_value%TYPE;
    --
BEGIN

	
	   SELECT c901_level_id, c901_level_value
	     INTO v_level_id, v_level_value
	     FROM t4015_global_sheet_mapping t4015, t4020_demand_master t4020
	    WHERE t4020.c4020_demand_master_id = p_demand_id
	      AND t4020.c4015_global_sheet_map_id = t4015.c4015_global_sheet_map_id;
          
    --To load set related information into temp table 
    gm_pkg_oppr_ld_consign_demand.gm_op_ld_set_info(p_demand_id, v_level_id, v_level_value);
    --
    -- Below procedure called to load Consignment demand information
    IF (p_demand_type = 40021) THEN
    --
        --
        gm_pkg_oppr_ld_consign_demand.gm_op_ld_distributor_info(p_demand_id,v_level_id, v_level_value );
        
      --  DBMS_OUTPUT.put_line ('************ Loading Set Consignment Demand   1****'  || ' - ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS') );
        gm_pkg_oppr_ld_mend_consign_demand.gm_op_ld_demand_fa_set (p_demand_id, p_demand_sheet_id, p_dmd_from_period, p_dmd_to_period);
        
			
      --  DBMS_OUTPUT.put_line ('************ Loading Set Consignment Demand   2****'  || ' - ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS') );
        gm_pkg_oppr_ld_mend_consign_demand.gm_op_ld_demand_fa_item (p_demand_id, p_demand_sheet_id, p_dmd_from_period, p_dmd_to_period);
        
     --   DBMS_OUTPUT.put_line ('************ Loading Item Consignment Demand   3****'  || ' - ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS') );
         

    --    
    ELSE
        
        gm_pkg_oppr_ld_mend_consign_demand.gm_op_ld_demand_inhosue_set (p_demand_id, p_demand_sheet_id, p_dmd_from_period, p_dmd_to_period);
        
	--	DBMS_OUTPUT.put_line ('************ Loading Inhouse Set Demand   4****'  || ' - ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS') );

        gm_pkg_oppr_ld_mend_consign_demand.gm_op_ld_demand_inhouse_item (p_demand_id, p_demand_sheet_id, p_dmd_from_period, p_dmd_to_period);
        
     --   DBMS_OUTPUT.put_line ('************ Loading Inhouse Item Demand   5****'  || ' - ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS') );

        -- This procedure will capture the demand for all the Write off parts during Loaner Reconciliation process .

        gm_pkg_oppr_ld_mend_consign_demand.gm_op_ld_demand_lnr_writeoff(p_demand_id,p_demand_sheet_id,p_dmd_from_period,p_dmd_to_period);


    END IF;

	-- Below procedure called to load Weighted and Avg demand information information
	gm_pkg_oppr_ld_demand.gm_op_ld_demand_calc_avg (p_demand_sheet_id, p_dmd_from_period);
	    
  
    --DBMS_OUTPUT.put_line ('Before calling gm_op_ld_forecast');
    gm_pkg_oppr_ld_mend_consign_demand.gm_op_ld_forecast (p_demand_id, p_demand_sheet_id, p_fct_from_period, p_fct_to_period) ;
         
    -- Below procedure called to load sales Weighted and Avg sales information
    gm_pkg_oppr_ld_demand.gm_op_ld_forecast_avg (p_demand_sheet_id, p_fct_from_period);
    --
    -- Below procedure called to load sales variance
    gm_pkg_oppr_ld_demand.gm_op_ld_forecast_variance (p_demand_sheet_id, p_fct_from_period);
        
    -- Below code to load Pending Requests
    gm_pkg_oppr_ld_mend_consign_demand.gm_op_ld_pending_request (p_demand_id, p_demand_sheet_id, p_fct_from_period, p_inventory_id
    ) ;
    --
     -- Below code to load One time need
    gm_pkg_oppr_ld_mend_consign_demand.gm_op_ld_otn(p_demand_id, p_demand_sheet_id, p_fct_from_period) ;
     


END gm_op_ld_main;



/*************************************************************************
* Purpose: Procedure used to load sales consignment demand for selected period
* 40030 maps to Group Mapping and 40032 maps to Region Mapping
*************************************************************************/
PROCEDURE gm_op_ld_demand_fa_set (
      p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE
    , p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE
    , p_from_period     IN DATE
    , p_to_period       IN DATE
)
AS
   
BEGIN

    --******************************
    -- load demand set information
    --******************************
  
        /* Below code for reference
        gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, set_val.setid, 102663, 50560, set_val.month_value,
        NULL, set_val.cons_qty) ;  */
        

    	/*The temp_set_info table used for improve performance */
    	DELETE FROM temp_set_info;
    		
    	
    	INSERT INTO temp_set_info(c207_set_id,c205_part_number_id,month_value,c207_type)
			SELECT DISTINCT setlist.my_temp_txn_id setid, t208.c205_part_number_id p_num, v9001.month_value,'SITEM'
				FROM my_temp_key_value setlist, t208_set_details t208, v9001_month_list v9001
				WHERE setlist.my_temp_txn_key = 'SET'
				AND setlist.my_temp_txn_id  = t208.c207_set_id
				AND t208.c208_inset_fl           = 'Y'
				AND t208.c208_void_fl           IS NULL
				AND v9001.month_value >= p_from_period
				AND v9001.month_value <= p_to_period;
				
		INSERT INTO temp_set_info(c207_set_id,month_value,c207_type)
			SELECT DISTINCT setlist.my_temp_txn_id setid,v9001.month_value,'SET'
				FROM my_temp_key_value setlist, v9001_month_list v9001
				WHERE setlist.my_temp_txn_key = 'SET'
				AND v9001.month_value >= p_from_period
				AND v9001.month_value <= p_to_period;
					                   
		-- Bulk Insert for Set demand information			                   
		INSERT INTO my_temp_demand_sheet_detail(c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id,c901_ref_type, c901_type, c4042_period
		, c205_part_number_id, c4042_qty)
			SELECT      s4042_demand_sheet_detail.NEXTVAL,p_demand_sheet_id, setid,102663,50560,month_value, NULL, NVL(cons_qty,0)
			FROM
				(SELECT partinfo.setid, partinfo.month_value, cons_qty
				FROM (SELECT tsi.c207_set_id setid, tsi.month_value
					FROM temp_set_info tsi
					WHERE c207_type='SET') partinfo,
						(SELECT   t504.c207_set_id setid,
						t504.c504_ship_date sdate,
						SUM(t504.c504_consign_qty) cons_qty
						FROM st504_consignment_set t504,
						my_temp_list distlist,
						my_temp_key_value setlist
						WHERE t504.c701_distributor_id = distlist.my_temp_txn_id
						AND t504.C5040_plant_id IN  (SELECT C5040_plant_id FROM temp_plant_id)
				AND t504.c207_set_id = setlist.my_temp_txn_id
				AND setlist.my_temp_txn_key = 'SET'
				GROUP BY t504.c207_set_id,t504.c504_ship_date ) consinfo
				WHERE partinfo.month_value = consinfo.sdate(+)
				AND partinfo.setid = consinfo.setid(+));
       
    
    
 		/* Below code for reference
         gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, set_val.setid, 102662, 50560, set_val.month_value,
        set_val.p_num, set_val.cons_qty) ;  */
    	
		-- Bulk Insert for Set part detail demand information
		
		INSERT INTO my_temp_demand_sheet_detail(c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id,c901_ref_type, c901_type, c4042_period
		, c205_part_number_id, c4042_qty)
			SELECT  s4042_demand_sheet_detail.NEXTVAL,p_demand_sheet_id, setid,102662,50560,month_value, p_num, NVL(cons_qty,0)
			FROM
				(SELECT partinfo.setid, partinfo.p_num, partinfo.month_value, cons_qty
						FROM (SELECT tsi.c207_set_id setid, tsi.c205_part_number_id p_num, tsi.month_value
						FROM temp_set_info  tsi
						WHERE c207_type='SITEM') partinfo,
							(SELECT   t505.c207_set_id setid, t505.c205_part_number_id p_num,
							t505.c504_ship_date sdate,
							SUM(t505.c504_consign_qty) cons_qty
							FROM st504_consignment_set_dtl t505,
							my_temp_list distlist,
							my_temp_key_value setlist
							WHERE t505.c701_distributor_id = distlist.my_temp_txn_id
							AND t505.c207_set_id = setlist.my_temp_txn_id
							AND setlist.my_temp_txn_key = 'SET'
							AND t505.C5040_plant_id IN  (SELECT C5040_plant_id FROM temp_plant_id)
						GROUP BY t505.c207_set_id , t505.c205_part_number_id ,t505.c504_ship_date) consinfo
				WHERE partinfo.month_value = consinfo.sdate(+)
				AND partinfo.setid = consinfo.setid(+)
				AND partinfo.p_num = consinfo.p_num (+));

END gm_op_ld_demand_fa_set;


/*************************************************************************
 * Purpose: Procedure used to load consignment item for selected period
 * 40030 maps to Group Mapping and 40032 maps to Region Mapping
 *************************************************************************/
PROCEDURE gm_op_ld_demand_fa_item (
        p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
        p_from_period     IN DATE,
        p_to_period       IN DATE)
AS
       

BEGIN
    -- ****************************
    -- load demand item information
    -- ****************************
 		
	/* Below code for reference
        --DBMS_OUTPUT.put_line ('Sheet ID  ****' || set_val.c205_part_number_id || ' - ' || set_val.item_qty);
        gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, -9999, 102661, 50560, set_val.month_value,
        set_val.p_num, set_val.cons_qty) ;  */

	
	-- Bulk Insert for item consignment demand information
		
		INSERT INTO my_temp_demand_sheet_detail(c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id,c901_ref_type, c901_type, c4042_period
		, c205_part_number_id, c4042_qty)
			SELECT  s4042_demand_sheet_detail.NEXTVAL,p_demand_sheet_id,-9999,102661,50560,month_value, p_num, NVL(cons_qty,0)
			FROM
				(SELECT partinfo.p_num, partinfo.month_value, cons_qty
				FROM 
					(SELECT DISTINCT setpartlist.my_temp_txn_id p_num, v9001.month_value
					FROM my_temp_key_value setpartlist, v9001_month_list v9001
					WHERE setpartlist.my_temp_txn_key = 'ITEM'
					AND v9001.month_value >= p_from_period
					AND v9001.month_value <= p_to_period
					ORDER BY setpartlist.my_temp_txn_id,v9001.month_value
					) partinfo,
						(SELECT   t505.c205_part_number_id p_num,
						t505.c504_ship_date sdate,
						SUM(t505.c504_consign_qty) cons_qty
						FROM st505_consignment_item t505,
						my_temp_list distlist
						WHERE t505.c701_distributor_id = distlist.my_temp_txn_id
						AND t505.C5040_plant_id IN (SELECT C5040_plant_id FROM temp_plant_id)
						GROUP BY t505.c205_part_number_id,t505.c504_ship_date ) consinfo
				WHERE partinfo.month_value = consinfo.sdate(+)
				AND partinfo.p_num = consinfo.p_num (+));
    
END gm_op_ld_demand_fa_item;
   

/*************************************************************************
* Purpose: Procedure used to load inhosue consignment demand for selected period
*************************************************************************/
PROCEDURE gm_op_ld_demand_inhosue_set (
        p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
        p_from_period     IN DATE,
        p_to_period       IN DATE)
AS
    

BEGIN

    --******************************
    -- load demand Inhouse set information
    --******************************
	    /* Below code for reference
        gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, set_val.setid, 102663, 50560, set_val.month_value,
                            NULL, set_val.cons_qty) ; */
	
	
			-- Bulk Insert for Inhouse Set consignment demand information
		
		INSERT INTO my_temp_demand_sheet_detail(c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id,c901_ref_type, c901_type, c4042_period
		, c205_part_number_id, c4042_qty)
			SELECT  s4042_demand_sheet_detail.NEXTVAL,p_demand_sheet_id,setid,102663,50560,month_value, NULL, NVL(cons_qty,0)
			FROM
				(SELECT partinfo.setid, partinfo.month_value,cons_qty
				FROM 
					(SELECT setlist.my_temp_txn_id setid, v9001.month_value
					FROM my_temp_key_value setlist, v9001_month_list v9001
					WHERE  setlist.my_temp_txn_key = 'SET'
					AND v9001.month_value >= p_from_period
					AND v9001.month_value <= p_to_period
					ORDER BY setlist.my_temp_txn_id,v9001.month_value
					) partinfo,
						(SELECT   t504.c207_set_id setid,
						t504.c504_ship_date sdate,
						t504.c504_consign_qty cons_qty
						FROM st504_inhouse_set t504,
						my_temp_key_value setlist
						WHERE t504.c207_set_id = setlist.my_temp_txn_id
						AND setlist.my_temp_txn_key = 'SET'
						AND t504.C5040_plant_id IN  (SELECT C5040_plant_id FROM temp_plant_id)) consinfo
				WHERE partinfo.month_value = consinfo.sdate(+)
				AND partinfo.setid = consinfo.setid(+));
				
	
	

    	/* Below code for reference
        gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, set_val.setid, 102662, 50560, set_val.month_value,
                        set_val.p_num, set_val.cons_qty) ;  */
       
         -- Bulk Insert for Inhouse Set detail consignment demand information
         
		INSERT INTO my_temp_demand_sheet_detail(c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id,c901_ref_type, c901_type, c4042_period
		, c205_part_number_id, c4042_qty)
			SELECT  s4042_demand_sheet_detail.NEXTVAL,p_demand_sheet_id,setid,102662,50560,month_value, p_num, NVL(cons_qty,0)
			FROM
				(SELECT partinfo.setid, partinfo.p_num, partinfo.month_value, cons_qty
				FROM 
					(SELECT DISTINCT setlist.my_temp_txn_id setid, t208.c205_part_number_id p_num, v9001.month_value
					FROM my_temp_key_value setlist, t208_set_details t208, v9001_month_list v9001
					WHERE setlist.my_temp_txn_key = 'SET'
					AND setlist.my_temp_txn_id  = t208.c207_set_id
					AND t208.c208_inset_fl           = 'Y'
					AND t208.c208_void_fl           IS NULL
					AND v9001.month_value >= p_from_period
					AND v9001.month_value <= p_to_period
					ORDER BY setlist.my_temp_txn_id , t208.c205_part_number_id , v9001.month_value
					) partinfo,
						(SELECT   t504.c207_set_id setid, t504.c205_part_number_id p_num,
						t504.c504_ship_date sdate,
						t504.c504_consign_qty cons_qty
						FROM st504_inhouse_set_dtl t504,
						my_temp_key_value setlist
						WHERE t504.c207_set_id = setlist.my_temp_txn_id
						AND setlist.my_temp_txn_key = 'SET'
						AND t504.C5040_plant_id IN  (SELECT C5040_plant_id FROM temp_plant_id)) consinfo
				WHERE partinfo.month_value = consinfo.sdate(+)
				AND partinfo.setid = consinfo.setid(+)
				AND partinfo.p_num = consinfo.p_num (+));    
    
END gm_op_ld_demand_inhosue_set;

/*************************************************************************
 * Purpose: Procedure used to load inhouse item for selected period
 *************************************************************************/
PROCEDURE gm_op_ld_demand_inhouse_item (
        p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
        p_from_period     IN DATE,
        p_to_period       IN DATE)
AS

BEGIN
   		
		/* Below code for reference
        gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, -9999, 102661, 50560, set_val.month_value,
        set_val.p_num, set_val.cons_qty) ; */

			
		-- Bulk Insert for Inhouse item consignment demand information
		INSERT INTO my_temp_demand_sheet_detail(c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id,c901_ref_type, c901_type, c4042_period
		, c205_part_number_id, c4042_qty)
			SELECT  s4042_demand_sheet_detail.NEXTVAL,p_demand_sheet_id,-9999,102661,50560,month_value, p_num, NVL(cons_qty,0)
			FROM
				(SELECT partinfo.p_num, partinfo.month_value, cons_qty
				FROM 
					(SELECT DISTINCT setpartlist.my_temp_txn_id p_num, v9001.month_value
					FROM my_temp_key_value setpartlist, v9001_month_list v9001
					WHERE setpartlist.my_temp_txn_key = 'ITEM'
					AND v9001.month_value >= p_from_period
					AND v9001.month_value <= p_to_period
					ORDER BY setpartlist.my_temp_txn_id, v9001.month_value
					) partinfo,
						(SELECT   t505.c205_part_number_id p_num,
						c504_ship_date sdate,
						c504_consign_qty cons_qty
						FROM st505_inhouse_item t505
						WHERE t505.C5040_plant_id IN  (SELECT C5040_plant_id FROM temp_plant_id)) consinfo
				WHERE partinfo.month_value = consinfo.sdate(+)
				AND partinfo.p_num = consinfo.p_num (+));
    
END gm_op_ld_demand_inhouse_item;

    --
    /*************************************************************************
    * Purpose: Procedure used to capture the DEMAND of all the written off Parts in the
    * Loaner Reconciliation process.
    * -99991 -- Write off part group id
    *************************************************************************/
    PROCEDURE gm_op_ld_demand_lnr_writeoff (
            p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE,
            p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
            p_from_period     IN DATE,
            p_to_period       IN DATE)
    AS
               
    BEGIN
        
			/* Below code for reference
            gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, '-99991', 102661,  50560, 
                write_off_parts.month_value, write_off_parts.p_num, write_off_parts.item_qty) ;  */
    
	    
	    	-- Bulk Insert for Inhouse item consignment demand information

	    	INSERT INTO my_temp_demand_sheet_detail(c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id,c901_ref_type, c901_type, c4042_period
			, c205_part_number_id, c4042_qty)
				SELECT  s4042_demand_sheet_detail.NEXTVAL,p_demand_sheet_id,'-99991',102661,50560,month_value, p_num, NVL(item_qty,0)
				FROM
					(SELECT partinfo.p_num,consinfo.sdate, NVL (c_qty, 0) item_qty, partinfo.month_value
					FROM
						(SELECT DISTINCT setpartlist.my_temp_txn_id p_num, v9001.month_value
						FROM my_temp_key_value setpartlist
						, v9001_month_list v9001
						WHERE setpartlist.my_temp_txn_key = 'ITEM'
						AND v9001.month_value >= p_from_period
						AND v9001.month_value <= p_to_period
						ORDER BY setpartlist.my_temp_txn_id , v9001.month_value
						) partinfo, 
							(SELECT TRUNC(t412.c413_created_date,'month') sdate,
							t412.c205_part_number_id p_num, SUM (t412.c413_item_qty) c_qty
							FROM st412_writeoff t412
							WHERE t412.C5040_plant_id IN  (SELECT C5040_plant_id FROM temp_plant_id)
							GROUP BY TRUNC(t412.c413_created_date,'month'),t412.c205_part_number_id) consinfo
					WHERE partinfo.month_value = consinfo.sdate(+)
					AND partinfo.p_num = consinfo.p_num (+));
	    
	    
        -- Delete the item consignment Part which does not contain any item consignment
        -- for selected period
         DELETE
           FROM my_temp_demand_sheet_detail
          WHERE c901_type             = 50560
            AND c4040_demand_sheet_id = p_demand_sheet_id
            AND c4042_ref_id          = '-99991'
            AND c205_part_number_id  IN
            (
                 SELECT t4042.c205_part_number_id
                   FROM my_temp_demand_sheet_detail t4042
                  WHERE t4042.c901_type             = 50560
                    AND t4042.c4040_demand_sheet_id = p_demand_sheet_id
                    AND t4042.c4042_ref_id          = '-99991'
               GROUP BY t4042.c205_part_number_id
                HAVING SUM (c4042_qty) = 0
            );         
    END gm_op_ld_demand_lnr_writeoff;
 --
    /*************************************************************************
    * Purpose: Procedure used to load sales forecast information
    * Based on the demand weighted will load forecase information
    *************************************************************************/
    PROCEDURE gm_op_ld_forecast (
            p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE,
            p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
            p_from_period     IN DATE,
            p_to_period       IN DATE)
    AS
    	v_history_fl            CHAR (1);
        v_comments_fl           CHAR (1);
        v_weighted_avg_diff     t4042_demand_sheet_detail.c4042_weighted_avg_diff_qty%TYPE;
        v_current_qty           t4042_demand_sheet_detail.c4042_qty%TYPE;
        
        -- Set Level Forecast
        CURSOR forecast_cur_set
        IS
            SELECT   setinfo.setid, setinfo.month_value,
                     setgrowthinfo.c901_ref_type growth_type,
                     setgrowthinfo.c4031_value growth_value, dmdweightavg.weight_avg,
                     setgrowthinfo.comments_fl comments_fl
                FROM (SELECT t4021.c207_set_id setid, t4021.month_value
                        FROM st4021_fct_consign_inhouse_set t4021
                       WHERE t4021.c4020_demand_master_id = p_demand_id) setinfo
                     -- Fetch Set Override info
                    ,(SELECT t4030.c4030_ref_id setid, t4030.c4031_start_date,
                             t4030.c901_ref_type, t4030.c4031_value,
                             NULL comments_fl
                        FROM st4030_fct_set_item_growth t4030
                       WHERE t4030.c4020_demand_master_id = p_demand_id
                         AND t4030.c901_growth_mapping_type = 20296) setgrowthinfo
                     --20296 SET ID
                     -- Fetch Weighted Average info
                    ,(SELECT t4042.c4042_ref_id setid, t4042.c4042_period,
                             t4042.c4042_qty weight_avg
                        FROM my_temp_demand_sheet_detail t4042
                       WHERE t4042.c4040_demand_sheet_id = p_demand_sheet_id
                         AND t4042.c901_type = 50562
                         AND t4042.c205_part_number_id IS NULL) dmdweightavg
               WHERE setinfo.month_value = setgrowthinfo.c4031_start_date(+)
                 AND setinfo.setid = setgrowthinfo.setid(+)
                 AND setinfo.setid = dmdweightavg.setid(+)
            ORDER BY setinfo.setid, setinfo.month_value;        

        -- Cursor to fetch set detail based on forecasted qty 
        CURSOR forecast_cur_set_detail (  v_set_id t208_set_details.c207_set_id%TYPE
                                        , v_set_qty t208_set_details.c208_set_qty%TYPE)
        IS
            SELECT t208.c207_set_id, t208.c205_part_number_id,
                   (t208.c208_set_qty * v_set_qty) c208_set_qty
              FROM t208_set_details t208
             WHERE t208.c207_set_id = v_set_id
               AND t208.c208_inset_fl = 'Y'
               AND t208.c208_set_qty <> 0
               AND t208.c208_void_fl IS NULL ;
                 
        -- Cursor to fetch item forecast information based on item     
        CURSOR forecast_cur_item_part
        IS
            SELECT   iteminfo.p_num, iteminfo.month_value,
                     itemgrowthinfo.c901_ref_type growth_type,
                     itemgrowthinfo.c4031_value growth_value, dmdweightavg.weight_avg,
                     itemgrowthinfo.comments_fl comments_fl, 
                     iteminfo.my_temp_txn_value primaryfl
                FROM (  
                SELECT DISTINCT setpartlist.my_temp_txn_id p_num, v9001.month_value
                             , setpartlist.my_temp_txn_value
                        FROM my_temp_key_value setpartlist, v9001_month_list v9001
                       WHERE setpartlist.my_temp_txn_key = 'ITEM' 
                         AND v9001.month_value >= p_from_period
                         AND v9001.month_value <= p_to_period
                         ) iteminfo
                     -- Fetch Item Override info
                    ,(SELECT st4030.c4030_ref_id p_num, st4030.c4031_start_date,
                             st4030.c901_ref_type, st4030.c4031_value,
                             NULL comments_fl
                        FROM st4030_fct_set_item_growth st4030
                       WHERE st4030.c4020_demand_master_id = p_demand_id
                         AND st4030.c901_growth_mapping_type = 20298) itemgrowthinfo
                     --20298 Part # - Consignment
                     -- Fetch Weighted Average info
                    ,(SELECT t4042.c205_part_number_id p_num, t4042.c4042_period,
                             t4042.c4042_qty weight_avg
                        FROM my_temp_demand_sheet_detail t4042
                       WHERE t4042.c4040_demand_sheet_id = p_demand_sheet_id
                         AND t4042.c901_type = 50562
                         AND t4042.c4042_ref_id = '-9999') dmdweightavg
               WHERE iteminfo.month_value = itemgrowthinfo.c4031_start_date(+)
                 AND iteminfo.p_num = itemgrowthinfo.p_num(+)
                 AND iteminfo.p_num = dmdweightavg.p_num(+)
            ORDER BY iteminfo.p_num, iteminfo.month_value; 

        -- Cursor to fetch write-off forecast information based on item
        CURSOR forecast_cur_writeoff_part
        IS
            SELECT   iteminfo.p_num, iteminfo.month_value,
                     NULL growth_type,
                     NULL growth_value, dmdweightavg.weight_avg,
                     NULL comments_fl
                FROM (  
                SELECT DISTINCT setpartlist.my_temp_txn_id p_num, v9001.month_value
                        FROM my_temp_key_value setpartlist, v9001_month_list v9001
                       WHERE setpartlist.my_temp_txn_key = 'ITEM' 
                         AND setpartlist.my_temp_txn_value = 'Y'   
                         AND v9001.month_value >= p_from_period
                         AND v9001.month_value <= p_to_period
                         ) iteminfo
                     -- Fetch Weighted Average info
                    ,(SELECT t4042.c205_part_number_id p_num, t4042.c4042_period,
                             t4042.c4042_qty weight_avg
                        FROM my_temp_demand_sheet_detail t4042
                       WHERE t4042.c4040_demand_sheet_id = p_demand_sheet_id
                         AND t4042.c901_type = 50562
                         AND t4042.c4042_ref_id = '-99991') dmdweightavg
               WHERE iteminfo.p_num = dmdweightavg.p_num
            ORDER BY iteminfo.p_num, iteminfo.month_value; 
        
    BEGIN
        -- *******************************
        -- load forecast set information
        -- *******************************
                   
        FOR set_val IN forecast_cur_set
        LOOP

        	-- To get forecast value 
            gm_pkg_oppr_ld_demand.gm_op_calc_forecast (set_val.growth_type,
                                   set_val.growth_value,
                                   set_val.weight_avg,
                                   'Y',
                                   v_current_qty,
                                   v_weighted_avg_diff,
                                   v_history_fl
                                  );
                                                
            -- Save Set forecast information
            gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, set_val.setid, 102663, 50563, set_val.month_value,
                            NULL, v_current_qty, NULL, NULL, NULL, v_history_fl, set_val.comments_fl, v_weighted_avg_diff) ;
            --
            FOR set_detail IN forecast_cur_set_detail(set_val.setid,v_current_qty)
            LOOP
            --
                gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, set_val.setid, 102662, 50563, set_val.month_value,
                             set_detail.c205_part_number_id , set_detail.c208_set_qty, NULL, NULL, NULL, v_history_fl
                             , set_val.comments_fl, v_weighted_avg_diff) ;
            --
            END LOOP;
                
        END LOOP;
    
    
        --***********************************
        -- load forecast item information
        --***********************************    
        FOR set_item_val IN forecast_cur_item_part
        LOOP
            
            -- If not primary part and not unit should skip the forecasr 
            IF ( set_item_val.primaryfl = 'N' and NVL(set_item_val.growth_type,0) != 20381)
            THEN
                v_current_qty := 0;
                v_weighted_avg_diff := NULL;
                v_history_fl := NULL;
            ELSE  
                -- To get forecast value 
                gm_pkg_oppr_ld_demand.gm_op_calc_forecast (set_item_val.growth_type,
                                       set_item_val.growth_value,
                                       set_item_val.weight_avg,
                                        set_item_val.primaryfl,
                                       v_current_qty,
                                       v_weighted_avg_diff,
                                       v_history_fl
                                      );
            END IF;   
             
            -- Save Item aggregated value 
            gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id,  -9999, 102661, 50563, set_item_val.month_value,
                             set_item_val.p_num , v_current_qty, NULL, NULL, NULL, v_history_fl, set_item_val.comments_fl, v_weighted_avg_diff) ;
            --
        END LOOP;
        
        --***********************************
        -- load forecast for item write-off 
        --***********************************    
        FOR set_wrieoff_val IN forecast_cur_writeoff_part
        LOOP
            -- To get forecast value 
            gm_pkg_oppr_ld_demand.gm_op_calc_forecast (set_wrieoff_val.growth_type,
                                   set_wrieoff_val.growth_value,
                                   set_wrieoff_val.weight_avg,
                                   'Y',
                                   v_current_qty,
                                   v_weighted_avg_diff,
                                   v_history_fl
                                  );
        
            -- Save Item aggregated value 
            gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, -99991, 102661, 50563, set_wrieoff_val.month_value,
                             set_wrieoff_val.p_num , v_current_qty, NULL, NULL, NULL, v_history_fl, set_wrieoff_val.comments_fl, v_weighted_avg_diff) ;
            --
        END LOOP;
    
    END gm_op_ld_forecast;
 
    /*************************************************************************
    * Purpose: Procedure used to load Pending Requests
    *************************************************************************/
    PROCEDURE gm_op_ld_pending_request (
            p_demand_master_id IN t4020_demand_master.c4020_demand_master_id%TYPE,
            p_demand_sheet_id  IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
            p_from_period      IN DATE,
            p_inventory_id     IN t250_inventory_lock.c250_inventory_lock_id%TYPE)
    AS
 	
    BEGIN
           
	        
	        -- load pending request information
       		/* Below code for reference
                gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, val.ref_id, 102662, val.code_id, p_from_period,
                val.p_num, val.qty, NULL, NULL, NULL) ;  */

	        -- Bulk Insert for load pending request information
	        
		INSERT INTO my_temp_demand_sheet_detail(c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id,c901_ref_type, c901_type, c4042_period
		, c205_part_number_id, c4042_qty,c4042_updated_by, c4042_updated_date, c205_parent_part_num_id)
			SELECT  s4042_demand_sheet_detail.NEXTVAL,p_demand_sheet_id,ref_id,102662,code_id,p_from_period, p_num, qty,NULL,NULL,NULL
			FROM
					(SELECT temp.ref_id ref_id, temp.p_num p_num, SUM (NVL (temp.qty, 0)) qty
					, DECODE (temp.sfl, 10, 50567, 15, 50568, 20, 50569) code_id
					FROM
					(
						SELECT NVL (st253d.c207_set_id, - 9999) ref_id, st253d.c205_part_number_id p_num, st253d.c521_qty qty
						, DECODE (st253d.c520_status_fl, 30, 20, st253d.c520_status_fl) sfl 
						FROM st253d_request_dtl_lock st253d 
						WHERE st253d.c520_request_txn_id    = p_demand_master_id
				  UNION ALL
						SELECT NVL (t504.c207_set_id, - 9999) ref_id, t504.c205_part_number_id p_num, t504.c505_item_qty qty
						, DECODE (t504.c520_status_fl, 30, 20, t504.c520_status_fl) sfl FROM st253a_consignment_lock t504
						WHERE t504.c520_request_txn_id    = p_demand_master_id
						)
						temp
						GROUP BY temp.ref_id, temp.p_num, temp.sfl);
	        
            
            -- Load Open set request 

       			/* Below code for reference
                gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, val.c207_set_id, 102663, val.code_id, p_from_period,
                NULL, val.qty, NULL, NULL, NULL) ; */
            
	    		-- Bulk Insert for Set Level forecast 

		INSERT INTO my_temp_demand_sheet_detail	(c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id,c901_ref_type, 
		c901_type, c4042_period, c205_part_number_id, c4042_qty,c4042_updated_by, c4042_updated_date, c205_parent_part_num_id)
			SELECT s4042_demand_sheet_detail.NEXTVAL,p_demand_sheet_id,c207_set_id,102663,code_id,p_from_period, NULL, NVL(qty,0),NULL,NULL,NULL
			FROM
				(SELECT st253d.c207_set_id, DECODE (st253d.c520_status_fl, 10, 50567, 15, 50568, 20, 50569,30, 50569) code_id
				, COUNT(1) qty
				FROM st253c_request_lock st253d
				WHERE st253d.c520_request_txn_id    = p_demand_master_id
				AND st253d.c520_master_request_id IS NULL
				AND st253d.c520_required_date 	 < p_from_period
				GROUP BY  st253d.c207_set_id, st253d.c520_status_fl);
												
            	/* Below code for reference
                gm_pkg_oppr_ld_demand.gm_sav_demand_sheet_request (p_demand_sheet_id, val.request_id, 50632,val.sfl) ;  */
     

            -- Bulk Insert for open_requests 
            
		INSERT INTO t4044_demand_sheet_request
					(c4044_demand_sheet_request_id, c4040_demand_sheet_id, c520_request_id, c901_source_type
				   , c4044_lock_status_fl)
			SELECT  s4044_demand_sheet_request.NEXTVAL,p_demand_sheet_id,request_id,50632,sfl
			FROM
				(SELECT st253c.c520_request_id request_id, st253c.c520_status_fl sfl
				, st253c.c207_set_id
				, DECODE (st253c.c520_status_fl, 10, 50567, 15, 50568, 20, 50569, 30, 50569) code_id
				FROM st253c_request_lock st253c
				WHERE st253c.c520_request_txn_id    = p_demand_master_id
				AND st253c.c520_master_request_id IS NULL);				 
									
            
     END gm_op_ld_pending_request;
     
    /*************************************************************************
    * Purpose: Procedure used to load sheet OTN Information
    * OTN -- One Time need
    * OTN -- Only Appliciable for Items
    *************************************************************************/
    PROCEDURE gm_op_ld_otn (
            p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE,
            p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
            p_to_period       IN DATE)
    AS
        
    BEGIN
        
       		/* Below code for reference
            gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, -9999, 102661, 4000112, p_to_period,
            otn_val.c205_part_number_id, otn_val.otnvalue, NULL, NULL, NULL) ; */


	    		-- Bulk Insert for load par information
         
		INSERT INTO my_temp_demand_sheet_detail(c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id,c901_ref_type, c901_type, c4042_period
		, c205_part_number_id, c4042_qty,c4042_updated_by, c4042_updated_date, c205_parent_part_num_id)
			SELECT  s4042_demand_sheet_detail.NEXTVAL,p_demand_sheet_id,-9999,102661,4000112,p_to_period, c205_part_number_id, otnvalue,
			NULL,NULL,NULL
			FROM
				(SELECT t4023.c205_part_number_id, t4023.c4023_qty  otnvalue
				FROM t4023_demand_otn_detail t4023
				WHERE t4023.c4020_demand_master_id = p_demand_id
				AND   C4023_VOID_FL IS NULL);
        --
    END gm_op_ld_otn;
  
END gm_pkg_oppr_ld_mend_consign_demand;
/
