/* Formatted on 2011/06/09 12:16 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\purchasing\gm_pkg_oppr_ld_mend_sales_demand.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ld_mend_sales_demand
IS
	/*************************************************************************
	 * Purpose: Procedure used to load sales demand, calculated value and
	 *	Forecast information
	 *************************************************************************/
	PROCEDURE gm_op_ld_main (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_demand_period 	IN	 t4020_demand_master.c4020_demand_period%TYPE
	  , p_forecast_period	IN	 t4020_demand_master.c4020_forecast_period%TYPE
	  , p_inc_current_fl	IN	 t4020_demand_master.c4020_include_current_fl%TYPE
	  , p_dmd_from_period	IN  DATE
	  , p_dmd_to_period		IN  DATE
	  , p_fct_from_period	IN  DATE
	  , p_fct_to_period		IN  DATE
	)
	AS
	v_from_period 	DATE;
	v_to_period		DATE;
		
		
	--
	BEGIN

		
        -- Below code to load required account info
        gm_pkg_oppr_ld_sales_demand.gm_op_ld_account_info(p_demand_id);
        
        -- Below procedure called to load sales demand information
		
		gm_pkg_oppr_ld_mend_sales_demand.gm_op_ld_demand (p_demand_id, p_demand_sheet_id, p_dmd_from_period, p_dmd_to_period);
        	
        
        -- Below procedure called to load sales Weighted and Avg sales information
		gm_pkg_oppr_ld_demand.gm_op_ld_demand_calc_avg (p_demand_sheet_id, p_fct_from_period);
		
		--gm_pkg_oppr_ld_mend_demand.gm_op_calc_period (p_load_date,12, NULL, 'F', v_fct_from_period, v_fct_to_period);
		                              
		-- Below procedure called to load forecast information
		gm_pkg_oppr_ld_sales_demand.gm_op_ld_forecast (p_demand_id, p_demand_sheet_id, p_fct_from_period, p_fct_to_period);
        
		-- Below procedure called to load sales Weighted and Avg sales information
		gm_pkg_oppr_ld_demand.gm_op_ld_forecast_avg (p_demand_sheet_id, p_fct_from_period);
		--
		-- Below procedure called to load sales variance
		gm_pkg_oppr_ld_demand.gm_op_ld_forecast_variance (p_demand_sheet_id, p_fct_from_period);
        
       	-- Below procedure called to load Sales Backorder
		gm_pkg_oppr_ld_mend_sales_demand.gm_op_ld_sales_bo (p_demand_id, p_demand_sheet_id, p_fct_from_period);
	--
	END gm_op_ld_main;

 
	/*************************************************************************
	 * Purpose: Procedure used to load sales demand for selected period
	 * 40030 maps to Group Mapping and 40032 maps to Region Mapping
	 *************************************************************************/
	PROCEDURE gm_op_ld_demand (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_from_period		IN	 DATE
	  , p_to_period 		IN	 DATE
	)
	AS
		--
		v_cur_part 		t4011_group_detail.c205_part_number_id%TYPE;
		v_pre_part 		t4011_group_detail.c205_part_number_id%TYPE;
		v_cur_month 	v9001_month_list.month_value%TYPE;
		v_pre_month 	v9001_month_list.month_value%TYPE;
		v_cur_grpid 	t4010_group.c4010_group_id%TYPE;
		v_pre_grpid 	t4010_group.c4010_group_id%TYPE;
		v_price_type    t4011_group_detail.c901_part_pricing_type%TYPE;
		--
		CURSOR demand_cur
		IS
			SELECT      partgrpinfo.c4010_group_id, partgrpinfo.c205_part_number_id, partgrpinfo.month_value
			, SUM (NVL (c502_item_qty, 0)) item_qty ,partgrpinfo.price_type price_type
				FROM 
					(SELECT t4011.c4010_group_id, t4011.c205_part_number_id, v9001.month_value, t4011.c901_part_pricing_type price_type
						FROM t4010_group t4010
						, t4021_demand_mapping t4021
						, t4011_group_detail t4011
						, v9001_month_list v9001
						WHERE t4021.c4020_demand_master_id = p_demand_id
						AND t4021.c901_ref_type = 40030
						AND t4021.c4021_ref_id = t4010.c4010_group_id
						AND t4010.c4010_group_id = t4011.c4010_group_id
						AND v9001.month_value >= p_from_period
						AND v9001.month_value <= p_to_period
						) partgrpinfo
						, (SELECT c501_order_date order_date
						, t501.c205_part_number_id, t501.c502_item_qty
						FROM my_temp_list mtl
						, st501_order_by_part t501
						WHERE t501.c704_account_id = mtl.my_temp_txn_id
						) partorderinfo
						WHERE partgrpinfo.month_value = partorderinfo.order_date(+)
						AND partgrpinfo.c205_part_number_id = partorderinfo.c205_part_number_id(+)
						GROUP BY partgrpinfo.c4010_group_id, partgrpinfo.c205_part_number_id, partgrpinfo.month_value, partgrpinfo.price_type
						ORDER BY c205_part_number_id,month_value;
	BEGIN
		--
		-- load demand information
		FOR set_val IN demand_cur
		LOOP
			--
			v_cur_month := set_val.month_value;
			v_cur_part  := set_val.c205_part_number_id;
			v_cur_grpid := set_val.c4010_group_id;
			v_price_type := set_val.price_type;
			
			-- When Same Part exist in same Sheet and in two different groups , then we are excluding the demand for Secondary group the part belongs to. 
			IF (v_cur_month = v_pre_month AND v_cur_part = v_pre_part 
				AND v_cur_grpid != v_pre_grpid AND v_price_type != '52080')
			THEN
				NULL;
			ELSE				
				gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
														  , set_val.c4010_group_id
	                                                      , 102660
														  , 50560
														  , set_val.month_value
														  , set_val.c205_part_number_id
														  , set_val.item_qty
														   );
			END IF;
			
			v_pre_month := v_cur_month;
			v_pre_part  := v_cur_part;
			v_pre_grpid := v_cur_grpid;
			v_cur_month := '';
			v_cur_part := '';
			v_cur_grpid := '';
			v_price_type := '';
		--
		END LOOP;
	--
	END gm_op_ld_demand;



/*************************************************************************
 * Purpose: Procedure used to load sales back order
 *************************************************************************/
	PROCEDURE gm_op_ld_sales_bo (
		p_demand_master_id	 IN   t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	 IN   t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_to_period 		 IN   DATE
	)
	AS
		
	BEGIN
		
		/* Below code for reference
		gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , val.ref_id
                                                      , 102660            
													  , 50567
													  , p_to_period
													  , val.p_num
													  , val.qty
													   ); */
		
		INSERT INTO my_temp_demand_sheet_detail (c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id, c901_ref_type, c901_type, c4042_period
		, c205_part_number_id, c4042_qty)
			SELECT  s4042_demand_sheet_detail.NEXTVAL,p_demand_sheet_id, ref_id,102660,50567,p_to_period, p_num, qty
			FROM
				(SELECT  t4011.c4010_group_id ref_id, t253f.c205_part_number_id p_num, SUM (t253f.c502_item_qty) qty
					FROM my_temp_list mtl
					, t4021_demand_mapping t4021
					, t4011_group_detail t4011
					, t253e_order_lock t253e
					, t253f_item_order_lock t253f
					, t4040_demand_sheet t4040
					WHERE t4021.c4020_demand_master_id = p_demand_master_id
					AND t4040.c4020_demand_master_id = p_demand_master_id
					AND t4040.c4040_demand_sheet_id = p_demand_sheet_id
					AND t4021.c901_ref_type = 40032
					AND t253e.c704_account_id = mtl.my_temp_txn_id
					AND t253e.c501_order_id = t253f.c501_order_id
					AND NVL (t253e.c901_order_type, -999) = 2525
					AND t253e.c501_status_fl = 0
					AND t253e.c501_void_fl IS NULL
					AND t4011.c4010_group_id IN (SELECT c4021_ref_id
						FROM t4021_demand_mapping t4021
						WHERE t4021.c4020_demand_master_id = p_demand_master_id
						AND t4021.c901_ref_type = 40030)
					AND t253f.c205_part_number_id = t4011.c205_part_number_id
					AND t253e.c250_inventory_lock_id = t4040.c250_inventory_lock_id
					AND t253f.c250_inventory_lock_id = t4040.c250_inventory_lock_id
					GROUP BY t4011.c4010_group_id, t253f.c205_part_number_id);
							
	END gm_op_ld_sales_bo;
--
END gm_pkg_oppr_ld_mend_sales_demand;
/
