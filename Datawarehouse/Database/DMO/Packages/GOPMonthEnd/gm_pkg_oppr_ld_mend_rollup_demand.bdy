/* Formatted on 2011/06/09 12:16 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\purchasing\gm_pkg_oppr_ld_mend_rollup_demand.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ld_mend_rollup_demand
IS
	/*************************************************************************
	 * Purpose: Procedure used to load sales demand, calculated value and
	 *	Forecast information
	 *************************************************************************/
	PROCEDURE gm_op_ld_main (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_demand_period 	IN	 t4020_demand_master.c4020_demand_period%TYPE
	  , p_forecast_period	IN	 t4020_demand_master.c4020_forecast_period%TYPE
	  , p_inc_current_fl	IN	 t4020_demand_master.c4020_include_current_fl%TYPE
      , p_inventory_id      IN  t250_inventory_lock.c250_inventory_lock_id%TYPE
  	  , p_load_date	DATE
	)
	AS
	    --
		v_fct_from_period  DATE;
		v_fct_to_period    DATE;
        --
        v_level_id                   t4015_global_sheet_mapping.c901_level_id%TYPE;
        v_level_value                t4015_global_sheet_mapping.c901_level_value%TYPE;
        v_demand_sheet_type          t4020_demand_master.c901_demand_type%TYPE;
        v_parent_demand_master_id    t4020_demand_master.c4020_parent_demand_master_id%TYPE; 
	--
	BEGIN
		
	   -- To get sheet related master information  
       SELECT t4015.c901_level_id, t4015.c901_level_value, t4020.c901_demand_type
              , t4020.c4020_parent_demand_master_id    
         INTO v_level_id, v_level_value, v_demand_sheet_type,v_parent_demand_master_id  
         FROM t4015_global_sheet_mapping t4015, t4020_demand_master t4020
        WHERE t4020.c4020_demand_master_id = p_demand_id
          AND t4015.c4015_global_sheet_map_id = t4020.c4015_global_sheet_map_id; 


		--DBMS_OUTPUT.put_line ('************ Before calling load sheet info - ' || p_demand_id|| ' *** ' || p_demand_sheet_id || ' **** ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
		
        -- Below code to load required account info
        gm_pkg_oppr_ld_rollup_demand.gm_op_ld_sheetinfo(p_demand_id, p_demand_sheet_id, v_level_id, v_level_value, v_demand_sheet_type , v_parent_demand_master_id);
        gm_pkg_oppr_ld_rollup_demand.gm_op_sav_sheet_assoc_map(p_demand_id, p_demand_sheet_id);
        gm_pkg_oppr_ld_rollup_demand.gm_op_sav_sheet_growth(p_demand_sheet_id);
        
		--DBMS_OUTPUT.put_line ('************ load sheet 1' || ' - ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
		gm_pkg_oppr_ld_mend_rollup_demand.gm_op_ld_demand (p_demand_sheet_id);
		--
		-- Below code to fetch forecast (from and to period)
		gm_pkg_oppr_ld_mend_demand.gm_op_calc_period (p_load_date,p_forecast_period, NULL, 'F', v_fct_from_period, v_fct_to_period);
     
        
		-- Below procedure called to load sales Weighted and Avg sales information
		gm_pkg_oppr_ld_demand.gm_op_ld_demand_calc_avg (p_demand_sheet_id, v_fct_from_period);
                              
		--
		-- Below procedure called to load forecast information
		--DBMS_OUTPUT.put_line ('************ load sheet 3' || ' - ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
		gm_pkg_oppr_ld_rollup_demand.gm_op_ld_forecast (p_demand_id, p_demand_sheet_id, v_fct_from_period, v_fct_to_period, 50563);
		--
		--DBMS_OUTPUT.put_line ('************ load sheet 3.1' || ' - ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
        -- Below procedure called to load sales Weighted and Avg sales information
		gm_pkg_oppr_ld_demand.gm_op_ld_forecast_avg (p_demand_sheet_id, v_fct_from_period);
		--
		--DBMS_OUTPUT.put_line ('************ load sheet 4' || ' - ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));

		gm_pkg_oppr_ld_demand.gm_op_ld_forecast_variance (p_demand_sheet_id, v_fct_from_period);
		--DBMS_OUTPUT.put_line ('************ load sheet 5' || ' - ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
        gm_pkg_oppr_ld_mend_rollup_demand.gm_op_ld_otn(p_demand_sheet_id,4000112);
        
        -- Loading back order from other sheet is not required because of TPR 
        -- Need to be revisited 
        gm_pkg_oppr_ld_mend_rollup_demand.gm_op_ld_bo(p_demand_sheet_id);
        
        -- Par Setup applicable for Company level sheet -- World Wide sheet  
        IF (v_level_id = 102580) 
        THEN 
            --40020 Sales Par 
            IF (v_demand_sheet_type =  40020) 
            THEN 
        		-- Below procedure called to load Par Information [To be moved to World wide sheet]
        		gm_pkg_oppr_ld_mend_rollup_demand.gm_op_ld_sales_par (p_demand_id, p_demand_sheet_id, v_fct_from_period);
                
            ELSE 
        		-- Below procedure called to load Par Information [To be moved to World wide sheet]
        		gm_pkg_oppr_ld_mend_rollup_demand.gm_op_ld_consign_par (p_demand_id, p_demand_sheet_id, v_fct_from_period);
                
                --Below code to load Pending Requests
                gm_pkg_oppr_ld_mend_consign_demand.gm_op_ld_pending_request (p_demand_id, p_demand_sheet_id, v_fct_from_period
                            , p_inventory_id) ;
            END IF;
            
            -- Load sub component information 
            gm_pkg_oppr_ld_rollup_demand.gm_op_ld_sub_components (p_demand_sheet_id);

            -- ************************************************************************
            -- To load OUS Forecast information (102581) will load division level sheet
            -- ************************************************************************  
			--DBMS_OUTPUT.put_line ('************ load sheet 5.1' || ' - ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
            gm_pkg_oppr_ld_rollup_demand.gm_op_ld_sheetinfo(p_demand_id, p_demand_sheet_id, 102581, NULL, v_demand_sheet_type , v_parent_demand_master_id);
            
            gm_pkg_oppr_ld_mend_rollup_demand.gm_op_ld_otn(p_demand_sheet_id,4000120);    
            
            gm_pkg_oppr_ld_rollup_demand.gm_op_ld_forecast (p_demand_id, p_demand_sheet_id, v_fct_from_period, v_fct_to_period, 4000111);
            
            -- ************************************************************************
            -- To load US Sheet information (-11111 will only load US Sheet
            -- ************************************************************************               
			--DBMS_OUTPUT.put_line ('************ load sheet 5.2' || ' - ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
            gm_pkg_oppr_ld_rollup_demand.gm_op_ld_sheetinfo(p_demand_id, p_demand_sheet_id, -11111, NULL, v_demand_sheet_type , v_parent_demand_master_id);

            gm_pkg_oppr_ld_mend_rollup_demand.gm_op_ld_otn(p_demand_sheet_id,4000119);
            
            gm_pkg_oppr_ld_rollup_demand.gm_op_ld_us_forecast(p_demand_sheet_id);
            
        END IF;
            
	--
	END gm_op_ld_main;

	/*************************************************************************
	 * Purpose: Procedure used to load demand for selected period 
	 *  and for selected group
	 *************************************************************************/
	PROCEDURE gm_op_ld_demand (
	   p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	)
	AS

	BEGIN
		
		
		/* Below code for reference
			gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , set_val.c4042_ref_id
                                                      , set_val.reftype  
													  , 50560
													  , set_val.month_value
													  , set_val.c205_part_number_id
													  , set_val.item_qty
													   );  */

	-- Bulk Insert for load demand information

		INSERT INTO my_temp_demand_sheet_detail(c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id,c901_ref_type, c901_type, c4042_period
		, c205_part_number_id, c4042_qty)
				SELECT  s4042_demand_sheet_detail.NEXTVAL,p_demand_sheet_id,c4042_ref_id,reftype,50560,month_value, c205_part_number_id, item_qty
				FROM
					(SELECT t4042.c4042_ref_id, t4042.c205_part_number_id, t4042.c4042_period month_value
					, c901_ref_type reftype , SUM (t4042.c4042_qty) item_qty
					FROM st4042_demand_sheet_detail t4042, my_temp_key_value associated_sheet
					WHERE t4042.c4040_demand_sheet_id = associated_sheet.my_temp_txn_id AND t4042.c901_type = 50560
					AND associated_sheet.my_temp_txn_key = 'SHEET'
					GROUP BY t4042.c4042_ref_id, t4042.c205_part_number_id, t4042.c4042_period, c901_ref_type);

							
	END gm_op_ld_demand;

--
	/*************************************************************************
	 * Purpose: Procedure used to load OTN for selected period 
	 *  and for selected key value
     * To be used for ALL, US and OUS
	 *************************************************************************/
	PROCEDURE gm_op_ld_otn (
	   p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
     , p_type               IN	 t4042_demand_sheet_detail.c901_type%TYPE
	)
	AS
            
	BEGIN
		
				/* Below code for reference
				gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , set_val.c4042_ref_id
                                                      , set_val.reftype 
													  , p_type
													  , set_val.month_value
													  , set_val.c205_part_number_id
													  , set_val.item_qty
													   ); */
	
		-- Bulk Insert for load par information

		INSERT INTO my_temp_demand_sheet_detail(c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id,c901_ref_type, c901_type, c4042_period
		, c205_part_number_id, c4042_qty)
			SELECT  s4042_demand_sheet_detail.NEXTVAL, p_demand_sheet_id, c4042_ref_id, reftype, p_type, month_value, c205_part_number_id, item_qty
			FROM
				(SELECT    t4042.c4042_ref_id, t4042.c205_part_number_id, t4042.c4042_period month_value
				, c901_ref_type reftype, SUM (t4042.c4042_qty) item_qty
				FROM st4042_demand_sheet_detail t4042, my_temp_key_value associated_sheet
				WHERE t4042.c4040_demand_sheet_id = associated_sheet.my_temp_txn_id AND t4042.c901_type = 4000112
				AND associated_sheet.my_temp_txn_key = 'SHEET'
				GROUP BY t4042.c4042_ref_id, t4042.c205_part_number_id, t4042.c4042_period, c901_ref_type);

	END gm_op_ld_otn;

	/*************************************************************************
	 * Purpose: Procedure used to load par information for Sales sheet 
     * Currently load is programed for all sheet and world wide sheet is handled 
     * in ld_main work flow 
	 *************************************************************************/
	PROCEDURE gm_op_ld_sales_par (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_to_period 		IN	 DATE
	)
	AS

	BEGIN
		-- load par information
       		/* Below code for reference
			gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , set_val.c4010_group_id
                                                      , 102660  
													  , 50566
													  , p_to_period
													  , set_val.c205_part_number_id
													  , set_val.parvalue
													   ); */
	
	    -- Bulk Insert for load par information

		INSERT INTO my_temp_demand_sheet_detail(c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id,c901_ref_type, c901_type, c4042_period
		, c205_part_number_id, c4042_qty)
			SELECT  s4042_demand_sheet_detail.NEXTVAL,p_demand_sheet_id,c4010_group_id,102660,50566,p_to_period, c205_part_number_id, parvalue
			FROM
				(SELECT t4011.c4010_group_id, t4011.c205_part_number_id, NVL (pvalue.parvalue, 0) parvalue
				FROM t4010_group t4010
				, t4021_demand_mapping t4021
				, t4011_group_detail t4011
				, (SELECT c205_part_number_id, c4022_par_value parvalue
				FROM t4022_demand_par_detail
				WHERE c4020_demand_master_id = p_demand_id
				AND c205_part_number_id IS NOT NULL) pvalue
				WHERE t4021.c4020_demand_master_id = p_demand_id
				AND t4021.c901_ref_type = 40030
				AND t4021.c4021_ref_id = t4010.c4010_group_id
				AND t4010.c4010_group_id = t4011.c4010_group_id
				AND t4011.c205_part_number_id = pvalue.c205_part_number_id(+));


	END gm_op_ld_sales_par;

	/*************************************************************************
	 * Purpose: Procedure used to load par information for consignment sheet
     * Will calculate Item and Set Par 
     * Currently load is rogramed for all sheet and world wide sheet is handled 
     * in ld_main work flow 
	 *************************************************************************/
	PROCEDURE gm_op_ld_consign_par (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_to_period 		IN	 DATE
	)
	AS

		-- Set Par
        CURSOR par_set_calc_cur
		IS
			SELECT  t4022.c207_set_id,  c4022_par_value parvalue
        	  FROM t4022_demand_par_detail t4022
        	 WHERE c4020_demand_master_id = p_demand_id 
               AND t4022.c207_set_id IS NOT NULL;


        -- Cursor to fetch set detail based on forecasted qty 
        CURSOR forecast_cur_set_detail (  v_set_id t208_set_details.c207_set_id%TYPE
                                        , v_set_qty t208_set_details.c208_set_qty%TYPE)
        IS
            SELECT t208.c207_set_id, t208.c205_part_number_id,
                   (t208.c208_set_qty * v_set_qty) c208_set_qty
              FROM t208_set_details t208
             WHERE t208.c207_set_id = v_set_id
               AND t208.c208_inset_fl = 'Y'
               AND t208.c208_set_qty <> 0
               AND t208.c208_void_fl IS NULL ;
                                      
	BEGIN
		
       		/* Below code for reference
			gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , set_val.item_id
                                                      , 102661
													  , 50566
													  , p_to_period
													  , set_val.c205_part_number_id
													  , set_val.parvalue
													   ); */

	-- Bulk Insert for item par information
			
		INSERT INTO my_temp_demand_sheet_detail(c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id,c901_ref_type, c901_type, c4042_period
		, c205_part_number_id, c4042_qty)
			SELECT  s4042_demand_sheet_detail.NEXTVAL,p_demand_sheet_id,item_id,102661,50566,p_to_period, c205_part_number_id, parvalue
			FROM
				(SELECT '-9999' item_id, c205_part_number_id, c4022_par_value parvalue
				FROM t4022_demand_par_detail t4022
				WHERE c4020_demand_master_id = p_demand_id
				AND c205_part_number_id IS NOT NULL);



       		/* Below code for reference
			gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , set_val.c207_set_id
                                                      , 102663
													  , 4000113
													  , p_to_period
													  , NULL
													  , set_val.parvalue
													   ); */
													   
 	-- load set par information
		FOR set_val IN par_set_calc_cur
		LOOP
			-- load par Info
			gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , set_val.c207_set_id
                                                      , 102663
													  , 4000113
													  , p_to_period
													  , NULL
													  , set_val.parvalue
													   );

            -- Load set related information 
            FOR set_detail IN forecast_cur_set_detail(set_val.c207_set_id, set_val.parvalue)
            LOOP
            --
                gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, set_val.c207_set_id, 102662,  4000113, p_to_period,
                             set_detail.c205_part_number_id , set_detail.c208_set_qty, NULL, NULL, NULL) ;
            --
            END LOOP;
            --
		END LOOP;
	--
	END  gm_op_ld_consign_par;

--
/*************************************************************************
 * Purpose: Procedure used to load back order from all related sheet
 *************************************************************************/
	PROCEDURE gm_op_ld_bo (
	    p_demand_sheet_id	 IN   t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	)
	AS

	BEGIN

		
			/* Below code for reference
			gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , set_val.c4042_ref_id
                                                      , set_val.c901_ref_type  
													  , set_val.c901_type
													  , set_val.month_value
													  , set_val.c205_part_number_id
													  , set_val.item_qty
													   ); */
	    -- Bulk Insert for load par information

		INSERT INTO my_temp_demand_sheet_detail(c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id,c901_ref_type, c901_type, c4042_period
		, c205_part_number_id, c4042_qty)
			SELECT  s4042_demand_sheet_detail.NEXTVAL,p_demand_sheet_id,c4042_ref_id,c901_ref_type,c901_type,month_value, c205_part_number_id, item_qty
			FROM
				(SELECT    t4042.c4042_ref_id, t4042.c901_ref_type , t4042.c205_part_number_id, t4042.c4042_period month_value
				, t4042.c901_type , SUM (t4042.c4042_qty) item_qty
				FROM st4042_demand_sheet_detail t4042, my_temp_key_value associated_sheet
				WHERE t4042.c4040_demand_sheet_id = associated_sheet.my_temp_txn_id
				AND t4042.c901_type IN (50567, 50568, 50569)
				AND associated_sheet.my_temp_txn_key = 'SHEET'
				GROUP BY t4042.c4042_ref_id,  t4042.c901_ref_type ,t4042.c205_part_number_id, t4042.c4042_period, t4042.c901_type);

	END gm_op_ld_bo;

END gm_pkg_oppr_ld_mend_rollup_demand;
/
