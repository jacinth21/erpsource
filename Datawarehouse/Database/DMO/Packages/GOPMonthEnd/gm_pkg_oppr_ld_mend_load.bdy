CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ld_mend_load
IS
/*******************************************************
 * Purpose: This pkg used for load GOP monthend Load
 *
 * modification history
 * ====================
 * Person		 Date	Comments
 * ---------   ------	------------------------------------------
 * psrinivasan    20191008  Initial Version
 *******************************************************/

	/*******************************************************
	* Purpose: 1. This procedure will load part attribute- cost
	*******************************************************/
	PROCEDURE gm_op_ld_part_attribute
	AS
	
	v_load_date  DATE;
	v_inventory_id  t250_inventory_lock.c250_inventory_lock_id%TYPE;
	
	BEGIN
	-- Context setup
	gm_pkg_cor_client_context.gm_sav_client_context('1000',get_code_name('105440'),get_code_name('105130'),'3000');

	-- To get load date basd on sysdate	
	gm_pkg_oppr_ld_mend_inventory.gm_op_fch_load_date(v_load_date);
	DBMS_OUTPUT.PUT_LINE('v_load_date:'||v_load_date);

	-- This code will get Inventroy Id
	gm_pkg_oppr_ld_mend_inventory.gm_op_fch_inventory_lock_id(v_load_date,20430,1,v_inventory_id);
	DBMS_OUTPUT.PUT_LINE('v_inventory_id:'||v_inventory_id);


	 DELETE FROM my_temp_part_list ;

         INSERT INTO my_temp_part_list
            (c205_part_number_id
            )
         SELECT DISTINCT c205_part_number_id pnum
           FROM t205_part_number t205,t2021_project_company_mapping t2021
           WHERE t205.c202_project_id=t2021.c202_project_id
           AND t2021.c1900_company_id<>1001
           AND t2021.c2021_void_fl IS NULL ;-- To execlude BBA Projects
          

	gm_pkg_oppr_ld_mend_inventory.gm_op_ld_part_attribute (v_inventory_id);
	
	END gm_op_ld_part_attribute;
	
	/*******************************************************
	* Purpose: 2. This procedure will load Inventroy for US and OUS
	* exec gm_pkg_oppr_ld_mend_load.gm_op_ld_main();
	*******************************************************/
	PROCEDURE gm_op_ld_inventory
	AS
	v_load_date  DATE;
	
	BEGIN
	-- To get load date basd on sysdate		
	gm_pkg_oppr_ld_mend_inventory.gm_op_fch_load_date(v_load_date);
	
	gm_pkg_oppr_ld_mend_inventory.gm_op_ld_demand_main (20430,v_load_date);
	--20430 Main inventroy lock
	
	END gm_op_ld_inventory;

	/*******************************************************
	* Purpose: 3. This procedure will load GOP Monthend Load by demand master
	* exec gm_pkg_oppr_ld_mend_load.gm_op_ld_main();
	*******************************************************/
	PROCEDURE gm_op_ld_main 
	AS
	v_demandsheetid  VARCHAR2(20);
	job_id number;
	v_ttp_detail_id t4052_ttp_detail.c4052_ttp_detail_id%TYPE;
	v_inventory_id  t250_inventory_lock.c250_inventory_lock_id%TYPE;
	v_ttp_id 		t4051_ttp_demand_mapping.c4050_ttp_id%TYPE;
	v_load_date  		DATE;
	v_fct_from_period 	DATE;
	v_fct_to_period 	DATE;
	v_dmd_from_period 	DATE;
	v_dmd_to_period 	DATE;
	v_ttp_detail_cnt	NUMBER;
 	
	BEGIN
	
			--For disable 2min job
	
			SELECT job INTO job_id
			FROM user_jobs 
			WHERE what='GLOBUS_DM_OPERATION.GM_PKG_OPPR_LD_DEMAND.GM_OP_EXEC_JOB_DETAIL;' ;
		
		dbms_job.broken(job_id, TRUE );
		COMMIT;

		-- To get load date basd on sysdate	
		gm_pkg_oppr_ld_mend_inventory.gm_op_fch_load_date(v_load_date);
		
		-- For Demand
		gm_pkg_oppr_ld_mend_demand.gm_op_calc_period (v_load_date,6, NULL, 'D', v_dmd_from_period, v_dmd_to_period);
		
		--For Forecast
		gm_pkg_oppr_ld_mend_demand.gm_op_calc_period (v_load_date,12, NULL, 'F', v_fct_from_period, v_fct_to_period);
		
	
			-- Below code to get current inventory value
	        -- Should always pick US (Company level Inventory) 
	        -- Thats the reason global inventory hardcoded to 1 
			SELECT MAX (t250.c250_inventory_lock_id)
			  	INTO v_inventory_id
			 FROM t250_inventory_lock t250
			 WHERE t250.c901_lock_type = 20430
	         AND t250.c4016_global_inventory_map_id  = 1
	         AND t250.c250_void_fl IS NULL;
	

	    FOR someparent IN (
	        SELECT t4020.c4020_demand_master_id, t4020.c4020_demand_nm FROM t4020_demand_master t4020
	        WHERE t4020.c4020_parent_demand_master_id IS NULL  
	        AND t4020.c4020_inactive_fl IS NULL
	        AND t4020.c901_company_id IS NOT NULL
	        ORDER BY UPPER(t4020.c4020_demand_nm) )
	    LOOP
	    	
	    	-- For Creating TTP Details Id
		         v_ttp_detail_id := NULL;
             
		         BEGIN  
                   SELECT c4050_ttp_id
                    INTO v_ttp_id
                    FROM t4051_ttp_demand_mapping
                    WHERE c4020_demand_master_id=someparent.c4020_demand_master_id ;
                EXCEPTION WHEN OTHERS
                       THEN
                            v_ttp_id := NULL;
                 END;
	
	        
			   IF v_ttp_id IS NOT NULL
			   THEN
			   	--Check if TTP Detail exists for the current Month for the TTP.
			   		SELECT count(1)
			   			INTO v_ttp_detail_cnt
			   		FROM t4052_ttp_detail
			   		WHERE c4050_ttp_id = v_ttp_id
			   		AND c4052_void_fl IS NULL
			   		AND c4052_ttp_link_date = v_fct_from_period;
			   		
			   		IF v_ttp_detail_cnt = 0
			   		THEN
			   			-- Create TTP Detail for the TTP.
			   			gm_pkg_oppr_ttp_finalize_setup.gm_save_ttp_details (v_ttp_detail_id, v_ttp_id, 50580, v_fct_from_period, 4, '30301');
			   		
			   		END IF;
	   		
			   		SELECT c4052_ttp_detail_id
                                INTO v_ttp_detail_id
                            FROM t4052_ttp_detail
                            WHERE c4050_ttp_id = v_ttp_id
                            AND c4052_void_fl IS NULL
                            AND c4052_ttp_link_date = v_fct_from_period;
                            
		   		END IF;
	   	
	    
          FOR someone IN (
                SELECT t4020.c4020_demand_master_id from t4020_demand_master t4020, t4015_global_sheet_mapping t4015  
                WHERE t4020.c4020_parent_demand_master_id = someparent.c4020_demand_master_id 
                AND t4020.c901_demand_type in ( 40021,40020,40022 )  
                AND t4020.c4015_global_sheet_map_id = t4015.c4015_global_sheet_map_id
				AND t4020.c4020_inactive_fl IS NULL
				AND t4020.c4020_void_fl IS NULL 
                AND t4015.c901_access_type = 102623
                AND t4020.c4020_demand_period IS NOT NULL
                --40021,40020,40022 Consignment, Sales and Inhouse
            	)
          	LOOP
         
              v_demandsheetid := someone.c4020_demand_master_id;      
              
              gm_pkg_oppr_ld_mend_demand.gm_op_ld_demand_main(v_demandsheetid, v_load_date ,v_dmd_from_period,v_dmd_to_period, 
              												  v_fct_from_period,v_fct_to_period,v_inventory_id,v_ttp_detail_id,'30301');     
              
          	END LOOP;
            COMMIT;
          	-- To Load the readonly sheet
         	gm_pkg_oppr_ld_mend_demand.gm_op_exec_job_detail(v_load_date ,v_dmd_from_period,v_dmd_to_period, 
              												  v_fct_from_period,v_fct_to_period,v_inventory_id,v_ttp_detail_id);
																  
          	gm_pkg_oppr_ld_summary.gm_op_ld_summary_main(v_ttp_detail_id,NULL,'30301');
          
          -- To cleanup staging table after rollup sheet completed    												  
		  	execute immediate 'TRUNCATE TABLE st4042_demand_sheet_detail'; 
		  	
		  	COMMIT;
		END LOOP; 
	
		gm_pkg_oppr_ld_mend_load.gm_op_multipart_sheet_load(v_load_date,v_inventory_id);
		
		--To update Original Order qty and Cost
			UPDATE t4056_ttp_mon_summary
			SET c4056_org_order_qty =c4056_order_qty,
			    c4056_org_order_cost=c4056_order_cost
			WHERE c4056_load_date   =v_load_date;
		
		--For Enable 2min job
	
		SELECT job INTO job_id
		FROM user_jobs where what='GLOBUS_DM_OPERATION.GM_PKG_OPPR_LD_DEMAND.GM_OP_EXEC_JOB_DETAIL;' ;
		
		dbms_job.broken(job_id, FALSE );
		COMMIT;
	
	END gm_op_ld_main;

	/*******************************************************
	* Purpose: 4. This procedure will load Multi Part sheets
	*******************************************************/
	PROCEDURE gm_op_multipart_sheet_load(
	  p_load_date	IN  DATE
	, p_inventory_id IN NUMBER
	)
	AS
	v_demandsheetid  VARCHAR2(20);
	v_ttp_detail_id t4052_ttp_detail.c4052_ttp_detail_id%TYPE;
	v_ttp_detail_cnt 		NUMBER;
	v_request_by			VARCHAR2(20) :='30301';
 	
	
	CURSOR cur_mp_master_insert
		IS
			  SELECT t4050.c4050_ttp_id ttp_id, t4051.c4020_demand_master_id dmd_mas_id, C906_RULE_VALUE lvl_val,comp_div.Div_id  
				FROM globus_dm_operation.t4050_ttp t4050,
				    globus_dm_operation.t4051_ttp_demand_mapping t4051,t906_rules t906,
				    globus_dm_operation.t4020_demand_master t4020,
                                        (SELECT C906_RULE_ID comp_id, C906_RULE_VALUE Div_id
                                          FROM t906_rules t906
                                        WHERE C906_RULE_GRP_ID ='GOP_MP_MEND_LOCK'
                                         AND t906.c906_void_fl IS NULL) comp_div
				WHERE t4050.c4050_ttp_id         = t4051.c4050_ttp_id
				AND t4051.c4020_demand_master_id =t4020.c4020_demand_master_id
                AND t4020.c4020_demand_master_id = t906.c906_rule_id
                AND c901_ttp_type                =50310
				AND t4020.c901_demand_type       = 40023
				AND t4050.c4050_void_fl IS NULL
				AND t4020.c4020_void_fl IS NULL
                AND C906_RULE_GRP_ID ='GOP_MPDMD_COMP_MAP'
                AND t906.c906_void_fl IS NULL
                and comp_div.comp_id =  t906.c906_rule_value;
                
                --PMT-57165 Multi Part Sheet for Arthroplasty
                --GOP_MP_MEND_LOCK This rule group used for company and division mapping
                --GOP_MPDMD_COMP_MAP This rule grup used for MP demand id and company id
                --GOP_MP_DIV_MAP This rule grup used for grouping division in one MP sheet
                --GOP_MP_SHEEET_LOCK This rule grup used for grouping company in one MP sheet

				
	BEGIN
		
		
	/* 1. PMT-57165 Added cursur,  MP entry in t4052_ttp_detail and t4040_demand_sheet
	   2. Pre-request ; For enabling new multip part sheet, to have an entry in T4020, t4050 and T4051
	   3. Have a rule entry in these group depends on the requirement
	   	   Rule Grop ids : GOP_MP_DIV_MAP, GOP_MP_MEND_LOCK, GOP_MP_SHEET_LOCK
	   
	   */
		
	-- 1. Create a new entry into t4052_ttp_detail for Multi Part Spine
			
		FOR cur_mp_mas_ins IN cur_mp_master_insert
    	LOOP
    		
   	 
	   	--Check if TTP Detail exists for the current Month for the TTP.
	   		SELECT COUNT(1)
	   		INTO v_ttp_detail_cnt
	   		FROM T4052_TTP_DETAIL
	   		WHERE C4050_TTP_ID = cur_mp_mas_ins.ttp_id
	   		AND C4052_VOID_FL IS NULL
	   		AND C4052_TTP_LINK_DATE = p_load_date;
	   		
	   		IF v_ttp_detail_cnt = 0
	   		THEN
	   			-- Create TTP Detail for the TTP.
   		
    		
	    		SELECT s4052_ttp_detail.NEXTVAL
					INTO v_ttp_detail_id
				FROM DUAL;
				
			INSERT INTO t4052_ttp_detail(c4052_ttp_detail_id,c4050_ttp_id,c4052_ttp_link_date,c901_status,c4052_created_by,c4052_created_date,c4052_forecast_period,c4052_load_date)
					     VALUES (v_ttp_detail_id,cur_mp_mas_ins.ttp_id,p_load_date,50580,v_request_by,p_load_date,4,p_load_date);
					     
			INSERT INTO t4040_demand_sheet (c4040_demand_sheet_id,c4020_demand_master_id,c4040_demand_period_dt,c4040_load_dt,c4040_demand_period,c901_status,c4040_void_fl,
			c4040_approved_by,c4040_forecast_period,c4040_approved_date,c4040_last_updated_by,c4052_ttp_detail_id,c4040_last_updated_date,c4040_primary_user,c4040_request_period,
			c901_demand_type,c250_inventory_lock_id,c901_level_id,c901_level_value,c901_access_type,c4015_global_sheet_map_id,c4016_global_inventory_map_id)
			VALUES (s4040_demand_sheet.nextval,cur_mp_mas_ins.dmd_mas_id,p_load_date,p_load_date,6,50550,null,'',12,'','',v_ttp_detail_id,'','303007',0,40023,
			p_inventory_id,null,cur_mp_mas_ins.lvl_val,null,null,null);
	
			gm_pkg_oppr_ld_multipart_ttp.gm_op_ld_missing_part(cur_mp_mas_ins.div_id,cur_mp_mas_ins.lvl_val);
			-- division_id,level value
			
			END IF;
			
			gm_pkg_oppr_ld_summary.gm_op_ld_summary_main(v_ttp_detail_id,'',v_request_by);
			COMMIT;
		
		END LOOP;	
	
		
		-- To load Multi part qty populate by TTP during monthend
		gm_pkg_oppr_ld_multipart.gm_op_multipart_qty_load_by_ttp(p_load_date,p_inventory_id);
		COMMIT;
	
	END gm_op_multipart_sheet_load;
	
END gm_pkg_oppr_ld_mend_load;
/