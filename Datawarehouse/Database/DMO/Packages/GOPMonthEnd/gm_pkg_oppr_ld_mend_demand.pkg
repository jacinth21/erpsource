create or replace PACKAGE gm_pkg_oppr_ld_mend_demand
IS
	/*******************************************************
	* Purpose: This procedure will be main procedure to
	*	to	load sheet information
	* Accepts demand id and loads demand  and forecast information
	*******************************************************/
	PROCEDURE gm_op_ld_demand_main (
		p_demand_id 	IN	t4020_demand_master.c4020_demand_master_id%TYPE
		, p_load_date	IN  DATE
		,p_dmd_from_period	IN  DATE
		,p_dmd_to_period	IN  DATE
        ,p_fct_from_period	IN  DATE
        ,p_fct_to_period	IN  DATE
        ,p_inventory_id		IN  NUMBER
        ,p_ttp_detail_id	IN 	NUMBER
	    ,p_request_by   IN	t4020_demand_master.c4020_created_by%TYPE DEFAULT '30301'
	  
	);

	/**********************************************************************
	*Purpose: Procedure used to execute load details and call "gm_op_ld_demand_main"
	* to load demand
	**********************************************************************/
	PROCEDURE gm_op_exec_job_detail(
		p_load_date	IN  DATE
		,p_dmd_from_period	IN  DATE
		,p_dmd_to_period	IN  DATE
        ,p_fct_from_period	IN  DATE
        ,p_fct_to_period	IN  DATE
        ,p_inventory_id		IN  NUMBER
        ,p_ttp_detail_id	IN 	NUMBER
        );
        
  	/*************************************************************************
	 * Purpose: Procedure used to load demand/forcast dates for selected period
	 *************************************************************************/
	PROCEDURE gm_op_calc_period (
		p_load_date			IN		DATE,
		p_demand_period    IN		t4020_demand_master.c4020_demand_period%TYPE
	  , p_inc_current_fl   IN		t4020_demand_master.c4020_include_current_fl%TYPE
	  , p_type			   IN		CHAR
	  , p_from_period	   OUT		DATE
	  , p_to_period 	   OUT		DATE
	);
        
     /***********************************************************
	* Purpose: Procedure used to save demand sheet information
	************************************************************/
     PROCEDURE gm_op_sav_sheet_main (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_demand_type		IN	 t4020_demand_master.c901_demand_type%TYPE
	  , p_demand_period 	IN	 t4020_demand_master.c4020_demand_period%TYPE
	  , p_forecast_period	IN	 t4020_demand_master.c4020_forecast_period%TYPE
	  , p_inc_current_fl	IN	 t4020_demand_master.c4020_include_current_fl%TYPE
	  , p_from_period		IN	 DATE
	  , p_primary_user		IN	 t4020_demand_master.c4020_primary_user%TYPE
	  , p_request_period	IN	 t4020_demand_master.c4020_request_period%TYPE
	  , p_inventory_id		IN	 t250_inventory_lock.c250_inventory_lock_id%TYPE
      , p_level_id          IN   t4040_demand_sheet.c901_level_id%TYPE                  
      , p_level_value       IN	 t4040_demand_sheet.c901_level_value%TYPE               
      , p_access_type       IN	 t4040_demand_sheet.c901_access_type%TYPE               
      , p_global_sheet_map_id       IN	t4040_demand_sheet.c4015_global_sheet_map_id%TYPE      
      , p_global_inventory_map_id   IN	t4040_demand_sheet.c4016_global_inventory_map_id%TYPE
      , p_request_by   IN	t4020_demand_master.c4020_created_by%type
      , p_ttp_detail_id IN 		T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE
      , p_load_date		IN		DATE
      
	);
    
     /*******************************************************
   * Description : Procedure to save demand sheet detail
   *        from temp table to main table  
   * Author 	 : Richard
   *******************************************************/
	--
	PROCEDURE gm_ld_demand_sheet_tmp_to_main;

END gm_pkg_oppr_ld_mend_demand;
/