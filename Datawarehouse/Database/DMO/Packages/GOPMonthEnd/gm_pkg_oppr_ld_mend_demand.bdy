/* Formatted on 2010/03/01 11:31 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\purchasing\gm_pkg_oppr_ld_mend_demand.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ld_mend_demand
IS
	/*******************************************************
	* Purpose: This procedure will be main procedure to
	*			load sheet information
	* Accepts demand id and loads demand  and forecast information
	*******************************************************/
	PROCEDURE gm_op_ld_demand_main (
		p_demand_id 	IN	t4020_demand_master.c4020_demand_master_id%TYPE
		, p_load_date	IN  DATE
		,p_dmd_from_period	IN  DATE
		,p_dmd_to_period	IN  DATE
        ,p_fct_from_period	IN  DATE
        ,p_fct_to_period	IN  DATE
        ,p_inventory_id		IN  NUMBER
        ,p_ttp_detail_id	IN 	NUMBER
	    ,p_request_by   	IN	t4020_demand_master.c4020_created_by%TYPE DEFAULT '30301'
	  
	)
	AS
		--
		v_demand_type       t4020_demand_master.c901_demand_type%TYPE;
		v_demand_period     t4020_demand_master.c4020_demand_period%TYPE;
		v_forecast_period   t4020_demand_master.c4020_forecast_period%TYPE;
		v_inc_current_fl    t4020_demand_master.c4020_include_current_fl%TYPE;
		v_demand_sheet_id   t4040_demand_sheet.c4040_demand_sheet_id%TYPE;
		v_demand_nm         t4020_demand_master.c4020_demand_nm%TYPE;
		v_primary_user      t4020_demand_master.c4020_primary_user%TYPE;
		v_request_period    t4020_demand_master.c4020_request_period%TYPE;
		v_inventory_id      t250_inventory_lock.c250_inventory_lock_id%TYPE;
        -- 
        -- Global mapping parameter 
        v_level_id                  t4040_demand_sheet.c901_level_id%TYPE;                  
        v_level_value               t4040_demand_sheet.c901_level_value%TYPE;               
        v_access_type               t4040_demand_sheet.c901_access_type%TYPE;               
        v_global_sheet_map_id       t4040_demand_sheet.c4015_global_sheet_map_id%TYPE;      
        v_global_inventory_map_id   t4040_demand_sheet.c4016_global_inventory_map_id%TYPE;
        --
        v_parent_demand_id          t4020_demand_master.c4020_parent_demand_master_id%TYPE;
        v_company_id                t4020_demand_master.c901_company_id%type;        
		--
		--v_start_dt	   DATE;
		
		
		v_exclude_dmd_mas_fl	t4020_demand_master.c4020_exclude_ld_fl%TYPE;
		v_ttp_id 				T4051_TTP_DEMAND_MAPPING.C4050_TTP_ID%TYPE;
		
		v_ttp_detail_id 		T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE;
		v_ttp_detail_cnt 		NUMBER;
		v_ttp_type   	   		t4050_ttp.c901_ttp_type%TYPE;	
		v_ld_start_dt	   NUMBER;
		v_ld_end_dt	   NUMBER;
		--
	--
	BEGIN

		--
		SELECT	   t4020.c901_demand_type, t4020.c4020_demand_nm, t4020.c4020_demand_period, t4020.c4020_forecast_period
				 , t4020.c4020_include_current_fl, t4020.c4020_primary_user, t4020.c4020_request_period
                 , t4015.c901_level_id , t4015.c901_level_value, t4015.c901_access_type 
                 , t4015.c4015_global_sheet_map_id, t4015.c4016_global_inventory_map_id
                 , t4020.c4020_parent_demand_master_id , t4020.c901_company_id, t4020.c4020_exclude_ld_fl
			  INTO v_demand_type, v_demand_nm, v_demand_period, v_forecast_period
				 , v_inc_current_fl, v_primary_user, v_request_period
                 , v_level_id, v_level_value, v_access_type 
                 , v_global_sheet_map_id, v_global_inventory_map_id
                 , v_parent_demand_id , v_company_id,v_exclude_dmd_mas_fl
			  FROM t4020_demand_master t4020 , t4015_global_sheet_mapping t4015
			 WHERE t4020.c4020_demand_master_id = p_demand_id
               AND t4020.c4015_global_sheet_map_id = t4015.c4015_global_sheet_map_id 
		FOR UPDATE;

		--
		SELECT s4040_demand_sheet.NEXTVAL
		  INTO v_demand_sheet_id
		  FROM DUAL;


		-- Below procedure will load demand sheet information
		gm_pkg_oppr_ld_mend_demand.gm_op_sav_sheet_main (p_demand_id
											, v_demand_sheet_id, v_demand_type, v_demand_period
											, v_forecast_period, v_inc_current_fl, p_fct_from_period
											, v_primary_user, v_request_period, p_inventory_id
                                            , v_level_id, v_level_value, v_access_type 
                                            , v_global_sheet_map_id, v_global_inventory_map_id,
                                            p_request_by,
                                            p_ttp_detail_id,p_load_date
											 );
		
		SELECT DECODE(p_request_by,'30301',v_exclude_dmd_mas_fl,'') INTO v_exclude_dmd_mas_fl FROM DUAL;
		
		UPDATE t4020_demand_master
		SET    c4020_exclude_ld_fl= v_exclude_dmd_mas_fl ,C4020_LAST_UPDATED_DATE=SYSDATE
		WHERE  c4020_demand_master_id = p_demand_id;
			
		-- When Exclude Flag is available, the demand Sheet should not be loaded.										
		IF (NVL(v_exclude_dmd_mas_fl,'N') = 'Y')
		THEN
/*			gm_pkg_oppr_ld_mend_demand.gm_op_send_email (p_demand_id
										, v_demand_nm
										, '01/' || TO_CHAR (p_load_date, 'MM/YYYY')
										, p_load_date
										, 'S'
										, 'SHEET LOAD EXCLUDED'
										, p_request_by
										 ); */
			COMMIT;
			
			return;
			
		END IF;

			
		--
		-- Below code to load demand mapping information
        --DBMS_OUTPUT.put_line ('Loading Sheet Mapping  ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
		gm_pkg_oppr_ld_demand.gm_op_sav_sheet_mapping (p_demand_id, v_demand_sheet_id);
        
		-- Below code to save growth information
        --DBMS_OUTPUT.put_line ('Loading Growth Mapping   ' || v_access_type || ' -- ' || v_demand_type || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS') );
		gm_pkg_oppr_ld_demand.gm_op_sav_sheet_growth (p_demand_id, v_demand_sheet_id, p_fct_from_period, p_fct_to_period);

		--
		-- based on the type will redirect to sales or consignment
		
		-- 102622 Represent Read only rollup sheet 
        IF (v_access_type =  102622) 
        THEN
			--
		--	v_ld_start_dt	:= dbms_utility.get_time(); This code for execution time calculation
			gm_pkg_oppr_ld_mend_rollup_demand.gm_op_ld_main (p_demand_id
												   , v_demand_sheet_id
												   , v_demand_period
												   , v_forecast_period
												   , v_inc_current_fl
                                                   , p_inventory_id 
                                                   , p_load_date
													);
	  -- v_ld_end_dt	:= dbms_utility.get_time()-v_ld_start_dt;	
     --  DBMS_OUTPUT.put_line ('Rollup  Seconds' || v_access_type || ' -- ' || v_demand_type || '--'||p_demand_id||'--'||v_ld_end_dt/100 );
       
        -- 40020 maps to sales consignment
		ELSIF (v_demand_type = 40020)
		THEN
			--
		--	v_ld_start_dt	:= dbms_utility.get_time();
			gm_pkg_oppr_ld_mend_sales_demand.gm_op_ld_main (p_demand_id
												   , v_demand_sheet_id
												   , v_demand_period
												   , v_forecast_period
												   , v_inc_current_fl
												   , p_dmd_from_period
												   , p_dmd_to_period
												   , p_fct_from_period
												   , p_fct_to_period
													);
       -- v_ld_end_dt	:= dbms_utility.get_time()-v_ld_start_dt;
		--DBMS_OUTPUT.put_line ('Sales Seconds' || v_access_type || ' -- ' || v_demand_type || '--'||p_demand_id||'--'||v_ld_end_dt/100 );
		-- 40021 maps to inhouse consignment
		ELSE
			-- Field and Inhouse consignment
			v_ld_start_dt	:= dbms_utility.get_time();
			gm_pkg_oppr_ld_mend_consign_demand.gm_op_ld_main (p_demand_id
													 , v_demand_sheet_id
													 , v_demand_type
													 , v_demand_period
													 , v_forecast_period
													 , v_inc_current_fl
													 , p_inventory_id													 
												     , p_dmd_from_period
												     , p_dmd_to_period
												     , p_fct_from_period
												     , p_fct_to_period
													  );
		--v_ld_end_dt	:= dbms_utility.get_time()-v_ld_start_dt;
		--DBMS_OUTPUT.put_line ('Consignment Seconds ' || v_access_type || ' -- ' || v_demand_type || '--'||p_demand_id||'--'||v_ld_end_dt/100 );
		END IF;
    
        -- if its a editable sheet the load all read only sheet 
        IF (v_access_type =  102623) 
        THEN   
            -- Load associated read only sheet information  
            gm_pkg_oppr_ld_demand.gm_op_sav_rollup_sheet_info (v_parent_demand_id 
                            , v_demand_type, v_company_id , v_level_id, v_level_value,p_request_by);
        END IF;                                         
        

        -- Copy the information from temp to main table and staging table
        gm_pkg_oppr_ld_mend_demand.gm_ld_demand_sheet_tmp_to_main;
              
		COMMIT;

		EXCEPTION
	    	WHEN NO_DATA_FOUND
			THEN
				ROLLBACK;
	            DBMS_OUTPUT.put_line ('******** Error please check **** ' || SQLERRM);

				COMMIT;
	
			WHEN OTHERS
			THEN
				ROLLBACK;
	            DBMS_OUTPUT.put_line ('******** Error please check **** ' || SQLERRM);

	            COMMIT;

	END gm_op_ld_demand_main;


	/**********************************************************************
	*Purpose: Procedure used to execute load details and call "gm_op_ld_demand_main"
	* to load demand
	**********************************************************************/
	PROCEDURE gm_op_exec_job_detail(
		 p_load_date	IN  DATE
		,p_dmd_from_period	IN  DATE
		,p_dmd_to_period	IN  DATE
        ,p_fct_from_period	IN  DATE
        ,p_fct_to_period	IN  DATE
        ,p_inventory_id		IN  NUMBER
        ,p_ttp_detail_id	IN 	NUMBER
        )
	AS
		
        v_ref_id	   t9302_execute_load.c9302_ref_id%TYPE;
		v_execute_load_id t9302_execute_load.c9302_execute_load_id%TYPE;
		v_request_by   t9302_execute_load.c9302_request_by%TYPE;
		v_ref_type	   t9302_execute_load.c901_ref_type%TYPE;
        --
        -- Cursor to load job in Que
        CURSOR cur_exec_job_que
        IS 
            SELECT c901_code_id
              FROM t901_code_lookup t901 
             WHERE t901.c901_code_grp  = 'JBSTU'
               AND t901.c901_active_fl = 1
          ORDER BY t901.c901_code_seq_no ;
         
        
		CURSOR cur_exec_job_detail(v_type t9302_execute_load.c901_ref_type%TYPE)
		IS
			  SELECT c9302_execute_load_id, c9302_ref_id, c901_ref_type, c9302_request_by
				FROM t9302_execute_load t9302
			   WHERE t9302.c9302_status = 10
                 AND t9302.c901_ref_type = v_type   
			ORDER BY c9302_execute_load_id;	
	BEGIN
		
		DELETE from MY_TEMP_KEY_VALUE;
        FOR var_job_detail IN cur_exec_job_que
        LOOP
        
    		FOR var_exec_job_detail IN cur_exec_job_detail(var_job_detail.c901_code_id)    
    		LOOP
    			BEGIN
	    			
	    			
    				v_execute_load_id := var_exec_job_detail.c9302_execute_load_id;
    				v_ref_id	:= var_exec_job_detail.c9302_ref_id;
    				v_request_by := var_exec_job_detail.c9302_request_by;
    				v_ref_type := var_exec_job_detail.c901_ref_type;
					
    				
    				UPDATE t9302_execute_load t9302
    				   SET t9302.c9302_status = 20
    					 , t9302.c9302_job_start_time = SYSDATE
    				 WHERE t9302.c9302_execute_load_id = v_execute_load_id;

    					gm_pkg_oppr_ld_mend_demand.gm_op_ld_demand_main (v_ref_id,p_load_date,p_dmd_from_period,p_dmd_to_period
    																	,p_fct_from_period,p_fct_to_period,p_inventory_id,p_ttp_detail_id,v_request_by);

    				UPDATE t9302_execute_load t9302
    				   SET t9302.c9302_status = 40
    					 , t9302.c9302_job_end_time = SYSDATE
    				 WHERE t9302.c9302_execute_load_id = v_execute_load_id;
					
    				--For the Sheet that is getting reloaded ,we need to reload the Summary.
	    			INSERT INTO MY_TEMP_KEY_VALUE VALUES(v_ref_id,v_request_by,v_ref_type);
	    			
	    			
    				COMMIT;
    			EXCEPTION
    				WHEN OTHERS
    				THEN
    					UPDATE t9302_execute_load t9302
    					   SET t9302.c9302_status = 30
    					 WHERE t9302.c9302_execute_load_id = v_execute_load_id;

    					COMMIT;
    			END;
    		END LOOP;
            --
        END LOOP;   
        
      	gm_pkg_oppr_ld_demand.gm_op_ld_ttp_summary();
      	
        COMMIT;
	END gm_op_exec_job_detail;
	
	/*************************************************************************
		* Purpose: Procedure used to load sales demand/forecast for selected period
		* p_type : D - Demand F - Forecast
		*************************************************************************/
	PROCEDURE gm_op_calc_period (
		p_load_date			IN		DATE,
		p_demand_period    IN		t4020_demand_master.c4020_demand_period%TYPE
	  , p_inc_current_fl   IN		t4020_demand_master.c4020_include_current_fl%TYPE
	  , p_type			   IN		CHAR
	  , p_from_period	   OUT		DATE
	  , p_to_period 	   OUT		DATE
	)
	AS
--
	BEGIN
		--
		IF (p_type = 'D')
		THEN
			SELECT TRUNC (LAST_DAY (DECODE (p_inc_current_fl, NULL, ADD_MONTHS (p_load_date, -1), p_load_date)))
			  INTO p_to_period
			  FROM DUAL;

			--
			SELECT TRUNC (ADD_MONTHS (p_to_period, (p_demand_period * -1))) + 1
			  INTO p_from_period
			  FROM DUAL;
		ELSE
			SELECT TO_DATE (   '01/'
							|| TO_CHAR ((DECODE (p_inc_current_fl, NULL, p_load_date, ADD_MONTHS (p_load_date, +1))), 'MM/YYYY')
						  , 'DD/MM/YYYY'
						   )
			  INTO p_from_period
			  FROM DUAL;

			--
			SELECT LAST_DAY ((ADD_MONTHS (p_from_period, (p_demand_period - 1))))
			  INTO p_to_period
			  FROM DUAL;
		END IF;

		--
		--DBMS_OUTPUT.put_line ('From and To Date  ****' || p_from_period || ' - ' || p_to_period || ' - '
		--					  || p_demand_period
		--					 );
	END gm_op_calc_period;
	
	/***********************************************************
	* Purpose: Procedure used to save demand sheet information
	************************************************************/
	PROCEDURE gm_op_sav_sheet_main (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_demand_type		IN	 t4020_demand_master.c901_demand_type%TYPE
	  , p_demand_period 	IN	 t4020_demand_master.c4020_demand_period%TYPE
	  , p_forecast_period	IN	 t4020_demand_master.c4020_forecast_period%TYPE
	  , p_inc_current_fl	IN	 t4020_demand_master.c4020_include_current_fl%TYPE
	  , p_from_period		IN	 DATE
	  , p_primary_user		IN	 t4020_demand_master.c4020_primary_user%TYPE
	  , p_request_period	IN	 t4020_demand_master.c4020_request_period%TYPE
	  , p_inventory_id		IN	 t250_inventory_lock.c250_inventory_lock_id%TYPE
      , p_level_id          IN   t4040_demand_sheet.c901_level_id%TYPE                  
      , p_level_value       IN	 t4040_demand_sheet.c901_level_value%TYPE               
      , p_access_type       IN	 t4040_demand_sheet.c901_access_type%TYPE               
      , p_global_sheet_map_id       IN	t4040_demand_sheet.c4015_global_sheet_map_id%TYPE      
      , p_global_inventory_map_id   IN	t4040_demand_sheet.c4016_global_inventory_map_id%TYPE
      , p_request_by   IN	t4020_demand_master.c4020_created_by%type
      , p_ttp_detail_id IN 		T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE
      , p_load_date		IN		DATE
      
	)
	AS
		--
		v_count 	   NUMBER;
		v_status	   t4040_demand_sheet.c901_status%TYPE;
	BEGIN
		--
		BEGIN
			--
			-- Check if record already found for selected period if yes delete the same
			SELECT c901_status
			  INTO v_status
			  FROM t4040_demand_sheet
			 WHERE c4020_demand_master_id = p_demand_id AND c4040_demand_period_dt = p_from_period AND ROWNUM = 1;

			IF (v_status <> 50550)
			THEN
				-- Error message is  Set Consignment is not in Shipped Status and cannot be rolled back.
					raise_application_error (-20016, 'DEMAND SHEET NOT IN OPEN STATE');
			ELSE
		-- PMT: 14582 : GOP DB SERVER MIGRATION. Excluding the below delete query for the monthly load. 
			 IF (P_REQUEST_BY <> '30301')
			 	THEN
				DELETE FROM t4042_demand_sheet_detail
					  WHERE c4040_demand_sheet_id IN (
								  SELECT c4040_demand_sheet_id
									FROM t4040_demand_sheet
								   WHERE c4020_demand_master_id = p_demand_id
										 AND c4040_demand_period_dt = p_from_period);

				DELETE FROM t4041_demand_sheet_mapping
					  WHERE c4040_demand_sheet_id IN (
								  SELECT c4040_demand_sheet_id
									FROM t4040_demand_sheet
								   WHERE c4020_demand_master_id = p_demand_id
										 AND c4040_demand_period_dt = p_from_period);

				DELETE FROM t4043_demand_sheet_growth
					  WHERE c4040_demand_sheet_id IN (
								  SELECT c4040_demand_sheet_id
									FROM t4040_demand_sheet
								   WHERE c4020_demand_master_id = p_demand_id
										 AND c4040_demand_period_dt = p_from_period);

				DELETE FROM t4044_demand_sheet_request
					  WHERE c4040_demand_sheet_id IN (
								  SELECT c4040_demand_sheet_id
									FROM t4040_demand_sheet
								   WHERE c4020_demand_master_id = p_demand_id
										 AND c4040_demand_period_dt = p_from_period);
				
                DELETE FROM t4046_demand_sheet_assoc_map 
					  WHERE c4040_demand_sheet_id IN (
								  SELECT c4040_demand_sheet_id
									FROM t4040_demand_sheet
								   WHERE c4020_demand_master_id = p_demand_id
										 AND c4040_demand_period_dt = p_from_period);


				DELETE FROM t4040_demand_sheet
					  WHERE c4020_demand_master_id = p_demand_id AND c4040_demand_period_dt = p_from_period;
		 END IF;
                
                -- Table used to load temporary information and finally will be moved to 
                -- table t4042_demand_sheet_detail
                execute immediate 'TRUNCATE TABLE my_temp_demand_sheet_detail';
                
			END IF;
		--
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				NULL;
		END;

		INSERT INTO t4040_demand_sheet
					(c4040_demand_sheet_id, c4020_demand_master_id, c4040_load_dt, c4040_demand_period
				   , c4040_forecast_period, c901_status, c4040_demand_period_dt, c4040_primary_user, c901_demand_type
				   , c4040_request_period, c250_inventory_lock_id, c901_level_id , c901_level_value, c901_access_type 
                   , c4015_global_sheet_map_id, c4016_global_inventory_map_id  ,C4052_TTP_DETAIL_ID
					)
			 VALUES (p_demand_sheet_id, p_demand_id, p_load_date, p_demand_period
				   , p_forecast_period, 50550, p_from_period, p_primary_user, p_demand_type
				   , p_request_period, p_inventory_id,  p_level_id, p_level_value, p_access_type 
                   , p_global_sheet_map_id, p_global_inventory_map_id,p_ttp_detail_id
					);
	END gm_op_sav_sheet_main;
	
  /*******************************************************
   * Description : Procedure to save demand sheet detail
   *        from temp table to main table  
   *******************************************************/
	--
PROCEDURE gm_ld_demand_sheet_tmp_to_main 
    AS
    BEGIN 
		INSERT INTO T4042_DEMAND_SHEET_DETAIL
					(c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id, c901_type, c4042_period
				   , c205_part_number_id, c4042_qty, c4042_updated_by, c4042_updated_date, c205_parent_part_num_id
				   , c4042_parent_ref_id, c4042_parent_qty_needed, c4042_history_fl, c4042_comments_fl
                   , c4042_weighted_avg_diff_qty, c901_ref_type
					)
			 SELECT c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id, c901_type, c4042_period
				   , c205_part_number_id, c4042_qty, c4042_updated_by, c4042_updated_date, c205_parent_part_num_id
				   , c4042_parent_ref_id, c4042_parent_qty_needed, c4042_history_fl, c4042_comments_fl
                   , c4042_weighted_avg_diff_qty, c901_ref_type 
               FROM  my_temp_demand_sheet_detail;    
         
        -- This stagint able used for rollup read instead T4042(has 55 million rows)
        INSERT INTO ST4042_DEMAND_SHEET_DETAIL
					(c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id, c901_type, c4042_period
				   , c205_part_number_id, c4042_qty, c4042_updated_by, c4042_updated_date, c205_parent_part_num_id
				   , c4042_parent_ref_id, c4042_parent_qty_needed, c4042_history_fl, c4042_comments_fl
                   , c4042_weighted_avg_diff_qty, c901_ref_type
					)
			 SELECT c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id, c901_type, c4042_period
				   , c205_part_number_id, c4042_qty, c4042_updated_by, c4042_updated_date, c205_parent_part_num_id
				   , c4042_parent_ref_id, c4042_parent_qty_needed, c4042_history_fl, c4042_comments_fl
                   , c4042_weighted_avg_diff_qty, c901_ref_type 
               FROM  my_temp_demand_sheet_detail;
    END gm_ld_demand_sheet_tmp_to_main;
        
END gm_pkg_oppr_ld_mend_demand;
/
