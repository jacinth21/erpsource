create or replace procedure gm_ld_mend_gop_monthly_load

AS

v_demandsheetid  VARCHAR2(20);
v_month VARCHAR2(2);
v_year VARCHAR2(4);
job_id number;
X NUMBER;
v_ttp_detail_id t4052_ttp_detail.c4052_ttp_detail_id%TYPE;

v_load_date  DATE;
v_inventory_id  t250_inventory_lock.c250_inventory_lock_id%TYPE;

v_fct_from_period DATE;
v_fct_to_period DATE;
v_dmd_from_period DATE;
v_dmd_to_period DATE;
 
BEGIN
	
	--For disabling 2min job
	
	SELECT job INTO job_id
	FROM user_jobs where what='GLOBUS_DM_OPERATION.GM_PKG_OPPR_LD_DEMAND.GM_OP_EXEC_JOB_DETAIL;' ;
	
	dbms_job.broken(job_id, TRUE );
	commit;

	gm_pkg_oppr_ld_mend_inventory.gm_op_fch_load_date(v_load_date);
	
	gm_pkg_oppr_ld_mend_demand.gm_op_calc_period (v_load_date,6, NULL, 'D', v_dmd_from_period, v_dmd_to_period);
	
	gm_pkg_oppr_ld_mend_demand.gm_op_calc_period (v_load_date,12, NULL, 'F', v_fct_from_period, v_fct_to_period);
	
	SELECT 	to_char(v_load_date,'MM'),to_char(v_load_date,'YYYY')
	INTO 	v_month, v_year 
	FROM 	DUAL;


	
	gm_pkg_oppr_ld_mend_inventory.gm_op_ld_demand_main (20430,v_load_date);
	COMMIT;



    FOR someparent IN (
        SELECT x.c4020_demand_master_id, x.c4020_demand_nm FROM t4020_demand_master x
        WHERE x.c4020_parent_demand_master_id IS NULL  
        and x.c4020_inactive_fl IS NULL
        AND x.c4020_demand_master_id =10147
	ORDER BY UPPER(x.c4020_demand_nm) )
    LOOP
          FOR someone IN (
                select x.c4020_demand_master_id from t4020_demand_master x, t4015_global_sheet_mapping y  
                where x.c4020_parent_demand_master_id = someparent.c4020_demand_master_id 
                and x.c901_demand_type in ( 40021,40020,40022 )  
                and x.c4015_global_sheet_map_id = y.c4015_global_sheet_map_id
				and x.c4020_inactive_fl is null
				and x.c4020_void_fl is null 
                and y.c901_access_type = 102623
                and x.c4020_demand_period is not null
            )
          LOOP
            --DBMS_OUTPUT.PUT_LINE('DEMAND SHEET ID ' || someone.C4020_DEMAND_MASTER_ID ||
             --                    ', Last name = ' || someone.C4020_DEMAND_NM);
              v_demandsheetid := someone.c4020_demand_master_id;      
              
              gm_pkg_oppr_ld_mend_demand.gm_op_ld_demand_main(v_demandsheetid, v_load_date ,v_dmd_from_period,v_dmd_to_period, 
              												  v_fct_from_period,v_fct_to_period,
              												 '30301');     
              
          END LOOP;
          -- To Load the readonly sheet
          gm_pkg_oppr_ld_mend_demand.GM_OP_EXEC_JOB_DETAIL (v_load_date ,v_dmd_from_period,v_dmd_to_period, 
              												  v_fct_from_period,v_fct_to_period);
          gm_pkg_oppr_ld_summary.gm_op_ld_summary_main('',v_demandsheetid,'30301');
    END LOOP;      


	-- 1. Create a new entry into t4052_ttp_detail for Multi Part Spine
			
    		SELECT s4052_ttp_detail.NEXTVAL
			  INTO v_ttp_detail_id
			FROM DUAL;
			
	INSERT INTO t4052_ttp_detail(c4052_ttp_detail_id,c4050_ttp_id,c4052_ttp_link_date,c901_status,c4052_created_by,c4052_created_date,c4052_forecast_period,c4052_load_date)
				     VALUES (v_ttp_detail_id,'1160',to_date(v_month||'/01/'||v_year,'MM/DD/YYYY'),50580,'839135',v_load_date,4,TRUNC(v_load_date,'month'));
				     
		INSERT INTO t4040_demand_sheet (c4040_demand_sheet_id,c4020_demand_master_id,c4040_demand_period_dt,c4040_load_dt,c4040_demand_period,c901_status,c4040_void_fl,c4040_approved_by,c4040_forecast_period,
		c4040_approved_date,c4040_last_updated_by,c4052_ttp_detail_id,c4040_last_updated_date,c4040_primary_user,c4040_request_period,c901_demand_type,c250_inventory_lock_id,c901_level_id,
		c901_level_value,
		c901_access_type,c4015_global_sheet_map_id,c4016_global_inventory_map_id)
		VALUES (s4040_demand_sheet.nextval,'22933',to_date(v_month||'/01/'||v_year,'MM/DD/YYYY'),v_load_date,6,50550,null,'',12,'','',v_ttp_detail_id,'','303007',0,40023,(
		SELECT c250_inventory_lock_id FROM t250_inventory_lock WHERE c250_void_fl IS NULL AND c901_lock_type=20430 and c4016_global_inventory_map_id=1 and c250_lock_date=to_date(v_month||'/01/'||v_year,'MM/DD/YYYY')
		),null,null,null,null,null);

	gm_pkg_oppr_ld_multipart_ttp.gm_op_ld_missing_part();
	gm_pkg_oppr_ld_summary.gm_op_ld_summary_main(v_ttp_detail_id,'','30301');
	-- 1. Create a new entry into t4052_ttp_detail for Multi Part Trauma

	SELECT s4052_ttp_detail.NEXTVAL
				  INTO v_ttp_detail_id
				FROM DUAL;	
				
	INSERT INTO t4052_ttp_detail(c4052_ttp_detail_id,c4050_ttp_id,c4052_ttp_link_date,c901_status,c4052_created_by,c4052_created_date,c4052_forecast_period,c4052_load_date)
				     VALUES (v_ttp_detail_id,'1259',to_date(v_month||'/01/'||v_year,'MM/DD/YYYY'),50580,'829135',v_load_date,4,TRUNC(v_load_date,'month'));	 	
				 		
				     
		INSERT INTO globus_dm_operation.t4040_demand_sheet (c4040_demand_sheet_id,c4020_demand_master_id,c4040_demand_period_dt,c4040_load_dt,c4040_demand_period,c901_status,c4040_void_fl,c4040_approved_by
		,c4040_forecast_period,
		c4040_approved_date,c4040_last_updated_by,c4052_ttp_detail_id,c4040_last_updated_date,c4040_primary_user,c4040_request_period,c901_demand_type,c250_inventory_lock_id,c901_level_id,
		c901_level_value,c901_access_type,c4015_global_sheet_map_id,c4016_global_inventory_map_id)
		VALUES (s4040_demand_sheet.nextval,'33694',to_date(v_month||'/01/'||v_year,'MM/DD/YYYY'),v_load_date,6,50550,null,'',12,'','',v_ttp_detail_id,'','839135',0,40023,(
		SELECT c250_inventory_lock_id FROM t250_inventory_lock WHERE c250_void_fl IS NULL AND c901_lock_type=20430 and c4016_global_inventory_map_id=1 and c250_lock_date=to_date(v_month||'/01/'||v_year,'MM/DD/YYYY')
		),null,'26240179',null,null,null);


	gm_pkg_oppr_ld_multipart_ttp.gm_op_ld_missing_part_trauma();
	gm_pkg_oppr_ld_summary.gm_op_ld_summary_main(v_ttp_detail_id,'','30301');
	commit;

	--For Enable 2min job
	
	SELECT job INTO job_id
	FROM user_jobs where what='GLOBUS_DM_OPERATION.GM_PKG_OPPR_LD_DEMAND.GM_OP_EXEC_JOB_DETAIL;' ;
	
	dbms_job.broken(job_id, FALSE );
	commit;
	
END gm_ld_mend_gop_monthly_load;
/