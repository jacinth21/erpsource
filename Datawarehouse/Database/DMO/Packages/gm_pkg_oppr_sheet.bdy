/* Formatted on 2010/05/26 17:50 (Formatter Plus v4.8.0) */

--@"C:\Database\Packages\Operations\purchasing\gm_pkg_oppr_sheet.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_sheet
IS
--
	PROCEDURE gm_fc_sav_demsheet (
		p_demandsheetid 	 IN 	  t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demandsheetname	 IN 	  t4020_demand_master.c4020_demand_nm%TYPE
	  , p_demandperiod		 IN 	  t4020_demand_master.c4020_demand_period%TYPE
	  , p_forecastperiod	 IN 	  t4020_demand_master.c4020_forecast_period%TYPE
	  , p_hierarchyid		 IN 	  t9350_hierarchy.c9350_hierarchy_id%TYPE
	  , p_includecurrfl 	 IN 	  t4020_demand_master.c4020_include_current_fl%TYPE
	  , p_inactiveflag		 IN 	  t4020_demand_master.c4020_inactive_fl%TYPE
	  , p_userid			 IN 	  t4020_demand_master.c4020_created_by%TYPE
	  , p_demandsheetowner	 IN 	  t4020_demand_master.c4020_primary_user%TYPE
	  , p_demandtype 		 IN 	  t4020_demand_master.c901_demand_type%TYPE
	  , p_requestperiod 	 IN 	  t4020_demand_master.c4020_request_period%TYPE
	  , p_parentmasterid  	 IN		  t4020_demand_master.c4020_parent_demand_master_id%TYPE
	  , p_globalmapid 		 IN 	  t4020_demand_master.c4015_global_sheet_map_id%TYPE
	  , p_company_id 		 IN 	  t4020_demand_master.c901_company_id%TYPE
	  , p_comments	         IN       t902_log.c902_comments%TYPE
	  , p_outdemsheetid 	 OUT	  t4020_demand_master.c4020_demand_master_id%TYPE
	)
	AS
				/* To be removed later.. keeping this as a reference for now - Joe
			demExp Exception;
			code number;
			msg varchar2(512);
			pragma exception_init (demExp, -00001);
			*/
			--
		/*******************************************************
			* Description : Procedure to save demand sheet
			* Author			: Joe P Kumar
			*******************************************************/
		v_cur_owner    VARCHAR2 (50) := NULL;
		v_old_inactive_fl	t4020_demand_master.c4020_inactive_fl%TYPE;
	    v_msg  VARCHAR2(100);
	BEGIN
		p_outdemsheetid := p_demandsheetid;

		--update the pricning update group for new primary user change/add
		IF p_demandsheetid IS NOT NULL
		THEN
			SELECT c4020_primary_user, c4020_inactive_fl
			  INTO v_cur_owner, v_old_inactive_fl
			  FROM t4020_demand_master
			 WHERE c4020_demand_master_id = p_outdemsheetid;
		END IF;

		--gm_pkg_pd_group_pricing.gm_update_demand_sheet_owner (v_cur_owner, p_demandsheetowner);

		UPDATE t4020_demand_master
		   SET c4020_demand_nm = NVL(TRIM (p_demandsheetname),c4020_demand_nm)
			 , c4020_demand_period = NVL(p_demandperiod,c4020_demand_period)
			 , c4020_forecast_period = NVL(p_forecastperiod,c4020_forecast_period)
			 , c4020_include_current_fl = NVL(p_includecurrfl,c4020_include_current_fl)
			 , c4020_inactive_fl = p_inactiveflag
			 , c901_demand_type  = NVL(p_demandtype,c901_demand_type)
			 , c4020_request_period  = NVL(p_requestperiod,c4020_request_period)
			 , c4020_parent_demand_master_id  = NVL(p_parentmasterid,c4020_parent_demand_master_id)
			 , c4015_global_sheet_map_id  = NVL(p_globalmapid,c4015_global_sheet_map_id)
			 , c901_company_id = NVL(p_company_id, c901_company_id)
			 , c4020_last_updated_by = p_userid
			 , c4020_last_updated_date = SYSDATE
			 , c4020_primary_user = p_demandsheetowner
		 WHERE c4020_demand_master_id = p_outdemsheetid;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s4020_demand_master.NEXTVAL
			  INTO p_outdemsheetid
			  FROM DUAL;

			INSERT INTO t4020_demand_master
						(c4020_demand_master_id, c4020_demand_nm, c4020_demand_period, c4020_forecast_period
					   , c4020_include_current_fl, c4020_inactive_fl, c4020_created_by, c4020_created_date
					   , c4020_primary_user, c901_demand_type, c4020_request_period
					   , c4020_parent_demand_master_id, c4015_global_sheet_map_id, c901_company_id
						)
				 VALUES (p_outdemsheetid, TRIM (p_demandsheetname), p_demandperiod, p_forecastperiod
					   , p_includecurrfl, p_inactiveflag, p_userid, SYSDATE
					   , p_demandsheetowner, p_demandtype, p_requestperiod
					   , p_parentmasterid, p_globalmapid, p_company_id
						);
		END IF;

		gm_pkg_cm_hierarchy.gm_cm_sav_hierarchyflow (p_hierarchyid, 60260, p_outdemsheetid, p_userid);

		IF NVL(v_old_inactive_fl,'N') <> NVL(p_inactiveflag,'N') THEN
			gm_pkg_oppr_sheet.gm_sav_inactivate_demsheet(p_outdemsheetid,p_inactiveflag,p_userid,p_comments);
		ELSE
			--Create the comments for the demand sheet
			IF p_comments IS NOT NULL 
			THEN
				gm_update_log(p_outdemsheetid, p_comments,'1227', p_userid,v_msg);-- 1227 - DemandSheet Log type				
			END IF;
		END IF;
		
	EXCEPTION
		/*To be removed later.. keeping this as a reference for now - Joe
		WHEN demExp THEN
		code := sqlcode;
		msg := sqlerrm;
		gm_procedure_log('sqlcode with save dem',code);
		gm_procedure_log('msg with save dem',msg); */
		WHEN DUP_VAL_ON_INDEX
		THEN
			-- msg := SQLERRM(-00001);
			raise_application_error (-20047, '');	-- 20047 - demand sheet name already present.
	END gm_fc_sav_demsheet;

--
	PROCEDURE gm_fc_fch_demandmap (
		p_demandsheetid    IN		t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demandsheetype   IN		t4020_demand_master.c901_demand_type%TYPE
	  , p_reftype		   IN		t4021_demand_mapping.c901_ref_type%TYPE
	  , p_outunselected    OUT		TYPES.cursor_type
	  , p_outselected	   OUT		TYPES.cursor_type
	  , p_request_period   OUT		t4020_demand_master.c4020_request_period%TYPE
	)
	AS
		/*******************************************************
			* Description : Procedure to fetch mapping information for a demand sheet
			* Author		: Joe P Kumar
			*******************************************************/
		v_demandsheetype t4020_demand_master.c901_demand_type%TYPE;
	BEGIN
		SELECT c4020_request_period, c901_demand_type
		  INTO p_request_period, v_demandsheetype
		  FROM t4020_demand_master
		 WHERE c4020_demand_master_id = p_demandsheetid;

		IF (p_reftype = 40030)	 -- for Group
		THEN
			gm_fc_fch_demandgroupmap (p_demandsheetid, v_demandsheetype, p_reftype, p_outunselected, p_outselected);
		ELSIF (p_reftype = 40031)	-- for Set
		THEN
			gm_fc_fch_demandsetmap (p_demandsheetid, v_demandsheetype, p_reftype, p_outunselected, p_outselected);
		ELSIF (p_reftype = 4000109)	-- for InHouse Set
		THEN
			gm_fc_fch_demandsetmap (p_demandsheetid, v_demandsheetype, p_reftype, p_outunselected, p_outselected);			
		ELSIF (p_reftype = 40032)	-- for Region
		THEN
			gm_fc_fch_demandcodemap (p_demandsheetid
								   , v_demandsheetype
								   , p_reftype
								   , 'REGN'
								   , p_outunselected
								   , p_outselected
									);
		ELSIF (p_reftype = 40033)	-- for InHouse
		THEN
			gm_fc_fch_demandcodemap (p_demandsheetid
								   , v_demandsheetype
								   , p_reftype
								   , 'INPRP'
								   , p_outunselected
								   , p_outselected
									);
		END IF;
	END gm_fc_fch_demandmap;

--
	PROCEDURE gm_fc_fch_demandgroupmap (
		p_demandsheetid    IN		t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demandsheetype   IN		t4020_demand_master.c901_demand_type%TYPE
	  , p_reftype		   IN		t4021_demand_mapping.c901_ref_type%TYPE
 	  , p_out_available    OUT		TYPES.cursor_type
	  , p_out_associated   OUT		TYPES.cursor_type
	)
	AS
	/*******************************************************
	 * Description : Procedure to fetch grouping information for a demand sheet
	 * Author		 : Joe P Kumar
	 *******************************************************/
	BEGIN
		OPEN p_out_available
		 FOR
			 SELECT   t4010.c4010_group_id ID, t4010.c4010_group_nm NAME
				 FROM t4010_group t4010
				WHERE t4010.c4010_group_id NOT IN (
						   SELECT NVL (t4021.c4021_ref_id, 1)
							FROM t4021_demand_mapping t4021, t4020_demand_master t4020 						
							WHERE t4020.c4020_demand_master_id = t4021.c4020_demand_master_id
							AND t4020.C901_DEMAND_TYPE =   '4000103' -- template
						   AND t4021.c901_ref_type = p_reftype)
				  AND t4010.c4010_void_fl IS NULL
				  AND t4010.c901_type = '40045'   -- 40045 is forecast
			 ORDER BY t4010.c4010_group_nm;

		OPEN p_out_associated
		 FOR
			 SELECT   t4010.c4010_group_id ID, t4010.c4010_group_nm NAME
				 FROM t4021_demand_mapping t4021, t4010_group t4010
				WHERE t4021.c4021_ref_id(+) = t4010.c4010_group_id
				  AND t4021.c901_ref_type = p_reftype
				  AND t4021.c4020_demand_master_id = p_demandsheetid
				  --AND get_demand_type (t4021.c4020_demand_master_id) = p_demandsheetype
				  AND t4010.c4010_void_fl IS NULL
				  AND t4010.c901_type = '40045'   -- 40045 is forecast
			 ORDER BY t4010.c4010_group_nm;
	END gm_fc_fch_demandgroupmap;

	--
	PROCEDURE gm_fc_fch_demandsetmap (
		p_demandsheetid    IN		t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demandsheetype   IN		t4020_demand_master.c901_demand_type%TYPE
	  , p_reftype		   IN		t4021_demand_mapping.c901_ref_type%TYPE
	  , p_out_available       OUT		TYPES.cursor_type
	  , p_out_associated	  OUT		TYPES.cursor_type
	)
	AS
	/*******************************************************
	 * Description : Procedure to fetch set information for a demand sheet
	 * Author		 : Joe P Kumar
	 *******************************************************/
	BEGIN
		OPEN p_out_available
		 FOR
			 SELECT   t207.c207_set_id ID, t207.c207_set_nm NAME
				 FROM t207_set_master t207
				WHERE t207.c207_set_id NOT IN (
						  SELECT NVL (t4021.c4021_ref_id, 1)
							FROM t4021_demand_mapping t4021, t4020_demand_master t4020 						
							WHERE t4020.c4020_demand_master_id = t4021.c4020_demand_master_id
							AND t4020.C901_DEMAND_TYPE =   '4000103'  -- template
							AND t4021.c901_ref_type = p_reftype)
				  AND t207.c207_void_fl IS NULL
				  AND t207.c207_obsolete_fl IS NULL
				  AND t207.c901_status_id=20367 
				  AND t207.c901_set_grp_type = 1601
				  AND t207.c207_type IN  ( 
					  	 SELECT c906_rule_value
						   FROM t906_rules
						  WHERE c906_rule_id     = p_reftype
						    AND c906_rule_grp_id = 'TEMPLATE_SET_MAP')
			 ORDER BY t207.c207_set_id;

		OPEN p_out_associated
		 FOR
			 SELECT   t207.c207_set_id ID, t207.c207_set_nm|| DECODE(t207.c901_status_id,20369,' (** '||GET_CODE_NAME(t207.c901_status_id)||') ') NAME
				 FROM t4021_demand_mapping t4021, t207_set_master t207
				WHERE t4021.c4021_ref_id(+) = t207.c207_set_id
				  AND t4021.c901_ref_type = p_reftype
				  AND t4021.c4020_demand_master_id = p_demandsheetid
				  --AND get_demand_type (t4021.c4020_demand_master_id) = p_demandsheetype
				  AND t207.c207_void_fl IS NULL
				  AND t207.c207_obsolete_fl IS NULL
				  AND t207.c901_set_grp_type = 1601
			 ORDER BY t207.c207_set_id;
	END gm_fc_fch_demandsetmap;

	--
	PROCEDURE gm_fc_fch_demandcodemap (
		p_demandsheetid    IN		t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demandsheetype   IN		t4020_demand_master.c901_demand_type%TYPE
	  , p_reftype		   IN		t4021_demand_mapping.c901_ref_type%TYPE
	  , p_codegroup 	   IN		t901_code_lookup.c901_code_grp%TYPE
	  , p_outunselected    OUT		TYPES.cursor_type
	  , p_outselected	   OUT		TYPES.cursor_type
	)
	AS
			/*******************************************************
		* Description : Procedure to fetch region information for a demand sheet
		* Author		: Joe P Kumar
		*******************************************************/
		v_demandsheettype t4020_demand_master.c901_demand_type%TYPE;
	BEGIN
		SELECT c901_demand_type
		  INTO v_demandsheettype
		  FROM t4020_demand_master
		 WHERE c4020_demand_master_id = p_demandsheetid;

		IF (v_demandsheettype IS NOT NULL)
		THEN
			OPEN p_outunselected
			 FOR
				 SELECT   t901.c901_code_id ID, t901.c901_code_nm NAME
					 FROM t901_code_lookup t901
					WHERE t901.c901_code_id NOT IN (
							  SELECT NVL (t4021.c4021_ref_id, 1)
								FROM t4021_demand_mapping t4021
							   WHERE t4021.c4020_demand_master_id = p_demandsheetid
								 AND get_demand_type (t4021.c4020_demand_master_id) = p_demandsheetype
								 AND t4021.c901_ref_type = p_reftype)
					  AND t901.c901_code_grp = p_codegroup
				 ORDER BY t901.c901_code_nm;

			OPEN p_outselected
			 FOR
				 SELECT   t901.c901_code_id ID, t901.c901_code_nm NAME
					 FROM t4021_demand_mapping t4021, t901_code_lookup t901
					WHERE t4021.c4021_ref_id(+) = t901.c901_code_id
					  AND t4021.c901_ref_type = p_reftype
					  AND t4021.c4020_demand_master_id = p_demandsheetid
					  AND get_demand_type (t4021.c4020_demand_master_id) = p_demandsheetype
					  AND t901.c901_code_grp = p_codegroup
				 ORDER BY t901.c901_code_nm;
		END IF;

		-- initially for the first time, the default is other internationals is unselected, all others selected
		IF (v_demandsheettype IS NULL)
		THEN
			OPEN p_outunselected
			 FOR
				 SELECT t901.c901_code_id ID, t901.c901_code_nm NAME
				   FROM t901_code_lookup t901
				  WHERE t901.c901_code_id = 4007 AND t901.c901_code_grp = p_codegroup;

			OPEN p_outselected
			 FOR
				 SELECT t901.c901_code_id ID, t901.c901_code_nm NAME
				   FROM t901_code_lookup t901
				  WHERE t901.c901_code_id <> 4007 AND t901.c901_code_grp = p_codegroup;
		END IF;
	END gm_fc_fch_demandcodemap;

--
	PROCEDURE gm_fc_fch_demsheet_detail (
		p_demandsheetid   IN	   t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_detail		  OUT	   TYPES.cursor_type
	)
	AS
	 /*******************************************************
	* Description : Procedure to fetch detail for a demand sheet
	* Author		: Xun
	*******************************************************/
	BEGIN
		OPEN p_detail
		 FOR
   			 SELECT t4020.c4020_demand_master_id demandsheetid, t4020.c4020_demand_nm demandsheetname
				  , t4020.c901_demand_type demandsheettype, get_code_name (t4020.c901_demand_type) demandsheettypename
				  , t4020.c4020_demand_period demandperiod, t4020.c4020_forecast_period forecastperiod
				  , DECODE (t4020.c4020_include_current_fl, 'Y', 'on', 'off') Includecurrmonth
				  , DECODE (t4020.c4020_inactive_fl, 'Y', 'on', 'off') activeflag
				  , t4020.c4020_primary_user demandsheetowner, t4020.c4020_request_period requestperiod
				  , TO_CHAR (t4020.c4020_created_date, 'mm/dd/yyyy') createddt
                  , get_code_name(t4015.c901_level_id) levelid, get_code_name(t4015.c901_level_value) levelvalue
                  , get_code_name(t4015.c901_access_type) accesstype,t4015.c901_access_type accesstypeid
                  , gm_pkg_oppr_sheet.get_demand_inactive_fl(t4020.c4020_parent_demand_master_id) templtinactivefl
			   FROM t4020_demand_master t4020, t4015_global_sheet_mapping t4015
			  WHERE t4020.c4020_void_fl IS NULL 
			    AND t4020.c4020_demand_master_id = p_demandsheetid
                AND t4020.c4015_global_sheet_map_id = t4015.c4015_global_sheet_map_id(+)
                AND t4015.c4015_void_fl            IS NULL;
	END gm_fc_fch_demsheet_detail;

--
	PROCEDURE gm_fch_demand_par_info (
		p_demandmasterid   IN		t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_partnumber	   IN		t205_part_number.c205_part_number_id%TYPE
	  , p_detail		   OUT		TYPES.cursor_type
	)
	AS
			/*******************************************************
		* Description : Procedure to fetch demand par info
		* Author		: Xun Qu
		******************************************************/
		v_demandsheettype t4020_demand_master.c901_demand_type%TYPE;
		v_pnum CLOB;
	BEGIN
		SELECT c901_demand_type
		  INTO v_demandsheettype
		  FROM t4020_demand_master
		 WHERE c4020_demand_master_id = p_demandmasterid;

		 v_pnum := replace(p_partnumber,',','|');
		 
		IF (v_demandsheettype = 40020)
		THEN
			OPEN p_detail
			 FOR
				 SELECT   t205.c205_part_number_id pnum, t205.c205_part_num_desc pdesc
						, pvalue.parvalue parvalue, NVL (pvalue.c4022_history_fl, 'N') history_fl
						, '' comments
						, get_log_flag (t205.c205_part_number_id || '-' || t4021.c4020_demand_master_id, 1231) rlog
						, pvalue.DTLID PARDETAILID
					 FROM t4010_group t4010
						, t4021_demand_mapping t4021
						, t4011_group_detail t4011
						, t205_part_number t205
						, (SELECT c205_part_number_id, c4022_par_value parvalue, t4022.c4022_history_fl, t4022.c4022_demand_par_detail_id DTLID
							 FROM t4022_demand_par_detail t4022
							WHERE c4020_demand_master_id = p_demandmasterid) pvalue
					WHERE t4021.c4020_demand_master_id = p_demandmasterid
					  AND t4021.c901_ref_type = 40030
					  AND t4021.c4021_ref_id = t4010.c4010_group_id
					  AND t4010.c4010_group_id = t4011.c4010_group_id
					  AND t4011.c205_part_number_id = t205.c205_part_number_id
					  AND t4011.c205_part_number_id = pvalue.c205_part_number_id(+)
					  --AND t4011.c205_part_number_id LIKE p_partnumber || '%'
						AND REGEXP_LIKE(t4011.c205_part_number_id, CASE WHEN TO_CHAR(v_pnum) IS NOT NULL THEN TO_CHAR(REGEXP_REPLACE(v_pnum,'[+]','\+')) ELSE REGEXP_REPLACE(t4011.c205_part_number_id,'[+]','\+') END)
				 ORDER BY t205.c205_part_number_id;
		ELSIF (v_demandsheettype = 40021 OR v_demandsheettype = 40022)
		THEN
			OPEN p_detail
			 FOR
				 SELECT DISTINCT t205.c205_part_number_id pnum, t205.c205_part_num_desc pdesc
							   , pvalue.parvalue parvalue, NVL (pvalue.c4022_history_fl, 'N') history_fl
							   , '' comments
							   , get_log_flag (t205.c205_part_number_id || '-' || t4021.c4020_demand_master_id
											 , 1231
											  ) rlog
							   , pvalue.DTLID PARDETAILID
							FROM t4021_demand_mapping t4021
							   , t208_set_details t208
							   , t205_part_number t205
							   , (SELECT c205_part_number_id, c4022_par_value parvalue, t4022.c4022_history_fl, t4022.c4022_demand_par_detail_id DTLID
									FROM t4022_demand_par_detail t4022
								   WHERE c4020_demand_master_id = p_demandmasterid) pvalue
						   WHERE t4021.c4020_demand_master_id = p_demandmasterid
							 AND t4021.c901_ref_type = DECODE(v_demandsheettype,  40022, 4000109, 40031)  --In-house type(40001090) added.
							 AND t4021.c4021_ref_id = t208.c207_set_id
							 AND NVL (t208.c208_set_qty, 0) = 0
							 AND t208.c208_void_fl IS NULL
							 AND t205.c205_part_number_id = t208.c205_part_number_id
							 AND t208.c205_part_number_id = pvalue.c205_part_number_id(+)
							-- AND t205.c205_part_number_id LIKE  '%' || p_partnumber || '%'
							AND REGEXP_LIKE(t205.c205_part_number_id, CASE WHEN TO_CHAR(v_pnum) IS NOT NULL THEN TO_CHAR(REGEXP_REPLACE(v_pnum,'[+]','\+')) ELSE REGEXP_REPLACE(t205.c205_part_number_id,'[+]','\+') END)
						ORDER BY t205.c205_part_number_id;
		END IF;
	END gm_fch_demand_par_info;

--
	PROCEDURE gm_sav_demand_par_info (
		p_userid	 IN   t4022_demand_par_detail.c4022_updated_by%TYPE
	  , p_inputstr	 IN   VARCHAR2
	)
	AS
		v_strlen	   NUMBER := NVL (LENGTH (p_inputstr), 0);
		v_string	   VARCHAR2 (30000) := p_inputstr;
		v_substring    VARCHAR2 (1000);
		v_demandmasterid t4020_demand_master.c4020_demand_master_id%TYPE;
		v_partnumber   t4022_demand_par_detail.c205_part_number_id%TYPE;
		v_parvalue	   t4022_demand_par_detail.c4022_par_value%TYPE;
		v_demand_par_detail_id t4022_demand_par_detail.c4022_demand_par_detail_id%TYPE;
		--
	/*******************************************************
	  * Description : Procedure to save demand sheet par
	  * Author		  : Xun
	  *******************************************************/
	BEGIN
		IF v_strlen > 0
		THEN
			WHILE INSTR (v_string, '|') <> 0
			LOOP
				--
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				--
				v_partnumber := NULL;
				v_demandmasterid := NULL;
				v_parvalue	:= NULL;
				--
				v_partnumber := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_demandmasterid := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_parvalue	:= v_substring;

				UPDATE t4022_demand_par_detail
				   SET c4022_par_value = v_parvalue
					 , c4022_updated_by = p_userid
					 , c4022_updated_date = SYSDATE
				 WHERE c205_part_number_id = v_partnumber AND c4020_demand_master_id = v_demandmasterid;

				IF (SQL%ROWCOUNT = 0)
				THEN
					INSERT INTO t4022_demand_par_detail
								(c4022_demand_par_detail_id, c4020_demand_master_id, c205_part_number_id
							   , c4022_par_value, c4022_updated_by, c4022_updated_date
								)
						 VALUES (s4022_demand_par_detail.NEXTVAL, v_demandmasterid, v_partnumber
							   , v_parvalue, p_userid, SYSDATE
								);
				END IF;
			END LOOP;
		END IF;
	END gm_sav_demand_par_info;

--

	/*******************************************************
			 * Description : Procedure to Update Demand Sheet Detail
			 * Author			: Xun
			 *******************************************************/
	PROCEDURE gm_fc_upt_demandsheetpar (
		p_demandsheetid   IN   t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_partnumber	  IN   t205_part_number.c205_part_number_id%TYPE
	  , p_parvalue		  IN   t4022_demand_par_detail.c4022_par_value%TYPE
	)
	AS
		v_demandsheettype t4020_demand_master.c901_demand_type%TYPE;
	BEGIN
		SELECT t4020.c901_demand_type
		  INTO v_demandsheettype
		  FROM t4040_demand_sheet t4040, t4020_demand_master t4020
		 WHERE t4040.c4020_demand_master_id = t4020.c4020_demand_master_id
		   AND t4040.c4040_demand_sheet_id = p_demandsheetid;

		UPDATE t4042_demand_sheet_detail t4042
		   SET t4042.c4042_qty = p_parvalue
		 WHERE t4042.c4040_demand_sheet_id = p_demandsheetid
		   AND t4042.c205_part_number_id = p_partnumber
		   AND t4042.c901_type = 50566
		   AND t4042.c4042_ref_id = DECODE (v_demandsheettype, 40020, t4042.c4042_ref_id, -9999);
	END gm_fc_upt_demandsheetpar;

--
	PROCEDURE gm_fch_demand_par_history (
		p_demandmastertid	IN		 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_partnumber		IN		 t205_part_number.c205_part_number_id%TYPE
	  , p_detail			OUT 	 TYPES.cursor_type
	)
	AS
	 /*******************************************************
	* Description : Procedure to fetch Par detail for a demand sheet
	* Author		: Xun
	*******************************************************/
	BEGIN
		OPEN p_detail
		 FOR
			 SELECT t4020.c4020_demand_nm demandsheetname, t4022.c205_part_number_id pnum
				  , get_partnum_desc (p_partnumber) pdesc, t4022.c4022_demand_par_detail_id detailid
			   FROM t4020_demand_master t4020, t4022_demand_par_detail t4022
			  WHERE t4020.c4020_demand_master_id = t4022.c4020_demand_master_id
				AND t4022.c4020_demand_master_id = p_demandmastertid
				AND t4022.c205_part_number_id = p_partnumber;
	END gm_fch_demand_par_history;

--
	PROCEDURE gm_fc_sav_demandsheetmap (
		p_demandsheetid    IN	t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demandsheetype   IN	t4020_demand_master.c901_demand_type%TYPE
	  , p_reftype		   IN	t4021_demand_mapping.c901_ref_type%TYPE
	  , p_inputstr		   IN	CLOB
	)
	AS
		/*******************************************************
			* Description : Procedure to save grouping information for a demand sheet
			* Author		: Joe P Kumar
			*******************************************************/
		v_demandsheettype t4020_demand_master.c901_demand_type%TYPE;
		v_demandsheetmonthcnt NUMBER (10);

		CURSOR cur_sheet_details
		IS
			SELECT c205_part_number_id refid
			  FROM my_t_part_list;
	BEGIN
		-- The input String in the form of 101.210,101.211 will be set into this temp view.
		my_context.set_my_cloblist (p_inputstr);

		SELECT COUNT (c4040_demand_sheet_id)
		  INTO v_demandsheetmonthcnt
		  FROM t4040_demand_sheet
		 WHERE c4020_demand_master_id = p_demandsheetid;

		SELECT c901_demand_type
		  INTO v_demandsheettype
		  FROM t4020_demand_master
		 WHERE c4020_demand_master_id = p_demandsheetid;

		IF (v_demandsheetmonthcnt > 0 AND v_demandsheettype <> p_demandsheetype)
		THEN
			-- Error message is Cannot change type as monthly demand sheets have already been generated
			raise_application_error (-20059, '');
		END IF;

		-- If its new mapping then remove old information and reload again
		-- For E.g. if sheet moved from sales to consignment then
		-- remove all old transaction
		IF (v_demandsheettype <> p_demandsheetype)
		THEN
			DELETE FROM t4021_demand_mapping
				  WHERE c4020_demand_master_id = p_demandsheetid;
		END IF;

		FOR var_sheet_detail IN cur_sheet_details
		LOOP
			UPDATE t4021_demand_mapping
			   SET c4021_ref_id = var_sheet_detail.refid
			 WHERE c4020_demand_master_id = p_demandsheetid
			   AND c901_ref_type = p_reftype
			   AND c4021_ref_id = var_sheet_detail.refid;

			IF (SQL%ROWCOUNT = 0)
			THEN
				INSERT INTO t4021_demand_mapping
							(c4021_demand_mapping_id, c4020_demand_master_id, c901_ref_type, c4021_ref_id
						   , c901_action_type
							)
					 VALUES (s4021_demand_mapping.NEXTVAL, p_demandsheetid, p_reftype, var_sheet_detail.refid
						   , DECODE (p_reftype, 40031, 50291, NULL)
							);
			END IF;
		END LOOP;

	END gm_fc_sav_demandsheetmap;

 --
/*******************************************************
 *		 Purpose: function is used to fetch the demandtype given the demand id
 *******************************************************/
--
	FUNCTION get_demand_type (
		p_demandsheetid   IN   t4020_demand_master.c4020_demand_master_id%TYPE
	)
		RETURN NUMBER
	IS
--
		v_demand_sheet_type NUMBER;
	BEGIN
		SELECT c901_demand_type
		  INTO v_demand_sheet_type
		  FROM t4020_demand_master
		 WHERE c4020_demand_master_id = p_demandsheetid;

		RETURN v_demand_sheet_type;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END get_demand_type;
	
		/********************************************************
			   * Description : Procedure to approve the demand sheet for that month
			   * Author 	: Joe P Kumar
			*******************************************************/
	PROCEDURE gm_fc_sav_approveds (
		p_demandsheetmonthid   IN	t4040_demand_sheet.c4040_demand_sheet_id%TYPE		
	  , p_userid			   IN	t4040_demand_sheet.c4040_last_updated_by%TYPE
	  , p_comments 			   IN    t902_log.c902_comments%TYPE DEFAULT NULL
	)
	AS
		v_level_id 			t4040_demand_sheet.C901_LEVEL_ID%TYPE;
		v_level_value 			t4040_demand_sheet.C901_LEVEL_VALUE%TYPE;
		v_demand_mas_id 	t4040_demand_sheet.C4020_DEMAND_MASTER_ID%TYPE;
		v_dmd_type_id 		t4020_demand_master.C901_DEMAND_TYPE%TYPE;
		v_templpt_id 		t4020_demand_master.C4020_PARENT_DEMAND_MASTER_ID%TYPE;
		v_inventory_id  	t4040_demand_sheet.C250_INVENTORY_LOCK_ID%TYPE;
		v_msg 				VARCHAR2(1000);
	
	CURSOR cur_dmd_sheet
	IS
	
	-- Based on the T4015 and V700 mapping, we will get the Region when the current demand sheet level is Zone.
	-- When the Level is Intermediate, we will get the Zone and the regions associated to it.
		 SELECT c4040_demand_sheet_id dmd_sheet_id
		FROM t4020_demand_master t4020,
			 t4015_global_sheet_mapping t4015 , t4040_demand_sheet t4040
		WHERE t4020.C4015_GLOBAL_SHEET_MAP_ID = t4015.C4015_GLOBAL_SHEET_MAP_ID
		AND t4020.C4020_DEMAND_MASTER_ID = t4040.C4020_DEMAND_MASTER_ID
		AND t4020.c4020_void_fl is NULL 
		and t4020.C4020_INACTIVE_FL IS NULL
		AND t4015.c4015_void_fl is NULL 
		and t4020.C901_COMPANY_ID = t4015.C901_COMPANY_ID
		AND t4020.C4020_PARENT_DEMAND_MASTER_ID = v_templpt_id
        and t4015.c901_level_id <> 102580 -- Worldwide 
        AND t4040.C250_INVENTORY_LOCK_ID = v_inventory_id
		AND t4020.C901_DEMAND_TYPE = v_dmd_type_id
		and  (t4015.c901_level_id, t4015.c901_level_value) IN (
	
		 SELECT DISTINCT 102584, v700.region_id
           FROM v700_territory_mapping_detail v700
           WHERE v700.gp_id = DECODE (v_level_id,102583, v_level_value,v700.gp_id) -- Zone Level Rollup 
           AND v700.divid = DECODE (v_level_id, 102582, 100824, v700.divid) -- Intermediate level rollup
           AND v700.divid = DECODE (v_level_id,102581, -1,v700.divid) -- Division level Rollup 
           AND v700.compid = DECODE (v_level_id,102580, -1,v700.compid) -- company level rollup
           AND v700.compid = DECODE (v_level_id,-11111, -1,v700.compid) -- Disable if its US 
           UNION ALL
           SELECT t4015.c901_level_id, t4015.c901_level_value
             FROM t4015_global_sheet_mapping t4015
            WHERE (t4015.c901_level_id = DECODE (v_level_id, 102580, c901_level_id, -1)   -- Company level Rollup
                OR t4015.c901_level_id = DECODE (v_level_id, 102581, 
                            DECODE (c901_level_value, 100823, -1, c901_level_id), -1) -- Division level rollup only ous
                OR t4015.c901_level_id = DECODE (v_level_id, -11111, 
                            DECODE (c901_level_value, 100823, c901_level_id,-1 ), -1) -- Only to pick US Sheet 
                  )
		)
		;
		
		
	BEGIN
		-- Check whether this demand sheet can be approved
		
	      gm_pkg_oppr_sheet.gm_fc_chk_request (p_demandsheetmonthid);

		-- Update
		UPDATE t4040_demand_sheet
		   SET c901_status = 50551
			 , c4040_approved_date = SYSDATE
			 , c4040_approved_by = p_userid
			 , c4040_last_updated_by = p_userid
			 , c4040_last_updated_date = SYSDATE
		 WHERE c4040_demand_sheet_id = p_demandsheetmonthid;
		 
		 SELECT C901_LEVEL_ID,C4020_DEMAND_MASTER_ID,C250_INVENTORY_LOCK_ID,C901_LEVEL_VALUE INTO v_level_id,v_demand_mas_id,v_inventory_id,v_level_value
		 FROM t4040_demand_sheet WHERE c4040_demand_sheet_id = p_demandsheetmonthid;
		 
		 --If the Level is Company, then get all the sheet under company and approve it.
		 IF v_level_id ='102580' -- Company
		 --102584  Region    ,102581  Division  ,102582 Intermediate   ,102604  OUS DISTRIBUTOR  
		 THEN
		 	SELECT C4020_PARENT_DEMAND_MASTER_ID,C901_DEMAND_TYPE
		 	INTO v_templpt_id,v_dmd_type_id
		 	FROM t4020_demand_master
		 	WHERE C4020_DEMAND_MASTER_ID = v_demand_mas_id;
		 	
		 	FOR cur_demand_sheet IN cur_dmd_sheet
    		LOOP
    			   UPDATE t4040_demand_sheet
				   SET c901_status = 50551
					 , c4040_approved_date = SYSDATE
					 , c4040_approved_by = p_userid
					 , c4040_last_updated_by = p_userid
					 , c4040_last_updated_date = SYSDATE
				   WHERE c4040_demand_sheet_id = cur_demand_sheet.dmd_sheet_id;
				   
				   gm_update_log(cur_demand_sheet.dmd_sheet_id,p_comments,'1226',p_userid,v_msg);-- 1226 - Approve Log type
				   
    		END LOOP;
		 
		 END IF;
	END gm_fc_sav_approveds;

 /*******************************************************
 * Purpose: function is used to fetch the demand sheet name given the demand sheet id
 *******************************************************/
--
	FUNCTION get_demandsheet_name (
		p_demandsheetid   IN   t4020_demand_master.c4020_demand_master_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_demand_sheet_name VARCHAR2 (100);
	BEGIN
		SELECT c4020_demand_nm
		  INTO v_demand_sheet_name
		  FROM t4020_demand_master
		 WHERE c4020_demand_master_id = p_demandsheetid;

		RETURN v_demand_sheet_name;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END get_demandsheet_name;

 /*******************************************************
 * Purpose: function is used to fetch the hierarchy id given the demand sheet name
 *******************************************************/
--
	FUNCTION get_hierarchy_id (
		p_type	   IN	t9352_hierarchy_flow.c901_ref_type%TYPE
	  , p_ref_id   IN	t9352_hierarchy_flow.c9352_ref_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_hierarchyid  t9350_hierarchy.c9350_hierarchy_id%TYPE;
	BEGIN
		SELECT c9350_hierarchy_id
		  INTO v_hierarchyid
		  FROM t9352_hierarchy_flow
		 WHERE c901_ref_type = p_type AND c9352_ref_id = p_ref_id;

		RETURN v_hierarchyid;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END get_hierarchy_id;

--
/*******************************************************
 * Purpose: Procedure is used to check if the qty in
			the request is greater than the required,
			if greater then an exception is raised
 *******************************************************/
--
	PROCEDURE gm_fc_chk_request (
		p_dsmonthid   IN   t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	)
	AS
		v_count 	   NUMBER;
	BEGIN
		/*
		SELECT COUNT (1)
		  INTO v_count
		  FROM (SELECT ID
					 ,	 gm_pkg_op_sheet_summary.get_ds_growth_request_count (dmid, ID, ref_type, req_date)
					   - growth_qty req_count
				  FROM (SELECT	 c4030_ref_id ID
							   , DECODE (NVL (TRIM (get_set_name (c4030_ref_id)), '0')
									   , '0', 20298
									   , NVL (TRIM (get_partnum_desc (c4030_ref_id)), '0'), '0'
									   , 20296
										) ref_type
							   , NVL (SUM (t4043.c4043_value), 0) growth_qty, c4043_start_date req_date
							   , t4040.c4020_demand_master_id dmid
							FROM t4040_demand_sheet t4040
							   , t4043_demand_sheet_growth t4043
							   , t4030_demand_growth_mapping t4030
						   WHERE t4040.c4040_demand_sheet_id = p_dsmonthid
							 AND t4040.c4040_demand_sheet_id = t4043.c4040_demand_sheet_id
							 AND t4030.c4030_demand_growth_id = t4043.c4030_demand_growth_id
							 AND c4043_start_date BETWEEN t4040.c4040_demand_period_dt
													  AND ADD_MONTHS (t4040.c4040_demand_period_dt
																	, (t4040.c4040_request_period - 1)
																	 )
							 AND t4040.c901_demand_type IN ('40021', '40022')
							 AND (	 t4043.c901_ref_type = '20381'
								  OR (t4043.c901_ref_type = '20382' AND t4043.c4043_value <> 0)
								 )
						GROUP BY t4040.c4020_demand_master_id, c4043_start_date, c4030_ref_id)) temp
		 WHERE temp.req_count > 0;
		*/
		
		  SELECT COUNT (1)
		  INTO v_count
		  FROM (SELECT ID
					 ,	 gm_pkg_oppr_sheet_summary.get_ds_growth_request_count (dmid, ID, ref_type, req_date)
                    - growth_qty req_count
				  FROM (SELECT	 C4030_GRTH_REF_ID ID
							   , DECODE (NVL (TRIM (get_set_name (C4030_GRTH_REF_ID)), '0')
									   , '0', 20298
									   , NVL (TRIM (get_partnum_desc (C4030_GRTH_REF_ID)), '0'), '0'
									   , 20296
										) ref_type
							   , NVL (SUM (t4043.c4043_value), 0) growth_qty, c4043_start_date req_date
							   , t4040.c4020_demand_master_id dmid
							FROM t4040_demand_sheet t4040
							   , t4043_demand_sheet_growth t4043
							 WHERE t4040.c4040_demand_sheet_id in ( 
								 SELECT t4046.c4040_demand_sheet_assoc_id FROM t4046_demand_sheet_assoc_map t4046
								WHERE t4046.c4040_demand_sheet_id = p_dsmonthid
								UNION
								SELECT p_dsmonthid FROM DUAL  ) AND 
                           t4040.c4040_demand_sheet_id = t4043.c4040_demand_sheet_id
							 AND c4043_start_date BETWEEN t4040.c4040_demand_period_dt
													  AND ADD_MONTHS (t4040.c4040_demand_period_dt
																	, (t4040.c4040_request_period - 1)
																	 )
							 AND t4040.c901_demand_type IN ('40021', '40022') --Consignment, In-House Consignment
							 AND (	 t4043.c901_ref_type = '20381'OR (t4043.c901_ref_type = '20382' AND t4043.c4043_value <> 0)) -- U , A
						GROUP BY t4040.c4020_demand_master_id, c4043_start_date, C4030_GRTH_REF_ID))temp WHERE temp.req_count > 0;
						
		IF v_count > 0
		THEN
			raise_application_error ('-20075', '');
		END IF;
	
	END gm_fc_chk_request;

--

	/*******************************************************
	* Purpose: function is used to fetch the demand sheet status
	*******************************************************/
	FUNCTION get_demand_sheet_status (
		p_demand_master_id	 t4040_demand_sheet.c4020_demand_master_id%TYPE
	  , p_date				 t4031_growth_details.c4031_start_date%TYPE
	)
		RETURN NUMBER
	IS
/*	  Description	  : THIS FUNCTION RETURNS STATUS FOR DEMAND SHEET
 Parameters 		: p_demand_master_id,P_date
*/
		v_status	   NUMBER;
	BEGIN
		BEGIN
			SELECT c901_status
			  INTO v_status
			  FROM t4040_demand_sheet t4040
			 WHERE t4040.c4020_demand_master_id = p_demand_master_id AND t4040.c4040_demand_period_dt = p_date;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN NULL;
		END;

		RETURN v_status;
	END get_demand_sheet_status;

--

	/*******************************************************
   * Description : Procedure to fetch Set Mapping Info
   * Author 	 : Xun
   *******************************************************/
--
	PROCEDURE gm_fc_fch_setmap (
		p_demand_master_id	 IN 	  t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_setmapdetails 	 OUT	  TYPES.cursor_type
	)
	AS
		v_count 	   NUMBER;
	BEGIN
		OPEN p_setmapdetails
		 FOR
			 SELECT   DECODE (c4021_ref_id, '-9999', '', c4021_ref_id) setid
					, DECODE (c4021_ref_id, '-9999', 'Item Consignment', get_set_name (c4021_ref_id)) setname
					, c901_action_type setactiontype, c4021_stack_month monthid
				 FROM t4021_demand_mapping
				WHERE c4020_demand_master_id = p_demand_master_id AND c901_ref_type IN (40031, 40034)
			 UNION
			 SELECT   '' setid, 'Item Consignment' setname, NULL setactiontype, NULL monthid
				 FROM DUAL
				WHERE 0 IN (SELECT COUNT (1)
							  FROM t4021_demand_mapping
							 WHERE c4020_demand_master_id = p_demand_master_id AND c901_ref_type = 40034)	--first time, there's no item consignment
			 ORDER BY setid;
	END gm_fc_fch_setmap;

/*******************************************************
	  * Description : Procedure to save Set Mapping Info
	  * Author		  : Xun
	  *******************************************************/
	PROCEDURE gm_fc_sav_setmap (
		p_inputstr	 IN   VARCHAR2
	)
	AS
		v_strlen	   NUMBER := NVL (LENGTH (p_inputstr), 0);
		v_string	   VARCHAR2 (30000) := p_inputstr;
		v_substring    VARCHAR2 (1000);
		v_demandmasterid t4020_demand_master.c4020_demand_master_id%TYPE;
		v_setid 	   t4022_demand_par_detail.c205_part_number_id%TYPE;
		v_setactiontype t4022_demand_par_detail.c4022_par_value%TYPE;
		v_dmapping_id  t4021_demand_mapping.c4021_demand_mapping_id%TYPE;
		v_month 	   t4021_demand_mapping.c4021_stack_month%TYPE;
	--
	BEGIN
		IF v_strlen > 0
		THEN
			WHILE INSTR (v_string, '|') <> 0
			LOOP
				--
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				--
				v_setid 	:= NULL;
				v_demandmasterid := NULL;
				v_setactiontype := NULL;
				--
				v_setid 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_demandmasterid := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_setactiontype := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_month 	:= v_substring;

				IF (v_setid = 'null')	--update and insert only for item
				THEN
					UPDATE t4021_demand_mapping
					   SET c901_action_type = v_setactiontype
						 , c4021_stack_month = v_month
					 WHERE c4020_demand_master_id = v_demandmasterid AND c901_ref_type = 40034;   --item

					IF (SQL%ROWCOUNT = 0)
					THEN
						SELECT s4021_demand_mapping.NEXTVAL
						  INTO v_dmapping_id
						  FROM DUAL;

						INSERT INTO t4021_demand_mapping
									(c4021_demand_mapping_id, c4020_demand_master_id, c901_ref_type, c4021_ref_id
								   , c901_action_type, c4021_stack_month
									)
							 VALUES (v_dmapping_id, v_demandmasterid, 40034, '-9999'
								   , v_setactiontype, v_month
									);
					END IF;
				END IF;

				UPDATE t4021_demand_mapping
				   SET c901_action_type = v_setactiontype
					 , c4021_stack_month = v_month
				 WHERE c4021_ref_id = v_setid AND c4020_demand_master_id = v_demandmasterid AND c901_ref_type = 40031;	 --set
			END LOOP;
		END IF;
	END gm_fc_sav_setmap;

--
--
/*******************************************************
 * Purpose: Procedure is used to pull the crossover parts
			of the given sheets in to Multipart Sheet
 *******************************************************/
--
	PROCEDURE gm_pull_multipartsheet (
		p_demand_sheetid	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_userid			IN	 t4045_demand_sheet_assoc.c4045_last_updated_by%TYPE
	  , p_ignore_ttp_summary IN  VARCHAR DEFAULT 'N'
	)
	AS
		v_email_ids 	VARCHAR2(1000);
		v_email_subject	VARCHAR2(1000);
		v_email_body	VARCHAR2(4000);
		v_demand_nm		t4020_demand_master.c4020_demand_nm%TYPE;
	BEGIN
		SELECT get_rule_value('MULTI_PART_EMAIL','MULTI_PART_PULL')||','||get_user_emailid(p_userid)
		     , get_rule_value('MULTI_PART_EMAIL_SUB','MULTI_PART_PULL')
		  INTO v_email_ids, v_email_subject FROM DUAL;

		  v_email_body	:= ' The following Demand Sheet pulled successfully. ';
		
		BEGIN
			gm_op_sav_multipartsheet(p_demand_sheetid,p_userid,p_ignore_ttp_summary);
		    COMMIT;
		    
			EXCEPTION WHEN OTHERS THEN
				ROLLBACK;
				v_email_body	:= ' The following Demand Sheet pull failed. ';	
		END;
		BEGIN		
			SELECT t4020.c4020_demand_nm 
			  INTO v_demand_nm
		      FROM t4020_demand_master t4020, t4040_demand_sheet t4040
		     WHERE t4020.c4020_demand_master_id = t4040.c4020_demand_master_id
		       AND t4040.c4040_demand_sheet_id  = p_demand_sheetid
		       AND t4020.c4020_void_fl         IS NULL
		       AND t4040.c4040_void_fl         IS NULL;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				v_demand_nm	:= '';	
		END;		
		v_email_body := v_email_body ||' '|| v_demand_nm;
		gm_com_send_email_prc (v_email_ids, v_email_subject, v_email_body);	
		
	END gm_pull_multipartsheet;
--
/*******************************************************
 * Purpose: Procedure is used to save the crossover parts
			of the given sheets in to Multipart Sheet
 *******************************************************/
--
	PROCEDURE gm_op_sav_multipartsheet (
		p_demand_sheetids	IN	 VARCHAR2
	  , p_userid			IN	 t4045_demand_sheet_assoc.c4045_last_updated_by%TYPE
	  , p_ignore_ttp_summary IN  VARCHAR 
	)
	AS
	v_inventory_id  t250_inventory_lock.c250_inventory_lock_id%TYPE;
	
	BEGIN
				
						SELECT MAX (t250.c250_inventory_lock_id)
						INTO v_inventory_id
						FROM t250_inventory_lock t250
						WHERE t250.c901_lock_type = 20430
						AND t250.c4016_global_inventory_map_id  = 1  
						--US Inventory
						AND t250.c250_void_fl IS NULL;
											         
		gm_op_sav_multipartsheet(p_demand_sheetids,p_userid,p_ignore_ttp_summary,TRUNC (SYSDATE, 'MM'),v_inventory_id);
		
END gm_op_sav_multipartsheet;

--
/*******************************************************
 * Purpose: Procedure is used to save the crossover parts
			of the given sheets in to Multipart Sheet
 *******************************************************/
--
	PROCEDURE gm_op_sav_multipartsheet (
		p_demand_sheetids	IN	 VARCHAR2
	  , p_userid			IN	 t4045_demand_sheet_assoc.c4045_last_updated_by%TYPE
	  , p_ignore_ttp_summary	IN  VARCHAR 
	  , p_load_date				IN DATE
	  , p_inventory_id			IN t250_inventory_lock.c250_inventory_lock_id%TYPE
	)
	AS
		v_multi_sheet_id t4040_demand_sheet.c4040_demand_sheet_id%TYPE;
		v_inventory_id t4040_demand_sheet.c250_inventory_lock_id%TYPE;
		v_multi_sheet_status t4040_demand_sheet.c901_status%TYPE;
		v_comp_id t4020_demand_master.c901_company_id%TYPE;
		v_level_value t4040_demand_sheet.c901_level_value%TYPE;
		v_ttp_detail_id	t4040_demand_sheet.c4052_ttp_detail_id%TYPE;
		v_mp_ttp_detail_id	t4040_demand_sheet.c4052_ttp_detail_id%TYPE;
		v_div_id	  t1910_division.c1910_division_id%TYPE;
		v_div_map_id	  t1910_division.c1910_division_id%TYPE;
		v_status NUMBER;
		v_demand_sheetids VARCHAR2(500);

		CURSOR cur_crossover_part
		IS
			SELECT c205_part_number_id pnum, c4042_period period, c901_type fctype, SUM(c4042_qty) qty
			FROM ts4042_ttp_multipart_month
			WHERE c205_part_number_id IN (SELECT DISTINCT c205_part_number_id FROM ts4042_ttp_multipart_month 
			WHERE c4042_upd_flag='Y' AND c4052_ttp_detail_id=v_ttp_detail_id)
			GROUP BY c205_part_number_id, c4042_period, c901_type;  
			 
			   
	BEGIN
				
		my_context.set_my_inlist_ctx (p_demand_sheetids);
		v_demand_sheetids :=p_demand_sheetids;
				
		BEGIN
 					
 				BEGIN
	 			
	 				-- To get the company id for demand sheet ids
					SELECT max(t4020.c901_company_id),max(c4052_ttp_detail_id)
		 				into v_comp_id ,v_ttp_detail_id
						FROM t4020_demand_master t4020, t4040_demand_sheet t4040
		 				WHERE t4020.c4020_demand_master_id=t4040.c4020_demand_master_id
		 				AND c4040_demand_period_dt =TRUNC (p_load_date, 'MM')
		 				AND t4040.c4040_void_fl	IS NULL	
		                AND c4040_demand_sheet_id IN (SELECT * FROM v_in_list);
 			  
		                		                
                EXCEPTION WHEN OTHERS
          		THEN
          			v_comp_id := '100800';
          		END; 
          		
          			BEGIN
		          		
		          		 SELECT c906_rule_value INTO v_level_value
							FROM t906_rules t906
							WHERE c906_rule_id =v_comp_id
							AND t906.c906_rule_grp_id ='GOP_MP_SHEET_LOCK'
		                    AND t906.c906_void_fl IS NULL; 
  				
		            EXCEPTION WHEN OTHERS
	          		THEN
	          			v_comp_id := '100800';
	          		END; 
			END;
		
			-- PMT-57165 Exception we are hard coding as MP spine 
		BEGIN
			--c901_level_value Multi Part Trauma demand sheet only has level value
			SELECT	   t4040.c4040_demand_sheet_id, t4040.c250_inventory_lock_id, t4040.c901_status,c4052_ttp_detail_id
				  INTO v_multi_sheet_id, v_inventory_id, v_multi_sheet_status,v_mp_ttp_detail_id
				  FROM t4040_demand_sheet t4040
				 WHERE t4040.c901_demand_type = '40023'
				  AND NVL(t4040.c901_level_value,'1') = NVL(v_level_value,'1')
				  AND t4040.c250_inventory_lock_id = p_inventory_id
				  AND t4040.c4040_void_fl	IS NULL		   
			FOR UPDATE;
		 
			IF v_multi_sheet_status <> 50550
			THEN
				RETURN;
			END IF;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				raise_application_error ('-20099', '');
		END;

		
		gm_pkg_oppr_ld_multipart.gm_op_multipart_fetch_qty_by_ttp(v_demand_sheetids,v_multi_sheet_id);
		
		FOR cur_val IN cur_crossover_part
		LOOP
			gm_op_sav_multipart_detail (v_multi_sheet_id, cur_val.pnum, cur_val.qty, cur_val.period, p_userid, cur_val.fctype);
			
		END LOOP;
		
							
		UPDATE ts4042_ttp_multipart_month SET c4042_upd_flag=NULL
		WHERE c4042_upd_flag='Y'
		AND c4052_ttp_detail_id=v_ttp_detail_id; 
		
		INSERT INTO t4045_demand_sheet_assoc
					(c4045_demand_sheet_assoc_id, c4040_demand_sheet_id, c4020_demand_master_assoc_id, c901_status
				   , c4045_demand_period_dt, c4045_last_updated_by, c4045_last_updated_date)
			SELECT s4045_demand_sheet_assoc.NEXTVAL, v_multi_sheet_id, c4020_demand_master_id, c901_status
				 , t4040.c4040_demand_period_dt, p_userid, TRUNC (SYSDATE)
			  FROM t4040_demand_sheet t4040
			 WHERE t4040.c4040_demand_sheet_id IN (SELECT * FROM v_in_list);
			
			 --TTP SUmmary should be reloaded when the individual TTP is Lock &Generated.
			 IF p_ignore_ttp_summary ='N'
			 THEN
				-- TTP Summary Load
				gm_pkg_oppr_ld_summary.gm_op_ld_summary_main(v_mp_ttp_detail_id,v_multi_sheet_id,p_userid);
			END IF;
			
END gm_op_sav_multipartsheet;
--
/*******************************************************
 * Purpose: Procedure is used to save the details of
			of the given part in to Multipart Sheet
 *******************************************************/
--
	PROCEDURE gm_op_sav_multipart_detail (
		p_multi_sheet_id   IN	t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_pnum			   IN	t4042_demand_sheet_detail.c205_part_number_id%TYPE
	  , p_qty			   IN	t4042_demand_sheet_detail.c4042_qty%TYPE
	  , p_period		   IN	t4042_demand_sheet_detail.c4042_period%TYPE
	  , p_user_id		   IN	t4042_demand_sheet_detail.c4042_updated_by%TYPE
	  , p_type			   IN   t4042_demand_sheet_detail.c901_type%TYPE	
	)
	AS
		v_detail_id    t4042_demand_sheet_detail.c4042_demand_sheet_detail_id%TYPE;
		v_qty NUMBER;
	BEGIN
	      -- v_qty := ROUND(p_qty,0);
		  -- handled round in temp load itself
		/*	UPDATE t4042_demand_sheet_detail
			   --SET c4042_qty = c4042_qty + v_qty -- When enter decimal point in Part Number assembly screen, need to be display in TTP finilyzing screen.
			   -- Changed as part of PMT-38024
				SET c4042_qty = p_qty 
				 , c4042_updated_by = p_user_id
				 , c4042_updated_date = TRUNC (SYSDATE)
			 WHERE  c4040_demand_sheet_id = p_multi_sheet_id
			   AND c205_part_number_id = p_pnum
			   AND c4042_period = p_period
			   AND c901_type = p_type
			   AND c4042_ref_id = '-9999';
			   */
			   
			   
			    /* New Table Map */
			   UPDATE t4042_mp_demand_sheet_detail
				SET c4042_qty = p_qty 
				 , c4042_updated_by = p_user_id
				 , c4042_updated_date = SYSDATE
			 WHERE  c4040_demand_sheet_id = p_multi_sheet_id
			   AND c205_part_number_id = p_pnum
			   AND c4042_period = p_period
			   AND c901_type = p_type
			   AND c4042_ref_id = '-9999';			   
			    
			   
		IF (SQL%ROWCOUNT = 0)
		THEN
			
			/*	INSERT INTO t4042_demand_sheet_detail
							(c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id, c4042_period
						   , c4042_qty, c4042_updated_by, c901_type, c205_part_number_id, c4042_updated_date
							)
					 VALUES (s4042_demand_sheet_detail.NEXTVAL, p_multi_sheet_id, '-9999', p_period
						   , p_qty, p_user_id, p_type, p_pnum, TRUNC (SYSDATE)
							);*/
							
							
				/*  New Table Map */			
				INSERT INTO t4042_mp_demand_sheet_detail
							( c4040_demand_sheet_id, c4042_ref_id, c4042_period
						   , c4042_qty, c4042_updated_by, c901_type, c205_part_number_id, c4042_updated_date
							)
					 VALUES (p_multi_sheet_id, '-9999', p_period
						   , p_qty, p_user_id, p_type, p_pnum, SYSDATE
							);							
							
		END IF;
		
	END gm_op_sav_multipart_detail;

--
--
/***********************************************************
 * Purpose: Function to get the demand sheet id's as comma
 *			  seperated string for the given ttp_detail_id
 ***********************************************************/
--
	FUNCTION get_demand_sheet_ids (
		p_ttp_detail_id   IN   t4040_demand_sheet.c4052_ttp_detail_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_ids		   VARCHAR2 (1000);

		CURSOR cur_part
		IS
			SELECT c4040_demand_sheet_id ID
			  FROM t4040_demand_sheet
			 WHERE c4052_ttp_detail_id = p_ttp_detail_id
			  AND c901_level_id = 102580;
	BEGIN
		FOR cur_val IN cur_part
		LOOP
			v_ids		:= v_ids || cur_val.ID || ',';
		END LOOP;

		RETURN v_ids;
	END get_demand_sheet_ids;

--
/***********************************************************
 * Purpose: Procedure to fetch the Set Mapping Info for launch
 ***********************************************************/
--
	PROCEDURE gm_fch_setmap_for_launch (
		p_demand_master_id	 IN 	  t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_setmapdetails 	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_setmapdetails
		 FOR
			 SELECT   c4021_demand_mapping_id mappingid, DECODE (c4021_ref_id, '-9999', '', c4021_ref_id) ID
					, c901_ref_type ref_type
					, DECODE (c901_ref_type
							, 40030, get_group_name (c4021_ref_id)
							, 40031, get_set_name (c4021_ref_id)
							, 4000109, get_set_name (c4021_ref_id)
							, 40034, 'Item Consignment'
							 ) NAME
					, TO_CHAR (c4021_launch_date, 'mm/dd/yyyy') launchdt, NULL newdt, c4021_history_fl histfl
					, DECODE (DECODE (c901_ref_type, 40031, get_shippedset_count (c4021_ref_id),
							                 4000109, get_shippedset_count (c4021_ref_id), 0)
							, 0, 'N'
							, 'Y'
							 ) setshipped
				 FROM t4021_demand_mapping
				WHERE c4020_demand_master_id = p_demand_master_id
				  AND c901_ref_type IN (40030, 40031, 40034, 4000109)
				  AND c4021_ref_id IS NOT NULL
			 UNION
			 SELECT   0 mappingid, '' ID, 40034 ref_type, 'Item Consignment' NAME, NULL launchdt, NULL newdt
					, NULL histfl, 'N' setshipped
				 FROM DUAL
				WHERE 0 IN (SELECT COUNT (1)
							  FROM t4021_demand_mapping
							 WHERE c4020_demand_master_id = p_demand_master_id AND c901_ref_type = 40034)	--first time, there's no item consignment
				  AND 0 IN (SELECT DECODE (t4020.c901_demand_type, 40020, 1, 0)
							  FROM t4020_demand_master t4020
							 WHERE t4020.c4020_demand_master_id = p_demand_master_id)
			 ORDER BY ID;
	END gm_fch_setmap_for_launch;

	
	FUNCTION get_shippedset_count (
      p_set_id   IN   t207_set_master.c207_set_id%TYPE
   )
      RETURN NUMBER
   IS
      v_count   NUMBER;
   BEGIN
      BEGIN
         SELECT COUNT (1)
           INTO v_count
           FROM t504_consignment t504
          WHERE c207_set_id = p_set_id
            AND c504_status_fl = 4                                 --completed
            AND c504_void_fl IS NULL;
      EXCEPTION
         WHEN OTHERS
         THEN
            v_count := 0;
      END;

      RETURN v_count;
   END get_shippedset_count;
--
/***********************************************************
 * Purpose: Procedure to save the launch date
 ***********************************************************/
--
	PROCEDURE gm_sav_launchdates (
		p_sheetid	 IN   t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_inputstr	 IN   VARCHAR2
	  , p_userid	 IN   t4020_demand_master.c4020_created_by%TYPE
	)
	AS
		v_substring    VARCHAR2 (2000);
		v_strlen	   NUMBER := NVL (LENGTH (p_inputstr), 0);
		v_string	   VARCHAR2 (30000) := p_inputstr;
		v_refid 	   t4021_demand_mapping.c4021_ref_id%TYPE;
		v_reftype	   t4021_demand_mapping.c901_ref_type%TYPE;
		v_new_launchdt VARCHAR2 (10);
		v_new_launchdate DATE;
		v_old_launchdt DATE;
		v_mthdiff	   NUMBER;
		v_demand_sheet_id t4040_demand_sheet.c4040_demand_sheet_id%TYPE;

		CURSOR curr_group_parts
		IS
			SELECT c205_part_number_id pnum
			  FROM t4011_group_detail t4011, t4030_demand_growth_mapping t4030
			 WHERE t4011.c205_part_number_id = t4030.c4030_ref_id
			   AND c901_ref_type = 20295
			   AND c4010_group_id = v_refid
			   AND c4020_demand_master_id = p_sheetid
			   AND c4030_void_fl IS NULL;

		CURSOR curr_consign_parts
		IS
			SELECT c4030_ref_id pnum
			  FROM t4030_demand_growth_mapping t4030
			 WHERE c901_ref_type = 20298 AND c4020_demand_master_id = p_sheetid AND c4030_void_fl IS NULL;
	BEGIN
		IF v_strlen > 0
		THEN
			WHILE INSTR (v_string, '|') <> 0
			LOOP
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				--
				v_refid 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_reftype	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_new_launchdt := v_substring;

				--Fetch  the old launch date
				BEGIN
					SELECT c4021_launch_date, TO_DATE (v_new_launchdt, 'mm/dd/yyyy')
					  INTO v_old_launchdt, v_new_launchdate
					  FROM t4021_demand_mapping
					 WHERE c4020_demand_master_id = p_sheetid AND c901_ref_type = v_reftype AND c4021_ref_id = v_refid;
				EXCEPTION
					WHEN NO_DATA_FOUND
					THEN
						v_old_launchdt := NULL;

						SELECT TO_DATE (v_new_launchdt, 'mm/dd/yyyy')
						  INTO v_new_launchdate
						  FROM DUAL;
				END;

				--update the launch date to new launch date
				UPDATE t4021_demand_mapping
				   SET c4021_launch_date = v_new_launchdate
					 , c4021_last_updated_by = p_userid
					 , c4021_last_updated_date = SYSDATE
				 WHERE c4020_demand_master_id = p_sheetid AND c901_ref_type = v_reftype AND c4021_ref_id = v_refid;

				--If record not available for -9999 then insert for that sheet ID
				IF (SQL%ROWCOUNT = 0 AND v_refid = '-9999')
				THEN
					INSERT INTO t4021_demand_mapping
								(c4021_demand_mapping_id, c4020_demand_master_id, c901_ref_type, c4021_ref_id
							   , c4021_stack_month, c4021_launch_date, c4021_created_by, c4021_created_date
								)
						 VALUES (s4021_demand_mapping.NEXTVAL, p_sheetid, 40034, '-9999'
							   , 0, v_new_launchdate, p_userid, SYSDATE
								);
				END IF;

				-- chk if growth needs to be moved
				IF (v_old_launchdt IS NOT NULL)
				THEN
					SELECT MONTHS_BETWEEN (v_new_launchdate, v_old_launchdt)
					  INTO v_mthdiff
					  FROM DUAL;

					IF (v_mthdiff != 0 AND v_reftype != 40034)
					THEN
						--call prc to move the growth for set and group
						gm_pkg_oppr_sheet.gm_sav_move_growth (p_sheetid
														  , v_refid
														  , v_old_launchdt
														  , v_new_launchdate
														  , p_userid
														   );

						--move growth for group part details too
						IF v_reftype = 40030
						THEN
							FOR currindex IN curr_group_parts
							LOOP
								gm_pkg_oppr_sheet.gm_sav_move_growth (p_sheetid
																  , currindex.pnum
																  , v_old_launchdt
																  , v_new_launchdate
																  , p_userid
																   );
							END LOOP;
						END IF;
					END IF;

					IF (v_mthdiff != 0 AND v_reftype = 40034)	-- Item (-9999)
					THEN
						FOR currindex IN curr_consign_parts
						LOOP
							gm_pkg_oppr_sheet.gm_sav_move_growth (p_sheetid
															  , currindex.pnum
															  , v_old_launchdt
															  , v_new_launchdate
															  , p_userid
															   );
						END LOOP;
					END IF;
				END IF;
			END LOOP;

			-- reload the sheet
			BEGIN
				SELECT c4040_demand_sheet_id
				  INTO v_demand_sheet_id
				  FROM t4040_demand_sheet
				 WHERE c901_status = 50550	 -- open
				   AND c4040_demand_period_dt =
										 TO_DATE (TO_CHAR (SYSDATE, 'mm') || '/' || TO_CHAR (SYSDATE, 'YYYY')
												, 'mm/yyyy')
				   AND c4020_demand_master_id = p_sheetid
				   AND c4040_void_fl IS NULL;
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					v_demand_sheet_id := 0;
			END;
			
			--(To be fixed later)
			/* IF (v_demand_sheet_id != 0)
			THEN
				BEGIN
					gm_pkg_common_cancel.gm_cm_sav_cancelrow
														 (v_demand_sheet_id
														, 90710
														, 'EXEDS'
														, 'Reloading sheet automatically after moving the launch date'
														, p_userid
														 );
				EXCEPTION
					WHEN OTHERS
					THEN
						NULL;
				END;
			END IF;*/
		END IF;
	END gm_sav_launchdates;

/***********************************************************
 * Purpose: Procedure to move the growth
 ***********************************************************/
--
	PROCEDURE gm_sav_move_growth (
		p_sheetid		 IN   t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_refid 		 IN   t4021_demand_mapping.c4021_ref_id%TYPE
	  , v_old_launchdt	 IN   t4031_growth_details.c4031_start_date%TYPE
	  , v_new_launchdt	 IN   t4031_growth_details.c4031_end_date%TYPE
	  , p_userid		 IN   t4020_demand_master.c4020_created_by%TYPE
	)
	AS
		v_moveby	   NUMBER;
		v_abs_moveby   NUMBER;
		v_tot_recs	   NUMBER := 0;
		v_current_rowindex NUMBER := 0;
		v_demand_growth_id t4030_demand_growth_mapping.c4030_demand_growth_id%TYPE;
		v_growth_details_id t4031_growth_details.c4031_growth_details_id%TYPE;
		v_current_startdate t4031_growth_details.c4031_start_date%TYPE;
		v_start_date   t4031_growth_details.c4031_start_date%TYPE;
		v_end_date	   t4031_growth_details.c4031_end_date%TYPE;
		v_type		   t4030_demand_growth_mapping.c901_ref_type%TYPE;
		v_message	   VARCHAR2 (100);

		CURSOR cur_tmptbl_details
		IS
			SELECT	 TO_DATE (my_temp_txn_id, 'mm/dd/yyyy') sdate, my_temp_txn_key refvalue, my_temp_txn_value reftype
				FROM my_temp_key_value
			ORDER BY TO_DATE (my_temp_txn_id, 'mm/dd/yyyy');

		CURSOR cur_set_request_details
		IS
			SELECT c520_request_to reqto, c520_request_id reqid, c520_required_date reqdate, c901_ship_to shipto
				 , c520_ship_to_id shiptoid
			  FROM t520_request
			 WHERE c520_request_txn_id = p_sheetid
			   AND c901_request_source = 50616
			   AND c207_set_id = p_refid
			   AND c520_void_fl IS NULL
			   AND c520_status_fl < 40
			   AND c520_master_request_id IS NULL;

		CURSOR cur_item_request_details
		IS
			SELECT c520_request_to reqto, t520.c520_request_id reqid, c520_required_date reqdate, c901_ship_to shipto
				 , c520_ship_to_id shiptoid
			  FROM t520_request t520, t521_request_detail t521
			 WHERE t520.c520_request_id = t521.c520_request_id
			   AND t521.c205_part_number_id = p_refid
			   AND c207_set_id IS NULL
			   AND c520_void_fl IS NULL
			   AND c520_master_request_id IS NULL
			   AND c901_request_source = '50616'   --order planning
			   AND c520_request_to IS NULL
			   AND c520_status_fl < 40;
	BEGIN
		SELECT MONTHS_BETWEEN (v_new_launchdt, v_old_launchdt)
		  INTO v_moveby
		  FROM DUAL;

		DELETE FROM my_temp_key_value;

		INSERT INTO my_temp_key_value
			SELECT	 TO_CHAR (t4031.c4031_start_date, 'mm/dd/yyyy') sdate, t4031.c4031_value refvalue
				   , t4031.c901_ref_type reftype
				FROM t4030_demand_growth_mapping t4030, t4031_growth_details t4031
			   WHERE t4030.c4030_demand_growth_id = t4031.c4030_demand_growth_id
				 AND t4030.c4020_demand_master_id = p_sheetid
				 AND t4030.c4030_ref_id = p_refid
				 AND t4031.c4031_start_date >= v_old_launchdt
				 AND t4030.c4030_void_fl IS NULL
				 AND t4031.c4031_void_fl IS NULL
				 AND t4031.c4031_value IS NOT NULL
			ORDER BY t4031.c4031_start_date;

		SELECT COUNT (1), ABS (v_moveby)
		  INTO v_tot_recs, v_abs_moveby
		  FROM my_temp_key_value;

		IF (v_tot_recs != 0)
		THEN
			SELECT c4030_demand_growth_id, c901_ref_type
			  INTO v_demand_growth_id, v_type
			  FROM t4030_demand_growth_mapping t4030
			 WHERE t4030.c4020_demand_master_id = p_sheetid AND t4030.c4030_ref_id = p_refid AND c4030_void_fl IS NULL;

			FOR currindex IN cur_tmptbl_details
			LOOP
				v_current_rowindex := v_current_rowindex + 1;

				SELECT ADD_MONTHS (currindex.sdate, v_moveby), LAST_DAY (ADD_MONTHS (currindex.sdate, v_moveby))
				  INTO v_start_date, v_end_date
				  FROM DUAL;

				BEGIN
					SELECT c4031_growth_details_id
					  INTO v_growth_details_id
					  FROM t4031_growth_details
					 WHERE c4030_demand_growth_id = v_demand_growth_id
					   AND c4031_start_date = v_start_date
					   AND c4031_void_fl IS NULL;
				EXCEPTION
					WHEN NO_DATA_FOUND
					THEN
						v_growth_details_id := 0;
				END;

				--		Update the growth value
				UPDATE t4031_growth_details
				   SET c4031_value = currindex.refvalue
					 , c901_ref_type = currindex.reftype
					 , c4031_last_updated_by = p_userid
					 , c4031_last_updated_date = SYSDATE
				 WHERE c4031_start_date = v_start_date
				   AND c4030_demand_growth_id = v_demand_growth_id
				   AND c4031_void_fl IS NULL;

				IF (SQL%ROWCOUNT = 0)
				THEN
					SELECT s4031_growth.NEXTVAL
					  INTO v_growth_details_id
					  FROM DUAL;

					INSERT INTO t4031_growth_details
								(c4031_growth_details_id, c4031_value, c901_ref_type, c4031_start_date, c4031_end_date
							   , c4031_created_by, c4031_created_date, c4030_demand_growth_id
								)
						 VALUES (v_growth_details_id, currindex.refvalue, currindex.reftype, v_start_date, v_end_date
							   , p_userid, SYSDATE, v_demand_growth_id
								);
				END IF;

				-- enter comments manually
				gm_update_log (v_growth_details_id
							 , 'Moving growth as launch date moved from ' || v_old_launchdt || ' to ' || v_new_launchdt
							 , 1224
							 , p_userid
							 , v_message
							  );

				--If moving fwd then for first v_moveby months make value as null
				IF v_moveby > 0
				THEN
					IF v_current_rowindex <= v_moveby
					THEN
						SELECT c4031_growth_details_id
						  INTO v_growth_details_id
						  FROM t4031_growth_details
						 WHERE c4030_demand_growth_id = v_demand_growth_id
						   AND c4031_start_date = currindex.sdate
						   AND c4031_void_fl IS NULL;

						UPDATE t4031_growth_details
						   SET c4031_value = 0
							 , c901_ref_type = currindex.reftype
							 , c4031_last_updated_by = p_userid
							 , c4031_last_updated_date = SYSDATE
						 WHERE c4031_start_date = currindex.sdate
						   AND c4030_demand_growth_id = v_demand_growth_id
						   AND c4031_void_fl IS NULL;

						-- enter comments manually
						gm_update_log (v_growth_details_id
									 ,	  'Moving growth as launch date moved from '
									   || v_old_launchdt
									   || ' to '
									   || v_new_launchdt
									 , 1224
									 , p_userid
									 , v_message
									  );
					END IF;
				ELSIF v_moveby < 0
				THEN   -- If moving backword then for last v_moveby months make value as null
					IF (v_tot_recs - v_current_rowindex) < v_abs_moveby
					THEN
						SELECT c4031_growth_details_id
						  INTO v_growth_details_id
						  FROM t4031_growth_details
						 WHERE c4030_demand_growth_id = v_demand_growth_id
						   AND c4031_start_date = currindex.sdate
						   AND c4031_void_fl IS NULL;

						UPDATE t4031_growth_details
						   SET c4031_value = 0
							 , c901_ref_type = currindex.reftype
							 , c4031_last_updated_by = p_userid
							 , c4031_last_updated_date = SYSDATE
						 WHERE c4031_start_date = currindex.sdate
						   AND c4030_demand_growth_id = v_demand_growth_id
						   AND c4031_void_fl IS NULL;

						-- enter comments manually
						gm_update_log (v_growth_details_id
									 ,	  'Moving growth as launch date moved from '
									   || v_old_launchdt
									   || ' to '
									   || v_new_launchdt
									 , 1224
									 , p_userid
									 , v_message
									  );
					END IF;
				END IF;
			END LOOP;
			--(To be fixed later)
			--	if type set/item Update C520_REQUIRED_DATE
			/* IF v_type = '20296'   -- set
			THEN
				FOR currindex IN cur_set_request_details
				LOOP
					--gm_pkg_op_request_master.gm_sav_request_edit_info (TO_CHAR (ADD_MONTHS (currindex.reqdate, v_moveby)
					--														  , 'mm/dd/yyyy'
					--														   )
					--												 , currindex.reqto
					--												 , p_userid
					--												 , currindex.reqid
					--												 , currindex.shipto
					--												 , currindex.shiptoid
					--												  );
				END LOOP;
			END IF;

			IF v_type = '20298'   -- item
			THEN
				FOR currindex IN cur_item_request_details
				LOOP
					gm_pkg_op_request_master.gm_sav_request_edit_info (TO_CHAR (ADD_MONTHS (currindex.reqdate, v_moveby)
																			  , 'mm/dd/yyyy'
																			   )
																	 , currindex.reqto
																	 , p_userid
																	 , currindex.reqid
																	 , currindex.shipto
																	 , currindex.shiptoid
																	  );
				END LOOP;
			END IF;*/
		END IF;
	END gm_sav_move_growth;

--
/***********************************************************
 * Purpose: Function to get the launch date when demandsheetid , refid and reftype is passed
 ***********************************************************/
--
	FUNCTION get_launch_date (
		p_sheetid	IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_refid 	IN	 t4021_demand_mapping.c4021_ref_id%TYPE
	  , p_reftype	IN	 t4021_demand_mapping.c901_ref_type%TYPE
	)
		RETURN DATE
	IS
		v_date		   DATE;
		v_reftype	   t4021_demand_mapping.c901_ref_type%TYPE;
		v_refid 	   t4021_demand_mapping.c4021_ref_id%TYPE;
	BEGIN
		v_refid 	:= p_refid;

		IF p_reftype = '20296'	 --setid
		THEN
			v_reftype	:= 40031;
		ELSIF p_reftype = '20297'	--group id
		THEN
			v_reftype	:= 40030;
		ELSIF p_reftype = '20298'	--Part # - Consignment
		THEN
			v_reftype	:= 40034;
			v_refid 	:= '-9999';
		ELSIF p_reftype = '20295'
		-- Part # - Group
		THEN
			-- fetch groupid
			v_reftype	:= 40030;

			BEGIN
				SELECT c4010_group_id
				  INTO v_refid
				  FROM t4011_group_detail t4011, t4021_demand_mapping t4021
				 WHERE c205_part_number_id = p_refid
				   AND c4020_demand_master_id = p_sheetid
				   AND t4011.c4010_group_id = t4021.c4021_ref_id;
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					v_refid 	:= p_refid;
			END;
		END IF;

		BEGIN
			SELECT c4021_launch_date
			  INTO v_date
			  FROM t4021_demand_mapping
			 WHERE c4020_demand_master_id = p_sheetid AND c901_ref_type = v_reftype AND c4021_ref_id = v_refid;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN NULL;
		END;

		RETURN v_date;
	END get_launch_date;


	 /******************************************************************
	* Description : Function to get part_pricing_type
	****************************************************************/
	FUNCTION get_pricing_type (
		p_pnum					  IN   t4011_group_detail.c205_part_number_id%TYPE
	  , p_primary_part_inputstr   IN   VARCHAR2
	)
		RETURN NUMBER
	IS
/*	Description 	: THIS FUNCTION RETURNS part pricing type in group detail table
 Parameters 		: p_pnum, p_inputstr --- ex 101.109^52081|101.111^52080
*/
		v_strlen_primary_part NUMBER := NVL (LENGTH (p_primary_part_inputstr), 0);
		v_primary_part_string VARCHAR2 (4000) := p_primary_part_inputstr;
		v_primary_part_substring VARCHAR2 (1000);
		v_groupid	   t4010_group.c4010_group_id%TYPE;
		v_pnum		   t4011_group_detail.c205_part_number_id%TYPE;
		v_part_type    t4011_group_detail.c901_part_pricing_type%TYPE;
		v_part_primary_lock t4011_group_detail.c4011_primary_part_lock_fl%TYPE;
		v_pricing_type NUMBER;
	BEGIN
		IF v_strlen_primary_part > 0
		THEN
			WHILE INSTR (v_primary_part_string, '|') <> 0
			LOOP
				--
				v_primary_part_substring := SUBSTR (v_primary_part_string, 1, INSTR (v_primary_part_string, '|') - 1);
				v_primary_part_string := SUBSTR (v_primary_part_string, INSTR (v_primary_part_string, '|') + 1);
				--
				v_groupid	:= NULL;
				v_pnum		:= NULL;
				v_part_type := NULL;
				v_part_primary_lock := NULL;
				--
				v_groupid	:= SUBSTR (v_primary_part_substring, 1, INSTR (v_primary_part_substring, '^') - 1);
				v_primary_part_substring := SUBSTR (v_primary_part_substring, INSTR (v_primary_part_substring, '^') + 1);
				v_pnum		:= SUBSTR (v_primary_part_substring, 1, INSTR (v_primary_part_substring, '^') - 1);
				v_primary_part_substring := SUBSTR (v_primary_part_substring, INSTR (v_primary_part_substring, '^') + 1);
				v_part_type := SUBSTR (v_primary_part_substring, 1, INSTR (v_primary_part_substring, '^') - 1);
				v_primary_part_substring := SUBSTR (v_primary_part_substring, INSTR (v_primary_part_substring, '^') + 1);
				v_part_primary_lock := v_primary_part_substring;

				--check to see if part will be changed from primary to secondary
				IF (p_pnum = v_pnum)
				THEN
					RETURN v_part_type;
				END IF;
			END LOOP;

			RETURN 52080;
		ELSE
			RETURN 52080;
		END IF;
	END get_pricing_type;


	/********************************************************************
		* Description : Function to get part list price in the group
		******************************************************************/
	FUNCTION get_primary_part_groupid (
		p_pnum	 IN   t4011_group_detail.c205_part_number_id%TYPE
	)
		RETURN NUMBER
	IS
		v_groupid	   NUMBER;
	BEGIN
		BEGIN
			SELECT t4010.c4010_group_id
			  INTO v_groupid
			  FROM t4011_group_detail t4011, t4010_group t4010
			 WHERE  T4010.C4010_GROUP_ID = t4011.C4010_GROUP_ID AND c205_part_number_id = p_pnum AND c901_part_pricing_type = 52080 AND T4010.C4010_VOID_FL IS NULL;
		--list price
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN 0;
		END;

		RETURN v_groupid;
	END get_primary_part_groupid;

	/************************************************************************************************************
	 Description	: THIS FUNCTION RETURNS part primary lock flag from the input string for a given part number
	 Parameters 		: p_pnum, p_inputstr --- ex 101.109^52081^Y|101.111^52080^N
	**************************************************************************************************************/
	FUNCTION get_primary_lock_from_input (
		p_pnum					  IN   t4011_group_detail.c205_part_number_id%TYPE
	  , p_primary_part_inputstr   IN   VARCHAR2
	)
		RETURN VARCHAR2
	IS
		v_strlen_primary_part NUMBER := NVL (LENGTH (p_primary_part_inputstr), 0);
		v_primary_part_string VARCHAR2 (4000) := p_primary_part_inputstr;
		v_primary_part_substring VARCHAR2 (1000);
		v_groupid	   t4010_group.c4010_group_id%TYPE;
		v_pnum		   t4011_group_detail.c205_part_number_id%TYPE;
		v_part_type    t4011_group_detail.c901_part_pricing_type%TYPE;
		v_part_primary_lock t4011_group_detail.c4011_primary_part_lock_fl%TYPE;
		v_pricing_type NUMBER;
	BEGIN
		IF v_strlen_primary_part > 0
		THEN
			WHILE INSTR (v_primary_part_string, '|') <> 0
			LOOP
				--
				v_primary_part_substring := SUBSTR (v_primary_part_string, 1, INSTR (v_primary_part_string, '|') - 1);
				v_primary_part_string := SUBSTR (v_primary_part_string, INSTR (v_primary_part_string, '|') + 1);
				--
				v_groupid	:= NULL;
				v_pnum		:= NULL;
				v_part_type := NULL;
				v_part_primary_lock := NULL;
				--
				v_groupid	:= SUBSTR (v_primary_part_substring, 1, INSTR (v_primary_part_substring, '^') - 1);
				v_primary_part_substring := SUBSTR (v_primary_part_substring, INSTR (v_primary_part_substring, '^') + 1);
				v_pnum		:= SUBSTR (v_primary_part_substring, 1, INSTR (v_primary_part_substring, '^') - 1);
				v_primary_part_substring := SUBSTR (v_primary_part_substring, INSTR (v_primary_part_substring, '^') + 1);
				v_part_type := SUBSTR (v_primary_part_substring, 1, INSTR (v_primary_part_substring, '^') - 1);
				v_primary_part_substring := SUBSTR (v_primary_part_substring, INSTR (v_primary_part_substring, '^') + 1);
				v_part_primary_lock := v_primary_part_substring;

				--check to see if part will be changed from primary to secondary
				IF (p_pnum = v_pnum)
				THEN
					RETURN v_part_primary_lock;
				END IF;
			END LOOP;

			RETURN 'Y';
		ELSE
			RETURN 'Y';
		END IF;
	END get_primary_lock_from_input;
	--
	/*******************************************************
	* Purpose: This Procedure is used to inactivate demand sheet
	*******************************************************/
	--
	PROCEDURE gm_sav_inactivate_demsheet (
	        p_sheet_id    IN t4020_demand_master.c4020_demand_master_id%TYPE,
	        p_inactive_fl IN t4020_demand_master.c4020_inactive_fl%TYPE,
	        p_user_id     IN t4020_demand_master.c4020_last_updated_by%TYPE,
	        p_comments	  IN t902_log.c902_comments%TYPE)
	AS
	    v_demand_type 	t4020_demand_master.c901_demand_type%TYPE;
	    v_templt_id 	t4020_demand_master.c4020_parent_demand_master_id%TYPE;
	    v_global_map_id t4020_demand_master.c4015_global_sheet_map_id%TYPE;
	    v_comp_id 	    t4020_demand_master.c901_company_id%TYPE;
	    v_level_id 		t4015_global_sheet_mapping.c901_level_id%TYPE;
	    v_level_value 	t4015_global_sheet_mapping.c901_level_value%TYPE;
		v_msg 			VARCHAR2(100);	    
	BEGIN
		
		BEGIN
		     SELECT c901_demand_type, c4020_parent_demand_master_id, c4015_global_sheet_map_id, c901_company_id
		       INTO v_demand_type, v_templt_id, v_global_map_id, v_comp_id 
		       FROM t4020_demand_master
		      WHERE c4020_void_fl         IS NULL
		        AND c4020_demand_master_id = p_sheet_id;
		        
		     SELECT c901_level_id, c901_level_value
		       INTO v_level_id, v_level_value
		       FROM t4015_global_sheet_mapping
		      WHERE c4015_void_fl            IS NULL
		        AND c4015_global_sheet_map_id = v_global_map_id;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				RETURN;
		END; 
		
	    IF v_level_id = 102584 THEN --102584-> Region
	     	--Create the comments for the demand sheet at Region level
			IF p_comments IS NOT NULL 
			THEN
				gm_update_log(p_sheet_id, p_comments,'1227', p_user_id, v_msg);-- 1227 - DemandSheet Log type				
			END IF;	        
	    	RETURN;
	    ELSE
			gm_sav_inactvt_aggrgt_sheet(v_templt_id, v_demand_type, v_comp_id, v_level_id, v_level_value,p_inactive_fl,p_user_id,p_comments);
	    END IF;
	END gm_sav_inactivate_demsheet;
	--
	/*******************************************************
	* Purpose: This Procedure is used to inactivate aggregative demand sheet,
	* it will call recursively to inactivate the sub aggregative sheets
	*******************************************************/
	--
	PROCEDURE gm_sav_inactvt_aggrgt_sheet (
	        p_templt_id   IN t4020_demand_master.c4020_parent_demand_master_id%TYPE,
	        p_demand_type IN t4020_demand_master.c901_demand_type%TYPE,
	        p_comp_id 	  IN t4020_demand_master.c901_company_id%TYPE,
	        p_level_id    IN t4015_global_sheet_mapping.c901_level_id%TYPE,
	        p_level_value IN t4015_global_sheet_mapping.c901_level_value%TYPE,
	        p_inactive_fl IN t4020_demand_master.c4020_inactive_fl%TYPE,
	        p_user_id     IN t4020_demand_master.c4020_last_updated_by%TYPE,
	        p_comments	  IN t902_log.c902_comments%TYPE)
	AS
	    CURSOR cur_div
	    IS
	         SELECT DISTINCT compid, divid levelvalue
	           FROM v700_territory_mapping_detail
	          WHERE compid      = p_level_value
	            AND divid IS NOT NULL;
	    CURSOR cur_zone
	    IS
	         SELECT DISTINCT compid, gp_id levelvalue
	           FROM v700_territory_mapping_detail
	          WHERE divid       = p_level_value
	            AND compid      = p_comp_id
	            AND gp_id IS NOT NULL;
	    CURSOR cur_region
	    IS
	         SELECT DISTINCT compid, region_id levelvalue
	           FROM v700_territory_mapping_detail
	          WHERE gp_id       = p_level_value
	          	AND compid      = p_comp_id
	            AND region_id IS NOT NULL;
	    CURSOR cur_intermed_zone
	    IS
	         SELECT DISTINCT compid, gp_id levelvalue
			   FROM V700_Territory_Mapping_Detail
			  WHERE gp_id    IS NOT NULL
			    AND compid    = p_comp_id
				AND divid     = 100824  -- OUS DIVISION
				AND ( 
						(p_level_value = 102603 AND Gp_Id IN ( -- 102603 OUS DIRECT 
													SELECT C906_Rule_Value
													  FROM T906_Rules
													 WHERE C906_Rule_Grp_Id = 'OUS_DIRECT_ZONES')
						)
						OR 
						(p_level_value = 102604 AND Gp_Id NOT IN( --102604 OUS DISTRIBUTOR
													SELECT C906_Rule_Value
													  FROM T906_Rules
													 WHERE C906_Rule_Grp_Id = 'OUS_DIRECT_ZONES')
						)
					); 	            
	    CURSOR cur_intermed 
	    IS
	    	 SELECT c901_code_id levelvalue
			   FROM t901_code_lookup
			  WHERE c901_code_grp  = 'LVLINT'
			    AND c901_active_fl = 1;
	BEGIN
		
	    IF p_level_id = 102584 THEN --102584-> Region
	        RETURN;
	    ELSIF p_level_id = 102583 THEN --102583 -> Zone
	   		FOR v_cur IN cur_region
	        LOOP
	        	gm_sav_inactivate_sheet(p_templt_id, p_demand_type, v_cur.compid,v_cur.levelvalue, p_inactive_fl, p_user_id, p_comments);
	        END LOOP;
	   		gm_sav_inactivate_sheet(p_templt_id, p_demand_type, p_comp_id,p_level_value, p_inactive_fl, p_user_id, p_comments);
	    ELSIF p_level_id = 102582  THEN --102582  -> Intermediate 
	   		FOR v_cur IN cur_intermed_zone
	        LOOP
	        	gm_sav_inactvt_aggrgt_sheet(p_templt_id, p_demand_type,v_cur.compid,102583,v_cur.levelvalue,p_inactive_fl, p_user_id, p_comments);--102583 -> Zone
	        END LOOP;
	        gm_sav_inactivate_sheet(p_templt_id, p_demand_type, p_comp_id,p_level_value, p_inactive_fl, p_user_id, p_comments);
	   	ELSIF p_level_id = 102581 THEN --102581 -> Division
	   		IF p_level_value = 100824 THEN -- OUS DIVISION
				FOR v_cur IN cur_intermed
		        LOOP
		        	gm_sav_inactvt_aggrgt_sheet(p_templt_id, p_demand_type,p_comp_id,102582,v_cur.levelvalue,p_inactive_fl, p_user_id, p_comments);--102582  -> Intermediate 
		        END LOOP;	   		
	   		ELSE
		   		FOR v_cur IN cur_zone
		        LOOP
		        	gm_sav_inactvt_aggrgt_sheet(p_templt_id, p_demand_type,v_cur.compid,102583,v_cur.levelvalue,p_inactive_fl, p_user_id, p_comments);--102583 -> Zone
		        END LOOP;
	        END IF;
	   		gm_sav_inactivate_sheet(p_templt_id, p_demand_type, p_comp_id,p_level_value, p_inactive_fl, p_user_id, p_comments);
	   ELSIF p_level_id = 102580 THEN --102580 -> Company
	   		FOR v_cur IN cur_div
	        LOOP
	        	gm_sav_inactvt_aggrgt_sheet(p_templt_id, p_demand_type,v_cur.compid,102581,v_cur.levelvalue,p_inactive_fl, p_user_id, p_comments); --102581 -> Division
	        END LOOP;
	   		gm_sav_inactivate_sheet(p_templt_id, p_demand_type, p_comp_id,p_level_value, p_inactive_fl, p_user_id, p_comments);
	   END IF;
	END gm_sav_inactvt_aggrgt_sheet;
	--
	/*******************************************************
	* Purpose: This Procedure is used to inactivate the demand sheet
	*******************************************************/
	--
	PROCEDURE gm_sav_inactivate_sheet(
	        p_templt_id   IN t4020_demand_master.c4020_parent_demand_master_id%TYPE,
	        p_demand_type IN t4020_demand_master.c901_demand_type%TYPE,
	        p_comp_id 	  IN t4020_demand_master.c901_company_id%TYPE,
	        p_level_value IN t4015_global_sheet_mapping.c901_level_value%TYPE,
	        p_inactive_fl IN t4020_demand_master.c4020_inactive_fl%TYPE,
	        p_user_id     IN t4020_demand_master.c4020_last_updated_by%TYPE,
	        p_comments	  IN t902_log.c902_comments%TYPE)
	AS
	    v_global_map_id t4020_demand_master.c4015_global_sheet_map_id%TYPE;
	    v_sheet_id      t4020_demand_master.c4020_demand_master_id%TYPE;	
	    v_msg  VARCHAR2(100);
	BEGIN
		BEGIN
		         SELECT c4015_global_sheet_map_id
		           INTO v_global_map_id
		           FROM t4015_global_sheet_mapping
		          WHERE c4015_void_fl   IS NULL
		            AND c901_level_value = p_level_value
		            AND c901_company_id  = p_comp_id;
		            
		         SELECT c4020_demand_master_id
		           INTO v_sheet_id
		           FROM t4020_demand_master
		          WHERE c4020_void_fl                IS NULL
		            AND c901_demand_type              = p_demand_type
		            AND c4020_parent_demand_master_id = p_templt_id
		            AND c4015_global_sheet_map_id     = v_global_map_id;
				EXCEPTION WHEN NO_DATA_FOUND THEN
					RETURN;
			END;
			
	         UPDATE t4020_demand_master
	            SET c4020_inactive_fl       = p_inactive_fl
	              , c4020_last_updated_by   = p_user_id
	              , c4020_last_updated_date = SYSDATE
	          WHERE c4020_demand_master_id  = v_sheet_id;

	        --Create the comments for the demand sheet
			IF p_comments IS NOT NULL 
			THEN
				gm_update_log(v_sheet_id,p_comments,'1227',p_user_id,v_msg);-- 1227 - DemandSheet Log type				
			END IF;	          
	END gm_sav_inactivate_sheet;
 --
/*******************************************************
 *		 Purpose: function is used to fetch the demand inactive flag given the demand id
 *******************************************************/
--
	FUNCTION get_demand_inactive_fl(
		p_demandsheetid   IN   t4020_demand_master.c4020_demand_master_id%TYPE
	)
		RETURN  VARCHAR2 
	IS
--
		v_inactive_fl  VARCHAR2(1);
	BEGIN
		
		SELECT c4020_inactive_fl
		  INTO v_inactive_fl
		  FROM t4020_demand_master
		 WHERE c4020_demand_master_id = p_demandsheetid;

		RETURN v_inactive_fl;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN NULL;
	END get_demand_inactive_fl;	
	
--
/*******************************************************
 * Purpose: Procedure is used to save the crossover parts
			of the given sheets in to Multipart Sheet
 * Author: Agilan Singaravel		
 *******************************************************/
--
	PROCEDURE gm_sync_mutipart_dtls_by_demand_sheet_ids (
		p_demand_sheetids		IN	 VARCHAR2
	  , p_userid				IN	 t4045_demand_sheet_assoc.c4045_last_updated_by%TYPE
	  , p_ignore_ttp_summary	IN   VARCHAR 
	  , p_load_date				IN 	 DATE
	  , p_inventory_id			IN 	 t250_inventory_lock.c250_inventory_lock_id%TYPE
	)
	AS
		v_multi_sheet_id t4040_demand_sheet.c4040_demand_sheet_id%TYPE;
		v_inventory_id t4040_demand_sheet.c250_inventory_lock_id%TYPE;
		v_multi_sheet_status t4040_demand_sheet.c901_status%TYPE;
		v_comp_id t4020_demand_master.c901_company_id%TYPE;
		v_level_value t4040_demand_sheet.c901_level_value%TYPE;
		v_ttp_detail_id	t4040_demand_sheet.c4052_ttp_detail_id%TYPE;
		v_mp_ttp_detail_id	t4040_demand_sheet.c4052_ttp_detail_id%TYPE;
		v_div_id	  t1910_division.c1910_division_id%TYPE;
		v_div_map_id	  t1910_division.c1910_division_id%TYPE;
		v_status NUMBER;
		v_demand_sheetids VARCHAR2(500);

		CURSOR cur_crossover_part
		IS
			SELECT c205_part_number_id pnum, c4042_period period, c901_type fctype, SUM(c4042_qty) qty
			FROM ts4042_ttp_multipart_month
			WHERE c205_part_number_id IN (SELECT DISTINCT c205_part_number_id FROM ts4042_ttp_multipart_month 
			WHERE c4042_upd_flag='Y' AND c4052_ttp_detail_id=v_ttp_detail_id)
			GROUP BY c205_part_number_id, c4042_period, c901_type;  
			 
			   
	BEGIN
				
		my_context.set_my_inlist_ctx (p_demand_sheetids);
		v_demand_sheetids :=p_demand_sheetids;
				
		BEGIN
 					
 				BEGIN
	 			
	 				-- To get the company id for demand sheet ids
					SELECT max(t4020.c901_company_id),max(c4052_ttp_detail_id)
		 				into v_comp_id ,v_ttp_detail_id
						FROM t4020_demand_master t4020, t4040_demand_sheet t4040
		 				WHERE t4020.c4020_demand_master_id=t4040.c4020_demand_master_id
		 				AND c4040_demand_period_dt =TRUNC (p_load_date, 'MM')
		 				AND t4040.c4040_void_fl	IS NULL	
		                AND c4040_demand_sheet_id IN (SELECT * FROM v_in_list);
 			  
		                		                
                EXCEPTION WHEN OTHERS
          		THEN
          			v_comp_id := '100800';
          		END; 
          		
          			BEGIN
		          		
		          		 SELECT c906_rule_value INTO v_level_value
							FROM t906_rules t906
							WHERE c906_rule_id =v_comp_id
							AND t906.c906_rule_grp_id ='GOP_MP_SHEET_LOCK'
		                    AND t906.c906_void_fl IS NULL; 
  				
		            EXCEPTION WHEN OTHERS
	          		THEN
	          			v_comp_id := '100800';
	          		END; 
			END;
		
			-- PMT-57165 Exception we are hard coding as MP spine 
		BEGIN
			--c901_level_value Multi Part Trauma demand sheet only has level value
			SELECT	   t4040.c4040_demand_sheet_id, t4040.c250_inventory_lock_id, t4040.c901_status,c4052_ttp_detail_id
				  INTO v_multi_sheet_id, v_inventory_id, v_multi_sheet_status,v_mp_ttp_detail_id
				  FROM t4040_demand_sheet t4040
				 WHERE t4040.c901_demand_type = '40023'
				  AND NVL(t4040.c901_level_value,'1') = NVL(v_level_value,'1')
				  AND t4040.c250_inventory_lock_id = p_inventory_id
				  AND t4040.c4040_void_fl	IS NULL		   
			FOR UPDATE;
		 
			IF v_multi_sheet_status <> 50550
			THEN
				RETURN;
			END IF;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				raise_application_error ('-20099', '');
		END;

		
		gm_pkg_oppr_ld_multipart.gm_op_multipart_fetch_qty_by_ttp(v_demand_sheetids,v_multi_sheet_id);
		
		FOR cur_val IN cur_crossover_part
		LOOP
			gm_op_sav_multipart_detail (v_multi_sheet_id, cur_val.pnum, cur_val.qty, cur_val.period, p_userid, cur_val.fctype);
			
		END LOOP;
		
							
		UPDATE ts4042_ttp_multipart_month SET c4042_upd_flag=NULL
		WHERE c4042_upd_flag='Y'
		AND c4052_ttp_detail_id=v_ttp_detail_id; 
				
			 --TTP SUmmary should be reloaded when the individual TTP is Lock &Generated.
			 IF p_ignore_ttp_summary ='N'
			 THEN
				-- TTP Summary Load
			 	gm_pkg_oppr_ld_summary.gm_op_ld_summary_main(v_mp_ttp_detail_id,v_multi_sheet_id,p_userid);
			END IF;
		
		gm_pkg_oppr_ld_demand.gm_sav_vendor_sync_load(v_mp_ttp_detail_id, p_userid ,109021) ;
		
	END gm_sync_mutipart_dtls_by_demand_sheet_ids;
	
END gm_pkg_oppr_sheet;
/
