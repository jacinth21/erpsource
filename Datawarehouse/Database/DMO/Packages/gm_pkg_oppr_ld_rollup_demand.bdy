/* Formatted on 2011/06/09 12:16 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\purchasing\gm_pkg_oppr_ld_rollup_demand.bdy";
-- show err  Package Body gm_pkg_oppr_ld_sales_demand
-- exec gm_pkg_oppr_ld_demand.gm_op_ld_demand_main(17);
/*
 DELETE FROM T4042_DEMAND_SHEET_DETAIL;
 DELETE FROM T4041_DEMAND_SHEET_MAPPING;
 DELETE FROM T4040_DEMAND_SHEET;
 COMMIT ;
 exec gm_pkg_oppr_ld_demand.gm_op_ld_demand_main(5009);

*/

CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ld_rollup_demand
IS
	/*************************************************************************
	 * Purpose: Procedure used to load sales demand, calculated value and
	 *	Forecast information
	 *************************************************************************/
	PROCEDURE gm_op_ld_main (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_demand_period 	IN	 t4020_demand_master.c4020_demand_period%TYPE
	  , p_forecast_period	IN	 t4020_demand_master.c4020_forecast_period%TYPE
	  , p_inc_current_fl	IN	 t4020_demand_master.c4020_include_current_fl%TYPE
      , p_inventory_id      IN  t250_inventory_lock.c250_inventory_lock_id%TYPE
	)
	AS
	    --
		v_from_period  DATE;
		v_to_period    DATE;
        --
        v_level_id                   t4015_global_sheet_mapping.c901_level_id%TYPE;
        v_level_value                t4015_global_sheet_mapping.c901_level_value%TYPE;
        v_demand_sheet_type          t4020_demand_master.c901_demand_type%TYPE;
        v_parent_demand_master_id    t4020_demand_master.c4020_parent_demand_master_id%TYPE; 
	--
	BEGIN
	   -- To get sheet related master information  
       SELECT t4015.c901_level_id, t4015.c901_level_value, t4020.c901_demand_type
              , t4020.c4020_parent_demand_master_id    
         INTO v_level_id, v_level_value, v_demand_sheet_type,v_parent_demand_master_id  
         FROM t4015_global_sheet_mapping t4015, t4020_demand_master t4020
        WHERE t4020.c4020_demand_master_id = p_demand_id
          AND t4015.c4015_global_sheet_map_id = t4020.c4015_global_sheet_map_id; 

        --
		-- Below code to fetch demand (from and to period)
		gm_pkg_oppr_ld_demand.gm_op_calc_period (p_demand_period, p_inc_current_fl, 'D', v_from_period, v_to_period);
		--
		--DBMS_OUTPUT.put_line ('************ Before calling load sheet info - ' || p_demand_id|| ' *** ' || p_demand_sheet_id || ' **** ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
		
        -- Below code to load required account info
        gm_pkg_oppr_ld_rollup_demand.gm_op_ld_sheetinfo(p_demand_id, p_demand_sheet_id, v_level_id, v_level_value, v_demand_sheet_type , v_parent_demand_master_id);
        gm_pkg_oppr_ld_rollup_demand.gm_op_sav_sheet_assoc_map(p_demand_id, p_demand_sheet_id);
        gm_pkg_oppr_ld_rollup_demand.gm_op_sav_sheet_growth(p_demand_sheet_id);
        
		-- Below procedure called to load sales demand information
        --DBMS_OUTPUT.put_line ('************ load sheet 1' || ' - ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
		gm_pkg_oppr_ld_rollup_demand.gm_op_ld_demand (p_demand_sheet_id);
		--
		-- Below code to fetch forecast (from and to period)
		gm_pkg_oppr_ld_demand.gm_op_calc_period (p_forecast_period, p_inc_current_fl, 'F', v_from_period, v_to_period);
        --DBMS_OUTPUT.put_line ('************ load sheet 2' || ' - ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
        
		-- Below procedure called to load sales Weighted and Avg sales information
		gm_pkg_oppr_ld_demand.gm_op_ld_demand_calc_avg (p_demand_sheet_id, v_from_period);
                              
		--
		-- Below procedure called to load forecast information
		--DBMS_OUTPUT.put_line ('************ load sheet 3' || ' - ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
		gm_pkg_oppr_ld_rollup_demand.gm_op_ld_forecast (p_demand_id, p_demand_sheet_id, v_from_period, v_to_period, 50563);
		--
		--DBMS_OUTPUT.put_line ('************ load sheet 3.1' || ' - ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
        -- Below procedure called to load sales Weighted and Avg sales information
		gm_pkg_oppr_ld_demand.gm_op_ld_forecast_avg (p_demand_sheet_id, v_from_period);
		--
		--DBMS_OUTPUT.put_line ('************ load sheet 4' || ' - ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));

		gm_pkg_oppr_ld_demand.gm_op_ld_forecast_variance (p_demand_sheet_id, v_from_period);
		--DBMS_OUTPUT.put_line ('************ load sheet 5' || ' - ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
        gm_pkg_oppr_ld_rollup_demand.gm_op_ld_otn(p_demand_sheet_id,4000112);
        
        -- Loading back order from other sheet is not required because of TPR 
        -- Need to be revisited 
        gm_pkg_oppr_ld_rollup_demand.gm_op_ld_bo(p_demand_sheet_id);
        
        -- Par Setup applicable for Company level sheet -- World Wide sheet  
        IF (v_level_id = 102580) 
        THEN 
            --40020 Sales Par 
            IF (v_demand_sheet_type =  40020) 
            THEN 
        		-- Below procedure called to load Par Information [To be moved to World wide sheet]
        		gm_pkg_oppr_ld_rollup_demand.gm_op_ld_sales_par (p_demand_id, p_demand_sheet_id, v_from_period);
                
            ELSE 
        		-- Below procedure called to load Par Information [To be moved to World wide sheet]
        		gm_pkg_oppr_ld_rollup_demand.gm_op_ld_consign_par (p_demand_id, p_demand_sheet_id, v_from_period);
                
                --Below code to load Pending Requests
                gm_pkg_oppr_ld_consign_demand.gm_op_ld_pending_request (p_demand_id, p_demand_sheet_id, v_from_period
                            , p_inventory_id) ;
            END IF;
            
            -- Load sub component information 
            gm_pkg_oppr_ld_rollup_demand.gm_op_ld_sub_components (p_demand_sheet_id);

            -- ************************************************************************
            -- To load OUS Forecast information (102581) will load division level sheet
            -- ************************************************************************  
			--DBMS_OUTPUT.put_line ('************ load sheet 5.1' || ' - ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
            gm_pkg_oppr_ld_rollup_demand.gm_op_ld_sheetinfo(p_demand_id, p_demand_sheet_id, 102581, NULL, v_demand_sheet_type , v_parent_demand_master_id);
            
            gm_pkg_oppr_ld_rollup_demand.gm_op_ld_otn(p_demand_sheet_id,4000120);    
            
            gm_pkg_oppr_ld_rollup_demand.gm_op_ld_forecast (p_demand_id, p_demand_sheet_id, v_from_period, v_to_period, 4000111);
            
            -- ************************************************************************
            -- To load US Sheet information (-11111 will only load US Sheet
            -- ************************************************************************               
			--DBMS_OUTPUT.put_line ('************ load sheet 5.2' || ' - ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
            gm_pkg_oppr_ld_rollup_demand.gm_op_ld_sheetinfo(p_demand_id, p_demand_sheet_id, -11111, NULL, v_demand_sheet_type , v_parent_demand_master_id);

            gm_pkg_oppr_ld_rollup_demand.gm_op_ld_otn(p_demand_sheet_id,4000119);
            
            gm_pkg_oppr_ld_rollup_demand.gm_op_ld_us_forecast(p_demand_sheet_id);
            
        END IF;
            
		-- Below procedure called to load Sales Backorder
		--DBMS_OUTPUT.put_line ('************ load sheet 6' || ' - ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
		--gm_pkg_oppr_ld_sales_demand.gm_op_ld_sales_bo (p_demand_id, p_demand_sheet_id, v_from_period);
		--DBMS_OUTPUT.put_line ('************ load sheet 7' || ' - ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
	--
	END gm_op_ld_main;

  /*************************************************************************
    * Purpose: Procedure to be used to load Account related information based on
    * sheet information [Load based on level]
    *  Staredt with Zone, Region, Intermediate . Will load account in temp table
    *************************************************************************/
    PROCEDURE gm_op_ld_sheetinfo (
       p_demand_id               IN  t4020_demand_master.c4020_demand_master_id%TYPE
     , p_demand_sheet_id	     IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE 
     , p_level_id                IN	 t4015_global_sheet_mapping.c901_level_id%TYPE
     , p_level_value             IN	 t4015_global_sheet_mapping.c901_level_value%TYPE
     , p_demand_sheet_type       IN	 t4020_demand_master.c901_demand_type%TYPE
     , p_parent_demand_master_id IN	 t4020_demand_master.c4020_parent_demand_master_id%TYPE
    )
    AS
       --
       v_demand_period_dt           t4040_demand_sheet.c4040_demand_period_dt%TYPE;  
       v_rule_grp					VARCHAR(100);
       v_lvl_val					t4015_global_sheet_mapping.c901_level_value%TYPE;
       --
    BEGIN
        -- To get the montnly sheet information  
        SELECT t4040.c4040_demand_period_dt
          INTO v_demand_period_dt
          FROM t4040_demand_sheet t4040
         WHERE t4040.c4040_demand_sheet_id = p_demand_sheet_id;
       --
       DELETE FROM my_temp_key_value;

       	BEGIN
       		SELECT C906_RULE_ID into v_lvl_val FROM T906_RULES WHERE C906_RULE_GRP_ID='GOP_ROLLUP_EDIT'
			AND C906_VOID_FL IS NULL
			AND C906_RULE_ID=p_level_value;
			
			EXCEPTION WHEN OTHERS
			THEN                 
                   v_lvl_val := NULL;
            END;
		
         /* If zone level editable then below rollup level to be pick from GOP_ROLLUP_EDIT configurable rule*/
            
	        IF ((p_level_id =  102581 and p_level_value =100824)  -- 102581 Divison and 100824 OUS
		        OR (p_level_id  =  102582 and p_level_value =102603)  --102582 Intermediate and 102603 OUS Direct
		        OR p_level_id   = 102580	--102580 Company
		        OR p_level_value = v_lvl_val)  -- Zone
	        -- for Northern europe zone
	        -- For Zone level editable
	        THEN
	           v_rule_grp := 'GOP_ROLLUP_EDIT';
	        END IF;
	        
	        
       -- 102583-- If its mapped to Zone system will display zone information
       -- 100823 US Zone   
       INSERT INTO my_temp_key_value
                    (my_temp_txn_id, my_temp_txn_key, my_temp_txn_value)
            SELECT t4040.c4040_demand_sheet_id, 'SHEET', t4040.c4020_demand_master_id
              FROM t4015_global_sheet_mapping t4015, t4020_demand_master t4020, t4040_demand_sheet t4040
             WHERE t4020.c4020_parent_demand_master_id = p_parent_demand_master_id
               AND t4020.c901_demand_type = p_demand_sheet_type
               AND t4040.c4040_demand_period_dt = v_demand_period_dt
               and t4020.C4020_VOID_FL is null  
               -- Paddy void fl added
               AND t4040.c4020_demand_master_id = t4020.c4020_demand_master_id
               AND t4015.c4015_global_sheet_map_id = t4020.c4015_global_sheet_map_id 
               AND (t4015.c901_level_id, t4015.c901_level_value) IN 
                                        (SELECT DISTINCT 102584, v700.region_id
                                           FROM v700_territory_mapping_detail v700
                                           WHERE v700.gp_id = DECODE (p_level_id,102583, p_level_value,v700.gp_id) -- Zone Level Rollup 
                                           AND v700.divid = DECODE (p_level_id, 102581, 100824, v700.divid) -- OUS Level 
                                           --AND v700.divid = DECODE (p_level_id,102581, -1,v700.divid) -- Division level Rollup 
                                           AND v700.compid = DECODE (p_level_id,102580, -1,v700.compid) -- company level rollup
                                           AND v700.compid = DECODE (p_level_id,-11111, -1,v700.compid) -- Disable if its US
                                           AND v700.disttypeid = DECODE (p_level_value,102604, 70105,v700.disttypeid) -- Intermediate level rollup
                                           AND v700.disttypeid != DECODE (p_level_value,102603, 70105,'-1') -- Intermediate level rollup 
                                           UNION ALL
                                           SELECT t4015.c901_level_id, t4015.c901_level_value
                                             FROM t4015_global_sheet_mapping t4015
                                            WHERE  (t4015.c901_access_type = DECODE (p_level_id, 102580, 102623, -1)   -- Company level Rollup
	                                                OR (
		                                                c901_level_id = DECODE (p_level_id, -11111, 102581, -1)   -- For US , take the level id and level value of Division and US.
		                                                AND c901_level_value = DeCODE(p_level_id, -11111, 100823, -1)
	                                               	   )
                                                	)
                                              AND t4015.c4015_void_fl IS NULL
   											UNION ALL
   											--SELECT 102583 , 100361 FROM DUAL
                                            SELECT TO_NUMBER(C906_RULE_VALUE), TO_NUMBER(C906_RULE_ID) FROM T906_RULES WHERE C906_RULE_GRP_ID=v_rule_grp
											AND C906_VOID_FL IS NULL
											--102583 Zone 100361	Northern Europe
                                           );

        -- If its not a sales sheet 
        -- 40030 represent group 
        IF ( p_demand_sheet_type != 40020) 
        THEN  
            -- Below code for item consignment group information 
            INSERT INTO my_temp_key_value
                        (my_temp_txn_id, my_temp_txn_key, my_temp_txn_value)
                SELECT DISTINCT t208.c205_part_number_id, '-9999', DECODE(primary_part_id, NULL, 'N', 'Y') 
                  FROM t4021_demand_mapping t4021,
                       t208_set_details t208,
                       (SELECT DISTINCT t4011.c205_part_number_id primary_part_id
                                   FROM t4010_group t4010,
                                        t4021_demand_mapping t4021,
                                        t4011_group_detail t4011
                                  WHERE t4021.c4020_demand_master_id =
                                           (SELECT c4020_parent_demand_master_id
                                              FROM t4020_demand_master
                                             WHERE c4020_demand_master_id = p_demand_id)
                                    AND t4021.c901_ref_type  = 40030
                                    AND t4021.c4021_ref_id   = t4010.c4010_group_id
                                    AND t4010.c4010_group_id = t4011.c4010_group_id
                                    AND t4011.c901_part_pricing_type = 52080) primary_part
                 WHERE t4021.c4020_demand_master_id = p_demand_id
                   AND t4021.c901_ref_type IN (40031, 4000109)
                   AND t4021.c4021_ref_id = t208.c207_set_id
                   AND t208.c205_part_number_id = primary_part.primary_part_id(+)
                   AND t208.c208_void_fl IS NULL;  
                   
            -- Below code repeated for loaner write off 
            INSERT INTO my_temp_key_value
                        (my_temp_txn_id, my_temp_txn_key, my_temp_txn_value)
                SELECT DISTINCT t208.c205_part_number_id, '-99991', DECODE(primary_part_id, NULL, 'N', 'Y') 
                  FROM t4021_demand_mapping t4021,
                       t208_set_details t208,
                       (SELECT DISTINCT t4011.c205_part_number_id primary_part_id
                                   FROM t4010_group t4010,
                                        t4021_demand_mapping t4021,
                                        t4011_group_detail t4011
                                  WHERE t4021.c4020_demand_master_id =
                                           (SELECT c4020_parent_demand_master_id
                                              FROM t4020_demand_master
                                             WHERE c4020_demand_master_id = p_demand_id)
                                    AND t4021.c901_ref_type  = 40030
                                    AND t4021.c4021_ref_id   = t4010.c4010_group_id
                                    AND t4010.c4010_group_id = t4011.c4010_group_id
                                    AND t4011.c901_part_pricing_type = 52080) primary_part
                 WHERE t4021.c4020_demand_master_id = p_demand_id
                   AND t4021.c901_ref_type IN (40031, 4000109)
                   AND t4021.c4021_ref_id = t208.c207_set_id
                   AND t208.c205_part_number_id = primary_part.primary_part_id(+)
                   AND t208.c208_void_fl IS NULL;  
                   
                   
                     
        ELSE
            INSERT INTO my_temp_key_value
                (my_temp_txn_id, my_temp_txn_key, my_temp_txn_value)
                SELECT t4011.c205_part_number_id, t4011.c4010_group_id,   
                      DECODE(t4011.c901_part_pricing_type, 52080, 'Y', 'N' )    
                  FROM t4010_group t4010
                      , t4021_demand_mapping t4021
                      , t4011_group_detail t4011
                 WHERE t4021.c4020_demand_master_id = p_demand_id
                   AND t4021.c901_ref_type = 40030
                   AND t4021.c4021_ref_id = t4010.c4010_group_id
                   AND t4010.c4010_group_id = t4011.c4010_group_id;
                 
        END IF;
                    
    END gm_op_ld_sheetinfo;
   

  /*************************************************************************
    * Purpose: Procedure to be used to save Assoc Sheet information
    *************************************************************************/
    PROCEDURE gm_op_sav_sheet_assoc_map (
       p_demand_id          IN   t4020_demand_master.c4020_demand_master_id%TYPE
     , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
    )
    AS
    --
    BEGIN
       -- Insert Associated sheet information for Readonly sheet 
        INSERT INTO t4046_demand_sheet_assoc_map
                    (c4040_demand_sheet_id, c4040_demand_sheet_assoc_id, c4020_demand_master_assoc_id, c4020_demand_master_id)
            SELECT p_demand_sheet_id, my_temp_txn_id, my_temp_txn_value, p_demand_id
              FROM my_temp_key_value WHERE my_temp_txn_key = 'SHEET';
        --
    END gm_op_sav_sheet_assoc_map;   


	/***********************************************************
	* Purpose: Procedure used to save demand sheet growth information
	******************************************** ****************/
	PROCEDURE gm_op_sav_sheet_growth (
	   p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	)
	AS
	--
	BEGIN
		--
		INSERT INTO t4043_demand_sheet_growth
					(c4043_demand_sheet_growth_id, c4040_demand_sheet_id, c901_ref_type
				   , c4043_start_date, c4043_end_date, c901_grth_ref_type , c4030_grth_ref_id, c4043_value)
			SELECT  s4043_demand_sheet_growth.NEXTVAL, p_demand_sheet_id
                    , c901_ref_type, c4043_start_date, c4043_end_date, c901_grth_ref_type
                   , c4030_grth_ref_id, c4043_value
              FROM (     
                    SELECT  t4043.c901_ref_type, t4043.c4043_start_date, t4043.c4043_end_date, t4043.c901_grth_ref_type
                           , t4043.c4030_grth_ref_id, SUM (NVL(t4043.c4043_value,0)) c4043_value
                      FROM t4043_demand_sheet_growth t4043, my_temp_key_value associated_sheet
                     WHERE associated_sheet.my_temp_txn_id = t4043.c4040_demand_sheet_id  
                       AND associated_sheet.my_temp_txn_key = 'SHEET'    
                  GROUP BY   t4043.c901_ref_type
                           , t4043.c4043_start_date
                           , t4043.c4043_end_date
                           , t4043.c901_grth_ref_type
                           , t4043.c4030_grth_ref_id);
	--
	END gm_op_sav_sheet_growth;
    
	--
	/*************************************************************************
	 * Purpose: Procedure used to load demand for selected period 
	 *  and for selected group
	 *************************************************************************/
	PROCEDURE gm_op_ld_demand (
	   p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	)
	AS
		--

		--
		CURSOR demand_cur
		IS
            SELECT    t4042.c4042_ref_id, t4042.c205_part_number_id, t4042.c4042_period month_value
                    , c901_ref_type reftype , SUM (t4042.c4042_qty) item_qty
                FROM t4042_demand_sheet_detail t4042, my_temp_key_value associated_sheet
               WHERE t4042.c4040_demand_sheet_id = associated_sheet.my_temp_txn_id AND t4042.c901_type = 50560
                 AND associated_sheet.my_temp_txn_key = 'SHEET'
            GROUP BY t4042.c4042_ref_id, t4042.c205_part_number_id, t4042.c4042_period, c901_ref_type;
	BEGIN
		--
		-- load demand information
		FOR set_val IN demand_cur
		LOOP
			--
			gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , set_val.c4042_ref_id
                                                      , set_val.reftype  
													  , 50560
													  , set_val.month_value
													  , set_val.c205_part_number_id
													  , set_val.item_qty
													   );
		--
		END LOOP;
	--
	END gm_op_ld_demand;

--
	/*************************************************************************
	 * Purpose: Procedure used to load OTN for selected period 
	 *  and for selected key value
     * To be used for ALL, US and OUS
	 *************************************************************************/
	PROCEDURE gm_op_ld_otn (
	   p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
     , p_type               IN	 t4042_demand_sheet_detail.c901_type%TYPE
	)
	AS
		--
    CURSOR demand_cur
    IS
        SELECT    t4042.c4042_ref_id, t4042.c205_part_number_id, t4042.c4042_period month_value
                , c901_ref_type reftype, SUM (t4042.c4042_qty) item_qty
            FROM t4042_demand_sheet_detail t4042, my_temp_key_value associated_sheet
           WHERE t4042.c4040_demand_sheet_id = associated_sheet.my_temp_txn_id AND t4042.c901_type = 4000112
             AND associated_sheet.my_temp_txn_key = 'SHEET'
        GROUP BY t4042.c4042_ref_id, t4042.c205_part_number_id, t4042.c4042_period, c901_ref_type;
            
	BEGIN
		--
		-- load demand information
		FOR set_val IN demand_cur
		LOOP
			--
			gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , set_val.c4042_ref_id
                                                      , set_val.reftype 
													  , p_type
													  , set_val.month_value
													  , set_val.c205_part_number_id
													  , set_val.item_qty
													   );
		--
		END LOOP;
	--
	END gm_op_ld_otn;

	--
	/*************************************************************************
	 * Purpose: Procedure used to load sales forecast information
	 * Based on the demand weighted will load forecase information
	 *************************************************************************/
	PROCEDURE gm_op_ld_forecast (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_from_period		IN	 DATE
	  , p_to_period 		IN	 DATE
      , p_type              IN	 t4042_demand_sheet_detail.c901_type%TYPE
	)
	AS
		--
		v_history_fl            CHAR (1);
        v_comments_fl           CHAR (1);
        v_weighted_avg_diff     t4042_demand_sheet_detail.c4042_weighted_avg_diff_qty%TYPE;
        v_current_qty           t4042_demand_sheet_detail.c4042_qty%TYPE;

		-- only for item, write off and sales forecast
		CURSOR forecast_cur
		IS
            SELECT   partforecast.c4042_ref_id, partforecast.c901_ref_type reftype, partforecast.c205_part_number_id,
            		partforecast.month_value , partforecast.current_qty 
                   , partforecast.weighted_avg_diff weighted_avg_diff
                   , NVL(overriddenweightavg.overridden_weight_avg,dmdweightavg.weight_avg) weight_avg
                   , overriddenweightavg.overridden_weight_avg overridden_weight_avg
                   , partforecast.comments_fl
                   , partforecast.history_fl history_fl
                   , dmdweightavg.primary_fl
                FROM                    -- Fetch Weighted Average info
					(SELECT t4042.c4042_ref_id, t4042.c205_part_number_id, t4042.c4042_period, 
            				  DECODE(SIGN(t4042.c4042_qty),-1,0,t4042.c4042_qty) weight_avg
                            , associated_part.my_temp_txn_value primary_fl
                        FROM my_temp_key_value associated_part, my_temp_demand_sheet_detail t4042
                       WHERE t4042.c4040_demand_sheet_id = p_demand_sheet_id 
					     AND t4042.c901_type = 50562
                         AND t4042.c205_part_number_id  = associated_part.my_temp_txn_id  
                         AND t4042.c4042_ref_id  = associated_part.my_temp_txn_key 
                         AND t4042.c901_ref_type NOT IN (102662,102663) ) dmdweightavg,
				(SELECT   t4042.c4042_ref_id, t4042.c205_part_number_id, t4042.c4042_period month_value,  t4042.c901_ref_type  
                             , SUM (NVL(t4042.c4042_weighted_avg_diff_qty,0)) weighted_avg_diff, MAX (t4042.c4042_history_fl) history_fl
                             , SUM (t4042.c4042_qty) current_qty
                             , MAX (t4042.c4042_comments_fl) comments_fl
                          FROM t4042_demand_sheet_detail t4042, my_temp_key_value associated_sheet
                         WHERE t4042.c4040_demand_sheet_id  = associated_sheet.my_temp_txn_id 
						   AND t4042.c901_type = 50563
						   AND t4042.c901_ref_type NOT IN (102662,102663)
                           AND associated_sheet.my_temp_txn_key = 'SHEET'
                      GROUP BY t4042.c4042_ref_id, t4042.c901_ref_type, t4042.c205_part_number_id, t4042.c4042_period) partforecast
                       -- Fetch Overridden Weighted Average info 
                ,(SELECT t4042.c205_part_number_id , t4042.c4042_period,
                             t4042.c4042_qty overridden_weight_avg
                             ,NVL(t4042.c4042_weighted_avg_diff_qty,0) weighted_avg_diff
                        FROM my_temp_demand_sheet_detail t4042
                       WHERE t4042.c4040_demand_sheet_id = p_demand_sheet_id
                         AND t4042.c901_type = 4000344
                         AND t4042.c901_ref_type IN (102661)
                         ) overriddenweightavg
               WHERE partforecast.c4042_ref_id   = dmdweightavg.c4042_ref_id   
                 AND partforecast.c205_part_number_id  = dmdweightavg.c205_part_number_id 
                 AND partforecast.c205_part_number_id  = overriddenweightavg.c205_part_number_id(+) ;                 
            --ORDER BY partforecast.c4042_ref_id, partforecast.c205_part_number_id, partforecast.month_value;

		-- only for set forecast
		CURSOR set_forecast_cur
		IS
            SELECT   partforecast.c4042_ref_id, partforecast.c901_ref_type reftype, partforecast.c205_part_number_id, partforecast.month_value , 
            		 partforecast.current_qty, 
            		partforecast.weighted_avg_diff, 
                   	 NVL(overriddenweightavg.overridden_weight_avg,dmdweightavg.weight_avg) weight_avg
                   	 ,overriddenweightavg.overridden_weight_avg overridden_weight_avg
                   , partforecast.comments_fl 
                   , partforecast.history_fl history_fl
                FROM (SELECT   t4042.c4042_ref_id, t4042.c205_part_number_id, t4042.c4042_period month_value,  t4042.c901_ref_type  
                             , SUM (NVL(t4042.c4042_weighted_avg_diff_qty,0)) weighted_avg_diff, MAX (t4042.c4042_history_fl) history_fl
                             , SUM (t4042.c4042_qty) current_qty
                             , MAX (t4042.c4042_comments_fl) comments_fl
                          FROM t4042_demand_sheet_detail t4042, my_temp_key_value associated_sheet
                         WHERE t4042.c4040_demand_sheet_id = associated_sheet.my_temp_txn_id AND t4042.c901_type = 50563
                           AND associated_sheet.my_temp_txn_key = 'SHEET'
                           AND t4042.c901_ref_type IN (102663)
                      GROUP BY t4042.c4042_ref_id, t4042.c901_ref_type, t4042.c205_part_number_id, t4042.c4042_period) partforecast
                   -- Fetch Weighted Average info
            ,        (SELECT t4042.c4042_ref_id, t4042.c205_part_number_id, t4042.c4042_period, 
            			 DECODE(SIGN(t4042.c4042_qty),-1,0,t4042.c4042_qty) weight_avg
                        FROM my_temp_demand_sheet_detail t4042
                       WHERE t4042.c4040_demand_sheet_id = p_demand_sheet_id AND t4042.c901_type = 50562
                       AND t4042.c901_ref_type IN (102663) ) dmdweightavg
                  -- Fetch Overridden Weighted Average info 
                ,(SELECT t4042.c4042_ref_id , t4042.c4042_period,
                             t4042.c4042_qty overridden_weight_avg
                             ,NVL(t4042.c4042_weighted_avg_diff_qty,0) weighted_avg_diff
                        FROM my_temp_demand_sheet_detail t4042
                       WHERE t4042.c4040_demand_sheet_id = p_demand_sheet_id
                         AND t4042.c901_type = 4000344
                         AND t4042.c901_ref_type IN (102663)
                         ) overriddenweightavg
               WHERE partforecast.c4042_ref_id = dmdweightavg.c4042_ref_id
               		 AND partforecast.c4042_ref_id = overriddenweightavg.c4042_ref_id(+)		
            ORDER BY partforecast.c4042_ref_id, partforecast.c205_part_number_id, partforecast.month_value;

       

            
        CURSOR forecast_cur_set_detail (  v_set_id t208_set_details.c207_set_id%TYPE
                                        , v_set_qty t208_set_details.c208_set_qty%TYPE)
        IS
            SELECT t208.c207_set_id, t208.c205_part_number_id,
                   (t208.c208_set_qty * v_set_qty) c208_set_qty
              FROM t208_set_details t208
             WHERE t208.c207_set_id = v_set_id
               AND t208.c208_inset_fl = 'Y'
               AND t208.c208_set_qty <> 0
               AND t208.c208_void_fl IS NULL ;
               
	BEGIN
		--
		-- load demand information
		FOR set_val IN forecast_cur
		LOOP
            
            -- if its forecasted for OUS Consignement sheet 
            IF (p_type = 4000111) 
            THEN 
	            v_current_qty := 	set_val.current_qty;
            ELSE 
            
                v_current_qty := 	set_val.weight_avg + set_val.weighted_avg_diff;
            END IF;    	
           
            -- If forecast is less than zero then override the forecast to zero
            -- If Primary Flag is Zero then Qty is Zero 
            IF ( v_current_qty < 0 OR set_val.primary_fl = 'N')
            THEN
                 v_current_qty := 0;
            END IF;  
        
			IF ( set_val.primary_fl = 'N' and  set_val.weighted_avg_diff != 0 ) 
			THEN 
				v_current_qty := set_val.weighted_avg_diff;
			END IF;
			
			
			
			
			-- Save the value
			gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, set_val.c4042_ref_id, set_val.reftype  
										, p_type, set_val.month_value, set_val.c205_part_number_id, v_current_qty, NULL, NULL, NULL
										, set_val.history_fl, set_val.comments_fl, set_val.weighted_avg_diff
													   );
		END LOOP;

		-- *********************************
        -- load Set Information 
        -- *********************************
		FOR set_val IN set_forecast_cur
		LOOP
            
            -- if its forecasted for OUS Consignement sheet 
            IF (p_type = 4000111) 
            THEN 
	            v_current_qty := 	set_val.current_qty;
            ELSE 
                v_current_qty := 	set_val.weight_avg + set_val.weighted_avg_diff;
            END IF;    	
      		
            -- If forecast is less than zero then override the forecast to zero
            IF ( v_current_qty < 0)
            THEN
                 v_current_qty := 0;
            END IF;  
        	
            
			-- Save the value
			gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id ,set_val.c4042_ref_id ,set_val.reftype, p_type -- Forecast 
													  , set_val.month_value, set_val.c205_part_number_id, v_current_qty
													  , NULL, NULL, NULL, set_val.history_fl, set_val.comments_fl, set_val.weighted_avg_diff
													   );

            -- Load set related information 
            FOR set_detail IN forecast_cur_set_detail(set_val.c4042_ref_id, v_current_qty)
            LOOP
            --
                gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, set_val.c4042_ref_id, 102662,  p_type, set_val.month_value,
                             set_detail.c205_part_number_id , set_detail.c208_set_qty, NULL, NULL, NULL 
                                , set_val.history_fl, set_val.comments_fl, set_val.weighted_avg_diff) ;
            --
            END LOOP;--
		--
		END LOOP;

	--
	END gm_op_ld_forecast;




	/*************************************************************************
	 * Purpose: Procedure used to load par information for Sales sheet 
     * Currently load is programed for all sheet and world wide sheet is handled 
     * in ld_main work flow 
	 *************************************************************************/
	PROCEDURE gm_op_ld_sales_par (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_to_period 		IN	 DATE
	)
	AS
		--
		CURSOR forecast_calc_cur
		IS
			SELECT t4011.c4010_group_id, t4011.c205_part_number_id, NVL (pvalue.parvalue, 0) parvalue
			  FROM t4010_group t4010
				 , t4021_demand_mapping t4021
				 , t4011_group_detail t4011
				 , (SELECT c205_part_number_id, c4022_par_value parvalue
					  FROM t4022_demand_par_detail
					 WHERE c4020_demand_master_id = p_demand_id 
                       AND c205_part_number_id IS NOT NULL) pvalue
			 WHERE t4021.c4020_demand_master_id = p_demand_id
			   AND t4021.c901_ref_type = 40030
			   AND t4021.c4021_ref_id = t4010.c4010_group_id
			   AND t4010.c4010_group_id = t4011.c4010_group_id
			   AND t4011.c205_part_number_id = pvalue.c205_part_number_id(+);
	BEGIN
		-- load par information
		FOR set_val IN forecast_calc_cur
		LOOP
			-- load par Info
			gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , set_val.c4010_group_id
                                                      , 102660  
													  , 50566
													  , p_to_period
													  , set_val.c205_part_number_id
													  , set_val.parvalue
													   );
		--
		END LOOP;
	--
	END gm_op_ld_sales_par;

	/*************************************************************************
	 * Purpose: Procedure used to load par information for consignment sheet
     * Will calculate Item and Set Par 
     * Currently load is rogramed for all sheet and world wide sheet is handled 
     * in ld_main work flow 
	 *************************************************************************/
	PROCEDURE gm_op_ld_consign_par (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_to_period 		IN	 DATE
	)
	AS
		--
		CURSOR par_item_calc_cur
		IS
			SELECT '-9999' item_id, c205_part_number_id, c4022_par_value parvalue
        	  FROM t4022_demand_par_detail t4022
        	 WHERE c4020_demand_master_id = p_demand_id 
               AND c205_part_number_id IS NOT NULL;

		-- Set Par
        CURSOR par_set_calc_cur
		IS
			SELECT  t4022.c207_set_id,  c4022_par_value parvalue
        	  FROM t4022_demand_par_detail t4022
        	 WHERE c4020_demand_master_id = p_demand_id 
               AND t4022.c207_set_id IS NOT NULL;


        -- Cursor to fetch set detail based on forecasted qty 
        CURSOR forecast_cur_set_detail (  v_set_id t208_set_details.c207_set_id%TYPE
                                        , v_set_qty t208_set_details.c208_set_qty%TYPE)
        IS
            SELECT t208.c207_set_id, t208.c205_part_number_id,
                   (t208.c208_set_qty * v_set_qty) c208_set_qty
              FROM t208_set_details t208
             WHERE t208.c207_set_id = v_set_id
               AND t208.c208_inset_fl = 'Y'
               AND t208.c208_set_qty <> 0
               AND t208.c208_void_fl IS NULL ;
                                      
	BEGIN
		-- load item par information
		FOR set_val IN par_item_calc_cur
		LOOP
			-- load par Info
			gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , set_val.item_id
                                                      , 102661
													  , 50566
													  , p_to_period
													  , set_val.c205_part_number_id
													  , set_val.parvalue
													   );
		--
		END LOOP;


		-- load set par information
		FOR set_val IN par_set_calc_cur
		LOOP
			-- load par Info
			gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , set_val.c207_set_id
                                                      , 102663
													  , 4000113
													  , p_to_period
													  , NULL
													  , set_val.parvalue
													   );

            -- Load set related information 
            FOR set_detail IN forecast_cur_set_detail(set_val.c207_set_id, set_val.parvalue)
            LOOP
            --
                gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, set_val.c207_set_id, 102662,  4000113, p_to_period,
                             set_detail.c205_part_number_id , set_detail.c208_set_qty, NULL, NULL, NULL) ;
            --
            END LOOP;
            --
		END LOOP;
	--
	END  gm_op_ld_consign_par;

--
/*************************************************************************
 * Purpose: Procedure used to load back order from all related sheet
 *************************************************************************/
	PROCEDURE gm_op_ld_bo (
	    p_demand_sheet_id	 IN   t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	)
	AS
 		--
    CURSOR demand_cur
    IS
        SELECT    t4042.c4042_ref_id, t4042.c901_ref_type , t4042.c205_part_number_id, t4042.c4042_period month_value
                , t4042.c901_type , SUM (t4042.c4042_qty) item_qty
            FROM t4042_demand_sheet_detail t4042, my_temp_key_value associated_sheet
           WHERE t4042.c4040_demand_sheet_id = associated_sheet.my_temp_txn_id 
             AND t4042.c901_type IN (50567, 50568, 50569)
             AND associated_sheet.my_temp_txn_key = 'SHEET'
        GROUP BY t4042.c4042_ref_id,  t4042.c901_ref_type ,t4042.c205_part_number_id, t4042.c4042_period, t4042.c901_type;
            
	BEGIN
		--
		-- load demand information
		FOR set_val IN demand_cur
		LOOP
			--
			gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , set_val.c4042_ref_id
                                                      , set_val.c901_ref_type  
													  , set_val.c901_type
													  , set_val.month_value
													  , set_val.c205_part_number_id
													  , set_val.item_qty
													   );
		--
		END LOOP;
        
	END gm_op_ld_bo;
--
	/********************************************************************
	 * Purpose: Procedure used to load sub component and calulate qty
	 ********************************************************************/
	PROCEDURE gm_op_ld_sub_components (
		p_demandsheetid   IN   t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	)
	AS
		v_demand_sheet_id t4040_demand_sheet.c4040_demand_sheet_id%TYPE;
		v_ref_id	   t4042_demand_sheet_detail.c4042_ref_id%TYPE;
		v_type		   t4042_demand_sheet_detail.c901_type%TYPE;
		v_period	   t4042_demand_sheet_detail.c4042_period%TYPE;
		v_partnum	   t4042_demand_sheet_detail.c205_part_number_id%TYPE;
		v_qty		   t4042_demand_sheet_detail.c4042_qty%TYPE;
		v_parent_part_id t4042_demand_sheet_detail.c205_part_number_id%TYPE;
		v_sub_part_id  t4042_demand_sheet_detail.c205_part_number_id%TYPE;
		v_sub_qty	   t4042_demand_sheet_detail.c4042_qty%TYPE;
		v_new_qty	   NUMBER;

		CURSOR cur_parent_details
		IS
			SELECT t4042.c4040_demand_sheet_id dmid, c205_part_number_id partnum, c4042_ref_id refid
				 , c4042_period period, c4042_qty qty, c4042_history_fl historyfl, c901_type parent_type
			  FROM my_temp_demand_sheet_detail t4042
			 WHERE t4042.c4040_demand_sheet_id = p_demandsheetid
			   AND t4042.c205_part_number_id IN (
										 SELECT 	t205a.c205_from_part_number_id
											   FROM t205a_part_mapping t205a
										 START WITH t205a.c205_to_part_number_id IN (SELECT t205d.c205_part_number_id
																					   FROM t205d_sub_part_to_order t205d)
										 CONNECT BY PRIOR t205a.c205_from_part_number_id = t205a.c205_to_part_number_id
										 AND t205a.c205a_void_fl IS NULL
										   GROUP BY t205a.c205_from_part_number_id)
			   AND t4042.c901_type IN (50563, 50567, 50568, 50569);
		CURSOR cur_sub_details
		IS
			SELECT		 t205a.c205_from_part_number_id parent_part, t205a.c205_to_part_number_id sub_part
					 , (t205a.c205a_qty * NVL (PRIOR t205a.c205a_qty, 1)) qty
				  FROM t205a_part_mapping t205a
				 WHERE t205a.c205_to_part_number_id IN (SELECT t205d.c205_part_number_id
															FROM t205d_sub_part_to_order t205d)
			      AND t205a.c205a_void_fl IS NULL											
			START WITH t205a.c205_from_part_number_id = v_partnum	--'124.000'
			CONNECT BY PRIOR t205a.c205_to_part_number_id = t205a.c205_from_part_number_id;
	BEGIN
		FOR var_parent_detail IN cur_parent_details
		LOOP
			v_new_qty	:= 0;
			v_demand_sheet_id := var_parent_detail.dmid;
			v_ref_id	:= var_parent_detail.refid;
			v_type		:= var_parent_detail.parent_type;
			v_period	:= var_parent_detail.period;
			v_partnum	:= var_parent_detail.partnum;
			v_qty		:= var_parent_detail.qty;

			FOR var_sub_detail IN cur_sub_details
			LOOP
				v_parent_part_id := var_sub_detail.parent_part;
				v_sub_part_id := var_sub_detail.sub_part;
				v_sub_qty	:= var_sub_detail.qty;
				gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (v_demand_sheet_id
									  , 9999999999
                                      , 102664
									  , v_type
									  , v_period
									  , v_sub_part_id
									  , v_qty * v_sub_qty
									  , v_partnum
									  , v_ref_id
									  , v_sub_qty
									   );
			END LOOP;
		END LOOP;
	END gm_op_ld_sub_components;

	/*************************************************************************
	 * Purpose: Procedure used to load US Forecast 
	 *  Calculation based on Over all minus us need 
	 *************************************************************************/
	PROCEDURE gm_op_ld_us_forecast (
	   p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
    )
	AS
		--
    CURSOR demand_cur
    IS
        SELECT    t4042.c4042_ref_id,  t4042.c901_ref_type ,t4042.c205_part_number_id, t4042.c4042_period month_value
                , t4042.c205_parent_part_num_id
                , t4042.c4042_parent_ref_id
                , t4042.c4042_parent_qty_needed
                , SUM (DECODE(t4042.c901_type,50563, t4042.c4042_qty, 0)) forecast_qty
                , SUM (DECODE(t4042.c901_type,4000111, t4042.c4042_qty, 0)) ous_forecast_qty
            FROM my_temp_demand_sheet_detail t4042
           WHERE t4042.c4040_demand_sheet_id = p_demand_sheet_id 
             AND t4042.c901_type IN (50563,  4000111)
        GROUP BY t4042.c4042_ref_id, t4042.c901_ref_type , t4042.c205_part_number_id, t4042.c4042_period
                 , t4042.c205_parent_part_num_id , t4042.c4042_parent_ref_id, t4042.c4042_parent_qty_needed;
            
	BEGIN
		
        --
		-- load demand information
		FOR set_val IN demand_cur
		LOOP
			--
			gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , set_val.c4042_ref_id
                                                      , set_val.c901_ref_type
													  , 4000110
													  , set_val.month_value
													  , set_val.c205_part_number_id
													  , (set_val.forecast_qty - set_val.ous_forecast_qty)
                                                      , set_val.c205_parent_part_num_id 
                                                      , set_val.c4042_parent_ref_id
                                                      , set_val.c4042_parent_qty_needed 
													   );
		--
		END LOOP;
	--
	END gm_op_ld_us_forecast;
END gm_pkg_oppr_ld_rollup_demand;
/
