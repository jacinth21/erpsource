/* Formatted on 2009/12/28 18:08 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\purchasing\gm_pkg_oppr_ld_consign_demand.bdy";
-- show err  Package Body gm_pkg_oppr_ld_consign_demand
-- exec gm_pkg_oppr_ld_demand.gm_op_ld_demand_main(33);
/*
DELETE FROM T4042_DEMAND_SHEET_DETAIL;
DELETE FROM T4041_DEMAND_SHEET_MAPPING;
DELETE FROM T4040_DEMAND_SHEET;
COMMIT ;
exec gm_pkg_oppr_ld_demand.gm_op_ld_demand_main(20);
*/
CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ld_consign_demand
IS
    /*************************************************************************
    * Purpose: Procedure used to load sales demand, calculated value and
    * Forecast information
    *************************************************************************/
PROCEDURE gm_op_ld_main (
        p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
        p_demand_type     IN t4020_demand_master.c901_demand_type%TYPE,
        p_demand_period   IN t4020_demand_master.c4020_demand_period%TYPE,
        p_forecast_period IN t4020_demand_master.c4020_forecast_period%TYPE,
        p_inc_current_fl  IN t4020_demand_master.c4020_include_current_fl%TYPE,
        p_inventory_id    IN t250_inventory_lock.c250_inventory_lock_id%TYPE)
AS
    --
    v_from_period DATE;
    v_to_period DATE;
    --
    v_level_id      t4015_global_sheet_mapping.c901_level_id%TYPE;
    v_level_value   t4015_global_sheet_mapping.c901_level_value%TYPE;
    --
BEGIN
    --
    -- Below code to fetch demand (from and to period)
    gm_pkg_oppr_ld_demand.gm_op_calc_period (p_demand_period, p_inc_current_fl, 'D', v_from_period, v_to_period) ;
    --
       --
   SELECT c901_level_id, c901_level_value
     INTO v_level_id, v_level_value
     FROM t4015_global_sheet_mapping t4015, t4020_demand_master t4020
    WHERE t4020.c4020_demand_master_id = p_demand_id
      AND t4020.c4015_global_sheet_map_id = t4015.c4015_global_sheet_map_id;
          
    --To load set related information into temp table 
    gm_pkg_oppr_ld_consign_demand.gm_op_ld_set_info(p_demand_id, v_level_id, v_level_value);
    --
    -- Below procedure called to load sales demand information
    IF (p_demand_type = 40021) THEN
    --
        --
        gm_pkg_oppr_ld_consign_demand.gm_op_ld_distributor_info(p_demand_id,v_level_id, v_level_value );
        gm_pkg_oppr_ld_consign_demand.gm_op_ld_demand_fa_set (p_demand_id, p_demand_sheet_id, v_from_period, v_to_period);
        gm_pkg_oppr_ld_consign_demand.gm_op_ld_demand_fa_item (p_demand_id, p_demand_sheet_id, v_from_period, v_to_period);
         

    --    
    ELSE
        
        gm_pkg_oppr_ld_consign_demand.gm_op_ld_demand_inhosue_set (p_demand_id, p_demand_sheet_id, v_from_period, v_to_period);


        gm_pkg_oppr_ld_consign_demand.gm_op_ld_demand_inhouse_item (p_demand_id, p_demand_sheet_id, v_from_period, v_to_period);

        -- This procedure will capture the demand for all the Write off parts during Loaner Reconciliation process .

        gm_pkg_oppr_ld_consign_demand.gm_op_ld_demand_lnr_writeoff(p_demand_id,p_demand_sheet_id,v_from_period,v_to_period);


    END IF;

	-- Below procedure called to load Weighted and Avg demand information information
	gm_pkg_oppr_ld_demand.gm_op_ld_demand_calc_avg (p_demand_sheet_id, v_from_period);
    
    --
    -- Below code to fetch forecast (from and to period)
    --DBMS_OUTPUT.put_line ('Before calling gm_op_calc_period');
    gm_pkg_oppr_ld_demand.gm_op_calc_period (p_forecast_period, p_inc_current_fl, 'F', v_from_period, v_to_period) ;
    --
    -- Below procedure called to load forecast information
    --DBMS_OUTPUT.put_line ('Before calling gm_op_ld_forecast');
    gm_pkg_oppr_ld_consign_demand.gm_op_ld_forecast (p_demand_id, p_demand_sheet_id, v_from_period, v_to_period) ;
    --
         
    -- Below procedure called to load sales Weighted and Avg sales information
    gm_pkg_oppr_ld_demand.gm_op_ld_forecast_avg (p_demand_sheet_id, v_from_period);
    --
    -- Below procedure called to load sales variance
    gm_pkg_oppr_ld_demand.gm_op_ld_forecast_variance (p_demand_sheet_id, v_from_period);
        

    -- Below code to load par information
    --DBMS_OUTPUT.put_line ('Before calling gm_op_ld_par_value');
    --DBMS_OUTPUT.put_line ('Before calling gm_op_ld_forecast - ' || p_demand_id || ' -- ' || p_demand_sheet_id);
    --gm_pkg_oppr_ld_consign_demand.gm_op_ld_par_value (p_demand_id, p_demand_sheet_id, v_from_period) ;
    
    -- Below code to load Pending Requests
    gm_pkg_oppr_ld_consign_demand.gm_op_ld_pending_request (p_demand_id, p_demand_sheet_id, v_from_period, p_inventory_id
    ) ;
    --
     -- Below code to load One time need
    gm_pkg_oppr_ld_consign_demand.gm_op_ld_otn(p_demand_id, p_demand_sheet_id, v_from_period) ;
     
    -- Below procedure called to load sales Weighted and Avg sales information
    --gm_pkg_oppr_ld_consign_demand.gm_op_ld_forecast_calc (p_demand_sheet_id, v_from_period);
    --
    --
    --
    -- Delete the item consignment Part which does not contain any item consignment
    -- for selected period
    /*  -- Commented, as we are not seeing the consignment record for countries which does not have any demand. 
    DELETE
       FROM my_temp_demand_sheet_detail
      WHERE c4040_demand_sheet_id = p_demand_sheet_id
        AND c4042_ref_id          = '-9999'
        AND c205_part_number_id  IN
        (
             SELECT t4042.c205_part_number_id
               FROM my_temp_demand_sheet_detail t4042
              WHERE t4042.c901_type             IN (50560, 50563)
                AND t4042.c4040_demand_sheet_id = p_demand_sheet_id
                AND t4042.c4042_ref_id          = '-9999'
           GROUP BY t4042.c205_part_number_id
            HAVING SUM (c4042_qty) = 0
        ) ; 
   */
      --

END gm_op_ld_main;
--
   /*************************************************************************
    * Purpose: Procedure to be used to load Distributor  related information based on
    * sheet information [Load based on level]
    *  Start with Zone, Region, Will load distributor in temp table
    *************************************************************************/
    PROCEDURE gm_op_ld_distributor_info (
       p_demand_id   IN  t4020_demand_master.c4020_demand_master_id%TYPE
     , p_level_id    IN  t4015_global_sheet_mapping.c901_level_id%TYPE
     , p_level_value IN  t4015_global_sheet_mapping.c901_level_value%TYPE
    )
    AS
    --
    BEGIN
       --
      	DELETE FROM my_temp_list;
       -- Modified Truncate to Delete for PMT-36744
       --
       INSERT INTO my_temp_list
                   (my_temp_txn_id)
          SELECT DISTINCT v700.d_id
                     FROM v700_territory_mapping_detail v700
                    WHERE v700.divid =  DECODE (p_level_id,102581, p_level_value,v700.divid)
                      AND v700.gp_id = DECODE (p_level_id,102583, p_level_value,v700.gp_id) -- Added for Zone level edit
                      AND v700.region_id = DECODE (p_level_id,102584, p_level_value,v700.region_id)
                      AND v700.disttypeid = DECODE (p_level_id,102582, p_level_value,v700.disttypeid);
    --
    END gm_op_ld_distributor_info;

   /*************************************************************************
    * Purpose: Procedure to be used to load set information related to the group
    * to be used by detail sql 
    *************************************************************************/
    PROCEDURE gm_op_ld_set_info (
      p_demand_id   IN   t4020_demand_master.c4020_demand_master_id%TYPE
    , p_level_id    IN  t4015_global_sheet_mapping.c901_level_id%TYPE 
    , p_level_value IN  t4015_global_sheet_mapping.c901_level_value%TYPE
    )
    AS
       --
    BEGIN
       --
        -- Temp value to be moved to a stand allow forecast
        --
        DELETE FROM  my_temp_key_value;
        
       -- Modified Truncate to Delete for PMT-36744
       
        -- Table to hold set information for selected sheet 
        INSERT INTO my_temp_key_value
                (my_temp_txn_id, my_temp_txn_key)
            SELECT t4021.c4021_ref_id, 'SET'
             FROM t4021_demand_mapping t4021
            WHERE t4021.c4020_demand_master_id = p_demand_id AND t4021.c901_ref_type IN (40031, 4000109);
            
        
        -- Holds Part related information for the selected sheet 
        -- It also included if is a primary sheet 
        INSERT INTO my_temp_key_value
                    (my_temp_txn_id, my_temp_txn_key, my_temp_txn_value)
           SELECT DISTINCT t208.c205_part_number_id, 'ITEM', DECODE(primary_part_id, NULL, 'N', 'Y') 
                      FROM t4021_demand_mapping t4021,
                           t208_set_details t208,
                           (SELECT DISTINCT t4011.c205_part_number_id primary_part_id
                                       FROM t4010_group t4010,
                                            t4021_demand_mapping t4021,
                                            t4011_group_detail t4011
                                      WHERE t4021.c4020_demand_master_id =
                                               (SELECT c4020_parent_demand_master_id
                                                  FROM t4020_demand_master
                                                 WHERE c4020_demand_master_id = p_demand_id)
                                        AND t4021.c901_ref_type = 40030
                                        AND t4021.c4021_ref_id = t4010.c4010_group_id
                                        AND t4010.c4010_group_id =
                                                                  t4011.c4010_group_id
                                        AND t4011.c901_part_pricing_type = 52080) primary_part
                     WHERE t4021.c4020_demand_master_id = p_demand_id
                       AND t4021.c901_ref_type IN (40031, 4000109)
                       AND t4021.c4021_ref_id = t208.c207_set_id
                       AND t208.c205_part_number_id = primary_part.primary_part_id(+)
                       AND t208.c208_void_fl IS NULL;    
        --
        -- Load Contry related information
   /*     INSERT INTO my_temp_key_value
                    (my_temp_txn_id, my_temp_txn_key) 
         SELECT t906.c906_rule_id , 'EXT_COUNTRY_FLAG'
           FROM t906_rules t906
          WHERE t906.c906_rule_grp_id = 'OUS_REGION_MAPPING'
            AND t906.c906_rule_value  = DECODE(p_level_id, 102581,  p_level_value, -9999)
          UNION ALL 
         select to_char(t710.c901_country_id),  'EXT_COUNTRY_FLAG' 
           FROM t710_sales_hierarchy t710 
          WHERE t710.c901_area_id   = DECODE(p_level_id, 102584,  p_level_value, -9999) 
            AND t710.c710_active_fl = 'Y'; */
           
         execute immediate 'TRUNCATE TABLE temp_plant_id';
                       
			INSERT INTO temp_plant_id
			    (c5040_plant_id)
			SELECT c5040_plant_id 
			FROM t5041_plant_company_mapping
			WHERE c1900_company_id IN
			    (SELECT t906.c906_rule_id
			    FROM t906_rules t906
			    WHERE t906.c906_rule_grp_id = 'REGION_COMP_MAPPING'
			   -- AND t906.c906_rule_value    = DECODE(p_level_id,102584,p_level_value,102581,p_level_value, -9999)
			   AND t906.c906_rule_value    = DECODE(p_level_id,102584,p_level_value,102581,p_level_value,102583,p_level_value, -9999)
			    AND t906.c906_void_fl IS NULL)
			AND c5041_void_fl IS NULL;
			--Ex If Level id Region(102584) then leval valu Region id, If leve id Division(102581 applicable only for US) then level value Divison Id
                                     
    END gm_op_ld_set_info;
/*************************************************************************
* Purpose: Procedure used to load sales consignment demand for selected period
* 40030 maps to Group Mapping and 40032 maps to Region Mapping
*************************************************************************/
PROCEDURE gm_op_ld_demand_fa_set (
      p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE
    , p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE
    , p_from_period     IN DATE
    , p_to_period       IN DATE
)
AS
    -- **************************************
    -- fetch Aggregiated set information
    -- ************************************** 
    -- as part of 12C change the 40057 ICT type removed in all below Consingment query as this qty being captured in sales demand
    CURSOR demand_set_cur
    IS
        SELECT partinfo.setid, partinfo.month_value, NVL (c_qty, 0) -  NVL (r_qty, 0) cons_qty
      FROM (SELECT setlist.my_temp_txn_id setid, v9001.month_value
              FROM my_temp_key_value setlist, v9001_month_list v9001
             WHERE  setlist.my_temp_txn_key = 'SET'
               AND v9001.month_value >= p_from_period
               AND v9001.month_value <= p_to_period
                ORDER BY setlist.my_temp_txn_id,v9001.month_value
               ) partinfo,
           (SELECT   t504.c207_set_id setid,
                     TRUNC(t504.c504_ship_date,'month') sdate,
                     COUNT (1) c_qty
                FROM t504_consignment t504,
                     my_temp_list distlist,
                     my_temp_key_value setlist
               WHERE t504.c701_distributor_id IS NOT NULL
                 AND t504.c701_distributor_id = distlist.my_temp_txn_id
                 AND t504.c207_set_id IS NOT NULL
                 AND t504.C5040_plant_id IN  (SELECT C5040_plant_id FROM temp_plant_id)
--                 (
--			                SELECT my_temp_txn_id FROM my_temp_key_value
--			                 WHERE my_temp_txn_key = 'RGN_COMP_PLANT_FLAG')
                 AND t504.c207_set_id = setlist.my_temp_txn_id
                 AND setlist.my_temp_txn_key = 'SET'
                 -- Remove the consignment transfers
                 AND NOT EXISTS (
                        SELECT c921_ref_id
                          FROM t920_transfer t920, t921_transfer_detail t921
                         WHERE t920.c920_transfer_id = t921.c920_transfer_id
                           AND c921_ref_id = t504.c504_consignment_id
                           AND t920.c920_void_fl IS NULL
                           AND t920.c920_transfer_date >= p_from_period
                           AND t920.c920_transfer_date <= p_to_period
                           AND t921.c901_link_type = 90360)
                 AND t504.c504_ship_date >= p_from_period
                 AND t504.c504_ship_date <= p_to_period
                 AND t504.c504_status_fl = '4'
                 AND t504.c504_type = 4110
                 AND t504.c504_void_fl IS NULL
            GROUP BY t504.c207_set_id, TRUNC(t504.c504_ship_date,'month')) consinfo,
           (SELECT   t506.c207_set_id setid,
                     TRUNC(t506.c506_credit_date,'month') sdate,
                     COUNT (1) r_qty
                FROM t506_returns t506,
                     my_temp_list distlist,
                     my_temp_key_value setlist
               WHERE t506.c701_distributor_id IS NOT NULL
                 AND t506.c701_distributor_id = distlist.my_temp_txn_id
                 AND t506.c506_void_fl IS NULL
                 AND t506.c506_status_fl = '2'
                 AND t506.c207_set_id IS NOT NULL
                 AND t506.c207_set_id = setlist.my_temp_txn_id
                 AND setlist.my_temp_txn_key = 'SET'
                 AND t506.C5040_plant_id IN  (SELECT C5040_plant_id FROM temp_plant_id)
--                 (
--			                SELECT my_temp_txn_id FROM my_temp_key_value
--			                 WHERE my_temp_txn_key = 'RGN_COMP_PLANT_FLAG')
                 AND NOT EXISTS (
                        SELECT c921_ref_id
                          FROM t920_transfer t920, t921_transfer_detail t921
                         WHERE t920.c920_transfer_id = t921.c920_transfer_id
                           AND c921_ref_id = t506.c506_rma_id
                           AND t920.c920_void_fl IS NULL
                           AND t920.c920_transfer_date >= p_from_period
                           AND t920.c920_transfer_date <= p_to_period
                           AND t921.c901_link_type = 90361)
                 AND t506.c506_credit_date >= p_from_period
                 AND t506.c506_credit_date <= p_to_period
            GROUP BY t506.c207_set_id, TRUNC(t506.c506_credit_date,'month')) returninfo
     WHERE partinfo.month_value = consinfo.sdate(+)
       AND partinfo.setid = consinfo.setid(+)
       AND partinfo.month_value = returninfo.sdate(+)
       AND partinfo.setid = returninfo.setid(+) ;
    
    -- ******************************
    -- Only Set Part Details 
    -- ******************************
    CURSOR demand_item_cur
    IS
        SELECT partinfo.setid, partinfo.p_num, partinfo.month_value, NVL (c_qty, 0) -  NVL (r_qty, 0) cons_qty
          FROM (SELECT DISTINCT setlist.my_temp_txn_id setid, t208.c205_part_number_id p_num, v9001.month_value
                  FROM my_temp_key_value setlist, t208_set_details t208, v9001_month_list v9001
                 WHERE setlist.my_temp_txn_key = 'SET'
                   AND setlist.my_temp_txn_id  = t208.c207_set_id
                   AND t208.c208_inset_fl           = 'Y'
                   AND t208.c208_void_fl           IS NULL
                   AND v9001.month_value >= p_from_period
                   AND v9001.month_value <= p_to_period
                   ORDER BY setlist.my_temp_txn_id,t208.c205_part_number_id,v9001.month_value
                   ) partinfo,
               (SELECT   t504.c207_set_id setid, t505.c205_part_number_id p_num,
                         TRUNC(t504.c504_ship_date,'month') sdate,
                         SUM (t505.c505_item_qty) c_qty
                    FROM t504_consignment t504,
                         t505_item_consignment t505, 
                         my_temp_list distlist,
                         my_temp_key_value setlist
                   WHERE t504.c701_distributor_id IS NOT NULL
                     AND t504.c701_distributor_id = distlist.my_temp_txn_id
                     AND t504.c504_consignment_id  = t505.c504_consignment_id
                     AND t504.c207_set_id IS NOT NULL
                     AND t504.c207_set_id = setlist.my_temp_txn_id
                     AND setlist.my_temp_txn_key = 'SET'
                     AND t504.C5040_plant_id IN  (SELECT C5040_plant_id FROM temp_plant_id)
--                     (
--			                SELECT my_temp_txn_id FROM my_temp_key_value
--			                 WHERE my_temp_txn_key = 'RGN_COMP_PLANT_FLAG')                     
                     -- Remove the consignment transfers
                     AND NOT EXISTS (
                            SELECT c921_ref_id
                              FROM t920_transfer t920, t921_transfer_detail t921
                             WHERE t920.c920_transfer_id = t921.c920_transfer_id
                               AND c921_ref_id = t504.c504_consignment_id
                               AND t920.c920_void_fl IS NULL
                               AND t920.c920_transfer_date >= p_from_period
                               AND t920.c920_transfer_date <= p_to_period 
                               AND t921.c901_link_type = 90360)
                     AND t504.c504_ship_date >= p_from_period
                     AND t504.c504_ship_date <= p_to_period
                     AND t504.c504_status_fl = '4'
                     AND t504.c504_type = 4110
                     AND t504.c504_void_fl IS NULL
                     AND TRIM (t505.c505_control_number) IS NOT NULL
                     AND (t505.c901_type                 IS NULL
                        OR t505.c901_type                   <> 100880)
                GROUP BY t504.c207_set_id, t505.c205_part_number_id, TRUNC(t504.c504_ship_date,'month') ) consinfo,
               (SELECT   t506.c207_set_id setid,
                         TRUNC(t506.c506_credit_date,'month') sdate,                          
                         t507.c205_part_number_id p_num, SUM (t507.c507_item_qty )  r_qty
                    FROM t506_returns t506,
                         t507_returns_item t507, 
                         my_temp_list distlist,
                         my_temp_key_value setlist
                   WHERE t506.c701_distributor_id IS NOT NULL
                     AND t506.c701_distributor_id = distlist.my_temp_txn_id
                     AND t506.c506_void_fl IS NULL
                     AND t506.c506_status_fl = '2'
                     AND t506.c207_set_id IS NOT NULL
                     AND t506.c207_set_id = setlist.my_temp_txn_id
                     AND t506.c506_rma_id = t507.c506_rma_id
                     AND t507.c507_status_fl IN ('C', 'W')
                     AND setlist.my_temp_txn_key = 'SET'
                     AND t506.C5040_plant_id IN  (SELECT C5040_plant_id FROM temp_plant_id)
--                     (
--			                SELECT my_temp_txn_id FROM my_temp_key_value
--			                 WHERE my_temp_txn_key = 'RGN_COMP_PLANT_FLAG')                     
                     AND NOT EXISTS (
                            SELECT c921_ref_id
                              FROM t920_transfer t920, t921_transfer_detail t921
                             WHERE t920.c920_transfer_id = t921.c920_transfer_id
                               AND c921_ref_id = t506.c506_rma_id
                               AND t920.c920_void_fl IS NULL
                               AND t920.c920_transfer_date >= p_from_period
                               AND t920.c920_transfer_date <= p_to_period
                               AND t921.c901_link_type = 90361)
                     AND t506.c506_credit_date >= p_from_period
                     AND t506.c506_credit_date <= p_to_period
                GROUP BY t506.c207_set_id, t507.c205_part_number_id, TRUNC(t506.c506_credit_date,'month')) returninfo
         WHERE partinfo.month_value = consinfo.sdate(+)
           AND partinfo.setid = consinfo.setid(+)
           AND partinfo.p_num = consinfo.p_num (+)
           AND partinfo.month_value = returninfo.sdate(+)
           AND partinfo.setid = returninfo.setid(+)
           AND partinfo.p_num = returninfo.p_num (+);

BEGIN

    --******************************
    -- load demand set information
    --******************************
    FOR set_val IN demand_set_cur
    LOOP
        --
        --DBMS_OUTPUT.put_line ('Sheet ID  ****' || set_val.c205_part_number_id || ' - ' || set_val.item_qty);
        gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, set_val.setid, 102663, 50560, set_val.month_value,
        NULL, set_val.cons_qty) ;
        --
    END LOOP;
    --
    -- load demand item information
    FOR set_val IN demand_item_cur
    LOOP
        --
        --DBMS_OUTPUT.put_line ('Sheet ID  ****' || set_val.c205_part_number_id || ' - ' || set_val.item_qty);
        gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, set_val.setid, 102662, 50560, set_val.month_value,
        set_val.p_num, set_val.cons_qty) ;
        --
    END LOOP;
    --
END gm_op_ld_demand_fa_set;


/*************************************************************************
 * Purpose: Procedure used to load consignment item for selected period
 * 40030 maps to Group Mapping and 40032 maps to Region Mapping
 *************************************************************************/
PROCEDURE gm_op_ld_demand_fa_item (
        p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
        p_from_period     IN DATE,
        p_to_period       IN DATE)
AS
    -- ******************************
    -- Only Item consignment Details 
    -- ******************************
    CURSOR demand_item_cur
    IS
        SELECT partinfo.p_num, partinfo.month_value,  NVL (c_qty, 0) -  NVL (r_qty, 0) cons_qty
          FROM (SELECT DISTINCT setpartlist.my_temp_txn_id p_num, v9001.month_value
                  FROM my_temp_key_value setpartlist, v9001_month_list v9001
                 WHERE setpartlist.my_temp_txn_key = 'ITEM'
                   AND v9001.month_value >= p_from_period
                   AND v9001.month_value <= p_to_period
                   ORDER BY setpartlist.my_temp_txn_id,v9001.month_value
                   ) partinfo,
               (SELECT   t505.c205_part_number_id p_num,
                         TRUNC(t504.c504_ship_date,'month') sdate,
                         SUM (t505.c505_item_qty) c_qty
                    FROM t504_consignment t504,
                         t505_item_consignment t505, 
                         my_temp_list distlist
                   WHERE t504.c701_distributor_id IS NOT NULL
                     AND t504.c701_distributor_id = distlist.my_temp_txn_id
                     AND t504.c504_consignment_id  = t505.c504_consignment_id
                     AND t504.c207_set_id IS NULL
                     -- Remove excess from item consignent 
                     AND NOT EXISTS
                        (
                             SELECT a.c504_consignment_id a
                               FROM t504a_consignment_excess a
                              WHERE a.c504_consignment_id = t504.c504_consignment_id
                        )             
                     -- Remove the consignment transfers
                     AND NOT EXISTS (
                            SELECT c921_ref_id
                              FROM t920_transfer t920, t921_transfer_detail t921
                             WHERE t920.c920_transfer_id = t921.c920_transfer_id
                               AND c921_ref_id = t504.c504_consignment_id
                               AND t920.c920_void_fl IS NULL
                               AND t920.c920_transfer_date >= p_from_period
                               AND t920.c920_transfer_date <= p_to_period
                               AND t921.c901_link_type = 90360)
                     AND t504.c504_ship_date >= p_from_period
                     AND t504.c504_ship_date <= p_to_period
                     AND t504.c504_status_fl = '4'
                     AND t504.c504_type = 4110
                     AND t504.C5040_plant_id IN (SELECT C5040_plant_id FROM temp_plant_id)
--                     (
--			                SELECT my_temp_txn_id FROM my_temp_key_value
--			                 WHERE my_temp_txn_key = 'RGN_COMP_PLANT_FLAG')                     
                     AND t504.c504_void_fl IS NULL
                     AND TRIM (t505.c505_control_number) IS NOT NULL
                     AND (t505.c901_type   IS NULL
                        OR t505.c901_type                   <> 100880)
                GROUP BY t505.c205_part_number_id, TRUNC(t504.c504_ship_date,'month')) consinfo
                , (SELECT   t507.c205_part_number_id p_num, TRUNC(t506.c506_credit_date,'month') sdate,                          
                         SUM (t507.c507_item_qty )  r_qty
                    FROM t506_returns t506,
                         t507_returns_item t507, 
                         my_temp_list distlist
                   WHERE t506.c701_distributor_id IS NOT NULL
                     AND t506.c701_distributor_id = distlist.my_temp_txn_id
                     AND t506.c506_void_fl IS NULL
                     AND t506.c506_status_fl = '2'
                     AND t506.c207_set_id IS NULL
                     AND t506.c506_rma_id = t507.c506_rma_id
                     --Need to take the RA that is Credited (Exclude Write-off)
                     --AND t507.c507_status_fl IN ('C', 'W')
                     AND t507.c507_status_fl='C'
                     --Exclude RA Created from FA.
                     AND (t506.C901_TXN_SOURCE is NULL OR t506.C901_TXN_SOURCE <>'102680')
                     AND t506.C5040_plant_id IN  (SELECT C5040_plant_id FROM temp_plant_id)
--                     (
--			                SELECT my_temp_txn_id FROM my_temp_key_value
--			                 WHERE my_temp_txn_key = 'RGN_COMP_PLANT_FLAG')                     
                     AND NOT EXISTS (
                            SELECT c921_ref_id
                              FROM t920_transfer t920, t921_transfer_detail t921
                             WHERE t920.c920_transfer_id = t921.c920_transfer_id
                               AND c921_ref_id = t506.c506_rma_id
                               AND t920.c920_void_fl IS NULL
                               AND t920.c920_transfer_date >= p_from_period
                               AND t920.c920_transfer_date <= p_to_period
                               AND t921.c901_link_type = 90361)
                     AND t506.c506_credit_date >= p_from_period
                     AND t506.c506_credit_date <= p_to_period
                GROUP BY t507.c205_part_number_id, TRUNC(t506.c506_credit_date,'month')) returninfo
         WHERE partinfo.month_value = consinfo.sdate(+)
           AND partinfo.p_num = consinfo.p_num (+)
           AND partinfo.month_value = returninfo.sdate(+)
           AND partinfo.p_num = returninfo.p_num (+);
           

BEGIN
    -- ****************************
    -- load demand item information
    -- ****************************
    FOR set_val IN demand_item_cur
    LOOP
        --
        --DBMS_OUTPUT.put_line ('Sheet ID  ****' || set_val.c205_part_number_id || ' - ' || set_val.item_qty);
        gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, -9999, 102661, 50560, set_val.month_value,
        set_val.p_num, set_val.cons_qty) ;
        --
    END LOOP;
    

    --
END gm_op_ld_demand_fa_item;
   

/*************************************************************************
* Purpose: Procedure used to load inhosue consignment demand for selected period
*************************************************************************/
PROCEDURE gm_op_ld_demand_inhosue_set (
        p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
        p_from_period     IN DATE,
        p_to_period       IN DATE)
AS
    -- **************************************
    -- fetch Aggregiated set information
    -- ************************************** 
    CURSOR demand_set_cur
    IS
        SELECT partinfo.setid, partinfo.month_value, NVL (c_qty, 0) -  NVL (r_qty, 0) cons_qty
      FROM (SELECT setlist.my_temp_txn_id setid, v9001.month_value
              FROM my_temp_key_value setlist, v9001_month_list v9001
             WHERE  setlist.my_temp_txn_key = 'SET'
               AND v9001.month_value >= p_from_period
               AND v9001.month_value <= p_to_period 
               ORDER BY setlist.my_temp_txn_id,v9001.month_value
               ) partinfo,
           (SELECT   t504.c207_set_id setid,
                     TRUNC(t504.c504_ship_date,'month') sdate,
                     COUNT (1) c_qty
                FROM t504_consignment t504,
                     my_temp_key_value setlist
               WHERE t504.c701_distributor_id IS  NULL
                 AND t504.c704_account_id = '01'
                 AND t504.c207_set_id IS NOT NULL
                 AND t504.c207_set_id = setlist.my_temp_txn_id
                 AND setlist.my_temp_txn_key = 'SET'
                 AND t504.C5040_plant_id IN  (SELECT C5040_plant_id FROM temp_plant_id)
--                 (
--			                SELECT my_temp_txn_id FROM my_temp_key_value
--			                 WHERE my_temp_txn_key = 'RGN_COMP_PLANT_FLAG')                 
                 -- Remove the consignment transfers
                 AND NOT EXISTS (
                        SELECT c921_ref_id
                          FROM t920_transfer t920, t921_transfer_detail t921
                         WHERE t920.c920_transfer_id = t921.c920_transfer_id
                           AND c921_ref_id = t504.c504_consignment_id
                           AND t920.c920_void_fl IS NULL
                           AND t920.c920_transfer_date >= p_from_period
                           AND t920.c920_transfer_date <= p_to_period
                           AND t921.c901_link_type = 90360)
                 AND t504.c504_ship_date >= p_from_period
                 AND t504.c504_ship_date <= p_to_period
                 AND t504.c504_status_fl = '4'
                 AND t504.c504_void_fl IS NULL
            GROUP BY t504.c207_set_id, TRUNC(t504.c504_ship_date,'month')) consinfo,
           (SELECT   t506.c207_set_id setid,
                     TRUNC(t506.c506_credit_date,'month') sdate,
                     COUNT (1) r_qty
                FROM t506_returns t506,
                     MY_TEMP_KEY_VALUE setlist
               WHERE t506.c701_distributor_id IS NULL
                 AND t506.c704_account_id = '01'
                 AND t506.c506_void_fl IS NULL
                 AND t506.c506_status_fl = '2'
                 AND t506.c207_set_id IS NOT NULL
                 AND t506.c207_set_id = setlist.my_temp_txn_id
                 AND setlist.my_temp_txn_key = 'SET'
                 AND t506.C5040_plant_id IN  (SELECT C5040_plant_id FROM temp_plant_id)
--                 (
--			                SELECT my_temp_txn_id FROM my_temp_key_value
--			                 WHERE my_temp_txn_key = 'RGN_COMP_PLANT_FLAG')                 
                 AND NOT EXISTS (
                        SELECT c921_ref_id
                          FROM t920_transfer t920, t921_transfer_detail t921
                         WHERE t920.c920_transfer_id = t921.c920_transfer_id
                           AND c921_ref_id = t506.c506_rma_id
                           AND t920.c920_void_fl IS NULL
                           AND t920.c920_transfer_date >= p_from_period
                           AND t920.c920_transfer_date <= p_to_period
                           AND t921.c901_link_type = 90361)
                 AND t506.c506_credit_date >= p_from_period
                 AND t506.c506_credit_date <= p_to_period
            GROUP BY t506.c207_set_id, TRUNC(t506.c506_credit_date,'month')) returninfo
     WHERE partinfo.month_value = consinfo.sdate(+)
       AND partinfo.setid = consinfo.setid(+)
       AND partinfo.month_value = returninfo.sdate(+)
       AND partinfo.setid = returninfo.setid(+) ;
    
    -- ******************************
    -- Only Set Part Details 
    -- ******************************
    CURSOR demand_item_cur
    IS
        SELECT partinfo.setid, partinfo.p_num, partinfo.month_value, NVL (c_qty, 0) -  NVL (r_qty, 0) cons_qty
          FROM (SELECT DISTINCT setlist.my_temp_txn_id setid, t208.c205_part_number_id p_num, v9001.month_value
                  FROM my_temp_key_value setlist, t208_set_details t208, v9001_month_list v9001
                 WHERE setlist.my_temp_txn_key = 'SET'
                   AND setlist.my_temp_txn_id  = t208.c207_set_id
                   AND t208.c208_inset_fl           = 'Y'
                   AND t208.c208_void_fl           IS NULL
                   AND v9001.month_value >= p_from_period
                   AND v9001.month_value <= p_to_period 
                   ORDER BY setlist.my_temp_txn_id , t208.c205_part_number_id , v9001.month_value
                   ) partinfo,
               (SELECT   t504.c207_set_id setid, t505.c205_part_number_id p_num,
                         TRUNC(t504.c504_ship_date,'month') sdate,
                         SUM (t505.c505_item_qty) c_qty
                    FROM t504_consignment t504,
                         t505_item_consignment t505, 
                         my_temp_key_value setlist
                   WHERE t504.c701_distributor_id IS NULL
                     AND t504.c704_account_id      = '01'
                     AND t504.c504_consignment_id  = t505.c504_consignment_id
                     AND t504.c207_set_id IS NOT NULL
                     AND t504.c207_set_id = setlist.my_temp_txn_id
                     AND setlist.my_temp_txn_key = 'SET'
                     AND t504.C5040_plant_id IN  (SELECT C5040_plant_id FROM temp_plant_id)
--                     (
--			                SELECT my_temp_txn_id FROM my_temp_key_value
--			                 WHERE my_temp_txn_key = 'RGN_COMP_PLANT_FLAG')                     
                     -- Remove the consignment transfers
                     AND NOT EXISTS (
                            SELECT c921_ref_id
                              FROM t920_transfer t920, t921_transfer_detail t921
                             WHERE t920.c920_transfer_id = t921.c920_transfer_id
                               AND c921_ref_id = t504.c504_consignment_id
                               AND t920.c920_void_fl IS NULL
                               AND t920.c920_transfer_date >= p_from_period
                               AND t920.c920_transfer_date <= p_to_period 
                               AND t921.c901_link_type = 90360)
                     AND t504.c504_ship_date >= p_from_period
                     AND t504.c504_ship_date <= p_to_period
                     AND t504.c504_status_fl = '4'
                     AND t504.c504_void_fl IS NULL
                     AND TRIM (t505.c505_control_number) IS NOT NULL
                     AND (t505.c901_type                 IS NULL
                        OR t505.c901_type                   <> 100880)
                GROUP BY t504.c207_set_id, t505.c205_part_number_id, TRUNC(t504.c504_ship_date,'month')) consinfo,
               (SELECT   t506.c207_set_id setid,
                         TRUNC(t506.c506_credit_date,'month') sdate,                          
                         t507.c205_part_number_id p_num, SUM (t507.c507_item_qty )  r_qty
                    FROM t506_returns t506,
                         t507_returns_item t507, 
                         my_temp_key_value setlist
                   WHERE t506.c701_distributor_id IS NULL
                     AND t506.c704_account_id = '01'
                     AND t506.c506_void_fl IS NULL
                     AND t506.c506_status_fl = '2'
                     AND t506.c207_set_id IS NOT NULL
                     AND t506.c207_set_id = setlist.my_temp_txn_id
                     AND t506.c506_rma_id = t507.c506_rma_id
                     AND t507.c507_status_fl IN ('C', 'W')
                     AND setlist.my_temp_txn_key = 'SET'
                     AND t506.C5040_plant_id IN  (SELECT C5040_plant_id FROM temp_plant_id)
--                     (
--			                SELECT my_temp_txn_id FROM my_temp_key_value
--			                 WHERE my_temp_txn_key = 'RGN_COMP_PLANT_FLAG')                     
                     AND NOT EXISTS (
                            SELECT c921_ref_id
                              FROM t920_transfer t920, t921_transfer_detail t921
                             WHERE t920.c920_transfer_id = t921.c920_transfer_id
                               AND c921_ref_id = t506.c506_rma_id
                               AND t920.c920_void_fl IS NULL
                               AND t920.c920_transfer_date >= p_from_period
                               AND t920.c920_transfer_date <= p_to_period
                               AND t921.c901_link_type = 90361)
                     AND t506.c506_credit_date >= p_from_period
                     AND t506.c506_credit_date <= p_to_period
                GROUP BY t506.c207_set_id, t507.c205_part_number_id, TRUNC(t506.c506_credit_date,'month')) returninfo
         WHERE partinfo.month_value = consinfo.sdate(+)
           AND partinfo.setid = consinfo.setid(+)
           AND partinfo.p_num = consinfo.p_num (+)
           AND partinfo.month_value = returninfo.sdate(+)
           AND partinfo.setid = returninfo.setid(+)
           AND partinfo.p_num = returninfo.p_num (+);

BEGIN

    --******************************
    -- load demand set information
    --******************************
    FOR set_val IN demand_set_cur
    LOOP
        --
        --DBMS_OUTPUT.put_line ('Sheet ID  ****' || set_val.c205_part_number_id || ' - ' || set_val.item_qty);
        gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, set_val.setid, 102663, 50560, set_val.month_value,
                            NULL, set_val.cons_qty) ;
        --
    END LOOP;
    --
    -- load demand item information
    FOR set_val IN demand_item_cur
    LOOP
        --
        --DBMS_OUTPUT.put_line ('Sheet ID  ****' || set_val.c205_part_number_id || ' - ' || set_val.item_qty);
        gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, set_val.setid, 102662, 50560, set_val.month_value,
                        set_val.p_num, set_val.cons_qty) ;
        --
    END LOOP;
    --
END gm_op_ld_demand_inhosue_set;

/*************************************************************************
 * Purpose: Procedure used to load inhouse item for selected period
 *************************************************************************/
PROCEDURE gm_op_ld_demand_inhouse_item (
        p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
        p_from_period     IN DATE,
        p_to_period       IN DATE)
AS
    -- ******************************
    -- Only Item consignment Details 
    -- ******************************
    CURSOR demand_item_cur
    IS
        SELECT partinfo.p_num, partinfo.month_value,  NVL (c_qty, 0) -  NVL (r_qty, 0) cons_qty
          FROM (SELECT DISTINCT setpartlist.my_temp_txn_id p_num, v9001.month_value
                  FROM my_temp_key_value setpartlist, v9001_month_list v9001
                 WHERE setpartlist.my_temp_txn_key = 'ITEM'
                   AND v9001.month_value >= p_from_period
                   AND v9001.month_value <= p_to_period
                   ORDER BY setpartlist.my_temp_txn_id, v9001.month_value
                   ) partinfo,
               (SELECT   t505.c205_part_number_id p_num,
                         TRUNC(t504.c504_ship_date,'month') sdate,
                         SUM (t505.c505_item_qty) c_qty
                    FROM t504_consignment t504,
                         t505_item_consignment t505 
                   WHERE t504.c701_distributor_id IS NULL
                     AND t504.c704_account_id = '01'
                     AND t504.c504_consignment_id  = t505.c504_consignment_id
                     AND t504.c207_set_id IS NULL
                     AND t504.C5040_plant_id IN  (SELECT C5040_plant_id FROM temp_plant_id)
--                     (
--			                SELECT my_temp_txn_id FROM my_temp_key_value
--			                 WHERE my_temp_txn_key = 'RGN_COMP_PLANT_FLAG')                     
                     -- Remove excess from item consignent 
                     AND NOT EXISTS
                        (
                             SELECT a.c504_consignment_id a
                               FROM t504a_consignment_excess a
                              WHERE a.c504_consignment_id = t504.c504_consignment_id
                        )             
                     -- Remove the consignment transfers
                     AND NOT EXISTS (
                            SELECT c921_ref_id
                              FROM t920_transfer t920, t921_transfer_detail t921
                             WHERE t920.c920_transfer_id = t921.c920_transfer_id
                               AND c921_ref_id = t504.c504_consignment_id
                               AND t920.c920_void_fl IS NULL
                               AND t920.c920_transfer_date >= p_from_period
                               AND t920.c920_transfer_date <= p_to_period
                               AND t921.c901_link_type = 90360)
                     AND t504.c504_ship_date >= p_from_period
                     AND t504.c504_ship_date <= p_to_period
                     AND t504.c504_status_fl = '4'
                     AND t504.c504_void_fl IS NULL
                     AND TRIM (t505.c505_control_number) IS NOT NULL
                     AND (t505.c901_type   IS NULL
                        OR t505.c901_type                   <> 100880)
                GROUP BY t505.c205_part_number_id, TRUNC(t504.c504_ship_date,'month')) consinfo
                , (SELECT   t507.c205_part_number_id p_num, TRUNC(t506.c506_credit_date,'month') sdate,                          
                         SUM (t507.c507_item_qty )  r_qty
                    FROM t506_returns t506,
                         t507_returns_item t507
                   WHERE t506.c701_distributor_id IS NULL
                     AND t506.c704_account_id = '01'
                     AND t506.c506_void_fl IS NULL
                     AND t506.c506_status_fl = '2'
                     AND t506.c207_set_id IS NULL
                     AND t506.c506_rma_id = t507.c506_rma_id
                     AND t507.c507_status_fl IN ('C', 'W')
                     AND t506.C5040_plant_id IN  (SELECT C5040_plant_id FROM temp_plant_id)
--                     (
--			                SELECT my_temp_txn_id FROM my_temp_key_value
--			                 WHERE my_temp_txn_key = 'RGN_COMP_PLANT_FLAG')                     
                     AND NOT EXISTS (
                            SELECT c921_ref_id
                              FROM t920_transfer t920, t921_transfer_detail t921
                             WHERE t920.c920_transfer_id = t921.c920_transfer_id
                               AND c921_ref_id = t506.c506_rma_id
                               AND t920.c920_void_fl IS NULL
                               AND t920.c920_transfer_date >= p_from_period
                               AND t920.c920_transfer_date <= p_to_period
                               AND t921.c901_link_type = 90361)
                     AND t506.c506_credit_date >= p_from_period
                     AND t506.c506_credit_date <= p_to_period
                GROUP BY t507.c205_part_number_id, TRUNC(t506.c506_credit_date,'month')) returninfo
         WHERE partinfo.month_value = consinfo.sdate(+)
           AND partinfo.p_num = consinfo.p_num (+)
           AND partinfo.month_value = returninfo.sdate(+)
           AND partinfo.p_num = returninfo.p_num (+);

BEGIN
    -- ****************************
    -- load demand item information
    -- ****************************
    FOR set_val IN demand_item_cur
    LOOP
        --
        --DBMS_OUTPUT.put_line ('Sheet ID  ****' || set_val.c205_part_number_id || ' - ' || set_val.item_qty);
        gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, -9999, 102661, 50560, set_val.month_value,
        set_val.p_num, set_val.cons_qty) ;
        --
    END LOOP;
    
END gm_op_ld_demand_inhouse_item;

    --
    /*************************************************************************
    * Purpose: Procedure used to capture the DEMAND of all the written off Parts in the
    * Loaner Reconciliation process.
    * -99991 -- Write off part group id
    *************************************************************************/
    PROCEDURE gm_op_ld_demand_lnr_writeoff (
            p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE,
            p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
            p_from_period     IN DATE,
            p_to_period       IN DATE)
    AS
               
        CURSOR write_off_part_cur
        IS
             SELECT partinfo.p_num 
             ,consinfo.sdate, NVL (c_qty, 0) item_qty
             , partinfo.month_value
               FROM
                (SELECT DISTINCT setpartlist.my_temp_txn_id p_num, v9001.month_value
                      FROM my_temp_key_value setpartlist
                         , v9001_month_list v9001
                     WHERE setpartlist.my_temp_txn_key = 'ITEM' 
                       AND v9001.month_value >= p_from_period
                       AND v9001.month_value <= p_to_period
                       ORDER BY setpartlist.my_temp_txn_id , v9001.month_value
                       ) partinfo
              , (SELECT TRUNC(t413.c413_created_date,'month') sdate,
                   t413.c205_part_number_id p_num, SUM (t413.c413_item_qty) c_qty
                   FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
                  WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
                    AND t412.c412_status_fl        = '4'
                    AND t412.c412_void_fl         IS NULL
                    AND t412.C5040_plant_id IN  (SELECT C5040_plant_id FROM temp_plant_id)
--                    (
--			                SELECT my_temp_txn_id FROM my_temp_key_value
--			                 WHERE my_temp_txn_key = 'RGN_COMP_PLANT_FLAG')                    
                    AND t413.c413_status_fl        = 'W'
                    AND t413.c413_void_fl         IS NULL
                    AND t413.c413_created_date >= p_from_period
                    AND t413.c413_created_date <= p_to_period
                  GROUP BY t413.c205_part_number_id, TRUNC(t413.c413_created_date,'month')
            ) consinfo
          WHERE partinfo.month_value = consinfo.sdate(+) 
            AND partinfo.p_num = consinfo.p_num (+);
    BEGIN
        FOR write_off_parts IN write_off_part_cur
        LOOP
        --
            gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, '-99991', 102661,  50560, 
                write_off_parts.month_value, write_off_parts.p_num, write_off_parts.item_qty) ;
        --
        END LOOP;
        
        -- Delete the item consignment Part which does not contain any item consignment
        -- for selected period
         DELETE
           FROM my_temp_demand_sheet_detail
          WHERE c901_type             = 50560
            AND c4040_demand_sheet_id = p_demand_sheet_id
            AND c4042_ref_id          = '-99991'
            AND c205_part_number_id  IN
            (
                 SELECT t4042.c205_part_number_id
                   FROM my_temp_demand_sheet_detail t4042
                  WHERE t4042.c901_type             = 50560
                    AND t4042.c4040_demand_sheet_id = p_demand_sheet_id
                    AND t4042.c4042_ref_id          = '-99991'
               GROUP BY t4042.c205_part_number_id
                HAVING SUM (c4042_qty) = 0
            );         
    END gm_op_ld_demand_lnr_writeoff;
    --
    /*************************************************************************
    * Purpose: Procedure used to load sales forecast information
    * Based on the demand weighted will load forecase information
    *************************************************************************/
    PROCEDURE gm_op_ld_forecast (
            p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE,
            p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
            p_from_period     IN DATE,
            p_to_period       IN DATE)
    AS
    	v_history_fl            CHAR (1);
        v_comments_fl           CHAR (1);
        v_weighted_avg_diff     t4042_demand_sheet_detail.c4042_weighted_avg_diff_qty%TYPE;
        v_current_qty           t4042_demand_sheet_detail.c4042_qty%TYPE;
        
        -- Set Level Forecast
        CURSOR forecast_cur_set
        IS
            SELECT   setinfo.setid, setinfo.month_value,
                     setgrowthinfo.c901_ref_type growth_type,
                     setgrowthinfo.c4031_value growth_value, dmdweightavg.weight_avg,
                     setgrowthinfo.comments_fl comments_fl
                FROM (SELECT t4021.c4021_ref_id setid, v9001.month_value
                        FROM t4021_demand_mapping t4021, v9001_month_list v9001
                       WHERE t4021.c4020_demand_master_id = p_demand_id
                         AND t4021.c901_ref_type in (40031, 4000109)
                         AND v9001.month_value >= p_from_period
                         AND v9001.month_value <= p_to_period) setinfo
                     -- Fetch Set Override info
                    ,(SELECT t4030.c4030_ref_id setid, t4031.c4031_start_date,
                             t4031.c4031_end_date, t4031.c901_ref_type, t4031.c4031_value,
                             NULL comments_fl
                        FROM t4020_demand_master t4020,
                             t4030_demand_growth_mapping t4030,
                             t4031_growth_details t4031
                       WHERE t4020.c4020_demand_master_id = p_demand_id
                         AND t4020.c4020_demand_master_id = t4030.c4020_demand_master_id
                         AND t4031.c4030_demand_growth_id = t4030.c4030_demand_growth_id
                         AND t4030.c901_ref_type = 20296
                         AND t4031.c4031_start_date >= p_from_period
                         AND t4031.c4031_end_date <= p_to_period
                         AND t4030.c4030_void_fl IS NULL
                         AND t4031.c4031_void_fl IS NULL
                         AND t4031.c4031_value IS NOT NULL) setgrowthinfo
                     -- Fetch Weighted Average info
                    ,(SELECT t4042.c4042_ref_id setid, t4042.c4042_period,
                             t4042.c4042_qty weight_avg
                        FROM my_temp_demand_sheet_detail t4042
                       WHERE t4042.c4040_demand_sheet_id = p_demand_sheet_id
                         AND t4042.c901_type = 50562
                         AND t4042.c205_part_number_id IS NULL) dmdweightavg
               WHERE setinfo.month_value = setgrowthinfo.c4031_start_date(+)
                 AND setinfo.setid = setgrowthinfo.setid(+)
                 AND setinfo.setid = dmdweightavg.setid(+)
            ORDER BY setinfo.setid, setinfo.month_value;        

        -- Cursor to fetch set detail based on forecasted qty 
        CURSOR forecast_cur_set_detail (  v_set_id t208_set_details.c207_set_id%TYPE
                                        , v_set_qty t208_set_details.c208_set_qty%TYPE)
        IS
            SELECT t208.c207_set_id, t208.c205_part_number_id,
                   (t208.c208_set_qty * v_set_qty) c208_set_qty
              FROM t208_set_details t208
             WHERE t208.c207_set_id = v_set_id
               AND t208.c208_inset_fl = 'Y'
               AND t208.c208_set_qty <> 0
               AND t208.c208_void_fl IS NULL ;
                 
        -- Cursor to fetch item forecast information based on item     
        CURSOR forecast_cur_item_part
        IS
            SELECT   iteminfo.p_num, iteminfo.month_value,
                     itemgrowthinfo.c901_ref_type growth_type,
                     itemgrowthinfo.c4031_value growth_value, dmdweightavg.weight_avg,
                     itemgrowthinfo.comments_fl comments_fl, 
                     iteminfo.my_temp_txn_value primaryfl
                FROM (  
                SELECT DISTINCT setpartlist.my_temp_txn_id p_num, v9001.month_value
                             , setpartlist.my_temp_txn_value
                        FROM my_temp_key_value setpartlist, v9001_month_list v9001
                       WHERE setpartlist.my_temp_txn_key = 'ITEM' 
                         AND v9001.month_value >= p_from_period
                         AND v9001.month_value <= p_to_period
                         ) iteminfo
                     -- Fetch Item Override info
                    ,(SELECT t4030.c4030_ref_id p_num, t4031.c4031_start_date,
                             t4031.c4031_end_date, t4031.c901_ref_type, t4031.c4031_value,
                             NULL comments_fl
                        FROM t4020_demand_master t4020,
                             t4030_demand_growth_mapping t4030,
                             t4031_growth_details t4031
                       WHERE t4020.c4020_demand_master_id = p_demand_id
                         AND t4020.c4020_demand_master_id = t4030.c4020_demand_master_id
                         AND t4031.c4030_demand_growth_id = t4030.c4030_demand_growth_id
                         AND t4030.c901_ref_type = 20298
                         AND t4031.c4031_start_date >= p_from_period
                         AND t4031.c4031_end_date <= p_to_period
                         AND t4030.c4030_void_fl IS NULL
                         AND t4031.c4031_void_fl IS NULL
                         AND t4031.c4031_value IS NOT NULL) itemgrowthinfo
                     -- Fetch Weighted Average info
                    ,(SELECT t4042.c205_part_number_id p_num, t4042.c4042_period,
                             t4042.c4042_qty weight_avg
                        FROM my_temp_demand_sheet_detail t4042
                       WHERE t4042.c4040_demand_sheet_id = p_demand_sheet_id
                         AND t4042.c901_type = 50562
                         AND t4042.c4042_ref_id = '-9999') dmdweightavg
               WHERE iteminfo.month_value = itemgrowthinfo.c4031_start_date(+)
                 AND iteminfo.p_num = itemgrowthinfo.p_num(+)
                 AND iteminfo.p_num = dmdweightavg.p_num(+)
            ORDER BY iteminfo.p_num, iteminfo.month_value; 

        -- Cursor to fetch write-off forecast information based on item
        CURSOR forecast_cur_writeoff_part
        IS
            SELECT   iteminfo.p_num, iteminfo.month_value,
                     NULL growth_type,
                     NULL growth_value, dmdweightavg.weight_avg,
                     NULL comments_fl
                FROM (  
                SELECT DISTINCT setpartlist.my_temp_txn_id p_num, v9001.month_value
                        FROM my_temp_key_value setpartlist, v9001_month_list v9001
                       WHERE setpartlist.my_temp_txn_key = 'ITEM' 
                         AND setpartlist.my_temp_txn_value = 'Y'   
                         AND v9001.month_value >= p_from_period
                         AND v9001.month_value <= p_to_period
                         ) iteminfo
                     -- Fetch Weighted Average info
                    ,(SELECT t4042.c205_part_number_id p_num, t4042.c4042_period,
                             t4042.c4042_qty weight_avg
                        FROM my_temp_demand_sheet_detail t4042
                       WHERE t4042.c4040_demand_sheet_id = p_demand_sheet_id
                         AND t4042.c901_type = 50562
                         AND t4042.c4042_ref_id = '-99991') dmdweightavg
               WHERE iteminfo.p_num = dmdweightavg.p_num
            ORDER BY iteminfo.p_num, iteminfo.month_value; 
        
    BEGIN
        -- *******************************
        -- load forecast set information
        -- *******************************
        FOR set_val IN forecast_cur_set
        LOOP

        	-- To get forecast value 
            gm_pkg_oppr_ld_demand.gm_op_calc_forecast (set_val.growth_type,
                                   set_val.growth_value,
                                   set_val.weight_avg,
                                   'Y',
                                   v_current_qty,
                                   v_weighted_avg_diff,
                                   v_history_fl
                                  );
                                  
                                    
            -- Save Set forecast information
            gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, set_val.setid, 102663, 50563, set_val.month_value,
                            NULL, v_current_qty, NULL, NULL, NULL, v_history_fl, set_val.comments_fl, v_weighted_avg_diff) ;
            --
            FOR set_detail IN forecast_cur_set_detail(set_val.setid,v_current_qty)
            LOOP
            --
                gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, set_val.setid, 102662, 50563, set_val.month_value,
                             set_detail.c205_part_number_id , set_detail.c208_set_qty, NULL, NULL, NULL, v_history_fl
                             , set_val.comments_fl, v_weighted_avg_diff) ;
            --
            END LOOP;
                
        END LOOP;
    
    
        --***********************************
        -- load forecast item information
        --***********************************    
        FOR set_item_val IN forecast_cur_item_part
        LOOP
            
            -- If not primary part and not unit should skip the forecasr 
            IF ( set_item_val.primaryfl = 'N' and NVL(set_item_val.growth_type,0) != 20381)
            THEN
                v_current_qty := 0;
                v_weighted_avg_diff := NULL;
                v_history_fl := NULL;
            ELSE  
                -- To get forecast value 
                gm_pkg_oppr_ld_demand.gm_op_calc_forecast (set_item_val.growth_type,
                                       set_item_val.growth_value,
                                       set_item_val.weight_avg,
                                        set_item_val.primaryfl,
                                       v_current_qty,
                                       v_weighted_avg_diff,
                                       v_history_fl
                                      );
            END IF;            
            -- Save Item aggregated value 
            gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id,  -9999, 102661, 50563, set_item_val.month_value,
                             set_item_val.p_num , v_current_qty, NULL, NULL, NULL, v_history_fl, set_item_val.comments_fl, v_weighted_avg_diff) ;
            --
        END LOOP;
        
        --***********************************
        -- load forecast for item write-off 
        --***********************************    
        FOR set_wrieoff_val IN forecast_cur_writeoff_part
        LOOP
            -- To get forecast value 
            gm_pkg_oppr_ld_demand.gm_op_calc_forecast (set_wrieoff_val.growth_type,
                                   set_wrieoff_val.growth_value,
                                   set_wrieoff_val.weight_avg,
                                   'Y',
                                   v_current_qty,
                                   v_weighted_avg_diff,
                                   v_history_fl
                                  );
        
            -- Save Item aggregated value 
            gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, -99991, 102661, 50563, set_wrieoff_val.month_value,
                             set_wrieoff_val.p_num , v_current_qty, NULL, NULL, NULL, v_history_fl, set_wrieoff_val.comments_fl, v_weighted_avg_diff) ;
            --
        END LOOP;
    
    END gm_op_ld_forecast;

    /*************************************************************************
    * Purpose: Procedure used to load sheet OTN Information
    * OTN -- One Time need
    * OTN -- Only Appliciable for Items
    *************************************************************************/
    PROCEDURE gm_op_ld_otn (
            p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE,
            p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
            p_to_period       IN DATE)
    AS
        --
        CURSOR otn_cur
        IS
            SELECT t4023.c205_part_number_id, t4023.c4023_qty  otnvalue
               FROM t4023_demand_otn_detail t4023
          WHERE t4023.c4020_demand_master_id = p_demand_id
          AND   C4023_VOID_FL IS NULL;
        
    BEGIN
        -- load par information
        FOR otn_val IN otn_cur
        LOOP
        	
            -- Load par Info
            gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, -9999, 102661, 4000112, p_to_period,
            otn_val.c205_part_number_id, otn_val.otnvalue, NULL, NULL, NULL) ;
            --
        END LOOP;
        --
    END gm_op_ld_otn;

    --
    /*************************************************************************
    * Purpose: Procedure used to load Pending Requests
    *************************************************************************/
    PROCEDURE gm_op_ld_pending_request (
            p_demand_master_id IN t4020_demand_master.c4020_demand_master_id%TYPE,
            p_demand_sheet_id  IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
            p_from_period      IN DATE,
            p_inventory_id     IN t250_inventory_lock.c250_inventory_lock_id%TYPE)
    AS
        --
        
        CURSOR prequest_calc_cur
        IS
             SELECT temp.ref_id ref_id, temp.p_num p_num, SUM (NVL (temp.qty, 0)) qty
              , DECODE (temp.sfl, 10, 50567, 15, 50568, 20, 50569) code_id
               FROM
                (
                     SELECT NVL (t520.c207_set_id, - 9999) ref_id, t521.c205_part_number_id p_num, t521.c521_qty qty
                      , DECODE (c520_status_fl, 30, 20, c520_status_fl) sfl, t520.c520_required_date rdate
                       FROM t253c_request_lock t520, t253d_request_detail_lock t521
                      WHERE t520.c520_request_txn_id    = p_demand_master_id
                        AND t520.c901_request_source    = '50616'
                        AND t520.c520_request_id        = t521.c520_request_id
                        AND t520.c250_inventory_lock_id = p_inventory_id
                        AND t520.c250_inventory_lock_id = t521.c250_inventory_lock_id
                        AND t520.c520_void_fl          IS NULL
                        --AND t520.c520_delete_fl IS NULL
                        AND t520.c520_status_fl     > 0
                        AND t520.c520_status_fl     < 40
                        AND t520.c520_required_date < p_from_period
              UNION ALL
                 SELECT NVL (t520.c207_set_id, - 9999) ref_id, t505.c205_part_number_id p_num, t505.c505_item_qty qty
                  , DECODE (c520_status_fl, 30, 20, c520_status_fl) sfl, t520.c520_required_date rdate
                   FROM t253c_request_lock t520, t253a_consignment_lock t504, t253b_item_consignment_lock t505
                  WHERE t520.c520_request_txn_id    = p_demand_master_id
                    AND t520.c901_request_source    = '50616'
                    AND t520.c250_inventory_lock_id = p_inventory_id
                    AND t520.c520_request_id        = t504.c520_request_id
                    AND t520.c250_inventory_lock_id = t504.c250_inventory_lock_id
                    AND t504.c504_consignment_id    = t505.c504_consignment_id
                    AND t504.c250_inventory_lock_id = t505.c250_inventory_lock_id
                    AND t520.c520_void_fl          IS NULL
                    --AND t520.c520_delete_fl IS NULL
                    AND t520.c520_status_fl     > 0
                    AND t520.c520_status_fl     < 40
                    AND t520.c520_required_date < p_from_period
                )
                temp
           GROUP BY temp.ref_id, temp.p_num, temp.sfl;
           
            CURSOR open_requests
            IS
                 SELECT t520.c520_request_id request_id, t520.c520_status_fl sfl
                        , t520.c207_set_id
                        , DECODE (t520.c520_status_fl, 10, 50567, 15, 50568, 20, 50569, 30, 50569) code_id 
                   FROM t253c_request_lock t520
                  WHERE t520.c520_request_txn_id    = p_demand_master_id
                    AND t520.c250_inventory_lock_id = p_inventory_id
                    AND t520.c901_request_source    = '50616'
                    AND t520.c520_void_fl          IS NULL
                    --AND t520.c520_delete_fl IS NULL
                    AND t520.c520_status_fl          > 0
                    AND t520.c520_status_fl          < 40
                    AND t520.c520_master_request_id IS NULL;

            -- Open Set Request (if any) 
            CURSOR open_set_request
            IS
                 SELECT t520.c207_set_id
                        , DECODE (t520.c520_status_fl, 10, 50567, 15, 50568, 20, 50569,30, 50569) code_id
                        , COUNT(1) qty 
                   FROM t253c_request_lock t520
                  WHERE t520.c520_request_txn_id    = p_demand_master_id
                    AND t520.c250_inventory_lock_id = p_inventory_id
                    AND t520.c901_request_source    = '50616'
                    AND t520.c520_void_fl          IS NULL
                    --AND t520.c520_delete_fl IS NULL
                    AND t520.c520_status_fl          > 0
                    AND t520.c520_status_fl          < 40
                    AND t520.c520_required_date 	 < p_from_period
                    AND t520.c520_master_request_id IS NULL
                  GROUP BY  t520.c207_set_id, t520.c520_status_fl ;

        BEGIN
            -- load pending request information
            FOR val IN prequest_calc_cur
            LOOP
                gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, val.ref_id, 102662, val.code_id, p_from_period,
                val.p_num, val.qty, NULL, NULL, NULL) ;
                --
            END LOOP;
            
            -- Load Open set request 
            FOR val IN open_set_request
            LOOP
                -- Set Level forecast  
                gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, val.c207_set_id, 102663, val.code_id, p_from_period,
                NULL, val.qty, NULL, NULL, NULL) ;
 
            END LOOP;
            
            FOR val IN open_requests
            LOOP
                gm_pkg_oppr_ld_demand.gm_sav_demand_sheet_request (p_demand_sheet_id, val.request_id, 50632,val.sfl) ;
            END LOOP;
            --
        END gm_op_ld_pending_request;
        --


-- 
END gm_pkg_oppr_ld_consign_demand;
/
