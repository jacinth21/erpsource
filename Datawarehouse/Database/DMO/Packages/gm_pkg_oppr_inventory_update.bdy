/* Formatted on 2009/09/30 15:50 (Formatter Plus v4.8.0) */

--@"c:\database\packages\operations\purchasing\gm_pkg_oppr_inventory_update.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_inventory_update IS
  /*******************************************************
  * Description : Main Procedure for void open Request
  author: Xun
  *******************************************************/
  PROCEDURE gm_op_sav_tpr_clear_request
  (
    p_request_id   IN t520_request.c520_request_id%TYPE,
    p_action_type  IN VARCHAR2,
    p_user_id      IN t520_request.c520_created_by%TYPE,
    p_old_req_date IN t520_request.c520_required_date%TYPE,
    p_dsmonthid    IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE DEFAULT NULL
  ) AS
    v_request_id       t520_request.c520_request_id%TYPE;
    v_lock_status_fl   VARCHAR2(100);
    v_demand_period_dt DATE;
    v_newreq_dt        NUMBER;
    v_invlockdt        NUMBER;
    v_sheet_id         t4040_demand_sheet.c4040_demand_sheet_id%TYPE;
    v_set_id           t520_request.c207_set_id%TYPE;
    v_inv_lock_id      t4040_demand_sheet.c250_inventory_lock_id%TYPE;
    v_log_action       T901_code_lookup.c901_code_id%TYPE;
    v_new_req_date     t520_request.c520_required_date%TYPE;
    v_source           t520_request.c901_request_source%TYPE;
    v_master_sheet_id  t520_request.c520_request_txn_id%TYPE;
    v_status           t520_request.C520_STATUS_FL%TYPE;
    v_action_type      VARCHAR2(50) := p_action_type;
    v_count            NUMBER;
    v_level_id         t4015_global_sheet_mapping.C901_LEVEL_ID%TYPE;
    v_level_value      t4015_global_sheet_mapping.C901_LEVEL_VALUE%TYPE;
    v_process_clear       VARCHAR2(1) := 'Y';
  
  BEGIN
  
    SELECT t520.c520_request_txn_id, t520.c901_request_source
      INTO v_master_sheet_id, v_source
      FROM t520_request t520
     WHERE t520.c520_request_id =
           (SELECT t520a.c520_master_request_id
              FROM t520_request t520a
             WHERE t520a.c520_request_id = p_request_id)
           OR (t520.c520_request_id = p_request_id AND
           t520.c520_master_request_id IS NULL);
  
    IF v_source = 50616
    THEN
      -- order planning request only
    
      SELECT TO_NUMBER(TO_CHAR(t520.c520_required_date, 'YYYYMM')) newreqdt,
             t520.c207_set_id, t520.c520_required_date, C520_STATUS_FL
        INTO v_newreq_dt, v_set_id, v_new_req_date, v_status
        FROM t520_request t520
       WHERE t520.c520_request_id = p_request_id;
    
      DBMS_OUTPUT.PUT_LINE('v_master_sheet_id' || v_master_sheet_id);
      SELECT t4040.c4040_demand_sheet_id
        INTO v_sheet_id
        FROM t4040_demand_sheet t4040
       WHERE t4040.c4020_demand_master_id = v_master_sheet_id
             AND
             t4040.c4040_demand_period_dt =
             TO_DATE('01/' || TO_CHAR(SYSDATE, 'MM/YYYY'), 'DD/MM/YYYY')
             AND t4040.c901_status = 50550; -- open sheet
    
      DBMS_OUTPUT.PUT_LINE('v_master_sheet_id' || v_master_sheet_id ||
                           'v_sheet_id' || v_sheet_id);
    
      SELECT t4040.c250_inventory_lock_id, t4040.c4040_demand_period_dt,
             TO_NUMBER(TO_CHAR(t250.c250_lock_date, 'YYYYMM')) invlockdt
        INTO v_inv_lock_id, v_demand_period_dt, v_invlockdt
        FROM t4040_demand_sheet t4040, t250_inventory_lock t250
       WHERE t4040.c4040_demand_sheet_id = v_sheet_id
             AND t250.c250_inventory_lock_id = t4040.c250_inventory_lock_id;
    
    ELSE
      RETURN;
    END IF;
    DBMS_OUTPUT.PUT_LINE('v_status' || v_status || 'v_inv_lock_id' ||
                         v_inv_lock_id);
    -- If the Status of the Request is 10 and when the action is common pool,we should void the request, as it is in BO status.
    IF (v_status = '10' AND v_action_type = 'common_pool') -- Back Order
    THEN
      v_action_type := 'void';      
    END IF;
  
    IF (v_action_type = 'void')
    THEN
      gm_pkg_oppr_inventory_update.gm_op_sav_tpr_void_request(p_request_id,
                                                              p_user_id,
                                                              v_inv_lock_id);
      v_log_action := 90850;
    
      IF (v_newreq_dt < v_invlockdt)
      THEN
        gm_pkg_oppr_inventory_update.gm_op_sav_tpr_clear_pbo (p_request_id
                                  , p_user_id
                                  , v_inv_lock_id
                                  , v_sheet_id
                                  , v_set_id
                                  , 1
                                   );
        gm_pkg_oppr_inventory_update.gm_op_sav_tpr_clear_pbl (p_request_id
                                  , p_user_id
                                  , v_inv_lock_id
                                  , v_sheet_id
                                  , v_set_id
                                  , 1
                                   );
        gm_pkg_oppr_inventory_update.gm_op_sav_tpr_clear_pcs (p_request_id
                                  , p_user_id
                                  , v_inv_lock_id
                                  , v_sheet_id
                                  , v_set_id
                                  , 1
                                   );
        
        v_action_type := 'common_pool';
        v_process_clear := 'N';
      END IF;
    END IF;
  
    IF (v_action_type = 'common_pool')
    THEN
      DBMS_OUTPUT.PUT_LINE('Inside Common Pool');
      -- Set the Required date to NULL, Request Txn ID to NULL. 
      UPDATE t253c_request_lock
         SET c520_required_date     = '',
             C520_REQUEST_TXN_ID    = '',
             c520_last_updated_by   = p_user_id,
             c520_last_updated_date = SYSDATE
       WHERE c520_request_id = p_request_id
             AND c250_inventory_lock_id = v_inv_lock_id
             AND c520_void_fl IS NULL;
    
      --Below SQL to update child record
      UPDATE t253c_request_lock
         SET c520_required_date     = '',
             C520_REQUEST_TXN_ID    = '',
             c520_last_updated_by   = p_user_id,
             c520_last_updated_date = SYSDATE
       WHERE c520_master_request_id = p_request_id
             AND c250_inventory_lock_id = v_inv_lock_id
             AND c520_void_fl IS NULL;
    
      IF v_master_sheet_id IS NOT NULL
      THEN
        BEGIN
          SELECT T4015.C901_LEVEL_ID, T4015.C901_LEVEL_VALUE
            INTO v_level_id, v_level_value
            FROM T4015_GLOBAL_SHEET_MAPPING T4015, T4020_DEMAND_MASTER T4020
           WHERE T4020.C4015_GLOBAL_SHEET_MAP_ID =
                 T4015.C4015_GLOBAL_SHEET_MAP_ID
                 AND C4020_DEMAND_MASTER_ID = v_master_sheet_id;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            v_level_id    := NULL;
            v_level_value := NULL;
        END;
      
        IF (v_newreq_dt < v_invlockdt)
     	THEN
	        IF (v_level_id IS NOT NULL AND v_level_value IS NOT NULL)
	        THEN
	          IF v_level_id = '102581' AND v_level_value = '100823' -- 102581(DIVISION), 100823 (US)
	          THEN
	            DBMS_OUTPUT.PUT_LINE('Inside Common v_status' || v_sheet_id ||
	                                 'v_set_id' || v_set_id || 'v_status' ||
	                                 v_status);
				
					                                 
		            -- CLEAR US TPR                     
		           gm_pkg_oppr_inventory_update.gm_op_sav_clear_tpr(p_request_id,
		                                                             p_user_id,
		                                                             v_inv_lock_id,
		                                                             v_sheet_id,
		                                                             v_set_id,
		                                                             1,
		                                                             '4000107' -- TPR US
		                                                            ,
		                                                             v_status);
		          ELSE
		            --CLEAR OUS TPR
		            gm_pkg_oppr_inventory_update.gm_op_sav_clear_tpr(p_request_id,
		                                                             p_user_id,
		                                                             v_inv_lock_id,
		                                                             v_sheet_id,
		                                                             v_set_id,
		                                                             1,
		                                                             '4000108' -- TPR OUS
		                                                            ,
		                                                             v_status);
		          END IF;
		          --CLEAR OVERALL TPR
		          gm_pkg_oppr_inventory_update.gm_op_sav_clear_tpr(p_request_id,
		                                                           p_user_id,
		                                                           v_inv_lock_id,
		                                                           v_sheet_id,
		                                                           v_set_id,
		                                                           1,
		                                                           '4000106' -- TPR 
		                                                          ,
		                                                           v_status);
		        END IF;
	        
	     END IF;
      
        DBMS_OUTPUT.PUT_LINE('Inside Common v_status' || v_status ||
                             'p_dsmonthid' || p_dsmonthid);
        IF v_status != 10
        THEN
          --The request is moved to common pool from the sheet.
          --So, the request and sheet mapping deleting here. 
          DELETE t4044_demand_sheet_request
           WHERE c4040_demand_sheet_id = v_sheet_id
                 AND c520_request_id = p_request_id;
        END IF;
      
      END IF;
      v_log_action := 4000309;
      
      --Calling this only for Common Pool scenario, as v_action_type is modified.
      IF (v_process_clear = 'Y')
      THEN
      
	      IF (v_newreq_dt < v_invlockdt)
	      THEN
			      gm_pkg_oppr_inventory_update.gm_op_sav_tpr_clear_pbo (p_request_id
			                                  , p_user_id
			                                  , v_inv_lock_id
			                                  , v_sheet_id
			                                  , v_set_id
			                                  , 1
			                                   );
			        gm_pkg_oppr_inventory_update.gm_op_sav_tpr_clear_pbl (p_request_id
			                                  , p_user_id
			                                  , v_inv_lock_id
			                                  , v_sheet_id
			                                  , v_set_id
			                                  , 1
			                                   );
			        gm_pkg_oppr_inventory_update.gm_op_sav_tpr_clear_pcs (p_request_id
			                                  , p_user_id
			                                  , v_inv_lock_id
			                                  , v_sheet_id
			                                  , v_set_id
			                                  , 1
			                                  );
		END IF;		
     END IF;
    END IF;
  
    IF (v_action_type = 'dateupdate')
    THEN
      gm_pkg_oppr_inventory_update.gm_op_sav_tpr_upt_request(p_request_id,
                                                             p_user_id,
                                                             v_inv_lock_id,
                                                             v_new_req_date);
      v_log_action := 90851;
    
      IF (TO_NUMBER(TO_CHAR(p_old_req_date, 'YYYYMM')) < v_invlockdt AND
         v_newreq_dt >= v_invlockdt)
      THEN
        gm_pkg_oppr_inventory_update.gm_op_sav_tpr_clear_pbo(p_request_id,
                                                             p_user_id,
                                                             v_inv_lock_id,
                                                             v_sheet_id,
                                                             v_set_id,
                                                             1);
        gm_pkg_oppr_inventory_update.gm_op_sav_tpr_clear_pbl(p_request_id,
                                                             p_user_id,
                                                             v_inv_lock_id,
                                                             v_sheet_id,
                                                             v_set_id,
                                                             1);
        gm_pkg_oppr_inventory_update.gm_op_sav_tpr_clear_pcs(p_request_id,
                                                             p_user_id,
                                                             v_inv_lock_id,
                                                             v_sheet_id,
                                                             v_set_id,
                                                             1);
      END IF;
    
      IF (v_newreq_dt < v_invlockdt)
      THEN
        gm_pkg_oppr_inventory_update.gm_op_sav_tpr_clear_pbo(p_request_id,
                                                             p_user_id,
                                                             v_inv_lock_id,
                                                             v_sheet_id,
                                                             v_set_id,
                                                             -1);
        gm_pkg_oppr_inventory_update.gm_op_sav_tpr_clear_pbl(p_request_id,
                                                             p_user_id,
                                                             v_inv_lock_id,
                                                             v_sheet_id,
                                                             v_set_id,
                                                             -1);
        gm_pkg_oppr_inventory_update.gm_op_sav_tpr_clear_pcs(p_request_id,
                                                             p_user_id,
                                                             v_inv_lock_id,
                                                             v_sheet_id,
                                                             v_set_id,
                                                             -1);
      END IF;
    END IF;
  
    --    gm_pkg_oppr_inventory_update.gm_op_inv_log_update (p_request_id, v_inv_lock_id, 90870, v_log_action, p_user_id);
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN;
  END gm_op_sav_tpr_clear_request;

  /*******************************************************
  * Description : Procedure to update Request
  author: Xun
  *******************************************************/
  PROCEDURE gm_op_sav_tpr_upt_request
  (
    p_req_id      IN t907_cancel_log.c907_ref_id%TYPE,
    p_userid      IN t101_user.c101_user_id%TYPE,
    p_inv_lock_id IN t4040_demand_sheet.c250_inventory_lock_id%TYPE,
    p_req_date    IN t520_request.c520_required_date%TYPE
  ) AS
  BEGIN
    UPDATE t253c_request_lock
       SET c520_required_date     = p_req_date,
           c520_last_updated_by   = p_userid,
           c520_last_updated_date = SYSDATE
     WHERE c520_request_id = p_req_id
           AND c250_inventory_lock_id = p_inv_lock_id;
  
    --Below SQL to update child record
    UPDATE t253c_request_lock
       SET c520_required_date     = p_req_date,
           c520_last_updated_by   = p_userid,
           c520_last_updated_date = SYSDATE
     WHERE c520_master_request_id = p_req_id
           AND c250_inventory_lock_id = p_inv_lock_id;
    --
  END gm_op_sav_tpr_upt_request;

  /*******************************************************
  * Description : Procedure to void Request
    author: Xun
  *******************************************************/
  PROCEDURE gm_op_sav_tpr_void_request
  (
    p_req_id      IN t907_cancel_log.c907_ref_id%TYPE,
    p_userid      IN t101_user.c101_user_id%TYPE,
    p_inv_lock_id IN t4040_demand_sheet.c250_inventory_lock_id%TYPE
  ) AS
  BEGIN
    UPDATE t253c_request_lock
       SET c520_void_fl           = 'Y',
           c520_last_updated_by   = p_userid,
           c520_last_updated_date = SYSDATE
     WHERE c520_request_id = p_req_id
           AND c250_inventory_lock_id = p_inv_lock_id;
  
    UPDATE t253a_consignment_lock
       SET c504_void_fl           = 'Y',
           c504_last_updated_by   = p_userid,
           c504_last_updated_date = SYSDATE
     WHERE c520_request_id = p_req_id
           AND c250_inventory_lock_id = p_inv_lock_id;
  
    --Below SQL to update child record
    UPDATE t253c_request_lock
       SET c520_void_fl           = 'Y',
           c520_last_updated_by   = p_userid,
           c520_last_updated_date = SYSDATE
     WHERE c520_master_request_id = p_req_id
           AND c250_inventory_lock_id = p_inv_lock_id;
    --
  END gm_op_sav_tpr_void_request;

  /*******************************************************
  * Description : Procedure for pbo
    author: Xun
  *******************************************************/
  PROCEDURE gm_op_sav_tpr_clear_pbo
  (
    p_req_id      IN t907_cancel_log.c907_ref_id%TYPE,
    p_userid      IN t101_user.c101_user_id%TYPE,
    p_inv_lock_id IN t4040_demand_sheet.c250_inventory_lock_id%TYPE,
    p_sheet_id    IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
    p_set_id      IN t520_request.c207_set_id%TYPE,
    p_value       IN NUMBER
  ) AS
  	
  	v_parts_cnt NUMBER := 0; 
    CURSOR pbo_cur IS
      SELECT c205_part_number_id pum_id, (c521_qty * p_value) qty
        FROM t253d_request_detail_lock t253a
       WHERE (c520_request_id = p_req_id OR
             c520_request_id IN
             (SELECT c520_request_id
                 FROM t253c_request_lock
                WHERE c520_master_request_id = p_req_id
                      AND c250_inventory_lock_id = p_inv_lock_id))
             AND c250_inventory_lock_id = p_inv_lock_id;
  BEGIN
    FOR set_val IN pbo_cur
    LOOP
      gm_pkg_oppr_inventory_update.gm_op_sav_demand_sheet_update(p_sheet_id,
                                                                 p_set_id,
                                                                 p_inv_lock_id,
                                                                 50567,
                                                                 set_val.pum_id,
                                                                 set_val.qty,
                                                                 p_userid);
	v_parts_cnt := v_parts_cnt +1;                                                                 
    END LOOP;
    
    --After all the parts in a set is reduced, we should reduce the set Qty itself.
    IF (v_parts_cnt >0) THEN
    --Only if any of the parts quantiy is reduced, we should reduce the set qty.
	    gm_pkg_oppr_inventory_update.gm_op_sav_dmd_sheet_update_set(p_sheet_id,
	                                                                 p_set_id,
	                                                                 p_inv_lock_id,
	                                                                 50567,                                                                 
	                                                                 1,
	                                                                 p_userid);
	END IF;	                                                                
    --
  END gm_op_sav_tpr_clear_pbo;

  /*******************************************************
  * Description : Procedure for pbl
    author: Xun
  *******************************************************/
  PROCEDURE gm_op_sav_tpr_clear_pbl
  (
    p_req_id      IN t907_cancel_log.c907_ref_id%TYPE,
    p_userid      IN t101_user.c101_user_id%TYPE,
    p_inv_lock_id IN t4040_demand_sheet.c250_inventory_lock_id%TYPE,
    p_sheet_id    IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
    p_set_id      IN t520_request.c207_set_id%TYPE,
    p_value       IN NUMBER
  ) AS
  
  v_parts_cnt NUMBER := 0; 
    CURSOR pbo_cur IS
      SELECT t253b.c205_part_number_id pum_id,
             (t253b.c505_item_qty * p_value) qty
        FROM t253c_request_lock t253c, t253a_consignment_lock t253a,
             t253b_item_consignment_lock t253b
       WHERE t253c.c520_request_id = p_req_id
             AND t253c.c520_request_id = t253a.c520_request_id
             AND t253a.c504_consignment_id = t253b.c504_consignment_id
             AND t253c.c520_status_fl = 15
             AND t253c.c250_inventory_lock_id = p_inv_lock_id
             AND t253a.c250_inventory_lock_id = p_inv_lock_id
             AND t253b.c250_inventory_lock_id = p_inv_lock_id;
  BEGIN
    FOR set_val IN pbo_cur
    LOOP
      gm_pkg_oppr_inventory_update.gm_op_sav_demand_sheet_update(p_sheet_id,
                                                                 p_set_id,
                                                                 p_inv_lock_id,
                                                                 50568,
                                                                 set_val.pum_id,
                                                                 set_val.qty,
                                                                 p_userid);
		v_parts_cnt := v_parts_cnt+1;	                                                                 
    END LOOP;
    
    --After all the parts in a set is reduced, we should reduce the set Qty itself.
     IF (v_parts_cnt >0) THEN
    --Only if any of the parts quantiy is reduced, we should reduce the set qty.
	    gm_pkg_oppr_inventory_update.gm_op_sav_dmd_sheet_update_set(p_sheet_id,
	                                                                 p_set_id,
	                                                                 p_inv_lock_id,
	                                                                 50568,                                                                 
	                                                                 1,
	                                                                 p_userid);
	END IF;	                                                                 
    --
  END gm_op_sav_tpr_clear_pbl;

  /*******************************************************
  * Description : This procedure will reduce the TPR value based on the passed in TPR TYPE.
    author: Rajeshwaran
  *******************************************************/
  PROCEDURE gm_op_sav_clear_tpr
  (
    p_req_id      IN t907_cancel_log.c907_ref_id%TYPE,
    p_userid      IN t101_user.c101_user_id%TYPE,
    p_inv_lock_id IN t4040_demand_sheet.c250_inventory_lock_id%TYPE,
    p_sheet_id    IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
    p_set_id      IN t520_request.c207_set_id%TYPE,
    p_value       IN NUMBER,
    p_tpr_type    IN t901_code_lookup.c901_code_id%TYPE,
    P_status      IN t253c_request_lock.c520_status_fl%TYPE
  ) AS
  	CURSOR tpr_cur IS
      SELECT c205_part_number_id pum_id, (c521_qty * p_value) qty
        FROM t253d_request_detail_lock t253a
       WHERE (c520_request_id = p_req_id OR
             c520_request_id IN
             (SELECT c520_request_id
                 FROM t253c_request_lock
                WHERE c520_master_request_id = p_req_id
                      AND c250_inventory_lock_id = p_inv_lock_id))
             AND 1 = DECODE(P_status, '10', 1, -1)
             AND c250_inventory_lock_id = p_inv_lock_id
      UNION
      SELECT t253b.c205_part_number_id pum_id,
             (t253b.c505_item_qty * p_value) qty
        FROM t253c_request_lock t253c, t253a_consignment_lock t253a,
             t253b_item_consignment_lock t253b
       WHERE t253c.c520_request_id = p_req_id
             AND t253c.c520_request_id = t253a.c520_request_id
             AND t253a.c504_consignment_id = t253b.c504_consignment_id
             AND t253c.c520_status_fl IN (15, 20) -- Take BackLog, Ready To Consign status only.
             AND 1 = DECODE(P_status, '10', -1, 1)
             AND t253c.c520_void_fl IS NULL
             AND t253c.c250_inventory_lock_id = p_inv_lock_id
             AND t253a.c250_inventory_lock_id = p_inv_lock_id
             AND t253b.c250_inventory_lock_id = p_inv_lock_id;
  BEGIN
  
    DBMS_OUTPUT.PUT_LINE('p_req_id' || p_req_id || 'p_inv_lock_id' ||
                         p_inv_lock_id || 'p_sheet_id' || p_sheet_id ||
                         P_status);
    FOR set_val IN tpr_cur
    LOOP
    
      gm_pkg_oppr_inventory_update.gm_op_sav_inv_detail_update(p_inv_lock_id,
                                                               p_tpr_type,
                                                               set_val.pum_id,
                                                               set_val.qty);
     
    END LOOP;
    --
  END gm_op_sav_clear_tpr;
  /*******************************************************
  * Description : Procedure for pcs
    author: Xun
  *******************************************************/
  PROCEDURE gm_op_sav_tpr_clear_pcs
  (
    p_req_id      IN t907_cancel_log.c907_ref_id%TYPE,
    p_userid      IN t101_user.c101_user_id%TYPE,
    p_inv_lock_id IN t4040_demand_sheet.c250_inventory_lock_id%TYPE,
    p_sheet_id    IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
    p_set_id      IN t520_request.c207_set_id%TYPE,
    p_value       IN NUMBER
  ) AS
  	v_parts_cnt NUMBER := 0; 
    CURSOR pbo_cur IS
      SELECT t253b.c205_part_number_id pum_id,
             (t253b.c505_item_qty * p_value) qty
        FROM t253c_request_lock t253c, t253a_consignment_lock t253a,
             t253b_item_consignment_lock t253b
       WHERE t253c.c520_request_id = p_req_id
             AND t253c.c520_request_id = t253a.c520_request_id
             AND t253a.c504_consignment_id = t253b.c504_consignment_id
             AND t253c.c520_status_fl IN (20, 30)
             AND t253c.c250_inventory_lock_id = p_inv_lock_id
             AND t253a.c250_inventory_lock_id = p_inv_lock_id
             AND t253b.c250_inventory_lock_id = p_inv_lock_id;
  BEGIN
    FOR set_val IN pbo_cur
    LOOP
     
      gm_pkg_oppr_inventory_update.gm_op_sav_demand_sheet_update(p_sheet_id,
                                                                 p_set_id,
                                                                 p_inv_lock_id,
                                                                 50569,
                                                                 set_val.pum_id,
                                                                 set_val.qty,
                                                                 p_userid);
	  v_parts_cnt := v_parts_cnt+1;                                                                  
    END LOOP;
    --
    --After all the parts in a set is reduced, we should reduce the set Qty itself.
    dbms_output.put_line(' Before SEt Reduction - PCS;'||p_sheet_id);
    
     IF (v_parts_cnt >0) THEN
    --Only if any of the parts quantiy is reduced, we should reduce the set qty.
	    gm_pkg_oppr_inventory_update.gm_op_sav_dmd_sheet_update_set(p_sheet_id,
	                                                                 p_set_id,
	                                                                 p_inv_lock_id,
	                                                                 50569,                                                                 
	                                                                 1,
	                                                                 p_userid);
	END IF;	                                                                 
  END gm_op_sav_tpr_clear_pcs;

  /*******************************************************
  * Description : Procedure for inventory detail update
    author: Xun
  *******************************************************/
  PROCEDURE gm_op_sav_inv_detail_update
  (
    p_inv_lock_id IN t4040_demand_sheet.c250_inventory_lock_id%TYPE,
    p_type        IN t901_code_lookup.c901_code_id%TYPE,
    p_part_num    IN t253d_request_detail_lock.c205_part_number_id%TYPE,
    p_qty         IN t253d_request_detail_lock.c521_qty%TYPE
  ) AS
  	
    CURSOR sub_parts_cur IS
      SELECT t205a.c205_to_part_number_id sub_part,
             (t205a.c205a_qty * NVL(PRIOR t205a.c205a_qty, 1)) qty
        FROM t205a_part_mapping t205a, t205d_sub_part_to_order t205d
       WHERE t205a.c205_to_part_number_id = t205d.c205_part_number_id
       START WITH t205a.c205_from_part_number_id = p_part_num
      CONNECT BY PRIOR t205a.c205_to_part_number_id =
                  t205a.c205_from_part_number_id
               AND t205a.c205a_void_fl IS NULL;   --- added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
  
  BEGIN
	   --PMT-30031 GOP_To show Inventory details in TTP summary
    IF p_type = 20460 THEN --Open PO 
    UPDATE t251_inventory_lock_details
    SET C251_OPEN_PO           =DECODE( SIGN(C251_OPEN_PO - p_qty),-1,0,C251_OPEN_PO - p_qty)
    WHERE c205_part_number_id  = p_part_num
    AND c250_inventory_lock_id = p_inv_lock_id;
   ELSIF p_type = 20461 THEN --Open DHR 
     UPDATE t251_inventory_lock_details
     SET C251_OPEN_DHR          =DECODE( SIGN(C251_OPEN_DHR - p_qty),-1,0,C251_OPEN_DHR - p_qty)
     WHERE c205_part_number_id  = p_part_num
     AND c250_inventory_lock_id = p_inv_lock_id;
   ELSIF p_type = 20462 THEN --Build Set
      UPDATE t251_inventory_lock_details
      SET C251_BUILD_SET         =DECODE( SIGN(C251_BUILD_SET - p_qty),-1,0,C251_BUILD_SET - p_qty)
      WHERE c205_part_number_id  = p_part_num
      AND c250_inventory_lock_id = p_inv_lock_id;
  ELSIF p_type  = 20463 THEN --PO DHR
    UPDATE t251_inventory_lock_details
    SET C251_PO_DHR            =DECODE( SIGN(C251_PO_DHR - p_qty),-1,0,C251_PO_DHR - p_qty)
    WHERE c205_part_number_id  = p_part_num
    AND c250_inventory_lock_id = p_inv_lock_id;
  ELSIF p_type  = 20464 THEN --In Stock 
    UPDATE t251_inventory_lock_details
    SET C251_IN_STOCK          =DECODE( SIGN(C251_IN_STOCK - p_qty),-1,0,C251_IN_STOCK - p_qty)
    WHERE c205_part_number_id  = p_part_num
    AND c250_inventory_lock_id = p_inv_lock_id;
  ELSIF p_type  = 4000114 THEN --RW Inventory
    UPDATE t251_inventory_lock_details
    SET C251_RW_INVENTORY      =DECODE( SIGN(C251_RW_INVENTORY - p_qty),-1,0,C251_RW_INVENTORY - p_qty)
    WHERE c205_part_number_id  = p_part_num
    AND c250_inventory_lock_id = p_inv_lock_id;
  ELSIF p_type  = 20455 THEN --Quarantine Allocated (Inventory) 
    UPDATE t251_inventory_lock_details
    SET C251_QUAR_AVAIL        =DECODE( SIGN(C251_QUAR_AVAIL - p_qty),-1,0,C251_QUAR_AVAIL - p_qty)
    WHERE c205_part_number_id  = p_part_num
    AND c250_inventory_lock_id = p_inv_lock_id;
  ELSIF p_type  = 20454 THEN --Quarantine Allocated (Scrap) 
    UPDATE t251_inventory_lock_details
    SET C251_QUAR_ALLOC        =DECODE( SIGN(C251_QUAR_ALLOC - p_qty),-1,0,C251_QUAR_ALLOC - p_qty)
    WHERE c205_part_number_id  = p_part_num
    AND c250_inventory_lock_id = p_inv_lock_id;
  ELSIF p_type  = 20477 THEN --Parent Part Qty
    UPDATE t251_inventory_lock_details
    SET C251_PARENT_PART_QTY   =DECODE( SIGN(C251_PARENT_PART_QTY - p_qty),-1,0,C251_PARENT_PART_QTY - p_qty)
    WHERE c205_part_number_id  = p_part_num
    AND c250_inventory_lock_id = p_inv_lock_id;
  ELSIF p_type   = 4000106 THEN --TPR
    UPDATE t251_inventory_lock_details
    SET C251_TPR               =DECODE( SIGN(C251_TPR - p_qty),-1,0,C251_TPR - p_qty)
    WHERE c205_part_number_id  = p_part_num
    AND c250_inventory_lock_id = p_inv_lock_id;
  ELSIF p_type = 4000107 THEN --TPR US
     UPDATE t251_inventory_lock_details
     SET C251_Tpr_Us            =DECODE( SIGN(C251_Tpr_Us - p_qty),-1,0,C251_Tpr_Us - p_qty)
     WHERE c205_part_number_id  = p_part_num
    AND c250_inventory_lock_id = p_inv_lock_id;
  ELSIF p_type  = 4000108 THEN --TPR OUS
    UPDATE t251_inventory_lock_details
    SET C251_TPR_OUS           =DECODE( SIGN(C251_TPR_OUS - p_qty),-1,0,C251_TPR_OUS - p_qty)
    WHERE c205_part_number_id  = p_part_num
    AND c250_inventory_lock_id = p_inv_lock_id;
  END IF;
  
    FOR sub_parts_details_cur IN sub_parts_cur
    LOOP
     --PMT-30031 GOP_To show Inventory details in TTP summary  
    IF p_type = 20460 THEN --Open PO 
      UPDATE t251_inventory_lock_details
      SET C251_OPEN_PO =DECODE( SIGN(C251_OPEN_PO- (p_qty * sub_parts_details_cur.qty)),-1,0, C251_OPEN_PO- (p_qty * sub_parts_details_cur.qty))
      WHERE c205_part_number_id = sub_parts_details_cur.sub_part
      AND c250_inventory_lock_id     = p_inv_lock_id;
  ELSIF p_type = 20461 THEN --Open DHR 
     UPDATE t251_inventory_lock_details
     SET C251_OPEN_DHR              =DECODE( SIGN(C251_OPEN_DHR - (p_qty * sub_parts_details_cur.qty)),-1,0,C251_OPEN_DHR - (p_qty * sub_parts_details_cur.qty))
     WHERE c205_part_number_id = sub_parts_details_cur.sub_part
    AND c250_inventory_lock_id     = p_inv_lock_id;
  ELSIF p_type = 20462 THEN --Build Set
    UPDATE t251_inventory_lock_details
    SET C251_BUILD_SET             =DECODE( SIGN(C251_BUILD_SET - (p_qty * sub_parts_details_cur.qty)),-1,0,C251_BUILD_SET - (p_qty * sub_parts_details_cur.qty))
    WHERE c205_part_number_id = sub_parts_details_cur.sub_part
    AND c250_inventory_lock_id     = p_inv_lock_id;
  ELSIF p_type  = 20463 THEN --PO DHR
    UPDATE t251_inventory_lock_details
    SET C251_PO_DHR                =DECODE( SIGN(C251_PO_DHR - (p_qty * sub_parts_details_cur.qty)),-1,0,C251_PO_DHR - (p_qty * sub_parts_details_cur.qty))
    WHERE c205_part_number_id = sub_parts_details_cur.sub_part
    AND c250_inventory_lock_id     = p_inv_lock_id;
  ELSIF p_type  = 20464 THEN --In Stock 
    UPDATE t251_inventory_lock_details
    SET C251_IN_STOCK              =DECODE( SIGN(C251_IN_STOCK - (p_qty * sub_parts_details_cur.qty)),-1,0,C251_IN_STOCK - (p_qty * sub_parts_details_cur.qty))
    WHERE c205_part_number_id = sub_parts_details_cur.sub_part
    AND c250_inventory_lock_id     = p_inv_lock_id;
  ELSIF p_type = 4000114 THEN --RW Inventory
    UPDATE t251_inventory_lock_details
    SET C251_RW_INVENTORY          =DECODE( SIGN(C251_RW_INVENTORY - (p_qty * sub_parts_details_cur.qty)),-1,0,C251_RW_INVENTORY - (p_qty * sub_parts_details_cur.qty))
    WHERE c205_part_number_id = sub_parts_details_cur.sub_part
    AND c250_inventory_lock_id     = p_inv_lock_id;
  ELSIF p_type = 20455 THEN --Quarantine Allocated (Inventory) 
    UPDATE t251_inventory_lock_details
    SET C251_QUAR_AVAIL            =DECODE( SIGN(C251_QUAR_AVAIL - (p_qty * sub_parts_details_cur.qty)),-1,0,C251_QUAR_AVAIL - (p_qty * sub_parts_details_cur.qty))
    WHERE c205_part_number_id = sub_parts_details_cur.sub_part
    AND c250_inventory_lock_id     = p_inv_lock_id;
  ELSIF p_type  = 20454 THEN --Quarantine Allocated (Scrap) 
    UPDATE t251_inventory_lock_details
    SET C251_QUAR_ALLOC            =DECODE( SIGN(C251_QUAR_ALLOC - (p_qty * sub_parts_details_cur.qty)),-1,0,C251_QUAR_ALLOC - (p_qty * sub_parts_details_cur.qty))
    WHERE c205_part_number_id = sub_parts_details_cur.sub_part
    AND c250_inventory_lock_id     = p_inv_lock_id;
  ELSIF p_type = 20477 THEN --Parent Part Qty
    UPDATE t251_inventory_lock_details
    SET C251_PARENT_PART_QTY       =DECODE( SIGN(C251_PARENT_PART_QTY - (p_qty * sub_parts_details_cur.qty)),-1,0,C251_PARENT_PART_QTY - (p_qty * sub_parts_details_cur.qty))
    WHERE c205_part_number_id = sub_parts_details_cur.sub_part
   AND c250_inventory_lock_id     = p_inv_lock_id;
  ELSIF p_type = 4000106 THEN --TPR
    UPDATE t251_inventory_lock_details
    SET C251_TPR                   =DECODE( SIGN(C251_TPR - (p_qty * sub_parts_details_cur.qty)),-1,0,C251_TPR - (p_qty * sub_parts_details_cur.qty))
    WHERE c205_part_number_id = sub_parts_details_cur.sub_part
    AND c250_inventory_lock_id     = p_inv_lock_id;
  ELSIF p_type = 4000107 THEN --TPR US
    UPDATE t251_inventory_lock_details
    SET C251_Tpr_Us                =DECODE( SIGN(C251_Tpr_Us - (p_qty * sub_parts_details_cur.qty)),-1,0,C251_Tpr_Us - (p_qty * sub_parts_details_cur.qty))
    WHERE c205_part_number_id = sub_parts_details_cur.sub_part
    AND c250_inventory_lock_id     = p_inv_lock_id;
  ELSIF p_type = 4000108 THEN --TPR OUS
    UPDATE t251_inventory_lock_details
    SET C251_TPR_OUS               =DECODE( SIGN(C251_TPR_OUS - (p_qty * sub_parts_details_cur.qty)),-1,0,C251_TPR_OUS - (p_qty * sub_parts_details_cur.qty))
    WHERE c205_part_number_id = sub_parts_details_cur.sub_part
    AND c250_inventory_lock_id     = p_inv_lock_id;
    END IF;
    END LOOP;
  END gm_op_sav_inv_detail_update;

  /*******************************************************
  * Description : Procedure for demand sheet update
    author: Xun
  *******************************************************/
  PROCEDURE gm_op_sav_demand_sheet_update
  (
    p_sheet_id    t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
    p_set_id      IN t520_request.c207_set_id%TYPE,
    p_inv_lock_id IN t4040_demand_sheet.c250_inventory_lock_id%TYPE,
    p_type        IN t901_code_lookup.c901_code_id%TYPE,
    p_part_num    IN t253d_request_detail_lock.c205_part_number_id%TYPE,
    p_qty         IN t253d_request_detail_lock.c521_qty%TYPE,
    p_userid      IN t101_user.c101_user_id%TYPE DEFAULT NULL
  ) AS
  BEGIN
    UPDATE t4042_demand_sheet_detail t4042
       SET t4042.c4042_qty = DECODE( SIGN(t4042.c4042_qty - p_qty),-1,0,t4042.c4042_qty - p_qty)
       	, C4042_UPDATED_DATE = SYSDATE
       	, C4042_UPDATED_BY = p_userid
     WHERE t4042.c4040_demand_sheet_id in (
			SELECT c4040_demand_sheet_id 
			FROM   t4046_demand_sheet_assoc_map 
			WHERE  C4040_DEMAND_SHEET_ASSOC_ID=p_sheet_id
			UNION 
			SELECT c4040_demand_sheet_assoc_id 
			FROM   t4046_demand_sheet_assoc_map 
			WHERE  C4040_DEMAND_SHEET_ASSOC_ID=p_sheet_id
			UNION 
			SELECT C4040_DEMAND_SHEET_ID 
			FROM   t4046_demand_sheet_assoc_map 
			WHERE  C4040_DEMAND_SHEET_ID =p_sheet_id
		)
	   AND t4042.c205_part_number_id = p_part_num
	   AND t4042.c901_type = p_type
	   AND
	   t4042.c4042_ref_id = DECODE(p_set_id, NULL, '-9999', p_set_id);
  END gm_op_sav_demand_sheet_update;
  
    /*******************************************************
  * Description : Procedure to adjust the quantity for the set
    author: Rajeshwaran
  *******************************************************/
  PROCEDURE gm_op_sav_dmd_sheet_update_set
  (
    p_sheet_id    t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
    p_set_id      IN t520_request.c207_set_id%TYPE,
    p_inv_lock_id IN t4040_demand_sheet.c250_inventory_lock_id%TYPE,
    p_type        IN t901_code_lookup.c901_code_id%TYPE,
    p_qty         IN t253d_request_detail_lock.c521_qty%TYPE,
    p_userid      IN t101_user.c101_user_id%TYPE DEFAULT NULL
  ) AS
  BEGIN
    UPDATE t4042_demand_sheet_detail t4042
       SET t4042.c4042_qty = DECODE( SIGN(t4042.c4042_qty - p_qty),-1,0,t4042.c4042_qty - p_qty)
         , C4042_UPDATED_DATE = SYSDATE
         , C4042_UPDATED_BY = p_userid
     WHERE t4042.c4040_demand_sheet_id in (
			SELECT c4040_demand_sheet_id 
			FROM   t4046_demand_sheet_assoc_map 
			WHERE  C4040_DEMAND_SHEET_ASSOC_ID = p_sheet_id
			UNION 
			SELECT c4040_demand_sheet_assoc_id 
			FROM   t4046_demand_sheet_assoc_map 
			WHERE  C4040_DEMAND_SHEET_ASSOC_ID = p_sheet_id
			UNION 
			SELECT C4040_DEMAND_SHEET_ID 
			FROM   t4046_demand_sheet_assoc_map 
			WHERE  C4040_DEMAND_SHEET_ID =p_sheet_id
		)
	   AND t4042.c205_part_number_id IS NULL
	   AND t4042.c901_type = p_type
	   AND t4042.c901_ref_type ='102663' -- Set Part Net Total
	   AND t4042.c4042_ref_id = p_set_id;
  END gm_op_sav_dmd_sheet_update_set;

  /*******************************************************
  * Description : Procedure for Inventory TXN LOG update
    author: Xun
  *******************************************************/
  /*  PROCEDURE gm_op_inv_log_update (
    p_req_id    IN   t254_inventory_txn_log.c254_txn_id%TYPE
    , p_inv_lock_id IN   t4040_demand_sheet.c250_inventory_lock_id%TYPE
    , p_type      IN   t901_code_lookup.c901_code_id%TYPE
    , p_action    IN   t254_inventory_txn_log.c901_action%TYPE
    , p_userid    IN   t101_user.c101_user_id%TYPE
  )
  AS
  BEGIN
    INSERT INTO t254_inventory_txn_log
          (c254_inventory_txn_log_id, c250_inventory_lock_id, c254_txn_id, c901_txn_type, c901_action
           , c254_last_updated_by, c254_last_updated_date
          )
       VALUES (s254_inventory_txn_log.NEXTVAL, p_inv_lock_id, p_req_id, p_type, p_action
           , p_userid, SYSDATE
          );
  END gm_op_inv_log_update; */
  --

  PROCEDURE gm_op_exec_dsload
  (
    p_dsmonthid IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
    p_user_id   IN t101_user.c101_user_id%TYPE
  ) AS
    v_status_fl        t4040_demand_sheet.c901_status%TYPE;
    v_master_demand_id t4040_demand_sheet.c4020_demand_master_id%TYPE;
  BEGIN
    SELECT c901_status, c4020_demand_master_id
      INTO v_status_fl, v_master_demand_id
      FROM t4040_demand_sheet
     WHERE c4040_demand_sheet_id = p_dsmonthid;
  
    -- If Sheet is not Open (50550) then throw error
    IF v_status_fl <> '50550'
    THEN
      -- Error message is Sorry. Only OPEN sheets can be reloaded
      raise_application_error(-20779, '');
    END IF;
  
    gm_pkg_oppr_ld_demand.gm_op_sav_load_detail(v_master_demand_id,
                                                91951,
                                                p_user_id);
  END gm_op_exec_dsload;
END gm_pkg_oppr_inventory_update;
/
