CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ld_ttp_vendor_po_dashboard_rpt
IS

	/*************************************************************************
    * Description : Procedure used to fetch and return PO vendor details from T4058_TTP_VENDOR_CAPACITY_SUMMARY.
    *   			Vendor Id, PO Load Date, PO Stauts Id are passed as paramerter.
    * Author      : T.S.Ramachandiran
  	*************************************************************************/
  	PROCEDURE gm_fch_vendor_po_dashboard_dtls (
		p_vendor_id				IN		T4058_TTP_VENDOR_CAPACITY_SUMMARY.C301_VENDOR_ID%TYPE,
		p_load_date				IN		VARCHAR2,
		p_status_id				IN		T4058_TTP_VENDOR_CAPACITY_SUMMARY.C901_STATUS%TYPE,
		p_out_ven_po_dtls 		OUT 	CLOB
	)
	AS
	  	v_load_date DATE;
	  	v_date_fmt VARCHAR2(12);
	BEGIN

  	--To remove to_char functionality get date format using dual
  		SELECT to_date(p_load_date,'MM/yyyy'), get_compdtfmt_frm_cntx ()
     		INTO v_load_date, v_date_fmt
    	FROM DUAL;
		
		SELECT JSON_ARRAYAGG(
				JSON_OBJECT (
					'vendorid'		VALUE 	T4058.C301_VENDOR_ID,
					'vendornm'		VALUE 	T4058.C301_VENDOR_NAME,
					'statusid'		VALUE 	T4058.C901_STATUS,
					'statusnm'		VALUE 	DECODE(t901.c901_code_id ,108862,'Ready for PO Generation',26241143,'Ready for PO Re-Generation',T901.C901_CODE_NM),
					'potype'		VALUE 	NVL(T4058.C901_PO_TYPE,'3100'),
					'hpotype'		VALUE 	T4058.C901_PO_TYPE,
					'pocreatedby'	VALUE	NVL(T101.C101_USER_F_NAME || ' ' || T101.C101_USER_L_NAME,' '),
					'pocreateddt'	VALUE 	NVL(TO_CHAR(T4058.C4058_PO_CREATED_DATE, v_date_fmt||' HH:MI:SS AM'),' '),
					'raisepoqty'	VALUE 	T4058.C4058_RAISE_PO_QTY,
					'pomanualfl'	VALUE  	NVL(GET_RULE_VALUE (T4058.C301_Vendor_Id, 'GEN_VEN_PO_MANUAL'), 'N'),
					'povencapaid'	VALUE	T4058.C4058_TTP_VENDOR_CAPA_SUMMARY_ID,
					'raisepoprice'	VALUE 	T4058.C4058_ORDER_COST)ORDER BY UPPER(T4058.C301_VENDOR_NAME) RETURNING CLOB)			
			INTO p_out_ven_po_dtls
			FROM T4058_TTP_VENDOR_CAPACITY_SUMMARY T4058,T101_User T101,T901_Code_Lookup T901
			WHERE T4058.C4058_PO_CREATED_BY = T101.C101_User_Id (+)
				AND T4058.C901_STATUS = T901.C901_Code_Id
				AND T901.C901_CODE_GRP = 'TTPVST'
				AND	T4058.C901_STATUS= DECODE(p_status_id,0,T4058.C901_STATUS,p_status_id)
				AND	T4058.C301_VENDOR_ID= DECODE(p_vendor_id,0,T4058.C301_Vendor_Id,p_vendor_id)
				AND T4058.C4058_LOAD_DATE = v_load_date
				AND T4058.C4058_VOID_FL is NULL;
				
	END gm_fch_vendor_po_dashboard_dtls;
	
   /****************************************************************************************************
    * Description 	: 	Procedure used to fetch  PO Generation Failed status hyperlink report From
    * 					GOP--Transactions--TTP By Vendor--Create PO By Vendor
    * Author   		: 	Prabhu vigneshwaran M D
  	*****************************************************************************************************/	
   PROCEDURE gm_fch_vendor_po_dashboard_failure (
        p_capa_dtls_id          IN    t4058_ttp_vendor_capacity_summary.c4058_ttp_vendor_capa_summary_id%TYPE,
        p_out_po_failure_dtls   OUT   CLOB
    ) AS
    BEGIN
        SELECT JSON_OBJECT(
            'vendorname'   VALUE t4058.c301_vendor_name, 
            'loaddate'     VALUE to_char(t4058.c4058_load_date, 'mm/yyyy'), 
            'errordetails' VALUE t4058.c4058_error_details) 
        INTO p_out_po_failure_dtls
        FROM
            t4058_ttp_vendor_capacity_summary t4058
        WHERE
            t4058.c4058_ttp_vendor_capa_summary_id = p_capa_dtls_id
            AND t4058.c4058_void_fl IS NULL;

    END gm_fch_vendor_po_dashboard_failure;

/*************************************************************************
* Description : Procedure used to fetch  PO Generation Success Details Report
* Screen location :  GOP--Transactions--TTP By Vendor--Create PO By Vendor-->PO Generated Hyper link
* Author      : Tamizhthangam Ramasamy
**************************************************************************/
PROCEDURE gm_fch_vendor_po_dashboard_success(
    p_capa_dtls_id    IN t4058_ttp_vendor_capacity_summary.c4058_ttp_vendor_capa_summary_id%TYPE,
    p_out_ven_po_dtls OUT CLOB 
  )
AS
  v_out_po_details CLOB;
  v_out_header_dtls VARCHAR2(2000);
  v_date_fmt VARCHAR2 (12);
BEGIN
  
  SELECT get_compdtfmt_frm_cntx () INTO v_date_fmt FROM DUAL;
  --Fetch the header data and assign value to header variable
  SELECT JSON_OBJECT ( 
        'vendorname' value t4058.c301_vendor_name,
        'loadperiod' value  NVL(TO_CHAR(t4058.c4058_load_date, 'mm/yyyy'),''),
        'pocreatedby' value get_user_name(t4058.c4058_po_created_by),
        'pocreateddate' value NVL(TO_CHAR(t4058.c4058_po_created_date, v_date_fmt ||' HH:MI:SS AM'),'')
     ) INTO v_out_header_dtls
    FROM t4058_ttp_vendor_capacity_summary t4058
   WHERE t4058.c4058_ttp_vendor_capa_summary_id = p_capa_dtls_id
     AND t4058.c4058_void_fl                  is null;
  
  --to Fetch the po success  details from T4065_Ttp_Vendor_Po_Dtls
  SELECT JSON_ARRAYAGG( JSON_OBJECT ( 
        'purchaseordid'value t4065.c401_purchase_ord_id, 
        'totalamt' value t4065.c401_po_total_amount, 
        'pototalqty' value t4065.c4065_po_total_qty, 
        'powocnt' value t4065.c4065_po_wo_cnt 
       )returning CLOB)
    INTO v_out_po_details
    FROM t4065_ttp_vendor_po_dtls t4065
   WHERE t4065.c4058_ttp_vendor_capa_summary_id= p_capa_dtls_id
     AND t4065.c4065_void_fl           IS NULL;
 
  p_out_ven_po_dtls := v_out_header_dtls ||'^' || v_out_po_details;
  
END gm_fch_vendor_po_dashboard_success;

END gm_pkg_oppr_ld_ttp_vendor_po_dashboard_rpt;
/