CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ld_ttp_by_vendor_load
IS
/*******************************************************
 * Purpose: This pkg used for to load GOP monthend Load staging tables which will have only six months data
 *
 * modification history
 * ====================
 * Person		 Date	Comments
 * ---------   ------	------------------------------------------
 * psrinivasan    20191116  Initial Version
 * exec gm_pkg_oppr_ld_ttp_by_vendor_load.gm_ld_last_day_job_main(1000,6);
 *******************************************************/
	PROCEDURE gm_ld_last_day_job_main(
		p_company_id		IN	 NUMBER
	  , p_duration	 		IN	 NUMBER
	  , p_load_date 		IN 	DATE
	) 
	AS
		--
		v_inventory_id     t250_inventory_lock.c250_inventory_lock_id%type;
	    --
	BEGIN
		--
		dbms_output.put_line ('**********************  gm_ld_last_day_job_main start time >> '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
	    /* *********************************************************
		 * Delete the stagge and temp tables for monthend Procedures
		 * Tables as follows
		 * 1)ST408_VENDOR_DHR_BY_PART 2)ST402_VENDOR_WO_BY_PART
		 * 3)ST402_TOP_2_VENDOR_DTLS,4)ST405_VENDOR_ACTIVE_PRICING
		 * 5)ST502_BACK_ORDER_DTLS6)ST520_REQUEST_PRIORITY_DTLS
		 * Temp tables
		 * 1)TEMP_VENDOR_DHR_DTLS 2)TEMP_VENDOR_WO_DTLS 3)TEMP_VENDOR_CAPACITY_SUMMARY_DTLS
		 * 4)TEMP_3MONTH_VENDOR_HISTORICAL_RECEIPT 5)TEMP_SET_PRIORITY_DTLS
		 **********************************************************/
		 dbms_output.put_line ('1.started the delete table '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		 
		 --2 truncate all the stage table
		 
	    gm_pkg_oppr_ld_ttp_by_vendor_stage.gm_delete_stage_table();
	    --
	    dbms_output.put_line ('2.after the delete table '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		 --
	     /* *********************************************************
		 * Call the stage procedure For staging
		 * Stage Table-st408,st402,st405
		 **********************************************************/
	    dbms_output.put_line ('3.started the stage main '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		 --3
		gm_pkg_oppr_ld_ttp_by_vendor_stage.gm_sav_process_stage_main(p_company_id,p_duration,p_load_date);
		commit;
		--
		dbms_output.put_line ('4.after the stage main '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		 --
	    /**********************************************************
		 * Call the stage procedure for forecast staging
		 * Stage Table-st502,st520
		 **********************************************************/
		--
		dbms_output.put_line ('5.started the forecast stage main '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		 --4
		gm_pkg_oppr_ld_ttp_by_vendor_forecast_stage.gm_sav_process_backorder_stage_main(p_company_id,p_duration,p_load_date);
		commit;
		--
		dbms_output.put_line ('6.after the forecast stage main '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		 --
		/**********************************************************
		 * Read the stage table and write into temp tables
		 * Temp Table -temp_vendor_dhr_dtls,temp_vendor_wo_dtls
		 * temp_vendor_capacity_summary_dtls,temp_set_priority_dtls
		 **********************************************************/
		--
		dbms_output.put_line ('7.started the temp data main '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		 --5
		gm_pkg_oppr_ld_ttp_by_vendor_temp_load.gm_sav_temp_data_main(p_load_date);
		commit;
		--
		dbms_output.put_line ('8.after the temp data main '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		 --
		/**********************************************************
		 * Read from temp_vendor_dhr_dtls  and write into temp tables
		 * Temp Table-temp_3month_vendor_historical_receipt
		 **********************************************************/
		
		--
		dbms_output.put_line ('9.started the capa summary temp data  '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		 --6
		gm_pkg_oppr_ld_ttp_by_vendor_temp_load.gm_sav_capa_summary_dtls_tmp(p_load_date,3,p_company_id);
		commit;
		--
		dbms_output.put_line ('10.after the capa summary temp data  '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		 --
		/**********************************************************
		 * Read from  temp tables and store into main tables
		 * Main Table-t4061_ttp_capa_forecast_vendor_dtls
		 **********************************************************/
		--
		dbms_output.put_line ('11.started the forecast vendor main  '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		 --7
		gm_pkg_oppr_ld_ttp_capa_master_txn.gm_sav_master_data_to_forecast_vendor(p_load_date);		
		commit;
		--
		dbms_output.put_line ('12.after the forecast vendor main  '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		 --
		--
		dbms_output.put_line ('**********************  gm_ld_last_day_job_main end time >> '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		-- 
	END gm_ld_last_day_job_main;

/*******************************************************
 * Purpose: This pkg used for load GOP First day Load staging tables
 *
 *******************************************************/
	PROCEDURE gm_ld_first_day_job_main(
		p_company_id		IN	 NUMBER
	  , p_duration	 		IN	 NUMBER
	  , p_load_date	        IN   DATE
	)
	AS
		--
		v_inventory_id     t250_inventory_lock.c250_inventory_lock_id%type;
		v_user_id NUMBER := 30301; -- Manual load
	--
	BEGIN
		--
		dbms_output.put_line ('**********************  gm_ld_first_day_job_main start time >> '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		--
	     /* *********************************************************
		 * Delete the stagge and temp tables for first Day Procedures
		 * Tables as follows
		 * 1)ST251_INVENTORY_LOCK_DETAILS 2)ST4042_SALES_REPL_DETAIL
		 * 3)ST4055_TTP_SUMMARY
		 * Temp tables-1)TEMP_SALES_REPL_LOAD
		 **********************************************************/	
		--
		dbms_output.put_line ('1.started the delete forecast stage table  '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		--1
		gm_pkg_oppr_ld_ttp_by_vendor_forecast_stage.gm_delete_forecast_stage_table();
		--
		dbms_output.put_line ('2.after the delete forecast stage table  '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		--
		--To get inventory id from t250_inventory_lock table
		gm_fch_inventory_id(p_load_date,v_inventory_id);
		commit;
		
		--
		dbms_output.put_line ('3.started the invendor stage table  '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM') ||' Inv Lock Id ==> '|| v_inventory_id);
		--
		
		--2 Read from t250 and t251 and store into st251
		gm_pkg_oppr_ld_ttp_by_vendor_forecast_stage.gm_sav_inventory_qty_stage_table(p_load_date,v_inventory_id);
		commit;
		--
		dbms_output.put_line ('4.after the invendor stage table  '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		--
		/*************************************************************
		 * This is the main procedure for load data into staging table
		 * 1)StageTable-st4055 maintable-t4055
		 * 2)StageTable-st4042 maintble-t4040,t4042
		 * Read from stage table and store into temp table
		 * 3)Temptable-temp-sales_replenish_load stagetable-st4042
		 *************************************************************/
		--
		dbms_output.put_line ('5.started the ttp forecast stage table  '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		--3
		gm_pkg_oppr_ld_ttp_by_vendor_forecast_stage.gm_process_ttp_forecast_stage_main(p_load_date);
		commit;
		--
		dbms_output.put_line ('6.after the ttp forecast stage table  '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		--
		/**********************************************************
		 * Read from  temp tables and store into main tables
		 * Main Table-t4057,t4058,t4059,t4062,t4063,t4064
		 **********************************************************/
		--
		dbms_output.put_line ('7.started the master data monthly load main  '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		--4
		gm_pkg_oppr_ld_ttp_capa_master_txn.gm_sav_master_data_monthly_load_main(p_load_date, v_user_id);
		commit;
		--
		dbms_output.put_line ('8.after the master data monthly load main  '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		--
	    /**********************************************************
		 * Read from  temp tables and Update into main tables
		 * Main Table-t4057,t4058,t4059,t4062,t4063,t4064
		 **********************************************************/
		
		--This procedure is used to update sync fl as null in t4068 override details
		gm_pkg_oppr_ttp_capa_load_override_vendor.gm_upd_vendor_override_master_sync_dtls(null,null,v_user_id);
		
		--This procedure is used to update t5057 vendor summary details based on t4068 values
		gm_pkg_oppr_ttp_capa_load_override_vendor.gm_upd_override_vendor_dtls(p_load_date, v_user_id);
		
		--This procedure is used to update sync fl as Y in t4068 override details
		gm_pkg_oppr_ttp_capa_load_override_vendor.gm_upd_vendor_override_master_sync_dtls(null,'Y',v_user_id);
		
		dbms_output.put_line ('9.started the update master data load '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		--5
		gm_pkg_oppr_ld_ttp_capa_master_txn.gm_upd_master_data_load(p_load_date, v_user_id);
		commit;
		--
		dbms_output.put_line ('10.after the update master data load '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		--
		
		--
		dbms_output.put_line ('**********************  gm_ld_first_day_job_main end time >> '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		--
	END gm_ld_first_day_job_main;
	
	
	
/*********************************************************************
 * Purpose: This procedure used to Get Inventory id from t250 table
 * 
 **********************************************************************/
	PROCEDURE gm_fch_inventory_id(
	p_load_date IN DATE,
	p_inventory_id OUT t250_inventory_lock.c250_inventory_lock_id%type
	)
	
	AS	
	
	BEGIN
		--Get InventoryLock Id from t250 table
			BEGIN
			  SELECT  MAX(t250.c250_inventory_lock_id) 
				INTO  p_inventory_id 
			    FROM  t250_inventory_lock t250   
			    WHERE t250.c250_void_fl IS NULL 
			    AND   t250.c901_lock_type = 20430 --main inventory lock(20430)
			    AND t250.c4016_global_inventory_map_id  = 1
			    AND T250.C250_LOCK_DATE = p_load_date; 
              
			 EXCEPTION WHEN NO_DATA_FOUND THEN
			 p_inventory_id := NULL;
			 END;	
			 
			 DBMS_OUTPUT.put_line ('Get Inventory Id' || p_inventory_id ||'-'|| TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
	END gm_fch_inventory_id;

	/*******************************************************
	* Purpose: This procedure used to call the last day job and getting
	*    the load date from month end procedure
	*******************************************************/
	PROCEDURE gm_ld_last_day_job_main (
	        p_company_id IN NUMBER,
	        p_duration   IN NUMBER)
	AS
	    v_load_date DATE;
	BEGIN
	    --1. to get the load date from month end procedrue.
	    /* *********************************************************
		 * Load date will be calculated from GOP monthend procedure,
		 * so that it can be executed last day of the month
		 **********************************************************/
	    gm_pkg_oppr_ld_mend_inventory.gm_op_fch_load_date (v_load_date) ;
	    
	    -- 2. to call the last day job
	    gm_pkg_oppr_ld_ttp_by_vendor_load.gm_ld_last_day_job_main (p_company_id, p_duration, v_load_date) ;
	    --
	END gm_ld_last_day_job_main; 

	/*******************************************************
	* Purpose: This procedure used to call the First day Load job
	* 1. load stage
	* 2. load main table
	* 3. update the order/required qty
	* 4. calculate forecast qty
	* 5. form the JSON data - chart
	*******************************************************/
	PROCEDURE gm_ld_first_day_job_main (
	        p_company_id IN NUMBER,
	        p_duration   IN NUMBER)
	AS
	    v_load_date DATE;
	BEGIN
		
	    --1 to get the load date using month end procedure
	    /* *********************************************************
	    * Load date will be calculated from GOP monthend procedure,
	    * so that it can be executed first day of the month
	    **********************************************************/
		
	    gm_pkg_oppr_ld_mend_inventory.gm_op_fch_load_date (v_load_date) ;
	    
	    -- 2 to call the first day job
	    gm_pkg_oppr_ld_ttp_by_vendor_load.gm_ld_first_day_job_main (p_company_id, p_duration, v_load_date) ;
	    
	END gm_ld_first_day_job_main;
	
END gm_pkg_oppr_ld_ttp_by_vendor_load;
/