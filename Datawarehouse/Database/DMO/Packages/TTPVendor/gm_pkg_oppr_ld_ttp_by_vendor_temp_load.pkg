CREATE OR REPLACE
PACKAGE gm_pkg_oppr_ld_ttp_by_vendor_temp_load
IS
  /*******************************************************
  * Purpose: This pkg used for load GOP monthend Load staging tables
  *
  * modification history
  * ====================
  * Person   Date Comments
  * ---------   ------ ------------------------------------------
  * psrinivasan    20191116  Initial Version
  *******************************************************/
PROCEDURE gm_sav_temp_data_main (p_load_date IN DATE);


  /*******************************************************
  * Purpose: This procedure will read from dhr stage to dhr temp
  *******************************************************/
PROCEDURE gm_sav_dhr_temp_data (p_load_date IN DATE);


  /*******************************************************
  * Purpose: This procedure will read from wo stage to wo temp
  *******************************************************/
PROCEDURE gm_sav_wo_temp_data (p_load_date IN DATE);


  /********************************************************************************************************************
  * Purpose: This procedure will read from TEMP_DHR, TEMP_WO and VENDOR STAGE TOP2 to TEMP_VENDOR_CAPACITY_SUMMARY_DTLS
  *********************************************************************************************************************/
PROCEDURE gm_sav_vendor_summary_temp_data;

  /*******************************************************************************
  * Purpose: This procedure will read from set priority stage to set priority temp
  ********************************************************************************/
PROCEDURE gm_sav_temp_set_priority_by_month (p_load_date IN DATE);

  /***************************************************************************
  * This procedure is used to save temp_3month_vendor_historical_receipt data
  ****************************************************************************/
PROCEDURE gm_sav_capa_summary_dtls_tmp(
    p_load_date        IN DATE ,
    p_dhr_avg_duration IN NUMBER ,
    p_company_id       IN NUMBER );
    
  /****************************************************************************
  * This procedure is used to save temp_3month_vendor_historical_receipt data
  *****************************************************************************/
PROCEDURE gm_sav_temp_vendor_receipt_by_mon(
    p_load_date        IN DATE ,
    p_dhr_avg_duration IN NUMBER ,
    p_company_id       IN NUMBER );
    
  /******************************************************************************
  * This procedure is used to update temp_3month_vendor_historical_receipt data
  *******************************************************************************/
PROCEDURE gm_upd_forecast_receipt_qty(
    p_load_date        IN DATE ,
    p_dhr_avg_duration IN NUMBER ,
    p_company_id       IN NUMBER );
  /***************************************************************************
  * This procedure is used to update temp historical receipt 8 month avg qty
  ***************************************************************************/
PROCEDURE gm_tmp_upd_dhr_avg_qty(
    p_dhr_avg_duration                    IN NUMBER,
    p_total_working_days_pre_third_month  IN NUMBER,
    p_total_working_days_pre_second_month IN NUMBER,
    p_total_working_days_pre_first_month  IN NUMBER );
  /**********************************************************
  * This procedure used to find no.of working days for month
  ***********************************************************/
PROCEDURE get_no_working_day_by_mon(
    p_load_date  IN DATE,
    p_company_id IN t1904_company_working_day_by_mon.c1900_company_id%type,
    p_total_working_days OUT NUMBER );
  /****************************************
  * To Load sales replish data to temp
  ****************************************/
PROCEDURE gm_sav_temp_sales_repl_data(
    p_ttp_detail_id IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE,
    p_load_date  IN DATE);
    
    /****************************************
  * To Load multipart sales replish data to temp
  ****************************************/
PROCEDURE gm_sav_temp_multipart_sales_repl_data(
    p_ttp_detail_id IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE,
    p_load_date  IN DATE);
    
  /********************************************************************************
  * This procedure is used to update total required qty to temp sales repli table
  *********************************************************************************/
PROCEDURE gm_upd_total_required_data_to_temp(
    p_ttp_detail_id IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE );
  /*******************************************************
  * To update safety stock details on TEMP_SALES_REPL_LOAD
  *********************************************************/
PROCEDURE gm_upd_safety_stock_dtls(
    p_ttp_detail_id IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE );
  /****************************
  * To calculate sefety stock
  ******************************/
PROCEDURE gm_fch_safety_stock_by_part(
    p_replish_qty_01 IN NUMBER,
    p_replish_qty_02 IN NUMBER,
    p_replish_qty_03 IN NUMBER,
    p_net_on_hand    IN NUMBER,
    p_out_value OUT NUMBER );
END gm_pkg_oppr_ld_ttp_by_vendor_temp_load;
/