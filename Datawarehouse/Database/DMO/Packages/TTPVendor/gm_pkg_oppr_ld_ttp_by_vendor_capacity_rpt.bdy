
CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ld_ttp_by_vendor_capacity_rpt
IS
	/*************************************************************************
    * Description : Procedure to used to fetch data from t4059 table which is 
    * 				used for Dashboard screen
    * Author   : 
    *************************************************************************/
PROCEDURE gm_fch_capacity_dashboard_dtls(
            p_ttp_id              IN    t4059_ttp_capacity_summary.c4050_ttp_id%TYPE,
            p_status_id           IN    t4059_ttp_capacity_summary.c901_status%TYPE,
            p_lock_period         IN    varchar2,
            p_out_ttp_ven_dtls    OUT 	clob
           )
           
     AS
     v_load_date DATE;
     v_date_fmt VARCHAR2 (12);
     BEGIN
	     
	  --To remove to_char functionality get date format using dual  
     SELECT to_date(p_lock_period,'MM/yyyy'), get_compdtfmt_frm_cntx ()
     	INTO v_load_date, v_date_fmt
     FROM DUAL;


     		SELECT JSON_ARRAYAGG(
 			    	JSON_OBJECT (
      				'ttpid'        value t4059.c4050_ttp_id,
      				'ttpdetailid'  value t4059.c4052_ttp_detail_id,
       				'ttpname'      value t4059.c4050_ttp_name,
       			    'statusid'	   value t4059.c901_status,
   		 			'statusnm'     value t901_ttp_vendor.c901_code_nm,
   		 			'orderqty'     value t4059.c4059_order_qty,
   		 			'orderamt'     value t4059.c4059_order_cost,
    		 		'lockedby'     value NVL(t101.c101_user_f_name || ' ' || t101.c101_user_l_name,' '),
   		 			'lockeddate'   value NVL(TO_CHAR(t4059.c4059_locked_date, v_date_fmt||' HH:MI:SS AM'),' '),
   		 			'openvendor'   value t4059.c4059_pending_approval_cnt,
   		 			'sheetstatus' value  t4052.c901_status
   		 			)ORDER BY upper(t4059.c4050_ttp_name)  returning CLOB)
      		INTO  p_out_ttp_ven_dtls
  			FROM  t4059_ttp_capacity_summary t4059,t4052_ttp_detail t4052,t4050_ttp t4050,t101_user t101,t901_code_lookup t901_ttp_status, t901_code_lookup t901_ttp_vendor
       	   WHERE t4059.c4050_ttp_id = t4050.c4050_ttp_id
            AND  t4059.c4052_ttp_detail_id = t4052.c4052_ttp_detail_id
            AND  t901_ttp_status.c901_code_id = t4052.c901_status
            AND  t901_ttp_vendor.c901_code_id = t4059.c901_status
            AND  t4059.c4059_locked_by = t101.c101_user_id (+)
            AND  t4059.c901_status  = DECODE(p_status_id,0,t4059.c901_status,p_status_id)
            AND  t4059.c4050_ttp_id = DECODE(p_ttp_id,0,t4059.c4050_ttp_id,p_ttp_id)
            AND  t4059.c4059_load_date = v_load_date
            AND  t4059.c4059_void_fl IS NULL
            AND  t4050.c4050_void_fl IS NULL
            AND  t4052.c4052_void_fl IS NULL;
    
	 END gm_fch_capacity_dashboard_dtls;
--
 /***********************************************************************
 * Description : This Procedure is used to fetch the TTP Summary Details 
 *                based on TTP id or vendor id
 * Author      : Tamizhthangam Ramasamy
 ***********************************************************************/
--
PROCEDURE gm_fch_capacity_summary_dtls(
     p_ttp_id           IN   t4050_ttp.c4050_ttp_id%TYPE,
     p_vendor_id        IN   t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_id%TYPE,
     p_load_Date        IN   VARCHAR2,
     p_out_summary_dtls OUT  CLOB
 
)
AS
 v_load_date DATE;
 v_out_Details CLOB;
 v_out_heder_dtls CLOB;
 v_out_status CLOB;
BEGIN
	 --To remove to_char functionality get date format using dual  
    SELECT to_date(p_load_date,'MM/yyyy') INTO v_load_date FROM DUAL;
    
	SELECT JSON_ARRAYAGG( 
  	   JSON_OBJECT(
  		 'partnum'         value t4057.c205_part_number_id,
  		 'partnumdesc'	   value t4057.c205_part_num_desc,
  		 'ttpname'		   value t4050.c4050_ttp_nm,
  		 'vendorname'      value t4057.c301_vendor_name,
  		 'histrecpmon01'   value t4057.c4057_hist_receipt_month_01,
  		 'histrecpmon02'   value t4057.c4057_hist_receipt_month_02,
  		 'histrecpmon03'   value t4057.c4057_hist_receipt_month_03,
  		 'histrecpmon04'   value t4057.c4057_hist_receipt_month_04,
  		 'histrecpmon05'   value t4057.c4057_hist_receipt_month_05,
  		 'histrecpmon06'   value t4057.c4057_hist_receipt_month_06,
  		 'hispomon01'      value t4057.c4057_hist_po_month_01,
		 'hispomon02'	   value t4057.c4057_hist_po_month_02,
		 'hispomon03'	   value t4057.c4057_hist_po_month_03,
		 'hispomon04'	   value t4057.c4057_hist_po_month_04,
		 'hispomon05'	   value t4057.c4057_hist_po_month_05,
		 'hispomon06'	   value t4057.c4057_hist_po_month_06,
		 'forecastmon01'   value t4057.c4057_forecast_month_01,
		 'forecastmon02'   value t4057.c4057_forecast_month_02,
		 'forecastmon03'   value t4057.c4057_forecast_month_03,
		 'forecastmon04'   value t4057.c4057_forecast_month_04,
		 'forecastmon05'   value t4057.c4057_forecast_month_05,
		 'forecastmon06'   value t4057.c4057_forecast_month_06,
		 'totalordqty'     value t4057.c4057_total_order_qty,
		 'historysplitper' value t4057.c4057_history_split_per,
		 'overridesplitper'value  t4057.c4057_override_split_per,
		 'overridesplitfl' value t4057.c4057_override_split_hist_fl,
		 'othervendorname' value t4057.c301_second_vendor_name,
		 'othervenraisepoqty' value t4057.c4057_second_vendor_raise_po_qty,
		 'historyavgqty'   value t4057.c4057_history_avg_qty,
	     'recommandqty'    value t4057.c4057_recommand_qty,
		 'raisepoqty'     value nvl(t4057.c4057_old_po_qty,0)+t4057.c4057_raise_po_qty ,
		 'raisepoqtyvar'  value t4057.c4057_raise_po_qty_variance,
		 'absraisepoqtyvar' value t4057.c4057_abs_raise_po_qty_variance,
		 'ttpvendorcapadtlsid' value t4057.c4057_ttp_vendor_capa_dtls_id,
		 'ttpchartbypartdata' value t4057.c4057_ttp_vendor_capa_dtls_id || '~' ||t4057.c205_part_number_id,
  		 'ttpdetailid'     value t4052.c4052_ttp_detail_id,
  		 'statusid'  value t4052.c901_status,
  		 'approvedby' value  t4057.c4057_approved_by,
  		 'overridevendorid' value t4057.c301_override_vendor_id,
  		 'overridevendorname' value decode(c301_override_vendor_id,c301_second_vendor_id,null,t4057.c301_override_vendor_name),
  		 'othervendorsplitper' value t4057.c4057_other_override_split_per,
  		 'vendorid' value t4057.c301_vendor_id,
  		 'editrowfl' value t4057.c4057_edit_row_lock_fl
  		 )
  ORDER BY t4057.c4057_abs_raise_po_qty_variance desc,t4057.c205_part_number_id ,UPPER(t4057.c301_vendor_name) RETURNING CLOB)
  INTO v_out_Details
  FROM t4057_ttp_vendor_capacity_summary_dtls t4057,t4052_ttp_detail t4052,t4050_ttp t4050 
  WHERE t4050.c4050_ttp_id = t4052.c4050_ttp_id
    AND t4052.c4052_ttp_detail_id =t4057.c4052_ttp_detail_id
    AND t4057.c4057_load_date =t4052.c4052_ttp_link_date
    AND t4050.c4050_ttp_id = decode(p_ttp_id,null,t4050.c4050_ttp_id,p_ttp_id)
    AND t4057.c301_vendor_id = decode(p_vendor_id,null,t4057.c301_vendor_id,p_vendor_id)
    AND t4057.c4057_load_date =v_load_date
    AND t4057.c4057_void_fl IS NULL
    AND t4050.c4050_void_fl IS NULL
    AND t4052.c4052_void_fl  IS NULL;
    
    -- to get the Header Details based on Load Date 
    SELECT 'Part#,Part Num Desc,TTP Name,Vendor,'||''||To_Char(Add_Months(v_load_date,-6), 'Mon')||',
              '||To_Char(Add_Months(v_load_date,-5), 'Mon')||',
              '||To_Char(Add_Months(v_load_date,-4), 'Mon')||',
              '||To_Char(Add_Months(v_load_date,-3), 'Mon')||',
              '|| To_Char(Add_Months(v_load_date,-2), 'Mon')||',
              '||To_Char(Add_Months(v_load_date,-1), 'Mon')||',
              '||To_Char(v_load_date, 'Mon')||',
              '||To_Char(Add_Months(v_load_date,+1), 'Mon')||',
              '||To_Char(Add_Months(v_load_date,+2), 'Mon')||',
              '||To_Char(Add_Months(v_load_date,+3), 'Mon')||',
              '||To_Char(Add_Months(v_load_date,+4), 'Mon')||',
              '||To_Char(Add_Months(v_load_date,+5), 'Mon')||','||'Total Qty to Order,
               Hist. Split%, Override Split%,, Other Vendor,Other Vendor Raise PO Qty,Override Vendor,Other Vendor Split%, Avg PO Hist. Qty,Recomm. PO Qty,Raise PO Qty,Raise PO Qty Variance,ABS - Raise PO Qty,Chart,,,,,'  
       INTO v_out_heder_dtls FROM dual;
    
     --Get vendor status from t4058 table and concat the status value in p_out_summary_dtls parameter.
     --This status will help us to enable or disable Approve/Rollback buttons
     
     -- PC:1754: to fix the TTP name filter issue (to handled the no data found exception)
       BEGIN
        SELECT  to_char(c901_status) 
	      INTO  v_out_status
       FROM   t4058_ttp_vendor_capacity_summary
      WHERE   c301_vendor_id = p_vendor_id
        AND   c4058_load_date = v_load_date
        AND   c4058_void_fl IS NULL;
       EXCEPTION WHEN OTHERS
       THEN
       		v_out_status := NULL;
       END;
       
	p_out_summary_dtls := v_out_Details || '^'|| v_out_heder_dtls || '^' || v_out_status;

END gm_fch_capacity_summary_dtls;
PROCEDURE gm_fch_chart_by_part_detail(
     p_ttp_capa_id           IN   t4063_ttp_forecast_by_part_dtls.c4057_ttp_by_vendor_summary_id%TYPE,
     p_part_num              IN   t4063_ttp_forecast_by_part_dtls.c205_part_number_id%TYPE,
     p_chart_by              IN   VARCHAR2,
     p_load_Date        	 IN   VARCHAR2,
     p_out_chart_dtls 	     OUT  CLOB
)
AS
	v_load_date DATE;
BEGIN
	--To remove to_char functionality get date format using dual
	SELECT to_date(p_load_date,'MM/yyyy') INTO v_load_date FROM DUAL;

	SELECT JSON_OBJECT('chartbypartjsondata' value decode(p_chart_by,'Receipt',c4063_chart_receipt_json_data,c4063_chart_po_json_data) RETURNING CLOB)
  	  INTO p_out_chart_dtls
	  FROM t4063_ttp_forecast_by_part_dtls
	 WHERE c205_part_number_id = p_part_num
	   AND c4057_ttp_by_vendor_summary_id = p_ttp_capa_id
	   AND c4063_load_date = v_load_date
	   AND c4063_void_fl is null;
	   
END gm_fch_chart_by_part_detail;
	
 /***********************************************************************
 * Description : This Procedure is used to fetch the TTP Vendor chart details
 * 				 based on vendor id
 * Author      : Agilan Singaravel
 ***********************************************************************/
PROCEDURE gm_fch_chart_by_vendor_detail(
     p_vendor_id             IN   t4064_ttp_forecast_by_vendor_dtls.c301_vendor_id%TYPE,
     p_load_date             IN   VARCHAR2,
     p_chart_by              IN   VARCHAR2,
     p_out_chart_dtls 	     OUT  CLOB
)
AS
	v_load_date DATE;
BEGIN
	--To remove to_char functionality get date format using dual
	SELECT to_date(p_load_date,'MM/yyyy') INTO v_load_date FROM DUAL;
	
	SELECT JSON_OBJECT('chartbyvendorjsondata' value decode(p_chart_by,'Receipt',c4064_chart_receipt_json_data,c4064_chart_po_json_data) RETURNING CLOB)
  	  INTO p_out_chart_dtls
	  FROM t4064_ttp_forecast_by_vendor_dtls
	 WHERE c301_vendor_id = p_vendor_id
	   AND c4064_load_date = v_load_date
	   AND c4064_void_fl is null;
	
END gm_fch_chart_by_vendor_detail;

/***********************************************************************
* Description : This Procedure is used to fetch the TTP Footer chart details
*      based on filters
* Author      : Tamizhthangam Ramasamy
***********************************************************************/
PROCEDURE gm_fch_chart_by_filter_dtls(
    p_input_str              IN CLOB,
    p_chart_by               IN VARCHAR2,
    p_out_footer_chart_dtls  OUT CLOB )
AS
BEGIN
	 my_context.set_my_inlist_ctx (p_input_str);
  IF p_chart_by ='Receipt' THEN
    gm_fch_receipt_chart_filter_dtls(p_out_footer_chart_dtls);
  ELSE
    gm_fch_po_chart_filter_dtls(p_out_footer_chart_dtls);
  END IF;
END gm_fch_chart_by_filter_dtls;
/*************************************************************************
* Description : Procedure to used to fetch JSON data based on filtered value by Receipt
* Author      : Tamizhthangam Ramasamy
*************************************************************************/
PROCEDURE gm_fch_receipt_chart_filter_dtls(
    p_out_footer_chart_dtls OUT CLOB )
AS
BEGIN
 
  
  SELECT LISTAGG(JSON_DATA,'') WITHIN GROUP (
  ORDER BY JSON_DATA)
  INTO p_out_footer_chart_dtls
  FROM
    (
    -- BOs
    SELECT '[{seriesname:"BOs",valueBgColor:"#008080",data:['
      ||REPLACE(JSON_OBJECT( 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE TO_CHAR(SUM(c4063_cal_back_order_qty)), 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0'),',','},{')
      ||']},'
      ||
      --Sales Replenishment
      '{seriesname:"Sales Replenishment",valueBgColor:"#0000FF",data:['
      ||REPLACE(JSON_OBJECT( 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE SUM(c4063_sales_replish_01), 'value' VALUE SUM(c4063_sales_replish_02), 'value' VALUE SUM(c4063_sales_replish_03), 'value' VALUE SUM(c4063_sales_replish_04), 'value' VALUE SUM(C4063_Sales_Replish_05), 'value' VALUE SUM(c4063_sales_replish_06)),',','},{')
      ||']},'
      ||
      --Safety Stock
      '{seriesname:"Safety Stock",valueBgColor:"#8A2BE2",data:['
      ||REPLACE(JSON_OBJECT( 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE SUM(c4063_cal_safety_stock_01), 'value' VALUE SUM(c4063_cal_safety_stock_02), 'value' VALUE SUM(c4063_cal_safety_stock_03), 'value' VALUE SUM(c4063_cal_safety_stock_04), 'value' VALUE SUM(c4063_cal_safety_stock_05), 'value' VALUE SUM(c4063_cal_safety_stock_06)),',','},{')
      ||']},'
      ||
      --Priority Set Previous Month
      '{seriesname:"Priority Set Previous Month",valueBgColor:"#FFFF00",data:['
      ||REPLACE(JSON_OBJECT( 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE SUM(c4063_cal_set_priority_pre_mon_qty), 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0'),',','},{')
      ||']},'
      ||
      --Priority Set Current Month
      '{seriesname:"Priority Set Current Month",valueBgColor:"#008000",data:['
      ||REPLACE(JSON_OBJECT( 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE SUM(c4063_cal_set_priority_01), 'value' VALUE SUM(c4063_cal_set_priority_02), 'value' VALUE SUM(c4063_cal_set_priority_03), 'value' VALUE SUM(c4063_cal_set_priority_04), 'value' VALUE SUM(c4063_cal_set_priority_05), 'value' VALUE SUM(c4063_cal_set_priority_06)),',','},{')
      ||']},'
      ||
      --Split Other Needs
      '{seriesname:"Split Other Needs",valueBgColor:"#00BFFF",data:['
      ||REPLACE(JSON_OBJECT( 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE SUM(c4063_cal_chart_other_need_01), 'value' VALUE SUM(c4063_cal_other_need_02), 'value' VALUE SUM(c4063_cal_other_need_03), 'value' VALUE SUM(c4063_cal_other_need_04), 'value' VALUE SUM(c4063_cal_other_need_05), 'value' VALUE SUM(c4063_cal_other_need_06)),',','},{')
      ||']},'
      ||
      --FCST Historical Receipt Avg
      '{seriesname:"FCST Historical Receipt Avg",renderas:"Line",valueBgColor:"#FF0000",data:['
      ||REPLACE(JSON_OBJECT( 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE SUM(c4063_pre_hist_rec_avg_03), 'value' VALUE SUM(c4063_pre_hist_rec_avg_02), 'value' VALUE SUM(c4063_pre_hist_rec_avg_01), 'value' VALUE SUM(c4063_fcst_hist_rec_avg_01), 'value' VALUE SUM(c4063_fcst_hist_rec_avg_02), 'value' VALUE SUM(c4063_fcst_hist_rec_avg_03), 'value' VALUE SUM(c4063_fcst_hist_rec_avg_04), 'value' VALUE SUM(c4063_fcst_hist_rec_avg_05), 'value' VALUE SUM(c4063_fcst_hist_rec_avg_06)),',','},{')
      ||']},'
      ||
      --Actual Receipt (Historical)
      '{seriesname:" Actual Receipt (Historical)",valueBgColor:"#7CFC00",data:['
      ||REPLACE(JSON_OBJECT( 'value' VALUE SUM(c4063_cal_hist_receipt_month_01), 'value' VALUE SUM(c4063_cal_hist_receipt_month_02) , 'value' VALUE SUM(c4063_cal_hist_receipt_month_03), 'value' VALUE SUM(c4063_cal_hist_receipt_month_04), 'value' VALUE SUM(c4063_cal_hist_receipt_month_05) , 'value' VALUE SUM(c4063_cal_hist_receipt_month_06), 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0' , 'value' VALUE '0', 'value' VALUE '0'),',','},{')
      ||']}]' JSON_DATA
    FROM t4063_ttp_forecast_by_part_dtls t4063,v_in_list v_list
    WHERE t4063.c4057_ttp_by_vendor_summary_id =v_list.token
      AND t4063.c4063_void_fl  IS NULL
    );
END gm_fch_receipt_chart_filter_dtls;
/*************************************************************************
* Description : Procedure to used to fetch JSON data based on filtered value by PO
* Author      : Tamizhthangam Ramasamy
*************************************************************************/
PROCEDURE gm_fch_po_chart_filter_dtls(
    p_out_footer_chart_dtls OUT CLOB )
AS
BEGIN
  
  SELECT LISTAGG(JSON_DATA,'') WITHIN GROUP (
  ORDER BY JSON_DATA)
  INTO p_out_footer_chart_dtls
  FROM
    (
    -- BOs
    SELECT '[{seriesname:"BOs",valueBgColor:"#008080",data:['
      ||REPLACE(JSON_OBJECT( 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE TO_CHAR(SUM(c4063_cal_back_order_qty)), 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0'),',','},{')
      ||']},'
      ||
      --Sales Replenishment
      '{seriesname:"Sales Replenishment",valueBgColor:"#0000FF",data:['
      ||REPLACE(JSON_OBJECT( 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE SUM(c4063_sales_replish_01), 'value' VALUE SUM(c4063_sales_replish_02), 'value' VALUE SUM(c4063_sales_replish_03), 'value' VALUE SUM(c4063_sales_replish_04), 'value' VALUE SUM(c4063_sales_replish_05), 'value' VALUE SUM(c4063_sales_replish_06)),',','},{')
      ||']},'
      ||
      --Safety Stock
      '{seriesname:"Safety Stock",valueBgColor:"#8A2BE2",data:['
      ||REPLACE(JSON_OBJECT( 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE SUM(c4063_cal_safety_stock_01), 'value' VALUE SUM(c4063_cal_safety_stock_02), 'value' VALUE SUM(c4063_cal_safety_stock_03), 'value' VALUE SUM(c4063_cal_safety_stock_04), 'value' VALUE SUM(c4063_cal_safety_stock_05), 'value' VALUE SUM(c4063_cal_safety_stock_06)),',','},{')
      ||']},'
      ||
      --Priority Set Previous Month
      '{seriesname:"Priority Set Previous Month",valueBgColor:"#FFFF00",data:['
      ||REPLACE(JSON_OBJECT( 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE SUM(c4063_cal_set_priority_pre_mon_qty), 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0'),',','},{')
      ||']},'
      ||
      --Priority Set Current Month
      '{seriesname:"Priority Set Current Month",valueBgColor:"#008000",data:['
      ||REPLACE(JSON_OBJECT( 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE SUM(c4063_cal_set_priority_01), 'value' VALUE SUM(c4063_cal_set_priority_02), 'value' VALUE SUM(c4063_cal_set_priority_03), 'value' VALUE SUM(c4063_cal_set_priority_04), 'value' VALUE SUM(c4063_cal_set_priority_05), 'value' VALUE SUM(c4063_cal_set_priority_06)),',','},{')
      ||']},'
      ||
      --Split Other Needs
      '{seriesname:"Split Other Needs",valueBgColor:"#00BFFF",data:['
      ||REPLACE(JSON_OBJECT( 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE SUM(c4063_cal_chart_other_need_01), 'value' VALUE SUM(c4063_cal_other_need_02), 'value' VALUE SUM(c4063_cal_other_need_03), 'value' VALUE SUM(c4063_cal_other_need_04), 'value' VALUE SUM(c4063_cal_other_need_05), 'value' VALUE SUM(c4063_cal_other_need_06)),',','},{')
      ||']},'
      ||
      --FCST Historical Receipt Avg
      '{seriesname:"FCST Historical Receipt Avg",renderas:"Line",valueBgColor:"#FF0000",data:['
      ||REPLACE(JSON_OBJECT( 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE SUM(c4063_pre_hist_rec_avg_03), 'value' VALUE SUM(c4063_pre_hist_rec_avg_02), 'value' VALUE SUM(c4063_pre_hist_rec_avg_01), 'value' VALUE SUM(c4063_fcst_hist_rec_avg_01), 'value' VALUE SUM(c4063_fcst_hist_rec_avg_02), 'value' VALUE SUM(c4063_fcst_hist_rec_avg_03), 'value' VALUE SUM(c4063_fcst_hist_rec_avg_04), 'value' VALUE SUM(c4063_fcst_hist_rec_avg_05), 'value' VALUE SUM(c4063_fcst_hist_rec_avg_06)),',','},{')
      ||']},'
      ||
      --Actual PO Qty (Historical)
      '{seriesname:"Actual PO Qty (Historical)",valueBgColor:"#7CFC00",data:['
      ||REPLACE(JSON_OBJECT( 'value' VALUE SUM(c4063_cal_hist_po_month_01), 'value' VALUE SUM(c4063_cal_hist_po_month_02), 'value' VALUE SUM(c4063_cal_hist_po_month_03), 'value' VALUE SUM(c4063_cal_hist_po_month_04), 'value' VALUE SUM(c4063_cal_hist_po_month_05), 'value' VALUE SUM(c4063_cal_hist_po_month_06), 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0', 'value' VALUE '0'),',','},{')
      ||']}]' JSON_DATA
    FROM t4063_ttp_forecast_by_part_dtls t4063, v_in_list v_list
    WHERE t4063.c4057_ttp_by_vendor_summary_id =v_list.token
       AND t4063.c4063_void_fl  IS NULL
    );
END gm_fch_po_chart_filter_dtls;

END gm_pkg_oppr_ld_ttp_by_vendor_capacity_rpt;
/