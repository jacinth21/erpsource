create or replace PACKAGE BODY gm_pkg_oppr_ld_ttp_by_vendor_capacity_txn
IS
/*************************************************************************
*Definition: This Procedure used to update the open status to 
			  approval status For Vendor
*Author:
**************************************************************************/
PROCEDURE gm_upd_vendor_capacity_approval(
p_vendor_id IN t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_id%TYPE,
p_load_date IN varchar2,
p_user_id   IN t4057_ttp_vendor_capacity_summary_dtls.c4057_approved_by%TYPE,
p_out_error_details OUT VARCHAR2
)

AS
 v_load_date DATE;
 v_vendor_open_cnt NUMBER;
 
CURSOR ttp_details_cur
 IS
	SELECT c4052_ttp_detail_id ttp_detail_id FROM t4057_ttp_vendor_capacity_summary_dtls
      WHERE c301_vendor_id = p_vendor_id
    AND c4057_load_date = v_load_date
    AND c4057_void_fl IS NULL
    GROUP BY c4052_ttp_detail_id;

BEGIN
	  --To remove to_char functionality get date format using dual  
     SELECT to_date(p_load_date ,'MM/yyyy')
     	INTO v_load_date
     FROM DUAL;
	
  
     gm_pkg_oppr_ld_ttp_by_vendor_capacity_txn.gm_validate_vendor_approval(p_vendor_id,v_load_date,p_out_error_details);
     
     IF (p_out_error_details = 'N')
     THEN
     --Update approved by and approved date in t4057
     gm_pkg_oppr_ld_ttp_by_vendor_capacity_txn.gm_upd_vendor_capa_summ_approved_dtls(p_vendor_id,v_load_date,p_user_id);
     
     gm_pkg_oppr_ld_ttp_by_vendor_capacity_txn.gm_upd_vendor_capa_summary_status(p_vendor_id,v_load_date,108862,p_user_id);		
		 
	   --Update open count based on ttp details id
	   FOR ttp_detail IN ttp_details_cur
	   LOOP
	   	--fetch open status from t4058 table 	
		SELECT count(1)
			INTO v_vendor_open_cnt
		FROM t4058_ttp_vendor_capacity_summary
		WHERE c4058_load_date     = v_load_date
		AND c4058_approved_by  IS NULL
		AND c4058_void_fl      IS NULL
		AND c301_vendor_id     IN
		  (SELECT c301_vendor_id
		  FROM t4057_ttp_vendor_capacity_summary_dtls
		  WHERE c4052_ttp_detail_id = ttp_detail.ttp_detail_id
		  AND c301_vendor_id       <> p_vendor_id
		  AND c4057_void_fl        IS NULL
		  GROUP BY c301_vendor_id
		  );

	       
	   gm_upd_pending_open_approval_cnt(v_vendor_open_cnt,ttp_detail.ttp_detail_id,p_user_id);
	   
	   END LOOP;  
       END IF;
  
END gm_upd_vendor_capacity_approval;
/*************************************************************************
*Definition: This Procedure used to validate vendor if vendor is already
			 approved
*Author:
**************************************************************************/

PROCEDURE gm_validate_vendor_approval(
p_vendor_id IN t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_id%TYPE,
p_load_date IN DATE,
p_out_error_details OUT VARCHAR2
)

AS
v_vendor_cnt NUMBER;
v_err_msg_fl VARCHAR2(10):= 'N';
BEGIN
	
	SELECT COUNT (1)
     INTO v_vendor_cnt
	FROM t4058_ttp_vendor_capacity_summary
	WHERE c301_vendor_id=p_vendor_id
	AND c4058_load_date = p_load_date
	AND c901_status    <> 108860 --Open status
	AND c4058_void_fl  IS NULL;
	
	IF (v_vendor_cnt > 0) THEN
	p_out_error_details :=  'This Vendor has already been Approved. Please enter another Vendor.'; 
	v_err_msg_fl := 'Y';
	p_out_error_details := v_err_msg_fl||'##'||p_out_error_details;
	ELSE
	p_out_error_details := v_err_msg_fl;
	
	END IF;
	
END gm_validate_vendor_approval;


/*************************************************************************
*Definition: This Procedure used to update the pending approval count
*Author:
**************************************************************************/
PROCEDURE gm_upd_pending_open_approval_cnt(
p_vendor_open_cnt IN NUMBER,
p_ttp_detail_id   IN t4059_ttp_capacity_summary.c4052_ttp_detail_id%TYPE,
p_user_id         IN t4059_ttp_capacity_summary.c4059_last_updated_by%TYPE
)

AS

BEGIN
	--Update open status count in t4059 table
	UPDATE t4059_ttp_capacity_summary
	 SET c4059_pending_approval_cnt = p_vendor_open_cnt,
         c4059_last_updated_by      = p_user_id,
         c4059_last_updated_date    = CURRENT_DATE
	WHERE c4052_ttp_detail_id       = p_ttp_detail_id
	AND c4059_void_fl             IS NULL;
	
END gm_upd_pending_open_approval_cnt;
/*************************************************************************
*Definition: 
*Author:
**************************************************************************/
PROCEDURE gm_upd_vendor_capa_summ_approved_dtls(
p_vendor_id IN t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_id%TYPE,
p_load_date IN DATE,
p_user_id   IN t4057_ttp_vendor_capacity_summary_dtls.c4057_approved_by%TYPE
)

AS

BEGIN
	
		--Update approved by and approved date in t4057
     	UPDATE t4057_ttp_vendor_capacity_summary_dtls
			SET c4057_approved_by    = p_user_id,
  				c4057_approved_date  = CURRENT_DATE,
  				c4057_last_updated_by = p_user_id,
  				c4057_last_updated_date=CURRENT_DATE
			WHERE c301_vendor_id     = p_vendor_id
			 AND c4057_load_date      = p_load_date
			 AND c4057_void_fl       IS NULL;
	
END gm_upd_vendor_capa_summ_approved_dtls;
/*************************************************************************
*Definition: 
*Author:
**************************************************************************/

PROCEDURE gm_upd_vendor_capa_summary_status(
p_vendor_id IN t4058_ttp_vendor_capacity_summary.c301_vendor_id%TYPE,
p_load_date IN DATE,
p_status_id IN t4058_ttp_vendor_capacity_summary.c901_status%TYPE,
p_user_id   IN t4058_ttp_vendor_capacity_summary.c4058_approved_by%TYPE
)

AS

BEGIN
	
	--update status open to approved,approved by and approved date in t4058	
		UPDATE t4058_ttp_vendor_capacity_summary
		   SET  c901_status            =p_status_id,--approved
  				c4058_approved_by      =p_user_id,
  				c4058_approved_date    =CURRENT_DATE,
  				c4058_last_updated_by  =p_user_id,
  				c4058_last_updated_date=CURRENT_DATE
		  WHERE c301_vendor_id     = p_vendor_id
			AND c4058_load_date    = p_load_date
			AND c4058_void_fl       IS NULL;
	
END gm_upd_vendor_capa_summary_status;

/*************************************************************************
*Definition: This Procedure used to save the override percentage and raise PO qty values
*Author: mselvamani
**************************************************************************/
PROCEDURE gm_override_split_qty_dtls(
    p_input_str IN CLOB,
    p_load_date IN VARCHAR2,
    p_user_id   IN t4059_ttp_capacity_summary.c4059_last_updated_by%TYPE,
    p_err_string OUT CLOB )
AS
  v_load_date DATE;
  v_strlen                   NUMBER         := NVL (LENGTH (p_input_str), 0) ;
  v_string                   VARCHAR2(4000) := p_input_str;
  v_substring                VARCHAR2(4000);
  v_capacity_summary_dtls_id NUMBER;
  v_override_per             NUMBER;
  v_out_error_dtls           VARCHAR2(4000);
  v_hist_per                 NUMBER;
  v_rasie_po_qty             NUMBER;
  v_status                   VARCHAR2(50);
  v_override_other_vendor_id t4057_ttp_vendor_capacity_summary_dtls.c301_override_vendor_id%type;
  v_override_other_split_per t4057_ttp_vendor_capacity_summary_dtls.c4057_other_override_split_per%type;
  v_ttp_dtl_id				 t4057_ttp_vendor_capacity_summary_dtls.c4052_ttp_detail_id%type;
  v_other_vendor_id			 t4057_ttp_vendor_capacity_summary_dtls.c301_second_vendor_id%type;
  v_load_dt					 t4057_ttp_vendor_capacity_summary_dtls.c4057_load_date%type;
  v_other_vendor_pec		 t4057_ttp_vendor_capacity_summary_dtls.c4057_other_override_split_per%type;
  v_part_num				 t4057_ttp_vendor_capacity_summary_dtls.c205_part_number_id%type;
  v_part_desc			     t4057_ttp_vendor_capacity_summary_dtls.c205_part_num_desc%type;
  v_total_order_qty			 t4057_ttp_vendor_capacity_summary_dtls.c4057_total_order_qty%type;
  v_po_fl					 t4057_ttp_vendor_capacity_summary_dtls.c4057_po_fl%type;
  v_raise_po_qty			 t4057_ttp_vendor_capacity_summary_dtls.c4057_raise_po_qty%type;
  v_new_raise_po_qty		 t4057_ttp_vendor_capacity_summary_dtls.c4057_raise_po_qty%type;
  v_vendor_id				 t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_id%type;
  v_other_capacity_summary_dtls_id t4057_ttp_vendor_capacity_summary_dtls.c4057_ttp_vendor_capa_dtls_id%type;
  v_historical_per  		 t4057_ttp_vendor_capacity_summary_dtls.c4057_history_split_per%type;
  v_old_override_vendor_id	 t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_id%type;
  v_raise_po_quantity		 t4057_ttp_vendor_capacity_summary_dtls.c4057_raise_po_qty%type;
  v_vendor_moq				 st405_vendor_active_pricing.c405_vendor_moq%type;
  v_vendor_price			 st405_vendor_active_pricing.c405_vendor_price%type;
  v_qty_quoted				 st405_vendor_active_pricing.c405_qty_quoted%type;
  v_pricing_id				 st405_vendor_active_pricing.c405_pricing_id%type;
  v_po_count				 NUMBER;
BEGIN
  --Update open status count in t4059 table
  --To remove to_char functionality get date format using dual
  SELECT to_date(p_load_date ,'MM/yyyy')
  INTO v_load_date
  FROM DUAL;
  IF v_strlen                    > 0 THEN
    WHILE INSTR (v_string, '|') <> 0
    LOOP
      v_out_error_dtls           := NULL;
      v_substring                := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
      v_string                   := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
      v_capacity_summary_dtls_id := NULL;
      v_override_per             := NULL;
      v_rasie_po_qty             := NULL;
      v_capacity_summary_dtls_id := TO_NUMBER(SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
      v_substring                := SUBSTR (v_substring, INSTR (v_substring, '^')              + 1);
      v_override_per             := TO_NUMBER(SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
      v_substring                := SUBSTR (v_substring, INSTR (v_substring, '^')              + 1);
      v_rasie_po_qty             := TO_NUMBER(SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1));
      v_substring                := SUBSTR (v_substring, INSTR (v_substring, '^')              + 1);
      v_override_other_vendor_id	 := TO_NUMBER(SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1));
      v_substring                := SUBSTR (v_substring, INSTR (v_substring, '^')              + 1);
      v_override_other_split_per := TO_NUMBER(v_substring);
      --This Procedure is used to check the approved status of vendor
      gm_pkg_oppr_ld_ttp_by_vendor_capacity_txn.gm_validate_capacity_dtls(v_capacity_summary_dtls_id,v_hist_per,v_rasie_po_qty,v_out_error_dtls,v_status);
      
      
      SELECT c4052_ttp_detail_id, c301_vendor_id ,c301_second_vendor_id,c301_override_vendor_id, c4057_load_date,
      		 c4057_other_override_split_per, c205_part_number_id,c205_part_num_desc,c4057_total_order_qty,
      		 c4057_history_split_per
        INTO v_ttp_dtl_id, v_vendor_id,v_other_vendor_id,v_old_override_vendor_id, v_load_dt, v_other_vendor_pec, 
             v_part_num, v_part_desc,v_total_order_qty,v_historical_per
        FROM T4057_TTP_VENDOR_CAPACITY_SUMMARY_DTLS
       WHERE c4057_ttp_vendor_capa_dtls_id = v_capacity_summary_dtls_id
         AND c4057_void_fl IS NULL;
      
      --Check v_out_error_dtls for  Update Percentage and Raise PO Qty in t4057 table
      IF v_status != '108862' THEN
        UPDATE t4057_ttp_vendor_capacity_summary_dtls
        SET c4057_override_split_per        = DECODE(v_override_per,NULL,null,v_override_per),
          c4057_raise_po_qty                = ROUND((DECODE(v_override_per,NULL,c4057_history_split_per ,v_override_per)/100)*c4057_total_order_qty,0),
          c4057_raise_po_qty_variance = nvl(c4057_recommand_qty,0) - ROUND((DECODE(v_override_per,NULL,c4057_history_split_per ,v_override_per)/100)*C4057_TOTAL_ORDER_QTY,0),
		  c4057_abs_raise_po_qty_variance = ABS(c4057_recommand_qty - ROUND((DECODE(v_override_per,NULL,c4057_history_split_per ,v_override_per)/100)*C4057_TOTAL_ORDER_QTY,0)),
          c4057_calclation_fl               = 'Y',
          c301_override_vendor_id			= v_override_other_vendor_id,
          c301_override_vendor_name = get_vendor_name(v_override_other_vendor_id),
          c4057_other_override_split_per	= v_override_other_split_per,
          c4057_last_updated_by             = p_user_id,
          c4057_last_updated_date           = CURRENT_DATE
        WHERE c4057_ttp_vendor_capa_dtls_id = v_capacity_summary_dtls_id
        AND c4057_void_fl                  IS NULL;
      ELSIF v_status                        = '108862' THEN
        UPDATE t4057_ttp_vendor_capacity_summary_dtls
        SET C4057_RAISE_PO_QTY              = DECODE(v_rasie_po_qty,NULL,C4057_RAISE_PO_QTY,v_rasie_po_qty),
          c4057_calclation_fl               = 'Y',
          c301_override_vendor_id			= v_override_other_vendor_id,
          c301_override_vendor_name = get_vendor_name(v_override_other_vendor_id),
          c4057_other_override_split_per	= v_override_other_split_per,
          c4057_last_updated_by             = p_user_id,
          c4057_last_updated_date           = CURRENT_DATE
        WHERE c4057_ttp_vendor_capa_dtls_id = v_capacity_summary_dtls_id
        AND c4057_void_fl                  IS NULL;
      END IF;
      
      --PBUG-4104 - if Raise PO qty zero then no need to compare with MOQ ---PMT-50039
     UPDATE t4057_ttp_vendor_capacity_summary_dtls
        SET c4057_raise_po_qty            = GREATEST(NVL(c4057_vendor_moq,0), c4057_raise_po_qty,0), 
            c4057_last_updated_by         = p_user_id,
            c4057_last_updated_date       = CURRENT_DATE
      WHERE c4057_ttp_vendor_capa_dtls_id = v_capacity_summary_dtls_id
        AND c4057_raise_po_qty            > 0
        AND c4057_void_fl IS NULL;

      --To insert and update override vendor details 
        IF v_override_other_vendor_id IS NOT NULL THEN
        	BEGIN
        	SELECT c4057_po_fl, nvl(c4057_old_po_qty,0)+c4057_raise_po_qty,c4057_ttp_vendor_capa_dtls_id,
        	       ROUND(((v_override_other_split_per/100)*c4057_total_order_qty),0)
        	  INTO v_po_fl, v_raise_po_qty,v_other_capacity_summary_dtls_id, v_new_raise_po_qty
        	  FROM T4057_TTP_VENDOR_CAPACITY_SUMMARY_DTLS
        	 WHERE c301_vendor_id = v_override_other_vendor_id
        	   AND c205_part_number_id = v_part_num
        	   AND c4052_ttp_detail_id = v_ttp_dtl_id
        	   AND c4057_load_date = v_load_dt
        	   AND c4057_void_fl is null;
        EXCEPTION WHEN OTHERS THEN
        BEGIN
            SELECT c4057_po_fl, nvl(c4057_old_po_qty,0)+c4057_raise_po_qty,c4057_ttp_vendor_capa_dtls_id,
        	       ROUND(((v_override_other_split_per/100)*c4057_total_order_qty),0)
        	  INTO v_po_fl, v_raise_po_qty,v_other_capacity_summary_dtls_id, v_new_raise_po_qty
        	  FROM T4057_TTP_VENDOR_CAPACITY_SUMMARY_DTLS
        	 WHERE c301_vendor_id = v_other_vendor_id
        	   AND c205_part_number_id = v_part_num
        	   AND c4052_ttp_detail_id = v_ttp_dtl_id
        	   AND c4057_load_date = v_load_dt
        	   AND c4057_void_fl is null;
        EXCEPTION WHEN OTHERS THEN
        BEGIN
	        SELECT c4057_po_fl, nvl(c4057_old_po_qty,0)+c4057_raise_po_qty,c4057_ttp_vendor_capa_dtls_id,
        	       ROUND(((v_override_other_split_per/100)*c4057_total_order_qty),0)
        	  INTO v_po_fl, v_raise_po_qty,v_other_capacity_summary_dtls_id, v_new_raise_po_qty
        	  FROM T4057_TTP_VENDOR_CAPACITY_SUMMARY_DTLS
        	 WHERE c301_vendor_id = v_vendor_id
        	   AND c205_part_number_id = v_part_num
        	   AND c4052_ttp_detail_id = v_ttp_dtl_id
        	   AND c4057_load_date = v_load_dt
        	   AND c4057_void_fl is null;
         EXCEPTION WHEN OTHERS THEN
               v_po_fl := NULL;
               v_raise_po_qty := 0;
         END; 
         END;
         END;
        	 
      BEGIN
	  	SELECT c405_vendor_moq,c405_vendor_price,c405_qty_quoted,c405_pricing_id
	      INTO v_vendor_moq,v_vendor_price,v_qty_quoted,v_pricing_id
	      FROM ST405_VENDOR_ACTIVE_PRICING
	     WHERE c301_vendor_id = v_override_other_vendor_id
	       AND c205_part_number_id = v_part_num
	       AND (v_raise_po_qty >= DECODE(c405_from_qty, 1, 0, c405_from_qty)  
			AND v_raise_po_qty <= c405_to_qty  );
	  EXCEPTION WHEN OTHERS THEN
	  		v_vendor_moq := 0;
	  		v_vendor_price := 0;
	  		v_qty_quoted := 0;
	  		v_pricing_id := 0;
	  END;
       
	     SELECT count(1) INTO v_po_count
	  	   FROM T4058_TTP_VENDOR_CAPACITY_SUMMARY
	      WHERE c4058_load_date = v_load_date
	   		AND c301_vendor_id = v_override_other_vendor_id
	   	    AND c901_status in ('26240822','26240823')
	        AND c4058_void_fl is null;
	       
	       
         	IF v_po_fl = 'Y' THEN
         		v_raise_po_quantity := v_new_raise_po_qty - v_raise_po_qty;
         	ELSE
         		v_raise_po_quantity := v_new_raise_po_qty;
         	END IF;
         		   
	    	gm_pkg_oppr_ttp_capa_load_override_vendor.gm_sav_ttp_vendor_capa_summary_dtls(v_ttp_dtl_id,
        				v_part_num,v_part_desc,v_load_dt,v_override_other_vendor_id,null,
        				v_vendor_price,v_qty_quoted,v_vendor_moq,v_pricing_id,v_total_order_qty,v_vendor_id,v_override_other_split_per,(100-v_override_other_split_per),v_raise_po_quantity, NULL, p_user_id);
        
        	 -- to compare vendor MOQ and update udpate the raise po Qty
        
       		gm_pkg_oppr_ld_ttp_by_vendor_capacity_txn. gm_upd_max_venor_moq_vs_raise_po_qty (v_other_capacity_summary_dtls_id, p_user_id);
       
        	-- to update 			
        	IF (v_po_fl = 'Y' OR v_po_count = '1') THEN         	
        	BEGIN
	        	SELECT c4057_ttp_vendor_capa_dtls_id
        	      INTO v_other_capacity_summary_dtls_id
        	      FROM T4057_TTP_VENDOR_CAPACITY_SUMMARY_DTLS
        	     WHERE c301_vendor_id = v_override_other_vendor_id
        	       AND c205_part_number_id = v_part_num
        	       AND c4052_ttp_detail_id = v_ttp_dtl_id
        	       AND c4057_load_date = v_load_dt
        	       AND c4057_void_fl is null;
            EXCEPTION WHEN OTHERS THEN
               v_other_capacity_summary_dtls_id := 0;
            END; 
        		gm_pkg_oppr_ttp_capa_load_override_vendor.gm_upd_po_regenerate_dtls(v_other_capacity_summary_dtls_id, v_override_other_vendor_id, v_part_num, 
        			v_raise_po_qty,v_new_raise_po_qty,'TBG', v_load_dt, p_user_id);
        		
        		-- if new entry update to t4057 (need to update greatest values)

       			gm_pkg_oppr_ld_ttp_by_vendor_capacity_txn. gm_upd_max_venor_moq_vs_raise_po_qty (v_other_capacity_summary_dtls_id, p_user_id);
       		
        		gm_pkg_oppr_ttp_capa_load_override_vendor.gm_sav_excess_po_dtls(v_ttp_dtl_id,v_override_other_vendor_id,v_part_num,v_raise_po_qty,v_new_raise_po_qty,p_user_id,v_other_capacity_summary_dtls_id,v_load_dt);
        	END IF;
       
        	IF v_other_vendor_id IS NOT NULL THEN
        		gm_pkg_oppr_ttp_capa_load_override_vendor.gm_lock_part_by_ttp_capa_summary(v_part_num,v_vendor_id,v_override_other_vendor_id,v_load_dt,'Y',p_user_id);
        	END IF;
        	--We need to discuss
        	IF v_old_override_vendor_id <> v_override_other_vendor_id THEN
        		gm_pkg_oppr_ttp_capa_load_override_vendor.gm_lock_part_by_ttp_capa_summary(v_part_num,v_vendor_id,v_override_other_vendor_id,v_load_dt,'Y',p_user_id);
        		gm_pkg_oppr_ttp_capa_load_override_vendor.gm_lock_part_by_ttp_capa_summary(v_part_num,v_vendor_id,v_old_override_vendor_id,v_load_dt,null,p_user_id);
        	END IF;
        
        ELSE
      
        	IF v_other_vendor_id IS NOT NULL THEN
        	BEGIN
        		SELECT c4057_po_fl, nvl(c4057_old_po_qty,0)+c4057_raise_po_qty,c4057_ttp_vendor_capa_dtls_id,
        	       	   ROUND(((v_override_other_split_per/100)*c4057_total_order_qty),0)
        	  	  INTO v_po_fl, v_raise_po_qty,v_other_capacity_summary_dtls_id, v_new_raise_po_qty
        	      FROM T4057_TTP_VENDOR_CAPACITY_SUMMARY_DTLS
        	     WHERE c301_vendor_id = v_other_vendor_id
        	       AND c205_part_number_id = v_part_num
        	       AND c4052_ttp_detail_id = v_ttp_dtl_id
        	       AND c4057_load_date = v_load_dt
        	       AND c4057_void_fl is null;
        	 EXCEPTION WHEN OTHERS THEN
               v_po_fl := NULL;
               v_raise_po_qty := 0;
             END; 
             
            BEGIN
	  			SELECT c405_vendor_moq,c405_vendor_price,c405_qty_quoted,c405_pricing_id
	      		  INTO v_vendor_moq,v_vendor_price,v_qty_quoted,v_pricing_id
	      		  FROM ST405_VENDOR_ACTIVE_PRICING
	     		 WHERE c301_vendor_id = v_override_other_vendor_id
	       		   AND c205_part_number_id = v_part_num
	       		   AND (v_raise_po_qty >= DECODE(c405_from_qty, 1, 0, c405_from_qty)  
				   AND v_raise_po_qty <= c405_to_qty  );
	  		EXCEPTION WHEN OTHERS THEN
	  			v_vendor_moq := 0;
	  			v_vendor_price := 0;
	  			v_qty_quoted := 0;
	  			v_pricing_id := 0;
	  		END;  
        	
       		IF v_po_fl = 'Y' THEN
         		v_raise_po_quantity := v_new_raise_po_qty - v_raise_po_qty;
         	ELSE
         		v_raise_po_quantity := v_new_raise_po_qty;
         	END IF;
       			
        	gm_pkg_oppr_ttp_capa_load_override_vendor.gm_sav_ttp_vendor_capa_summary_dtls(v_ttp_dtl_id,
        					v_part_num,v_part_desc,v_load_dt,v_other_vendor_id,null,
        					v_vendor_price,v_qty_quoted,v_vendor_moq,v_pricing_id,v_total_order_qty,null,v_override_other_split_per,v_override_per,v_raise_po_quantity, NULL, p_user_id);
         
         SELECT count(1) INTO v_po_count
	  	   FROM T4058_TTP_VENDOR_CAPACITY_SUMMARY
	      WHERE c4058_load_date = v_load_date
	   		AND c301_vendor_id = v_other_vendor_id
	   	    AND c901_status in ('26240822','26240823')
	        AND c4058_void_fl is null;
	        
        	IF (v_po_fl = 'Y' OR v_po_count = '1') THEN       	
        		gm_pkg_oppr_ttp_capa_load_override_vendor.gm_upd_po_regenerate_dtls(v_other_capacity_summary_dtls_id, v_other_vendor_id, v_part_num,
        			v_raise_po_qty,v_new_raise_po_qty,'TBG', v_load_date, p_user_id);
        		gm_pkg_oppr_ttp_capa_load_override_vendor.gm_sav_excess_po_dtls(v_ttp_dtl_id,v_other_vendor_id,v_part_num,v_raise_po_qty,v_new_raise_po_qty,p_user_id,v_other_capacity_summary_dtls_id,v_load_dt);
        	END IF;
        	 	
        	END IF;
        	
        END IF;
        
--        IF v_other_vendor_id <> v_override_other_vendor_id THEN
--        		gm_pkg_oppr_ttp_capa_load_override_vendor.gm_lock_part_by_ttp_capa_summary(v_part_num,v_other_vendor_id,v_override_other_vendor_id,v_load_dt,'Y',p_user_id);	
--        END IF;
      -- Append error string based on v_override_per and v_out_error_dtls
      IF v_override_per IS NOT NULL AND v_out_error_dtls IS NOT NULL THEN
        p_err_string    := p_err_string ||','|| v_out_error_dtls;
      END IF;
    END LOOP;
    --This procedure is used to update vendor status details
    gm_pkg_oppr_ttp_capa_load_override_vendor.gm_process_vendor_summary_dtls(v_load_dt,p_user_id);
    
  END IF;
END gm_override_split_qty_dtls;
/*************************************************************************
*Definition: This Procedure used to validate the vendor status for update Override Percentage
*Author: mselvamani
**************************************************************************/
PROCEDURE gm_validate_capacity_dtls(
    p_capacity_summary_dtls_id IN VARCHAR2,
    p_hist_per                 IN VARCHAR2,
    p_rasie_po_qty             IN t4059_ttp_capacity_summary.c4059_last_updated_by%TYPE,
    p_out_error_dtls OUT VARCHAR2,
    p_status OUT VARCHAR2 )
AS
  v_count NUMBER;
  v_load_date t4057_ttp_vendor_capacity_summary_dtls.c4057_load_date%TYPE;
  v_vendor_id t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_id%TYPE;
  v_status_id t4058_ttp_vendor_capacity_summary.c901_status%TYPE;
             
BEGIN
  --selecting ttp_detail_id and vendor_id based on c4057_ttp_vendor_capa_dtls_id
  SELECT c4057_load_date,
    c301_vendor_id
  INTO v_load_date,
    v_vendor_id
  FROM t4057_ttp_vendor_capacity_summary_dtls
  WHERE c4057_ttp_vendor_capa_dtls_id=p_capacity_summary_dtls_id
  AND c4057_void_fl                 IS NULL;
  
  --selecting status based on ttp_detail_id and vendor_id
  SELECT c901_status
  INTO v_status_id
  FROM t4058_ttp_vendor_capacity_summary
  WHERE c4058_load_date = v_load_date
  AND c301_vendor_id        = v_vendor_id
  AND c4058_void_fl        IS NULL;
  p_status                 := v_status_id;
  -- check the v_status_id for Percentage error check
  IF (v_status_id     = '108862') THEN
    p_out_error_dtls := p_capacity_summary_dtls_id;
  END IF;
END gm_validate_capacity_dtls;
/*************************************************************************
*Definition: This Procedure used to override percentage and qty through JMS call
*Author:mselvamani
**************************************************************************/
PROCEDURE gm_process_override_split_qty(
    p_load_date IN VARCHAR2,
    p_user_id   IN VARCHAR2 )
AS
  v_load_date DATE;
  CURSOR ttp_update_vendor_percentage_dtls
  IS
    SELECT c4052_ttp_detail_id ttp_detail_id
    FROM t4057_ttp_vendor_capacity_summary_dtls
    WHERE c4057_load_date   = v_load_date
    AND c4057_calclation_fl = 'Y'
    AND c4057_void_fl      IS NULL
    GROUP BY c4052_ttp_detail_id;
BEGIN
  SELECT to_date(p_load_date ,'MM/yyyy') INTO v_load_date FROM DUAL;
  --
  FOR ttp_dtls IN ttp_update_vendor_percentage_dtls
  LOOP
    -- This procedure is used to update the values in t4058_ttp_vendor_capacity_summary and t4059_ttp_capacity_summary
    -- to pass the load date (as a parameter)
    gm_pkg_oppr_ld_ttp_capa_master_txn.gm_upd_vendor_capa_master_dtls(ttp_dtls.ttp_detail_id,'N', v_load_date, p_user_id);
    --
    UPDATE t4057_ttp_vendor_capacity_summary_dtls
    SET c4057_calclation_fl   = NULL,
      c4057_last_updated_by   = p_user_id,
      c4057_last_updated_date = CURRENT_DATE
    WHERE c4052_ttp_detail_id = ttp_dtls.ttp_detail_id
    AND c4057_calclation_fl   = 'Y'
    AND c4057_void_fl        IS NULL;
  END LOOP;
END gm_process_override_split_qty;


/*************************************************************************
*Definition: This Procedure used to update the Approve status to 
			  Open status For Vendor.
*Author:      Prabhu Vigneshwaran M D
**************************************************************************/
PROCEDURE gm_upd_vendor_capacity_rollback(
p_vendor_id IN t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_id%TYPE,
p_load_date IN varchar2,
p_user_id   IN t4057_ttp_vendor_capacity_summary_dtls.c4057_approved_by%TYPE,
p_out_error_details OUT VARCHAR2
)

AS
 v_load_date DATE;
 v_vendor_open_cnt NUMBER;
 
CURSOR ttp_details_cur
 IS
	SELECT c4052_ttp_detail_id ttp_detail_id FROM t4057_ttp_vendor_capacity_summary_dtls
      WHERE c301_vendor_id = p_vendor_id
    AND c4057_load_date = v_load_date
    AND c4057_void_fl IS NULL
    GROUP BY c4052_ttp_detail_id;

BEGIN
	  --To remove to_char functionality get date format using dual  
     SELECT to_date(p_load_date ,'MM/yyyy')
     	INTO v_load_date
     FROM DUAL;
	
  
     gm_pkg_oppr_ld_ttp_by_vendor_capacity_txn.gm_validate_vendor_rollback(p_vendor_id,v_load_date,p_out_error_details);
     
     IF (p_out_error_details = 'N')
     THEN
     --Update approved by and approved date is null in t4057
     gm_pkg_oppr_ld_ttp_by_vendor_capacity_txn.gm_upd_vendor_capa_summ_rollback_dtls(p_vendor_id,v_load_date,p_user_id);
     --Update status as open ,approved by,approved date as null in t4058 -- 108860 Open 
     gm_pkg_oppr_ld_ttp_by_vendor_capacity_txn.gm_upd_vendor_capa_summ_open_status(p_vendor_id,v_load_date,108860,p_user_id);		
		 
	   --Update open count based on ttp details id
	   FOR ttp_detail IN ttp_details_cur
	   LOOP
	   	--fetch open status from t4058 table 	
		SELECT count(1)
			INTO v_vendor_open_cnt
		FROM t4058_ttp_vendor_capacity_summary
		WHERE c4058_load_date     = v_load_date
		AND c4058_approved_by  IS NULL
		AND c4058_void_fl      IS NULL
		AND c301_vendor_id  IN
		  (SELECT c301_vendor_id
		  FROM t4057_ttp_vendor_capacity_summary_dtls
		  WHERE c4052_ttp_detail_id = ttp_detail.ttp_detail_id
		  AND c301_vendor_id       <> p_vendor_id
		  AND c4057_void_fl        IS NULL
		  GROUP BY c301_vendor_id
		  );

	       
	   gm_pkg_oppr_ld_ttp_by_vendor_capacity_txn.gm_upd_pending_open_approval_cnt(v_vendor_open_cnt,ttp_detail.ttp_detail_id,p_user_id);
	   
	   END LOOP;  
       END IF;
  
END gm_upd_vendor_capacity_rollback;
/*************************************************************************
*Definition: This Procedure used to validate vendor if vendor is not
			 approved
*Author:
**************************************************************************/

PROCEDURE gm_validate_vendor_rollback(
p_vendor_id IN t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_id%TYPE,
p_load_date IN DATE,
p_out_error_details OUT VARCHAR2
)

AS
v_vendor_cnt NUMBER;
v_err_msg_fl VARCHAR2(10):= 'N';
BEGIN
	
	SELECT COUNT (1)
     INTO v_vendor_cnt
	FROM t4058_ttp_vendor_capacity_summary
	WHERE c301_vendor_id=p_vendor_id
	AND c4058_load_date = p_load_date
	AND c901_status    <> 108862 --Approved status
	AND c4058_void_fl  IS NULL;
	
	IF (v_vendor_cnt > 0) THEN
	p_out_error_details :=  'This Vendor is not Approved Status. Please enter another Vendor.'; 
	v_err_msg_fl := 'Y';
	p_out_error_details := v_err_msg_fl||'##'||p_out_error_details;
	ELSE
	p_out_error_details := v_err_msg_fl;
	
	END IF;
	
END gm_validate_vendor_rollback;


/*************************************************************************
*Definition: This Procedure Used to Update approved by and approved date is null 
*			 in t4057
*Author:     Prabhu vigneshwaran M D 
**************************************************************************/
PROCEDURE gm_upd_vendor_capa_summ_rollback_dtls(
p_vendor_id IN t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_id%TYPE,
p_load_date IN DATE,
p_user_id   IN t4057_ttp_vendor_capacity_summary_dtls.c4057_approved_by%TYPE
)

AS

BEGIN
	
		--Update approved by and approved date is null in t4057
     	UPDATE t4057_ttp_vendor_capacity_summary_dtls
			SET c4057_approved_by    = NULL,
  				c4057_approved_date  = NULL,
  				c4057_last_updated_by = p_user_id,
  				c4057_last_updated_date=CURRENT_DATE
			WHERE c301_vendor_id     = p_vendor_id
			 AND c4057_load_date      = p_load_date
			 AND c4057_approved_by IS NOT NULL
			 AND c4057_void_fl       IS NULL;
	
END gm_upd_vendor_capa_summ_rollback_dtls;
/*************************************************************************
*Definition: This Procedure used to update open status,approved by 
			 and approved date is null in t4058
*Author:     Prabhu vigneshwaran M D 
**************************************************************************/

PROCEDURE gm_upd_vendor_capa_summ_open_status(
p_vendor_id IN t4058_ttp_vendor_capacity_summary.c301_vendor_id%TYPE,
p_load_date IN DATE,
p_status_id IN t4058_ttp_vendor_capacity_summary.c901_status%TYPE,
p_user_id   IN t4058_ttp_vendor_capacity_summary.c4058_approved_by%TYPE
)

AS

BEGIN
--
		UPDATE t4058_ttp_vendor_capacity_summary
		   SET  c901_status            =p_status_id,
  				c4058_approved_by      =NULL,
  				c4058_approved_date    =NULL,
  				c4058_last_updated_by  =p_user_id,
  				c4058_last_updated_date=CURRENT_DATE
		  WHERE c301_vendor_id     = p_vendor_id
			AND c4058_load_date    = p_load_date
			AND c4058_approved_by IS NOT NULL
			AND c4058_void_fl       IS NULL;
	
END gm_upd_vendor_capa_summ_open_status;


/*********************************************************************************
    * Description : Procedure used to update the raise po qty based on max of MOQ or raise po qty
    * Author      : mmuthusamy
**********************************************************************************/

PROCEDURE gm_upd_max_venor_moq_vs_raise_po_qty (
p_capacity_summary_dtls_id   IN t4057_ttp_vendor_capacity_summary_dtls.c4057_ttp_vendor_capa_dtls_id%TYPE,
p_user_id   IN t4057_ttp_vendor_capacity_summary_dtls.c4057_last_updated_by%TYPE
)

AS

BEGIN
	
	--PBUG-4104 - if Raise PO qty zero then no need to compare with MOQ ---PMT-50039
     UPDATE t4057_ttp_vendor_capacity_summary_dtls
        SET c4057_raise_po_qty            = GREATEST(NVL(c4057_vendor_moq,0), c4057_raise_po_qty,0), 
            c4057_last_updated_by         = p_user_id,
            c4057_last_updated_date       = CURRENT_DATE
      WHERE c4057_ttp_vendor_capa_dtls_id = p_capacity_summary_dtls_id
        AND c4057_raise_po_qty            > 0
        AND c4057_void_fl IS NULL;
	
	END gm_upd_max_venor_moq_vs_raise_po_qty;
	
END gm_pkg_oppr_ld_ttp_by_vendor_capacity_txn;
/