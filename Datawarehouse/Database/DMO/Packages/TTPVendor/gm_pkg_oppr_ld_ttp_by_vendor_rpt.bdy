--@C:\PMT\bit\SpineIT-ERP\Datawarehouse\Database\DMO\Packages\TTPVendor\gm_pkg_oppr_ld_ttp_by_vendor_rpt.bdy;
CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ld_ttp_by_vendor_rpt
IS
--
 /***********************************************************************
 * Description : This Procedure is used to fethch the TTP Summary Detils 
 *                based on TTP id or vendor id
 * Author      : Agilan Singaravel
 ***********************************************************************/
--
PROCEDURE gm_fch_ttp_by_vendor_summary_rpt(
     p_ttp_id           IN   t4050_ttp.c4050_ttp_id%TYPE,
     p_vendor_id        IN   t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_id%TYPE,
     p_load_Date        IN   VARCHAR2,
     p_out_summary_dtls OUT  CLOB
 
)
AS
 v_load_date DATE;
BEGIN
	 --To remove to_char functionality get date format using dual  
     SELECT to_date(p_load_Date,'MM/yyyy') INTO v_load_date FROM DUAL;
 	
     --TTP Summary Detils     
	SELECT JSON_ARRAYAGG( 
  	   JSON_OBJECT(
  	     'ttpid'   		   value 	TTPID,
  		 'ttpdtlid'        value 	TTP_DTL_ID,
  		 'ttpname'         value 	TTP_NM,
  		 'vendorid'   	   value 	VEN_ID,
  	     'vendorname'      value 	TRIM(REGEXP_REPLACE(VEN_NM,'[^ ,0-9A-Za-z]', ' ')),
  		 'status'   	   value 	status,
  		 'recommandqty'    value REC_QTY,
  		 'raisepoqty'      value 	PO_QTY,
  		 'ordercost'       value 	ORD_COST,
  		 'period'          value    to_char(LOAD_DT,'Month' || ' '||'YYYY'),
  		 'month'           value    to_char(LOAD_DT,'mm'),
         'year'            value    to_char(LOAD_DT,'yyyy')
  		 )
	  	ORDER BY UPPER(TTP_NM),UPPER(VEN_NM) RETURNING CLOB)
	  	INTO p_out_summary_dtls
	  	FROM (SELECT
	    t4050.c4050_ttp_id ttpid,
	    t4057.c4052_ttp_detail_id ttp_dtl_id,
	    t4050.c4050_ttp_nm ttp_nm,
	    t4057.c301_vendor_id ven_id,
	    t4057.c301_vendor_name ven_nm,
	    t901.c901_code_nm status,
	    SUM(t4057.c4057_recommand_qty) rec_qty,
	    SUM(NVL(t4057.c4057_old_po_qty,0))+SUM(t4057.c4057_raise_po_qty) po_qty,
	    SUM(t4057.c4057_order_cost) ord_cost,
	    t4057.c4057_load_date load_dt
	  FROM 
		t4057_ttp_vendor_capacity_summary_dtls t4057,
		t4058_ttp_vendor_capacity_summary t4058,
		  t901_code_lookup t901,
		  t4052_ttp_detail t4052,
		  t4050_ttp t4050
		  WHERE t4057.c301_vendor_id = t4058.c301_vendor_id
		AND t4057.c4057_load_date  = t4058.c4058_load_date
		AND t4057.c4057_load_date = v_load_date
		AND t901.c901_code_id      = t4058.c901_status
		AND t4050.c4050_ttp_id = decode(p_ttp_id,null,t4050.c4050_ttp_id,p_ttp_id)
		AND t4057.c301_vendor_id = decode(p_vendor_id,null,t4057.c301_vendor_id,p_vendor_id)
		AND t4052.c4052_ttp_detail_id = T4057.c4052_ttp_detail_id 
		AND t4052.c4052_void_fl IS NULL
		AND t4050.c4050_void_fl IS NULL
		AND T4057.c4057_void_fl IS NULL
		AND t4058.c4058_void_fl IS NULL
		AND t4052.c4050_ttp_id = t4050.c4050_ttp_id
		AND t901.c901_void_fl     IS NULL
		GROUP BY  t4050.c4050_ttp_id,
		  t4057.c4052_ttp_detail_id,
		    t4050.c4050_ttp_nm,
		    t4057.c301_vendor_id,
		    t4057.c301_vendor_name,
		    t4057.c4057_load_date,
		    t901.c901_code_nm);
      
END gm_fch_ttp_by_vendor_summary_rpt;

 /*******************************************************
  * Description : This Procedure is used to fetch the TTP Vendor Report details
    author: mkosalram
  *******************************************************/
PROCEDURE gm_fch_ttp_by_vendor_part_rpt(
     p_ttp_id           IN   t4050_ttp.c4050_ttp_id%TYPE,
     p_vendor_id        IN   t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_id%TYPE,
     p_load_Date        IN   VARCHAR2,
     p_out_summary_dtls OUT  CLOB
)
AS
 v_load_date DATE;
 v_date_fmt VARCHAR2 (12);
 v_out_Details CLOB;
 v_out_heder_dtls CLOB;
BEGIN
	-- to get date format from context
	 --To remove to_char functionality get date format using dual  
     SELECT to_date(p_load_Date,'MM/yyyy'), get_compdtfmt_frm_cntx ()
       INTO v_load_date, v_date_fmt
       FROM DUAL;
       
      
SELECT JSON_ARRAYAGG( 
  	   JSON_OBJECT(
  	   	 'ttpname'		   value t4050.c4050_ttp_nm,
  	   	 'status'		   value t901.c901_code_nm,
  		 'vendorname'      value TRIM(REGEXP_REPLACE(t4057.c301_vendor_name,'[^ ,0-9A-Za-z]', ' ')),
  		 'partnum'         value t4057.c205_part_number_id,
  		 'partnumdesc'	   value TRIM(REGEXP_REPLACE(t4057.c205_part_num_desc,'[^ ,0-9A-Za-z]', ' ')),
  		 'period'		   value TO_CHAR(t4057.c4057_load_date, 'Mon ' || ' YYYY'),
	     'recommandqty'    value t4057.c4057_recommand_qty,
		 'raisePOqty'      value NVL(t4057.c4057_old_po_qty,0)+t4057.c4057_raise_po_qty,
		 'orderamt'		   value t4057.c4057_order_cost,
		 'ttpdid'		   value t4050.c4050_ttp_id,
		 'vendordid'	   value t4057.c301_vendor_id,
		 'ttpdmonth'	   value TO_CHAR(t4057.c4057_load_date, 'MM'),
		 'ttpdyear'	   	   value TO_CHAR(t4057.c4057_load_date, 'YYYY')
  		 )
  ORDER BY t4050.c4050_ttp_nm, t4057.c301_vendor_name, t4057.c205_part_number_id RETURNING CLOB)
  INTO p_out_summary_dtls
  FROM t4057_ttp_vendor_capacity_summary_dtls t4057,
  t4058_ttp_vendor_capacity_summary t4058,
  t4052_ttp_detail t4052,t4050_ttp t4050,
  t901_code_lookup t901
  WHERE t4050.c4050_ttp_id = t4052.c4050_ttp_id
    AND t4052.c4052_ttp_detail_id =t4057.c4052_ttp_detail_id
    AND t4058.c301_vendor_id=t4057.c301_vendor_id
	AND t4057.c4057_load_date     = t4058.c4058_load_date
  	AND t4057.c4057_load_date = t4052.c4052_ttp_link_date
	AND t4057.c4057_load_date = v_load_date
	AND t901.c901_code_id = t4058.c901_status
	AND t4057.c301_vendor_id = decode(p_vendor_id,null,t4057.c301_vendor_id,p_vendor_id)
	AND t4050.c4050_ttp_id = decode(p_ttp_id,null,t4050.c4050_ttp_id,p_ttp_id)
	AND t4057.c4057_void_fl IS null 
	AND t4052.C4052_void_fl IS null 
	AND t4050.C4050_void_fl IS null 
	AND t901.c901_void_fl   IS null
	AND t4058.c4058_void_fl IS null;


END gm_fch_ttp_by_vendor_part_rpt;

END gm_pkg_oppr_ld_ttp_by_vendor_rpt;
/