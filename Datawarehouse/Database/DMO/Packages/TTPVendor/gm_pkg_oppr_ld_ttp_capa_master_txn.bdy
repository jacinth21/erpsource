create or replace PACKAGE BODY gm_pkg_oppr_ld_ttp_capa_master_txn
IS

	/*************************************************************************
    * Description : Procedure to used to save the t4061_ttp_capa_forecast_vendor_dtls table data.
    *   			This information come from temp table
    * Author   : 
    *************************************************************************/
	procedure gm_sav_master_data_to_forecast_vendor (
		p_load_date	IN DATE
	)
	AS
	BEGIN
	--To save Main table t4061
		INSERT INTO t4061_ttp_capa_forecast_vendor_dtls (c4061_load_date,c301_vendor_id,c205_part_number_id,
    				 	c4061_historical_receipt_avg,c4061_fcst_hist_rec_avg_01,c4061_fcst_hist_rec_avg_02,
    			     	c4061_fcst_hist_rec_avg_03,c4061_fcst_hist_rec_avg_04,c4061_fcst_hist_rec_avg_05,
    				 	c4061_fcst_hist_rec_avg_06,c4061_prev_hist_rec_avg_01,c4061_prev_hist_rec_avg_02,
    				 	c4061_prev_hist_rec_avg_03, c4061_total_historical_receipt_qty )
    				 
    	SELECT  p_load_date, temp_vendor_id, c205_part_number_id,
           		temp_historical_receipt_3_mon_avg_qty,temp_fcst_hist_qty_mon_01,temp_fcst_hist_qty_mon_02,
           		temp_fcst_hist_qty_mon_03,temp_fcst_hist_qty_mon_04,temp_fcst_hist_qty_mon_05,
           		temp_fcst_hist_qty_mon_06,temp_prev_hist_receipt_avg_qty_01,temp_prev_hist_receipt_avg_qty_02,
           		temp_prev_hist_receipt_avg_qty_03, temp_dhr_qty
      	FROM temp_3month_vendor_historical_receipt;

	END gm_sav_master_data_to_forecast_vendor;
 
	/*************************************************************************
    * Description : Procedure to used to save all the master table data.
    *               This information come from temp table
    * Author   : 
    *************************************************************************/   
	procedure gm_sav_master_data_monthly_load_main (
		p_load_date	IN   DATE,
		p_user_id   IN   t4052_ttp_detail.c4052_last_updated_by%TYPE
	)
	AS
	BEGIN
		--1)To save t4057_ttp_vendor_capacity_summary_dtls
		gm_sav_master_to_capacity_summary_dtls (p_load_date,p_user_id);
		-- commit the code - above procedrue insert the data from ST4055 (more then 70,000) record
		-- So, i added the commit
		commit;
		
		-- This code update the vendor price based on Raise po qty
    	gm_upd_vendor_price('',p_load_date,p_user_id);
    
		--2)t4062_ttp_capa_forecast_inventory_dtls
		gm_sav_master_to_forecast_inventory (p_load_date,p_user_id);
		--3)t4063_ttp_forecast_by_part_dtls
		gm_sav_master_to_forecast_by_part (p_load_date,p_user_id);
		--4)t4064_ttp_forecast_by_vendor_dtls
		gm_sav_master_to_vendor_capacity_summary (p_load_date,p_user_id);
		--5)t4058_ttp_vendor_capacity_summary
		gm_sav_master_ven_capacity_summary (p_load_date);
		--6)t4059_ttp_capacity_summary
		gm_sav_master_to_capacity_summary (p_load_date);
		

	END gm_sav_master_data_monthly_load_main;

	/*************************************************************************
    * Description : Procedure to used to save master table data (T4057).
    *     This information come from temp table
    * Author   : 
    *************************************************************************/   
	procedure gm_sav_master_to_capacity_summary_dtls (
		p_load_date	IN DATE,
		p_user_id IN t4052_ttp_detail.c4052_last_updated_by%TYPE
	)
	AS
	v_cnt NUMBER := 0;
	v_vendor_id NUMBER;
	v_vendor_name VARCHAR2 (4000);
	v_vendor_price NUMBER;
	v_quoted_qty NUMBER;
	v_vendor_moq t4057_ttp_vendor_capacity_summary_dtls.c4057_vendor_moq%TYPE;
	v_price_id   t4057_ttp_vendor_capacity_summary_dtls.c4057_pricing_id%TYPE;
	v_no_vendor_id t402_work_order.c301_vendor_id%TYPE;
	--
	CURSOR ttp_empty_vendor_cur
	IS
	select C4057_TTP_VENDOR_CAPA_DTLS_ID id, c205_part_number_id pnum from t4057_ttp_vendor_capacity_summary_dtls
	where c301_vendor_id IS NULL
	and c4057_load_date = p_load_date
	AND c4057_void_fl IS NULL;
	--
	BEGIN

	 	INSERT INTO t4057_ttp_vendor_capacity_summary_dtls(
        	  		c4052_ttp_detail_id, c301_vendor_id, c301_vendor_name
      				, c4057_load_date, c205_part_number_id, c205_part_num_desc
      				, c402_vendor_price, c4057_hist_receipt_month_01, c4057_hist_receipt_month_02
      				, c4057_hist_receipt_month_03, c4057_hist_receipt_month_04, c4057_hist_receipt_month_05
      				, c4057_hist_receipt_month_06, c4057_hist_po_month_01, c4057_hist_po_month_02
      				, c4057_hist_po_month_03, c4057_hist_po_month_04, c4057_hist_po_month_05
      				, c4057_hist_po_month_06, c4057_history_split_per, c301_second_vendor_id
      				, c301_second_vendor_name, c4057_history_avg_qty, c4057_vendor_moq
      				, c4057_other_override_split_per,c4057_raise_po_qty
    	)
 		SELECT st4055.c4052_ttp_detail_id, tmp.c301_vendor_id, tmp.c301_vendor_name
  			 , st4055.c4055_load_date, st4055.c205_part_number_id, st4055.c205_part_num_desc
  			 , nvl(tmp.temp_vendor_price,0), nvl(tmp.temp_hist_receipt_month_01,0), nvl(tmp.temp_hist_receipt_month_02,0)
  			 , nvl(tmp.temp_hist_receipt_month_03,0), nvl(tmp.temp_hist_receipt_month_04,0), nvl(tmp.temp_hist_receipt_month_05,0)
  			 , nvl(tmp.temp_hist_receipt_month_06,0), nvl(tmp.temp_hist_po_month_01,0), nvl(tmp.temp_hist_po_month_02,0)
 			 , nvl(tmp.temp_hist_po_month_03,0), nvl(tmp.temp_hist_po_month_04,0), nvl(tmp.temp_hist_po_month_05,0)
 			 , nvl(tmp.temp_hist_po_month_06,0), nvl(tmp.temp_history_split_per,0), tmp.temp_second_vendor_id
 			 , tmp.temp_second_vendor_name, nvl(tmp.temp_history_po_6_mon_avg_qty,0), nvl(tmp.temp_vendor_moq,0)
 			 -- to update the other vendor per
 			 , DECODE(tmp.temp_second_vendor_id, NULL, NULL,100 - nvl(tmp.temp_history_split_per,0)),0
 		  FROM temp_vendor_capacity_summary_dtls tmp, st4055_ttp_summary st4055
 		  WHERE st4055.c205_part_number_id = tmp.c205_part_number_id
 		   AND st4055.c4055_load_date        = p_load_date;
 		   
 		--PC-2509 To bring open end WO search and load
 		--PC-2511 if no vendor price then - load in No Vendor
     	gm_pkg_oppr_ttp_capa_load_override_vendor.gm_upd_open_wo_vendor_dtls(p_load_date,1000,6,p_user_id);
     	--Get No Vendor Id From Rule table to Update Edit row lock flag column in t4057 table
     	v_no_vendor_id := get_rule_value('NO_VENDOR','TTP_BY_VEND_PO_GEN');
     	--Call gm_upd_edit_row_lock_fl procedure to update C4057_EDIT_ROW_LOCK_FL is Y in t4057 table
     	--For no vendor : 690
     	gm_pkg_oppr_ttp_capa_load_override_vendor.gm_upd_edit_row_lock_fl(p_load_date,v_no_vendor_id);
	END gm_sav_master_to_capacity_summary_dtls;
	
	/***********************************************************************************************************************************
    * Description : Procedure to update the vendor price based on the Raise PO Qty and the layers that is formed to get the vendor price.
    
    * Author   : Rajeshwaran 
    ***********************************************************************************************************************************/   
	procedure gm_upd_vendor_price (
		p_ttp_detail_id   IN t4059_ttp_capacity_summary.c4052_ttp_detail_id%TYPE,
		p_load_date	IN DATE,
		p_user_id IN t4052_ttp_detail.c4052_last_updated_by%TYPE
	)
	AS
		
		type pnum_array      IS        TABLE OF  T205_PART_NUMBER.c205_part_number_id%TYPE;
		type vend_price_array IS        TABLE OF  t4057_ttp_vendor_capacity_summary_dtls.C402_VENDOR_PRICE%TYPE;
		type ttp_dtl_id_array IS        TABLE OF  t4057_ttp_vendor_capacity_summary_dtls.C4052_TTP_DETAIL_ID%TYPE;
		type vend_quote_qty_array IS    TABLE OF   st405_vendor_active_pricing.c405_qty_quoted%TYPE;
		type price_id_array IS          TABLE OF  st405_vendor_active_pricing. c405_pricing_id %TYPE;
		--
		arr_pnum pnum_array;
		arr_vend_price vend_price_array;
		arr_ttp_dtl_id ttp_dtl_id_array;
		arr_vend_quote_qty vend_quote_qty_array;
		arr_price_id  price_id_array;
		--
		v_vendor_id NUMBER;
		
		-- to get all the vendor id
		CURSOR vendor_cur
		IS
		 SELECT t4057.c301_vendor_id vendor_id
		   FROM t4057_ttp_vendor_capacity_summary_dtls t4057
		  WHERE t4057.c4057_load_date     = NVL(p_load_date,c4057_load_date)
		    AND t4057.c4052_ttp_detail_id = NVL(p_ttp_detail_id,t4057.c4052_ttp_detail_id)
		    AND t4057.c4057_void_fl     IS NULL
		GROUP BY t4057.c301_vendor_id;
		
		CURSOR ttp_vend_price_cur
		IS

			SELECT t4057.C4052_TTP_DETAIL_ID ttp_dtl_id ,t4057.C205_PART_NUMBER_ID pnum, st405.C405_VENDOR_PRICE vend_price,
			        NVL(st405.c405_qty_quoted,0) vend_quote_qty,c405_pricing_id price_id
			FROM 	t4057_ttp_vendor_capacity_summary_dtls  t4057,
					st405_vendor_active_pricing st405
			WHERE C4057_VOID_FL is null 
			AND C4057_RAISE_PO_QTY >= 0
			AND t4057.C301_VENDOR_ID 		 = st405.C301_VENDOR_ID
			AND t4057.C205_PART_NUMBER_ID 	 = st405.C205_PART_NUMBER_ID
			-- to add the vendor id
			AND t4057.c301_vendor_id = v_vendor_id
			AND c4052_ttp_detail_id			 = NVL(p_ttp_detail_id,c4052_ttp_detail_id)
			AND t4057.c4057_load_date        = NVL(p_load_date,c4057_load_date)
			AND	(t4057.C4057_RAISE_PO_QTY >= DECODE(st405.C405_FROM_QTY, 1, 0, st405.C405_FROM_QTY)  
			AND t4057.C4057_RAISE_PO_QTY <= st405.C405_TO_QTY  );				
	
	BEGIN
	 -- bulk update
	
		-- to loop the vendor ids
		FOR vendor_dtl IN vendor_cur
		LOOP
			-- assign the vendor 
			v_vendor_id := vendor_dtl.vendor_id;
			--
			
			
			OPEN ttp_vend_price_cur;
			LOOP
			FETCH ttp_vend_price_cur BULK COLLECT INTO  arr_ttp_dtl_id, arr_pnum,arr_vend_price ,arr_vend_quote_qty,arr_price_id LIMIT 500;
	
			FORALL i in 1.. arr_pnum.COUNT
			
			
				UPDATE t4057_ttp_vendor_capacity_summary_dtls 
				SET
					C402_VENDOR_PRICE = arr_vend_price(i),
	           	    C4057_ORDER_COST = arr_vend_price(i) * c4057_raise_po_qty,
	                c4057_last_updated_by = p_user_id,
	                c4057_last_updated_date = CURRENT_DATE,
	                c4057_qty_quoted = NVL(arr_vend_quote_qty(i),0), --PBUG-4105
	                c4057_pricing_id = arr_price_id(i) -- PC-295 updating pricing id 
				WHERE c4052_ttp_detail_id = arr_ttp_dtl_id(i)
					AND c205_part_number_id = arr_pnum(i)
					AND c301_vendor_id = v_vendor_id
					AND c4057_void_fl IS NULL;
				
			EXIT WHEN ttp_vend_price_cur%NOTFOUND;
		    END LOOP;
		    CLOSE ttp_vend_price_cur;
		    
		END LOOP; -- end of vendor details loop
	
	END gm_upd_vendor_price;
	
  	/*************************************************************************
    * Description : Procedure to used to save master table data (T4062).
    *     This information come from temp table
    * Author   : 
    *************************************************************************/ 
	procedure gm_sav_master_to_forecast_inventory (
		p_load_date	IN DATE,
		p_user_id IN t4052_ttp_detail.c4052_last_updated_by%TYPE
	)
	AS
	BEGIN
	
	 	INSERT INTO t4062_ttp_capa_forecast_inventory_dtls (c4062_load_date,
        			 c205_part_number_id, c4052_ttp_detail_id
      			  , c4062_back_order_qty,c4062_set_priority_pre_mon_qty, c4062_set_priority_qty_01
      			  , c4062_set_priority_qty_02, c4062_set_priority_qty_03, c4062_set_priority_qty_04
      			  , c4062_set_priority_qty_05, c4062_set_priority_qty_06--, c4062_last_updated_by, c4062_last_updated_date     
      			  , c4062_net_on_hand, c4062_total_required_qty, c4062_sales_replish_01
				  ,	c4062_sales_replish_02, c4062_sales_replish_03, c4062_sales_replish_04
				  ,	c4062_sales_replish_05, c4062_sales_replish_06,	c4062_sales_replish_07
				  ,	c4062_sales_replish_08,	c4062_sales_replish_09, c4062_sales_replish_10
				  ,	c4062_sales_replish_11, c4062_sales_replish_12,c4062_safety_stock_01
				  ,	c4062_safety_stock_02, c4062_safety_stock_03, c4062_safety_stock_04
				  ,	c4062_safety_stock_05, c4062_safety_stock_06
    	)
    	  SELECT  trunc(p_load_date,'month'),st4055.c205_part_number_id, st4055.c4052_ttp_detail_id --p_load_date,
 			 , sum(nvl(st502.c502_bo_qty,0)), sum(nvl(tmp_set.c520_previous_sp_qty,0)), sum(nvl(tmp_set.c520_future_sp_qty_01,0))
 			 , sum(nvl(tmp_set.c520_future_sp_qty_02,0)), sum(nvl(tmp_set.c520_future_sp_qty_03,0)), sum(nvl(tmp_set.c520_future_sp_qty_04,0))
 			 , sum(nvl(tmp_set.c520_future_sp_qty_05,0)), sum(nvl(tmp_set.c520_future_sp_qty_06,0))--, p_user_id, CURRENT_DATE
 			 , sum(nvl(st4055.c251_net_on_hand, 0)), 0, 0
 			 , 0, 0, 0
 			 , 0, 0, 0
 			 , 0, 0, 0
 			 , 0, 0, 0
 			 , 0, 0, 0
 			 , 0, 0
  		  FROM st502_back_order_dtls st502, temp_set_priority_dtls tmp_set, ST4055_TTP_SUMMARY st4055
 		 WHERE st4055.c205_part_number_id = tmp_set.c205_part_number_id (+)
   		   AND st4055.c205_part_number_id = st502.c205_part_number_id (+)
         group by  st4055.c205_part_number_id,st4055.c4052_ttp_detail_id,trunc(p_load_date,'month');
         
	END gm_sav_master_to_forecast_inventory;

	/*************************************************************************
    * Description : Procedure to used to save master table data (T4063).
    *     This information come from temp table
    * Author   : 
    *************************************************************************/  
	procedure gm_sav_master_to_forecast_by_part (
		p_load_date	IN DATE,
		p_user_id IN t4052_ttp_detail.c4052_last_updated_by%TYPE
	)
	AS
	BEGIN
		
 		INSERT INTO t4063_ttp_forecast_by_part_dtls
 		(
        	c4057_ttp_by_vendor_summary_id, c301_vendor_id, c205_part_number_id
      	  , c4063_load_date, c4063_history_split_per, c4063_historical_receipt_avg
     	  , c4063_fcst_hist_rec_avg_01, c4063_fcst_hist_rec_avg_02, c4063_fcst_hist_rec_avg_03
    	  , c4063_fcst_hist_rec_avg_04, c4063_fcst_hist_rec_avg_05, c4063_fcst_hist_rec_avg_06
   	      , c4063_pre_hist_rec_avg_01, c4063_pre_hist_rec_avg_02, c4063_pre_hist_rec_avg_03,
   	      c4063_cal_hist_receipt_month_01,c4063_cal_hist_receipt_month_02,c4063_cal_hist_receipt_month_03,
   	      c4063_cal_hist_receipt_month_04,c4063_cal_hist_receipt_month_05,c4063_cal_hist_receipt_month_06,
   	      c4063_cal_hist_po_month_01,c4063_cal_hist_po_month_02,c4063_cal_hist_po_month_03,c4063_cal_hist_po_month_04,
   	      c4063_cal_hist_po_month_05,c4063_cal_hist_po_month_06,c4052_ttp_detail_id
   	      --
   	      , c4063_cal_back_order_qty, c4063_cal_set_priority_pre_mon_qty, c4063_cal_set_priority_01
   	      , c4063_cal_set_priority_02 , c4063_cal_set_priority_03 , c4063_cal_set_priority_04
		  ,	c4063_cal_set_priority_05 , c4063_cal_set_priority_06 , c4063_total_required_qty
		  ,	c4063_sales_replish_01 , c4063_sales_replish_02 , c4063_sales_replish_03
		  ,	c4063_sales_replish_04 , c4063_sales_replish_05 , c4063_sales_replish_06
		  ,	c4063_sales_replish_07, c4063_sales_replish_08    	  
    	)
 	SELECT t4057.c4057_ttp_vendor_capa_dtls_id, t4057.c301_vendor_id, t4057.c205_part_number_id
  		 , t4057.c4057_load_date, t4057.c4057_history_split_per, NVL(t4061.c4061_historical_receipt_avg, 0)
 		 , NVL(t4061.c4061_fcst_hist_rec_avg_01, 0), NVL(t4061.c4061_fcst_hist_rec_avg_02, 0), NVL(t4061.c4061_fcst_hist_rec_avg_03, 0)
 		 , NVL(t4061.c4061_fcst_hist_rec_avg_04, 0), NVL(t4061.c4061_fcst_hist_rec_avg_05, 0), NVL(t4061.c4061_fcst_hist_rec_avg_06, 0)
 		 , NVL(t4061.c4061_prev_hist_rec_avg_01,0), NVL(t4061.c4061_prev_hist_rec_avg_02,0), NVL(t4061.c4061_prev_hist_rec_avg_03, 0)
 		 , NVL(t4057.c4057_hist_receipt_month_01, 0),NVL(t4057.c4057_hist_receipt_month_02, 0),NVL(t4057.c4057_hist_receipt_month_03, 0)
    	 , NVL(t4057.c4057_hist_receipt_month_04, 0),NVL(t4057.c4057_hist_receipt_month_05, 0), NVL(t4057.c4057_hist_receipt_month_06, 0)
    	 , NVL(t4057.c4057_hist_po_month_01, 0), NVL(t4057.c4057_hist_po_month_02, 0), NVL(t4057.c4057_hist_po_month_03, 0)
    	 , NVL(t4057.c4057_hist_po_month_04, 0)
    	 , NVL(t4057.c4057_hist_po_month_05, 0), NVL(t4057.c4057_hist_po_month_06, 0), t4057.c4052_ttp_detail_id
    	 --
    	 , 0, 0, 0
    	 , 0, 0, 0
    	 , 0, 0, 0
    	 , 0, 0, 0
    	 , 0, 0, 0
    	 , 0, 0
   	  FROM t4057_ttp_vendor_capacity_summary_dtls t4057, t4061_ttp_capa_forecast_vendor_dtls t4061
     WHERE t4057.c205_part_number_id = t4061.c205_part_number_id (+)
       AND t4057.c301_vendor_id      = t4061.c301_vendor_id (+)
       AND t4057.c4057_load_date     = t4061.c4061_load_date (+)
       AND t4057.c4057_load_date     = p_load_date
      AND t4061.c4061_void_fl      IS NULL
      AND t4057.c4057_void_fl      IS NULL;
	
	END gm_sav_master_to_forecast_by_part;

	/*************************************************************************
    * Description : Procedure to used to save master table data (T4058).
    *     This information come from temp table
    * Author   : 
    *************************************************************************/ 
	procedure gm_sav_master_to_vendor_capacity_summary (
		p_load_date	IN DATE,
		p_user_id IN t4052_ttp_detail.c4052_last_updated_by%TYPE
	)
	AS
	BEGIN
		
 		INSERT INTO t4064_ttp_forecast_by_vendor_dtls
    	(
        	c301_vendor_id, c4064_load_date, c4064_historical_receipt_avg
      	  , c4064_fcst_hist_rec_avg_01, c4064_fcst_hist_rec_avg_02, c4064_fcst_hist_rec_avg_03
      	  , c4064_fcst_hist_rec_avg_04, c4064_fcst_hist_rec_avg_05, c4064_fcst_hist_rec_avg_06
     	  , c4064_pre_hist_rec_avg_01,  c4064_pre_hist_rec_avg_02, c4064_pre_hist_rec_avg_03
     	  ,c4064_cal_hist_receipt_month_01,c4064_cal_hist_receipt_month_02,c4064_cal_hist_receipt_month_03
     	  ,c4064_cal_hist_receipt_month_04,c4064_cal_hist_receipt_month_05,c4064_cal_hist_receipt_month_06
     	  ,c4064_cal_hist_po_month_01,c4064_cal_hist_po_month_02,c4064_cal_hist_po_month_03
     	  ,c4064_cal_hist_po_month_04,c4064_cal_hist_po_month_05,c4064_cal_hist_po_month_06
    	)
 		SELECT t4057.c301_vendor_id, t4057.c4057_load_date, sum (nvl (t4061.c4061_historical_receipt_avg, 0))
  			 , sum (nvl (t4061.c4061_fcst_hist_rec_avg_01, 0)), sum (nvl (t4061.c4061_fcst_hist_rec_avg_02, 0)) 
  			 , sum (nvl (t4061.c4061_fcst_hist_rec_avg_03, 0)), sum (nvl (t4061.c4061_fcst_hist_rec_avg_04, 0)) 
    		 , sum (nvl (t4061.c4061_fcst_hist_rec_avg_05, 0)), sum (nvl (t4061.c4061_fcst_hist_rec_avg_06, 0))
    		 , sum (nvl (t4061.c4061_prev_hist_rec_avg_01, 0)), sum (nvl (t4061.c4061_prev_hist_rec_avg_02, 0))
    		 , sum (nvl (t4061.c4061_prev_hist_rec_avg_03, 0)),sum(nvl(t4057.c4057_hist_receipt_month_01,0))
    		 ,sum(nvl(t4057.c4057_hist_receipt_month_02,0)), sum(nvl(t4057.c4057_hist_receipt_month_03,0))
    		 ,sum(nvl(t4057.c4057_hist_receipt_month_04,0)), sum(nvl(t4057.c4057_hist_receipt_month_05,0))
    		 ,sum(nvl(t4057.c4057_hist_receipt_month_06,0)),sum(nvl(t4057.c4057_hist_po_month_01,0))
    		 ,sum(nvl(t4057.c4057_hist_po_month_02,0)),sum(nvl(t4057.c4057_hist_po_month_03,0))
    		 ,sum(nvl(t4057.c4057_hist_po_month_04,0)),sum(nvl(t4057.c4057_hist_po_month_05,0))
    		 ,sum(nvl(t4057.c4057_hist_po_month_06,0))
   		 FROM t4057_ttp_vendor_capacity_summary_dtls t4057, t4061_ttp_capa_forecast_vendor_dtls t4061
  		WHERE t4057.c205_part_number_id = t4061.c205_part_number_id (+)
    	  AND t4057.c301_vendor_id      = t4061.c301_vendor_id (+)
    	  AND t4057.c4057_load_date     = t4061.c4061_load_date (+)
    	  AND t4057.c4057_load_date     = p_load_date
    	  AND t4061.c4061_void_fl      IS NULL
    	  AND t4057.c4057_void_fl      IS NULL
	 GROUP BY t4057.c301_vendor_id, t4057.c4057_load_date;
	
	END gm_sav_master_to_vendor_capacity_summary;
	
	
   /*************************************************************************
    * Description : Procedure to used to save master table data (T4058).
    *     This information come from temp table
    * Author   : 
    *************************************************************************/ 
	procedure gm_sav_master_ven_capacity_summary (
		p_load_date	IN DATE
	)
	AS
	BEGIN
 		INSERT INTO t4058_ttp_vendor_capacity_summary
    	(c4058_load_date,c301_vendor_id,c301_vendor_name,c901_status,c901_po_type)
 		 SELECT t4057.c4057_load_date, t4057.c301_vendor_id, t4057.c301_vendor_name,108860,3100 --TBE
         FROM  t4057_ttp_vendor_capacity_summary_dtls t4057,t4052_ttp_detail t4052
         WHERE t4052.c4052_ttp_link_date =  t4057.c4057_load_date
         AND   t4057.c4057_load_date     = p_load_date
         AND   t4057.c301_vendor_id IS NOT NULL
         AND   t4052.c4052_void_fl IS NULL
         group by t4057.c4057_load_date, t4057.c301_vendor_id,t4057.c301_vendor_name;
    	
	END gm_sav_master_ven_capacity_summary;
	
	

	/*************************************************************************
    * Description : Procedure to used to save master table data (T4059).
    *     This information come from temp table
    * Author   : 
    *************************************************************************/ 
	procedure gm_sav_master_to_capacity_summary (
		p_load_date	IN DATE
	)
	AS
	BEGIN
	
 		INSERT INTO t4059_ttp_capacity_summary
    	(
        	c4050_ttp_id,c4052_ttp_detail_id, c4050_ttp_name, c4059_load_date,c901_status
    	)
 	    SELECT t4050.c4050_ttp_id,t4057.c4052_ttp_detail_id, t4050.c4050_ttp_nm, t4057.c4057_load_date,108866 -- status --
          FROM  t4057_ttp_vendor_capacity_summary_dtls t4057,t4052_ttp_detail t4052, t4050_ttp t4050
         WHERE  t4050.c4050_ttp_id        = t4052.c4050_ttp_id
         AND    t4057.c4052_ttp_detail_id =  t4052.c4052_ttp_detail_id
         AND    t4052.c4052_ttp_link_date =  t4057.c4057_load_date
         AND    t4057.c4057_load_date     = p_load_date
         AND    t4052.c4052_void_fl      IS NULL
         AND    t4050.c4050_void_fl      IS NULL
         AND    t4057.c301_vendor_id IS NOT NULL
         GROUP BY t4050.c4050_ttp_id,t4057.c4052_ttp_detail_id,t4050.c4050_ttp_nm,t4057.c4057_load_date;
    	
	END gm_sav_master_to_capacity_summary;
	
	/*************************************************************************
    * Description : Procedure to used to update master data required details.
    * Author   : 
    *************************************************************************/ 
	procedure gm_upd_master_data_load (
		p_load_date	IN DATE,
		p_user_id IN t4052_ttp_detail.c4052_last_updated_by%TYPE
	)
	AS
	CURSOR ttp_details_cur
	IS
 		SELECT t4052.c4052_ttp_detail_id ttp_detail_id
   		  FROM t4052_ttp_detail t4052
  		 WHERE t4052.c4052_ttp_link_date = p_load_date
           AND t4052.c4052_void_fl      IS NULL
	  ORDER BY t4052.c4052_ttp_detail_id;

	BEGIN
		-- 
		--
		dbms_output.put_line ('^^^^ Started master data load  '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		
		FOR ttp_dtls IN ttp_details_cur
	    LOOP
	    dbms_output.put_line ('********************* TTP started  '||ttp_dtls.ttp_detail_id ||' '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		
	     gm_upd_vendor_capa_master_dtls (ttp_dtls.ttp_detail_id, 'Y', p_load_date, p_user_id); 
	     dbms_output.put_line ('********************* TTP ended  '||ttp_dtls.ttp_detail_id ||' '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
	     -- totally 4 tables updated, so added the commit 
	     commit;
		
	    END LOOP;
dbms_output.put_line ('^^^^ Ended master data load  '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		
	END  gm_upd_master_data_load;
	
	/*************************************************************************
    * Description : Procedure to used to update master data required details based on ttp id.
    * Author   : 
    *************************************************************************/
	procedure gm_upd_vendor_capa_master_dtls (
		p_ttp_details_id	IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE,
		p_first_time_fl IN VARCHAR2,
		p_load_date IN DATE,
		p_user_id IN t4052_ttp_detail.c4052_last_updated_by%TYPE
	)
	AS
	
	CURSOR vendor_id_cur 
	IS
   		SELECT c301_vendor_id vendor_id,c4057_load_date load_date
		  FROM t4057_ttp_vendor_capacity_summary_dtls
		 WHERE c4052_ttp_detail_id = p_ttp_details_id
    	   AND c4057_void_fl IS NULL
         GROUP BY c301_vendor_id,c4057_load_date;
	BEGIN
		
			IF p_first_time_fl = 'Y' THEN
			--1)t4057_ttp_vendor_capacity_summary_dtls
			gm_upd_ord_qty_capacity_summary_dtls_table (p_ttp_details_id,  p_user_id);
	
			dbms_output.put_line ('AAAA before T4062  '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		
			--2)t4062_ttp_capa_forecast_inventory_dtls
			gm_upd_required_and_repln_qty_inventory_table (p_ttp_details_id, p_user_id);
			dbms_output.put_line ('AAAA After T4062  '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		
			END IF;
			
			--to update second vendor PO qty on t5047
			gm_upd_other_vendor_PO_qty(p_ttp_details_id,  p_user_id);
			
			--3)T4057_TTP_VENDOR_CAPACITY_SUMMARY_DTLS
			
			-- As Order cost going to be updated, need to update the Vendor Price based on the Raise PO Qty. 
			gm_upd_vendor_price_by_calculation (p_ttp_details_id, p_user_id);
			
			gm_upd_order_cost(p_ttp_details_id);
			
			dbms_output.put_line ('AAAA before T4063  '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		
			--4)T4063_TTP_FORECAST_BY_PART_DTLS
			gm_calc_forecast_by_part (p_ttp_details_id, p_load_date, p_user_id);

			dbms_output.put_line ('AAAA after T4063  '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		
			FOR  vendor IN vendor_id_cur
			loop
			-- 5)T4064_TTP_FORECAST_BY_VENDOR_DTLS
			gm_calc_forecast_by_vendor (vendor.vendor_id, vendor.load_date, p_user_id);
	
		    END loop;
		    dbms_output.put_line ('AAAA After T4064  '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		
			--6)T4057_TTP_VENDOR_CAPACITY_SUMMARY_DTLS
			gm_upd_forecast_data_capactiy_summary_dtls_table (p_ttp_details_id, p_user_id);
	
			--7)T4058_TTP_VENDOR_CAPACITY_SUMMARY
			FOR  vendor IN vendor_id_cur
			loop
			gm_upd_overall_summary_to_vendor_summary_table (vendor.vendor_id, vendor.load_date, p_user_id);
			END loop;
	
			IF p_first_time_fl = 'Y' THEN
			--8)T4059_TTP_CAPACITY_SUMMARY
			gm_upd_orginal_ord_dtls_capacity_summary (p_ttp_details_id, p_user_id);
		
			
			
			END IF;
			--9)T4059_TTP_CAPACITY_SUMMARY
			gm_upd_ord_dtls_capacity_summary (p_ttp_details_id, p_user_id);
	
			--10)T4059_TTP_CAPACITY_SUMMARY
			gm_upd_pending_approval_cnt (p_ttp_details_id, p_user_id);
	
	END gm_upd_vendor_capa_master_dtls;
	
	/*************************************************************************
    * Description : Procedure to used to update master data required details based on ttp id.
    * Author   : 1
    *************************************************************************/  
	procedure gm_upd_ord_qty_capacity_summary_dtls_table (
		p_ttp_details_id	IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE,
		p_user_id IN t4052_ttp_detail.c4052_last_updated_by%TYPE
	)
	AS
		--
		type pnum_array      IS        TABLE OF  T205_PART_NUMBER.c205_part_number_id%TYPE;
		type order_qty_array IS        TABLE OF  st4055_ttp_summary.c4055_total_order_qty%TYPE;
		--
		arr_pnum pnum_array;
		arr_qty order_qty_array;
		--
	CURSOR ttp_ord_qty_cur
	IS

		SELECT c205_part_number_id pnum, c4055_total_order_qty order_qty
 		  FROM st4055_ttp_summary st4055
		 WHERE c4052_ttp_detail_id = p_ttp_details_id;

 
	BEGIN
	 -- bulk update
	
		OPEN ttp_ord_qty_cur;
		LOOP
		FETCH ttp_ord_qty_cur BULK COLLECT INTO arr_pnum,arr_qty LIMIT 500;

		FORALL i in 1.. arr_pnum.COUNT
		
			UPDATE t4057_ttp_vendor_capacity_summary_dtls 
			SET
				c4057_total_order_qty = arr_qty(i),
           	    c4057_recommand_qty = round((arr_qty(i) * c4057_history_split_per)/ 100 , 0),
                c4057_raise_po_qty = NVL(round((arr_qty(i) * NVL(c4057_override_split_per, c4057_history_split_per) )/ 100 , 0), 0),
                c4057_raise_po_qty_variance = round((arr_qty(i) * c4057_history_split_per)/ 100 , 0) - round((arr_qty(i) * nvl(c4057_override_split_per, c4057_history_split_per) )/ 100 , 0),
				c4057_abs_raise_po_qty_variance = ABS(round((arr_qty(i) * c4057_history_split_per)/ 100 , 0) - round((arr_qty(i) * nvl(c4057_override_split_per, c4057_history_split_per) )/ 100 , 0)),
                c4057_last_updated_by = p_user_id,
                c4057_last_updated_date = CURRENT_DATE,
                c4057_qty_quoted     = NVL(c4057_qty_quoted,0) --PBUG-4105
			WHERE c4052_ttp_detail_id = p_ttp_details_id
			AND c205_part_number_id = arr_pnum(i)
			AND c4057_void_fl IS NULL;
			
		EXIT WHEN ttp_ord_qty_cur%NOTFOUND;
	    END LOOP;
	    CLOSE ttp_ord_qty_cur;
	 
	--PBUG-4104 - if Raise PO qty zero then no need to compare with MOQ
	UPDATE t4057_ttp_vendor_capacity_summary_dtls
      SET c4057_raise_po_qty      = GREATEST(NVL(c4057_vendor_moq,0),c4057_raise_po_qty,0), 
          c4057_last_updated_by   = p_user_id, 
          c4057_last_updated_date = CURRENT_DATE
    WHERE c4052_ttp_detail_id     = p_ttp_details_id
      AND c4057_raise_po_qty  > 0
      AND c4057_void_fl IS NULL;

	END gm_upd_ord_qty_capacity_summary_dtls_table;
	
	/*************************************************************************
    * Description : Procedure to used to update second vendor po qty
    * Author   : Agilan Singaravel
    *************************************************************************/  
	PROCEDURE gm_upd_other_vendor_PO_qty
		(p_ttp_details_id	IN 		t4052_ttp_detail.c4052_ttp_detail_id%TYPE,
		 p_user_id 		    IN 		t4052_ttp_detail.c4052_last_updated_by%TYPE
	)
	AS
	
	CURSOR ttp_other_ven_po_qty_cur
	IS

		SELECT c205_part_number_id part,
			   c4057_raise_po_qty po_qty,
			   c301_second_vendor_id sec_ven_id,
			   c301_vendor_id ven_id
		  FROM T4057_TTP_VENDOR_CAPACITY_SUMMARY_DTLS 
		 WHERE c4052_ttp_detail_id = p_ttp_details_id 
		   AND c4057_void_fl is null;
		 
	BEGIN

		FOR v_other_po_qty IN ttp_other_ven_po_qty_cur
		LOOP
			
			IF NVL(v_other_po_qty.sec_ven_id,0) = 0 THEN
			
			 	UPDATE T4057_TTP_VENDOR_CAPACITY_SUMMARY_DTLS
                   SET c4057_second_vendor_raise_po_qty = 0
                     , c4057_last_updated_by = p_user_id
                     , c4057_last_updated_date = CURRENT_DATE
                 WHERE c205_part_number_id = v_other_po_qty.part
               	   AND c301_vendor_id = v_other_po_qty.ven_id
                   AND c4052_ttp_detail_id = p_ttp_details_id
                   AND c4057_void_fl is null;
      
      		ELSE
				UPDATE T4057_TTP_VENDOR_CAPACITY_SUMMARY_DTLS
			       SET c4057_second_vendor_raise_po_qty = NVL(v_other_po_qty.po_qty,0)
			         , c4057_last_updated_by = p_user_id
				     , c4057_last_updated_date = CURRENT_DATE
		         WHERE c205_part_number_id = v_other_po_qty.part
			   	   AND c301_vendor_id = v_other_po_qty.sec_ven_id
			       AND c4052_ttp_detail_id = p_ttp_details_id
			       AND c4057_void_fl is null;
		   END IF;
		   
		END LOOP;

	END  gm_upd_other_vendor_PO_qty;	
	
	/*************************************************************************
    * Description : Procedure to used to update order cost to t4057
    * Author   : 1
    *************************************************************************/  
	PROCEDURE gm_upd_order_cost
		(p_ttp_details_id	IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE
	)
	AS

    type pnum_array IS     TABLE OF  T205_PART_NUMBER.c205_part_number_id%TYPE;
    
    arr_pnum pnum_array;
    
	CURSOR ttp_ord_qty_cur
	IS

		SELECT c205_part_number_id 
 		  FROM st4055_ttp_summary st4055
		 WHERE c4052_ttp_detail_id = p_ttp_details_id;

		 
	BEGIN
	
		--
		OPEN ttp_ord_qty_cur;
		LOOP
		FETCH ttp_ord_qty_cur BULK COLLECT INTO arr_pnum LIMIT 500;
		
		FORALL i in 1.. arr_pnum.COUNT
			UPDATE T4057_TTP_VENDOR_CAPACITY_SUMMARY_DTLS
			SET C4057_ORDER_COST = round((C4057_RAISE_PO_QTY  * C402_VENDOR_PRICE),0)
			WHERE C4052_TTP_DETAIL_ID = p_ttp_details_id
			AND c205_part_number_id = arr_pnum(i)
			AND c4057_void_fl IS NULL;		
		EXIT WHEN ttp_ord_qty_cur%NOTFOUND;
	    END LOOP;
	    CLOSE ttp_ord_qty_cur;
	END  gm_upd_order_cost;

	/*************************************************************************
    * Description : Procedure to used to update master data required details based on ttp id.
    * Author   : 2
    *************************************************************************/   
	procedure gm_upd_required_and_repln_qty_inventory_table (
		p_ttp_details_id	IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE,
		p_user_id IN t4052_ttp_detail.c4052_last_updated_by%TYPE
	)
	AS
		--
		type pnum_array IS     TABLE OF  T205_PART_NUMBER.c205_part_number_id%TYPE;
		type total_req_qty_array IS    TABLE OF  st4055_ttp_summary.c4055_total_order_qty%TYPE ;
		type month_array IS    TABLE OF  temp_sales_repl_load.c4042_sales_replish_qty_01%TYPE ;
		--
		arr_pnum pnum_array;
		arr_total_req_qty total_req_qty_array;
		--
		arr_m1 month_array;
		arr_m2 month_array;
		arr_m3 month_array;
		arr_m4 month_array;
		arr_m5 month_array;
		arr_m6 month_array;
		arr_m7 month_array;
		arr_m8 month_array;
		arr_m9 month_array;
		arr_m10 month_array;
		arr_m11 month_array;
		arr_m12 month_array;
		--
		arr_safety_stock_m1 month_array;
		arr_safety_stock_m2 month_array;
		arr_safety_stock_m3 month_array;
		arr_safety_stock_m4 month_array;
		arr_safety_stock_m5 month_array;
		arr_safety_stock_m6 month_array;
		--
	CURSOR sales_repl_qty_cur
	IS

		SELECT c205_part_number_id pnum, c4055_total_required_qty order_qty,
			   c4042_sales_replish_qty_01,c4042_sales_replish_qty_02,c4042_sales_replish_qty_03,
			   c4042_sales_replish_qty_04,c4042_sales_replish_qty_05,c4042_sales_replish_qty_06,
			   c4042_sales_replish_qty_07,c4042_sales_replish_qty_08,c4042_sales_replish_qty_09,
			   c4042_sales_replish_qty_10,c4042_sales_replish_qty_11,c4042_sales_replish_qty_12,
			   c4042_safety_stock_qty_01, c4042_safety_stock_qty_02, c4042_safety_stock_qty_03,
			   c4042_safety_stock_qty_04,c4042_safety_stock_qty_05,c4042_safety_stock_qty_06
  
          FROM temp_sales_repl_load tmp_sales
         WHERE c4052_ttp_detail_id = p_ttp_details_id;
        -- AND c205_part_number_id = '193.001';

	BEGIN
		-- bulk update
	
		OPEN sales_repl_qty_cur;
		LOOP
		FETCH sales_repl_qty_cur BULK COLLECT INTO arr_pnum,arr_total_req_qty,arr_m1,arr_m2,arr_m3,arr_m4, arr_m5, arr_m6 
		,arr_m7, arr_m8, arr_m9, arr_m10, arr_m11, arr_m12
		,arr_safety_stock_m1, arr_safety_stock_m2, arr_safety_stock_m3, arr_safety_stock_m4, arr_safety_stock_m5, arr_safety_stock_m6 LIMIT 500;

		FORALL i in 1.. arr_pnum.COUNT
			UPDATE t4062_ttp_capa_forecast_inventory_dtls 
			SET
				c4062_total_required_qty = nvl(arr_total_req_qty(i),0),
				c4062_sales_replish_01 =   nvl(arr_m1 (i),0),
				c4062_sales_replish_02 = nvl(arr_m2 (i),0),
				c4062_sales_replish_03 = nvl(arr_m3 (i),0),
				c4062_sales_replish_04 = nvl(arr_m4 (i),0),
				c4062_sales_replish_05 = nvl(arr_m5 (i),0),
				c4062_sales_replish_06 = nvl(arr_m6 (i),0),
				c4062_sales_replish_07 = nvl(arr_m7 (i),0),
				c4062_sales_replish_08 = nvl(arr_m8 (i),0),
				c4062_sales_replish_09 = nvl(arr_m9 (i),0),
				c4062_sales_replish_10 = nvl(arr_m10 (i),0),
				c4062_sales_replish_11 = nvl(arr_m11 (i),0),
				c4062_sales_replish_12 = nvl(arr_m12 (i),0),
				c4062_safety_stock_01 = NVL(arr_safety_stock_m1 (i),0),
				c4062_safety_stock_02 = NVL(arr_safety_stock_m2 (i),0),
				c4062_safety_stock_03 = NVL(arr_safety_stock_m3 (i),0),
				c4062_safety_stock_04 = NVL(arr_safety_stock_m4 (i),0),
				c4062_safety_stock_05 = NVL(arr_safety_stock_m5 (i),0),
				c4062_safety_stock_06 = NVL(arr_safety_stock_m6 (i),0),
				c4062_last_updated_by = p_user_id,
				c4062_last_updated_date = CURRENT_DATE
			WHERE c4052_ttp_detail_id = p_ttp_details_id
			AND c205_part_number_id = arr_pnum(i)
			AND c4062_void_fl IS NULL;
			
		EXIT WHEN sales_repl_qty_cur%NOTFOUND;
	    END LOOP;
	    CLOSE sales_repl_qty_cur;
		    
	END gm_upd_required_and_repln_qty_inventory_table;

	/*************************************************************************
    * Description : Procedure to used to update master data required details based on ttp id.
    * Author   : 3
    *************************************************************************/   
	procedure gm_calc_forecast_by_part (
		p_ttp_details_id	IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE,
		p_load_date IN DATE,
		p_user_id IN t4052_ttp_detail.c4052_last_updated_by%TYPE
	)
	AS
		--
		type ttp_by_vendor_sum_id_array IS    TABLE OF  t4057_ttp_vendor_capacity_summary_dtls.c4057_ttp_vendor_capa_dtls_id%TYPE ;--column name change
		type back_order_array IS    TABLE OF  t4062_ttp_capa_forecast_inventory_dtls.c4062_back_order_qty%TYPE ;
		type set_priority_array IS    TABLE OF  t4062_ttp_capa_forecast_inventory_dtls.c4062_set_priority_qty_01%TYPE ;
		--
		type total_req_qty_array IS    TABLE OF  st4055_ttp_summary.c4055_total_order_qty%TYPE ;
		type month_array IS    TABLE OF  temp_sales_repl_load.c4042_sales_replish_qty_01%TYPE ;
		--
    	arr_ttp_sum_id ttp_by_vendor_sum_id_array;
		arr_calc_back_ord back_order_array;
		--
		arr_prev_sp_qty set_priority_array;
		arr_sp_m1 set_priority_array;
		arr_sp_m2 set_priority_array;
		arr_sp_m3 set_priority_array;
		arr_sp_m4 set_priority_array;
		arr_sp_m5 set_priority_array;
		arr_sp_m6 set_priority_array;
		--
    	arr_total_req_qty total_req_qty_array;
		--
		arr_m1 month_array;
		arr_m2 month_array;
		arr_m3 month_array;
		arr_m4 month_array;
		arr_m5 month_array;
		arr_m6 month_array;
		arr_m7 month_array;
		arr_m8 month_array;
		arr_m9 month_array;
		arr_m10 month_array;
		arr_m11 month_array;
		arr_m12 month_array;
		--
		arr_net_on_hand_qty total_req_qty_array;
		-- calc total req
		arr_total_req_01_qty month_array;
		arr_cal_net_on_hand_01_qty total_req_qty_array;

	CURSOR manual_calc_forecast_by_part_cur
	IS
		
		
			SELECT t4057.c4057_ttp_vendor_capa_dtls_id, 
			   round(t4062.c4062_back_order_qty * nvl(t4057.c4057_override_split_per, t4057.c4057_history_split_per) /100, 0),
			   round(t4062.c4062_set_priority_pre_mon_qty * nvl(t4057.c4057_override_split_per, c4057_history_split_per) /100, 0),
			   round(t4062.c4062_set_priority_qty_01 * nvl(t4057.c4057_override_split_per, t4057.c4057_history_split_per) /100, 0),
			   round(t4062.c4062_set_priority_qty_02 * nvl(t4057.c4057_override_split_per, t4057.c4057_history_split_per) /100, 0),
			   round(t4062.c4062_set_priority_qty_03 * nvl(t4057.c4057_override_split_per, t4057.c4057_history_split_per) /100, 0),
			   round(t4062.c4062_set_priority_qty_04 * nvl(t4057.c4057_override_split_per, t4057.c4057_history_split_per) /100, 0),
			   round(t4062.c4062_set_priority_qty_05 * nvl(t4057.c4057_override_split_per, t4057.c4057_history_split_per) /100, 0),
			   round(t4062.c4062_set_priority_qty_06 * nvl(t4057.c4057_override_split_per, t4057.c4057_history_split_per) /100, 0),
			   round(t4062.c4062_total_required_qty * nvl(t4057.c4057_override_split_per, t4057.c4057_history_split_per) /100, 0), 
			   round(NVL(t4062.c4062_net_on_hand, 0) * nvl(t4057.c4057_override_split_per, t4057.c4057_history_split_per) /100, 0),
			   round(t4062.c4062_sales_replish_01 * nvl(t4057.c4057_override_split_per, t4057.c4057_history_split_per) /100, 0),
			   round(t4062.c4062_sales_replish_02 * nvl(t4057.c4057_override_split_per, t4057.c4057_history_split_per) /100, 0),
			   round(t4062.c4062_sales_replish_03 * nvl(t4057.c4057_override_split_per, t4057.c4057_history_split_per) /100, 0),
			   round(t4062.c4062_sales_replish_04 * nvl(t4057.c4057_override_split_per, t4057.c4057_history_split_per) /100, 0),
			   round(t4062.c4062_sales_replish_05 * nvl(t4057.c4057_override_split_per, t4057.c4057_history_split_per) /100, 0),
			   round(t4062.c4062_sales_replish_06 * nvl(t4057.c4057_override_split_per, t4057.c4057_history_split_per) /100, 0),
			   round(t4062.c4062_sales_replish_07 * nvl(t4057.c4057_override_split_per, t4057.c4057_history_split_per) /100, 0),
			   round(t4062.c4062_sales_replish_08 * nvl(t4057.c4057_override_split_per, t4057.c4057_history_split_per) /100, 0),
			   --
			   round(t4062.c4062_sales_replish_09 * nvl(t4057.c4057_override_split_per, t4057.c4057_history_split_per) /100, 0),
			   round(t4062.c4062_sales_replish_10 * nvl(t4057.c4057_override_split_per, t4057.c4057_history_split_per) /100, 0),
			   round(t4062.c4062_sales_replish_11 * nvl(t4057.c4057_override_split_per, t4057.c4057_history_split_per) /100, 0),
			   round(t4062.c4062_sales_replish_12 * nvl(t4057.c4057_override_split_per, t4057.c4057_history_split_per) /100, 0)
			   -- to update total req qty (hist split)
			   , round(t4062.c4062_total_required_qty * nvl(t4057.c4057_override_split_per, t4057.c4057_history_split_per) /100, 0)
			   , round(t4062.c4062_net_on_hand * nvl(t4057.c4057_override_split_per, t4057.c4057_history_split_per) /100, 0)
		  FROM t4057_ttp_vendor_capacity_summary_dtls t4057, 
		  	   t4062_ttp_capa_forecast_inventory_dtls t4062
		 WHERE t4057.c4052_ttp_detail_id = t4062.c4052_ttp_detail_id 
		    AND t4057.c205_part_number_id = t4062.c205_part_number_id
		    AND t4057.c4052_ttp_detail_id = p_ttp_details_id
		    AND t4057.c4057_void_fl is null
		    AND t4062.c4062_void_fl is null;
	BEGIN

	-- bulk update
	
		OPEN manual_calc_forecast_by_part_cur;
		LOOP
		FETCH manual_calc_forecast_by_part_cur BULK COLLECT INTO arr_ttp_sum_id,arr_calc_back_ord,arr_prev_sp_qty, arr_sp_m1, arr_sp_m2
		, arr_sp_m3, arr_sp_m4, arr_sp_m5, arr_sp_m6
		, arr_total_req_qty ,arr_net_on_hand_qty, arr_m1,arr_m2,arr_m3,arr_m4,arr_m5,arr_m6,arr_m7,arr_m8,arr_m9,arr_m10,arr_m11,arr_m12
		, arr_total_req_01_qty, arr_cal_net_on_hand_01_qty LIMIT 500;
		
		FORALL i in 1.. arr_ttp_sum_id.COUNT
			UPDATE T4063_TTP_FORECAST_BY_PART_DTLS 
			SET
			C4063_CAL_BACK_ORDER_QTY = nvl(arr_calc_back_ord (i),0),
			C4063_CAL_SET_PRIORITY_PRE_MON_QTY = nvl(arr_prev_sp_qty (i),0),
			C4063_CAL_SET_PRIORITY_01 = nvl(arr_sp_m1 (i),0),
			C4063_CAL_SET_PRIORITY_02 = nvl(arr_sp_m2 (i),0),
			C4063_CAL_SET_PRIORITY_03 = nvl(arr_sp_m3 (i),0),
			C4063_CAL_SET_PRIORITY_04 = nvl(arr_sp_m4 (i),0),
			C4063_CAL_SET_PRIORITY_05 = nvl(arr_sp_m5 (i),0),
			C4063_CAL_SET_PRIORITY_06 = nvl(arr_sp_m6 (i),0),
			c4063_total_required_qty = nvl(arr_total_req_qty(i),0),
			--
			c4063_net_on_hand = arr_net_on_hand_qty (i),
			c4063_sales_replish_01 = nvl(arr_m1 (i),0),
			c4063_sales_replish_02 = nvl(arr_m2 (i),0),
			c4063_sales_replish_03 = nvl(arr_m3 (i),0),
			c4063_sales_replish_04 = nvl(arr_m4 (i),0),
			c4063_sales_replish_05 = nvl(arr_m5 (i),0),
			c4063_sales_replish_06 = nvl(arr_m6 (i),0),
			c4063_sales_replish_07 = nvl(arr_m7 (i),0),
			c4063_sales_replish_08 = nvl(arr_m8 (i),0),
			--
			c4063_cal_total_required_01 = nvl(arr_total_req_01_qty (i),0),
			c4063_cal_net_on_hand_01 = NVL(arr_cal_net_on_hand_01_qty (i),0),
			--
			c4063_last_updated_by = p_user_id,
			c4063_last_updated_date = CURRENT_DATE
			WHERE c4057_ttp_by_vendor_summary_id = arr_ttp_sum_id (i)
			AND c4063_void_fl IS NULL;
			
		EXIT WHEN manual_calc_forecast_by_part_cur%NOTFOUND;
	    END LOOP;
	    CLOSE manual_calc_forecast_by_part_cur;
	    
	    -- 3.1)To calculate safty stock	    
	    gm_calc_safety_stock (p_ttp_details_id); 
	    
	    --3.2)To update the total (C4063_CAL_INV_TOTAL_01 - C4063_CAL_INV_TOTAL_06)	
	    gm_calc_inv_total (p_ttp_details_id); 
	    --update (Total Required * Hist Split) - (BOs + Sales Repl + Safety Stock + set priority previous month + set priority current month)
	    
	    --3.3)To calclate other needs basics flow
	    gm_calc_others_needs (p_ttp_details_id);
	    

	    --3.4)To update overall total	    
	    gm_upd_overall_total (p_ttp_details_id);
	    
	    --3.4.1) to update TTP chart formula
	    -- PMT-55992-forecast-chart-value-change (05/20/2020)			
		gm_pkg_oppr_ld_ttp_by_vendor_chart.gm_upd_main_forecast_chart_dtls (p_ttp_details_id, p_user_id);
	    
	    --3.5)To update chart json data update (Prabhu to form the JSON string and update )	    
	    gm_upd_chart_json_data_by_part (p_ttp_details_id, p_load_date);
	    
	END gm_calc_forecast_by_part;

	
	/*************************************************************************
    * Description : Procedure to used to calculate safety stock
    * Author   : 3
    *************************************************************************/   
	PROCEDURE gm_calc_safety_stock(
		p_ttp_details_id	IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE
	)
	AS
		
		BEGIN
		
			-- to update the safety stock details
		
				    UPDATE t4063_ttp_forecast_by_part_dtls 
			 		   SET c4063_cal_safety_stock_01 = (c4063_sales_replish_02 + c4063_sales_replish_03) - c4063_cal_net_on_hand_01,
			 		   	   c4063_cal_safety_stock_02 = (c4063_sales_replish_04 - c4063_sales_replish_02) ,
			 		       c4063_cal_safety_stock_03 = (c4063_sales_replish_05 - c4063_sales_replish_03) , 
			 		       c4063_cal_safety_stock_04 = (c4063_sales_replish_06 - c4063_sales_replish_04) ,
			 		       c4063_cal_safety_stock_05 = (c4063_sales_replish_07 - c4063_sales_replish_05) ,
			 		       c4063_cal_safety_stock_06 = (c4063_sales_replish_08 - c4063_sales_replish_06)
			 		 WHERE c4052_ttp_detail_id = p_ttp_details_id
			 		 and c4063_void_fl IS NULL;

			 	 -- to handle the negative values - stock 1
			 		 
			 		 UPDATE t4063_ttp_forecast_by_part_dtls 
	  			   SET c4063_cal_safety_stock_01 = 0
			 WHERE c4052_ttp_detail_id    = p_ttp_details_id
			    AND c4063_cal_safety_stock_01 < 0
			    AND c4063_void_fl         IS NULL;
			    
			    /*
			    -- to handle the negative values - stock 2
			 		 
			 		 UPDATE t4063_ttp_forecast_by_part_dtls 
	  			   SET c4063_cal_safety_stock_02 = 0
			 WHERE c4052_ttp_detail_id    = p_ttp_details_id
			    AND c4063_cal_safety_stock_02 < 0
			    AND c4063_void_fl         IS NULL;

			    -- to handle the negative values - stock 3
			 		 
			 		 UPDATE t4063_ttp_forecast_by_part_dtls 
	  			   SET c4063_cal_safety_stock_03 = 0
			 WHERE c4052_ttp_detail_id    = p_ttp_details_id
			    AND c4063_cal_safety_stock_03 < 0
			    AND c4063_void_fl         IS NULL;

			    -- to handle the negative values - stock 4
			 		 
			 		 UPDATE t4063_ttp_forecast_by_part_dtls 
	  			   SET c4063_cal_safety_stock_04 = 0
			 WHERE c4052_ttp_detail_id    = p_ttp_details_id
			    AND c4063_cal_safety_stock_04 < 0
			    AND c4063_void_fl         IS NULL;

			    -- to handle the negative values - stock 5
			 		 
			 		 UPDATE t4063_ttp_forecast_by_part_dtls 
	  			   SET c4063_cal_safety_stock_05 = 0
			 WHERE c4052_ttp_detail_id    = p_ttp_details_id
			    AND c4063_cal_safety_stock_05 < 0
			    AND c4063_void_fl         IS NULL;

			    -- to handle the negative values - stock 6
			 		 
			 		 UPDATE t4063_ttp_forecast_by_part_dtls 
	  			   SET c4063_cal_safety_stock_06 = 0
			 WHERE c4052_ttp_detail_id    = p_ttp_details_id
			    AND c4063_cal_safety_stock_06 < 0
			    AND c4063_void_fl         IS NULL;
			    
			    */
	    
	END gm_calc_safety_stock;

	/*************************************************************************
    * Description : Procedure to used to calculate inventory total
    * Author   : 3
    *************************************************************************/ 
	PROCEDURE gm_calc_inv_total(
		p_ttp_details_id	IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE
	)
	AS
	--
		type pnum_array IS     TABLE OF  T205_PART_NUMBER.c205_part_number_id%TYPE;
		type pk_array IS     TABLE OF  t4063_ttp_forecast_by_part_dtls.c4063_ttp_forecast_by_part_dtls_id%TYPE;
		--
		arr_pnum pnum_array;
		arr_pk pk_array;
		--
		CURSOR calc_inv_total_cur
		IS
		       	 SELECT c4063_ttp_forecast_by_part_dtls_id
				   FROM t4063_ttp_forecast_by_part_dtls t4063
				  WHERE t4063.c4052_ttp_detail_id = p_ttp_details_id
				    AND t4063.c4063_void_fl      IS NULL;
		BEGIN	
			
			OPEN calc_inv_total_cur;
			LOOP
			FETCH calc_inv_total_cur BULK COLLECT INTO arr_pk LIMIT 500;
			FORALL i in 1.. arr_pk.COUNT
				UPDATE t4063_ttp_forecast_by_part_dtls
				   SET c4063_cal_inv_total_01 = NVL((c4063_cal_back_order_qty + c4063_cal_set_priority_pre_mon_qty
				   								+ c4063_cal_set_priority_01 + c4063_sales_replish_01 + c4063_cal_safety_stock_01 ), 0),
					   c4063_cal_inv_total_02 = NVL((c4063_cal_set_priority_02
					   							+ c4063_sales_replish_02 + c4063_cal_safety_stock_02 ), 0),
					   c4063_cal_inv_total_03 = NVL((c4063_cal_set_priority_03
					   							+ c4063_sales_replish_03 + c4063_cal_safety_stock_03 ), 0),
					   c4063_cal_inv_total_04 = NVL((c4063_cal_set_priority_04 
					   							+ c4063_sales_replish_04 + c4063_cal_safety_stock_04 ), 0),
				       c4063_cal_inv_total_05 = NVL((c4063_cal_set_priority_05
				       							+ c4063_sales_replish_05 + c4063_cal_safety_stock_05 ), 0),
					   c4063_cal_inv_total_06 = NVL((c4063_cal_set_priority_06 
					   							+ c4063_sales_replish_06 + c4063_cal_safety_stock_06 ), 0)
				  WHERE  c4063_ttp_forecast_by_part_dtls_id = arr_pk(i)
 	      			 AND c4063_void_fl IS NULL;
 	      			 	 
 	      	EXIT WHEN calc_inv_total_cur%NOTFOUND;
	        END LOOP;
	       	        
	        CLOSE calc_inv_total_cur;
	        
	        
	END gm_calc_inv_total;
	
	/*************************************************************************
    * Description : Procedure to used to calculate other needs
    * Author   : 3
    *************************************************************************/ 
	PROCEDURE gm_calc_others_needs(
		p_ttp_details_id	IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE
	)
	AS
	v_other_need_01 NUMBER;

	CURSOR overall_total_cur
	IS
	
	  SELECT t4063.c4063_ttp_forecast_by_part_dtls_id by_part_dtls_id,
			   t4063.c4063_cal_inv_total_01 inv_total_01,t4063.c4063_cal_inv_total_02 inv_total_02,
			   t4063.c4063_cal_inv_total_03 inv_total_03,
			   t4063.c4063_cal_inv_total_04 inv_total_04,
			   t4063.c4063_cal_inv_total_05 inv_total_05,
			   t4063.c4063_cal_inv_total_06 inv_total_06,
			   t4063.c4063_cal_total_required_01 total_oth_need_01,
			   t4063.c4063_cal_total_required_02 total_oth_need_02,
			   t4063.c4063_cal_total_required_03 total_oth_need_03,
			   t4063.c4063_cal_total_required_04 total_oth_need_04,
			   t4063.c4063_cal_total_required_05 total_oth_need_05,
			   t4063.c4063_cal_total_required_06 total_oth_need_06,
			   NVL(t4063.c4063_fcst_hist_rec_avg_01, 0) rece_avg_01,t4063.c4063_fcst_hist_rec_avg_02 rece_avg_02,
			   t4063.c4063_fcst_hist_rec_avg_03 rece_avg_03,t4063.c4063_fcst_hist_rec_avg_04 rece_avg_04,
			   t4063.c4063_fcst_hist_rec_avg_05 rece_avg_05,t4063.c4063_fcst_hist_rec_avg_06 rece_avg_06,
			   t4063.c4063_cal_inv_total_01 total_qty
			   -- new formula changes
			   , t4063.c4063_cal_total_required_01 cal_total_req
			   --
			   , t4063.c4063_cal_net_on_hand_01 net_on_hand
			   , C4063_CAL_BACK_ORDER_QTY bo_qty, C4063_CAL_SET_PRIORITY_PRE_MON_QTY sppm_qty
			   , C4063_CAL_SET_PRIORITY_01 spcm_qty
			   , C4063_SALES_REPLISH_01 sales_rep_curr_qty
			   , (C4063_SALES_REPLISH_02 + C4063_SALES_REPLISH_03) sales_rep_next_2mon_qty
			   -- PMT-51948 : TTP forecast formula changes (03/20/2020)
			   -- to get the sales rep (next 3 month qty)
			   , NVL((C4063_SALES_REPLISH_02 + C4063_SALES_REPLISH_03 + C4063_SALES_REPLISH_04), 0) sales_rep_next_3mon_qty
			   
		  from t4063_ttp_forecast_by_part_dtls t4063
 		 WHERE t4063.c4052_ttp_detail_id = p_ttp_details_id-- 26169
 		   AND t4063.c4063_void_fl is null;
	
	BEGIN
	
		FOR overall IN overall_total_cur
		LOOP
			--
			v_other_need_01 := 0;
			-- current month
			IF  overall.cal_total_req > overall.total_qty
			THEN
				-- PMT-51948 : TTP forecast formula changes (03/20/2020)
				-- To reduce next 3 month (sales Rep qty)
				v_other_need_01 := ROUND(NVL(overall.cal_total_req - overall.total_qty, 0) - overall.sales_rep_next_3mon_qty, 0);
				-- Compare the values and update to 0 values (if neg)
				IF v_other_need_01 < 0
				THEN
					v_other_need_01 := 0;
				END IF;
				--
				UPDATE T4063_TTP_FORECAST_BY_PART_DTLS 
	  			   SET C4063_CAL_OTHER_NEED_01 = v_other_need_01
				 WHERE C4063_TTP_FORECAST_BY_PART_DTLS_ID = overall.by_part_dtls_id
				   AND c4063_void_fl IS NULL;
			ELSE
				UPDATE T4063_TTP_FORECAST_BY_PART_DTLS 
	               SET C4063_CAL_OTHER_NEED_01 = v_other_need_01
				 WHERE C4063_TTP_FORECAST_BY_PART_DTLS_ID = overall.by_part_dtls_id
				   AND c4063_void_fl IS NULL;	
			END IF;   
			
			-- to find the total requirement values (current month)
			gm_pkg_oppr_ld_ttp_capa_master_txn.gm_upd_first_month_overall_total (overall.by_part_dtls_id,
			overall.net_on_hand
			, overall.bo_qty, overall.sppm_qty, overall.spcm_qty, overall.sales_rep_curr_qty, overall.sales_rep_next_2mon_qty
			, overall.cal_total_req, overall.rece_avg_01, v_other_need_01, overall.total_qty);

			-- to update net on hand
			gm_upd_net_on_hand (overall.by_part_dtls_id);
			
			
		END LOOP;
		
			    
	END gm_calc_others_needs;
	
	/*************************************************************************
    * Description : Procedure to used to calculate other needs
    * Author   : 3
    *************************************************************************/ 
	PROCEDURE gm_upd_overall_total(
		p_ttp_details_id	IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE
	)
	AS
	
	BEGIN
		-- to update overall total
	
			update t4063_ttp_forecast_by_part_dtls
			   set C4063_CAL_OVERALL_TOTAL_02 = c4063_cal_total_required_02 - c4063_cal_net_on_hand_02, 
				   C4063_CAL_OVERALL_TOTAL_03 = c4063_cal_total_required_03 - c4063_cal_net_on_hand_03,
				   C4063_CAL_OVERALL_TOTAL_04 = c4063_cal_total_required_04 - c4063_cal_net_on_hand_04,
				   C4063_CAL_OVERALL_TOTAL_05 = c4063_cal_total_required_05 - c4063_cal_net_on_hand_05,
				   C4063_CAL_OVERALL_TOTAL_06 = c4063_cal_total_required_06 - c4063_cal_net_on_hand_06
			 WHERE  c4052_ttp_detail_id = p_ttp_details_id
 	      			 AND c4063_void_fl IS NULL;

	    	-- to avoid the negative qty- c4063_cal_overall_total_01
	    				
			UPDATE t4063_ttp_forecast_by_part_dtls 
	  			   SET c4063_cal_overall_total_01 = 0
			 WHERE c4052_ttp_detail_id    = p_ttp_details_id
			    AND c4063_cal_overall_total_01 < 0
			    AND c4063_void_fl         IS NULL;
			    
	    -- to avoid the negative qty- c4063_cal_overall_total_01
	    				
			UPDATE t4063_ttp_forecast_by_part_dtls 
	  			   SET c4063_cal_overall_total_02 = 0
			 WHERE c4052_ttp_detail_id    = p_ttp_details_id
			    AND c4063_cal_overall_total_02 < 0
			    AND c4063_void_fl         IS NULL;
			    
		-- to avoid the negative qty- c4063_cal_overall_total_01
	    				
			UPDATE t4063_ttp_forecast_by_part_dtls 
	  			   SET c4063_cal_overall_total_03 = 0
			 WHERE c4052_ttp_detail_id    = p_ttp_details_id
			    AND c4063_cal_overall_total_03 < 0
			    AND c4063_void_fl         IS NULL;	    

		-- to avoid the negative qty- c4063_cal_overall_total_01
	    				
			UPDATE t4063_ttp_forecast_by_part_dtls 
	  			   SET c4063_cal_overall_total_04 = 0
			 WHERE c4052_ttp_detail_id    = p_ttp_details_id
			    AND c4063_cal_overall_total_04 < 0
			    AND c4063_void_fl         IS NULL;
			    
		-- to avoid the negative qty- c4063_cal_overall_total_01
	    				
			UPDATE t4063_ttp_forecast_by_part_dtls 
	  			   SET c4063_cal_overall_total_05 = 0
			 WHERE c4052_ttp_detail_id    = p_ttp_details_id
			    AND c4063_cal_overall_total_05 < 0
			    AND c4063_void_fl         IS NULL;
		-- to avoid the negative qty- c4063_cal_overall_total_01
	    				
			UPDATE t4063_ttp_forecast_by_part_dtls 
	  			   SET c4063_cal_overall_total_06 = 0
			 WHERE c4052_ttp_detail_id    = p_ttp_details_id
			    AND c4063_cal_overall_total_06 < 0
			    AND c4063_void_fl         IS NULL;
			    
	END gm_upd_overall_total;

	/*************************************************************************
    * Description : Procedure to used to update JSON data
    * Author   : 
    *************************************************************************/ 	
	PROCEDURE gm_upd_chart_json_data_by_part(
	p_ttp_details_id	IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE,
	p_load_date IN DATE
	)
	AS

	--
	CURSOR forecast_by_part_cur
	IS
	SELECT t4063.C4063_TTP_FORECAST_BY_PART_DTLS_ID ttp_forecast_id
   FROM T4063_TTP_FORECAST_BY_PART_DTLS t4063
  WHERE C4052_TTP_DETAIL_ID = p_ttp_details_id
    AND C4063_VOID_FL      IS NULL
    order by C4063_TTP_FORECAST_BY_PART_DTLS_ID;
	--
	BEGIN
	--
	dbms_output.put_line (' ^^^^^^ ###### Started '|| p_ttp_details_id)	;
	FOR forecast_by_po IN 	forecast_by_part_cur
	LOOP
	  
      gm_upd_chart_json_data_by_po(forecast_by_po.ttp_forecast_id, p_load_date);
      
      gm_upd_chart_json_data_by_receipt(forecast_by_po.ttp_forecast_id, p_load_date);
    
    END LOOP;
    --
      
    END gm_upd_chart_json_data_by_part;  
    
   	/*************************************************************************
    * Description : Procedure to used to update JSON data by PO
    * Author   : 
    *************************************************************************/ 	
	PROCEDURE gm_upd_chart_json_data_by_po(
	p_ttp_forecast_by_part_dtls_id	IN t4063_ttp_forecast_by_part_dtls.c4063_ttp_forecast_by_part_dtls_id%TYPE,
	p_load_date IN DATE
	) 
	
	AS
	v_po_chart CLOB;
    
	BEGIN

	--
		SELECT LISTAGG(JSON_DATA,'') WITHIN GROUP (
			ORDER BY JSON_DATA) INTO v_po_chart
		FROM
 		 (
  SELECT '[{category:['
    || REPLACE(Json_Object( 'label' Value TO_CHAR(Add_Months(p_load_date,-6), 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,-5), 'Mon')
    	, 'label' Value TO_CHAR(Add_Months(p_load_date,-4), 'Mon'), 'label' VALUE TO_CHAR(Add_Months(p_load_date,-3), 'Mon')
    	, 'label' Value TO_CHAR(Add_Months(p_load_date,-2), 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,-1), 'Mon')
    	, 'label' Value TO_CHAR(p_load_date, 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,+1), 'Mon')
    	, 'label' Value TO_CHAR(Add_Months(p_load_date,+2), 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,+3), 'Mon')
    	, 'label' Value TO_CHAR(Add_Months(p_load_date,+4), 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,+5), 'Mon')),',','},{')
    ||']}],' JSON_DATA
  FROM Dual
  UNION ALL
  -- BOs
  SELECT 'dataset:[{seriesname:"BOs",valueBgColor:"#008080",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0'
    	, 'value' Value TO_CHAR(c4063_cal_chart_back_order_qty), 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0'
    	, 'value' Value '0'),',','},{')
    ||']},'
    ||
    --Sales Replenishment
    '{seriesname:"Sales Replenishment",valueBgColor:"#0000FF",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0'
    	, 'value' Value c4063_cal_chart_sales_replish_01, 'value' Value c4063_cal_chart_sales_replish_02, 'value' Value c4063_cal_chart_sales_replish_03
    	, 'value' Value c4063_cal_chart_sales_replish_04, 'value' Value c4063_cal_chart_sales_replish_05, 'value' Value c4063_cal_chart_sales_replish_06),',','},{')
    ||']},'
    ||
    --Safety Stock
    '{seriesname:"Safety Stock",valueBgColor:"#8A2BE2",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0'
    , 'value' Value c4063_cal_chart_safety_stock_01, 'value' Value c4063_cal_chart_safety_stock_02, 'value' Value c4063_cal_chart_safety_stock_03
    , 'value' Value c4063_cal_chart_safety_stock_04, 'value' Value c4063_cal_chart_safety_stock_05, 'value' Value c4063_cal_chart_safety_stock_06),',','},{')
    ||']},'
    ||
    --Priority Set Previous Month
    '{seriesname:"Priority Set Previous Month",valueBgColor:"#FFFF00",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0'
    	, 'value' Value c4063_cal_chart_set_priority_pre_mon_qty, 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0'
    	, 'value' Value '0'),',','},{')
    ||']},'
    || 
    --Priority Set Current Month
    '{seriesname:"Priority Set Current Month",valueBgColor:"#008000",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0'
    , 'value' Value c4063_cal_chart_set_priority_01, 'value' Value c4063_cal_chart_set_priority_02, 'value' Value c4063_cal_chart_set_priority_03
    , 'value' Value c4063_cal_chart_set_priority_04, 'value' Value c4063_cal_chart_set_priority_05, 'value' Value c4063_cal_chart_set_priority_06),',','},{')
    ||']},'
    ||
    --Split Other Needs
    '{seriesname:"Split Other Needs",valueBgColor:"#00BFFF",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0'
    , 'value' Value c4063_cal_chart_other_need_01, 'value' Value C4063_CAL_OTHER_NEED_02, 'value' Value C4063_CAL_OTHER_NEED_03
    , 'value' Value C4063_CAL_OTHER_NEED_04, 'value' Value C4063_Cal_Other_Need_05, 'value' Value C4063_Cal_Other_Need_06),',','},{')
    ||']},'
    ||
    --FCST Historical Receipt Avg
    '{seriesname:"FCST Historical Receipt Avg",renderas:"Line",valueBgColor:"#FF0000",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0'
    , 'value' Value '0', 'value' Value '0', 'value' Value C4063_FCST_HIST_REC_AVG_01
    , 'value' Value C4063_Fcst_Hist_Rec_Avg_02, 'value' Value C4063_Fcst_Hist_Rec_Avg_03, 'value' Value C4063_Fcst_Hist_Rec_Avg_04
    , 'value' Value C4063_Fcst_Hist_Rec_Avg_05, 'value' Value C4063_Fcst_Hist_Rec_Avg_06),',','},{')
    ||']},'
    ||
    --Actual PO Qty (Historical)
    '{seriesname:"Actual PO Qty (Historical)",valueBgColor:"#7CFC00",data:['
    ||REPLACE(Json_Object( 'value' Value C4063_CAL_HIST_PO_MONTH_01, 'value' Value C4063_CAL_HIST_PO_MONTH_02
    , 'value' Value C4063_CAL_HIST_PO_MONTH_03, 'value' Value C4063_CAL_HIST_PO_MONTH_04, 'value' Value C4063_CAL_HIST_PO_MONTH_05
    , 'value' Value C4063_CAL_HIST_PO_MONTH_06, 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0'
    , 'value' Value '0'),',','},{')
    ||']}]' Json_Data
  FROM T4063_TTP_FORECAST_BY_PART_DTLS
  WHERE c4063_ttp_forecast_by_part_dtls_id = p_ttp_forecast_by_part_dtls_id
  );
  
  UPDATE T4063_TTP_FORECAST_BY_PART_DTLS 
  SET C4063_CHART_PO_JSON_DATA = v_po_chart
  WHERE c4063_ttp_forecast_by_part_dtls_id = p_ttp_forecast_by_part_dtls_id;
  --
  
--  
END gm_upd_chart_json_data_by_po;
--  
  	/*************************************************************************
    * Description : Procedure to used to update JSON data by PO
    * Author   : 
    *************************************************************************/ 	
	PROCEDURE gm_upd_chart_json_data_by_receipt(
	p_ttp_forecast_by_part_dtls_id	IN t4063_ttp_forecast_by_part_dtls.c4063_ttp_forecast_by_part_dtls_id%TYPE,
	p_load_date IN DATE
	)
	
	AS
	
    --
	v_receipt_chart CLOB;
	--
	BEGIN
	--	
  SELECT LISTAGG(JSON_DATA,'') WITHIN GROUP (
ORDER BY JSON_DATA) INTO v_receipt_chart
FROM
  (
  -- x axis Replace current_date with load date
  SELECT '[{category:['
    || REPLACE(Json_Object( 'label' Value TO_CHAR(Add_Months(p_load_date,-6), 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,-5), 'Mon')
    , 'label' Value TO_CHAR(Add_Months(p_load_date,-4), 'Mon'), 'label' VALUE TO_CHAR(Add_Months(p_load_date,-3), 'Mon')
    , 'label' Value TO_CHAR(Add_Months(p_load_date,-2), 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,-1), 'Mon')
    , 'label' Value TO_CHAR(p_load_date, 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,+1), 'Mon')
    , 'label' Value TO_CHAR(Add_Months(p_load_date,+2), 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,+3), 'Mon')
    , 'label' Value TO_CHAR(Add_Months(p_load_date,+4), 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,+5), 'Mon')),',','},{')
    ||']}],' JSON_DATA
  FROM Dual
  UNION ALL
  -- BOs
  SELECT 'dataset:[{seriesname:"BOs",valueBgColor:"#008080",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0'
    , 'value' Value TO_CHAR(c4063_cal_chart_back_order_qty), 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0'
    , 'value' Value '0'),',','},{')
    ||']},'
    ||
    --Sales Replenishment
    '{seriesname:"Sales Replenishment",valueBgColor:"#0000FF",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0'
    , 'value' Value '0', 'value' Value c4063_cal_chart_sales_replish_01, 'value' Value c4063_cal_chart_sales_replish_02
    , 'value' Value c4063_cal_chart_sales_replish_03, 'value' Value c4063_cal_chart_sales_replish_04, 'value' Value c4063_cal_chart_sales_replish_05
    , 'value' Value c4063_cal_chart_sales_replish_06),',','},{')
    ||']},'
    ||
    --Safety Stock
    '{seriesname:"Safety Stock",valueBgColor:"#8A2BE2",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0'
    , 'value' Value '0', 'value' Value c4063_cal_chart_safety_stock_01, 'value' Value c4063_cal_chart_safety_stock_02
    , 'value' Value c4063_cal_chart_safety_stock_03, 'value' Value c4063_cal_chart_safety_stock_04, 'value' Value c4063_cal_chart_safety_stock_05
    , 'value' Value c4063_cal_chart_safety_stock_06),',','},{')
    ||']},'
    ||
    --Priority Set Previous Month
    '{seriesname:"Priority Set Previous Month",valueBgColor:"#FFFF00",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0'
    , 'value' Value '0', 'value' Value 	c4063_cal_chart_set_priority_pre_mon_qty, 'value' Value '0', 'value' Value '0'
    , 'value' Value '0', 'value' Value '0', 'value' Value '0'),',','},{')
    ||']},'
    ||   
    --Priority Set Current Month
    '{seriesname:"Priority Set Current Month",valueBgColor:"#008000",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0'
    , 'value' Value '0', 'value' Value 	c4063_cal_chart_set_priority_01, 'value' Value c4063_cal_chart_set_priority_02
    , 'value' Value c4063_cal_chart_set_priority_03, 'value' Value c4063_cal_chart_set_priority_04, 'value' Value c4063_cal_chart_set_priority_05
    , 'value' Value c4063_cal_chart_set_priority_06),',','},{')
    ||']},'
    ||
    --Split Other Needs
    '{seriesname:"Split Other Needs",valueBgColor:"#00BFFF",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0'
    , 'value' Value '0', 'value' Value c4063_cal_chart_other_need_01, 'value' Value C4063_CAL_OTHER_NEED_02
    , 'value' Value C4063_CAL_OTHER_NEED_03, 'value' Value C4063_CAL_OTHER_NEED_04, 'value' Value C4063_Cal_Other_Need_05
    , 'value' Value C4063_Cal_Other_Need_06),',','},{')
    ||']},'
    ||
    --FCST Historical Receipt Avg
    '{seriesname:"FCST Historical Receipt Avg",renderas:"Line",valueBgColor:"#FF0000",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0'
    , 'value' Value '0', 'value' Value '0', 'value' Value C4063_FCST_HIST_REC_AVG_01
    , 'value' Value C4063_Fcst_Hist_Rec_Avg_02, 'value' Value C4063_Fcst_Hist_Rec_Avg_03, 'value' Value C4063_Fcst_Hist_Rec_Avg_04
    , 'value' Value C4063_Fcst_Hist_Rec_Avg_05, 'value' Value C4063_Fcst_Hist_Rec_Avg_06),',','},{')
    ||']},'
    ||
    --Actual Receipt (Historical)
    '{seriesname:" Actual Receipt (Historical)",valueBgColor:"#7CFC00",data:['
    ||REPLACE(Json_Object( 'value' Value C4063_CAL_HIST_RECEIPT_MONTH_01, 'value' Value C4063_CAL_HIST_RECEIPT_MONTH_02
    , 'value' Value C4063_CAL_HIST_RECEIPT_MONTH_03, 'value' Value C4063_CAL_HIST_RECEIPT_MONTH_04, 'value' Value C4063_CAL_HIST_RECEIPT_MONTH_05
    , 'value' Value C4063_CAL_HIST_RECEIPT_MONTH_06, 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0'
    , 'value' Value '0', 'value' Value '0'),',','},{')
    ||']}]' Json_Data
  FROM T4063_TTP_FORECAST_BY_PART_DTLS
  WHERE c4063_ttp_forecast_by_part_dtls_id = p_ttp_forecast_by_part_dtls_id
  );
 --
    UPDATE  t4063_ttp_forecast_by_part_dtls 
  		SET c4063_chart_receipt_json_data = v_receipt_chart
  	WHERE c4063_ttp_forecast_by_part_dtls_id = p_ttp_forecast_by_part_dtls_id;
	 
END  gm_upd_chart_json_data_by_receipt;
	
	
	

	/*************************************************************************
    * Description : Procedure to used to update master data required details based on ttp id.
    * Author   : 4
    *************************************************************************/ 
	procedure gm_calc_forecast_by_vendor (
		p_vendor_id	IN t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_id%TYPE,
		p_load_date IN DATE,
		p_user_id IN t4052_ttp_detail.c4052_last_updated_by%TYPE
	)
	AS
	
	CURSOR forecast_by_vendor_cur
	IS
		SELECT t4057.C301_VENDOR_ID vendor_id, t4057.C4057_LOAD_DATE load_date,
			   sum (C4063_CAL_BACK_ORDER_QTY) back_ord_qty,
			   sum (C4063_CAL_SET_PRIORITY_PRE_MON_QTY) pre_mon_sp_qty,
			   sum (C4063_CAL_SET_PRIORITY_01) sp_1,
			   sum (C4063_CAL_SET_PRIORITY_02) sp_2,
			   sum (C4063_CAL_SET_PRIORITY_03) sp_3,
			   sum (C4063_CAL_SET_PRIORITY_04) sp_4,
			   sum (C4063_CAL_SET_PRIORITY_05) sp_5,
			   sum (C4063_CAL_SET_PRIORITY_06) sp_6,
			   sum (C4063_NET_ON_HAND) net_on_hand,
			   sum (C4063_TOTAL_REQUIRED_QTY) total_req,
			   sum (C4063_SALES_REPLISH_01) sales_repl_1,
			   sum (C4063_SALES_REPLISH_02) sales_repl_2,
			   sum (C4063_SALES_REPLISH_03) sales_repl_3,
			   sum (C4063_SALES_REPLISH_04) sales_repl_4,
			   sum (C4063_SALES_REPLISH_05) sales_repl_5,
			   sum (C4063_SALES_REPLISH_06) sales_repl_6,
			   sum (C4063_SALES_REPLISH_07) sales_repl_7,
			   sum (C4063_SALES_REPLISH_08) sales_repl_8,
			   sum (C4063_CAL_SAFETY_STOCK_01) safety_stock_1,
			   sum (C4063_CAL_SAFETY_STOCK_02) safety_stock_2,
			   sum (C4063_CAL_SAFETY_STOCK_03) safety_stock_3,
			   sum (C4063_CAL_SAFETY_STOCK_04) safety_stock_4,
			   sum (C4063_CAL_SAFETY_STOCK_05) safety_stock_5,
			   sum (C4063_CAL_SAFETY_STOCK_06) safety_stock_6,
			   sum (C4063_CAL_INV_TOTAL_01) inv_total_1,
			   sum (C4063_CAL_INV_TOTAL_02) inv_total_2,
			   sum (C4063_CAL_INV_TOTAL_03) inv_total_3,
			   sum (C4063_CAL_INV_TOTAL_04) inv_total_4,
			   sum (C4063_CAL_INV_TOTAL_05) inv_total_5,
			   sum (C4063_CAL_INV_TOTAL_06) inv_total_6,
			   sum (C4063_HISTORICAL_RECEIPT_AVG) his_rec_avg,
			   sum (C4063_FCST_HIST_REC_AVG_01) fsct_rec_1,
			   sum (C4063_FCST_HIST_REC_AVG_02) fsct_rec_2,
		  	   sum (C4063_FCST_HIST_REC_AVG_03) fsct_rec_3,
			   sum (C4063_FCST_HIST_REC_AVG_04) fsct_rec_4,
			   sum (C4063_FCST_HIST_REC_AVG_05) fsct_rec_5,
			   sum (C4063_FCST_HIST_REC_AVG_06) fsct_rec_6,
				--sum (C4063_OTHER_NEED_FLG
			   sum (C4063_CAL_OTHER_NEED_01) cal_other_need_1,
			   sum (C4063_CAL_OTHER_NEED_02) cal_other_need_2,
			   sum (C4063_CAL_OTHER_NEED_03) cal_other_need_3,
			   sum (C4063_CAL_OTHER_NEED_04) cal_other_need_4,
			   sum (C4063_CAL_OTHER_NEED_05) cal_other_need_5,
			   sum (C4063_CAL_OTHER_NEED_06) cal_other_need_6,
			   sum (C4063_PRE_HIST_REC_AVG_01) previous_hist_rec_1,
			   sum (C4063_PRE_HIST_REC_AVG_02) previous_hist_rec_2,
			   sum (C4063_PRE_HIST_REC_AVG_03) previous_hist_rec_3,
			   sum (C4063_CAL_OVERALL_TOTAL_01) cal_overall_total_1,
			   sum (C4063_CAL_OVERALL_TOTAL_02) cal_overall_total_2,
			   sum (C4063_CAL_OVERALL_TOTAL_03) cal_overall_total_3,
			   sum (C4063_CAL_OVERALL_TOTAL_04) cal_overall_total_4,
			   sum (C4063_CAL_OVERALL_TOTAL_05) cal_overall_total_5,
			   sum (C4063_CAL_OVERALL_TOTAL_06 ) cal_overall_total_6,
			   sum (c4063_cal_chart_other_need_01) chart_other_need_total,
			   -- PMT-55992-forecast-chart-value-change (05/20/2020)
			   -- new forumla changes
			   sum(c4063_cal_chart_back_order_qty) chart_bo_qty,
			   sum(c4063_cal_chart_sales_replish_01) chart_sales_repl_qty_01,
			   sum(c4063_cal_chart_sales_replish_02) chart_sales_repl_qty_02,
			   sum(c4063_cal_chart_sales_replish_03) chart_sales_repl_qty_03,
			   sum(c4063_cal_chart_sales_replish_04) chart_sales_repl_qty_04,
			   sum(c4063_cal_chart_sales_replish_05) chart_sales_repl_qty_05,
			   sum(c4063_cal_chart_sales_replish_06) chart_sales_repl_qty_06,
			   sum(c4063_cal_chart_safety_stock_01) chart_safty_stock_01,
			   --
			   sum(c4063_cal_chart_safety_stock_02) chart_safty_stock_02,
			   sum(c4063_cal_chart_safety_stock_03) chart_safty_stock_03,
			   sum(c4063_cal_chart_safety_stock_04) chart_safty_stock_04,
			   sum(c4063_cal_chart_safety_stock_05) chart_safty_stock_05,
			   sum(c4063_cal_chart_safety_stock_06) chart_safty_stock_06,
			   --
			   sum(c4063_cal_chart_set_priority_pre_mon_qty) chart_sppm,
			   sum(c4063_cal_chart_set_priority_01) chart_set_priority_01,
			   --
			   sum(c4063_cal_chart_set_priority_02) chart_set_priority_02,
			   sum(c4063_cal_chart_set_priority_03) chart_set_priority_03,
			   sum(c4063_cal_chart_set_priority_04) chart_set_priority_04,
			   sum(c4063_cal_chart_set_priority_05) chart_set_priority_05,
			   sum(c4063_cal_chart_set_priority_06) chart_set_priority_06,
			   --
			   sum(c4063_cal_chart_all_total_01) chart_overall_total_01,
			   sum(c4063_cal_chart_all_total_02) chart_overall_total_02,
			   sum(c4063_cal_chart_all_total_03) chart_overall_total_03,
			   sum(c4063_cal_chart_all_total_04) chart_overall_total_04,
			   sum(c4063_cal_chart_all_total_05) chart_overall_total_05,
			   sum(c4063_cal_chart_all_total_06) chart_overall_total_06
			   
		  FROM T4063_TTP_FORECAST_BY_PART_DTLS t4063, T4057_TTP_VENDOR_CAPACITY_SUMMARY_DTLS t4057
         WHERE t4057.C4057_TTP_VENDOR_CAPA_DTLS_ID = t4063.C4057_TTP_BY_VENDOR_SUMMARY_ID
 		   AND t4057.C301_VENDOR_ID = p_vendor_id
 		   AND t4057.C4057_LOAD_DATE = p_load_date
		   AND t4057.c4057_void_fl IS NULL
 		   AND t4063.c4063_void_fl IS NULL
	  GROUP BY t4057.C301_VENDOR_ID, t4057.C4057_LOAD_DATE;

	BEGIN

		FOR upd_forecast_vendor IN forecast_by_vendor_cur
		LOOP
	
			UPDATE T4064_TTP_FORECAST_BY_VENDOR_DTLS
			   SET C4064_CAL_BACK_ORDER_QTY = upd_forecast_vendor.back_ord_qty,
			   	   C4064_CAL_SET_PRIORITY_PRE_MON_QTY = upd_forecast_vendor.pre_mon_sp_qty,
			   	   C4064_CAL_SET_PRIORITY_01 = upd_forecast_vendor.sp_1,
			   	   C4064_CAL_SET_PRIORITY_02 = upd_forecast_vendor.sp_2,
			   	   C4064_CAL_SET_PRIORITY_03 = upd_forecast_vendor.sp_3,
			   	   C4064_CAL_SET_PRIORITY_04 = upd_forecast_vendor.sp_4,
			   	   C4064_CAL_SET_PRIORITY_05 = upd_forecast_vendor.sp_5,
			   	   C4064_CAL_SET_PRIORITY_06 = upd_forecast_vendor.sp_6,
			   	   C4064_SALES_REPLISH_01 = upd_forecast_vendor.sales_repl_1,
			   	   C4064_SALES_REPLISH_02 = upd_forecast_vendor.sales_repl_2,
			   	   C4064_SALES_REPLISH_03 = upd_forecast_vendor.sales_repl_3,
			   	   C4064_SALES_REPLISH_04 = upd_forecast_vendor.sales_repl_4,
			   	   C4064_SALES_REPLISH_05 = upd_forecast_vendor.sales_repl_5,
			   	   C4064_SALES_REPLISH_06 = upd_forecast_vendor.sales_repl_6,
			   	   C4064_SALES_REPLISH_07 = upd_forecast_vendor.sales_repl_7,
			   	   C4064_SALES_REPLISH_08 = upd_forecast_vendor.sales_repl_8,
			   	   C4064_CAL_SAFETY_STOCK_01 = upd_forecast_vendor.safety_stock_1,
			   	   C4064_CAL_SAFETY_STOCK_02 = upd_forecast_vendor.safety_stock_2,
			   	   C4064_CAL_SAFETY_STOCK_03 = upd_forecast_vendor.safety_stock_3,
			   	   C4064_CAL_SAFETY_STOCK_04 = upd_forecast_vendor.safety_stock_4,
			   	   C4064_CAL_SAFETY_STOCK_05 = upd_forecast_vendor.safety_stock_5,
			   	   C4064_CAL_SAFETY_STOCK_06 = upd_forecast_vendor.safety_stock_6,
			   	   C4064_CAL_INV_TOTAL_01 = upd_forecast_vendor.inv_total_1,
			   	   C4064_CAL_INV_TOTAL_02 = upd_forecast_vendor.inv_total_2,
			   	   C4064_CAL_INV_TOTAL_03 = upd_forecast_vendor.inv_total_3,
			   	   C4064_CAL_INV_TOTAL_04 = upd_forecast_vendor.inv_total_4,
			   	   C4064_CAL_INV_TOTAL_05 = upd_forecast_vendor.inv_total_5,
			   	   C4064_CAL_INV_TOTAL_06 = upd_forecast_vendor.inv_total_6,
			   	   C4064_HISTORICAL_RECEIPT_AVG = upd_forecast_vendor.his_rec_avg,
			   	   C4064_FCST_HIST_REC_AVG_01 = upd_forecast_vendor.fsct_rec_1,
			   	   C4064_FCST_HIST_REC_AVG_02 = upd_forecast_vendor.fsct_rec_2,
			   	   C4064_FCST_HIST_REC_AVG_03 = upd_forecast_vendor.fsct_rec_3,
			   	   C4064_FCST_HIST_REC_AVG_04 = upd_forecast_vendor.fsct_rec_4,
			   	   C4064_FCST_HIST_REC_AVG_05 = upd_forecast_vendor.fsct_rec_5,
			   	   C4064_FCST_HIST_REC_AVG_06 = upd_forecast_vendor.fsct_rec_6,
			   	   --C4064_OTHER_NEED_FLG = upd_forecast_vendor.
			   	   C4064_CAL_OTHER_NEED_01 = round(upd_forecast_vendor.cal_other_need_1, 0),
			   	   C4064_CAL_OTHER_NEED_02 = round(upd_forecast_vendor.cal_other_need_2, 0),
			   	   C4064_CAL_OTHER_NEED_03 = round(upd_forecast_vendor.cal_other_need_3, 0),
			   	   C4064_CAL_OTHER_NEED_04 = round(upd_forecast_vendor.cal_other_need_4, 0),
			   	   C4064_CAL_OTHER_NEED_05 = round(upd_forecast_vendor.cal_other_need_5, 0),
			   	   C4064_CAL_OTHER_NEED_06 = round(upd_forecast_vendor.cal_other_need_6, 0),
			   	   C4064_PRE_HIST_REC_AVG_01 = round(upd_forecast_vendor.previous_hist_rec_1, 0),
			   	   C4064_PRE_HIST_REC_AVG_02 = round(upd_forecast_vendor.previous_hist_rec_2, 0),
			   	   C4064_PRE_HIST_REC_AVG_03 = round(upd_forecast_vendor.previous_hist_rec_3, 0),
			   	   C4064_CAL_OVERALL_TOTAL_01 = round(upd_forecast_vendor.cal_overall_total_1, 0),
			   	   C4064_CAL_OVERALL_TOTAL_02 = round(upd_forecast_vendor.cal_overall_total_2, 0),
			   	   C4064_CAL_OVERALL_TOTAL_03 = round(upd_forecast_vendor.cal_overall_total_3, 0),
			   	   C4064_CAL_OVERALL_TOTAL_04 = round(upd_forecast_vendor.cal_overall_total_4, 0),
			   	   C4064_CAL_OVERALL_TOTAL_05 = round(upd_forecast_vendor.cal_overall_total_5, 0),
			   	   C4064_CAL_OVERALL_TOTAL_06 = round(upd_forecast_vendor.cal_overall_total_6, 0),
			   	   c4064_cal_chart_other_need_01 = round(upd_forecast_vendor.chart_other_need_total, 0),
			   	   --PMT-55992-forecast-chart-value-change
			   	   -- chart data update
			   	   c4064_cal_chart_back_order_qty = round(upd_forecast_vendor.chart_bo_qty, 0),
					c4064_cal_chart_sales_replish_01 = round(upd_forecast_vendor.chart_sales_repl_qty_01, 0),
					c4064_cal_chart_sales_replish_02 = round(upd_forecast_vendor.chart_sales_repl_qty_02, 0),
					c4064_cal_chart_sales_replish_03 = round(upd_forecast_vendor.chart_sales_repl_qty_03, 0),
					c4064_cal_chart_sales_replish_04 = round(upd_forecast_vendor.chart_sales_repl_qty_04, 0),
					c4064_cal_chart_sales_replish_05 = round(upd_forecast_vendor.chart_sales_repl_qty_05, 0),
					c4064_cal_chart_sales_replish_06 = round(upd_forecast_vendor.chart_sales_repl_qty_06, 0),
					c4064_cal_chart_safety_stock_01 = round(upd_forecast_vendor.chart_safty_stock_01, 0),
					--
					c4064_cal_chart_safety_stock_02 = round(upd_forecast_vendor.chart_safty_stock_02, 0),
					c4064_cal_chart_safety_stock_03 = round(upd_forecast_vendor.chart_safty_stock_03, 0),
					c4064_cal_chart_safety_stock_04 = round(upd_forecast_vendor.chart_safty_stock_04, 0),
					c4064_cal_chart_safety_stock_05 = round(upd_forecast_vendor.chart_safty_stock_05, 0),
					c4064_cal_chart_safety_stock_06 = round(upd_forecast_vendor.chart_safty_stock_06, 0),
					--
					c4064_cal_chart_set_priority_pre_mon_qty = round(upd_forecast_vendor.chart_sppm, 0),
					c4064_cal_chart_set_priority_01 = round(upd_forecast_vendor.chart_set_priority_01, 0),
					--
					c4064_cal_chart_set_priority_02 = round(upd_forecast_vendor.chart_set_priority_02, 0),
					c4064_cal_chart_set_priority_03 = round(upd_forecast_vendor.chart_set_priority_03, 0),
					c4064_cal_chart_set_priority_04 = round(upd_forecast_vendor.chart_set_priority_04, 0),
					c4064_cal_chart_set_priority_05 = round(upd_forecast_vendor.chart_set_priority_05, 0),
					c4064_cal_chart_set_priority_06 = round(upd_forecast_vendor.chart_set_priority_06, 0),
					--
			   	   c4064_cal_chart_all_total_01 =  round(upd_forecast_vendor.chart_overall_total_01, 0),
			   	   c4064_cal_chart_all_total_02 =  round(upd_forecast_vendor.chart_overall_total_02, 0),
			   	   c4064_cal_chart_all_total_03 =  round(upd_forecast_vendor.chart_overall_total_03, 0),
			   	   c4064_cal_chart_all_total_04 =  round(upd_forecast_vendor.chart_overall_total_04, 0),
			   	   c4064_cal_chart_all_total_05 =  round(upd_forecast_vendor.chart_overall_total_05, 0),
			   	   c4064_cal_chart_all_total_06 =  round(upd_forecast_vendor.chart_overall_total_06, 0),
			   	   --
			   	   C4064_LAST_UPDATED_BY	= p_user_id,
			   	   C4064_LAST_UPDATED_DATE	= CURRENT_DATE
			 WHERE C301_VENDOR_ID = upd_forecast_vendor.vendor_id
			   AND C4064_LOAD_DATE = upd_forecast_vendor.load_date
			   AND c4064_void_fl IS NULL;
		END LOOP;
	
	-- to update vendor chart JSON data.
	 gm_upd_vendor_chart_json_data_by_po (p_vendor_id, p_load_date);--it is ttp detail id or vendor id
	--
	 gm_upd_vendor_chart_json_data_by_receipt (p_vendor_id, p_load_date);--it is ttp detail id or vendor id
	
	END gm_calc_forecast_by_vendor;

	/*************************************************************************
    * Description : Procedure to used to update master data required details based on ttp id.
    * Author   : 5
    *************************************************************************/   
	procedure gm_upd_forecast_data_capactiy_summary_dtls_table (
		p_ttp_details_id	IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE,
		p_user_id IN t4052_ttp_detail.c4052_last_updated_by%TYPE
	)
	AS
	--
		type capa_summary_dtls_array IS    TABLE OF  t4057_ttp_vendor_capacity_summary_dtls.c4057_ttp_vendor_capa_dtls_id%TYPE ;
		type total_qty_array IS    TABLE OF  t4063_ttp_forecast_by_part_dtls.c4063_cal_overall_total_01%TYPE ;
	--
    	arr_capa_sum_dtls_id capa_summary_dtls_array;
		arr_total_qty_1 total_qty_array;
		arr_total_qty_2 total_qty_array;
		arr_total_qty_3 total_qty_array;
		arr_total_qty_4 total_qty_array;
		arr_total_qty_5 total_qty_array;
		arr_total_qty_6 total_qty_array;
	--
	CURSOR all_forecast_total_cur
	IS
	
	 SELECT t4057.C4057_TTP_VENDOR_CAPA_DTLS_ID capa_dtls_id,
			   C4063_CAL_OVERALL_TOTAL_01,C4063_CAL_OVERALL_TOTAL_02,
			   C4063_CAL_OVERALL_TOTAL_03,C4063_CAL_OVERALL_TOTAL_04,
			   C4063_CAL_OVERALL_TOTAL_05,C4063_CAL_OVERALL_TOTAL_06
	      FROM T4063_TTP_FORECAST_BY_PART_DTLS t4063, T4057_TTP_VENDOR_CAPACITY_SUMMARY_DTLS t4057
 		 WHERE t4057.C4057_TTP_VENDOR_CAPA_DTLS_ID = t4063.C4057_TTP_BY_VENDOR_SUMMARY_ID
 		   AND t4057.C4052_TTP_DETAIL_ID = p_ttp_details_id
           AND t4057.c4057_void_fl IS NULL
 		   AND t4063.c4063_void_fl IS NULL;


	BEGIN
		-- bulk update	
	
		OPEN all_forecast_total_cur;
			LOOP
		FETCH all_forecast_total_cur BULK COLLECT INTO arr_capa_sum_dtls_id,arr_total_qty_1,arr_total_qty_2,arr_total_qty_3,arr_total_qty_4,arr_total_qty_5,arr_total_qty_6 LIMIT 500;

		FORALL i in 1.. arr_capa_sum_dtls_id.COUNT
			UPDATE T4057_TTP_VENDOR_CAPACITY_SUMMARY_DTLS 
			   SET C4057_FORECAST_MONTH_01 = arr_total_qty_1 (i),
			   	   C4057_FORECAST_MONTH_02 = arr_total_qty_2 (i),
			   	   C4057_FORECAST_MONTH_03 = arr_total_qty_3 (i),
			   	   C4057_FORECAST_MONTH_04 = arr_total_qty_4 (i),
			   	   C4057_FORECAST_MONTH_05 = arr_total_qty_5 (i),
			   	   C4057_FORECAST_MONTH_06 = arr_total_qty_6 (i),
			   	   c4057_last_updated_by = p_user_id,
			   	   c4057_last_updated_date = CURRENT_DATE
			 WHERE C4057_TTP_VENDOR_CAPA_DTLS_ID = arr_capa_sum_dtls_id (i)--check column
			   AND c4057_void_fl IS NULL;
			
		EXIT WHEN all_forecast_total_cur%NOTFOUND;
	    END LOOP;
	    CLOSE all_forecast_total_cur;

	END gm_upd_forecast_data_capactiy_summary_dtls_table;

	/*************************************************************************
    * Description : Procedure to used to update master data required details based on ttp id.
    * Author   : 6
    *************************************************************************/  
	procedure gm_upd_overall_summary_to_vendor_summary_table (
		p_vendor_id	IN t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_id%TYPE,
		p_load_date IN DATE,
		p_user_id IN t4052_ttp_detail.c4052_last_updated_by%TYPE
	)
	AS

	CURSOR upd_vendor_summary_cur
	IS

		SELECT c301_vendor_id vendor_id, c4057_load_date load_date,
			   SUM (nvl(c4057_recommand_qty,0)) recomm_qty,
			   SUM (nvl(c4057_old_po_qty,0))+SUM (nvl(c4057_raise_po_qty,0)) raise_po_qty,
			   SUM (nvl(c4057_order_cost,0)) order_cost
		  FROM t4057_ttp_vendor_capacity_summary_dtls
		 WHERE c301_vendor_id = p_vendor_id
		   AND c4057_load_date = p_load_date
		   AND c301_vendor_id IS NOT NULL
		   AND c4057_void_fl IS NULL
		   AND C4057_QTY_QUOTED >=0
	  GROUP BY c301_vendor_id, c4057_load_date;

	BEGIN
		FOR upd_vendor IN upd_vendor_summary_cur
		LOOP
		
			UPDATE t4058_ttp_vendor_capacity_summary
			   SET c4058_recommand_qty = upd_vendor.recomm_qty,
			   	   c4058_raise_po_qty  = upd_vendor.raise_po_qty,
			   	   c4058_order_cost  = upd_vendor.order_cost,
			   	   c4058_last_updated_by = p_user_id,
			   	   c4058_last_updated_date = current_date
			 WHERE c301_vendor_id = upd_vendor.vendor_id
			   AND c4058_load_date = upd_vendor.load_date
			   AND c301_vendor_id = p_vendor_id
			   AND c4058_void_fl IS NULL;
		END LOOP;
	
	END gm_upd_overall_summary_to_vendor_summary_table;
	
	/*************************************************************************
    * Description : Procedure to used to update master data required details based on ttp id.
    * Author   : 7
    *************************************************************************/   
	procedure gm_upd_orginal_ord_dtls_capacity_summary (
		p_ttp_details_id	IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE,
		p_user_id IN t4052_ttp_detail.c4052_last_updated_by%TYPE
	)
	AS
		v_org_order_qty t4057_ttp_vendor_capacity_summary_dtls.c4057_raise_po_qty%TYPE;
		v_org_order_cost NUMBER (15, 2);
	BEGIN

		SELECT SUM (nvl(c4057_raise_po_qty,0)) raise_po_qty,
           	   SUM (nvl(c4057_order_cost,0)) order_cost 
		  INTO v_org_order_qty,
     		   v_org_order_cost
	      FROM t4057_ttp_vendor_capacity_summary_dtls
		 WHERE c4052_ttp_detail_id = p_ttp_details_id
		   AND c301_vendor_id IS NOT NULL
		   AND c4057_void_fl IS NULL;

	--
		UPDATE t4059_ttp_capacity_summary
		   SET c4059_org_order_qty = v_org_order_qty,
		   	   c4059_org_order_cost = v_org_order_cost,
		   	   c4059_last_updated_by = p_user_id,
		   	   c4059_last_updated_date = CURRENT_DATE
		 WHERE c4052_ttp_detail_id = p_ttp_details_id
		   AND c4059_void_fl IS NULL;
	
	END gm_upd_orginal_ord_dtls_capacity_summary;

	/*************************************************************************
    * Description : Procedure to used to update master data required details based on ttp id.
    * Author   : 8
    *************************************************************************/  
	procedure gm_upd_ord_dtls_capacity_summary (
		p_ttp_details_id	IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE,
		p_user_id IN t4052_ttp_detail.c4052_last_updated_by%TYPE
	)
	AS
		v_order_qty t4057_ttp_vendor_capacity_summary_dtls.c4057_raise_po_qty%TYPE;
		v_order_cost NUMBER (15, 2);
	BEGIN

		SELECT SUM (nvl(c4057_raise_po_qty,0)) raise_po_qty,
           	   SUM (nvl(c4057_order_cost,0)) order_cost 
	      INTO v_order_qty,
     	       v_order_cost
	  	  FROM t4057_ttp_vendor_capacity_summary_dtls
	 	 WHERE c4052_ttp_detail_id = p_ttp_details_id
	 	   AND c301_vendor_id IS NOT NULL
	   	   AND c4057_void_fl IS NULL;

	--
		UPDATE t4059_ttp_capacity_summary
	   	   SET c4059_order_qty = v_order_qty,
	   	  	   c4059_order_cost = v_order_cost,
	   	  	   c4059_last_updated_by = p_user_id,
	   	  	   c4059_last_updated_date = CURRENT_DATE
		 WHERE c4052_ttp_detail_id = p_ttp_details_id
	  	   AND c4059_void_fl IS NULL;
	
	END gm_upd_ord_dtls_capacity_summary;

	/*************************************************************************
    * Description : Procedure to used to update pending approval vendor cnt.
    * Author   : 9
    *************************************************************************/ 
	procedure gm_upd_pending_approval_cnt (
		p_ttp_details_id	IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE,
		p_user_id IN t4052_ttp_detail.c4052_last_updated_by%TYPE
	)
	AS
		v_vendor_cnt NUMBER;
		v_load_date DATE;
	BEGIN
	
		
	    SELECT count(1) INTO v_vendor_cnt
	    	FROM(SELECT c301_vendor_id
		FROM t4057_ttp_vendor_capacity_summary_dtls
		WHERE c4052_ttp_detail_id = p_ttp_details_id
		AND c4057_void_fl IS NULL
		AND C4057_APPROVED_BY IS NULL
		AND c301_vendor_id IS NOT NULL
		GROUP BY c301_vendor_id);
	
		--
		UPDATE t4059_ttp_capacity_summary
		   SET c4059_pending_approval_cnt = v_vendor_cnt,
		       c4059_last_updated_by = p_user_id,
		       c4059_last_updated_date = CURRENT_DATE
		 WHERE c4052_ttp_detail_id = p_ttp_details_id
		   AND c4059_void_fl IS NULL;

	END gm_upd_pending_approval_cnt;
	
	/********************************************************************************************
    * Description : Procedure to push the data from t4055 to st4055 when lock and generate happen.
    * Author   : Agilan Singaravel
    *********************************************************************************************/ 
	PROCEDURE gm_push_vendor_capa_lock_dtls(
    p_ttp_details_id IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE,
    p_user_id        IN t4052_ttp_detail.c4052_last_updated_by%TYPE 
    )
    AS
    	v_load_date	   	   DATE;
    	v_status           t4059_ttp_capacity_summary.c901_status%TYPE;
    	v_type			   t4050_ttp.c901_ttp_type%type;
    BEGIN
		BEGIN
			-- to get the load date from ttp details id.
		 SELECT c4052_ttp_link_date
	       		INTO v_load_date
		       FROM t4052_ttp_detail
		      WHERE c4052_ttp_detail_id = p_ttp_details_id
		        AND c4052_void_fl      IS NULL;
		        
	        -- PC-2525: to avoid getting load date from mend load.
	    	-- gm_pkg_oppr_ld_mend_inventory.gm_op_fch_load_date(v_load_date);
	    
	    	--Delete summary stage table details based on ttp detail id 
	    	--dbms_output.put_line ('1.started the delete summary stage table  '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
			delete from ST4055_TTP_SUMMARY where C4052_TTP_DETAIL_ID = p_ttp_details_id;

			--To load st4055 summary data from t4055 summary table based on ttp detail id
			--dbms_output.put_line ('2.started the summary stage data load  '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
		   
			/*  PC-2950: 7_Multi Part part sync to TTP By Vendor
			 *  When Growth setup done for Multi Parts then the forecast values is 
			 *  pushing to TTP By Vendor (Multi Part sheets)
			 */ 
			BEGIN
				SELECT t4050.c901_ttp_type 
			  	  INTO v_type
			  	  FROM T4052_TTP_DETAIL t4052, T4050_TTP t4050
		     	 WHERE t4050.c4050_ttp_id = t4052.c4050_ttp_id
			   	   AND t4052.c4052_ttp_detail_id = p_ttp_details_id
			   	   AND t4052.c4052_void_fl IS NULL
			   	   AND t4050.c4050_void_fl IS NULL;
		 	EXCEPTION WHEN NO_DATA_FOUND THEN
		 		v_type := null;
		 	END;
			   
		 	-- delete the temp_sales_repl_load  based on TTP detail id
		 	
		 	DELETE FROM temp_sales_repl_load  WHERE c4052_ttp_detail_id  = p_ttp_details_id;
		 	
		 	-- to load the data from T4055.
		 	
		 	
		 	
			IF v_type = 50310 THEN --Multi Part
				-- to load the data from T4055.
				gm_pkg_oppr_ld_ttp_by_vendor_temp_load.gm_sav_temp_multipart_sales_repl_data (p_ttp_details_id,v_load_date);
				--
				gm_pkg_oppr_ld_ttp_by_vendor_stage.gm_sav_ttp_multipart_summary_stage_data(p_ttp_details_id,v_load_date);
			ELSE
				-- to load the data from T4055.
				gm_pkg_oppr_ld_ttp_by_vendor_temp_load.gm_sav_temp_sales_repl_data (p_ttp_details_id,v_load_date);
				--
				gm_pkg_oppr_ld_ttp_by_vendor_stage.gm_sav_ttp_summary_stage_data(p_ttp_details_id,v_load_date);
			END IF;
			
			--to update capacity summary details to t4057,t4058
			--dbms_output.put_line ('3.started the gm_upd_vendor_capa_master_dtls  '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
			gm_upd_vendor_capa_master_dtls(p_ttp_details_id,'Y', v_load_date, p_user_id);
		
			v_status := 108867;   --open status for ttp dashboard
		
			EXCEPTION WHEN OTHERS  THEN
				v_status := '108866';  --status TBE
				--dbms_output.put_line(SQLERRM);
		END;
		
		--to update status to open for t4059_ttp_capacity_summary table
		gm_upd_capa_summary_sts(p_ttp_details_id,v_status,p_user_id);  
		
	END gm_push_vendor_capa_lock_dtls;
	
	/***********************************************************************************
    * Description : Procedure to update the status for open when finalizing lock screnn.
    * Author   : Agilan Singaravel
    *************************************************************************************/ 
	procedure gm_upd_capa_summary_sts(
		p_ttp_details_id  	IN      t4059_ttp_capacity_summary.c4052_ttp_detail_id%TYPE,
		p_status_id         IN      t4059_ttp_capacity_summary.c901_status%TYPE,
		p_user_id           IN      t4059_ttp_capacity_summary.c4059_last_updated_by%TYPE
	)
	AS
	BEGIN
	
		UPDATE t4059_ttp_capacity_summary
	   	   SET c901_status = p_status_id,
	           c4059_last_updated_by = p_user_id,
	           c4059_last_updated_date = current_date
	     WHERE c4052_ttp_detail_id = p_ttp_details_id
	       AND c4059_void_fl is null;
	   
	END gm_upd_capa_summary_sts;

	
/*************************************************************************
  * Description : Procedure to used to update JSON Data by Part.
  * Author   : 
*************************************************************************/     
PROCEDURE gm_upd_vendor_chart_json_data_by_po (
    p_vendor_id IN t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_id%TYPE,
    p_load_date IN DATE)
    
    AS
v_po_chart CLOB;
    
	BEGIN

	--
	SELECT LISTAGG(JSON_DATA,'') WITHIN GROUP (
			ORDER BY JSON_DATA)
            INTO v_po_chart
		FROM
 		 (
  SELECT '[{category:['
    || REPLACE(Json_Object( 'label' Value TO_CHAR(Add_Months(p_load_date,-6), 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,-5), 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,-4), 'Mon'), 'label' VALUE TO_CHAR(Add_Months(p_load_date,-3), 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,-2), 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,-1), 'Mon'), 'label' Value TO_CHAR(p_load_date, 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,+1), 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,+2), 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,+3), 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,+4), 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,+5), 'Mon')),',','},{')
    ||']}],' JSON_DATA
  FROM Dual
  UNION ALL
  -- BOs
  SELECT 'dataset:[{seriesname:"BOs",valueBgColor:"#008080",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value TO_CHAR(C4064_CAL_CHART_BACK_ORDER_QTY), 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0'),',','},{')
    ||']},'
    ||
    --Sales Replenishment
    '{seriesname:"Sales Replenishment",valueBgColor:"#0000FF",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value C4064_CAL_CHART_SALES_REPLISH_01, 'value' Value C4064_CAL_CHART_SALES_REPLISH_02, 'value' Value C4064_CAL_CHART_SALES_REPLISH_03, 'value' Value C4064_CAL_CHART_SALES_REPLISH_04, 'value' Value C4064_CAL_CHART_SALES_REPLISH_05, 'value' Value C4064_CAL_CHART_SALES_REPLISH_06),',','},{')
    ||']},'
    ||
    --Safety Stock
    '{seriesname:"Safety Stock",valueBgColor:"#8A2BE2",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value C4064_CAL_CHART_SAFETY_STOCK_01, 'value' Value C4064_CAL_CHART_SAFETY_STOCK_02, 'value' Value C4064_CAL_CHART_SAFETY_STOCK_03, 'value' Value C4064_CAL_CHART_SAFETY_STOCK_04, 'value' Value C4064_CAL_CHART_SAFETY_STOCK_05, 'value' Value C4064_CAL_CHART_SAFETY_STOCK_06),',','},{')
    ||']},'
    ||
    --Priority Set Previous Month
    '{seriesname:"Priority Set Previous Month",valueBgColor:"#FFFF00",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value C4064_CAL_CHART_SET_PRIORITY_PRE_MON_QTY, 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0'),',','},{')
    ||']},'
    ||
    --Priority Set Current Month
    '{seriesname:"Priority Set Current Month",valueBgColor:"#008000",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value 	C4064_CAL_CHART_SET_PRIORITY_01, 'value' Value C4064_CAL_CHART_SET_PRIORITY_02, 'value' Value C4064_CAL_CHART_SET_PRIORITY_03, 'value' Value C4064_CAL_CHART_SET_PRIORITY_04, 'value' Value C4064_CAL_CHART_SET_PRIORITY_05, 'value' Value C4064_CAL_CHART_SET_PRIORITY_06),',','},{')
    ||']},'
    ||
    --Split Other Needs
    '{seriesname:"Split Other Needs",valueBgColor:"#00BFFF",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value c4064_cal_chart_other_need_01, 'value' Value C4064_CAL_OTHER_NEED_02, 'value' Value C4064_CAL_OTHER_NEED_03, 'value' Value C4064_CAL_OTHER_NEED_04, 'value' Value C4064_Cal_Other_Need_05, 'value' Value C4064_Cal_Other_Need_06),',','},{')
    ||']},'
    ||
    --FCST Historical Receipt Avg
    '{seriesname:"FCST Historical Receipt Avg",renderas:"Line",valueBgColor:"#FF0000",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value C4064_FCST_HIST_REC_AVG_01, 'value' Value C4064_Fcst_Hist_Rec_Avg_02, 'value' Value C4064_Fcst_Hist_Rec_Avg_03, 'value' Value C4064_Fcst_Hist_Rec_Avg_04, 'value' Value C4064_Fcst_Hist_Rec_Avg_05, 'value' Value C4064_Fcst_Hist_Rec_Avg_06),',','},{')
    ||']},'
    ||
    --Actual PO Qty (Historical)
    '{seriesname:"Actual PO Qty (Historical)",valueBgColor:"#7CFC00",data:['
    ||REPLACE(Json_Object( 'value' Value C4064_CAL_HIST_PO_MONTH_01, 'value' Value C4064_CAL_HIST_PO_MONTH_02, 'value' Value C4064_CAL_HIST_PO_MONTH_03, 'value' Value C4064_CAL_HIST_PO_MONTH_04, 'value' Value C4064_CAL_HIST_PO_MONTH_05, 'value' Value C4064_CAL_HIST_PO_MONTH_06, 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0'),',','},{')
    ||']}]' Json_Data
  FROM T4064_TTP_FORECAST_BY_VENDOR_DTLS
  WHERE C301_VENDOR_ID = p_vendor_id
  AND C4064_LOAD_DATE = p_load_date
  AND C4064_VOID_FL IS NULL
  );
  
  UPDATE T4064_TTP_FORECAST_BY_VENDOR_DTLS 
  SET C4064_CHART_PO_JSON_DATA = v_po_chart
   WHERE C301_VENDOR_ID = p_vendor_id
  AND C4064_LOAD_DATE = p_load_date
  AND C4064_VOID_FL IS NULL;	    
	    
	    
	END gm_upd_vendor_chart_json_data_by_po;
    
/*************************************************************************
  * Description : Procedure to used to update JSON Data by Part.
  * Author   : 
*************************************************************************/     
PROCEDURE gm_upd_vendor_chart_json_data_by_receipt (
    p_vendor_id IN t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_id%TYPE,
    p_load_date IN DATE)
    
    AS
     --
	v_receipt_chart CLOB;
	BEGIN
		--
		
SELECT LISTAGG(JSON_DATA,'') WITHIN GROUP (
ORDER BY JSON_DATA)
INTO v_receipt_chart
FROM
  (
  -- x axis Replace current_date with load date
  SELECT '[{category:['
    || REPLACE(Json_Object( 'label' Value TO_CHAR(Add_Months(p_load_date,-6), 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,-5), 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,-4), 'Mon'), 'label' VALUE TO_CHAR(Add_Months(p_load_date,-3), 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,-2), 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,-1), 'Mon'), 'label' Value TO_CHAR(p_load_date, 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,+1), 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,+2), 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,+3), 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,+4), 'Mon'), 'label' Value TO_CHAR(Add_Months(p_load_date,+5), 'Mon')),',','},{')
    ||']}],' JSON_DATA
  FROM Dual
  UNION ALL
  -- BOs
  SELECT 'dataset:[{seriesname:"BOs",valueBgColor:"#008080",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value TO_CHAR(C4064_CAL_CHART_BACK_ORDER_QTY), 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0'),',','},{')
    ||']},'
    ||
    --Sales Replenishment
    '{seriesname:"Sales Replenishment",valueBgColor:"#0000FF",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value C4064_CAL_CHART_SALES_REPLISH_01, 'value' Value C4064_CAL_CHART_SALES_REPLISH_02, 'value' Value C4064_CAL_CHART_SALES_REPLISH_03, 'value' Value C4064_CAL_CHART_SALES_REPLISH_04, 'value' Value C4064_CAL_CHART_SALES_REPLISH_05, 'value' Value C4064_CAL_CHART_SALES_REPLISH_06),',','},{')
    ||']},'
    ||
    --Safety Stock
    '{seriesname:"Safety Stock",valueBgColor:"#8A2BE2",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value 	C4064_CAL_CHART_SAFETY_STOCK_01, 'value' Value C4064_CAL_CHART_SAFETY_STOCK_02, 'value' Value C4064_CAL_CHART_SAFETY_STOCK_03, 'value' Value C4064_CAL_CHART_SAFETY_STOCK_04, 'value' Value C4064_CAL_CHART_SAFETY_STOCK_05, 'value' Value C4064_CAL_CHART_SAFETY_STOCK_06),',','},{')
    ||']},'
    ||
    --Priority Set Previous Month
    '{seriesname:"Priority Set Previous Month",valueBgColor:"#FFFF00",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value 	C4064_CAL_CHART_SET_PRIORITY_PRE_MON_QTY, 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0'),',','},{')
    ||']},'
    ||
    --Priority Set Current Month
    '{seriesname:"Priority Set Current Month",valueBgColor:"#008000",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value 	C4064_CAL_CHART_SET_PRIORITY_01, 'value' Value C4064_CAL_CHART_SET_PRIORITY_02, 'value' Value C4064_CAL_CHART_SET_PRIORITY_03, 'value' Value C4064_CAL_CHART_SET_PRIORITY_04, 'value' Value C4064_CAL_CHART_SET_PRIORITY_05, 'value' Value C4064_CAL_CHART_SET_PRIORITY_06),',','},{')
    ||']},'
    ||
    --Split Other Needs
    '{seriesname:"Split Other Needs",valueBgColor:"#00BFFF",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value c4064_cal_chart_other_need_01, 'value' Value C4064_CAL_OTHER_NEED_02, 'value' Value C4064_CAL_OTHER_NEED_03, 'value' Value C4064_CAL_OTHER_NEED_04, 'value' Value C4064_Cal_Other_Need_05, 'value' Value C4064_Cal_Other_Need_06),',','},{')
    ||']},'
    ||
    --FCST Historical Receipt Avg
    '{seriesname:"FCST Historical Receipt Avg",renderas:"Line",valueBgColor:"#FF0000",data:['
    ||REPLACE(Json_Object( 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value C4064_FCST_HIST_REC_AVG_01, 'value' Value C4064_Fcst_Hist_Rec_Avg_02, 'value' Value C4064_Fcst_Hist_Rec_Avg_03, 'value' Value C4064_Fcst_Hist_Rec_Avg_04, 'value' Value C4064_Fcst_Hist_Rec_Avg_05, 'value' Value C4064_Fcst_Hist_Rec_Avg_06),',','},{')
    ||']},'
    ||
    --Actual Receipt (Historical)
    '{seriesname:" Actual Receipt (Historical)",valueBgColor:"#7CFC00",data:['
    ||REPLACE(Json_Object( 'value' Value C4064_CAL_HIST_RECEIPT_MONTH_01, 'value' Value C4064_CAL_HIST_RECEIPT_MONTH_02, 'value' Value C4064_CAL_HIST_RECEIPT_MONTH_03, 'value' Value C4064_CAL_HIST_RECEIPT_MONTH_04, 'value' Value C4064_CAL_HIST_RECEIPT_MONTH_05, 'value' Value C4064_CAL_HIST_RECEIPT_MONTH_06, 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0', 'value' Value '0'),',','},{')
    ||']}]' Json_Data
  FROM T4064_TTP_FORECAST_BY_VENDOR_DTLS
  WHERE  C301_VENDOR_ID = p_vendor_id
  AND C4064_LOAD_DATE = p_load_date	
  AND C4064_VOID_FL IS NULL
  );

  -- to update the receipt JSON data
  
    UPDATE  T4064_TTP_FORECAST_BY_VENDOR_DTLS 
  		SET c4064_chart_receipt_json_data = v_receipt_chart
  	WHERE  C301_VENDOR_ID = p_vendor_id
  AND C4064_LOAD_DATE = p_load_date
  AND C4064_VOID_FL IS NULL;
	    
	    
	END gm_upd_vendor_chart_json_data_by_receipt;

	
	/*************************************************************************
  * Description : Procedure to used to update overall total based on the formula.
  * Author   : 
	*************************************************************************/   
	
	PROCEDURE gm_upd_first_month_overall_total
	(p_ttp_part_dtls_id IN NUMBER,
	p_net_on_hand IN NUMBER,
	p_bo_qty IN NUMBER,
	p_sp_pre_mon_qty IN NUMBER,
	p_sp_cur_mon_qty IN NUMBER,
	p_sales_rep_curr_qty IN NUMBER,
	p_sales_rep_next_2mon_qty IN NUMBER,
	p_total_need_qty IN NUMBER,
	p_fcst_hist_avg_qty IN NUMBER,
	p_other_need_qty IN NUMBER,
	p_immediate_need_qty IN NUMBER)
	AS
	v_overall_total NUMBER := 0;
	v_cal_total NUMBER;
	v_cal_other_needs NUMBER;
	--
	v_tmp_other_need_total NUMBER;
	v_tmp_chart_val NUMBER := 0;
	v_overall_other_needs NUMBER;
	-- PMT-51948 : TTP forecast formula changes (03/20/2020)
	v_safety_stock_01 NUMBER;
	
	BEGIN
	
	
			-- PMT-51948 : TTP forecast new formula changes (03/20/2020)
			
			SELECT GREATEST  ((p_sales_rep_next_2mon_qty - p_net_on_hand), 0)
			 INTO v_safety_stock_01 FROM DUAL;
			 
		-- 1. to find the calc flag
	-- new formula 	
	--NOH - (BO + SR 1+ PSPM + PSCM) - (SR 2 + SR 3) > 0		
	--	MIN( Inv Qty + Other need + (SR 2 + SR 3) - NOH , FCST avg)	
		
	--((BO + SR 1 + PSPM + PSCM ) + (SR 2 + SR3) - NOH < FCST	
	--AND	
	--Other need >0	
	--	MIN (Inv qty + Other need + (SR 2 + SR 3) - NOH - Saftety stock ), FCST qty)
		
	--	(BO + SR 1 + PSPM + PSCM + SR 2 + SR 3) - NOH
	
	
	IF (p_net_on_hand - (p_bo_qty + p_sales_rep_curr_qty + p_sp_pre_mon_qty + p_sp_cur_mon_qty) - p_sales_rep_next_2mon_qty > 0)
	THEN
		v_overall_total := least(p_fcst_hist_avg_qty, ((p_immediate_need_qty + p_other_need_qty + p_sales_rep_next_2mon_qty) - p_net_on_hand));
	ELSE
		IF (((p_bo_qty + p_sales_rep_curr_qty + p_sp_pre_mon_qty + p_sp_cur_mon_qty + p_sales_rep_next_2mon_qty) - p_net_on_hand < p_fcst_hist_avg_qty) AND p_other_need_qty > 0)
		THEN
			v_overall_total := least(p_fcst_hist_avg_qty, ((p_immediate_need_qty + p_other_need_qty + p_sales_rep_next_2mon_qty) - (p_net_on_hand + v_safety_stock_01)));
		ELSE
			v_overall_total := (p_bo_qty + p_sales_rep_curr_qty + p_sp_pre_mon_qty + p_sp_cur_mon_qty + p_sales_rep_next_2mon_qty) - p_net_on_hand;
		END IF;
	END IF;
	
	/*
	
		IF (p_net_on_hand < (p_total_need_qty + p_sales_rep_next_2mon_qty))
		THEN
			-- 2. to calculate the total req
			IF ((p_net_on_hand - (p_bo_qty + p_sales_rep_curr_qty + p_sp_pre_mon_qty + p_sp_cur_mon_qty ) - (p_sales_rep_next_2mon_qty)) >0)
			THEN
			-- PMT-51948 : TTP forecast formula changes (03/20/2020)
			--v_cal_total := (p_total_need_qty + p_sales_rep_next_2mon_qty) - p_net_on_hand;
			-- new changes (as of 03/20)
			v_cal_total := (p_immediate_need_qty + p_other_need_qty + p_sales_rep_next_2mon_qty) - (p_net_on_hand);
			
				-- to find the min values
				v_overall_total := p_fcst_hist_avg_qty;
				
				-- to assign the least values
				SELECT least (p_fcst_hist_avg_qty, v_cal_total) INTO v_overall_total 
				FROM DUAL;
				--
			ELSIF(((p_bo_qty + p_sales_rep_curr_qty + p_sp_pre_mon_qty + p_sp_cur_mon_qty + p_sales_rep_next_2mon_qty) - p_net_on_hand) < p_fcst_hist_avg_qty
					AND p_other_need_qty > 0)
			THEN
				v_cal_total := (p_immediate_need_qty + p_other_need_qty + p_sales_rep_next_2mon_qty) - (p_net_on_hand + v_safety_stock_01);
			
				-- to find the min values
				v_overall_total := p_fcst_hist_avg_qty;
				
				-- to assign the least values
				SELECT least (p_fcst_hist_avg_qty, v_cal_total) INTO v_overall_total 
				FROM DUAL;
				
				
			ELSE
			--
			v_overall_total := (p_bo_qty + p_sales_rep_curr_qty + p_sp_pre_mon_qty + p_sp_cur_mon_qty + p_sales_rep_next_2mon_qty) - (p_net_on_hand );
			
			END IF; -- end if big cal
		
		END IF; -- end if main
		
		*/
		
			-- to calculate over all total (old formula)
			--v_overall_other_needs := (p_other_need_qty - v_overall_total - (p_net_on_hand - p_immediate_need_qty - p_sales_rep_next_2mon_qty));
		
			--
			v_overall_other_needs := 0;
			-- formula
			IF ((p_net_on_hand > (p_immediate_need_qty + p_other_need_qty + p_sales_rep_next_2mon_qty)) OR ((p_net_on_hand - (p_sales_rep_next_2mon_qty + p_other_need_qty + p_immediate_need_qty)) + p_fcst_hist_avg_qty >=0) )
			THEN
			
				v_overall_other_needs := 0;
			ELSE
				v_overall_other_needs := greatest (0,  (p_net_on_hand  - p_sales_rep_next_2mon_qty)) - p_immediate_need_qty - p_other_need_qty + v_overall_total;
				
				
			END IF;
			
				
			--
				--dbms_output.put_line ('p_ttp_part_dtls_id ' ||p_ttp_part_dtls_id ||' v_overall_other_needs qty ' || v_overall_other_needs);
			
		
			 -- to check negative values and reset the values
		   IF v_overall_other_needs < 0
		   THEN
				v_overall_other_needs := abs(v_overall_other_needs);
		   ELSE
				v_overall_other_needs := 0;
		   END IF;
		   
		   -- finally to div by 3
		   v_cal_other_needs := ROUND(v_overall_other_needs / 3);
			
		/*
		   -- to calculate others needs
			v_tmp_other_need_total := ((p_net_on_hand - p_immediate_need_qty) - p_sales_rep_next_2mon_qty);
			
			-- to decide the calc
			IF v_tmp_other_need_total < 0
			THEN
				v_cal_other_needs := ROUND((p_total_need_qty - v_overall_total) /3);
			ELSE
				--v_cal_other_needs := ROUND(((p_other_need_qty - v_overall_total - p_net_on_hand - (p_immediate_need_qty))- p_sales_rep_next_2mon_qty)/3);
		   		v_cal_other_needs := ROUND((p_other_need_qty - v_overall_total - v_tmp_other_need_total)/3);				
			END IF;
		  
		   */
			
		  
		   
		   -- to find the chart others need
			IF p_other_need_qty <> 0
			THEN
				v_tmp_chart_val := p_other_need_qty - v_overall_other_needs ;
			END IF;
			
		   	-- to update the overall 1 total and other needs
		
			UPDATE t4063_ttp_forecast_by_part_dtls 
	               SET c4063_cal_overall_total_01 = v_overall_total
	               , c4063_cal_other_need_02 = v_cal_other_needs
	               , c4063_cal_other_need_03 = v_cal_other_needs
	               , c4063_cal_other_need_04 = v_cal_other_needs
	               , c4063_cal_other_need_05 = 0
	               , c4063_cal_other_need_06 = 0
	               -- to update total needs
	               , c4063_cal_total_required_02 = c4063_cal_inv_total_02 + v_cal_other_needs
	               , c4063_cal_total_required_03 = c4063_cal_inv_total_03 + v_cal_other_needs
	               , c4063_cal_total_required_04 = c4063_cal_inv_total_04 + v_cal_other_needs
	               , c4063_cal_total_required_05 = c4063_cal_inv_total_05 
	               , c4063_cal_total_required_06 = c4063_cal_inv_total_06 
	               --
	               , c4063_manual_cal_other_need_qty = CASE WHEN v_overall_other_needs < 0 THEN 0 ELSE v_overall_other_needs END
	               , c4063_cal_chart_other_need_01 =  CASE WHEN v_tmp_chart_val < 0 THEN 0 ELSE v_tmp_chart_val END 
				 WHERE c4063_ttp_forecast_by_part_dtls_id = p_ttp_part_dtls_id
				   AND c4063_void_fl IS NULL;	
				   
		-- to validate the negative qty check ( calculate other need 01)
			UPDATE t4063_ttp_forecast_by_part_dtls 
	  			   SET c4063_cal_other_need_01 = 0
			 WHERE c4063_ttp_forecast_by_part_dtls_id = p_ttp_part_dtls_id
			    AND c4063_cal_other_need_01 < 0
			    AND c4063_void_fl         IS NULL;
		
				   
		END gm_upd_first_month_overall_total;
		
		/*************************************************************************
	  * Description : Procedure to used to Net on Hand values (based on formula)
	  * Author   : 
		*************************************************************************/   
		
		PROCEDURE gm_upd_net_on_hand (
		p_ttp_part_dtls_id IN NUMBER
		)
		AS
		v_net_on_hand_01_qty NUMBER;
		v_total_req_01_qty NUMBER;
		v_sales_rep_02_qty NUMBER;
		v_sales_rep_03_qty NUMBER;
		--
		v_total_req_02_qty NUMBER;
		v_total_req_03_qty NUMBER;
		v_total_req_04_qty NUMBER;
		v_total_req_05_qty NUMBER;
		--
		v_net_hand_02_qty NUMBER;
		v_net_hand_03_qty NUMBER;
		v_net_hand_04_qty NUMBER;
		v_net_hand_05_qty NUMBER;
		v_net_hand_06_qty NUMBER;
		--
		v_inv_total_01 NUMBER;
		v_other_need_01 NUMBER;
		
		BEGIN
			--
			SELECT c4063_cal_net_on_hand_01, c4063_cal_total_required_01 , C4063_SALES_REPLISH_02 , C4063_SALES_REPLISH_03
			, c4063_cal_total_required_02, c4063_cal_total_required_03, c4063_cal_total_required_04
			, c4063_cal_total_required_05, C4063_CAL_INV_TOTAL_01, C4063_CAL_OTHER_NEED_01
			INTO v_net_on_hand_01_qty, v_total_req_01_qty, v_sales_rep_02_qty, v_sales_rep_03_qty
			, v_total_req_02_qty, v_total_req_03_qty, v_total_req_04_qty
			, v_total_req_05_qty, v_inv_total_01, v_other_need_01
			FROM t4063_ttp_forecast_by_part_dtls
			WHERE c4063_ttp_forecast_by_part_dtls_id = p_ttp_part_dtls_id
				   AND c4063_void_fl IS NULL;	
			-- calculate 2nd month
				   
				   v_net_hand_02_qty := (v_net_on_hand_01_qty - v_inv_total_01 - v_other_need_01) - (v_sales_rep_02_qty + v_sales_rep_03_qty);
			-- calculate 3rd month
					v_net_hand_03_qty := v_net_hand_02_qty - v_total_req_02_qty;
			
			-- calculate 4th month
					v_net_hand_04_qty := v_net_hand_03_qty - v_total_req_03_qty; 	
			
-- calculate 5th month
					v_net_hand_05_qty := v_net_hand_04_qty - v_total_req_04_qty;
					
					-- calculate 6th month
					v_net_hand_06_qty := v_net_hand_05_qty - v_total_req_05_qty;
			-- to update the net on hand 02
			
			UPDATE t4063_ttp_forecast_by_part_dtls 
	               SET 
	               -- to update total needs
	               c4063_cal_net_on_hand_02 = v_net_hand_02_qty
	               , c4063_cal_net_on_hand_03 = v_net_hand_03_qty
					, c4063_cal_net_on_hand_04 = v_net_hand_04_qty
					, c4063_cal_net_on_hand_05 = v_net_hand_05_qty
					, c4063_cal_net_on_hand_06 = v_net_hand_06_qty
	               --
				 WHERE c4063_ttp_forecast_by_part_dtls_id = p_ttp_part_dtls_id
				   AND c4063_void_fl IS NULL;	
			
				  --  
			-- to validate the negative qty check (net on hand 02)
			UPDATE t4063_ttp_forecast_by_part_dtls 
	  			   SET c4063_cal_net_on_hand_02 = 0
			 WHERE c4063_ttp_forecast_by_part_dtls_id = p_ttp_part_dtls_id
			    AND c4063_cal_net_on_hand_02 < 0
			    AND c4063_void_fl         IS NULL;
			    
			  			-- to validate the negative qty check (net on hand 03)
			UPDATE t4063_ttp_forecast_by_part_dtls 
	  			   SET c4063_cal_net_on_hand_03 = 0
			 WHERE c4063_ttp_forecast_by_part_dtls_id = p_ttp_part_dtls_id
			    AND c4063_cal_net_on_hand_03 < 0
			    AND c4063_void_fl         IS NULL;
			    
			    			-- to validate the negative qty check (net on hand 04)
			UPDATE t4063_ttp_forecast_by_part_dtls 
	  			   SET c4063_cal_net_on_hand_04 = 0
			 WHERE c4063_ttp_forecast_by_part_dtls_id = p_ttp_part_dtls_id
			    AND c4063_cal_net_on_hand_04 < 0
			    AND c4063_void_fl         IS NULL;
			    
			    			-- to validate the negative qty check (net on hand 05)
			UPDATE t4063_ttp_forecast_by_part_dtls 
	  			   SET c4063_cal_net_on_hand_05 = 0
			 WHERE c4063_ttp_forecast_by_part_dtls_id = p_ttp_part_dtls_id
			    AND c4063_cal_net_on_hand_05 < 0
			    AND c4063_void_fl         IS NULL;
			    
			    			-- to validate the negative qty check (net on hand 02)
			UPDATE t4063_ttp_forecast_by_part_dtls 
	  			   SET c4063_cal_net_on_hand_06 = 0
			 WHERE c4063_ttp_forecast_by_part_dtls_id = p_ttp_part_dtls_id
			    AND c4063_cal_net_on_hand_06 < 0
			    AND c4063_void_fl         IS NULL;
				   
		END gm_upd_net_on_hand;
		
	/***********************************************************************************************************************************
    * Description : Procedure to update the vendor price based on the Raise PO Qty and the layers that is formed to get the vendor price.
    
    * Author   : mmuthusamy 
    ***********************************************************************************************************************************/   
	procedure gm_upd_vendor_price_by_calculation (
		p_ttp_detail_id   IN t4059_ttp_capacity_summary.c4052_ttp_detail_id%TYPE,
		p_user_id IN t4052_ttp_detail.c4052_last_updated_by%TYPE
	)
	AS
		
		type pnum_array      IS        TABLE OF  T205_PART_NUMBER.c205_part_number_id%TYPE;
		type vend_price_array IS        TABLE OF  t4057_ttp_vendor_capacity_summary_dtls.C402_VENDOR_PRICE%TYPE;
		type ttp_dtl_id_array IS        TABLE OF  t4057_ttp_vendor_capacity_summary_dtls.C4052_TTP_DETAIL_ID%TYPE;
		type vend_quote_qty_array IS    TABLE OF   st405_vendor_active_pricing.c405_qty_quoted%TYPE;
		type price_id_array IS          TABLE OF  st405_vendor_active_pricing. c405_pricing_id %TYPE;
		--
		type ttp_vendor_capa_dtls_id_array  IS TABLE OF t4057_ttp_vendor_capacity_summary_dtls.c4057_ttp_vendor_capa_dtls_id%TYPE;
		--
		arr_pnum pnum_array;
		arr_vend_price vend_price_array;
		arr_ttp_dtl_id ttp_dtl_id_array;
		arr_vend_quote_qty vend_quote_qty_array;
		arr_price_id  price_id_array;
		--
		arr_ttp_vendor_capa_id ttp_vendor_capa_dtls_id_array;
		--
		v_vendor_id NUMBER;
		
		-- to get all the vendor id
		CURSOR vendor_cur
		IS
		 SELECT t4057.c301_vendor_id vendor_id
		   FROM t4057_ttp_vendor_capacity_summary_dtls t4057
		  WHERE t4057.c4052_ttp_detail_id = p_ttp_detail_id
		  	AND t4057.c4057_calclation_fl = 'Y'
		    AND t4057.c4057_void_fl     IS NULL
		GROUP BY t4057.c301_vendor_id;
		
		CURSOR ttp_vend_price_cur
		IS

			SELECT t4057.c4057_ttp_vendor_capa_dtls_id ttp_vendor_capa_dtls_id,
					t4057.C4052_TTP_DETAIL_ID ttp_dtl_id ,t4057.C205_PART_NUMBER_ID pnum, st405.C405_VENDOR_PRICE vend_price,
			        NVL(st405.c405_qty_quoted,0) vend_quote_qty,c405_pricing_id price_id
			FROM 	t4057_ttp_vendor_capacity_summary_dtls  t4057,
					st405_vendor_active_pricing st405
			WHERE C4057_VOID_FL is null 
			AND C4057_RAISE_PO_QTY >= 0
			AND t4057.C301_VENDOR_ID 		 = st405.C301_VENDOR_ID
			AND t4057.C205_PART_NUMBER_ID 	 = st405.C205_PART_NUMBER_ID
			-- to add the vendor id
			AND t4057.c4057_calclation_fl = 'Y'
			AND t4057.c301_vendor_id = v_vendor_id
			AND c4052_ttp_detail_id			 = p_ttp_detail_id
			AND	(t4057.C4057_RAISE_PO_QTY >= DECODE(st405.C405_FROM_QTY, 1, 0, st405.C405_FROM_QTY)  
			AND t4057.C4057_RAISE_PO_QTY <= st405.C405_TO_QTY  );				
	
	BEGIN
	 -- bulk update
	
		-- to loop the vendor ids
		FOR vendor_dtl IN vendor_cur
		LOOP
			-- assign the vendor 
			v_vendor_id := vendor_dtl.vendor_id;
			--
			
			
			OPEN ttp_vend_price_cur;
			LOOP
			FETCH ttp_vend_price_cur BULK COLLECT INTO  arr_ttp_vendor_capa_id, arr_ttp_dtl_id,
				arr_pnum,arr_vend_price ,arr_vend_quote_qty,arr_price_id LIMIT 500;
	
			FORALL i in 1.. arr_pnum.COUNT
			
			-- to avoid the negative qty (added the abs)
				UPDATE t4057_ttp_vendor_capacity_summary_dtls 
				SET
					C402_VENDOR_PRICE = arr_vend_price(i),
	           	    C4057_ORDER_COST = arr_vend_price(i) * abs(c4057_raise_po_qty),
	                c4057_last_updated_by = p_user_id,
	                c4057_last_updated_date = CURRENT_DATE,
	                c4057_qty_quoted = NVL(arr_vend_quote_qty(i),0), --PBUG-4105
	                c4057_pricing_id = arr_price_id(i) -- PC-295 updating pricing id 
				WHERE c4057_ttp_vendor_capa_dtls_id = arr_ttp_vendor_capa_id(i)
					AND c4057_void_fl IS NULL;
				
			EXIT WHEN ttp_vend_price_cur%NOTFOUND;
		    END LOOP;
		    CLOSE ttp_vend_price_cur;
		    
		END LOOP; -- end of vendor details loop
	
	END gm_upd_vendor_price_by_calculation;
		 
END gm_pkg_oppr_ld_ttp_capa_master_txn;
/