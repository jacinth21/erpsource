CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ld_ttp_by_vendor_chart
IS
    -- Private Procedure Declaration
    /*************************************************************************
    * Description : This procedure used to calculate chart data and inset to temp table
    * Author   :
    *************************************************************************/
PROCEDURE gm_upd_tmp_forecast_chart (
        p_ttp_details_id IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE) ;
        
    /*************************************************************************
    * Description : This procedure used to calculate chart data and update T4063
    *     table
    * Author   :
    *************************************************************************/
PROCEDURE gm_upd_forecast_to_main_table (
        p_ttp_details_id IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE,
        p_user_id        IN t4052_ttp_detail.c4052_last_updated_by%TYPE) ;
        
    -- Private Procedure end
    
    /*************************************************************************
    * Description : This procedure used to calculate chart data and update T4063
    *     table
    * Author   :
    *************************************************************************/
PROCEDURE gm_upd_main_forecast_chart_dtls (
        p_ttp_details_id IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE,
        p_user_id        IN t4052_ttp_detail.c4052_last_updated_by%TYPE)
AS
BEGIN
    -- 0 delete temp table data.
     DELETE FROM st4063_ttp_forecast_chart_formula;
     
    -- 1 to calculate chart values and store to temp table.
    gm_upd_tmp_forecast_chart (p_ttp_details_id) ;
    
    -- 2 to update chart details to main table.
    gm_upd_forecast_to_main_table (p_ttp_details_id, p_user_id) ;
    --
    
END gm_upd_main_forecast_chart_dtls;
--

/*************************************************************************
* Description : This procedure used to calculate chart data and store to temp
*     table
* Author   :
*************************************************************************/
PROCEDURE gm_upd_tmp_forecast_chart (
        p_ttp_details_id IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE)
AS
    v_chart_overall_total_01 NUMBER;
    v_chart_overall_total_02 NUMBER;
    v_chart_overall_total_03 NUMBER;
    v_chart_overall_total_04 NUMBER;
    v_chart_overall_total_05 NUMBER;
    v_chart_overall_total_06 NUMBER;
    --
    v_chart_other_need_01 NUMBER;
    --
    v_tmp_other_need        NUMBER;
    v_chart_total           NUMBER;
    v_chart_spcm_01         NUMBER;
    v_chart_sppm            NUMBER;
    v_chart_safety_stock_01 NUMBER;
    v_chart_sales_rep_01    NUMBER;
    v_chart_sales_rep_02    NUMBER;
    v_chart_sales_rep_03    NUMBER;
    v_chart_sales_rep_04    NUMBER;
    v_chart_sales_rep_05    NUMBER;
    v_chart_sales_rep_06    NUMBER;
    v_chart_bo_qty          NUMBER;
    --
	v_chart_safety_stock_02 NUMBER;
	v_chart_safety_stock_03 NUMBER;
	v_chart_safety_stock_04 NUMBER;
	v_chart_safety_stock_05 NUMBER;
	v_chart_safety_stock_06 NUMBER;
	--
	v_chart_spcm_02         NUMBER;
	v_chart_spcm_03         NUMBER;
	v_chart_spcm_04         NUMBER;
	v_chart_spcm_05         NUMBER;
	v_chart_spcm_06         NUMBER;
	--
    CURSOR ttp_forecast_by_part_cur
    IS
         SELECT t4063.c4063_ttp_forecast_by_part_dtls_id part_dtls_id, t4063.c4063_cal_back_order_qty bo_qty,
            t4063.c4063_cal_net_on_hand_01 noh_01, t4063.c4063_sales_replish_01 sales_rep_1, t4063.c4063_sales_replish_02
            sales_rep_2, t4063.c4063_sales_replish_03 sales_rep_3, t4063.c4063_sales_replish_04 sales_rep_4
          , t4063.c4063_sales_replish_05 sales_rep_5, t4063.c4063_sales_replish_06 sales_rep_6,
            t4063.c4063_sales_replish_07 sales_rep_7, t4063.c4063_sales_replish_08 sales_rep_8,
            t4063.c4063_cal_safety_stock_01 safety_stock_1, t4063.c4063_cal_safety_stock_02 safety_stock_2,
            t4063.c4063_cal_safety_stock_03 safety_stock_3, t4063.c4063_cal_safety_stock_04 safety_stock_4,
            t4063.c4063_cal_safety_stock_05 safety_stock_5, t4063.c4063_cal_safety_stock_06 safety_stock_6,
            t4063.c4063_cal_set_priority_pre_mon_qty sppm, t4063.c4063_cal_set_priority_01 sp_1,
            t4063.c4063_cal_set_priority_02 sp_2, t4063.c4063_cal_set_priority_03 sp_3, t4063.c4063_cal_set_priority_04
            sp_4, t4063.c4063_cal_set_priority_05 sp_5, t4063.c4063_cal_set_priority_06 sp_6
          , t4063.c4063_cal_other_need_01 other_need_01, t4063.c4063_cal_other_need_02 other_need_02,
            t4063.c4063_cal_other_need_03 other_need_03, t4063.c4063_cal_other_need_04 other_need_04,
            t4063.c4063_cal_other_need_05 other_need_05, t4063.c4063_cal_other_need_06 other_need_06,
            t4063.c4063_fcst_hist_rec_avg_01 hist_receipt_avg_01, t4063.c205_part_number_id pnum
            --
            ,t4063.c4063_cal_net_on_hand_02 noh_02, t4063.c4063_cal_net_on_hand_03 noh_03, t4063.c4063_cal_net_on_hand_04 noh_04
            , t4063.c4063_cal_net_on_hand_05 noh_05, t4063.c4063_cal_net_on_hand_06 noh_06
			--
			, t4063.c4063_cal_inv_total_01 inv_total_01
			, t4063.c4063_cal_overall_total_01 overall_01
           FROM t4063_ttp_forecast_by_part_dtls t4063
          WHERE t4063.c4052_ttp_detail_id = p_ttp_details_id
            AND t4063.c4063_void_fl      IS NULL;
    --
BEGIN
    --
    FOR ttp_part IN ttp_forecast_by_part_cur
    LOOP
        -- to set the default values
        v_chart_other_need_01 := 0;
        
        -- 1 calcuate BO
		-- Formula: Back order qty - Net on Hand.
		-- to avoid negative values used greatest function.
		
         SELECT greatest (0, ttp_part.bo_qty - ttp_part.noh_01)
           INTO v_chart_bo_qty
           FROM DUAL;
           
        -- 2.1 cal sales Repl 01
		-- Formula: (Back order qty + Sales Repl Qty (01)) - (Net on Hand + Chart back order qty).
		-- Other months: Sales Repl Qty (xx) - Net on Hand
		-- to avoid negative values used greatest function.
		
		 SELECT greatest (0, (ttp_part.bo_qty + ttp_part.sales_rep_1) - (ttp_part.noh_01 + v_chart_bo_qty)),
		 	greatest (0, ttp_part.sales_rep_2 - ttp_part.noh_02), greatest (0, ttp_part.sales_rep_3 - ttp_part.noh_03),
		 	greatest (0, ttp_part.sales_rep_4         - ttp_part.noh_04),
		 	greatest (0, ttp_part.sales_rep_5 - ttp_part.noh_05),
		 	greatest (0, ttp_part.sales_rep_6         - ttp_part.noh_06)
		   INTO v_chart_sales_rep_01, v_chart_sales_rep_02, v_chart_sales_rep_03
		  , v_chart_sales_rep_04, v_chart_sales_rep_05, v_chart_sales_rep_06
		   FROM DUAL;
		   
        --3 cal safety stock
		-- Formula: (Back order qty + Sales Repl Qty (01)+ Sales Repl Qty (02) + Sales Repl Qty (03)) - (Net on Hand + Chart back order qty + Chart Sales Rep 01).
		-- to avoid negative values used greatest function.
		
		-- Formula: 2, 3, 4, 5, 6 (month)
			-- IF(safety stock 2 < 0 then safety stock OR Max (0, SR 2+ safety stock 2 - (NOH +  chart sales rep))
			
         SELECT greatest (0, (ttp_part.bo_qty + ttp_part.sales_rep_1 + ttp_part.sales_rep_2 + ttp_part.sales_rep_3) -
            (ttp_part.noh_01                     + v_chart_bo_qty + v_chart_sales_rep_01))
			--
			, CASE WHEN ttp_part.safety_stock_2 < 0 THEN ttp_part.safety_stock_2 ELSE greatest (0, (ttp_part.sales_rep_2 + ttp_part.safety_stock_2) - (ttp_part.noh_02 + v_chart_sales_rep_02)) END
			, CASE WHEN ttp_part.safety_stock_3 < 0 THEN ttp_part.safety_stock_3 ELSE greatest (0, (ttp_part.sales_rep_3 + ttp_part.safety_stock_3) - (ttp_part.noh_03 + v_chart_sales_rep_03)) END
			, CASE WHEN ttp_part.safety_stock_4 < 0 THEN ttp_part.safety_stock_4 ELSE greatest (0, (ttp_part.sales_rep_4 + ttp_part.safety_stock_4) - (ttp_part.noh_04 + v_chart_sales_rep_04)) END
			, CASE WHEN ttp_part.safety_stock_5 < 0 THEN ttp_part.safety_stock_5 ELSE greatest (0, (ttp_part.sales_rep_5 + ttp_part.safety_stock_5) - (ttp_part.noh_05 + v_chart_sales_rep_05)) END
			, CASE WHEN ttp_part.safety_stock_6 < 0 THEN ttp_part.safety_stock_6 ELSE greatest (0, (ttp_part.sales_rep_6 + ttp_part.safety_stock_6) - (ttp_part.noh_06 + v_chart_sales_rep_06)) END
           INTO v_chart_safety_stock_01
		   --
		   , v_chart_safety_stock_02,v_chart_safety_stock_03, v_chart_safety_stock_04
		   , v_chart_safety_stock_05, v_chart_safety_stock_06
           FROM DUAL;
		 
        --4 cal priority set pre month
		-- Formula: (Back order qty + Sales Repl Qty (01)+ Sales Repl Qty (02) + Sales Repl Qty (03)+
		--	 		Set Pro. pre. Month) - (Net on Hand + Chart back order qty + Chart Sales Rep 01 + Chart Safety stock 01).
		-- to avoid negative values used greatest function.
		
         SELECT greatest (0, (ttp_part.bo_qty + ttp_part.sales_rep_1 + ttp_part.sales_rep_2 + ttp_part.sales_rep_3 +
            ttp_part.sppm)                    - (ttp_part.noh_01 + v_chart_bo_qty + v_chart_sales_rep_01 +
            v_chart_safety_stock_01))
           INTO v_chart_sppm
           FROM DUAL;
           
        --5 cal priority set current month
		-- Formula: 1st month (Back order qty + Sales Repl Qty (01)+ Sales Repl Qty (02) + Sales Repl Qty (03)+
		--	 		Set Pro. pre. Month + Set Pro. current month) - (Net on Hand + Chart back order qty + Chart Sales Rep 01 + Chart Safety stock 01 + Chart SPPM).
		-- to avoid negative values used greatest function.
		
		-- Formula: other month 
		--Max (0, SR 1 + saftety stock 1 + PSCM 1 - ( NOH + chart sales rep + chart safety stock)
		
         SELECT greatest (0, (ttp_part.bo_qty + ttp_part.sales_rep_1 + ttp_part.sales_rep_2 + ttp_part.sales_rep_3 +
            ttp_part.sppm                    + ttp_part.sp_1) - (ttp_part.noh_01 + v_chart_bo_qty + v_chart_sales_rep_01 +
            v_chart_safety_stock_01          + v_chart_sppm))
			--
			,greatest (0, (ttp_part.sales_rep_2 + ttp_part.safety_stock_2 + ttp_part.sp_2) - (ttp_part.noh_02 + v_chart_sales_rep_02 + v_chart_safety_stock_02))
			,greatest (0, (ttp_part.sales_rep_3 + ttp_part.safety_stock_3 + ttp_part.sp_3) - (ttp_part.noh_03 + v_chart_sales_rep_03 + v_chart_safety_stock_03))
			,greatest (0, (ttp_part.sales_rep_4 + ttp_part.safety_stock_4 + ttp_part.sp_4) - (ttp_part.noh_04 + v_chart_sales_rep_04 + v_chart_safety_stock_04))
			,greatest (0, (ttp_part.sales_rep_5 + ttp_part.safety_stock_5 + ttp_part.sp_5) - (ttp_part.noh_05 + v_chart_sales_rep_05 + v_chart_safety_stock_05))
			,greatest (0, (ttp_part.sales_rep_6 + ttp_part.safety_stock_6 + ttp_part.sp_6) - (ttp_part.noh_06 + v_chart_sales_rep_06 + v_chart_safety_stock_06))
           INTO v_chart_spcm_01, v_chart_spcm_02, v_chart_spcm_03,
		   v_chart_spcm_04, v_chart_spcm_05, v_chart_spcm_06
           FROM DUAL;
           
        --6.1 Other needs (normal)
         SELECT (v_chart_bo_qty + v_chart_sales_rep_01 + v_chart_safety_stock_01 + v_chart_sppm + v_chart_spcm_01)
           INTO v_chart_total
           FROM DUAL;
           
         
        -- to compare chart total and historical fcst total or overall total =0 
        IF (v_chart_total           > ttp_part.hist_receipt_avg_01 OR ttp_part.overall_01 <=0) THEN
            v_chart_other_need_01 := 0;
        ELSE
            --6.2 other needs (cal)
			-- Formula: to add BO + SR1+ SR2+ SR3+ SPPM + SPCM+ Other needs - NOH
			-- to avoid negative values used greatest function.
			
             SELECT greatest (0, (ttp_part.bo_qty + ttp_part.sales_rep_1 + ttp_part.sales_rep_2 + ttp_part.sales_rep_3 +
               ttp_part.sppm                    + ttp_part.sp_1 + ttp_part.other_need_01) - (ttp_part.noh_01))
               INTO v_tmp_other_need
               FROM DUAL;
               --
               
            IF v_tmp_other_need        > ttp_part.hist_receipt_avg_01 THEN
            	--PMT-55992:  formula : (least (other need, (Hist fcst - chart total)) 
                v_chart_other_need_01 := least ((ttp_part.hist_receipt_avg_01 - v_chart_total), ttp_part.other_need_01);
            ELSE
            	-- formula updated
                v_chart_other_need_01 := greatest((least ( (ttp_part.inv_total_01 + ttp_part.other_need_01 + ttp_part.sales_rep_2 + ttp_part.sales_rep_3) - (ttp_part.noh_01) , ttp_part.other_need_01)), 0);
				
            END IF; -- v_tmp_other_need
            --
        END IF; -- v_chart_total
        --
        
        -- to update the details
        -- Overall chart total
         SELECT greatest((v_chart_total + v_chart_other_need_01), 0), greatest((v_chart_sales_rep_02 + v_chart_safety_stock_02 +
            v_chart_spcm_02      + ttp_part.other_need_02), 0), greatest((v_chart_sales_rep_03 + v_chart_safety_stock_03 +
            v_chart_spcm_03      + ttp_part.other_need_03), 0), greatest((v_chart_sales_rep_04 + v_chart_safety_stock_04 +
            v_chart_spcm_04      + ttp_part.other_need_04), 0), greatest((v_chart_sales_rep_05 + v_chart_safety_stock_05 +
            v_chart_spcm_05      + ttp_part.other_need_05), 0), greatest((v_chart_sales_rep_06 + v_chart_safety_stock_06 +
            v_chart_spcm_06      + ttp_part.other_need_06), 0)
           INTO v_chart_overall_total_01, v_chart_overall_total_02, v_chart_overall_total_03
          , v_chart_overall_total_04, v_chart_overall_total_05, v_chart_overall_total_06
           FROM DUAL;
        --
		 -- 02 (To compare the data and reset the safety stock values)
		IF ((v_chart_sales_rep_02 + v_chart_safety_stock_02 + v_chart_spcm_02 + ttp_part.other_need_02) < 0 AND v_chart_safety_stock_02 < 0)
		THEN
			v_chart_safety_stock_02 := v_chart_safety_stock_02 - ((v_chart_sales_rep_02 + v_chart_safety_stock_02 + v_chart_spcm_02 + ttp_part.other_need_02));
		END IF;
		
		-- 03 (To compare the data and reset the safety stock values)
		IF ((v_chart_sales_rep_03 + v_chart_safety_stock_03 + v_chart_spcm_03 + ttp_part.other_need_03) < 0 AND v_chart_safety_stock_03 < 0)
		THEN
			v_chart_safety_stock_03 := v_chart_safety_stock_03 - ((v_chart_sales_rep_03 + v_chart_safety_stock_03 + v_chart_spcm_03 + ttp_part.other_need_03));
		END IF;
		
		-- 04 (To compare the data and reset the safety stock values)
		IF ((v_chart_sales_rep_04 + v_chart_safety_stock_04 + v_chart_spcm_04 + ttp_part.other_need_04) < 0 AND v_chart_safety_stock_04 < 0)
		THEN
			v_chart_safety_stock_04 := v_chart_safety_stock_04 - ((v_chart_sales_rep_04 + v_chart_safety_stock_04 + v_chart_spcm_04 + ttp_part.other_need_04));
		END IF;
		
		-- 05 (To compare the data and reset the safety stock values)
		IF ((v_chart_sales_rep_05 + v_chart_safety_stock_05 + v_chart_spcm_05 + ttp_part.other_need_05) < 0 AND v_chart_safety_stock_05 < 0)
		THEN
			v_chart_safety_stock_05 := v_chart_safety_stock_05 - ((v_chart_sales_rep_05 + v_chart_safety_stock_05 + v_chart_spcm_05 + ttp_part.other_need_05));
		END IF;
		
		-- 06 (To compare the data and reset the safety stock values)
		IF ((v_chart_sales_rep_06 + v_chart_safety_stock_06 + v_chart_spcm_06 + ttp_part.other_need_06) < 0 AND v_chart_safety_stock_06 < 0)
		THEN
			v_chart_safety_stock_06 := v_chart_safety_stock_06 - ((v_chart_sales_rep_06 + v_chart_safety_stock_06 + v_chart_spcm_06 + ttp_part.other_need_06));
		END IF;
		
		
        -- insert data into temp table
         INSERT
           INTO st4063_ttp_forecast_chart_formula
            (
                c4063_ttp_forecast_by_part_dtls_id, c4052_ttp_detail_id, tmp_chart_back_order_qty
              , tmp_chart_sales_replish_01, tmp_chart_sales_replish_02, tmp_chart_sales_replish_03
              , tmp_chart_sales_replish_04, tmp_chart_sales_replish_05, tmp_chart_sales_replish_06
              , tmp_chart_safety_stock_01, tmp_chart_set_priority_pre_mon_qty, tmp_chart_set_priority_01
              , tmp_chart_other_need_01, tmp_chart_all_total_01, tmp_chart_all_total_02
              , tmp_chart_all_total_03, tmp_chart_all_total_04, tmp_chart_all_total_05
              , tmp_chart_all_total_06, c205_part_number_id
			  --
			  , tmp_chart_safety_stock_02, tmp_chart_safety_stock_03, tmp_chart_safety_stock_04, tmp_chart_safety_stock_05, tmp_chart_safety_stock_06
			  , tmp_chart_set_priority_02, tmp_chart_set_priority_03, tmp_chart_set_priority_04, tmp_chart_set_priority_05, tmp_chart_set_priority_06
            )
            VALUES
            (
                ttp_part.part_dtls_id, p_ttp_details_id, v_chart_bo_qty
              , v_chart_sales_rep_01, v_chart_sales_rep_02, v_chart_sales_rep_03
              , v_chart_sales_rep_04, v_chart_sales_rep_05, v_chart_sales_rep_06
              , v_chart_safety_stock_01, v_chart_sppm, v_chart_spcm_01
              , v_chart_other_need_01, v_chart_overall_total_01, v_chart_overall_total_02
              , v_chart_overall_total_03, v_chart_overall_total_04, v_chart_overall_total_05
              , v_chart_overall_total_06, ttp_part.pnum
			  --
			  , v_chart_safety_stock_02, v_chart_safety_stock_03, v_chart_safety_stock_04, v_chart_safety_stock_05, v_chart_safety_stock_06
			  , v_chart_spcm_02, v_chart_spcm_03, v_chart_spcm_04, v_chart_spcm_05, v_chart_spcm_06
            ) ;
          --  
    END LOOP;
    --
END gm_upd_tmp_forecast_chart;
--
/*************************************************************************
* Description : This procedure used to calculate chart data and update T4062
*     table
* Author   :
*************************************************************************/
PROCEDURE gm_upd_forecast_to_main_table
    (
        p_ttp_details_id IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE,
        p_user_id        IN t4052_ttp_detail.c4052_last_updated_by%TYPE
    )
AS
    --
type ttp_dtl_id_array
IS
    TABLE OF t4057_ttp_vendor_capacity_summary_dtls.C4052_TTP_DETAIL_ID%TYPE;
type qty_array
IS
    TABLE OF T4063_TTP_FORECAST_BY_PART_DTLS.C4063_CAL_BACK_ORDER_QTY%TYPE;
    --
    arr_ttp_dtl_id ttp_dtl_id_array;
    arr_forecast_by_part_dtls_id ttp_dtl_id_array;
    --
    arr_back_ord qty_array;
    arr_sales_rep_01 qty_array;
    arr_sales_rep_02 qty_array;
    arr_sales_rep_03 qty_array;
    arr_sales_rep_04 qty_array;
    arr_sales_rep_05 qty_array;
    arr_sales_rep_06 qty_array;
    --
    arr_safety_stock_01 qty_array;
    arr_sppm_qty qty_array;
    arr_spcm_qty qty_array;
    arr_other_need_qty qty_array;
    --
    arr_all_total_01 qty_array;
    arr_all_total_02 qty_array;
    arr_all_total_03 qty_array;
    arr_all_total_04 qty_array;
    arr_all_total_05 qty_array;
    arr_all_total_06 qty_array;
	--
	arr_safety_stock_02 qty_array;
	arr_safety_stock_03 qty_array;
	arr_safety_stock_04 qty_array;
	arr_safety_stock_05 qty_array;
	arr_safety_stock_06 qty_array;
	--
	arr_spcm_02 qty_array;
	arr_spcm_03 qty_array;
	arr_spcm_04 qty_array;
	arr_spcm_05 qty_array;
	arr_spcm_06 qty_array;
	--
    -- bulk update query
    CURSOR tmp_chart_data_cur
    IS
         SELECT c4063_ttp_forecast_by_part_dtls_id id, tmp_chart_back_order_qty tmp_bo_qty, tmp_chart_sales_replish_01
            tmp_sales_rep_01, tmp_chart_sales_replish_02 tmp_sales_rep_02, tmp_chart_sales_replish_03 tmp_sales_rep_03
          , tmp_chart_sales_replish_04 tmp_sales_rep_04, tmp_chart_sales_replish_05 tmp_sales_rep_05,
            tmp_chart_sales_replish_06 tmp_sales_rep_06, tmp_chart_safety_stock_01 tmp_safety_stock_01,
            tmp_chart_set_priority_pre_mon_qty tmp_sppm, tmp_chart_set_priority_01 tmp_sp_01, tmp_chart_other_need_01
            tmp_other_need, tmp_chart_all_total_01 tmp_all_01, tmp_chart_all_total_02 tmp_all_02
          , tmp_chart_all_total_03 tmp_all_03, tmp_chart_all_total_04 tmp_all_04, tmp_chart_all_total_05 tmp_all_05
          , tmp_chart_all_total_06 tmp_all_06
		  --
		  , tmp_chart_safety_stock_02 tmp_chart_saf_stock_02, tmp_chart_safety_stock_03 tmp_chart_saf_stock_03, tmp_chart_safety_stock_04 tmp_chart_saf_stock_04
		  , tmp_chart_safety_stock_05 tmp_chart_saf_stock_05, tmp_chart_safety_stock_06 tmp_chart_saf_stock_06
		  , tmp_chart_set_priority_02 tmp_chart_spcm_02, tmp_chart_set_priority_03 tmp_chart_spcm_03, tmp_chart_set_priority_04 tmp_chart_spcm_04
		  , tmp_chart_set_priority_05 tmp_chart_spcm_05, tmp_chart_set_priority_06 tmp_chart_spcm_06
           FROM st4063_ttp_forecast_chart_formula
          WHERE c4052_ttp_detail_id = p_ttp_details_id;
    --
BEGIN
    OPEN tmp_chart_data_cur;
    LOOP
        FETCH tmp_chart_data_cur BULK COLLECT
           INTO arr_forecast_by_part_dtls_id, arr_back_ord, arr_sales_rep_01
          , arr_sales_rep_02, arr_sales_rep_03, arr_sales_rep_04
          , arr_sales_rep_05, arr_sales_rep_06, arr_safety_stock_01
          , arr_sppm_qty, arr_spcm_qty, arr_other_need_qty
          , arr_all_total_01, arr_all_total_02, arr_all_total_03
          , arr_all_total_04, arr_all_total_05, arr_all_total_06
		  --
		  ,arr_safety_stock_02, arr_safety_stock_03, arr_safety_stock_04, arr_safety_stock_05, arr_safety_stock_06
		  ,arr_spcm_02, arr_spcm_03, arr_spcm_04, arr_spcm_05, arr_spcm_06

		  LIMIT 1000;
          
        FORALL i IN 1.. arr_forecast_by_part_dtls_id.COUNT
        --
		 UPDATE t4063_ttp_forecast_by_part_dtls
		SET c4063_cal_chart_back_order_qty           = arr_back_ord (i), 
			c4063_cal_chart_sales_replish_01 = arr_sales_rep_01 (i),
		    c4063_cal_chart_sales_replish_02         = arr_sales_rep_02 (i),
		    c4063_cal_chart_sales_replish_03 = arr_sales_rep_03 (i),
		    c4063_cal_chart_sales_replish_04         = arr_sales_rep_04 (i), 
		    c4063_cal_chart_sales_replish_05 = arr_sales_rep_05 (i),
		    c4063_cal_chart_sales_replish_06         = arr_sales_rep_06 (i),
		    c4063_cal_chart_safety_stock_01 = arr_safety_stock_01 (i),
		    c4063_cal_chart_set_priority_pre_mon_qty = arr_sppm_qty (i),
		    c4063_cal_chart_set_priority_01 = arr_spcm_qty (i),
		    c4063_cal_chart_other_need_01            = arr_other_need_qty (i), 
		    c4063_cal_chart_all_total_01 = arr_all_total_01 (i),
		    c4063_cal_chart_all_total_02        = arr_all_total_02 (i), 
		    c4063_cal_chart_all_total_03 = arr_all_total_03 (i),
		    c4063_cal_chart_all_total_04          = arr_all_total_04 (i), 
		    c4063_cal_chart_all_total_05 = arr_all_total_05 (i), 
		    c4063_cal_chart_all_total_06          = arr_all_total_06 (i)
			--
			, c4063_cal_chart_safety_stock_02 = arr_safety_stock_02 (i),
			c4063_cal_chart_safety_stock_03 = arr_safety_stock_03 (i),
			c4063_cal_chart_safety_stock_04 = arr_safety_stock_04 (i),
			c4063_cal_chart_safety_stock_05 = arr_safety_stock_05 (i),
			c4063_cal_chart_safety_stock_06 = arr_safety_stock_06 (i),
			--
			c4063_cal_chart_set_priority_02 = arr_spcm_02 (i),
			c4063_cal_chart_set_priority_03 = arr_spcm_03 (i),
			c4063_cal_chart_set_priority_04 = arr_spcm_04 (i),
			c4063_cal_chart_set_priority_05 = arr_spcm_05 (i),
			c4063_cal_chart_set_priority_06 = arr_spcm_06 (i)
		  WHERE c4063_ttp_forecast_by_part_dtls_id   = arr_forecast_by_part_dtls_id (i) ;
          --
        EXIT
        --
    WHEN tmp_chart_data_cur%NOTFOUND;
    --
    END LOOP;
    --
    CLOSE tmp_chart_data_cur;
    --
END gm_upd_forecast_to_main_table;
--

END gm_pkg_oppr_ld_ttp_by_vendor_chart;
/