CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ld_ttp_by_vendor_forecast_stage
IS
/*******************************************************
 * Purpose: This pkg used for to load GOP monthend Load staging tables which will have only six months data
 *
 * modification history
 * ====================
 * Person		 Date	Comments
 * ---------   ------	------------------------------------------
 * asingaravel    20191120  Initial Version
 *******************************************************/

   /*******************************************************
	* To truncate all Staging and TEMP table before reload data 
	*******************************************************/
	PROCEDURE gm_delete_forecast_stage_table	
	AS	
	BEGIN
		
		execute immediate 'TRUNCATE TABLE ST251_INVENTORY_LOCK_DETAILS';
		execute immediate 'TRUNCATE TABLE ST4042_SALES_REPL_DETAIL';
		execute immediate 'TRUNCATE TABLE ST4055_TTP_SUMMARY';
		execute immediate 'TRUNCATE TABLE TEMP_SALES_REPL_LOAD';
	END gm_delete_forecast_stage_table;

   /*******************************************************
	* This main procedure for load data into staging table
	*******************************************************/
	PROCEDURE gm_sav_process_backorder_stage_main( 
	  p_company_id	IN NUMBER
	, p_duration	IN NUMBER
	, p_load_date	IN DATE
	)
	AS
	
	BEGIN
	
		--1)For to store Backorder and set priority data in Staging table
		gm_sav_backorder_qty_stage_data(p_company_id,p_load_date);

		--2)For to store set priority data in staging table
	    gm_sav_set_priority_stage_data(p_company_id,p_duration,p_load_date);

   END gm_sav_process_backorder_stage_main;
    
    /*******************************************************
	* To Load BackOrder staging table
	*******************************************************/
	PROCEDURE gm_sav_backorder_qty_stage_data(
	  p_company_id	IN NUMBER
	, p_load_date	IN DATE
	)
	AS	
	BEGIN
	--
		INSERT INTO st502_back_order_dtls (c205_part_number_id,c502_bo_qty)
		SELECT pnum,  sum (qty) from (
		-- to get Sales BO Order qty 
		SELECT t502.c205_part_number_id pnum,sum(nvl(t502.c502_item_qty,0)) qty
		FROM t502_item_order t502,t501_order t501 
        WHERE t501.c501_order_id = t502.c501_order_id 
        AND t501.c901_order_type= 2525
        AND t501.c1900_company_id = p_company_id 
        AND t501.c501_status_fl = '0' 
        --AND C205_PART_NUMBER_ID = '632.072'
        AND EXISTS
	    (
	        SELECT DISTINCT V700S.REP_ID
	           FROM V700_TERRITORY_MAPPING_DETAIL V700S
	          WHERE V700S.REP_COMPID NOT IN
	            (
	                 SELECT C906_RULE_ID
	                   FROM T906_RULES
	                  WHERE C906_RULE_GRP_ID = 'ADDNL_COMP_IN_SALES'
	                    AND C906_VOID_FL    IS NULL
	            )
	            AND V700S.REP_ID = T501.C703_SALES_REP_ID
	    )
	    AND EXISTS
	    (
	         SELECT V700S.ac_id
	           FROM v700_territory_mapping_detail V700S
	          WHERE (V700S.COMPID                = DECODE (100803, 100803, NULL, 100803)
	            OR DECODE (100803, 100803, 1, 2) = 1)
	            AND V700S.ac_id                  = T501.C704_ACCOUNT_ID
	    )
	    AND EXISTS
	    (
	         SELECT V700S.REP_ID
	           FROM V700_TERRITORY_MAPPING_DETAIL V700S
	          WHERE V700S.DIVID = 100823
	            AND V700S.REP_ID = T501.C703_SALES_REP_ID
	    )
        AND c502_void_fl is null 
        AND c501_void_fl is null 
        AND c501_delete_fl IS NULL
        GROUP BY c205_part_number_id
        UNION ALL
        -- to get Item BO Order qty
        select pnum,  sum(qty) qty
	    FROM (
	        SELECT T521.c205_part_number_id pnum,nvl(T521.C521_QTY,0) qty
	        FROM t520_request t520, T521_REQUEST_DETAIL T521
	        , (
	                SELECT DISTINCT AD_ID, AD_NAME, REGION_ID
	                  , REGION_NAME, D_ID, D_NAME
	                  , D_NAME_EN, VP_ID, VP_NAME
	                  , V700.D_ACTIVE_FL, V700.D_COMPID
	                   FROM V700_TERRITORY_MAPPING_DETAIL V700
	                  WHERE ( (COMPID                    = DECODE (100803, 100803, NULL, 100803)
	                    OR DECODE (100803, 100803, 1, 2) = 1))
	                    AND DIVID                       = 100823
	                    AND V700.REP_COMPID NOT         IN
			            (
			                 SELECT C906_RULE_ID
			                   FROM T906_RULES
			                  WHERE C906_RULE_GRP_ID = 'ADDNL_COMP_IN_SALES'
			                    AND C906_VOID_FL    IS NULL
			            )
	            )
	            V700
	        WHERE T520.C520_REQUEST_ID          = T521.C520_REQUEST_ID
	            AND T520.C520_STATUS_FL           = 10
	            AND T520.C520_REQUEST_TO         IS NOT NULL
	            AND T520.C520_VOID_FL            IS NULL
	            AND T520.C520_REQUEST_TO          = V700.D_ID
	            --AND c205_part_number_id = '101.251'
	            AND T520.C1900_COMPANY_ID        = p_company_id
	            AND T520.C207_SET_ID             IS NULL
	            AND (T520.C520_MASTER_REQUEST_ID IS NULL
	            OR T520.C520_MASTER_REQUEST_ID   IN
	            (
	                 SELECT C520_REQUEST_ID FROM T520_REQUEST
	                 	WHERE C207_SET_ID IS NULL
			                 AND C1900_COMPANY_ID = p_company_id
		                	 AND C520_VOID_FL           IS NULL			            	 
	            ))
	            UNION ALL
	            SELECT T521.C205_PART_NUMBER_ID PNUM
	          , NVL(T521.C521_QTY,0) QTY
	           FROM T520_REQUEST T520, T521_REQUEST_DETAIL T521
	          WHERE T520.C520_REQUEST_ID          = T521.C520_REQUEST_ID
	            AND T520.C520_STATUS_FL           = 10
	            AND T520.C520_REQUEST_TO         IS NOT NULL
	            AND T520.C520_REQUEST_FOR         = 40025 -- Account Consignment
	            --AND c205_part_number_id = '101.251'
	            AND T520.C1900_COMPANY_ID        = p_company_id
	            AND T520.C520_VOID_FL            IS NULL
	            AND T520.C207_SET_ID             IS NULL
	            AND (T520.C520_MASTER_REQUEST_ID IS NULL
	            OR T520.C520_MASTER_REQUEST_ID   IN
	            (
	                 SELECT C520_REQUEST_ID
	                 FROM T520_REQUEST
	                 WHERE C207_SET_ID IS NULL
	                 AND C1900_COMPANY_ID = p_company_id
	                 AND C520_VOID_FL           IS NULL
	            ))
        
        ) group by pnum
        -- to get Shipped set BO Order qty
        UNION ALL
        SELECT T521.C205_PART_NUMBER_ID pnum, SUM (T521.C521_QTY) QTY
	   FROM T520_REQUEST T520, T521_REQUEST_DETAIL T521
	   , (
	        SELECT DISTINCT AD_ID, AD_NAME, REGION_ID
	          , REGION_NAME, D_ID, D_NAME
	          , D_NAME_EN, VP_ID, VP_NAME
	          , V700.D_ACTIVE_FL, V700.D_COMPID
	           FROM V700_TERRITORY_MAPPING_DETAIL V700
	          WHERE ( (COMPID                    = DECODE (100803, 100803, NULL, 100803)
	            OR DECODE (100803, 100803, 1, 2) = 1))
	            AND DIVID                       = 100823
	            AND V700.REP_COMPID NOT         IN
	            (
	                 SELECT C906_RULE_ID
	                   FROM T906_RULES
	                  WHERE C906_RULE_GRP_ID = 'ADDNL_COMP_IN_SALES'
	                    AND C906_VOID_FL    IS NULL
	            )	         
	    )
	    V700
	  WHERE T520.C520_REQUEST_ID         = T521.C520_REQUEST_ID
	    AND T520.C520_STATUS_FL          = 10
	    AND T520.C520_REQUEST_TO        IS NOT NULL
        --AND C205_PART_NUMBER_ID = '632.072'
	    AND T520.C520_VOID_FL           IS NULL
	    AND T520.C520_REQUEST_TO         = V700.D_ID
	    AND T520.C1900_COMPANY_ID       = p_company_id
	    AND T520.C520_MASTER_REQUEST_ID IN
	    (
	         SELECT C520_REQUEST_ID
	           FROM T520_REQUEST
	          WHERE C520_STATUS_FL = 40
	          	AND C1900_COMPANY_ID = p_company_id
                AND C520_VOID_FL           IS NULL
	            AND C207_SET_ID   IS NOT NULL
	    )
	GROUP BY T521.C205_PART_NUMBER_ID
    ) group by pnum;

    -- to update the load date.
    
    UPDATE st502_back_order_dtls set c502_load_date = p_load_date;
    
    END gm_sav_backorder_qty_stage_data;
    
   /*******************************************************
	* To Load BackOrder staging table
	*******************************************************/
	PROCEDURE gm_sav_set_priority_stage_data(
	  p_company_id	IN NUMBER
	, p_duration	IN NUMBER
	, p_load_date	IN DATE
	)
	AS	
	BEGIN
		
		INSERT INTO st520_request_priority_dtls(c205_part_number_id,c520_load_date,c520_required_date,c520_previous_sp_qty,c520_future_sp_qty)
		SELECT c205_part_number_id,p_load_date, TRUNC(c520_required_date,'month'), sum(nvl(c521_qty,0)),0  --nvl(sum(c521_qty),'-999')
		FROM t520_request t520,t521_request_detail t521
		WHERE t520.c520_request_id = t521.c520_request_id
		AND c520_status_fl < 20
		AND c1900_company_id = p_company_id
		AND c5403_request_priority IS NOT NULL
		AND c520_void_fl is null
		AND c520_required_date <  trunc(p_load_date)
		GROUP BY c205_part_number_id, TRUNC(c520_required_date,'month')
		UNION ALL
		SELECT c205_part_number_id,p_load_date,TRUNC(c520_required_date,'month'),0,sum(nvl(c521_qty,0))
		FROM t520_request t520,t521_request_detail t521
		WHERE t520.c520_request_id = t521.c520_request_id
		AND c520_status_fl < 20
		AND c1900_company_id = p_company_id
		AND c520_void_fl is null
		AND c5403_request_priority IS NOT NULL
		AND c520_required_date <=  trunc(ADD_MONTHS(p_load_date , p_duration),'month')
		AND c520_required_date >  trunc(p_load_date)
		GROUP BY c205_part_number_id,TRUNC(c520_required_date,'month');
		
    END gm_sav_set_priority_stage_data;
    
    
   /*******************************************************
	* To Load Inventory staging table
	*******************************************************/
    PROCEDURE gm_sav_inventory_qty_stage_table(
         p_load_date	IN DATE
        ,p_inventory_id IN t250_inventory_lock.c250_inventory_lock_id%type
	)	
	AS	
	BEGIN
		-- PMT-51948 : TTP forecast formula changes (03/20/2020)
		-- To added the build set Qty to (Net On Hand)
		INSERT INTO st251_inventory_lock_details (c250_inventory_lock_id,c205_part_number_id,c251_net_on_hand)
    	SELECT t250.c250_inventory_lock_id,t251.c205_part_number_id,(nvl(c251_in_stock,0) + nvl(c251_open_dhr,0) + nvl(c251_build_set, 0))
        FROM t250_inventory_lock t250,t251_inventory_lock_details t251
    	WHERE t251.c250_inventory_lock_id = t250.c250_inventory_lock_id
        AND t250.c250_lock_date = p_load_date
        AND t250.c250_inventory_lock_id = p_inventory_id
        AND t250.c250_void_fl IS NULL;
        
    END gm_sav_inventory_qty_stage_table; 
    
    
    
    /*******************************************************
	* This main procedure for load data into staging table
	*******************************************************/
	PROCEDURE gm_process_ttp_forecast_stage_main( 
	p_load_date	IN DATE
	)
	AS
	--
	 CURSOR ttp_details_cur 
     IS        
		 SELECT t4052.c4052_ttp_detail_id ttp_detail_id, t4050.c901_ttp_type ttp_type
		   FROM t4052_ttp_detail t4052, t4050_ttp t4050
		  WHERE t4050.c4050_ttp_id        = t4052.C4050_TTP_ID
		    AND t4052.c4052_ttp_link_date = p_load_date
		    AND t4050.c4050_void_fl      IS NULL
		    AND t4052.c4052_void_fl      IS NULL
		    order by t4052.c4052_ttp_detail_id,t4050.c901_ttp_type desc; 
		    
	BEGIN
			dbms_output.put_line ('inside gm_process_ttp_forecast_stage_main  '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
	
	    FOR ttp_details_id_cur IN ttp_details_cur
        	LOOP
        	-- ttp type available only multipart sheet.
        	
        	IF ttp_details_id_cur.ttp_type IS NULL -- non multi parts sheet
        	THEN
	        	--1)Save TTP summary table details into stage table
	        	 gm_pkg_oppr_ld_ttp_by_vendor_stage.gm_sav_ttp_summary_stage_data(ttp_details_id_cur.ttp_detail_id,p_load_date);
	        	 --2)
	        	 
	        	  dbms_output.put_line ('IIIIIII Sales Repl started   '||ttp_details_id_cur.ttp_detail_id ||' '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
	
	        	 -- New formula changes (to skip the sales repl stage load)
	        	  --gm_sav_ttp_sales_repl_stage_data(ttp_details_id_cur.ttp_detail_id,p_load_date);
	        	 
	        	 
	        	  dbms_output.put_line ('IIIIIII Sales Repl ended   '||ttp_details_id_cur.ttp_detail_id ||' '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
	
	        	 --3)
	        	 gm_pkg_oppr_ld_ttp_by_vendor_temp_load.gm_sav_temp_sales_repl_data(ttp_details_id_cur.ttp_detail_id, p_load_date);
        	ELSE  -- Multipart
        	
	        	--1)Save TTP summary table details into stage table
	        	 gm_pkg_oppr_ld_ttp_by_vendor_stage.gm_sav_ttp_multipart_summary_stage_data (ttp_details_id_cur.ttp_detail_id,p_load_date);
	        	--	
	        	--2)
	        	-- New formula changes (to skip the sales repl stage load)
	        	-- gm_sav_ttp_multipart_sales_repl_stage_data(ttp_details_id_cur.ttp_detail_id,p_load_date);
	        	 --3)
	        	 gm_pkg_oppr_ld_ttp_by_vendor_temp_load.gm_sav_temp_multipart_sales_repl_data(ttp_details_id_cur.ttp_detail_id, p_load_date);
        	END IF;
        	--
     END LOOP;
	
     dbms_output.put_line ('after completed gm_process_ttp_forecast_stage_main   '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
	
     
	END gm_process_ttp_forecast_stage_main;
    
    
 
    /*******************************************
     * To Load Sales Rep staging table
     ********************************************/     
     PROCEDURE  gm_sav_ttp_sales_repl_stage_data(
     	p_ttp_detail_id IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE,
     	p_load_date IN DATE
     )
     AS
     BEGIN

        INSERT INTO st4042_sales_repl_detail (c4052_ttp_detail_id,c4042_load_date
        ,c205_part_number_id,c4042_period,c4042_qty,c205_crossover_fl)
SELECT t4042.c4052_ttp_detail_id,T4042.load_date, t4042.c205_part_number_id pnum,
              c4042_period period,
              SUM((CASE WHEN refid IS NULL THEN DECODE (t4042.c901_type, 50563, NVL(c4042_qty, 0), 0) ELSE 0 END)) qty,
        T205.c205_crossover_fl
              FROM
                  -- Below code to fetch demand sheet based on par value
                  (
                       SELECT t4040.c4052_ttp_detail_id,t4040.c4040_demand_period_dt load_date, t4042.c205_part_number_id, t4042.c4042_period
                        ,T4042.c901_type, NVL (DECODE (t4042.c901_type, 50563, DECODE (
                          parvalue.c205_part_number_id, NULL, t4042.c4042_qty, DECODE (parvalue.parperiod, t4042.c4042_period,
                          parvalue.parvalue, 0)), 0), 0) c4042_qty,t4042.c4042_parent_ref_id refid
                         FROM t4042_demand_sheet_detail t4042, t4040_demand_sheet t4040
                   , (
                               SELECT c4040_demand_sheet_id, c205_part_number_id, parperiod
                                , demandvalue, usdemand, ousdemand
                                , parvalue, (
                                  CASE
                                      WHEN (parvalue - ousdemand) > 0
                                      THEN (parvalue - ousdemand)
                                      ELSE 0
                                  END) usparvalue, ousdemand ousparvalue
                                 FROM
                                  (
                                       SELECT t4042.c4040_demand_sheet_id, t4042.c205_part_number_id, MIN (t4042.c4042_period)
                                          parperiod, SUM (DECODE (t4042.c901_type, 50563, t4042.c4042_qty, 0)) demandvalue, SUM (
                                          DECODE (t4042.c901_type, 4000110, t4042.c4042_qty, 0)) usdemand, SUM (DECODE (
                                          t4042.c901_type, 4000111, t4042.c4042_qty, 0)) ousdemand, SUM (DECODE (t4042.c901_type,
                                          50566, t4042.c4042_qty, 0)) parvalue
                                         FROM t4042_demand_sheet_detail T4042 , t4040_demand_sheet t4040
                                        WHERE t4042.c4040_demand_sheet_id  = t4040.c4040_demand_sheet_id
                                    AND t4040.c4052_ttp_detail_id=p_ttp_detail_id
                                          AND t4042.c4042_period BETWEEN p_load_date AND ADD_MONTHS (p_load_date, (12- 1)) -- INPUT current month
                                          AND t4042.c901_type IN (50563, 50566, 4000110, 4000111)
                                    AND t4040.c4040_void_fl IS NULL
                                     GROUP BY t4042.c4040_demand_sheet_id, t4042.c205_part_number_id
                                  )
                                WHERE parvalue > demandvalue
                          )
                          parvalue -- Above to fetch part which has par value more than forecast
                        WHERE T4042.c4040_demand_sheet_id  = t4040.c4040_demand_sheet_id
                          AND t4042.c901_type              = 50563
                    -- 50563 FORECAST
                    AND t4040.c4052_ttp_detail_id=p_ttp_detail_id
                    AND t4040.c901_demand_type    = 40020 -- Sales
                          AND t4042.c4042_period         <= ADD_MONTHS (p_load_date, (12 - 1))   -- INPUT current month
                    AND t4040.c4040_void_fl IS NULL
                    and t4040.c901_level_id       = 102580--Company
                          AND t4042.c4040_demand_sheet_id = parvalue.c4040_demand_sheet_id(+)
                          AND t4042.c205_part_number_id   = parvalue.c205_part_number_id(+)
                          )
                  t4042, t205_part_number T205
                WHERE t4042.c205_part_number_id  = t205.c205_part_number_id
                  AND t4042.c205_part_number_id IS NOT NULL
                GROUP BY t4042.c4052_ttp_detail_id,T4042.load_date,c4042_period
                ,t4042.c205_part_number_id,t205.c205_crossover_fl;

        
	  END gm_sav_ttp_sales_repl_stage_data;

	  
    /*******************************************
     * To Load Sales Rep - multipart staging table
     ********************************************/     
     PROCEDURE  gm_sav_ttp_multipart_sales_repl_stage_data(
     	p_ttp_detail_id IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE,
     	p_load_date IN DATE
     )
     AS
     BEGIN

	     -- to update the multipart parts - ttp detail id
	     
		UPDATE st4042_sales_repl_detail
		SET c4052_ttp_detail_id       = p_ttp_detail_id
		  WHERE c4042_sales_repl_sno IN
		    (
		         SELECT st4042.c4042_sales_repl_sno
		           FROM st4042_sales_repl_detail st4042, t4055_ttp_summary t4055
		          WHERE st4042.c205_crossover_fl         IS NOT NULL
		            AND st4042.c205_part_number_id = t4055.c205_part_number_id
		            AND t4055.c4055_load_date = st4042.c4042_load_date
		            --AND t4055.c4055_load_date      = p_load_date
		            AND t4055.c4052_ttp_detail_id  = p_ttp_detail_id
		            AND t4055.c901_level_id        = 102580 -- World wide
		            AND t4055.c4055_void_fl       IS NULL
		    ) ;
    
		/*INSERT INTO st4042_sales_repl_detail (c4052_ttp_detail_id,c4042_load_date,c205_part_number_id,c4042_period,c4042_qty)
    	SELECT t4040.c4052_ttp_detail_id,t4040.c4040_demand_period_dt,t4042.c205_part_number_id,t4042.c4042_period,nvl(t4042.c4042_qty,0)
    	FROM t4040_demand_sheet t4040,t4042_demand_sheet_detail t4042, t205_part_number t205
    	WHERE t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
        AND t4040.c4052_ttp_detail_id = p_ttp_detail_id 
        AND t205.c205_part_number_id = t4042.c205_part_number_id
        AND  t205.c205_crossover_fl  = 'Y' -- only multi part
        AND t4040.c4040_demand_period_dt = p_load_date
        AND t4040.c901_demand_type    = 40020 -- Sales
		AND t4040.c901_level_id       = 102580--Company
        AND t4040.c901_level_value    = 100800--Globus Medical Inc
        AND t4042.c901_type           = 50563 --Forecast
        AND t4040.c4040_void_fl IS NULL;
        */
	  END gm_sav_ttp_multipart_sales_repl_stage_data;

END gm_pkg_oppr_ld_ttp_by_vendor_forecast_stage;
/