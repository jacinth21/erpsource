--@"Datawarehouse/Database/DMO/Packages/TTPVendor/gm_pkg_oppr_ttp_capa_load_override_vendor.pkg";
CREATE or REPLACE PACKAGE BODY gm_pkg_oppr_ttp_capa_load_override_vendor
IS
/*************************************************************************
    * Description : Procedure to used to Update Open work Order Vendor
    * Details in t4057 table
    * Author   : prabhu vigneshwaran M D 
*************************************************************************/    

PROCEDURE gm_upd_open_wo_vendor_dtls(
	  p_load_date 		IN 	 t4057_ttp_vendor_capacity_summary_dtls.c4057_load_date%TYPE,
	  p_company_id		IN	 t1900_company.c1900_company_id%TYPE,
	  p_duration	    IN	 NUMBER,
	  p_user_id 		IN 	 t4052_ttp_detail.c4052_last_updated_by%TYPE
)
AS
v_vendor_id t402_work_order.c301_vendor_id%TYPE;
v_vendor_name t301_vendor.c301_vendor_name%TYPE;
v_pnum t402_work_order.c205_part_number_id%TYPE;
v_created_dt t402_work_order.c402_created_date%TYPE;
v_vendor_price st405_vendor_active_pricing.c405_vendor_price%TYPE;
v_quoted_qty st405_vendor_active_pricing.c405_qty_quoted%TYPE;
v_vendor_moq st405_vendor_active_pricing.c405_qty_quoted%TYPE;
v_pricing_id st405_vendor_active_pricing.c405_pricing_id%TYPE;
v_no_vendor_id t402_work_order.c301_vendor_id%TYPE;
v_no_vendor_name t301_vendor.c301_vendor_name%TYPE;

CURSOR ttp_open_parts_cur
	IS
	--Cursor: To get the TTP details id, Part number from st4055_ttp_summary 
    --where part number not in (T4057 based on load date)
    SELECT  st4055.c4052_ttp_detail_id ttp_detail_id,
    	    st4055.c4055_load_date load_date, 
			st4055.c205_part_number_id partnum, 
			st4055.c205_part_num_desc pdesc,
			st4055.c4055_total_order_qty total_order_qty
	FROM st4055_ttp_summary st4055
	WHERE st4055.c205_part_number_id 
	NOT IN
		(
		SELECT c205_part_number_id
		FROM t4057_ttp_vendor_capacity_summary_dtls
		WHERE c4057_load_date = p_load_date
		AND c4057_void_fl IS NULL
		GROUP BY c205_part_number_id
		);
		
	--
	BEGIN
		  --Get the no vendor id from rule_value
		  v_no_vendor_id := get_rule_value('NO_VENDOR','TTP_BY_VEND_PO_GEN');
		  --Get no vendor name from t301 table by using no_vendor_id:690
		BEGIN
		SELECT c301_vendor_name 
		INTO   v_no_vendor_name
		FROM   t301_vendor 
		WHERE c301_vendor_id  = v_no_vendor_id;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		v_no_vendor_name := NULL;
		END;
		
		FOR ttp_open_part_upd IN ttp_open_parts_cur
         LOOP   
         
         	BEGIN
	         	
         	SELECT vendid,vendor_name,pnum,createddate,vendorprice,quotedqty,vendormoq,pricingid
         	INTO   v_vendor_id,v_vendor_name,v_pnum,v_created_dt,v_vendor_price,v_quoted_qty,v_vendor_moq,v_pricing_id
			FROM
				(
				SELECT t402.c301_vendor_id vendid, t301.c301_vendor_name vendor_name, 
    					t402.c205_part_number_id pnum, 
    					TRUNC (t402.c402_created_date, 'mm') createddate,
						MIN (st405.c405_vendor_price) vendorprice, 
						NVL(st405.c405_qty_quoted,0) quotedqty,
						NVL(st405.c405_vendor_moq,0) vendormoq, 
						st405.c405_pricing_id pricingid
				   FROM t402_work_order t402, st405_vendor_active_pricing st405,t301_vendor t301
				  WHERE t402.c301_vendor_id = t301.c301_vendor_id
					AND TRUNC (t402.c402_created_date, 'mm') < TRUNC (add_months (current_date, -p_duration), 'month')
					AND t402.c1900_company_id = p_company_id
					AND t402.c205_part_number_id = st405.c205_part_number_id
					AND t402.c301_vendor_id = st405.c301_vendor_id
					AND t402.c205_part_number_id = ttp_open_part_upd.partnum --'101.107' 
					AND t402.c402_void_fl IS NULL
			   GROUP BY t402.c301_vendor_id, t301.c301_vendor_name, t402.c205_part_number_id, TRUNC (t402.c402_created_date, 'mm')
						,st405.c405_qty_quoted, st405.c405_vendor_moq, st405.c405_pricing_id
				ORDER BY TRUNC (c402_created_date, 'mm') DESC, vendorprice
				)
			WHERE rownum = 1;
--				
			  EXCEPTION WHEN NO_DATA_FOUND THEN
			  --If no vendor  identified above query(no recent wo,no vendor identified in same month,no wo for part), 
			  --then pick a vendor who has lower price for a part from st405_vendor_active_pricing
				BEGIN
				SELECT vendorid,vendorname,partnum,vendorprice,quotedqty,vendormoq,pricingid 
				INTO  v_vendor_id,v_vendor_name,v_pnum,v_vendor_price,v_quoted_qty,v_vendor_moq,v_pricing_id
				FROM (
                SELECT 
                	t301.c301_vendor_id vendorid,
                	t301.c301_vendor_name vendorname,
                	st405.c205_part_number_id partnum,
                	MIN(st405.c405_vendor_price) vendorprice,
                	nvl(st405.c405_qty_quoted,0) quotedqty,
                	nvl(st405.c405_vendor_moq,0) vendormoq,
                	st405.c405_pricing_id pricingid
                 
                FROM  st405_vendor_active_pricing st405,t301_vendor t301
                WHERE st405.c301_vendor_id = t301.c301_vendor_id
                AND   st405.c205_part_number_id = ttp_open_part_upd.partnum 
                GROUP BY t301.c301_vendor_id, t301.c301_vendor_name, st405.c205_part_number_id,st405.c405_qty_quoted,st405.c405_vendor_moq
                		 ,st405.c405_pricing_id
               	ORDER BY vendorprice
                -- order by vendorprice desc avoided order by desc because it fetch larger cost price values instead fetch min cost price
                ) where rownum = 1;
                
                EXCEPTION WHEN NO_DATA_FOUND THEN
					
			  v_vendor_id := v_no_vendor_id;
			  v_vendor_price := 0;
			  v_quoted_qty := 0;
			  v_vendor_moq := 0;
			  v_pricing_id := NULL;
			  v_vendor_name := v_no_vendor_name;
			  END;
			  END;
				--To call new procedure and update T4057 table
				gm_pkg_oppr_ttp_capa_load_override_vendor.gm_sav_ttp_vendor_capa_summary_dtls
				(ttp_open_part_upd.ttp_detail_id,ttp_open_part_upd.partnum,ttp_open_part_upd.pdesc,
				p_load_date,v_vendor_id,v_vendor_name,v_vendor_price,v_quoted_qty,v_vendor_moq,v_pricing_id,ttp_open_part_upd.total_order_qty,null,null,null,null, 'Y', p_user_id);
         END LOOP;
	--NULL;
	END gm_upd_open_wo_vendor_dtls;
	
 /*************************************************************************
    * Description : Procedure to used to Insert data Details in t4057 table
    * Author   : prabhu vigneshwaran M D 
  *************************************************************************/  
PROCEDURE gm_sav_ttp_vendor_capa_summary_dtls(
p_ttp_detail_id IN t4057_ttp_vendor_capacity_summary_dtls.c4052_ttp_detail_id%type,
p_part_num 		IN t4057_ttp_vendor_capacity_summary_dtls.c205_part_number_id%TYPE,
p_desc 			IN t4057_ttp_vendor_capacity_summary_dtls.c205_part_num_desc%TYPE,
p_load_date 	IN t4057_ttp_vendor_capacity_summary_dtls.c4057_load_date%TYPE,
p_vend_id 		IN t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_id%TYPE,
p_vend_name 	IN t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_name%TYPE,
p_vend_price 	IN t4057_ttp_vendor_capacity_summary_dtls.c402_vendor_price%TYPE,
p_quoted_qty 	IN t4057_ttp_vendor_capacity_summary_dtls.c4057_qty_quoted%TYPE,
p_vendor_moq 	IN t4057_ttp_vendor_capacity_summary_dtls.c4057_vendor_moq%TYPE,
p_pricig_id  	IN t4057_ttp_vendor_capacity_summary_dtls.c4057_pricing_id%TYPE,
p_tot_order_qty IN t4057_ttp_vendor_capacity_summary_dtls.c4057_total_order_qty%TYPE,
p_override_vend_id	IN	t4057_ttp_vendor_capacity_summary_dtls.c301_override_vendor_id%TYPE,
p_vend_split_per	IN	t4057_ttp_vendor_capacity_summary_dtls.c4057_history_split_per%type,
p_override_vend_split_per	IN	t4057_ttp_vendor_capacity_summary_dtls.c4057_other_override_split_per%type,
p_raise_po_qty		IN		t4057_ttp_vendor_capacity_summary_dtls.c4057_raise_po_qty%type,
p_skip_forecast_part_fl IN VARCHAR2,
p_user_id  		IN t4052_ttp_detail.c4052_last_updated_by%TYPE
)
AS
--
v_wo_qty_mon_01 temp_vendor_wo_dtls.temp_wo_qty_mon_01%type;
v_wo_qty_mon_02 temp_vendor_wo_dtls.temp_wo_qty_mon_02%type;
v_wo_qty_mon_03 temp_vendor_wo_dtls.temp_wo_qty_mon_03%type;
v_wo_qty_mon_04 temp_vendor_wo_dtls.temp_wo_qty_mon_04%type;
v_wo_qty_mon_05 temp_vendor_wo_dtls.temp_wo_qty_mon_05%type;
v_wo_qty_mon_06 temp_vendor_wo_dtls.temp_wo_qty_mon_06%type; 

v_dhr_qty_mon_01 temp_vendor_dhr_dtls.temp_dhr_qty_mon_01%type;
v_dhr_qty_mon_02 temp_vendor_dhr_dtls.temp_dhr_qty_mon_02%type;
v_dhr_qty_mon_03 temp_vendor_dhr_dtls.temp_dhr_qty_mon_03%type;
v_dhr_qty_mon_04 temp_vendor_dhr_dtls.temp_dhr_qty_mon_04%type;
v_dhr_qty_mon_05 temp_vendor_dhr_dtls.temp_dhr_qty_mon_05%type;
v_dhr_qty_mon_06 temp_vendor_dhr_dtls.temp_dhr_qty_mon_06%type;

v_history_avg_qty t4057_ttp_vendor_capacity_summary_dtls.c4057_history_avg_qty%type;
v_capacity_id			t4057_ttp_vendor_capacity_summary_dtls.c4057_ttp_vendor_capa_dtls_id%type;
v_t4058_cnt				NUMBER;
v_t4064_cnt				NUMBER;
v4058_order_cost		t4057_ttp_vendor_capacity_summary_dtls.c4057_order_cost%type;
v4058_recommand_qty		t4057_ttp_vendor_capacity_summary_dtls.c4057_recommand_qty%type;
v4058_raise_po_qty		t4057_ttp_vendor_capacity_summary_dtls.c4057_raise_po_qty%type;
BEGIN
	--BEGIN
	 -- to udpdate the data based on vendor/partnumber/loaddate
	
	 UPDATE t4057_ttp_vendor_capacity_summary_dtls 
	    SET c301_vendor_id = p_vend_id
	    ,  c301_vendor_name = get_vendor_name(p_vend_id)
	    ,  c402_vendor_price = p_vend_price 
	    ,  c4057_qty_quoted  = NVL(p_quoted_qty,0)
	    ,  c4057_vendor_moq = NVL(p_vendor_moq,0) 
	    ,  c4057_pricing_id = p_pricig_id
	    ,  c4057_total_order_qty = p_tot_order_qty
	    ,  c4057_override_split_per = p_vend_split_per
	    ,  c301_override_vendor_id = p_override_vend_id
	    ,  c301_override_vendor_name = get_vendor_name(p_override_vend_id)
	    ,  c4057_other_override_split_per = p_override_vend_split_per 
	    ,  c4057_raise_po_qty = p_raise_po_qty
        ,  c4057_calclation_fl = DECODE(NVL(p_tot_order_qty,0),0, NULL,'Y'),
        --
        c4057_raise_po_qty_variance = nvl(c4057_recommand_qty,0) - ROUND((p_vend_split_per/100)*c4057_total_order_qty,0),
		c4057_abs_raise_po_qty_variance = ABS(c4057_recommand_qty - ROUND((p_vend_split_per/100)*c4057_total_order_qty,0)),
		c4057_second_vendor_raise_po_qty = ROUND((NVL(p_override_vend_split_per,0) /100)*p_tot_order_qty,0),
         c4057_last_updated_by             = p_user_id,
         c4057_last_updated_date           = CURRENT_DATE
	    WHERE c301_vendor_id = p_vend_id 
	    AND c205_part_number_id = p_part_num
	    AND c4057_load_date = p_load_date
	    AND c4052_ttp_detail_id = p_ttp_detail_id
	    AND c4057_void_fl IS NULL;
	    
	  IF SQL%NOTFOUND THEN
	  --get the WO data from temp table and stored into local variable.
	  		BEGIN
		  		SELECT temp_wo_qty_mon_01,
		  			   temp_wo_qty_mon_02,
		  			   temp_wo_qty_mon_03,
		  			   temp_wo_qty_mon_04,
		  			   temp_wo_qty_mon_05,
		  			   temp_wo_qty_mon_06 
		  	    INTO v_wo_qty_mon_01,v_wo_qty_mon_02,v_wo_qty_mon_03,
		  	    	 v_wo_qty_mon_04,v_wo_qty_mon_05,v_wo_qty_mon_06
		  		FROM temp_vendor_wo_dtls 
		  		WHERE c205_part_number_id = p_part_num 
		  		AND   c301_vendor_id = p_vend_id ;
		  	EXCEPTION WHEN OTHERS THEN
		  		v_wo_qty_mon_01 := 0;
		  		v_wo_qty_mon_02 := 0;
		  		v_wo_qty_mon_03 := 0;
		  		v_wo_qty_mon_04 := 0;
		  		v_wo_qty_mon_05 := 0;
		  		v_wo_qty_mon_06 := 0;
		  	END;
		  		v_history_avg_qty := (v_wo_qty_mon_01+v_wo_qty_mon_02+v_wo_qty_mon_03+
		  							 v_wo_qty_mon_04+v_wo_qty_mon_05+v_wo_qty_mon_06)/6;
		  	--get the DHR data from DHR temp table
		  	BEGIN
			  	SELECT temp_dhr_qty_mon_01,
			  		   temp_dhr_qty_mon_02,
			  		   temp_dhr_qty_mon_03,
			  		   temp_dhr_qty_mon_04,
			  		   temp_dhr_qty_mon_05,
			  		   temp_dhr_qty_mon_06 
			  	 INTO v_dhr_qty_mon_01,v_dhr_qty_mon_02,v_dhr_qty_mon_03,
			  	 	  v_dhr_qty_mon_04,v_dhr_qty_mon_05,v_dhr_qty_mon_06
			     FROM temp_vendor_dhr_dtls 
			     WHERE c205_part_number_id = p_part_num
			     AND c301_vendor_id = p_vend_id ;

			EXCEPTION WHEN OTHERS THEN
				v_dhr_qty_mon_01 := 0;
				v_dhr_qty_mon_02 := 0;
				v_dhr_qty_mon_03 := 0;
				v_dhr_qty_mon_04 := 0;
				v_dhr_qty_mon_05 := 0;
				v_dhr_qty_mon_06 := 0;	
			END;

 		   -- No vendor record update
 		   INSERT INTO t4057_ttp_vendor_capacity_summary_dtls(
 		               c4052_ttp_detail_id, c301_vendor_id,c301_vendor_name,
 		               c4057_load_date, c205_part_number_id,c205_part_num_desc
		   			  ,c4057_hist_receipt_month_01,c4057_hist_receipt_month_02
      	   			  ,c4057_hist_receipt_month_03, c4057_hist_receipt_month_04, 
      	   			  c4057_hist_receipt_month_05,c4057_hist_receipt_month_06, 
      	   			  c4057_hist_po_month_01, c4057_hist_po_month_02,
      	   			  c4057_hist_po_month_03, c4057_hist_po_month_04, 
      	   			  c4057_hist_po_month_05,c4057_hist_po_month_06, 
      	   			  c4057_history_split_per, c301_second_vendor_id,
      	   			  c301_second_vendor_name,c4057_history_avg_qty,
      	   			  c4057_vendor_moq,c4057_total_order_qty,c4057_override_split_per,
      	   			  c301_override_vendor_id,c301_override_vendor_name,c4057_other_override_split_per,
      	   			  c4057_forecast_month_01,c4057_forecast_month_02,c4057_forecast_month_03,
      	   			  c4057_forecast_month_04,c4057_forecast_month_05,c4057_forecast_month_06,
      	   			  c4057_raise_po_qty,c4057_calclation_fl, c402_vendor_price, c4057_qty_quoted,
      	   			  c4057_pricing_id, c4057_recommand_qty, c4057_raise_po_qty_variance,
      	   			  c4057_abs_raise_po_qty_variance)
      	   		VALUES(p_ttp_detail_id,p_vend_id,decode(p_vend_id,null,null,get_vendor_name(p_vend_id)),
      	   			   p_load_date,p_part_num,p_desc,
      	   			   v_dhr_qty_mon_01,v_dhr_qty_mon_02,
      	   			   v_dhr_qty_mon_03,v_dhr_qty_mon_04,
      	   			   v_dhr_qty_mon_05,v_dhr_qty_mon_06,
      	   			   v_wo_qty_mon_01,v_wo_qty_mon_02,
      	   			   v_wo_qty_mon_03,v_wo_qty_mon_04,
      	   			   v_wo_qty_mon_05,v_wo_qty_mon_06,
      	   			   decode(p_override_vend_id,null,100,0),NULL,
      	   			   NULL,v_history_avg_qty,
      	   			   p_vendor_moq,p_tot_order_qty,p_vend_split_per,
      	   			   p_override_vend_id,decode(p_override_vend_id,null,null,get_vendor_name(p_override_vend_id)),p_override_vend_split_per,
      	   			   0,0,0,0,0,0, p_raise_po_qty, DECODE(NVL(p_tot_order_qty,0),0, NULL,'Y'),p_vend_price, NVL(p_quoted_qty, 0),
      	   			   p_pricig_id, 0, 0 - ROUND((p_vend_split_per/100)*p_tot_order_qty,0),
      	   			   ABS(0 - ROUND((p_vend_split_per/100)*p_tot_order_qty,0)));
      	   			  
      	 	--Insert forecast details for new override vendor to show chart based on parts
      	 BEGIN
      	   	SELECT c4057_ttp_vendor_capa_dtls_id
      	   	  INTO v_capacity_id
      	   	  FROM t4057_ttp_vendor_capacity_summary_dtls
      	   	 WHERE c301_vendor_id = p_vend_id
      	   	   AND c4052_ttp_detail_id = p_ttp_detail_id
      	   	   AND c205_part_number_id = p_part_num
      	   	   AND c4057_load_date = p_load_date
      	   	   AND c4057_void_fl is null;
      	 EXCEPTION WHEN OTHERS THEN
      	 	v_capacity_id := null;
      	 END;
      	 
      	  -- to compare vendor MOQ and update the raise po Qty
        
       gm_pkg_oppr_ld_ttp_by_vendor_capacity_txn. gm_upd_max_venor_moq_vs_raise_po_qty (v_capacity_id, p_user_id);
      

       IF p_skip_forecast_part_fl IS NULL
       THEN
      		INSERT INTO T4063_TTP_FORECAST_BY_PART_DTLS (
      					c4057_ttp_by_vendor_summary_id, c301_vendor_id, c205_part_number_id
      	  			  , c4063_load_date, c4063_history_split_per, c4063_historical_receipt_avg
     	  			  , c4063_fcst_hist_rec_avg_01, c4063_fcst_hist_rec_avg_02, c4063_fcst_hist_rec_avg_03
    	  			  , c4063_fcst_hist_rec_avg_04, c4063_fcst_hist_rec_avg_05, c4063_fcst_hist_rec_avg_06
   	      			  , c4063_pre_hist_rec_avg_01, c4063_pre_hist_rec_avg_02, c4063_pre_hist_rec_avg_03
   	      			  , c4063_cal_hist_receipt_month_01,c4063_cal_hist_receipt_month_02,c4063_cal_hist_receipt_month_03
   	      			  , c4063_cal_hist_receipt_month_04,c4063_cal_hist_receipt_month_05,c4063_cal_hist_receipt_month_06 
   	      			  , c4063_cal_hist_po_month_01,c4063_cal_hist_po_month_02,c4063_cal_hist_po_month_03,c4063_cal_hist_po_month_04
   	      			  , c4063_cal_hist_po_month_05,c4063_cal_hist_po_month_06,c4052_ttp_detail_id
   	      			  , c4063_cal_back_order_qty, c4063_cal_set_priority_pre_mon_qty, c4063_cal_set_priority_01
   	      			  , c4063_cal_set_priority_02 , c4063_cal_set_priority_03 , c4063_cal_set_priority_04
		  			  ,	c4063_cal_set_priority_05 , c4063_cal_set_priority_06 , c4063_total_required_qty
		  			  ,	c4063_sales_replish_01 , c4063_sales_replish_02 , c4063_sales_replish_03
		  			  ,	c4063_sales_replish_04 , c4063_sales_replish_05 , c4063_sales_replish_06
		  			  ,	c4063_sales_replish_07, c4063_sales_replish_08) 
		  	  VALUES (
      					v_capacity_id, p_vend_id, p_part_num,
      					p_load_date,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,p_ttp_detail_id,0,0,0,0,0,0,0,0,
      					0,0,0,0,0,0,0,0,0);
      		
      		END IF;
      		
      		 SELECT count(1) INTO v_t4064_cnt
      		   FROM T4064_TTP_FORECAST_BY_VENDOR_DTLS
      		  WHERE c4064_load_date = p_load_date
      		    AND c301_vendor_id = p_vend_id
      		    AND c4064_void_fl IS NULL;
      		
      		 --Insert new override vendor details to show chart based on vendor 
      		 IF (v_t4064_cnt = 0 AND p_override_vend_id IS NOT NULL) THEN
      		 	INSERT INTO T4064_TTP_FORECAST_BY_VENDOR_DTLS(
      		 				c301_vendor_id, c4064_load_date, c4064_historical_receipt_avg
      	  				  , c4064_fcst_hist_rec_avg_01, c4064_fcst_hist_rec_avg_02, c4064_fcst_hist_rec_avg_03
      	  				  , c4064_fcst_hist_rec_avg_04, c4064_fcst_hist_rec_avg_05, c4064_fcst_hist_rec_avg_06
     	  				  , c4064_pre_hist_rec_avg_01,  c4064_pre_hist_rec_avg_02, c4064_pre_hist_rec_avg_03
     	  				  , c4064_cal_hist_receipt_month_01,c4064_cal_hist_receipt_month_02,c4064_cal_hist_receipt_month_03
     	  				  , c4064_cal_hist_receipt_month_04,c4064_cal_hist_receipt_month_05,c4064_cal_hist_receipt_month_06
     	  				  , c4064_cal_hist_po_month_01,c4064_cal_hist_po_month_02,c4064_cal_hist_po_month_03
     	  				  , c4064_cal_hist_po_month_04,c4064_cal_hist_po_month_05,c4064_cal_hist_po_month_06)
     	  		   VALUES ( p_vend_id,p_load_date,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
      		 END IF;
      		 
      		 SELECT count(1) INTO v_t4058_cnt
      		   FROM T4058_TTP_VENDOR_CAPACITY_SUMMARY
      		  WHERE c4058_load_date = p_load_date
      		    AND c301_vendor_id = p_vend_id
      		    AND c4058_void_fl IS NULL;
      		 
      		 IF (v_t4058_cnt = 0 AND p_override_vend_id IS NOT NULL) THEN
      		 	SELECT sum(NVL(c4057_order_cost, 0)), 
		   			   sum(c4057_recommand_qty), sum(c4057_raise_po_qty)+sum(nvl(c4057_old_po_qty,0))
	  			  INTO v4058_order_cost, v4058_recommand_qty, v4058_raise_po_qty
	  	          FROM T4057_TTP_VENDOR_CAPACITY_SUMMARY_DTLS
	 			 WHERE c301_vendor_id = p_vend_id
	   			   AND c4057_load_date = p_load_date
	   			   AND c4057_void_fl is null
  			  GROUP BY c301_vendor_name;
  			  
      		 	INSERT INTO t4058_ttp_vendor_capacity_summary(
      		 				c4058_load_date,c301_vendor_id,c301_vendor_name,c901_status,
      		 				c4058_recommand_qty,c4058_raise_po_qty,c4058_order_cost,c901_po_type)
      		 		 VALUES (p_load_date,p_vend_id,get_vendor_name(p_vend_id),108860,
      		 		 		 v4058_recommand_qty,v4058_raise_po_qty,v4058_order_cost,3100);
      		 END IF;
	  END IF;
	    
END gm_sav_ttp_vendor_capa_summary_dtls;

/*************************************************************************
    * Description : Procedure to used to Update Edit row Lock Flag in t4057 table
    * Author   : prabhu vigneshwaran M D 
 *************************************************************************/	
PROCEDURE gm_upd_edit_row_lock_fl(
p_load_date 	IN t4057_ttp_vendor_capacity_summary_dtls.c4057_load_date%TYPE,
p_no_vend_id 		IN t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_id%TYPE
)
AS
BEGIN
	--TO Update Edit Row Lock flag for novendor id : 690
	UPDATE t4057_ttp_vendor_capacity_summary_dtls
	SET    c4057_edit_row_lock_fl = 'Y'
	WHERE  c301_vendor_id = p_no_vend_id
	AND   c4057_load_date = p_load_date
	AND   c4057_void_fl IS NULL;
	
END gm_upd_edit_row_lock_fl;
/******************************************************************************
    * Description : Procedure to used to update override sync fl in t4068 table
    * Author   : Agilan Singaravel
 ******************************************************************************/	
PROCEDURE gm_upd_vendor_override_master_sync_dtls(
p_ttp_detail_id IN t4057_ttp_vendor_capacity_summary_dtls.c4052_ttp_detail_id%type,
p_sync_fl 		IN t4068_ttp_capa_vendor_override_dtls.c4068_override_sync_fl%TYPE,
p_user_id  		IN t4068_ttp_capa_vendor_override_dtls.c4068_updated_by%TYPE
)
AS
BEGIN
	
	UPDATE t4068_ttp_capa_vendor_override_dtls
	   SET c4068_override_sync_fl = p_sync_fl
	     , c4068_updated_by = p_user_id
	     , c4068_updated_date = CURRENT_DATE
	 WHERE c4068_void_fl IS NULL;
END gm_upd_vendor_override_master_sync_dtls;

/**************************************************************************************************
    * Description : Procedure to used to Insert or update vendor details from t4068 to t4057 table
    * Author   : Agilan Singaravel
 **************************************************************************************************/	
PROCEDURE gm_upd_override_vendor_dtls(
p_load_date 	IN t4057_ttp_vendor_capacity_summary_dtls.c4057_load_date%TYPE,
p_user_id  		IN t4068_ttp_capa_vendor_override_dtls.c4068_updated_by%TYPE
)
AS
v_ttp_detail_id		t4057_ttp_vendor_capacity_summary_dtls.c4052_ttp_detail_id%type;
v_total_order_qty	t4057_ttp_vendor_capacity_summary_dtls.c4057_total_order_qty%type;
v_vendor_moq		st405_vendor_active_pricing.c405_vendor_moq%type;
v_vendor_price		st405_vendor_active_pricing.c405_vendor_price%type;
v_qty_quoted		st405_vendor_active_pricing.c405_qty_quoted%type;
v_pricing_id		st405_vendor_active_pricing.c405_pricing_id%type;
v_other_vendor_moq		st405_vendor_active_pricing.c405_vendor_moq%type;
v_other_vendor_price		st405_vendor_active_pricing.c405_vendor_price%type;
v_other_qty_quoted		st405_vendor_active_pricing.c405_qty_quoted%type;
v_other_pricing_id		st405_vendor_active_pricing.c405_pricing_id%type;
CURSOR fetch_ven_parts
IS
	SELECT c205_part_number_id partnum, c301_vendor_id venid
		 , c4068_split_per splitper, c301_other_vendor_id othervenid
		 , c4068_other_split_per othersplitper
	  FROM T4068_TTP_CAPA_VENDOR_OVERRIDE_DTLS
	 WHERE c4068_override_sync_fl is null
	   AND c4068_void_fl is null;
	   
	-- to get the ttp detils id - based on part and load date:
	CURSOR ttp_by_vendor_part_cur(partnum	IN	t4068_ttp_capa_vendor_override_dtls.c205_part_number_id%type)
	IS
		SELECT c4052_ttp_detail_id ttp_dtl_id
		  FROM T4057_TTP_VENDOR_CAPACITY_SUMMARY_DTLS
         WHERE c4057_load_date = p_load_date
  		   AND c205_part_number_id = partnum
           AND C4057_VOID_FL  IS NULL           
	  GROUP BY c4052_ttp_detail_id;	
BEGIN
	
	FOR v_ven_dtls IN fetch_ven_parts
	LOOP
	
	-- to loop ttp parts cur
	FOR v_ttp_part IN ttp_by_vendor_part_cur(v_ven_dtls.partnum)
	LOOP
		v_ttp_detail_id := v_ttp_part.ttp_dtl_id;
		--
	BEGIN
		SELECT c4057_total_order_qty
		  INTO v_total_order_qty
   		  FROM T4057_TTP_VENDOR_CAPACITY_SUMMARY_DTLS
         WHERE c4057_load_date = p_load_date
  		   AND c205_part_number_id = v_ven_dtls.partnum
		   AND c4052_ttp_detail_id = v_ttp_detail_id
           AND C4057_VOID_FL  IS NULL;
      EXCEPTION WHEN OTHERS THEN
      	v_total_order_qty := 0;
      END;
	  
	  BEGIN
	  	SELECT c405_vendor_moq,c405_vendor_price,c405_qty_quoted,c405_pricing_id
	      INTO v_vendor_moq,v_vendor_price,v_qty_quoted,v_pricing_id
	      FROM ST405_VENDOR_ACTIVE_PRICING
	     WHERE c301_vendor_id = v_ven_dtls.venid
	       AND c205_part_number_id = v_ven_dtls.partnum;
	  EXCEPTION WHEN OTHERS THEN
	  		v_vendor_moq := 0;
	  		v_vendor_price := 0;
	  		v_qty_quoted := 0;
	  		v_pricing_id := 0;
	  END;
	  
	  BEGIN
	  	SELECT c405_vendor_moq,c405_vendor_price,c405_qty_quoted,c405_pricing_id
	      INTO v_other_vendor_moq,v_other_vendor_price,v_other_qty_quoted,v_other_pricing_id
	      FROM ST405_VENDOR_ACTIVE_PRICING
	     WHERE c301_vendor_id = v_ven_dtls.othervenid
	       AND c205_part_number_id = v_ven_dtls.partnum;
	  EXCEPTION WHEN OTHERS THEN
	  		v_other_vendor_moq := 0;
	  		v_other_vendor_price := 0;
	  		v_other_qty_quoted := 0;
	  		v_other_pricing_id := 0;
	  END;
	  
	  gm_lock_part_by_ttp_capa_summary(v_ven_dtls.partnum,v_ven_dtls.venid,v_ven_dtls.othervenid,p_load_date,'Y',p_user_id);
	  --gm_lock_part_by_ttp_capa_summary(v_ven_dtls.partnum,v_ven_dtls.othervenid,v_ven_dtls.othervenid,p_load_date,'Y',p_user_id);
	  
	  gm_sav_ttp_vendor_capa_summary_dtls(v_ttp_detail_id,v_ven_dtls.partnum,get_partnum_desc(v_ven_dtls.partnum),p_load_date,v_ven_dtls.venid,'',v_vendor_price,v_qty_quoted,v_vendor_moq,v_pricing_id,v_total_order_qty,v_ven_dtls.othervenid,v_ven_dtls.splitper,v_ven_dtls.othersplitper,((v_ven_dtls.splitper/100)*v_total_order_qty), NULL, p_user_id);
	  gm_sav_ttp_vendor_capa_summary_dtls(v_ttp_detail_id,v_ven_dtls.partnum,get_partnum_desc(v_ven_dtls.partnum),p_load_date,v_ven_dtls.othervenid,'',v_other_vendor_price,v_other_qty_quoted,v_other_vendor_moq,v_other_pricing_id,v_total_order_qty,v_ven_dtls.venid,v_ven_dtls.othersplitper,v_ven_dtls.splitper,((v_ven_dtls.othersplitper/100)*v_total_order_qty), NULL, p_user_id);

	END LOOP; -- end loop ttp parts cur
	
	END LOOP;
	--
	-- After override the vendor details to update the calculation flag as null
	
	   UPDATE t4057_ttp_vendor_capacity_summary_dtls
	    SET c4057_calclation_fl   = NULL
	     , c4057_last_updated_by = p_user_id
	     , c4057_last_updated_date = CURRENT_DATE
	    WHERE  c4057_calclation_fl   = 'Y'
	    and c4057_load_date = p_load_date
	    AND c4057_void_fl        IS NULL;
	    
END gm_upd_override_vendor_dtls;

/**********************************************************************
    * Description : Procedure to used to update edit row lock fl to t4057 table
    * Author   : Agilan Singaravel
 ***********************************************************************/	
PROCEDURE gm_lock_part_by_ttp_capa_summary(
p_part_num 		IN t4057_ttp_vendor_capacity_summary_dtls.c205_part_number_id%TYPE,
p_vend_id 		IN t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_id%TYPE,
p_override_vend_id 		IN t4057_ttp_vendor_capacity_summary_dtls.c301_override_vendor_id%TYPE,
p_load_date 	IN t4057_ttp_vendor_capacity_summary_dtls.c4057_load_date%TYPE,
p_lock_fl		IN t4057_ttp_vendor_capacity_summary_dtls.c4057_edit_row_lock_fl%type,
p_user_id  		IN t4057_ttp_vendor_capacity_summary_dtls.c4057_last_updated_by%TYPE
)
AS
BEGIN
		
	UPDATE t4057_ttp_vendor_capacity_summary_dtls
	   SET c4057_edit_row_lock_fl = p_lock_fl
	     , c4057_override_split_per = DECODE(p_lock_fl,'Y','0',c4057_override_split_per)
         , c4057_raise_po_qty = DECODE(p_lock_fl,'Y','0',c4057_raise_po_qty)
	     , c4057_last_updated_by = p_user_id
	     , c4057_last_updated_date = CURRENT_DATE
	 WHERE c205_part_number_id = p_part_num
	   AND c301_vendor_id NOT IN (p_vend_id,p_override_vend_id)
	   AND c4057_load_date = p_load_date
	   AND c4057_void_fl is null;
	
END gm_lock_part_by_ttp_capa_summary;
/*************************************************************************
    * Description : Procedure to used to fetch data from t4059 table which is 
    * 				used to show active price vendors based on part number
    * Author      : Agilan Singaravel 
    *************************************************************************/
	PROCEDURE gm_fch_vendor_price_by_part(
			p_ttp_capa_id         IN   t4057_ttp_vendor_capacity_summary_dtls.c4057_ttp_vendor_capa_dtls_id%TYPE,
            p_company_id 		  IN   t1900_company.c1900_company_id%TYPE,
            p_out_other_ven_dtls  OUT  clob
    )
    AS
    v_company_id        t1900_company.c1900_company_id%TYPE;
    v_part_num			t4057_ttp_vendor_capacity_summary_dtls.c205_part_number_id%type;
    v_vendor_id			t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_id%type;
    v_other_vendor_id	t4057_ttp_vendor_capacity_summary_dtls.c301_second_vendor_id%type;
    v_override_vendor_id	t4057_ttp_vendor_capacity_summary_dtls.c301_override_vendor_id%type;
    v_po_fl					t4057_ttp_vendor_capacity_summary_dtls.c4057_po_fl%type;
    --
    v_load_date   DATE;
    v_non_po_cnt NUMBER;
    BEGIN
	    --SELECT get_compid_frm_cntx() INTO v_company_id  FROM dual;
	  BEGIN
	    SELECT c301_vendor_id, c205_part_number_id, c301_second_vendor_id, c301_override_vendor_id
	    	, c4057_load_date
	      INTO v_vendor_id, v_part_num, v_other_vendor_id, v_override_vendor_id
	      	, v_load_date
	      FROM t4057_ttp_vendor_capacity_summary_dtls
	     WHERE c4057_ttp_vendor_capa_dtls_id = p_ttp_capa_id
	       AND c4057_void_fl IS NULL;
	  EXCEPTION WHEN OTHERS THEN
	  		v_vendor_id := null;
	  		v_part_num := null;
	  		v_other_vendor_id := null;
	  		v_override_vendor_id := null;
	  END;
	  
	  --26240821	PO Generation In-Progress
	--26240822	PO Generated
	--26240823	PO Generation Failed
	--26241143	PO Regenerate
	
	  SELECT count (1) INTO v_non_po_cnt
   FROM t4058_ttp_vendor_capacity_summary
  WHERE c4058_load_date = v_load_date
    AND c301_vendor_id  = NVL(v_other_vendor_id, NVL(v_override_vendor_id, 0))
    AND c901_status IN (26240821, 26240822, 26240823, 26241143)
    AND c4058_void_fl  IS NULL ;
    
	  -- to check only PO Generated
	  IF v_non_po_cnt = 0 THEN
	    SELECT JSON_ARRAYAGG(
 			    	JSON_OBJECT (
      				'vendorid'     value t301.c301_vendor_id,
      				'vendorname'   value t301.c301_vendor_name
   		 	   )ORDER BY upper(t301.c301_vendor_name)  returning CLOB)
      	  INTO p_out_other_ven_dtls
	      FROM T405_VENDOR_PRICING_DETAILS t405,T301_VENDOR t301
		 WHERE t405.c301_vendor_id = t301.c301_vendor_id
		   AND t405.c405_active_fl = 'Y'
		   AND t405.c405_void_fl is null
		   AND t301.c301_vendor_id not in (v_vendor_id,NVL(v_other_vendor_id, 0), NVL(v_override_vendor_id, 0))
		   AND t301.c1900_company_id = p_company_id
		   AND t405.c205_part_number_id = v_part_num;
	  ELSE
	  	p_out_other_ven_dtls := 'N';
	  END IF;
	  --GROUP BY t301.c301_vendor_id,t301.c301_vendor_name;
    END gm_fch_vendor_price_by_part;

/*********************************************************************************************
    * Description : Procedure to used update PO regenate fl and new raise po qty and old qty
    * Author      : Agilan Singaravel 
**********************************************************************************************/
PROCEDURE gm_upd_po_regenerate_dtls(
p_ttp_capa_id         IN   t4057_ttp_vendor_capacity_summary_dtls.c4057_ttp_vendor_capa_dtls_id%TYPE,
p_vendor_id 		  IN   t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_id%TYPE,
p_part_num 			  IN   t4057_ttp_vendor_capacity_summary_dtls.c205_part_number_id%TYPE,
p_raise_po_qty		  IN   t4057_ttp_vendor_capacity_summary_dtls.c4057_raise_po_qty%type,
p_new_raise_po_qty	  IN   t4057_ttp_vendor_capacity_summary_dtls.c4057_raise_po_qty%type,
p_regenerate_po_fl	  IN   t4057_ttp_vendor_capacity_summary_dtls.c4057_regen_po_fl%type,
p_load_date			  IN   DATE,
p_user_id  			  IN   t4057_ttp_vendor_capacity_summary_dtls.c4057_last_updated_by%TYPE
)
AS
v_vendor_capa_summary_id NUMBER;
v_wo_qty number;
BEGIN
	
	-- 1 to get the Vendor capacity summary id
BEGIN	
 SELECT c4058_ttp_vendor_capa_summary_id
   INTO v_vendor_capa_summary_id
   FROM t4058_ttp_vendor_capacity_summary
  WHERE c301_vendor_id  = p_vendor_id
    AND c4058_load_date = p_load_date
    AND c4058_void_fl IS NULL;
 EXCEPTION WHEN OTHERS
 THEN
 	v_vendor_capa_summary_id := NULL;
 END;
	
 --2 to get the PO and purchase order details
 
BEGIN
		SELECT sum(t402.c402_qty_ordered)
			INTO v_wo_qty
	  FROM T402_WORK_ORDER t402, t401_purchase_order t401, t4065_ttp_vendor_po_dtls t4065
	 WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
	 	AND t402.c205_part_number_id = p_part_num
	 	AND t4065.c401_purchase_ord_id = t402.c401_purchase_ord_id
	 	AND t4065.c4058_ttp_vendor_capa_summary_id = v_vendor_capa_summary_id
	    AND t4065.c4065_void_fl IS NULL
	    AND t402.c402_void_fl IS NULL
	    AND t401.c401_void_fl IS NULL;
EXCEPTION WHEN OTHERS
THEN
v_wo_qty := 0;
END;
	
-- to update the old PO qty

	UPDATE T4057_TTP_VENDOR_CAPACITY_SUMMARY_DTLS
	   SET c4057_old_po_qty = v_wo_qty
	     , c4057_raise_po_qty = p_new_raise_po_qty - v_wo_qty
	     , c4057_regen_po_fl = p_regenerate_po_fl
	     , c4057_last_updated_by = p_user_id
	     , c4057_last_updated_date = CURRENT_DATE
	 WHERE c4057_ttp_vendor_capa_dtls_id = p_ttp_capa_id
	   AND c4057_void_fl IS NULL;
	   
END gm_upd_po_regenerate_dtls;

/*********************************************************************************
    * Description : Procedure to used update or insert po details for excess qty
    * Author      : Agilan Singaravel 
**********************************************************************************/
PROCEDURE gm_sav_excess_po_dtls(
p_ttp_dtl_id		  IN   t4057_ttp_vendor_capacity_summary_dtls.c4052_ttp_detail_id%TYPE,
p_vendor_id			  IN   t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_id%type,
p_part_num			  IN   t4057_ttp_vendor_capacity_summary_dtls.c205_part_number_id%type,
p_po_qty			  IN   t4069_ttp_capa_excess_po_dtls.c4069_po_regen_qty%type,
p_total_qty			  IN   t4069_ttp_capa_excess_po_dtls.c4069_total_qty%type,
p_user_id  			  IN   t4057_ttp_vendor_capacity_summary_dtls.c4057_last_updated_by%TYPE,
p_ttp_capa_id         IN   t4057_ttp_vendor_capacity_summary_dtls.c4057_ttp_vendor_capa_dtls_id%TYPE,
p_load_date			  IN   t4069_ttp_capa_excess_po_dtls.c4069_load_date%type
)
AS
v_work_order_id			t402_work_order.c402_work_order_id%type;
v_po_id					t402_work_order.c401_purchase_ord_id%type;
v_wo_qty				T402_WORK_ORDER.c402_qty_ordered%type;
v_status				NUMBER;
----
v_old_wo_qty NUMBER;
v_po_regen_qty NUMBER;
BEGIN
	--109021	Open
--109024	Completed
	--
	--
  SELECT NVL (c4057_old_po_qty, 0), (p_total_qty - NVL (c4057_old_po_qty, 0)), 109021
   INTO v_old_wo_qty, v_po_regen_qty, v_status
   FROM t4057_ttp_vendor_capacity_summary_dtls
  WHERE c4057_ttp_vendor_capa_dtls_id = p_ttp_capa_id
    AND c4057_void_fl                IS NULL;
	
	 -- to update the void flag (negative qty)
     UPDATE t4069_ttp_capa_excess_po_dtls
    SET c4069_void_fl                     = 'Y', c4069_last_updated_by = p_user_id, c4069_last_updated_date = CURRENT_DATE
      WHERE c4057_ttp_vendor_capa_dtls_id = p_ttp_capa_id
        AND c901_status                  <> 109024 -- completed
        --AND c4069_po_regen_qty <= 0
        AND c4069_void_fl IS NULL;
    
        -- to avoid zero qty
    IF v_po_regen_qty <> 0
    THEN
		-- negative qty???
		IF v_po_regen_qty < 0 
		THEN
			gm_reduce_excess_po_dtls (p_ttp_dtl_id, p_vendor_id, p_part_num, v_old_wo_qty, p_total_qty, v_status, v_po_regen_qty, p_user_id, p_ttp_capa_id, p_load_date);
		ELSE
		
			gm_sav_ttp_capa_excess_po_dtls (p_ttp_dtl_id, p_vendor_id, p_part_num, v_old_wo_qty, p_total_qty, NULL, v_status, v_po_regen_qty, p_user_id, p_ttp_capa_id, p_load_date, NULL, NULL);
		END IF; -- v_po_regen_qty negative check
		--
	END IF; --v_po_regen_qty not equal to zero
	
 END gm_sav_excess_po_dtls;

/*********************************************************************************
    * Description : Procedure to used update or insert po details for excess qty
    * Author      : Agilan Singaravel 
**********************************************************************************/
PROCEDURE gm_process_vendor_summary_dtls(
p_load_date 	IN 	 t4057_ttp_vendor_capacity_summary_dtls.c4057_load_date%TYPE,
p_user_id  		IN   t4057_ttp_vendor_capacity_summary_dtls.c4057_last_updated_by%TYPE
)
AS
CURSOR vendor_list_cur
IS
	SELECT t4057.c301_vendor_id vendorid
	  FROM T4057_TTP_VENDOR_CAPACITY_SUMMARY_DTLS t4057, 
	  	   T4058_TTP_VENDOR_CAPACITY_SUMMARY t4058
	 WHERE t4058.c4058_load_date = t4057.c4057_load_date
	   AND t4057.c4057_calclation_fl = 'Y'
	   AND t4057.c4057_regen_po_fl = 'TBG'
	   AND t4058.c4058_load_date = p_load_date
	   AND t4058.c901_status not in ('26240822','26240823')
	   AND t4057.c4057_void_fl is null
	   AND t4058.c4058_void_fl is null
  GROUP BY t4057.c301_vendor_id;
	  
BEGIN
	FOR v_vendor_list_cur IN vendor_list_cur
	LOOP
		gm_upd_vendor_capa_summary(v_vendor_list_cur.vendorid,p_load_date,p_user_id);
	END LOOP;
END gm_process_vendor_summary_dtls;
/*********************************************************************************
    * Description : This procedure used to update the PO status for the vendor
    * Author      : Agilan Singaravel 
**********************************************************************************/
PROCEDURE gm_upd_vendor_capa_summary(
p_vendor_id		IN	 t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_id%type,
p_load_date 	IN 	 t4057_ttp_vendor_capacity_summary_dtls.c4057_load_date%TYPE,
p_user_id  		IN   t4057_ttp_vendor_capacity_summary_dtls.c4057_last_updated_by%TYPE
)
AS
v_status		t4058_ttp_vendor_capacity_summary.c901_status%type;
v_count			NUMBER;
v_vendor_name	t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_name%type;
v_order_cost	t4057_ttp_vendor_capacity_summary_dtls.c4057_order_cost%type;
v_recommand_qty	t4057_ttp_vendor_capacity_summary_dtls.c4057_recommand_qty%type;
v_raise_po_qty	t4057_ttp_vendor_capacity_summary_dtls.c4057_raise_po_qty%type;
BEGIN
	SELECT c301_vendor_name, sum(c4057_order_cost), 
		   sum(c4057_recommand_qty), sum(c4057_raise_po_qty)+sum(nvl(c4057_old_po_qty,0))
	  INTO v_vendor_name, v_order_cost, v_recommand_qty, v_raise_po_qty
	  FROM T4057_TTP_VENDOR_CAPACITY_SUMMARY_DTLS
	 WHERE c301_vendor_id = p_vendor_id
	   AND c4057_load_date = p_load_date
	   AND c4057_void_fl is null
  GROUP BY c301_vendor_name;
	   
	BEGIN
		SELECT c901_status
	  	  INTO v_status
	  	  FROM T4058_TTP_VENDOR_CAPACITY_SUMMARY
	 	 WHERE c4058_load_date = p_load_date
	   	   AND c301_vendor_id = p_vendor_id
	   	   AND c4058_void_fl is null;
	EXCEPTION WHEN OTHERS THEN
		v_status := NULL;
    END;
    
    SELECT count(1)
      INTO v_count
      FROM T4057_TTP_VENDOR_CAPACITY_SUMMARY_DTLS
     WHERE NVL(c4057_regen_po_fl, '-999') = 'TBG'
       AND c301_vendor_id = p_vendor_id
	   AND c4057_load_date = p_load_date
	   AND c4057_void_fl is null;

	  --To get postive data count (t4057) and PO Generated then update status to PO Regen
	   
	IF (v_count <> 0 AND v_status = 26240822) THEN --PO Generated
		UPDATE T4058_TTP_VENDOR_CAPACITY_SUMMARY
		   SET c901_status = 26241143  --PO Regenerate
		     , c4058_last_updated_by = p_user_id
		     , c4058_last_updated_date = CURRENT_DATE
		 WHERE c301_vendor_id = p_vendor_id
		   AND c4058_load_date = p_load_date
		   AND c4058_void_fl is null;
    END IF;
  
    IF v_status IS NULL THEN
    	INSERT INTO T4058_TTP_VENDOR_CAPACITY_SUMMARY(
    				c301_vendor_id,c301_vendor_name,c4058_load_date,c901_status,
    				c4058_recommand_qty,c4058_raise_po_qty,c4058_order_cost,c901_po_type)
    	     VALUES (p_vendor_id,v_vendor_name,p_load_date,108860,
    	     		 v_recommand_qty,v_raise_po_qty, v_order_cost,3100);  
    END IF;
    
END gm_upd_vendor_capa_summary;

/*********************************************************************************
    * Description : Procedure used to update or insert excess po details
    * Author      : mmuthusamy
**********************************************************************************/
PROCEDURE gm_sav_ttp_capa_excess_po_dtls(
p_ttp_dtl_id		  IN   t4057_ttp_vendor_capacity_summary_dtls.c4052_ttp_detail_id%TYPE,
p_vendor_id			  IN   t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_id%type,
p_part_num			  IN   t4057_ttp_vendor_capacity_summary_dtls.c205_part_number_id%type,
p_po_qty			  IN   t4069_ttp_capa_excess_po_dtls.c4069_po_regen_qty%type,
p_total_qty			  IN   t4069_ttp_capa_excess_po_dtls.c4069_total_qty%type,
p_wo_qty			  IN t4069_ttp_capa_excess_po_dtls.c402_wo_qty%type,
p_status			  IN t4069_ttp_capa_excess_po_dtls.c901_status%type,
p_po_regen_qty        IN t4069_ttp_capa_excess_po_dtls.c4069_po_regen_qty%type,
p_user_id  			  IN   t4057_ttp_vendor_capacity_summary_dtls.c4057_last_updated_by%TYPE,
p_ttp_capa_id         IN   t4057_ttp_vendor_capacity_summary_dtls.c4057_ttp_vendor_capa_dtls_id%TYPE,
p_load_date			  IN   t4069_ttp_capa_excess_po_dtls.c4069_load_date%type,
p_wo_id IN VARCHAR2,
p_po_id IN VARCHAR2
)
AS
v_work_order_id			t402_work_order.c402_work_order_id%type;
v_po_id					t402_work_order.c401_purchase_ord_id%type;
v_wo_qty				T402_WORK_ORDER.c402_qty_ordered%type;
v_status				NUMBER;

BEGIN

	
	UPDATE t4069_ttp_capa_excess_po_dtls
	   SET c301_vendor_id = p_vendor_id
	     , c4069_old_po_qty = p_po_qty
	     , c4069_total_qty = p_total_qty
	     , c4069_po_regen_qty = p_po_regen_qty
	     , c4069_last_updated_by = p_user_id
	     , c4069_last_updated_date = CURRENT_DATE
	     , c402_work_order_id = p_wo_id
	     , c402_wo_qty = p_wo_qty
	     , c401_purchase_ord_id = p_po_id
	 WHERE c4057_ttp_vendor_capa_dtls_id = p_ttp_capa_id
	   AND c901_status <> 109024 -- completed
	   AND c4069_po_regen_qty > 0
	   AND c4069_void_fl is null;
	   
	IF SQL%NOTFOUND THEN
		INSERT INTO t4069_ttp_capa_excess_po_dtls(
					c4052_ttp_detail_id,c301_vendor_id,c205_part_number_id,
					c4069_total_qty,c4069_old_po_qty,c4069_po_regen_qty,
					c4057_ttp_vendor_capa_dtls_id,c4069_load_date,
					c402_work_order_id,c401_purchase_ord_id, c901_status, c402_wo_qty)
			 VALUES (p_ttp_dtl_id,p_vendor_id,p_part_num,
			 		p_total_qty,p_po_qty,p_po_regen_qty,
			 		p_ttp_capa_id,p_load_date,p_wo_id,p_po_id, p_status, p_wo_qty);
	END IF;
END gm_sav_ttp_capa_excess_po_dtls;

/*********************************************************************************
    * Description : Procedure to used update negative Qty (each line WO and PO details)
    * Author      : mmuthusamy
**********************************************************************************/
PROCEDURE gm_reduce_excess_po_dtls (
        p_ttp_dtl_id   IN t4057_ttp_vendor_capacity_summary_dtls.c4052_ttp_detail_id%TYPE,
        p_vendor_id    IN t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_id%type,
        p_part_num     IN t4057_ttp_vendor_capacity_summary_dtls.c205_part_number_id%type,
        p_po_qty       IN t4069_ttp_capa_excess_po_dtls.c4069_po_regen_qty%type,
        p_total_qty    IN t4069_ttp_capa_excess_po_dtls.c4069_total_qty%type,
        p_status       IN t4069_ttp_capa_excess_po_dtls.c901_status%type,
        p_po_regen_qty IN t4069_ttp_capa_excess_po_dtls.c4069_po_regen_qty%type,
        p_user_id      IN t4057_ttp_vendor_capacity_summary_dtls.c4057_last_updated_by%TYPE,
        p_ttp_capa_id  IN t4057_ttp_vendor_capacity_summary_dtls.c4057_ttp_vendor_capa_dtls_id%TYPE,
        p_load_date    IN t4069_ttp_capa_excess_po_dtls.c4069_load_date%type)
AS
    --
    v_vendor_capa_summary_id t4058_ttp_vendor_capacity_summary.c4058_ttp_vendor_capa_summary_id%TYPE;
    v_po_gen_qty NUMBER;
    --
    CURSOR wo_dtls_cur
    IS
         SELECT t402.c402_work_order_id wo_id, t402.c401_purchase_ord_id po_id, t402.c402_qty_ordered qty
            --INTO v_work_order_id, v_po_id, v_wo_qty, v_status
           FROM t402_work_order t402, t401_purchase_order t401, t4065_ttp_vendor_po_dtls t4065
          WHERE t401.c401_purchase_ord_id              = t402.c401_purchase_ord_id
            AND t402.c205_part_number_id               = p_part_num
            AND t4065.c401_purchase_ord_id             = t402.c401_purchase_ord_id
            AND t4065.c4058_ttp_vendor_capa_summary_id = v_vendor_capa_summary_id
            AND t4065.c4065_void_fl                   IS NULL
            AND t402.c402_void_fl                     IS NULL
            AND t401.c401_void_fl                     IS NULL;
BEGIN
	
    -- 1. to get the vendor capa summary id
     SELECT c4058_ttp_vendor_capa_summary_id, ABS (p_po_regen_qty)
       INTO v_vendor_capa_summary_id, v_po_gen_qty
       FROM t4058_ttp_vendor_capacity_summary
      WHERE c4058_load_date = p_load_date
        AND c301_vendor_id  = p_vendor_id
        AND c4058_void_fl  IS NULL;
        
    
   
        
    FOR wo_dtls IN wo_dtls_cur
    LOOP
        IF v_po_gen_qty      > 0 THEN
            IF (v_po_gen_qty > wo_dtls.qty) THEN
                
                --
                v_po_gen_qty := v_po_gen_qty - wo_dtls.qty;
                --
                gm_sav_ttp_capa_excess_po_dtls (p_ttp_dtl_id, p_vendor_id, p_part_num, wo_dtls.qty, p_total_qty,
                wo_dtls.qty, p_status, wo_dtls.qty * - 1, p_user_id, p_ttp_capa_id, p_load_date, wo_dtls.wo_id,
                wo_dtls.po_id) ;
                --
            ELSE
                
                gm_sav_ttp_capa_excess_po_dtls (p_ttp_dtl_id, p_vendor_id, p_part_num, wo_dtls.qty, p_total_qty,
                wo_dtls.qty, p_status, v_po_gen_qty * - 1, p_user_id, p_ttp_capa_id, p_load_date, wo_dtls.wo_id,
                wo_dtls.po_id) ;
                --
                v_po_gen_qty := 0;
            END IF;
        END IF; -- end of v_po_gen_qty > 0
    END LOOP;
END gm_reduce_excess_po_dtls;

	  
END gm_pkg_oppr_ttp_capa_load_override_vendor;
/