--@C:\PMT\bit\SpineIT-ERP\Datawarehouse\Database\DMO\Packages\TTPVendor\gm_pkg_oppr_ld_ttp_by_vendor_lock.bdy;
CREATE OR REPLACE
PACKAGE BODY gm_pkg_oppr_ld_ttp_by_vendor_lock
IS
  /*************************************************************************
  * Description : Procedure to used to Upate the status as lock in progress
  *                to t4058 and t4059 table
  * Author      : Tamizhthangam Ramasamy
  *************************************************************************/
PROCEDURE gm_upd_ttp_by_vendor_lock(
    p_input_str IN CLOB,
    p_load_date IN VARCHAR2,
    p_user_id   IN t4059_ttp_capacity_summary.C4059_LOCKED_BY%TYPE,
    p_err_dtls OUT CLOB )
AS
  v_load_date DATE;
  v_ttp_dtl_id t4059_ttp_capacity_summary.C4052_TTP_DETAIL_ID %TYPE;
  v_err_dtls   VARCHAR2(1000);
  v_err CLOB;
  
  CURSOR ttp_id_cur
  IS
    SELECT c205_part_number_id ttpid FROM my_t_part_list;
BEGIN
  -- set the clobe value to context
  my_context.set_my_cloblist(p_input_str);
  SELECT to_date(p_load_date ,'MM/yyyy') INTO v_load_date FROM DUAL;
  
  FOR ttp_id IN ttp_id_cur
  LOOP
    gm_validate_ttp_by_vendor_lock(ttp_id.ttpid,v_load_date,v_err_dtls);
    
    IF v_err_dtls IS NOT NULL THEN
      v_err       := v_err || v_err_dtls || ',';
    END IF;
    
    IF v_err_dtls IS NULL AND v_err IS NULL THEN
      -- update the staus as Lock In porogress in t4059_ttp_capacity_summary Table
      UPDATE t4059_ttp_capacity_summary
      SET c901_status     = 108863,-- Lock In progress
        c4059_last_updated_by   = p_user_id,
        c4059_last_updated_date = CURRENT_DATE
      WHERE c4050_ttp_id    = ttp_id.ttpid
      AND   c4059_load_date = v_load_date
      AND   c4059_void_fl  IS NULL;
  
    END IF;
  END LOOP;
  
  IF v_err IS NOT NULL THEN
    p_err_dtls  := v_err;
  END IF;
  
END gm_upd_ttp_by_vendor_lock;
/*************************************************************************
* Description : Procedure to used to validate the TTP Status is not in locked status 
*                if it is in locked status send error details
* Author      : Tamizhthangam Ramasamy
*************************************************************************/
PROCEDURE gm_validate_ttp_by_vendor_lock(
    p_ttp_id    IN t4059_ttp_capacity_summary.c4050_ttp_id%TYPE,
    p_load_date IN t4059_ttp_capacity_summary.c4059_load_date%TYPE,
    p_err_dtls OUT VARCHAR2 )
AS
  v_status_cnt NUMBER;
BEGIN
  
  SELECT COUNT (1)
    INTO v_status_cnt
    FROM t4059_ttp_capacity_summary
   WHERE c4050_ttp_id  = p_ttp_id
     AND c901_status   <> 108865  --locked status
     AND c4059_load_date = p_load_date
     AND c4059_void_fl  IS NULL;
   --Check the status count equal to zero if ttp id is already locked.if it is locked means disable check box 
  IF v_status_cnt = 0 THEN
    p_err_dtls       := p_ttp_id;
  END IF;
  --if status count is greater than zero which means ttp is in approved status 
  --so change the status approved to lock in progress based on status count
  IF v_status_cnt > 0 THEN
    p_err_dtls  := '';
  END IF;
  
END gm_validate_ttp_by_vendor_lock;

/*********************************************************************************************
* Description : Procedure to used to Upate the TTP vendor  status as locked or lock failed
*                to t4058 and t4059 table
* Author      : Tamizhthangam Ramasamy
*************************************************************************************************/
--	PROCEDURE gm_upd_ttp_by_vendor_status(
--    p_ttp_dtl_id    IN CLOB,
--    p_load_date IN VARCHAR2,
--    p_user_id   IN t4059_ttp_capacity_summary.C4059_LOCKED_BY%TYPE )
--AS
--  v_load_date DATE;
--  v_cnt NUMBER;
--  
--  CURSOR ttp_dtl_id_cur
--  IS
--    SELECT c205_part_number_id ttpdtlid FROM my_t_part_list WHERE c205_part_number_id IS NOT NULL;
--BEGIN
--  -- set the clobe value to context
--  my_context.set_my_cloblist(p_ttp_dtl_id);
--  SELECT to_date(p_load_date ,'MM/yyyy') INTO v_load_date FROM DUAL;
--  
--  FOR ttp_dtl_id IN ttp_dtl_id_cur
--  LOOP
--      
--      -- update the staus as locked in t4059_ttp_capacity_summary Table
--      UPDATE t4059_ttp_capacity_summary
--         SET c901_status      = 108865,-- Locked status
--             c4059_locked_by  = p_user_id,
--            c4059_locked_date = CURRENT_DATE
--      WHERE c4052_ttp_detail_id  = ttp_dtl_id.ttpdtlid
--        AND c901_status       IN (108863,108864)--lock in progress,lock failed
--        AND c4059_void_fl  IS NULL;  
--  END LOOP;
--END gm_upd_ttp_by_vendor_status;


/*************************************************************************
* Description : Procedure to create the PO String which will be used to create the PO.
* Author      : Rajeshwaran Varatharajan
*************************************************************************/

--PROCEDURE gm_fch_po_generate_str(
--	p_ttp_dtl_ids 	IN CLOB,
--	p_po_str_curosr	OUT TYPES.CURSOR_TYPE	
--)
--AS
--	v_ttp_dtl_id 		T4059_TTP_CAPACITY_SUMMARY.C4052_TTP_DETAIL_ID%TYPE;
--	v_vendor_id  		T301_VENDOR.C301_VENDOR_ID%TYPE;
--	V_PO_STERILE_STR	CLOB;
--	V_PO_NON_STERILE_STR CLOB;
--	--Get ttp detail id from temp table
--	CURSOR ttpdtl_id_cur
--	  IS
--	    SELECT c205_part_number_id ttp_dtl_id FROM my_t_part_list
--	    WHERE c205_part_number_id IS NOT NULL;
--	--Get Vendor id from t4059,t4057 table  
--	CURSOR vendor_id_cur
--	IS
--		SELECT t4057.c301_vendor_id vend_id
--		FROM 	t4059_ttp_capacity_summary T4059,
--				t4057_ttp_vendor_capacity_summary_dtls t4057
--		WHERE t4059.c901_status=108863--Lock in progress
--		AND t4059.c4052_ttp_detail_id = t4057.c4052_ttp_detail_id
--		AND t4057.c4052_ttp_detail_id = v_ttp_dtl_id 
--		AND t4059.c4059_void_fl IS NULL 
--		AND t4057.c4057_void_fl IS NULL
--		AND t4057.c301_vendor_id IS NOT NULL
--		GROUP BY t4057.c301_vendor_id;
--		    
--	BEGIN
--	  -- set the CLOB value to context
--	  my_context.set_my_cloblist(p_ttp_dtl_ids);
--	  --loop the cursor based on ttp detail id
--			FOR ttpdtl_id IN ttpdtl_id_cur
--	  		LOOP
--	  			v_ttp_dtl_id := ttpdtl_id.ttp_dtl_id;
--	  			--loop the cursor based on vendor id 
--	  			FOR vendor_id IN vendor_id_cur
--	  			LOOP
--	  			
--	  				v_vendor_id := vendor_id.vend_id;
--	  				--fetch steril part string,nonsterile part string using get_PO_string function
--	  				SELECT get_PO_string(v_ttp_dtl_id,v_vendor_id,108863,'Y' ) , get_PO_string(v_ttp_dtl_id,v_vendor_id,108863,'N' )
--	  				INTO	v_po_sterile_str , v_po_non_sterile_str
--	  				FROM DUAL;
--	  				--Update sterile part string, non sterile part string in t4058 table columns
--	  				UPDATE t4058_ttp_vendor_capacity_summary
--	  				SET	c4058_po_sterile_str = v_po_sterile_str,
--	  					c4058_po_non_sterile_str = v_po_non_sterile_str
--	  				WHERE c4052_ttp_detail_id = v_ttp_dtl_id 
--	  				AND C301_Vendor_Id = v_vendor_id 
--	  				AND c4058_void_fl IS NULL;
--	  				  					 			  				
--	  			END LOOP;	  			
--	  		END LOOP;
--	  		--fetch sterile po id ,nonsterile po id ,sterile str,non sterile str
--	  	OPEN p_po_str_curosr
--	  	FOR
--	  		SELECT 	c301_vendor_id vendorid, 
--	  				c4052_ttp_detail_id ttpdetailid, 
--	  				c4058_po_sterile_str posterilestr, 
--	  				c4058_po_non_sterile_str nonposterilestr, 
--	  				c4058_load_date loaddate, 
--	  				c401_sterile_po_id sterilepoid, 
--	  				c401_non_sterile_po_id nonsterilepoid
--	  		FROM t4058_ttp_vendor_capacity_summary T4058,my_t_part_list temp
--	  		WHERE c4052_ttp_detail_id = temp.c205_part_number_id 
--	  		AND   temp.c205_part_number_id IS NOT NULL 
--	  		AND   c4058_po_fl IS NULL 
--	  		AND   c4058_void_fl IS NULL;
--		
--END gm_fch_po_generate_str;


/*************************************************************************
* Description : Function to get the PO Sterile/ Non Sterile string for the passed in Vendor, 
* 				TTP Detail and product classification
* Author      : Rajeshwaran Varatharajan
*************************************************************************/
--		FUNCTION get_PO_string (
--			p_ttp_dtl_id IN  T4057_Ttp_Vendor_Capacity_Summary_Dtls.C4052_Ttp_Detail_Id%TYPE,
--			p_vendor_id  IN  T301_VENDOR.C301_VENDOR_ID%TYPE ,
--			p_status	 IN  T4059_Ttp_Capacity_Summary.C901_STATUS%TYPE,
--			p_prod_fl    IN  VARCHAR
--		)RETURN CLOB
--		
--		AS
--		v_po_str CLOB;
--		BEGIN
--		
--			BEGIN
--			
--				SELECT LISTAGG(T4057.c205_part_number_id||','||T4057.c4057_raise_po_qty||','||T4057.c402_vendor_price||'|')
--				 -- FAR flg is not required in the input string, as we can hardcode to N in the required procedure.
--				 WITHIN GROUP (ORDER BY T4057.C4052_Ttp_Detail_Id) 
--				 INTO v_po_str
--				 FROM 	t4057_ttp_vendor_capacity_summary_dtls T4057,
--				 		t4059_ttp_capacity_summary T4059,
--				 		t205_part_number T205 
--				 WHERE  T4057.c4052_ttp_detail_id = T4059.c4052_ttp_detail_id 
--				 AND    T4057.c4052_ttp_detail_id= p_ttp_dtl_id 
--				 AND    T4057.c205_part_number_id = T205.c205_part_number_id 
--				 AND    DECODE(T205.c205_product_class,4030,'Y','N') = p_prod_fl--if produclass 4030 means get sterile part else get non sterile and N/A product class parts
--				 AND    T4059.c901_status = p_status  -- Lock In Progress
--				 AND    T4057.c301_vendor_id= p_vendor_id
--				 AND    T4057.c4057_void_fl IS NULL
--				 AND    T4059.c4059_void_fl IS NULL;
--				 		
--  			EXCEPTION WHEN 
--  			OTHERS THEN
--  				v_po_str := '';
--  			END;
--		  			
--		  	RETURN 	v_po_str;	
--		
--		END get_PO_string;


		/*************************************************************************
		* Description : Procedure to update the PO ID or the error msg raised from PO generation process.
		* Author      : Rajeshwaran Varatharajan
		*************************************************************************/
--		PROCEDURE gm_upd_vendor_po_dtls(
--			p_ttp_dtl_id 	IN  T4057_Ttp_Vendor_Capacity_Summary_Dtls.C4052_Ttp_Detail_Id%TYPE,
--			p_vendor_id  	IN  T301_VENDOR.C301_VENDOR_ID%TYPE ,
--			p_po_id		 	IN	T401_PURCHASE_ORDER.C401_PURCHASE_ORD_ID%TYPE,
--			p_sterile_fl	IN  VARCHAR,
--			p_err_msg		IN  CLOB	
--		)
--		
--		AS
--		v_po_sterile_str CLOB;
--		v_po_non_sterile_str CLOB;
--		v_sterile_po_id VARCHAR2(20);
--		v_non_sterile_po_id  VARCHAR2(20);
--		v_sterile_po_fl varchar2 (1);
--		v_non_sterile_po_fl varchar2 (1);
--		
--			BEGIN
--				
--				----check sterile part flag as Y , then update c401_sterile_po_id and concat c4058_error_details|| p_err_msg 
--				----check  non sterile part flag as empty , then update c401_non_sterile_po_id and concat c4058_error_details|| p_err_msg 
--					UPDATE t4058_ttp_vendor_capacity_summary
--					SET  c401_sterile_po_id = decode(p_sterile_fl,'Y',p_po_id,c401_sterile_po_id),
--						 c401_non_sterile_po_id = decode(p_sterile_fl,'',p_po_id,c401_non_sterile_po_id),
--						 c4058_error_details = c4058_error_details|| p_err_msg
--					WHERE c4052_ttp_detail_id = p_ttp_dtl_id 
--					AND   c4058_void_fl IS NULL 
--					AND   c301_vendor_id = p_vendor_id;		
--				
--					
--			--Fetch c4058_po_sterile_str,c4058_po_non_sterile_str,c401_sterile_po_id,c401_non_sterile_po_id values
--						SELECT  c4058_po_sterile_str, 
--	  							c4058_po_non_sterile_str, 
--	  							c401_sterile_po_id,
--	  							c401_non_sterile_po_id
--	  					INTO v_po_sterile_str,v_po_non_sterile_str,v_sterile_po_id,v_non_sterile_po_id
--	  					FROM t4058_ttp_vendor_capacity_summary	
--	  				   WHERE c4052_ttp_detail_id = p_ttp_dtl_id
--	  				     AND c301_vendor_id = p_vendor_id
--						 AND c4058_void_fl IS NULL;		
--	  					 
--					 	--Check if C4058_PO_STERILE_STR is null and C4058_STERILE_PO_ID is null or C4058_PO_STERILE_STR is not null and C4058_STERILE_PO_ID is not null then assign ster flag as Y	
--					     
--						 IF  ((v_po_sterile_str IS  NULL and v_sterile_po_id IS NULL) OR (v_po_sterile_str IS NOT  NULL and v_sterile_po_id IS NOT NULL))  THEN 
--						 		v_sterile_po_fl := 'Y';
--						 END IF;
--						--Check if C4058_PO_NON_STERILE_STR is null and C4058_NON_STERILE_PO_ID is null or C4058_PO_NON_STERILE_STR is not null and C4058_NON_STERILE_PO_ID is not null then assign non ster flag as Y
--						 IF  ((v_po_non_sterile_str IS NULL and v_non_sterile_po_id  IS NULL) OR (v_po_non_sterile_str IS NOT NULL and v_non_sterile_po_id IS NOT NULL))  THEN 		
--						 v_non_sterile_po_fl := 'Y';
--						 END IF;
--						 
--						--If both sterile and non sterile flag are Y, then update C4058_PO_FL as Y
--						 IF  (v_sterile_po_fl = 'Y' and v_non_sterile_po_fl = 'Y')  THEN
--						 			UPDATE t4058_ttp_vendor_capacity_summary
--										SET  c4058_po_fl = 'Y'
--									WHERE c4052_ttp_detail_id = p_ttp_dtl_id 
--									AND c4058_void_fl IS NULL 
--					   			    AND c301_vendor_id = p_vendor_id;	
--						 END IF;
--			
--		END gm_upd_vendor_po_dtls;
END gm_pkg_oppr_ld_ttp_by_vendor_lock;
/
