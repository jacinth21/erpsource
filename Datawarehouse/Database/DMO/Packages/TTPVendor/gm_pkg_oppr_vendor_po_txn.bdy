--@C:\PMT\bit\SpineIT-ERP\Datawarehouse\Database\DMO\Packages\TTPVendor\gm_pkg_oppr_vendor_po_txn.bdy;
CREATE OR REPLACE
PACKAGE BODY gm_pkg_oppr_vendor_po_txn
IS
  /****************************************************************************************************
  * Description  : This Procedure used to save the vendor PO Type when vendor status is open or approved.
  * Scrren location :GOP-->Transactions-->TTPByVendor-->Create PO Dashboard
  *****************************************************************************************************/
PROCEDURE gm_sav_vendor_po_type_dtls(
    p_input_str IN CLOB,
    p_user_id   IN t4058_ttp_vendor_capacity_summary.c4058_last_updated_by%type,
    p_out_msg OUT VARCHAR2 )
AS
  v_strlen                 NUMBER         := NVL (LENGTH (p_input_str), 0) ;
  v_string                 CLOB := p_input_str;
  v_substring              VARCHAR2(4000);
  v_vendor_capa_summary_id NUMBER;
  v_po_type                NUMBER;
  v_out_error_dtls         VARCHAR2(4000);
  v_status_id t4058_ttp_vendor_capacity_summary.c901_status%TYPE;
BEGIN
  IF v_strlen                    > 0 THEN
    WHILE INSTR (v_string, '|') <> 0
    LOOP
      v_substring              := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
      v_string                 := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
      v_vendor_capa_summary_id := NULL;
      v_po_type                := NULL;
      v_vendor_capa_summary_id := TO_NUMBER(SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
      v_substring              := SUBSTR (v_substring, INSTR (v_substring, '^')              + 1);
      v_po_type                := TO_NUMBER(v_substring);
      --checking ttp details status
      SELECT c901_status
        INTO v_status_id
        FROM t4058_ttp_vendor_capacity_summary
       WHERE C4058_TTP_VENDOR_CAPA_SUMMARY_ID = v_vendor_capa_summary_id
         AND c4058_void_fl                     IS NULL FOR UPDATE;
         
         --108860 - open status ,108862 - Approved status
      IF (v_status_id                        = 108860 OR v_status_id = 108862) THEN
        gm_pkg_oppr_vendor_po_txn.gm_upd_vendor_po_type(v_vendor_capa_summary_id,v_po_type,p_user_id);
      ELSE
        p_out_msg := p_out_msg || v_vendor_capa_summary_id;
      END IF;
    END LOOP;
  END IF;
END gm_sav_vendor_po_type_dtls;
/****************************************************************************************************
* Description  :  Procedure used to update the PO Type
*****************************************************************************************************/
PROCEDURE gm_upd_vendor_po_type(
    p_vendor_capa_summary_id IN t4058_ttp_vendor_capacity_summary.C4058_TTP_VENDOR_CAPA_SUMMARY_ID%TYPE,
    p_po_type                IN t4058_ttp_vendor_capacity_summary.C901_PO_TYPE%type,
    p_user_id                IN t4058_ttp_vendor_capacity_summary.c4058_last_updated_by%type )
AS
BEGIN
  UPDATE t4058_ttp_vendor_capacity_summary
     SET c901_po_type                     = p_po_type,
         c4058_last_updated_by            = p_user_id,
         c4058_last_updated_date          = CURRENT_DATE
   WHERE c4058_ttp_vendor_capa_summary_id = p_vendor_capa_summary_id
  AND c4058_void_fl                     IS NULL;
END gm_upd_vendor_po_type;
/****************************************************************************************************
* Description  :  Procedure  used to update the vendor po generation
*                  status as failed or po generated using following values 
* for eg: vendorcapasummaryid ,status id , user id , out parameter(invalid capa ids)
*****************************************************************************************************/
PROCEDURE gm_upd_bulk_vendor_po_status(
    p_vendor_capa_summary_ids IN CLOB,
    p_po_status              IN t4058_ttp_vendor_capacity_summary.c901_status%type,
    p_user_id                IN t4058_ttp_vendor_capacity_summary.c4058_last_updated_by%type,
    p_out_invalid_capa_ids OUT CLOB )
AS
  v_out_cnt NUMBER;
  v_out_invalid_capa_ids CLOB;
 
BEGIN
  my_context.set_my_inlist_ctx (p_vendor_capa_summary_ids);
 
 gm_pkg_oppr_vendor_po_txn.gm_validate_vendor_po_initiate(v_out_invalid_capa_ids);
 
  --26240821 PO generation In progress ,108862 - approved , 26240823 - PO generation Failed
  --26241143 PO Regeneration
  UPDATE t4058_ttp_vendor_capacity_summary t4058
     SET c901_status                        = p_po_status,
         c4058_last_updated_by                = p_user_id,
         c4058_last_updated_date              = CURRENT_DATE
   WHERE c4058_ttp_vendor_capa_summary_id IN (SELECT token FROM v_in_list)
     AND t4058.c901_status  IN(108862,26240823,26241143)
     AND c4058_void_fl                     IS NULL;
     
  p_out_invalid_capa_ids := v_out_invalid_capa_ids;

END gm_upd_bulk_vendor_po_status;
/****************************************************************************************************
* Description  :  Procedure used to validate the capa id is in Approved status or not
*****************************************************************************************************/
PROCEDURE gm_validate_vendor_po_initiate(
    p_out_invalid_capa_ids OUT CLOB)
AS
BEGIN
	
    SELECT RTRIM(XMLAGG(XMLELEMENT(e,capa_summary_id || ',')).EXTRACT('//text()').getclobval(),',')  
      INTO  p_out_invalid_capa_ids
     FROM(
         SELECT t4058.c4058_ttp_vendor_capa_summary_id capa_summary_id
           FROM t4058_ttp_vendor_capacity_summary t4058,v_in_list v_list
          WHERE t4058.c4058_ttp_vendor_capa_summary_id = v_list.token
            AND t4058.c901_status NOT IN(108862,26240823,26241143)
            AND t4058.c4058_void_fl     IS NULL
          );
   
END gm_validate_vendor_po_initiate;
/****************************************************************************************************
* Description  :  Procedure used  to update the ttp vendor status as PO generation in progress
*****************************************************************************************************/
PROCEDURE gm_upd_vendor_po_status(
    p_capa_summary_id IN t4058_ttp_vendor_capacity_summary.c4058_ttp_vendor_capa_summary_id%type,
    p_status_id       IN t4058_ttp_vendor_capacity_summary.C901_STATUS%type,
    p_err_details     IN t4058_ttp_vendor_capacity_summary.c4058_error_details%type,
    p_user_id         IN t4058_ttp_vendor_capacity_summary.c4058_last_updated_by%type )
AS
BEGIN
  UPDATE t4058_ttp_vendor_capacity_summary
     SET c901_status                     = p_status_id,
         c4058_error_details             = p_err_details,
         c4058_last_updated_by           = p_user_id,
         c4058_po_created_by             = DECODE(p_status_id,26240822,p_user_id,NULL),
         c4058_po_created_date           = DECODE(p_status_id,26240822,CURRENT_DATE,NULL),
         c4058_last_updated_date         = CURRENT_DATE
  WHERE c4058_ttp_vendor_capa_summary_id = p_capa_summary_id
  AND c4058_void_fl                     IS NULL;
END gm_upd_vendor_po_status;
/****************************************************************************************************
* Description  :  Procedure used  to fetch Vendor Details from t4058 table based on status and load date
*****************************************************************************************************/
PROCEDURE gm_fch_vendor_po_dtls(
    p_po_status IN t4058_ttp_vendor_capacity_summary.c901_status%type,
    p_load_date IN VARCHAR2,
    p_out_ven_po_dtls OUT TYPES.cursor_type )
AS
  v_load_date DATE;
  v_po_wo_cnt NUMBER;
BEGIN
  SELECT to_date(p_load_date,'MM/yyyy')INTO v_load_date FROM DUAL;
  
  OPEN p_out_ven_po_dtls FOR 
        SELECT T4058.c301_vendor_id vendorid,
               T4058.c4058_ttp_vendor_capa_summary_id vendorcapasummid,
               T4058.c901_po_type potype,
               NVL(GET_RULE_VALUE('VEN_PO_WO_CNT','VEN_PO_WO_CNT'), '75') po_wo_cnt,
               NVL(GET_RULE_VALUE(T4058.c301_vendor_id, 'VEN_PO_GEN_BY_TTP'), 'N')po_gen_by_ttp_fl
          FROM t4058_ttp_vendor_capacity_summary T4058 
         WHERE T4058.C901_Status = p_po_status 
           AND T4058.c4058_load_date = v_load_date 
           AND T4058.c4058_void_fl IS NULL;
END gm_fch_vendor_po_dtls;
/****************************************************************************************************
* Description  :  Procedure used  to fetch Vendor part number from t4057 table based on vendor id and load date
*****************************************************************************************************/
PROCEDURE gm_fch_vendor_po_parts(
    p_vendor_id IN t4057_ttp_vendor_capacity_summary_dtls.c301_vendor_id%type,
    p_load_date IN VARCHAR2,
    p_out_part_str OUT CLOB )
AS
  v_load_date DATE;
BEGIN
  SELECT to_date(p_load_date,'MM/yyyy')INTO v_load_date FROM DUAL;
  
  -- PC-376: PO creation only for DCO parts. To exclude the Non DCO parts
  
  SELECT RTRIM(XMLAGG(XMLELEMENT(e,partnum|| ',')).EXTRACT('//text()').getclobval(),',')
  INTO p_out_part_str
  FROM
    (SELECT t4057.c205_part_number_id partnum
       FROM t4057_ttp_vendor_capacity_summary_dtls t4057
      WHERE t4057.c301_vendor_id   = p_vendor_id
        AND t4057.c4057_load_date    = v_load_date
        AND t4057.c4057_raise_po_qty > 0
        AND t4057.C4057_QTY_QUOTED  >= 0
        AND (t4057.c4057_po_fl IS NULL OR 
        	 t4057.c4057_regen_po_fl = 'TBG')
        AND t4057.c4057_void_fl     IS NULL
    );
END gm_fch_vendor_po_parts;
/****************************************************************************************************
* Description  :  Procedure used  to call the create po string procedure based on v_po_gen_by_ttp_fl
*****************************************************************************************************/
PROCEDURE gm_process_vendor_po_str(
    p_vendor_id        IN t4058_ttp_vendor_capacity_summary.c301_vendor_id%type,
    p_ven_capa_summ_id IN t4058_ttp_vendor_capacity_summary.c4058_ttp_vendor_capa_summary_id%type,
    p_po_type          IN t4058_ttp_vendor_capacity_summary.c901_po_type%type,
    p_user_id          IN t4058_ttp_vendor_capacity_summary.c4058_last_updated_by%type,
    p_po_wo_cnt        IN  VARCHAR2,
    p_po_gen_by_ttp_fl IN VARCHAR2)
AS
BEGIN

  IF p_po_gen_by_ttp_fl        ='Y' THEN
    gm_pkg_oppr_vendor_po_txn.gm_create_po_str_by_ttp(p_ven_capa_summ_id,p_po_wo_cnt,p_user_id);
  ELSE
    gm_pkg_oppr_vendor_po_txn.gm_create_po_str_by_vendor(p_ven_capa_summ_id,p_po_wo_cnt,p_user_id);
  END IF;
END gm_process_vendor_po_str;
/****************************************************************************************************
* Description  :  Procedure used  to create the po string by vendor
*****************************************************************************************************/
PROCEDURE gm_create_po_str_by_vendor(
    p_vendor_capa_summary_id IN t4058_ttp_vendor_capacity_summary.c4058_ttp_vendor_capa_summary_id%TYPE,
    p_wo_split_cnt           IN NUMBER,
    p_user_id                IN t4058_ttp_vendor_capacity_summary.c4058_last_updated_by%type )
AS
  v_vendor_id t4058_ttp_vendor_capacity_summary.c301_vendor_id%TYPE;
  v_load_date t4058_ttp_vendor_capacity_summary.c4058_load_date%TYPE;
  v_po_type t4058_ttp_vendor_capacity_summary.c901_po_type%TYPE;
  v_total_line_item_cnt NUMBER;
  v_po_count            NUMBER;
  v_start_value         NUMBER;
  v_wo_split_val        NUMBER;
  v_po_str CLOB;
  v_po_creation_comments VARCHAR2(2000);
  
BEGIN
	
  SELECT T4058.c301_vendor_id ,T4058.c4058_load_date , T4058.c901_po_type
    INTO v_vendor_id,v_load_date,v_po_type
    FROM t4058_ttp_vendor_capacity_summary T4058
   WHERE T4058.c4058_ttp_vendor_capa_summary_id = p_vendor_capa_summary_id
     AND T4058.c4058_void_fl                     IS NULL;
  
     -- PC-376: PO creation only for DCO parts. To exclude the Non DCO parts
     
  -- get the total line count based on vendor id and load date
  SELECT COUNT(1)
    INTO v_total_line_item_cnt
    FROM t4057_ttp_vendor_capacity_summary_dtls t4057
   WHERE t4057.c301_vendor_id   = v_vendor_id
     AND t4057.c4057_load_date    = v_load_date
     AND t4057.c4057_raise_po_qty > 0
     AND t4057.C4057_QTY_QUOTED >= 0 
     AND t4057.c4057_no_wo_dco_fl IS NULL
     AND (t4057.c4057_po_fl IS NULL OR 
          t4057.c4057_regen_po_fl = 'TBG')
     AND t4057.c4057_void_fl     IS NULL;
     
  v_po_count                  := CEIL(v_total_line_item_cnt/p_wo_split_cnt);
  v_start_value               := 0;
  v_wo_split_val              := p_wo_split_cnt;
  
  WHILE(v_po_count > 0)
  LOOP
   SELECT RTRIM(XMLAGG(XMLELEMENT(e,inputstr || '^')).EXTRACT('//text()').getclobval(),',') 
    INTO v_po_str
    FROM
      (SELECT t4057.c205_part_number_id
        ||','
        ||t4057.c402_vendor_price
        ||','
        ||t4057.c4057_raise_po_qty
        ||','
        ||TRIM(t205.c205_rev_num)
        ||','
        ||t4057.c4057_pricing_id inputstr-- PC-295 Adding pricing id in PO creation string
      FROM T4057_Ttp_Vendor_Capacity_Summary_Dtls t4057,
        t205_part_number t205
      WHERE t4057.c301_vendor_id    = v_vendor_id
      AND t4057.c4057_load_date     = v_load_date
      AND t4057.c205_part_number_id = t205.c205_part_number_id
      AND t4057.c4057_raise_po_qty  > 0
      AND t4057.C4057_QTY_QUOTED >= 0 
      AND t4057.c4057_no_wo_dco_fl IS NULL
      AND (t4057.c4057_po_fl IS NULL OR 
           t4057.c4057_regen_po_fl = 'TBG')
      AND t4057.c4057_void_fl      IS NULL
      ORDER BY t4057.c4057_ttp_vendor_capa_dtls_id Offset v_start_value ROWS
      FETCH NEXT p_wo_split_cnt ROWS ONLY
      );
    
    v_po_creation_comments := 'PO Created through TTP By Vendor based on Vendor' ;
    -- to save po string to t4065 table
    gm_sav_ttp_by_vendor_po_details(p_vendor_capa_summary_id,p_wo_split_cnt,v_po_creation_comments,v_po_str,p_user_id);
    
    v_start_value := v_wo_split_val;
    v_wo_split_val:=v_wo_split_val+p_wo_split_cnt;
    v_po_count    :=v_po_count    -1;
  END LOOP;
END gm_create_po_str_by_vendor;
/****************************************************************************************************
* Description  :  Procedure used  to create the po string by TTP
*****************************************************************************************************/
PROCEDURE gm_create_po_str_by_ttp(
    p_vendor_capa_summary_id IN t4058_ttp_vendor_capacity_summary.C4058_TTP_VENDOR_CAPA_SUMMARY_ID%TYPE,
    p_wo_split_cnt           IN NUMBER,
    p_user_id                IN t4058_ttp_vendor_capacity_summary.C4058_LAST_UPDATED_BY%type )
AS
  -- CURSOR vendor_dtls_cur IS
  v_vendor_id t4058_ttp_vendor_capacity_summary.c301_vendor_id%TYPE;
  v_load_date t4058_ttp_vendor_capacity_summary.C4058_LOAD_DATE%TYPE;
  v_po_type t4058_ttp_vendor_capacity_summary.c901_po_type%TYPE;
  v_total_line_item_cnt  NUMBER;
  v_po_count             NUMBER;
  v_start_value          NUMBER;
  v_wo_split_val         NUMBER;
  v_po_str               VARCHAR2(4000);
  v_po_creation_comments VARCHAR2(2000);
  
  CURSOR ttp_dtls_cur
  IS
    SELECT DISTINCT t4057.c4052_ttp_detail_id ttpdtlid
      FROM t4057_ttp_vendor_capacity_summary_dtls t4057,t4052_ttp_detail t4052
     WHERE t4057.c301_vendor_id      = v_vendor_id
       AND t4057.c4057_load_date     = v_load_date
       AND t4057.c4057_void_fl       IS NULL
       AND t4052.c4052_ttp_detail_id = t4057.c4052_ttp_detail_id
       AND t4052.c4052_void_fl      IS NULL       
       AND t4057.c4057_no_wo_dco_fl IS NULL
       AND t4057.c4057_po_fl IS NULL ;
       
BEGIN
	
  SELECT T4058.c301_vendor_id , T4058.c4058_load_date , T4058.c901_po_type
    INTO v_vendor_id,v_load_date, v_po_type
    FROM t4058_ttp_vendor_capacity_summary T4058
   WHERE T4058.c4058_ttp_vendor_capa_summary_id = p_vendor_capa_summary_id
     AND T4058.c4058_void_fl                     IS NULL;
    
     -- PC-376: PO creation only for DCO parts. To exclude the Non DCO parts
     
  FOR ttp_dtl IN ttp_dtls_cur
  LOOP
    SELECT COUNT(1)
      INTO v_total_line_item_cnt
      FROM t4057_ttp_vendor_capacity_summary_dtls t4057,t205_part_number t205
     WHERE t4057.c4052_ttp_detail_id=ttp_dtl.ttpdtlid
       AND t4057.c205_part_number_id  = t205.c205_part_number_id
       AND t4057.c4057_load_date      = v_load_date
       AND t4057.c301_vendor_id       = v_vendor_id
       AND t205.c205_product_family  != '4052' --NON graphic case family
       AND t4057.c4057_raise_po_qty   > 0
       AND t4057.C4057_QTY_QUOTED >= 0 
       AND t4057.c4057_no_wo_dco_fl IS NULL
       AND (t4057.c4057_po_fl IS NULL OR 
        	t4057.c4057_regen_po_fl = 'TBG')
       AND t4057.c4057_void_fl       IS NULL;
      
    v_po_count                    := CEIL(v_total_line_item_cnt/p_wo_split_cnt);
    v_start_value                 :=0;
    v_wo_split_val                :=p_wo_split_cnt;
    
    WHILE(v_po_count               > 0)
    LOOP
      SELECT RTRIM(XMLAGG(XMLELEMENT(e,inputstr || '^')).EXTRACT('//text()').getclobval(),',') 
      INTO v_po_str
      FROM
        (SELECT t4057.c205_part_number_id
          ||','
          ||t4057.c402_vendor_price
          ||','
          ||t4057.c4057_raise_po_qty
          ||','
          ||TRIM(t205.c205_rev_num)
          ||','
          ||t4057.c4057_pricing_id inputstr-- PC-295 Adding pricing id in PO creation string
        FROM T4057_Ttp_Vendor_Capacity_Summary_Dtls t4057 ,t205_part_number t205
       WHERE t4057.c301_vendor_id    = v_vendor_id
         AND t4057.c4057_load_date     = v_load_date
         AND t4057.c205_part_number_id = t205.c205_part_number_id
         AND t4057.c4052_ttp_detail_id =ttp_dtl.ttpdtlid
         AND t4057.c4057_void_fl      IS NULL
         AND t205.c205_product_family != '4052'
         AND t4057.c4057_raise_po_qty  > 0
         AND t4057.C4057_QTY_QUOTED >= 0 
         AND t4057.c4057_no_wo_dco_fl IS NULL
       	 AND (t4057.c4057_po_fl IS NULL OR 
        	  t4057.c4057_regen_po_fl = 'TBG')
    ORDER BY t4057.c4057_ttp_vendor_capa_dtls_id Offset v_start_value ROWS
        FETCH NEXT p_wo_split_cnt ROWS ONLY
        );
        
      v_po_creation_comments := 'PO Created through TTP By Vendor based on TTP Name';
      gm_sav_ttp_by_vendor_po_details(p_vendor_capa_summary_id,p_wo_split_cnt,v_po_creation_comments,v_po_str,p_user_id);
     
      v_start_value := v_wo_split_val;
      v_wo_split_val:=v_wo_split_val +p_wo_split_cnt;
      v_po_count    :=v_po_count     -1;
    
      END LOOP; --While LOOP end 
  END LOOP; --for loop end
  
  gm_create_po_str_by_product_family(p_vendor_capa_summary_id,v_vendor_id,v_po_type,v_load_date,p_wo_split_cnt,p_user_id);
END gm_create_po_str_by_ttp;
/****************************************************************************************************
* Description  :  Procedure used  to create the po string  by product family(graphic case)
*****************************************************************************************************/
PROCEDURE gm_create_po_str_by_product_family(
    p_vendor_capa_summary_id IN t4058_ttp_vendor_capacity_summary.c4058_ttp_vendor_capa_summary_id%TYPE,
    p_vendor_id              IN t4058_ttp_vendor_capacity_summary.c301_vendor_id%TYPE,
    p_po_type                IN t4058_ttp_vendor_capacity_summary.c901_po_type%TYPE,
    p_load_date              IN t4058_ttp_vendor_capacity_summary.c4058_load_date%TYPE,
    p_wo_split_cnt           IN NUMBER,
    p_user_id                IN t4058_ttp_vendor_capacity_summary.c4058_last_updated_by%type )
AS
  v_total_line_item_cnt NUMBER;
  v_po_count            NUMBER;
  v_start_value         NUMBER;
  v_wo_split_val        NUMBER;
  v_po_str CLOB;
  v_po_creation_comments VARCHAR2(2000);
  
BEGIN

	-- PC-376: PO creation only for DCO parts. To exclude the Non DCO parts
	
  SELECT COUNT(1)
   INTO v_total_line_item_cnt
   FROM t4057_ttp_vendor_capacity_summary_dtls t4057,t205_part_number t205
  WHERE c301_vendor_id           =p_vendor_id
    AND c4057_load_date          = p_load_date
    AND t205.c205_part_number_id = t4057.c205_part_number_id
    AND t205.c205_product_family = '4052' --Graphic case Family id
    AND t4057.c4057_raise_po_qty > 0
    AND t4057.C4057_QTY_QUOTED >= 0 
    AND t4057.c4057_no_wo_dco_fl IS NULL
    AND c4057_void_fl           IS NULL;
    
  v_po_count                  := CEIL(v_total_line_item_cnt/p_wo_split_cnt);
  v_start_value               :=0;
  v_wo_split_val              :=p_wo_split_cnt;
  
  WHILE(v_po_count             > 0)
  LOOP
     SELECT RTRIM(XMLAGG(XMLELEMENT(e,inputstr || '^')).EXTRACT('//text()').getclobval(),',') 
    INTO v_po_str
    FROM
      (SELECT t4057.c205_part_number_id
        ||','
        ||t4057.c402_vendor_price
        ||','
        ||t4057.c4057_raise_po_qty
        ||','
        ||TRIM(t205.c205_rev_num)
        ||','
        ||t4057.c4057_pricing_id inputstr-- PC-295 Adding pricing id in PO creation string
      FROM t4057_ttp_vendor_capacity_summary_dtls t4057 ,t205_part_number t205
     WHERE t4057.c205_part_number_id = t205.c205_part_number_id
        AND t4057.c4057_load_date     = p_load_date
        AND t4057.c301_vendor_id    = p_vendor_id
        AND t4057.c4057_void_fl      IS NULL
        AND t205.c205_product_family  = '4052'
        AND t4057.c4057_raise_po_qty  > 0
        AND t4057.C4057_QTY_QUOTED >= 0 
        AND t4057.c4057_no_wo_dco_fl IS NULL
   ORDER BY t4057.c4057_ttp_vendor_capa_dtls_id Offset v_start_value ROWS
      FETCH NEXT p_wo_split_cnt ROWS ONLY
      );
     
    v_po_creation_comments := 'PO Created through TTP By Vendor based on By Family' ;
    gm_sav_ttp_by_vendor_po_details(p_vendor_capa_summary_id,p_wo_split_cnt,v_po_creation_comments,v_po_str,p_user_id);
    
    v_start_value := v_wo_split_val;
    v_wo_split_val:=v_wo_split_val +p_wo_split_cnt;
    v_po_count    :=v_po_count     -1;
  END LOOP;
  
END gm_create_po_str_by_product_family;
/****************************************************************************************************
* Description  :  Procedure used  to save the PO string to t4065 table
*****************************************************************************************************/
PROCEDURE gm_sav_ttp_by_vendor_po_details(
    p_vendor_capa_summary_id IN t4065_ttp_vendor_po_dtls.c4058_ttp_vendor_capa_summary_id%type,
    p_wo_split_cnt           IN t4065_ttp_vendor_po_dtls.c4065_po_wo_cnt%type,
    p_po_creation_comments   IN t4065_ttp_vendor_po_dtls.c4065_po_creation_comments%type,
    p_po_str                 IN t4065_ttp_vendor_po_dtls.c4065_po_string %type,
    p_user_id                IN t4065_ttp_vendor_po_dtls.c4065_last_updated_by%type )
AS
BEGIN
	
  INSERT
  INTO t4065_ttp_vendor_po_dtls
    (
      c4065_ttp_by_vendor_po_dtl, c4058_ttp_vendor_capa_summary_id ,c401_purchase_ord_id,
      c401_po_total_amount ,c4065_po_total_qty ,c4065_po_wo_cnt ,c4065_void_fl ,
      c4065_last_updated_by,c4065_last_updated_date,c4065_po_creation_comments,c4065_po_string
    )
    VALUES
    (
      s4065_TTP_BY_VENDOR_PO_DTL.NEXTVAL , p_vendor_capa_summary_id,NULL,
      NULL,NULL,NULL,NULL,
      p_user_id,CURRENT_DATE,p_po_creation_comments, p_po_str
    );
END gm_sav_ttp_by_vendor_po_details;
/****************************************************************************************************
* Description  :  Procedure used to fetch the PO details from t4065 table
*****************************************************************************************************/
PROCEDURE gm_fch_vendor_po_str(
    p_vendor_capa_summary_id IN t4065_ttp_vendor_po_dtls.c4058_ttp_vendor_capa_summary_id%TYPE,
    p_out_po_str OUT TYPES.cursor_type
  )
AS
BEGIN
  OPEN p_out_po_str FOR 
         SELECT c4065_po_string postring,
                c4065_po_creation_comments pocomments,
                c4065_ttp_by_vendor_po_dtl venpodtlid 
           FROM t4065_ttp_vendor_po_dtls 
          WHERE c4058_ttp_vendor_capa_summary_id = p_vendor_capa_summary_id 
            AND c401_purchase_ord_id IS NULL 
            AND c4065_void_fl IS NULL;
END gm_fch_vendor_po_str;
/****************************************************************************************************
* Description  :  Procedure used  to update the po id, total qty , po amount to t4065 table based on PO dtl id
*****************************************************************************************************/
PROCEDURE gm_upd_vendor_po_dtls
  (
    p_po_id       IN t4065_ttp_vendor_po_dtls.c401_purchase_ord_id%type,
    p_po_dlts_str IN CLOB,
    p_po_dtl_id   IN t4065_ttp_vendor_po_dtls.c4065_ttp_by_vendor_po_dtl%type,
    p_user_id     IN t4065_ttp_vendor_po_dtls.c4065_last_updated_by%type
  )
AS
  v_string CLOB := p_po_dlts_str;
  v_substring VARCHAR2 (3000);
  v_total_qty NUMBER;
  v_wo_cnt    NUMBER;
  v_po_amount NUMBER;
  
BEGIN
	
  WHILE INSTR (v_string, '|') <> 0
  LOOP
    v_substring :=(SUBSTR (v_string, 1, INSTR (v_string, '|') - 1));
    v_string    := (SUBSTR (v_string, INSTR (v_string, '|')   + 1));
    v_total_qty := NULL;
    v_wo_cnt    := NULL;
    v_po_amount := NULL;
    v_po_amount := TO_NUMBER(SUBSTR (v_substring, 1, instr (v_substring, '^') - 1));
    v_substring := SUBSTR (v_substring, instr (v_substring, '^')              + 1);
    v_total_qty := TO_NUMBER(SUBSTR (v_substring, 1, instr (v_substring, '^') - 1));
    v_substring := SUBSTR (v_substring, instr (v_substring, '^')              + 1);
    v_wo_cnt    := v_substring;
    
  END LOOP;--END while LOOP
  
   UPDATE t4065_ttp_vendor_po_dtls
       SET c401_purchase_ord_id       = p_po_id,
           c401_po_total_amount       = v_po_amount,
           c4065_po_total_qty         = v_total_qty,
           c4065_po_wo_cnt            = v_wo_cnt,
           c4065_last_updated_by      = p_user_id,
           c4065_last_updated_date    = CURRENT_DATE
     WHERE c4065_ttp_by_vendor_po_dtl = p_po_dtl_id
       AND c4065_void_fl               IS NULL
       AND c401_purchase_ord_id        IS NULL;
       
END gm_upd_vendor_po_dtls;
/****************************************************************************************************
* Description  :  Procedure used  to fetch the PO IDs based on CAPA summary ID to void the POs and WOs
*****************************************************************************************************/
PROCEDURE gm_fch_vendor_po_ids(
    p_vendor_capa_summary_id IN t4065_ttp_vendor_po_dtls.c4058_ttp_vendor_capa_summary_id%TYPE,
    p_out_po_id OUT VARCHAR2 )
AS
BEGIN
	
  SELECT RTRIM(XMLAGG(XMLELEMENT(e,poid || ',')).EXTRACT('//text()').getclobval(),',')
    INTO p_out_po_id
   FROM (
        SELECT c401_purchase_ord_id poid
          FROM t4065_ttp_vendor_po_dtls
         WHERE c4058_ttp_vendor_capa_summary_id = p_vendor_capa_summary_id
           AND c4065_void_fl                     IS NULL
           AND c401_purchase_ord_id              IS NOT NULL
         );
END gm_fch_vendor_po_ids;



 /****************************************************************************************************
* Description  :  Procedure to set WO DCO Fl as these parts do not have WO DCO.
*****************************************************************************************************/
PROCEDURE gm_upd_wo_dco_flag (
        p_vendor_id        IN t4058_ttp_vendor_capacity_summary.c301_vendor_id%TYPE,
        p_load_date        IN VARCHAR2,
        p_wo_dco_flg       IN VARCHAR2,
        p_part_miss_wo_doc IN CLOB,
        p_user_id          IN t4065_ttp_vendor_po_dtls.c4065_last_updated_by%type)
AS
    v_load_date DATE;
BEGIN
	--
     SELECT to_date (p_load_date, 'MM/yyyy') INTO v_load_date FROM DUAL;
     --
    my_context.set_my_cloblist (p_part_miss_wo_doc||',') ;
    
    -- Update the WO DCO flag for these Parts which do not have WO DCO.So PO cannot be created.
     UPDATE t4057_ttp_vendor_capacity_summary_dtls
    SET c4057_no_wo_dco_fl       = p_wo_dco_flg, c4057_last_updated_by = p_user_id, c4057_last_updated_date = CURRENT_DATE
      WHERE c301_vendor_id       = p_vendor_id
        AND c4057_load_date      = v_load_date
        AND c205_part_number_id IN
        (
             SELECT c205_part_number_id
               FROM my_t_part_list
              WHERE c205_part_number_id IS NOT NULL
        )
        AND c4057_void_fl IS NULL ;
        
END gm_upd_wo_dco_flag;


/****************************************************************************************************
* Description  :  Procedure to set PO Fl as PO has been generated for these Parts
*****************************************************************************************************/
PROCEDURE gm_upd_part_po_flag (
        p_vendor_id IN t4058_ttp_vendor_capacity_summary.c301_vendor_id%TYPE,
        p_load_date IN VARCHAR2,
        p_user_id   IN t4065_ttp_vendor_po_dtls.c4065_last_updated_by%type)
AS
    v_load_date DATE;
BEGIN
	--
     SELECT to_date (p_load_date, 'MM/yyyy') INTO v_load_date FROM DUAL;
    
     
     -- to update the status
     
     UPDATE t4069_ttp_capa_excess_po_dtls
	   SET c901_status = 109024 
	 WHERE c4057_ttp_vendor_capa_dtls_id IN (
	 SELECT c4057_ttp_vendor_capa_dtls_id
	 FROM t4057_ttp_vendor_capacity_summary_dtls
	 	WHERE c301_vendor_id      = p_vendor_id
        AND c4057_load_date     = v_load_date
        AND (c4057_po_fl IS NULL OR
             c4057_regen_po_fl = 'TBG')
        AND c4057_raise_po_qty  >= 0     
        AND c4057_void_fl      IS NULL
        AND c4057_no_wo_dco_fl IS NULL
	 )
	   AND c901_status <> 109024 -- completed
	   AND c4069_void_fl is null;
     
    -- Update the PO Flag for all the parts for the Vendor and the Load Date.
    
     
     UPDATE t4057_ttp_vendor_capacity_summary_dtls
        SET c4057_po_fl             = 'Y', 
            c4057_regen_po_fl = 'GENERATED',
            c4057_last_updated_by = p_user_id, c4057_last_updated_date = CURRENT_DATE
      WHERE c301_vendor_id      = p_vendor_id
        AND c4057_load_date     = v_load_date
        AND (c4057_po_fl IS NULL OR
             c4057_regen_po_fl = 'TBG') 
        AND c4057_raise_po_qty  >= 0     
        AND c4057_void_fl      IS NULL
        AND c4057_no_wo_dco_fl IS NULL;
        
END gm_upd_part_po_flag;
 

END gm_pkg_oppr_vendor_po_txn;
/