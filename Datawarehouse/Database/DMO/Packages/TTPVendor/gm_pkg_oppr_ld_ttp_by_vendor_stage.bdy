CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ld_ttp_by_vendor_stage
IS
/*******************************************************
 * Purpose: This pkg used for to load GOP monthend Load staging tables which will have only six months data
 *
 * modification history
 * ====================
 * Person		 Date	Comments
 * ---------   ------	------------------------------------------
 * psrinivasan    20191116  Initial Version
 *******************************************************/

   /*******************************************************
	* To truncate all Staging and TEMP table before reload data 
	*******************************************************/
	PROCEDURE gm_delete_stage_table
	AS
	BEGIN
		--Truncate Stage Tables
		execute immediate 'TRUNCATE TABLE ST408_VENDOR_DHR_BY_PART';
		execute immediate 'TRUNCATE TABLE ST402_VENDOR_WO_BY_PART';
		execute immediate 'TRUNCATE TABLE ST402_TOP_2_VENDOR_DTLS';
		execute immediate 'TRUNCATE TABLE ST405_VENDOR_ACTIVE_PRICING';
		execute immediate 'TRUNCATE TABLE ST502_BACK_ORDER_DTLS';
		execute immediate 'TRUNCATE TABLE ST520_REQUEST_PRIORITY_DTLS';
		--Truncate Temp Tables
		execute immediate 'TRUNCATE TABLE TEMP_VENDOR_DHR_DTLS';
		execute immediate 'TRUNCATE TABLE TEMP_VENDOR_WO_DTLS';
		execute immediate 'TRUNCATE TABLE TEMP_VENDOR_CAPACITY_SUMMARY_DTLS';
		execute immediate 'TRUNCATE TABLE TEMP_3MONTH_VENDOR_HISTORICAL_RECEIPT';
		execute immediate 'TRUNCATE TABLE TEMP_SET_PRIORITY_DTLS';	
		
    END gm_delete_stage_table;

/*******************************************************
	* This main procedure for load data into staging table
	* 
  *******************************************************/
	PROCEDURE gm_sav_process_stage_main(
	  p_company_id	IN NUMBER
	, p_duration	IN NUMBER
	, p_load_date	IN DATE
	)
	AS	
	BEGIN
	
		--1.For to store DHR data in Staging table ST408_VENDOR_DHR_BY_PART
		-- as per new forecast formula (to find FCST hist rec Avg need 12 month data)
		-- So, hardcode the values.
		gm_sav_dhr_stage_data(p_company_id, 12, p_load_date);

		--2.For To store WO Data in Stage table ST402_VENDOR_WO_BY_PART
		gm_sav_wo_stage_data(p_company_id,p_duration,p_load_date);

		--3.For to store top 2 vendors details in stage table ST402_TOP_2_VENDOR_DTLS
		gm_find_top2_vendor_dtls();

		--4.For to store active price partnumbers in stage table ST405_VENDOR_ACTIVE_PRICING
		gm_sav_vendor_active_price_stage(p_company_id);

    END gm_sav_process_stage_main;
    
    /*******************************************************
	* To Load DHR staging table
	*******************************************************/
	PROCEDURE gm_sav_dhr_stage_data(
	  p_company_id	IN NUMBER
	, p_duration	IN NUMBER
	, p_load_date	IN DATE
	)
	AS	
	BEGIN
		
		INSERT INTO st408_vendor_dhr_by_part (c301_vendor_id,c205_part_number_id,c408_created_date,c408_dhr_qty)
		SELECT c301_vendor_id, c205_part_number_id, TRUNC(c408_created_date,'month'), nvl(c408_shipped_qty,0) 
		FROM t408_dhr
		WHERE c408_void_fl           IS NULL
		AND TRUNC(c408_created_date) >= TRUNC (add_months (p_load_date, - p_duration), 'month')
		AND TRUNC(c408_created_date)  < p_load_date
		AND c1900_company_id          = p_company_id
		ORDER BY c408_created_date;

    END gm_sav_dhr_stage_data;
    
    /*******************************************************
	* To Load WO Staging table
	*******************************************************/
	PROCEDURE gm_sav_wo_stage_data(
	  p_company_id	IN NUMBER
	, p_duration	IN NUMBER
	, p_load_date	IN DATE
	)
	AS	
	BEGIN
	
		INSERT INTO  st402_vendor_wo_by_part(c301_vendor_id,c205_part_number_id,c402_created_date,c402_wo_qty)
		SELECT c301_vendor_id,c205_part_number_id, TRUNC(c402_created_date,'month'), nvl(c402_qty_ordered,0)
		FROM t402_work_order
		WHERE c402_void_fl           IS NULL
		AND TRUNC(c402_created_date) >= TRUNC (add_months (p_load_date, - p_duration), 'month')
		AND TRUNC(c402_created_date) <= p_load_date
		AND c1900_company_id          = p_company_id
		ORDER BY c402_created_date;

    END gm_sav_wo_stage_data;
    
  	/*******************************************************
	* To Load TOP TWO vendors in staging table
	*******************************************************/	
	PROCEDURE gm_find_top2_vendor_dtls
	AS	
	BEGIN
		
		BEGIN
		INSERT INTO st402_top_2_vendor_dtls(c301_vendor_id,c205_part_number_id,c402_load_date,c402_total_vendor_qty,c402_avg_wo_qty,
											c402_total_part_qty,c402_hist_split_per)
		SELECT toptwo.c301_vendor_id,toptwo.c205_part_number_id,trunc(SYSDATE,'month'),toptwo.c402_wo_qty,
											CASE WHEN toptwo.c402_wo_qty IS NOT NULL AND toptwo.c402_wo_qty>0 THEN
											ROUND((NVL(toptwo.c402_wo_qty,0)/6),0) ELSE 0 END avg_wo_qty,
											NVL(toptot.qty_tot,0), 
											CASE WHEN toptot.qty_tot IS NOT NULL AND toptot.qty_tot>0 THEN 
											ROUND((NVL(toptwo.c402_wo_qty,0)*100/NVL(toptot.qty_tot,0)),0) ELSE 0 END hist_split_per
		FROM
		(SELECT c205_part_number_id,c301_vendor_id,c402_wo_qty,rank_id FROM
		(SELECT c205_part_number_id, c301_vendor_id, c402_wo_qty
		  , RANK () OVER (PARTITION BY c205_part_number_id ORDER BY c402_wo_qty DESC)
		  RANK_DUMMY, row_number () OVER (PARTITION BY c205_part_number_id order by c402_wo_qty 
		    DESC) AS RANK_ID
		   FROM (SELECT c301_vendor_id,c205_part_number_id,SUM(c402_wo_qty)  c402_wo_qty
		        FROM st402_vendor_wo_by_part GROUP BY c301_vendor_id,c205_part_number_id)
		    	)
		    WHERE RANK_ID<=2)TOPTWO,     
		        (SELECT C205_PART_NUMBER_ID,SUM(C402_WO_QTY) QTY_TOT,MAX(C301_VENDOR_ID) C301_VENDOR_ID FROM
		        (SELECT C205_PART_NUMBER_ID, C301_VENDOR_ID, C402_WO_QTY
		          , RANK () OVER (PARTITION BY C205_PART_NUMBER_ID ORDER BY C402_WO_QTY DESC) RANK_DUMMY,
		          row_number () over (partition BY C205_PART_NUMBER_ID order by C402_WO_QTY 
		            DESC) AS RANK_ID
		           FROM (SELECT C301_VENDOR_ID,C205_PART_NUMBER_ID,SUM(C402_WO_QTY)  C402_WO_QTY
		                FROM ST402_VENDOR_WO_BY_PART GROUP BY C301_VENDOR_ID,C205_PART_NUMBER_ID)
		            	)
		            WHERE RANK_ID<=2 
		            GROUP BY C205_PART_NUMBER_ID) TOPTOT  
		            -- TO get total of each top two vendor's qty
		            WHERE TOPTWO.C205_PART_NUMBER_ID = TOPTOT.C205_PART_NUMBER_ID;
		            
		   EXCEPTION WHEN OTHERS  THEN
		   DBMS_OUTPUT.put_line(SQLERRM);
		   END;
		   
		-- To update 2nd vendor id  details        
		gm_upd_top2_second_vendor_dtl();
	
	END gm_find_top2_vendor_dtls;
	
	/*******************************************************
	* To update sencond vendor
	*******************************************************/
	PROCEDURE gm_upd_top2_second_vendor_dtl	
	AS	
	v_pnum t205_part_number.c205_part_number_id%TYPE;
	  CURSOR cur_2vendor_parts
       IS 
            SELECT c205_part_number_id pnum 
            FROM st402_top_2_vendor_dtls 
            GROUP BY c205_part_number_id
			HAVING COUNT(1)>1;
			
		CURSOR cur_get_vendor
        IS 
            SELECT C301_VENDOR_ID vendor_id
            FROM ST402_TOP_2_VENDOR_DTLS 
			WHERE C205_PART_NUMBER_ID = v_pnum;
	BEGIN
		
		FOR vendor2_parts IN cur_2vendor_parts
        LOOP
        	v_pnum := vendor2_parts.pnum;
        	
        	FOR upd_2nd_vendor IN cur_get_vendor
        	LOOP
        		UPDATE ST402_TOP_2_VENDOR_DTLS SET C301_SECOND_VENDOR_ID=upd_2nd_vendor.vendor_id
				WHERE C301_VENDOR_ID<>upd_2nd_vendor.vendor_id
				AND C205_PART_NUMBER_ID=v_pnum;
			        	 
			END LOOP;
				-- reset v_pnum
        		v_pnum := NULL;
		END LOOP;
        
	END gm_upd_top2_second_vendor_dtl;
	
    /*******************************************************
	* To Load vendor active price stage table
	*******************************************************/
	PROCEDURE gm_sav_vendor_active_price_stage(
	  p_company_id	IN 	NUMBER
	)
	AS	
	--
	v_ven_max_wo_qty NUMBER;
	--
	BEGIN
	
		/*
    	INSERT INTO st405_vendor_active_pricing (c301_vendor_id,c205_part_number_id,c405_vendor_price,c405_uom_qty,c405_vendor_moq)
    	SELECT c301_vendor_id,c205_part_number_id,max(nvl(c405_cost_price,0)),max(nvl(c405_uom_qty,0)),0    
		FROM t405_vendor_pricing_details
    	WHERE c405_active_fl = 'Y'
    	AND c1900_company_id = p_company_id 	
        AND c405_void_fl IS NULL
        GROUP BY c301_vendor_id,c205_part_number_id;
		*/
		--
		SELECT NVL(get_rule_value ('MAX_WO_QTY', 'TTP_VEN_MAX_WOQTY'), 25000) INTO v_ven_max_wo_qty FROM DUAL;
		--
		-- PC-298, Fetching vendor MOQ value from  t3010_vendor_moq table
		-- PC-295, Fetching pricing id value from t405_vendor_pricing_details table
		-- PC-300, Fetching pre_qty & curr_qty as c405_qty_quoted-1 in sub query for picking vendor price by quoted qty
		
		INSERT INTO st405_vendor_active_pricing (c301_vendor_id,c205_part_number_id,c405_vendor_price,c405_uom_qty
		,c405_vendor_moq,c405_qty_quoted,c405_from_qty,c405_to_qty,c405_pricing_id)
SELECT c301_vendor_id,c205_part_number_id,c405_cost_price,c405_uom_qty,NVL(c405_vendor_moq,0),c405_qty_quoted,from_qty,to_qty,price_id
FROM
	(SELECT c301_vendor_id,c205_part_number_id,c405_cost_price,c405_uom_qty
	,vendor_moq c405_vendor_moq,c405_qty_quoted,DECODE(temp_qty,0,1,pre_qty+1) from_qty,DECODE(curr_qty,0,v_ven_max_wo_qty,curr_qty) as to_qty
	,price_id
       FROM(
       SELECT t405.c301_vendor_id,t405.c205_part_number_id,c405_qty_quoted,c405_cost_price, c405_uom_qty,
			LAG(c405_qty_quoted,1,0) over (PARTITION BY t405.c301_vendor_id,t405.c205_part_number_id ORDER BY c405_qty_quoted) as temp_qty,
			LAG(c405_qty_quoted,0,0) over (PARTITION BY t405.c301_vendor_id,t405.c205_part_number_id ORDER BY c405_qty_quoted) as pre_qty,
			LEAD(c405_qty_quoted,1,0) over (PARTITION BY t405.c301_vendor_id,t405.c205_part_number_id ORDER BY c405_qty_quoted)  as curr_qty,
			t3010.c3010_moq  vendor_moq,t405.c405_pricing_id price_id
			  FROM   
			    t405_vendor_pricing_details t405, (
			        SELECT c301_vendor_id,c205_part_number_id FROM (
			        SELECT c301_vendor_id,c205_part_number_id,count(1) 
					FROM t405_vendor_pricing_details
			    	WHERE c405_active_fl = 'Y'
			    	AND c1900_company_id = p_company_id 	
			        AND c405_void_fl IS NULL
			        GROUP BY c301_vendor_id,c205_part_number_id
			        HAVING COUNT(1)=1) 
			        ) oneprice,t3010_vendor_moq t3010
			    	WHERE t405.c205_part_number_id=oneprice.c205_part_number_id
                    AND t405.c301_vendor_id=oneprice.c301_vendor_id
                    AND t405.c205_part_number_id   = T3010.C205_Part_Number_Id(+)
		 			AND t405.C301_Vendor_Id        = T3010.C301_Vendor_Id(+)
					AND T3010.C3010_Void_Fl(+) IS NULL
                    AND c405_active_fl = 'Y'
			    	AND c1900_company_id = p_company_id 	
			        AND c405_void_fl IS NULL
		 GROUP BY t405.c301_vendor_id,t405.c205_part_number_id,c405_qty_quoted,c405_cost_price ,c405_uom_qty,t3010.c3010_moq,t405.c405_pricing_id
         ORDER BY t405.c301_vendor_id,t405.c205_part_number_id)
UNION ALL
	SELECT c301_vendor_id,c205_part_number_id,c405_cost_price,c405_uom_qty
	,vendor_moq c405_vendor_moq,c405_qty_quoted,decode(temp_qty,0,1,pre_qty+1) from_qty,decode(curr_qty,0,v_ven_max_wo_qty,curr_qty) as to_qty
	,price_id
       FROM(
       SELECT t405.c301_vendor_id,t405.c205_part_number_id,c405_qty_quoted,c405_cost_price, c405_uom_qty,
			LAG(c405_qty_quoted,1,0) over (PARTITION BY t405.c301_vendor_id,t405.c205_part_number_id ORDER BY c405_qty_quoted) as temp_qty,
			LAG(c405_qty_quoted-1,0,0) over (PARTITION BY t405.c301_vendor_id,t405.c205_part_number_id ORDER BY c405_qty_quoted) as pre_qty,
			LEAD(c405_qty_quoted-1,1,0) over (PARTITION BY t405.c301_vendor_id,t405.c205_part_number_id ORDER BY c405_qty_quoted)  as curr_qty,
			t3010.c3010_moq  vendor_moq,t405.c405_pricing_id price_id
			  FROM   
			    t405_vendor_pricing_details t405, (
			        SELECT c301_vendor_id,c205_part_number_id FROM (
			        SELECT c301_vendor_id,c205_part_number_id,count(1) 
					FROM t405_vendor_pricing_details
			    	WHERE c405_active_fl = 'Y'
			    	AND c1900_company_id = p_company_id 	
			        AND c405_void_fl IS NULL
			        GROUP BY c301_vendor_id,c205_part_number_id
			        HAVING COUNT(1)>1) 
			        ) oneprice,t3010_vendor_moq t3010
			    	WHERE t405.c205_part_number_id=oneprice.c205_part_number_id
                    AND t405.c301_vendor_id=oneprice.c301_vendor_id
                    AND c405_active_fl = 'Y'
                    AND t405.c205_part_number_id   = T3010.C205_Part_Number_Id(+)
		 			AND t405.C301_Vendor_Id        = T3010.C301_Vendor_Id(+)
		 			AND T3010.C3010_Void_Fl(+) IS NULL
			    	AND c1900_company_id = p_company_id 	
			        AND c405_void_fl IS NULL
                    AND t405.c405_qty_quoted IS NOT NULL
		 GROUP BY t405.c301_vendor_id,t405.c205_part_number_id,c405_qty_quoted,c405_cost_price ,c405_uom_qty,t3010.c3010_moq,t405.c405_pricing_id
         ORDER BY t405.c301_vendor_id,t405.c205_part_number_id)
         );

    END gm_sav_vendor_active_price_stage;
    
    /*******************************************
     * To Load ttp summary stage table
     ********************************************/  
     PROCEDURE  gm_sav_ttp_summary_stage_data(
     p_ttp_detail_id   IN 	t4052_ttp_detail.c4052_ttp_detail_id%TYPE,
     p_load_date       IN 	DATE)
     
     AS
     
     BEGIN
	   
	    --Read the data from t4055 table and stage into st4055
	  	INSERT INTO st4055_ttp_summary (c4052_ttp_detail_id, c4055_load_date,c205_part_number_id,c205_part_num_desc,c205_crossover_fl,
	  									c4055_total_required_qty,c4055_total_order_qty,c901_level_id, c901_level_value, c251_net_on_hand)
        SELECT t4055.c4052_ttp_detail_id,t4055.c4055_load_date,t4055.c205_part_number_id,t4055.c205_part_num_desc,
        	   t205.c205_crossover_fl,nvl(t4055.c4055_total_required_qty,0),nvl(t4055.c4055_total_order_qty,0),t4055.c901_level_id,
        	   t4055.c901_level_value, st251.c251_net_on_hand
        FROM   t4055_ttp_summary t4055,t205_part_number t205, st251_inventory_lock_details st251
        WHERE  t4055.c205_part_number_id = t205.c205_part_number_id
        AND    c4055_load_date = p_load_date
        AND    c4052_ttp_detail_id = p_ttp_detail_id
        AND    t4055.C205_PART_NUMBER_ID = st251.C205_PART_NUMBER_ID  (+)
        AND  t205.c205_crossover_fl IS NULL
        AND    c901_level_id = 102580 --Company
        AND    C4055_VOID_FL is NULL;
	 END gm_sav_ttp_summary_stage_data;

/*******************************************
  * To Load ttp multipart summary stage table
********************************************/
    
PROCEDURE gm_sav_ttp_multipart_summary_stage_data(
    p_ttp_detail_id IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE,
    p_load_date     IN DATE )	 
    
     AS
     
     BEGIN
	   
	    --Read the data from t4055 table and stage into st4055
	  	INSERT INTO st4055_ttp_summary (c4052_ttp_detail_id, c4055_load_date,c205_part_number_id,c205_part_num_desc,c205_crossover_fl,
	  									c4055_total_required_qty,c4055_total_order_qty,c901_level_id, c901_level_value, c251_net_on_hand)
        SELECT t4055.c4052_ttp_detail_id,t4055.c4055_load_date,t4055.c205_part_number_id,t4055.c205_part_num_desc,
        	   t205.c205_crossover_fl,nvl(t4055.c4055_total_required_qty,0),nvl(t4055.c4055_total_order_qty,0),t4055.c901_level_id,
        	   t4055.c901_level_value, st251.c251_net_on_hand
        FROM   t4055_ttp_summary t4055,t205_part_number t205, st251_inventory_lock_details st251
        WHERE  t4055.c205_part_number_id = t205.c205_part_number_id
        AND    t4055.c4055_load_date = p_load_date
        AND    t4055.c4052_ttp_detail_id = p_ttp_detail_id
        AND    t4055.C205_PART_NUMBER_ID = st251.C205_PART_NUMBER_ID  (+)
        AND    t205.c205_crossover_fl  = 'Y' -- only multi part
        AND    c901_level_id = 102580 --Company
        AND    C4055_VOID_FL is NULL;
        
	 END gm_sav_ttp_multipart_summary_stage_data;
END gm_pkg_oppr_ld_ttp_by_vendor_stage;
/