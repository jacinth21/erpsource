CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ld_ttp_by_vendor_temp_load
IS


/*******************************************************
 * Purpose: This pkg used for to load GOP monthend Load staging tables which will have only six months data
 *
 * modification history
 * ====================
 * Person		 Date	Comments
 * ---------   ------	------------------------------------------
 * psrinivasan    20191116  Initial Version
 *******************************************************/

	PROCEDURE gm_sav_temp_data_main (p_load_date IN DATE)
	AS
	
	BEGIN
		--1)Read from stage dhr table and write into temp dhr table
		gm_sav_dhr_temp_data(p_load_date);

		--2)Read from stage wo table and write into temp wo table
		gm_sav_wo_temp_data(p_load_date);

		--3)Read from stop2,tempwo,tempdhr and sactivepricing table and write into temp_vendor_capacity_summary_dtls
		gm_sav_vendor_summary_temp_data();

		--4)Read from st520 and store temp_set_priority_dtls
		gm_sav_temp_set_priority_by_month(p_load_date);
		
	END gm_sav_temp_data_main;
	/*******************************************************
	* Purpose: This procedure will read from dhr stage to dhr temp
	*******************************************************/
	PROCEDURE gm_sav_dhr_temp_data (p_load_date IN DATE)
	AS
	
	BEGIN
	
		INSERT INTO temp_vendor_dhr_dtls (c301_vendor_id,c205_part_number_id,temp_dhr_qty_mon_01,temp_dhr_qty_mon_02,
										  temp_dhr_qty_mon_03,temp_dhr_qty_mon_04,temp_dhr_qty_mon_05,temp_dhr_qty_mon_06)
		SELECT c301_vendor_id,c205_part_number_id
			  ,max(temp_dhr_qty_mon_01) temp_dhr_qty_mon_01
			  ,max(temp_dhr_qty_mon_02) temp_dhr_qty_mon_02
			  ,max(temp_dhr_qty_mon_03) temp_dhr_qty_mon_03
			  ,max(temp_dhr_qty_mon_04) temp_dhr_qty_mon_04
			  ,max(temp_dhr_qty_mon_05) temp_dhr_qty_mon_05
			  ,max(temp_dhr_qty_mon_06) temp_dhr_qty_mon_06
		FROM
		  (SELECT c301_vendor_id,c205_part_number_id
		       , (CASE WHEN c408_created_date = ADD_MONTHS(TRUNC(p_load_date , 'month'),-6) THEN dhr_qty ELSE 0 END) temp_dhr_qty_mon_01
		       , (CASE WHEN c408_created_date = ADD_MONTHS(TRUNC(p_load_date , 'month'),-5)  THEN dhr_qty ELSE 0 END) temp_dhr_qty_mon_02
		       , (CASE WHEN c408_created_date = ADD_MONTHS(TRUNC(p_load_date , 'month'),-4)  THEN dhr_qty ELSE 0 END) temp_dhr_qty_mon_03
		       , (CASE WHEN c408_created_date = ADD_MONTHS(TRUNC(p_load_date , 'month'),-3)  THEN dhr_qty ELSE 0 END) temp_dhr_qty_mon_04
		       , (CASE WHEN c408_created_date = ADD_MONTHS(TRUNC(p_load_date , 'month'),-2)  THEN dhr_qty ELSE 0 END) temp_dhr_qty_mon_05
		       , (CASE WHEN c408_created_date = ADD_MONTHS(TRUNC(p_load_date , 'month'),-1)  THEN dhr_qty ELSE 0 END) temp_dhr_qty_mon_06
				FROM
        		(     
        			SELECT   c301_vendor_id,c205_part_number_id,c408_created_date,sum(NVL(c408_dhr_qty, 0)) dhr_qty
        			FROM     st408_vendor_dhr_by_part
        			GROUP BY c301_vendor_id,c205_part_number_id,c408_created_date) 
        )GROUP BY c301_vendor_id,c205_part_number_id;

    END gm_sav_dhr_temp_data;
    
    /*******************************************************
	* Purpose: This procedure will read from wo stage to wo temp
	*******************************************************/
    PROCEDURE gm_sav_wo_temp_data (p_load_date IN DATE)
	AS
	
	BEGIN
	
		INSERT INTO temp_vendor_wo_dtls (c301_vendor_id,c205_part_number_id,temp_wo_qty_mon_01,temp_wo_qty_mon_02,
										 temp_wo_qty_mon_03,temp_wo_qty_mon_04,temp_wo_qty_mon_05,temp_wo_qty_mon_06)
		SELECT     c301_vendor_id,c205_part_number_id
       		      ,max(temp_wo_qty_mon_01) temp_wo_qty_mon_01
			      ,max(temp_wo_qty_mon_02) temp_wo_qty_mon_02
			      ,max(temp_wo_qty_mon_03) temp_wo_qty_mon_03
			      ,max(temp_wo_qty_mon_04) temp_wo_qty_mon_04
			      ,max(temp_wo_qty_mon_05) temp_wo_qty_mon_05
			      ,max(temp_wo_qty_mon_06) temp_wo_qty_mon_06
        FROM (
        		SELECT c301_vendor_id,c205_part_number_id
		       , (CASE WHEN c402_created_date = ADD_MONTHS(TRUNC(p_load_date , 'month'),-6) THEN wo_qty ELSE 0 END)  temp_wo_qty_mon_01   
		       , (CASE WHEN c402_created_date = ADD_MONTHS(TRUNC(p_load_date , 'month'),-5)  THEN wo_qty ELSE 0 END) temp_wo_qty_mon_02
		       , (CASE WHEN c402_created_date = ADD_MONTHS(TRUNC(p_load_date , 'month'),-4)  THEN wo_qty ELSE 0 END) temp_wo_qty_mon_03
		       , (CASE WHEN c402_created_date = ADD_MONTHS(TRUNC(p_load_date , 'month'),-3)  THEN wo_qty ELSE 0 END) temp_wo_qty_mon_04
		       , (CASE WHEN c402_created_date = ADD_MONTHS(TRUNC(p_load_date , 'month'),-2)  THEN wo_qty ELSE 0 END) temp_wo_qty_mon_05
		       , (CASE WHEN c402_created_date = ADD_MONTHS(TRUNC(p_load_date , 'month'),-1)  THEN wo_qty ELSE 0 END) temp_wo_qty_mon_06
		FROM
        (     
        SELECT c301_vendor_id,c205_part_number_id,c402_created_date,sum(NVL(c402_wo_qty, 0)) wo_qty
        FROM st402_vendor_wo_by_part
        GROUP BY c301_vendor_id,c205_part_number_id,c402_created_date)
        )GROUP BY c301_vendor_id,c205_part_number_id;

    END gm_sav_wo_temp_data;
  
    /********************************************************************************************************************
	* Purpose: This procedure will read from TEMP_DHR, TEMP_WO and VENDOR STAGE TOP2 to TEMP_VENDOR_CAPACITY_SUMMARY_DTLS
	*********************************************************************************************************************/
    PROCEDURE gm_sav_vendor_summary_temp_data
	AS
	
	BEGIN
		
		INSERT INTO temp_vendor_capacity_summary_dtls(c301_vendor_id,c301_vendor_name,temp_load_date,c205_part_number_id,
		temp_vendor_moq,temp_vendor_price,temp_hist_receipt_month_01,temp_hist_receipt_month_02,temp_hist_receipt_month_03,
		temp_hist_receipt_month_04,temp_hist_receipt_month_05,temp_hist_receipt_month_06,temp_hist_po_month_01,temp_hist_po_month_02,
		temp_hist_po_month_03,temp_hist_po_month_04,temp_hist_po_month_05,temp_hist_po_month_06,temp_history_split_per,
		temp_second_vendor_id,temp_second_vendor_name,temp_history_po_6_mon_avg_qty)		
		SELECT st402.c301_vendor_id,get_vendor_name(st402.c301_vendor_id),st402.c402_load_date,st402.c205_part_number_id
		, NVL(t3010.c3010_moq,0)--PMT-50039-MOQ_Implement_in _TTP_By_Vendor
		,'' --vendor price 
		--, st405.c405_vendor_moq,
		-- st405.c405_vendor_price,
		   ,    nvl(temp_dhr.temp_dhr_qty_mon_01,0),nvl(temp_dhr.temp_dhr_qty_mon_02,0),nvl(temp_dhr.temp_dhr_qty_mon_03,0)
		   ,nvl(temp_dhr.temp_dhr_qty_mon_04,0),nvl(temp_dhr.temp_dhr_qty_mon_05,0),
		   nvl(temp_dhr.temp_dhr_qty_mon_06,0),
			   nvl(temp_wo.temp_wo_qty_mon_01,0),nvl(temp_wo.temp_wo_qty_mon_02,0),nvl(temp_wo.temp_wo_qty_mon_03,0)
			   ,nvl(temp_wo.temp_wo_qty_mon_04,0),nvl(temp_wo.temp_wo_qty_mon_05,0),nvl(temp_wo.temp_wo_qty_mon_06,0),
			   st402.c402_hist_split_per,st402.c301_second_vendor_id,get_vendor_name(st402.c301_second_vendor_id),nvl(st402.c402_avg_wo_qty,0)
		FROM st402_top_2_vendor_dtls st402, temp_vendor_wo_dtls temp_wo, temp_vendor_dhr_dtls temp_dhr,t3010_vendor_moq t3010
		-- As the st405 will have multiple rows for each vendor, part combination, we cannot use this table as it will give cartesian output.
		--, st405_vendor_active_pricing st405
        WHERE 
        /*
         st405.c205_part_number_id = temp_dhr.c205_part_number_id(+) AND
         st405.c205_part_number_id = temp_wo.c205_part_number_id(+) AND 
         st402.c205_part_number_id= st405.c205_part_number_id AND
         st405.c301_vendor_id = temp_dhr.c301_vendor_id(+)
         AND st405.c301_vendor_id = temp_wo.c301_vendor_id(+)
           AND st402.c301_vendor_id= st405.c301_vendor_id
         */
        	st402.c205_part_number_id = temp_dhr.c205_part_number_id(+) 
         AND st402.c205_part_number_id = temp_wo.c205_part_number_id(+) 
         AND st402.c301_vendor_id = temp_dhr.c301_vendor_id(+)
         AND st402.c301_vendor_id = temp_wo.c301_vendor_id(+)
         AND st402.c205_part_number_id   = T3010.C205_Part_Number_Id(+)
		 AND st402.C301_Vendor_Id        = T3010.C301_Vendor_Id(+)
		 AND T3010.C3010_Void_Fl(+) IS NULL;
         
	
	END gm_sav_vendor_summary_temp_data;
	
	/*******************************************************************************
	* Purpose: This procedure will read from set priority stage to set priority temp
	********************************************************************************/
    PROCEDURE gm_sav_temp_set_priority_by_month (p_load_date IN DATE)
	AS
	
	BEGIN
		
		INSERT INTO temp_set_priority_dtls(c205_part_number_id,c520_previous_sp_qty,c520_future_sp_qty_01,c520_future_sp_qty_02,c520_future_sp_qty_03,
		c520_future_sp_qty_04,c520_future_sp_qty_05,c520_future_sp_qty_06
		--,c520_previous_month_flag
		)
		SELECT c205_part_number_id, sum(c520_previous_sp_qty)
			   , sum(CASE WHEN c520_required_date = ADD_MONTHS(TRUNC(p_load_date , 'month'),0) THEN sp_qty ELSE 0 END) c520_future_sp_qty_01 
			   , sum(CASE WHEN c520_required_date = ADD_MONTHS(TRUNC(p_load_date , 'month'),1) THEN sp_qty ELSE 0 END) c520_future_sp_qty_02
			   , sum(CASE WHEN c520_required_date = ADD_MONTHS(TRUNC(p_load_date , 'month'),2) THEN sp_qty ELSE 0 END) c520_future_sp_qty_03
			   , sum(CASE WHEN c520_required_date = ADD_MONTHS(TRUNC(p_load_date , 'month'),3) THEN sp_qty ELSE 0 END) c520_future_sp_qty_04
			   , sum(CASE WHEN c520_required_date = ADD_MONTHS(TRUNC(p_load_date , 'month'),4) THEN sp_qty ELSE 0 END) c520_future_sp_qty_05
			   , sum(CASE WHEN c520_required_date = ADD_MONTHS(TRUNC(p_load_date , 'month'),5) THEN sp_qty ELSE 0 END) c520_future_sp_qty_06
			   --, c520_previous_month_flag  
		FROM
		(
			SELECT c205_part_number_id,NVL(c520_previous_sp_qty, 0) c520_previous_sp_qty,c520_required_date,c520_previous_month_flag
			,sum(nvl(c520_future_sp_qty, 0)) sp_qty
			FROM st520_request_priority_dtls
			GROUP BY c205_part_number_id,c520_required_date,c520_previous_sp_qty,c520_previous_month_flag
		)
		GROUP BY c205_part_number_id;

	END gm_sav_temp_set_priority_by_month;
	
	/***************************************************************************
	 * This procedure is used to save temp_3month_vendor_historical_receipt data
	 ****************************************************************************/
	PROCEDURE gm_sav_capa_summary_dtls_tmp(
		p_load_date	  				IN   DATE	
	  , p_dhr_avg_duration	 		IN	 NUMBER
	  , p_company_id		IN	 NUMBER
	)	
	AS	
	BEGIN
		--1)Read data from temp_vendor_dhr_dtls and store into temp_3month_vendor_historical_receipt
		gm_sav_temp_vendor_receipt_by_mon(p_load_date,p_dhr_avg_duration,p_company_id);

		--2)Update forecast receipt month table(temp_3month_vendor_historical_receipt)
		gm_upd_forecast_receipt_qty(p_load_date,p_dhr_avg_duration,p_company_id);
		
	END gm_sav_capa_summary_dtls_tmp;
    
	/****************************************************************************
	 * This procedure is used to save temp_3month_vendor_historical_receipt data
	*****************************************************************************/
	PROCEDURE gm_sav_temp_vendor_receipt_by_mon(
		p_load_date	  				IN 	 DATE	
	  , p_dhr_avg_duration	 		IN	 NUMBER
	  , p_company_id       IN NUMBER 
	)	
	AS	
	v_last_12_mon_working_day_cnt NUMBER;
	BEGIN
		-- to get last 12 month working day count
		BEGIN
			SELECT sum(c1904_total_no_working_days)
			INTO v_last_12_mon_working_day_cnt
	 		FROM t1904_company_working_day_by_mon
    		WHERE c1904_month_date BETWEEN ADD_MONTHS(p_load_date,-12) AND ADD_MONTHS(p_load_date,-1)
    		AND  c1900_company_id = p_company_id;
    		
		EXCEPTION WHEN  NO_DATA_FOUND  THEN
    			v_last_12_mon_working_day_cnt := 0; 
    	END;
    	
		--Get Last 12 months data and store into appropriate columns
		INSERT INTO temp_3month_vendor_historical_receipt (temp_vendor_id,c205_part_number_id,temp_dhr_qty
		, temp_historical_receipt_3_mon_avg_qty
		-- to update the hist receipt avg qty
		,temp_prev_hist_receipt_avg_qty_01,temp_prev_hist_receipt_avg_qty_02,temp_prev_hist_receipt_avg_qty_03)
        SELECT   st408.c301_vendor_id, st408.c205_part_number_id, sum(NVL(c408_dhr_qty, 0)) dhr_qty
        ,ROUND((sum(NVL(c408_dhr_qty, 0))/v_last_12_mon_working_day_cnt), 2) avg_qty
        --
        , sum(nvl(temp_dhr_qty_mon_04,0)),sum(nvl(temp_dhr_qty_mon_05,0)),sum(nvl(temp_dhr_qty_mon_06,0))
        FROM     st408_vendor_dhr_by_part st408, temp_vendor_dhr_dtls tmp
        WHERE st408.c301_vendor_id = tmp.c301_vendor_id (+)
        AND st408.c205_part_number_id = tmp.c205_part_number_id (+)
        GROUP BY st408.c301_vendor_id,st408.c205_part_number_id;
        
        -- to update the working day cnt
        UPDATE temp_3month_vendor_historical_receipt
        SET temp_total_working_days = v_last_12_mon_working_day_cnt;
        
	END gm_sav_temp_vendor_receipt_by_mon;	
	
	/******************************************************************************
	 * This procedure is used to update temp_3month_vendor_historical_receipt data
	*******************************************************************************/
	PROCEDURE gm_upd_forecast_receipt_qty(
		 p_load_date	  IN DATE	
		,p_dhr_avg_duration IN NUMBER
		,p_company_id   IN NUMBER
	)	
	AS
		total_working_days_pre_first_month NUMBER;
		total_working_days_pre_second_month NUMBER;
		total_working_days_pre_third_month NUMBER;
		-- future month
		total_working_days_curr_month NUMBER;
		total_working_days_future_1_month NUMBER;
		total_working_days_future_2_month NUMBER;
		total_working_days_future_3_month NUMBER;
		total_working_days_future_4_month NUMBER;
		total_working_days_future_5_month NUMBER;
		
	BEGIN 
		--Get no of working days based on month
		get_no_working_day_by_mon(ADD_MONTHS(p_load_date,-1),p_company_id,total_working_days_pre_first_month);  
		get_no_working_day_by_mon(ADD_MONTHS(p_load_date,-2),p_company_id,total_working_days_pre_second_month);
		get_no_working_day_by_mon(ADD_MONTHS(p_load_date,-3),p_company_id,total_working_days_pre_third_month);
		--
		get_no_working_day_by_mon(p_load_date, p_company_id,total_working_days_curr_month);
		get_no_working_day_by_mon(ADD_MONTHS(p_load_date, 1),p_company_id,total_working_days_future_1_month);
		get_no_working_day_by_mon(ADD_MONTHS(p_load_date, 2),p_company_id,total_working_days_future_2_month);
		get_no_working_day_by_mon(ADD_MONTHS(p_load_date, 3),p_company_id,total_working_days_future_3_month);
		get_no_working_day_by_mon(ADD_MONTHS(p_load_date, 4),p_company_id,total_working_days_future_4_month);
		get_no_working_day_by_mon(ADD_MONTHS(p_load_date, 5),p_company_id,total_working_days_future_5_month);
	
		--Update forecaste history quantity month based 3monthAverage qty * no of working days based on month
			update temp_3month_vendor_historical_receipt
			   set temp_fcst_hist_qty_mon_01 = ROUND(NVL(temp_historical_receipt_3_mon_avg_qty * total_working_days_curr_month, 0)),
				   temp_fcst_hist_qty_mon_02 = ROUND(NVL(temp_historical_receipt_3_mon_avg_qty * total_working_days_future_1_month, 0)),
				   temp_fcst_hist_qty_mon_03 = ROUND(NVL(temp_historical_receipt_3_mon_avg_qty * total_working_days_future_2_month, 0)),
				   temp_fcst_hist_qty_mon_04 = ROUND(NVL(temp_historical_receipt_3_mon_avg_qty * total_working_days_future_3_month, 0)),
				   temp_fcst_hist_qty_mon_05 = ROUND(NVL(temp_historical_receipt_3_mon_avg_qty * total_working_days_future_4_month, 0)),
				   temp_fcst_hist_qty_mon_06 = ROUND(NVL(temp_historical_receipt_3_mon_avg_qty * total_working_days_future_5_month, 0)),
				   -- to update previous hist rec
				   temp_prev_hist_receipt_avg_qty_01 = ROUND(temp_prev_hist_receipt_avg_qty_01 / total_working_days_pre_first_month),
				   temp_prev_hist_receipt_avg_qty_02 = ROUND(temp_prev_hist_receipt_avg_qty_02 / total_working_days_pre_second_month),
				   temp_prev_hist_receipt_avg_qty_03 = ROUND(temp_prev_hist_receipt_avg_qty_03 / total_working_days_pre_third_month)
				   ;
				   
	END gm_upd_forecast_receipt_qty;	
	
	/***************************************************************************
	 * This procedure is used to update temp historical receipt 8 month avg qty
	 ***************************************************************************/
	PROCEDURE gm_tmp_upd_dhr_avg_qty(
		p_dhr_avg_duration 						IN 	NUMBER,
		p_total_working_days_pre_third_month  	IN 	NUMBER,
	    p_total_working_days_pre_second_month 	IN  NUMBER,
	    p_total_working_days_pre_first_month 	IN  NUMBER
	)	
	AS	
	BEGIN
		
		UPDATE temp_3month_vendor_historical_receipt
		   SET temp_historical_receipt_3_mon_avg_qty = NVL(round(((TEMP_PREV_HIST_RECEIPT_AVG_QTY_01 / p_total_working_days_pre_third_month) 
														 + (TEMP_PREV_HIST_RECEIPT_AVG_QTY_02 / p_total_working_days_pre_second_month)
													     + (TEMP_PREV_HIST_RECEIPT_AVG_QTY_03 / p_total_working_days_pre_first_month))/p_dhr_avg_duration,2),0);
															
  	END gm_tmp_upd_dhr_avg_qty;
		
	/**********************************************************
    * This procedure used to find no.of working days for month
    ***********************************************************/
	PROCEDURE get_no_working_day_by_mon (
		p_load_date	IN DATE,
		p_company_id IN t1904_company_working_day_by_mon.c1900_company_id%type,
        p_total_working_days    OUT NUMBER
    )
	AS
		v_total_working_days NUMBER;
	BEGIN

		BEGIN 
			SELECT c1904_total_no_working_days INTO v_total_working_days
	 		FROM t1904_company_working_day_by_mon
    		WHERE c1904_month_date = trunc(p_load_date,'month')
    		AND  c1900_company_id = p_company_id;
          		 p_total_working_days := v_total_working_days;
    	EXCEPTION WHEN  NO_DATA_FOUND  THEN
    			v_total_working_days := 0; 
    	END;

	END get_no_working_day_by_mon;
    /****************************************
     * To Load Stage sales replenish table into
     * Temp Sales replenish table
     ****************************************/
     procedure gm_sav_temp_sales_repl_data(
         p_ttp_detail_id IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE,
         p_load_date IN DATE
     )       
     AS
     BEGIN
	     
	     --
	     
	      INSERT
		   INTO TEMP_SALES_REPL_LOAD
		    (
		        c4052_ttp_detail_id, c205_part_number_id, c4042_sales_replish_qty_01
		      , c4042_sales_replish_qty_02, c4042_sales_replish_qty_03, c4042_sales_replish_qty_04
		      , c4042_sales_replish_qty_05, c4042_sales_replish_qty_06, c4042_sales_replish_qty_07
		      , c4042_sales_replish_qty_08, c4042_sales_replish_qty_09, c4042_sales_replish_qty_10
		      , c4042_sales_replish_qty_11, c4042_sales_replish_qty_12
		    )
		 SELECT t4055.c4052_ttp_detail_id, t4055.c205_part_number_id, t4055.c4055_month_01
		  , t4055.c4055_month_02, t4055.c4055_month_03, t4055.c4055_month_04
		  , t4055.c4055_month_05, t4055.c4055_month_06, t4055.c4055_month_07
		  , t4055.c4055_month_08, t4055.c4055_month_09, t4055.c4055_month_10
		  , t4055.c4055_month_11, t4055.c4055_month_12
		   FROM t4055_ttp_summary t4055, t205_part_number t205
		  WHERE t4055.c205_part_number_id = t205.c205_part_number_id
		    AND t4055.c4055_load_date     = p_load_date
		    AND t4055.c4052_ttp_detail_id = p_ttp_detail_id
		    AND t205.c205_crossover_fl   IS NULL -- non multi part
		    AND t4055.c901_level_id             = 102580 --Company
		    AND t4055.c4055_void_fl            IS NULL; 
	        
         --Update total required qty for TEMP_SALES_REPL_LOAD
		gm_upd_total_required_data_to_temp(p_ttp_detail_id);  
		
		--Update safety stock details for TEMP_SALES_REPL_LOAD
		gm_upd_safety_stock_dtls(p_ttp_detail_id); 
				 
	END gm_sav_temp_sales_repl_data;
	
	
	 /****************************************
  * To Load multipart sales replish data to temp
  ****************************************/
PROCEDURE gm_sav_temp_multipart_sales_repl_data(
    p_ttp_detail_id IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE,
    p_load_date IN DATE )
    AS
    
    BEGIN
	    --
	     
			 INSERT
		   INTO TEMP_SALES_REPL_LOAD
		    (
		        c4052_ttp_detail_id, c205_part_number_id, c4042_sales_replish_qty_01
		      , c4042_sales_replish_qty_02, c4042_sales_replish_qty_03, c4042_sales_replish_qty_04
		      , c4042_sales_replish_qty_05, c4042_sales_replish_qty_06, c4042_sales_replish_qty_07
		      , c4042_sales_replish_qty_08, c4042_sales_replish_qty_09, c4042_sales_replish_qty_10
		      , c4042_sales_replish_qty_11, c4042_sales_replish_qty_12
		    )
		 SELECT t4055.c4052_ttp_detail_id, t4055.c205_part_number_id, t4055.c4055_month_01
		  , t4055.c4055_month_02, t4055.c4055_month_03, t4055.c4055_month_04
		  , t4055.c4055_month_05, t4055.c4055_month_06, t4055.c4055_month_07
		  , t4055.c4055_month_08, t4055.c4055_month_09, t4055.c4055_month_10
		  , t4055.c4055_month_11, t4055.c4055_month_12
		   FROM t4055_ttp_summary t4055, t205_part_number t205
		  WHERE t4055.c205_part_number_id = t205.c205_part_number_id
		    AND t4055.c4055_load_date     = p_load_date
		    AND t4055.c4052_ttp_detail_id = p_ttp_detail_id
		    AND t205.c205_crossover_fl    = 'Y' -- Only multi part
		    AND t4055.c901_level_id             = 102580 --Company
		    AND t4055.c4055_void_fl            IS NULL; 
	    
        --Update total required qty for TEMP_SALES_REPL_LOAD
		gm_upd_total_required_data_to_temp(p_ttp_detail_id);  
		
		--Update safety stock details for TEMP_SALES_REPL_LOAD
		gm_upd_safety_stock_dtls(p_ttp_detail_id); 
		
	    END gm_sav_temp_multipart_sales_repl_data;
	
	/********************************************************************************
 	* This procedure is used to update total required qty to temp sales repli table
 	*********************************************************************************/
	procedure gm_upd_total_required_data_to_temp(
		p_ttp_detail_id IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE
	)
	AS

	v_net_on_hand number;
	
	CURSOR sttp_summary_part_num_cur
	IS
		SELECT c205_part_number_id p_num,
				NVL(c4055_total_required_qty, 0) v_tat_req_qty,
				c205_crossover_fl v_cross_fl,
				c251_net_on_hand noh_qty
          FROM st4055_ttp_summary 
         WHERE c4052_ttp_detail_id = p_ttp_detail_id;
	     
	      BEGIN
 
			 FOR  sttp_summary in sttp_summary_part_num_cur
			 loop
			 --

				UPDATE temp_sales_repl_load 
				SET    c4055_total_required_qty = sttp_summary.v_tat_req_qty,
				   	   c205_crossover_fl = sttp_summary.v_cross_fl,
				   	   c251_net_on_hand = sttp_summary.noh_qty
		 		 WHERE c4052_ttp_detail_id = p_ttp_detail_id
		 	       AND c205_part_number_id = sttp_summary.p_num;
		 	       
		    END loop;
		 
	END gm_upd_total_required_data_to_temp;
		  
	/*******************************************************
 	* To update safety stock details on TEMP_SALES_REPL_LOAD
 	*********************************************************/
	procedure gm_upd_safety_stock_dtls(
		p_ttp_detail_id IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE
	)
	AS		 
		 v_safety_stock_qty_01 NUMBER;
		 v_safety_stock_qty_02 NUMBER;
		 v_safety_stock_qty_03 NUMBER;
		 v_safety_stock_qty_04 NUMBER;
		 v_safety_stock_qty_05 NUMBER;
		 v_safety_stock_qty_06 NUMBER;
		 --
	CURSOR sales_repl_qty_cur
	IS
		 SELECT C205_PART_NUMBER_ID partnum, 
		 		C4042_SALES_REPLISH_QTY_01 SALES_REPLISH_QTY_01,
				C4042_SALES_REPLISH_QTY_02 SALES_REPLISH_QTY_02,
				C4042_SALES_REPLISH_QTY_03 SALES_REPLISH_QTY_03,
				C4042_SALES_REPLISH_QTY_04 SALES_REPLISH_QTY_04,
				C4042_SALES_REPLISH_QTY_05 SALES_REPLISH_QTY_05,
				C4042_SALES_REPLISH_QTY_06 SALES_REPLISH_QTY_06,
				C4042_SALES_REPLISH_QTY_07 SALES_REPLISH_QTY_07,
				C4042_SALES_REPLISH_QTY_08 SALES_REPLISH_QTY_08,
				C4042_SALES_REPLISH_QTY_09 SALES_REPLISH_QTY_09,
				C4042_SALES_REPLISH_QTY_10 SALES_REPLISH_QTY_10,
				C4042_SALES_REPLISH_QTY_11 SALES_REPLISH_QTY_11,
				C4042_SALES_REPLISH_QTY_12 SALES_REPLISH_QTY_12,
				NVL(C251_NET_ON_HAND, 0)  net_on_hand
		   FROM TEMP_SALES_REPL_LOAD 
		  WHERE C4052_TTP_DETAIL_ID = p_ttp_detail_id;
				
	BEGIN
		
		--
			 FOR sales_repl_qty IN sales_repl_qty_cur
			 loop
					-- 1 cal safety stock - current month
			 		gm_fch_safety_stock_by_part(0, sales_repl_qty.SALES_REPLISH_QTY_02
			 		,sales_repl_qty.SALES_REPLISH_QTY_03,sales_repl_qty.net_on_hand,V_SAFETY_STOCK_QTY_01);
			 		
			 		-- 2 cal safety stock - current month +1
			 		gm_fch_safety_stock_by_part(0, 0
			 		,sales_repl_qty.SALES_REPLISH_QTY_04,sales_repl_qty.SALES_REPLISH_QTY_01,V_SAFETY_STOCK_QTY_02);
			 		
			 		-- 3 cal safety stock - current month +2
			 		gm_fch_safety_stock_by_part(0, 0
			 		,sales_repl_qty.SALES_REPLISH_QTY_05,sales_repl_qty.SALES_REPLISH_QTY_02,V_SAFETY_STOCK_QTY_03);
			 		
			 		-- 4 cal safety stock - current month +3
			 		gm_fch_safety_stock_by_part(0, 0
			 		,sales_repl_qty.SALES_REPLISH_QTY_06,sales_repl_qty.SALES_REPLISH_QTY_03,V_SAFETY_STOCK_QTY_04);
			 		
			 		-- 5 cal safety stock - current month +4
			 		gm_fch_safety_stock_by_part(0, 0
			 		,sales_repl_qty.SALES_REPLISH_QTY_07,sales_repl_qty.SALES_REPLISH_QTY_04,V_SAFETY_STOCK_QTY_05);
			 		
			 		-- 6 cal safety stock - current month +5
			 		gm_fch_safety_stock_by_part(0, 0
			 		,sales_repl_qty.SALES_REPLISH_QTY_08,sales_repl_qty.SALES_REPLISH_QTY_05,V_SAFETY_STOCK_QTY_06);
			 
			 		
			 		UPDATE temp_sales_repl_load 
			 		   SET c4042_safety_stock_qty_01 = v_safety_stock_qty_01,
			 		       c4042_safety_stock_qty_02 = v_safety_stock_qty_02,
			 		       c4042_safety_stock_qty_03 = v_safety_stock_qty_03,
			 		       c4042_safety_stock_qty_04 = v_safety_stock_qty_04,
			 		       c4042_safety_stock_qty_05 = v_safety_stock_qty_05,
			 		       c4042_safety_stock_qty_06 = v_safety_stock_qty_06
			 		 WHERE c205_part_number_id = sales_repl_qty.partnum
			 		 AND  c4052_ttp_detail_id = p_ttp_detail_id;
			 		 
			 END loop;
			 
	END gm_upd_safety_stock_dtls;

	/****************************
 	* To calculate sefety stock 
 	******************************/
	procedure gm_fch_safety_stock_by_part(
	     p_replish_qty_01    IN NUMBER,
	     p_replish_qty_02 IN NUMBER,
	     p_replish_qty_03 IN NUMBER,
	     p_net_on_hand IN NUMBER,
	     p_out_value  OUT  NUMBER
	)
	AS
	v_total_replish_qty NUMBER;
	BEGIN
		     -- ((Sales Repl. CM+1 + CM+2 + CM+3) � Current Month) * Hist Split %
		     v_total_replish_qty := (p_replish_qty_01+p_replish_qty_02+p_replish_qty_03);
		     --
		     IF v_total_replish_qty > p_net_on_hand
		     THEN
		     	v_total_replish_qty := v_total_replish_qty - p_net_on_hand;
		     ELSE
		     	v_total_replish_qty := 0;
		     END IF;
		     --
		     p_out_value := NVL(v_total_replish_qty, 0);
		     --
	END gm_fch_safety_stock_by_part;
	     	     
END gm_pkg_oppr_ld_ttp_by_vendor_temp_load;
/