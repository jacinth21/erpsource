create or replace PACKAGE BODY gm_pkg_oppr_ttp_summary_calc
IS


PROCEDURE gm_ttp_summary_calc_main (
	p_ttp_detail_id 	 IN T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE ,
	p_lvl_id	         IN T4040_DEMAND_SHEET.c901_level_id%TYPE, -- 102580 , WW
	p_inventoryid	     IN t250_inventory_lock.c250_inventory_lock_id%TYPE,
    p_lvl_value         IN T4040_DEMAND_SHEET.c901_level_value%TYPE
  	)
AS




 cur_parent_sub_part	    TYPES.cursor_type;
 v_parentpart				T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
 v_family_part			T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
 v_sub_part					T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
 v_family					T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
 v_sub_part_qty				NUMBER;
 v_actual_qty				NUMBER;
 v_level					NUMBER;
 v_db_calc_lead_time		VARCHAR2(100);
 v_calc_lead_time			NUMBER;
 v_lead_time				NUMBER :=7;
 v_sub_part_lt				NUMBER;
 v_lead_time_wks			NUMBER;
 v_lead_time_US_Adj_qty		NUMBER;
 v_lead_time_OUS_Adj_qty	NUMBER;
 v_forecast_month			NUMBER;
 v_current_period			DATE;
 v_excs_wks					NUMBER(10,2);
 v_dflt_lead_time			NUMBER :=7; -- To take from Rules.
 v_add_rem_month			NUMBER :=1 ; -- Default Should be 1.
 v_prev_month				DATE;
 v_months_to_add			NUMBER(10,2);
 v_def_months_to_add		NUMBER;
 v_US_Parent_need			NUMBER(10,2);
 v_OUS_Parent_need			NUMBER(20,2);
 v_lead_time_pointer		NUMBER :=4;-- Lead time calculation to start checking from the 5th month forecast.
 v_forecast_us				T4055_TTP_SUMMARY.C4055_US_FORECAST_QTY%TYPE;
 v_forecast_ous				T4055_TTP_SUMMARY.C4055_OUS_FORECAST_QTY%TYPE;
 v_forecast					T4055_TTP_SUMMARY.C4055_FORECAST_QTY%TYPE;
 v_PNeed_US					NUMBER(10,2);
 v_PNeed_OUS				NUMBER(20,2);
 v_ous_otn					T4055_TTP_SUMMARY.C4023_OUS_OTN%TYPE;
 v_us_otn					T4055_TTP_SUMMARY.C4023_US_OTN%TYPE;
 v_set_par					T4055_TTP_SUMMARY.C4055_SET_PAR%TYPE;
 v_tpr_us					NUMBER;
 v_tpr_ous					NUMBER;
 v_tot_inv					NUMBER;
 v_rw_inv					NUMBER;
 v_open_po					NUMBER;
 v_LTA_US					NUMBER;
 v_LTA_OUS					NUMBER;
 v_rem_inv					NUMBER;
 V_CALCULATED_US_PNEED		NUMBER;
 V_CALCULATED_US_LTA		NUMBER;
 V_CALCULATED_OUS_PNEED		NUMBER;
 V_PARENT_NEED				NUMBER;
 V_CALC_PNEED				NUMBER;
 V_CALC_LTA					NUMBER;
 V_PNEED_WW					NUMBER;
 V_LTA_WW					NUMBER;
 v_pp_pneed_us              NUMBER;
 v_pp_pneed_ous             NUMBER;
 v_tmp						NUMBER;


begin

	SELECT get_rule_value('FORECASTMONTHS','ORDERPLANNING')
		INTO 	v_forecast_month
		FROM 	DUAL;

	SELECT get_rule_value('4000101','LEADTIME')
		INTO 	v_dflt_lead_time
		FROM 	DUAL;

	SELECT DISTINCT C4040_DEMAND_PERIOD_DT
		INTO v_current_period
	FROM T4040_DEMAND_SHEET
	WHERE
	C4052_TTP_DETAIL_ID = p_ttp_detail_id
	--AND c901_level_id=p_lvl_id
	GROUP BY C4040_DEMAND_PERIOD_DT
	;

	SELECT to_number(get_rule_value('MONTHS_TO_ADD','GOP-NON-STERILE-PNED'))
		INTO 	v_def_months_to_add
		FROM 	DUAL;


	DELETE FROM MY_TEMP_KEY_VALUE;

	gm_pkg_oppr_ld_summary.gm_ld_part_info(p_ttp_detail_id,p_lvl_id,p_lvl_value);

	gm_pkg_oppr_ld_summary.gm_ld_us_ous_forecast(p_ttp_detail_id,p_lvl_id,p_lvl_value);


	gm_pkg_oppr_ttp_summary.gm_ld_spec_parent_sub_parts(p_inventoryid,cur_parent_sub_part);
	--gm_pkg_oppr_ttp_summary.gm_ld_parent_sub_parts(p_inventoryid,cur_parent_sub_part);
	LOOP
		FETCH cur_parent_sub_part
			INTO v_parentpart, v_family_part, v_sub_part, v_family, v_sub_part_qty, v_actual_qty, v_level, v_db_calc_lead_time;
    
    EXIT WHEN cur_parent_sub_part%NOTFOUND;
		IF v_parentpart IS NOT NULL AND v_sub_part IS NOT NULL
		THEN
				my_context.set_my_inlist_ctx(v_db_calc_lead_time);


				v_excs_wks :=0;
				v_lead_time_pointer :=4;
				v_lead_time_US_Adj_qty :=0;
				v_lead_time_OUS_Adj_qty :=0;
				v_LTA_US:=0;

				--From the comma seperated lead time,get the sum.
				SELECT SUM(TOKEN)
				INTO
				v_calc_lead_time
				FROM v_in_list
				WHERE TOKEN IS NOT NULL;

			--	DBMS_OUTPUT.PUT_LINE(' Begin :v_parentpart:'||v_parentpart||':v_sub_part-1:'||v_sub_part ||':v_family_part:'||v_family_part||':v_calc_lead_time:'||v_calc_lead_time);
    
				SELECT C255_LEAD_TIME
				INTO v_lead_time
				FROM T4055_TTP_SUMMARY
				WHERE C4055_VOID_FL IS NULL
				AND C4052_TTP_DETAIL_ID = p_ttp_detail_id
				AND c901_level_id = p_lvl_id
				AND C205_PART_NUMBER_ID = v_sub_part;
				
				SELECT NVL(C4055_US_FORECAST_QTY,0) , NVL(C4023_US_OTN,0), NVL(C4055_SET_PAR,0), NVL(C4055_OUS_FORECAST_QTY,0),NVL(C4023_OUS_OTN,0),NVL(C255_PARENT_US_LTA,0)
		        ,NVL(C255_PARENT_OUS_LTA,0)
				INTO v_forecast_us, v_US_OTN, v_set_par, v_forecast_ous, v_ous_otn,v_pp_pneed_us,v_pp_pneed_ous
				FROM T4055_TTP_SUMMARY
				WHERE C4055_VOID_FL IS NULL
				AND C4052_TTP_DETAIL_ID = p_ttp_detail_id
				AND c901_level_id = p_lvl_id
				AND C205_PART_NUMBER_ID = v_parentpart;

				

				SELECT NVL((C251_IN_STOCK+C251_OPEN_PO+C251_OPEN_DHR+C251_BUILD_SET),0) , NVL(C251_TPR_US,0), NVL(C251_TPR_OUS,0), NVL(C251_RW_INVENTORY,0),NVL(C251_OPEN_PO,0)
				INTO v_tot_inv, v_tpr_us, v_tpr_ous, v_rw_inv,v_open_po
				FROM T251_INVENTORY_LOCK_DETAILS
				WHERE C250_INVENTORY_LOCK_ID = p_inventoryid
				AND C205_PART_NUMBER_ID = v_parentpart;
       --  DBMS_OUTPUT.PUT_LINE(v_parentpart||'v_forecast_us:'||v_forecast_us ||':v_calc_lead_time:'||v_calc_lead_time||':v_lead_time:'||v_lead_time||':v_open_po:'||v_open_po);

				v_lead_time_wks := v_calc_lead_time + v_lead_time;

				IF (v_family_part = v_sub_part)
				THEN
					SELECT addMoreForecastMonths(v_forecast_month,v_current_period,
										  p_ttp_detail_id,p_lvl_id,v_parentpart,'US_FCST_',
										  v_def_months_to_add)
					INTO v_lead_time_US_Adj_qty
					FROM DUAL;

					SELECT addMoreForecastMonths(v_forecast_month,v_current_period,
										  p_ttp_detail_id,p_lvl_id,v_parentpart,'OUS_FCST_',
										  v_def_months_to_add)
					INTO v_lead_time_OUS_Adj_qty
					FROM DUAL;

				END IF;

				v_excs_wks := v_lead_time_wks - v_dflt_lead_time;

				IF (v_excs_wks <0)
				THEN

					v_excs_wks := v_excs_wks *-1;
					v_add_rem_month := -1;
					v_forecast_month := v_forecast_month -1;

				END IF;

				SELECT ADD_MONTHS(v_current_period ,v_forecast_month)
				INTO v_prev_month
				FROM DUAL;

			--	 DBMS_OUTPUT.PUT_LINE(':v_excs_wks:'||v_excs_wks ||':v_lead_time_US_Adj_qty:'||v_lead_time_US_Adj_qty||':v_lead_time_OUS_Adj_qty:'||v_lead_time_OUS_Adj_qty);

				/* If the v_lead_time_wks = 12, the excess week is 5 (12-7).
				* So we need to take 5th month forecast and 0.25* of 6th month Forecast'
				* If the v_lead_time_wks = 10, the excess week is 3, so we will take 0.75*5th month forecast.
				*/
				FOR i in 1..v_excs_wks
				LOOP
					v_months_to_add := v_months_to_add + 0.25;
					--0.25 denotes One Week Per Month.

					--DBMS_OUTPUT.PUT_LINE(':v_months_to_add:'||v_months_to_add);
					IF (MOD(i,4)=0)
					THEN
					--DBMS_OUTPUT.PUT_LINE(':>>>:'||v_lead_time_pointer);
						v_lead_time_pointer := v_lead_time_pointer+1;
						v_lead_time_US_Adj_qty := v_lead_time_US_Adj_qty +
												   gm_pkg_oppr_ld_summary.get_oppr_mon_fcst_qty(p_ttp_detail_id,p_lvl_id,v_parentpart,'US_FCST_'|| (v_lead_time_pointer));
						v_lead_time_OUS_Adj_qty := v_lead_time_OUS_Adj_qty +
												   gm_pkg_oppr_ld_summary.get_oppr_mon_fcst_qty(p_ttp_detail_id,p_lvl_id,v_parentpart,'OUS_FCST_'|| (v_lead_time_pointer));
						--Take the forecast for the 5th month and move forward based on the Lead Time.
						v_months_to_add :=0;

					END IF;
			--		DBMS_OUTPUT.PUT_LINE(':v_lead_time_US_Adj_qty:'||v_lead_time_US_Adj_qty);
			--		DBMS_OUTPUT.PUT_LINE(':v_lead_time_OUS_Adj_qty:'||v_lead_time_OUS_Adj_qty);
				END LOOP;
        
				IF v_months_to_add >0
				THEN
					v_lead_time_pointer :=v_lead_time_pointer+1;
					
				/*	 DBMS_OUTPUT.PUT_LINE(':v_lead_time_pointer:'||v_lead_time_pointer||':v_months_to_add:'||v_months_to_add||':v_parentpart:'||v_parentpart||':func:'||
					 gm_pkg_oppr_ld_summary.get_oppr_mon_fcst_qty(p_ttp_detail_id,p_lvl_id,v_parentpart,'US_FCST_'|| (v_lead_time_pointer))
					 ||':v_months_to_add:'||v_months_to_add);
					 DBMS_OUTPUT.PUT_LINE('OUS_FCST_ :v_lead_time_pointer:'||v_lead_time_pointer||':v_months_to_add:'||v_months_to_add||':v_parentpart:'||v_parentpart||':func:'||
					 gm_pkg_oppr_ld_summary.get_oppr_mon_fcst_qty(p_ttp_detail_id,p_lvl_id,v_parentpart,'OUS_FCST_'|| (v_lead_time_pointer))
					 ||':v_months_to_add:'||v_months_to_add);
					
					 */
					--Move the Month to one more month so the right forecast will be taken.
					v_lead_time_US_Adj_qty := v_lead_time_US_Adj_qty + gm_pkg_oppr_ld_summary.get_oppr_mon_fcst_qty(p_ttp_detail_id,p_lvl_id,v_parentpart,'US_FCST_'|| (v_lead_time_pointer))
																	 *v_months_to_add;
					v_lead_time_OUS_Adj_qty := v_lead_time_OUS_Adj_qty +
																	gm_pkg_oppr_ld_summary.get_oppr_mon_fcst_qty(p_ttp_detail_id,p_lvl_id,v_parentpart,'OUS_FCST_'|| (v_lead_time_pointer))
																	*v_months_to_add;
				--	 DBMS_OUTPUT.PUT_LINE(':v_lead_time_US_Adj_qty:'||v_lead_time_US_Adj_qty);
				--	 DBMS_OUTPUT.PUT_LINE(':v_lead_time_OUS_Adj_qty:'||v_lead_time_OUS_Adj_qty);
				END IF;

				SELECT
					NVL(get_temp_value(v_family_part||v_sub_part,'PNEED_US'),0),
					NVL(get_temp_value(v_parentpart||v_sub_part,'PNEED_US'),0),
					NVL(get_temp_value(v_family_part|| v_sub_part,'PNEED_OUS'),0),
					NVL(get_temp_value(v_parentpart||v_sub_part,'PNEED_OUS'),0),
					NVL(get_temp_value(v_family_part||v_sub_part, 'LTA_US'),0)
				INTO v_PNeed_US,
		             v_pp_pneed_us,
		             v_PNeed_OUS,
		             v_pp_pneed_ous,
		             v_LTA_US
				FROM DUAL;

			--	 dbms_output.put_line(':v_lead_time_US_Adj_qty:'||v_lead_time_US_Adj_qty||':'||v_forecast_us||':'||v_tpr_us||':'||v_pp_pneed_us||':'||v_set_par||':'||v_us_otn||':'||v_tot_inv||':v_PNeed_US:'||v_PNeed_US||':v_actual_qty:'||v_actual_qty);
				v_US_Parent_need := GREATEST(0, (v_lead_time_US_Adj_qty+
											v_forecast_us+
											v_tpr_us +
											v_pp_pneed_us+
											v_set_par +
											v_us_otn
											)
											-
											v_tot_inv
										);
			--	 dbms_output.put_line(':v_US_Parent_need:'||v_US_Parent_need);
				v_calculated_US_PNeed :=  (v_US_Parent_need+v_open_po)*v_actual_qty + v_PNeed_US;
			--	 dbms_output.put_line(':v_calculated_US_PNeed:'||v_calculated_US_PNeed);

					gm_sav_temp_value(v_family_part||v_sub_part,'PNEED_US',GREATEST(NVL(v_calculated_US_PNeed,0),0));

					SELECT NVL(get_temp_value(v_family_part||v_sub_part, 'PNEED_US'),0)
					INTO v_PNeed_US
					FROM DUAL;
			--		 dbms_output.put_line(': 1 v_PNeed_US:'||v_PNeed_US ||':v_calculated_US_PNeed:'||v_calculated_US_PNeed);

				v_calculated_US_LTA := (v_US_Parent_need*v_actual_qty)+v_LTA_US;

				gm_sav_temp_value(v_family_part||v_sub_part,'LTA_US',GREATEST(NVL(v_calculated_US_LTA,0),0));

				SELECT get_temp_value(v_family_part||v_sub_part, 'LTA_US')
				INTO v_LTA_US
				FROM DUAL;

				/*dbms_output.put_line(v_family_part||': 25 v_LTA_US:'||v_LTA_US);
				 dbms_output.put_line(v_family_part||': 26 v_tot_inv:'||v_tot_inv||':'||v_lead_time_US_Adj_qty||':'||v_forecast_us||':'||v_tpr_us||':'||v_us_otn||':'||v_set_par||':'||v_pp_pneed_us
				 ||':v_LTA_US:'||v_LTA_US);
				 */
				v_rem_inv := GREATEST(0,
								v_tot_inv -
									(v_lead_time_US_Adj_qty+
									  v_forecast_us+
									  v_tpr_us+
									  v_us_otn+
									  v_set_par+
									  v_pp_pneed_us
									)
								);

			--	 dbms_output.put_line(':v_rem_inv'||v_rem_inv);
			--	 dbms_output.put_line(v_family_part||': 27 :'||v_lead_time_OUS_Adj_qty||':'||v_forecast_ous||':'||v_tpr_ous||':'||v_PNeed_OUS||':'||v_ous_otn||':'||v_rw_inv||':'||v_rem_inv);
				v_OUS_Parent_need :=  GREATEST (0,
												(v_lead_time_OUS_Adj_qty +
												v_forecast_ous+
												v_tpr_ous+
												v_pp_pneed_ous+
												v_ous_otn
												) -
												 (v_rw_inv+v_rem_inv)
										   );

			--	 dbms_output.put_line(':v_OUS_Parent_need'||v_OUS_Parent_need);

				v_calculated_OUS_PNeed := (v_OUS_Parent_need*v_actual_qty)+v_PNeed_OUS;
			--	 dbms_output.put_line(':v_calculated_OUS_PNeed'||v_calculated_OUS_PNeed);

					gm_sav_temp_value(v_family_part||v_sub_part,'PNEED_OUS',GREATEST(NVL(v_calculated_OUS_PNeed,0),0));
					gm_sav_temp_value(v_family_part||v_sub_part,'LTA_OUS',GREATEST(NVL(v_calculated_OUS_PNeed,0),0));

					SELECT get_temp_value(v_family_part||v_sub_part, 'PNEED_OUS')
					INTO v_PNeed_OUS
					FROM DUAL;

					SELECT get_temp_value(v_family_part||v_sub_part, 'LTA_OUS')
					INTO v_LTA_OUS
					FROM DUAL;


					v_parent_need := v_OUS_Parent_need + v_US_Parent_need;
					v_calc_pneed := v_calculated_OUS_PNeed + v_calculated_US_PNeed;
					v_calc_LTA := v_calculated_US_LTA + v_calculated_OUS_PNeed;

					gm_sav_temp_value(v_family_part||v_sub_part,'PNEED',GREATEST(NVL(v_calc_pneed,0),0));
					gm_sav_temp_value(v_family_part||v_sub_part,'LTA',GREATEST(NVL(v_calc_LTA,0),0));

					SELECT get_temp_value(v_family_part||v_sub_part, 'PNEED')
					INTO v_PNeed_ww
					FROM DUAL;

					SELECT get_temp_value(v_family_part||v_sub_part, 'LTA')
					INTO v_LTA_WW
					FROM DUAL;

				--	 dbms_output.put_line(v_sub_part||'28:v_PNeed_ww'||v_PNeed_ww||':v_LTA_WW:'||v_LTA_WW);

				IF (v_family_part = v_sub_part)
				THEN
					SELECT NVL(get_temp_value(v_family_part||v_sub_part, 'PNEED'),0) , 
						  NVL(get_temp_value(v_family_part||v_sub_part, 'PNEED_US'),0) , 
						  NVL(get_temp_value(v_family_part||v_sub_part, 'PNEED_OUS'),0)
					INTO v_PNeed_ww, v_PNeed_US, v_PNeed_OUS
					FROM DUAL;

					SELECT 	NVL(get_temp_value(v_family_part||v_sub_part, 'LTA'),0) , 
							NVL(get_temp_value(v_family_part||v_sub_part, 'LTA_US'),0) , 
							NVL(get_temp_value(v_family_part||v_sub_part, 'LTA_OUS'),0)
					INTO v_LTA_WW, v_LTA_US, v_LTA_OUS
					FROM DUAL;

					v_PNeed_ww := v_PNeed_ww * v_add_rem_month ;
					v_pneed_us := v_PNeed_US* v_add_rem_month;
					v_pneed_ous := v_PNeed_OUS * v_add_rem_month;

					v_LTA_WW := v_LTA_WW* v_add_rem_month;
					v_LTA_US := v_LTA_US* v_add_rem_month;
					v_LTA_OUS := v_LTA_OUS* v_add_rem_month;

			--		dbms_output.put_line(v_sub_part||'>>>## 29:v_PNeed_ww'||v_PNeed_ww||':v_PNeed_OUS:'||v_PNeed_OUS||':v_pneed_us:'||v_pneed_us||':v_LTA_WW'
			--									   ||v_LTA_WW||':v_LTA_US'||v_LTA_US||':v_LTA_OUS'||v_LTA_OUS);

					UPDATE T4055_TTP_SUMMARY SET 	C255_PARENT_NEED = ROUND(v_PNeed_ww,0) ,
												 	C255_PARENT_NEED_US	= ROUND(v_pneed_us,0),
												 	C255_PARENT_NEED_OUS	= ROUND(v_pneed_OUS,0),
													
												 	C255_PARENT_OUS_LTA = ROUND(v_LTA_OUS,0),
												 	C255_PARENT_US_LTA	= ROUND(v_LTA_US,0),
												 	C255_PARENT_LTA	= ROUND(v_LTA_WW,0),
												 	
												 --C255_PARENT_US_LTA	= ROUND(v_LTA_US,0),
												  	C255_LTA	= ROUND(v_LTA_WW,0),
												  	C255_US_LTA	= ROUND(v_LTA_US,0),
												  	C255_OUS_LTA	= ROUND(v_LTA_OUS,0)
					WHERE C4052_TTP_DETAIL_ID = p_ttp_detail_id
						  AND C901_LEVEL_ID = p_lvl_id
						  AND C4055_VOID_FL IS NULL
						  AND C205_PART_NUMBER_ID = v_sub_part;
						  
         select C255_PARENT_NEED
						  INTO v_tmp
						  FROM T4055_TTP_SUMMARY
					  	  WHERE C4052_TTP_DETAIL_ID = p_ttp_detail_id
						  AND C901_LEVEL_ID = p_lvl_id
						  AND C4055_VOID_FL IS NULL
						  AND C205_PART_NUMBER_ID = v_sub_part;
						  
				--		  dbms_output.put_line(v_sub_part||'>>>## 30: PNeed :'||v_tmp);
              
              select C255_LTA
						  INTO v_tmp
						  FROM T4055_TTP_SUMMARY
					  	  WHERE C4052_TTP_DETAIL_ID = p_ttp_detail_id
						  AND C901_LEVEL_ID = p_lvl_id
						  AND C4055_VOID_FL IS NULL
						  AND C205_PART_NUMBER_ID = v_sub_part;
						  
				--		  dbms_output.put_line(v_sub_part||' : End :>>>## 31: LTA :'||v_tmp);
				
				END IF;
			END IF;

	--	EXIT WHEN cur_parent_sub_part%NOTFOUND;
		END LOOP;


		gm_ld_lead_timeAdj(p_ttp_detail_id,p_lvl_id);

	 END gm_ttp_summary_calc_main	;


Function get_temp_value(pnum  MY_TEMP_KEY_VALUE.MY_TEMP_TXN_ID%TYPE,
						Key  MY_TEMP_KEY_VALUE.MY_TEMP_TXN_KEY%TYPE )
						RETURN VARCHAR2
IS
	v_cnt NUMBER;
	v_value MY_TEMP_KEY_VALUE.MY_TEMP_TXN_VALUE%TYPE;
BEGIN

	SELECT COUNT(1)
		INTO	v_cnt
		FROM MY_TEMP_KEY_VALUE
		WHERE MY_TEMP_TXN_ID = pnum
		AND MY_TEMP_TXN_KEY = Key;

		IF v_cnt =1
		THEN
			SELECT MY_TEMP_TXN_VALUE
				INTO v_value
				FROM MY_TEMP_KEY_VALUE
				WHERE
				MY_TEMP_TXN_ID=pnum
				AND MY_TEMP_TXN_KEY = Key;
		END IF;

		RETURN v_value;
END get_temp_value;


procedure gm_sav_temp_value(
		pnum  IN MY_TEMP_KEY_VALUE.MY_TEMP_TXN_ID%TYPE,
		Key  IN  MY_TEMP_KEY_VALUE.MY_TEMP_TXN_KEY%TYPE ,
		value  IN  MY_TEMP_KEY_VALUE.MY_TEMP_TXN_VALUE%TYPE
)
AS
BEGIN
  DELETE FROM MY_TEMP_KEY_VALUE WHERE MY_TEMP_TXN_ID=pnum AND MY_TEMP_TXN_KEY=Key;
	INSERT INTO MY_TEMP_KEY_VALUE VALUES(pnum,Key, value);
	
END gm_sav_temp_value;

FUNCTION  addMoreForecastMonths (
p_forecast_mon		NUMBER,
p_monthYear			DATE ,
p_ttp_detail_id 	T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE ,
p_lvl_id	        T4040_DEMAND_SHEET.c901_level_id%TYPE ,
p_pnum				T205_PART_NUMBER.c205_part_number_id%TYPE,
p_type				VARCHAR2,
p_months_to_add		NUMBER
)
RETURN NUMBER
IS

v_more_forecast NUMBER :=0;
v_forecast_mon NUMBER;
BEGIN

	v_forecast_mon := p_forecast_mon;
	--dbms_output.put_line(':p_pnum:'||p_pnum||':v_forecast_mon:'||v_forecast_mon||':p_type:'||p_type||':'||p_ttp_detail_id||':'||p_lvl_id||':'||p_pnum||':'||p_months_to_add);
	FOR i in 1 .. p_months_to_add
	LOOP

		v_forecast_mon :=v_forecast_mon+i;

		v_more_forecast :=v_more_forecast+
						  gm_pkg_oppr_ld_summary.get_oppr_mon_fcst_qty(p_ttp_detail_id,p_lvl_id,p_pnum,p_type||v_forecast_mon);
		--dbms_output.put_line(p_type||':v_more_forecast:'||v_more_forecast||':v_forecast_mon:'||v_forecast_mon||':i:'||i);
	END LOOP;



	RETURN v_more_forecast;
END addMoreForecastMonths;

PROCEDURE gm_ld_lead_timeAdj(
	p_ttp_detail_id 	 IN T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE ,
	p_lvl_id	         IN T4040_DEMAND_SHEET.c901_level_id%TYPE -- 102580 , WW

)
AS

v_pnum	T4055_TTP_SUMMARY.C205_PART_NUMBER_ID%TYPE;
v_lead_time_pointer		NUMBER :=4;-- Lead time calculation to start checking from the 5th month forecast.
v_dflt_lead_time			NUMBER :=7; -- To take from Rules.
v_lead_time_wks			NUMBER;

v_lead_time_US_Adj_qty NUMBER :=0;
v_lead_time_OUS_Adj_qty NUMBER :=0;
v_lead_time_WW_Adj_qty NUMBER :=0;
v_excs_wks			NUMBER;
v_add_rem_month			NUMBER :=1 ; -- Default Should be 1.
v_forecast_month		NUMBER;
v_prev_month				DATE;
v_current_period			DATE;
v_months_to_add			NUMBER(10,2);

	CURSOR cur_ttp_summary
	IS
		SELECT C255_LTA LTA,C255_LEAD_TIME LEAD_TIME,C205_PART_NUMBER_ID PNUM
		FROM T4055_TTP_SUMMARY
		WHERE C4052_TTP_DETAIL_ID =p_ttp_detail_id AND C4055_VOID_FL IS NULL
		AND C901_LEVEL_ID = p_lvl_id;
BEGIN

	SELECT get_rule_value('4000101','LEADTIME')
		INTO 	v_dflt_lead_time
		FROM 	DUAL;

	SELECT get_rule_value('FORECASTMONTHS','ORDERPLANNING')
		INTO 	v_forecast_month
		FROM 	DUAL;

	SELECT to_number(get_rule_value('MONTHS_TO_ADD','GOP-NON-STERILE-PNED'))
		INTO 	v_months_to_add
		FROM 	DUAL;

	SELECT DISTINCT C4040_DEMAND_PERIOD_DT
		INTO v_current_period
	FROM T4040_DEMAND_SHEET
	WHERE
	C4052_TTP_DETAIL_ID = p_ttp_detail_id
--	AND c901_level_id=p_lvl_id
	GROUP BY C4040_DEMAND_PERIOD_DT
	;

	FOR cur_ttp in cur_ttp_summary
	LOOP
		v_excs_wks :=0;
		v_lead_time_wks :=0;
		v_lead_time_US_Adj_qty :=0;
		v_lead_time_OUS_Adj_qty :=0;
		v_lead_time_WW_Adj_qty := 0;
		v_lead_time_pointer :=4;
		v_months_to_add :=0;

		v_pnum := cur_ttp.pnum;
		v_lead_time_wks := cur_ttp.LEAD_TIME;

		v_excs_wks := v_lead_time_wks - v_dflt_lead_time;

		IF (v_excs_wks <0)
		THEN

			v_excs_wks := v_excs_wks *-1;
			v_add_rem_month := -1;
			v_forecast_month := v_forecast_month -1;

		END IF;

		SELECT ADD_MONTHS(v_current_period ,v_forecast_month)
		INTO v_prev_month
		FROM DUAL;

		/* If the v_lead_time_wks = 12, the excess week is 5 (12-7).
		* So we need to take 5th month forecast and 0.25* of 6th month Forecast'
		* If the v_lead_time_wks = 10, the excess week is 3, so we will take 0.75*5th month forecast.
		*/
		FOR i in 1..v_excs_wks
		LOOP
			v_months_to_add := v_months_to_add + 0.25;

			IF (MOD(i,4)=0)
			THEN
			--DBMS_OUTPUT.PUT_LINE(':>>>:'||v_lead_time_pointer);
				v_lead_time_pointer := v_lead_time_pointer+1;

				v_lead_time_WW_Adj_qty := v_lead_time_WW_Adj_qty +
										gm_pkg_oppr_ld_summary.get_oppr_mon_fcst_qty(p_ttp_detail_id,p_lvl_id,v_pnum,'C4055_MONTH_'|| (lpad(v_lead_time_pointer,2,0))
										,'WW_FCST');

				v_lead_time_US_Adj_qty := v_lead_time_US_Adj_qty +
										   gm_pkg_oppr_ld_summary.get_oppr_mon_fcst_qty(p_ttp_detail_id,p_lvl_id,v_pnum,'US_FCST_'|| (v_lead_time_pointer));

				v_lead_time_OUS_Adj_qty := v_lead_time_OUS_Adj_qty +
										   gm_pkg_oppr_ld_summary.get_oppr_mon_fcst_qty(p_ttp_detail_id,p_lvl_id,v_pnum,'OUS_FCST_'|| (v_lead_time_pointer));
				--Take the forecast for the 5th month and move forward based on the Lead Time.
				v_months_to_add :=0;

			END IF;

		END LOOP;



		IF v_months_to_add >0
		THEN
			v_lead_time_pointer :=v_lead_time_pointer+1;

		--	DBMS_OUTPUT.PUT_LINE(':v_lead_time_pointer:'||v_lead_time_pointer||':v_parentpart:'||v_pnum||':func:'||
		--	get_oppr_mon_fcst_qty(p_ttp_detail_id,p_lvl_id,v_pnum,'US_FCST_'|| (v_lead_time_pointer))
		--	||':v_months_to_add:'||v_months_to_add);
			--Move the Month to one more month so the right forecast will be taken.

			v_lead_time_WW_Adj_qty := v_lead_time_WW_Adj_qty +
									gm_pkg_oppr_ld_summary.get_oppr_mon_fcst_qty(p_ttp_detail_id,p_lvl_id,v_pnum,'C4055_MONTH_'|| (lpad(v_lead_time_pointer,2,0)),'WW_FCST')
									*v_months_to_add;

			v_lead_time_US_Adj_qty := v_lead_time_US_Adj_qty +
										gm_pkg_oppr_ld_summary.get_oppr_mon_fcst_qty(p_ttp_detail_id,p_lvl_id,v_pnum,'US_FCST_'|| (v_lead_time_pointer))
										*v_months_to_add;

			v_lead_time_OUS_Adj_qty := v_lead_time_OUS_Adj_qty +
										gm_pkg_oppr_ld_summary.get_oppr_mon_fcst_qty(p_ttp_detail_id,p_lvl_id,v_pnum,'OUS_FCST_'|| (v_lead_time_pointer))
										*v_months_to_add;

		--	DBMS_OUTPUT.PUT_LINE('gm_ld_lead_timeAdj'||p_ttp_detail_id||':'||v_pnum||':'||v_lead_time_US_Adj_qty||':'||v_lead_time_OUS_Adj_qty||':'||v_lead_time_WW_Adj_qty);
		END IF;

		/*
		UPDATE T4055_TTP_SUMMARY SET C255_US_LTA = NVL(C255_US_LTA,0)+ROUND(v_lead_time_US_Adj_qty,0), 
		C255_OUS_LTA = NVL(C255_OUS_LTA,0)+ROUND(v_lead_time_OUS_Adj_qty ,0),
		C255_LTA = NVL(C255_LTA,0)+ROUND(v_lead_time_WW_Adj_qty,0)
		WHERE C4052_TTP_DETAIL_ID =p_ttp_detail_id AND C4055_VOID_FL IS NULL
		AND C901_LEVEL_ID = p_lvl_id 
		AND C205_PART_NUMBER_ID = v_pnum;
		*/
		UPDATE T4055_TTP_SUMMARY SET C255_US_LTA = ROUND(v_lead_time_US_Adj_qty,0), 
		C255_OUS_LTA = ROUND(v_lead_time_OUS_Adj_qty ,0),
		C255_LTA = ROUND(v_lead_time_WW_Adj_qty,0)
		WHERE C4052_TTP_DETAIL_ID =p_ttp_detail_id AND C4055_VOID_FL IS NULL
		AND C901_LEVEL_ID = p_lvl_id 
		AND C205_PART_NUMBER_ID = v_pnum;

	END LOOP;


	END gm_ld_lead_timeAdj;

END gm_pkg_oppr_ttp_summary_calc;
/