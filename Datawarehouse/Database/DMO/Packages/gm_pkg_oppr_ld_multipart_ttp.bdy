-- @"c:\database\packages\operations\purchasing\gm_pkg_oppr_ld_multipart_ttp.bdy";

create or replace PACKAGE BODY gm_pkg_oppr_ld_multipart_ttp
IS
    /*******************************************************
   * Description : This procedure will be used to load all the 
   * parts that were excluded from Lock & generate of Individual TTP and
   * bringing the missing parts back to the Multi-Part TTP.
   * This procedure will check if the missing parts have any inventory or Set PAR or OTN
   * If the part has any quantity in any of the above mentioned bucket, we will load that
   * part in the Multi-Part TTP Sheet.
   *******************************************************/
PROCEDURE gm_op_ld_missing_part(
		p_divison_id		IN		t1910_division.c1910_division_id%TYPE
	  , p_lvl_value         IN 		t4040_demand_sheet.c901_level_value%TYPE
	)
AS
--
  	CURSOR missingpart_cur(v_inventory_lock_id t250_inventory_lock.c250_inventory_lock_id%TYPE
  						 ,v_demand_sheet_id t4042_demand_sheet_detail.c4040_demand_sheet_id%TYPE
  						  )
	IS
	
    --PMT-30031 GOP_To show Inventory details in TTP summary
	-- Get the Non Trauma Multi-Part Part that is not part of the Multi Part TTP but having Qty in Stock/TPR/DHR etc.
	select c205_part_number_id, SUM(NVL(C251_OPEN_PO,0) + NVL(C251_OPEN_DHR,0) + NVL(C251_BUILD_SET,0) + NVL(C251_IN_STOCK,0) + NVL(C251_TPR,0)) Inv_Qty
	from t251_inventory_lock_details 
	where c250_inventory_lock_id=v_inventory_lock_id 
	and  c205_part_number_id in (
		select t205.c205_part_number_id 
		from t205_part_number t205
		, t202_project t202
		where t205.c202_project_id = t202.c202_project_id
		and t205.c205_crossover_fl='Y' 
		and t205.c901_status_id=20367
		and t202.c1910_division_id IN (select MY_TEMP_TXN_ID from MY_TEMP_LIST)
		minus 
		select distinct c205_part_number_id 
		from t4042_mp_demand_sheet_detail   
		/*  New Table Map in 2nd PC */
		where c4040_demand_sheet_id=v_demand_sheet_id 
		and c901_type=50563
		and c205_part_number_id is not null
	) group by c205_part_number_id;



	--
	v_pnum 		t251_inventory_lock_details.c205_part_number_id%TYPE;
	v_set_par  	NUMBER;
	v_otn_qty	NUMBER;
	v_qty 		NUMBER := 0;
	v_inventory_lock_id t250_inventory_lock.c250_inventory_lock_id%TYPE;
	v_demand_sheet_id t4042_demand_sheet_detail.c4040_demand_sheet_id%TYPE;
	v_inv_qty	NUMBER;
	
--
BEGIN
--
	--Get the Current Inventory Lock ID for US.
	SELECT 	max(c250_inventory_lock_id) 
	INTO 	v_inventory_lock_id
	FROM 	t250_inventory_lock
	WHERE 	c4016_global_inventory_map_id='1'  
	AND		c250_void_fl is null  
	AND		c901_lock_type='20430';
	
	--Get the CUrrent Multi-Part TTP ID.
	SELECT 	max(c4040_demand_sheet_id) 
	INTO 	v_demand_sheet_id
	FROM 	T4040_demand_sheet 
	WHERE 	c4040_void_fl is null  
	AND		c901_demand_type='40023'  -- Multi Part TTP
	AND		c901_status='50550'  	-- 50550 Open, 50551 Approved
	AND		c250_inventory_lock_id=v_inventory_lock_id
	AND     nvl(c901_level_value,'1')= nvl(p_lvl_value,'1');
 	
	--DBMS_OUTPUT.PUT_LINE('v_inventory_lock_id'||v_inventory_lock_id||'  v_demand_sheet_id  '||v_demand_sheet_id);
	
	DELETE FROM MY_TEMP_LIST;
		    	
					-- PMT-57165 Added division mapping in rule for to group division in one MP sheet
					INSERT INTO my_temp_list
				    	 SELECT to_number(c906_rule_id) c1910_division_id
						FROM t1910_division t1910, t906_rules t906
						WHERE to_char(t1910.c1910_division_id)=t906.c906_rule_value
	                    AND c1910_division_id =p_divison_id
						AND c1910_void_fl       IS NULL
	                    AND t906.c906_rule_grp_id ='GOP_MP_DIV_MAP'
	                    AND t906.c906_void_fl IS NULL
                    UNION  
	                    SELECT c1910_division_id 
	                    FROM t1910_division
	                    WHERE c1910_division_id = p_divison_id
	                    AND c1910_void_fl       IS NULL;
				    

	FOR missingpart_val IN missingpart_cur(v_inventory_lock_id,v_demand_sheet_id)
	LOOP    	
		v_pnum := missingpart_val.c205_part_number_id;
		v_inv_qty	:= missingpart_val.inv_qty;
    	
			
		-- Get the Set PAR value for the Part.
		BEGIN
			SELECT 	SUM(NVL(t208.c208_set_qty*t4022.c4022_par_value,0)) 
			INTO 	v_set_par
			FROM 	t4022_demand_par_detail t4022,t208_set_details t208,t207_set_master t207
			WHERE 	t4022.c207_set_id = t208.c207_set_id
			AND 	t207.c207_set_id = t208.c207_set_id
			AND 	t207.c207_void_fl is null
			AND 	t207.c901_set_grp_type = 1601
			AND 	t208.c205_part_number_id=v_pnum and t208.c208_void_fl is null;
        EXCEPTION WHEN NO_DATA_FOUND
        THEN
        	v_set_par :=0;
        END;
		
        --Get the OTN FOr the Part.
		BEGIN	
			SELECT 	NVL(SUM(c4023_qty),0)
			INTO	v_otn_qty
			FROM 	t4023_demand_otn_detail 
			WHERE 	c4023_void_fl is null 
			AND 	c205_part_number_id=v_pnum;
		EXCEPTION WHEN NO_DATA_FOUND
		THEN
			v_otn_qty :=0;
		END ;
		
		
		v_qty := NVL(v_set_par,0) + NVL(v_otn_qty,0)+ NVL(v_inv_qty,0) ;
		--DBMS_OUTPUT.PUT_LINE('v_qty'||v_qty);
		
		--If the Part has a Positive Quantity then it has to be added to the Multi Part TTP.
		IF v_qty >0
		THEN
		/*	INSERT INTO t4042_demand_sheet_detail(c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id, c901_type, c4042_period, 
			c4042_qty, c4042_updated_by, c4042_updated_date, c205_parent_part_num_id, c4042_parent_ref_id, c4042_parent_qty_needed
			, c4042_history_fl, c4042_comments_fl, c4042_weighted_avg_diff_qty, c901_ref_type,c205_part_number_id)
			VALUES (s4042_demand_sheet_detail.NEXTVAL, v_demand_sheet_id, '-9999', 50563,  
			to_date('01/'||to_char(sysdate,'MM/YYYY'),'DD/MM/YYYY'), ROUND(0,0), 
			30301, SYSDATE, '', '', ROUND(0,0), '', '', '', '',
			v_pnum); */
			
			/*  New Table Map */
			INSERT INTO t4042_mp_demand_sheet_detail(c4040_demand_sheet_id, c4042_ref_id, c901_type, c4042_period, 
			c4042_qty, c4042_updated_by, c4042_updated_date, c205_parent_part_num_id, c4042_parent_ref_id, c4042_parent_qty_needed
			, c4042_history_fl, c4042_comments_fl, c4042_weighted_avg_diff_qty, c901_ref_type,c205_part_number_id)
			VALUES (v_demand_sheet_id, '-9999', 50563,  
			to_date('01/'||to_char(sysdate,'MM/YYYY'),'DD/MM/YYYY'), ROUND(0,0), 
			30301, SYSDATE, '', '', ROUND(0,0), '', '', '', '',
			v_pnum);
			
			
			--DBMS_OUTPUT.PUT_LINE('v_qty '||v_qty||' v_set_par'||v_set_par||'  v_otn_qty  '||v_otn_qty||' v_inv_qty '||v_inv_qty);
			v_qty := 0;
			
		END IF;
		
    END LOOP;
		DBMS_OUTPUT.PUT_LINE('OUT 1');
	--
--	COMMIT;
	EXCEPTION	WHEN OTHERS
	THEN
		DBMS_OUTPUT.PUT_LINE(SQLERRM);
		ROLLBACK;
		
END gm_op_ld_missing_part;

/***************************************************************************************************
    * Description : Procedure to used to sync the multipart from part growth to 
    * ttp by vendor summary screen.
    * Author   : Agilan Singarvel
    ************************************************************************************************/

PROCEDURE gm_sync_multipart_dtls(
		p_ttp_dtl_id		IN	t4052_ttp_detail.c4052_ttp_detail_id%TYPE,
        p_user_id	   		IN	t4020_demand_master.c4020_created_by%TYPE
)
AS
	v_demandsheet_ids 	VARCHAR2 (4000);
	v_inventory_id  	t250_inventory_lock.c250_inventory_lock_id%TYPE;
BEGIN
	
	v_demandsheet_ids := gm_pkg_oppr_sheet.get_demand_sheet_ids(p_ttp_dtl_id);
	
	SELECT MAX (t250.c250_inventory_lock_id)
	  INTO v_inventory_id
	  FROM t250_inventory_lock t250
	 WHERE t250.c901_lock_type = 20430 --Main Inventory Lock
	   AND t250.c4016_global_inventory_map_id  = 1  
	   AND t250.c250_void_fl IS NULL;
	
	gm_pkg_oppr_sheet.gm_sync_mutipart_dtls_by_demand_sheet_ids(v_demandsheet_ids,p_user_id,'N',TRUNC (SYSDATE, 'MM'),v_inventory_id);
	
END gm_sync_multipart_dtls;
/***************************************************************************************************
    * Description : Procedure to used to load forecast data for Multi Part
    * Created this procedure from gm_pkg_oppr_ld_summary.gm_ld_forecast to handle only Multi Part with New Table Map 
    * Author   : Paddy Srinivasan
    ************************************************************************************************/
	PROCEDURE gm_ld_mp_forecast (
		p_ttp_detail_id 	IN T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE ,
		p_lvl_value         IN T4040_DEMAND_SHEET.c901_level_value%TYPE ,
		p_inv_lck_id        IN T4040_DEMAND_SHEET.C250_INVENTORY_LOCK_ID%TYPE ,
		p_load_dt	        IN T4055_TTP_SUMMARY.C4055_LOAD_DATE%TYPE ,
		p_request_by   		IN	t4020_demand_master.c4020_created_by%TYPE
	)
	AS
	v_forecast_month NUMBER;

	type pnum_array IS     TABLE OF  T205_PART_NUMBER.c205_part_number_id%TYPE;
	type month_array IS    TABLE OF  T4055_TTP_SUMMARY.C4055_MONTH_01%TYPE ;

	arr_pnum pnum_array;
	arr_m1 month_array;
	arr_m2 month_array;
	arr_m3 month_array;
	arr_m4 month_array;
	arr_m5 month_array;
	arr_m6 month_array;
	arr_m7 month_array;
	arr_m8 month_array;
	arr_m9 month_array;
	arr_m10 month_array;
	arr_m11 month_array;
	arr_m12 month_array;
	arr_par month_array;

	Cursor cur_forecast_by_month
	IS
	--12 Months Forecast
--DEV Independence MIS Apr'2019

		select  pnum
		      , MAX(Month_1) Month_1
		      , MAX(Month_2) Month_2
		      , MAX(Month_3) Month_3
		      , MAX(Month_4) Month_4
		      , MAX(Month_5) Month_5
		      , MAX(Month_6) Month_6
		      , MAX(Month_7) Month_7
		      , MAX(Month_8) Month_8
		      , MAX(Month_9) Month_9
		      , MAX(Month_10) Month_10
		      , MAX(Month_11) Month_11
		      , MAX(Month_12) Month_12
		FROM (
		select   period,pnum
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(p_load_dt , 'month'),0) THEN qty ELSE 0 END) Month_1
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(p_load_dt , 'month'),1)  THEN qty ELSE 0 END) Month_2
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(p_load_dt , 'month'),2)  THEN qty ELSE 0 END) Month_3
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(p_load_dt , 'month'),3)  THEN qty ELSE 0 END) Month_4
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(p_load_dt , 'month'),4)  THEN qty ELSE 0 END) Month_5
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(p_load_dt , 'month'),5)  THEN qty ELSE 0 END) Month_6
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(p_load_dt , 'month'),6)  THEN qty ELSE 0 END) Month_7
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(p_load_dt , 'month'),7)  THEN qty ELSE 0 END) Month_8
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(p_load_dt , 'month'),8)  THEN qty ELSE 0 END) Month_9
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(p_load_dt , 'month'),9)  THEN qty ELSE 0 END) Month_10
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(p_load_dt , 'month'),10)  THEN qty ELSE 0 END) Month_11
		       , (CASE WHEN period = ADD_MONTHS(TRUNC(p_load_dt , 'month'),11) THEN qty ELSE 0 END) Month_12
		from
		(SELECT t4042.c205_part_number_id pnum,
		c4042_period period,
		--SUM (DECODE (refid, NULL, DECODE (t4042.c901_type, 50563, c4042_qty, 0), 0))  qty
		SUM((CASE WHEN refid IS NULL THEN DECODE (t4042.c901_type, 50563, c4042_qty, 0) ELSE 0 END)) qty
		 FROM
		    -- Below code to fetch demand sheet based on par value
		    (
		         SELECT t4042.c4040_demand_sheet_id, t4042.c205_part_number_id, t4042.c4042_period
		          ,T4042.C901_TYPE, NVL (DECODE (t4042.c901_type, 50563, DECODE (
		            parvalue.c205_part_number_id, NULL, t4042.c4042_qty, DECODE (parvalue.parperiod, t4042.c4042_period,
		            parvalue.parvalue, 0)), 0), 0) C4042_QTY,t4042.c4042_parent_ref_id refid
		           FROM t4042_mp_demand_sheet_detail t4042, my_temp_list my_temp_dsid, (
		                 SELECT c4040_demand_sheet_id, c205_part_number_id, parperiod
		                  , demandvalue, usdemand, ousdemand
		                  , parvalue, (
		                    CASE
		                        WHEN (parvalue - ousdemand) > 0
		                        THEN (parvalue - ousdemand)
		                        ELSE 0
		                    END) usparvalue, ousdemand ousparvalue
		                   FROM
		                    (
		                         SELECT t4042.c4040_demand_sheet_id, t4042.c205_part_number_id, MIN (t4042.c4042_period)
		                            parperiod, SUM (DECODE (t4042.c901_type, 50563, t4042.c4042_qty, 0)) demandvalue, SUM (
		                            DECODE (t4042.c901_type, 4000110, t4042.c4042_qty, 0)) usdemand, SUM (DECODE (
		                            t4042.c901_type, 4000111, t4042.c4042_qty, 0)) ousdemand, SUM (DECODE (t4042.c901_type,
		                            50566, t4042.c4042_qty, 0)) parvalue
		                           FROM t4042_mp_demand_sheet_detail T4042 , my_temp_list my_temp_dsid
		                          WHERE t4042.c4040_demand_sheet_id  = my_temp_dsid.MY_TEMP_TXN_ID
		                            AND t4042.c4042_period BETWEEN p_load_dt AND ADD_MONTHS (p_load_dt, (v_forecast_month- 1)) -- INPUT current month
		                            AND t4042.c901_type IN (50563, 50566, 4000110, 4000111)
		                       GROUP BY t4042.c4040_demand_sheet_id, t4042.c205_part_number_id
		                    )
		                  WHERE parvalue > demandvalue
		            )
		            parvalue -- Above to fetch part which has value more than par
		          WHERE T4042.C4040_DEMAND_SHEET_ID  = my_temp_dsid.MY_TEMP_TXN_ID
		            AND t4042.c901_type              = 50563--, 4000110, 4000111)
		            AND t4042.c4042_period         <= ADD_MONTHS (p_load_dt, (12 - 1))   -- INPUT current month
		            AND t4042.c4040_demand_sheet_id = parvalue.c4040_demand_sheet_id(+)
		            AND t4042.c205_part_number_id   = parvalue.c205_part_number_id(+)
		            )
		    t4042,  T205_PART_NUMBER T205
		  WHERE t4042.c205_part_number_id  = t205.c205_part_number_id
		    AND t4042.c205_part_number_id IS NOT NULL
		 --and t4042.c205_part_number_id='3135.0313'
		  GROUP BY c4042_period ,T4042.C205_PART_NUMBER_ID
		)
		)
		group by PNUM;

	BEGIN
		SELECT get_rule_value('FORECASTMONTHS','ORDERPLANNING')
		INTO 	v_forecast_month
		FROM 	DUAL;


		OPEN cur_forecast_by_month;
		LOOP
		FETCH cur_forecast_by_month BULK COLLECT INTO arr_pnum,arr_m1,arr_m2,arr_m3,arr_m4,arr_m5,arr_m6,arr_m7,arr_m8,arr_m9,arr_m10,arr_m11,arr_m12  LIMIT 500;

		FORALL i in 1.. arr_pnum.COUNT
			UPDATE T4055_TTP_SUMMARY SET
			C4055_MONTH_01 = arr_m1(i),C4055_MONTH_02=arr_m2(i),C4055_MONTH_03=arr_m3(i),C4055_MONTH_04=arr_m4(i),C4055_MONTH_05=arr_m5(i),C4055_MONTH_06=arr_m6(i),
			C4055_MONTH_07=arr_m7(i),C4055_MONTH_08=arr_m8(i),C4055_MONTH_09=arr_m9(i),C4055_MONTH_10=arr_m10(i),C4055_MONTH_11=arr_m11(i),C4055_MONTH_12=arr_m12(i),
			C4055_FORECAST_QTY = (arr_m1(i)+arr_m2(i)+arr_m3(i)+arr_m4(i))
           -- ,			C4055_PAR = arr_par(i)
			WHERE C4052_TTP_DETAIL_ID = p_ttp_detail_id
			AND c205_part_number_id = arr_pnum(i)
			AND nvl(c901_level_value,1) = nvl(p_lvl_value,1)
			AND C4055_VOID_FL IS NULL;
			--AND (nvl(c901_level_id,1) = nvl(p_lvl_id,1) OR c901_level_value = p_lvl_value);
			-- Multi Part Handling, Level id null for Multi part Spine, level_valu has value only for Multi Part Trauma

		EXIT WHEN cur_forecast_by_month%NOTFOUND;
	    END LOOP;
	    CLOSE cur_forecast_by_month;


	END gm_ld_mp_forecast;
/***************************************************************************************************
    * Description : Procedure to used to load forecast data for Multi Part
    * Created this procedure from gm_pkg_oppr_ld_summary.gm_ld_forecast_grouping to handle only Multi Part with New Table Map 
    * Author   : Paddy Srinivasan
    ************************************************************************************************/
	PROCEDURE gm_ld_mp_forecast_grouping (
		p_ttp_detail_id 	IN T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE ,
		p_lvl_value         IN T4040_DEMAND_SHEET.c901_level_value%TYPE ,
		p_inv_lck_id        IN T4040_DEMAND_SHEET.C250_INVENTORY_LOCK_ID%TYPE ,
		p_load_dt	        IN T4055_TTP_SUMMARY.C4055_LOAD_DATE%TYPE ,
		p_request_by   		IN	t4020_demand_master.c4020_created_by%TYPE
	)
	AS
	v_forecast_month NUMBER;
	v_multipart 	NUMBER;

	type pnum_array IS     TABLE OF  T205_PART_NUMBER.c205_part_number_id%TYPE;
	type tpr_fl_array IS     TABLE OF  T4055_TTP_SUMMARY.C4055_TPR_FL%TYPE;
	type usforecast_array IS    TABLE OF  T4055_TTP_SUMMARY.C4055_US_FORECAST_QTY%TYPE ;
	type ousforecast_array IS    TABLE OF  T4055_TTP_SUMMARY.C4055_OUS_FORECAST_QTY%TYPE ;


	arr_pnum pnum_array;
	arr_tpr_fl tpr_fl_array;
	arr_usforecast usforecast_array;
	arr_ousforecast ousforecast_array;
	arr_wwforecast usforecast_array;


	Cursor cur_forecast_grouping
	IS
		 SELECT t205.c205_part_number_id pnum,
		 -- If TPR_FL = 1 then take TPR from t251, else 0
        SUM(CASE WHEN refid IS NULL THEN 1 ELSE 0 END) TPR_FL,
 		SUM(CASE WHEN refid IS NULL THEN c4042_qty ELSE 0 END) WW_Forecast,
 		SUM(CASE WHEN refid IS NULL THEN c4042_US_qty ELSE 0 END) US_Forecast,
 		SUM(CASE WHEN refid IS NULL THEN c4042_OUS_qty ELSE 0 END) OUS_Forecast
   FROM
   -- Below code to fetch demand sheet based on par value
    (
         SELECT t4042.c205_part_number_id, SUM (DECODE (t4042.c901_type, 50563, DECODE (parvalue.c205_part_number_id,
            NULL, t4042.c4042_qty, DECODE (parvalue.parperiod, t4042.c4042_period, parvalue.parvalue, 0)), 0))
            c4042_qty, SUM (DECODE (t4042.c901_type, 4000110, DECODE (parvalue.c205_part_number_id, NULL,
            t4042.c4042_qty, DECODE (parvalue.parperiod, t4042.c4042_period, parvalue.usparvalue, 0)), 0)) c4042_US_qty
            , SUM (DECODE (t4042.c901_type, 4000111, DECODE (parvalue.c205_part_number_id, NULL, t4042.c4042_qty,
            DECODE (parvalue.parperiod, t4042.c4042_period, parvalue.ousparvalue, 0)), 0)) c4042_OUS_qty,
            t4042.c4042_parent_ref_id refid
           FROM t4042_mp_demand_sheet_detail t4042, my_temp_list my_temp_dsid,  (
                 SELECT c4040_demand_sheet_id, c205_part_number_id, parperiod
                  , demandvalue, usdemand, ousdemand
                  , parvalue, (
                    CASE
                        WHEN (parvalue - ousdemand) > 0
                        THEN (parvalue - ousdemand)
                        ELSE 0
                    END) usparvalue, ousdemand ousparvalue
                   FROM
                    (
                         SELECT t4042.c4040_demand_sheet_id, t4042.c205_part_number_id, MIN (t4042.c4042_period)
                            parperiod, SUM (DECODE (t4042.c901_type, 50563, t4042.c4042_qty, 0)) demandvalue, SUM (
                            DECODE (t4042.c901_type, 4000110, t4042.c4042_qty, 0)) usdemand, SUM (DECODE (
                            t4042.c901_type, 4000111, t4042.c4042_qty, 0)) ousdemand, SUM (DECODE (t4042.c901_type,
                            50566, t4042.c4042_qty, 0)) parvalue
                           FROM t4042_mp_demand_sheet_detail t4042, my_temp_list my_temp_dsid
                          WHERE t4042.c4040_demand_sheet_id = my_temp_dsid.MY_TEMP_TXN_ID
                            AND t4042.c4042_period <= ADD_MONTHS (p_load_dt, (v_forecast_month - 1))
                            AND t4042.c901_type IN (50563, 4000110, 4000111, 50566)
                       GROUP BY t4042.c4040_demand_sheet_id, t4042.c205_part_number_id
                    )
                  WHERE parvalue > demandvalue
            )
            parvalue -- Above to fetch part which has value more than par
          WHERE t4042.c4040_demand_sheet_id = my_temp_dsid.MY_TEMP_TXN_ID
            AND t4042.c901_type                                                                             IN (50563, 4000106, 4000110, 4000111)
            AND t4042.c4042_period <= ADD_MONTHS (p_load_dt, (v_forecast_month - 1))
            AND t4042.c4040_demand_sheet_id = parvalue.c4040_demand_sheet_id(+)
            AND t4042.c205_part_number_id   = parvalue.c205_part_number_id(+)
       GROUP BY t4042.c205_part_number_id, t4042.c4042_parent_ref_id
    )
    forecast,t205_part_number t205
  WHERE t205.c205_part_number_id  = forecast.c205_part_number_id
     AND t205.c205_part_number_id IS NOT NULL
    --and t205.c205_part_number_id='693.001'
GROUP BY T205.C205_Part_Number_Id
;

	BEGIN
		SELECT get_rule_value('FORECASTMONTHS','ORDERPLANNING')
		INTO 	v_forecast_month
		FROM 	DUAL;

	 	SELECT count(1)
	 	INTO v_multipart
	    FROM t4040_demand_sheet
	    WHERE c4052_ttp_detail_id=p_ttp_detail_id
	    AND c901_demand_type=40023
	    AND c4040_void_fl IS NULL;
		--40023 multi part

		OPEN cur_forecast_grouping;
		LOOP
		FETCH cur_forecast_grouping BULK COLLECT INTO arr_pnum,arr_tpr_fl,arr_wwforecast,arr_usforecast,arr_ousforecast LIMIT 500;

		FORALL i in 1.. arr_pnum.COUNT

			UPDATE T4055_TTP_SUMMARY SET
					C4055_US_FORECAST_QTY =arr_usforecast(i),
					C4055_OUS_FORECAST_QTY=arr_ousforecast(i),
					C4055_FORECAST_QTY= arr_wwforecast(i),
					C4055_TPR_FL= decode(v_multipart,1,1,arr_tpr_fl(i))
			WHERE C4052_TTP_DETAIL_ID = p_ttp_detail_id
			AND c205_part_number_id = arr_pnum(i)
			AND c901_level_value = p_lvl_value
			AND C4055_VOID_FL IS NULL;

		EXIT WHEN cur_forecast_grouping%NOTFOUND;
	    END LOOP;
	    CLOSE cur_forecast_grouping;
	END gm_ld_mp_forecast_grouping;
        
END gm_pkg_oppr_ld_multipart_ttp;
/