/* Formatted on 2013/07/03  */
--@"c:\database\packages\operations\purchasing\gm_pkg_oppr_dmd_mas_rpt.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_dmd_mas_rpt
IS
--
--
	/**********************************************************
 	* Purpose: This procedure is fetchinng the Template Report
 	* Created By HReddi
 	***********************************************************/
	PROCEDURE gm_fch_template_rpt(
		p_template_id   IN   t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_set_id		IN	 t4021_demand_mapping.c4021_ref_id%TYPE
	  , p_set_grp_nm    IN	 VARCHAR2
	  , p_out_cur	    OUT	 TYPES.cursor_type
	)
	AS
	BEGIN	
		OPEN p_out_cur
		 FOR    SELECT DMD.DEMANDSHEETNAME DEMANDSHEETNAME
                     ,  DMD.DEMANDPERIOD DEMANDPERIOD
                     ,  DMD.FORECASTPERIOD FORECASTPERIOD
                     ,  DMD.ACTIVEFLAG ACTIVEFLAG
                     ,  DMD.refid setid, DMD.set_groupnm SETGRPNM
           		FROM (
   					SELECT t4020.C4020_DEMAND_NM DEMANDSHEETNAME
              			 , t4020.C4020_DEMAND_PERIOD DEMANDPERIOD
              			 , t4020.C4020_FORECAST_PERIOD FORECASTPERIOD
              			 , DECODE (t4020.C4020_INACTIVE_FL, 'Y', 'InActive','Active') ACTIVEFLAG
              			 , DECODE(get_set_name (t4021.c4021_ref_id),' ', '-', t4021.c4021_ref_id) refid
                         , DECODE(get_set_name (t4021.c4021_ref_id),' ', get_group_name (t4021.c4021_ref_id),get_set_name (t4021.c4021_ref_id)) set_groupnm
                      FROM t4020_demand_master t4020, t4021_demand_mapping t4021
                     WHERE t4020.c4020_demand_master_id = t4021.c4020_demand_master_id
                       AND t4020.c4020_void_fl         IS NULL
                       AND t4020.c4020_demand_master_id = DECODE(p_template_id,'0',t4020.c4020_demand_master_id,p_template_id) 
                       AND t4020.c4020_void_fl IS NULL
                       AND t4020.c901_demand_type   = 4000103
                       AND t4021.c901_ref_type IN (40030, 40031, 4000109)
                  ORDER BY DEMANDSHEETNAME
                      ) DMD
          		WHERE UPPER(DMD.set_groupnm) like '%' || NVL(UPPER(p_set_grp_nm),'')  || '%' 
				  AND DMD. Refid like '%' || NVL(p_set_id,'')  || '%'
			ORDER BY DMD.refid,DMD.set_groupnm ;
	END gm_fch_template_rpt;
	
	/*****************************************************************
 	* Purpose: This procedure is fetchinng all the Active Templates
 	* Created By HReddi
 	*****************************************************************/
	PROCEDURE gm_fch_templates(
		p_out_cur	    OUT	 TYPES.cursor_type
	)
	AS
	BEGIN	
		OPEN p_out_cur 
		 FOR	
	   		SELECT c4020_demand_master_id DMID,c4020_demand_nm DMNM 
      		  FROM t4020_demand_master 
     		 WHERE c4020_void_fl IS NULL  
       		   AND C4020_inactive_fl IS NULL 
       		   AND c901_demand_type   = 4000103
       		   ORDER BY UPPER(c4020_demand_nm);
    END gm_fch_templates;
    
	/***********************************************************
 	* Purpose: This procedure will fetch Demand sheet Names
 	* Created By : Jignesh Shah
 	************************************************************/
	PROCEDURE gm_fch_editable_dmd_sheet(
		p_template_id   IN		t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_region_id		IN		t4015_global_sheet_mapping.c901_level_value%TYPE
	  , p_sheet_type	IN		t4020_demand_master.c901_demand_type%TYPE
	  , p_out_cur	    OUT	 	TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_cur 
		FOR	
			SELECT t4020.c4020_demand_master_id DMID, t4020.c4020_demand_nm DMNM
            FROM t4015_global_sheet_mapping t4015, t4020_demand_master t4020
            WHERE t4020.c4015_global_sheet_map_id = t4015.c4015_global_sheet_map_id
            AND t4020.c4020_void_fl         IS NULL
            AND t4020.c4020_inactive_fl     IS NULL
            AND t4015.c4015_void_fl         IS NULL
            AND t4015.c901_access_type		 = 102623       --editable
            AND T4020.C901_Demand_Type		IN (40021,40022)
            AND t4020.c4020_parent_demand_master_id = NVL(p_template_id,t4020.c4020_parent_demand_master_id)
            AND t4015.c901_level_value = NVL(p_region_id,t4015.c901_level_value)
            AND t4020.c901_demand_type = NVL(p_sheet_type,t4020.c901_demand_type)    
            ORDER BY UPPER(t4020.c4020_demand_nm);
    END gm_fch_editable_dmd_sheet;

	/*******************************************************
 	* Purpose: This Procedure is used to fetch editable demand sheet part details
 	* Created By APrasath
 	*******************************************************/
	PROCEDURE gm_fch_part_otn (
		p_demandmasterid   IN		t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_partnumber	   IN		t205_part_number.c205_part_number_id%TYPE
	  , p_detail		   OUT		TYPES.cursor_type
	)
	AS
		v_demandsheettype t4020_demand_master.c901_demand_type%TYPE;
		v_pnum CLOB;
	BEGIN
		v_pnum :=replace(p_partnumber,',','|');
		OPEN p_detail
		FOR
			SELECT DISTINCT t205.c205_part_number_id pnum, t205.c205_part_num_desc pdesc
					,  otn.otnvalue  otnvalue, NVL (otn.c4023_history_fl, 'N') history_fl
					, '' comments
					, get_log_flag (t205.c205_part_number_id || '-' || t4021.c4020_demand_master_id,4000105) rlog
					, get_user_name(otn.c4023_last_updated_by) lastupdatedby, otn.c4023_demand_otn_detail_id refid
          			, to_char(otn.c4023_last_updated_date,'MM/DD/YYYY') lastupdateddate
			FROM t4021_demand_mapping t4021
					, t208_set_details t208
					, t205_part_number t205
					, (SELECT  t4023.c205_part_number_id, t4023.c4023_qty otnvalue, t4023.c4023_history_fl
								,t4023.c4023_last_updated_by,t4023.c4023_last_updated_date, t4023.c4023_demand_otn_detail_id
					   FROM t4023_demand_otn_detail t4023
					   WHERE t4023.c4020_demand_master_id = DECODE (p_demandmasterid, '0', c4020_demand_master_id, p_demandmasterid)
					   ) otn
			WHERE t208.c205_part_number_id = otn.c205_part_number_id(+)
			AND t4021.c4020_demand_master_id = DECODE (p_demandmasterid, '0', t4021.c4020_demand_master_id, p_demandmasterid)
			--AND t205.c205_part_number_id LIKE '%' || NVL(p_partnumber, t205.c205_part_number_id) || '%'  
			AND REGEXP_LIKE(t205.c205_part_number_id, CASE WHEN TO_CHAR(v_pnum) IS NOT NULL THEN TO_CHAR(REGEXP_REPLACE(v_pnum,'[+]','\+')) ELSE REGEXP_REPLACE(t205.c205_part_number_id,'[+]','\+') END)
			AND t4021.c901_ref_type IN (40031,4000109)
			AND t4021.c4021_ref_id = t208.c207_set_id
			--AND NVL (t208.c208_set_qty, 0) = 0
			AND t208.c208_void_fl IS NULL
			AND t205.c205_part_number_id = t208.c205_part_number_id
			AND t205.c205_active_fl = 'Y'
			ORDER BY t205.c205_part_number_id;
		
	END gm_fch_part_otn; 
	
	/**********************************************************************
 	* Purpose: This procedure is fetchinng demand sheets accross the world
 	* Created By HReddi
 	***********************************************************************/
	PROCEDURE gm_fch_worldwide_dmd_sheet(
	     p_demandsheet_types IN   VARCHAR2
		,p_out_cur	    	OUT	 TYPES.cursor_type
	)
	AS
	BEGIN
		my_context.set_my_inlist_ctx (p_demandsheet_types);
		
		OPEN p_out_cur 
		 FOR	
	   		 SELECT t4020.c4020_demand_master_id DMID, t4020.c4020_demand_nm DMNM
               FROM t4015_global_sheet_mapping t4015, t4020_demand_master t4020
              WHERE t4020.c4015_global_sheet_map_id = t4015.c4015_global_sheet_map_id
                AND t4020.c4020_void_fl            IS NULL
                AND t4015.c4015_void_fl            IS NULL
                AND t4015.c901_level_id            = 102580   -- company (world wide)
                AND t4020.c4020_inactive_fl        IS NULL
                AND t4020.c901_demand_type         IN ( SELECT token	
                										  FROM v_in_list WHERE token IS NOT NULL)
           ORDER BY UPPER(t4020.c4020_demand_nm) ;
    END gm_fch_worldwide_dmd_sheet;
    
	/********************************************************************
 	* Purpose: This procedure is fetchinng the PAR Details for Set's
 	* Created By HReddi
 	*********************************************************************/
	PROCEDURE gm_fch_set_par(
		p_demand_sheet_id   IN   t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_set_id			IN	 t4021_demand_mapping.c4021_ref_id%TYPE
	  , p_set_name   		IN	 VARCHAR2
	  , p_out_cur	    	OUT	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_cur
			 FOR
				SELECT 
					   DISTINCT t207.c207_set_id setid
				              , t207.c207_set_nm sdesc
  							  , t4020.c4020_demand_master_id dmdmasterid
  							  , t4022.c4022_par_value parvalue
  							  , NVL (t4022.c4022_history_fl, 'N') history_fl
                              , get_user_name (t4022.c4022_updated_by) updatedby
                              , TO_CHAR (t4022.c4022_updated_date, 'MM/dd/yyyy') updateddate
                              , '' comments
                              , get_log_flag (t207.c207_set_id || '-' || t4020.c4020_demand_master_id, 4000115) rlog
                              , t4022.c4022_demand_par_detail_id DTLID
   						   FROM t4021_demand_mapping t4021, T207_SET_MASTER T207, t4020_demand_master t4020, t4015_global_sheet_mapping t4015, t4022_demand_par_detail t4022
						  WHERE t4021.c4020_demand_master_id    = t4022.c4020_demand_master_id(+)
						    AND t4021.c901_ref_type             IN (40031)
						    AND T4021.C4021_REF_ID              = t4022.C207_SET_ID(+)
						    AND T207.C207_SET_ID                = T4021.C4021_REF_ID
						    AND t4020.c4020_demand_master_id    = DECODE (p_demand_sheet_id, '0', t4020.c4020_demand_master_id, p_demand_sheet_id)
						    AND t4020.c4020_demand_master_id    = t4021.c4020_demand_master_id 
						    AND t4015.c4015_global_sheet_map_id = t4020.c4015_global_sheet_map_id
						    AND t4015.c901_company_id           = t4020.c901_company_id
						    AND t4015.c901_level_id             = 102580
						    AND t4020.c4020_inactive_fl         IS NULL
						    AND t4020.c901_demand_type          IN (40021,4000109)
						    AND T4021.C4021_REF_ID 				LIKE '%' || NVL (p_set_id, T4021.C4021_REF_ID) || '%'
						    AND UPPER (T207.C207_SET_NM) 		LIKE '%' || NVL (UPPER (p_set_name), '') || '%'
					   ORDER BY T207.C207_SET_ID ; 
	END gm_fch_set_par;
	
	/********************************************************
 	* Purpose: This procedure will fetch all Active Regions
 	* Created By : Jignesh Shah
 	*********************************************************/
	PROCEDURE gm_fch_region(
		p_grp_ids	IN		VARCHAR2
	  , p_out_cur	OUT	 	TYPES.cursor_type
	)
	AS
	BEGIN
		my_context.set_my_inlist_ctx(p_grp_ids);
		OPEN p_out_cur 
		FOR	
			SELECT * FROM(
	   			SELECT c901_code_id codeid, DECODE(c901_code_grp,'REGN', get_rule_value(C901_CODE_ID, 'REG_SHRT_CODE'),c901_code_nm) CODENM
			 	  FROM T901_Code_Lookup
				 WHERE C901_Code_Grp In (SELECT token FROM v_in_list WHERE token IS NOT NULL)
            	   AND c901_code_id   != 100824  --OUS
			       AND C901_Active_Fl = 1
			       UNION ALL
                SELECT TO_NUMBER(C906_RULE_ID) codeid,C906_RULE_VALUE CODENM 
                FROM T906_RULES WHERE C906_RULE_GRP_ID='REG_SHRT_CODE' 
                AND C906_VOID_FL IS NULL
                AND C906_RULE_ID IN (SELECT C906_RULE_ID from T906_RULES 
                					WHERE C906_RULE_GRP_ID='GOP_ROLLUP_EDIT'
                					AND C906_VOID_FL IS NULL)	
                -- 100361 Northern Europe Zone
			)WHERE CODENM IS NOT NULL Order By Codenm;
    END gm_fch_region;
	
    /*****************************************************************
 	* Purpose: This procedure will fetch Demand sheet Names
 	* Created By : Jignesh Shah
 	*****************************************************************/
	PROCEDURE gm_fch_demand_sheet(
		p_template_id   IN		t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_region_id		IN		t4015_global_sheet_mapping.c901_level_value%TYPE
	  , p_sheet_type	IN		t4020_demand_master.c901_demand_type%TYPE
	  , p_out_cur	    OUT	 	TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_cur 
		FOR	
			SELECT t4020.c4020_demand_master_id DMID, t4020.c4020_demand_nm DMNM
            FROM t4015_global_sheet_mapping t4015, t4020_demand_master t4020
            WHERE t4020.c4015_global_sheet_map_id = t4015.c4015_global_sheet_map_id
            AND t4020.c4020_void_fl         IS NULL
            AND t4020.c4020_inactive_fl     IS NULL
            AND t4015.c4015_void_fl         IS NULL
            AND t4015.c901_level_id         != 102580       --company
            AND t4015.c901_access_type		 = 102623       --editable
            AND T4020.C901_Demand_Type		IN (40020,40021,40022)
            AND t4020.c4020_parent_demand_master_id = decode (p_template_id,'0',t4020.c4020_parent_demand_master_id,p_template_id)
            AND t4015.c901_level_value = decode (p_region_id,'0',t4015.c901_level_value,p_region_id)
            AND t4020.c901_demand_type = decode (p_sheet_type,'0',t4020.c901_demand_type,p_sheet_type)    
            ORDER BY UPPER(t4020.c4020_demand_nm);
    END gm_fch_demand_sheet;

	 /*****************************************************************
 	* Purpose: This procedure is to fetch override demand values
 	* Created By : Elango
 	*****************************************************************/
	PROCEDURE gm_fch_dmd_avg_override(
		p_override_id      IN  t4032_demand_override.c4032_override_id%TYPE
	  , p_demand_master_id IN  t4032_demand_override.c4020_demand_master_id%TYPE
	  , p_ref_id	       IN  t4032_demand_override.c4032_ref_id%TYPE
	  , p_detail_cur	  OUT  TYPES.cursor_type
	)
	AS 
	v_override_id   t4032_demand_override.c4032_override_id%TYPE;
	v_count  NUMBER;	
	BEGIN
	--Initially there will be no records in t4032_demand_override table.
	--So based on below count firing the differnt query to display info in UI.
	   SELECT count(1) 
	     INTO v_count
		 FROM t4032_demand_override T4032, T4020_DEMAND_MASTER T4020
		WHERE T4032.C4020_DEMAND_MASTER_ID = T4020.C4020_DEMAND_MASTER_ID
		  AND T4032.C4020_DEMAND_MASTER_ID = p_demand_master_id 
		  AND T4032.C4032_REF_ID      = p_ref_id  
	  	  AND T4032.C4032_VOID_FL IS NULL;	
	  	  
	  IF (v_count = 0) 
	  THEN 	 
	     OPEN p_detail_cur 
		  FOR	
       SELECT gm_pkg_oppr_sheet.get_demandsheet_name(p_demand_master_id)  demandSheetNm,
			  DECODE(get_set_name(p_ref_id),' ','102960','102961')  reftype,
			  DECODE(get_set_name(p_ref_id),' ',GET_CODE_NAME('102960'),GET_CODE_NAME('102961'))  reftypeNm,
		      p_ref_id || ' - ' || DECODE(get_set_name(p_ref_id),' ',get_partnum_desc(p_ref_id),get_set_name(p_ref_id)) strDescription,
		      NVL(p_demand_master_id,'') demandMasterId
		FROM DUAL;	
    ELSE 
        OPEN p_detail_cur 
		FOR	
		SELECT T4020.C4020_DEMAND_MASTER_ID demandMasterId,
			   T4032.C4032_OVERRIDE_ID DEMANDOVERRIDEID,
			   T4032.C4040_DEMAND_SHEET_ID DEMANDSHEETID,
			   T4020.C4020_DEMAND_NM demandSheetNm , 
		       p_ref_id || ' - ' ||DECODE(T4032.C901_REF_TYPE,'102960',get_partnum_desc(T4032.C4032_REF_ID),get_set_name(T4032.C4032_REF_ID)) strDescription,
		       T4032.C901_REF_TYPE reftype,
		       GET_CODE_NAME(T4032.C901_REF_TYPE) reftypeNm,
		       T4032.C4032_QTY overrideWeigthedAvg,
		       T4032.C4032_HISTORY_FL historyFl
		 FROM t4032_demand_override T4032, T4020_DEMAND_MASTER T4020
		WHERE T4032.C4020_DEMAND_MASTER_ID = T4020.C4020_DEMAND_MASTER_ID
		  AND T4032.C4020_DEMAND_MASTER_ID = p_demand_master_id  
		  AND T4032.C4032_REF_ID      = p_ref_id   
		  AND T4032.C4032_OVERRIDE_ID = NVL(p_override_id ,T4032.C4032_OVERRIDE_ID)
	  	  AND T4032.C4032_VOID_FL IS NULL;	 
    END IF;
END gm_fch_dmd_avg_override;

END gm_pkg_oppr_dmd_mas_rpt;
/
