/* Formatted on 2009/11/24 19:18 (Formatter Plus v4.8.0) */
-- @"C:\database\packages\Operations\Purchasing\gm_pkg_oppr_ttp_summary.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ttp_summary
IS
--
  /*******************************************************
   * Description : Main Procedure to generate ttp
   *               monthly summary cross tab report
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_ttp_mon_summary (
		p_ttpid 		   IN		t4050_ttp.c4050_ttp_id%TYPE
	  , p_monyear		   IN		VARCHAR2
	  , p_demandsheetids   IN		VARCHAR2
	  , p_level_id		   IN		NUMBER
	  , p_inventoryid	   IN OUT	t250_inventory_lock.c250_inventory_lock_id%TYPE
	  , p_forecastmonth    IN OUT	t4052_ttp_detail.c4052_forecast_period%TYPE
	  , p_outfcheader	   OUT		TYPES.cursor_type
	  , p_outfcdetails	   OUT		TYPES.cursor_type
	  , p_outinvheader	   OUT		TYPES.cursor_type
	  , p_outinvdetails    OUT		TYPES.cursor_type
	  , p_outtotalreqqty   OUT		TYPES.cursor_type
	  , p_outtotordqty	   OUT		TYPES.cursor_type
	  , p_outunitprice	   OUT		TYPES.cursor_type
	  , p_sheetname 	   OUT		TYPES.cursor_type
	  , p_status		   OUT		VARCHAR2
	  , p_finaldate 	   OUT		VARCHAR2
	  , p_ttp_type		   OUT		VARCHAR2
	  , p_parentsubdtl	   OUT		TYPES.cursor_type
	  , p_ds_status 	   OUT		VARCHAR2
	  , p_outaddheader	   OUT		TYPES.cursor_type
	  , p_outadddetails	   OUT		TYPES.cursor_type
	)
	AS
		v_ttp_detail_id t4052_ttp_detail.c4052_ttp_detail_id%TYPE;
		v_demand_sheetids VARCHAR2 (5000);
		v_string	   VARCHAR2 (100);
		v_inventoryid  t250_inventory_lock.c250_inventory_lock_id%TYPE;
		v_forecast_period t4052_ttp_detail.c4052_forecast_period%TYPE;
		v_fc_period_for_calc t4052_ttp_detail.c4052_forecast_period%TYPE;
		v_fc_start_date DATE;
		
		CURSOR cur_demandsheetids
		IS
			SELECT c4040_demand_sheet_id ID
			  FROM t4040_demand_sheet
			 WHERE c4052_ttp_detail_id = v_ttp_detail_id and c901_level_id=p_level_id;
	BEGIN
		SELECT c901_ttp_type
		  INTO p_ttp_type
		  FROM t4050_ttp
		 WHERE c4050_ttp_id = p_ttpid;
		 
		 SELECT get_rule_value('FC_MONTHS','TTPCALCULATION') 
		   INTO v_fc_period_for_calc 
		   FROM DUAl;

		IF (p_demandsheetids IS NULL)
		THEN
			v_demand_sheetids := 0;

			-- Fetch TTP Summary information
			SELECT c4052_ttp_detail_id, c901_status, c4052_forecast_period
			  INTO v_ttp_detail_id, p_status, v_forecast_period
			  FROM t4052_ttp_detail
			 WHERE c4050_ttp_id = p_ttpid AND TO_CHAR (c4052_ttp_link_date, 'MM/YYYY') = p_monyear;
			 
            FOR cur_val IN cur_demandsheetids
			LOOP
				v_demand_sheetids := cur_val.ID || ',' || v_demand_sheetids;
			END LOOP;
		--
		   p_forecastmonth := 	v_forecast_period;
		ELSE			
			v_demand_sheetids := p_demandsheetids;
			v_forecast_period := p_forecastmonth;
			p_status	:= NULL;			
		END IF;
		
		v_inventoryid := p_inventoryid;			
		
		my_context.set_my_inlist_ctx (v_demand_sheetids); 	
		
		SELECT DECODE (COUNT (1), 0, 'false', 'true')
		  INTO p_ds_status
		  FROM t4040_demand_sheet t4040
		 WHERE c4040_demand_sheet_id IN (SELECT *
										   FROM v_in_list) AND t4040.c901_status <> 50551;

		-- Below code to fecth total order qty
		gm_fc_fch_totreqqty_ttp (v_ttp_detail_id, p_outtotordqty);
		-- Below procedure called to fetch ttp
		gm_fc_fch_demandsheet_name (p_sheetname);
		gm_fc_fch_fcheader (v_fc_period_for_calc, p_outfcheader);
		gm_fc_fch_fcdetails (v_fc_period_for_calc, v_forecast_period, p_outfcdetails);
		gm_fc_fch_invheader (p_outinvheader);
		gm_fc_fch_invdetails (v_inventoryid, p_outinvdetails);
		-- Code to fetch unit cost
		--gm_fc_fch_unitprice (v_ttp_detail_id, p_outunitprice);
		gm_fc_fch_part_attributes(v_inventoryid,p_level_id, p_outunitprice);
		-- Below sql to fetch total required qty
		gm_fc_fch_totreqqty (v_forecast_period, v_inventoryid, p_outtotalreqqty);
		gm_fc_fch_additional_header(v_forecast_period,p_outaddheader);
		gm_fc_fch_additional_need(v_forecast_period,p_outadddetails);
		
		gm_ld_parent_sub_parts (v_inventoryid, p_parentsubdtl);

		SELECT TO_CHAR (MAX (c4042_period), 'Mon YY')	-- '8/1/2007'
		  INTO p_finaldate
		  FROM t4042_demand_sheet_detail t4042
		 WHERE t4042.c901_type = 50563 AND t4042.c4040_demand_sheet_id IN (SELECT *
																			 FROM v_in_list);	   
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			raise_application_error (-20900, '');	-- No Demand Sheets have been mapped
	END gm_fc_fch_ttp_mon_summary;

--
  /*******************************************************
   * Description : Procedure to fetch Demand Sheet information
   *			   header and footer information
   *******************************************************/
--
	PROCEDURE gm_fc_fch_demandsheet_name (
		p_sheetname   OUT	TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_sheetname
		 FOR
			 SELECT   t4040.c4020_demand_master_id dmid, t4040.c4040_demand_sheet_id ID, t4020.c4020_demand_nm NAME
				 FROM t4020_demand_master t4020, t4040_demand_sheet t4040
				WHERE t4040.c4040_demand_sheet_id IN (SELECT *
														FROM v_in_list)
				  AND t4020.c4020_demand_master_id = t4040.c4020_demand_master_id
			 ORDER BY NAME;
	END gm_fc_fch_demandsheet_name;

--
  /*******************************************************
   * Description : Procedure to generate header for forecast
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_fcheader (
		p_forecastmonth   IN	   t4052_ttp_detail.c4052_forecast_period%TYPE
	  , p_outfcheader	  OUT	   TYPES.cursor_type
	)
	AS
		v_fc_start_date DATE;
	BEGIN
		SELECT MIN (c4042_period)	-- '8/1/2007'
		  INTO v_fc_start_date
		  FROM t4042_demand_sheet_detail t4042
		 WHERE t4042.c901_type = 50563 AND t4042.c4040_demand_sheet_id IN (SELECT *
																			 FROM v_in_list);

		OPEN p_outfcheader
		 FOR
			 SELECT   period, seq_no, c4042_period
				 FROM (
				 	   SELECT 'Description' period, 0 seq_no, TO_DATE ('01/01/2000', 'MM/DD/YYYY') c4042_period
						 FROM DUAL
					   UNION ALL
					   SELECT DISTINCT DECODE (t4042.c901_type
											 , 50563, 'FC-'|| TO_CHAR (c4042_period, 'Mon YY')
											 , 4000110, 'FC_US-'||TO_CHAR (C4042_PERIOD, 'Mon YY')
                                             , 4000111, 'FC_OUS-'||TO_CHAR (c4042_period, 'Mon YY')
											 , get_code_name (t4042.c901_type)
											  ) period
									 , t901.c901_code_seq_no seq_no, c4042_period
								  FROM t4042_demand_sheet_detail t4042, t901_code_lookup t901
								 WHERE t4042.c901_type = t901.c901_code_id
								   AND t4042.c901_type IN
										   (50563,4000110,4000111)   -- Forecast , Pending Back Order, Pending Back Log, Pending Consign and Ship
								   AND t4042.c4040_demand_sheet_id IN (SELECT *
																		 FROM v_in_list)
								   AND t4042.c4042_period BETWEEN v_fc_start_date
															  AND ADD_MONTHS (v_fc_start_date, (p_forecastmonth - 1)))
			 ORDER BY seq_no, c4042_period;
	END gm_fc_fch_fcheader;

	
--
  /*******************************************************
   * Description : Procedure to generate additional need header
   * Author 	 : APrasath
   *******************************************************/
--
	PROCEDURE gm_fc_fch_additional_header (
		p_forecastmonth   IN	   t4052_ttp_detail.c4052_forecast_period%TYPE
	  , p_outaddheader	  OUT	   TYPES.cursor_type
	)
	AS		
	v_fc_start_date DATE;
	BEGIN
		SELECT MIN (c4042_period)	-- '8/1/2007'
		  INTO v_fc_start_date
		  FROM t4042_demand_sheet_detail t4042
		 WHERE t4042.c901_type IN (4000112, 4000113) AND t4042.c4040_demand_sheet_id IN (SELECT *
																			 FROM v_in_list);

		OPEN p_outaddheader
		 FOR
			   SELECT DISTINCT get_code_name (t4042.c901_type) period
									 , t901.c901_code_seq_no seq_no, c4042_period
								  FROM t4042_demand_sheet_detail t4042, t901_code_lookup t901
								 WHERE t4042.c901_type = t901.c901_code_id
								   AND t4042.c901_type IN (4000112,4000113)   -- set par,one time need
								   AND t4042.c4040_demand_sheet_id IN (SELECT * FROM v_in_list) 
									AND t4042.c4042_period BETWEEN v_fc_start_date
																	   AND ADD_MONTHS (v_fc_start_date
																					 , (p_forecastmonth - 1)
																					  )
								  
			 ORDER BY seq_no, c4042_period;
	END gm_fc_fch_additional_header;
--
  /*******************************************************
   * Description : Procedure to generate details seection for forecast
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_fcdetails (
		p_forecastcalcmonth   IN	   t4052_ttp_detail.c4052_forecast_period%TYPE
	  , p_forecastmonth   IN	   t4052_ttp_detail.c4052_forecast_period%TYPE	
	  , p_outfcdetails	  OUT	   TYPES.cursor_type
	)
	AS
		v_fc_start_date DATE;
	BEGIN
		SELECT MIN (c4042_period)	-- '8/1/2007'
		  INTO v_fc_start_date
		  FROM t4042_demand_sheet_detail t4042
		 WHERE t4042.c901_type = 50563 AND t4042.c4040_demand_sheet_id IN (SELECT *
																			 FROM v_in_list);

		OPEN p_outfcdetails
		 FOR
			 SELECT   DECODE (t4042.c205_part_number_id
							, NULL, DECODE (t205.c205_product_family
										  , NULL, DECODE (GROUPING_ID (t205.c205_product_family
																	 , t4042.c205_part_number_id
																	  )
														, 3, 'Total'
														, 'Group Missing'
														 )
										  , t205.c205_product_family
										   )
							, t4042.c205_part_number_id
							 ) num
					, DECODE (t4042.c205_part_number_id
							, NULL, DECODE (t205.c205_product_family
										  , NULL, DECODE (GROUPING_ID (t205.c205_product_family
																	 , t4042.c205_part_number_id
																	  )
														, 3, 'Total'
														, 'Group Missing'
														 )
										  , get_code_name (t205.c205_product_family)
										   )
							, t4042.c205_part_number_id
							 ) pnum
					, DECODE (t4042.c901_type
							, 50563,  'FC-' || TO_CHAR (c4042_period, 'Mon YY')
							, 4000110, 'FC_US-'||TO_CHAR (C4042_PERIOD, 'Mon YY')
                            , 4000111, 'FC_OUS-'||TO_CHAR (c4042_period, 'Mon YY')
							, get_code_name (t4042.c901_type)
							 ) period
					,  SUM ( DECODE(refid
							, NULL, DECODE (t4042.c901_type
										  , 50563, c4042_qty
										  , 4000110, C4042_US_QTY
			              				  , 4000111, c4042_OUS_qty
										  , 0
							 			  )
							 , 0)) qty
					--, SUM (c4042_qty) qty
					  ,GROUPING_ID (t205.c205_product_family, t4042.c205_part_number_id) grpid
					  , 'N' hfl
					-- no need hfl to show override color on the ttp summary screen
					, DECODE (t4042.c205_part_number_id
							, NULL, DECODE (t205.c205_product_family
										  , NULL, DECODE (GROUPING_ID (t205.c205_product_family
																	 , t4042.c205_part_number_id
																	  )
														, 3, ' '
														, 'Group Missing'
														 )
										  , ' '
										   )
							, t205.c205_part_num_desc
							 ) description
					, NVL (c205_crossover_fl, 'N') crossoverfl 
				 FROM
					  -- Below code to fetch demand sheet based on par value
					  (SELECT t4042.c4040_demand_sheet_id, t4042.c205_part_number_id, t4042.c4042_period , t4042.c4042_history_fl
							, T4042.C901_TYPE
							, NVL(DECODE (t4042.c901_type
                                                               , 50563, DECODE (parvalue.c205_part_number_id
                                                                                      , NULL, t4042.c4042_qty
                                                                                      , DECODE (parvalue.parperiod
                                                                                                  , t4042.c4042_period, parvalue.parvalue
                                                                                                  , 0
                                                                                                   )
                                                                                       )
                                                               , 0
                                                                  ),0) C4042_QTY
                            , NVL(DECODE (t4042.c901_type
                                                               , 4000110, DECODE (parvalue.c205_part_number_id
                                                                                      , NULL, t4042.c4042_qty
                                                                                      , DECODE (parvalue.parperiod
                                                                                                  , t4042.c4042_period, parvalue.usparvalue
                                                                                                  , 0
                                                                                                   )
                                                                                       )
                                                               , 0
                                                                  ),0)C4042_US_QTY
                            ,NVL(DECODE (t4042.c901_type
                                                               , 4000111, DECODE (parvalue.c205_part_number_id
                                                                                      , NULL, t4042.c4042_qty
                                                                                      , DECODE (parvalue.parperiod
                                                                                                  , t4042.c4042_period, parvalue.ousparvalue
                                                                                                  , 0
                                                                                                   )
                                                                                       )
                                                               , 0
                                                                  ),0)c4042_OUS_qty
							, t4042.c4042_parent_ref_id refid
						 FROM T4042_DEMAND_SHEET_DETAIL t4042
							, (SELECT c4040_demand_sheet_id, c205_part_number_id, parperiod
                                            , demandvalue, usdemand, ousdemand, parvalue
                         , (CASE WHEN (parvalue - ousdemand) > 0 THEN (parvalue - ousdemand) ELSE 0 END) usparvalue, ousdemand ousparvalue
                                                   FROM (SELECT   t4042.c4040_demand_sheet_id, t4042.c205_part_number_id
                                                                        , MIN (t4042.c4042_period) parperiod
                                                                        , SUM (DECODE (t4042.c901_type, 50563, t4042.c4042_qty, 0)) demandvalue
                                                                        , SUM (DECODE (t4042.c901_type, 4000110, t4042.c4042_qty, 0)) usdemand
                                                                        , SUM (DECODE (t4042.c901_type, 4000111, t4042.c4042_qty, 0)) ousdemand
                                                                        , SUM (DECODE (t4042.c901_type, 50566, t4042.c4042_qty, 0)) parvalue
                                                                  FROM T4042_DEMAND_SHEET_DETAIL T4042
                                                                  WHERE t4042.c4040_demand_sheet_id IN (SELECT *
																				  FROM v_in_list)
                                                                    AND t4042.c4042_period BETWEEN v_fc_start_date
                                                                                                            AND ADD_MONTHS (v_fc_start_date
                                                                                                                                 , (p_forecastmonth - 1)
                                                                                                                                    )
                                                                    AND t4042.c901_type IN (50563, 4000110, 4000111,  50566)
                                                            GROUP BY t4042.c4040_demand_sheet_id, t4042.c205_part_number_id)
                                                  WHERE parvalue > demandvalue) parvalue 	 -- Above to fetch part which has value more than par
						WHERE T4042.C4040_DEMAND_SHEET_ID IN (SELECT *
																				  FROM v_in_list)
						  AND t4042.c901_type IN (50563,4000110, 4000111)
						  AND t4042.c4042_period <= ADD_MONTHS (v_fc_start_date, (p_forecastcalcmonth - 1))
						  AND t4042.c4040_demand_sheet_id = parvalue.c4040_demand_sheet_id(+)
						  AND t4042.c205_part_number_id = parvalue.c205_part_number_id(+)) t4042
					, T205_PART_NUMBER T205
				WHERE t4042.c205_part_number_id = t205.c205_part_number_id
				  AND t4042.c205_part_number_id IS NOT NULL
			 GROUP BY c4042_period
					, t4042.c901_type
					, ROLLUP (t205.c205_product_family
							, (T4042.C205_PART_NUMBER_ID, T205.C205_PART_NUM_DESC, T205.C205_CROSSOVER_FL))
			 ORDER BY t205.c205_product_family, grpid, num, period;
	END gm_fc_fch_fcdetails;

--
  /*******************************************************
   * Description : Procedure to generate details seection for forecast
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_additional_need (
		p_forecastmonth   IN	   t4052_ttp_detail.c4052_forecast_period%TYPE
	  , p_outadddetails	  OUT	   TYPES.cursor_type
	)
	AS
		v_fc_start_date DATE;
	BEGIN
		SELECT MIN (c4042_period)	-- '8/1/2007'
		  INTO v_fc_start_date
		  FROM t4042_demand_sheet_detail t4042
		 WHERE t4042.c901_type IN (4000112, 4000113, 4000119, 4000120) AND t4042.c4040_demand_sheet_id IN (SELECT *
																			 FROM v_in_list);

		OPEN p_outadddetails
		 FOR
			 SELECT   DECODE (t4042.c205_part_number_id
							, NULL, DECODE (t205.c205_product_family
										  , NULL, DECODE (GROUPING_ID (t205.c205_product_family
																	 , t4042.c205_part_number_id
																	  )
														, 3, 'Total'
														, 'Group Missing'
														 )
										  , t205.c205_product_family
										   )
							, t4042.c205_part_number_id
							 ) num
					, DECODE (t4042.c205_part_number_id
							, NULL, DECODE (t205.c205_product_family
										  , NULL, DECODE (GROUPING_ID (t205.c205_product_family
																	 , t4042.c205_part_number_id
																	  )
														, 3, 'Total'
														, 'Group Missing'
														 )
										  , get_code_name (t205.c205_product_family)
										   )
							, t4042.c205_part_number_id
							 ) pnum
					,  get_code_name (t4042.c901_type)
							  period
					, SUM (DECODE (refid, NULL, c4042_qty, 0)) qty
					--, SUM (c4042_qty) qty
					  ,GROUPING_ID (t205.c205_product_family, t4042.c205_part_number_id) grpid
					, NVL (c205_crossover_fl, 'N') crossoverfl
					, DECODE (t4042.c205_part_number_id
							, NULL, DECODE (t205.c205_product_family
										  , NULL, DECODE (GROUPING_ID (t205.c205_product_family
																	 , t4042.c205_part_number_id
																	  )
														, 3, 'Total'
														, 'Group Missing'
														 )
										  , get_code_name (t205.c205_product_family)
										   )
							, t205.c205_part_num_desc
							 ) description
				 FROM
					  -- Below code to fetch demand sheet based on par value
					  (SELECT t4042.c4040_demand_sheet_id, t4042.c205_part_number_id, t4042.c4042_period
							, t4042.c901_type
							, t4042.c4042_qty c4042_qty
							, t4042.c4042_parent_ref_id refid
						 FROM T4042_DEMAND_SHEET_DETAIL t4042							
						WHERE T4042.C4040_DEMAND_SHEET_ID IN (SELECT *
																FROM v_in_list)
						  AND t4042.c901_type IN (4000112, 4000113, 4000119, 4000120) --set par,one time need
						  AND t4042.c4042_period BETWEEN v_fc_start_date
																	   AND ADD_MONTHS (v_fc_start_date
																					 , (p_forecastmonth - 1)
																					  )
						  ) t4042
					, T205_PART_NUMBER T205
				WHERE t4042.c205_part_number_id = t205.c205_part_number_id  
				  AND t4042.c205_part_number_id IS NOT NULL
			 GROUP BY c4042_period
					, t4042.c901_type
					, ROLLUP (t205.c205_product_family
							, (T4042.C205_PART_NUMBER_ID, T205.C205_PART_NUM_DESC, T205.C205_CROSSOVER_FL))
			 ORDER BY t205.c205_product_family, grpid, num, period;
	END gm_fc_fch_additional_need;	
--
  /*******************************************************
   * Description : Procedure to generate header seection for inventory
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_invheader (
		p_outinvheader	 OUT   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outinvheader
		 FOR
			 SELECT   c901_code_nm period, c901_code_seq_no seq
				 FROM t901_code_lookup
				WHERE c901_code_id IN (20464, 20462, 20460, 20477, 20454, 20455, 4000114)
			 UNION
			 SELECT   'Total Inventory' period, 100 seq
				 FROM DUAL
			 ORDER BY seq;
	END gm_fc_fch_invheader;

--
  /*******************************************************
   * Description : Procedure to generate details section for inventory
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_invdetails (
		p_inventoryid	  IN	   t250_inventory_lock.c250_inventory_lock_id%TYPE
	  , p_outinvdetails   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outinvdetails
		--PMT-30031 GOP_To show Inventory details in TTP summary
		 FOR
			SELECT DECODE (T251.C205_Part_Number_Id , NULL, 
			DECODE (T205.C205_Product_Family , NULL, 
			DECODE (Grouping_Id (T205.C205_Product_Family , T251.C205_Part_Number_Id ) , 3, 'Total' , 'Group Missing' ) , T205.C205_Product_Family ) , 
			T251.C205_Part_Number_Id ) Id ,
            DECODE (T251.C205_Part_Number_Id , NULL, 
            DECODE (T205.C205_Product_Family , NULL, 
            DECODE (Grouping_Id (T205.C205_Product_Family , T251.C205_Part_Number_Id ) , 3, 'Total' , 'Group Missing' ) ,
            Get_Code_Name (T205.C205_Product_Family) ) , T251.C205_Part_Number_Id ) Name ,
            SUM(T251.C251_Quar_Alloc) AS "Quarantine Allocated (Scrap) ",
            SUM(T251.C251_Quar_Avail) AS "Quarantine Allocated (Inventory) ",
            SUM(T251.C251_Open_Po)    AS "Open PO ",
            SUM(T251.C251_Open_Dhr)   AS "Open DHR ",
            SUM(T251.C251_Build_Set)  AS "Build Set",
            SUM(T251.C251_In_Stock)   AS "In Stock ",
            SUM(T251.C251_Parent_Part_Qty) AS "Parent Part Qty",
            SUM(T251.C251_Rw_Inventory)    AS "RW Inventory",
            SUM(NVL(T251.C251_In_Stock,0)+ NVL(T251.C251_Open_Po,0)+ NVL(T251.C251_Open_Dhr,0) + NVL(T251.C251_Build_Set,0)) AS "Total Inventory",
            GROUPING_ID (t205.c205_product_family, t251.c205_part_number_id) grpid ,
             T205.C205_Product_Family Pfamily
          FROM T251_INVENTORY_LOCK_DETAILs t251 ,
             (SELECT DISTINCT T4042.C205_Part_Number_Id
              FROM t4042_demand_sheet_detail t4042
              WHERE t4042.c4040_demand_sheet_id IN
                                                (SELECT * FROM v_in_list
                                                 )
              AND t4042.c901_type IN (50563)
            ) t4042 ,
           t205_part_number t205
          WHERE t251.c205_part_number_id  = t4042.c205_part_number_id
          AND t251.c205_part_number_id    = t205.c205_part_number_id
          AND t251.c250_inventory_lock_id = p_inventoryid
          GROUP BY rollup(t205.c205_product_family,t251.c205_part_number_id);
	END gm_fc_fch_invdetails;

--
  /*******************************************************
   * Description : Procedure to generate details section for Total Required Qty
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_totreqqty (
		p_forecastmonth 	  IN	   t4052_ttp_detail.c4052_forecast_period%TYPE
	  , p_inventory_lock_id   IN	   t250_inventory_lock.c250_inventory_lock_id%TYPE
	  , p_outordqtydetails	  OUT	   TYPES.cursor_type
	)
	AS
		v_fc_start_date DATE;
	BEGIN
		SELECT MIN (c4042_period)	-- '8/1/2007'
		  INTO v_fc_start_date
		  FROM t4042_demand_sheet_detail t4042
		 WHERE t4042.c901_type = 50563 AND t4042.c4040_demand_sheet_id IN (SELECT *
																			 FROM v_in_list);

		OPEN p_outordqtydetails
		 FOR
		 --PMT-30031 GOP_To show Inventory details in TTP summary
			SELECT DECODE (t205.c205_part_number_id , NULL, DECODE (t205.c205_product_family , NULL, DECODE (GROUPING_ID (t205.c205_product_family , t205.c205_part_number_id ) , 3, 'Total' , 'Group Missing' ) , t205.c205_product_family ) , t205.c205_part_number_id ) ID ,
                   DECODE (t205.c205_part_number_id , NULL, DECODE (t205.c205_product_family , NULL, DECODE (GROUPING_ID (t205.c205_product_family , t205.c205_part_number_id ) , 3, 'Total' , 'Group Missing' ) , get_code_name (t205.c205_product_family) ) , t205.c205_part_number_id ) NAME ,
                   1 pqty ,
                   SUM (DECODE (refid, NULL,NVL(c4042_qty, 0),0)) forecast_qty ,
                   SUM (DECODE (refid, NULL,NVL(c4042_US_qty, 0),0)) forecast_qty_US ,
                   SUM (DECODE (refid, NULL,NVL(c4042_OUS_qty, 0),0)) forecast_qty_OUS ,
                   SUM (DECODE (refid, NULL,NVL(TPR, 0),0)) TPR ,
                   SUM (DECODE (refid, NULL,NVL(TPR_US, 0),0)) TPR_US ,
                   SUM (DECODE (refid, NULL,NVL(TPR_OUS, 0),0)) TPR_OUS ,
                   GROUPING_ID (t205.c205_product_family, t205.c205_part_number_id) grpid
           FROM -- Below code to fetch demand sheet based on par value
             (SELECT t4042.c205_part_number_id ,
              SUM (DECODE (t4042.c901_type , 50563, DECODE (parvalue.c205_part_number_id , NULL, t4042.c4042_qty , DECODE (parvalue.parperiod , t4042.c4042_period, parvalue.parvalue , 0 ) ) , 0 ) ) c4042_qty ,
              SUM (DECODE (t4042.c901_type , 4000110, DECODE (parvalue.c205_part_number_id , NULL, t4042.c4042_qty , DECODE (parvalue.parperiod , t4042.c4042_period, parvalue.usparvalue , 0 ) ) , 0 ) ) c4042_US_qty ,
              SUM (DECODE (t4042.c901_type , 4000111, DECODE (parvalue.c205_part_number_id , NULL, t4042.c4042_qty , DECODE (parvalue.parperiod , t4042.c4042_period, parvalue.ousparvalue , 0 ) ) , 0 ) ) c4042_OUS_qty ,
              t4042.c4042_parent_ref_id refid
          FROM t4042_demand_sheet_detail t4042 ,
              (SELECT c4040_demand_sheet_id,
               c205_part_number_id,
               parperiod ,
               demandvalue,
               usdemand,
               ousdemand,
               parvalue ,
             (
         CASE
         WHEN (parvalue - ousdemand) > 0
         THEN (parvalue - ousdemand)
         ELSE 0
         END) usparvalue,
         ousdemand ousparvalue
       FROM
         (SELECT t4042.c4040_demand_sheet_id,
          t4042.c205_part_number_id ,
          MIN (t4042.c4042_period) parperiod ,
          SUM (DECODE (t4042.c901_type, 50563, t4042.c4042_qty, 0)) demandvalue ,
          SUM (DECODE (t4042.c901_type, 4000110, t4042.c4042_qty, 0)) usdemand ,
          SUM (DECODE (t4042.c901_type, 4000111, t4042.c4042_qty, 0)) ousdemand ,
          SUM (DECODE (t4042.c901_type, 50566, t4042.c4042_qty, 0)) parvalue
      FROM t4042_demand_sheet_detail t4042
      WHERE t4042.c4040_demand_sheet_id IN
        (SELECT * FROM v_in_list
        )
      AND t4042.c4042_period BETWEEN v_fc_start_date AND ADD_MONTHS (v_fc_start_date , (p_forecastmonth - 1) )
      AND t4042.c901_type IN (50563, 4000110, 4000111, 50566)
      GROUP BY t4042.c4040_demand_sheet_id,
        t4042.c205_part_number_id
        )
      WHERE parvalue > demandvalue
       ) parvalue -- Above to fetch part which has value more than par
      WHERE t4042.c4040_demand_sheet_id IN
      (SELECT * FROM v_in_list
      )
      AND t4042.c901_type  IN (50563, 4000106, 4000110, 4000111)
      AND t4042.c4042_period BETWEEN v_fc_start_date AND ADD_MONTHS (v_fc_start_date, (p_forecastmonth - 1))
      AND t4042.c4040_demand_sheet_id = parvalue.c4040_demand_sheet_id(+)
      AND t4042.c205_part_number_id   = parvalue.c205_part_number_id(+)
     GROUP BY t4042.c205_part_number_id,
      t4042.c4042_parent_ref_id
      ) forecast ,
      (SELECT t251.c205_part_number_id ,
       SUM(C251_TPR) TPR ,
       SUM(C251_TPR_US) TPR_US ,
       SUM(C251_TPR_OUS) TPR_OUS
     FROM T250_Inventory_Lock T250,
          t251_inventory_lock_details t251
    WHERE t250.c250_inventory_lock_id = t251.c250_inventory_lock_id
    AND t250.c250_inventory_lock_id   = p_inventory_lock_id
    GROUP BY C205_Part_Number_Id
      ) inventory ,
      t205_part_number t205
   WHERE t205.c205_part_number_id = forecast.c205_part_number_id
     AND t205.c205_part_number_id   = inventory.c205_part_number_id(+)
     AND t205.c205_part_number_id  IS NOT NULL
     GROUP BY Rollup (T205.C205_Product_Family, T205.C205_Part_Number_Id)
     ORDER BY t205.c205_product_family,grpid,NAME;
	END gm_fc_fch_totreqqty;

--
--
  /*******************************************************
   * Description : Procedure to generate details section for Total Required Qty for TTP Summary
   * Author 	 : D James
   *******************************************************/
--
	PROCEDURE gm_fc_fch_totreqqty_ttp (
		p_ttpdetailid		 IN 	  t4053_ttp_part_detail.c4052_ttp_detail_id%TYPE
	  , p_outordqtydetails	 OUT	  TYPES.cursor_type
	)
	AS
		v_fc_start_date VARCHAR2 (20);
	BEGIN
		OPEN p_outordqtydetails
		 FOR
			 
		 SELECT  
		 DECODE (t4053.c205_part_number_id, NULL,
		 DECODE (t205.c205_product_family, NULL, DECODE (GROUPING_ID (t205.c205_product_family, t4053.c205_part_number_id), 3, 'Total', 'Group Missing'), t205.c205_product_family), t4053.c205_part_number_id) ID, 
		 DECODE (t4053.c205_part_number_id, NULL, DECODE (t205.c205_product_family, NULL, DECODE (GROUPING_ID (t205.c205_product_family, t4053.c205_part_number_id),3,'Total','Group Missing'), get_group_name (t205.c205_product_family)), t4053.c205_part_number_id) NAME, 
		 SUM (t4053.c4053_qty) qty, 
		 GROUPING_ID (t205.c205_product_family, 
		 t4053.c205_part_number_id) grpid, 
		 SUM (t4053.c4053_qty * t4053.c4053_cost_price) totalcost
		 FROM t4053_ttp_part_detail t4053, t205_part_number t205
		 WHERE t4053.c205_part_number_id = t205.c205_part_number_id
		 AND t4053.c4052_ttp_detail_id = p_ttpdetailid
	     GROUP BY ROLLUP (t205.c205_product_family, t4053.c205_part_number_id)
	     ORDER BY t205.c205_product_family, grpid, NAME;
	END gm_fc_fch_totreqqty_ttp;
--
 /*******************************************************
   * Description : Procedure to generate Unit Price for each part
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_part_attributes (
		p_inventory_lock_id   IN	   t250_inventory_lock.c250_inventory_lock_id%TYPE
	  , p_level_id			  IN	   NUMBER  	
	  , p_outunitprice	 	  OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		 OPEN p_outunitprice
			 FOR
				  SELECT t255.c205_part_number_id id, t205.c205_part_num_desc nm, t255.c255_lead_time lead_time
					   , t255.c255_calc_lead_time calc_lead_time
				   	   , DECODE(p_level_id,102580,t255.c255_unit_cost,0,c255_unit_cost,0) unit_price
				   FROM t255_part_attribute_lock t255, t205_part_number t205
			      WHERE t255.c250_inventory_lock_id = p_inventory_lock_id
	    			AND t255.c205_part_number_id = t205.c205_part_number_id
	    			AND t255.c205_part_number_id IN(                
						         SELECT  DISTINCT c205_part_number_id 
						           FROM t4042_demand_sheet_detail t4042, v_in_list
						          WHERE t4042.c4040_demand_sheet_id = token 
						            AND t4042.c901_type = 50563);
	END gm_fc_fch_part_attributes;
--
 /*******************************************************
   * Description : Procedure to generate Unit Price for each part
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_unitprice (
		p_ttpdetailid	 IN 	  t4053_ttp_part_detail.c4052_ttp_detail_id%TYPE
	  , p_outunitprice	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		IF (p_ttpdetailid IS NULL)
		THEN
			OPEN p_outunitprice
			 FOR
				 SELECT   t4042.c205_part_number_id ID, t205.c205_part_num_desc nm
						, MAX (NVL (t405.c405_cost_price, 0) / NVL (c405_uom_qty, 1)) unit_price
					 FROM t205_part_number t205
						, t4040_demand_sheet t4040
						, t4042_demand_sheet_detail t4042
						, T405_VENDOR_PRICING_DETAILS t405
					WHERE t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
					  AND t205.c205_part_number_id = t4042.c205_part_number_id
					  AND t205.c205_part_number_id = t405.c205_part_number_id(+)
					  AND t405.c405_active_fl(+) = 'Y'
					  AND t405.c405_void_fl IS NULL
					  AND t4042.c901_type IN (50563, 50567, 50568, 50569)	--Forecast, PBO, PBL, PCS
					  AND t4040.c4040_demand_sheet_id IN (SELECT *
															FROM v_in_list)
				 GROUP BY t4042.c205_part_number_id, t205.c205_part_num_desc;
		ELSE
			OPEN p_outunitprice
			 FOR
				 SELECT   t4053.c205_part_number_id ID, '' nm, t4053.c4053_cost_price unit_price
					 FROM t4053_ttp_part_detail t4053, t205_part_number t205
					WHERE t4053.c4052_ttp_detail_id = p_ttpdetailid
					  AND t205.c205_part_number_id = t4053.c205_part_number_id
				 ORDER BY t205.c205_product_family, t205.c205_part_number_id;
		END IF;
	END gm_fc_fch_unitprice;

		--
  /*******************************************************
   * Description : Procedure to fetch demand sheets associated with the part
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_partdemandsheetlist (
		p_demandsheetids   IN		VARCHAR2
	  , p_pnum			   IN		t205_part_number.c205_part_number_id%TYPE
	  , p_outheader 	   OUT		TYPES.cursor_type
	  , p_outdetails	   OUT		TYPES.cursor_type
	  , p_partdetails	   OUT		VARCHAR2
	)
	AS
	BEGIN
		-- The input String in the form of 5,8 will be set into this temp view.
		my_context.set_my_inlist_ctx (p_demandsheetids);

		SELECT c205_part_number_id || ' - ' || c205_part_num_desc
		  INTO p_partdetails
		  FROM t205_part_number
		 WHERE c205_part_number_id = p_pnum;

		OPEN p_outheader
		 FOR
			 SELECT DISTINCT DECODE (t4042.c901_type
								   , 50566, get_code_name (50566)
								   , TO_CHAR (t4042.c4042_period, 'Mon YY')
									) period
						   , t4042.c4042_period period1
						FROM t4040_demand_sheet t4040, t4042_demand_sheet_detail t4042
					   WHERE t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
						 AND t4040.c4040_demand_sheet_id IN (SELECT *
															   FROM v_in_list)
						 AND t4042.c205_part_number_id = p_pnum
						 AND t4042.c901_type IN (50563, 50566)
					ORDER BY period1, period DESC;

		OPEN p_outdetails
		 FOR
			 SELECT   t4040.c4020_demand_master_id dmdid
					, gm_pkg_oppr_sheet.get_demandsheet_name (t4040.c4020_demand_master_id) dmdsheetname
					, DECODE (t4042.c901_type
							, 50566, get_code_name (50566)
							, TO_CHAR (t4042.c4042_period, 'Mon YY')
							 ) period
					, SUM (t4042.c4042_qty) qty
				 FROM t4040_demand_sheet t4040, t4042_demand_sheet_detail t4042
				WHERE t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
				  AND t4040.c4040_demand_sheet_id IN (SELECT *
														FROM v_in_list)
				  AND t4042.c205_part_number_id = p_pnum
				  AND t4042.c901_type IN (50563, 50566)
			 GROUP BY t4040.c4020_demand_master_id, t4042.c901_type, t4042.c4042_period
			 ORDER BY t4040.c4020_demand_master_id, t4042.c901_type, t4042.c4042_period;
	END gm_fc_fch_partdemandsheetlist;

		--
  /*******************************************************
   * Description : Procedure to fetch request associated
				   with the demand sheets
   * Author 	 : VPrasath
   *******************************************************/
--
	PROCEDURE gm_fc_fch_request (
		p_demandsheetids   IN		VARCHAR2
	  , p_outdetails	   OUT		TYPES.cursor_type
	)
	AS
		v_ttp_detail_id t4052_ttp_detail.c4052_ttp_detail_id%TYPE;
	BEGIN
		-- The input String in the form of 5,8 will be set into this temp view.
		my_context.set_my_inlist_ctx (p_demandsheetids);

		OPEN p_outdetails
		 FOR
			 SELECT t4020.c4020_demand_nm ds_name, t4020.c4020_request_period req_period, c4030_ref_id ID
				  , DECODE (t4030.c901_ref_type
						  , 20296, get_set_name (c4030_ref_id)
						  , get_partnum_desc (c4030_ref_id)
						   ) NAME
				  , TO_CHAR (LAST_DAY (c4043_start_date), 'Mon-YY') required_date, t4043.c4043_value required_qty
			   FROM t4020_demand_master t4020
				  , t4040_demand_sheet t4040
				  , t4043_demand_sheet_growth t4043
				  , t4030_demand_growth_mapping t4030
			  WHERE t4020.c4020_demand_master_id = t4040.c4020_demand_master_id
				AND t4040.c4040_demand_sheet_id IN (SELECT *
													  FROM v_in_list)
				AND t4040.c4040_demand_sheet_id = t4043.c4040_demand_sheet_id
				AND t4030.c4030_demand_growth_id = t4043.c4030_demand_growth_id
				AND c4043_start_date BETWEEN t4040.c4040_demand_period_dt
										 AND ADD_MONTHS (t4040.c4040_demand_period_dt
													   , (t4040.c4040_request_period - 1))
				AND t4043.c4043_value >0
				AND t4040.c901_demand_type IN ('40021', '40022');
	END gm_fc_fch_request;

--
  /*******************************************************
   * Description : Procedure to fetch request for TPR from
   *			   locked data
   * Author 	 : VPrasath
   *******************************************************/
--
	PROCEDURE gm_fc_fch_tprdetails (
		p_pnum				 IN 	  t205_part_number.c205_part_number_id%TYPE
	  , p_inv_lock_id		 IN 	  t250_inventory_lock.c250_inventory_lock_id%TYPE
	  , p_status			 IN 	  t253c_request_lock.c520_status_fl%TYPE
	  , p_demandsheetid 	 IN 	  t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_demandmasterid	 IN 	  t4040_demand_sheet.c4020_demand_master_id%TYPE
	  , p_setid 			 IN 	  t253c_request_lock.c207_set_id%TYPE
	  , p_type 			     IN 	  t901_code_lookup.c901_code_id%TYPE
	  , p_outdetails		 OUT	  TYPES.cursor_type
	  , p_out_void_details	 OUT	  TYPES.cursor_type
	  , p_desc				 OUT	  VARCHAR2
	  , p_salesbo_details	 OUT	  TYPES.cursor_type
	  , p_loanerbo_details   OUT	  TYPES.cursor_type
	)
--
	AS
		v_inv_lock_id  t250_inventory_lock.c250_inventory_lock_id%TYPE;
		v_type   	   t901_code_lookup.c901_code_id%TYPE;
	BEGIN
		IF p_demandsheetid IS NOT NULL
		THEN
			SELECT c250_inventory_lock_id
			  INTO v_inv_lock_id
			  FROM t4040_demand_sheet
			 WHERE c4040_demand_sheet_id = p_demandsheetid;
		ELSE
			v_inv_lock_id := p_inv_lock_id;
		END IF;
		
		IF p_type IS NULL
		THEN
			v_type := '4000106'; --TPR
		ELSE
			v_type := p_type;
		END IF;
		
		SELECT DECODE(NVL(p_pnum,''),'',GET_SET_NAME(p_setid),get_partnum_desc (p_pnum))
		  INTO p_desc
		  FROM DUAL;
		
		DELETE FROM my_temp_key_value;

        -- Below SQL to fetch only OUS Shipment
        -- 100823 Represemt US Sheet  
        INSERT INTO my_temp_key_value
                    (my_temp_txn_id, my_temp_txn_key)
			SELECT DISTINCT v700.d_id, 'DISTRIBUTOR'
			  FROM v700_territory_mapping_detail v700
		     WHERE v700.divid = 100824
             UNION ALL
            SELECT t4020.c4020_demand_master_id, 'SHEET'
              FROM t4015_global_sheet_mapping t4015, t4020_demand_master t4020
             WHERE t4020.c901_demand_type IN (40021, 40022)
               AND t4020.c4015_global_sheet_map_id = t4015.c4015_global_sheet_map_id
               AND (t4015.c901_level_id, t4015.c901_level_value) IN (
                              SELECT t4015.c901_level_id, t4015.c901_level_value
                                FROM t4015_global_sheet_mapping t4015
                               WHERE t4015.c901_level_value != 100823
                                 AND t4015.c901_access_type = 102623);    
		
		IF v_type = '4000106' THEN -- TPR
		
			gm_fch_req_details(p_pnum, v_inv_lock_id, p_status, p_demandmasterid
							 , p_setid, p_outdetails, p_out_void_details
							 , p_salesbo_details);
							 
		ELSIF v_type = '4000107' THEN -- TPR US
		 
		 	gm_fch_us_req_details(p_pnum, v_inv_lock_id, p_status, p_demandmasterid
							 	, p_setid, p_outdetails, p_out_void_details
							 	, p_salesbo_details);
							 	
    	ELSIF v_type = '4000108' THEN -- TPR OUS
    	     
		    gm_fch_ous_req_details(p_pnum, v_inv_lock_id, p_status, p_demandmasterid
							 	, p_setid, p_outdetails, p_out_void_details
							 	, p_salesbo_details);
							 	
		END IF;
			    
		OPEN p_loanerbo_details
		FOR
		SELECT t253g.c412_inhouse_trans_id inhouse_trans_id, t253g.c412_ref_id refid, t253h.c413_item_qty qty
			   FROM 
			   	  t253g_inhouse_transaction_lock t253g
				  , t253h_inhouse_trans_items_lock t253h
			  WHERE t253g.c250_inventory_lock_id = v_inv_lock_id
				AND t253g.c250_inventory_lock_id = t253h.c250_inventory_lock_id
				AND t253g.c412_inhouse_trans_id = t253h.c412_inhouse_trans_id
				AND t253g.c412_type IN(100062,1006572) --product loaner and in-house loaner 
				AND t253g.c412_status_fl='0' 
				AND t253g.c412_void_fl IS NULL
				AND t253h.c413_void_fl IS NULL
				-- Do not show this for TPR OUS ,as this is applicable for TPR and TPR US alone.
				AND 1 = DECODE(v_type,'4000108','-1',1) 
			    AND t253h.c205_part_number_id = p_pnum;
	END gm_fc_fch_tprdetails;

--
  /*******************************************************
   * Description : Procedure to fetch request for TPR from
   *			   locked data
   * Author 	 : VPrasath
   *******************************************************/
--
	PROCEDURE gm_fch_req_details (
		p_pnum				 IN 	  t205_part_number.c205_part_number_id%TYPE
	  , p_inv_lock_id		 IN 	  t250_inventory_lock.c250_inventory_lock_id%TYPE
	  , p_status			 IN 	  t253c_request_lock.c520_status_fl%TYPE
	  , p_demandmasterid	 IN 	  t4040_demand_sheet.c4020_demand_master_id%TYPE
	  , p_setid 			 IN 	  t253c_request_lock.c207_set_id%TYPE
	  ,	p_outdetails		 OUT	  TYPES.cursor_type
	  , p_out_void_details	 OUT	  TYPES.cursor_type	
	  , p_salesbo_details	 OUT	  TYPES.cursor_type	
	)
	AS
	BEGIN
		my_context.set_my_inlist_ctx (p_status);

		IF p_pnum IS NOT NULL 
		THEN
		    OPEN p_outdetails
			 FOR
			 SELECT   t253d.c520_request_id reqid, '' cons_id
					, TO_CHAR (t253c.c520_request_date, 'MM/DD/YYYY') request_date
					, TO_CHAR (t253c.c520_required_date, 'MM/DD/YYYY') required_date
					, gm_pkg_oppr_sheet_summary.get_request_status (t253c.c520_status_fl) sfl
					, t253d.c521_qty qty
					, get_user_name (t253c.c520_last_updated_by) updated_by
					, TO_CHAR (t253c.c520_last_updated_date, 'MM/DD/YYYY') updated_date
					, NVL(DECODE(gm_pkg_oppr_sheet.get_demandsheet_name(t253c.c520_request_txn_id),'0'
									, t253c.c520_master_request_id
									, gm_pkg_oppr_sheet.get_demandsheet_name (t253c.c520_request_txn_id))
									, '') sheet
					, get_code_name (t253c.c901_request_source) SOURCE
					, DECODE (t253c.c520_request_for
							, 40021, get_distributor_name (t253c.c520_request_to)
							, 40022, get_user_name (t253c.c520_ship_to_id)
							 ) req_to
				 FROM t253c_request_lock t253c, t253d_request_detail_lock t253d, t250_inventory_lock t250
				WHERE t250.c250_inventory_lock_id = p_inv_lock_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t250.c250_inventory_lock_id = t253d.c250_inventory_lock_id
				  AND (t253c.c520_required_date < t250.c250_lock_date
				  		OR
				  		t253c.c901_request_source='4000121'
				  		)
				  AND t253d.c205_part_number_id = p_pnum
				  AND t253c.c520_request_id = t253d.c520_request_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t253c.c901_request_source <> 50617
				  --50616 - Order Planning, 50618 - Customer Service, 4000121 - ETL OUS Sales Restock
				  --10, 'Back Order', 15, 'Back Log', 20, 'Ready to Consign'
				  AND (((t253c.c901_request_source = 50616 AND t253c.c520_status_fl IN (10,15,20,30)) 
				  OR (t253c.c901_request_source = 50618 AND t253c.c520_status_fl IN (10,15,20,30))
				  OR (t253c.c901_request_source = 4000121 AND t253c.c520_status_fl IN (10,15,20,30))
				  )OR (t253c.c901_request_source NOT IN (50616,50618,4000121)))
				  AND t253c.c520_void_fl IS NULL
				  AND DECODE(p_demandmasterid,'','1',NVL (t253c.c520_request_txn_id, '1')) IN (
				  SELECT c4020_demand_master_assoc_id FROM t4046_demand_sheet_assoc_map WHERE c4020_demand_master_assoc_id=p_demandmasterid
				  UNION
				  SELECT c4020_demand_master_assoc_id FROM t4046_demand_sheet_assoc_map WHERE c4020_demand_master_id=p_demandmasterid				  
				   UNION
				   SELECT c4020_demand_master_id FROM t4046_demand_sheet_assoc_map WHERE c4020_demand_master_id=p_demandmasterid
				  UNION
				  SELECT '1' FROM DUAL )
				  --As Status 20,30 is considered as PCS , we will be passing 20,30 from UI,so the status is converted to v-inlist to handle multple.
				  --More over when TPR link is clicked, the status will not be passed, so we are skipping the status fl check.
				  AND DECODE(p_status,NULL,1,t253c.c520_status_fl) IN (SELECT * FROM v_in_list UNION SELECT '1' FROM DUAL)
				  AND DECODE (p_setid, NULL, '1', NVL (t253c.c207_set_id, '-9999')) = NVL (p_setid, '1')
			UNION ALL
			--Code to get the Drilldown details for the Back Order from Shipped Sets of Source Set Building.
						SELECT   t253d.c520_request_id reqid, '' cons_id
					, TO_CHAR (t253c.c520_request_date, 'MM/DD/YYYY') request_date
					, TO_CHAR (t253c.c520_required_date, 'MM/DD/YYYY') required_date
					, gm_pkg_oppr_sheet_summary.get_request_status (t253c.c520_status_fl) sfl
					, t253d.c521_qty qty
					, get_user_name (t253c.c520_last_updated_by) updated_by
					, TO_CHAR (t253c.c520_last_updated_date, 'MM/DD/YYYY') updated_date
					, NVL(DECODE(gm_pkg_oppr_sheet.get_demandsheet_name(t253c.c520_request_txn_id),'0'
									, t253c.c520_master_request_id
									, gm_pkg_oppr_sheet.get_demandsheet_name (t253c.c520_request_txn_id))
									, '') sheet
					, get_code_name (t253c.c901_request_source) SOURCE
					, DECODE (t253c.c520_request_for
							, 40021, get_distributor_name (t253c.c520_request_to)
							, 40022, get_user_name (t253c.c520_ship_to_id)
							 ) req_to
				 FROM t253c_request_lock t253c, t253d_request_detail_lock t253d, t250_inventory_lock t250
				WHERE t250.c250_inventory_lock_id = p_inv_lock_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t250.c250_inventory_lock_id = t253d.c250_inventory_lock_id
				  AND t253c.c520_required_date < t250.c250_lock_date
				  AND t253d.c205_part_number_id = p_pnum
				  AND t253c.c520_request_id = t253d.c520_request_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t253c.c901_request_source = 50617
				  AND t253c.c520_void_fl IS NULL
				AND t253c.c520_status_fl      =10
				AND t253c.C520_REQUEST_TO     IS NOT NULL
				and t253c.C520_REQUEST_TO <> '01'
				AND t253c.c901_request_source = 50617  -- Set Building
				AND t253c.C207_SET_ID IS NULL  
				AND t253c.C520_MASTER_REQUEST_ID IS NOT NULL
				AND DECODE(p_status,NULL,1,t253c.c520_status_fl) IN (SELECT * FROM v_in_list UNION SELECT '1' FROM DUAL)
				AND DECODE (p_setid, NULL, '1', NVL (t253c.c207_set_id, '-9999')) = NVL (p_setid, '1')
			 UNION ALL
			 SELECT   t253a.c520_request_id req_id, t253a.c504_consignment_id cons_id
					, TO_CHAR (t253c.c520_request_date, 'MM/DD/YYYY') request_date
					, TO_CHAR (t253c.c520_required_date, 'MM/DD/YYYY') required_date
					, gm_pkg_oppr_sheet_summary.get_request_status (t253c.c520_status_fl) sfl, t253b.c505_item_qty qty
					, get_user_name (t253c.c520_last_updated_by) updated_by
					, TO_CHAR (t253c.c520_last_updated_date, 'MM/DD/YYYY') updated_date
					, NVL(DECODE(gm_pkg_oppr_sheet.get_demandsheet_name(t253c.c520_request_txn_id)
								 , '0',t253c.c520_master_request_id
								 , gm_pkg_oppr_sheet.get_demandsheet_name (t253c.c520_request_txn_id))
								 , '') sheet
					, get_code_name (t253c.c901_request_source) SOURCE
					, DECODE (t253c.c520_request_for
							, 40021, get_distributor_name (t253c.c520_request_to)
							, 40022, get_user_name (t253c.c520_ship_to_id)
							 ) req_to
				 FROM t253a_consignment_lock t253a
					, t253b_item_consignment_lock t253b
					, t253c_request_lock t253c
					, t250_inventory_lock t250
				WHERE t250.c250_inventory_lock_id = p_inv_lock_id
				  AND t250.c250_inventory_lock_id = t253a.c250_inventory_lock_id
				  AND t250.c250_inventory_lock_id = t253b.c250_inventory_lock_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND (t253c.c520_required_date < t250.c250_lock_date
				  		OR
				  		t253c.c901_request_source='4000121'
				  		)
				  AND t253b.c205_part_number_id = p_pnum
				  AND t253a.c520_request_id = t253c.c520_request_id
				  AND t253a.c504_consignment_id = t253b.c504_consignment_id
				  AND t253c.c901_request_source <> 50617 
				  --50616 - Order Planning, 50618 - Customer Service, 4000121 - ETL OUS Sales Restock
                  --10, 'Back Order', 15, 'Back Log', 20, 'Ready to Consign'
                  AND (((t253c.c901_request_source = 50616 AND t253c.c520_status_fl IN (10,15,20,30)) 
                  OR (t253c.c901_request_source = 50618 AND t253c.c520_status_fl IN (10,15,20,30))
                  OR (t253c.c901_request_source = 4000121 AND t253c.c520_status_fl IN (10,15,20,30))
                  )OR (t253c.c901_request_source NOT IN (50616,50618,4000121))) 
				  AND t253c.c520_void_fl IS NULL
				  AND t253a.c504_void_fl IS NULL
				  AND DECODE(p_demandmasterid,'','1',NVL (t253c.c520_request_txn_id, '1')) IN (
				   SELECT c4020_demand_master_assoc_id FROM t4046_demand_sheet_assoc_map WHERE c4020_demand_master_assoc_id=p_demandmasterid
				   UNION
				   SELECT c4020_demand_master_assoc_id FROM t4046_demand_sheet_assoc_map WHERE c4020_demand_master_id=p_demandmasterid
				   UNION
				   SELECT c4020_demand_master_id FROM t4046_demand_sheet_assoc_map WHERE c4020_demand_master_id=p_demandmasterid
				   UNION
				  SELECT '1' FROM DUAL  )
				  --As Status 20,30 is considered as PCS , we will be passing 20,30 from UI,so the status is converted to v-inlist to handle multple.
				  --More over when TPR link is clicked, the status will not be passed, so we are skipping the status fl check.
				  AND DECODE(p_status,NULL,1,t253c.c520_status_fl) IN (SELECT * FROM v_in_list UNION SELECT '1' FROM DUAL)
				  AND DECODE (p_setid, NULL, '1', NVL (t253a.c207_set_id, '-9999')) = NVL (p_setid, '1') 
		      ORDER BY reqid, cons_id, request_date, required_date, sfl;
		ELSIF (p_pnum IS NULL AND p_setid IS NOT NULL)
		THEN
			OPEN p_outdetails
			 FOR
			 SELECT   t250.c250_inventory_lock_id,t253c.c520_request_id reqid, '' cons_id
					, TO_CHAR (t253c.c520_request_date, 'MM/DD/YYYY') request_date
					, TO_CHAR (t253c.c520_required_date, 'MM/DD/YYYY') required_date
					, gm_pkg_oppr_sheet_summary.get_request_status (t253c.c520_status_fl) sfl
					, 1 qty
					, get_user_name (t253c.c520_last_updated_by) updated_by
					, TO_CHAR (t253c.c520_last_updated_date, 'MM/DD/YYYY') updated_date
					, NVL(DECODE(gm_pkg_oppr_sheet.get_demandsheet_name(t253c.c520_request_txn_id),'0'
									, t253c.c520_master_request_id
									, gm_pkg_oppr_sheet.get_demandsheet_name (t253c.c520_request_txn_id))
									, '') sheet
					, get_code_name (t253c.c901_request_source) SOURCE
					, DECODE (t253c.c520_request_for
							, 40021, get_distributor_name (t253c.c520_request_to)
							, 40022, get_user_name (t253c.c520_ship_to_id)
							 ) req_to
				 FROM t253c_request_lock t253c, t250_inventory_lock t250
				WHERE t250.c250_inventory_lock_id = p_inv_lock_id 
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t253c.c520_required_date < t250.c250_lock_date				  		
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t253c.c901_request_source <> 50617
                  AND t253c.c207_set_id= p_setid
				  AND t253c.c520_void_fl IS NULL
                  AND DECODE(p_demandmasterid,'','1',NVL (t253c.c520_request_txn_id, '1')) IN (
				  SELECT c4020_demand_master_assoc_id FROM t4046_demand_sheet_assoc_map WHERE c4020_demand_master_assoc_id=p_demandmasterid
				  UNION
				  SELECT c4020_demand_master_assoc_id FROM t4046_demand_sheet_assoc_map WHERE c4020_demand_master_id=p_demandmasterid
				  UNION
				  SELECT distinct c4020_demand_master_id FROM t4046_demand_sheet_assoc_map WHERE c4020_demand_master_id=p_demandmasterid
                  UNION
				  SELECT '1' FROM DUAL )
				  AND DECODE(p_status,'10',1,-1) = 1
				  AND DECODE(p_status,NULL,1,t253c.c520_status_fl) IN (SELECT * FROM v_in_list UNION SELECT '1' FROM DUAL)
                  UNION
				 select t250.c250_inventory_lock_id,t253c.c520_request_id reqid,t253.c504_consignment_id cons_id
					, TO_CHAR (t253c.c520_request_date, 'MM/DD/YYYY') request_date
					, TO_CHAR (t253c.c520_required_date, 'MM/DD/YYYY') required_date
					, gm_pkg_oppr_sheet_summary.get_request_status (t253c.c520_status_fl) sfl
					, 1 qty
					, get_user_name (t253c.c520_last_updated_by) updated_by
					, TO_CHAR (t253c.c520_last_updated_date, 'MM/DD/YYYY') updated_date
					, NVL(DECODE(gm_pkg_oppr_sheet.get_demandsheet_name(t253c.c520_request_txn_id),'0'
									, t253c.c520_master_request_id
									, gm_pkg_oppr_sheet.get_demandsheet_name (t253c.c520_request_txn_id))
									, '') sheet
					, get_code_name (t253c.c901_request_source) SOURCE
					, DECODE (t253c.c520_request_for
							, 40021, get_distributor_name (t253c.c520_request_to)
							, 40022, get_user_name (t253c.c520_ship_to_id)
							 ) req_to     
                   from t253a_consignment_lock t253,t253c_request_lock t253c , t250_inventory_lock t250
				WHERE t250.c250_inventory_lock_id = p_inv_lock_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t253c.c520_required_date < t250.c250_lock_date
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
                  AND t253.C520_REQUEST_ID = t253c.C520_REQUEST_ID
				  AND t253c.c901_request_source <> 50617
                  AND t253c.c207_set_id= p_setid
				  AND t253c.c520_void_fl IS NULL
                  AND DECODE(p_demandmasterid,'','1',NVL (t253c.c520_request_txn_id, '1')) IN (
				  SELECT c4020_demand_master_assoc_id FROM t4046_demand_sheet_assoc_map WHERE c4020_demand_master_assoc_id=p_demandmasterid
				  UNION
				  SELECT c4020_demand_master_assoc_id FROM t4046_demand_sheet_assoc_map WHERE c4020_demand_master_id=p_demandmasterid
				  UNION
				  SELECT distinct c4020_demand_master_id FROM t4046_demand_sheet_assoc_map WHERE c4020_demand_master_id=p_demandmasterid
                  UNION
				  SELECT '1' FROM DUAL )
				  AND DECODE(p_status,'10',-1,1) = 1
				  AND DECODE(p_status,NULL,1,t253c.c520_status_fl) IN (SELECT * FROM v_in_list UNION SELECT '1' FROM DUAL)
				   ORDER BY reqid, cons_id, request_date, required_date, sfl
				  ;
		END IF;     
		
		 OPEN p_out_void_details
		 FOR
			 SELECT   t253d.c520_request_id reqid, '' cons_id
					, TO_CHAR (t253c.c520_request_date, 'MM/DD/YYYY') request_date
					, TO_CHAR (t253c.c520_required_date, 'MM/DD/YYYY') required_date
					, gm_pkg_oppr_sheet_summary.get_request_status (t253c.c520_status_fl) sfl, t253d.c521_qty qty
					, get_user_name (t253c.c520_last_updated_by) updated_by
					, TO_CHAR (t253c.c520_last_updated_date, 'MM/DD/YYYY') updated_date
					, gm_pkg_oppr_sheet.get_demandsheet_name (t253c.c520_request_txn_id) sheet
					, get_code_name (t253c.c901_request_source) SOURCE
					, DECODE (t253c.c520_request_for
							, 40021, get_distributor_name (t253c.c520_request_to)
							, 40022, get_user_name (t253c.c520_ship_to_id)
							 ) req_to
				 FROM t253c_request_lock t253c
				 	, t253d_request_detail_lock t253d
				 	, t250_inventory_lock t250
				 WHERE t250.c250_inventory_lock_id = p_inv_lock_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t250.c250_inventory_lock_id = t253d.c250_inventory_lock_id
				  AND (t253c.c520_required_date < t250.c250_lock_date
				  	  OR
				  	   t253c.c901_request_source='4000121'
				  		)
				  AND t253d.c205_part_number_id = p_pnum
				  AND t253c.c520_request_id = t253d.c520_request_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t253c.c901_request_source <> 50617
				  AND t253c.c520_void_fl IS NOT NULL
				  AND NVL (t253c.c520_request_txn_id, '1') =
								NVL (p_demandmasterid, NVL (t253c.c520_request_txn_id, '1'))
				  AND t253c.c520_status_fl = NVL (p_status, t253c.c520_status_fl)
				  AND DECODE (p_setid, NULL, '1', NVL (t253c.c207_set_id, '-9999')) = NVL (p_setid, '1') 
			 UNION ALL
			 SELECT   t253a.c520_request_id req_id, t253a.c504_consignment_id cons_id
					, TO_CHAR (t253c.c520_request_date, 'MM/DD/YYYY') request_date
					, TO_CHAR (t253c.c520_required_date, 'MM/DD/YYYY') required_date
					, gm_pkg_oppr_sheet_summary.get_request_status (t253c.c520_status_fl) sfl, t253b.c505_item_qty qty
					, get_user_name (t253c.c520_last_updated_by) updated_by
					, TO_CHAR (t253c.c520_last_updated_date, 'MM/DD/YYYY') updated_date
					, gm_pkg_oppr_sheet.get_demandsheet_name (t253c.c520_request_txn_id) sheet
					, get_code_name (t253c.c901_request_source) SOURCE
					, DECODE (t253c.c520_request_for
							, 40021, get_distributor_name (t253c.c520_request_to)
							, 40022, get_user_name (t253c.c520_ship_to_id)
							 ) req_to
				 FROM t253a_consignment_lock t253a
					, t253b_item_consignment_lock t253b
					, t253c_request_lock t253c
					, t250_inventory_lock t250
				WHERE t250.c250_inventory_lock_id = p_inv_lock_id
				  AND t250.c250_inventory_lock_id = t253a.c250_inventory_lock_id
				  AND t250.c250_inventory_lock_id = t253b.c250_inventory_lock_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND (t253c.c520_required_date < t250.c250_lock_date
				  	   OR
				  		t253c.c901_request_source='4000121'
				  		)
				  AND t253b.c205_part_number_id = p_pnum
				  AND t253a.c520_request_id = t253c.c520_request_id
				  AND t253a.c504_consignment_id = t253b.c504_consignment_id
				  AND t253c.c901_request_source <> 50617
				  AND t253c.c520_void_fl IS NOT NULL
				  AND t253a.c504_void_fl IS NOT NULL
				  AND NVL (t253c.c520_request_txn_id, '1') =
															NVL (p_demandmasterid, NVL (t253c.c520_request_txn_id, '1'))
				  AND t253c.c520_status_fl = NVL (p_status, t253c.c520_status_fl)
				  AND DECODE (p_setid, NULL, '1', NVL (t253a.c207_set_id, '-9999')) = NVL (p_setid, '1') 
			 ORDER BY reqid, cons_id, request_date, required_date, sfl;
			 
		OPEN p_salesbo_details
		 FOR
			 SELECT t253e.c501_order_id ordid
			      , get_account_name (t253e.c704_account_id) accid
			      , t253f.c502_item_qty qty
			      , TO_CHAR(t253e.c501_order_date,'MM/DD/YYYY') request_date
				  , GET_REP_NAME(t253e.C703_SALES_REP_ID) requested_name
			   FROM t253e_order_lock t253e
				  , t253f_item_order_lock t253f
			  WHERE t253e.c250_inventory_lock_id = p_inv_lock_id
				AND t253e.c250_inventory_lock_id = t253f.c250_inventory_lock_id
				AND t253e.c501_order_id = t253f.c501_order_id
				AND ((NVL (t253e.c901_order_type, -999) =2525 AND t253e.c501_status_fl = 0) OR (NVL (t253e.c901_order_type, -999)=101260)) -- back order and ACK
				AND t253e.c501_void_fl IS NULL
			    AND t253f.c205_part_number_id = p_pnum 
			 UNION --Show Sales back order and Sales Replenishment Back Orders from OUS 
			 SELECT t253a.c520_request_id ordid
			      , DECODE (t253c.c901_purpose, 4000097, get_distributor_name (t253c.c520_request_to)--4000097-OUS Sales Replenishments
			        								   , GET_ACCOUNT_NAME (t253c.c520_request_to)) accid
			      , t253b.c505_item_qty qty
			      , TO_CHAR (t253c.c520_request_date,'MM/DD/YYYY') request_date
			      , DECODE (t253c.c901_request_by_type, 50626, GET_REP_NAME (t253c.c520_request_by)
			      									  , 50625, GET_USER_NAME (t253c.c520_request_by)
			      									  		 , GET_DISTRIBUTOR_NAME (t253c.c520_request_by)
			      									  		 ) requested_name
			   FROM t253a_consignment_lock t253a
			      , t253b_item_consignment_lock t253b
			      , t253c_request_lock t253c
			  	  , t250_inventory_lock t250
			  WHERE t250.c250_inventory_lock_id = p_inv_lock_id
			    AND t250.c250_inventory_lock_id = t253a.c250_inventory_lock_id
			    AND t250.c250_inventory_lock_id = t253b.c250_inventory_lock_id
			    AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
			    AND (t253c.c520_required_date < t250.c250_lock_date
			    	OR
				  	t253c.c901_request_source='4000121'
				  	)
			    AND t253b.c205_part_number_id = p_pnum
			    AND t253a.c520_request_id     = t253c.c520_request_id
			    AND t253a.c504_consignment_id = t253b.c504_consignment_id
			    AND t253c.c520_status_fl = 10 --back order
			    AND t253c.c901_request_source = 4000121 --4000121 - ETL OUS Sales Restock
			    AND t253c.c520_void_fl IS NULL
			    AND t253a.c504_void_fl IS NULL
			    AND NVL (t253c.c520_request_txn_id, '1') = NVL (p_demandmasterid, NVL (t253c.c520_request_txn_id, '1'))
			    AND t253c.c520_status_fl = NVL (p_status, t253c.c520_status_fl)
			    AND DECODE (p_setid, NULL, '1', NVL (t253a.c207_set_id, '-9999')) = NVL (p_setid, '1');

	END gm_fch_req_details;	

--
  /*******************************************************
   * Description : Procedure to fetch us requests for TPR 
   * 			   from locked data
   * Author 	 : VPrasath
   *******************************************************/
--
	PROCEDURE gm_fch_us_req_details (
	    p_pnum				 IN 	  t205_part_number.c205_part_number_id%TYPE
	  , p_inv_lock_id		 IN 	  t250_inventory_lock.c250_inventory_lock_id%TYPE
	  , p_status			 IN 	  t253c_request_lock.c520_status_fl%TYPE
	  , p_demandmasterid	 IN 	  t4040_demand_sheet.c4020_demand_master_id%TYPE
	  , p_setid 			 IN 	  t253c_request_lock.c207_set_id%TYPE
	  ,	p_outdetails		 OUT	  TYPES.cursor_type
	  , p_out_void_details	 OUT	  TYPES.cursor_type	
	  , p_salesbo_details	 OUT	  TYPES.cursor_type	
	)
	AS
	BEGIN
			 OPEN p_outdetails
		 	 FOR
			 SELECT   t253d.c520_request_id reqid, '' cons_id
					, TO_CHAR (t253c.c520_request_date, 'MM/DD/YYYY') request_date
					, TO_CHAR (t253c.c520_required_date, 'MM/DD/YYYY') required_date
					, gm_pkg_oppr_sheet_summary.get_request_status (t253c.c520_status_fl) sfl
					, t253d.c521_qty qty
					, get_user_name (t253c.c520_last_updated_by) updated_by
					, TO_CHAR (t253c.c520_last_updated_date, 'MM/DD/YYYY') updated_date
					, NVL(DECODE(gm_pkg_oppr_sheet.get_demandsheet_name(t253c.c520_request_txn_id),'0'
									, t253c.c520_master_request_id
									, gm_pkg_oppr_sheet.get_demandsheet_name (t253c.c520_request_txn_id))
									, '') sheet
					, get_code_name (t253c.c901_request_source) SOURCE
					, DECODE (t253c.c520_request_for
							, 40021, get_distributor_name (t253c.c520_request_to)
							, 40022, get_user_name (t253c.c520_ship_to_id)
							 ) req_to
				 FROM t253c_request_lock t253c, t253d_request_detail_lock t253d, t250_inventory_lock t250
				WHERE t250.c250_inventory_lock_id = p_inv_lock_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t250.c250_inventory_lock_id = t253d.c250_inventory_lock_id
				  AND (t253c.c520_required_date < t250.c250_lock_date
				  		OR
				  		t253c.c901_request_source='4000121'
				  		)
				  AND t253d.c205_part_number_id = p_pnum
				  AND t253c.c520_request_id = t253d.c520_request_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t253c.c901_request_source <> 50617
				  --50616 - Order Planning, 50618 - Customer Service, 4000121 - ETL OUS Sales Restock
                  --10, 'Back Order', 15, 'Back Log', 20, 'Ready to Consign'
                  AND (((t253c.c901_request_source = 50616 AND t253c.c520_status_fl IN (10,15,20,30)) 
                  OR (t253c.c901_request_source = 50618 AND t253c.c520_status_fl IN (10,15,20,30))
                  OR (t253c.c901_request_source = 4000121 AND t253c.c520_status_fl IN (15,20))
                  )OR (t253c.c901_request_source NOT IN (50616,50618,4000121)))
				  AND t253c.c520_void_fl IS NULL
				  AND NVL (t253c.c520_request_txn_id, '1') =
									NVL (p_demandmasterid, NVL (t253c.c520_request_txn_id, '1'))
				  AND t253c.c520_status_fl = NVL (p_status, t253c.c520_status_fl)
				  AND DECODE (p_setid, NULL, '1', NVL (t253c.c207_set_id, '-9999')) = NVL (p_setid, '1')
				  AND ( (  NVL(t253c.c520_request_to,'-9999') NOT IN (SELECT my_temp_txn_id
                                                      FROM my_temp_key_value
                                                     WHERE my_temp_txn_key = 'DISTRIBUTOR') )
                    AND (  NVL(t253c.c520_request_txn_id,'-9999')  NOT IN (SELECT my_temp_txn_id
                                                      FROM my_temp_key_value
                                                     WHERE my_temp_txn_key = 'SHEET') )
                      ) 
                      
               UNION ALL
			--Code to get the Drilldown details for the Back Order from Shipped Sets of Source Set Building.
						SELECT   t253d.c520_request_id reqid, '' cons_id
					, TO_CHAR (t253c.c520_request_date, 'MM/DD/YYYY') request_date
					, TO_CHAR (t253c.c520_required_date, 'MM/DD/YYYY') required_date
					, gm_pkg_oppr_sheet_summary.get_request_status (t253c.c520_status_fl) sfl
					, t253d.c521_qty qty
					, get_user_name (t253c.c520_last_updated_by) updated_by
					, TO_CHAR (t253c.c520_last_updated_date, 'MM/DD/YYYY') updated_date
					, NVL(DECODE(gm_pkg_oppr_sheet.get_demandsheet_name(t253c.c520_request_txn_id),'0'
									, t253c.c520_master_request_id
									, gm_pkg_oppr_sheet.get_demandsheet_name (t253c.c520_request_txn_id))
									, '') sheet
					, get_code_name (t253c.c901_request_source) SOURCE
					, DECODE (t253c.c520_request_for
							, 40021, get_distributor_name (t253c.c520_request_to)
							, 40022, get_user_name (t253c.c520_ship_to_id)
							 ) req_to
				 FROM t253c_request_lock t253c, t253d_request_detail_lock t253d, t250_inventory_lock t250
				WHERE t250.c250_inventory_lock_id = p_inv_lock_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t250.c250_inventory_lock_id = t253d.c250_inventory_lock_id
				  AND t253c.c520_required_date < t250.c250_lock_date
				  AND t253d.c205_part_number_id = p_pnum
				  AND t253c.c520_request_id = t253d.c520_request_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t253c.c901_request_source = 50617
				  AND t253c.c520_void_fl IS NULL
				AND t253c.c520_status_fl      =10
				AND t253c.C520_REQUEST_TO     IS NOT NULL
				and t253c.C520_REQUEST_TO <> '01'
				AND t253c.c901_request_source = 50617  -- Set Building
				AND t253c.C207_SET_ID IS NULL  
				AND t253c.C520_MASTER_REQUEST_ID IS NOT NULL
				AND DECODE(p_status,NULL,1,t253c.c520_status_fl) IN (SELECT * FROM v_in_list UNION SELECT '1' FROM DUAL)
				AND DECODE (p_setid, NULL, '1', NVL (t253c.c207_set_id, '-9999')) = NVL (p_setid, '1')
			 UNION ALL
			 SELECT   t253a.c520_request_id req_id, t253a.c504_consignment_id cons_id
					, TO_CHAR (t253c.c520_request_date, 'MM/DD/YYYY') request_date
					, TO_CHAR (t253c.c520_required_date, 'MM/DD/YYYY') required_date
					, gm_pkg_oppr_sheet_summary.get_request_status (t253c.c520_status_fl) sfl, t253b.c505_item_qty qty
					, get_user_name (t253c.c520_last_updated_by) updated_by
					, TO_CHAR (t253c.c520_last_updated_date, 'MM/DD/YYYY') updated_date
					, NVL(DECODE(gm_pkg_oppr_sheet.get_demandsheet_name(t253c.c520_request_txn_id)
								 , '0',t253c.c520_master_request_id
								 , gm_pkg_oppr_sheet.get_demandsheet_name (t253c.c520_request_txn_id))
								 , '') sheet
					, get_code_name (t253c.c901_request_source) SOURCE
					, DECODE (t253c.c520_request_for
							, 40021, get_distributor_name (t253c.c520_request_to)
							, 40022, get_user_name (t253c.c520_ship_to_id)
							 ) req_to
				 FROM t253a_consignment_lock t253a
					, t253b_item_consignment_lock t253b
					, t253c_request_lock t253c
					, t250_inventory_lock t250
				WHERE t250.c250_inventory_lock_id = p_inv_lock_id
				  AND t250.c250_inventory_lock_id = t253a.c250_inventory_lock_id
				  AND t250.c250_inventory_lock_id = t253b.c250_inventory_lock_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND (t253c.c520_required_date < t250.c250_lock_date
				  	  OR
				  		t253c.c901_request_source='4000121'
				  	  )
				  AND t253b.c205_part_number_id = p_pnum
				  AND t253a.c520_request_id = t253c.c520_request_id
				  AND t253a.c504_consignment_id = t253b.c504_consignment_id
				  AND t253c.c901_request_source <> 50617 
				  --50616 - Order Planning, 50618 - Customer Service, 4000121 - ETL OUS Sales Restock
                  --10, 'Back Order', 15, 'Back Log', 20, 'Ready to Consign'
                  AND (((t253c.c901_request_source = 50616 AND t253c.c520_status_fl IN (10,15,20,30)) 
                  OR (t253c.c901_request_source = 50618 AND t253c.c520_status_fl IN (10,15,20,30))
                  OR (t253c.c901_request_source = 4000121 AND t253c.c520_status_fl IN (15,20))
                  )OR (t253c.c901_request_source NOT IN (50616,50618,4000121))) 
				  AND t253c.c520_void_fl IS NULL
				  AND t253a.c504_void_fl IS NULL
				  AND NVL (t253c.c520_request_txn_id, '1') =
															NVL (p_demandmasterid, NVL (t253c.c520_request_txn_id, '1'))
				  AND t253c.c520_status_fl = NVL (p_status, t253c.c520_status_fl)
				  AND DECODE (p_setid, NULL, '1', NVL (t253a.c207_set_id, '-9999')) = NVL (p_setid, '1') 
				  AND ( ( NVL(t253c.c520_request_to,'-9999') NOT IN (SELECT my_temp_txn_id
                                                      FROM my_temp_key_value
                                                     WHERE my_temp_txn_key = 'DISTRIBUTOR') )
                    AND (NVL(t253c.c520_request_txn_id,'-9999')  NOT IN (SELECT my_temp_txn_id
                                                      FROM my_temp_key_value
                                                     WHERE my_temp_txn_key = 'SHEET') )
                      )                              
		      ORDER BY reqid, cons_id, request_date, required_date, sfl;
		      
		 OPEN p_out_void_details
		 FOR
			 SELECT   t253d.c520_request_id reqid, '' cons_id
					, TO_CHAR (t253c.c520_request_date, 'MM/DD/YYYY') request_date
					, TO_CHAR (t253c.c520_required_date, 'MM/DD/YYYY') required_date
					, gm_pkg_oppr_sheet_summary.get_request_status (t253c.c520_status_fl) sfl, t253d.c521_qty qty
					, get_user_name (t253c.c520_last_updated_by) updated_by
					, TO_CHAR (t253c.c520_last_updated_date, 'MM/DD/YYYY') updated_date
					, gm_pkg_oppr_sheet.get_demandsheet_name (t253c.c520_request_txn_id) sheet
					, get_code_name (t253c.c901_request_source) SOURCE
					, DECODE (t253c.c520_request_for
							, 40021, get_distributor_name (t253c.c520_request_to)
							, 40022, get_user_name (t253c.c520_ship_to_id)
							 ) req_to
				 FROM t253c_request_lock t253c
				 	, t253d_request_detail_lock t253d
				 	, t250_inventory_lock t250
				 WHERE t250.c250_inventory_lock_id = p_inv_lock_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t250.c250_inventory_lock_id = t253d.c250_inventory_lock_id
				  AND (t253c.c520_required_date < t250.c250_lock_date
				  	   OR
				  	    t253c.c901_request_source='4000121'
				  		)
				  AND t253d.c205_part_number_id = p_pnum
				  AND t253c.c520_request_id = t253d.c520_request_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t253c.c901_request_source <> 50617
				  AND t253c.c520_void_fl IS NOT NULL
				  AND NVL (t253c.c520_request_txn_id, '1') =
								NVL (p_demandmasterid, NVL (t253c.c520_request_txn_id, '1'))
				  AND t253c.c520_status_fl = NVL (p_status, t253c.c520_status_fl)
				  AND DECODE (p_setid, NULL, '1', NVL (t253c.c207_set_id, '-9999')) = NVL (p_setid, '1') 
			 UNION ALL
			 SELECT   t253a.c520_request_id req_id, t253a.c504_consignment_id cons_id
					, TO_CHAR (t253c.c520_request_date, 'MM/DD/YYYY') request_date
					, TO_CHAR (t253c.c520_required_date, 'MM/DD/YYYY') required_date
					, gm_pkg_oppr_sheet_summary.get_request_status (t253c.c520_status_fl) sfl, t253b.c505_item_qty qty
					, get_user_name (t253c.c520_last_updated_by) updated_by
					, TO_CHAR (t253c.c520_last_updated_date, 'MM/DD/YYYY') updated_date
					, gm_pkg_oppr_sheet.get_demandsheet_name (t253c.c520_request_txn_id) sheet
					, get_code_name (t253c.c901_request_source) SOURCE
					, DECODE (t253c.c520_request_for
							, 40021, get_distributor_name (t253c.c520_request_to)
							, 40022, get_user_name (t253c.c520_ship_to_id)
							 ) req_to
				 FROM t253a_consignment_lock t253a
					, t253b_item_consignment_lock t253b
					, t253c_request_lock t253c
					, t250_inventory_lock t250
				WHERE t250.c250_inventory_lock_id = p_inv_lock_id
				  AND t250.c250_inventory_lock_id = t253a.c250_inventory_lock_id
				  AND t250.c250_inventory_lock_id = t253b.c250_inventory_lock_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND (t253c.c520_required_date < t250.c250_lock_date
				  	  OR
				  		t253c.c901_request_source='4000121'
				  		)
				  AND t253b.c205_part_number_id = p_pnum
				  AND t253a.c520_request_id = t253c.c520_request_id
				  AND t253a.c504_consignment_id = t253b.c504_consignment_id
				  AND t253c.c901_request_source <> 50617
				  AND t253c.c520_void_fl IS NOT NULL
				  AND t253a.c504_void_fl IS NOT NULL
				  AND NVL (t253c.c520_request_txn_id, '1') =
															NVL (p_demandmasterid, NVL (t253c.c520_request_txn_id, '1'))
				  AND t253c.c520_status_fl = NVL (p_status, t253c.c520_status_fl)
				  AND DECODE (p_setid, NULL, '1', NVL (t253a.c207_set_id, '-9999')) = NVL (p_setid, '1') 
				  AND ( ( t253c.c520_request_for = 40021
                         AND t253c.c520_request_to NOT IN (SELECT my_temp_txn_id
                                                      FROM my_temp_key_value
                                                     WHERE my_temp_txn_key = 'DISTRIBUTOR') )
                    OR ( t253c.c901_request_source    = '50616'                             
                        AND t253c.c520_request_txn_id  NOT IN (SELECT my_temp_txn_id
                                                      FROM my_temp_key_value
                                                     WHERE my_temp_txn_key = 'SHEET') )
                      )                              
			 ORDER BY reqid, cons_id, request_date, required_date, sfl;
			 
		OPEN p_salesbo_details
		 FOR
			 SELECT t253e.c501_order_id ordid
			      , get_account_name (t253e.c704_account_id) accid
			      , t253f.c502_item_qty qty
			      , TO_CHAR(t253e.c501_order_date,'MM/DD/YYYY') request_date
				  , get_rep_name(t253e.C703_SALES_REP_ID) requested_name
			   FROM t253e_order_lock t253e
				  , t253f_item_order_lock t253f
			  WHERE t253e.c250_inventory_lock_id = p_inv_lock_id
				AND t253e.c250_inventory_lock_id = t253f.c250_inventory_lock_id
				AND t253e.c501_order_id = t253f.c501_order_id
				AND ((NVL (t253e.c901_order_type, -999) =2525 AND t253e.c501_status_fl = 0) OR (NVL (t253e.c901_order_type, -999)=101260))  -- back order and ACK
				AND t253e.c501_void_fl IS NULL
			    AND t253f.c205_part_number_id = p_pnum 
			 UNION --Show Sales back order and Sales Replenishment Back Orders from OUS 
			 SELECT t253a.c520_request_id ordid
			      , DECODE (t253c.c901_purpose, 4000097, get_distributor_name (t253c.c520_request_to)--4000097-OUS Sales Replenishments
			        								   , GET_ACCOUNT_NAME (t253c.c520_request_to)) accid
			      , t253b.c505_item_qty qty
			      , TO_CHAR (t253c.c520_request_date,'MM/DD/YYYY') request_date
			      , DECODE (t253c.c901_request_by_type, 50626, GET_REP_NAME (t253c.c520_request_by)
			      									  , 50625, GET_USER_NAME (t253c.c520_request_by)
			      									  		 , GET_DISTRIBUTOR_NAME (t253c.c520_request_by)
			      									  		 ) requested_name
			   FROM t253a_consignment_lock t253a
			      , t253b_item_consignment_lock t253b
			      , t253c_request_lock t253c
			  	  , t250_inventory_lock t250
			  WHERE t250.c250_inventory_lock_id = p_inv_lock_id
			    AND t250.c250_inventory_lock_id = t253a.c250_inventory_lock_id
			    AND t250.c250_inventory_lock_id = t253b.c250_inventory_lock_id
			    AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
			    AND (t253c.c520_required_date < t250.c250_lock_date
			    	OR
				  	t253c.c901_request_source='4000121'
				  	)
			    AND t253b.c205_part_number_id = p_pnum
			    AND t253a.c520_request_id     = t253c.c520_request_id
			    AND t253a.c504_consignment_id = t253b.c504_consignment_id
			    AND t253c.c520_status_fl = 10 --back order
			    AND t253c.c901_request_source = 4000121 --4000121 - ETL OUS Sales Restock
			    AND t253c.c520_void_fl IS NULL
			    AND t253a.c504_void_fl IS NULL
			    AND NVL (t253c.c520_request_txn_id, '1') = NVL (p_demandmasterid, NVL (t253c.c520_request_txn_id, '1'))
			    AND t253c.c520_status_fl = NVL (p_status, t253c.c520_status_fl)
			    AND DECODE (p_setid, NULL, '1', NVL (t253a.c207_set_id, '-9999')) = NVL (p_setid, '1')
		      	AND ( ( t253c.c520_request_for = 40021
                         AND t253c.c520_request_to NOT IN (SELECT my_temp_txn_id
                                                      FROM my_temp_key_value
                                                     WHERE my_temp_txn_key = 'DISTRIBUTOR') )
                    OR ( t253c.c901_request_source    = '50616'                             
                        AND t253c.c520_request_txn_id  NOT IN (SELECT my_temp_txn_id
                                                      FROM my_temp_key_value
                                                     WHERE my_temp_txn_key = 'SHEET') )
                      );
	END gm_fch_us_req_details;	
--
  /*******************************************************
   * Description : Procedure to fetch ous requests for TPR 
   * 			   from locked data
   * Author 	 : VPrasath
   *******************************************************/
--
	PROCEDURE gm_fch_ous_req_details (
	  	p_pnum				 IN 	  t205_part_number.c205_part_number_id%TYPE
	  , p_inv_lock_id		 IN 	  t250_inventory_lock.c250_inventory_lock_id%TYPE
	  , p_status			 IN 	  t253c_request_lock.c520_status_fl%TYPE
	  , p_demandmasterid	 IN 	  t4040_demand_sheet.c4020_demand_master_id%TYPE
	  , p_setid 			 IN 	  t253c_request_lock.c207_set_id%TYPE
	  ,	p_outdetails		 OUT	  TYPES.cursor_type
	  , p_out_void_details	 OUT	  TYPES.cursor_type	
	  , p_salesbo_details	 OUT	  TYPES.cursor_type	
	)
	AS
	BEGIN
			 OPEN p_outdetails
		 	 FOR
	    	   SELECT   t253d.c520_request_id reqid, '' cons_id
					, TO_CHAR (t253c.c520_request_date, 'MM/DD/YYYY') request_date
					, TO_CHAR (t253c.c520_required_date, 'MM/DD/YYYY') required_date
					, gm_pkg_oppr_sheet_summary.get_request_status (t253c.c520_status_fl) sfl
					, t253d.c521_qty qty
					, get_user_name (t253c.c520_last_updated_by) updated_by
					, TO_CHAR (t253c.c520_last_updated_date, 'MM/DD/YYYY') updated_date
					, NVL(DECODE(gm_pkg_oppr_sheet.get_demandsheet_name(t253c.c520_request_txn_id),'0'
									, t253c.c520_master_request_id
									, gm_pkg_oppr_sheet.get_demandsheet_name (t253c.c520_request_txn_id))
									, '') sheet
					, get_code_name (t253c.c901_request_source) SOURCE
					, DECODE (t253c.c520_request_for
							, 40021, get_distributor_name (t253c.c520_request_to)
							, 40022, get_user_name (t253c.c520_ship_to_id)
							 ) req_to
				 FROM t253c_request_lock t253c, t253d_request_detail_lock t253d, t250_inventory_lock t250
				WHERE t250.c250_inventory_lock_id = p_inv_lock_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t250.c250_inventory_lock_id = t253d.c250_inventory_lock_id
				  AND t253c.c520_required_date < t250.c250_lock_date				  	  	
				  AND t253d.c205_part_number_id = p_pnum
				  AND t253c.c520_request_id = t253d.c520_request_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t253c.c901_request_source <> 50617
				  --50616 - Order Planning, 50618 - Customer Service, 4000121 - ETL OUS Sales Restock
                  --10, 'Back Order', 15, 'Back Log', 20, 'Ready to Consign'
                  AND (((t253c.c901_request_source = 50616 AND t253c.c520_status_fl IN (10,15,20,30)) 
                  OR (t253c.c901_request_source = 50618 AND t253c.c520_status_fl IN (10,15,20,30))
                  OR (t253c.c901_request_source = 4000121 AND t253c.c520_status_fl IN (15,20))
                  )OR (t253c.c901_request_source NOT IN (50616,50618,4000121)))
				  AND t253c.c520_void_fl IS NULL
				  AND NVL (t253c.c520_request_txn_id, '1') =
									NVL (p_demandmasterid, NVL (t253c.c520_request_txn_id, '1'))
				  AND t253c.c520_status_fl = NVL (p_status, t253c.c520_status_fl)
				  AND DECODE (p_setid, NULL, '1', NVL (t253c.c207_set_id, '-9999')) = NVL (p_setid, '1')
				  AND ( ( t253c.c520_request_for = 40021
                         AND t253c.c520_request_to IN (SELECT my_temp_txn_id
                                                      FROM my_temp_key_value
                                                     WHERE my_temp_txn_key = 'DISTRIBUTOR') )
                    OR ( t253c.c901_request_source    = '50616'                             
                        AND t253c.c520_request_txn_id IN (SELECT my_temp_txn_id
                                                      FROM my_temp_key_value
                                                     WHERE my_temp_txn_key = 'SHEET') )
                      )                              
			 UNION ALL
			 SELECT   t253a.c520_request_id req_id, t253a.c504_consignment_id cons_id
					, TO_CHAR (t253c.c520_request_date, 'MM/DD/YYYY') request_date
					, TO_CHAR (t253c.c520_required_date, 'MM/DD/YYYY') required_date
					, gm_pkg_oppr_sheet_summary.get_request_status (t253c.c520_status_fl) sfl, t253b.c505_item_qty qty
					, get_user_name (t253c.c520_last_updated_by) updated_by
					, TO_CHAR (t253c.c520_last_updated_date, 'MM/DD/YYYY') updated_date
					, NVL(DECODE(gm_pkg_oppr_sheet.get_demandsheet_name(t253c.c520_request_txn_id)
								 , '0',t253c.c520_master_request_id
								 , gm_pkg_oppr_sheet.get_demandsheet_name (t253c.c520_request_txn_id))
								 , '') sheet
					, get_code_name (t253c.c901_request_source) SOURCE
					, DECODE (t253c.c520_request_for
							, 40021, get_distributor_name (t253c.c520_request_to)
							, 40022, get_user_name (t253c.c520_ship_to_id)
							 ) req_to
				 FROM t253a_consignment_lock t253a
					, t253b_item_consignment_lock t253b
					, t253c_request_lock t253c
					, t250_inventory_lock t250
				WHERE t250.c250_inventory_lock_id = p_inv_lock_id
				  AND t250.c250_inventory_lock_id = t253a.c250_inventory_lock_id
				  AND t250.c250_inventory_lock_id = t253b.c250_inventory_lock_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t253c.c520_required_date < t250.c250_lock_date				  		
				  AND t253b.c205_part_number_id = p_pnum
				  AND t253a.c520_request_id = t253c.c520_request_id
				  AND t253a.c504_consignment_id = t253b.c504_consignment_id
				  AND t253c.c901_request_source <> 50617 
				  --50616 - Order Planning, 50618 - Customer Service, 4000121 - ETL OUS Sales Restock
                  --10, 'Back Order', 15, 'Back Log', 20, 'Ready to Consign'
                  AND (((t253c.c901_request_source = 50616 AND t253c.c520_status_fl IN (10,15,20,30)) 
                  OR (t253c.c901_request_source = 50618 AND t253c.c520_status_fl IN (10,15,20,30))
                  OR (t253c.c901_request_source = 4000121 AND t253c.c520_status_fl IN (15,20))
                  )OR (t253c.c901_request_source NOT IN (50616,50618,4000121))) 
				  AND t253c.c520_void_fl IS NULL
				  AND t253a.c504_void_fl IS NULL
				  AND NVL (t253c.c520_request_txn_id, '1') =
										NVL (p_demandmasterid, NVL (t253c.c520_request_txn_id, '1'))
				  AND t253c.c520_status_fl = NVL (p_status, t253c.c520_status_fl)
				  AND DECODE (p_setid, NULL, '1', NVL (t253a.c207_set_id, '-9999')) = NVL (p_setid, '1') 
				  AND (( t253c.c520_request_for = 40021
                         AND t253c.c520_request_to IN (SELECT my_temp_txn_id
                                                      FROM my_temp_key_value
                                                     WHERE my_temp_txn_key = 'DISTRIBUTOR') )
                    OR ( t253c.c901_request_source    = '50616'                             
                        AND t253c.c520_request_txn_id IN (SELECT my_temp_txn_id
                                                      FROM my_temp_key_value
                                                     WHERE my_temp_txn_key = 'SHEET') )
                      )                              
		      ORDER BY reqid, cons_id, request_date, required_date, sfl;
		      
		 OPEN p_out_void_details
		 FOR
			 SELECT   t253d.c520_request_id reqid, '' cons_id
					, TO_CHAR (t253c.c520_request_date, 'MM/DD/YYYY') request_date
					, TO_CHAR (t253c.c520_required_date, 'MM/DD/YYYY') required_date
					, gm_pkg_oppr_sheet_summary.get_request_status (t253c.c520_status_fl) sfl, t253d.c521_qty qty
					, get_user_name (t253c.c520_last_updated_by) updated_by
					, TO_CHAR (t253c.c520_last_updated_date, 'MM/DD/YYYY') updated_date
					, gm_pkg_oppr_sheet.get_demandsheet_name (t253c.c520_request_txn_id) sheet
					, get_code_name (t253c.c901_request_source) SOURCE
					, DECODE (t253c.c520_request_for
							, 40021, get_distributor_name (t253c.c520_request_to)
							, 40022, get_user_name (t253c.c520_ship_to_id)
							 ) req_to
				 FROM t253c_request_lock t253c
				 	, t253d_request_detail_lock t253d
				 	, t250_inventory_lock t250
				 WHERE t250.c250_inventory_lock_id = p_inv_lock_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t250.c250_inventory_lock_id = t253d.c250_inventory_lock_id
				  AND t253c.c520_required_date < t250.c250_lock_date
				  AND t253d.c205_part_number_id = p_pnum
				  AND t253c.c520_request_id = t253d.c520_request_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t253c.c901_request_source <> 50617
				  AND t253c.c520_void_fl IS NOT NULL
				  AND NVL (t253c.c520_request_txn_id, '1') =
								NVL (p_demandmasterid, NVL (t253c.c520_request_txn_id, '1'))
				  AND t253c.c520_status_fl = NVL (p_status, t253c.c520_status_fl)
				  AND DECODE (p_setid, NULL, '1', NVL (t253c.c207_set_id, '-9999')) = NVL (p_setid, '1') 
			 UNION ALL
			 SELECT   t253a.c520_request_id req_id, t253a.c504_consignment_id cons_id
					, TO_CHAR (t253c.c520_request_date, 'MM/DD/YYYY') request_date
					, TO_CHAR (t253c.c520_required_date, 'MM/DD/YYYY') required_date
					, gm_pkg_oppr_sheet_summary.get_request_status (t253c.c520_status_fl) sfl, t253b.c505_item_qty qty
					, get_user_name (t253c.c520_last_updated_by) updated_by
					, TO_CHAR (t253c.c520_last_updated_date, 'MM/DD/YYYY') updated_date
					, gm_pkg_oppr_sheet.get_demandsheet_name (t253c.c520_request_txn_id) sheet
					, get_code_name (t253c.c901_request_source) SOURCE
					, DECODE (t253c.c520_request_for
							, 40021, get_distributor_name (t253c.c520_request_to)
							, 40022, get_user_name (t253c.c520_ship_to_id)
							 ) req_to
				 FROM t253a_consignment_lock t253a
					, t253b_item_consignment_lock t253b
					, t253c_request_lock t253c
					, t250_inventory_lock t250
				WHERE t250.c250_inventory_lock_id = p_inv_lock_id
				  AND t250.c250_inventory_lock_id = t253a.c250_inventory_lock_id
				  AND t250.c250_inventory_lock_id = t253b.c250_inventory_lock_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t253c.c520_required_date < t250.c250_lock_date
				  AND t253b.c205_part_number_id = p_pnum
				  AND t253a.c520_request_id = t253c.c520_request_id
				  AND t253a.c504_consignment_id = t253b.c504_consignment_id
				  AND t253c.c901_request_source <> 50617
				  AND t253c.c520_void_fl IS NOT NULL
				  AND t253a.c504_void_fl IS NOT NULL
				  AND NVL (t253c.c520_request_txn_id, '1') =
															NVL (p_demandmasterid, NVL (t253c.c520_request_txn_id, '1'))
				  AND t253c.c520_status_fl = NVL (p_status, t253c.c520_status_fl)
				  AND DECODE (p_setid, NULL, '1', NVL (t253a.c207_set_id, '-9999')) = NVL (p_setid, '1') 
				  AND (( t253c.c520_request_for = 40021
                         AND t253c.c520_request_to IN (SELECT my_temp_txn_id
                                                      FROM my_temp_key_value
                                                     WHERE my_temp_txn_key = 'DISTRIBUTOR') )
                    OR ( t253c.c901_request_source    = '50616'                             
                        AND t253c.c520_request_txn_id IN (SELECT my_temp_txn_id
                                                      FROM my_temp_key_value
                                                     WHERE my_temp_txn_key = 'SHEET') )
                      )                              
			 ORDER BY reqid, cons_id, request_date, required_date, sfl;
			 
		 OPEN p_salesbo_details
		 FOR
			 SELECT t253e.c501_order_id ordid
			      , get_account_name (t253e.c704_account_id) accid
			      , t253f.c502_item_qty qty
			      , TO_CHAR(t253e.c501_order_date,'MM/DD/YYYY') request_date
				  , GET_REP_NAME(t253e.C703_SALES_REP_ID) requested_name
			   FROM t253e_order_lock t253e
				  , t253f_item_order_lock t253f
			  WHERE t253e.c250_inventory_lock_id = p_inv_lock_id
				AND t253e.c250_inventory_lock_id = t253f.c250_inventory_lock_id
				AND t253e.c501_order_id = t253f.c501_order_id
				AND ((NVL (t253e.c901_order_type, -999) =2525 AND t253e.c501_status_fl = 0) OR (NVL (t253e.c901_order_type, -999)=101260)) -- back order and ACK
				AND t253e.c501_void_fl IS NULL
			    AND t253f.c205_part_number_id = p_pnum 
			 UNION --Show Sales back order and Sales Replenishment Back Orders from OUS 
			 SELECT t253a.c520_request_id ordid
			      , DECODE (t253c.c901_purpose, 4000097, get_distributor_name (t253c.c520_request_to)--4000097-OUS Sales Replenishments
			        								   , GET_ACCOUNT_NAME (t253c.c520_request_to)) accid
			      , t253b.c505_item_qty qty
			      , TO_CHAR (t253c.c520_request_date,'MM/DD/YYYY') request_date
			      , DECODE (t253c.c901_request_by_type, 50626, GET_REP_NAME (t253c.c520_request_by)
			      									  , 50625, GET_USER_NAME (t253c.c520_request_by)
			      									  		 , GET_DISTRIBUTOR_NAME (t253c.c520_request_by)
			      									  		 ) requested_name
			   FROM t253a_consignment_lock t253a
			      , t253b_item_consignment_lock t253b
			      , t253c_request_lock t253c
			  	  , t250_inventory_lock t250
			  WHERE t250.c250_inventory_lock_id = p_inv_lock_id
			    AND t250.c250_inventory_lock_id = t253a.c250_inventory_lock_id
			    AND t250.c250_inventory_lock_id = t253b.c250_inventory_lock_id
			    AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
			    AND t253c.c520_required_date < t250.c250_lock_date
			    AND t253b.c205_part_number_id = p_pnum
			    AND t253a.c520_request_id     = t253c.c520_request_id
			    AND t253a.c504_consignment_id = t253b.c504_consignment_id
			    AND t253c.c520_status_fl = 10 --back order
			    AND t253c.c901_request_source = 4000121 --4000121 - ETL OUS Sales Restock
			    AND t253c.c520_void_fl IS NULL
			    AND t253a.c504_void_fl IS NULL
			    AND NVL (t253c.c520_request_txn_id, '1') = NVL (p_demandmasterid, NVL (t253c.c520_request_txn_id, '1'))
			    AND t253c.c520_status_fl = NVL (p_status, t253c.c520_status_fl)
			    AND DECODE (p_setid, NULL, '1', NVL (t253a.c207_set_id, '-9999')) = NVL (p_setid, '1')
			    AND (( t253c.c520_request_for = 40021
                         AND t253c.c520_request_to IN (SELECT my_temp_txn_id
                                                      FROM my_temp_key_value
                                                     WHERE my_temp_txn_key = 'DISTRIBUTOR') )
                    OR ( t253c.c901_request_source    = '50616'                             
                        AND t253c.c520_request_txn_id IN (SELECT my_temp_txn_id
                                                      FROM my_temp_key_value
                                                     WHERE my_temp_txn_key = 'SHEET') )
                      );  
END gm_fch_ous_req_details;
	
	 /*****************************************************
	  *Description : Procedure to fetch Parent Part Qty Details
	  *
	  * Author		: VPrasath
	************************************************************/
	PROCEDURE gm_fc_fch_ppqdetails (
		p_pnum					 IN 	  t205_part_number.c205_part_number_id%TYPE
	  , p_inv_lock_id			 IN 	  t251_inventory_lock_details.c250_inventory_lock_id%TYPE
	  , p_desc					 OUT	  VARCHAR2
	  , p_parentpartqtydetails	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_parentpartqtydetails
		 FOR
		  --PMT-30031 GOP_To show Inventory details in TTP summary
			SELECT pnum,
			       des,
                   SUM (instock) instock,
                   SUM (opendhr) opendhr,
                   MAX (sub_part_qty) sub_part_qty ,
                   SUM (total_instock) total_instock,
                   SUM (total_opendhr) total_opendhr ,
                   (NVL (SUM (total_instock), 0) + NVL (SUM (total_opendhr), 0)) total
           FROM
               (SELECT T251.C205_Part_Number_Id Pnum,
                Get_Partnum_Desc (T251.C205_Part_Number_Id) Des ,
                T251.C251_In_Stock Instock ,
                T251.C251_Open_Dhr Opendhr,
                T205a.Sub_Part_Qty ,
                T251.C251_In_Stock * T205a.Sub_Part_Qty Total_Instock ,
                T251.C251_Open_Dhr * T205a.Sub_Part_Qty Total_Opendhr
          FROM t251_inventory_lock_details t251 ,
              (SELECT t205a.c205_from_part_number_id,
                      t205a.c205a_qty sub_part_qty
                 From T205a_Part_Mapping T205a
                 START WITH t205a.c205_to_part_number_id = p_pnum
                 AND t205a.c205_to_part_number_id IN
                 (SELECT t205d.c205_part_number_id FROM t205d_sub_part_to_order t205d
                  )
            CONNECT BY PRIOR t205a.c205_from_part_number_id = t205a.c205_to_part_number_id
            AND t205a.c205a_void_fl IS NULL
                      ) t205a
            Where T205a.C205_From_Part_Number_Id = T251.C205_Part_Number_Id
           AND t251.c250_inventory_lock_id      = p_inv_lock_id
                   )GROUP BY pnum,des;  
          SELECT get_partnum_desc (p_pnum)
		  INTO p_desc
		  FROM DUAL;
	END gm_fc_fch_ppqdetails;

/*****************************************************
   *Description : Procedure to fetch Parent-Sub PartDetails
   *
   * Author 	 : RShah
 ************************************************************/
	PROCEDURE gm_ld_parent_sub_parts (
		p_inventoryid	     	 IN    t250_inventory_lock.c250_inventory_lock_id%TYPE
	  , p_parentsubpartdetails	 OUT   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_parentsubpartdetails
		 FOR
				   SELECT t205a.c205_from_part_number_id parentpart
  					    , c205_to_part_number_id actual_sub_part
				        , CONNECT_BY_ROOT c205_to_part_number_id AS subpart
				        , CONNECT_BY_ROOT c205_from_part_number_id AS family
				        , CONNECT_BY_ROOT t205a.c205a_qty AS QTY
   						, t205a.c205a_qty ACTUAL_QTY
   						, LEVEL
              			, SYS_CONNECT_BY_PATH(t205a.c255_lead_time,',') CALC_LEAD_TIME
 					 FROM 
				           (SELECT  t205a.c205_from_part_number_id, t205a.c205_to_part_number_id, c205a_qty, c255_lead_time
				              FROM t205a_part_mapping t205a
				                 , t255_part_attribute_lock t255
				           	 WHERE t205a.c205_from_part_number_id = t255.c205_part_number_id
				           	   AND t255.c250_inventory_lock_id = p_inventoryid
				           	   AND t205a.c205a_void_fl IS NULL
                          GROUP BY t205a.c205_from_part_number_id, t205a.c205_to_part_number_id, c205a_qty, c255_lead_time
				           ) t205a
			   START WITH c205_to_part_number_id IN (SELECT DISTINCT c205_part_number_id 
               					                       FROM t205d_sub_part_to_order t205d 
                                 					  WHERE c205d_void_fl IS NULL
                                     					AND c205_part_number_id IS NOT NULL) 
		 CONNECT BY PRIOR t205a.c205_from_part_number_id = t205a.c205_to_part_number_id
     ORDER BY family,LEVEL DESC;
     
	END gm_ld_parent_sub_parts;

	 /*******************************************************
	* Description : Main Procedure to get ttp sheet details
	* Author	  : Ritesh
	*******************************************************/
	PROCEDURE gm_fc_fch_ttp_summary_sheets (
		p_ttpid 		   IN		t4050_ttp.c4050_ttp_id%TYPE
	  , p_monyear		   IN		VARCHAR2
	  , p_demandsheetids   IN		VARCHAR2
	  , p_inventoryid	   IN OUT	t250_inventory_lock.c250_inventory_lock_id%TYPE
	  , p_forecastmonth    IN		t4052_ttp_detail.c4052_forecast_period%TYPE
	  , p_sheetname 	   OUT		TYPES.cursor_type
	  , p_status		   OUT		VARCHAR2
	  , p_finaldate 	   OUT		VARCHAR2
	  , p_ds_status 	   OUT		VARCHAR2
	)
	AS
		v_ttp_detail_id t4052_ttp_detail.c4052_ttp_detail_id%TYPE;
		v_demand_sheetids VARCHAR2 (5000);
		v_string	   VARCHAR2 (100);
		v_inventoryid  t250_inventory_lock.c250_inventory_lock_id%TYPE;
		v_forecast_period t4052_ttp_detail.c4052_forecast_period%TYPE;
		v_fc_start_date DATE;

		CURSOR cur_demandsheetids
		IS
			SELECT c4040_demand_sheet_id ID
			  FROM t4040_demand_sheet
			 WHERE c4052_ttp_detail_id = v_ttp_detail_id;
	BEGIN
		
		
		IF (p_demandsheetids IS NULL)
		THEN
			v_demand_sheetids := 0;

			-- Fetch TTP Summary information
			SELECT c4052_ttp_detail_id, c901_status, c4052_forecast_period
			  INTO v_ttp_detail_id, p_status, v_forecast_period
			  FROM t4052_ttp_detail
			 WHERE c4050_ttp_id = p_ttpid AND TO_CHAR (c4052_ttp_link_date, 'MM/YYYY') = p_monyear;

			-- Fetch Inventory information
			SELECT c250_inventory_lock_id
			  INTO v_inventoryid
			  FROM t252_inventory_lock_ref
			 WHERE c252_ref_id = v_ttp_detail_id AND c901_ref_type = 20480;

			p_inventoryid := v_inventoryid;

			FOR cur_val IN cur_demandsheetids
			LOOP
				v_demand_sheetids := cur_val.ID || ',' || v_demand_sheetids;
			END LOOP;
		--
		ELSE
			v_demand_sheetids := p_demandsheetids;
			v_forecast_period := p_forecastmonth;
			p_status	:= NULL;
			v_inventoryid := p_inventoryid;
		END IF;
		
		

		my_context.set_my_inlist_ctx (v_demand_sheetids);

		SELECT DECODE (COUNT (1), 0, 'false', 'true')
		  INTO p_ds_status
		  FROM t4040_demand_sheet t4040
		 WHERE c4040_demand_sheet_id IN (SELECT *
										   FROM v_in_list) AND t4040.c901_status <> 50551;
		

		gm_fc_fch_demandsheet_name (p_sheetname);

		SELECT TO_CHAR (MAX (c4042_period), 'Mon YY')	-- '8/1/2007'
		  INTO p_finaldate
		  FROM t4042_demand_sheet_detail t4042
		 WHERE t4042.c901_type = 50563 AND t4042.c4040_demand_sheet_id IN (SELECT *
																			 FROM v_in_list);
																			 
		--raise_application_error('-20999',' p_demandsheetids: ' || p_demandsheetids || ' p_forecastmonth: ' || p_forecastmonth || ' p_finaldate: ' ||p_finaldate );
																			 
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			raise_application_error (-20057, '');	-- No Demand Sheets have been mapped
	END gm_fc_fch_ttp_summary_sheets;
	
	 /*******************************************************
	* Description : Procedure to get TTP Column Names
	* Author	  : VPrasath
	*******************************************************/
	PROCEDURE gm_fch_ttp_header (
		 p_split	  IN  	VARCHAR2 
	   , p_lvl_id 	  IN  	VARCHAR2
	   , p_lvl_hchy   IN 	VARCHAR2
	   , p_cur 	   	  OUT	TYPES.cursor_type
	)
	AS
	v_count_val NUMBER;
	v_count_id	NUMBER;
	v_lvl_hchy VARCHAR2(20);
	v_lvl_id VARCHAR2(20);
	
	BEGIN
		
		-- If p_lvl_id and p_lvl_hchy is not empty
		--,consider the ttp as multi part and fetch the company level columns 

		IF p_lvl_id = '0' AND p_lvl_hchy = '0' THEN
			v_lvl_id := 102580;
		END IF;
		
		IF p_split = 'Y' AND p_lvl_id = '0' AND p_lvl_hchy = '0' THEN 
		  v_lvl_id := v_lvl_id || 'SPLIT';
		  v_lvl_hchy := p_lvl_hchy || 'SPLIT';
		ELSIF p_split = 'Y' THEN
			v_lvl_id := p_lvl_id || 'SPLIT';
		  	v_lvl_hchy := p_lvl_hchy || 'SPLIT';
		ELSE
		  v_lvl_id := NVL(v_lvl_id,p_lvl_id);
		  v_lvl_hchy := p_lvl_hchy;
		END IF;
		
		select COUNT(1)  
		  INTO v_count_id
		  FROM t906_rules T906 
		 WHERE T906.c906_rule_id = v_lvl_id
		   AND c906_rule_grp_id = 'TTPCOLUMNS'
		   AND c906_void_fl IS NULL
		   AND c906_rule_value NOT IN (
           SELECT c901_code_nm
   			FROM t901_code_lookup
  			WHERE c901_code_grp = 'TTPFTR'
    		 AND c901_void_fl IS NULL);
		
		select COUNT(1)  
		 INTO v_count_val
		  FROM t906_rules T906 
		 WHERE T906.c906_rule_id = v_lvl_hchy
		   AND c906_rule_grp_id = 'TTPCOLUMNS'
		   AND c906_void_fl IS NULL
		    AND c906_rule_value NOT IN (
           SELECT c901_code_nm
   			FROM t901_code_lookup
  			WHERE c901_code_grp = 'TTPFTR'
    		 AND c901_void_fl IS NULL);

		IF v_count_id > 0 THEN
		OPEN p_cur FOR
		  SELECT C906_RULE_VALUE COL_NAME, c906_rule_id VAL
		   FROM t906_rules T906 
		  WHERE T906.c906_rule_id = v_lvl_id
		   AND c906_rule_grp_id = 'TTPCOLUMNS'
		   AND c906_void_fl IS NULL
		    AND c906_rule_value NOT IN (
           SELECT c901_code_nm
   			FROM t901_code_lookup
  			WHERE c901_code_grp = 'TTPFTR'
    		 AND c901_void_fl IS NULL);
		ELSIF v_count_val > 0 THEN
		OPEN p_cur FOR
		  SELECT C906_RULE_VALUE COL_NAME, c906_rule_id VAL
		   FROM t906_rules T906 
		  WHERE T906.c906_rule_id = v_lvl_hchy
		   AND c906_rule_grp_id = 'TTPCOLUMNS'
		   AND c906_void_fl IS NULL
		    AND c906_rule_value NOT IN (
           SELECT c901_code_nm
   			FROM t901_code_lookup
  			WHERE c901_code_grp = 'TTPFTR'
    		 AND c901_void_fl IS NULL);
		ELSE
		OPEN p_cur FOR
		  SELECT C906_RULE_VALUE COL_NAME, c906_rule_id VAL
		   FROM t906_rules T906 
		  WHERE T906.c906_rule_id = 'OTHERS'
		   AND c906_rule_grp_id = 'TTPCOLUMNS'
		   AND c906_void_fl IS NULL
		    AND c906_rule_value NOT IN (
           SELECT c901_code_nm
   			FROM t901_code_lookup
  			WHERE c901_code_grp = 'TTPFTR'
    		 AND c901_void_fl IS NULL);
		END IF;
		
END gm_fch_ttp_header;

	--
	/*******************************************************
   * Description : function to fetch inventory id
   * Author 	 : aprasath
   *******************************************************/
--
	FUNCTION gm_fch_ttp_inventoryid (
	    p_ttp_id 	       IN	 t4050_ttp.c4050_ttp_id%TYPE
	  , p_level_id	       IN	 t901_code_lookup.c901_code_id%TYPE
	  , p_company_id       IN	 t901_code_lookup.c901_code_id%TYPE
	  , p_month			   IN	 VARCHAR2
	  , p_year			   IN	 VARCHAR2
	)
	RETURN NUMBER
	IS
	v_demand_type 	   NUMBER;
	v_inventoryid      NUMBER;
	BEGIN
		
			SELECT MAX(T4020.c901_demand_type) 
			   INTO v_demand_type
			   FROM t4051_ttp_demand_mapping T4051
			      , t4020_demand_master t4020
			      , t4050_ttp t4050
			  WHERE t4051.c4020_demand_master_id = t4020.c4020_demand_master_id
			    AND t4050.c4050_ttp_id = t4051.c4050_ttp_id
			    AND t4050.c4050_ttp_id = p_ttp_id
			    AND t4020.c4020_void_fl IS NULL
			    AND t4050.c4050_void_fl IS NULL
			ORDER BY t4050.c4050_ttp_nm;
			
		
		--'40023' -- If multipart, always fetch the US Inventory
			SELECT MAX(t250.c250_inventory_lock_id) INTO  v_inventoryid 
			     FROM t250_inventory_lock t250
			           ,  t4016_global_inventory_mapping t4016            
			    WHERE t250.c250_void_fl IS NULL 
			          AND t250.c901_lock_type = '20430' --main inventory lock  20430
			          AND T250.C250_LOCK_DATE >= TO_DATE( p_month||'/'||p_year,'MM/YYYY') 
			          AND T250.C250_LOCK_DATE <= LAST_DAY(TO_DATE(p_month||'/'||p_year,'MM/YYYY'))
			          AND t4016.c4016_global_inventory_map_id = t250.c4016_global_inventory_map_id
			           AND NVL(t4016.c4016_global_inventory_map_id,'-9999')= NVL(( SELECT  t4015.c4016_global_inventory_map_id 
						                                FROM T4015_GLOBAL_SHEET_MAPPING T4015
						                                 WHERE NVL(T4015.C901_LEVEL_VALUE,'-9999') = 
						                                  DECODE(v_demand_type,'40023','100823', NVL(p_level_id,'-9999'))-- Level Selected ---- Hard code to US for Multi Part
						                                   AND NVL(T4015.C901_COMPANY_ID,'-9999') = DECODE(v_demand_type,'40023','100800',NVL(p_company_id,'-9999')) -- Company Based on the TTP Selected--Hard code to Globus for Multi Part
						                                   AND T4015.c4015_void_fl IS NULL  GROUP BY t4015.c4016_global_inventory_map_id
		                                        			),'-9999');
		 
	RETURN v_inventoryid;     	    
	END gm_fch_ttp_inventoryid;
	
/*****************************************************************
 * Description : function to get access type for passing demand sheets
 * Author 	 : Arockia Prasath
 *****************************************************************/

	FUNCTION gm_fch_ttp_access_type (
		p_demandsheetids   IN		VARCHAR2
	)
		RETURN NUMBER
	IS
		v_access_type  NUMBER;
	BEGIN
		my_context.set_my_inlist_ctx (p_demandsheetids); 
		
		SELECT t4015.c901_access_type INTO v_access_type
			   FROM t4040_demand_sheet t4040, t4020_demand_master t4020, t4015_global_sheet_mapping t4015
			  WHERE t4040.c4040_demand_sheet_id    in (SELECT * FROM v_in_list)
			    AND t4020.c4020_demand_master_id    = t4040.c4020_demand_master_id
			    AND t4015.c4015_global_sheet_map_id = t4020.c4015_global_sheet_map_id
			    AND t4040.c4040_void_fl            IS NULL
			    AND t4020.c4020_void_fl            IS NULL
			    AND t4015.c4015_void_fl            IS NULL
			GROUP BY t4015.c901_access_type;
		RETURN v_access_type;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END gm_fch_ttp_access_type;
/*****************************************************************
 * Description : Procedure to fetch new ttp summary details from new table (T251_INVENTORY_LOCK_DETAILS)
 * Author 	   : mmuthusamy
 *****************************************************************/
 PROCEDURE gm_fch_new_ttp_summary_dtls(
        p_ttpid 		    IN		t4050_ttp.c4050_ttp_id%TYPE,
 	    p_ttpnm		        IN		t4050_ttp.C4050_TTP_NM%TYPE,
	    p_demandsheetids    IN		VARCHAR2,
	    p_inventoryid	    IN    	t250_inventory_lock.c250_inventory_lock_id%TYPE,
	    p_level_id	        IN	    t901_code_lookup.c901_code_id%TYPE,
	    p_forecastmonth     IN 	    t4052_ttp_detail.c4052_forecast_period%TYPE,
	    p_monyear           IN      VARCHAR2, 
	    p_out_summary_cur   OUT	    TYPES.cursor_type	
	)
	AS
	v_ttp_detail_id t4052_ttp_detail.c4052_ttp_detail_id%TYPE;
	v_ttp_type  t4050_ttp.c901_ttp_type%TYPE;
	v_level_value	 t901_code_lookup.c901_code_id%TYPE;
	BEGIN
	--set p_demandsheetids into inlist 
	my_context.set_my_inlist_ctx (p_demandsheetids);
	
	-- to asssign the level id to local variable
	
	v_level_value := p_level_id;
	
	-- Fetch TTP_detail_id
	BEGIN
		
	SELECT t4052.c4052_ttp_detail_id, t4050.c901_ttp_type
	  INTO v_ttp_detail_id, v_ttp_type
	  FROM t4052_ttp_detail t4052, t4050_ttp t4050
	 WHERE t4050.c4050_ttp_id = p_ttpid
	   AND t4050.c4050_ttp_id = t4052.c4050_ttp_id
	   AND t4052.c4052_ttp_link_date = to_date('01/' || p_monyear, 'dd/MM/yyyy')
	  -- AND C4052_FORECAST_PERIOD = p_forecastmonth
	   AND t4052.c4052_void_fl IS NULL
	   AND t4050.C4050_void_fl IS NULL;
	   
	  EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
		v_ttp_detail_id := 0;
		v_ttp_type := 0;
		END;
		
	-- Multi part sheet - always available TTP type. based on type to set level value.
	 IF v_level_value = 0 AND v_ttp_type = 50310
	 THEN
	 
	 	BEGIN
		    -- Multipart spine (level value always - Null) so setting the default company - 100800
		     
			SELECT NVL (c901_level_value, 100800) INTO v_level_value
			   FROM t4040_demand_sheet
			  WHERE c4052_ttp_detail_id   = v_ttp_detail_id
			    --AND c4040_demand_sheet_id IN (SELECT * FROM v_in_list) 
			    AND c4040_void_fl IS NULL;
			    
		    EXCEPTION
			WHEN OTHERS
			THEN
				v_level_value := 0;
			END;
	 
	 END IF; --end v_ttp_type check

		
	OPEN p_out_summary_cur
	FOR
	
   SELECT 
   t4055.c4055_ttp_summary_id ttp_summary_id, 
   t4055.c205_part_number_id pnum, 
   t4055.c205_part_num_desc part_desc,
   NVL(t4055.c255_unit_cost, 0) unit_cost, 
   NVL(t4055.c255_lead_time,0) lead_time, 
   decode(t4055.c4055_month_01,0,'-',t4055.c4055_month_01) mon_01, 
   decode(t4055.c4055_month_02,0,'-',t4055.c4055_month_02) mon_02, 
   decode(t4055.c4055_month_03,0,'-',t4055.c4055_month_03) mon_03, 
   decode(t4055.c4055_month_04,0,'-',t4055.c4055_month_04) mon_04, 
   decode(t4055.c4055_month_05,0,'-',t4055.c4055_month_05) mon_05, 
   decode(t4055.c4055_month_06,0,'-',t4055.c4055_month_06) mon_06, 
   decode(t4055.c4055_month_07,0,'-',t4055.c4055_month_07) mon_07, 
   decode(t4055.c4055_month_08,0,'-',t4055.c4055_month_08) mon_08, 
   decode(t4055.c4055_month_09,0,'-',t4055.c4055_month_09) mon_09, 
   decode(t4055.c4055_month_10,0,'-',t4055.c4055_month_10) mon_10, 
   decode(t4055.c4055_month_11,0,'-',t4055.c4055_month_11) mon_11,
   decode(t4055.c4055_month_12,0,'-',t4055.c4055_month_12) mon_12,
   --
  decode(t4055.c4055_forecast_qty,0,'-',t4055.c4055_forecast_qty) forecast_qty,
  decode(t4055.c4055_us_forecast_qty,0,'-',t4055.c4055_us_forecast_qty) us_forecast_qty,
  decode(t4055.c4055_ous_forecast_qty,0,'-',t4055.c4055_ous_forecast_qty) ous_forecast_qty,
  decode(t4055.c255_lta,0,'-',t4055.c255_lta) lta,
  decode(t4055.c255_us_lta,0,'-',t4055.c255_us_lta) us_lta,
  decode(t4055.c255_ous_lta,0,'-',t4055.c255_ous_lta) ous_lta,
  decode(t4055.c255_parent_lta,0,'-',t4055.c255_parent_lta) parent_lta,
  decode(nvl(t4055.c255_parent_us_lta, 0),0,'-',t4055.c255_parent_us_lta) parent_us_lta,
  decode(nvl(t4055.c255_parent_ous_lta, 0),0,'-',t4055.c255_parent_ous_lta) parent_ous_lta,
  decode(nvl(t4055.c255_parent_need, 0),0,'-',t4055.c255_parent_need) parent_need,
   
    -- missing TPR
 decode(nvl(t4055.c4055_set_par, 0),0,'-',t4055.c4055_set_par) set_par,
 decode(nvl(t4055.c4023_otn, 0),0,'-',t4055.c4023_otn) otn,
 decode(nvl(t4055.c4023_us_otn, 0),0,'-',t4055.c4023_us_otn) us_otn,
 decode(nvl(t4055.c4023_ous_otn, 0),0,'-',t4055.c4023_ous_otn) ous_otn,
 decode(nvl(t4055.c4055_total_required_qty, 0),0,'-',t4055.c4055_total_required_qty) total_req_qty,
 decode(nvl(t4055.c4055_total_order_qty, 0),0,'-',t4055.c4055_total_order_qty) total_ord_qty,
 NVL(t4055.c4055_total_order_cost, 0) total_ord_cost,
 t4055.c4052_ttp_detail_id ttp_detail_id,
 t205.c205_product_family prod_family_id,
 t901_part_family.c901_code_nm prod_family_name,
 t205.c205_crossover_fl crossover_fl,
   -- inventory qty
  nvl(t251.qn_allo, '-') qn_allo, 
  nvl(t251.qn_avail, '-') qn_avail, 
  nvl(t251.open_po, '-') open_po, 
  nvl(t251.open_dhr, '-') open_dhr, 
  nvl(t251.build_set, '-') build_set, 
  nvl(t251.in_stock, '-') in_stock, 
  nvl(t251.parent_part_qty, '-') parent_part_qty, 
  nvl(t251.rw_inv, '-') rw_inv, 
  decode(t251.total_inv, null, '-', t251.total_inv) total_inv,
  decode(t4055.c4055_tpr_fl, 1, t251.tpr, 0) tpr, 
  decode(t4055.c4055_tpr_fl, 1, t251.us_tpr, 0)  us_tpr, 
  decode(t4055.c4055_tpr_fl, 1, t251.ous_tpr, 0) ous_tpr,
  decode(nvl(t4055.C4025_SAFETY_STOCK_QTY, 0),0,'-',t4055.C4025_SAFETY_STOCK_QTY) sstock_qty
FROM t4055_ttp_summary t4055, 
     t205_part_number t205, 
     --t251_inventory_lock_details t251,
     t901_code_lookup t901_part_family,
     (select t251.c205_part_number_id pnum,  decode(t251.c251_quar_alloc,0,'-',t251.c251_quar_alloc) qn_allo, 
  decode(t251.c251_quar_avail,0,'-',t251.c251_quar_avail) qn_avail, 
  decode(t251.c251_open_po,0,'-',t251.c251_open_po) open_po, 
  decode(t251.c251_open_dhr,0,'-',t251.c251_open_dhr) open_dhr, 
  decode(t251.c251_build_set,0,'-',t251.c251_build_set) build_set, 
  decode(t251.c251_in_stock,0,'-',t251.c251_in_stock) in_stock, 
  decode(t251.c251_parent_part_qty,0,'-',t251.c251_parent_part_qty) parent_part_qty, 
  decode(t251.c251_rw_inventory,0,'-',t251.c251_rw_inventory) rw_inv, 
  (t251.c251_in_stock + t251.c251_open_po + t251.c251_open_dhr + t251.c251_build_set) total_inv,
  t251.c251_tpr tpr, 
  t251.c251_tpr_us  us_tpr, 
  t251.c251_tpr_ous ous_tpr from T251_INVENTORY_LOCK_DETAILS t251
  where t251.c250_inventory_lock_id = p_inventoryid
  ) t251
            
  WHERE t205.c205_part_number_id  = t4055.c205_part_number_id
    AND t4055.c205_part_number_id    = t251.pnum (+)
    AND t205.c205_product_family = t901_part_family.c901_code_id
    --AND t251.c250_inventory_lock_id = p_inventoryid 
    AND t4055.c901_level_value = v_level_value
    AND t4055.c4052_ttp_detail_id = v_ttp_detail_id
    AND t4055.c4055_void_fl      IS NULL
    AND t901_part_family.c901_void_fl IS NULL
ORDER BY t901_part_family.c901_code_id,t205.c205_part_number_id;
--

END gm_fch_new_ttp_summary_dtls;	


      /* *****************************************************************
      * Description:  This Procedure will set the specific parts for which the Parent and Sub Parts will be pulled.
      *  As each TTP have different Part list,we need this procedure to get the Parent And sub part for the TTP only. 
      * Author        : Rajeshwaran
      *****************************************************************/
PROCEDURE gm_ld_spec_parent_sub_parts (
            p_inventoryid           IN    t250_inventory_lock.c250_inventory_lock_id%TYPE
        , p_parentsubpartdetails    OUT   TYPES.cursor_type
      )
      AS
      BEGIN
            OPEN p_parentsubpartdetails
            FOR
                           SELECT t205a.c205_from_part_number_id parentpart
                                  , c205_to_part_number_id actual_sub_part
                                , CONNECT_BY_ROOT c205_to_part_number_id AS subpart
                                , CONNECT_BY_ROOT c205_from_part_number_id AS family
                                , CONNECT_BY_ROOT t205a.c205a_qty AS QTY
                                    , t205a.c205a_qty ACTUAL_QTY
                                    , LEVEL
                              , SYS_CONNECT_BY_PATH(t205a.c255_lead_time,',') CALC_LEAD_TIME
                              FROM 
                                   (SELECT  t205a.c205_from_part_number_id, t205a.c205_to_part_number_id, c205a_qty, c255_lead_time
                                      FROM t205a_part_mapping t205a
                                         , t255_part_attribute_lock t255
                                    WHERE t205a.c205_from_part_number_id = t255.c205_part_number_id
                                      AND t255.c250_inventory_lock_id = p_inventoryid
                                      AND t205a.c205a_void_fl IS NULL
                                      AND t255.c205_part_number_id IN (SELECT c205_part_number_id FROM my_temp_part_list)
                          GROUP BY t205a.c205_from_part_number_id, t205a.c205_to_part_number_id, c205a_qty, c255_lead_time
                                   ) t205a
                     START WITH c205_to_part_number_id IN (SELECT DISTINCT c205_part_number_id 
                                                                 FROM t205d_sub_part_to_order t205d 
                                                              WHERE c205d_void_fl IS NULL
                                                                  AND c205_part_number_id IN (SELECT c205_part_number_id FROM my_temp_part_list)
                                                                  AND c205_part_number_id IS NOT NULL) 
             CONNECT BY PRIOR t205a.c205_from_part_number_id = t205a.c205_to_part_number_id
     ORDER BY family,LEVEL DESC;

  END gm_ld_spec_parent_sub_parts; 

END gm_pkg_oppr_ttp_summary;
/
