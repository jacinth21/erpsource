/* Formatted on 2010/03/01 11:31 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\purchasing\gm_pkg_oppr_ld_demand.bdy"
-- show err  Package Body gm_pkg_oppr_ld_demand
-- exec gm_pkg_oppr_ld_demand.gm_op_ld_demand_main(15);

/*
 DELETE FROM T4042_DEMAND_SHEET_DETAIL WHERE c4040_demand_sheet_id IN (SELECT c4040_demand_sheet_id FROM t4040_demand_sheet
					WHERE	c4020_demand_master_id = 18)  ;
 DELETE FROM T4041_DEMAND_SHEET_MAPPING WHERE c4040_demand_sheet_id IN (SELECT c4040_demand_sheet_id FROM t4040_demand_sheet
					WHERE	c4020_demand_master_id = 18)  ;
 DELETE FROM T4040_DEMAND_SHEET WHERE  c4020_demand_master_id = 18;
 COMMIT ;
 exec gm_pkg_oppr_ld_demand.gm_op_ld_demand_main(7325);
*/

CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ld_demand
IS
	/*******************************************************
	* Purpose: This procedure will be main procedure to
	*			load sheet information
	* Accepts demand id and loads demand  and forecast information
	*******************************************************/
	PROCEDURE gm_op_ld_demand_main (
		p_demand_id 		t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_request_by   IN	t4020_demand_master.c4020_created_by%TYPE DEFAULT '30301'
	)
	AS
		--
		v_demand_type       t4020_demand_master.c901_demand_type%TYPE;
		v_demand_period     t4020_demand_master.c4020_demand_period%TYPE;
		v_forecast_period   t4020_demand_master.c4020_forecast_period%TYPE;
		v_inc_current_fl    t4020_demand_master.c4020_include_current_fl%TYPE;
		v_demand_sheet_id   t4040_demand_sheet.c4040_demand_sheet_id%TYPE;
		v_demand_nm         t4020_demand_master.c4020_demand_nm%TYPE;
		v_primary_user      t4020_demand_master.c4020_primary_user%TYPE;
		v_request_period    t4020_demand_master.c4020_request_period%TYPE;
		v_inventory_id      t250_inventory_lock.c250_inventory_lock_id%TYPE;
        -- 
        -- Global mapping parameter 
        v_level_id                  t4040_demand_sheet.c901_level_id%TYPE;                  
        v_level_value               t4040_demand_sheet.c901_level_value%TYPE;               
        v_access_type               t4040_demand_sheet.c901_access_type%TYPE;               
        v_global_sheet_map_id       t4040_demand_sheet.c4015_global_sheet_map_id%TYPE;      
        v_global_inventory_map_id   t4040_demand_sheet.c4016_global_inventory_map_id%TYPE;
        --
        v_parent_demand_id          t4020_demand_master.c4020_parent_demand_master_id%TYPE;
        v_company_id                t4020_demand_master.c901_company_id%type;        
		--
		v_start_dt	   DATE;
		v_from_period  DATE;
		v_to_period    DATE;
		v_exclude_dmd_mas_fl	t4020_demand_master.c4020_exclude_ld_fl%TYPE;
		v_ttp_id 				T4051_TTP_DEMAND_MAPPING.C4050_TTP_ID%TYPE;
		
		v_ttp_detail_id 		T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE;
		v_ttp_detail_cnt 		NUMBER;
		v_ttp_type   	   		t4050_ttp.c901_ttp_type%TYPE;	
		--
	--
	BEGIN
		v_start_dt	:= SYSDATE;

		--
		SELECT	   t4020.c901_demand_type, t4020.c4020_demand_nm, t4020.c4020_demand_period, t4020.c4020_forecast_period
				 , t4020.c4020_include_current_fl, t4020.c4020_primary_user, t4020.c4020_request_period
                 , t4015.c901_level_id , t4015.c901_level_value, t4015.c901_access_type 
                 , t4015.c4015_global_sheet_map_id, t4015.c4016_global_inventory_map_id
                 , t4020.c4020_parent_demand_master_id , t4020.c901_company_id, t4020.c4020_exclude_ld_fl
			  INTO v_demand_type, v_demand_nm, v_demand_period, v_forecast_period
				 , v_inc_current_fl, v_primary_user, v_request_period
                 , v_level_id, v_level_value, v_access_type 
                 , v_global_sheet_map_id, v_global_inventory_map_id
                 , v_parent_demand_id , v_company_id,v_exclude_dmd_mas_fl
			  FROM t4020_demand_master t4020 , t4015_global_sheet_mapping t4015
			 WHERE t4020.c4020_demand_master_id = p_demand_id
               AND t4020.c4015_global_sheet_map_id = t4015.c4015_global_sheet_map_id 
		FOR UPDATE;

		--
		SELECT s4040_demand_sheet.NEXTVAL
		  INTO v_demand_sheet_id
		  FROM DUAL;

		-- Below code to fetch forecast (from and to period)
		gm_pkg_oppr_ld_demand.gm_op_calc_period (v_forecast_period, v_inc_current_fl, 'F', v_from_period, v_to_period);

		--
		-- Below code to get current inventory value
        -- Should always pick US (Company level Inventory) 
        -- Thats the reason global inventory hardcoded to 1 
		SELECT MAX (t250.c250_inventory_lock_id)
		  INTO v_inventory_id
		  FROM t250_inventory_lock t250
		 WHERE t250.c901_lock_type = 20430
           AND t250.c4016_global_inventory_map_id  = 1;
           
        --Get TTP ID For the template
        BEGIN
	        SELECT C4050_TTP_ID
	        INTO v_ttp_id
	        FROM T4051_TTP_DEMAND_MAPPING
	        WHERE C4020_DEMAND_MASTER_ID=v_parent_demand_id;
	   EXCEPTION WHEN OTHERS
	   THEN
	   		v_ttp_id := null;
	   END;
        
	   IF v_ttp_id IS NOT NULL
	   THEN
	   	--Check if TTP Detail exists for the current Month for the TTP.
	   		SELECT COUNT(1)
	   		INTO v_ttp_detail_cnt
	   		FROM T4052_TTP_DETAIL
	   		WHERE C4050_TTP_ID = v_ttp_id
	   		AND C4052_VOID_FL IS NULL
	   		AND C4052_TTP_LINK_DATE = v_from_period;
	   		
	   		IF v_ttp_detail_cnt = 0
	   		THEN
	   			-- Create TTP Detail for the TTP.
	   			
	   			gm_pkg_oppr_ttp_finalize_setup.gm_save_ttp_details (v_ttp_detail_id, v_ttp_id, 50580, v_from_period, 4, p_request_by);
	   		
	   		END IF;
	   		
	   		IF v_ttp_detail_cnt = 1
	   		THEN
		   		SELECT C4052_TTP_DETAIL_ID
		   		INTO v_ttp_detail_id
		   		FROM T4052_TTP_DETAIL
		   		WHERE C4050_TTP_ID = v_ttp_id
		   		AND C4052_VOID_FL IS NULL
		   		AND C4052_TTP_LINK_DATE = v_from_period;
	   		END IF;
	   	END IF;

		-- Below procedure will load demand sheet information
		gm_pkg_oppr_ld_demand.gm_op_sav_sheet_main (p_demand_id
											, v_demand_sheet_id, v_demand_type, v_demand_period
											, v_forecast_period, v_inc_current_fl, v_from_period
											, v_primary_user, v_request_period, v_inventory_id
                                            , v_level_id, v_level_value, v_access_type 
                                            , v_global_sheet_map_id, v_global_inventory_map_id,
                                            p_request_by,
                                            v_ttp_detail_id
											 );
		
		SELECT DECODE(p_request_by,'30301',v_exclude_dmd_mas_fl,'') INTO v_exclude_dmd_mas_fl FROM DUAL;
		
		UPDATE t4020_demand_master
		SET    c4020_exclude_ld_fl= v_exclude_dmd_mas_fl ,C4020_LAST_UPDATED_DATE=SYSDATE
		WHERE  c4020_demand_master_id = p_demand_id;
			
		-- When Exclude Flag is available, the demand Sheet should not be loaded.										
		IF (NVL(v_exclude_dmd_mas_fl,'N') = 'Y')
		THEN
			gm_pkg_oppr_ld_demand.gm_op_send_email (p_demand_id
										, v_demand_nm
										, '01/' || TO_CHAR (v_start_dt, 'MM/YYYY')
										, v_start_dt
										, 'S'
										, 'SHEET LOAD EXCLUDED'
										, p_request_by
										 ); 
			COMMIT;
			
			return;
			
		END IF;

			
		--
		-- Below code to load demand mapping information
        --DBMS_OUTPUT.put_line ('Loading Sheet Mapping  ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
		gm_pkg_oppr_ld_demand.gm_op_sav_sheet_mapping (p_demand_id, v_demand_sheet_id);
        
		-- Below code to save growth information
        --DBMS_OUTPUT.put_line ('Loading Growth Mapping   ' || v_access_type || ' -- ' || v_demand_type || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS') );
		gm_pkg_oppr_ld_demand.gm_op_sav_sheet_growth (p_demand_id, v_demand_sheet_id, v_from_period, v_to_period);

		--
		-- based on the type will redirect to sales or consignment
		
		-- 102622 Represent Read only rollup sheet 
        IF (v_access_type =  102622) 
        THEN
			--
			gm_pkg_oppr_ld_rollup_demand.gm_op_ld_main (p_demand_id
												   , v_demand_sheet_id
												   , v_demand_period
												   , v_forecast_period
												   , v_inc_current_fl
                                                   , v_inventory_id 
													);
        -- 40020 maps to sales consignment
		ELSIF (v_demand_type = 40020)
		THEN
			--
			gm_pkg_oppr_ld_sales_demand.gm_op_ld_main (p_demand_id
												   , v_demand_sheet_id
												   , v_demand_period
												   , v_forecast_period
												   , v_inc_current_fl
													);
        -- 40021 maps to inhouse consignment
		ELSE
			-- Field and Inhouse consignment
			gm_pkg_oppr_ld_consign_demand.gm_op_ld_main (p_demand_id
													 , v_demand_sheet_id
													 , v_demand_type
													 , v_demand_period
													 , v_forecast_period
													 , v_inc_current_fl
													 , v_inventory_id
													  );
		END IF;
    
        -- if its a editable sheet the load all read only sheet 
        IF (v_access_type =  102623) 
        THEN   
            -- Load associated read only sheet information  
            gm_pkg_oppr_ld_demand.gm_op_sav_rollup_sheet_info (v_parent_demand_id 
                            , v_demand_type, v_company_id , v_level_id, v_level_value,p_request_by);
        END IF;                                         
        
        --Load the TTP Summary for the TTP.
        --gm_pkg_oppr_ld_summary.gm_op_ld_summary_main(v_ttp_detail_id,'',p_request_by);
        
        -- Copy the information from temp to main table 
        gm_pkg_oppr_ld_demand.gm_ld_demand_sheet_tmp_to_main;
              
		gm_pkg_oppr_ld_demand.gm_op_send_email (p_demand_id
											, v_demand_nm
											, '01/' || TO_CHAR (v_start_dt, 'MM/YYYY')
											, v_start_dt
											, 'S'
											, 'SUCCESS'
											, p_request_by
											 ); 
		COMMIT;

		EXCEPTION
	    	WHEN NO_DATA_FOUND
			THEN
				ROLLBACK;
	            DBMS_OUTPUT.put_line ('******** Error please check **** ' || SQLERRM);
				-- Final log insert statement (Error Message)
				gm_pkg_oppr_ld_demand.gm_op_send_email (p_demand_id
													, v_demand_nm
													, TO_CHAR (v_start_dt, 'MM') || '/01/' || TO_CHAR (v_start_dt, 'YYYY')
													, v_start_dt
													, 'E'
													, SQLERRM
													, p_request_by
													 ); 
				COMMIT;
		--
	
			WHEN OTHERS
			THEN
				ROLLBACK;
	            DBMS_OUTPUT.put_line ('******** Error please check **** ' || SQLERRM);
				-- Final log insert statement (Error Message)
				gm_pkg_oppr_ld_demand.gm_op_send_email (p_demand_id
													, v_demand_nm
													, TO_CHAR (v_start_dt, 'MM') || '/01/' || TO_CHAR (v_start_dt, 'YYYY')
													, v_start_dt
													, 'E'
													, SQLERRM
													, p_request_by
													 );
				COMMIT;
			--
	END gm_op_ld_demand_main;

--
	/***********************************************************
	* Purpose: Procedure used to save demand sheet information
	************************************************************/
	PROCEDURE gm_op_sav_sheet_main (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_demand_type		IN	 t4020_demand_master.c901_demand_type%TYPE
	  , p_demand_period 	IN	 t4020_demand_master.c4020_demand_period%TYPE
	  , p_forecast_period	IN	 t4020_demand_master.c4020_forecast_period%TYPE
	  , p_inc_current_fl	IN	 t4020_demand_master.c4020_include_current_fl%TYPE
	  , p_from_period		IN	 DATE
	  , p_primary_user		IN	 t4020_demand_master.c4020_primary_user%TYPE
	  , p_request_period	IN	 t4020_demand_master.c4020_request_period%TYPE
	  , p_inventory_id		IN	 t250_inventory_lock.c250_inventory_lock_id%TYPE
      , p_level_id          IN   t4040_demand_sheet.c901_level_id%TYPE                  
      , p_level_value       IN	 t4040_demand_sheet.c901_level_value%TYPE               
      , p_access_type       IN	 t4040_demand_sheet.c901_access_type%TYPE               
      , p_global_sheet_map_id       IN	t4040_demand_sheet.c4015_global_sheet_map_id%TYPE      
      , p_global_inventory_map_id   IN	t4040_demand_sheet.c4016_global_inventory_map_id%TYPE
      , p_request_by   IN	t4020_demand_master.c4020_created_by%type
      , p_ttp_detail_id IN 		T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE
      
	)
	AS
		--
		v_count 	   NUMBER;
		v_status	   t4040_demand_sheet.c901_status%TYPE;
	BEGIN
		--
		BEGIN
			--
			-- Check if record already found for selected period if yes delete the same
			SELECT c901_status
			  INTO v_status
			  FROM t4040_demand_sheet
			 WHERE c4020_demand_master_id = p_demand_id AND c4040_demand_period_dt = p_from_period AND ROWNUM = 1;

			IF (v_status <> 50550)
			THEN
				-- Error message is  Set Consignment is not in Shipped Status and cannot be rolled back.
					raise_application_error (-20016, 'DEMAND SHEET NOT IN OPEN STATE');
			ELSE
		-- PMT: 14582 : GOP DB SERVER MIGRATION. Excluding the below delete query for the monthly load. 
			 IF (P_REQUEST_BY <> '30301')
			 	THEN
				DELETE FROM t4042_demand_sheet_detail
					  WHERE c4040_demand_sheet_id IN (
								  SELECT c4040_demand_sheet_id
									FROM t4040_demand_sheet
								   WHERE c4020_demand_master_id = p_demand_id
										 AND c4040_demand_period_dt = p_from_period);

				DELETE FROM t4041_demand_sheet_mapping
					  WHERE c4040_demand_sheet_id IN (
								  SELECT c4040_demand_sheet_id
									FROM t4040_demand_sheet
								   WHERE c4020_demand_master_id = p_demand_id
										 AND c4040_demand_period_dt = p_from_period);

				DELETE FROM t4043_demand_sheet_growth
					  WHERE c4040_demand_sheet_id IN (
								  SELECT c4040_demand_sheet_id
									FROM t4040_demand_sheet
								   WHERE c4020_demand_master_id = p_demand_id
										 AND c4040_demand_period_dt = p_from_period);

				DELETE FROM t4044_demand_sheet_request
					  WHERE c4040_demand_sheet_id IN (
								  SELECT c4040_demand_sheet_id
									FROM t4040_demand_sheet
								   WHERE c4020_demand_master_id = p_demand_id
										 AND c4040_demand_period_dt = p_from_period);
				
                DELETE FROM t4046_demand_sheet_assoc_map 
					  WHERE c4040_demand_sheet_id IN (
								  SELECT c4040_demand_sheet_id
									FROM t4040_demand_sheet
								   WHERE c4020_demand_master_id = p_demand_id
										 AND c4040_demand_period_dt = p_from_period);


				DELETE FROM t4040_demand_sheet
					  WHERE c4020_demand_master_id = p_demand_id AND c4040_demand_period_dt = p_from_period;
		 END IF;
                
                -- Table used to load temporary information and finally will be moved to 
                -- table t4042_demand_sheet_detail
                execute immediate 'TRUNCATE TABLE my_temp_demand_sheet_detail';

			END IF;
		--
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				NULL;
		END;

		INSERT INTO t4040_demand_sheet
					(c4040_demand_sheet_id, c4020_demand_master_id, c4040_load_dt, c4040_demand_period
				   , c4040_forecast_period, c901_status, c4040_demand_period_dt, c4040_primary_user, c901_demand_type
				   , c4040_request_period, c250_inventory_lock_id, c901_level_id , c901_level_value, c901_access_type 
                   , c4015_global_sheet_map_id, c4016_global_inventory_map_id  ,C4052_TTP_DETAIL_ID
					)
			 VALUES (p_demand_sheet_id, p_demand_id, SYSDATE, p_demand_period
				   , p_forecast_period, 50550, p_from_period, p_primary_user, p_demand_type
				   , p_request_period, p_inventory_id,  p_level_id, p_level_value, p_access_type 
                   , p_global_sheet_map_id, p_global_inventory_map_id,p_ttp_detail_id
					);
	END gm_op_sav_sheet_main;

--
	/***********************************************************
	* Purpose: Procedure used to save demand sheet information
	************************************************************/
	PROCEDURE gm_op_sav_sheet_mapping (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	)
	AS
	--
	BEGIN
		--
		INSERT INTO t4041_demand_sheet_mapping
					(c4041_demand_sheet_map_id, c4040_demand_sheet_id, c901_ref_type, c4041_ref_id, c901_action_type)
			SELECT s4041_demand_sheet_mapping.NEXTVAL, p_demand_sheet_id, c901_ref_type, c4021_ref_id
				 , c901_action_type
			  FROM t4021_demand_mapping t4021, t4010_group t4010
			 WHERE t4021.c4020_demand_master_id = p_demand_id
			   AND t4021.c4021_ref_id = (TO_CHAR (t4010.c4010_group_id(+))) 
			   AND t4010.c4010_void_fl IS NULL;
	--
	END gm_op_sav_sheet_mapping;

	/***********************************************************
	* Purpose: Procedure used to save demand sheet growth information
	************************************************************/
	PROCEDURE gm_op_sav_sheet_growth (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_from_period		IN	 DATE
	  , p_to_period 		IN	 DATE
	)
	AS
	--
	BEGIN
		--
		INSERT INTO t4043_demand_sheet_growth
					(c4043_demand_sheet_growth_id, c4040_demand_sheet_id, c901_ref_type
				   , c4043_value, c4043_start_date, c4043_end_date, c901_grth_ref_type , c4030_grth_ref_id)
			SELECT s4043_demand_sheet_growth.NEXTVAL, p_demand_sheet_id, t4031.c901_ref_type 
				 , t4031.c4031_value, t4031.c4031_start_date, t4031.c4031_end_date
                 , t4030.c901_ref_type ,t4030.c4030_ref_id  
			  FROM t4030_demand_growth_mapping t4030, t4031_growth_details t4031, t4010_group t4010
			 WHERE t4030.c4020_demand_master_id = p_demand_id
			   AND t4030.c4030_demand_growth_id = t4031.c4030_demand_growth_id
			   AND t4030.c4030_void_fl IS NULL
			   AND t4031.c4031_start_date >= p_from_period
			   AND t4031.c4031_start_date <= p_to_period
               AND t4031.c4031_void_fl IS NULL 
			   AND t4030.c4030_ref_id = (TO_CHAR (t4010.c4010_group_id(+))) 
			   AND t4010.c4010_void_fl IS NULL;
	--
	END gm_op_sav_sheet_growth;

--
	/***********************************************************
	* Purpose: Procedure used to save demand detail information
	************************************************************/
	PROCEDURE gm_op_sav_sheet_detail (
		p_demand_sheet_id	  IN   t4040_demand_sheet.c4040_demand_sheet_id%TYPE
      , p_ref_id			  IN   t4042_demand_sheet_detail.c4042_ref_id%TYPE
      , p_ref_type	          IN   t4042_demand_sheet_detail.c901_ref_type%TYPE
	  , p_type				  IN   t4042_demand_sheet_detail.c901_type%TYPE
	  , p_period			  IN   t4042_demand_sheet_detail.c4042_period%TYPE
	  , p_part_number_id	  IN   t4042_demand_sheet_detail.c205_part_number_id%TYPE
	  , p_qty				  IN   t4042_demand_sheet_detail.c4042_qty%TYPE
	  , p_parent_part_id	  IN   t4042_demand_sheet_detail.c205_part_number_id%TYPE DEFAULT NULL
	  , p_parent_ref_id 	  IN   t4042_demand_sheet_detail.c4042_ref_id%TYPE DEFAULT NULL
	  , p_parent_qty_needed   IN   t4042_demand_sheet_detail.c4042_parent_qty_needed%TYPE DEFAULT NULL
      , p_history_fl          IN   t4042_demand_sheet_detail.c4042_history_fl%TYPE  DEFAULT NULL
      , p_comments_fl         IN   t4042_demand_sheet_detail.c4042_comments_fl%TYPE DEFAULT NULL
      , p_weighted_avg_diff   IN   t4042_demand_sheet_detail.c4042_weighted_avg_diff_qty%TYPE   DEFAULT NULL
	)
	AS
	--
    v_start_dt	   DATE;
	BEGIN
		--
        v_start_dt	:= SYSDATE;
		INSERT INTO my_temp_demand_sheet_detail
					(c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id, c901_type, c4042_period
				   , c205_part_number_id, c4042_qty, c4042_updated_by, c4042_updated_date, c205_parent_part_num_id
				   , c4042_parent_ref_id, c4042_parent_qty_needed, c4042_history_fl, c4042_comments_fl
                   , c4042_weighted_avg_diff_qty, c901_ref_type
					)
			 VALUES (s4042_demand_sheet_detail.NEXTVAL, p_demand_sheet_id, p_ref_id, p_type, p_period
				   , p_part_number_id, ROUND(p_qty,0), 30301, SYSDATE, p_parent_part_id
				   , p_parent_ref_id, ROUND(p_parent_qty_needed,0), p_history_fl, p_comments_fl
                   , p_weighted_avg_diff, p_ref_type
                   );
	END gm_op_sav_sheet_detail;

	/*************************************************************************
		* Purpose: Procedure used to load sales demand/forecast for selected period
		* p_type : D - Demand F - Forecast
		*************************************************************************/
	PROCEDURE gm_op_calc_period (
		p_demand_period    IN		t4020_demand_master.c4020_demand_period%TYPE
	  , p_inc_current_fl   IN		t4020_demand_master.c4020_include_current_fl%TYPE
	  , p_type			   IN		CHAR
	  , p_from_period	   OUT		DATE
	  , p_to_period 	   OUT		DATE
	)
	AS
--
	BEGIN
		--
		IF (p_type = 'D')
		THEN
			SELECT TRUNC (LAST_DAY (DECODE (p_inc_current_fl, NULL, ADD_MONTHS (SYSDATE, -1), SYSDATE)))
			  INTO p_to_period
			  FROM DUAL;

			--
			SELECT TRUNC (ADD_MONTHS (p_to_period, (p_demand_period * -1))) + 1
			  INTO p_from_period
			  FROM DUAL;
		ELSE
			SELECT TO_DATE (   '01/'
							|| TO_CHAR ((DECODE (p_inc_current_fl, NULL, SYSDATE, ADD_MONTHS (SYSDATE, +1))), 'MM/YYYY')
						  , 'DD/MM/YYYY'
						   )
			  INTO p_from_period
			  FROM DUAL;

			--
			SELECT LAST_DAY ((ADD_MONTHS (p_from_period, (p_demand_period - 1))))
			  INTO p_to_period
			  FROM DUAL;
		END IF;

		--
		--DBMS_OUTPUT.put_line ('From and To Date  ****' || p_from_period || ' - ' || p_to_period || ' - '
		--					  || p_demand_period
		--					 );
	END gm_op_calc_period;

	/*************************************************************************
		* Purpose: Procedure used to email
		*************************************************************************/
	PROCEDURE gm_op_send_email (
		p_demand_id    IN	t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_nm    IN	t4020_demand_master.c4020_demand_nm%TYPE
	  , p_period	   IN	VARCHAR2
	  , p_start_dt	   IN	DATE
	  , p_type		   IN	CHAR
	  , p_message	   IN	VARCHAR2
	  , p_request_by   IN	t4020_demand_master.c4020_created_by%TYPE
	)
	AS
		--
		v_msg		   VARCHAR2 (100):='' ;
		subject 	   VARCHAR2 (1000) := p_demand_nm || ' Order Planning Sheet Loaded Successfully	';
		mail_body	   VARCHAR2 (3000);
		crlf		   VARCHAR2 (2) := CHR (13) || CHR (10);
		p_end_dt	   DATE;
		to_mail 	   t906_rules.c906_rule_value%TYPE;
--
	BEGIN
		--
		--DBMS_OUTPUT.put_line ('Inside Email ****');
		p_end_dt	:= SYSDATE;
		
		IF p_message ='SHEET LOAD EXCLUDED'
		THEN
			subject := p_demand_nm || ' Order Planning Sheet Excluded Successfully';
		END IF;

		--
		-- Final log insert statement
		INSERT INTO t910_load_log_table
					(c910_load_log_id, c910_log_name, c910_state_dt, c910_end_dt, c910_status_details, c910_status_fl
					)
			 VALUES (s910_load_log.NEXTVAL, 'OP' || p_demand_nm, p_start_dt, p_end_dt, p_message, p_type
					);

		--
		--DBMS_OUTPUT.put_line ('From and To Date  ****');
		mail_body	:=
			   ' Order Planning Sheet Load Status'
			|| crlf
			|| ' Sheet Name	 :- '
			|| p_demand_nm
			|| crlf
			|| ' Period		 :- '
			|| p_period
			|| crlf
			|| ' START Time	:- '
			|| TO_CHAR (p_start_dt, 'dd/mm/yyyy hh24:mi:ss')
			|| crlf
			|| ' END Time :- '
			|| TO_CHAR (p_end_dt, 'dd/mm/yyyy hh24:mi:ss')
			|| crlf
			|| ' MESSAGE '
			|| p_message;
		--
		--DBMS_OUTPUT.put_line ('Before calling  gm_cm_mail_grp');

		--
		IF (p_type = 'E')
		THEN
			--
			subject 	:= p_demand_nm || ' Order Planning Sheet Loaded Error Contact IT';
			to_mail 	:= get_rule_value ('ORDERPLAN', 'EMAIL');
			gm_com_send_email_prc (to_mail, subject, mail_body);
		ELSE
			IF (p_request_by <> '30301')
			THEN
				--
				gm_pkg_cm_hierarchy.gm_cm_mail_grp (90735, p_demand_id, subject, mail_body, p_request_by);
			END IF;
		END IF;
--
	END gm_op_send_email;

	/***********************************************************************
	 *Purpose: Procedure used to execute load
	 ***********************************************************************/
	PROCEDURE gm_op_sav_load_detail (
		p_ref_id	   IN	t9302_execute_load.c9302_ref_id%TYPE
	  , p_ref_type	   IN	t9302_execute_load.c901_ref_type%TYPE
	  , p_request_by   IN	t9302_execute_load.c9302_request_by%TYPE
	)
	AS
		v_count 	   NUMBER;
		v_execute_load_id NUMBER;
	BEGIN
		SELECT COUNT (1)
		  INTO v_count
		  FROM t9302_execute_load t9302
		 WHERE t9302.c9302_ref_id = p_ref_id AND t9302.c901_ref_type = p_ref_type AND t9302.c9302_status IN (10, 20);

        -- below error condition is only applicable for Editbale sheet 
		IF (v_count <> 0 and p_ref_type <> 91952)
		THEN
			raise_application_error (-20923, '');
        ELSIF (v_count <> 0 )
        THEN 
             RETURN;
                
		END IF;

		SELECT s9302_execute_load.NEXTVAL
		  INTO v_execute_load_id
		  FROM DUAL;

		INSERT INTO t9302_execute_load
					(c9302_execute_load_id, c9302_ref_id, c901_ref_type, c9302_request_by, c9302_request_date
				   , c9302_status
					)
			 VALUES (v_execute_load_id, p_ref_id, p_ref_type, p_request_by, SYSDATE
				   , 10
					);
	END gm_op_sav_load_detail;

	/**********************************************************************
	*Purpose: Procedure used to execute load details and call "gm_op_ld_demand_main"
	* to load demand
	**********************************************************************/
	PROCEDURE gm_op_exec_job_detail
	AS
		
        v_ref_id	   t9302_execute_load.c9302_ref_id%TYPE;
		v_execute_load_id t9302_execute_load.c9302_execute_load_id%TYPE;
		v_request_by   t9302_execute_load.c9302_request_by%TYPE;
		v_ref_type	   t9302_execute_load.c901_ref_type%TYPE;
        --
        -- Cursor to load job in Que
        CURSOR cur_exec_job_que
        IS 
            SELECT c901_code_id
              FROM t901_code_lookup t901 
             WHERE t901.c901_code_grp  = 'JBSTU'
               AND t901.c901_active_fl = 1
          ORDER BY t901.c901_code_seq_no ;
         
        
		CURSOR cur_exec_job_detail(v_type t9302_execute_load.c901_ref_type%TYPE)
		IS
			  SELECT c9302_execute_load_id, c9302_ref_id, c901_ref_type, c9302_request_by
				FROM t9302_execute_load t9302
			   WHERE t9302.c9302_status = 10
                 AND t9302.c901_ref_type = v_type   
			ORDER BY c9302_execute_load_id;	
	BEGIN
		
		DELETE from MY_TEMP_KEY_VALUE;
        FOR var_job_detail IN cur_exec_job_que
        LOOP
        
    		FOR var_exec_job_detail IN cur_exec_job_detail(var_job_detail.c901_code_id)    
    		LOOP
    			BEGIN
	    			
	    			
    				v_execute_load_id := var_exec_job_detail.c9302_execute_load_id;
    				v_ref_id	:= var_exec_job_detail.c9302_ref_id;
    				v_request_by := var_exec_job_detail.c9302_request_by;
    				v_ref_type := var_exec_job_detail.c901_ref_type;
					
    				
    				UPDATE t9302_execute_load t9302
    				   SET t9302.c9302_status = 20
    					 , t9302.c9302_job_start_time = SYSDATE
    				 WHERE t9302.c9302_execute_load_id = v_execute_load_id;

    				--COMMIT;
    				IF v_ref_type = 4000123 THEN -- Multipart pull
    					-- When we pull for Multi-Part,we should not reload TTp Summary for each loop,as this is handled in gm_op_ld_ttp_summary procedure.
    					gm_pkg_oppr_sheet.gm_pull_multipartsheet(v_ref_id, v_request_by,'Y');
                        COMMIT;
    				ELSE
    					gm_pkg_oppr_ld_demand.gm_op_ld_demand_main (v_ref_id, v_request_by);
					END IF;

    				UPDATE t9302_execute_load t9302
    				   SET t9302.c9302_status = 40
    					 , t9302.c9302_job_end_time = SYSDATE
    				 WHERE t9302.c9302_execute_load_id = v_execute_load_id;
					
    				--For the Sheet that is getting reloaded ,we need to reload the Summary.
	    			INSERT INTO MY_TEMP_KEY_VALUE VALUES(v_ref_id,v_request_by,v_ref_type);
	    			
	    			
    				--COMMIT;
    			EXCEPTION
    				WHEN OTHERS
    				THEN
    					UPDATE t9302_execute_load t9302
    					   SET t9302.c9302_status = 30
    					 WHERE t9302.c9302_execute_load_id = v_execute_load_id;

    					COMMIT;
    			END;
    		END LOOP;
            --
        END LOOP;   
        
        --Saving Vendor Qty push Detail 
      	gm_pkg_oppr_ld_demand.gm_sav_vendor_qty_push_dtl();
        
      	gm_pkg_oppr_ld_demand.gm_op_ld_ttp_summary('Y');
      	 

      	
        COMMIT;
	END gm_op_exec_job_detail;
	
	
	/*
	 * This procedure will load all the TTP Summary for the reloaded Demand Sheet or the Multi-Part Pull.
	 */
	PROCEDURE gm_op_ld_ttp_summary(
		p_multi_part_fl		IN		VARCHAR2 DEFAULT NULL
	)
	AS
	v_cnt 				number;
	v_multi_part_cnt 	number;
	/*
		 * This Curosr will get the TTP Detail ID for the Demand Sheet that is reloaded or for the Multi-Part Sheet.
		 * We should ignore 30301 user ID as that will be used when loading the Month end GOP Job.
		 * 
		 */
		CURSOR cur_ttp_summary_load 
		IS
			    select t4040.C4052_TTP_DETAIL_ID TTP_DTL_ID,TEMP.MY_TEMP_TXN_KEY requested_by  
			    from MY_TEMP_KEY_value TEMP, T4040_DEMAND_SHEET t4040
		         WHERE t4040.C4020_DEMAND_MASTER_ID = TEMP.MY_TEMP_TXN_ID
		         AND TEMP.MY_TEMP_TXN_KEY <> '30301'
		         AND t4040.C4040_DEMAND_PERIOD_DT = TRUNC(SYSDATE,'Mon')
		         AND t4040.C4052_TTP_DETAIL_ID IS NOT NULL
		         AND TEMP.MY_TEMP_TXN_VALUE <> '4000123' -- Non  Multi-Part		         
		         UNION 		         
		         select t4040.C4052_TTP_DETAIL_ID TTP_DTL_ID ,TEMP.MY_TEMP_TXN_KEY requested_by
		         from MY_TEMP_KEY_value TEMP, T4040_DEMAND_SHEET t4040
		         WHERE t4040.C4040_DEMAND_SHEET_ID = TEMP.MY_TEMP_TXN_ID
		         AND t4040.C4040_DEMAND_PERIOD_DT = TRUNC(SYSDATE,'Mon')
		         AND TEMP.MY_TEMP_TXN_KEY <> '30301'
		         AND t4040.C4052_TTP_DETAIL_ID IS NOT NULL
		         AND TEMP.MY_TEMP_TXN_VALUE = '4000123'; -- Multi-Part 
	BEGIN
		
		select count(1) into v_cnt
		from MY_TEMP_KEY_value ;
		
		DBMS_OUTPUT.PUT_LINE('::;v_cnt::'||v_cnt);
		
		  FOR ttp_summary_load IN cur_ttp_summary_load   
    		LOOP
    			BEGIN
	    			DBMS_OUTPUT.PUT_LINE(' >>> TTP DTL ID:'||ttp_summary_load.TTP_DTL_ID);
	    			gm_pkg_oppr_ld_summary.gm_op_ld_summary_main(ttp_summary_load.TTP_DTL_ID,'',ttp_summary_load.requested_by);
	    			COMMIT;
	    			
	    	/*  PC-2950: 7_Multi Part part sync to TTP By Vendor
			 *  When Growth setup done for Multi Parts then the forecast sync is happening in to 
			 *  respective Multi Part sheet and also pushing to TTP By Vendor (Multi Part sheets)
			 */ 
	    		IF p_multi_part_fl = 'Y' THEN
	    			SELECT COUNT(1)
	    			  INTO v_multi_part_cnt
	    			  FROM TS4042_TTP_MULTIPART_MONTH
	    			 WHERE c4052_ttp_detail_id = ttp_summary_load.TTP_DTL_ID;
	    			 
	    			IF v_multi_part_cnt <> 0 THEN
	    			 	gm_pkg_oppr_ld_multipart_ttp.gm_sync_multipart_dtls(ttp_summary_load.TTP_DTL_ID,ttp_summary_load.requested_by);
	    				COMMIT;
	    			 END IF;
	    		END IF;
	    			
	    			
	    		EXCEPTION WHEN OTHERS THEN
	    		--When there is a problem in reloading the TTP Summary, rollback.
	    			ROLLBACK;
	    		END;
    		END LOOP;
        
        
    		
	END gm_op_ld_ttp_summary;
    
	/*************************************************************************
	 * Purpose: Procedure used to load demand Wieghted Avg and Avg
	 * for the selected period
	 *************************************************************************/
	PROCEDURE gm_op_ld_demand_calc_avg (
		p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_to_period 		IN	 DATE
	)
	AS
		v_demand_master_id  t4040_demand_sheet.C4020_DEMAND_MASTER_ID%TYPE;
		v_set_id			T4032_DEMAND_OVERRIDE.C4032_REF_ID%TYPE;
		v_pnum				T4032_DEMAND_OVERRIDE.C4032_REF_ID%TYPE;
		v_history_fl		VARCHAR2(1);
		v_ref_type			VARCHAR2(15);
		v_override_qty		T4032_DEMAND_OVERRIDE.C4032_QTY%TYPE;
		v_comments_fl		VARCHAR2(1);
		v_dmd_type			t4040_demand_sheet.C901_DEMAND_TYPE%TYPE;
		v_ref_id			t4042_demand_sheet_detail.c4042_ref_id%TYPE;
		
		--
		CURSOR demand_calc_cur
		IS
			SELECT	 c4042_ref_id, c205_part_number_id, c901_ref_type reftype, AVG (c4042_qty) sales_avg
				   , SUM (c4042_qty * val_index) / SUM (val_index) sales_weight_avg
				FROM (SELECT t4042.c205_part_number_id, t4042.c4042_ref_id,t4042.c901_ref_type,  t4042.c4042_period, t4042.c4042_qty
						   , ROW_NUMBER () OVER (PARTITION BY t4042.c4042_ref_id, t4042.c205_part_number_id ORDER BY t4042.c4042_period)
																											  val_index
						FROM my_temp_demand_sheet_detail t4042
					   WHERE t4042.c4040_demand_sheet_id = p_demand_sheet_id AND t4042.c901_type = 50560)
			GROUP BY c4042_ref_id, c205_part_number_id, c901_ref_type;
		
	 	-- Cursor to calculate the Demand Override.
		CURSOR overide_calc_cur
		IS
			SELECT t4032.C4032_REF_ID REF_ID  , C4032_QTY QTY ,  t4032.C901_REF_TYPE REF_TYPE        
			, t4032.C4032_COMMENTS_FL comments_fl
			FROM T4032_DEMAND_OVERRIDE t4032
			WHERE t4032.C4020_DEMAND_MASTER_ID  = v_demand_master_id
			AND t4032.C901_REF_TYPE IN (102960,102961) -- PART, SET
			AND t4032.C4032_VOID_FL IS NULL;
			
	BEGIN
		
		SELECT C4020_DEMAND_MASTER_ID , C901_DEMAND_TYPE INTO v_demand_master_id , v_dmd_type
		FROM t4040_demand_sheet 
		WHERE C4040_DEMAND_SHEET_ID = p_demand_sheet_id;
		
		-- load demand information
		FOR set_val IN demand_calc_cur
		LOOP
			-- Load Average Info
			gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , set_val.c4042_ref_id
                                                      , set_val.reftype  
													  , 50561
													  , p_to_period
													  , set_val.c205_part_number_id
													  , ROUND (set_val.sales_avg, 0)
													   );
			-- Load Weighted Avergae Info
			gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , set_val.c4042_ref_id
                                                      , set_val.reftype
													  , 50562
													  , p_to_period
													  , set_val.c205_part_number_id
													  , ROUND (set_val.sales_weight_avg, 0)
													   );
			BEGIN									   
				SELECT t4032.C4032_REF_ID , t4032.C4032_QTY , t4032.C4032_COMMENTS_FL
				INTO v_set_id, v_override_qty,    v_comments_fl   
				FROM T4032_DEMAND_OVERRIDE t4032
				WHERE t4032.C4020_DEMAND_MASTER_ID  = v_demand_master_id
				AND t4032.C901_REF_TYPE IN (102961)  --SET
				AND t4032.C4032_REF_ID = set_val.c4042_ref_id
				AND t4032.C4032_VOID_FL IS NULL;	
			EXCEPTION WHEN OTHERS
			THEN
				v_set_id :='';
				v_override_qty :='';
			END;
			
			IF  (v_set_id IS NOT NULL AND v_set_id = set_val.c4042_ref_id
				AND set_val.c205_part_number_id IS NOT NULL)
			THEN
				BEGIN
					SELECT  (t208.c208_set_qty * v_override_qty) 
					  INTO v_override_qty 
		              FROM t208_set_details t208
		             WHERE t208.c207_set_id = set_val.c4042_ref_id
			       	   AND t208.c205_part_number_id = set_val.c205_part_number_id
		               AND t208.c208_inset_fl = 'Y'
		               AND t208.c208_set_qty <> 0
		               AND t208.c208_void_fl IS NULL ;
	               
						-- Load Overridden Weighted Avg
						SELECT DECODE(v_override_qty,NULL,'','Y') INTO v_history_fl FROM DUAL;
						gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
																  , set_val.c4042_ref_id
			                                                      , set_val.reftype
																  , 4000344  --Overridden Weighted Avg1
																  , p_to_period
																  , set_val.c205_part_number_id -- Part
																  , ROUND (v_override_qty,0)
																  ,NULL,NULL,NULL
																  ,v_history_fl
																  ,v_comments_fl,ROUND (v_override_qty, 0)
																   );
				EXCEPTION WHEN OTHERS
				THEN	
					NULL;
				END ;
			END IF;
			
		--
		END LOOP;
		
		
		FOR override_val IN overide_calc_cur
		LOOP
			
			SELECT DECODE(override_val.REF_TYPE,102961,override_val.REF_ID,'') INTO v_set_id
			FROM DUAL;
			
			SELECT DECODE(override_val.REF_TYPE,102960,override_val.REF_ID,'') INTO v_pnum
			FROM DUAL;
			
			SELECT DECODE(override_val.qty,NULL,'','Y') INTO v_history_fl FROM DUAL;
			
			SELECT DECODE(v_set_id,NULL,'102661','102663') INTO v_ref_type FROM DUAL;
			
			SELECT DECODE(override_val.comments_fl,NULL,'','Y') INTO v_comments_fl FROM DUAL;
			
			SELECT NVL(v_set_id,'-9999') INTO v_ref_id FROM DUAL;
			
			-- If its a sales sheet, for the demand sheet and part id combination get the group id.
			IF v_dmd_type = '40020'	--Sales
			THEN
				BEGIN
					SELECT c4042_ref_id INTO v_ref_id
					FROM  my_temp_demand_sheet_detail 
					WHERE c4040_demand_sheet_id = p_demand_sheet_id
					AND   c205_part_number_id = v_pnum
					AND   ROWNUM = 1;
				EXCEPTION WHEN OTHERS
				THEN
					NULL;
				END;
			END IF;
			
			-- Load Overridden Weighted Avg
			gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , v_ref_id -- Set/Part/Group ID
                                                      , v_ref_type
													  , 4000344  --Overridden Weighted Avg1
													  , p_to_period
													  , v_pnum -- Part
													  , ROUND (override_val.qty,0)
													  ,NULL,NULL,NULL
													  ,v_history_fl
													  ,v_comments_fl,ROUND (override_val.qty, 0)
													   );
		END LOOP;													   
	--
	END gm_op_ld_demand_calc_avg;
    
    /*************************************************************************
	 * Purpose: Procedure used to load  forecast Avg [Sales. Consign, Rollup]
	 * for the selected period
	 *************************************************************************/
	PROCEDURE gm_op_ld_forecast_avg (
		p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_to_period 		IN	 DATE
	)
	AS
		--
		CURSOR forecast_calc_cur
		IS
			SELECT	 c4042_ref_id, c205_part_number_id, c901_ref_type reftype, AVG (c4042_qty) forecast_avg
				FROM (SELECT t4042.c205_part_number_id, t4042.c4042_ref_id, t4042.c901_ref_type, 
                             t4042.c4042_period, t4042.c4042_qty
						FROM my_temp_demand_sheet_detail t4042
					   WHERE t4042.c4040_demand_sheet_id = p_demand_sheet_id
						 AND t4042.c901_type = 50563
						 AND t4042.c4042_period < ADD_MONTHS (p_to_period, 4))
			GROUP BY c4042_ref_id, c205_part_number_id, c901_ref_type ;
			--only need calaulate the forecast avg by 4 months.
	BEGIN
		-- load demand information
		FOR set_val IN forecast_calc_cur
		LOOP
			-- Load Average Info
			gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , set_val.c4042_ref_id
													  , set_val.reftype
                                                      , 50564
													  , p_to_period
													  , set_val.c205_part_number_id
													  , ROUND (set_val.forecast_avg, 0)
													   );
		--
		END LOOP;
	--
	END gm_op_ld_forecast_avg;

	/*************************************************************************
	 * Purpose: Procedure used to load Variance %
	 * for the selected sheet
     * 50564 -- Forecast Avg , 50562 -- Demand Avg
	 *************************************************************************/
	PROCEDURE gm_op_ld_forecast_variance (
		p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_to_period 		IN	 DATE
	)
	AS
		--
		CURSOR forecast_calc_cur
		IS
			SELECT	 t4042.c205_part_number_id, t4042.c4042_ref_id, t4042.c901_ref_type reftype
				   , SUM (DECODE (t4042.c901_type, 50564, t4042.c4042_qty, 0)) forecastavg
                   , SUM (DECODE (t4042.c901_type, 50562, t4042.c4042_qty, 0)) demandavg
				FROM my_temp_demand_sheet_detail t4042
			   WHERE t4042.c4040_demand_sheet_id = p_demand_sheet_id AND t4042.c901_type IN (50562, 50564)
			GROUP BY t4042.c205_part_number_id, t4042.c4042_ref_id, t4042.c901_ref_type;
        --
		v_forecastavg	        NUMBER;
        v_calculate_variance    NUMBER;
        v_mainvalue             NUMBER;
        v_demandavg             NUMBER;
        v_divisor             NUMBER;
        --
	BEGIN
		-- load demand information
		FOR set_val IN forecast_calc_cur
		LOOP

			v_forecastavg := NVL(set_val.forecastavg,0); 
			v_demandavg := set_val.demandavg;
            
			IF (v_forecastavg <=0 AND v_demandavg <=0) 
            THEN
                v_calculate_variance := 0; 
            ELSE
            
                v_demandavg := set_val.demandavg;
            
                IF (v_demandavg < 0) 
                THEN
                    v_demandavg := 0;
                END IF;
                --
                v_mainvalue :=   v_forecastavg - v_demandavg;  
                
		--As the variance has to be calculated even if forecast is 0, we are doing this to avoid exception.
                SELECT DECODE(v_forecastavg,0,v_demandavg,v_forecastavg) INTO v_divisor  FROM DUAL;
                
                v_calculate_variance := ROUND((v_mainvalue * 100) / v_divisor,0); 
            END IF;
            -- Load Average Info
			gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , set_val.c4042_ref_id
                                                      , set_val.reftype  
													  , 50565
													  , p_to_period
													  , set_val.c205_part_number_id
													  , v_calculate_variance 
													   );
		--
		END LOOP;
	--
	END gm_op_ld_forecast_variance;

        
	/*************************************************************************
	 * Purpose: Procedure used to calculate forecast based on in parameter 
	 * common forecast model to be used for sales, FA, Inhouse
	 *************************************************************************/
	PROCEDURE gm_op_calc_forecast (
        p_growth_type	    IN 	t4031_growth_details.c901_ref_type%TYPE
       ,p_growth_value	    IN 	t4031_growth_details.c4031_value%TYPE
       ,p_weight_avg     	IN 	t4042_demand_sheet_detail.c4042_weighted_avg_diff_qty%TYPE
       ,p_primaryfl         IN  CHAR
       ,p_forecast_value 	OUT t4042_demand_sheet_detail.c4042_qty%TYPE
       ,p_weighted_avg_diff OUT t4042_demand_sheet_detail.c4042_weighted_avg_diff_qty%TYPE
       ,p_history_fl	    OUT	CHAR
	)
    AS
    --
        v_weight_avg  t4042_demand_sheet_detail.c4042_weighted_avg_diff_qty%TYPE;
        
    BEGIN
    --
        
        v_weight_avg  := p_weight_avg;       
        -- If weighted avg is less than zero change the weighted avg to zero 
        IF ( p_weight_avg < 0)
        THEN
             v_weight_avg := 0;
        END IF; 
        
        -- If no growth then default to Weighted Avergae 
         IF (p_growth_type IS NULL)
         THEN 
             p_forecast_value := v_weight_avg;
             
         -- If its a Unit forecast     
        ELSIF (p_growth_type = 20381)
        THEN
             p_forecast_value := p_growth_value ;
             p_history_fl  := 'Y';
             
             IF (p_primaryfl = 'N')
             THEN
                p_weighted_avg_diff := p_growth_value; 
             ELSE          
                p_weighted_avg_diff := p_growth_value - v_weight_avg;
             END IF;   
                  
                   
        -- If its an unit plus average     
        ELSIF (p_growth_type = 20382)
        THEN
        	 p_forecast_value := v_weight_avg + p_growth_value;
             
             -- If not primary only set the growth
             -- Average is not appliable even if its Average in growth  
             IF (p_primaryfl = 'N')
             THEN
                p_forecast_value := p_growth_value;
             END IF;
             p_history_fl  := 'Y';
            
            p_weighted_avg_diff := p_growth_value;   
        END IF; 
        
        -- If forecast is less than zero then override the forecast to zero
        IF ( p_forecast_value < 0)
        THEN
             p_forecast_value := 0;
        END IF;  
	--
	END gm_op_calc_forecast;

	/*************************************************************************
	 * Purpose: Procedure used to load rollup sheet information
     * to be in the Que to execute   
     * IF DIVISION 
     *      LOAD COMPANY
     * IF INTERMEDIATE 
     *      LOAD COMPANY, LOAD DVISION 
     * IF REGION
     *      LOAD COMPANY, LOAD DIVISION, LOAD INTERMEDIATE, LOAD ZONE 
	 *************************************************************************/
	PROCEDURE gm_op_sav_rollup_sheet_info (
        p_parent_demand_id  IN	 t4020_demand_master.c4020_parent_demand_master_id%TYPE
      , p_demand_type       IN	 t4020_demand_master.c901_demand_type%type        
      , p_company_id        IN	 t4020_demand_master.c901_company_id%type
      , p_level_id          IN   t4040_demand_sheet.c901_level_id%TYPE                  
      , p_level_value       IN	 t4040_demand_sheet.c901_level_value%TYPE  
      , p_request_by        IN	t4020_demand_master.c4020_created_by%TYPE              
	)
    AS
    --
        p_division          t4015_global_sheet_mapping.c901_level_value%TYPE := -1111111;
        p_intermediate      t4015_global_sheet_mapping.c901_level_value%TYPE := -1111111;
        p_zone              t4015_global_sheet_mapping.c901_level_value%TYPE := -1111111;
        p_dist_tye          v700_territory_mapping_detail.disttypeid%TYPE;
        v_lvl_val			t4015_global_sheet_mapping.c901_level_value%TYPE;
    --
    	CURSOR rollup_sheet_cur
		IS              
            SELECT t4020.c4020_demand_master_id, t4020.c4020_demand_nm
              FROM t4015_global_sheet_mapping t4015, t4020_demand_master t4020
             WHERE t4020.c4020_parent_demand_master_id = p_parent_demand_id
               AND t4020.c901_demand_type = p_demand_type
               AND t4020.c4015_global_sheet_map_id = t4015.c4015_global_sheet_map_id
               AND (t4015.c901_level_id, t4015.c901_level_value) IN (
                       SELECT t4015.c901_level_id, t4015.c901_level_value
                         FROM t4015_global_sheet_mapping t4015
                        WHERE (   (t4015.c901_level_id = 102580 AND t4015.c901_level_value = p_company_id )   --  Company level load for all sheet
                               OR (t4015.c901_level_id = 102581 AND t4015.c901_level_value = p_division )
                               OR (t4015.c901_level_id = 102582 AND t4015.c901_level_value = p_intermediate )
                               OR (t4015.c901_level_id = 102583 AND t4015.c901_level_value = p_zone)
                              )
                          AND t4015.c901_company_id = p_company_id);    
    BEGIN
    --
        -- 102581 -- Division 
        IF (p_level_id !=  102581) 
        THEN
           -- Load ous information 
           p_division := 100824;  
	           
        END IF;
        
        -- 102584 -- Region 
        IF (p_level_id =  102584) 
        THEN
            
            -- To get Zone informartion [EU , APAC, AFRICA]
            BEGIN 
                
                SELECT gp_id, v700.disttypeid
                  INTO p_zone,p_dist_tye 
                  FROM v700_territory_mapping_detail v700
                 WHERE v700.region_id = p_level_value AND ROWNUM = 1;
            --     
        	EXCEPTION
        		WHEN NO_DATA_FOUND
        		THEN                 
                   NULL;
            END;
            -- 102604 OUS DISTRIBUTOR and 102603 OUS DIRECT
            p_intermediate :=  102603;
            IF (p_dist_tye = 70105)
            THEN 
                p_intermediate :=  102604;
            END IF;
                         
        END IF;
        /* This block for Zone level editable */
        BEGIN
       		SELECT C906_RULE_ID into v_lvl_val FROM T906_RULES WHERE C906_RULE_GRP_ID='GOP_ROLLUP_EDIT'
			AND C906_VOID_FL IS NULL
			AND C906_RULE_ID=p_level_value;
			
				EXCEPTION WHEN OTHERS
				THEN
				v_lvl_val := NULL;
			END;
				
            
        IF (v_lvl_val IS NOT NULL) 
        -- for Northern europe zone
        THEN
           p_division := 100824; 
           p_intermediate :=  102603;
           -- 102603  OUS direct
	           
        END IF;
         
        FOR var_sheet_detail IN rollup_sheet_cur
		LOOP
        --    
            DBMS_OUTPUT.put_line ('Inslde loop  ' || var_sheet_detail.c4020_demand_nm || ' - ' || var_sheet_detail.c4020_demand_master_id);
            
            gm_pkg_oppr_ld_demand.gm_op_sav_load_detail (var_sheet_detail.c4020_demand_master_id 
		                                                                        , 91952, p_request_by);
	    --
        END LOOP;                   
    -- 
	END gm_op_sav_rollup_sheet_info;


  /*******************************************************
   * Description : Procedure to save demand sheet request
   * Author 	 : VPrasath
   *******************************************************/
	--
	PROCEDURE gm_sav_demand_sheet_request (
		p_demand_sheet_id	IN	 t4044_demand_sheet_request.c4040_demand_sheet_id%TYPE
	  , p_request_id		IN	 t4044_demand_sheet_request.c520_request_id%TYPE
	  , p_source_type		IN	 t4044_demand_sheet_request.c901_source_type%TYPE
	  , p_status_fl 		IN	 t4044_demand_sheet_request.c4044_lock_status_fl%TYPE
	)
	AS
	BEGIN
		INSERT INTO t4044_demand_sheet_request
					(c4044_demand_sheet_request_id, c4040_demand_sheet_id, c520_request_id, c901_source_type
				   , c4044_lock_status_fl
					)
			 VALUES (s4044_demand_sheet_request.NEXTVAL, p_demand_sheet_id, p_request_id, p_source_type
				   , p_status_fl
					);
	END gm_sav_demand_sheet_request;

  /*******************************************************
   * Description : Procedure to save demand sheet detail
   *        from temp table to main table  
   * Author 	 : Richard
   *******************************************************/
	--
PROCEDURE gm_ld_demand_sheet_tmp_to_main 
    AS
    BEGIN 
		INSERT INTO T4042_DEMAND_SHEET_DETAIL
					(c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id, c901_type, c4042_period
				   , c205_part_number_id, c4042_qty, c4042_updated_by, c4042_updated_date, c205_parent_part_num_id
				   , c4042_parent_ref_id, c4042_parent_qty_needed, c4042_history_fl, c4042_comments_fl
                   , c4042_weighted_avg_diff_qty, c901_ref_type
					)
			 SELECT c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id, c901_type, c4042_period
				   , c205_part_number_id, c4042_qty, c4042_updated_by, c4042_updated_date, c205_parent_part_num_id
				   , c4042_parent_ref_id, c4042_parent_qty_needed, c4042_history_fl, c4042_comments_fl
                   , c4042_weighted_avg_diff_qty, c901_ref_type 
               FROM  my_temp_demand_sheet_detail;      
    END gm_ld_demand_sheet_tmp_to_main;
   /*******************************************************
   * Description : Procedure to save TTP vendor sync  
   *               load with Open status so that job can take all
   * 			   the open ttp detail id to push TTP vendor qty
   * Author 	 : ppandiyan
   *******************************************************/
    PROCEDURE gm_sav_vendor_qty_push_dtl 
    AS
    	CURSOR cur_ttp_dtl
        IS
        SELECT T4040.C4052_TTP_DETAIL_ID TTP_DTL_ID,TEMP.MY_TEMP_TXN_KEY REQUESTED_BY FROM MY_TEMP_KEY_VALUE TEMP, T4040_DEMAND_SHEET T4040
		WHERE T4040.C4020_DEMAND_MASTER_ID = TEMP.MY_TEMP_TXN_ID
		AND TEMP.MY_TEMP_TXN_KEY <> '30301' 
		AND T4040.C4040_DEMAND_PERIOD_DT = TRUNC(CURRENT_DATE,'MON') 
		AND T4040.C4052_TTP_DETAIL_ID IS NOT NULL
		AND T4040.C4040_VOID_FL IS NULL;
		
        
    BEGIN 
    --insert into T9303_TTP_VENDOR_SYNC_LOAD with the status 'Open'
    	FOR ttp_detail IN cur_ttp_dtl
         LOOP
				--saving into T9303_TTP_VENDOR_SYNC_LOAD table
				gm_sav_vendor_sync_load(ttp_detail.TTP_DTL_ID,ttp_detail.REQUESTED_BY,109021) ;--109021 --'Open'
         	
     	END LOOP;
    END gm_sav_vendor_qty_push_dtl;
    
   /*******************************************************
   * Description : Procedure to push TTP vendor capa lock details (Job)
   * 			   and once job is succeeded then it will update status as 'Completed' 
   * 			   otherwise 'Failed'
   * Author 	 : ppandiyan
   *******************************************************/
    PROCEDURE gm_op_exec_vendor_qty_push 
    AS
    v_ttp_detail_id  T9303_TTP_VENDOR_SYNC_LOAD.C9303_REF_ID%TYPE;
    v_request_by     T9303_TTP_VENDOR_SYNC_LOAD.C9303_REQUEST_BY%TYPE;
    
    	CURSOR cur_ttp_dtl
        IS
		SELECT C9303_TTP_VENDOR_SYNC_LOAD_ID LOAD_ID,C9303_REF_ID TTP_DETAIL_ID,C9303_REQUEST_BY REQUESTED_BY
		FROM T9303_TTP_VENDOR_SYNC_LOAD 
		WHERE C901_STATUS=109021
		AND C9303_REF_ID IS NOT NULL
		AND C9303_VOID_FL IS NULL; --109021 -- open status

		
        
    BEGIN 
    
    	FOR ttp_vendor_sync IN cur_ttp_dtl
         LOOP
         BEGIN
            v_ttp_detail_id := ttp_vendor_sync.TTP_DETAIL_ID;
            v_request_by    := ttp_vendor_sync.REQUESTED_BY;
			-- to update the status
			--109021-Open,109022-In Progress,109023-Failed,109024-Completed
            gm_upd_ttp_vendor_sync_status(v_ttp_detail_id,v_request_by,109021,109022);--109022 --In Progress
         
				-- existing procedure call to push vendor qty
					gm_pkg_oppr_ld_ttp_capa_master_txn.gm_push_vendor_capa_lock_dtls(v_ttp_detail_id,v_request_by);
			-- to update the status
			gm_upd_ttp_vendor_sync_status(v_ttp_detail_id,v_request_by,109022,109024);--109024 --Completed

         	COMMIT;
         	
    		EXCEPTION
    			WHEN OTHERS
    			THEN
    					ROLLBACK;
    					-- to update the status as Failed
    					gm_upd_ttp_vendor_sync_status(v_ttp_detail_id,v_request_by,109021,109023);--109023 --Failed
    					COMMIT;
    			END;
         	
     	END LOOP;
    END gm_op_exec_vendor_qty_push;
    
      /*******************************************************
   * Description : Procedure to update TTP vendor qty push status 
   * Author 	 : ppandiyan
   *******************************************************/
    PROCEDURE gm_upd_ttp_vendor_sync_status(
    p_ttp_detail_id IN T9303_TTP_VENDOR_SYNC_LOAD.C9303_REF_ID%TYPE,
    p_request_by    IN T9303_TTP_VENDOR_SYNC_LOAD.C9303_REQUEST_BY%TYPE,
    p_old_status        IN T9303_TTP_VENDOR_SYNC_LOAD.C901_STATUS%TYPE,
    p_new_status        IN T9303_TTP_VENDOR_SYNC_LOAD.C901_STATUS%TYPE
    ) 
    AS   
    BEGIN 
    				UPDATE T9303_TTP_VENDOR_SYNC_LOAD 
			        SET C901_STATUS = p_new_status,
			         	C9303_REQUEST_BY = p_request_by,
			         	C9303_JOB_END_TIME = CURRENT_DATE
			        WHERE C9303_REF_ID = p_ttp_detail_id
			        AND C901_STATUS = p_old_status
			        AND C9303_VOID_FL IS NULL;

    END gm_upd_ttp_vendor_sync_status; 
    
   /*******************************************************
   * Description : Procedure to insert TTP vendor qty push status 
   * Author 	 : ppandiyan
   *******************************************************/
    PROCEDURE gm_sav_vendor_sync_load(
    p_ttp_detail_id IN T9303_TTP_VENDOR_SYNC_LOAD.C9303_REF_ID%TYPE,
    p_request_by    IN T9303_TTP_VENDOR_SYNC_LOAD.C9303_REQUEST_BY%TYPE,
    p_status        IN T9303_TTP_VENDOR_SYNC_LOAD.C901_STATUS%TYPE
    ) 
    AS   
    BEGIN 
    		INSERT INTO T9303_TTP_VENDOR_SYNC_LOAD 
         		(C9303_REF_ID,C9303_REQUEST_BY,C9303_REQUEST_DATE,C901_STATUS)
			VALUES
				(p_ttp_detail_id,p_request_by,CURRENT_DATE,p_status);--109021 --'Open'

    END gm_sav_vendor_sync_load; 
        
END gm_pkg_oppr_ld_demand;
/
