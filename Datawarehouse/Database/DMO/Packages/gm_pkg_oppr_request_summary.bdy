create or replace
PACKAGE BODY gm_pkg_oppr_request_summary
IS

/****************************************************
 *Prc to get GOP OUS  Request details
 ****************************************************/
PROCEDURE gm_fch_mrid_from_csnid (
        p_consignid      IN  t504_consignment.c504_consignment_id%TYPE,
        p_out_request_id OUT t520_request.c520_request_id%TYPE)
AS
BEGIN
     SELECT c520_request_id mrid
       INTO p_out_request_id
       FROM t504_consignment
      WHERE c504_consignment_id = p_consignid
        AND c504_void_fl       IS NULL;
END gm_fch_mrid_from_csnid;

/****************************************************
 *Prc to get GOP OUS Consign details
 ****************************************************/

PROCEDURE gm_fch_set_master (
    p_consignid      IN t504_consignment.c504_consignment_id%TYPE,
    p_out_request_id OUT  TYPES.cursor_type
   )
AS
BEGIN
    OPEN p_out_request_id
	FOR 
	SELECT  T505.C205_PART_NUMBER_ID PNUM 
	,GET_PARTNUM_DESC(T505.C205_PART_NUMBER_ID) PDESC
	,T505.C901_WAREHOUSE_TYPE WHTYPE
	,NVL(T505.C505_ITEM_QTY,0) IQTY
	,T505.C505_CONTROL_NUMBER CNUM
	,NVL(T505.C505_ITEM_PRICE,0) PRICE 
	,SETDT.CRITFL,SETDT.CRITQTY,SETDT.SETFL,SETDT.CTRITAG,SETDT.CRITTAG,NVL(SETDT.QTY, 0) QTY					
	,get_part_attribute_value(t505.c205_part_number_id, 92340) tagfl
	 FROM    T504_CONSIGNMENT T504 ,
		 T505_ITEM_CONSIGNMENT T505, 
		 (SELECT T208.C205_PART_NUMBER_ID, T208.C208_CRITICAL_FL CRITFL
		,DECODE(T208.C208_CRITICAL_QTY,NULL,T208.C208_SET_QTY,T208.C208_CRITICAL_QTY) CRITQTY
		,T208.C208_INSET_FL SETFL 
		,T208.C208_CRITICAL_TAG CTRITAG
		,GET_CODE_NAME(T208.C901_CRITICAL_TYPE) CRITTAG
		,T208.C208_SET_QTY QTY
		 FROM T208_SET_DETAILS T208, T504_CONSIGNMENT T504
		 WHERE T504.C504_CONSIGNMENT_ID = p_consignid  --input Consign ID
		 AND T208.C207_SET_ID = T504.C207_SET_ID 
		 AND T208.C208_VOID_FL IS NULL) SETDT
	
	 WHERE  T504.C504_CONSIGNMENT_ID = p_consignid    --input Consign ID
	 AND T504.C504_CONSIGNMENT_ID = T505.C504_CONSIGNMENT_ID
	 AND T505.C505_VOID_FL is NULL
	 AND T505.C205_PART_NUMBER_ID = SETDT.C205_PART_NUMBER_ID (+)
	 ORDER BY T505.C205_PART_NUMBER_ID;
 
END gm_fch_set_master;


/****************************************************
 **Prc to get GOP OUS Consign details
 ****************************************************/   
PROCEDURE gm_fch_cn_dtls(
    p_consignid IN t504_consignment.c504_consignment_id%TYPE,
    p_type      IN VARCHAR2,
    p_out_request_id OUT  TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_request_id
	FOR 
	SELECT C504_CONSIGNMENT_ID CID
    , C701_DISTRIBUTOR_ID DISTID
    , GET_OUS_CONSIGN_BILL_ADD(C504_CONSIGNMENT_ID) BILLADD1 
    , GET_CONSIGN_BILL_ADD (C504_CONSIGNMENT_ID) BILLADD
    , gm_pkg_cm_shipping_info.get_ship_add(C504_CONSIGNMENT_ID,decode(c504_type,9110,50186,50181)) SHIPADD
    , C504_LAST_UPDATED_DATE UDATE
    , GET_USER_NAME(C504_LAST_UPDATED_BY) NAME, C504_RETURN_DATE EDATE
    , GET_CODE_NAME(C504_DELIVERY_CARRIER) SCARR
    , GET_CODE_NAME(C504_DELIVERY_MODE) SMODE
    , C504_TRACKING_NUMBER TRACK
    , C504_STATUS_FL CONSIGNSTATUS
    , C504_SHIP_DATE SDATE
    , C504_type TYPE
    , NVL(C504_SHIP_COST,0)  SHIPCOST 
    , C504_COMMENTS ICTNOTES
    , get_compid_from_distid(C701_DISTRIBUTOR_ID) COMPANYID 
    , T504.C504_SHIP_TO SHIPTO 
    , GET_USER_NAME(p_type) LUSERNM , get_rule_value(p_type,'USERTITLE') UTITLE
    , t504.c1910_division_id division_id
 FROM T504_CONSIGNMENT T504
 WHERE C504_CONSIGNMENT_ID = p_consignid  --input Consign ID
 AND C504_VOID_FL IS NULL ;
 
END gm_fch_cn_dtls;


/****************************************************
 *Prc to get GOP OUS Returns details
 ****************************************************/   
PROCEDURE gm_fch_ous_ra_dtls(
    p_raid        IN T506_RETURNS.C506_RMA_ID%TYPE
    ,p_ra_dtl      OUT  TYPES.cursor_type
    ,p_ra_item_dtl OUT  TYPES.cursor_type
    ,p_ra_init     OUT  TYPES.cursor_type
   )
AS
v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
BEGIN
	 
	SELECT get_plantid_frm_cntx()
      INTO v_plant_id
      FROM DUAL;
      
    OPEN p_ra_dtl
	FOR 
	 SELECT A.C506_RMA_ID RAID, NVL(A.C701_DISTRIBUTOR_ID,A.C704_ACCOUNT_ID) DID 
	   , A.C506_STATUS_FL STATUS 
	   , A.C506_CREDIT_DATE CREDITEMAILDATE 
	   , DECODE(A.C701_DISTRIBUTOR_ID, NULL, GET_ACCOUNT_NAME(A.C704_ACCOUNT_ID), GET_DISTRIBUTOR_NAME(A.C701_DISTRIBUTOR_ID)) DNAME 
	   , A.C506_STATUS_FL STATUS, 
	   GET_USER_NAME(A.C506_LAST_UPDATED_BY) CREDITBY, 
	   A.C506_COMMENTS COMMENTS, A.C506_CREATED_DATE CDATE, 
	   A.C506_AC_CREDIT_DT CREDITDATE, 
	   GET_USER_NAME(A.C506_CREATED_BY) PER, GET_CODE_NAME(A.C506_TYPE) TYPE, 
	   A.C506_CREDIT_MEMO_ID MEMOID, T501.C503_INVOICE_ID CREDITINVID , 
	   GET_CODE_NAME(A.C506_REASON) REASON, A.C207_SET_ID SETID,
	   DECODE(A.C506_TYPE,3308,GET_OUS_CONSIGN_BILL_ADD(A.C506_REF_ID), GET_CREDIT_BILL_ADD (A.C506_RMA_ID)) ACCADD, 
	   A.C506_LAST_UPDATED_DATE UDATE,
	   T501.C501_COMMENTS ORDER_COMMENTS, 
	   C506_EXPECTED_DATE EDATE 
	   ,T501.C501_SHIP_COST SCOST 
	   ,A.C506_CREATED_DATE RETDATE 
	   ,A.C501_ORDER_ID ORDER_ID, T501.C901_ORDER_TYPE ORDTYPE, A.C506_TYPE RETTYPE  
	   ,nvl(get_rule_value (A.c701_distributor_id, 'CC-LIST-RETURNS'),get_rule_value ('DEFAULTSHIPEMAIL', 'EMAIL')) CCLIST
	   FROM T506_RETURNS A
	   ,T501_ORDER T501 
	   WHERE A.C506_RMA_ID =  p_raid
	   AND A.C506_RMA_ID = T501.C506_RMA_ID(+)
	   AND A.C5040_PLANT_ID = v_plant_id
	   AND A.C506_VOID_FL IS NULL 
	   AND (T501.c901_ext_country_id is NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes));-- To include ext country id for GH 

	OPEN p_ra_item_dtl
	FOR 
		SELECT -1 * SUM(C507_ITEM_QTY) QTY , -1 * SUM(C507_ITEM_QTY) IQTY
				, nvl(C507_ITEM_PRICE,0) PRICE , '-' CNUM 
				, DECODE (C507_STATUS_FL, 'R', 'Pending Return', 'C' , 'Return', 'W', 'Write-off','M','Missing') SFL   
				, C205_PART_NUMBER_ID PNUM , GET_PARTNUM_DESC(C205_PART_NUMBER_ID) PDESC   
		FROM  T507_RETURNS_ITEM   
		WHERE C506_RMA_ID IN ( SELECT  C506_RMA_ID FROM T506_returns T506 WHERE T506.C506_RMA_ID = p_raid	)
		AND C507_STATUS_FL IN ('C', 'W', 'R','M')   
		GROUP BY C507_ITEM_PRICE,C205_PART_NUMBER_ID,C507_STATUS_FL,C507_ITEM_PRICE  ORDER BY C205_PART_NUMBER_ID ;

	OPEN p_ra_init
	FOR 
		SELECT -1 * SUM(C507_ITEM_QTY) QTY , -1 * SUM(C507_ITEM_QTY) IQTY, nvl(C507_ITEM_PRICE,0) PRICE  
			 , '-' CNUM , DECODE (C507_STATUS_FL, 'R', 'Pending Return', 'C' , 'Return', 'W', 'Write-off','M','Missing') SFL   
			 , t507.C205_PART_NUMBER_ID PNUM , GET_PARTNUM_DESC(t507.C205_PART_NUMBER_ID) PDESC   
		FROM  T507_RETURNS_ITEM t507  , t205_part_number t205  
		WHERE C506_RMA_ID IN ( 
							SELECT  C506_RMA_ID FROM T506_returns T506 
							WHERE T506.C506_RMA_ID =  p_raid )
		  AND C507_STATUS_FL IN ('C', 'W', 'R','M')  
		  AND t507.c205_part_number_id  = t205.c205_part_number_id 
		  AND t205.c202_project_id NOT IN (
							SELECT c906_rule_id FROM t906_rules
							WHERE c906_rule_grp_id = 'EXVND' 
							AND c906_void_fl IS NULL )
 		GROUP BY C507_ITEM_PRICE,t507.C205_PART_NUMBER_ID ,C507_STATUS_FL,C507_ITEM_PRICE  ORDER BY t507.C205_PART_NUMBER_ID;

END gm_fch_ous_ra_dtls;

END gm_pkg_oppr_request_summary;
/