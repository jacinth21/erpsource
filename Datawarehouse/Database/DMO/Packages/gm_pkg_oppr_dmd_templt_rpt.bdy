/* Formatted on 2009/12/08 19:47 (Formatter Plus v4.8.0) */
-- @"C:\database\Packages\operations\purchasing\gm_pkg_oppr_dmd_templt_rpt.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_oppr_dmd_templt_rpt
IS
    --
    /*******************************************************
    * Purpose: This Procedure is used to fetch demand templates
    *******************************************************/
    --
PROCEDURE gm_fch_demand_template_list (
        p_out OUT TYPES.cursor_type)
AS
BEGIN
	OPEN p_out FOR
	     SELECT C4020_DEMAND_MASTER_ID CODEID, C4020_DEMAND_NM CODENM
	       FROM T4020_DEMAND_MASTER t4020
	      WHERE C4020_VOID_FL   IS NULL
	        AND C901_DEMAND_TYPE = 4000103 --Demand Sheet Template
	   ORDER BY UPPER(C4020_DEMAND_NM);
END gm_fch_demand_template_list;
--
/*******************************************************
* Purpose: This Procedure is used to fetch demand template details
*******************************************************/
--
PROCEDURE gm_fch_demand_template (
        p_demand_templt_id IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_detail          OUT TYPES.cursor_type)
AS
BEGIN
	OPEN p_detail FOR
	     SELECT c4020_demand_master_id DEMANDTEMPLTID, c4020_demand_nm DEMANDTEMPLTNM,
	        c4020_demand_period DEMANDPERIOD, c4020_forecast_period FORECASTPERIOD,
	        c4020_request_period SETBUILD, c4020_inactive_fl INACTIVEFLAG,
	        c901_company_id COMPANYID, c4020_primary_user DEMANDSHEETOWNER,
	        get_code_name (c901_company_id) COMPANYNM, get_user_name(c4020_primary_user) OWNERNM,
	        c901_sheet_status SHEETSTATUSID, get_code_name (c901_sheet_status) SHEETSTATUS
	       FROM t4020_demand_master
	      WHERE c4020_void_fl         IS NULL
	        AND c4020_demand_master_id = p_demand_templt_id;
END gm_fch_demand_template;
--
/*******************************************************
* Purpose: This Procedure is used to fetch demand template mapping details
*******************************************************/
--
PROCEDURE gm_fch_template_mapping (
        p_dmd_tmplt_id 			IN  t4020_demand_master.c4020_demand_master_id%TYPE,
        p_grp_selected 			OUT TYPES.cursor_type,
        p_grp_unselected 		OUT TYPES.cursor_type,
        p_cons_selected 		OUT TYPES.cursor_type,
        p_cons_unselected 		OUT TYPES.cursor_type,
        p_inhouse_selected 		OUT TYPES.cursor_type,
        p_inhouse_unselected 	OUT TYPES.cursor_type)
AS
BEGIN
    --Group
    gm_pkg_oppr_sheet.gm_fc_fch_demandgroupmap (p_dmd_tmplt_id, NULL, 40030,p_grp_selected,p_grp_unselected) ;
    -- Consignment Set
    gm_pkg_oppr_sheet.gm_fc_fch_demandsetmap (p_dmd_tmplt_id, NULL, 40031, p_cons_selected, p_cons_unselected) ;
    -- InHouse Set
    gm_pkg_oppr_sheet.gm_fc_fch_demandsetmap (p_dmd_tmplt_id, NULL, 4000109, p_inhouse_selected, p_inhouse_unselected);
END gm_fch_template_mapping;	
--
/*******************************************************
* Purpose: This Procedure is used to fetch demand sheets for the template
*******************************************************/
--
PROCEDURE gm_fch_demand_sheet_by_tmplt(
        p_demand_templt_id IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_detail          OUT TYPES.cursor_type)
AS
BEGIN
	OPEN p_detail FOR
	     SELECT c4020_demand_master_id DEMANDTEMPLTID, c4020_demand_nm DEMANDTEMPLTNM
	       FROM t4020_demand_master
	      WHERE c4020_void_fl         IS NULL
	      	AND C901_demand_type != 4000103 --Demand Sheet Template
	      	AND C4020_inactive_fl IS NULL
	        AND c4020_parent_demand_master_id = p_demand_templt_id
	   ORDER BY UPPER(c4020_demand_nm);
END gm_fch_demand_sheet_by_tmplt;

/********************************************************************
* Description : Function to get sheet access type
******************************************************************/
FUNCTION get_sheet_access_type (
        p_sheet_id IN t4020_demand_master.c4020_demand_master_id%TYPE)
    RETURN NUMBER
IS
    v_access_type NUMBER;
BEGIN
    BEGIN
         SELECT t4015.c901_access_type
           INTO v_access_type
           FROM t4020_demand_master t4020, t4015_global_sheet_mapping t4015
          WHERE t4020.c4020_void_fl            IS NULL
            AND t4020.c4015_global_sheet_map_id = t4015.c4015_global_sheet_map_id
            AND t4020.c4020_demand_master_id    = p_sheet_id
            AND t4020.c4020_inactive_fl IS NULL
            AND t4015.c4015_void_fl            IS NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN NULL;
    END;
    RETURN v_access_type;
END get_sheet_access_type;
END gm_pkg_oppr_dmd_templt_rpt;
/