create or replace PACKAGE BODY gm_pkg_oppr_ttp_finalize_setup
IS
 /*********************************************************
    * Description : This procedure is used to fetch the TTP SummaryApproval Screen. Based on TTPID,
    				Status,lock Period and category.
    *  Author:      mmuthusamy
    *********************************************************/
PROCEDURE gm_fch_approval_dtls(
            p_ttp_id              IN    T4050_TTP.C4050_TTP_ID%TYPE,
            p_status_id           IN    T4052_TTP_DETAIL.C901_STATUS%TYPE,
            p_lock_period         IN    VARCHAR2,
            p_category            IN    T4050_TTP.C901_TTP_CATEGORY%TYPE,
            p_out_ttp_summary     OUT 	TYPES.cursor_type
           )
     AS
     v_load_date DATE;
     v_date_fmt VARCHAR2 (12);
	 BEGIN
	-- to get date format from context

	 --To remove to_char functionality get date format using dual  
     SELECT to_date(p_lock_period || '/01' , 'MM/yyyy/dd'), get_compdtfmt_frm_cntx ()
     	INTO v_load_date, v_date_fmt
     FROM DUAL;
    --
    OPEN p_out_ttp_summary
	FOR
   SELECT t4050.c4050_ttp_id ttp_id,
      	t4052.c4052_ttp_detail_id ttp_detail_id,
        t4050.c4050_ttp_nm ttp_name,
        t901_ttp_cate.c901_code_nm ttp_category,
        t901_ttp_status.c901_code_nm ttp_status_name,
        t4052.c901_status ttp_status_id,
        t4052.c4052_forecast_period ttp_period,
        t101_approve.c101_user_f_name || ' ' || t101_approve.c101_user_l_name approved_by,
        TO_CHAR (t4052.c4052_approved_date, v_date_fmt ||' HH:MI AM') approved_date,
        TO_CHAR(t4052.c4052_ttp_link_date,v_date_fmt) load_date,
        t101_lock.c101_user_f_name || ' ' || t101_lock.c101_user_l_name locked_by,
        TO_CHAR (t4052.c4052_locked_dt, v_date_fmt ||' HH:MI AM') locked_date,
        t4056.c4056_order_qty total_ord_qty, t4056.c4056_order_cost total_ord_cost,
        gm_pkg_oppr_ttp_finalize_setup.get_ttp_summary_avg_qty (t4050.c4050_ttp_id, t4056.c901_level_id, t4056.c901_level_value, t4052.c4052_ttp_link_date) ttp_avg_ord_qty,
        gm_pkg_oppr_ttp_finalize_setup.get_ttp_summary_avg_amount (t4050.c4050_ttp_id, t4056.c901_level_id, t4056.c901_level_value, t4052.c4052_ttp_link_date) ttp_avg_ord_cost,
        t4056.c901_level_value level_value
     FROM t4050_ttp t4050, t4052_ttp_detail t4052,
      t4056_ttp_mon_summary t4056,
      t901_code_lookup t901_ttp_cate,
      t901_code_lookup t901_ttp_status,
      t101_user t101_approve,
      t101_user t101_lock
      WHERE t4050.c4050_ttp_id   = t4052.c4050_ttp_id
       AND t4052.c4052_ttp_detail_id = t4056.c4052_ttp_detail_id
       AND t901_ttp_cate.c901_code_id = t4050.c901_ttp_category
       AND t901_ttp_status.c901_code_id = t4052.c901_status
       AND t4052.c4052_approved_by = t101_approve.c101_user_id (+)
       AND t4052.c4052_locked_by = t101_lock.c101_user_id (+)
       AND t4052.c901_status = DECODE(p_status_id, 0, t4052.c901_status, p_status_id)
       AND t4056.c4056_load_date = v_load_date
       AND t4052.c4052_ttp_link_date = v_load_date
       AND t4050.c4050_ttp_id  = DECODE (p_ttp_id, 0, t4050.c4050_ttp_id, p_ttp_id)
       AND t4050.c901_ttp_category  = DECODE (p_category, 0, t4050.c901_ttp_category, p_category)
       AND t4050.c4050_void_fl   IS NULL
       AND t4052.c4052_void_fl IS NULL
       AND t4056.C4056_VOID_FL  IS NULL
       ORDER BY upper(ttp_name);
       
END gm_fch_approval_dtls;				 
				

   /********************************************************************
   *Description : This procedure is used to Save TTP   Approval
   * Author:      mmuthusamy 
   *******************************************************************/
PROCEDURE gm_sav_ttp_approval_dtls
								(p_input_str IN CLOB,
							     p_load_date IN VARCHAR2,
							     p_action IN VARCHAR2,
                                 p_user_id IN T4050_TTP.c4050_last_updated_by%TYPE,
                                 p_out_invalid_ttp_name OUT TYPES.cursor_type
                                 )
AS

v_ttp_dtls_id t4052_ttp_detail.c4052_ttp_detail_id%TYPE;
v_ttp_status t4052_ttp_detail.c901_status%TYPE;
v_load_date DATE;
v_in_load_date DATE;
v_forecast NUMBER;
v_cnt NUMBER;
v_status VARCHAR2 (20);

CURSOR ttp_dtls_cur
IS
 SELECT tmp.c205_part_number_id ttp_id  FROM my_t_part_list tmp, t4052_ttp_detail t4052
 WHERE tmp.c205_part_number_id = t4052.C4050_TTP_ID
 AND t4052.c901_status IN (SELECT *
										   FROM v_in_list)
 AND t4052.c4052_ttp_link_date =  v_in_load_date
 AND t4052.C4052_VOID_FL  IS NULL;
           
BEGIN
   --Based on paction change the status
	IF (p_action='Approve')THEN
		v_status := '50580, 50583'; -- Open, Approval Failed
	ELSIF (p_action='Lock')THEN
		v_status := '50582, 26240575'; -- Approved, Lock Failed
	END IF;
	
	--To remove to_char functionality get date format using dual   
  SELECT to_date(p_load_date || '/01' , 'MM/yyyy/dd')
  --, DECODE(p_action, 'Approve', 50580, 'Lock', 50582)
     INTO v_in_load_date
     --, v_status
     FROM DUAL;
     
		-- set the clobe value to context
		my_context.set_my_cloblist(p_input_str);
		my_context.set_my_inlist_ctx (v_status); 	
	-- validate the status (Open) - if other then open status - throws an error message.
	 OPEN p_out_invalid_ttp_name
	 FOR
	   SELECT
	            t4050.C4050_TTP_NM ttp_name            
      FROM t4052_ttp_detail t4052, T4050_TTP t4050
      WHERE t4050.C4050_TTP_ID = t4052.C4050_TTP_ID
      AND EXISTS
              (
           SELECT c205_part_number_id
           FROM my_t_part_list
           WHERE c205_part_number_id = t4050.C4050_TTP_ID
              )
       AND t4052.c901_status NOT IN (SELECT *
										   FROM v_in_list) 
       AND t4050.C4050_VOID_FL  IS NULL
       AND t4052.C4052_VOID_FL  IS NULL
       AND t4052.c4052_ttp_link_date =  v_in_load_date
      ORDER BY upper (t4050.C4050_TTP_NM) ;
	
       /*
        * For Fist time TTP details not available. below cursor need to check the ttp id and insert the records.
        * If record already available then, to update the status.        
        */
      
      
       FOR ttp_dtls IN ttp_dtls_cur
       LOOP
       
       v_ttp_dtls_id := NULL;
       v_ttp_status := NULL;
       --
        SELECT count (1)
        INTO v_cnt
        FROM t4052_ttp_detail t4052
        WHERE t4052.c4052_ttp_link_date = v_in_load_date
        AND t4052.C4050_TTP_ID                   = ttp_dtls.ttp_id
        AND t4052.C4052_VOID_FL                 IS NULL;
        -- For Fist time TTP details not available.Below count is used to  check whether record is available or not if not insert the new record.
       IF v_cnt = 0
       THEN
       --
       BEGIN
       gm_save_ttp_details (v_ttp_dtls_id, ttp_dtls.ttp_id, 50580, to_date(p_load_date || '/01' , 'MM/yyyy/dd'), 4, p_user_id);
       EXCEPTION WHEN OTHERS
    THEN
    v_forecast := NULL;
    v_ttp_status := NULL;
    END;
    
       
      END IF;--End IF Loop
      
      
    -- to get the ttp details id (based on load date / ttp id)
     BEGIN
     SELECT 
     	 t4052.C4052_TTP_DETAIL_ID, 
    	 t4052.c901_status 
     INTO v_ttp_dtls_id, v_ttp_status
     FROM t4052_ttp_detail t4052
     WHERE t4052.c4052_ttp_link_date  = v_in_load_date
     AND t4052.C4050_TTP_ID                   = ttp_dtls.ttp_id
     AND t4052.C4052_VOID_FL                 IS NULL;
     EXCEPTION WHEN OTHERS
     THEN
    	v_ttp_dtls_id := NULL;
    	v_ttp_status := NULL;
    END;
     --check whether status is open OR ttp details is not null.
     --Removed and condition beacause of if we approve failed status when request qty  is adjust. 
     
    If p_action='Approve' AND v_ttp_dtls_id IS NOT NULL THEN            
    	     gm_save_ttp_details (v_ttp_dtls_id, NULL, 50581, NULL, NULL, p_user_id);
    ELSIF p_action = 'Lock' AND v_ttp_dtls_id IS NOT NULL THEN
    	     gm_save_ttp_details (v_ttp_dtls_id, NULL, 26240573, NULL, NULL, p_user_id);
    END IF; 
    
   END LOOP;  --End If Loop     
END gm_sav_ttp_approval_dtls;				 
  
  /********************************************************************
    * Description : This procedure is used to process TTP   Approval
    * Author:       mmuthusamy
    *******************************************************************/    
PROCEDURE gm_process_ttp_approval
		             (p_load_date IN VARCHAR2,
		              p_user_id IN VARCHAR2)
	AS
	v_parent_demand_id VARCHAR2 (40);
	v_cnt NUMBER;
	v_ttp_dtls_id t4052_ttp_detail.c4052_ttp_detail_id%TYPE;
	v_demand_type NUMBER;
	--
	CURSOR parent_demand_dtls_cur
	IS
	SELECT 
			t4051.C4050_TTP_ID ttp_id, 
			t4051.C4020_DEMAND_MASTER_ID demand_master_id, 
			t4052.C4052_TTP_DETAIL_ID ttp_detail_id
   FROM     t4052_ttp_detail t4052, 
   		    T4051_TTP_DEMAND_MAPPING t4051
   WHERE    t4051.C4050_TTP_ID = t4052.C4050_TTP_ID
    AND     t4052.C901_STATUS   = 50581 -- In progress
    AND     t4052.C4052_VOID_FL IS NULL
    AND     TO_CHAR (t4052.c4052_ttp_link_date, 'MM/yyyy') = p_load_date;
	
    
    CURSOR demand_dtls_cur
    IS
    
    		SELECT c4040_demand_sheet_id demand_sheet_id
			  FROM t4040_demand_sheet
			 WHERE c4052_ttp_detail_id = v_ttp_dtls_id
			 -- 102580 (company level - sheet )
			 -- to handle the multipart - 40023
			  AND DECODE(v_demand_type, 40023, -999, c901_level_id) = DECODE(v_demand_type, 40023, -999, 102580)
			  AND c4040_void_fl IS NULL;
    
	BEGIN	
		--SELECT to_date(p_load_date || '/01' , 'MM/yyyy/dd')INTO v_load_date FROM DUAL;
		
		FOR parent_demand_dtls IN parent_demand_dtls_cur
		LOOP
			--
			v_parent_demand_id := parent_demand_dtls.demand_master_id;
			--
			BEGIN
				-- to get the demand tye (to handle the multi part)
				SELECT c901_demand_type
					INTO  v_demand_type
				 FROM t4020_demand_master
				 WHERE c4020_demand_master_id = v_parent_demand_id
				 AND c4020_void_fl IS NULL;
			EXCEPTION WHEN OTHERS
			THEN
				v_demand_type := NULL;
			END;
			 
			v_ttp_dtls_id := parent_demand_dtls.ttp_detail_id;
			--
			
			FOR  demand_dtls IN demand_dtls_cur
			LOOP
			--
				BEGIN
				gm_pkg_oppr_sheet.gm_fc_sav_approveds (demand_dtls.demand_sheet_id, p_user_id);
				EXCEPTION WHEN OTHERS
				THEN
				--50583 failed
				
				gm_save_ttp_details (v_ttp_dtls_id, NULL, 50583, NULL, NULL, p_user_id);
				
				END;
			END LOOP;
		--
		
			SELECT count(1)
			INTO v_cnt
			FROM  t4052_ttp_detail t4052 where C4052_TTP_DETAIL_ID = v_ttp_dtls_id
			AND C901_STATUS = 50581 ;-- IN progress
			
				IF v_cnt <> 0
				THEN  
			--50582 approved
				gm_save_ttp_details (v_ttp_dtls_id, parent_demand_dtls.ttp_id, 50582, to_date(p_load_date || '/01' , 'MM/yyyy/dd'), NULL, p_user_id);
				
				END IF;
					
		END LOOP;
		
END gm_process_ttp_approval;				 
				
  
   /*********************************************************
    * Description : This procedure is used to fetch TTP Failed Details
    * Author:       mmuthusamy
    *********************************************************/	
		
PROCEDURE gm_fch_ttp_failed_dtls
	(
	p_ttp_dtls_id       IN T4052_TTP_DETAIL.C4052_TTP_DETAIL_ID%TYPE,
    p_out_ttp_summary   OUT TYPES.cursor_type
	)
	AS
	v_ttp_id T4052_TTP_DETAIL.C4050_TTP_ID%TYPE;
	v_ttp_status NUMBER;
	v_load_date VARCHAR2(20);
	--
	v_demand_sheet_ids VARCHAR2(100);
	v_demand_master_id T4051_TTP_DEMAND_MAPPING.C4020_DEMAND_MASTER_ID%TYPE;
	v_date_fmt VARCHAR2 (20);
	BEGIN
		
	   SELECT get_compdtfmt_frm_cntx ()
       INTO v_date_fmt
       FROM dual; 
		
		-- get ttp and load information.
			 SELECT t4052.C4050_TTP_ID,
  					t4052.C901_STATUS ,
  					TO_CHAR(t4052.c4052_ttp_link_date, 'MM/yyyy') ,
  					t4051.C4020_DEMAND_MASTER_ID
				INTO v_ttp_id,
  					 v_ttp_status,
                     v_load_date ,
                     v_demand_master_id
				FROM T4052_TTP_DETAIL t4052,
  					 T4051_TTP_DEMAND_MAPPING t4051
                WHERE t4052.C4052_TTP_DETAIL_ID = p_ttp_dtls_id--pass parameter
                  AND t4051.C4050_TTP_ID        = t4052.C4050_TTP_ID
                  AND C4052_VOID_FL  IS NULL;
	--
		
            delete from my_temp_list;
    
    -- to get all the demand sheet id
    INSERT INTO my_temp_list(my_temp_txn_id)
    
          SELECT  
         		 t4040.C4040_DEMAND_SHEET_ID
  			FROM T4020_DEMAND_MASTER t4020,
  				 T4040_DEMAND_SHEET t4040
           WHERE t4020.C4020_PARENT_DEMAND_MASTER_ID = v_demand_master_id
             AND t4020.C4020_DEMAND_MASTER_ID = t4040.C4020_DEMAND_MASTER_ID
             AND t4020.C4015_GLOBAL_SHEET_MAP_ID     = 1
             AND TO_CHAR (t4040.C4040_LOAD_DT, 'MM/yyyy') = v_load_date
             AND C4020_VOID_FL                IS NULL
             AND t4020.C901_DEMAND_TYPE=40021; --Consignment 
     OPEN  p_out_ttp_summary
     FOR
        SELECT 
        t4043.c4030_grth_ref_id ref_id ,
        NVL(trim(get_set_name (t4043.c4030_grth_ref_id)),get_partnum_desc (t4043.c4030_grth_ref_id)) vname , 
        TO_CHAR (t4043.c4043_start_date, 'Mon-YY') required_month ,
        TO_CHAR(TRUNC(t4043.c4043_start_date,'MM'),'MM/DD/YYYY') req_month_firstday , 
        TO_CHAR(last_day(t4043.c4043_start_date),'MM/DD/YYYY') req_month_lastday , NVL (t4043.c4043_value, 0) growth_qty , 
        gm_pkg_oppr_sheet_summary.get_ds_growth_request_count (t4020.c4020_demand_master_id ,
        t4043.c4030_grth_ref_id , 
        t4043.c901_grth_ref_type , 
        t4043.c4043_start_date ,t4040.C4040_DEMAND_SHEET_ID ) request_qty , 
        t4043.c901_grth_ref_type ref_type, 
        t4040.c4020_demand_master_id dmd_mast_id, 
        t4020.C4020_DEMAND_NM dmd_mast_nm ,
        t4015.c901_access_type accesstype,
        t4040.C4040_DEMAND_SHEET_ID sheet_id
		FROM t4040_demand_sheet t4040 , t4043_demand_sheet_growth t4043 , t4020_demand_master t4020 , t4015_global_sheet_mapping t4015
		WHERE t4040.c4040_demand_sheet_id IN (select my_temp_txn_id from my_temp_list)
		AND t4040.c4040_demand_sheet_id    = t4043.c4040_demand_sheet_id
		AND t4040.c4020_demand_master_id   = t4020.c4020_demand_master_id
		AND t4043.c4030_grth_ref_id        = NVL(NULL, t4043.c4030_grth_ref_id)
		AND t4020.c4015_global_sheet_map_id    = t4015.c4015_global_sheet_map_id
		AND SIGN (NVL (t4043.c4043_value, 0)) <= DECODE (t4043.c901_ref_type , 20381, SIGN (NVL (t4043.c4043_value, 0)) ,SIGN (NVL (t4043.c4043_value, 0) - 1))
		AND t4043.c4043_start_date <= ADD_MONTHS (t4040.c4040_demand_period_dt , DECODE (t4040.c4040_request_period, 0,  -1, (t4040.c4040_request_period - 1))) 
		AND t4015.C4015_VOID_FL IS NULL
		AND t4020.C4020_VOID_FL IS NULL
        AND t4040.C4040_VOID_FL IS NULL
     ORDER BY t4043.c901_grth_ref_type, t4043.c4030_grth_ref_id;
     
     
END gm_fch_ttp_failed_dtls;				 
				
		
  /*********************************************************
    * Description : This procedure is used to Save TTP Failed Details
    * Author:       mmuthusamy
    *********************************************************/		 	 
PROCEDURE gm_save_ttp_details (
		p_ttp_dtls_id IN OUT t4052_ttp_detail.c4052_ttp_detail_id%TYPE,
		p_ttp_id IN t4052_ttp_detail.c4050_ttp_id%TYPE,
		p_status IN t4052_ttp_detail.c901_status%TYPE,
		p_load_date IN t4052_ttp_detail.c4052_load_date%TYPE,
		p_forecast IN t4052_ttp_detail.c4052_forecast_period%TYPE,
		p_user_id IN t4052_ttp_detail.c4052_created_by%TYPE
		)
		AS
		v_ttp_detail_id t4052_ttp_detail.c4052_ttp_detail_id%TYPE;
		v_demand_sheet_status NUMBER := 50551;
		BEGIN			
			UPDATE t4052_ttp_detail
			set 
			c901_status = p_status,
			c4052_approved_by = DECODE(p_status, 50582, p_user_id, c4052_approved_by),
			c4052_approved_date = DECODE(p_status, 50582, CURRENT_DATE, c4052_approved_date),
			c4052_locked_by = DECODE(p_status, 26240574, p_user_id, c4052_locked_by),
			c4052_locked_dt = DECODE(p_status, 26240574, CURRENT_DATE, c4052_locked_dt),
			c4052_last_updated_by = p_user_id,
			c4052_last_updated_date = CURRENT_DATE
			where 
			c4052_ttp_detail_id = p_ttp_dtls_id;
			
			IF (SQL%ROWCOUNT = 0) THEN
               SELECT s4052_ttp_detail.NEXTVAL
			  INTO v_ttp_detail_id
			  FROM DUAL;
			
			-- 1. Create a new entry onto t4052_ttp_detail
			INSERT INTO t4052_ttp_detail
						(
						c4052_ttp_detail_id, 
						c4050_ttp_id, 
						c4052_ttp_link_date, 
						c901_status, 
						c4052_created_by,
					    c4052_created_date, 
					    c4052_forecast_period, 
					    c4052_load_date
						)
				 VALUES (
				 		v_ttp_detail_id, 
				 		p_ttp_id, 
				 		p_load_date, 
				 		p_status, 
				 		p_user_id,
					    CURRENT_DATE, 
					    p_forecast, 
					    p_load_date
						);	
				    UPDATE t4040_demand_sheet t4040
                    SET t4040.c4052_ttp_detail_id = v_ttp_detail_id ,
                        t4040.c4040_last_updated_by    = p_user_id ,
                        t4040.c4040_last_updated_date  = CURRENT_DATE
                        WHERE t4040.c4052_ttp_detail_id IS NULL
                        AND t4040.c4040_demand_sheet_id IN
                      (SELECT t4040.c4040_demand_sheet_id  FROM t4020_demand_master t4020, t4040_demand_sheet t4040
                        WHERE t4020.c4020_demand_master_id = t4040.c4020_demand_master_id
                        AND c4020_parent_demand_master_id= ( select C4020_DEMAND_MASTER_ID from t4051_ttp_demand_mapping where C4050_TTP_ID=p_ttp_id) -- variable79
                        AND  C4040_DEMAND_PERIOD_DT=  p_load_date
                         AND t4020.C4020_VOID_FL IS NULL
                         AND t4040.C4040_VOID_FL IS NULL
                         );
				END IF;	
			-- to update the ttp details id to Demand sheet
			-- 2. Update the v_ttp_detail_id created into t4040_demand_sheet for all the selected demand sheets
		     IF (p_status = 50582) THEN
				    UPDATE t4040_demand_sheet t4040
                    SET 
                        t4040.c901_status = v_demand_sheet_status,
                        t4040.c4040_last_updated_by    = p_user_id ,
                        t4040.c4040_last_updated_date  = CURRENT_DATE
                        WHERE t4040.c4052_ttp_detail_id IS NULL
                        AND t4040.c4040_demand_sheet_id IN
                      (SELECT t4040.c4040_demand_sheet_id  FROM t4020_demand_master t4020, t4040_demand_sheet t4040
                        WHERE t4020.c4020_demand_master_id = t4040.c4020_demand_master_id
                        AND c4020_parent_demand_master_id= ( select C4020_DEMAND_MASTER_ID from t4051_ttp_demand_mapping where C4050_TTP_ID=p_ttp_id) -- variable79
                        AND  C4040_DEMAND_PERIOD_DT=  p_load_date
                         AND t4020.C4020_VOID_FL IS NULL
                         AND t4040.C4040_VOID_FL IS NULL
                         );         
			END IF;
			
END gm_save_ttp_details;	

--    /********************************************************************
--    * Description :
--    * Author:       
--    *******************************************************************/       
--    PROCEDURE gm_fch_ttp_lock_in_progress_dtls
--			(p_lock_date IN VARCHAR2,
--		     p_out_lock_details   OUT TYPES.cursor_type)
--	AS
--	BEGIN
--		
--	END gm_fch_ttp_lock_in_progress_dtls;		     

	 /*********************************************************
	* Description : This procedure is used to update TTP details status
	* Author:       mmuthusamy
	*********************************************************/
	PROCEDURE gm_upd_ttp_details_status (
	        p_ttp_id  IN t4052_ttp_detail.c4050_ttp_id%TYPE,
	        p_status  IN t4052_ttp_detail.c901_status%TYPE,
	        p_user_id IN t4052_ttp_detail.c4052_created_by%TYPE)
	AS
	    v_ttp_detail_id t4052_ttp_detail.c4052_ttp_detail_id%TYPE;
	    v_demand_status NUMBER;
	    v_cnt NUMBER := 0;
	    v_demandsheet_ids VARCHAR2 (4000) ;
	BEGIN
		
		-- To set the demand status
	    -- 50582 Approved, 26240574 - Locked (TTP status)
	    -- 50551 Approved, 50552 - Locked (Demand Status)
	    
	    BEGIN
	        -- Fetch TTP details information
	         SELECT c4052_ttp_detail_id, DECODE (p_status, 50582, 50551, 26240574, 50552, NULL)
	           INTO v_ttp_detail_id, v_demand_status
	           FROM t4052_ttp_detail
	          WHERE c4050_ttp_id                             = p_ttp_id
	            AND c4052_ttp_link_date = TRUNC(CURRENT_DATE,'month')
	            AND c4052_void_fl     IS NULL;
	    EXCEPTION
	    WHEN OTHERS THEN
	        v_ttp_detail_id := NULL;
	    END;
	    
	    -- to validate all the sheet are approved/locked
	    IF v_demand_status IS NOT NULL
	    THEN
	    	-- to get the demand sheet ids
	    	v_demandsheet_ids := gm_pkg_oppr_sheet.get_demand_sheet_ids (v_ttp_detail_id);
	    	
	    	-- to setting the demand sheet ids to context
	    	
	    	my_context.set_my_inlist_ctx (v_demandsheet_ids); 	
	    	
		    SELECT count (1)
		    	INTO v_cnt
			 FROM t4040_demand_sheet t4040, v_in_list tmp
			  WHERE c4040_demand_sheet_id = tmp.token
			  	AND c901_status <> v_demand_status 
			    AND c4040_void_fl      IS NULL;
	    END IF;
	    
	    IF v_ttp_detail_id IS NOT NULL AND v_cnt = 0 THEN
	        gm_pkg_oppr_ttp_finalize_setup.gm_save_ttp_details (v_ttp_detail_id, p_ttp_id, p_status, NULL, NULL, p_user_id) ;
	    END IF;
	    
	    
	END gm_upd_ttp_details_status;
	
	
	/*********************************************************
	* Description : This procedure is used to fetch the TTP lock information.
	* Author:       mmuthusamy
	*********************************************************/
	
	PROCEDURE gm_fch_ttp_details_lock_info (
	        p_ttp_dtls_id IN t4052_ttp_detail.c4052_ttp_detail_id%TYPE,
	        p_out_part_str OUT CLOB,
	        p_out_ttp_lock_info OUT TYPES.cursor_type)
	AS
	    v_inventory_lock_id t4040_demand_sheet.c250_inventory_lock_id%TYPE;
	    v_demandsheet_ids VARCHAR2 (4000) ;
	    v_level_value t4055_ttp_summary.c901_level_value%TYPE;
	    v_level_id t4055_ttp_summary.c901_level_id%TYPE;
	    
	    CURSOR ttp_lock_str_cur
	    IS
	         SELECT t4055.c205_part_number_id pnum, NVL (t4055.C4055_TOTAL_ORDER_QTY, 0) qty, t4055.c205_part_number_id ||
	            '^'|| NVL (t4055.C4055_TOTAL_ORDER_QTY, 0) ||'|' finalVal, t4055.C4052_TTP_DETAIL_ID ttp_detail_id
	           FROM T4055_TTP_SUMMARY t4055
	          WHERE t4055.C4052_TTP_DETAIL_ID = p_ttp_dtls_id
	          	-- to handle the multi-part (level value, level id always blank - demand sheet)
	          	-- 100800	Globus Medical Inc (default values)
				-- 102580	Company  (default values)
				
	            AND NVL(t4055.c901_level_value, 100800)    = NVL(v_level_value, 100800)
	            AND NVL(t4055.c901_level_id, 102580) = NVL(v_level_id, 102580)

	            -- In exsiting Lock and Gen - we are avoiding the GMxxxx parts.
	            -- Same way we are getting only '.' parts and for the order qty string.
	            
	            --AND t4055.c205_part_number_id LIKE '%.%'
	            -- above line commented for handling stelkast parts PMT-45727
	            AND t4055.c4055_void_fl                  IS NULL;
	            
	BEGIN
		
	    --1. to get the demand sheet ids
	    v_demandsheet_ids := gm_pkg_oppr_sheet.get_demand_sheet_ids (p_ttp_dtls_id) ;
	    
	    -- 1.1 If Multi parts then demand sheet ids should be empty.
	    IF v_demandsheet_ids IS NULL
	    THEN
	    	-- based on TTP details id to get the demand sheet id (only one sheet id)
	    	
		    BEGIN
			    
			    SELECT c4040_demand_sheet_id INTO v_demandsheet_ids
			   FROM t4040_demand_sheet
			  WHERE c4052_ttp_detail_id = p_ttp_dtls_id
			  	AND c901_demand_type = 40023 -- Multi Part
			    AND c4040_void_fl      IS NULL;
			    
		    EXCEPTION
		    WHEN OTHERS THEN
		    	v_demandsheet_ids := NULL;
		    END;
		    --
	    END IF;
	    
	    --2. set the values to context
	    my_context.set_my_inlist_ctx (v_demandsheet_ids) ;
	    
	    --3. get the inventory information
	    BEGIN
	        SELECT DISTINCT c250_inventory_lock_id, c901_level_value
	        	, c901_level_id
	           INTO v_inventory_lock_id, v_level_value
	           	, v_level_id
	            --INTO v_level_value
	           FROM t4040_demand_sheet
	          WHERE c4052_ttp_detail_id    = p_ttp_dtls_id
	            AND c4040_demand_sheet_id IN
	            (
	                 SELECT * FROM v_in_list
	            )
	            
	            -- Based on T4052 status we are driven, TTP summary. In case if status not updated properly in T4052 then, we need to avoid the Lock and generate.
	            
	            AND c901_status = 50551 -- Only fetch Approved records
	            AND c4040_void_fl IS NULL;
	    EXCEPTION
	    WHEN OTHERS THEN
	        v_inventory_lock_id := NULL;
	    END;
	    
	    --4. set the input string
	    
	    FOR ttp_lock_str IN ttp_lock_str_cur
	    LOOP
	        p_out_part_str := p_out_part_str || ttp_lock_str.finalVal;
	    END LOOP;
	    
	    --5. Get the required field from ttp details.
	    
	    OPEN p_out_ttp_lock_info FOR
	    SELECT t4052.c4052_ttp_detail_id dmttpdetailid, t4052.c4050_ttp_id ttpid,
	    	v_demandsheet_ids demandsheetids,
	    	NVL (t4052.c4052_forecast_period, get_rule_value ('FORECASTMONTHS', 'ORDERPLANNING')) ttpforecastmonths,
	    	v_inventory_lock_id inventoryid
	    FROM t4052_ttp_detail t4052
	    WHERE t4052.c4052_ttp_detail_id = p_ttp_dtls_id
	    AND t4052.c4052_void_fl  IS NULL;
	    
	END gm_fch_ttp_details_lock_info;	

	/*******************************************************
    * Description : This function used to get the TTP summary 3 month avg qty details
    * Author   : Mani
    *******************************************************/

		FUNCTION get_ttp_summary_avg_qty (
        p_ttp_id IN t4052_ttp_detail.c4050_ttp_id%TYPE,
        p_level_id IN t4056_ttp_mon_summary.c901_level_id%TYPE, 
        p_level_value IN t4056_ttp_mon_summary.c901_level_value%TYPE,
        p_load_date IN t4056_ttp_mon_summary.c4056_load_date%TYPE)
    RETURN NUMBER
    
IS
    v_avg_qty t4056_ttp_mon_summary.c4056_order_qty%TYPE;
BEGIN
    -- PMT-41517 - Formatting 3 Month Avg qty Column value into two decimal points in TTP Summary Approval report
    SELECT ROUND(sum(NVL(c4056_order_qty,0)/3),2)
     INTO v_avg_qty
    FROM t4056_ttp_mon_summary t4056, t4052_ttp_detail t4052
    WHERE t4052.c4052_ttp_detail_id = t4056.c4052_ttp_detail_id
    AND t4052.c4050_ttp_id = p_ttp_id
    AND t4056.c901_level_id = p_level_id
    AND t4056.c901_level_value = p_level_value
    AND t4052.c4052_ttp_link_date BETWEEN add_months (TRUNC (p_load_date, 'MM'), -3) AND add_months (TRUNC (p_load_date, 'MM'), -1)
    AND t4052.c4052_void_fl IS NULL
    AND t4056.c4056_void_fl IS NULL;
        
        RETURN NVL(v_avg_qty, 0);
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN 0;
END get_ttp_summary_avg_qty;



    /*******************************************************
    * Description : This function used to get the TTP summary 3 month avg order amount details
    * Author   : Mani
    *******************************************************/

		 FUNCTION get_ttp_summary_avg_amount (
        p_ttp_id IN t4052_ttp_detail.c4050_ttp_id%TYPE,
        p_level_id IN t4056_ttp_mon_summary.c901_level_id%TYPE, 
        p_level_value IN t4056_ttp_mon_summary.c901_level_value%TYPE,
        p_load_date IN t4056_ttp_mon_summary.c4056_load_date%TYPE)
    RETURN NUMBER
    
IS
    v_avg_cost  t4056_ttp_mon_summary.c4056_order_cost%TYPE;
BEGIN
    -- PMT-41517 - Formatting 3 Month Avg qty Column value into two decimal points in TTP Summary Approval report
      SELECT ROUND(sum(NVL(c4056_order_cost,0)/3),2)
       INTO v_avg_cost
       FROM t4056_ttp_mon_summary t4056, t4052_ttp_detail t4052
      WHERE t4052.c4052_ttp_detail_id = t4056.c4052_ttp_detail_id
      AND t4052.c4050_ttp_id   = p_ttp_id
      AND t4056.c901_level_id = p_level_id
      AND t4056.c901_level_value = p_level_value
    AND t4052.c4052_ttp_link_date BETWEEN add_months (TRUNC (p_load_date, 'MM'), - 3) AND add_months (TRUNC (p_load_date, 'MM'), -1)
    AND t4052.c4052_void_fl IS NULL
        AND t4056.c4056_void_fl      IS NULL;
        
        RETURN NVL(v_avg_cost, 0);
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN 0;
END get_ttp_summary_avg_amount;

END gm_pkg_oppr_ttp_finalize_setup;
/