/* Formatted on 2009/12/08 19:47 (Formatter Plus v4.8.0) */
-- @"C:\database\Packages\operations\purchasing\gm_pkg_oppr_global_mapping_rpt.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_oppr_global_mapping_rpt
IS
    --
    /*******************************************************
    * Purpose: This Procedure is used to fetch the Global Mapping details
    *******************************************************/
    --
PROCEDURE gm_fch_ds_global_mapping_dtl (
        p_company_id   IN t4015_global_sheet_mapping.c901_company_id%TYPE,
        p_level_id     IN t4015_global_sheet_mapping.c901_level_id%TYPE,
        p_value_id     IN t4015_global_sheet_mapping.c901_level_value%TYPE,
        p_access_id    IN t4015_global_sheet_mapping.c901_access_type%TYPE,
        p_inventory_id IN t4016_global_inventory_mapping.c901_level_value%TYPE,
        p_out_cur 	  OUT TYPES.cursor_type)
AS
BEGIN
	
    OPEN p_out_cur 
     FOR
     SELECT t4015.c4015_global_sheet_map_id glblshtmapid, t4015.c901_company_id companyid,get_code_name (t4015.c901_company_id) company
          , t4015.c901_level_id levelid, get_code_name (t4015.c901_level_id) levelnm, t4015.c901_level_value valueid
          , DECODE(t4015.c901_level_id,102584,GET_RULE_VALUE(t4015.c901_level_value,'REG_SHRT_CODE'),get_code_name(t4015.c901_level_value)) lvlvalue
          , t4015.c901_access_type accesstypeid, get_code_name (t4015.c901_access_type) accesstype, get_code_name ( t4016.c901_level_value) inventory
       FROM t4015_global_sheet_mapping t4015, t4016_global_inventory_mapping t4016
      WHERE t4016.c4016_global_inventory_map_id = t4015.c4016_global_inventory_map_id
        AND t4015.c901_company_id               = NVL (p_company_id, t4015.c901_company_id)
        AND t4015.c901_level_id                 = NVL (p_level_id, t4015.c901_level_id)
        AND t4015.c901_access_type              = NVL (p_access_id, t4015.c901_access_type)
        AND t4015.c901_level_value              = NVL (p_value_id, t4015.c901_level_value)
        AND t4016.c901_level_value              = NVL (p_inventory_id, t4016.c901_level_value)
        AND t4015.c4015_void_fl                IS NULL
        AND t4016.c4016_void_fl                IS NULL
   ORDER BY t4015.c901_company_id, t4015.c901_level_id;
   
END gm_fch_ds_global_mapping_dtl;

   /*************************************************************************************
    * Purpose: This Procedure is used to fetch the Global Mapping Inventory List Details
    *************************************************************************************/
    --
	PROCEDURE gm_fch_glbmap_Inventory (        
	    p_out_cur 	  OUT TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_cur FOR
			SELECT DISTINCT(t4016.c901_level_value) CODEID 
                 , get_code_name(t4016.c901_level_value) CODENM  
              FROM t4016_global_inventory_mapping t4016
             WHERE t4016.c4016_void_fl IS NULL 
          ORDER BY codenm;
	END gm_fch_glbmap_Inventory;
	
	 /*************************************************************************************
    * Purpose: This Procedure is used to fetch the Global Mapping Level List Details
    *************************************************************************************/
    --
	PROCEDURE gm_fch_glbmap_LevelValue (        
	    p_out_cur 	  OUT TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_cur FOR
			SELECT DISTINCT(T4015.c901_level_value) CODEID
	             , DECODE(T4015.c901_level_id,102584,get_rule_value(T4015.c901_level_value, 'REG_SHRT_CODE') , get_code_name(T4015.c901_level_value)) CODENM 
	          FROM t4015_global_sheet_mapping T4015 
	         WHERE t4015.c4015_void_fl IS NULL
	           AND  t4015.c901_level_value IS NOT NULL
	      ORDER BY codenm;
	END gm_fch_glbmap_LevelValue;
END gm_pkg_oppr_global_mapping_rpt;
/