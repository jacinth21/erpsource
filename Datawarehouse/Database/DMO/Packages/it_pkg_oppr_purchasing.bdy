/* Formatted on 2011/03/04 15:17 (Formatter Plus v4.8.0) */

-- @"C:\Data Correction\Script\Globus_DM_Operation\it_pkg_oppr_purchasing.bdy"

CREATE OR REPLACE PACKAGE BODY it_pkg_oppr_purchasing
IS
--
	
 /*********************************************************************************
  * Description : This procedure is used to approve the Demand Sheet for the passed in Information.
  **********************************************************************************/	
--Sample Input  gm_save_ttp_details (ttp_dtls.ttp_id, p_ttp_status,p_demand_sheet_status,p_user_id);
--				gm_save_ttp_details (1112,50582,50551,303510);
PROCEDURE  gm_approve_demand_sheet 
(
		p_ttp_id IN t4052_ttp_detail.c4050_ttp_id%TYPE,
		p_ttp_status IN t4052_ttp_detail.c901_status%TYPE,
		p_demand_sheet_status IN t4040_demand_sheet.c901_status%TYPE,
		p_user_id IN t4052_ttp_detail.c4052_created_by%TYPE
		)
		AS
		v_ttp_detail_id t4052_ttp_detail.c4052_ttp_detail_id%TYPE;
		v_ttp_dtl_count    	NUMBER;
		v_ttp_count			NUMBER;
		v_forecast			NUMBER:=4;
		v_load_date 		DATE;
		
		BEGIN	
				BEGIN
					/* Validation TTP id */
					SELECT COUNT(1)
					INTO v_ttp_count
					FROM t4051_ttp_demand_mapping
					WHERE c4050_ttp_id =p_ttp_id;
					
					IF v_ttp_count=0
					THEN
						raise_application_error ('-20756', 'Enter valid TTP ID');
					END IF;
				END;
		
			SELECT TRUNC(SYSDATE,'month') 
			INTO v_load_date 
			FROM DUAL;
	
			SELECT COUNT(1)
			INTO v_ttp_dtl_count
			FROM t4052_ttp_detail
			WHERE C4050_TTP_ID     =p_ttp_id
			AND C4052_VOID_FL     IS NULL
			AND C4052_TTP_LINK_DATE=v_load_date;
			
			
		IF (v_ttp_dtl_count > 0) THEN
			RETURN;
			
		ELSE
			
			SELECT s4052_ttp_detail.NEXTVAL
			  INTO v_ttp_detail_id
			  FROM DUAL;
			-- 1. Create a new entry onto t4052_ttp_detail
			INSERT INTO t4052_ttp_detail
						(
						c4052_ttp_detail_id, 
						c4050_ttp_id, 
						c4052_ttp_link_date, 
						c901_status, 
						c4052_created_by,
					    c4052_created_date, 
					    c4052_forecast_period, 
					    c4052_load_date
						)
				 VALUES (
				 		v_ttp_detail_id, 
				 		p_ttp_id, 
				 		v_load_date, 
				 		p_ttp_status, 
				 		p_user_id,
					    CURRENT_DATE, 
					    v_forecast, 
					    v_load_date
						);	
			-- to update the ttp details id to Demand sheet
			-- 2. Update the v_ttp_detail_id created into t4040_demand_sheet for all the selected demand sheets
			
			UPDATE t4040_demand_sheet t4040
                SET t4040.c4052_ttp_detail_id = v_ttp_detail_id ,
                      t4040.c901_status = p_demand_sheet_status, -- Approved,
                       t4040.c4040_last_updated_by    = p_user_id ,
                       t4040.c4040_last_updated_date  = CURRENT_DATE
                      WHERE t4040.c4052_ttp_detail_id IS NULL
                       AND t4040.c4040_demand_sheet_id IN
                      (SELECT t4040.c4040_demand_sheet_id  FROM t4020_demand_master t4020, t4040_demand_sheet t4040
                        WHERE t4020.c4020_demand_master_id = t4040.c4020_demand_master_id
						AND t4020.c4020_void_fl IS NULL
						AND t4040.c4040_void_fl IS NULL
                       and c4020_parent_demand_master_id= ( select C4020_DEMAND_MASTER_ID from t4051_ttp_demand_mapping where C4050_TTP_ID=p_ttp_id) 
                        and C4040_DEMAND_PERIOD_DT=  v_load_date);
			
			v_ttp_dtl_count :=0;
			v_ttp_count 	:=0;
					
		END IF;			
END gm_approve_demand_sheet;


/*********************************************************************************
  * Description : This procedure is used to move date from Tt042 to Archive and Delete from Tt042
  * exec it_pkg_oppr_purchasing.gm_move_arch_demand_sheet_dtl('01-MAY-2018');
  **********************************************************************************/
PROCEDURE gm_move_arch_demand_sheet_dtl(
		p_load_date           IN     T4040_DEMAND_SHEET.C4040_DEMAND_PERIOD_DT%type
		)
		AS
		  l_counter       NUMBER:=0;
		  l_counter_tot   NUMBER:=0;
		  commit_after_in NUMBER:=10; -- Input no of DEMAND_SHEET_ID after commit


		 CURSOR dem_sheet_cur
		  IS
			SELECT C4040_DEMAND_SHEET_ID
			FROM GLOBUS_DM_OPERATION.T4040_DEMAND_SHEET
			WHERE C4040_DEMAND_PERIOD_DT=p_load_date;
    
    BEGIN
    
     FOR dem_sheet_cur_loop IN dem_sheet_cur
      LOOP
    
        INSERT into GLOBUS_DM_OPERATION.T4042_DEMAND_SHEET_DETAIL_ARCH SELECT *FROM GLOBUS_DM_OPERATION.T4042_DEMAND_SHEET_DETAIL WHERE  C4040_DEMAND_SHEET_ID= dem_sheet_cur_loop.C4040_DEMAND_SHEET_ID;
    
         DELETE GLOBUS_DM_OPERATION.T4042_DEMAND_SHEET_DETAIL
         WHERE C4040_DEMAND_SHEET_ID= dem_sheet_cur_loop.C4040_DEMAND_SHEET_ID;
    
    
         dbms_output.put_line(' (l_counter) :'|| l_counter || '  dem_sheet_cur_loop.C4040_DEMAND_SHEET_ID=' || dem_sheet_cur_loop.C4040_DEMAND_SHEET_ID);
        IF l_counter = commit_after_in THEN
          COMMIT;
          dbms_output.put_line('Commit After (l_counter) :'|| l_counter);
          l_counter_tot := l_counter+l_counter_tot;
          l_counter     := 0;
        ELSE
          l_counter := l_counter + 1;
    
        END IF;
      END LOOP;
    
          dbms_output.put_line('gm_move_arch_demand_sheet_dtl Total Records (l_counter_tot) :'|| l_counter_tot);
          COMMIT;
        EXCEPTION
        WHEN OTHERS THEN
          dbms_output.put_line('gm_move_arch_demand_sheet_dtl An error was encountered - ' || SQLERRM);

 END gm_move_arch_demand_sheet_dtl; 
 
 /*********************************************************************************
  * Description : This procedure is used to delete from Archive 
  * exec it_pkg_oppr_purchasing.gm_del_arch_demand_sheet_dtl('01-MAY-2019');
  **********************************************************************************/
PROCEDURE gm_del_arch_demand_sheet_dtl
(
		p_load_date           IN     T4040_DEMAND_SHEET.C4040_DEMAND_PERIOD_DT%type
		)
		AS
	l_counter       NUMBER:=0;
	l_counter_tot   NUMBER:=0;
	commit_after_in NUMBER:=10; -- Input no of DEMAND_SHEET_ID after commit

  CURSOR dem_sheet_cur
  IS
    SELECT C4040_DEMAND_SHEET_ID
    FROM GLOBUS_DM_OPERATION.T4040_DEMAND_SHEET
    WHERE C4040_DEMAND_PERIOD_DT=p_load_date;

    BEGIN
    
      l_counter := 1;
        
      FOR dem_sheet_cur_loop IN dem_sheet_cur
      LOOP
    
         DELETE GLOBUS_DM_OPERATION.T4042_DEMAND_SHEET_DETAIL_ARCH
         WHERE C4040_DEMAND_SHEET_ID= dem_sheet_cur_loop.C4040_DEMAND_SHEET_ID;
    
   
         dbms_output.put_line(' (l_counter) :'|| l_counter || '  dem_sheet_cur_loop.C4040_DEMAND_SHEET_ID=' || dem_sheet_cur_loop.C4040_DEMAND_SHEET_ID);
    
        IF l_counter = commit_after_in THEN
          COMMIT;
          dbms_output.put_line('Commit After (l_counter) :'|| l_counter);
          l_counter_tot := l_counter+l_counter_tot;
          l_counter     := 0;
        ELSE
          l_counter := l_counter + 1;
    
        END IF;
      END LOOP;

  dbms_output.put_line('gm_del_arch_demand_sheet_dtl Total Records (l_counter_tot) :'|| l_counter_tot);
  COMMIT;
	EXCEPTION
	WHEN OTHERS THEN
	  dbms_output.put_line('gm_del_arch_demand_sheet_dtl An error was encountered - ' || SQLERRM);

END gm_del_arch_demand_sheet_dtl;
	
END it_pkg_oppr_purchasing;
/
