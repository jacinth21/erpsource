/* Formatted on 2013/07/03  */
--@"c:\database\packages\operations\purchasing\gm_pkg_oppr_dmd_mas_txn.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_dmd_mas_txn
IS

--
/*******************************************************
 * Purpose: This Procedure is used to
 * fetch editable demand sheet
 *******************************************************/
--
	PROCEDURE gm_sav_part_otn (
		p_userid	 IN   t4023_demand_otn_detail.c4023_last_updated_by%TYPE
	  , p_inputstr	 IN   VARCHAR2
	)
	AS
		v_strlen	   NUMBER := NVL (LENGTH (p_inputstr), 0);
		v_string	   VARCHAR2 (30000) := p_inputstr;
		v_substring    VARCHAR2 (1000);
		v_demandmasterid t4020_demand_master.c4020_demand_master_id%TYPE;
		v_partnumber   t4023_demand_otn_detail.c205_part_number_id%TYPE;
		v_otnvalue	   t4023_demand_otn_detail.c4023_qty%TYPE;
		--
	/*******************************************************
	  * Description : Procedure to save demand sheet par
	  * Author		  : Xun
	  *******************************************************/
	BEGIN
		IF v_strlen > 0
		THEN
			WHILE INSTR (v_string, '|') <> 0
			LOOP
				--
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				--
				v_partnumber := NULL;
				v_demandmasterid := NULL;
				v_otnvalue	:= NULL;
				--
				v_partnumber := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_demandmasterid := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_otnvalue	:= v_substring;
			
				UPDATE t4023_demand_otn_detail
				   SET c4023_qty = v_otnvalue
					 , c4023_last_updated_by = p_userid
					 , c4023_last_updated_date = SYSDATE
				 WHERE c205_part_number_id = v_partnumber AND c4020_demand_master_id = v_demandmasterid;

				IF (SQL%ROWCOUNT = 0)
				THEN
					INSERT INTO t4023_demand_otn_detail
								(c4023_demand_otn_detail_id, c4020_demand_master_id, c205_part_number_id
							   , c4023_qty, c4023_last_updated_by, c4023_last_updated_date
								)
						 VALUES (s4023_demand_otn_detail.NEXTVAL, v_demandmasterid, v_partnumber
							   , v_otnvalue, p_userid, SYSDATE
								);
				END IF;
			END LOOP;
		END IF;
	END gm_sav_part_otn;

	
--
/************************************************************
* Description 	: Procedure to save PAR values for the Set's
* Author		: Hreddi
*************************************************************/	
	PROCEDURE gm_sav_set_par (
	  p_input_str		IN	 VARCHAR2
	, p_user_id		    IN	 t207_set_master.c207_last_updated_by%TYPE
	)
	AS
		v_strlen			 NUMBER := NVL (LENGTH (p_input_str), 0);
	    v_string	   		 VARCHAR2 (4000) := p_input_str;
	    v_substring    		 VARCHAR2 (4000);
	    v_demandsheet_id     t4022_demand_par_detail.c4020_demand_master_id%TYPE;
	  	v_set_id 			 t4022_demand_par_detail.c207_set_id%TYPE;
	  	v_par_value	         t4022_demand_par_detail.c4022_par_value%TYPE;
	  	v_demandshtid		 t4022_demand_par_detail.c4020_demand_master_id%TYPE;
	  	v_count              NUMBER;
	BEGIN
		IF v_strlen > 0
	    THEN
	      WHILE INSTR (v_string, '|') <> 0
	      LOOP
	      	    v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				--
				v_demandsheet_id := NULL;
				v_set_id := NULL;
				v_par_value	:= NULL;
				--
				v_demandsheet_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_set_id         := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_par_value	     := v_substring;
				
				IF v_demandsheet_id IS NULL THEN
					BEGIN
						 SELECT t4020.c4020_demand_master_id INTO v_demandshtid
		   				   FROM t4021_demand_mapping t4021, t4020_demand_master t4020, t4015_global_sheet_mapping t4015
		  				  WHERE c4021_ref_id 					= v_set_id
						    AND t4020.c4020_demand_master_id    = t4021.c4020_demand_master_id
						    AND t4020.c4020_inactive_fl         IS NULL
						    AND t4020.c4020_void_fl             IS NULL
						    AND t4015.c4015_global_sheet_map_id = t4020.c4015_global_sheet_map_id
						    AND t4015.c4015_void_fl             IS NULL
						    AND t4015.c901_level_id             = '102580' -- Company
						    AND t4020.c901_demand_type          = '40021'  -- Consignment Sheet
						    AND t4015.c901_company_id           = t4020.c901_company_id
						    AND t4021.c901_ref_type             = '40031'  -- Consignment Set
					   ORDER BY t4021.c4021_ref_id;
				   EXCEPTION WHEN NO_DATA_FOUND
				   THEN
				   		GM_RAISE_APPLICATION_ERROR('-20999','297',v_set_id);
				   END;
			   ELSE
			   		v_demandshtid := v_demandsheet_id;
			   END IF;
			
			   SELECT COUNT(1) 
			     INTO v_count 
			     FROM t4022_demand_par_detail 
				WHERE c207_set_id = v_set_id 
				  AND c4020_demand_master_id = v_demandshtid;

			/* IF Condition is added for, from JS we are getting the input string for empty Sets also. so we should not updated or Insert for which we are not submitted.
			 * SO we are checking the condition that if PAR is aleady setted means then it has to update for that demand sheet id  or else it has to Insert the new Record  */
			   IF v_par_value IS NOT NULL OR (v_par_value IS NULL AND v_count > 0) THEN
			   
			       UPDATE t4022_demand_par_detail t4022
					  SET t4022.c4022_par_value = v_par_value
						, t4022.c4022_updated_by = p_user_id
						, t4022.c4022_updated_date = SYSDATE
					WHERE t4022.c207_set_id = v_set_id 
					  AND t4022.c4020_demand_master_id = v_demandshtid;
					  
					IF (SQL%ROWCOUNT = 0)
					THEN
						INSERT INTO t4022_demand_par_detail
									( c4022_demand_par_detail_id, c4020_demand_master_id, c207_set_id
								    , c4022_par_value, c4022_updated_by, c4022_updated_date
									)
							 VALUES ( s4022_demand_par_detail.NEXTVAL, v_demandshtid, v_set_id
								    , v_par_value, p_user_id, SYSDATE
									);
					END IF;
				END IF;
	      END LOOP;
	    END IF;
	END gm_sav_set_par;
	
	
/************************************************************
* Description 	: Procedure to save override demand values
* Author		: Elango
*************************************************************/			
PROCEDURE gm_sav_dmd_avg_override (
     p_demand_master_id   IN  t4032_demand_override.c4020_demand_master_id%TYPE,
     p_demand_sheet_id    IN  t4032_demand_override.c4040_demand_sheet_id%TYPE,
     p_ref_id   	 IN   t4032_demand_override.c4032_ref_id%TYPE,
     p_ref_type  	 IN   t4032_demand_override.c901_ref_type%TYPE,
     p_qty           IN   t4032_demand_override.c4032_qty%TYPE,
     p_comment 		 IN   VARCHAR2,
     p_user_id    	 IN   t4032_demand_override.c4032_last_updated_by%TYPE,
     p_override_id   OUT  t4032_demand_override.c4032_override_id%TYPE
   )
   AS 
   v_count         NUMBER;
   v_msg           varchar(4000);
   v_override_id   t4032_demand_override.c4032_override_id%TYPE;
   v_execute_load_id NUMBER;
   
   BEGIN
		--
   UPDATE t4032_demand_override
      SET c4032_qty  = p_qty, 
          c4032_comments_fl = DECODE (p_comment, NULL, '', 'Y'), 
          c4032_last_updated_by =   p_user_id, 
          c4032_last_updated_date = SYSDATE
  	WHERE c4020_demand_master_id  = p_demand_master_id
  	  AND c4032_ref_id    = p_ref_id
  	  AND c901_ref_type   = p_ref_type
  	  AND c4032_void_fl   IS NULL;
    
    IF (SQL%ROWCOUNT   = 0)
	THEN
     SELECT S4032_DEMAND_OVERRIDE.NEXTVAL 
     INTO p_override_id 
     FROM DUAL;
     
     INSERT
       INTO t4032_demand_override
        (
            C4032_OVERRIDE_ID, C4020_DEMAND_MASTER_ID, C4040_DEMAND_SHEET_ID
          , C4032_REF_ID, C901_REF_TYPE, C4032_QTY
          , C4032_COMMENTS_FL, C901_OVERIDE_TYPE, C4032_CREATED_BY
          , C4032_CREATED_DATE
        )
        VALUES
        (
            p_override_id, p_demand_master_id, p_demand_sheet_id
          , p_ref_id, p_ref_type, p_qty
          , DECODE ( p_comment, NULL ,'','Y'), '102962', p_user_id
          , SYSDATE
        ) ;
	END IF;

	IF p_override_id IS NULL 
	THEN
	   SELECT T4032.C4032_OVERRIDE_ID 
		 INTO v_override_id
		 FROM t4032_demand_override T4032
	  	WHERE c4020_demand_master_id  = p_demand_master_id
	  	  AND c4032_ref_id    = p_ref_id
	  	  AND c901_ref_type   = p_ref_type
	  	  AND c4032_void_fl   IS NULL; 	
	  	  
	  	 p_override_id := v_override_id;	  	 
  	END IF;
  	
	IF p_comment IS NOT NULL THEN
	    --Create the comments for the Override
	    gm_update_log (p_override_id, p_comment, '4000344', p_user_id, v_msg) ;
	  
	END IF;
	
	SELECT COUNT (1)
	  INTO v_count
	  FROM t9302_execute_load t9302
	 WHERE t9302.c9302_ref_id  = p_demand_master_id
	   AND t9302.c901_ref_type = '91952'
	   AND t9302.c9302_status  = 10;
	   
	--Check if the Sheet is already scheduled, if not schedule it.
	IF v_count = 0
	THEN
	--In the below prc checking 10, 20 status. but we need to check status only if it is 10. so commenting the below code.  
	--gm_pkg_oppr_ld_demand.gm_op_sav_load_detail (p_demand_master_id, '91952', p_user_id) ; 
	    
		SELECT s9302_execute_load.NEXTVAL
		  INTO v_execute_load_id
		  FROM DUAL;

		INSERT INTO t9302_execute_load
					(c9302_execute_load_id, c9302_ref_id, c901_ref_type, c9302_request_by, c9302_request_date
				   , c9302_status
					)
			 VALUES (v_execute_load_id, p_demand_master_id, '91952', p_user_id, SYSDATE
				   , 10
					);
	END IF;

    END gm_sav_dmd_avg_override;

END gm_pkg_oppr_dmd_mas_txn;
/
