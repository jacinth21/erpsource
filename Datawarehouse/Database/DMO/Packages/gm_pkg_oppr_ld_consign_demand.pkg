/* Formatted on 2008/06/23 10:13 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\purchasing\gm_pkg_oppr_ld_consign_demand.pkg"
-- show err  Package Body gm_pkg_oppr_ld_consign_demand

CREATE OR REPLACE PACKAGE gm_pkg_oppr_ld_consign_demand
IS
/*******************************************************
 * Purpose: Package holds Order Planning load procedure
 *			for Sales deman, Called from the main procedure
 *			GM_PKG_OP_LD_DEMAND
 *
 * modification history
 * ====================
 * Person		 Date	Comments
 * ---------   ------	------------------------------------------
 * Richardk    20070813  Initial Version
 *******************************************************/

	/*************************************************************************
	 * Purpose: Procedure used to load sales demand, calculated value and
	 *	Forecast information
	 *************************************************************************/
	PROCEDURE gm_op_ld_main (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_demand_type		IN	 t4020_demand_master.c901_demand_type%TYPE
	  , p_demand_period 	IN	 t4020_demand_master.c4020_demand_period%TYPE
	  , p_forecast_period	IN	 t4020_demand_master.c4020_forecast_period%TYPE
	  , p_inc_current_fl	IN	 t4020_demand_master.c4020_include_current_fl%TYPE
	  , p_inventory_id		IN	 t250_inventory_lock.c250_inventory_lock_id%TYPE
	);

	/*************************************************************************
	 * Purpose: Procedure to be used to load Distributor related information based on
	 * sheet information [Load based on level]
	 *  Start with Zone, Region, load Distributor Info in temp table 
	 *************************************************************************/
	PROCEDURE gm_op_ld_distributor_info (
       p_demand_id   IN  t4020_demand_master.c4020_demand_master_id%TYPE
     , p_level_id    IN  t4015_global_sheet_mapping.c901_level_id%TYPE
     , p_level_value IN  t4015_global_sheet_mapping.c901_level_value%TYPE
    );
   /*************************************************************************
    * Purpose: Procedure to be used to load set information related to the group
    * to be used by detail sql 
    *************************************************************************/
    PROCEDURE gm_op_ld_set_info (
      p_demand_id   IN   t4020_demand_master.c4020_demand_master_id%TYPE
    , p_level_id    IN  t4015_global_sheet_mapping.c901_level_id%TYPE 
    , p_level_value IN  t4015_global_sheet_mapping.c901_level_value%TYPE
    );
    
	/*************************************************************************
	 * Purpose: Procedure used to load consignment set for selected period
	 * 40030 maps to Group Mapping and 40032 maps to Region Mapping
	 *************************************************************************/
	PROCEDURE gm_op_ld_demand_fa_set (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_from_period		IN	 DATE
	  , p_to_period 		IN	 DATE
	);
 
 
	/*************************************************************************
	 * Purpose: Procedure used to load consignment item for selected period
	 * 40030 maps to Group Mapping and 40032 maps to Region Mapping
	 *************************************************************************/
	PROCEDURE gm_op_ld_demand_fa_item (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_from_period		IN	 DATE
	  , p_to_period 		IN	 DATE
	);

    /*************************************************************************
    * Purpose: Procedure used to load inhosue consignment demand for selected period
    *************************************************************************/
    PROCEDURE gm_op_ld_demand_inhosue_set (
         p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE
       , p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE
       , p_from_period     IN DATE
       , p_to_period       IN DATE
    );
 
     /*************************************************************************
     * Purpose: Procedure used to load inhouse item for selected period
     *************************************************************************/
    PROCEDURE gm_op_ld_demand_inhouse_item (
         p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE
       , p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE
       , p_from_period     IN DATE
       , p_to_period       IN DATE
    );

   /*************************************************************************
    * Purpose: Procedure used to capture the DEMAND of all the written off Parts in the
    * Loaner Reconciliation process.
    *************************************************************************/
    PROCEDURE gm_op_ld_demand_lnr_writeoff (
         p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE
       , p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE
       , p_from_period     IN DATE
       , p_to_period       IN DATE
     );

	/*************************************************************************
	 * Purpose: Procedure used to load sales forecast information
	 * Based on the demand weighted will load forecase information
	 *************************************************************************/
	PROCEDURE gm_op_ld_forecast (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_from_period		IN	 DATE
	  , p_to_period 		IN	 DATE
	);

--
	/*************************************************************************
	 * Purpose: Procedure used to load Pending Requests
	 *************************************************************************/
	PROCEDURE gm_op_ld_pending_request (
		p_demand_master_id	 IN   t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	 IN   t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_from_period		 IN   DATE
	  , p_inventory_id		 IN   t250_inventory_lock.c250_inventory_lock_id%TYPE
	);
--

    /*************************************************************************
    * Purpose: Procedure used to load sheet OTN Information
    * OTN -- One Time need
    * OTN -- Only Appliciable for Items
    *************************************************************************/
    PROCEDURE gm_op_ld_otn (
            p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE,
            p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
            p_to_period       IN DATE
     );
        
END gm_pkg_oppr_ld_consign_demand;
/
