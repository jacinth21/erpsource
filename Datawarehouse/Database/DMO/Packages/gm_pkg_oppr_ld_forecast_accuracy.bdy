CREATE OR REPLACE PACKAGE body GLOBUS_DM_OPERATION.gm_pkg_oppr_ld_forecast_accuracy
IS
		PROCEDURE GM_LD_forecast_accuracy_main(
		p_load_dt IN DATE,
		p_load_start_dt IN DATE

	)
	AS
		v_load_start_dt DATE :=p_load_start_dt;
		v_temp_start_dt  DATE;
		v_months NUMBER := 11;
		v_month_index NUMBER := 12;
		v_month_index_temp NUMBER;
		v_month DATE;
		v_user_id VARCHAR2(10) :=303510;
		v_FORECAST_ACCURACY_ID NUMBER;
		l_cntr NUMBER;


	begin


		UPDATE T4048_FORECAST_ACCURACY_DETAILS SET C4048_VOID_FL ='Y',C4048_LAST_UPDATED_DT = CURRENT_DATE,
		C4048_LAST_UPDATED_BY = v_user_id
		WHERE
		C4047_FORECAST_ACCURACY_ID IN (
			SELECT C4047_FORECAST_ACCURACY_ID FROM T4047_FORECAST_ACCURACY
			WHERE C4047_VOID_FL IS NULL
			AND C4047_LOAD_DT = p_load_dt
		)
		AND C4048_VOID_FL IS NULL;

		UPDATE T4047_FORECAST_ACCURACY
			SET C4047_VOID_FL = 'Y',
				C4047_UPDATED_DT = CURRENT_DATE,
				C4047_UPDATED_BY = v_user_id
		WHERE
			C4047_LOAD_DT = p_load_dt
			AND C4047_VOID_FL IS  NULL;

		INSERT INTO T4047_FORECAST_ACCURACY (C4047_LOAD_DT,C4047_UPDATED_BY,C4047_UPDATED_DT)
		VALUES(p_load_dt,v_user_id,CURRENT_DATE);

		SELECT C4047_FORECAST_ACCURACY_ID
		INTO v_FORECAST_ACCURACY_ID
		FROM
		T4047_FORECAST_ACCURACY
		WHERE C4047_LOAD_DT = p_load_dt
			AND C4047_VOID_FL IS  NULL;


		DBMS_OUTPUT.PUT_LINE('ID:'||v_FORECAST_ACCURACY_ID);
		commit;
		gm_ld_part_number(v_FORECAST_ACCURACY_ID,p_load_dt);
		commit;



		FOR l_cntr  IN  0..v_months
		LOOP
			SELECT ADD_MONTHS(v_load_start_dt,(l_cntr)*-1)
			INTO v_temp_start_dt
			FROM DUAL;

			v_month_index_temp:=v_month_index-l_cntr;
			DBMS_OUTPUT.PUT_LINE(l_cntr||'::'||v_temp_start_dt ||':v_month_index:'||(v_month_index_temp));

			GM_LD_forecast_accuracy_dtls ( v_FORECAST_ACCURACY_ID,v_temp_start_dt,v_month_index_temp);
			commit;

		END LOOP;

		dbms_output.put_line('--> Begin:'||TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
		gm_ld_calc_variance(v_FORECAST_ACCURACY_ID, 0);
		commit;

		dbms_output.put_line('--> Begin:'||TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
		--gm_upd_part_rank(v_FORECAST_ACCURACY_ID,p_load_dt);


		dbms_output.put_line(' Begin:'||TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
		gm_upd_system_id(v_FORECAST_ACCURACY_ID);
		commit;

		dbms_output.put_line(' Begin:'||TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
		gm_upd_group_id(v_FORECAST_ACCURACY_ID);
		commit;
		/*
		dbms_output.put_line(' Begin:'||TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
		gm_ld_calc_variance_by_group(v_FORECAST_ACCURACY_ID);
		commit;

		dbms_output.put_line(' Begin:'||TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
		gm_ld_calc_variance_by_system(v_FORECAST_ACCURACY_ID);
		*/

		dbms_output.put_line('--> Begin:'||TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
	commit;

	end GM_LD_forecast_accuracy_main;




	PROCEDURE GM_LD_forecast_accuracy_dtls(
	P_fcst_accur_ID IN T4047_FORECAST_ACCURACY.C4047_FORECAST_ACCURACY_ID%TYPE,
	p_load_dt IN DATE ,
	p_month_index IN  NUMBER

	)
	AS

	v_load_dt DATE := p_load_dt;

	v_fcst_add_month NUMBER := 3;
	v_dmd_add_month  NUMBER := 4;


	v_fcst_mnth DATE;
	v_4042_period_dmdwtdavg DATE;
	v_4042_period_fcst DATE;
	v_4040_dmd_period_dt_dmd DATE;
	v_4042_period_dmd DATE;
	c4047_fcst_acc_id NUMBER;

	BEGIN


	v_4042_period_dmdwtdavg := v_load_dt;

	SELECT add_months(v_load_dt,v_fcst_add_month),	add_months(v_load_dt,v_fcst_add_month) , add_months(v_load_dt,v_dmd_add_month)
	INTO	v_4042_period_fcst , v_4042_period_dmd , v_4040_dmd_period_dt_dmd

	FROM DUAL;
	v_4042_period_dmdwtdavg := v_load_dt;

	dbms_output.put_line(' Begin:'||TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS')||':v_4042_period_dmdwtdavg'||v_4042_period_dmdwtdavg);

	gm_ld_dmd_wtd_avg(P_fcst_accur_ID,v_load_dt,v_4042_period_fcst, p_month_index);
	COMMIT;
	dbms_output.put_line(' 1 :'||TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
	dbms_output.put_line('v_load_dt:'||v_load_dt||':v_4042_period_fcst:'||v_4042_period_fcst);

	gm_ld_forecast(P_fcst_accur_ID,v_load_dt,v_4042_period_fcst, p_month_index);
	dbms_output.put_line(' 2:'||TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
	COMMIT;
	gm_ld_actuals(P_fcst_accur_ID,v_load_dt,v_4042_period_fcst,v_4040_dmd_period_dt_dmd,v_4042_period_dmd, p_month_index);
	dbms_output.put_line(' 3:'||TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));

--	gm_ld_calc_variance(P_fcst_accur_ID, p_month_index);
	/*


	dbms_output.put_line(' 2:'||TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
	dbms_output.put_line('v_4040_dmd_period_dt_dmd:'||v_4040_dmd_period_dt_dmd||':v_4042_period_dmd:'||v_4042_period_dmd);

	gm_ld_actuals(v_4040_dmd_period_dt_dmd,v_4042_period_dmd, p_month_index);

	dbms_output.put_line(' 3:'||TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
	commit;

	gm_ld_calc_variance(P_fcst_accur_ID, p_month_index);
	dbms_output.put_line(' 4:'||TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS'));
	*/
	END GM_LD_forecast_accuracy_dtls;




	PROCEDURE gm_ld_part_number(
		P_fcst_accur_ID IN T4047_FORECAST_ACCURACY.C4047_FORECAST_ACCURACY_ID%TYPE,
		p_load_dt IN DATE
	)

	AS
		type pnum_array			IS     TABLE OF  T205_PART_NUMBER.c205_part_number_id%TYPE;
		type rank_array	IS     TABLE OF  T4048_FORECAST_ACCURACY_DETAILS.C4072_RANK%TYPE;
		type level_id_array	IS     TABLE OF  T4048_FORECAST_ACCURACY_DETAILS.C901_LEVEL_ID%TYPE;
		type level_value_array	IS     TABLE OF  T4048_FORECAST_ACCURACY_DETAILS.C901_LEVEL_VALUE%TYPE;

		arr_pnum pnum_array;
		arr_level_id level_id_array;
		arr_level_value level_value_array;
		arr_rank rank_array;

		CURSOR cur_part_list
		IS
		/*
			select  C205_PART_NUMBER_ID pnum ,C901_LEVEL_VALUE lvl_value from T4060_PART_FORECAST_QTY where
			last_day(trunc(C4060_LAST_UPDATED_DATE))= last_day(add_months(p_load_dt,-1)) and
			C901_FORECAST_TYPE in (40021,40020,40022) and C901_LEVEL_VALUE in(100801,26230708,100800,26240569,26240179)
			--and C205_PART_NUMBER_ID='124.000'
			group by C205_PART_NUMBER_ID,C901_LEVEL_VALUE;
			*/
			 select C205_PART_NUMBER_ID pnum,C4072_RANK rank, t4040_level.C901_LEVEL_ID lvl_id,t4040_level.C901_LEVEL_VALUE lvl_value
			 from T4072_PURCHASE_PART_RANK_DETAIL,
			 (
			 select distinct C901_LEVEL_ID, C901_LEVEL_VALUE
					from T4040_DEMAND_SHEET where C4040_demand_period_dt = p_load_dt
					and C901_ACCESS_TYPE =102623
				)t4040_level
			 where C4071_PURCHASE_PART_RANK_ID in (
				select C4071_PURCHASE_PART_RANK_ID from T4071_PURCHASE_PART_RANK where C4071_VOID_FL IS NULL AND
				last_day(C4071_LOAD_DATE) = last_day(add_months(trunc(p_load_dt),-1)))
				--and C205_PART_NUMBER_ID in ('1119.0110')
				-- '1119.0100','1119.0000','124.000',
				--and C205_PART_NUMBER_ID='1119.0000'
				;


	BEGIN
		OPEN cur_part_list;
		LOOP

		FETCH cur_part_list BULK COLLECT INTO arr_pnum,arr_rank,arr_level_id,arr_level_value  LIMIT 5000;

		FORALL i in 1.. arr_pnum.COUNT

		INSERT INTO T4048_FORECAST_ACCURACY_DETAILS (C205_PART_NUMBER_ID
		,C4047_FORECAST_ACCURACY_ID,C4072_RANK, C901_LEVEL_ID,C901_LEVEL_VALUE
		)
		VALUES (arr_pnum(i),P_fcst_accur_ID, arr_rank(i),arr_level_id(i),arr_level_value(i)
		);


		EXIT WHEN cur_part_list%NOTFOUND;
	    END LOOP;
	    CLOSE cur_part_list;


	END gm_ld_part_number;

	PROCEDURE gm_ld_dmd_wtd_avg(
		P_fcst_accur_ID IN T4047_FORECAST_ACCURACY.C4047_FORECAST_ACCURACY_ID%TYPE,
		p_load_dt IN DATE,
		P_4042_period_dmdwtdavg IN DATE,
		p_month_index IN  NUMBER
	)
	AS
		type pnum_array			IS     TABLE OF  T205_PART_NUMBER.c205_part_number_id%TYPE;
		type lvlid_array		IS     TABLE OF  T4040_DEMAND_SHEET.C901_LEVEL_ID%TYPE;
		type lvl_value_array		IS     TABLE OF  T4040_DEMAND_SHEET.C901_LEVEL_VALUE%TYPE;
		type sales_wtd_avg_array	IS     TABLE OF  T4042_DEMAND_SHEET_DETAIL.C4042_QTY%TYPE;
		type cons_wtd_avg_array		IS     TABLE OF  T4042_DEMAND_SHEET_DETAIL.C4042_QTY%TYPE;
		type inhouse_wtd_array		IS     TABLE OF  T4042_DEMAND_SHEET_DETAIL.C4042_QTY%TYPE;

		arr_pnum pnum_array;
		arr_lvl_id lvlid_array;
		arr_lvl_value lvl_value_array;
		arr_sales_wtd_avg sales_wtd_avg_array;
		arr_cons_wtd_avg cons_wtd_avg_array;
		arr_inhouse_wtd_avg inhouse_wtd_array;


		CURSOR CUR_dmd_wtd_avg
		IS
			select pnum,lvl_id ,lvl_value, SUM(CASE WHEN sheet_type = 40020 THEN qty ELSE 0 END) sales,
			SUM(CASE WHEN sheet_type = 40021 THEN qty ELSE 0 END)cons,
			SUM(CASE WHEN sheet_type = 40022 THEN qty ELSE 0 END)in_house
			from (
				select t4040.C901_LEVEL_ID lvl_id,t4040.C901_LEVEL_VALUE lvl_value, t4020.C4020_DEMAND_NM
				,C4040_demand_period_dt period, t4042.C205_PART_NUMBER_ID pnum
				,t4042.C4042_QTY qty
				--,get_code_name(t4042.c901_type) type
				, t4020.C901_DEMAND_TYPE sheet_type
				from
				T4040_DEMAND_SHEET t4040,  T4042_DEMAND_SHEET_DETAIL t4042, T4020_DEMAND_MASTER t4020,
				-- T4048_FORECAST_ACCURACY_DETAILS t4048,
				T4021_DEMAND_MAPPING t4021,
				T4011_GROUP_DETAIL t4011,T4010_GROUP t4010
				--, T4047_FORECAST_ACCURACY t4047
				where
					--and t4040.C901_LEVEL_ID in (102580)
					t4040.C901_ACCESS_TYPE =102623 -- editable
					and t4040.C4040_DEMAND_SHEET_ID = t4042.C4040_DEMAND_SHEET_ID
					and t4020.C4020_DEMAND_MASTER_ID = t4040.C4020_DEMAND_MASTER_ID
					/*
					and t4042.c205_part_number_id = t4048.c205_part_number_id
					AND T4048.c4048_VOID_FL IS NULL
					AND t4047.C4047_FORECAST_ACCURACY_ID = t4048.C4047_FORECAST_ACCURACY_ID
					and t4047.C4047_VOID_FL is null
					and t4047.C4047_LOAD_DT = trunc(current_date,'MM')
					*/
					and t4021.C4020_DEMAND_MASTER_ID = t4020.C4020_DEMAND_MASTER_ID
					AND t4020.C901_DEMAND_TYPE=40020
					and  t4010.C4010_GROUP_ID = t4011.C4010_GROUP_ID
					and t4010.C4010_VOID_FL is null
					and t4011.C901_PART_PRICING_TYPE=52080
					and t4021.C4021_REF_ID = t4010.C4010_GROUP_ID
					and t4011.C205_PART_NUMBER_ID=t4042.c205_part_number_id
					--and t4011.C205_PART_NUMBER_ID in ('1119.0110')
					and t4021.C901_REF_TYPE = 40030
					and t4042.C4042_QTY >0
					and t4042.C901_TYPE =50562
					and C4040_demand_period_dt= p_load_dt
					and t4042.C4042_PERIOD = p_load_dt

			union all
				--ARCHIVE
				select t4040.C901_LEVEL_ID lvl_id,t4040.C901_LEVEL_VALUE lvl_value, t4020.C4020_DEMAND_NM
				,C4040_demand_period_dt period, t4042_arch.C205_PART_NUMBER_ID pnum
				,t4042_arch.C4042_QTY qty
				, t4020.C901_DEMAND_TYPE sheet_type
				from
				T4040_DEMAND_SHEET t4040,  T4042_DEMAND_SHEET_DETAIL_ARCH t4042_arch, T4020_DEMAND_MASTER t4020,
				-- T4048_FORECAST_ACCURACY_DETAILS t4048,
				T4021_DEMAND_MAPPING t4021,
				T4011_GROUP_DETAIL t4011,T4010_GROUP t4010
				-- , T4047_FORECAST_ACCURACY t4047
				where
				--	t4040.C901_LEVEL_ID =102580  -- company
					t4040.C901_ACCESS_TYPE =102623   -- editable
					and t4040.C4040_DEMAND_SHEET_ID = t4042_arch.C4040_DEMAND_SHEET_ID
					and t4020.C4020_DEMAND_MASTER_ID = t4040.C4020_DEMAND_MASTER_ID
					/*
					and t4042_arch.c205_part_number_id = t4048.c205_part_number_id
					AND T4048.c4048_VOID_FL IS NULL
					AND t4047.C4047_FORECAST_ACCURACY_ID = t4048.C4047_FORECAST_ACCURACY_ID
					and t4047.C4047_VOID_FL is null
					and t4047.C4047_LOAD_DT = trunc(current_date,'MM')
					*/
					and t4021.C4020_DEMAND_MASTER_ID = t4020.C4020_DEMAND_MASTER_ID
					AND t4020.C901_DEMAND_TYPE=40020
					and  t4010.C4010_GROUP_ID = t4011.C4010_GROUP_ID
					and t4010.C4010_VOID_FL is null
					and t4011.C901_PART_PRICING_TYPE=52080
					and t4021.C4021_REF_ID = t4010.C4010_GROUP_ID
					and t4011.C205_PART_NUMBER_ID=t4042_arch.c205_part_number_id
					--and t4011.C205_PART_NUMBER_ID in ('1119.0110')
					and t4021.C901_REF_TYPE = 40030
					and t4042_arch.C4042_QTY >0
					and t4042_arch.C901_TYPE =50562
					and C4040_demand_period_dt= p_load_dt
					and t4042_arch.C4042_PERIOD = p_load_dt


			) group by pnum,lvl_id,lvl_value
			;


	BEGIN

	dbms_output.put_line(':gm_ld_dmd_wtd_avg:'||P_fcst_accur_ID ||':'||p_load_dt||':'||P_4042_period_dmdwtdavg||':'||p_month_index||':');

	OPEN CUR_dmd_wtd_avg;
		LOOP
		FETCH CUR_dmd_wtd_avg BULK COLLECT INTO arr_pnum,arr_lvl_id,arr_lvl_value,arr_sales_wtd_avg,arr_cons_wtd_avg
				,arr_inhouse_wtd_avg LIMIT 500;

		FORALL i in 1.. arr_pnum.COUNT





		UPDATE 	T4048_FORECAST_ACCURACY_DETAILS
		SET
			C4048_1_SALES_WTD_AVG_QTY = DECODE(p_month_index,1,nvl(arr_sales_wtd_avg(i),0),C4048_1_SALES_WTD_AVG_QTY),
			C4048_2_SALES_WTD_AVG_QTY = DECODE(p_month_index,2,nvl(arr_sales_wtd_avg(i),0),C4048_2_SALES_WTD_AVG_QTY),
			C4048_3_SALES_WTD_AVG_QTY = DECODE(p_month_index,3,nvl(arr_sales_wtd_avg(i),0),C4048_3_SALES_WTD_AVG_QTY),
			C4048_4_SALES_WTD_AVG_QTY = DECODE(p_month_index,4,nvl(arr_sales_wtd_avg(i),0),C4048_4_SALES_WTD_AVG_QTY),
			C4048_5_SALES_WTD_AVG_QTY = DECODE(p_month_index,5,nvl(arr_sales_wtd_avg(i),0),C4048_5_SALES_WTD_AVG_QTY),
			C4048_6_SALES_WTD_AVG_QTY = DECODE(p_month_index,6,nvl(arr_sales_wtd_avg(i),0),C4048_6_SALES_WTD_AVG_QTY),
			C4048_7_SALES_WTD_AVG_QTY = DECODE(p_month_index,7,nvl(arr_sales_wtd_avg(i),0),C4048_7_SALES_WTD_AVG_QTY),
			C4048_8_SALES_WTD_AVG_QTY = DECODE(p_month_index,8,nvl(arr_sales_wtd_avg(i),0),C4048_8_SALES_WTD_AVG_QTY),
			C4048_9_SALES_WTD_AVG_QTY = DECODE(p_month_index,9,nvl(arr_sales_wtd_avg(i),0),C4048_9_SALES_WTD_AVG_QTY),
			C4048_10_SALES_WTD_AVG_QTY = DECODE(p_month_index,10,nvl(arr_sales_wtd_avg(i),0),C4048_10_SALES_WTD_AVG_QTY),
			C4048_11_SALES_WTD_AVG_QTY = DECODE(p_month_index,11,nvl(arr_sales_wtd_avg(i),0),C4048_11_SALES_WTD_AVG_QTY),
			C4048_12_SALES_WTD_AVG_QTY = DECODE(p_month_index,12,nvl(arr_sales_wtd_avg(i),0),C4048_12_SALES_WTD_AVG_QTY),


			C4048_1_LOAD_DT =  DECODE(p_month_index,1,p_load_dt,C4048_1_LOAD_DT),
			C4048_2_LOAD_DT =  DECODE(p_month_index,2,p_load_dt,C4048_2_LOAD_DT),
			C4048_3_LOAD_DT =  DECODE(p_month_index,3,p_load_dt,C4048_3_LOAD_DT),
			C4048_4_LOAD_DT =  DECODE(p_month_index,4,p_load_dt,C4048_4_LOAD_DT),
			C4048_5_LOAD_DT =  DECODE(p_month_index,5,p_load_dt,C4048_5_LOAD_DT),
			C4048_6_LOAD_DT =  DECODE(p_month_index,6,p_load_dt,C4048_6_LOAD_DT),
			C4048_7_LOAD_DT =  DECODE(p_month_index,7,p_load_dt,C4048_7_LOAD_DT),
			C4048_8_LOAD_DT =  DECODE(p_month_index,8,p_load_dt,C4048_8_LOAD_DT),
			C4048_9_LOAD_DT =  DECODE(p_month_index,9,p_load_dt,C4048_9_LOAD_DT),
			C4048_10_LOAD_DT =  DECODE(p_month_index,10,p_load_dt,C4048_10_LOAD_DT),
			C4048_11_LOAD_DT =  DECODE(p_month_index,11,p_load_dt,C4048_11_LOAD_DT),
			C4048_12_LOAD_DT =  DECODE(p_month_index,12,p_load_dt,C4048_12_LOAD_DT),

			C4048_1_FCST_DT =  DECODE(p_month_index,1,P_4042_period_dmdwtdavg,C4048_1_FCST_DT),
			C4048_2_FCST_DT =  DECODE(p_month_index,2,P_4042_period_dmdwtdavg,C4048_2_FCST_DT),
			C4048_3_FCST_DT =  DECODE(p_month_index,3,P_4042_period_dmdwtdavg,C4048_3_FCST_DT),
			C4048_4_FCST_DT =  DECODE(p_month_index,4,P_4042_period_dmdwtdavg,C4048_4_FCST_DT),
			C4048_5_FCST_DT =  DECODE(p_month_index,5,P_4042_period_dmdwtdavg,C4048_5_FCST_DT),
			C4048_6_FCST_DT =  DECODE(p_month_index,6,P_4042_period_dmdwtdavg,C4048_6_FCST_DT),
			C4048_7_FCST_DT =  DECODE(p_month_index,7,P_4042_period_dmdwtdavg,C4048_7_FCST_DT),
			C4048_8_FCST_DT =  DECODE(p_month_index,8,P_4042_period_dmdwtdavg,C4048_8_FCST_DT),
			C4048_9_FCST_DT =  DECODE(p_month_index,9,P_4042_period_dmdwtdavg,C4048_9_FCST_DT),
			C4048_10_FCST_DT =  DECODE(p_month_index,10,P_4042_period_dmdwtdavg,C4048_10_FCST_DT),
			C4048_11_FCST_DT =  DECODE(p_month_index,11,P_4042_period_dmdwtdavg,C4048_11_FCST_DT),
			C4048_12_FCST_DT =  DECODE(p_month_index,12,P_4042_period_dmdwtdavg,C4048_12_FCST_DT)

		WHERE
			 C4047_FORECAST_ACCURACY_ID = P_fcst_accur_ID AND
			 C205_PART_NUMBER_ID = arr_pnum(i) AND
			 C901_LEVEL_VALUE = arr_lvl_value(i) AND
			  C901_LEVEL_ID = arr_lvl_ID(i) AND
			 C4048_VOID_FL IS NULL;

		EXIT WHEN CUR_dmd_wtd_avg%NOTFOUND;
	    END LOOP;
	    CLOSE CUR_dmd_wtd_avg;

		UPDATE 	T4048_FORECAST_ACCURACY_DETAILS
		SET
			C4048_1_LOAD_DT =  DECODE(p_month_index,1,p_load_dt,C4048_1_LOAD_DT),
			C4048_2_LOAD_DT =  DECODE(p_month_index,2,p_load_dt,C4048_2_LOAD_DT),
			C4048_3_LOAD_DT =  DECODE(p_month_index,3,p_load_dt,C4048_3_LOAD_DT),
			C4048_4_LOAD_DT =  DECODE(p_month_index,4,p_load_dt,C4048_4_LOAD_DT),
			C4048_5_LOAD_DT =  DECODE(p_month_index,5,p_load_dt,C4048_5_LOAD_DT),
			C4048_6_LOAD_DT =  DECODE(p_month_index,6,p_load_dt,C4048_6_LOAD_DT),
			C4048_7_LOAD_DT =  DECODE(p_month_index,7,p_load_dt,C4048_7_LOAD_DT),
			C4048_8_LOAD_DT =  DECODE(p_month_index,8,p_load_dt,C4048_8_LOAD_DT),
			C4048_9_LOAD_DT =  DECODE(p_month_index,9,p_load_dt,C4048_9_LOAD_DT),
			C4048_10_LOAD_DT =  DECODE(p_month_index,10,p_load_dt,C4048_10_LOAD_DT),
			C4048_11_LOAD_DT =  DECODE(p_month_index,11,p_load_dt,C4048_11_LOAD_DT),
			C4048_12_LOAD_DT =  DECODE(p_month_index,12,p_load_dt,C4048_12_LOAD_DT),

			C4048_1_FCST_DT =  DECODE(p_month_index,1,P_4042_period_dmdwtdavg,C4048_1_FCST_DT),
			C4048_2_FCST_DT =  DECODE(p_month_index,2,P_4042_period_dmdwtdavg,C4048_2_FCST_DT),
			C4048_3_FCST_DT =  DECODE(p_month_index,3,P_4042_period_dmdwtdavg,C4048_3_FCST_DT),
			C4048_4_FCST_DT =  DECODE(p_month_index,4,P_4042_period_dmdwtdavg,C4048_4_FCST_DT),
			C4048_5_FCST_DT =  DECODE(p_month_index,5,P_4042_period_dmdwtdavg,C4048_5_FCST_DT),
			C4048_6_FCST_DT =  DECODE(p_month_index,6,P_4042_period_dmdwtdavg,C4048_6_FCST_DT),
			C4048_7_FCST_DT =  DECODE(p_month_index,7,P_4042_period_dmdwtdavg,C4048_7_FCST_DT),
			C4048_8_FCST_DT =  DECODE(p_month_index,8,P_4042_period_dmdwtdavg,C4048_8_FCST_DT),
			C4048_9_FCST_DT =  DECODE(p_month_index,9,P_4042_period_dmdwtdavg,C4048_9_FCST_DT),
			C4048_10_FCST_DT =  DECODE(p_month_index,10,P_4042_period_dmdwtdavg,C4048_10_FCST_DT),
			C4048_11_FCST_DT =  DECODE(p_month_index,11,P_4042_period_dmdwtdavg,C4048_11_FCST_DT),
			C4048_12_FCST_DT =  DECODE(p_month_index,12,P_4042_period_dmdwtdavg,C4048_12_FCST_DT)


		WHERE C4048_VOID_FL IS NULL
		AND C4047_FORECAST_ACCURACY_ID = P_fcst_accur_ID;

	END gm_ld_dmd_wtd_avg;

PROCEDURE gm_ld_forecast(
		P_fcst_accur_ID IN T4047_FORECAST_ACCURACY.C4047_FORECAST_ACCURACY_ID%TYPE,
		p_load_dt IN DATE,
		P_4042_period_fcst IN DATE,
		p_month_index IN  NUMBER
	)
	AS
		type pnum_array			IS     TABLE OF  T205_PART_NUMBER.c205_part_number_id%TYPE;
		type lvlid_array		IS     TABLE OF  T4040_DEMAND_SHEET.C901_LEVEL_ID%TYPE;
		type lvl_value_array		IS     TABLE OF  T4040_DEMAND_SHEET.C901_LEVEL_VALUE%TYPE;
		type sales_fcst_array	IS     TABLE OF  T4042_DEMAND_SHEET_DETAIL.C4042_QTY%TYPE;
		type cons_fcst_array		IS     TABLE OF  T4042_DEMAND_SHEET_DETAIL.C4042_QTY%TYPE;
		type inhouse_fcst_array		IS     TABLE OF  T4042_DEMAND_SHEET_DETAIL.C4042_QTY%TYPE;

		arr_pnum pnum_array;
		arr_lvl_id lvlid_array;
		arr_lvl_value lvl_value_array;
		arr_sales_fcst sales_fcst_array;
		arr_cons_fcst cons_fcst_array;
		arr_inhouse_fcst inhouse_fcst_array;


		CURSOR CUR_dmd_wtd_avg
		IS
			select pnum,lvl_id ,lvl_value, SUM(CASE WHEN sheet_type = 40020 THEN qty ELSE 0 END) sales,
			SUM(CASE WHEN sheet_type = 40021 THEN qty ELSE 0 END)cons,
			SUM(CASE WHEN sheet_type = 40022 THEN qty ELSE 0 END)in_house
			from (
				select t4040.C901_LEVEL_ID lvl_id,t4040.C901_LEVEL_VALUE lvl_value, t4020.C4020_DEMAND_NM
				,C4040_demand_period_dt period
				, t4042.C205_PART_NUMBER_ID pnum
				,t4042.C4042_QTY qty
				, t4020.C901_DEMAND_TYPE sheet_type
				from T4040_DEMAND_SHEET t4040,  T4042_DEMAND_SHEET_DETAIL t4042, T4020_DEMAND_MASTER t4020
				where C4040_demand_period_dt= p_load_dt
				--and t4040.C901_LEVEL_ID in (102580)
				and t4040.C901_ACCESS_TYPE =102623 -- editable
				and t4040.C4040_DEMAND_SHEET_ID = t4042.C4040_DEMAND_SHEET_ID
				and t4020.C4020_DEMAND_MASTER_ID = t4040.C4020_DEMAND_MASTER_ID
				and t4042.C4042_QTY >0
				and t4042.C901_TYPE =50563  -- Forecast
				 and t4042.C901_REF_TYPE=102660  -- Group Net Total
				 --and t4042.C205_PART_NUMBER_ID in ('1119.0110')
				-- For some sheet the demand wtg avg is not showing up in the right period,so we cannot use this filter.
				and t4042.C4042_PERIOD = P_4042_period_fcst

				UNION ALL
				--Archival

				select t4040.C901_LEVEL_ID lvl_id,t4040.C901_LEVEL_VALUE lvl_value, t4020.C4020_DEMAND_NM
				,C4040_demand_period_dt period
				, T4042_ARCH.C205_PART_NUMBER_ID pnum
				,T4042_ARCH.C4042_QTY qty
				, t4020.C901_DEMAND_TYPE sheet_type
				from T4040_DEMAND_SHEET t4040, T4042_DEMAND_SHEET_DETAIL_ARCH T4042_ARCH, T4020_DEMAND_MASTER t4020
				where C4040_demand_period_dt= p_load_dt
				--and t4040.C901_LEVEL_ID in (102580)
				and t4040.C901_ACCESS_TYPE =102623 -- editable
				and t4040.C4040_DEMAND_SHEET_ID = T4042_ARCH.C4040_DEMAND_SHEET_ID
				and t4020.C4020_DEMAND_MASTER_ID = t4040.C4020_DEMAND_MASTER_ID
				and T4042_ARCH.C4042_QTY >0
				--and T4042_ARCH.C205_PART_NUMBER_ID in ('1119.0110')
				and T4042_ARCH.C901_TYPE =50563  -- Forecast
				 and T4042_ARCH.C901_REF_TYPE=102660  -- Group Net Total
				-- For some sheet the demand wtg avg is not showing up in the right period,so we cannot use this filter.
				and T4042_ARCH.C4042_PERIOD = P_4042_period_fcst

			) group by pnum,lvl_id,lvl_value
			;


	BEGIN
	dbms_output.put_line(':gm_ld_forecast:'||P_fcst_accur_ID ||':'||p_load_dt||':'||P_4042_period_fcst||':'||p_month_index||':');

	OPEN CUR_dmd_wtd_avg;
		LOOP
		FETCH CUR_dmd_wtd_avg BULK COLLECT INTO arr_pnum,arr_lvl_id,arr_lvl_value,arr_sales_fcst,arr_cons_fcst
			,arr_inhouse_fcst LIMIT 500;

		FORALL i in 1.. arr_pnum.COUNT

		UPDATE 	T4048_FORECAST_ACCURACY_DETAILS
		SET
			C4048_1_SALES_FORECAST_QTY = DECODE(p_month_index,1,nvl(arr_sales_fcst(i),0),C4048_1_SALES_FORECAST_QTY),
			C4048_2_SALES_FORECAST_QTY = DECODE(p_month_index,2,nvl(arr_sales_fcst(i),0),C4048_2_SALES_FORECAST_QTY),
			C4048_3_SALES_FORECAST_QTY = DECODE(p_month_index,3,nvl(arr_sales_fcst(i),0),C4048_3_SALES_FORECAST_QTY),
			C4048_4_SALES_FORECAST_QTY = DECODE(p_month_index,4,nvl(arr_sales_fcst(i),0),C4048_4_SALES_FORECAST_QTY),
			C4048_5_SALES_FORECAST_QTY = DECODE(p_month_index,5,nvl(arr_sales_fcst(i),0),C4048_5_SALES_FORECAST_QTY),
			C4048_6_SALES_FORECAST_QTY = DECODE(p_month_index,6,nvl(arr_sales_fcst(i),0),C4048_6_SALES_FORECAST_QTY),
			C4048_7_SALES_FORECAST_QTY = DECODE(p_month_index,7,nvl(arr_sales_fcst(i),0),C4048_7_SALES_FORECAST_QTY),
			C4048_8_SALES_FORECAST_QTY = DECODE(p_month_index,8,nvl(arr_sales_fcst(i),0),C4048_8_SALES_FORECAST_QTY),
			C4048_9_SALES_FORECAST_QTY = DECODE(p_month_index,9,nvl(arr_sales_fcst(i),0),C4048_9_SALES_FORECAST_QTY),
			C4048_10_SALES_FORECAST_QTY = DECODE(p_month_index,10,nvl(arr_sales_fcst(i),0),C4048_10_SALES_FORECAST_QTY),
			C4048_11_SALES_FORECAST_QTY = DECODE(p_month_index,11,nvl(arr_sales_fcst(i),0),C4048_11_SALES_FORECAST_QTY),
			C4048_12_SALES_FORECAST_QTY = DECODE(p_month_index,12,nvl(arr_sales_fcst(i),0),C4048_12_SALES_FORECAST_QTY),

			C4048_1_LOAD_DT =  DECODE(p_month_index,1,p_load_dt,C4048_1_LOAD_DT),
			C4048_2_LOAD_DT =  DECODE(p_month_index,2,p_load_dt,C4048_2_LOAD_DT),
			C4048_3_LOAD_DT =  DECODE(p_month_index,3,p_load_dt,C4048_3_LOAD_DT),
			C4048_4_LOAD_DT =  DECODE(p_month_index,4,p_load_dt,C4048_4_LOAD_DT),
			C4048_5_LOAD_DT =  DECODE(p_month_index,5,p_load_dt,C4048_5_LOAD_DT),
			C4048_6_LOAD_DT =  DECODE(p_month_index,6,p_load_dt,C4048_6_LOAD_DT),
			C4048_7_LOAD_DT =  DECODE(p_month_index,7,p_load_dt,C4048_7_LOAD_DT),
			C4048_8_LOAD_DT =  DECODE(p_month_index,8,p_load_dt,C4048_8_LOAD_DT),
			C4048_9_LOAD_DT =  DECODE(p_month_index,9,p_load_dt,C4048_9_LOAD_DT),
			C4048_10_LOAD_DT =  DECODE(p_month_index,10,p_load_dt,C4048_10_LOAD_DT),
			C4048_11_LOAD_DT =  DECODE(p_month_index,11,p_load_dt,C4048_11_LOAD_DT),
			C4048_12_LOAD_DT =  DECODE(p_month_index,12,p_load_dt,C4048_12_LOAD_DT),

			C4048_1_FCST_DT =  DECODE(p_month_index,1,P_4042_period_fcst,C4048_1_FCST_DT),
			C4048_2_FCST_DT =  DECODE(p_month_index,2,P_4042_period_fcst,C4048_2_FCST_DT),
			C4048_3_FCST_DT =  DECODE(p_month_index,3,P_4042_period_fcst,C4048_3_FCST_DT),
			C4048_4_FCST_DT =  DECODE(p_month_index,4,P_4042_period_fcst,C4048_4_FCST_DT),
			C4048_5_FCST_DT =  DECODE(p_month_index,5,P_4042_period_fcst,C4048_5_FCST_DT),
			C4048_6_FCST_DT =  DECODE(p_month_index,6,P_4042_period_fcst,C4048_6_FCST_DT),
			C4048_7_FCST_DT =  DECODE(p_month_index,7,P_4042_period_fcst,C4048_7_FCST_DT),
			C4048_8_FCST_DT =  DECODE(p_month_index,8,P_4042_period_fcst,C4048_8_FCST_DT),
			C4048_9_FCST_DT =  DECODE(p_month_index,9,P_4042_period_fcst,C4048_9_FCST_DT),
			C4048_10_FCST_DT =  DECODE(p_month_index,10,P_4042_period_fcst,C4048_10_FCST_DT),
			C4048_11_FCST_DT =  DECODE(p_month_index,11,P_4042_period_fcst,C4048_11_FCST_DT),
			C4048_12_FCST_DT =  DECODE(p_month_index,12,P_4042_period_fcst,C4048_12_FCST_DT)

		WHERE
			  C4047_FORECAST_ACCURACY_ID = P_fcst_accur_ID AND
			 C205_PART_NUMBER_ID = arr_pnum(i) AND
			 C901_LEVEL_VALUE = arr_lvl_value(i) AND
			  C901_LEVEL_ID = arr_lvl_ID(i) AND
			 C4048_VOID_FL IS NULL;

		EXIT WHEN CUR_dmd_wtd_avg%NOTFOUND;
	    END LOOP;
	    CLOSE CUR_dmd_wtd_avg;


	END gm_ld_forecast;

	PROCEDURE gm_ld_actuals(
		P_fcst_accur_ID IN T4047_FORECAST_ACCURACY.C4047_FORECAST_ACCURACY_ID%TYPE,
		p_gop_load_dt IN DATE,
		P_fcst_Dt IN DATE,

		p_load_dt IN DATE,
		P_4042_period_dmd IN DATE,
		p_month_index IN  NUMBER
	)
	AS

		type pnum_array			IS     TABLE OF  T205_PART_NUMBER.c205_part_number_id%TYPE;
		type lvlid_array		IS     TABLE OF  T4040_DEMAND_SHEET.C901_LEVEL_ID%TYPE;
		type lvl_value_array		IS     TABLE OF  T4040_DEMAND_SHEET.C901_LEVEL_VALUE%TYPE;
		type sales_dmd_array	IS     TABLE OF  T4042_DEMAND_SHEET_DETAIL.C4042_QTY%TYPE;
		type cons_dmd_array		IS     TABLE OF  T4042_DEMAND_SHEET_DETAIL.C4042_QTY%TYPE;
		type inhouse_dmd_array		IS     TABLE OF  T4042_DEMAND_SHEET_DETAIL.C4042_QTY%TYPE;

		arr_pnum pnum_array;
		arr_lvl_id lvlid_array;
		arr_lvl_value lvl_value_array;
		arr_sales_dmd sales_dmd_array;
		arr_cons_dmd cons_dmd_array;
		arr_inhouse_dmd inhouse_dmd_array;


		CURSOR CUR_dmd_wtd_avg
		IS
			select pnum,lvl_id ,lvl_value, SUM(CASE WHEN sheet_type = 40020 THEN qty ELSE 0 END) sales,
			SUM(CASE WHEN sheet_type = 40021 THEN qty ELSE 0 END)cons,
			SUM(CASE WHEN sheet_type = 40022 THEN qty ELSE 0 END)in_house
			from (
				select t4040.C901_LEVEL_ID lvl_id,t4040.C901_LEVEL_VALUE lvl_value, t4020.C4020_DEMAND_NM
				,C4040_demand_period_dt period
				, t4042.C205_PART_NUMBER_ID pnum
				,t4042.C4042_QTY qty
				, t4020.C901_DEMAND_TYPE sheet_type

				FROM T4040_DEMAND_SHEET t4040,  T4042_DEMAND_SHEET_DETAIL t4042, T4020_DEMAND_MASTER t4020,
				--T4048_FORECAST_ACCURACY_DETAILS t4048,
				T4021_DEMAND_MAPPING t4021,
				T4011_GROUP_DETAIL t4011,T4010_GROUP t4010
				--, T4047_FORECAST_ACCURACY t4047
				where
					-- t4040.C901_LEVEL_ID in (102580)
					 t4040.C901_ACCESS_TYPE =102623 -- editable
					and t4040.C4040_DEMAND_SHEET_ID = t4042.C4040_DEMAND_SHEET_ID
					and t4020.C4020_DEMAND_MASTER_ID = t4040.C4020_DEMAND_MASTER_ID
					/*
					and t4042.c205_part_number_id = t4048.c205_part_number_id
					AND T4048.c4048_VOID_FL IS NULL
					AND  t4047.C4047_FORECAST_ACCURACY_ID = t4048.C4047_FORECAST_ACCURACY_ID
					and t4047.C4047_VOID_FL is null
					and t4047.C4047_LOAD_DT = trunc(current_date,'MM')
					*/
					and t4021.C4020_DEMAND_MASTER_ID = t4020.C4020_DEMAND_MASTER_ID
					AND t4020.C901_DEMAND_TYPE=40020
					and  t4010.C4010_GROUP_ID = t4011.C4010_GROUP_ID
					and t4010.C4010_VOID_FL is null
					and t4011.C901_PART_PRICING_TYPE=52080
					--and T4042.C205_PART_NUMBER_ID in ('1119.0110')
					and t4021.C4021_REF_ID = t4010.C4010_GROUP_ID
					and t4011.C205_PART_NUMBER_ID=t4042.c205_part_number_id
					and t4021.C901_REF_TYPE = 40030
					and t4042.C4042_QTY >0
					and t4042.C901_TYPE =50560
					and C4040_demand_period_dt= p_load_dt
					and t4042.C4042_PERIOD = P_4042_period_dmd

					--Archival
					UNION ALL

					select t4040.C901_LEVEL_ID lvl_id,t4040.C901_LEVEL_VALUE lvl_value, t4020.C4020_DEMAND_NM
				,C4040_demand_period_dt period
				, t4042_ARCH.C205_PART_NUMBER_ID pnum
				,t4042_ARCH.C4042_QTY qty
				, t4020.C901_DEMAND_TYPE sheet_type

				FROM T4040_DEMAND_SHEET t4040,  T4042_DEMAND_SHEET_DETAIL_ARCH t4042_ARCH, T4020_DEMAND_MASTER t4020,
				--T4048_FORECAST_ACCURACY_DETAILS t4048,
				T4021_DEMAND_MAPPING t4021,
				T4011_GROUP_DETAIL t4011,T4010_GROUP t4010
				--, T4047_FORECAST_ACCURACY t4047
				where
					--and t4040.C901_LEVEL_ID in (102580)
					 t4040.C901_ACCESS_TYPE =102623 -- editable
					and t4040.C4040_DEMAND_SHEET_ID = t4042_ARCH.C4040_DEMAND_SHEET_ID
					and t4020.C4020_DEMAND_MASTER_ID = t4040.C4020_DEMAND_MASTER_ID
					/*
					and t4042_ARCH.c205_part_number_id = t4048.c205_part_number_id
					AND T4048.c4048_VOID_FL IS NULL
					AND  t4047.C4047_FORECAST_ACCURACY_ID = t4048.C4047_FORECAST_ACCURACY_ID
					and t4047.C4047_VOID_FL is null
					and t4047.C4047_LOAD_DT = trunc(current_date,'MM')
					*/
					and t4021.C4020_DEMAND_MASTER_ID = t4020.C4020_DEMAND_MASTER_ID
					AND t4020.C901_DEMAND_TYPE=40020
					and  t4010.C4010_GROUP_ID = t4011.C4010_GROUP_ID
					and t4010.C4010_VOID_FL is null
					and t4011.C901_PART_PRICING_TYPE=52080
					and t4021.C4021_REF_ID = t4010.C4010_GROUP_ID
					--and T4042_ARCH.C205_PART_NUMBER_ID in ('1119.0110')
					and t4011.C205_PART_NUMBER_ID=t4042_ARCH.c205_part_number_id
					and t4021.C901_REF_TYPE = 40030
					and t4042_ARCH.C4042_QTY >0
					and t4042_ARCH.C901_TYPE =50560
					and C4040_demand_period_dt= p_load_dt
					and t4042_ARCH.C4042_PERIOD = P_4042_period_dmd

			) group by pnum,lvl_id,lvl_value
			;


	BEGIN

	dbms_output.put_line('gm_ld_actuals:'||P_fcst_accur_ID||':'||p_gop_load_dt||':'||P_fcst_Dt||':p_load_dt:'||p_load_dt||':P_4042_period_dmd:'||P_4042_period_dmd);

	OPEN CUR_dmd_wtd_avg;
		LOOP
		FETCH CUR_dmd_wtd_avg BULK COLLECT INTO arr_pnum,arr_lvl_id,arr_lvl_value,arr_sales_dmd,arr_cons_dmd
			,arr_inhouse_dmd LIMIT 500;

		FORALL i in 1.. arr_pnum.COUNT

		UPDATE 	T4048_FORECAST_ACCURACY_DETAILS
		SET
			C4048_1_ACTUAL_SALES_QTY = DECODE(p_month_index,1,nvl(arr_sales_dmd(i),0),C4048_1_ACTUAL_SALES_QTY),
			C4048_2_ACTUAL_SALES_QTY = DECODE(p_month_index,2,nvl(arr_sales_dmd(i),0),C4048_2_ACTUAL_SALES_QTY),
			C4048_3_ACTUAL_SALES_QTY = DECODE(p_month_index,3,nvl(arr_sales_dmd(i),0),C4048_3_ACTUAL_SALES_QTY),
			C4048_4_ACTUAL_SALES_QTY = DECODE(p_month_index,4,nvl(arr_sales_dmd(i),0),C4048_4_ACTUAL_SALES_QTY),
			C4048_5_ACTUAL_SALES_QTY = DECODE(p_month_index,5,nvl(arr_sales_dmd(i),0),C4048_5_ACTUAL_SALES_QTY),
			C4048_6_ACTUAL_SALES_QTY = DECODE(p_month_index,6,nvl(arr_sales_dmd(i),0),C4048_6_ACTUAL_SALES_QTY)	,
			C4048_7_ACTUAL_SALES_QTY = DECODE(p_month_index,7,nvl(arr_sales_dmd(i),0),C4048_7_ACTUAL_SALES_QTY),
			C4048_8_ACTUAL_SALES_QTY = DECODE(p_month_index,8,nvl(arr_sales_dmd(i),0),C4048_8_ACTUAL_SALES_QTY),
			C4048_9_ACTUAL_SALES_QTY = DECODE(p_month_index,9,nvl(arr_sales_dmd(i),0),C4048_9_ACTUAL_SALES_QTY),
			C4048_10_ACTUAL_SALES_QTY = DECODE(p_month_index,10,nvl(arr_sales_dmd(i),0),C4048_10_ACTUAL_SALES_QTY),
			C4048_11_ACTUAL_SALES_QTY = DECODE(p_month_index,11,nvl(arr_sales_dmd(i),0),C4048_11_ACTUAL_SALES_QTY),
			C4048_12_ACTUAL_SALES_QTY = DECODE(p_month_index,12,nvl(arr_sales_dmd(i),0),C4048_12_ACTUAL_SALES_QTY),

			C4048_1_LOAD_DT =  NVL(C4048_1_LOAD_DT,DECODE(p_month_index,1,p_gop_load_dt,C4048_1_LOAD_DT)),
			C4048_2_LOAD_DT =  NVL(C4048_2_LOAD_DT,DECODE(p_month_index,2,p_gop_load_dt,C4048_2_LOAD_DT)),
			C4048_3_LOAD_DT =  NVL(C4048_3_LOAD_DT,DECODE(p_month_index,3,p_gop_load_dt,C4048_3_LOAD_DT)),
			C4048_4_LOAD_DT =  NVL(C4048_4_LOAD_DT,DECODE(p_month_index,4,p_gop_load_dt,C4048_4_LOAD_DT)),
			C4048_5_LOAD_DT =  NVL(C4048_5_LOAD_DT,DECODE(p_month_index,5,p_gop_load_dt,C4048_5_LOAD_DT)),
			C4048_6_LOAD_DT =  NVL(C4048_6_LOAD_DT,DECODE(p_month_index,6,p_gop_load_dt,C4048_6_LOAD_DT)),
			C4048_7_LOAD_DT =  NVL(C4048_7_LOAD_DT,DECODE(p_month_index,7,p_gop_load_dt,C4048_7_LOAD_DT)),
			C4048_8_LOAD_DT =  NVL(C4048_8_LOAD_DT,DECODE(p_month_index,8,p_gop_load_dt,C4048_8_LOAD_DT)),
			C4048_9_LOAD_DT =  NVL(C4048_9_LOAD_DT,DECODE(p_month_index,9,p_gop_load_dt,C4048_9_LOAD_DT)),
			C4048_10_LOAD_DT =  NVL(C4048_10_LOAD_DT,DECODE(p_month_index,10,p_gop_load_dt,C4048_10_LOAD_DT)),
			C4048_11_LOAD_DT = NVL(C4048_11_LOAD_DT, DECODE(p_month_index,11,p_gop_load_dt,C4048_11_LOAD_DT)),
			C4048_12_LOAD_DT = NVL(C4048_12_LOAD_DT, DECODE(p_month_index,12,p_gop_load_dt,C4048_12_LOAD_DT)),

			C4048_1_FCST_DT =  NVL(C4048_1_FCST_DT,DECODE(p_month_index,1,P_fcst_Dt,C4048_1_FCST_DT)),
			C4048_2_FCST_DT =  NVL(C4048_2_FCST_DT,DECODE(p_month_index,2,P_fcst_Dt,C4048_2_FCST_DT)),
			C4048_3_FCST_DT =  NVL(C4048_3_FCST_DT,DECODE(p_month_index,3,P_fcst_Dt,C4048_3_FCST_DT)),
			C4048_4_FCST_DT =  NVL(C4048_4_FCST_DT,DECODE(p_month_index,4,P_fcst_Dt,C4048_4_FCST_DT)),
			C4048_5_FCST_DT =  NVL(C4048_5_FCST_DT,DECODE(p_month_index,5,P_fcst_Dt,C4048_5_FCST_DT)),
			C4048_6_FCST_DT =  NVL(C4048_6_FCST_DT,DECODE(p_month_index,6,P_fcst_Dt,C4048_6_FCST_DT)),
			C4048_7_FCST_DT =  NVL(C4048_7_FCST_DT,DECODE(p_month_index,7,P_fcst_Dt,C4048_7_FCST_DT)),
			C4048_8_FCST_DT =  NVL(C4048_8_FCST_DT,DECODE(p_month_index,8,P_fcst_Dt,C4048_8_FCST_DT)),
			C4048_9_FCST_DT =  NVL(C4048_9_FCST_DT,DECODE(p_month_index,9,P_fcst_Dt,C4048_9_FCST_DT)),
			C4048_10_FCST_DT = NVL(C4048_10_FCST_DT, DECODE(p_month_index,10,P_fcst_Dt,C4048_10_FCST_DT)),
			C4048_11_FCST_DT = NVL(C4048_11_FCST_DT, DECODE(p_month_index,11,P_fcst_Dt,C4048_11_FCST_DT)),
			C4048_12_FCST_DT = NVL(C4048_12_FCST_DT, DECODE(p_month_index,12,P_fcst_Dt,C4048_12_FCST_DT))

			--,C4048_DMD_CYCLE_MONTH = p_load_dt
			--,C4048_DMD_MONTH = P_4042_period_dmd
		WHERE
			 C4047_FORECAST_ACCURACY_ID = P_fcst_accur_ID AND
			 C205_PART_NUMBER_ID = arr_pnum(i) AND
			 C901_LEVEL_VALUE = arr_lvl_value(i) AND
			  C901_LEVEL_ID = arr_lvl_ID(i) AND
			 C4048_VOID_FL IS NULL;

		EXIT WHEN CUR_dmd_wtd_avg%NOTFOUND;
	    END LOOP;
	    CLOSE CUR_dmd_wtd_avg;


	END gm_ld_actuals;



	PROCEDURE gm_ld_calc_variance(
		P_fcst_accur_ID IN T4047_FORECAST_ACCURACY.C4047_FORECAST_ACCURACY_ID%TYPE,
		p_month_index IN  NUMBER
	)
	AS


		v number;
		pnum varchar2(20);
		sales_fcst number(10,10);
		sales_fcst_pct number(10,10);

		CURSOR CUR_Variance
		IS

			select C205_PART_NUMBER_ID pnum,C901_LEVEL_ID lvl_id , C901_LEVEL_VALUE lvl_val,
					(nvl(C4048_1_SALES_WTD_AVG_QTY,0)-nvl(C4048_1_ACTUAL_SALES_QTY,0)) sales_var_1,
				   ABS((nvl(C4048_1_SALES_WTD_AVG_QTY,0)-nvl(C4048_1_ACTUAL_SALES_QTY,0))) abs_sales_var_1

					,DECODE(NVL(C4048_1_SALES_WTD_AVG_QTY,0),0,0,
					ROUND(((nvl(C4048_1_SALES_WTD_AVG_QTY,0)-nvl(C4048_1_ACTUAL_SALES_QTY,0)))/NVL(C4048_1_SALES_WTD_AVG_QTY,1),4)) sales_var_pct_1
					,abs(DECODE(NVL(C4048_1_SALES_WTD_AVG_QTY,0),0,0,
					ROUND(((nvl(C4048_1_SALES_WTD_AVG_QTY,0)-nvl(C4048_1_ACTUAL_SALES_QTY,0)))/NVL(C4048_1_SALES_WTD_AVG_QTY,1),4))) abs_sales_var_pct_1
					,(nvl(C4048_1_SALES_FORECAST_QTY,0)-nvl(C4048_1_ACTUAL_SALES_QTY,0)) FCST_VAR_1
					,ABS(nvl(C4048_1_SALES_FORECAST_QTY,0)-nvl(C4048_1_ACTUAL_SALES_QTY,0)) ABS_FCST_VAR_1
					,

					ROUND(DECODE(NVL(C4048_1_SALES_FORECAST_QTY,0),0,0,
					round((nvl(C4048_1_SALES_FORECAST_QTY,0)-nvl(C4048_1_ACTUAL_SALES_QTY,0))/nvl(C4048_1_SALES_FORECAST_QTY,1),4)),4)
					 FCST_VAR_PCT_1,

					 abs(ROUND(DECODE(NVL(C4048_1_SALES_FORECAST_QTY,0),0,0,
					round((nvl(C4048_1_SALES_FORECAST_QTY,0)-nvl(C4048_1_ACTUAL_SALES_QTY,0))/nvl(C4048_1_SALES_FORECAST_QTY,1),4)),4))
					 abs_FCST_VAR_PCT_1,
					--2
					(nvl(C4048_2_SALES_WTD_AVG_QTY,0)-nvl(C4048_2_ACTUAL_SALES_QTY,0)) sales_var_2,
				   ABS((nvl(C4048_2_SALES_WTD_AVG_QTY,0)-nvl(C4048_2_ACTUAL_SALES_QTY,0))) abs_sales_var_2

					,DECODE(NVL(C4048_2_SALES_WTD_AVG_QTY,0),0,0,
					ROUND(((nvl(C4048_2_SALES_WTD_AVG_QTY,0)-nvl(C4048_2_ACTUAL_SALES_QTY,0)))/NVL(C4048_2_SALES_WTD_AVG_QTY,1),4)) sales_var_pct_2
					,abs(DECODE(NVL(C4048_2_SALES_WTD_AVG_QTY,0),0,0,
					ROUND(((nvl(C4048_2_SALES_WTD_AVG_QTY,0)-nvl(C4048_2_ACTUAL_SALES_QTY,0)))/NVL(C4048_2_SALES_WTD_AVG_QTY,1),4))) abs_sales_var_pct_2
					,(nvl(C4048_2_SALES_FORECAST_QTY,0)-nvl(C4048_2_ACTUAL_SALES_QTY,0)) FCST_VAR_2
					,ABS(nvl(C4048_2_SALES_FORECAST_QTY,0)-nvl(C4048_2_ACTUAL_SALES_QTY,0)) ABS_FCST_VAR_2
					,

					ROUND(DECODE(NVL(C4048_2_SALES_FORECAST_QTY,0),0,0,
					round((nvl(C4048_2_SALES_FORECAST_QTY,0)-nvl(C4048_2_ACTUAL_SALES_QTY,0))/nvl(C4048_2_SALES_FORECAST_QTY,1),4)),4)
					 FCST_VAR_PCT_2,
					abs(ROUND(DECODE(NVL(C4048_2_SALES_FORECAST_QTY,0),0,0,
					round((nvl(C4048_2_SALES_FORECAST_QTY,0)-nvl(C4048_2_ACTUAL_SALES_QTY,0))/nvl(C4048_2_SALES_FORECAST_QTY,1),4)),4))
					 abs_FCST_VAR_PCT_2,
					 --3
					 (nvl(C4048_3_SALES_WTD_AVG_QTY,0)-nvl(C4048_3_ACTUAL_SALES_QTY,0)) sales_var_3,
				   ABS((nvl(C4048_3_SALES_WTD_AVG_QTY,0)-nvl(C4048_3_ACTUAL_SALES_QTY,0))) abs_sales_var_3

					,DECODE(NVL(C4048_3_SALES_WTD_AVG_QTY,0),0,0,
					ROUND(((nvl(C4048_3_SALES_WTD_AVG_QTY,0)-nvl(C4048_3_ACTUAL_SALES_QTY,0)))/NVL(C4048_3_SALES_WTD_AVG_QTY,1),4)) sales_var_pct_3
					,abs(DECODE(NVL(C4048_3_SALES_WTD_AVG_QTY,0),0,0,
					ROUND(((nvl(C4048_3_SALES_WTD_AVG_QTY,0)-nvl(C4048_3_ACTUAL_SALES_QTY,0)))/NVL(C4048_3_SALES_WTD_AVG_QTY,1),4))) abs_sales_var_pct_3
					,(nvl(C4048_3_SALES_FORECAST_QTY,0)-nvl(C4048_3_ACTUAL_SALES_QTY,0)) FCST_VAR_3
					,ABS(nvl(C4048_3_SALES_FORECAST_QTY,0)-nvl(C4048_3_ACTUAL_SALES_QTY,0)) ABS_FCST_VAR_3
					,

					ROUND(DECODE(NVL(C4048_3_SALES_FORECAST_QTY,0),0,0,
					round((nvl(C4048_3_SALES_FORECAST_QTY,0)-nvl(C4048_3_ACTUAL_SALES_QTY,0))/nvl(C4048_3_SALES_FORECAST_QTY,1),4)),4)
					 FCST_VAR_PCT_3,
					abs(ROUND(DECODE(NVL(C4048_3_SALES_FORECAST_QTY,0),0,0,
					round((nvl(C4048_3_SALES_FORECAST_QTY,0)-nvl(C4048_3_ACTUAL_SALES_QTY,0))/nvl(C4048_3_SALES_FORECAST_QTY,1),4)),4))
					 abs_FCST_VAR_PCT_3,
					--4
					(nvl(C4048_4_SALES_WTD_AVG_QTY,0)-nvl(C4048_4_ACTUAL_SALES_QTY,0)) sales_var_4,
				   ABS((nvl(C4048_4_SALES_WTD_AVG_QTY,0)-nvl(C4048_4_ACTUAL_SALES_QTY,0))) abs_sales_var_4

					,DECODE(NVL(C4048_4_SALES_WTD_AVG_QTY,0),0,0,
					ROUND(((nvl(C4048_4_SALES_WTD_AVG_QTY,0)-nvl(C4048_4_ACTUAL_SALES_QTY,0)))/NVL(C4048_4_SALES_WTD_AVG_QTY,1),4)) sales_var_pct_4
					,abs(DECODE(NVL(C4048_4_SALES_WTD_AVG_QTY,0),0,0,
					ROUND(((nvl(C4048_4_SALES_WTD_AVG_QTY,0)-nvl(C4048_4_ACTUAL_SALES_QTY,0)))/NVL(C4048_4_SALES_WTD_AVG_QTY,1),4))) abs_sales_var_pct_4
					,(nvl(C4048_4_SALES_FORECAST_QTY,0)-nvl(C4048_4_ACTUAL_SALES_QTY,0)) FCST_VAR_4
					,ABS(nvl(C4048_4_SALES_FORECAST_QTY,0)-nvl(C4048_4_ACTUAL_SALES_QTY,0)) ABS_FCST_VAR_4
					,

					ROUND(DECODE(NVL(C4048_4_SALES_FORECAST_QTY,0),0,0,
					round((nvl(C4048_4_SALES_FORECAST_QTY,0)-nvl(C4048_4_ACTUAL_SALES_QTY,0))/nvl(C4048_4_SALES_FORECAST_QTY,1),4)),4)
					 FCST_VAR_PCT_4,
					 abs(ROUND(DECODE(NVL(C4048_4_SALES_FORECAST_QTY,0),0,0,
					round((nvl(C4048_4_SALES_FORECAST_QTY,0)-nvl(C4048_4_ACTUAL_SALES_QTY,0))/nvl(C4048_4_SALES_FORECAST_QTY,1),4)),4))
					 abs_FCST_VAR_PCT_4,
					--5
					(nvl(C4048_5_SALES_WTD_AVG_QTY,0)-nvl(C4048_5_ACTUAL_SALES_QTY,0)) sales_var_5,
				   ABS((nvl(C4048_5_SALES_WTD_AVG_QTY,0)-nvl(C4048_5_ACTUAL_SALES_QTY,0))) abs_sales_var_5

					,DECODE(NVL(C4048_5_SALES_WTD_AVG_QTY,0),0,0,
					ROUND(((nvl(C4048_5_SALES_WTD_AVG_QTY,0)-nvl(C4048_5_ACTUAL_SALES_QTY,0)))/NVL(C4048_5_SALES_WTD_AVG_QTY,1),4)) sales_var_pct_5
					,abs(DECODE(NVL(C4048_5_SALES_WTD_AVG_QTY,0),0,0,
					ROUND(((nvl(C4048_5_SALES_WTD_AVG_QTY,0)-nvl(C4048_5_ACTUAL_SALES_QTY,0)))/NVL(C4048_5_SALES_WTD_AVG_QTY,1),4))) abs_sales_var_pct_5
					,(nvl(C4048_5_SALES_FORECAST_QTY,0)-nvl(C4048_5_ACTUAL_SALES_QTY,0)) FCST_VAR_5
					,ABS(nvl(C4048_5_SALES_FORECAST_QTY,0)-nvl(C4048_5_ACTUAL_SALES_QTY,0)) ABS_FCST_VAR_5
					,

					ROUND(DECODE(NVL(C4048_5_SALES_FORECAST_QTY,0),0,0,
					round((nvl(C4048_5_SALES_FORECAST_QTY,0)-nvl(C4048_5_ACTUAL_SALES_QTY,0))/nvl(C4048_5_SALES_FORECAST_QTY,1),4)),4)
					 FCST_VAR_PCT_5,

					 abs(ROUND(DECODE(NVL(C4048_5_SALES_FORECAST_QTY,0),0,0,
					round((nvl(C4048_5_SALES_FORECAST_QTY,0)-nvl(C4048_5_ACTUAL_SALES_QTY,0))/nvl(C4048_5_SALES_FORECAST_QTY,1),4)),4))
					 abs_FCST_VAR_PCT_5,
					--6
					(nvl(C4048_6_SALES_WTD_AVG_QTY,0)-nvl(C4048_6_ACTUAL_SALES_QTY,0)) sales_var_6,
				   ABS((nvl(C4048_6_SALES_WTD_AVG_QTY,0)-nvl(C4048_6_ACTUAL_SALES_QTY,0))) abs_sales_var_6

					,DECODE(NVL(C4048_6_SALES_WTD_AVG_QTY,0),0,0,
					ROUND(((nvl(C4048_6_SALES_WTD_AVG_QTY,0)-nvl(C4048_6_ACTUAL_SALES_QTY,0)))/NVL(C4048_6_SALES_WTD_AVG_QTY,1),4)) sales_var_pct_6
					,abs(DECODE(NVL(C4048_6_SALES_WTD_AVG_QTY,0),0,0,
					ROUND(((nvl(C4048_6_SALES_WTD_AVG_QTY,0)-nvl(C4048_6_ACTUAL_SALES_QTY,0)))/NVL(C4048_6_SALES_WTD_AVG_QTY,1),4))) abs_sales_var_pct_6
					,(nvl(C4048_6_SALES_FORECAST_QTY,0)-nvl(C4048_6_ACTUAL_SALES_QTY,0)) FCST_VAR_6
					,ABS(nvl(C4048_6_SALES_FORECAST_QTY,0)-nvl(C4048_6_ACTUAL_SALES_QTY,0)) ABS_FCST_VAR_6
					,

					ROUND(DECODE(NVL(C4048_6_SALES_FORECAST_QTY,0),0,0,
					round((nvl(C4048_6_SALES_FORECAST_QTY,0)-nvl(C4048_6_ACTUAL_SALES_QTY,0))/nvl(C4048_6_SALES_FORECAST_QTY,1),4)),4)
					 FCST_VAR_PCT_6,
					 abs(ROUND(DECODE(NVL(C4048_6_SALES_FORECAST_QTY,0),0,0,
					round((nvl(C4048_6_SALES_FORECAST_QTY,0)-nvl(C4048_6_ACTUAL_SALES_QTY,0))/nvl(C4048_6_SALES_FORECAST_QTY,1),4)),4))
					 abs_FCST_VAR_PCT_6,

					--7
					(nvl(C4048_7_SALES_WTD_AVG_QTY,0)-nvl(C4048_7_ACTUAL_SALES_QTY,0)) sales_var_7,
				   ABS((nvl(C4048_7_SALES_WTD_AVG_QTY,0)-nvl(C4048_7_ACTUAL_SALES_QTY,0))) abs_sales_var_7

					,DECODE(NVL(C4048_7_SALES_WTD_AVG_QTY,0),0,0,
					ROUND(((nvl(C4048_7_SALES_WTD_AVG_QTY,0)-nvl(C4048_7_ACTUAL_SALES_QTY,0)))/NVL(C4048_7_SALES_WTD_AVG_QTY,1),4)) sales_var_pct_7
					,abs(DECODE(NVL(C4048_7_SALES_WTD_AVG_QTY,0),0,0,
					ROUND(((nvl(C4048_7_SALES_WTD_AVG_QTY,0)-nvl(C4048_7_ACTUAL_SALES_QTY,0)))/NVL(C4048_7_SALES_WTD_AVG_QTY,1),4))) abs_sales_var_pct_7
					,(nvl(C4048_7_SALES_FORECAST_QTY,0)-nvl(C4048_7_ACTUAL_SALES_QTY,0)) FCST_VAR_7
					,ABS(nvl(C4048_7_SALES_FORECAST_QTY,0)-nvl(C4048_7_ACTUAL_SALES_QTY,0)) ABS_FCST_VAR_7
					,

					ROUND(DECODE(NVL(C4048_7_SALES_FORECAST_QTY,0),0,0,
					round((nvl(C4048_7_SALES_FORECAST_QTY,0)-nvl(C4048_7_ACTUAL_SALES_QTY,0))/nvl(C4048_7_SALES_FORECAST_QTY,1),4)),4)
					 FCST_VAR_PCT_7,
					 abs(ROUND(DECODE(NVL(C4048_7_SALES_FORECAST_QTY,0),0,0,
					round((nvl(C4048_7_SALES_FORECAST_QTY,0)-nvl(C4048_7_ACTUAL_SALES_QTY,0))/nvl(C4048_7_SALES_FORECAST_QTY,1),4)),4))
					 abs_FCST_VAR_PCT_7,

					 --8
					 (nvl(C4048_8_SALES_WTD_AVG_QTY,0)-nvl(C4048_8_ACTUAL_SALES_QTY,0)) sales_var_8,
				   ABS((nvl(C4048_8_SALES_WTD_AVG_QTY,0)-nvl(C4048_8_ACTUAL_SALES_QTY,0))) abs_sales_var_8

					,DECODE(NVL(C4048_8_SALES_WTD_AVG_QTY,0),0,0,
					ROUND(((nvl(C4048_8_SALES_WTD_AVG_QTY,0)-nvl(C4048_8_ACTUAL_SALES_QTY,0)))/NVL(C4048_8_SALES_WTD_AVG_QTY,1),4)) sales_var_pct_8
					,abs(DECODE(NVL(C4048_8_SALES_WTD_AVG_QTY,0),0,0,
					ROUND(((nvl(C4048_8_SALES_WTD_AVG_QTY,0)-nvl(C4048_8_ACTUAL_SALES_QTY,0)))/NVL(C4048_8_SALES_WTD_AVG_QTY,1),4))) abs_sales_var_pct_8
					,(nvl(C4048_8_SALES_FORECAST_QTY,0)-nvl(C4048_8_ACTUAL_SALES_QTY,0)) FCST_VAR_8
					,ABS(nvl(C4048_8_SALES_FORECAST_QTY,0)-nvl(C4048_8_ACTUAL_SALES_QTY,0)) ABS_FCST_VAR_8
					,

					ROUND(DECODE(NVL(C4048_8_SALES_FORECAST_QTY,0),0,0,
					round((nvl(C4048_8_SALES_FORECAST_QTY,0)-nvl(C4048_8_ACTUAL_SALES_QTY,0))/nvl(C4048_8_SALES_FORECAST_QTY,1),4)),4)
					 FCST_VAR_PCT_8,
					 abs(ROUND(DECODE(NVL(C4048_8_SALES_FORECAST_QTY,0),0,0,
					round((nvl(C4048_8_SALES_FORECAST_QTY,0)-nvl(C4048_8_ACTUAL_SALES_QTY,0))/nvl(C4048_8_SALES_FORECAST_QTY,1),4)),4))
					 abs_FCST_VAR_PCT_8,

					 --9
					 (nvl(C4048_9_SALES_WTD_AVG_QTY,0)-nvl(C4048_9_ACTUAL_SALES_QTY,0)) sales_var_9,
				   ABS((nvl(C4048_9_SALES_WTD_AVG_QTY,0)-nvl(C4048_9_ACTUAL_SALES_QTY,0))) abs_sales_var_9

					,DECODE(NVL(C4048_9_SALES_WTD_AVG_QTY,0),0,0,
					ROUND(((nvl(C4048_9_SALES_WTD_AVG_QTY,0)-nvl(C4048_9_ACTUAL_SALES_QTY,0)))/NVL(C4048_9_SALES_WTD_AVG_QTY,1),4)) sales_var_pct_9
					,abs(DECODE(NVL(C4048_9_SALES_WTD_AVG_QTY,0),0,0,
					ROUND(((nvl(C4048_9_SALES_WTD_AVG_QTY,0)-nvl(C4048_9_ACTUAL_SALES_QTY,0)))/NVL(C4048_9_SALES_WTD_AVG_QTY,1),4))) abs_sales_var_pct_9
					,(nvl(C4048_9_SALES_FORECAST_QTY,0)-nvl(C4048_9_ACTUAL_SALES_QTY,0)) FCST_VAR_9
					,ABS(nvl(C4048_9_SALES_FORECAST_QTY,0)-nvl(C4048_9_ACTUAL_SALES_QTY,0)) ABS_FCST_VAR_9
					,

					ROUND(DECODE(NVL(C4048_9_SALES_FORECAST_QTY,0),0,0,
					round((nvl(C4048_9_SALES_FORECAST_QTY,0)-nvl(C4048_9_ACTUAL_SALES_QTY,0))/nvl(C4048_9_SALES_FORECAST_QTY,1),4)),4)
					 FCST_VAR_PCT_9,
					 abs(ROUND(DECODE(NVL(C4048_9_SALES_FORECAST_QTY,0),0,0,
					round((nvl(C4048_9_SALES_FORECAST_QTY,0)-nvl(C4048_9_ACTUAL_SALES_QTY,0))/nvl(C4048_9_SALES_FORECAST_QTY,1),4)),4))
					 abs_FCST_VAR_PCT_9,

					--10
					(nvl(C4048_10_SALES_WTD_AVG_QTY,0)-nvl(C4048_10_ACTUAL_SALES_QTY,0)) sales_var_10,
				   ABS((nvl(C4048_10_SALES_WTD_AVG_QTY,0)-nvl(C4048_10_ACTUAL_SALES_QTY,0))) abs_sales_var_10

					,DECODE(NVL(C4048_10_SALES_WTD_AVG_QTY,0),0,0,
					ROUND(((nvl(C4048_10_SALES_WTD_AVG_QTY,0)-nvl(C4048_10_ACTUAL_SALES_QTY,0)))/NVL(C4048_10_SALES_WTD_AVG_QTY,1),4)) sales_var_pct_10
					,abs(DECODE(NVL(C4048_10_SALES_WTD_AVG_QTY,0),0,0,
					ROUND(((nvl(C4048_10_SALES_WTD_AVG_QTY,0)-nvl(C4048_10_ACTUAL_SALES_QTY,0)))/NVL(C4048_10_SALES_WTD_AVG_QTY,1),4))) abs_sales_var_pct_10
					,(nvl(C4048_10_SALES_FORECAST_QTY,0)-nvl(C4048_10_ACTUAL_SALES_QTY,0)) FCST_VAR_10
					,ABS(nvl(C4048_10_SALES_FORECAST_QTY,0)-nvl(C4048_10_ACTUAL_SALES_QTY,0)) ABS_FCST_VAR_10
					,

					ROUND(DECODE(NVL(C4048_10_SALES_FORECAST_QTY,0),0,0,
					round((nvl(C4048_10_SALES_FORECAST_QTY,0)-nvl(C4048_10_ACTUAL_SALES_QTY,0))/nvl(C4048_10_SALES_FORECAST_QTY,1),4)),4)
					 FCST_VAR_PCT_10,
					 abs(ROUND(DECODE(NVL(C4048_10_SALES_FORECAST_QTY,0),0,0,
					round((nvl(C4048_10_SALES_FORECAST_QTY,0)-nvl(C4048_10_ACTUAL_SALES_QTY,0))/nvl(C4048_10_SALES_FORECAST_QTY,1),4)),4))
					 abs_FCST_VAR_PCT_10,

					--11
					(nvl(C4048_11_SALES_WTD_AVG_QTY,0)-nvl(C4048_11_ACTUAL_SALES_QTY,0)) sales_var_11,
				   ABS((nvl(C4048_11_SALES_WTD_AVG_QTY,0)-nvl(C4048_11_ACTUAL_SALES_QTY,0))) abs_sales_var_11

					,DECODE(NVL(C4048_11_SALES_WTD_AVG_QTY,0),0,0,
					ROUND(((nvl(C4048_11_SALES_WTD_AVG_QTY,0)-nvl(C4048_11_ACTUAL_SALES_QTY,0)))/NVL(C4048_11_SALES_WTD_AVG_QTY,1),4)) sales_var_pct_11
					,abs(DECODE(NVL(C4048_11_SALES_WTD_AVG_QTY,0),0,0,
					ROUND(((nvl(C4048_11_SALES_WTD_AVG_QTY,0)-nvl(C4048_11_ACTUAL_SALES_QTY,0)))/NVL(C4048_11_SALES_WTD_AVG_QTY,1),4))) abs_sales_var_pct_11
					,(nvl(C4048_11_SALES_FORECAST_QTY,0)-nvl(C4048_11_ACTUAL_SALES_QTY,0)) FCST_VAR_11
					,ABS(nvl(C4048_11_SALES_FORECAST_QTY,0)-nvl(C4048_11_ACTUAL_SALES_QTY,0)) ABS_FCST_VAR_11
					,

					ROUND(DECODE(NVL(C4048_11_SALES_FORECAST_QTY,0),0,0,
					round((nvl(C4048_11_SALES_FORECAST_QTY,0)-nvl(C4048_11_ACTUAL_SALES_QTY,0))/nvl(C4048_11_SALES_FORECAST_QTY,1),4)),4)
					 FCST_VAR_PCT_11,
					 abs(ROUND(DECODE(NVL(C4048_11_SALES_FORECAST_QTY,0),0,0,
					round((nvl(C4048_11_SALES_FORECAST_QTY,0)-nvl(C4048_11_ACTUAL_SALES_QTY,0))/nvl(C4048_11_SALES_FORECAST_QTY,1),4)),4))
					 abs_FCST_VAR_PCT_11,

					--12
					(nvl(C4048_12_SALES_WTD_AVG_QTY,0)-nvl(C4048_12_ACTUAL_SALES_QTY,0)) sales_var_12,
				   ABS((nvl(C4048_12_SALES_WTD_AVG_QTY,0)-nvl(C4048_12_ACTUAL_SALES_QTY,0))) abs_sales_var_12

					,DECODE(NVL(C4048_12_SALES_WTD_AVG_QTY,0),0,0,
					ROUND(((nvl(C4048_12_SALES_WTD_AVG_QTY,0)-nvl(C4048_12_ACTUAL_SALES_QTY,0)))/NVL(C4048_12_SALES_WTD_AVG_QTY,1),4)) sales_var_pct_12
					,abs(DECODE(NVL(C4048_12_SALES_WTD_AVG_QTY,0),0,0,
					ROUND(((nvl(C4048_12_SALES_WTD_AVG_QTY,0)-nvl(C4048_12_ACTUAL_SALES_QTY,0)))/NVL(C4048_12_SALES_WTD_AVG_QTY,1),4))) abs_sales_var_pct_12
					,(nvl(C4048_12_SALES_FORECAST_QTY,0)-nvl(C4048_12_ACTUAL_SALES_QTY,0)) FCST_VAR_12
					,ABS(nvl(C4048_12_SALES_FORECAST_QTY,0)-nvl(C4048_12_ACTUAL_SALES_QTY,0)) ABS_FCST_VAR_12
					,

					ROUND(DECODE(NVL(C4048_12_SALES_FORECAST_QTY,0),0,0,
					round((nvl(C4048_12_SALES_FORECAST_QTY,0)-nvl(C4048_12_ACTUAL_SALES_QTY,0))/nvl(C4048_12_SALES_FORECAST_QTY,1),4)),4)
					 FCST_VAR_PCT_12,
					 abs(ROUND(DECODE(NVL(C4048_12_SALES_FORECAST_QTY,0),0,0,
					round((nvl(C4048_12_SALES_FORECAST_QTY,0)-nvl(C4048_12_ACTUAL_SALES_QTY,0))/nvl(C4048_12_SALES_FORECAST_QTY,1),4)),4))
					 abs_FCST_VAR_PCT_12

		from T4048_FORECAST_ACCURACY_DETAILS
		WHERE C4047_FORECAST_ACCURACY_ID= P_fcst_accur_ID
		AND C4048_VOID_FL IS NULL;


	BEGIN

		  FOR variance_lp IN CUR_Variance
        LOOP


			BEGIN

			pnum :=   variance_lp.pnum;
			--sales_fcst :=   variance_lp.FCST_VAR;
			--sales_fcst_pct :=   variance_lp.FCST_VAR_PCT;


					UPDATE 	T4048_FORECAST_ACCURACY_DETAILS
					SET
					C4048_1_SALES_VARIANCE_QTY = variance_lp.sales_var_1
					, C4048_1_SALES_ABS_VARIANCE_QTY = variance_lp.abs_sales_var_1
					,C4048_1_SALES_VARIANCE_PCT = variance_lp.sales_var_pct_1
					,C4048_1_abs_SALES_VARIANCE_PCT = variance_lp.abs_sales_var_pct_1
					,C4048_1_SALES_FCST_OVERRIDE_VARIANCE_QTY =  variance_lp.FCST_VAR_1
					,C4048_1_SALES_FCST_ABS_OVERRIDE_VARIANCE_QTY =  variance_lp.ABS_FCST_VAR_1
					,C4048_1_SALES_FCST_OVERRIDE_VARIANCE_PCT = variance_lp.FCST_VAR_PCT_1
					,C4048_1_ABS_SALES_FCST_OVERRIDE_VARIANCE_PCT = variance_lp.abs_FCST_VAR_PCT_1

					--2
					,C4048_2_SALES_VARIANCE_QTY = variance_lp.sales_var_2
					, C4048_2_SALES_ABS_VARIANCE_QTY = variance_lp.abs_sales_var_2
					,C4048_2_SALES_VARIANCE_PCT = variance_lp.sales_var_pct_2
					,C4048_2_abs_SALES_VARIANCE_PCT = variance_lp.abs_sales_var_pct_2
					,C4048_2_SALES_FCST_OVERRIDE_VARIANCE_QTY =  variance_lp.FCST_VAR_2
					,C4048_2_SALES_FCST_ABS_OVERRIDE_VARIANCE_QTY =  variance_lp.ABS_FCST_VAR_2
					,C4048_2_SALES_FCST_OVERRIDE_VARIANCE_PCT = variance_lp.FCST_VAR_PCT_2
					,C4048_2_abs_SALES_FCST_OVERRIDE_VARIANCE_PCT = variance_lp.abs_FCST_VAR_PCT_2

					--3
					,C4048_3_SALES_VARIANCE_QTY = variance_lp.sales_var_3
					, C4048_3_SALES_ABS_VARIANCE_QTY = variance_lp.abs_sales_var_3
					,C4048_3_SALES_VARIANCE_PCT = variance_lp.sales_var_pct_3
					,C4048_3_abs_SALES_VARIANCE_PCT = variance_lp.abs_sales_var_pct_3
					,C4048_3_SALES_FCST_OVERRIDE_VARIANCE_QTY =  variance_lp.FCST_VAR_3
					,C4048_3_SALES_FCST_ABS_OVERRIDE_VARIANCE_QTY =  variance_lp.ABS_FCST_VAR_3
					,C4048_3_SALES_FCST_OVERRIDE_VARIANCE_PCT = variance_lp.FCST_VAR_PCT_3
					,C4048_3_abs_SALES_FCST_OVERRIDE_VARIANCE_PCT = variance_lp.abs_FCST_VAR_PCT_3

					--4
					,C4048_4_SALES_VARIANCE_QTY = variance_lp.sales_var_4
					, C4048_4_SALES_ABS_VARIANCE_QTY = variance_lp.abs_sales_var_4
					,C4048_4_SALES_VARIANCE_PCT = variance_lp.sales_var_pct_4
					,C4048_4_abs_SALES_VARIANCE_PCT = variance_lp.abs_sales_var_pct_4
					,C4048_4_SALES_FCST_OVERRIDE_VARIANCE_QTY =  variance_lp.FCST_VAR_4
					,C4048_4_SALES_FCST_ABS_OVERRIDE_VARIANCE_QTY =  variance_lp.ABS_FCST_VAR_4
					,C4048_4_SALES_FCST_OVERRIDE_VARIANCE_PCT = variance_lp.FCST_VAR_PCT_4
					,C4048_4_abs_SALES_FCST_OVERRIDE_VARIANCE_PCT = variance_lp.abs_FCST_VAR_PCT_4

					--5
					,C4048_5_SALES_VARIANCE_QTY = variance_lp.sales_var_5
					, C4048_5_SALES_ABS_VARIANCE_QTY = variance_lp.abs_sales_var_5
					,C4048_5_SALES_VARIANCE_PCT = variance_lp.sales_var_pct_5
					,C4048_5_abs_SALES_VARIANCE_PCT = variance_lp.abs_sales_var_pct_5
					,C4048_5_SALES_FCST_OVERRIDE_VARIANCE_QTY =  variance_lp.FCST_VAR_5
					,C4048_5_SALES_FCST_ABS_OVERRIDE_VARIANCE_QTY =  variance_lp.ABS_FCST_VAR_5
					,C4048_5_SALES_FCST_OVERRIDE_VARIANCE_PCT = variance_lp.FCST_VAR_PCT_5
					,C4048_5_abs_SALES_FCST_OVERRIDE_VARIANCE_PCT = variance_lp.abs_FCST_VAR_PCT_5

					--6
					,C4048_6_SALES_VARIANCE_QTY = variance_lp.sales_var_6
					, C4048_6_SALES_ABS_VARIANCE_QTY = variance_lp.abs_sales_var_6
					,C4048_6_SALES_VARIANCE_PCT = variance_lp.sales_var_pct_6
					,C4048_6_abs_SALES_VARIANCE_PCT = variance_lp.abs_sales_var_pct_6
					,C4048_6_SALES_FCST_OVERRIDE_VARIANCE_QTY =  variance_lp.FCST_VAR_6
					,C4048_6_SALES_FCST_ABS_OVERRIDE_VARIANCE_QTY =  variance_lp.ABS_FCST_VAR_6
					,C4048_6_SALES_FCST_OVERRIDE_VARIANCE_PCT = variance_lp.FCST_VAR_PCT_6
					,C4048_6_abs_SALES_FCST_OVERRIDE_VARIANCE_PCT = variance_lp.abs_FCST_VAR_PCT_6


					--7
					,C4048_7_SALES_VARIANCE_QTY = variance_lp.sales_var_7
					, C4048_7_SALES_ABS_VARIANCE_QTY = variance_lp.abs_sales_var_7
					,C4048_7_SALES_VARIANCE_PCT = variance_lp.sales_var_pct_7
					,C4048_7_abs_SALES_VARIANCE_PCT = variance_lp.abs_sales_var_pct_7
					,C4048_7_SALES_FCST_OVERRIDE_VARIANCE_QTY =  variance_lp.FCST_VAR_7
					,C4048_7_SALES_FCST_ABS_OVERRIDE_VARIANCE_QTY =  variance_lp.ABS_FCST_VAR_7
					,C4048_7_SALES_FCST_OVERRIDE_VARIANCE_PCT = variance_lp.FCST_VAR_PCT_7
					,C4048_7_abs_SALES_FCST_OVERRIDE_VARIANCE_PCT = variance_lp.abs_FCST_VAR_PCT_7

					--8
					,C4048_8_SALES_VARIANCE_QTY = variance_lp.sales_var_8
					, C4048_8_SALES_ABS_VARIANCE_QTY = variance_lp.abs_sales_var_8
					,C4048_8_SALES_VARIANCE_PCT = variance_lp.sales_var_pct_8
					,C4048_8_abs_SALES_VARIANCE_PCT = variance_lp.abs_sales_var_pct_8
					,C4048_8_SALES_FCST_OVERRIDE_VARIANCE_QTY =  variance_lp.FCST_VAR_8
					,C4048_8_SALES_FCST_ABS_OVERRIDE_VARIANCE_QTY =  variance_lp.ABS_FCST_VAR_8
					,C4048_8_SALES_FCST_OVERRIDE_VARIANCE_PCT = variance_lp.FCST_VAR_PCT_8
					,C4048_8_abs_SALES_FCST_OVERRIDE_VARIANCE_PCT = variance_lp.abs_FCST_VAR_PCT_8

					--9
					,C4048_9_SALES_VARIANCE_QTY = variance_lp.sales_var_9
					, C4048_9_SALES_ABS_VARIANCE_QTY = variance_lp.abs_sales_var_9
					,C4048_9_SALES_VARIANCE_PCT = variance_lp.sales_var_pct_9
					,C4048_9_abs_SALES_VARIANCE_PCT = variance_lp.abs_sales_var_pct_9
					,C4048_9_SALES_FCST_OVERRIDE_VARIANCE_QTY =  variance_lp.FCST_VAR_9
					,C4048_9_SALES_FCST_ABS_OVERRIDE_VARIANCE_QTY =  variance_lp.ABS_FCST_VAR_9
					,C4048_9_SALES_FCST_OVERRIDE_VARIANCE_PCT = variance_lp.FCST_VAR_PCT_9
					,C4048_9_abs_SALES_FCST_OVERRIDE_VARIANCE_PCT = variance_lp.abs_FCST_VAR_PCT_9

					--10
					,C4048_10_SALES_VARIANCE_QTY = variance_lp.sales_var_10
					, C4048_10_SALES_ABS_VARIANCE_QTY = variance_lp.abs_sales_var_10
					,C4048_10_SALES_VARIANCE_PCT = variance_lp.sales_var_pct_10
					,C4048_10_abs_SALES_VARIANCE_PCT = variance_lp.abs_sales_var_pct_10
					,C4048_10_SALES_FCST_OVERRIDE_VARIANCE_QTY =  variance_lp.FCST_VAR_10
					,C4048_10_SALES_FCST_ABS_OVERRIDE_VARIANCE_QTY =  variance_lp.ABS_FCST_VAR_10
					,C4048_10_SALES_FCST_OVERRIDE_VARIANCE_PCT = variance_lp.FCST_VAR_PCT_10
					,C4048_10_abs_SALES_FCST_OVERRIDE_VARIANCE_PCT = variance_lp.abs_FCST_VAR_PCT_10

					--11
					,C4048_11_SALES_VARIANCE_QTY = variance_lp.sales_var_11
					, C4048_11_SALES_ABS_VARIANCE_QTY = variance_lp.abs_sales_var_11
					,C4048_11_SALES_VARIANCE_PCT = variance_lp.sales_var_pct_11
					,C4048_11_abs_SALES_VARIANCE_PCT = variance_lp.abs_sales_var_pct_11
					,C4048_11_SALES_FCST_OVERRIDE_VARIANCE_QTY =  variance_lp.FCST_VAR_11
					,C4048_11_SALES_FCST_ABS_OVERRIDE_VARIANCE_QTY =  variance_lp.ABS_FCST_VAR_11
					,C4048_11_SALES_FCST_OVERRIDE_VARIANCE_PCT = variance_lp.FCST_VAR_PCT_11
					,C4048_11_abs_SALES_FCST_OVERRIDE_VARIANCE_PCT = variance_lp.abs_FCST_VAR_PCT_11

					--12
					,C4048_12_SALES_VARIANCE_QTY = variance_lp.sales_var_12
					, C4048_12_SALES_ABS_VARIANCE_QTY = variance_lp.abs_sales_var_12
					,C4048_12_SALES_VARIANCE_PCT = variance_lp.sales_var_pct_12
					,C4048_12_abs_SALES_VARIANCE_PCT = variance_lp.abs_sales_var_pct_12
					,C4048_12_SALES_FCST_OVERRIDE_VARIANCE_QTY =  variance_lp.FCST_VAR_12
					,C4048_12_SALES_FCST_ABS_OVERRIDE_VARIANCE_QTY =  variance_lp.ABS_FCST_VAR_12
					,C4048_12_SALES_FCST_OVERRIDE_VARIANCE_PCT = variance_lp.FCST_VAR_PCT_12
					,C4048_12_abs_SALES_FCST_OVERRIDE_VARIANCE_PCT = variance_lp.abs_FCST_VAR_PCT_12

			WHERE
				C4047_FORECAST_ACCURACY_ID = P_fcst_accur_ID AND
				C205_PART_NUMBER_ID = pnum AND
				 C901_LEVEL_ID = variance_lp.lvl_id AND
				 C901_LEVEL_VALUE = variance_lp.lvl_val AND
				 C4048_VOID_FL IS NULL;


			EXCEPTION WHEN
			OTHERS THEN
				DBMS_OUTPUT.PUT_LINE(SQLERRM|| ':PNUM:'||pnum

				||':-:'||variance_lp.sales_var_pct_1
				||':-:'||variance_lp.sales_var_pct_2
				||':-:'||variance_lp.sales_var_pct_3
				||':-:'||variance_lp.sales_var_pct_4
				||':-:'||variance_lp.sales_var_pct_5
				||':-:'||variance_lp.sales_var_pct_6
				||':-:'||variance_lp.sales_var_pct_7
				||':-:'||variance_lp.sales_var_pct_8
				||':-:'||variance_lp.sales_var_pct_9
				||':-:'||variance_lp.sales_var_pct_10
				||':-:'||variance_lp.sales_var_pct_11
				||':-:'||variance_lp.sales_var_pct_12
						);
			v:='';

			END;

		END LOOP;


	END gm_ld_calc_variance;

	procedure gm_upd_part_rank(
		P_fcst_accur_ID IN T4047_FORECAST_ACCURACY.C4047_FORECAST_ACCURACY_ID%TYPE,
		p_load_dt IN DATE)
	AS
		type pnum_array			IS     TABLE OF  T205_PART_NUMBER.c205_part_number_id%TYPE;
		arr_pnum pnum_array;
		type rank_array			IS     TABLE OF  T4048_FORECAST_ACCURACY_DETAILS.C4072_RANK%TYPE;
		arr_rank rank_array;


		CURSOR part_rank
		IS

			  select C205_PART_NUMBER_ID,C4072_RANK from T4072_PURCHASE_PART_RANK_DETAIL where C4071_PURCHASE_PART_RANK_ID in (
				select C4071_PURCHASE_PART_RANK_ID from T4071_PURCHASE_PART_RANK where C4071_VOID_FL IS NULL AND
				last_day(C4071_LOAD_DATE) = last_day(add_months(trunc(p_load_dt),-1)));

	BEGIN

		OPEN part_rank;
		LOOP
		FETCH part_rank BULK COLLECT INTO arr_pnum,arr_rank LIMIT 500;

		FORALL i in 1.. arr_pnum.COUNT

		UPDATE 	T4048_FORECAST_ACCURACY_DETAILS
		SET 	C4072_RANK = arr_rank(i)
		WHERE
			 C205_PART_NUMBER_ID = arr_pnum(i)
			AND C4047_FORECAST_ACCURACY_ID = P_fcst_accur_ID
			AND C4048_VOID_FL IS NULL;

		EXIT WHEN part_rank%NOTFOUND;
	    END LOOP;
	    CLOSE part_rank;



	END gm_upd_part_rank;

	procedure gm_upd_system_id(
		P_fcst_accur_ID IN T4047_FORECAST_ACCURACY.C4047_FORECAST_ACCURACY_ID%TYPE
		)
	AS
		type pnum_array			IS     TABLE OF  T205_PART_NUMBER.c205_part_number_id%TYPE;
		arr_pnum pnum_array;
		type system_array			IS     TABLE OF  T4048_FORECAST_ACCURACY_DETAILS.C207_SYSTEM_ID%TYPE;
		arr_system system_array;

		type systemnm_array			IS     TABLE OF  T4048_FORECAST_ACCURACY_DETAILS.C207_SYSTEM_NM%TYPE;
		arr_systemnm systemnm_array;

		CURSOR cur_system
		IS

			  select  t4048.C205_PART_NUMBER_ID pnum, t207.c207_set_id system_id , t207.c207_set_nm system_nm
			  from T4048_FORECAST_ACCURACY_DETAILS t4048 , T207_SET_MASTER T207,
			  T208_SET_DETAILS T208, T4047_FORECAST_ACCURACY t4047
			  where t4048.C4047_FORECAST_ACCURACY_ID = P_fcst_accur_ID
			  AND T207.c207_set_id = t208.c207_set_id
			  and T208.c208_void_fl is null
			  and T207.C901_SET_GRP_TYPE = 1600
			  AND T4048.C205_PART_NUMBER_ID =  t208.C205_PART_NUMBER_ID(+)
			  AND T4048.C4048_VOID_FL IS NULL
			  AND t4047.C4047_FORECAST_ACCURACY_ID = t4048.C4047_FORECAST_ACCURACY_ID
				and t4047.C4047_VOID_FL is null
				and t4047.C4047_LOAD_DT = trunc(current_date,'MM')
			  group by t207.c207_set_id ,t4048.C205_PART_NUMBER_ID,t207.c207_set_nm  ;

	BEGIN

		OPEN cur_system;
		LOOP
		FETCH cur_system BULK COLLECT INTO arr_pnum,arr_system, arr_systemnm LIMIT 500;

		FORALL i in 1.. arr_pnum.COUNT

		UPDATE 	T4048_FORECAST_ACCURACY_DETAILS
		SET 	C207_SYSTEM_ID = arr_system(i),
				C207_SYSTEM_NM = arr_systemnm(i)
		WHERE
			 C205_PART_NUMBER_ID = arr_pnum(i)
			AND C4047_FORECAST_ACCURACY_ID = P_fcst_accur_ID
			AND C4048_VOID_FL IS NULL;

		EXIT WHEN cur_system%NOTFOUND;
	    END LOOP;
	    CLOSE cur_system;



	END gm_upd_system_id;

	procedure gm_upd_group_id(
		P_fcst_accur_ID IN T4047_FORECAST_ACCURACY.C4047_FORECAST_ACCURACY_ID%TYPE
		)
	AS
		type pnum_array			IS     TABLE OF  T205_PART_NUMBER.c205_part_number_id%TYPE;
		arr_pnum pnum_array;
		type group_array			IS     TABLE OF  T4048_FORECAST_ACCURACY_DETAILS.C4010_GROUP_ID%TYPE;
		arr_group group_array;
		type groupnm_array			IS     TABLE OF  T4048_FORECAST_ACCURACY_DETAILS.C4010_GROUP_NM%TYPE;
		arr_groupnm groupnm_array;

		CURSOR group_dtls
		IS

			  select t4011.C205_PART_NUMBER_ID pnum, t4010.C4010_GROUP_ID grp_id , t4010.C4010_GROUP_NM grp_nm
				from t4010_group t4010, T4011_GROUP_DETAIL t4011,T4048_FORECAST_ACCURACY_DETAILS t4048, T4047_FORECAST_ACCURACY t4047
				where t4010.C4010_GROUP_ID = t4011.C4010_GROUP_ID
				and t4010.C4010_VOID_FL is null
				and t4011.C901_PART_PRICING_TYPE=52080
				and  T4048.C4048_VOID_FL IS NULL
				AND t4047.C4047_FORECAST_ACCURACY_ID = t4048.C4047_FORECAST_ACCURACY_ID
				and t4047.C4047_VOID_FL is null
				and t4047.C4047_LOAD_DT = trunc(current_date,'MM')
				and T4048.C205_PART_NUMBER_ID =  t4011.C205_PART_NUMBER_ID
				and t4047.C4047_FORECAST_ACCURACY_ID = P_fcst_accur_ID ;

	BEGIN

		OPEN group_dtls;
		LOOP
		FETCH group_dtls BULK COLLECT INTO arr_pnum,arr_group, arr_groupnm LIMIT 500;

		FORALL i in 1.. arr_pnum.COUNT

		UPDATE 	T4048_FORECAST_ACCURACY_DETAILS
		SET 	C4010_GROUP_ID = arr_group(i),
				C4010_GROUP_NM = arr_groupnm(i)
		WHERE
			 C205_PART_NUMBER_ID = arr_pnum(i)
			AND C4047_FORECAST_ACCURACY_ID = P_fcst_accur_ID
			AND C4048_VOID_FL IS NULL;

		EXIT WHEN group_dtls%NOTFOUND;
	    END LOOP;
	    CLOSE group_dtls;



	END gm_upd_group_id;


	/*
	PROCEDURE gm_ld_calc_variance_by_group(
		P_fcst_accur_ID IN T4047_FORECAST_ACCURACY.C4047_FORECAST_ACCURACY_ID%TYPE
	)
	AS


		v number;

		CURSOR CUR_group_Variance
		IS

			select 	 C4010_GROUP_ID group_id,C4010_GROUP_NM group_nm
					,C4048_1_LOAD_DT load_dt_1 ,C4048_1_FCST_DT fcst_dt_1
					,C4048_2_LOAD_DT load_dt_2,C4048_2_FCST_DT fcst_dt_2
					,C4048_3_LOAD_DT load_dt_3,C4048_3_FCST_DT fcst_dt_3
					,C4048_4_LOAD_DT load_dt_4,C4048_4_FCST_DT fcst_dt_4
					,C4048_5_LOAD_DT load_dt_5,C4048_5_FCST_DT fcst_dt_5
					,C4048_6_LOAD_DT load_dt_6,C4048_6_FCST_DT fcst_dt_6
					, SUM(NVL(C4048_1_SALES_WTD_AVG_QTY,0)) sales_wtd_avg_1
					, SUM(NVL(C4048_1_SALES_FORECAST_QTY,0)) sales_fcst_1
					, SUM(NVL(C4048_1_ACTUAL_SALES_QTY,0)) act_sales_1
					,( SUM(nvl(C4048_1_SALES_WTD_AVG_QTY,0))-SUM(nvl(C4048_1_ACTUAL_SALES_QTY,0))) sales_var_1,
				   ABS(SUM(nvl(C4048_1_SALES_WTD_AVG_QTY,0))-SUM(nvl(C4048_1_ACTUAL_SALES_QTY,0))) abs_sales_var_1

					,DECODE(SUM(NVL(C4048_1_SALES_WTD_AVG_QTY,0)),0,0,
					ROUND(((SUM(nvl(C4048_1_SALES_WTD_AVG_QTY,0)) -SUM(nvl(C4048_1_ACTUAL_SALES_QTY,0))))/SUM(NVL(C4048_1_SALES_WTD_AVG_QTY,1)),4)) sales_var_pct_1
					,(SUM(nvl(C4048_1_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_1_ACTUAL_SALES_QTY,0))) FCST_VAR_1
					,ABS(SUM(nvl(C4048_1_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_1_ACTUAL_SALES_QTY,0))) ABS_FCST_VAR_1
					,

					ROUND(DECODE(SUM(NVL(C4048_1_SALES_FORECAST_QTY,0)),0,0,
					round((SUM(nvl(C4048_1_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_1_ACTUAL_SALES_QTY,0)))/ SUM(nvl(C4048_1_SALES_FORECAST_QTY,1)),4)),4)
					 FCST_VAR_PCT_1

					--2
					, SUM(NVL(C4048_2_SALES_WTD_AVG_QTY,0)) sales_wtd_avg_2
					, SUM(NVL(C4048_2_SALES_FORECAST_QTY,0)) sales_fcst_2
					, SUM(NVL(C4048_2_ACTUAL_SALES_QTY,0)) act_sales_2
					,( SUM(nvl(C4048_2_SALES_WTD_AVG_QTY,0))-SUM(nvl(C4048_2_ACTUAL_SALES_QTY,0))) sales_var_2,
				   ABS(SUM(nvl(C4048_2_SALES_WTD_AVG_QTY,0))-SUM(nvl(C4048_2_ACTUAL_SALES_QTY,0))) abs_sales_var_2

					,DECODE(SUM(NVL(C4048_2_SALES_WTD_AVG_QTY,0)),0,0,
					ROUND(((SUM(nvl(C4048_2_SALES_WTD_AVG_QTY,0)) -SUM(nvl(C4048_2_ACTUAL_SALES_QTY,0))))/SUM(NVL(C4048_2_SALES_WTD_AVG_QTY,1)),4)) sales_var_pct_2
					,(SUM(nvl(C4048_2_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_2_ACTUAL_SALES_QTY,0))) FCST_VAR_2
					,ABS(SUM(nvl(C4048_2_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_2_ACTUAL_SALES_QTY,0))) ABS_FCST_VAR_2
					,

					ROUND(DECODE(SUM(NVL(C4048_2_SALES_FORECAST_QTY,0)),0,0,
					round((SUM(nvl(C4048_2_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_2_ACTUAL_SALES_QTY,0)))/ SUM(nvl(C4048_2_SALES_FORECAST_QTY,1)),4)),4)
					 FCST_VAR_PCT_2

					 --3
					 , SUM(NVL(C4048_3_SALES_WTD_AVG_QTY,0)) sales_wtd_avg_3
					, SUM(NVL(C4048_3_SALES_FORECAST_QTY,0)) sales_fcst_3
					, SUM(NVL(C4048_3_ACTUAL_SALES_QTY,0)) act_sales_3
					, ( SUM(nvl(C4048_3_SALES_WTD_AVG_QTY,0))-SUM(nvl(C4048_3_ACTUAL_SALES_QTY,0))) sales_var_3,
				   ABS(SUM(nvl(C4048_3_SALES_WTD_AVG_QTY,0))-SUM(nvl(C4048_3_ACTUAL_SALES_QTY,0))) abs_sales_var_3

					,DECODE(SUM(NVL(C4048_3_SALES_WTD_AVG_QTY,0)),0,0,
					ROUND(((SUM(nvl(C4048_3_SALES_WTD_AVG_QTY,0)) -SUM(nvl(C4048_3_ACTUAL_SALES_QTY,0))))/SUM(NVL(C4048_3_SALES_WTD_AVG_QTY,1)),4)) sales_var_pct_3
					,(SUM(nvl(C4048_3_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_3_ACTUAL_SALES_QTY,0))) FCST_VAR_3
					,ABS(SUM(nvl(C4048_3_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_3_ACTUAL_SALES_QTY,0))) ABS_FCST_VAR_3
					,

					ROUND(DECODE(SUM(NVL(C4048_3_SALES_FORECAST_QTY,0)),0,0,
					round((SUM(nvl(C4048_3_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_3_ACTUAL_SALES_QTY,0)))/ SUM(nvl(C4048_3_SALES_FORECAST_QTY,1)),4)),4)
					 FCST_VAR_PCT_3

					--4
					, SUM(NVL(C4048_4_SALES_WTD_AVG_QTY,0)) sales_wtd_avg_4
					, SUM(NVL(C4048_4_SALES_FORECAST_QTY,0)) sales_fcst_4
					, SUM(NVL(C4048_4_ACTUAL_SALES_QTY,0)) act_sales_4
					, ( SUM(nvl(C4048_4_SALES_WTD_AVG_QTY,0))-SUM(nvl(C4048_4_ACTUAL_SALES_QTY,0))) sales_var_4,
				   ABS(SUM(nvl(C4048_4_SALES_WTD_AVG_QTY,0))-SUM(nvl(C4048_4_ACTUAL_SALES_QTY,0))) abs_sales_var_4

					,DECODE(SUM(NVL(C4048_4_SALES_WTD_AVG_QTY,0)),0,0,
					ROUND(((SUM(nvl(C4048_4_SALES_WTD_AVG_QTY,0)) -SUM(nvl(C4048_4_ACTUAL_SALES_QTY,0))))/SUM(NVL(C4048_4_SALES_WTD_AVG_QTY,1)),4)) sales_var_pct_4
					,(SUM(nvl(C4048_4_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_4_ACTUAL_SALES_QTY,0))) FCST_VAR_4
					,ABS(SUM(nvl(C4048_4_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_4_ACTUAL_SALES_QTY,0))) ABS_FCST_VAR_4
					,

					ROUND(DECODE(SUM(NVL(C4048_4_SALES_FORECAST_QTY,0)),0,0,
					round((SUM(nvl(C4048_4_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_4_ACTUAL_SALES_QTY,0)))/ SUM(nvl(C4048_4_SALES_FORECAST_QTY,1)),4)),4)
					 FCST_VAR_PCT_4

					--5
					, SUM(NVL(C4048_5_SALES_WTD_AVG_QTY,0)) sales_wtd_avg_5
					, SUM(NVL(C4048_5_SALES_FORECAST_QTY,0)) sales_fcst_5
					, SUM(NVL(C4048_5_ACTUAL_SALES_QTY,0)) act_sales_5
					, ( SUM(nvl(C4048_5_SALES_WTD_AVG_QTY,0))-SUM(nvl(C4048_5_ACTUAL_SALES_QTY,0))) sales_var_5,
				   ABS(SUM(nvl(C4048_5_SALES_WTD_AVG_QTY,0))-SUM(nvl(C4048_5_ACTUAL_SALES_QTY,0))) abs_sales_var_5

					,DECODE(SUM(NVL(C4048_5_SALES_WTD_AVG_QTY,0)),0,0,
					ROUND(((SUM(nvl(C4048_5_SALES_WTD_AVG_QTY,0)) -SUM(nvl(C4048_5_ACTUAL_SALES_QTY,0))))/SUM(NVL(C4048_5_SALES_WTD_AVG_QTY,1)),4)) sales_var_pct_5
					,(SUM(nvl(C4048_5_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_5_ACTUAL_SALES_QTY,0))) FCST_VAR_5
					,ABS(SUM(nvl(C4048_5_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_5_ACTUAL_SALES_QTY,0))) ABS_FCST_VAR_5
					,

					ROUND(DECODE(SUM(NVL(C4048_5_SALES_FORECAST_QTY,0)),0,0,
					round((SUM(nvl(C4048_5_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_5_ACTUAL_SALES_QTY,0)))/ SUM(nvl(C4048_5_SALES_FORECAST_QTY,1)),4)),4)
					 FCST_VAR_PCT_5

					--6
					, SUM(NVL(C4048_6_SALES_WTD_AVG_QTY,0)) sales_wtd_avg_6
					, SUM(NVL(C4048_6_SALES_FORECAST_QTY,0)) sales_fcst_6
					, SUM(NVL(C4048_6_ACTUAL_SALES_QTY,0)) act_sales_6
					,( SUM(nvl(C4048_6_SALES_WTD_AVG_QTY,0))-SUM(nvl(C4048_6_ACTUAL_SALES_QTY,0))) sales_var_6,
				   ABS(SUM(nvl(C4048_6_SALES_WTD_AVG_QTY,0))-SUM(nvl(C4048_6_ACTUAL_SALES_QTY,0))) abs_sales_var_6

					,DECODE(SUM(NVL(C4048_6_SALES_WTD_AVG_QTY,0)),0,0,
					ROUND(((SUM(nvl(C4048_6_SALES_WTD_AVG_QTY,0)) -SUM(nvl(C4048_6_ACTUAL_SALES_QTY,0))))/SUM(NVL(C4048_6_SALES_WTD_AVG_QTY,1)),4)) sales_var_pct_6
					,(SUM(nvl(C4048_6_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_6_ACTUAL_SALES_QTY,0))) FCST_VAR_6
					,ABS(SUM(nvl(C4048_6_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_6_ACTUAL_SALES_QTY,0))) ABS_FCST_VAR_6
					,

					ROUND(DECODE(SUM(NVL(C4048_6_SALES_FORECAST_QTY,0)),0,0,
					round((SUM(nvl(C4048_6_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_6_ACTUAL_SALES_QTY,0)))/ SUM(nvl(C4048_6_SALES_FORECAST_QTY,1)),4)),4)
					 FCST_VAR_PCT_6


		from T4048_FORECAST_ACCURACY_DETAILS
		WHERE C4047_FORECAST_ACCURACY_ID= P_fcst_accur_ID
		AND C4048_VOID_FL IS NULL
		GROUP BY C4010_GROUP_ID ,C4010_GROUP_NM,C4048_1_LOAD_DT,C4048_1_FCST_DT,C4048_2_LOAD_DT,C4048_2_FCST_DT,C4048_3_LOAD_DT,C4048_3_FCST_DT,
				C4048_4_LOAD_DT,C4048_4_FCST_DT,C4048_5_LOAD_DT
				,C4048_5_FCST_DT,C4048_6_LOAD_DT,C4048_6_FCST_DT;


	BEGIN

		  UPDATE T4049_FORECAST_ACCURACY_GROUP_DETAILS
		  SET C4049_VOID_FL ='Y'
		  WHERE C4047_FORECAST_ACCURACY_ID <> P_fcst_accur_ID
		  AND C4049_VOID_FL IS NULL;

		  FOR group_variance IN CUR_group_Variance
        LOOP



					INSERT INTO T4049_FORECAST_ACCURACY_GROUP_DETAILS(C4047_FORECAST_ACCURACY_ID,C4010_GROUP_ID,C4010_GROUP_NM,
					c4049_1_LOAD_DT,c4049_1_FCST_DT,C4049_1_SALES_WTD_AVG_QTY,C4049_1_SALES_FORECAST_QTY,C4049_1_ACTUAL_SALES_QTY,c4049_1_SALES_VARIANCE_QTY,c4049_1_SALES_ABS_VARIANCE_QTY,c4049_1_SALES_VARIANCE_PCT,c4049_1_SALES_FCST_OVERRIDE_VARIANCE_QTY,c4049_1_SALES_FCST_OVERRIDE_VARIANCE_PCT,
					c4049_2_LOAD_DT,c4049_2_FCST_DT,C4049_2_SALES_WTD_AVG_QTY,C4049_2_SALES_FORECAST_QTY,C4049_2_ACTUAL_SALES_QTY,c4049_2_SALES_VARIANCE_QTY,c4049_2_SALES_ABS_VARIANCE_QTY,c4049_2_SALES_VARIANCE_PCT,c4049_2_SALES_FCST_OVERRIDE_VARIANCE_QTY,c4049_2_SALES_FCST_OVERRIDE_VARIANCE_PCT,
					c4049_3_LOAD_DT,c4049_3_FCST_DT,C4049_3_SALES_WTD_AVG_QTY,C4049_3_SALES_FORECAST_QTY,C4049_3_ACTUAL_SALES_QTY,c4049_3_SALES_VARIANCE_QTY,c4049_3_SALES_ABS_VARIANCE_QTY,c4049_3_SALES_VARIANCE_PCT,c4049_3_SALES_FCST_OVERRIDE_VARIANCE_QTY,c4049_3_SALES_FCST_OVERRIDE_VARIANCE_PCT,
					c4049_4_LOAD_DT,c4049_4_FCST_DT,C4049_4_SALES_WTD_AVG_QTY,C4049_4_SALES_FORECAST_QTY,C4049_4_ACTUAL_SALES_QTY,c4049_4_SALES_VARIANCE_QTY,c4049_4_SALES_ABS_VARIANCE_QTY,c4049_4_SALES_VARIANCE_PCT,c4049_4_SALES_FCST_OVERRIDE_VARIANCE_QTY,c4049_4_SALES_FCST_OVERRIDE_VARIANCE_PCT,
					c4049_5_LOAD_DT,c4049_5_FCST_DT,C4049_5_SALES_WTD_AVG_QTY,C4049_5_SALES_FORECAST_QTY,C4049_5_ACTUAL_SALES_QTY,c4049_5_SALES_VARIANCE_QTY,c4049_5_SALES_ABS_VARIANCE_QTY,c4049_5_SALES_VARIANCE_PCT,c4049_5_SALES_FCST_OVERRIDE_VARIANCE_QTY,c4049_5_SALES_FCST_OVERRIDE_VARIANCE_PCT,
					c4049_6_LOAD_DT,c4049_6_FCST_DT,C4049_6_SALES_WTD_AVG_QTY,C4049_6_SALES_FORECAST_QTY,C4049_6_ACTUAL_SALES_QTY,c4049_6_SALES_VARIANCE_QTY,c4049_6_SALES_ABS_VARIANCE_QTY,c4049_6_SALES_VARIANCE_PCT,c4049_6_SALES_FCST_OVERRIDE_VARIANCE_QTY,c4049_6_SALES_FCST_OVERRIDE_VARIANCE_PCT

					)
					VALUES (
					P_fcst_accur_ID,group_variance.group_id,group_variance.group_nm
					,group_variance.load_dt_1,group_variance.fcst_dt_1,group_variance.sales_wtd_avg_1,group_variance.sales_fcst_1,group_variance.act_sales_1,group_variance.sales_var_1 ,group_variance.abs_sales_var_1,group_variance.sales_var_pct_1,group_variance.FCST_VAR_1,group_variance.FCST_VAR_PCT_1
					,group_variance.load_dt_2,group_variance.fcst_dt_2,group_variance.sales_wtd_avg_2,group_variance.sales_fcst_2,group_variance.act_sales_2,group_variance.sales_var_2 ,group_variance.abs_sales_var_2,group_variance.sales_var_pct_2,group_variance.FCST_VAR_2,group_variance.FCST_VAR_PCT_2
					,group_variance.load_dt_3,group_variance.fcst_dt_3,group_variance.sales_wtd_avg_3,group_variance.sales_fcst_3,group_variance.act_sales_3,group_variance.sales_var_3 ,group_variance.abs_sales_var_3,group_variance.sales_var_pct_3,group_variance.FCST_VAR_3,group_variance.FCST_VAR_PCT_3
					,group_variance.load_dt_4,group_variance.fcst_dt_4,group_variance.sales_wtd_avg_4,group_variance.sales_fcst_4,group_variance.act_sales_4,group_variance.sales_var_4 ,group_variance.abs_sales_var_4,group_variance.sales_var_pct_4,group_variance.FCST_VAR_4,group_variance.FCST_VAR_PCT_4
					,group_variance.load_dt_5,group_variance.fcst_dt_5,group_variance.sales_wtd_avg_5,group_variance.sales_fcst_5,group_variance.act_sales_5,group_variance.sales_var_5 ,group_variance.abs_sales_var_5,group_variance.sales_var_pct_5,group_variance.FCST_VAR_5,group_variance.FCST_VAR_PCT_5
					,group_variance.load_dt_6,group_variance.fcst_dt_6,group_variance.sales_wtd_avg_6,group_variance.sales_fcst_6,group_variance.act_sales_6,group_variance.sales_var_6 ,group_variance.abs_sales_var_6,group_variance.sales_var_pct_6,group_variance.FCST_VAR_6,group_variance.FCST_VAR_PCT_6

					);

		END LOOP;


	END gm_ld_calc_variance_by_group;

	PROCEDURE gm_ld_calc_variance_by_system(
		P_fcst_accur_ID IN T4047_FORECAST_ACCURACY.C4047_FORECAST_ACCURACY_ID%TYPE
	)
	AS


		v number;

		CURSOR CUR_system_Variance
		IS

			select 	 C207_SYSTEM_ID system_id,C207_SYSTEM_NM system_nm
					,C4048_1_LOAD_DT load_dt_1 ,C4048_1_FCST_DT fcst_dt_1
					,C4048_2_LOAD_DT load_dt_2,C4048_2_FCST_DT fcst_dt_2
					,C4048_3_LOAD_DT load_dt_3,C4048_3_FCST_DT fcst_dt_3
					,C4048_4_LOAD_DT load_dt_4,C4048_4_FCST_DT fcst_dt_4
					,C4048_5_LOAD_DT load_dt_5,C4048_5_FCST_DT fcst_dt_5
					,C4048_6_LOAD_DT load_dt_6,C4048_6_FCST_DT fcst_dt_6
					, SUM(NVL(C4048_1_SALES_WTD_AVG_QTY,0)) sales_wtd_avg_1
					, SUM(NVL(C4048_1_SALES_FORECAST_QTY,0)) sales_fcst_1
					, SUM(NVL(C4048_1_ACTUAL_SALES_QTY,0)) act_sales_1
					,( SUM(nvl(C4048_1_SALES_WTD_AVG_QTY,0))-SUM(nvl(C4048_1_ACTUAL_SALES_QTY,0))) sales_var_1,
				   ABS(SUM(nvl(C4048_1_SALES_WTD_AVG_QTY,0))-SUM(nvl(C4048_1_ACTUAL_SALES_QTY,0))) abs_sales_var_1

					,DECODE(SUM(NVL(C4048_1_SALES_WTD_AVG_QTY,0)),0,0,
					ROUND(((SUM(nvl(C4048_1_SALES_WTD_AVG_QTY,0)) -SUM(nvl(C4048_1_ACTUAL_SALES_QTY,0))))/SUM(NVL(C4048_1_SALES_WTD_AVG_QTY,1)),4)) sales_var_pct_1
					,(SUM(nvl(C4048_1_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_1_ACTUAL_SALES_QTY,0))) FCST_VAR_1
					,ABS(SUM(nvl(C4048_1_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_1_ACTUAL_SALES_QTY,0))) ABS_FCST_VAR_1
					,

					ROUND(DECODE(SUM(NVL(C4048_1_SALES_FORECAST_QTY,0)),0,0,
					round((SUM(nvl(C4048_1_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_1_ACTUAL_SALES_QTY,0)))/ SUM(nvl(C4048_1_SALES_FORECAST_QTY,1)),4)),4)
					 FCST_VAR_PCT_1

					--2
					, SUM(NVL(C4048_2_SALES_WTD_AVG_QTY,0)) sales_wtd_avg_2
					, SUM(NVL(C4048_2_SALES_FORECAST_QTY,0)) sales_fcst_2
					, SUM(NVL(C4048_2_ACTUAL_SALES_QTY,0)) act_sales_2
					,( SUM(nvl(C4048_2_SALES_WTD_AVG_QTY,0))-SUM(nvl(C4048_2_ACTUAL_SALES_QTY,0))) sales_var_2,
				   ABS(SUM(nvl(C4048_2_SALES_WTD_AVG_QTY,0))-SUM(nvl(C4048_2_ACTUAL_SALES_QTY,0))) abs_sales_var_2

					,DECODE(SUM(NVL(C4048_2_SALES_WTD_AVG_QTY,0)),0,0,
					ROUND(((SUM(nvl(C4048_2_SALES_WTD_AVG_QTY,0)) -SUM(nvl(C4048_2_ACTUAL_SALES_QTY,0))))/SUM(NVL(C4048_2_SALES_WTD_AVG_QTY,1)),4)) sales_var_pct_2
					,(SUM(nvl(C4048_2_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_2_ACTUAL_SALES_QTY,0))) FCST_VAR_2
					,ABS(SUM(nvl(C4048_2_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_2_ACTUAL_SALES_QTY,0))) ABS_FCST_VAR_2
					,

					ROUND(DECODE(SUM(NVL(C4048_2_SALES_FORECAST_QTY,0)),0,0,
					round((SUM(nvl(C4048_2_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_2_ACTUAL_SALES_QTY,0)))/ SUM(nvl(C4048_2_SALES_FORECAST_QTY,1)),4)),4)
					 FCST_VAR_PCT_2

					 --3
					 , SUM(NVL(C4048_3_SALES_WTD_AVG_QTY,0)) sales_wtd_avg_3
					, SUM(NVL(C4048_3_SALES_FORECAST_QTY,0)) sales_fcst_3
					, SUM(NVL(C4048_3_ACTUAL_SALES_QTY,0)) act_sales_3
					, ( SUM(nvl(C4048_3_SALES_WTD_AVG_QTY,0))-SUM(nvl(C4048_3_ACTUAL_SALES_QTY,0))) sales_var_3,
				   ABS(SUM(nvl(C4048_3_SALES_WTD_AVG_QTY,0))-SUM(nvl(C4048_3_ACTUAL_SALES_QTY,0))) abs_sales_var_3

					,DECODE(SUM(NVL(C4048_3_SALES_WTD_AVG_QTY,0)),0,0,
					ROUND(((SUM(nvl(C4048_3_SALES_WTD_AVG_QTY,0)) -SUM(nvl(C4048_3_ACTUAL_SALES_QTY,0))))/SUM(NVL(C4048_3_SALES_WTD_AVG_QTY,1)),4)) sales_var_pct_3
					,(SUM(nvl(C4048_3_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_3_ACTUAL_SALES_QTY,0))) FCST_VAR_3
					,ABS(SUM(nvl(C4048_3_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_3_ACTUAL_SALES_QTY,0))) ABS_FCST_VAR_3
					,

					ROUND(DECODE(SUM(NVL(C4048_3_SALES_FORECAST_QTY,0)),0,0,
					round((SUM(nvl(C4048_3_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_3_ACTUAL_SALES_QTY,0)))/ SUM(nvl(C4048_3_SALES_FORECAST_QTY,1)),4)),4)
					 FCST_VAR_PCT_3

					--4
					, SUM(NVL(C4048_4_SALES_WTD_AVG_QTY,0)) sales_wtd_avg_4
					, SUM(NVL(C4048_4_SALES_FORECAST_QTY,0)) sales_fcst_4
					, SUM(NVL(C4048_4_ACTUAL_SALES_QTY,0)) act_sales_4
					, ( SUM(nvl(C4048_4_SALES_WTD_AVG_QTY,0))-SUM(nvl(C4048_4_ACTUAL_SALES_QTY,0))) sales_var_4,
				   ABS(SUM(nvl(C4048_4_SALES_WTD_AVG_QTY,0))-SUM(nvl(C4048_4_ACTUAL_SALES_QTY,0))) abs_sales_var_4

					,DECODE(SUM(NVL(C4048_4_SALES_WTD_AVG_QTY,0)),0,0,
					ROUND(((SUM(nvl(C4048_4_SALES_WTD_AVG_QTY,0)) -SUM(nvl(C4048_4_ACTUAL_SALES_QTY,0))))/SUM(NVL(C4048_4_SALES_WTD_AVG_QTY,1)),4)) sales_var_pct_4
					,(SUM(nvl(C4048_4_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_4_ACTUAL_SALES_QTY,0))) FCST_VAR_4
					,ABS(SUM(nvl(C4048_4_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_4_ACTUAL_SALES_QTY,0))) ABS_FCST_VAR_4
					,

					ROUND(DECODE(SUM(NVL(C4048_4_SALES_FORECAST_QTY,0)),0,0,
					round((SUM(nvl(C4048_4_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_4_ACTUAL_SALES_QTY,0)))/ SUM(nvl(C4048_4_SALES_FORECAST_QTY,1)),4)),4)
					 FCST_VAR_PCT_4

					--5
					, SUM(NVL(C4048_5_SALES_WTD_AVG_QTY,0)) sales_wtd_avg_5
					, SUM(NVL(C4048_5_SALES_FORECAST_QTY,0)) sales_fcst_5
					, SUM(NVL(C4048_5_ACTUAL_SALES_QTY,0)) act_sales_5
					, ( SUM(nvl(C4048_5_SALES_WTD_AVG_QTY,0))-SUM(nvl(C4048_5_ACTUAL_SALES_QTY,0))) sales_var_5,
				   ABS(SUM(nvl(C4048_5_SALES_WTD_AVG_QTY,0))-SUM(nvl(C4048_5_ACTUAL_SALES_QTY,0))) abs_sales_var_5

					,DECODE(SUM(NVL(C4048_5_SALES_WTD_AVG_QTY,0)),0,0,
					ROUND(((SUM(nvl(C4048_5_SALES_WTD_AVG_QTY,0)) -SUM(nvl(C4048_5_ACTUAL_SALES_QTY,0))))/SUM(NVL(C4048_5_SALES_WTD_AVG_QTY,1)),4)) sales_var_pct_5
					,(SUM(nvl(C4048_5_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_5_ACTUAL_SALES_QTY,0))) FCST_VAR_5
					,ABS(SUM(nvl(C4048_5_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_5_ACTUAL_SALES_QTY,0))) ABS_FCST_VAR_5
					,

					ROUND(DECODE(SUM(NVL(C4048_5_SALES_FORECAST_QTY,0)),0,0,
					round((SUM(nvl(C4048_5_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_5_ACTUAL_SALES_QTY,0)))/ SUM(nvl(C4048_5_SALES_FORECAST_QTY,1)),4)),4)
					 FCST_VAR_PCT_5

					--6
					, SUM(NVL(C4048_6_SALES_WTD_AVG_QTY,0)) sales_wtd_avg_6
					, SUM(NVL(C4048_6_SALES_FORECAST_QTY,0)) sales_fcst_6
					, SUM(NVL(C4048_6_ACTUAL_SALES_QTY,0)) act_sales_6
					,( SUM(nvl(C4048_6_SALES_WTD_AVG_QTY,0))-SUM(nvl(C4048_6_ACTUAL_SALES_QTY,0))) sales_var_6,
				   ABS(SUM(nvl(C4048_6_SALES_WTD_AVG_QTY,0))-SUM(nvl(C4048_6_ACTUAL_SALES_QTY,0))) abs_sales_var_6

					,DECODE(SUM(NVL(C4048_6_SALES_WTD_AVG_QTY,0)),0,0,
					ROUND(((SUM(nvl(C4048_6_SALES_WTD_AVG_QTY,0)) -SUM(nvl(C4048_6_ACTUAL_SALES_QTY,0))))/SUM(NVL(C4048_6_SALES_WTD_AVG_QTY,1)),4)) sales_var_pct_6
					,(SUM(nvl(C4048_6_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_6_ACTUAL_SALES_QTY,0))) FCST_VAR_6
					,ABS(SUM(nvl(C4048_6_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_6_ACTUAL_SALES_QTY,0))) ABS_FCST_VAR_6
					,

					ROUND(DECODE(SUM(NVL(C4048_6_SALES_FORECAST_QTY,0)),0,0,
					round((SUM(nvl(C4048_6_SALES_FORECAST_QTY,0))- SUM(nvl(C4048_6_ACTUAL_SALES_QTY,0)))/ SUM(nvl(C4048_6_SALES_FORECAST_QTY,1)),4)),4)
					 FCST_VAR_PCT_6


		from T4048_FORECAST_ACCURACY_DETAILS
		WHERE C4047_FORECAST_ACCURACY_ID= P_fcst_accur_ID
		AND C4048_VOID_FL IS NULL
		GROUP BY C207_SYSTEM_ID ,C207_SYSTEM_nm,C4048_1_LOAD_DT,C4048_1_FCST_DT,C4048_2_LOAD_DT,C4048_2_FCST_DT,C4048_3_LOAD_DT,C4048_3_FCST_DT,
				C4048_4_LOAD_DT,C4048_4_FCST_DT,C4048_5_LOAD_DT
				,C4048_5_FCST_DT,C4048_6_LOAD_DT,C4048_6_FCST_DT;


	BEGIN

		  UPDATE T4050_FORECAST_ACCURACY_SYSTEM_DETAILS
		  SET c4050_VOID_FL ='Y'
		  WHERE C4047_FORECAST_ACCURACY_ID <> P_fcst_accur_ID
		  AND c4050_VOID_FL IS NULL;

		  FOR system_variance IN CUR_system_Variance
        LOOP



					INSERT INTO T4050_FORECAST_ACCURACY_SYSTEM_DETAILS(C4047_FORECAST_ACCURACY_ID,C207_SYSTEM_ID,C207_SYSTEM_NM,
					c4050_1_LOAD_DT,c4050_1_FCST_DT,c4050_1_SALES_WTD_AVG_QTY,c4050_1_SALES_FORECAST_QTY,c4050_1_ACTUAL_SALES_QTY,c4050_1_SALES_VARIANCE_QTY,c4050_1_SALES_ABS_VARIANCE_QTY,c4050_1_SALES_VARIANCE_PCT,c4050_1_SALES_FCST_OVERRIDE_VARIANCE_QTY,c4050_1_SALES_FCST_OVERRIDE_VARIANCE_PCT,
					c4050_2_LOAD_DT,c4050_2_FCST_DT,c4050_2_SALES_WTD_AVG_QTY,c4050_2_SALES_FORECAST_QTY,c4050_2_ACTUAL_SALES_QTY,c4050_2_SALES_VARIANCE_QTY,c4050_2_SALES_ABS_VARIANCE_QTY,c4050_2_SALES_VARIANCE_PCT,c4050_2_SALES_FCST_OVERRIDE_VARIANCE_QTY,c4050_2_SALES_FCST_OVERRIDE_VARIANCE_PCT,
					c4050_3_LOAD_DT,c4050_3_FCST_DT,c4050_3_SALES_WTD_AVG_QTY,c4050_3_SALES_FORECAST_QTY,c4050_3_ACTUAL_SALES_QTY,c4050_3_SALES_VARIANCE_QTY,c4050_3_SALES_ABS_VARIANCE_QTY,c4050_3_SALES_VARIANCE_PCT,c4050_3_SALES_FCST_OVERRIDE_VARIANCE_QTY,c4050_3_SALES_FCST_OVERRIDE_VARIANCE_PCT,
					c4050_4_LOAD_DT,c4050_4_FCST_DT,c4050_4_SALES_WTD_AVG_QTY,c4050_4_SALES_FORECAST_QTY,c4050_4_ACTUAL_SALES_QTY,c4050_4_SALES_VARIANCE_QTY,c4050_4_SALES_ABS_VARIANCE_QTY,c4050_4_SALES_VARIANCE_PCT,c4050_4_SALES_FCST_OVERRIDE_VARIANCE_QTY,c4050_4_SALES_FCST_OVERRIDE_VARIANCE_PCT,
					c4050_5_LOAD_DT,c4050_5_FCST_DT,c4050_5_SALES_WTD_AVG_QTY,c4050_5_SALES_FORECAST_QTY,c4050_5_ACTUAL_SALES_QTY,c4050_5_SALES_VARIANCE_QTY,c4050_5_SALES_ABS_VARIANCE_QTY,c4050_5_SALES_VARIANCE_PCT,c4050_5_SALES_FCST_OVERRIDE_VARIANCE_QTY,c4050_5_SALES_FCST_OVERRIDE_VARIANCE_PCT,
					c4050_6_LOAD_DT,c4050_6_FCST_DT,c4050_6_SALES_WTD_AVG_QTY,c4050_6_SALES_FORECAST_QTY,c4050_6_ACTUAL_SALES_QTY,c4050_6_SALES_VARIANCE_QTY,c4050_6_SALES_ABS_VARIANCE_QTY,c4050_6_SALES_VARIANCE_PCT,c4050_6_SALES_FCST_OVERRIDE_VARIANCE_QTY,c4050_6_SALES_FCST_OVERRIDE_VARIANCE_PCT
					)
					VALUES (
					P_fcst_accur_ID,system_variance.system_id,system_variance.system_nm
					,system_variance.load_dt_1,system_variance.fcst_dt_1,system_variance.sales_wtd_avg_1,system_variance.sales_fcst_1,system_variance.act_sales_1,system_variance.sales_var_1 ,system_variance.abs_sales_var_1,system_variance.sales_var_pct_1,system_variance.FCST_VAR_1,system_variance.FCST_VAR_PCT_1
					,system_variance.load_dt_2,system_variance.fcst_dt_2,system_variance.sales_wtd_avg_2,system_variance.sales_fcst_2,system_variance.act_sales_2,system_variance.sales_var_2 ,system_variance.abs_sales_var_2,system_variance.sales_var_pct_2,system_variance.FCST_VAR_2,system_variance.FCST_VAR_PCT_2
					,system_variance.load_dt_3,system_variance.fcst_dt_3,system_variance.sales_wtd_avg_3,system_variance.sales_fcst_3,system_variance.act_sales_3,system_variance.sales_var_3 ,system_variance.abs_sales_var_3,system_variance.sales_var_pct_3,system_variance.FCST_VAR_3,system_variance.FCST_VAR_PCT_3
					,system_variance.load_dt_4,system_variance.fcst_dt_4,system_variance.sales_wtd_avg_4,system_variance.sales_fcst_4,system_variance.act_sales_4,system_variance.sales_var_4 ,system_variance.abs_sales_var_4,system_variance.sales_var_pct_4,system_variance.FCST_VAR_4,system_variance.FCST_VAR_PCT_4
					,system_variance.load_dt_5,system_variance.fcst_dt_5,system_variance.sales_wtd_avg_5,system_variance.sales_fcst_5,system_variance.act_sales_5,system_variance.sales_var_5 ,system_variance.abs_sales_var_5,system_variance.sales_var_pct_5,system_variance.FCST_VAR_5,system_variance.FCST_VAR_PCT_5
					,system_variance.load_dt_6,system_variance.fcst_dt_6,system_variance.sales_wtd_avg_6,system_variance.sales_fcst_6,system_variance.act_sales_6,system_variance.sales_var_6 ,system_variance.abs_sales_var_6,system_variance.sales_var_pct_6,system_variance.FCST_VAR_6,system_variance.FCST_VAR_PCT_6

					);

		END LOOP;


	END gm_ld_calc_variance_by_system;

	*/

END gm_pkg_oppr_ld_forecast_accuracy;
/
