/* Formatted on 2011/06/09 12:16 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\purchasing\gm_pkg_oppr_ld_sales_demand.bdy";
-- show err  Package Body gm_pkg_oppr_ld_sales_demand
-- exec gm_pkg_oppr_ld_demand.gm_op_ld_demand_main(17);
/*
 DELETE FROM T4042_DEMAND_SHEET_DETAIL;
 DELETE FROM T4041_DEMAND_SHEET_MAPPING;
 DELETE FROM T4040_DEMAND_SHEET;
 COMMIT ;
 exec gm_pkg_oppr_ld_demand.gm_op_ld_demand_main(5009);

*/

CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ld_sales_demand
IS
	/*************************************************************************
	 * Purpose: Procedure used to load sales demand, calculated value and
	 *	Forecast information
	 *************************************************************************/
	PROCEDURE gm_op_ld_main (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_demand_period 	IN	 t4020_demand_master.c4020_demand_period%TYPE
	  , p_forecast_period	IN	 t4020_demand_master.c4020_forecast_period%TYPE
	  , p_inc_current_fl	IN	 t4020_demand_master.c4020_include_current_fl%TYPE
	)
	AS
		--
		v_from_period  DATE;
		v_to_period    DATE;
	--
	BEGIN
		--
		-- Below code to fetch demand (from and to period)
		gm_pkg_oppr_ld_demand.gm_op_calc_period (p_demand_period, p_inc_current_fl, 'D', v_from_period, v_to_period);
		--
		--DBMS_OUTPUT.put_line ('************ Inside sales   ****' || v_from_period || ' - ' || v_to_period);
		--
        -- Below code to load required account info
        gm_pkg_oppr_ld_sales_demand.gm_op_ld_account_info(p_demand_id);
        
        --DBMS_OUTPUT.put_line ('************ Loading Demand   1****'  || ' - ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS') );
		-- Below procedure called to load sales demand information
		gm_pkg_oppr_ld_sales_demand.gm_op_ld_demand (p_demand_id, p_demand_sheet_id, v_from_period, v_to_period);
        --DBMS_OUTPUT.put_line ('************ Loaded Demand   2****'  || ' - ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS') );		
--
		-- Below code to fetch forecast (from and to period)
		gm_pkg_oppr_ld_demand.gm_op_calc_period (p_forecast_period, p_inc_current_fl, 'F', v_from_period, v_to_period);
        
        --DBMS_OUTPUT.put_line ('************ Weighted Avg   ****' );
		-- Below procedure called to load sales Weighted and Avg sales information
		gm_pkg_oppr_ld_demand.gm_op_ld_demand_calc_avg (p_demand_sheet_id, v_from_period);
                              
		--
        --DBMS_OUTPUT.put_line ('************ Loading Forecast   3****'  || ' - ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS') );
		-- Below procedure called to load forecast information
		gm_pkg_oppr_ld_sales_demand.gm_op_ld_forecast (p_demand_id, p_demand_sheet_id, v_from_period, v_to_period);
        --DBMS_OUTPUT.put_line ('************ Loaded Forecast   4****'  || ' - ' || TO_CHAR(SYSDATE,'MM/DD/YYYY HH:MI:SS') );
		--
		-- Below procedure called to load sales Weighted and Avg sales information
		gm_pkg_oppr_ld_demand.gm_op_ld_forecast_avg (p_demand_sheet_id, v_from_period);
		--
		-- Below procedure called to load sales variance
		gm_pkg_oppr_ld_demand.gm_op_ld_forecast_variance (p_demand_sheet_id, v_from_period);
        
		-- Below procedure called to load Par Information [To be moved to World wide sheet]
		--gm_pkg_oppr_ld_sales_demand.gm_op_ld_par_value (p_demand_id, p_demand_sheet_id, v_from_period);
        
		-- Below procedure called to load Sales Backorder
		gm_pkg_oppr_ld_sales_demand.gm_op_ld_sales_bo (p_demand_id, p_demand_sheet_id, v_from_period);
	--
	END gm_op_ld_main;

  /*************************************************************************
    * Purpose: Procedure to be used to load Account related information based on
    * sheet information [Load based on level]
    *  Staredt with Zone, Region, Intermediate . Will load account in temp table
    *************************************************************************/
    PROCEDURE gm_op_ld_account_info (
       p_demand_id   IN   t4020_demand_master.c4020_demand_master_id%TYPE
    )
    AS
       --
       v_level_id      t4015_global_sheet_mapping.c901_level_id%TYPE;
       v_level_value   t4015_global_sheet_mapping.c901_level_value%TYPE;
    --
    BEGIN
       --
       SELECT c901_level_id, c901_level_value
         INTO v_level_id, v_level_value
         FROM t4015_global_sheet_mapping t4015, t4020_demand_master t4020
        WHERE t4020.c4020_demand_master_id = p_demand_id
          AND t4020.c4015_global_sheet_map_id = t4015.c4015_global_sheet_map_id;

       --
       DELETE FROM my_temp_list;

       --
       INSERT INTO my_temp_list
                   (my_temp_txn_id)
          SELECT DISTINCT v700.ac_id
                     FROM v700_territory_mapping_detail v700
                    WHERE v700.divid =  DECODE (v_level_id,102581, v_level_value,v700.divid)
                     and v700.gp_id = DECODE (v_level_id,102583, v_level_value,v700.gp_id) -- Added for Zone level edit
                      AND v700.region_id = DECODE (v_level_id,102584, v_level_value,v700.region_id)
                      AND v700.disttypeid = DECODE (v_level_id,102582, '70105',v700.disttypeid) -- OUS Distributor 
                      AND v700.ac_id NOT IN (SELECT c704_account_id from t704_account where c1900_company_id=1001); -- Exclude BBA Account
    --
    END gm_op_ld_account_info;
   
	--
	/*************************************************************************
	 * Purpose: Procedure used to load sales demand for selected period
	 * 40030 maps to Group Mapping and 40032 maps to Region Mapping
	 *************************************************************************/
	PROCEDURE gm_op_ld_demand (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_from_period		IN	 DATE
	  , p_to_period 		IN	 DATE
	)
	AS
		--
		v_cur_part 		t4011_group_detail.c205_part_number_id%TYPE;
		v_pre_part 		t4011_group_detail.c205_part_number_id%TYPE;
		v_cur_month 	v9001_month_list.month_value%TYPE;
		v_pre_month 	v9001_month_list.month_value%TYPE;
		v_cur_grpid 	t4010_group.c4010_group_id%TYPE;
		v_pre_grpid 	t4010_group.c4010_group_id%TYPE;
		v_price_type    t4011_group_detail.c901_part_pricing_type%TYPE;
		--
		CURSOR demand_cur
		IS
			SELECT	 partgrpinfo.c4010_group_id, partgrpinfo.c205_part_number_id, partgrpinfo.month_value
				   , SUM (NVL (c502_item_qty, 0)) item_qty ,partgrpinfo.price_type price_type
				FROM (SELECT t4011.c4010_group_id, t4011.c205_part_number_id, v9001.month_value, t4011.c901_part_pricing_type price_type
						FROM t4010_group t4010
						   , t4021_demand_mapping t4021
						   , t4011_group_detail t4011
						   , v9001_month_list v9001
					   WHERE t4021.c4020_demand_master_id = p_demand_id
						 AND t4021.c901_ref_type = 40030
						 AND t4021.c4021_ref_id = t4010.c4010_group_id
						 AND t4010.c4010_group_id = t4011.c4010_group_id
						 AND v9001.month_value >= p_from_period
						 AND v9001.month_value <= p_to_period
						 ) partgrpinfo
				   , (SELECT TRUNC(t501.c501_order_date,'month') order_date
						   , t502.c205_part_number_id, t502.c502_item_qty
						FROM my_temp_list mtl
						   , t501_order t501
						   , t502_item_order t502
					   WHERE t501.c704_account_id = mtl.my_temp_txn_id
						 AND t501.c501_order_date >= p_from_period
						 AND t501.c501_order_date <= p_to_period
						 AND t501.c501_order_id = t502.c501_order_id
						 AND t501.c501_void_fl IS NULL
						 AND NVL (t501.c901_order_type, -999) <> 2524
						 AND NVL (c901_order_type, -9999) NOT IN (SELECT TO_NUMBER(c906_rule_value) FROM v901_order_type_grp)
					   ) partorderinfo
			   WHERE partgrpinfo.month_value = partorderinfo.order_date(+)
			   AND partgrpinfo.c205_part_number_id = partorderinfo.c205_part_number_id(+)					 
			GROUP BY partgrpinfo.c4010_group_id, partgrpinfo.c205_part_number_id, partgrpinfo.month_value, partgrpinfo.price_type
			ORDER BY c205_part_number_id,month_value;
	BEGIN
		--
		-- load demand information
		FOR set_val IN demand_cur
		LOOP
			--
			v_cur_month := set_val.month_value;
			v_cur_part  := set_val.c205_part_number_id;
			v_cur_grpid := set_val.c4010_group_id;
			v_price_type := set_val.price_type;
			
			-- When Same Part exist in same Sheet and in two different groups , then we are excluding the demand for Secondary group the part belongs to. 
			IF (v_cur_month = v_pre_month AND v_cur_part = v_pre_part 
				AND v_cur_grpid != v_pre_grpid AND v_price_type != '52080')
			THEN
				NULL;
			ELSE				
				gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
														  , set_val.c4010_group_id
	                                                      , 102660
														  , 50560
														  , set_val.month_value
														  , set_val.c205_part_number_id
														  , set_val.item_qty
														   );
			END IF;
			
			v_pre_month := v_cur_month;
			v_pre_part  := v_cur_part;
			v_pre_grpid := v_cur_grpid;
			v_cur_month := '';
			v_cur_part := '';
			v_cur_grpid := '';
			v_price_type := '';
		--
		END LOOP;
	--
	END gm_op_ld_demand;



	--
	/*************************************************************************
	 * Purpose: Procedure used to load sales forecast information
	 * Based on the demand weighted will load forecase information
	 *************************************************************************/
	PROCEDURE gm_op_ld_forecast (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_from_period		IN	 DATE
	  , p_to_period 		IN	 DATE
	)
	AS
		--
		v_history_fl            CHAR (1);
        v_comments_fl           CHAR (1);
        v_weighted_avg_diff     t4042_demand_sheet_detail.c4042_weighted_avg_diff_qty%TYPE;
        v_current_qty           t4042_demand_sheet_detail.c4042_qty%TYPE;

    	--
		CURSOR forecast_cur
		IS
			SELECT	 partgrpinfo.c4010_group_id, partgrpinfo.c205_part_number_id, partgrpinfo.month_value
				   , NVL (partgrowhtinfo.c901_ref_type, grpgrowthinfo.c901_ref_type) growth_type
				   , NVL (partgrowhtinfo.c4031_value, grpgrowthinfo.c4031_value) growth_value, dmdweightavg.weight_avg
                   , NVL(partgrowhtinfo.comments_fl, grpgrowthinfo.comments_fl )comments_fl
                   , DECODE(partgrpinfo.c901_part_pricing_type, 52080, 'Y', 'N' ) primaryfl            
				FROM (SELECT t4011.c4010_group_id, t4011.c205_part_number_id, v9001.month_value 
                        , t4011.c901_part_pricing_type
						FROM t4010_group t4010
						   , t4021_demand_mapping t4021
						   , t4011_group_detail t4011
						   , v9001_month_list v9001
					   WHERE t4021.c4020_demand_master_id = p_demand_id
						 AND t4021.c901_ref_type = 40030
						 AND t4021.c4021_ref_id = t4010.c4010_group_id
						 AND t4010.c4010_group_id = t4011.c4010_group_id
						 AND v9001.month_value >= p_from_period
						 AND v9001.month_value <= p_to_period
                         ) partgrpinfo
                    -- Fetch Group level info      
				   , (SELECT t4030.c4030_ref_id GROUP_ID, t4031.c4031_start_date, t4031.c4031_end_date
						   , t4031.c901_ref_type, t4031.c4031_value
                           , NULL comments_fl
                           --get_log_flag (t4031.c4031_growth_details_id, 1224) comments_fl
						FROM t4020_demand_master t4020, t4030_demand_growth_mapping t4030, t4031_growth_details t4031
					   WHERE t4020.c4020_demand_master_id = p_demand_id
						 AND t4020.c4020_demand_master_id = t4030.c4020_demand_master_id
						 AND t4031.c4030_demand_growth_id = t4030.c4030_demand_growth_id
						 AND t4030.c901_ref_type = 20297
						 AND t4031.c4031_start_date >= p_from_period
						 AND t4031.c4031_end_date <= p_to_period
						 AND t4030.c4030_void_fl IS NULL
                         AND t4031.c4031_void_fl IS NULL
						 AND t4031.c4031_value IS NOT NULL) grpgrowthinfo
                   -- Fetch Part Level override info      
				   , (SELECT t4030.c4030_ref_id part_number, t4031.c4031_start_date, t4031.c4031_end_date
						   , t4031.c901_ref_type, t4031.c4031_value
                           , NULL comments_fl
						FROM t4020_demand_master t4020, t4030_demand_growth_mapping t4030, t4031_growth_details t4031
					   WHERE t4020.c4020_demand_master_id = p_demand_id
						 AND t4020.c4020_demand_master_id = t4030.c4020_demand_master_id
						 AND t4031.c4030_demand_growth_id = t4030.c4030_demand_growth_id
						 AND t4030.c901_ref_type = 20295
						 AND t4031.c4031_start_date >= p_from_period
						 AND t4031.c4031_end_date <= p_to_period
						 AND t4030.c4030_void_fl IS NULL
                         AND t4031.c4031_void_fl IS NULL
						 AND t4031.c4031_value IS NOT NULL) partgrowhtinfo
                   -- Fetch Weighted Average info      
				   , (SELECT t4042.c4042_ref_id group_id,t4042.c205_part_number_id, t4042.c4042_period, t4042.c4042_qty weight_avg
						FROM my_temp_demand_sheet_detail t4042
					   WHERE t4042.c4040_demand_sheet_id = p_demand_sheet_id AND t4042.c901_type = 50562) dmdweightavg
			   WHERE partgrpinfo.c4010_group_id = grpgrowthinfo.group_id (+)
				 AND partgrpinfo.month_value = grpgrowthinfo.c4031_start_date (+)
				 AND partgrpinfo.c205_part_number_id = partgrowhtinfo.part_number(+)
				 AND partgrpinfo.month_value = partgrowhtinfo.c4031_start_date(+)
				 AND partgrpinfo.c205_part_number_id = dmdweightavg.c205_part_number_id(+)
                 AND partgrpinfo.c4010_group_id = dmdweightavg.group_id(+)
				 --AND partgrpinfo.month_value = dmdweightavg.c4042_period(+)
			ORDER BY partgrpinfo.c205_part_number_id, partgrpinfo.month_value;
	BEGIN
		--
        DBMS_OUTPUT.put_line ('******** Inside Weighted Avg new logic flow **** ' );
		-- load demand information
		FOR set_val IN forecast_cur
		LOOP
          
           IF ( set_val.primaryfl = 'N' and NVL(set_val.growth_type,0) != 20381)
            THEN
                v_current_qty := 0;
                v_weighted_avg_diff := 0;
                v_history_fl := NULL;
            ELSE      
              -- Get Forecast value 
              gm_pkg_oppr_ld_demand.gm_op_calc_forecast (set_val.growth_type,
                                             set_val.growth_value,
                                             set_val.weight_avg,
                                             set_val.primaryfl, 
                                             v_current_qty,
                                             v_weighted_avg_diff,
                                             v_history_fl
                                            );
            END IF;			
      
			-- Save the value
			gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , set_val.c4010_group_id
                                                      , 102660            
													  , 50563 -- Forecast qty
													  , set_val.month_value
													  , set_val.c205_part_number_id
													  , v_current_qty
													  , NULL
													  , NULL
													  , NULL
													  , v_history_fl
													  , set_val.comments_fl
													  , v_weighted_avg_diff
													   );
		--
		END LOOP;
	--
	END gm_op_ld_forecast;

--
/*************************************************************************
 * Purpose: Procedure used to load sales back order
 *************************************************************************/
	PROCEDURE gm_op_ld_sales_bo (
		p_demand_master_id	 IN   t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	 IN   t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_to_period 		 IN   DATE
	)
	AS
		CURSOR salesbo_cur
		IS
			SELECT	 t4011.c4010_group_id ref_id, t253f.c205_part_number_id p_num, SUM (t253f.c502_item_qty) qty
				FROM my_temp_list mtl
				   , t4021_demand_mapping t4021
				   , t4011_group_detail t4011
				   , t253e_order_lock t253e
				   , t253f_item_order_lock t253f
				   , t4040_demand_sheet t4040
			   WHERE t4021.c4020_demand_master_id = p_demand_master_id
				 AND t4040.c4020_demand_master_id = p_demand_master_id
				 AND t4040.c4040_demand_sheet_id = p_demand_sheet_id
				 AND t4021.c901_ref_type = 40032
				 AND t253e.c704_account_id = mtl.my_temp_txn_id
				 AND t253e.c501_order_id = t253f.c501_order_id
				 AND NVL (t253e.c901_order_type, -999) = 2525
				 AND t253e.c501_status_fl = 0
				 AND t253e.c501_void_fl IS NULL
				 AND t4011.c4010_group_id IN (SELECT c4021_ref_id
												FROM t4021_demand_mapping t4021
											   WHERE t4021.c4020_demand_master_id = p_demand_master_id
                                                 AND t4021.c901_ref_type = 40030)
				 AND t253f.c205_part_number_id = t4011.c205_part_number_id
				 AND t253e.c250_inventory_lock_id = t4040.c250_inventory_lock_id
				 AND t253f.c250_inventory_lock_id = t4040.c250_inventory_lock_id
			GROUP BY t4011.c4010_group_id, t253f.c205_part_number_id;
	BEGIN
		FOR val IN salesbo_cur
		LOOP
			gm_pkg_oppr_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , val.ref_id
                                                      , 102660            
													  , 50567
													  , p_to_period
													  , val.p_num
													  , val.qty
													   );
		END LOOP;
	END gm_op_ld_sales_bo;
--
END gm_pkg_oppr_ld_sales_demand;
/
