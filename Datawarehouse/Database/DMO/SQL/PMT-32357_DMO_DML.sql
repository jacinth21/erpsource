
--T4070_DIVISION_MP_MAPPING Insert
--Spine(2000),Multi Part Spine Sheet(22933)
INSERT INTO globus_dm_operation.T4070_DIVISION_MP_MAPPING (C4070_DIVISION_MP_MAP_ID, C1910_DIVISION_ID, C4020_DEMAND_MASTER_ID, C4070_VOID_FL, C4070_CREATED_BY, C4070_CREATED_DATE, C4070_UPDATED_BY,C4070_UPDATED_DATE)
VALUES (globus_dm_operation.S4070_DIVISION_MP_MAP_ID.nextval, '2000', '22933', null, 303510, sysdate, null,null);

--Algea Therapies(2001),Multi Part Spine Sheet(22933)
INSERT INTO globus_dm_operation.T4070_DIVISION_MP_MAPPING (C4070_DIVISION_MP_MAP_ID, C1910_DIVISION_ID, C4020_DEMAND_MASTER_ID, C4070_VOID_FL, C4070_CREATED_BY, C4070_CREATED_DATE, C4070_UPDATED_BY,C4070_UPDATED_DATE)
VALUES (globus_dm_operation.S4070_DIVISION_MP_MAP_ID.nextval, '2001', '22933', null, 303510, sysdate, null,null);

--I-N-Robotics(2004),Multi Part Spine Sheet(22933)
INSERT INTO globus_dm_operation.T4070_DIVISION_MP_MAPPING (C4070_DIVISION_MP_MAP_ID, C1910_DIVISION_ID, C4020_DEMAND_MASTER_ID, C4070_VOID_FL, C4070_CREATED_BY, C4070_CREATED_DATE, C4070_UPDATED_BY,C4070_UPDATED_DATE)
VALUES (globus_dm_operation.S4070_DIVISION_MP_MAP_ID.nextval, '2004', '22933', null, 303510, sysdate, null,null);

 --Trauma(2005),Multi Part Trauma Sheet(33694)
INSERT INTO globus_dm_operation.T4070_DIVISION_MP_MAPPING (C4070_DIVISION_MP_MAP_ID, C1910_DIVISION_ID, C4020_DEMAND_MASTER_ID, C4070_VOID_FL, C4070_CREATED_BY, C4070_CREATED_DATE, C4070_UPDATED_BY,C4070_UPDATED_DATE)
VALUES (globus_dm_operation.S4070_DIVISION_MP_MAP_ID.nextval, '2005', '33694', null, 303510, sysdate, null,null);


--Rank INSERT
--108320(A rank)
INSERT INTO globus_dm_operation.T4070C_RANK_CONFIG(C4070C_RANK_CONFIG_ID, C4070C_FROM_PERCENT, C4070C_TO_PERCENT,C901_RANK_CD, C4070C_VOID_FL)
VALUES (globus_dm_operation.S4070C_RANK_CONFIG_ID.nextval,0,80,108320,null);

--108321(B rank)
INSERT INTO globus_dm_operation.T4070C_RANK_CONFIG(C4070C_RANK_CONFIG_ID, C4070C_FROM_PERCENT, C4070C_TO_PERCENT,C901_RANK_CD, C4070C_VOID_FL)
VALUES (globus_dm_operation.S4070C_RANK_CONFIG_ID.nextval,80,95,108321,null);

--108321(C Rank)
INSERT INTO globus_dm_operation.T4070C_RANK_CONFIG(C4070C_RANK_CONFIG_ID, C4070C_FROM_PERCENT, C4070C_TO_PERCENT,C901_RANK_CD, C4070C_VOID_FL)
VALUES (globus_dm_operation.S4070C_RANK_CONFIG_ID.nextval,95,100,108322,null);