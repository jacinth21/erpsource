
 CREATE OR REPLACE PUBLIC SYNONYM gm_pkg_oppr_ld_ttp_by_vendor_capacity_txn FOR globus_dm_operation.gm_pkg_oppr_ld_ttp_by_vendor_capacity_txn;
 CREATE OR REPLACE PUBLIC SYNONYM gm_pkg_oppr_id_ttp_by_vendor_capacity_rpt FOR globus_dm_operation.gm_pkg_oppr_id_ttp_by_vendor_capacity_rpt;
 
-- Audit trial id
 INSERT
   INTO T940_AUDIT_TRAIL
    (
        C940_AUDIT_TRAIL_ID, C940_TABLE_NAME, C940_COLUMN_NAME
      , C940_CREATED_BY, C940_CREATED_DATE
    )
    VALUES
    (
        '315', 'T4057_TTP_VENDOR_CAPACITY_SUMMARY_DTLS', 'C4057_OVERRIDE_SPLIT_PER'
      , '706322', CURRENT_DATE
    ) ;

