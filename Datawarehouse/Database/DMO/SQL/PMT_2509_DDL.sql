--t4057_ddl
ALTER TABLE t4057_ttp_vendor_capacity_summary_dtls ADD (
    c301_override_vendor_id          NUMBER,
    c301_override_vendor_name        VARCHAR2(255),
    c4057_other_override_spilt_per   NUMBER(4, 2),
    c4057_other_override_vendor_fl   VARCHAR(1),
    c4057_edit_row_lock_fl           VARCHAR(1),
    c4057_old_po_qty                 NUMBER,
    c4057_regen_po_fl                VARCHAR(1)
);