/* Formatted on 2008/12/19 17:36 (Formatter Plus v4.8.0) */
CREATE OR REPLACE VIEW v_in_list
AS
	SELECT	   TRIM (SUBSTR (txt
						   , INSTR (txt, ',', 1, LEVEL) + 1
						   , INSTR (txt, ',', 1, LEVEL + 1) - INSTR (txt, ',', 1, LEVEL) - 1
							)
					) AS token
		  FROM (SELECT ',' || SYS_CONTEXT ('gop_inlist_ctx', 'txt', 4000) || ',' txt
				  FROM DUAL)
	CONNECT BY LEVEL <=
					 LENGTH (SYS_CONTEXT ('gop_inlist_ctx', 'txt', 4000))
				   - LENGTH (REPLACE (SYS_CONTEXT ('gop_inlist_ctx', 'txt', 4000), ',', ''))
				   + 1
/
CREATE OR REPLACE VIEW v_double_in_list
AS
	SELECT	   TRIM (SUBSTR (txt
						   , INSTR (txt, SYS_CONTEXT ('gop_inlisttoken_ctx', 'txt', 1), 1, LEVEL) + 1
						   ,   INSTR (txt, SYS_CONTEXT ('gop_inlisttoken_ctx', 'txt', 1), 1, LEVEL + 1)
							 - INSTR (txt, SYS_CONTEXT ('gop_inlisttoken_ctx', 'txt', 1), 1, LEVEL)
							 - 1
							)
					) AS token
			 , TRIM (SUBSTR (txtii
						   , INSTR (txtii, SYS_CONTEXT ('gop_inlisttoken_ctx', 'txt', 1), 1, LEVEL) + 1
						   ,   INSTR (txtii, SYS_CONTEXT ('gop_inlisttoken_ctx', 'txt', 1), 1, LEVEL + 1)
							 - INSTR (txtii, SYS_CONTEXT ('gop_inlisttoken_ctx', 'txt', 1), 1, LEVEL)
							 - 1
							)
					) AS tokenii
		  FROM (SELECT	  SYS_CONTEXT ('gop_inlisttoken_ctx', 'txt', 1)
					   || SYS_CONTEXT ('gop_inlisti_ctx', 'txt', 4000)
					   || SYS_CONTEXT ('gop_inlisttoken_ctx', 'txt', 1) txt
					 ,	  SYS_CONTEXT ('gop_inlisttoken_ctx', 'txt', 1)
					   || SYS_CONTEXT ('gop_inlistii_ctx', 'txt', 4000)
					   || SYS_CONTEXT ('gop_inlisttoken_ctx', 'txt', 1) txtii
				  FROM DUAL)
	CONNECT BY LEVEL <=
					 LENGTH (SYS_CONTEXT ('gop_inlisti_ctx', 'txt', 4000))
				   - LENGTH (REPLACE (SYS_CONTEXT ('gop_inlisti_ctx', 'txt', 4000)
									, SYS_CONTEXT ('gop_inlisttoken_ctx', 'txt', 1)
									, ''
									 )
							)
				   + 1
/
