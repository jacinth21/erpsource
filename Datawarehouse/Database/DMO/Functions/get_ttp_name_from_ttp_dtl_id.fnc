
  CREATE OR REPLACE FUNCTION get_ttp_name_from_ttp_dtl_id (
        p_ttp_detail_id t4052_ttp_detail.C4052_ttp_detail_id%TYPE)
        
    RETURN VARCHAR2
IS
    /*  Description     : This Function used to fetch the TTP name based on TTP details id
    Parameters   : p_ttp_detail_id
    */
    v_ttp_name t4050_ttp.c4050_ttp_nm%TYPE;
BEGIN
	--
    BEGIN
	    --
         SELECT t4050.c4050_ttp_nm
           INTO v_ttp_name
           FROM t4052_ttp_detail t4052, t4050_ttp t4050
          WHERE t4050.c4050_ttp_id        = t4052.c4050_ttp_id
            AND t4052.c4052_ttp_detail_id = p_ttp_detail_id
            AND t4050.c4050_void_fl      IS NULL
            AND t4052.c4052_void_fl      IS NULL;
          --  
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_ttp_name := NULL;
    END;
    --
    RETURN v_ttp_name;
    --
END get_ttp_name_from_ttp_dtl_id;
/
