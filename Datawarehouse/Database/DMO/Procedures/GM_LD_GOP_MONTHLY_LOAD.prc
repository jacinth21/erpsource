create or replace procedure GM_LD_GOP_MONTHLY_LOAD
AS

v_demandsheetid  VARCHAR2(20);
v_month VARCHAR2(2);
v_year VARCHAR2(4);
job_id number;
X NUMBER;
v_ttp_detail_id t4052_ttp_detail.c4052_ttp_detail_id%TYPE;
  
BEGIN


SELECT 	to_char(sysdate,'MM'),to_char(sysdate,'YYYY')
INTO 	v_month, v_year 
FROM 	DUAL;

SELECT job INTO job_id
		FROM user_jobs where what='GLOBUS_DM_OPERATION.GM_PKG_OPPR_LD_DEMAND.GM_OP_EXEC_JOB_DETAIL;' ;
		DBMS_JOB.remove(job_id); 	
		commit;
		
		
	
	BEGIN
	  SYS.DBMS_JOB.SUBMIT
		( job       => X 
		 ,what      => 'GLOBUS_DM_OPERATION.GM_PKG_OPPR_LD_DEMAND.GM_OP_EXEC_JOB_DETAIL;'
		 ,next_date => to_date(v_month||'/01/'||v_year||' 08:10:00','mm/dd/yyyy hh24:mi:ss')
		 ,interval  => 'SYSDATE+2/1440 '
		 ,no_parse  => TRUE
		);
	  SYS.DBMS_OUTPUT.PUT_LINE('Job Number is: ' || to_char(x));
	END;
	
	
	gm_pkg_oppr_ld_inventory.gm_op_ld_demand_main (20430);
	COMMIT;



    FOR someparent IN (
        SELECT x.c4020_demand_master_id, x.c4020_demand_nm FROM t4020_demand_master x
        WHERE x.C4020_PARENT_DEMAND_MASTER_ID IS NULL  
        and x.C4020_INACTIVE_FL IS NULL
	ORDER BY UPPER(x.c4020_demand_nm) )
    LOOP
          FOR someone IN (
                select x.* from t4020_demand_master x, t4015_global_sheet_mapping y  
                where x.C4020_PARENT_DEMAND_MASTER_ID = someparent.c4020_demand_master_id 
                and x.C901_DEMAND_TYPE IN ( 40021,40020,40022 )  
                and x.C4015_GLOBAL_SHEET_MAP_ID = y.C4015_GLOBAL_SHEET_MAP_ID
		and x.C4020_INACTIVE_FL IS NULL
		and x.c4020_void_fl IS NULL 
                and y.C901_ACCESS_TYPE = 102623
            )
          LOOP
            --DBMS_OUTPUT.PUT_LINE('DEMAND SHEET ID ' || someone.C4020_DEMAND_MASTER_ID ||
             --                    ', Last name = ' || someone.C4020_DEMAND_NM);
              v_demandsheetid := someone.C4020_DEMAND_MASTER_ID;                
              gm_pkg_oppr_ld_demand.gm_op_ld_demand_main(v_demandsheetid,'30301');     
              
          END LOOP;
          -- To Load the readonly sheet
          gm_pkg_oppr_ld_demand.GM_OP_EXEC_JOB_DETAIL ();
          gm_pkg_oppr_ld_summary.gm_op_ld_summary_main('',v_demandsheetid,'30301');
    END LOOP;      


	-- 1. Create a new entry into t4052_ttp_detail for Multi Part Spine
			
    		SELECT s4052_ttp_detail.NEXTVAL
			  INTO v_ttp_detail_id
			FROM DUAL;
			
INSERT INTO t4052_ttp_detail(c4052_ttp_detail_id,c4050_ttp_id,c4052_ttp_link_date,c901_status,c4052_created_by,c4052_created_date,c4052_forecast_period,c4052_load_date)
				     VALUES (v_ttp_detail_id,'1160',to_date(v_month||'/01/'||v_year,'MM/DD/YYYY'),50580,'839135',SYSDATE,4,TRUNC(sysdate,'month'));
				     
Insert into t4040_demand_sheet (C4040_DEMAND_SHEET_ID,C4020_DEMAND_MASTER_ID,C4040_DEMAND_PERIOD_DT,C4040_LOAD_DT,C4040_DEMAND_PERIOD,C901_STATUS,C4040_VOID_FL,C4040_APPROVED_BY,C4040_FORECAST_PERIOD,
C4040_APPROVED_DATE,C4040_LAST_UPDATED_BY,C4052_TTP_DETAIL_ID,C4040_LAST_UPDATED_DATE,C4040_PRIMARY_USER,C4040_REQUEST_PERIOD,C901_DEMAND_TYPE,C250_INVENTORY_LOCK_ID,C901_LEVEL_ID,
C901_LEVEL_VALUE,
C901_ACCESS_TYPE,C4015_GLOBAL_SHEET_MAP_ID,C4016_GLOBAL_INVENTORY_MAP_ID)
values (s4040_demand_sheet.NEXTVAL,'22933',to_date(v_month||'/01/'||v_year,'MM/DD/YYYY'),sysdate,6,50550,null,'',12,'','',v_ttp_detail_id,'','303007',0,40023,(
select C250_INVENTORY_LOCK_ID from T250_INVENTORY_LOCK where c250_void_fl is null and C901_LOCK_TYPE=20430 and C4016_GLOBAL_INVENTORY_MAP_ID=1 and C250_LOCK_DATE=to_date(v_month||'/01/'||v_year,'MM/DD/YYYY')
),null,null,null,null,null);

gm_pkg_oppr_ld_multipart_ttp.gm_op_ld_missing_part();
gm_pkg_oppr_ld_summary.gm_op_ld_summary_main(v_ttp_detail_id,'','30301');
	-- 1. Create a new entry into t4052_ttp_detail for Multi Part Trauma

SELECT s4052_ttp_detail.NEXTVAL
			  INTO v_ttp_detail_id
			FROM DUAL;	
			
INSERT INTO t4052_ttp_detail(c4052_ttp_detail_id,c4050_ttp_id,c4052_ttp_link_date,c901_status,c4052_created_by,c4052_created_date,c4052_forecast_period,c4052_load_date)
				     VALUES (v_ttp_detail_id,'1259',to_date(v_month||'/01/'||v_year,'MM/DD/YYYY'),50580,'829135',SYSDATE,4,TRUNC(sysdate,'month'));	 	
				 		
				     
Insert into GLOBUS_DM_OPERATION.t4040_demand_sheet (C4040_DEMAND_SHEET_ID,C4020_DEMAND_MASTER_ID,C4040_DEMAND_PERIOD_DT,C4040_LOAD_DT,C4040_DEMAND_PERIOD,C901_STATUS,C4040_VOID_FL,C4040_APPROVED_BY
,C4040_FORECAST_PERIOD,
C4040_APPROVED_DATE,C4040_LAST_UPDATED_BY,C4052_TTP_DETAIL_ID,C4040_LAST_UPDATED_DATE,C4040_PRIMARY_USER,C4040_REQUEST_PERIOD,C901_DEMAND_TYPE,C250_INVENTORY_LOCK_ID,C901_LEVEL_ID,
C901_LEVEL_VALUE,C901_ACCESS_TYPE,C4015_GLOBAL_SHEET_MAP_ID,C4016_GLOBAL_INVENTORY_MAP_ID)
values (s4040_demand_sheet.NEXTVAL,'33694',to_date(v_month||'/01/'||v_year,'MM/DD/YYYY'),sysdate,6,50550,null,'',12,'','',v_ttp_detail_id,'','839135',0,40023,(
select C250_INVENTORY_LOCK_ID from T250_INVENTORY_LOCK where c250_void_fl is null and C901_LOCK_TYPE=20430 and C4016_GLOBAL_INVENTORY_MAP_ID=1 and C250_LOCK_DATE=to_date(v_month||'/01/'||v_year,'MM/DD/YYYY')
),null,'26240179',null,null,null);


gm_pkg_oppr_ld_multipart_ttp.gm_op_ld_missing_part_trauma();
gm_pkg_oppr_ld_summary.gm_op_ld_summary_main(v_ttp_detail_id,'','30301');

commit;
END GM_LD_GOP_MONTHLY_LOAD;
/