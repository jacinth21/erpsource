# -*- coding: utf-8 -*-
"""
Created on Mon Oct 21 14:23:00 2019

@author: MAzarudeen
"""

import cx_Oracle
import codecs
import pandas as pd
from datetime import datetime
from datetime import timedelta
import requests
import time
import random


def data_generation(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x):
    Orders_Pending_control = a
    Orders_Pending_Shipping = b
    Orders_Pending_Shipping_In_Progress=c
    Orders_Ready_shipping= d
    Orders_Same_Day = e
    Orders_Same_Day_Pending = f
    Consignment_Pending_control = g
    Consignment_Ready_shipping = h
    Consignment_Pending_Shipping = i
    Consignment_Pending_Shipping_In_Progress =j
    Consignment_Same_Day = k
    Consignment_Same_Day_Pending = l
    Loaner_Pending_control = m
    Loaner_Ready_shipping = n
    Loaner_Pending_Shipping = o
    Loaner_Pending_Shipping_In_Progress = p
    Loaner_Same_Day = q
    Loaner_Same_Day_Pending = r
    Loaner_Replenishment_Pending_control = s
    Loaner_Replenishment_Ready_shipping = t
    Loaner_Replenishment_Pending_Shipping = u
    Loaner_Replenishment_Pending_Shipping_In_Progress =v
    Loaner_Replenishment_Same_Day = w
    Loaner_Replenishment_Same_Day_Pending = x
    DateTime = datetime.now().isoformat()
   
    return [Orders_Pending_control,Orders_Pending_Shipping,Orders_Pending_Shipping_In_Progress,Orders_Ready_shipping,Orders_Same_Day,Orders_Same_Day_Pending,
            Consignment_Pending_control,Consignment_Ready_shipping,Consignment_Pending_Shipping,Consignment_Pending_Shipping_In_Progress,Consignment_Same_Day,Consignment_Same_Day_Pending,
            Loaner_Pending_control,Loaner_Ready_shipping,Loaner_Pending_Shipping,Loaner_Pending_Shipping_In_Progress,Loaner_Same_Day,Loaner_Same_Day_Pending,
            Loaner_Replenishment_Pending_control,Loaner_Replenishment_Ready_shipping,Loaner_Replenishment_Pending_Shipping,Loaner_Replenishment_Pending_Shipping_In_Progress,Loaner_Replenishment_Same_Day,Loaner_Replenishment_Same_Day_Pending,DateTime]
            


if __name__ == '__main__':

#Connection to DB
    dbConnection=codecs.open('C:/com/txtfile/DBConnection.txt','r','utf8')
    content=dbConnection.read()
    conn=cx_Oracle.connect(content)
    cursor =conn.cursor()
#To get the REST_API_URL of the report from rules table
    Qry = cursor.execute("Select globus_app.get_rule_value('SHIP_SUMMARY','PBI_SHIP_SUMMARY') from dual")
    rows = cursor.fetchone()
    print(rows[0])
   
    REST_API_URL = rows[0]
    while True:
       
        data_raw = []

        p_out_dtls= cursor.var(cx_Oracle.CURSOR)
    
        cursor.callproc('globus_app.gm_fch_ship_summay_dtls',[p_out_dtls])
        
        out_dtls = p_out_dtls.getvalue()

        for row in out_dtls:
            print("Connection Success ",str(row[0]))
            sales_data = data_generation(str(row[0]),str(row[1]),str(row[2]),str(row[3]),str(row[4]),str(row[5]),str(row[6]),str(row[7]),str(row[8]),str(row[9]),str(row[10]),str(row[11]),str(row[12]),str(row[13]),str(row[14]),str(row[15]),str(row[16]),str(row[17]),str(row[18]),str(row[19]),str(row[20]),str(row[21]),str(row[22]),str(row[23]))
            data_raw.append(sales_data)
            print("Raw data - ", data_raw)


       
         # set the header record
        HEADER = ["Orders_Pending_control","Orders_Pending_Shipping","Orders_Pending_Shipping_In_Progress","Orders_Ready_shipping","Orders_Same_Day","Orders_Same_Day_Pending",
                  "Consignment_Pending_control","Consignment_Ready_shipping","Consignment_Pending_Shipping","Consignment_Pending_Shipping_In_Progress","Consignment_Same_Day","Consignment_Same_Day_Pending",
                  "Loaner_Pending_control","Loaner_Ready_shipping","Loaner_Pending_Shipping","Loaner_Pending_Shipping_In_Progress","Loaner_Same_Day","Loaner_Same_Day_Pending",
                  "Loaner_Replenishment_Pending_control","Loaner_Replenishment_Ready_shipping","Loaner_Replenishment_Pending_Shipping","Loaner_Replenishment_Pending_Shipping_In_Progress","Loaner_Replenishment_Same_Day","Loaner_Replenishment_Same_Day_Pending","DateTime"]            
        data_df = pd.DataFrame(data_raw, columns=HEADER)
        data_json = bytes(data_df.to_json(orient='records'), encoding='utf-8')
        print("JSON dataset", data_json)

        # Post the data on the Power BI API
        req = requests.post(REST_API_URL, data_json)

        print("Data posted in Power BI API")
        time.sleep(60)
    cursor.close()
    conn.close()
    
    
        
