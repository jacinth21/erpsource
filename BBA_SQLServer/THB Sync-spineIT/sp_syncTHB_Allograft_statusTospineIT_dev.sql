USE [THB]
GO
/****** Object:  StoredProcedure [access].[sp_syncTHB_Allograft_statusTospineIT_dev]    Script Date: 6/1/2020 6:49:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Gomathi>
-- Create date: <05/28/2020>
-- Description:	<Job to sync THB WIP status data to spineIT>
-- =============================================
ALTER PROCEDURE [access].[sp_syncTHB_Allograft_statusTospineIT_dev]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--DECLARE THE VARIABLES FOR HOLDING DATA.

DECLARE @Product_Num	VARCHAR(50), @total_count INT, @status INT ,@RN INT
,@CreatedBy VARCHAR(300)
,@CreatedDate Date, @THB_MASTER_LOAD_ID INT;

-- DECLARE THE VARIABLES FOR Exception Handling.			
   DECLARE @ErrorMessage VARCHAR(8000), @ErrorLine INT, @ErrorProc VARCHAR(200), 
           @ErrorNum INT, @ErrorSeverity INT, @ErrorState INT

		Set @CreatedDate= GETDATE();

    -- Insert statements for procedure here
BEGIN TRY

 delete from GMITESTDB..[GLOBUS_APP].[T2562_THB_WIP_INVENTORY_STATUS];
 DECLARE FETCHTHBALLOGRAFTS CURSOR READ_ONLY
 
   
 FOR 
select Released.ProductNo, sum (Released.Total_count) Count,Released.CurrentStatusId

from
(
SELECT
      	 products.ProductNo ProductNo
		 ,COUNT(products.ProductNo) Total_count
		 ,case (allograft.CurrentStatusId)
		  WHEN '4'  THEN '109760'  -- Pending-Lyo
		  WHEN '5'  THEN '109760'  -- Pending-Irr
		  WHEN '29' THEN '109761'   -- Released to Rad
		  WHEN '9'  THEN '109762'  -- QA Hold
		  WHEN '6'  THEN '109780'  -- Pending-Release
		  WHEN '7'  THEN '109781'  -- Pending Processing
		  WHEN '11' THEN '109782'  -- Rework
		  WHEN '12' THEN '109783'  -- Reworked
		  WHEN '13' THEN '109784'  -- WIP
		    END CurrentStatusId
  FROM [THB].[dbo].[tblAllograftIDNoTracking] allograft, [thb].[dbo].[tblProducts] products
  where allograft.ProductId=products.Id
  AND allograft.CurrentStatusId in (4,5,29,9,6,7,11,12,13)
 GROUP BY products.ProductNo, allograft.CurrentStatusId) Released
 GROUP BY Released.ProductNo,Released.CurrentStatusId;

 OPEN FETCHTHBALLOGRAFTS
	    FETCH NEXT FROM FETCHTHBALLOGRAFTS INTO
		@Product_Num, @total_count, @status
		   
--LOOP UNTIL RECORDS ARE AVAILABLE.
	  WHILE @@FETCH_STATUS = 0
   
   BEGIN

  		 SELECT   @THB_MASTER_LOAD_ID=(c2562_thb_wip_inventory_status_id) FROM GMITESTDB..[GLOBUS_APP].[T2562_THB_WIP_INVENTORY_STATUS] THB_LOAD   
			WHERE    C205_PART_NUMBER=@Product_Num AND C901_STATUS=@status;

	   update GMITESTDB..[GLOBUS_APP].[T2562_THB_WIP_INVENTORY_STATUS]
          set C2562_QTY=@total_count
        where c2562_thb_wip_inventory_status_id=@THB_MASTER_LOAD_ID;

   IF @@ROWCOUNT = 0  

   INSERT INTO GMITESTDB..[GLOBUS_APP].[T2562_THB_WIP_INVENTORY_STATUS]
		  (C205_PART_NUMBER,C2562_QTY,C901_STATUS,C2562_CREATED_DATE, c2562_created_by,C1900_COMPANY_ID)
		  values (@Product_Num, @total_count,@status,@CreatedDate,30301,'1001') 
    	
-- Outer loop starts [Fetch next in the loop]
 FETCH NEXT FROM  FETCHTHBALLOGRAFTS
	    INTO @Product_Num, @total_count, @status;
END
 CLOSE FETCHTHBALLOGRAFTS
DEALLOCATE FETCHTHBALLOGRAFTS

END TRY

-- Exception Handling
	BEGIN CATCH
		IF @@TRANCOUNT <> 0
		BEGIN
		print ' Transaction ROLLBACK for Load Number';	
			ROLLBACK TRANSACTION
			 CLOSE FETCHTHBALLOGRAFTS
               DEALLOCATE FETCHTHBALLOGRAFTS
			 CLOSE FETCHTHBALLOGRAFTS
               DEALLOCATE FETCHTHBALLOGRAFTS
		END
			
		SELECT @ErrorNum = ERROR_NUMBER(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE(), @ErrorProc = ERROR_PROCEDURE(),	@ErrorLine = ERROR_LINE(), @ErrorMessage = ERROR_MESSAGE() + ' Procedure:%s'
		RAISERROR (@ErrorMessage, @ErrorSeverity, 7, @ErrorProc)-- WITH LOG
	END CATCH;
	
END
