USE [THB]
GO
/****** Object:  StoredProcedure [dbo].[sp_syncTHB_Weekly_capacity_statusTospineIT_dev]    Script Date: 6/1/2020 1:53:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- 
-- =============================================
-- Author:		<Gomathi>
-- Create date: <05/28/2020>
-- Description:	<Job to sync THB WIP status data to spineIT>
-- =============================================
ALTER PROCEDURE [dbo].[sp_syncTHB_Weekly_capacity_statusTospineIT_dev]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--DECLARE THE VARIABLES FOR HOLDING DATA.

DECLARE @Product_Num	VARCHAR(50), @total_count INT, @status INT ,@RN INT
,@CreatedBy VARCHAR(300)
,@CreatedDate Date
,@DateReleased datetime2
,@Starting_year Date;

-- DECLARE THE VARIABLES FOR Exception Handling.			
   DECLARE @ErrorMessage VARCHAR(8000), @ErrorLine INT, @ErrorProc VARCHAR(200), 
           @ErrorNum INT, @ErrorSeverity INT, @ErrorState INT

		Set @CreatedDate= GETDATE();

		Set @Starting_year=DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0)

BEGIN TRY

delete from GMITESTDB..[GLOBUS_APP].[T2563_THB_WEEKLY_CAPACITY];

 DECLARE FETCHTHBWEEKLYCAPACITY CURSOR READ_ONLY
 
 FOR 

  -- Yearly Weekly Product count Status
	 SELECT products.ProductNo, COUNT(*) AS Count , DATEADD(DAY, -DATEDIFF(DAY, 0, [DateReleased]) % 7, [DateReleased]) AS [Week of]
  
	FROM [THB].[dbo].[tblAllograftIDNoTracking] , [thb].[dbo].[tblProducts] products
	where CurrentStatusId in (30)
	and [THB].[dbo].[tblAllograftIDNoTracking].[DateReleased]>=@Starting_year
	and [THB].[dbo].[tblAllograftIDNoTracking].ProductId=products.Id
	GROUP BY products.ProductNo,DATEADD(DAY, -DATEDIFF(DAY, 0, [DateReleased]) % 7, [DateReleased]);


 OPEN FETCHTHBWEEKLYCAPACITY
	    FETCH NEXT FROM FETCHTHBWEEKLYCAPACITY INTO
		@Product_Num, @total_count, @DateReleased

--LOOP UNTIL RECORDS ARE AVAILABLE.
	  WHILE @@FETCH_STATUS = 0
   
   BEGIN
   
   INSERT INTO GMITESTDB..[GLOBUS_APP].[T2563_THB_WEEKLY_CAPACITY]
		  (C205_PART_NUMBER,C2563_RECEIVED_COUNT,C2563_RECEIVED_WEEK_OF,C2563_CREATED_DATE,c2563_created_by,C1900_COMPANY_ID)
		  values (@Product_Num, @total_count,@DateReleased,@CreatedDate,30301,'1001') 
    	
-- Outer loop starts [Fetch next in the loop]
 FETCH NEXT FROM FETCHTHBWEEKLYCAPACITY INTO
		@Product_Num, @total_count, @DateReleased
END
 CLOSE FETCHTHBWEEKLYCAPACITY
DEALLOCATE FETCHTHBWEEKLYCAPACITY

END TRY

-- Exception Handling
	BEGIN CATCH
		IF @@TRANCOUNT <> 0
		BEGIN
		print ' Transaction ROLLBACK for Load Number';	
			ROLLBACK TRANSACTION
			 CLOSE FETCHTHBWEEKLYCAPACITY
               DEALLOCATE FETCHTHBWEEKLYCAPACITY
			 CLOSE FETCHTHBWEEKLYCAPACITY
               DEALLOCATE FETCHTHBWEEKLYCAPACITY
		END
			
		SELECT @ErrorNum = ERROR_NUMBER(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE(), @ErrorProc = ERROR_PROCEDURE(),	@ErrorLine = ERROR_LINE(), @ErrorMessage = ERROR_MESSAGE() + ' Procedure:%s'
		RAISERROR (@ErrorMessage, @ErrorSeverity, 7, @ErrorProc)-- WITH LOG
	END CATCH;
	
END
