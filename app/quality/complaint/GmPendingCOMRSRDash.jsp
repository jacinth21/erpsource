<%
/****************************************************************************************************
 * File		 		: GmPendingCOMRSRDash.jsp
 * Desc		 		: This JSP Shows the pending COM/RSR details at a glance
 * Version	 		: 1.0
 * author			: HReddi
******************************************************************************************************/
%>
<!-- quality\complaint\GmPendingCOMRSRDash.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtPendingCOMRSRDash" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtPendingCOMRSRDash:setLocale value="<%=strLocale%>"/>
<fmtPendingCOMRSRDash:setBundle basename="properties.labels.quality.complaint.GmPendingCOMRSRDash"/>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%> 
<bean:define id="xmlGridData" name="frmCOMRSRReport" property="xmlGridData" type="java.lang.String"> </bean:define>
<%

	String strQualCmplJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_QUALITY_COMPLAINT");
	String strTodaysDate = (String)session.getAttribute("strSessTodaysDate")==null?"":(String)session.getAttribute("strSessTodaysDate");
	String strApplnDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Pending COM/RSR Dashboard  </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strQualCmplJsPath%>/GmPendingCOMRSRDash.js"></script> 
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script type="text/javascript">
var objGridData = '<%=xmlGridData%>';
</script>
<style>
 	.exportlinks{display: inline-block;}
 	.submitPendingCOMRSR{display: inline-block; margin-left:15px;}
 	table.hdr tr:nth-child(3) td:nth-child(11){text-align: center !important;}
 	table.hdr tr:nth-child(3) td:nth-child(11) .hdrcell {padding-left: 0;}
</style>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<html:form  action="/gmCOMRSRReportAction.do?method=pendingCOMRSRDash">
<html:hidden property="strOpt" name="frmCOMRSRReport"/>
<html:hidden property="raID" name="frmCOMRSRReport"/>
<html:hidden property="hDisplayNm" name="frmCOMRSRReport"/>
<html:hidden property="hRedirectURL" name="frmCOMRSRReport"/>

<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" colspan="2" class="RightDashBoardHeader" ><fmtPendingCOMRSRDash:message key="LBL_PENDING_COM_RSR_DASHBOARD"/></td>
			<td  class="RightDashBoardHeader" align="right" colspan=""></td>
		    <td align="right" class=RightDashBoardHeader>
				<fmtPendingCOMRSRDash:message key="IMG_ALT_HELP" var="varHelp" /><img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("PEND_COMRSR_DASBOARD")%>');" />
			</td> 
		</tr>			
			<%if( xmlGridData.indexOf("cell") != -1){%>
				<tr>
					<td colspan="8"><div  id="pendcomrsrreport" style="" height="500px" width="1099px"></div></td>
				</tr>
			<%}else{%>
				<tr><td colspan="8" align="center" class="RightText"><fmtPendingCOMRSRDash:message key="LBL_NO_DATA"/></td></tr>
			<%} %>	
		<%if( xmlGridData.indexOf("cell") != -1) {%>
		<tr>
			<td colspan="8" align="center">
			    <div class='exportlinks'><fmtPendingCOMRSRDash:message key="DIV_EXPORT_OPT"/>: <img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="fnDownloadXLS();"> <fmtPendingCOMRSRDash:message key="DIV_EXCEL"/></a></div>
			    <div class='submitPendingCOMRSR'><input type="button" name="Btn_Submit" style="vertical-align: middle;" class="Button" value="Ignore" accesskey="" onclick="fnIgnore();"></div>
			</td>
		</tr>
		<%} %>
	</table>	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

