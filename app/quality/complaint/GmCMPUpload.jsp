<%
/**********************************************************************************
 * File		 		: GmCMPUpload.jsp
 * Desc		 		: Upload COM/RSR Files
 * Version	 		: 1.0
 * author			: Velu
************************************************************************************/
%>
<!-- quality\complaint\GmCMPUpload.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtCMPUpload" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtCMPUpload:setLocale value="<%=strLocale%>"/>
<fmtCMPUpload:setBundle basename="properties.labels.quality.complaint.GmCMPUpload"/>

<%@ page import ="com.globus.common.beans.GmGridFormat"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<bean:define id="strRefType" name="frmUploadDetail" property="refType" type="java.lang.String"></bean:define>
<bean:define id="strXmlData" name="frmUploadDetail" property="strXmlData" type="java.lang.String"></bean:define>
<bean:define id="strRefID" name="frmUploadDetail" property="strRefID" type="java.lang.String"></bean:define>
<bean:define id="intRecSize" name="frmUploadDetail" property="intRecSize" type="java.lang.Integer"></bean:define>
<bean:define id="strOpt" name="frmUploadDetail" property="strOpt" type="java.lang.String"></bean:define>
<bean:define id="message" name="frmUploadDetail" property="message" type="java.lang.String"></bean:define>
<bean:define id="strUpdAccessFl" name="frmUploadDetail" property="strUpdAccessFl" type="java.lang.String"></bean:define>

<% 

	String strQualCmplJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_QUALITY_COMPLAINT");
    GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.quality.complaint.GmCMPUpload", strSessCompanyLocale);
	String strWikiTitle =  GmCommonClass.getWikiTitle("UPLOADCOMRSR");
	String strLableRefId = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_COM_RSR"));
	String strLogType = "";
	String strClassOdd = "";
	String strClass = "";
	String strHighlights = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_COM_RSR_HIGHLIGHTS"));
	strUpdAccessFl = !strUpdAccessFl.equals("Y")?"true":"";
	
	if(strRefType.equals("102790") || strRefType.equals("102791")){
		strClass = "Shade";
		if(strRefType.equals("102790"))	strLogType = "4000319";
		if(strRefType.equals("102791"))	strLogType = "4000320";
	}else if(strRefType.equals("102789")){
		strLableRefId  = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SCAR"));
		strLogType = "4000318";
		strClass = "Odd";
		strClassOdd = "Shade";
		strWikiTitle = GmCommonClass.getWikiTitle("UPLOADSCAR");
		strHighlights = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SCAR_HIGHLIGHTS"));
	}else if(strRefType.equals("102788")){
		strLableRefId  = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_CAPA"));
		strLogType = "4000317";
		strClass = "Shade";
		strWikiTitle = GmCommonClass.getWikiTitle("UPLOADCAPA");
		strHighlights = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_CAPA_HIGHLIGHTS"));
	}

%>

<HTML>
<HEAD>
<TITLE> Globus Medical: COM/RSR Upload </TITLE>


<script language="JavaScript" src="<%=strQualCmplJsPath%>/GmCMPUpload.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmProgress.css">
<script>
	var objGridData = '<%=strXmlData%>';
	var strOpt;
	var accessFl='<%=strUpdAccessFl%>';
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10"  onLoad="fnUploadOnPageLoad()">
<html:form action="/gmUploadAction.do" enctype="multipart/form-data">
<html:hidden property="strOpt"  name="frmUploadDetail"/>
<html:hidden property="refType" name="frmUploadDetail"/>
<html:hidden property="logType" name="frmUploadDetail" value="<%=strLogType%>"/>
 <input type="hidden" name="hDisplayNm">
 <input type="hidden" name="hRedirectURL">
	 
<input type="hidden" name="hSkipCmnCnl" value="Upload Screen">
<input type="hidden" name="hDisplayNm" value="/gmPricingStatusReport.do">
			<table border="0" bordercolor="red" width="100%" class="DtTable950" cellspacing="0" cellpadding="0">
				<tr><td class="Line" colspan="5"></td></tr>
				<%if(!strOpt.equals("Summary")){ %>
					<tr>
						<td height="25" class="RightDashBoardHeader"><%=strLableRefId%>&nbsp;<fmtCMPUpload:message key="LBL_FILE_UPLOAD"/></td>
						<td  height="25" class="RightDashBoardHeader">
							<fmtCMPUpload:message key="IMG_ALT_HELP" var = "varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
						</td>
					</tr>
					<% if(!message.equals("")){ %>			
							<tr>
								<td colspan="5" align="center" height="25">&nbsp;<font style="color: Blue;"><b><bean:write name="frmUploadDetail" property="message"/></b></font></td>
							</tr>
					<%}%>
				 	
					<tr><td><table border="0" width="100%" cellspacing="0" cellpadding="0"><tr>
						<td  class="RightTableCaption" height="35" align ="right" width="45%" ><%=strLableRefId%> #:&nbsp;<html:text property="strRefID"  maxlength="20" size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" />
						</td><td>&nbsp; <fmtCMPUpload:message key="BTN_LOAD" var="varLoad"/><gmjsp:button name="Btn_Submit" value="&nbsp;${varLoad}&nbsp;"  gmClass="button" buttonType="Load" onClick="fnUploadLoad();" /></td>
					</tr></table></td></tr>
					
				<%}else{ %><!-- Summary screen does not need Wiki Link -->
					<tr>
						<td height="25" class="RightDashBoardHeader">&nbsp;<fmtCMPUpload:message key="LBL_SUMMARY_SCREEN"/></td>
					</tr>
				<%} %>
				<%if(!strOpt.equals("") && !strOpt.equals("load")){ %>
				<tr><td class="Line" colspan="5"></td></tr>
				<tr bgcolor="#e4e6f2" class="ShadeRightTableCaption"><td colspan="5" height="25">
				<%if(!strOpt.equals("Summary")){ %>
					<a style="text-decoration:none" href="javascript:fnShowFilters('tabRecv');" tabindex="-1" class="RightText" style="text-decoration:none; color:black"><IMG id="tabRecvimg" border=0 src="<%=strImagePath%>/minus.gif">
				<%} %>
				<%=strHighlights%>
				<%if(!strOpt.equals("Summary")){ %>
					</a></td></tr>
				<%} %>
				 <tr><td colspan="5"> 
					<table border="0" width="100%" cellspacing="0" cellpadding="0" id="tabRecv" > 
				 <% if(strRefType.equals("102788")){ %> 
				 <tr>
					<td colspan="6">
					<jsp:include page="/gmCapaSetup.do?method=capaHighlights" flush="true">
					<jsp:param name="haction" value="CAPAHighlights" />
					<jsp:param name="strOpt" value="<%=strOpt%>" />
					<jsp:param name="strCapaId" value="<%=strRefID%>" />
					</jsp:include>
					</td>
				</tr> 
				 <%}else if(strRefType.equals("102789")){ %> 
				 <tr>
					<td colspan="6">
					<jsp:include page="/gmScarInitiation.do?method=scarHighlights" flush="true">
					<jsp:param name="strOpt" value="<%=strOpt%>" />
					<jsp:param name="scarId" value="<%=strRefID%>" />
					</jsp:include>
					</td>
				</tr> 
				 <%}else if(strRefType.equals("102790") || strRefType.equals("102791")){ %> 
				 <tr>
					<td colspan="6">
					<jsp:include page="/gmCOMRSRHighlights.do?method=comrsrHighlights" flush="true">
							<jsp:param name="strOpt" value="<%=strOpt%>" />
							<jsp:param name="comRsrID" value="<%=strRefID%>" />
					</jsp:include>
					</td>
				</tr> 
				 <%}%>  
			</table>
			</td>
			</tr>
			<%if(!strOpt.equals("Summary")){ %> <!-- Summary screen does not need upload option -->
				<tr class="<%=strClass%>">
					<td height="30" colspan="5" align="center" valign="middle">&nbsp;&nbsp;<b><fmtCMPUpload:message key="LBL_SELECT_FILE_TO_UPLOAD"/>:</b>
					&nbsp;<html:file property="file"  name="frmUploadDetail"/></td>
					<tr><td class="LLine" colspan="5"></td></tr>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr class="<%=strClassOdd%>"><td colspan="5" height="30" align="center"><fmtCMPUpload:message key="BTN_UPLOAD" var="varUpload"/><gmjsp:button controlId="Submit"  name="Submit" value="${varUpload}"  gmClass="button" buttonType="Save" onClick="fnValidateUpload();"/>	</td>

				</tr>
			<%}else{ %>
				<tr bgcolor="#e4e6f2" class="ShadeRightTableCaption">
					<td colspan="5" height="25">&nbsp;<fmtCMPUpload:message key="LBL_FILE_UPLOAD"/>:</td>
				</tr>	
			<%} %>
				<tr><td class="LLine" colspan="5"></td></tr>
				
				<%if(!strOpt.equals("Summary")){ %>
					<tr><td colspan="5" height="25" bgcolor="#EEEEEE" class="RightTableCaption"><fmtCMPUpload:message key="LBL_PREVIOUS_UPLOADS"/></td><%} %> 
					<%if(intRecSize > 0){ %> 
					<tr><td colspan="5"><div id="grpData" style="grid" height="150px" width="950">&nbsp;</div></td>
					<%}else{%>
				 	<tr><td height="25" colspan="5"><font color="blue" size="1">&nbsp;<fmtCMPUpload:message key="LBL_NO_DATA_FOUND"/></font></td></tr>
				<%}%>
					<%if(!strOpt.equals("Summary")){ %> <!-- Summary screnn does not need comments section -->
						<tr><td colspan="6" height="1" class="LLine"></td></tr>
						<tr>
							<td colspan="5" align="center" height="25">
	                		<jsp:include page="/common/GmIncludeLog.jsp" >
	                		<jsp:param name="FORMNAME" value="frmUploadDetail" />
							<jsp:param name="ALNAME" value="alLogReasons" />
							<jsp:param name="LogMode" value="Edit" />
							</jsp:include></td>
						</tr>
						<tr><td colspan="6" align="center" height="40" ><fmtCMPUpload:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button controlId="Btn_Submit" value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnUploadSubmit();" disabled="<%=strUpdAccessFl%>" /></td></tr>
					<%} %> 
				
				<%if(strOpt.equals("Summary")){ %> <!-- Close button for Summary screen -->
					<tr><td class="LLine" colspan="5"></td></tr>
					<tr height="30"><td align="center">
					<fmtCMPUpload:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" buttonType="Load" onClick="fnUploadClose();" />
					</td></tr>
				<%} %>
				<%}%>
			</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>