<%
/**********************************************************************************
 * File		 		: GmScarInitiation.jsp
 * Desc		 		: Scar Initiation
 * Version	 		: 1.0
 * author			: Manikandan
************************************************************************************/
%>
<!-- quality\complaint\GmEnggEvaluation.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtEnggEvaluation" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtEnggEvaluation:setLocale value="<%=strLocale%>"/>
<fmtEnggEvaluation:setBundle basename="properties.labels.quality.complaint.GmEnggEvaluation"/>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*"%>
<%@page import="java.util.Date"%>
<bean:define id="strCOMRSRID" name="frmEnggEvaluation" property="incidentId" type="java.lang.String"></bean:define>
<bean:define id="alChooseAction" name="frmEnggEvaluation" property="alChooseAction" type="java.util.ArrayList"> </bean:define>
<bean:define id="intResultSize" name="frmEnggEvaluation" property="intSize" type="java.lang.Integer"></bean:define>
<bean:define id="message" name="frmEnggEvaluation" property="message" type="java.lang.String"></bean:define>
<bean:define id="strOpt" name="frmEnggEvaluation" property="strOpt" type="java.lang.String"></bean:define>
<bean:define id="strRefType" name="frmEnggEvaluation" property="strRefType" type="java.lang.String"></bean:define>
<bean:define id="status" name="frmEnggEvaluation" property="status" type="java.lang.String"></bean:define>
<% 


	String strQualCmplJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_QUALITY_COMPLAINT");
	String strWikiTitle = GmCommonClass.getWikiTitle("ENGINEERING_EVALUATION");
    ArrayList alRemoveCodeIDs = new ArrayList();
    ArrayList alRemoveIDs = new ArrayList();
    alRemoveCodeIDs.add("102844");
    alRemoveCodeIDs.add("102847");
    alRemoveIDs.add("102848");
    alChooseAction = GmCommonClass.parseNullArrayList(GmCommonClass.toRemoveCodeIdsFromArrayList(alRemoveCodeIDs,alChooseAction));
    alChooseAction = GmCommonClass.parseNullArrayList(GmCommonClass.toRemoveCodeIdsFromArrayList(alRemoveIDs,alChooseAction)); 
    String strCollapseImage = "/images/minus.gif";
    String strApplnDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
 %>
<HTML>
<HEAD>
<TITLE> Globus Medical: Engg Evaluation </TITLE>
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel = "stylesheet" type = "text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strQualCmplJsPath%>/GmEnggEvaluation.js"></script>
<script type="text/javascript">

var hCOMRSRID  = '<%=strCOMRSRID%>';
var confirmMsg = false;
var strRefType = '<%=strRefType%>';
var format = '<%=strApplnDateFmt%>';

</script>
</HEAD>
<BODY leftmargin="20" topmargin="10"">
<html:form action="/gmEnggEvaluation.do?">
<html:hidden property="strOpt" name="frmEnggEvaluation"  /> 

<!-- Custom tag lib code modified for JBOSS migration changes -->
<!-- Struts tag lib code modified for JBOSS migration changes -->
<table border="0" bordercolor="lightblue" class="DtTable1000" cellspacing="0" cellpadding="0" >
<tr>
		<td width="1" class="Line"></td>
	</tr>
	<tr>
		<td>
			<table border="0" bordercolor="lightblue" width="100%" cellspacing="0" cellpadding="0">
				<tr><td class="Line" colspan="4"></td></tr>
				<tr>
					<td colspan="3" height="25" class="RightDashBoardHeader">&nbsp;<fmtEnggEvaluation:message key="LBL_COM_RSR_ENGINEERING_EVALUATIONS"/></td>
					<td  height="25" class="RightDashBoardHeader">
						<fmtEnggEvaluation:message key="IMG_ALT_HELP" var="varHelp" /><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>
				</tr>
			</table>
			
			<table border="0" bordercolor="lightblue" width="100%" cellspacing="0" cellpadding="0">
				<tr><td class="Line" colspan="4"></td></tr>
				
						<% if(!message.equals("")){ %>			
							<tr>
								<td colspan="4" align="center" height="25">&nbsp;<font style="color: Blue;"><b><bean:write name="frmEnggEvaluation" property="message"/></b></font></td>
							</tr>
						<%}%>
				<tr>
					<td class="RightTableCaption" height="25" align ="Right" width="184"><fmtEnggEvaluation:message key="LBL_COM_RSR"/>:</td>
					<td width="235">&nbsp;<html:text property="incidentId"  maxlength="20" size="30" styleClass="InputArea" style="text-transform:uppercase;"
						onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
					</td>
					<td colspan="2" align="left" height="30">&nbsp;
				        <fmtEnggEvaluation:message key="BTN_LOAD" var="varLoad"/><gmjsp:button name="Btn_Submit" value="${varLoad}" buttonType="Load" gmClass="button" onClick="fnonLoad();" />
                    </td>
				</tr>
			</table>
			<table border="0" bordercolor="lightblue" width="100%" cellspacing="0" cellpadding="0">
				<tr bgcolor="#e4e6f2">
				<td height="25" colspan="5" class="ShadeRightTableCaption" >&nbsp;<a href="javascript:fnShowFilters('tabQaComHighlights');" tabindex="-1" style="text-decoration:none; color:black">
					<IMG id="tabQaComHighlightsimg" border=0 src="<%=strCollapseImage%>">&nbsp;<fmtEnggEvaluation:message key="LBL_COM_RSR_HIGHLIGHTS"/>:</a></td>
			</tr>
			</table> 
				<tr id="tabQaComHighlights" style="display:block;" >
				<td>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
			 		<tr>
						<td colspan="5">
							<jsp:include page="/gmCOMRSRHighlights.do?method=editIncidentInfo" flush="true">
								<jsp:param name="haction" value="CMPHighlights" />
								<jsp:param name="strOpt"  value="<%=strOpt%>" />
								<jsp:param name="comRsrID" value="<%=strCOMRSRID%>" />
							</jsp:include> 
						</td>
					</tr>  
				</table>
		</td>
		</tr>
		<tr>
		<td>
			<table border="0" bordercolor="lightblue" width="100%" cellspacing="0" cellpadding="0">
				<tr><td class="Line" colspan="5"></td></tr>
				<tr bgcolor="#e4e6f2">
					<td height="25" colspan="5" class="ShadeRightTableCaption" >&nbsp;<a href="javascript:fnShowFilters('tabRec');" tabindex="-1" style="text-decoration:none; color:black">
					<IMG id="tabRecimg" border=0 src="<%=strCollapseImage%>">&nbsp;<fmtEnggEvaluation:message key="LBL_GENERAL_DETAILS"/>:</a>
					</td>
				</tr>
			</table> 
			
			<tr style="display:block;" > 
			<td>
			<table border="0" width="100%" cellspacing="0" cellpadding="0" id="tabRec">
				<tr><td class="LLine" colspan="5"></td></tr>
				<tr class="shade" >
					<td class="RightTableCaption" align="right" height="25" width="180"><fmtEnggEvaluation:message key="LBL_DATE_RECEIVED_FROM_EVALUATOR"/>:</td>
					<td align ="left" colspan="3">&nbsp;<gmjsp:calendar SFFormName="frmEnggEvaluation" controlName="evalRecDt" gmClass="InputArea" 
									onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>
				<tr>
					<td class="RightTableCaption" align="right" height="25" width="180"><fmtEnggEvaluation:message key="LBL_DETAILED_EVALUATION_RESULTS_CONCLUSIONS"/>:</td>
					<td align ="left" colspan="3" height="20%">&nbsp;<html:textarea property="evalResult"  cols="130" style="height:70px; display:inline-block; width:99%;"
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>
				<tr class="shade">
					<td class="RightTableCaption" align="right" height="25" width="180"><fmtEnggEvaluation:message key="LBL_EVALUATOR"/>:</td>
					<td align ="left" colspan="3">&nbsp;<gmjsp:dropdown width="200" controlName="evalId" SFFormName="frmEnggEvaluation"
						SFSeletedValue="evalId" defaultValue= "[Choose One]" SFValue="alAssignedTo" codeId="ID" codeName="NAME" />
					</td>
				</tr>
				<tr>
				
					<td class="RightTableCaption" align="right" height="25" width="180"><fmtEnggEvaluation:message key="LBL_COM_RST_DESPOSITION"/>:</td>
					<%if(strRefType.equals("102790")){ %>
					<td align ="left" >&nbsp;<gmjsp:dropdown width="200" controlName="comRsrDispNum" SFFormName="frmEnggEvaluation"
						SFSeletedValue="comRsrDispNum" defaultValue= "[Choose One]" SFValue="alComRsrDis" codeId="CODEID" codeName="CODENM" />
					</td>
					<%} %><%else { %>
					<td align ="left" >&nbsp;<gmjsp:dropdown width="200" controlName="comRsrDispNum" SFFormName="frmEnggEvaluation"
						SFSeletedValue="comRsrDispNum" defaultValue= "[Choose One]"  SFValue="alComRsrDis" codeId="CODEID" codeName="CODENM" />
					</td>
					<%} %>
					<td class="RightTableCaption" align="right" height="25" width="180"><fmtEnggEvaluation:message key="LBL_PRODUCT_DISPOSITION"/>:</td>
					<td align ="left" >&nbsp;<gmjsp:dropdown width="250" controlName="pdtDispTypNum" SFFormName="frmEnggEvaluation"
						SFSeletedValue="pdtDispTypNum" defaultValue= "[Choose One]" SFValue="alProdDisp" codeId="CODEID" codeName="CODENM" />
					</td>
				</tr>
				<tr class="shade">
					<td class="RightTableCaption" align="right" height="25" width="180"><fmtEnggEvaluation:message key="LBL_CORRECTIVE_AND_PREVENTIVE_ACTION_COMMENTS"/>:</td>
					<td align ="left" colspan="3" height="20%">&nbsp;<html:textarea property="capaComments"  cols="130" style="height:70px; display:inline-block; width:99%;"
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
				<tr>
						<td colspan="5"> 
							<jsp:include page="/common/GmIncludeLog.jsp" >
							<jsp:param name="FORMNAME" value="frmEnggEvaluation" />
							<jsp:param name="ALNAME" value="alLogReasons" />
							<jsp:param name="LogMode" value="Edit" />
							</jsp:include>
						</td>
				</tr>
				<tr>
					
					<td colspan="5" align="center" height="30">&nbsp;
					<%if(!status.equals("102841")){ %> <!-- 102841 status is closed -->
				         <fmtEnggEvaluation:message key="BTN_RESET" var="varReset"/><gmjsp:button name="Btn_Reset" value="${varReset}" buttonType="Save" gmClass="button" style="width: 6em" onClick="fnReset();" />
				         <fmtEnggEvaluation:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button name="Btn_Submit" value="${varSubmit}" buttonType="Save" gmClass="button" style="width: 6em" onClick="fnonSubmit();" />
				     <% }%>
                    </td>
                    
				</tr>
				
			
				<% if(intResultSize >= 1){%>
				<tr class="shade">
					<td colspan="5" align="center" height="30">&nbsp;
				         <gmjsp:dropdown width="300" controlName="chooseAction" SFFormName="frmEnggEvaluation"
						SFSeletedValue="chooseAction" defaultValue= "[Choose One]" SFValue="alChooseAction" codeId="CODEID" codeName="CODENM" />&nbsp;
				        <fmtEnggEvaluation:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button name="Btn_Go" value="${varSubmit}" buttonType="Save" gmClass="button" style="width: 6em" onClick="fnGo();" />
                    </td>
				</tr>
			</table>
			<% }%>	
				
			
					
				</table>
		
			</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>