<%
/**********************************************************************************
 * File		 		: GmIncludeCMPHighlights.jsp
 * Desc		 		: Include in Upload for COMPLAINT SCREEN
 * Version	 		: 1.0
 * author			: Velu
************************************************************************************/
%>
<!-- quality\complaint\GmIncludeCMPHighlights.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtIncludeCMPHighlights" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtIncludeCMPHighlights:setLocale value="<%=strLocale%>"/>
<fmtIncludeCMPHighlights:setBundle basename="properties.labels.quality.complaint.GmIncludeCMPHighlights"/>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<bean:define id="complaint" name="frmIncidentInfo" property="complaint" type="java.lang.String"></bean:define>
<bean:define id="intSize" name="frmIncidentInfo" property="intSize" type="java.lang.Integer"></bean:define>
<bean:define id="detailDescEvent" name="frmIncidentInfo" property="detailDescEvent" type="java.lang.String"></bean:define>
<bean:define id="lotNum" name="frmIncidentInfo" property="lotNum" type="java.lang.String"></bean:define>
<%
String strQualCmplJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_QUALITY_COMPLAINT");
%>
<HTML>

<HEAD>
<TITLE> Globus Medical: CMP Highlights </TITLE>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strQualCmplJsPath%>/GmCMPUpload.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
</HEAD>
<BODY leftmargin="20" topmargin="10">
		<table border="0" bordercolor="red" width="100%" cellspacing="0" cellpadding="0">
		<%if(intSize > 0){ %>
			<tr class="Odd">
				<td class="RightTableCaption" align="right" width="20%"  ><fmtIncludeCMPHighlights:message key="LBL_COM_RSR"/>:</td>
				 <td height="25" width="30%">&nbsp;<bean:write name="frmIncidentInfo" property="comRsrID"/> </td>
				 <td class="RightTableCaption" align="right" width="20%" ><fmtIncludeCMPHighlights:message key="LBL_PROJECT"/>:</td>
				 <td width="35%">&nbsp;<bean:write name="frmIncidentInfo" property="projDesc"/> </td>  
			</tr>	
			<tr><td class="LLine" colspan="5"></td></tr> 
			<tr class="Shade">
				<td class="RightTableCaption" align="right"  ><fmtIncludeCMPHighlights:message key="LBL_PART"/>:</td>
				 <td height="25">&nbsp;<bean:write name="frmIncidentInfo" property="partNum"/> </td> 
				 <td class="RightTableCaption" align="right"  ><fmtIncludeCMPHighlights:message key="LBL_LOT"/>:</td>
				 <td >&nbsp;
				 <div id="showDiv" style="margin-top:0px;margin-left:7px;width: 305px; height: 25px;<%if(lotNum.length()>100){ %>overflow-y:scroll<%}%>">
				 <bean:write name="frmIncidentInfo" property="lotNum"/> </div></td> 
			</tr>	
			<tr><td class="LLine" colspan="5"></td></tr> 
			<tr class="Odd">
				<td class="RightTableCaption" align="right"  ><fmtIncludeCMPHighlights:message key="LBL_PART_DESCRIPTION"/>:</td>
				 <td height="25">&nbsp;<bean:write name="frmIncidentInfo" property="partDesc"/> </td>
				 <td class="RightTableCaption" align="right"  ><fmtIncludeCMPHighlights:message key="LBL_PART_RECEIVED_DATE"/>:</td>
				 <td >&nbsp;<bean:write name="frmIncidentInfo" property="partRecvDt"/> </td>  
			</tr>	
			<tr><td class="LLine" colspan="5"></td></tr> 
			<tr class="Shade">
				<td class="RightTableCaption" align="right"  ><fmtIncludeCMPHighlights:message key="LBL_COM_RSR_DESCRIPTION"/>:</td>
				 <td colspan="4">&nbsp;
				 <div id="showDiv" style="vertical-align: middle;margin-left:7px;width: 800px; height: 70px;<%if(detailDescEvent.length()>1000){ %>overflow-y:scroll<%}%>"> 
				 <bean:write name="frmIncidentInfo" property="detailDescEvent"/></div></td>
			</tr>
			<tr><td class="LLine" colspan="5"></td></tr>				
			<tr class="Odd">
		 		<td class="RightTableCaption" align="right" height="25" ><fmtIncludeCMPHighlights:message key="LBL_STATUS"/>:</td>
				<td>&nbsp;<bean:write name="frmIncidentInfo" property="statusNm"/> </td>
			</tr> 
			<tr><td class="LLine" colspan="5"></td></tr> 
		<%}else{%>
				 <tr><td height="25" colspan="5"><font color="blue" size="1">&nbsp;<fmtIncludeCMPHighlights:message key="LBL_NO_DATA"/>!</font></td></tr>
				<tr><td class="LLine" colspan="5"></td></tr>
		<%}%>
		</table>
</BODY>
</HTML>