
<%
/****************************************************************************************************
 * File		 		: GmCOMRSRReport.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: 
******************************************************************************************************/
%>
<!-- quality\complaint\GmCOMRSRReport.jsp -->
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtCOMRSRReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtCOMRSRReport:setLocale value="<%=strLocale%>"/>
<fmtCOMRSRReport:setBundle basename="properties.labels.quality.complaint.GmCOMRSRReport"/>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%> 
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<bean:define id="alCOMRSRList" name="frmCOMRSRReport" property="alCOMRSRList" type="java.util.ArrayList"></bean:define>
<bean:define id="alRepList" name="frmCOMRSRReport" property="alRepList" type="java.util.ArrayList"></bean:define>
<bean:define id="alEmpName" name="frmCOMRSRReport" property="alEmpName" type="java.util.ArrayList"></bean:define>
<bean:define id="alStatusList" name="frmCOMRSRReport" property="alStatusList" type="java.util.ArrayList"></bean:define>
<bean:define id="alElapsedDays" name="frmCOMRSRReport" property="alElapsedDays" type="java.util.ArrayList"></bean:define>
<bean:define id="alChooseActionList" name="frmCOMRSRReport" property="alChooseActionList" type="java.util.ArrayList"></bean:define>
<bean:define id="xmlGridData" name="frmCOMRSRReport" property="xmlGridData" type="java.lang.String"> </bean:define>
<bean:define id="intResultSize" name="frmCOMRSRReport" property="intResultSize" type="java.lang.Integer"> </bean:define>
<bean:define id="accessFl" name="frmCOMRSRReport" property="accessFl" type="java.lang.String"> </bean:define>
<bean:define id="complaint" name="frmCOMRSRReport" property="complaint" type="java.lang.String"></bean:define>
<%
String strQualCmplJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_QUALITY_COMPLAINT");
response.setContentType("text/html; charset=UTF-8");
response.setCharacterEncoding("UTF-8");
HashMap hcboVal = null;
GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.quality.complaint.GmCOMRSRReport", strSessCompanyLocale);
String strTodaysDate = (String)session.getAttribute("strSessTodaysDate")==null?"":(String)session.getAttribute("strSessTodaysDate");
String strApplnDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
String strTitle = "";
String strWikiTitle = "";
if(accessFl.equals("true")){ // to get whether modify CAPA or CAPA report
	strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_MODIFY_GENERATE_COM_RSR_REPORT"));
	strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("MODIFY_COMRSR_REPORT"));
}else{
	strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_COM_RSR_REPORT"));
	strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("COM/RSR_REPORT"));
}
String strDisableDropdown = "";
if(complaint.equals("0") || complaint.equals("")){
	strDisableDropdown = "disabled";
}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Modify & Report on COM/RSR  </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid_form.js "></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strQualCmplJsPath%>/GmCOMRSRReport.js"></script> 


<script type="text/javascript">
var objGridData = '<%=xmlGridData%>';
var todaysDate 	= '<%=strTodaysDate%>';
var format 		= '<%=strApplnDateFmt%>';
var accessFl = '<%=accessFl%>';
var RepLen = <%=alRepList.size()%>;
var EmpLen = <%=alEmpName.size()%>;
var lblelapseddays = '<fmtCOMRSRReport:message key="LBL_ELAPSED_DAYS"/>';
<%
hcboVal = new HashMap();
for (int i=0;i<alRepList.size();i++)
{
	hcboVal = (HashMap)alRepList.get(i);
%>
var alRepListArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
}
%>

<%
hcboVal = new HashMap();
for (int i=0;i<alEmpName.size();i++)
{
	hcboVal = (HashMap)alEmpName.get(i);
%>
var alEmpListArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
}
%>
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();" onkeypress="fnEnter();">
<html:form  action="/gmCOMRSRReportAction.do?method=listCOMRSRDetails">
<html:hidden property="strOpt" name="frmCOMRSRReport"/>
<html:hidden property="hCOMRSRID" name="frmCOMRSRReport"/>
<html:hidden property="hStatusId" name="frmCOMRSRReport"/>
<html:hidden property="hIssueTyp" name="frmCOMRSRReport"/>
<html:hidden property="hDisplayNm" name="frmCOMRSRReport"/>
<html:hidden property="hRedirectURL" name="frmCOMRSRReport"/>
<html:hidden property="accessFl" name="frmCOMRSRReport"/>

<!-- Custom tag lib code modified for JBOSS migration changes -->
<table border="0"  class="DtTable1100" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="5">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25" colspan="2" class="RightDashBoardHeader">&nbsp;<%=strTitle%></td>
						<td  class="RightDashBoardHeader" align="right" colspan=""></td>
				    	<td align="right" class=RightDashBoardHeader>
						<fmtCOMRSRReport:message key="IMG_ALT_HELP" var="varHelp" /><img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />&nbsp;
						</td>
					</tr>
				</table>
			</td> 
		</tr>
		<tr><td class="Line" height="1" colspan="7"></td></tr>	
		<tr class="evenshade">
			<td class="RightTableCaption" height="30" align="Right" width="20%">&nbsp;<fmtCOMRSRReport:message key="LBL_COM_OR_RSR"/>:</td>
			<td width="30%">&nbsp;<gmjsp:dropdown controlName="comorrsr"  SFFormName='frmCOMRSRReport' SFSeletedValue="comorrsr"  defaultValue= "[Choose One]"	
				SFValue="alCOMRSRList" codeId="CODEID" codeName="CODENM"/>
			</td>
			<td class="RightTableCaption" height="30" align="Right" width="15%">&nbsp;<fmtCOMRSRReport:message key="LBL_COMPLAINANT"/>:</td>
			<td width="45%" >&nbsp;<gmjsp:dropdown controlName="complaint" SFFormName="frmCOMRSRReport"
						SFSeletedValue="complaint" defaultValue= "[Choose One]" SFValue="alComplaint" codeId="CODEID" codeName="CODENM" onChange="javascript:fnComplaintNames();"/>
			</td>
		</tr>
		<tr><td class="LLine" height="1" colspan="7"></td></tr>	
		<tr class="oddshade">
		<td class="RightTableCaption" height="30" align="Right">&nbsp;<fmtCOMRSRReport:message key="LBL_COM_RSR"/>:</td>
			<td align="left" >&nbsp;<html:text maxlength="50" property="comRSR" size="20" style="text-transform:uppercase;"  
							styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>
			<td class="RightTableCaption" height="30" align="Right">&nbsp;<fmtCOMRSRReport:message key="LBL_COMPLAINANT_NAME"/>:</td>
			<% if(complaint.equals("102812")){%>
				<td >&nbsp;<gmjsp:dropdown disabled="<%=strDisableDropdown%>"   controlName="complaintname" SFFormName="frmCOMRSRReport" SFSeletedValue="complaintname" defaultValue= "[Choose One]" 
								SFValue="alRepList" codeId="ID" codeName="NAME" />
				</td>
			<%}else{%>
				<td >&nbsp;<gmjsp:dropdown disabled="<%=strDisableDropdown%>"   controlName="complaintname" SFFormName="frmCOMRSRReport" SFSeletedValue="complaintname" defaultValue= "[Choose One]" 
								SFValue="alEmpName" codeId="ID" codeName="NAME" /> 
				</td>	
			<%} %>	
		</tr>	
		<tr><td class="LLine" height="1" colspan="7"></td></tr>	
		<tr class="evenshade">
			<td class="RightTableCaption" height="30" align="Right" >&nbsp;<fmtCOMRSRReport:message key="LBL_PART"/>:</td>
			<td align="left" >&nbsp;<html:text maxlength="50" property="part" size="20"   
							styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>
			<td class="RightTableCaption" height="30" align="Right">&nbsp;<fmtCOMRSRReport:message key="LBL_RA"/>:</td>
			<td align="left" >&nbsp;<html:text maxlength="50" property="raID" size="20"   
							styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>					
		</tr>	
		<tr><td class="LLine" height="1" colspan="7"></td></tr>	
		<tr class="oddshade">
			<td class="RightTableCaption" height="30" align="Right" >&nbsp;<fmtCOMRSRReport:message key="LBL_LOT"/>:</td>
			<td align="left" >&nbsp;<html:text maxlength="50" property="lot" size="20"   
							styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>
			<td class="RightTableCaption" height="30" align="Right" >&nbsp;<fmtCOMRSRReport:message key="LBL_STATUS"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="status"  SFFormName='frmCOMRSRReport' SFSeletedValue="status"  defaultValue= "[Choose One]"	
				SFValue="alStatusList" codeId="CODEID" codeName="CODENM"/>					
		</tr>	
		<tr><td class="LLine" height="1" colspan="7"></td></tr>	
		<tr class="evenshade">
			<td class="RightTableCaption" height="30" align="right"><fmtCOMRSRReport:message key="LBL_DATE_RANGE"/>:</td>
			<td class="RightText">&nbsp;<fmtCOMRSRReport:message key="LBL_FROM"/>:&nbsp;<gmjsp:calendar SFFormName="frmCOMRSRReport" controlName="frmDate"  gmClass="InputArea" 
					onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;&nbsp;<fmtCOMRSRReport:message key="LBL_To"/>:&nbsp;<gmjsp:calendar SFFormName="frmCOMRSRReport" controlName="toDate"  gmClass="InputArea" 
					onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/></td>
			<td class="RightTableCaption" height="30" align="Right" >&nbsp;<fmtCOMRSRReport:message key="LBL_ELAPSED_DAYS"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="elapsedDays"  SFFormName='frmCOMRSRReport' SFSeletedValue="elapsedDays"  defaultValue= "[Choose One]"	
				SFValue="alElapsedDays" codeId="CODEID" codeName="CODENM"/>&nbsp;<html:text maxlength="2" property="elapsedVal" size="4"   
							styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />&nbsp;&nbsp;&nbsp;&nbsp;
				<fmtCOMRSRReport:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}" style="width: 4em;height: 23px" buttonType="Load" name="Btn_Load" gmClass="button" onClick="fnLoad();" /></td>					
		</tr>
		<tr><td class="LLine" height="1" colspan="7"></td></tr>	
			<%if( intResultSize > 0){%>
				<tr>
					<td colspan="7"><div  id="dataGridDiv" style="" height="500px"></div></td>
				</tr>						
			<%}else if(!intResultSize.equals(0)){%>
				<tr><td colspan="7" align="center" class="RightText"><fmtCOMRSRReport:message key="LBL_NO_DATA"/></td></tr>
			<%}else{%>
				<tr><td colspan="7" align="center" class="RightText"><fmtCOMRSRReport:message key="LBL_NO_DATA_AVAILABLE"/></td></tr>
			<%} %>		
			<%if( intResultSize > 0) {%>
		<tr><td height="1" colspan="7" class="LLine"></td></tr>
		<%if(accessFl.equals("true")){%> <!-- Only for Modify, Chooose action is needed -->	
		<tr>
			<td align="right" height="50"><fmtCOMRSRReport:message key="DIV_EXPORT_OPT"/>:<img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="fnExportExcel();"><fmtCOMRSRReport:message key="DIV_EXCEL"/></a>
			<td align="right" colspan="2">&nbsp;<gmjsp:dropdown controlName="chooseAction"  SFFormName='frmCOMRSRReport' SFSeletedValue="chooseAction"  defaultValue= "Choose One"	
						SFValue="alChooseActionList" codeId="CODEID" codeName="CODENM"/>
			</td>
					
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtCOMRSRReport:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" name="Btn_Submit" buttonType="Save" gmClass="button" onClick="fnSubmit();" tabindex="25"/></td>
		</tr>
		<%}else{ %>
		<tr>
			<td align="center" height="25" colspan="5" width="25%"><fmtCOMRSRReport:message key="DIV_EXPORT_OPT"/>:<img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="fnExportExcel();"><fmtCOMRSRReport:message key="DIV_EXCEL"/></a>
			</td>
		</tr>
		<%} %>
		<%} %>
</table>	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

