<%
/**********************************************************************************
 * File		 		: GmCOMRSRSummary.jsp
 * Desc		 		: Summary Screen of COM/RSR
 * Version	 		: 1.0
 * author			: Arajan
************************************************************************************/
%>
<!-- quality\complaint\GmCOMRSRSummary.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtCOMRSRSummary" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtCOMRSRSummary:setLocale value="<%=strLocale%>"/>
<fmtCOMRSRSummary:setBundle basename="properties.labels.quality.complaint.GmCOMRSRSummary"/>
<%@ page import ="com.globus.common.beans.GmGridFormat"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>

<bean:define id="detailDescEvent" name="frmCOMRSRProcess" property="detailDescEvent" type="java.lang.String"></bean:define>
<bean:define id="evalResult" name="frmCOMRSRProcess" property="evalResult" type="java.lang.String"></bean:define>
<bean:define id="capaComments" name="frmCOMRSRProcess" property="capaComments" type="java.lang.String"></bean:define>
<bean:define id="intRecSize" name="frmCOMRSRProcess" property="intRecSize" type="java.lang.Integer"></bean:define>
<bean:define id="strXmlData" name="frmCOMRSRProcess" property="strXmlData" type="java.lang.String"></bean:define>
<%
String strQualCmplJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_QUALITY_COMPLAINT");
%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<TITLE> Globus Medical: COM/RSR Summary Screen </TITLE>
<script language="JavaScript" src="<%=strQualCmplJsPath%>/GmCOMRSRSummary.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script>
	var objGridData = '<%=strXmlData%>';
	var strOpt;
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<html:form action="/gmCOMRSRProcess.do" enctype="multipart/form-data">
<html:hidden property="comRsrID" name="frmCOMRSRProcess"/>
<html:hidden property="strOpt" name="frmCOMRSRProcess"/>
<html:hidden property="refType" name="frmCOMRSRProcess"/>
	<table border="0" class="DtTable950" cellspacing="0" cellpadding="0">
		<tr><td class="Line" colspan="4"></td></tr>
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtCOMRSRSummary:message key="LBL_COM_RSR_SUMMARY_SCREEN"/></td>
		</tr>
		<tr><td class="LLine" colspan="4"></td></tr>
		<tr bgcolor="#e4e6f2" class="ShadeRightTableCaption" height="25">
			<td colspan="4" height="25">&nbsp;<fmtCOMRSRSummary:message key="LBL_GENERAL_DETAILS"/>:</td>
		</tr>
		<tr>
			<td>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td class="LLine" colspan="4"></td></tr>
					<tr class="Odd">
						<td class="RightTableCaption" align="right" width="30%"><fmtCOMRSRSummary:message key="LBL_COM_RSR"/>:</td>
						<td height="20" width="20%">&nbsp;<bean:write name="frmCOMRSRProcess" property="comRsrID"/> </td>
						<td class="RightTableCaption" align="right" width="20%"><fmtCOMRSRSummary:message key="LBL_STATUS"/>:</td>
						<td height="25" width="20%">&nbsp;<bean:write name="frmCOMRSRProcess" property="statusnm"/> </td>  
					</tr>
					<tr><td class="LLine" colspan="4"></td></tr>
					<tr class="Shade">
						<td class="RightTableCaption" align="right"><fmtCOMRSRSummary:message key="LBL_COMPLAINANT"/>:</td>
						<td height="25">&nbsp;<bean:write name="frmCOMRSRProcess" property="repName"/> </td>
						<td class="RightTableCaption" align="right"><fmtCOMRSRSummary:message key="LBL_INCIDENT_CATEGORY"/>:</td>
						<td height="25">&nbsp;<bean:write name="frmCOMRSRProcess" property="incidentCatNm"/> </td>
					</tr>
					<tr><td class="LLine" colspan="4"></td></tr>
					<tr class="Odd">
						<td class="RightTableCaption" align="right"><fmtCOMRSRSummary:message key="LBL_COMPLAINT_RECEIVED_DATE"/>:</td>
						<td height="25">&nbsp;<bean:write name="frmCOMRSRProcess" property="complRecvDt"/> </td>
						<td class="RightTableCaption" align="right"><fmtCOMRSRSummary:message key="LBL_SURGEON"/>:</td>
						<td height="25" >&nbsp;<bean:write name="frmCOMRSRProcess" property="surgeoonName"/> </td>  
					</tr>
					<tr><td class="LLine" colspan="4"></td></tr>
					<tr class="Shade">
						<td class="RightTableCaption" align="right"><fmtCOMRSRSummary:message key="LBL_COMPLAINT_DESCRIPTION"/>:</td>
						<td colspan="3">&nbsp;
						<div id="showDiv" style="vertical-align: middle;margin-left:7px;width: 710px; height: 70px;<%if(detailDescEvent.length()>1000){ %>overflow-y:scroll<%}%>"> 
						<bean:write name="frmCOMRSRProcess" property="detailDescEvent"/></div>
						</td>
					</tr>
					<tr><td class="LLine" colspan="4"></td></tr>
					<tr class="Odd">
						<td class="RightTableCaption" align="right"><fmtCOMRSRSummary:message key="LBL_MDR"/>:</td>
						<td height="25">&nbsp;<bean:write name="frmCOMRSRProcess" property="mdrID"/> </td>
						<td class="RightTableCaption" align="right"><fmtCOMRSRSummary:message key="LBL_MDR_REPORTED_DATE"/>:</td>
						<td height="25" >&nbsp;<bean:write name="frmCOMRSRProcess" property="mdrReportDt"/> </td>  
					</tr>
					<tr><td class="LLine" colspan="4"></td></tr>
					<tr class="Shade">
						<td class="RightTableCaption" align="right"><fmtCOMRSRSummary:message key="LBL_MEDDEV"/>:</td>
						<td height="25">&nbsp;<bean:write name="frmCOMRSRProcess" property="meddevID"/> </td>
						<td class="RightTableCaption" align="right"><fmtCOMRSRSummary:message key="LBL_MEDEV_REPORTED_DATE"/>:</td>
						<td height="25" >&nbsp;<bean:write name="frmCOMRSRProcess" property="meddevReportDt"/> </td>  
					</tr>
				</table>
			</td>
		</tr>
		<tr><td class="LLine" colspan="4"></td></tr>
		<tr bgcolor="#e4e6f2" class="ShadeRightTableCaption">
			<td colspan="4" height="25">&nbsp;<fmtCOMRSRSummary:message key="LBL_PART_DETAILS"/>:</td>
		</tr>
		<tr><td class="LLine" colspan="4"></td></tr>
		<tr>
			<td>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td class="LLine" colspan="4"></td></tr>
					<tr class="Odd">
						<td class="RightTableCaption" align="right" width="25%"><fmtCOMRSRSummary:message key="LBL_PART"/>:</td>
						<td height="25" width="25%">&nbsp;<bean:write name="frmCOMRSRProcess" property="partnum"/> </td>
						<td class="RightTableCaption" align="right" width="25%"><fmtCOMRSRSummary:message key="LBL_PART_DESCRIPTION"/>:</td>
						<td height="25" width="29%">&nbsp;<bean:write name="frmCOMRSRProcess" property="partdesc"/> </td>  
					</tr>
					<tr><td class="LLine" colspan="4"></td></tr>
					<tr class="Shade">
						<td class="RightTableCaption" align="right"><fmtCOMRSRSummary:message key="LBL_PROJECT"/>:</td>
						<td height="25">&nbsp;<bean:write name="frmCOMRSRProcess" property="projdesc"/> </td>
						<td class="RightTableCaption" align="right"><fmtCOMRSRSummary:message key="LBL_GEM"/>:</td>
						<td height="25" >&nbsp;<bean:write name="frmCOMRSRProcess" property="gemnm"/> </td>  
					</tr>
					<tr><td class="LLine" colspan="4"></td></tr>
					<tr class="Odd">
						<td class="RightTableCaption" align="right"><fmtCOMRSRSummary:message key="LBL_TYPE"/>:</td>
						<td height="25">&nbsp;<bean:write name="frmCOMRSRProcess" property="typenm"/> </td>
						<td class="RightTableCaption" align="right"><fmtCOMRSRSummary:message key="LBL_LOT"/>:</td>
						<td height="25" >&nbsp;<bean:write name="frmCOMRSRProcess" property="lotnum"/> </td>  
					</tr>
				</table>
			</td>
		</tr>
		<tr><td class="LLine" colspan="4"></td></tr>
		<tr bgcolor="#e4e6f2" class="ShadeRightTableCaption">
			<td colspan="4" height="25">&nbsp;<fmtCOMRSRSummary:message key="LBL_CLOSURE_DETAILS"/>:</td>
		</tr>
		<tr><td class="LLine" colspan="4"></td></tr>
		<tr>
			<td>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td class="LLine" colspan="4"></td></tr>
					<tr class="Odd">
						<td class="RightTableCaption" align="right" width="32%"><fmtCOMRSRSummary:message key="LBL_COM_RSR_CLOSURE_DATE"/>:</td>
						<td height="25" width="29%">&nbsp;<bean:write name="frmCOMRSRProcess" property="closureDt"/> </td>
						<td class="RightTableCaption" align="right" width="25%"><fmtCOMRSRSummary:message key="LBL_COM_RSR_DISPOSITION"/>:</td>
						<td height="25" width="27%">&nbsp;<bean:write name="frmCOMRSRProcess" property="comRsrDispTyp"/> </td>  
					</tr>
					<tr><td class="LLine" colspan="4"></td></tr>
					<tr class="Shade">
						<td class="RightTableCaption" align="right"><fmtCOMRSRSummary:message key="LBL_DETAILED_EVALUATION_RESULTS_CONCLUSIONS"/>:</td>
						<td height="25" colspan="3">&nbsp;
						<div id="showDiv" style="vertical-align: middle;margin-left:7px;width: 710px; height: 70px;<%if(evalResult.length()>1000){ %>overflow-y:scroll<%}%>">
						<bean:write name="frmCOMRSRProcess" property="evalResult"/> </td>
					</tr>
					<tr><td class="LLine" colspan="4"></td></tr>
					<tr class="Odd">
						<td class="RightTableCaption" align="right"><fmtCOMRSRSummary:message key="LBL_PRPDICT_DISPOSITION"/>:</td>
						<td height="25" colspan="3">&nbsp;<bean:write name="frmCOMRSRProcess" property="pdtDispTyp"/> </td>
					</tr>
					<tr><td class="LLine" colspan="4"></td></tr>
					<tr class="Shade">
						<td class="RightTableCaption" align="right"><fmtCOMRSRSummary:message key="LBL_PREVIOUS_ACTIONS"/>:</td>
						<td height="25" colspan="3">&nbsp;<bean:write name="frmCOMRSRProcess" property="prvActions"/> </td>
					</tr>
					<tr><td class="LLine" colspan="4"></td></tr>
					<tr class="Odd">
						<td class="RightTableCaption" align="right"><fmtCOMRSRSummary:message key="LBL_CORRECTIVE_AND_PRVENTIVE_ACTIONS"/>Corrective and Preventive Actions:</td>
						<td height="25" colspan="3">&nbsp;
						<div id="showDiv" style="vertical-align: middle;margin-left:7px;width: 710px; height: 70px;<%if(capaComments.length()>1000){ %>overflow-y:scroll<%}%>">
						<bean:write name="frmCOMRSRProcess" property="capaComments"/> </td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td class="LLine" colspan="4"></td></tr>
		<tr bgcolor="#e4e6f2" class="ShadeRightTableCaption">
			<td colspan="4" height="25">&nbsp;<fmtCOMRSRSummary:message key="LBL_FILE_UPLOADS"/>:</td>
		</tr>
		<tr><td class="LLine" colspan="4"></td></tr>
		<%if(intRecSize > 0){ %> 
			<tr><td colspan="4"><div id="grpData" style="grid" height="150px" width="950">&nbsp;</div></td></tr>
		<%}else{%>
			<tr><td height="25" colspan="4"><font color="blue" size="1"><fmtCOMRSRSummary:message key="LBL_NO_DATA"/></font></td></tr>
		<%}%>
		<tr><td class="LLine" colspan="4"></td></tr>
		<tr height="30"><td align="center">
			<fmtCOMRSRSummary:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="${varClose}" name="Btn_Close" buttonType="Load" gmClass="button" onClick="fnClose();" />
		</td></tr>
	</table>
	</html:form>
</BODY>
<%@ include file="/common/GmFooter.inc"%>
</HTML>