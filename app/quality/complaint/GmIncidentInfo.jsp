<%
/****************************************************************************************************
 * File		 		: GmIncidentInfo.jsp
 * Desc		 		: This JSP Used to Complaint Setup
 * Version	 		: 1.0
 * author			: HReddi
******************************************************************************************************/
%>
<!-- quality\complaint\GmIncidentInfo.jsp -->
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtIncidentInfo" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtIncidentInfo:setLocale value="<%=strLocale%>"/>
<fmtIncidentInfo:setBundle basename="properties.labels.quality.complaint.GmIncidentInfo"/>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%> 
<bean:define id="comRsrID" name="frmIncidentInfo" property="comRsrID" type="java.lang.String"> </bean:define>
<bean:define id="raID" name="frmIncidentInfo" property="raID" type="java.lang.String"> </bean:define>
<bean:define id="screenType" name="frmIncidentInfo" property="screenType" type="java.lang.String"> </bean:define>
<bean:define id="complaint" name="frmIncidentInfo" property="complaint" type="java.lang.String"></bean:define>
<bean:define id="mdrReport" name="frmIncidentInfo" property="mdrReport" type="java.lang.String"></bean:define>
<bean:define id="status" name="frmIncidentInfo" property="status" type="java.lang.String"></bean:define>
<bean:define id="meddevReport" name="frmIncidentInfo" property="meddevReport" type="java.lang.String"></bean:define>
<bean:define id="strOpt" name="frmIncidentInfo" property="strOpt" type="java.lang.String"> </bean:define>
<bean:define id="strMessage" name="frmIncidentInfo" property="strMessage" type="java.lang.String"> </bean:define>
<bean:define id="alEmpList" name="frmIncidentInfo" property="alEmpList" type="java.util.ArrayList"></bean:define>
<bean:define id="alRepList" name="frmIncidentInfo" property="alRepList" type="java.util.ArrayList"></bean:define>
<bean:define id="complaintname" name="frmIncidentInfo" property="complaintname" type="java.lang.String"> </bean:define>
<%
	String strQualCmplJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_QUALITY_COMPLAINT");
	response.setContentType("text/html; charset=UTF-8");
	response.setCharacterEncoding("UTF-8");
	String strTodaysDate = (String)session.getAttribute("strSessTodaysDate")==null?"":(String)session.getAttribute("strSessTodaysDate");
	String strApplnDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	HashMap hcboVal = null;
	String strDisableDropdown = "";
	boolean strDisableTexBox = true;
	if(complaint.equals("0") || complaint.equals("")){
		strDisableDropdown = "disabled";
	}
	if(meddevReport.equals("Yes") || mdrReport.equals("Yes")){
		strDisableTexBox = false;
	}
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Pending COM/RSR Dashboard  </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strQualCmplJsPath%>/GmIncidentInfo.js"></script> 
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script type="text/javascript">
var hIncidentId = '<%=comRsrID%>';
var RepLen = <%=alRepList.size()%>;
var EmpLen = <%=alEmpList.size()%>;
var mdrValue = '<%=mdrReport%>';
var medValue = '<%=meddevReport%>';
var compname = '<%=complaintname%>';
var format = '<%=strApplnDateFmt%>';

<%
hcboVal = new HashMap();
for (int i=0;i<alRepList.size();i++)
{
	hcboVal = (HashMap)alRepList.get(i);
%>
var alRepListArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
}
%>

<%
hcboVal = new HashMap();
for (int i=0;i<alEmpList.size();i++)
{
	hcboVal = (HashMap)alEmpList.get(i);
%>
var alEmpListArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
}
%>
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="javascript:fnSetDefaultValues();">
<html:form  action="/gmCOMRSRSetupAction.do">
<html:hidden property="strOpt" name="frmIncidentInfo"/>
<html:hidden property="raID" name="frmIncidentInfo"/>
<html:hidden property="status" name="frmIncidentInfo"/>
<html:hidden property="hIncidentID" value="<%=comRsrID%>"/>

<!-- Custom tag lib code modified for JBOSS migration changes -->
<!-- Struts tag lib code modified for JBOSS migration changes -->

<table class="DtTable1000" cellspacing="0" cellpadding="0" onkeypress="fnEnter();">
		<tr>
			<td height="25" colspan="4" class="RightDashBoardHeader" ><fmtIncidentInfo:message key="LBL_INCIDENT_INFORMATION"/></td>
		    <td align="right" class=RightDashBoardHeader>
				<fmtIncidentInfo:message key="IMG_ALT_HELP" var="varHelp" /><img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("INCIDENT_INFO")%>');" />
			</td> 
		</tr>
		<% if(!strMessage.equals("")){ %>
		<tr>
			<td align="center" height="25" colspan="5">&nbsp;<font style="font-size: x-small;color: blue; font-weight: bold;"><bean:write name="frmIncidentInfo" property="strMessage"/></font></td>							
		</tr>
		<%} %>
		<tr>
		<td colspan="5">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightTableCaption" width="18%" align="Right" height="25">&nbsp;<fmtIncidentInfo:message key="LBL_COM_RSR"/>:</td>
						<td width="25%">&nbsp;<html:text maxlength="50" property="comRsrID" size="20"   
							styleClass="InputArea" style="text-transform:uppercase;" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="3" /></td>
						<td align="left" width="50%"><fmtIncidentInfo:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}" style="width: 4em;height: 23px" tabindex="14" name="Btn_Load" gmClass="button" buttonType="Load" onClick="fnLoad();" /></td>
					</tr>
				</table>						
		</td>
		</tr>
		<tr><td colspan="5" class="LLine" height="1"></td></tr>
		<tr id="displaytable">
			<td height="25" class="ShadeRightTableCaption" colspan="5"><a href="javascript:fnShowFilters('tabGeneralDetails');" tabindex="-1" style="text-decoration:none; color:black" >
			<IMG id="tabGeneralDetailsimg" border=0 src="<%=strImagePath%>/minus.gif">&nbsp;<fmtIncidentInfo:message key="LBL_GENERAL_DETAILS"/></a>		
		</td>
		</tr>
		<tr id="tabGeneralDetails" style="display:table-row;">			
			<td colspan="5">
				<table border="0" cellspacing="0" cellpadding="0">
				<tr class="">				
					<td class="RightTableCaption" align="right" height="25" width="18%"><fmtIncidentInfo:message key="LBL_RA"/>:</td>
					<td colspan="2">&nbsp;<bean:write name="frmIncidentInfo" property="raID"/></td>					
					<td class="RightTableCaption" align="right" height="25" width="17%"><fmtIncidentInfo:message key="LBL_ISSUE_TYPE"/>:</td>
					<td align ="left" >&nbsp;<gmjsp:dropdown controlName="issueType" SFFormName="frmIncidentInfo" 
						SFSeletedValue="issueType" defaultValue= "[Choose One]" SFValue="alIssueType" codeId="CODEID" codeName="CODENM" />
					</td>
				</tr>
				<tr><td class="LLine" colspan="5" height="1"></td></tr>
				<tr class="shade">
					<td class="RightTableCaption" align="right" height="25" ><fmtIncidentInfo:message key="LBL_COMPLAINANT"/>:</td>
					<td align ="left" >&nbsp;<gmjsp:dropdown controlName="complaint" SFFormName="frmIncidentInfo"
						SFSeletedValue="complaint" defaultValue= "[Choose One]" SFValue="alComplaint" codeId="CODEID" codeName="CODENM" onChange="javascript:fnComplaintNames();" />
					</td>
					<% if(complaint.equals("102812")){%>
						
						<td >&nbsp;<gmjsp:dropdown disabled="<%=strDisableDropdown%>" controlName="complaintname" SFFormName="frmIncidentInfo" SFSeletedValue="complaintname" defaultValue= "[Choose One]" 
								SFValue="alRepList" codeId="ID" codeName="NAME" onChange="javascript:fnRepDetails(this);"/>
						</td>
					<%}else{%>
						<td >&nbsp;<gmjsp:dropdown disabled="<%=strDisableDropdown%>" controlName="complaintname" SFFormName="frmIncidentInfo" SFSeletedValue="complaintname" defaultValue= "[Choose One]" 
								SFValue="alEmpList" codeId="ID" codeName="NAME" onChange="javascript:fnRepDetails(this);"/> 
						</td>	
					<%} %>					
					<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_STATUS"/>:</td>
					<td>&nbsp;<bean:write name="frmIncidentInfo" property="statusNm"/></td>
				</tr>	
				<tr><td class="LLine" colspan="5" height="1"></td></tr>
				<tr>
					<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_ADDRESS"/>:</td>
					<td colspan="2">&nbsp;<html:text maxlength="1000" property="address" size="40"   
							styleClass="InputArea"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="3" /></td>
					<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_PHONE_NUMBER"/>:</td>
					<td colspan="2">&nbsp;<html:text maxlength="20" property="phoneNumber" size="20"   
							styleClass="InputArea"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="3" /></td>
				</tr>
				<tr><td class="LLine" colspan="5" height="1"></td></tr>
				<tr class="shade">
					<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_COMPLAINANT_COMPANY"/>:</td>
					<td colspan="2">&nbsp;<html:text maxlength="200" property="company" size="20"   
							styleClass="InputArea"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="3" /></td> 
					<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_AREA_DIRECTOR"/>:</td>
					<td align ="left" >&nbsp;<gmjsp:dropdown controlName="areaDirector" SFFormName="frmIncidentInfo"
						SFSeletedValue="areaDirector" defaultValue= "[Choose One]" SFValue="alAreaDirect" codeId="AD_ID" codeName="AD_NAME" />
					</td>
				</tr>
				<tr><td class="LLine" colspan="5" height="1"></td></tr>
				<tr>
					<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_INCIDENT_DATE"/>:</td>
					<td colspan="2">&nbsp;<gmjsp:calendar SFFormName="frmIncidentInfo" controlName="dtIncDate" gmClass="InputArea" 
									onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
					</td>
					<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_INCEDENT_CATEGORY"/>:</td>
					<td align ="left" >&nbsp;<gmjsp:dropdown controlName="incidentCat" SFFormName="frmIncidentInfo"
						SFSeletedValue="incidentCat" defaultValue= "[Choose One]" SFValue="alIncidentCat" codeId="CODEID" codeName="CODENM" />
					</td>
				</tr>
				<tr><td class="LLine" colspan="5" height="1"></td></tr>
				<tr class="shade">
					<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_COMPLAINT_RECEIVED_BY"/>:</td>
					<td align ="left" colspan="2">&nbsp;<gmjsp:dropdown controlName="complRecvdBy" SFFormName="frmIncidentInfo"
						SFSeletedValue="complRecvdBy" defaultValue= "[Choose One]" SFValue="alComplRecvBy" codeId="ID" codeName="NAME" />
					</td>
					<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_COMPLAINT_RECEIVED_DATE"/>:</td>
					<td>&nbsp;<gmjsp:calendar SFFormName="frmIncidentInfo" controlName="complRecvDt" gmClass="InputArea" 
									onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>
				<tr><td class="LLine" colspan="5" height="1"></td></tr>
				<tr>
					<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_COMPLAINT_RECEIVED_VIA"/>:</td>
					<td align ="left" colspan="2">&nbsp;<gmjsp:dropdown controlName="complRecvVia" SFFormName="frmIncidentInfo"
						SFSeletedValue="complRecvVia" defaultValue= "[Choose One]" SFValue="alComplRecvVia" codeId="CODEID" codeName="CODENM" />
					</td>
					<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_ORIGINATOR"/>:</td>
					<td align ="left" colspan="2">&nbsp;<gmjsp:dropdown controlName="originator" SFFormName="frmIncidentInfo"
						SFSeletedValue="originator" defaultValue= "[Choose One]" SFValue="alOriginator" codeId="ID" codeName="NAME" />
					</td>
				</tr>
				<tr><td class="LLine" colspan="5" height="1"></td></tr>
				<tr class="shade">
					<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_MDR_REPORTABLE"/>:</td>
					<td align ="left" colspan="2">&nbsp;<html:radio property="mdrReport" value="Yes" onclick="javascript:fnEnableMDR(this);"/>&nbsp;<fmtIncidentInfo:message key="LBL_YES"/>&nbsp;
						<html:radio property="mdrReport" value="No" onclick="javascript:fnEnableMDR(this);" />&nbsp;<fmtIncidentInfo:message key="LBL_NO"/>
					</td>
					<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_MEDDEV_REPORTABLE"/>:</td>
					<td>&nbsp;<html:radio value="Yes" property="meddevReport" onclick="javascript:fnEnableMEDDEV(this);"/>&nbsp;<fmtIncidentInfo:message key="LBL_YES"/>&nbsp;
						<html:radio value="No" property="meddevReport" onclick="javascript:fnEnableMEDDEV(this);"/>&nbsp;<fmtIncidentInfo:message key="LBL_NO"/>
					</td>
				</tr>
				<tr><td class="LLine" colspan="5" height="1"></td></tr>
				<tr>
					<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_MDR"/>:</td>
					<td colspan="2">&nbsp;<html:text property="mdrID" maxlength="25" size="30" styleClass="InputArea"
						onfocus="changeBgColor(this,'#AACCE8');" disabled="<%=strDisableTexBox %>" onblur="changeBgColor(this,'#ffffff');" /></div>
					</td>
					<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_MEDDEV"/>:</td>
					<td>&nbsp;<html:text property="meddevID" maxlength="20" size="20" styleClass="InputArea"
						onfocus="changeBgColor(this,'#AACCE8');" disabled="<%=strDisableTexBox %>" onblur="changeBgColor(this,'#ffffff');"/></div>
					</td>
				</tr>
				<tr><td class="LLine" colspan="5" height="1"></td></tr>
				<tr class="shade">
					<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_MDR_REPORTED_DATE"/>:</td>
					<td align ="left" colspan="2">&nbsp;<gmjsp:calendar SFFormName="frmIncidentInfo" controlName="mdrReportDt" gmClass="InputArea" 
									onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
					</td>
					<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_MEDDEV_REPORTED_DATE"/>:</td>
					<td>&nbsp;<gmjsp:calendar SFFormName="frmIncidentInfo" controlName="meddevReportDt" gmClass="InputArea" 
									onFocus="changeBgColor(this,'#AACCE8');"  onBlur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>
				<tr><td class="LLine" colspan="5" height="1"></td></tr>
				<tr>
					<td class="RightTableCaption" align="right"><fmtIncidentInfo:message key="LBL_DETAILED_DESCRIPTION_OF_EVENTS"/>:</td>
					<td align ="left" colspan="4" height="20%">&nbsp;<html:textarea property="detailDescEvent" cols="110" style="height:70px"
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>					
			</table>
		</td>
	</tr>
	<tr><td class="ELine" colspan="5" height="1"></td></tr>
	<tr id="displaytable">
		<td height="25" class="ShadeRightTableCaption" colspan="5"><a href="javascript:fnShowFilters('tabPartDetails');" tabindex="-1" style="text-decoration:none; color:black" >
		<IMG id="tabPartDetailsimg" border=0 src="<%=strImagePath%>/minus.gif">&nbsp;<fmtIncidentInfo:message key="LBL_PART_DETAILS"/></a></td>
	</tr>
		<tr id="tabPartDetails" style="display:table-row;">
			<td colspan="5">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr class="shade">
						<td class="RightTableCaption" align="right" height="25" width="16%"><fmtIncidentInfo:message key="LBL_PART"/>:</td>
						<td colspan="2" width="40%">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="left" width="10%">&nbsp;<html:text property="partNum" maxlength="15" size="20" styleClass="InputArea"
											onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');javascript:fnPartDetails(this); "/>
									</td>
									<td width="3%"><IMG align="left" id="validatePart" border=0 height="20" width="20" src="" style="display: none" /></td>
									<td align="left" width="60%"><div id="validateText" class="RegularText"></div> </td>
								</tr>
							</table>
						</td>
						<td class="RightTableCaption" align="right" height="25" width="17%"><fmtIncidentInfo:message key="LBL_PART_DESCRIPTION"/>:</td>
						<td>&nbsp;<html:text property="partDesc" size="35" styleClass="InputArea"
							onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/>
						</td>
					</tr>
					<tr><td class="LLine" colspan="5" height="1"></td></tr>
					<tr>
					<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_PROJECT"/>:</td>
						<td align ="left" colspan="2">&nbsp;<gmjsp:dropdown controlName="project" SFFormName="frmIncidentInfo"
							SFSeletedValue="project" defaultValue= "[Choose One]" SFValue="alProjectList" codeId="ID" codeName="NAME" />
						</td>
						<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_GEM"/>:</td>
						<td align ="left" colspan="2">&nbsp;<gmjsp:dropdown controlName="gem" SFFormName="frmIncidentInfo"
							SFSeletedValue="gem" defaultValue= "[Choose One]" SFValue="alGemList" codeId="PARTYID" codeName="USERNAME" />
						</td>
					</tr>
					<tr><td class="LLine" colspan="5" height="1"></td></tr>
					<tr class="shade">
					<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_TYPE"/>:</td>
						<td align ="left" colspan="2">&nbsp;<gmjsp:dropdown controlName="type" SFFormName="frmIncidentInfo"
							SFSeletedValue="type" defaultValue= "[Choose One]" SFValue="alTypeList" codeId="CODEID" codeName="CODENM" />
						</td>
						<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_LOT"/>:</td>
						<td colspan="2">&nbsp;<html:text property="lotNum" maxlength="255" size="20" styleClass="InputArea"
							onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
						</td>
					</tr>
					<tr><td class="LLine" colspan="5" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_QUALITY"/>:</td>
						<td colspan="2">&nbsp;<html:text property="quantity" maxlength="20" size="20" styleClass="InputArea"
							onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />							
						</td>
						<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_LOANER_OR_CONSIGNMENT"/>:</td>						
						<td align ="left" colspan="2">&nbsp;<gmjsp:dropdown controlName="lonorConsign" SFFormName="frmIncidentInfo"
							SFSeletedValue="lonorConsign" defaultValue= "[Choose One]" SFValue="alLoanorConsign" codeId="CODEID" codeName="CODENM" />
						</td>
					</tr>
				</table>
			</td>
		</tr>	
		<tr><td class="ELine" colspan="5" height="1"></td></tr>
	<tr id="displaytable">
		<td height="25" class="ShadeRightTableCaption" colspan="5"><a href="javascript:fnShowFilters('tabIncidentDetails');"  tabindex="-1" style="text-decoration:none; color:black">
		<IMG id="tabIncidentDetailsimg" border=0 src="<%=strImagePath%>/minus.gif">&nbsp;<fmtIncidentInfo:message key="LBL_INCIDENT_DETAILS"/></a></td>
	</tr>
		<tr id="tabIncidentDetails" style="display:table-row;">
			<td colspan="5">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr class="shade">
						<td class="RightTableCaption" align="right" height="25" width="35%"  colspan="3">
							<table>
								<tr>
									<td class="RightTableCaption" align="right" width="50%"><fmtIncidentInfo:message key="LBL_IS_THE_REPORTED_CONDITION_DUE_TO_NORMAL_WEAR_AND_USAGE"/>:</td>
									<td>&nbsp;<html:radio value="Yes" property="normalWearFl"/>&nbsp;<fmtIncidentInfo:message key="LBL_YES"/>&nbsp;
								  		<html:radio value="No" property="normalWearFl"/>&nbsp;<fmtIncidentInfo:message key="LBL_NO"/></td>									
								 </tr>
									<tr>
									<td class="RightTableCaption" align="right" height="25" width="50%"><fmtIncidentInfo:message key="LBL_DID_THIS_EVENT_HAPPEN_DURING_SURGERY"/>:</td>						
									<td align ="left" colspan="3">&nbsp;<gmjsp:dropdown controlName="didThisEventHappenDuring" SFFormName="frmIncidentInfo"
										SFSeletedValue="didThisEventHappenDuring" defaultValue= "[Choose One]" SFValue="alDidThisEventHappenDuring" codeId="CODEID" codeName="CODENM" />
									</td>									
								 </tr>
								 
							</table>
						</td>
								
				<!-- 		<td align ="left" colspan="2">
							<table>
								<tr>
									<td class="RightTableCaption" align="left" width="20%" colspan="2">&nbsp;<html:radio value="Yes" property="normalWearFl"/>&nbsp;<fmtIncidentInfo:message key="LBL_YES"/>&nbsp;
								  		<html:radio value="No" property="normalWearFl"/>&nbsp;<fmtIncidentInfo:message key="LBL_NO"/></td><br>
								</tr>									
							<!-- 	<tr>
									<td class="RightTableCaption" align="left" width="20%" height="25" colspan="2">&nbsp;<html:radio value="Yes" property="eventSurgeyFl" />&nbsp;<fmtIncidentInfo:message key="LBL_YES"/>&nbsp;
								  		<html:radio value="No" property="eventSurgeyFl"/>&nbsp;<fmtIncidentInfo:message key="LBL_NO"/>
								 	</td>
								</tr> -->
					<!-- 		</table>	-->	
					
						<td class="RightTableCaption" align="right" height="25" width="30%"><fmtIncidentInfo:message key="LBL_IF_YES_PLEASE_EXPLAIN"/>:</td>
								<td align ="left" colspan="2" height="40%">&nbsp;<html:textarea property="ifYesExplain" cols="40" style="height:70px"
									styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
								</td>
								</tr>
								
					
					<tr><td class="LLine" colspan="5" height="1"></td></tr>
					<tr>
					<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_TYPE_OF_SURGERY"/>:</td>
						<td colspan="2">&nbsp;<html:text property="typeOfSurgery" maxlength="20" size="20" styleClass="InputArea"
							onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />							
						</td>
						<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_SI_THIS_A_REVISION_SURGERY"/>:</td>
						<td>&nbsp;<html:radio value="Yes" property="reviSurgery" />&nbsp;<fmtIncidentInfo:message key="LBL_YES"/>&nbsp;
						<html:radio value="No" property="reviSurgery" />&nbsp;<fmtIncidentInfo:message key="LBL_NO"/>
					</td>
					</tr>
					<tr><td class="LLine" colspan="5" height="1"></td></tr>
					<tr class="shade">
						<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_SURGEON_NAME"/>:</td>
						<td colspan="2">&nbsp;<html:text property="surgeoonName"  maxlength="20" size="20" styleClass="InputArea"
							onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />							
						</td>
						<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_ANY_ADEVERSE_EFFECT_T0_THE_PATIENT"/>:</td>						
						<td align ="left" colspan="2">&nbsp;<gmjsp:dropdown controlName="adverseEffect" SFFormName="frmIncidentInfo"
							SFSeletedValue="adverseEffect" defaultValue= "[Choose One]" SFValue="aladverseEffect" codeId="CODEID" codeName="CODENM" />
						</td>
					</tr>
					<tr><td class="LLine" colspan="5" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_HOSPITAL"/>:</td>
						<td align ="left" colspan="2">&nbsp;<gmjsp:dropdown controlName="hospitalName" width="250"   SFFormName="frmIncidentInfo"
							SFSeletedValue="hospitalName" defaultValue= "[Choose One]" SFValue="alHospitalList" codeId="ID" codeName="NAME" />
						</td>
						<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_DATE_OF_SURGERY"/>:<fmtIncidentInfo:message key="LBL_IF_APPLICABLE"/></td>						
						<td align ="left" colspan="2">&nbsp;<gmjsp:calendar SFFormName="frmIncidentInfo" controlName="dateOfSurgery" gmClass="InputArea" 
									onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
						</td>
					</tr>
					<tr><td class="LLine" colspan="5" height="1"></td></tr>
					<tr class="shade">
						<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_WAS_EVENT_IDENTIFIED_POST_OPERATIVELY"/>:</td>
						<td align ="left"  colspan="2">&nbsp;<html:radio value="Yes" property="postOpeFlag" />&nbsp;<fmtIncidentInfo:message key="LBL_YES"/>&nbsp;
						<html:radio value="No" property="postOpeFlag"/>&nbsp;<fmtIncidentInfo:message key="LBL_NO"/>
						</td>
						<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_IF_POST_OPERATIVE_WHAT_WAS_THE_DATE_OF_THE_ORIGINAL_SUEGERY"/>:</td>						
						<td align ="left" colspan="2">&nbsp;<gmjsp:calendar SFFormName="frmIncidentInfo" controlName="postOpeDate" gmClass="InputArea" 
									onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
						</td>
					</tr>
					<tr><td class="LLine" colspan="5" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_PROCEDURE_OUTCOME"/>:</td>						
						<td align ="left" colspan="2">&nbsp;<gmjsp:dropdown controlName="procedureOutcome" SFFormName="frmIncidentInfo"
							SFSeletedValue="procedureOutcome" defaultValue= "[Choose One]" SFValue="alprocedureOutcome" codeId="CODEID" codeName="CODENM" />
						</td>
						<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_DID_THIS_EVENT_HAPPEN_IN_A_DEMONSTRATION"/>:</td>
						<td align ="left"  colspan="2">&nbsp;<html:radio value="Yes" property="eventDemoFl" />&nbsp;<fmtIncidentInfo:message key="LBL_YES"/>&nbsp;
							<html:radio value="No" property="eventDemoFl"/>&nbsp;<fmtIncidentInfo:message key="LBL_NO"/>
						</td>
						
					</tr>
					<tr><td class="LLine" colspan="5" height="1"></td></tr>
					<tr class="shade">
						<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_DO_YOU_REQUEST_A_REPLACEMENT_FROM_IDENTICAL_PART_NUMBER"/>:</td>
						<td align ="left"  colspan="2">&nbsp;<html:radio value="Yes" property="replacePartFl" />&nbsp;<fmtIncidentInfo:message key="LBL_YES"/>&nbsp;
							<html:radio value="No" property="replacePartFl"/>&nbsp;<fmtIncidentInfo:message key="LBL_NO"/>
						</td>
						<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_IS_THE_PART_AVAILABLE_FOR_EVALUATION"/>:</td>						
						<td align ="left"  colspan="2">&nbsp;<html:radio value="Yes" property="partAvailEvalFl"/>&nbsp;<fmtIncidentInfo:message key="LBL_YES"/>&nbsp;
							<html:radio value="No" property="partAvailEvalFl"/>&nbsp;<fmtIncidentInfo:message key="LBL_NO"/>
						</td>
					</tr>
					<tr><td class="LLine" colspan="5" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_HAS_THE_PART_BEEN_DECONTAMINATED_BEFORE_RETURN"/>:</td>
						<td align ="left"  colspan="2">&nbsp;<html:radio value="Yes" property="deconFl" />&nbsp;<fmtIncidentInfo:message key="LBL_YES"/>&nbsp;
							<html:radio value="No" property="deconFl"/>&nbsp;<fmtIncidentInfo:message key="LBL_NO"/>
						</td>
						<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_DO_YOU_NEED_A_REPLACEMENT"/>:</td>						
						<td align ="left"  colspan="2">&nbsp;<html:radio value="Yes" property="replacementFl"/>&nbsp;<fmtIncidentInfo:message key="LBL_YES"/>&nbsp;
							<html:radio value="No" property="replacementFl"/>&nbsp;<fmtIncidentInfo:message key="LBL_NO"/>
						</td>						
					</tr>
					<tr class="shade">
					<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_IF_SO_HAS_IT_BEEN_SHIPPED_TO_GLOBUS_MEDICAL"/>:</td>						
						<td align ="left"  colspan="2">&nbsp;<html:radio value="Yes" property="shippedFl"/>&nbsp;<fmtIncidentInfo:message key="LBL_YES"/>&nbsp;
							<html:radio value="No" property="shippedFl"/>&nbsp;<fmtIncidentInfo:message key="LBL_NO"/>
						</td>
					<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_DATE_SHIPPED"/>:</td>						
						<td align ="left" colspan="2">&nbsp;<gmjsp:calendar SFFormName="frmIncidentInfo" controlName="dateOfShipped" gmClass="InputArea" 
									onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
						</td>
						
					</tr>
					<tr><td class="LLine" colspan="5" height="1"></td></tr>
					<tr>					
					<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_CARRIER"/>:</td>						
						<td align ="left" colspan="2">&nbsp;<gmjsp:dropdown controlName="carrier" SFFormName="frmIncidentInfo"
							SFSeletedValue="carrier" defaultValue= "[Choose One]" SFValue="alCarrier" codeId="CODEID" codeName="CODENM" />
						</td>
					<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_TRACKING_NUMBER"/>:</td>
						<td colspan="2">&nbsp;<html:text property="trackingNumber"  maxlength="35" size="35" styleClass="InputArea"
							onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />							
						</td>
						
					</tr>
					<tr><td class="LLine" colspan="5" height="1"></td></tr>
					<tr class="shade">
					<td class="RightTableCaption" align="right" height="25"><fmtIncidentInfo:message key="LBL_ARE_YOU_REQUESTING_FIELD-SERVICE"/>:</td>						
						<td align ="left"  colspan="2">&nbsp;<html:radio value="Yes" property="reqFieldService"/>&nbsp;<fmtIncidentInfo:message key="LBL_YES"/>&nbsp;
							<html:radio value="No" property="reqFieldService"/>&nbsp;<fmtIncidentInfo:message key="LBL_NO"/>
						</td>
					
					<td class="RightTableCaption" align="right" height="25" width="30%"><fmtIncidentInfo:message key="LBL_ANY_OTHER_RELEVANT_INFORMATION"/>:</td>
						<td align ="left" colspan="2" height="40%">&nbsp;<html:textarea property="otherDetails" cols="40" style="height:70px"
							styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>				
				</table>
			</td>
		</tr>
		<tr><td class="LLine" colspan="5" height="1"></td></tr>		
		<tr>
			<td colspan="5"> 
				<jsp:include page="/common/GmIncludeLog.jsp" >
				<jsp:param name="FORMNAME" value="frmIncidentInfo" />
				<jsp:param name="ALNAME" value="alLogReasons" />
				<jsp:param name="LogMode" value="Edit" />
				</jsp:include>
			</td>
		</tr>
		<%if(!status.equals("102841")){ %>
		<tr>
			<td colspan="5" align="center" height="30">&nbsp;
				<fmtIncidentInfo:message key="BTN_RESET" var="varReset"/><gmjsp:button name="Btn_Reset" value="${varReset}" buttonType="Save" gmClass="button" style="width: 6em" onClick="fnReset();" />
				<fmtIncidentInfo:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button name="Btn_Submit" value="${varSubmit}" buttonType="Save" gmClass="button" style="width: 6em" onClick="fnSubmit();" />
            </td>
		</tr>	
		<%} %>
		<%if(strOpt.equals("Edit")){ %>
		<tr><td class="LLine" colspan="5" height="1"></td></tr>
		<tr class="shade">
			<td colspan="5" class="RightTableCaption" align="Center" height="28">&nbsp;
			  <gmjsp:dropdown controlName="chooseAction" SFFormName="frmIncidentInfo" SFSeletedValue="chooseAction" 
			   SFValue="alChosseActionList" codeId="CODEID" codeName="CODENM" 
			   defaultValue="[Choose One]"/> &nbsp;&nbsp;<fmtIncidentInfo:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" buttonType="Save" gmClass="button" style="height: 23px; width: 5em;" onClick="fnGo();"/>
			</td>
		</tr>
		<tr></tr>
		<%} %>
</table>		
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

