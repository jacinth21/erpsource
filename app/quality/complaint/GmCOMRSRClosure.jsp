<%
	/**********************************************************************************
	 * File		 		: GmCOMRSRClosure.jsp
	 * Desc		 		: Closure COM/RSR 
	 * Version	 		: 1.0
	 * author			: Elango
	 ************************************************************************************/
%>
<!-- quality\complaint\GmCOMRSRClosure.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtCOMRSRClosure" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtCOMRSRClosure:setLocale value="<%=strLocale%>"/>
<fmtCOMRSRClosure:setBundle basename="properties.labels.quality.complaint.GmCOMRSRClosure"/>
<%@ page import="com.globus.common.beans.GmGridFormat"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="com.globus.common.beans.GmCommonControls"%>
<bean:define id="alChooseAction" name="frmCOMRSRProcess" property="alChooseActionList" type="java.util.ArrayList"> </bean:define>
<bean:define id="strOpt"		name="frmCOMRSRProcess" property="strOpt"		type="java.lang.String"></bean:define>
<bean:define id="strCOMRSRID"	name="frmCOMRSRProcess" property="closeCOMRSRID"	type="java.lang.String"></bean:define>
<bean:define id="alEmployeeList" name="frmCOMRSRProcess" property="alEmployeeList"	type="java.util.ArrayList"></bean:define>
<bean:define id="alLogReasons"  name="frmCOMRSRProcess" property="alLogReasons"	type="java.util.ArrayList"></bean:define>
<bean:define id="intResultSize" name="frmCOMRSRProcess" property="intSize" type="java.lang.Integer"></bean:define>
<bean:define id="strMessage"    name="frmCOMRSRProcess" property="strMessage" type="java.lang.String"></bean:define>
<bean:define id="status"       name="frmCOMRSRProcess" property="status" type="java.lang.String"></bean:define>
<bean:define id="issueType"       name="frmCOMRSRProcess" property="issueType" type="java.lang.String"></bean:define>
<!-- PC-4223-COM/RSR Closure Email Comments Update -->
<bean:define id="closeCmts"  name="frmCOMRSRProcess" property="closeCmts" type="java.lang.String"></bean:define> 



<%

	String strQualCmplJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_QUALITY_COMPLAINT");
	String strWikiTitle = GmCommonClass.getWikiTitle("COMRSR_CLOSURE");
	ArrayList alRemoveCodeIDs = new ArrayList();
	ArrayList alRemoveIDs = new ArrayList();
	alRemoveCodeIDs.add("102845");
	alRemoveCodeIDs.add("102847");
	alChooseAction = GmCommonClass.parseNullArrayList(GmCommonClass.toRemoveCodeIdsFromArrayList(alRemoveCodeIDs,alChooseAction));
	String strLableRefId = "COM/RSR ";
	String strClassOdd = "";
	String strHighStrOpt = "";
	String currentDate = GmCommonClass.parseNull((String)session.getAttribute("strSessTodaysDate"));
	String strApplnDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	String strCollapseImage = "/images/minus.gif";
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: COM/RSR Closure</TITLE>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strQualCmplJsPath%>/GmCOMRSRClosure.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmProgress.css">
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script>
	var strOpt;
	var hCOMRSRID  = '<%=strCOMRSRID%>';
	var confirmMsg = false;
	var statusId   = '<%=status%>';
	var issue   = '<%=issueType%>';
	var currentDate = '<%=currentDate%>';
	var format = '<%=strApplnDateFmt%>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmCOMRSRProcess.do?method=closeCOMRSR" >
<html:hidden property="strOpt" name="frmCOMRSRProcess" />
<html:hidden property="hDisplayNm" name="frmCOMRSRProcess"/>
<html:hidden property="hRedirectURL" name="frmCOMRSRProcess"/>

<!-- Custom tag lib code modified for JBOSS migration changes -->

<table border="0" bordercolor="lightblue" class="DtTable1000" cellspacing="0" cellpadding="0" >
<tr>
	<td>
	<table border="0" bordercolor="" width="100%" class="DtTable950"	cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" colspan="2" class="RightDashBoardHeader">&nbsp;<fmtCOMRSRClosure:message key="LBL_CLOSE_COMPLAINT_AND_GENERATE_CLOSURE_LETTER"/></td>
			<td height="25"  class="RightDashBoardHeader"><fmtCOMRSRClosure:message key="IMG_ALT_HELP" var = "varHelp"/><img align="right"
				id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
				title='${varHelp}' width='16' height='16'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		</table>
		<table border="0" bordercolor="" width="100%" class="DtTable950" cellspacing="0" cellpadding="0">
		<% if(!strMessage.equals("")){ %>			
			<tr>
				<td colspan="3" align="center" height="25">&nbsp;<font style="color: Blue;"><b><bean:write name="frmCOMRSRProcess" property="strMessage"/></b></font></td>
			</tr>
		<%}%>
		<tr>
			<td class="RightTableCaption" height="25" align ="Right" width="148"><fmtCOMRSRClosure:message key="LBL_COMRSR"/>:</td>
			<td>
			&nbsp;<html:text property="closeCOMRSRID" maxlength="20" size="30"	onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" style="text-transform:uppercase;" />
			&nbsp;<fmtCOMRSRClosure:message key="BTN_LOAD" var="varLoad"/><gmjsp:button name="Btn_Load" value="${varLoad}" gmClass="button" buttonType="Load" style="height: 20px; width: 5em;" onClick="fnLoadCOMRSR();" />
			</td>
		</tr>
		</table>
		<table border="0" bordercolor="" width="100%" class="DtTable950" cellspacing="0" cellpadding="0">
			<tr bgcolor="#e4e6f2"> 
				<td height="25" colspan="4" class="ShadeRightTableCaption" >&nbsp;<a href="javascript:fnShowFilters('tabQaComHighlights');" tabindex="-1" style="text-decoration:none; color:black">
					<IMG id="tabQaComHighlightsimg" border=0 src="<%=strCollapseImage%>">&nbsp;<fmtCOMRSRClosure:message key="LBL_COMRSR_HIGHLIGHTS"/>:</a>
				</td>
			</tr>
		 <tr style="display:block;" >
			<td colspan="5">
				<table border="0" width="100%" cellspacing="0" cellpadding="0"	id="tabQaComHighlights">
					<tr>
						<td colspan="5">
							<jsp:include page="/gmCOMRSRHighlights.do?method=editIncidentInfo" flush="true">
								<jsp:param name="haction" value="CMPHighlights" />
								<jsp:param name="strOpt"  value="<%=strOpt%>" />
								<jsp:param name="comRsrID" value="<%=strCOMRSRID%>" />
							</jsp:include> 
					</tr>
				</table>
			</td>
		</tr> 
		<tr bgcolor="#e4e6f2">
			<td height="25" colspan="5" class="ShadeRightTableCaption" >&nbsp;<a href="javascript:fnShowFilters('tabClosure');" tabindex="-1" style="text-decoration:none; color:black">
				<IMG id="tabClosureimg" border=0 src="<%=strCollapseImage%>">&nbsp;<fmtCOMRSRClosure:message key="LBL_CLOSURE_DETAILS"/>:</a>
			</td>
		</tr>
		<tr  style="display:block;">
			<td colspan="5">
			<table border="0" bordercolor="red" width="100%" cellspacing="0" cellpadding="0" id="tabClosure">
				<tr class="Shade">
					<td class="RightTableCaption" align="right" width="15%"><fmtCOMRSRClosure:message key="LBL_COMPLAINT_CLOSED_BY"/>:</td>
					<td height="25" width="30%">&nbsp;<gmjsp:dropdown width="200"	controlName="closedBy" SFFormName="frmCOMRSRProcess"
						SFSeletedValue="closedBy" defaultValue="[Choose One]"	SFValue="alEmployeeList" codeId="USERNM" codeName="RUSERNM" /></td>
					<td class="RightTableCaption" align="right" width="20%"><fmtCOMRSRClosure:message key="LBL_COMPLAINTS_CLOSED_DATE"/>:</td>
					<td width="35%">&nbsp;<gmjsp:calendar	SFFormName="frmCOMRSRProcess" controlName="closedDt"
						gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"	onBlur="changeBgColor(this,'#ffffff');" /></td>
				</tr>
				<tr><td class="LLine" colspan="5"></td></tr>
				<tr class="Odd">
					<td class="RightTableCaption" align="right" width="15%"><fmtCOMRSRClosure:message key="LBL_PREVIOUS_ACTIONS"/>:</td>
					<td height="25" colspan="3">&nbsp;<html:textarea property="prevComments" cols="110" style="height:70px"
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"		onblur="changeBgColor(this,'#ffffff');" /></td>
				</tr>
				<tr><td class="LLine" colspan="5"></td></tr>
				<tr class="Shade">
					<td class="RightTableCaption" align="right" width="15%"><fmtCOMRSRClosure:message key="LBL_CLOSURE_COMMENTS"/>:</td>
					<td height="25" colspan="3">&nbsp;<html:textarea	property="closeCmts" cols="110" style="height:70px"
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"	onblur="changeBgColor(this,'#ffffff');" /></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr><td class="LLine" colspan="5"></td></tr>
		<tr><td colspan="6" height="1" class="LLine"></td></tr>
		<tr>
			<td colspan="5" align="center" height="25">
				<jsp:include page="/common/GmIncludeLog.jsp">
				<jsp:param name="FORMNAME" value="frmCOMRSRProcess" />
				<jsp:param name="ALNAME" value="alLogReasons" />
				<jsp:param name="LogMode" value="Edit" />
			</jsp:include></td>
		</tr>
		
		<tr>
		
			<td align="center" height="30" colspan="5" width="15%">
			<%if(!status.equals("102841")){ %> <!-- 102841 status is closed  -->
			
		    <fmtCOMRSRClosure:message key="BTN_RESET" var="varReset"/><gmjsp:button controlId="Btn_Reset" value="${varReset}"  gmClass="button" buttonType="Save" style="width: 6em" onClick="fnonReset();" />
			<fmtCOMRSRClosure:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button controlId="Btn_Submit" value="${varSubmit}"  gmClass="button" buttonType="Save" style="width: 6em" onClick="fnonSubmit();" /> 
			<%} %>
			<%if(status.equals("102841")){ %> <!-- 102841 status is closed  -->
			<fmtCOMRSRClosure:message key="BTN_EMAIL" var="varEmail"/><gmjsp:button controlId="Btn_Email" value="${varEmail}" gmClass="button" buttonType="Save" onClick="fnEmail();" />
			<%}%>
			</td>
		</tr>
		<% if(intResultSize >= 1){%>
		<tr class="shade">
		<td colspan="5" height="25" align="Center" width="23%">
		<gmjsp:dropdown	controlName="chooseAction" SFFormName="frmCOMRSRProcess"
				SFSeletedValue="chooseAction" SFValue="alChooseActionList"	codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" />
				&nbsp;&nbsp;<fmtCOMRSRClosure:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button controlId="Btn_Submit" value="${varSubmit}" buttonType="Save" gmClass="button" style="width: 5em" onClick="fnGo();" />
		</td>
			</tr>
		<% }%>
	</table>
	</td>
	</tr>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>