<%
/**********************************************************************************
 * File		 		: GmQAEvaluation.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: arajan
************************************************************************************/
%>
<!-- quality\complaint\GmQAEvaluation.jsp -->
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtQAEvaluation" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtQAEvaluation:setLocale value="<%=strLocale%>"/>
<fmtQAEvaluation:setBundle basename="properties.labels.quality.complaint.GmQAEvaluation"/>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*"%>

<bean:define id="strOpt" name="frmQAEvaluation" property="strOpt" type="java.lang.String"></bean:define>
<bean:define id="COMRSRId" name="frmQAEvaluation" property="COMRSRId" type="java.lang.String"></bean:define>
<bean:define id="strMessage" name="frmQAEvaluation" property="strMessage" type="java.lang.String"></bean:define>
<bean:define id="strRefType" name="frmQAEvaluation" property="strRefType" type="java.lang.String"></bean:define>
<bean:define id="status" name="frmQAEvaluation" property="status" type="java.lang.String"></bean:define>
<bean:define id="resultSize" name="frmQAEvaluation" property="resultSize" type="java.lang.Integer"></bean:define>

<% 

	String strQualCmplJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_QUALITY_COMPLAINT");
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=UTF-8");
	response.setCharacterEncoding("UTF-8");
	String strWikiTitle = GmCommonClass.getWikiTitle("QA_EVAL");
	String strCollapseImage = "/images/minus.gif";
	String strApplnDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: QA Evaluations </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strQualCmplJsPath%>/GmQAEvaluation.js"></script>
<script type="text/javascript">
var strRefType = '<%=strRefType%>';
var hCOMRSRId = '<%=COMRSRId%>';
var status = '<%=status%>';
var format = '<%=strApplnDateFmt%>';
</script>

</HEAD>
<BODY leftmargin="20" topmargin="10">
<html:form action="/gmCOMRSRSetup.do?method=addQAEvaluation">
<html:hidden property="strOpt" name="frmQAEvaluation"/>
<html:hidden property="hCOMRSRId" name="frmQAEvaluation"/>

<!-- Custom tag lib code modified for JBOSS migration changes -->
<!-- Struts tag lib code modified for JBOSS migration changes -->

<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
	<tr>
		<td>
		
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="3" height="25" class="RightDashBoardHeader">&nbsp;<fmtQAEvaluation:message key="LBL_COM_RSR_TITLE"/></td>
					<td  height="25" class="RightDashBoardHeader">
						<fmtQAEvaluation:message key="IMG_ALT_HELP" var="varHelp" /><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr><td class="LLine" colspan="4"></td></tr>
				<% if (!strMessage.equals("")) { %>
					<tr>
						<td class="RightTableCaption" align="center" HEIGHT="25" colspan="4"> <font color = "Blue"> <%=strMessage%> </font></td>
					</tr>
				<% } %>
				<tr>
					<td class="RightTableCaption" HEIGHT="25" align="right" width="185">
					<fmtQAEvaluation:message key="LBL_COM_RSR" var="varCOM"/><gmjsp:label type="RegularText" SFLblControlName="${varCOM}:" td="false" /> </td>
					<td width="220">&nbsp;<html:text property="COMRSRId" maxlength="20" size="30" styleClass="InputArea" style="text-transform:uppercase;"
						onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
					</td>
					<td colspan="2" align="left" height="30">&nbsp;
				       <fmtQAEvaluation:message key="BTN_LOAD" var="varLoad"/><gmjsp:button name="Btn_Load" value="&nbsp;${varLoad}&nbsp;" buttonType="Load" gmClass="button" onClick="fnLoadQAEval();" />
                    </td>
				</tr>
			</table>
		</td>
	</tr>
	<tr class="ShadeRightTableCaption">
		<td height="25" colspan="4" >&nbsp;<a href="javascript:fnQAShowFilters('tabQaComHighlights');" tabindex="-1" style="text-decoration:none; color:black">
			<IMG id="tabQaComHighlightsimg" border=0 src="<%=strCollapseImage%>">&nbsp;<fmtQAEvaluation:message key="LBL_COM_RSR_HIGHLIGHTS"/>:</a>
		</td>
	</tr>
	<tr>
		<td colspan="6">
			<table border="0" width="100%" cellspacing="0" cellpadding="0" id="tabQaComHighlights" style="display:block">
				<tr> <td>
					<jsp:include page="/gmCOMRSRHighlights.do?method=editIncidentInfo" flush="true">
						<jsp:param name="haction" value="CMPHighlights" />
						<jsp:param name="strOpt" value="<%=strOpt%>" />
						<jsp:param name="comRsrID" value="<%=COMRSRId%>" />
					</jsp:include>
				</td> </tr>
			</table>
		</td>
	</tr> 
	<tr class="ShadeRightTableCaption">
		<td height="25" colspan="4" >&nbsp;<a href="javascript:fnQAShowFilters('tabIniQAEva');" tabindex="-1" style="text-decoration:none; color:black">
			<IMG id="tabIniQAEvaimg" border=0 src="<%=strCollapseImage%>">&nbsp;<fmtQAEvaluation:message key="LBL_INITIAL_QA_EVALUATION"/></a>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" width="100%" cellspacing="0" cellpadding="0" id="tabIniQAEva" style="display:block">
				<tr><td class="LLine" colspan="4"></td></tr>
				<tr class="shade">
					<td class="RightTableCaption" HEIGHT="25" align="right" width="180">
					<fmtQAEvaluation:message key="LBL_DHR_NCMR_REVIEW" var="varDHR"/><gmjsp:label type="RegularText" SFLblControlName="${varDHR}:" td="false" /> </td>
					<td align ="left" colspan="3" height="20%">&nbsp;<html:textarea property="DHRNCMRRev" cols="120" style="height:70px"
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>
				<tr><td class="LLine" colspan="4"></td></tr>	
				<tr>
					<td class="RightTableCaption" align="right" height="25" width="180">
					<fmtQAEvaluation:message key="LBL_CAPA_SCAP_REVIEW" var="varCAPA"/><gmjsp:label type="RegularText" SFLblControlName="${varCAPA}:" td="false" /></td>
					<td align ="left" colspan="3" height="20%">&nbsp;<html:textarea property="CAPASCARRev" cols="120" style="height:70px"
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>
				<tr><td class="LLine" colspan="4"></td></tr>
				<tr class="shade">
					<td class="RightTableCaption" align="right" height="25" width="180">
					<fmtQAEvaluation:message key="LBL_COMPLAINTS_REVIEW" var="varComplaints"/><gmjsp:label type="RegularText" SFLblControlName="${varComplaints}:" td="false" /></td>
					<td align ="left" colspan="3" height="20%">&nbsp;<html:textarea property="compRev" cols="120" style="height:70px"
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>
				<tr><td class="LLine" colspan="4"></td></tr>
				<tr>
					<td class="RightTableCaption" align="right" height="25">
					<fmtQAEvaluation:message key="LBL_REVIEW_COMPLETED_BY" var="varReviewConpletedBy"/><gmjsp:label type="RegularText" SFLblControlName="${varReviewConpletedBy}:" td="false" /></td>
					<td align ="left" >&nbsp;<gmjsp:dropdown width="200" controlName="revCompdBy" SFFormName="frmQAEvaluation"
						SFSeletedValue="revCompdBy" defaultValue= "[Choose One]" SFValue="alRevCompBy" codeId="USERNM" codeName="RUSERNM" />
						
						
					</td>
					<td class="RightTableCaption" align="right" height="25" width="180">
					<fmtQAEvaluation:message key="LBL_DATA_REVIEW_COMPLETED" var="varDataReviewCompleted"/><gmjsp:label type="RegularText" SFLblControlName="${varDataReviewCompleted}:" td="false" /></td>
					<td width="270">&nbsp;<gmjsp:calendar SFFormName="frmQAEvaluation" controlName="dtRevComp" gmClass="InputArea" 
									onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>
				<tr><td class="LLine" colspan="4"></td></tr>
				<tr class="shade">
					<td class="RightTableCaption" align="right" height="25">
					<fmtQAEvaluation:message key="LBL_PART_RECEIVED_DATE" var="varPartReceived"/><gmjsp:label type="RegularText" SFLblControlName="${varPartReceived}:" td="false" /></td>
					<td>&nbsp;<gmjsp:calendar SFFormName="frmQAEvaluation" controlName="dtPartRec" gmClass="InputArea" 
									onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
					</td>
					<td class="RightTableCaption" align="right" height="25" width="180">
					<fmtQAEvaluation:message key="LBL_DATA_SENT" var="varDateSent"/><gmjsp:label type="RegularText" SFLblControlName="${varDateSent}:" td="false" /></td>
					<td width="270">&nbsp;<gmjsp:calendar SFFormName="frmQAEvaluation" controlName="dtEvalSend" gmClass="InputArea" 
									onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr class="ShadeRightTableCaption">
		<td height="25" colspan="4" >&nbsp;<a href="javascript:fnQAShowFilters('tabQAFollowEva');" tabindex="-1" style="text-decoration:none; color:black">
			<IMG id="tabQAFollowEvaimg" border=0 src="<%=strCollapseImage%>">&nbsp;<fmtQAEvaluation:message key="LBL_FOLLOW_UP_QA_EVALUATIONS"/></a>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" width="100%" cellspacing="0" cellpadding="0" id="tabQAFollowEva" style="display:block">
				<tr><td class="LLine" colspan="4"></td></tr>
				<tr class="shade">
					<td class="RightTableCaption" align="left" height="25" width="250">&nbsp;
					<fmtQAEvaluation:message key="LBL_DEVIATIONS_IDENTIFIED_IN_DHR" var="varDeviations"/><gmjsp:label type="RegularText" SFLblControlName="${varDeviations}" td="false" /></td>
					<td>&nbsp;<html:radio property="DHRDev" value="Yes"/> <fmtQAEvaluation:message key="LBL_YES"/> &nbsp;
							<html:radio property="DHRDev" value="No"/> <fmtQAEvaluation:message key="LBL_NO"/> &nbsp;
							<html:radio property="DHRDev" value="N/A"/><fmtQAEvaluation:message key="LBL_N_A"/> 
					</td>
					<td class="RightTableCaption" align="left" height="25" width="250">
					<fmtQAEvaluation:message key="LBL_REP_SURGEON_QUESTIONNAIRE_ANSWERS" var="varRep"/><gmjsp:label type="RegularText" SFLblControlName="${varRep}" td="false" /></td>
					<td>&nbsp;<html:radio property="repSurgAns" value="Yes"/> <fmtQAEvaluation:message key="LBL_YES"/>&nbsp;
							<html:radio property="repSurgAns" value="No"/> <fmtQAEvaluation:message key="LBL_NO"/>&nbsp;
							<html:radio property="repSurgAns" value="N/A"/> <fmtQAEvaluation:message key="LBL_N_A"/>
					</td>
				</tr>
				<tr><td class="LLine" colspan="4"></td></tr>
				<tr>
					<td class="RightTableCaption" align="left" height="25" width="250">&nbsp;
					<fmtQAEvaluation:message key="LBL_PROPER_STORAGE_DISTRIBUTION" var="varProper"/><gmjsp:label type="RegularText" SFLblControlName="${varProper}" td="false" /></td>
					<td>&nbsp;<html:radio property="storage" value="Yes"/> <fmtQAEvaluation:message key="LBL_YES"/> &nbsp;
							<html:radio property="storage" value="No"/> <fmtQAEvaluation:message key="LBL_NO"/> &nbsp;
							<html:radio property="storage" value="N/A"/> <fmtQAEvaluation:message key="LBL_N_A"/>
					</td>
					<td class="RightTableCaption" align="left" height="25" width="250">
					<fmtQAEvaluation:message key="LBL_UTILIZED_PER_PACKAGE_INSERT" var="varUtilized"/><gmjsp:label type="RegularText" SFLblControlName="${varUtilized}" td="false" /></td>
					<td>&nbsp;<html:radio property="utilizedPac" value="Yes"/> <fmtQAEvaluation:message key="LBL_YES"/> &nbsp;
							<html:radio property="utilizedPac" value="No"/> <fmtQAEvaluation:message key="LBL_NO"/> &nbsp;
							<html:radio property="utilizedPac" value="N/A"/> <fmtQAEvaluation:message key="LBL_N_A"/>
					</td>
					
				</tr>
				<tr><td class="LLine" colspan="4"></td></tr>
				<tr class="shade">
					<td class="RightTableCaption" align="left" height="25" width="250">&nbsp;
					<fmtQAEvaluation:message key="LBL_MARKET_ROMOVAL_REQUIRED" var="varMarket"/><gmjsp:label type="RegularText" SFLblControlName="${varMarket}" td="false" /></td>
					<td>&nbsp;<html:radio property="removalReq" value="Yes"/> <fmtQAEvaluation:message key="LBL_YES"/> &nbsp;
							<html:radio property="removalReq" value="No"/> <fmtQAEvaluation:message key="LBL_NO"/> &nbsp;
							<html:radio property="removalReq" value="N/A"/> <fmtQAEvaluation:message key="LBL_N_A"/>
					</td>
					<td class="RightTableCaption" align="left" height="25" width="250">
					<fmtQAEvaluation:message key="LBL_RISK_ANALYSIS_REQUIRED" var="varRisk"/><gmjsp:label type="RegularText" SFLblControlName="${varRisk}" td="false" /></td>
					<td>&nbsp;<html:radio property="riskAnalysis" value="Yes"/> <fmtQAEvaluation:message key="LBL_YES"/> &nbsp;
							<html:radio property="riskAnalysis" value="No"/> <fmtQAEvaluation:message key="LBL_NO"/> &nbsp;
							<html:radio property="riskAnalysis" value="N/A"/> <fmtQAEvaluation:message key="LBL_N_A"/>
					</td>	
				</tr>
				<tr><td class="LLine" colspan="4"></td></tr>
				<tr>
					<td class="RightTableCaption" align="left" height="25" width="250">&nbsp;
					<fmtQAEvaluation:message key="LBL_NCMR_SCAR_GENERATED" var="varNCMR"/><gmjsp:label type="RegularText" SFLblControlName="${varNCMR}" td="false" /></td>
					<td>&nbsp;<html:radio property="NCMRSCARGen" value="Yes"/> <fmtQAEvaluation:message key="LBL_YES"/> &nbsp;
							<html:radio property="NCMRSCARGen" value="No"/> <fmtQAEvaluation:message key="LBL_NO"/> &nbsp;
							<html:radio property="NCMRSCARGen" value="N/A"/><fmtQAEvaluation:message key="LBL_N_A"/>
					</td>
					<td class="RightTableCaption" align="left" height="25" width="250">
					<fmtQAEvaluation:message key="LBL_CORRECTIVE_ACTION_REQUIRED" var="varCorrective"/><gmjsp:label type="RegularText" SFLblControlName="${varCorrective}" td="false" /></td>
					<td>&nbsp;<html:radio property="correctiveAct" value="Yes"/> <fmtQAEvaluation:message key="LBL_YES"/> &nbsp;
							<html:radio property="correctiveAct" value="No"/> <fmtQAEvaluation:message key="LBL_NO"/> &nbsp;
							<html:radio property="correctiveAct" value="N/A"/> <fmtQAEvaluation:message key="LBL_N_A"/>
					</td>
				</tr>
				<tr><td class="LLine" colspan="4"></td></tr>
				<tr class="shade">
					<td class="RightTableCaption" align="left" height="25" width="250">&nbsp;
					<fmtQAEvaluation:message key="LBL_NCMR_SCAR" var="varNCMR"/><gmjsp:label type="RegularText" SFLblControlName="${varNCMR}" td="false" /></td>
					<td>&nbsp;<html:text property="NCMRSCARId" styleClass="InputArea" style="text-transform:uppercase;"
						onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" maxlength="255"/>
					<td class="RightTableCaption" align="left" height="25" width="250">
					<fmtQAEvaluation:message key="LBL_CAPA" var="varCAPA"/><gmjsp:label type="RegularText" SFLblControlName="${varCAPA}" td="false" /></td>
					<td>&nbsp;<html:text property="CAPAId" styleClass="InputArea" style="text-transform:uppercase;"
						onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" maxlength="255"/>
					</td>
				</tr>
				<tr><td class="Line" colspan="4"></td></tr>
				<tr>
					<td colspan="4">
						<table cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td class="RightTableCaption" align="right" height="25" width="180">
								<fmtQAEvaluation:message key="LBL_QA_REVIEW_COMPLETED_BY" var="varQAReview"/><gmjsp:label type="RegularText" SFLblControlName="${varQAReview}:" td="false" /></td>
								<td>&nbsp;<gmjsp:dropdown width="200" controlName="QARevCompBy" SFFormName="frmQAEvaluation"
									SFSeletedValue="QARevCompBy" defaultValue= "[Choose One]" SFValue="alQARevCompBy" codeId="ID" codeName="NAME" />
								</td>
								<td class="RightTableCaption" align="right" height="25" width="180">
								<fmtQAEvaluation:message key="LBL_DATA_COMPLETED" var="varDataCompleted"/><gmjsp:label type="RegularText" SFLblControlName="${varDataCompleted}:" td="false" /></td>
								<td width="270">&nbsp;<gmjsp:calendar SFFormName="frmQAEvaluation" controlName="dtCompleted" gmClass="InputArea" 
									onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="4"> 
			<jsp:include page="/common/GmIncludeLog.jsp" >
				<jsp:param name="FORMNAME" value="frmQAEvaluation" />
				<jsp:param name="ALNAME" value="alLogReasons" />
				<jsp:param name="LogMode" value="Edit" />
			</jsp:include>
		</td>
	</tr>
	<tr>
		<td>
			<table cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td colspan="4" align="center" height="30">&nbsp;
						<%if (!status.equals("102841")) {%>
					       <fmtQAEvaluation:message key="BTN_RESET" var="varReset"/> <gmjsp:button name="Btn_Reset" value="${varReset}" buttonType="Save" gmClass="button" style="width: 6em" onClick="fnQAReset();" />
					       <fmtQAEvaluation:message key="BTN_SUBMIT" var="varSubmit"/> <gmjsp:button name="Btn_Submit" value="${varSubmit}" buttonType="Save" gmClass="button" style="width: 6em" onClick="fnQASubmit();" />
				        <%}%>
				        <%if (strRefType.equals("102790")) {%>
				        	<fmtQAEvaluation:message key="BTN_PRINT" var="varPrint"/><gmjsp:button name="Btn_Print" value="${varPrint}" buttonType="Load" gmClass="button" style="width: 6em" onClick="fnQAPrint();" />
				        <%}%>
                    </td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table cellspacing="0" cellpadding="0" width="100%">
				<tr><td class="LLine" colspan="4"></td></tr>
				<%if (resultSize > 0) {%>
					<tr class="shade">
						<td align="right" height="30" width="60%">&nbsp;
					        <gmjsp:dropdown controlName="chooseAction" SFFormName="frmQAEvaluation"
							SFSeletedValue="chooseAction" defaultValue= "[Choose One]" SFValue="alChooseAction" codeId="CODEID" codeName="CODENM" />&nbsp;
					        
	                    </td>
	                    <td align="left">&nbsp;<fmtQAEvaluation:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button name="Btn_Go" value="${varSubmit}" buttonType="Save" gmClass="button" style="width: 6em" onClick="fnQAGo();" /></td>
					</tr>
				<%} %>
			</table>
		</td>
	</tr>
</table>			
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>