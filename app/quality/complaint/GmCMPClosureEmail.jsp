
<%
	/****************************************************************************************************
	 * File		 		: GmCMPClosureEmail.jsp
	 * Desc		 		: This JSP used to show Closure Email details
	 * Version	 		: 1.0
	 * author			: HReddi
	 ******************************************************************************************************/
%>
<!-- quality\complaint\GmCMPClosureEmail.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtCMPClosureEmail"
	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtCMPClosureEmail:setLocale value="<%=strLocale%>" />
<fmtCMPClosureEmail:setBundle basename="properties.labels.quality.complaint.GmCMPClosureEmail" />
<%@ page import="com.globus.common.beans.GmJasperReport"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<bean:define id="refID" name="frmCOMRSRProcess" property="refID"
	type="java.lang.String">
</bean:define>
<bean:define id="refType" name="frmCOMRSRProcess" property="refType"
	type="java.lang.String">
</bean:define>
<bean:define id="strOpt" name="frmCOMRSRProcess" property="strOpt"
	type="java.lang.String">
</bean:define>
<bean:define id="emailCommnets" name="frmCOMRSRProcess"
	property="emailCommnets" type="java.lang.String">
</bean:define>
<bean:define id="userName" name="frmCOMRSRProcess" property="userName"
	type="java.lang.String">
</bean:define>
<bean:define id="issueBy" name="frmCOMRSRProcess" property="issueBy"
	type="java.lang.String">
</bean:define>
<bean:define id="userDesignation" name="frmCOMRSRProcess"
	property="userDesignation" type="java.lang.String">
</bean:define>
<bean:define id="image_path" name="frmCOMRSRProcess"
	property="image_path" type="java.lang.String">
</bean:define>

<%
String strQualCmplJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_QUALITY_COMPLAINT");
	String strTodaysDate = (String) session
			.getAttribute("strSessTodaysDate") == null
			? ""
			: (String) session.getAttribute("strSessTodaysDate");
	String strApplnDateFmt = GmCommonClass.parseNull((String) session
			.getAttribute("strSessApplDateFmt"));
	String strHeader = "";
	String strHtmlJasperRpt = "";

	if (refType.equals("102788")) {
		strHeader = "CAPA";
	} else if (refType.equals("102789")) {
		strHeader = "SCAR";
	} else if (refType.equals("102790")) {
		strHeader = "COM";
	} else if (refType.equals("102791")) {
		strHeader = "RSR";
	}
	if (!issueBy.equals("")) {
		issueBy = "Dear " + issueBy;
	}
	HashMap hmResult = new HashMap();
	hmResult.put("EMAILCONTENT", emailCommnets);
	hmResult.put("REFTYPE", refType);
	hmResult.put("USERNM", userName);
	hmResult.put("COMPLAINTNM", issueBy);
	hmResult.put("USERDESIG", userDesignation);
	hmResult.put("IMAGE_PATH", image_path);
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Pending COM/RSR Dashboard</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript"
	src="<%=strJsPath%>/jquery.richtextarea.min.js"></script>
<script language="JavaScript"
	src="<%=strQualCmplJsPath%>/GmCMPClosureEmail.js"></script>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");

.ui-richtextarea-content {
	overflow: auto;
	height: 100%;
	width: 100%;
	border-left: 1 solid black;
	border-right: 1 solid black;
	border-top: 1 solid black;
	border-bottom: 1 solid black;
}
</style>

</HEAD>
<BODY leftmargin="20" topmargin="10">
	<html:form action="/gmCOMRSRProcess.do">
		<html:hidden property="strOpt" name="frmCOMRSRProcess" />
		<html:hidden property="refID" name="frmCOMRSRProcess" />
		<html:hidden property="refType" name="frmCOMRSRProcess" />
		<html:hidden property="issueBy" name="frmCOMRSRProcess" />
		<%
			if (!strOpt.equals("Load")) {
		%>
		<html:hidden property="issueType" name="frmCOMRSRProcess" />
		<html:hidden property="toEmail" name="frmCOMRSRProcess" />
		<html:hidden property="ccEmail" name="frmCOMRSRProcess" />
		<html:hidden property="emailSubject" name="frmCOMRSRProcess" />
		<html:hidden property="userName" name="frmCOMRSRProcess" />
		<%
			}
		%>

		<!-- Struts tag lib code modified for JBOSS migration changes -->
		<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" colspan="2" class="RightDashBoardHeader"><%=strHeader%>
					<fmtCMPClosureEmail:message key="LBL_CLOSURE_EMAIL"/></td>
				<td class="RightDashBoardHeader" align="right" colspan=""></td>
				<td align="right" class=RightDashBoardHeader><fmtCMPClosureEmail:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}'
					width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("CLOSURE_EMAIL")%>');" />
				</td>
			</tr>
			<%
				if (strOpt.equals("email")) {
			%>
			<tr>
				<td class="RighttableCaption" colspan="2" align="center"><font
					style="font-weight: bold; font-family: Arial; font-size: x-small; color: blue"><fmtCMPClosureEmail:message
							key="LBL_EMAIL_SENT_SUCCESSFULLY"/> </font></td>
			</tr>
			<%
				}
			%>
			<tr>
				<td width="15%" class="RightTableCaption" align="Right" height="25">&nbsp;<fmtCMPClosureEmail:message
						key="LBL_TRANSACTION_ID"/>:
				</td>
				<td width="85%" colspan="3">&nbsp;<bean:write
						name="frmCOMRSRProcess" property="refID" /></td>
			</tr>
			<tr>
				<td colspan="5" class="LLine" height="1"></td>
			</tr>
			<tr class="shade">
				<td class="RightTableCaption" align="right" width="20%"><fmtCMPClosureEmail:message
						key="LBL_TO"/>:</td>
				<%
					if (strOpt.equals("preview") || strOpt.equals("email")) {
				%>
				<td align="left" colspan="3" height="25">&nbsp;<bean:write
						name="frmCOMRSRProcess" property="toEmail" /></td>
				<%
					} else {
				%>
				<td align="left" colspan="3">&nbsp;<html:textarea
						property="toEmail" cols="90" style="height:25px"
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');" />
				</td>
				<%
					}
				%>
			</tr>
			<tr>
				<td colspan="5" class="LLine" height="1"></td>
			</tr>
			<tr>
				<td class="RightTableCaption" align="right"><fmtCMPClosureEmail:message
						key="LBL_CC"/>:</td>
				<%
					if (strOpt.equals("preview") || strOpt.equals("email")) {
				%>
				<td align="left" colspan="3" height="25">&nbsp;<bean:write
						name="frmCOMRSRProcess" property="ccEmail" /></td>
				<%
					} else {
				%>
				<td align="left" colspan="3">&nbsp;<html:textarea
						property="ccEmail" cols="90" style="height:25px"
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');" />
				</td>
				<%
					}
				%>
			</tr>
			<tr>
				<td colspan="5" class="LLine" height="1"></td>
			</tr>
			<tr class="shade">
				<td class="RightTableCaption" align="right"><fmtCMPClosureEmail:message
						key="LBL_SUBJECT"/>:</td>
				<%
					if (strOpt.equals("preview") || strOpt.equals("email")) {
				%>
				<td align="left" colspan="3" height="25">&nbsp;<bean:write
						name="frmCOMRSRProcess" property="emailSubject" /></td>
				<%
					} else {
				%>
				<td align="left" colspan="3">&nbsp;<html:textarea
						property="emailSubject" cols="90" style="height:25px"
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');" />
				</td>
				<%
					}
				%>
			</tr>
			<tr>
				<td colspan="5" class="LLine" height="1"></td>
			</tr>
			<tr>
				<%
					if (strOpt.equals("preview") || strOpt.equals("email")) {
				%>
				<td class="RightTableCaption" align="right"></td>
				<td align="left">
					<%
						String strJasperPath = GmCommonClass
										.getString("GMJASPERLOCATION");
								GmJasperReport gmJasperReport = new GmJasperReport();
								gmJasperReport.setRequest(request);
								gmJasperReport.setResponse(response);
								gmJasperReport
										.setJasperReportName("/GmCMPClosureEmail.jasper");
								gmJasperReport.setHmReportParameters(hmResult);
								gmJasperReport.setReportDataList(null);
								gmJasperReport.setPageHeight(40);
								gmJasperReport.setBlDisplayImage(true);
								strHtmlJasperRpt = gmJasperReport.getHtmlReport();
					%> <%=strHtmlJasperRpt%>
				</td>
				<%
					} else {
				%>
				<td class="RightTableCaption" align="right"><fmtCMPClosureEmail:message
						key="LBL_EVALUATION_COMMENTS"/>:&nbsp;</td>
				<td align="left" colspan="3"><textarea name="emailCommnets"
						id="emailCommnets" rows="20" cols="92" styleClass="InputArea"
						onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');"><bean:write
							name="frmCOMRSRProcess" property="emailCommnets" /></textarea></td>
			</tr>
			<%
				}
			%>
			<tr>
				<td colspan="5" class="LLine" height="1"></td>
			</tr>
			<tr>
				<%
					if (strOpt.equals("preview")) {
				%>
				<td colspan="5" align="center" height="30">&nbsp; <fmtCMPClosureEmail:message
						key="BTN_SEND" var="varSend" />
					<gmjsp:button name="Btn_Send" value="${varSend}" gmClass="button"
						style="width: 6em" buttonType="Save" onClick="fnSend();" /> <fmtCMPClosureEmail:message
						key="BTN_CLOSE" var="varClose" />
					<gmjsp:button name="Btn_Close" value="${varClose}" gmClass="button"
						style="width: 6em" buttonType="Load" onClick="fnClose();" />
				</td>
				<%
					} else if (strOpt.equals("email")) {
				%>
				<td colspan="5" align="center" height="30">&nbsp; <fmtCMPClosureEmail:message
						key="BTN_CLOSE" var="varClose" />
					<gmjsp:button name="Btn_Close" value="${varClose}" gmClass="button"
						style="width: 6em" buttonType="Load" onClick="fnClose();" />
				</td>
				<%
					} else {
				%>
				<td colspan="5" align="center" height="30">&nbsp; <fmtCMPClosureEmail:message
						key="BTN_RESET" var="varReset" />
					<gmjsp:button name="Btn_Reset" value="${varReset}" gmClass="button"
						style="width: 6em" buttonType="Save" onClick="fnReset();" /> <fmtCMPClosureEmail:message
						key="BTN_PREVIEW" var="varPreview" />
					<gmjsp:button name="Btn_Preview" value="${varPreview}"
						gmClass="button" style="width: 6em" buttonType="Load"
						onClick="fnPreview();" /> <fmtCMPClosureEmail:message
						key="BTN_CANCEL" var="varCancel" />
					<gmjsp:button name="Btn_Cancel" value="${varCancel}"
						gmClass="button" style="width: 6em" buttonType="Save"
						onClick="fnCancel();" />
				</td>
				<%
					}
				%>
			</tr>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
<script type="text/javascript">
	$("#emailCommnets").richtextarea();
</script>
</HTML>

