<!-- quality\GmPartNumberAssemblyUplad.jsp -->
 <%
/**********************************************************************************
 * File		 		: GmPartNumberAssemblyUplad.jsp
 * Desc		 		: This screen is used for the Part Number frmPartSetuptenance
 * Version	 		: 1.0
 * author			: rajan
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%> 
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.net.URLEncoder" %>
<%@ taglib prefix="fmtPartNumberAssembly" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<bean:define id="bomId" name="frmPartNumberAssembly" property="bomId" type="java.lang.String"> </bean:define>
<bean:define id="alSubComp" name="frmPartNumberAssembly" property="alSubComp" type="java.util.ArrayList"> </bean:define>
<bean:define id="alPartType" name="frmPartNumberAssembly" property="alPartType" type="java.util.ArrayList"> </bean:define>
<bean:define id="strPTMAPHtml" name="frmPartNumberAssembly" property="strPTMAPHtml" type="java.lang.String"></bean:define>	
<bean:define id="alLogReasons" name="frmPartNumberAssembly" property="alLogReasons" type="java.util.ArrayList"> </bean:define>

<fmtPartNumberAssembly:setLocale value="<%=strLocale%>"/>
<fmtPartNumberAssembly:setBundle basename="properties.labels.quality.GmPartNumberAssemblyUplad"/>
<%

String strQualJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_QUALITY");
String strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("PARTNUMBER_BOM_MAPPING"));

// to add the sub-component drop down values
HashMap hmPartType = new HashMap();

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Part Number - Bulk Sub Component Mapping </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/fonts/font_roboto/roboto.css"/>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.css">
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<%-- <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxCombo.js"></script> --%>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>

 <script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx_deprecated.js"></script> 
<script language="JavaScript" src="<%=strQualJsPath%>/GmPartNumberAssemblyUpload.js"></script>



<script type="text/javascript">
var lblPartNumber='<fmtPartNumberAssembly:message key="LBL_PART_NUM"/>';
var lblComments='Comments';
var PTMAPArr = new Array(1);
<%=strPTMAPHtml%>;

var arrayPartType = new Array();

//dropdown values for sterilization method
<%for (int i = 0; i < alPartType.size(); i++) {
	hmPartType = (HashMap) alPartType.get(i);%>
	arrayPartType.push('<%=hmPartType.get("CODEID")%>'+","+'<%=hmPartType.get("CODENM")%>');
<%}%>

</script>
 <style type="text/css">
         table.DtTable850 {
             margin: 0 0 0 0;
             width: 850px;
             border: 1px solid #676767;
         }
.even{
	background-color:#ffffff !important;
}
.uneven{
background-color:#dcedfc !important;
}
		
div.gridbox table.hdr td {
   color: #000000 !important;
   background-color: #d2e9fc !important;
}
div.gridbox_material.gridbox .xhdr {
    background:#dcedfc !important;
}
</style>

</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<html:form action="gmPartNumberAssembly.do?method=loadPartNumberAssemblyUploadDtls">
<html:hidden property="inputString" name ="frmPartNumberAssembly"/>
<html:hidden property="strPartNumbers" name ="frmPartNumberAssembly"/>
<html:hidden property="voidinputstring" name ="frmPartNumberAssembly"/>
<html:hidden property="bomId" name ="frmPartNumberAssembly" styleId="bomId" />

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
		
			<td height="25" class="RightDashBoardHeader" colspan="4">&nbsp;<fmtPartNumberAssembly:message key="LBL_HEADER"/></td>
			<td class="RightDashBoardHeader"><fmtPartNumberAssembly:message key="LBL_HELP" var = "varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
		</tr>		
		<tr class='shade'>
			<td class="RightTableCaption" width="300" align="right" height="25" ><font color="red">*</font>&nbsp;<fmtPartNumberAssembly:message key="LBL_ACTION"/>&nbsp;:&nbsp;</td>					
			<td width="200">
				<gmjsp:dropdown controlName="subComponent" controlId="subComponent" SFFormName="frmPartNumberAssembly" SFSeletedValue="subComponent"
				SFValue="alSubComp" codeId="CODEID"  codeName="CODENM" onChange="fnChangeAction ();" /> 
			</td>
			<td class="RightTableCaption" align="right" width="80" height="25">&nbsp;</td>
			<td width="230">&nbsp;
			</td>
			<td></td>
		</tr>	
		<tr>
			<td class="RightTableCaption" align="right" height="25" width="250"><font color="red">*</font>&nbsp;<fmtPartNumberAssembly:message key="LBL_PART_NUM"/>&nbsp;:&nbsp;</td>		
			<td width="250">
			<html:text size="20" styleClass="InputArea"  name="frmPartNumberAssembly"  property="partNumber" maxlength="20" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" onchange="fnCheckPartNum(this.value);"/>
			<span id="DivShowPartAvail" style="vertical-align: middle; display: none;"> <img height="24" width="25" title="Part# available" src="<%=strImagePath%>/success.gif"></img></span>
			<span id="DivShowPartExists" style="vertical-align: middle; display: none;"> <img title="Part# Invalid" src="<%=strImagePath%>/delete.gif"></img> </span>
			</td>
			<td class="RightTableCaption" align="right" height="25" width="80">&nbsp;</td>
			<td width="380">&nbsp;</td>
			<td></td>
		</tr>
		
		<tr class='shade'>
			<td class="RightTableCaption" align="right" height="25" width="250">&nbsp;<fmtPartNumberAssembly:message key="LBL_PART_DESC"/>&nbsp;:&nbsp;</td>		
			<td width="250" id="partDesc">&nbsp;
			</td>
			<td class="RightTableCaption" align="right" height="25" width="250">&nbsp;<fmtPartNumberAssembly:message key="LBL_BOM_ID"/>&nbsp;:&nbsp;</td>
			<td width="250" id="bomid">
			<td width="250">&nbsp;</td>
			
		</tr>
		<tr>
			<td class="RightTableCaption" align="right" height="25" width="250">&nbsp;<fmtPartNumberAssembly:message key="LBL_FAMILY"/>&nbsp;:&nbsp;</td>		
			<td width="250" id="prodFamily">&nbsp;
			</td>
			<td class="RightTableCaption" align="right" height="25" width="250">&nbsp;<fmtPartNumberAssembly:message key="LBL_STERILITY"/>&nbsp;:&nbsp;</td>
			<td width="250" id="prodClass"></td>
			<td width="250">&nbsp;
			</td>
		</tr>
		<tr><td class="LLine" colspan="5" height="1" border="1px solid #666666"></td></tr>
		<tr style="display: none" id="trImageShow" class="Shade" height="25">
		<td colspan="4" width="1250">
		<table>	
				<tr  height="25">		
						<td  width="28" colspan="1" tabIndex="-1"><fmtPartNumberAssembly:message key="IMG_ALT_ADD_ROW" var="varAddRow"/><a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="${varAddRow}" style="border: none;" onClick="javascript:addRow()"height="14"></a></td>
						<td  width="28" colspan="1" tabIndex="-1"><fmtPartNumberAssembly:message key="IMG_ALT_REMOVE_ROW" var="varRemoveRow"/><a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/delete.gif" alt="${varRemoveRow}" style="border: none;" onClick="javascript:removeSelectedRow()" height="14"></a></td>
						<td  width="28" colspan="1" tabIndex="-1"><fmtPartNumberAssembly:message key="IMG_ALT_ADD_ROWS_CLIPBOARD" var="varAddRowsClipboard"/><a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/row_add_after_1.gif" alt="${varAddRowsClipboard}" style="border: none;" onClick="javascript:fnAddRowFromClipboard()" height="14"></a></td>					
						<td  colspan="2" align="right" >
						<div id="progress" style="display:none;"><img src="<%=strImagePath%>/success_y.gif" height="15"> </div></td>
						<td><div id="msg"  align="center"  style="display:none;color:green;font-weight:bold;"></div> 
						<div id="erromsg" align="center" style="display:none; color:red;font-weight:bold;"></div>
					</td>
			   </tr>	
		</table>
		</td>
		<td></td>
		</tr>
		<tr><td class="LLine" colspan="5" height="1"></td></tr>
		<tr id="trDiv">
              <td colspan="5">
	         <div id="dataGridDiv" height="400px"></div></td>
        </tr> 
        <tr><td class="LLine" colspan="5" height="1"></td></tr>
       <tr>
				   <td colspan="5" > 
							<jsp:include page="/common/GmIncludeLog.jsp" >
							<jsp:param name="FORMNAME" value="frmPartNumberAssembly" /> 
							<jsp:param name="ALNAME" value="alLogReasons" />
							<jsp:param name="LogMode" value="Edit" />
							<jsp:param name="Mandatory" value="yes" />	
							</jsp:include> 
					</td>
		 </tr>
		 <tr><td class="LLine" colspan="4" height="1"></td></tr>
		  <tr id="trAddOrUpdate">
		     <td class="RightTableCaption" align="right" height="25" width="250" colspan="2">&nbsp;<fmtPartNumberAssembly:message key="LBL_ADD_UPDATE_BOM"/>:</td>
		     <td width="80" colspan="3">&nbsp;
	        	<html:checkbox property="bomFlag" styleId="bomFlag" name="frmPartNumberAssembly" /> 
		     </td>
		  </tr>
		  <tr style="display: none" id="buttonshow" height="30"><td colspan="5"  align="center">
          <div id="buttonDiv">            
	            <fmtPartNumberAssembly:message key="BTN_SUBMIT" var="varSave"/>
	            <gmjsp:button value="${varSave}" gmClass="button"  name="BTN_SUBMIT" onClick="fnSubmit();"  buttonType="Save"/>			
		  </div>
			</td>
		</tr>
	</table>
	</html:form>	

 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

