 <%
/*******************************************************************************************************
 * File		 		: GmSetBundleRptDetails.jsp
 * Desc		 		: This JSP an included jsp to show the detiails section Set Bundle Mapping Report
 * Version	 		: 1.0
 * author			: Agilan
*********************************************************************************************************/
%>

<%@ page language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.Date"%>
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtSetBundleRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- quality\GmSetBundleRptDetails.jsp -->
<fmtSetBundleRpt:setLocale value="<%=strLocale%>"/>
<fmtSetBundleRpt:setBundle basename="properties.labels.quality.GmSetBundleReport"/>

<bean:define id="alRptDetails" name="frmSetBundleRpt" property="alRptDetails" type="java.util.List"> </bean:define>

<%
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
int intLength = alRptDetails.size();
int intSize = 0;
int intCount = 0;
String strLine = "";
String strBundleName = "";
String strProjectName = "";
String strType = "";
String strBundleNm = "";
String strBundleId = "";
String strSetId = "";
String strInitDt = "";
String strSetName = "";
String strDivision = "";
String dtImpDt = "";
String strImpDt = "";
String strStatus = "";
//String strCount = "";
String strUpdatedBy = "";
Date strUpdatedDate = null;
String strShade = "";
String strCallFlag = "";
int intAllCnt = 0;
String strAccName = "";
String strAccountIds = "";
boolean blFlag = false;

String strSubmitAccess = GmCommonClass.parseNull((String)request.getAttribute("SUBMITACCFL"));
String strBtnDisable = "false";
if(!strSubmitAccess.equals("Y")){
	strBtnDisable = "true";
} 
 

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Globus Medical: Set Bundle Report Details</title>
</head>
<body leftmargin="20" topmargin="10">

	<table border="0" width="100%" cellspacing="0" cellpadding="0">
		<tr><td colspan="8" height="1" bgcolor="#cccccc"></td></tr>
			<tr>
				<td colspan="9">
					<table border="0"  width="100%" cellspacing="0" cellpadding="0">
						<tr class="ShadeRightTableCaption">
							<!-- <td width="5%" height="24">Select</td> -->
							<td width="10%"><fmtSetBundleRpt:message key="LBL_SET_BUNDLE_NAME"/></td>
							<td width="12%"><fmtSetBundleRpt:message key="LBL_SET_ID"/></td>
							<td width="12%" align="center"><fmtSetBundleRpt:message key="LBL_SET_NAME"/></td>
							<td width="10%"><fmtSetBundleRpt:message key="LBL_PROJECT_NAME"/></td>							
							<td width="14%" align="center"><fmtSetBundleRpt:message key="LBL_DIVISION"/></td>
							<td width="13%"><fmtSetBundleRpt:message key="LBL_Type"/></td>
							<td width="13%"><fmtSetBundleRpt:message key="LBL_STATUS"/></td>
							<!-- <td width="10%"># of Parts</td> -->
							<td width="10%"><fmtSetBundleRpt:message key="LBL_UPDATED_BY"/></td>
							<td width="8%"><fmtSetBundleRpt:message key="LBL_UPDATED_DATE"/></td>
						</tr>
 <%
						if (intLength > 0)
						{
							HashMap hmLoop = new HashMap();
							HashMap hmTempLoop = new HashMap();						
							intSize = alRptDetails.size();
							hmTempLoop = new HashMap();
							strLine = "";
							blFlag = false;
	
							for (int i = 0;i < intSize ;i++ ){
								hmLoop = (HashMap)alRptDetails.get(i);
								if (i<intSize-1){
									hmTempLoop = (HashMap)alRptDetails.get(i+1);
									log.debug("hmTempLoop==>"+hmTempLoop);
									strBundleName = GmCommonClass.parseNull((String)hmTempLoop.get("SETBUNDLENM"));
								}
								strBundleId = GmCommonClass.parseNull((String)hmLoop.get("SETBUNDLEID"));
								strBundleId = strBundleId.substring(3,strBundleId.length());
								strBundleNm = GmCommonClass.parseNull((String)hmLoop.get("SETBUNDLENM"));
								strSetId = GmCommonClass.parseNull((String)hmLoop.get("SETID"));
								strSetName = GmCommonClass.parseNull((String)hmLoop.get("SETNAME"));	
								strProjectName = GmCommonClass.parseNull((String)hmLoop.get("SETNAME"));
								strDivision = GmCommonClass.parseNull((String)hmLoop.get("DIVNAME"));
								strType = GmCommonClass.parseNull((String)hmLoop.get("SETYPE"));
								strStatus = GmCommonClass.parseNull((String)hmLoop.get("STATUS"));
								strUpdatedBy = GmCommonClass.parseNull((String)hmLoop.get("UPDATEDBY"));
								strUpdatedDate = (Date)hmLoop.get("UPDATEDDATE");
								if (strBundleNm.equals(strBundleName)){
									intCount++;
									strLine = "<TR><TD colspan=10 height=1 class=Line></TD></TR>";
								}else{
									strAccountIds = strAccountIds+strBundleNm+',';
									strLine = "<TR><TD colspan=10 height=1 class=Line></TD></TR>";
									if (intCount > 0){
										strLine = "";
									}else{
										blFlag = true;
									}
									intCount = 0;
								}
								
								if(i == intSize-1 && intSize != 1){ 
								  strAccountIds = strAccountIds+strBundleNm+',';
								}
	
								if (intCount > 1){
									//strRepNm = "";
									strLine = "";
								}
							
								strShade	= (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows
		
								out.print(strLine);	
								if (intCount == 1 || blFlag){
%>
	
						<tr id="tr<%=strBundleId%>">		
							<td class="RightText" colspan="4" height="20">
							<fmtPartPriceAdjDetails:message key="LBL_CLICK_TO_EXPAND" var="varClickToExpand"/>
							&nbsp;<A class="RightText" title="click to extends" href="javascript:Toggle('<%=strBundleId%>')"><%=strBundleNm%></td>
							<td class="RightText"  colspan="2" height="20"><%=strDivision%> </td>
							<td class="RightText"  height="20"><%=strStatus%> </td>
							<td class="RightText" height="20"><%=strUpdatedBy%> </td>
							<td class="RightText" height="20"><%=strUpdatedDate%> </td>
						
						</tr>
							<tr>
								<td colspan="10"><div style="display:none" id="div<%=strBundleId%>">
									<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tab<%=strBundleId%>">
<%
									intAllCnt++;
									blFlag = false;
									}
%>					<TR <%=strShade%>>
						
						<td width="20%" colspan="1" class="RightText" align="center">&nbsp;<%=strSetId%></td>
						<td width="12%" class="RightText" align="center">&nbsp;<%=strSetName%></td>
						<td width="10%" class="RightText" align="left">&nbsp;<%=strProjectName%></td>	
						<td width="14%" class="RightText" align="center">&nbsp;</td> 					
						<td width="13%" class="RightText" align="left">&nbsp;<%=strType%></td>	
						<td width="10%" class="RightText" align="left">&nbsp;</td> 
					    <td width="10%" class="RightText" align="left">&nbsp;</td>	
						<td width="10%" class="RightText" align="left">&nbsp;</td>
								
					</TR>
									<tr><td colspan="10" height="1" bgcolor="#eeeeee"></td></tr>
<%					
								if ( intCount == 0 || i == intSize-1)
								{
%>
								</table></div>
							</td>
						</tr>
<%
								}
								
							} // End of For
								 
						}else{
%>									<tr>
										<td colspan="10" align="center" class="RightTextRed"><fmtSetBundleRpt:message key="LBL_NO_RECORDS"/> !</td>
									</tr>
<%
						} 
%> 
					</TABLE>
				</TD>
			</tr>

	</table>
	
<%@ include file="/common/GmFooter.inc"%>

</body>
</html>