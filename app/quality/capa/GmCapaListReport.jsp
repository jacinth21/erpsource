<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
/**********************************************************************************
 * File		 		: GmCapaListReport.jsp
 * Desc		 		: Screen to Modify CAPA
 * Version	 		: 1.0
 * author			: arajan
************************************************************************************/
%>
<!-- quality\capa\GmCapaListReport.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtCapaListReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtCapaListReport:setLocale value="<%=strLocale%>"/>
<fmtCapaListReport:setBundle basename="properties.labels.quality.capa.GmCapaListReport"/>

<bean:define id="gridData" name="frmCapaListReport" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="intResultSize" name="frmCapaListReport" property="intResultSize" type="java.lang.Integer"> </bean:define>
<bean:define id="submitdDtFrom" name="frmCapaListReport" property="submitdDtFrom" type="java.lang.String"> </bean:define>
<bean:define id="submitdDtTo" name="frmCapaListReport" property="submitdDtTo" type="java.lang.String"> </bean:define>
<bean:define id="accessFl" name="frmCapaListReport" property="accessFl" type="java.lang.String"> </bean:define>
 
<%    
String strQualityCapaJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_QUALITY_CAPA");
response.setContentType("text/html; charset=UTF-8");
response.setCharacterEncoding("UTF-8");
String strWikiTitle = "";
String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
String strTitle = "";
GmResourceBundleBean gmResourceBundleBeanlbl = 
GmCommonClass.getResourceBundleBean("properties.labels.quality.capa.GmCapaListReport", strSessCompanyLocale);
if(accessFl.equals("true")){ // to get whether modify CAPA or CAPA report
	strTitle =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_MODIFY_GENERATE_CAPA_REPORT"));
	strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("MODIFY_CAPA"));
}else{
	strTitle =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_CAPA_REPORT"));
	strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("CAPA_REPORT"));
}

%>
 
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Globus Medical: Modify and Report CAPA</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid_form.js "></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strQualityCapaJsPath%>/GmCapaListReport.js"></script>

<script>
	var objGridData;
	var format = '<%=strApplDateFmt%>';
	objGridData = '<%=gridData%>';
	var accessFl = '<%=accessFl%>';
	var lblelapseddays ='<fmtCapaListReport:message key="LBL_ELAPSED_DAYS"/>';
</script>

</HEAD>

<!-- Custom tag lib code modified for JBOSS migration changes -->

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();" onkeypress="fnEnter();">
<html:form action="/gmCapaListReport.do?method=listCapaDetails">
<html:hidden property="strOpt" name="frmCapaListReport"/>
<html:hidden property="hCapaID" name="frmCapaListReport"/>
<html:hidden property="hDisplayNm" name="frmCapaListReport"/>
<html:hidden property="hRedirectURL" name="frmCapaListReport"/>
<html:hidden property="hStatusId" name="frmCapaListReport"/>
<html:hidden property="accessFl" name="frmCapaListReport"/>

<table border="0" class="DtTable1030" cellspacing="0" cellpadding="0" >
	<tr>
		<td>
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
					
				<tr>
					<td colspan="2" height="25" class="RightDashBoardHeader">&nbsp;<%=strTitle%></td>
					<td  height="25" class="RightDashBoardHeader">
						<fmtCapaListReport:message key="IMG_ALT_HELP" var="varHelp" /><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>
				</tr>
			</table>
		</td>				
	</tr>
	
	<tr>
		<td>
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr><td class="LLine" colspan="4"></td></tr>
				<tr>
					<td class="RightTableCaption" HEIGHT="25" align="right" width="10%">
					<fmtCapaListReport:message key="LBL_CAPA" var="varCAPA"/><gmjsp:label type="RegularText" SFLblControlName="${varCAPA}:" td="false" /> </td>
					<td width="15%">&nbsp;
						<html:text property="capaID" styleClass="InputArea" name="frmCapaListReport"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="1"/>
					</td>
					
					<td class="RightTableCaption" HEIGHT="25" align="right" width="10%">
					<fmtCapaListReport:message key="LBL_TYPE" var="varType"/><gmjsp:label type="RegularText" SFLblControlName="${varType}:" td="false" /> </td>
					<td width="15%">&nbsp;
						<gmjsp:dropdown controlName="type" SFFormName="frmCapaListReport" SFSeletedValue="type"
						SFValue="alMfgType" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" tabIndex="2"/>
					</td>
				</tr>
				<tr><td class="LLine" colspan="4"></td></tr>
				<tr class="Shade">
					<td class="RightTableCaption" HEIGHT="25" align="right" width="10%">
					<fmtCapaListReport:message key="LBL_REQUESTED_BY" var="varRequestedBy"/><gmjsp:label type="RegularText" SFLblControlName="${varRequestedBy}:" td="false" /> </td>
					<td width="15%">&nbsp;
						<gmjsp:dropdown controlName="requestedBy" SFFormName="frmCapaListReport" SFSeletedValue="requestedBy"
						SFValue="alRequestedBy" codeId = "ID"  codeName = "NAME" defaultValue= "[Choose One]" tabIndex="3"/>
					</td>
					
					<td class="RightTableCaption" HEIGHT="25" align="right" width="10%">
					<fmtCapaListReport:message key="LBL_STATUS" var="varStatus"/><gmjsp:label type="RegularText" SFLblControlName="${varStatus}:" td="false" /> </td>
					<td width="15%">&nbsp;
						<gmjsp:dropdown controlName="status" SFFormName="frmCapaListReport" SFSeletedValue="status"
						SFValue="alStatus" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" tabIndex="4"/>
					</td>
				</tr>
				<tr><td class="LLine" colspan="4"></td></tr>
				<tr>
					<td class="RightTableCaption" HEIGHT="25" align="right" width="10%">
					<fmtCapaListReport:message key="LBL_REFERENCE" var="varReference"/><gmjsp:label type="RegularText" SFLblControlName="${varReference}:" td="false" /> </td>
					<td width="15%">&nbsp;
						<html:text property="part" styleClass="InputArea" name="frmCapaListReport"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="5"/>
					</td>
					
					<td class="RightTableCaption" HEIGHT="25" align="right" width="10%">
					<fmtCapaListReport:message key="LBL_LOT" var="varLot"/><gmjsp:label type="RegularText" SFLblControlName="${varLot}:" td="false" /> </td>
					<td width="15%">&nbsp;
						<html:text property="lot" styleClass="InputArea" name="frmCapaListReport"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="6"/>
					</td>
				</tr>
				<tr><td class="LLine" colspan="4"></td></tr>
				<tr class="Shade">
					<td class="RightTableCaption" HEIGHT="25" align="right" width="10%">
					<fmtCapaListReport:message key="LBL_PROJECT" var="varProject"/><gmjsp:label type="RegularText" SFLblControlName="${varProject}:" td="false" /> </td>
					<td width="15%">&nbsp;
						<html:text property="projectID" styleClass="InputArea" name="frmCapaListReport"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="7"/>
					</td>
					<td class="RightTableCaption" HEIGHT="25" align="right" width="10%">
					<fmtCapaListReport:message key="LBL_ELAPSED_DAYS" var="varElapsedDays"/><gmjsp:label type="RegularText" SFLblControlName="${varElapsedDays}:" td="false" /> </td>
					<td width="15%">&nbsp;
						<gmjsp:dropdown controlName="elapsedDys" SFFormName="frmCapaListReport" SFSeletedValue="elapsedDys"
						SFValue="alElapsedDys" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" tabIndex="8"/>
						&nbsp;&nbsp;
						<html:text property="elapsedDysVal" styleClass="InputArea" name="frmCapaListReport" size="3" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="9"/>
					</td>
				</tr>
				<tr><td class="LLine" colspan="4"></td></tr>
				<tr>
					<td colspan="4">
						<table border="0" width="100%" cellspacing="0" cellpadding="0" height="30">
							<tr>
								<td class="RightTableCaption" HEIGHT="25" align="right" width="20%">
									<fmtCapaListReport:message key="LBL_CAPA_SUBMITTED_DATE_RANGE" var="varCAPASubmittedDateRange"/><gmjsp:label type="RegularText" SFLblControlName="${varCAPASubmittedDateRange}:" td="false"/> </td>
								<td class="RightTableCaption" HEIGHT="25" align="left" colspan="1" width="50%">&nbsp;
									<fmtCapaListReport:message key="LBL_FROM" var="varFrom"/><gmjsp:label type="RegularText" SFLblControlName="${varFrom}:" td="false" />&nbsp;
									<gmjsp:calendar SFFormName="frmCapaListReport" controlName="submitdDtFrom" gmClass="InputArea" textValue="<%=(submitdDtFrom.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(submitdDtFrom,strApplDateFmt).getTime())))%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="10"/>
									&nbsp;	
									<fmtCapaListReport:message key="LBL_TO" var="varTo"/><gmjsp:label type="RegularText" SFLblControlName="${varTo}:" td="false" />					
									<gmjsp:calendar SFFormName="frmCapaListReport" controlName="submitdDtTo" gmClass="InputArea" textValue="<%=(submitdDtTo.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(submitdDtTo,strApplDateFmt).getTime())))%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="11"/>
								</td>
								<td></td>
								<td>&nbsp;
									<fmtCapaListReport:message key="BTN_LOAD" var="varLoad"/><gmjsp:button name="Btn_Load" value="${varLoad}" gmClass="button" buttonType="Load" onClick="fnReload();" style="width: 5em; height: 23px; font-family: Trebuchet MS, arial, sans-serif;font-size : 9pt" tabindex="12"/>
								</td>
							</tr>
						</table>
					</td>				
				</tr>
				<tr><td class="LLine" colspan="4"></td></tr>
				<tr>
					<%if( intResultSize >= 1) {%>
						<td colspan="4">
								<table border="0" cellspacing="0" cellpadding="0" width="100%">
									<tr> 
										<td colspan="2">
											<div id="dataGridDiv" class="grid" height="500px"></div>
										</td>
									</tr>
									<%if(accessFl.equals("true")){%> <!-- Only for Modify, Chooose action is needed -->		 	
										<tr>
											<td align="left" height="30" width="40%">	
												<div class='exportlinks'><fmtCapaListReport:message key="DIV_EXPORT_OPT"/>: <img src='img/ico_file_excel.png' /> <a href="#" onclick="javascript:fnExportExcel();"><fmtCapaListReport:message key="DIV_EXCEL"/>  </a>                        
									    		</div>	
											</td>								
											<td height="30">
												<gmjsp:dropdown controlName="chooseAction" SFFormName="frmCapaListReport" SFSeletedValue="chooseAction"
												SFValue="alChooseAction" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose Action]" tabIndex="13"/>
												&nbsp;&nbsp;&nbsp;&nbsp;
												<fmtCapaListReport:message key="BTN-SUBMIT" var="varSubmit"/><gmjsp:button name="Btn_Submit" value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnChooseAction()" style="width: 5em; height: 23px; font-family: Trebuchet MS, arial, sans-serif;font-size : 9pt" tabindex="14"/>
											</td>
							   			</tr>
						   			<%}else{ %>
							   			<tr>
												<td align="center" height="30" width="100%">	
													<div class='exportlinks'><fmtCapaListReport:message key="DIV_EXPORT_OPT"/>: <img src='img/ico_file_excel.png' /> <a href="#" onclick="javascript:fnExportExcel();"><fmtCapaListReport:message key="DIV_EXCEL"/>  </a>                        
										    		</div>	
												</td>
										</tr>
									<%} %>		
						   			
						   			<tr><td class="LLine" height="1" colspan="2"></td></tr>     			 	 				      
								</table> 
						</td>
					<%}else{%>
							<td height="25" colspan="4" align="center"><font color="blue"><fmtCapaListReport:message key="LBL_NO_DATA"/></font></td>
					<%} %>  
				</tr>
			</table>
		</td>
	</tr>
	
</table>			
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>