<%
/**********************************************************************************
 * File		 		: GmIncludeCAPAHighlights.jsp
 * Desc		 		: Include in Upload for COMPLAINT SCREEN
 * Version	 		: 1.0
 * author			: Velu
************************************************************************************/
%>
<!-- quality\capa\GmIncludeCAPAHighlights.jsp -->
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtIncludeCAPAHighlights" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtIncludeCAPAHighlights:setLocale value="<%=strLocale%>"/>
<fmtIncludeCAPAHighlights:setBundle basename="properties.labels.quality.capa.GmIncludeCAPAHighlights"/>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<bean:define id="intSize" name="frmCapaSetup" property="intResultSize" type="java.lang.Integer"></bean:define>
<bean:define id="strCapaDesc" name="frmCapaSetup" property="strCapaDesc" type="java.lang.String"></bean:define>
<bean:define id="strPrevCapas" name="frmCapaSetup" property="strPrevCapas" type="java.lang.String"></bean:define>
<bean:define id="strComplaint" name="frmCapaSetup" property="strComplaint" type="java.lang.String"></bean:define>
<%
String strQualCmplJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_QUALITY_COMPLAINT");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: CAPA Highlights </TITLE>
<script language="JavaScript" src="javascript/Error.js"></script>
<script language="JavaScript" src="<%=strQualCmplJsPath%>/GmCMPUpload.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
</HEAD>
<BODY leftmargin="20" topmargin="10">
		<table border="0" bordercolor="red" width="100%" cellspacing="0" cellpadding="0">
		<%if(intSize > 0){%>
			<tr class="Odd">
				<td class="RightTableCaption" align="right" width="30%" ><fmtIncludeCAPAHighlights:message key="LBL_CAPA"/>:</td>
				 <td height="25" width="30%">&nbsp;<bean:write name="frmCapaSetup" property="strCapaId"/> </td>
				 <td class="RightTableCaption" align="right" width="20%" ><fmtIncludeCAPAHighlights:message key="LBL_STATUS"/>:</td>
				 <td width="35%">&nbsp;<bean:write name="frmCapaSetup" property="statusName"/></td>  
			</tr>	
			<tr><td class="LLine" colspan="5"></td></tr> 
			<tr class="Shade">
				<td class="RightTableCaption" align="right"  ><fmtIncludeCAPAHighlights:message key="LBL_NCMR_COMPLAINT"/>:</td>
				 <td height="25">&nbsp;
				 <div id="showDiv" style="margin-top:0px;margin-left:7px;width: 275px; height: 50px;<%if(strComplaint.length()>100){ %>overflow-y:scroll<%}%>">
				 <bean:write name="frmCapaSetup" property="strComplaint"/> </td> 
				 <td class="RightTableCaption" align="right"  ><fmtIncludeCAPAHighlights:message key="LBL_LOT"/>:</td>
				 <td >&nbsp;<bean:write name="frmCapaSetup" property="strLotNums"/></td> 
			</tr>	
			<tr><td class="LLine" colspan="5"></td></tr> 
			<tr class="Odd">
				<td class="RightTableCaption" align="right"  ><fmtIncludeCAPAHighlights:message key="LBL_REFERENCE"/>:</td>
				 <td height="25" colspan="3">&nbsp;<bean:write name="frmCapaSetup" property="strRefID"/></td>
			</tr>	
			<tr><td class="LLine" colspan="5"></td></tr> 
			<tr class="Shade">
				<td class="RightTableCaption" align="right"  ><fmtIncludeCAPAHighlights:message key="LBL_CAPA_DESCRIPTION"/>:</td>
				 <td colspan="3"><div id="showDiv" style="vertical-align: middle;margin-left:7px;width: 800px; height: 70px;<%if(strCapaDesc.length()>1000){ %>overflow-y:scroll<%}%>">
				 <bean:write name="frmCapaSetup" property="strCapaDesc"/></div></td>
				   
			</tr>
			<tr><td class="LLine" colspan="5"></td></tr> 
			<tr >
			<td class="RightTableCaption" align="right"  ><fmtIncludeCAPAHighlights:message key="LBL_PREVIOUS_CAPA"/>:</td>
				<td colspan="3"><div id="showDiv" style="vertical-align: middle;margin-left:7px;width: 800px; height: 70px;<%if(strPrevCapas.length()>1000){ %>overflow-y:scroll<%}%>">
				 <bean:write name="frmCapaSetup" property="strPrevCapas"/></td>
			</tr>				
			<tr><td class="Line" colspan="5"></td></tr>
		<%}else{%>
				 <tr><td height="25" colspan="5"><font color="blue" size="1"><fmtIncludeCAPAHighlights:message key="LBL_DATA_NOT_FOUND"/>!</font></td></tr>
				<tr><td class="LLine" colspan="5"></td></tr>
		<%}%>
		</table>
</BODY>
</HTML>