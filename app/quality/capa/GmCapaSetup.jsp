<%
/**********************************************************************************
 * File		 		: GmCapaInitiation.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: 
************************************************************************************/
%>
<!-- \quality\capa\GmCapaSetup.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtCapaSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtCapaSetup:setLocale value="<%=strLocale%>"/>
<fmtCapaSetup:setBundle basename="properties.labels.quality.capa.GmCapaSetup"/>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*"%>
<bean:define id="strCapaId" name="frmCapaSetup" property="strCapaId" type="java.lang.String"></bean:define>
<bean:define id="status" name="frmCapaSetup" property="status" type="java.lang.String"></bean:define>
<bean:define id="statusName" name="frmCapaSetup" property="statusName" type="java.lang.String"></bean:define>
<bean:define id="message" name="frmCapaSetup" property="message" type="java.lang.String"></bean:define>
<bean:define id="alAssignedNames" name="frmCapaSetup" property="alAssignedNames" type="java.util.ArrayList"></bean:define>
<bean:define id="alAssignedTo" name="frmCapaSetup" property="alAssignedTo" type="java.util.ArrayList"></bean:define>
<bean:define id="intResultSize" name="frmCapaSetup" property="intResultSize" type="java.lang.Integer"> </bean:define>
<bean:define id="alChooseAction" name="frmCapaSetup" property="alcboActions" type="java.util.ArrayList"> </bean:define>
<bean:define id="strButtonDisable" name="frmCapaSetup" property="strButtonDisable" type="java.lang.String"></bean:define>
<% 
	response.setContentType("text/html; charset=UTF-8");
	response.setCharacterEncoding("UTF-8");
	String strWikiTitle = GmCommonClass.getWikiTitle("CAPA_INITIATION");
	String strCollapseImage = "/images/minus.gif";
	
	String strQualityCapaJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_QUALITY_CAPA");
	ArrayList alRemoveCodeIDs = new ArrayList();
	alRemoveCodeIDs.add("102765");
	
	alChooseAction = GmCommonClass.parseNullArrayList(GmCommonClass.toRemoveCodeIdsFromArrayList(alRemoveCodeIDs,alChooseAction));
	String struCurrentdate = GmCommonClass.parseNull((String)session.getAttribute("strSessTodaysDate"));
	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	strButtonDisable = !strButtonDisable.equals("Y")?"true":"";
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: CAPA Initiation </TITLE>
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel = "stylesheet" type = "text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strQualityCapaJsPath%>/GmCapaSetup.js"></script> 
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
 

<script>
	var confirmMsg = false;
	var status = '<%=status%>';
	var hCAPAID = '<%=strCapaId%>';
	var format = '<%=strApplDateFmt%>';
	var currdate = '<%=struCurrentdate%>';
	var lblReference = '<fmtCapaSetup:message key="LBL_REFERENCE"/>';

	var FormVldArrLen = <%=alAssignedNames.size()%>;
	<% HashMap hmValue = new HashMap();
		for(int i=0; i<alAssignedNames.size(); i++){
			hmValue = (HashMap)alAssignedNames.get(i);
	%>
	var FormVldArr<%=i%> = "<%=hmValue.get("ID")%>|<%=hmValue.get("NAME")%>";
	<%}%>

	var FormAssignedToLen = <%=alAssignedTo.size()%>;
	<% HashMap hmResult = new HashMap();
		for(int i=0; i<alAssignedTo.size(); i++){
		hmResult = (HashMap)alAssignedTo.get(i);
	%>
	var FormAssignedTo<%=i%> = "<%=hmResult.get("ID")%>|<%=hmResult.get("NAME")%>";
	<%}%>
	
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
<html:form action="/gmCapaSetup.do?">
<html:hidden property="strOpt" name="frmCapaSetup" />
<html:hidden property="strAssignedTo" name="frmCapaSetup"/>
<input type="hidden" name="hDisplayNm"/>
<input type="hidden" name="hRedirectURL"/>

<!-- Custom tag lib code modified for JBOSS migration changes -->
<!-- Struts tag lib code modified for JBOSS migration changes -->

<table border="0" bordercolor="lightblue" class="DtTable900" cellspacing="0" cellpadding="0" >
	<tr>
		<td>
			<table border="0" bordercolor="lightblue" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="3" height="25" class="RightDashBoardHeader">&nbsp;<fmtCapaSetup:message key="LBL_CAPA_INITIATION_AND_CLOSURE"/></td>
					<td  height="25" class="RightDashBoardHeader">
					 <fmtCapaSetup:message key="IMG_ALT_HELP" var="varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' ='${varHelp}' width='16' height='16'
						onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');"/>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" bordercolor="lightblue" width="100%" cellspacing="0" cellpadding="0">
				<% if(!message.equals("")){ %>			
					
					<tr>
						<td colspan="5" align="center" height="25">&nbsp;<font style="color: Blue;"><b><bean:write name="frmCapaSetup" property="message"/></b></font></td>
					</tr>
				<%}%>
				
				<tr>
					<td class="RightTableCaption" height="25" align ="Right" width="19%"><fmtCapaSetup:message key="LBL_CAPA"/>:</td>
					<td width="220">&nbsp;<html:text property="strCapaId" maxlength="20" size="20" styleClass="InputArea"
						onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
					</td>
					<td colspan="2" align="left" height="30">&nbsp;
				        <fmtCapaSetup:message key="BTN_LOAD" var="varLoad"/><gmjsp:button name="Btn_Load" value="${varLoad}"  gmClass="button" buttonType="Load" onClick="fnLoad();" />
                    </td>
				</tr>
			</table>
		</td>
	</tr>
	<tr class="ShadeRightTableCaption">
		<td height="25" colspan="4" ><a href="javascript:fnShowFilters('tabGeneral');" tabindex="-1" style="text-decoration:none; color:black" >
			<IMG id="tabGeneralimg" border=0 src="<%=strCollapseImage%>">&nbsp;<fmtCapaSetup:message key="LBL_GENERAL_DETAILS"/>:</a>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" bordercolor="lightblue" width="100%" cellspacing="0" cellpadding="0" id="tabGeneral" style="display:block">
				<tr><td class="Line" colspan="4"></td></tr>
				<tr class="shade">
					<td class="RightTableCaption" align="right" height="25" width="20%"><fmtCapaSetup:message key="LBL_TYPE"/>:</td>
					<td align ="left" width="25%">&nbsp;<gmjsp:dropdown controlName="strMfgType" SFFormName="frmCapaSetup"
						SFSeletedValue="strMfgType" defaultValue= "[Choose One]" SFValue="alMfgType" codeId="CODEID" codeName="CODENM" />
					</td>
					<td class="RightTableCaption" align="right" height="25" width="20%"><fmtCapaSetup:message key="LBL_REQUESTED_BY"/>:</td>
					<td width="40%">&nbsp;<gmjsp:dropdown controlName="strRequestedBy" SFFormName="frmCapaSetup"
						SFSeletedValue="strRequestedBy" defaultValue= "[Choose One]" SFValue="alRequestedBy" codeId="ID" codeName="NAME" />
					</td>
				</tr>
				<tr><td class="LLine" colspan="4"></td></tr> 
				<tr>
					<td class="RightTableCaption" align="right" height="25"><fmtCapaSetup:message key="LBL_NCMR_COMPLAINT"/>:</td>
					<td height="70px">&nbsp;<html:textarea property="strComplaint"  cols="35" style="height:50px"
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
					</td>
					<td class="RightTableCaption" align="right" height="25"><fmtCapaSetup:message key="LBL_ISSUED_BY"/>:</td>
					<td>&nbsp;<gmjsp:dropdown controlName="strIssuedBy" SFFormName="frmCapaSetup"
						SFSeletedValue="strIssuedBy" width="185" defaultValue= "[Choose One]" SFValue="alIssuedBy" codeId="USERNM" codeName="RUSERNM" />
					</td>
				</tr>
				<tr><td class="LLine" colspan="4"></td></tr> 
				<tr class="shade">
					<td class="RightTableCaption" align="right" height="25"><fmtCapaSetup:message key="LBL_REFERENCE"/>:</td>
					<td height="70px">&nbsp;<html:textarea property="strRefID" cols="35" style="height:50px"
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
					</td>
					<td class="RightTableCaption" align="right" height="25"><fmtCapaSetup:message key="LBL_LOT"/>:</td>
					<td height="70px">&nbsp;<html:textarea property="strLotNums" cols="35" style="height:50px"
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
					</td>
					
				</tr>
				<tr><td class="LLine" colspan="4"></td></tr> 
				<tr>
					<td class="RightTableCaption" align="right" height="25"><fmtCapaSetup:message key="LBL_PROJECT"/>:</td>
					<td height="70px">&nbsp;<html:textarea property="strProjectID" cols="35" style="height:50px"
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
					</td>
					<td class="RightTableCaption" align="right" height="25"><fmtCapaSetup:message key="LBL_STATUS"/>:
					<td colspan="3">&nbsp;<bean:write name="frmCapaSetup" property="statusName"/>
					</td>
				</tr>
				<tr><td class="LLine" colspan="4"></td></tr> 
				<tr class="shade">
					<td class="RightTableCaption" align="right" height="25"><fmtCapaSetup:message key="LBL_CATEGORY"/>:</td>
					<td colspan="3">&nbsp;<html:text property="strCategory"  maxlength="255" size="30" styleClass="InputArea"
						onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>
			</table>
		</td>
	 </tr>
	 <tr class="ShadeRightTableCaption">
		<td height="25" colspan="4" ><a href="javascript:fnShowFilters('tabInitial');" tabindex="-1" style="text-decoration:none; color:black">
			<IMG id="tabInitialimg" border=0 src="<%=strCollapseImage%>">&nbsp;<fmtCapaSetup:message key="LBL_INITIAL_QA_EVALUATION_DETAILS"/>:</a>
		</td>
	</tr>
	<tr>
		<td>
		 <!-- Added style="display:table" changes in PC-3662-Edge Browser fixes for Quality Module -->
			<table border="0" bordercolor="lightblue" width="100%" cellspacing="0" cellpadding="0" id="tabInitial" style="display:table">
				<tr><td class="Line" colspan="4"></td></tr>
				<tr>
					<td class="RightTableCaption" align="right" height="25"><fmtCapaSetup:message key="LBL_CAPA_DESCRIPTION"/>:</td>
					<td align ="left" colspan="3" height="70px">&nbsp;<html:textarea property="strCapaDesc" cols="100" style="height:70px"
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>
				<tr><td class="LLine" colspan="4"></td></tr> 
				<tr class="shade">
					<td class="RightTableCaption" align="right" height="25" ><fmtCapaSetup:message key="LBL_ASSINGED_TO"/>:</td>
					<td align ="left" height="70px" colspan="3">&nbsp;<select name="combo" id="AssingedNameCombo" size="4" multiple="multiple" style="width: 200px" ></select>
					&nbsp;&nbsp;&nbsp;<!--<img align="middle" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/delete.gif' title='delete' width='16' height='16' onClick="fnRemoveNames()"/>
					<img align="middle" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/add.gif' title='Add' width='16' height='16' onclick="fnAssignedToNames()"/>-->
					<gmjsp:button align="middle" name="Add" value=">>" controlId="Add" gmClass="button" style="width: 2em;height:2em;font-weight:bold;font-size: 12px;" buttonType="Load" onClick="fnAssignedToNames();" />&nbsp;&nbsp;&nbsp;
					<gmjsp:button align="middle" name="Delete" value="X" controlId="Delete" gmClass="button" style="width: 2em;height:2em;background-color:red" buttonType="Load" onClick="fnRemoveNames();" />
					&nbsp;&nbsp;&nbsp;<select name="combo" id="AssignedToCombo" size="4" multiple="multiple" style="width: 200px;" >
					</td>					
					
					<!-- <td align ="left" height="70px" colspan="2">&nbsp;<select name="combo" id="AssignedToCombo" size="4" multiple="multiple" style="width: 200px;" ></select>	</td> -->
				</tr>
				<tr><td class="LLine" colspan="4"></td></tr> 
				<tr>
					<td class="RightTableCaption" align="right" height="25" width="21%"><fmtCapaSetup:message key="LBL_PREVIOUS_CAPAS"/>:</td>
					<td align ="left" height="70px" width="35%">&nbsp;<html:textarea property="strPrevCapas" cols="30" style="height:70px"
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
					</td>
					<td class="RightTableCaption" align="right" height="25" width="20%"><fmtCapaSetup:message key="LBL_CAPA_SUBMITTED_DATE"/>:</td>
					<td width="40%">&nbsp;<gmjsp:calendar SFFormName="frmCapaSetup" controlName="strCapaSubDt" gmClass="InputArea" 
									onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>
				<tr><td class="LLine" colspan="4"></td></tr> 
				<tr class="shade">
					<td class="RightTableCaption" align="right" height="25"><fmtCapaSetup:message key="LBL_RESPONSE_DUE_DATE"/>:</td>
					<td align ="left">&nbsp;<gmjsp:calendar SFFormName="frmCapaSetup" controlName="strRespDueDt" gmClass="InputArea" 
									onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
					</td>
					<td class="RightTableCaption" align="right" height="25"><fmtCapaSetup:message key="LBL_PRIORITY_LEVEL"/>:</td>
					<td >&nbsp;<gmjsp:dropdown controlName="strPrtyLvl" SFFormName="frmCapaSetup"
						SFSeletedValue="strPrtyLvl" defaultValue= "[Choose One]" SFValue="alPrtyLvl" codeId="CODEID" codeName="CODENM" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr class="ShadeRightTableCaption">
		<td height="25" colspan="4" ><a href="javascript:fnShowFilters('tabClosure');" tabindex="-1" style="text-decoration:none; color:black">
			<IMG id="tabClosureimg" border=0 src="<%=strCollapseImage%>">&nbsp;<fmtCapaSetup:message key="LBL_CLOSURE_DETAILS"/>:</a>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" bordercolor="lightblue" width="100%" cellspacing="0" cellpadding="0" id="tabClosure" style="display:block">
				<tr><td class="Line" colspan="5"></td></tr>
				<tr>
					<td class="RightTableCaption" align="right" height="25" width="21%"><fmtCapaSetup:message key="LBL_CLOSED_DATE"/>:</td>
					<td align ="left" width="35%">&nbsp;<gmjsp:calendar SFFormName="frmCapaSetup" controlName="strClosedDt" gmClass="InputArea" 
									onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
					</td>
					<td class="RightTableCaption" align="right" height="25" width="20%"><fmtCapaSetup:message key="LBL_DAYS_TO_CLOSE"/>:</td>
					<td height="25" width="40%" >&nbsp;<bean:write name="frmCapaSetup" property="daysToClose"/> </td>					 
				</tr>
				<tr><td class="LLine" colspan="5"></td></tr> 
				<tr class="shade">
					<td class="RightTableCaption" align="right" height="25"><fmtCapaSetup:message key="LBL_CAPA_RESOPONSE"/>:</td>
					<td align ="left" colspan="4" height="70px">&nbsp;<html:textarea property="strCapaResp" cols="100" style="height:70px"
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td class="LLine" colspan="5"></td></tr>
  	<tr>
		<td colspan="5"> 
			<jsp:include page="/common/GmIncludeLog.jsp" >
				<jsp:param name="FORMNAME" value="frmCapaSetup" />
				<jsp:param name="ALNAME" value="alLogReasons" />
				<jsp:param name="LogMode" value="Edit" />
			</jsp:include>
		</td>
	</tr>
		<tr>
				<td colspan="5" align="center" height="30">&nbsp;
				<%if(!status.equals("102771") && !status.equals("102770")){ %>
				   <fmtCapaSetup:message key="BTN_RESET" var="varReset"/>	<gmjsp:button name="Btn_Reset" value="${varReset}" controlId="Reset" gmClass="button" buttonType="Save" style="width: 6em" onClick="fnReset();" />&nbsp;
				   <fmtCapaSetup:message key="BTN_SUBMIT" var="varSubmit"/>	<gmjsp:button name="Btn_Submit" value="${varSubmit}" controlId="Submit" gmClass="button" style="width: 6em" buttonType="Save" onClick="fnSubmit();" disabled="<%=strButtonDisable%>" />&nbsp;
			   	
			   	  
				<%} %>
				<%if( intResultSize >= 1) {%>
			   		<fmtCapaSetup:message key="BTN_PRINT" var="varPrint"/><gmjsp:button name="Btn_Print" value="${varPrint}" controlId="Print" gmClass="button" style="width: 6em" buttonType="Load" onClick="fnPrint();" />&nbsp;
			   	<%}%>
			
				<!-- This IF block is added for if the CAPA Status is Closed[102771] , then we are going to show these Email and Print Button -->	
		<%if(status.equals("102771")){ %>
			   	<fmtCapaSetup:message key="BTN_EMAIL" var="varEmail"/><gmjsp:button name="Btn_Email" value="${varEmail}" controlId="Email" gmClass="button" style="width: 6em" buttonType="Save" onClick="fnEmail();" /></td>                   
		<%} %>
	   	</td>
		</tr>
	<%if( intResultSize >= 1) {%>
		<tr><td class="LLine" colspan="5"></td></tr> 
		<tr class="shade">
			<td height="30" align="center">
				<gmjsp:dropdown controlName="strCboSelected" SFFormName="frmCapaSetup" SFSeletedValue="strCboSelected"
				SFValue="alcboActions" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose Action]" tabIndex="13"/>&nbsp;&nbsp;&nbsp;&nbsp;
				<fmtCapaSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnChooseAction()" style="width: 5em; height: 22" tabindex="14"/>
			</td>
		</tr>
	<%}%>
	</table>			
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>