
<%
	/**********************************************************************************
	 * File		 		: GmIssuingAgencyConfig.jsp
	 * Desc		 		: Setting up UDI Configuration
	 * Version	 		: 1.0
	 * author			: 
	 ************************************************************************************/
%>
<!-- quality\GmIssuingAgencyConfig.jsp -->
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtIssuingAgencyConfig" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtIssuingAgencyConfig:setLocale value="<%=strLocale%>"/>
<fmtIssuingAgencyConfig:setBundle basename="properties.labels.quality.GmIssuingAgencyConfig"/>
<%@ page import="java.util.ArrayList,java.util.HashMap"%>
<%

String strQualJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_QUALITY");
	String strWikiTitle = GmCommonClass.parseNull((String) GmCommonClass.getWikiTitle("UDI_CONFIG_SETUP"));
	HashMap hmIdentifierDetail = new HashMap();
	HashMap hmIssuingAgency = new HashMap();
	HashMap hmLoop = new HashMap();
	HashMap hcboVal = new HashMap();
	String strAgencyIdenId = "";
	String strCodeID = "";
	String strSelected = "";
	String strConfigDetailId = "";
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: UDI Configuration Setup</TITLE>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strQualJsPath%>/GmIssuingAgencyConfig.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<bean:define id="alAllIdentifier" name="frmIssuingAgencyConfig" property="alAllIdentifier" type="java.util.ArrayList"></bean:define>
<bean:define id="alIssuingAgency" name="frmIssuingAgencyConfig"	property="alIssuingAgency" type="java.util.ArrayList"></bean:define>
<bean:define id="alResult" name="frmIssuingAgencyConfig" property="alResult" type="java.util.ArrayList"></bean:define>
<bean:define id="alIdentifier" name="frmIssuingAgencyConfig" property="alIdentifier" type="java.util.ArrayList"></bean:define>
<bean:define id="buttonDisabled" name="frmIssuingAgencyConfig" property="buttonDisabled" type="java.lang.String"></bean:define>
<script>
	var count =2;
	var savedIdentLen = <%=alResult.size()%>;
	var preSelect = '';
	var dropDownVal = '<option value=0 id=0 >[Choose One]</option>';
	var identifierLen = <%=alAllIdentifier.size()%>;
	// DI number check
	var firstIdenDiNumber = '';
</script>
<%
	for (int i = 0; i < alAllIdentifier.size(); i++) {
		hmIdentifierDetail = (HashMap) alAllIdentifier.get(i);
		String strAgencyId = (String) hmIdentifierDetail.get("ID");
%>
<script>put('<%=i%>','<%=hmIdentifierDetail.get("AG_ID")%>^^<%=strAgencyId%>^^<%=hmIdentifierDetail.get("NM")%>^^<%=hmIdentifierDetail.get("IDENT_SYMBOL_ID")%>');</script>
<%
	}
%>

<%
	for (int i = 0; i < alIssuingAgency.size(); i++) {
		hmIssuingAgency = (HashMap) alIssuingAgency.get(i);
		String strAgencyId = (String) hmIssuingAgency.get("ID");
		strAgencyId = strAgencyId + "UDI";
%>
<script>put('<%=strAgencyId%>','<%=hmIssuingAgency.get("SHORTNM")%>');</script>
<%
	}
%>

</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnOnLoad();">
	<html:form action="/gmIssuingAgencyConfig.do?method=issuingAgencyConfiguration">
		<html:hidden property="strOpt" />
		<html:hidden property="inputStr" value="" />

		<table class="DtTable800" cellspacing="0" cellpadding="0">

			<tr>
				<td colspan="3" height="25" class="RightDashBoardHeader">&nbsp;<fmtIssuingAgencyConfig:message key="LBL_UDI_CONFIGURATION_SETUP"/></td>
				<td height="25" class="RightDashBoardHeader">
<fmtIssuingAgencyConfig:message key="IMG_ALT_HELP" var = "varHelp"/><img align="right"
					id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>

			<tr>
				<td colspan="2" class="RightTableCaption" align="right"  width="300"><fmtIssuingAgencyConfig:message key="LBL_CONFIGURATION" var="varConfiguration"/><gmjsp:label
						type="RegularText" SFLblControlName="${varConfiguration}:" td="false" />
				</td>
				<td colspan="2" width="500">&nbsp;<gmjsp:dropdown
						controlName="configName" SFFormName="frmIssuingAgencyConfig"
						SFSeletedValue="configName" SFValue="alConfiguration" codeId="ID"
						codeName="NAME" defaultValue="[Choose One]"
						onChange="fnChangeConfig();" /></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
			<tr class="Shade">
				<td colspan="2" class="RightTableCaption" align="right"><fmtIssuingAgencyConfig:message key="LBL_ISSUING_AGENCY" var="varIssuing"/><gmjsp:label
						type="MandatoryText" SFLblControlName="${varIssuing}:" td="false" />
				</td>
				<td colspan="2">&nbsp;<gmjsp:dropdown
						controlName="issuingAgency" SFFormName="frmIssuingAgencyConfig"
						SFSeletedValue="issuingAgency" SFValue="alIssuingAgency"
						codeId="ID" codeName="NAME" defaultValue="[Choose One]"
						onChange="fnChangeIssuAgency(this);" /></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
			<tr>
				<td colspan="2" class="RightTableCaption" align="right"><fmtIssuingAgencyConfig:message key="LBL_CONFIGURATION_NAME" var="varConfigurationName"/><gmjsp:label
						type="MandatoryText" SFLblControlName="${varConfigurationName}:"
						td="false" />
				</td>
				<td colspan="2"><div id="shortName" style="display: none;"></div>
					<html:text property="txt_configName" name="frmIssuingAgencyConfig"
						onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
						onblur="changeBgColor(this,'#ffffff');"></html:text>
				</td>
			</tr>
			<logic:notEqual property="samplePattern" value=""
				name="frmIssuingAgencyConfig">
				<tr>
					<td class="LLine" height="1" colspan="4"></td>
				</tr>
				<tr class="Shade">
					<td colspan="2" class="RightTableCaption" align="right"><fmtIssuingAgencyConfig:message key="LBL_SAMPLE_PATTERN"/>:</td>
					<td colspan="2">&nbsp;<bean:write
							name="frmIssuingAgencyConfig" property="samplePattern"></bean:write>
					</td>
				</tr>
			</logic:notEqual>

			<TR>
				<td colspan="4">
					<div style="overflow: auto; height: auto;">
						<table cellpadding="0" cellspacing="0" width="100%" border="1"
							bordercolor="gainsboro" id="tabIssuingAgency">
							<thead>
								<TR class="ShadeRightTableCaption" height="20">
									<TH class="RightText" width="20" align="center">#</TH>
									<TH class="RightText" width="100" align="center"><fmtIssuingAgencyConfig:message key="LBL_IDENTIFIERS"/></TH>
								</TR>
							</thead>
							<TBODY>
								<logic:equal property="strOpt" value=""
									name="frmIssuingAgencyConfig">
									<tr height="30">
										<td class="RightText" align="center">1<input
											type="hidden" name="hConfigDtlId0" value="" />
										</td>
										<td align="left"><select name="identifier0"
											id="identifier0" class="RightText"
											onFocus="changeBgColor(this,'#AACCE8');"
											onBlur="changeBgColor(this,'#ffffff');">
												<option value=0 id=0><fmtIssuingAgencyConfig:message key="LBL_CHOOSE_ONE"/>
										</select></td>
									</tr>
									<tr height="30">
										<td class="RightText" align="center">2<input
											type="hidden" name="hConfigDtlId1" value="" />
										</td>
										<td align="left"><select name="identifier1"
											id="identifier1" class="RightText"
											onFocus="changeBgColor(this,'#AACCE8');"
											onBlur="changeBgColor(this,'#ffffff');">
												<option value=0 id=0><fmtIssuingAgencyConfig:message key="LBL_CHOOSE_ONE"/></option>
										</select></td>
									</tr>
								</logic:equal>
								<logic:notEqual property="strOpt" value=""
									name="frmIssuingAgencyConfig">
									<%
										for (int i = 0; i < alResult.size(); i++) {
													hmLoop = (HashMap) alResult.get(i);
													strAgencyIdenId = GmCommonClass.parseZero((String) hmLoop.get("ID"));
													strConfigDetailId = GmCommonClass.parseNull((String) hmLoop.get("CONFDTLID"));
									%>
									<tr height="30">
										<td class="RightText" align="center"><%=i + 1%><input
											type="hidden" name="hConfigDtlId<%=i%>"
											value="<%=strConfigDetailId%>" />
										</td>
										<td align="left"><select name="identifier<%=i%>"
											id="identifier<%=i%>" class="RightText"
											onFocus="changeBgColor(this,'#AACCE8');"
											onBlur="changeBgColor(this,'#ffffff');">
												<option value=0 id=0><fmtIssuingAgencyConfig:message key="LBL_CHOOSE_ONE"/></option>
												<%
													for (int j = 0; j < alIdentifier.size(); j++) {
																	hcboVal = (HashMap) alIdentifier.get(j);
																	strCodeID = GmCommonClass.parseNull((String)(String) hcboVal.get("ID"));
																	strSelected = strAgencyIdenId.equals(strCodeID) ? "selected": "";
												%>
												<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NM")%></option>
												<%
													}
												%>
										</select></td>
									</tr>
									<%
										}
									%>


								</logic:notEqual>
							</TBODY>
						</table>
					</div></td>
			</TR>
			<tr class="Shade" height="30">
				<td colspan="4" align="center"><fmtIssuingAgencyConfig:message key="BTN_ADD_ROW" var="varAddRow"/><gmjsp:button value="${varAddRow}"
						gmClass="button" name="btn_Add" style="width: 6em"
						buttonType="Save" onClick="fnAddRow();"
						disabled="<%=buttonDisabled %>" /> <fmtIssuingAgencyConfig:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}"
						gmClass="button" name="btn_submit" style="width: 6em"
						buttonType="Save" onClick="fnSubmit();"
						disabled="<%=buttonDisabled %>" />&nbsp;&nbsp;</td>
			</tr>
			<logic:notEqual property="samplePattern" value=""
				name="frmIssuingAgencyConfig">
				<tr>
					<td class="LLine" height="1" colspan="4"></td>
				</tr>
				<tr>
					<td colspan="4" class="RightTableCaptionRed">&nbsp;<fmtIssuingAgencyConfig:message key="LBL_IF_ANY_CHANGES_ARE_MADE_TO_THESE_EDENTIFIERS_PLEASE_ENSURE_PROPER_APPROVAL_PROCESS_HAS_BEEN_FOLLOWED"/></td>
				</tr>
			</logic:notEqual>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>