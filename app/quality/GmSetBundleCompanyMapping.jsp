<%
/**********************************************************************************
 * File		 		: GmSetBundleCompanyMapping.jsp
 * Desc		 		: This screen is used load the Set Bundle Company mapping details
 * Version	 		: 1.0
 * author			: Agilan 
************************************************************************************/
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtSystemCompanyMap" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- prodmgmnt\GmSetBundleCompanyMapping.jsp -->
<fmtSystemCompanyMap:setLocale value="<%=strLocale%>"/>
<fmtSystemCompanyMap:setBundle basename="properties.labels.prodmgmnt.GmSystemCompanyMap"/>
<bean:define id="gridData" name="frmSetBundleMapping" property="gridXmlData" type="java.lang.String"> </bean:define> 
<%
String strProdMgmntJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Globus Medical: System - Company Mapping</title>
<link rel="stylesheet" type="text/css"	href="<%=strJsPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">

<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmSystemDetail.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script>
	var objGridData;
	objGridData = '<%=gridData%>';
</script>

</HEAD>
<BODY onload="fnOnPageLoad();">

<table  border="0" width="100%" cellspacing="0" cellpadding="0" >
	<tr>	
		<td>
			<%if( gridData.indexOf("cell") == -1){%>
		<tr>
			<td colspan="3" align="center"><div  id="ShowDetails" align="center"></div></td>							
		</tr>
		<%} %>
			<div id="dataGridDiv"  style="width:695px;height:500px;"></div>
		</td>
		</tr>
				
		<tr>						
		<td align="center"><br>
	             		<div class='exportlinks'><fmtSystemCompanyMap:message key="DIV_EXPORT_OPTIONS"/>: <img src='img/ico_file_excel.png' />&nbsp; <a href="#" onclick="fnExport();"> <fmtSystemCompanyMap:message key="DIV_EXCEL"/> </a> </div> </td>
    <tr>
	  
</table>			

<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
