<!-- quality\GmPartAttributeBulkSetup.jsp -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<%@ taglib prefix="fmtPartAttributeBulkSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtPartAttributeBulkSetup:setLocale value="<%=strLocale%>"/>
<fmtPartAttributeBulkSetup:setBundle basename="properties.labels.quality.GmPartAttributeBulkSetup"/>

<bean:define id="gridData" name="frmPartAttributeBulkSetup" property="gridData" type="java.lang.String"/> 
<bean:define id ="attributeType" name="frmPartAttributeBulkSetup" property="attributeType" type="java.lang.String"/> 
<bean:define id ="strInvalidParts" name="frmPartAttributeBulkSetup" property="strInvalidParts" type="java.lang.String"/>
<bean:define id ="accessAllComp" name="frmPartAttributeBulkSetup" property="accessAllComp" type="java.lang.String"/> 
<bean:define id ="accessVoidfl" name="frmPartAttributeBulkSetup" property="accessVoidfl" type="java.lang.String"/> 
<bean:define id="jsonString" name="frmPartAttributeBulkSetup" property="jsonString" type="java.lang.String"/>
<%

String strQualJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_QUALITY");
String strWikiTitle = GmCommonClass.getWikiTitle("PART_ATTRIBUTE_SETUP_BULK"); 
String strDisabled = (accessVoidfl.equals("Y")) ? "false":"true";
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Part Attribute Setup - Bulk </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css"> 
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<script language="JavaScript" src="<%=strQualJsPath%>/GmPartAttributeBulk.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/prodmgmnt/GmRule.js"></script> 


 <script>
	var objGridData = '<%=gridData%>';
	var jsonCompFl = '<%=jsonString%>';
	var lblAttributeType = '<fmtPartAttributeBulkSetup:message key="LBL_ATTRIBUTE_TYPE"/>';
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onload = "fnOnPageLoadSetup()">
<html:form action="/gmpartAttrBlkSetup.do?method=savePartAttribute">
<html:hidden property="strOpt" value="frmPartAttributeBulkSetup"/>
<html:hidden property="screenType" name="frmPartAttributeBulkSetup"/>
<html:hidden property="chkAllcompany" name="frmPartAttributeBulkSetup"/>
<table border="0"  height="60" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr  height="24">
			<td class="RightDashBoardHeader" width="30%">&nbsp;<fmtPartAttributeBulkSetup:message key="LBL_PART_ATTRIBUTE_SETUP_BULK"/></td>
			<td class="RightDashBoardHeader" align="right">
					<fmtPartAttributeBulkSetup:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
						height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');"/>&nbsp;
			</td>
		</tr>
		
		<tr height="24">
			<td  class="RightTableCaption" align="right"><font color="red">*</font>&nbsp;<fmtPartAttributeBulkSetup:message key="LBL_ATTRIBUTE_TYPE"/>:</td>
			<td align="left">&nbsp;<gmjsp:dropdown width="200" controlName="attributeType" SFFormName="frmPartAttributeBulkSetup" SFSeletedValue="attributeType" 
			codeId="CODENMALT" codeName="CODENM" tabIndex="1" SFValue="alAttrType" defaultValue="[Choose One]" onChange="javascript:fnSetCompanyFlagSetup();"/>
			</td>
			
			
		</tr>
		<tr>
						<td class="RightTextBlue">
							
						</td>
						<td  border="1"class="RightTextBlue" align="left" style="display: none;" id="appforAllCmp">
							&nbsp;&nbsp;<fmtPartAttributeBulkSetup:message key="LBL_APPLICABLE_FOR_ALL_COMPANIES"/>
						</td>
					</tr>		
		
		
		<tr>
			<td class="RightTableCaption" align="right"><font color="red">*</font>&nbsp;<fmtPartAttributeBulkSetup:message key="LBL_ENTER_DATA_PART_ATTRIBUTE_VALUE"/>:</td>
			<td>&nbsp;<html:textarea name="frmPartAttributeBulkSetup" property="attributeValue" cols="70" rows="20" 
			onfocus="changeBgColor(this,'#AACCE8');" styleId="attributeValue" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');fnOnBlurEnterData();" /><span align="right" class="RightTextBlue" id="NoOfcnt" style="display: none;"></span></td>

		</tr>
		
		
		<tr height="28">
			<td  align="center" colspan="2" >
			<fmtPartAttributeBulkSetup:message key="BTN_VOID" var="varVoid"/><gmjsp:button value="&nbsp;${varVoid}&nbsp;"  name="Btn_Submit" gmClass="button" onClick="fnVoid()" disabled="<%=strDisabled%>" buttonType="Save" />&nbsp;&nbsp;
				<fmtPartAttributeBulkSetup:message key="BTN_RESET" var="varReset"/><gmjsp:button value="&nbsp;${varReset}&nbsp;" name="Btn_Reset" gmClass="button" onClick="fnReset()" buttonType="Save" />&nbsp;&nbsp;
			<fmtPartAttributeBulkSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="&nbsp;${varSubmit}&nbsp;" name="Btn_Submit" gmClass="button" onClick="fnSubmitAttribute()" buttonType="Save" />
			</td>
		</tr>
		<% if(!strInvalidParts.equals("")){%>
<tr height="28"><td colspan="2" ><div class="RightTableCaption" height = "25px" > <img border="0"  src="<%=strImagePath%>/30.gif" height="20" width="20"><font color="red">&nbsp;&nbsp;<fmtPartAttributeBulkSetup:message key="LBL_PLEASE_CORRECT_INVALID_PARTS"/>:&nbsp;<%=strInvalidParts%></font></div></td></tr>	
	
	<%}%>
		<tr height="24"></tr>
		

<%if (gridData.indexOf("cell") != -1) {%>
			

	<tr><td colspan="2">
	<table border="0" cellspacing="0" cellpadding="0">

	<tr>
	<td><div  id="dataGridDiv" height="400px"></div></td></tr>
	<tr>						
					<td align="center" colspan="7"><br>
	             			<div class='exportlinks'><fmtPartAttributeBulkSetup:message key="DIV_EXPORT_OPT"/>: <img src='img/ico_file_excel.png' />&nbsp; <a href="#" onclick="fnExport();"><fmtPartAttributeBulkSetup:message key="DIV_EXCEL"/></a></div>                         
	       			</td>
       			</tr>
       			
       			
	<% }else if (!gridData.equals("")) {
			%>
			<tr>
				<td colspan="9" height="1" class="LLine"></td>
			</tr>
			<tr>
			<tr>
				<td colspan="9" align="center" class="RightText"><fmtPartAttributeBulkSetup:message key="LBL_NOTHING_FOUND_TO_DISPLAY"/></td>
			</tr>
			</tr>
			<%} %>
			
	</table>
	


</table> 
</html:form>
</BODY>

<%@ include file="/common/GmFooter.inc" %>

</HTML>
