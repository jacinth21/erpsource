 <%
/**********************************************************************************
 * File		 		: GmPartPDSetup.TESTjsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: 
************************************************************************************/
%>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %> 
<%@ taglib prefix="fmtPartPDSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- quality\GmPartPDSetup.jsp -->
<fmtPartPDSetup:setLocale value="<%=strLocale%>"/>
<fmtPartPDSetup:setBundle basename="properties.labels.quality.GmPartPDSetup"/>
<bean:define id="hmAttrVal" name="frmUDIProductDevelopment" property="hmAttrVal" type="java.util.HashMap"></bean:define>
<bean:define id="hmDropdown" name="frmUDIProductDevelopment" property="hmResult" type="java.util.HashMap"/>
<bean:define id="strAccessFl" name="frmUDIProductDevelopment" property="setSetupSubmitAcc" type="java.lang.String" />
<bean:define id="alContainLatex" name="frmUDIProductDevelopment" property="alContainLatex" type="java.util.ArrayList"></bean:define>
<bean:define id="alReqSteril" name="frmUDIProductDevelopment" property="alReqSteril" type="java.util.ArrayList"></bean:define>
<bean:define id="alSterilMthod" name="frmUDIProductDevelopment" property="alSterilMthod" type="java.util.ArrayList"></bean:define>
<bean:define id="alIsDevice" name="frmUDIProductDevelopment" property="alIsDevice" type="java.util.ArrayList"></bean:define>
<bean:define id="alSizeType" name="frmUDIProductDevelopment" property="alSizeType" type="java.util.ArrayList"></bean:define>
<bean:define id="alSizeUOM" name="frmUDIProductDevelopment" property="alSizeUOM" type="java.util.ArrayList"></bean:define>
<bean:define id="editDisable" name="frmUDIProductDevelopment" property="editDisable" type="java.lang.String"> </bean:define>

<%! HashMap hmDropdownSet = new HashMap();
	ArrayList alLength = new ArrayList();
	ArrayList alArea = new ArrayList();
	ArrayList alWeight = new ArrayList();
	ArrayList alTotalVolume = new ArrayList();
	ArrayList alGauge = new ArrayList();
	ArrayList alAngle = new ArrayList();
	ArrayList alPressure = new ArrayList();
	ArrayList alAllUom = new ArrayList();
%>
<%
String strProdMgmntJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
	String strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("PART_PD_TAB"));
	String strDisable = strAccessFl.equals("Y")?"":"true";
	if(editDisable.equalsIgnoreCase("true")){
	  strDisable = "true";
	}
	String strPartNum = GmCommonClass.parseNull((String)hmAttrVal.get("PNUM"));
	String strPartDesc = GmCommonClass.parseNull((String)hmAttrVal.get("PART_DESC"));
	strPartDesc = GmCommonClass.getStringWithTM(strPartDesc);
	String strProjectId = GmCommonClass.parseNull((String)hmAttrVal.get("PROJECTID"));
	String strContainsLatex = GmCommonClass.parseNull((String)hmAttrVal.get("CONTAIN_LATEX"));
	String strReqSteril = GmCommonClass.parseNull((String)hmAttrVal.get("REQUIRE_STERILE"));
	String strSterilMthod = GmCommonClass.parseNull((String)hmAttrVal.get("STERILE_METHOD"));
	String strIsDevice = GmCommonClass.parseNull((String)hmAttrVal.get("DEVICE_AVALIABLE"));
	String strSize1Type = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE1_TYPE"));
	String strSize1TypeText = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE1_TYPE_TEXT"));
	String strSize1Value = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE1_VALUE"));
	String strSize1UOM = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE1_UOM"));
	String strSize2Type = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE2_TYPE"));
	String strSize2TypeText = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE2_TYPE_TEXT"));
	String strSize2Value = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE2_VALUE"));
	String strSize2UOM = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE2_UOM"));
	String strSize3Type = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE3_TYPE"));
	String strSize3TypeText = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE3_TYPE_TEXT"));
	String strSize3Value = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE3_VALUE"));
	String strSize3UOM = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE3_UOM"));
	String strSize4Type = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE4_TYPE"));
	String strSize4TypeText = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE4_TYPE_TEXT"));
	String strSize4Value = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE4_VALUE"));
	String strSize4UOM = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE4_UOM"));
	
	//History Flag values
	String strPartNumHisFlag = GmCommonClass.parseNull((String)hmAttrVal.get("PNUM_FL"));
	String strPartDescHisFlag = GmCommonClass.parseNull((String)hmAttrVal.get("PART_DESC_FL"));
	String strProjectIdHisFlag = GmCommonClass.parseNull((String)hmAttrVal.get("PROJECTID_FL"));
	String strContainsLatexHisFlag = GmCommonClass.parseNull((String)hmAttrVal.get("CONTAIN_LATEX_FL"));
	String strReqSterilHisFlag = GmCommonClass.parseNull((String)hmAttrVal.get("REQUIRE_STERILE_FL"));
	String strSterilMthodHisFlag = GmCommonClass.parseNull((String)hmAttrVal.get("STERILE_METHOD_FL"));
	String strIsDeviceHisFlag = GmCommonClass.parseNull((String)hmAttrVal.get("DEVICE_AVALIABLE_FL"));
	String strSize1TypeHisFlag = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE1_TYPE_FL"));
	String strSize1TypeTextHisFlag = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE1_TYPE_TEXT_FL"));
	
	String strSize1ValueHisFlag = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE1_VALUE_FL"));
	String strSize1UOMHisFlag = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE1_UOM_FL"));
	String strSize2TypeHisFlag = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE2_TYPE_FL"));
	String strSize2TypeTextHisFlag = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE2_TYPE_TEXT_FL"));
	String strSize2ValueHisFlag = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE2_VALUE_FL"));
	String strSize2UOMHisFlag = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE2_UOM_FL"));
	String strSize3TypeHisFlag = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE3_TYPE_FL"));
	String strSize3TypeTextHisFlag = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE3_TYPE_TEXT_FL"));
	String strSize3ValueHisFlag = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE3_VALUE_FL"));
	String strSize3UOMHisFlag = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE3_UOM_FL"));
	String strSize4TypeHisFlag = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE4_TYPE_FL"));
	String strSize4TypeTextHisFlag = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE4_TYPE_TEXT_FL"));
	String strSize4ValueHisFlag = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE4_VALUE_FL"));
	String strSize4UOMHisFlag = GmCommonClass.parseNull((String)hmAttrVal.get("SIZE4_UOM_FL"));
	String strPartSterileFlag = GmCommonClass.parseNull((String)hmAttrVal.get("STERILEPART"));
	//String strserialNumFl = GmCommonClass.parseNull((String)hmAttrVal.get("SERIALNUMNEEDFL")).equals("Y")?"checked=checked":"";
	
	//Based on the History flag value we are showing History Icon
	strPartNumHisFlag = strPartNumHisFlag.equals("Y") ? "<img style='cursor:hand' alt='View Part Number' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1079'); border='0'>": "";
	strPartDescHisFlag = strPartDescHisFlag.equals("Y") ? "<img style='cursor:hand' alt='View Part Description' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1089'); border='0'>": "";
	strProjectIdHisFlag = strProjectIdHisFlag.equals("Y") ? "<img style='cursor:hand' alt='View ProjectId' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1081'); border='0'>": "";
	strContainsLatexHisFlag = strContainsLatexHisFlag.equals("Y") ? "<img style='cursor:hand' alt='View Contains Latex Information' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1078'); border='0'>": "";
	strReqSterilHisFlag = strReqSterilHisFlag.equals("Y") ? "<img style='cursor:hand' alt='View Requires Steril Information' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1079'); border='0'>": "";
	strSterilMthodHisFlag = strSterilMthodHisFlag.equals("Y") ? "<img style='cursor:hand' alt='View Sterilization Method' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1080'); border='0'>": "";
	strIsDeviceHisFlag = strIsDeviceHisFlag.equals("Y") ? "<img style='cursor:hand' alt='View IsDevice flag' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1081'); border='0'>": "";
	
	strSize1TypeHisFlag = strSize1TypeHisFlag.equals("Y") ? "<img style='cursor:hand' alt='View Size 1 Type' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1082'); border='0'>": "";
	strSize1TypeTextHisFlag = strSize1TypeTextHisFlag.equals("Y") ? "<img style='cursor:hand' alt='View Size 1 Type Text' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1083'); border='0'>": "";
	strSize1ValueHisFlag = strSize1ValueHisFlag.equals("Y") ? "<img style='cursor:hand' alt='View Size 1 Value' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1084'); border='0'>": "";
	strSize1UOMHisFlag = strSize1UOMHisFlag.equals("Y") ? "<img style='cursor:hand' alt='View Size 1UOM' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1085'); border='0'>": "";
	
	strSize2TypeHisFlag = strSize2TypeHisFlag.equals("Y") ? "<img style='cursor:hand' alt='View Size 2 Type' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1086'); border='0'>": "";
	strSize2TypeTextHisFlag = strSize2TypeTextHisFlag.equals("Y") ? "<img style='cursor:hand' alt='View Size 2 Type Text' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1087'); border='0'>": "";
	strSize2ValueHisFlag = strSize2ValueHisFlag.equals("Y") ? "<img style='cursor:hand' alt='View Size 2 Value' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1088'); border='0'>": "";
	strSize2UOMHisFlag = strSize2UOMHisFlag.equals("Y") ? "<img style='cursor:hand' alt='View Size 2 UOM' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1089'); border='0'>": "";

	strSize3TypeHisFlag = strSize3TypeHisFlag.equals("Y") ? "<img style='cursor:hand' alt='View Size 3 Type' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1090'); border='0'>": "";
	strSize3TypeTextHisFlag = strSize3TypeTextHisFlag.equals("Y") ? "<img style='cursor:hand' alt='View Size 3 Type Text' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1091'); border='0'>": "";
	strSize3ValueHisFlag = strSize3ValueHisFlag.equals("Y") ? "<img style='cursor:hand' alt='View Size 3 Value' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1092'); border='0'>": "";
	strSize3UOMHisFlag = strSize3UOMHisFlag.equals("Y") ? "<img style='cursor:hand' alt='View Size 3 UOM' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1093'); border='0'>": "";

	strSize4TypeHisFlag = strSize4TypeHisFlag.equals("Y") ? "<img style='cursor:hand' alt='View Size 4 Type' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1094'); border='0'>": "";
	strSize4TypeTextHisFlag = strSize4TypeTextHisFlag.equals("Y") ? "<img style='cursor:hand' alt='View Size 4 Type Text' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1095'); border='0'>": "";
	strSize4ValueHisFlag = strSize4ValueHisFlag.equals("Y") ? "<img style='cursor:hand' alt='View Size 4 Value' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1096'); border='0'>": "";
	strSize4UOMHisFlag = strSize4UOMHisFlag.equals("Y") ? "<img style='cursor:hand' alt='View Size 4 UOM' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1097'); border='0'>": "";

	alLength = GmCommonClass.parseNullArrayList((ArrayList) hmDropdown.get("alLength"));
	alArea = GmCommonClass.parseNullArrayList((ArrayList) hmDropdown.get("alArea"));
	alWeight = GmCommonClass.parseNullArrayList((ArrayList) hmDropdown.get("alWeight"));
	alTotalVolume = GmCommonClass.parseNullArrayList((ArrayList) hmDropdown.get("alTotalVolume"));
	alGauge = GmCommonClass.parseNullArrayList((ArrayList) hmDropdown.get("alGauge"));
	alAngle = GmCommonClass.parseNullArrayList((ArrayList) hmDropdown.get("alAngle"));
	alPressure = GmCommonClass.parseNullArrayList((ArrayList) hmDropdown.get("alPressure"));
	alAllUom =  alSizeUOM;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: PD Input Screen </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmPartNumberSetup.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>
var arrayLength = new Array();
var arrayArea = new Array();
var arrayWeight = new Array();
var arrayTotalVol = new Array();
var arrayGauge = new Array();
var arrayAngle = new Array();
var arrayPressure = new Array();
var arrayFullUom = new Array();
<%-- var strSerialChkBox = '<%=strserialNumFl%>'; --%>
var lblContainsLatex='<fmtPartPDSetup:message key="LBL_CONTAINS_LATEX"/>';
var lblRequiresSterilization='<fmtPartPDSetup:message key="LBL_REQUIRES_STERILIZATION"/>';
var lblIsDevAvail = '<fmtPartPDSetup:message key="LBL_IS_DEVICE_AVAILABLE"/>';
var lblSterilizationMethod = '<fmtPartPDSetup:message key="LBL_STERILIZATION_METHOD"/>';

//dropdown values for Length
<%for (int i = 0; i < alLength.size(); i++) {
				hmDropdownSet = (HashMap) alLength.get(i);%>
arrayLength.push('<%=hmDropdownSet.get("CODEID")%>'+","+'<%=hmDropdownSet.get("CODENM")%>');
<%}%>
//dropdown values for Area
<%for (int i = 0; i < alArea.size(); i++) {
				hmDropdownSet = (HashMap) alArea.get(i);%>
arrayArea.push('<%=hmDropdownSet.get("CODEID")%>'+","+'<%=hmDropdownSet.get("CODENM")%>');
<%}%>
//dropdown values for Weight
<%for (int i = 0; i < alWeight.size(); i++) {
				hmDropdownSet = (HashMap) alWeight.get(i);%>
arrayWeight.push('<%=hmDropdownSet.get("CODEID")%>'+","+'<%=hmDropdownSet.get("CODENM")%>');
<%}%>
//dropdown values for TotalVolume
<%for (int i = 0; i < alTotalVolume.size(); i++) {
				hmDropdownSet = (HashMap) alTotalVolume.get(i);%>
arrayTotalVol.push('<%=hmDropdownSet.get("CODEID")%>'+","+'<%=hmDropdownSet.get("CODENM")%>');
<%}%>
//dropdown values for Gauge
<%for (int i = 0; i < alGauge.size(); i++) {
				hmDropdownSet = (HashMap) alGauge.get(i);%>
arrayGauge.push('<%=hmDropdownSet.get("CODEID")%>'+","+'<%=hmDropdownSet.get("CODENM")%>');
<%}%>
//dropdown values for Angle
<%for (int i = 0; i < alAngle.size(); i++) {
				hmDropdownSet = (HashMap) alAngle.get(i);%>
arrayAngle.push('<%=hmDropdownSet.get("CODEID")%>'+","+'<%=hmDropdownSet.get("CODENM")%>');
<%}%>

//dropdown values for Pressure
<%for (int i = 0; i < alPressure.size(); i++) {
				hmDropdownSet = (HashMap) alPressure.get(i);%>
arrayPressure.push('<%=hmDropdownSet.get("CODEID")%>'+","+'<%=hmDropdownSet.get("CODENM")%>');
<%}%>
//dropdown values for UOM
<%for (int i = 0; i < alSizeUOM.size(); i++) {
				hmDropdownSet = (HashMap) alSizeUOM.get(i);%>
arrayFullUom.push('<%=hmDropdownSet.get("CODEID")%>'+","+'<%=hmDropdownSet.get("CODENM")%>');
<%}%>
</script>
</HEAD>
<%!public ArrayList getUOMType(String strType) {
		ArrayList alUom = new ArrayList();
		/* 104032 Circumference
			 104033 Depth
			 104035 Outer Diameter
			 104036 Height
			 104037 Length
			 104038 Lumen/Inner Diameter
			 104041 Width
			 104044 Pore Size */
		if (strType.equals("104032") || strType.equals("104033")
				|| strType.equals("104035") || strType.equals("104036")
				|| strType.equals("104037") || strType.equals("104038")
				|| strType.equals("104041") || strType.equals("104044")) {
			alUom = alLength;
		} else if (strType.equals("104045")) { // 104045 Area/Surface Area
			alUom = alArea;
		} else if (strType.equals("104042")) { // 104042 Weight
			alUom = alWeight;
		} else if (strType.equals("104040")) { // 104040 Total Volume
			alUom = alTotalVolume;
		} else if (strType.equals("104034") || strType.equals("104039")) { // 104034 Catheter Gauge // 104039	Needle Gauge
			alUom = alGauge;
		} else if (strType.equals("104046")) { // 104046 Angle
			alUom = alAngle;
		} else if (strType.equals("104043")) { // 104043 Pressure
			alUom = alPressure;
		} else if (strType.equals("104047")) { // 104047 Other
			alUom = alAllUom;
		}
		return alUom;
	}%>
<BODY onload="fnPdOnLoad();">
<html:form action="/gmUDIProductDevelopment.do?method=pdPartUDISetup">

<html:hidden property="haction" name="frmUDIProductDevelopment"/>
<html:hidden property="strinputStr" value="" name="frmUDIProductDevelopment"/>
<html:hidden property="partNumber" name="frmUDIProductDevelopment"/>
<html:hidden property="printProjectId" name="frmUDIProductDevelopment" value="<%=strProjectId %>"/>
<input type="hidden" name="hSterileFl" value="<%=strPartSterileFlag%>">

		<table border="0" width="948px" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" class="RightDashBoardHeader" colspan="2"><fmtPartPDSetup:message key="LBL_PD_INPUT_SCREEN"/></td>
					<fmtPartPDSetup:message key="IMG_ALT_HELP" var="VarHelp"/>
				<td align="right" class=RightDashBoardHeader><img id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${VarHelp}' width='16'
					height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />&nbsp;
				</td>	
			</tr>
			<tr>
				<td class="Line" height="1" colspan="3"></td>
			</tr>
			<tr>
				<td align="right" class="RightTableCaption" HEIGHT="23"><fmtPartPDSetup:message key="LBL_PART_NUMBER"/>:</td>
				<td colspan="2" class="RightText">&nbsp;<%=strPartNum%>&nbsp;<%=strPartNumHisFlag%></td>
			</tr>
			<tr>
				<td colspan="3" class="LLine"></td>
			</tr>
			<tr class="Shade">
				<td align="right" class="RightTableCaption" HEIGHT="23"><fmtPartPDSetup:message key="LBL_PART_DESCRIPTION"/>:</td>
				<td class="RightText" colspan="2">&nbsp;<%=strPartDesc%>&nbsp;<%=strPartDescHisFlag%></td>
			</tr>
			<tr>
				<td colspan="3" class="LLine"></td>
			</tr>
			<tr>
				<td align="right" class="RightTableCaption" HEIGHT="23"><fmtPartPDSetup:message key="LBL_PROJECT_ID"/>:</td>
				<td class="RightText" colspan="2">&nbsp;<%=strProjectId%>&nbsp;<%=strProjectIdHisFlag%></td>
			</tr>
			<tr>
				<td colspan="3" class="LLine"></td>
			</tr>
			<tr class="Shade">
				<fmtPartPDSetup:message key="LBL_CONTAINS_LATEX" var="varContainsLatex"/>
				<td align="right" class="RightTableCaption" HEIGHT="23"><gmjsp:label
						type="MandatoryText" SFLblControlName="${varContainsLatex}:" td="false" /></td>
				<td colspan="2" class="RightText">&nbsp;<gmjsp:dropdown
						controlName="contains" controlId="size"
						seletedValue="<%=strContainsLatex%>" value="<%=alContainLatex%>"
						codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" />&nbsp;<%=strContainsLatexHisFlag%>
				</td>
			</tr>
			<tr>
				<td colspan="3" class="LLine"></td>
			</tr>
			<tr>
			<fmtPartPDSetup:message key="LBL_REQUIRES_STERILIZATION" var="varRequiresSterilization"/>
				<td align="right" class="RightTableCaption" HEIGHT="23"><gmjsp:label
						type="MandatoryText" SFLblControlName="${varRequiresSterilization}:" td="false" /></td>
				<td colspan="2" class="RightTableCaption">&nbsp;<gmjsp:dropdown
						controlName="ReqSteril" seletedValue="<%=strReqSteril%>"
						value="<%=alReqSteril%>" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" />&nbsp;<%=strReqSterilHisFlag%></td>
			</tr>
			<tr>
				<td colspan="3" class="LLine"></td>
			</tr>
			<tr class="Shade">
			<fmtPartPDSetup:message key="LBL_STERILIZATION_METHOD" var="varSterilizationMethod"/>
				<td align="right" class="RightTableCaption" HEIGHT="23"><gmjsp:label
						type="RegularText" SFLblControlName="${varSterilizationMethod}:" td="false" /></td>
				<td colspan="2" class="RightTableCaption">&nbsp;<gmjsp:dropdown
						controlName="SterilMthod" seletedValue="<%=strSterilMthod%>"
						value="<%=alSterilMthod%>" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" />&nbsp;<%=strSterilMthodHisFlag%></td>
			</tr>
			<tr>
				<td colspan="3" class="LLine"></td>
			</tr>
			<tr>
			<fmtPartPDSetup:message key="LBL_IS_DEVICE_AVAILABLE" var="varIsDeviceAvailable"/>
				<td align="right" class="RightTableCaption" HEIGHT="23"><gmjsp:label
						type="MandatoryText" SFLblControlName="${varIsDeviceAvailable}:" td="false" /></td>
				<td colspan="2" class="RightTableCaption">&nbsp;<gmjsp:dropdown
						controlName="IsDevice" seletedValue="<%=strIsDevice%>"
						value="<%=alIsDevice%>" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" onChange="fnChangeDeviceSize(this.form,this);" />&nbsp;<%=strIsDeviceHisFlag%></td>
			</tr>
			<tr>
				<td colspan="3" class="LLine"></td>
			</tr>
			<%-- <tr class="Shade">
			<fmtPartPDSetup:message key="LBL_SERIAL_NEEDED" var="varSerialNumberNeeded"/>
				<td align="right" class="RightTableCaption" HEIGHT="23"><gmjsp:label
						type="RegularText" SFLblControlName=" ${varSerialNumberNeeded}?:" td="false" /></td>
				<td colspan="2" class="RightTableCaption">&nbsp;<input type="checkbox" id="serialNumFl" <%=strserialNumFl%>></td>
			</tr> --%>
			<tr>
				<td class="Line" height="1" colspan="3"></td>
			</tr>
			<tr>
				<td height="25" class="ShadeRightTableCaption" colspan="3">&nbsp;<fmtPartPDSetup:message key="LBL_SIZE"/>
					1</td>
			</tr>
			<tr>
				<td colspan="3" class="Line"></td>
			</tr>
			<tr>
				<fmtPartPDSetup:message key="LBL_TYPE" var="varType"/>
				<td align="right" class="RightTableCaption" HEIGHT="23"><gmjsp:label
						type="RegularText" SFLblControlName="${varType}:" td="false" /></td>
				<td colspan="2" class="RightTableCaption">&nbsp;<gmjsp:dropdown
						controlName="Size1Type" seletedValue="<%=strSize1Type%>"
						value="<%=alSizeType%>" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" onChange="fnChangeSizeType(this.form,this,'Size1UOM', 'size1_type', 'size1_value');" />&nbsp;<%=strSize1TypeHisFlag%></td>
			</tr>
			<tr>
				<td colspan="3" class="LLine"></td>
			</tr>
			<tr class="Shade">
				<fmtPartPDSetup:message key="LBL_SIZE_TYPE_TEXT" var="varSizeTypeText"/>
				<td align="right" class="RightTableCaption" HEIGHT="23"><gmjsp:label
						type="RegularText" SFLblControlName="${varSizeTypeText}:" td="false" /></td>
				<td colspan="2" class="RightTableCaption">&nbsp;<input
					type="text" value="<%=strSize1TypeText%>" name="size1_type">&nbsp;<%=strSize1TypeTextHisFlag%></td>
			</tr>
			<tr>
				<td colspan="3" class="LLine"></td>
			</tr>
			<tr>
				<fmtPartPDSetup:message key="LBL_VALUE" var="varValue"/>
				<td align="right" class="RightTableCaption" HEIGHT="23"><gmjsp:label
						type="RegularText" SFLblControlName="${varValue}:" td="false" /></td>
				<td colspan="2" class="RightTableCaption">&nbsp;<input
					type="text" value="<%=strSize1Value%>" name="size1_value">&nbsp;<%=strSize1ValueHisFlag%></td>
			</tr>
			<tr>
				<td colspan="3" class="LLine"></td>
			</tr>
			<tr class="Shade">
				<fmtPartPDSetup:message key="LBL_U_OF_M" var="varUOfM"/>
				<td align="right" class="RightTableCaption" HEIGHT="23"><gmjsp:label
						type="RegularText" SFLblControlName="${varUOfM}:" td="false" /></td>
				<td colspan="2" class="RightTableCaption">&nbsp;<gmjsp:dropdown
						controlName="Size1UOM" seletedValue="<%=strSize1UOM%>"
						value="<%=getUOMType(strSize1Type)%>" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" />&nbsp;<%=strSize1UOMHisFlag%></td>
			</tr>
			<tr>
				<td class="Line" height="1" colspan="3"></td>
			</tr>
			<tr>
				<td height="25" class="ShadeRightTableCaption" colspan="3">&nbsp;<fmtPartPDSetup:message key="LBL_SIZE"/>
					2</td>
			</tr>
			<tr>
				<td colspan="3" class="Line"></td>
			</tr>
			<tr>
				<fmtPartPDSetup:message key="LBL_TYPE" var="varType"/>
				<td align="right" class="RightTableCaption" HEIGHT="23"><gmjsp:label
						type="RegularText" SFLblControlName="${varType}:" td="false" /></td>
				<td colspan="2" class="RightTableCaption">&nbsp;<gmjsp:dropdown
						controlName="Size2Type" seletedValue="<%=strSize2Type%>"
						value="<%=alSizeType%>" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" onChange="fnChangeSizeType(this.form,this,'Size2UOM', 'size2_type', 'size2_value');"/>&nbsp;<%=strSize2TypeHisFlag%></td>
			</tr>
			<tr>
				<td colspan="3" class="LLine"></td>
			</tr>
			<tr class="Shade">
				<fmtPartPDSetup:message key="LBL_SIZE_TYPE_TEXT" var="varSizeTypeText"/>
				<td align="right" class="RightTableCaption" HEIGHT="23"><gmjsp:label
						type="RegularText" SFLblControlName="${varSizeTypeText}:" td="false" /></td>
				<td colspan="2" class="RightTableCaption">&nbsp;<input
					type="text" value="<%=strSize2TypeText%>" name="size2_type" >&nbsp;<%=strSize2TypeTextHisFlag%></td>
			</tr>
			<tr>
				<td colspan="3" class="LLine"></td>
			</tr>
			<tr>
				<fmtPartPDSetup:message key="LBL_VALUE" var="varValue"/>
				<td align="right" class="RightTableCaption" HEIGHT="23"><gmjsp:label
						type="RegularText" SFLblControlName="${varValue}:" td="false" /></td>
				<td colspan="2" class="RightTableCaption">&nbsp;<input
					type="text" value="<%=strSize2Value%>" name="size2_value">&nbsp;<%=strSize2ValueHisFlag%></td>
			</tr>
			<tr>
				<td colspan="3" class="LLine"></td>
			</tr>
			<tr class="Shade">
				<fmtPartPDSetup:message key="LBL_U_OF_M" var="varUOfM"/>
				<td align="right" class="RightTableCaption" HEIGHT="23"><gmjsp:label
						type="RegularText" SFLblControlName="${varUOfM}:" td="false" /></td>
				<td colspan="2" class="RightTableCaption">&nbsp;<gmjsp:dropdown
						controlName="Size2UOM" seletedValue="<%=strSize2UOM%>"
						value="<%=getUOMType(strSize2Type)%>" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" />&nbsp;<%=strSize2UOMHisFlag%></td>
			</tr>
			<tr>
				<td class="Line" height="1" colspan="3"></td>
			</tr>
			<tr>
				<td height="25" class="ShadeRightTableCaption" colspan="3">&nbsp;<fmtPartPDSetup:message key="LBL_SIZE"/>
					3</td>
			</tr>
			<tr>
				<td colspan="3" class="Line"></td>
			</tr>
			<tr>
				<fmtPartPDSetup:message key="LBL_TYPE" var="varType"/>
				<td align="right" class="RightTableCaption" HEIGHT="23"><gmjsp:label
						type="RegularText" SFLblControlName="${varType}:" td="false" /></td>
				<td colspan="2" class="RightTableCaption">&nbsp;<gmjsp:dropdown
						controlName="Size3Type" seletedValue="<%=strSize3Type%>"
						value="<%=alSizeType%>" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" onChange="fnChangeSizeType(this.form,this,'Size3UOM', 'size3_type', 'size3_value');" />&nbsp;<%=strSize3TypeHisFlag%></td>
			</tr>
			<tr>
				<td colspan="3" class="LLine"></td>
			</tr>
			<tr class="Shade">
				<fmtPartPDSetup:message key="LBL_SIZE_TYPE_TEXT" var="varSizeTypeText"/>
				<td align="right" class="RightTableCaption" HEIGHT="23"><gmjsp:label
						type="RegularText" SFLblControlName="${varSizeTypeText}:" td="false" /></td>
				<td colspan="2" class="RightTableCaption">&nbsp;<input
					type="text" value="<%=strSize3TypeText%>" name="size3_type">&nbsp;<%=strSize3TypeTextHisFlag%></td>
			</tr>
			<tr>
				<td colspan="3" class="LLine"></td>
			</tr>
			<tr>
				<fmtPartPDSetup:message key="LBL_VALUE" var="varValue"/>
				<td align="right" class="RightTableCaption" HEIGHT="23"><gmjsp:label
						type="RegularText" SFLblControlName="${varValue}:" td="false" /></td>
				<td colspan="2" class="RightTableCaption">&nbsp;<input
					type="text" value="<%=strSize3Value%>" name="size3_value">&nbsp;<%=strSize3ValueHisFlag%></td>
			</tr>
			<tr>
				<td colspan="3" class="LLine"></td>
			</tr>
			<tr class="Shade">
				<fmtPartPDSetup:message key="LBL_U_OF_M" var="varUOfM"/>
				<td align="right" class="RightTableCaption" HEIGHT="23"><gmjsp:label
						type="RegularText" SFLblControlName="${varUOfM}:" td="false" /></td>
				<td colspan="2" class="RightTableCaption">&nbsp;<gmjsp:dropdown
						controlName="Size3UOM" seletedValue="<%=strSize3UOM%>"
						value="<%=getUOMType(strSize3Type)%>" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" />&nbsp;<%=strSize3UOMHisFlag%></td>
			</tr>
			<tr>
				<td class="Line" height="1" colspan="3"></td>
			</tr>
			<tr>
				<td height="25" class="ShadeRightTableCaption" colspan="3">&nbsp;<fmtPartPDSetup:message key="LBL_SIZE"/>
					4</td>
			</tr>
			<tr>
				<td colspan="3" class="Line"></td>
			</tr>
			<tr>
				<fmtPartPDSetup:message key="LBL_TYPE" var="varType"/>
				<td align="right" class="RightTableCaption" HEIGHT="23"><gmjsp:label
						type="RegularText" SFLblControlName="${varType}:" td="false" /></td>
				<td colspan="2" class="RightTableCaption">&nbsp;<gmjsp:dropdown
						controlName="Size4Type" seletedValue="<%=strSize4Type%>"
						value="<%=alSizeType%>" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" onChange="fnChangeSizeType(this.form,this,'Size4UOM', 'size4_type', 'size4_value');" />&nbsp;<%=strSize4TypeHisFlag%></td>
			</tr>
			<tr>
				<td colspan="3" class="LLine"></td>
			</tr>
			<tr class="Shade">
				<fmtPartPDSetup:message key="LBL_SIZE_TYPE_TEXT" var="varSizeTypeText"/>
				<td align="right" class="RightTableCaption" HEIGHT="23"><gmjsp:label
						type="RegularText" SFLblControlName="${varSizeTypeText}:" td="false" /></td>
				<td colspan="2" class="RightTableCaption">&nbsp;<input
					type="text" value="<%=strSize4TypeText%>" name="size4_type">&nbsp;<%=strSize4TypeTextHisFlag%></td>
			</tr>
			<tr>
				<td colspan="3" class="LLine"></td>
			</tr>
			<tr>
				<fmtPartPDSetup:message key="LBL_VALUE" var="varValue"/>
				<td align="right" class="RightTableCaption" HEIGHT="23"><gmjsp:label
						type="RegularText" SFLblControlName="${varValue}:" td="false" /></td>
				<td colspan="2" class="RightTableCaption">&nbsp;<input
					type="text" value="<%=strSize4Value%>" name="size4_value">&nbsp;<%=strSize4ValueHisFlag%></td>
			</tr>
			<tr>
				<td colspan="3" class="LLine"></td>
			</tr>
			<tr class="Shade">
				<fmtPartPDSetup:message key="LBL_U_OF_M" var="varUOfM"/>
				<td align="right" class="RightTableCaption" HEIGHT="23"><gmjsp:label
						type="RegularText" SFLblControlName="${varUOfM}:" td="false" /></td>
				<td colspan="2" class="RightTableCaption">&nbsp;<gmjsp:dropdown
						controlName="Size4UOM" seletedValue="<%=strSize4UOM%>"
						value="<%=getUOMType(strSize4Type)%>" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" />&nbsp;<%=strSize4UOMHisFlag%></td>
			</tr>
			<tr>
				<td class="Line" height="1" colspan="8"></td>
			</tr>
			<tr>
				<td colspan="8"><jsp:include page="/common/GmIncludeLog.jsp">
						<jsp:param name="FORMNAME" value="frmUDIProductDevelopment" />
						<jsp:param name="ALNAME" value="alLogReasons" />
						<jsp:param name="LogMode" value="Edit" />
						<jsp:param name="Mandatory" value="yes" />
					</jsp:include></td>
			</tr>

			<tr height="30">
				<fmtPartPDSetup:message key="BTN_SUBMIT" var="varSubmit"/>
				<fmtPartPDSetup:message key="BTN_PRINT" var="varPrint"/>
				<td align="center" colspan="3"><gmjsp:button value="${varSubmit}"
						gmClass="button" name="btn_submit" style="width: 6em"
						buttonType="Save" disabled="<%=strDisable%>"
						onClick="fnPDSubmit(this.form);" />&nbsp;&nbsp;&nbsp; <gmjsp:button
						value="${varPrint}" gmClass="button" name="btn_Print" style="width: 6em"
						buttonType="Load" onClick="fnPDPrint(this.form);" />&nbsp;&nbsp;&nbsp;
				</td>
			</tr>
		</table>
	</html:form>
</BODY>
<%@ include file="/common/GmFooter.inc" %>

</HTML>
