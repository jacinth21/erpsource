 <%@page import="com.globus.common.beans.GmCalenderOperations"%>
<%
/*******************************************************************************************************
 * File		 		: GmSetBundleRpt.jsp
 * Desc		 		: This JSP is like a container jsp for the screen to show set bundle report
 * Version	 		: 1.0
 * author			: Agilan 
*********************************************************************************************************/
%>

<%@ page language="java"%>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtSetBundleRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- Quality\GmSetBundleRpt.jsp --><fmtSetBundleRpt:setLocale value="<%=strLocale%>"/>
<fmtSetBundleRpt:setBundle basename="properties.labels.quality.GmSetBundleReport"/>
<bean:define id="setBundleName" name="frmSetBundleRpt" property="setBundleName" type="java.lang.String"></bean:define>
<bean:define id="searchsetBundleName" name="frmSetBundleRpt" property="searchsetBundleName" type="java.lang.String"></bean:define>

<%    
String strQualJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_QUALITY");
response.setContentType("text/html; charset=UTF-8");
response.setCharacterEncoding("UTF-8");
String strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("SET_BUNLDE_REPORT"));
String strGridXmlData = (String)request.getAttribute("GRIDXMLDATA");
ArrayList alRptDetails = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALRPTDETAILS"));
String strSessApplDateFmt = strGCompDateFmt;
String strTodaysDate = GmCalenderOperations.getCurrentDate(strSessApplDateFmt);	
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Globus Medical: Set Bundle Report</title>
<link rel="stylesheet" type="text/css" href="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<style type="text/css">
table.DtTable1030 {
	margin: 0 0 0 0;
	width: 1030px;
	border: 1px solid  #676767;
}
</style>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strQualJsPath%>/GmSetBundleReport.js"></script> 
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script  language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script> 

<script>
var format = "<%=strSessApplDateFmt%>";
var todaysDate 	= '<%=strTodaysDate%>';
var objGridData ='<%=strGridXmlData%>';
</script>

</head>
<body leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<html:form action="/gmSetBundleMappingReport.do?method=setBundleRpt">
	<table border="0" class="DtTable1030" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" width="90%">&nbsp;<fmtSetBundleRpt:message key="LBL_SET_BUDLE_REPORT"/></td>
			<td  align="right" class="RightDashBoardHeader" width="10%">
				<fmtSetBundleRpt:message key="IMG_ALT_HELP" var = "varHelp"/>
				<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');"/>
			</td>
		</tr>
     	<tr><td colspan="8" height="1" bgcolor="#cccccc"></td></tr>     	
     	<tr><td colspan="8">
     	<table border="0" width="100%" cellspacing="0" cellpadding="0">
	     	<tr height="25">
	     	
	     		<td height="27" class="RightTableCaption" align="right"><font color="red">*</font> <fmtSetBundleRpt:message key="LBL_SET_BUNDLE_NAME"/></td>
			
				<td><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="setBundleName" />
					<jsp:param name="METHOD_LOAD" value="loadSetBundleNmList" />
					<jsp:param name="WIDTH" value="400" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="TAB_INDEX" value="4"/>
					<jsp:param name="CONTROL_NM_VALUE" value="<%=searchsetBundleName%>" /> 
					<jsp:param name="CONTROL_ID_VALUE" value="<%=setBundleName%>" />  					
					<jsp:param name="SHOW_DATA" value="200" />
				 <jsp:param name="AUTO_RELOAD" value="" /> 
					</jsp:include></td>	
		
			<td height="27" class="RightTableCaption" align="right" ><fmtSetBundleRpt:message key="LBL_SET_ID"/> </td>
			
			<td >:&nbsp;<gmjsp:dropdown controlName="setId" SFFormName="frmSetBundleRpt" SFSeletedValue="setId" SFValue="alSetId" codeId="ID" codeName="ID" defaultValue="[Choose one]"  /></td>
	
	     		</tr>
	     		<tr><td colspan="8" height="1" bgcolor="#cccccc"></td></tr>
	     		<tr class="Shade" height="25">
	     		<td height="27" class="RightTableCaption" align="right" ><fmtSetBundleRpt:message key="LBL_DIVISION"/> </td>
			
			<td >:&nbsp;<gmjsp:dropdown controlName="divName" SFFormName="frmSetBundleRpt" SFSeletedValue="divName" SFValue="alDivList" codeId="DIVISION_ID" codeName="DIVISION_NAME" defaultValue="[Choose one]"  /></td>
		
			<td height="27" class="RightTableCaption" align="right"><fmtSetBundleRpt:message key="LBL_STATUS"/> </td>
			
			<td >:&nbsp;<gmjsp:dropdown controlName="status" SFFormName="frmSetBundleRpt" SFSeletedValue="status" SFValue="alStatus" codeId="CODEID" codeName="CODENM" defaultValue="[Choose one]" /></td>
	     		
				<td height="30" align="left">&nbsp;<fmtSetBundleRpt:message key="BTN_LOAD" var="varLoad"/><gmjsp:button name="Btn_Load" value="${varLoad}" gmClass="button" style="width:4em"  onClick="javascript:fnLoadDashDetails();" tabindex="2" buttonType="Load" />
				</td>				
	     	</tr>
	     	<tr><td colspan="8" height="1" bgcolor="#cccccc"></td></tr>
     	</table>
     	</td></tr>
      <%if(strGridXmlData  != null && alRptDetails.size() > 0){%>  
     	<tr>
				<td colspan="8">
					<div id="SetBundleRpt" style="height: 1030px; width:1080px"></div>
				</td>			
		</tr>
 	
     	<tr><td colspan="8" height="1" bgcolor="#cccccc"></td></tr>
     		<tr>
			<td colspan="8" align="center">
			    <div class='exportlinks'><fmtSetBundleRpt:message key="DIV_EXPORT_OPT"/> : <img src='img/ico_file_excel.png' onclick="fnExcel();"/>&nbsp;<a href="#" onclick="fnExcel();"><fmtSetBundleRpt:message key="DIV_EXCEL"/></a></div>
			</td>
		</tr>
		 <%}else{%>
		 <tr>
		<td colspan="8" align="center" class="RightText"><fmtSetBundleRpt:message key="LBL_NO_DATA_AVAILABLE"/></td>
		</tr>
		<%} %>	
	</table>
<%@ include file="/common/GmFooter.inc"%>
</html:form>
</body>
</html>