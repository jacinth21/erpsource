<%
/**********************************************************************************
 * File		 		: GmPartRegulatoryReport.jsp
 * Desc		 		: This screen is used fetch the Regulatory Report information
 * Version	 		: 1.0
 * author			: rdinesh
************************************************************************************/
%>
<!-- quality\GmPartRegulatoryReport.jsp -->
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtPartRegulatoryReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtPartRegulatoryReport:setLocale value="<%=strLocale%>"/>
<fmtPartRegulatoryReport:setBundle basename="properties.labels.quality.GmPartRegulatoryReport"/>
<bean:define id="gridData" name="frmPartParameterDtls" property="gridXml" type="java.lang.String"></bean:define>

<%
String strQualJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_QUALITY");
	String strWikiTitle = GmCommonClass.getWikiTitle("PART_REGULATORY_REPORT");
	String strSearch = (String)request.getAttribute("hSearch")==null?"":(String)request.getAttribute("hSearch");
%>

<html>
<head>
<title>Globus Medical:Part Regulatory Report</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strQualJsPath%>/GmPartRegulatoryReport.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script>
	var objGridData = '<%=gridData%>';
</script>
<style type="text/css">
div.gridbox_dhx_skyblue table.hdr td {
       text-align:center;
}
</style>
</head>
<body leftmargin="20" topmargin="10" onload="fnOnPageLoad()" onkeypress="fnEnter()">
<html:form action="/gmPartParameterDtls.do?method=udiRegulatoryReports">
<html:hidden property="strOpt" value="frmPartParameterDtls"/>
<input type="hidden" name="hSearch" value="<%=strSearch%>">
<table class="DtTable1500" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" class="RightDashBoardHeader" colspan="5">&nbsp;<fmtPartRegulatoryReport:message key="LBL_REGULATORY_REPORT"/></td>
				<td align="right" class=RightDashBoardHeader><fmtPartRegulatoryReport:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
 					title='${varHelp}' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
			</tr>
			<tr>
				<td class="RightTableCaption" HEIGHT="30" align="right">
						<fmtPartRegulatoryReport:message key="LBL_PROJECT_ID" var="varProjectID"/><gmjsp:label type="RegularText" SFLblControlName="${varProjectID}:" td="false" /></td>
				<td >&nbsp;<gmjsp:dropdown controlName="projectId" SFFormName="frmPartParameterDtls" SFSeletedValue="projectId"
						SFValue="alProjectId" codeId = "ID"  codeName = "IDNAME" defaultValue= "[Choose One]" tabIndex="1" width="300"/>
						</td>
				<td class="RightTableCaption" HEIGHT="30" align="right" >
						<fmtPartRegulatoryReport:message key="LBL_PART" var="varPart"/><gmjsp:label type="RegularText" SFLblControlName="${varPart}:" td="false" /></td>
				<td >&nbsp;<html:text property="regulatoryPartNum" styleClass="InputArea" name="frmPartParameterDtls" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="2" size="25"/>&nbsp;<select name="Cbo_Search" class="RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
						<option value="0" ><fmtPartRegulatoryReport:message key="LBL_CHOOSE_ONE"/>
						<option value="LIT" ><fmtPartRegulatoryReport:message key="LBL_LITERAL"/>
						<option value="LIKEPRE" ><fmtPartRegulatoryReport:message key="LBL_LIKE_PREFIX"/>
						<option value="LIKESUF" ><fmtPartRegulatoryReport:message key="LBL_LIKE_SUFFIX"/>
				</select>
				</td>
			</tr>
			<tr><td colspan="7" height="1" class="LLine"></td></tr>
			<tr class="shade">
				<td class="RightTableCaption" HEIGHT="30" align="right">
				<fmtPartRegulatoryReport:message key="LBL_PART_DESCRIPTION" var="varPartDescription"/><gmjsp:label type="RegularText" SFLblControlName="${varPartDescription}:" td="false" /></td>
				<td>&nbsp;<html:text property="regulatoryPartDesc" styleClass="InputArea" name="frmPartParameterDtls" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="3" size="47"/></td>
				<td class="RightTableCaption" HEIGHT="30" align="right">
				<fmtPartRegulatoryReport:message key="LBL_SUBMISSION" var="varSubmission"/><gmjsp:label type="RegularText" SFLblControlName="${varSubmission}:" td="false" /></td>
				<td colspan="3">&nbsp;<html:text property="submissionNo" styleClass="InputArea" name="frmPartParameterDtls" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="4" size="25"/>
				&nbsp;&nbsp;&nbsp;<fmtPartRegulatoryReport:message key="BTN_LOAD" var="varLoad"/><gmjsp:button name="Btn_Load" value="${varLoad}" gmClass="button" style="width: 5em; height: 23px; font-family: Trebuchet MS, arial, sans-serif;font-size : 9pt" onClick="javascript:fnLoad();" tabindex="5" buttonType="Load" /></td>
			</tr>	
			<%if(!gridData.equals("")) {%> 
				<tr>
					<td colspan="9"><div  id="dataGridDiv" width="1500px" height="450px"></div>
					<div id="pagingArea"></div></td>
				</tr>
				<tr>						
					<td align="center" colspan="7"><br>
	             			<div class='exportlinks'><fmtPartRegulatoryReport:message key="DIV_EXPORT_OPT"/>: <img src='img/ico_file_excel.png' />&nbsp; <a href="#" onclick="fnExport();"> <fmtPartRegulatoryReport:message key="DIV_EXCEL"/> </a>	</div>                         
	       			</td>
       			<tr>
			<%}else  if(gridData.indexOf("cell") != -1){%>
				<tr><td colspan="9" height="1" class="LLine"></td></tr>
				<tr height="25px"><td colspan="7" align="center" class="RightText" ><fmtPartRegulatoryReport:message key="LBL_NOTHING_FOUND_TO_DISPLAY"/></td></tr>
			<%}else{ %>
				<tr><td colspan="9" height="1" class="LLine"></td></tr>
				<tr height="25px"><td colspan="9" align="center" class="RightText" ><fmtPartRegulatoryReport:message key="LBL_NO_DATA"/></td></tr>
			<%} %>	
</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>