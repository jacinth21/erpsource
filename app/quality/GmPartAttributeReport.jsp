
<!-- quality\GmPartAttributeReport.jsp -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<%@ taglib prefix="fmtPartAttributeReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtPartAttributeReport:setLocale value="<%=strLocale%>"/>
<fmtPartAttributeReport:setBundle basename="properties.labels.quality.GmPartAttributeReport"/>


<bean:define id="gridData" name="frmPartAttributeBulkSetup" property="gridData" type="java.lang.String"/> 
<bean:define id ="attributeType" name="frmPartAttributeBulkSetup" property="attributeType" type="java.lang.String"/> 
<bean:define id ="alSystemType" name="frmPartAttributeBulkSetup" property="alSystemType" type="java.util.ArrayList"/>
<bean:define id ="setId" name="frmPartAttributeBulkSetup" property="setId" type="java.lang.String"/> 
<bean:define id ="accessAllComp" name="frmPartAttributeBulkSetup" property="accessAllComp" type="java.lang.String"/> 
<bean:define id="jsonString" name="frmPartAttributeBulkSetup" property="jsonString" type="java.lang.String"/>
<%
String strQualJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_QUALITY");
String strWikiTitle = GmCommonClass.getWikiTitle("PART_ATTRIBUTE_REPORT_BULK");


%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Part Attribute - Bulk </TITLE>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">

 

<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script> 
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strQualJsPath%>/GmPartAttributeBulk.js"></script>
 
 

  <script>
	var objGridData = '<%=gridData%>';
	var jsonCompFl = '<%=jsonString%>';
	
  </script> 

</HEAD>
<BODY leftmargin="20" topmargin="10"  onload = "fnOnPageLoadRpt()">

 <html:form action="/gmpartAttrBlkSetup.do?method=loadPartAttribute">
<html:hidden property="strOpt" value="frmPartAttributeBulkSetup"/>
<html:hidden property="screenType" name="frmPartAttributeBulkSetup"/>
<table border="0"  height="60" class="DtTable1100" cellspacing="0" cellpadding="0">
		<tr  height="25"><td class="RightDashBoardHeader" colspan="3">&nbsp;<fmtPartAttributeReport:message key="LBL_PART_ATTRIBUTE_REPORT"/></td>
		 
		     <td class="RightDashBoardHeader" align="right">
				
<fmtPartAttributeReport:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
					height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');"/>&nbsp;
			    </td> 
		 
		</tr>
		
			
				
					<tr class="oddshade" height="28">
						 <td  class="RightTableCaption"  align="right"><fmtPartAttributeReport:message key="LBL_ATTRIBUTE_TYPE"/>:&nbsp;</td>
						 <td class="RightTableCaption"> <gmjsp:dropdown controlName="attributeType" SFFormName="frmPartAttributeBulkSetup" SFSeletedValue="attributeType" 
						codeId="CODENMALT" codeName="CODENM" tabIndex="1" SFValue="alAttrType" defaultValue="[Choose One]" onChange="javascript:fnSetCompanyFlagRpt();"/>
						</td>
						
						 <td class="RightTableCaption" align="right"><fmtPartAttributeReport:message key="LBL_SYSTEM"/>:&nbsp; </td>
						<td>
						<gmjsp:dropdown controlName="setId" SFFormName="frmPartAttributeBulkSetup" SFSeletedValue="setId" 
						codeId="ID" codeName="NAME" tabIndex="1" SFValue="alSystemType" defaultValue="[Choose One]"/>
							
						</td>
						
						
					</tr>
					<tr>
						<td  height="30"class="RightTableCaption"  align="right"><fmtPartAttributeReport:message key="LBL_PARTS"/>:&nbsp;</td>
						<td  height="30"> <html:text name="frmPartAttributeBulkSetup" property="partNums" styleId="PartValue" styleClass="InputArea"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/></td>
									
						<td class="RightTableCaption" align="right" height="30"><fmtPartAttributeReport:message key="LBL_APPLICABLE_FOR_ALL_COMPANIES"/>:&nbsp;</td>
						<td  height="30">
						<html:checkbox name="frmPartAttributeBulkSetup" property="chkAllcompany"/>&nbsp;&nbsp;&nbsp;
									 &nbsp;&nbsp;<fmtPartAttributeReport:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;${varLoad}&nbsp;"gmClass="button" buttonType="Load" onClick="fnReload()" />&nbsp;&nbsp; 
						</td>
								
					</tr>
					<%
				if (gridData.indexOf("cell") != -1) {
			%>
								

				<tr>
				<td colspan="4"><div  id="dataGridDiv" height="650px"></div></td></tr>
				<tr>						
					<td align="center" colspan="7"><br>
					
	             			<div class='exportlinks'><fmtPartAttributeReport:message key="DIV_EXPORT_OPT"/>: <img src='img/ico_file_excel.png' />&nbsp; <a href="#" onclick="fnExport();"><fmtPartAttributeReport:message key="DIV_EXCEL"/>  </a></div>                         
	       			</td>
       			</tr><%} else if (!gridData.equals("")) {
			%>
			<tr>
				<td colspan="9" height="1" class="LLine"></td>
			</tr>
			<tr>
			<tr>
				<td colspan="9" align="center" class="RightText"><fmtPartAttributeReport:message key="LBL_NOTHING_FOUND_TO_DISPLAY"/>
					</td>
			</tr>
			</tr>
			<%}%>
 

</table>
</html:form> 
</BODY>
<%@ include file="/common/GmFooter.inc" %>

</HTML>