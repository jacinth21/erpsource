<%
/**********************************************************************************
 * File		 		: GmModifyScar.jsp
 * Desc		 		: Setting up Modify Scar
 * Version	 		: 1.0
 * author			: Manikandan
************************************************************************************/
%>
<!-- quality\scar\GmScarListReport.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ taglib prefix="fmtScarListReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtScarListReport:setLocale value="<%=strLocale%>"/>
<fmtScarListReport:setBundle basename="properties.labels.quality.scar.GmScarListReport"/>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*"%>
<bean:define id="scarId" name="frmScarListReport" property="scarId" type="java.lang.String"></bean:define>
<bean:define id="alStatus" name="frmScarListReport" property="alStatus" type="java.util.ArrayList"></bean:define>
<bean:define id="alChooseAction" name="frmScarListReport" property="alChooseAction" type="java.util.ArrayList"></bean:define>
<bean:define id="arraySize" name="frmScarListReport" property="arraySize" type="java.lang.Integer"></bean:define> 
<bean:define id="gridXmlData" name="frmScarListReport" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="accessFl" name="frmScarListReport" property="accessFl" type="java.lang.String"> </bean:define>
<% 
String strQualScarJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_QUALITY_SCAR");
GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.quality.scar.GmScarListReport", strSessCompanyLocale);
	String strTodaysDate = (String)session.getAttribute("strSessTodaysDate")==null?"":(String)session.getAttribute("strSessTodaysDate");
	String strApplnDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	String strTitle = "";
	String strWikiTitle = "";
	
	ArrayList alRemoveCodeIDs = new ArrayList();
	ArrayList alPoType = new ArrayList();
	alRemoveCodeIDs.add("102776");
	      alPoType = GmCommonClass.parseNullArrayList(GmCommonClass.toRemoveCodeIdsFromArrayList(alRemoveCodeIDs,alChooseAction));
	      if(accessFl.equals("true")){ // to get whether modify CAPA or CAPA report
	    		strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_MODIFY_GENERATE_SCAR_REPORT"));
	    		strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("MODIFY_SCAR"));
	    	}else{
	    		strTitle =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SCAR_REPORT"));
	    		strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("SCAR_REPORT"));
	    	}

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Modify/Generate Scar Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="STYLESHEET" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strQualScarJsPath%>/GmScarListReport.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");</style>
     
<style>
div.myexportlinks {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	margin: 2px 0 10px 0;
}
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script type="text/javascript">
var objGridData;
objGridData = '<%=gridXmlData%>';
var arraySize ='';
var gridObj ='';
var todaysDate 	= '<%=strTodaysDate%>';
var format 		= '<%=strApplnDateFmt%>';
var accessFl = '<%=accessFl%>';
</script>  


</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnLoad();" onkeypress="fnEnter();";>
<html:form  action="/gmScarListReport.do?method=listScarDetails">
<html:hidden property="strOpt" name="frmScarListReport" />
<html:hidden property="hscarId" name="frmScarListReport" />
<html:hidden property="hstatus" name="frmScarListReport" />
<html:hidden property="hDisplayNm" name="frmScarListReport"/>
<html:hidden property="hRedirectURL" name="frmScarListReport"/>
<html:hidden property="accessFl" name="frmScarListReport"/>

<!-- Custom tag lib code modified for JBOSS migration changes -->

		<table border="0" width="100%" class="DtTable900" cellspacing="0" cellpadding="0">
		<tr><td class="Line" colspan="4"></td></tr>
			<tr>
				<td height="25" class="RightDashBoardHeader" colspan="3">&nbsp;<%=strTitle%></td>
				 <td  height="25" class="RightDashBoardHeader">
						<fmtScarListReport:message key="IMG_ALT_HELP" var = "varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='/images/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td> 
			</tr>
		  <tr><td class="LLine" colspan="4"></td></tr>
			<tr>
				<td  class="RightTableCaption" height="25" align ="right" width="200"  >&nbsp;<fmtScarListReport:message key="LBL_SCAR"/>:</td>
				<td>&nbsp;<html:text property="scarId"  maxlength="20" size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" /></td>
				 <td class="RightTableCaption" align="right" height="25" width="100" >&nbsp;<fmtScarListReport:message key="LBL_STATUS"/>:</td>
				<td align="left" colspan="2" width="200">&nbsp;<gmjsp:dropdown width="100" controlName="status" SFFormName="frmScarListReport"
						SFSeletedValue="status" defaultValue= "[Choose One]" SFValue="alStatus" codeId="CODEID" codeName="CODENM" />
				</td>  
			</tr>
		 <tr><td class="LLine" colspan="4"></td></tr> 
			<tr class="shade">
				<td class="RightTableCaption" align="right" height="25">&nbsp;<fmtScarListReport:message key="LBL_SUPPLIER_NAME"/>:</td>
				 <td align ="left" >&nbsp;<gmjsp:dropdown width="200" controlName="assignedName" SFFormName="frmScarListReport"
						SFSeletedValue="assignedName" defaultValue= "[Choose One]" SFValue="alAssignedTo" codeId="ID" codeName="NAME" />
					</td> 
				<td class="RightTableCaption" align="right" height="25" width="100">&nbsp;<fmtScarListReport:message key="LBL_ELEPSED_DAYS"/>:</td>
				  <td align ="left" colspan="2" width="200">&nbsp;<gmjsp:dropdown width="100" controlName="elpasedDys" SFFormName="frmScarListReport"
						SFSeletedValue="elpasedDys" defaultValue= "[Choose One]" SFValue="alElapsedDys" codeId="CODEID" codeName="CODENM" />
				 &nbsp; <html:text property="elpasedDysVal" maxlength="10" size="3" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" /> 
				 </td> 
				  	
			</tr>
			<tr><td class="LLine" colspan="4"></td></tr>
			<tr>
				<td class="RightTableCaption" align="right" height="25">&nbsp;<fmtScarListReport:message key="LBL_NCMR_COMPLAINT"/>:</td>
				<td>&nbsp;<html:text property="reference"  maxlength="20" size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" /></td>
				 <td class="RightTableCaption" align="right" height="25" width="100">&nbsp;<fmtScarListReport:message key="LBL_FOLLOW_UP"/>:</td>
				 <td align ="left" colspan="2" width="200">&nbsp;<gmjsp:dropdown width="100" controlName="followUp" SFFormName="frmScarListReport"
						SFSeletedValue="followUp" defaultValue= "[Choose One]" SFValue="alFollowUp" codeId="CODEID" codeName="CODENM" />
					</td>
			</tr> 
			 <tr><td class="LLine" colspan="4"></td></tr> 
			 <tr class="shade">
			 <td class="RightTableCaption" align="right" height="25"  >&nbsp;<fmtScarListReport:message key="LBL_ASSIGNED_DATE_RANGE"/>:</td>
			<td width="350">&nbsp;<fmtScarListReport:message key="LBL_FROM" var="varFrom"/><gmjsp:label type="RegularText"  SFLblControlName="${varFrom}:" td="false"/>
				<gmjsp:calendar SFFormName="frmScarListReport" controlName="assignedFromDt" gmClass="InputArea" 
						onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
				&nbsp;&nbsp;&nbsp;<fmtScarListReport:message key="LBL_TO" var="varTo"/><gmjsp:label type="RegularText"  SFLblControlName="${varTo}:" td="false"/>&nbsp;<gmjsp:calendar SFFormName="frmScarListReport" controlName="assignedToDt" gmClass="InputArea" 
						onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
			</td>
			<td colspan="3" align="center" height="25">&nbsp;
				       <fmtScarListReport:message key="BTN_LOAD" var="varLoad"/><gmjsp:button name="Btn_Load" value="${varLoad}&nbsp;&nbsp;" style="height: 23px; width: 5em;" gmClass="button" buttonType="Load" onClick="fnLoad();" />
            </td>
			 </tr>
			<%if(!gridXmlData.equals("")){%>
				<tr>
					<td colspan="4">
						<div  id="SCARDATA" style="" height="500px" width="899px"></div>
					</td>
				</tr>							
			<%}else{%>
				<tr>
					<td colspan="4" height="1" class="LLine"></td></tr>
				<tr>
					<td colspan="4" align="center" class="RightText">
					<fmtScarListReport:message key="LBL_NO_DATA"/>
					</td>
				</tr> 
			<%}%>
		    <% if(arraySize>=1){%>
		    	<%if(accessFl.equals("true")){%> <!-- Only for Modify, Chooose action is needed -->	
			<tr>
				<td  colspan="4" >
					<table  border="0" cellspacing="0" cellpadding="0" >
						<tr>
							<td height="25" colspan="2" width="25%"><div class="myexportlinks"><fmtScarListReport:message key="DIV_EXPORT_OPT"/>: <img	src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="fnExportExcel();"> <fmtScarListReport:message key="DIV_EXCEL"/> </a></div></td>
							<td width="10%"></td>
							<td class="RightTableCaption" width="20%"><gmjsp:dropdown controlName="chooseAction" SFFormName="frmScarListReport"
								SFSeletedValue="chooseAction" defaultValue= "[Choose One]" SFValue="alChooseAction" codeId="CODEID" codeName="CODENM" /></td>
						    <td align="center"><fmtScarListReport:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button name="Btn_Submit" buttonType="Save" value="${varSubmit}" controlId="Submit" gmClass="button" style="width: 6em; height: 23px " onClick="fnSubmit();" /></td>
						    <td width="25%"></td>		
                	    </tr>
                	    <%}else{ %>
                	    	<tr>
								<td align="center" height="25" colspan="5" width="25%">
									<div class="myexportlinks"><fmtScarListReport:message key="DIV_EXPORT_OPT"/>:<img src='img/ico_file_excel.png' />&nbsp;
										<a href="#" onclick="fnExportExcel();"> <fmtScarListReport:message key="DIV_EXCEL"/> 
										</a>
									</div>
								</td>                    
							</tr>
								<% }%>
            		</table>
				</td>
			</tr>
			<%}%>
			</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
				