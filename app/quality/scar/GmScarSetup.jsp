<%
/**********************************************************************************
 * File		 		: GmScarSetup.jsp
 * Desc		 		: Scar Initiation
 * Version	 		: 1.0
 * author			: Manikandan
************************************************************************************/
%>
<!-- quality\scar\GmScarSetup.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtScarSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtScarSetup:setLocale value="<%=strLocale%>"/>
<fmtScarSetup:setBundle basename="properties.labels.quality.scar.GmScarSetup"/>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*"%>
<%@page import="java.util.Date"%>
<bean:define id="scarId" name="frmScarInitiation" property="scarId" type="java.lang.String"></bean:define>
<bean:define id="status" name="frmScarInitiation" property="status" type="java.lang.String"></bean:define>
<bean:define id="message" name="frmScarInitiation" property="message" type="java.lang.String"></bean:define>
<bean:define id="alAssignedTo" name="frmScarInitiation" property="assignedTo" type="java.lang.String"></bean:define>
<bean:define id="intResultSize" name="frmScarInitiation" property="intSize" type="java.lang.Integer"></bean:define>
<bean:define id="alChooseAction" name="frmScarInitiation" property="alChooseAction" type="java.util.ArrayList"> </bean:define>
<bean:define id="alFollowUpFl" name="frmScarInitiation" property="alFollowUpFl" type="java.util.ArrayList"> </bean:define>
<bean:define id="strButtonDisable" name="frmScarInitiation" property="strButtonDisable" type="java.lang.String"></bean:define>
<% 
	
	String strQualScarJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_QUALITY_SCAR");
	response.setContentType("text/html; charset=UTF-8");
	response.setCharacterEncoding("UTF-8");
	String strWikiTitle = GmCommonClass.getWikiTitle("SCAR_INITIATION");
	
	ArrayList alRemoveCodeIDs = new ArrayList();
	alRemoveCodeIDs.add("102775");
	
	alChooseAction = GmCommonClass.parseNullArrayList(GmCommonClass.toRemoveCodeIdsFromArrayList(alRemoveCodeIDs,alChooseAction));
	String strCurrentdate = GmCommonClass.parseNull((String)session.getAttribute("strSessTodaysDate"));
	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	strButtonDisable = !strButtonDisable.equals("Y")?"true":"";
	


%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Scar Initiation </TITLE>
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel = "stylesheet" type = "text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strQualScarJsPath%>/GmScarSetup.js"></script> 

</HEAD>

<script>
	var confirmMsg = false;
	var status = '<%=status%>';
	var hSCARID = '<%=scarId%>';
	var format = '<%=strApplDateFmt%>';
	var currentdate = '<%=strCurrentdate%>';
</script>

<BODY leftmargin="20" topmargin="10"  onLoad="fnOnPageLoad();">
<html:form  action="/gmScarInitiation.do?">
<html:hidden property="strOpt" name="frmScarInitiation" />
<input type="hidden" name="hDisplayNm"/>
<input type="hidden" name="hRedirectURL"/>

<!-- Custom tag lib code modified for JBOSS migration changes -->
<!-- Struts tag lib code modified for JBOSS migration changes -->
			<table border="0" bordercolor="red" width="100%" class="DtTable900" cellspacing="0" cellpadding="0">
				<tr>
					<td height="25" class="RightDashBoardHeader" colspan="3">&nbsp;<fmtScarSetup:message key="LBL_SCAR_INITIATION_AND_CLOSURE"/></td>
					<td  height="25" class="RightDashBoardHeader">
						<fmtScarSetup:message key="IMG_ALT_HELP" var = "varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>
				</tr>
			 	 <tr>
					<td colspan="4">
						<table border="0" width="100%" cellspacing="0" cellpadding="0">
						<tr><td class="LLine" colspan="5"></td></tr>
						<% if(!message.equals("")){ %>			
							<tr>
								<td colspan="5" align="center" height="25">&nbsp;<font style="color: Blue;"><b><bean:write name="frmScarInitiation" property="message"/></b></font></td>
							</tr>
						<%}%>
							<tr>
						  		<td class="RightTableCaption" align="right" width="20.5%"><fmtScarSetup:message key="LBL_SCAR"/>:</td>
						  		<td width="200">&nbsp;<html:text maxlength="30" property="scarId" size="20"   
									styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="3" /></td>
						  		<td colspan="2" align="left" height="25" >
						  		<fmtScarSetup:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}&nbsp;&nbsp;" style="width: 4em;height: 23px" tabindex="14" name="Btn_Load" gmClass="button" buttonType="Load" onClick="fnLoad();" />
						  		</td>
						 		</tr>
						</table>
					</td>
				</tr>	
				<tr><td class="LLine" colspan="5"></td></tr>
				<tr bgcolor="#e4e6f2">
				<td height="25" class="ShadeRightTableCaption" colspan="5"><a href="javascript:fnShowFilters('tabRecv');" style="text-decoration:none; color:black">
						<IMG id="tabRecvimg" border=0 src="<%=strImagePath%>/minus.gif">&nbsp;<fmtScarSetup:message key="LBL_GENERAL_DETAILS"/></td></a>
				</tr>
				 <tr id="tabRecv" style="display:table-row;">
				 	<td colspan="5"> 
						<table border="0" width="100%" cellspacing="0" cellpadding="0"> 
							<tr><td class="LLine" colspan="5"></td></tr>
							<tr class="shade">
								<td class="RightTableCaption" align="right" width="25%"><fmtScarSetup:message key="LBL_NCMR_COMPLAINT"/>:</td>
								<td align ="left"  height="20%">&nbsp;<html:textarea   property="complaint"  cols="40" style="height:50px"
									styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
								</td>
					 			<td class="RightTableCaption" align="right" height="25" width="25%"><fmtScarSetup:message key="LBL_STATUS"/>:</td>
					 			<td width="30%">&nbsp;<bean:write name="frmScarInitiation" property="statusName"/></td> 
							</tr> 
							<tr><td class="LLine" colspan="5"></td></tr>
							<tr>
								<td class="RightTableCaption" align="right" height="25"><fmtScarSetup:message key="LBL_SUPPLIER_NAME"/>:</td>
					 			<td align ="left" ><gmjsp:dropdown  controlName="assignedTo" SFFormName="frmScarInitiation"
									SFSeletedValue="assignedTo" defaultValue= "[Choose One]" SFValue="alAssignedTo" codeId="ID" codeName="NAME" />
								</td>
								<td class="RightTableCaption" align="right" height="25" ><fmtScarSetup:message key="LBL_ISSUED_BY"/>:</td>
								<td >&nbsp;<gmjsp:dropdown  controlName="strIssuedBy" SFFormName="frmScarInitiation"
									SFSeletedValue="strIssuedBy" width="160" defaultValue= "[Choose One]" SFValue="alIssuedBy" codeId="USERNM" codeName="RUSERNM"/>
								</td> 
							</tr>
							<tr><td class="LLine" colspan="5"></td></tr>
					    	<tr class="shade">
								<td class="RightTableCaption" align="right"  ><fmtScarSetup:message key="LBL_SCAR_DESCRIPTION"/>:</td>
								<td colspan="3" align ="left"  height="20%">&nbsp;<html:textarea  property="scarDesc"  cols="100" style="height:70px"
									styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
								</td>
							</tr>
							<tr><td class="LLine" colspan="5"></td></tr>
					    	<tr>
								<td class="RightTableCaption" align="right"  ><fmtScarSetup:message key="LBL_PRIVIOUS_SCARS"/>:</td>
								<td colspan="3" align ="left"  height="20%">&nbsp;<html:textarea property="prevScar"  cols="100" style="height:70px"
									styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
								</td>
							</tr>
							<tr><td class="LLine" colspan="5"></td></tr> 
				 			<tr  class="shade">							
						    	<td class="RightTableCaption" align="right" height="25"><fmtScarSetup:message key="LBL_ASSIGNED_DATE"/>:</td>
								<td>&nbsp;<gmjsp:calendar SFFormName="frmScarInitiation" controlName="assignedDt" gmClass="InputArea" 
									onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"  />
								</td>	 
								<td class="RightTableCaption" align="right" height="25" onBlur="fndaystoclose()"><fmtScarSetup:message key="LBL_RESPONSE_DUE_DATE"/>:</td>
								<td colspan="3">&nbsp;<gmjsp:calendar SFFormName="frmScarInitiation" controlName="respDueDt" gmClass="InputArea" 
									onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
								</td>	
							</tr>
						</table>
					</td>
				</tr>					
				<tr><td class="LLine" colspan="5"></td></tr>
				<tr bgcolor="#e4e6f2">
				<td height="25" class="ShadeRightTableCaption" colspan="5"><a href="javascript:fnShowFilters('ClosetabRecv');" style="text-decoration:none; color:black">
						<IMG id="ClosetabRecvimg" border=0 src="<%=strImagePath%>/minus.gif">&nbsp;<fmtScarSetup:message key="LBL_CLOSURE_DETAILS"/></td></a>
				</tr>
				 <tr id="ClosetabRecv" style="display:table-row;">
				 	<td colspan="5"> 
						<table border="0" width="100%" cellspacing="0" cellpadding="0"> 
							<tr><td class="LLine" colspan="5"></td></tr>				
							<tr>
								<td width="20%" class="RightTableCaption" align="right"  ><fmtScarSetup:message key="LBL_CLOSED_DATE"/>:</td>
								<td width="35%">&nbsp;<gmjsp:calendar SFFormName="frmScarInitiation" controlName="closedDt" gmClass="InputArea" 
									onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"  />
								</td>
					 			<td class="RightTableCaption" align="right" height="25" width="23%"><fmtScarSetup:message key="LBL_DAY_TO-CLOSE"/>:</td>
					 			<td width="25%">&nbsp;<bean:write name="frmScarInitiation" property="daysToClose"/></td> 
							</tr>
							<tr><td class="LLine" colspan="5"></td></tr>
							<tr class="shade">
								<td width="20%" class="RightTableCaption" align="right"  ><fmtScarSetup:message key="LBL_FOLLOW_UP"/>:</td>
								<td valign="middle">&nbsp;<gmjsp:dropdown  controlName="followUpFlag" SFFormName="frmScarInitiation" onChange="fnSetEsimetedDt()"
									SFSeletedValue="followUpFlag" defaultValue= "[Choose One]" SFValue="alFollowUpFl" codeId="CODEID" codeName="CODENM"/>
					 			</td>
					 			<td class="RightTableCaption" align="right" height="25" width="23%"><fmtScarSetup:message key="LBL_ESTIMATED_FOLLOW_UP_DATE"/>:</td>
					 			<td>&nbsp;<gmjsp:calendar SFFormName="frmScarInitiation" controlName="estimetdFlUpDt" gmClass="InputArea" 
									onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
								</td>
					 		</tr>
								
							<tr><td class="LLine" colspan="5"></td></tr>
							<tr>
								<td class="RightTableCaption" align="right"  ><fmtScarSetup:message key="LBL_ANY_OTHER_RELEVANT_INFORMATION"/>:</td>
								<td  colspan="3" align ="left"  height="20%">&nbsp;<html:textarea property="relaventInfo"  cols="100" style="height:70px"
									styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
								</td>
								
							</tr> 
						</table>
					</td>
				</tr>
						<tr><td class="LLine" colspan="5"></td></tr>
				  		<tr>
					 		<td colspan="5"> 
								<jsp:include page="/common/GmIncludeLog.jsp" >
								<jsp:param name="FORMNAME" value="frmScarInitiation" />
								<jsp:param name="ALNAME" value="alLogReasons" />
								<jsp:param name="LogMode" value="Edit" />
								</jsp:include>
							</td>
						</tr> 
						<tr>
							<td colspan="5" align="center" height="30">&nbsp;
						<%if(!status.equals("102781") && !status.equals("102780")){ %>
						
				        	<fmtScarSetup:message key="BTN_RESET" var="varReset"/><gmjsp:button name="Btn_Reset" value="${varReset}" controlId="Reset" buttonType="Save" gmClass="button" style="width: 6em" onClick="fnReset();" />&nbsp;
				        	<fmtScarSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button name="Btn_Submit" value="${varSubmit}" buttonType="Save" controlId="Submit" gmClass="button" style="width: 6em" onClick="fnSubmit();" disabled="<%=strButtonDisable%>"/>&nbsp;

				 		<%} %>
				 		<% if(intResultSize >= 1){%>
				   			<fmtScarSetup:message key="BTN_PRINT" var="varPrint"/><gmjsp:button name="Btn_Print" value="${varPrint}" controlId="Print" gmClass="button" buttonType="Load" style="width: 6em" onClick="fnPrint();" />&nbsp;
						<%} %>
						<%if(status.equals("102781")){ %> <!-- if status is closed -->
							<fmtScarSetup:message key="BTN_EMAIL" var="varEmail"/><gmjsp:button name="Btn_Email" value="${varEmail}" controlId="Email" buttonType="Save" gmClass="button" style="width: 6em" onClick="fnEmail();" />
						<%} %>
						</td>                   
						</tr>  
				 		<% if(intResultSize >= 1){%>
							<tr>
								<td  colspan="5" >
									<table  border="0" cellspacing="0" cellpadding="0" >
										<tr class="shade">
											<td ></td>
											<td class="RightTableCaption" align="center"><gmjsp:dropdown controlName="chooseAction" SFFormName="frmScarInitiation"
												SFSeletedValue="chooseAction" defaultValue= "[Choose One]" SFValue="alChooseAction" codeId="CODEID" codeName="CODENM" />
										   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtScarSetup:message key="BTN_SAVE" var="varSave"/><gmjsp:button name="Btn_Submit" value="${varSave}" controlId="Submit" buttonType="Save" gmClass="button" style="height: 23px; width: 5em;" onClick="fnChooseAction();" /></td>
										   		
				                	    </tr>
				            		</table>
								</td>
							</tr>
					<% }%>
								 
				</table>				
			</html:form>
		<%@ include file="/common/GmFooter.inc"%>
	</BODY>
</HTML>