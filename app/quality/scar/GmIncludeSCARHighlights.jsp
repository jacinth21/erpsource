<%
/**********************************************************************************
 * File		 		: GmIncludeSCARHighlights.jsp
 * Desc		 		: Include in Upload for COMPLAINT SCREEN
 * Version	 		: 1.0
 * author			: Velu
************************************************************************************/
%>
<!-- quality\scar\GmIncludeSCARHighlights.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtIncludeSCARHighlights" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtIncludeSCARHighlights:setLocale value="<%=strLocale%>"/>
<fmtIncludeSCARHighlights:setBundle basename="properties.labels.quality.scar.GmIncludeSCARHighlights"/>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
 
<bean:define id="intSize" name="frmScarInitiation" property="intSize" type="java.lang.Integer"></bean:define>
<bean:define id="scarDesc" name="frmScarInitiation" property="scarDesc" type="java.lang.String"></bean:define>
<bean:define id="prevScar" name="frmScarInitiation" property="prevScar" type="java.lang.String"></bean:define>
<bean:define id="complaint" name="frmScarInitiation" property="complaint" type="java.lang.String"></bean:define>
<HTML>

<HEAD>
<TITLE> Globus Medical: SCAR Highlights </TITLE>
<!--<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/quality/complaint/GmCMPUpload.js"></script>
-->
</HEAD>
<BODY leftmargin="20" topmargin="10">
		<table border="0" bordercolor="red" width="100%" cellspacing="0" cellpadding="0">
		<%if(intSize > 0){ %>
			<tr class="Odd">
				<td class="RightTableCaption" align="right" width="25%"  ><fmtIncludeSCARHighlights:message key="LBL_SCAR"/>:</td>
				 <td height="25" width="30%">&nbsp;<bean:write name="frmScarInitiation" property="scarId"/> </td>
				 <td class="RightTableCaption" align="right" width="20%" ><fmtIncludeSCARHighlights:message key="LBL_STATUS"/>:</td>
				 <td width="35%">&nbsp;<bean:write name="frmScarInitiation" property="statusName"/> </td>  
			</tr>	
			<tr><td class="LLine" colspan="5"></td></tr> 
			<tr class="Shade">
				<td class="RightTableCaption" align="right"  ><fmtIncludeSCARHighlights:message key="LBL_NCMR_COMPLAINT"/>:</td>
				 <td height="25">&nbsp;
				 <div id="showDiv" style="margin-top:0px;margin-left:7px;width: 275px; height: 50px;<%if(complaint.length()>100){ %>overflow-y:scroll<%}%>">
				 <bean:write name="frmScarInitiation" property="complaint"/></div></td>
				 <td class="RightTableCaption" align="right"  ><fmtIncludeSCARHighlights:message key="LBL_SUPPLIER_NAME"/>:</td>
				 <td height="25">&nbsp;<bean:write name="frmScarInitiation" property="assignedNames"/> </td> 
			</tr>	
			<tr><td class="LLine" colspan="5"></td></tr> 
			<tr class="Odd">
				<td class="RightTableCaption" align="right"  ><fmtIncludeSCARHighlights:message key="LBL_SCAR_DESCRIPTION"/>:</td>
				 <td  colspan="3">&nbsp;
				 <div id="showDiv" style="vertical-align: middle;margin-left:7px;width: 800px; height: 70px;<%if(scarDesc.length()>1000){ %>overflow-y:scroll<%}%>"> 
				 <bean:write name="frmScarInitiation" property="scarDesc"/></div> </td>
			</tr>				
			<tr><td class="LLine" colspan="5"></td></tr>
			<tr class="Shade">
				<td class="RightTableCaption" align="right"  ><fmtIncludeSCARHighlights:message key="LBL_PREVIOUS_SCAR"/>:</td>
				 <td  colspan="3">&nbsp;
				 <div id="showDiv" style="vertical-align: middle;margin-left:7px;width: 800px; height: 70px;<%if(prevScar.length()>1000){ %>overflow-y:scroll<%}%>"> 
				 <bean:write name="frmScarInitiation" property="prevScar"/></div> </td>
			</tr> 
			<tr><td class="Line" colspan="5"></td></tr>		
		<%}else{%>
				 <tr><td height="25" colspan="5"><font color="blue" size="1"><fmtIncludeSCARHighlights:message key="LBL_NO_DATA"/>!</font></td></tr>
				<tr><td class="LLine" colspan="5"></td></tr>
		<%}%>
		</table>
</BODY>
</HTML>