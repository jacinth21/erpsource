<%@ page language="java" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtSetBundleHistory" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- \accounts\physicalaudit\GmTagHistoryReport.jsp -->
<fmtSetBundleHistory:setLocale value="<%=strLocale%>"/>
<fmtSetBundleHistory:setBundle basename="properties.labels.quality.GmSetBundleMapping"/>

<%
GmServlet gm = new GmServlet();
gm.checkSession(response,session);
String strApplDateFmt = strGCompDateFmt;
strApplDateFmt = strApplDateFmt + " K:m:s ";
String strRptFmt = "{0,date,"+strApplDateFmt+"}";
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Tag History</TITLE>
<style type="text/css" media="all">
     @import url("styles/screen.css");
</style>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script>

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" >
 
<html:form action="/gmSetBundleMapping.do"  >

	<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="12" height="25" class="RightDashBoardHeader">
				<fmtSetBundleHistory:message key="LBL_SET_BUNDLE_HISTORY_DETAILS"/>
				
			</td>
		</tr>
		<tr><td colspan="12" class="Line" height="1"></td></tr>		
	    <tr>
			<td colspan="12" width="100%">
				<display:table name="requestScope.frmSetBundleMapping.rdHistory.rows" export="false" class="its" requestURI="/gmSetBundleMapping.do" id="currentRowObject" >
 				<fmtSetBundleHistory:message key="LBL_KEYWORDS" var="varKey"/><display:column property="KEYWORD" title="${varKey}" class="alignleft"/>
 				<fmtSetBundleHistory:message key="LBL_UPDATED_BY" var="varUpBy"/><display:column property="UPDATEDBY" title="${varUpBy}" class="alignleft" />
 				<fmtSetBundleHistory:message key="LBL_UPDATED_DATE" var="varUpDate"/><display:column property="UPDATEDDATE" title="${varUpDate}" class="alignleft" />
				
				</display:table>
			</td>
		</tr> 
	</table>
	<table border="0" cellspacing="0" cellpadding="0">
	<tr><td colspan="12"></td></tr>
	<tr>
	<td align="center" height="30" colspan="12"><fmtSetBundleHistory:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="&nbsp;${varClose}&nbsp;" gmClass="button" onClick="window.close();" buttonType="Load"/></td>
	</tr>
	</table>
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

