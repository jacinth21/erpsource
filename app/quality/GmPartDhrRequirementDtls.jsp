<%@page import="com.globus.common.beans.GmResourceBundleBean"%>

<%
	/**********************************************************************************
	 * File		 		: GmPartDhrRequirementDtls.jsp
	 * Desc		 		: Part DHR Requirement details to be update
	 * Version	 		: 1.0
	 * author			: 
	 ************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ taglib prefix="fmtPartDhrReq" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmPartDhrRequirementDtls.jsp -->
<fmtPartDhrReq:setLocale value="<%=strLocale%>"/>
<fmtPartDhrReq:setBundle basename="properties.labels.quality.GmPartDhrRequirementDtls"/>
<bean:define id="partStatus" name="frmPartParameterDtls" property="partStatus" type="java.lang.String"> </bean:define>
<bean:define id="diNumber" name="frmPartParameterDtls" property="diNumber" type="java.lang.String"> </bean:define>
<bean:define id="vendorDesign" name="frmPartParameterDtls" property="vendorDesign" type="java.lang.String"> </bean:define>
<bean:define id="buttonAccess" name="frmPartParameterDtls" property="buttonAccess" type="java.lang.String"> </bean:define>
<bean:define id="hmAttrVal" name="frmPartParameterDtls" property="hmAttrVal" type="java.util.HashMap"> </bean:define>
<bean:define id="editDisable" name="frmPartParameterDtls" property="editDisable" type="java.lang.String"> </bean:define>
<%
String strProdMgmntJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
	String strChecked = "";
	String strButtonDisable = buttonAccess.equals("Y")?"":"true";
	if(editDisable.equalsIgnoreCase("true")){
	  strButtonDisable = "true";
	}
	Boolean blCheckDisable = false;
	String strVendorDisable = "";
	String strCheckDisable = "";
	String strIssuingAgencyHistoryFl = "";
	String strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("PART_DHR_REQUIREMENT"));
	
	if(partStatus.equals("20369")){ //Obsolete
		blCheckDisable = true;
		strVendorDisable = "disabled";
	}
	if(!diNumber.equals("")){
		strChecked = "checked";
		blCheckDisable = true;
		strVendorDisable = "disabled";
		strCheckDisable = "disabled";
	}
	if(vendorDesign.equals("103420")){ // 103420 - Yes
		strCheckDisable = "disabled"; 
	}
	String strPartNum = GmCommonClass.parseNull((String) hmAttrVal.get("PARTNUMBER"));
	strIssuingAgencyHistoryFl = GmCommonClass.parseNull((String)hmAttrVal.get("ISSUING_AGENCY_FL"));
	strIssuingAgencyHistoryFl = strIssuingAgencyHistoryFl.equals("Y") ? "<img style='cursor:hand' alt='View Issuing Agency details' src=   '"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1150'); border='0'>": "";
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Part Number - DHR Requirement</TITLE>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmPartNumberSetup.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script>
var lblMaterialCert='<fmtPartDhrReq:message key="LBL_MATERIAL_CERT"/>';
var lblPrimaryPackaging = '<fmtPartDhrReq:message key="LBL_PRIMARY_PACKAGING"/>';
var lblSecondaryPackaging='<fmtPartDhrReq:message key="LBL_SECONDARY_PACKAGING"/>';
var lblComplianceCert ='<fmtPartDhrReq:message key="LBL_COMPLIANCE_CERT"/>';
var lblUdiEtchReq = '<fmtPartDhrReq:message key="LBL_UDIETCH"/>';
var lblDiOnlyEtchReq = '<fmtPartDhrReq:message key="LBL_DIETCH"/>';
var lblPiOnlyEtchReq='<fmtPartDhrReq:message key="LBL_PIETCH"/>';
var lblBulkPackaging = '<fmtPartDhrReq:message key="LBL_BULK_PACKAGING"/>';
var lblIssuingAgencyConfig = '<fmtPartDhrReq:message key="LBL_ISSUE_AGENCY"/>';

</script>
</HEAD>
<BODY onload="fnOnLoadDHRTab();">
	<html:form action="/gmPartParameterDtls.do?method=dhrRequirementDtls">
		<html:hidden property="strOpt" value="" />
		<html:hidden property="partNumber" />
		<html:hidden property="diNumber"/>
		<html:hidden property="inputStr" value=""/>
		<html:hidden property="sterilePart" />
		<html:hidden property="partStatus" />
		<input type="hidden" name = "hVendorDesign" value="<%=vendorDesign %>"/>

		<table border="0" width="948px" cellspacing="0" cellpadding="0">

			<tr>
				<td colspan="7" height="25" class="RightDashBoardHeader"><fmtPartDhrReq:message key="LBL_DHR_HEADER"/></td>
				<fmtPartDhrReq:message key="LBL_HELP" var="varHelp"/>
				<td align="right" class=RightDashBoardHeader><img id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16'
					height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />&nbsp;
				</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>

			<tr>
			<fmtPartDhrReq:message key="LBL_MATERIAL_CERT" var="varMaterialCert"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="MandatoryText" SFLblControlName="${varMaterialCert}:" td="false" />
				</td>
				<td colspan="2">&nbsp;<gmjsp:dropdown
						controlName="materialCert" SFFormName="frmPartParameterDtls"
						SFSeletedValue="materialCert" SFValue="alMaterialCert"
						codeId="CODENMALT" codeName="CODENM" defaultValue="[Choose One]" />
				</td>
				<fmtPartDhrReq:message key="LBL_HARDNESS_CERT" var="varHardnessCert"/>
				<td colspan="2" class="RightTableCaption" align="right"><gmjsp:label
						type="RegularText" SFLblControlName="${varHardnessCert}:" td="false" />
				</td>
				<td colspan="2">&nbsp;<gmjsp:dropdown
						controlName="handnessCert" SFFormName="frmPartParameterDtls"
						SFSeletedValue="handnessCert" SFValue="alHandnessCert"
						codeId="CODENMALT" codeName="CODENM" defaultValue="[Choose One]" />
				</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr class="Shade">
			<fmtPartDhrReq:message key="LBL_PRIMARY_PACKAGING" var="varPrimaryPackaging"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="MandatoryText" SFLblControlName="${varPrimaryPackaging}:"
						td="false" />
				</td>
				<td colspan="2">&nbsp;<gmjsp:dropdown controlName="primaryPack"
						SFFormName="frmPartParameterDtls" SFSeletedValue="primaryPack"
						SFValue="alPrimaryPack" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]"  />
				</td>
				<fmtPartDhrReq:message key="LBL_SECONDARY_PACKAGING" var="varSecondaryPackaging"/>
				<td colspan="2" class="RightTableCaption" align="right"><gmjsp:label
						type="MandatoryText" SFLblControlName="${varSecondaryPackaging}:"
						td="false" />
				</td>
				<td colspan="2">&nbsp;<gmjsp:dropdown
						controlName="secondaryPack" SFFormName="frmPartParameterDtls"
						SFSeletedValue="secondaryPack" SFValue="alSecondaryPack"
						codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"
						 /></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr>
			<fmtPartDhrReq:message key="LBL_COMPLIANCE_CERT" var="varComplianceCert"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="MandatoryText" SFLblControlName="${varComplianceCert}:"
						td="false" />
				</td>
				<td colspan="2">&nbsp;<gmjsp:dropdown
						controlName="complianceCert" SFFormName="frmPartParameterDtls"
						SFSeletedValue="complianceCert" SFValue="alComplianceCert"
						codeId="CODENMALT" codeName="CODENM" defaultValue="[Choose One]"
						 /></td>
						 <fmtPartDhrReq:message key="LBL_DI_NUMBER" var="varDiNumber"/>
				<td colspan="2" class="RightTableCaption" align="right"><gmjsp:label
						type="RegularText" SFLblControlName="${varDiNumber}:" td="false" />
				</td>
				<td colspan="2">&nbsp;<input type="checkbox" name="diNumberFl" <%=strCheckDisable%> onclick="fnDoCheck(this.form, this);" value="" <%=strChecked %>/><logic:notEqual name="frmPartParameterDtls" property="diNumber" value="">&nbsp;(<bean:write name="frmPartParameterDtls" property="diNumber" />)</logic:notEqual>
				</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr class="Shade">
			 <fmtPartDhrReq:message key="LBL_ISSUE_AGENCY" var="varIssueAgency"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><div id="issueAgencyMandatory" style="display: none;"><font color="red">*</font>&nbsp;</div><gmjsp:label
						type="RegularText" SFLblControlName="${varIssueAgency}:" td="false" />
				</td>
				<td colspan="2">&nbsp;<gmjsp:dropdown
						controlName="issuingAgencyConf" SFFormName="frmPartParameterDtls"
						SFSeletedValue="issuingAgencyConf" SFValue="alIssuingAgencyConf"
						codeId="ID" codeName="NAME" defaultValue="[Choose One]" onChange="fnChangeIssueAgency(this.form);"
						 />&nbsp;<%=strIssuingAgencyHistoryFl %></td>
						 <fmtPartDhrReq:message key="LBL_SAMPLE_PATTERN" var="varSamplePattern"/>
				<td colspan="2" class="RightTableCaption" align="right"><gmjsp:label
						type="RegularText" SFLblControlName="${varSamplePattern}:" td="false" /></td>
				<td colspan="2" width="350">&nbsp;<logic:notEqual
						name="frmPartParameterDtls" property="diNumber" value="">
						<bean:write name="frmPartParameterDtls" property="samplePattern" />
					</logic:notEqual></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr>
			<fmtPartDhrReq:message key="LBL_WO_DCO" var="varWoDco"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varWoDco}:" td="false" />
				</td>
				<td colspan="2">&nbsp;<gmjsp:dropdown controlName="woCreated"
						SFFormName="frmPartParameterDtls" SFSeletedValue="woCreated"
						SFValue="alWoCreated" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]"  />
				</td>
				<fmtPartDhrReq:message key="LBL_UDIETCH" var="varUdiEtch"/>
				<td colspan="2" class="RightTableCaption" align="right"><div id="udiEtchMandatory" style="display: none;"><font color="red">*</font>&nbsp;</div><gmjsp:label
						type="RegularText" SFLblControlName="${varUdiEtch}:"
						td="false" />
				</td>
				<td colspan="2">&nbsp;<gmjsp:dropdown controlName="udiEtchReq"
						SFFormName="frmPartParameterDtls" SFSeletedValue="udiEtchReq"
						SFValue="alUDIEtchRequired" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]"  />
				</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr class="Shade">
			<fmtPartDhrReq:message key="LBL_DIETCH" var="varDiEtch"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><div id="diOnlyMandatory" style="display: none;"><font color="red">*</font>&nbsp;</div><gmjsp:label
						type="RegularText" SFLblControlName="${varDiEtch}:"
						td="false" />
				</td>
				<td colspan="2">&nbsp;<gmjsp:dropdown
						controlName="diOnlyEtchReq" SFFormName="frmPartParameterDtls"
						SFSeletedValue="diOnlyEtchReq" SFValue="alDIOnlyEtchRequired"
						codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"
						 /></td>
						 <fmtPartDhrReq:message key="LBL_PIETCH" var="varPIEtch"/>
				<td colspan="2" class="RightTableCaption" align="right"><div id="piOnlyMandatory" style="display: none;"><font color="red">*</font>&nbsp;</div><gmjsp:label
						type="RegularText" SFLblControlName="${varPIEtch}: 	"
						td="false" />
				</td>
				<td colspan="2">&nbsp;<gmjsp:dropdown
						controlName="piOnlyEtchReq" SFFormName="frmPartParameterDtls"
						SFSeletedValue="piOnlyEtchReq" SFValue="alPIOnlyEtchRequired"
						codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"
						 /></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr>
			<fmtPartDhrReq:message key="LBL_VENDOR_DESIGN" var="varVendorDesign"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varVendorDesign}:" td="false" />
				</td>
				<td colspan="2">&nbsp;<gmjsp:dropdown
						controlName="vendorDesign" SFFormName="frmPartParameterDtls"
						SFSeletedValue="vendorDesign" SFValue="alVendorDesign"
						codeId="CODEID" codeName="CODENM" disabled="<%=strVendorDisable%>" onChange="fnChangeVendorDesign(this.form);"/>
				</td>
				<fmtPartDhrReq:message key="LBL_BULK_PACKAGING" var="varBulkPackaging"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><div style="display:none;" id="bulkPackMandatory"><font color="red">*</font>&nbsp;</div><gmjsp:label
						type="RegularText" SFLblControlName="${varBulkPackaging}:" td="false" />
				</td>
				<td colspan="2">&nbsp;<gmjsp:dropdown
						controlName="bulkPackage" SFFormName="frmPartParameterDtls"
						SFSeletedValue="bulkPackage" SFValue="alBulkPackage"
						codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"
						 /></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr>
		<td colspan="8"> 
			<jsp:include page="/common/GmIncludeLog.jsp" >
				<jsp:param name="FORMNAME" value="frmPartParameterDtls" />
				<jsp:param name="ALNAME" value="alLogReasons" />
				<jsp:param name="Mandatory" value="yes" />
				<jsp:param name="LogMode" value="Edit" />
			</jsp:include>
		</td>
	</tr>
			<tr height="30">
			 <fmtPartDhrReq:message key="BTN_SUBMIT" var="varSubmit"/>
				<td colspan="8" align="center"><gmjsp:button value="${varSubmit}"
						gmClass="button" name="btn_submit" style="width: 6em"
						buttonType="Save" onClick="fnSaveDHRInfo(this.form);" disabled="<%=strButtonDisable %>"/></td>
			</tr>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>