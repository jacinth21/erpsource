
<%@page import="java.util.Date"%>
<%
	/**********************************************************************************
	 * File		 		: GmPartRegulatorySetup.jsp
	 * Desc		 		: Part DHR Requirement details to be update
	 * Version	 		: 1.0
	 * author			: 
	 ************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ taglib prefix="fmtPartRegulatorySetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- quality\GmPartRegulatorySetup.jsp -->
<fmtPartRegulatorySetup:setLocale value="<%=strLocale%>"/>
<fmtPartRegulatorySetup:setBundle basename="properties.labels.quality.GmPartRegulatorySetup"/>
<bean:define id="hmAttrVal" name="frmPartParameterDtls" property="hmAttrVal" type="java.util.HashMap"></bean:define>
<bean:define id="hmDropdown" name="frmPartParameterDtls" property="hmResult" type="java.util.HashMap"/>
<bean:define id="applnDateFmt" name="frmPartParameterDtls" property="applnDateFmt" type="java.lang.String"></bean:define>
<bean:define id="buttonAccess" name="frmPartParameterDtls" property="buttonAccess" type="java.lang.String"> </bean:define>
<bean:define id="editDisable" name="frmPartParameterDtls" property="editDisable" type="java.lang.String"> </bean:define>
<%

	String strProdMgmntJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
	String strQualJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_QUALITY");
	String strBrandNm = "";
	String strPartDes = "";
	String strUsRegClassification = "";
	String strUsRegPathway = "";
	String strFDASubNo1 = "";
	String strFDASubNo2 = "";
	String strFDASubNo3 = "";
	String strFDASubNo4 = "";
	String strFDASubNo5 = "";
	String strFDASubNo6 = "";
	String strFDASubNo7 = "";
	String strFDASubNo8 = "";
	String strFDASubNo9 = "";
	String strFDASubNo10 = "";
	String strApprovalDt = "";
	Date dtApprovalDt = null;
	String strNtfReported = "";
	String strFDAListing = "";
	String strPMASupNo1 = "";
	String strPMASupNo2 = "";
	String strPMASupNo3 = "";
	String strPMASupNo4 = "";
	String strPMASupNo5 = "";
	String strPMASupNo6 = "";
	String strPMASupNo7 = "";
	String strPMASupNo8 = "";
	String strPMASupNo9 = "";
	String strPMASupNo10 = "";
	String strFDAProCode1 = "";
	String strFDAProCode2 = "";
	String strFDAProCode3 = "";
	String strFDAProCode4 = "";
	String strFDAProCode5 = "";
	String strFDAProCode6 = "";
	String strFDAProCode7 = "";
	String strFDAProCode8 = "";
	String strFDAProCode9 = "";
	String strFDAProCode10 = "";
	String strEURegPathway = "";
	String strGMDNCode = "";
	String strCETechFileNum = "";
	String strCETechFileRev = "";
	String strNotes = "";
	String strPartNum ="";
	String strFDASubmission1Flag = "";
	String strFDASubmission2Flag = "";
	String strFDASubmission3Flag = "";
	String strFDASubmission4Flag = "";
	String strFDASubmission5Flag = "";
	String strFDASubmission6Flag = "";
	String strFDASubmission7Flag = "";
	String strFDASubmission8Flag = "";
	String strFDASubmission9Flag = "";
	String strFDASubmission10Flag = "";
	String strPMASup1Flag = "";
	String strPMASup2Flag = "";
	String strPMASup3Flag = "";
	String strPMASup4Flag = "";
	String strPMASup5Flag = "";
	String strPMASup6Flag = "";
	String strPMASup7Flag = "";
	String strPMASup8Flag = "";
	String strPMASup9Flag = "";
	String strPMASup10Flag = "";
	String strButtonDisable = "";
	String strExemptChecked = "";
	String strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("PART_REGULATORY"));
	
	
	HashMap hmDropdownSet = new HashMap();
	ArrayList alExempt = new ArrayList();
	ArrayList alClassII = new ArrayList();
	ArrayList alClassIII = new ArrayList();
	
	ArrayList alUsRegClass = new ArrayList();
	ArrayList alUsRegPathway = new ArrayList();
	ArrayList alEuRegPathway = new ArrayList();
		
	alUsRegClass = GmCommonClass.parseNullArrayList((ArrayList)hmAttrVal.get("alUsRegClass"));
	alEuRegPathway = GmCommonClass.parseNullArrayList((ArrayList)hmAttrVal.get("alEuRegPatyway"));
	
	alExempt = GmCommonClass.parseNullArrayList((ArrayList) hmDropdown.get("alExempt"));
	alClassII = GmCommonClass.parseNullArrayList((ArrayList) hmDropdown.get("alClassII"));
	alClassIII = GmCommonClass.parseNullArrayList((ArrayList) hmDropdown.get("alClassIII"));
	
	// to get the hashmap value
	strPartNum = GmCommonClass.parseNull((String)hmAttrVal.get("PNUM"));
	strPartDes = GmCommonClass.parseNull((String)hmAttrVal.get("PART_DESC"));
	strPartDes = GmCommonClass.getStringWithTM(strPartDes);
	strBrandNm = GmCommonClass.parseNull((String)hmAttrVal.get("BRAND_NAME"));
	strUsRegClassification = GmCommonClass.parseNull((String)hmAttrVal.get("US_REG_CLASSIFICATION"));
	//Checking the condi for reloading the screen
	if(strUsRegClassification.equals("104681") || strUsRegClassification.equals("104682") || strUsRegClassification.equals("104685")){
		alUsRegPathway = GmCommonClass.parseNullArrayList((ArrayList) hmDropdown.get("alExempt")); 
	}else if(strUsRegClassification.equals("104683")){
		alUsRegPathway = GmCommonClass.parseNullArrayList((ArrayList) hmDropdown.get("alClassII"));
	}else if(strUsRegClassification.equals("104684")){
		alUsRegPathway = GmCommonClass.parseNullArrayList((ArrayList) hmDropdown.get("alClassIII"));
	}
	strButtonDisable = buttonAccess.equals("Y")?"":"true";
	if(editDisable.equalsIgnoreCase("true")){
	  strButtonDisable = "true";
	}
	strUsRegPathway = GmCommonClass.parseNull((String)hmAttrVal.get("US_REG_PATHWAY"));
	strFDASubNo1 = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUB_NO1"));
	strFDASubNo2 = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUB_NO2"));
	strFDASubNo3 = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUB_NO3"));
	strFDASubNo4 = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUB_NO4"));
	strFDASubNo5 = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUB_NO5"));
	strFDASubNo6 = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUB_NO6"));
	strFDASubNo7 = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUB_NO7"));
	strFDASubNo8 = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUB_NO8"));
	strFDASubNo9 = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUB_NO9"));
	strFDASubNo10 = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUB_NO10"));
	strApprovalDt = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_APPROVAL_DATE"));
	dtApprovalDt = GmCommonClass.getStringToDate(strApprovalDt, applnDateFmt);
	strNtfReported = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_NTF_REPORTED"));
	strFDAListing = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_LISTING"));
	strPMASupNo1 = GmCommonClass.parseNull((String)hmAttrVal.get("PMA_SUP_NO1"));
	strPMASupNo2 = GmCommonClass.parseNull((String)hmAttrVal.get("PMA_SUP_NO2"));
	strPMASupNo3 = GmCommonClass.parseNull((String)hmAttrVal.get("PMA_SUP_NO3"));
	strPMASupNo4 = GmCommonClass.parseNull((String)hmAttrVal.get("PMA_SUP_NO4"));
	strPMASupNo5 = GmCommonClass.parseNull((String)hmAttrVal.get("PMA_SUP_NO5"));
	strPMASupNo6 = GmCommonClass.parseNull((String)hmAttrVal.get("PMA_SUP_NO6"));
	strPMASupNo7 = GmCommonClass.parseNull((String)hmAttrVal.get("PMA_SUP_NO7"));
	strPMASupNo8 = GmCommonClass.parseNull((String)hmAttrVal.get("PMA_SUP_NO8"));
	strPMASupNo9 = GmCommonClass.parseNull((String)hmAttrVal.get("PMA_SUP_NO9"));
	strPMASupNo10 = GmCommonClass.parseNull((String)hmAttrVal.get("PMA_SUP_NO10"));
	strFDAProCode1 = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_PRODUCT_CODE1"));
	strFDAProCode2 = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_PRODUCT_CODE2"));
	strFDAProCode3 = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_PRODUCT_CODE3"));
	strFDAProCode4 = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_PRODUCT_CODE4"));
	strFDAProCode5 = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_PRODUCT_CODE5"));
	strFDAProCode6 = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_PRODUCT_CODE6"));
	strFDAProCode7 = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_PRODUCT_CODE7"));
	strFDAProCode8 = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_PRODUCT_CODE8"));
	strFDAProCode9 = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_PRODUCT_CODE9"));
	strFDAProCode10 = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_PRODUCT_CODE10"));
	strEURegPathway = GmCommonClass.parseNull((String)hmAttrVal.get("EU_REG_PATHWAY"));
	strGMDNCode = GmCommonClass.parseNull((String)hmAttrVal.get("GMDN_CODE"));
	strCETechFileNum = GmCommonClass.parseNull((String)hmAttrVal.get("EU_CE_TECH_FILE_NUM"));
	strCETechFileRev = GmCommonClass.parseNull((String)hmAttrVal.get("EU_CE_TECH_FILE_REV"));
	strNotes = GmCommonClass.parseNull((String)hmAttrVal.get("NOTES"));
	strExemptChecked = GmCommonClass.parseNull((String)hmAttrVal.get("EXEMPT_SUBMISSION"));
	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	String struCurrentdate = GmCommonClass.parseNull((String)session.getAttribute("strSessTodaysDate"));
	
	//History Flag values
	String strUsRegClassificationFlag = GmCommonClass.parseNull((String)hmAttrVal.get("US_REG_CLASSIFICATION_FL"));
	String strUsRegPathwayFlag = GmCommonClass.parseNull((String)hmAttrVal.get("US_REG_PATHWAY_FL"));
	String strFDAProCode1Flag = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_PRODUCT_CODE1_FL"));
	String strFDAProCode2Flag = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_PRODUCT_CODE2_FL"));
	String strFDAProCode3Flag = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_PRODUCT_CODE3_FL"));
	String strFDAProCode4Flag = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_PRODUCT_CODE4_FL"));
	String strFDAProCode5Flag = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_PRODUCT_CODE5_FL"));
	String strFDAProCode6Flag = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_PRODUCT_CODE6_FL"));
	String strFDAProCode7Flag = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_PRODUCT_CODE7_FL"));
	String strFDAProCode8Flag = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_PRODUCT_CODE8_FL"));
	String strFDAProCode9Flag = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_PRODUCT_CODE9_FL"));
	String strFDAProCode10Flag = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_PRODUCT_CODE10_FL"));
	String strEURegPathwayFlag = GmCommonClass.parseNull((String)hmAttrVal.get("EU_REG_PATHWAY_FL"));
	String strCETechFileRevFlag  = GmCommonClass.parseNull((String)hmAttrVal.get("EU_CE_TECH_FILE_REV_FL"));
	//
	String strBrandNmFl = GmCommonClass.parseNull((String)hmAttrVal.get("BRAND_NAME_FL"));
	String strFDAListingNumFl = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_LISTING_FL"));
	String strGMDNCodeFl = GmCommonClass.parseNull((String)hmAttrVal.get("GMDN_CODE_FL"));
	String strSubNum1Fl =GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUB_NUM1_FL"));
	String strSubNum2Fl = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUB_NUM2_FL"));
	String strSubNum3Fl = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUB_NUM3_FL"));
	String strSubNum4Fl = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUB_NUM4_FL"));
	String strSubNum5Fl = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUB_NUM5_FL"));
	String strSubNum6Fl = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUB_NUM6_FL"));
	String strSubNum7Fl = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUB_NUM7_FL"));
	String strSubNum8Fl = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUB_NUM8_FL"));
	String strSubNum9Fl = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUB_NUM9_FL"));
	String strSubNum10Fl = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUB_NUM10_FL"));
	String strSupNum1Fl = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUP_NUM1_FL"));
	String strSupNum2Fl = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUP_NUM2_FL"));
	String strSupNum3Fl =GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUP_NUM3_FL"));
	String strSupNum4Fl = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUP_NUM4_FL"));
	String strSupNum5Fl = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUP_NUM5_FL"));
	String strSupNum6Fl = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUP_NUM6_FL"));
	String strSupNum7Fl = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUP_NUM7_FL"));
	String strSupNum8Fl = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUP_NUM8_FL"));
	String strSupNum9Fl = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUP_NUM9_FL"));
	String strSupNum10Fl = GmCommonClass.parseNull((String)hmAttrVal.get("FDA_SUP_NUM10_FL"));
	String strExemptSubFl = GmCommonClass.parseNull((String)hmAttrVal.get("EXEMPT_SUB_FL"));
	
	//FDASubmission flag value taken for validating FDA sequence
	if(!strFDASubNo1.equals("")){
		strFDASubmission1Flag = "Y";
	}if(!strFDASubNo2.equals("")){
		strFDASubmission2Flag = "Y";
	}if(!strFDASubNo3.equals("") ){
		strFDASubmission3Flag = "Y";
	}if(!strFDASubNo4.equals("") ){
		strFDASubmission4Flag = "Y";
	}if(!strFDASubNo5.equals("") ){
		strFDASubmission5Flag = "Y";
	}if(!strFDASubNo6.equals("")){
		strFDASubmission6Flag = "Y";
	}if(!strFDASubNo7.equals("")){
		strFDASubmission7Flag = "Y";
	}if(!strFDASubNo8.equals("") ){
		strFDASubmission8Flag = "Y";
	}if(!strFDASubNo9.equals("")){
		 strFDASubmission9Flag = "Y";
	}if(!strFDASubNo10.equals("")){
		 strFDASubmission10Flag = "Y";
	}
	
	//PMA Supplement field validating sequence
	if(!strPMASupNo1.equals("")){
		strPMASup1Flag = "Y";
	}if(!strPMASupNo2.equals("")){
		strPMASup2Flag = "Y";
	}if(!strPMASupNo3.equals("")){
		strPMASup3Flag = "Y";
	}if(!strPMASupNo4.equals("")){
		strPMASup4Flag = "Y";
	}if(!strPMASupNo5.equals("")){
		strPMASup5Flag = "Y";
	}if(!strPMASupNo6.equals("")){
		strPMASup6Flag = "Y";
	}if(!strPMASupNo7.equals("")){
		strPMASup7Flag = "Y";
	}if(!strPMASupNo8.equals("")){
		strPMASup8Flag = "Y";
	}if(!strPMASupNo9.equals("")){
		strPMASup9Flag = "Y";
	}if(!strPMASupNo10.equals("")){
		strPMASup10Flag = "Y";
	}
	
	//Based on the History flag value we are showing History Icon
	strUsRegClassificationFlag = strUsRegClassificationFlag.equals("Y") ? "<img style='cursor:hand' alt='View US Regulatory Classification' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1120'); border='0'>": "";
	strUsRegPathwayFlag = strUsRegPathwayFlag.equals("Y") ? "<img style='cursor:hand' alt='View US Regulatory Pathway' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1121'); border='0'>": "";
	strFDAProCode1Flag = strFDAProCode1Flag.equals("Y") ? "<img style='cursor:hand' alt='View FDA Product Code1 detail' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1122'); border='0'>": "";
	strFDAProCode2Flag = strFDAProCode2Flag.equals("Y") ? "<img style='cursor:hand' alt='View FDA Product Code2 detail' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1123'); border='0'>": "";
	strFDAProCode3Flag = strFDAProCode3Flag.equals("Y") ? "<img style='cursor:hand' alt='View FDA Product Code3 detail' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1124'); border='0'>": "";
	strFDAProCode4Flag = strFDAProCode4Flag.equals("Y") ? "<img style='cursor:hand' alt='View FDA Product Code4 detail' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1125'); border='0'>": "";
	strFDAProCode5Flag = strFDAProCode5Flag.equals("Y") ? "<img style='cursor:hand' alt='View FDA Product Code5 detail' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1126'); border='0'>": "";
	strFDAProCode6Flag = strFDAProCode6Flag.equals("Y") ? "<img style='cursor:hand' alt='View FDA Product Code6 detail' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1127'); border='0'>": "";
	strFDAProCode7Flag = strFDAProCode7Flag.equals("Y") ? "<img style='cursor:hand' alt='View FDA Product Code7 detail' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1128'); border='0'>": "";
	strFDAProCode8Flag = strFDAProCode8Flag.equals("Y") ? "<img style='cursor:hand' alt='View FDA Product Code8 detail' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1129'); border='0'>": "";
	strFDAProCode9Flag = strFDAProCode9Flag.equals("Y") ? "<img style='cursor:hand' alt='View FDA Product Code9 detail' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1130'); border='0'>": "";
	strFDAProCode10Flag = strFDAProCode10Flag.equals("Y") ? "<img style='cursor:hand' alt='View FDA Product Code10 detail' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1131'); border='0'>": "";
	strEURegPathwayFlag = strEURegPathwayFlag.equals("Y") ? "<img style='cursor:hand' alt='View EU Regulatory Pathway' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1132'); border='0'>": "";
	strCETechFileRevFlag = strCETechFileRevFlag .equals("Y") ? "<img style='cursor:hand' alt='View CE Tech File Rev' src='"+strImagePath+"/icon_History.gifs' onClick=fnShowHistory('"+ strPartNum + "','1133'); border='0'>": "";
	//
	strBrandNmFl = strBrandNmFl .equals("Y") ? "<img style='cursor:hand' alt='View Brand Name details' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1151'); border='0'>": "";
	strFDAListingNumFl = strFDAListingNumFl.equals("Y") ? "<img style='cursor:hand' alt='View FDA Listing details' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1172'); border='0'>": "";
	strGMDNCodeFl = strGMDNCodeFl.equals("Y") ? "<img style='cursor:hand' alt='View GMDN code details' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1173'); border='0'>": "";
	strSubNum1Fl = strSubNum1Fl.equals("Y") ? "<img style='cursor:hand' alt='View FDA Submission Number1' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1152'); border='0'>": "";
	strSubNum2Fl = strSubNum2Fl.equals("Y") ? "<img style='cursor:hand' alt='View FDA Submission Number2' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1153'); border='0'>": "";
	strSubNum3Fl = strSubNum3Fl.equals("Y") ? "<img style='cursor:hand' alt='View FDA Submission Number3' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1154'); border='0'>": "";
	strSubNum4Fl = strSubNum4Fl.equals("Y") ? "<img style='cursor:hand' alt='View FDA Submission Number4' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1155'); border='0'>": "";
	strSubNum5Fl = strSubNum5Fl.equals("Y") ? "<img style='cursor:hand' alt='View FDA Submission Number5' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1156'); border='0'>": "";
	strSubNum6Fl = strSubNum6Fl.equals("Y") ? "<img style='cursor:hand' alt='View FDA Submission Number6' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1157'); border='0'>": "";
	strSubNum7Fl = strSubNum7Fl.equals("Y") ? "<img style='cursor:hand' alt='View FDA Submission Number7' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1158'); border='0'>": "";
	strSubNum8Fl = strSubNum8Fl.equals("Y") ? "<img style='cursor:hand' alt='View FDA Submission Number8' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1159'); border='0'>": "";
	strSubNum9Fl = strSubNum9Fl.equals("Y") ? "<img style='cursor:hand' alt='View FDA Submission Number9' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1160'); border='0'>": "";
	strSubNum10Fl = strSubNum10Fl.equals("Y") ? "<img style='cursor:hand' alt='View FDA Submission Number10' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1161'); border='0'>": "";
	strSupNum1Fl = strSupNum1Fl.equals("Y") ? "<img style='cursor:hand' alt='View FDA Supplement Number1' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1162'); border='0'>": "";
	strSupNum2Fl = strSupNum2Fl.equals("Y") ? "<img style='cursor:hand' alt='View FDA Supplement Number2' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1163'); border='0'>": "";
	strSupNum3Fl = strSupNum3Fl.equals("Y") ? "<img style='cursor:hand' alt='View FDA Supplement Number3' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1164'); border='0'>": "";
	strSupNum4Fl = strSupNum4Fl.equals("Y") ? "<img style='cursor:hand' alt='View FDA Supplement Number4' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1165'); border='0'>": "";
	strSupNum5Fl = strSupNum5Fl.equals("Y") ? "<img style='cursor:hand' alt='View FDA Supplement Number5' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1166'); border='0'>": "";
	strSupNum6Fl = strSupNum6Fl.equals("Y") ? "<img style='cursor:hand' alt='View FDA Supplement Number6' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1167'); border='0'>": "";
	strSupNum7Fl = strSupNum7Fl.equals("Y") ? "<img style='cursor:hand' alt='View FDA Supplement Number7' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1168'); border='0'>": "";
	strSupNum8Fl = strSupNum8Fl.equals("Y") ? "<img style='cursor:hand' alt='View FDA Supplement Number8' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1169'); border='0'>": "";
	strSupNum9Fl = strSupNum9Fl.equals("Y") ? "<img style='cursor:hand' alt='View FDA Supplement Number9' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1170'); border='0'>": "";
	strSupNum10Fl = strSupNum10Fl.equals("Y") ? "<img style='cursor:hand' alt='View FDA Supplement Number10' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1171'); border='0'>": "";
	strExemptSubFl= strExemptSubFl.equals("Y") ? "<img style='cursor:hand' alt='View Exempt From Premarket Submission' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1174'); border='0'>": "";
	
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Part Number -Regulatory Setup</TITLE>
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmPartNumberSetup.js"></script>
<script language="JavaScript" src="<%=strQualJsPath%>/GmPartRegulatorySetup.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<script>
var format = '<%=strApplDateFmt%>';
var currdate = '<%=struCurrentdate%>';
var arrayExempt = new Array();
var arrayClassII = new Array();
var arrayClassIII = new Array();
var lblBrandName = '<fmtPartRegulatorySetup:message key="LBL_BRAND_NAME" />';
var lblUsRegulatoryClass = '<fmtPartRegulatorySetup:message key="LBL_US_REGULATORY_CLASSIFICATION" />';
var lblUSRegulatoryPathWay='<fmtPartRegulatorySetup:message key="LBL_US_REGULATORY_PATHWAY" />';
var lblEuRegPathWay = '<fmtPartRegulatorySetup:message key="LBL_EU_REGULATORY_PATHWAY" />';
var lblNtfReported = '<fmtPartRegulatorySetup:message key="LBL_NTF_REPORTED" />';

//dropdown values for Exempt
<%for (int i = 0; i < alExempt.size(); i++) {
				hmDropdownSet = (HashMap) alExempt.get(i);%>
arrayExempt.push('<%=hmDropdownSet.get("CODEID")%>'+","+'<%=hmDropdownSet.get("CODENM")%>');
<%}%>

//dropdown values for ClassII
<%for (int i = 0; i < alClassII.size(); i++) {
				hmDropdownSet = (HashMap) alClassII.get(i);%>
arrayClassII.push('<%=hmDropdownSet.get("CODEID")%>'+","+'<%=hmDropdownSet.get("CODENM")%>');
<%}%>

//dropdown values for ClassIII
<%for (int i = 0; i < alClassIII.size(); i++) {
				hmDropdownSet = (HashMap) alClassIII.get(i);%>
arrayClassIII.push('<%=hmDropdownSet.get("CODEID")%>'+","+'<%=hmDropdownSet.get("CODENM")%>');
<%}%>
</script>
</HEAD>
<BODY onload="fnRegulatoryLoad();">
	<html:form action="/gmPartParameterDtls.do?method=udiRegulatorySetup">
		<html:hidden property="strOpt" value="" />
		<html:hidden property="partNumber" />
		<html:hidden property="inputStr" value="" />
		<html:hidden property="hBrandName" value="<%=strBrandNm%>"/>
		<html:hidden property="hPMASup1" value="<%=strPMASup1Flag%>"/>
		<html:hidden property="hPMASup2" value="<%=strPMASup2Flag%>"/>
		<html:hidden property="hPMASup3" value="<%=strPMASup3Flag%>"/>
		<html:hidden property="hPMASup4" value="<%=strPMASup4Flag%>"/>
		<html:hidden property="hPMASup5" value="<%=strPMASup5Flag%>"/>
		<html:hidden property="hPMASup6" value="<%=strPMASup6Flag%>"/>
		<html:hidden property="hPMASup7" value="<%=strPMASup7Flag%>"/>
		<html:hidden property="hPMASup8" value="<%=strPMASup8Flag%>"/>
		<html:hidden property="hPMASup9" value="<%=strPMASup9Flag%>"/>
		<html:hidden property="hPMASup10" value="<%=strPMASup10Flag%>"/>
		<html:hidden property="hFDASubmission1" value="<%=strFDASubmission1Flag%>"/>
		<html:hidden property="hFDASubmission2" value="<%=strFDASubmission2Flag%>"/>
		<html:hidden property="hFDASubmission3" value="<%=strFDASubmission3Flag%>"/>
		<html:hidden property="hFDASubmission4" value="<%=strFDASubmission4Flag%>"/>
		<html:hidden property="hFDASubmission5" value="<%=strFDASubmission5Flag%>"/>
		<html:hidden property="hFDASubmission6" value="<%=strFDASubmission6Flag%>"/>
		<html:hidden property="hFDASubmission7" value="<%=strFDASubmission7Flag%>"/>
		<html:hidden property="hFDASubmission8" value="<%=strFDASubmission8Flag%>"/>
		<html:hidden property="hFDASubmission9" value="<%=strFDASubmission9Flag%>"/>
		<html:hidden property="hFDASubmission10" value="<%=strFDASubmission10Flag%>"/>

		<table border="0" width="948px" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="7" height="25" class="RightDashBoardHeader"><fmtPartRegulatorySetup:message key="LBL_REGULATORY_SETUP"/></td>
					<fmtPartRegulatorySetup:message key="LBL_HELP" var="varHelp"/>
				<td align="right" class=RightDashBoardHeader><img id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16'
					height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />&nbsp;
				</td>	
			</tr>

			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr>
			<fmtPartRegulatorySetup:message key="LBL_BRAND_NAME" var="varBransName"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="MandatoryText" SFLblControlName="${varBransName}:" td="false" />
				</td>
				<td colspan="2">&nbsp;<input type="text" name="txt_Brand_Nm"
					value="<%=strBrandNm%>" onfocus="changeBgColor(this,'#AACCE8');" tabIndex="1"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);">&nbsp;<%=strBrandNmFl %></td>
				<td align="right" class="RightTableCaption" colspan="2"><fmtPartRegulatorySetup:message key="LBL_PART_DESCRIPTION"/>:</td>
				<td class="RightText" colspan="2">&nbsp;<%=strPartDes %></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr>
				<td height="23" class="ShadeRightTableCaption" colspan="8">&nbsp;<fmtPartRegulatorySetup:message key="LBL_US"/></td>
			</tr>
			<tr>
				<fmtPartRegulatorySetup:message key="LBL_US_REGULATORY_CLASSIFICATION" var="varUsRegulatoryClass"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24" width="200"><gmjsp:label
						type="MandatoryText"
						SFLblControlName="${varUsRegulatoryClass}:" td="false" /></td>
				<td colspan="2">&nbsp;<gmjsp:dropdown
						controlName="cbo_Us_Reg_Class"
						seletedValue="<%=strUsRegClassification%>"
						value="<%=alUsRegClass%>" codeId="CODEID" codeName="CODENM" tabIndex="2"
						defaultValue="[Choose One]" onChange="javascript:fnGetUSPathWay(this.form,this,'cbo_Us_Reg_Pathway');"/>&nbsp;<%=strUsRegClassificationFlag%>
				</td>
				<fmtPartRegulatorySetup:message key="LBL_US_REGULATORY_PATHWAY" var="varUSRegulatoryPathway"/>
				<td colspan="2" class="RightTableCaption" align="right" width="300"><gmjsp:label
						type="MandatoryText" SFLblControlName="${varUSRegulatoryPathway}:"
						td="false" /></td>
				<td colspan="2">&nbsp;<gmjsp:dropdown
						controlName="cbo_Us_Reg_Pathway"
						seletedValue="<%=strUsRegPathway%>" value="<%=alUsRegPathway%>" tabIndex="3"
						codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" onChange="javascript:fnSetNTFReport(this.form,this);"/>&nbsp;<%=strUsRegPathwayFlag%>
				</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr>
				<td height="23" class="ShadeRightTableCaption" colspan="8">&nbsp;<fmtPartRegulatorySetup:message key="LBL_FDA"/></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr>
				<fmtPartRegulatorySetup:message key="LBL_NO" var="varNo"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varNo} 1:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text" name="txt_Fda_sub_no1"
					value="<%=strFDASubNo1%>" onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="7" tabIndex="4"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onChange="javascript:fnSetAprrovalDate(this.form,this);">&nbsp;<%=strSubNum1Fl %>
				</td>
				<fmtPartRegulatorySetup:message key="LBL_NO" var="varNo"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varNo} 2:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text" name="txt_Fda_sub_no2"
					value="<%=strFDASubNo2%>" onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="7" tabIndex="5"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onChange="javascript:fnSetAprrovalDate(this.form,this);">&nbsp;<%=strSubNum2Fl %>
				</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr class="Shade">
				<fmtPartRegulatorySetup:message key="LBL_NO" var="varNo"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varNo} 3:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text" name="txt_Fda_sub_no3"
					value="<%=strFDASubNo3%>" onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="7" tabIndex="6"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onChange="javascript:fnSetAprrovalDate(this.form,this);">&nbsp;<%=strSubNum3Fl %>
				</td>
				<fmtPartRegulatorySetup:message key="LBL_NO" var="varNo"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varNo} 4:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text" name="txt_Fda_sub_no4"
					value="<%=strFDASubNo4%>" onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="7" tabIndex="7"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onChange="javascript:fnSetAprrovalDate(this.form,this);">&nbsp;<%=strSubNum4Fl %>
				</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr>
				<fmtPartRegulatorySetup:message key="LBL_NO" var="varNo"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varNo} 5:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text" name="txt_Fda_sub_no5"
					value="<%=strFDASubNo5%>" onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="7" tabIndex="8"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onChange="javascript:fnSetAprrovalDate(this.form,this);">&nbsp;<%=strSubNum5Fl %>
				</td>
				<fmtPartRegulatorySetup:message key="LBL_NO" var="varNo"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varNo} 6:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text" name="txt_Fda_sub_no6"
					value="<%=strFDASubNo6%>" onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="7" tabIndex="9"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onChange="javascript:fnSetAprrovalDate(this.form,this);">&nbsp;<%=strSubNum6Fl %>
				</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr class="Shade">
				<fmtPartRegulatorySetup:message key="LBL_NO" var="varNo"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varNo} 7:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text" name="txt_Fda_sub_no7"
					value="<%=strFDASubNo7%>" onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="7" tabIndex="10"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onChange="javascript:fnSetAprrovalDate(this.form,this);">&nbsp;<%=strSubNum7Fl %>
				</td>
				<fmtPartRegulatorySetup:message key="LBL_NO" var="varNo"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varNo} 8:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text" name="txt_Fda_sub_no8"
					value="<%=strFDASubNo8%>" onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="7" tabIndex="11"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onChange="javascript:fnSetAprrovalDate(this.form,this);">&nbsp;<%=strSubNum8Fl %>
				</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr>
			<fmtPartRegulatorySetup:message key="LBL_NO" var="varNo"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varNo} 9:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text" name="txt_Fda_sub_no9"
					value="<%=strFDASubNo9%>" onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="7" tabIndex="12"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onChange="javascript:fnSetAprrovalDate(this.form,this);">&nbsp;<%=strSubNum9Fl %>
				</td>
				<fmtPartRegulatorySetup:message key="LBL_NO" var="varNo"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varNo} 10:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text" name="txt_Fda_sub_no10" 
					value="<%=strFDASubNo10%>" onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="7" tabIndex="13"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onChange="javascript:fnSetAprrovalDate(this.form,this);">&nbsp;<%=strSubNum10Fl %>
				</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr class="Shade">
				<fmtPartRegulatorySetup:message key="LBL_APPROVAL_DATE" var="varApprovalDate"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varApprovalDate}:" td="false" />
				</td>
				<td colspan="2">&nbsp;<gmjsp:calendar
						textControlName="txt_Approval_Dt"
						textValue="<%=dtApprovalDt==null?null:new java.sql.Date(dtApprovalDt.getTime())%>"
						gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" tabIndex="14" />
				</td>
				<fmtPartRegulatorySetup:message key="LBL_NTF_REPORTED" var="varNTDReported"/>
				<td colspan="2" class="RightTableCaption" align="right"><gmjsp:label
						type="RegularText" SFLblControlName="${varNTDReported}:" td="false" />
				</td>
				<td colspan="2">&nbsp;<input type="text"
					name="txt_Ntf_Reported" value="<%=strNtfReported%>"
					onfocus="changeBgColor(this,'#AACCE8');" tabIndex="15"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" maxlength="40">
				</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr>
				<fmtPartRegulatorySetup:message key="LBL_FDA_LISTING" var="varFDAListing"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varFDAListing}:" td="false" />
				</td>
				<fmtPartRegulatorySetup:message key="LBL_EXEMPT_FROM_FDA_SUBMISSION" var="varExemptFromFDA"/>
				<td colspan="2">&nbsp;<input type="text" name="txt_Fda_Listing" MAXLENGTH ="7"
					value="<%=strFDAListing%>" onfocus="changeBgColor(this,'#AACCE8');" tabIndex="16"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" >&nbsp;<%=strFDAListingNumFl %></td>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText"
						SFLblControlName="${varExemptFromFDA}:" td="false" />
				</td>
				<td colspan="2">&nbsp;<%=strExemptChecked%>&nbsp;<%=strExemptSubFl %></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr>
				<td height="23" class="ShadeRightTableCaption" colspan="8">&nbsp;<fmtPartRegulatorySetup:message key="LBL_PMA_SUPPLEMENT_NUMBER"/></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr>
				<fmtPartRegulatorySetup:message key="LBL_NO" var="varNo"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varNo} 1:" td="false" /></td>
				
				<td colspan="2">&nbsp;<input type="text" name="txt_Pma_Sup_no1"
					value="<%=strPMASupNo1%>" onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="3" tabIndex="17"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onChange="javascript:fnSetAprrovalDate(this.form,this);">&nbsp;<%=strSupNum1Fl %></td>
				<fmtPartRegulatorySetup:message key="LBL_NO" var="varNo"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varNo} 2:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text" name="txt_Pma_Sup_no2"
					value="<%=strPMASupNo2%>" onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="3" tabIndex="18"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onChange="javascript:fnSetAprrovalDate(this.form,this);">&nbsp;<%=strSupNum2Fl %></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr class="Shade">
				<fmtPartRegulatorySetup:message key="LBL_NO" var="varNo"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varNo} 3:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text" name="txt_Pma_Sup_no3"
					value="<%=strPMASupNo3%>" onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="3" tabIndex="19"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onChange="javascript:fnSetAprrovalDate(this.form,this);">&nbsp;<%=strSupNum3Fl %></td>
				<fmtPartRegulatorySetup:message key="LBL_NO" var="varNo"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varNo} 4:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text" name="txt_Pma_Sup_no4" 
					value="<%=strPMASupNo4%>" onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="3" tabIndex="20"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onChange="javascript:fnSetAprrovalDate(this.form,this);">&nbsp;<%=strSupNum4Fl %></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr>
				<fmtPartRegulatorySetup:message key="LBL_NO" var="varNo"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varNo} 5:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text" name="txt_Pma_Sup_no5"
					value="<%=strPMASupNo5%>" onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="3" tabIndex="21"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onChange="javascript:fnSetAprrovalDate(this.form,this);">&nbsp;<%=strSupNum5Fl %></td>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varNo} 6:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text" name="txt_Pma_Sup_no6"
					value="<%=strPMASupNo6%>" onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="3" tabIndex="22"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onChange="javascript:fnSetAprrovalDate(this.form,this);">&nbsp;<%=strSupNum6Fl %></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr class="Shade">
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varNo} 7:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text" name="txt_Pma_Sup_no7"
					value="<%=strPMASupNo7%>" onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="3" tabIndex="23"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onChange="javascript:fnSetAprrovalDate(this.form,this);">&nbsp;<%=strSupNum7Fl %></td>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varNo} 8:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text" name="txt_Pma_Sup_no8"
					value="<%=strPMASupNo8%>" onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="3" tabIndex="24"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onChange="javascript:fnSetAprrovalDate(this.form,this);">&nbsp;<%=strSupNum8Fl %></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varNo} 9:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text" name="txt_Pma_Sup_no9"
					value="<%=strPMASupNo9%>" onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="3" tabIndex="25"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onChange="javascript:fnSetAprrovalDate(this.form,this);">&nbsp;<%=strSupNum9Fl %></td>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varNo} 10:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text" name="txt_Pma_Sup_no10"
					value="<%=strPMASupNo10%>" onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="3" tabIndex="26"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onChange="javascript:fnSetAprrovalDate(this.form,this);">&nbsp;<%=strSupNum10Fl %></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr>
				<td height="23" class="ShadeRightTableCaption" colspan="8">&nbsp;<fmtPartRegulatorySetup:message key="LBL_FDA_PRODUCT_CODE" var="varNo"/></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr>
			<fmtPartRegulatorySetup:message key="LBL_CODE" var="varCode"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varCode} 1:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text"
					name="txt_Fda_Prod_Code1" value="<%=strFDAProCode1%>"
					onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="3" tabIndex="27"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onkeypress="return fnFDAProCode(event)">&nbsp;<%=strFDAProCode1Flag%></td>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varCode} 2:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text"
					name="txt_Fda_Prod_Code2" value="<%=strFDAProCode2%>"
					onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="3" tabIndex="28"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onkeypress="return fnFDAProCode(event)">&nbsp;<%=strFDAProCode2Flag%></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr class="Shade">
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varCode} 3:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text"
					name="txt_Fda_Prod_Code3" value="<%=strFDAProCode3%>"
					onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="3" tabIndex="29"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onkeypress="return fnFDAProCode(event)">&nbsp;<%=strFDAProCode3Flag%></td>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varCode} 4:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text"
					name="txt_Fda_Prod_Code4" value="<%=strFDAProCode4%>"
					onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="3" tabIndex="30"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onkeypress="return fnFDAProCode(event)">&nbsp;<%=strFDAProCode4Flag%></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varCode} 5:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text"
					name="txt_Fda_Prod_Code5" value="<%=strFDAProCode5%>"
					onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="3" tabIndex="31"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onkeypress="return fnFDAProCode(event)">&nbsp;<%=strFDAProCode5Flag%></td>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varCode} 6:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text"
					name="txt_Fda_Prod_Code6" value="<%=strFDAProCode6%>"
					onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="3" tabIndex="32"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onkeypress="return fnFDAProCode(event)">&nbsp;<%=strFDAProCode6Flag%></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr class="Shade">
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varCode} 7:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text"
					name="txt_Fda_Prod_Code7" value="<%=strFDAProCode7%>"
					onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="3" tabIndex="33"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onkeypress="return fnFDAProCode(event)">&nbsp;<%=strFDAProCode7Flag%></td>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varCode} 8:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text"
					name="txt_Fda_Prod_Code8" value="<%=strFDAProCode8%>"
					onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="3" tabIndex="34"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onkeypress="return fnFDAProCode(event)">&nbsp;<%=strFDAProCode8Flag%></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varCode} 9:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text"
					name="txt_Fda_Prod_Code9" value="<%=strFDAProCode9%>"
					onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="3" tabIndex="35"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onkeypress="return fnFDAProCode(event)">&nbsp;<%=strFDAProCode9Flag%></td>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varCode} 10:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text"
					name="txt_Fda_Prod_Code10" value="<%=strFDAProCode10%>"
					onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="3" tabIndex="36"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onkeypress="return fnFDAProCode(event)">&nbsp;<%=strFDAProCode10Flag%></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr>
				<td height="23" class="ShadeRightTableCaption" colspan="8">&nbsp;<fmtPartRegulatorySetup:message key="LBL_EU" /></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr>
			<fmtPartRegulatorySetup:message key="LBL_EU_REGULATORY_PATHWAY" var="varEuRegulatoryPathway"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="MandatoryText" SFLblControlName="${varEuRegulatoryPathway}:"
						td="false" /></td>
				<td colspan="2">&nbsp;<gmjsp:dropdown
						controlName="cbo_Eu_Reg_Pathway"
						seletedValue="<%=strEURegPathway%>" value="<%=alEuRegPathway%>" tabIndex="37"
						codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" />&nbsp;<%=strEURegPathwayFlag%>
				</td>
				<fmtPartRegulatorySetup:message key="LBL_GMDN_CODE" var="varGmdnCode"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varGmdnCode}:" td="false" /></td>
				<td colspan="2">&nbsp;<input type="text"
					name="txt_Gmdn_Code" value="<%=strGMDNCode%>"
					onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="6" tabIndex="38"
					onblur="changeBgColor(this,'#ffffff');" onkeypress="return fnGMDNCode(event,txt_Gmdn_Code)">&nbsp;<%=strGMDNCodeFl %> 
				</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr class="Shade">
			<fmtPartRegulatorySetup:message key="LBL_CE_TECH_FILE_NUMBER" var="varCETechFileNumber"/>
				<td colspan="2" class="RightTableCaption" align="right"><gmjsp:label
						type="RegularText" SFLblControlName="${varCETechFileNumber}:"
						td="false" /></td>
				<td colspan="2">&nbsp;<input type="text"
					name="txt_Ce_Tech_File_Num" value="<%=strCETechFileNum%>" tabIndex="39"
					onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="8" onChange="javascript:fnSetCETechFileRev(this.form,this);"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" >
				</td>
				<fmtPartRegulatorySetup:message key="LBL_CE_TECH_FIVE_REV" var="varCeTechFiveRev"/>
				<td colspan="2" class="RightTableCaption" align="right" height="24"><gmjsp:label
						type="RegularText" SFLblControlName="${varCeTechFiveRev}:" td="false" />
				</td>
				<td colspan="2">&nbsp;<input type="text"
					name="txt_Ce_Tech_File_Rev" value="<%=strCETechFileRev%>"
					onfocus="changeBgColor(this,'#AACCE8');" MAXLENGTH="1" tabIndex="40"
					onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" onkeypress="return fnTechFileRev(event,txt_Ce_Tech_File_Rev)">
				</td>


			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr>
				<td height="23" class="ShadeRightTableCaption" colspan="8">&nbsp;<fmtPartRegulatorySetup:message key="LBL_NOTES"/></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr>
				<td colspan="8" align="left" height="24">&nbsp;<textarea
						rows="3" cols="115" name="txt_Notes"
						onfocus="changeBgColor(this,'#AACCE8');" tabIndex="41"
						onblur="changeBgColor(this,'#ffffff');"><%=strNotes %></textarea>
				</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr>
				<td colspan="8"><jsp:include page="/common/GmIncludeLog.jsp">
						<jsp:param name="FORMNAME" value="frmPartParameterDtls" />
						<jsp:param name="ALNAME" value="alLogReasons" />
						<jsp:param name="LogMode" value="Edit" />
						<jsp:param name="TabIndex" value="42" />
						<jsp:param name="Mandatory" value="yes" />
					</jsp:include></td>
			</tr>
			<tr height="30">
			<fmtPartRegulatorySetup:message key="BTN_SUBMIT" var="varSubmit"/>
				<td colspan="8" align="center"><gmjsp:button value="${varSubmit}" tabindex="43"
						gmClass="button" name="btn_Reg_Submit" style="width: 6em"
						buttonType="Save" disabled="<%=strButtonDisable %>" onClick="fnSaveRegulatoryInfo(this.form);" /></td>
			</tr>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>