<%
	/*******************************************************************************************************
	 * File		 		: GmSetMappingUpload.jsp
	 * Desc		 		: This JSP is used to upload multiple parts to set mapping
	 * Version	 		: 1.0
	 * author			:  
	 *********************************************************************************************************/
%>

<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@page import="com.globus.common.beans.GmCalenderOperations"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*"%>

<%@ taglib prefix="fmtSetMappingUpload"
	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>

<!-- Quality\GmSetMappingUpload.jsp-->
<fmtSetMappingUpload:setLocale value="<%=strLocale%>" />
<fmtSetMappingUpload:setBundle
	basename="properties.labels.quality.GmSetMappingUpload" />
<bean:define id="submitAccessFl" name="frmSetMappingUpload"
	property="submitAccessFl" type="java.lang.String"></bean:define>
<bean:define id="strSetId" name="frmSetMappingUpload" property="setId"
	type="java.lang.String">
</bean:define>
<bean:define id="strSetName" name="frmSetMappingUpload"
	property="searchsetId" type="java.lang.String">
</bean:define>

<%
	String strQualJsPath = GmFilePathConfigurationBean
			.getFilePathConfig("JS_QUALITY");
	String strWikiTitle = GmCommonClass.parseNull(GmCommonClass
			.getWikiTitle("SET_MAPPING_UPLOAD"));
	String strJSONString = "{\"STROPT\":\"" + "SETMAP" + "\"}";
%>

<html>
<head>
<title>Globus Medical: Set Mapping Bulk upload</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/fonts/font_roboto/roboto.css"/>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.css">
<script language="JavaScript"
    src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript"
    src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript"
    src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>

 <script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx_deprecated.js"></script> 

<%-- <script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_form.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>

<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_markers.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxMenu/dhtmlxmenu.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxMessage/dhtmlxmessage.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_fast.js"></script> --%>

<script language="JavaScript" src="<%=strQualJsPath%>/GmSetMappingUpload.js"></script>

<script>
var gridObj = "";
</script>
<style type="text/css">
.even{
	background-color:#ffffff !important;
}
.uneven{
background-color:#dcedfc !important;;
}
		
div.gridbox table.hdr td {
   color: #000000 !important;
   background-color: #d2e9fc !important;
}
div.gridbox_material.gridbox .xhdr {
    background:#dcedfc !important;
}
</style>
</head>
<body leftmargin="20" topmargin="10" onload="fnOnPageLoad ();" >
	<html:form action="/gmSetMappingUpload.do?">
		<html:hidden property="strOpt"/>
		<html:hidden property="haction"/>
		<html:hidden property="inputStr" value="" />
		
		<table class="DtTable1000" cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td colspan="3" class="RightDashBoardHeader" height="30">&nbsp;<fmtSetMappingUpload:message
						key="LBL_SET_MAP_BULK_UPD" /></td>
				<td class="RightDashBoardHeader" align="right"><fmtSetMappingUpload:message
						key="IMG_ALT_HELP" var="varHelp" /> <img align="middle"
					id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td align="right" class="RightTableCaption" colspan="1" width="15%"
					HEIGHT="40"><fmtSetMappingUpload:message key="LBL_SET_NAME" />:&nbsp;</td>
				<td align="left" colspan="1" style="width: 0px;"><html:text
						property="setIdVal" onfocus="changeBgColor(this,'#AACCE8');"
						styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"
						onchange="javascript:fnValidateSetId(this);" tabindex="1" size="20" />
				</td>
				<td colspan="2" aligh="left"><table>
						<tr>
							<td><jsp:include page="/common/GmAutoCompleteInclude.jsp">
									<jsp:param name="CONTROL_NAME" value="setId" />
									<jsp:param name="PARAMETER" value="<%=strJSONString%>" />
									<jsp:param name="METHOD_LOAD"
										value="loadSetNameList&searchType=PrefixSearch" />
									<jsp:param name="WIDTH" value="550" />
									<jsp:param name="CSS_CLASS" value="search" />
									<jsp:param name="TAB_INDEX" value="2" />
									<jsp:param name="CONTROL_NM_VALUE" value="<%=strSetName%>" />
									<jsp:param name="CONTROL_ID_VALUE" value="<%=strSetId%>" />
									<jsp:param name="SHOW_DATA" value="10" />
									<jsp:param name="AUTO_RELOAD" value="fnLoadSetUploadDtls();" />

								</jsp:include></Td>
						</tr>
					</table></td>

				<%-- <fmtSetMapSetup:message key="BTN_LOAD" var="varLoad"/>
			<td colspan="2" align="center"><gmjsp:button gmClass="Button" name="Btn_Go" value="&nbsp;${varLoad}&nbsp;" buttonType="Load" onClick ="fnGo();" /></td> --%>
			</tr>

			
						<tr>
							<td class="LLine" colspan="8" height="1"></td>
						</tr>

						<tr height="25" class="Shade" style="display: none;" id="trLastUpd">
							<td class="RightTableCaption" align="right" width="300"><fmtSetMappingUpload:message
									key="LBL_LAST_UPLOADED_BY" var="varLastUploadBy" />
								<gmjsp:label type="RegularText"
									SFLblControlName="${varLastUploadBy}" td="false" />:&nbsp;</td>&nbsp;
							<td id="lbl_last_updated_by" align="left"></td>

							<td class="RightTableCaption" align="right"><fmtSetMappingUpload:message
									key="LBL_LAST_UPLOADED_DATE" var="varLastUploadDate" />
								<gmjsp:label type="RegularText"
									SFLblControlName="${varLastUploadDate}" td="false" />:&nbsp;</td width= "100px">&nbsp;
							<td id="lbl_last_updated_date" align="left"></td>
						</tr>
						<tr>
							<td class="LLine" colspan="4" height="1"></td>
						</tr>

						<tr height="25" style="display: none;" id="trLastStatus">
							<td class="RightTableCaption" align="right"><fmtSetMappingUpload:message
									key="LBL_LAST_UPLOADED_STATUS" var="varLastUploadStatus" />
								<gmjsp:label type="RegularText"
									SFLblControlName="${varLastUploadStatus}" td="false" />:&nbsp;
							</td>&nbsp;
							<td id="lbl_last_updated_status" align="left"></td>

							<td colspan="2">&nbsp;</td>
						</tr>


						<tr class="Shade" height="25" style="display: none;" id= "trImages">
							<td colspan="4">
								<table>
									<tr height="25">
										<td width="28" colspan="1" tabIndex="-1"><fmtSetMappingUpload:message
												key="IMG_ALT_COPY" var="varCopy" /><a href="#"><img
												src="<%=strImagePath%>/dhtmlxGrid/copy.gif"
												onClick="javascript:docopy()" alt="${varCopy}"
												style="border: none;" height="14"></a></td>
										<td width="28" colspan="1" tabIndex="-1"><fmtSetMappingUpload:message
												key="IMG_ALT_PASTE" var="varpaste" /><a href="#"> <img
												src="<%=strImagePath%>/dhtmlxGrid/paste.gif"
												alt="${varpaste}" style="border: none;"
												onClick="javascript:pasteToGrid()" height="14"></a></td>
										<td width="28" colspan="1" tabIndex="-1"><fmtSetMappingUpload:message
												key="IMG_ALT_ADD_ROW" var="varAddRow" /><a href="#"> <img
												src="<%=strImagePath%>/dhtmlxGrid/add.gif"
												alt="${varAddRow}" style="border: none;"
												onClick="javascript:fnAddRows()" height="14"></a></td>
										<td width="28" colspan="1" tabIndex="-1"><fmtSetMappingUpload:message
												 key="IMG_ALT_REMOVE_ROW" var="varRemoveRow" /> <a href="#">
												<img src="<%=strImagePath%>/dhtmlxGrid/delete.gif"
												alt="${varRemoveRow}" style="border: none;"
												onClick="javascript:fnRemoveSetUploadRow()" height="14">
										</a></td>
										<td width="28" colspan="1" tabIndex="-1"><fmtSetMappingUpload:message
												key="IMG_ALT_ADD_ROWS_CLIPBOARD" var="varAddRowsClipboard" /><a
											href="#"> <img
												src="<%=strImagePath%>/dhtmlxGrid/row_add_after_1.gif"
												alt="${varAddRowsClipboard}" style="border: none;"
												onClick="javascript:fnAddRowFromClipboard()" height="14"></a></td>

										<td colspan="2" align="right">
											<div id="progress" style="display: none;">
												<img src="<%=strImagePath%>/success_y.gif" height="15">
											</div>
										</td>
										<td><div id="msg" align="center"
												style="display: none; color: green; font-weight: bold;"></div>
											<div id="erromsg" align="center"
												style="display: none; color: red; font-weight: bold;"></div>
										</td>
									</tr>

								</table>
							</td>
						</tr>
						<tr id="trDiv" style="display: none;">
							<td colspan="8">
								<div id="dataGridDiv" height="300px" ></div>
							</td>
						</tr>
						
						<tr id="trComments" style="display: none;">
                             <td colspan="4">
                                 <jsp:include page="/common/GmIncludeLog.jsp">
                                     <jsp:param name="FORMNAME" value="frmSetMappingUpload" />
                                     <jsp:param name="ALNAME" value="alLogReasons" />
                                     <jsp:param name="LogMode" value="txt_LogReason" />
                                     <jsp:param name="Mandatory" value="yes" />
                                     <jsp:param name="TabIndex" value="10" />
                                 </jsp:include>
                             </td>

                         </tr>

						<tr height="30" style="display: none;" id="trAction">
							<td colspan="2" align="Right" class="RightTableCaption"><fmtSetMappingUpload:message key="LBL_ACTION" var="varUploadAction"/>
							<gmjsp:label type="MandatoryText" SFLblControlName="${varUploadAction}:" td="false" />&nbsp;</td>
							<td colspan="1"><gmjsp:dropdown controlName="uploadAction"
									SFFormName="frmSetMappingUpload" SFSeletedValue="uploadAction"
									SFValue="alUploadAction" codeId="CODEID" codeName="CODENM"
									defaultValue="[Choose One]" tabIndex="10" /></td>


							<td colspan="1" align="left" width="70%"><fmtSetMappingUpload:message
									key="BTN_SUBMIT" var="varSubmit" />
								<gmjsp:button value="${varSubmit}" gmClass="button"
									name="btn_Submit" style="width: 8em" buttonType="Save"
									onClick="fnSubmit ();" disabled="<%=submitAccessFl%>"
									tabindex="11" />
								</td>	
						</tr>


		</table>
		<%@ include file="/common/GmFooter.inc"%>
	</html:form>
</body>
</html>