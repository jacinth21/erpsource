 <%@page import="com.globus.common.beans.GmCalenderOperations"%>
 <%
/*******************************************************************************************************
 * File		 		: GmSetListCopy.jsp
 * Desc		 		: This JSP is like a container jsp for the screen to show set list copy page
 * Version	 		: 1.0
 * author			: jgurunathan 
*********************************************************************************************************/
%>

 <%@ page language="java"%>
 <%@ page import ="com.globus.common.beans.GmCommonControls"%>
 <%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

 <%@ include file="/common/GmHeader.inc"%>

 <%@ taglib prefix="frmSetListCopy" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <%@ page isELIgnored="false" %>

 <!-- Quality\GmSetListCopy.jsp -->
 <frmSetListCopy:setLocale value="<%=strLocale%>" />
 <frmSetListCopy:setBundle basename="properties.labels.quality.GmSetListCopy" />
 <bean:define id="fromSetId" name="frmSetListCopy" property="fromSetId" type="java.lang.String"></bean:define>
 <bean:define id="fromSetName" name="frmSetListCopy" property="fromSetName" type="java.lang.String"></bean:define>
 <bean:define id="toSetId" name="frmSetListCopy" property="toSetId" type="java.lang.String"></bean:define>
 <bean:define id="toSetName" name="frmSetListCopy" property="toSetName" type="java.lang.String"></bean:define>

 <%    
String strQualJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_QUALITY");
response.setContentType("text/html; charset=UTF-8");
response.setCharacterEncoding("UTF-8");
String strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("SET_LIST_COPY"));
String strGridXmlData = (String)request.getAttribute("GRIDXMLDATA");
ArrayList alRptDetails = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALRPTDETAILS"));
String strSessApplDateFmt = strGCompDateFmt;
String strTodaysDate = GmCalenderOperations.getCurrentDate(strSessApplDateFmt);	

String strFromSetID ="";
String strToSetID ="";
String strComment = ""; 
String strSubmit="";
String strJSONString = "{\"STROPT\":\""+"SETMAP"+"\"}";
%>

 <html>

 <head>
     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <title>Globus Medical: Set List Copy</title>
     <link rel="stylesheet" type="text/css" href="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
     <style type="text/css">
         table.DtTable1030 {
             margin: 0 0 0 0;
             width: 1030px;
             border: 1px solid #676767;
         }
     </style>
     <script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
     <script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
     <script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
     <script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
     <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
     <script language="JavaScript" src="<%=strQualJsPath%>/GmSetListCopy.js"></script>
     <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
     <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
     <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>

     <script>
         var format = "<%=strSessApplDateFmt%>";
         var todaysDate = '<%=strTodaysDate%>';
     </script>

 </head>

 <body leftmargin="20" topmargin="10">
     <html:form action="/gmSetListCopy.do?method=loadSetListCopy">
         <html:hidden property="strOpt" />
         <table border="0" class="DtTable1030" cellspacing="0" cellpadding="0">
             <tr>
                 <td class="RightDashBoardHeader" HEIGHT="25">Set List Copy</td>
                 <td align="right" class="RightDashBoardHeader" width="10%">
                     <frmSetListCopy:message key="IMG_ALT_HELP" var="varHelp" />
                     <img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
                 </td>
             </tr>
             <tr>
                 <td colspan="8" height="1" bgcolor="#cccccc"></td>
             </tr>
             <tr>
                 <td colspan="8">
                     <table border="0" width="100%" cellspacing="0" cellpadding="0">
                     
                         <tr HEIGHT="30">
                             <td class="RightTableCaption" width="200" align="right" HEIGHT="24">
                                 <font color="red">*</font>
                                 <frmSetListCopy:message key="LBL_FROM_SET" />:
                             </td>
                             <td width="100">
                                 <html:text property="txtFromSetID" name="frmSetListCopy" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" onchange="javascript:fnValidateSet (this,'From');" tabindex="1" />
                             </td>
                             <td style="line-height: 0;">&nbsp;&nbsp;&nbsp;<jsp:include page="/common/GmAutoCompleteInclude.jsp">
                                     <jsp:param name="CONTROL_NAME" value="fromSetId" />
                                     <jsp:param name="PARAMETER" value="<%= strJSONString%>" />
                                     <jsp:param name="METHOD_LOAD" value="loadSetNameList&searchType=PrefixSearch" />
                                     <jsp:param name="WIDTH" value="350" />
                                     <jsp:param name="CSS_CLASS" value="search" />
                                     <jsp:param name="TAB_INDEX" value="2" />
                                     <jsp:param name="CONTROL_NM_VALUE" value="<%= fromSetName%>" />
                                     <jsp:param name="CONTROL_ID_VALUE" value="<%=fromSetId %>" />
                                     <jsp:param name="SHOW_DATA" value="10" />
                                     <jsp:param name="AUTO_RELOAD" value="fnUpdateFromSet ();" />

                                 </jsp:include>
                             </td>
                         </tr>
                         <tr>
                             <td colspan="8" height="1" bgcolor="#cccccc"></td>
                         </tr>
                         <tr class=ShadeBlueBk HEIGHT="30">
                             <td class="RightTableCaption" width="200" align="right" HEIGHT="24">
                                 <font color="red">*</font>
                                 <frmSetListCopy:message key="LBL_TO_SET" />:
                             </td>
                             <td>
                                 <html:text property="txtToSetID" name="frmSetListCopy" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" onchange="javascript:fnValidateSet (this, 'To');" tabindex="3" />
                             </td>
                             <td style="line-height: 0;">&nbsp;&nbsp;&nbsp;<jsp:include page="/common/GmAutoCompleteInclude.jsp">
                                     <jsp:param name="CONTROL_NAME" value="toSetId" />
                                     <jsp:param name="PARAMETER" value="<%= strJSONString%>" />
                                     <jsp:param name="METHOD_LOAD" value="loadSetNameList&searchType=PrefixSearch" />
                                     <jsp:param name="WIDTH" value="350" />
                                     <jsp:param name="CSS_CLASS" value="search" />
                                     <jsp:param name="TAB_INDEX" value="4" />
                                     <jsp:param name="CONTROL_NM_VALUE" value="<%= toSetName%>" />
                                     <jsp:param name="CONTROL_ID_VALUE" value="<%=toSetId %>" />
                                     <jsp:param name="SHOW_DATA" value="10" />
                                     <jsp:param name="AUTO_RELOAD" value="fnUpdateToSet ();" />

                                 </jsp:include>
                             </td>
                         </tr>

                         <tr>
                             <td colspan="3">
                                 <jsp:include page="/common/GmIncludeLog.jsp">
                                     <jsp:param name="FORMNAME" value="frmSetListCopy" />
                                     <jsp:param name="ALNAME" value="alLogReasons" />
                                     <jsp:param name="LogMode" value="txt_LogReason" />
                                     <jsp:param name="Mandatory" value="yes" />
                                     <jsp:param name="TabIndex" value="5" />
                                 </jsp:include>
                             </td>

                         </tr>

<tr height="5"></tr>
						
                         <tr>
                             <frmSetListCopy:message key="BTN_SUBMIT" var="varSubmit" />
                             <td colspan="3" align="center" height="24">&nbsp;
                                 <gmjsp:button value="${varSubmit}" name="Btn_Submit" gmClass="Button" buttonType="Save" disabled="<%=strSubmit%>" onClick="javascript:fnSubmit(this.form);" />&nbsp;
                             </td>
                         </tr>
<tr height="5"></tr>
                         <tr>
                             <td colspan="8" height="1" bgcolor="#cccccc"></td>
                         </tr>
                     </table>
                 </td>
             </tr>

         </table>
         <%@ include file="/common/GmFooter.inc"%></%@>

     </html:form>
 </body>

 </html>