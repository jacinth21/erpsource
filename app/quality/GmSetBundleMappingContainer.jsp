<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtSetBundleSetupContainer" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- quality\GmSetBundleMappingContainer.jsp -->
<fmtSetBundleSetupContainer:setLocale value="<%=strLocale%>"/>
<fmtSetBundleSetupContainer:setBundle basename="properties.labels.quality.GmSetBundleMapping"/>
<% 
	String strWikiTitle = GmCommonClass.getWikiTitle("SYSTEM_SETUP");
%>
<bean:define id="strBundleId" name="frmSetBundleMapping" property="strBundleId" scope="request" type="java.lang.String"></bean:define>
<bean:define id="tabToLoad" name="frmSetBundleMapping" property="tabToLoad" type="java.lang.String"></bean:define>
<%
String strQualJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_QUALITY");
String strSetupTabClass ="selected";
String strMappingTabClass ="";
//while submitting the Mapping Tab we need to display the Mapping screen after reloading.
if(tabToLoad.equals("mapping")){
	 strSetupTabClass ="";
	 strMappingTabClass ="selected";
}

%>
<html>
<head>
<title>System Setup</title>

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strQualJsPath%>/GmSetBundle.js"></script> 
<script language="JavaScript" src="<%=strQualJsPath%>/GmSetBundleMapping.js"></script> 
 <!-- <script language="JavaScript" src="javascript/prodmgmnt/GmSystemMapping.js"></script> -->
<script language="javascript" type="text/javascript" src="<%=strExtWebPath%>/tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
	function fnHtmlEditor(){
		tinyMCE.init({
			mode : "specific_textareas",
		    editor_selector : "HtmlTextArea",
			theme : "advanced" ,
			theme_advanced_path : false,
			height : "300",
			width: "500" 
		});
}
</script> 
</head>
<body leftmargin="20" topmargin="10" onload="fnAjaxTabLoad();" > 
<html:form action="/gmSetBundleMapping.do">
<html:hidden property="hcurrentTab" value="Project_Setup"/>

<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtSetBundleSetupContainer:message key="LBL_HEADING"/></td>
			<td align="right" class=RightDashBoardHeader > 	
				<fmtSetBundleSetupContainer:message key="IMG_ALT_HELP" var="varHelp"/>
				<img id='imgEdit' style='cursor:hand' src='/images/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
		</tr>		
		<tr>
			<td class="RightTableCaption" align="right" height="30" width="35%"><font color="red">*</font>&nbsp;<fmtSetBundleSetupContainer:message key="LBL_BUNDLE_LIST"/>:</td>					
			<td width="65%">&nbsp;<gmjsp:dropdown controlName="strBundleId" onChange="fnfetchSystemDet(this.form);" SFFormName="frmSetBundleMapping" SFSeletedValue="strBundleId"
				SFValue="alResult" codeId="ID"  codeName="NM"  defaultValue="[Choose One]" width="315"/>
			</td>
		</tr>
		<tr>
			<td colspan="2" height="1" class="LLine" ></td>
		</tr>
</html:form>		
		<tr>
			<td colspan="2">
				<ul id="maintab" class="shadetabs">
				
				<li onclick="fnUpdateTabIndex(this);"><a href="/gmSetBundleMapping.do?strBundleId=<bean:write name="frmSetBundleMapping" property="strBundleId"/>&strOpt=fetch" title="Details" rel="ajaxdivcontentarea" id="system"  
				class="<%=strSetupTabClass%>"><fmtSetBundleSetupContainer:message key="LBL_DETAILS"/></a></li>
				<logic:notEqual name="frmSetBundleMapping" property="strBundleId" value="">
		 				<logic:notEqual name="frmSetBundleMapping" property="strBundleId" value="0">
							<li ><a href="/gmSetBundle.do?strOpt=fetch&systemId=<bean:write name="frmSetBundleMapping" property="strBundleId"/>"
								rel="ajaxcontentarea" id="mapping" title="mapping" class="<%=strMappingTabClass%>"><fmtSetBundleSetupContainer:message key="LBL_MAPPING"/></a>
							</li>											
							<li><a href="/gmLanguageDetail.do?typeID=26240459&refID=<bean:write name="frmSetBundleMapping" property="strBundleId"/>&strOpt=report"
									rel="#iframe" id="language" title="Language"><fmtSetBundleSetupContainer:message key="LBL_LANGUAGE"/></a>
							</li>
							<li><a href="/gmUploadDetail.do?refID=<bean:write name="frmSetBundleMapping" property="strBundleId"/>&strOpt=report&typeID=26240459"
									rel="#iframe" id="upload" title="Upload"><fmtSetBundleSetupContainer:message key="LBL_UPLOADS"/></a>
							</li>
								<li ><a href="/gmSetBundleMapping.do?strOpt=setBundleCompanyMap&strBundleId=<bean:write name="frmSetBundleMapping" property="strBundleId"/>"
								rel="#iframe" id="syscompmapping" title="System Company Mapping"><fmtSetBundleSetupContainer:message key="LBL_BUNDLE_COMPANY_MAPPING"/></a>
							</li>
						</logic:notEqual>
					</logic:notEqual>
				</ul>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<div id="ajaxdivcontentarea" class="contentstyle" style="display:visible; overflow:auto;" >
				</div>
			</td>
		</tr>
	</table>

<%@ include file="/common/GmFooter.inc"%>
</body>
</html>