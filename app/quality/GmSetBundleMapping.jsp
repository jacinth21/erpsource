<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ taglib prefix="fmtSetBundleSetupContainer" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- quality\GmSetBundleMapping.jsp -->
<fmtSetBundleSetupContainer:setLocale value="<%=strLocale%>"/>
<fmtSetBundleSetupContainer:setBundle basename="properties.labels.quality.GmSetBundleMapping"/>
<bean:define id="strBundleId" name="frmSetBundleMapping" property="strBundleId" type="java.lang.String"> </bean:define>
<bean:define id="keyCount" name="frmSetBundleMapping" property="keyCount" type="java.lang.Integer"> </bean:define>
<%
String strCollapseImage = strImagePath+"/minus.gif";
HashMap hmSystemMap = new HashMap();
hmSystemMap.put("ID","CODEID");
hmSystemMap.put("PID","CODEID");
hmSystemMap.put("NM","CODENM");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>System Setup</title>

</head>
<body leftmargin="20" topmargin="10">
<html:form action="/gmSetBundleMapping.do">
 <html:hidden styleId="strOpt" property="strOpt"/>
<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="2" class="LLine" height="1"></td>
	</tr>
	 <tr>
		 <td colspan="2" width="100%" height="100" valign="top">
			<table border="0" width="100%" cellspacing="0" cellpadding="0"> 
				<tr class="ShadeRightTableCaption">
					<td height="25" colspan="2" ><a href="javascript:fnShowFilters('tabSystem');" tabindex="-1" style="text-decoration:none; color:black" >
						<IMG id="tabSystemimg" border=0 src="<%=strCollapseImage%>">&nbsp;<fmtSetBundleSetupContainer:message key="LBL_BASIC"/></a>
					</td>
				</tr>
				<tr>
				<td colspan="2">
				               <!-- Added style="display:table" changes in PC-3662-Edge Browser fixes for Quality Module -->
					<table border="0" bordercolor="lightblue" width="100%" cellspacing="0" cellpadding="0" id="tabSystem" style="display:table">
						<tr><td colspan="2" class="LLine" height="1"></td></tr>
						<tr>
							<td class="RightTableCaption" align="right" height="24"><font color="red">*</font>&nbsp;<fmtSetBundleSetupContainer:message key="LBL_BUNDLE_NAME"/>&nbsp;:&nbsp;</td>	
						 <td> 
						     <html:text size="20" maxlength="30" name="frmSetBundleMapping"  property="txtSetName" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" ></html:text> 
						</td> 
						</tr>
						<tr><td colspan="2" class="LLine" height="1"></td></tr>
						<tr class="Shade">
		                	<td class="RightTableCaption" align="right" height="24"><fmtSetBundleSetupContainer:message key="LBL_BUNDLE_ID"/>&nbsp;:&nbsp;</td> 
		                	<td><bean:write name="frmSetBundleMapping" property="strBundleId"/></td>
		                </tr>						
		                <tr><td colspan="2" class="LLine" height="1"></td></tr>
						<tr>
							<td class="RightTableCaption" align="right" height="320"><fmtSetBundleSetupContainer:message key="LBL_BUNDLE_DESC"/>&nbsp;:&nbsp;</td>
							 <td>  
							 <textarea name="txtSetDesc" maxlength="500" class="HtmlTextArea"  onfocus="changeBgColor(this,'#AACCE8');" 
									onblur="changeBgColor(this,'#ffffff');"  rows="3" cols="50"> <bean:write name="frmSetBundleMapping" property="txtSetDesc"/></textarea> 
							</td> 
						</tr>
						<tr><td colspan="2" class="LLine" height="1"></td></tr>
						<tr class="Shade">
		                	<td class="RightTableCaption" align="right" height="320"><fmtSetBundleSetupContainer:message key="LBL_DETAIL_DESC"/>&nbsp;:&nbsp;</td> 
		                	<td><textarea name="txtSetDetailDesc" maxlength="500" class="HtmlTextArea" onfocus="changeBgColor(this,'#AACCE8');" 
									onblur="changeBgColor(this,'#ffffff');"  rows="3" cols="50"> <bean:write name="frmSetBundleMapping" property="txtSetDetailDesc"/></textarea> 
							</td>
		                </tr>
		                <tr><td colspan="2" class="LLine" height="1"></td></tr>
		                <tr>
							<td class="RightTableCaption" align="right" height="24"><font color="red">*</font>&nbsp;<fmtSetBundleSetupContainer:message key="LBL_DIVISION"/>:&nbsp;</td>
							<td><gmjsp:dropdown controlName="divName" SFFormName="frmSetBundleMapping" SFSeletedValue="divName"
							SFValue="alDivList" codeId="DIVISION_ID"  codeName="DIVISION_NAME"  defaultValue="[Choose One]"/>
							</td>
						</tr>
						<tr><td colspan="2" class="LLine" height="1"></td></tr>
						<tr class="Shade">
							<td class="RightTableCaption" align="right" height="24"><font color="red">*</font>&nbsp;<fmtSetBundleSetupContainer:message key="LBL_STATUS"/>:&nbsp;</td>
							<td>
							<gmjsp:dropdown controlName="stsName" SFFormName="frmSetBundleMapping" SFSeletedValue="stsName"
							SFValue="alSetStatus" codeId="CODEID"  codeName="CODENM"/>
							</td>
						</tr>
						<tr class="Shade"><td class="RightTableCaption" valign="middle" align="right" HEIGHT="1	"></td></tr>
					<tr><td class="RightTableCaption" valign="middle" align="right" height="25">&nbsp;<fmtSetBundleSetupContainer:message key="LBL_KEYWORDS"/>:&nbsp;</td>
						<td><textarea name="txtKeywords" class="InputArea" onfocus="changeBgColor(this,'#AACCE8');" 
									onblur="changeBgColor(this,'#ffffff');"  rows="3" cols="50"><bean:write name="frmSetBundleMapping" property="txtKeywords"/></textarea> 
					<%if(keyCount>1) { %>
						<a href="javascript:fnTagHistory(this.form);"> <img width="14" height="15" title="Click here to see the history" style="cursor: hand;" src="/images/icon_History.gif"> </a>
					<%} %>
						</td>
						
					</tr>					
					 <tr><td class="RightTableCaption" valign="middle" align="right" HEIGHT="2"></td></tr>					
					</table>
				</td>
				</tr>						
				<tr>
					<td colspan="2"> 
						<jsp:include page="/common/GmIncludeLog.jsp" > 
						<jsp:param name="FORMNAME" value="frmSetBundleMapping" />
						<jsp:param name="ALNAME" value="alLogReasons" />
						<jsp:param name="LogMode" value="Edit" />
						<jsp:param name="Mandatory" value="yes" />
						</jsp:include>
					</td>
				</tr>
				<tr><td colspan="2" height="15"></td></tr>
				<tr>
					<td colspan="2" align="center" height="24">					
		 				<%-- <logic:equal name="frmSetBundleMapping" property="setSetupSubmitAcc" value="Y">  --%>
			 				<fmtSetBundleSetupContainer:message key="BTN_SUBMIT" var="varSubmit"/>
			 				<fmtSetBundleSetupContainer:message key="BTN_RESET" var="varReset"/>
					    	<gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnSetSubmit(this.form);"/>&nbsp;&nbsp;
                    		<gmjsp:button value="${varReset}" gmClass="button" buttonType="Save" onClick="fnSystemReset(this.form);"/>&nbsp;&nbsp;
                    </td>
				</tr>
				<tr><td colspan="2" height="15"></td></tr>
			</table>
		</td>
	</tr>			
</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>