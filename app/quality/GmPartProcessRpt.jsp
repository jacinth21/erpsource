 <%
/**********************************************************************************
 * File		 		: GmPartProcessRpt.jsp
 * Desc		 		: This screen is used for displayimg the Part process Report.
 * Version	 		: 1.0
 * author			: Hreddi
************************************************************************************/
%>
<!-- quality\GmPartProcessRpt.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtPartProcessRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtPartProcessRpt:setLocale value="<%=strLocale%>"/>
<fmtPartProcessRpt:setBundle basename="properties.labels.quality.GmPartProcessRpt"/>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<bean:define id="alProjectList" name="frmPartProcessRpt" property="alProjectList" type="java.util.ArrayList"> </bean:define>
<bean:define id="alSystemList" name="frmPartProcessRpt" property="alSystemList" type="java.util.ArrayList"> </bean:define>
<bean:define id="alVendorList" name="frmPartProcessRpt" property="alVendorList" type="java.util.ArrayList"> </bean:define>
<bean:define id="xmlGridData" name="frmPartProcessRpt" property="xmlGridData" type="java.lang.String"> </bean:define>
<%
String strQualJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_QUALITY");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Part Process Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="STYLESHEET" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strQualJsPath%>/GmPartProcessRpt.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script>
var objGridData;
objGridData = '<%=xmlGridData%>';
var gridObj ='';
</script>
</HEAD>
<%
String strWikiTitle = GmCommonClass.getWikiTitle("PART_PROCESS_REPORTS"); 
%>

<!-- Custom tag lib code modified for JBOSS migration changes -->
<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<html:form action="/gmPartProcessRpt.do?">
	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="4">&nbsp;<fmtPartProcessRpt:message key="LBL_PART_PROCESS_REPORT"/></td>
			<td class="RightDashBoardHeader"><fmtPartProcessRpt:message key="IMG_ALT_HELP" var = "varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
		</tr>		
		<tr class='shade'>
			<td class="RightTableCaption" width="100" align="right" height="25" >&nbsp;<fmtPartProcessRpt:message key="LBL_PROJECT"/>:</td>					
			<td width="320">&nbsp;
				<gmjsp:dropdown controlName="projectID" controlId="projectID" SFFormName="frmPartProcessRpt" SFSeletedValue="projectID"
				SFValue="alProjectList" codeId="ID"  codeName="NAME"  defaultValue="[Choose One]" width="275"/>
			</td>
			<td class="RightTableCaption" align="right" width="80" height="25">&nbsp;<fmtPartProcessRpt:message key="LBL_SYSTEM"/>:</td>
			<td>&nbsp;
				<gmjsp:dropdown controlName="systemID" SFFormName="frmPartProcessRpt" SFSeletedValue="systemID"
				SFValue="alSystemList"  codeId="ID"  codeName="NAME"  defaultValue="[Choose One]" width="230"/>
			</td>
			<td></td>
		</tr>	
		<tr>
			<td class="RightTableCaption" align="right" height="25" width="100">&nbsp;<fmtPartProcessRpt:message key="LBL_PART"/>:</td>		
			<td width="320">&nbsp;
			<html:text size="20" styleClass="InputArea"  name="frmPartProcessRpt"  property="partNum" maxlength="20" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/>
			</td>
			<td class="RightTableCaption" align="right" height="25" width="80">&nbsp;<fmtPartProcessRpt:message key="LBL_VENDOR"/>:</td>
			<td>&nbsp;
				<gmjsp:dropdown controlName="vendorID" SFFormName="frmPartProcessRpt" SFSeletedValue="vendorID"
				SFValue="alVendorList" codeId="ID"  codeName="NAME"  defaultValue="[Choose One]" width="230"/>
			</td>
			<td align ="center" width="150"><fmtPartProcessRpt:message key="BTN_LOAD" var="varLoad"/> <gmjsp:button value="${varLoad}" name="btn_Load" gmClass="button" buttonType="Load" style="width: 5em" onClick="return fnLoad();" />  </td>
		</tr>
			<%if(!xmlGridData.equals("")){%>
		<tr>
			<td colspan="8">
				<div  id="Div_ValidPO" style="" height="471px" width="870px"></div>
				<div id="pagingArea" width="870px"></div>		
			</td>
		</tr>
			<%}else{%>
		<tr> <td colspan="8" height="1" class="LLine"></td>	</tr>
		<tr> <td colspan="8" align="center" height="" class="RightText"><fmtPartProcessRpt:message key="LBL_NO_DATA"/></td></tr>
			<%}%>
		<tr> <td height="1" colspan="8" class="LLine"></td> </tr>
			<%if( xmlGridData.indexOf("cell") != -1) {%>
		<tr>
			<td colspan="8" align="center">
			    <div class='exportlinks'><fmtPartProcessRpt:message key="DIV_EXPORT_OPT"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="fnDownloadXLS();"><fmtPartProcessRpt:message key="DIV_EXCEL"/></a></div>
			</td>
		</tr>
		<%} %>	
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
