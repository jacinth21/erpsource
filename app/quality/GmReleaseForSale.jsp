<%
/**********************************************************************************
 * File		 		: GmReleaseForSale.jsp
 * Desc		 		: Setting up Sale
 * Version	 		: 1.0
 * author			: Jignesh
************************************************************************************/
%>
<!-- quality\GmReleaseForSale.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtReleaseForSale" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtReleaseForSale:setLocale value="<%=strLocale%>"/>
<fmtReleaseForSale:setBundle basename="properties.labels.quality.GmReleaseForSale"/>
<%@ page import ="com.globus.common.beans.GmGridFormat"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<% 
	String strWikiTitle = GmCommonClass.getWikiTitle("RELEASE_FOR_SALE");
	String strQualJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_QUALITY");
	HashMap hmMapCOUNTRY = new HashMap();
	hmMapCOUNTRY.put("ID","");
	hmMapCOUNTRY.put("PID","CODEID");
	hmMapCOUNTRY.put("NM","CODENM");

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Release For Sale </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/fonts/font_roboto/roboto.css"/>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>

<script language="JavaScript" src="<%=strQualJsPath%>/GmReleaseForSale.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx_deprecated.js"></script>  

<bean:define id="strOpt" name="frmReleaseForSale" property="strOpt" type="java.lang.String"> </bean:define>
<bean:define id="gridData" name="frmReleaseForSale" property="strXMLGrid" type="java.lang.String"> </bean:define>
<bean:define id="msg" name="frmReleaseForSale" property="msg" type="java.lang.String"> </bean:define>
<bean:define id="invalidParts" name="frmReleaseForSale" property="invalidParts" type="java.lang.String"> </bean:define>
<bean:define id="alRFSCountry" name="frmReleaseForSale" property="alRFSCountry" type="java.util.ArrayList"> </bean:define>

<script>

	var objGridData;
	var strOpt;
	objGridData ='<%=gridData%>';
	strOpt = '<%=strOpt%>';

</script>
<style>
.even{
	background-color:#ffffff !important;
}
.uneven{
background-color:#dcedfc !important;;
}
		
div.gridbox table.hdr td {
   color: #000000 !important;
   background-color: #d2e9fc !important;
}
div.gridbox_material.gridbox .xhdr {
    background:#dcedfc !important;
}
</style>
</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad()">
<html:form action="/gmReleaseForSale.do">
<html:hidden property="strOpt" value=""/>
<html:hidden property="msg"/>
<html:hidden property="invalidParts"/>
<html:hidden property="hChkRFSCountry"/>
<html:hidden property="hPartNos"/>

<table  class="DtTable700" cellspacing="0" cellpadding="0" >
	<tr>
		<td width="1" class="Line"></td>
	</tr>
	<tr>
		<td>
			<table border="0" bordercolor="red" width="100%" cellspacing="0" cellpadding="0">
				<tr><td class="Line" colspan="3"></td></tr>
				<tr>
					<td colspan="2" height="25" class="RightDashBoardHeader">&nbsp;<fmtReleaseForSale:message key="LBL_RELEASE_FOR_SALE_SETUP"/></td>
				</tr>
				<tr><td class="LLine" height="1" colspan="3"></td></tr>
				<logic:notEqual  name="frmReleaseForSale" property="strOpt" value="load">
				<tr>
					<td colspan="3">
					<div id="rfMessage_Div">
						<table border="0" bordercolor="red" width="100%" cellspacing="0" cellpadding="0">
						<tr class="ShadeRightTableCaption">
							<td height="25" class="RightTableCaption" align="left">&nbsp;<bean:write name="frmReleaseForSale" property="msg"/> </td>
						</tr>
						</table>
					</div>
					</td>
				</tr>
				</logic:notEqual>
				<tr><td class="LLine" height="1" colspan="3"></td></tr>
				<logic:notEqual  name="frmReleaseForSale" property="strOpt" value="save">
				<tr>
					<td class="RightTableCaption" align="left" colspan="3" height="30">
					<div id="rfs_grid" style="display:inline" >
					<table cellpadding="1" cellspacing="1" border="0">
						<tr>
							<td class="RightTableCaption">
								&nbsp;<fmtReleaseForSale:message key="IMG_ALT_COPY" var = "varCopy"/><img src="<%=strImagePath%>/dhtmlxGrid/copy.gif" onClick="javascript:docopy()" alt="${varCopy}" style="border: none;" height="14">
								&nbsp;<fmtReleaseForSale:message key="IMG_ALT_PASTE" var = "varPaste"/><img src="<%=strImagePath%>/dhtmlxGrid/paste.gif" alt="${varPaste}" style="border: none;" onClick="javascript:pasteToGrid()" height="14">
								&nbsp;<fmtReleaseForSale:message key="IMG_ALT_ADD_ROW" var = "varAddRow"/><img src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="${varAddRow}" style="border: none;" onClick="javascript:addRow()"height="14">
								&nbsp;<fmtReleaseForSale:message key="IMG_ALT_REMOVE_ROW" var = "varRemoveRow"/><img src="<%=strImagePath%>/dhtmlxGrid/delete.gif" alt="${varRemoveRow}" style="border: none;" onClick="javascript:removeSelectedRow()" height="14">
								&nbsp;<fmtReleaseForSale:message key="IMG_ALT_ADD_ROWS_FROM_CLIPBOARD" var = "varAddRowsFromClipboard"/><img src="<%=strImagePath%>/dhtmlxGrid/row_add_after_1.gif" alt="${varAddRowsFromClipboard}" style="border: none;" onClick="javascript:addRowFromClipboard()" height="14">
							</td>									
						</tr>
					</table>
					</div>
					</td>				
			 	</tr>
			 	</logic:notEqual>
			 	<tr id="gridData">
					<td colspan="3">
						<div id="mygrid_container" style="height:250px"></div>			
					</td>
				</tr>
				
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<div id="rfsDetail_div">
				<table border="0" bordercolor="blue" width="100%" cellspacing="0" cellpadding="0">
					<logic:notEqual  name="frmReleaseForSale" property="strOpt" value="save">
					<tr  class="oddshade">
						<td class="RightTableCaption" align="right" height="24" ><font color="red">*</font> <fmtReleaseForSale:message key="LBL_COUNTRY"/>:&nbsp; </td>
						<td>&nbsp; 
							 <%=GmCommonControls.getChkBoxGroup("",alRFSCountry,"COUNTRY",hmMapCOUNTRY,"CODENM")%>&nbsp;
						</td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr class="evenshade">
						<td class="RightTableCaption" align="right" height="24"> <font color="red">*</font> <fmtReleaseForSale:message key="LBL_RELEASE_FOR_SALE"/>:&nbsp; </td>
						<td><gmjsp:dropdown controlName="rfsStatus" SFFormName="frmReleaseForSale" SFSeletedValue="rfsStatus"
							SFValue="alRFSStatus" codeId = "CODEID"  codeName = "CODENM"   defaultValue= "[Choose One]" />				
						</td>
					</tr>
					</logic:notEqual>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
				</table>
			</div>
		</td>
	</tr>
	<logic:notEqual  name="frmReleaseForSale" property="strOpt" value="load">
	<tr>
		<td colspan="3">
			<div id="rfsPartInfo_div">
				<table border="0" bordercolor="red" width="100%" cellspacing="0" cellpadding="0">
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="ShadeRightTableCaption">
						<td class="RightTableCaption" Height="20" colspan="2">&nbsp;<fmtReleaseForSale:message key="LBL_INVALID_PARTS"/></td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="evenshade">
						<td>&nbsp;&nbsp;
							<font color="red"><b><bean:write name="frmReleaseForSale" property="invalidParts"/></b></font>
						</td>
					</tr>
				</table>
			</div>
		</td>
	</tr>
	</logic:notEqual>
	<tr>
		<td colspan="3">
			<table border="0" bordercolor="red" width="100%" cellspacing="0" cellpadding="0">
				<tr class="ShadeRightTableCaption">
					<td class="RightTableCaption" align="left" valign="top"  colspan="2" height="24">&nbsp;<fmtReleaseForSale:message key="LBL_COMMENTS"/></td>
				</tr>
				<!-- Struts tag lib code modified for JBOSS migration changes -->
				<tr>
					<td class="RightTableCaption" colspan="2">
					<logic:equal  name="frmReleaseForSale" property="strOpt" value="load">
						<html:textarea property="comments" readonly="false" rows="4" cols="113" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
					</logic:equal>	
					<logic:equal  name="frmReleaseForSale" property="strOpt" value="save">
						<html:textarea property="comments" readonly="true" rows="4" cols="113" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
					</logic:equal>	
					</td>
				</tr>
				<tr><td class="LLine" height="1" colspan="2"></td></tr>
				<tr>                        
				    <td colspan="2" align="center" height="30">&nbsp;
				        <fmtReleaseForSale:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button name="Btn_Submit" value="&nbsp;${varSubmit}&nbsp;" buttonType="Save" gmClass="button" onClick="fnSubmit();" />
						<fmtReleaseForSale:message key="BTN_RESET" var="varReset"/><gmjsp:button value="&nbsp;${varReset}&nbsp;"  name="Btn_Reset" buttonType="Save" gmClass="button" onClick="fnReset();" />
                    </td>
				</tr>
			</table>
		</td>
	</tr>
</table>			
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>