<%
/**********************************************************************************************
 * File		 		: GmIncludePublicationInfo.jsp
 * Desc		 		: JSP file to display the publication display tag in setup and summary pages
 * Version	 		: 1.0
 * author			: Satyajit Thadeshwar
/***********************************************************************************************/
%>
<!--\party\GmIncludePublicationInfo.jsp  -->
<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtIncludePublicationInfo" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtIncludePublicationInfo:setLocale value="<%=strLocale%>"/>
<fmtIncludePublicationInfo:setBundle basename="properties.labels.party.GmIncludePublicationInfo"/>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<%@ page import="com.globus.party.forms.GmPublicationSetupForm"%>
<% 
GmPublicationSetupForm form = (GmPublicationSetupForm)pageContext.getAttribute("frmPublicationSetup", PageContext.REQUEST_SCOPE);
String partyId = form.getPartyId();
%>
<% 
	String displayType = request.getParameter("TYPE");
	String strMedia = displayType.equalsIgnoreCase("Setup") ? "HTML" : "FALSE";
	boolean bSortable = displayType.equalsIgnoreCase("Setup") ? true : false;
	bSortable = false;
%>
<display:table name="requestScope.frmPublicationSetup.lresult" class="gmits" id="currentRowObject"  export = "false" 
	decorator="com.globus.party.displaytag.DTPublicationInfoWrapper" > 
	<display:column property="PUBID" title="" class="alignleft" media="<%=strMedia%>"/>
	<fmtIncludePublicationInfo:message key="LBL_TYPE" var="varType"/><display:column property="PUBTYPE" title="${varType}" class="alignleft" sortable="<%=bSortable%>"/>
	<fmtIncludePublicationInfo:message key="LBL_TITLE" var="varTitle"/><display:column property="TITLE" title="${varTitle}" class="alignleft" sortable="<%=bSortable%>"/>
	<fmtIncludePublicationInfo:message key="LBL_DATE" var="varDate"/><display:column property="PUBDATE" title="${varDate}" sortable="<%=bSortable%>" class="alignleft"  />
	<fmtIncludePublicationInfo:message key="LBL_AUTHORS" var="varAuthors"/><display:column property="PAUTHOR" title="${varAuthors}" sortable="<%=bSortable%>" class="alignleft" />
	<fmtIncludePublicationInfo:message key="LBL_REFERENCE_URL" var="varReferenceURL"/><display:column property="PUBREF" title="${varReferenceURL}" class="alignleft" autolink="true"/>					
</display:table>
<% 
	String morePublications = GmCommonClass.parseNull((String)request.getAttribute("MORE_PUBLICATIONS"));
	if("Report".equals(displayType) && morePublications.equals("true")) { %>
	<div align="right"><a href="javascript:windowOpener('gmPublicationSetup.do?method=loadPublicationReport&TYPE=&partyId=<%=partyId%>', 'Publications', 'resizable=yes,scrollbars=yes,top=40,left=50,width=600,height=300,align=center');"><fmtIncludePublicationInfo:message key="LBL_MORE"/></a>&nbsp;</div>
<% } %>