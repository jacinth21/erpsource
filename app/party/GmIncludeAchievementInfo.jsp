<%
	/**********************************************************************************
	 * File		 		: GmIncludeAchievementInfo.jsp
	 * Desc		 		: Awards - include page
	 * Version	 		: 1.0
	 * author			: Brinal G
	 ************************************************************************************/
%>

<!-- \party\GmIncludeAchievementInfo.jsp -->
<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtIncludeAchievementInfo" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>


<fmtIncludeAchievementInfo:setLocale value="<%=strLocale%>"/>
<fmtIncludeAchievementInfo:setBundle basename="properties.labels.party.GmIncludeAchievementInfo"/>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<%@ page import="com.globus.party.forms.GmAcheivementSetupForm"%>
<% 
GmAcheivementSetupForm form = (GmAcheivementSetupForm)pageContext.getAttribute("frmAchievementSetup", PageContext.REQUEST_SCOPE);
String partyId = form.getPartyId();
%>
<% 

	String displayType = GmCommonClass.parseNull(request.getParameter("TYPE"));
	String strMedia = displayType.equals("Setup") ? "HTML" : "FALSE";
	boolean bSortable = displayType.equalsIgnoreCase("Setup") ? true : false;
	bSortable = false;
%>
<display:table name="requestScope.frmAchievementSetup.lresult" class="gmits" id="achievementtable" 
	decorator="com.globus.party.displaytag.DTAchievementWrapper">
	<display:column property="ID" title="" class="aligncenter" media="<%=strMedia%>"/>
	<fmtIncludeAchievementInfo:message key="LBL_TYPE" var="varType"/><display:column property="TYPE" title="${varType}" class="alignleft" sortable="<%=bSortable%>"/>
	<fmtIncludeAchievementInfo:message key="LBL_TITLE" var="varTitle"/><display:column property="TITLE" title="${varTitle}" class="alignleft" sortable="<%=bSortable%>"/>
	<fmtIncludeAchievementInfo:message key="LBL_DESCRIPTION" var="varDescription"/><display:column property="DETAIL" title="${varDescription}" class="alignleft" sortable="<%=bSortable%>"/>
	<fmtIncludeAchievementInfo:message key="LBL_DATE" var="varDate"/><display:column property="DT" title="${varDate}" class="alignleft" sortable="<%=bSortable%>"/>
	<fmtIncludeAchievementInfo:message key="LBL_CO_INVESTIGATORS" var="varCoInvestigators"/><display:column property="COINVESTIGATORS" title="${varCoInvestigators}" class="alignleft" sortable="false"/>
</display:table>
<% 
	String moreAchievements = GmCommonClass.parseNull((String)request.getAttribute("MORE_ACHIEVEMENTS"));
	if("Report".equals(displayType) && moreAchievements.equals("true")) { %>
	<div align="right">
	<a href="javascript:windowOpener('gmAchievementSetup.do?method=loadAchievementReport&TYPE=&partyId=<%=partyId%>', 'Awards, Grants and Patents', 'resizable=yes,scrollbars=yes,top=40,left=50,width=600,height=300,align=center');">More</a>&nbsp;
	</div>
<% } %>	