<%
/**********************************************************************************
 * File		 		: GmPartyResults.jsp
 * Desc		 		: This screen is used to display Issue or missing Tag Count
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>
<!--\party\GmPartyResults.jsp  -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>

<%@ taglib prefix="fmtPartyResults" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartyResults:setLocale value="<%=strLocale%>"/>
<fmtPartyResults:setBundle basename="properties.labels.party.GmPartyResults"/>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<bean:define id="alResult" name="frmPartySearch" property="alResult" scope="request" type="java.util.ArrayList"></bean:define>
<%
String strWikiTitle = GmCommonClass.getWikiTitle("PARTY_RESULTS");
%>
<html>
<title>Party Results </title>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>	

<%
ArrayList alList = alResult;//GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("RESULT"));
String strReturnValue = "";
HashMap hmMap = new HashMap();
String strPartyID = "";
String strPartyName = "";
String strPartyType = "";
String strFileId = "";
double intRowSize = 4; 
double imageSize = alList.size();
double i = Math.abs(imageSize / intRowSize);
int totalRows = (int) Math.ceil(i) ;
int totalTds = (int)(totalRows * intRowSize);
int lastRowColspan = (int)(totalTds - imageSize) + 1 ;

%>
<BODY leftmargin="20" topmargin="10" >

 <table border="0" class="DtTable700" cellspacing="0" cellpadding="1"> 
		<tr>
			<td colspan="5" height="20" class="RightDashBoardHeader">
<fmtPartyResults:message key="LBL_PARTY_RESULTS"/></td>	 
			<td  height="20" class="RightDashBoardHeader" align="right">
			<fmtPartyResults:message key="IMG_ALT_HELP" var="varHelp"/>
			<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>	
		<tr><td>
<table>
<% 
GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.party.GmPartyResults", strSessCompanyLocale);

String strNoResultsFound =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_NO_SEARCH_RESULT_FOUND"));
try
{
	 if (imageSize ==0)
	 {
		 strReturnValue = "<tr> <td height=50 align=center valign =center> <b> "+strNoResultsFound+" </b></td></tr>"; 
	 }
	  
	for (int count =0 ; count < imageSize  ; count++) 	
		{		   
		    if ( count % intRowSize == 0) 
		    {
		    	strReturnValue += "<tr>";
		    }

		    if (count == (imageSize - 1) )
		    {		    	
		    	strReturnValue += "<td colspan="+lastRowColspan+">";	
		    }
		    else
		    {
		    	strReturnValue += "<td>";  
		    }
			hmMap =(HashMap)alList.get(count);
			strPartyID = (String)hmMap.get("PARTY_ID");			 
			strPartyName = (String)hmMap.get("PARTY_NM");
			strPartyType = (String)hmMap.get("PARTY_TYPE");
			strFileId = (String)hmMap.get("FILE_ID");
			strReturnValue += "<table><tr><td>";
			//strReturnValue += "<a href=\"javascript:alert('Summary Page Under Construction');\">";
			strReturnValue += "<a href=gmPartySummary.do?partyId="+strPartyID+"&partyType="+strPartyType+">";
			strReturnValue += "<img src=/GmCommonFileOpenServlet?uploadString=UPLOADIMAGESHOME&sId="+strFileId+" height=80 width=80 /> </a> ";
			strReturnValue += "</td></tr>";
			strReturnValue += "<tr><td>";
			strReturnValue += strPartyName;
			strReturnValue += "</td></tr></table>";
			strReturnValue += "</td> ";
			
			 if ( (count + 1) % intRowSize == 0) 
			    {
			    	strReturnValue += "</tr>";
			    }						
		}
out.println(strReturnValue);
}
catch(Exception ex)
{
	
}
%>	
</table>
</td></tr>
</table>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</html>