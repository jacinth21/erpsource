<%
	/**********************************************************************************
	 * File		 		: GmIncludeLicenseInfo.jsp
	 * Desc		 		: License - include page
	 * Version	 		: 1.0
	 * author			: Angela Xiang
	 ************************************************************************************/
%>
<!--\party\GmIncludeLicenseInfo.jsp  -->
<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtIncludeLicenseInfo" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtIncludeLicenseInfo:setLocale value="<%=strLocale%>"/>
<fmtIncludeLicenseInfo:setBundle basename="properties.labels.party.GmIncludeLicenseInfo"/>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<%@ page import="com.globus.party.forms.GmLicenseSetupForm"%>
<% 
GmLicenseSetupForm form = (GmLicenseSetupForm)pageContext.getAttribute("frmLicenseSetup", PageContext.REQUEST_SCOPE);
String partyId = form.getPartyId();
%>
<% 
	String displayType = GmCommonClass.parseNull(request.getParameter("TYPE"));
	String strMedia = displayType.equals("Setup") ? "HTML" : "FALSE";
	boolean bSortable = displayType.equalsIgnoreCase("Setup") ? true : false;
	bSortable = false;
%>
<display:table name="requestScope.frmLicenseSetup.lresult"  class="gmits"  id="licenseinfotable" decorator="com.globus.party.displaytag.DTLicenseWrapper" >
	<display:column property="LICENSEID" title="" class="aligncenter" media="<%=strMedia%>"/>
	<fmtIncludeLicenseInfo:message key="LBL_LICENSE" var="varLicense"/><display:column property="TITLE" title="${varLicense}" class="alignleft" sortable="<%=bSortable%>"/>
	<fmtIncludeLicenseInfo:message key="LBL_DATE_OF_ISSUE" var="varDateofIssue"/><display:column property="DATEISSUE" title="${varDateofIssue}" class="alignleft" sortable="<%=bSortable%>"/>
	<fmtIncludeLicenseInfo:message key="LBL_DESCRIPTION" var="varDescription"/><display:column property="DESCRIPTION" title="${varDescription}" class="alignleft" />
	<fmtIncludeLicenseInfo:message key="LBL_COUNTRY" var="varCountry"/><display:column property="COUNTRYNAME" title="${varCountry}" class="alignleft" sortable="<%=bSortable%>"/>
	<fmtIncludeLicenseInfo:message key="LBL_STATE" var="varState"/><display:column property="STATENAME" title="${varState}" class="alignleft" sortable="<%=bSortable%>" />
</display:table>
<% 
	String moreLicenses = GmCommonClass.parseNull((String)request.getAttribute("MORE_LICENSES"));
	if(moreLicenses.equals("true")) { %>
	<div align="right">
		<a href="javascript:windowOpener('gmLicenseSetup.do?method=loadLicenseReport&TYPE=&partyId=<%=partyId%>', 'Licenses', 'resizable=yes,scrollbars=yes,top=40,left=50,width=600,height=300,align=center');"><fmtIncludeLicenseInfo:message key="LBL_MORE"/></a>&nbsp;
	</div>
<% } %>	