<!--party\GmPartySetup.jsp  -->
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtPartySetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartySetup:setLocale value="<%=strLocale%>"/>
<fmtPartySetup:setBundle basename="properties.labels.party.GmPartySetup"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Party Setup</title>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
</head>
<bean:define id="partyType" name="frmPartySetup" property="partyType" scope="request" type="java.lang.String"></bean:define>
<%
partyType = partyType == null ? "" : partyType;

String strOddShade = "";
String strEvenShade = "";
String strDealerNameENFlag = "";
String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
String strClassShade="";
GmResourceBundleBean gmResourceBundleBean =
GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
 strDealerNameENFlag = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHOW_DEALER_NAME_EN"));
  if (partyType.equals("7007"))
  {
	  strOddShade = "oddshade";
	  strEvenShade ="evenshade";
  }
  else
  {
	  strOddShade = "Shade";
	  strEvenShade ="";
  }
%>
<body leftmargin="20" topmargin="10">
<html:form action="/gmPartySetup.do">
<html:hidden property="strOpt"/>
<html:hidden property="partyType" styleId="partyType"/>

<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VDPTY"/>
<html:hidden property="accessFlag"/> 

<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="2" height="15"></td>
	</tr>
	 <tr>
		 <td colspan="2" width="100%" height="100" valign="top">
			<table border="0" width="100%" cellspacing="0" cellpadding="0"> 
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<logic:equal name="frmPartySetup" property="displayPartyName" value="true">
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr class="<%=strOddShade%>">
					<td class="RightTableCaption" align="right" width="40%" height="24"><font color="red">*&nbsp;</font><fmtPartySetup:message key="LBL_NAME"/>:</td>
					<td width="60%">&nbsp; <html:text property="partyName" size="30"
						onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
						onblur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>			
				</logic:equal>
				<logic:notEqual name="frmPartySetup" property="displayPartyName" value="true">
				<logic:notEqual name="frmPartySetup" property="accessFlag" value="Y">
				<tr class="<%=strOddShade%>">
					<td class="RightTableCaption" align="right" width="40%" height="24"><font color="red">*&nbsp;</font><fmtPartySetup:message key="LBL_FIRST_NAME"/>:</td>
					<td width="60%">&nbsp; <html:text property="firstName" size="30"
						onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
						onblur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr class="<%=strEvenShade%>">
					<td class="RightTableCaption" align="right" width="40%" height="24"><font color="red">*&nbsp;</font><fmtPartySetup:message key="LBL_LAST_NAME"/>:</td>
					<td width="60%">&nbsp; <html:text property="lastName" size="30"
						onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
						onblur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr class="<%=strOddShade%>">
					<td class="RightTableCaption" align="right" width="40%" height="24"><fmtPartySetup:message key="LBL_MIDDLE_INITIAL"/>:</td>
					<td width="60%">&nbsp; <html:text property="middleInitial" size="5"
						onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
						onblur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				</logic:notEqual>			
				</logic:notEqual>
				<logic:notEqual name="frmPartySetup" property="displayPartyName" value="true">
				<logic:equal name="frmPartySetup" property="accessFlag" value="Y">
				<tr class="<%=strOddShade%>">
					<td class="RightTableCaption" align="right" width="40%" height="24"><font color="red">*&nbsp;</font><fmtPartySetup:message key="LBL_NAME"/>:</td>
					<td width="60%">&nbsp; <html:text property="partyName" size="30"
						onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
						onblur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<%if(strDealerNameENFlag.equals("YES")){%>
				<tr class="<%=strEvenShade%>">
					<td class="RightTableCaption" align="right" width="40%" height="24"><font color="red">*&nbsp;</font><fmtPartySetup:message key="LBL_DEALER_NAME_EN"/>:</td>
					<td width="60%">&nbsp; <html:text property="partyNameEn" size="30"
						onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
						onblur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>
				<% 
				strClassShade= "oddshade";
				}else{
				  strClassShade = "evenshade";
				}
				%>
				
				</logic:equal>
				</logic:notEqual>
                <tr class="<%=strClassShade%>">
                	<td class="RightTableCaption" align="right" width="40%" height="24"><fmtPartySetup:message key="LBL_INACTIVE_FLAG"/>:</td> 
                	<td width="60%">&nbsp;<html:checkbox property="inactiveFlag" /></td>
                </tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr><td colspan="2" height="15"></td></tr>
				<tr>
					<td colspan="2">
						<jsp:include page="/common/GmIncludeLog.jsp">
							<jsp:param name="FORMNAME" value="frmPartySetup" />
							<jsp:param name="ALNAME" value="alLogReasons" />
							<jsp:param name="LogMode" value="Edit" />
						</jsp:include>
					</td>
				</tr>
				<tr><td colspan="2" height="15"></td></tr>
				<tr>
					<td colspan="2" align="center" height="24">&nbsp;
                    	<fmtPartySetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" gmClass="button" name="Btn_Submit" buttonType="Save" onClick="fnPartySubmit(this.form);"/>&nbsp;&nbsp;
                    	<fmtPartySetup:message key="BTN_RESET" var="varReset"/><gmjsp:button value="${varReset}" gmClass="button" buttonType="Save" onClick="fnPartyReset(this.form);"/>&nbsp;&nbsp;
                    	<fmtPartySetup:message key="BTN_VOID" var="varVoid"/><gmjsp:button value="${varVoid}" gmClass="button" buttonType="Save" onClick="fnPartyVoid(this.form);"/>
					</td>
				</tr>
				<tr><td colspan="2" height="15"></td></tr>
			</table>
		</td>
	</tr>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>