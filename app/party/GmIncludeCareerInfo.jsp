<%
/**********************************************************************************************
 * File		 		: GmIncludeCareerInfo.jsp
 * Desc		 		: JSP file to display the career display tag in setup and summary pages
 * Version	 		: 1.1
 * Author			: Tarika
 * Last Updated By	: Satyajit Thadeshwar
/***********************************************************************************************/
%>
<!--\party\GmIncludeCareerInfo.jsp  -->
<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtIncludeCareerInfo" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtIncludeCareerInfo:setLocale value="<%=strLocale%>"/>
<fmtIncludeCareerInfo:setBundle basename="properties.labels.party.GmIncludeCareerInfo"/>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<% 
	String displayType = request.getParameter("TYPE");
	String strMedia = displayType.equalsIgnoreCase("Setup") ? "HTML" : "FALSE";
	boolean bSortable = displayType.equalsIgnoreCase("Setup") ? true : false;
	bSortable = false;
%>
<display:table name="requestScope.frmCareerSetup.lresult" class="gmits" id="currentRowObject" decorator="com.globus.party.displaytag.DTCareerInfoWrapper">  
	<display:column property="CAREERID" title="" class="aligncenter" media="<%=strMedia%>"/>
	<fmtIncludeCareerInfo:message key="LBL_TITLE" var="varTitle"/><display:column property="TITLE" title="${varTitle}" class="alignleft"/>
	<fmtIncludeCareerInfo:message key="LBL_COMPANY_NAME" var="varCompanyName"/><display:column property="COMPANYNAME" class="alignleft" title="${varCompanyName}"/> 
	<fmtIncludeCareerInfo:message key="LBL_START_DATE" var="varStartDate"/><display:column property="STARTDATE" title="${varStartDate}" sortable="<%=bSortable%>" class="alignleft"/>
	<fmtIncludeCareerInfo:message key="LBL_END_DATE" var="varEndDate"/><display:column property="ENDDATE" title="${varEndDate}" sortable="<%=bSortable%>"  class="alignleft" />
	<fmtIncludeCareerInfo:message key="LBL_GROUP" var="varGroup"/><display:column property="GROUPDIVISION" title="${varGroup}" sortable="<%=bSortable%>" class="alignleft" />		
	<fmtIncludeCareerInfo:message key="LBL_COUNTRY" var="varCountry"/><display:column property="COUNTRY" title="${varCountry}" sortable="<%=bSortable%>" class="alignleft" />		
	<fmtIncludeCareerInfo:message key="LBL_STATE" var="varState"/><display:column property="STATE" title="${varState}" sortable="<%=bSortable%>" class="alignleft" />
	<fmtIncludeCareerInfo:message key="LBL_CITY" var="varCity"/><display:column property="CITY" title="${varCity}" sortable="<%=bSortable%>" class="alignleft" />		
</display:table>
<%-- 
	<display:column property="INDUSTRY" title="Industry" sortable="<%=bSortable%>" class="alignleft" />	
	<display:column property="COMPANYDESC" title="Description" sortable="<%=bSortable%>" class="alignleft" />
--%>					
