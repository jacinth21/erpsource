<!--party\GmIncludeGlobusInfoSummary.jsp  -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtIncludeGlobusInfoSummary" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtIncludeGlobusInfoSummary:setLocale value="<%=strLocale%>"/>
<fmtIncludeGlobusInfoSummary:setBundle basename="properties.labels.party.GmIncludeGlobusInfoSummary"/>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
</head>
<body>
<table cellspacing="0" cellpadding="0" style="border: 1px solid #676767; WORD-BREAK:BREAK-ALL;">
	<tr class="TableHeaderRow">
		<td align="left" width="40%">&nbsp;<fmtIncludeGlobusInfoSummary:message key="LBL_GLOBUS_ASSOCIATION"/></td>
		<td align="right" width="60%">
			<a href="/gmPartySetup.do?partyId=<bean:write name="frmGlobusInfoSummary" property="partyId"/>&partyType=<bean:write name="frmGlobusInfoSummary" property="partyType"/>"><fmtIncludeGlobusInfoSummary:message key="LBL_EDIT"/>Edit</a>&nbsp;
		</td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>		
	<tr height="20">
		<td align="right" width="40%" class="RightTableCaption"><fmtIncludeGlobusInfoSummary:message key="LBL_ASSOCIATED_REPS"/>:&nbsp;</td>
		<td align="left" width="60%"><bean:write name="frmGlobusInfoSummary" property="reps"/></td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>	
	<tr height="20" class="Shade">
		<td align="right" width="40%" class="RightTableCaption"><fmtIncludeGlobusInfoSummary:message key="LBL_AREA_DIRECTOR"/>:&nbsp;</td>
		<td align="left" width="60%"><bean:write name="frmGlobusInfoSummary" property="adName"/></td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>	
	<tr height="20">
		<td align="right" width="40%" class="RightTableCaption"><fmtIncludeGlobusInfoSummary:message key="LBL_REGION"/>:&nbsp;</td>
		<td align="left" width="60%"><bean:write name="frmGlobusInfoSummary" property="region"/></td>
	</tr>
</table>
</body>
</html>