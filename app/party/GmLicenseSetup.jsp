<!--\party\GmLicenseSetup.jsp  -->
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtLicenseSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtLicenseSetup:setLocale value="<%=strLocale%>"/>
<fmtLicenseSetup:setBundle basename="properties.labels.party.GmLicenseSetup"/>
<HTML>
<HEAD>
<TITLE>Globus Medical: License Setup</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
</head>
<body leftmargin="20" topmargin="10">
<html:form action="/gmLicenseSetup.do">
<html:hidden property="licenseId"/>
<html:hidden property="partyId"/>

<html:hidden property="hTxnId" value="" />
<html:hidden property="hTxnName" value="" />
<html:hidden property="hCancelType" value="VDLIC" />
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr><td colspan="2" height="15"></td></tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr height="24" class="Shade">
			<td width="30%" class="RightTableCaption" align="right"><font color="red">*</font>&nbsp;<fmtLicenseSetup:message key="LBL_LICENSE"/>:&nbsp;</td>
			<td width="70%">&nbsp;<html:text property="title" size="61"
				onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
				onblur="changeBgColor(this,'#ffffff');" /></td>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr height="24">
			<td width="30%" class="RightTableCaption" align="right"><fmtLicenseSetup:message key="LBL_DATE_OF_ISSUE"/>:&nbsp;</td>
			<td width="70%" class="RightTableCaption">&nbsp;<fmtLicenseSetup:message key="LBL_MONTH"/>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
				<gmjsp:dropdown controlName="issueMonth" SFFormName="frmLicenseSetup" SFSeletedValue="issueMonth" 
				SFValue="alMonth" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" />
				&nbsp;Year&nbsp;&nbsp;<html:text property="issueYear" size="4" onfocus="changeBgColor(this,'#AACCE8');"
				styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" />
			</td>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr height="24" class="Shade">
			<td width="30%" class="RightTableCaption" align="right">&nbsp;<fmtLicenseSetup:message key="LBL_DESCRIPTION"/>:&nbsp;</td>
			<td width="70%">
			&nbsp;<html:textarea property="description" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
					onblur="changeBgColor(this,'#ffffff');" cols="60" rows="3"></html:textarea></td>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr height="24">
			<td width="30%" class="RightTableCaption" align="right">&nbsp;<fmtLicenseSetup:message key="LBL_COUNTRY"/>:&nbsp;</td>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
			<td width="70%">&nbsp;<gmjsp:dropdown controlName="country"
				SFFormName="frmLicenseSetup" SFSeletedValue="country"
				SFValue="alCountries" codeId="CODEID" codeName="CODENM"
				defaultValue="[Choose One]" />
			</td>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr height="24" class="Shade">
			<td width="30%" class="RightTableCaption" align="right"><fmtLicenseSetup:message key="LBL_STATE"/>:&nbsp;</td>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
			<td width="70%">&nbsp;<gmjsp:dropdown controlName="state"
				SFFormName="frmLicenseSetup" SFSeletedValue="state"
				SFValue="alStates" codeId="CODEID" codeName="CODENM"
				defaultValue="[Choose One]" />
			</td>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr><td colspan="2" height="15"></td></tr>
		<tr height="24">
			<td colspan="2" align="center">
			<!-- Struts tag lib code modified for JBOSS migration changes -->
				<fmtLicenseSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button name="Submit" buttonType="Save" value="${varSubmit}" gmClass="button" onClick="fnLicenseSubmit(this.form);" />
				<fmtLicenseSetup:message key="BTN_RESET" var="varReset"/><gmjsp:button name="resetBtn" value="${varReset}" gmClass="button" buttonType="Save" onClick="fnLicenseReset(this.form);" />&nbsp;
				<fmtLicenseSetup:message key="BTN_VOID" var="varVoid"/><gmjsp:button name="voidBtn" value="${varVoid}" gmClass="button" buttonType="Save" onClick="fnLicenseVoid(this.form);" />&nbsp;
			</td>
		</tr>
		<tr><td colspan="3" height="15"></td></tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr>
			<td colspan="2">
			<!-- Changed for JBOSS migration -->
				<jsp:include page="/gmLicenseSetup.do?method=loadLicenseReport" flush="true">
					<jsp:param name="TYPE" value="Setup" />
				</jsp:include>
			</td>
		</tr>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>


</BODY>

</HTML>