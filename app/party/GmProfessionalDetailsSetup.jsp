
<%
	/**********************************************************************************
	 * File		 		: GmProfessionalDetailsSetup.jsp
	 * Desc		 		: ProfessionalDetailsSetup - Professional Details page
	 * Version	 		: 1.0
	 * author			: Tarika Chandure
	 ************************************************************************************/
%>
<!-- party\GmProfessionalDetailsSetup.jsp -->

<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtProfessionalDetailsSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtProfessionalDetailsSetup:setLocale value="<%=strLocale%>"/>
<fmtProfessionalDetailsSetup:setBundle basename="properties.labels.party.GmProfessionalDetailsSetup"/>

String strPartyJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PARTY");


<%@page import="java.util.ArrayList"%>
<html>
<head>

<%
ArrayList fieldValue = (ArrayList)request.getAttribute("fieldValue");
GmCommonClass.parseNullArrayList(fieldValue);
log.debug("fieldValue = " + fieldValue);
%>

<title>Professional Details</title>
<script>
function fnSave() {
	document.frmProfessionalDetailsSetup.hAction.value = "save";
	document.frmProfessionalDetailsSetup.submit();  
}
</script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strPartyJsPath%>/GmParty.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

</head>
<body>
<html:form action="/gmProfessionalDetailsSetup.do" >
<input type="hidden" name="hAction"	/>
<html:hidden property="partyId" />
<table border="0" class="DtTable680" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr><td colspan="2" height="15"></td></tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<%				
					String style = "";
					for (int i = 0;i < fieldValue.size() ;i++ ) {
					HashMap	hmLoop = (HashMap)fieldValue.get(i);
					if(i%2 == 0) {
						style = "Shade";
					} else {
						style = "N/A";
					}
				%>
					<tr class="<%=style%>">
						<td height="25" align="right" valign="top"><b><%=hmLoop.get("CODENM")%>&nbsp;:&nbsp;</b></td> 
						<td align="left" valign="center">&nbsp;
						<!-- Struts tag lib code modified for JBOSS migration changes -->
							<html:textarea rows="3" cols="50" property='<%=(String)hmLoop.get("CODENMALT")%>' 
							name="frmProfessionalDetailsSetup" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" 
							onblur="changeBgColor(this,'#ffffff');"></html:textarea> &nbsp;&nbsp;<fmtProfessionalDetailsSetup:message key="LBL_SEPARATE_BY_SEMICOLON"/> 
						</td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<%} %>
				<tr><td colspan="2" height="15"></td></tr>
				<tr>						    					
					<td colspan="2" height="24" align="center"><fmtProfessionalDetailsSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" buttonType="Save" gmClass="button" onClick="fnSave();"/></td>						
				</tr>
				<tr><td colspan="2" height="15"></td></tr>				
			</table>
		</td>
	</tr>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>