<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
/**********************************************************************************
 * File		 		: GmPartyContactSetup.jsp
 * Desc		 		: Setting up contacts for the party
 * Version	 		: 1.0
 * author			: Satyajit Thadeshwar
************************************************************************************/
%>

<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtPartyContactSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartyContactSetup:setLocale value="<%=strLocale%>"/>
<fmtPartyContactSetup:setBundle basename="properties.labels.party.GmPartyContactSetup"/>

<% 
String strWikiTitle = GmCommonClass.getWikiTitle("CONTACT_SETUP");
try { %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title> Globus Medical: Contact Setup </title>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>
var lblContactType='<fmtPartyContactSetup:message key="LBL_CONTACT_TYPE"/>';
var lblPreference='<fmtPartyContactSetup:message key="LBL_PREFERENCE"/>';
var lblContactMode='<fmtPartyContactSetup:message key="LBL_CONTACT_MODE"/>';
var lblContactValue='<fmtPartyContactSetup:message key="LBL_CONTACT_VALUE"/>';
function fnFetch() {	
	fnReset();
	document.frmPartyContactSetup.submit();
}

function fnEditId(val) {	
	document.frmPartyContactSetup.hcontactID.value = val;
	document.frmPartyContactSetup.strOpt.value = "edit";
	document.frmPartyContactSetup.submit();
}

function fnSubmit(val) {
	fnValidateDropDn('partyId',message_prodmgmnt[313]);
	fnValidateDropDn('contactType',lblContactType);
	fnValidateDropDn('preference',lblPreference);
	fnValidateDropDn('contactMode',lblContactMode);
	fnValidateDropDn('contactValue',lblContactValue);
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmPartyContactSetup.strOpt.value = 'save';
	fnStartProgress('Y');
	document.frmPartyContactSetup.submit();
}

function fnReset() { 
 	document.frmPartyContactSetup.hcontactID.value = "";
 	document.frmPartyContactSetup.contactType.value = "0";
 	document.frmPartyContactSetup.preference.value = "0";
 	document.frmPartyContactSetup.contactMode.value = "0";
 	document.frmPartyContactSetup.contactValue.value = "";
 	document.frmPartyContactSetup.inActiveFlag.checked = false;  
}
</script>

</head>
<body>

<!-- Custom tag lib code modified for JBOSS migration changes -->
<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmContactInfo.do">
<html:hidden property="strOpt" value=""/>
<html:hidden property="hcontactID" name="frmPartyContactSetup"/>
<html:hidden property="partyType" name="frmPartyContactSetup"/>
 
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtPartyContactSetup:message key="LBL_CONTACT_SETUP"/></td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' 
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>			
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
		<tr>
			<td width="698" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="30"></font><font color="red">*</font>&nbsp;<fmtPartyContactSetup:message key="LBL_CONTACT_TYPE"/>:</td> 
                        <td>&nbsp;
							<gmjsp:dropdown controlName="contactType" SFFormName="frmPartyContactSetup" SFSeletedValue="contactType"
								SFValue="alContactTypes" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
                        </td>
                    </tr>                    
                    <tr><td colspan="2" bgcolor="#cccccc"></td></tr> 
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="30"></font><font color="red">*</font>&nbsp;<fmtPartyContactSetup:message key="LBL_PREFERENCE"/>:</td>
                        <td>&nbsp;
							<gmjsp:dropdown controlName="preference" SFFormName="frmPartyContactSetup" SFSeletedValue="preference"
								SFValue="alPreferences" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
						</td>
                    </tr>                    
                    <tr><td colspan="2" bgcolor="#cccccc"></td></tr> 
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtPartyContactSetup:message key="LBL_CONTACT_MODE"/>:</td>
                        <td>&nbsp;
							<gmjsp:dropdown controlName="contactMode" SFFormName="frmPartyContactSetup" SFSeletedValue="contactMode"
								SFValue="alContactModes" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
						</td>
                    </tr>
                    <tr><td colspan="2" bgcolor="#cccccc"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24"><fmtPartyContactSetup:message key="LBL_CONTACT_VALUE"/>:</td> 
                        <td>&nbsp; <html:text property="contactValue"   size="12" name="frmPartyContactSetup" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
                        </td> 
                    </tr>
                    <tr><td colspan="2" bgcolor="#cccccc"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24"><fmtPartyContactSetup:message key="LBL_INACTIVE_FLAG"/>:</td> 
                        <td>&nbsp;<html:checkbox property="inActiveFlag" />
                         </td>
                    </tr>
                    <tr>
                    <td colspan="2">
                    	<jsp:include page="/common/GmIncludeLog.jsp" >
							<jsp:param name="FORMNAME" value="frmPartyContactSetup" />
							<jsp:param name="ALNAME" value="alLogReasons" />
							<jsp:param name="LogMode" value="Edit" />
						</jsp:include>
					</td>
                    </tr>                    
                    <tr>                        
                        <td colspan="2" align="center" height="30">&nbsp;
                        <fmtPartyContactSetup:message key="BTN_ADD_NEW" var="varAddNew"/><gmjsp:button value="${varAddNew}" gmClass="button" buttonType="Save" onClick="fnReset();"/>   
                        <fmtPartyContactSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnSubmit();" />                     
                        </td>
                    </tr> 
           			<tr> 
            			<td colspan="2">
            				<display:table name="requestScope.frmPartyContactSetup.ldtResult" class="its" id="currentRowObject"  export = "false" decorator="com.globus.displaytag.beans.GmCommonSetupWrapper" > 
							<display:column property="ID" title="" class="alignleft"/>
							<fmtPartyContactSetup:message key="LBL_TYPE" var="varType"/><display:column property="CTYPE" title="${varType}" class="alignleft"/> 
							<fmtPartyContactSetup:message key="LBL_MODE" var="varMode"/><display:column property="CMODE" title="${varMode}" sortable="true"  />
							<fmtPartyContactSetup:message key="LBL_VALUE" var="varValue"/><display:column property="CVALUE" title="${varValue}" sortable="true" class="alignleft" />
							<fmtPartyContactSetup:message key="LBL_PREFERENCE" var="varPreference"/><display:column property="PREF" title="${varPreference}" sortable="true"  class="alignleft" />
							<fmtPartyContactSetup:message key="LBL_STATUS" var="varStatus"/><display:column property="INACTIVEFL" title="${varStatus}" sortable="true"  class="aligncenter" />						
							</display:table> 
						</td>
   					</tr> 
      				</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>

</body>
<% 
} 
catch (Exception e) {
e.printStackTrace();
} %>
</html>