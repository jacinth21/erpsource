<!-- party\GmPartySetupContainer.jsp -->
<%@ include file="/common/GmHeader.inc" %>
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ taglib prefix="fmtPartySetupContainer" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page isELIgnored="false"%>

<fmtPartySetupContainer:setLocale value="<%=strLocale%>"/>
<fmtPartySetupContainer:setBundle basename="properties.labels.party.GmPartySetupContainer"/>
<bean:define id="partyId" name="frmPartySetup" property="partyId" scope="request" type="java.lang.String"></bean:define>
<bean:define id="partyType" name="frmPartySetup" property="partyType" scope="request" type="java.lang.String"></bean:define>
<bean:define id="strDealerAcctFlag" name="frmPartySetup" property="strDealerAcctFlag" scope="request" type="java.lang.String"></bean:define>
<%

String strPartyJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PARTY");
String strCommonJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_COMMON");
String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
GmResourceBundleBean gmResourceBundleBean =
GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
String strPaymentTermFlag = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHOW_PAYMENTTERM_MONTH"));
String strValidatePartyList = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("VALIDATE_PARTY_TYPE"));

partyType = partyType == null ? "" : partyType;

String strBodyLeftMargin ="";
String strBodyTopMargin ="";
String strHeader = "";

  if (partyType.equals("7007"))
  {
	  strBodyLeftMargin = "0";
	  strBodyTopMargin = "0";
	  strHeader = "Header";
  }
  else
  {
	  strBodyLeftMargin = "20";
	  strBodyTopMargin = "10";
	  strHeader = "RightDashBoardHeader";
  }
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Party Setup</title>

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />

<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strPartyJsPath%>/GmParty.js"></script>
<script language="JavaScript" src="<%=strCommonJsPath%>/GmPartyShipParamsSetup.js"></script>
<script language="JavaScript" src="<%=strPartyJsPath%>/GmDealerInoviceParams.js"></script>
<script>
var paymentTermFlag = '<%=strPaymentTermFlag%>';
var strDealerAcctFlag = '<%=strDealerAcctFlag%>';
function fnOpenSummaryPage(partyId, partyType) {
	windowOpener('gmPartySummary.do?partyId='+partyId+'&partyType='+partyType, 'PartySummary', 'resizable=yes,scrollbars=yes,top=40,left=50,width=800,height=1000,align=center');
}
var partyType = '<%=partyType%>';
var validatePartyList = '<%= strValidatePartyList %>';
var validatePartyListArr = validatePartyList.split(',');
var inclPartyIdFlag = 'N';
//Based in
if (validatePartyListArr.indexOf(partyType)!= -1){
	inclPartyIdFlag = 'Y';
}
</script>
<%
// Below css file is needed only for the screens in Clinical module; 7000: Surgeon, 7007:Site Coordinator
if(partyType.equals("7000") || partyType.equals("7007")){ %>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<%} %>
</head>
<body leftmargin="<%=strBodyLeftMargin%>" topmargin="<%=strBodyTopMargin%>" onload="fnAjaxTabLoad();">
<html:form action="/gmPartySetup.do">

<html:hidden property="hcurrentTab" value=""/>
<html:hidden property="partyType" styleId="partyType"/>
<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="<%=strHeader %>">&nbsp;<bean:write name="frmPartySetup" property="title"/>&nbsp;<fmtPartySetupContainer:message key="LBL_SETUP"/></td>
			<td align="right" class="<%=strHeader %>" > 
			     <% if (!partyType.equals("7007"))
  					 {
				 %>  
				
<fmtPartySetupContainer:message key="IMG_ALT_HELP" var="varHelp"/><img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' 
				onClick="javascript:fnPartyHelp('<%=strWikiPath%>');" />&nbsp;
				 
				<% } %>
			</td>
		</tr>
		<tr>
			<td colspan="2" height="1" bgcolor="#666666"></td>
		</tr>
			<tr>
				<td class="RightTableCaption" align="right" height="30" width="40%"><font color="red">*</font>&nbsp;<bean:write name="frmPartySetup" property="title"/>&nbsp;:</td>					
				<td width="60%">&nbsp;<!-- Custom tag lib code modified for JBOSS migration changes -->
					<gmjsp:dropdown controlName="partyId" onChange="fnPartyFetch(this.form);" SFFormName="frmPartySetup" SFSeletedValue="partyId"
					SFValue="alParty" codeId="ID"  codeName="NAME"  defaultValue="[Choose One]"/>
					
					<logic:notEqual name="frmPartySetup" property="partyId" value="">
		 				<logic:notEqual name="frmPartySetup" property="partyId" value="0">
		 					 <logic:notEqual name="frmPartySetup" property="partyType" value="7007">
		 					 	<logic:notEqual name="frmPartySetup" property="partyType" value="7008"> 
									<logic:notEqual name="frmPartySetup" property="accessFlag" value="Y">
									&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:fnOpenSummaryPage('<bean:write name="frmPartySetup" property="partyId"/>', '<bean:write name="frmPartySetup" property="partyType"/>');"><fmtPartySetupContainer:message key="LBL_VIEW_SUMMARY"/></a>
									</logic:notEqual>
								</logic:notEqual>
							</logic:notEqual>
						</logic:notEqual>
					</logic:notEqual>
				</td>
			</tr>
		<tr>
			<td colspan="2" height="1" bgcolor="#666666"></td>
		</tr>	
</html:form>
		<tr>
			<td colspan="2">
				<ul id="maintab" class="shadetabs">
					<li onclick="fnUpdateTabIndex(this);"><a href="/gmPartySetup.do?partyId=<bean:write name="frmPartySetup" property="partyId"/>&partyType=<bean:write name="frmPartySetup" property="partyType"/>
					&strOpt=fetch&partyType=<bean:write name="frmPartySetup" property="partyType"/>" title="Basic Info" rel="ajaxdivcontentarea" name="Basic Info"><fmtPartySetupContainer:message key="LBL_BASIC_INFO"/></a></li>
					<logic:equal name="frmPartySetup" property="partyType" value="7003">
					<li onclick = "fnUpdateTabIndex(this);"><a href="/gmPartyShipParamsSetup.do?method=setPartyShipParam&strOpt=fetchParams&screenType=group&accountId=&partyID=<bean:write name="frmPartySetup" property="partyId"/>" rel="#iframe" id="shipdetails" title="shipdetails" > <fmtPartySetupContainer:message key="LBL_SHIP_PARAMETER"/></a></li>
					</logic:equal>
					<logic:notEqual name="frmPartySetup" property="partyId" value="">
		 				<logic:notEqual name="frmPartySetup" property="partyId" value="0">
							<logic:iterate id="tabs" name="frmPartySetup" property="alTabs">									
								<logic:equal name="tabs" property="TABLABEL" value="Upload Section"> 										   	
									<li onclick="fnUpdateTabIndex(this);"><a href="/<bean:write name="tabs" property="ACTION"/>partyId=<bean:write name="frmPartySetup" property="partyId"/>&partyType=<bean:write name="frmPartySetup" property="partyType"/>&strOpt=fetch&includedPage=true" title="<bean:write name="tabs" property="TABLABEL"/>" rel="#iframe" name="<bean:write name="tabs" property="TABLABEL"/>"><bean:write name="tabs" property="TABLABEL"/></a></li>										   		
								</logic:equal>
								<logic:notEqual name="tabs" property="TABLABEL" value="Upload Section">
							   		<li onclick="fnUpdateTabIndex(this);"><a href="/<bean:write name="tabs" property="ACTION"/>partyId=<bean:write name="frmPartySetup" property="partyId"/>&partyType=<bean:write name="frmPartySetup" property="partyType"/>&strOpt=fetch&includedPage=true" title="<bean:write name="tabs" property="TABLABEL"/>" rel="ajaxdivcontentarea" name="<bean:write name="tabs" property="TABLABEL"/>"><bean:write name="tabs" property="TABLABEL"/></a></li>
								</logic:notEqual>
							</logic:iterate>
						</logic:notEqual>
					</logic:notEqual>
				</ul>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<div id="ajaxdivcontentarea" class="contentstyle" style="display:visible;height:600px;overflow:auto;" >
				</div>
			</td>
		</tr>
	</table>

<%@ include file="/common/GmFooter.inc"%>
</body>
</html>