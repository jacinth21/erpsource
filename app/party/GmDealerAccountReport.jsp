
<!-- party\GmDealerAccountReport.jsp -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<%@ taglib prefix="fmtDealerAccountReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<fmtDealerAccountReport:setLocale value="<%=strLocale%>"/>
<fmtDealerAccountReport:setBundle basename="properties.labels.sales.admin.GmDealerAccountReport"/>

<bean:define id="partyType" name="frmDealerSetup" property="partyType" scope="request" type="java.lang.String"></bean:define>
<bean:define id="gridData" name="frmDealerSetup" property="gridData" type="java.lang.String"/> 

<HTML>
<HEAD>
<TITLE> Globus Medical: Dealer Account Report </TITLE>
<% String strPartyJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PARTY");%>


<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">

 

<script language="JavaScript" src="<%=strJsPath %>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath %>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script> 
<script language="JavaScript" src="<%=strPartyJsPath%>/GmDealerInoviceParams.js"></script>
 <%

String strWikiTitle = GmCommonClass.getWikiTitle("DEALER_ACCOUNT_REPORT");


%>
 

  <script>
  var partyType = '<%=partyType%>';
  var objGridData = '<%=gridData%>';

  </script> 

</HEAD>
<BODY leftmargin="20" topmargin="10" onload = "fnOnPageLoadRpt()" >

 <html:form action="/gmDealerSetup.do?method=fetchDealerSetup">
 <html:hidden property="strOpt" name="frmDealerSetup"/>
 <html:hidden property="partyType" styleId="partyType"/>
<table border="0"  height="60" class="DtTable1200" cellspacing="0" cellpadding="0">
		<tr  height="25"><td class="RightDashBoardHeader" colspan="4">&nbsp;<fmtDealerAccountReport:message key="LBL_DEALER_ACCOUNT_REPORT"/></td>
		 
		     <td class="RightDashBoardHeader">
		     <fmtDealerAccountReport:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
					height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');"/>&nbsp;
					</td>
		</tr>
			
					<tr class="oddshade" height="28">
						 <td  class="RightTableCaption"  align="right" width="15%"><fmtDealerAccountReport:message key="LBL_DEALER_LIST"/>:&nbsp;</td>
						 <td class="RightTableCaption" width="50%">   <gmjsp:dropdown controlName="dealerName" SFFormName="frmDealerSetup" SFSeletedValue="dealerName" 
						codeId="ID" codeName="NAME" tabIndex="1" SFValue="alDealerNm" defaultValue="[Choose One]" onChange="fnOnChange();"/>
						 <input type="text" size="10" id="dealerId" styleClass="InputArea"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" onChange="javascript:fnChangeDealer(this);" /> 
						</td>
						<td class="RightTableCaption" align="right" colspan="1"><fmtDealerAccountReport:message key="LBL_STATUS"/>:&nbsp; </td>
						<td class="RightTableCaption">
						
						  <gmjsp:dropdown controlName="statusFl" SFFormName="frmDealerSetup" SFSeletedValue="statusFl" 
						    codeId="CODEID" codeName="CODENM" tabIndex="1" SFValue="alstatus" defaultValue="[Choose One]"/>
					   </td>   					
					   <td class="RightTableCaption" colspan="1">
							 &nbsp;&nbsp;<fmtDealerAccountReport:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;${varLoad}&nbsp;"gmClass="button" buttonType="Load" onClick="fnLoadRpt()" />&nbsp;&nbsp; 
					   </td>	
						
     				</tr>
					
					  <%
				if (gridData.indexOf("cell") != -1) {
			%> 
								

				<tr>
				<td colspan="5"><div  id="dataGridDiv" height="650px"></div></td></tr>
				<tr>						
					<td align="center" colspan="7"><br>
					
	             			<div class='exportlinks'><fmtDealerAccountReport:message key="DIV_EXPORT_OPT"/>: <img src='img/ico_file_excel.png' />&nbsp; <a href="#" onclick="fnExport();"><fmtDealerAccountReport:message key="DIV_EXCEL"/>  </a></div>                         
	       			</td>
       			</tr>  <%} else if (!gridData.equals("")) {
			%>
			<tr>
				<td colspan="9" height="1" class="LLine"></td>
			</tr>
			<tr>
			<tr>
				<td colspan="9" align="center" class="RightText"><fmtDealerAccountReport:message key="LBL_NOTHING_FOUND_TO_DISPLAY"/>
					</td>
			</tr>
			
			 <%}%> 


</table>
</html:form> 
</BODY>
<%@ include file="/common/GmFooter.inc" %>

</HTML>