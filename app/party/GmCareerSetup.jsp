
<%
/**********************************************************************************
 * File		 		: GmCareerSetup.jsp
 * Desc		 		: JSP for setting up the career details for party
 * Version	 		: 1.1
 * Author			: Tarika Chandure
 * Last Updated By	: Satyajit Thadeshwar
 **********************************************************************************/
%>
<!--\party\GmCareerSetup.jsp  -->
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtCareerSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtCareerSetup:setLocale value="<%=strLocale%>"/>
<fmtCareerSetup:setBundle basename="properties.labels.party.GmCareerSetup"/>
<html>
<head>
<title>Career Setup</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<BODY leftmargin="20" topmargin="10">

<html:form action="/gmCareerSetup.do" >
<html:hidden property="partyId"/>
<html:hidden property="hcareerId"/>
<html:hidden property="strOpt"/>

<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VDCAR"/>

<!-- Custom tag lib code modified for JBOSS migration changes -->
<!-- Struts tag lib code modified for JBOSS migration changes -->


<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr height="15"><td colspan="2" ></td></tr> 
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr class="Shade">
					<td class="RightTableCaption" width="35%" height="24" align="right"><font color="red">*</font>&nbsp;<fmtCareerSetup:message key="LBL_TITLE"/> :</td> 
					<td width="65%" align="left">&nbsp;&nbsp;<html:text size="41" property="title" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/></td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr>
					<td class="RightTableCaption" width="30%" height="24" align="right"><font color="red">*</font>&nbsp;<fmtCareerSetup:message key="LBL_INSTITUTE_COMPANY"/> :</td> 
					<td width="65%" align="left">&nbsp;&nbsp;<html:text size="41" property="companyName" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/></td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr class="Shade">
					<td class="RightTableCaption" width="35%" height="24" align="right"><fmtCareerSetup:message key="LBL_INSTITUTE_COMPANY_DESCRIPTION"/> :</td> 
					<td width="65%" align="left">&nbsp;
					<html:textarea property="companyDesc" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" 
					onblur="changeBgColor(this,'#ffffff');" rows="3" cols="40"></html:textarea>
					</td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr>
					<td class="RightTableCaption" width="35%" height="24" align="right"><fmtCareerSetup:message key="LBL_START_DATE"/> :</td> 
					<td width="65%" align="left">
						&nbsp;&nbsp;<fmtCareerSetup:message key="LBL_MONTH"/>:&nbsp;<gmjsp:dropdown controlName="startMonth" SFFormName="frmCareerSetup" SFSeletedValue="startMonth"
						SFValue="alMonths" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
						&nbsp;&nbsp;<fmtCareerSetup:message key="LBL_YEAR"/>Year&nbsp;:&nbsp;<html:text property="startYear" size="4"/></td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr class="Shade">
					<td class="RightTableCaption" width="35%" height="24" align="right"><fmtCareerSetup:message key="LBL_END_DATE"/>:</td> 
					<td width="65%" align="left">
						&nbsp;&nbsp;<fmtCareerSetup:message key="LBL_MONTH"/>:&nbsp;<gmjsp:dropdown controlName="endMonth" SFFormName="frmCareerSetup" SFSeletedValue="endMonth"
						SFValue="alMonths"  codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
						&nbsp;&nbsp;<fmtCareerSetup:message key="LBL_YEAR"/>:&nbsp;<html:text property="endYear" size="4" />
						&nbsp;&nbsp;<html:checkbox property="stillInPosition"/> <fmtCareerSetup:message key="LBL_STILL_IN_THE_POSITION"/></td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr>
					<td class="RightTableCaption" width="35%" height="24" align="right"><font color="red">*</font>&nbsp;<fmtCareerSetup:message key="LBL_GROUP_DIVISION"/> :</td> 
					<td width="65%" align="left">&nbsp;&nbsp;<html:text property="groupDivision" size="41" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/></td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr class="Shade">
					<td class="RightTableCaption" width="35%" height="24" align="right"><font color="red">*</font>&nbsp;<fmtCareerSetup:message key="LBL_COUNTRY"/> :</td> 
					<td width="65%" align="left">
						&nbsp;&nbsp;<gmjsp:dropdown controlName="country" SFFormName="frmCareerSetup" SFSeletedValue="country"
						SFValue="alCountry" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" /></td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr>
					<td class="RightTableCaption" width="35%" height="24" align="right"><font color="red">*</font>&nbsp;<fmtCareerSetup:message key="LBL_STATE"/> :</td> 
					<td width="65%" align="left">
					&nbsp;&nbsp;<gmjsp:dropdown controlName="state" SFFormName="frmCareerSetup" SFSeletedValue="state"
						SFValue="alState" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" /></td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr class="Shade">
					<td class="RightTableCaption" width="35%" height="24" align="right"><fmtCareerSetup:message key="LBL_CITY"/> :</td> 
					<td width="65%" align="left">&nbsp;&nbsp;<html:text property="city" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/></td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr>
					<td class="RightTableCaption" width="35%" height="24" align="right"><fmtCareerSetup:message key="LBL_INDUSTRY"/> :</td> 
					<td width="65%" align="left">&nbsp;
					<gmjsp:dropdown controlName="industry" SFFormName="frmCareerSetup" SFSeletedValue="industry"
						SFValue="alIndustry"  codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
					</td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr><td colspan="2" height="15"></td></tr> 
				<tr> 
					<td colspan="2" align="center" height="24" >
						<fmtCareerSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnCareerSubmit(this.form);"/>&nbsp;&nbsp;
						<fmtCareerSetup:message key="BTN_RESET" var="varReset"/><gmjsp:button value="${varReset}" gmClass="button" buttonType="Save" onClick="fnCareerReset(this.form);" />&nbsp;&nbsp;
						<fmtCareerSetup:message key="BTN_VOID" var="varVoid"/><gmjsp:button value="${varVoid}" gmClass="button" buttonType="Save" onClick="fnCareerVoid(this.form);"/>
					</td>
				</tr>
				<tr><td colspan="2" height="15"></td></tr>
				<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr> 
			</table>
		</td>
	</tr>
    <tr>
    	<td>
    	<!-- Changed for JBOSS migration -->
    		<jsp:include page="/gmCareerSetup.do?method=loadCareerReport" flush="true">
    			<jsp:param name="TYPE" value="Setup" />
    		</jsp:include>
    	</td>
    </tr>
    <tr><td class="Line" height="1"></td></tr> 	
<%-- 
	<tr>
		<td width="100%" height="100" valign="top">
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
			<div><jsp:include page="/party/GmIncludeCareerInfo.jsp"/>
			</div>
		</td>
	</tr>
	
--%>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>
