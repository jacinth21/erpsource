<%
/**********************************************************************************************
 * File		 		: GmIncludeContactInfo.jsp
 * Desc		 		: JSP file to display the contact display tag in setup and summary pages
 * Version	 		: 1.0
 * author			: Satyajit Thadeshwar
/***********************************************************************************************/
%>
<!--\party\GmIncludeContactInfo.jsp  -->
<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtIncludeContactInfo" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtIncludeContactInfo:setLocale value="<%=strLocale%>"/>
<fmtIncludeContactInfo:setBundle basename="properties.labels.party.GmIncludeContactInfo"/>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<% 
	String displayType = request.getParameter("TYPE");
	String strMedia = displayType.equalsIgnoreCase("Setup") ? "HTML" : "FALSE";
	boolean bSortable = displayType.equalsIgnoreCase("Setup") ? true : false;
	bSortable = false;
%>
<display:table name="requestScope.frmContactSetup.lresult" class="gmits" id="currentRowObject"  export = "false" 
	decorator="com.globus.party.displaytag.DTContactInfoWrapper" > 
	<display:column property="ID" title="" class="aligncenter" media="<%=strMedia%>"/>
	<fmtIncludeContactInfo:message key="LBL_CONTACT_TYPE" var="varContactType"/><display:column property="CTYPE" title="${varContactType}" class="alignleft" sortable="<%=bSortable%>" /> 
	<fmtIncludeContactInfo:message key="LBL_CONTACT_MODE" var="varContactMode"/><display:column property="CMODE" title="${varContactMode}" sortable="<%=bSortable%>" class="alignleft"/>
	<fmtIncludeContactInfo:message key="LBL_VALUE" var="varValue"/><display:column property="CVALUE" title="${varValue}" sortable="<%=bSortable%>" class="alignleft" />
	<fmtIncludeContactInfo:message key="LBL_PRIMARY" var="varPrimary"/><display:column property="PRIMARYFL" title="${varPrimary}" sortable="<%=bSortable%>"  class="aligncenter" />	
	<fmtIncludeContactInfo:message key="LBL_STATUS" var="varStatus"/><display:column property="INACTIVEFL" title="${varStatus}" sortable="<%=bSortable%>"  class="aligncenter" />						
</display:table>
<%@ include file="/common/GmFooter.inc" %>