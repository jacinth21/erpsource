<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
/**********************************************************************************
 * File		 		: GmAddressSetup.jsp
 * Desc		 		: Setting up Address for party (For Jira testing tobe removed)
 * Version	 		: 1.0
 * author			: Brinal
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtAddressSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- globusMedApp\party\GmAddressSetup.jsp -->
<fmtAddressSetup:setLocale value="<%=strLocale%>"/>
<fmtAddressSetup:setBundle basename="properties.labels.party.GmAddressSetup"/>
<%
//edit Sales Rep details
String strDisable="";
String streditAccessFlag="";
String strWikiTitle = GmCommonClass.getWikiTitle("ADDRESS_SETUP");
String strPartyJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PARTY");
String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
GmResourceBundleBean gmResourceBundleBean =
GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
 String strDealerNameENFlag = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHOW_DEALER_NAME_EN"));
%>

<bean:define id="activeFlg" name="frmAddressSetup" property="activeFlag" type="java.lang.String"></bean:define>
<bean:define id ="partyType" name="frmAddressSetup" property="partyType" type="java.lang.String"/> 
<bean:define id="editAccessFlag" name="frmAddressSetup" property="editAccessFl" type="java.lang.String"></bean:define>
<%
String strSubmitDisable = "";
if(!editAccessFlag.equals("Y")){
	strSubmitDisable = "true";
}
 %>
 
<HTML>
<HEAD>
<TITLE> Globus Medical: Address Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strPartyJsPath%>/GmParty.js"></script>
<script>
var partyType='<%=partyType%>';

</script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

</HEAD>

<BODY leftmargin="20" topmargin="10" onload="refeshParentPage()">
<html:form action="/gmAddressSetup.do" >
<html:hidden property="strOpt"  name="frmAddressSetup"/>
<html:hidden property="haddressID" name="frmAddressSetup"/>
<html:hidden property="partyId" name="frmAddressSetup"/>
<html:hidden property="includedPage" name="frmAddressSetup" styleId="includedPage"/>
<html:hidden property="transactPage" name="frmAddressSetup"/>
<html:hidden property="addIdReturned" name="frmAddressSetup"/>
<html:hidden property="refID" name="frmAddressSetup"/>
<html:hidden property="repID" name="frmAddressSetup"/>
<html:hidden property="oldAddID" name="frmAddressSetup"/>
<html:hidden property="fromStrOpt" name="frmAddressSetup"/>
<html:hidden property="shipMode" name="frmAddressSetup"/>
<html:hidden property="caseDistID" name="frmAddressSetup"/>
<html:hidden property="caseRepID" name="frmAddressSetup"/>
<html:hidden property="caseAccID" name="frmAddressSetup"/>
<html:hidden property="hActiveFlag" name="frmAddressSetup" value="<%=activeFlg%>"/>
<html:hidden property="hAddrId" name="frmAddressSetup"/>
<html:hidden property="hcount" name="frmAddressSetup"/>
<html:hidden property="partyType" name="frmAddressSetup"/>

<input type="hidden" name="hTxnId"/>
<input type="hidden" name="hTxnName"/>
<input type="hidden" name="hCancelType" value="VDADD"/>
<input type="hidden" name="hAction" />
<input type="hidden" name="hRedirectURL" >
<input type="hidden" name="hDisplayNm" >

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<logic:notEqual name="frmAddressSetup" property="includedPage" value="true">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtAddressSetup:message key="TD_ADDRESS_SETUP"/></td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			<fmtAddressSetup:message key="LBL_HELP" var="varHelp"/>
			<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
		       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		    </td>
		</tr>
		
		
		<tr><td colspan="2" height="0" bgcolor="#666666"></td></tr>
		</logic:notEqual>
		<tr>
			<td width="698" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">		     
                    <tr><td colspan="2" height="15"></td></tr> 
                    <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
                    <tr class="Shade">
                     <%if(partyType.equals("26230725")){ %> 
                       <td width="40%" class="RightTableCaption" align="right" HEIGHT="24" ></font><font color="red">*</font>&nbsp;<fmtAddressSetup:message key="LBL_NAME"/>:</td> 
                        <td>&nbsp;<!-- Custom tag lib code modified for JBOSS migration changes -->
							<html:text property="billname"  size="50" maxlength="225" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
                        </td>
                         <%}else{ %> 
                        <td width="40%" class="RightTableCaption" align="right" HEIGHT="24" ></font><font color="red">*</font>&nbsp;<fmtAddressSetup:message key="LBL_ADDRESS_TYPE"/>:</td> 
                        <td>&nbsp;<!-- Custom tag lib code modified for JBOSS migration changes -->
							<gmjsp:dropdown controlName="addressType" SFFormName="frmAddressSetup" SFSeletedValue="addressType"
								SFValue="alAddressTypes" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
                        </td>
                       <%} %> 
                    </tr>                    
                                     
                    <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
                    <tr>
                        <td width="40%" class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtAddressSetup:message key="LBL_ADDRESS"/> 1:</td>
                        <td>&nbsp; <html:text property="billAdd1"  size="50" maxlength="225" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
                        </td> 
                    </tr>
                    <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
                    <tr class="Shade">
                        <td width="40%" class="RightTableCaption" align="right" HEIGHT="24"><fmtAddressSetup:message key="LBL_ADDRESS"/> 2:</td> 
                        <td>&nbsp; <html:text property="billAdd2"   size="50" maxlength="225" name="frmAddressSetup" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
                        </td> 
                    </tr>
                    <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
                    <tr>
                        <td width="40%" class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtAddressSetup:message key="LBL_CITY"/>:</td>                         
                        <td>&nbsp; <html:text property="billCity"   size="30" maxlength="225" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
                        </td> 
                    </tr>
                    <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
                    <tr class="Shade">
                        <td width="40%" class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtAddressSetup:message key="LBL_STATE"/>:</td> 
                        <td>&nbsp;<!-- Custom tag lib code modified for JBOSS migration changes -->
                			<gmjsp:dropdown controlName="state" SFFormName="frmAddressSetup" SFSeletedValue="state"
								SFValue="alStates" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
				        </td>
                    </tr>
                    <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
                    <tr>
                        <td width="40%" class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtAddressSetup:message key="LBL_ZIP"/>:</td> 
                        <td>&nbsp; <html:text property="zipCode"  size="10" maxlength="10" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
                        </td> 
                    </tr>
                    <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
                    <tr class="Shade">
                        <td width="40%" class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtAddressSetup:message key="LBL_COUNTRY"/>:</td> 
                        <td>&nbsp;<!-- Custom tag lib code modified for JBOSS migration changes -->
	                         <gmjsp:dropdown controlName="country" SFFormName="frmAddressSetup" SFSeletedValue="country"
								SFValue="alCountries" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
                        </td>
                    </tr>
                    <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
                    <%if(!partyType.equals("26230725")){ %> 
                    <tr>
                        <td width="40%" class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtAddressSetup:message key="LBL_PREF_CARRIER"/>:</td> 
                        <td>&nbsp;<!-- Custom tag lib code modified for JBOSS migration changes -->
	                         <gmjsp:dropdown controlName="carrier" SFFormName="frmAddressSetup" SFSeletedValue="carrier"
								SFValue="alCarriers" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
                        </td>
                    </tr>
                    <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
                      <%} %>    
                    <logic:equal name="frmAddressSetup" property="primaryStsFl" value="Y">
                     <%if(!partyType.equals("26230725")){ %>
                    <tr class="Shade">
                        <td width="40%" class="RightTableCaption" align="right" HEIGHT="30">&nbsp;<fmtAddressSetup:message key="LBL_PREFERENCE"/>:</td>
                        <td>&nbsp;
							<html:text property="preference"  size="15" maxlength="4" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
						</td>
                    </tr>  
                     <%} %>    
                    
                    <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
                    <tr>
                        <td width="40%" class="RightTableCaption" align="right" HEIGHT="24"><fmtAddressSetup:message key="LBL_INACTIVE_FLAG"/>:</td> 
                        <td>&nbsp;<html:checkbox property="activeFlag" />
                         </td>
                    </tr>
                    </logic:equal>
                    <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
                    <logic:equal name="frmAddressSetup" property="primaryFlag" value="on">
                    	<tr class="Shade">
                        	<td width="40%" class="RightTableCaption" align="right" HEIGHT="24"><fmtAddressSetup:message key="LBL_PRIMARY_FLAG"/>:</td> 
                        	<td>&nbsp;<html:checkbox property="primaryFlag" disabled="true"/></td>
                    	</tr>
                    </logic:equal>
                    <logic:notEqual name="frmAddressSetup" property="primaryFlag" value="on">
                    <logic:equal name="frmAddressSetup" property="primaryStsFl" value="Y">
                    	<tr class="Shade">
                        	<td width="40%" class="RightTableCaption" align="right" HEIGHT="24"><fmtAddressSetup:message key="LBL_PRIMARY_FLAG"/>:</td> 
                        	<td>&nbsp;<html:checkbox property="primaryFlag" /></td>
                    	</tr>
                    </logic:equal>
                    </logic:notEqual>  
                                 
                    <tr>
                    	<td colspan="2">
                    		<jsp:include page="/common/GmIncludeLog.jsp" >
								<jsp:param name="FORMNAME" value="frmAddressSetup" />
								<jsp:param name="ALNAME" value="alLogReasons" />
								<jsp:param name="LogMode" value="Edit" />
							</jsp:include>
						</td>
                    </tr>
                    <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
                    <tr><td colspan="2" height="15"></td></tr>                    
                    <tr>                        
                        <td colspan="2" align="center" height="30">&nbsp;
                        	<fmtAddressSetup:message key="BTN_SUBMIT" var="varSubmit"/>
                        	<gmjsp:button value="${varSubmit}" buttonType="Save" disabled="<%=strSubmitDisable%>" gmClass="button" onClick="fnAddressSubmit(this.form);" />
                        	<fmtAddressSetup:message key="BTN_ADD_NEW" var="varAddNew"/>
                        	<gmjsp:button value="${varAddNew}" gmClass="button" disabled="<%=strSubmitDisable%>" buttonType="Save" onClick="fnAddressReset(this.form);"/> 
                        	<fmtAddressSetup:message key="BTN_VOID" var="varVoid"/>  
                        	<gmjsp:button value="${varVoid}" gmClass="button" buttonType="Save" disabled="<%=strSubmitDisable%>" onClick="fnAddressVoid(this.form);" /> 
                        	<logic:equal name="frmAddressSetup" property="transactPage" value="areaset">
                        	<fmtAddressSetup:message key="BTN_BACK" var="varBack"/>
                        	<gmjsp:button value="${varBack}" gmClass="button" buttonType="Load" onClick="fnBackToAreaset(this.form);" /> 
                        	</logic:equal>
                        	<logic:equal name="frmAddressSetup" property="transactPage" value="CASE">
                        	<fmtAddressSetup:message key="BTN_CLOSE" var="varClose"/>
                        	<gmjsp:button value="${varClose}" gmClass="button" onClick="fnClose();" buttonType="Load" /> 
                        	</logic:equal>
                        	
                        </td>
                    </tr>
                    <tr><td colspan="2" height="15"></td></tr>
                    <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
                    <tr>
                    	<td colspan="2">
                    	<!-- Changed for JBOSS migration -->
    						<jsp:include page="/gmAddressSetup.do?method=loadAddressReport" flush="true">
    							<jsp:param name="FORMNAME" value="frmAddressSetup" />
    							<jsp:param name="LISTNAME" value="lresult" />
    							<jsp:param name="TYPE" value="Setup" />
    							<jsp:param name="strDealerNameENFlag" value="<%=strDealerNameENFlag%>" />
    							</jsp:include>
						</td>
                    </tr>              
 			  		   
  		  	  </tr>	
      		  </table>
  			  </td>
  		      </tr>	
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

