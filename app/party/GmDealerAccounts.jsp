
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtDealerAccounts" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtDealerAccounts:setLocale value="<%=strLocale%>"/>
<fmtDealerAccounts:setBundle basename="properties.labels.sales.admin.GmDealerAccounts"/>
<bean:define id="dealerID" name="frmDealerSetup" property="dealerID" type="java.lang.String"/> 
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<HTML>
<HEAD>
<TITLE> Globus Medical: Dealer Accounts</TITLE>
<% 
String strPartyJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PARTY");
%>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strPartyJsPath%>/GmDealerInoviceParams.js"></script> 
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />

<script>
var dealerID ='<%=dealerID%>' ;


</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnAccountDetails('<%=dealerID%>');">


	<table class="DtTable900" cellspacing="0" cellpadding="0">		
		<tr  class="Shade">
			<td colspan="2">
				<div id="parentDiv"></div>
			</td>
		</tr>
		
		<tr>
        <td align="center" height="30">&nbsp;
        <fmtDealerAccounts:message key="BTN_CLOSE" var="varClose"/>
			<gmjsp:button value="${varClose}"  name="Btn_Close"  gmClass="Button" buttonType="Load"  onClick="window.close();" />&nbsp;
	    </td>
	</tr>	

</table>

<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
