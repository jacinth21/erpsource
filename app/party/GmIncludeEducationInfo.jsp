<%
/**********************************************************************************************
 * File		 		: GmIncludeEducationInfo.jsp
 * Desc		 		: JSP file to display the education display tag in setup and summary pages
 * Version	 		: 1.0
 * author			: Satyajit Thadeshwar
/***********************************************************************************************/
%>
<!--\party\GmIncludeEducationInfo.jsp  -->
<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtIncludeEducationInfo" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtIncludeEducationInfo:setLocale value="<%=strLocale%>"/>
<fmtIncludeEducationInfo:setBundle basename="properties.labels.party.GmIncludeEducationInfo"/>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<%
	String displayType = GmCommonClass.parseNull(request.getParameter("TYPE"));
	String strMedia = displayType.equals("Setup") ? "HTML" : "FALSE";
	boolean bSortable = displayType.equals("Setup") ? true : false;
	bSortable = false;
%>
<display:table name="requestScope.frmEducationSetup.lresult" class="gmits" id="edutable" decorator="com.globus.party.displaytag.DTEducationInfoWrapper"> 						
		<display:column property="ID" title=""  class="aligncenter" media="<%=strMedia%>"/>
 		<fmtIncludeEducationInfo:message key="LBL_LEVEL" var="varLevel"/><display:column property="EDUCATIONLEVEL" title="${varLevel}" class="alignleft"/> 
		<fmtIncludeEducationInfo:message key="LBL_INSTITUTE_NAME" var="varInstituteName"/><display:column property="INSTITUTENAME" title="${varInstituteName}" class="alignleft" sortable="<%=bSortable%>"  />
		<fmtIncludeEducationInfo:message key="LBL_CLASS_YEAR" var="varClassYear"/><display:column property="CLASSYEAR" title="${varClassYear}" sortable="<%=bSortable%>" class="alignleft" />
		<fmtIncludeEducationInfo:message key="LBL_COURSE" var="varCourse"/><display:column property="COURSE" title="${varCourse}" sortable="<%=bSortable%>" class="alignleft" />
		<fmtIncludeEducationInfo:message key="LBL_DESIGNATION" var="varDesignation"/><display:column property="DESIGNATION" title="${varDesignation}" sortable="<%=bSortable%>" class="alignleft" />					
</display:table>