<%@ include file="/common/GmHeader.inc" %>
<!-- party\GmPartySummary.jsp -->
<%@ taglib prefix="fmtPartySummary" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- BUG-12191 -->
<% 
String strPartyJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PARTY");
%>
<fmtPartySummary:setLocale value="<%=strLocale%>"/>
<fmtPartySummary:setBundle basename="properties.labels.party.GmPartySummary"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Globus Medical: Party Summary</title>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="JavaScript" src="<%=strPartyJsPath%>/GmParty.js"></script>

<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
</head>
<body leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<table class="DtTable700" border="1" cellspacing="10" cellpadding="10">
	<tr>
		<td width="50%">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2">
						<div id="basicinfo">
							<a href="/gmBasicInfoSummary.do?partyId=<bean:write name="frmPartySetup" property="partyId"/>&partyType=<bean:write name="frmPartySetup" property="partyType"/>" rel="basicinfocontentarea" title="Basic Info"></a>
   							<div id="basicinfocontentarea"></div>
  						</div>			
					</td>
				</tr>				
			</table>			
		</td>
		<td width="50%" rowspan="2" valign="top">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2" >
						<div id="aboutme">
							<a href="/gmAboutMeSummary.do?partyId=<bean:write name="frmPartySetup" property="partyId"/>&partyType=<bean:write name="frmPartySetup" property="partyType"/>" title="AboutMe" rel="aboutmecontentarea"></a>
							<div id="aboutmecontentarea"></div>
  						</div>				
					</td>
				</tr>				
			</table>			
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2">
						<div id="globusinfo">
							<a href="/gmGlobusInfoSummary.do?partyId=<bean:write name="frmPartySetup" property="partyId"/>&partyType=<bean:write name="frmPartySetup" property="partyType"/>" title="GlobusInfo" rel="globusinfocontentarea"></a>
							<div id="globusinfocontentarea"></div>
						</div>			
					</td>
				</tr>				
			</table>			
		</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">
			<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
				<tr class="TableHeaderRow">
					<td align="left">&nbsp;<fmtPartySummary:message key="LBL_EDUCATION"/></td>
					<td align="right">
						<a href="/gmPartySetup.do?partyId=<bean:write name="frmPartySetup" property="partyId"/>&partyType=<bean:write name="frmPartySetup" property="partyType"/>"><fmtPartySummary:message key="LBL_EDIT"/></a>&nbsp;
					</td>					
				</tr>
				<tr>
					<td colspan="2">
						<div id="education">
							<a href="/gmEducationSetup.do?method=loadEducationReport&TYPE=Report&partyId=<bean:write name="frmPartySetup" property="partyId"/>&partyType=<bean:write name="frmPartySetup" property="partyType"/>" title="Education" rel="educationcontentarea"></a>
							<div id="educationcontentarea"></div>
  						</div>	
					</td>
				</tr>				
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
				<tr class="TableHeaderRow">
					<td align="left">&nbsp;<fmtPartySummary:message key="LBL_PROFESSIONAL_EXPERIENCE"/></td>
					<td align="right">
						<a href="/gmPartySetup.do?partyId=<bean:write name="frmPartySetup" property="partyId"/>&partyType=<bean:write name="frmPartySetup" property="partyType"/>"><fmtPartySummary:message key="LBL_EDIT"/></a>&nbsp;
					</td>					
				</tr>
				<tr>
					<td colspan="2">
						<div id="career">
							<a href="/gmCareerSetup.do?method=loadCareerReport&TYPE=Report&partyId=<bean:write name="frmPartySetup" property="partyId"/>&partyType=<bean:write name="frmPartySetup" property="partyType"/>" title="Career" rel="careercontentarea"></a>
							<div id="careercontentarea"></div>
  						</div>	
					</td>
				</tr>				
			</table>			
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
				<tr class="TableHeaderRow">
					<td align="left">&nbsp;<fmtPartySummary:message key="LBL_AFFILIATIONS"/></td>
					<td align="right">
						<a href="/gmPartySetup.do?partyId=<bean:write name="frmPartySetup" property="partyId"/>&partyType=<bean:write name="frmPartySetup" property="partyType"/>"><fmtPartySummary:message key="LBL_EDIT"/></a>&nbsp;
					</td>					
				</tr>
				<tr>
					<td colspan="2">
						<div id="affiliations">
							<a href="/gmAffiliationSetup.do?method=loadAffiliationReport&TYPE=Report&partyId=<bean:write name="frmPartySetup" property="partyId"/>&partyType=<bean:write name="frmPartySetup" property="partyType"/>" title="Affiliations" rel="affiliationscontentarea"></a>
							<div id="affiliationscontentarea"></div>
  						</div>	
					</td>
				</tr>				
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
				<tr class="TableHeaderRow">
					<td align="left">&nbsp;<fmtPartySummary:message key="LBL_PUBLICATION"/></td>
					<td align="right">
						<a href="/gmPartySetup.do?partyId=<bean:write name="frmPartySetup" property="partyId"/>&partyType=<bean:write name="frmPartySetup" property="partyType"/>"><fmtPartySummary:message key="LBL_EDIT"/></a>&nbsp;
					</td>					
				</tr>
				<tr>
					<td colspan="2">
						<div id="publications">
							<a href="/gmPublicationSetup.do?method=loadPublicationReport&TYPE=Report&partyId=<bean:write name="frmPartySetup" property="partyId"/>&partyType=<bean:write name="frmPartySetup" property="partyType"/>" title="Publications" rel="publicationscontentarea"></a>
							<div id="publicationscontentarea"></div>
						</div>
					</td>
				</tr>				
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
				<tr class="TableHeaderRow">
					<td align="left">&nbsp;<fmtPartySummary:message key="LBL_LICENSES"/></td>
					<td align="right">
						<a href="/gmPartySetup.do?partyId=<bean:write name="frmPartySetup" property="partyId"/>&partyType=<bean:write name="frmPartySetup" property="partyType"/>"><fmtPartySummary:message key="LBL_EDIT"/></a>&nbsp;
					</td>					
				</tr>
				<tr>
					<td colspan="2">
						<div id="licenses">
							<a href="/gmLicenseSetup.do?method=loadLicenseReport&TYPE=Report&partyId=<bean:write name="frmPartySetup" property="partyId"/>&partyType=<bean:write name="frmPartySetup" property="partyType"/>" title="Licenses" rel="licensescontentarea"></a>
							<div id="licensescontentarea"></div>
  						</div>
					</td>
				</tr>				
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
				<tr class="TableHeaderRow">
					<td align="left">&nbsp;<fmtPartySummary:message key="LBL_AWARDS_GRANTS_AND_PATENTS"/></td>
					<td align="right">
						<a href="/gmPartySetup.do?partyId=<bean:write name="frmPartySetup" property="partyId"/>&partyType=<bean:write name="frmPartySetup" property="partyType"/>"><fmtPartySummary:message key="LBL_EDIT"/></a>&nbsp;
					</td>					
				</tr>
				<tr>
					<td colspan="2">
						<div id="achievements">
							<a href="/gmAchievementSetup.do?method=loadAchievementReport&TYPE=Report&partyId=<bean:write name="frmPartySetup" property="partyId"/>&partyType=<bean:write name="frmPartySetup" property="partyType"/>" title="Achievements" rel="achievementscontentarea"></a>
							<div id="achievementscontentarea"></div>
  						</div>
					</td>
				</tr>				
			</table>			
		</td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>
</table>
<br/>
<table style="width: 700px; border: 0px solid  #676767;" border="0" cellspacing="10" cellpadding="10">
	<tr>
		<td colspan="2" align="center">
			<fmtPartySummary:message key="BTN_PRINT" var="varPrint"/><gmjsp:button value="&nbsp;${varPrint}" name="btnPrint" gmClass="button" buttonType="Load" onClick="fnPrint();" />
		</td>
	</tr>		
</table>
	<script type="text/javascript">				    
		var basicinfotab=new ddajaxtabs("basicinfo", "basicinfocontentarea");				
		basicinfotab.setpersist(false);								
		basicinfotab.init();					    
		
		var aboutmetab=new ddajaxtabs("aboutme", "aboutmecontentarea");				
		aboutmetab.setpersist(false);								
		aboutmetab.init();

		var globusinfotab=new ddajaxtabs("globusinfo", "globusinfocontentarea");				
		globusinfotab.setpersist(false);								
		globusinfotab.init();
													    
		var educationtab=new ddajaxtabs("education", "educationcontentarea");				
		educationtab.setpersist(false);								
		educationtab.init();

		var careertab=new ddajaxtabs("career", "careercontentarea");				
		careertab.setpersist(false);								
		careertab.init();

		var affiliationstab=new ddajaxtabs("affiliations", "affiliationscontentarea");				
		affiliationstab.setpersist(false);								
		affiliationstab.init();	

		var publicationstab=new ddajaxtabs("publications", "publicationscontentarea");				
		publicationstab.setpersist(false);								
		publicationstab.init();	

		var licensestab=new ddajaxtabs("licenses", "licensescontentarea");				
		licensestab.setpersist(false);								
		licensestab.init();	
		
		var licensestab=new ddajaxtabs("licenses", "licensescontentarea");				
		licensestab.setpersist(false);								
		licensestab.init();
		
		var achievementstab=new ddajaxtabs("achievements", "achievementscontentarea");				
		achievementstab.setpersist(false);								
		achievementstab.init();
	</script>
	<%@ include file="/common/GmFooter.inc" %>
</body>
</html>