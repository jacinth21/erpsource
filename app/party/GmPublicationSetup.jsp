
<%
/**********************************************************************************
 * File		 		: GmPublicationSetup.jsp
 * Desc		 		: This screen is used for Publication Setup
 * Version	 		: 1.1
 * Author			: Xun
 * Last Modified By	: Satyajit Thadeshwar
 ************************************************************************************/
%>
<!-- party\GmPublicationSetup.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java"%>

<%@ taglib prefix="fmtPublicationSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPublicationSetup:setLocale value="<%=strLocale%>"/>
<fmtPublicationSetup:setBundle basename="properties.labels.party.GmPublicationSetup"/>

<html>
<head>
<title>Globus Medical: Publication Setup </title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
</head>

<body leftmargin="20" topmargin="10">
<html:form action="/gmPublicationSetup.do">
<html:hidden property="strOpt"/>
<html:hidden property="partyId"/>
<html:hidden property="hpublicationID"  />

<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VDPUB"/>

<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr><td colspan="3" height="15"></td></tr>
		<tr><td colspan="3" class="LLine" height="1"></td></tr>
		<tr class="Shade" height="24">
			<td width="30%" class="RightTableCaption" align="right"><font color="red">*</font>&nbsp;<fmtPublicationSetup:message key="LBL_TYPE"/>:&nbsp;</td>
			<td width="40%"><gmjsp:dropdown controlName="typeID" SFFormName="frmPublicationSetup" SFSeletedValue="typeID"
				SFValue="alTypeList" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" /></td>
			<td width="30%"class="RightTableCaption"> </td>
		</tr>
		<tr><td colspan="3" class="LLine" height="1"></td></tr>
		<tr height="24">
			<td width="30%" align="right" class="RightTableCaption"><font color="red">*</font>&nbsp;<fmtPublicationSetup:message key="LBL_TITLE"/>:&nbsp;</td>
			<td width="40%" ><html:text property="title"  size="56" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
    		             </td>
			<td width="30%"> </td>
		</tr>
		<tr><td colspan="3" class="LLine" height="1"></td></tr>
		<tr height="24" class="Shade">
			<td width="30%" align="right" class="RightTableCaption">&nbsp;&nbsp;&nbsp;&nbsp;<fmtPublicationSetup:message key="LBL_FULL_REFERENCE"/>:&nbsp;</td>
			<td width="40%" valign="center" height="50" >
				<html:textarea property="reference" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
				onblur="changeBgColor(this,'#ffffff');" rows="3" cols="55"></html:textarea></td> 
			<td width="30%" ></td>
		</tr>
		<tr><td colspan="3" class="LLine" height="1"></td></tr>
		<tr height="24">
			<td width="30%" align="right" class="RightTableCaption">&nbsp;&nbsp;&nbsp;&nbsp;<fmtPublicationSetup:message key="LBL_ABSTRACT"/>:&nbsp;</td>
			<td width="40%" valign="center" height="50" >
				<html:textarea property="pabstract" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
				onblur="changeBgColor(this,'#ffffff');" rows="3" cols="55"></html:textarea></td> 
			<td width="30%" ></td>
		</tr>
		<tr><td colspan="3" class="LLine" height="1"></td></tr>
		<tr height="24" class="Shade">
			<td width="30%" align="right" class="RightTableCaption"><fmtPublicationSetup:message key="LBL_PUBLICATION_DATE"/> :&nbsp;</td>
			<td width="40%" class="RightTableCaption">&nbsp;<fmtPublicationSetup:message key="LBL_MONTH"/>:&nbsp;&nbsp;<gmjsp:dropdown controlName="monthID" SFFormName="frmPublicationSetup" SFSeletedValue="monthID"
				SFValue="alMonthList" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" />
				&nbsp;&nbsp;<fmtPublicationSetup:message key="LBL_YEAR"/>:
			<html:text property="pyear" maxlength= "4" size="4" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
				</td>
			<td width="30%" >&nbsp; </td>
		</tr>
		<tr><td colspan="3" class="LLine" height="1"></td></tr>
		<tr height="24">
			<td width="30%" align="right" class="RightTableCaption"><font color="red">*</font>&nbsp;<fmtPublicationSetup:message key="LBL_AUTHORS"/>&nbsp;:&nbsp;</td>
			<td width="40%" height="50">
				<html:textarea property="author" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" 
				onblur="changeBgColor(this,'#ffffff');" rows="3" cols="55"></html:textarea></td> 
			<td width="30%">&nbsp;<fmtPublicationSetup:message key="LBL_SEPARATE_BY_SEMICOLON"/></td>
		</tr>
		<tr><td colspan="3" class="LLine" height="1"></td></tr>
		<tr height="24" class="Shade">
			<td width="30%" align="right" class="RightTableCaption"><fmtPublicationSetup:message key="LBL_REFERENCE_URL"/> :&nbsp;</td>
			<td width="40%" ><html:text property="refURL"  size="56" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/></td>
			<td width="30%"> </td>
		</tr>
		<tr><td colspan="3" class="LLine" height="1"></td></tr>
		<tr><td colspan="3" height="15"></td></tr> 
        <tr height="24">
           	<td colspan="3" align="center">
	            <fmtPublicationSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnPublicationSubmit(this.form);" /> 
	           <fmtPublicationSetup:message key="BTN_RESET" var="varReset"/><gmjsp:button value="${varReset}" gmClass="button" buttonType="Save" onClick="fnPublicationReset(this.form);" /> 
	            <fmtPublicationSetup:message key="BTN_VOID" var="varVoid"/><gmjsp:button value="${varVoid}" gmClass="button" buttonType="Save" onClick="fnPublicationVoid(this.form);" />
            </td>
       	</tr>
       	<tr><td colspan="3" height="15"></td></tr> 
		<tr><td colspan="3" class="LLine" height="1"></td></tr>
    	<tr height="24">
    		<td colspan="3">
    		<!-- Changed for JBOSS migration -->
    			<jsp:include page="/gmPublicationSetup.do?method=loadPublicationReport" flush="true">
    				<jsp:param name="TYPE" value="Setup" />
    			</jsp:include>
    		</td>
    	</tr> 
		<tr><td colspan="3" class="LLine" height="1"></td></tr>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>
