<%
	/**********************************************************************************
	 * File		 		: GmIncludeAffiliationInfo.jsp
	 * Desc		 		: Affiliation - include page
	 * Version	 		: 1.0
	 * author			: Angela Xiang
	 ************************************************************************************/
%>
<!-- \party\GmIncludeAffiliationInfo.jsp -->
<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtIncludeAffiliationInfo" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>


<fmtIncludeAffiliationInfo:setLocale value="<%=strLocale%>"/>
<fmtIncludeAffiliationInfo:setBundle basename="properties.labels.party.GmIncludeAffiliationInfo"/>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<%@ page import="com.globus.party.forms.GmAffiliationSetupForm"%>
<% 
GmAffiliationSetupForm form = (GmAffiliationSetupForm)pageContext.getAttribute("frmAffiliationSetup", PageContext.REQUEST_SCOPE);
String partyId = form.getPartyId();
%>
<% 		

	String displayType = GmCommonClass.parseNull(request.getParameter("TYPE"));
	String strMedia = displayType.equals("Setup") ? "HTML" : "FALSE";
	boolean bSortable = displayType.equalsIgnoreCase("Setup") ? true : false;
	bSortable = false;
%>
<display:table name="requestScope.frmAffiliationSetup.lresult"  class="gmits"  id="affiliationtable" 
decorator="com.globus.party.displaytag.DTAffiliationDetailWrapper" >
	<display:column property="AFFID" title="" class="aligncenter" media="<%=strMedia%>"/>
	<fmtIncludeAffiliationInfo:message key="LBL_ORGANISATION" var="varOrganization"/><display:column property="AFFNAME" title="${varOrganization}" class="alignleft" sortable="<%=bSortable%>"/>
	<fmtIncludeAffiliationInfo:message key="LBL_TYPE" var="varType"/><display:column property="AFFTYPE" title="${varType}" class="alignleft" sortable="<%=bSortable%>"/>
	<fmtIncludeAffiliationInfo:message key="LBL_COMMITTEE" var="varCommittee"/><display:column property="COMMITTEENAME" title="${varCommittee}" class="alignleft" sortable="<%=bSortable%>"/>
	<fmtIncludeAffiliationInfo:message key="LBL_ROLE" var="varRole"/><display:column property="ROLE" title="${varRole}" class="alignleft" sortable="<%=bSortable%>"/>
	<fmtIncludeAffiliationInfo:message key="LBL_START_YEAR" var="varStartYear"/><display:column property="STARTYEAR" title="${varStartYear}Start Year" class="alignleft" sortable="<%=bSortable%>" />
	<fmtIncludeAffiliationInfo:message key="LBL_END_YEAR" var="varEndYear"/><display:column property="ENDYEAR" title="${varEndYear}" class="alignleft" sortable="<%=bSortable%>" />
</display:table>
<% 
	String moreAffiliations = GmCommonClass.parseNull((String)request.getAttribute("MORE_AFFILIATIONS"));
	if("Report".equals(displayType) && moreAffiliations.equals("true")) { %>
	<div align="right">
	<a href="javascript:windowOpener('gmAffiliationSetup.do?method=loadAffiliationReport&TYPE=&partyId=<%=partyId%>', 'Affiliations', 'resizable=yes,scrollbars=yes,top=40,left=50,width=600,height=300,align=center');">More</a>&nbsp;
	</div>
<% } %>		