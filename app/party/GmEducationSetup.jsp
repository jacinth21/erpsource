<%
/**********************************************************************************************
 * File		 		: GmEducationSetup.jsp
 * Desc		 		: JSP file to display the education display tag in setup and summary pages
 * Version	 		: 1.0
 * author			: Satyajit Thadeshwar
/***********************************************************************************************/
%>
<!--\party\GmEducationSetup.jsp  -->
<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtEducationSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtEducationSetup:setLocale value="<%=strLocale%>"/>
<fmtEducationSetup:setBundle basename="properties.labels.party.GmEducationSetup"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Insert title here</title>
</head>
<body leftmargin="20" topmargin="10" >
<html:form action="/gmEducationSetup.do">

<html:hidden property="strOpt"/>
<html:hidden property="partyId"/>
<html:hidden property="heducationId" name="frmEducationSetup"/>

<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VDEDU"/>

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr><td colspan="2" height="15"></td></tr>		
		<tr><td colspan="2" class="LLine" height="1"></td></tr>	
		<tr class="Shade">
			<td class="RightTableCaption" align="right" height="24" width="30%"><font color="red">*</font>&nbsp;<fmtEducationSetup:message key="LBL_LEVEL"/>:</td>
			<td width="70%">&nbsp;<!-- Custom tag lib code modified for JBOSS migration changes -->
				<gmjsp:dropdown controlName="educationLevel" SFFormName="frmEducationSetup" SFSeletedValue="educationLevel"
				SFValue="alLevel" codeId="CODEID" codeName="CODENM" onChange="fnEducationFetch(this.form);" defaultValue="[Choose One]"/>
			</td>
		</tr>		
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr>
			<td class="RightTableCaption" align="right" height="24" width="30%"><font color="red">*</font>&nbsp;<fmtEducationSetup:message key="LBL_INSTITUTE"/>:</td>
			<td width="70%" colspan="3">&nbsp;
				<html:text property="instituteName" size="60" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" />
			</td>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr class="Shade">
			<td class="RightTableCaption" align="right" height="24" width="30%"><font color="red">*</font>&nbsp;<fmtEducationSetup:message key="LBL_CLASS_YEAR"/>:</td>
			<td width="70%">&nbsp;
				<html:text property="classYear" size="4" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" />
				&nbsp;&nbsp;(For example, 1983)
			</td>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr >
			<td class="RightTableCaption" align="right" height="24" width="30%"><font color="red">*</font>&nbsp;<bean:write name="frmEducationSetup" property="courseLabel"/>&nbsp;:</td>
			<td width="70%">&nbsp;
				<html:text property="course" size="60" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" />
			</td>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr class="Shade">
			<td class="RightTableCaption" align="right" height="24" width="30%"><font color="red">*</font>&nbsp;<fmtEducationSetup:message key="LBL_DESIGNATION"/>:</td>
			<td width="70%">&nbsp;<!-- Custom tag lib code modified for JBOSS migration changes -->
				<gmjsp:dropdown controlName="designation" SFFormName="frmEducationSetup" SFSeletedValue="designation"
				SFValue="alDesignation" codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]"/>				
			</td>			
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr><td colspan="3" height="15"></td></tr>
		<tr>
			<td colspan="2" align="center" height="30">
				<fmtEducationSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button name="submitBtn" value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnEducationSubmit(this.form);"/>&nbsp;&nbsp;
				<fmtEducationSetup:message key="BTN_RESET" var="varReset"/><gmjsp:button name="resetBtn" value="${varReset}" gmClass="button" buttonType="Save" onClick="fnEducationReset(this.form);"/>&nbsp;&nbsp;
				<fmtEducationSetup:message key="BTN_VOID" var="varVoid"/><gmjsp:button name="voidBtn" value="${varVoid}" gmClass="button" buttonType="Save" onClick="fnEducationVoid(this.form);"/>&nbsp;

			</td>
		</tr>
		<tr><td colspan="3" height="15"></td></tr>	
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
    	<tr> <td colspan="2">
    	<!-- Changed for JBOSS migration -->    	
    		<jsp:include page="/gmEducationSetup.do?method=loadEducationReport" flush="true">    		    
    			<jsp:param name="TYPE" value="Setup"/>
    		</jsp:include>
    		</td>
    	</tr>    	    					
	</table>
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</body>
</html>
