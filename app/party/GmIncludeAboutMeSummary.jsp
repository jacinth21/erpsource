<!-- party\GmIncludeAboutMeSummary.jsp -->
<%@ include file="/common/GmHeader.inc" %>


<%@ taglib prefix="fmtIncludeAboutMeSummary" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtIncludeAboutMeSummary:setLocale value="<%=strLocale%>"/>
<fmtIncludeAboutMeSummary:setBundle basename="properties.labels.party.GmIncludeAboutMeSummary"/>
<bean:define id="strHobbies" name="frmAboutMeSummary" property="hobbies" type="java.lang.String"></bean:define>
<bean:define id="strSpecialty" name="frmAboutMeSummary" property="speciality" type="java.lang.String"></bean:define>
<bean:define id="strProfessionalInterests" name="frmAboutMeSummary" property="professionalInterests" type="java.lang.String"></bean:define>
<bean:define id="strFileId" name="frmAboutMeSummary" property="fileId" type="java.lang.String"></bean:define>
<html>
<head>
<title>About Me Section</title>

<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>

</head>
<body>
<table class="DtSummaryTable" cellspacing="0" cellpadding="0">
	<tr class="TableHeaderRow">
		<td align="left">&nbsp;<fmtIncludeAboutMeSummary:message key="LBL_PERSONAL_DETAILS"/></td>
		<td align="right">
			<a href="/gmPartySetup.do?partyId=<bean:write name="frmAboutMeSummary" property="partyId"/>&partyType=<bean:write name="frmAboutMeSummary" property="partyType"/>"></a>&nbsp;
		</td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>	
	<tr height="20">
		<td align="right" width="45%" class="RightTableCaption"><fmtIncludeAboutMeSummary:message key="LBL_GENDER"/>:&nbsp;</td>
		<td align="left" width="55%"><bean:write name="frmAboutMeSummary" property="gender"/></td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>
	<tr height="20" class="Shade">
		<td align="right" width="45%" class="RightTableCaption"><fmtIncludeAboutMeSummary:message key="LBL_DATE_OF_BIRTH"/>:&nbsp;</td>
		<td align="left" width="55%"><bean:write name="frmAboutMeSummary" property="dateOfBirth"/></td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>
	<tr height="20">
		<td align="right" width="45%" class="RightTableCaption"><fmtIncludeAboutMeSummary:message key="LBL_HOMETOWN"/>:&nbsp;</td>
		<td align="left" width="55%"><bean:write name="frmAboutMeSummary" property="hometown"/></td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>
	<tr height="20" class="Shade">
		<td align="right" width="45%" class="RightTableCaption"><fmtIncludeAboutMeSummary:message key="LBL_PERSONAL_WEBSITE"/>:&nbsp;</td>
		<td align="left" width="55%"><a href="http://<bean:write name="frmAboutMeSummary" property="personalWebsite"/>" target="_blank"><bean:write name="frmAboutMeSummary" property="personalWebsite"/></a></td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>
	<tr height="20">
		<td align="right" width="45%" class="RightTableCaption"><fmtIncludeAboutMeSummary:message key="LBL_RELATIONSHIP_STATUS"/>:&nbsp;</td>
		<td align="left" width="55%"><bean:write name="frmAboutMeSummary" property="relationshipStatus"/></td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>
	<tr height="20" class="Shade">
		<td align="right" width="45%" class="RightTableCaption"><fmtIncludeAboutMeSummary:message key="LBL_SPOUSE_DETAILS"/>:&nbsp;</td>
		<td align="left" width="55%"><bean:write name="frmAboutMeSummary" property="spouseDetails"/></td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>
	<tr height="20">
		<td align="right" width="45%" class="RightTableCaption"><fmtIncludeAboutMeSummary:message key="LBL_CHILDREN"/>:&nbsp;</td>
		<td align="left" width="55%"><bean:write name="frmAboutMeSummary" property="children"/></td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>
	<tr height="20" class="Shade">
		<td align="right" width="45%" class="RightTableCaption"><fmtIncludeAboutMeSummary:message key="LBL_CHILDREN_DETAILS"/>:&nbsp;</td>
		<td align="left" width="55%"><bean:write name="frmAboutMeSummary" property="childrenDetails"/></td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>
	<tr height="40">
		<td align="right" width="45%" class="RightTableCaption"><fmtIncludeAboutMeSummary:message key="LBL_HOBBIES"/>:&nbsp;</td>
		<td align="left" width="55%"><%=strHobbies%></td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>
	<tr height="20" class="Shade">
		<td colspan="2">
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>
	<tr height="40">
		<td align="right" width="45%" class="RightTableCaption"><fmtIncludeAboutMeSummary:message key="LBL_SPECIALTY"/>:&nbsp;</td>
		<td align="left" width="55%"><%=strSpecialty%></td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>
	<tr height="40" class="Shade">
		<td align="right" width="45%" class="RightTableCaption"><fmtIncludeAboutMeSummary:message key="LBL_INTEREST"/>:&nbsp;</td>
		<td align="left" width="55%"><font color="blue"><%=strProfessionalInterests%></font></td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>
	<tr height="20">
		<td align="right" width="45%" class="RightTableCaption"><fmtIncludeAboutMeSummary:message key="LBL_CV"/>:&nbsp;</td>
		<td align="left" width="55%">
		<logic:notEqual name="frmAboutMeSummary" property="fileId" value="">
			<font color="blue"><a href="javascript:fnOpenFile(<%=strFileId%>);"><fmtIncludeAboutMeSummary:message key="LBL_VIEW"/></a></font>
		</logic:notEqual>
		</td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>
</table>
</body>
</html>