<%
	/**********************************************************************************
	 * File		 		: GmIncludeAddressInfo.jsp
	 * Desc		 		: Address - include page
	 * Version	 		: 1.0
	 * Author			: Brinal G
	 * Last Modified By	: Satyajit Thadeshwar
	 ************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtAddressSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false"%>

<fmtAddressSetup:setLocale value="<%=strLocale%>"/>
<fmtAddressSetup:setBundle basename="properties.labels.party.GmAddressSetup"/>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<% 

	String displayType = request.getParameter("TYPE");
	String strMedia = displayType.equalsIgnoreCase("Setup") ? "HTML" : "FALSE";
	boolean bSortable = displayType.equalsIgnoreCase("Setup") ? true : false;
	bSortable = false;
	 String strDealerNameENFlag = request.getParameter("strDealerNameENFlag");
%>
<%@ page language="java"%>
<meta http-equiv="Content-Type" content="text/html; charset=UFT-8">
<fmtAddressSetup:message key="DT_ADDR_TYPE" var="varAddrType"/>
<fmtAddressSetup:message key="DT_BILL_ADD" var="varBillAdd"/>
<fmtAddressSetup:message key="DT_BILL_CITY" var="varBillCity"/>
<fmtAddressSetup:message key="DT_STATE" var="varState"/>
<fmtAddressSetup:message key="DT_COUNTRY" var="varCountry"/>
<fmtAddressSetup:message key="DT_PREF_CARRIER" var="varPrefCarrier"/>
<fmtAddressSetup:message key="DT_ZIP_CODE" var="varZipCode"/>
<fmtAddressSetup:message key="DT_STATUS" var="varStatus"/>
<fmtAddressSetup:message key="DT_PRIMARY" var="varPrimary"/>
<display:table name="requestScope.frmAddressSetup.lresult" class="gmits" id="currentRowObject" 
	export = "false" decorator="com.globus.party.displaytag.DTAddressInfoWrapper" requestURI="gmAddressSetup.do"> 
	<display:column property="ID" title=""  class="alignleft" media="<%=strMedia%>"/>
	<%if(!strDealerNameENFlag.equals("YES")){%>
	<display:column property="ADDRESSTYPE" title="${varAddrType}" class="aligncenter"/> 
	<%} %>
	<display:column property="BILLADD" title="${varBillAdd}" class="alignleft" sortable="<%=bSortable%>"  />							
	<display:column property="BILLCITY" title="${varBillCity}" class="alignleft" sortable="<%=bSortable%>"  />
	<display:column property="STATE" title="${varState}" class="alignleft" sortable="<%=bSortable%>"  />
	<display:column property="COUNTRY" title="${varCountry}" class="alignleft" sortable="<%=bSortable%>"  />
	<%if(!strDealerNameENFlag.equals("YES")){%>
	<display:column property="PREFCARRIER" title="${varPrefCarrier}" class="alignleft" sortable="<%=bSortable%>"  />
	<%}%>
	<display:column property="ZIPCODE" title="${varZipCode}" class="alignright" sortable="<%=bSortable%>"  />
	<display:column property="INACTIVEFL" title="${varStatus}" class="aligncenter" sortable="<%=bSortable%>"  />
	<display:column property="PRIMARYFL" title="${varPrimary}" class="aligncenter" sortable="<%=bSortable%>"  />							
</display:table>  
    
<%@ include file="/common/GmFooter.inc" %>		    
