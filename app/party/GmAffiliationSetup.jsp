<!-- \party\GmAffiliationSetup.jsp -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtAffiliationSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtAffiliationSetup:setLocale value="<%=strLocale%>"/>
<fmtAffiliationSetup:setBundle basename="properties.labels.party.GmAffiliationSetup"/>
<html>
<head>
<title> Globus Medical: Affiliation Setup </title>
</head>
<body leftmargin="20" topmargin="10" >
 
<html:form action="/gmAffiliationSetup.do">
<html:hidden property="partyId"/>
<html:hidden property="affId"/>
<html:hidden property="strOpt"/>

<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VDAFF"/>

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr><td colspan="2" height="15"></td></tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>	
		<tr height="24" class="Shade"> 
			<td width ="30%" class="RightTableCaption" align="right"><font color="red">*</font>&nbsp;<fmtAffiliationSetup:message key="LBL_ORGANISATION"/>:&nbsp;</td>
			<td width ="70%">&nbsp;<html:text property="organization"  size="60" onfocus="changeBgColor(this,'#AACCE8');" 
				styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>          		
	      	</td>
      	</tr>
    	<tr><td colspan="2" class="LLine" height="1"></td></tr>
	    <tr height="24">
			<td class="RightTableCaption" align="right"><font color="red">*</font>&nbsp;<fmtAffiliationSetup:message key="LBL_TYPE"/>:&nbsp;</td> 		
			<td width ="70%"><!-- Custom tag lib code modified for JBOSS migration changes -->
				&nbsp;<gmjsp:dropdown controlName="affType" SFFormName="frmAffiliationSetup" SFSeletedValue="affType"
				SFValue="alTypes" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" />    
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>	
		<tr height="24" class="Shade"> 
			<td width ="30%" class="RightTableCaption" align="right"><fmtAffiliationSetup:message key="LBL_COMMITTEE_NAME"/>:&nbsp;</td>
			<td width ="70%">&nbsp;<html:text property="committeeName"  size="60" onfocus="changeBgColor(this,'#AACCE8');" 
				styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>          		
	      	</td>
      	</tr>		
		<tr><td colspan="2" class="LLine" height="1"></td></tr>	
		<tr height="24" class="Shade"> 
			<td width ="30%" class="RightTableCaption" align="right"><fmtAffiliationSetup:message key="LBL_ROLE"/>:&nbsp;</td>
			<td width ="70%">&nbsp;<html:text property="role"  size="30" onfocus="changeBgColor(this,'#AACCE8');" 
				styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>          		
	      	</td>
      	</tr>	 	
	 	<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr height="24"> 
			<td class="RightTableCaption" align="right"><fmtAffiliationSetup:message key="LBL_START_YEAR"/>:&nbsp;</td>
			<td width ="70%">&nbsp;<html:text property="startYear"  size="4" onfocus="changeBgColor(this,'#AACCE8');" 
				styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>          		
	      	</td>	
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr height="24" class="Shade"> 
	      	<td class="RightTableCaption" align="right"><fmtAffiliationSetup:message key="LBL_END_YEAR"/>:&nbsp;</td>
			<td width ="70%">
				&nbsp;<html:text property="endYear"  size="4" onfocus="changeBgColor(this,'#AACCE8');" 
				styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
				&nbsp;&nbsp;<html:checkbox property="stillInPosition"/>&nbsp;<fmtAffiliationSetup:message key="LBL_STILL_IN_POSITION"/>          		
	      </td>	
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
	   	<tr><td colspan="2" height="15"></td></tr>
		<tr height="24"> 
			<td colspan="2" align="center" ><!-- Struts tag lib code modified for JBOSS migration changes -->
				<fmtAffiliationSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button  name="Submit" value="${varSubmit}" buttonType="Save" gmClass="button" onClick="fnAffiliationSubmit(this.form);"/>&nbsp;&nbsp;
				<fmtAffiliationSetup:message key="BTN_RESET" var="varReset"/><gmjsp:button  name="resetBtn" value="${varReset}" buttonType="Save" gmClass="button" onClick="fnAffiliationReset(this.form);"/>&nbsp;&nbsp;
				<fmtAffiliationSetup:message key="BTN_VOID" var="varVoid"/><gmjsp:button  name="voidBtn" value="${varVoid}" buttonType="Save" gmClass="button" onClick="fnAffiliationVoid(this.form);"/>
			</td>
		</tr>
		<tr><td colspan="2" height="15"></td></tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
	 	<tr>
	 		<td colspan="2">
	 		<!-- Changed for JBOSS migration -->
				<jsp:include page="/gmAffiliationSetup.do?method=loadAffiliationReport" flush="true">  	
					<jsp:param name="TYPE" value="Setup"/>													
				</jsp:include>
			</td>
		</tr>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>