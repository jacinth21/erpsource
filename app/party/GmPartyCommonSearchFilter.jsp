 <%
/**********************************************************************************
 * File		 		: GmPartyCommonSearchFilter.jsp
 * Desc		 		: This file is used to render the individual filter fields in the blocks
 *					  for party common search page.
 * Version	 		: 1.0
 * author			: Satyajit Thadeshwar
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>

 <%@ taglib prefix="fmtPartyCommonSearchFilter" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartyCommonSearchFilter:setLocale value="<%=strLocale%>"/>
<fmtPartyCommonSearchFilter:setBundle basename="properties.labels.party.GmPartyCommonSearchFilter"/>
<%@ page import="java.util.ArrayList,java.util.HashMap" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Search Filter</title>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusParty.css">
</head>
<body>
<table class="SearchScreenTable" cellspacing="0" cellpadding="0">
	<%
		HashMap hmField = new HashMap();
		ArrayList alFields = (ArrayList)request.getAttribute("FIELDS");
		for (int i=0; i<alFields.size(); i++ ) {
			
			hmField = (HashMap)alFields.get(i);
			String fieldId = (String)hmField.get("LOOKUPID");
			String fieldLabel = (String)hmField.get("FIELDLABEL");
			String fieldType = (String)hmField.get("FIELDTYPE");
			String styleClass = "";
			if(i%2 == 0)
				styleClass = "Shade";
			else
				styleClass = "None";
	%>
	<tr height="24" class="<%=styleClass%>">
		<td class="RightTableCaption" align="right" width="40%"><%=fieldLabel%>&nbsp;:&nbsp;</td>
		<td align="left" width="60%">
	<% 
			if(fieldType.equals("92016")) {
				String strSize = (String)hmField.get("FIELDATTR");
	%>
			<input type="text" id="<%=fieldId%>" name="filtertext" onfocus="changeBgColor(this,'#AACCE8');" 
			class="InputArea" onblur="changeBgColor(this,'#ffffff');" size="<%=strSize%>"/>
	<% 
		} else if(fieldType.equals("92017")) {
			ArrayList alAttr = (ArrayList)hmField.get("FIELDATTR");
	%>
	<!-- Custom tag lib code modified for JBOSS migration changes -->
			<gmjsp:dropdown controlName="filterdropdown" seletedValue="<%=fieldLabel%>" 	
			value="<%=alAttr%>" codeId="LOOKUPID" codeName="CODENM" defaultValue="[Choose One]"/>
	<%	} 
	%>
		</td>
		
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>
	<% 
		} // End of for loop
	%>
</table>
</body>
</html>