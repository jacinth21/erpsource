<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
/**********************************************************************************
 * File		 		: GmPartyAddressSetup.jsp
 * Desc		 		: Setting up address for the party
 * Version	 		: 1.0
 * author			: Satyajit Thadeshwar
************************************************************************************/
%>
<!--party\GmPartyAddressSetup.jsp  -->

<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtPartyAddressSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartyAddressSetup:setLocale value="<%=strLocale%>"/>
<fmtPartyAddressSetup:setBundle basename="properties.labels.party.GmPartyAddressSetup"/>

<% 
//String strWikiPath = GmCommonClass.getString("GWIKI");
String strWikiTitle = GmCommonClass.getWikiTitle("CONTACT_SETUP");
//Logger log = GmLogger.getInstance(GmCommonConstants.COMMON);  // Instantiating the Logger 
try { %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title> Globus Medical: Contact Setup </title>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>
</script>
</head>

<body leftmargin="20" topmargin="10" >
<html:form action="/gmAddressInfo.do" >
<html:hidden property="strOpt"  name="frmCommonAddress"/>
<html:hidden property="haddressID" name="frmCommonAddress"/>
<html:hidden property="partyType" name="frmCommonAddress"/>
 
 <!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtPartyAddressSetup:message key="LBL_ADDRESS_SETUP"/></td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
		<tr>
			<td width="698" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="30"></font><font color="red">*</font>&nbsp;<fmtPartyAddressSetup:message key="LBL_ADDRESS_TYPE"/>:</td> 
                        <td>&nbsp;
							<gmjsp:dropdown controlName="addressType" SFFormName="frmCommonAddress" SFSeletedValue="addressType"
								SFValue="alAddressTypes" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
                        </td>
                    </tr>                    
                    <tr><td colspan="2" bgcolor="#cccccc"></td></tr> 
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="30"></font><font color="red">*</font>&nbsp;<fmtPartyAddressSetup:message key="LBL_PREFERENCE"/>:</td>
                        <td>&nbsp;
							<gmjsp:dropdown controlName="preference" SFFormName="frmCommonAddress" SFSeletedValue="preference"
								SFValue="alPreferences" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
						</td>
                    </tr>                    
                    <tr><td colspan="2" bgcolor="#cccccc"></td></tr> 
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtPartyAddressSetup:message key="LBL_ADDRESS_1"/>:</td>
                        <td>&nbsp; <html:text property="billAdd1"  size="50" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
                        </td> 
                    </tr>
                    <tr><td colspan="2" bgcolor="#cccccc"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24"><fmtPartyAddressSetup:message key="LBL_ADDRESS_2"/>:</td> 
                        <td>&nbsp; <html:text property="billAdd2"   size="50" name="frmCommonAddress" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
                        </td> 
                    </tr>
                    <tr><td colspan="2" bgcolor="#cccccc"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtPartyAddressSetup:message key="LBL_CITY"/>:</td>                         
                        <td>&nbsp; <html:text property="billCity"   size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
                        </td> 
                    </tr>
                    <tr><td colspan="2" bgcolor="#cccccc"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtPartyAddressSetup:message key="LBL_STATE"/>:</td> 
                        <td>&nbsp;
                			<gmjsp:dropdown controlName="state" SFFormName="frmCommonAddress" SFSeletedValue="state"
								SFValue="alStates" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
				        </td>
                    </tr>
                    <tr><td colspan="2" bgcolor="#cccccc"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtPartyAddressSetup:message key="LBL_ZIP"/>:</td> 
                        <td>&nbsp; <html:text property="zipCode"  size="10"  onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
                        </td> 
                    </tr>
                    <tr><td colspan="2" bgcolor="#cccccc"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtPartyAddressSetup:message key="LBL_COUNTRY"/>:</td> 
                        <td>&nbsp;
	                         <gmjsp:dropdown controlName="country" SFFormName="frmCommonAddress" SFSeletedValue="country"
								SFValue="alCountries" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
                        </td>
                    </tr>
                    <tr><td colspan="2" bgcolor="#cccccc"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24"><fmtPartyAddressSetup:message key="LBL_INACTIVE_FLAG"/>:</td> 
                        <td>&nbsp;<html:checkbox property="activeFlag" />
                         </td>
                    </tr>
                    <tr>
                    <td colspan="2">
                    	<jsp:include page="/common/GmIncludeLog.jsp" >
							<jsp:param name="FORMNAME" value="frmCommonAddress" />
							<jsp:param name="ALNAME" value="alLogReasons" />
							<jsp:param name="LogMode" value="Edit" />
						</jsp:include>
					</td>
                    </tr>                    
                    <tr>                        
                        <td colspan="2" align="center" height="30">&nbsp;
                        <fmtPartyAddressSetup:message key="BTN_ADD_NEW" var="varAddNew"/><gmjsp:button value="${varAddNew}" gmClass="button" buttonType="Save" onClick="fnReset();"/>   
                        <fmtPartyAddressSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnSubmit();" />                     
                        </td>
                    </tr> 
           <tr> 
            <td colspan="2">
            <display:table name="requestScope.frmCommonAddress.ldtResult" class="its" id="currentRowObject"  export = "false" decorator="com.globus.displaytag.beans.GmCommonSetupWrapper" > 
			<display:column property="ID" title=""  class="alignleft"/>
			<fmtPartyAddressSetup:message key="LBL_TYPE" var="varType"/><display:column property="ADDRESSTYPE" title="${varType}" class="aligncenter"/> 
			<fmtPartyAddressSetup:message key="LBL_ADDRESS_LINE1" var="varAddressLine1"/><display:column property="BILLADD1" title="${varAddressLine1}" sortable="true"  />
			<fmtPartyAddressSetup:message key="ADDRESS_LINE2" var="varAddressLine2"/><display:column property="BILLADD2" title="${varAddressLine2}" sortable="true" class="aligncenter" />
			<fmtPartyAddressSetup:message key="LBL_CITY" var="varCity"/><display:column property="BILLCITY" title="${varCity}" sortable="true"  />
			<fmtPartyAddressSetup:message key="LBL_STATE" var="varState"/><display:column property="STATE" title="${varState}" sortable="true"  />
			<fmtPartyAddressSetup:message key="LBL_COUNTRY" var="varCountry"/><display:column property="COUNTRY" title="${varCountry}" sortable="true"  />
			<fmtPartyAddressSetup:message key="LBL_ZIP" var="varZip"/><display:column property="ZIPCODE" title="${varZip}" sortable="true"  />
			<fmtPartyAddressSetup:message key="LBL_PREFERENCE" var="varPreference"/><display:column property="PREFERENCE" title="${varPreference}" sortable="true"  />
			<fmtPartyAddressSetup:message key="LBL_STATUS" var="varStatus"/><display:column property="INACTIVEFL" title="${varStatus}" sortable="true"  />						
			</display:table> 
		</td>
    </tr> 
      				</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
<% 
} 
catch (Exception e) {
e.printStackTrace();
} %>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>