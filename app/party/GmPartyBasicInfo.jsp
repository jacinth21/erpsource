<%
/**********************************************************************************
 * File		 		: GmPartySetup.jsp 
 * Desc		 		: Basic into tab setup details
************************************************************************************/
%>
<!-- \party\GmPartyBasicInfo.jsp -->
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtPartyBasicInfo" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtPartyBasicInfo:setLocale value="<%=strLocale%>"/>
<fmtPartyBasicInfo:setBundle basename="properties.labels.party.GmPartyBasicInfo"/>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Party Setup</title>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
</head>
<bean:define id="partyType" name="frmPartySetup" property="partyType" scope="request" type="java.lang.String"></bean:define>
<bean:define id="partyId" name="frmPartySetup" property="partyId" scope="request" type="java.lang.String"></bean:define>
<bean:define id="strReloadCnt" name="frmPartySetup" property="strReloadCnt" scope="request" type="java.lang.String"></bean:define>
<%
String strPartyJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PARTY");
partyType = partyType == null ? "" : partyType;

String strOddShade = "";
String strEvenShade = "";

  if (partyType.equals("7007"))
  {
	  strOddShade = "oddshade";
	  strEvenShade ="evenshade";
  }
  else
  {
	  strOddShade = "Shade";
	  strEvenShade ="";
  }
%>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strPartyJsPath%>/GmPartyBasicInfo.Js"></script>
<script>
var savedPartyId = '<%=partyId%>';
var reloadCnt = '<%=strReloadCnt%>';
</script>
<style>
table.DtTable770 {
	margin: 0 0 0 0;
	width: 780px;
	border: 1px solid  #676767;
}
</style>
<body leftmargin="10" topmargin="15" onload="fnOnPageLoad();">
<html:form action="/gmPartySetup.do" >
<html:hidden property="strOpt"/>
<html:hidden property="partyType" styleId="partyType"/>

<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VDPTY"/>
<html:hidden property="accessFlag"/> 
<html:hidden property="haction"/>
<html:hidden property="strReloadCnt"/>

<table border="0" class="DtTable770" cellspacing="0" cellpadding="0">	
		 <tr>
		 <td colspan="2" width="100%" height="100" valign="top">
			<table border="0" width="100%" cellspacing="0" cellpadding="0"> 
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<logic:equal name="frmPartySetup" property="displayPartyName" value="true">
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr class="<%=strOddShade%>">
					<td class="RightTableCaption" align="right" width="40%" height="24"><font color="red">*&nbsp;</font><fmtPartyBasicInfo:message key="LBL_NAME"/>:</td>
					<td width="60%">&nbsp;<html:text property="partyName" size="30"
						onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
						onblur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>			
				</logic:equal>
				<logic:notEqual name="frmPartySetup" property="displayPartyName" value="true">
				<logic:notEqual name="frmPartySetup" property="accessFlag" value="Y">
				<tr class="<%=strOddShade%>">
					<td class="RightTableCaption" align="right" width="40%" height="24"><font color="red">*&nbsp;</font><fmtPartyBasicInfo:message key="LBL_FIRST_NAME"/>:</td>
					<td width="60%">&nbsp; <html:text property="firstName" size="30"
						onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
						onblur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr class="<%=strEvenShade%>">
					<td class="RightTableCaption" align="right" width="40%" height="24"><font color="red">*&nbsp;</font><fmtPartyBasicInfo:message key="LBL_LAST_NAME"/>:</td>
					<td width="60%">&nbsp; <html:text property="lastName" size="30"
						onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
						onblur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr class="<%=strOddShade%>">
					<td class="RightTableCaption" align="right" width="40%" height="24"><fmtPartyBasicInfo:message key="LBL_MIDDLE_INITIAL"/>:</td>
					<td width="60%">&nbsp; <html:text property="middleInitial" size="5"
						onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
						onblur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				</logic:notEqual>			
				</logic:notEqual>
				<logic:notEqual name="frmPartySetup" property="displayPartyName" value="true">
				<logic:equal name="frmPartySetup" property="accessFlag" value="Y">
				<tr class="<%=strOddShade%>">
					<td class="RightTableCaption" align="right" width="40%" height="24"><font color="red">*&nbsp;</font><fmtPartyBasicInfo:message key="LBL_NAME"/>:</td>
					<td width="60%">&nbsp;<html:text property="partyName" size="30"
						onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
						onblur="changeBgColor(this,'#ffffff');" />
					</td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				</logic:equal>
				</logic:notEqual>
                <tr class="<%=strEvenShade%>">
                	<td class="RightTableCaption" align="right" width="40%" height="24"><fmtPartyBasicInfo:message key="LBL_INACTIVE_FLAG"/>:</td> 
                	<td width="60%"><html:checkbox property="inactiveFlag" /></td>
                </tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr><td colspan="2" height="15"></td></tr>
				<tr>
					<td colspan="2">
						<jsp:include page="/common/GmIncludeLog.jsp">
							<jsp:param name="FORMNAME" value="frmPartySetup" />
							<jsp:param name="ALNAME" value="alLogReasons" />
							<jsp:param name="LogMode" value="Edit" />
						</jsp:include>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center" height="30">&nbsp;
                    	<fmtPartyBasicInfo:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" gmClass="button" name="Btn_Submit" buttonType="Save" onClick="fnPartySubmit(this.form);return false;"/>&nbsp;&nbsp;
                    	<fmtPartyBasicInfo:message key="BTN_RESET" var="varReset"/><gmjsp:button value="${varReset}" gmClass="button" buttonType="Save" onClick="fnPartyReset();"/>&nbsp;&nbsp;
                    	<fmtPartyBasicInfo:message key="BTN_VOID" var="varVoid"/><gmjsp:button value="${varVoid}" gmClass="button" style="width:5em;" buttonType="Save" onClick="fnPartyVoid(this.form);"/>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>