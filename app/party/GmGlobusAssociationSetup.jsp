<%
/**********************************************************************************
 * File		 		: GmGlobusAssociationSetup.jsp
 * Desc		 		: JSP for setting up Globus Association for party
 * Version	 		: 1.0
 * author			: Satyajit Thadeshwar
************************************************************************************/
%>
<!-- \party\GmGlobusAssociationSetup.jsp-->
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtGlobusAssociationSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtGlobusAssociationSetup:setLocale value="<%=strLocale%>"/>
<fmtGlobusAssociationSetup:setBundle basename="properties.labels.party.GmGlobusAssociationSetup"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body leftmargin="20" topmargin="10">
<html:form action="/gmGlobusAssociationSetup.do">
<html:hidden property="strOpt"/>
<html:hidden property="partyId"/>
<html:hidden property="partyType"/>
<html:hidden property="hglobusAssociationId"/>

<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VDGAS"/>
<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="2" height="15"></td>
	</tr>
	 <tr>
		 <td colspan="2" width="100%" height="100" valign="top">
		 	<table border="0" width="100%" cellspacing="0" cellpadding="0"> 
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr class="Shade">
					<td class="RightTableCaption" align="right" height="24" width="40%"><font color="red">*</font>&nbsp;<fmtGlobusAssociationSetup:message key="LBL_PARTY_TYPE"/>:</td>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<td width="60%">&nbsp; <gmjsp:dropdown controlName="personnelType" onChange="fnGlobusLoad(this.form);"
						SFFormName="frmGlobusAssociationSetup" SFSeletedValue="personnelType" SFValue="alPersonnelType"
						codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" /></td>
				</tr>
				<tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
				</tr>
				<tr>
					<td class="RightTableCaption" align="right" height="24" width="40%"><font color="red">*</font>&nbsp;<fmtGlobusAssociationSetup:message key="LBL_NAME"/>:</td>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<td width="60%">&nbsp; <gmjsp:dropdown controlName="personnelName" SFFormName="frmGlobusAssociationSetup"
						SFSeletedValue="personnelName" SFValue="alPersonnelName" codeId="ID"
						codeName="NAME" defaultValue="[Choose One]" /></td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<logic:equal name="frmGlobusAssociationSetup" property="primaryFlag" value="on">
					<tr class="Shade">
						<td class="RightTableCaption" align="right" height="24"><fmtGlobusAssociationSetup:message key="LBL_PRIMARY_FLAG"/>:</td>
						<td>&nbsp;<html:checkbox property="primaryFlag" disabled="true"/></td>
					</tr>
				</logic:equal>
				<logic:notEqual name="frmGlobusAssociationSetup" property="primaryFlag" value="on">
					<tr class="Shade">
						<td class="RightTableCaption" align="right" height="24"><fmtGlobusAssociationSetup:message key="LBL_PRIMARY_FLAG"/>:</td>
						<td>&nbsp;<html:checkbox property="primaryFlag"/></td>
					</tr>
				</logic:notEqual>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr><td colspan="2" height="15"></td></tr>		
				<tr>
					<td colspan="2" align="center" height="24">
						<fmtGlobusAssociationSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button name="submitBtn" value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnGlobusSubmit(this.form);" />&nbsp; 
						<fmtGlobusAssociationSetup:message key="BTN_RESET" var="varReset"/><gmjsp:button name="resetBtn" value="${varReset}" gmClass="button" buttonType="Save" onClick="fnGlobusReset(this.form);" />&nbsp; 
						<fmtGlobusAssociationSetup:message key="BTN_VOID" var="varVoid"/><gmjsp:button name="voidBtn" value="${varVoid}" gmClass="button" buttonType="Save" onClick="fnGlobusVoid(this.form);" />
					</td>
				</tr>
				<tr><td colspan="2" height="15"></td></tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
    			<tr>
    				<td colspan="2">
    				<!-- Changed for JBOSS migration -->
    					<jsp:include page="/gmGlobusAssociationSetup.do?method=loadGlobusAssociationReport" flush="true">
    						<jsp:param name="TYPE" value="Setup" />
    					</jsp:include>
    				</td>
    			</tr> 
			</table>
		</td>
	</tr>
</table>	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>