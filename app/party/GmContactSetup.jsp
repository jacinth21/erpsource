<%
/**********************************************************************************
 * File		 		: GmContactSetup.jsp
 * Desc		 		: Setting up Contacts
 * Version	 		: 1.1
 * Author			: Joe P Kumar
 * Last modified by	: Satyajit Thadeshwar
************************************************************************************/
%>
<!--\party\GmContactSetup.jsp  -->
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtContactSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<fmtContactSetup:setLocale value="<%=strLocale%>"/>
<fmtContactSetup:setBundle basename="properties.labels.party.GmContactSetup"/>

<%
String strWikiTitle = GmCommonClass.getWikiTitle("CONTACT_SETUP");
String strPartyJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PARTY");

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Contact Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">

<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strPartyJsPath%>/GmParty.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
</head>
<bean:define id="partyType" name="frmContactSetup" property="partyType" scope="request" type="java.lang.String"></bean:define>
<bean:define id="editAccessFlag" name="frmContactSetup" property="editAccessFl" type="java.lang.String"></bean:define>

<%
String strSubmitDisable = "";
if(!editAccessFlag.equals("Y")){
	strSubmitDisable = "true";
}
partyType = partyType == null ? "" : partyType;

String strOddShade = "";
String strEvenShade = "";

  if (partyType.equals("7007"))
  {
	  strOddShade = "oddshade";
	  strEvenShade ="evenshade";
  }
  else
  {
	  strOddShade = "Shade";
	  strEvenShade ="";
  }
%>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmContactSetup.do">
<html:hidden property="strOpt" value=""/>
<html:hidden property="hcontactId" name="frmContactSetup"/>
<html:hidden property="partyId"/>
<html:hidden property="partyType" name="frmContactSetup"/>
<html:hidden property="includedPage" name="frmContactSetup"/>

<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VDCON"/>
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<logic:notEqual name="frmContactSetup" property="includedPage" value="true">
			<tr>
				<td height="25" class="RightDashBoardHeader"><fmtContactSetup:message key="LBL_CONTACT_SETUP"/></td>
				<td  height="25" class="RightDashBoardHeader" align="right">
				<fmtContactSetup:message key="IMG_ALT_HELP" var="varHelp"/>
				<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
		       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		    	</td>
			</tr>
			<tr><td colspan="2" height="0" bgcolor="#666666"></td></tr>
		</logic:notEqual>		
		<tr>
			<td width="698" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr><td colspan="2" height="15"></td></tr> 
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
                    <tr class="<%=strOddShade%>">
                        <td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtContactSetup:message key="LBL_CONTACT_MODE"/>:</td>
                        <td>&nbsp;<!-- Custom tag lib code modified for JBOSS migration changes -->
							<gmjsp:dropdown controlName="contactMode" SFFormName="frmContactSetup" SFSeletedValue="contactMode"
								SFValue="alContactMode" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
						</td>
                    </tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
                    <tr class="<%=strEvenShade%>">
                        <td class="RightTableCaption" align="right" height="24"><font color="red">*</font>&nbsp;<fmtContactSetup:message key="LBL_CONTACT_TYPE"/>:</td> 
                        <td>&nbsp;
							<html:text property="contactType" name="frmContactSetup" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
						</td>
                    </tr>                    
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
                    <tr class="<%=strOddShade%>">
                        <td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtContactSetup:message key="LBL_CONTACT_VALUE"/>:</td> 
                        <td>&nbsp; <html:text property="contactValue" name="frmContactSetup" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
                        </td> 
                    </tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
                    <tr class="<%=strEvenShade%>">
                        <td class="RightTableCaption" align="right" height="24"><fmtContactSetup:message key="LBL_PREFERENCE"/>:</td>
						<td>&nbsp;
							<html:text property="preference" size="4" name="frmContactSetup" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
						</td>
                    </tr>                    
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
                    <tr class="<%=strOddShade%>">
                        <td class="RightTableCaption" align="right" HEIGHT="24"><fmtContactSetup:message key="LBL_INACTIE_FLAG"/> :</td> 
                        <td>&nbsp;<html:checkbox property="inactiveFlag" />
                         </td>
                    </tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
                    <logic:equal name="frmContactSetup" property="primaryFlag" value="on">
                    	<tr class="<%=strEvenShade%>">
                        	<td class="RightTableCaption" align="right" HEIGHT="24"><fmtContactSetup:message key="LBL_PRIMARY_CONTACT"/> :</td> 
                        	<td>&nbsp;<html:checkbox disabled="true" property="primaryFlag" /></td>
                    	</tr>
                    	
                    </logic:equal>
                    <logic:notEqual name="frmContactSetup" property="primaryFlag" value="on">
                    	<tr class="<%=strEvenShade%>">
                        	<td class="RightTableCaption" align="right" HEIGHT="24"><fmtContactSetup:message key="LBL_PRIMARY_CONTACT"/> :</td> 
                        	<td>&nbsp;<html:checkbox property="primaryFlag" /></td>
                    	</tr>
                    </logic:notEqual>                    
                    <tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
                    <tr><td colspan="2" height="15"></td></tr>
                    <td colspan="2">
                    	<jsp:include page="/common/GmIncludeLog.jsp" >
							<jsp:param name="FORMNAME" value="frmContactSetup" />
							<jsp:param name="ALNAME" value="alLogReasons" />
							<jsp:param name="LogMode" value="Edit" />
						</jsp:include>
					</td>
                    </tr>                    
                    <tr><td colspan="2" height="15"></td></tr>
                    <tr>                        
                        <td colspan="2" align="center" height="24">
							<fmtContactSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button name="submitBtn" value="${varSubmit}" gmClass="button" buttonType="Save" disabled="<%=strSubmitDisable%>" onClick="fnContactSubmit(this.form);"/>&nbsp;&nbsp;
							<fmtContactSetup:message key="BTN_RESET" var="varReset"/><gmjsp:button name="resetBtn" value="${varReset}" gmClass="button" buttonType="Save" onClick="fnContactReset(this.form);"/>&nbsp;&nbsp;
							<fmtContactSetup:message key="BTN_VOID" var="varVoid"/><gmjsp:button name="voidBtn" value="${varVoid}" disabled="<%=strSubmitDisable%>" gmClass="button" buttonType="Save" onClick="fnContactVoid(this.form);"/>
                        </td>
                    </tr>
                    <tr><td colspan="2" height="15"></td></tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
    				<tr>
    					<td colspan="2">
    					<!-- Changed for JBOSS migration -->
    						<jsp:include page="/gmContactSetup.do?method=loadContactReport" flush="true">
    							<jsp:param name="TYPE" value="Setup" />
    						</jsp:include>
    					</td>
    				</tr>  
			</table>
 	   </td>
	  </tr>	
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

