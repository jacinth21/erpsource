<!--\party\GmIncludeBasicInfoSummary.jsp  -->
<%@ include file="/common/GmHeader.inc" %>


<%@ taglib prefix="fmtIncludeBasicInfoSummary" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>


<fmtIncludeBasicInfoSummary:setLocale value="<%=strLocale%>"/>
<fmtIncludeBasicInfoSummary:setBundle basename="properties.labels.party.GmIncludeBasicInfoSummary"/>
<bean:define id="strAddress" name="frmBasicInfoSummary" property="address" scope="request" type="java.lang.String"></bean:define>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Summary Page</title>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
</head>
<body>
<html:form action="/gmBasicInfoSummary.do">
<table cellspacing="0" cellpadding="0" style="border: 1px solid #676767; WORD-BREAK:BREAK-ALL;">
<%
try {

	String fileId = (String)request.getAttribute("FILEID");
	String hSize = request.getParameter("HSIZE") == null ? "120" : request.getParameter("HSIZE");
	String vSize = request.getParameter("VSIZE") == null ? "120" : request.getParameter("VSIZE");
	String display = "";
	
	display += "<tr><td colspan=\"2\" height=\"10\"></td></tr>";
	display += "<tr><td colspan=\"2\" align=\"center\">";
	display += "<img src=/GmCommonFileOpenServlet?uploadString=UPLOADIMAGESHOME&sId="+fileId+" height="+hSize+" width="+vSize+"/>";
	display += "</td></tr><tr><td colspan=\"2\" height=\"10\"></td></tr>";
	out.println(display);

} catch (Exception e) {}
%>
</table>
<table class="DtSummaryTable" cellspacing="0" cellpadding="0">
	<tr class="TableHeaderRow">
		<td align="left"><fmtIncludeBasicInfoSummary:message key="LBL_BASIC_INFO"/></td>
		<td align="right"></td>
	</tr>	
	<tr height="22">
		<td align="right" width="30%" class="RightTableCaption"><fmtIncludeBasicInfoSummary:message key="LBL_NAME"/>:&nbsp;</td>
		<td align="left" width="70%"><bean:write name="frmBasicInfoSummary" property="name"/></td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>
	<tr class="Shade"><td colspan="2" height="5"></td></tr>
	<logic:iterate id="contacts" name="frmBasicInfoSummary" property="alContacts">
		<tr class="Shade">
			<td align="right" width="30%" class="RightTableCaption"><bean:write name="contacts" property="CMODE"/>&nbsp;:&nbsp;</td>
			<td align="left" width="70%"><bean:write name="contacts" property="CVALUE"/></td>
		</tr>
	</logic:iterate>	
	<tr class="Shade">
		<td align="right" width="30%"></td>
		<td align="right" width="70%"><a href="javascript:windowOpener('gmContactSetup.do?method=loadContactReport&TYPE=Report&partyId=<bean:write name="frmBasicInfoSummary" property="partyId"/>', 'Contacts', 'resizable=yes,scrollbars=yes,top=40,left=50,width=600,height=300,align=center');"><fmtIncludeBasicInfoSummary:message key="LBL_MORE"/></a>&nbsp;</td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>
	<tr height="22">
		<td align="right" width="30%" class="RightTableCaption"><fmtIncludeBasicInfoSummary:message key="LBL_ADDRESS"/>:&nbsp;</td>
		<td align="left" width="70%"><%=strAddress%></td>
	</tr>
	<tr>
		<td align="right" width="30%"></td>
		<td align="right" width="70%"><a href="javascript:windowOpener('gmAddressSetup.do?method=loadAddressReport&TYPE=Report&partyId=<bean:write name="frmBasicInfoSummary" property="partyId"/>', 'Addresses', 'resizable=yes,scrollbars=yes,top=40,left=50,width=600,height=300,align=center');"><fmtIncludeBasicInfoSummary:message key="LBL_MORE"/></a>&nbsp;</td>
	</tr>
</table>
</html:form>
</body>
</html>