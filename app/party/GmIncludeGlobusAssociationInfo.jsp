<%
/**********************************************************************************************
 * File		 		: GmIncludeContactInfo.jsp
 * Desc		 		: JSP file to display the contact display tag in setup and summary pages
 * Version	 		: 1.0
 * author			: Satyajit Thadeshwar
/***********************************************************************************************/
%>
<!--\party\GmIncludeGlobusAssociationInfo.jsp  -->
<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtIncludeGlobusAssociationInfo" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtIncludeGlobusAssociationInfo:setLocale value="<%=strLocale%>"/>
<fmtIncludeGlobusAssociationInfo:setBundle basename="properties.labels.party.GmIncludeGlobusAssociationInfo"/>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<% 
	String displayType = request.getParameter("TYPE");
	String strMedia = displayType.equalsIgnoreCase("Setup") ? "HTML" : "FALSE";
	boolean bSortable = displayType.equalsIgnoreCase("Setup") ? true : false;
	bSortable = false;
%>
<display:table name="requestScope.frmGlobusAssociationSetup.lresult" class="gmits" id="currentRowObject"  export = "false" 
	decorator="com.globus.party.displaytag.DTGlobusInfoWrapper" > 
	<display:column property="ID" title="" class="aligncenter" media="<%=strMedia%>"/>
	<fmtIncludeGlobusAssociationInfo:message key="LBL_PARTY_NAME" var="varPartyName"/><display:column property="PERSONNELNAME" title="${varPartyName}" class="alignleft" sortable="<%=bSortable%>"/>
	<fmtIncludeGlobusAssociationInfo:message key="LBL_PARTY_TYPE" var="varPartyType"/><display:column property="PERSONNELTYPE1" title="${varPartyType}" class="alignleft" sortable="<%=bSortable%>" />
	<fmtIncludeGlobusAssociationInfo:message key="LBL_PRIMARY" var="varPrimary"/><display:column property="PRIMARYFLAG" title="${varPrimary}" sortable="<%=bSortable%>" class="aligncenter" />				
</display:table>