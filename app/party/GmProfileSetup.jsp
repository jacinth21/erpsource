<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- \party\GmProfileSetup.jsp-->
<%@ include file="/common/GmHeader.inc" %>
<% 
Object bean = pageContext.getAttribute("frmPartySetup", PageContext.REQUEST_SCOPE);
pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
%>
<meta http-equiv="Content-Type" content="text/html; charset=UFT-8">
<title>Profile Setup</title>
</head>
<body>
<html:form action="/gmProfileSetup.do">
<html:hidden property="strOpt"/>
<html:hidden property="partyId"/>
<html:hidden property="partyType"/>
</html:form>
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<ul id="profiletab" class="shadetabs">
					<logic:iterate id="ptabs" name="frmPartySetup" property="alTabs" >					
								<logic:equal name="ptabs" property="TABLABEL" value="Professional Details"> 										   	
								<li onclick="fnUpdateTabIndex(this);"><a href="/<bean:write name="ptabs" property="ACTION"/>partyId=<bean:write name="frmPartySetup" property="partyId"/>&partyType=<bean:write name="frmPartySetup" property="partyType"/>&strOpt=fetch&includedPage=true" title="<bean:write name="ptabs" property="TABLABEL"/>" rel="#iframe" name="<bean:write name="ptabs" property="TABLABEL"/>"><bean:write name="ptabs" property="TABLABEL"/></a></li>										   		
								</logic:equal>
								
								<logic:notEqual name="ptabs" property="TABLABEL" value="Professional Details">		
								<li onclick="fnUpdateTabIndex(this);"><a href="/<bean:write name="ptabs" property="ACTION"/>partyId=<bean:write name="frmPartySetup" property="partyId"/>&partyType=<bean:write name="frmPartySetup" property="partyType"/>&strOpt=fetch" title="<bean:write name="ptabs" property="TABLABEL"/>" rel="profiledivcontentarea" name="<bean:write name="ptabs" property="TABLABEL"/>"><bean:write name="ptabs" property="TABLABEL"/></a></li>
								</logic:notEqual>												
					</logic:iterate>
				</ul>
			</td>
		</tr>
		<tr>
			<td>
				<div id="profiledivcontentarea" class="contentstyle" style="display:visible;height:577px;overflow:auto;" >
				</div>
			</td>
		</tr>
	</table>
</body>
</html>