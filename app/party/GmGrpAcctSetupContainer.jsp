<%
/**********************************************************************************
 * File		 		: GmGrpAcctSetupContainer.jsp 
 * Desc		 		: Group Account setup details
 * author			: Jreddy
************************************************************************************/
%>
<!-- \party\GmGrpAcctSetupContainer.jsp -->
<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtGrpAcctSetupContainer" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtGrpAcctSetupContainer:setLocale value="<%=strLocale%>"/>
<fmtGrpAcctSetupContainer:setBundle basename="properties.labels.party.GmGrpAcctSetupContainer"/>
<bean:define id="partyId" name="frmPartySetup" property="partyId" scope="request" type="java.lang.String"></bean:define>
<bean:define id="partyType" name="frmPartySetup" property="partyType" scope="request" type="java.lang.String"></bean:define>

<%
String strPartyJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PARTY");
partyType = partyType == null ? "" : partyType;

String strBodyLeftMargin ="";
String strBodyTopMargin ="";
String strHeader = "";

  if (partyType.equals("7007"))
  {
	  strBodyLeftMargin = "0";
	  strBodyTopMargin = "0";
	  strHeader = "Header";
  }
  else
  {
	  strBodyLeftMargin = "20";
	  strBodyTopMargin = "10";
	  strHeader = "RightDashBoardHeader";
  }
  
  String strTDHeight = "330px";  
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Party Setup</title>

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />

<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxlayout/dhtmlxlayout.js "></script>
<script language="JavaScript" src="<%=strPartyJsPath%>/GmPartyBasicInfo.js"></script>
<link rel="STYLESHEET" type="text/css"    href="<%=strExtWebPath%>/dhtmlx/dhtmlxTabbar/dhtmlxtabbar.css">

<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxTabbar/dhtmlxtabbar.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxToolbar/dhtmlxtoolbar.js"></script>
<style>
table.DtTable800 {
	margin: 0 0 0 0;
	width: 800px;
	border: 1px solid  #676767;
}
</style>
<script>
var savedPartyId = '<%=partyId%>';
function fnOpenSummaryPage(partyId, partyType) {
	windowOpener('gmPartySummary.do?partyId='+partyId+'&partyType='+partyType, 'PartySummary', 'resizable=yes,scrollbars=yes,top=40,left=50,width=800,height=1000,align=center');
}<%-- 
var FormVldArrLen = <%=alRequestedby.size()%>; --%>
function fnOnloadpage(){
	var myTabbar = new dhtmlXTabBar("my_tabbar");
	myTabbar.setSkin("winscarf");
	myTabbar.setImagePath("/extweb/dhtmlx/dhtmlxTabbar/imgs/"); 
	myTabbar.addTab("basicInfo", message_prodmgmnt[327]);
	myTabbar.addTab("shipparameters",message_prodmgmnt[328]);
	myTabbar.addTab("pricingparameters",message_prodmgmnt[329]);
	
	myTabbar.attachEvent("onSelect", function(id,last_id){		 
		if(id=="basicInfo"){
			document.getElementById("my_tabbar").style.height = '330px';	
			myTabbar.cells(id).attachURL('/gmPartySetup.do?haction=DIVCONTAINER&partyId=<bean:write name="frmPartySetup" property="partyId"/>&partyType=<bean:write name="frmPartySetup" property="partyType"/>&strOpt=basicinfo&'+fnAppendCompanyInfo());
		}else if(id=="shipparameters"){
			document.getElementById("my_tabbar").style.height = '430px';	
			myTabbar.cells(id).attachURL('/gmPartyShipParamsSetup.do?method=setPartyShipParam&strOpt=fetchParams&screenType=group&accountId=&partyID=<bean:write name="frmPartySetup" property="partyId"/>&'+fnAppendCompanyInfo());	     	
		}else if(id=="pricingparameters"){
			document.getElementById("my_tabbar").style.height = '400px';	
			myTabbar.cells(id).attachURL('/gmPricingParamsSetup.do?method=fetchPricingParams&hScreenType=groupAccountSetup&partyId=<bean:write name="frmPartySetup" property="partyId"/>&'+fnAppendCompanyInfo());		  
		}
	     return true;
	 });
	myTabbar.setTabActive("basicInfo");
	}

</script>
<%
// Below css file is needed only for the screens in Clinical module; 7000: Surgeon, 7007:Site Coordinator
if(partyType.equals("7000") || partyType.equals("7007")){ %>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<%} %>
</head>
<body leftmargin="<%=strBodyLeftMargin%>" topmargin="<%=strBodyTopMargin%>" onload="fnOnloadpage();">
<html:form action="/gmPartySetup.do">
<html:hidden property="strOpt" value=""/>
<html:hidden property="hcurrentTab" value=""/>
<html:hidden property="partyType" styleId="partyType"/>
<html:hidden property="haction" styleId="hAction"/>
<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="<%=strHeader %>">&nbsp;<bean:write name="frmPartySetup" property="title"/>&nbsp;
<fmtGrpAcctSetupContainer:message key="LBL_SETUP"/></td>
			<td align="right" class="<%=strHeader %>" > 
			     <% if (!partyType.equals("7007"))
  					 {
				 %>  
				 <fmtGrpAcctSetupContainer:message key="IMG_ALT_HELP" var="varHelp"/>
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' 
				onClick="javascript:fnPartyHelp('<%=strWikiPath%>');" />&nbsp;
				 
				<% } %>
			</td>
		</tr>
		<tr>
			<td colspan="2" height="1" bgcolor="#666666"></td>
		</tr>
			<tr>
				<td class="RightTableCaption" colspan="2" align="center" height="30" width="100%"><font color="red">*</font>&nbsp;<bean:write name="frmPartySetup" property="title"/>:				
					<gmjsp:dropdown controlName="partyId" onChange="fnPartyFetch(this.form);" SFFormName="frmPartySetup" SFSeletedValue="partyId"
					SFValue="alParty" codeId="ID"  codeName="NAME"  defaultValue="[Choose One]"/>
					
					 <input type="text" size="8" value="<%=partyId%>" name="grpPartyId" id="grpPartyId" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" onChange="fnPartyFetchNm(this.form);">
					 
					 <logic:notEqual name="frmPartySetup" property="partyId" value="">
		 				<logic:notEqual name="frmPartySetup" property="partyId" value="0">
		 					 <logic:notEqual name="frmPartySetup" property="partyType" value="7007">
		 					 	<logic:notEqual name="frmPartySetup" property="partyType" value="7008"> 
									<logic:notEqual name="frmPartySetup" property="accessFlag" value="Y">
									&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:fnOpenSummaryPage('<bean:write name="frmPartySetup" property="partyId"/>', '<bean:write name="frmPartySetup" property="partyType"/>');">
<fmtGrpAcctSetupContainer:message key="LBL_VIEW_SUMMARY"/></a>
									</logic:notEqual>
								</logic:notEqual>
							</logic:notEqual>
						</logic:notEqual>
					</logic:notEqual>
				</td>	
			</tr>	
			
		<tr>
			<td colspan="2" id="my_tabbar"style="height:<%= strTDHeight%>; width: 800px;"></td>
		</tr>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>