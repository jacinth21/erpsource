
<%@ include file="/common/GmHeader.inc"%>	
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page language="java"%>
																							
<%@ page import="java.util.ArrayList,java.util.HashMap"%>

<%@ taglib prefix="fmtDealerInvoiceParamSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- custservice\GmDealerInvoiceParamSetup.jsp -->
<fmtDealerInvoiceParamSetup:setLocale value="<%=strLocale%>"/>
<fmtDealerInvoiceParamSetup:setBundle basename="properties.labels.sales.admin.GmDealerInvoiceParamSetup"/>
<%

String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
GmResourceBundleBean gmResourceBundleBean =
GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
String strPaymentTermFlag = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHOW_PAYMENTTERM_MONTH"));
 
%>
<bean:define id="electronicEmail" name="frmDealerSetup" property="electronicEmail" type="java.lang.String"></bean:define>

<HTML>
<HEAD>
<TITLE>Globus Medical: Dealer Invoice Parameters</TITLE>


</HEAD>
<BODY>
	<html:form action="/gmDealerSetup.do?">
<html:hidden property="strOpt" name="frmDealerSetup" value="" />
<html:hidden property="partyId"/>
<html:hidden property="partyType" name="frmDealerSetup"/>
<html:hidden property="includedPage" name="frmDealerSetup"/>
<html:hidden property="paymentTermFlag" name="frmDealerSetup" value="<%=strPaymentTermFlag%>" />

		<table border="0" class="DtTable700" cellspacing="0" cellpadding="0" style="height: auto;">

			
			<tr><td class="LLine" height="1" colspan="2"></td></tr>
			
			<tr height="25" class="evenshade" >
			
			<fmtDealerInvoiceParamSetup:message key="LBL_INVOICE_CUSTOMER_TYPE" var="varInvoiceCustomerType"/>
				<gmjsp:label type="MandatoryText" SFLblControlName="${varInvoiceCustomerType}:"
					td="true" />
			<td width=60%>
			&nbsp;<gmjsp:dropdown controlName="invoicecustomertype" SFFormName="frmDealerSetup" SFSeletedValue="invoicecustomertype"
					SFValue="alInvoiceCustomer" codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]"/>
			
				</td>
			</tr>
						<tr><td class="LLine" height="1" colspan="2"></td></tr>
			<tr height="25" class="oddshade" >
			<fmtDealerInvoiceParamSetup:message key="LBL_CONSOLIDATED_BILLING" var="varConsolidatedBill"/>
				<gmjsp:label type="BoldText" SFLblControlName="${varConsolidatedBill}:"
					td="true" />
			<td width=60%>
			&nbsp;<html:checkbox property="consolidatedBilling"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/></td>
				
			
				</td>
			</tr>
			<tr><td class="LLine" height="1" colspan="2"></td></tr>
			<tr height="25" class="evenshade" >
			<fmtDealerInvoiceParamSetup:message key="LBL_INVOICE_CLOSING_DATE" var="varInvoiceClosingDate"/>
				<gmjsp:label type="MandatoryText" SFLblControlName="${varInvoiceClosingDate}:"
					td="true" />
			<td width=60%>
			&nbsp;<gmjsp:dropdown controlName="invoiceClosingDate" SFFormName="frmDealerSetup" SFSeletedValue="invoiceClosingDate"
					SFValue="alInvoiceClosingDate" codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]"/>
				
			
				</td>
			</tr>
						<tr><td class="LLine" height="1" colspan="2"></td></tr>
			<tr height="25" class="oddshade" >
			<fmtDealerInvoiceParamSetup:message key="LBL_PAYMENT_TERMS" var="varPaymentTerms"/>
				<gmjsp:label type="MandatoryText" SFLblControlName="${varPaymentTerms}:"
					td="true" />
			<td width=60% class="RightTableCaption">
					&nbsp;<gmjsp:dropdown controlName="paymentTerms" SFFormName="frmDealerSetup" SFSeletedValue="paymentTerms"
					SFValue="alPaymentTerms" codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]"/>
				</td>
			</tr>
			<tr><td class="LLine" height="1" colspan="2"></td></tr>
			<tr height="25" class="evenshade" >
			<fmtDealerInvoiceParamSetup:message key="LBL_COLLECTOR_ID" var="varCollectorID"/>
				<gmjsp:label type="BoldText" SFLblControlName="${varCollectorID}:"
					td="true" />
			<td width=60%>
			&nbsp;<gmjsp:dropdown controlName="colletorId" SFFormName="frmDealerSetup" SFSeletedValue="colletorId"
					SFValue="alCollector" codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]"/>
				
			
				</td>
			</tr>
			<tr><td class="LLine" height="1" colspan="2"></td></tr>	
			<tr height="25" class="oddshade" >
			<fmtDealerInvoiceParamSetup:message key="LBL_CREDIT_RATING" var="varCreditRating"/>
				<gmjsp:label type="BoldText" SFLblControlName="${varCreditRating}:"
					td="true" />
			<td width=60%>
			&nbsp;<html:text size="20" name="frmDealerSetup"	property="creditRating" styleClass="InputArea"
				onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" maxlength="20" />
				
			
				</td>
			</tr>
            <tr><td class="LLine" height="1" colspan="2"></td></tr>
			<tr height="25" class="evenshade" >
			<fmtDealerInvoiceParamSetup:message key="LBL_ELECTRONIC_EMAIL" var="varElectronicEmail"/>
				<gmjsp:label type="BoldText" SFLblControlName="${varElectronicEmail}:"
					td="true" />
			<td width=60%>
			&nbsp;<html:checkbox property="electronicEmail"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/></td>
				</td>
			</tr>
			<tr><td class="LLine" height="1" colspan="2"></td></tr>
			<tr height="25" class="oddshade" >
			<fmtDealerInvoiceParamSetup:message key="LBL_ELECTRONIC_FILE_FORMAT" var="varElectronicFileFormat"/>
				<gmjsp:label type="MandatoryText" SFLblControlName="${varElectronicFileFormat}:"
					td="true" />
			<td width=60%>
			&nbsp;<gmjsp:dropdown controlName="elecronicFileFormat" SFFormName="frmDealerSetup" SFSeletedValue="elecronicFileFormat"
					SFValue="alElecrtonicFileFmt" codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]"/>
				</td>
			</tr>
			<tr><td class="LLine" height="1" colspan="2"></td></tr>
			
			<tr height="25" class="evenshade" >
			<fmtDealerInvoiceParamSetup:message key="LBL_RECIPIENTS_EMAIL" var="varRecipientsEmail"/>
				<gmjsp:label type="MandatoryText" SFLblControlName="${varRecipientsEmail}:"
					td="true" />
			<td width=70%>
			&nbsp;<html:text  size="50" name="frmDealerSetup"	property="receipientsEmail" styleClass="InputArea"
				onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"  />
				</td>
			</tr>
			<tr><td class="LLine" height="1" colspan="2"></td></tr>
			<tr height="25" class="oddshade" >
			<fmtDealerInvoiceParamSetup:message key="LBL_CSV_TEMPLATE_NAME" var="varCSVTemplateName"/>
				<gmjsp:label type="BoldText" SFLblControlName="${varCSVTemplateName}:"
					td="true" />
			<td width=60%>
			&nbsp;<gmjsp:dropdown controlName="csvTemplatteName" SFFormName="frmDealerSetup" SFSeletedValue="csvTemplatteName"
					SFValue="alCsvTempName" codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]"/>
				
			
				</td>
			</tr>
			<tr><td class="LLine" height="1" colspan="2"></td></tr>
			<tr height="25" class="evenshade" >
			<fmtDealerInvoiceParamSetup:message key="LBL_INVOICE_LAYOUT" var="varInvoiceLayout"/>
				<gmjsp:label type="MandatoryText" SFLblControlName="${varInvoiceLayout}:"
					td="true" />
			<td width=60%>
			&nbsp;<gmjsp:dropdown controlName="invoiceLayout" SFFormName="frmDealerSetup" SFSeletedValue="invoiceLayout"
					SFValue="alInvoiceLayout" codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]"/>
				
			
				</td>
			</tr>
			<tr><td class="LLine" height="1" colspan="2"></td></tr>
			<tr height="25" class="oddshade" >
			<fmtDealerInvoiceParamSetup:message key="LBL_VENDOR" var="varVendor"/>
				<gmjsp:label type="BoldText" SFLblControlName="${varVendor}:"
					td="true" />
			<td width=60%>
			&nbsp;<html:text size="50" name="frmDealerSetup"	property="vendor" styleClass="InputArea"
				onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" maxlength="20" />
				
			
				</td>
			</tr>
			<tr><td class="LLine" height="1" colspan="2"></td></tr>
			
			<tr>
			<td colspan="2"> 
				<jsp:include page="/common/GmIncludeLog.jsp" >
					<jsp:param name="FORMNAME" value="frmDealerSetup" />
					<jsp:param name="ALNAME" value="alLogReasons" />
					<jsp:param name="LogMode" value="Edit" />
					<jsp:param name="Mandatory" value="yes" />
				</jsp:include>
			</td>
			</tr>
			<tr height="30">
				<td colspan="2" align="center">
				<fmtDealerInvoiceParamSetup:message key="BTN_SUBMIT" var="varSubmit"/>
				<gmjsp:button controlId="Btn_Submit" value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnDealerSubmit(this.form);" />
					
				</td>
			</tr>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
	</BODY>
</HTML>