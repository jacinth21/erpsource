<!-- party\GmPersonalInfoSetup.jsp -->
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtPersonalInfoSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPersonalInfoSetup:setLocale value="<%=strLocale%>"/>
<fmtPersonalInfoSetup:setBundle basename="properties.labels.party.GmPersonalInfoSetup"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Personal Details</title>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
</head>
<body leftmargin="20" topmargin="10">
<html:form action="/gmPersonalInfoSetup.do" >
<input type="hidden" name="hAction"	/>
<html:hidden property="partyId"/>
<input type="hidden" property="status" />

<!-- Custom tag lib code modified for JBOSS migration changes -->
<!-- Struts tag lib code modified for JBOSS migration changes -->

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td colspan="3" height="15"></td></tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr height="24" class="Shade">
						<td width="25%" align="right" class="RightTableCaption"><fmtPersonalInfoSetup:message key="LBL_GENDER"/>:</td>
						<td width="50%" align="left">
							&nbsp;&nbsp;<gmjsp:dropdown controlName="gender" SFFormName="frmPersonalInfoSetup" SFSeletedValue="gender"
							SFValue="alGender" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" /></td>
						<td width="25%"><font color="red"><bean:write name="frmPersonalInfoSetup" property="savedLabel"/></font></td>
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr height="24">
						<td width="25%" align="right" class="RightTableCaption"><fmtPersonalInfoSetup:message key="LBL_DATE_OF_BIRTH"/>:</td> 
						<td width="50%" align="left">
						&nbsp;&nbsp;<html:text property="dateOfBirth" size="12" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');"/>
						&nbsp;&nbsp;<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmPersonalInfoSetup.dateOfBirth');" 
						title="Click to open Calendar"  src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" 
						height=18 width=19 /></td>
						<td width="25%"></td>
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr height="24" class="Shade">
						<td width="25%" align="right" class="RightTableCaption"><fmtPersonalInfoSetup:message key="LBL_HOME_TOWN"/> :</td> 
						<td width="50%" align="left">
						&nbsp;&nbsp;<html:text property="hometown" size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" 
					onblur="changeBgColor(this,'#ffffff');"/></td>
						<td width="25%"></td>
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr height="24">
						<td width="25%" align="right" class="RightTableCaption"><fmtPersonalInfoSetup:message key="LBL_PERSONAL_WEBSITE"/> :</td> 
						<td width="50%" align="left">
						&nbsp;&nbsp;<html:text property="personalWebsite" size="30" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');"/></td>
						<td width="25%"></td>
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr height="24" class="Shade">
						<td width="25%" align="right" class="RightTableCaption"><fmtPersonalInfoSetup:message key="LBL_RELATIONSHIP_STATUS"/> :</td> 
						<td width="50%" align="left">
						&nbsp;&nbsp;<gmjsp:dropdown controlName="relationshipStatus" SFFormName="frmPersonalInfoSetup" SFSeletedValue="relationshipStatus"
								SFValue="alRelationshipStatus" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" onChange="fnRelationshipStatus(this.form)"/></td>
						<td width="25%"></td>
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr height="24">
						<td width="25%" align="right" class="RightTableCaption"><fmtPersonalInfoSetup:message key="LBL_SPOUSE_DETAILS"/>  :</td> 
						<td width="50%" align="left">
							<logic:equal name="frmPersonalInfoSetup" property="disableSpouseDet" value="true">
								&nbsp;&nbsp;<html:textarea rows="3" cols="50" property="spouseDetails" disabled="true" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
								onblur="changeBgColor(this,'#ffffff');"></html:textarea>
							</logic:equal>
							<logic:notEqual name="frmPersonalInfoSetup" property="disableSpouseDet" value="true">
								&nbsp;&nbsp;<html:textarea rows="3" cols="50" property="spouseDetails" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
								onblur="changeBgColor(this,'#ffffff');"></html:textarea>
							</logic:notEqual>
						</td>
						<td width="25%"></td>
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr height="24" class="Shade">
						<td width="25%" align="right" class="RightTableCaption"><fmtPersonalInfoSetup:message key="LBL_CHILDREN"/> :</td> 
						<td width="50%" align="left">
						&nbsp;&nbsp;<gmjsp:dropdown controlName="children" SFFormName="frmPersonalInfoSetup" SFSeletedValue="children"
								SFValue="alChildren" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" onChange="fnChildrenCount(this.form)"/></td>
						<td width="25%"></td>
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr height="24">
						<td width="25%" align="right" class="RightTableCaption"><fmtPersonalInfoSetup:message key="LBL_CHILDREN_DETAILS"/>  :</td> 
						<td width="50%" align="left">
							<logic:equal name="frmPersonalInfoSetup" property="disableChildDet" value="true">
								&nbsp;&nbsp;<html:textarea rows="3" cols="50" property="childrenDetails" disabled="true" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
								onblur="changeBgColor(this,'#ffffff');"/>
							</logic:equal>
							<logic:notEqual name="frmPersonalInfoSetup" property="disableChildDet" value="true">
								&nbsp;&nbsp;<html:textarea property="childrenDetails" rows="3" cols="50" onfocus="changeBgColor(this,'#AACCE8');"  	
								onblur="changeBgColor(this,'#ffffff');" />
							</logic:notEqual>
						</td>
						<td width="25%"></td>
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr height="24" class="Shade">
						<td width="25%" align="right" class="RightTableCaption"><fmtPersonalInfoSetup:message key="LBL_HOBBIES"/> :</td> 
						<td width="50%">
						&nbsp;&nbsp;<html:textarea cols="50" property="hobbies" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');"></html:textarea>
						</td>
						<td width="25%"><fmtPersonalInfoSetup:message key="LBL_SEPARATE_BY_SEMICOLON"/></td>
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr><td colspan="3" height="15"></td></tr>
					<tr height="24">
						<td colspan="3" align="center">
							<fmtPersonalInfoSetup:message key="BTN_SUBMTI" var="varSubmit"/><gmjsp:button  value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnPersonalInfoSubmit(this.form);"/>
						</td>
					</tr>
					<tr><td colspan="3" height="15"></td></tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
				</table>
			</td>
		</tr>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>