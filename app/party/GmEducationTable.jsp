<%
/**********************************************************************************************
 * File		 		: GmEducationTable.jsp
 * Desc		 		: JSP file to display the education display tag in setup and summary pages
 * Version	 		: 1.0
 * author			: Satyajit Thadeshwar
/***********************************************************************************************/
%>
<!-- \party\GmEducationTable.jsp -->
<%@ include file="/common/GmHeader.inc" %>



<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}

%>
<%@ taglib prefix="fmtEducationTable" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtEducationTable:setLocale value="<%=strLocale%>"/>
<fmtEducationTable:setBundle basename="properties.labels.party.GmEducationTable"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UFT-8">
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
</head>
<body leftmargin="20" topmargin="10">
	<% 
		String formName = request.getParameter("FORMNAME");
		String listName = request.getParameter("LISTNAME");
		String displayType = request.getParameter("TYPE");
		String tableName = "requestScope." + formName + "." + listName;
	%>
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<display:table name="<%=tableName%>" class="its" id="currentRowObject" 
					decorator="com.globus.displaytag.beans.GmCommonSetupWrapper" > 
					<% if(displayType.equals("Setup")) { %>
						<display:column property="ID" title=""  class="aligncenter"/>
						<fmtEducationTable:message key="LBL_LEVEL" var="varLevel"/><display:column property="EDUCATIONLEVEL" title="${varLevel}" class="aligncenter"/> 
						<fmtEducationTable:message key="LBL_INSTITUTENAME" var="varInstituteName"/><display:column property="INSTITUTENAME" title="${varInstituteName}" class="aligncenter" sortable="true"  />
						<fmtEducationTable:message key="LBL_CLASSYEAR" var="varClassYear"/><display:column property="CLASSYEAR" title="${varClassYear}" sortable="true" class="aligncenter" />
						<fmtEducationTable:message key="LBL_COURSE" var="varCourse"/><display:column property="COURSE" title="${varCourse}" sortable="true" class="aligncenter" />
						<fmtEducationTable:message key="LBL_DESIGNATION" var="varDesignation"/><display:column property="DESIGNATION" title="${varDesignation}" sortable="true" class="aligncenter" />
					<% } else { %>
						<display:column property="EDUCATIONLEVEL" title=""/> 
						<fmtEducationTable:message key="LBL_INSTITUTENAME" var="varInstituteName"/><display:column property="INSTITUTENAME" title="${varInstituteName}" class="aligncenter"/>
						<fmtEducationTable:message key="LBL_CLASSYEAR" var="varClassYear"/><display:column property="CLASSYEAR" title="${varClassYear}" sortable="false" class="aligncenter" />
						<fmtEducationTable:message key="LBL_COURSE" var="varCourse"/><display:column property="COURSE" title="${varCourse}" sortable="false" class="aligncenter" />
						<fmtEducationTable:message key="LBL_DESIGNATION" var="varDesignation"/><display:column property="DESIGNATION" title="${varDesignation}" sortable="false" class="aligncenter" />
					<% } %>
				</display:table>
			</td>
		</tr>
	</table>
</body>
</html>