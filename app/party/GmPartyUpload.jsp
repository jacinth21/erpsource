
<%
	/**********************************************************************************
	 * File		 		: GmPartyUpload.jsp
	 * Desc		 		: Upload - Upload page
	 * Version	 		: 1.0
	 * author			: Tarika Chandure
	 ************************************************************************************/
%>
<!-- party\GmPartyUpload.jsp -->
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtPartyUpload" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartyUpload:setLocale value="<%=strLocale%>"/>
<fmtPartyUpload:setBundle basename="properties.labels.party.GmPartyUpload"/>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<html>
<%
   // Setting up form values
	String strAction = GmCommonClass.parseNull((String) request.getAttribute("hAction"));;		 	
	String strPartyList = GmCommonClass.parseNull((String) request.getAttribute("strPartyList"));		
	String partyId = GmCommonClass.parseNull((String) request.getAttribute("partyId"));
	partyId = partyId == "" ? GmCommonClass.parseNull((String) request.getParameter("partyId")) : partyId;
	
	//Setting up List box values
	ArrayList alPartyList = GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("alPartyList"));
	ArrayList alUploadinfo = GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("alUploadinfo"));

%>
<head>
<title>Upload File</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
</head>
<BODY>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>
function fnUpReload(){
	document.upform.hAction.value='reload';
	document.upform.submit();
}

function upload()
  {
    if(document.upform.uploadfile.value == "")
    { 
	      alert("Select a file to upload");      
	      return false;
    }
    else{
	   document.upform.hAction.value='upload';
	   document.upform.submit();
    }
    toggledisplay();
  }

function fnOpenForm(Id)
{
	windowOpener("/GmCommonFileOpenServlet?sId="+Id+"&uploadString=PARTYUPLOADHOME","OpenDoc","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");	
}
</script>
<form method="post" name="upform" enctype="multipart/form-data" action="/GmPartyUploadServlet">

<input type="hidden" name="hAction" value"<%=strAction%>"> 
<input type="hidden" name="sId"> 
<input type="hidden" name="partyId" value="<%=partyId %>"> 

<table border="0" class="DtTable698" cellspacing="0" cellpadding="0">
	<tr height="24">
		<td class="RightTableCaption" width="50%" align="right"><fmtPartyUpload:message key="LBL_FILE_TYPE"/>:&nbsp;</td>
		<td width="50%" align="left">&nbsp;
		<!-- Custom tag lib code modified for JBOSS migration changes -->
			<gmjsp:dropdown controlName="strPartyList" seletedValue="<%=strPartyList %>"
			value="<%=alPartyList%>" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"
			onChange="fnUpReload(this.form)" />
		</td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>
	<%
		if ((strAction.equals("reload") || strAction.equals("upload")) && !strPartyList.equals("")) {
	%>
	<tr>
		<td colspan="2" width="100%" height="100" valign="top">
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
			<div align="center"><jsp:include page="/common/GmUpload.jsp"/></div>
		</td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>
	<tr>
		<td colspan="2" >
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td class="TableCaption" colspan="4" align="left">&nbsp;<fmtPartyUpload:message key="LBL_PREVIOUS_UPLOAD"/></td>
				</tr>
				<tr><td colspan="4" class="LLine" height="1"></td></tr>
				<tr>
					<td class="RightTableCaption" height="20" align="center">&nbsp;&nbsp;</td>
					<td class="RightTableCaption">&nbsp;<fmtPartyUpload:message key="LBL_FILE_NAME"/></td>
					<td class="RightTableCaption">&nbsp;<fmtPartyUpload:message key="LBL_UPLOAD_DATE"/></td>
					<td class="RightTableCaption">&nbsp;<fmtPartyUpload:message key="LBL_UPLOADED_BY"/></td>
				</tr>
				<tr><td colspan="4" class="LLine" height="1"></td></tr>
				<%
					for (int i = 0; i < alUploadinfo.size(); i++) {
						HashMap hmLoop = (HashMap) alUploadinfo.get(i);
						String fileName = (String)hmLoop.get("NAME");
						String source = "";
						if(fileName.endsWith(".pdf"))
							source = "<%=strImagePath%>/pdf_icon.jpg";
						else if(fileName.endsWith(".doc") || fileName.endsWith(".DOC"))
							source = <%=strImagePath%>/word_icon.jpg";
						else if(fileName.endsWith(".rtf") || fileName.endsWith(".RTF"))
							source = "<%=strImagePath%>/word_icon.jpg";
						else if(fileName.endsWith(".jpg") || fileName.endsWith(".JPG") || fileName.endsWith(".bmp"))
							source = "<%=strImagePath%>/jpg_icon.jpg";
						
						
				%>
				<tr>
					<td align="center" height="20" style="width: 45">
						<a href="javascript:fnOpenForm(<%=hmLoop.get("ID")%>);">
							<img border="0" src="<%=source%>" height="20px" width="20px">
						</a>
					</td>
					<td class="RightText">&nbsp;<%=hmLoop.get("NAME")%></td>
					<td class="RightText">&nbsp;<%=hmLoop.get("CDATE")%></td>
					<td class="RightText">&nbsp;<%=hmLoop.get("UNAME")%></td>
				</tr>
				<tr><td colspan="4" class="LLine" height="1"></td></tr>
				<%
					}
				%>
			</table>
		</td>
	</tr>
<%} %>
</table>
</form>
</body>
</html>
