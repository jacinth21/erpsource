<!--\party\GmInternalCommentsSetup.jsp  -->
<html>
<head>
<TITLE> Globus Medical: Internal Comments </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtInternalCommentsSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtInternalCommentsSetup:setLocale value="<%=strLocale%>"/>
<fmtInternalCommentsSetup:setBundle basename="properties.labels.party.GmInternalCommentsSetup"/>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
</head>
<body leftmargin="20" topmargin="10">
<html:form action="/gmInternalCommentsSetup.do" >
<html:hidden property="partyId"  name="frmPartySetup"/>

<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
		<td bgcolor="white" class="RightTableCaption" align="left" height="30" >
		<%--<%=strDisclaimer%> --%>
		&nbsp;
<fmtInternalCommentsSetup:message key="LBL_COMMENTS_SECTION"/>
	</td>
		</tr>
		<tr>
			<td colspan="2" height="1" bgcolor="#666666"></td>
		</tr>
		<tr>
			<td colspan="2" width="698" height="100" valign="top">
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="3" class="Line"></td>
				</tr>
				<tr>
					<td colspan="3">
						<jsp:include page="/common/GmIncludeLog.jsp">
							<jsp:param name="FORMNAME" value="frmPartySetup" />
							<jsp:param name="ALNAME" value="alLogReasons" />
							<jsp:param name="LogMode" value="Edit" />
						</jsp:include>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="center" height="30">&nbsp;
	<fmtInternalCommentsSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button buttonType="Save"  name="Submit" value="${varSubmit}" gmClass="button" onClick="fnInternalCommentsSubmit(this.form);"/>
	
				</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>