<%
/**********************************************************************************
 * File		 		: GmAchievementSetup.jsp
 * Desc		 		: Setting up Awards for party
 * Version	 		: 1.0
 * author			: Brinal G
************************************************************************************/
%>
<!--\party\GmAchievementSetup.jsp  -->
<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtAchievementSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtAchievementSetup:setLocale value="<%=strLocale%>"/>
<fmtAchievementSetup:setBundle basename="properties.labels.party.GmAchievementSetup"/>
<HTML>
<HEAD>
<TITLE> Globus Medical: Address Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
</HEAD>
<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmAchievementSetup.do" >
<html:hidden property="strOpt"  name="frmAchievementSetup"/>
<html:hidden property="hachievementID"  name="frmAchievementSetup"/>
<html:hidden property="partyId"  name="frmAchievementSetup"/>

<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VDACH"/>
<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr><td colspan="3" height="15"></td></tr>
				<tr><td colspan="3" class="LLine" height="1"></td></tr>
                <tr class="Shade">
	                <td width="30%" class="RightTableCaption" align="right"><font color="red">*</font>&nbsp;<fmtAchievementSetup:message key="LBL_TYPE"/>:&nbsp;</td> 
                    <td width="40%"><!-- Custom tag lib code modified for JBOSS migration changes -->
						&nbsp;<gmjsp:dropdown controlName="type" SFFormName="frmAchievementSetup" SFSeletedValue="type"
						SFValue="alType" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
                    </td>
                    <td width="30%"></td>
               	</tr> 
               	<tr><td colspan="3" class="LLine" height="1"></td></tr>
                <tr>
	                <td width="30%" class="RightTableCaption" align="right"><font color="red">*</font>&nbsp;<fmtAchievementSetup:message key="LBL_TITLE"/>:&nbsp;</td> 
                    <td width="40%">&nbsp;<html:text property="title" size="56" name="frmAchievementSetup" 
                    		onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
					</td>
					<td width="30%"></td>
               	</tr>                    
                <tr><td colspan="3" class="LLine" height="1"></td></tr>
                <tr class="Shade">
                	<td width="30%" class="RightTableCaption" align="right"><fmtAchievementSetup:message key="LBL_DESCRIPTION"/>:&nbsp;</td>
                    <td width="40%">&nbsp;<html:textarea property="description" cols="55" rows ="3" onfocus="changeBgColor(this,'#AACCE8');" 
                    styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"></html:textarea></td>
                    <td width="30%"></td>
                </tr>                    
                <tr><td colspan="3" class="LLine" height="1"></td></tr>
                <tr>
                	<td width="30%" class="RightTableCaption" align="right"><fmtAchievementSetup:message key="LBL_MONTH"/>:&nbsp;</td>
                	<!-- Custom tag lib code modified for JBOSS migration changes -->
                    <td width="40%">&nbsp;<gmjsp:dropdown controlName="month" SFFormName="frmAchievementSetup" SFSeletedValue="month"
						SFValue="alMonth" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />                        
                    	<b><fmtAchievementSetup:message key="LBL_YEAR"/>:&nbsp;</b>
                       	<html:text property="year" maxlength="4" size="4" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/>
                    </td> 
                    <td width="30%"></td>
                </tr>
				<tr>
					<td colspan="3" class="LLine" height="1"></td></tr>
				<tr class="Shade">
					<td width="30%" align="right" class="RightTableCaption"><fmtAchievementSetup:message key="LBL_CO_INVESTIGATIORS"/>:&nbsp;</td>
					<td width="40%" height="50">
						&nbsp;<html:textarea property="coInvestigators" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" 
						onblur="changeBgColor(this,'#ffffff');" rows="3" cols="55"/></td> 
					<td width="30%"><fmtAchievementSetup:message key="LBL_SEPARATE_BY_SEMICOLON"/></td>
				</tr>                
                <tr><td colspan="3" class="LLine" height="1"></td></tr>
                <tr><td colspan="3" height="15"></td></tr>
                <tr>                        
                	<td colspan="3" align="center" height="24"><!-- Struts tag lib code modified for JBOSS migration changes -->
                        <fmtAchievementSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button  name="Submit" value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnAchieveSubmit(this.form);"/>&nbsp;&nbsp;
						<fmtAchievementSetup:message key="BTN_RESET" var="varReset"/><gmjsp:button name="resetBtn" value="${varReset}" gmClass="button" buttonType="Save" onClick="fnAchieveReset(this.form);"/>&nbsp;&nbsp;
						<fmtAchievementSetup:message key="BTN_VOID" var="varVoid"/><gmjsp:button name="voidBtn" value="${varVoid}" buttonType="Save" gmClass="button" onClick="fnAchieveVoid(this.form);"/>                    
                    </td>
                </tr>
                <tr><td colspan="3" height="15"></td></tr>
                <tr><td colspan="3" class="LLine" height="1"></td></tr>
             </table>
           </td>
	</tr>
    <tr>
    	<td colspan="3">
    	<!-- Changed for JBOSS migration -->
        	<jsp:include page="/gmAchievementSetup.do?method=loadAchievementReport" flush="true">
				<jsp:param name="TYPE" value="Setup"/>															
			</jsp:include>
		</td>
	</tr>                    
</table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>

