 <%
/**********************************************************************************
 * File		 		: GmPartyCommonSearch.jsp
 * Desc		 		: This file is used to render the party common search page
 * Version	 		: 1.0
 * author			: Satyajit Thadeshwar
************************************************************************************/
%>
<!--\party\GmPartyCommonSearch.jsp  -->
<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtPartyCommonSearch" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtPartyCommonSearch:setLocale value="<%=strLocale%>"/>
<fmtPartyCommonSearch:setBundle basename="properties.labels.party.GmPartyCommonSearch"/>
<bean:define id="alTabs" name="frmPartyCommonSearch" property="alTabs" scope="request" type="java.util.ArrayList"></bean:define>
<%
String strPartyJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PARTY");
String strWikiTitle = GmCommonClass.getWikiTitle("PARTY_SEARCH");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Party Common Search</title>
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="JavaScript" src="<%=strPartyJsPath%>/GmParty.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusParty.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
</head>
<body leftmargin="20" topmargin="10" onload="fnHideDivTags();">
<html:form action="/gmPartyCommonSearch.do">
<html:hidden property="partyType"/>
<table class="DtTable700" cellspacing="0" cellpadding="0">
 	<tr>
		<td height="25" class="RightDashBoardHeader">&nbsp;<bean:write name="frmPartyCommonSearch" property="title"/>&nbsp;<fmtPartyCommonSearch:message key="LBL_SEARCH"/></td>
		<td align="right" class=RightDashBoardHeader > 	
		<fmtPartyCommonSearch:message key="IMG_ALT_HELP" var="varHelp"/>
			<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
		</td>	
	</tr>
	<tr>
		<td colspan="2">
			<table class="DtTable700" cellspacing="5" cellpadding="5">
 				<tr><td height="10" colspan="2"></td></tr>
 				<tr>
					<td class="RightTableCaption">
						&nbsp;&nbsp;<fmtPartyCommonSearch:message key="LBL_FIRST_NAME"/>:&nbsp;
						<input type="text" id="10011" name="filtertext" 
						onfocus="changeBgColor(this,'#AACCE8');" class="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
					</td>
					<td class="RightTableCaption">						
						&nbsp;<fmtPartyCommonSearch:message key="LBL_LAST_NAME"/>:&nbsp;
						<input type="text" id="10012" name="filtertext" 
						onfocus="changeBgColor(this,'#AACCE8');" class="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
					</td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr><td height="10" colspan="2"></td></tr>
 				<logic:iterate id="tabs" name="frmPartyCommonSearch" property="alTabs">	
					<tr>
						<td colspan="2">
							<table class="SearchScreenTable" border="0" cellspacing="0" cellpadding="0">
								<tr class="SearchScreenTableHeader">
									<td align="left">&nbsp;
										<a href="javascript:toggleDisplay('<bean:write name="tabs" property="TAB"/>contentarea');">
										<bean:write name="tabs" property="TABLABEL"/></a>
									</td>
								</tr>
								<tr id="tr<bean:write name="tabs" property="TAB"/>contentarea">
									<td>
										<div id="<bean:write name="tabs" property="TAB"/>">
											<a href="/gmPartyCommonSearch.do?strOpt=filterpage&searchGroup=<bean:write name="tabs" property="TAB"/>" 
											title="<bean:write name="tabs" property="TAB"/>" rel="<bean:write name="tabs" property="TAB"/>contentarea"></a>
  										</div>
  										<div id="<bean:write name="tabs" property="TAB"/>contentarea"></div>
										<script>
											new ddajaxtabs("<bean:write name="tabs" property="TAB"/>", "<bean:write name="tabs" property="TAB"/>contentarea").init();
										</script>  							
									</td>
								</tr>				
							</table>
						</td>
					</tr>
					<tr><td height="10" colspan="2"></td></tr>
				</logic:iterate>
				<tr>
					<td align="center" colspan="2">
						<fmtPartyCommonSearch:message key="BTN_SEARCH" var="varSearch"/><gmjsp:button value="${varSearch}" buttonType="Load" gmClass="button" onClick="fnSearch();"/>
					</td>
				</tr>
				<tr><td height="10" colspan="2"></td></tr>				
			</table>
		</td>
	</tr>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>