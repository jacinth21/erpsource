<%
/**********************************************************************************
 * File		 		: GmSetPrioritization.jsp
 * Desc		 		: Set Prioritization Buffer Setup
 * Version	 		: 1.0
 * author			: arajan
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@page import="java.util.Date"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<!-- operations\GmPartRedesignation.jsp -->
<%@ taglib prefix="fmtPartRedesignation" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%> 
<fmtPartRedesignation:setLocale value="<%=strLocale%>"/>
<fmtPartRedesignation:setBundle basename="properties.labels.operations.GmPartRedesignation"/>

<% 
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
%>

<bean:define id="gridData" name="frmPartRedesignation" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="partId" name="frmPartRedesignation" property="partId" type="java.lang.String"> </bean:define>
<bean:define id="donorId" name="frmPartRedesignation" property="donorId" type="java.lang.String"> </bean:define>
<%-- <bean:define id="alPartsList" name="frmPartRedesignation" property="alPartsList" type="java.util.ArrayList"> </bean:define> --%>
<bean:define id="strTxnIds" name="frmPartRedesignation" property="strTxnIds" type="java.lang.String"> </bean:define>

<%
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
String strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("PART_REDESIGNATION"));
HashMap hFormVal = null;
HashMap hFormPartVal = null;
HashMap hFormToPartVal = null;
ArrayList alCartList = new ArrayList();
ArrayList alFromPart = new ArrayList();
alCartList = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("CARTDATA"));
HashMap hToVal = null;
String strApplDateFmt = GmCommonClass.parseNull((String)strGCompDateFmt);
String struCurrentdate = GmCommonClass.parseNull((String) GmCalenderOperations.getCurrentDate(strApplDateFmt));

   //The following code added for passing the company info to the child js fnFilterLoad function
	String strCompanyInfo ="{\"cmpid\":\""+GmCommonClass.parseNull((String)gmDataStoreVO.getCmpid())+"\",\"partyid\":\""+GmCommonClass.parseNull((String)gmDataStoreVO.getPartyid())+"\",\"plantid\":\""+GmCommonClass.parseNull((String)gmDataStoreVO.getPlantid())+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);

%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE> Globus Medical: Set Prioritization Buffer </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCombo/dhtmlxcombo.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmPartRedesignation.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script> 
 <script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<style type="text/css">
body{
    margin-left:20px;
    margin-top: 10px;
}

#currDtls, #currPartDtls, #currDonorDtls  {
    display: inline-block;
    margin: 0px 10px 0px 0px;
    vertical-align: top;
}

#currDtls div, #currPartDtls div, #currDonorDtls div {
	display: inline-block;
	border: 1px solid #ccc;
    border-radius: 10px;
    padding: 2px 5px 2px 5px;
    background-color: #EEE;
    box-shadow: 2px 2px 1px #030303;
}

#currDtls div {
	background-color: #eee7f8;
}

#currDonorDtls div {
	background-color: #e1effa;
}

#currPartDtls div {
	background-color: #fff8e7;
}

li {
	/* style='list-style-type: none; vertical-align: bottom;' */
	list-style-type: none;
	vertical-align: bottom;
	padding-bottom: 0.5%;
	padding-top: 0.5%;
}
.hdrcell select {
	font-weight: normal;
}

.hdrcell input {
	font-weight: normal;
}

</style>
<script>
var companyInfoObj = '<%=strCompanyInfo%>';
var objGridData = '<%=gridData%>';
var transID =  '<%=strTxnIds%>';

var cartSize = '<%=alCartList.size()%>';
var currdate = '<%=struCurrentdate%>';

</script>

<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
	<html:form action="/gmPartredesignation.do?method=fetchPartRedesignation">
	<html:hidden property="strOpt" name="frmPartRedesignation"/>
	<html:hidden property="hInputString" name="frmPartRedesignation"/>
	<html:hidden property="hctlNumInvString" name="frmPartRedesignation"/>
	<html:hidden property="hfinalComments" name="frmPartRedesignation"/>
	<html:hidden property="hScannedLotStr" name="frmPartRedesignation"/>
	<html:hidden property="hScannedPartStr" name="frmPartRedesignation"/>
	<html:hidden property="hScannedDonorStr" name="frmPartRedesignation"/>
	
		<table class="DtTable1100" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="25" class="RightDashBoardHeader">&nbsp;<fmtPartRedesignation:message key="LBL_INITIATE"/></td>
					<fmtPartRedesignation:message key="LBL_HELP" var="varHelp"/>
					<td align="right" class="RightDashBoardHeader"><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
						title='${varHelp}' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>
				</tr>					
					<tr>
						<td colspan="2">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr><td class="RightTableCaption" HEIGHT="30" align="right" width="10%">
								<fmtPartRedesignation:message key="LBL_PART" var="varPart"/>
								<gmjsp:label type="RegularText" SFLblControlName="${varPart}" td="false" /></td>								
								<td width="15%">&nbsp;<html:text property="partId" styleClass="InputArea" name="frmPartRedesignation" value='<%=partId %>' onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');fnGeneratePartStr(this);" tabindex="1" size="15"/>
								</td>
								<td class="RightTableCaption" HEIGHT="30" align="right" width="10%">
								<fmtPartRedesignation:message key="LBL_DONOR" var="varDonor"/>
								<gmjsp:label type="RegularText" SFLblControlName="${varDonor}" td="false" /></td>
								<td width="15%">&nbsp;<html:text property="donorId" styleClass="InputArea" name="frmPartRedesignation" value='<%=donorId %>' onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');fnGenerateDonorStr(this);" tabindex="2" size="15"/>
								</td>
								<td class="RightTableCaption" HEIGHT="30" align="right" width="15%"><fmtPartRedesignation:message key="LBL_SCAN_LOT"/></td>
								<td width="20%">&nbsp;<html:text property="controlId" styleClass="InputArea" name="frmPartRedesignation" value='' onfocus="changeBgColor(this,'#AACCE8');" onkeypress="javascript:fnAddLotCode(this);" style="text-transform:uppercase;" onblur="changeBgColor(this,'#ffffff');" tabindex="3" size="25"/>
								</td>
																				
								<td width="10%"><fmtPartRedesignation:message key="BTN_LOAD" var="varLoad"/><gmjsp:button name="Btn_Load" value="${varLoad}" gmClass="button" style="width: 5em; height: 23px; font-family: Trebuchet MS, arial, sans-serif;font-size : 9pt" onClick="javascript:fnLoad(this);" tabindex="4" buttonType="Load" /></td>
								</tr>
								<tr><td class="LLine" height="1" colspan="7"></td></tr>	
								<tr id="partTr" style="display: none;"><td colspan="7" height="40px">
									&nbsp; <b><fmtPartRedesignation:message key="LBL_PART"/></b> <div id="partDiv">
									<ul id="ulPartDtls"></ul>
									</div>
								</td></tr>
								<tr><td class="LLine" height="1" colspan="7"></td></tr>
								<tr id="donorTr" style="display: none;"><td colspan="7" height="40px">
									&nbsp;<b><fmtPartRedesignation:message key="LBL_DONOR"/></b> <div id="donorDiv">
									<ul id="ulDonorDtls"></ul>
									</div>
								</td></tr>
								<tr><td class="LLine" height="1" colspan="7"></td></tr>
								<tr id="scannedLotTr" style="display: none;"><td colspan="7" height="40px">
									&nbsp;<b><fmtPartRedesignation:message key="LBL_LOT"/></b> <div id="scannedLotDiv">
									<ul id="ulLotDtls"></ul>
									</div>
								</td></tr>
							</table>
						</td>
					</tr>	
					
         	<%if(gridData.indexOf("cell") != -1) {%> 
	
          <tr><td class="LLine" colspan="5"></td></tr> 
         <tr>
            <td class="RightTableCaption" align="right" height="25"><fmtPartRedesignation:message key="LBL_STAGE_DATE" var="varDt"/>
            <gmjsp:label type="MandatoryText" SFLblControlName="${varDt} :" td="false"/></td>
            <td>&nbsp;<gmjsp:calendar SFFormName="<%=strFormName%>" controlName="stgDt" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
           onBlur="changeBgColor(this,'#ffffff');" tabIndex="1"/></td>
        </tr>
					
					 	<tr>
							<td colspan="2"><div  id="dataGridDiv" height="450px" width="1098px"></div></td>
						</tr>
						<tr><td class="LLine" height="1" colspan="2"></td></tr>	
						<tr>
						<td colspan="2"> 
							<jsp:include page="/common/GmIncludeLog.jsp" >
								<jsp:param name="FORMNAME" value="frmPartRedesignation" />
								<jsp:param name="ALNAME" value="alLogReasons" />
								<jsp:param name="LogMode" value="Edit" />
							</jsp:include>
						</td>
						</tr>
						<logic:empty name="frmPartRedesignation" property="strTxnIds">
							<tr>
								<td align="center" height="30px" colspan="2"><fmtPartRedesignation:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button name="Btn_Submit" value="${varSubmit}" gmClass="button" onClick="javascript:fnSubmit(this);" tabindex="4" buttonType="save" />&nbsp;
									<fmtPartRedesignation:message key="BTN_RESET" var="varReset"/><gmjsp:button name="Btn_Reset" value="${varReset}" gmClass="button" onClick="javascript:fnReset(this);" tabindex="5" buttonType="Load" buttonTag="True"/></td>
								</tr>
						</logic:empty>
					<%}else  if(!gridData.equals("")){%>
						<tr class="Shade"><td colspan="2" align="center" class="RightText" ><fmtPartRedesignation:message key="LBL_NTNG_FOUND"/></td></tr>
					<%} %>
					<logic:notEmpty name="frmPartRedesignation" property="strTxnIds" >
						<tr height="25">
							<td height="25" colspan="4" align="center" class="">
								<b><font style="color: blue">The transaction  <%=strTxnIds%> <fmtPartRedesignation:message key="LBL_CREATED_SUCCESSFULLY"/> .</font></b>
							</td>
						</tr>
						<tr>
							<td align="center" height="30px" colspan="2"><fmtPartRedesignation:message key="BTN_PIC" var="varPic"/><gmjsp:button name="Btn_PicSlip" value="${varPic}" gmClass="button" onClick="javascript:fnViewPicSlip();" tabindex="4" buttonType="save" />&nbsp;</td>
						</tr>						
					</logic:notEmpty>					
			</table>

	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>