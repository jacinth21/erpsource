
<!-- \operations\GmPOHistory.jsp -->
 <%@page import="java.util.Date"%>
<%
/**********************************************************************************
 * File		 		: GmPOHistory.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>


<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtPOHistory" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPOHistory:setLocale value="<%=strLocale%>"/>
<fmtPOHistory:setBundle basename="properties.labels.operations.GmPOHistory"/>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%
try {
	response.setCharacterEncoding("UTF-8");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	ArrayList alList = new ArrayList();
	int intSize = 0;
	if (hmReturn != null)
	{
		alList = (ArrayList)hmReturn.get("POREPORT");
		intSize = alList.size();
	}
	
	String strShade = "";

	String strPOId = "";
	String strPartNum = "";
	String strPartDesc = "";

	String strQtyPlaced = "";
	String strQtyRec = "";
	String strControl = "";
	String strDate = "";
	String strPackSlip = "";
	String strWorkId = "";
	String strQtyShelf = "";
	String strVendor = "";
	String strVendorId ="";
	String strQtyStocked = "";
	String strQtyPend = "";
	String strVendorCurrency = "";
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: PO History Report </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>

function fnCallPO(po,vid)
{
	windowOpener("/GmPOServlet?hAction=ViewPO&hPOId="+po+"&hVenId="+vid,"PO","resizable=yes,scrollbar=yes,top=150,left=50,width=750,height=600");
}

</script>

</HEAD>

<BODY leftmargin="15" topmargin="10">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmPOReportServlet">

	<table border="0" width="550" cellspacing="0" cellpadding="0">
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
		<tr>
			<td class="Line" width="1" rowspan="5"></td>
			<td height="25" colspan="2"  class="RightDashBoardHeader"><fmtPOHistory:message key="LBL_PO_HISTORY_BY_PART_NUMBER"/></td>
			<td class="Line" width="1" rowspan="5"></td>
		</tr>
		<tr><td colspan="2" class="Line" height="1"></td></tr>
		<tr>
			<td width="548" height="70" valign="top" colspan="2">
				<TABLE cellpadding="1" cellspacing="0" border="0" width="100%">
					<TR class="Shade">
						<TD class="RightTableCaption" align="center" width="90"><fmtPOHistory:message key="LBL_PURCHASE"/><BR><fmtPOHistory:message key="LBL_ORDER"/></TD>
						<TD class="RightTableCaption" align="center" width="90"><fmtPOHistory:message key="LBL_WORK_ORDER"/></TD>
						<TD class="RightTableCaption" align="center" width="80"><fmtPOHistory:message key="LBL_VENDOR"/></TD>
						<TD class="RightTableCaption" align="center" width="50"><fmtPOHistory:message key="LBL_QTY"/><br><fmtPOHistory:message key="LBL_PLACED"/></TD>
						<TD class="RightTableCaption" align="center" width="50"><fmtPOHistory:message key="LBL_QTY"/><br><fmtPOHistory:message key="LBL_STOCKED"/></TD>
						<TD class="RightTableCaption" align="center" width="50"><fmtPOHistory:message key="LBL_QTY"/><br><fmtPOHistory:message key="LBL_IN_WIP"/></TD>
						<TD class="RightTableCaption" align="center" width="70"><fmtPOHistory:message key="LBL_PO_DATE"/></TD>
						<TD class="RightTableCaption" align="center" width="10"></TD>
						<TD class="RightTableCaption" align="center" width="50"><fmtPOHistory:message key="LBL_COST"/></TD>
					</TR>
					<tr><td colspan="10" height="1" bgcolor="#666666"></td></tr>
<%
					if (intSize > 0)
					{
						HashMap hmLoop = new HashMap();

						for (int i = 0;i < intSize ;i++ )
						{
							hmLoop = (HashMap)alList.get(i);

							strPOId = GmCommonClass.parseNull((String)hmLoop.get("ID"));
							strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
							strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
							strQtyPlaced = GmCommonClass.parseNull((String)hmLoop.get("QTYORD"));
							strQtyRec = GmCommonClass.parseNull((String)hmLoop.get("QTYREC"));
							strQtyShelf = GmCommonClass.parseNull((String)hmLoop.get("WIP"));
							strDate = GmCommonClass.getStringFromDate((Date)hmLoop.get("PODATE"),strGCompDateFmt);
							strVendorCurrency = GmCommonClass.parseNull((String)hmLoop.get("VENDCURR"));
							strPackSlip = GmCommonClass.parseNull((String)hmLoop.get("CPRICE"));
							strWorkId = GmCommonClass.parseNull((String)hmLoop.get("WORKID"));
							strVendor = GmCommonClass.parseNull((String)hmLoop.get("VENDNM"));
							strVendorId = GmCommonClass.parseNull((String)hmLoop.get("VID"));
							strQtyStocked = GmCommonClass.parseZero((String)hmLoop.get("STOCK"));
							strQtyPend = GmCommonClass.parseNull((String)hmLoop.get("PEND"));
%>
								<TR>
									<td height="20" nowrap><a class="RightText" href="javascript:fnCallPO('<%=strPOId%>','<%=strVendorId%>')"><%=strPOId%></td>
									<td class="RightText" align="center"><%=strWorkId%></td>
									<td class="RightText" align="center">&nbsp;<%=strVendor%></td>
									<td class="RightText" align="center">&nbsp;<%=strQtyPlaced%></td>
									<td class="RightText" align="center">&nbsp;<%=strQtyStocked%></td>
									<td class="RightText" align="center">&nbsp;<%=strQtyShelf%></td>
									<td class="RightText" align="center"><%=strDate%>&nbsp;</td>
									<td class="RightText" align="center"><%=strVendorCurrency%>&nbsp;</td>
									<td class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strPackSlip)%>&nbsp;</td>
								<TR>
								<tr><td colspan="10" height="1" bgcolor="#eeeeee"></td></tr>
<%					
						} // End of For
					}else{
%>
								<tr>
									<td colspan="10" align="center" class="RightTextRed"><fmtPOHistory:message key="LBL_NO_PO_S_HAVE_BEEN_PLACED_THIS_PART_NUMBER"/>!</td>
								</tr>
<%
					}
%>
				</TABLE>
			<BR>
			<fmtPOHistory:message key="LBL_CLOSE" var="varClose"/>
			<center><gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close();" buttonType="Load" /></center>
			</TD>
		</tr>
		<tr><td colspan="3" class="Line" height="1"></td></tr>
    </table>

</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
