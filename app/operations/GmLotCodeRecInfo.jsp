<%
/**********************************************************************************
 * File		 		: GmLotCodeRecInfo.jsp
 * Desc		 		: This Jsp is for the receiving information section
 * Version	 		: 1.0
 * author			: arajan
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtLotRecInfo" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
  <!-- \operations\operations\GmLotCodeRecInfo.jsp--> 
<fmtLotRecInfo:setLocale value="<%=strLocale%>"/>
<fmtLotRecInfo:setBundle basename="properties.labels.operations.GmLotCodeReport"/>
<% 
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
%>
<HTML>
<HEAD>

<script>
</script>
<!-- style added for PC-3659 support Edge alignment -->
 <style>
	#RecDataGrid .objbox{
		 height: auto !important;
	}
	#RecDataGrid table
	{
	width:100% !important;
	margin-right:0 !important;
	padding-right:0 !important; 
	}
	#RecDataGrid
	{
		height: auto !important;
		width: 100% !important; 
		margin:	0 !important;
	}
</style> 
<BODY leftmargin="20" topmargin="10">
	
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr><td class="Line" colspan="5"></td></tr> 
			<tr>
			<!-- This JSP is for the section "Donor Details" -->
					<td height="24" class="ShadeRightTableCaption" colspan="5">&nbsp;<b><fmtLotRecInfo:message key="TD_LOT_REC_INFO_HEADER"/></b></td>
		    </tr>	
		    <tr id="DivRecGrid">
				<td>
				<div id="RecDataGrid" height="100"></div>
				</td>
			</tr>
			<tr id="DivRecNoDataMessage"><td colspan="3" height="20" align="Center"><fmtLotRecInfo:message key="TD_NO_DATA_FOUND"/></td></tr>
		</table>

	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>