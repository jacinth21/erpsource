<!-- operations\GmLotExpiryReport.jsp -->
<%
	/**********************************************************************************
	  * File		 		: GmLotExpiryReport.jsp
      * Desc		 		: This screen is used to display Lot Expiry Report
      * author			    : Agilan Singaravel
	 ************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@page import="java.util.Date"%> 
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%> 
<%@ page import = "java.util.*,java.io.*"%>
<%@ taglib prefix="fmtLotExpiryReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtLotExpiryReport:setLocale value="<%=strLocale%>"/>
<fmtLotExpiryReport:setBundle basename="properties.labels.operations.GmLotExpiryReport"/>
<bean:define id="strOpt" name="frmLotExpiryReport" property="strOpt" scope="request" type="java.lang.String"></bean:define>
<%
String strOperationsJsPath= GmCommonClass.parseNull(GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS"));
String strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("LOT_EXPIRY_REPORT"));
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Lot Expiry Report</TITLE>
 <link rel="styleSheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css"> 
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmProgress.css">
<script language="JavaScript" src="<%=strJsPath%>/GmProgress.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/dhtmlxlayout.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmLotExpiryReport.js"></script> 
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmLotExpiryPopupReport.js"></script> 
<script type="text/javascript" src="<%=strExtWebPath%>/fusionchart/fusioncharts.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/fusionchart/fusioncharts.maps.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/fusionchart/fusioncharts.usa.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script type="text/javascript">
var gCmpDateFmt = "<%=strGCompDateFmt%>";
</script>

<!--  <style type="text/css" media="all">
@import url("styles/screen.css"); 
 </style>  -->
<style> div.gridbox_dhx_skyblue table.hdr td { text-align: center; vertical-align: middle !important; }
.lineHide {
display:none;
}
 </style> 
  <style>
.box {
  width: 370px;
  border: 1px solid #000;
  margin: auto;
  height: 250px;
  margin-bottom: 5px;
}
.head{
/*  background-color:#2F75B5;  */
  border-bottom: 1px solid #000;
  height: 20px;
}
.headlabel{
    padding-bottom: 15px;
}
.content{
padding-top: 50px;
}
 .objbox{
height:187px;
}
</style> 
</HEAD>
<body leftmargin="15" topmargin="5" onLoad="fnOnload(); ">
 <html:form action="/gmLotExpiryReport.do?method=loadLotExpiryDashboard" > 
 <html:hidden name="frmLotExpiryReport"  property="strOpt"  />  
 <table class="DtTable1000" border="0" cellspacing="0" cellpadding="0">
			<tr>
			  <%if(strOpt.equals("loadSterile")){ %>
				<td colspan="6" height="25" class="RightDashBoardHeader">&nbsp;<fmtLotExpiryReport:message key="LBL_LOT_EXPIRY_REPORT"/> </td>
				<%}else{ %>
				<td colspan="6" height="25" class="RightDashBoardHeader">&nbsp;<fmtLotExpiryReport:message key="LBL_LOT_EXPIRY_REPORT_TISSUE"/> </td>
				<%} %>
				<td height="25" align="right" class=RightDashBoardHeader><fmtLotExpiryReport:message key="IMG_ALT_HELP" var="varHelp"/>				    
				 <img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
             </tr>
             
            <tr > 
				<td class="RightTableCaption"  colspan="3">
				<table><tr><td align="right" style="display:inline-flex;width:92px;"><fmtLotExpiryReport:message key="LBL_PROJECT_LIST"/>:</td>
					<td align="left"> 
		 			<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
								<jsp:param name="CONTROL_NAME" value="projectId" />
								<jsp:param name="METHOD_LOAD" value="loadAllCompanyProjectNameList&searchType=LikeSearch&searchBy=AllProjectId" />
								<jsp:param name="WIDTH" value="280px" />
								<jsp:param name="CSS_CLASS" value="search" />
								<jsp:param name="TAB_INDEX" value="1"/>
								<jsp:param name="CONTROL_NM_VALUE" value="" /> 
								<jsp:param name="CONTROL_ID_VALUE" value="" />
								<jsp:param name="SHOW_DATA" value="100" />
								<jsp:param name="AUTO_RELOAD" value="" />					
					</jsp:include> 	
				</td> </tr></table></td>
				<td class="RightTableCaption" HEIGHT="20" colspan="3">
				<table><tr><td align="right" style="padding-left: 64px;"><fmtLotExpiryReport:message key="LBL_PART_NUM"/>:&nbsp; </td>
				<td class="RightText" id="partNum" > 
				   <html:text property="partNum" size="30" maxlength="20" style="width:100px" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" />&nbsp;&nbsp; 			
				<gmjsp:dropdown controlName="strPartLiteral" SFFormName="frmLotExpiryReport" SFSeletedValue="strPartLiteral"
							SFValue="alPartSearch" codeId = "CODENMALT"  codeName = "CODENM" defaultValue= "[Choose One]" />
				</td></tr></table></td>
				
				<td colspan="3" ><table><tr><td align="right" style="padding-left: 20px;">&nbsp;<input type="checkbox" size="30"  name="Chk_ActiveFl" checked onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" >
			   </td> <td class="RightTableCaption" align="left" HEIGHT="20" ><fmtLotExpiryReport:message key="LBL_EXCLUDE_QUAR"/>&nbsp;</td> 				
				<td align="center" height="20" colspan="1" > 
				<fmtLotExpiryReport:message key="BTN_LOAD" var="varLoad"/>&nbsp;&nbsp;
			    <gmjsp:button value="&nbsp;${varLoad}&nbsp;"gmClass="button" buttonType="Load" onClick="fnLoad();" /> 
			    </td>	</tr></table></td>	
		  </tr>		  
		 <tr><td class="Line" height="1" colspan="8"></td></tr>
		 
	<tr height="30" id="DivRecNothingMessage" style="display:none;">
				<td colspan="8"  class="RightTextBlue"><div align="center"><fmtLotExpiryReport:message key="LBL_NO_DATA" />
					</div></td>
		</tr> 
	<tr class="hline" style="display:none;">
	<td style="padding-left:10px;padding-top:5px;" colspan="3"><div class="box">
		<div class="RightDashBoardHeader head" >&nbsp;<fmtLotExpiryReport:message key="LBL_EXPIRED"/></div>
		<div id="loadExpired" style="width:100%; height:auto;"></div>
		<div id="excelToExpired" class='exportlinks' style="width:97%;  text-align:center;"><fmtLotExpiryReport:message key="LBL_EXPORT_OPTIONS" />&nbsp;:&nbsp;<img src='img/ico_file_excel.png' onclick="fnExcel('expired');" />&nbsp;<a href="#" onclick="fnExcel('expired');">
			<fmtLotExpiryReport:message key="LBL_EXCEL" /> </a>
		</div>
		</div>
	</td>		
	<td style="padding-left:10px;padding-top:5px;" colspan="3"><div class="box">
		<div class="RightDashBoardHeader head">&nbsp;<fmtLotExpiryReport:message key="LBL_EXPIRED_IN_30D"/></div>
		<div id="load30Days" style="width:100%;height:auto;"></div>
		<div id="excelTo30Days" class='exportlinks' style="width:97%;  text-align:center;"><fmtLotExpiryReport:message key="LBL_EXPORT_OPTIONS" />&nbsp;:&nbsp;<img src='img/ico_file_excel.png' onclick="fnExcel('30Days');" />&nbsp;<a href="#" onclick="fnExcel('30Days');">
			<fmtLotExpiryReport:message key="LBL_EXCEL" /> </a>
		</div>
		</div>
	</td>
		<td style="padding-left:10px;padding-top:5px;padding-right:10px;" colspan="3"><div class="box">
		<div class="RightDashBoardHeader head">&nbsp;<fmtLotExpiryReport:message key="LBL_EXPIRED_IN_31to60D"/></div>
		<div id="load60Days" style="width:100%;height:auto;"></div>
		<div id="excelTo60Days" class='exportlinks' style="width:97%;  text-align:center;"><fmtLotExpiryReport:message key="LBL_EXPORT_OPTIONS" />&nbsp;:&nbsp;<img src='img/ico_file_excel.png' onclick="fnExcel('60Days');"/>&nbsp;<a href="#" onclick="fnExcel('60Days');">
			<fmtLotExpiryReport:message key="LBL_EXCEL" /> </a>
		</div>
		</div>
	</td>	
	</tr>		
	<tr class="hline" style="display:none;">
	<td style="padding-left:10px;padding-top:3px;" colspan="3"><div class="box">
		<div class="RightDashBoardHeader head" >&nbsp;<fmtLotExpiryReport:message key="LBL_EXPIRED_IN_61to90D"/></div>
		<div id="load90Days" style="width:100%;height:auto;"></div>
		<div id="excelTo90Days" class='exportlinks' style="width:97%;  text-align:center;"><fmtLotExpiryReport:message key="LBL_EXPORT_OPTIONS" />&nbsp;:&nbsp;<img src='img/ico_file_excel.png' onclick="fnExcel('90Days');" />&nbsp;<a href="#" onclick="fnExcel('90Days');">
			<fmtLotExpiryReport:message key="LBL_EXCEL" /> </a>
		</div>
		</div>
	</td>		
	<td style="padding-left:10px;padding-top:3px;" colspan="3"><div class="box">
		<div class="RightDashBoardHeader head">&nbsp;<fmtLotExpiryReport:message key="LBL_OTHERS"/></div>
		<div id="loadOthers" style="width:100%;height:auto;"></div>
		<div id="excelToOthers" class='exportlinks' style="width:97%; text-align:center;"><fmtLotExpiryReport:message key="LBL_EXPORT_OPTIONS" />&nbsp;:&nbsp;<img src='img/ico_file_excel.png' onclick="fnExcel('others');" />&nbsp;<a href="#" onclick="fnExcel('others');">
			<fmtLotExpiryReport:message key="LBL_EXCEL" /> </a>
		</div>
		</div>
	</td>
		<td style="padding-left:10px;padding-top:3px;padding-right:10px;" colspan="3"><div class="box">
		<div class="RightDashBoardHeader head">&nbsp;<fmtLotExpiryReport:message key="LBL_LOT_EXPIRATION"/></div>
		<div id="chart-container" style="width:100%;height:197px;"></div></div>
	</td>	
	</tr>
       </table>
 </html:form> 
  <%@ include file="/common/GmFooter.inc" %> 
</BODY>
</HTML>