 <%
/**********************************************************************************
 * File		 		: GmWOPrint.jsp
 * Desc		 		: This screen is used for the Work Order - Print Formart
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmJasperReport"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>

<%@ taglib prefix="fmtWOPrint" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\GmWOPrint.jsp -->

<fmtWOPrint:setLocale value="<%=strLocale%>"/>
<fmtWOPrint:setBundle basename="properties.labels.operations.GmWOPrint"/>

<%

try {
	//commented the below lines for PC-194 -Ability to print WO paperwork in Vendor portal

	//GmServlet gm = new GmServlet();
	//gm.checkSession(response,session);
	
	String strPrintDHR = GmCommonClass.parseNull((String)request.getParameter("PrintDHR"));

	HashMap hmReturn = new HashMap();
	if(!strPrintDHR.equals("Y")){
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	} else {
		// get the HashMap from GmPOReceiveServlet  (Print DHR)
		hmReturn = (HashMap)request.getAttribute("hmReturnWODetails");
	}
	HashMap hmWODetails = new HashMap();
	ArrayList alSubWorkOrder = new ArrayList();
	ArrayList alSubDHR = new ArrayList();
	
	String strRptName = (String)request.getAttribute("STRRPTNAME");
	String strHtmlJasperRpt = "";
	String strDesc = "";
	String strMatCertFl = "";
	String strUser = "";
	String strDraw = "";
	String strDrawRev = "";
	String strFooter = "";
	String strVALDFL = "";
	String strValidFLContent = "";
	String strValidateFLContent = "";
	String subReportPath = "";	
	String strSter = "";
	boolean bolSter = false;
	// getting the company locale
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
		
	if (hmReturn != null)
	{	
		subReportPath = GmCommonClass.parseNull((String)hmReturn.get("SUBREPORT_DIR"));
		hmWODetails = (HashMap)hmReturn.get("WODETAILS");
		
		strDesc = GmCommonClass.parseNull((String)hmWODetails.get("PDESC"));
		strMatCertFl = GmCommonClass.parseNull((String)hmWODetails.get("MCFL"));
		strSter = GmCommonClass.parseNull((String)hmWODetails.get("STERFL"));
		strFooter = GmCommonClass.parseNull((String)hmWODetails.get("FOOTER"));
		strVALDFL = GmCommonClass.parseNull((String)hmWODetails.get("VALIDATIONFL"));	
		String strArr[] = strFooter.split("\\^");
		strFooter = strArr[0];
		
		alSubWorkOrder = (ArrayList)hmReturn.get("SUBWODETAILS");
		alSubDHR = (ArrayList)hmReturn.get("SUBDHRDETAILS");
		int intsubAsssize = alSubWorkOrder.size();
		int intsubDHRsize = alSubDHR.size();
		
		hmWODetails.put("INTSUBASSSIZE",alSubWorkOrder.size());
		hmWODetails.put("INTSUBDHRSIZE",alSubDHR.size());
		HashMap hmLoop1 = new HashMap();
		hmLoop1.put("PNUM", "");	
		
		if(intsubAsssize == 0){			
			alSubWorkOrder.add(hmReturn);
		}		
		if(intsubDHRsize == 0){	
			alSubDHR.add(hmReturn);
		}
		if (strSter.equals("1"))
		{
			strMatCertFl = "A";
			hmWODetails.put("VALIDFLCONTENT",strMatCertFl);
			bolSter = true;
		}
		if(strVALDFL.equals("Y")){
			strValidFLContent = "\t- <b>("+GmCommonClass.getString("GMVALIDATIONFL")+")</b>";
			strValidateFLContent = "\t<b>("+GmCommonClass.getString("GMVALIDATIONFLCONTENT")+")</b>";
		}else{
			strValidFLContent = "";
			strValidateFLContent ="";
		}
		strDesc = GmCommonClass.getStringWithTM(strDesc);
		strUser = (String)hmWODetails.get("CUSER");
		strUser = strUser.concat(".gif");
		hmWODetails.put("IMGPATH",strImagePath);
		hmWODetails.put("USERIMG",strUser);
		hmWODetails.put("PDESC",strDesc);
		hmWODetails.put("VALIDFLCONTENT",strValidFLContent);
		hmWODetails.put("VALIDATEFLCONTENT",strValidateFLContent);
		hmWODetails.put("ALSUBASSEMBLYDETAIL",alSubWorkOrder);
		hmWODetails.put("ALSUBDHRDETAIL",alSubDHR);
		hmWODetails.put("RPTFOOTER",strFooter);
		hmWODetails.put("SUBREPORT_DIR",subReportPath);
		hmWODetails.put("LOGOIMAGE", gmResourceBundleBean.getProperty("WO.COMPANY_LOGO"));
		
	}

	int intSize = 0;
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: WO Print</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>

function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmOrder" method="POST" action="<%=strServletPath%>/GmWOServlet">
<input type="hidden" name="hWOId" value="">
<input type="hidden" name="hAction" value="">

	
	 	<tr>	
			<td>
				<%
				GmJasperReport gmJasperReport = new GmJasperReport();			
				gmJasperReport.setRequest(request);
				gmJasperReport.setResponse(response);
				gmJasperReport.setJasperReportName(strRptName);
				gmJasperReport.setHmReportParameters(hmWODetails);
				gmJasperReport.setReportDataList(alSubWorkOrder);
				gmJasperReport.setBlDisplayImage(true);
				gmJasperReport.setBlUseNewSession(true);
				gmJasperReport.setPageHeight(791);  //PMT-9260 : (Page height avoid blank page at end of report);
				strHtmlJasperRpt = gmJasperReport.getHtmlReport();				
				%>	
				<%=strHtmlJasperRpt %>			
			</td>
		</tr>


<div id="button1" align="center">
<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr><td height="2"></td></tr>
		<tr>
			<td>
			<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
					<jsp:include page="/common/GmIncludeDateStamp.jsp" />
			</td>	
		</tr>
	</table>
	</div>
	<%if(!strPrintDHR.equals("Y")){ %>
<BR>
<div id="button1" align="center">
	<table border="0" width="700" cellspacing="0" cellpadding="0" >
		<tr>
			<td align="center" height="30" id="button">
			<fmtWOPrint:message key="BTN_PRINT" var="varPrint"/>
				<gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" onClick="fnPrint();" buttonType="Load" />&nbsp;&nbsp;
				<fmtWOPrint:message key="BTN_CLOSE" var="varClose"/>
				<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close()" buttonType="Load" />&nbsp;&nbsp;
			</td>
		<tr>
	</table>
	</div>
     <%} %>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
