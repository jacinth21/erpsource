 <%
/*********************************************************************************************************
 * File		 		: GmPartLabelHeaderInc.jsp
 * Desc		 		: This screen is used to show the header section of PTRD transaction
 * Version	 		: 1.0
 * author			: Arajan
**********************************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>

<%@page import="java.util.HashMap"%>
<%@ taglib prefix="frmPartLabelHeader" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmItemHeaderInclude.jsp -->
<frmPartLabelHeader:setLocale value="<%=strLocale%>"/>
<frmPartLabelHeader:setBundle basename="properties.labels.operations.GmPartLabel"/>
<%
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
String strApplDateFmt = strGCompDateFmt;
%>

<bean:define id="comments" name="<%=strFormName%>" property="comments" type="java.lang.String"></bean:define>
<bean:define id="statusfl" name="frmPartLabel" property="statusfl" type="java.lang.String"></bean:define>
<bean:define id="createdDate" name="<%=strFormName%>" property="cdate" type="java.util.Date"></bean:define> 
<bean:define id="strAction" name="<%=strFormName%>" property="haction" type="java.lang.String"></bean:define>
<bean:define id="accessFlg" name="<%=strFormName%>" property="accessFlg" type="java.lang.String"></bean:define>
<bean:define id="QCAccessFlg" name="<%=strFormName%>" property="QCAccessFlg" type="java.lang.String"></bean:define>
<bean:define id="QCFlag" name="<%=strFormName%>" property="QC" type="java.lang.String"></bean:define>
<bean:define id="printLabelsFlg" name="<%=strFormName%>" property="labels" type="java.lang.String"></bean:define>
<bean:define id="statusfl" name="<%=strFormName%>" property="statusfl" type="java.lang.String"></bean:define>
<!-- New Bean variable to add staging parameters -->

<bean:define id="preStagingFlg" name="<%=strFormName%>" property="preStagingFlag" type="java.lang.String"></bean:define>
<bean:define id="stagingFlg" name="<%=strFormName%>" property="stagingFlag" type="java.lang.String"> </bean:define> 
<bean:define id="preStaging" name="<%=strFormName%>" property="labelprestage" type="java.lang.String"></bean:define>
<bean:define id="staging" name="<%=strFormName%>" property="labelstage" type="java.lang.String"> </bean:define> 
<%
String strPrintLabelCheck = "";
String stQCCheck = "";
String strPreStagingChk = "";
String strStagingChk = "";
String strPrintLabelDisable = "disabled";
String stQCDisable = "disabled";
String strPreStagingDisable = "disabled";
String strStagingDisable = "disabled";
String strChkDisable = "";

if(accessFlg.equals("Y")){	
	strPrintLabelDisable = "";
}
if(QCAccessFlg.equals("Y")){	
	stQCDisable = "";	
}
if(preStagingFlg.equals("Y")){	
   strPreStagingDisable = "";	
}
if(stagingFlg.equals("Y")){	
	strStagingDisable = "";	
}
if(printLabelsFlg.equals("Y")){	
	strPrintLabelCheck = "Checked";
}
if(QCFlag.equals("Y")){	
	stQCCheck = "Checked";
}
if(preStaging.equals("Y")){	
	strPreStagingChk = "Checked";
}
if(staging.equals("Y")){	
	strStagingChk = "Checked";
}

//when QC is already checked. Print label should not allow to edit who has access only for print label check box
if(accessFlg.equals("Y") && QCAccessFlg.equals("")){
	if(QCFlag.equals("Y")){
        strPrintLabelDisable = "disabled";
 }      
}
//when Print label is unchecked. QC should not allow to edit who has access only for QC check box
if(QCAccessFlg.equals("Y") && accessFlg.equals("")){
	if((printLabelsFlg.equals("")||printLabelsFlg.equals("N"))){
        stQCDisable = "disabled";
   }
}

//when Print label and Qc are unchecked. pre Staging should be disabled
if(printLabelsFlg.equals("") && QCAccessFlg.equals("")){
	strPreStagingDisable = "disabled";
}

//when Print label, Qc and Pre-Staging are unchecked. Staging should be disabled
if(printLabelsFlg.equals("") && QCAccessFlg.equals("") && preStagingFlg.equals("")){
	strStagingDisable = "disabled";
}

// If the Pre-Staging is not checked disabling the Staging checkbox

if(preStaging.equals("")){
	strStagingDisable = "disabled";
}
// Once inspection is done disable the check boxes
if(strAction.equals("PP") && (statusfl.equals("2") || statusfl.equals("3"))){
  strChkDisable = "disabled";
  strStagingDisable = "disabled";
  strPreStagingDisable = "disabled";


}

%>
<script>
var status = '<%=statusfl%>';
var strAction = '<%=strAction%>';

</script>
<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightText" HEIGHT="20" align="right" width="35%">&nbsp;<b><frmPartLabelHeader:message key="LBL_TRANSACTIONID"/>:</b></td>
						<td colspan="3">&nbsp; <bean:write name="<%=strFormName%>" property="txnid" />   </td>
					</tr>
					<tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>	
					<tr class="shade">
						<td class="RightText" HEIGHT="20" align="right" >&nbsp;<b><frmPartLabelHeader:message key="LBL_TRANSACTIONTYPE"/>:</b></td>
						<td colspan="5">&nbsp; <bean:write name="<%=strFormName%>" property="txntypenm" /> </td>
					</tr>
					<tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>	
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><frmPartLabelHeader:message key="LBL_PURPOSE"/>:</b></td>
						<td colspan="3"> &nbsp; <bean:write name="<%=strFormName%>" property="purpose" />  </td>
					</tr>
					<tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>	
					<tr class="shade">
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><frmPartLabelHeader:message key="LBL_REFERENCEID"/>:</b></td>
						<td colspan="5">&nbsp;<bean:write name="<%=strFormName%>" property="refid" /> </td>
					</tr>
					<tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>	
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><frmPartLabelHeader:message key="LBL_SETNAME"/>:</b></td>
						<td colspan="3">&nbsp;<bean:write name="<%=strFormName%>" property="setname" /> </td>
					</tr>
					
					<tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>	
					<tr class="shade">
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><frmPartLabelHeader:message key="LBL_INITIATEDBY"/>:</b></td>
						<td colspan="2">&nbsp;  <bean:write name="<%=strFormName%>" property="cname" />  </td>
						<td class="RightText" ><b><frmPartLabelHeader:message key="LBL_DATEINITIATED"/>: </b>&nbsp;<%=GmCommonClass.getStringFromDate(createdDate,strApplDateFmt)%> </td>
					</tr>
					
					<tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>	
					<tr>
						<td class="RightText" valign="top" align="right" HEIGHT="20"><b><frmPartLabelHeader:message key="LBL_COMMENTS"/>:</b></td>
						<!-- If comment has more characters display an expand icon to view the whole comment -->
						<% if(comments.length() > 50){ %>
							<td colspan="3" HEIGHT="20" valign="top" >&nbsp;<%=comments.substring(0,50) %>...&nbsp;&nbsp;
								<img style="cursor:hand; border-width: 1px; border-style: solid;" align="middle" src='<%=strImagePath%>/expand_icon.png' title="Expand" width="15" height="15" onclick="fnExpandSummary('<%=comments%>')"/>
							</td>
						<%}else{ %>
							<td colspan="3" HEIGHT="20" valign="top" >&nbsp;<%=comments%></td>
						<%} %>
					</tr>
						<tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>
  		 <tr class="shade">                   
 <%if(strAction.equals("PP")){%>
  
  <td class ="RightText" height="10" align="center" style = "font-weight: bold;">Print Labels:<input type="checkbox" id="chkprintlable" name="printlable" onClick ="javascript:fnCheck('PartLabelChk');" <%=strPrintLabelDisable%> <%=strPrintLabelCheck%> <%=strChkDisable%> ></td>
  <td class ="RightText" height="10" align = "left" style = "font-weight: bold;">QC Verification of Labels:<input type="checkbox" id="chkqcverification" name="qcverification" onClick ="javascript:fnCheck('QCLabelChk');" <%=stQCDisable%> <%=stQCCheck%> <%=strChkDisable%> ></td>
  <td class ="RightText" height="10" align = "left" style = "font-weight: bold;">Pre Staging:<input type="checkbox" id="preStaging" name="pstaging" onClick ="javascript:fnCheck('PreStagingChk');" <%=strPreStagingDisable%> <%=strPreStagingChk%> <%=strChkDisable%>></td>
  <td class ="RightText" height="10" align = "left" style = "font-weight: bold;">Staging:<input type="checkbox" id="staging" name="stging" onClick ="javascript:fnCheck('StagingChk');" <%=strStagingDisable%> <%=strStagingChk%> <%=strChkDisable%>></td>
   
<%}%>
   </tr>
   <tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>
<%if(strAction.equals("PV")){%>
	 <tr class="shade">
           <td class ="RightText" height="10" align="center" style = "font-weight: bold;">Print Labels:<input type="checkbox" id="chkprintlable" name="printlable" onClick ="javascript:fnCheck('PartLabelChk');" disabled checked></td>
            <td class ="RightText" height="10"  align = "left" style = "font-weight: bold;">QC Verification of Labels:<input type="checkbox" id="chkqcverification" name="qcverification" disabled checked></td>
            <td class ="RightText" height="10" align = "left" style = "font-weight: bold;">Pre Staging:<input type="checkbox" id="preStaging" name="pstaging" disabled checked></td>
            <td class ="RightText" height="10" align = "left" style = "font-weight: bold;">Staging:<input type="checkbox" id="staging" name="stging" disabled checked></td>
            </tr>
             <tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr> 
             <%if(statusfl.equals("4")){%> 
            <tr height="30">
			     <td class="RightText" HEIGHT="20" align="right" width="20%"><b><frmPartLabelHeader:message key="LBL_LOT_CODE"/>:</b></td>
			     <td>&nbsp;<input type="text" name="scanLotCode" id="scanLotCode" class="InputArea" style="text-transform:uppercase;" maxlength="40" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" onkeypress="javascript:fnVerifyLotCode(this);" disabled ="true"/> </td>
		    
		     </tr>
		     <%}else{%>
		     <tr height="30">
			     <td class="RightText" HEIGHT="20" align="right" width="20%"><b><frmPartLabelHeader:message key="LBL_LOT_CODE"/>:</b></td>
			     <td>&nbsp;<input type="text" name="scanLotCode" id="scanLotCode" class="InputArea" style="text-transform:uppercase;" maxlength="40" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" onkeypress="javascript:fnVerifyLotCode(this);"/> </td>
		     </tr>
		     <%}%>
		        
		     <tr class="errortr"><td></td><td align="left"><div id="errormessage"></div></td></tr>
<%}%>		 
</table>