<%
/**********************************************************************************
 * File		 		: GmChangeLot.jsp
 * Desc		 		: This screen is used to Change Lot for the transactions
 * Version	 		: 1.0
 * author			: Vinoth Petchimuthu
************************************************************************************/
%>

<!--\operations\GmChangeLot.jsp  -->
<%@ page language="java"%>

<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtChangeLot" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtChangeLot:setLocale value="<%=strLocale%>"/>
<fmtChangeLot:setBundle basename="properties.labels.operations.GmChangeLot"/>

<%
    String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	String strWikiTitle = GmCommonClass.getWikiTitle("CHANGE_LOT");
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Change Lot#</TITLE>

<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmChangeLot.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
</HEAD>

<BODY leftmargin="20" topmargin="10">

<html:form action="/gmChangeLotNumber.do?method=changeLotNumber">
	<table  width="600" cellspacing="0" cellpadding="0" style="border: 1px solid  #676767;" >
		<tr>
			<td  height="25" class="RightDashBoardHeader"><fmtChangeLot:message key="LBL_CHANGE_LOT"/></td>
			<td align="right" class=RightDashBoardHeader><fmtChangeLot:message key="IMG_ALT_HELP" var="varHelp"/><img id='imgEdit'
				align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
				title='${varHelp}'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<tr>
			<td  colspan="7" class="Line" height="1"></td>
			
		</tr>
		<tr>
			<td colspan="7" class="LLine" height="1"></td>
			
		</tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr height="25"><fmtChangeLot:message key="LBL_TRANSACTION_ID" var="varTxnId"/>
			<gmjsp:label type="BoldText"  SFLblControlName="${varTxnId}" td="true"/>
			<td width="70%">&nbsp; : <html:text
				property="transactionId" styleId="transactionId" size="30" 
				onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
				onblur="changeBgColor(this,'#ffffff');" tabindex="1"/></td>
		</tr>
		<tr>
			<td colspan="7" class="LLine" height="1"></td>
			
		</tr>
		<tr class="Shade" height="25"><fmtChangeLot:message key="LBL_PART_NUMBER" var="varPartNumber"/>
			<gmjsp:label type="BoldText"  SFLblControlName="${varPartNumber}" td="true"/>
			<td width="70%">&nbsp; : <html:text
				property="partNumber" size="30" 
				onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
				onblur="changeBgColor(this,'#ffffff');" tabindex="2" /></td>
		</tr>
		<tr>
			<td  colspan="7" class="LLine" height="1"></td>
			
		</tr>
		<tr height="25"><fmtChangeLot:message key="LBL_QUANTITY" var="varQuantity"/>
			<gmjsp:label type="BoldText"  SFLblControlName="${varQuantity}" td="true"/>
			<td width="70%">&nbsp; : <html:text
				property="quantity" styleId="quantity" size="30" 
				onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
				onblur="changeBgColor(this,'#ffffff');" tabindex="3"/></td>
		</tr>		
		<tr>
			<td  colspan="7" class="LLine" height="1"></td>
			
		</tr>
			
		<tr class="Shade" height="25"><fmtChangeLot:message key="LBL_OLD_CONTRL_NUMBER" var="varOldCntrlNumber"/>
			<gmjsp:label type="BoldText"  SFLblControlName="${varOldCntrlNumber}" td="true"/> 
			<td width="70%">&nbsp; : <html:text
				property="oldCntrlNumber" styleId="oldCntrlNumber" size="30" 
				onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
				onblur="changeBgColor(this,'#ffffff');" tabindex="4"/></td>
		</tr>	
		<tr>
			<td colspan="7" class="LLine" height="1"></td>
			
		</tr>
		<tr height="25"><fmtChangeLot:message key="LBL_NEW_CONTRL_NUMBER" var="varNewCntrlNumber"/>
			<gmjsp:label type="BoldText"  SFLblControlName="${varNewCntrlNumber}" td="true"/> 
			<td width="70%">&nbsp; : <html:text
				property="newCntrlNumber" styleId="newCntrlNumber" size="30" 
				onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
				onblur="changeBgColor(this,'#ffffff');" tabindex="5"/></td>
		</tr>
		<tr>
			<td colspan="7" class="LLine" height="1"></td>
			
		</tr>
		
	<tr height="25">
		<td align="center" colspan="7" height="30">
						<fmtChangeLot:message key="BTN_RESET" var="varReset"/>
				<gmjsp:button name="button_reset" value="${varReset}" style="width: 6em" gmClass="button" onClick="fnReset();" tabindex="7" buttonType="Save" />
				<fmtChangeLot:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button
					value="&nbsp;${varSubmit}&nbsp;" name="btn_Submit" tabindex="6"
						gmClass="Button" onClick="fnSave();" buttonType="Save" />
				</td>
	</tr>
	 <tr>
	 	 <td colspan="7"><div id="successCont" style="text-align: center; font-size: 12px;"></div></td>
	 </tr>
	
</table>
 
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>