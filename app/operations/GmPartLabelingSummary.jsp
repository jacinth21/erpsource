<%
/*********************************************************************************************************
 * File		 		: GmPartLabelingSummary.jsp
 * Desc		 		: This screen is used display the whole summary information
 * Version	 		: 1.0
 * author			: Arajan
**********************************************************************************************************/
%>
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ taglib prefix="fmtPartLabelSummary" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmPartLabelingSummary.jsp -->
<fmtPartLabelSummary:setLocale value="<%=strLocale%>"/>
<fmtPartLabelSummary:setBundle basename="properties.labels.operations.GmPartLabel"/>
<% 
GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.operations.GmPartLabel", strSessCompanyLocale);
String strComments =GmCommonClass.parseNull((String)request.getParameter("comments"));
String strClose =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_CLOSE"));
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: Summary of Transaction</TITLE>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<script>
function fnClose(){
	window.close();
}
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnPageLoad();">
	<table style="width: 100%; border: 1px solid  #676767" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtPartLabelSummary:message key="LBL_SUMMARY_HEADER"/></td>
		</tr>
		<tr><td height="10px"></td></tr>
		<tr>
			<td height="25px">&nbsp;&nbsp;<%=strComments %>&nbsp;&nbsp;</td>
		</tr>
		<tr><td height="5px"></td></tr>
		<tr><td height="1" bgcolor="#cccccc"></td></tr>	
		<tr>
			<td height="30px" align="center"><gmjsp:button value="<%=strClose%>"  name="Btn_Submit" gmClass="button" onClick="fnClose()" buttonType="Load" /></td>
		</tr>
		
</table>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>