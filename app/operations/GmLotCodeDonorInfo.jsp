<%
/**********************************************************************************
 * File		 		: GmLotCodeDonorInfo.jsp
 * Desc		 		: This Jsp is for the donor information seciton
 * Version	 		: 1.0
 * author			: arajan
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtLotDonorInfo" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
  <!-- \operations\GmLotCodeDonorInfo.jsp--> 
<fmtLotDonorInfo:setLocale value="<%=strLocale%>"/>
<fmtLotDonorInfo:setBundle basename="properties.labels.operations.GmLotCodeReport"/>
<% 
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
%>
<HTML>
<HEAD>

<script>
</script>

<BODY leftmargin="20" topmargin="10">
	
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr><td class="Line" colspan="5"></td></tr> 
			<tr>
					<td height="24" class="ShadeRightTableCaption" colspan="5">&nbsp;<b><fmtLotDonorInfo:message key="TD_LOT_DONOR_INFO_HEADER"/></b></td>
		    </tr>	
		    <tr>
				<td class="RightText" HEIGHT="20"  align="right" width="20%">&nbsp;<b><fmtLotDonorInfo:message key="LBL_DONOR_NUM"/>:&nbsp;</b></td>
				<td width="30%" id="donorNum">&nbsp;<bean:write name="<%=strFormName %>" property="donorNum"/></td>
				<td class="RightText" HEIGHT="20"  align="right" width="20%">&nbsp;<b><fmtLotDonorInfo:message key="LBL_DONOR_AGE"/>:&nbsp;</b></td>
				<td width="30%" id="donorAge">&nbsp;<bean:write name="<%=strFormName %>" property="donorAge"/></td>
			</tr>
			
			<tr class="Shade">
				<td class="RightText" HEIGHT="20"  align="right">&nbsp;<b><fmtLotDonorInfo:message key="LBL_DONOR_SEX"/>:&nbsp;</b></td>
				<td id="donorSex">&nbsp;<bean:write name="<%=strFormName %>" property="donorSex"/></td>
				<td class="RightText" HEIGHT="20"  align="right">&nbsp;<b><fmtLotDonorInfo:message key="LBL_INTRNAT_USE"/>:&nbsp;</b></td>
				<td id="intUse">&nbsp;<bean:write name="<%=strFormName %>" property="intUse"/></td>
			</tr>
			
			<tr>
				<td class="RightText" HEIGHT="20"  align="right">&nbsp;<b><fmtLotDonorInfo:message key="LBL_FOR_REARCH"/>:&nbsp;</b></td>
				<td colspan="3" id="forResearch">&nbsp;<bean:write name="<%=strFormName %>" property="forResearch"/></td>
			</tr>
		     
		</table>

	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>