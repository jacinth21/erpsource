
<!--\operations\GmPOReportPendVendor.jsp  -->

 <%
/**********************************************************************************
 * File		 		: GmPOReportPendVendor.jsp
 * Desc		 		: This screen is used for the Pending PO Report By Vendor
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@page import="java.net.URLEncoder"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmCommonBarCodeServlet"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtPOReportPendVendor" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPOReportPendVendor:setLocale value="<%=strLocale%>"/>
<fmtPOReportPendVendor:setBundle basename="properties.labels.operations.GmPOReportPendVendor"/>
<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strDTDateFmt = "{0,date,"+strGCompDateFmt+"}";
	String strhAction = (String)request.getAttribute("hAction");
	strhAction = strhAction == null?"Load":strhAction;
	String strType = GmCommonClass.parseNull((String)request.getAttribute("hType"));
	strType = strType.equals("")?"0":strType;
	HashMap hmParams = (HashMap)request.getAttribute("hmParam");
	String strhUserId = GmCommonClass.parseNull((String)hmParams.get("EMPID"));
	
	ArrayList alVendorList = new ArrayList();
	ArrayList alPOTypeList = new ArrayList();
	ArrayList alUsers = new ArrayList();
	String strGridXmlData =  GmCommonClass.parseNull((String)request.getAttribute("GRIDXMLDATA"));
	String strWikiTitle = GmCommonClass.getWikiTitle("VENDOR_PO_PENDING");
	if (strhAction.equals("Load") || strhAction.equals("Go"))
	{
		alVendorList =  GmCommonClass.parseNullArrayList((ArrayList)session.getAttribute("VENDORLIST"));
		alPOTypeList =  GmCommonClass.parseNullArrayList((ArrayList)session.getAttribute("POTYPELIST"));
		alUsers = GmCommonClass.parseNullArrayList((ArrayList)session.getAttribute("USERLIST"));
		
	}
	
	int intSize = 0;
	String strVendorId = (String)request.getAttribute("hVendorId");
	strVendorId = strVendorId == null?"":strVendorId;

	String strTypeId = (String)request.getAttribute("hTypeId");
	strTypeId = strTypeId == null?"":strTypeId;
	String strCollapseImage = strImagePath+"/minus.gif";//32321
	HashMap hcboVal = null;
	String strCodeID = "";
	String strSelected = "";
	final String strProjId = (String)request.getAttribute("hProjId")==null?"":(String)request.getAttribute("hProjId");
	final String strPartNumber = (String)request.getAttribute("hPartNum")==null?"":(String)request.getAttribute("hPartNum");
	final String strSearch = (String)request.getAttribute("hSearch")==null?"":(String)request.getAttribute("hSearch");

	// Added for the PMT-18670 by suganthi
	String strCompanyId = gmDataStoreVO.getCmpid();
	String strPlantId = gmDataStoreVO.getPlantid();
	String strPartyId = gmDataStoreVO.getPartyid();
	String strCompDtFmt = gmDataStoreVO.getCmpdfmt();
	String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\",\"cmpdfmt\":\""+strCompDtFmt+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
	// Added for the PMT-18670 by suganthi
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: PO Pending Report - By Vendor </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<%-- <link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css"> --%>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/operations/GmPOReportPendVendor.js"></script>

<script>
var objGridData;
objGridData ='<%=strGridXmlData%>';
var companyInfoObj = '<%=strCompanyInfo%>';
var strType = '<%=strType%>';
		
var strServletPath = '<%=strServletPath%>';
var expandNum = 1;


</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmPOReportServlet?hAction=Go&hOpt=PendVend">
<input type="hidden" name="hAction" value="Load">
<input type="hidden" name="Txt_PO" value="">
<input type="hidden" name="hType" value="">
<input type="hidden" name="hOpt" value="">
<input type="hidden" name="hstrType" value="<%=strType%>">

	<table class="DtTable1650" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" bgcolor="#666666"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td valign="top">
				<table border="0"  width="100%" cellspacing="0" cellpadding="0">
					<tr height="25" >
						<td height="25" colspan="7">
							<table border="0"  width="100%" cellspacing="0" cellpadding="0">
							<tr><td height="25" class="RightDashBoardHeader">
							<fmtPOReportPendVendor:message key="LBL_PENDING_PO_REPORT_BY_VENDOR"/>
							</td><td  class="RightDashBoardHeader">
							<fmtPOReportPendVendor:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
							height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');"/>&nbsp;
							</td>
							</tr>
							</table>
						</td></tr>
					<tr><td class="Line" colspan="7"></td></tr>
					<tr>
						<td colspan="4" class="RightText" valign="absmiddle" HEIGHT="30" width="50%" style= "padding-bottom: 11px; white-space: nowrap; display: inline-block;"><BR>&nbsp;<fmtPOReportPendVendor:message key="LBL_VENDOR_NAME"/>:
								&nbsp;<select name="hVendorId" class="RightText" tabindex="3">
								<option value="0" ><fmtPOReportPendVendor:message key="LBL_CHOOSE_ONE"/>
		<%
					  		intSize = alVendorList.size();
							hcboVal = new HashMap();
					  		for (int i=0;i<intSize;i++)
					  		{
					  			hcboVal = (HashMap)alVendorList.get(i);
					  			strCodeID = (String)hcboVal.get("ID");
								strSelected = strVendorId.equals(strCodeID)?"selected":"";
		%>
									<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
		<%
					  		}
		%>
							</select> &nbsp;&nbsp;
							<select name="Cbo_Type" class="RightText" tabindex="3">
								<option value="0" ><fmtPOReportPendVendor:message key="LBL_CHOOSE_ONE"/>
								<option value="Pend"><fmtPOReportPendVendor:message key="LBL_PENDING"/>
								<option value="Due10"><fmtPOReportPendVendor:message key="LBL_DUE_IN_10_DAYS"/>
								<option value="Due15"><fmtPOReportPendVendor:message key="LBL_DUE_IN_15_DAYS"/>
								<option value="Due30"><fmtPOReportPendVendor:message key="LBL_DUE_IN_30_DAYS"/>
							</select>&nbsp;&nbsp;
						</td>
						<td>
						<table border="0"  borderwidth="350" cellspacing="0" cellpadding="0">
						<tr>
						<td colspan="2" class="RightText" valign="absmiddle" HEIGHT="30"><BR>&nbsp;<fmtPOReportPendVendor:message key="LBL_TYPE_OF_PO"/>:
								&nbsp;<select name="hTypeId" class="RightText" tabindex="3">
								<option value="0" ><fmtPOReportPendVendor:message key="LBL_CHOOSE_ONE"/>
		<%
					  		intSize = alPOTypeList.size();
							hcboVal = new HashMap();
					  		for (int i=0;i<intSize;i++)
					  		{
					  			hcboVal = (HashMap)alPOTypeList.get(i);
					  			strCodeID = (String)hcboVal.get("CODEID");
								strSelected = strTypeId.equals(strCodeID)?"selected":"";
		%>
									<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
		<%
					  		}
		%>
							</select> &nbsp;&nbsp;
							</td>
							<td class="RightText"    HEIGHT="30"  ><BR>&nbsp;<fmtPOReportPendVendor:message key="LBL_RAISED_BY"/>:&nbsp;	
			
						<gmjsp:dropdown controlName="Cbo_UserId"  seletedValue="<%=strhUserId%>"  defaultValue= "[All Employees]" tabIndex="1"   value="<%=alUsers%>" codeId="ID" codeName="NAME"/>
					</td>
							</tr>
							</table><br>
							</td>
							
					</tr>
					
									
					<tr><td class="Line" colspan="7"></td></tr>			
					<tr>
						<td colspan="7"> 
							<jsp:include page="/operations/inventory/include/GmPartFilter.jsp" >
							<jsp:param name="hPartNum" value="<%=strPartNumber%>" />
							<jsp:param name="hSearch" value="<%=strSearch%>" />
							<jsp:param name="hProjId" value="<%=strProjId%>" />
						</jsp:include>
						</td>
					</tr>
					<tr height="5px"></tr>				
					<tr><td class="LLine" colspan="7"></td></tr>	
					<%if(strGridXmlData.indexOf("cell") != -1) { %>
					<%-- <tr><td style="height: 25px;"><a href='#' onclick = "fnExpColl();"> <IMG id="PendPOImg" style="padding-left: 10px;" border=0 src="<%=strCollapseImage%>">&nbsp;Expand All/Collapse All</a></td></tr> --%>
					<tr>
						<td colspan="7">
							<div id="PoPendVenRpt" style="height:690px; width:100%"></div>
						</td>
					</tr>
					<tr>						
					<td align="center" colspan="8"><br>
	             			<div class='exportlinks'><fmtPOReportPendVendor:message key="LBL_EXCEL"/>: <img src='img/ico_file_excel.png' />&nbsp; <a href="#" onclick="fnDownloadXLS();"><fmtPOReportPendVendor:message key="LBL_EXCEL"/>  </a></div>                         
	       			</td>
       			</tr>
					<% }else{ %>	
					<tr><td colspan="2" align="center" class="RightTextRed"><fmtPOReportPendVendor:message key="LBL_NO_POS_HAVE_BEEN_PLACED"/> </td></tr>
<%}%>	 
				</table>
			</td>
			<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif"></td>
		</tr>
		<tr>
			<td colspan="7" height="1" bgcolor="#666666"></td>
		</tr>
    </table>

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
