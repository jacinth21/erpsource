<%
/**********************************************************************************
 * File		 		: GmModifyControlNumber.jsp
 * Desc		 		: Modify control number
 * Version	 		: 1.0
 * author			: Satyajit
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtModifyControlNumber" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtModifyControlNumber:setLocale value="<%=strLocale%>"/>
<fmtModifyControlNumber:setBundle basename="properties.labels.operations.GmModifyControlNumber"/>

<!-- operations.GmModifyControlNumber.jsp -->
<%String strWikiTitle = GmCommonClass.getWikiTitle("MODIFY_CONTROL_NUMBER");%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Shipping Include </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GmModifyControlNumber.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script>
var lblRefId = '<fmtModifyControlNumber:message key="LBL_REF_ID"/>';

</script>
</head>
<body leftmargin="20" topmargin="10" onLoad="fnSetFocus();">
<html:form action ="/gmModifyControlNumber.do" >
<table border="0" class="DtTable1100"  cellspacing="0" cellpadding="0" >	
		<tr>
			<td height="25" class="RightDashBoardHeader" ><fmtModifyControlNumber:message key="LBL_PRO_TRANS"/> </td>
			<td  height="25" class="RightDashBoardHeader" align="right" colspan="2">
			<fmtModifyControlNumber:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
		       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		    </td>
		</tr>	
		<tr><td colspan="3" height="1" bgcolor="#cccccc"></td></tr>
		<tr height="30" style="vertical-align: middle;">
			<td class="RightTableCaption" width="400" align="right"><font color="red">*&nbsp;</font><fmtModifyControlNumber:message key="LBL_REF_ID"/>:</td>
			<td width="240" >&nbsp;
		        <html:text property="refId" name="frmModifyControlNumber" size="30" onfocus="changeBgColor(this,'#AACCE8');" 
		        styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" onkeypress="fnCallGo()"/>
		        </td>
		        <td>
		         &nbsp;&nbsp;&nbsp;<fmtModifyControlNumber:message key="BTN_LOAD" var="varLoad" />
		         <gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="fnGo();"  buttonType="Load" />
		    </td> 
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#cccccc"></td></tr>
		<logic:notEqual name="frmModifyControlNumber" property="refId" value="">
			<tr>
				<td colspan="3">
					<iframe id="myFrame" src='<bean:write name="frmModifyControlNumber" property="url"/>' width="100%" scrolling="yes" height="700" frameborder="0"  vspace="0"  hspace="0"  marginwidth="0"  marginheight="0"></iframe>
				</td>
			</tr>	
		</logic:notEqual>
        </table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>