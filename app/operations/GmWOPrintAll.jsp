
 <%
/**********************************************************************************
 * File		 		: GmWOPrintAll.jsp
 * Desc		 		: This screen is used for the Work Order - Print Formart
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmJasperReport"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>

<%@ taglib prefix="fmtWOPrintAll" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\GmWOPrintAll.jsp -->

<fmtWOPrintAll:setLocale value="<%=strLocale%>"/>
<fmtWOPrintAll:setBundle basename="properties.labels.operations.GmWOPrintAll"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmWODetails = new HashMap();
	HashMap hmLoop = new HashMap();
	ArrayList alReturn = new ArrayList();
	ArrayList alSubWorkOrder = new ArrayList();
	ArrayList alSubDHR = new ArrayList();

	String strRptName = (String)request.getAttribute("STRRPTNAME");
	String strHtmlJasperRpt = "";
	String strDesc = "";
	String strUser = "";
	String strFooter = "";
	String strVALDFL = "";
	String strValidFLContent = "";
	String strValidateFLContent = "";
	String strSter = "";
	// getting the company locale
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
	
	int intSize = 0;
	int intSubWOSize = 0;
	int intSubDHRSize = 0;
	
	boolean bolSter = false;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: WO Print</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>

function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<form>
<%
	if (hmReturn != null)
	{
		String subReportPath = GmCommonClass.parseNull((String)hmReturn.get("SUBREPORT_DIR"));
		alReturn = (ArrayList)hmReturn.get("ALLWO");

		intSize = alReturn.size();
		for (int k=0;k<intSize;k++)
		{
			hmLoop = (HashMap)alReturn.get(k);
			hmWODetails = (HashMap)hmLoop.get("WODETAILS");

			strDesc = GmCommonClass.parseNull((String)hmWODetails.get("PDESC"));
			strFooter = GmCommonClass.parseNull((String)hmWODetails.get("FOOTER"));
			strVALDFL = GmCommonClass.parseNull((String)hmWODetails.get("VALIDATIONFL"));	
			strSter = GmCommonClass.parseNull((String)hmWODetails.get("STERFL"));
			String strArr[] = strFooter.split("\\^");
			strFooter = strArr[0];
			
			alSubWorkOrder = (ArrayList)hmLoop.get("SUBWODETAILS");
			alSubDHR = (ArrayList)hmLoop.get("SUBDHRDETAILS");
			int intsubAsssize = alSubWorkOrder.size();
			int intsubDHRsize = alSubDHR.size();
			hmWODetails.put("INTSUBASSSIZE",alSubWorkOrder.size());
			hmWODetails.put("INTSUBDHRSIZE",alSubDHR.size());
			HashMap hmLoop1 = new HashMap();
			hmLoop1.put("PNUM", "");		
			if(intsubAsssize == 0){			
				alSubWorkOrder.add(hmLoop);
			}		
			if(intsubDHRsize == 0){	
				alSubDHR.add(hmLoop);
			}
			if(strVALDFL.equals("Y")){
				strValidFLContent = "\t- <b>("+GmCommonClass.getString("GMVALIDATIONFL")+")</b>";
				strValidateFLContent = "\t<b>("+GmCommonClass.getString("GMVALIDATIONFLCONTENT")+")</b>";
			}else{
				strValidFLContent = "";
				strValidateFLContent ="";
			}
			if (strSter.equals("1"))
			{
				bolSter = true;
			}
			
			strDesc = GmCommonClass.getStringWithTM(strDesc);
			strUser = (String)hmWODetails.get("CUSER");
			strUser = strUser.concat(".gif");
			hmWODetails.put("IMGPATH",strImagePath);
			hmWODetails.put("USERIMG",strUser);
			hmWODetails.put("PDESC",strDesc);
			hmWODetails.put("VALIDFLCONTENT",strValidFLContent);
			hmWODetails.put("VALIDATEFLCONTENT",strValidateFLContent);
			hmWODetails.put("SUBREPORT_DIR",subReportPath);
			hmWODetails.put("RPTFOOTER",strFooter);
			hmWODetails.put("ALSUBASSEMBLYDETAIL",alSubWorkOrder);
			hmWODetails.put("ALSUBDHRDETAIL",alSubDHR);
			hmWODetails.put("LOGOIMAGE", gmResourceBundleBean.getProperty("WO.COMPANY_LOGO"));
%>
<tr>	
			<td>
				<%
				GmJasperReport gmJasperReport = new GmJasperReport();			
				gmJasperReport.setRequest(request);
				gmJasperReport.setResponse(response);
				gmJasperReport.setJasperReportName(strRptName);
				gmJasperReport.setHmReportParameters(hmWODetails);
				gmJasperReport.setReportDataList(alSubWorkOrder);
				gmJasperReport.setBlDisplayImage(true);
				gmJasperReport.setBlUseNewSession(true);
				strHtmlJasperRpt = gmJasperReport.getHtmlReport();				
				%>	
				<%=strHtmlJasperRpt %>			
			</td>
</tr>
<%
			if (k+1 < intSize)
			{
%>
	<p STYLE="page-break-after: always"></p>
<%
			}
		} // End of FOR
	} // END OF IF

%>
<div id="button" align="center">
	<table border="0" width="700" cellspacing="0" cellpadding="0" >
		<tr>
			<td align="center" height="30">
			<fmtWOPrintAll:message key="BTN_PRINT" var="varPrint"/>
				<gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" onClick="fnPrint();" buttonType="Load" />&nbsp;&nbsp;
				<fmtWOPrintAll:message key="BTN_CLOSE" var="varClose"/>
				<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close()" buttonType="Load" />&nbsp;&nbsp;
			</td>
		<tr>
	</table>
</DIV>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
