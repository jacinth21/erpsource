 <%
/**********************************************************************************
 * File		 		: GmPartNumInvReport.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<!--operations\GmNCMRReport.jsp  -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.PopChartUtils"%>
<%@ page import="java.util.Date"%>
<%@ taglib prefix="fmtNCMRReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtNCMRReport:setLocale value="<%=strLocale%>"/>
<fmtNCMRReport:setBundle basename="properties.labels.operations.GmNCMRReport"/>

<%
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=UTF-8");
	response.setCharacterEncoding("UTF-8");
%>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strApplDateFmt = strGCompDateFmt;
	ArrayList alReturn = (ArrayList)request.getAttribute("alReturn");
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	
	String strFromDt 	= GmCommonClass.parseNull((String)request.getAttribute("hFromDate")) ;
	String strToDt 		= GmCommonClass.parseNull((String)request.getAttribute("hToDate"));
	String strPartNums 	= GmCommonClass.parseNull((String)request.getAttribute("hPartNum"));
	String strPartName 	= GmCommonClass.parseNull((String)request.getAttribute("hPartName"));
	String strVendorName = GmCommonClass.parseNull((String)request.getAttribute("hVendorName"));
	String strNCMRID 	= "";
	String strSortCName = GmCommonClass.parseNull((String)request.getAttribute("hSort"));
	String strSortType 	= GmCommonClass.parseNull((String)request.getAttribute("hSortAscDesc"));
	String strNCMRId 	= GmCommonClass.parseNull((String)request.getAttribute("hNCMRId"));	
	String strEVALID 	= GmCommonClass.parseNull((String)request.getAttribute("hEVALId"));
	String strNonConf 	= GmCommonClass.parseNull((String)request.getAttribute("hNonConf"));	
	String strDHRId		= GmCommonClass.parseNull((String)request.getAttribute("hDHRId"));	
	String strType 		= GmCommonClass.parseNull((String)request.getAttribute("Cbo_Type"));
	String strTypeNew 		= GmCommonClass.parseNull((String)request.getAttribute("Cbo_TypeNew"));
	String strGridXmlData = GmCommonClass.parseNull((String)request.getAttribute("GRIDXMLDATA"));
	HashMap hmLoop = new HashMap();
	
	Date dtFrmDate = null;
	Date dtToDate = null;	
	dtFrmDate = (Date)GmCommonClass.getStringToDate(strFromDt,strApplDateFmt);
	dtToDate = (Date)GmCommonClass.getStringToDate(strToDt,strApplDateFmt);	
	int intLoop = 0;	
	if (alReturn != null){
		intLoop = alReturn.size();
	} 
	int intSize = 0;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: DHR Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="javascript/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmNCMRReport.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>
var objGridData;
objGridData ='<%=strGridXmlData%>';
var Cbo_Type = '<%=strType%>';
var Cbo_TypeNew = '<%=strTypeNew%>';
var arraySize = '<%=intLoop%>';
var strSortCName = '<%=strSortCName%>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnOnPageLoad();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmNCMRReportServlet">
<input type="hidden" name="hId" value="">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hSort" value="<%=strSortCName%>">
<input type="hidden" name="hSortAscDesc" value="<%=strSortType%>">
	<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">		
		<tr>
			<td height="25" class="RightDashBoardHeader">
				<fmtNCMRReport:message key="LBL_NCMR"/>   
			</td>
		</tr>
		<tr>
			<td class="RightTableCaption" colspan="2" HEIGHT="30">
				&nbsp;<fmtNCMRReport:message key="LBL_PART_NUMBERS"/>&nbsp;<input type="text" size="50" value="<%=strPartNums%>" name="Txt_PartNum" 
				class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
				<span class="RightTextBlue"><fmtNCMRReport:message key="LBL_ENTER_PART"/></span>
			</td>
		</tr>
		<tr><td class="LLine" height="1" colspan="2"></td></tr>
		<tr class="Shade">
			<td class="RightTableCaption" colspan="2" HEIGHT="30">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtNCMRReport:message key="LBL_PART_NAME"/>:&nbsp;<input type="text" size="35" 
				value="<%=strPartName%>" name="Txt_PartName" class="InputArea" 
				onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" 
				tabindex=2>&nbsp;&nbsp;<fmtNCMRReport:message key="LBL_VENDOR_NAME"/>:&nbsp;<input type="text" size="35" value="<%=strVendorName%>" 
				name="Txt_VendorName" class="InputArea" 
				onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >
			</td>
		</tr>
		<tr><td class="LLine" height="1" colspan="2"></td></tr>
		<tr>
			<td class="RightTableCaption" colspan="2" HEIGHT="30">
				&nbsp;<fmtNCMRReport:message key="LBL_NON_CONF"/>:&nbsp;<input type="text" size="35" 
				value="<%=strNonConf%>" name="Txt_NonConf" class="InputArea" 
				onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" 
				tabindex=2>&nbsp;&nbsp;<fmtNCMRReport:message key="LBL_NCMR#"/>:&nbsp;<input type="text" size="15" value="<%=strNCMRId%>" 
				name="Txt_NCMRNO" class="InputArea" 
				onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"> 
				&nbsp;&nbsp;<fmtNCMRReport:message key="LBL_DHR#"/>:&nbsp;<input type="text" size="15" value="<%=strDHRId%>" 
				name="Txt_DHRNO" class="InputArea" 
				onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"> 
			</td>
		</tr>
		<tr><td class="LLine" height="1" colspan="2"></td></tr>
		<tr class="Shade">
			<td class="RightTableCaption" HEIGHT="24">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtNCMRReport:message key="LBL_FROM_DATE"/>:&nbsp;<gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=dtFrmDate==null?null:new java.sql.Date(dtFrmDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="2"/>
				&nbsp;&nbsp;<fmtNCMRReport:message key="LBL_TO_DATE"/>: <gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=dtToDate==null?null:new java.sql.Date(dtToDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="3"/> 
				&nbsp;&nbsp;<fmtNCMRReport:message key="LBL_STATUS"/> :
				&nbsp;&nbsp;<select name="Cbo_Type" id="Type" class="RightText" tabindex="10">
			 	<option value="0" ><fmtNCMRReport:message key="LBL_OPEN"/></option>
			 	<option value="1" ><fmtNCMRReport:message key="LBL_CLOSED"/></option>
			 	<option value="2" ><fmtNCMRReport:message key="LBL_ALL"/></option>
		 		</select>
		 		&nbsp;&nbsp;<fmtNCMRReport:message key="LBL_TYPE"/> :
				&nbsp;&nbsp;<select name="Cbo_TypeNew" value = '<%=strTypeNew%>' id="Type" class="RightText" tabindex="10">
				<option value="0" ><fmtNCMRReport:message key="LBL_NCMR"/></option>
			 	<option value="1" ><fmtNCMRReport:message key="LBL_EVAL"/></option>
			 	<option value="2" ><fmtNCMRReport:message key="LBL_ALL"/></option>
			 	</select>&nbsp;&nbsp;<fmtNCMRReport:message key="LBL_EVAL_ID"/>:&nbsp;<input type="text" size="15" value="<%=strEVALID%>" name="Txt_EVALID" class="InputArea" 
				onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"> 
				&nbsp;&nbsp;<fmtNCMRReport:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" gmClass="button" onClick="fnSubmit();" style="height:22;width:4em;" tabindex="25" buttonType="Load" />&nbsp;				<%if(intLoop > 0){ %>
				<fmtNCMRReport:message key="BTN_EXCEL" var="varExcel"/><gmjsp:button value="${varExcel}" gmClass="button" style="height:22;width:9em;" onClick="javascript:fnPrintDownVer('Excel');" buttonType="Load" />
				<%}%>
			</td>
		</tr>
		
		<tr><td class="Line" height="1" colspan="2"></td></tr>
		<% if(intLoop > 0) { %> 
		<tr>
				<td colspan="8">
					<div id="NCMRRpt" style="height: 580px; width: 100%"></div>
					<%-- <input type="hidden" name="hLen" value="<%=intSize%>">	 --%>
					<!-- <div id="pagingArea" style=" width:800px"></div> -->
					</td>
					
			</tr>
	
				<tr><td colspan="16" bgcolor="#eeeeee" height="1"></td></tr>
 <%
				
			} else	{
%> 
			 <tr>
				<td height="30" colspan="16" align="center" class="RightTextBlue"><fmtNCMRReport:message key="LBL_NO_NCMR"/>!</td>
			</tr>
<%		}
%>	 		
	
	</table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
