
<%
	/***************************************************************************************************************************************
	 * File		 		: GmFieldSalesWarehouseLogRpt.jsp
	 * Desc		 		: This screen is used for Loading the Field Sales Log reports.
	 * Version	 		: 1.0
	 * author			: 
	 ****************************************************************************************************************************************/
%>
 <!-- \operations\purchasing\GmFieldSalesWarehouseLogRpt.jsp-->  
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*"%>
<%@ page import="com.globus.common.beans.GmCommonControls"%>
<%@ taglib prefix="fmtFSWareLog" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtFSWareLog:setLocale value="<%=strLocale%>"/>
<fmtFSWareLog:setBundle basename="properties.labels.operations.GmFieldSalesWarehouseRpt"/>

<bean:define id="gridData" name="frmfieldSalesWarehouse" property="gridData" type="java.lang.String"></bean:define>
<bean:define id="locationType" name="frmfieldSalesWarehouse" property="locationType" type="java.lang.String"></bean:define>
<%
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	String strWikiTitle = GmCommonClass.parseNull((String)GmCommonClass.getWikiTitle("FIELD_SALES_NET_QUANTITY_LOG"));
	String strApplDateFmt = strGCompDateFmt;
	String strCurDate = GmCommonClass.parseNull((String) session.getAttribute("strSessTodaysDate"));
	String strTitle= "GlobusOne Enterprise Portal: Field Sales Net Qty Log";
	if(locationType.equals("26230710") || locationType.equals("70110"))
	{
		strTitle="GlobusOne Enterprise Portal: Account Net Qty Log";
	}
%>
<HTML>
<HEAD>


<TITLE><%=strTitle%></TITLE>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<link rel="stylesheet" type="text/css" href="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmFieldSalesWarehouseLogRpt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>

<script>
var objGridData = '';
objGridData = '<%=gridData%>';
var date_format = '<%=strApplDateFmt%>';
</script>
</HEAD>

<!-- Custom tag lib code modified for JBOSS migration changes -->
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();" onkeypress="fnEnter();">
	<html:form action="/gmFieldSalesWarehouseRpt.do">
		<html:hidden property="strOpt" />
		<html:hidden property="warehouseId"  value="3"/>
 		<html:hidden property="locationId" />
 		<html:hidden property="locationType"  value="<%=locationType%>"/> 	
 		<html:hidden property="lotNum" /> 		
 			
		<html:hidden property="partNumber" /> 
				<table border="0" class="DtTable950" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="7" height="25" class="RightDashBoardHeader">
					<logic:equal name="frmfieldSalesWarehouse" property="locationType" value="4000338"><fmtFSWareLog:message key="LBL_FS_QTY_LOG_HEADER"/></logic:equal>
				    <logic:equal name="frmfieldSalesWarehouse" property="locationType" value="56005"><fmtFSWareLog:message key="LBL_ACCT_QTY_LOG_HEADER"/></logic:equal>				
					<logic:equal name="frmfieldSalesWarehouse" property="locationType" value="70110"><fmtFSWareLog:message key="LBL_ACCT_QTY_LOG_HEADER"/></logic:equal>
					<logic:equal name="frmfieldSalesWarehouse" property="locationType" value="26230710"><fmtFSWareLog:message key="LBL_ACCT_QTY_LOG_HEADER"/></logic:equal>
					
					</td>
				<fmtFSWareLog:message key="IMG_ALT_HELP" var = "varHelp"/>
				<td height="25" class="RightDashBoardHeader"><img align="right"
					id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td colspan="8" class="LLine" height="1"></td>
			</tr>
			<tr height="25">
				<fmtFSWareLog:message key="LBL_TXN_DT_FRM" var = "varTxnDtFrm"/> 
				<td class="RightTableCaption" colspan="2" align="right">&nbsp;<gmjsp:label
						type="RegularText" SFLblControlName="${varTxnDtFrm}:" td="false" /></td>
				<td class="RightText">&nbsp;<gmjsp:calendar SFFormName="frmfieldSalesWarehouse" controlName="fromDt" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
				onBlur="changeBgColor(this,'#ffffff');"/></td>
				<fmtFSWareLog:message key="LBL_TXN_DT_TO" var = "varTxnDtTo"/> 
				<td class="RightTableCaption" colspan="2" align="right">&nbsp;<gmjsp:label type="RegularText" SFLblControlName="${varTxnDtTo}:" td="false" /></td>
				<td>&nbsp;<gmjsp:calendar SFFormName="frmfieldSalesWarehouse" controlName="toDt" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
				onBlur="changeBgColor(this,'#ffffff');"/></td>
			</tr>
			<tr>
				<td colspan="8" class="LLine" height="1"></td>
			</tr>
			<tr height="30" class="Shade">
				<fmtFSWareLog:message key="LBL_HIST_DATA" var = "varHistData"/>  
				<td class="RightTableCaption" colspan="2" align="right">&nbsp;<gmjsp:label
						type="RegularText" SFLblControlName="${varHistData}:" td="false" />
				</td>
				<td colspan="2">&nbsp;<html:checkbox  property="historicData" tabindex = "-1"/>
				</td>
				<fmtFSWareLog:message key="BTN_LOAD" var = "varLoad"/>  
			<td class="RightTableCaption" colspan="4" align="left"> <gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="fnLoad();" tabindex="4" buttonType="Load" /></td>
			</tr>
				<%
					if (gridData.indexOf("cell") != -1) {
				%>
			<tr>
				<td colspan="8">
					<div id="acctivityRpt" style="height: 320px;"></div>
					<div id="pagingArea" style=" width:800px"></div>
					</td>
					
			</tr>
				
				<tr>
				<td colspan="8" class="LLine" height="1"></td>
			</tr>
			<tr height="25">
			<td colspan="8" align="center">
			    <div class='exportlinks'><fmtFSWareLog:message key="DIV_EXPORT_OPT"/> : <img
					src='img/ico_file_excel.png' />&nbsp;<a href="#"
					onclick="fnExport();"> <fmtFSWareLog:message key="DIV_EXCEL"/>
				</a></div>
			</td>
		</tr>
				<%
					}else{
				%>
			<tr>
				<td colspan="8" class="LLine" height="1"></td>
			</tr>	
			<tr>
			<td colspan="8" align="center" class="RegularText"><fmtFSWareLog:message key="MSG_NO_DATA"/></td>
			</tr>
			<%} %>
			<tr>
				<td colspan="8" class="LLine" height="1"></td>
			</tr>	
			<tr height="30">
			<fmtFSWareLog:message key="BTN_CLOSE" var = "varClose"/>  
			<td class="RightTableCaption" colspan="8" align="center"><gmjsp:button value="&nbsp;${varClose}&nbsp;" gmClass="button" onClick="fnFsLogClose();" tabindex="5" buttonType="Load" /></td>
			</tr>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
