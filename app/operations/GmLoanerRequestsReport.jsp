 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@page import="com.globus.common.beans.GmCommonBean"%>
<%
/**********************************************************************************
 * File		 		: GmLoanerReests.jsp
 * Desc		 		: This screen is used for the displaying loaner request report
 * Version	 		: 1.1
 * author			: Xun
 * 
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page buffer="16kb" autoFlush="true" %>
<%@ taglib prefix="fmtLoanerRequestRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel"%>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmLoanerRequests.jsp -->
<fmtLoanerRequestRpt:setLocale value="<%=strLocale%>"/>
<fmtLoanerRequestRpt:setBundle basename="properties.labels.custservice.ProcessRequest.GmLoanerProcessRequest"/>

<bean:define id="alStatus" name="frmLoanerReqRpt" property="alStatus" type="java.util.ArrayList"></bean:define>
<bean:define id="alLoanerRequest" name="frmLoanerReqRpt" property="alLoanerRequest" type="java.util.ArrayList"></bean:define>
<bean:define id="strStatus" name="frmLoanerReqRpt" property="hStatus" type="java.lang.String"></bean:define>
<bean:define id="gridXmlData" name="frmLoanerReqRpt" property="gridXmlData" type="java.lang.String"></bean:define>
<bean:define id="dtType" name="frmLoanerReqRpt" property="dtType" type="java.lang.String"></bean:define>  
<bean:define id="strEsclAccessFl" name="frmLoanerReqRpt" property="strEsclAccessFl" type="java.lang.String"></bean:define>
<bean:define id="strEsclRemAccessFl" name="frmLoanerReqRpt" property="strEsclRemAccessFl" type="java.lang.String"></bean:define>
<bean:define id="strFromDt" name="frmLoanerReqRpt" property="fromDt" type="java.lang.String"></bean:define> 
<bean:define id="strToDt" name="frmLoanerReqRpt" property="toDt" type="java.lang.String"></bean:define> 
<bean:define id="alSize" name="frmLoanerReqRpt" property="alSize" type="java.lang.Integer"></bean:define>
<bean:define id="showGrid" name="frmLoanerReqRpt" property="showGrid" type="java.lang.Boolean"></bean:define>
<%
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	String strSessApplDateFmt= strGCompDateFmt;//GmCommonClass.parseNull((String)request.getSession().getAttribute("strSessApplDateFmt"));
	String strhAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");	
	
	GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.ProcessRequest.GmLoanerProcessRequest", strSessCompanyLocale);
	String strHeader =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_REPORT_LOANER_REQUESTS"));
	
	HashMap hmMapStatus = new HashMap();
	hmMapStatus.put("ID","CODENMALT");
	hmMapStatus.put("PID","CODENMALT");
	hmMapStatus.put("NM","CODENM");
%>
<HTML>
<TITLE> Globus Medical: Open Loaner Requests</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="STYLESHEET" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="javascript" src="<%=strOperationsJsPath%>/GmLoanerRequestsReport.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script>
var strdtType  = "<%=dtType%>";
var format = '<%=strSessApplDateFmt%>';
<%-- var strInHouseType = "<%=strInHouseType%>"; --%>
var gridXmlData='<%=gridXmlData%>';
var mygrid ='';
var strEsclFl = '<%=strEsclAccessFl%>';
var strRemEscFl = '<%=strEsclRemAccessFl%>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnOnLoad();">
<html:form action="/gmOprLoanerReqRpt.do">
 <input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hTxnId" value="">
<input type="hidden" name="hId" value="">
<input type="hidden" name="hPrdReqId" value="">
<input type="hidden" name="hStatus" value="<%=strStatus%>">
<input type="hidden" name="hAccessFl" value=""/>

<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" class="DtTable1200" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" colspan="2" class="RightDashBoardHeader" ><%=strHeader%> </td>
			<td  class="RightDashBoardHeader" align="right" colspan="2"></td>
		    <td align="right" class=RightDashBoardHeader>
		    <fmtLoanerRequestRpt:message key="LBL_HELP" var="varHelp"/>
				<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' 
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("LOANER_REQUESTS_REPORT")%>');" />
			</td> 
		</tr>
		<tr><td class="Line" height="1" colspan="5"></td></tr>	
		<tr class="evenshade">
			<td height="25" class="RightTableCaption" align="right">&nbsp;<fmtLoanerRequestRpt:message key="LBL_DISTRIBUTORNAME"/>:</td>
			 <td><gmjsp:dropdown controlName="distId" SFFormName="frmLoanerReqRpt" SFSeletedValue="distId" 
				SFValue="alDistributor" codeId = "ID" codeName = "NAME" defaultValue= "[Choose One]" tabIndex="1"/>  
			</td>
						
			<td height="25" class="RightTableCaption" align="right"><fmtLoanerRequestRpt:message key="LBL_SETNAME"/>:</td>
			<td colspan="2">&nbsp;<html:text name="frmLoanerReqRpt" property ="setName"  tabindex="2"
				styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />					 		
			</td>
                        	
		</tr>
		<tr><td class="Line" height="1" colspan="5"></td></tr>
		<tr class="oddShade">
			<td HEIGHT="30" class="RightTableCaption" align="right">&nbsp;<fmtLoanerRequestRpt:message key="LBL_DATERANGE"/>:</td>
			<td class="RightTableCaption">&nbsp;<fmtLoanerRequestRpt:message key="LBL_TYPE"/>:
					<gmjsp:dropdown controlName="dtType" SFFormName="frmLoanerReqRpt" SFSeletedValue="dtType" 
				SFValue="alType" codeId = "CODEID" codeName = "CODENM" defaultValue= "[Choose One]" tabIndex="1"/>  
					<BR><BR>&nbsp;<fmtLoanerRequestRpt:message key="LBL_FROM"/>:&nbsp;<gmjsp:calendar textControlName="fromDt" SFFormName='frmLoaner' 
					textValue="<%=(strFromDt.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(strFromDt,strSessApplDateFmt).getTime())))%>"  
			 	onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
			&nbsp;<fmtLoanerRequestRpt:message key="LBL_TO"/>:&nbsp;<gmjsp:calendar textControlName="toDt" SFFormName='frmLoaner'  
			textValue="<%=(strToDt.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(strToDt,strSessApplDateFmt).getTime())))%>"  
				onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
			</td>
			<td class="RightTableCaption" align="right">&nbsp;<fmtLoanerRequestRpt:message key="LBL_STATUS"/>:&nbsp;</td>
			<td height="60">&nbsp;&nbsp;<%=GmCommonControls.getChkBoxGroup("",alStatus,"status",hmMapStatus)%></td>		
			<fmtLoanerRequestRpt:message key="BTN_LOAD" var="varLoad"/>   
			<td width="150"><gmjsp:button name="Btn_Go" value="${varLoad}" style="width: 6em; height:2.5em" onClick="return fnReload();" tabindex="3" gmClass="button" buttonType="Load" /></td>
		</tr>
		<%// if (strhAction.equals("Reload")) { %>
		<tr><td class="Line" height="1" colspan="6"></td></tr>		
		<tr>
		<logic:equal property="showGrid" name="frmLoanerReqRpt" value="true">
			<td colspan="5">
					 <div  id="dataGridDiv" style="height:550px ; "></div>
			</td>
				   <tr><td  colspan="5" align="center"><div class='exportlinks'><fmtLoanerRequestRpt:message key="LBL_EXPORTOPTIONS"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
	               onclick="fnDownloadXLS();"> <fmtLoanerRequestRpt:message key="LBL_EXCEL"/> </a></div></td> </tr></logic:equal>
		<logic:equal property="showGrid" name="frmLoanerReqRpt" value="false">
			<td class="RightText" colspan="5">
			  <fmtLoanerRequestRpt:message key="LBL_NOTHING_TO_FOUND_TO_DISPLAY"/>
			</td>	
		</logic:equal>
       </tr>
		 <% if (alSize >0) { %> 
                   <tr height = "35">
                   
                    	<td colspan="5" class="RightTableCaption" align="center"><fmtLoanerRequestRpt:message key="LBL_CHOOSEACTION"/>:&nbsp;
						<gmjsp:dropdown controlName="reqAction" SFFormName="frmLoanerReqRpt" SFSeletedValue="reqAction" 
						SFValue="alReqType" codeId = "CODEID" codeName = "CODENM" defaultValue= "[Choose One]" tabIndex="5"/>  
			
						<fmtLoanerRequestRpt:message key="BTN_SUBMIT" var="varSubmit"/> 
						<gmjsp:button name="Btn_Submit" value="${varSubmit}" style="width:6em; height:2.5em;" gmClass="button" onClick="return fnSubmit();" buttonType="Save" /> 
		                </td>
                    </tr>
                    <% } %>
                    
              <% //} %> 
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>	
</BODY>

</HTML>
