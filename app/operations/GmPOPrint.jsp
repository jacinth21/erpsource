<!-- \operations\GmPOPrint.jsp -->
 <%@page import="java.util.Date"%>
<%
/**********************************************************************************
 * File		 		: GmPOPrint.jsp
 * Desc		 		: This screen is used for the Packing Slip
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="fmtPOPrint" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>

<%@ taglib prefix="fmtPrint" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPrint:setLocale value="<%=strLocale%>"/>
<fmtPrint:setBundle basename="properties.labels.operations.GmPOPrint"/>

<%
try {
	response.setCharacterEncoding("UTF-8");
  //commented the below lines for PC-194 -Ability to print WO paperwork in Vendor portal
	//GmServlet gm = new GmServlet();
	//gm.checkSession(response,session);

	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");

	HashMap hmPODetails = new HashMap();
	HashMap hmTemp = new HashMap();
	ArrayList alItemDetails = new ArrayList();

	String strShade = "";

	String strPOId = "";
	String strVendorAdd = "";
	String strPODate = "";
	String strReqdDate = "";
	String strPOTotal = "";
	String strPOType = "";
	
	String strWOId = "";
	String strPartNum = "";
	String strDesc = "";
	String strPrice = "";
	String strQty = "";
	String strRate = "";
	String strAmount = "";
	String strUser = "";
	String strTotal = "";
	String strUserName = "";
	String strComments = "";
	String strFARFl = "";
	String strWORev = "";
	String strPayTerms = "";
	
	String strSplitVal = "";
	String strRawRate = "";
	String strFabRate = "";
	String strRawAmount = "";
	String strFabAmount = "";
	String strMaterialVal = "";
	String strSec199 = ""; 
	String strRawTotal = "";
	String strRawFlag = "";
	String strVendorCurrency = "";
	String strVALDFL = "";
	String strContent = "";
	String strWoDueDate = "";
	
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);

	HashMap hmCompanyAddress = GmCommonClass.fetchCompanyAddress(gmDataStoreVO.getCmpid(), "");
	String strCompanyName = GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYNAME"));
	String strCompanyAddress = GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYADDRESS")).replaceAll("/","<br>");
	String strCompShipAddr = GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPSHIPADDR")).replaceAll("/","<br>");
	String strPoPrintHeader = "Purchase Order";
	String strCompLogo =  GmCommonClass.parseNull((String)hmCompanyAddress.get("COMP_LOGO"));
	String strShowPaymentTerms = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PO.SHOW_PAYMENT_TERMS"));
	
	//PC-3769: To get term and conditions from paperwork properties and show in PO print footer section
	String strShowTermsandConditions = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PO.TERMS_CONDITION"));
	if (hmReturn != null)
	{
		hmPODetails = (HashMap)hmReturn.get("PODETAILS");
		alItemDetails = (ArrayList)hmReturn.get("ITEMDETAILS");

		strPOId = (String)hmPODetails.get("POID");
		strVendorAdd = (String)hmPODetails.get("VADD");
		strPODate =GmCommonClass.getStringFromDate((Date)hmPODetails.get("PODATE"),strGCompDateFmt);
		strReqdDate = GmCommonClass.getStringFromDate((Date)hmPODetails.get("RQDATE"),strGCompDateFmt);
		strPOTotal = (String)hmPODetails.get("TOTAL");
		strUser = (String)hmPODetails.get("CUSER");
		strComments = (String)hmPODetails.get("COMMENTS");
		strUser = strUser.concat(".gif");
		strUserName = (String)hmPODetails.get("CNAME");
		strPOType = (String)hmPODetails.get("POTYPE");
		strVendorCurrency = (String)hmPODetails.get("VENDCURR");
		strPayTerms = GmCommonClass.parseNull((String)hmPODetails.get("PAYTERMS"));
	}
	
	//PMT-41678 Change Ship To Address on Purchase Orders
	String strCompShipAddrByPOType = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PO_SHIP_ADDRESS_"+strPOType)).replaceAll("/","<br>");
    
    if(!strCompShipAddrByPOType.equals("")){
        strCompShipAddr = strCompShipAddrByPOType;
    }
	
	
	if(strPOType.equals("102365")) // Stock transfer order type
		strPoPrintHeader = "Stock Transfer";
	int intSize = 0;
	HashMap hcboVal = null;
	
	for (int i=0;i<alItemDetails.size();i++)
	{
		hmTemp = (HashMap)alItemDetails.get(i);
		strSplitVal = GmCommonClass.parseNull( (String)hmTemp.get("SPLIT_COST"));  
		if(!strSplitVal.equals(""))
		{	
			strSec199 = "Y";
			break;
		}
	}

	StringBuffer sbStartSection = new StringBuffer();
	sbStartSection.append("<table border=0 width=700 cellspacing=0 cellpadding=0>");
	sbStartSection.append("<tr><td bgcolor=#666666 colspan=3></td></tr>");
	sbStartSection.append("<tr><td bgcolor=#666666 width=1></td>");
	sbStartSection.append("<td>");
	sbStartSection.append("<table cellpadding=0 cellspacing=0 border=0 width='100%'>");
	sbStartSection.append("<tr><td bgcolor=#666666 height=1 colspan=6></td></tr>");
	sbStartSection.append("<tr>");
	sbStartSection.append("<td width=160><img src=");sbStartSection.append(strImagePath);
	sbStartSection.append("/" + strCompLogo + ".gif width=138 height=60></td>");
	//sbStartSection.append("<td class=RightText width=190>Globus Medical Inc.<br>Valley Forge Business Center<br>2560 General Armistead Avenue<br>Audubon, PA 19403 </td>");
	sbStartSection.append("<td class=RightText width=190>");
	sbStartSection.append(strCompanyName);
	sbStartSection.append("<BR>" + strCompanyAddress + "</td>");
	sbStartSection.append("<td align=right class=RightText ><font size=+3>"+strPoPrintHeader+"</font></td>");
	sbStartSection.append("</tr>");
	sbStartSection.append("<tr><td colspan=3 height=1 bgcolor=#666666></td></tr>");
	sbStartSection.append("<tr><td colspan=3>");
	sbStartSection.append("<table border=0 width=100% cellspacing=0 cellpadding=0>");
	sbStartSection.append("<tr bgcolor=#eeeeee  class=RightTableCaption>");
	sbStartSection.append("<td height=24 align=center width=250>&nbsp;Vendor</td>");
	sbStartSection.append("<td bgcolor=#666666 width=1 rowspan=11></td>");
	sbStartSection.append("<td width=250 align=center>&nbsp;Ship To</td>");
	sbStartSection.append("<td bgcolor=#666666 width=1 rowspan=11></td>");
	sbStartSection.append("<td width=100 align=center>&nbsp;PO Date</td>");
	sbStartSection.append("<td width=100 align=center>&nbsp;PO Number</td>");
	sbStartSection.append("</tr><tr><td bgcolor=#666666 height=1 colspan=6></td></tr>");
	sbStartSection.append("<tr><td class=RightText rowspan=5 valign=top>&nbsp;");sbStartSection.append(strVendorAdd);
	sbStartSection.append("</td>");
	sbStartSection.append("<td rowspan=5 class=RightText valign=top>");
	sbStartSection.append(strCompanyName);
	sbStartSection.append("<BR>" + strCompShipAddr + "</td>");
	//sbStartSection.append("<td rowspan=5 class=RightText valign=top>&nbsp;Globus Medical Inc.<br>&nbsp;ATTN:Receiving<br>&nbsp;2560 General Armistead Avenue<br>&nbsp;Audubon, PA 19403</td>");
	sbStartSection.append("<td height=25 class=RightText align=center>&nbsp;");sbStartSection.append(strPODate);
	sbStartSection.append("</td><td class=RightTableCaption align=center>&nbsp;");
	sbStartSection.append(strPOId);
	sbStartSection.append("</td></tr><tr><td bgcolor=#666666 height=1 colspan=2></td></tr>");
	sbStartSection.append("<tr bgcolor=#eeeeee  class=RightTableCaption><td align=center colspan=2>Raised By</td></tr>");
	sbStartSection.append("<tr><td bgcolor=#666666 height=1 colspan=2></td></tr>");
	sbStartSection.append("<tr>");
	sbStartSection.append("<td align=center height=25 nowrap class=RightText colspan=2>&nbsp;");sbStartSection.append(strUserName);
	sbStartSection.append("</td></tr><tr><td bgcolor=#666666 height=1 colspan=6></td></tr></table></td></tr>");
	sbStartSection.append("<tr><td colspan=3><table border=0 width=100% cellspacing=0 cellpadding=0>");
	sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption><td width=25 height=24>S#</td><td width=90 height=24>WO Due</td><td width=100 height=24>&nbsp;Part #</td>");
	sbStartSection.append("<td  width=400>Description</td><td align=center width=30>Rev</td><td align=center width=60>UOM/<BR>Units</td>");
	sbStartSection.append("<td align=center width=60>Order<BR>Qty</td>");
	sbStartSection.append("<td align=center width=60>&nbsp;&nbsp;Price</td><td align=center width=80>Extended<BR>Price</td></tr>");
	sbStartSection.append("<tr><td colspan=9 height=1 bgcolor=#666666></td></tr>");
	sbStartSection.append("</table></td></tr></table></td><td bgcolor=#666666 width=1></td></tr></table>");

	StringBuffer sbEndSection = new StringBuffer();
	sbEndSection.append("<table width=700 cellspacing=0 cellpadding=0>");
	sbEndSection.append("<tr><td bgcolor=#666666 width=1 rowspan=8></td><td height=1 width=698></td><td bgcolor=#666666 width=1 rowspan=7></td></tr>");
	sbEndSection.append("<tr><td height=10>&nbsp;</td></tr>");
	sbEndSection.append("<tr><td height=50>&nbsp;</td></tr>");
	sbEndSection.append("<tr><td height=20 class=RightText valign=top>&nbsp;<b>");sbEndSection.append(strUserName);
	sbEndSection.append("</b><BR>&nbsp;<font size=-2>(Purchasing Agent)</font></td></tr>");
	if(strShowPaymentTerms.equalsIgnoreCase("YES")){
		sbEndSection.append("<tr><td height=20 class=RightText valign=top><BR>&nbsp;<B>Payment Terms -</B>&nbsp;");
		sbEndSection.append(strPayTerms);
		sbEndSection.append("</td></tr>");
	}
	sbEndSection.append("<tr><td height=20 class=RightText valign=top><BR>&nbsp;<B><u>Comments</u></B><BR>&nbsp;");
	sbEndSection.append(strComments);
	sbEndSection.append("<br><br></td></tr>");
	sbEndSection.append("<tr><td height=30 class=RightText valign=top>&nbsp;<B><u>NOTES</u></B>:"); 

	if(strSec199.equals("Y"))
	{
	sbEndSection.append("<BR>-<font color=red>*</font>Raw Material to be acquired by Globus Medical, Inc., upon the earlier of issuance of  material to the contractor work"); 
	sbEndSection.append("<BR>&nbsp;&nbsp;order or delivery of the materials to the contractor, in accordance with Section 1.3.1(a) of the Supplier Quality ");  
	sbEndSection.append("<BR>&nbsp;&nbsp;Agreement. ");  		 
	} 
	sbEndSection.append("<BR>-Purchase Order Number must appear on all Invoices, Packing Slips etc.");
	sbEndSection.append("<BR>-Please notify us immediately if you are unable to complete the order by date specified.");
	sbEndSection.append("<BR>-If First Article is required, Please submit First Article prior to production run. Production run to happen only after First &nbsp;Article is approved");	
	sbEndSection.append("<BR><BR></td></tr>");
	if(!strShowTermsandConditions.equals("")){
	sbEndSection.append("<tr><td height=30 class=RightText valign=top>&nbsp;"); sbEndSection.append(strShowTermsandConditions);
	sbEndSection.append("</td></tr>"); 
	}
	sbEndSection.append("<tr><td height=1 bgcolor=#666666></td></tr></table>");
	sbEndSection.append("<table border=0 width=700 cellspacing=0 cellpadding=0><tr><td align=center height=30 id=button>");
	sbEndSection.append("<input type=button value=&nbsp;Print&nbsp; name=Btn_Print class=button onClick=fnPrint();>&nbsp;&nbsp;");
	sbEndSection.append("<input type=button value=&nbsp;Close&nbsp; name=Btn_Close class=button onClick=window.close();>&nbsp;");
	sbEndSection.append("</td><tr></table>");

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: PO </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<style>
TR.PageBreak {
     page-break-after: always;
}
</style>
<script>

function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<!--StartSection starts here-->
<%out.print(sbStartSection);%>
<!--StartSection ends here-->
	<table border="0" width="700" cellspacing="0" cellpadding="0">
<%
	double dbCount = 0.0;
	intSize = alItemDetails.size();
	//dbCount = intSize%35;
	//out.print("dbCount is:"+dbCount);
	hcboVal = new HashMap();
	String strItems = "";
	String strUOM = "";
	double dbAmount = 0.0;
	double dbTotal = 0.0;
	int intQty = 0;
	double dbRawTotal = 0.0;
	
	for (int i=0;i<intSize;i++)
	{
		hcboVal = (HashMap)alItemDetails.get(i);
		strWOId = (String)hcboVal.get("WID");
		strPartNum = (String)hcboVal.get("PNUM");
		strDesc = (String)hcboVal.get("PDESC");
		strQty = (String)hcboVal.get("QTY");
		strRate = (String)hcboVal.get("RATE");
		intQty = Integer.parseInt(strQty);
		strFARFl = GmCommonClass.parseNull((String)hcboVal.get("FARFL"));
		strUOM = GmCommonClass.parseNull((String)hcboVal.get("UOMANDQTY"));
		strWORev =  GmCommonClass.parseNull((String)hcboVal.get("WOREV"));
		strVALDFL = GmCommonClass.parseNull((String)hcboVal.get("VALIDATIONFL"));
		
		strSplitVal = GmCommonClass.parseNull( (String)hcboVal.get("SPLIT_COST"));  
		//To add work order due date in PO print screen (PC-3837)
		strWoDueDate =  GmCommonClass.parseNull( (String)hcboVal.get("WODUEDATE"));
		
		if(!strSplitVal.equals(""))
		{ 	
			strRawRate = "" + Double.parseDouble(strSplitVal);			
			strFabRate	= "" + (Double.parseDouble(strRate)- Double.parseDouble(strSplitVal)); 
			strRawAmount = "" + Double.parseDouble(strRawRate)* Double.parseDouble(strQty);
			strFabAmount = 	"" + Double.parseDouble(strFabRate)* Double.parseDouble(strQty);	
			dbRawTotal = dbRawTotal + Double.parseDouble(strRawAmount);  // used for total raw material cost show PO print 
			strRawFlag = "Y";
		}
		
		dbAmount = Double.parseDouble(strRate);
		dbAmount = intQty * dbAmount; // Multiply by Qty
		strAmount = ""+dbAmount;

		dbTotal = dbTotal + dbAmount;
		strTotal = ""+dbTotal;
		if(strFARFl.equals("Y")){
			strContent = "&nbsp;&nbsp;<b>- (First Article)</b>";
		}else if(!strVALDFL.equals("")){
			strContent = "&nbsp;&nbsp;<b>- ("+strVALDFL+")</b>";
		}else {
			strContent = "";
		}
%>
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td width="25" class="RightText" height="24">&nbsp;<%=i+1%></td>
			<td width="70" class="RightText" height="20">&nbsp;<%=strWoDueDate%></td>
			<td width="70" class="RightText" height="20">&nbsp;<%=strPartNum%></td>
			<td width="400" class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%><%=strContent%></td>
			<td class="RightText"  align="center"><%=strWORev%></td>
			<td width="60" class="RightText" align="center">&nbsp;<%=strUOM%></td>
			<td width="60" class="RightText" align="center">&nbsp; <%=strQty%></td>
			<td width="60" align="right" class="RightText"> <%=GmCommonClass.getStringWithCommas(strRate)%>&nbsp;&nbsp;&nbsp;</td>
			<td width="80" align="right" class="RightText"> <%=GmCommonClass.getStringWithCommas(strAmount)%>&nbsp;</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
		<tr><td bgcolor="#eeeeee" colspan="10" height="1"></td></tr>
<%
		if(!strSplitVal.equals("")&& strPOType.equals("3100"))
		{	 
%>	
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td width="25" class="RightText" height="24">&nbsp;</td>
			<td width="70" class="RightText" height="20">&nbsp;</td>
			<td width="60" class="RightText" height="20">&nbsp;</td>
			<td width="400" class="RightText">&nbsp;100.000 - <font color=red>*</font><fmtPrint:message key="LBL_RAW_MATERIAL"/><%=GmCommonClass.getStringWithCommas(strRawRate)%> / ea. - $<%=GmCommonClass.getStringWithCommas(strRawAmount)%></td>
			<td class="RightText"  align="center"> </td>
			<td width="60" class="RightText" align="center">&nbsp; </td>
			<td width="60" class="RightText" align="center">&nbsp; </td>
			<td width="60" align="right" class="RightText"> &nbsp;&nbsp;&nbsp;</td>
			<td width="80" align="right" class="RightText">&nbsp;</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
		<tr><td bgcolor="#eeeeee" colspan="9" height="1"></td></tr>
		
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td width="25" class="RightText" height="24">&nbsp;</td>
			<td width="70" class="RightText" height="20">&nbsp;</td>
			<td width="60" class="RightText" height="20">&nbsp;</td>
			<td width="400" class="RightText">&nbsp;<fmtPrint:message key="LBL_FABRICATION_SERVICES"/> - $<%=GmCommonClass.getStringWithCommas(strFabRate)%> / ea. - $<%=GmCommonClass.getStringWithCommas(strFabAmount)%></td>
			<td class="RightText"  align="center"> </td>
			<td width="60" class="RightText" align="center">&nbsp; </td>
			<td width="60" class="RightText" align="center">&nbsp;</td>
			<td width="60" align="right" class="RightText"> &nbsp;&nbsp;</td>
			<td width="80" align="right" class="RightText">&nbsp;</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
		<tr><td bgcolor="#eeeeee" colspan="10" height="1"></td></tr>
			
<%
		}

		if (i == 29)
		{
%>
		<tr><td colspan="10" height="1" bgcolor="#666666"></td></tr>
		<tr><td colspan="10" height="20" class="RightText" align="right"><fmtPrint:message key="LBL_CONT_ON_NEXT_PAGE"/></td></tr>
		</table>
		<p STYLE="page-break-after: always"></p>
<%
		out.print(sbStartSection);
%>
		<table border="0" width="700" cellspacing="0" cellpadding="0">
<%
		} //end of IF

	}
%>
		<tr><td colspan="10" height="1" bgcolor="#666666"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1" height="1"></td>
			<td colspan="7" height="25">&nbsp;</td>
			<td width="60" class="RightTableCaption" align="center"><fmtPrint:message key="LBL_TOTAL"/></td>
			<td width="80" class="RightTableCaption" align="right"><%=strVendorCurrency%>&nbsp;<%=GmCommonClass.getStringWithCommas(strTotal)%>&nbsp;</td>
			<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1" height="1"></td>
		</tr>
		<tr><td colspan="10" height="1" bgcolor="#666666"></td></tr>
		
	<%	 // used for total raw material cost show PO print 
		if(strRawFlag.equals("Y")&& strPOType.equals("3100"))
		{	 
			strRawTotal = "" + dbRawTotal;
%>		
	<tr>
			<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1" height="1"></td> 
			<td colspan="10"  class="RightTableCaption" align="left"><fmtPrint:message key="LBL_TOTAL_RAW_MATERIAL_COST"/>: $<%=GmCommonClass.getStringWithCommas(strRawTotal)%>&nbsp;</td>
			<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1" height="1"></td> 
		</tr>
	
	<%
		 
	}
%>	
	</table>
<!--EndSection starts here-->
<%out.print(sbEndSection);%>
<!--EndSection ends here-->

<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>
<%@ include file="/common/GmFooter.inc"%>
</HTML>
