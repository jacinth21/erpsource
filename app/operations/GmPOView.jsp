<!-- \operations\GmPOView.jsp -->
 <%
/**********************************************************************************
 * File		 		: GmPOView.jsp
 * Desc		 		: This screen is used for the Packing Slip
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@page import="java.util.Date"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<%@ taglib prefix="fmtPOView" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPOView:setLocale value="<%=strLocale%>"/>
<fmtPOView:setBundle basename="properties.labels.operations.GmPOView"/>

<%
	response.setCharacterEncoding("UTF-8");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	String stroperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	String strWikiTitle = GmCommonClass.getWikiTitle("PURCHASE_ORDER_VIEW");

	String strAccessId = (String)session.getAttribute("strSessAccLvl") ==null?"":(String)session.getAttribute("strSessAccLvl");
	String strDeptId = 	(String)session.getAttribute("strSessDeptId") == null?"":(String)session.getAttribute("strSessDeptId");
	String strAccess = 	GmCommonClass.parseNull((String)request.getAttribute("ACCESSFL"));
	String strSessApplDateFmt = strGCompDateFmt;
	String strCloseFlag = GmCommonClass.parseNull((String)request.getAttribute("hCloseFlag"));
    strCountryCode = GmCommonClass.parseNull((String)GmCommonClass.countryCode);	
	int intAccId = Integer.parseInt(strAccessId);
	boolean editFl = false;
	if (strDeptId.equals("Z") || strAccess.equals("Y") || (strDeptId.equals("W") && intAccId > 3))
	{
		editFl = true;
	}
	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");

	HashMap hmPODetails = new HashMap();

	ArrayList alItemDetails = new ArrayList();

	String strShade = "";

	String strPOId = "";
	String strVendorId = "";
	String strVendorAdd = "";
	String strPODate = "";
	String strReqdDate = "";
	String strPOTotal = "";
	String strComments = "";
	
	String strWOId = "";
	String strPartNum = "";
	String strDesc = "";
	String strPrice = "";
	String strQty = "";
	String strRate = "";
	String strAmount = "";
	String strUser = "";
	String strTotal = "";
	String strUserName = "";
	String strFooter = "";
	String strDocRev = "";
	String strDocActiveFl = "";
	String strWORev = "";
	String strPOTypeNm = "";
	String strWOLog = "";
	Date dtReqdDate = null;
	String strVendorCurrency = "";
	String strVALDFL = "";
	String strContent = "";
	String strPublishChecked = "";
	String strPublishFl = "";
	String strPublishedby = "";
	String strStatus = "";
	String strPublishedDt = "";
	String VPAccessfl ="";
	String strPublishDisabled ="disabled";
	String strWoDueDt = "";
	
	if (hmReturn != null)
	{
		hmPODetails = (HashMap)hmReturn.get("PODETAILS");
		alItemDetails = (ArrayList)hmReturn.get("ITEMDETAILS");
		strPOId = (String)hmPODetails.get("POID");
		strVendorId = (String)hmPODetails.get("VID");
		strVendorAdd = (String)hmPODetails.get("VNAME");
		strPODate = GmCommonClass.getStringFromDate((Date)hmPODetails.get("PODATE"),strGCompDateFmt);
		strReqdDate =  GmCommonClass.getStringFromDate((Date)hmPODetails.get("RQDATE"),strGCompDateFmt);
		strPOTotal = (String)hmPODetails.get("TOTAL");
		strUser = (String)hmPODetails.get("CUSER");
		strComments = (String)hmPODetails.get("COMMENTS");
		strUser = strUser.concat(".gif");
		strUserName = (String)hmPODetails.get("CNAME");
		strPOTypeNm = (String)hmPODetails.get("POTYPENM");
		strVendorCurrency = (String)hmPODetails.get("VENDCURR");		
		dtReqdDate = GmCommonClass.getStringToDate(strReqdDate,strSessApplDateFmt);
		strPublishFl = GmCommonClass.parseNull((String) hmPODetails.get("PUBFL"));
		strPublishedby = GmCommonClass.parseNull((String) hmPODetails.get("PUBLBY"));
		strStatus = GmCommonClass.parseNull((String) hmPODetails.get("STATUS"));
		strPublishedDt =  GmCommonClass.getStringFromDate((Date) hmPODetails.get("PUBLDT"),strGCompDateFmt);
		VPAccessfl = GmCommonClass.parseNull((String) hmPODetails.get("VENFL"));
		strPublishChecked = strPublishFl.equals("Y")?"checked":"";
		if(!strPublishFl.equals("Y")){
			strPublishDisabled = "";
		}
	}

	int intSize = 0;
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: PO </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=stroperationsJsPath%>/GmPOView.js"></script>
<style>
TR.PageBreak {
     page-break-after: always;
}
</style>
<script>
var poId = '<%=strPOId%>';

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmOrder" method="POST" action="<%=strServletPath%>/GmWOServlet">
<input type="hidden" name="hWOId" value="">
<input type="hidden" name="hPOId" value="<%=strPOId%>">
<input type="hidden" name="hVendId" value="<%=strVendorId%>">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hPublishFL" value="<%=strPublishFl%>">
	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr>
			<td bgcolor="#666666" colspan="3"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="4"></td>
			<td height="25" class="RightDashBoardHeader"><fmtPOView:message key="LBL_PURCHASE_ORDER_VIEW"/><img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
			<td bgcolor="#666666" width="1" rowspan="4"></td>
		</tr>
			<td bgcolor="#666666" colspan="3"></td>
		<tr>
			<td width="698" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="2">
						<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr>
									<td class="RightTableCaption" height="24" align="Right" width="250"><fmtPOView:message key="LBL_PO_ID"/>:&nbsp;</td>
									<td class="RightTableCaption">&nbsp;<%=strPOId%></td>
								</tr>
								<tr><td class="LLine" height="1" colspan="2"></td>
								<tr class="Shade">
									<td class="RightTableCaption" height="24" align="Right" width="250"><fmtPOView:message key="LBL_VENDOR"/>:&nbsp;</td>
									<td class="RightText">&nbsp;<%=strVendorAdd%></td>
								</tr>
								<tr><td class="LLine" height="1" colspan="2"></td>
								<tr>
									<td class="RightTableCaption" height="24" align="Right" width="250"><fmtPOView:message key="LBL_PO_TYPE"/>:&nbsp;</td>
									<td class="RightText">&nbsp;<%=strPOTypeNm%></td>
								</tr>
								<tr><td class="LLine" height="1" colspan="2"></td>
								<tr class="Shade">
									<td class="RightTableCaption" height="24" align="Right" width="250"><fmtPOView:message key="LBL_RAISED_BY"/>:&nbsp;</td>
									<td class="RightText">&nbsp;<%=strUserName%></td>
								</tr>
								<tr><td class="LLine" height="1" colspan="2"></td>
								<tr>
									<td class="RightTableCaption" height="24" align="Right" width="250"><fmtPOView:message key="LBL_PO_DATE"/>:&nbsp;</td>
									<td class="RightText">&nbsp;<%=strPODate%></td>
								</tr>
								<tr><td class="LLine" height="1" colspan="2"></td>
								<tr class="Shade">
									<td class="RightTableCaption" height="24" align="Right" width="250"><fmtPOView:message key="LBL_REQUIRED_DATE"/>:&nbsp;</td>
<%
											if (editFl)
											{
%>
												<td>&nbsp;<gmjsp:calendar textControlName="Txt_ReqDate" gmClass="InputArea" textValue="<%=dtReqdDate==null?null:new java.sql.Date(dtReqdDate.getTime())%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
												</td>
<%
											}else{
%>
												<td class="RightText">&nbsp;<%=strReqdDate%><Br></td>
<%
											}
%>
								</tr>
								<% if(VPAccessfl.equals("Y") && editFl){ %>
								<tr><td class="Line" height="1" colspan="2"></td></tr>
								<tr class="ShadeRightTableCaption" height="24">
								<td colspan="2">&nbsp;<fmtPOView:message key="LBL_PUBLISH_DETAILS"/>:</td>
								</tr>
								<tr><td class="Line" height="1" colspan="2"></td></tr>
								<tr >
				        <td class="RightTableCaption" align="right" HEIGHT="24"><fmtPOView:message key="LBL_PUBLISH"/>?:&nbsp;</td>
                        <td><input type="checkbox" id="Chk_PublishFl" size="30" <%=strPublishChecked%> <%=strPublishDisabled%> name="Chk_PublishFl" value="Y" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >                </td>
                        </tr>
                  <tr>
					<td class="LLine" height="1" colspan="2"></td>
				<tr class="Shade">
					<td class="RightTableCaption" height="24" align="Right" width="250"><fmtPOView:message key="LBL_PUBLISH_BY"/>:&nbsp;</td>
					<td class="RightText">&nbsp;<%=strPublishedby%></td>
				</tr>
				<tr>
					<td class="LLine" height="1" colspan="2"></td>
				<tr>
					<td class="RightTableCaption" height="24" align="Right" width="250"><fmtPOView:message key="LBL_PUBLISH_DT"/>:&nbsp;</td>
					<td class="RightText">&nbsp;<%=strPublishedDt%></td>
				</tr> 
				<tr>
					<td class="LLine" height="1" colspan="2"></td>
				<tr class="Shade">
					<td class="RightTableCaption" height="24" align="Right" width="250"><fmtPOView:message key="LBL_STATUS"/>:&nbsp;</td>
					<td class="RightText">&nbsp;<%=strStatus%></td>
				</tr> 	
				<%	} %>
				<tr><td class="Line" height="1" colspan="2"></td></tr>		
							</table>
						</td>
					</tr>
					<tr>
						<td align="center" colspan="2" valign="top">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr class="ShadeRightTableCaption">
									<td width="25" height="24"><fmtPOView:message key="LBL_S"/></td>
									<td width="60" height="24">&nbsp;<fmtPOView:message key="LBL_PART"/></td>
									<td width="400"><fmtPOView:message key="LBL_DESCRIPTION"/></td>
									<td align="center" nowrap><fmtPOView:message key="LBL_WORK_ORDER"/></td>
									<td align="center" width="60"><fmtPOView:message key="LBL_WO_DUE"/></td>
									<td align="center" width="60"><fmtPOView:message key="LBL_WO"/><BR><fmtPOView:message key="LBL_REV"/></td>
									<td align="center" width="60"><fmtPOView:message key="LBL_UOM"/>/<BR><fmtPOView:message key="LBL_UNITS"/></td>
									<td align="center" width="60"><fmtPOView:message key="LBL_ORDER"/><BR><fmtPOView:message key="LBL_QTY"/></td>
									<td align="center" width="60">&nbsp;&nbsp;<fmtPOView:message key="LBL_PRICE"/></td>
									<td align="center" width="80"><fmtPOView:message key="LBL_EXTENDED"/><BR><fmtPOView:message key="LBL_PRICE"/></td>
								</tr>
								<tr>
									<td colspan="10" height="1" bgcolor="#666666"></td>
								</tr>
<%
			  		intSize = alItemDetails.size();
					hcboVal = new HashMap();
					String strItems = "";
					double dbAmount = 0.0;
					double dbTotal = 0.0;
					int intQty = 0;
					String strFARFl = "";
					String strUOM = "";

			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alItemDetails.get(i);
						strWOId = (String)hcboVal.get("WID");
			  			strPartNum = (String)hcboVal.get("PNUM");
						strDesc = (String)hcboVal.get("PDESC");
						strQty = GmCommonClass.parseZero((String)hcboVal.get("QTY"));
						strRate = GmCommonClass.parseZero((String)hcboVal.get("RATE"));
						strFARFl = GmCommonClass.parseNull((String)hcboVal.get("FARFL"));						
						strUOM = GmCommonClass.parseNull((String)hcboVal.get("UOMANDQTY"));
						strWORev =  GmCommonClass.parseNull((String)hcboVal.get("WOREV"));
						//PC-3447-WO Due Date Update in Purchase Order-Edit
						strWoDueDt = GmCommonClass.parseNull((String) hcboVal.get("WODUEDATE"));
						 								
						strWOLog =  GmCommonClass.parseNull((String)hcboVal.get("WLOG"));
						strVALDFL = GmCommonClass.parseNull((String)hcboVal.get("VALIDATIONFL"));					
						
						strFooter = GmCommonClass.parseNull((String)hcboVal.get("FOOTER"));
						String strArr[] = strFooter.split("\\^");
						strFooter = strArr[0];
						strDocRev = strArr[1];
						strDocActiveFl = strArr[2];	
						
						intQty = Integer.parseInt(strQty);
						dbAmount = Double.parseDouble(strRate);
						dbAmount = intQty * dbAmount; // Multiply by Qty
						strAmount = ""+dbAmount;

						dbTotal = dbTotal + dbAmount;
						strTotal = ""+dbTotal;
						strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
						//strShade = i == 20?"class=PageBreak":""; //For alternate Shading of rows
						if(strFARFl.equals("Y")){
							strContent = "&nbsp;&nbsp;<b>- (First Article)</b>";
						}else if(!strVALDFL.equals("")){
							strContent = "&nbsp;&nbsp;<b>- ("+strVALDFL+")</b>";
						}else {
							strContent = "";
						}
%>
								<tr>
									<td class="RightText">&nbsp;<%=i+1%></td>
									<td height="20">&nbsp;<fmtPOView:message key="IMG_CLICK_VIEW_WORK_ORDER" var="varClickviewWorkOrder"/><a class="RightText" title="${varClickviewWorkOrder}-<%=strWOId%>" href="javascript:fnWOView('<%=strWOId%>','<%=strDocRev%>','<%=strDocActiveFl%>');"><%=strPartNum%></a></td>
									<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%><%=strContent%></td>
									<td class="RightText" nowrap>&nbsp;<%=strWOId%><fmtPOView:message key="IMG_CLICK_ENTER_VIEW_COMMENTS" var="varClickEnterViewComments"/><img id="imgEdit" style="cursor:hand"  
				 						<% if (strWOLog.equals("N"))
				 						{ %>
				 							src="<%=strImagePath%>/phone_icon.jpg" 
				 						<%} else {%>	
				 							src="<%=strImagePath%>/phone-icon_ans.gif" 
				 						<% }%>
				 						title="${varClickEnterViewComments}" width="22" height="20" onClick="javascript:fnOpenLog('<%=strWOId%>' )"/> 
									</td>
									<td class="RightText" align="center">&nbsp;<%=strWoDueDt%></td>
									<td class="RightText"  align="center"><%=strWORev%></td>
									<td class="RightText" align="center">&nbsp;<%=strUOM%></td>
									<td class="RightText" align="center">&nbsp;<%=strQty%></td>
									<td align="right" class="RightText"><%=GmCommonClass.getStringWithCommas(strRate)%>&nbsp;</td>
									<td align="right" class="RightText"><%=GmCommonClass.getStringWithCommas(strAmount)%>&nbsp;</td>
								</tr>
								<tr><td colspan="10" height="1" bgcolor="#eeeeee"></td></tr>
<%
					}
							strFooter = GmCommonClass.parseNull((String)hcboVal.get("FOOTER"));
							String strArr[] = strFooter.split("\\^");
							strFooter = strArr[0];
							strDocRev = strArr[1];
							strDocActiveFl = strArr[2];	
							String AllWOPrint = "fnAllWOPrint('"+strDocRev+"','"+strDocActiveFl+"')";
%>
								<tr>
									<td colspan="10" height="1" bgcolor="#666666"></td>
								</tr>
								<tr>
									<td colspan="8" height="25">&nbsp;<input type="hidden" name="hPOTotal" value="<%=strTotal%>"></td>
									<td class="RightTableCaption" align="center"><fmtPOView:message key="LBL_TOTAL"/></td>
									<td class="RightTableCaption" align="right"><%=strVendorCurrency%>&nbsp;<%=GmCommonClass.getStringWithCommas(strTotal)%>&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" height="1" bgcolor="#666666"></td>
					</tr>
					<tr>
						<td colspan="2" height="10">&nbsp;</td>
					</tr>
<%
								if (editFl)
								{
%>	
					<tr>
						<td class="RightText" valign="top" HEIGHT="24" align="right"><fmtPOView:message key="LBL_ADDITIONAL_DESCRIPTION"/>:</td>
						<td>&nbsp;<textarea name="Txt_Comments" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" rows=5 cols=45 value="" tabindex=24><%=strComments%></textarea><br>
						</td>
					</tr>
					<tr>
						<td colspan="2" height="30" align="center">
						<fmtPOView:message key="BTN_SUBMIT" var="varSubmit"/>
							<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" name="Btn_Submit" gmClass="button" buttonType="Save" onClick="javascript:fnSubmit();" />
						</td>
					</tr>
<%
								}else{
%>
					<tr>
						<td class="RightText" HEIGHT="24" align="right"><fmtPOView:message key="LBL_ADDITIONAL_INSTRUCTIONS"/> :</td>
						<td class="RightText" width="300">&nbsp;<%=strComments%><br>
						</td>
					</tr>
<%
								}
%>
				</table>

			</td>
		</tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>

    </table>

	<table border="0" width="700" cellspacing="0" cellpadding="0" >
		<tr>
			<td align="center" height="30" id="button">
			<%if(!strCloseFlag.equals("Y")){ %>
			<fmtPOView:message key="BTN_RETURN" var="varReturn"/>
				<gmjsp:button value="&nbsp;${varReturn}&nbsp;" name="Btn_Return" gmClass="button"  buttonType="Load" onClick="history.go(-1)" />&nbsp;&nbsp;
				<%}%>
				<%if (strCountryCode.equals("en")){  %>
				<fmtPOView:message key="BTN_PRINT_PO" var="varPrintPO"/>
				<gmjsp:button value="${varPrintPO}" name="Btn_ViewPrint" gmClass="button" buttonType="Load" onClick="fnViewPrint();" />&nbsp;&nbsp;
				<fmtPOView:message key="BTN_PRINT_ALL_WO" var="varPrintAllWO"/>
				<gmjsp:button value="${varPrintAllWO}" name="Btn_ViewPrintWO" gmClass="button" buttonType="Load" onClick="<%=AllWOPrint%>" />&nbsp;&nbsp;
					<%if (editFl){%>
					<fmtPOView:message key="BTN_PRINT_FAX_COVER" var="varPrintFAXCover"/>
						<gmjsp:button value="${varPrintFAXCover}" name="Btn_FaxPrint" gmClass="button" buttonType="Load" onClick="fnViewFaxPrint();" />
					<%}%>
				<%} else{ %>
				<fmtPOView:message key="BTN_PRINT_PO_VERSION" var="varPrintPOVersion"/>
				<gmjsp:button value="${varPrintPOVersion}" name="Btn_ViewPrint" gmClass="button" buttonType="Load" onClick="fnViewPrint();" />&nbsp;&nbsp;
				<%} %>
				<%if(strCloseFlag.equals("Y")){ %>
				<fmtPOView:message key="BTN_CLOSE" var="varClose"/>
				<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_ViewClose" gmClass="button" buttonType="Load" onClick="fnViewClose();" />
				<%} %>
			</td>
		<tr>
	</table>
</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>



