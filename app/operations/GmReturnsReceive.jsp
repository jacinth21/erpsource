<!--\operations\GmReturnsReceive.jsp  -->

 <%
/**********************************************************************************
 * File		 		: GmReturnsReceive.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ taglib prefix="fmtReturnReceive" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtReturnReceive:setLocale value="<%=strLocale%>"/>
<fmtReturnReceive:setBundle basename="properties.labels.operations.GmReturnsReceive"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction");

	ArrayList alReturnsDashboard = new ArrayList();
	alReturnsDashboard = (ArrayList)hmReturn.get("PENDRETURNS");
	
	
	String strVendorName = "";
	String strDHRId = "";
	String strPartNum = "";
	String strDesc = "";
	String strFlag = "";
	String strControlFl = "";
	String strInspFl = "";
	String strPackFl = "";
	String strVeriFl = "";
	String strLine = "";
	String strRAId = "";
	String strReturnType = "";
	String strReturnReason = "";
	String strExpectedDate = "";
	String strNCMRId = "";
	String strCreatedDate = "";
	String strCreditDate = "";

	HashMap hmTempLoop = new HashMap();
	String strNextId = "";
	int intCount = 0;
	boolean blFlag = false;
	String strIniFlag = "";
	String strWIPFlag = "";
	String strTemp = "";
	boolean blFlag1 = false;
	int intDispCount = 0;
	int intInspCount = 0;
	int intPackCount = 0;
	int intVeriCount = 0;
	int intConCount = 0;
	int intCreditCount = 0;
	int intReprocessCount = 0;	

	int intReturnLength = alReturnsDashboard.size();
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Receive Returns </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>

function fnCallEditReturn(val)
{
//	document.frmAccount.hRAId.value = val;
fnStartProgress('Y');
	document.frmAccount.submit();
}

function Toggle(val)
{
	
	var obj = eval("document.all.div"+val);
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		trobj.className="ShadeRightTableCaptionBlue";
		//tabobj.style.background="#c6e6fe";
		//tabobj.style.background="#d7ecfd";
		tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		trobj.className="";
		tabobj.style.background="#ffffff";
	}
}

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmReturnReceiveServlet">
<input type="hidden" name="hRAId" value="">
<input type="hidden" name="hAction" value="EditLoadDash">

	<table border="0" width="900" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" bgcolor="#666666"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td width="900" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="7" height="25" class="RightDashBoardHeader"><fmtReturnReceive:message key="LBL_RECEIVE_RETURNS"/></td>
					</tr>
					<tr><td class="Line" colspan="7"></td></tr>
					<tr>
						<td colspan="7" height="25" align="center" class = "RightTextBlue"><fmtReturnReceive:message key="LBL_PLEASE_ENTER_RAID_OR_BROWSE_THROUGH_RETURN_TYPE"/> </td>
					</tr>
					
					<tr><td class="Line" colspan="7"></td></tr>
					
					<tr height = "30">
						
						
						<td colspan ="7" class="RightText" HEIGHT="25" align="left">&nbsp;<fmtReturnReceive:message key="LBL_ENTER_RAID_#"/>:
						&nbsp;&nbsp;
						<input type="text" size="15" value="GM-RA-" name="Txt_RAID" class="InputArea"  onFocus="changeBgColor(this,'#AACCE8');" 
						onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;
						<fmtReturnReceive:message key="LBL_LOAD_RA" var="varLoadRA"/>
						<gmjsp:button 
						value="${varLoadRA}" gmClass="button" onClick="javascript:fnCallEditReturn(document.frmAccount.Txt_RAID.value);" tabindex="13" buttonType="Load" />
						</td>
						
					</tr>

					<tr><td class="Line" colspan="7"></td></tr>
					
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" width="60" align="center" nowrap><fmtReturnReceive:message key="LBL_RETURN"/><BR><fmtReturnReceive:message key="LBL_TYPE"/></td>
						<td width="80" nowrap align="center"><fmtReturnReceive:message key="LBL_DISTRIBUTOR"/>/<br><fmtReturnReceive:message key="LBL_ACCOUNT"/></td>
						<td HEIGHT="24" width="60"><fmtReturnReceive:message key="LBL_RA_ID"/></td>
						<td width="70" align="center"><fmtReturnReceive:message key="LBL_INITIATED"/><br><fmtReturnReceive:message key="LBL_DATE"/></td>
						<td width="70" align="center"><fmtReturnReceive:message key="LBL_EXPECTED"/><br><fmtReturnReceive:message key="LBL_DATE"/></td>
						<td width="130" align="center"><fmtReturnReceive:message key="LBL_REASON"/></td>
						<td width="330"><fmtReturnReceive:message key="LBL_COMMENTS"/></td>
					</tr>
<%
						if (intReturnLength > 0)
						{
							hmTempLoop = new HashMap();
							strNextId = "";
							intCount = 0;
							blFlag = false;
							blFlag1 = false;
							strTemp = "";
							String strTypeId = "";
							String strDistId = "";
							String strDistName = "";
							String strAccId = "";
							String strAccName = "";
							String strNextDistId = "";
							String strComments = "";
							String strDateInitiated = "";
							String strPerson = "";
							String strShade = "";

							int intDistCount = 0;
							boolean bolDistFl = false;
													
							HashMap hmReturnLoop = new HashMap();
							for (int k = 0;k < intReturnLength ;k++ )
							{
								hmReturnLoop = (HashMap)alReturnsDashboard.get(k);

								if (k<intReturnLength-1)
								{
									hmTempLoop = (HashMap)alReturnsDashboard.get(k+1);
									strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("TYPEID"));
									strNextDistId = GmCommonClass.parseNull((String)hmTempLoop.get("DID"));
									strNextDistId = strNextDistId.equals("")?GmCommonClass.parseNull((String)hmTempLoop.get("ACCID")):strNextDistId;
								}
								
								strTypeId = GmCommonClass.parseNull((String)hmReturnLoop.get("TYPEID"));
								strRAId = GmCommonClass.parseNull((String)hmReturnLoop.get("RAID"));
								strPerson = GmCommonClass.parseNull((String)hmReturnLoop.get("PER"));
								strDateInitiated = GmCommonClass.parseNull((String)hmReturnLoop.get("CDATE"));
								strReturnType = GmCommonClass.parseNull((String)hmReturnLoop.get("TYPE"));
								strReturnReason = GmCommonClass.parseNull((String)hmReturnLoop.get("REASON"));
								strComments = GmCommonClass.parseNull((String)hmReturnLoop.get("COMMENTS"));
								strExpectedDate = GmCommonClass.parseNull((String)hmReturnLoop.get("EDATE"));
								strDistId = GmCommonClass.parseNull((String)hmReturnLoop.get("DID"));
								strDistName = GmCommonClass.parseNull((String)hmReturnLoop.get("DNAME"));
								strAccId = GmCommonClass.parseNull((String)hmReturnLoop.get("ACCID"));
								strAccName = GmCommonClass.parseNull((String)hmReturnLoop.get("ANAME"));
								if (strDistId.equals(""))
								{
									strDistId = strAccId;
									strDistName = strAccName;
									
								}

								if (strTypeId.equals(strNextId))
								{
									intCount++;
									strLine = "<TR><TD colspan=7 height=1 class=Line></TD></TR>";
								}
								else
								{
									blFlag1 = true;
									strLine = "<TR><TD colspan=7 height=1 class=Line></TD></TR>";
									if (intCount > 0)
									{
										strReturnType = "";
										strLine = "";
									}
									else
									{
										blFlag = true;
									}
									intCount = 0;
									//
								}
								
								if (strDistId.equals(strNextDistId))
								{
									intDistCount++;
								}
								else
								{
									if (intDistCount > 0)
									{
										strDistName = "";
										strLine = "";
									}
									else
									{
										bolDistFl = true;
									}
									intDistCount = 0;
								}
								
																
								if (intCount > 1)
								{
									strReturnType = "";
									strLine = "";
								}
							
								out.print(strLine);
								strShade	= (k%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows
								if (intCount == 1 || blFlag)
								{
%>
					<tr id="trret<%=strTypeId%>">
					<fmtReturnReceive:message key="LBL_CLICK_HERE_TO_EXPAND" var="varClickHereToExpand"/>
						<td colspan="7" height="18" class="RightText">&nbsp;<A class="RightText" title="${varClickHereToExpand}" href="javascript:Toggle('ret<%=strTypeId%>')"><%=strReturnType%></a></td>
					</tr>
					<tr>
						<td colspan="7"><div style="display:none" id="divret<%=strTypeId%>">
							<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tabret<%=strTypeId%>">
<%
								blFlag = false;
								}
								if (intDistCount == 1 || bolDistFl)
								{
%>
								<TR><TD colspan=7 height=1 bgcolor="#cccccc"></TD></TR>
								<tr id="trDist<%=strTypeId%><%=strDistId%>">
									<td nowrap width="68">&nbsp;</td>
									<fmtReturnReceive:message key="LBL_CLICK_HERE_TO_EXPAND" var="varClickHereToExpand"/>
									<td width="95%" colspan="6" height="20" class="RightText">&nbsp;<A class="RightText" title="${varClickHereToExpand}" href="javascript:Toggle('Dist<%=strTypeId%><%=strDistId%>')"><%=strDistName%></a></td>
								</tr>
								<tr>
									<td colspan="7"><div style="display:none" id="divDist<%=strTypeId%><%=strDistId%>">
										<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tabDist<%=strTypeId%><%=strDistId%>">
<%
								bolDistFl = false;
								}
%>
								<tr <%=strShade%>>
									<td width="60" class="RightText">&nbsp;</td>
									<td width="60" class="RightText">&nbsp;</td>
									<td width="80" HEIGHT="20" class="RightText">&nbsp;<a class=RightText href= javascript:fnCallEditReturn('<%=strRAId%>');><%=strRAId%></td>
									<td width="70" class="RightText">&nbsp;<%=strDateInitiated%></td>
									<td width="70" class="RightText">&nbsp;<%=strExpectedDate%></td>
									<td width="130" class="RightText"><%=strReturnReason%></td>
									<td width="330" class="RightText">&nbsp;<%=strComments%></td>
								</tr>
								<TR><TD colspan=7 height=1 bgcolor=#cccccc></TD></TR>
<%					
								if (intDistCount == 0 )
								{
%>
							</table></div></td></tr>
<%
								}

								if (blFlag1 || k+1==intReturnLength)
								{
%>
							</table></div>
						</td>
					</tr>
<%
								}
							blFlag1	= false;
							}
						}else{
%>
					<tr>
						<td colspan="7" align="center" class="RightTextBlue" height="30"><fmtReturnReceive:message key="LBL_NO_RETURNS_INITIATED_AT_THIS_MOMENT"/>!</td>
					</tr>
<%
						}
%>
							</table></div>
						</td>
					</tr>				
				</table>
			</td>
			<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1"></td>
		</tr>
		<tr><td colspan="7" bgcolor="#666666"></td></tr>
	</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
