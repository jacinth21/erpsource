<%
/**********************************************************************************
 * File		 		: GmFieldSalesLoaners.jsp
 * Desc		 		: This screen is used for the displaying loaner status report
 * Version	 		: 1.1
 * author			: Manikandan Rajasekaran
 * 
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.text.*" %>
<!-- WEB-INF added JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib prefix="fmtLoanerUsageSummary" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- operations\loaners\GmLoanerUsageSummary.jsp -->
<fmtLoanerUsageSummary:setLocale value="<%=strLocale%>"/>
<fmtLoanerUsageSummary:setBundle basename="properties.labels.operations.loaners.GmLoanerUsageSummary"/>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%
    String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	String strWikiTitle = GmCommonClass.getWikiTitle("LOANER_USAGE_SUMMARY");
	String strCurrentdate = GmCommonClass.parseNull((String)session.getAttribute("strSessTodaysDate"));
	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	
%>
<bean:define id="strOpt" name="frmFieldSalesLoaners" property="strOpt" type="java.lang.String"></bean:define>
<bean:define id="strScreenType" name="frmFieldSalesLoaners" property="hScreenType" type="java.lang.String"></bean:define>
<bean:define id="gridData" name="frmFieldSalesLoaners" property="strXmlData" type="java.lang.String"></bean:define>

<html>
<head>

<title>Loaner Usage Summary</title>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">

<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script type="text/javascript" src="<%=strOperationsJsPath%>/GmLoanerUsageSummary.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>


<script>
var gridObjData ='<%=gridData%>';
var gridObj;
var format = '<%=strApplDateFmt%>';
var currentdate = '<%=strCurrentdate%>';

</script>
</head>
<BODY leftmargin="20" topmargin="10" onload="javascript:fnOnPageLoad();">
<html:form action="/gmFieldSalesLoaners.do?method=fetchLoanerUsageSummary" method="post" styleId="form">
<html:hidden property="strOpt" name="frmFieldSalesLoaners" />
<html:hidden property="distid" name="frmFieldSalesLoaners" />
<html:hidden property="hScreenType" name="frmFieldSalesLoaners" />


<table cellspacing="0"  cellpadding="0" border="0"  class="DtTable1000">
	

	<tr>
		<td class="RightDashBoardHeader" height="25" colspan="5"><fmtLoanerUsageSummary:message key="LBL_LOANER_USAGE_SUMMARY"/></td>
		<td height="25" align="right" class=RightDashBoardHeader >
		<fmtLoanerUsageSummary:message key="IMG_ALT_HELP" var = "varHelp"/>
		<img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
 					title='${varHelp}' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
		</td>
		
	</tr>
	
	<tr>
		<td colspan="6">
		<table cellspacing="0" cellpadding="0" class="DtTable1000">
			<tr><td><jsp:include page="/sales/GmSalesFilters.jsp" >
						<jsp:param name="HIDE" value="SYSTEM" />
						<jsp:param name="FRMNAME" value="frmFieldSalesLoaners" />
						<jsp:param name="URL" value="/gmFieldSalesLoaners.do?method=fetchLoanerUsageSummary" />
						<jsp:param name="HIDEBUTTON" value="HIDEGO" />
		</jsp:include>
	  </td>
	</tr>
	</table>
	</td>
	</tr>
	<tr><td class="LLine" colspan="6"></td></tr>
	<tr class="oddshade">
		<td class="RightTableCaption" height="25" colspan="1" align="right">
		<fmtLoanerUsageSummary:message key="LBL_SET_NAME" var="varSetName"/>
			<gmjsp:label type="RegularText" SFLblControlName="${varSetName}" td="false" /></td>
		<td class="RightTableCaption" height="25" colspan="1">
			&nbsp;<gmjsp:dropdown controlName="setName" SFFormName="frmFieldSalesLoaners" SFSeletedValue="setName"
				tabIndex= "1" SFValue="alSets" codeId="ID"  codeName="NAME" defaultValue= "[Choose One]" width="200"/></td>
				
				<td class="RightTableCaption" height="25" colspan="1"   align="right"><fmtLoanerUsageSummary:message key="LBL_SET_ID" var="varSetID"/><gmjsp:label type="RegularText" SFLblControlName="${varSetID}" td="false" /></td>
		<td colspan="3">&nbsp;<html:textarea  property="setId"  cols="50" rows="2" style="height:50px"
									styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" tabindex="2" onblur="changeBgColor(this,'#ffffff');" /></td>
		
		
		
			
	</tr>
	<tr><td class="LLine" colspan="7"></td></tr>
	<tr class="evenshade">
		<td class="RightTableCaption" height="25" colspan="1"  align="right"><fmtLoanerUsageSummary:message key="LBL_LOAN_FROM_DATE"/>:</td>
		<td colspan="1">&nbsp;<gmjsp:calendar SFFormName="frmFieldSalesLoaners" controlName="loanfromdt" gmClass="InputArea" 
									tabIndex= "3" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"  />
								</td>	
		<td class="RightTableCaption" height="25" colspan="1"  align="right"><fmtLoanerUsageSummary:message key="LBL_LOAN_TO_DATE"/>:</td>
		<td colspan="1">&nbsp;<gmjsp:calendar SFFormName="frmFieldSalesLoaners" controlName="loantodt" gmClass="InputArea" 
									tabIndex= "4" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"  />
								</td>
		<td height="25" colspan="1" align ="center" >
		<fmtLoanerUsageSummary:message key="BTN_LOAD" var="varLoad"/>
		<gmjsp:button name="Btn_Load" value="${varLoad}" style="width: 5em; height: 23px;" gmClass="Button"  onClick="javascript:fnLoad();" tabindex="5" buttonType="Load" />
	</td>
	</tr>	
	<tr id="datagrid" style="display:none">
		<td colspan="6"><div  id="dataGridDiv"   width="1000px" height="460px"></div> </td>
	</tr>
	
	<tr id="Export" style="display: none;">						
		<%if (strScreenType.equals("")){%>
		 					
		<td align="center" colspan="6" ><br>
	       <div  id="Export"  class='exportlinks'><fmtLoanerUsageSummary:message key="DIV_EXPORT_OPT"/>: <img src='img/ico_file_excel.png' />&nbsp; <a href="#" onclick="fnExport();"> <fmtLoanerUsageSummary:message key="DIV_EXCEL"/> </a> </div> </td>
	  <%}else{ %>
	  <td height="25" colspan="6" align ="center" >
	  <fmtLoanerUsageSummary:message key="BTN_CLOSE" var="varClose"/>
		<gmjsp:button name="Btn_Close" value="${varClose}" style="width: 5em; height: 23px;" gmClass="Button"  onClick="javascript:fnClose();" tabindex="5" buttonType="Load" />
		</td>
		
		<%} %>
    </tr>
   
    <tr style="display: none;" id="mess">
	 <td colspan="7"  align="center"><br> <div><font color="red" size="4px" face="sans-serif"><strong><fmtLoanerUsageSummary:message key="MSG_NO_DATA"/>  </strong></font></div></td>
	</tr>

</table>
	
		
	
  
</html:form>
<%@ include file="/common/GmFooter.inc" %>

</body>
</html>