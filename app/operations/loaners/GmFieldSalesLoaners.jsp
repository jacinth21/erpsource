<%
/**********************************************************************************
 * File		 		: GmFieldSalesLoaners.jsp
 * Desc		 		: This screen is used for the displaying loaner status report
 * Version	 		: 1.1
 * author			: Brinal
 * 
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.text.*" %>
<!-- WEB-INF added JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib prefix="fmtFieldSalesLoaners" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!--operations\loaners\GmFieldSalesLoaners.jsp-->
<fmtFieldSalesLoaners:setLocale value="<%=strLocale%>"/>
<fmtFieldSalesLoaners:setBundle basename="properties.labels.operations.loaners.GmFieldSalesLoaners"/>


<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	//String strWikiTitle = GmCommonClass.getWikiTitle("CONTROL_NUMBER_EXCEPTION_REPORT");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strUserName = GmCommonClass.parseNull((String)(String)session.getAttribute("strSessShName"));
	String strAppldateFmt = GmCommonClass.parseNull((String)(String)session.getAttribute("strSessApplDateFmt"));
	strAppldateFmt = strAppldateFmt + " K:m:s a";
	SimpleDateFormat df = new SimpleDateFormat(strAppldateFmt);
	//SimpleDateFormat df = new SimpleDateFormat(strDf);
	String strDate = (String)df.format(new java.util.Date());
	

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Field Sales report</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css"> 
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">
<script type="text/javascript" src="<%=strOperationsJsPath%>/GmCommonValidation.js"></script>
<script type="text/javascript" src="<%=strOperationsJsPath%>/GmFieldSalesLoaners.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<bean:define id="gridData" name="frmFieldSalesLoaners" property="strXmlData" type="java.lang.String"></bean:define>
<script>
var gridObjData ='<%=gridData%>';
var gridObj;
function fnOnPageLoad() {	
	if(gridObjData!="")
	{
		gridObj = initGridWithDistributedParsing('dataGridDiv',gridObjData);
	   	gridObj.enableTooltips("false,true,true,true,true,true,true,true,true,true,true,true");
		gridObj.attachHeader('#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter');
		gridObj.enablePaging(true,21,5,"pagingArea",true,"infoArea");
		gridObj.setPagingSkin("bricks");		
		gridObj.enableHeaderMenu();
		gridObj.setColumnHidden(3,true);
		gridObj.setColumnHidden(5,true);		
		gridObj.sortRows(11,"date","desc");
		gridObj.enableMultiline(false);	
		gridObj.enableBlockSelection(true); 
		gridObj.attachEvent("onKeyPress", keyPressed);		
	}
}

function fnViewFile(val,type)
{
	var filename = '';
	if (val == 1)
	{
		filename = 'FieldSalesLoanerReport.wmv';
	}
	if (type == 'V')
	{
		windowOpener("html/movies/"+filename,"html","resizable=yes,scrollbars=yes,top=150,left=200,width=750,height=600");
	}
	
}


</script>
</head>
<BODY leftmargin="20" topmargin="10" onload="javascript:fnOnPageLoad();">
<html:form action="/gmFieldSalesLoaners.do" method="post" styleId="form">
<html:hidden property="strOpt" />
<input type="hidden" name="hAction" />
<table cellspacing="0" cellpadding="0" class="DtTable700">
	<tr>
		<td height="25" width="95%" class="RightDashBoardHeader"><fmtFieldSalesLoaners:message key="LBL_FIELD_SALES_LOANER_REPORT"/></td>
		<td align="right" class="RightDashBoardHeader">
		<fmtFieldSalesLoaners:message key="IMG_ALT_TUTORIAL" var="vartutorial"/>
						<a href="javascript:fnViewFile('1','V');"><img src="<%=strImagePath%>/wmv_icon.gif" border="0" height="16" width="16" alt="${vartutorial}"></a>&nbsp;&nbsp;
		</td>
	</tr>
	<tr class="shade"></tr>	
	<tr>
		<td colspan="2">
		<jsp:include page="/sales/GmSalesFilters.jsp" >
						<jsp:param name="HIDE" value="SYSTEM" />
						<jsp:param name="FRMNAME" value="frmFieldSalesLoaners" />
						<jsp:param name="URL" value="/gmFieldSalesLoaners.do?strOpt=load" />
						<jsp:param name="HIDEBUTTON" value="HIDEGO" />
		</jsp:include>
	  </td>
	</tr>
	<tr>
		<td colspan="2">
			<table cellspacing="0" >
				<tr>
					 
					<td class="RightTableCaption" bgcolor="#eeeeee" height="18" ><fmtFieldSalesLoaners:message key="LBL_SETS"/></td>
					<td class="RightTableCaption" bgcolor="#eeeeee" height="18" ><fmtFieldSalesLoaners:message key="LBL_STATUS"/></td>
					<td class="RightTableCaption" bgcolor="#eeeeee" height="18" >&nbsp;</td>
					<td class="RightTableCaption" bgcolor="#eeeeee" height="18" rowspan="2" width="100" valign="middle" align="center">
					<fmtFieldSalesLoaners:message key="BTN_LOAD" var="varLoad"/>					
							<gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="Button" onClick="fnLoad();" tabindex="25" buttonType="Load" />&nbsp;
							<fmtFieldSalesLoaners:message key="BTN_RESET" var="varReset"/>
							<BR><BR><gmjsp:button value="&nbsp;${varReset}&nbsp;" gmClass="Button" onClick="fnReload();" tabindex="25" buttonType="Save" />&nbsp;
					</td>
				</tr>
			 	<tr>
				<td width="30%">							
					<div style="display: visible; height: 100px; overflow: auto; border-width: 1px; border-style: solid; margin-left: 8px; margin-right: 8px;">
						<table>
							<logic:iterate id="alSets" name="frmFieldSalesLoaners" property="alSets">
								<tr>
									<td>
										<htmlel:multibox property="selectedSets" value="${alSets.SETID}" />
										<bean:write name="alSets" property="SNAME" />
									</td>
								</tr>
							</logic:iterate>
						</table>
					</div>
				</td>
				<td width="20%">							
					<div style="display: visible; height: 100px; overflow: auto; border-width: 1px; border-style: solid; margin-left: 8px; margin-right: 8px;">
						<table>
							<logic:iterate id="alStatus" name="frmFieldSalesLoaners" property="alStatus">
								<tr>
									<td>
										<htmlel:multibox property="selectedStatus" value="${alStatus.CODENMALT}" />
										<bean:write name="alStatus" property="CODENM" />
									</td>
								</tr>
							</logic:iterate>
						</table>
					</div>
				</td>	
				<td align="right" valign="middle" width="26%">
						<table cellspacing="0" width="100%">
							<tr>
								<td align="right" class="RightTableCaption" nowrap="nowrap"><fmtFieldSalesLoaners:message key="LBL_SET_NAME"/>:</td>						
								<td>&nbsp;<html:text property="setName" name="frmFieldSalesLoaners" size="19"/></td>
							</tr>
							<tr>
								<td align="right" class="RightTableCaption"><fmtFieldSalesLoaners:message key="LBL_REQUEST_NO"/>:</td>
								<td>&nbsp;<html:text property="reqId" name="frmFieldSalesLoaners" size="19" /></td>
							</tr>
							<tr>
								<td align="right" class="RightTableCaption"><fmtFieldSalesLoaners:message key="LBL_ETCH_ID"/>:</td>
								<td>&nbsp;<html:text property="etchId" name="frmFieldSalesLoaners" size="19"/></td>
							</tr>
							<tr>
								<td align="right" class="RightTableCaption"><fmtFieldSalesLoaners:message key="LBL_CONSIGN_ID"/>:</td>
								<td>&nbsp;<html:text property="cnId" name="frmFieldSalesLoaners" size="19"/></td>
							</tr>
							<tr>
								<td align="right" class="RightTableCaption"><fmtFieldSalesLoaners:message key="LBL_RETURN_IN"/>:</td>
								<td class="RightTableCaption">&nbsp;<html:text property="returnIn" name="frmFieldSalesLoaners" size="3"/>
								<fmtFieldSalesLoaners:message key="LBL_OVERDUE"/>:<html:checkbox property="daysOverDue" name="frmFieldSalesLoaners"/></td>
							</tr>					
						</table>									
					</td>
				</tr>
			</table>	 
		</td>
	</tr>
	<tr class="shade">
		<td align="center" colspan="2">
			<fmtFieldSalesLoaners:message key="LBL_REPORT_AS_OF"/>: <%=strDate%> &nbsp;&nbsp;&nbsp;<fmtFieldSalesLoaners:message key="LBL_BY"/>:<%=strUserName%><BR>
		</td>
	</tr>
	
	<tr>
	<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
		<td colspan="2">
			<jsp:include page="/operations/loaners/GmFieldSalesGridView.jsp"/>
		</td>
	</tr>
  </table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
<%
 strPage ="GmFieldSalesLoaners.jsp";
 strPageNm = "Field Sales report";
%>
</body>
</html>