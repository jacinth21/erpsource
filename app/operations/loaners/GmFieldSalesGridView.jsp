<%
/**********************************************************************************
 * File		 		: GmFieldSalesGridView.jsp
 * Desc		 		: Include for Field sales loaner report
 * Version	 		: 1.0
 * author			: Brinal
************************************************************************************/
%>

<%@ include file="/common/GmHeader.inc" %>
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}

%>
<!-- operations\loaners\GmFieldSalesGridView.jsp -->
<%@ taglib prefix="fmtFieldSalesGridView" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtFieldSalesGridView:setLocale value="<%=strLocale%>"/>
<fmtFieldSalesGridView:setBundle basename="properties.labels.operations.loaners.GmFieldSalesGridView"/>
<HTML>
<HEAD>

<TITLE> Globus Medical: Field sales loaner report Include </TITLE>

</HEAD>
<table cellspacing="0" cellpadding="0" class="DtTable900">
	
	<tr>
		<td>
		<div id="dataGridDiv" width="900px" height="500px"></div>
		<div id="pagingArea" width="900px" height="500px"></div>
		</td>
	</tr>
	<tr>
         <td align="center">
          <div class='exportlinks'><fmtFieldSalesGridView:message key="DIV_EXPORT_OPT"/> <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
               onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> <fmtFieldSalesGridView:message key="DIV_EXCEL"/> </a></div>
           </td>
	</tr>
	<tr>
		<td>
		
		</td>
	</tr>
</table>

</HTML>











