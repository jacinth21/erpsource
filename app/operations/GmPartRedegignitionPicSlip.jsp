<%
/**********************************************************************************
 * File		 		: GmPartRedegignitionPicSlip.jsp
 * Desc		 		: PTRD Pic Slip in Jasper Report
 * Version	 		: 1.0
 * author			: HReddi
************************************************************************************/
%>
<%@ page import ="com.globus.common.beans.GmJasperReport"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtPartRedegignitionPicSlip" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartRedegignitionPicSlip:setLocale value="<%=strLocale%>"/>
<fmtPartRedegignitionPicSlip:setBundle basename="properties.labels.operations.GmPartRedegignitionPicSlip"/>
<!--operations\GmPartRedegignitionPicSlip.jsp  -->
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>
 <%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.globus.common.beans.GmCalenderOperations"%>

<%
	String strHtmlJasperRpt = "";	
	String strApplnDateFmt = GmCommonClass.parseNull((String)strGCompDateFmt); ;
	String strTodaysDate = GmCommonClass.parseNull((String)GmCalenderOperations.getCurrentDate(strApplnDateFmt));
	SimpleDateFormat dfDateFmt = new SimpleDateFormat(strApplnDateFmt+" HH:mm:ss");	
	HashMap hmHeaderValues = new HashMap();
	ArrayList alCartDetails = new ArrayList();
	hmHeaderValues = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("HEADERDATA"));
	alCartDetails = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("CARTDETAILS"));
	String strTransID = "";
	String strTransType = "";
	String strSource = "50183";
	strTransID = GmCommonClass.parseNull((String)hmHeaderValues.get("ID"));
	strTransType = GmCommonClass.parseNull((String)hmHeaderValues.get("TRANSTYPE"));
	
	String strDate = "";
	Date dtFromDate= null;
	Date dtPrintDate= new Date();
	dtFromDate = GmCommonClass.getStringToDate(strDate,strApplnDateFmt);	
	String strPrintDate = dfDateFmt.format(dtPrintDate);
	String strRuleId = strTransID + "$" + strTransType;
	String strId = strTransID+"$"+strSource;
	hmHeaderValues.put("2DBARCODE",strRuleId);
	hmHeaderValues.put("FEDEXCODE",strId);
	hmHeaderValues.put("PRINTDATE",strPrintDate);
%>
<script>
function fnPrint(){
	window.print();
	return false;
}
function hidePrint(){
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";	
}
function showPrint(){
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
</script>
<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<table>
		<tr>		
			<td align="center" id="button">
				<fmtPartRedegignitionPicSlip:message key="BTN_PRINT" var="varPrint"/><gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" tabindex="2" onClick="return fnPrint();" buttonType="Load" />&nbsp;&nbsp;
				<fmtPartRedegignitionPicSlip:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" tabindex="3"	onClick="window.close();" buttonType="Load" />				
			</td>
		</tr>		
	 	<tr>	
			 <td>
				<%
				String strJasperName 	= "/GmPartReDesigPicSlipPrint.jasper";
				GmJasperReport gmJasperReport = new GmJasperReport();			
				gmJasperReport.setRequest(request);
				gmJasperReport.setResponse(response);
				gmJasperReport.setJasperReportName(strJasperName);
				gmJasperReport.setHmReportParameters(hmHeaderValues);
				gmJasperReport.setReportDataList(alCartDetails);
				gmJasperReport.setBlDisplayImage(true);
				strHtmlJasperRpt = gmJasperReport.getHtmlReport();				
				%>	
				<%=strHtmlJasperRpt %>			
			</td>
		</tr>   
</table>
<%@ include file="/common/GmFooter.inc"%>
</BODY>