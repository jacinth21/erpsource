 <%
/**********************************************************************************
 * File		 		: GmPartNumInvExcel.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page import="java.util.Locale"%>


<%@ page language="java" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.text.*,java.lang.String,java.util.StringTokenizer" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- operations\GmPartNumInvExcel.jsp -->
<%
Locale locale = null;
String strLocale = "";

String strJSLocale = "";

String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));

if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>


<%@ taglib prefix="fmtPartNumInvExcel" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartNumInvExcel:setLocale value="<%=strLocale%>"/>
<fmtPartNumInvExcel:setBundle basename="properties.labels.operations.GmPartNumInvExcel"/>

<%
String format = request.getParameter("hAction");
if ((format != null) && (format.equals("Excel"))) {
response.setContentType("application/vnd.ms-excel");
response.setHeader("Content-disposition","attachment;filename=InventoryReport.xls");
}
%>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	String strCssPath = GmCommonClass.getString("GMSTYLES");
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	String strCommonPath = GmCommonClass.getString("GMCOMMON");
	DecimalFormat doublef = new DecimalFormat("#.##");

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");

	HashMap hmLoop = new HashMap();
	HashMap hcboVal = new HashMap();

	String strProjId = (String)request.getAttribute("hProjId")==null?"":(String)request.getAttribute("hProjId");

	String strPartNum = "";
	String strPartDesc = "";
	String strQtyInStock = "";
	String strQtyInShelf = "";
	String strQtyInShelfAlloc = "";
	String strShelfInAlloc = "";
	String strQtyShipAlloc="";
	String strSkipLoanerSection = GmCommonClass.parseNull(GmCommonClass.getString("LOAD_LOANER_INV_SECTION"));
	
	String strQtyRMAlloc = "";
	String strQtyMFGTBR = "";
	String strQtyMFGRelease = "";
	String strQtyDHRWIP = "";
	String strQtyDHR_PEN_QC_INS_ = "";
	String strQtyDHR_PEN_QC_REL = "";
	String strQtyDHR_QC_APP = "";
	String strQtyBUILTSET = "";	
	
	
	String strQtyInBulk = "";
	String strQtyInWIPSets = "";	
	String strQtyInBulkAlloc = "";
	String strTBE = "";
	String strRawMatQty = "";
	
	String strRepackAvail = "";
	String strRepackAlloc = "";
	
	String strQtyInQuarantine = "";
	String strQtyInQuarantineAlloc = "";
	
	String strQtyInReturnsHold = "";
	String strQtyInReturnsHoldAlloc = "";
	String strQtyInPO = "";
	String strQtyInWIP = "";
	String strQtyInFA = "";
	String strQtyInFAWIP = "";
	String strDHRAlloc="";
	
	String strIhAVailQty = "";
	String strIhAllocQty = "";
	String strIhSetQty = "";
	String strIhItemQty = "";
	
	String strQtyInRW = "";
	String strQtyInRWAlloc = "";
	
	String strLoanQty = ""; 
	
	String strProjName = "";

	String strShade = "";
	String strIncludedInvID = "";
	int intLoop = 0;
	
	String strShowFG = "";
	String strShowRM = "";
	String strShowBulk = "";
	String strShowBS = "";
	String strShowPN = "";
	String strShowQN = "";
	String strShowRtn = "";
	String strShowPO = "";
	String strShowMFG = "";
	String strShowDHR = "";
	String strShowShip = "";
	String strShowIH = "";
	String strShowLN = "";
	String strShowRW = "";
	String strShowIT = "";
	String strQtyInIntransit = "";
	String strQtyInIntransitAlloc = "";
	
	ArrayList alReport = new ArrayList();

	if (hmReturn != null) 
	{
		alReport = (ArrayList)hmReturn.get("REPORT");
	}
	String strIncludedInv = GmCommonClass.parseNull((String)request.getAttribute("INCLUDEINV"));
	String strExcludedInv = GmCommonClass.parseNull((String)request.getAttribute("EXCLUDEINV"));	
	if (alReport != null)
	{
		intLoop = alReport.size();
	}
	
	StringTokenizer strIncludeInvs = new StringTokenizer(strIncludedInv,",");
	while(strIncludeInvs.hasMoreTokens())
	{
		strIncludedInvID = strIncludeInvs.nextToken();
		if(strIncludedInvID.equals("60001"))
		{
			strShowFG = "Y";	
		}
		if(strIncludedInvID.equals("60002"))
		{
			strShowRM = "Y";	
		}
		if(strIncludedInvID.equals("60003"))
		{
			strShowBulk = "Y";	
		}
		if(strIncludedInvID.equals("60004"))
		{
			strShowBS = "Y";
		}
		if(strIncludedInvID.equals("60005"))
		{
			strShowPN = "Y";	
		}
		if(strIncludedInvID.equals("60006"))
		{
			strShowQN = "Y";
		}
		if(strIncludedInvID.equals("60007"))
		{
			strShowRtn = "Y";	
		}
		if(strIncludedInvID.equals("60008"))
		{
			strShowPO = "Y";
		}
		if(strIncludedInvID.equals("60009"))
		{
			strShowMFG = "Y";
		}
		if(strIncludedInvID.equals("60010"))
		{
			strShowDHR = "Y";
		}
		if(strIncludedInvID.equals("60011"))
		{
			strShowShip = "Y";
		}
		if(strIncludedInvID.equals("60012"))
		{
			strShowIH = "Y";
		}
		if(strIncludedInvID.equals("60013"))
		{
			strShowLN = "Y";
		}
		if(strIncludedInvID.equals("60014"))
		{
			strShowRW = "Y";
		}
		if(strIncludedInvID.equals("60015"))
		{
			strShowIT = "Y";
		}
	}
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Inventory Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<style>
.RightTableCaption {
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
}

.Line{
	background-color: #676767;
}
TR.RightTableCaption{
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
	background-color: #eeeeee;
}
.RightText {
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	FONT-FAMILY: verdana, arial, sans-serif;
}
</style>

</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmAccount" method="POST">
	<table border="0" width="700" cellspacing="0" cellpadding="0">
		
		<tr>
			<td colspan="2" align="center" height="25"><font size="+2"><fmtPartNumInvExcel:message key="LBL_REPORT"/></font></td>
		</tr>
		<tr>
			<td colspan="2" valign="top">
				<table border="1" width="100%" cellspacing="0" cellpadding="0" valign="top">
					<tr class="RightTableCaption">
						<td HEIGHT="24" align="center" width="60"><fmtPartNumInvExcel:message key="LBL_PART_NUM"/></td>
						<td width="400">&nbsp;<fmtPartNumInvExcel:message key="LBL_DESCRIPTION"/></td>
						
						<% if(!strShowFG.equals("")){%>
						<td colspan="2" align="center"><fmtPartNumInvExcel:message key="LBL_FG_INVENTORY"/><BR></td>
						<%} %>
						<% if(!strShowRW.equals("")){%>
						<td colspan="2" align="center"><fmtPartNumInvExcel:message key="LBL_RW_INVENTORY"/><BR></td>
						<%} %>	
						<% if(!strShowRM.equals("")){%>
						<td colspan="2" align="center"><fmtPartNumInvExcel:message key="LBL_RM_INVENTORY"/><BR></td>
						<%} %>
						<% if(!strShowBulk.equals("")){%>
						<td colspan="4" align="center"><fmtPartNumInvExcel:message key="LBL_BULK_LOOSE"/><BR></td>
						<%} %>
						<% if(!strShowBS.equals("")){%>
						<td width="80" align="center"><fmtPartNumInvExcel:message key="LBL_BUILT_SET"/><br></td>
						<% } %>	
						<% if(!strShowPN.equals("")){%>
						<td colspan="2" align="center"><fmtPartNumInvExcel:message key="LBL_RE_PACK"/></td>	
						<%} %>
							<% if(!strShowQN.equals("")){%>					
						<td colspan="2" align="center"><fmtPartNumInvExcel:message key="LBL_QUAN_INV"/></td>
						<%} %>
							<% if(!strShowRtn.equals("")){%>
						<td colspan="2" width="100" align="center"><fmtPartNumInvExcel:message key="LBL_RETURNS_HOLD"/><BR></td>
						<%} %>
							<% if(!strShowPO.equals("")){%>
						<td width="80" align="center"><fmtPartNumInvExcel:message key="LBL_QTY_PO"/><br></td>
						<%} %>
						<% if(!strShowMFG.equals("")){%>
						<th colspan="2" align="center"  ><fmtPartNumInvExcel:message key="LBL_MFG_RUNS"/></th>
						<% } %>	
						<% if(!strShowDHR.equals("")){%>
						<th colspan="5" align="center" ><fmtPartNumInvExcel:message key="LBL_DHR"/></th>						
						<% } %>	
						<% if(!strShowShip.equals("")){%>
						<th width="60" align="center" ><fmtPartNumInvExcel:message key="LBL_SHIPPING_HOLD"/></th>
						<% } %>	
						<% if(!strShowIH.equals("")){%>
						<th colspan="4" align="center"><fmtPartNumInvExcel:message key="LBL_IN_HUS"/></th>
						<% }%>	
						<% if(!strShowLN.equals("")){%>
						<th width="50" align="center"><fmtPartNumInvExcel:message key="LBL_LOANER_INV"/><br></th>
						<%} %>	
						<% if(!strShowIT.equals("")){%>
						<th colspan="2" align="center" ><fmtPartNumInvExcel:message key="LBL_IN_TRANSIT"/></th>						
						<% } %>						
						
					</tr>
					<tr class="RightTableCaption">
						<td colspan="2">&nbsp;</td>
					
					  	<% if(!strShowFG.equals("")){%>
						<td width="60" align="center"><fmtPartNumInvExcel:message key="LBL_AVAIL"/>.</td>
						<td width="60" align="center"><fmtPartNumInvExcel:message key="LBL_ALLOC"/></td>						
						<%} %>
						<% if(!strShowRW.equals("")){%>
						<td width="60" align="center"><fmtPartNumInvExcel:message key="LBL_AVAIL"/></td>	
						<td width="60" align="center" ><fmtPartNumInvExcel:message key="LBL_ALLOC"/>.</td>
						<%} %>
						<% if(!strShowRM.equals("")){%>
						<td width="60" align="center"><fmtPartNumInvExcel:message key="LBL_AVAIL"/></td>						
						<td width="60" align="center" ><fmtPartNumInvExcel:message key="LBL_ALLOC"/>.</td>
						<%} %>
						<% if(!strShowBulk.equals("")){%>
						<td width="60" align="center"><fmtPartNumInvExcel:message key="LBL_WIP_SETS"/></td>
						<td width="60" align="center"><fmtPartNumInvExcel:message key="LBL_TBE_SETS"/></td>
						<td width="60" align="center"><fmtPartNumInvExcel:message key="LBL_LOOSE"/></td>
						<td width="60" align="center"><fmtPartNumInvExcel:message key="LBL_ALLOC"/>.</td>
						<%} %>
						<% if(!strShowBS.equals("")){%>
						<td width="60" align="center">&nbsp;</td>
						<% } %>		
						<% if(!strShowPN.equals("")){%>
						<td width="60" align="center"><fmtPartNumInvExcel:message key="LBL_AVAIL"/>.</td>
						<td width="60" align="center"><fmtPartNumInvExcel:message key="LBL_ALLOC"/></td>
						<%} %>
						<% if(!strShowQN.equals("")){%>
						<td width="60" align="center"><fmtPartNumInvExcel:message key="LBL_AVAIL"/>.</td>
						<td width="60" align="center"><fmtPartNumInvExcel:message key="LBL_ALLOC"/></td>
						<%} %>
						<% if(!strShowRtn.equals("")){%>
						<td width="60" align="center"><fmtPartNumInvExcel:message key="LBL_AVAIL"/>.</td>
						<td width="60" align="center"><fmtPartNumInvExcel:message key="LBL_ALLOC"/></td>
						<%}%>
						<% if(!strShowPO.equals("")){%>
						<td width="80">&nbsp;</td>
						<%} %>					
						<% if(!strShowMFG.equals("")){%>
						<th width="50" align="center" class="ShadeDarkOrangeTD"><fmtPartNumInvExcel:message key="LBL_TBR"/></th>
						<th width="50" align="center" class="ShadeDarkOrangeTD"><fmtPartNumInvExcel:message key="LBL_RELEASED"/></th>
						<% } %>	
						<% if(!strShowDHR.equals("")){%>
						<th width="50" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvExcel:message key="LBL_WIP"/></th>
						<th width="50" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvExcel:message key="LBL_REC"/></th>
						<th width="50" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvExcel:message key="LBL_QC_INS"/></th>						
						<th width="50" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvExcel:message key="LBL_QC_REL"/><br></th>
						<th width="50" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvExcel:message key="LBL_ALLOC"/></th>						
						<% } %>	
						<% if(!strShowShip.equals("")){%>
						<th width="60" align="center" ><fmtPartNumInvExcel:message key="LBL_ALLOC"/></th>
						<% } %>	
						<% if(!strShowIH.equals("")){%>
						<th width="50" align="center" ><fmtPartNumInvExcel:message key="LBL_AVAIL"/></th>
						<th width="50" align="center" ><fmtPartNumInvExcel:message key="LBL_ALLOC"/></th>
						<th width="50" align="center" ><fmtPartNumInvExcel:message key="LBL_SETS"/></th>
						<th width="50" align="center" ><fmtPartNumInvExcel:message key="LBL_ITEMS"/></th>
						<%} %>
						<% if(!strShowLN.equals("")){%>
						<th width="50" align="center"><fmtPartNumInvExcel:message key="LBL_AVAIL"/></th>
						<%}%>
						<% if(!strShowIT.equals("")){%>
						<td width="60" align="center"><fmtPartNumInvExcel:message key="LBL_AVAIL"/>.</td>
						<td width="60" align="center"><fmtPartNumInvExcel:message key="LBL_ALLOC"/></td>						
						<%} %>						
							
					</tr>
<%
			if (intLoop > 0)
			{
				int intStock = 0;
				int intBulk = 0;
				int intReturns = 0;
				int intPO = 0;
				int intDHR = 0;
				
				for (int i = 0;i < intLoop ;i++ )
				{
					hmLoop = (HashMap)alReport.get(i);
					strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
					strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));

					strQtyInShelf = GmCommonClass.parseZero((String)hmLoop.get("INSHELF"));
					strQtyInShelfAlloc = GmCommonClass.parseZero((String)hmLoop.get("SHELFALLOC"));
					strShelfInAlloc = GmCommonClass.parseZero((String)hmLoop.get("INV_ALLOC_TO_PACK"));
					
					strQtyInRW = GmCommonClass.parseZero((String)hmLoop.get("INRW"));
					strQtyInRWAlloc = GmCommonClass.parseZero((String)hmLoop.get("RWALLOC"));
										
					strRawMatQty = GmCommonClass.parseZero((String)hmLoop.get("RMQTY"));
					double dRawMatQty = Double.parseDouble(strRawMatQty);
					strRawMatQty = doublef.format(dRawMatQty);
					 
					strRepackAvail = GmCommonClass.parseZero((String)hmLoop.get("REPACKAVAIL"));
					strRepackAlloc = GmCommonClass.parseZero((String)hmLoop.get("REPACKALLOC"));
					
					strQtyInQuarantine = GmCommonClass.parseZero((String)hmLoop.get("INQUARAN"));
					strQtyInQuarantineAlloc = GmCommonClass.parseZero((String)hmLoop.get("QUARALLOC"));
					
					strQtyInBulk = GmCommonClass.parseZero((String)hmLoop.get("INBULK"));
					strQtyInWIPSets = GmCommonClass.parseZero((String)hmLoop.get("IN_WIP_SET"));
					strQtyInBulkAlloc = GmCommonClass.parseZero((String)hmLoop.get("BULKALLOC"));
					strTBE = GmCommonClass.parseZero((String)hmLoop.get("C205_TBE_ALLOC"));
					
					strQtyInReturnsHold = GmCommonClass.parseZero((String)hmLoop.get("RETURNAVAIL"));
					strQtyInReturnsHoldAlloc = GmCommonClass.parseZero((String)hmLoop.get("RETURNALLOC"));
					
					strQtyInPO = GmCommonClass.parseZero((String)hmLoop.get("POPEND"));

					strQtyInWIP = GmCommonClass.parseZero((String)hmLoop.get("POWIP"));
					
					strQtyBUILTSET = GmCommonClass.parseZero((String)hmLoop.get("BUILD_SET_QTY"));
					strQtyRMAlloc = GmCommonClass.parseZero((String)hmLoop.get("RM_ALLOC_QTY"));
					strQtyMFGTBR = GmCommonClass.parseZero((String)hmLoop.get("MFG_TBR"));	
					strQtyMFGRelease = GmCommonClass.parseZero((String)hmLoop.get("MFG_RELEASE"));
					
					double dQtyMFGTBR = Double.parseDouble(strQtyMFGTBR);
					double dQtyMFGRelease = Double.parseDouble(strQtyMFGRelease);
					strQtyMFGTBR = doublef.format(dQtyMFGTBR);
					strQtyMFGRelease = doublef.format(dQtyMFGRelease);
					
					strQtyBUILTSET = GmCommonClass.parseZero((String)hmLoop.get("BUILD_SET_QTY"));
					strQtyDHRWIP = GmCommonClass.parseZero((String)hmLoop.get("DHR_WIP"));	
					strQtyDHR_PEN_QC_INS_ = GmCommonClass.parseZero((String)hmLoop.get("DHR_PEN_QC_INS"));	
					strQtyDHR_PEN_QC_REL = GmCommonClass.parseZero((String)hmLoop.get("DHR_PEN_QC_REL"));	
					strQtyDHR_QC_APP = GmCommonClass.parseZero((String)hmLoop.get("DHR_QC_APPR"));
					strDHRAlloc = GmCommonClass.parseZero((String)hmLoop.get("DHRALLOC"));
					strQtyShipAlloc = GmCommonClass.parseZero((String)hmLoop.get("SHIPALLOC"));
					
					strIhAVailQty = GmCommonClass.parseZero((String)hmLoop.get("IH_AVAIL_QTY"));
					strIhAllocQty = GmCommonClass.parseZero((String)hmLoop.get("IH_ALLOC_QTY"));
					strIhSetQty = GmCommonClass.parseZero((String)hmLoop.get("IH_SET_QTY"));
					strIhItemQty = GmCommonClass.parseZero((String)hmLoop.get("IH_ITEM_QTY"));

					strLoanQty = GmCommonClass.parseZero((String)hmLoop.get("LOANQTY"));
					
					strQtyInIntransit = GmCommonClass.parseZero((String)hmLoop.get("INTRANS_AVAIL_QTY"));
					strQtyInIntransitAlloc = GmCommonClass.parseZero((String)hmLoop.get("INTRANS_ALLOC_QTY"));

%>
						<tr>
							<td width="60" height="20" class="RightText">&nbsp;<%=strPartNum%></td>
							<td width="400" nowrap class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
							<% if(!strShowFG.equals("")){%>
							<td width="60" class="RightText" align="right"><%=strQtyInShelf%></td>
							<td width="60" class="RightText" align="right"><%=strQtyInShelfAlloc%></td>							
							<%} %>
							<% if(!strShowRW.equals("")){%>
							<td width="50" class="RightText" align="right"><%=strQtyInRW%></td>							
							<td width="50" class="RightText" align="right"><%=strQtyInRWAlloc%></td>
							<%} %>
							<% if(!strShowRM.equals("")){%>
							<td width="50" class="RightText" align="right"><%=strRawMatQty%></td>							
							<td width="50" class="RightText" align="right"><%=strQtyRMAlloc%></td>
							<%} %>
							<% if(!strShowBulk.equals("")){%>
							<td width="60" class="RightText" align="right"><%=strQtyInWIPSets%></td>
							<td width="50" class="RightText" align="right"><%=strTBE%></td>
							<td width="60" class="RightText" align="right"><%=strQtyInBulk%></td>
							<td width="60" class="RightText" align="right"><%=strQtyInBulkAlloc%></td>
							<%} %>
							<% if(!strShowBS.equals("")){%>		
							<td width="60" class="RightText" align="right"><%=strQtyBUILTSET%></td>						 
							<% } %>	
							<% if(!strShowPN.equals("")){%>
							<td width="60" class="RightText" align="right"><%=strRepackAvail%></td>
							<td width="60" class="RightText" align="right"><%=strRepackAlloc%></td>
							<%} %>
							<% if(!strShowQN.equals("")){%>
							<td width="60" class="RightText" align="right"><%=strQtyInQuarantine%></td>
							<td width="60" class="RightText" align="right"><%=strQtyInQuarantineAlloc%></td>
							<%} %>
							<% if(!strShowRtn.equals("")){%>
							<td width="60" class="RightText" align="right"><%=strQtyInReturnsHold%></td>
							<td width="60" class="RightText" align="right"><%=strQtyInReturnsHoldAlloc%></td>
							<%} %>
							<% if(!strShowPO.equals("")){%>
							<td width="60" class="RightText" align="right"><%=strQtyInPO%></td>
							<%} %> 
							<% if(!strShowMFG.equals("")){%>		
							<td width="60" class="RightText" align="right"><%=strQtyMFGTBR%></td>
							<td width="60" class="RightText" align="right"><%=strQtyMFGRelease%></td>
							<% } %>									
							<% if(!strShowDHR.equals("")){%>	
							<td width="60" class="RightText" align="right"><%=strQtyDHRWIP%></td>
							<td width="60" class="RightText" align="right"><%=strQtyDHR_PEN_QC_INS_%></td>	
							<td width="60" class="RightText" align="right"><%=strQtyDHR_PEN_QC_REL%></td>
							<td width="60" class="RightText" align="right"><%=strQtyDHR_QC_APP%></td>
							<td width="60" class="RightText" align="right"><%=strDHRAlloc%></td>								
							<% } %>	
							<% if(!strShowShip.equals("")){%>
							<td width="60" class="RightText" align="right"><%=strQtyShipAlloc%></td>
							<% } %>														
							<% if(!strShowIH.equals("")){%>
							<td width="60" class="RightText" align="right"><%=strIhAVailQty%></td>	
							<td width="60" class="RightText" align="right"><%=strIhAllocQty%></td>
							<td width="60" class="RightText" align="right"><%=strIhSetQty%></td>
							<td width="60" class="RightText" align="right"><%=strIhItemQty%></td>
							<%} %>
							<% if(!strShowLN.equals("")){%>
							<td align="right"><%=strLoanQty%></td>
							<%} %>							
							<% if(!strShowIT.equals("")){%>
							<td width="60" class="RightText" align="right"><%=strQtyInIntransit%></td>
							<td width="60" class="RightText" align="right"><%=strQtyInIntransitAlloc%></td>							
							<%} %>
						</tr>
<%
				}
			} else	{
%>
					<tr>
						<td height="30" colspan="15" align="center" class="RightTextBlue"><fmtPartNumInvExcel:message key="LBL_NO_PART"/> !</td>
					</tr>
<%
		}
%>
				</table>
			</td>
		</tr>
	</table>
		
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
