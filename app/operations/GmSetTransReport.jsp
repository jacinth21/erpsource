 <%
/**********************************************************************************
 * File		 		: GmSetTransReport.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>

<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<!--operations\GmSetTransReport.jsp  -->
<%@ taglib prefix="fmtSetTransReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtSetTransReport:setLocale value="<%=strLocale%>"/>
<fmtSetTransReport:setBundle basename="properties.labels.operations.GmSetTransReport"/>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="java.util.Date"%>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");

	HashMap hmLoop = new HashMap();
	HashMap hcboVal = new HashMap();

	String strFromDt = (String)request.getAttribute("hFrmDt")==null?"":(String)request.getAttribute("hFrmDt");
	String strToDt = (String)request.getAttribute("hToDt")==null?"":(String)request.getAttribute("hToDt");
	String strApplDateFmt = strGCompDateFmt;//GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	
	Date dtFrmDate = null;
	Date dtToDate = null;
	
	dtFrmDate = (Date)GmCommonClass.getStringToDate(strFromDt,strApplDateFmt);
	dtToDate = (Date)GmCommonClass.getStringToDate(strToDt,strApplDateFmt);

	String strSetId = "";
	String strSetNm = "";
	String strConsignCount = "";
	String strReturnCount = "";

	String strShade = "";
	String strCodeID = "";
	String strSelected = "";
	int intLoop = 0;

	ArrayList alReport = new ArrayList();

	if (hmReturn != null) 
	{
		alReport = (ArrayList)hmReturn.get("SETREPORT");
	}

	if (alReport != null)
	{
		intLoop = alReport.size();
	}

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Consignment Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script>
var dateFmt = '<%=strApplDateFmt%>';
function fnSubmit(){
	objStartDt = document.frmAccount.Txt_FrmDt;
	objEndDt = document.frmAccount.Txt_ToDt;
	if(objStartDt.value != ""){		 
		CommonDateValidation(document.frmAccount.Txt_FrmDt,dateFmt, message[611]);	 
	}

	if(objEndDt.value != ""){ 
		CommonDateValidation( document.frmAccount.Txt_ToDt,dateFmt, message[611]);	 
	}
	
	var fromToDiff = dateDiff(objStartDt.value, objEndDt.value, dateFmt);
	
	if(fromToDiff < 0){
   	 Error_Details(message_operations[530]);
	 }

	if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}	
	
	document.frmAccount.hAction.value = "Reload";
	fnStartProgress('Y');
	document.frmAccount.submit();
}

function fnPrintVer(val)
{
windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId="+val,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmSetTransReportServlet">
<input type="hidden" name="hId" value="">
<input type="hidden" name="hAction" value="">

	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr>
			<td rowspan="20" width="1" class="Line"></td>
			<td colspan="2" height="1" bgcolor="#666666"></td>
			<td rowspan="20" width="1" class="Line"></td>
		</tr>
		<tr>
			<td colspan="2" height="25" class="RightDashBoardHeader">
				<fmtSetTransReport:message key="LBL_REPORT"/>
			</td>
		</tr>
		<tr><td class="Line" height="1" colspan="2"></td></tr>
		<tr>
			<td>&nbsp;</td>
			<td class="RightText" HEIGHT="24" ><fmtSetTransReport:message key="LBL_FROM_DT"/>:&nbsp;<gmjsp:calendar textControlName="Txt_FrmDt" textValue="<%=dtFrmDate==null?null:new java.sql.Date(dtFrmDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
&nbsp;&nbsp;<fmtSetTransReport:message key="LBL_TO_DATE"/>: <gmjsp:calendar textControlName="Txt_ToDt" textValue="<%=dtToDate==null?null:new java.sql.Date(dtToDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;&nbsp;
<fmtSetTransReport:message key="BTN_GO" var="varGo"/><gmjsp:button value="&nbsp;${varGo}&nbsp;" name="Btn_Go" gmClass="button" onClick="javascript:fnSubmit();" buttonType="Load" />
			</td>
		</tr>
		<tr><td class="Line" colspan="2"></td></tr>
		<tr>
			<td colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" align="center" width="60"><fmtSetTransReport:message key="LBL_SET_ID"/></td>
						<td width="400">&nbsp;<fmtSetTransReport:message key="LBL_SET_NAME"/></td>
						<td width="100" align="center"><fmtSetTransReport:message key="LBL_QTY_CONSIG"/><br></td>
						<td width="100" align="center"><fmtSetTransReport:message key="LBL_QTY_RET"/><br></td>
					</tr>
					<tr><td class="Line" colspan="4"></td></tr>
					<tr>
						<td colspan="4">
							<div style="display:visible;width: 697px; height: 460px; overflow: auto;">
								<table border="0" width="100%" cellspacing="0" cellpadding="0">					
<%
			if (intLoop > 0)
			{
				for (int i = 0;i < intLoop ;i++ )
				{
					hmLoop = (HashMap)alReport.get(i);
					strSetId = GmCommonClass.parseNull((String)hmLoop.get("SETID"));
					strSetNm = GmCommonClass.parseNull((String)hmLoop.get("SNAME"));
					strConsignCount = (String)hmLoop.get("CONCNT");
					strReturnCount = (String)hmLoop.get("RETCNT");
%>
									<tr>
										<td width="60" height="20" class="RightText">&nbsp;<%=strSetId%></td>
										<td width="400" class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strSetNm)%></td>
										<td width="100" class="RightText" align="center"><%=strConsignCount%></td>
										<td width="100" class="RightText" align="center"><%=strReturnCount%></td>
									</tr>
									<tr><td colspan="5" bgcolor="#eeeeee" height="1"></td></tr>
<%
				}
			} else	{
%>
					<tr>
						<td height="30" colspan="4" align="center" class="RightTextBlue"><fmtSetTransReport:message key="LBL_NO_SETS"/> !</td>
					</tr>
<%
		}
%>
								</table>
							</DIV>
						</td>
					</tr>
					<tr><td colspan="5" height="1" bgcolor="#666666"></td></tr>
				</table>
			</td>
		</tr>
	</table>
		
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>

</BODY>

</HTML>
