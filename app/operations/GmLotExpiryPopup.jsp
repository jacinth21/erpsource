<!-- \operations\GmLotExpiryPopup.jsp -->
<%
	/*******************************************************************************************************************
	  * File		 		: GmLotExpiryPopup.jsp
      * Desc		 		: This screen is used to display Lot Expiry details when Qty link click on Lot Expiry Report
      * author			    : Agilan Singaravel
	 ******************************************************************************************************************/
%>
 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<%@ page language="java"%>
<%@page import="java.util.Date"%> 
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%> 
<%@ page import = "java.util.*,java.io.*,java.net.URLEncoder "%>
<%@ taglib prefix="fmtLotExpiryReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtLotExpiryReport:setLocale value="<%=strLocale%>"/>
<fmtLotExpiryReport:setBundle basename="properties.labels.operations.GmLotExpiryReport"/>
<bean:define id="partNum" name="frmLotExpiryReport" property="partNum" scope="request" type="java.lang.String"></bean:define>
<bean:define id="loadFlg" name="frmLotExpiryReport" property="loadFlg" scope="request" type="java.lang.String"></bean:define>
<bean:define id="projectId" name="frmLotExpiryReport" property="projectId" scope="request" type="java.lang.String"></bean:define>
<bean:define id="strOpt" name="frmLotExpiryReport" property="strOpt" scope="request" type="java.lang.String"></bean:define>
<bean:define id="chk_ActiveFl" name="frmLotExpiryReport" property="chk_ActiveFl" scope="request" type="java.lang.String"></bean:define>

<%String strOperationsJsPath= GmCommonClass.parseNull(GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS"));
String strTitle = "";
if(loadFlg.equals("expired")) {
	strTitle = "(Expired)";
} else if(loadFlg.equals("others")) {
	strTitle = "(Expired in Others)";
}else if(loadFlg.equals("30Days")) {
	strTitle = "(Expired in 30 Days)";
}else if(loadFlg.equals("60Days")) {
	strTitle = "(Expired in 60 Days)";
}else if(loadFlg.equals("90Days")) {
	strTitle = "(Expired in 90 Days)";
}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Lot Expiry Report</TITLE>
<link rel="styleSheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css"> 
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmProgress.css">
<script language="JavaScript" src="<%=strJsPath%>/GmProgress.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmGrid.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmLotExpiryPopupReport.js"></script> 
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script type="text/javascript">
var gCmpDateFmt = "<%=strGCompDateFmt%>";
var strPartNumber = "<%=partNum%>";
var strLoadFl = "<%=loadFlg%>";
var strProjectId = "<%=projectId%>";
var strOpt = "<%=strOpt%>";
var strExcQuar = "<%=chk_ActiveFl%>";
</script>

<!-- <style type="text/css" media="all">
@import url("styles/screen.css"); 
 </style> -->
<style> div.gridbox_dhx_skyblue table.hdr td { text-align: center; vertical-align: middle !important; }
 </style> 
  <style>
.box {
  width: 700px;
  border: 1px solid #000;
  margin: auto;
  height: 230px;
  margin-bottom: 20px;
}
.head{
 /*  background-color:#cee4ff; */
  border-bottom: 1px solid #000;
  height: 20px;
}
.headlabel{
    padding-bottom: 15px;
}
.content{
padding-top: 50px;
}
 .objbox{
height:135px;
} 
</style> 
</HEAD>
<body leftmargin="15" topmargin="10" onLoad="fnLoadPopup(); ">
 <html:form action="/gmLotExpiryReport.do?method=loadExpiryLotDetails" > 
 <html:hidden name="frmLotExpiryReport"  property="strOpt" value="" />  
 <table class="DtTable1000" border="0" cellspacing="0" cellpadding="0">
			<tr>
			  <%if(strOpt.equals("loadSterile")){ %>
				<td colspan="6" height="25" class="RightDashBoardHeader">&nbsp;<fmtLotExpiryReport:message key="LBL_LOT_EXPIRY_REPORT"/> <%=strTitle%> </td>
				<%}else{ %>
				<td colspan="6" height="25" class="RightDashBoardHeader">&nbsp;<fmtLotExpiryReport:message key="LBL_LOT_EXPIRY_REPORT_TISSUE"/>  <%=strTitle%></td>
				<%} %>
			 </tr>	
	<tr>
	<td style="padding-left:10px;padding-top:10px;"><div class="box">
		<div class="RightDashBoardHeader head" ><font size="3">&nbsp;<fmtLotExpiryReport:message key="LBL_INVENTORY"/></font></div>
		<div id="loadInventory" style="width:100%;height:auto;"></div>
		<div id="excelToInventory" class='exportlinks' style="width:97%; height:10px; text-align:center;"><fmtLotExpiryReport:message key="LBL_EXPORT_OPTIONS" />&nbsp;:&nbsp;<img src='img/ico_file_excel.png' onclick="fnExportExcel('Inventory');" />&nbsp;<a href="#" onclick="fnExportExcel('Inventory');">
			<fmtLotExpiryReport:message key="LBL_EXCEL" /> </a>
		</div>
		</div>
	</td>
		
	<td style="padding-left:10px;padding-top:10px;"><div class="box">
		<div class="RightDashBoardHeader head"><font size="3">&nbsp;<fmtLotExpiryReport:message key="LBL_CONSIGNMENT_SET"/></font></div>
		<div id="loadConsignmentSet" style="width:99%%;height:auto;"></div>
		<div id="excelToConsignmentSet" class='exportlinks' style="width:97%; height:10px; text-align:center;"><fmtLotExpiryReport:message key="LBL_EXPORT_OPTIONS" />&nbsp;:&nbsp;<img src='img/ico_file_excel.png' onclick="fnExportExcel('Consignment');" />&nbsp;<a href="#" onclick="fnExportExcel('Consignment');">
			<fmtLotExpiryReport:message key="LBL_EXCEL" /> </a>
		</div>
		</div>
	</td>	
	</tr>
		
	<tr>
	<td style="padding-left:10px;padding-top:10px;" ><div class="box">
		<div class="RightDashBoardHeader head" ><font size="3">&nbsp;<fmtLotExpiryReport:message key="LBL_FIELDSALES"/></font></div>
		<div id="loadFieldSales" style="width:100%;height:auto;"></div>
		<div id="excelToFieldSales" class='exportlinks' style="width:97%; height:10px; text-align:center;"><fmtLotExpiryReport:message key="LBL_EXPORT_OPTIONS" />&nbsp;:&nbsp;<img src='img/ico_file_excel.png' onclick="fnExportExcel('FieldSales');" />&nbsp;<a href="#" onclick="fnExportExcel('FieldSales');">
			<fmtLotExpiryReport:message key="LBL_EXCEL" /> </a>
		</div>
		</div>
	</td>
		
	<td style="padding-left:10px;padding-top:10px;"><div class="box">
		<div class="RightDashBoardHeader head"><font size="3">&nbsp;<fmtLotExpiryReport:message key="LBL_LOANER_SET"/></font></div>
		<div id="loadLoanerSet" style="width:100%;height:auto;"></div>
		<div id="excelToLoanerSet" class='exportlinks' style="width:97%; height:10px; text-align:center;"><fmtLotExpiryReport:message key="LBL_EXPORT_OPTIONS" />&nbsp;:&nbsp;<img src='img/ico_file_excel.png' onclick="fnExportExcel('Loaner');" />&nbsp;<a href="#" onclick="fnExportExcel('Loaner');">
			<fmtLotExpiryReport:message key="LBL_EXCEL" /> </a>
		</div>
		</div>
	</td>	
	</tr>
       </table>
 </html:form> 
  <%@ include file="/common/GmFooter.inc" %> 
</BODY>
</HTML>