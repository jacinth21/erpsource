<%
/**********************************************************************************
 * File		 		: GmDonorInfo.jsp
 * Desc		 		: This Jsp is the main JSP which contains JSPs for different sections like "Donor Details", "Donor Parameters", "Generate Control Number" and "Saved Control Number"
 * Version	 		: 1.0
 * author			: arajan
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtDonorInfo" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- operations\GmDonorInfo.jsp -->

<fmtDonorInfo:setLocale value="<%=strLocale%>"/>
<fmtDonorInfo:setBundle basename="properties.labels.operations.GmDonorInfo.jsp"/>


<bean:define id="strRsBulkIdCnt" name="frmDonorInfo" property="strRsBulkIdCnt" type="java.lang.String" ></bean:define>
<bean:define id="screenType" name="frmDonorInfo" property="screenType" type="java.lang.String" ></bean:define>
<bean:define id="serialNumFl" name="frmDonorInfo" property="serialNumFl" type="java.lang.String" ></bean:define>
<bean:define id="matType" name="frmDonorInfo" property="matType" type="java.lang.String" ></bean:define>
<%
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE> Globus Medical: Donor Details </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmDonorInfo.js"></script>

<script>

</script>

<BODY leftmargin="20" topmargin="10">
	<html:form action="/gmDonorInfo.do?method=fetchDonorInfo">
	<html:hidden property="strOpt" name="frmDonorInfo"/>
	<html:hidden property="hInputString" name="frmDonorInfo"/>
	<html:hidden property="hDhrId" styleId="hDhrId" name="frmDonorInfo"/>
	<html:hidden property="hVendorId" name="frmDonorInfo"/>
	<html:hidden property="ncmrStatus" name="frmDonorInfo"/>
	<html:hidden property="hMode" styleId="hMode" name="frmDonorInfo"/>
	
		<table class="DtTable500" border="0" cellspacing="0" cellpadding="0">
				
			<tr>
			<!-- This JSP is for the section "Donor Details" -->
				<td>
					<jsp:include page="/operations/GmDonorDetail.jsp" >
					<jsp:param name="FORMNAME" value="frmDonorInfo" />
					</jsp:include>
				</td>
		     </tr>	
		     <%if(matType.equals("100845")){ %>
		     <tr>
		     <!-- This JSP is for the section "Donor Parameters" -->
				<td>
					<jsp:include page="/operations/GmDonorParam.jsp" >
					<jsp:param name="FORMNAME" value="frmDonorInfo" />
					</jsp:include>
				</td>
		     </tr>
		     <%} %>
		     <!-- this section is different if the DHR is from Bulk shipment -->
		     <%-- <%if(!strRsBulkIdCnt.equals("0") && screenType.equals("NcmrUpdate")){ %> --%>
		     <%if(screenType.equals("NcmrUpdate")){ %>
		     <tr>
				<td>
				<!-- This JSP is for the section "Saved Control Number" -->
					<jsp:include page="/operations/GmSavedLotCodeStatus.jsp" >
					<jsp:param name="FORMNAME" value="frmDonorInfo" />
					</jsp:include>
				</td>
		     </tr>
		     <%}else{ %>
		     
		     <tr>
				<td>
				<!-- This JSP is for the section "Generate Control Number" and "Saved Control Number" -->
					<jsp:include page="/operations/GmGenerateCtrlNum.jsp" >
					<jsp:param name="FORMNAME" value="frmDonorInfo" />
					</jsp:include>
				</td>
		     </tr>
		     
		     <%} %>
		</table>

	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>