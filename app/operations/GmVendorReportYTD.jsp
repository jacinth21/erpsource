 <%
/**********************************************************************************
 * File		 		: GmVendorReportYTD.jsp
 * Desc		 		: This screen is used to display Vendor Report YTD
 * Version	 		: 1.0
 * author			: RichardK
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<!-- operations\GmVendorReportYTD.jsp -->
<%@ taglib prefix="fmtVendorReportYTD" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtVendorReportYTD:setLocale value="<%=strLocale%>"/>
<fmtVendorReportYTD:setBundle basename="properties.labels.operations.GmVendorReportYTD"/>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>

<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%
	GmServlet gm = new GmServlet();
	HashMap hmReturn = new HashMap();
	
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	
	gm.checkSession(response,session);
	
	String strShowArrow = GmCommonClass.parseZero((String)request.getAttribute("Chk_ShowArrowFl"));
	String strShowPercent = GmCommonClass.parseZero((String)request.getAttribute("Chk_ShowPercentFl"));
	String strHideAmount = GmCommonClass.parseZero((String)request.getAttribute("Chk_HideAmountFl"));
	String strHeader = (String)request.getAttribute("strHeader");
	
	String strhAction = (String)request.getAttribute("hAction");
	
	if (strhAction == null)
	{
		strhAction = (String)session.getAttribute("hAction");
	}
	
	strhAction = (strhAction == null)?"Load":strhAction;
		
	// To Generate the Crosstab report
	gmCrossTab.setHeader("Sales By Vendor By Month");
	gmCrossTab.setExport(true, pageContext, "/GmVendorYTDReportServlet", "VendorYTDReport");
	// To display percent changed information
	if (strShowPercent.equals("1"))
	{
		gmCrossTab.setReportType(gmCrossTab.CROSSTAB_SALE_PER);	
	}	
	if (strShowArrow.equals("1") )
	{
		gmCrossTab.setShowArrow(true);; // To Display % arrow information
	}

	if (strHideAmount.equals("1"))
	{
		gmCrossTab.setHideAmount(true);	
	}	
	
	// Setting Display Parameter
	gmCrossTab.setAmountWidth(80);
	gmCrossTab.setNameWidth(250);
	gmCrossTab.setRowHeight(22);
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: YTD #</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>

function fnSubmit()
{
	document.frmOrder.submit();
}

function fnGo()
{
	fnStartProgress("Y");
	document.frmOrder.submit();
}

function fnCallDetail(val)
{
	document.frmOrder.DistributorID.value = val;
	document.frmOrder.hAction.value = "LoadAccount";
	document.frmOrder.submit();
}

function fnLoad()
{
	document.frmOrder.Cbo_FromMonth.value = '<%=request.getAttribute("Cbo_FromMonth")%>';
	document.frmOrder.Cbo_ToMonth.value = '<%=request.getAttribute("Cbo_ToMonth")%>';
	document.frmOrder.Cbo_FromYear.value = '<%=request.getAttribute("Cbo_FromYear")%>';
	document.frmOrder.Cbo_ToYear.value = '<%=request.getAttribute("Cbo_ToYear")%>';
	
	document.frmOrder.Chk_ShowPercentFl.checked = <%=(strShowPercent == "0")?"false":"true"%>;
	document.frmOrder.Chk_ShowArrowFl.checked = <%=(strShowArrow == "0")?"false":"true"%>;
	document.frmOrder.Chk_HideAmountFl.checked = <%=(strHideAmount == "0")?"false":"true"%>;

}


</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad()">
<FORM name="frmOrder" method="post" action = "<%=strServletPath%>/GmVendorYTDReportServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="DistributorID" value="<%=request.getAttribute("strDistributorID")%>">
<TABLE cellSpacing=0 cellPadding=0  border=0 width="1000">
<TBODY>
	<tr>
		<td rowspan="10" class=Line noWrap width=1></td>
		<td class=Line noWrap height=1></td>
		<td rowspan="10" class=Line noWrap width=1></td>
	</tr>
	<tr class=Line><td height=25 class=RightDashBoardHeader><%=strHeader %> <fmtVendorReportYTD:message key="LBL_IN_ZERO"/></td></tr>
	<tr class=Line><td noWrap height=1></td></tr>
	<!-- Addition Filter Information -->
	<tr>
		<td><jsp:include page="/common/GmCrossTabFilterInclude.jsp" />  </td>
	</tr>
	<tr class=borderDark>
		<td noWrap height=1>  </td>
	</tr>
	<tr>
		<td noWrap height=3>  </td>
	</tr>	
	<tr>
		<td align=center> <%=gmCrossTab.PrintCrossTabReport(hmReturn) %></td>
	</tr>
</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
