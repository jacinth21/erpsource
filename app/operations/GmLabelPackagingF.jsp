 <%
/**********************************************************************************
 * File		 		: GmLabelPackagingF.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: arajan
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.servlets.GmCommonBarCodeServlet"%>

<%@ taglib prefix="fmtLabelPackagingF" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\GmLabelPackagingF.jsp -->

<fmtLabelPackagingF:setLocale value="<%=strLocale%>"/>
<fmtLabelPackagingF:setBundle basename="properties.labels.operations.GmLabelPackagingF"/>

<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	
	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmDHRDetails = new HashMap();
	String strPartNum = "";
	String strQtyRej = "";
	String strCreatedDate = "";
	String strQtyOrdered = "";
	String strINSFooter ="";
	String strNewInsert = "";
	String strFooter ="";
	String strLPFooter = "";
	String strLabel ="";
	String strPackPrim = "";
	String strPackSec = "";
	String strInsert = "";
	String strImg = "s_old_logo.gif";
	String strApplnDateFmt = strGCompDateFmt;
	int intWidth = 170;
	
	String strCompanyLogo = GmCommonClass.parseNull((String)request.getParameter("CompanyLogo"));
	strCompanyLogo = strCompanyLogo.equals("")?"100800":strCompanyLogo;
	if (hmReturn != null)
	{
		hmDHRDetails = (HashMap)hmReturn.get("DHRDETAILS");
		strPartNum = GmCommonClass.parseNull((String)hmDHRDetails.get("PNUM"));
		//strCreatedDate = GmCommonClass.parseNull((String)hmDHRDetails.get("CDATE"));
		strCreatedDate = GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("CDATE"),strApplnDateFmt);
		strQtyOrdered = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYORD"));
		strQtyRej = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYREJ"));
		strINSFooter = GmCommonClass.parseNull ((String)hmDHRDetails.get("INSFOOTER"));
		strLabel = strPartNum.replaceAll("\\.","");
		strPackPrim = GmCommonClass.parseNull((String)hmDHRDetails.get("PACKPRIM"));
		strPackSec = GmCommonClass.parseNull((String)hmDHRDetails.get("PACKSEC"));
		strInsert = GmCommonClass.parseNull((String)hmDHRDetails.get("INSID"));
		strLPFooter = GmCommonClass.parseNull((String)hmDHRDetails.get("LPFOOTER"));
	}
	int intSize = 0;
	HashMap hcboVal = null;
	if(!strINSFooter.equals("")){
		 strNewInsert  ="/ GI001A";
		 String strArr[] = strINSFooter.split("\\^");
		 strFooter = strArr[0];
		 strImg ="s_logo1.png";
		 intWidth =270;
		}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: DHR Print Version </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>
</script>
<style type="text/css">
td.LineBreak{
word-break: break-all;
}
</style>
</HEAD>

<BODY leftmargin="20" topmargin="20"  onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmOrder" method="POST" action="<%=strServletPath%>/GmPOReceiveServlet">
	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td bgcolor="#666666" rowspan="16" width="1"></td>
			<td bgcolor="#666666" colspan="4" height="1"></td>
			<td bgcolor="#666666" rowspan="15" width="1"></td>
		</tr>
		<tr>
			<td height="80" width="<%=intWidth%>">&nbsp;<img src="<%=strImagePath%>/<%=strCompanyLogo%>.gif" width="138" height="60"></td>
			<td class="RightText" width="130">&nbsp;</td>
			<td class="RightText" width="1"bgcolor="#666666"></td>
			<td align="right" class="RightText"><font size=+2> <fmtLabelPackagingF:message key="LBL_LABELING_PACKAGING_FORM"/> &nbsp;<br>&nbsp;</font></td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
		<tr><td bgcolor="#eeeeee" height="8" colspan="4"></td></tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
		<tr>
			<td colspan="4">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightTableCaption" width="18%" align="right"><fmtLabelPackagingF:message key="LBL_PRODUCT_NUMBER"/>:</td>
						<td class="RightText" width="18%" align="left" >&nbsp;<%=strPartNum%></td>
						<td class="RightTableCaption" width="18%" height="30" align="right"><fmtLabelPackagingF:message key="LBL_LABEL_PART_NUMBER"/>:</td>
						<td class="RightText" width="16%" align="left">&nbsp;GM<%=strLabel%></td>
						<td class="RightTableCaption" width="10%" align="left"><fmtLabelPackagingF:message key="LBL_REV"/>:</td>
						<td class="RightText" width="10%">&nbsp;</td>
					</tr>
					<tr><td bgcolor="#666666" colspan="6"></td></tr>
					<tr><td height="350" colspan="6"></td></tr>
					<tr><td bgcolor="#666666" colspan="6"></td></tr>
					<tr>
						<td colspan="6">
							<font face="verdana" size="-4"><fmtLabelPackagingF:message key="LBL_MODEL_LABEL"/></font>
						</td>
					</tr>

					<tr><td bgcolor="#666666" colspan="6"></td></tr>
					<tr><td class="RightText" bgcolor="#eeeeee" height="25" colspan="6">&nbsp;<b><fmtLabelPackagingF:message key="LBL_PACKAGING_BILL_MATERIALS"/></b>:</td></tr>
					<tr><td bgcolor="#666666" colspan="6"></td></tr>
					<tr>
						<td colspan="6">
							<table border="0" width="100%" cellspacing="0" cellpadding="0" >
								<tr class="shade">
									<td height="20" class="RightText">&nbsp;<fmtLabelPackagingF:message key="LBL_PRIMARY_PACKAGING_MATERIALS"/></td>
									<td class="RightText"><fmtLabelPackagingF:message key="LBL_QTY"/></td>
									<td class="RightText" width="1" bgcolor="#666666" rowspan="22"></td>
									<td class="RightText" width="8" bgcolor="#eeeeee" rowspan="22"></td>
									<td class="RightText" width="1" bgcolor="#666666" rowspan="22"></td>
									<td class="RightText">&nbsp;<fmtLabelPackagingF:message key="LBL_SECONDARY_PACKAGING_MATERIALS"/></td>
									<td class="RightText"><fmtLabelPackagingF:message key="LBL_QTY"/></td>
								</tr>
								<tr><td colspan="7" bgcolor="#666666" height="1"></td></tr>
								<tr><td colspan="7" bgcolor="#eeeeee" height="1"></td></tr>
								<tr>
									<td class="RightText" height="20">&nbsp;&nbsp;<b><%=strPackPrim%></b></td>
									<td class="RightText">&nbsp;<b>1</b></td>
									<td class="RightText">&nbsp;&nbsp;<b><%=strPackSec%></b></td>
									<td class="RightText">&nbsp;<b>1</b></td>
								</tr>
								<tr><td colspan="7" bgcolor="#eeeeee" height="1"></td></tr>
								<tr><td colspan="7" height="20">&nbsp;</td></tr>
								<tr><td colspan="7" bgcolor="#eeeeee" height="1"></td></tr>
								<tr><td colspan="7" height="20">&nbsp;</td></tr>
								<tr><td colspan="7" bgcolor="#eeeeee" height="1"></td></tr>
								<tr><td colspan="7" height="20">&nbsp;</td></tr>
								<tr><td colspan="7" bgcolor="#eeeeee" height="1"></td></tr>
								<tr><td colspan="7" bgcolor="#666666" height="1"></td></tr>
								<tr class="shade">
									<td align="center" height="20"  class="RightText">&nbsp;<fmtLabelPackagingF:message key="LBL_INSERTS"/></td>
									<td class="RightText"><fmtLabelPackagingF:message key="LBL_QTY"/></td>
									<td align="center" class="RightText">&nbsp;<fmtLabelPackagingF:message key="LBL_LABELS"/></td>
									<td class="RightText"><fmtLabelPackagingF:message key="LBL_QTY"/></td>
								</tr>
								<tr><td colspan="7" bgcolor="#666666" height="1"></td></tr>
								<tr><td colspan="7" bgcolor="#eeeeee" height="1"></td></tr>
								<tr>
									<td colspan="2" height="20">&nbsp;&nbsp;<b><%=strInsert%></b></td>
									<td colspan="2" height="20" class="RightText">&nbsp;&nbsp;<b>GM<%=strLabel%></b></td>
								</tr>
								<tr><td colspan="7" bgcolor="#eeeeee" height="1"></td></tr>
								<tr>
									<td colspan="2" height="20">&nbsp;&nbsp;</td>
									<td colspan="2" height="20">&nbsp;&nbsp;</td>
								</tr>
								<tr><td colspan="7" bgcolor="#eeeeee" height="1"></td></tr>
								<tr><td colspan="7" height="20">&nbsp;</td></tr>
								<tr><td colspan="7" bgcolor="#eeeeee" height="1"></td></tr>
								<tr><td colspan="7" height="20">&nbsp;</td></tr>
								<tr><td colspan="7" bgcolor="#eeeeee" height="1"></td></tr>
							</table>
						</td>
					</tr>
					<tr><td bgcolor="#666666" colspan="6"></td></tr>
					 
				</table>
			</td>
		</tr>
	</table>
	<BR>
	<BR>
	<BR>
	<BR>
	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td width="250"bgcolor="#666666" height="1"></td>
			<td height="1" width="200"></td>
			<td  width="150" bgcolor="#666666" height="1"></td>
		</tr>
		<tr>
			<td class="RightText" align="center" height="10"> <fmtLabelPackagingF:message key="LBL_VP_OPERATIONS_DESIGNEE"/></td>
			<td class="RightText">&nbsp;</td>
			<td class="RightText" align="center"><fmtLabelPackagingF:message key="LBL_DATE"/></td>
		</tr>
	</table>
	<BR>
	<BR>
	<span class="RightText">
	<%if(!strINSFooter.equals("")){ 
	%>
	   <%=strFooter%>
	<%}else{ %>
		<fmtLabelPackagingF:message key="LBL_GM_REV_DOC"/>
	<%} %>
	</span>
	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr><td height="2"></td></tr>
		<tr>
			<td>
			<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
					<jsp:include page="/common/GmIncludeDateStamp.jsp" />
			</td>	
		</tr>
	</table>
</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
