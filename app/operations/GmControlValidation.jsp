<%
/*********************************************************************************************************
 * File		 		: GmControlValidation.jsp
 * Desc		 		: This screen is used to validate the control#
 * Version	 		: 1.0
 * author			: dreddy
**********************************************************************************************************/
%>
<%@ page language="java"%>
<%@page import="java.util.ArrayList, java.util.HashMap"%>
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtControlValidation" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\GmControlValidation -->

<fmtControlValidation:setLocale value="<%=strLocale%>"/>
<fmtControlValidation:setBundle basename="properties.labels.operations.GmControlValidation"/>

<HTML>
<HEAD>
<TITLE>Globus Medical: Control# Validation</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">

<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>
function fnSetFocus()
{
	var txtPartcnum = document.frmControlValidation.partcnum;
	txtPartcnum.focus();
	txtPartcnum.select();
}

function fnChangeFocus()
{
	var ret = true;
	
	if(event.keyCode == 13)
	 {	 
		if(fnCheck())	
		{
			document.frmControlValidation.submit();		
		}		
		else
		{
			ret =  false;
		}		
	 }
	 return ret;
}

function fnCheck()
{
	var bool = true;
	var txtPartcnum = document.frmControlValidation.partcnum.value;
	if(txtPartcnum == '')
	{
	Error_Details(message_operations[515]);
	bool = false;
	}
	
	if(txtPartcnum != '')
	{
	partcnumarr = txtPartcnum.split('^');
	arrLen = partcnumarr.length;
	   if(arrLen != '2'){
		 Error_Details(message_operations[516]);
		 bool = false;
	   }
	}
	
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();	
		return false;	
	}

	return bool;
}


function fnGo()
{
	if (fnCheck())
	{
		fnStartProgress('Y');
		document.frmControlValidation.submit();		
	}	
}
</script>
</HEAD>
<bean:define id="partnum" name="frmControlValidation" property="partcnum" type="java.lang.String"></bean:define>
<body leftmargin="20" topmargin="10" onload="fnSetFocus();">
<html:form method="POST"  action="/gmControlValidationAction.do" > 

<table border="0" class="DtTable700"  cellspacing="0" cellpadding="0" >	
		<tr>
			<td height="25" class="RightDashBoardHeader" ><fmtControlValidation:message key="LBL_CONTROL_NUMBER_VALIDATION"/></td>
			<td  height="25" class="RightDashBoardHeader" align="right"></td>
			<td height="25" class="RightDashBoardHeader" align="right"></td>
		</tr>	
      <tr class="shade">
           <td class="RightTableCaption" HEIGHT="30" align="right" width="40%"><b><fmtControlValidation:message key="LBL_PART_CONTROL_NUMBER"/>:</b>&nbsp;</td>
          <td><html:text  name="frmControlValidation" property="partcnum" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"  onkeypress="return fnChangeFocus();" value=""/>  
        &nbsp;</td><td align="left" width="30%">
        <fmtControlValidation:message key="LBL_LOAD" var="varLoad"/>
        <gmjsp:button name="Btn_Load" value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;"  onClick="fnGo();" gmClass="button" buttonType="Load" />
     
         </td>
       </tr>
       <tr>
       <td colspan="3">
       <%@include file="/operations/GmPartControlMessage.jsp" %>
       </td>
       </tr>
</table>
<br>
<table style="width:700px;" border="0">

<%
if(partnum.length()>1){
if(strMsg.equals("")){
	%>
	<tr>
		<td align="center" colspan="3">
		<b><span style="color:green; "><fmtControlValidation:message key="LBL_PART_CONTROL_COMBINATION_EXIST"/></span></b>
		</td>  
	</tr>
	<% 
}else{
%>
	<tr>
		<td align="center" colspan="3">
		<b><span  style="color:red;"><fmtControlValidation:message key="LBL_PART_CONTROL_COMBINATION_NOT_EXIST"/></span></b>
		</td>
	</tr>
<%} 
}
%>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>

</BODY>
</HTML>