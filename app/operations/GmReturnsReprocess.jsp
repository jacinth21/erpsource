
<!-- \operations\GmReturnsReprocess.jsp -->
 <%
/**********************************************************************************
 * File		 		: GmReturnsReprocess.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@page import="java.net.URLEncoder"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ taglib prefix="fmtReturnsReprocess" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<%@ taglib prefix="fmtReturnsReprocess" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtReturnsReprocess:setLocale value="<%=strLocale%>"/>
<fmtReturnsReprocess:setBundle basename="properties.labels.operations.GmReturnsReprocess"/>






<!-- GmReturnsReprocess.jsp -->
<fmtReturnsReprocess:setLocale value="<%=strLocale%>"/>
<fmtReturnsReprocess:setBundle basename="properties.labels.operations.GmReturnsReprocess"/>
<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ page import="java.sql.Date" %>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.operations.GmReturnsReprocess", strSessCompanyLocale);
	String strDisabledLbl=GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PART_CANNOT_BE_REPROCESSED_AS_IT_IS_A_LOANER_PART"));
	ArrayList alConsequences = (ArrayList)request.getAttribute("CONSEQUENCES");
	
	String strApplDateFmt = strGCompDateFmt;
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");

	String strhAction = (String)request.getParameter("hAction") == null?"":(String)request.getParameter("hAction");
	String strReturnIds = (String)request.getAttribute("hReturnIds") == null?"":(String)request.getAttribute("hReturnIds");
	String strOraMsg = (String)request.getAttribute("hOraMsg") == null?"":(String)request.getAttribute("hOraMsg");
	String strRAIDs = GmCommonClass.parseNull((String)request.getParameter("hRAIDs"));
	String strRetursAttached = GmCommonClass.parseNull((String)request.getParameter("Txt_Returns"));
	
	
	out.print(strOraMsg);
	String stroperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	String strRAId = "";
	String strDesc = "";
	String strSetName = "";
	String strReason = "";
	String strSplitFlag="";
	String strType = "";
	String strRetType = "";
	String strCreatedBy = "";
	java.sql.Date dtCreatedDate = null;
	java.sql.Date dtEDate = null;
	java.sql.Date dtRDate = null;
	String strDistName = "";
	String strAcctName = "";
	String strRetDate = "";
	String strExpDate = "";
	String strCreditDate = "";

	String strItemQty = "";
	String strControl = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strItemType = "";
	String strsterileflVal="";
	String strSetID = "";
	String strTagable = "";
	String strDate = "";
	ArrayList alSetLoad = new ArrayList();
	HashMap hmReturnDetails = new HashMap();
	String strRetDefQtyFl = "";
	int intPriceSize = 0;

	if (hmReturn != null)
	{
			alSetLoad = (ArrayList)hmReturn.get("RAITEMDETAILS");
			log.debug("alSetLoad : "+alSetLoad);
			hmReturnDetails = (HashMap)hmReturn.get("RADETAILS");
			strRAId = GmCommonClass.parseNull((String)hmReturnDetails.get("RAID"));
			strSetName = GmCommonClass.parseNull((String)hmReturnDetails.get("SNAME"));
			strSetID = GmCommonClass.parseNull((String)hmReturnDetails.get("SETID"));
			strType = GmCommonClass.parseNull((String)hmReturnDetails.get("TYPE"));
			strRetType = GmCommonClass.parseNull((String)hmReturnDetails.get("TYPEID"));
			strReason = GmCommonClass.parseNull((String)hmReturnDetails.get("REASON"));
			strSplitFlag=GmCommonClass.parseNull((String)hmReturnDetails.get("SPLITFL"));
			
			strCreatedBy = GmCommonClass.parseNull((String)hmReturnDetails.get("CUSER"));
			dtCreatedDate= (java.sql.Date)hmReturnDetails.get("CDATE");
			strDate = GmCommonClass.getStringFromDate(dtCreatedDate,strApplDateFmt);
			
			strDesc = GmCommonClass.parseNull((String)hmReturnDetails.get("COMMENTS"));
			strDistName = GmCommonClass.parseNull((String)hmReturnDetails.get("DNAME"));
			strAcctName = GmCommonClass.parseNull((String)hmReturnDetails.get("ANAME"));
			dtRDate = (java.sql.Date)hmReturnDetails.get("RETDATE");
			strRetDate = GmCommonClass.getStringFromDate(dtRDate,strApplDateFmt);
			dtEDate = (java.sql.Date)hmReturnDetails.get("EDATE");
			strExpDate = GmCommonClass.getStringFromDate(dtEDate,strApplDateFmt);
			strCreditDate = GmCommonClass.parseNull(GmCommonClass.getStringFromDate((java.sql.Date)hmReturnDetails.get("CRDATE"),strApplDateFmt));
			
			strDistName = strDistName.equals("")?strAcctName:strDistName;
			strRetDefQtyFl = GmCommonClass.parseNull((String)hmReturn.get("RETURNDEFQTYFL"));
	}
	
	String strSelectedSetID = request.getParameter("Cbo_SetID") == null ? "0" : GmCommonClass.parseNull((String)request.getParameter("Cbo_SetID"));
    
	String strColSpan = "4";
	if(strRetType.equals("3309") || strRetType.equals("3308")){
		strColSpan = "3";
	}
    if(strSelectedSetID.equals("0") && 	!strSetID.equals(""))
    {
	   strSelectedSetID = strSetID;
    }
    
	ArrayList alSets = request.getAttribute("SETS") == null ? new ArrayList() : (ArrayList) request.getAttribute("SETS");	
	
	String strHMode = GmCommonClass.parseNull(request.getParameter("hMode"));
	strHMode = strHMode.equals("") ? "View" : strHMode;
	
	int intSize = 0;
	HashMap hcboVal = null;
	
	//String strWikiPath = GmCommonClass.getString("GWIKI");
	String strWikiTitle = GmCommonClass.getWikiTitle("RETURN_REPROCESS");
	
	// Get the company information from gmDataStoreVO
		String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
		strCompanyInfo = URLEncoder.encode(strCompanyInfo);
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Returns Reprocess </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>


<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=stroperationsJsPath%>/GmReturnsReprocess.js"></script>
<script>
var splitfl = '<%=strSplitFlag%>';

</script>
</HEAD>
<BODY leftmargin="20" topmargin="10">
<FORM name="frmVendor" method="POST" action="/gmRuleEngine.do?companyInfo=<%=strCompanyInfo%>">
<%-- <FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmReturnProcessServlet">--%>
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hId" value="<%=strRAId%>">
<input type="hidden" name="hCNStr" value="">
<input type="hidden" name="hQNStr" value="">
<input type="hidden" name="hQNSterileStr" value="">
<input type="hidden" name="hPNStr" value="">
<input type="hidden" name="hIAStr" value="">
<input type="hidden" name="hIHStr" value="">
<input type="hidden" name="hRetType" value="<%=strRetType%>">
<input type="hidden" name="hRAIDs" value="<%=strRAIDs%>">
<input type="hidden" name="hRequestID" value="">
<input type="hidden" name="hTxnId">
<input type="hidden" name="hCancelType">
<input type="hidden" name="RE_FORWARD" value="gmReturnProcessServlet">
<input type="hidden" name="txnStatus" value="VERIFY">
<input type="hidden" name="RE_TXN" value="RETURNSWRAPPER">
<input type="hidden" name="TXN_TYPE_ID" value="RETURN">
<input type="hidden" name="CNRTNTYPE" value="">
<input type="hidden" name="PNRTNTYPE" value=""> 
<input type="hidden" name="hFGStr" value=""><!--PC-4788-New Inhouse Transaction - Returns to Finished Goods  -->
	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" colspan="3" class="RightDashBoardHeader"><fmtReturnsReprocess:message key="LBL_RETURNS_REPROCESS"/></td>
			<td align="right" class=RightDashBoardHeader > 
			<fmtReturnsReprocess:message key="IMG_HELP" var="varHelp"/>	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>		
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="4"></td></tr>
		<tr> 
		 <td height="1" colspan="4">
				<jsp:include page="/operations/returns/GmIncludeReturnHeader.jsp" >
				<jsp:param name="hMode" value="<%=strHMode%>" />				
				</jsp:include>
			</td>
		</tr>
		<tr class="ShadeRightTableCaption"><td colspan="4" height="25"><fmtReturnsReprocess:message key="LBL_REQUESTS_ASSOCIATED_TO_THE_SET"/></td></tr>
		<tr>
		<td height="1" colspan="4">
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->		    
			<jsp:include page="/operations/returns/GmSetRequests.jsp" />					
			</td>
		</tr>
		<tr><td colspan="4" class="Line" height="1"></td></tr>
		<tr><td colspan="4" height="10"></td></tr>
         <tr>
            <td>
              <table width="100%" cellpadding="0" cellspacing="0" border="0"><tr>
		           <td class="RightTableCaption" align="right" height="25" colspan="2"><fmtReturnsReprocess:message key="LBL_SET"/>:</td>
	           <td valign="middle" colspan="2">&nbsp;<gmjsp:dropdown controlName="Cbo_SetID" seletedValue="<%=strSelectedSetID%>" 	
				  value="<%= alSets%>" codeId = "ID"  codeName = "NAME"  defaultValue="[Choose One]" onChange="fnReload();" />
	           </td>
	         </tr>	
	          <tr>
		           <td colspan="2" class="RightTableCaption" align="right" height="25"><b><fmtReturnsReprocess:message key="LBL_ATTACH_RETURNS"/>:</b></td>
		           <td  valign="middle">&nbsp;<input type="text" name="Txt_Returns" class="InputArea" value=<%=strRetursAttached%>>&nbsp;&nbsp;   <fmtReturnsReprocess:message key="BTN_ADD" var="varAdd"/>
		           <gmjsp:button value="&nbsp;${varAdd}&nbsp;" gmClass="button" onClick="fnAdd();" buttonType="Load" /></td>
		         </tr>	
		        </table> 	         
		      </td>
		    </tr>     
		    <tr><td colspan="4" height="10"></td></tr>
					
<%					
   if(strhAction.equals("Reload") || strhAction.equals("Save") || strhAction.equals("AddRA") )
    {	    
%>    				
					<tr>
						<td colspan="4">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">
								<tr><td colspan="9" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td height="25" rowspan="2"><fmtReturnsReprocess:message key="LBL_PART_NUMBER"/></td>
									<td width="350" rowspan="2"><fmtReturnsReprocess:message key="LBL_DESCRIPTION"/></td>
									<td width="25" rowspan="2"><fmtReturnsReprocess:message key="LBL_SET_QTY"/></td>
									<td align="center" rowspan="2"><fmtReturnsReprocess:message key="LBL_QTY"/><BR><fmtReturnsReprocess:message key="LBL_CREDITED"/></td>
									<td align="center" rowspan="2"><fmtReturnsReprocess:message key="LBL_CONTROL"/><br><fmtReturnsReprocess:message key="LBL_NUMBER"/></td>
									<td align="center" colspan="<%=strColSpan%>"><fmtReturnsReprocess:message key="LBL_REPROCESS_QTY"/></td>
								</tr>
								<%
									if (strRetType.equals("3308")){												
								%>
								<tr class="ShadeRightTableCaption">
									<td align="center"><fmtReturnsReprocess:message key="LBL_RHIA"/></td>
									<td align="center"><fmtReturnsReprocess:message key="LBL_RHQN"/></td>
									<td align="center"><fmtReturnsReprocess:message key="LBL_RHIH"/></td>
								</tr>								
								<%
									}else if (strRetType.equals("3309")){												
								%>
								<tr class="ShadeRightTableCaption">
									<td align="center"><fmtReturnsReprocess:message key="LBL_RHQN"/></td>
									<td align="center"><fmtReturnsReprocess:message key="LBL_RFG"/></td><!--RFG column added- PC-4788 -->
									<td align="center"><fmtReturnsReprocess:message key="LBL_RHPN"/></td>
								</tr>
								<%}else{ %>
								<tr class="ShadeRightTableCaption">
									<td align="center"><fmtReturnsReprocess:message key="LBL_CN"/></td>
									<td align="center"><fmtReturnsReprocess:message key="LBL_RQN"/></td>
									<td align="center"><fmtReturnsReprocess:message key="LBL_RFG"/></td><!--RFG column added- PC-4788 -->
									<td align="center"><fmtReturnsReprocess:message key="LBL_RPN"/></td>
								</tr>	
								<%} %>							
								<tr><td class=Line colspan=9 height=1></td></tr>
								
<%
						intSize = alSetLoad != null ? alSetLoad.size() : 0;
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();
							HashMap hmTempLoop = new HashMap();
							
							String strNextPartNum = "";
							int intCount = 0;
							String strColor = "";
							String strLine = "";
							String strPartNumHidden = "";
							boolean bolQtyFl = false;
							
							
							String strSetFlag = "";
							String strSetQty = "";
							
							for (int i=0;i<intSize;i++)
							{
							
							String strDisabled = "";
							String strQty = "";
							String strPackQty = "";
							String strAllDisabled = "";
							
								hmLoop = (HashMap)alSetLoad.get(i);
								if (i<intSize-1)
								{
									hmTempLoop = (HashMap)alSetLoad.get(i+1);
									strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("PNUM"));
									
								}
								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
								strPartNumHidden = strPartNum;
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strItemQty = GmCommonClass.parseNull((String)hmLoop.get("IQTY"));
								strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
								strSetFlag = GmCommonClass.parseNull((String)hmLoop.get("ISSETFL"));
								strSetQty = GmCommonClass.parseNull((String)hmLoop.get("SET_QTY"));
								strItemType = GmCommonClass.parseNull((String)hmLoop.get("ITYPE"));
								strTagable = GmCommonClass.parseNull((String)hmLoop.get("TAGABLE"));
								//Get sterile flag from GmProcessReturnsBean
								strsterileflVal=GmCommonClass.parseNull((String)hmLoop.get("PARTCLASS"));
							
							
								if(strSetFlag.equals("N"))
								 {
								 strPackQty = strItemQty;
								  strDisabled = "disabled";
								 }
								 else{
									strQty = strItemQty;
								}								
																
								if (strPartNum.equals(strNextPartNum))
								{
									intCount++;
									strLine = "<TR><TD colspan=8 height=1 bgcolor=#eeeeee></TD></TR>";
								}

								else
								{
									strLine = "<TR><TD colspan=8 height=1 bgcolor=#eeeeee></TD></TR>";
									if (intCount > 0)
									{
										strPartNum = "";
										strPartDesc = "";
										strLine = "";
									}
									else
									{

									}
									intCount = 0;
								}

								if (intCount > 1)
								{
									strPartNum = "";
									strPartDesc = "";
									strLine = "";
								}
								
								
							
								if(!strSetQty.equals("") && !strQty.equals(""))
								{
									 int iQty = Integer.parseInt(strQty);
									 int iSetQty = Integer.parseInt(strSetQty);
									 int iPackQty = Integer.parseInt(GmCommonClass.parseZero(strPackQty));
									  if( iQty > iSetQty)
									  {
										 int iDiff = iQty - iSetQty;
										 iQty = iQty - iDiff;
										 iPackQty = iPackQty + iDiff;
									  }
									 strPackQty = iPackQty == 0 ? "" : ""+iPackQty;
									 strQty = iQty == 0 ? "" : ""+iQty;									
								}
								
								if(strSetQty.equals("0"))
								{
									strSetQty = "-";
									strDisabled = "disabled";
									if(strItemType.equals("50301"))
									{
										strPackQty = "";
										strAllDisabled = "disabled title='"+strDisabledLbl+"'";
									}
									else
									{
										strPackQty = strItemQty;
									}

								}
								String strAlign = "center";
								if(!strTagable.equals(""))
								{
									strAlign ="left";
								}
								
								out.print(strLine);
%>
								<tr>
		
									<td class="RightText" height="20" align="<%=strAlign%>">
									<% if(!strTagable.equals("")){%>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=javascript:fnOpenTag('<%=strPartNum %>','<%=strRAId %>')>  <img style='cursor:hand' src='<%=strImagePath%>/tag.jpg'  width='12' height='12' />  </a><%}%>&nbsp;&nbsp;&nbsp;<%=strPartNum%>
									<input type="hidden" name="hPartNum<%=i%>" value="<%=strPartNumHidden%>">
									<input type="hidden" name="hISterileFlag<%=i%>" value="<%=strsterileflVal%>"></td>
									<td class="RightText"><%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									<td class="RightText"><%=strSetQty%> <input type="hidden" name="hSetQty<%=i%>" value="<%=strSetQty%>"></td>
									<td align="center" class="RightText"><%=strItemQty%>
									<input type="hidden" name="hQty<%=i%>" value="<%=strItemQty%>"></td>
									<td class="RightText">&nbsp;<%=strControl%>
									<input type="hidden" name="hCNum<%=i%>" value="<%=strControl%>">
									<input type="hidden" name="hIType<%=i%>" value="<%=strItemType%>"></td>
									<!-- Add Sterile flag in a hidden field for each part number -->
									
									<%if(strRetType.equals("3308")){ %>
										<td>&nbsp;<input type="text" <%=strAllDisabled%> name="Txt_IA<%=i%>" value="" class="RightText" size="3" ></td>
										<td>&nbsp;<input type="text" <%=strAllDisabled%> name="Txt_QN<%=i%>" value="" class="RightText" size="3" ></td>
										<td>&nbsp;<input type="text" <%=strAllDisabled%> name="Txt_IH<%=i%>" value="" class="RightText" size="3" ></td>
									<%}else if (strRetType.equals("3309")){
										if(strRetDefQtyFl.equals("Y")){%>
										<td>&nbsp;<input type="text" <%=strAllDisabled%> name="Txt_QN<%=i%>" value="<%=strPackQty%>" class="RightText" size="3" ></td>
										<td>&nbsp;<input type="text" <%=strAllDisabled%> name="Txt_FG<%=i%>" value="" class="RightText" size="3"></td>	<!--RFG column added- PC-4788 -->
										<td>&nbsp;<input type="text" <%=strAllDisabled%> name="Txt_PN<%=i%>" value="" class="RightText" size="3"></td>	
										<%}else{ %>
											<td>&nbsp;<input type="text" <%=strAllDisabled%> name="Txt_QN<%=i%>" value="" class="RightText" size="3" ></td>
										<td>&nbsp;<input type="text" <%=strAllDisabled%> name="Txt_FG<%=i%>" value="" class="RightText" size="3"></td>	<!--RFG column added- PC-4788 -->
										<td>&nbsp;<input type="text" <%=strAllDisabled%> name="Txt_PN<%=i%>" value="<%=strPackQty%>" class="RightText" size="3"></td>	
										<%} %>							
									<%}else{ %>
									<td>&nbsp;<input type="text" <%=strDisabled%> <%=strAllDisabled%> name="Txt_CN<%=i%>" value="<%=strQty%>" class="RightText" size="3"></td>
									<% if(strRetDefQtyFl.equals("Y")){ %>
										<td>&nbsp;<input type="text" <%=strAllDisabled%> name="Txt_QN<%=i%>" value="<%=strPackQty%>" class="RightText" size="3" ></td>
										<td>&nbsp;<input type="text" <%=strAllDisabled%> name="Txt_FG<%=i%>" value="" class="RightText" size="3"></td>	<!--RFG column added- PC-4788 -->
										<td>&nbsp;<input type="text" <%=strAllDisabled%> name="Txt_PN<%=i%>" value="" class="RightText" size="3"></td>	
										<%}else{ %>
										<td>&nbsp;<input type="text" <%=strAllDisabled%> name="Txt_QN<%=i%>" value="" class="RightText" size="3" ></td>
										<td>&nbsp;<input type="text" <%=strAllDisabled%> name="Txt_FG<%=i%>" value="" class="RightText" size="3"></td>	<!--RFG column added- PC-4788 -->
										<td>&nbsp;<input type="text" <%=strAllDisabled%> name="Txt_PN<%=i%>" value="<%=strPackQty%>" class="RightText" size="3"></td>	
										<%} %>	
									<%} %>
								</tr>
<%
							}//End of FOR Loop
						}else {
%>
						<tr><td colspan="5" height="50" align="center" class="RightTextRed"><BR><fmtReturnsReprocess:message key="LBL_NO_PARTS_HAVE_BEEN_CREDITED_OR_ERROR_RETRIEVING_PARTS"/></td></tr>
<%
						}		
%>
							</table>
						</td>								<input type="hidden" name="hCnt" value="<%=intSize%>">
					</tr>
					
<%
    }
%>
					
					
				 </table>
			 </td>
		</tr>	
    </table>
 <%
		if (intSize > 0) {
%>	
	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		
		<tr><td colspan="2" height="1" ></td></tr>
		<tr>
			<td colspan="2">
			<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
					<jsp:include page="/common/GmRuleDisplayInclude.jsp"/>
			</td>
		</tr>
		<tr><td colspan="2" height="1" ></td></tr>
		<tr>
			<td colspan="2" id="log"> 
				<jsp:include page="/common/GmIncludeLog.jsp" >
					<jsp:param name="LogType" value="" />
					<jsp:param name="LogMode" value="Edit" />
				</jsp:include>
			</td>
		</tr>
		<tr>	
			<td colspan="4" height="30" align="center">

<%
		if (intSize > 0) {
%>						
						
				<fmtReturnsReprocess:message key="BTN_ROLLBACK" var="varRollBack"/><gmjsp:button value="${varRollBack}" gmClass="button" buttonType="Save" onClick="fnRollback();"/>
				<fmtReturnsReprocess:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" gmClass="button" onClick="fnSubmit();" buttonType="Save" />
						
<%
		}else if (strhAction.equals("Save")) {
%>
		<fmtReturnsReprocess:message key="LBL_FOLLOWING_REPROCESS_IDS_WERE_GENERATED_AS_A_RESULT_OF_THIS_TRANSACTION"/>: <%=strReturnIds%>
<%
		}
	 	
%>
			</td>
		</tr>
	</table>
	
	<% 
	 }	
%>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>

</BODY>

</HTML>
