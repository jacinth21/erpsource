 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
/**********************************************************************************
 * File		 		: GmDHRUpdate.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,org.apache.commons.beanutils.DynaBean,java.util.Iterator,java.util.List" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtDHRUpdate" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\GmDHRUpdate.jsp -->

<fmtDHRUpdate:setLocale value="<%=strLocale%>"/>
<fmtDHRUpdate:setBundle basename="properties.labels.operations.GmDHRUpdate"/>


<%
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	strCssPath = GmCommonClass.getString("GMSTYLES");
	strImagePath = GmCommonClass.getString("GMIMAGES");
	strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	strCommonPath = GmCommonClass.getString("GMCOMMON");
	String strWikiTitle = GmCommonClass.getWikiTitle("DEVICE_HISTORY_RECORD_UPDATE_DHR");
	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strMode = (String)request.getAttribute("hMode")==null?"":(String)request.getAttribute("hMode");
	String hAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	GmResourceBundleBean gmResourceBundleBeanlbl = 
	    GmCommonClass.getResourceBundleBean("properties.labels.operations.GmDHRUpdate", strSessCompanyLocale);
	// System.out.println(" Str Mode is " + strMode);
	DynaBean Dbean = null;
	List ldhrPartResult = (List)request.getAttribute("ldhrPartResult");
	ArrayList alReturnReasons = (ArrayList)request.getAttribute("hNCMRReason");
	HashMap hmDHRDetails = new HashMap();
	HashMap hmInHouseTxn=new HashMap();
	ArrayList alEmpList = new ArrayList();
	ArrayList alSubDHR = new ArrayList();
	
	//Added to display as blocks
	String strInspImg = strImagePath + "/plus.gif";
	String strInspStyle = "display:none";
	String strInspBgColor = "#e4e6f2";
	String strPackImg = strImagePath + "/plus.gif";
	String strPackStyle ="display:none";
	String strPackBgColor = "#e4e6f2";
	String strVerImg = strImagePath + "/minus.gif";
	String strVerStyle = "display:block";
	String strVerBgColor = "#e4e6f2";

	String strShade = "";
	
	String strDHRId = "";
	String strVendorId = "";
	String strVendName = "";
	String strPartNum = "";
	String strDesc = "";
	String strWOId = "";
	String strControlNum = "";
	String strQtyRec = "";
	String strQtyInspected = "";
	String strQtyReject = "";
	String strActionTaken = "";
	String strCodeID = "";
	String strSelected = "";
	String strReceivedBy = "";
	String strInspectedBy = "";
	String strInspectedByNm = "";
	String strQtyPackaged = "";
	String strPackagedBy = "";
	String strPackagedByNm = "";
	String strQtyInShelf = "";
	String strVerifiedBy = "";
	String strVerifiedByNm = "";
	String strNCMRId = "";
	String strCriticalFl = "";
	String strCreatedDate = "";
	String strRejReason = "";
	String strReasonType = "";
	String strReasonTypeID = "";
	String strCloseQty = "";
	String strNCMRFl = "";
	String strReceivedTS = "";
	String strInspectedTS = "";
	String strPackagedTS = "";
	String strVerifiedTS = "";
	String strSubComFl = "";
	String strSterFl = "";
	String strChecked = "";
	String strLabelFl = "";
	String strPackFl = "";
	String strPartSterFl = "";
	String strFooter = "";
	String strDocRev = "";
	String strDocActiveFl = "";
	String strFARFl = "";
	String strBackOrderFl = "";	
	String strNCMRAction = "";
	String strNCMRRemarks = "";
	String strNCMRAuthBy = "";
	String strNCMRAuthDt = "";
	String strDisabled = "";
	String strStatusFl = "";
	String strSessErrMsg = "";
	String strStatusErrFl = "NULL";
	String strFromPage = "";
	String strDHRVoidFl = "";
	String strQtyShipped = "";
	String strUOM = "";
	String strGridXmlData = "";
	String strBQtyPackaged="";
	String strRMQtyPackaged="";
	String strInsertID ="";
	String strPrimaryPack ="";
	String strSecondaryPack="";
	String strPoType="";
	String strPartClass="";
	String strTxnSts = "";
	String strUdiVerify = "";
	String strUdiChecked = "";
	String strProdType = "";
	String strDonorNum = "";
	String strSerialNumFl = "";
	String strApplnDateFmt = strGCompDateFmt;
	NumberFormat nf = NumberFormat.getInstance();
	String strUserId = 	(String)session.getAttribute("strSessUserId");
	String strDeptId =GmCommonClass.parseNull((String)session.getAttribute("strSessDeptSeq"));
	String strQtyRecd="0";
	strDHRId = (String)hmDHRDetails.get("ID")==null?"":(String)hmDHRDetails.get("ID");
	
	String strXmlData=GmCommonClass.parseNull((String)request.getAttribute("XMLDATA"));
	//strDHRId = "";
	
	int intNCMRFl = 0;
	
	if (hmReturn != null)
	{
		hmDHRDetails = (HashMap)hmReturn.get("DHRDETAILS");
		hmInHouseTxn=GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("INHOUSETXNS"));
		alSubDHR = (ArrayList)hmReturn.get("SUBDHRDETAILS");
		strGridXmlData = GmCommonClass.parseNull((String)hmReturn.get("GRIDXMLDATA"));
		strDHRId        = GmCommonClass.parseNull((String)hmDHRDetails.get("ID"));
		strVendorId 	= GmCommonClass.parseNull((String)hmDHRDetails.get("VID"));
		strVendName 	= GmCommonClass.parseNull((String)hmDHRDetails.get("VNAME"));
		strPartNum 		= GmCommonClass.parseNull((String)hmDHRDetails.get("PNUM"));
		strDesc 		= GmCommonClass.parseNull((String)hmDHRDetails.get("PDESC"));
		strWOId 		= GmCommonClass.parseNull((String)hmDHRDetails.get("WOID"));
		strControlNum 	= GmCommonClass.parseNull((String)hmDHRDetails.get("CNUM"));
		strQtyRec 		= GmCommonClass.parseZero((String)hmDHRDetails.get("QTYREC"));
		strReceivedBy 	= GmCommonClass.parseNull((String)hmDHRDetails.get("UNAME"));
		strQtyInspected = GmCommonClass.parseZero((String)hmDHRDetails.get("QTYINSP"));
		strQtyReject 	= GmCommonClass.parseZero((String)hmDHRDetails.get("QTYREJ"));
		strActionTaken 	= GmCommonClass.parseNull((String)hmDHRDetails.get("ACTTKN"));
		strInspectedBy 	= GmCommonClass.parseNull((String)hmDHRDetails.get("INSPBY"));
		strInspectedByNm = GmCommonClass.parseNull((String)hmDHRDetails.get("INSPBYNM"));
		strPackagedBy 	= GmCommonClass.parseNull((String)hmDHRDetails.get("PACKBY"));
		strPackagedByNm = GmCommonClass.parseNull((String)hmDHRDetails.get("PACKBYNM"));
		strQtyInShelf 	= GmCommonClass.parseNull((String)hmDHRDetails.get("QTYSHELF"));
		strVerifiedBy 	= GmCommonClass.parseNull((String)hmDHRDetails.get("VERIFYBY"));
		strVerifiedByNm = GmCommonClass.parseNull((String)hmDHRDetails.get("VERIFYBYNM"));
		strNCMRId 		= GmCommonClass.parseNull((String)hmDHRDetails.get("NCMRID"));
		strCriticalFl 	= GmCommonClass.parseNull((String)hmDHRDetails.get("CFL"));
		//strCreatedDate 	= GmCommonClass.parseNull((String)hmDHRDetails.get("CDATE"));
		strCreatedDate = GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("CDATE"),strApplnDateFmt);
		strRejReason 	= GmCommonClass.parseNull((String)hmDHRDetails.get("REJREASON"));
		strReasonType 	= GmCommonClass.parseNull((String)hmDHRDetails.get("REASON_TYPE"));
		strReasonTypeID = GmCommonClass.parseNull((String)hmDHRDetails.get("REASON_ID"));
		strNCMRFl 		= GmCommonClass.parseZero((String)hmDHRDetails.get("NCMRFL"));
		strCloseQty 	= GmCommonClass.parseZero((String)hmDHRDetails.get("CLOSEQTY"));
		strReceivedTS 	= GmCommonClass.parseNull((String)hmDHRDetails.get("RECTS"));
		strInspectedTS 	= GmCommonClass.parseNull((String)hmDHRDetails.get("INSTS"));
		strPackagedTS 	= GmCommonClass.parseNull((String)hmDHRDetails.get("PACKTS"));
		strVerifiedTS 	= GmCommonClass.parseNull((String)hmDHRDetails.get("VERTS"));
		strSubComFl 	= GmCommonClass.parseNull((String)hmDHRDetails.get("SUBFL"));
		strSterFl 		= GmCommonClass.parseNull((String)hmDHRDetails.get("STERFL"));
		strChecked 		= GmCommonClass.parseNull((String)hmDHRDetails.get("SKIPLBLFL"));
		strNCMRAction 	= GmCommonClass.parseNull((String)hmDHRDetails.get("DISP"));
		strNCMRRemarks 	= GmCommonClass.parseNull((String)hmDHRDetails.get("DISPREMARKS"));
		strNCMRAuthBy 	= GmCommonClass.parseNull((String)hmDHRDetails.get("DISPBY"));
		//strNCMRAuthDt 	= GmCommonClass.parseNull((String)hmDHRDetails.get("DISPDATE"));
		strNCMRAuthDt = GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("DISPDATE"),strApplnDateFmt);
		strBackOrderFl	= GmCommonClass.parseNull((String)hmDHRDetails.get("BACKORDER_FLAG"));
		strStatusFl = GmCommonClass.parseNull((String)hmDHRDetails.get("SFL"));
		strPoType = GmCommonClass.parseNull((String)hmDHRDetails.get("WOTYPE"));
		strPartClass = GmCommonClass.parseNull((String)hmDHRDetails.get("PCLASS"));
		// strDHRVoidFl = GmCommonClass.parseNull((String)hmDHRDetails.get("DHRVFL");
		strUOM = GmCommonClass.parseNull((String)hmDHRDetails.get("UOMANDQTY"));
		strQtyShipped = GmCommonClass.parseNull((String)hmDHRDetails.get("SHIPQTY"));
		strUdiVerify = GmCommonClass.parseNull((String)hmDHRDetails.get("UDIVERIFY")); 
		strLabelFl = strChecked.equals("1")?"Yes":"-";
		strChecked = strChecked.equals("1")?"checked":"";
		strUdiChecked = strUdiVerify.equals("Y")?"checked":"";
		strPackFl = GmCommonClass.parseNull((String)hmDHRDetails.get("SKIPPACKFL"));
		strPackFl = strPackFl.equals("1")?"Yes":"-";
		strInsertID = GmCommonClass.parseNull((String)hmDHRDetails.get("INSID"));
		strPrimaryPack = GmCommonClass.parseNull((String)hmDHRDetails.get("PACKPRIM"));
		strSecondaryPack = GmCommonClass.parseNull((String)hmDHRDetails.get("PACKSEC"));
		strProdType = GmCommonClass.parseNull((String)hmDHRDetails.get("PRODTYPE"));
		strDonorNum 	= GmCommonClass.parseNull((String)hmDHRDetails.get("DONORNUMBER"));
		strSerialNumFl 	= GmCommonClass.parseNull((String)hmDHRDetails.get("SERIALNUMNEEDFL"));
		// System.out.println(" Value of Status flag from Backend " + strStatusFl);
		
		String strMod = "";
		String strCnum ="";
		strFromPage = (String)request.getAttribute("FromPage")==null?"":(String)request.getAttribute("FromPage");
		strCnum = strControlNum.toUpperCase();
		if(strPartClass.equals("4031") && strPoType.equals("3101") && strCnum.equals("NOC#"))
		{
			strTxnSts = "";
		}
		else
		{
			strTxnSts = "PROCESS";
		}
		
		if (strStatusFl .equals("0")){
			strMod = "C";
		}
		else if (strStatusFl.equals("1")){
			strMod = "I";
			
			strInspImg = strImagePath + "/minus.gif";
			strInspStyle = "display:block";
			strInspBgColor = "#9AADF";
			
			strPackImg = strImagePath + "/plus.gif";
			strPackStyle = "display:none";
			strVerImg = strImagePath + "/plus.gif";
			strVerStyle = "display:none";
		}
		else if (strStatusFl.equals("2")){
			strMod = "P";
			
			strPackImg = strImagePath + "/minus.gif";
			strPackStyle = "display:block";
			strPackBgColor = "#9AADF";
			
			strInspImg = strImagePath + "/plus.gif";
			strInspStyle = "display:none";
			strVerImg = strImagePath + "/plus.gif";
			strVerStyle = "display:none";
		}
		else if (strStatusFl.equals("3")){
			strMod = "V";
			
			strVerImg = strImagePath + "/minus.gif";
			strVerStyle = "display:block";
			strVerBgColor = "#9AADF";
			
			strInspImg = strImagePath + "/plus.gif";
			strInspStyle = "display:none";
			strPackImg = strImagePath + "/plus.gif";
			strPackStyle = "display:none";
		}
		
		if (!(strMod.equals(strMode)))
		{
		   strSessErrMsg = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_DATA_ALREADY_FILLED"));
		   strStatusErrFl = strMode.concat(strFromPage);
		   strDisabled = "true";
		   strMode = strMod;
		}
		
		strPartSterFl = GmCommonClass.parseNull((String)hmDHRDetails.get("PCLASS"));
		strPartSterFl = strPartSterFl.equals("4030")?"1":"";
		
		strFooter = GmCommonClass.parseNull((String)hmDHRDetails.get("FOOTER"));
		if(!strFooter.equals("")){
			
		String strArr[] = strFooter.split("\\^");
		strFooter = strArr[0];
		strDocRev = strArr[1];
		strDocActiveFl = strArr[2];
	}
		strFARFl = GmCommonClass.parseNull((String)hmDHRDetails.get("FARFL"));
								
		intNCMRFl = Integer.parseInt(strNCMRFl);
		
		alEmpList = (ArrayList)hmReturn.get("EMPLIST");
		
	}
	
	strQtyRec = GmCommonClass.parseZero(strQtyRec);
	strQtyReject = GmCommonClass.parseZero(strQtyReject);
	strCloseQty = GmCommonClass.parseZero(strCloseQty);
	
	
	try {
		strQtyRecd=""+ (Integer.parseInt(strQtyRec)-Integer.parseInt(strQtyReject) + Integer.parseInt(strCloseQty));
		nf = java.text.NumberFormat.getIntegerInstance();			
	}
	catch(NumberFormatException nfe)
	{
		strQtyRecd=""+ (Double.parseDouble(strQtyRec)-Double.parseDouble(strQtyReject) + Double.parseDouble(strCloseQty));					
	}		
	

	int intSize = 0;
	HashMap hcboVal = null;
	
    java.util.Calendar cal = java.util.Calendar.getInstance(java.util.TimeZone.getDefault());
    String DATE_FORMAT = "MM/dd/yyyy";
    java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(DATE_FORMAT);
    sdf.setTimeZone(java.util.TimeZone.getDefault());
	String strDate = sdf.format(cal.getTime());
	String strTotal = "";
	if(ldhrPartResult != null)
	{
		for (Iterator iterator = ldhrPartResult.iterator(); iterator.hasNext();)
		{
			Dbean = (DynaBean) iterator.next();
			strTotal = String.valueOf(Dbean.get("DHRQTY"));
			strQtyPackaged = (strQtyPackaged.equals(""))?String.valueOf(Dbean.get("TO_SHELF")):strQtyPackaged;
			strBQtyPackaged = (strBQtyPackaged.equals(""))?String.valueOf(Dbean.get("TO_BULK")):strBQtyPackaged;
			strRMQtyPackaged = (strRMQtyPackaged.equals(""))?String.valueOf(Dbean.get("TO_RM")):strRMQtyPackaged;
		}
		
		if(!strQtyPackaged.equals("") && !strBQtyPackaged.equals("") && !strRMQtyPackaged.equals("") && (!strNCMRId.startsWith("GM-EVAL") || (intNCMRFl!=0)) && intNCMRFl!=1){ 
			// Shelf Qty CHeck
			int intValue=Integer.parseInt(strQtyRecd)-Integer.parseInt(strQtyPackaged);

			if(intValue>0){
				// Bulk Qty Check
				if(intValue <= Integer.parseInt(strBQtyPackaged)){
					strBQtyPackaged = "" + intValue;
					intValue = 0;
				}else{
					intValue = intValue - Integer.parseInt(strBQtyPackaged);
				}
				// RM Qty Check
				if(intValue > 0 && intValue<=Integer.parseInt(strRMQtyPackaged)){
					strRMQtyPackaged = "" + intValue;
					intValue = 0;
				}else{
					intValue = intValue - Integer.parseInt(strRMQtyPackaged);
				}	
			}
			else
			{
				 if(NumberUtils.isNumber(strQtyRecd))
				 {				
					strQtyPackaged =  strQtyRecd ;	
				 }
				 else
				 {
					 strQtyPackaged = "";
				 }
				strBQtyPackaged="0";
				strRMQtyPackaged = "0";
			}

			double dbRemain = Double.parseDouble(GmCommonClass.parseZero(strQtyRecd)) - Double.parseDouble(GmCommonClass.parseZero(strQtyPackaged)) - Double.parseDouble(GmCommonClass.parseZero(strBQtyPackaged)) - Double.parseDouble(GmCommonClass.parseZero(strRMQtyPackaged));
			
			
			if(dbRemain>0){
				strQtyPackaged = "" + nf.format((Double.parseDouble(GmCommonClass.parseZero(strQtyPackaged)) + dbRemain));
				strQtyPackaged = strQtyPackaged.replace(",","");
				}
			
		}else{
			strQtyPackaged = "";
			strBQtyPackaged="";
			strRMQtyPackaged = "";
		}
		
	  
	}
	//The following code added for passing the company info to the child js fnFilterLoad function
	String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);	

%>

<%@page import="java.text.NumberFormat"%>
<%@page import="org.apache.commons.lang.NumberUtils"%><HTML>

<HEAD>
<TITLE> Globus Medical: DHR Update </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmGridCommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmDHRUpdate.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>
var companyInfoObj = '<%=strCompanyInfo%>';
var gStrDHRId = "<%=strDHRId%>";
var gStrVendorId = "<%=strVendorId%>";
var gStrInspectedBy = "<%=strInspectedBy%>";
var gStrPackagedBy = "<%=strPackagedBy%>";
var gStrUserId = "<%=strUserId%>";
var gStrMode = "<%=strMode%>";
var gStrServletPath = "<%=strServletPath%>";
var gStrWOId = "<%=strWOId%>";
var gStrDocRev = "<%=strDocRev%>";
var gStrDocActiveFl = "<%=strDocActiveFl%>";
var gStrNCMRId = "<%=strNCMRId%>";
var gStrNCMRFl="<%=strNCMRFl%>";
var gStrDate = "<%=strDate%>";
var hDeptId = "<%=strDeptId%>";
var objGridData = '<%=strXmlData%>';
var qtyPackages = '<%=strQtyPackaged%>';
var gStrBQtyPack='<%=strBQtyPackaged%>';
var gstrRMQtyPack = '<%=strRMQtyPackaged%>';
var gstrPartClass = '<%=strPartClass%>';
var gstrPoType='<%=strPoType%>';
var gstrControlNum = '<%=strControlNum%>';
var lblDHRId = '<fmtDHRUpdate:message key="LBL_DHR_ID"/>';

function fnCheckKey()
{
	if(event.keyCode == 13)
		 {	
		   //alert("Coming In");
		 	var rtVal = fnFetchDHR();
			if(rtVal == false){
				if(!e) var e = window.event;

		        e.cancelBubble = true;
		        e.returnValue = false;

		        if (e.stopPropagation) {
		                e.stopPropagation();
		                e.preventDefault();
		        }
			}
		 }
}
</script> 
</HEAD>
  
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
<FORM name="frmOrder" method="POST">
<input type="hidden" name="hDHRId" value="<%=strDHRId%>">
<input type="hidden" name="hId" value="<%=strDHRId%>"/>
<input type="hidden" name="hMode" />
<input type="hidden" name="hFrom" />
<input type="hidden" name="hAction" value="<%=strMode%>">
<input type="hidden" name="hPartNum" value="<%=strPartNum%>">
<input type="hidden" name="hWOId" value="<%=strWOId%>">
<input type="hidden" name="hRecQty" value="<%=strQtyRec%>">
<input type="hidden" name="hRejQty" value="<%=strQtyReject%>">
<input type="hidden" name="hInputString" value="">
<input type="hidden" name="hSterFl" value="<%=strSterFl%>">
<input type="hidden" name="hPartSterFl" value="<%=strPartSterFl%>">
<input type="hidden" name="hNCMRFl" value="<%=intNCMRFl%>">
<input type="hidden" name="hStatusFl" value="<%=strStatusFl%>">
<input type="hidden" name="hControlNum" value="<%=strControlNum%>">
<input type="hidden" name="RE_FORWARD" value="gmDHRUpdateServlet">
<input type="hidden" name="txnStatus" value="<%=strTxnSts%>">
<input type="hidden" name="RE_TXN" value="<%=strMode%>">
<input type="hidden" name="totalQty" value="<%=strTotal%>">
<input type="hidden" name="hInpStr">
<input type="hidden" name="hpnumLcnStr" value="">

	<table border="0" cellspacing="0" cellpadding="0" class="DtTable700" >
		<tr>
			<td>
				<table border="0" cellspacing="0" cellpadding="0" width="100%">
					<tr>
						<td height="25" class="RightDashBoardHeader"> <fmtDHRUpdate:message key="LBL_UPDATE_HISTORY_RECORD"/></td>
						<td height="25" class="RightDashBoardHeader" align="right"><fmtDHRUpdate:message key="IMG_ALT_HELP" var="varHelp"/>
						<img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>			
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="698" height="5" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
<% 
				if(hAction.equals("Load"))
				{
%>				
				    <tr>
						<td colspan="4">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr class="shade">
									<td class="RightTableCaption" align="right" HEIGHT="30" width="25%"><font color="red">* </font><fmtDHRUpdate:message key="LBL_DHR_ID"/>:</td>
									<td class="RightText" width="24%">&nbsp;<input type="text" size="20" value="<%=strDHRId%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" class="InputArea" name="Txt_DHRID" id="Txt_DHRID" onkeypress="javascript:fnCheckKey();">
									</td>  
									<td> <fmtDHRUpdate:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}" name="Btn_Fetch" gmClass="button" onClick="fnFetchDHR();" buttonType="Load" />&nbsp;
									</td>  
								</tr>
							</table>
						</td>
					</tr>
<% 
				}else{
%>							 	
<%
				if (strCriticalFl.equals("Y")){
%>
					<tr>
						<td colspan="4" class="RightTextRed" align="center" HEIGHT="24">
						<fmtDHRUpdate:message key="LBL_CRITICAL_PROCESS_INFORM_PRODUCT_ENGINEER"/><b></b>  <BR>
						<script>
							alert(message_operations[501]);
						</script>
						</td>
					</tr>
					<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
<%
				}
				if (strFARFl.equals("Y")){
%>
					<tr>
						<td colspan="4" class="RightTextRed" align="center" HEIGHT="24">
						<fmtDHRUpdate:message key="LBL_FIRST_ARTICLE_PROCESS_INFORM_PRODUCT_ENGINEER"/> <b></b> <BR>
						<script>
							alert(message_operations[502]);
						</script>
						</td>
					</tr>
					<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
<%
				}
				if (strBackOrderFl.equals("Y")){
%>
					<tr>
						<td colspan="4" class="RightTextRed" align="center" HEIGHT="24">
						<fmtDHRUpdate:message key="LBL_BACK_ORDER_PROCESS_INFORM_CUSTOM_SERVICE"/> <b></b>  <BR>
						<script>
							alert(message_operations[503]);
						</script>
						</td>
					</tr>
					<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
<%
				}
%>
					<tr>
						<td colspan="4">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr class="shade" width="100%">
									<td class="RightTableCaption" align="right" HEIGHT="30"><font color="red">* </font><fmtDHRUpdate:message key="LBL_DHR_ID"/>:</td>
									<td class="RightText" align="left">
									<table cellspacing="0" cellpadding="0" border="0"><tr><td width="55%">&nbsp;<input type="text" size="20" value="<%=strDHRId%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" class="InputArea" name="Txt_DHRID" onkeypress="javascript:fnCheckKey();"></td>
									    <td>&nbsp;<fmtDHRUpdate:message key="BTN_LOAD" var="varLoad"/><gmjsp:button align="left" value="${varLoad}" name="Btn_Fetch" gmClass="button" onClick="fnFetchDHR();" buttonType="Load" />
									</td></tr></table>
									</td>    
									<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRUpdate:message key="LBL_BIN"/> #:</td>
									<td></td>									
								</tr>
		           				<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>								
								<tr>
									<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRUpdate:message key="LBL_CONTROL_NUMBER"/>:</td>
									<td class="RightText">&nbsp;<%=strControlNum%></td>
									<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRUpdate:message key="LBL_VENDOR"/>:</td>
									<td class="RightText">&nbsp;<%=strVendName%></td>
								</tr>
		           				<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
								<tr class="shade">
									<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRUpdate:message key="LBL_WORK_ORDER"/>:</td>
									<td class="RightText">&nbsp;<%=strWOId%></td>
									<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRUpdate:message key="LBL_UOM_UNITS"/>:</td>
									<td class="RightText">&nbsp;<%=strUOM%></td>
								</tr>								
		           				<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
								<tr colspan="4">
									<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRUpdate:message key="LBL_CREATED_DATE"/>:</td>
									<td class="RightText">&nbsp;<%=strCreatedDate%></td>
									<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRUpdate:message key="LBL_DONOR_#"/>:</td>
									<td class="RightText" colspan="3">&nbsp;<%=strDonorNum%>
									<%if((strProdType.equals("100845") && !strDonorNum.equals("")) || strSerialNumFl.equals("Y")){// 100845: tissue, "D" icon should show only if the part is of tissue type%>
									&nbsp;<fmtDHRUpdate:message key="LBL_DONOR" var="varDonor"/><img align="top" id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/d.gif' title='${varDonor}' onclick="javascript:fnCallDonor('<%=strDHRId%>','<%=strVendorId%>', '<%=strStatusFl%>')" width='14' height='14'/>
									<%} %>
									</td>
								</tr>
							</table>
						 </td>
					</tr>
					<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
					<tr>
						<td class="RightTableCaption" bgcolor="#eeeeee" HEIGHT="18" colspan="4">&nbsp;<fmtDHRUpdate:message key="LBL_BACK_ORDER_DETAILS"/></td>
					</tr>
					<tr>
						<td colspan="4">
						<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
							<jsp:include page="/operations/GmBODHRReportInc.jsp"/>
						</td>
					</tr>
					<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
					<tr bgcolor="#e4e6f2" class="RightTableCaption"><td colspan="4" height="25">&nbsp;<IMG id="tabSubAssmbimg" border=0 src="<%=strImagePath%>/plus.gif">&nbsp;<a href="javascript:fnShowFilters('tabSubAssmb');" tabindex="-1" class="RightText"><fmtDHRUpdate:message key="LBL_SUB_ASSEMBLY_DETAILS"/></a></td></tr>
					<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
					<tr>
						<td colspan="4">
							<table border="0" width="100%" cellspacing="0" cellpadding="0" id="tabSubAssmb" style="display:none">
<%
				if (strSubComFl.equals("1")){
%>
								<tr>
									<td class="RightTableCaption" HEIGHT="18">&nbsp;<fmtDHRUpdate:message key="LBL_SUB_COMP_ID"/></td>
									<td class="RightTableCaption" width="350"><fmtDHRUpdate:message key="LBL_DESCRIPTION"/></td>
									<td class="RightTableCaption"><fmtDHRUpdate:message key="LBL_CONTROL_NUMBER"/></td>
								</tr>
		           				<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
<%
				  		intSize = alSubDHR.size();
						hcboVal = new HashMap();
						String strSubDHRId = "";
						String strSubCompId = "";
						String strSubDesc = "";
						String strControl = "";
						
				  		for (int s=0;s<intSize;s++)
				  		{
				  			hcboVal = (HashMap)alSubDHR.get(s);
				  			strSubDHRId = GmCommonClass.parseNull((String)hcboVal.get("SUBDHRID"));
				  			strSubCompId = GmCommonClass.parseNull((String)hcboVal.get("SUBASMBID"));
				  			strSubDesc = GmCommonClass.parseNull((String)hcboVal.get("PDESC"));
				  			strControl = GmCommonClass.parseNull((String)hcboVal.get("CNUM"));
%>
								<tr>
									<td class="RightText" HEIGHT="18">&nbsp;&nbsp;<%=strSubCompId%></td>
									<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strSubDesc)%></td>
<%		
							if (strMode.equals("C"))
							{
%>
									<td class="RightText">&nbsp;<input name="Txt_CNum<%=s%>" value="<%=strControl%>" type="text" size="9" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" class="InputArea">
									<input type="hidden" name="hSubDHR<%=s%>" value="<%=strSubDHRId%>">
									<input type="hidden" name="hSubComp<%=s%>" value="<%=strSubCompId%>">
									</td>
<%
				  			}else{
%>
									<td class="RightText">&nbsp;<%=strControl%></td>
<%
							}
%>									
								</tr>
<%
				  		}
%>							<input type="hidden" name="hSubCnt" value="<%=intSize%>">
					<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
<%
				}else{
%>
					<tr><td colspan="4" class="RightText" height="30" align="center"><fmtDHRUpdate:message key="LBL_NOT_APPLICABLE"/></td></tr>
<%
				}
%>
							</table>
						</td>
					</tr>					

					<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
					<tr bgcolor="#e4e6f2" class="RightTableCaption"><td colspan="4" height="25">&nbsp;<IMG id="tabRecvimg" border=0 src="<%=strImagePath%>/plus.gif">&nbsp;<a href="javascript:fnShowFilters('tabRecv');" tabindex="-1" class="RightText"><fmtDHRUpdate:message key="LBL_RECEIVING"/></a></td></tr>					
					<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
					<tr>
						<td colspan="4">
							<table border="0" width="100%" cellspacing="0" cellpadding="0" id="tabRecv" style="display:none">
								<tr>
									<td class="RightTableCaption" width="170" align="right" HEIGHT="24"><fmtDHRUpdate:message key="LBL_PO_QTY_SHIPPED"/>:</td>
									<td class="RightText" width="100">&nbsp;<%=strQtyShipped%></td>
									<td class="RightTableCaption" width="250" align="right">&nbsp;<fmtDHRUpdate:message key="LBL_QTY_RECIEVED"/>:</td>
									<td class="RightText" width="170">&nbsp;<%=strQtyRec%></td>
								</tr>
								<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
								<tr>
									<td class="RightTableCaption" align="right">&nbsp;<fmtDHRUpdate:message key="LBL_RECIEVED_BY"/>:</td>
									<td class="RightText" width="300">&nbsp;<%=strReceivedBy%></td>
									<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRUpdate:message key="LBL_RECIEVED_TIMESTAMP"/>:</td>
									<td class="RightText">&nbsp;<%=strReceivedTS%></td>
								</tr>
							</table>
						</td>
					</tr>			
					<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
					<tr bgcolor="<%=strInspBgColor%>" class="RightTableCaption"><td colspan="4" height="25">&nbsp;<IMG id="tabInspimg" border=0 src="<%=strInspImg%>">&nbsp;<a href="javascript:fnShowFilters('tabInsp');" tabindex="-1" class="RightText"><fmtDHRUpdate:message key="LBL_INSPECTION"/></a></td></tr>
					<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
					<tr>
						<td colspan="4">
							<table border="0" width="100%" cellspacing="0" cellpadding="0" id="tabInsp" style="<%=strInspStyle%>">						
<%					
			if (strMode.equals("I"))
			{
%>
							<tr>
								<td class="RightTableCaption" align="right" HEIGHT="24" width="170"><fmtDHRUpdate:message key="LBL_QTY_INSPECTED"/>:</td>
								<td class="RightText" width="100">&nbsp;<input type="text" size="4" value="<%=strQtyRecd%>" 
								onFocus="changeBgColor(this,'#AACCE8');"  
								onBlur="changeBgColor(this,'#ffffff');" class="InputArea" name="Txt_QtyInsp"></td>
								<td class="RightTableCaption" align="right" width="250">&nbsp;<fmtDHRUpdate:message key="LBL_INSPECTED_BY"/>:</td>
								<td class="RightText" width="170">&nbsp;
									<input type="hidden" name="Cbo_InspEmp" value="<%=strUserId%>">
<%			  		intSize = alEmpList.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alEmpList.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
			  			if (strUserId.equals(strCodeID))
			  			{
			  				out.println(hcboVal.get("NAME"));
			  				break;	
			  			}
			  		}
%>				
								</td>
							</tr>	
<%		
			}else{


				if (strStatusErrFl.equals("IDashboardDHR"))
				{
%>
							<tr>
								<td class="RightTextRed" align="center" HEIGHT="24" colspan="4"> <%=strSessErrMsg%> </td>
							</tr>
<%
				}
%>							<tr>	
								<td class="RightTableCaption" align="right" HEIGHT="24" width="170"><fmtDHRUpdate:message key="LBL_QTY_INSPECTED"/>:</td>
								<td class="RightText" width="100">&nbsp;<%=strQtyInspected%></td>
								<td class="RightTableCaption" align="right" width="250">&nbsp;<fmtDHRUpdate:message key="LBL_INSPECTED_BY"/>:</td>
								<td class="RightText" width="170">&nbsp;<%=strInspectedByNm%></td>
							</tr>	
<%		
			}
%>
				           	<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
							<tr colspan = "4">
								<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRUpdate:message key="LBL_INSPECTED_TIMESTAMP"/>:</td>
								<td class="RightText" width = "150" >&nbsp;<%=strInspectedTS%></td>
								<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRUpdate:message key="LBL_ETCH_VERIFIED_PER_WO"/>:</td>
								<td class="RightText" >&nbsp;<input type="checkbox" <%=strUdiChecked%> name="Chk_UDI"></td>
							</tr>
				           	<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
							<tr>
								<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRUpdate:message key="LBL_QTY_REJECTED"/>:</td>
<%		
			if (strMode.equals("I") || !strNCMRFl.equals("3"))
			{
%>
								<td class="RightText">&nbsp;<input type="text" value="<%=strQtyReject%>" size="4" 
								onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" 
								class="InputArea"  name="Txt_QtyRej"></td>
<%		
			}else{
%>
								<td class="RightText">&nbsp;<%=strQtyReject%></td>
<%		
			}
%>
								<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRUpdate:message key="LBL_RAISED_NCMR_ID"/>:</td>
								<td class="RightText">&nbsp;<%=strNCMRId.startsWith("GM-NCMR")?strNCMRId:""%></td>
							</tr>
				           	<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
				           	<tr colspan = "4">
							<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRUpdate:message key="LBL_REJECTION_TYPE"/>:</td>
<%		
			if (strMode.equals("I")){
				
%>							<td class="RightText"> &nbsp;<select name="Cbo_ReasonType" id="Region" class="RightText" >
								<option value="0" >[Choose One]
<%			  		intSize = alReturnReasons.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alReturnReasons.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strReasonTypeID.equals(strCodeID)?"selected":"";
%>								<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%			  		}
%>							</select></td>
							
<%		
			}else{
%>
							<td class="RightText">&nbsp;<%=strReasonType%></td>
							
<%		
			}
%>
							<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRUpdate:message key="LBL_RAISED_NCMR_EVALUATION_ID"/>:</td>
								<td class="RightText">&nbsp;<%=strNCMRId.startsWith("GM-EVAL")?strNCMRId:""%></td>
							</tr>
				           	<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
							<tr colspan = "4">
								<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRUpdate:message key="LBL_REASON_FOR_REJECTION"/>:</td>
<%		
			if (strMode.equals("I")){
%>	
								<td class="RightText" colspan="3">&nbsp;<textarea name="Txt_RejReason" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
								onBlur="changeBgColor(this,'#ffffff');" rows=4 cols=45 value="" ><%=strRejReason%></textarea></td>
<%		
			}else{
%>
								<td class="RightText" colspan="3">&nbsp;<%=strRejReason%></td>
<%		
			}
%>
							</tr>
				           	<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
<%		
			if (intNCMRFl > 0 && intNCMRFl < 2 ){
				
				if (!strQtyReject.equals("0"))
				{
					strDisabled = "true";
				}	
%>
							<tr>
								<td class="RightTextRed" align="center" HEIGHT="24" colspan="4"><fmtDHRUpdate:message key="LBL_NCMR_CLOSED_CONTACT_QA_DIRECTOR"/><br></td>
							</tr>
<%
}
%>
							<tr>
								<td class="RightTableCaption" colspan="4" HEIGHT="24">&nbsp;&nbsp;<u><fmtDHRUpdate:message key="LBL_NCMR_DISPOSITION_DETAILS"/></u></td>
							</tr>
							<tr>
								<td class="RightTableCaption" align="right" HEIGHT="22"><fmtDHRUpdate:message key="LBL_ACTION_TAKEN"/>:</td>
								<td class="RightText">&nbsp;<%=strNCMRAction%></td>
								<td class="RightTableCaption" align="right" valign="top" HEIGHT="22"><fmtDHRUpdate:message key="LBL_REMARKS"/>:</td>
								<td class="RightText">&nbsp;<%=strNCMRRemarks%></td>						
							</tr>
				            <tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
							<tr>
								<td class="RightTableCaption" align="right" HEIGHT="22"><fmtDHRUpdate:message key="LBL_AUTHORIZED_BY"/>:</td>
								<td class="RightText">&nbsp;<%=strNCMRAuthBy%></td>
								<td class="RightTableCaption" align="right" HEIGHT="22"><fmtDHRUpdate:message key="LBL_DATE"/>:</td>
								<td class="RightText">&nbsp;<%=strNCMRAuthDt%></td>
							</tr>
							<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
							<tr colspan="4">
								<td class="RightTableCaption" align="right" HEIGHT="22"><fmtDHRUpdate:message key="LBL_NCMR_CLOSEOUT_QTY"/>:</td>
								<td class="RightText">&nbsp;<%=strCloseQty%>
								<input type="hidden" name="hCloseQty" value="<%=strCloseQty%>"></td>
								<td></td>
								<td></td>						
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
				<tr bgcolor="<%=strPackBgColor%>" class="RightTableCaption"><td colspan="4" height="25">&nbsp;<IMG id="tabPkgimg" border=0 src="<%=strPackImg%>">&nbsp;<a href="javascript:fnShowFilters('tabPkg');" tabindex="-1" class="RightText"><fmtDHRUpdate:message key="LBL_PROCESSING"/></a></td></tr>
				<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
				<tr>
					<td colspan="4">
						<table border="0" cellspacing="0" cellpadding="0" id="tabPkg" style="<%=strPackStyle%>">
										
<%
			if (strMode.equals("P"))
			{
%>
							<tr>
							<td colspan="8"><div id="dataGridDiv" class="grid" style="height:155px;"></div></td>
							
							</tr>
                            <tr>
								<td class="RightTableCaption" align="right" HEIGHT="24" width="200"><fmtDHRUpdate:message key="LBL_SKIP_LABELING"/> ?:</td>
								<td class="RightText"  HEIGHT="24" width="100">&nbsp;<input type="checkbox" <%=strChecked%> name="Chk_Skip"></td>
									
								<td class="RightTableCaption" width="200" align="right"><fmtDHRUpdate:message key="LBL_PROCESSED_BY"/>:</td>
								<td class="RightText" width="170">
								<input type="hidden" name="Cbo_Emp" value="<%=strUserId%>">
<%			  	
					intSize = alEmpList.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alEmpList.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
			  			if (strUserId.equals(strCodeID))
			  			{
			  				out.println(hcboVal.get("NAME"));
			  				break;	
			  			}
			  		}
%>								</td>
							</tr>
							<%
							//}
			}else{
%>
<%
					if (strStatusErrFl.equals("PDashboardDHR"))
					{
%>		
							<tr>
								<td class="RightTextRed" align="center" HEIGHT="24" colspan="4"> <%=strSessErrMsg%> </td>
							</tr>
<%
					}
%>							<tr>
							<td colspan="8"><div id="dataGridDiv" class="grid" style="height : 155px;"></div></td>
							</tr>
							<tr>							
								<td class="RightTableCaption" align="right" width="11%"><fmtDHRUpdate:message key="LBL_PROCESSED_BY"/>:</td>
								<td class="RightText" width="19%">&nbsp;<%=strPackagedByNm%></td>
								<td class="RightTableCaption" align="right" HEIGHT="24" width="15%"><fmtDHRUpdate:message key="LBL_SKIP_LABELING"/> ?:</td>
								<td class="RightText" HEIGHT="24" width="14%">&nbsp;<%=strLabelFl%></td><%-- 
								<td class="RightTableCaption" align="right" HEIGHT="15" width="14%">Skip Processing?:</td>
								<td class="RightText" HEIGHT="24" width="16%">&nbsp;<%=strPackFl%></td>--%>
							</tr>
<% 
}
%>
<% if (strCountryCode.equals("en")){ %>
						 <tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
							<tr colspan="4">
								<td class="RightTableCaption" align="right" ><fmtDHRUpdate:message key="LBL_PRIMARY_PACKAGING"/>:</td>
								<td class="RightText">&nbsp;<%=strPrimaryPack%></td>
								<td class="RightTableCaption" align="right" ><fmtDHRUpdate:message key="LBL_SECONDARY_PACKAGING"/>:</td>
								<td class="RightText">&nbsp;<%=strSecondaryPack%></td>						
						    </tr>
						    <tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
							<tr colspan="4">
								<td class="RightTableCaption" align="right" ><fmtDHRUpdate:message key="LBL_INSERT_ID"/>:</td>
								<td class="RightText">&nbsp;<%=strInsertID%></td>
								<td class="RightTableCaption" align="right" ></td>
								<td class="RightText">&nbsp;</td>						
						    </tr>
<%} %>
						</table>
					</td>
				</tr>
				<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
				<tr bgcolor="<%=strVerBgColor%>" class="RightTableCaption"><td colspan="4" height="25">&nbsp;<IMG id="tabVerimg" border=0 src="<%=strVerImg%>">&nbsp;<a href="javascript:fnShowFilters('tabVer');" tabindex="-1" class="RightText"><fmtDHRUpdate:message key="LBL_VERIFICATION"/></a></td></tr>
				<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
				<tr>
					<td colspan="4">
						<table border="0" width="100%" cellspacing="0" cellpadding="0" id="tabVer" style="<%=strVerStyle%>">
						
<%		
			if (strMode.equals("V"))
			{
%>
							<tr>
								<td class="RightTableCaption" align="right" HEIGHT="24" width="170"> <fmtDHRUpdate:message key="LBL_QTY_TO_VERIFY"/>:</td>
								<td class="RightText" width="100">&nbsp;<input type="text" size="4" value="<%=strQtyInShelf%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" class="InputArea" name="Txt_Qty"></td>
								<td class="RightTableCaption" align="right" width="250"><fmtDHRUpdate:message key="LBL_QTY_TO_VERIFIED_BY"/>:</td>
								<td class="RightText" width="170">&nbsp;
								<input type="hidden" name="Cbo_Emp" value="<%=strUserId%>" >
<%
					
			  		intSize = alEmpList.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alEmpList.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
			  			if (strUserId.equals(strCodeID))
			  			{
			  				out.println(hcboVal.get("NAME"));
			  				break;	
			  			}
			  		}
%>
								</td>
							</tr>
						
<%		
			}else{

				if (strStatusErrFl.equals("PDashboardDHR"))
				{
%>		
							<tr>
								<td class="RightTextRed" align="center" HEIGHT="24" colspan="4"> <%=strSessErrMsg%> </td>
							</tr>
<%
				}
%>
							<tr>
								<td class="RightTableCaption" align="right" HEIGHT="24" width="170"> <fmtDHRUpdate:message key="LBL_QTY_VERIFIED"/>:</td>
								<td class="RightText" width="100">&nbsp;<%=strQtyInShelf%></td>
								<td class="RightTableCaption" align="right" width="250"> <fmtDHRUpdate:message key="LBL_VERIFIED_BY"/>:</td>
								<td class="RightText" width="170">&nbsp;<%=strVerifiedByNm%></td>
							</tr>
							<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
							<tr colspan="4">
								<td class="RightTableCaption" align="right" HEIGHT="24" width="200"> <fmtDHRUpdate:message key="LBL_VERIFIED_TIMESTAMP"/>:</td>
								<td class="RightText" width="175">&nbsp;<%=strVerifiedTS%></td>
								<td width="100">&nbsp;</td>
								<td width="200">&nbsp;</td>
							</tr>								
<%		
			}
%>
						</table>
					</td>
				</tr>		
<%						
	}
%>
			</table>
		</td>
	</tr>
	
	<tr>
		<td>
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
				<jsp:include page="/common/GmRuleDisplayInclude.jsp"/>
		</td>
	</tr>
  </table>


<%
	if(!hAction.equals("Load")){
%>
	<table border="0" cellspacing="0" cellpadding="0" class="DtTable700">
		<tr>
			<td align="center" height="30" id="button">
<%		if (strMode.equals("Load"))
		{
%>			<fmtDHRUpdate:message key="BTN_RETURN" var="varReturn"/>
            <gmjsp:button value="&nbsp;${varReturn}&nbsp;" name="Btn_Return" gmClass="button" buttonType="Load" onClick="history.go(-1)" />&nbsp;&nbsp;
<%
		}else if (strMode.equals("C") || strMode.equals("I") || strMode.equals("P") || strMode.equals("V")|| strMode.equals("E")){
			if (((strStatusErrFl.substring(0,1)).equals("I") && !strNCMRId.equals(""))){
				if(strNCMRId.startsWith("GM-EVAL")){
					%>			
					<fmtDHRUpdate:message key="BTN_PRINT_EVAL" var="varPrintEVAL"/>
					<gmjsp:button value="${varPrintEVAL}" name="Btn_ViewPrint" gmClass="button"  buttonType="Load" onClick="fnViewPrintEVAL();" />&nbsp;&nbsp;
				<%}else{ %>
					<fmtDHRUpdate:message key="BTN_PRINT_NCMR" var="varPrintNCMR"/>
					<gmjsp:button value="${varPrintNCMR}" name="Btn_ViewPrint" gmClass="button"  buttonType="Load" onClick="fnViewPrintNCMR();" />&nbsp;&nbsp;
				<%} %>
<%			}else{
			if((strNCMRId.startsWith("GM-EVAL")) && (intNCMRFl==0)){
				strDisabled = "true";
			}
%>	
<% if (strCountryCode.equals("en")){ %>
			<fmtDHRUpdate:message key="BTN_UPDATE" var="varUpdate"/>
			<gmjsp:button value=" ${varUpdate}" name="Btn_Update" disabled="<%=strDisabled%>" gmClass="button" buttonType="Save" onClick="fnUpdate();" />&nbsp;&nbsp;
			<%}else{ %>
			<fmtDHRUpdate:message key="BTN_SUBMIT" var="varSubmit"/>
			<gmjsp:button value="${varSubmit}" name="Btn_Update" disabled="<%=strDisabled%>" gmClass="button" buttonType="Save" onClick="fnUpdate();" />&nbsp;&nbsp;
			<%} %>
<%				
		}		
	}
%>
<% if (strCountryCode.equals("en")){ %>
				<fmtDHRUpdate:message key="BTN_PRINT" var="varPrint"/>
				<gmjsp:button value="${varPrint}" name="Btn_ViewPrint" gmClass="button"  buttonType="Load" onClick="fnViewPrint();" />&nbsp;&nbsp;
				<%} %>
				<%
				if (strFARFl.equals("Y")){
                %>
				<fmtDHRUpdate:message key="BTN_PRINT_FA" var="varPrintFA"/>
				<gmjsp:button value="${varPrintFA}" name="Btn_FAPrint" gmClass="button" buttonType="Load" onClick="fnViewPrintFAR();" />&nbsp;&nbsp;
				<% }
					if(!strVerifiedTS.equals("")){
				%>
				<fmtDHRUpdate:message key="BTN_PRINT_ALL" var="varPrintAll"/>
				<gmjsp:button value="${varPrintAll}" name="Btn_ViewPrintAll" gmClass="button" buttonType="Load" onClick="fnViewPrintAll();" />&nbsp;&nbsp;
<%} %>
	<%if(strMode.equals("V") && ((strProdType.equals("100845") && !strDonorNum.equals("")) || strSerialNumFl.equals("Y")) && !strQtyRecd.equals("0")){ // The button should show only once the processing is complete and only for tissue type part
	// Button should not be shown if all the quantities are rejected 
	%>			
			<fmtDHRUpdate:message key="BTN_ASSIGN_LOT" var="varAssignLot"/>
			<gmjsp:button value="${varAssignLot}" name="Btn_AssignLotNum" gmClass="button" buttonType="Load" onClick="fnAssignLotNum();" />&nbsp;&nbsp;
	<%} %>
			</td>
		</tr>
	</table>
<%
    } 
%>	
</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
