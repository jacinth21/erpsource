 <%
/**********************************************************************************
 * File		 		: GmNCMRList.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Richardk
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %>
<%@ page import="java.util.Date"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- operations\GmNCMRList.jsp -->
<%@ taglib prefix="fmtNCMRList" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtNCMRList:setLocale value="<%=strLocale%>"/>
<fmtNCMRList:setBundle basename="properties.labels.operations.GmNCMRList"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	int intSize = 0;
	HashMap hmLoop = new HashMap();
	HashMap hcboVal = null;

	String strCodeID = "";
	String strDistId = "";
	String strSelected = "";
	String strVendorId = "";

	String strNCMRId 		= "";
	String strNCMRType      = "";
	String strNCMRDT 		= "";
	String strPartNum 		= "";
	String strPartName 		= "";
	String strControlNum 	= "";
	String strWorkOrder 	= "";
	String strQtyRej 		= "";
	String strCloseQty 		= "";
	
	String strOrdId = "";
	String strAction = "";
	String strOrderType = "";
	
/* 	String strOrdId = "";
	String strFormattedOrderId = "";
	String strOrdDt = "";
	String strPrice = "";
	String strOrdBy = "";
	String strAction = "";
	String strItems = "";
	String strStatusFl = "";
	String strTrackNum = "";
	String strPO = "";
	String strInvId = "";
	String strShipToDate = "";
	String strOrderType = ""; */
	String strFrmDate = (String)request.getAttribute("hFrom")==null?"":(String)request.getAttribute("hFrom");
	String strToDate = (String)request.getAttribute("hTo")==null?"":(String)request.getAttribute("hTo");
	String strApplDateFmt = strGCompDateFmt;
	
	Date dtFrmDate = null;
	Date dtToDate = null;
	
	dtFrmDate = (Date)GmCommonClass.getStringToDate(strFrmDate,strApplDateFmt);
	dtToDate = (Date)GmCommonClass.getStringToDate(strToDate,strApplDateFmt);
	
	String strShade = "";
	String strProjId = "";
	String strParam = (String)request.getAttribute("hMode");
	String strApplnDateFmt = strGCompDateFmt;
	ArrayList alReturn = new ArrayList();
	ArrayList alVendor = new ArrayList();
	ArrayList alNCMRLIST = new ArrayList();

	int intLength = 0;

	HashMap hmReturn = new HashMap();
	
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	alVendor = (ArrayList)hmReturn.get("VENDORLIST");
	
	if (strParam.equals("Reload"))
	{	strVendorId = (String)request.getAttribute("hVendorID");
		alNCMRLIST = (ArrayList)hmReturn.get("NCMRLIST");
		
		intLength = alNCMRLIST.size();
		strAction = (String)request.getAttribute("hMode"); 
	}

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: NCMR  EDIT</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script>
var gCmpDateFmt = "<%=strGCompDateFmt%>";
function fnGo()
{
	var objFromDt = document.frmAccount.Txt_FromDate;
	var objToDt = document.frmAccount.Txt_ToDate;
	CommonDateValidation(objFromDt, gCmpDateFmt,Error_Details_Trans(message_operations[505],gCmpDateFmt));
	CommonDateValidation(objToDt, gCmpDateFmt,Error_Details_Trans(message_operations[506],gCmpDateFmt));
	var dateRange = dateDiff(objFromDt.value,objToDt.value,gCmpDateFmt);
	if(!isNaN(dateRange) && dateRange < 0){
		 Error_Details(message_operations[504]);
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmAccount.hMode.value = "Reload";
	fnStartProgress('Y');
	document.frmAccount.submit();
}

function fnLoad()
{
	var obj = document.frmAccount.Cbo_Action;
	/* if (document.frmAccount.hAction.value != "")
	{
		for (var i=0;i<obj.length ;i++ )
		{
			if (document.frmAccount.hAction.value == obj.options[i].value)
			{
				obj.options[i].selected = true;
				break;
			}
		}
	}*/

}

function fnCallEdit()
{
	Error_Clear();
	var len = document.frmAccount.hLen.value;
	var cnt = 0;
	var sfl = "";
	var obj = "";
	var objOrder = "";
	var orderType = "";
    var ncmrType = "";
    
	if ( len == 1)
	{
		document.frmAccount.hNCMRId.value = document.frmAccount.rad.value;
		ncmrType = document.frmAccount.rad.id;
	}
	else if ( len >1)
	{
		var arr = document.frmAccount.rad;
		for (var i=0;i<arr.length ;i++ )
		{
			if (arr[i].checked == true)
			{				
				document.frmAccount.hNCMRId.value = arr[i].value;
				ncmrType = arr[i].id;								
				cnt++;
				break;
			}

		}
		if (cnt == 0)
		{
			alert(message_operations[507]);
			return;
		}
	}

	// Called when Edit Action is selected
	if (document.frmAccount.Cbo_Action1.value == "EA")
	{
		if (ncmrType != '51062')
		{
			Error_Details(message_operations[508]);
		}
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
		document.frmAccount.hMode.value = 'A';
		document.frmAccount.hAction.value = 'UpdateNCMR';
		document.frmAccount.action = "GmNCMRServlet";
		fnStartProgress('Y');
		document.frmAccount.submit();
	}

	// Called when Edit Action is selected
	if (document.frmAccount.Cbo_Action1.value == "EC")
	{
		if (ncmrType != '51062')
		{
			Error_Details(message_operations[508]);
		}
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
		document.frmAccount.hMode.value = 'C';
		document.frmAccount.hAction.value = 'UpdateNCMR';
		document.frmAccount.action = "GmNCMRServlet";
		fnStartProgress('Y');
		document.frmAccount.submit();
	}
	if(document.frmAccount.Cbo_Action1.value == "ER")
	{
		document.frmAccount.hMode.value = 'Q';
		document.frmAccount.hAction.value = 'UpdateNCMR';
		document.frmAccount.action = "GmNCMRServlet";
		fnStartProgress('Y');
		document.frmAccount.submit();
	}

}

function fnViewDetails(hNCMRID)
{
	windowOpener("/GmNCMRServlet?hAction=P&hNCMRId="+hNCMRID,"INVPOP","resizable=yes,scrollbars=yes,top=200,left=150,width=800,height=535");
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnLoad();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmNCMRUpdateServlet">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hMode" value="">
<input type="hidden" name="hNCMRId" value="">
<input type="hidden" name="hNCMRLIST" value="YES">

<input type="hidden" name="hLen" value="<%=intLength%>">

	<table border="0" width="900" cellspacing="0" cellpadding="0">
		<tr >
			<td rowspan="10" width="1" class="Line"></td>
			<td bgcolor="#666666" height="1"></td>
			<td bgcolor="#666666"  ></td>
			<td bgcolor="#666666" ></td>
			
			<td rowspan="10" width="1" class="Line"></td>
		</tr> 
		<tr>
			<td height="25"class="RightDashBoardHeader"><fmtNCMRList:message key="LBL_NCMR_VENDOR"/></td>
			<td  class="RightDashBoardHeader"></td>
			<td class="RightDashBoardHeader"></td>
		</tr>
		<tr>
			<td class="RightTextBlue" >&nbsp;<fmtNCMRList:message key="LBL_CHOOSE_VENDOR"/> <BR><BR>
			</td>
			<td class="RightText"></td>
			<td class="RightText"></td>
		</tr>
		<tr>
			<td class="RightText" width="45%" ><fmtNCMRList:message key="LBL_VENDOR_NAME"/>:&nbsp;
			 <select name="Cbo_VendorId" id="Region" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" 
			onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtNCMRList:message key="LBL_CHOOSE"/>
<%
					intSize = alVendor.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alVendor.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
						strSelected = strVendorId.equals(strCodeID)?"selected":"";
%>					<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
<%			  		}
%>
						</select>
			</td>
			<td class="RightText" width="20%"> </td>
			<td></td>
		</tr>
		<tr>
			<td bgcolor="#666666" height="1"></td>
			<td  bgcolor="#666666" ></td>			
			<td  bgcolor="#666666"></td>
		</tr>
		<tr>
			<td height="30" class="RightText" >&nbsp;&nbsp;&nbsp;&nbsp;<fmtNCMRList:message key="LBL_FROM_DT"/>:&nbsp;&nbsp;<gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=dtFrmDate==null?null:new java.sql.Date(dtFrmDate.getTime())%>" 
				gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="1"/></td>
			<td height="30"  class="RightText"><fmtNCMRList:message key="LBL_TO_DT"/>:&nbsp;<gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=dtToDate==null?null:new java.sql.Date(dtToDate.getTime())%>"
				 gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="2"/></td>
				 <fmtNCMRList:message key="LBL_GO" var="varGo"/>
			<td><gmjsp:button value="&nbsp;${varGo}&nbsp;" gmClass="button" onClick="fnGo();" tabindex="25" buttonType="Load" />&nbsp;
			</td>
		</tr>	
		<tr>
			<td bgcolor="#666666" height="1"></td>
			<td  bgcolor="#666666" ></td>			
			<td  bgcolor="#666666"></td>
		</tr>
    </table>
<%
	if (strParam.equals("Reload"))
	{
%>
	<table border="0" width="900" cellspacing="0" cellpadding="0">
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td width="900" height="100" valign="top">
<%
	if (intLength > 0)
	{
%>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" align="left" width="120"><fmtNCMRList:message key="LBL_NCMR_ID"/></td>
						<td align="center"><fmtNCMRList:message key="LBL_DATE"/></td>
						<td align="center"><fmtNCMRList:message key="LBL_PART#"/></td>
						<td align="center" width="400" ><fmtNCMRList:message key="LBL_PART_NAME"/></td>
						<td align="center"><fmtNCMRList:message key="LBL_CH#"/></td>
						<td align="center"><fmtNCMRList:message key="LBL_WO_ID"/> </td>
						<td align="center"><fmtNCMRList:message key="LBL_R_QTY"/>.<BR></td>
						<td align="center"><fmtNCMRList:message key="LBL_C_QTY"/>.<BR></td>
					</tr>
					<tr><td class="Line" colspan="8"></td></tr>
<%						for (int i = 0;i < intLength ;i++ )
						{
							hmLoop = (HashMap)alNCMRLIST.get(i);
						
							strNCMRId = GmCommonClass.parseNull((String)hmLoop.get("NCMRID"));
							//strNCMRDT = GmCommonClass.parseNull((String)hmLoop.get("CDATE"));
							strNCMRDT = GmCommonClass.getStringFromDate((java.util.Date)hmLoop.get("CDATE"),strApplnDateFmt);
							strPartNum = (String)hmLoop.get("PNUM");
							strPartName = (String)hmLoop.get("PDESC");
							strControlNum = (String)hmLoop.get("CNUM");
							strWorkOrder = (String)hmLoop.get("WOID");
							strQtyRej = (String)hmLoop.get("QTYREJ");
							strCloseQty = (String)hmLoop.get("CLOSEQTY");
							strNCMRType = GmCommonClass.parseNull((String)hmLoop.get("TYPE"));
							
							strShade	= (i%2 == 0)?"class=Shade":""; //For alternate Shading of rows
%>					<tr <%=strShade%>>
						<td height="20" class="RightText"><input type="radio" value="<%=strNCMRId%>" id="<%=strNCMRType%>"
								name="rad">&nbsp;
						<a href="javascript:fnViewDetails('<%=strNCMRId%>');" class="RightText"><%=strNCMRId%></a>
						<td class="RightText">&nbsp;<%=strNCMRDT%></td>
						<td class="RightText" align="right"><%=strPartNum%>&nbsp;</td>
						<td class="RightText" align="left"><%=strPartName%>&nbsp;</td>
						<td class="RightText" align="left"><%=strControlNum%>&nbsp;</td>
						<td class="RightText" align="left"><%=strWorkOrder%>&nbsp;</td>
						<td class="RightText" align="left"><%=strQtyRej%>&nbsp;</td>
						<td class="RightText" align="left"><%=strCloseQty%>&nbsp;</td>
					</tr>
<%						}%>
					<tr>
						<td colspan="8" class="RightText" align="Center"><BR><fmtNCMRList:message key="LBL_CHOOSE_ACT"/>:&nbsp;
						<select name="Cbo_Action1" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" 
							onBlur="changeBgColor(this,'#ffffff');">
							<option value="0" ><fmtNCMRList:message key="LBL_CHOOSE"/></option>
							<option value="EA"><fmtNCMRList:message key="LBL_ACT_INFO"/></option>
							<option value="EC"><fmtNCMRList:message key="LBL_CLO_INFO"/></option>
							<option value="ER"><fmtNCMRList:message key="LBL_REJ_INFO"/></option>
						</select>
						<fmtNCMRList:message key="BTN_SUBMIT" var="varSubmit"/>
						<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="fnCallEdit();" 
						tabindex="25" buttonType="Save" />&nbsp;<BR><BR>
						</td>
					</tr>
				</table>
<%
	}else{
%>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" align="center" width="100"><fmtNCMRList:message key="LBL_NCMR_ID"/></td>
						<td align="center"><fmtNCMRList:message key="LBL_DATE"/></td>
						<td align="center"><fmtNCMRList:message key="LBL_PART#"/></td>
						<td align="center"><fmtNCMRList:message key="LBL_PART_NAME"/></td>
						<td align="center"><fmtNCMRList:message key="LBL_CH#"/></td>
						<td align="center"><fmtNCMRList:message key="LBL_WO_ID"/> </td>
						<td align="center"><fmtNCMRList:message key="LBL_R_QTY"/>.<BR></td>
						<td align="center"><fmtNCMRList:message key="LBL_C_QTY"/>.<BR></td>
					</tr>
					<tr><td class="Line" colspan="8"></td></tr>					
					<tr><td colspan="8" class="RightTextRed" align="center"> <BR><fmtNCMRList:message key="LBL_NO_NCMR"/></td></tr>
				</table>
<%
		}
%>
			</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>
<%
	}
%>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>

</BODY>

</HTML>
