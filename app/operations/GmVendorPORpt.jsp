<!-- \operations\GmVendorPORpt.jsp -->
 
 <%
/**********************************************************************************
 * File		 		: GmVendorPORpt.jsp
 * Desc		 		: This screen is used to show the Vendor PO Report
 * Version	 		: 1.0
 * author			: 
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@page import="java.util.Date"%>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<!-- WEB-INF path corrected for JBOSS migration changes -->


<%@ taglib prefix="fmtPOReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPOReport:setLocale value="<%=strLocale%>"/>
<fmtPOReport:setBundle basename="properties.labels.operations.GmPOReport"/>

<bean:define id="vendid" name="frmVendorPOReport" property="vendid" scope="request" type="java.lang.String"></bean:define>
<bean:define id="poid" name="frmVendorPOReport" property="poid" scope="request" type="java.lang.String"></bean:define>
<bean:define id="frmdt" name="frmVendorPOReport" property="frmdt" scope="request" type="java.lang.String"></bean:define>
<bean:define id="todt" name="frmVendorPOReport" property="todt" scope="request" type="java.lang.String"></bean:define>
<bean:define id="potype" name="frmVendorPOReport" property="potype" scope="request" type="java.lang.String"></bean:define>
<bean:define id="empid" name="frmVendorPOReport" property="empid" scope="request" type="java.lang.String"></bean:define>
<bean:define id="status" name="frmVendorPOReport" property="status" scope="request" type="java.lang.String"></bean:define>
<bean:define id="cbo_Action" name="frmVendorPOReport" property="cbo_Action" scope="request" type="java.lang.String"></bean:define>
<bean:define id="poamt" name="frmVendorPOReport" property="poamt" scope="request" type="java.lang.String"></bean:define>
<bean:define id="strGridXmlData" name="frmVendorPOReport" property="strGridXmlData" scope="request" type="java.lang.String"></bean:define>
<bean:define id="strOpt" name="frmVendorPOReport" property="strOpt" scope="request" type="java.lang.String"></bean:define>
<bean:define id="strHoldFL" name="frmVendorPOReport" property="strHoldFL" scope="request" type="java.lang.String"></bean:define>

<bean:define id="alVendorList" name="frmVendorPOReport" property="alVendorList" scope="request" type="java.util.ArrayList"></bean:define>
<bean:define id="alUsers" name="frmVendorPOReport" property="alUsers" scope="request" type="java.util.ArrayList"></bean:define>
<bean:define id="alTypes" name="frmVendorPOReport" property="alTypes" scope="request" type="java.util.ArrayList"></bean:define>
<bean:define id="alPOAction" name="frmVendorPOReport" property="alPOAction" scope="request" type="java.util.ArrayList"></bean:define>
<bean:define id="alProject" name="frmVendorPOReport" property="alProject" scope="request" type="java.util.ArrayList"></bean:define>
<bean:define id="alPODetails" name="frmVendorPOReport" property="alPODetails" scope="request" type="java.util.ArrayList"></bean:define>
<bean:define id="alStatus" name="frmVendorPOReport" property="alStatus" scope="request" type="java.util.ArrayList"></bean:define>

<% 
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	// Based on application date format we need to pick date from calendar
	String strApplDateFmt = strGCompDateFmt;
	String strWikiTitle = GmCommonClass.getWikiTitle("VENDOR_PORTAL_PO_REPORT");
	String stroperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	Date dtfromdate=GmCommonClass.getStringToDate(frmdt,strApplDateFmt);
	Date dttodate=GmCommonClass.getStringToDate(todt,strApplDateFmt);

	int intSize = 0;
	HashMap hcboVal = null;
	double dbTotal = 0.0;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: PO Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/OrderPlan.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=stroperationsJsPath%>/GmVendorPOReport.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDashboard.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script>
 var objGridData;
objGridData ='<%=strGridXmlData%>';
var gStrServletPath = "<%=strServletPath%>";
var gCmpDateFmt = "<%=strGCompDateFmt%>";

</script>


</HEAD>

<BODY leftmargin="20" topmargin="10" onload="javascript:fnOnPageLoad();">
<FORM name="frmVendorPOReport" method="POST" action="/gmVendorPOReport.do?method=loadVendorPOReport">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hOpt" value="">
<INPUT type="hidden" name="hpostring" value="">
<input type="hidden" name="hPOId" value=""> 
<input type="hidden" name="hVenId" value="">
<input type="hidden" name="htype" value="">
<input type="hidden" name="hmode" value="">
<input type="hidden" name="hTxnId" value="">
<input type="hidden" name="hCancelType" value="">
<input type="hidden" name="strAction" value="">
<input type="hidden" name="strOpt" value="<%=strOpt%>">
<input type="hidden" name="strHoldFL" value="<%=strHoldFL%>">

<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0" >
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtPOReport:message key="LBL_VENDOR_PO_REPORT"/></td>
			<td align="right" class=RightDashBoardHeader ><fmtPOReport:message key="IMG_ALT_HELP" var = "varHelp"/> 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />&nbsp;
				</td>
		</tr>
		<tr>
			<td width="1000" height="80" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr >
						<td class="RightTableCaption" align="right"  width="130" HEIGHT="30">&nbsp;<fmtPOReport:message key="LBL_VENDOR_NAME"/>:&nbsp;</td>
						<td width="500">
						<gmjsp:dropdown controlName="vendid"  seletedValue="<%=vendid%>"  defaultValue= "[All Vendors]"	
							tabIndex="1"  value="<%=alVendorList%>" codeId="ID" codeName="NAME" width="350"/>
						</td>
						<td  class="RightTableCaption" align="right"  width="150">&nbsp;<fmtPOReport:message key="LBL_PO_ID"/>:&nbsp;</td>
						<td width="220"><input type="text" size="20" value="<%=poid%>" name="poid" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;</td>
					 
					</tr>
					 
					
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr  class = "Shade" >
						<td class="RightTableCaption" align="right"  HEIGHT="24"   width="130">&nbsp;<fmtPOReport:message key="LBL_FROM_DATE"/>:&nbsp;</td>
						<td class="RightTableCaption" width="500">
						<table border="0" cellspacing="0" cellpadding="0">
						<tr>
						<td><gmjsp:calendar textControlName="frmdt" gmClass="InputArea" textValue="<%=dtfromdate==null?null:new java.sql.Date(dtfromdate.getTime())%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/></td>
						<td class="RightTableCaption">&nbsp;&nbsp;<fmtPOReport:message key="LBL_TO_DATE"/>&nbsp;</td>
						<td><gmjsp:calendar textControlName="todt" gmClass="InputArea" textValue="<%=dttodate==null?null:new java.sql.Date(dttodate.getTime())%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;</td>
						</tr>
						</table>
						</td>
						<td  class="RightTableCaption" align="right"  width="150">&nbsp;<fmtPOReport:message key="LBL_TYPE_OF_PO"/>:&nbsp;</td>
						<td width="220">
						<gmjsp:dropdown controlName="potype"  seletedValue="<%=potype%>" defaultValue= "[All Types]"	tabIndex="1"  value="<%=alTypes%>" codeId="CODEID" codeName="CODENM"/>
						</td>
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right"  HEIGHT="30"  width="130">&nbsp;<fmtPOReport:message key="LBL_EMP_NAME"/>:&nbsp;</td>
						<td class="RightTableCaption" HEIGHT="30" width="500"><gmjsp:dropdown controlName="empid"  seletedValue="<%=empid%>" defaultValue= "[All Employees]" tabIndex="1"   value="<%=alUsers%>" codeId="ID" codeName="NAME"/>
						&nbsp;&nbsp;<fmtPOReport:message key="LBL_STATUS"/>:&nbsp;
						<gmjsp:dropdown controlName="status"  seletedValue="<%=status%>" defaultValue= "[Choose One]" tabIndex="1"   value="<%=alStatus%>" codeId="CODEID" codeName="CODENM"/>
						</td>
						<td class="RightTableCaption" align="right"   width="150">&nbsp;<fmtPOReport:message key="LBL_PO_AMT"/>:&nbsp;</td>		
						<td width="220"><input type="text" size="10" value="<%=poamt%>" name="poamt" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;
						&nbsp;&nbsp;
						<fmtPOReport:message key="LBL_LOAD" var="varLoad"/>
						<gmjsp:button value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" gmClass="button" onClick="fnGo();" tabindex="25" buttonType="Load" />
						</td>
					</tr>					
					
					<% if (strOpt.equals("Load")) {%> 	
					<tr><td colspan="4" class="Line" height="1"></td></tr>	
					<% if (strGridXmlData.indexOf("cell") != -1) {%> 			
					<tr><td colspan="4" >
					<div id="dataGridDiv" style="" height="470px" width="1000px"></div>				
					</td></tr>
					<tr>
        			 <td align="center" colspan="4">
          				<div class='exportlinks'><fmtPOReport:message key="LBL_EXPORT_OPTIONS"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
               					onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> <fmtPOReport:message key="LBL_EXCEL"/> </a></div>
           			 </td>
				   </tr>
				   <tr>
					<td colspan="4" class="RightText" align="Center"><BR><fmtPOReport:message key="LBL_CHOOSE_ACTION"/>:&nbsp;
						
						<gmjsp:dropdown controlName="cbo_Action" seletedValue="" defaultValue= "[Choose One]" tabIndex="2"   value="<%=alPOAction%>" codeId="CODEID" codeName="CODENM"/>
						<fmtPOReport:message key="LBL_SUBMIT" var="varSubmit"/>
						<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="fnCallEdit();" tabindex="25" buttonType="Save" />&nbsp;<BR><BR>
						</td>
					</tr>
					<% }else{ %>  
					<tr><td colspan="4" align="center" height="25" class="RegularText"><fmtPOReport:message key="NO_DATA_FOUND"/></td></tr>
					<%} %>	
					<%} %>		
				</table>
			</td>
		</tr>
    </table>

</FORM>

<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
