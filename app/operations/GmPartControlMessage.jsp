<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %> 
<%@page import="java.util.HashMap"%> 


   
<!-- operations\GmPartControlMessage.jsp -->
    <%
    String strCss = GmCommonClass.getString("GMSTYLES");
    String strMsg = "";
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.globus.common.beans.GmCommonClass"%><html>
<head>
<title>Insert title here</title>
<style type="text/css" media="all">
     @import url("<%=strCss%>/screen.css");
</style>
</head>
<bean:define id="alsult" name="frmControlValidation" property="alResult" type="java.util.ArrayList"></bean:define>
<bean:define id="partnum" name="frmControlValidation" property="partcnum" type="java.lang.String"></bean:define>
<body>
<%if(alsult.size()>0){ 
	HashMap hmReturn =(HashMap)alsult.get(0);
	strMsg = GmCommonClass.parseNull((String)hmReturn.get("MSG"));
%>
<table border="0" style="overflow:auto;"  cellspacing="0" cellpadding="0">
 <tr>		
  	<td  height="1" bgcolor="#666666" align="left"></td>
 </tr>
<tr>
  <td>
   <display:table name="requestScope.frmControlValidation.alResult" class="its" id="currentRowObject" decorator=""> 
 	<fmtControlValidation:message key="DT_PART" var="varPart"/><display:column property="PNUM" title="${varPart}" group="1" class="alignleft"/>
	<fmtControlValidation:message key="DT_DESC" var="varDesc"/><display:column property="PARTDESC" title="${varDesc}" group="1" class="alignleft" /> 
	<fmtControlValidation:message key="DT_CONTROL" var="varControl"/><display:column property="CNUM" title="${varControl}"  class="alignleft" />
   </display:table>
 </td>
</tr>
</table>
<br>
<%
}
%>
</body>
</html>