
<%
	/**********************************************************************************
	 * File		 		: GmFARPrint.jsp
	 * Desc		 		: This screen is used for the GmFARPRint - First Artical 
	 * Version	 		: 1.0
	 * author			: Kulanthai velu
	 ************************************************************************************/
%>
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="com.globus.common.servlets.GmServlet"%>

<%@ taglib prefix="fmtFARPrint" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\GmFARPrint.jsp -->

<fmtFARPrint:setLocale value="<%=strLocale%>"/>
<fmtFARPrint:setBundle basename="properties.labels.operations.GmFARPrint"/>

<%
try {
		GmServlet gm = new GmServlet();
		gm.checkSession(response, session);

		HashMap hmReturn = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmReturn"));
		HashMap hmWODetails = new HashMap();
		String strPartNum = "";
		String strDesc = "";
		String strVendorNm = "";
		String strProjectNum="";
		String strProjectDesc="";
		
		 if (hmReturn != null){
		 		hmWODetails = (HashMap)hmReturn.get("WODETAILS");
			
		 		strPartNum = GmCommonClass.parseNull((String)hmWODetails.get("PARTNUM"));
		 		strDesc = GmCommonClass.parseNull((String)hmWODetails.get("PDESC"));
		  		strProjectNum = GmCommonClass.parseNull((String)hmWODetails.get("PRJID"));
		  		strProjectDesc = GmCommonClass.parseNull((String)hmWODetails.get("PRJNAME"));
		  		strVendorNm =  GmCommonClass.parseNull((String)hmWODetails.get("VNAME"));
		 }
		 System.out.println("strImagePath::"+strImagePath);

%>
<HTML>
<HEAD>
<TITLE>Globus Medical: FA Print</TITLE>
<meta http-equiv="Content-Type"	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>

function fnPrint()
{
	window.print();
}
function fnClose()
{
	window.close();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}

</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();"	onafterprint="showPrint();">
<FORM name="frmOrder" method="POST"	action="<%=strServletPath%>/GmWOServlet">
<input type="hidden" name="hWOId" value=""> <input type="hidden" name="hAction"	value=""><BR>
<table cellpadding="0" cellspacing="0" border="0" class="DtTable700">
		<tr>
			<td width="170">&nbsp;&nbsp;&nbsp;<img src="<%=strImagePath%>/s_logo1.png"></td>
			<td class="RightText" width="1" bgcolor="#666666"></td>
			<td align="center" class="RightText"><font size=+3 face="Calibri"><b><fmtFARPrint:message key="LBL_FIRST_ARTICLE_APPROVAL_FORM"/><BR> &nbsp;</b></font><BR>
			</td>
		</tr>
	</table>
	<BR>
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td><img src="<%=strImagePath%>/spacer.gif" width="1"></td>
		</tr>
		<tr>
			<td class="RightText" height="30" align="right"><B><fmtFARPrint:message key="LBL_PART_NUMBER"/>	</B>:</td>
			<td class="RightText" bgcolor="#eeeeee">&nbsp;<%=strPartNum %></td>
			<td class="RightText" height="30" align="right"><B><fmtFARPrint:message key="LBL_DESCRIPTION"/></B>:</td>
			<td class="RightText" bgcolor="#eeeeee">&nbsp;<%=strDesc %></td>
		</tr>
		<tr>
			<td colspan="6" height="1" class="Line"></td>
		</tr>
		<tr>
			<td class="RightText" height="30" align="right"><B><fmtFARPrint:message key="LBL_PROJECT_NUMBER"/>
			</B>:</td>
			<td class="RightText" bgcolor="#eeeeee">&nbsp;<%=strProjectNum %></td>
			<td class="RightText" align="right"><B><fmtFARPrint:message key="LBL_PROJECT_NAME"/></B>:</td>
			<td class="RightText" bgcolor="#eeeeee">&nbsp;<%=strProjectDesc%></td>
		</tr>
		<tr>
			<td colspan="6" class="Line"></td>
		</tr>
		<tr>
			<td class="RightText" height="35" align="right"><B><fmtFARPrint:message key="LBL_VENDOR"/></B>:</td>
			<td class="RightText" colspan="4"  bgcolor="#eeeeee">&nbsp;<%=strVendorNm %></td>
		</tr>
	</table>
		<BR>
		<tr>
		<td class="RightText" height="20" colspan="4">&nbsp;<b><fmtFARPrint:message key="LBL_INSPECTION_RECORD"/>	</b></td>
		</tr>
		<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td><img src="<%=strImagePath%>/spacer.gif" width="1"></td>
		</tr>
		<tr>
			<td class="RightText" height="23" height="30">&nbsp;&nbsp;&nbsp;<input
				type="checkbox">&nbsp;&nbsp;<b><fmtFARPrint:message key="LBL_INSPECTION_SHEET"/></b></td>
			<td class="RightText" height="30">&nbsp;&nbsp;&nbsp;<input
				type="checkbox">&nbsp;&nbsp; <b><fmtFARPrint:message key="LBL_DRAWING_MARKED_UP"/></b></td>
			<td class="RightText" height="30">&nbsp;&nbsp;&nbsp;<input
				type="checkbox">&nbsp;&nbsp; <b><fmtFARPrint:message key="LBL_DRAWING_W/O"/> </b></td>
			<td class="RightText">&nbsp;</td>
		</tr>
	</table>
	<BR>
	<tr>
		<td class="RightText"  height="20" colspan="4">&nbsp;<b><fmtFARPrint:message key="LBL_INSPECTION_TOOLS"/>		 </b><i></i></td>
	</tr>
	<BR>
	<table class="DtTable700" border="0"  cellspacing="0" cellpadding="0">
		<tr bgcolor="#eeeeee">
			<td height="25" class="RightText" colspan="3" align="center"  >&nbsp;<B><fmtFARPrint:message key="LBL_DIMENSION_VERIFICATION"/>	</B></td>
			<td class="RightText" width="1" bgcolor="#666666"></td>
 			<td height="25" class="RightText" colspan="4" align="center">&nbsp;<B><fmtFARPrint:message key="LBL_FUCTIONAL_VERIFICATION"/>	</B></td>
		</tr>
			<tr>
				<td colspan="7"  class="Line"></td>
			</tr>
			<tr bgcolor="#eeeeee">
				<th class="RightText" width="160" align="center"  height="25"><b><fmtFARPrint:message key="LBL_MEASURING_DEVICE_ID"/></b></th>
				<td class="Line" width="1" ></td>
				<th class="RightText" align="center" ><b><fmtFARPrint:message key="LBL_MEASURING_DEVICE_ID"/></b></th>
				<td class="Line" width="1" ></td>
				<th class="RightText" align="center" width="180"><b><fmtFARPrint:message key="LBL_GAUGE_ID"/></b></th>
				<td class="Line" width="1" ></td>
				<th class="RightText" width="180" align="center"><b><fmtFARPrint:message key="LBL_GAUGE_ID"/></b></th>
			</tr>
			<tr>
				<td colspan="7" class="Line"></td>
			</tr>
			<tr>
				<td height="40">&nbsp;</td>
				<td class="RightText" width="1" bgcolor="#666666"></td>
				<td>&nbsp;</td>
				<td class="RightText" width="1" bgcolor="#666666"></td>
				<td>&nbsp;</td>
				<td class="RightText" width="1" bgcolor="#666666"></td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="7"  class="Line"></td>
			</tr>	
			<tr >
				<td height="40">&nbsp;</td>
				<td class="RightText" width="1" bgcolor="#666666"></td>
				<td>&nbsp;</td>
				<td class="RightText" width="1" bgcolor="#666666"></td>
				<td>&nbsp;</td>
				<td class="RightText" width="1" bgcolor="#666666"></td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="7" class="Line"></td>
			</tr>
			<tr>
				<td height="40">&nbsp;</td>
				<td class="RightText" width="1" bgcolor="#666666"></td>
				<td>&nbsp;</td>
				<td class="RightText" width="1" bgcolor="#666666"></td>
				<td>&nbsp;</td>
				<td class="RightText" width="1" bgcolor="#666666"></td>
				<td>&nbsp;</td>
			</tr>
				<tr>
				<td colspan="7" class="Line"></td>
			</tr>
			<tr>
				<td height="40">&nbsp;</td>
				<td class="RightText" width="1" bgcolor="#666666"></td>
				<td>&nbsp;</td>
				<td class="RightText" width="1" bgcolor="#666666"></td>
				<td>&nbsp;</td>
				<td class="RightText" width="1" bgcolor="#666666"></td>
					<td>&nbsp;</td>
			</tr>
 
		</table>
		<tr>
			<td><< <fmtFARPrint:message key="LBL_ATTACH_NECESSARY_SHEET"/>  >></td>
		</tr>
		
		<tr>
			<td bgcolor="#666666" colspan="6"></td>
		</tr>
		<BR><BR>
		<tr>
			<td class="RightText"  height="20" colspan="4">&nbsp;<b><fmtFARPrint:message key="LBL_ENGINEERING_DISPOSITION"/></b></td>
		</tr>
	<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" height="20" bgcolor="#eeeeee"><b>&nbsp;<fmtFARPrint:message key="LBL_ACTION_TAKEN"/></b></td>
			<td class="RightText" width="1" bgcolor="#666666"></td>
 			<td height="20" bgcolor="#eeeeee"><b>&nbsp;<fmtFARPrint:message key="LBL_COMMENTS"/></b></td>
		</tr>
		<tr>
			<td colspan="6" height="1" bgcolor="#666666"></td>
		</tr>
		<tr>
		<td width="50" align="center"> &nbsp;&nbsp;&nbsp;&nbsp;<input	type="checkbox"></td>
		<td class="RightText" width="1" bgcolor="#666666"></td>
		<td height="40" width="150" >&nbsp;<fmtFARPrint:message key="LBL_APPROVE_AS"/></td>
		<td class="RightText" width="1" bgcolor="#666666"></td>
		<td width="450">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="6" height="1" bgcolor="#666666"></td>
		</tr>
		<tr>
		<td  align="center"> &nbsp;&nbsp;&nbsp;&nbsp;<input	type="checkbox"></td>
		<td class="RightText" width="1" bgcolor="#666666"></td>
		<td height="40">&nbsp;<fmtFARPrint:message key="LBL_APPROVE_CHANGES"/></td>
		<td class="RightText" width="1" bgcolor="#666666"></td>
		<td width="450">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="6" height="1" bgcolor="#666666"></td>
		</tr>
		<tr>
		<td align="center"> &nbsp;&nbsp;&nbsp;&nbsp;<input	type="checkbox"></td>
		<td class="RightText" width="1" bgcolor="#666666"></td>
		<td height="40">&nbsp;<fmtFARPrint:message key="LBL_NOT_APPROVED"/></td>
				<td class="RightText" width="1" bgcolor="#666666"></td>
		<td width="450">&nbsp;</td>
		</tr>
	</table>
		<BR>
	<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
		<tr >
		<td bgcolor="#eeeeee" height="20" colspan="7" align="center"><b><fmtFARPrint:message key="LBL_APPROVALS"/></b></td></tr>
			<tr>
				<td colspan="7" height="1" bgcolor="#666666"></td>
			</tr>
		<tr>
 
		<td height="40" > &nbsp;&nbsp;<b><fmtFARPrint:message key="LBL_RECEIVING_INSPECTOR"/></b></td>
 		<td class="RightText" width="1" bgcolor="#666666"></td>
		<td valign="top" width="250" colspan="3"> <font size="0">&nbsp;<fmtFARPrint:message key="LBL_SIGNATURE"/></font></td>
 		<td class="RightText" width="1" bgcolor="#666666"></td>
		<td valign="top" width="110"> <font size="0">&nbsp;<fmtFARPrint:message key="LBL_DATE"/></font></td>
		</tr>
			<tr>
				<td colspan="7" height="1" bgcolor="#666666"></td>
			</tr>
		<tr>
		<td height="40" >&nbsp; <b><fmtFARPrint:message key="LBL_PRODUCT_ENGINEER"/></b></td>
		<td class="RightText" width="1" bgcolor="#666666"></td>
		<td valign="top" width="250" colspan="3"> <font size="0">&nbsp;<fmtFARPrint:message key="LBL_SIGNATURE"/></font></td>
 		<td class="RightText" width="1" bgcolor="#666666"></td>
		<td valign="top"> <font size="0">&nbsp;<fmtFARPrint:message key="LBL_DATE"/></font></td>
		</tr>
				<tr>
				<td colspan="7" height="1" bgcolor="#666666"></td>
			</tr>
		<tr>
		<td height="40">&nbsp; <b><fmtFARPrint:message key="LBL_GROUP_MANAGER"/></b></td>
		<td class="RightText" width="1" bgcolor="#666666"></td>
		<td valign="top" width="250" colspan="3"> <font size="0">&nbsp;<fmtFARPrint:message key="LBL_SIGNATURE"/></font></td>
 		<td class="RightText" width="1" bgcolor="#666666"></td>
		<td valign="top"> <font size="0">&nbsp;<fmtFARPrint:message key="LBL_DATE"/></font></td>
		</tr>
		</tr>
		<tr>
				<td colspan="7" height="1" bgcolor="#666666"></td>
			</tr>
		<tr>
		<td height="40">&nbsp; <b><fmtFARPrint:message key="LBL_(PD)_VICE_PRESIDENT"/></b></td>
		<td class="RightText" width="1" bgcolor="#666666"></td>
		<td valign="top" width="250" colspan="3"> <font size="0">&nbsp;<fmtFARPrint:message key="LBL_SIGNATURE"/></font></td>
 		<td class="RightText" width="1" bgcolor="#666666"></td>
		<td valign="top"> <font size="0">&nbsp;<fmtFARPrint:message key="LBL_DATE"/></font></td>
		</tr>
			
 		
</table>
<BR><BR>
<span class="RightText">GM-H007; Rev. A; DCO#11-000666</span>
<BR>
		<table width="700" border="0" cellspacing="0" cellpadding="0">
		<tr>
 		<td align="center" height="30" id="button">
 		<fmtFARPrint:message key="BTN_PRINT" var="varPrint"/>
 				<gmjsp:button onClick="fnPrint();" gmClass="button" value = "&nbsp;${varPrint}&nbsp;" buttonType="Load" />&nbsp;
 				<fmtFARPrint:message key="BTN_CLOSE" var="varClose"/>
 				<gmjsp:button onClick="fnClose();" gmClass="button" value = "&nbsp;${varClose}&nbsp;" buttonType="Load" />
		</td>
  		</tr>
  		<tr><td><%@ include file="/common/GmFooter.inc" %></td></tr>
  		</table>
  		
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
