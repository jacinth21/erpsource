 <%
/**********************************************************************************
 * File		 		: GmWOPrint.jsp
 * Desc		 		: This screen is used for the Work Order - Print Formart
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ taglib prefix="fmtWOPrintD" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\GmWOPrintD.jsp -->

<fmtWOPrintD:setLocale value="<%=strLocale%>"/>
<fmtWOPrintD:setBundle basename="properties.labels.operations.GmWOPrintD"/>


<%

try {
	//GmServlet gm = new GmServlet();
	//gm.checkSession(response,session);

	String strPrintDHR = GmCommonClass.parseNull((String)request.getParameter("PrintDHR"));

	HashMap hmReturn = new HashMap();
	if(!strPrintDHR.equals("Y")){
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	} else {
		// get the HashMap from GmPOReceiveServlet  (Print DHR)
		hmReturn = (HashMap)request.getAttribute("hmReturnWODetails");
	}
	HashMap hmWODetails = new HashMap();
	ArrayList alSubWorkOrder = new ArrayList();
	ArrayList alSubDHR = new ArrayList();
		
	String strWOId = "";
	String strPOId = "";
	String strPartNum = "";
	String strDesc = "";
	String strQty = "";
	String strMatSpec = "";
	String strStartDate = "";
	String strNeedByDate = "";
	String strVendorNm = "";
	String strLotCode = "";
	String strCompCertFl = "";
	String strMatCertFl = "";
	String strHardTestFl = "";
	String strUser = "";
	String strDraw = "";
	String strDrawRev = "";
	String strSubPartNum = "";
	String strSubPartDesc = "";
	String strSubControl = "";
	String strSubManfDt = "";
	String strFooter = "";
	String strVALDFL = "";
	String strContent = "";
	String strValidFLContent = "";
	String strValidateFLContent = "";
	String strCompanyLogo = GmCommonClass.parseNull((String) hmReturn.get("LOGO"));
	strCompanyLogo = strCompanyLogo.equals("")?GmCommonClass.parseNull((String)request.getParameter("CompanyLogo")):strCompanyLogo;
		
	String strYes = "<td><img src="+strImagePath+"/y.gif></td><td><img src="+strImagePath+"/n.gif></td><td><img src="+strImagePath+"/n.gif></td>";
	String strNo = "<td><img src="+strImagePath+"/n.gif></td><td><img src="+strImagePath+"/y.gif></td><td><img src="+strImagePath+"/n.gif></td>";
	String strNA = "<td><img src="+strImagePath+"/n.gif></td><td><img src="+strImagePath+"/n.gif></td><td><img src="+strImagePath+"/y.gif></td>";
	String strNoData = "<td><img src="+strImagePath+"/n.gif></td><td><img src="+strImagePath+"/n.gif></td><td><img src="+strImagePath+"/n.gif></td>";
	String strInsp = "<td><img src="+strImagePath+"/n.gif></td><td><img src="+strImagePath+"/y.gif></td><td></td>";
	String strSter = "";
	boolean bolSter = false;
		
	if (hmReturn != null)
	{
		hmWODetails = (HashMap)hmReturn.get("WODETAILS");

		strWOId = GmCommonClass.parseNull((String)hmWODetails.get("WID"));
		strPOId = GmCommonClass.parseNull((String)hmWODetails.get("POID"));
		strPartNum = GmCommonClass.parseNull((String)hmWODetails.get("PARTNUM"));
		strDesc = GmCommonClass.parseNull((String)hmWODetails.get("PDESC"));
		strQty = GmCommonClass.parseNull((String)hmWODetails.get("QTY"));
		strMatSpec = GmCommonClass.parseNull((String)hmWODetails.get("MSPEC"));
		strStartDate = GmCommonClass.parseNull((String)hmWODetails.get("CDATE"));
		strNeedByDate = GmCommonClass.parseNull((String)hmWODetails.get("RDATE"));
		strVendorNm = GmCommonClass.parseNull((String)hmWODetails.get("VNAME"));
		strLotCode = GmCommonClass.parseNull((String)hmWODetails.get("LC"));
		strCompCertFl = GmCommonClass.parseNull((String)hmWODetails.get("CCFL"));
		strMatCertFl = GmCommonClass.parseNull((String)hmWODetails.get("MCFL"));
		strHardTestFl = GmCommonClass.parseNull((String)hmWODetails.get("HTFL"));
		strDraw = GmCommonClass.parseNull((String)hmWODetails.get("DRAW"));
		strDrawRev = GmCommonClass.parseNull((String)hmWODetails.get("DRAWREV"));
		strSter = GmCommonClass.parseNull((String)hmWODetails.get("STERFL"));
		strFooter = GmCommonClass.parseNull((String)hmWODetails.get("FOOTER"));
		strVALDFL = GmCommonClass.parseNull((String)hmWODetails.get("VALIDATIONFL"));	

		String strArr[] = strFooter.split("\\^");
		strFooter = strArr[0];
				
		alSubWorkOrder = (ArrayList)hmReturn.get("SUBWODETAILS");
		alSubDHR = (ArrayList)hmReturn.get("SUBDHRDETAILS");
				
		if (strCompCertFl.equals("Y"))
		{
			strCompCertFl = strYes;
		}
		else if (strCompCertFl.equals("N"))
		{
			strCompCertFl = strNo;
		}
		else if (strCompCertFl.equals("A"))
		{
			strCompCertFl = strNA;
		}
		else
		{
			strCompCertFl = strNoData;
		}

		if (strMatCertFl.equals("Y"))
		{
			strMatCertFl = strYes;
		}
		else if (strMatCertFl.equals("N"))
		{
			strMatCertFl = strNo;
		}
		else if (strMatCertFl.equals("A"))
		{
			strMatCertFl = strNA;
		}
		else
		{
			strMatCertFl = strNoData;
		}

		if (strHardTestFl.equals("Y"))
		{
			strHardTestFl = strYes;
		}
		else if (strHardTestFl.equals("N"))
		{
			strHardTestFl = strNo;
		}
		else if (strHardTestFl.equals("A"))
		{
			strHardTestFl = strNA;
		}
		else
		{
			strHardTestFl = strNoData;
		}
		
		if (strSter.equals("1"))
		{
			strSter = strYes;
			bolSter = true;
			strMatCertFl = strNA;
		}
		else
		{
			strSter = strNA;
		}
		if(strVALDFL.equals("Y")){
			strValidFLContent = "\t- <b>("+GmCommonClass.getString("GMVALIDATIONFL")+")</b>";
			strValidateFLContent = "\t<b>("+GmCommonClass.getString("GMVALIDATIONFLCONTENT")+")</b>";
		}else{
			strValidFLContent = "";
			strValidateFLContent ="";
		}
		strUser = (String)hmWODetails.get("CUSER");
		strUser = strUser.concat(".gif");		
		
	}

	int intSize = 0;
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: WO Print</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>

function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmOrder" method="POST" action="<%=strServletPath%>/GmWOServlet">
<input type="hidden" name="hWOId" value="">
<input type="hidden" name="hAction" value="">
<BR>
	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td bgcolor="#666666" rowspan="3" width="1"></td>
			<td bgcolor="#666666" colspan="4" height="1"></td>
			<td bgcolor="#666666" rowspan="3" width="1"></td>
		</tr>
		<tr>
			<td width="170" height="70"><img src="<%=strImagePath%>/<%=strCompanyLogo%>.gif" width="138" height="60"></td>
			<td class="RightText" width="130">&nbsp;</td>
			<td class="RightText" width="1"bgcolor="#666666"></td>
			<td align="right" class="RightText"><font size=+3><fmtWOPrintD:message key="LBL_WORK_ORDER"/></font>&nbsp;</td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
	</table>
	<BR>
	<BR>
	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr>
			<td bgcolor="#666666" rowspan="20" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1"></td>
			<td bgcolor="#666666" colspan="4" height="1"></td>
			<td bgcolor="#666666" rowspan="20" width="1"></td>
		</tr>
		<tr>
			<td class="RightText" align="right" height="25"><B><fmtWOPrintD:message key="LBL_WORK_ORDER_#"/></B>:</td>
			<td class="RightText" width="220">&nbsp;<%=strWOId%><%=strValidFLContent %></td>
			<td class="RightText" align="right"><B><fmtWOPrintD:message key="LBL_PO_#"/></B>:</td>
			<td class="RightText">&nbsp;<%=strPOId%></td>
		</tr>
		<tr><td colspan="4" height="1" bgcolor="#CCCCCC"></td></tr>
		<tr>
			<td class="RightText"  height="25" align="right"><B><fmtWOPrintD:message key="LBL_PART_NUMBER"/></B>:</td>
			<td class="RightText">&nbsp;<%=strPartNum%></td>
			<td class="RightText"  height="25" align="right"><B><fmtWOPrintD:message key="LBL_DESCRIPTION"/></B>:</td>
			<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
		</tr>
		</tr><td colspan="4" height="1" bgcolor="#CCCCCC"></td></tr>
		<tr>
			<td class="RightText" height="25" align="right"><B><fmtWOPrintD:message key="LBL_START_DATE"/></B>:</td>
			<td class="RightText">&nbsp;<%=strStartDate%></td>
			<td class="RightText" align="right"><B><fmtWOPrintD:message key="LBL_NEED_BY"/></B>:</td>
			<td class="RightText">&nbsp;<%=strNeedByDate%></td>
		</tr>
		<tr>
			<td colspan="4" height="1" bgcolor="#CCCCCC"></td>
		</tr>
		<tr>
			<td class="RightText" height="25"  align="right"><B><fmtWOPrintD:message key="LBL_VENDOR"/></B>:</td>
			<td class="RightText">&nbsp;<%=strVendorNm%></td>
			<td class="RightText" align="right"><B><fmtWOPrintD:message key="LBL_LOT_#"/></B>:</td>
			<td class="RightText">&nbsp;<%=strLotCode%>[A-Z][0-3][0-9][0-9][A-Z]<%=strDrawRev%></td>
		</tr>
		</tr>
			<td colspan="4" height="1" bgcolor="#CCCCCC"></td>
		</tr>
		<tr>
			<td class="RightText"  align="right"><B><fmtWOPrintD:message key="LBL_QUANTITY"/></B>:</td>
			<td class="RightText">&nbsp;<%=strQty%><%=strValidateFLContent%></td>
			<td colspan="2" rowspan="7" align="center">
				<table width="270" border="0" cellspacing="0">
					<tr>
						<td class="RightText" align="right">&nbsp;</td>
						<td class="RightText" width="30" align=center><fmtWOPrintD:message key="LBL_YES"/></td>
						<td class="RightText" width="30" align=center><fmtWOPrintD:message key="LBL_NO"/></td>
						<td class="RightText" width="30" align=center><fmtWOPrintD:message key="LBL_N/A"/></td>
					</tr>
					<tr align="center">
						<td class="RightText" height="20" align="right"><B><fmtWOPrintD:message key="LBL_MATERIAL_CERTIFICATION"/></B>:</td>
						<%=strMatCertFl%>
					</tr>
					<tr align="center">
						<td class="RightText" nowrap height="20" align="right"><B><fmtWOPrintD:message key="LBL_CERTIFICATION_COMPLIANCE"/></B>:</td>
						<%=strCompCertFl%>
					</tr>
					<tr align="center">
						<td class="RightText" nowrap height="20" align="right"><B><fmtWOPrintD:message key="LBL_HARDNESS_TEST_REQUIRED"/> ?</B>:</td>
						<%=strHardTestFl%>
					</tr>
					<tr align="center">
						<td class="RightText" nowrap height="20" align="right"><B><fmtWOPrintD:message key="LBL_INSPECTION_SHEET_ATTACHED"/> ?</B>:</td>
						<%=strInsp%>
					</tr>
					<tr align="center">
						<td class="RightText" nowrap height="20" align="right"><B><fmtWOPrintD:message key="LBL_STERILIZATION_REQUIRED"/> ?</B>:</td>
						<%=strSter%>
					</tr>					
				</table>
			</td>
		</tr>
		</tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
		<tr>
			<td class="RightText" align="right"><B><fmtWOPrintD:message key="LBL_DRAWING_#"/></B>:</td>
			<td class="RightText">&nbsp;<%=strDraw%></td>
			</td>
		</tr>
		</tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>		
		<tr>
			<td class="RightText"  nowrap align="right"><B><fmtWOPrintD:message key="LBL_DRAWING_REV_#"/></B>:</td>
			<td class="RightText">&nbsp;<%=strDrawRev%></td>
		</tr>
		</tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>		
		<tr>
			<td class="RightText" height="20" nowrap align="right"><B><fmtWOPrintD:message key="LBL_MATERIAL_SPECIFICATION"/></B>:</td>
			<td class="RightText">&nbsp;<%=strMatSpec%></td>
		</tr>
		<tr>
			<td colspan="4" height="1" bgcolor="#666666"></td>
		</tr>
	</table>
<BR>
<span class="RightText"><b><fmtWOPrintD:message key="LBL_SUB_ASSEMBLY_DETAILS"/>:</b></span><BR>
	<table width="700" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td rowspan="25" width="1" bgcolor="#666666"><img src="<%=strImagePath%>/spacer.gif" width="1"></td>
			<td colspan="5" height="1" bgcolor="#666666"></td>
			<td rowspan="25" width="1" bgcolor="#666666"></td>
		</tr>
		<tr class="RightTableCaption">
			<td height="18"><fmtWOPrintD:message key="LBL_SUB_COMP_ID"/></td>
			<td widht="440"><fmtWOPrintD:message key="LBL_DESCRIPTION"/></td>
			<td><fmtWOPrintD:message key="LBL_DRAWING_#"/></td>
			<td><fmtWOPrintD:message key="LBL_REV#"/></td>
			<td><fmtWOPrintD:message key="LBL_MATERIAL_SPEC"/></td>
		</tr>
		<tr><td colspan="5" height="1" bgcolor="#666666"></td></tr>
<%
  		intSize = alSubWorkOrder.size();
		hcboVal = new HashMap();
		if ( intSize > 0)
		{
	  		for (int i=0;i<intSize;i++)
	  		{
	  			hcboVal = (HashMap)alSubWorkOrder.get(i);
	  			strSubPartNum = GmCommonClass.parseNull((String)hcboVal.get("PNUM"));
				strSubPartDesc = GmCommonClass.getStringWithTM((String)hcboVal.get("PDESC"));
				strDraw = GmCommonClass.parseNull((String)hcboVal.get("DRAW"));
				strDrawRev = GmCommonClass.parseNull((String)hcboVal.get("REV"));
				strMatSpec = GmCommonClass.parseNull((String)hcboVal.get("MSPEC"));
%>
				<tr>
					<td height="18" class="RightText">&nbsp;<%=strSubPartNum%></td>
					<td class="RightText">&nbsp;<%=strSubPartDesc%></td>
					<td class="RightText">&nbsp;<%=strDraw%></td>
					<td class="RightText">&nbsp;<%=strDrawRev%></td>
					<td class="RightText">&nbsp;<%=strMatSpec%></td>
				</tr>
				<tr><td colspan="5" height="1" bgcolor="#eeeeee"></td></tr>
<%
			}
		}else{
%>
		<tr><td colspan="5" height="30" align="center" class="RightText"><fmtWOPrintD:message key="LBL_NOT_APPLICABLE"/></td></tr>
<%
			}
%>
		<tr><td colspan="5" height="1" bgcolor="#666666"></td></tr>
	</table>
<BR>

<span class="RightText"><b><fmtWOPrintD:message key="LBL_SERIALIZATION_REQUIRED"/>:</b></span><BR>
<span class="RightText"><fmtWOPrintD:message key="LBL_INCLUDE_ORGINAL_CONTROL_NUMBER"/></span>
	<table class="DtTable700" cellspacing="0" cellpadding="0" border="0">
		
		<tr class="RightTableCaption">
			<td height="18"><fmtWOPrintD:message key="LBL_SUB_COMP_ID"/></td>
			<td width="400"><fmtWOPrintD:message key="LBL_DESCRIPTION"/></td>
			<td width="100"><fmtWOPrintD:message key="LBL_MANF_DATE"/></td>
			<td width="100"><fmtWOPrintD:message key="LBL_LOT_#"/></td>
		</tr>
		<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
<%
  		intSize = alSubDHR.size();
		hcboVal = new HashMap();
		if ( intSize > 0)
		{
	  		for (int i=0;i<intSize;i++)
	  		{
	  			hcboVal = (HashMap)alSubDHR.get(i);
	  			strSubPartNum = GmCommonClass.parseNull((String)hcboVal.get("SUBASMBID"));
				strSubPartDesc = GmCommonClass.getStringWithTM((String)hcboVal.get("PDESC"));
				strSubControl = GmCommonClass.parseNull((String)hcboVal.get("CNUM"));
				strSubManfDt = GmCommonClass.parseNull((String)hcboVal.get("MDATE"));
%>
				<tr>
					<td height="18" class="RightText">&nbsp;<%=strSubPartNum%></td>
					<td class="RightText">&nbsp;<%=strSubPartDesc%></td>
					<td class="RightText">&nbsp;<%=strSubManfDt%></td>
					<td class="RightText">&nbsp;<%=strSubControl%></td>
				</tr>
				<tr><td colspan="4" height="1" bgcolor="#eeeeee"></td></tr>
<%
			}
		}else{
%>
		<tr><td colspan="4" height="30" align="center" class="RightText"><fmtWOPrintD:message key="LBL_NOT_APPLICABLE"/></td></tr>
<%
			}
%>
		
	</table>
<BR>
<span class="RightText"><b><fmtWOPrintD:message key="LBL_MANUFACTURING_IN_HOUSE"/>:</b></span><BR>
	<table width="700" cellspacing="1" cellpadding="0" bgcolor="#cccccc" border="0">
		<tr align="center" bgcolor="white" class="RightTableCaption">
			<td height="18">&nbsp;<fmtWOPrintD:message key="LBL_OPERATION#"/>&nbsp;</td>
			<td>&nbsp;<fmtWOPrintD:message key="LBL_MATCH#"/>&nbsp;</td>
			<td><fmtWOPrintD:message key="LBL_OPERATION_DESCRIPTION"/></td>
			<td><fmtWOPrintD:message key="LBL_PC_INTLS"/></td>
			<td><fmtWOPrintD:message key="LBL_QTY_GOOD"/></td>
			<td><fmtWOPrintD:message key="LBL_QTY_SCRAP"/></td>
			<td>&nbsp;<fmtWOPrintD:message key="LBL_INTLS"/>&nbsp;</td>
			<td>&nbsp;<fmtWOPrintD:message key="LBL_MFG_DATE"/><br>&nbsp;&nbsp;</td>
			<td>&nbsp;<fmtWOPrintD:message key="LBL_COMMENTS"/>&nbsp;</td>
			<td>&nbsp;<fmtWOPrintD:message key="LBL_CELL"/>&nbsp;</td>
		</tr>
		<tr bgcolor="white">
			<td height="18">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr bgcolor="white">
			<td height="18">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr bgcolor="white">
			<td height="18">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
<BR>
<BR>
	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td bgcolor="#666666" rowspan="4" width="1"></td>
			<td bgcolor="#666666" colspan="4" height="1"></td>
			<td bgcolor="#666666" rowspan="4" width="1"></td>
		</tr>
		<tr>
			<td width="170"><img src="<%=strImagePath%>/<%=strUser%>"></td>
			<td class="RightText" width="130">&nbsp;</td>
			<td class="RightText" width="1"bgcolor="#666666"></td>
			<td align="center" class="RightText"><%=strStartDate%>&nbsp;</td>
		</tr>
		<tr>
			<td width="170" class="RightTableCaption"><fmtWOPrintD:message key="LBL_VP_OPERATION_DESIGNEE"/></td>
			<td class="RightText" width="130">&nbsp;</td>
			<td class="RightText" width="1"bgcolor="#666666"></td>
			<td align="center" class="RightTableCaption"><fmtWOPrintD:message key="LBL_DATE"/>&nbsp;</td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
	</table>
<BR>
<BR>
<span class="RightText"><%=strFooter%></span>
<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr><td height="2"></td></tr>
		<tr>
			<td>
			<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
					<jsp:include page="/common/GmIncludeDateStamp.jsp" />
			</td>	
		</tr>
	</table>
	<%if(!strPrintDHR.equals("Y")){ %>
<BR>
	<table border="0" width="700" cellspacing="0" cellpadding="0" >
		<tr>
			<td align="center" height="30" id="button">
			<fmtWOPrintD:message key="BTN_PRINT" var="varPrint"/>
				<gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" onClick="fnPrint();" buttonType="Load" />&nbsp;&nbsp;
				<fmtWOPrintD:message key="BTN_CLOSE" var="varClose"/>
				<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close()" buttonType="Load" />&nbsp;&nbsp;
			</td>
		<tr>
	</table>
     <%} %>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>

</BODY>

</HTML>
