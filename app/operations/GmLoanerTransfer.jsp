 <%@page import="com.globus.common.beans.GmCalenderOperations"%>
<%
/**********************************************************************************
 * File		 		: GmInHouseItemConsignSetup.jsp
 * Desc		 		: Currently used only for BULK
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Set" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ taglib prefix="fmtLoanerTransfer" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!--GmLoanerTransfer.jsp  -->
<fmtLoanerTransfer:setLocale value="<%=strLocale%>"/>
<fmtLoanerTransfer:setBundle basename="properties.labels.operations.GmLoanerTransfer"/>

<%@page import="org.apache.commons.collections.MultiHashMap"%><HTML>
<%
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
%>
<HEAD>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/displaytag.css");
</style>
<TITLE> Globus Medical: Gm Loaner Transfer </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmLoanerTransfer.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmCommonShippingReport.js"></script>

<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/skins/dhtmlxcalendar_yahoolike.css">


<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_dhxcalendar.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid_form.js "></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">


<bean:define id="gridData" name="frmLoanerTransfer" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="haction" name="frmLoanerTransfer" property="haction" type="java.lang.String"> </bean:define>
<bean:define id="toDistributor" name="frmLoanerTransfer" property="toDistributor" type="java.lang.String"> </bean:define>

<% 
	ArrayList alResult = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("alResult"));
	ArrayList alReqList = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("alReqList"));
	ArrayList alLReqList = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("alLNReqList"));
	ArrayList alReqDtl = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("alReqDtl"));
	ArrayList alReps = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("alReps"));
	ArrayList alAssocReps = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("alAssocReps"));
	
	ArrayList alAccount = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("alAccount"));
	String strType = GmCommonClass.parseNull((String)request.getAttribute("strType"));
	GmCalenderOperations.setTimeZone(strGCompTimeZone);
	GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.operations.GmLoanerTransfer", strSessCompanyLocale);
	String strTodaysDate = GmCalenderOperations.getCurrentDate(strGCompDateFmt);//(String) request.getSession().getAttribute("strSessTodaysDate");
	String strApplDateFmt =strGCompDateFmt;// GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	int j=0;
	String strHeader="";
	String strWikiTitle ="LOANER_TRANSFER";
	String strTable ="DtTable1300";
	if(haction.equals("INHOUSELOANER")){
		strHeader=GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_IN_HOUSE"));
		strWikiTitle ="INHOUSE_LOANER_TRANSFER";
		strTable = "dttable900";
	}
%>

<script>
var strTodaysDate = '<%=strTodaysDate%>';
var dateFmt = '<%=strApplDateFmt%>';
	var objGridData;
	objGridData = '<%=gridData%>';

	var RepDetArr = new Array();
	var RepArr = new Array();
	var ReqDtlArr = new Array();
	var AccArr = new Array();
	var RepsArr = new Array();
	var AssocRepsArr = new Array();
	var fromDistributor = "";
	var toDistributor = "";
	
	var Type = '<%=strType%>'; 
	var RepLen = <%=alResult.size()%>;
	<%
	    int repCnt =0;
	    int accCnt =0;
	    int assocrepCnt = 0;
		int intSize = alResult.size();
	 	HashMap hmResult = new HashMap();
		for (int i=0;i<intSize;i++)
		{
			hmResult = (HashMap)alResult.get(i);
	%>
			RepArr[<%=i%>]="<%=hmResult.get("SETID")%>";
	<%
	}
	%>
	<%
		int intReqSize = alReqList.size();
		MultiHashMap multiHashMap=new MultiHashMap(); 

	 	HashMap hmResultReq = new HashMap();
		for (int i=0;i<intReqSize;i++)
		{
			hmResultReq = (HashMap)alReqList.get(i);%>
			RepDetArr[<%=i%>]=new Array(2);
			RepDetArr[<%=i%>][0]="<%=hmResultReq.get("SETID")%>";
			RepDetArr[<%=i%>][1]="<%=hmResultReq.get("REQUESTID")%>";
				
		<%}
	%>
	
	<%
	  int intReqDtlSize = alLReqList.size();

	  HashMap hmReqDtl = new HashMap();
	 
	  for(int i=0;i<intReqDtlSize;i++)
	  {
		  hmReqDtl = (HashMap)alLReqList.get(i);%>
		  ReqDtlArr[<%=j%>] = "<%=hmReqDtl.get("CODEID")%> , <%=hmReqDtl.get("CODENM")%>";
		  
	 <% j++;
	 }
	%>
	<%
	 if(!haction.equals("INHOUSELOANER")){
	  int intReqDtl = alReqDtl.size();
	
	  for(int i=0;i<intReqDtl;i++)
	  {
		  hmReqDtl = (HashMap)alReqDtl.get(i);%>
		  ReqDtlArr[<%=j%>] = "<%=hmReqDtl.get("CODEID")%>,<%=hmReqDtl.get("CODENM")%>";
		 
	 <%  j++;
	 }
	 }
	%>
	<%
	// sales reps belongs to distributor
	if(!toDistributor.equals("")){
	HashMap hmRep = new HashMap();
	String strDistId ="";
	for (int i=0;i<alReps.size();i++)
	{
		hmRep = (HashMap)alReps.get(i);
		strDistId = GmCommonClass.parseNull((String)hmRep.get("DID"));
	  if(strDistId.equals(toDistributor)){
	%>
		RepsArr[<%=repCnt%>] ="<%=hmRep.get("REPID")%>,<%=hmRep.get("NAME")%>,<%=hmRep.get("PARTYID")%>,<%=hmRep.get("DID")%>";
	<%
	repCnt++;
	
	}
	}
	%>
	
	<%	
	// Accounts for To Distributor
	HashMap hcboVal = new HashMap();
	for (int i=0;i<alAccount.size();i++)
	{
		hcboVal = (HashMap)alAccount.get(i);
		strDistId = GmCommonClass.parseNull((String)hcboVal.get("DID"));
	if(strDistId.equals(toDistributor)){
	%>
		AccArr[<%=accCnt%>] ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>,<%=hcboVal.get("DID")%>";
	<%
	accCnt++;
	
	}
	}
	}
	%>
	<%
	// sales reps belongs to distributor
	
	HashMap hmRep = new HashMap();
	
	for (int i=0;i<alAssocReps.size();i++)
	{
		hmRep = (HashMap)alAssocReps.get(i);
		
	%>
		AssocRepsArr[<%=assocrepCnt%>] ="<%=hmRep.get("ASSOCREPNM")%>,<%=hmRep.get("ASSOCREPID")%>,<%=hmRep.get("REPID")%>,<%=hmRep.get("REPNM")%>";
	<%
	assocrepCnt++;
	
	}
	%>
</script>

</HEAD>
<Body leftmargin="20" topmargin="10" onLoad="javascript:fnOnPageLoad();">
<html:form action ="/gmLoanerTransferAction.do">
<html:hidden property="strOpt" value=""/>
<html:hidden property="haction" value="<%=haction%>"/>
<html:hidden property="strInputString" value=""/>
<html:hidden property="fromDistName" value=""/>
<html:hidden property="toDistName" value=""/>

		<table border="0" class="<%=strTable%>" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="3">&nbsp;<%=strHeader%><fmtLoanerTransfer:message key="LBL_LOANER_TRANSFER"/>  </td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			<fmtLoanerTransfer:message key="IMG_HELP" var="varHelp"/> 
			<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle(strWikiTitle)%>');" />
		    </td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="4"></td></tr>
		
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr class="Shade">
			<td height="25" class="RightTableCaption" align="right"><fmtLoanerTransfer:message key="LBL_FROM"/>:</td>
			<td>&nbsp;
			<logic:equal property="haction" value="INHOUSELOANER" name="frmLoanerTransfer">
				<gmjsp:dropdown controlName="fromDistributor"  SFFormName="frmLoanerTransfer" SFSeletedValue="fromDistributor" 	
						 SFValue="alFromDistributor" codeId = "ID"  codeName = "NAME" defaultValue= "[Choose One]"  />
			</logic:equal>
			<logic:notEqual property="haction" value="INHOUSELOANER" name="frmLoanerTransfer">
				<gmjsp:dropdown controlName="fromDistributor"  SFFormName="frmLoanerTransfer" SFSeletedValue="fromDistributor" 	
							 SFValue="alFromDistributor" codeId = "ID"  codeName = "NAME" defaultValue= "[Choose One]"  />
			</logic:notEqual>										
			</td>
			<td height="25" class="RightTableCaption" align="right"><fmtLoanerTransfer:message key="LBL_TO"/>:</td>
			<td>&nbsp;
			<logic:notEqual property="haction" value="INHOUSELOANER" name="frmLoanerTransfer">
				<gmjsp:dropdown controlName="toDistributor" SFFormName="frmLoanerTransfer" SFSeletedValue="toDistributor" 	
						 SFValue="alToDistributor" codeId = "ID"  codeName = "NAME" defaultValue= "[Choose One]"  />
			</logic:notEqual>
			<logic:equal property="haction" value="INHOUSELOANER" name="frmLoanerTransfer">	
			        <gmjsp:dropdown controlName="toDistributor" SFFormName="frmLoanerTransfer" SFSeletedValue="toDistributor" 	
						 SFValue="alToDistributor" codeId = "ID"  codeName = "NAME" defaultValue= "[Choose One]"  />
			</logic:equal>
			&nbsp;&nbsp;
			<fmtLoanerTransfer:message key="BTN_LOAD" var="varLoad"/>
			<gmjsp:button value="${varLoad}" gmClass="Button" onClick="fnLoad();" buttonType="Load" />		 										
			</td>
		</tr>
		<tr>
           	<td colspan="4">
				<div id="LoanerData" style="grid" height="700px"></div>
		    </td>	
		</tr>
		<%if(gridData.indexOf("cell") != -1) {%>
		<tr> <td colspan="4" height="50" align="center">	
		<fmtLoanerTransfer:message key="BTN_SUBMIT" var="varSubmit"/>
		<gmjsp:button value="${varSubmit}" gmClass="Button" onClick="fnSubmit();" buttonType="Save" />
		</td></tr>
		<%}%>
	 </table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
