<?xml version="1.0" encoding="ISO-8859-1" ?>
<%
/**********************************************************************************
 * File		 		: GmControlNumberExceptionReport.jsp
 * Desc		 		: This screen is used for the displaying part number details
 * Version	 		: 1.1
 * author			: Karthik
 * 
************************************************************************************/
%>
<%@include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtControlNumberExceptionReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<!-- operations\GmControlNumberExceptionReport.jsp -->

<fmtControlNumberExceptionReport:setLocale value="<%=strLocale%>"/>
<fmtControlNumberExceptionReport:setBundle basename="properties.labels.operations.GmControlNumberExceptionReport"/>

<%
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	String strWikiTitle = GmCommonClass.getWikiTitle("CONTROL_NUMBER_EXCEPTION_REPORT");
	//The following code added for passing the company info to the child js fnFilterLoad function
	String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
%>
<head>
<title>Control Number </title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script type="text/javascript" src="<%= strOperationsJsPath%>/GmControlNumberException.js"></script>

<script language="JavaScript" 
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
	<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<bean:define id="gridData" name="frmControlNumberException" property="strXmlData" type="java.lang.String"></bean:define>
<bean:define id="prodType" name="frmControlNumberException" property="prodType" type="java.lang.String"></bean:define>
<bean:define id="donorNM"  name="frmControlNumberException"  property="donorNM"  type="java.lang.String"></bean:define>
<bean:define id="vendorId" name="frmControlNumberException" property="vendorId" type="java.lang.String"></bean:define>

<script>
<%String strApplDateFmt = strGCompDateFmt;%>
var format = '<%=strApplDateFmt%>';
var gridObjData ='<%=gridData%>';
var mygrid;  
var prodType = '<%=prodType%>';
var donorNM = '<%=donorNM%>';
var vendorId = '<%=vendorId%>';
var companyInfoObj = '<%=strCompanyInfo%>';
</script>
</head>
<body leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<html:form action="/gmControlNumber.do" method="post" styleId="form">
<html:hidden property="strOpt" name="frmControlNumberException"/>
<html:hidden name="frmControlNumberException" property="ctrlAsd_ID" />
<html:hidden property="prodType"  name="frmControlNumberException" />

<table cellspacing="0" cellpadding="0" class="DtTable700">
	<tr>
		<td colspan="3" height="25" class="RightDashBoardHeader"><fmtControlNumberExceptionReport:message key="LBL_NO_OVERRIDE_REPORT"/></td>
		<td align="right" class="RightDashBoardHeader"><fmtControlNumberExceptionReport:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' style='cursor: hand'
				src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
	</tr>
	<tr class="Line"></tr>
	<tr class="shade">
	<td align="right" height="30">
	<b><fmtControlNumberExceptionReport:message key="LBL_PART_#"/>:&nbsp;</b>
	<html:text property="partNumber" style="text-transform: uppercase;" name="frmControlNumberException"/>
	</td>
	<td align="right">
	<b><fmtControlNumberExceptionReport:message key="LBL_PART_DESCRIPTION"/>:&nbsp;</b></td>
	<td><html:text property="partDesc" name="frmControlNumberException"/>
	</td><td align="right"></td></tr>
    <tr class="Line"></tr>
	<tr>
	<td align="right" height="30">
	<b><fmtControlNumberExceptionReport:message key="LBL_LOT_NUMBER"/>:&nbsp;</b>
	<html:text property="controlNM" style="text-transform: uppercase;" name="frmControlNumberException"/>
	</td>
     <td align="right" height="30" >&nbsp;<b><fmtControlNumberExceptionReport:message key="LBL_FROM"/> :</b>
     <gmjsp:calendar SFFormName="frmControlNumberException" controlName="fromDt"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff'); " />
     </td>
	 <td align="right" height="30" >&nbsp;<b><fmtControlNumberExceptionReport:message key="LBL_TO"/> :</b>
     <gmjsp:calendar SFFormName="frmControlNumberException" controlName="toDt"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff'); " />
     </td>
	<td align="center"><fmtControlNumberExceptionReport:message key="BTN_LOAD" var="varLoad"/>
	<gmjsp:button name="Btn_Load" value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" gmClass="button" onClick="fnReLoad();" buttonType="Load" />
	</td>
	</tr>
		<tr class="Line"></tr>
	<%if(gridData.indexOf("cell") != -1) {%> 
		<tr>
			<td colspan="4">
			<div id="dataGridDiv" width="700px" height="500px"></div>
			</td>
		</tr>
		<tr>
	         <td colspan="4" align="center">
	          <div class='exportlinks'><fmtControlNumberExceptionReport:message key="DIV_EXPORT_OPT"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
	               onclick="mygrid.toExcel('/phpapp/excel/generate.php');"> <fmtControlNumberExceptionReport:message key="DIV_EXCEL"/> </a></div>
	           </td>
		</tr>
	<%} else  if(!gridData.equals("")){%>
			<tr><td colspan="4" align="center" class="RightText" ><fmtControlNumberExceptionReport:message key="MSG_NO_DATA"/></td></tr>
	<%} %>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>