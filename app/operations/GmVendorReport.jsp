<%@ page import ="com.globus.common.beans.GmCommonControls"%>
 <%
/**********************************************************************************
 * File		 		: GmVendorSetup.jsp
 * Desc		 		: This screen is used for the Vendor Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ include file="/common/GmHeader.inc" %> 
<!-- operations\GmVendorReport.jsp -->
<%@ taglib prefix="fmtVendorReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtVendorReport:setLocale value="<%=strLocale%>"/>
<fmtVendorReport:setBundle basename="properties.labels.operations.GmVendorReport"/>
<%

	response.setCharacterEncoding("UTF-8");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strWikiTitle = GmCommonClass.getWikiTitle("VENDOR_REPORT");
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmParam = (HashMap)request.getAttribute("hmParam");
	ArrayList alReturn = (ArrayList)request.getAttribute("alReturn");
	ArrayList alType = new ArrayList();
	ArrayList alStatus = new ArrayList();
	ArrayList alFamily = new ArrayList();
	if (hmReturn != null)
	{
		alType = (ArrayList)hmReturn.get("TYPE");
		alStatus = (ArrayList)hmReturn.get("STATUS");
		alFamily = (ArrayList)hmReturn.get("PFLY");
	}
	
	HashMap hmMap = new HashMap();
	hmMap.put("ID","");
	hmMap.put("PID","CODEID");
	hmMap.put("NM","CODENM");	
	
	String strGridData = GmCommonClass.parseNull((String)request.getAttribute("GRIDXMLDATA"));
	log.debug("strGridData"+strGridData);
	String strTypeIds = "";
	String strCatIds = "";
	String strStatIds = "";
	
	if (hmParam != null)
	{
		strTypeIds = (String)hmParam.get("TYPE");
		strCatIds = (String)hmParam.get("CATEGORY");
		strStatIds = (String)hmParam.get("STATUS");
	}
	String strOperationsJsPath = GmCommonClass.parseNull((String)GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS"));
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Vendor Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid_form.js "></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmVendorReport.js"></script>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
  </style>   
<script>
var objGridData;
objGridData = '<%=strGridData%>';
strServletPath = '<%=strServletPath%>';
var typeids = '<%=strTypeIds%>';
var catids = '<%=strCatIds%>';
var status = '<%=strStatIds%>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10"  onload="javascript:fnLoad();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmOperReportServlet">
<input type="hidden" name="hColumn" value="">
<input type="hidden" name="hId" value="">
<input type="hidden" name="hPgToLoad" value="GmVendorServlet">
<input type="hidden" name="hSubMenu" value="Setup">
<input type="hidden" name="hTypeIds" value="">
<input type="hidden" name="hCatIds" value="">
<input type="hidden" name="hStatIds" value="">
<input type="hidden" name="hAction" value="">

<table border="0" class="DtTable1500" cellspacing="0" cellpadding="0">
	<tr>
		<td height="25" class="RightDashBoardHeader">&nbsp;<fmtVendorReport:message key="LBL_VENDOR_REPORT"/></td>
		<td  height="25" class="RightDashBoardHeader">
						<fmtVendorReport:message key="IMG_HELP" var = "varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>
	</tr>
	<tr>
		<td>
			<table id="tabFilter" cellspacing="0">
				<tr height="18" bgcolor="#eeeeee" class="RightTableCaption">
					<td><fmtVendorReport:message key="LBL_TYPE"/></td>
					<td><fmtVendorReport:message key="LBL_CATEGORY"/></td>
					<td><fmtVendorReport:message key="LBL_STATUS"/></td>
					<td bgcolor="white" width="100%" rowspan="2" valign="middle">&nbsp;&nbsp;&nbsp;&nbsp;
						<fmtVendorReport:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" gmClass="button" onClick=" return fnReload();" tabindex="2" buttonType="Load" />
					</td>
				</tr>
				<tr>
					<td><%=GmCommonControls.getChkBoxGroup("",alType,"Type",hmMap)%></td>
					<td><%=GmCommonControls.getChkBoxGroup("",alFamily,"Cat",hmMap)%></td>
					<td><%=GmCommonControls.getChkBoxGroup("",alStatus,"Stat",hmMap)%></td>
				</tr>
			</table>		
		</td>
	</tr>
	<tr>
		<td colspan= "2">		
		    <div id="dataGridDiv" class="grid" height="500px"></div>
		</td>
		<tr>						
				<td align="center" colspan="8"><br>
	             			<div class='exportlinks'><fmtVendorReport:message key="LBL_EXCEL"/>: <img src='img/ico_file_excel.png' />&nbsp; <a href="#" onclick="fnDownloadXLS();"><fmtVendorReport:message key="LBL_EXCEL"/>  </a></div>                         
		</td>
	</tr>
	</tr> 
</table>
	<%@ include file="/common/GmFooter.inc"%>
</FORM>

</BODY>

</HTML>
