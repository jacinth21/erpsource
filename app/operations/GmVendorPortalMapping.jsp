<%
/**********************************************************************************
 * File		 		: GmVendorPortalMapping.jsp
 * Desc		 		: Vendor Portal Mapping
 * Version	 		: 1.0
 * author			: RAJA
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ taglib prefix="fmtVendorPortalMapping" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtVendorPortalMapping:setBundle basename="properties.labels.operations.GmVendorPortalMapping"/>
<bean:define id="gridData" name="frmVendorPortalMap" property="gridData" type="java.lang.String"/>
<bean:define id="alVendorMapList" name="frmVendorPortalMap" property="alVendorMapList" type="java.util.ArrayList"/>

<!-- GmVendorPortalMapping.jsp -->

<%
String stroperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
String strWikiTitle = GmCommonClass.getWikiTitle("VENDOR_PORTAL_MAPPING");
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Vendor Mapping Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">

<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=stroperationsJsPath%>/GmVendorPortalMapping.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script> 
<script type="text/javascript">
var objGridData = '<%=gridData%>';
var partyAlLen = '<%=alVendorMapList.size()%>';
<% 
HashMap hcboVal = new HashMap();
for (int i=0;i<alVendorMapList.size();i++)
{
	hcboVal = (HashMap)alVendorMapList.get(i);
%>
var RepArr<%=i%> ="<%=hcboVal.get("TOPARTYID")%>";
<%
}
%>
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<html:form action="/gmVendorPortalMapping.do?method=fetchVendorPortalMap">
<html:hidden name="frmVendorPortalMap" property="fromVendorId"  />

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="4">&nbsp;<fmtVendorPortalMapping:message key="LBL_VEN_PORTAL_MAP"/></td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />&nbsp;
	       </td>
		</tr>
		<tr>
			<td bgcolor="#666666" height="1" colspan="5"></td>
		</tr>
		<tr>
			<td width="698"  valign="top" colspan="5">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">  
				    <tr><td class="ELine" colspan="5"></td></tr>
                    <tr>
                    <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtVendorPortalMapping:message key="LBL_VEN_SEARCH"/> :</td> 
                    <td>
	                     <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
						<jsp:param name="CONTROL_NAME" value="toPartyID"/>
						<jsp:param name="METHOD_LOAD" value="loadVendorList" />
						<jsp:param name="WIDTH" value="300" />
						<jsp:param name="CSS_CLASS" value="search" />
						<jsp:param name="TAB_INDEX" value="4"/>
						<jsp:param name="CONTROL_NM_VALUE" value="" /> 
						<jsp:param name="CONTROL_ID_VALUE" value="" />
						<jsp:param name="SHOW_DATA" value="100" />
						<jsp:param name="AUTO_RELOAD" value="fnSave();" />					
						</jsp:include>
                     </td> 
                    </tr>       
	    <tr>
			<td class="Line" height="1" colspan="5"></td>
		</tr> 
		
		<%
			  if (gridData.indexOf("cell") != -1) {
			%>

			<tr>
				<td colspan="5">
					<div id="dataGridDiv" class="" height="400px"></div>
				</td>
			</tr>

			<tr>
				<td colspan="5" align="center">
					<div class='exportlinks'>
						<fmtVendorPortalMapping:message key="LBL_EXPORT_OPTIONS" />
						: <img src='img/ico_file_excel.png' onclick="fnExportExcel();"/>&nbsp;<a href="#"
							onclick="fnExportExcel();"><fmtVendorPortalMapping:message
								key="LBL_EXCEL" /> </a>
					</div>
				</td>
			</tr>
				 <tr>
			<td class="LLine" height="2" colspan="5"></td>
		</tr>
				<tr align="center">
			<td height="30" colspan="5">
			<gmjsp:button value="Close" name="Btn_Close" gmClass="button" buttonType="Load" onClick="javascript:fnClose();" /></td>
	   			</tr>
			<%
			  } else {
			%>
			<tr>
				<td class="LLine" colspan="5" height="1"></td>
			</tr>
			<tr height="30">
				<td colspan="5" align="center"><fmtVendorPortalMapping:message
						key="LBL_NOTHING_FOUND_TO_DISPLAY" /></td>
			</tr>
			<%
			  }
			%>
		



	</table>
	</td>
	</tr>
	</table>
	</html:form>
	 
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>