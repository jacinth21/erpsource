 <%
/**********************************************************************************
 * File		 		: GmOrderPlan.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@page import="java.net.URLEncoder"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- Imports for Logger -->

<%@ page import="com.globus.common.beans.GmResourceBundleBean" %> 

<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ taglib prefix="fmtOrderPlan" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtOrderPlan:setLocale value="<%=strLocale%>"/>
<fmtOrderPlan:setBundle basename="properties.labels.operations.GmOrderPlan"/>

<!-- operations\GmOrderPlan.jsp -->
<%
	response.setCharacterEncoding("UTF-8");
	String strWikiTitle = GmCommonClass.getWikiTitle("RAISE_PURCHASE_ORDER"); 
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	String strType = GmCommonClass.parseNull((String)request.getAttribute("hType"));
	String strSearch = GmCommonClass.parseNull((String)request.getAttribute("hSearch"));
	String strVendorId = GmCommonClass.parseNull((String)request.getAttribute("hVendId"));
	String strPartNums = GmCommonClass.parseNull((String)request.getAttribute("hPartNum"));
	String strQtyFlag = GmCommonClass.parseNull((String)request.getAttribute("hQtyFlag"));
	String strViaCellCompFL = GmCommonClass.parseNull((String)request.getAttribute("VIACELLCOMPFL"));
	
	String strWODCOCompfl = GmCommonClass.parseNull((String)request.getAttribute("WODCO_VLDN_COMPANY_FL"));
	String strWODCOValidnPOType = GmCommonClass.parseNull((String)request.getAttribute("WODCO_VLDN_PO_TYPE"));
	
	
	strQtyFlag = strQtyFlag.equals("Y")?"checked":"";
	String strProjId =	"";
	String strPartNum = "";
	String strPartDesc = "";
	String strVendorNm = "";
	String strCostPrice = "";
	String strCostPriceWithComma = "";
	String strQtyQuoted = "";
	String strUOM = "";
	String strUOMQty = "";
	String strPriceId = "";
	String strValidateFl = "";
	String strVerifyFl = "";
	int intTextBoxCount = 0;
	String strTemp = "";
	ArrayList alProjList = new ArrayList();
	ArrayList alList = new ArrayList();
	ArrayList alPoType = new ArrayList();
	int intSize = 0;
	String strRev = "";
	String strOrdPlanQty = "";
	String strPartStatus = "";
	String strStatusId = "";
	String strRevTemp = "";
	String strVendorCurrency = "";
	String strDisabled = "";
	String strTitle = "";
	String strWODCO = "";
	String strWODCOColor = "";
	
	GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.operations.GmOrderPlan", strSessCompanyLocale);
	
	String strPoType = GmCommonClass.parseNull((String)request.getAttribute("hPoType"));
	alPoType = GmCommonClass.parseNullArrayList((ArrayList)session.getAttribute("alPOTYPE"));
	ArrayList alVendorList = new ArrayList();

	if (strhAction.equals("Load") || strhAction.equals("PriceLoad"))
	{
		alProjList = GmCommonClass.parseNullArrayList((ArrayList)session.getAttribute("PROJECTS"));
		strProjId = (String)request.getAttribute("hProjId")== null?"":(String)request.getAttribute("hProjId");
		alVendorList = GmCommonClass.parseNullArrayList((ArrayList)session.getAttribute("VENDORLIST"));
	}

	if(strPoType.equals("3100")){		
		strDisabled = "";
		strTitle     = "";
	}else{		
		strDisabled = "disabled";
		strTitle     = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_AVAILABLE_FOR_PO_TYPE_IS_PRODUCT"));
	}
	HashMap hcboVal = null;
	String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Order Planning </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/OrderPlan.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<script>

function fnCallProject(val)
{
	// document.frmVendor.hAction.value = 'PriceLoad';
  	document.frmVendor.submit();
}

function fnGo()
{
	var potype = document.frmVendor.Cbo_PoType.value
	if (potype == '3101')
			{
				 Error_Details(message[801]);
			}
	
	document.frmVendor.hAction.value = 'PriceLoad';
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	fnStartProgress();
	document.frmVendor.submit();

}

function fnCallHistory(pnum)
{
	windowOpener("/GmOrderPlanServlet?hAction=ViewHistory&hPO="+pnum,"POHistory","resizable=yes,scrollbars=yes,top=350,left=280,width=600,height=300");
}

function fnLoad()
{
	var sel = document.frmVendor.hSearch.value;
	if (sel != '')
	{
		document.frmVendor.Cbo_Search.value = sel;
	}
}
var viaCellCompFlag = '<%=strViaCellCompFL%>';
var wodco_vldn_company = '<%=strWODCOCompfl%>';
var wodco_vldn_potype = '<%=strWODCOValidnPOType%>';

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10"  onLoad="javascript:fnLoad();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmOrderPlanServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<INPUT type="hidden" name="hPOString" value="">
<INPUT type="hidden" name="hWOString" value="">
<INPUT type="hidden" name="hSearch" value="<%=strSearch%>">

	<table border="0" class="DtTable950" cellspacing="0" cellpadding="0"><tr>
			<td colspan="5" height="25" class="RightDashBoardHeader">&nbsp;<fmtOrderPlan:message key="LBL_RAISE_PUR_ORD"/></td>
				<td height="25" class="RightDashBoardHeader" style="display: none;">
				<fmtOrderPlan:message key="IMG_HELP" var = "varHelp"/><img align="right"
					id='imgEdit' style='cursor: hand' src='<%=strExtWebPath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			
		</tr>
		<tr>
			<td height="100" valign="top" colspan="6">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="6">
						<!-- Custom tag lib code modified for JBOSS migration changes -->
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr> 
									<td class="RightTableCaption" HEIGHT="30" align="right">&nbsp;<fmtOrderPlan:message key="LBL_PRO_NAME"/>:	</td>
									<td colspan="2">&nbsp;<gmjsp:dropdown controlName="hProjId"  seletedValue="<%= strProjId %>" 	
									width="350" value="<%= alProjList%>" codeId = "ID" codeName = "IDNAME" defaultValue= "[Choose One]" tabIndex="1" /></td>
									<td class="RightTableCaption" align="right"><fmtOrderPlan:message key="LBL_PO_TYPE"/>: </td>
									<td colspan="2">&nbsp;<gmjsp:dropdown controlName="Cbo_PoType"  seletedValue="<%= strPoType %>" 	
									width="250" value="<%= alPoType%>" codeId = "CODEID" codeName = "CODENM" defaultValue= "[Choose One]" tabIndex="2" /></td>
								</tr>
								<tr>
									<td colspan="6" class="ELine" height="1"></td>
								</tr>
								<tr>
									<td class="RightTableCaption" HEIGHT="24" align="right">&nbsp;<fmtOrderPlan:message key="LBL_PART_NUM"/>: </td>
									<td>&nbsp;<input type="text" size="47" value="<%=strPartNums%>" name="Txt_PartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="3" ></td>
									<td>&nbsp;<select name="Cbo_Search" class="RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="4">
												<option value="0" ><fmtOrderPlan:message key="LBL_CHOOSE"/>
												<option value="LIT" ><fmtOrderPlan:message key="LBL_LITREAL"/>
												<option value="LIKEPRE" ><fmtOrderPlan:message key="LBL_LK_PRE"/>
												<option value="LIKESUF" ><fmtOrderPlan:message key="LBL_LK_SUF"/>
											</select>
									</td>
									<td class="RightTableCaption" align="right"><fmtOrderPlan:message key="LBL_VENDORS"/>:</td><td>&nbsp;<gmjsp:dropdown controlName="Cbo_VendId"  seletedValue="<%=strVendorId%>" 	
									width="250" value="<%= alVendorList%>" codeId = "ID" codeName = "NAME" defaultValue= "[Choose One]" tabIndex="5" />	</td>
									<td height="30"><fmtOrderPlan:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="fnGo();" tabindex="7" buttonType="Load" /></td>
								</tr>
								<tr>
									<td colspan="6" class="ELine" height="1"></td>
								</tr>
								<tr>
									<td align="right" class="RightTableCaption" HEIGHT="24"><fmtOrderPlan:message key="LBL_QTY_TO_ORDER"/>:</td><td>&nbsp;<input type="checkbox" name="Chk_QtyToOrder" <%=strQtyFlag %> tabindex="6">	</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="3" class="Line" height="1"></td>
					</tr>
					<TR>
						<td colspan="3">
							<div style="overflow:auto; height:400px;">
							<TABLE cellpadding="1" width="1100" cellspacing="0" border="0">
							<thead>
								<TR class="ShadeRightTableCaption" style="position:relative; top:expression(this.offsetParent.scrollTop);">
									<TD class="RightText" width="60"><fmtOrderPlan:message key="LBL_PART"/></TD>
									<TD class="RightText" width="450" align="center"><fmtOrderPlan:message key="LBL_PART_DESC"/></TD>
									<TD class="RightText" width="60" align="center"><fmtOrderPlan:message key="LBL_STATUS"/></TD>
									<TD class="RightText" width="60" align="center"><fmtOrderPlan:message key="LBL_REV_LEVEL"/><BR></TD>
									<TD class="RightText" align="center" width="60"><fmtOrderPlan:message key="LBL_WO_DCO"/></TD>
									<TD class="RightText" align="center" width="60"><fmtOrderPlan:message key="LBL_UOM"/></TD>
									<TD class="RightText" align="center" width="60"><fmtOrderPlan:message key="LBL_UOM_QTY"/><BR></TD>
									<TD class="RightText" width="150" align="center"><fmtOrderPlan:message key="LBL_VENDOR"/></TD>
									<TD class="RightText" width="20"><fmtOrderPlan:message key="LBL_CURR"/></TD>
									<TD class="RightText" align="center" width="50"><fmtOrderPlan:message key="LBL_COST"/></TD>
									<TD class="RightText" align="center" width="60"><fmtOrderPlan:message key="LBL_QTY_QUOTED"/><br></TD>
									<TD class="RightText" align="center" width="60"><fmtOrderPlan:message key="LBL_QTY_ORDER"/><BR></TD>
									<TD class="RightText" align="center" width="50"><fmtOrderPlan:message key="LBL_QTY_PLACE"/></TD>
									<TD class="RightText" align="center" width="80"><fmtOrderPlan:message key="LBL_CRICTICAL"/></TD>
									<TD class="RightText" align="center" width="80"><fmtOrderPlan:message key="LBL_FAR"/></TD>
									<TD class="RightText" align="center" width="80"><fmtOrderPlan:message key="LBL_VALDI"/>?</TD>
									<TD class="RightText" align="center" width="50" ><fmtOrderPlan:message key="LBL_LOTS"/></TD>
									<TD class="RightText" align="center" width="80"><fmtOrderPlan:message key="LBL_VALIDATED"/></TD>
									<TD class="RightText" align="center" width="80"><fmtOrderPlan:message key="LBL_VERIFIED"/></TD>
								</TR>
								<tr style="position:relative; top:expression(this.offsetParent.scrollTop);"><td colspan="18" class="Line" height="1"></td></tr>
							</thead>
							<tbody>								
<%
					if (strhAction.equals("PriceLoad") )
					{
						alList = (ArrayList)hmReturn.get("VENDORPRICELIST");
						if (alList != null)
						{
							intSize = alList.size();
						}
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();
							HashMap hmTempLoop = new HashMap();
							String strNextPartNum = "";
							int intCount = 0;
							String strColor = "";
							strWODCOColor = "";
							String strQtyColor = "lightgreen";
							String strLine = "";
							for (int i = 0;i < intSize ;i++ )
							{
								hmLoop = (HashMap)alList.get(i);
								if (i<intSize-1)
								{
									hmTempLoop = (HashMap)alList.get(i+1);
									strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("PNUM"));
								}
								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
								strPartDesc = GmCommonClass.getStringWithTM(GmCommonClass.parseNull((String)hmLoop.get("PDESC")));
								strTemp = strPartNum;
								strOrdPlanQty = GmCommonClass.parseNull((String)hmLoop.get("AQTY"));
								strPartStatus = GmCommonClass.parseNull((String)hmLoop.get("STATUS"));
								strStatusId = GmCommonClass.parseNull((String)hmLoop.get("SID"));
								strRev = GmCommonClass.parseNull((String)hmLoop.get("DRAWREV"));
								strRevTemp = strRev; 
								strUOM = GmCommonClass.parseNull((String)hmLoop.get("UOM"));
								strUOMQty = GmCommonClass.parseNull((String)hmLoop.get("UOMQTY"));
								strWODCO =  GmCommonClass.parseNull((String)hmLoop.get("WO_DCO"));
								
								strWODCOColor = "";
								if (strWODCO.equalsIgnoreCase("No"))
									strWODCOColor = "FFCCCB"; //light red
								
								if (strPartNum.equals(strNextPartNum))
								{
									intCount++;
								}
								else
								{
									if (intCount > 0)
									{
										strPartNum = "";
										strPartDesc = "";
										strOrdPlanQty = "";
										strPartStatus = "";
										strRev = "";
										strUOM = "";
										strUOMQty = "";
										strColor = "bgcolor=white";
										strQtyColor = "";
										strWODCO = "";
										strWODCOColor = "";
									}
									else
									{
										strColor = "";
									}
									intCount = 0;
									strLine = "<TR><TD colspan=13 height=1 bgcolor=#eeeeee></TD></TR>";
								}

								if (intCount > 1)
								{
									strPartNum = "";
									strPartDesc = "";
									strOrdPlanQty = "";
									strPartStatus = "";
									strRev = "";
									strUOM = "";
									strUOMQty = "";
									strColor = "bgcolor=white";
									strQtyColor = "";
									strWODCO = "";
									strWODCOColor = "";
								}

								strVendorNm = GmCommonClass.parseNull((String)hmLoop.get("VNAME"));
								strVendorId = GmCommonClass.parseNull((String)hmLoop.get("VID"));
								strVendorCurrency =  GmCommonClass.parseNull((String)hmLoop.get("VENDCURR"));
								strCostPrice = GmCommonClass.parseNull((String)hmLoop.get("CPRICE"));
								strCostPriceWithComma = strCostPrice == ""?"":GmCommonClass.getStringWithCommas(strCostPrice);
								strQtyQuoted = GmCommonClass.parseNull((String)hmLoop.get("QTYQ"));
								strPriceId = GmCommonClass.parseNull((String)hmLoop.get("PID"));
								strValidateFl = GmCommonClass.parseNull((String)hmLoop.get("VALIDFL"));
								strVerifyFl  = GmCommonClass.parseNull((String)hmLoop.get("VERIFL"));
								
%>
											<TR id="tr<%=i%>">
												<td width="60" <%=strColor%>><a class="RightText" href="javascript:fnCallHistory('<%=strPartNum%>');"><%=strPartNum%></a>
													<INPUT type="hidden" name="hPartNum<%=i%>" value="<%=strTemp%>">
													<INPUT type="hidden" name="hVend<%=i%>" value="<%=strVendorId%>">
													<INPUT type="hidden" name="hCost<%=i%>" value="<%=strCostPrice%>">
													<INPUT type="hidden" name="hRev<%=i%>" value="<%=strRevTemp%>">
													<INPUT type="hidden" name="hPid<%=i%>" value="<%=strPriceId%>">
													<INPUT type="hidden" name="hSid<%=i%>" value="<%=strStatusId%>">
													<INPUT type="hidden" name="hWODCOID<%=i%>" value="<%=GmCommonClass.parseNull((String)hmLoop.get("WO_DCO_ID"))%>">
												</td>
												<td <%=strColor%> nowrap class="RightTextAS">&nbsp;<%=strPartDesc%></td>
												<td <%=strColor%> class="RightText" align="center"><%=strPartStatus%></td>
												<td <%=strColor%> class="RightText" align="center"><%=strRev%></td>
												<td bgcolor="<%=strWODCOColor%>" class="RightText" align="center"><%=strWODCO%></td>
												<td class="RightText" align="center"><%=strUOM%></td>
												<td class="RightText" align="center"><%=strUOMQty%></td>
												<td class="RightText"><%=strVendorNm%></td>
												<td class="RightText" align="center"><%=strVendorCurrency%></td>
												<td class="RightText" align="right"><%=strCostPriceWithComma%></td>
												<td class="RightText" align="center"><%=strQtyQuoted%></td>
												<td class="RightText" align="center" bgcolor="<%=strQtyColor%>"><%=strOrdPlanQty%></td>
<%
									intTextBoxCount++;	
									strQtyColor = "lightgreen";
									if (strVendorNm.equals(""))
									{
%>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
<%
									}else{	
%>
												<td align="right"><input type="text" size="6" class="InputArea" id="<%=strTemp%>" name="Txt_Qty<%=i%>" onFocus="changeTRBgColor(<%=i%>,'#AACCE8');" onBlur="changeTRBgColor(<%=i%>,'#ffffff');"></td>
												<td align="center"><input type="Checkbox" name="Chk_Flag<%=i%>"></td>
												<td align="center"><input type="Checkbox" name="Chk_FAR<%=i%>"></td>
												<td align="center"><input <%=strDisabled%> type="Checkbox" name="Validate_Flag<%=i%>" title="<%=strTitle%>" onclick="fnEnableLots(<%=i%>);"></td>
												<td align="right"><input type="text" disabled="disabled" size="6" class="InputArea" maxlength="10" id="<%=strTemp%>" title="<%=strTitle%>" name="Lots_No<%=i%>" onFocus="changeTRBgColor(<%=i%>,'#AACCE8');" onBlur="changeTRBgColor(<%=i%>,'#ffffff');"></td>
												<td class="RightText" align="center" <%=strDisabled%> title="<%=strTitle%>" ><%=strValidateFl%></td>
												<td class="RightText" align="center" title="<%=strTitle%>" <%=strDisabled%>><%=strVerifyFl%></td>
<%
									}
%>
											</TR>
<%
							out.print(strLine);
							}
%>
										</tbody>
									</table>
								</DIV>
								</td>
					</tr>
					<tr><td colspan="13" class="Line" height="1"></td></tr>
					<tr>
									<td colspan="13" class="RightTextRed" align="center" height="30"> 
									 <fmtOrderPlan:message key="LBL_PLZ_SUBMIT"/>
										<INPUT type="hidden" name="hTextboxCnt" value="<%=intTextBoxCount%>">
										<fmtOrderPlan:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="&nbsp;${varSubmit}&nbsp;" controlId="Btn_Submit" gmClass="button" onClick="fnSubmit();" tabindex="8" buttonType="Save" />&nbsp;	
									</td>
								</tr>
<%
						}else{
%>
								<tr><td colspan="18" class="RightTextRed" align="center" height="25"> <fmtOrderPlan:message key="LBL_NOTHING_FOUND"/>.</td></tr>
<%
						}
					}
%>
								
							</TABLE>
						</TD>
				   </TR>
				</TABLE>
</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
