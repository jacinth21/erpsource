 <%
/**********************************************************************************
 * File		 		: GmDHRPrintReviewFormD.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: 
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%> 


<%@ taglib prefix="fmtDHRPrintReviewFormD" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\GmDHRPrintReviewFormD.jsp -->

<fmtDHRPrintReviewFormD:setLocale value="<%=strLocale%>"/>
<fmtDHRPrintReviewFormD:setBundle basename="properties.labels.operations.GmDHRPrintReviewFormD"/>

<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	strCssPath = GmCommonClass.getString("GMSTYLES");
	strImagePath = GmCommonClass.getString("GMIMAGES");
	strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	strCommonPath = GmCommonClass.getString("GMCOMMON");
	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmDHRDetails = new HashMap();
	ArrayList alSubDHR = new ArrayList(); 
	String strDHRId = "";
	String strVendorId = "";
	String strVendName = "";
	String strPartNum = "";
	String strDesc = "";
	String strWOId = "";
	String strControlNum = "";
	String strQtyRec = ""; 
	String strManufDate = ""; 
	String strCreatedDate = ""; 
	String strProjId = "";  
	String strSubPartNum = "";
	String strSubPartDesc = "";
	String strSubControl = "";
	String strSubManfDt = "";  
	String strDonorNum = "";
	String strExpirydt = "";
	String strDRFooter = "";
	String strProdType = "";
	String strApplnDateFmt = strGCompDateFmt;
	if (hmReturn != null)
	{
		hmDHRDetails = (HashMap)hmReturn.get("DHRDETAILS");

		strDHRId = GmCommonClass.parseNull((String)hmDHRDetails.get("ID"));
		strVendorId = GmCommonClass.parseNull((String)hmDHRDetails.get("VID"));
		strVendName = GmCommonClass.parseNull((String)hmDHRDetails.get("VNAME"));
		strPartNum = GmCommonClass.parseNull((String)hmDHRDetails.get("PNUM"));
		strDesc = GmCommonClass.parseNull((String)hmDHRDetails.get("PDESC"));
		strWOId = GmCommonClass.parseNull((String)hmDHRDetails.get("WOID"));
		strControlNum = GmCommonClass.parseNull((String)hmDHRDetails.get("CNUM"));
		strQtyRec = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYREC"));
		//strManufDate = GmCommonClass.parseNull((String)hmDHRDetails.get("MDATE")); 
		strManufDate = GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("MDATE"),strApplnDateFmt);
		//strCreatedDate = GmCommonClass.parseNull((String)hmDHRDetails.get("CDATE")); 
		strCreatedDate = GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("CDATE"),strApplnDateFmt);
		strProjId = GmCommonClass.parseNull((String)hmDHRDetails.get("PROJID"));   
		alSubDHR = (ArrayList)hmReturn.get("SUBDHRDETAILS");  
		strDonorNum = GmCommonClass.parseNull((String)hmDHRDetails.get("DONORNUMBER"));
		strDonorNum = strDonorNum.equals("")? "N/A" : strDonorNum;
		strExpirydt = GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("EXPDT"),strApplnDateFmt);
		strExpirydt = strExpirydt.equals("")? "N/A" : strExpirydt;
		strDRFooter= GmCommonClass.parseNull((String)hmDHRDetails.get("DRFOOTER"));
		strProdType = GmCommonClass.parseNull((String)hmDHRDetails.get("PRODTYPE"));
	}
	String strHideButton = GmCommonClass.parseNull((String)request.getParameter("HideButton"));
	String strCompanyLogo = GmCommonClass.parseNull((String)request.getParameter("CompanyLogo"));
	strCompanyLogo = strCompanyLogo.equals("")?"100800":strCompanyLogo;
	
	if(strControlNum.equals("")){
		strControlNum =GmCommonClass.parseNull(GmCommonClass.getRuleValue(strProdType,"DHRLOT"));
	}
	
	int intSize = 0;
	HashMap hcboVal = null; 
	String strArr[] = strDRFooter.split("\\^");
	String strFooter = strArr[0];
	String strDocRev = strArr[1];
	String strDocActiveFl = strArr[2];
%> 
 
<HTML>
<HEAD>
<TITLE> Globus Medical: DHR Print Version </TITLE> 
 
</HEAD>

<BODY leftmargin="20" topmargin="20"  >
<FORM name="frmOrder" method="POST" action="<%=strServletPath%>/GmPOReceiveServlet">
 

	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td bgcolor="#666666" rowspan="15" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1"></td>
			<td bgcolor="#666666" colspan="4" height="1"></td>
			<td bgcolor="#666666" rowspan="15" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1"></td>
		</tr>
		<tr>
			<td height="80" width="170"><img src="<%=strImagePath%>/<%=strCompanyLogo %>.gif" width="138" height="60"></td>
			<td class="RightText" width="130">&nbsp;</td>
			<td class="RightText" width="1"bgcolor="#666666"></td>
			<td align="right" class="RightText"><font size=+2><fmtDHRPrintReviewFormD:message key="LBL_DEVICE_HISTORY_REVIEW"/>&nbsp;<br>&nbsp;</font></td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
		<tr><td bgcolor="#eeeeee" height="8" colspan="4"></td></tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
		<tr>
			<td colspan="4">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightTableCaption" height="30" align="right"><fmtDHRPrintReviewFormD:message key="LBL_PROJECT"/>:</td>
						<td class="RightText">&nbsp;<%=strProjId%></td>
						<td class="RightTableCaption" align="right"><fmtDHRPrintReviewFormD:message key="LBL_PART_NUMBER"/>:</td>
						<td class="RightText">&nbsp;<%=strPartNum%></td>
						<td class="RightTableCaption" align="right"><fmtDHRPrintReviewFormD:message key="LBL_DHR_ID"/>:</td>
						<td class="RightText">&nbsp;<%=strDHRId%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="6"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right"><fmtDHRPrintReviewFormD:message key="LBL_WORK_ORDER_ID"/>:</td>
						<td class="RightText">&nbsp;<%=strWOId%></td>
						<td class="RightText" height="30" align="right"><fmtDHRPrintReviewFormD:message key="LBL_PART_DESCRIPTION"/>:</td>
						<td colspan="3" class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
					</tr>
					<tr><td bgcolor="#666666" colspan="6"></td></tr>
					<tr><td class="RightText" bgcolor="#eeeeee" height="25" colspan="6">&nbsp;<b><fmtDHRPrintReviewFormD:message key="LBL_RECEIPTS_ROUTING_PRODUCT_TRAVELLER"/></font>:</td></tr>
					<tr><td bgcolor="#666666" colspan="6"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right"><fmtDHRPrintReviewFormD:message key="LBL_DATE_OF_MANUFACTURING"/>:</td>
						<td class="RightText">&nbsp;<%=strManufDate%></td>
						<td class="RightText" height="30" align="right"><fmtDHRPrintReviewFormD:message key="LBL_QUANTITY"/>:</td>
						<td colspan="3" class="RightText">&nbsp;<%=strQtyRec%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="6"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right"><fmtDHRPrintReviewFormD:message key="LBL_LOT_#"/>:</td>
						<td class="RightText">&nbsp;<%=strControlNum%></td>
						<td class="RightText" height="30" align="right"><fmtDHRPrintReviewFormD:message key="LBL_QUANTITY_RELEASED"/>:</td>
						<td colspan="3" class="RightText">&nbsp;</td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="6"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right"><fmtDHRPrintReviewFormD:message key="LBL_DONOR_#"/>:</td>
						<td class="RightText">&nbsp;<%=strDonorNum%></td>
						<td class="RightText" align="right"><fmtDHRPrintReviewFormD:message key="LBL_EXPIRATION_DATE"/>:&nbsp;</td>
						<td class="RightText"><%=strExpirydt%></td>
					</tr>
					<tr>
						<td class="RightText" colspan="6">
						<span class="RightText">&nbsp;<b><fmtDHRPrintReviewFormD:message key="LBL_SUB_ASSEMBLY_DETAILS"/>:</b></span><BR>
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
								<tr><td colspan="4" height="1" bgcolor="#CCCCCC"></td></tr>
								<tr>
									<td class="RightText" height="18">&nbsp;<fmtDHRPrintReviewFormD:message key="LBL_SUB_COMP_ID"/></td>
									<td class="RightText" width="400"><fmtDHRPrintReviewFormD:message key="LBL_DESCRIPTION"/></td>
									<td class="RightText" width="100"><fmtDHRPrintReviewFormD:message key="LBL_CONTROL_#"/></td>
									<td class="RightText" width="100"><fmtDHRPrintReviewFormD:message key="LBL_MANF_DATE"/></td>
								</tr>
								<tr><td colspan="4" height="1" bgcolor="#CCCCCC"></td></tr>
						<%
						  		intSize = alSubDHR.size();
								hcboVal = new HashMap();
								if ( intSize > 0)
								{
							  		for (int i=0;i<intSize;i++)
							  		{
							  			hcboVal = (HashMap)alSubDHR.get(i);
							  			strSubPartNum = (String)hcboVal.get("SUBASMBID");
										strSubPartDesc = GmCommonClass.getStringWithTM((String)hcboVal.get("PDESC"));
										strSubControl = (String)hcboVal.get("CNUM");
										strSubManfDt = GmCommonClass.getStringFromDate((java.util.Date)hcboVal.get("MDATE"),strApplnDateFmt);
						
						%>
										<tr>
											<td height="18" class="RightText">&nbsp;<%=strSubPartNum%></td>
											<td class="RightText">&nbsp;<%=strSubPartDesc%></td>
											<td class="RightText">&nbsp;<%=strSubControl%></td>
											<td class="RightText">&nbsp;<%=strSubManfDt%></td>
										</tr>
										<tr><td colspan="4" height="1" bgcolor="#eeeeee"></td></tr>
						<%
									}
								}else{
						%>
								<tr><td colspan="4" height="30" align="center" class="RightText"><fmtDHRPrintReviewFormD:message key="LBL_NOT_APPLICABLE"/></td></tr>
						<%
									}
						%>
							</table>
						</td>
					</tr>
					<tr><td bgcolor="#666666" colspan="6"></td></tr>
					<tr><td class="RightText" bgcolor="#eeeeee" height="25" colspan="6">&nbsp;<b><fmtDHRPrintReviewFormD:message key="LBL_ACCEPTANCE_RECORDS"/></font>:</td></tr>
					<tr><td bgcolor="#666666" colspan="6"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right"><fmtDHRPrintReviewFormD:message key="LBL_INSPECTION_SHEET"/>:</td>
						<td class="RightText">&nbsp;<input type="checkbox"><fmtDHRPrintReviewFormD:message key="LBL_YES"/>&nbsp;&nbsp;<input type="checkbox"><fmtDHRPrintReviewFormD:message key="LBL_NO"/></td>
						<td colspan="2" class="RightText" height="30" align="right"><fmtDHRPrintReviewFormD:message key="LBL_CERTIFICATE_COMPLIANCE_IF_APPL"/></font>:</td>
						<td colspan="2" class="RightText">&nbsp;<input type="checkbox"><fmtDHRPrintReviewFormD:message key="LBL_YES"/>&nbsp;&nbsp;<input type="checkbox"><fmtDHRPrintReviewFormD:message key="LBL_NO"/></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="6"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right"><fmtDHRPrintReviewFormD:message key="LBL_MATERIAL_CERTIFICATION"/>:</td>
						<td class="RightText">&nbsp;<input type="checkbox"><fmtDHRPrintReviewFormD:message key="LBL_YES"/>&nbsp;&nbsp;<input type="checkbox"><fmtDHRPrintReviewFormD:message key="LBL_NO"/></td>
						<td colspan="2" class="RightText" height="30" align="right"><fmtDHRPrintReviewFormD:message key="LBL_LABELING_PACKAGING_FORM_IF_APPL"/></font>:</td>
						<td colspan="2" class="RightText">&nbsp;<input type="checkbox"><fmtDHRPrintReviewFormD:message key="LBL_YES"/>&nbsp;&nbsp;<input type="checkbox"><fmtDHRPrintReviewFormD:message key="LBL_NO"/></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="6"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right"><fmtDHRPrintReviewFormD:message key="LBL_PRODUCT_TRAVELLER"/>:</td>
						<td class="RightText">&nbsp;<input type="checkbox"><fmtDHRPrintReviewFormD:message key="LBL_YES"/>&nbsp;&nbsp;<input type="checkbox"><fmtDHRPrintReviewFormD:message key="LBL_NO"/></td>
						<td colspan="4"></td>
					</tr>
					<tr><td bgcolor="#666666" colspan="6"></td></tr>
					<tr><td class="RightText" bgcolor="#eeeeee" height="25" colspan="6">&nbsp;<b><fmtDHRPrintReviewFormD:message key="LBL_REVIEW_CHECKLIST"/></b>:</td></tr>
					<tr><td bgcolor="#666666" colspan="6"></td></tr>
					<tr>
						<td colspan="4" height="40"></td>
						<td colspan="2" class="RightText">&nbsp;&nbsp;&nbsp;&nbsp;<fmtDHRPrintReviewFormD:message key="LBL_APPLICABLE_NOT_APPLICABLE"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</tr>
					<tr>
						<td colspan="4" class="RightText" height="30" align="right"><fmtDHRPrintReviewFormD:message key="LBL_FINISHED_DEVICE_INSPECTION_PERFORMED"/>:</td>
						<td colspan="2" class="RightText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=<%=strImagePath%>/n.gif>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=<%=strImagePath%>/n.gif></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="6"></td></tr>
					<tr>
						<td colspan="4" class="RightText" height="30" align="right"><fmtDHRPrintReviewFormD:message key="LBL_COUNT_PERFORMED"/>:</td>
						<td colspan="2" class="RightText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=<%=strImagePath%>/n.gif>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=<%=strImagePath%>/n.gif></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="6"></td></tr>
					<tr>
						<td colspan="4" class="RightText" height="30" align="right"><fmtDHRPrintReviewFormD:message key="LBL_DEVICE_HISTORY_COMPLETE_ACCURATE"/>:</td>
						<td colspan="2" class="RightText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=<%=strImagePath%>/n.gif>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=<%=strImagePath%>/n.gif></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="6"></td></tr>
					<tr>
						<td colspan="4" class="RightText" height="30" align="right"><fmtDHRPrintReviewFormD:message key="LBL_PRODUCT_PACKAGE_LABEL"/>:</td>
						<td colspan="2" class="RightText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=<%=strImagePath%>/n.gif>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=<%=strImagePath%>/n.gif></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="6"></td></tr>
					<tr>
						<td colspan="4" class="RightText" height="30" align="right"><fmtDHRPrintReviewFormD:message key="LBL_CRCT_LABEL_INSERT"/>:</td>
						<td colspan="2" class="RightText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=<%=strImagePath%>/n.gif>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=<%=strImagePath%>/n.gif></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="6"></td></tr>
					<tr>
						<td colspan="4" class="RightText" height="30" align="right"><fmtDHRPrintReviewFormD:message key="LBL_WORK_OPERATION_RECEIPTS"/>:</td>
						<td colspan="2" class="RightText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=<%=strImagePath%>/n.gif>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=<%=strImagePath%>/n.gif></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="6"></td></tr>
					<tr>
						<td colspan="4" class="RightText" height="30" align="right"><fmtDHRPrintReviewFormD:message key="LBL_PACKING_SLIP"/>:</td>
						<td colspan="2" class="RightText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=<%=strImagePath%>/n.gif>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=<%=strImagePath%>/n.gif></td>
					</tr>
					<tr><td bgcolor="#666666" colspan="6"></td></tr>
				</table>
			</td>
		</tr>
	</table>
	<BR>
	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td class="RightText" bgcolor="#eeeeee" height="20" align="center"><fmtDHRPrintReviewFormD:message key="LBL_QUALITY_REQUIREMENTS"/>.</td>
		</tr>		
	</table>
	<BR>
	<BR>
	<BR>
	<BR>
	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td width="250"bgcolor="#666666" height="1"></td>
			<td height="1" width="200"></td>
			<td  width="150" bgcolor="#666666" height="1"></td>
		</tr>
		<tr>
			<td class="RightText" align="center" height="10"> <fmtDHRPrintReviewFormD:message key="LBL_AUTHORIZED_SIGN"/></td>
			<td class="RightText">&nbsp;</td>
			<td class="RightText" align="center"><fmtDHRPrintReviewFormD:message key="LBL_DATE"/></td>
		</tr>
	</table>
	<BR>
	<BR>
	<span class="RightText"><%=strFooter%></span>
	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr><td height="2"></td></tr>
		<tr>
			<td>
			<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
					<jsp:include page="/common/GmIncludeDateStamp.jsp" />
			</td>	
		</tr>
	</table>
	

</FORM>
</BODY>

</HTML>
