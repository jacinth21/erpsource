 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@page import="java.util.StringTokenizer"%>
<%
/**********************************************************************************
 * File		 		: GmInHouseItemConsignSetup.jsp
 * Desc		 		: Currently used only for BULK
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtInHouseItemConsignSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\GmInHouseItemConsignSetup.jsp -->

<fmtInHouseItemConsignSetup:setLocale value="<%=strLocale%>"/>
<fmtInHouseItemConsignSetup:setBundle basename="properties.labels.operations.GmInHouseItemConsignSetup"/>


<%
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
try {
	
	 request.setCharacterEncoding("UTF-8");
	    response.setContentType("text/html; charset=UTF-8");
	    response.setCharacterEncoding("UTF-8");
	
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
	GmResourceBundleBean gmResourceBundleBeanlbl = 
	    GmCommonClass.getResourceBundleBean("properties.labels.operations.GmInHouseItemConsignSetup", strSessCompanyLocale);
	// getting currency symbol from session
	String strCurrSign = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
	
	HashMap hmReturn = (HashMap)session.getAttribute("hmReturn");
	String strhAction = (String)session.getAttribute("hAction") == null?"":(String)session.getAttribute("hAction");
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strTemp = "";

	String strConsignId = (String)session.getAttribute("CONSIGNID") == null?"":(String)session.getAttribute("CONSIGNID");
	String strSetName = "";
	String strAccName = "";
	String strUserName = "";
	String strIniDate = "";
	String strItemQty = "";
	String strControl = "";
	String strPartNums = (String)session.getAttribute("strPartNums") == null?"":(String)session.getAttribute("strPartNums");
	String strOpt = (String)session.getAttribute("strSessOpt") == null?"":(String)session.getAttribute("strSessOpt");
	HashMap hmTransRules=GmCommonClass.parseNullHashMap((HashMap)session.getAttribute("TRANS_RULES"));
	String strAddCart = (String)session.getAttribute("ADDCARTTYPE") == null?"106457":(String)session.getAttribute("ADDCARTTYPE");
	String strRowCount = (String)session.getAttribute("CARTROWCOUNT") == null?"0":(String)session.getAttribute("CARTROWCOUNT"); 
	String strTransParts = "";
	
	String strNewPartNums = "";
	String strPartDesc = "";
	String strFlag = ""; 
	String strStockLabel = "";
	String strType = "";
	String strDistribAccId = "";
	String strPurpose = "";
	String strBillTo = "";
	String strShipTo = "";
	String strShipToId = "";
	strShipToId = (String)session.getAttribute("strSessUserId");
	String strFinalComments = "";
	String strShipFl = "";
	String strDelFl = "";
	String strHeader = "";
	String strLabel1 = "";
	String strSkipVal = "";
	//String strMessage = "";

	String strTypeRule = "";
	String strShipToRule = "";
	String strHeaderRule = "";
	String strReasonLabel = "";
	String strConsignIDLabel = "";
	String strStockFl = "";
	String strWikiTitle = "";

	ArrayList alPurpose = new ArrayList();
	ArrayList alEmpList = new ArrayList();
	ArrayList alQDList = new ArrayList();

	HashMap hmConsignDetails = new HashMap();
	HashMap hmTemp = new HashMap();
	HashMap hmParam =  new HashMap();
	HashMap hmOtherDetails =  new HashMap();
	
	boolean bolFl0 = false;

	int intPriceSize = 0;

	if (hmReturn != null)
	{
		hmTemp = (HashMap)hmReturn.get("CONLISTS");
		alPurpose = (ArrayList)hmTemp.get("PURPOSE");
		alEmpList = (ArrayList)hmTemp.get("EMPLIST");
		alQDList = (ArrayList)hmReturn.get("QDLIST");
	}

	hmParam = (HashMap)session.getAttribute("PARAM");

	if (hmParam != null)
	{
		strType = (String)hmParam.get("TYPE");
		if (strType != "")
		{
			bolFl0 = true;
		}
		strPurpose = (String)hmParam.get("PURPOSE");
		strBillTo = (String)hmParam.get("BILLTO");
		strShipTo = (String)hmParam.get("SHIPTO");
		strShipToId = GmCommonClass.parseNull((String)hmParam.get("SHIPTOID"));
		strFinalComments = (String)hmParam.get("COMMENTS");
		strShipFl = (String)hmParam.get("SHIPFL");
		strChecked = strShipFl.equals("1")?"checked":"";
		strDelFl = (String)hmParam.get("SHIPFL");
	}
	if (strOpt.equals("InHouse"))
	{
		strType = "4112";
		strShipTo = "4123";
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_CONSIGN_IHOUSE"));
		strLabel1 = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_CONSIN_TO"));
	}
	else if (strOpt.equals("Repack"))
	{
		strType = "4114";
		strShipTo = "";
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SHELF_TO_REPACKAGE"));
		strLabel1 = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_AUTHORIZED_BY"));
		//strMessage = "&nbsp;This transaction enables you to take parts from the shelf for repacking without leaving Inventory.<Br>&nbsp;Please make sure that paperwork is attached to the bin containing the parts when being processed.";
	}
	else if (strOpt.equals("QuaranIn"))
	{
		strType = "4113";
		strShipTo = "";
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SHELF_TO_QUARANTINE"));
		strLabel1 = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_AUTHORIZED_BY"));
		//strMessage = "&nbsp;This transaction enables you to remove parts from Inventory and move to the Quarantine Area. The Qty moved will reflect changes (in Qty on Shelf '-' and Qty in Quarantine '+') only after this transaction has been verified.<Br>&nbsp;Please make sure that paperwork is attached to the bin containing the parts when being processed.";
	}		
	else if (strOpt.equals("QuaranOut"))
	{
		strType = "4115";
		strShipTo = "";
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_QUARANTINE_TO_SCRAP"));
		strLabel1 = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_INITIATED_BY"));
		//strMessage = "&nbsp;This transaction enables you to remove/scrap parts from Quarantine and move it totally out of the system. The Qty moved will reflect changes (Qty in Quarantine '-') only after this transaction has been verified.<Br>&nbsp;Please make sure that paperwork is attached to the bin containing the parts when being processed.";
	}
	else if (strOpt.equals("InvenIn"))
	{
		strType = "4116";
		strShipTo = "";
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_QUARANTINE_TO_SHELF"));
		strLabel1 = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_INITIATED_BY"));
		//strMessage = "&nbsp;This transaction enables you to put back parts to the Shelf from Quarantine. The Qty moved will reflect changes (Qty in Quarantine '-' and Qty on Shelf '+') only after this transaction has been verified.<Br>&nbsp;Please make sure that paperwork is attached to the bin containing the parts when being processed.";
	}
	else if (strOpt.equals("BulkOut"))
	{
		strType = "4117";
		strShipTo = "";
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SHELF_TO_BULK"));
		strLabel1 = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_INITIATED_BY"));;
		//strMessage = "&nbsp;This transaction enables you to transfer parts from the Shelf to Bulk Shelf. The Qty moved will reflect changes (Qty in Bulk '+' and Qty on Shelf '-') only after this transaction has been verified.<Br>&nbsp;Please make sure that paperwork is attached to the bin containing the parts when being processed.";
	}
	else if (strOpt.equals("BulkIn"))
	{
		strType = "4118";
		strShipTo = "";
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_BULK_TO_SHELF"));
		strLabel1 = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_INITIATED_BY"));
		//strMessage = "&nbsp;This transaction enables you to transfer parts from the Bulk to Shelf. The Qty moved will reflect changes (Qty in Shelf '+' and Qty in Bulk '-') only after this transaction has been verified.<Br>&nbsp;Please make sure that paperwork is attached to the bin containing the parts when being processed.";
	}
	else if (strOpt.equals("RawMatScrap"))
	{
		strType = "40051";
		strShipTo = "";
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_RAW_MATERIAL_TO_SCRAP"));
		strLabel1 = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_INITIATED_BY"));
		//strMessage = "&nbsp;This transaction enables you to transfer parts from the Raw Materials to Scrap. The Qty moved will reflect changes (Qty in Shelf '+' and Qty in Bulk '-') only after this transaction has been verified.<Br>&nbsp;Please make sure that paperwork is attached to the bin containing the parts when being processed.";
	}
	else if (strOpt.equals("RawMatQuar"))
	{
		strType = "40052";
		strShipTo = "";
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_RAW_MATERAIL_TO_QUARANTINE"));
		strLabel1 = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_INITIATED_BY"));
		//strMessage = "&nbsp;This transaction enables you to transfer parts from the Raw Materials to Quarantine. The Qty moved will reflect changes (Qty in Shelf '+' and Qty in Bulk '-') only after this transaction has been verified.<Br>&nbsp;Please make sure that paperwork is attached to the bin containing the parts when being processed.";
	}
	else if (strOpt.equals("RawMatFG"))
	{
		strType = "40053";
		strShipTo = "";
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_RAW_MATERIAL_TO_FINISHED_GOODS"));
		strLabel1 = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_INITIATED_BY"));
		//strMessage = "&nbsp;This transaction enables you to transfer parts from the Raw Materials to Finished Goods. The Qty moved will reflect changes (Qty in Shelf '+' and Qty in Bulk '-') only after this transaction has been verified.<Br>&nbsp;Please make sure that paperwork is attached to the bin containing the parts when being processed.";
	}
	else if (strOpt.equals("FGRawMat"))
	{
		strType = "40054";
		strShipTo = "";
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_FINIDHED_GOODS_TO_RAW"));
		strLabel1 = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_INITIATED_BY"));
		//strMessage = "&nbsp;This transaction enables you to transfer parts from Finished Goods to Raw Materials. The Qty moved will reflect changes (Qty in Shelf '+' and Qty in Bulk '-') only after this transaction has been verified.<Br>&nbsp;Please make sure that paperwork is attached to the bin containing the parts when being processed.";
	}
  
	if(hmTransRules.size()>0){
		strTypeRule   = GmCommonClass.parseNull((String)hmTransRules.get("ICE_TYPE")) ;
		strShipToRule = GmCommonClass.parseNull((String)hmTransRules.get("ICE_SHIPTO")) ;
		strHeaderRule = GmCommonClass.parseNull((String)hmTransRules.get("ICE_HEADER")) ;
		strSkipVal    = GmCommonClass.parseNull((String)hmTransRules.get("SKIP_VALIDATION"));
		strConsignIDLabel    = GmCommonClass.parseNull((String)hmTransRules.get("ICE_IDLABEL"));
		strReasonLabel    = GmCommonClass.parseNull((String)hmTransRules.get("ICE_REASON_LABEL"));
		strStockFl    = GmCommonClass.parseNull((String)hmTransRules.get("SKIP_STOCK"));
		
		if(strSkipVal.equals("")){
			strSkipVal = "N";
		}
		if(strConsignIDLabel.equals("") && strReasonLabel.equals("") && strStockFl.equals(""))
		{
			strReasonLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_REASON"));
			strConsignIDLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_CONSINGMENT_ID"));
			strStockFl = "Y";
		}
		if(!strTypeRule.equals(""))
			strType = strTypeRule;
		if(!strShipToRule.equals(""))
			strShipTo = strShipToRule;
		if(!strHeaderRule.equals(""))
			strHeader = strHeaderRule+GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_INITIATE"));
		strLabel1 = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_INITIATED_BY"));
	}
	int intSize = 0;
	HashMap hcboVal = null;
	
	if(strType.equals("400083")){ // 400083 -- Quarantine To Repack
	  strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("Quarantine_To_Repack_Initiate"));
	}else{
	  strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("INHOUSE_TRANSACTION"));
	}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: InHouse Item Consign </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmInhouseItemConsignSetup.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script>
var strSkipVal = "<%= strSkipVal %>";
var stockFl = "<%=strStockFl%>";
var label = "<%=strReasonLabel%>";
var cartType="<%=strAddCart%>";
var rowCnt = '<%=strRowCount%>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmConsignInHouseServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hConsignId" value="<%=strConsignId%>">
<input type="hidden" name="hDelPartNum" value="">
<input type="hidden" name="Cbo_Type" value="<%=strType%>">
<input type="hidden" name="Cbo_BillTo" value="01">
<input type="hidden" name="Cbo_ShipTo" value="<%=strShipTo%>">
<input type="hidden" name="hMode" value="<%=strOpt%>">
<input type="hidden" name="hAddCartType" value="<%=strAddCart%>">

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" ><%=strHeader%></td>
			<td height="25" bgcolor="#666666"  class="RightDashBoardHeader" align="Right">
				<fmtInHouseItemConsignSetup:message key="IMG_ALT_HELP" var = "varHelp"/>
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' 
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');"  />
			</td>
		</tr>
		<tr>
			<td width="698" height="100" valign="top" colspan="3">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
<%
		if (!strhAction.equals("Load"))
		{
%>
					<tr class="shade">
						<td class="RightText" HEIGHT="20" width="200" align="right">&nbsp;<B><%=strConsignIDLabel%>:</B></td>
						<td class="RightText" width="498">&nbsp;<%=strConsignId%></td>
					</tr>
<%
		}
%>
					<tr>
						<td class="RightText" HEIGHT="25" align="right">&nbsp;<B><%=strReasonLabel%>:</B></td>
						<td class="RightText">&nbsp;<select name="Cbo_Purpose" id="Region" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtInHouseItemConsignSetup:message key="LBL_CHOOSE_ONE"/>
<%
			  		intSize = alPurpose.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alPurpose.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strPurpose.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>
							</select>
						</td>
					</tr>
					<tr class="shade">
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><%=strLabel1%>:</b></td>
						<td class="RightText">&nbsp;<select name="Cbo_Values" id="Region" class="RightText"  tabindex="3" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtInHouseItemConsignSetup:message key="LBL_CHOOSE_ONE"/>
<%
			  		intSize = alEmpList.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alEmpList.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
						strSelected = strShipToId.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
<%
			  		}
%>						
						</select>
						</td>
					</tr>
					<tr>
						<td class="RightText" valign="top" align="right" HEIGHT="24"><b><fmtInHouseItemConsignSetup:message key="LBL_COMMENTS"/>:</b></td>
						<td>&nbsp;<textarea name="Txt_Comments" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1 rows=5 cols=50 value="<%=strFinalComments%>"><%=strFinalComments%></textarea></td>
					</tr>
<%
					if (strOpt.equals("InHouse"))
					{
%>					
					<tr class="shade">
						<td class="RightText" align="right" HEIGHT="24"><b><fmtInHouseItemConsignSetup:message key="LBL_SHIPPING_REQUIRED"/> ?:</b></td>
						<td><input type="checkbox" name="Chk_ShipFlag" onFocus="changeBgColor(this,'#AACCE8');" <%=strChecked%> onBlur="changeBgColor(this,'#ffffff');" tabindex=2></td>
					</tr>
<%
					}else{
%>
					<tr>
						<td colspan="2"><input type="hidden" name="Chk_ShipFlag" value="1"></td>
					</tr>										
<%
					}
		if (!strhAction.equals("Load") && !strhAction.equals("OrdPlaced"))
		{
%>					
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr>
					<td class="RightText" align="right" height="30"><b><fmtInHouseItemConsignSetup:message key="LBL_ADD_CART"/></b></td>
					<td  style="padding-left: 4px;">
					 <gmjsp:dropdown  controlName="Cbo_Add_Cart" value="<%=alQDList%>" seletedValue="<%=strAddCart%>"  codeId="CODEID" onChange="fnAddCartTxt(this)"  codeName="CODENM" />  
					</td>
					</tr>
					<tr>
						<td height="30" align="center" colspan="2" class="RightTextBlue">
						<div id="byTransTxtId"><b><fmtInHouseItemConsignSetup:message key="LBL_SCAN_TRANS"/></b></div>
						<div id="byPartTxtId"><b><fmtInHouseItemConsignSetup:message key="LBL_SHOPPING_CART_CLICK_GO"/></b></div>
						<br>&nbsp;&nbsp;&nbsp;<input type="text" size="40" value="" name="Txt_PartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1 onkeypress="return fnPreventAutoSubmit()">
						<fmtInHouseItemConsignSetup:message key="BTN_GO" var="varGo"/>
						<gmjsp:button value="&nbsp;${varGo}&nbsp;" name="Btn_GoCart" gmClass="button" onClick="fnAddToCart();" tabindex="16" buttonType="Load" /><br><br>
						</td>
					</tr>
					<%if(strAddCart.equals("106458")){ %>
					<tr>
					<td colspan="2" id="transCopyViewId"><b><fmtInHouseItemConsignSetup:message key="LBL_COPY_TRANS_ID"/></b><%=strPartNums %></td>
					</tr>
					<%} %>
					
					<%if(!strRowCount.equals("0")){ %>
					<tr>
					<td colspan="2" class="RightTextRed"><b><fmtInHouseItemConsignSetup:message key="LBL_NO_ROWS_1"/> <%=strRowCount %> <fmtInHouseItemConsignSetup:message key="LBL_NO_ROWS_2"/></b></td>
					</tr>
					<%} %>
<%
		}
		if (!strhAction.equals("Load"))
		{
			//String strStockLabel = "";
			String strStockLabelRule = "";
			strStockLabelRule=GmCommonClass.parseNull((String)hmTransRules.get("TRANS_LABEL"));
			System.out.println("strStockLabelRule strStockLabelRule strStockLabelRule "+strStockLabelRule);
							if (strOpt.equals("QuaranOut") || strOpt.equals("InvenIn"))
							{
								strStockLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_IN_QUARANTINE"));
							}else if (strOpt.equals("BulkIn")){
								strStockLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_IN_BULK"));
							}else{
								strStockLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_IN_SHELF"));
							}		
							if(!strStockLabelRule.equals("")){
								strStockLabel = strStockLabelRule;
							}
%>	

					<tr>
						<td align="center" colspan="2" valign="top">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td align="center" width="80" height="25"><fmtInHouseItemConsignSetup:message key="LBL_PART_#"/></td>
									<td align="center" width="200" colspan="<%=strStockFl.equals("N")?"2":"1"%>"><fmtInHouseItemConsignSetup:message key="LBL_DESCRIPTION"/></td>
									<%if(strStockFl.equals("Y")){%>
									<td align="center" width="70"><%=strStockLabel%></td>
									<%}%>
									<td align="center" width="80"><fmtInHouseItemConsignSetup:message key="LBL_PRICE"/></td>
									<td align="center" width="80"><fmtInHouseItemConsignSetup:message key="LBL_QUANTITY"/></td>
									<td align="center" width="100"><fmtInHouseItemConsignSetup:message key="LBL_AMOUNT"/></td>
								</tr>
								<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
<%
			String strTotal = "";
			if (!strPartNums.equals(""))
			{
					hmReturn = (HashMap)session.getAttribute("hmOrder");
					HashMap hmCart = (HashMap)session.getAttribute("hmCart");
					StringTokenizer strTok=null;
						String strTransIds="";
					if(strAddCart.equals("106458")){// 106458 - by transaction
					  strTransParts= (String)hmReturn.get("TRANSPARTNUMS");
					  strTransIds = (String)hmReturn.get("TRANSIDS");
					  strTok = new StringTokenizer(strTransParts,",");
					}else{
					  strTok = new StringTokenizer(strPartNums,",");
					}
					
					String strPartNum = "";
					HashMap hmVal = new HashMap();
					HashMap hmLoop = new HashMap();
					String strId = "";
					String strDesc = "";
					String strPrice = "";
					String strControlNum = "";
					String strTransPartQty = "";

					String strAmount = "";
					String strQty = "1";
					String strStock = "";
					String strUnPartNums = "";
					double dbPrice = 0.0;
					double dbDiscount = 0.0;
					double dbDiscPrice = 0.0;
					String strDiscPrice = "";
					String strShade = "";
					int i=0;
					double dbAmt = 0.0;
					double dbTotal = 0.0;
					boolean bolFl = false;
					boolean bolFl2 = false;
					String strQuarantineStock = "";
					String strWIPQty = "";
					int intBulkStock = 0;
					int intWIPQty = 0;
					String strDelPartNum = "";

					double dbQty = 0.0;
					if (strTok.countTokens() == 1)
					{
						bolFl2 = true;
					}
					while (strTok.hasMoreTokens())
					{
						i++;
						strPartNum 	= strTok.nextToken();
						if (hmCart != null)
						{
							hmLoop = (HashMap)hmCart.get(strPartNum);
							if (hmLoop != null)
							{
								strQty = (String)hmLoop.get("QTY");
							}
							else
							{
								strQty = "1";
							}
						}

						strQty = strQty == null?"1":strQty;
						dbQty = Double.parseDouble(strQty);
						hmVal = (HashMap)hmReturn.get(strPartNum);
						if (hmVal.isEmpty())
						{
							//bolFl = false;
							i--;
						}
						else if (hmVal.isEmpty() && bolFl2)
						{
							bolFl = false;
						}
						else
						{
							strNewPartNums = strNewPartNums + strPartNum + ",";
							bolFl = true;
							strId = (String)hmVal.get("ID");
							strDesc = (String)hmVal.get("PDESC");
							strPrice = (String)hmVal.get("PRICE");
							strDelPartNum = strId;
							if(strAddCart.equals("106458")){//-- 106458- by transaction type
							  if (hmCart== null || hmCart.size()==0){
							  strQty=GmCommonClass.parseZero((String)hmVal.get("QTY"));
							  dbQty = Double.parseDouble(strQty);
							  }
							  strControlNum = (String)hmVal.get("CTN_NUM");//-- control number from transaction
							  strTransPartQty = (String)hmVal.get("QTY");// -- transaction qty
							  strDelPartNum = strPartNum;
							}
							
							if (strOpt.equals("QuaranOut") || strOpt.equals("InvenIn")){
								strStock = GmCommonClass.parseZero((String)hmVal.get("QSTOCK"));
							}
							else if (strOpt.equals("BulkIn")){
								strStock = GmCommonClass.parseZero((String)hmVal.get("BSTOCK"));
							}
							else if (strOpt.equals("RawMatScrap") || strOpt.equals("RawMatQuar") || strOpt.equals("RawMatFG")){
								strStock = GmCommonClass.parseZero((String)hmVal.get("AQTY"));
							}
							else if(!strStockLabelRule.equals("")){
								strStock=GmCommonClass.parseZero((String)hmVal.get((String)hmTransRules.get("STOCK_KEY")));
							}
							else{
								strStock = GmCommonClass.parseZero((String)hmVal.get("STOCK"));
							}
							 
							double  dbStock = Double.parseDouble(strStock);
							if(!strOpt.equals("BulkIn")){
								strStock = dbStock < 0?"0":strStock;
							}
							/*if (strhAction.equals("OrdPlaced"))
							{
								int intStock = Integer.parseInt(strStock);
								intStock = intStock - intQty; 
								strStock = "" + intStock;
							}*/
							
							dbPrice = Double.parseDouble(strPrice);
							dbAmt = dbQty * dbPrice; // Multiply by Qty
							strDiscPrice = String.valueOf(dbPrice);
							strAmount = String.valueOf(dbAmt);
							dbTotal = dbTotal + dbAmt;
							strShade	= (i%2 == 0)?"class=Shade":""; //For alternate Shading of rows


%>
								<tr <%=strShade%>>
									<td class="RightText" align="center" height="20">
<%
							if (!strhAction.equals("OrdPlaced"))
							{
%>									
									<a  href="javascript:fnRemoveItem('<%=strDelPartNum%>');"><fmtInHouseItemConsignSetup:message key="IMG_ALT_REMOVE" var = "varRemove"/><img border="0" Alt='${varRemove}' valign="bottom" src="<%=strImagePath%>/btn_remove.gif" height="15" width="15"></a><%}%>&nbsp;<%=strId%></td>
									<td class="RightText" colspan="<%=strStockFl.equals("N")?"2":"1"%>"><%=strDesc%></td>
									<%if(strStockFl.equals("Y")){%>
									<td class="RightText" align="center"><%=strStock%><input type="hidden" name="hStock<%=i%>" value="<%=strStock%>"></td>
									<%}%>
									<td class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strDiscPrice)%>&nbsp;
									<input type="hidden" name="hPrice<%=i%>" value="<%=strDiscPrice%>">
									<input type="hidden" name="hOldTransQty<%=i%>" value="<%=strTransPartQty%>">
									<input type="hidden" name="hCtlNum<%=i%>" value="<%=strControlNum%>">
									<input type="hidden" name="hPNum<%=i%>" value="<%=strId%>">
									</td>
									<td class="RightText" align="center">
<%
							if (!strhAction.equals("OrdPlaced"))
							{
%>
										&nbsp;<input type="text" size="5" value="<%=strQty%>" name="Txt_Qty<%=i%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
<%
							}else{	
%>
										<%=strQty%>
<%							}%>
									</td>
									<td class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strAmount)%>&nbsp;</td>
								</tr>
<%
						strTotal = ""+dbTotal;
						}
					}
						if (!strNewPartNums.equals(""))
						{
						  if(strAddCart.equals("106458")){//--106458 - By transaction type 
						    strNewPartNums = strTransIds;
						  }else{
							strNewPartNums = strNewPartNums.substring(0,strNewPartNums.length()-1);
						  }
						}
						if (bolFl)
						{
%>
								<tr>
									<td colspan="6" height="1" bgcolor="#666666"></td>
								</tr>
								<tr class="RightTableCaption">
									<td colspan="4" align="right" height="23">
									<input type="hidden" name="hTotal" value="<%=strTotal%>">
									</td>
									<td align="center">Total</td>
									<td align="right"><%=strCurrSign%><%=GmCommonClass.getStringWithCommas(strTotal)%>&nbsp;</td>
								</tr>	
<%
						}						

						if (!bolFl)
						{
%>
								<tr>
									<td colspan="6" height="30" class="RightTextBlue" align="center"><fmtInHouseItemConsignSetup:message key="MSG_NO_DATA"/></td>
								</tr>
<%
						}
}else{ %>
								<tr>
									<td colspan="6" height="30" class="RightTextBlue" align="center"><fmtInHouseItemConsignSetup:message key="MSG_NO_DATA"/></td>
								</tr>
<%
			}
		}
%>
							</table><input type="hidden" name="hPartNums" value="<%=strNewPartNums%>">
							<input type="hidden" name="hTransPartNums" value="<%=strTransParts%>">
						</td>
					</tr>
<%
		if (strhAction.equals("Load"))
		{
%>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>					
					<tr>
						<td colspan="2" height="30" align="center">
							<fmtInHouseItemConsignSetup:message key="BTN_INITIATE" var="varInitiate"/><gmjsp:button value="${varInitiate}" gmClass="button" onClick="fnInitiate();" tabindex="13" buttonType="Save" />
						</td>
					</tr>
<%
		}else if (strhAction.equals("PopOrd")){
%>
		<tr>
		<%
		String strPlaceOrder = "fnPlaceOrder('"+strStockLabel+"')";
		%>
			<td colspan="3" align="center" height="30">
				<fmtInHouseItemConsignSetup:message key="BTN_UPDATE" var="varUpdate"/>
				<gmjsp:button value="&nbsp;${varUpdate}&nbsp;" name="Btn_UpdateQty" gmClass="button" onClick="fnUpdateCart();" buttonType="Save" />
				&nbsp;&nbsp;
				<fmtInHouseItemConsignSetup:message key="BTN_SUBMIT" var="varSubmit"/>
				<gmjsp:button value="&nbsp;${varSubmit}  &nbsp;" name="Btn_PlaceOrd" gmClass="button" onClick="<%=strPlaceOrder%>" buttonType="Save" />
			</td>
		<tr>
<%	
	}else if (strhAction.equals("OrdPlaced"))
	{
%>
					<tr>
						<td colspan="2" height="30" align="center">
							<fmtInHouseItemConsignSetup:message key="BTN_PIC_SLIP" var="varPIC_Slip"/>
							<gmjsp:button value="${varPIC_Slip}" gmClass="button" onClick="fnPicSlip();" tabindex="13" buttonType="Save" />&nbsp;&nbsp;
						</td>
					</tr>
<%
	}								
%>
				</table>
			 </td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
    </table>
    <script>
    var transType='<%=strType%>';
    var hidPartVal='<%=strNewPartNums%>';
    // 400083-Quarantine to Repack, to disabled add to cart dropdown for other transaction type
    if(transType=='400083'){
    	if(document.getElementById('Cbo_Add_Cart') != undefined){
    		document.getElementById('Cbo_Add_Cart').disabled=false;
    	}	
    }else{
    	if(document.getElementById('Cbo_Add_Cart') != undefined){
    		document.getElementById('Cbo_Add_Cart').disabled=true;
    	}
    }
    
    if(hidPartVal!=''){//  if transaction is entered with part or transaction , disabled add to cart dropdown 
    	document.getElementById('Cbo_Add_Cart').disabled=true;
    }else{
    	if(cartType=='106458'){//--cart is empty and 106458 - by transaction type , empty transaction copied label
    	document.getElementById('transCopyViewId').innerHTML='';
    	}
    }
    
    if(cartType=='106458'){//--  106458 - by transaction , to display related label
    	document.getElementById("byTransTxtId").style.display="block";
		document.getElementById("byPartTxtId").style.display="none";
    }else{
    	if(document.getElementById("byTransTxtId")!= undefined){
    		document.getElementById("byTransTxtId").style.display="none";
			document.getElementById("byPartTxtId").style.display="block";
    	}
    }
    
    </script>
<%--  
<script>
<%
	if (strhAction.equals("OrdPlaced"))
	{
%>
	alert("Your consignment has been placed. Please print PIC Slip");
<%
	}
%>
</script>
--%> 
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>

</BODY>

</HTML>
