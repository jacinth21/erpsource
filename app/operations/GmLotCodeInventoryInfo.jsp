<%
/**********************************************************************************
 * File		 		: GmLotCodeInventoryInfo.jsp
 * Desc		 		: This Jsp is for the inventory information section
 * Version	 		: 1.0
 * author			: arajan
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtLotInvInfo" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
  <!-- \operations\GmLotCodeInventoryInfo.jsp--> 
<fmtLotInvInfo:setLocale value="<%=strLocale%>"/>
<fmtLotInvInfo:setBundle basename="properties.labels.operations.GmLotCodeReport"/>
<% 
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
%>
<HTML>
<HEAD>

<script>
</script>
<!-- style added for PC-3659 support Edge alignment -->
<style>
	#InvDataGrid
	{
		height: auto !important;
		width: 100% !important;
		margin:	0 !important;
	}
	#InvDataGrid .objbox{
	 height: auto !important;
    }
</style>
<BODY leftmargin="20" topmargin="10">
	
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr><td class="Line" colspan="5"></td></tr> 
			<tr>
					<td height="24" class="ShadeRightTableCaption" colspan="5">&nbsp;<b><fmtLotInvInfo:message key="TD_LOT_INV_INFO_HEADER"/></b></td>
		    </tr>	
		    <tr id="DivInvGrid">
				<td>
				<div id="InvDataGrid" height="50"></div>
				</td>
			</tr>
			<tr id="DivInvNoDataMessage"><td colspan="3" height="20" align="Center"><fmtLotInvInfo:message key="TD_NO_DATA_FOUND"/></td></tr>
			
		</table>

	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>