
<%
	/**********************************************************************************
	 * File		 		: GmReturnForCredit.jsp
	 * Desc		 		: This screen is used for the 
	 * Version	 		: 1.0
	 * author			: Venkata Prasath
	 ************************************************************************************/
%>
<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Date"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtReturnForCredit"
	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<!-- GmReturnForCredit.jsp -->
<fmtReturnForCredit:setLocale value="<%=strLocale%>" />
<fmtReturnForCredit:setBundle
	basename="properties.labels.operations.logistics.GmReturnForCredit" />


<%
	String strWikiTitle = GmCommonClass
			.getWikiTitle("RETURNS_FOR_CREDIT");
	HashMap hmReturn = (HashMap) request.getAttribute("hmReturn");
	HashMap hmConsignDetails = new HashMap();
	String strhAction = (String) request.getParameter("hAction") == null ? ""
			: (String) request.getParameter("hAction");
	String strFrom = GmCommonClass.parseNull((String) request
			.getAttribute("hFrom"));
	String strApplnDateFmt = strGCompDateFmt;
	GmCalenderOperations.setTimeZone(strGCompTimeZone);
	String strTodaysDate = GmCalenderOperations
			.getCurrentDate(strGCompDateFmt);
	String strPartNum = "";
	String strPartDesc = "";
	String strQty = "";
	String strItemQty = "";
	String strTagable = "";
	String strConsQty = "";
	String strInitQty = "";
	String strReturnQty = "";
	String strMissingQty = "";
	String strExcessQty = "";
	String strRAId = "";
	String strRetType = "";
	String strValidAddressFl = "";
	String strTaxCountryFl = "";
	String strTaxDate = "";
	String strInvoiceId = "";
	String strOrderDate = "";

	ArrayList alRetLoad = new ArrayList();
	alRetLoad = request.getAttribute("alReturn") == null ? new ArrayList()
			: (ArrayList) request.getAttribute("alReturn");
	String strMessage = request.getAttribute("MESSAGE") == null ? ""
			: (String) request.getAttribute("MESSAGE");
	String strRAComplete = GmCommonClass.parseNull((String) request
			.getParameter("Chk_RAComplete"));

	int intSize = 0;
	HashMap hcboVal = null;

	//GmCalenderOperations gmCalenderOperations = new GmCalenderOperations();
	//String strCreditDate = (request.getParameter("Txt_CreditDate") == null) ? GmCalenderOperations.addDays(0) : GmCommonClass.parseNull((String)request.getParameter("Txt_CreditDate"));
	Date dtCreditDate = request.getParameter("Txt_CreditDate") == null ? (Date) GmCommonClass
			.getCurrentDate(strApplnDateFmt)
			: (Date) GmCalenderOperations.getDate(request
					.getParameter("Txt_CreditDate"));

	if (hmReturn != null) {
		hmConsignDetails = (HashMap) hmReturn.get("RADETAILS");
		if (hmConsignDetails != null) {
			strRAId = GmCommonClass.parseNull((String) hmConsignDetails
					.get("RAID"));
			strRetType = GmCommonClass
					.parseNull((String) hmConsignDetails.get("TYPEID"));
			strValidAddressFl = GmCommonClass
					.parseNull((String) hmConsignDetails
							.get("VALID_ADD_FL"));
			strTaxCountryFl = GmCommonClass
					.parseNull((String) hmConsignDetails
							.get("TAX_COUNTRY_FL"));
			strTaxDate = GmCommonClass
					.parseNull((String) hmConsignDetails
							.get("TAX_START_DATE"));
			strInvoiceId = GmCommonClass
					.parseNull((String) hmConsignDetails.get("INVID"));
			strOrderDate = GmCommonClass
					.parseNull((String) hmConsignDetails
							.get("ORDERDATE"));
		}
	}
%>


<%@page import="java.util.Calendar"%>
<%@page import="java.util.TimeZone"%><HTML>
<HEAD>
<TITLE>Globus Medical: Return For Credit</TITLE>

<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Rule.css">
<link rel="stylesheet" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script>
var validAddFl = '<%=strValidAddressFl%>';
var taxCountryFl = '<%=strTaxCountryFl%>';
var taxDate = '<%=strTaxDate%>';
var dateFmt = '<%=strApplnDateFmt%>';
var invId = '<%=strInvoiceId%>';
var orderdate = '<%=strOrderDate%>';
/*
 * This funciton will call when click on "Credit" Button from Operation Dashboard.
 */
function fnSubmit()
{
 if(document.frmVendor.Txt_CreditDate.value == '')
 {
  Error_Details(message[583]);
 }
  if(document.frmVendor.Chk_RAComplete.checked == true)
    {
     document.frmVendor.Chk_RAComplete.value = 2;
    }
    
	document.frmVendor.hAction.value = "SaveCredit";
	
	if (ErrorCount > 0)
	 {
		Error_Show();
		Error_Clear();
		return false;
	}
	fnStartProgress('Y');
  	document.frmVendor.submit();
}
/*
 * This funciton will call when click on "Issue Credit" Button from AR Dashboard.
 * It will form inputstring for partial issue credit each line item from GmIncludeReturnHeader.jsp  
 */
		
function fnAccountSubmit()
{
 if(document.frmVendor.Txt_CreditDate.value == '')
 {
  Error_Details(message[583]);
 }
//Tax alert
 	var taxDateDiff = '';
	taxDateDiff = dateDiff(taxDate, orderdate, dateFmt);
	if (taxDateDiff >= 0) {
		if (invId !='' && taxCountryFl == 'Y' && validAddFl != 'Y') {
			//4002 - The address has not been validated and incorrect tax calculation is possible. Do you want to proceed?
			Error_Details(message[4002]);
		}
	}
	
  if(document.frmVendor.Chk_RAComplete.checked == true)
    {
     document.frmVendor.Chk_RAComplete.value = 2;
    }
    
	document.frmVendor.hAction.value = "SaveCredit";
	var creditString = "";
	var errCreditQty = "";
	var errMoreCreditQty = "";
	var fullFlag = 0;
	var partCnt = (document.frmVendor.hPartCount.value != undefined)?parseInt(document.frmVendor.hPartCount.value):0;
	for(i=0;i<partCnt;i++){
		var pnum = 	document.getElementById("hPnum"+i).value;
		var cnum = 	document.getElementById("hCnum"+i).value;
		var price = document.getElementById("hPrice"+i).value;
		var type = 	document.getElementById("hType"+i).value;	
		var intQty = parseFloat(document.getElementById("hInitQty"+i).value);
		var returnQty = parseFloat(document.getElementById("hReturnQty"+i).value);	
		var creditQty = parseFloat(document.getElementById("TxtCreditQty"+i).value);
		var unitPrice = document.getElementById("hUnitPrice"+i).value;
		var unitPriceAdj = document.getElementById("hUnitPriceAdj"+i).value;
		var adjCode = document.getElementById("hAdjCode"+i).value;		
		returnQty = isNaN(returnQty)?intQty:returnQty;
		
        
        if(isNaN(creditQty) || creditQty < 0 ){
        	errCreditQty+=(errCreditQty.indexOf(pnum) != -1)? "":", "+pnum;
        	continue;	
         }

        if(returnQty<creditQty){
        	errMoreCreditQty+=(errMoreCreditQty.indexOf(pnum) != -1)? "":", "+pnum;
        	continue;	
        }
      //pnum^credit_qty^cnum^item_price^type|
        if(creditQty>0){
			creditString += pnum+"^"+creditQty+"^"+cnum+"^"+price+"^"+type+"^"+unitPrice+"^"+unitPriceAdj+"^"+adjCode+"|";
        }
		if(returnQty==creditQty){
			fullFlag++;
		}else if(returnQty>creditQty){
			creditString += pnum+"^"+(returnQty-creditQty)+"^"+cnum+"^0^"+type+"^"+unitPrice+"^"+unitPriceAdj+"^"+adjCode+"|";
		}				

	}

	  if (errCreditQty != '')
		{
		  Error_Details(Error_Details_Trans(message[10024],errCreditQty.substr(1,errCreditQty.length)));
		}
	  if (errMoreCreditQty != '')
		{
		  Error_Details(Error_Details_Trans( message[10025],errMoreCreditQty.substr(1,errMoreCreditQty.length)));
		}	
	if (ErrorCount > 0)
	 {
		Error_Show();
		Error_Clear();
		return false;
	}
	document.getElementById("hCreditString").value = creditString
	fnStartProgress('Y');
	document.frmVendor.submit();
}

function fnVoid()
{
		document.frmVendor.hTxnId.value = document.frmVendor.hRAId.value;
		document.frmVendor.action ="<%=strServletPath%>/GmCommonCancelServlet";
		document.frmVendor.hAction.value = "Load";
		document.frmVendor.hCancelType.value = 'VDRTN'
		document.frmVendor.submit();
}

function fnRollback()
{
		document.frmVendor.hTxnId.value = document.frmVendor.hRAId.value;
		document.frmVendor.action ="<%=strServletPath%>/GmCommonCancelServlet";
		document.frmVendor.hAction.value = "Load";		
		document.frmVendor.hCancelType.value = 'RBRTN'
		document.frmVendor.submit();
}

function fnLoad()
{
   //document.frmVendor.Txt_CreditDate.value = '<%=dtCreditDate%>';
		document.frmVendor.Chk_RAComplete.checked = "true";
		document.frmVendor.Chk_RAComplete.disabled = "true";
	}

	function fnOpenTag(strPnum, strRaId) {
		windowOpener("/gmTag.do?strOpt=reload&pnum=" + strPnum + "&refID="
				+ strRaId + "&refType=51001", "PrntInv",
				"resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=600");
	}
</script>


</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnLoad()">
	<FORM name="frmVendor" method="POST"
		action="<%=strServletPath%>/GmReturnCreditServlet">
		<input type="hidden" name="hAction" value="<%=strhAction%>"> <input
			type="hidden" name="hCancelType"> <input type="hidden"
			name="hOpt"> <input type="hidden" name="hTxnId"> <input
			type="hidden" name="hFrom" value="<%=strFrom%>">

		<table border="0" class="DtTable950" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" class="RightDashBoardHeader"><fmtReturnForCredit:message key="LBL_CREDIT_RETURNS" /></td>
				<td height="25" class="RightDashBoardHeader" align="right">
				<fmtReturnForCredit:message key="IMG_HELP" var="varHelp"/>
				<img
					id='imgEdit' align="right" style='cursor: hand'
					src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16'
					height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td bgcolor="#666666" height="1" colspan="3"></td>
			</tr>
			<%
				if (!strMessage.equals("")) {
			%>
			<tr>
				<td height="25" align="center"><%=strMessage%></td>
			</tr>
			<tr>
				<td bgcolor="#666666" height="1" colspan="3"></td>
			</tr>
			<%
				}
			%>
			<tr>
				<td height="1" colspan="3"><jsp:include
						page="/operations/returns/GmIncludeReturnHeader.jsp">
						<jsp:param name="hMode" value="View" />
					</jsp:include></td>
			</tr>
			<tr>
				<td colspan="3">
					<table width="100%" align="center">
						<!-- Custom tag lib code modified for JBOSS migration changes -->
						<tr>
							<td class="RightTableCaption" align="right" width="40%"><fmtReturnForCredit:message key="LBL_CREDIT_DATE" var="varCreditDate" /><gmjsp:label
									type="RegularText" SFLblControlName="${varCreditDate}:" td="false" /></td>
							<td>&nbsp;<gmjsp:calendar textControlName="Txt_CreditDate"
									textValue="<%=new java.sql.Date(dtCreditDate.getTime())%>"
									gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
									onBlur="changeBgColor(this,'#ffffff');" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class=Line colspan="3" height=1></td>
			</tr>
			<%
				if (strFrom.equals("AccountsReturnsDashboard")) {
			%>
			<tr>
				<td colspan="3"><jsp:include
						page="/operations/logistics/GmReturnForCreditInclude.jsp" /></td>
			</tr>
			<%
				} else {
			%>
			<tr>
				<td colspan="3">
					<%
						if (strhAction.equals("LoadCredit")
									|| strhAction.equals("SaveCredit")) {
					%>
					<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td colspan="3">
								<table cellspacing="0" cellpadding="0" border="0" width="100%"
									id="myTable">
									<tr class="ShadeRightTableCaption">
										<td width="78" align="center" height="25"><fmtReturnForCredit:message key="LBL_PART_NUMBER" /></td>
										<td width="340"><fmtReturnForCredit:message key="LBL_DESCRIPTION" /></td>
										<%
											if (strRetType.equals("3308")) {
										%>
										<td width="70" align="center"><fmtReturnForCredit:message key="LBL_LOANED" /><br><fmtReturnForCredit:message key="LBL_QTY" />
										</td>
										<%
											} else {
										%>
										<td width="60" align="center"><fmtReturnForCredit:message key="LBL_CONS" /><br><fmtReturnForCredit:message key="LBL_QTY" />
										</td>
										<%
											}
										%>
										<td width="60" align="center"><fmtReturnForCredit:message key="LBL_INIT" />.<br><fmtReturnForCredit:message key="LBL_QTY" />
										</td>
										<td width="60" align="center"><fmtReturnForCredit:message key="LBL_RET" /><br><fmtReturnForCredit:message key="LBL_QTY" />
										</td>
										<td width="60" align="center"><fmtReturnForCredit:message key="LBL_MISSING" /><br><fmtReturnForCredit:message key="LBL_QTY" />
										</td>
										<td width="60" align="center"><fmtReturnForCredit:message key="LBL_EXCESS" /><br><fmtReturnForCredit:message key="LBL_QTY" />
										</td>
									</tr>
									<tr>
										<td class=Line colspan="7" height="1"></td>
									</tr>
									<%
										intSize = alRetLoad.size();
												if (intSize > 0) {
													HashMap hmLoop = new HashMap();

													int intSetQty = 0;
													int intRetQty = 0;
													int intReconQty = 0;

													int intConsQty = 0;
													double intInitQty = 0;
													double intReturnQty = 0;
													double intMissingQty = 0;
													double intExcessQty = 0;

													for (int i = 0; i < intSize; i++) {
														hmLoop = (HashMap) alRetLoad.get(i);

														strPartNum = GmCommonClass
																.parseNull((String) hmLoop.get("PNUM"));
														strPartDesc = GmCommonClass
																.parseNull((String) hmLoop.get("PDESC"));
														strConsQty = GmCommonClass
																.parseNull((String) hmLoop.get("CONS_QTY"));
														intConsQty = strConsQty.equals("") ? 0 : Integer
																.parseInt(strConsQty);
														strInitQty = GmCommonClass
																.parseNull((String) hmLoop.get("INIT_QTY"));
														intInitQty = strInitQty.equals("") ? 0 : Double.parseDouble(strInitQty);
														strReturnQty = GmCommonClass
																.parseNull((String) hmLoop
																		.get("RETURN_QTY"));
														strTagable = GmCommonClass
																.parseNull((String) hmLoop.get("TAGABLE"));
														intReturnQty = strReturnQty.equals("") ? 0
																: Double.parseDouble(strReturnQty);
														intMissingQty = intInitQty - intReturnQty <= 0 ? 0
																: intInitQty - intReturnQty;
														
														// consigned qty is negative then, initiated qty to be create the Excess txn
														if(intConsQty < 0){
														  intExcessQty = intReturnQty;
														}else{ 
														  intExcessQty = intConsQty - intReturnQty >= 0 ? 0
																: -(intConsQty - intReturnQty);
														}
														strConsQty = intConsQty == 0 ? "-" : String
																.valueOf(intConsQty);
														strInitQty = intInitQty == 0 ? "-" : String
																.valueOf(intInitQty);
														strReturnQty = intReturnQty == 0 ? "-" : String
																.valueOf(strReturnQty);
														strMissingQty = intMissingQty == 0 ? "-" : String
																.valueOf(intMissingQty);
														strExcessQty = intExcessQty == 0 ? "-" : String
																.valueOf(intExcessQty);
														String strAlign = "right";
														if (!strTagable.equals("")) {
															strAlign = "center";
														}
									%>
									<tr>
										<td class="RightText" height="20" align="<%=strAlign%>">&nbsp;
											<%
												if (!strTagable.equals("")) {
											%> <a href=javascript:fnOpenTag('<%=strPartNum%>','<%=strRAId%>')>
												<img style='cursor: hand' src='<%=strImagePath%>/tag.jpg' width='12'
												height='12' />
										</a>
											<%
												}
											%>&nbsp;<%=strPartNum%>
										</td>
										<td class="RightText">&nbsp;<%=GmCommonClass
									.getStringWithTM(strPartDesc)%></td>
										<td align="center" class="RightText"><%=strConsQty%></td>
										<td align="center" class="RightText">&nbsp;<%=strInitQty%></td>
										<td align="center" class="RightText">&nbsp;<%=strReturnQty%></td>
										<td align="center" class="RightText">&nbsp;<%=strMissingQty%></td>
										<td align="center" class="RightText">&nbsp;<%=strExcessQty%></td>
									</tr>
									<tr>
										<td class="Line" colspan="7" height="1"></td>
									</tr>
									<%
										}//End of FOR Loop
												} else {
									%>
									<tr>
										<td colspan="5" height="50" align="center"
											class="RightTextRed"><BR><fmtReturnForCredit:message key="LBL_NO_PART_NUMBER_RETURNED"/></td>
									</tr>
									<%
										}
									%>
								</table>
							</td>
							<td><input type="hidden" name="hCnt" value="<%=intSize%>"></td>
						</tr>
						<%
							}
								/* Below Section to enter comments information */
						%>
						</tr>
						<%
							}
						%>

						<tr>
							<td colspan="4" align="center">
								<table width="30%">
									<tr>
										<%
											if (!strFrom.equals("AccountsReturnsDashboard")) {
										%>
										<td height="30" align="right" width="375">
										<fmtReturnForCredit:message key="BTN_ROLLBACK" var="varRollback"/>
										<gmjsp:button
												value="${varRollback}" gmClass="button" onClick="fnRollback();"
												buttonType="Save" /></td>
										<td height="30" align="left" width="450">
										<fmtReturnForCredit:message key="BTN_CREDIT" var="varCredit"/>
										<gmjsp:button
												value="${varCredit}" gmClass="button" onClick="fnSubmit();"
												buttonType="Save" /></td>
										<%
											} else {
										%>
										<td class="RightText" valign="top" align="left" HEIGHT="24"><fmtReturnForCredit:message key="LBL_COMMENTS"/>:</td>
										<td>&nbsp;<textarea name="Txt_Comments" class="InputArea"
												onFocus="changeBgColor(this,'#AACCE8');"
												onBlur="changeBgColor(this,'#ffffff');" tabindex=1 rows=5
												cols=85 value=""></textarea></td>
									</tr>
									<tr>
										<td height="30" align="center" colspan=2>
										<fmtReturnForCredit:message key="BTN_ISSUE_CREDIT" var="varIssueCredit"/>
										<gmjsp:button
												value="${varIssueCredit}" gmClass="button"
												onClick="fnAccountSubmit();" buttonType="Save" /></td>
										<%
											}
										%>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</FORM>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
