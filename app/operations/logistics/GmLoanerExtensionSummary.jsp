
<%
/**********************************************************************************
 * File		 		: GmLoanerExtension.jsp
 * Desc		 		: This screen is used to Loaner Extension and replenish In-House Sets
 * Version	 		: 1.0
 * author			: Angela
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>


<%@ taglib prefix="fmtLoanerExtensionSummary" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmLoanerExtensionSummary.jsp -->
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<fmtLoanerExtensionSummary:setLocale value="<%=strLocale%>"/>
<fmtLoanerExtensionSummary:setBundle basename="properties.labels.operations.logistics.GmLoanerExtensionSummary"/>

<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	String strCssPath = GmCommonClass.getString("GMSTYLES");
	
	HashMap hmCsg = new HashMap();
	hmCsg = (HashMap)request.getAttribute("hmCONSIGNMENT");
	if (hmCsg == null)
	{
		hmCsg = (HashMap)session.getAttribute("hmCONSIGNMENT");
	}
	String strType = GmCommonClass.parseNull((String)hmCsg.get("CTYPE"));

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Loaner Extension Summary</TITLE>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
</head>
<SCRIPT>
var type = '<%=strType%>';
function fnPrintVer(val)
{
windowOpener("/GmInHouseSetServlet?hAction=ViewPrint&hConsignId="+val+"&hType="+type,"LoanPrint","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}
</SCRIPT>
<BODY leftmargin="20" topmargin="10">
<FORM name="frmLoanerExtension" method="POST" action="">

	<table border="0" width="700"   class="DtTable850" cellspacing="0" cellpadding="0">
	<tr><td>
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
		<tr><td  height="1" class="Line"></td></tr>
		<tr>
			<td rowspan="15" width="1" class="Line"></td>
			<td height="25" class="RightDashBoardHeader" colspan="2"><fmtLoanerExtensionSummary:message key="LBL_SUMMARY"/></td>
			<td rowspan="15" width="1" class="Line"></td>
		
	  	</tr>
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->	  	
		<tr>
			<td >
				<jsp:include page="/include/GmIncludeConsignment.jsp" />
			
				</td>
		</tr>
				</table>
	<jsp:include page="GmIncludeLESummary.jsp" />
				
	</td>	</tr></table>
		
</FORM>
</BODY>
</HTML>
