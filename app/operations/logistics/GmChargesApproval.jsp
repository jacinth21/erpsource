 <%
/**********************************************************************************
 * File		 		: GmChargesApproval.jsp
 * Desc		 		: This screen is used for Charges Approval
 * Version	 		: 1.1
 * author			: Brinal G
 * 
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Iterator" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ page import="org.apache.commons.beanutils.DynaBean"%>

<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page buffer="16kb" autoFlush="true" %>
<%@ taglib prefix="fmtChargesApproval" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- GmChargesApproval.jsp-->
<fmtChargesApproval:setLocale value="<%=strLocale%>"/>
<fmtChargesApproval:setBundle basename="properties.labels.operations.logistics.GmChargesApproval"/>
<bean:define id="statuslist" name="frmChargesApproval" property="alStatustListRpt" type="java.util.List"> </bean:define>
<bean:define id="returnList" name="frmChargesApproval" property="alReturn" type="java.util.ArrayList"> </bean:define>
<bean:define id="deptId" name="frmChargesApproval" property="deptId" type="java.lang.String"> </bean:define>
<bean:define id="chargesFor" name="frmChargesApproval" property="chargesFor" type="java.lang.String"> </bean:define>
<bean:define id="shwDtlFl" name="frmChargesApproval" property="shwDtl" type="java.lang.String"> </bean:define>
<bean:define id="applnDateFmt" name="frmChargesApproval" property="applnDateFmt" type="java.lang.String"> </bean:define>
<bean:define id="applnCurrFmt" name="frmChargesApproval" property="applnCurrFmt" type="java.lang.String"> </bean:define>
<bean:define id="applnCurrSign" name="frmChargesApproval" property="applnCurrSign" type="java.lang.String"> </bean:define>
<bean:define id="ldResult" name="frmChargesApproval" property="ldResult" type="java.util.List"> </bean:define>
<%
		String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
		String strWikiTitle = GmCommonClass.getWikiTitle("CHARGES_APPROVAL");
		ArrayList alList = new ArrayList();
		alList= GmCommonClass.parseNullArrayList((ArrayList) returnList);
		int rowsize = alList.size();
		String strRptFmt = "{0,date,"+applnDateFmt+"}";
		String strCurrFmt = "{0,number,"+applnCurrSign+applnCurrFmt+"}";
		boolean showDetailsDisable = (chargesFor.equals("50891"))?true:false;		
		String strDisable = "";
		if(chargesFor.equals("50891")){
			strDisable = "disabled";
		}
		ArrayList alReportList = new ArrayList();
		alReportList = GmCommonClass.parseNullArrayList((ArrayList) ldResult);
		String strShowDist = GmCommonClass.parseNull(request.getParameter("shwDist"));
		String strShowDirRep = GmCommonClass.parseNull(request.getParameter("shwDirRep"));
		
		if(strShowDist.equals("") || strShowDist.equals("on"))
		{
			strShowDist= "true";
		}
		if(strShowDirRep.equals("") || strShowDirRep.equals("on"))
		{
			strShowDirRep= "true";
		}
		String strChkTicket 		= GmCommonClass.parseNull((String)request.getAttribute("CHK_TICKET"));
		String strChkRepname 		= GmCommonClass.parseNull((String)request.getAttribute("CHK_REPNAME"));
		String strChkListPrice 		= GmCommonClass.parseNull((String)request.getAttribute("CHK_LSTPRICE"));
		String strChkConsignment 	= GmCommonClass.parseNull((String)request.getAttribute("CHK_CONSIGNMENT"));
		String strDtTablewidth = "1500";
		int intColumnCnt = 0;
	
		if(strChkTicket.equals("on")){
			intColumnCnt++;
		}
		if(strChkRepname.equals("on")){
			intColumnCnt++;
		}
		if(strChkListPrice.equals("on")){
			intColumnCnt++;
		}
		if(strChkConsignment.equals("on")){
			intColumnCnt++;
		}
		if(intColumnCnt >= 3){
			strDtTablewidth = "1600";
		}
		request.setAttribute("COLUMNCOUNT",intColumnCnt);
%>
<HTML>
<TITLE> Globus Medical: Charges Approval</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strOperationsJsPath%>/GmChargesApproval.js"></script>
<script>
var totalRecs = '<%=rowsize%>';
var dptID = '<%=deptId%>';
var format = '<%=applnDateFmt%>';

function fnFormLoad(){

	<% 	if (deptId.equals("2000")) { %>	
		document.frmChargesApproval.status.disabled = true;
	<% } %>
	
	<%if (strShowDist.equals("true")) {%>
	
	document.frmChargesApproval.shwDist.checked = true;
<%}	if (strShowDirRep.equals("true")) {%>

	document.frmChargesApproval.shwDirRep.checked = true;
<%}%>
	
	if (document.frmChargesApproval.cbo_head_stat == undefined){		
		return;
	}
	document.frmChargesApproval.cbo_head_stat.options.length = 0;	
	document.frmChargesApproval.cbo_head_stat.options[0] = new Option("[Select All]","0");
	var i = 1;
	<%
		 Iterator alListIter;
		alListIter = statuslist.iterator();		
		 String strOptKey = "";
		 String strOptValue = "";
		 HashMap hmStatusList = new HashMap();
		while (alListIter.hasNext()) {
			hmStatusList = (HashMap) alListIter.next();
			strOptKey = (String) hmStatusList.get("CODENMALT");
			strOptValue = (String) hmStatusList.get("CODENM");		
	%>
		var id = '<%=strOptKey%>';
		var name = '<%=strOptValue%>';	
		document.frmChargesApproval.cbo_head_stat.options[i] = new Option(name,id);
		i++;
	<% } %>	

	
}

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnFormLoad();fnLoadIncludeFilters('<%= chargesFor %>');">
<html:form action="/gmChargesApproval.do">
<html:hidden property="updateString"/>
<html:hidden property="strOpt"/>
<html:hidden property="inputString"/>
<html:hidden property="screenType"/>
<html:hidden property="distName"/>
<html:hidden property="repName"/>
 <table border="0" class="DtTable<%=strDtTablewidth%>" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" ><fmtChargesApproval:message key="LBL_LOANER_CHARGES"/> </td>
			<td  height="25" class="RightDashBoardHeader" align="right" colspan="8">
			<fmtChargesApproval:message key="IMG_HELP" var="varHelp"/>
			<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
		       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		    </td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="8"></td></tr>
		<tr class="">
		<!-- Custom tag lib code modified for JBOSS migration changes -->
			<td height="25" class="RightTableCaption" align="right" width="12%">&nbsp;<fmtChargesApproval:message key="LBL_DISTRIBUTOR_NAME"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="dist"  SFFormName='frmChargesApproval' SFSeletedValue="dist"  defaultValue= "[Choose One]"	
						SFValue="alDistList" codeId="ID" codeName="NAME"  />  
		         			
			</td>
			<td height="25" class="RightTableCaption" align="right" width="15%">&nbsp;<fmtChargesApproval:message key="LBL_REP_NAME"/>:</td>
			<td colspan="2">&nbsp;<gmjsp:dropdown controlName="rep"  SFFormName='frmChargesApproval' SFSeletedValue="rep"  defaultValue= "[Choose One]"	
						SFValue="alRepList" codeId="ID" codeName="NAME"  /> 
			</td>
			
			<td height="25" class="RightTableCaption" align="right">&nbsp;<fmtChargesApproval:message key="LBL_REQUEST_ID"/>:</td>
            <td>&nbsp;<html:text maxlength="50" property="requestID"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /> </td> 
			
		</tr>
		<tr><td colspan="8" class="LLine"></td></tr> 
		<tr class="shade">
			<td height="25" class="RightTableCaption" align="right">&nbsp;<fmtChargesApproval:message key="LBL_STATUS"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="status"  SFFormName='frmChargesApproval' SFSeletedValue="status"  defaultValue= "[Choose One]"	
						SFValue="alStatus" codeId="CODENMALT" codeName="CODENM"   	 /> 
			</td>
			<td height="25" class="RightTableCaption" align="right" width="15%">&nbsp;<fmtChargesApproval:message key="LBL_INCIDENT_TYPE"/>:</td>
			<td colspan="4"><div id="missingfive">&nbsp;<gmjsp:dropdown controlName="incident" SFFormName='frmChargesApproval' SFSeletedValue="incident"  defaultValue= "[Choose One]"	
						SFValue="alIncidentList" codeId="CODEID" codeName="CODENM"  /> </div>
			</td>
		</tr>
		<tr><td class="LLine" height="1" colspan="8"></td></tr>	
		<tr class="">		
			<td class="RightTableCaption" align="right">&nbsp;<fmtChargesApproval:message key="LBL_FROM_DATE"/>:</td>
			<td class="RightTableCaption">&nbsp;<gmjsp:calendar SFFormName="frmChargesApproval"  controlName="fromDate" tabIndex="4"  gmClass="InputArea" 
					onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;<fmtChargesApproval:message key="LBL_TO_DATE"/>:&nbsp;<gmjsp:calendar SFFormName="frmChargesApproval" controlName="toDate"  gmClass="InputArea" 
					onFocus="changeBgColor(this,'#AACCE8');"  tabIndex="5" onBlur="changeBgColor(this,'#ffffff');"/></td>
			<td height="25" class="RightTableCaption" align="right">&nbsp;<fmtChargesApproval:message key="LBL_CHARGES_TYPE"/>:</td>
			<td width=8%>&nbsp;<gmjsp:dropdown controlName="chargesFor"  SFFormName='frmChargesApproval' SFSeletedValue="chargesFor"  defaultValue= "[Choose One]"	
						SFValue="alType" codeId="CODENMALT" codeName="CODENM" onChange="javascript:fnLoadIncludeFilters(this.value);fnhideDiv();" /></td>
			<td> <table>
			<tr> <td align="right"><div id="missingsix"><html:checkbox property="shwDtl" name="frmChargesApproval"/></div></td>
			     <td class="RightTableCaption">&nbsp;<fmtChargesApproval:message key="LBL_SHOW_DETAILS"/></td> 
				 <td><div id="normalload"><fmtChargesApproval:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}" style="width: 5em;height: 22px;" gmClass="button" onClick="fnGo();" buttonType="Load" /></div></td>
			</tr></table></td>	 			
		</tr>
			<tr id="missingone" style="display: none;"><td class="LLine" height="1" colspan="8"></td></tr>	
			<tr id="missingtwo" style="display: none;" class="shade">
				<td height="25" class="RightTableCaption" align="right">&nbsp;<fmtChargesApproval:message key="LBL_REQUEST_ID"/>:</td>
				<td>&nbsp;<html:text maxlength="50" property="requestID" size="20"   
							styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="3" />
				</td>
				<td height="25" class="RightTableCaption" align="right" width="15%">&nbsp;<fmtChargesApproval:message key="LBL_SHOW"/>:</td>
				<td colspan="6" class="RightTableCaption">&nbsp;
				 <input type='checkbox' name="shwDist" />&nbsp;<fmtChargesApproval:message key="LBL_DISTRIBUTOR"/>
				 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				 <input type='checkbox' name="shwDirRep" />&nbsp;<fmtChargesApproval:message key="LBL_DIRECT_SALES_REP"/>
				</td>
			</tr>
			<tr id="missingthree" style="display: none;"><td class="LLine" height="1" colspan="8"></td></tr>	
			<tr id="missingfour" style="display: none;" >
				<td height="25" colspan="8">
					<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="15%" align="center" class="RightTableCaption">&nbsp;<html:checkbox property="chk_ticket" name="frmChargesApproval"/>&nbsp;<fmtChargesApproval:message key="LBL_TICKET"/></td>
							<td align="center" class="RightTableCaption">&nbsp;<html:checkbox property="chk_repname" name="frmChargesApproval"/>&nbsp;<fmtChargesApproval:message key="LBL_REP_NAME"/></td>
							<td align="center" class="RightTableCaption">&nbsp;<html:checkbox property="chk_assocrepname"  name="frmChargesApproval"/>&nbsp;<fmtChargesApproval:message key="LBL_ASSOC_REP"/></td>
							<td align="center" class="RightTableCaption">&nbsp;<html:checkbox property="chk_listprice" name="frmChargesApproval"/>&nbsp;<fmtChargesApproval:message key="LBL_LIST_PRICE"/></td>
							<td align="center" class="RightTableCaption">&nbsp;<html:checkbox property="chk_consignment" name="frmChargesApproval"/>&nbsp;<fmtChargesApproval:message key="LBL_CONSIGNMENT"/></td>
							<td>
							<fmtChargesApproval:message key="BTN_LOAD" var="varLoad"/>
							<gmjsp:button value="${varLoad}" style="width: 5em;height: 22px;" gmClass="button" onClick="fnMissingDetails();" buttonType="Load" /></td>							
						</tr>
					</table>
				</td>
				
			</tr>
		<tr><td class="LLine" height="1" colspan="8"></td></tr>	
		<%if(!chargesFor.equals("50891")){%>	
		<tr>
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
			<td colspan="8"><div id="otherdata">	
				<jsp:include page="/include/GmIncludeChargeApprovalDetails.jsp" />
				</div>
			</td>
		</tr>	
		<%}else{ %>
		<tr>
			<td colspan="8"><div id="missingdata">	
				<jsp:include page="/include/GmMissingChargesDetails.jsp" >
				<jsp:param value="FORMNAME" name="frmChargesApproval"/>
				</jsp:include></div>			
			</td>
		</tr>	
		<%}%>
		<tr>
			<td colspan="5" align="center">
				<div id="messagedata" style="visibility: hidden;"><fmtChargesApproval:message key="LBL_NOTHING_FOUND_TO_DISPLAY"/></div></td>
		</tr>
		<% if ((deptId.equals("2001") || deptId.equals("2006")) && rowsize > 0){ %>
		<tr><td class="LLine" height="1" colspan="8"></td></tr>	
		<tr></tr>
		<tr><td class="aligncenter"  colspan="8">
		<fmtChargesApproval:message key="BTN_SUBMIT" var="varSubmit"/>
              <gmjsp:button value="${varSubmit}" gmClass="button" onClick="fnSubmit();" buttonType="Save" /> 
		    </td>
		</tr>
		<tr></tr>
        <% } %>
	</table>
</html:form>
		<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
