<%
/**********************************************************************************
 * File		 		: GmIncludeLoanerTagPaperwork.jsp
 * Desc		 		: This screen is inclluded in the GmPrintLoanerPaperWork.jsp
 * Version	 		: 1.0
 * author			: Dhana Reddy
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %>

<%@ taglib prefix="fmtIncludeLoanerTagPaperwork" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmIncludeLoanerTagPaperwork.jsp -->

<fmtIncludeLoanerTagPaperwork:setLocale value="<%=strLocale%>"/>
<fmtIncludeLoanerTagPaperwork:setBundle basename="properties.labels.operations.logistics.GmIncludeLoanerTagPaperwork"/>



<%
HashMap hmCsg = new HashMap();
hmCsg = (HashMap)request.getAttribute("hmCONSIGNMENT");
if (hmCsg == null)
{
	hmCsg = (HashMap)session.getAttribute("hmCONSIGNMENT");
}
String strConsignId = GmCommonClass.parseNull((String)hmCsg.get("CONSIGNID"));
String strSetName = GmCommonClass.parseNull((String)hmCsg.get("SETNAME"));
String strEtchId = GmCommonClass.parseNull((String)hmCsg.get("ETCHID"));
strEtchId = strEtchId.equals("")?"LOANER-":strEtchId;
String strSetid = GmCommonClass.parseNull((String)hmCsg.get("SETID"));
String strTagId = GmCommonClass.parseNull((String)hmCsg.get("TAGID"));
String strRuleID = strConsignId + "$" + "50182";
/* strSetName = (strSetName.length()>40)?strSetName.substring(0,40):strSetName; Now we are showing the complete set name */
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Loaner Tag Paperwork</title>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
</head>
<body>
 <table border="0" align="right" cellpadding="0" cellspacing="0" style='width:400px; ;table-layout:fixed; border-left: 2px dotted gray; border-top: 2px dotted gray;'>
 	<tr>
 		<td width="25px" rowspan="16">&nbsp;</td>
 	</tr>
	<tr>
 		<td colspan="4" align="center"><b><fmtIncludeLoanerTagPaperwork:message key="LBL_GLOBUS_MEDICAL"/></b></td>
 	</tr>
	<tr>
	     <td colspan="2"><fmtIncludeLoanerTagPaperwork:message key="LBL_SET_ID"/>:&nbsp;<b><%=strSetid%></b></td>
	     <td colspan="2">
	     	<table cellpadding="0" cellspacing="0" border="0">
	     		<tr>
	     			<td width="145"><fmtIncludeLoanerTagPaperwork:message key="LBL_ETCH_ID"/>:&nbsp;<b><%=strEtchId%></b></td>
	     			<td colspan="2" align="center"><img src='/GmCommonBarCodeServlet?ID=<%=strRuleID%>&type=2D' height="30" width="35" /></td>
	     		</tr>
	     	</table>
	     	</td>
	</tr>
	<tr><td colspan="4" height="7">&nbsp;</td></tr>
	<tr>
	    <td colspan="4" ><fmtIncludeLoanerTagPaperwork:message key="LBL_SET_NAME"/>:&nbsp;<b><%=GmCommonClass.getStringWithTM(strSetName)%></b></td>
	</tr>
	<tr><td colspan="4" height="7">&nbsp;</td></tr> 
	<tr>
		<td colspan="4"><fmtIncludeLoanerTagPaperwork:message key="LBL_CN"/>:&nbsp;<b><%=strConsignId%></b></td>
	</tr>
	<tr><td colspan="4">&nbsp;</td></tr>
	<tr>
	   	<td colspan="2"><fmtIncludeLoanerTagPaperwork:message key="LBL_CHECKED_BY"/>:&nbsp;___________</td>
	    <td colspan="2"><fmtIncludeLoanerTagPaperwork:message key="LBL_PROCESSED_BY"/>:&nbsp;___________</td>
	</tr>
	<tr><td colspan="4">&nbsp;</td></tr>
	<tr>
	    <td colspan="2"><fmtIncludeLoanerTagPaperwork:message key="LBL_CHECKED_DATE"/>:&nbsp;__________</td>
	    <td colspan="2"><fmtIncludeLoanerTagPaperwork:message key="LBL_PROCESSED_DATE"/>:&nbsp;__________</td>
	</tr>
	<tr><td colspan="4" height="7">&nbsp;</td></tr>
	<tr>
	    <td colspan="4">______________________________________________________</td>
	</tr>
</table>
</body>
</html>