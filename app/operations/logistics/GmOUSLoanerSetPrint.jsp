 <%
/**********************************************************************************
 * File		 		: GmLoanerSetPrint.jsp
 * Desc		 		: This screen is used for the print version of Set Loaner
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %> 
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@page import="java.util.Date"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>

<%@ taglib prefix="fmtOUSLoanerSetPrint" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmOUSLoanerSetPrint.jsp -->

<fmtOUSLoanerSetPrint:setLocale value="<%=strLocale%>"/>
<fmtOUSLoanerSetPrint:setBundle basename="properties.labels.operations.logistics.GmOUSLoanerSetPrint"/>


<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session); 
	
	String strApplnDateFmt = strGCompDateFmt;

	HashMap hmReturn = new HashMap();
	HashMap hmReturnShipDetails = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmCONSIGNMENT");

	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean rbPackSlipHeaderInfo = GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
	HashMap hmCompanyAddress = GmCommonClass.fetchCompanyAddress(gmDataStoreVO.getCmpid(), "2000");
	HashMap hmConDetails = new HashMap();
	ArrayList alSetLoad = (ArrayList)request.getAttribute("CONSIGNSETDETAILS");
	String strType = GmCommonClass.parseNull((String)request.getAttribute("hType"));
	String strAddress = GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYADDRESS"));
	String strGMName = GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYNAME"));
	strAddress = strAddress.replaceAll("/", "\n<br>");
	GmResourceBundleBean gmResourceBundleBeanlbl = 
	    GmCommonClass.getResourceBundleBean("properties.labels.operations.logistics.GmOUSLoanerSetPrint", strSessCompanyLocale);
	
	String strAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	strAction = (strAction == null) ? "ViewPrint" : strAction;
	String lblHospitalConsignment = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_HOSPITAL_CONSINGMENT"));
	String lblLoanerStockSheet = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_LOANER_STOCK_SHEET"));
	String strHeader  =	strType.equals("40050")?lblHospitalConsignment:lblLoanerStockSheet;
	
	String strShade = "";
	String strConsignId = "";
	String strConsignAdd = "";
	String strShipAdd = "";
	Date dtDate = null;
	Date dtShippingDt = null;
	String strShowLotNum = GmCommonClass.parseNull((String)rbPackSlipHeaderInfo.getProperty("LOANER.SHOW_CNUM_LN_STK_SHT"));
	
	String strTemp = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strQty = "";
	String strUserName = "";
	String strRefId = "";
	String strAccId = "";
	String strLocAccId = "";
	String strAccNm = "";
	String strLocAccNm = "";
	String strRepNm = "";
	String strLocRepNm = "";
	String strSetID = "";
	String strSetNm = "";
	String strOrderedByLbl = "";
	String strDetailsLbl = "";
	String strModeLbl = "";
	String strTrackLbl = "";
	String strShipMode = "";
	String strTrackNum = "";
	
strHeader = strType.equals("40050")? ( GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("HOSPITAL_CONSIGNMENT")).equals("") ? "Hospital Consignment" : GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("HOSPITAL_CONSIGNMENT")))
			   :(GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("LOANER_STOCK_SHEET")).equals("") ? "Loaner Stock Sheet": GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("LOANER_STOCK_SHEET"))   );	
String strLoanedToLbl     = GmCommonClass.parseNull((String)rbPackSlipHeaderInfo.getProperty("LOANED_TO")).equals("")? "Loaned To" :GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("LOANED_TO")) ;
String strSetDetailsLbl     = GmCommonClass.parseNull((String)rbPackSlipHeaderInfo.getProperty("SET_DETAILS")).equals("")? "Set Details" :GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("SET_DETAILS")) ;
String strDateLbl     = GmCommonClass.parseNull((String)rbPackSlipHeaderInfo.getProperty("DATE")).equals("")? "Date" :GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("DATE")) ;
String strShippingDtLbl     = GmCommonClass.parseNull((String)rbPackSlipHeaderInfo.getProperty("SHIPPING_DATE")).equals("")? "Shipping Date" :GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("SHIPPING_DATE")) ;
String strTxnIDLbl     = GmCommonClass.parseNull((String)rbPackSlipHeaderInfo.getProperty("TRANSACTION_ID")).equals("")? "Trans. ID" :GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("TRANSACTION_ID")) ;		   
String strPartNumLbl  = GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("PART")).equals("")? "Part #" :GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("PART")) ; 
String strPartDescLbl = GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("DESCRIPTION")).equals("")? "Description" :GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("DESCRIPTION")) ;
String strStockQtyLbl  =  GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("STOCK_QTY")).equals("")? "Stock Qty" :GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("STOCK_QTY")) ;
String strCheckoutLbl  = GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("CHECK_OUT")).equals("")? "Check Out" :GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("CHECK_OUT")) ; 
String strCheckInLbl = GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("CHECK_IN")).equals("")? "Check In" :GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("CHECK_IN")) ;
String strUsedLbl  =  GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("USED")).equals("")? "Used" :GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("USED")) ;
String strDispatchAgntLbl = GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("DISPATCH_AGENT")).equals("")? "Dispatch Agent" :GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("DISPATCH_AGENT")) ;
String strNotesLbl  =  GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("NOTES")).equals("")? "NOTES" :GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("NOTES")) ;
String strLotNumLbl  =  GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("LOT_NUM")).equals("")? "Lot#" :GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("LOT_NUM")) ;
String strPrintedByLbl =GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("PRINTED_BY"));
String strPrintedDateLbl =GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("PRINTED_DATE"));	
strLotNumLbl=strShowLotNum.equalsIgnoreCase("yes")?"":"";
String strAccIdLbl = GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("ACCOUNTID")).equals("")? "Account ID" :GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("ACCOUNTID")) ;
strOrderedByLbl = GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("ORDERBY")).equals("")? "Sales Rep" :GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("ORDERBY")) ;
strDetailsLbl = GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("DETAILS")).equals("")? "Set ID/Name" :GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("DETAILS")) ;
strModeLbl = GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("SHIPMODE")).equals("")? "Ship Mode" :GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("SHIPMODE")) ;
strTrackLbl = GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("TRACK")).equals("")? "Tracking #" :GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("TRACK")) ;
String strShippingToHeader  = GmCommonClass.parseNull((String)rbPackSlipHeaderInfo.getProperty("SHIP_TO")).equals("")? "Ship To" :GmCommonClass.parseNull((String) rbPackSlipHeaderInfo.getProperty("SHIP_TO")) ;
String strShippingDt = "";
if (hmReturn != null)
	{
		strConsignId = GmCommonClass.parseNull((String)hmReturn.get("CONSIGNID"));
		strConsignAdd = GmCommonClass.parseNull((String)hmReturn.get("BILLNM"));
		strShipAdd = GmCommonClass.getStringWithTM(GmCommonClass.parseNull((String)hmReturn.get("SETNAME")));
		dtDate = (Date)hmReturn.get("LDATE"); 
		dtShippingDt = (Date)hmReturn.get("SDATE");
		strShippingDt = GmCommonClass.parseNull((String)GmCommonClass.getStringFromDate(dtShippingDt,strApplnDateFmt));
		strUserName = GmCommonClass.parseNull((String)hmReturn.get("ETCHID"));
		strRefId = GmCommonClass.parseNull((String)hmReturn.get("REFID"));
		strSetID = GmCommonClass.parseNull((String)hmReturn.get("SETID"));
		strSetNm = GmCommonClass.parseNull((String)hmReturn.get("SETNAME"));
		
		hmReturnShipDetails = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("SHIPDETAILS"));
		
		strShipAdd = GmCommonClass.parseNull((String)hmReturnShipDetails.get("SHIPADD"));
		strAccId = GmCommonClass.parseNull((String)hmReturnShipDetails.get("ACCID"));
		strRepNm = GmCommonClass.parseNull((String)hmReturnShipDetails.get("REPNM"));		
		strShipMode = GmCommonClass.parseNull((String)hmReturnShipDetails.get("SMODE"));
		strTrackNum = GmCommonClass.parseNull((String)hmReturnShipDetails.get("TRACK"));
		System.out.println("strShipAdd:"+strShipAdd);
	}
	//strConsignId = strConsignId + "$50182";
	int intSize = 0;
	HashMap hcboVal = null;
	String strClass="DtRuleTable700";
	StringBuffer sbStartSection = new StringBuffer();
	sbStartSection.append("<table border=0 cellspacing=0 cellpadding=0><tr><td align=center height=30 id=button>");
	
		//sbStartSection.append("<input type=button value=&nbsp;Print&nbsp; name=Btn_Print class=button onClick=fnPrint();>&nbsp;&nbsp;");
		sbStartSection.append("</td><tr></table>");
			
	sbStartSection.append("<table border=0 class="+strClass+" cellspacing=0 cellpadding=0 align=center>");
	sbStartSection.append("<tr><td bgcolor=#666666 colspan=3></td></tr>");
	sbStartSection.append("<tr><td bgcolor=#666666 width=1></td>");
	sbStartSection.append("<td>");
	sbStartSection.append("<table cellpadding=0 cellspacing=0 border=0 >");
	sbStartSection.append("<tr>");
	sbStartSection.append("<td width=170><img src=");sbStartSection.append(strImagePath);
	sbStartSection.append("/100800.gif width=138 height=60></td>");
	sbStartSection.append("<td class=RightText width=300>");
	sbStartSection.append(strGMName);
	sbStartSection.append("<br>");
	sbStartSection.append(strAddress);
	sbStartSection.append("<br></td>");
	sbStartSection.append("<td align=right class=RightText width=450><font size=+2>");
	sbStartSection.append(strHeader);
	sbStartSection.append("</font>&nbsp;<br><img align=right src='/GmCommonBarCodeServlet?ID=");
	sbStartSection.append(strConsignId + "$50182&type=2d"); 
	sbStartSection.append("	' height=40 width=40/></td>"); 
	sbStartSection.append("	<tr> ");						
	sbStartSection.append("	<td colspan=3>");
	sbStartSection.append("	<img align=left src='/GmCommonBarCodeServlet?type=2d&ID=");
	sbStartSection.append(strConsignId); 
	sbStartSection.append("' height=40 width=40 />&nbsp;&nbsp;</td>");							
	sbStartSection.append("	</tr> ");
	sbStartSection.append("</tr>");
	sbStartSection.append("<tr><td height=1 colspan=3></td></tr>");
	sbStartSection.append("<tr><td bgcolor=#666666 colspan=3></td></tr>");
	sbStartSection.append("<tr><td colspan=3>");
	sbStartSection.append("<table border=0 cellspacing=0 cellpadding=0>");
	sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption>");
	sbStartSection.append("<td height=24 align=center width=250>&nbsp;"+strLoanedToLbl+"</td>");
	sbStartSection.append("<td bgcolor=#666666 width=1 rowspan=15></td>");
	sbStartSection.append("<td width=250 align=center>&nbsp;"+strShippingToHeader+"</td>");
	sbStartSection.append("<td bgcolor=#666666 width=1 rowspan=15></td>");
	sbStartSection.append("<td width=100 align=center>&nbsp;"+strDateLbl+"</td>");
	sbStartSection.append("<td width=100 align=center>&nbsp;"+strTxnIDLbl+"</td>");
	sbStartSection.append("</tr><tr><td bgcolor=#666666 height=1 colspan=6></td></tr>");
	sbStartSection.append("<tr><td class=RightText rowspan=9 valign=top>&nbsp;");sbStartSection.append(strConsignAdd);
	sbStartSection.append("</td>");
	sbStartSection.append("<td rowspan=9 class=RightText valign=top>");sbStartSection.append(strShipAdd);
	sbStartSection.append("</td>");
	sbStartSection.append("<td height=25 class=RightText align=center>&nbsp;");sbStartSection.append(GmCommonClass.getStringFromDate(dtDate,strApplnDateFmt));
	sbStartSection.append("</td><td class=RightText align=center>&nbsp;");
	sbStartSection.append(strConsignId);
	sbStartSection.append("</td></tr><tr><td bgcolor=#666666 height=1 colspan=2></td></tr>");
	sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption><td align=center colspan=2>"+strShippingDtLbl+"</td></tr>");
	sbStartSection.append("<tr><td bgcolor=#666666 height=1 colspan=2></td></tr>");
	sbStartSection.append("<tr><td align=center class=RightText colspan=2 height=19>"+strShippingDt+"</td></tr>");
	sbStartSection.append("<tr><td bgcolor=#666666 height=1 colspan=2></td></tr>");
	sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption><td align=center width=120>"+strAccIdLbl+"</td><td align=center>Tag ID</td></tr>");
	sbStartSection.append("<tr><td bgcolor=#666666 height=1 colspan=2></td></tr>");
	sbStartSection.append("<tr><td align=center class=RightText>&nbsp;");
	sbStartSection.append(strAccId);
	sbStartSection.append("<BR></td><td align=center height=25 nowrap class=RightText>&nbsp;");sbStartSection.append(strUserName);
	sbStartSection.append("</td></tr><tr><td bgcolor=#666666 height=1 colspan=6></td></tr>");

	sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption>");
	sbStartSection.append("<td height=24 align=center width=250>&nbsp;"+strOrderedByLbl+"</td>");
	sbStartSection.append("<td width=250 align=center>&nbsp;"+strDetailsLbl+"</td>");
	sbStartSection.append("<td width=80 align=center>&nbsp;&nbsp;"+strModeLbl+"</td>");
	sbStartSection.append("<td width=120 align=center>"+strTrackLbl+"</td>");
	sbStartSection.append("</tr><tr><td bgcolor=#666666 height=1 colspan=6></td></tr>");
	
	sbStartSection.append("<tr><td height=24 align=center width=250>&nbsp;"+strRepNm+"</td>");
	sbStartSection.append("<td width=250 align=center>&nbsp;"+strSetID+" "+strSetNm+"</td>");
	sbStartSection.append("<td width=100 align=center>&nbsp;"+strShipMode+"</td>");
	sbStartSection.append("<td width=100 align=center>&nbsp;"+strTrackNum+"</td>");
	sbStartSection.append("</tr><tr><td bgcolor=#666666 height=1 colspan=6></td></tr>");
	
	sbStartSection.append("</table></td></tr>");
	sbStartSection.append("<tr><td colspan=3><table border=0 cellspacing=0 cellpadding=0>");
	sbStartSection.append("<tr class=RightTableCaption><td width=25 height=24>S#</td><td width=60 height=24>&nbsp;"+strPartNumLbl+"</td>");
	sbStartSection.append("<td width=450>"+strPartDescLbl+"</td><td width=100 align=center>" +strLotNumLbl  + "</td><td align=left width=60 align=center>"+strStockQtyLbl+"</td>");
	sbStartSection.append("<td width=40 align=center>"+strCheckoutLbl+"</td><td width=40 align=center>"+strCheckInLbl+"</td><td width=40 align=center>"+strUsedLbl+"</td></tr>");
	sbStartSection.append("</table></td></tr></table></td><td bgcolor=#666666 width=1></td></tr></table>");

	StringBuffer sbEndSection = new StringBuffer();
	sbEndSection.append("<table class="+strClass+" cellspacing=0 cellpadding=0 align=center>");
	sbEndSection.append("<tr><td bgcolor=#666666 width=1 rowspan=7></td><td height=1 width=698></td><td bgcolor=#666666 width=1 rowspan=7></td></tr>");
	sbEndSection.append("<tr><td height=10>&nbsp;</td></tr>");
	sbEndSection.append("<tr><td height=50>&nbsp;</td></tr>");
	sbEndSection.append("<tr><td height=20 class=RightText valign=top><B>"+strDispatchAgntLbl+"</B></td></tr>");
	sbEndSection.append("<tr><td height=30 class=RightText valign=top><B><u>"+strNotesLbl+"</u></B>:");
	//sbEndSection.append("<BR>-Please sign and return the Consignment Agreement attached with this Form");
	sbEndSection.append("</td></tr>");
	sbEndSection.append("<tr><td height=1></td></tr></table>");
	

%>
<HTML>
<HEAD>
<TITLE> GlobusOne Enterprise Portal: Loaner Set - Stock Sheet </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Rule.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<style>
TR.PageBreak {
     page-break-after: always;
}
</style>
<script>

function fnPrint()
{
	window.print();
}
function fnClose(){
	window.close();
}
var tdinnner = "";
function hidePrint()
{
	buttonTableObj=document.getElementById("butTableF");
	buttonTableObj.style.display='none';
	buttonTableObj=document.getElementById("butTableS");
	buttonTableObj.style.display='none';
}

function showPrint()
{
	buttonTableObj=document.getElementById("butTableF");
	buttonTableObj.style.display='block';
	buttonTableObj=document.getElementById("butTableS");
	buttonTableObj.style.display='block';

}
</script>
</HEAD>

<BODY topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<!--StartSection starts here-->
<%
if(strAction.equals("ViewPrint")) 
	{ 
%>
<table border=0 class="DtRuleTable700" cellspacing=0 cellpadding=0 id="butTableF" >
	<tr>
	<td align=center height=30 id=button>
	<fmtOUSLoanerSetPrint:message key="BTN_PRINT" var="varPrint"/>
	<input type=button value=&nbsp;${varPrint}&nbsp; name=Btn_Print class=button onClick=fnPrint();>&nbsp;&nbsp;
	<fmtOUSLoanerSetPrint:message key="BTN_CLOSE" var="varClose"/>
	<input type="button" value="&nbsp;${varClose}Close&nbsp;" class="button" onClick="fnClose();" tabindex=25>
	</td></tr></table>
<%
	}
out.print(sbStartSection);%>
<!--StartSection ends here-->
	<table border="0" class="DtRuleTable700" cellspacing="0" cellpadding="0" align=center >
<%
	double dbCount = 0.0;
	HashMap hmLoop = new HashMap();
	HashMap hmTempLoop = new HashMap();

	String strNextPartNum = "";
	int intCount = 0;
	String strColor = "";
	String strLine = "";
	String strPartNumHidden = "";
	String strPrice = "";
	String strTotal = "";
	String strAmount = "";
	String strCtrlNum = "";
	double dbAmount = 0.0;
	double dbTotal = 0.0;
	int intQty = 0;
	intSize = alSetLoad.size();
	
	for (int i=0;i<intSize;i++)
	{
		hmLoop = (HashMap)alSetLoad.get(i);
		if (i<intSize-1)
		{
			hmTempLoop = (HashMap)alSetLoad.get(i+1);
			strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("PNUM"));
		}
		strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
		strPartNumHidden = strPartNum;
		strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
		strQty = GmCommonClass.parseZero((String)hmLoop.get("QTY"));
		strCtrlNum = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
		strTemp = strPartNum;
		
		if (strType.equals("40050"))
		{
			strPrice = GmCommonClass.parseZero((String)hmLoop.get("PRICE"));

			intQty = Integer.parseInt(strQty);
			dbAmount = Double.parseDouble(strPrice);
			dbAmount = intQty * dbAmount; // Multiply by Qty
			strAmount = ""+dbAmount;

			dbTotal = dbTotal + dbAmount;
			strTotal = "$"+dbTotal;
		}
		else
		{
			strPrice = "-";
			strAmount = "-";
			strTotal = "-";
		}
		/*if (strPartNum.equals(strNextPartNum))
		{*/
			//intCount++;
			strLine = "<TR><TD colspan=10 height=1 bgcolor=#eeeeee></TD></TR>";
		/*}

		else
		{
			strLine = "<TR><TD colspan=9 height=1 bgcolor=#eeeeee></TD></TR>";
			if (intCount > 0)
			{
				strPartNum = "";
				strPartDesc = "";
				strQty = "";
				strLine = "";
			}
			else
			{
				//strColor = "";
			}
			intCount = 0;
		}*/

		/*if (intCount > 1)
		{
			strPartNum = "";
			strPartDesc = "";
			strQty = "";
			strLine = "";
		}
		*/
		strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
		out.print(strLine);
%>
		<tr>
			<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif"></td>
			<td class="RightText" width="25"><%=i+1%></td>
			<td class="RightText" width="60" height="20">&nbsp;<%=strPartNum%></td>
			<td class="RightText" width="450"><%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
			<td class="RightText" align="center" width="100">&nbsp;<%=strCtrlNum%></td>
			<td class="RightText" align="center" width="60">&nbsp;<%=strQty%></td>
			<td align="center"><input type="text" size="4" class="RightText"></td>
			<td align="center"><input type="text" size="4" class="RightText"></td>
			<td align="center"><input type="text" size="4" class="RightText"></td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
<%
		if ((i+1)%24 == 0 )
		{
%>
		<tr><td colspan="10" height="1" bgcolor="#666666"></td></tr>
		<tr><td colspan="10" height="20" class="RightText" align="right"><fmtOUSLoanerSetPrint:message key="LBL_CONT_ON_NXT_PAGE"/>-></td></tr>
		</table>
		<p STYLE="page-break-after: always"></p>
<%
		out.print(sbStartSection);
%>
		<table border="0" class="DtRuleTable700" cellspacing="0" cellpadding="0" align=center>
<%
		} //end of IF

	} // end of FOR
%>

		<tr><td colspan="10" height="1" bgcolor="#666666"></td></tr>	
	</table>
<!--EndSection starts here-->
<%out.print(sbEndSection);
if(strAction.equals("ViewPrint")) 
	{ 

%>
<table border=0 class="DtTable700" cellspacing=0 cellpadding=0 align=center>
	<tr>
		<td>
			<jsp:include page="/common/GmRuleDisplayInclude.jsp">
			<jsp:param name="Show" value="true" />
			<jsp:param name="Fonts" value="false" />
			</jsp:include>
		</td>
	</tr>
</table>
<table border=0 class="DtRuleTable700" cellspacing=0 cellpadding=0 align=center>	
	<tr>
		<td>
				<jsp:include page="/common/GmIncludeDateStamp.jsp">
				<jsp:param name="PRINTEDBYLBL" value="<%=strPrintedByLbl%>" /> 
				<jsp:param name="PRINTEDDATELBL" value="<%=strPrintedDateLbl%>" />
				</jsp:include>
		</td>	
	</tr>
</table>	
<table border=0 class="DtRuleTable700" cellspacing=0 cellpadding=0 align=center id="butTableS">		
	<tr>
		<td height="30" id=button>
		<fmtOUSLoanerSetPrint:message key="BTN_PRINT" var="varPrint"/>
		<input type=button value="&nbsp;${varPrint}&nbsp;" name=Btn_Print class=button onClick=fnPrint();>&nbsp;&nbsp;
		<fmtOUSLoanerSetPrint:message key="BTN_CLOSE" var="varClose"/>
		<input type="button" value="&nbsp;${varClose}&nbsp;" class="button" onClick="fnClose();" tabindex=25>
		</td>
	</tr>	
</table>
<!--EndSection ends here-->

<%
	}
%>

<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
