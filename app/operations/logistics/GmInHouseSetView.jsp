<%
/**********************************************************************************
 * File		 		: GmInHouseSetView.jsp
 * Desc		 		: This screen is used to display an In-House Set's Details
 * Version	 		: 1.0
 * author			: James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>


<!-- operations\logistics\GmInHouseSetView.jsp -->
<%@ taglib prefix="fmtConsSetView" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtConsSetView:setLocale value="<%=strLocale%>"/>
<fmtConsSetView:setBundle basename="properties.labels.include.GmIncludeConsignment"/>
 
<%String strWikiTitle = GmCommonClass.getWikiTitle("SETS_VIEW");%>

<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	log.debug("strAction is "+strAction);
	String strTransId = GmCommonClass.parseNull((String)request.getAttribute("TRANSID"));
	String strCancelTyp = GmCommonClass.parseNull((String)request.getAttribute("CANCELTYP"));
	//String strWikiTitle = GmCommonClass.getWikiTitle("SET_DETAILS");

	HashMap hmCsg = new HashMap();
	// To get the Consignment information 
	hmCsg = (HashMap)request.getAttribute("hmCONSIGNMENT");
	if (hmCsg == null)
	{
		hmCsg = (HashMap)session.getAttribute("hmCONSIGNMENT");
	}
	
	String strReplenishId = GmCommonClass.parseNull((String)request.getAttribute("REPLENISHID"));	
	String strConsignId = GmCommonClass.parseNull((String)hmCsg.get("CONSIGNID"));
	String strLoanStatus = GmCommonClass.parseNull((String)hmCsg.get("LOANSFL"));
	String strStatus = GmCommonClass.parseNull((String)hmCsg.get("LOANSFLCD"));//to get status -20 Pending Return
	String strType = GmCommonClass.parseNull((String)hmCsg.get("CTYPE"));
	String strReturnDt = GmCommonClass.parseNull((String)hmCsg.get("RETURNDT"));
	String strProductRQId = GmCommonClass.parseNull((String)hmCsg.get("PRODLOANERRQID"));  // added strProductRQId need value if the company is ITALY  - PMT-42950 
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: In-House Set's View Mode </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<SCRIPT>
	var replenishId = '<%=strReplenishId%>';
	var val = '<%=strConsignId%>';
	var type = '<%=strType%>';
	var status = '<%=strStatus%>';
	var loanerReqId = '<%=strProductRQId%>';  /*added loanerReqId need value if the company is ITALY  - PMT-42950 */
	
function fnPrintVer()
{
	windowOpener("/GmInHouseSetServlet?hAction=ViewPrint&hConsignId="+val+"&hType="+type+"&status="+status+"&loanerReqId="+loanerReqId,"LoanPrint","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPrintLetter()
{
	windowOpener("/GmInHouseSetServlet?hAction=ViewLetter&hConsignId="+val+"&hType="+type,"LoanPrint","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPrintVerLetter()
{
	windowOpener("/GmInHouseSetServlet?hAction=ViewLetterVer&hConsignId="+val+"&hType="+type+"&status="+status+"&loanerReqId="+loanerReqId,"LoanPrint","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPicSlip()
{	
	windowOpener("<%=strServletPath%>/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="+replenishId,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
}


</SCRIPT>
</head>
<BODY leftmargin="20" topmargin="10">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmConsignSetServlet">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hType" value="<%=strType%>">

	<table border="0" class="DtTable700" width=700 cellspacing="0" cellpadding="0">
		<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>	
		<tr>
			<td rowspan="20" width="1" class="Line"></td>
			<td height="25" class="RightDashBoardHeader" ><fmtConsSetView:message key="LBL_SETS_VIEW"/></td>
			<fmtConsSetView:message key="IMG_ALT_HELP" var = "varHelp"/>	
			<td class="RightDashBoardHeader" align="right"><img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />&nbsp;</td>		
			<td rowspan="20" width="1" class="Line"></td>  
		</tr>
		<tr>
			<td colspan="2">	
				<jsp:include page="/include/GmIncludeConsignment.jsp" >
				<jsp:param name="PARENTJSP" value="SetView"/>
				</jsp:include>
			</td>
			<td></td>
		</tr>		
		<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>	
		
			<% if (!strLoanStatus.equalsIgnoreCase("Pending Check"))
					{
			%>
			<tr>
			<td colspan="2">
				<jsp:include page="/include/GmIncludeConsignSetDetails.jsp" />
			</td>
			<td></td>
			</tr>
			<%		
				}
			%>
			
<%
	if ( (strAction.equalsIgnoreCase("ReturnSet") || strAction.equalsIgnoreCase("ProcessReturnSet") ||  strAction.equalsIgnoreCase("ExtenWithRefill") )  && !strTransId.equals(""))
	{
%>	
		<tr>
			<td colspan="2" align="center" class="RightTextBlue">

				<BR><BR>
<fmtConsSetView:message key="MSG_TRANS_RESULT"/> :<b><%=strTransId%></b>
<BR><BR>
<fmtConsSetView:message key="MSG_TRANS_ON_DASH"/>
			</td>
			<td></td>
		</tr>		
<%
	}

	if (strLoanStatus.equalsIgnoreCase("Pending Shipping") || strLoanStatus.equalsIgnoreCase("Pending Return"))
	{
%>	
		<tr>
			<td colspan="2" align="center">
			<% if (strCancelTyp.equalsIgnoreCase("50322")) {
			%>
			<fmtConsSetView:message key="BTN_PIC_SLIP" var = "varPicSlip"/>
			<gmjsp:button value="&nbsp;${varPicSlip}&nbsp;" gmClass="button" onClick="fnPicSlip();" tabindex="25" buttonType="Save" />&nbsp;&nbsp;&nbsp;
			<%} 
		 else { %>
		 		<fmtConsSetView:message key="BTN_PRINT_VER" var = "varPrintVer"/> 
				<gmjsp:button value="&nbsp;${varPrintVer}&nbsp;" gmClass="button" onClick="fnPrintVer();" tabindex="25" buttonType="Load" />&nbsp;&nbsp;&nbsp;
				<fmtConsSetView:message key="BTN_PRINT_VER_LETR" var = "varPrintVerLtr"/> 
				<gmjsp:button value="&nbsp;${varPrintVerLtr}&nbsp;" gmClass="button" onClick="fnPrintVerLetter();" tabindex="25" buttonType="Load" />
			<%} %>
				<fmtConsSetView:message key="BTN_PRINT_LETR" var = "varPrintLtr"/>
				<gmjsp:button value="&nbsp;${varPrintLtr}&nbsp;" gmClass="button" onClick="fnPrintLetter();" tabindex="25" buttonType="Load" />
				<BR><BR>				
			</td>
			
		</tr>		
<%
	}
%>	
<tr><td colspan="2" height="1" class="Line"></td></tr>
	</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
