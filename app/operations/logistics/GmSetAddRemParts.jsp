 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
/**********************************************************************************
 * File		 			: GmSetAddRemParts.jsp
 * Desc		 		: Used for Adding Removing Parts from a Set
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.StringTokenizer" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtSetAddRemParts" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmSetAddRemParts.jsp -->

<fmtSetAddRemParts:setLocale value="<%=strLocale%>"/>
<fmtSetAddRemParts:setBundle basename="properties.labels.operations.logistics.GmSetAddRemParts"/>



<%
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");

	HashMap hmReturn = (HashMap)session.getAttribute("hmReturn");
	String strhAction = (String)session.getAttribute("hAction") == null?"":(String)session.getAttribute("hAction");
	String strChecked = "";
	String strPurpose = "";

	String strConsignId = (String)session.getAttribute("CONSIGNID") == null?"":(String)session.getAttribute("CONSIGNID");
	log.debug(" Consign ID from session in JSP is " + strConsignId);
	String strPartNums = (String)session.getAttribute("strPartNums") == null?"":(String)session.getAttribute("strPartNums");
	String strOpt = GmCommonClass.parseNull((String)request.getAttribute("hOpt"));
	strOpt = strOpt.equals("")?(String)session.getAttribute("hOpt"):strOpt;
	GmResourceBundleBean gmResourceBundleBeanlbl = 
	    GmCommonClass.getResourceBundleBean("properties.labels.operations.logistics.GmSetAddRemParts", strSessCompanyLocale);
	
	String strNewPartNums = "";
	String strType = "";
	String strReason = GmCommonClass.parseNull((String)session.getAttribute("strREASON"));
	String strShipTo = "";
	String strShipToId =  GmCommonClass.parseNull((String)session.getAttribute("strSessUserId"));
	String strBillTo = "";
	String strFinalComments = "";
	String strShipFl = "0";
	String strDelFl = "";
	String strHeader = "";
	String strLabel1 = "";
	String strMessage = "";
	String strOprnType = "";
	String strEtchId = "";
	String strSelected = "";
 	boolean bolFl0;
	String strOprType = GmCommonClass.parseNull((String)session.getAttribute("strOPRTYPE"));
	String strTxnId = GmCommonClass.parseNull((String)session.getAttribute("TXNID"));

	ArrayList alReason = new ArrayList();
	ArrayList alOprnType = new ArrayList();
	ArrayList alInHouseOperation = new ArrayList();

	HashMap hmConsignDetails = new HashMap();
	HashMap hmParam =  new HashMap();
 

	alReason = (ArrayList)session.getAttribute("REASON");
	alInHouseOperation = (ArrayList)session.getAttribute("OPRTYPE");
	hmParam = (HashMap)session.getAttribute("PARAM");
	log.debug("Values inside hmparam is -- " + hmParam);
	
	if (hmParam != null)
	{
		strType = (String)hmParam.get("TYPE");
		if (strType != "")
		{
			bolFl0 = true;
		}
		strPurpose = (String)hmParam.get("PURPOSE");
		strBillTo = (String)hmParam.get("BILLTO");
		strShipTo = (String)hmParam.get("SHIPTO");
		strShipToId = GmCommonClass.parseNull((String)hmParam.get("SHIPTOID"));
		strFinalComments = (String)hmParam.get("COMMENTS");
		strShipFl = (String)hmParam.get("SHIPFL");
		strChecked = strShipFl.equals("1")?"checked":"";
		strDelFl = (String)hmParam.get("SHIPFL");
	}
	log.debug("hmConsignment values are" +(HashMap)session.getAttribute("hmCONSIGNMENT"));
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: InHouse Item Consign </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="Javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="Javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="Javascript" src="<%=strOperationsJsPath%>/GmSetAddRemParts.js"></script>
<script>
var lblType = '<fmtSetAddRemParts:message key="LBL_TYPE"/>';
var lblreason ='<fmtSetAddRemParts:message key="LBL_REASON"/>';
function fnPrintVer()
{
windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId=<%=strConsignId%>","Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnAgree()
{
	windowOpener("/GmConsignSetServlet?hId=<%=strConsignId%>&hAction=Ack&","Ack","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPicSlip()
{
	val = '<%=strTxnId%>';
	var refid=document.frmSetAddRemParts.hConsignId.value;
	var type='<%=strType%>';	
	//alert(val + '-' + refid);
	<%--windowOpener("<%=strServletPath%>/GmSetAddRemPartsServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");--%>
	windowOpener("/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="+val+"&ruleSource="+type+"&refId="+refid,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
}

var len = <%=alReason.size()%>;

<%
	int intSize = alReason.size();
	HashMap hcboVal = new HashMap();
	for (int i=0;i<intSize;i++)
	{
		hcboVal = (HashMap)alReason.get(i);
%>
	var arr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%>,<%=hcboVal.get("CODENMALT")%>";
<%
	}
%>
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="javascript:fnSetReason();">
<xml src="<%=strXmlPath%>/<%=strOpt%>.xml" id="xmldso" async="false"></xml>

<FORM name="frmSetAddRemParts" method="POST" action="<%=strServletPath%>/GmSetAddRemPartsServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hOpt" value="<%=strOpt%>">
<input type="hidden" name="hDelPartNum" value="">
<input type="hidden" name="Cbo_Type" value="<%=strType%>">
<input type="hidden" name="Cbo_BillTo" value="01">
<input type="hidden" name="Cbo_ShipTo" value="<%=strShipTo%>">
<input type="hidden" name="hMode" value="<%=strOpt%>">
<input type="hidden" name="hReasonId" value="<%=strReason%>">
<input type="hidden" name="hTxnId" value="<%=strTxnId%>">
	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="5"></td>
			<%if (strOpt.equals("4127")){%>
				<td height="25" class="RightDashBoardHeader"><fmtSetAddRemParts:message key="LBL_PRODUCT_PROCESS_REFILLS"/></td>
			<%}else if (strOpt.equals("4119")){ %>
				<td height="25" class="RightDashBoardHeader"><fmtSetAddRemParts:message key="LBL_IN-HOUSE_PROCESS_REFILLS"/></td>
			<%} %>			
			<td bgcolor="#666666" width="1" rowspan="5"></td>
		</tr>
		<tr><td bgcolor="#666666" height="1"></td></tr>
		<tr>
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
			<td>
				<jsp:include page="/include/GmIncludeConsignment.jsp" />
			
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
<%
		if (!strhAction.equals("Load") )
		{
%>		
					<tr>
						<td class="RightText" HEIGHT="25" align="right">&nbsp;<b><fmtSetAddRemParts:message key="LBL_TRANSACTION_ID"/>:</b></td>
						<td class="RightText">&nbsp;<%=strTxnId%>
						</td>
					</tr>
<%		}
%>

					<tr>
						<td class="RightText" HEIGHT="25" align="right"><font color="red">*</font>&nbsp;<b><fmtSetAddRemParts:message key="LBL_TYPE"/>:</b></td>
						<td class="RightText">&nbsp;<select name="Cbo_OprType" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" onChange="javascript:fnSetReason()">
								<option value="0" ><fmtSetAddRemParts:message key="LBL_CHOOSE_ONE"/>
<%
			intSize = alInHouseOperation.size();
			hcboVal = new HashMap();
	  		for (int i=0;i<intSize;i++)
	  		{
	  			hcboVal = (HashMap)alInHouseOperation.get(i);
				strSelected = strOprType.equals(hcboVal.get("CODEID"))?"selected":"";
%>
								<option <%=strSelected%> value="<%=hcboVal.get("CODEID")%>"><%=hcboVal.get("CODENM")%></option>
<%	  		}
%>
							</select>
						</td>
					</tr>
					<tr>
						<td class="RightText" HEIGHT="25" align="right"><font color="red">*</font>&nbsp;<b><fmtSetAddRemParts:message key="LBL_REASON"/>:</b></td>
						<td class="RightText" id="tdreason">&nbsp;<select name="Cbo_Reason" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
							<option value="0" ><fmtSetAddRemParts:message key="LBL_CHOOSE_ONE"/>
						</td>
					</tr>
					
					<tr>
						<td class="RightText" valign="top" align="right" HEIGHT="24"><b><fmtSetAddRemParts:message key="LBL_COMMENTS"/>:</b></td>
						<td>&nbsp;<textarea name="Txt_Comments" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1 rows=5 cols=50 value="<%=strFinalComments%>"><%=strFinalComments%></textarea></td>
					</tr>
<%
					if (strOpt.equals("InHouse"))
					{
%>					
					<tr class="shade">
						<td class="RightText" align="right" HEIGHT="24"><b><fmtSetAddRemParts:message key="LBL_SHIPPING_REQUIRED"/> ?:</b></td>
						<td><input type="checkbox" name="Chk_ShipFlag" onFocus="changeBgColor(this,'#AACCE8');" <%=strChecked%> onBlur="changeBgColor(this,'#ffffff');" tabindex=2></td>
					</tr>
<%
					}else{
%>
					<tr>
						<td colspan="2"><input type="hidden" name="Chk_ShipFlag" value="1"></td>
					</tr>										
<%
					}
		if (!strhAction.equals("Load") && !strhAction.equals("OrdPlaced"))
		{
%>					
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr>
						<td height="30" align="center" colspan="2" class="RightTextBlue"><br><b><fmtSetAddRemParts:message key="LBL_SHOPPING_CART"/></b><br><fmtSetAddRemParts:message key="LBL_ENTER_PART#_GO_ADD_ITEMS"/>.<br>&nbsp;&nbsp;&nbsp;<input type="text" size="40" value="" name="Txt_PartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
						<fmtSetAddRemParts:message key="BTN_GO" var="varGo" />
						<gmjsp:button value="&nbsp;${varGo}&nbsp;" name="Btn_GoCart" gmClass="button" onClick="fnAddToCart();" tabindex="16" buttonType="Load" /><br><br>
						</td>
					</tr>
<%
		}
		if (!strhAction.equals("Load"))
		{
			String strStockLabel = "";
							if (strOpt.equals("QuaranOut") || strOpt.equals("InvenIn"))
							{
								strStockLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_IN_QUARANTINE"));
							}else if (strOpt.equals("BulkIn")){
								strStockLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_IN_BULK"));
							}else{
								strStockLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_IN_SHELF"));
							}		
%>	

					<tr>
						<td align="center" colspan="2" valign="top">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td align="center" width="80" height="25"><fmtSetAddRemParts:message key="LBL_PART_#"/></td>
									<td align="center" width="200"><fmtSetAddRemParts:message key="LBL_DESCRIPTION"/></td>
									<td align="center" width="70"><%=strStockLabel%></td>
									<td align="center" width="70"><fmtSetAddRemParts:message key="LBL_SET_QTY"/></td>
									<td align="center" width="70"><fmtSetAddRemParts:message key="LBL_QTY_IN_SET"/></td>
									<td align="center" width="80"><fmtSetAddRemParts:message key="LBL_QUANTITY"/></td>
								</tr>
								<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
<%
			String strTotal = "";
			if (!strPartNums.equals(""))
			{
					hmReturn = (HashMap)session.getAttribute("hmOrder");
					HashMap hmCart = (HashMap)session.getAttribute("hmCart");
					StringTokenizer strTok = new StringTokenizer(strPartNums,",");
					String strPartNum = "";
					HashMap hmVal = new HashMap();
					HashMap hmLoop = new HashMap();
					String strId = "";
					String strDesc = "";
					String strPrice = "";
					String strSetQty = "";
					String strMissingQty = "";

					String strAmount = "";
					String strQty = "1";
					String strStock = "";
					String strShade = "";
					int i=0;
					boolean bolFl = false;
					boolean bolFl2 = false;
					int intQty = 0;
					if (strTok.countTokens() == 1)
					{
						bolFl2 = true;
					}
					while (strTok.hasMoreTokens())
					{
						i++;
						strPartNum 	= strTok.nextToken();
						if (hmCart != null)
						{
							hmLoop = (HashMap)hmCart.get(strPartNum);
							if (hmLoop != null)
							{
								strQty = (String)hmLoop.get("QTY");
							}
							else
							{
								strQty = "1";
							}
						}

						strQty = strQty == null?"1":strQty;
						intQty = Integer.parseInt(strQty);
						hmVal = (HashMap)hmReturn.get(strPartNum);
						if (hmVal.isEmpty())
						{
							//bolFl = false;
						}
						else if (hmVal.isEmpty() && bolFl2)
						{
							bolFl = false;
						}
						else
						{
							strNewPartNums = strNewPartNums + strPartNum + ",";
							bolFl = true;
							strId = (String)hmVal.get("ID");
							strDesc = (String)hmVal.get("PDESC");
							strPrice = (String)hmVal.get("PRICE");
							strSetQty = GmCommonClass.parseZero((String)hmVal.get("SETQTY"));
							strMissingQty = GmCommonClass.parseZero((String)hmVal.get("MISSINGQTY"));
							int iQtyInSet = Integer.parseInt(strSetQty) - Integer.parseInt(strMissingQty);
							strStock = GmCommonClass.parseZero((String)hmVal.get("STOCK"));
							
							int intStock = Integer.parseInt(strStock);
							strStock = intStock < 0?"0":strStock;
							strShade	= (i%2 == 0)?"class=Shade":""; //For alternate Shading of rows

%>
								<tr <%=strShade%>>
									<td class="RightText" align="center" height="20"  width="70">
<%
							if (!strhAction.equals("OrdPlaced"))
							{
%>									<fmtSetAddRemParts:message key="LBL_REMOVE_ITEM_FROM_CART" var="varRemoveItemFromCart"/>
									<a  href="javascript:fnRemoveItem('<%=strId%>');"><img border="0" Alt='${varRemoveItemFromCart}' valign="bottom" src="<%=strImagePath%>/btn_remove.gif" height="15" width="15"></a><%}%>&nbsp;<%=strId%></td>
									<td class="RightText"  width="300"><%=GmCommonClass.getStringWithTM(strDesc)%></td>
									<td class="RightText" align="center"  width="70"><%=strStock%><input type="hidden" name="hStock<%=i%>" value="<%=strStock%>"></td>
									<td class="RightText" align="center"  width="70"><%=strSetQty%><input type="hidden" name="hQtyInCsgSet<%=i%>" value="<%=strSetQty%>">
									<input type="hidden" name="hPrice<%=i%>" value="<%=strPrice%>">
									</td>
									<td class="RightText" align="center"  width="70"><%=iQtyInSet%></td>
									<td class="RightText" align="center">
<%
							if (!strhAction.equals("OrdPlaced"))
							{
%>
										&nbsp;<input type="text" size="5" value="<%=strQty%>"  width="80" name="Txt_Qty<%=i%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
<%
							}else{	
%>
										<%=strQty%>
<%							}%>
									</td>
								</tr>
<%
						}
					}
						if (!strNewPartNums.equals(""))
						{
							strNewPartNums = strNewPartNums.substring(0,strNewPartNums.length()-1);
						}
						if (bolFl)
						{
%>
								<tr>
									<td colspan="6" height="1" bgcolor="#666666"></td>
								</tr>
<%
						}						

						if (!bolFl)
						{
%>
								<tr>
									<td colspan="6" height="30" class="RightTextBlue" align="center"><fmtSetAddRemParts:message key="MSG_NO_DATA"/>
</td>
								</tr>
<%
						}
}else{ %>
								<tr>
									<td colspan="6" height="30" class="RightTextBlue" align="center"><fmtSetAddRemParts:message key="MSG_NO_DATA"/>
									</td>
								</tr>
<%
			}
		}
%>
							</table><input type="hidden" name="hPartNums" value="<%=strNewPartNums%>">
						</td>
					</tr>
<%
		if (strhAction.equals("Load"))
		{
%>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>					
					<tr>
						<td colspan="2" height="30" align="center">
						<fmtSetAddRemParts:message key="BTN_INITIATE" var="varInitiate"/>
							<gmjsp:button value="${varInitiate}" gmClass="button" onClick="fnInitiate();" tabindex="13" buttonType="Save" />
						</td>
					</tr>
<%
		}else if (strhAction.equals("PopOrd")){
%>
					<tr>
						<td colspan="3" align="center" height="30">
						<fmtSetAddRemParts:message key="BTN_UPDATE_QTY" var="varUpdateQty"/>
							<gmjsp:button value="&nbsp;${varUpdateQty}&nbsp;" name="Btn_UpdateQty" gmClass="button" onClick="fnUpdateCart();" buttonType="Save" />
							&nbsp;&nbsp;
							<fmtSetAddRemParts:message key="LBL_PLAACE_ORDER" var="varPlaceOrder"/>
							<gmjsp:button value="&nbsp;${varPlaceOrder}&nbsp;" name="Btn_PlaceOrd" gmClass="button" onClick="fnPlaceOrder();"  buttonType="Save" />
						</td>
					<tr>
<%	
	}else if (strhAction.equals("OrdPlaced"))
	{
%>
					<tr>
						<td colspan="2" height="30" align="center">
						<fmtSetAddRemParts:message key="BTN_PIC_SLIP" var="varPICSlip"/>
							<gmjsp:button value="${varPICSlip}" gmClass="button" onClick="fnPicSlip();" tabindex="13" buttonType="Save" />&nbsp;&nbsp;
						</td>
					</tr>
<%
	}								
%>
				</table>
			 </td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
    </table>
<%--
<script>
<%
	if (strhAction.equals("OrdPlaced"))
	{
%>
//	alert("Your consignment has been placed. Please print PIC Slip");
<%
	}
%>
</script>
--%>  
</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
