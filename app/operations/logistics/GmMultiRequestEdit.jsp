<%
/**********************************************************************************
 * File		 		: GmMultiRequestEdit jsp
 * Desc		 		: Modify Requests edit for Multi sets  
 * 
************************************************************************************/
%>
<!-- \operations\logistics\GmMultiRequestEdit.jsp-->  
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtMultiReqEdit" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtMultiReqEdit:requestEncoding value="UTF-8" />
<fmtMultiReqEdit:setLocale value="<%=strLocale%>"/>
<fmtMultiReqEdit:setBundle basename="properties.labels.operations.logistics.GmMultiRequestEdit"/>

<table border="0" width="100%"  cellspacing="0" cellpadding="0">	
		<tr class="ShadeRightTableCaption">
			<td height="25"  colspan="8"><fmtMultiReqEdit:message key="MULTI_REQUEST_EDIT_HEADER"/></td>			
		</tr>	
		<tr><td class="line" colspan="2"></td></tr>
			
		<tr><td colspan="10" height="1" bgcolor="#cccccc"></td></tr>
		
		     <tr class="oddshade"> 
		       <td width="25%" height="25" class="RightTableCaption" align="right" ><fmtMultiReqEdit:message key="LBL_REQUEST_DATE" />&nbsp;:&nbsp;</td> 
				<td id="lbl_request_date" align="left" width="400">
				</td> 
				<td class="RightText">
				    &nbsp;&nbsp;
				</td> 
				<td class="RightText">
				    &nbsp;&nbsp;
				</td> 
			</tr>
			
			  <tr class="evenshade"> 
			   <td width="25%"  height="25" class="RightTableCaption" align="right" ><fmtMultiReqEdit:message key="LBL_SOURCE" />&nbsp;:&nbsp;</td> 
				 <td id="lbl_source" align="left" width="400"> 		 
				</td>  
				 <td width="25%" height="25" class="RightTableCaption" align="right" ><fmtMultiReqEdit:message key="LBL_REQ_TRANS_ID" />&nbsp;:&nbsp;</td> 
				<td id="lbl_req_trans_id" align="left"width="400">
				</td>  
			</tr>
			
			<tr class="oddshade"> 
			 <td height="25" class="RightTableCaption" align="right" ><fmtMultiReqEdit:message key="LBL_SET_ID" />&nbsp;:&nbsp;</td> 
				 <td id="lbl_set_id" align="left"width="400"> </td>  
				 
			 <td height="25" class="RightTableCaption" align="right" ><fmtMultiReqEdit:message key="LBL_SET_NAME" />&nbsp;:&nbsp;</td> 	 
				
				<td id="lbl_set_name" align="left"width="400">
				</td>  
			</tr>
			
			<tr class="evenshade"> 
				<td class="RightTableCaption" height="25" align="right">&nbsp; <fmtMultiReqEdit:message key="LBL_REQ_TYPE"/>&nbsp;:&nbsp;</td>
				
				<td id="lbl_req_type" align="left" width="400">	</td>  
				 
				<td height="25" class="RightTableCaption" align="right" ><font color="red">*</font>  <fmtMultiReqEdit:message key="LBL_REQUEST_TO" />&nbsp;:&nbsp;</td>
				<td class="RightText">
				<gmjsp:dropdown width="200" controlName="strReqTo" SFFormName="frmMultiProcConsign" SFSeletedValue="strReqTo" SFValue="alRequestTo" codeId = "ID" codeName = "NAME"
				 defaultValue="[Choose One]" />
				</td>
			</tr>
			
			<tr class="oddshade"> 
				<td class="RightTableCaption" height="25" align="right">&nbsp;<fmtMultiReqEdit:message key="LBL_REQ_STATUS"/>&nbsp;:&nbsp;</td>
				<td id="lbl_req_status" align="left" width="400"></td>  
				<td class="RightTableCaption" height="25" align="right">&nbsp;<fmtMultiReqEdit:message key="LBL_REQUEST_FOR"/>&nbsp;:&nbsp;</td>
					<td id="lbl_req_for" align="left" width="400">
				</td> 
			</tr>
			<tr class="evenshade"> 
			<td height="25" class="RightTableCaption" align="right" ><font color="red">*</font>  <fmtMultiReqEdit:message key="LBL_PLANNED_SHIP_DATE" />&nbsp;:&nbsp;</td>
				<td class="RightText">
				<gmjsp:calendar  SFFormName='frmMultiProcConsign' SFDtTextControlName="strPlanedShipDt" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/> 
				    &nbsp;&nbsp;
				</td> 
				<td height="25" class="RightTableCaption" align="right" ><font color="red">*</font>  <fmtMultiReqEdit:message key="LBL_REQUIRED_DATE" />&nbsp;:&nbsp;</td> 
				<td class="RightText">
				<gmjsp:calendar  SFFormName='frmMultiProcConsign' SFDtTextControlName="strRequirDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/> 
				    &nbsp;&nbsp;
				</td>  
			</tr>
		<tr><td colspan="10" height="1" bgcolor="#cccccc"></td></tr>
	</table>
<%@ include file="/common/GmFooter.inc"%>
