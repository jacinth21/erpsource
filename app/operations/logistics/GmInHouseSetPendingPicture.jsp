<?xml version="1.0" encoding="ISO-8859-1" ?>
    <%@include file="/common/GmHeader.inc"%>
    <%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
    
    <%
    	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
    	ArrayList CONSIGNSETDETAILS=GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("CONSIGNSETDETAILS"));
		String strAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
		HashMap hmCsg = new HashMap();
		ArrayList alPendPictureCam = new ArrayList();
		String strPendPictureCamId = "";
		// To get the Consignment information 
		hmCsg = (HashMap)request.getAttribute("hmCONSIGNMENT");
		if (hmCsg == null)
		{
			hmCsg = (HashMap)session.getAttribute("hmCONSIGNMENT");
		}

		String strReplenishId = GmCommonClass.parseNull((String)request.getAttribute("REPLENISHID"));	
		String strConsignId = GmCommonClass.parseNull((String)hmCsg.get("CONSIGNID"));
		String strLoanStatus = GmCommonClass.parseNull((String)hmCsg.get("LOANSFL"));
		String strType = GmCommonClass.parseNull((String)hmCsg.get("CTYPE"));
		String strReturnDt = GmCommonClass.parseNull((String)hmCsg.get("RETURNDT"));
		strPendPictureCamId = GmCommonClass.parseNull((String)request.getAttribute("strPendPictureCamId"));
		alPendPictureCam = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALPENDPICTURECAM"));
		String strDefaultComment = GmCommonClass.parseNull((String)GmCommonClass.getRuleValue("LOANER_SCREEN_PP","DEFAULT_COMMENT")); 
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%><html xmlns="http://www.w3.org/1999/xhtml">


<%@ taglib prefix="fmtInHouseSetPendingPicture" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmInHouseSetPendingPicture.jsp -->

<fmtInHouseSetPendingPicture:setLocale value="<%=strLocale%>"/>
<fmtInHouseSetPendingPicture:setBundle basename="properties.labels.operations.logistics.GmInHouseSetPendingPicture"/>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Process Loaner Set - Pending Picture</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css"/>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmInHouseSetPendingPicture.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script type="text/javascript">
var sServletPath = '<%=strServletPath%>';
var val = '<%=strConsignId%>';
var type = '<%=strType%>';
var default_comment = '<%=strDefaultComment%>';
function fnPrintVer()
{

windowOpener("/GmInHouseSetServlet?hAction=ViewPrint&hConsignId="+val+"&hType="+type,"LoanPrint","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}
</script>
</head>
<body onkeyup="fnEnter();" leftmargin="20" topmargin="10" >
<form name="frmPicture" method="post">
<input type="hidden" name="hAction" id="hAction"/>
<input type="hidden" name="hCancelType" />
<input type="hidden" name="hTxnId" />
	<table border="0" cellspacing="0" cellpadding="0" class="GtTable850">
	<tr>
	<td class="RightDashBoardHeader" height="20" align="left"><fmtInHouseSetPendingPicture:message key="LBL_PROCESS_PENDING_PIC"/></td>
	<td height="25" class="RightDashBoardHeader" align="right"><fmtInHouseSetPendingPicture:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%//=strWikiTitle%>');" /></td>
	</tr>
	<tr>
	<td colspan="2">
	<jsp:include page="/include/GmIncludeConsignment.jsp" >
	<jsp:param name="PARENTJSP" value="PendingPic"/>
	</jsp:include>
	</td>
	</tr>
			
	<%
	if(!strAction.equals("Process_Picture")){
	%>
	<tr>
	 <td colspan="2">
               		<jsp:include page="/common/GmIncludeLog.jsp">
					<jsp:param name="LogType" value="" />
					<jsp:param name="LogMode" value="Edit" />
					<jsp:param name="ShowCam" value="true" />				
				</jsp:include>
			</td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<tr><td colspan="2"><input type="radio" name="Chb_AckRback" value="Ack" checked></input> <fmtInHouseSetPendingPicture:message key="LBL_PICTURE_TAKEN"/></td></tr>
	<tr><td colspan="2"><input type="radio" name="Chb_AckRback" value="Rback"></input> <fmtInHouseSetPendingPicture:message key="LBL_ROLLBACK_TO_PENDING_CHECK"/> </td></tr>
	
	<tr><td colspan="2"></td></tr>
	<tr>
		<td align="center" colspan="2">
		<fmtInHouseSetPendingPicture:message key="BTN_SUBMIT" var="varSubmit"/>
		<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="fnSubmit();" buttonType="Save"/>
		</td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<tr><td colspan="2"></td></tr>		
		<%} %>
	</table>
	</form>
	<%@ include file="/common/GmFooter.inc" %>
</body>

</html>