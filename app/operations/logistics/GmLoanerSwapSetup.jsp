<%
/**********************************************************************************
 * File		 		: GmLoanerSwapSetup.jsp
 * Created Date		: Aug 2010 	
 * author			: Velu
************************************************************************************/
%>

<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtLoanerSwapSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmLoanerSwapSetup.jsp -->

<fmtLoanerSwapSetup:setLocale value="<%=strLocale%>"/>
<fmtLoanerSwapSetup:setBundle basename="properties.labels.operations.logistics.GmLoanerSwapSetup"/>

<%
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
String strWikiTitle = GmCommonClass.getWikiTitle("LOANER_SWAP_SETUP");
%>



<HTML>
<HEAD>
<TITLE> Globus Medical: Address Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="styles/gmCommon.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strJsPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmLoanerSwap.js"></script>

<bean:define id="gridData" name="frmSetMapping" property="gridXmlData" type="java.lang.String"> </bean:define>
</HEAD>
<script>

	var objGridData;
	var strGridData='<%=gridData%>';
	
	var strTotLength = strGridData.length;
	var firstIndex=strGridData.indexOf('<column');
	var secIndex=strGridData.indexOf('</column>');

	var strSubStrOne = strGridData.substring(0,firstIndex);
	var strSubStrTwo = strGridData.substring(secIndex+8,strTotLength);
	objGridData = strSubStrOne + strSubStrTwo;

	firstIndex = 0;	secIndex = 0; strSubStrOne = 0; strSubStrTwo = 0;

	firstIndex = objGridData.indexOf('<cell>');
	secIndex = objGridData.indexOf('</cell>');
	
	var strSubStrOne = objGridData.substring(0,firstIndex);
	var strSubStrTwo = objGridData.substring(secIndex+6,strTotLength);
	objGridData = strSubStrOne + strSubStrTwo;

	function displayDropDown(){
		var frm = document.getElementsByName('frmSetMapping')[0];  
		i = 0;
		var consignIdText="";
		var loanerIdText="";
		consignIdlen = frm.consignsetid.length
		var w = frm.loanersetid.selectedIndex;
	    var selected_text = frm.loanersetid.options[w].value;	    
		var index = selected_text.indexOf('L');
		
		var loanerIdText = selected_text.substring(0,index);
		for (i = 0; i < consignIdlen; i++) {
			consignIdText = frm.consignsetid[i].value;
			if(loanerIdText == consignIdText){
				frm.consignsetid[i].selected = consignIdText;
			}
		}
	}
	
</script>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnOnPageLoad();">
<html:form action="/gmSetMappingAction.do" >
<html:hidden property="strOpt"  name="frmSetMapping" value=""/>
	<table border="0" class="GtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtLoanerSwapSetup:message key="LBL_LOANER_CONSIGNMENT_MAPPING"/> </td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			<fmtLoanerSwapSetup:message key="IMG_ALT_HELP" var = "varHelp"/>
			<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
		       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		    </td>
		</tr>
		<tr><td colspan="2" height="0" bgcolor="#666666"></td></tr>
		<tr>
			<td width="698" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">		     
			<!-- Custom tag lib code modified for JBOSS migration changes -->	
                    <tr>
                        <td width="40%" class="RightTableCaption" align="right" HEIGHT="24" ></font><font color="red">*</font>&nbsp;<fmtLoanerSwapSetup:message key="LBL_LOANER_SET_ID"/>:</td> 
                        <td>&nbsp;<gmjsp:dropdown controlName="loanersetid" SFFormName="frmSetMapping" SFSeletedValue="loanersetid"
								SFValue="alresultLSetId" codeId = "ID"  codeName = "IDNAME"  defaultValue= "[Choose One]" onChange="displayDropDown()"/>
                        </td>
                    </tr>
                    <tr><td colspan="2" class="LLine" height="1" ></td></tr>                
                      <tr class="Shade">
                        <td width="40%" class="RightTableCaption" align="right" HEIGHT="24" ></font><font color="red">*</font>&nbsp;<fmtLoanerSwapSetup:message key="LBL_CONSIGN_SET_ID"/>:</td> 
                        <td>&nbsp;<gmjsp:dropdown controlName="consignsetid" SFFormName="frmSetMapping" SFSeletedValue="consignsetid"
								SFValue="alresultCSetId" codeId = "ID"  codeName = "IDNAME"  defaultValue= "[Choose One]" />
                        </td>
                    </tr> 
                     <tr><td colspan="2" class="LLine" height="1" ></td></tr>       
                     <tr>                        
                        <td colspan="2" align="center" height="30">&nbsp;
                        <fmtLoanerSwapSetup:message key="BTN_SUBMIT" var="varSubmit"/>
                        	<gmjsp:button value="${varSubmit }"  style="width: 6em; height: 22" gmClass="button" onClick="fnSubmit();" buttonType="Save" />
                        	<fmtLoanerSwapSetup:message key="BTN_RESET" var="varReset"/>
                        	<gmjsp:button value="${varReset}"  style="width: 6em;height: 22" gmClass="button" onClick="fnReset();" buttonType="Save" />   
                        </td>
                    </tr> 
                 </table> 
                 </td>
                 </tr>
                  <tr>
				 	<td valign="top" colspan="2">             	
						<div id="employeedata" style="grid" height="100px"></div>
		   			 </td>
				</tr>	                  
        		</table>
  			 
   		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>

