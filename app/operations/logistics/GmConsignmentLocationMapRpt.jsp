 <%
/*********************************************************************************************************
 * File		 		: GmConsignmentLocationMapRpt.jsp
 * Desc		 		: This screen is used to display Print Location for WIP sets
 * Version	 		: 1.0
 * author			: Ramachandiran Selvaraj
**********************************************************************************************************/
%>

<!-- \operations\inventory\GmConsignmentLocationMapRpt.jsp -->
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtConsignmentLocationRpt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<fmtConsignmentLocationRpt:setLocale value="<%=strLocale%>"/>
<fmtConsignmentLocationRpt:setBundle
	basename="properties.labels.operations.logistics.GmConsignmentLocationMapRpt" />
	
	<bean:define id="saveFl" name="frmConsignmentLocationReport" property="saveFl" type="java.lang.String"></bean:define>
<%
	String strOperationsJsPath = GmFilePathConfigurationBean
			.getFilePathConfig("JS_OPERATIONS");
	String strWikiTitle = GmCommonClass
			.parseNull((String) GmCommonClass
					.getWikiTitle("LOCATION_WIP_SET_MAPPING"));
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: Print Location for WIP Set Mapping</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/skins/dhtmlxcalendar_yahoolike.css"> 
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_dhxcalendar.js"></script> 
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmConsignmentLocationMapRpt.js"></script>
<script>
var strSaveFl = '<%=saveFl%>';
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
	<html:form action="/gmConsignmentLocationRpt.do?method=loadConsignmentLocMapDetails">
		<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" colspan="8" class="RightDashBoardHeader"><fmtConsignmentLocationRpt:message
						key="LBL_LOCATION_WIP_SET_MAPPING" /></td>
				<td colspan="2" class="RightDashBoardHeader" align="right"
					width="20"><fmtConsignmentLocationRpt:message key="IMG_HELP"
						var="varHelp" /> <img id='imgEdit' style='cursor: hand'
					src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16'
					height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr class="evenshade">
				<td class="RightTableCaption" align="right" HEIGHT="30">&nbsp;<fmtConsignmentLocationRpt:message
						key="LBL_WAREHOUSE" var="varWarehouse" /> <gmjsp:label
						type="BoldText" SFLblControlName="${varWarehouse}:" td="false" /></td>
				<td>&nbsp;<gmjsp:dropdown controlName="warehouseID"
						SFFormName="frmConsignmentLocationReport" SFSeletedValue="warehouseID"
						SFValue="alWareHouse" codeId="ID" codeName="WHNM"
						defaultValue="[Choose One]" tabIndex="2" width="150" />
				</td>
				
				<td class="RightTableCaption" align="right" HEIGHT="30">
				<fmtConsignmentLocationRpt:message key="LBL_LOCATION" var="varLocation"/>
				<gmjsp:label type="BoldText"  SFLblControlName="${varLocation}:" td="false"/></td>
				<td>&nbsp;<html:text property="locationID" size="15"
					onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" 
					onblur="changeBgColor(this,'#ffffff');" tabindex="3"/>
				</td>
				
				<td class="RightTableCaption" align="right" HEIGHT="30">
				<fmtConsignmentLocationRpt:message key="LBL_CN_ID" var="varCNID"/>
				<gmjsp:label type="BoldText"  SFLblControlName="${varCNID}:" td="false"/></td>
				<td>&nbsp;<html:text property="cnID" size="15"
					onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" 
					onblur="changeBgColor(this,'#ffffff');" tabindex="4" />
				</td>
				
				<td class="RightTableCaption" align="right" HEIGHT="30">&nbsp;<fmtConsignmentLocationRpt:message
						key="LBL_ZONE" var="varZone" /> <gmjsp:label
						type="BoldText" SFLblControlName="${varZone}:" td="false" /></td>
				<td>&nbsp;<gmjsp:dropdown controlName="zoneID"
						SFFormName="frmConsignmentLocationReport" SFSeletedValue="zoneID"
						SFValue="alZones" codeId="CODEID" codeName="CODENMALT"
						defaultValue="[Choose One]" tabIndex="5"  width="150"/>
				</td>
				
				<td class="RightTableCaption" align="right" HEIGHT="30">
				<fmtConsignmentLocationRpt:message key="LBL_SHELF" var="varShelf" />
				<gmjsp:label type="BoldText" SFLblControlName="${varShelf}:" td="false" /></td>
				<td>&nbsp;<html:text
					property="shelf" size="15"
					onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
					onblur="changeBgColor(this,'#ffffff');" tabindex="6"/>	
				</td>
				
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="10"></td>
			</tr>
			<tr class="oddshade" height="30">
				<td class="RightTableCaption" align="right" HEIGHT="30">&nbsp;<fmtConsignmentLocationRpt:message
						key="LBL_TYPE" var="varType" /> <gmjsp:label
						type="BoldText" SFLblControlName="${varType}:" td="false" /></td>
				<td>&nbsp;<gmjsp:dropdown controlName="invTypeID"
						SFFormName="frmConsignmentLocationReport" SFSeletedValue="invTypeID"
						SFValue="alInvLocTypes" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" tabIndex="7" width="150"/>
				</td>
				<td class="RightTableCaption" align="right" HEIGHT="30">&nbsp;<fmtConsignmentLocationRpt:message
						key="LBL_STATUS" var="varStatus" /> <gmjsp:label
						type="BoldText" SFLblControlName="${varStatus}:" td="false" /></td>
				<td>&nbsp;<gmjsp:dropdown controlName="statusID"
						SFFormName="frmConsignmentLocationReport" SFSeletedValue="statusID"
						SFValue="alStatus" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" tabIndex="8" width="150" />
				</td>
				<td class="RightTableCaption" align="right" HEIGHT="30">
				<fmtConsignmentLocationRpt:message key="LBL_AISLE" var="varAisle" />
				<gmjsp:label type="BoldText" SFLblControlName="${varAisle}:" td="false" /></td>
				<td>&nbsp;<html:text
					property="aisleNum" size="15"
					onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
					onblur="changeBgColor(this,'#ffffff');" tabindex="9"/>	
				</td>
				<td class="RightTableCaption" align="right" HEIGHT="30">&nbsp;<fmtConsignmentLocationRpt:message
						key="LBL_BUILDING" var="varBuilding" /> <gmjsp:label
						type="BoldText" SFLblControlName="${varBuilding}:" td="false" /></td>
				<td>&nbsp;<gmjsp:dropdown controlName="buildingID"
						SFFormName="frmConsignmentLocationReport" SFSeletedValue="buildingID"
						SFValue="alBuilding" codeId="BID" codeName="BNM"
						defaultValue="[Choose One]" tabIndex="10" width="180" />
				</td>
				<td align="center" colspan="2" height="25"><fmtConsignmentLocationRpt:message key="LBL_LOAD" var="varLoad"/>
				<gmjsp:button value="&nbsp;${varLoad}&nbsp;" name="Btn_Submit" gmClass="button"
					onClick="fnLoad();" tabindex="11" buttonType="Load" /></td>
			</tr>
			<tr>
				<td class="LLine" colspan="10" height="1"></td>
			</tr>

			<tr>
				<td colspan="10"><div id="dataGridDiv" class="" height="400px"></div></td>
			</tr>
			<tr style="display: none" id="DivExportExcel" height="25">
				<td colspan="10" align="center">
					<div class='exportlinks'>
						<fmtConsignmentLocationRpt:message key="LBL_EXPORT_OPTIONS" />
						: <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
							onclick="fnExport();"><fmtConsignmentLocationRpt:message
								key="LBL_EXCEL" /> </a>
					</div>
				</td>
			</tr>
			<tr>
				<td class="LLine" colspan="10" height="1"></td>
			</tr>
			<tr style="display: none" id="DivButton" height="30">
				<td colspan="10" align="center" class="RegularText">
				<fmtConsignmentLocationRpt:message key="LBL_PRINT" var="varPrint"/>
						<gmjsp:button value="&nbsp;${varPrint}&nbsp;" gmClass="button" 
						onClick="fnPrint();" tabindex="12" buttonType="Load"/></td>
			</tr>
			<tr style="display: none" id="DivDataRow">
				<td colspan="10" align="center" class="RegularText" id="DivData"
					HEIGHT="30"><div id="DivNothingMessage">
						<fmtConsignmentLocationRpt:message key="NO_DATA_FOUND" />
					</div></td>
			</tr>
		</table>
	</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>