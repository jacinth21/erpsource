<!-- operations\logistics\GmLotDetailReport.jsp -->
<%
	/**********************************************************************************
	  * File		 		: GmLotDetailReport.jsp
      * Desc		 		: This screen is used to display Lot Detail Report
      * author			    : Agilan Singaravel
	 ************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@page import="java.util.Date"%> 
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%> 
<%@ page import = "java.util.*,java.io.*"%>
<%@ taglib prefix="fmtLotDetailRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtLotDetailRpt:setLocale value="<%=strLocale%>"/>
<fmtLotDetailRpt:setBundle basename="properties.labels.operations.logistics.GmLotDetailReport"/>

<%
String strLogisticsJsPath= GmCommonClass.parseNull(GmFilePathConfigurationBean.getFilePathConfig("JS_LOGISTICS"));
String strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("LOT_TRACK_DETAIL_RPT"));
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Lot Track Report (Non Tissue)</TITLE>
<link rel="styleSheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css"> 
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strLogisticsJsPath%>/GmLotDetailReport.js"></script> 
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/jquery-latest.js"></script>
<script type="text/javascript">
var gCmpDateFmt = "<%=strGCompDateFmt%>";
</script>

<style type="text/css" media="all">
@import url("styles/screen.css"); 
</style>
<style> div.gridbox_dhx_skyblue table.hdr td { text-align: center; vertical-align: middle !important; }
.lineHide {
display:none;
}
 </style>
</HEAD>
<body leftmargin="15" topmargin="10" onLoad="fnOnload(); ">
 <html:form action="/gmLotDetailRpt.do?method=loadLotDetailsReport" > 
 <html:hidden name="frmLotDetailReport"  property="strOpt" value="" />  
 <table class="DtTable800" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="5" height="25" class="RightDashBoardHeader">&nbsp;<fmtLotDetailRpt:message key="LOT_TRACK_DETAIL_RPT"/> </td>
				<td height="25" align="right" class=RightDashBoardHeader><fmtLotDetailRpt:message key="IMG_ALT_HELP" var="varHelp"/>				    
				 <img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
             </tr>
             
            <tr class="evenshade"> 
				<td class="RightTableCaption" align="right" HEIGHT="30"><fmtLotDetailRpt:message key="LBL_LOT_NUM"/>:&nbsp;</td>
				<td class="RightText">
				   <html:text property="strLotNumber" size="23" maxlength="40" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" />
				    &nbsp;&nbsp;
				</td>  
				<td class="RightTableCaption" align="right" HEIGHT="30"><fmtLotDetailRpt:message key="LBL_PART_NUM"/>:&nbsp;</td>
				<td class="RightText">
				   <html:text property="strPartNumber" size="15" maxlength="20" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>&nbsp;&nbsp; 
				
				<gmjsp:dropdown controlName="strPartLiteral" SFFormName="frmLotDetailReport" SFSeletedValue="strPartLiteral"
							SFValue="alPartSearch" codeId = "CODENMALT"  codeName = "CODENM" defaultValue= "[Choose One]" tabIndex="5"/>
				</td>
				<td align="center" height="25" colspan="1">
				<fmtLotDetailRpt:message key="BTN_LOAD" var="varLoad"/>
			    <gmjsp:button value="&nbsp;${varLoad}&nbsp;"gmClass="button" buttonType="Load" onClick="fnLoad();" /> 
			    </td>
			   <!--  <td align="right" height="25" colspan="1">&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
		  </tr>
		
	 	 <tr><td colspan="6" height="1" bgcolor="#000000"></td></tr>
 		 
					<tr>
						<td colspan="6">
							<jsp:include page="/operations/logistics/GmLotDetailProdInfo.jsp">
								<jsp:param name="FRMNAME" value="frmLotDetailReport" />
							</jsp:include>
						</td>
					</tr>
					
		<tr class="lineHide"><td colspan="6" height="1" bgcolor="#000000"></td></tr>
 		 
					<tr>
						<td colspan="6">
							<jsp:include page="/operations/logistics/GmLotDetailRecInfo.jsp">
								<jsp:param name="FRMNAME" value="frmLotDetailReport" />
							</jsp:include>
						</td>
					</tr>
		<tr class="lineHide"><td colspan="6" height="1" bgcolor="#000000"></td></tr>	 
					<tr>
						<td colspan="6">
							<jsp:include page="/operations/logistics/GmLotDetailInventoryInfo.jsp">
								<jsp:param name="FRMNAME" value="frmLotDetailReport" />
							</jsp:include>
						</td>
					</tr>
		<tr class="lineHide"><td colspan="6" height="1" bgcolor="#000000"></td></tr>	 
					<tr>
						<td colspan="6">
							<jsp:include page="/operations/logistics/GmLotDetailFieldSalesInfo.jsp">
								<jsp:param name="FRMNAME" value="frmLotDetailReport" />
							</jsp:include>
						</td>
					</tr>
		<tr class="lineHide"><td colspan="6" height="1" bgcolor="#000000"></td></tr>	 
					<tr>
						<td colspan="6">
							<jsp:include page="/operations/logistics/GmLotDetailConsignmentInfo.jsp">
								<jsp:param name="FRMNAME" value="frmLotDetailReport" />
							</jsp:include>
						</td>
					</tr>
		<tr class="lineHide"><td colspan="6" height="1" bgcolor="#000000"></td></tr>	 
					<tr>
						<td colspan="6">
							<jsp:include page="/operations/logistics/GmLotDetailLoanerInfo.jsp">
								<jsp:param name="FRMNAME" value="frmLotDetailReport" />
							</jsp:include>
						</td>
					</tr>
       </table>
 </html:form> 
  <%@ include file="/common/GmFooter.inc" %> 
</BODY>
</HTML>