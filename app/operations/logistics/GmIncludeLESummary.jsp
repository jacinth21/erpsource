<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="org.apache.commons.beanutils.RowSetDynaClass"%>

<%@ include file="/common/GmHeader.inc" %> 

<%@ taglib prefix="fmtIncludeLESummary" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmIncludeLESummary.jsp -->

<fmtIncludeLESummary:setLocale value="<%=strLocale%>"/>
<fmtIncludeLESummary:setBundle basename="properties.labels.operations.logistics.GmIncludeLESummary"/>


<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	String strRptFmt = "{0,date,"+strApplDateFmt+"}";

%>
<table border="0" width="700" cellspacing="0" cellpadding="0">
<tr><td colspan="3" height="1" bgcolor="#000000"></td></tr>
	<tr class="ShadeRightTableCaption">
			<td height="23" width="30%"><fmtIncludeLESummary:message key="LBL_LOANER_EX_SUMM_DET"/></td>
			<td height="23" width="20%" ></td>
			<td height="23" width="50%"></td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#000000"></td></tr>
<tr>
			<td  colspan="3" width="100%">
			<display:table name="requestScope.LEDETAILS.rows" export="false" class="its" id="currentRowObject">	
			 <fmtIncludeLESummary:message key="DT_EXPECTED_DATE" var="varExpectedDate"/>
					<display:column property="EXPECTDT" title="${varExpectedDate}" class="alignleft" sortable="true"  format="<%=strRptFmt%>"/>
					 <fmtIncludeLESummary:message key="DT_TYPE" var="varType"/>
					<display:column property="REASONTYPE" title="${varType}" class="alignleft"  sortable="true" />
					<fmtIncludeLESummary:message key="DT_COMMENTS" var="varComments"/>
					<display:column property="COMMENTS" title="${varComments}" class="alignleft" />
					<fmtIncludeLESummary:message key="DT_CREATED_BY" var="varCreatedBy"/>
					<display:column property="CREATBY" title="${varCreatedBy}" class="alignleft" />
					<fmtIncludeLESummary:message key="DT_CREATED_DATE" var="varCreatedDate"/>
					<display:column property="CREATDT" title="${varCreatedDate}" class="alignleft" format="<%=strRptFmt%>" />
		</display:table>
		</td>
		</tr>
	</table>