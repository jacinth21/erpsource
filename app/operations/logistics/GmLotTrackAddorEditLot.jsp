<!-- \logistics\GmLotTrackAddorEditLot.jsp -->
<%
	/**********************************************************************************
	 * File		 		: GmLotTrackAddorEditLot.jsp
	 * Desc		 		: Add/Edit Lot Track Details view Page
	 * author			: Vinoth Petchimuthu
	 ************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtAddorEditLotTrackDetail"
	uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ page isELIgnored="false"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page
	import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder"%>

<%@ page import="com.globus.common.beans.GmCommonClass"%>
<fmtAddorEditLotTrackDetail value="<%=strLocale%>" />
<fmtAddorEditLotTrackDetail:setBundle
	basename="properties.labels.operations.logistics.GmLotTrackNonTissueRpt" />
<bean:define id="alPlantList" name="frmLotTrackNonTissueRpt"
	property="alPlantList" type="java.util.ArrayList">
</bean:define>
<bean:define id="plantNm" name="frmLotTrackNonTissueRpt"
	property="plantNm" type="java.lang.String">
</bean:define>
<bean:define id="companyNm" name="frmLotTrackNonTissueRpt"
	property="companyNm" type="java.lang.String">
</bean:define>
<bean:define id="strDisable" name="frmLotTrackNonTissueRpt"
	property="strDisable" type="java.lang.String">
</bean:define>
<bean:define id="strAccess" name="frmLotTrackNonTissueRpt"
	property="strAccess" type="java.lang.String">
</bean:define>
<%
	String strWikiTitle = GmCommonClass
			.getWikiTitle("ADD_OR_EDIT_LOT_TRACK_DETAIL");
	String strLogisticsJsPath = GmFilePathConfigurationBean
			.getFilePathConfig("JS_LOGISTICS");
	String strPartNumber = "";
	String strPartDescription = "";
	String strLotNumber = "";
	String strQty = "";
	String strWarehouseType = "";
	String strLocation = "";
	String strLocationId = "";
	String strCompany = "";
	String strPlant = "";
	String strCntrlNumInvId = "";

	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap) request.getAttribute("HMRETURN");
	HashMap hcboVal = null;
	if (hmReturn != null) {
		strPartNumber = GmCommonClass.parseNull((String) hmReturn
				.get("PNUMID"));
		strPartDescription = GmCommonClass.parseNull((String) hmReturn
				.get("PARTDESC"));
		strLotNumber = GmCommonClass.parseNull((String) hmReturn
				.get("CNUM"));
		strQty = GmCommonClass.parseNull((String) hmReturn.get("QTY"));
		strWarehouseType = GmCommonClass.parseNull((String) hmReturn
				.get("WHTYPE"));
		strLocation = GmCommonClass.parseNull((String) hmReturn
				.get("LOCNM"));
		strLocationId = GmCommonClass.parseNull((String) hmReturn
				.get("LOCID"));
		strCompany = GmCommonClass.parseNull((String) hmReturn
				.get("CMPID"));
		strPlant = GmCommonClass.parseNull((String) hmReturn
				.get("PLNTID"));
		strCntrlNumInvId = GmCommonClass.parseNull((String) hmReturn
				.get("CNTRLINVID"));

	}
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Add/Edit Lot Track Details</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<link rel="stylesheet" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript"
	src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript"
	src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="javascript" src="<%=strJsPath%>/GmGrid.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="javascript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script type="text/javascript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_fast.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript"
	src="<%=strLogisticsJsPath%>/GmLotTrackAddorEditLot.js"></script>
<script>
var PlantLength = <%=alPlantList.size()%>;
var defaultCompany = '<%=companyNm%>';
var defaultPlant = '<%=plantNm%>';
var strCntrlNumInvId = '<%=strCntrlNumInvId%>';
var strAccess = '<%=strAccess%>';
<% hcboVal = new HashMap();
for (int i=0;i<alPlantList.size();i++){
	hcboVal = (HashMap)alPlantList.get(i); %>
var AllPlantArr<%=i%> ="<%=hcboVal.get("PLANTID")%>,<%=hcboVal.get("PLANTNM")%>";
<% } %>
	
</script>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnload();">
	<html:form
		action="/gmLotTrackNonTissueRpt.do?method=gmAddorEditLotTrackDetail">
		<html:hidden name="frmLotTrackNonTissueRpt" property="strOpt" value="" />
		<input type="hidden" name="strCntrlNumInvId" value="<%=strCntrlNumInvId%>">
		<table border="0" class="DtTable950" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" colspan="2" class="RightDashBoardHeader">&nbsp;<fmtAddorEditLotTrackDetail:message
						key="LBL_ADD_OR_EDIT_LOT_DETAIL" /></td>

				<td height="25" class="RightDashBoardHeader" align="right"><fmtAddorEditLotTrackDetail:message
						key="IMG_ALT_HELP" var="varHelp" /> <img id='imgEdit'
					align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td class="Line" height="1" colspan="5"></td>
			</tr>
			<tr>
				<td colspan="2" id="successCont" style="left: 255px; position: relative;"></td>
			</tr>
			<tr class="evenshade" colspan="2">
				<td class="RightRedCaption" colspan="2" align="right"
					HEIGHT="30"><fmtAddorEditLotTrackDetail:message
						key="LBL_PART_NUM" /></td>
				<td>:&nbsp;<input type="text" size="23" maxlength="20"
					style="height: 12px; width: 200px;" class="InputArea"
					onFocus="changeBgColor(this,'#AACCE8');" name="Txt_PNum" 
					onBlur="changeBgColor(this,'#ffffff'); fnGetPartDesc(this);" id="partNum"
					<%=strDisable%> value="<%=strPartNumber%>" tabIndex="1">
				</td>
			</tr>
			<tr class="oddshade" colspan="2">

				<td class="RightRedCaption" colspan="2" align="right"
					style="height: 30px;"><fmtAddorEditLotTrackDetail:message
						key="LBL_PART_DESC" var="varPartDesc" /> <gmjsp:label
						type="RegularText" SFLblControlName="${varPartDesc}" td="false" /></td>
				<td class="RightText" id="partDesc">:&nbsp;<%=strPartDescription%></td>
			</tr>
			<tr class="evenshade" colspan="2">
				<td class="RightRedCaption" colspan="2" align="right"
					style="height: 30px;"><fmtAddorEditLotTrackDetail:message
						key="LBL_LOT_NUM" /></td>
				<td>:&nbsp;<input type="text" size="23" maxlength="40"
					style="height: 12px; width: 200px;" class="InputArea"
					onFocus="changeBgColor(this,'#AACCE8');"
					onBlur="changeBgColor(this,'#ffffff');" id="lotNum"
					value="<%=strLotNumber%>" tabIndex="2">
				</td>
			</tr>
			<tr class="oddshade" colspan="2">
				<td class="RightRedCaption" colspan="2" align="right"
					style="height: 30px;"><fmtAddorEditLotTrackDetail:message
						key="LBL_QTY" /></td>
				<td>:&nbsp;<input type="text" size="40" maxlength="4"
					style="height: 12px; width: 60px;" class="InputArea"
					onFocus="changeBgColor(this,'#AACCE8');"
					onBlur="changeBgColor(this,'#ffffff');" id="qty"
					value="<%=strQty%>" tabIndex="3">
				</td>
			</tr>
			<tr class="evenshade" colspan="2">
				<td class="RightRedCaption" colspan="2" align="right"
					style="height: 30px;"><fmtAddorEditLotTrackDetail:message
						key="LBL_COMPANY" var="varCompany" /> <gmjsp:label
						type="RegularText" SFLblControlName="${varCompany}" td="false" />&nbsp;</td>
				<td>:&nbsp;<gmjsp:dropdown controlName="companyNm"
						SFFormName="frmLotTrackNonTissueRpt" SFSeletedValue="companyNm"
						seletedValue="<%=strCompany%>" SFValue="alCompanyList"
						codeId="COMPANYID" codeName="COMPANYNM" tabIndex="4"
						onChange="fnGetPlantList(this);" defaultValue="[Choose One]" />
				</td>
			</tr>
			<tr class="oddshade" colspan="2">
				<td class="RightRedCaption" colspan="2" align="right"
					style="height: 30px;"><fmtAddorEditLotTrackDetail:message
						key="LBL_PLANT" var="varPlant" /> <gmjsp:label type="RegularText"
						SFLblControlName="${varPlant}" td="false" />&nbsp;</td>
				<td>:&nbsp;<gmjsp:dropdown controlName="plantNm"
						SFFormName="frmLotTrackNonTissueRpt" SFSeletedValue="plantNm"
						seletedValue="<%=strPlant%>" SFValue="alPlantList" tabIndex="5"
						codeId="PLANTID" codeName="PLANTNM" defaultValue="[Choose One]" />
				</td>
			</tr>
			<tr class="evenshade" colspan="2">
				<td class="RightRedCaption" colspan="2" align="right"
					style="height: 30px;"><fmtAddorEditLotTrackDetail:message
						key="LBL_WAREHOUSE_TYPE" var="varWarehouseType" /> <gmjsp:label
						type="RegularText" SFLblControlName="${varWarehouseType}"
						td="false" />&nbsp;</td>
				<td>:&nbsp;<gmjsp:dropdown controlName="warehouseType"
						SFFormName="frmLotTrackNonTissueRpt"
						SFSeletedValue="warehouseType"
						seletedValue="<%=strWarehouseType%>" SFValue="alWarehouseType"
						codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"
						tabIndex="6" onChange="javascript:fnChangeConsignee(this);"/>
				</td>
			</tr>
			<tr class="oddshade" colspan="2">
				<td class="RightRedCaption" colspan="2" align="right"
					style="height: 30px;"><fmtAddorEditLotTrackDetail:message
						key="LBL_LOCATION" var="varConsigNam" />
					<gmjsp:label type="RegularText" SFLblControlName="${varConsigNam}"
						td="false" />&nbsp;</td>
				<td id="chooseOne_blk" style="display: block; margin-top: 5px;"
					colspan="7">:&nbsp;<input type="text" size="60"
					style="height: 12px;" class="InputArea" 
					onFocus="changeBgColor(this,'#AACCE8');"
					onBlur="changeBgColor(this,'#ffffff');" id="location"
					value="<%=strLocation%>" seletedValue="<%=strLocation%>" tabIndex="7">
				</td>
				<td id="consignee_blk" style="display: none; margin-top: 5px;"
					colspan="7">:&nbsp;<gmjsp:dropdown controlName="fieldSalesNm"
						SFFormName="frmLotTrackNonTissueRpt" SFSeletedValue="fieldSalesNm"
						seletedValue="<%=strLocationId%>" SFValue="alFieldSales" codeId="ID"
						codeName="NAME" defaultValue="[Choose One]" tabIndex="7"/>
				</td>
				<td id="account_blk" style="display: none; margin-top: 5px;"
					colspan="7">:&nbsp;<gmjsp:dropdown controlName="accountNm"
						SFFormName="frmLotTrackNonTissueRpt" SFSeletedValue="accountNm"
						seletedValue="<%=strLocationId%>" SFValue="alAccounts" codeId="ID"
						codeName="NAME" defaultValue="[Choose One]" tabIndex="7" />
				</td>

			</tr>
			<tr>
				<td class="Line" height="1" colspan="5"></td>
			</tr>
			<tr class="evenshade">
				<td class="RightRedCaption" colspan="5"
					style="height: 50px; padding-left: 80px;" align="center"><fmtAddorEditLotTrackDetail:message
						key="BTN_SUBMIT" var="varSubmit" /> <gmjsp:button
						name="button_submit" style="width: 5em" value="${varSubmit}"
						gmClass="button" onClick="fnSave();" tabindex="8" buttonType="Save" />&nbsp; <fmtAddorEditLotTrackDetail:message
						key="BTN_RESET" var="varReset" /> <gmjsp:button
						name="button_reset" value="${varReset}" style="width: 6em"
						gmClass="button" onClick="fnReset();" tabindex="9"
						buttonType="Save" /></td>
			</tr>
		</table>
	</html:form>

	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
