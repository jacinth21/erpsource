 <%
/**********************************************************************************
 * File		 		: GmLoanerAckPrint.jsp
 * Desc		 		: This screen is used for the
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %> 
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmJasperReport"%>
<%@page import="java.util.Date"%>

<%@ taglib prefix="fmtLoanerAckPrint" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmLoanerAckPrint.jsp  -->

<fmtLoanerAckPrint:setLocale value="<%=strLocale%>"/>
<fmtLoanerAckPrint:setBundle basename="properties.labels.operations.logistics.GmLoanerAckPrint"/>


<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session); 
	String strApplnDateFmt = strGCompDateFmt;
		
	Date dtLDate = null; 
	Date dtEDate = null; 

	HashMap hmCsg = new HashMap();
	HashMap hmJasperParams = new HashMap(); 
	hmJasperParams = (HashMap)request.getAttribute("HMJASPERPARAMS");
	// To get the Consignment information 
	hmCsg = (HashMap)request.getAttribute("hmCONSIGNMENT");
	if (hmCsg == null){
		hmCsg = (HashMap)session.getAttribute("hmCONSIGNMENT");
	}
	 
	dtLDate = (Date)hmCsg.get("LDATE");
	dtEDate = (Date)hmCsg.get("EDATE");
	String strHtmlJasperRpt = ""; //Added for JBOSS migration
	String strType = GmCommonClass.parseNull((String)request.getAttribute("hType"));
	String strBillNm = GmCommonClass.parseNull((String)hmCsg.get("BILLNM"));
	String strCompanyId = GmCommonClass.parseNull((String)hmCsg.get("COMPANYID"));
	String strHeader  =	strType.equals("40050")?"HOSPITAL CONSIGNMENT AGREEMENT":"LOANER ACKNOWLEDGEMENT LETTER";		
	String strJasName = GmCommonClass.parseNull((String)hmJasperParams.get	("JASNAME"));
	String strDivisionID = strCompanyId.equals("100801")?"2001":"2000";
	HashMap hmCompanyAddress = GmCommonClass.fetchCompanyAddress(gmDataStoreVO.getCmpid(), strDivisionID);
	String strCompanyLogo = GmCommonClass.parseNull((String)hmCompanyAddress.get("LOGO"));
	String strCompanyName = GmCommonClass.parseNull((String)hmCompanyAddress.get("COMPNAME"));	
	String strCompanyAddress = GmCommonClass.parseNull((String)hmCompanyAddress.get("COMPADDRESS"));
	String strCompanyPhone = GmCommonClass.parseNull((String)hmCompanyAddress.get("PH"));
	String strCompanyFax = GmCommonClass.parseNull((String)hmCompanyAddress.get("FAX"));
	String strCompanyShortName = GmCommonClass.parseNull((String)hmCompanyAddress.get("COMP_SHORT_NAME"));
	String strCustServicePhNumber = GmCommonClass.parseNull((String)hmCompanyAddress.get("CUST_SERVICE"));
	strCompanyAddress = strCompanyAddress.replaceAll("/","<br>");
	hmJasperParams.put("LNRDATE",GmCommonClass.getStringFromDate(dtLDate,strApplnDateFmt));
	hmJasperParams.put("ENDDATE",GmCommonClass.getStringFromDate(dtEDate,strApplnDateFmt));
	hmJasperParams.put("LOGO",strCompanyLogo);
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Loaner Letter</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>

<script>
function fnPrint(){
	window.print();
}
var tdinnner = "";
function hidePrint(){
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}
function showPrint(){
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
</script>
</HEAD>
<BODY topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
	<%
		if (strType.equals("4127")){
	%>
	<div id="jasper" > 
	<%
		String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
		GmJasperReport gmJasperReport = new GmJasperReport();
		gmJasperReport.setRequest(request);
		gmJasperReport.setResponse(response);
		gmJasperReport.setJasperReportName(strJasName);
		gmJasperReport.setHmReportParameters(hmJasperParams);
		gmJasperReport.setBlDisplayImage(true);
		strHtmlJasperRpt = gmJasperReport.getHtmlReport();
%>	
<%=strHtmlJasperRpt %>	
</div>
<%
	}else if (strType.equals("40050")){
%>
	<table border="0" width="700" cellspacing="0" cellpadding="0" align=center>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="6"></td>
			<td bgcolor="#666666" colspan="3"></td>
			<td bgcolor="#666666" width="1" rowspan="6"></td>
		</tr>
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td width="170"><img src="<%=strImagePath%>/<%=strCompanyId%>.gif" width="138" height="60"></td>
						<td class="RightText" align="right" width="100%"> <%=strCompanyName %> <br> <%=strCompanyAddress %> <br><br><font size=-2> <%=strCompanyPhone %> <br> <%=strCompanyFax %></font>
						</td>
						<td width="20">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td bgcolor="#666666"></td></tr>
		<tr>
			<td width="698" height="70" valign="top" align="center" class="RightText"><BR><font size="+1"><%=strHeader %></font><BR><BR></td>
		</tr>
		<tr>
			<td width="690" height="80" valign="top" class="RightText">
			<%=GmCommonClass.getStringFromDate(dtLDate,strApplnDateFmt)%><br><br>
<BR>
<%=strCompanyName %> ("<%=strCompanyShortName %>") is consigning to <%=strBillNm %> the items listed on the Consignment Schedule attached hereto.  
<BR>This Agreement shall govern the Parties relationship for all <%=strCompanyShortName %> items consigned to <%=strBillNm %>.
<BR><BR>
<%=strBillNm %> hereby acknowledges and agrees as follows:
<li>Consignment inventory shall remain the property of <%=strCompanyShortName %> until withdrawn for use or otherwise purchased by <%=strBillNm %>.  
<li><%=strBillNm %> is responsible to maintain all consigned inventory in good working order and condition, and is responsible for the full value of the consigned inventory for any items which are used, lost, stolen, or damaged while in its possession.
<li>This Agreement shall govern the parties' relationship for all <%=strCompanyShortName %> products consigned to Hospital, whether or not identified on the Consignment Schedule.
<li><%=strBillNm %> shall issue a purchase order prior to taking delivery of product(s). 
<li><%=strCompanyShortName %> and <%=strBillNm %> agree to conduct scheduled reviews of all consigned inventory, either monthly or as mutually agreed by the Parties.  
<li><%=strBillNm %> shall use reasonable efforts to identify all consigned inventory as being the property of <%=strCompanyShortName %> and to separate such inventory from <%=strBillNm %>'s other property. 
<br><br>
ACKNOWLEDGED AND AGREED ON BEHALF OF <b><u><%=strBillNm %></u></b>: 
<br><br/><br><br/>
By:  ______________________________________
<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Signature)
<br><br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;______________________________________
<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Print Name)
<br><br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;______________________________________
<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Title
<br><br/>
<%
	}
%>
			</td>
		</tr>
		<tr><td bgcolor="#666666" height="1"></td></tr>
    </table>
	<table border="0" width="700" cellspacing="0" cellpadding="0" align=center>
		<tr>
			<td align="center" height="30" id="button">
			<fmtLoanerAckPrint:message key="BTN_PRINT" var="varPrint"/>
				<gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Prt" gmClass="button" onClick="fnPrint();" buttonType="Load" />&nbsp;&nbsp;
				<fmtLoanerAckPrint:message key="BTN_CLOSE" var="varClose"/>
				<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Cls" gmClass="button" onClick="window.close();" buttonType="Load" />
			</td>
		<tr>
	</table>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
