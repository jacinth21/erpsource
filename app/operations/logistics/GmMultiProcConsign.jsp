 <!-- operations\logistics\GmMultiProcConsign.jsp -->
 <%
	/**********************************************************************************
	 * File		 		: GmMultiProcConsign.jsp
	 * Desc		 		: This screen is used for the Select Multi Sets
	 * author			: Raja 
	 ************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@page import="java.util.Date"%> 
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%> 
<%@page import="com.globus.common.beans.GmCalenderOperations"%>
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.net.URLEncoder" %>
<%@ taglib prefix="fmtGmMultiProcConsign" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtGmMultiProcConsign:setLocale value="<%=strLocale%>"/>
<fmtGmMultiProcConsign:setBundle basename="properties.labels.operations.logistics.GmMultiProcConsign"/>
<%
String strLogisticsJsPath= GmCommonClass.parseNull(GmFilePathConfigurationBean.getFilePathConfig("JS_LOGISTICS"));
String strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("MULTI_SELECT_SETS"));
String strApplDateFmt = strGCompDateFmt;
GmCalenderOperations.setTimeZone(strGCompTimeZone);
String strTodays = GmCalenderOperations.getCurrentDate(strGCompDateFmt);
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Requests - Multi Select Sets </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="styleSheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css"> 
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strLogisticsJsPath%>/GmMultiProcConsign.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/jquery-latest.js"></script>
<script type="text/javascript">
var date_format = '<%=strApplDateFmt%>';
var today_Dt = '<%=strTodays%>';
</script>
<style type="text/css" media="all">
@import url("styles/screen.css"); 
</style>
<style type="text/css">
         table.DtTable1200 {
             margin: 0 0 0 0;
             width: 1200px;
             border: 1px solid #676767;
         }
</style>
</HEAD>
<body leftmargin="15" topmargin="10" onLoad="fnOnPageLoad(); ">
 <html:form action="/gmMultiProcConsign.do" > 
 

<table class="DtTable1200" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="9" height="25" class="RightDashBoardHeader">&nbsp;<fmtGmMultiProcConsign:message key="CONSIGN_MULTI_SET_HEADER"/> </td>
				<td height="25" align="right" class=RightDashBoardHeader><fmtGmMultiProcConsign:message key="IMG_ALT_HELP" var="varHelp"/>				    
				 <img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
             </tr>
             
             <tr><td>
             <table class="DtTable1995">
              <tr class="evenshade"> 
				<td class="RightTableCaption" align="right" HEIGHT="30"><fmtGmMultiProcConsign:message key="LBL_SET_NUM"/>&nbsp;:&nbsp;</td>
				<td class="RightText">
				   <html:text property="strSetName" size="23" maxlength="40" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" tabindex="1" />
				    &nbsp;&nbsp;
				</td>  
				<td class="RightTableCaption" align="right" HEIGHT="30"><fmtGmMultiProcConsign:message key="LBL_SET_ID"/>&nbsp;:&nbsp;</td>
				<td class="RightText">
				   <html:text property="strSetId" size="23" maxlength="20" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" tabindex="2"/>&nbsp;&nbsp; 
				</td>
				
				<td class="RightTableCaption" align="right" HEIGHT="30"><fmtGmMultiProcConsign:message key="LBL_Status"/>:&nbsp;</td>
				 <td class="RightText">
				 <select name="strStatus" id="strStatus" tabindex="3">
					<option value="50646" selected><fmtGmMultiProcConsign:message key="OPT_READY_TO_CONSIGN"/></option>  
					<option value="50644"><fmtGmMultiProcConsign:message key="OPT_BACK_ORDER"/></option>
				    <option value="50645"><fmtGmMultiProcConsign:message key="OPT_BACK_LOG"/></option>
				</select>
				</td>
			    <td align="right" height="25" colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		    </tr>
		    <tr class="evenshade"> 
		    <td class="RightTableCaption" align="right" HEIGHT="30"><fmtGmMultiProcConsign:message key="LBL_REQ_ID"/>&nbsp;:&nbsp;</td>
		    <td class="RightText">
			<html:text property="strReqId" size="23" maxlength="40" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" tabindex="3" />
				    &nbsp;&nbsp;
				</td>
		    <td class="RightTableCaption" align="right" HEIGHT="30"><fmtGmMultiProcConsign:message key="LBL_CONSIGN_ID"/>&nbsp;:&nbsp;</td>
		    <td class="RightText">
			 <html:text property="strConsignmentId" size="23" maxlength="40" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" tabindex="4" />
				    &nbsp;&nbsp;
		    </td> 
		  
		   <td height="25" align="right" colspan="1">
				<fmtGmMultiProcConsign:message key="BTN_LOAD" var="varLoad"/>
			    <gmjsp:button value="&nbsp;${varLoad}&nbsp;"gmClass="button" buttonType="Load" onClick="fnLoad();" /> 
		   </td>
		  </tr>
		 </table>
		  </td> 
		  </tr>
		 <tr>
		 <td><div id="msg"  align="left"  style="display:none; color:green; font-weight:bold;"></div> 
		   <div id="erromsg" align="left" style="display:none; color:red;font-weight:bold;"></div>
		 </td>
		 </tr>
		 
		 <tr><td class="LLine" colspan="10" height="1"></td></tr>
	     <tr class="oddshade"> 
				<td colspan="10"><div id="dataGridDiv" height="200px"></div></td>
	     </tr>
         <tr><td class="LLine" colspan="10" height="1"></td></tr>
         <tr class="ShadeRightTableCaption">
		   <td height="30" colspan="14">Selected Sets:</td>
	    </tr>
		  
		<tr><td class="LLine" colspan="10" height="1"></td></tr>
		<tr class="evenshade"> 
				<td colspan="10"><div id="dataGridDiv2" height="600px"></div></td>
		</tr> 
         <tr><td class="LLine" colspan="10" height="1"></td></tr>
        <tr>
		  <td colspan="10">
		    <jsp:include page="/operations/logistics/GmMultiRequestEdit.jsp" >
			<jsp:param name="screenType" value="" />	
			</jsp:include>
		 </td>   
	   </tr> 
	   <tr><td class="LLine" colspan="10" height="1"></td></tr> 
        <tr>
	    <td colspan="10">
		<jsp:include page="/operations/shipping/GmIncShipDetails.jsp" /> 
	    </td>  
		</tr> 
       <tr><td colspan="10" height="1"></td></tr>
       <tr>
	      <td class="RightTableCaption" align="center" colspan="10">
	          <table border="0" cellspacing="0" cellpadding="0" >
	           <tr><td colspan="10" height="30px;"></td></tr>
			     <tr><td align="center">
				 	<fmtGmMultiProcConsign:message key="BTN_SUBMIT" var="varSubmit"/>
				 	<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" style="width: 6em" gmClass="button" buttonType="Save" onClick="fnSubmit();"/>
				
				 	<fmtGmMultiProcConsign:message key="BTN_RESET" var="varReset"/>
					 <gmjsp:button value="&nbsp;${varReset}&nbsp;" style="width: 6em" name="Btn_Reset" gmClass="button" buttonType="Save" onClick="fnReset();" /> 
			      </td>
			  </tr>
			  </table>
		</td></tr>
	<tr><td colspan="10" height="1"></td></tr>
</table>
</html:form>
  <%@ include file="/common/GmFooter.inc" %> 
</BODY>
</HTML>