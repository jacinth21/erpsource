
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

 <%@page import="java.util.Date"%>
 
 <%@ taglib prefix="fmtIncludeLEEdit" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ include file="/common/GmHeader.inc" %>
<!-- operations\logistics\GmIncludeLEEdit.jsp  -->
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}

%>
<fmtIncludeLEEdit:setLocale value="<%=strLocale%>"/>
<fmtIncludeLEEdit:setBundle basename="properties.labels.operations.logistics.GmIncludeLEEdit"/>
 
<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strApplDateFmt = strGCompDateFmt;
	String strReasonType = GmCommonClass.parseNull((String) request.getAttribute("REASONTYPE"));
	ArrayList alReasonTypes = (ArrayList)request.getAttribute("REASONTS");
	String strTodaysDate = (String)session.getAttribute("strSessTodaysDate")==null?"":(String)session.getAttribute("strSessTodaysDate");
	String strType = strReasonType!=""?strReasonType : "50321";
	String strComments = GmCommonClass.parseNull((String) request.getAttribute("EXTCOMMENT"));
	Date dtSelectExtDate = (Date)request.getAttribute("EXTDATE");
	Date dtSelectSurgDate = (Date)request.getAttribute("SURGDATE");
	String strInputComment = strComments!=""?strComments:"";
	String strChkType = strType.equals("50321")? "Enabled" :"Disabled";
	String strSurgDtAccFl = GmCommonClass.parseNull((String) request.getAttribute("STRSURGERYACCESSFL"));
	String strConsignmentType = GmCommonClass.parseNull((String)request.getAttribute("STRCONSIGNMENTTYPE"));
%>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<table border="0" width="700" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>	
		
		<tr class="ShadeRightTableCaption">
			<td height="25" colspan="2" ><fmtIncludeLEEdit:message key="LBL_NEW_LOANER_EX_DET"/></td>
		</tr>
		<tr><td colspan="2" height="1" bgcolor="#000000"></td></tr>	
		<tr class="evenshade">
			<td class="RightTableCaption"  HEIGHT="25" align="right"><font color="red">*</font><fmtIncludeLEEdit:message key="LBL_TYPE"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="Cbo_ReasonType" onChange="javascript:fnCheckReasonType();" seletedValue="<%=strType%>"	
						 value="<%=alReasonTypes%>" codeId = "CODEID" codeName = "CODENM"  />	
			</td>
		</tr>
		<tr><td class="LLine" height="1" colspan="2"></td></tr>
		<tr class="oddshade">
		<!-- Custom tag lib code modified for JBOSS migration changes -->
			<% if (strConsignmentType.equals("4127")){ %>
			<td class="RightTableCaption" width="412" HEIGHT="25" align="right"><font id="divMandSurgDt" color="red">*</font><fmtIncludeLEEdit:message key="LBL_SURGERY_DATE"/>:</td>
			<% }else{ %>
			<td class="RightTableCaption" width="412" HEIGHT="25" align="right"><fmtIncludeLEEdit:message key="LBL_SURGERY_DATE"/>:</td>
			<% } %>
			<td>&nbsp;<input type="text" size="9" name="Txt_SurgeryDate" id="Txt_SurgeryDate" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
						value="<%=dtSelectSurgDate==null?"":(GmCommonClass.getStringFromDate(dtSelectSurgDate, strApplDateFmt))%>" onBlur="changeBgColor(this,'#ffffff');fnAddExtDate('tabout','');">
                        <img id="Img_Date" style="cursor:hand" onclick="javascript:showSglCalendar('dcalendardiv','Txt_SurgeryDate');" title="Click to open Calendar" src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />
                         <div id="dcalendardiv" style="position: absolute; z-index: 10;"></div>
			</td> 
		</tr>
		<tr><td class="LLine" height="1" colspan="2"></td></tr>
		<tr class="evenshade">
			<td class="RightTableCaption" width="412" HEIGHT="25" align="right"><font color="red">*</font><fmtIncludeLEEdit:message key="LBL_EX_DATE_OF_RETURN"/>:</td>
			<td align="Left">&nbsp;<gmjsp:calendar textControlName="Txt_ExtensionDate" textValue="<%=dtSelectExtDate==null?null:new java.sql.Date(dtSelectExtDate.getTime())%>"  
						gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
			</td> 
		</tr >
		<tr><td class="LLine" height="1" colspan="2"></td></tr>
		<tr class="oddshade">
			<td class="RightTableCaption" HEIGHT="25" align="right"><fmtIncludeLEEdit:message key="LBL_EX_FOR_WHOLE_REQ"/>:</td>
			<td>&nbsp;<input type="checkbox" name="strChkTypeFl" <%=strChkType%>> </td>
		</tr>
		<tr><td class="LLine" height="1" colspan="2"></td></tr>
		<tr class="ShadeRightTableCaption">
			<td colspan="2"  HEIGHT="25"><font color="red">*</font><fmtIncludeLEEdit:message key="LBL_COMMENTS"/>:&nbsp;&nbsp; </td>
		</tr>
		<tr><td class="LLine" height="1" colspan="2"></td></tr>
		<tr class="evenshade">
			<td colspan="2" >&nbsp;&nbsp;<textarea name="txt_Comments" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
				onBlur="changeBgColor(this,'#ffffff');" rows="3" cols="70"><%=strInputComment%></textarea></td>
		</tr>	
</table>