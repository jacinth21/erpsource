<%
	/**********************************************************************************
	 * File		 		: GmKitMapping.jsp
	 * Desc		 		: This screen is used for the kit mapping 
	 * author			: Agilan 
	 ************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%> 
<%@ taglib prefix="fmtKitMapping" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- Opertions\logistics\GmKitMapping.jsp -->
<fmtKitMapping:setLocale value="<%=strLocale%>"/>
<fmtKitMapping:setBundle basename="properties.labels.operations.logistics.GmKitMapping"/>
<bean:define id="kitId" name="frmKitMapping" property="kitId" scope="request" type="java.lang.String"></bean:define>
<bean:define id="kitNm" name="frmKitMapping" property="kitNm" scope="request" type="java.lang.String"></bean:define>
<bean:define id="setId" name="frmKitMapping" property="setId" scope="request" type="java.lang.String"></bean:define>
<bean:define id="setNm" name="frmKitMapping" property="setNm" scope="request" type="java.lang.String"></bean:define>
<bean:define id="kitNmImg" name="frmKitMapping" property="image" scope="request" type="java.lang.String"></bean:define>
<bean:define id="activeFl" name="frmKitMapping" property="activeFl" scope="request" type="java.lang.String"></bean:define>
<bean:define id="tagId" name="frmKitMapping" property="tagId" scope="request" type="java.lang.String"></bean:define>
<bean:define id="alList" name="frmKitMapping" property="alList" type="java.util.ArrayList"></bean:define>
<%
String strLogisticsJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_LOGISTICS");
String strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("KIT_MAPPING"));
String strSetID = GmCommonClass.parseNull((String)request.getAttribute("SetId"));
String a = setId;
int intTRCount=alList.size(); 
ArrayList alList1 = (ArrayList)request.getAttribute("ALLIST");
String strSetId="";
String strSetNm="";
String strTagId="";
String strKitNm="";
String strActiveFl = "";
HashMap hmLoop = new HashMap();
for (int i=0; i<intTRCount;i++ ){
	hmLoop = (HashMap)alList1.get(i);	
	strKitNm = GmCommonClass.parseNull((String)hmLoop.get("KITNM"));
	strActiveFl = GmCommonClass.parseNull((String)hmLoop.get("ACTIVEFL"));
}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Kit Mapping Setup</title>
</head>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<%-- <link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css"> --%>
<link rel="stylesheet" type="text/css"	href="<%=strJsPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCombo/dhtmlxcombo.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strLogisticsJsPath%>/GmKitMapping.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/jquery-latest.js"></script>
<script src="<%=strJsPath%>/jquery.min.js"></script>
<script>
var stractiveFl = '<%=strActiveFl%>';
</script>
<body leftmargin="20" topmargin="10" onload="fnLoad();">
<html:form action="/gmkitMapping.do?method=loadKitMapping">
 <html:hidden styleId="strOpt" property="strOpt"/>

 <input type="hidden" id="hcnt" name="hRowCnt" value="<%=intTRCount%>"/>
<table border="0" class="DtTable700" cellspacing="0" cellpadding="10px">
<tr>
			<td height="25" width="80%" class="RightDashBoardHeader">&nbsp;<fmtKitMapping:message key="LBL_HEADING"/></td>
			<td class=RightDashBoardHeader></td><td class=RightDashBoardHeader></td><td align="right" class=RightDashBoardHeader > 	
				<fmtKitMapping:message key="IMG_ALT_HELP" var="varHelp"/>
				<img id='imgEdit' style='cursor:hand' src='/images/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
			
		</tr>	
		<tr>
			<td colspan="4" height="1" class="LLine" ></td>
		</tr>	
		<tr>
			<td class="RightTableCaption" align="right" height="30" ><fmtKitMapping:message key="LBL_KIT_LIST"/>:</td>
			<td width="300" align="left"><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="kitId" />
					<jsp:param name="METHOD_LOAD" value="loadKitNameList" />
					<jsp:param name="WIDTH" value="300" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="TAB_INDEX" value="4"/>
					<jsp:param name="CONTROL_NM_VALUE" value="<%=strKitNm%>" /> 
					<jsp:param name="CONTROL_ID_VALUE" value="<%=kitId%>" />  					
					<jsp:param name="SHOW_DATA" value="10" />
				 <jsp:param name="AUTO_RELOAD" value="fnFetchKitDetails(this.value,this.form)" /> 
					</jsp:include></td>
			<td class="RightTableCaption" align="left" height="30" style="width:70px;">&nbsp;&nbsp;<fmtKitMapping:message key="LBL_KIT_ID"/>: </td>
 			   <td width="400" style="margin-left:0.5px;" align="left"><b><bean:write name="frmKitMapping" property="kitId"/></b></td>	 	
 			   	
		</tr>
		<tr bgcolor="#000000">
			<td colspan="4" height="1"></td>
		</tr>

	 <tr>
		 <td colspan="4" width="100%" height="100" valign="top">
			<table border="0" width="100%" cellspacing="0" cellpadding="0"> 
				<tr bgcolor="#E8E8E8" class="RightTableCaption">
					<td height="25" colspan="2" ><fmtKitMapping:message key="LBL_KIT_DETAILS"/></td>
				</tr>
				<tr bgcolor="#000000">
			<td colspan="2" height="1"></td>
		</tr>
				<tr>
							<td class="RightTableCaption" align="right" height="24"><font color="red">*</font>&nbsp;<fmtKitMapping:message key="LBL_KIT_NAME"/>&nbsp;:</td>	
						 <td> <html:text size="50" maxlength="30" name="frmKitMapping" style="margin-left:3.9px;  border-color:gray;"  value='<%=strKitNm%>' property="kitNm" onfocus="changeBgColor(this,'#AACCE8');" 
						     styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" onchange="fnSearchKitNm(this.value,this.form);"></html:text> 		      
				    <span id="validcheck" style="vertical-align:top; width:28px; display:inline-flex;">&nbsp;
				    <img id="setSuccimg" style="display:none;" tabindex="-1" height=16 width=19 title="${varSetAvailable}" src="<%=strImagePath%>/success.gif"></img>
				    <img id="setErrimg" style="display:none;" tabindex="-1"  title="Kit Name already exists" height=13 width=13 src="<%=strImagePath%>/error.gif"></img>
				<%--    <%if(!strKitNm.equals("")) {%>
				    <img id="setERRimg" style="display:inline;" tabindex="-1"  title="Kit Name already exists" height=13 width=13 src="<%=strImagePath%>/error.gif"></img>
				    <%} %> --%>  
				    </span>
				   
				    
						</td> 				    	
						</tr>
						<tr><td colspan="2" height="1" class="LLine" ></td></tr>
							<tr class="shade">
							<td class="RightTableCaption" align="right" height="24"><font color="red">*</font>&nbsp;<fmtKitMapping:message key="LBL_SET_SELECTION"/>&nbsp;:</td>	
						<td align="left" style="margin-top: 3.5px; position:absolute;">
						<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
						<jsp:param name="CONTROL_NAME" value="setId" />
						<jsp:param name="PARAMETER" value="" />
						<jsp:param name="METHOD_LOAD" value="loadSetNameListKit" />
						<jsp:param name="WIDTH" value="300" />
						<jsp:param name="CSS_CLASS" value="search" />
						<jsp:param name="TAB_INDEX" value=""/>
						<jsp:param name="CONTROL_NM_VALUE" value="" /> 
						<jsp:param name="CONTROL_ID_VALUE" value="" />
						<jsp:param name="SHOW_DATA" value="100" />
						<jsp:param name="AUTO_RELOAD" value="fnGetSetDetails(this.value)" />					
						</jsp:include>		
						<span id="validSetError" style="vertical-align:top; display: none; float: right; position: relative;  top: -15px; right: -20px;"><img tabindex="-1"  title="Set not in Approved status" height=13 width=13 src="<%=strImagePath%>/error.gif"></img>
						</span>
				  		</td></tr>
						<tr><td colspan="2" height="1" class="LLine" ></td></tr>
						<tr>
							<td class="RightTableCaption" align="right" height="24"><fmtKitMapping:message key="LBL_ACTIVE"/>&nbsp;:</td>	
						 <td> 
				 	<!--  <input type="checkbox" id="activeFl" onclick="uncheck();" value="">  -->
						      <html:checkbox name="frmKitMapping" property="activeFl" title="on" onclick="uncheck();" ></html:checkbox>  
						</td> 
						</tr>
					<tr style="background:#000000;">
			<td colspan="2" height="1"></td>
		</tr>	
			<table border="0" width="100%" cellspacing="0" cellpadding="0" id="KitMapping">			
		 <THEAD>  			
				<tr  height="25"  style="background:#EEEEEE;" class="RightTableCaption">		 
					<td height="25" colspan="1" width="10"></td>					
					<td height="25" colspan="1" >&nbsp;<fmtKitMapping:message key="LBL_SET_ID"/></td>			
					<td height="25" colspan="1" >&nbsp;<fmtKitMapping:message key="LBL_SET_NAME"/></td>
					<td height="25" colspan="1" >&nbsp;<fmtKitMapping:message key="LBL_TAG_ID"/></td>
				</tr>
					<tr style="background:#000000;">
			<td colspan="6" height="1"></td>
		</tr>	
		</THEAD> 			
			<TBODY>	
			<%for (int i=0; i<intTRCount;i++ ){
				hmLoop = (HashMap)alList1.get(i);			
				strSetId = GmCommonClass.parseNull((String)hmLoop.get("SETID"));
				strSetNm = GmCommonClass.parseNull((String)hmLoop.get("SETNM"));
				strTagId = GmCommonClass.parseNull((String)hmLoop.get("TAGID"));
				int set=i;	
				if(!strSetId.equals("")){
			%>  <tr id="TR<%=i %>">
			<td id="removeId<%=i%>"><img border=0  Alt=Remove  title=Remove  valign=left src=/images/red_btn_minus.gif height=10 width=9 onclick="javascript:fnRemoveItem('<%=i%>')"></img></td>
			<td id="setId<%=i%>" ><%=strSetId%></td>
			<td id="setNm<%=i%>"><%=strSetNm%></td>
			<td id="Cell<%=i%>"><img border=0 Alt=Add valign=left src=/images/plus.gif height=10 width=9 onclick="javascript:fnAddTagRow('<%=i%>','<%=strSetId%>')"> 		
			<input type=text size=10 id="tagId<%=i%>" class=InputArea value='<%=strTagId%>' onblur="javascript:fnTagSearch(this.value,'<%=i%>')">
		 <%if(!strTagId.equals("")) {%>
			<span id="validSetSuc<%=i%>" style="vertical-align:top; display: inline;">&nbsp;<img tabindex="-1" height=16 width=19 title="${varSetAvailable}" src="<%=strImagePath%>/success.gif"></img></span>
				  <%} %>
				    <span id="validSetErr<%=i%>" style="vertical-align:top; display: none;">&nbsp;
				    <img tabindex="-1" id="ASSO<%=i%>" title="Invalid data entered" height=13 width=13 src="<%=strImagePath%>/error.gif"></img></span>
			</td>
		 </tr> 
					<%}} %> 
		
			</TBODY>
		
			</table> 
				<%if((intTRCount==0) || (strSetId.equals(""))){ %>
					<tr height="50"><td></td></tr>
				<% }%> 
			<input type="hidden" id="hNewCnt" name="hNewCnt" value="0">
							<tr><td colspan="4" height="1" class="LLine" ></td></tr>
					<tr>
					<td colspan="4"> 
						<jsp:include page="/common/GmIncludeLog.jsp" > 
						<jsp:param name="FORMNAME" value="frmKitMapping" />
						<jsp:param name="ALNAME" value="alLogReasons" />
						<jsp:param name="LogMode" value="Edit" />
						<jsp:param name="Mandatory" value="yes" />
						</jsp:include>
					</td>
				</tr>
				<tr><td colspan="4" height="1" class="LLine" ></td></tr>
				<tr>
					<td colspan="4" align="center" height="24">					
			 				<fmtKitMapping:message key="BTN_SUBMIT" var="varSubmit"/>
			 				<fmtKitMapping:message key="BTN_RESET" var="varReset"/>
					    	<gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnSubmit(this.form)"/>&nbsp;&nbsp;
                    		<gmjsp:button value="${varReset}" gmClass="button" buttonType="Save" onClick="fnReset(this.form)"/>&nbsp;&nbsp;
                    </td>
				</tr>
				
			</table>
		</td>
	</tr>			
</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>