<%@page import="java.util.Locale"%>
<%
/**********************************************************************************
 * File		 		: GmIncludeLEPartEdit.jsp
 * Desc		 		: This screen is used to edit part when loaner extension
 * Version	 		: 1.0
 * author			: Angela
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>


<%@ taglib prefix="fmtIncludeLEPartEdit" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmIncludeLEPartEdit.jsp -->
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
//GmDataStoreVO gmDataStoreVO = (GmDataStoreVO)request.getAttribute("gmDataStoreVO");
String strLocaleCompany = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strLocaleCompany);
String strPartLotFl=GmCommonClass.parseNull(gmResourceBundleBean.getProperty("LOANER_PART_LOT"));
%>
<fmtIncludeLEPartEdit:setLocale value="<%=strLocale%>"/>
<fmtIncludeLEPartEdit:setBundle basename="properties.labels.operations.logistics.GmIncludeLEPartEdit"/>

<script>
var lotFlag ='<%=strPartLotFl%>';
</script>
<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	
	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	ArrayList alSetLoad = (ArrayList)request.getAttribute("CONSIGNSETDETAILS");
	String strStatus =(String) request.getAttribute("STATUS");
	HashMap hmTemp = new HashMap();	
	String strType = (String) request.getAttribute("TYPE");
	String strStock = strType.equals("4119")?"INHOUSE_STOCK":"STOCK";
	int intSize = 0;
	HashMap hcboVal = null;
	String strShipTo = "";
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strTodaysDate = (String)session.getAttribute("strSessTodaysDate")==null?"":(String)session.getAttribute("strSessTodaysDate");
%>

<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="2">
			<div style="overflow:auto; height:400px;">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				  <thead>
					<tr style="position:relative; top:expression(this.offsetParent.scrollTop);"><td colspan="9" height="1" bgcolor="#666666"></td></tr>
					<tr class="ShadeRightTableCaption" style="position:relative; top:expression(this.offsetParent.scrollTop);">
						<td width="100" height="25"><fmtIncludeLEPartEdit:message key="LBL_PART"/></td>
						<td width="300"><fmtIncludeLEPartEdit:message key="LBL_DESCRIPTION"/></td>
						<td align="center" width="60"><fmtIncludeLEPartEdit:message key="LBL_QTY_IN_SET"/></td>
						<td align="center" width="60"><fmtIncludeLEPartEdit:message key="LBL_QTY_MISSING"/></td>
						<% if(strPartLotFl.equals("YES")){ %>
						<td align="center" width="180"><fmtIncludeLEPartEdit:message key="LBL_USED_LOT"/></td>
						<% } %>
						<%if(!strStatus.equals("HIDE")) {%>
						<td align="center" width="60"><fmtIncludeLEPartEdit:message key="LBL_QTY_TO_SHELF"/></td>
						<td align="center" width="60"><fmtIncludeLEPartEdit:message key="LBL_QTY_TO_QUARANTINE"/></td>
						<%}%>
						<td class="Line" width="1"></TD>
						<td align="center" width="60"><%=strType.equals("4119")?"In-House":"Shelf"%><fmtIncludeLEPartEdit:message key="LBL_QTY"/></td>
						<td align="center" width="60"><fmtIncludeLEPartEdit:message key="LBL_QTY_TO_REPLENISH"/></td>
					</tr>
					<tr><td colspan="9" height="1" bgcolor="#666666"></td></tr>
					</thead>
					<tbody>
<%
						intSize = alSetLoad.size();
						if (intSize > 0)
						{
							hcboVal = new HashMap();
							for (int i=0;i<intSize;i++)
							{
								hcboVal = (HashMap)alSetLoad.get(i);
%>
						<tr id="tr<%=i%>" >
							<td width="100" class="RightText" height="20" id="COLUMN5<%=i%>">
							<%if(strPartLotFl.equals("YES")){ %>
							<a href="javascript:fnSplit('<%=i%>','<%=(String)hcboVal.get("PNUM")%>');"> 
					  		<img src="<%=strImagePath%>/plus.gif" width="12" height="12" border="0"></a>
					  		<%} %>
							&nbsp;<%=(String)hcboVal.get("PNUM")%>
							<input type="hidden" name="hPartNum<%=i%>" value="<%=(String)hcboVal.get("PNUM")%>"/>
							<input type="hidden" name="hSetQty<%=i%>" value="<%=(String)hcboVal.get("QTY")%>"/>
							<input type="hidden" name="hPrice<%=i%>" value="<%=(String)hcboVal.get("PRICE")%>"/>
							</td>
							<td class="RightText" width="300" wrap>&nbsp;<%=GmCommonClass.getStringWithTM((String)hcboVal.get("PDESC"))%></td>
							<td align="center" class="erDark" id="COLUMN3<%=i%>"><%=(String)hcboVal.get("QTY")%></td>
							<td align="center" class="erMed" id="COLUMN1<%=i%>" ><input type="text" name="Txt_Miss<%=i%>" value="" size="3" class="RightText" onFocus="changeTROBgColor(this,<%=i%>,'#AACCE8');" 
							<% if(strPartLotFl.equals("YES")){ %>
							onBlur="fnPartLotAjax(<%=i%>,hPartNum<%=i%>.value);"
							<%} %>
							></td>
							<% if(strPartLotFl.equals("YES")){ %>
						        <td align="center"  id="COLUMN111<%=i%>" class="erMed" width="180" onchange="javascript:fnCheckLotNum(<%=i%>,hPartNum<%=i%>.value)">
						        <select name="Cbo_UseLot<%=i%>" id="Cbo_UseLot<%=i%>" style="width: 150px;" >
							    <option value="0" ><fmtIncludeLEPartEdit:message key="LBL_CHOOSE_ONE"/></option>
							</select>
						        </td>
						     <% } %>	
							<%if(!strStatus.equals("HIDE")) {%>
							<td align="center" class="erMed"><input type="text" name="Txt_Ret<%=i%>" value="" size="3" class="RightText" onFocus="changeTROBgColor(this,<%=i%>,'#AACCE8');"></td>
							<td align="center" class="erMed"><input type="text" name="Txt_Quar<%=i%>" value="" size="3" class="RightText" onFocus="changeTROBgColor(this,<%=i%>,'#AACCE8');"></td>
							<%}%>
							<td class="Line" width="1"></TD>
							<td align="center" class="erLight" id="COLUMN4<%=i%>" ><%=(String)hcboVal.get(strStock)%>
							<input type="hidden" name="hStock<%=i%>" value="<%=(String)hcboVal.get(strStock)%>"/></td>
							<td align="center" class="erLight" id="COLUMN2<%=i%>"><input type="text" name="Txt_Rep<%=i%>" value="" size="3" class="RightText" onFocus="changeTROBgColor(this,<%=i%>,'#AACCE8');"></td>
						</tr>
						<tr><td bgcolor="#eeeeee" colspan="9" height="1"></td></tr>					
<%
							} // end of FOR
						}else {
%>
						<tr><td colspan="9" height="50" align="center" class="RightTextRed"><BR><fmtIncludeLEPartEdit:message key="MSG_NO_DATA"/> !</td></tr>
<%
						}		
%>
					
					</tbody>
				</table>
			 </div>
			</td>
			<input type="hidden" name="hcounter" value="<%=intSize%>" />
			<input type="hidden" name="hCnt" value="<%=intSize%>">
		</tr>	
		<tr><td colspan="2" height="1" class="Line"></td></tr>
	</table>