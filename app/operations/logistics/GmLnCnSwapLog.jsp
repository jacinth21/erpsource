<%
/**********************************************************************************
 * File		 		: GmLnCnSWAPLog.jsp
 * Created Date		: Aug 2010 	
 * Description 		: LoanerConsignment SWAP report
 * author			: Velu
************************************************************************************/
%>

<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtLnCnSwapLog" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmLnCnSwapLog.jsp -->

<fmtLnCnSwapLog:setLocale value="<%=strLocale%>"/>
<fmtLnCnSwapLog:setBundle basename="properties.labels.operations.logistics.GmLnCnSwapLog"/>


<HTML>
<HEAD>
<TITLE> Globus Medical: Loaner to Consignment Details</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<%@ page import="com.globus.operations.logistics.forms.GmLnCnSWAPLogForm"%>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmLnCnSWAPLog.js"></script>

<bean:define id="gridData" name="frmLnCnSWAPLog" property="gridXmlData" type="java.lang.String"> </bean:define>

<%@ page import="java.util.List,java.util.Iterator"%>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
</HEAD>

<script>
	var objGridData;
	objGridData = '<%=gridData%>';
</script>


<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnOnPageLoad();">
<html:form action="/GmLnCnSWAPLog.do" >
		<table align="center">
			<tr><td align="left" height="25" class="RightDashBoardHeader">&nbsp;&nbsp;<fmtLnCnSwapLog:message key="LBL_CONSIGNMENT_DETAILS"/></td></tr>
       	 	<tr>
	       	 	<td  height="70" ><div id="astAttData" style="grid" height="350px" width="530px"></div></td>
       	 	</tr>	
			<tr>
			<fmtLnCnSwapLog:message key="BTN_CLOSE" var="varClose"/>
				<td  align="center"><gmjsp:button value="&nbsp;${varClose}&nbsp;" gmClass="button" onClick="fnClose();" tabindex="25" buttonType="Load" /></td>
			</tr>
   		</table>	     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
