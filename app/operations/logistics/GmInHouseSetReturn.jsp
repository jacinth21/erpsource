<%
/**********************************************************************************
 * File		 		: GmInHouseSetReturn.jsp
 * Desc		 		: This screen is used to return and reconcile In-House Sets
 * Version	 		: 1.0
 * author			: James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@page import="java.util.Date"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtInHouseSetReturn" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>


<!-- operations\logistics\GmInHouseSetReturn.jsp -->

<fmtInHouseSetReturn:setLocale value="<%=strLocale%>"/>
<fmtInHouseSetReturn:setBundle basename="properties.labels.operations.logistics.GmInHouseSetReturn"/>


<%
String strLogisticsJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_LOGISTICS");
String strWikiTitle = GmCommonClass.getWikiTitle("ACCEPT_RETURNS");
%>

<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	//ArrayList alSetLoad = (ArrayList)request.getAttribute("CONSIGNSETDETAILS");
	ArrayList alCam = new ArrayList();
	HashMap hmCsg = new HashMap();
	// To get the Consignment information 
	hmCsg = (HashMap)request.getAttribute("hmCONSIGNMENT");
	if (hmCsg == null)
	{
		hmCsg = (HashMap)session.getAttribute("hmCONSIGNMENT");
	}
	String strConsignId = GmCommonClass.parseNull((String)hmCsg.get("CONSIGNID"));
	String strType = GmCommonClass.parseNull((String)hmCsg.get("CTYPE"));
	String strTodaysDate = (String)session.getAttribute("strSessTodaysDate")==null?"":(String)session.getAttribute("strSessTodaysDate");
	Date dtCurrentDate = GmCommonClass.getCurrentDate(strGCompDateFmt, strGCompTimeZone);
	String strCurrentDate = GmCommonClass.getStringFromDate(dtCurrentDate,strGCompDateFmt);
	Date dtTodaysDate = GmCommonClass.getStringToDate(strTodaysDate,GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt")));
	String strHeader = GmCommonClass.parseNull((String)request.getAttribute("header"));
	String strSetID= GmCommonClass.parseNull(request.getParameter("hSetId"));
	String strCamId = GmCommonClass.parseNull((String)request.getAttribute("strCamId"));
	alCam = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALCAMERA"));
	String strDefaultComment = GmCommonClass.parseNull((String)GmCommonClass.getRuleValue("LOANER_SCREEN_AR","DEFAULT_COMMENT")); 
    
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: In-House Sets Returns </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strLogisticsJsPath%>/GmInHouseSetReturn.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
</head>
<SCRIPT>
	var val = '<%=strConsignId%>';
	var type = '<%=strType%>';
	var setid='<%=strSetID%>';	
	var default_comment='<%=strDefaultComment%>';
	var dateFmt =  '<%=strGCompDateFmt%>';
	var todaysdate = '<%=strCurrentDate%>';
</SCRIPT>
<BODY leftmargin="20" topmargin="10" onload="fnLoad();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmInHouseSetServlet">
<input type="hidden" name="hAction" value ="">
<input type="hidden" name="hOpt" value ="">


	<table border="0" class="DtTable800" width="800" cellspacing="0" cellpadding="0">
		<tr>
		<td class="RightDashBoardHeader">
			<table width="100%">
				<tr>
					<td height="25" class="RightDashBoardHeader" ><%=strHeader%></td>
					<td class="RightDashBoardHeader" align="right"><fmtInHouseSetReturn:message key="IMG_ALT_HELP" var = "varHelp"/>
					<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
				</tr>
			</table>
		</td>
		</tr>
		<tr>
			<td >	
				<jsp:include page="/include/GmIncludeConsignment.jsp" >
				<jsp:param name="PARENTJSP" value="AcceptReturn" />
				</jsp:include>
			</td>
		</tr>
			<tr><td colspan="2" height="1" class="Line"></td></tr>
		<tr>
             <td>
               	<jsp:include page="/common/GmIncludeLog.jsp" >
					<jsp:param name="LogType" value="" />
					<jsp:param name="LogMode" value="Edit" />
					<jsp:param name="ShowCam" value="true" />					
				</jsp:include>
			</td>
        </tr> 
		<tr><td  height="1" bgcolor="#cccccc"></td></tr>			
		<tr>
			<td align="center" height="30" valign="middle">
			<fmtInHouseSetReturn:message key="BTN_SUBMIT" var="varSubmit"/>
				<gmjsp:button  name="Btn_submit" value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="fnSubmit();" tabindex="25" buttonType="Save" />
			</td>
		</tr>		
	</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
