<!-- \operations\logistics\GmKitMappingReport.jsp -->
<%
	/**********************************************************************************
	 * File		 		: GmKitMappingReport.jsp
	 * Desc		 		: This screen is used for the kit mapping report
	 * author			: Agilan 
	 ************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@page import="java.util.Date"%> 
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%> 
<%@ page import = "java.util.*,java.io.*"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ taglib prefix="fmtKitMapping" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtKitMapping:setLocale value="<%=strLocale%>"/>
<fmtKitMapping:setBundle basename="properties.labels.operations.logistics.GmKitMappingReport"/>
<bean:define id="kitId" name="frmGmKitMapping" property="kitId" scope="request" type="java.lang.String"></bean:define>
<bean:define id="kitNm" name="frmGmKitMapping" property="kitNm" scope="request" type="java.lang.String"></bean:define>
<bean:define id="setId" name="frmGmKitMapping" property="setId" scope="request" type="java.lang.String"></bean:define>
<bean:define id="setNm" name="frmGmKitMapping" property="setNm" scope="request" type="java.lang.String"></bean:define>
<bean:define id="searchkitId" name="frmGmKitMapping" property="searchkitId" scope="request" type="java.lang.String"></bean:define>
<bean:define id="searchsetId" name="frmGmKitMapping" property="searchsetId" scope="request" type="java.lang.String"></bean:define>
<%
String strLogisticsJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_LOGISTICS");
String strGridXmlData = GmCommonClass.parseNull((String)request.getAttribute("XMLGRIDDATA"));  
ArrayList alReturn = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALRETURN"));
String selectedValue = GmCommonClass.parseNull((String)request.getAttribute("selectedValue"));
int intSize = alReturn.size();
String strWikiTitle = GmCommonClass.getWikiTitle("KIT_MAPPING_REPORT");
%>
<HTML>
<HEAD>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="styleSheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script  language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script> 
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strLogisticsJsPath%>/GmKitMapping.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script type="text/javascript">
var objGridData;
var selectedValues  = '<%=selectedValue%>';
objGridData = '<%=strGridXmlData%>'; 
var gridObj;

</script>
<style type="text/css" media="all">
@import url("styles/screen.css"); 
</style>

</HEAD>
<body leftmargin="15" topmargin="10" onload="selectedValue();fnOnPageLoad(); ">
 <html:form action="/gmKitMappingReport.do?method=loadKitMapReports" > 
 
 <html:hidden name="frmGmKitMapping"  property="strOpt" value="" /> 
 
<table class="DtTable1000" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="5" height="25" class="RightDashBoardHeader">&nbsp;<fmtKitMapping:message key="LBL_NAME"/> </td>
				<td height="25" align="right" class=RightDashBoardHeader><fmtKitMapping:message key="IMG_ALT_HELP" var="varHelp"/>				    
				 <img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
             </tr>
              <tr class="evenshade"> 
              <td width="80px" class="RightRedCaption" align="Right">
                  &nbsp;<fmtKitMapping:message key="LBL_KIT_NAME"/>:&nbsp;</td>
	 		                  <td width="150px">       <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
	 		                                        <jsp:param name="CONTROL_NAME" value="kitId" />
	 		                                        <jsp:param name="METHOD_LOAD" value="loadKitNameList" />
	 		                                        <jsp:param name="WIDTH" value="300" />
	 		                                        <jsp:param name="CSS_CLASS" value="search" />
	 		                                        <jsp:param name="TAB_INDEX" value="4"/>
	 		                                        <jsp:param name="CONTROL_NM_VALUE" value="<%=searchkitId%>" /> 
	 		                                        <jsp:param name="CONTROL_ID_VALUE" value="<%=kitId%>" />                                          
	 		                                        <jsp:param name="SHOW_DATA" value="" />
	 		                                        <jsp:param name="AUTO_RELOAD" value="" />
	 		                        </jsp:include></td>
	 		                            <td height="30" class="RightTableCaption" align="right" width="15%">&nbsp;<fmtKitMapping:message key="LBL_CASEID"/>:</td>
		<td width="10%">&nbsp;<html:text property="caseId"  size="23" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" /></td>
	 		                         
	 		          <td width="15%" class="RightTableCaption" HEIGHT="24"  align="right"><fmtKitMapping:message key="LBL_STATUS"/>:</td>
			<td>&nbsp;
			<select name="kitStatus">
						<option  value="0">Active</option>
						<option value="1">Inactive</option>
					  </select>
			<input type="hidden" name="selectedValue" value="<%=selectedValue%>"> </td>
			</tr>
 	 	 		                 	 	 		                
 	 	  	 	      <tr><td colspan="6" height="1" bgcolor="#CCCCCC"></td></tr>
					  <tr class="oddshade"> 
					    <td class="RightRedCaption" align="Right">&nbsp;<fmtKitMapping:message key="LBL_SET"/>:&nbsp;</td>
 	 	 		        <td colspan="1">
 	 	 		                        <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
 	 	 		                                        <jsp:param name="CONTROL_NAME" value="setId" />
 	 	 		                                        <jsp:param name="METHOD_LOAD" value="loadSetNameListKit" />
 	 	 		                                        <jsp:param name="WIDTH" value="300" />
 	 	 		                                        <jsp:param name="CSS_CLASS" value="search" />
 	 	 		                                        <jsp:param name="TAB_INDEX" value="4"/>
 	 	 		                                        <jsp:param name="CONTROL_NM_VALUE" value="<%=searchsetId%>" /> 
 	 	 		                                        <jsp:param name="CONTROL_ID_VALUE" value="<%=setId%>" />                                          
 	 	 		                                        <jsp:param name="SHOW_DATA" value="" />
 	 	 		                                        <jsp:param name="AUTO_RELOAD" value="" />
 	 	 		                                         </jsp:include>
 	 	 		          </td>  
 	 	 		    <td height="30" class="RightTableCaption" align="right" width="10%">&nbsp;<fmtKitMapping:message key="LBL_TAG"/>:</td>
		<td width="10%" >&nbsp;<html:text property="tagId"  size="23" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/></td> 	 	 		                       
 	 	 		              <td></td>            
 	 	 		           <td class="RightText" HEIGHT="24" align="left">
			              <gmjsp:button gmClass="button" name="Btn_Load" tabindex="25" buttonType="Save" onClick="fnLoadKit();" value="Load" />                    
						   </td> 
						 </tr>
 	 	 		         <tr><td colspan="6" height="1" bgcolor="#CCCCCC"></td></tr>
		        	      <%if(intSize > 0){ %> 
      						<tr> <td colspan="4">
 	 	 		              	<div id="kitMappingRpt" style="height: 500px; width:995px; display:table-caption;"></div>
 	 	 		            </td> </tr>
     	                 <tr><td colspan="6" height="1" bgcolor="#cccccc"></td></tr>
			     		<tr>
						<td colspan="6" align="center">
						    <div class='exportlinks'><fmtKitMapping:message key="LBL_EXPORT_OPTS"/> : <img src='img/ico_file_excel.png' onclick="fnExcel();" />&nbsp;<a href="#" onclick="fnExcel();"><fmtKitMapping:message key="LBL_EXCEL"/></a></div>
						</td>
		</tr>
		<% }else{%>
		 <tr>
		 <td height="30" colspan="6" align="center" class="RightTextRed"><fmtKitMapping:message key="LBL_NO_DIS"/>.</td>
		</tr>
		<%} %>	  		                
</table>
</html:form>
  <%@ include file="/common/GmFooter.inc" %> 
</BODY>
</HTML>