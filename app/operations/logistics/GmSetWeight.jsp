<%
/**********************************************************************************
 * File		 		: GmSetWeight.jsp
 * Version	 		: 1.0
 * author			: Jignesh Shah
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%> 
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtSetWeight" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmSetWeight.jsp -->

<fmtSetWeight:setLocale value="<%=strLocale%>"/>
<fmtSetWeight:setBundle basename="properties.labels.operations.logistics.GmSetWeight"/>

<bean:define id="strOpt" name="frmSetWeight" property="strOpt" type="java.lang.String"></bean:define>
<bean:define id="updatefl" name="frmSetWeight" property="updatefl" type="java.lang.String"></bean:define>
<bean:define id="returnReport" name="frmSetWeight" property="returnReport" type="java.util.List"></bean:define>

<%
	String strLogisticsJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_LOGISTICS");
	String strWikiTitle = GmCommonClass.getWikiTitle("SET_WEIGHT_SETUP");
	String strDisable = "true";
	ArrayList alReportList = new ArrayList();
	alReportList = GmCommonClass.parseNullArrayList((ArrayList) returnReport);
	int rowsize = alReportList.size();
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Set Weight Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- <link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css"> -->
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strLogisticsJsPath%>/GmSetWeight.js"></script>

<script>
var accessfl = '<%=updatefl%>';
var varRows = <%=rowsize%>;
</script>
<style>
table.itss {
	width: 100%;
}

table.itss thead tr {
	background-color: #cccccc;
	height: 25px;
}

table.itss tr.even {
	background-color: #ecf6fe;
}

table.itss thead th {
	FONT-SIZE: 11px; 
	text-align: left;
}
</style>
</HEAD>
<BODY leftmargin="20" topmargin="10" onkeyup="fnEnter();" >
<html:form action="/gmSetWeight.do?method=setWeightReport" >
<html:hidden property="strOpt" />
<html:hidden property="hinputStr" />
<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
	<tr>
		<td height="25" class="RightDashBoardHeader" colspan="6"><fmtSetWeight:message key="LBL_SETWEIGHT_SETUP"/></td>
		<td align="right" class=RightDashBoardHeader > 
		<fmtSetWeight:message key="IMG_ALT_HELP" var ="varHelp"/>	
			<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' 
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />  
		</td>
	</tr>
	<tr class="shade"> 
	<!-- Struts tag lib code modified for JBOSS migration changes -->            
    	<td class="RightTableCaption" align="Right" width="10%" height="25"><fmtSetWeight:message key="LBL_SET_ID"/>:</td>
        <td width="17%">&nbsp;<html:text maxlength="50" property="setId" size="15" styleClass="InputArea"
				onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
		</td>
		<td class="RightTableCaption" align="Right" width="8%" height="25"><fmtSetWeight:message key="LBL_SET_NAME"/>:</td>
		<td >&nbsp;<html:text maxlength="50" property="setNm" size="35" styleClass="InputArea"
			onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
		</td>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<td class="RightTableCaption" align="Right" height="25"><fmtSetWeight:message key="LBL_TYPE"/>:</td>
		<td align="left">&nbsp;<gmjsp:dropdown controlName="setType" width="100" SFFormName="frmSetWeight" SFSeletedValue="setType"
			 SFValue="alSetType" codeId="CODEID" codeName="CODENM" defaultValue= "[Choose One]" />
		</td>
        <td align="center">
        <fmtSetWeight:message key="BTN_LOAD" var="varLoad"/> 
        	<gmjsp:button value="${varLoad}" gmClass="Button" name="btn_Load" style=": height: 22" onClick="fnLoad();" buttonType="Load" />
        </td>
    </tr>
    <tr><td class="LLine" height="1" colspan="7"></td></tr>
    <tr>
		<td colspan="7" align="center">
			<display:table name="requestScope.frmSetWeight.returnReport" requestURI="/gmSetWeight.do?method=setWeightReport" 
				export="true" decorator="com.globus.operations.logistics.beans.DTSetWeightWrapper" class="itss" id="currentRowObject" >
			<display:setProperty name="export.excel.decorator" value="com.globus.operations.logistics.beans.DTSetWeightWrapper" />  
			<fmtSetWeight:message key="DT_SET_ID" var="varSetID"/>
			<display:column property="SETID" title="${varSetID}" sortable="true" class="alignleft" media="html"/>	
			<display:column property="EXCELSETID" title="${varSetID}" sortable="true" class="alignleft" media="excel"/>
			<fmtSetWeight:message key="DT_SET_NAME" var="varSetName"/>
			<display:column property="SETDESC" title="${varSetName}" sortable="true" class="alignleft" />
			<fmtSetWeight:message key="DT_WEIGHT_LBS" var="varWeightlbs"/>
			<display:column property="WEIGHT" title="${varWeightlbs}" media="html" style="text-align:left;width:100;"/>			
			<display:column property="EXCELWEIGHT" title="${varWeightlbs}" class="alignleft" media="excel" />
			<fmtSetWeight:message key="DT_UPDATED_BY" var="varUpdatedBy"/>
			<display:column property="UPDATEDBY" title="${varUpdatedBy}" sortable="true"  />
			<fmtSetWeight:message key="DT_UPDATED_DATE" var="varUpdated_Date"/>
			<display:column property="UPDATEDDATE" title="${varUpdated_Date}" sortable="true" class="aligncenter" />
			</display:table> 
		</td>
	</tr>
	<%if(rowsize > 0) { %>
    <tr><td colspan="7" class="LLine" height="1"></td></tr>
    <tr></tr>
    <%if(updatefl.equals("Y")){ %>
        	<tr><td align="center" colspan="7"><fmtSetWeight:message key="BTN_SUBMIT" var="varSubmit"/>
        	<gmjsp:button value="${varSubmit}" gmClass="Button" buttonType="Save"
    		name="Btn_Submit" disabled="<%=strDisable %>" style=": height: 22" onClick="fnSubmit();" /></td>
    	</tr>
    <%} %>
    <tr></tr>
    <%} %>
</table>		     	
</html:form>
</BODY>
 <%@ include file="/common/GmFooter.inc" %> 
</HTML>
