<%
/**********************************************************************************
 * File		 		: GmIncludeOpenBackOrder.jsp
 * Desc		 		: This screen is used to Show to Open Back Order
 * Version	 		: 1.0
 * author			: Rajkumar
************************************************************************************/
%>
   <%@include file="/common/GmHeader.inc" %>
   <%@ page import="java.util.ArrayList"%>
   <%@page import="java.util.HashMap"%>
   
   <!-- operations\logistics\GmIncludeOpenBackOrder.jsp -->
   
   <%@ taglib prefix="fmtIncludeOpenBackOrder" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtIncludeOpenBackOrder:setLocale value="<%=strLocale%>"/>
<fmtIncludeOpenBackOrder:setBundle basename="properties.labels.operations.logistics.GmIncludeOpenBackOrder"/>
   
    <%
    ArrayList alResult=(ArrayList)request.getAttribute("alResult");
    HashMap hmCsg = new HashMap(); 
	
	// To get the Consignment information 
	hmCsg = (HashMap)request.getAttribute("hmCONSIGNMENT");
	if (hmCsg == null)
	{
		hmCsg = (HashMap)session.getAttribute("hmCONSIGNMENT");
	}
    String strConsignId = GmCommonClass.parseNull((String)hmCsg.get("CONSIGNID"));
    String strTabClass = GmCommonClass.parseNull(request.getParameter("LogType"));
    strTabClass = strTabClass.equals("YES")?"DtTable950":"DtTable800";
    String strStatus = GmCommonClass.parseNull((String) hmCsg.get("LOANSFLCD"));
    String strhScrnFrom = GmCommonClass.parseNull(request.getParameter("hScreenFrom"));
    String strCNType = GmCommonClass.parseNull((String) hmCsg.get("CTYPE"));
    %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>
var cnType = '<%=strCNType%>';
</script>
<title>Open Transactions</title> 
</head>
<body>
<table border="0" style="overflow:auto;" class="<%=strTabClass%>" cellspacing="0" cellpadding="0">
<tr height="24" class="ShadeRightTableCaption">
<td align="left" class="RightTableCaption" colspan="2">
<%if(strhScrnFrom.equals("PROCESS_TXN_SCN")){%>
	<fmtIncludeOpenBackOrder:message key="LBL_OPEN_BO_TRANS_ASS_TO"/>
<%}else{%>
	<fmtIncludeOpenBackOrder:message key="LBL_OPEN_TRANS_ASS_TO"/>
<%} %>
<b><%=strConsignId%></b></td>
</tr>
<tr>
	<td>
			<display:table name="alResult" class="its" id="currentRowObject" decorator="com.globus.operations.logistics.beans.DtOpenBackOrder"> 
 		 		<fmtIncludeOpenBackOrder:message key="DT_PART" var="varPart"/><display:column property="PNUM" title="${varPart}" group="1" class="alignleft"/>
				<fmtIncludeOpenBackOrder:message key="DT_DESCRIPTION" var="varDescription"/><display:column property="PDESC" title="${varDescription}" group="1" class="alignleft" /> 
				<fmtIncludeOpenBackOrder:message key="DT_TRANSACTION_ID" var="varTransactionID"/><display:column property="ID" title="${varTransactionID}"  class="alignleft" />
				<fmtIncludeOpenBackOrder:message key="DT_BO_QTY" var="varBOQty"/><display:column property="QTY" title="${varBOQty}" class="alignright"/> 
				<fmtIncludeOpenBackOrder:message key="DT_DATE" var="varDate"/><display:column property="CRDATE" title="${varDate}" class="alignleft"/>
				<fmtIncludeOpenBackOrder:message key="DT_STATUS" var="varStatus"/><display:column property="STATUS" title="${varStatus}" class="alignleft"/>
				<fmtIncludeOpenBackOrder:message key="DT_TYPE" var="varType"/><display:column property="TYPE" title="${varType}" class="alignleft"/>
				<%if(strhScrnFrom.equals("PROCESS_TXN_SCN")){%>
				<fmtIncludeOpenBackOrder:message key="DT_BO_RPT" var="varBORpt"/><display:column property="BORPTFL"  title="${varBORpt}" class="aligncenter"/>
				<%} %>
			</display:table>
		</td>
</tr>
</table>
</body>

</html>