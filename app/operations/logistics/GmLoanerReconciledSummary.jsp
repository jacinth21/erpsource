<%
/**********************************************************************************
 * File		 		: Loaner Reconcililed.jsp
 * Desc		 		: Loaner Reconciled Summary
 * Version	 		: 1.0
 * author			: Brinal G
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtLoanerReconciledSummary" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmLoanerReconciledSummary.jsp -->

<fmtLoanerReconciledSummary:setLocale value="<%=strLocale%>"/>
<fmtLoanerReconciledSummary:setBundle basename="properties.labels. operations.logistics.GmLoanerReconciledSummary"/>

<%String strWikiTitle = GmCommonClass.getWikiTitle("LOANER_RECONCD_SUMMARY"); %>

<HTML>
<HEAD>

<TITLE> Globus Medical: Loaner Reconciliation </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/gmLoanerReconciliation.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
</HEAD>


<BODY leftmargin="20" topmargin="10" >

<html:form action="/gmLoanerReconciledSummary">
<html:hidden property="strOpt" name="frmLoanerReconciledSummary"/>
<input type="hidden" name ="hAction" />

<table border="0" class= "DtTable900" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" colspan="2" class="RightDashBoardHeader" ><fmtLoanerReconciledSummary:message key="LBL_LOANER_RECONCILED_SUMMARY"/></td>			
			<td class="RightDashBoardHeader"></td>
			<td  height="25" class="RightDashBoardHeader" align="right" colspan="2">
			   <fmtLoanerReconciledSummary:message key="IMG_ALT_HELP" var = "varHelp"/>
			    <img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
		       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		    </td>
		    <td></td>
		</tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr class="Shade">
			<td  height="25" class="RightTableCaption" align="Right"><fmtLoanerReconciledSummary:message key="LBL_DISTRIBUTOR_LIST"/>:</td>
			<td colspan="3">&nbsp;<gmjsp:dropdown controlName="dist"  SFFormName='frmLoanerReconciledSummary' SFSeletedValue="dist"  defaultValue= "[Choose One]"	
				SFValue="alDist" codeId="ID" codeName="NAME" /></td>				
		</tr>
		<tr><td class="LLine" height="1" colspan="4"></td></tr>
		<tr >
			<td  height="25" class="RightTableCaption" align="Right"><fmtLoanerReconciledSummary:message key="LBL_LOANER_TRANSACTION_ID"/>:</td>
			<td>&nbsp;<html:text property="lnTransId" name="frmLoanerReconciledSummary" size="15" /></td>	
			<td align="right" class="RightTableCaption" align="Right"><fmtLoanerReconciledSummary:message key="LBL_PART_#"/>:</td>
			<td>&nbsp;<html:text property="partNum" name="frmLoanerReconciledSummary" size="15" />	&nbsp;
				<fmtLoanerReconciledSummary:message key="BTN_LOAD" var="varLoad"/>
				<gmjsp:button value="&nbsp;${varLoad}&nbsp;"  name="Btn_Submit" gmClass="button" buttonType="Load" />	
			</td>						
		</tr>
		<tr><td class="LLine" height="1" colspan="4"></td></tr>
		<tr>
			<td colspan="4" align="center" height="50">
				<display:table name="requestScope.frmLoanerReconciledSummary.lresult" class="its" 
						id="currentRowObject" requestURI="/gmLoanerReconciledSummary.do" style="height:35" export="true" freezeHeader="true">
					<fmtLoanerReconciledSummary:message key="DT_DISTRIBUTOR" var="varDistributor"/>
					<display:column property="DNAME" title="${varDistributor}" group="1" style="width:100" sortable="true"/>
					<fmtLoanerReconciledSummary:message key="DT_TRANS_ID" var="varTransId"/>
					<display:column property="TRANSID" title="${varTransId }" style="width:50" sortable="true"/>			
					<fmtLoanerReconciledSummary:message key="DT_ETCH_ID" var="varEtchId"/>
					<display:column property="ETCHID" title="${varEtchId}Etch Id"  style="width:80" sortable="true"/>
					<fmtLoanerReconciledSummary:message key="DT_SET_ID" var="varSetId"/>
					<display:column property="SETID" title="${varSetId}"   style="width:100" sortable="true" class="aligncenter"/>											
					<fmtLoanerReconciledSummary:message key="DT_PART_NUM" var="varPartNum"/>
					<display:column property="PARTNUM" title="${varPartNum}"  style="width:100"  sortable="true" class="aligncenter"/>
					<fmtLoanerReconciledSummary:message key="DT_PART_DESC" var="varPartDesc"/>
					<display:column property="PARTDESC" escapeXml="true"  title="${varPartDesc}" style="width:150"  sortable="true" class="aligncenter"/>
					<fmtLoanerReconciledSummary:message key="DT_SOLD" var="varSold"/>
					<display:column property="SOLD" title="${varSold}"   sortable="true" style="width:50"class="aligncenter"/>
					<fmtLoanerReconciledSummary:message key="DT_RECONCILED" var="varReconciled"/>
					<display:column property="RECONCILED" title="${Reconciled}"    style="width:80"sortable="true" class="aligncenter"/>
					<fmtLoanerReconciledSummary:message key="DT_WRITEOFF" var="varWriteOff"/>
					<display:column property="WRITEOFF" title="${varWriteOff}"   style="width:80" sortable="true" class="aligncenter"/>
					<fmtLoanerReconciledSummary:message key="DT_CONSIGNED" var="varConsigned"/>
					<display:column property="CONSIGNED" title="${varConsigned}"  style="width:80"  sortable="true" class="aligncenter"/>
				</display:table>
			</td>
			
		</tr>		
</table>		
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>

