<%
/****************************************************************************************************
 * File		 		: GmPendingRtnIHItemRpt.jsp
 * Desc		 		: This jsp will show the Pending Shipping Return In-House Loaner Item Details
 * Version	 		: 1.0
 * author			: Hreddi
******************************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtPendingRtnIhItemReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmPendingRtnIHItemRpt.jsp -->
<fmtPendingRtnIhItemReport:setLocale value="<%=strLocale%>"/>
<fmtPendingRtnIhItemReport:setBundle basename="properties.labels.custservice.Shipping.GmShippingReport"/>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%> 
<bean:define id="xmlGridData" name="frmIHLoanerItem" property="xmlGridData" type="java.lang.String"></bean:define>
<bean:define id="eventLoanToName" name="frmIHLoanerItem" property="eventLoanToName" type="java.lang.String"></bean:define>
<bean:define id="dtType" name="frmIHLoanerItem" property="dtType" type="java.lang.String"></bean:define>
<bean:define id="loanType" name="frmIHLoanerItem" property="loanType" type="java.lang.String"></bean:define>
<bean:define id="alEmpList" name="frmIHLoanerItem" property="alEmpList" type="java.util.ArrayList"></bean:define>
<bean:define id="alRepList" name="frmIHLoanerItem" property="alRepList" type="java.util.ArrayList"></bean:define>
<bean:define id="alOUSList" name="frmIHLoanerItem" property="alOUSList" type="java.util.ArrayList"></bean:define>
<%
String strLogisticsJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_LOGISTICS");
HashMap hcboVal = null;
String strDisableDpdwn = "";
String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
if(eventLoanToName.equals("0") || eventLoanToName.equals("")){
	strDisableDpdwn = "disabled";
}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Event List Report  </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strLogisticsJsPath%>/GmPendingRtnIHItemRpt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script type="text/javascript">
var objGridData = '<%=xmlGridData%>';
var strdtType  = '<%=dtType%>';
var strloanType = '<%=loanType%>';
var RepLen = <%=alRepList.size()%>;
var EmpLen = <%=alEmpList.size()%>;
var OUSLen = <%=alOUSList.size()%>;
var format = '<%=strApplDateFmt%>';
<%
	hcboVal = new HashMap();
	for (int i=0;i<alRepList.size();i++){
		hcboVal = (HashMap)alRepList.get(i);
%>
		var alRepListArr<%=i%> ="<%=hcboVal.get("SID")%>,<%=hcboVal.get("NAME")%>";
<%  }   %>
<%
	hcboVal = new HashMap();
	for (int i=0;i<alEmpList.size();i++){
		hcboVal = (HashMap)alEmpList.get(i);
%>
		var alEmpListArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%	}%>

<%
hcboVal = new HashMap();
for (int i=0;i<alOUSList.size();i++)
{
	hcboVal = (HashMap)alOUSList.get(i);
%>
var alOUSListArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
}
%>
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<html:form  action="/gmIHLoanerItem.do?method=loadPendingRtnIHReport">
<html:hidden property="strOpt" name="frmIHLoanerItem"/>
<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" colspan="2" class="RightDashBoardHeader" ><fmtPendingRtnIhItemReport:message key="LBL_PENDING_RETURN_IH_ITEM_LOANERS"/></td>
			<td  class="RightDashBoardHeader" align="right" colspan=""></td>
		    <td align="right" class=RightDashBoardHeader>
				<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("PENDING_RETURN_INHOUSE_LOANER")%>');" />
			</td> 
		</tr>
		<tr><td class="Line" height="1" colspan="7"></td></tr>	
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr class="evenshade">
			<td class="RightRedCaption" height="25" align="Right" width=15%>&nbsp;<fmtPendingRtnIhItemReport:message key="LBL_EVENT_NAME"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="eventName"  SFFormName='frmIHLoanerItem' SFSeletedValue="eventName"  defaultValue= "[Choose One]"	
				SFValue="alEventName" codeId="CID" codeName="ENAME"/>
			</td>
			<td class="RightRedCaption" height="25" align="Right" width=15%>&nbsp;<fmtPendingRtnIhItemReport:message key="LBL_PURPOSE"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="eventPurpose"  SFFormName='frmIHLoanerItem' SFSeletedValue="eventPurpose"  defaultValue= "[Choose One]"	
				SFValue="alPurEvent" codeId="CODEID" codeName="CODENM"/>
			</td>			
		</tr>
		<tr><td class="LLine" height="1" colspan="7"></td></tr>	
		<tr class="oddshade">
		<td class="RightRedCaption" height="25" align="Right" >&nbsp;<fmtPendingRtnIhItemReport:message key="LBL_LOAN_TO"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="eventLoanToName"  SFFormName='frmIHLoanerItem' SFSeletedValue="eventLoanToName"  defaultValue= "[Choose One]"	
				SFValue="alLoanTo" codeId="CODEID" codeName="CODENM" onChange="fnLoanTo(this);"/></td>
			<td class="RightRedCaption" height="25" align="Right">&nbsp;<fmtPendingRtnIhItemReport:message key="LBL_LOAN_TO_NAME"/>:</td>
			<%if(eventLoanToName.equals("19518")){%>						
			<td>&nbsp;<gmjsp:dropdown controlName="loanToName"  SFFormName='frmIHLoanerItem' SFSeletedValue="loanToName"  defaultValue= "[Choose One]"	
				SFValue="alRepList" codeId="ID" codeName="NAME" disabled="<%=strDisableDpdwn%>" /></td>
				<%}else if(eventLoanToName.equals("19523") ){%>
				<td>&nbsp;<gmjsp:dropdown controlName="loanToName"  SFFormName='frmIHLoanerItem' SFSeletedValue="loanToName"  defaultValue= "[Choose One]"	
				SFValue="alOUSList" codeId="ID" codeName="NAME" disabled="<%=strDisableDpdwn%>" /></td>
			<%}else{%>
			<td>&nbsp;<gmjsp:dropdown controlName="loanToName"  SFFormName='frmIHLoanerItem' SFSeletedValue="loanToName"  defaultValue= "[Choose One]"	
				SFValue="alEmpList" codeId="ID" codeName="NAME" disabled="<%=strDisableDpdwn%>"/></td>
			<%}%>			
		</tr>	
		<tr><td class="LLine" height="1" colspan="7"></td></tr>	
		<tr class="evenshade">
			<td class="RightRedCaption" height="25" align="Right" >&nbsp;<fmtPendingRtnIhItemReport:message key="LBL_TRANSACTION_ID"/>:</td>
			<td align="left" >&nbsp;<html:text maxlength="50" property="transactionID" size="25"   
							styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>
			<td class="RightRedCaption" height="25" align="Right" >&nbsp;<fmtPendingRtnIhItemReport:message key="LBL_TYPE"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="loanType"  SFFormName='frmIHLoanerItem' SFSeletedValue="loanType"  defaultValue= "[Choose One]"	
				SFValue="alType" codeId="CODEID" codeName="CODENM"/></td>					
		</tr>	
		<tr><td class="LLine" height="1" colspan="7"></td></tr>	
		<tr class="oddshade">		
			<td HEIGHT="30" class="RightTableCaption" align="right">&nbsp;<fmtPendingRtnIhItemReport:message key="LBL_DATE_RANGE"/>:</td>
			<td class="RightText"><fmtPendingRtnIhItemReport:message key="LBL_TYPE"/>:&nbsp;<select name="dtType">
									 <option value='0' ><fmtPendingRtnIhItemReport:message key="LBL_CHOOSEONE"/>								 					
									 <option value='ESD'><fmtPendingRtnIhItemReport:message key="LBL_EVENTSTARTDATE"/>
									 <option value='EED'><fmtPendingRtnIhItemReport:message key="LBL_EVENTENDDATE"/>
									 </select>&nbsp;<BR><BR><fmtPendingRtnIhItemReport:message key="LBL_FROM"/>:
			<gmjsp:calendar SFFormName="frmIHLoanerItem" controlName="eventStartDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtPendingRtnIhItemReport:message key="LBL_TO"/>:&nbsp;<gmjsp:calendar SFFormName="frmIHLoanerItem" controlName="eventEndDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" /> </td>
			<td class="RightRedCaption" height="25" align="Right" >&nbsp;<fmtPendingRtnIhItemReport:message key="LBL_PART_NUMBER"/>:</td>
			<td>
			  <table border="0" cellspacing="0" cellpadding="0">
			     <tr>
			       <td align="left">&nbsp;<html:text maxlength="50" property="partNumer" size="25"   
				  styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
			      </td>
			      <fmtPendingRtnIhItemReport:message key="BTN_LOAD" var="varLoad"/>
			      <td width="35%" ><gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" buttonType="Load" onClick="javascript:fnLoad();"></gmjsp:button></td> 
			    </tr>
			  </table>
			</td>  
		</tr>	
		<tr><td class="LLine" height="1" colspan="7"></td></tr>	
			<%if( xmlGridData.indexOf("cell") != -1){%>
				<tr>
					<td colspan="7"><div  id="pendshipitemdata" style="" height="500px"></div></td>
				</tr>						
			<%}else if(!xmlGridData.equals("")){%>
				<tr><td colspan="7" align="center" class="RightText"><fmtPendingRtnIhItemReport:message key="LBL_NOTHINGFOUNDTODISPLAY"/></td></tr>
			<%}else{%>
				<tr><td colspan="7" align="center" class="RightText"><fmtPendingRtnIhItemReport:message key="LBL_NODATAAVAILABLE"/></td></tr>
			<%} %>
		<tr><td height="1" colspan="7" class="LLine"></td></tr>
		<%if( xmlGridData.indexOf("cell") != -1) {%>
		<tr>
			<td colspan="7" align="center">
			    <div class='exportlinks'><fmtPendingRtnIhItemReport:message key="LBL_EXPORT_OPTIONS"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="fnDownloadXLS();"> <fmtPendingRtnIhItemReport:message key="LBL_EXCEL"/> </a></div>
			</td>
		</tr>
		<%}%>			
</table>	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

