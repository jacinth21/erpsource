<%
/**********************************************************************************
 * File		 		: GmViewLoanerUsageHistory.jsp
 * Desc		 		: This screen is used to display Loaner Usage details 
 * Version	 		: 1.0
 * author			: Joe
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtViewLoanerUsageHistory" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmViewLoanerUsageHistory.jsp -->

<fmtViewLoanerUsageHistory:setLocale value="<%=strLocale%>"/>
<fmtViewLoanerUsageHistory:setBundle basename="properties.labels.operations.logistics.GmViewLoanerUsageHistory"/>


<HTML>
<HEAD>
<TITLE> Globus Medical: Loaner Usage History </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script>
 function fnViewSummary(val, consignID)
{ 
		if(val=='Y'||val=='y'){
			document.frmViewLoaner.hAction.value = 'EditLoad';
			document.frmViewLoaner.Cbo_Action.value = 'VIEWLESUMMARY';
			document.frmViewLoaner.hConsignId.value = consignID;
			document.frmViewLoaner.action = '<%=strServletPath%>/GmInHouseSetServlet';		
			document.frmViewLoaner.submit();
		}
}
</script>
</head>
<BODY leftmargin="20" topmargin="10">
<FORM name="frmViewLoaner">
<input type="hidden" name="hAction" value ="">
<input type="hidden" name="Cbo_Action" value ="">
<input type="hidden" name="hConsignId" value ="">  
	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="2"><fmtViewLoanerUsageHistory:message key="LBL_LOANER_DETAILS_VIEW_HISTORY"/></td>
		</tr>
		<tr>
			<td colspan="2">
			<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
				<jsp:include page="/operations/logistics/GmIncludeLoanerUsageDetails.jsp"/>
				
			</td>
		</tr>
	</table>
		<table border="0"  cellspacing="0" cellpadding="0">
		<tr>
			<td align="center" height="30">
			<fmtViewLoanerUsageHistory:message key="BTN_CLOSE" var="varClose"/>
				<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close();" buttonType="Load" />&nbsp;
			</td>
		</tr>
	</table>
</FORM>
</BODY>
</HTML>
