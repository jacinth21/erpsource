<%
/**********************************************************************************
 * File		 		: GmInHouseSetReturnProcessing.jsp
 * Desc		 		: This screen is used to return and reconcile In-House Sets
 * Version	 		: 1.0
 * author			: James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtInHouseSetReturnProcessing" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmInHouseSetReturnProcessing.jsp -->

<fmtInHouseSetReturnProcessing:setLocale value="<%=strLocale%>"/>
<fmtInHouseSetReturnProcessing:setBundle basename="properties.labels.operations.logistics.GmInHouseSetReturnProcessing"/>

<%
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
GmServlet gm = new GmServlet();
gm.checkSession(response,session);

HashMap hmReturn = new HashMap();
hmReturn = (HashMap)request.getAttribute("hmReturn");
ArrayList alSetLoad = (ArrayList)request.getAttribute("CONSIGNSETDETAILS"); 
ArrayList alBioList = (ArrayList)request.getAttribute("ALBIOLIST");
ArrayList alList = (ArrayList)request.getAttribute("ALDAMAGELIST");
HashMap hmCsg = new HashMap();
// To get the Consignment information 
hmCsg = (HashMap)request.getAttribute("hmCONSIGNMENT");
if (hmCsg == null)
{
	hmCsg = (HashMap)session.getAttribute("hmCONSIGNMENT");
}
String strType = GmCommonClass.parseNull((String)hmCsg.get("CTYPE"));
String strloansflcd = GmCommonClass.parseNull((String)hmCsg.get("LOANSFLCD"));
String strConsignId = GmCommonClass.parseNull((String)hmCsg.get("CONSIGNID"));
log.debug("strloansflcd========== "+strloansflcd);
String strSetID = GmCommonClass.parseNull(request.getParameter("hSetId"));
String strButtonNm="";
String strFnName="";
String strDisabled = "";
String strHeader = GmCommonClass.parseNull((String)request.getAttribute("header"));
String strCboOpt = GmCommonClass.parseNull((String)request.getAttribute("COMBO_OPT"));
String strReconfigloaner = GmCommonClass.parseNull((String)request.getAttribute("RECONFIGFL"));
if (strloansflcd.equals("25")) {// && strType.equals("4127")){
	strButtonNm="Release For Process";
}else {
	strButtonNm="Submit";
	strDisabled = "true";
}

HashMap hmTemp = new HashMap();	

int intSize = 0;
HashMap hcboVal = null;
String strShipTo = "";
String strSelected = "";
String strCodeID = "";
String strChecked = "";
String strTodaysDate = (String)session.getAttribute("strSessTodaysDate")==null?"":(String)session.getAttribute("strSessTodaysDate");
String strApplnDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
%>
<html:html>
<HEAD>
<TITLE> Globus Medical: Loaner Lot Reprocess </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmLotReprocess.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script>
var prevtr = 0;
var type = '<%=strType%>';
var lnSts = '<%=strloansflcd%>';
var setid ='<%=strSetID%>';	
var val = '<%=strConsignId%>';
var consId= '<%=strConsignId%>';
var cbo_type = '<%=strCboOpt%>';
var cbo_alBioList = '<%=alBioList%>';
var todaysDate = '<%=strTodaysDate%>';
var format = '<%=strApplnDateFmt%>';
var reconfigLoanFl ='<%=strReconfigloaner%>';
</script>
</head>
<BODY leftmargin="20" topmargin="10">
<html:form action="/gmLoanerLotReprocess.do">

<input type="hidden" name="method" value="loadPartDescription"/>
<input type="hidden" name="rollbackFl" id="rollbackFl" value=""/>
<input type="hidden" name="hSetId" id="hSetId" value="<%=strSetID%>"/><!-- PMT-52000 - Set the set id in hidden filed -->
	<table border="0" class="DtTable1000"   cellspacing="0" cellpadding="0">
		<tr><td colspan="3" height="1" class="Line"></td></tr>
		<tr>
			<td height="25" class="RightDashBoardHeader" ><%=strHeader%></td>
			<td height="25" class="RightDashBoardHeader" align="Right">
			<fmtInHouseSetReturnProcessing:message key="IMG_ALT_HELP" var = "varHelp"/>
			<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%//=strWikiTitle%>');" /></td>
		</tr>
		<tr>
			<td colspan="2">	
				<jsp:include page="/include/GmIncludeLoaner.jsp" >
				<jsp:param name="strSetID" value="<%=strSetID %>" />
				<jsp:param name="strConsignId" value="<%=strConsignId %>" />
				<jsp:param name="PARENTJSP" value="ProcessCheck"/>
				</jsp:include>
			</td>
		</tr>
		<tr><td colspan="2" height="2" bgcolor="#cccccc" ></td></tr>	
		<tr class="shade">
			<td colspan="2" class="RightDashBoardHeader" HEIGHT="20" align="left"><b><fmtInHouseSetReturnProcessing:message
								key="LBL_CONTROL" /></b></td>
		</tr>
		<tr>
			<td colspan="2">	
				<jsp:include page="/include/GmIncludeLoanerLotReprocess.jsp" >
				<jsp:param name="strSetID" value="<%=strSetID %>" />
				<jsp:param name="strConsignId" value="<%=strConsignId %>" />
				<jsp:param name="COMBO_OPT" value="<%=strCboOpt%>"/>
		                <jsp:param  name="STR_IMG_PATH" value="<%=strImagePath%>"/>
				</jsp:include>
			</td>
		</tr>
		<tr><td colspan="3" height="1" class="Line"></td></tr>
		<%if(!strCboOpt.equals("LOTRECONFIG")){ %>		
			<tr><td colspan="2">
			<jsp:include page="/include/GmIncludeLoanerOpenTrans.jsp">
			<jsp:param name="strConsignId" value="<%=strConsignId %>" />
			<jsp:param name="LogType" value="YES" />
			</jsp:include>	
			</td></tr>
	
			<%--  <% if (strloansflcd.equals("25")){ // pending check%>	 --%>
			<tr><td height="1" bgcolor="#cccccc" colspan="2"></td></tr>
			  <tr>
	             <td colspan="2">
	               	<jsp:include page="/include/GmIncludeLoanerCharges.jsp">
						<jsp:param name="strConsignId" value="<%=strConsignId %>" />
						<jsp:param name="alList" value="alReturn" />
						<jsp:param name="FORMNAME" value="frmloanerLotReprocess" />
					</jsp:include>	
				</td>
	        </tr>  
	      <%--   <%	}%>	  --%>
        <%  }%>
         <tr><td colspan="2" height="1" bgcolor="#cccccc" ></td></tr>			
         <tr>
             <td colspan="2">
               	<jsp:include page="/common/GmIncludeLog.jsp" >
					<jsp:param name="LogType" value="" />
					<jsp:param name="LogMode" value="Edit" />					
				</jsp:include>
			</td>
        </tr> 
        <tr><td colspan="2" height="1" bgcolor="#cccccc" ></td></tr>			
		<tr>
			<td colspan="2" class="aligncenter" align="center" height="30" valign="middle">	
			<fmtInHouseSetReturnProcessing:message key="LBL_RELEASE_FOR_PROCESS" var="varRelease"/>		
				<gmjsp:button value="<%=strButtonNm%>" gmClass="button" onClick="fnLoanerRcfSubmit();" controlId="SubmitButton" disabled="<%=strDisabled%>" tabindex="25" buttonType="Save" />&nbsp;
				
				<%if(!strCboOpt.equals("LOTRECONFIG")){ %>
					<fmtInHouseSetReturnProcessing:message key="BTN_CHECK_RULES" var="varCheckRules"/>
					<gmjsp:button value="&nbsp;${varCheckRules}&nbsp;" gmClass="button" onClick="fnLoadRules();" tabindex="26" buttonType="Save" />&nbsp;
					<fmtInHouseSetReturnProcessing:message key="BTN_PRINT_PAPERWORK" var="varPrintPaperWork"/>
					<gmjsp:button value="${varPrintPaperWork}" gmClass="button" onClick="fnPrint('');" tabindex="27" buttonType="Load" />&nbsp;
					<fmtInHouseSetReturnProcessing:message key="BTN_PRINT_PAPERWORK_MANUALLY" var="varPrintPaperWorkManually"/>
					<gmjsp:button value="${varPrintPaperWorkManually}" gmClass="button" onClick="fnPrint('manually');" tabindex="28" buttonType="Load" />
				<%  }%>
							
			</td>
		</tr>
		<tr><td colspan="2"><span id="successMsg" style="color: green;font-size: 15px;font-weight: bold;margin-left: 315px;"></span></td></tr>
			 <tr><td colspan="2" height="1" bgcolor="#cccccc" ></td></tr>			
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</html:html>
