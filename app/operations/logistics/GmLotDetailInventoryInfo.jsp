<!-- operations\logistics\GmLotDetailInventoryInfo.jsp -->
<%
	/**********************************************************************************
	  * File		 		: GmLotDetailInventoryInfo.jsp
      * Desc		 		: This screen is used to display inventory information for Inhouse
      * author			    : Agilan Singaravel
	 ************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@page import="java.util.Date"%> 
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%> 
<%@ page import = "java.util.*,java.io.*"%>
<%@ taglib prefix="fmtLotDetailRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtLotDetailRpt:setLocale value="<%=strLocale%>"/>
<fmtLotDetailRpt:setBundle basename="properties.labels.operations.logistics.GmLotDetailReport"/>

		<tr id="dataGridInv">
			<td  height="25" class="ShadeRightTableCaption" colspan="6">&nbsp;<fmtLotDetailRpt:message key="LOT_INVENTORY_INFO"/> </td>
		</tr>
        <tr class="lineHide"><td colspan="6" height="1" bgcolor="#000000"></td></tr>
	 
 		<tr id="dataGridInhouse">
			<td  height="25" class="ShadeRightTableCaption" colspan="6">&nbsp;<fmtLotDetailRpt:message key="LOT_INHOUSE_INFO"/> </td>
		</tr>
        <tr class="lineHide"><td colspan="6" height="1" bgcolor="#000000"></td></tr>
		<tr class="evenshade"> 
			<td colspan="6"><div id="dataGridDivforInhouseInfo" style="width:100%;"></div></td>
		</tr>
		<tr height="30" id="DivInhouseNothingMessage">
				<td colspan="6" align="center" class="RightTextBlue"><div><fmtLotDetailRpt:message key="LBL_NO_DATA" />
					</div></td>
		</tr>  
