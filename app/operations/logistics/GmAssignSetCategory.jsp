<%
/**********************************************************************************
 * File		 		: GmPrintPrice.jsp
 * Desc		 		: This screen is used to display Order Summary
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %> 
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@include file="/common/GmHeader.inc" %>
<%@ page buffer="16kb" autoFlush="true" %>
<%@ taglib prefix="fmtAssignSetCategory" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmAssignSetCategory.jsp -->

<fmtAssignSetCategory:setLocale value="<%=strLocale%>"/>
<fmtAssignSetCategory:setBundle basename="properties.labels.operations.logistics.GmAssignSetCategory"/>

<%
response.setHeader("Cache-Control", "no-cache, post-check=0, pre-check=0"); //HTTP 1.1
response.setHeader("Pragma", "no-cache");  //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server 
%>

<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
 
	String strInclude = GmCommonClass.getString("GMINCLUDE");
	 
	String strOpt = (String)session.getAttribute("strSessOpt") == null?"":(String)session.getAttribute("strSessOpt");
	String strPurpose = GmCommonClass.parseNull((String)request.getAttribute("STRPURPOSE"));
	String strConsignId = GmCommonClass.parseNull((String)request.getAttribute("CONSIGNID"));
	ArrayList alPurpose = (ArrayList)request.getAttribute("PURPOSE");
 
	HashMap hmConsignment = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmCONSIGNMENT")) ;
	 
	String strSetCategory = GmCommonClass.parseNull((String)hmConsignment.get("SETPURPOSE")); 
	
	%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Consignment Set Details </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language = "javascript">
function fnSubmit()
{
document.frmAssignSetCategory.hAction.value="Update";
fnStartProgress('Y');
document.frmAssignSetCategory.submit();
}
</script>
</head>
<body topmargin="10" leftmargin="20">
<xml src="<%=strXmlPath%>/<%=strOpt%>.xml" id="xmldso" async="false"></xml>
<FORM name="frmAssignSetCategory" method="POST" action="<%=strServletPath%>/GmAssignSetCategoryServlet">
<input type="hidden" name="hAction" value="">
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<%if (strOpt.equals("4127")){%>
				<td height="25" colspan="2" class="RightDashBoardHeader"><fmtAssignSetCategory:message key="LBL_LOANERS_ETCH_ID"/></td>
			<%}else if (strOpt.equals("4119")){ %>
				<td height="25" colspan="2" class="RightDashBoardHeader"><fmtAssignSetCategory:message key="LBL_IN_HOUSE_CATEGORY_ETCH_ID"/></td>
			<%} %>			
		</tr>
		<tr><td bgcolor="#666666" colspan="2" height="1"></td></tr>
		<tr>
			<td colspan="2">
				<jsp:include page="/include/GmIncludeConsignment.jsp" >
				<jsp:param name="ETCHSTATUS" value="edit" />
				</jsp:include>
			</td>
		</tr>
<%
	if (strOpt.equals("4119")){
%>		
		<!-- Custom tag lib code modified for JBOSS migration changes --> 
		
		<tr class="shade">
			<td class="RightTableCaption"   align="right">&nbsp;<fmtAssignSetCategory:message key="LBL_NEW_CATEGORY"/>:</td>
			<td width="375">&nbsp;<gmjsp:dropdown controlName="Cbo_Purpose"  seletedValue="<%= strSetCategory %>" 	
			tabIndex="1"  width="150" value="<%= alPurpose%>" codeId = "CODEID" codeName = "CODENM" defaultValue= "[Choose One]"  />
			</td>		
		</tr>
 
		 
<%
	}
%>		
		<tr>
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
			<td colspan="2">	
				<jsp:include page="/include/GmIncludeConsignSetDetails.jsp" />
			</td>
		</tr>
		<tr>
			<td colspan="2">	
				<jsp:include page="/common/GmIncludeLog.jsp" >
				<jsp:param name="LogType" value="" />
				<jsp:param name="LogMode" value="Edit" />
				</jsp:include>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="aligncenter">
			<fmtAssignSetCategory:message key="BTN_SUBMIT" var="varSubmit"/>	
				<gmjsp:button value="${varSubmit}" gmClass="button" onClick="fnSubmit();" tabindex="25" buttonType="Save" />&nbsp;<BR><BR>
			</td>
		</tr>
	</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</body>
</HTML>
