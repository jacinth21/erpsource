<%@page import="java.util.HashMap"%>
<%@page import="com.globus.common.beans.GmCommonControls"%>
<%
/**********************************************************************************
 * File		 		: GmIHItemRequestRpt.jsp
 * Version	 		: 1.0
 * author			: Jignesh Shah
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtIHItemRequestRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- operations\logistics\GmIHItemRequestRpt.jsp -->

<fmtIHItemRequestRpt:setLocale value="<%=strLocale%>"/>
<fmtIHItemRequestRpt:setBundle basename="properties.labels.operations.logistics.GmIHItemRequestRpt"/>

<bean:define id="xmlGridData" name="frmIHItemRequest" property="xmlGridData" type="java.lang.String"></bean:define>
<bean:define id="alStatus" name="frmIHItemRequest" property="alStatus" type="java.util.ArrayList"></bean:define>
<%
String strLogisticsJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_LOGISTICS");
String strSessApplDateFmt = strGCompDateFmt;

HashMap hmMapStatus = new HashMap();
hmMapStatus.put("ID","");
hmMapStatus.put("PID","CODENMALT");
hmMapStatus.put("NM","CODENM");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Inhouse Req Report  </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
.rowRev{
	color: blue;
	font-weight: bold;
}
</style>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strLogisticsJsPath%>/GmIHItemRequestRpt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script type="text/javascript">
var objGridData = '<%=xmlGridData%>';
var format = '<%=strSessApplDateFmt%>';
</script>
</HEAD>
<BODY leftmargin="20" topmargin="20" onload="fnOnPageLoad();" onkeyup= "fnEnter();">
<html:form  action="/gmIHItemRequest.do">
<html:hidden property="strOpt" name="frmIHItemRequest"/>
<html:hidden property="requestID" name="frmIHItemRequest"/>
<html:hidden property="requestTypeID" name="frmIHItemRequest"/>
<html:hidden property="hStatus" name="frmIHItemRequest"/>
	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		
		<tr>
			<td height="30" colspan="2" class="RightDashBoardHeader" ><fmtIHItemRequestRpt:message key="LBL_INHOUSE_REQ_REPORT"/></td>
			<td class="RightDashBoardHeader" align="right" colspan="4"></td>
			<td align="right" class=RightDashBoardHeader>
			<fmtIHItemRequestRpt:message key="IMG_ALT_HELP" var = "varHelp"/>
				<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("INHOUSE_REQ_REPORT")%>');" />
			</td> 
		</tr>
		<tr><td class="LLine" height="1" colspan="7"></td></tr>
		<tr class="evenshade">
			<td class="RightRedCaption" align="Right" height="25"><fmtIHItemRequestRpt:message key="LBL_REQUEST_TYPE"/>:</td>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
			<td class="RightRedCaption" height="25" colspan="3">
			&nbsp;<gmjsp:dropdown controlName="reqtype"  SFFormName='frmIHItemRequest' SFSeletedValue="reqtype"  defaultValue= "[Choose One]"	
					width="200" SFValue="alReqtype" codeId="CODENMALT" codeName="CODENM" onChange="fnChange()"/></td>
			<td class="RightRedCaption" align="Right"><fmtIHItemRequestRpt:message key="LBL_PURPOSE"/>:</td>
			<td Width="220">&nbsp;<gmjsp:dropdown controlName="purpose"  SFFormName='frmIHItemRequest' SFSeletedValue="purpose"  defaultValue= "[Choose One]"	
					width="200" SFValue="alPurpose" codeId="CODEID" codeName="CODENM" onChange="fnChange()"/></td>
		</tr>
		<tr><td class="LLine" height="1" colspan="7"></td></tr>
		<tr class="oddshade">
			<td class="RightRedCaption" align="Right" height="25" width="120">&nbsp;<fmtIHItemRequestRpt:message key="LBL_FROM_DATE"/>:</td>
			<td width="120">&nbsp;<gmjsp:calendar SFFormName="frmIHItemRequest" controlName="fromDt"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
			</td>
			<td class="RightRedCaption" align="Right" width="50"><fmtIHItemRequestRpt:message key="LBL_TO_DATE"/>:</td>
			<td width="120">&nbsp;<gmjsp:calendar SFFormName="frmIHItemRequest" controlName="toDt" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
			</td>
			<td class="RightRedCaption" align="Right"><fmtIHItemRequestRpt:message key="LBL_STATUS"/>:</td>
			<td Width="220" style="padding-left: 5px;padding-top: 5px;">
					<%=GmCommonControls.getChkBoxGroup("",alStatus,"status",hmMapStatus) %>
			</td>
			<fmtIHItemRequestRpt:message key="BTN_LOAD" var="varLoad"/>
			<td id="hideLoadBtn">&nbsp;<gmjsp:button value="&nbsp;${varLoad}&nbsp;"  name="Btn_Submit" gmClass="button" buttonType="Load" onClick=" fnLoad();"/></td>
		</tr>
			<tr id="hideContId" align="center"><td class="LLine" height="1" colspan="7"></td></tr>
		<tr id="hideTagContId" class="evenshade">
			<td  class="RightRedCaption" align="Right" height="25" width="120">&nbsp;<fmtIHItemRequestRpt:message key="LBL_TAG_ID"/>:</td>
			<td colspan="2" class="RightRedCaption" align="left">&nbsp;
			<html:text styleId="DisableTagId" property="strTagID"  size="15"  onfocus="javascript:fnToggleTag()" styleClass="InputArea" onblur="javascript:fnValidateTag(this.value)"/>
				 <span id="DivShowTagAvl" style="vertical-align:middle; display: none;"> <img height="16" width="19" src="<%=strImagePath%>/success.gif"></img></span>
				 <span id="DivShowTagNotExists" style="vertical-align:middle;display: none;"> <fmtIHItemRequestRpt:message key="LBL_INVALID_ID" var="varInvalidId"/><img  title="${varInvalidId}" height="12" width="12" src="<%=strImagePath%>/delete.gif"></img>
						 &nbsp;<%-- <img height="15" onclick="javascript:fnTagLookup()" border="0" title="" src="<%=strImagePath%>/location.png"></img> --%>
				 </span>
			</td>
			<td colspan="3"></td>
			<fmtIHItemRequestRpt:message key="BTN_LOAD" var="varLoad"/>
			<td align="center">&nbsp;<gmjsp:button value="&nbsp;${varLoad}&nbsp;"  name="Btn_Submit" gmClass="button" buttonType="Load" onClick=" fnLoad();"/></td>
		</tr>
		<tr><td class="LLine" height="1" colspan="7"></td></tr>
	  		<%if(xmlGridData.indexOf("cell") != -1){%>
		<tr>
	    	<td colspan="7" align="center">
				<div id="GridData" style="grid" height="450px" width="850px"></div>
		   	</td>	
		</tr>
			<%}else if(!xmlGridData.equals("")){%>
		<tr>
			<td colspan="7" align="center" class="RightText"><fmtIHItemRequestRpt:message key="MSG_NO_DATA"/>
			</td>
		</tr>
		<%}else{%>
		<tr>
			<td colspan="7" align="center" class="RightText"><fmtIHItemRequestRpt:message key="MSG_NO_DATA"/></td>
		</tr>
		<%} %>
		<tr>
			<td height="1" colspan="7" class="LLine"></td>
		</tr>
		<%if(xmlGridData.indexOf("cell") != -1) {%>
		<tr>
			<td colspan="7" align="left">
			    <div class='exportlinks'><fmtIHItemRequestRpt:message key="DIV_EXPORT_OPT"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="fnDownloadXLS();"> <fmtIHItemRequestRpt:message key="DIV_EXCEL"/> </a></div>
			</td>
		</tr>
		<tr>
			<td colspan="8" class="RightText" align="Center">
				<BR><b><fmtIHItemRequestRpt:message key="LBL_CHOOSE_ACTION"/></b>&nbsp;<gmjsp:dropdown controlName="chooseAction"  SFFormName='frmIHItemRequest' SFSeletedValue="chooseAction"  defaultValue= "[Choose One]"	
					SFValue="alChooseAction" codeId="CODEID" codeName="CODENM"/>&nbsp;&nbsp;&nbsp;
				<fmtIHItemRequestRpt:message key="BTN_SUBMIT" var="varSubmit"/>
				<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="fnSubmit();" tabindex="25" buttonType="Save" />&nbsp;<BR><BR>
			</td>
		</tr>	
		<% } %>	
	</table>	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>