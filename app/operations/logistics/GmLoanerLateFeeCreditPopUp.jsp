<%
/**********************************************************************************
 * File		 		: GmLoanerLateFeeCreditPopUp.jsp
 * Desc		 		: Loaner late fee credit screen
 * Version	 		: 1.0
 * author			: Sindhu
************************************************************************************/
%>

<!DOCTYPE html>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.Locale"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.util.GmImageUtil"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Iterator"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>	
<%@ taglib prefix="fmtLoanerLateFeePopup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtLoanerLateFeePopup:setLocale value="<%=strLocale%>"/>
<fmtLoanerLateFeePopup:setBundle basename="properties.labels.operations.logistics.GmLoanerLateFeeApproval"/>


<bean:define id="incidentId" name="frmLateFeeCharges" property="incidentId" type="java.lang.String"></bean:define>
<bean:define id="chargeFromDate" name="frmLateFeeCharges" property="chargeFromDate" type="java.lang.String"></bean:define>
<bean:define id="chargeToDate" name="frmLateFeeCharges" property="chargeToDate" type="java.lang.String"></bean:define>
<bean:define id="creditAmount" name="frmLateFeeCharges" property="creditAmount" type="java.lang.String"></bean:define>
<bean:define id="chargeDetailId" name="frmLateFeeCharges" property="chargeDetailId" type="java.lang.String"></bean:define>
<bean:define id="chargeFromDate" name="frmLateFeeCharges" property="chargeFromDate" type="java.lang.String"></bean:define>
<bean:define id="chargeToDate" name="frmLateFeeCharges" property="chargeToDate" type="java.lang.String"></bean:define>
<bean:define id="salesRep" name="frmLateFeeCharges" property="salesRep" type="java.lang.String"></bean:define>
<bean:define id="searchsalesRep" name="frmLateFeeCharges" property="searchsalesRep" type="java.lang.String"></bean:define>  
<bean:define id="fieldSales" name="frmLateFeeCharges" property="fieldSales" type="java.lang.String"></bean:define>
<bean:define id="searchfieldSales" name="frmLateFeeCharges" property="searchfieldSales" type="java.lang.String"></bean:define>  
<bean:define id="accessfl" name="frmLateFeeCharges" property="accessfl" type="java.lang.String"></bean:define>
<bean:define id="statuslist" name="frmLateFeeCharges" property="alStatustListRpt" type="java.util.List"> </bean:define>
<bean:define id="shwDtl" name="frmLateFeeCharges" property="shwDtl" type="java.lang.String"> </bean:define>
<bean:define id="strAvailAmount" name="frmLateFeeCharges" property="strAvailAmount" type="java.lang.String"></bean:define>
<bean:define id="amount" name="frmLateFeeCharges" property="amount" type="java.lang.String"></bean:define>
<%

String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
String strWikiTitle = GmCommonClass.getWikiTitle("LOANER_LATE_FEE_CREDIT");
String strDtTablewidth = "1600";

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);	
	String strTodaysDate = (String)session.getAttribute("strSessTodaysDate")==null?"":(String)session.getAttribute("strSessTodaysDate");
	String strApplnDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
 
	%>

<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE> Globus Medical: <fmtLoanerLateFeePopup:message key="LBL_CREDIT_LOANER"/> </TITLE>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/jquery-latest.js"></script>
<script language="javascript" src="<%=strOperationsJsPath%>/GmLoanerLateFeeApproval.js"></script>
<style type="text/css">
#creditAmt {
 color:red;
}

</style>
<script>
var todaysDate = '<%=strTodaysDate%>';
var format = '<%=strApplnDateFmt%>';
</script>
</head>
<body leftmargin="20" topmargin="10" style="padding-left: 2px;">
<html:form action="/gmLoanerLateFeeAction.do"> 
<html:hidden property="chargeDetailId" value="<%=chargeDetailId %>"/>
<html:hidden property="strAvailAmount" value="<%=strAvailAmount%>"/>
<html:hidden property="creditAmount" value="<%=creditAmount%>"/>

<table class="DtTable500" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" class="RightDashBoardHeader">&nbsp;<fmtLoanerLateFeePopup:message key="LBL_CREDIT_LOANER"/> 
			<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
		       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		    </td>
			</tr>	
			<tr>
			<td>
			<table style="border: 1px solid #cccccc">				
			<tr>
			   <td height="25" class="RightTableCaption" align="right">&nbsp;<fmtLoanerLateFeePopup:message key="LBL_INCIDENT_ID"/>:</td>
			   <td>&nbsp;<html:text  property="incidentId" value = "<%=incidentId%>" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /> </td>
			</tr>
			<tr><td  height="1" colspan="8" class="Line"></td></tr>
			<tr class="oddshade">
			   <td height="25" class="RightTableCaption" align="right">&nbsp;<fmtLoanerLateFeePopup:message key="LBL_CHARGE_FROM_DATE"/>:</td>
			   <td>&nbsp;<html:text  property="chargeFromDate" readonly="true" value = "<%=chargeFromDate%>" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /> </td>
			</tr>
			<tr><td  height="1" colspan="8" class="Line"></td></tr>
			<tr>
			   <td height="25" class="RightTableCaption" align="right">&nbsp;<fmtLoanerLateFeePopup:message key="LBL_CHARGE_TO_DATE"/>:</td>
			   <td >&nbsp;<html:text  property="chargeToDate" readonly="true" value = "<%=chargeToDate%>" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /> </td>
			</tr>
		 <tr><td  height="1" colspan="8" class="Line"></td></tr>
		    <tr class="oddshade">
			   <td height="25" class="RightTableCaption" align="right">&nbsp;<fmtLoanerLateFeePopup:message key="LBL_AMOUNT"/>:</td>
			   <td>&nbsp;<html:text property="amount" readonly="true" value = "<%=amount%>" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /> </td>
			</tr> 
			<tr><td  height="1" colspan="8" class="Line"></td></tr>
			<tr>
			   <tr><td height="25" class="RightTableCaption" align="right"><font color="red">*</font>&nbsp;<fmtLoanerLateFeePopup:message key="LBL_CREDIT_AMOUNT"/>:</td>
			   <td>&nbsp;<html:text  property="creditAmt" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /> </td>
			   </tr>
			   <% 
			    if(!creditAmount.equals("")){
			   if(Math.abs((Integer.parseInt(creditAmount)))>0){%>	
			   <tr>
			   <td height="25" class="RightTableCaption" align="right" >&nbsp;<font></font><fmtLoanerLateFeePopup:message key="LBL_PREVIOUS_CREDIT"/>:</td>
			   <td >&nbsp;<span id="creditAmt">(<%=Math.abs(Integer.parseInt(creditAmount))%>)</span></td>
			   </tr>
			   <%} 
			   }%>
			</tr>
			<tr><td  height="1" colspan="8" class="Line"></td></tr>	
			<tr class="oddshade" >		
			<td class="RightTableCaption" align="right"><font color="red">*</font>&nbsp;<fmtLoanerLateFeePopup:message key="LBL_CREDIT_DATE"/>:</td>
			<td  height="25" class="RightTableCaption" style="font-weight:normal">&nbsp;<gmjsp:calendar SFFormName="frmLateFeeCharges"  controlName="creditDate" gmClass="InputArea" 
					onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
			</tr>
			<tr><td  height="1" colspan="8" class="Line"></td></tr>
			<tr>
			   <td height="25" class="RightTableCaption" align="right" >&nbsp;<fmtLoanerLateFeePopup:message key="LBL_NOTES"/>:</td>
			   <td>&nbsp;<html:textarea  property="creditNotes"  cols="31" rows="3"	styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /> </td>
			</tr> 
			<tr><td  height="1" colspan="8" class="Line"></td></tr>
			<tr>
				<td align="center" colspan="2" height="40">
					<p>&nbsp;</p> <fmtLoanerLateFeePopup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" gmClass="button"
					name="Btn_Submit" style="width: 5em" buttonType="Save" onClick="fnSaveCredit(); " /> &nbsp;
					<fmtLoanerLateFeePopup:message key="BTN_CANCEL" var="varCancel"/><gmjsp:button value="${varCancel}" buttonType="Load" gmClass="button" name="Cancel"
					onClick="fnClose();" /></td></tr>
					</table>
					</td>
					</tr>
				
</table>
</html:form>
</body>
</html>