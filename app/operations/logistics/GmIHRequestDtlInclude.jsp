<%
/**********************************************************************************
 * File		 		: GmControlNumberCartInclude.jsp
 * Desc		 		: This screen is used to edit the control number details
                      used in O.R.
 * Version	 		: 1.0
 * author			: Venkata Prasath
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@page import="java.util.Date"%>

<%@ taglib prefix="fmtIHRequestDtlInclude" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmIHRequestDtlInclude.jsp -->

<fmtIHRequestDtlInclude:setLocale value="<%=strLocale%>"/>
<fmtIHRequestDtlInclude:setBundle basename="properties.labels.operations.logistics.GmIHRequestDtlInclude"/>

<bean:define id="alReplenish" name="frmIHItemRequest" property="alReplenish" type="java.util.ArrayList"></bean:define>

<%
	ArrayList alPartNmDetails = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("alPartNumDtl"));
	int partArraySize         = alPartNmDetails.size();
	String strPartNumber      = "";
	String strTempPartNumber  = "";
	String strOptionString 	  = "";		
		for(int x=0;x<partArraySize;x++){
			HashMap hmPartDetails = (HashMap)alPartNmDetails.get(x);
			strPartNumber = GmCommonClass.parseNull((String)hmPartDetails.get("PNUM"));		
			int arraySize = alReplenish.size();
			String strChooseOne = "<option value=\"0\" >[Choose One]</option>";
			strOptionString = "";
			for (int i=0; i<arraySize; i++){				
				HashMap hmloc = (HashMap)alReplenish.get(i);			
				String strCodeID   = GmCommonClass.parseNull((String)hmloc.get("CODEID"));
				String strCodeName = GmCommonClass.parseNull((String)hmloc.get("CODENM"));
				strOptionString += " <option value="+strCodeID+">"+strCodeName+"</option> ";					
				if(i >= (arraySize - 1)){					
				strOptionString = strChooseOne + strOptionString;
	%>
				<script>put('<%=strOptionString%>');</script>
	<%
				}
			}		
		}
%>
<table cellpadding="0" cellspacing="0" width="100%" border="1" bordercolor="gainsboro">
	<thead>
		<TR class="Shade" class="RightTableCaption" style="position:relative;">
			<TH class="RightText" width="130" align="center" height="25"><fmtIHRequestDtlInclude:message key="LBL_PART"/></TH>
			<TH class="RightText" width="400" align ="left">&nbsp;<fmtIHRequestDtlInclude:message key="LBL_PART_DESCRIPTION"/></TH>
			<TH class="RightText" width="100" align="center"><fmtIHRequestDtlInclude:message key="LBL_QTY"/> <br> <fmtfmtIHRequestDtlInclude:message key="LBL_PENDING"/></TH>
			<TH class="RightText" width="130" align="center"><fmtIHRequestDtlInclude:message key="LBL_REPLENISH_FORM"/></TH>
			<TH class="RightText" width="100" align="center"><fmtIHRequestDtlInclude:message key="LBL_BL_FG_IH"/> <br><fmtfmtIHRequestDtlInclude:message key="LBL_QTY"/> </TH>
			<TH class="RightText" width="100" align="center"><fmtIHRequestDtlInclude:message key="LBL_QTY_TO"/> <br> <fmtfmtIHRequestDtlInclude:message key="LBL_PROCESS"/></TH>
		</TR>	
	</thead>
	<TBODY>
	<%
		int intSize  = alPartNmDetails.size();

		if ( intSize == 0 )
		{
	%>
		<tr >
			<td  height="20" class="RightText" colspan = 6>
					<fmtIHRequestDtlInclude:message key="MSG_NO_DATA"/>
			</td>
		</tr>
	<%	
		}
		String strAllPnums = "";
		String strPreviousPnum = "";
		String strDisable = "";
		for (int i=0;i<intSize;i++)
		{
			HashMap hmPartNum =(HashMap)alPartNmDetails.get(i); 
			
			String strPartNumHidden = "";
			String strPartNum ="";
			String strPartNumID = "";
			String strDesc ="";
			String strQty = "";
			String strPrice = "";
			String strControlname = "";
			
			strPartNum =  GmCommonClass.parseNull((String)hmPartNum.get("PNUM"));
			strDesc =  GmCommonClass.parseNull((String)hmPartNum.get("PDESC"));
			strQty =  GmCommonClass.parseNull((String)hmPartNum.get("IQTY"));
			strPrice = GmCommonClass.parseNull((String)hmPartNum.get("IPRICE"));			
			String strfn = "javascript:fnGetPartQty('"+i+"',this,'"+strPartNum+"');";
			strControlname = "replenish"+i;
			strPartNumHidden = strPartNum;
			if(strQty.equals("0")){
				strDisable = "disabled";
			}else{				
				strDisable = "";
			}
			strPartNumID = strPartNum.replaceAll("\\.","");
			
			if (strPreviousPnum != strPartNum || i == (intSize-1)){
				strAllPnums += ","+strPartNumID;
				strPreviousPnum = strPartNum;
			}
			
			String strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
	%>
		<tr <%=strShade %>>
			<td  height="20" class="RightText">
				&nbsp;<a class="RightText" tabindex=-1 href="javascript:fnSplit('<%=i%>','<%=strPartNum%>','<%=strQty%>');"><%=strPartNum%></a>
			</td>
		   	<td id="CellRemove<%=i%>" class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%>
		   	</td>
			<td align="center" id="PendQty<%=i%>"><%=strQty%></td>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
			<td Width="130" align="center" id="Dropdown<%=i%>"><gmjsp:dropdown controlName="<%=strControlname %>" disabled="<%=strDisable%>" SFFormName='frmIHItemRequest' SFSeletedValue="replenish"  
				defaultValue= "[Choose One]" width="105" SFValue="alReplenish" codeId="CODEID" codeName="CODENM" onChange="<%=strfn%>"/></td>
			<td align="right" id="partqty<%=i%>"><span id="partNumQty<%=i%>"></span></td>
			<td align="center" id="Qty<%=i%>">
				<input type="text" size="2" id="Txt_Qty<%=i%>" name="Txt_Qty<%=i%>" value="" class="InputArea" <%=strDisable%> onFocus="changeBgColor(this,'#AACCE8');" 
					onBlur="changeBgColor(this,'#ffffff');" maxlength="5">
			</td>
			<input type="hidden" id="hPartNum<%=i%>" name="hPartNum<%=i%>" value="<%=strPartNumHidden%>">
			<input type="hidden" id="hQty<%=strPartNumID%>" name="hQty<%=strPartNumID%>" value="<%=strQty%>">
			<input type="hidden" id="hSplitQty<%=strPartNumID%>" name="hSplitQty<%=strPartNumID%>" value="<%=Integer.parseInt(strQty)-1%>">
			<input type="hidden" id="hReplenishQty<%=strPartNumID%>" name="hReplenishQty<%=strPartNumID%>" value="0">
		</tr>
	<%	} // For Loop ends here				
	%>
	<input type="hidden" id="hCnt" name="hCnt" value="<%=intSize-1%>">
	<input type="hidden" id="hNewCnt" name="hNewCnt" value="0"><%--This hidden field used to keep java script  var cnt value  --%>	
	<input type="hidden" id="hAllPnums" name="hAllPnums" value="<%=strAllPnums%>">
	</TBODY>		  
</table>
			