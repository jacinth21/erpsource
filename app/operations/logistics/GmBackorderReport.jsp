
<%@ page import ="com.globus.prodmgmnt.beans.GmProjectBean,com.globus.common.beans.GmCommonClass,com.globus.operations.beans.GmLogisticsBean"%>
<%@ page import="java.util.ArrayList,java.util.HashMap" %>
<%@ page import="org.apache.commons.beanutils.RowSetDynaClass"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ include file="/common/GmHeader.inc" %>
 
<%@ taglib prefix="fmtBackOrderReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
 <%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
}
%>
<!-- GmBackorderReport.jsp -->
<fmtBackOrderReport:setLocale value="<%=strLocale%>"/>
<fmtBackOrderReport:setBundle basename="properties.labels.custservice.ProcessRequest.GmProcessRequestItems"/>

<%	
try {
	String strRequestId = GmCommonClass.parseNull((String)request.getParameter("hRequestId"));	
	String strReportName = "";
	String strFormName = GmCommonClass.parseNull((String)request.getParameter("FORMNAME"));
	strReportName = !strFormName.equals("") ? "requestScope."+strFormName+".rdBackOrderDetails" :"requestScope.BACKORDERLIST";
    log.debug(" Report Name " + strReportName + " form name " + strFormName);
    ArrayList alBackOrderDetails = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("BACKORDERLIST"));
    boolean bShowDetails = true;
    
    if(strFormName.equals("") && alBackOrderDetails.size() == 0)
    {
    	bShowDetails = false;
    }
%>	
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/alternative.css");
</style>
<% if(bShowDetails) { %>
<table border=0 cellspacing=0 cellpadding=0 width="100%">
<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
	<tr>
			<td bgcolor="#666666" width="1" ></td>
			<td height="25" class="RightDashBoardHeader"><fmtBackOrderReport:message key="LBL_BACKORDERDETAILS"/> : <%=strRequestId%></td>
			<td bgcolor="#666666" width="1" ></td>
	</tr>
<tr>
		<td colspan="3" height="100" valign="top">
			<display:table cellspacing="0" cellpadding="0"  name="<%=strReportName%>" export="false"  class="its" id="currentRowObject"  >
			<fmtBackOrderReport:message key="LBL_PARTNUMBER" var="varPartNumber"/>
			<display:column property="PNUM" title="${varPartNumber}" sortable="true"  />
			<fmtBackOrderReport:message key="LBL_DESCRIPTION" var="varDescription"/>
	 		<display:column property="PDESC" title="${varDescription}" maxLength="40" />	
	 		<fmtBackOrderReport:message key="LBL_BACKORDERQTY" var="varBackordQty"/>				 		
			<display:column property="BOQTY" title="${varBackordQty}" style="text-align: center" />
			<fmtBackOrderReport:message key="LBL_BACKORDERREQID" var="varBackOrdReqId"/>
			<display:column property="BOREQID" title="${varBackOrdReqId}" />
			</display:table>						
		</td>
</tr>
	
</table>
 <%
}
}
catch(Exception e)
{
	e.printStackTrace();
}

%> 

