<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.operations.beans.GmOperationsBean"%>
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtConsignmentHeader" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtConsignmentHeader:setLocale value="<%=strLocale%>"/>
<fmtConsignmentHeader:setBundle basename="properties.labels.operations.logistics.GmConsignmentHeader"/>

<!-- operations\logistics\GmConsignmentHeader.jsp -->

<%


final String strConsignId = GmCommonClass.parseNull(request.getParameter("hConsignmentId"));


GmOperationsBean gmOper = new GmOperationsBean();
HashMap hmResult = gmOper.viewBuiltSetDetails(strConsignId);

if( hmResult == null )
	return ; 

final String strSetName= (String)hmResult.get("SNAME");
final String strSetId = (String)hmResult.get("SETID");
final String strAccName = (String)hmResult.get("ANAME");
final String strUserName = (String)hmResult.get("UNAME");
final String strIniDate = (String)hmResult.get("CDATE");
final String strDesc = (String)hmResult.get("COMMENTS");
final String strLastUpdNm = (String)hmResult.get("LUNAME");
final String strLastUpdDt = (String)hmResult.get("LUDATE");
final String strFlag = (String)hmResult.get("SFL");

%>

<tr>
<td class="RightTableCaption" HEIGHT="20" align="right" >&nbsp;<fmtConsignmentHeader:message key="LBL_CONSIGNMENT_ID"/>:</td>

<td colspan="3">&nbsp;    		        	
<input type="text" size="15" value="<%=strConsignId%>" name="consignmentID" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
<fmtConsignmentHeader:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}" gmClass="button"  onClick="fnLoadReconfig();" buttonType="Load" />&nbsp;&nbsp;
</td>					


</tr>
<tr class="shade">
<td class="RightTableCaption" HEIGHT="20" align="right" >&nbsp;<fmtConsignmentHeader:message key="LBL_SET_NAME"/>:</td>
<td class="RightText" colspan="3">&nbsp;<%=GmCommonClass.getStringWithTM(strSetName)%></td>
</tr>
<tr>
<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtConsignmentHeader:message key="LBL_INITIATED_BY"/>:</td>
<td class="RightText">&nbsp;<%=strUserName%></td>
<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtConsignmentHeader:message key="LBL_INITIATED_DATE"/>:</td>
<td class="RightText">&nbsp;<%=strIniDate%></td>
</tr>

<tr class="shade">
<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtConsignmentHeader:message key="LBL_LAST_UPDATED_BY"/>:</td>
<td class="RightText">&nbsp;<%=strLastUpdNm%></td>
<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtConsignmentHeader:message key="LBL_LAST_UPDATED_DATE"/>:</td>
<td class="RightText">&nbsp;<%=strLastUpdDt%></td>
</tr>
<%@ include file="/common/GmFooter.inc"%>



