<!-- operations\logistics\GmLotTrackTxnLogRpt.jsp -->
<%
	/**********************************************************************************
	  * File		 		: GmLotTrackTxnLogRpt.jsp
      * Desc		 		: This screen is used to display Lot Track Transactions log Report
      * author			    : Agilan Singaravel
	 ************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@page import="java.util.Date"%> 
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%> 
<%@ page import = "java.util.*,java.io.*"%>
<%@ taglib prefix="fmtLotTrackTxnLogRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtLotTrackTxnLogRpt:setLocale value="<%=strLocale%>"/>
<fmtLotTrackTxnLogRpt:setBundle basename="properties.labels.operations.logistics.GmLotTrackTxnLogRpt"/>

<%
String strLogisticsJsPath= GmCommonClass.parseNull(GmFilePathConfigurationBean.getFilePathConfig("JS_LOGISTICS"));
String strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("LOT_TRACK_TRANS_LOG_RPT"));
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Lot Track Transaction Log Report </TITLE>
<link rel="styleSheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css"> 
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strLogisticsJsPath%>/GmLotTrackTxnLogRpt.js"></script> 
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/jquery-latest.js"></script>
<script type="text/javascript">
var gCmpDateFmt = "<%=strGCompDateFmt%>";
</script>

<style type="text/css" media="all">
@import url("styles/screen.css"); 
</style>
<style> div.gridbox_dhx_skyblue table.hdr td { text-align: center; vertical-align: middle !important; } </style>
</HEAD>
<body leftmargin="15" topmargin="10" onLoad="fnOnload(); ">
 <html:form action="/gmLotTrackTxnLogRpt.do?method=loadLotTrackTxnLogDetails" > 
 <html:hidden name="frmLotTrackTxnLogRpt"  property="strOpt" value="" />  
 <html:hidden name="frmLotTrackTxnLogRpt"  property="strControlInvId"  />  
 <table class="DtTable1200" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="6" height="25" class="RightDashBoardHeader">&nbsp;<fmtLotTrackTxnLogRpt:message key="LOT_TRACK_TRANS_LOG_RPT_HEADER"/> </td>
				<td height="25" align="right" class=RightDashBoardHeader><fmtLotTrackTxnLogRpt:message key="IMG_ALT_HELP" var="varHelp"/>				    
				 <img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
             </tr>
             
            <tr class="evenshade"> 
				<td class="RightTableCaption" align="right" HEIGHT="30"><fmtLotTrackTxnLogRpt:message key="LBL_LOT_NUM"/>:&nbsp;</td>
				<td class="RightText">
				   <html:text property="strLotNumber" size="23" maxlength="40" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" style="width:186px;"/>
				    &nbsp;&nbsp;
				</td>  
				<td class="RightTableCaption" align="right" HEIGHT="30"><fmtLotTrackTxnLogRpt:message key="LBL_PART_NUM"/>:&nbsp;</td>
				<td class="RightText">
				   <html:text property="strPartNumber" size="23" maxlength="20" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>&nbsp;&nbsp; 
				
				<gmjsp:dropdown controlName="strPartLiteral" SFFormName="frmLotTrackTxnLogRpt" SFSeletedValue="strPartLiteral"
							SFValue="alPartSearch" codeId = "CODENMALT"  codeName = "CODENM" defaultValue= "[Choose One]" tabIndex="5"/>
				</td>
			    <td align="right" height="25" colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		  </tr>
		  <tr class="oddshade"> 
			    <td class="RightTableCaption" align="right" HEIGHT="30"><fmtLotTrackTxnLogRpt:message key="LBL_WAREHOUSE_TYPE"/>:&nbsp;</td>
				<td class="RightText">
				<gmjsp:dropdown controlName="warehouseType" SFFormName="frmLotTrackTxnLogRpt" SFSeletedValue="warehouseType"
							SFValue="alWarehouseType" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" tabIndex="5"/>
				</td> 
				<td class="RightTableCaption" align="right" HEIGHT="30"><fmtLotTrackTxnLogRpt:message key="LBL_EXPIRY_DATE"/>&nbsp;&nbsp;
				<fmtLotTrackTxnLogRpt:message key="LBL_FROM_DATE"/>:&nbsp;</td>
				  <td class="RightTableCaption" HEIGHT="30">  <gmjsp:calendar  SFFormName='frmLotTrackTxnLogRpt'  controlName="expiryDateFrom" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				 <fmtLotTrackTxnLogRpt:message key="LBL_TO_DATE"/>:&nbsp;
				    <gmjsp:calendar  SFFormName='frmLotTrackTxnLogRpt'  controlName="expiryDateTo" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/> &nbsp;&nbsp; 
				</td>
	          <td height="25" colspan="3">&nbsp;</td>
		 </tr>
		   <tr class="evenshade"> 
		  <td class="RightTableCaption" align="right" HEIGHT="30"><fmtLotTrackTxnLogRpt:message key="LBL_LOCATION"/>:&nbsp;</td>
	  	  <td class="RightText">
				   <html:text property="strLocation" size="23" onfocus="changeBgColor(this,'#AACCE8');" maxlength="40" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" style="width:186px;"/>
				    &nbsp;&nbsp;
		 </td> 
		 <td class="RightTableCaption" align="right" HEIGHT="30"><fmtLotTrackTxnLogRpt:message key="LBL_EXPIRED"/>:&nbsp;</td>
		 <td><html:checkbox property="strExpired"></html:checkbox></td>
		 <td align="center" height="25" colspan="1">
				<fmtLotTrackTxnLogRpt:message key="BTN_LOAD" var="varLoad"/>
			    <gmjsp:button value="&nbsp;${varLoad}&nbsp;"gmClass="button" buttonType="Load" onClick="fnLoad();" /> 
	    </td>
	   <td align="right" height="25" colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		 <tr><td colspan="7" height="1" bgcolor="#cccccc"></td></tr>
		 <tr class="evenshade"> 
				<td colspan="7"><div id="dataGridDiv" style="width:100%;"></div></td>
		</tr>
		<tr class="evenshade"> 
				<td colspan="7" align="center">
					<div class='exportlinks' id="DivExportExcel">
						<fmtLotTrackTxnLogRpt:message key="LBL_EXPORT_OPTIONS" />
						:<a href="#" onclick="fnDownloadXLS();"><img src='img/ico_file_excel.png' /></a>&nbsp;<a href="#"
							onclick="fnDownloadXLS();"><fmtLotTrackTxnLogRpt:message
								key="LBL_EXCEL" /></a>
					</div>
				</td>
		</tr>
		<tr height="30" id="DivNothingMessage">
				<td colspan="7" align="center"><div><fmtLotTrackTxnLogRpt:message key="LBL_NO_DATA" />
					</div></td>
		</tr>
       </table>
 </html:form> 
  <%@ include file="/common/GmFooter.inc" %> 
</BODY>
</HTML>