 <%
/**********************************************************************************
 * File		 		: GmReturnForCredit.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Venkata Prasath
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Date" %>
<%@ page import="com.globus.common.beans.GmCalenderOperations" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtReturnForCreditInclude"
	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
 <!-- GmReturnForCredit.jsp -->
<fmtReturnForCreditInclude:setLocale value="<%=strLocale%>" />
<fmtReturnForCreditInclude:setBundle
	basename="properties.labels.operations.logistics.GmReturnForCredit" />

<%

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmConsignDetails = new HashMap();
	String strhAction = GmCommonClass.parseNull((String)request.getParameter("hAction"));
	String strFrom = GmCommonClass.parseNull((String)request.getAttribute("hFrom"));
	String strApplnDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	String strPartNum = "";
	String strPrevsPartNum = "";
	String strCtrlNum = "";
	String strPartDesc = "";
	String strQty = "";
	String strItemQty = "";
	String strTagable = "";
	String strInitQty = "";
	String strReturnQty = "";
	String strRAId = "";
	String strRetType = "";
	String strPrice = "";
	String strItemType = "";
	String strUnitPrice = "";
	String strAdjVal = "";
	String strAdjCode = "";
	String strAdjCodeID = "";
	
	ArrayList alRetLoad = new ArrayList();
	alRetLoad = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("alReturn"));	
	
	int intSize = 0;
	HashMap hcboVal = null;
	

	if( hmReturn != null)
	 {
		 hmConsignDetails = (HashMap)hmReturn.get("RADETAILS");
		 if (hmConsignDetails != null)
			{
						strRAId = GmCommonClass.parseNull((String)hmConsignDetails.get("RAID"));
						strRetType = GmCommonClass.parseNull((String)hmConsignDetails.get("TYPEID"));
			}			
	 } 	 
%>
				<input type="hidden" id="hCreditString" name="hCreditString">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="3">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">
								<tr class="ShadeRightTableCaption">
									<td width="78" align="center" height="25"><fmtReturnForCreditInclude:message key="LBL_PART" /></td>
									<td width="340"><fmtReturnForCreditInclude:message key="LBL_PART_DESCRIPTION" /></td>
									<td width="60" align="center"><fmtReturnForCreditInclude:message key="LBL_LOT" /></td>
									<td width="60" align="center"><fmtReturnForCreditInclude:message key="LBL_UNIT_PRICE" /></td>
									<td width="60" align="center"><fmtReturnForCreditInclude:message key="LBL_UNIT_PRICE_ADJ" /></td>
									<td width="60" align="center"><fmtReturnForCreditInclude:message key="LBL_ADJ_CODE" /></td>
									<td width="60" align="center"><fmtReturnForCreditInclude:message key="LBL_NET_UNIT_PRICE" /></td>									
									<td width="60" align="center"><fmtReturnForCreditInclude:message key="LBL_INIT" /><br><fmtReturnForCreditInclude:message key="LBL_QTY" /></td>
									<td width="60" align="center"><fmtReturnForCreditInclude:message key="LBL_RET" /><br><fmtReturnForCreditInclude:message key="LBL_QTY" /></td>
									<td width="60" align="center"><fmtReturnForCreditInclude:message key="LBL_CREDIT" /><br><fmtReturnForCreditInclude:message key="LBL_QTY" /></td>
								</tr>
								<tr><td class=Line colspan="10" height="1"></td></tr>
								<%
									intSize = alRetLoad.size();
									if (intSize > 0){
									HashMap hmLoop = new HashMap();
		
									double intInitQty = 0.0;
									double intReturnQty = 0.0;
									double intCreditQty = 0.0;
									double dbPrice = 0;
									
							
									for (int i=0;i<intSize;i++)	{
										hmLoop = (HashMap)alRetLoad.get(i);

										strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
										strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
										strCtrlNum = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
										strUnitPrice = GmCommonClass.parseNull((String)hmLoop.get("UNITPRICE"));
										strAdjVal = GmCommonClass.parseNull((String)hmLoop.get("ADJVAL"));
										strAdjCode = GmCommonClass.parseNull((String)hmLoop.get("ADJCODE"));
										strAdjCodeID = GmCommonClass.parseNull((String)hmLoop.get("ADJCODEID"));
										strAdjCode = strAdjCode.equals("")? "N/A": strAdjCode;
										strPrice = GmCommonClass.parseNull((String)hmLoop.get("PRICE"));
										strInitQty = GmCommonClass.parseNull((String)hmLoop.get("INIT_QTY"));
										intInitQty = strInitQty.equals("") ? 0 : Double.parseDouble(strInitQty); 
										strReturnQty = GmCommonClass.parseNull((String)hmLoop.get("RETURN_QTY"));
										strItemType = GmCommonClass.parseNull((String)hmLoop.get("ITEMTYPE"));
										strTagable = GmCommonClass.parseNull((String)hmLoop.get("TAGABLE"));
										intReturnQty = strReturnQty.equals("") ? 0 : Double.parseDouble(strReturnQty);
										
										strInitQty = intInitQty == 0 ? "-" : String.valueOf(intInitQty);
										strReturnQty = intReturnQty == 0 ? "-" : String.valueOf(strReturnQty); 
										String strAlign = "right";
										if(!strTagable.equals("")){
											strAlign ="center";
										}
										intCreditQty = intReturnQty == 0 ? intInitQty : intReturnQty;
								%>
								<tr>
									<td class="RightText" height="20" align="<%=strAlign%>">&nbsp;
									<% if(!strTagable.equals("")){%>
										<a href=javascript:fnOpenTag('<%=strPartNum %>','<%=strRAId %>')>  <img style='cursor:hand' src='<%=strImagePath%>/tag.jpg'  width='12' height='12' />  </a><%}%>&nbsp;<%=strPartNum%>
									</td>
									<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									<td align="center" class="RightText"><%=strCtrlNum%></td>
									<td align="center" class="RightText"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strUnitPrice)) %></td>
									<td align="center" class="RightText"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strAdjVal)) %></td>
									<td align="center" class="RightText"><%=strAdjCode %></td>
									<td align="center" class="RightText">&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strPrice))%></td>										
									<td align="center" class="RightText">&nbsp;<%=(strPartNum.equals(strPrevsPartNum))?"":strInitQty%></td>										
									<td align="center" class="RightText">&nbsp;<%=GmCommonClass.getRedText(strReturnQty)%></td>
									<td align="center" class="RightText">&nbsp;
									<input type="text" size="2" id="TxtCreditQty<%=i%>" name="TxtCreditQty<%=i%>" value="<%=intCreditQty%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=3>&nbsp;
									<input type="hidden" id="hInitQty<%=i%>" name="hInitQty<%=i%>" value="<%=strInitQty%>">
									<input type="hidden" id="hReturnQty<%=i%>" name="hReturnQty<%=i%>" value="<%=strReturnQty%>">
									<input type="hidden" id="hPnum<%=i%>" name="hPnum<%=i%>" value="<%=strPartNum%>">
									<input type="hidden" id="hCnum<%=i%>" name="hCnum<%=i%>" value="<%=strCtrlNum%>">
									<input type="hidden" id="hPrice<%=i%>" name="hPrice<%=i%>" value="<%=strPrice%>">
									<input type="hidden" id="hType<%=i%>" name="hType<%=i%>" value="<%=strItemType%>">
									<input type="hidden" id="hUnitPrice<%=i%>" name="hUnitPrice<%=i%>" value="<%=strUnitPrice%>">
									<input type="hidden" id="hUnitPriceAdj<%=i%>" name="hUnitPriceAdj<%=i%>" value="<%=strAdjVal%>">
									<input type="hidden" id="hAdjCode<%=i%>" name="hAdjCode<%=i%>" value="<%=strAdjCodeID%>">
									</td>
								</tr>
								<tr><td  class="Line" colspan="10" height="1" ></td></tr>
		<%
		strPrevsPartNum = strPartNum;			
									}//End of FOR Loop
			}else{
		%>
				<tr>
					<td colspan="5" height="50" align="center" class="RightTextRed"><BR><fmtReturnForCreditInclude:message key="LBL_NO_PART_NUMBER_RETURNED" /></td>
				</tr>
		<%
			}		
		%>
						<input type="hidden" id="hPartCount" name="hPartCount" value="<%=intSize%>">
						</table>
				