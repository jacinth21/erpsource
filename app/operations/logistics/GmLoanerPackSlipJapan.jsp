
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
	/**********************************************************************************
	 * File		 		: GmPackSlip.jsp
	 * Desc		 		: This screen is used for the Packing Slip to japan
	 * Version	 		: 1.0
	 * author			: Mahavishnu
	 ************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Date" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<%@ page import="com.globus.common.beans.GmJasperReport"%>
<!-- \logistics\GmLoanerPackSlipJapan.jsp -->
<%
	
	String strAction = null;
	String strSource = "";
	String strApplnDateFmt = strGCompDateFmt;
	String strHtmlJasRpt="";


	strAction = GmCommonClass.parseNull((String) request
			.getAttribute("hAction"));
	strAction = (strAction.trim() == null) ? "PrintPack" : strAction;
	strAction = (strAction.equals("")) ? "PrintPack" : strAction;
	String strOpt = GmCommonClass.parseNull(request
			.getParameter("hOpt"));
	strSource = GmCommonClass.parseNull((String) request	
			.getAttribute("SOURCE"));
	String strGUID = GmCommonClass.parseNull(request
			.getParameter("hGUId"));

	HashMap hmAllData = new HashMap();
	HashMap hmConsignDetails = new HashMap();
	HashMap hmReturnVal = GmCommonClass.parseNullHashMap((HashMap)(HashMap)request.getAttribute("hmCONSIGNMENT"));
	ArrayList alSetLoadVal = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("CONSIGNSETDETAILS"));
	
	

	HashMap hmOrderDetails = new HashMap();
	HashMap hmShipDetails = new HashMap();
	HashMap hmSetDetails = new HashMap();

	ArrayList alCartDetails = new ArrayList();
	ArrayList alBackOrderDetails = new ArrayList();

	
	String strAccNm = "";
	String strDistRepNm = "";
	String strOrdId = "";

	String strShipDate = "";
	String strTrack = "";
	String strCompId = "";
	String strDivisionId = "";
	String strShowPhoneNo = "";
	String strShowGMNAAddress = "";
	String strCompanyId = "";
	String strPlantId = "";
	String strRepNm="";
	String strWareHouse="";
	String strStatus="";
	String strComments="";
	String strJasperName="";
	String strExpDate="";
	String strSetId="";
	String strSetName="";
	String strConsId="";
	String strTagId="";
	Date strSurgDate=null;

	String strOrdredDate = "";
	Date strShippingDate =null;

	//strJasperName="\\GmJapanLoanPackSlip.jasper";
	strPlantId = gmDataStoreVO.getPlantid();
	strCompanyId = GmCommonClass.parseNull(GmCommonClass.getRuleValue(
			strPlantId, "PLANT_PACK_SLIP")); //Rulevalue:= 1010 (or) 1017
	if (strCompanyId.equals("")) {
		strCompanyId = gmDataStoreVO.getCmpid();
	}
	String strCompanyLocale = GmCommonClass
			.getCompanyLocale(strCompanyId);
	String strLocaleCompany = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());

	GmResourceBundleBean gmResourceBundleBeanlbl = 
		    GmCommonClass.getResourceBundleBean("properties.labels.custservice.Shipping.GmPackSlip", strLocaleCompany);
	
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass
			.getResourceBundleBean("properties.Paperwork",
					strCompanyLocale);
	GmResourceBundleBean gmCompResourceBundleBean = GmCommonClass
			.getResourceBundleBean("properties.Company",
					strCompanyLocale);
	strShowPhoneNo = GmCommonClass
			.parseNull((String) gmResourceBundleBean
					.getProperty("PACK_SLIP.SHOW_PHONE_NO"));
	strShowGMNAAddress = GmCommonClass
			.parseNull((String) gmResourceBundleBean
					.getProperty("PACK_SLIP.SHOW_GMNA_ADDRESS"));
	//String straddress = GmCommonClass.getString("GMCOMPANYADDRESS");
	//String strGMName = GmCommonClass.getString("GMCOMPANYNAME");
	//straddress = "&nbsp;" + straddress.replaceAll("/", "\n<br>&nbsp;");
	
	if(hmReturnVal!=null){
		    strConsId = (String)hmReturnVal.get("CONSIGNID");
			strSetId = GmCommonClass.parseNull((String)hmReturnVal.get("SETID"));
			strSetName= GmCommonClass.parseNull((String)hmReturnVal.get("SETNAME"));
			strAccNm = (String)hmReturnVal.get("ACCNM");
			strDistRepNm = (String)hmReturnVal.get("DELEARNM");
			strRepNm=(String)hmReturnVal.get("REPNM");
			strAccNm = strAccNm.equals("")?strDistRepNm:strAccNm;
			strComments = (String)hmReturnVal.get("COMMENTS");
			strStatus = GmCommonClass.parseNull((String)hmReturnVal.get("LOANSFL"));
			strTagId=GmCommonClass.parseNull((String) hmReturnVal
				.get("TAGID"));
			
			
			strSurgDate = (java.util.Date) hmReturnVal.get("SURGERYDATE");
			
			  strShippingDate = (java.util.Date) hmReturnVal.get("SHIPDATE");
			
		}

	boolean blBackFl = false;
	
	
			  		
			  		hmAllData.put("ANAME",strAccNm);		
					hmAllData.put("REPDISTNM",strDistRepNm);	
					hmAllData.put("REPNM",strRepNm);	
					hmAllData.put("SHIP_DATE",strShippingDate);
					hmAllData.put("WAR_HOUSE","FG Warehouse");
					hmAllData.put("SET_ID",strSetId);
					hmAllData.put("SET_NAME",strSetName);
					hmAllData.put("BAR_CODE_ID",strSetId);
					hmAllData.put("SURG_DATE",strSurgDate);
					hmAllData.put("STATUS_FLG",strStatus);
					hmAllData.put("COMMENTS",strComments);
					hmAllData.put("CONS_ID",strConsId);
					hmAllData.put("ORDER_ID",strOrdId);
					hmAllData.put("TAG_ID",strTagId);
					hmAllData.put("LBL_HEADER",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_HEADER")));
					hmAllData.put("LBL_LBOX_NUM",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_LBOX_NUM")));
					hmAllData.put("LBL_LBOX_NAME",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_LBOX_NAME")));
					hmAllData.put("LBL_PRINT_DATE",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PRINT_DATE")));
					hmAllData.put("LBL_STATUS",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_STATUS")));
					hmAllData.put("LBL_WARE",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_WARE")));
					hmAllData.put("LBL_SHIP_DATE",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SHIP_DATE")));
					hmAllData.put("LBL_DEALER_NAME",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_DEALER_NAME")));
					hmAllData.put("LBL_HOSPITAL_NAME",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_HOSPITAL_NAME")));
					hmAllData.put("LBL_SUR_DATE",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SUR_DATE")));
					hmAllData.put("LBL_SALES_NAME",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SALES_NAME")));
					hmAllData.put("LBL_BILL_NAME",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_BILL_NAME")));
					hmAllData.put("LBL_MODEL_NUM",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_MODEL_NUM")));
					hmAllData.put("LBL_DESC",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_DESC")));
					hmAllData.put("LBL_LOT_NUM",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_LOT_NUM")));
					hmAllData.put("LBL_EXPIRE_DATE",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_EXPIRE_DATE")));
					hmAllData.put("LBL_QTY",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_QTY")));
					hmAllData.put("LBL_NOTE",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_NOTE")));
					hmAllData.put("LBL_FOOT_NOTE",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_FOOT_NOTE")));
					hmAllData.put("LBL_ORDER_ID",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ORDER_ID")));
					hmAllData.put("LBL_CONSID",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_CONS_ID")));
					hmAllData.put("LBL_TAG_ID",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_TAG_ID")));
%>

<TITLE>Globus Medical: Packing Slip</TITLE>
	
<table>
	<tr>		
    	<td>
		<%
			GmJasperReport gmJasReport = new GmJasperReport();
			gmJasReport.setRequest(request);
			gmJasReport.setResponse(response);
			gmJasReport.setJasperReportName("/GmJapanLoanPackSlip.jasper");
			gmJasReport.setHmReportParameters(hmAllData);
			gmJasReport.setReportDataList(alSetLoadVal);
			gmJasReport.setBlDisplayImage(true);
			strHtmlJasRpt = gmJasReport.getXHtmlReport();
		%>
		<%=strHtmlJasRpt%>
	</td>
	</tr>
	</table>
	
