<%
/**********************************************************************************
 * File		 		: GmInHouseSetShip.jsp
 * Desc		 		: This screen is used to ship out In-House Sets
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.*,java.text.*" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ taglib prefix="fmtInHouseSetShip" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmInHouseSetShip.jsp -->


<fmtInHouseSetShip:setLocale value="<%=strLocale%>"/>
<fmtInHouseSetShip:setBundle basename="properties.labels.operations.logistics.GmInHouseSetShip"/>


<%
	// Get today's date
	Calendar now = Calendar.getInstance();
	Calendar working;
	SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
	working = (Calendar) now.clone();
	working.add(Calendar.DAY_OF_YEAR, + 7);
	String strExpRetnDate = (String)formatter.format(working.getTime());
	
	String strWikiTitle = GmCommonClass.getWikiTitle("SHIP_OUT");
	
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	ArrayList alLoanTo = new ArrayList();
	ArrayList alShipTo = new ArrayList();
	ArrayList alDistributor = new ArrayList();
	ArrayList alRepList = new ArrayList();
	ArrayList alAccList = new ArrayList();
	ArrayList alEmpList = new ArrayList();
	ArrayList alCarrier = new ArrayList();
	ArrayList alMode = new ArrayList();
	HashMap hmTemp = new HashMap();
	String strOpt = (String)session.getAttribute("strSessOpt") == null?"":(String)session.getAttribute("strSessOpt");
	if (hmReturn != null)
	{
		hmTemp = (HashMap)hmReturn.get("SETCONLISTS");
		alLoanTo = (ArrayList)hmTemp.get("LOANTO");
		alShipTo = (ArrayList)hmTemp.get("SHIPTO");
		alDistributor = (ArrayList)hmTemp.get("DISTRIBUTORLIST");
		alRepList = (ArrayList)hmTemp.get("REPLIST");
		alAccList = (ArrayList)hmTemp.get("ACCLIST");
		alEmpList = (ArrayList)hmTemp.get("EMPLIST");
		alCarrier = (ArrayList)hmTemp.get("DELCARR");
		alMode = (ArrayList)hmTemp.get("DELMODE");
	}	

	int intSize = 0;
	HashMap hcboVal = null;
	String strShipTo = "";
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Consignment Set Details </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmInHouseSetShip.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
</head>
<SCRIPT>
var DistLen = <%=alDistributor.size()%>;
<%
	intSize = alDistributor.size();
	hcboVal = new HashMap();
	for (int i=0;i<intSize;i++)
	{
		hcboVal = (HashMap)alDistributor.get(i);
%>
	var DistArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
	}
%>



var AccLen = <%=alAccList.size()%>;
<%
	intSize = alAccList.size();
	hcboVal = new HashMap();
	for (int i=0;i<intSize;i++)
	{
		hcboVal = (HashMap)alAccList.get(i);
%>
	var AccArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>,<%=hcboVal.get("DID")%>";
<%
	}
%>

var EmpLen = <%=alEmpList.size()%>;
<%
	intSize = alEmpList.size();
	hcboVal = new HashMap();
	for (int i=0;i<intSize;i++)
	{
		hcboVal = (HashMap)alEmpList.get(i);
%>
	var EmpArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
	}
%>

</SCRIPT>
<BODY leftmargin="20" topmargin="10" onload="fnOnLoad();">
<xml src="<%=strXmlPath%>/<%=strOpt%>.xml" id="xmldso" async="false"></xml>
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmInHouseSetServlet">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hReqId" value="">

	<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
		<tr> 
			<td colspan="2"> <table cellspacing="0" cellpadding="0" border="0"><tr>
			<%if (strOpt.equals("4127")){%>
				<td height="25" class="RightDashBoardHeader"><fmtInHouseSetShip:message key="LBL_PROCESS_LOANERS_SHIP_OUT"/></td>
			<%}else if (strOpt.equals("4119")){ %>
				<td height="25" class="RightDashBoardHeader"><fmtInHouseSetShip:message key="LBL_PROCESS_INHOUSE_SHIP_OUT"/></td>
			<%} %>			
			<td height="25" class="RightDashBoardHeader" align="right">
			<fmtInHouseSetShip:message key="IMG_ALT_HELP" var = "varHelp"/>
			 <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> </td>				
			</tr></table></td>
		</tr>
		<tr>
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
			<td colspan="2">	
				<jsp:include page="/include/GmIncludeConsignment.jsp" />
				
			</td>
		</tr>
		<!-- 
		<tr class="ShadeRightTableCaption" height="23">  
			<td colspan="2">Loaner Pending Requests Details</td>
		</tr>
		<tr>
			<td colspan="2" height="30">
				 <display:table name="LOANERREQUESTS" class="its" id="currentRowObject"  decorator="com.globus.displaytag.beans.DTLoanerRequestMasterWrapper"> 
				    <display:column property="PDTREQID" title="Request ID" sortable="true" />
				    <display:column property="REQSDT" title="Requested Date" sortable="true" class="aligncenter" />
				    <display:column property="DNAME" title="Distributor" sortable="true" class="aligncenter"  />
				    <display:column property="SHIPTONM" title="Ship To" sortable="true" class="aligncenter"  />
					<display:column property="REQRDT" title="Required Date" sortable="true" class="aligncenter"  />
					<display:column property="CUSER" title="Created By" sortable="true" class="aligncenter"  />
					 
				 </display:table> 
			</td>
		</tr>
		 -->
		<tr><td colspan="2" height="1" bgcolor="#CCCCCC"></td></tr>
		<tr class="ShadeRightTableCaption">
			<td HEIGHT="25"  colspan="2"><fmtInHouseSetShip:message key="LBL_LOANER_DETAILS"/>:</td>
		<tr>		
		<tr>
			<td class="RightTableCaption" HEIGHT="23" align="right">&nbsp;<font color="red">*</font><fmtInHouseSetShip:message key="LBL_LOAN_TO"/>:</td>
			<td class="RightTableCaption"> 	&nbsp;<select name="Cbo_LoanTo" id="Con" class="RightText" tabindex="2" onChange="javascript:fnCallValues(this);"><option value="0" ><fmtInHouseSetShip:message key="LBL_CHOOSE_ONE"/>
<%
			  		intSize = alLoanTo.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alLoanTo.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strShipTo.equals(strCodeID)?"selected":"";
%>
				<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>
				</select>&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">*</font><fmtInHouseSetShip:message key="LBL_VALUES"/>:&nbsp;<select name="Cbo_ValuesCon" class="RightText" onChange="javascript:fnSetDistValues();" disabled tabindex="3"><option value="0" >[Choose One]
			</select>
			</td>
		</tr>
		<tr class="shade">
			<td class="RightTableCaption" HEIGHT="23" align="right">&nbsp;<fmtInHouseSetShip:message key="LBL_SALES_REP"/>:</td>
			<td>&nbsp;<select name="Cbo_LoanToRep" class="RightText" tabindex="2" onChange="javascript:fnSetRepValue();" disabled><option value="0" ><fmtInHouseSetShip:message key="LBL_CHOOSE_ONE"/></select>
			</td>
		</tr>
		<tr>
			<td class="RightTableCaption" HEIGHT="23" align="right">&nbsp;<fmtInHouseSetShip:message key="LBL_ACCOUNT"/>:</td>
			<td>&nbsp;<select name="Cbo_LoanToAcc" class="RightText" tabindex="2" onChange="javascript:fnSetAccValue();" disabled><option value="0" >[Choose One]</select>
			</td>
		</tr>
		<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
		<tr class="shade">
			<td height="23" align="right" class="RightTableCaption" width="20%"><fmtInHouseSetShip:message key="LBL_EXP_DATE_RETURN"/>:</td>
			<td>&nbsp;<input type="text" size="10" value="<%=strExpRetnDate %>" name="Txt_RetDate" class="InputArea" tabindex=1>&nbsp;<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmVendor.Txt_RetDate');" title="Click to open Calendar"  src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />
			</td>
		</tr>
		<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
		<tr>
			<td colspan="2">	
				<jsp:include page="/gmIncShipDetails.do">	
				<jsp:param name="screenType" value="Loaner" />
				</jsp:include>
			</td>
		</tr>	
		<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
		<tr>
			<td height="30" colspan="2"><a href="#" class="RightText" onClick="fnShow();">&nbsp;<b><fmtInHouseSetShip:message key="LBL_SHOW_SET_DETAILS"/></b>
				<DIV style="display:none" id="filter"><BR>
				<jsp:include page="/include/GmIncludeConsignSetDetails.jsp" />
				
				</DIV>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="aligncenter">
			<fmtInHouseSetShip:message key="BTN_SUBMIT" var="varSubmit"/>
				<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="fnSubmit();" tabindex="25" buttonType="Save" />&nbsp;<BR><BR>
			</td>
		</tr>
	</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
