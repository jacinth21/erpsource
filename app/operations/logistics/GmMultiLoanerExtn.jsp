 <!-- operations\logistics\GmMultiLoanerExtn.jsp -->
 <%
	/**********************************************************************************
	 * File		 		: GmMultiLoanerExtn.jsp
	 * Desc		 		: This screen is used for the Multi Loaner extension
	 * author			: Raja 
	 ************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@page import="java.util.Date"%> 
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%> 
<%@page import="com.globus.common.beans.GmCalenderOperations"%>
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.net.URLEncoder" %>
<%@ taglib prefix="fmtGmMultiLoanerExtn" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtGmMultiLoanerExtn:setLocale value="<%=strLocale%>"/>
<fmtGmMultiLoanerExtn:setBundle basename="properties.labels.operations.logistics.GmMultiLoanerExtn"/>
<%
String strLogisticsJsPath= GmCommonClass.parseNull(GmFilePathConfigurationBean.getFilePathConfig("JS_LOGISTICS"));
String strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("LOANER_EXTN_MULTI_SETS"));
String strApplDateFmt = strGCompDateFmt;
GmCalenderOperations.setTimeZone(strGCompTimeZone);
String strTodays = GmCalenderOperations.getCurrentDate(strGCompDateFmt);

String strRepId = GmCommonClass.parseNull((String)request.getAttribute("REPID"));
String strRepName = GmCommonClass.parseNull((String)request.getAttribute("REPNAME"));

String strDistId = GmCommonClass.parseNull((String)request.getAttribute("REPID"));
String strDistName = GmCommonClass.parseNull((String)request.getAttribute("REPNAME"));

String strCurrDate = GmCommonClass.parseNull((String) request.getAttribute("CurrDate"));

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Requests - Multi Loaner Extension </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="styleSheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css"> 

<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script> 
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script> 
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
	
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_dhxcalendar.js"></script> 

<script language="JavaScript" src="<%=strLogisticsJsPath%>/GmMultiLoanerExtn.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
 <script language="JavaScript" src="<%=strJsPath%>/jquery-latest.js"></script>
	
 


<script type="text/javascript">
var currDate = '<%= strCurrDate %>';
var date_format = '<%=strApplDateFmt%>';
var today_Dt = '<%=strTodays%>';
var currDateFmt = '<%=strApplDateFmt%>';  
</script>
<style type="text/css" media="all">
@import url("styles/screen.css"); 
</style>
<style type="text/css">
         table.DtTable1200 {
             margin: 0 0 0 0;
             width: 1200px;
             border: 1px solid #676767;
             height:305px;
         }
         #selectGridDiv .objbox{
           height:250px !important;
         }
         #selectGridDiv{
              height:310px !important;
         }
         
</style>
</HEAD>
<body leftmargin="15" topmargin="10" onLoad="fnOnPageLoad(); ">
 <html:form action="/gmMultiLoanerExtn.do" > 
 <input type="hidden" name="hSalesRepName" value ="<%=strRepName %>"/>
 <input type="hidden" name="hDistName" value ="<%=strDistName %>"/>
 

<table class="DtTable1200" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="9" height="25" class="RightDashBoardHeader">&nbsp;<fmtGmMultiLoanerExtn:message key="LOANER_EXTN_MULTI_SET_HEADER"/> </td>
				<td height="25" align="right" class=RightDashBoardHeader><fmtGmMultiLoanerExtn:message key="IMG_ALT_HELP" var="varHelp"/>				    
				 <img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
             </tr>
             <tr><td>
             <table class="DtTable1995">
              <tr class="evenshade"> 
				<td class="RightTableCaption" align="right" HEIGHT="30"><fmtGmMultiLoanerExtn:message key="LBL_SET_NUM"/>&nbsp;:&nbsp;</td>
				<td class="RightText">
				   <html:text property="strSetName" size="35" maxlength="40" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" tabindex="1" />
				    &nbsp;&nbsp;
				</td>  
				<td class="RightTableCaption" align="right" HEIGHT="30"><fmtGmMultiLoanerExtn:message key="LBL_SET_ID"/>&nbsp;:&nbsp;</td>
				<td class="RightText">
				   <html:text property="strSetId" size="35" maxlength="20" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" tabindex="2"/>&nbsp;&nbsp; 
				</td>
				<td class="RightTableCaption" align="right" HEIGHT="30"><fmtGmMultiLoanerExtn:message key="LBL_CONSIGN_ID"/>&nbsp;:&nbsp;</td>
		      <td class="RightText">
			 <html:text property="strConsignmentId" size="23" maxlength="40" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" tabindex="3" />
				    &nbsp;&nbsp;
		      </td> 
			    <td align="right" height="25" colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		    </tr>
			<tr>
				<td colspan="4" class="RightTableCaption" align="right" HEIGHT="30">
					<table>
					<tr>
						<fmtGmMultiLoanerExtn:message key="LBL_SALES_REP"
							var="varSalesRepList" />
						<gmjsp:label type="BoldText"
							SFLblControlName="${varSalesRepList}&nbsp;:" td="true" />
						<td><jsp:include page="/common/GmAutoCompleteInclude.jsp">
								<jsp:param name="CONTROL_NAME" value="Cbo_RepId" />
								<jsp:param name="METHOD_LOAD" value="loadSalesRepList" />
								<jsp:param name="WIDTH" value="250" />
								<jsp:param name="CSS_CLASS" value="search" />
								<jsp:param name="TAB_INDEX" value="4"/>
								<jsp:param name="CONTROL_NM_VALUE" value="<%=strRepName %>" />
								<jsp:param name="CONTROL_ID_VALUE" value="<%=strRepId %>" />
								<jsp:param name="SHOW_DATA" value="10" />
								<jsp:param name="ON_BLUR" value="fnSetRepName(this.value);" />
							</jsp:include></td>
							
							<fmtGmMultiLoanerExtn:message key="LBL_DISTRIBUTOR" var="varDistributorName"/>
							<gmjsp:label type="BoldText"  SFLblControlName="${varDistributorName}&nbsp;:" td="true"/>
							<td><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
							<jsp:param name="CONTROL_NAME" value="Cbo_DistId" />
							<jsp:param name="METHOD_LOAD" value="loadFieldSalesList" />
							<jsp:param name="WIDTH" value="250" />
							<jsp:param name="CSS_CLASS" value="search" />
							<jsp:param name="TAB_INDEX" value="5"/>
							<jsp:param name="CONTROL_NM_VALUE" value="<%=strDistName %>" /> 
							<jsp:param name="CONTROL_ID_VALUE" value="<%=strDistId %>" />  					
							<jsp:param name="SHOW_DATA" value="10" />
							<jsp:param name="ON_BLUR" value="fnDistName(this.value);" />
					</jsp:include></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>	
						</tr>
					</table>
				</td>
		  
		   <td height="25" align="right">
				<fmtGmMultiLoanerExtn:message key="BTN_LOAD" var="varLoad"/>
			    <gmjsp:button value="&nbsp;${varLoad}&nbsp;"gmClass="button" buttonType="Load" onClick="fnLoadLoanerDtl();" /> 
		   </td>
		  </tr>
		 </table> </td> </tr>
		 <tr><td class="LLine" colspan="10" height="1"></td></tr>
	     <tr class="oddshade"> 
		   <td colspan="10"><div id="loanerGridDiv" height="600px"></div></td>
	     </tr>
         <tr><td class="LLine" colspan="10" height="1"></td></tr>
         <tr class="ShadeRightTableCaption">
		   <td height="30" colspan="10">Selected Sets:</td>
	    </tr>
		  
		<tr><td class="LLine" colspan="10" height="1"></td></tr>
		<tr class="evenshade"> 
				<td colspan="10"><div id="selectGridDiv" height="600px"></div></td>
		</tr> 
         <tr><td class="LLine" colspan="10" height="1"></td></tr>
        <tr>
	      <td class="RightTableCaption" align="center" colspan="10">
	          <table border="0" cellspacing="0" cellpadding="0" >
	           <tr><td colspan="10" height="30px;"></td></tr>
			     <tr><td align="center">
				 	<fmtGmMultiLoanerExtn:message key="BTN_SUBMIT" var="varSubmit"/>
				 	<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" style="width: 6em" gmClass="button" buttonType="Save" onClick="fnSubmit();"/>
				
				 	<fmtGmMultiLoanerExtn:message key="BTN_RESET" var="varReset"/>
					 <gmjsp:button value="&nbsp;${varReset}&nbsp;" style="width: 6em" name="Btn_Reset" gmClass="button" buttonType="Save" onClick="fnReset();" /> 
			      </td>
			  </tr>
			  </table>
		 </td>
		</tr>
	<tr><td colspan="10" height="4"></td></tr>
</table>
</html:form>
  <%@ include file="/common/GmFooter.inc" %> 
</BODY>
</HTML>