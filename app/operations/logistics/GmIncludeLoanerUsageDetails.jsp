<%
/**********************************************************************************
 * File		 		: GmIncludeLoanerUsageDetails.jsp
 * Desc		 		: This screen is used to display the loaner usage details 
 * Version	 		: 1.0
 * author			: Satyajit Thadeshwar
 **********************************************************************************/
%>
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>

<!-- operations\logistics\GmIncludeLoanerUsageDetails.jsp -->

<%@ taglib prefix="fmtIncludeLoanerUsageDetails" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtIncludeLoanerUsageDetails:setLocale value="<%=strLocale%>"/>
<fmtIncludeLoanerUsageDetails:setBundle basename="properties.labels.operations.logistics.GmIncludeLoanerUsageDetails"/>


<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<%
	String type = request.getParameter("TYPE") == null ? "" : request.getParameter("TYPE");
	log.debug(" action / type " +type);
	request.setAttribute("TYPE",type);
%>
<%
String strApplDateFmt = strGCompDateFmt;
String strRptFmt = "{0,date,"+strApplDateFmt+"}";
%>
<table border="0" width="100%" cellspacing="0" cellpadding="0" >
	<tr>
		<td>
			<display:table name="requestScope.RSDHISTORYRPT.rows" class="its" id="currentRowObject"  decorator="com.globus.operations.displaytag.beans.DTLEHistoryViewWrapper">
				<fmtIncludeLoanerUsageDetails:message key="DT_NO" var="varNo"/><display:column title="${varNo}"> <%=pageContext.getAttribute("currentRowObject_rowNum")%> </display:column> 
				<fmtIncludeLoanerUsageDetails:message key="DT_TXNID" var="varTxnID"/><display:column property="LTXNID" title="${varTxnID}" sortable="true" style="width:50px" />
			<% if(type.equals("ReloadUsage")) { %>
				<fmtIncludeLoanerUsageDetails:message key="DT_DISTRIBUTOR" var="varDistributor"/><display:column property="DISTNM" title="${varDistributor}" maxLength="200" sortable="true" />
			<% } else { %>
				<fmtIncludeLoanerUsageDetails:message key="DT_ETCH_ID" var="varEtchId"/><display:column property="ETCHID" title="${varEtchId}" sortable="true" style="width:80px"/>
				<fmtIncludeLoanerUsageDetails:message key="DT_CONSIGNMENT_ID" var="varConsignmentId"/><display:column property="CONID" title="${varConsignmentId}" sortable="true" style="width:100px"/>
				<fmtIncludeLoanerUsageDetails:message key="DT_SET_ID" var="varSetId"/><display:column property="SNAME" title="${varSetId}" maxLength="20" sortable="true" />
			<% } %>
				<fmtIncludeLoanerUsageDetails:message key="DT_ACCOUNT" var="varAccount"/><display:column property="ANAME" title="${varAccount}" maxLength="20" sortable="true" />
				<fmtIncludeLoanerUsageDetails:message key="DT_REP" var="varRep"/><display:column property="RNAME" title="${varRep}" maxLength="20" sortable="true"/>
				<fmtIncludeLoanerUsageDetails:message key="DT_ASSOCREP" var="varAssocRep"/><display:column property="ASSOCREPNM" title="${varAssocRep}" maxLength="20" sortable="true"/>
				<fmtIncludeLoanerUsageDetails:message key="DT_EMPLOYEE" var="varEmployee"/><display:column property="EMPNM" title="${varEmployee}" maxLength="200" sortable="true" />
				<fmtIncludeLoanerUsageDetails:message key="DT_LOANED_DATE" var="varLoanedDate"/><display:column property="LDATE" title="${varLoanedDate}" sortable="true"   class="aligncenter"  format="<%=strRptFmt%>"/>
				<fmtIncludeLoanerUsageDetails:message key="DT_EXPRETURN_DATE" var="varExpReturnDate"/><display:column property="ERDATE" title="${varExpReturnDate}" sortable="true" class="aligncenter" format="<%=strRptFmt%>" />
				<fmtIncludeLoanerUsageDetails:message key="DT_ACTRETURN_DATE" var="varActReturnDate"/><display:column property="RDATE" title="${varActReturnDate}" sortable="true" class="aligncenter" format="<%=strRptFmt%>" />
				<fmtIncludeLoanerUsageDetails:message key="DT_DAYS_LOANED" var="varDaysLoaned"/><display:column property="USAGEDAYS" title="${varDaysLoaned}" sortable="true" class="aligncenter"  />
			
				<fmtIncludeLoanerUsageDetails:message key="DT_ISLOANEREXIT" var="varIsExit"/><display:column property="ISEXT" title="${varIsExit}" sortable="true" class="aligncenter"  />
				<fmtIncludeLoanerUsageDetails:message key="DT_IS_REPLEN" var="varIsReplen"/><display:column property="ISREP" title="${varIsReplen}" sortable="true" class="aligncenter"  />
				<fmtIncludeLoanerUsageDetails:message key="DT_PARENT_TXNID" var="varParentTxnID"/><display:column property="PARTID" title="${varParentTxnID}" sortable="true" class="aligncenter"  />
			
			</display:table>
		</td>
	</tr>
</table>