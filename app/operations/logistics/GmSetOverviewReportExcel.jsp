 <%
/**********************************************************************************
 * File		 		: GmSetOverviewReportExcel.jsp
 * Desc		 		: This screen is used to download the same in Excel format
 * Version	 		: 1.0
 * author			: Lakshmi
************************************************************************************/
%>
<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %> 
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<!-- \operations\logistics\GmSetOverviewReportExcel.jsp -->
 <%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap, java.util.Calendar" %>
<%@ page import ="com.globus.common.beans.GmCalenderOperations"%> 
 <!-- WEB-INF path corrected for JBOSS migration changes -->


<!-- Imports for Logger -->
 
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.beans.GmCalenderOperations"%>
 
<bean:define id="hmSetOverview" name="frmLogisticsReport" property="hmCrossTabReport" type="java.util.HashMap"> </bean:define> 
<%
	 
	 
	String strWikiTitle = GmCommonClass.getWikiTitle("SET_OVERVIEW");
	String a="";
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	gmCrossTab.setGeneralHeaderStyle("aaTopHeader");
	gmCrossTab.setTotalRequired(true);
	gmCrossTab.setColumnTotalRequired(true);
	gmCrossTab.setNoDivRequired(true);
	 
	gmCrossTab.setRowHighlightRequired(true);
	gmCrossTab.setLinkRequired(false);	

	gmCrossTab.setValueType(0);
	

	gmCrossTab.add_RoundOff("Avg Net","2");
	gmCrossTab.setDisableLine(true);

	
	
	gmCrossTab.setGeneralHeaderStyle("ShadeDarkBlueTD"); 
	gmCrossTab.addStyle("Name","ShadeMedGrayTD","ShadeDarkGrayTD");
	gmCrossTab.addStyle("Shipped","ShadeMedBrownTD","ShadeDarkBrownTD") ;
	gmCrossTab.addStyle("Pending Shipping","ShadeMedBrownTD","ShadeDarkBrownTD") ;
	gmCrossTab.addStyle("Ready To Consign","ShadeMedBrownTD","ShadeDarkBrownTD") ;
	gmCrossTab.addStyle("Pending Verification","ShadeMedBrownTD","ShadeDarkBrownTD") ;
	gmCrossTab.addStyle("WIP","ShadeMedBrownTD","ShadeDarkBrownTD") ;
	gmCrossTab.addStyle("Initiated","ShadeMedBrownTD","ShadeDarkBrownTD") ;
	gmCrossTab.addStyle("Inhouse Loaner","ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
	gmCrossTab.addStyle("Product Loaner","ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
	gmCrossTab.addStyle("Avg Net","ShadeLightGreenTD","ShadeDarkGreenTD") ;
	gmCrossTab.addStyle("Set PAR","ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
	gmCrossTab.addStyle("Owe","ShadeLightGreenTD","ShadeDarkGreenTD") ;
	gmCrossTab.addStyle("Yet To Build","ShadeLightYellowTD","ShadeMedYellowTD") ;
	
	
	gmCrossTab.setColumnOverRideFlag(true);
	//gmCrossTab.addLine("Name");
	
	//gmCrossTab.setDecorator("com.globus.crosstab.beans.GmSetOverviewDecorator");
			
	String strFromdaysDate = GmCalenderOperations.getFirstDayOfMonth(0);
	String strTodaysDate = GmCalenderOperations.getLastDayOfMonth(0);
	
	String strFromdayDate = GmCalenderOperations.getFirstDayOfMonth(-6);
	String strTodayDate = GmCalenderOperations.getLastDayOfMonth(-1);
	%>

<HTML>

<HEAD>
<TITLE> Globus Medical: Set Overview Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<style>
.borderDark {
	FONT-SIZE: 12px; 
	FONT-FAMILY: Helvetica,sans-serif; 
	BACKGROUND-COLOR: #7c816a
}

.borderLight {
	FONT-SIZE: 12px; 
	FONT-FAMILY: Helvetica,sans-serif; 
	BACKGROUND-COLOR: #cccccc
}

.ShadeDarkGrayTD{
	background-color: #c3c5b8;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeLightGrayTD{
	background-color: #dee0d2;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeMedGrayTD{
	background-color: #dee0d2;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeDarkBlueTD{
	background-color: #a0b3de;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}
.ShadeLightBlueTD{
	background-color: #f1f2f7;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeMedBlueTD{
	background-color: #e4e6f2;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeDarkGreenTD{
	background-color: #ccdd99;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeMedGreenTD{
	background-color: #e1f3a6;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeDarkOrangeTD{
	background-color: #feb469;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeMedOrangeTD{
	background-color: #ffecb9;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeLightOrangeTD{
	background-color: #fef2cc;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}
.ShadeBlueTD{
	background-color: #ecf6fe;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeRightTableCaptionBlueTD{
	FONT-SIZE: 9px; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
	background-color: #AACCE8;
}

.ShadeBlueTDSmall{
	background-color: #ecf6fe;
	COLOR : Black;
	font-family: sans-serif;
	font-size : 11px;
	font-style: normal;
	height: 20px;
}

.CrossRightText {
	FONT-SIZE: 9px; 
	COLOR: #000000; 
	FONT-FAMILY: verdana, arial, sans-serif;
}

.RightTextRedSmall { 
	FONT-SIZE: 11px; 
	COLOR: #FF0000; 
	FONT-FAMILY:sans-serif;
	text-decoration: None;
}

.RightTextSmall {
	FONT-SIZE: 9px; 
	COLOR: #000000; 
	FONT-FAMILY: verdana, arial, sans-serif;
}

.ShadeLightYellowTD {
	background-color: #ffffcc;
	color : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}

.ShadeMedYellowTD{
	background-color: #ffff99;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}

.ShadeMedBrownTD{
	background-color: #D2FFFF;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}


.ShadeDarkBrownTD{
	background-color: #BEFFFF;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}


.ShadeLightYellowTD {
	background-color: #ffffcc;
	color : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}

.ShadeMedYellowTD{
	background-color: #ffff99;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}
.ShadeLightGreenTD {
	background-color: #ecffaf;
	color : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}

.ShadeDarkGreenTD{
	background-color: #ccdd99;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}


	background-color: #dee0d2;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
</style>
</HEAD>

<TABLE cellSpacing=0 cellPadding=0  border=0 width="1000">
<TBODY>
	<tr>
		<td align=center><%=gmCrossTab.PrintCrossTabReport(hmSetOverview) %></td>
		
	</tr>
</FORM>
</BODY>
</HTML>