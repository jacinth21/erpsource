
<%
/**********************************************************************************
 * File		 		: GmInhouseLoanerItemPrint.jsp
 * Desc		 		: This screen is used for the
 * Version	 		: 1.0
 * author			: Gopinathan
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %> 
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmJasperReport"%>
<%@ page import="java.util.Date"%>
                                                                                                           
<%@ taglib prefix="fmtInhouseLoanerItemPrint" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmInhouseLoanerItemPrint.jsp -->

<fmtInhouseLoanerItemPrint:setLocale value="<%=strLocale%>"/>
<fmtInhouseLoanerItemPrint:setBundle basename="properties.labels.operations.logistics.GmInhouseLoanerItemPrint"/>

<%

String strAction = "";
String strReturnId = "";

HashMap hmResult = new HashMap();

strAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
strReturnId = GmCommonClass.parseNull((String)request.getAttribute("hReturnId"));


System.out.println("Return ID : : : "+strReturnId);

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Loaner Letter</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screenOus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script type="text/javascript">


function fnPrintDiv(){	
	window.frames["retItem"].focus();
    //window.frames.print();
    //Get the HTML of div
    var divElements1 = document.getElementById("jasperItem").innerHTML;
    //var divElements2= document.getElementById("ackPrint").innerHTML;
    //var divElements3 = document.getElementById(divID).innerHTML;    
    //Get the HTML of whole page
    var oldPage = document.body.innerHTML;

    //Reset the page's HTML with div's HTML only
    document.body.innerHTML = 
      "<html><head><title></title></head><body>" + 
      divElements1 + "</body>";
     // fnAutoPrint();
    // fnAutoPrint();    
    //Print Page
    //window.print();
    //Restore orignal HTML
    document.body.innerHTML = oldPage;
	window.print();		 
}
var tdinnner = "";
var strObject="";

function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}

function fnAutoPrint(){
	var OLECMDID = 6;
	//var WebBrowser1 = document.getElementById("WebBrowser1"); 
	var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
	document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
	WebBrowser1.ExecWB(OLECMDID, 2);
	WebBrowser1.outerHTML = "";
} 

</script>
</HEAD>
<BODY leftmargin="20" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<form action="">
<table border='0' width='100%' cellspacing='0' cellpadding='0'>
	<tr>
		<td align="right" id="button" >
		<fmtInhouseLoanerItemPrint:message key="IMG_ALT_CLICKTOPRINT" var = "varPrint"/>
			<img style="cursor:hand" src='<%=strImagePath%>/print-icon.png' height="25" width="25" alt="${varPrint}" onClick="return fnPrint();" />
			<fmtInhouseLoanerItemPrint:message key="IMG_ALT_CLICKTOCLOSE" var = "varClose"/>
			<img style="cursor:hand" src='<%=strImagePath%>/close_icon.png' height="20" width="20" alt="${varClose}" onClick="window.close();" />
		</td>
	</tr>
</table>
 <div id="jasperItem"> 
 <jsp:include page="/operations/logistics/GmInhouseLoanerAckPrint.jsp" >
				<jsp:param value="YES" name="hHideButton"/>
			</jsp:include>		
			<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
 <jsp:include page="/operations/logistics/GmInhouseLoanerItemDtlPrint.jsp" />
			
</div> 
<%if(strAction.equals("ViewLetterVer")){%>
<iframe id="retItem" src="/GmPrintCreditServlet?hAction=PRINT&hideBtn=Y&hRAID=<%=strReturnId%>" frameborder="0" width="100%" height="200%" scrolling="no"> 
</iframe>
<%}%>
</FORM>
<script type="text/javascript">
//fnBreakEveryDiv();
</script>
</BODY>
</HTML>