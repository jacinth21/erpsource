
<%
/**********************************************************************************
 * File		 		: GmIncludeLoanerEditParts.jsp
 * Desc		 		: This screen is used to add remove parts when reconfiguring a loaner
 * Version	 		: 1.0
 * author			: Brinal
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import="com.globus.valueobject.common.GmDataStoreVO"%> 

<%@ taglib prefix="fmtIncludeLoanerEditParts" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ include file="/common/GmHeader.inc" %>

<!--operations\logistics\GmIncludeLoanerEditParts.jsp -->
<%

//String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale="_"+strLocale;
}
//GmDataStoreVO gmDataStoreVO = (GmDataStoreVO)request.getAttribute("gmDataStoreVO");
String strLocaleCompany = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strLocaleCompany);
String strPartLotFl=GmCommonClass.parseNull(gmResourceBundleBean.getProperty("LOANER_PART_LOT"));
%>

<fmtIncludeLoanerEditParts:setLocale value="<%=strLocale%>"/>
<fmtIncludeLoanerEditParts:setBundle basename="properties.labels.operations.logistics.GmIncludeLoanerEditParts"/>

<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/gmjsp-taglib.tld" prefix="gmjsp"%>
<style type="text/css">

input.hide { font-family: Verdana, Arial, Helvetica, sans-serif; font-size:
10 px; color: #003068; text-decoration: none; background-color: #e4e6f2;
border-color: #88A0C8 #88A0C8 #88A0C8; border-style: solid;
border-top-width: 0 px; border-right-width: 0 px; border-bottom-width: 0
px; border-left-width: 0 px}
a{text-decoration: none;}
a:hover{text-decoration: none;}
</style>

<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	ArrayList alSetLoad = (ArrayList)request.getAttribute("CONSIGNSETDETAILS");
	ArrayList alRplnFrm = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("alRplnFrm"));
	ArrayList alMoveTo = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("alMoveTo"));
	
	HashMap hmCsg = (HashMap)request.getAttribute("hmCONSIGNMENT");
	if (hmCsg == null)
	{
		hmCsg = (HashMap)session.getAttribute("hmCONSIGNMENT");
	}
	String strloansflcd = GmCommonClass.parseNull((String)hmCsg.get("LOANSFLCD"));
	
	HashMap hmTemp = new HashMap();	

	int intSize = 0;
	int intSizeDd = 0;
	HashMap hcboVal = null;
	HashMap hcboValDd = null;
	String strShipTo = "";	
	String strChecked = "";	
	String strTodaysDate = (String)session.getAttribute("strSessTodaysDate")==null?"":(String)session.getAttribute("strSessTodaysDate");
	String strCbo_Repfrom ="";
	String strCbo_MoveTo ="";
	String strPartNum="";
	String strCodeID = "";
	String strCodeName = "";
	int intSetQty =0;
	int intSetListQty=0;
	int intBOQty=0;
	String strColor="black";
	String strColSpan = (strPartLotFl.equals("YES")  && !strloansflcd.equals("5") && !strloansflcd.equals("0") && !strloansflcd.equals("10"))?"11":"10";
	String strCboOpt = GmCommonClass.parseNull((String)request.getParameter("COMBO_OPT"));
	String strImgPath = GmCommonClass.parseNull((String)request.getParameter("STR_IMG_PATH"));
	String strSpiltDisableFl = "";

%>
<%
 for(int i=0; i<alRplnFrm.size();i++)
 {
	 HashMap hmRplnfrm = (HashMap) alRplnFrm.get(i);
	 String strID= (String)hmRplnfrm.get("CODEID");
%>
 <input type="hidden" name="strRplnFrm_<%=strID%>"	value=""> 
<%
}
%>

<%
 for(int i=0; i<alMoveTo.size();i++)
 {
	 HashMap hmMoveTo = (HashMap) alMoveTo.get(i);
	 String strID= (String)hmMoveTo.get("CODEID");
%>
 <input type="hidden" name="strMoveTo_<%=strID%>"	value=""> 
<%
}
%>
<script>
var lotFlag ='<%=strPartLotFl%>';
var loansflcd='<%=strloansflcd%>';
var cbo_type = '<%=strCboOpt%>';
</script>
<table border="0" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="2">
			<div style="overflow:auto; height:400px;">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				  <thead>
					<tr style="position:relative; top:expression(this.offsetParent.scrollTop);"><td colspan="<%=strColSpan%>" height="1" bgcolor="#666666"></td></tr>
					<tr class="ShadeRightTableCaption" style="position:relative; top:expression(this.offsetParent.scrollTop);">
						<td width="200" height="25"><fmtIncludeLoanerEditParts:message key="LBL_PART"/></td>
						<td width="300"><fmtIncludeLoanerEditParts:message key="LBL_DESCRIPTION"/></td>
						<!-- Reconfigure Lot Screen -->
						<%if(strCboOpt.equals("LOTRECONFIG")){ %>
							<td align="center" width="60" class="RightText"  bgcolor="#c3c5b8"><fmtIncludeLoanerEditParts:message key="LBL_SET_LIST_QTY"/></td>
							<td align="center" width="70" class="RightText"   bgcolor="#feb469" ><fmtIncludeLoanerEditParts:message key="LBL_QTY_IN_SET"/></td>
							<td align="center" width="70" class="RightText"   bgcolor="#feb469" ><fmtIncludeLoanerEditParts:message key="LBL_RECONFIG_QTY"/></td>
							<td align="center" width="150" class="RightText"   bgcolor="#ccdd99" ><fmtIncludeLoanerEditParts:message key="LBL_LOT_RMV"/></td>
							<td align="center" width="200" class="RightText"   bgcolor="#a0b3de" ><fmtIncludeLoanerEditParts:message key="LBL_NEW_LOT"/></td>
						<% }else{%>
							<td align="center" width="60" class="RightText"   bgcolor="#feb469" ><fmtIncludeLoanerEditParts:message key="LBL_QTY_IN_SET"/></td>
							<% if (!strloansflcd.equals("5") && !strloansflcd.equals("0") && !strloansflcd.equals("10"))  { %>
									<td align="center" width="60" class="RightText" bgcolor="#feb469" ><fmtIncludeLoanerEditParts:message key="LBL_QTY_MISSING"/></td>
							<% } %>
							<% if(strPartLotFl.equals("YES") && !strloansflcd.equals("5") && !strloansflcd.equals("0") && !strloansflcd.equals("10")){ %>
							<td align="center" width="180" class="RightText"  bgcolor="#feb469"><fmtIncludeLoanerEditParts:message key="LBL_USED_LOT"/></td>
							<% } %>
							<td align="center" width="60" class="RightText"  bgcolor="#c3c5b8"><fmtIncludeLoanerEditParts:message key="LBL_SET_LIST_QTY"/></td>
							<td align="center" width="60" class="RightText"  bgcolor="#a0b3de"><fmtIncludeLoanerEditParts:message key="LBL_REPLENISH_FORM"/></td>
							<!-- td class="Line" width="1"></TD-->
							<td align="center" width="60" class="RightText"  bgcolor="#a0b3de"><fmtIncludeLoanerEditParts:message key="LBL_AVAILABLE_QTY"/></td>
							<td align="center" width="60" class="RightText"  bgcolor="#a0b3de"><fmtIncludeLoanerEditParts:message key="LBL_QTY_TO_REPLENISH"/></td>
							<td align="center" width="60" class="RightText" bgcolor="#ccdd99" ><fmtIncludeLoanerEditParts:message key="LBL_MOVE_TO"/></td>
							<td align="center" width="60" class="RightText" bgcolor="#ccdd99"><fmtIncludeLoanerEditParts:message key="LBL_QTY_TO_REMOVE"/></td>
						<% }%>
					</tr>
					<tr><td colspan="11" height="1" bgcolor="#666666"></td></tr>
					</thead>
					<tbody>
<%
						intSize = alSetLoad.size();
						if (intSize > 0)
						{
							hcboVal = new HashMap();
							for (int i=0;i<intSize;i++)
							{
								hcboVal = (HashMap)alSetLoad.get(i);
								strCbo_Repfrom = "Cbo_Repfrom"+i;
								strCbo_MoveTo = "Cbo_MoveTo"+i;
								strPartNum = "hPartNum"+i;
								intSetQty = Integer.parseInt(GmCommonClass.parseZero((String)hcboVal.get("QTY")));
								intSetListQty=Integer.parseInt(GmCommonClass.parseZero((String)hcboVal.get("SETLISTQTY")));
								intBOQty = Integer.parseInt(GmCommonClass.parseZero((String)hcboVal.get("BOQTY")));
								if((intSetQty+intBOQty) != intSetListQty){
									strColor = "red";
								}else{
									strColor = "black";
								}
								
								if(strCboOpt.equals("LOTRECONFIG") && intSetQty <= 1){
								  strSpiltDisableFl = "none";
								}else{
								  strSpiltDisableFl = "";
								}
%>						
						<%if(strCboOpt.equals("LOTRECONFIG")){ %>
							<tr id="tr<%=i%>">
						<% }else{%>
							<tr id="tr<%=i%>"  onMouseover="fnProcessCheckRowHighlight(tr<%=i%>,<%=i%>)"  onMouseout="fnProcessCheckRestoreBgColor(tr<%=i%>,<%=i%>)" >
						<% }%>
												
							<td width="200" class="RightText" height="20" id="COLUMN1<%=i%>">&nbsp;
					  	<a  id="splitId<%=i%>" href="javascript:fnSplit('<%=i%>','<%=(String)hcboVal.get("PNUM")%>');"> 
					  		<img src="<%=strImagePath%>/plus.gif" width="12" height="12" border="0" ></a><font color="<%=strColor%>">
							<%=(String)hcboVal.get("PNUM")%></font>					
							<input type="hidden" name="hPartNum<%=i%>" value="<%=(String)hcboVal.get("PNUM")%>" id ="<%=i%>" />
							<input type="hidden" name="hpnum<%=i%>" value="<%=(String)hcboVal.get("PNUM")%>" id ="<%=i%>" />
							<input type="hidden" name="hSetQty<%=i%>" value="<%=(String)hcboVal.get("QTY")%>" id ="<%=i%>" />
							<input type="hidden" name="hPrice<%=i%>" value="<%=(String)hcboVal.get("PRICE")%>" id ="<%=i%>"/>
							<input type="hidden" name="hStock<%=i%>" value="" id ="<%=i%>"/>	
							<input type="hidden" name="hBOQty<%=i%>" value="<%=(String)hcboVal.get("BOQTY")%>" id ="<%=i%>"/>
							<input type="hidden" name="hExpandCnt<%=i%>" value=""/>						
							</td>
							<td class="RightText" width="250" wrap  id="COLUMN2<%=i%>">&nbsp;<font color="<%=strColor%>"><%=GmCommonClass.getStringWithTM((String)hcboVal.get("PDESC"))%></font></td>
							<!-- Reconfigure Lot Screen -->
							<%if(strCboOpt.equals("LOTRECONFIG")){ %>
								<td align="center" id="COLUMN5<%=i%>" class="RightText"><font color="<%=strColor%>"><%=(String)hcboVal.get("SETLISTQTY")%></font>
								<input type="hidden" name="hSetListQty<%=i%>" value="<%=(String)hcboVal.get("SETLISTQTY")%>" id ="<%=i%>"/></td>	
								<td align="center" class="RightText" bgcolor="#ffecb9"  id="COLUMN3<%=i%>"><font color="<%=strColor%>"><%=(String)hcboVal.get("QTY")%></font></td>
								<td align="center" class="RightText" bgcolor="#ffecb9" id="COLUMN4<%=i%>">
									<input type="text" name="Txt_Recon_Qty<%=i%>" value="" size="3" class="RightText" id ="<%=i%>" onBlur="fnPartLotAjax(<%=i%>,hPartNum<%=i%>.value);">
									</td>
								
							        <td align="center" id="COLUMN111<%=i%>"  class="RightText"  bgcolor="#e1f3a6">
							        <select name="Cbo_UseLot<%=i%>" id="Cbo_UseLot<%=i%>" style="width: 150px;" onchange="javascript:fnChangeTxtDisable(<%=i%>,hPartNum<%=i%>.value);">
								<option value="0" ><fmtIncludeLoanerEditParts:message key="LBL_CHOOSE_ONE"/></option>
								</select>
							        </td>
								<td  align="center" class="RightText"  bgcolor="#e4e6f2" id="COLUMN8<%=i%>">
									<input type="text" name="Txt_New_Lot<%=i%>" value="" size="20" class="RightText" id ="<%=i%>" onclick="javascript:fnToggleLot(<%=i%>)" onblur="javascript:fnValidateLot(<%=i%>,hPartNum<%=i%>.value)">
								<span id="DivShowTagAvl<%=i%>" style="vertical-align:middle; display:none;"> <img height="16" width="19" src="<%=strImgPath%>/success.gif"></img></span>
				 				<span id="DivShowTagNotExists<%=i%>" style="vertical-align:middle;display: none;"> <img  height="12" width="12" src="<%=strImgPath%>/delete.gif"></img></span>
				 				<input type="hidden" name="hInvalidLotFl<%=i%>" value="" id="hInvalidLotFl<%=i%>""/>	
									</td>
	
								
							<% }else{%>
								<td align="center" class="RightText" bgcolor="#ffecb9"  id="COLUMN3<%=i%>"><font color="<%=strColor%>"><%=(String)hcboVal.get("QTY")%></font></td>
								<% if (!strloansflcd.equals("5") && !strloansflcd.equals("0") && !strloansflcd.equals("10") && !strPartLotFl.equals("YES"))  { %>
									<td align="center" class="RightText" bgcolor="#fef2cc" id="COLUMN4<%=i%>">
									<input type="text" name="Txt_Miss<%=i%>" value="" size="3" class="RightText" id ="<%=i%>">
									</td>
								<% }%>
								<%if (!strloansflcd.equals("5") && !strloansflcd.equals("0") && !strloansflcd.equals("10") && strPartLotFl.equals("YES"))  { %>
								<td align="center" class="RightText" bgcolor="#fef2cc" id="COLUMN4<%=i%>">
									<input type="text" name="Txt_Miss<%=i%>" value="" size="3" class="RightText" id ="<%=i%>" onBlur="fnPartLotAjax(<%=i%>,hPartNum<%=i%>.value);">
									</td>
								<%} %>
								<% if(strPartLotFl.equals("YES") && !strloansflcd.equals("5") && !strloansflcd.equals("0") && !strloansflcd.equals("10")){ %>
							        <td align="center" id="COLUMN111<%=i%>"  class="RightText"  bgcolor="#ffecb9">
							        <select name="Cbo_UseLot<%=i%>" id="Cbo_UseLot<%=i%>" style="width: 150px;" class="RightText" onchange="javascript:fnCheckLotNum(<%=i%>,hPartNum<%=i%>.value)">
								<option value="0" ><fmtIncludeLoanerEditParts:message key="LBL_CHOOSE_ONE"/></option>
								</select>
							        </td>
							     <% } %>	
								<td align="center" id="COLUMN5<%=i%>" class="RightText"><font color="<%=strColor%>"><%=(String)hcboVal.get("SETLISTQTY")%></font>
								<input type="hidden" name="hSetListQty<%=i%>" value="<%=(String)hcboVal.get("SETLISTQTY")%>" id ="<%=i%>"/></td>		
								
								<td align="center" id="COLUMN6<%=i%>" class="RightText" nowrap bgcolor="#e4e6f2" >&nbsp;
								<select name="Cbo_Repfrom<%=i%>" id="Cbo_Repfrom<%=i%>" class="RightText"  onChange="javascript:fnCallAjax(hPartNum<%=i%>.value,this.value,<%=i%>);" id ="<%=i%>" >
								<option value="0" ><fmtIncludeLoanerEditParts:message key="LBL_CHOOSE_ONE"/></option>
								<% intSizeDd = alRplnFrm.size();
									hcboValDd = new HashMap();
									for (int j = 0; j < intSizeDd; j++) {
										hcboValDd = (HashMap) alRplnFrm.get(j);
										strCodeID = (String) hcboValDd.get("CODEID");
										strCodeName = (String) hcboValDd.get("CODENM");
									
								%>
								<option value="<%=strCodeID%>"><%=strCodeName%></option>
								<%
									}
								%>							 
								</select>				
								</td>
								<td align="center" id="COLUMN7<%=i%>" class="RightText" bgcolor="#e4e6f2" >  <input type="text" name="Txt_Stock<%=i%>" value="" size="5" class="hide" id ="<%=i%>" READONLY> </td>
								<td align="center" id="COLUMN8<%=i%>" class="RightText" bgcolor="#e4e6f2"><input type="text" name="Txt_Rep<%=i%>" value="" size="3" class="RightText" id ="<%=i%>"  onBlur=javascript:fnCheckRepType('<%=i%>','<%=(String)hcboVal.get("PNUM")%>'); ></td>
								<td align="center" id="COLUMN9<%=i%>" class="RightText" bgcolor="#ecffaf"><gmjsp:dropdown controlName="<%=strCbo_MoveTo%>" seletedValue="" value="<%=alMoveTo%>" codeId="CODEID"  codeName="CODENM"   defaultValue= "[Choose One]" /></td>
								<td align="center" id="COLUMN10<%=i%>" class="RightText" bgcolor="#e1f3a6"><input type="text" name="Txt_Remove<%=i%>" value="" size="3" class="RightText" id ="<%=i%>" onBlur="javascript:fnCheckMoveTo('<%=i%>','<%=(String)hcboVal.get("PNUM")%>');" ></td>
							<% }%>						
						</tr>
						<tr><td bgcolor="#eeeeee" colspan="9" height="1"></td></tr>					
<%
							} // end of FOR
						}else {
%>
						<tr><td colspan="9" height="50" align="center" class="RightTextRed"><BR><fmtIncludeLoanerEditParts:message key="MSG_NO_DATA"/> !</td></tr>
<%
						}		
%>
					
					</tbody>
				</table>
			 </div>
			</td>
			<input type="hidden" name="hCnt" value="<%=intSize%>">
		</tr>	
		<tr><td colspan="2" height="1" class="Line"></td></tr>
	</table>