<%
/**********************************************************************************
 * File		 		: GmPartFulfillReport.jsp
 * Desc		 		: This screen is used to display Part Fulfill Report
 * Version	 		: 1.0
 * author			: Lakshmi Madabhushanam
************************************************************************************/
%>
<%@ page language="java" %>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ include file="/common/GmHeader.inc" %> 

<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>


<%@ taglib prefix="fmtPartFulfillReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmPartFulfillReport.jsp -->

<fmtPartFulfillReport:setLocale value="<%=strLocale%>"/>
<fmtPartFulfillReport:setBundle basename="properties.labels.operations.logistics.GmPartFulfillReport"/>


<!-- Imports for Logger -->
 <bean:define id="alSets" name="frmLogisticsReport" property="alSets" type="java.util.ArrayList"> </bean:define>
 
<%
	String strWikiTitle = GmCommonClass.getWikiTitle("PART_FULFILL");
	String strBackOrderType = (String)request.getParameter("BackOrderType") == null?"NA":(String)request.getParameter("BackOrderType");
	String partsNum="NA";

if(strBackOrderType.equals("NA")==false){
	partsNum = (String)request.getParameter("hPartNum") == null?"":(String)request.getParameter("hPartNum");
	//System.out.println("PartNum"+partsNum1);
}

String strGridData = GmCommonClass.parseNull((String)request.getAttribute("strXml"));
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Part Fulfill -- Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script> 
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script>

var objGridData = '<%=strGridData%>';
var lblType = '<fmtPartFulfillReport:message key="LBL_TYPE"/>';
function fnOnPageLoad(){
	gridObj = initGrid('dataGrid',objGridData);
		
	}


function fnBackOrder()
 { 
 var BackOrderType='<%=strBackOrderType%>';
 var inputString = "";
 if (BackOrderType!='NA' && SwitchUserFl !='Y')
 {
	
 	SelAll = document.all.selectAll;
  	SelAll.checked = true;
  	QtyFlag=document.all.fulfillQtyFlag
	QtyFlag.checked = true;
	fnSelectAll('selectAll');
	// to get the parent windows - parts(Window operner - param - max allow 2000 char morethan 2000 char not load)
	var partsObj = window.opener.document.getElementById("hPartNums");
	var parts = '';
	if(partsObj == undefined || partsObj == null){
		parts = '<%=partsNum%>';
	}else{
		parts = partsObj.value;
	 	parts = parts.substr(0,parts.length-1);
	}
	// to set the parent windows parts
	document.frmLogisticsReport.partNum.value= parts;
   
 	if (BackOrderType=='50261')//Sales
 	{
 	
 	document.all.checkType.value='50240';
 	}
 	if (BackOrderType=='50263')//Item
 	{
 	document.all.checkType.value='50242';
 	}
 	if (BackOrderType=='50262')//Set Need
 	{
 	document.all.checkType.value='50241';
 	}
 
  if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
  document.frmLogisticsReport.go.disabled = true;		
  document.body.style.cursor = 'wait';
  document.frmLogisticsReport.fulfillQtyFlag.value = "true";
  // create a set string
  inputString = fnCheckSelections();
  document.frmLogisticsReport.hSetId.value = inputString;
  
  document.frmLogisticsReport.submit();
  }
 }
function fnGo()
 {
	var inputString="";
	inputString = fnCheckSelections();
	fnValidateDropDn('checkType',lblType);
  if (ErrorCount > 0)
		 {
			Error_Show(); 
			Error_Clear();					
			return false;			
		}
		else{		
		  document.frmLogisticsReport.go.disabled = true;	
		  document.frmLogisticsReport.hSetId.value = inputString;
		  	
		  document.body.style.cursor = 'wait';
		  document.frmLogisticsReport.fulfillQtyFlag.value = "true";
		  fnStartProgress('Y');
		  document.frmLogisticsReport.submit();
		  
		  }
 }
 
 function fnCheckSelections()
{
	 var chkdvals=""; ;
	 var unchkdvals ="";
	 var chk=0;	
	 var notchk=0;
	 var inputString="";

	objCheckSetArrLen = 0;
	setflag  = '';
	
	objCheckSetArr = document.frmLogisticsReport.checkSelectedSets;
	var part = document.frmLogisticsReport.partNum.value;
	if(objCheckSetArr) 
	{
		objCheckSetArrLen = objCheckSetArr.length;
		for(i = 0; i < objCheckSetArrLen; i ++) 
		{	
		    if(objCheckSetArr[i].checked)
		    {				    		   
				setflag = '1';
				chkdvals += objCheckSetArr[i].value+',';			
				chk ++;			
			
		    }else
		    {
		    	unchkdvals += objCheckSetArr[i].value+',';
				notchk ++;
		    }
		}
		
		 if(parseInt(objCheckSetArrLen)==parseInt(chk))
		 {
			 inputString="ALL|";
		 }else
		 {
			 if(parseInt(notchk)<parseInt(chk))
			 {
				 inputString="UNCHECKED|"+unchkdvals;
				 inputString=inputString.substring(0,inputString.length-1);
				 
			 }else
			 {
				 inputString="CHECKED|"+chkdvals;
				 if(chkdvals !='')
				 	inputString=inputString.substring(0,inputString.length-1);
			 }
		 }

		 if(inputString.length>4000)
		 {
			 Error_Details(message_operations[517]);	
		 }
		
		if(setflag=="")
		 {		 
			 document.frmLogisticsReport.hsetstatusNotcheck.value="true";
		 }
		 else
		 { 
		 	 document.frmLogisticsReport.hsetstatusNotcheck.value="";
		 }
		 
	}
	
	if(setflag=='' && part=='')
	{
	Error_Details(message_operations[518]);
	}
	return inputString;
}	

function fnSelectAll(varCmd)
{				
				objSelAll = document.all.selectAll;
				if (objSelAll ) {
				objCheckSetArr = document.all.checkSelectedSets;
					
				if ((objSelAll.checked || varCmd == 'selectAll') && objCheckSetArr)
				{
					objSelAll.checked = true;
					objCheckSetArrLen = objCheckSetArr.length;
					
					objCheckSetArr1 = document.frmLogisticsReport.checkSelectedSets;
					
					if (objCheckSetArr1.type == 'checkbox')
						{
							objCheckSetArr1.checked = true;
						}
													
					for(i = 0; i < objCheckSetArrLen; i ++) 
					{	
						objCheckSetArr[i].checked = true;
						
					}	 
				}
				else if (!objSelAll.checked  && objCheckSetArr ){
					objCheckSetArrLen = objCheckSetArr.length;
					
					objCheckSetArr1 = document.frmLogisticsReport.checkSelectedSets;
					if (objCheckSetArr1.type == 'checkbox')
						{
							objCheckSetArr1.checked = false;
						}
					
					for(i = 0; i < objCheckSetArrLen; i ++) 
					{	
						objCheckSetArr[i].checked = false;
					}
			
					}
				}
	}
	

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnBackOrder();fnOnPageLoad();">
<html:form action="/gmLogisticsReport.do?method=loadPartFulfillReport" >
	<html:hidden property="strOpt" value="" />
	<html:hidden property="hSetId" value=""/>
	<html:hidden property="hsetstatusNotcheck" />
	<jsp:include page="/common/GmScreenError.jsp"/>
	
	<table border="0" width="950" class="DtTable950" cellspacing="0"
		cellpadding="0" style="border-width: 1px ; border-style: solid;border-color: #000000">

		<tr>
			<td height=25 class=RightDashBoardHeader colspan=5><fmtPartFulfillReport:message key="LBL_PART_FULFILL_REPORT"/></td>
			<td align="right" class=RightDashBoardHeader><fmtPartFulfillReport:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit'
				style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16'
				height='16'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>

		<tr class=Line>
			<td noWrap height=1 colspan=6></td>
		</tr>
		<tr>
			<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtPartFulfillReport:message key="LBL_SET"/>:</td>
			<td width="900">&nbsp; &nbsp;
			<DIV
				style="display: visible; height: 100px; overflow: auto; border-width: 1px; border-style: solid; margin-left: 1px; margin-right: 2px;">
			<table >
				<tr>
					<td bgcolor="gainsboro" width="900"><html:checkbox property="selectAll"
						onclick="fnSelectAll('toggle');" /><B><fmtPartFulfillReport:message key="LBL_SELECT_ALL"/> </B></td>
				</tr>
			</table>

			<table>
				<fieldset><logic:iterate id="selectedSetlist"
					name="frmLogisticsReport" property="alSets">
					<tr>
						<td><htmlel:multibox property="checkSelectedSets"
							value="${selectedSetlist.ID}" /> <bean:write
							name="selectedSetlist" property="NAME" /></td>
					</tr>
				</logic:iterate></fieldset>
			</table>
			</DIV>
			<BR>
			</td>
			

			<td width="1000" height="100" valign="top">

			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td height="25"></td>
				</tr>
				<!-- Custom tag lib code modified for JBOSS migration changes -->
				<tr>
					<td class="RightTableCaption" align="right" HEIGHT="24"><fmtPartFulfillReport:message key="LBL_TYPE"/> &nbsp;</td>

					<td align="left"><gmjsp:dropdown controlName="checkType"
						SFFormName="frmLogisticsReport" SFSeletedValue="checkType"
						SFValue="alType" defaultValue="[Choose One]" codeId="CODEID"
						codeName="CODENM" /></td>
				</tr>

				<TR>
					<td class="RightTableCaption" align="right" HEIGHT="24"><fmtPartFulfillReport:message key="LBL_PART_#"/>: &nbsp;</td>
					<td align="left"><html:text property="partNum" size="50"
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');" />&nbsp;</td>
				</tr>
				<TR>
				   <td></td>
					<td align="left">
					<html:checkbox property="fulfillQtyFlag" /><fmtPartFulfillReport:message key="LBL_READY_TO_FULFILL"/>  &nbsp;&nbsp; 
					<fmtPartFulfillReport:message key="BTN_LOAD" var="varLoad"/>
					<gmjsp:button value="&nbsp;${ varLoad}&nbsp;" onClick="javascript:return fnGo();" name="go" gmClass="button" buttonType="Load" /></td>					
				</tr>
			</table>
			</td>
		</tr>
		
		<%if(!strGridData.equals("")){ %>
		<tr class=Line>
			<td noWrap height=1 colspan=6></td>
		</tr>
		<tr>
			<td colspan="6">
				<div  id ="dataGrid" style="height:400px;"></div>
			</td>
		</tr>
		<tr>
                <td colspan="6" align="center">
                <div class='exportlinks'><fmtPartFulfillReport:message key="DIV_EXPORT_OPT"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"><fmtPartFulfillReport:message key="DIV_EXCEL"/>
                                  </a>| <img src='<%=strImagePath%>/pdf_icon.gif' />&nbsp;<a href="#"
                                onclick="gridObj.toExcel('/phpapp/pdf/generate.php');"><fmtPartFulfillReport:message key="DIV_PDF"/> </a></div>
                </td>
		</tr>
		<%} %>
		</table>
    </html:form>
    <%@ include file="/common/GmFooter.inc" %>
    
</BODY>

</HTML>
