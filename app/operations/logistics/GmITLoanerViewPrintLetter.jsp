<%
/**********************************************************************************
 * File		 		: GmITLoanerViewPrintLetter.jsp
 * Desc		 		: This screen is used for printing letter and print version for Italy
 * author			: rajan
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>


<%@ taglib prefix="fmtLoanerViewPrintLetter" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmLoanerViewPrintLetter.jsp -->

<fmtLoanerViewPrintLetter:setLocale value="<%=strLocale%>"/>
<fmtLoanerViewPrintLetter:setBundle basename="properties.labels.operations.logistics.GmLoanerViewPrintLetter"/>


<style type="text/css">
    a {text-decoration: none;}
</style>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	String strCssPath1 = GmCommonClass.getString("GMSTYLES");
		
	HashMap hmCsg = new HashMap();
	
	HashMap hmConsignDtls = new HashMap();
	HashMap hmParam = new HashMap();
	hmParam = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmParamPrint"));
	hmConsignDtls = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmParamDtls"));
	HashMap[] hmConsign = new HashMap[hmParam.size()];
	ArrayList[] alConsignDtls = new ArrayList[hmConsignDtls.size()];
	
	String strType;
	String strPrintAll = GmCommonClass.parseNull((String)request.getAttribute("printAll"));
	String strLoanerStatusFlag = GmCommonClass.parseNull((String)request.getAttribute("STRLOANERSTATUSFLAG"));
	String strLoanerPaperworkFlag = GmCommonClass.parseNull((String)request.getAttribute("STRLOANERPAPERWORKFLAG"));
	if(strPrintAll.equals("PrintAll")){
		
		
		for(int i=0;i<hmParam.size();i++){
			hmConsign[i]= GmCommonClass.parseNullHashMap((HashMap)hmParam.get("hmCONSIGNMENT"+i));
		}
		
		for(int i=0;i<hmConsignDtls.size();i++){
			alConsignDtls[i]= GmCommonClass.parseNullArrayList((ArrayList)hmConsignDtls.get("CONSIGNSETDETAILS"+i));
		}
		strType = GmCommonClass.parseNull((String)request.getAttribute("hType"));
	}
	else{
	// To get the Consignment information 
		hmCsg = (HashMap)request.getAttribute("hmCONSIGNMENT");
		if (hmCsg == null)
		{
			hmCsg = (HashMap)session.getAttribute("hmCONSIGNMENT");
		}
		//String strLoanDate = GmCommonClass.parseNull((String)hmCsg.get("LDATE"));
		//String strExpDate = GmCommonClass.parseNull((String)hmCsg.get("EDATE"));
		String strBillNm = GmCommonClass.parseNull((String)hmCsg.get("BILLNM"));
		
		strType = GmCommonClass.parseNull((String)request.getAttribute("hType"));
	}
	////String strHeader  =	strType.equals("40050")?"HOSPITAL CONSIGNMENT AGREEMENT":"LOANER ACKNOWLEDGEMENT LETTER";
	String strPrintHeight=(strLoanerPaperworkFlag.equals("YES"))?"100":"30";
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Globus Medical:Loaner Set - Print Version and Loaner Letter</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath1%>/Globus.css"/>
<link rel="stylesheet" href="<%=strCssPath1%>/print.css" type="text/css" media="print" />
<script>
var printAll = '<%=strPrintAll%>';
function fnPrint()
{
	window.print();
}
function hidebuttons()
{

	strObjectCls=document.all.Btn_Cls;
	strObjectCls.style.display='none';
	strObjectPrt=document.all.Btn_Prt;
	strObjectPrt.style.display = 'none';
//	divObj=document.getElementById("Duplicate");
//	divObj.style.display='none';
	window.print();
}
function hidePrintMain()
{
//	divObj=document.getElementById("Duplicate");
//	divObj.style.display='block';
	buttonTdObj=document.getElementById("buttonTd");
	buttonTd.style.display='none';

}

function showPrintMain()
{
//	divObj=document.getElementById("Duplicate");
//	divObj.style.display='none';
	buttonTdObj=document.getElementById("buttonTd");
	buttonTd.style.display='block';
	
}
</script>
</head>
<BODY topmargin="10" onload="hidebuttons()" onbeforeprint="hidePrintMain();" onafterprint="showPrintMain();">
<form>
<table border="0" width="700" cellspacing="0" cellpadding="0" align=center>

<% if(strPrintAll.equals("PrintAll")){%>
	
				<p STYLE="page-break-after: always"></p>
	<%for(int i=0;i<hmParam.size();i++){
		String strCType = GmCommonClass.parseNull((String)hmConsign[i].get("CTYPE"));
		request.setAttribute("hmCONSIGNMENT",hmConsign[i]);
		if(i==0){
			if(strType.equals("4119")){%>
				<jsp:include page="/operations/logistics/GmInhouseLoanerAckPrint.jsp" >
				<jsp:param value="YES" name="hHideButton"/>
				</jsp:include>
				<p STYLE="page-break-after: always"></p>
		<%	}else{ %>
				<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
				<jsp:include page="/operations/logistics/GmLoanerAckPrint.jsp" />
				
				<p STYLE="page-break-after: always"></p>
		<%  }
		  } 
		if(strCType.equals("9110")){
			request.setAttribute("HMRESULT", hmConsign[i]);
			%>
			<div>
				<jsp:include page="/operations/logistics/GmInhouseLoanerItemDtlPrint.jsp" />
				
			</div>
			<%	
		}else{
		request.setAttribute("CONSIGNSETDETAILS",alConsignDtls[i]); 
		%>
		<div>
			<jsp:include page="/operations/logistics/GmITLoanerSetPrint.jsp" >
			<jsp:param name="hType" value="<%=strType%>" />
			</jsp:include>
		</div>
		<%}
		if(i<hmParam.size()-1){%>
		<p STYLE="page-break-after: always"></p>
		<%} %>
	<%} %>

<%}else{ 
	    if(strType.equals("4119")){%>
			<jsp:include page="/operations/logistics/GmInhouseLoanerAckPrint.jsp" >
			<jsp:param value="YES" name="hHideButton"/>
			</jsp:include>
			<p STYLE="page-break-after: always"></p>
	  <%}else{ %>
			<jsp:include page="/operations/logistics/GmLoanerAckPrint.jsp" />
			
			<p STYLE="page-break-after: always"></p>
	  <%}%>	
	  <%if(strLoanerPaperworkFlag.equals("YES")) {%>	
			<div align="center" style="PADDING-TOP: 50px; PADDING-RIGHT: 30px">
				<jsp:include page="/operations/logistics/GmLoanerDetailList.jsp" >
				<jsp:param value="YES" name="hHideButton"/>
				</jsp:include>
			</div>	
	   <%}else{ %>
	   			<div>
				<jsp:include page="/operations/logistics/GmITLoanerSetPrint.jsp" />

			</div>
	   <%}%>		
			
	  <%}%>
<table border="0" width="700" cellspacing="0" cellpadding="0" align=center>
<%if(!strLoanerPaperworkFlag.equals("YES")) {%>	
		<tr><td class="Line" colspan="2"></td></tr>
		<%} %>
		<tr>
			<td  align="center" height="<%=strPrintHeight%>"  width="700" id="buttonTd">
			<fmtLoanerViewPrintLetter:message key="BTN_PRINT" var="varPrint"/>
				<gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_PrintMain" gmClass="button" onClick="fnPrint();" buttonType="Load"  />&nbsp;&nbsp;
				<fmtLoanerViewPrintLetter:message key="BTN_CLOSE" var="varClose"/>
				<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_CloseMain" gmClass="button" onClick="window.close();" buttonType="Load" />
			</td>
		<tr>
</table>

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>