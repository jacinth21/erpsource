 <%
/**********************************************************************************
 * File		 		: GmSetBuildSetup.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Rakhi Gandhi
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib prefix="fmtSetReconfig" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmSetReconfig.jsp -->
<fmtSetReconfig:setLocale value="<%=strLocale%>"/>
<fmtSetReconfig:setBundle basename="properties.labels.custservice.ProcessRequest.GmProcessRequestItems"/>
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction") == null?"":(String)request.getAttribute("hAction");
	String strSource = (String)request.getAttribute("CONFIGSOURCE") == null?"":(String)request.getAttribute("CONFIGSOURCE");

	String strType = "" ;
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strProjId =	"";
	String strSetId = "";
	String strSetNm = "";
	String strDesc = "";
	String strTemp = "";
	String strShade = "";
	String strConsignId = "";
	String strSetName= "";
	String strAccName = "";
	String strUserName = "";
	String strIniDate = "";
	String strItemQty = "";
	String strControl = "";
	String strPartNum = "";
	String strPartDesc = "";
	
	String strWareHouse = "";
	String strQty = "";
	String strFlag = "";
	String strCheckedOpt = "";
	String strVerFlag = "";
	String strLastUpdNm = "";
	String strLastUpdDt = "";
	String strQtyInBulk = "";
	String strCritFl = "";
	String strCritQty = "";
	String strCritType = "";
	
	String strTypeCtrlName ="";
	String strRelToCtrlName ="";

	ArrayList alSetList = new ArrayList();
	ArrayList alSetLoad = new ArrayList();
	ArrayList alMissParts = new ArrayList();
	ArrayList alSimilarSets = new ArrayList();

	HashMap hmConsignDetails = null;
	HashMap hmLists = null;
	HashMap hmParts = null;
	ArrayList alActionType =null;
	ArrayList alActionTarget =null;
	ArrayList alAddParts = null;
	
	int intMissPartsSize = 0;
	int intSimSize = 0;
	int intAddPartsSize = 0;
	boolean blWipSet = true;
	
	
	hmLists = (HashMap)request.getAttribute("hmLists");
	
	if( hmLists != null )
	{		
		alActionType = (ArrayList)hmLists.get("RELTYPE");		
		alActionTarget = (ArrayList)hmLists.get("RELTRG");
		//out.println(alActionType.size());
	}

	
	if (hmReturn != null)
	{
		alSetList = (ArrayList)hmReturn.get("SETLIST");
		if (strhAction.equals("LoadSet")|| strhAction.equals("EditLoad") )
		{
			alSetLoad = (ArrayList)hmReturn.get("SETLOAD");
			alMissParts = (ArrayList)hmReturn.get("MISSINGPARTS");
			alSimilarSets = (ArrayList)hmReturn.get("SIMILARSETS");
			if (alMissParts != null)
			{
				intMissPartsSize = alMissParts.size();
			}

			if (alSimilarSets != null)
			{
				intSimSize = alSimilarSets.size();
			}
			
			strSetId = (String)request.getAttribute("hSetId") == null?"":(String)request.getAttribute("hSetId");
			if (strhAction.equals("EditLoad") )
			{
				hmConsignDetails = (HashMap)hmReturn.get("CONDETAILS");
				strConsignId = (String)hmConsignDetails.get("CID");
				strSetName= (String)hmConsignDetails.get("SNAME");
				strSetId = (String)hmConsignDetails.get("SETID");
				strAccName = (String)hmConsignDetails.get("ANAME");
				strUserName = (String)hmConsignDetails.get("UNAME");
				strIniDate = (String)hmConsignDetails.get("CDATE");
				strDesc = (String)hmConsignDetails.get("COMMENTS");
				strLastUpdNm = (String)hmConsignDetails.get("LUNAME");
				strLastUpdDt = (String)hmConsignDetails.get("LUDATE");
				strFlag = (String)hmConsignDetails.get("SFL");
				
				strChecked = strFlag.equals("2")?"checked":"";
				strVerFlag = (String)hmConsignDetails.get("VFL");
				if (strVerFlag.equals("1"))
				{
					strChecked = "checked";
				}
				else
				{
					strChecked = "";
				}
				
				if( ( strFlag.equals("2")   ) && strVerFlag.equals("1") ) {
					blWipSet = false;
				}
				
				hmParts = (HashMap)request.getAttribute("addParts");
				if(hmParts != null )
				{
					
					alAddParts = (ArrayList) hmParts.get("ADDPARTS");
					intAddPartsSize = alAddParts.size();
					//out.println(intAddPartsSize);
				}
				
				
			}
		}
	}

	int intSize = 0;
	HashMap hcboVal = null;
	
	String strAccessId = (String)session.getAttribute("strSessAccLvl") ==null?"":(String)session.getAttribute("strSessAccLvl");
	String strDeptId = 	(String)session.getAttribute("strSessDeptId") == null?"":(String)session.getAttribute("strSessDeptId");
	String strUserId = (String)session.getAttribute("strSessUserId") == null?"":(String)session.getAttribute("strSessUserId");
	int intAccId = Integer.parseInt(strAccessId);
	boolean editFl = false;
	if (strDeptId.equals("Z") || (strDeptId.equals("L") && intAccId > 2 ))
	{
		editFl = true;
	}

	
	String strWikiTitle = GmCommonClass.getWikiTitle("RECONFIG_CONS");
	  
	String strReqID = (String)request.getAttribute("REQID") ;
%>
 
<HTML>
<HEAD>
<TITLE> Globus Medical: Set Reconfigure </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script>
var cnt = 0;

function fnSubmit()
{
	Error_Clear();
	hcnt = parseInt(document.frmVendor.hCnt.value);
	hcnt = hcnt+cnt;
	document.frmVendor.hAction.value = 'Save';
	document.frmVendor.hCnt.value = hcnt;
  	val = document.frmVendor.hEditFl.value;
	val1 = document.frmVendor.hStatusFl.value;
	var strErrorMore = '';
	var theinputs = document.getElementsByTagName("input");
    var arr = document.frmVendor.pnums.value.split(',');
    var blk = '';
    var cnum = '';
    var qtycnt = 0;
    hcnt++;
    for(var i=0;i<arr.length;i++)
    {
       qid = arr[i];
       //pnum1 = qid.replace('.','');
       pnum1 = qid.replace(/\./g,"");
       blk = eval("document.frmVendor.Qty_Blk"+pnum1);
              
		    for(var j=0;j<hcnt;j++)
		    {
				pnumobj = eval("document.frmVendor.hPartNum"+j);
				if (pnumobj)
				{
					if (pnumobj.value == qid)
					{
						qtyobj = eval("document.frmVendor.Txt_Qty"+j);
						cnumobj = eval("document.frmVendor.Txt_CNum"+j);
						if(!isNaN(qtyobj.value))
						{
							qty = parseInt(qtyobj.value);
							cnum = cnumobj.value;
							if (qty >= 0 && cnum != '')
							{
								qtycnt = qtycnt + qty;
							}
						}
						else
						{
							alert(message[5573]);
						}
					}
				}
			}

		if (qtycnt > parseInt(blk.value) )
		{
			//alert("More than bulk:"+qid+":"+qtycnt);
			strErrorMore  = strErrorMore + ","+ qid;
		}
		qtycnt = 0;
	}
	
	if (strErrorMore != '')
	{
		Error_Details(Error_Details_Trans(message[5574],strErrorMore));
	}

	if (val1 == '2')
	{
		if (val == 'false')
		{
			alert(message[5575]);
			return false;
		}
		else
		{
			if (ErrorCount > 0)
			{
				Error_Show();
				return false;
			}else{		
			document.frmVendor.submit();
			}
		}
	}else{
		if (ErrorCount > 0)
		{
			Error_Show();
			return false;
		}else{	
			document.frmVendor.submit();
		}
	}
}


function fnSetCheckVer(val)
{
	if (val.checked == false)
	{
		document.frmVendor.hVerifyFl.value = "0";
	}
	else
	{
		document.frmVendor.hVerifyFl.value = "1";
	}
}


function fnUpdate(v )
{
	//var varmattyepid = document.frmManualAdjTxn.maTypeId.value;
	var len = "Cbo_Action".length ; 
	index  = v.substring ( len ) ;
	name =  "Cbo_ReleaseTo"+index ;
	ob = eval("document.frmVendor.Cbo_ReleaseTo"+index);
	obj = eval("document.frmVendor.Cbo_Action"+index);
	var strWareHouse ='';
	if(eval("document.frmVendor.hWareHouse"+index))
		strWareHouse = eval("document.frmVendor.hWareHouse"+index).value;
	var source = '<%=strSource%>';

	if(  <%=blWipSet%> == true ) 
	{
	if(source=='part'){
		if(strWareHouse == '56001')
			ob.value = "90822";
		else
			ob.value = "90805";
	}else{
		ob.value = "90806";  
		}
	}
	
	if(obj.value =="0") 
	 	ob.value = "0";  
}


function fnLoadSet(obj)
{
	document.frmVendor.hAction.value = 'LoadSet';
	document.frmVendor.hSetId.value = obj.value;
  	document.frmVendor.submit();
}

function fnSplit(val,pnum)
{

	cnt++;
	var hcnt = parseInt(document.frmVendor.hCnt.value);
	hcnt = hcnt+cnt;

	var NewQtyHtml = "<span id='Sp"+cnt+"'><BR>&nbsp;&nbsp;&nbsp;<input type='hidden' name='hPartNum"+hcnt+"' value='"+pnum+"'><input type='text' size='3' value='' name='Txt_Qty"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#AACCE8'); onBlur=changeBgColor(this,'#ffffff'); >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' size='9' value='' name='Txt_CNum"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#AACCE8'); onBlur=changeBgColor(this,'#ffffff'); >&nbsp;<a href=javascript:fnDelete("+cnt+");><img src=<%=strImagePath%>/btn_remove.gif border=0></a></span>";
	var QtyObj = eval("document.all.Cell"+val);
	QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
}

function fnDelete(val)
{
	obj = eval("document.all.Sp"+val);
	obj.innerHTML = "";
}

function fnInitiate()
{
	document.frmVendor.hAction.value = 'Initiate';
  	document.frmVendor.submit();
}


function fnCallOptional()
{
	document.frmVendor.hAction.value = 'EditLoad';

	if (document.frmVendor.Chk_Opt.checked == false)
	{
		document.frmVendor.hMode.value = 'SET';
	}
	else
	{
		document.frmVendor.hMode.value = 'OPT';
	}
  	document.frmVendor.submit();
}

function fnReconfigure()
{
	document.frmVendor.hAction.value = 'Reconfigure';
  	document.frmVendor.submit();
}

function fnReload()
{
	document.frmVendor.hAction.value = 'Reload';
  	document.frmVendor.submit();
}


function fnVoidSetCsg()
{
		document.frmVendor.action ="<%=strServletPath%>/GmCommonCancelServlet";
		document.frmVendor.hAction.value = "Load";
		document.frmVendor.submit();
}


function fnLoadReconfig(val)
{
	
	document.frmVendor.hAction.value = "ReloadReconfig";
	document.frmVendor.submit();
}




function fnSubmitReconfig()
{
	Error_Clear();
	
	
	var strErrorMore = '';
	var strQtyError = '';
	var strRWError = '';
	var strFGError = '';
	if( document.frmVendor.hConsignId.value != document.frmVendor.consignmentID.value )
	{
	    strErrorMore = message[5576];
	}
	
	hcnt = parseInt(document.frmVendor.hCnt.value);
	//alert(hcnt);
	hcnt = hcnt+cnt;
	
	document.frmVendor.hAction.value = 'SubmitReconfig';
	document.frmVendor.hCnt.value = hcnt;
	//alert(hcnt);
  	
	
	
    var arr = document.frmVendor.pnums.value.split(',');    
        
    hcnt++;
    var defSel = '';
	var isActionAvailable = false;
	
	for(var j=0;j<hcnt;j++)
	{
				pnumobj = eval("document.frmVendor.hPartNum"+j);
				if (pnumobj)
				{
						controlqtyobj = eval("document.frmVendor.hControlQty"+j);
						reconfqtyobj = eval("document.frmVendor.Txt_ReconfigQty"+j);
						defSel = '';
						
						if(eval("document.frmVendor.hWareHouse"+j))
			            	defSel = eval("document.frmVendor.hWareHouse"+j).value;
			            
						
						if(parseInt(reconfqtyobj.value,10) <= 0)
						 {
						    Error_Details(Error_Details_Trans(message[5577],pnumobj.value));
						 }
						if(!isNaN(reconfqtyobj.value) && !isNaN(controlqtyobj.value))
						{
							if (parseInt(controlqtyobj.value,10) < parseInt(reconfqtyobj.value,10)) 
							{					
								strQtyError = strQtyError + "," + pnumobj.value ; 
							}
						}
						else
						{
							alert(message[5573] + reconfqtyobj.value );
						}
						
						//check for dropdown selection
						obd = eval("document.frmVendor.Cbo_ReleaseTo"+j);
						v1 = obd.value ;  
						/*This code is disabled due to TKT-5985
		            	if(defSel == '56001' && (v1 != "90822" && v1 !=0)){
		            		strRWError += pnumobj.value + ",";
		            	}
		            	
		            	if(defSel == '90800' && (v1 != "90805" && v1 !=0)){
		            		strFGError += pnumobj.value + ",";
		            	}
		            	*/	
						obd1 = eval("document.frmVendor.Cbo_Action"+j);
						v2 = obd1.value ;  
						
						if( v2 != '0' && v1 == '0' ) 
						{						   
						  strErrorMore = strErrorMore +  pnumobj.value  + ",";  
						}
						
						if (v2 != '0' && v1 != '0')
						{
						 	isActionAvailable = true;
						}
						
						
				}
	}
		
	//alert(strErrorMore);
	
	 if( strQtyError != '')
	{
	Error_Details(Error_Details_Trans(message[5578],strQtyError));
	}
	else if (strErrorMore != '')
	{
	  Error_Details(Error_Details_Trans(message[5579],strErrorMore));
	}
	/*This code is disabled due to TKT-5985
	 if( strRWError != '')
	{
		Error_Details(" Following Part(s) "+ strRWError + " cannot be release to other inventory except RW");
	}
	 if( strFGError != '')
	{
		Error_Details(" Following Part(s) "+ strFGError + " cannot be release to other inventory except FG");
	}
	*/
	//..For add parts
	//..
	strErrorMore = '';
	
	haddpartscnt = parseInt(document.frmVendor.hAddPartsCnt.value);
	
	
	for(var j=0;j<haddpartscnt;j++)
	{
				pnumobj = eval("document.frmVendor.hAddPartNo"+j);
				if (pnumobj)
				{
						pendingqtyobj = eval("document.frmVendor.hPendingQty"+j);
						reconfqtyobj = eval("document.frmVendor.txt_addpartqty"+j);
						
						if(!isNaN(reconfqtyobj.value))
						{
							if ( parseInt(pendingqtyobj.value,10) < parseInt(reconfqtyobj.value,10) ) 
							{
								strErrorMore = strErrorMore + "," + pnumobj.value ; 
							}
							else
							{
							  isActionAvailable = true;
							}
						  	
						}
						else
						{
							alert(message[5573] + reconfqtyobj.value);
						}
				}
	}
	
	if( strErrorMore != '' )
	{
	Error_Details(Error_Details_Trans(message[5580],strErrorMore));
	}
		
    if(!isActionAvailable && ErrorCount == 0)
    {
      Error_Details(message[5581]);
    }		
	//..	    
    //..
    	
	if (ErrorCount > 0)
	{
		Error_Show();
		return false;
	}else
	{	
	    document.frmVendor.action ="/gmRuleEngine.do";
	    document.frmVendor.RE_FORWARD.value="gmSetBuildServlet";
	    document.frmVendor.RE_TXN.value="SETRECONFIGWRAPPER";
	    document.frmVendor.txnStatus.value="VERIFY";
	    fnStartProgress('Y');
		document.frmVendor.submit();
	}
	
}



function fnSubmitAddParts()
{
	Error_Clear();	
	document.frmAddParts.hAction.value = 'SubmitReconfigAddParts';
	var strErrorMore = '';
	
	//..
	
	haddpartscnt = parseInt(document.frmAddParts.hAddPartsCnt.value);
	
	
	for(var j=0;j<haddpartscnt;j++)
	{
				pnumobj = eval("document.frmAddParts.hAddPartNo"+j);
				if (pnumobj)
				{
						pendingqtyobj = eval("document.frmAddParts.hPendingQty"+j);
						reconfqtyobj = eval("document.frmAddParts.txt_addpartqty"+j);
						
						if(!isNaN(reconfqtyobj.value))
						{
							if ( pendingqtyobj.value < reconfqtyobj.value ) 
							{
								strErrorMore = strErrorMore + "," + pnumobj.value ; 
							}
						}
						else
						{
							alert(message[5573] + reconfqtyobj.value);
						}
				}
	}
	
	if( strErrorMore != '' )
	{
	Error_Details(Error_Details_Trans(message[5582],strErrorMore));
	}
		
	//..
	
	if (ErrorCount > 0)
	{
		Error_Show();
		return false;
	}else
	{	
		document.frmAddParts.submit();
	}
	
}


function fnDisableDropDown()
{
var source = '<%=strSource%>';
		hcnt = parseInt(document.frmVendor.hCnt.value);
		hcnt = hcnt+cnt;
    if(  <%=blWipSet%> == true ) {
    		ob1 = eval("document.frmVendor.Cbo_allReleaseTo");
    		if(source=='part'){
    		ob1.remove(3);
    		ob1.remove(2);		
    		}else{
			ob1.remove(1);
			ob1.remove(3); // remove the Release to RW
			ob1.remove(2); // remove the Release to Quarantine
			}
		for(var j=0;j<=hcnt;j++)
		{		
			ob = eval("document.frmVendor.Cbo_ReleaseTo"+j);
			if(source=='part'){	
			//alert('607');	
			ob.remove(3);  
			ob.remove(2);  		
			}else {	
			ob.remove(1);
			ob.remove(3); // remove the Release to RW
			ob.remove(2); // remove the Release to Quarantine 
			 
			}
		}
	  }
	  else{  
	  		ob1 = eval("document.frmVendor.Cbo_allReleaseTo");
			ob1.remove(2);
		for(var j=0;j<=hcnt;j++)
		{
			ob = eval("document.frmVendor.Cbo_ReleaseTo"+j);
			ob.remove(2);
		}
	  	
	  }
	  
	 
}

function fnApplyAllAction()
{
 // alert("selectedApplyAll is " + document.frmMaterialRequest.cbo_allAction.selectedIndex);
  
 var selectedApplyAll = document.frmVendor.Cbo_allAction.selectedIndex;
 obj2 = eval("document.frmVendor.Cbo_allAction");
 var size="<%=alSetLoad.size()%>";
 var source = '<%=strSource%>';
var defSel = '';
 for (var i=0;i<size ;i++ )
      {
             
            obj = eval("document.frmVendor.Cbo_Action"+i);              
             
               obj.options[selectedApplyAll].selected = true;
             
            }
            
      if(  obj2.value == "0") 
      {
            obj1 = eval("document.frmVendor.Cbo_allReleaseTo");
            obj1.value = 0;
            for (var i=0;i<size ;i++ )
      {
             
            obj = eval("document.frmVendor.Cbo_ReleaseTo"+i);                 
               obj.value = 0;  
            }
      }     
             
      if(  <%=blWipSet%> == true  && obj2.value != "0") 
      {
            obj1 = eval("document.frmVendor.Cbo_allReleaseTo");
            if(source=='part'){
            	obj1.value = "90805";
            }else{
            	obj1.value = "90806";
            }
            for (var i=0;i<size ;i++ )
      {   
            obj = eval("document.frmVendor.Cbo_ReleaseTo"+i);
            if(eval("document.frmVendor.hWareHouse"+i))
            	defSel = eval("document.frmVendor.hWareHouse"+i).value;
            
            if(source=='part'){
            	if(defSel == '56001')
         	  		obj.value = "90822";
            	else
            		obj.value = "90805";
            }else{
           		obj.value = "90806";
            }
                 
             
            }
       
      }     
       
}

function fnApplyAllReleaseTo()
{
 // alert("selectedApplyAll is " + document.frmVendor.cbo_allAction.selectedIndex);
  
 var selectedApplyAll = document.frmVendor.Cbo_allReleaseTo.selectedIndex;
 
 var size="<%=alSetLoad.size()%>";
 for (var i=0;i<size ;i++ )
	{
		 
		obj = eval("document.frmVendor.Cbo_ReleaseTo"+i);			
		 
		   obj.options[selectedApplyAll].selected = true;
		 
		}
	 
}

</script>


</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnDisableDropDown();">


<input type="hidden" name="hAddPartsSize" value="<%=intAddPartsSize%>">

	<table class="DtTable850"  cellspacing="0" cellpadding="0">		
		<tr><td>
		 <table cellspacing="0" cellpadding="0">
			<td height="25" class="RightDashBoardHeader"><fmtSetReconfig:message key="LBL_RECONFIG_TO_REQUEST"/></td>
			<td align="right" colspan="8" class=RightDashBoardHeader > 
				<fmtSetReconfig:message key="LBL_HELP" var="varHelp"/>	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
			</table>
		</td> </tr>
		<tr><td bgcolor="#666666" height="1" ></td></tr>
		<tr>
			<td height="100" valign="top">
				<html:form action="/gmRequestHeader.do">	
				<jsp:include page="/gmRequestHeader.do" flush="true">
				 <jsp:param name="FORMNAME" value="frmRequestHeader" />
				<jsp:param name="requestId" value="<%=strReqID%>"/>
				 <jsp:param name="hRequestView" value="headerView" />
		 		</jsp:include>
		 		</html:form>
		 	</td>
		</tr>
		<tr>
			<td>
		 <FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmSetBuildServlet">
		<input type="hidden" name="hAction" value="<%=strhAction%>">
		<input type="hidden" name="hConsignId" value="<%=strConsignId%>">
		<input type="hidden" name="hSetId" value="<%=strSetId%>">
		<input type="hidden" name="hMode" value="SET">
		<input type="hidden" name="hStatusFl" value="<%=strFlag%>">
		<input type="hidden" name="hVerifyFl" value="">
		<input type="hidden" name="hEditFl" value="<%=editFl%>">
		<input type="hidden" name="hTxnId" value="<%=strConsignId%>">
		<input type="hidden" name="hCancelType" value="VDSCN">
		<input type="hidden" name="hOpt" value="<%=strSource%>">
		<input type="hidden" name="RE_FORWARD" >
		<input type="hidden" name="RE_TXN" >
		<input type="hidden" name="txnStatus" >
		

<%   				  
		if (strhAction.equals("LoadReconfig") ||strhAction.equals("EditLoad"))
		{						 
%>		 
		 	<table border="0" width="850"  cellspacing="0" cellpadding="0">
					<tr>
						<td align="right" class="RightTableCaption">&nbsp;<fmtSetReconfig:message key="LBL_CONSIGNMENT_ID"/>:</td>
						 <td colspan="3">&nbsp;    		        	
    		        	<input type="text" size="15" value="<%=strConsignId%>" name="consignmentID" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >
    		        	<fmtSetReconfig:message key="BTN_LOAD" var="varLoad"/>
    		        	<gmjsp:button value="  ${varLoad}" gmClass="button"  onClick="fnLoadReconfig();"  buttonType="Save" />&nbsp;&nbsp;
    		        	</td>
    		        	<td width="45%"></td>
					</tr>						
				</table>
<%
		}
%>				
			</td>
		</tr>	
<%
					if (strhAction.equals("LoadSet")|| strhAction.equals("EditLoad") )
					{
%>
		<tr><td>
							<table cellspacing="0" cellpadding="0" border="0"  id="myTable">
								<tr><td colspan="8" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td width="70" height="25"><fmtSetReconfig:message key="LBL_PART_NUMBER"/></td>
									<td width="350"><fmtSetReconfig:message key="LBL_DESCRIPTION"/></td>
									<td width="50" align="center"><fmtSetReconfig:message key="LBL_sET_QTY"/></td>									
									<td  width="120" align="center">&nbsp; <fmtSetReconfig:message key="LBL_INSERT_QTY"/></td>
									<td width="70"><fmtSetReconfig:message key="LBL_CONTROL_NUM"/></td>
									<td width="50" align="center"><fmtSetReconfig:message key="LBL_QTY_RECONFIGURE"/></td> 
									<td width="150" align="center"> <fmtSetReconfig:message key="LBL_ACTION"/> </td>
									<td width="150" align="center"> <fmtSetReconfig:message key="LBL_RELEASE_TO"/> </td>
								</tr>
								<tr class="ShadeRightTableCaption">
									<td width="70" height="25"> </td>
									<td width="350"> </td>
									<td width="50" align="center"> </td>									
									<td  width="120" align="center">&nbsp;  </td>
									<td width="70"> &nbsp;&nbsp;&nbsp; </td>
									<td width="50" align="center"> </td> 
									<!-- Custom tag lib code modified for JBOSS migration changes -->
									<td width="150" align="center"> <gmjsp:dropdown controlName="Cbo_allAction"   	seletedValue="<%=strType%>"
											value="<%=alActionType%>" codeId = "CODEID"  codeName = "CODENM"   onChange="fnApplyAllAction()" defaultValue= "[Choose One]"/>
								
									  </td>
									<td width="150" align="center">    <gmjsp:dropdown controlName="Cbo_allReleaseTo"   seletedValue="<%=strType%>" onChange="fnApplyAllReleaseTo()"
											value="<%=alActionTarget%>" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]"/>	 
									   </td>
								</tr>
								<tr><td class=Line colspan=8 height=1></td></tr>
<%
						intSize = alSetLoad.size();
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();
							HashMap hmTempLoop = new HashMap();

							String strNextPartNum = "";
							int intCount = 0;
							String strColor = "";
							String strLine = "";
							String strPartNumHidden = "";
							int intSetQty = 0;
							boolean bolQtyFl = false;
							String strReadOnly = "";
							StringBuffer sbPartNum = new StringBuffer();
							String strQtyUsed = "";
							int intQtyUsed = 0;
							int intQtyBulk = 0;
							
							for (int i=0;i<intSize;i++)
							{
								hmLoop = (HashMap)alSetLoad.get(i);
								if (i<intSize-1)
								{
									hmTempLoop = (HashMap)alSetLoad.get(i+1);
									strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("PNUM"));
								}
								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
								sbPartNum.append(strPartNum);
								sbPartNum.append(",");
								strPartNumHidden = strPartNum;
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strWareHouse = GmCommonClass.parseNull((String)hmLoop.get("WHTYPE"));
								strQtyUsed = GmCommonClass.parseZero((String)hmLoop.get("QTYUSED"));
								intQtyUsed = Integer.parseInt(strQtyUsed);

								strQty = GmCommonClass.parseZero((String)hmLoop.get("QTY"));
								intSetQty = Integer.parseInt(strQty);
								//bolQtyFl = intSetQty > 1?true:false;
								strQtyInBulk = GmCommonClass.parseZero((String)hmLoop.get("QTYBULK"));
								intQtyBulk = Integer.parseInt(strQtyInBulk);
								intQtyBulk = intQtyBulk + intQtyUsed; // Adding the current Set's allocated qty
								strQtyInBulk = ""+intQtyBulk;
								strTemp = strPartNum;
								strCritQty = GmCommonClass.parseZero((String)hmLoop.get("CRITQTY"));

								if (strPartNum.equals(strNextPartNum))
								{
									intCount++;
									strLine = "<TR><TD colspan=7 height=1 bgcolor=#eeeeee></TD></TR>";
								}

								else
								{
									strLine = "<TR><TD colspan=7 height=1 bgcolor=#eeeeee></TD></TR>";
									if (intCount > 0)
									{
										strPartNum = "";
										strPartDesc = "";
										strQty = "";
										strQtyInBulk = "";
										//strColor = "bgcolor=white";
										strLine = "";
										strCritQty = "";
									}
									else
									{
										//strColor = "";
									}
									intCount = 0;
								}

								if (intCount > 1)
								{
									strPartNum = "";
									strPartDesc = "";
									strQty = "";
									strQtyInBulk = "";
									//strColor = "bgcolor=white";
									strLine = "";
									strCritQty = "";
								}
							
								strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
								strItemQty = GmCommonClass.parseNull((String)hmLoop.get("IQTY"));
								strItemQty = strItemQty == ""?strQty:strItemQty;
								strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
								if (intSetQty > 1 || intSetQty == 0)
								{
									bolQtyFl = true;
									strReadOnly = "";
								}
								else
								{
									bolQtyFl = false;
									//strItemQty = "1";
									strReadOnly = "ReadOnly";
								}
								
								
								out.print(strLine);
%>
								<tr >
<%
								if ((strhAction.equals("EditLoad")  && bolQtyFl) || (strhAction.equals("EditLoadDash") && bolQtyFl))
								{
%>
									<td class="RightText" height="20">&nbsp;
									
									<%=strPartNum%>
									<input type="hidden" name="hPartNum<%=i%>" value="<%=strPartNumHidden%>"></td>
<%
								}else{
%>
									<td class="RightText" height="20">&nbsp;<%=strPartNum%>
									<input type="hidden" name="hPartNum<%=i%>" value="<%=strPartNumHidden%>"></td>
<%
								}
%>
									<td class="RightText"><%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									<td align="center" class="RightText"><%=strQty%>
									<input type="hidden" name="hSetQty<%=i%>" value="<%=strQty%>"></td>
									</td>
									<!-- td strCritQty-->
									<!-- crit type -->
									<!--   bulktd-->
<%
								if (strhAction.equals("EditLoad") )
								{
									
									strTypeCtrlName = "Cbo_Action" + i;
									strRelToCtrlName  = "Cbo_ReleaseTo" + i;
									
%>
									<td id="Cell<%=i%>" align="center" class="RightText">&nbsp;&nbsp;&nbsp;     <%=strItemQty%>
									 <input type="hidden" name="hControlQty<%=i%>" value="<%=strItemQty%>">
									 <input type="hidden" name="hWareHouse<%=i%>" value="<%=strWareHouse%>"></td> 
									 </td>
									
									<td id="CellControl<%=i%>">     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									 <%=strControl%>  
									 <input type="hidden" name="hControlNum<%=i%>" value="<%=strControl%>">
									</td>
									
									<!--  new input fields -->
									<td id="RCell<%=i%>" class="RightText">&nbsp;&nbsp;&nbsp;<input type="text" size="3" value="<%=strItemQty%>" name="Txt_ReconfigQty<%=i%>" class="InputArea" onkeypress="return fnNumbersOnly(event)"   onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									&nbsp;
									</td>
									
									<td>
									<gmjsp:dropdown controlName="<%=strTypeCtrlName%>"   	seletedValue="<%=strType%>"
											value="<%=alActionType%>" codeId = "CODEID"  codeName = "CODENM"   onChange="fnUpdate(this.name);" defaultValue= "[Choose One]"/>
									</td>
									
									
									<td>
									<gmjsp:dropdown controlName="<%=strRelToCtrlName%>"   seletedValue="<%=strType%>"
											value="<%=alActionTarget%>" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]"/>
									</td>
<%
								}else{
%>
									<td>&nbsp;</td>
<%
								}
%>
								</tr>
<%
								//out.print(strLine);
							}//End of FOR Loop
							out.print("<input type=hidden name=pnums value="+sbPartNum.toString().substring(0,sbPartNum.length()-1)+">");
						}else {
%>
						<tr><td colspan="7" height="50" align="center" class="RightTextRed"><BR><fmtSetReconfig:message key="LBL_NO_PART_NUMBER"/></td></tr>
<%
						}		
%>
				</table><input type="hidden" name="hCnt" value="<%=intSize-1%>">
			</td></tr>
<%					}
%>					


<% if ( ! strhAction.equals("LoadReconfig") ) {
		
%>
<input type="hidden" name="hAddPartsCnt" value="<%=intAddPartsSize%>">
<%
	if( intAddPartsSize > 0 ) {
		//return ; //hide second section
	//}
%>
<tr> <td >
<table cellspacing="0" cellpadding="0">
	<tr>			
			<td height="25" class="RightDashBoardHeader"><fmtSetReconfig:message key="LBL_SET_RECONFIG"/></td>			
	</tr>	
	<tr>
		<td height="100" valign="top">
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
	
	<tr><td colspan="8" bgcolor="#666666"></td></tr>
		<tr class="ShadeRightTableCaption">
		<td> <fmtSetReconfig:message key="LBL_PART_NUM"/> </td>
		<td> <fmtSetReconfig:message key="LBL_DESCRIPTION"/> </td>
		<td> <fmtSetReconfig:message key="LBL_SET_QTY"/> </td>
		<td><fmtSetReconfig:message key="LBL_PENDING_QTY"/></td>
		<td> <fmtSetReconfig:message key="LBL_BULK_QTY"/> </td>
		<td> <fmtSetReconfig:message key="LBL_QTY"/> </td>
		</tr>
	<tr><td colspan="8" height="1" bgcolor="#666666"></td></tr>
<%	
		HashMap hmTemp = null;

		if( intAddPartsSize > 0 && strhAction.equals("EditLoad") )		
		{
			for ( int i = 0 ; i < intAddPartsSize ; i++ )
			{
				
				hmTemp = (HashMap)alAddParts.get(i); 
				
%>
				<tr>
					<td> <%=(String) hmTemp.get("PNUM") %>
						 <input type="hidden" name="hAddPartNo<%=i%>" value="<%= hmTemp.get("PNUM")%>">  
					</td>
					
					<td><%=(String) hmTemp.get("PDESC") %>
					</td>
					
					<td> <%= hmTemp.get("QTY")  %> 
						 <input type="hidden" name="hAddPartSetQty<%=i%>" value="<%= hmTemp.get("QTY")%>">
					</td>
					
					<td> <%= hmTemp.get("PENDINGQTY")  %> 
						 <input type="hidden" name="hPendingQty<%=i%>" value="<%= hmTemp.get("PENDINGQTY")%>">
					</td>
					
					
					<td> <%= hmTemp.get("BULKQTY") %>
						 <input type="hidden" name="hBulk<%=i%>" value="<%= hmTemp.get("BULKQTY") %>">  <!--  todo hardcoded to 1 use bulk qtyhere -->
					 </td>
					<td colspan="3">&nbsp;    		        	
    		        	<input type="text" size="5" value="" name="txt_addpartqty<%=i%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >
    		        </td> 				
				</tr>					
<%				
			}
		}
%>			
</table>
</td></tr>

<% } //end if size > 0 %>


<!-- /FORM -->


<tr>
			<td> 
				<jsp:include page="/operations/logistics/GmBackorderReport.jsp" >
				<jsp:param name="hConsignmentId" value="<%=strConsignId%>" />				
				<jsp:param name="hRequestId" value="<%=strReqID%>" />
				</jsp:include>
			</td>
</tr>
 <tr> <td>
 <!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
    <jsp:include page="/common/GmRuleDisplayInclude.jsp"/>
    </td>
 </tr>
 <tr>
			<td align="center" height="25">		
			<fmtSetReconfig:message key="BTN_SUBMIT" var="varSubmit"/>	     		       
    		       	<gmjsp:button value="  ${varSubmit}" gmClass="button" buttonType="Save" onClick="fnSubmitReconfig();" />&nbsp;&nbsp;    		 
    		</td>
</tr>

</table>




<% } %>

<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
