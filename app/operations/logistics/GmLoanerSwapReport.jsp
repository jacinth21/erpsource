<%
/**********************************************************************************
 * File		 		: GmLoanerSwapReport.jsp
 * Created Date		: Aug 2010 	
 * author			: Velu
************************************************************************************/

%>
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtLoanerSwapReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- operations\logistics\GmLoanerSwapReport.jsp -->

<fmtLoanerSwapReport:setLocale value="<%=strLocale%>"/>
<fmtLoanerSwapReport:setBundle basename="properties.labels.operations.logistics.GmLoanerSwapReport"/>


<%
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
String strWikiTitle = GmCommonClass.getWikiTitle("LOANER_SWAP_REPORT");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Address Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmLoanerSwap.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>

<bean:define id="gridData" name="frmSetMapping" property="gridXmlData" type="java.lang.String"> </bean:define>
</HEAD>

<script>
	var objGridData;
	objGridData = '<%=gridData%>';
</script>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnOnPageLoad();">
<html:form action="/gmSetMappingAction.do" >
<html:hidden property="strOpt"  name="frmSetMapping" value=""/>
<html:hidden property="loanersetid"  name="frmSetMapping"/>
<html:hidden property="consignsetid"  name="frmSetMapping"/>


	<table border="0" class="GtTable700" cellspacing="0" cellpadding="0">
		
		<tr>
			<td height="25" class="RightDashBoardHeader" ><fmtLoanerSwapReport:message key="LBL_SET_MAPPING_REPORT"/></td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			<fmtLoanerSwapReport:message key="IMG_ALT_HELP" var = "varHelp"/>			
			<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
		       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		    </td>
		</tr>
		 <tr>
			<td colspan="4">
				<table border="0"  cellspacing="0" cellpadding="0">	
					<tr>
			           	<td colspan="4" height="50" width="700">
							<div id="employeedata" style="grid" height="450px"></div>
					    </td>
					 </tr>
				</table>
			</td>
		</tr>	
		
		<tr>
                <td colspan="4" align="center">
                <div class='exportlinks'><fmtLoanerSwapReport:message key="DIV_EXPORT_OPT"/>
                 : <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> <fmtLoanerSwapReport:message key="DIV_EXCEL"/> </a> </div>
                </td>
		</tr>
    </table>
    	     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>

