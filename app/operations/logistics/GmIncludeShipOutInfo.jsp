<%
/**********************************************************************************
 * File		 		: GmInHouseSetShip.jsp
 * Desc		 		: This screen is used to ship out In-House Sets
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.*,java.text.*" %>
<!-- \sales\AreaSet\GmAddInfo.jsp -->
 <%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ taglib prefix="fmtIncludeShipOutInfo" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>


<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale="_"+strLocale;
}
%>

<fmtIncludeShipOutInfo:setLocale value="<%=strLocale%>"/>
<fmtIncludeShipOutInfo:setBundle basename="properties.labels.operations.logistics.GmIncludeShipOutInfo"/>

<!-- operations\logistics\GmIncludeShipOutInfo.jsp -->

<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	
	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	ArrayList alLoanTo = new ArrayList();
	ArrayList alShipTo = new ArrayList();
	ArrayList alDistributor = new ArrayList();
	ArrayList alRepList = new ArrayList();
	ArrayList alAccList = new ArrayList();
	ArrayList alEmpList = new ArrayList();
	ArrayList alCarrier = new ArrayList();
	ArrayList alMode = new ArrayList();
	HashMap hmTemp = new HashMap();
	String strOpt = (String)session.getAttribute("strSessOpt") == null?"":(String)session.getAttribute("strSessOpt");
	if (hmReturn != null)
	{
		//hmTemp = (HashMap)hmReturn.get("SETCONLISTS");
		hmTemp = (HashMap)hmReturn.get("INHOUSESHIPLIST");
		alLoanTo = (ArrayList)hmTemp.get("LOANTO");
		alShipTo = (ArrayList)hmTemp.get("SHIPTO");
		alDistributor = (ArrayList)hmTemp.get("DISTRIBUTORLIST");
		alRepList = (ArrayList)hmTemp.get("REPLIST");
		alAccList = (ArrayList)hmTemp.get("ACCLIST");
		alEmpList = (ArrayList)hmTemp.get("EMPLIST");
		alCarrier = (ArrayList)hmTemp.get("DELCARR");
		alMode = (ArrayList)hmTemp.get("DELMODE");
	}	

	int intSize = 0;
	HashMap hcboVal = null;
	String strShipTo = "";
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Include Account Type </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmInHouseSetShip.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<SCRIPT>
var DistLen = <%=alDistributor.size()%>;
<%
	intSize = alDistributor.size();
	hcboVal = new HashMap();
	for (int i=0;i<intSize;i++)
	{
		hcboVal = (HashMap)alDistributor.get(i);
%>
	var DistArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
	}
%>


var RepLen = <%=alRepList.size()%>;
<%
	intSize = alRepList.size();
	hcboVal = new HashMap();
	for (int i=0;i<intSize;i++)
	{
		hcboVal = (HashMap)alRepList.get(i);
%>
	var RepArr<%=i%> ="<%=hcboVal.get("REPID")%>,<%=hcboVal.get("NAME")%>,<%=hcboVal.get("DID")%>";
<%
	}
%>

var AccLen = <%=alAccList.size()%>;
<%
	intSize = alAccList.size();
	hcboVal = new HashMap();
	for (int i=0;i<intSize;i++)
	{
		hcboVal = (HashMap)alAccList.get(i);
%>
	var AccArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>,<%=hcboVal.get("DID")%>";
<%
	}
%>

var EmpLen = <%=alEmpList.size()%>;
<%
	intSize = alEmpList.size();
	hcboVal = new HashMap();
	for (int i=0;i<intSize;i++)
	{
		hcboVal = (HashMap)alEmpList.get(i);
%>
	var EmpArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
	}
%>


</SCRIPT>
</HEAD>

	<table border="0" width="700" class="DtTable850" cellspacing="0" cellpadding="0">
		
		<tr><td colspan="3" height="1" bgcolor="#eeeeee"></td></tr>
		<tr class="ShadeRightTableCaption">
		<td class="Line" HEIGHT="20" width="1" ></td>
			<td HEIGHT="20" >&nbsp;<fmtIncludeShipOutInfo:message key="LBL_SHIPPING_DETAILS"/>:</td>
		<td class="Line" HEIGHT="20" width="1" ></td>	
		<tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
		</table>
	<table border="0" width="700" class="DtTable850" cellspacing="0" cellpadding="0">	
		<tr><td class="Line" HEIGHT="20" width="1" ></td>
			<td class="alignright" HEIGHT="23" align="right">&nbsp; <font color="red">*</font><fmtIncludeShipOutInfo:message key="LBL_SHIP_TO"/> :</td>
			<td>&nbsp;<select name="Cbo_ShipTo" id="Ship" class="RightText" tabindex="2" onChange="javascript:fnCallValues(this);"><option value="0" ><fmtIncludeShipOutInfo:message key="LBL_CHOOSE_ONE"/>
<%
			  		intSize = alShipTo.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alShipTo.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strShipTo.equals(strCodeID)?"selected":"";
%>
				<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>
				</select>
			
			&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">*</font> <fmtIncludeShipOutInfo:message key="LBL_VALUES"/> :&nbsp;
				<select name="Cbo_ValuesShip" class="RightText"  disabled tabindex="3"><option value="0" ><fmtIncludeShipOutInfo:message key="LBL_CHOOSE_ONE"/>
			</select> 
			</td>
			<td class="Line" HEIGHT="20" width="1" ></td>
		</tr>	
		<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
		<tr>
		<td class="Line" HEIGHT="20" width="1" ></td>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
			<td class="alignright" HEIGHT="20" align="right"><font color="red">*</font><fmtIncludeShipOutInfo:message key="LBL_CARRIER"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Carr"  seletedValue="<%= strShipTo %>" 	
					tabIndex="1"  width="150" value="<%= alCarrier%>" codeId = "CODEID" codeName = "CODENM" defaultValue= "[Choose One]"  />
			&nbsp;&nbsp;&nbsp;&nbsp; 
					<font color="red">*</font><fmtIncludeShipOutInfo:message key="LBL_MODE"/>:&nbsp;<gmjsp:dropdown controlName="Cbo_Mode"  seletedValue="<%= strShipTo %>" 	
					tabIndex="1"  width="150" value="<%= alMode%>" codeId = "CODEID" codeName = "CODENM" defaultValue= "[Choose One]"  />					
			</td>
			<td class="Line" HEIGHT="20" width="1" ></td>
		</tr>
		<tr><td>
	
			<input type="hidden" name="Cbo_ValuesCon" value="">
			<input type="hidden" name="Cbo_LoanToRep"  value="">
			<input type="hidden" name="Cbo_LoanToAcc" value="">
			
			</td>
		</tr>	
		<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>	
		
		<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>	
	</table>

