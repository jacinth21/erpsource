<%
	/**********************************************************************************
	 * File		 		: GmLoanerSetAcceptanceRpt.jsp
	 * Desc		 		: Loaner Set Acceptance Report 
	 * Version	 		: 1.0
	 * author			: Suganthi sharmila
	 ************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<!-- \custservice\GmLoanerSetAcceptanceRpt.jsp -->
<%@ page isELIgnored="false"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.Date"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%> 
<%@ taglib prefix="frmLoanerSetAcceptanceReport" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<frmLoanerSetAcceptanceReport:setLocale value="<%=strLocale%>" />
<frmLoanerSetAcceptanceReport:setBundle basename="properties.labels.operations.logistics.GmLoanerSetAcceptanceRpt" />
<bean:define id="gridData" name="frmLoanerSetAcceptanceReport" property="gridXmlData" type="java.lang.String" />
<bean:define id="alType" name="frmLoanerSetAcceptanceReport" property="alType" type="java.util.ArrayList" />
<bean:define id="alStatus" name="frmLoanerSetAcceptanceReport" property="alStatus" type="java.util.ArrayList" />
<bean:define id="strType" name="frmLoanerSetAcceptanceReport" property="strType" type="java.lang.String" />
<bean:define id="strStatus" name="frmLoanerSetAcceptanceReport" property="strStatus" type="java.lang.String" />
<%
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
String strWikiTitle = GmCommonClass.getWikiTitle("RFID_RECV_TAG");
String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
GmResourceBundleBean gmResourceBundleBean1 = GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
Date dtFrmDate = null;
%>

<html>
<head>
<title>Stock Transfer Report</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath %>/GmLoanerSetAcceptanceRpt.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>

<script>
	 var objGridData = '<%=gridData%>';
	 var date_format = '<%=strGCompDateFmt%>';
</script>

</head>

<body leftmargin="20" topmargin="10" onLoad="javascript:fnOnPageLoad();">

<html:form action="/gmLoanerSetAcceptanceRpt.do?method=loadLoanerSetAcceptanceDetails">
<html:hidden name="frmLoanerSetAcceptanceReport" property="strOpt" value="" /> 

	<table class="DtTable850" border="0" cellspacing="0" cellpadding="0" >
	<tr>
		<td height="25" class="RightDashBoardHeader" colspan="3">&nbsp;<frmLoanerSetAcceptanceReport:message key="LBL_LOANER_SET_REPORT"/></td>
		<td align="right" class=RightDashBoardHeader><frmLoanerSetAcceptanceReport:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
 		    title='${varHelp}' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
		</td>
		
	</tr>
	 <tr><td colspan="3" height="1"></td></tr>
		<tr class="oddshade">
			<td height="25" class="RightTableCaption" align="Right"><frmLoanerSetAcceptanceReport:message key="LBL_TYPE"/>:</td>
			<td width="300">&nbsp;<gmjsp:dropdown controlName="strType"  seletedValue="<%=strType%>" 	
					    value="<%=alType%>" codeId = "CODENMALT" codeName = "CODENM" onChange="fnLoadDrpDwn()"/></td>
			<td height="25" class="RightTableCaption" align="Right"><frmLoanerSetAcceptanceReport:message key="LBL_STATUS"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="strStatus"  seletedValue="<%=strStatus%>" 	
					   value="<%= alStatus%>" codeId = "CODENMALT" codeName = "CODENM" /> </td> 
				
					
	</tr>
	<tr><td colspan="3" height="1"></td></tr>
		<tr class="evenshade">
			<td class="RightTableCaption" align="right"><span style="color:red;"> * </span><frmLoanerSetAcceptanceReport:message key="LBL_STRT_DT" var="varFromDt"/><gmjsp:label type="RegularText" SFLblControlName="${varFromDt} :" td="false" /></td>
			<td class="RightTableCaption" align="left">&nbsp;<gmjsp:calendar SFFormName="frmLoanerSetAcceptanceReport" controlName="startDtFrom" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" /></td>
			<td class="RightTableCaption" align="right"><span style="color:red;"> * </span><frmLoanerSetAcceptanceReport:message key="LBL_END_DT" var="varToDt"/><gmjsp:label type="RegularText" SFLblControlName="${varToDt} :" td="false" /></td>
			<td class="RightTableCaption" align="left">&nbsp;<gmjsp:calendar SFFormName="frmLoanerSetAcceptanceReport" controlName="startDtTo" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
			&nbsp;&nbsp;&nbsp;<gmjsp:button value="Load" buttonType="Load" onClick="fnLoad();" align="right"/> </td>
		</tr>
	<%
		if (gridData.indexOf("cell") != -1) {
	%>
		<tr>
			<td colspan="6">
				<div id="LoanerSetAcceptanceReport" style="height: 500px; width: 850px"></div>
			</td>
		</tr>
		<tr>
			<td colspan="6" align="center">
				<div class='exportlinks'>
					<frmLoanerSetAcceptanceReport:message key="EXPORT_OPTS" />: <img src='img/ico_file_excel.png' onclick="fnExcel();" />&nbsp;
					<a href="#" onclick="fnExcel();"><frmLoanerSetAcceptanceReport:message key="EXCEL" /></a>
				</div>
			</td>
		</tr>
	<%
		} else {
	%>
		<tr>
			<td colspan="5" height="1" class="LLine"></td>
		</tr>
		<tr>
			<td colspan="5" align="center" class="RightText"><frmLoanerSetAcceptanceReport:message key="LBL_NO_DATA"/></td>
		</tr>
	<%}%>
	</table>
</html:form>
</body>
<%@ include file="/common/GmFooter.inc"%>
</html>