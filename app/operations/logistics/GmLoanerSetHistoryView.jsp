<%
/**********************************************************************************
 * File		 		: GmLoanerSetHistoryView.jsp
 * Desc		 		: This screen is used to display an Loaner Set's Details (InHouse and Product)
 * Version	 		: 1.0
 * author			: James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<%@ taglib prefix="fmtLoanerSetHistoryView" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmLoanerSetHistoryView.jsp -->

<fmtLoanerSetHistoryView:setLocale value="<%=strLocale%>"/>
<fmtLoanerSetHistoryView:setBundle basename="properties.labels.operations.logistics.GmLoanerSetHistoryView"/>


<%
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	ArrayList alSetLoad = (ArrayList)request.getAttribute("CONSIGNSETDETAILS");
	String strAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	String strScreenType = GmCommonClass.parseNull((String)request.getAttribute("hScreenType"));
	
	HashMap hmTemp = new HashMap();	

	int intSize = 0;
	HashMap hcboVal = null;
	String strShipTo = "";
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Loaner Refill History </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmLoanerSetHistoryView.js"></script>

</head>
<SCRIPT>
var tdinnner = '';
</SCRIPT>
<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmConsignSetServlet">
<input type="hidden" name="hAction" value ="">
<input type="hidden" name="Cbo_Action" value ="">
	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="2"><fmtLoanerSetHistoryView:message key="LBL_LOANER_SET_HISTORY"/></td>
		</tr>
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
		<tr>
			<td colspan="2">
				<jsp:include page="/include/GmIncludeConsignment.jsp" />
				
			</td>
		</tr>
		<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>	
		<tr>
			<td height="30" colspan="2"><a href="#" class="RightText" onClick="fnShow();"><b><fmtLoanerSetHistoryView:message key="LBL_SHOE_SET_DETAILS"/></b>
				<DIV style="display:none" id="filter"><BR>
				<jsp:include page="/include/GmIncludeConsignSetDetails.jsp" />
				
				</DIV>
			</td>
		</tr>
<%
		if (strAction.equals("ReloadHist"))
		{
%>
		<tr class="ShadeRightTableCaption">
			<td height="23" colspan="2"><fmtLoanerSetHistoryView:message key="LBL_REFILL_HISTORY_PARTNUMBER"/>	</td>
		</tr>
		<tr>
			<td colspan="2">
				 <display:table name="SETHISTORY" class="its" id="currentRowObject"> 
				 <fmtLoanerSetHistoryView:message key="DT_TRANS_ID" var="varTransID"/>
				    <display:column property="TRANSID" title="${varTransID}" sortable="true"  />
				     <fmtLoanerSetHistoryView:message key="DT_REASON" var="varReason"/>
				    <display:column property="PURPOSE" title="${varReason}" sortable="true"  />
				     <fmtLoanerSetHistoryView:message key="DT_CREATED_BY" var="varCreatedBy"/>
				     <display:column property="CUSER" title="${varCreatedBy}" sortable="true"  />
				     <fmtLoanerSetHistoryView:message key="DT_CREATED_DT" var="varCreatedDt"/>
				     <display:column property="CDATE" title="${varCreatedDt}" sortable="true"  />
					 <fmtLoanerSetHistoryView:message key="DT_PART_NUMBER" var="varPartNumber"/>
					 <display:column property="PNUM" title="${varPartNumber}" sortable="true"  />
					 <fmtLoanerSetHistoryView:message key="DT_CONTROL_NUMBER" var="varControlNumber"/>
					 <display:column property="CNUM" title="${varControlNumber}" sortable="true"  />
					 <fmtLoanerSetHistoryView:message key="DT_QTY" var="varQty"/>
					 <display:column property="QTY" title="${varQty}" sortable="true"  />
				     <fmtLoanerSetHistoryView:message key="DT_VERIFIED_BY" var="varVerifiedBy"/>
				     <display:column property="VUSER" title="${varVerifiedBy}" sortable="true"  />
				     <fmtLoanerSetHistoryView:message key="DT_VERIFIED_DT" var="varVerifiedDT"/>
				     <display:column property="VDATE" title="${varVerifiedDT}" sortable="true"  />
				     <fmtLoanerSetHistoryView:message key="DT_LOAN_TO" var="varLoanTo"/>
				     <display:column property="CNAME" title="${varLoanTo}" sortable="true"  />
				 </display:table> 
			</td>
		</tr>
<%
		}else if (strAction.equals("ReloadUsage")){
%>		
		<tr class="ShadeRightTableCaption">
			<td height="23" colspan="2"><fmtLoanerSetHistoryView:message key="LBL_USAGE_HISTORY"/></td>
		</tr>
		<tr>
			<td colspan="2">
				<jsp:include page="/operations/logistics/GmIncludeLoanerUsageDetails.jsp" >
					<jsp:param name="TYPE" value="<%=strAction%>" />
				</jsp:include> 
			</td>
		</tr>
		<% if(strScreenType.equals("loanerreconcile")){ %>
		<tr><td class="LLine" height="1" colspan="2"></td></tr>
		<tr></tr>
		<tr>
			<td colspan="2" align="center" id="buttondiv"> 
			 <fmtLoanerSetHistoryView:message key="BTN_CLOSE" var="varClose"/>
				<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close();" buttonType="Load" /> </td>
		</tr>
		<tr></tr>
		<%}%>
<%
		}
%>		
	</table>
</FORM>
</BODY>
<%@ include file="/common/GmFooter.inc" %>
</HTML>
