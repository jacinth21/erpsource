<%
/**********************************************************************************
 * File		 		: GmLoanerDashBoard.jsp
 * Desc		 		: This screen is used for Loaner DashBoard
 * author			: Vinoth Petchimuthu
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ taglib prefix="fmtLoanerDashBoard" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import ="com.globus.common.beans.PopChartUtils"%>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean" %>

<!-- logistics\GmLoanerDashBoard.jsp -->

<fmtLoanerDashBoard:setBundle basename="properties.labels.operations.logistics.GmLoanerDashBoard"/>

<%
String strLogisticsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_LOGISTICS");
String strWikiTitle = GmCommonClass.getWikiTitle("LOANER_DASHBOARD");
String strRefreshTime = GmCommonClass.parseZero(GmCommonClass.getString("LNDASHREFRESH"));
strRefreshTime = strRefreshTime.equals("0")?"900":strRefreshTime;

HashMap hmResult = new HashMap();
hmResult = (HashMap) request.getAttribute("HMRESULT");
String strOpen = "";
String strAllocated = "";
String strPendingPick = "";
String strPutAway = "";
String strPendingShip = "";
String strToOpenSameDay= "";
String strToPickSameDay = "";
String strToShipSameDay = "";
if (hmResult != null) {
	strOpen = GmCommonClass.parseNull((String) hmResult.get("OPEN"));
	strAllocated = GmCommonClass.parseNull((String) hmResult.get("ALLOCATED"));
	strPendingPick = GmCommonClass.parseNull((String) hmResult.get("PENDINGPICK"));
	strPutAway = GmCommonClass.parseNull((String) hmResult.get("PENDINGPUTAWAY"));
	strPendingShip = GmCommonClass.parseNull((String) hmResult.get("PENDINGSHIP"));
	strToOpenSameDay = GmCommonClass.parseNull((String) hmResult.get("OPENSAMEDAY"));
	strToPickSameDay = GmCommonClass.parseNull((String) hmResult.get("TOPICKSAMEDAY"));
	strToShipSameDay = GmCommonClass.parseNull((String) hmResult.get("TOSHIPSAMEDAY"));
}
%>


<html>
<head>
<title> Loaner Dashboard</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>


<meta http-equiv="refresh" content="<%=strRefreshTime%>"/>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxChart/dhtmlxchart.css"/>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css"></link>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css"></link>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxChart/dhtmlxchart.js" type="text/javascript"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxChart/dhtmlxchart.js" type="text/javascript"></script>
<script language="JavaScript" src="<%=strLogisticsJsPath%>/GmLoanerDashBoard.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/fusionchart/fusioncharts.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/fusionchart/fusioncharts.maps.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/fusionchart/fusioncharts.usa.js"></script>
<script>
$(document).ready(function(){
  $('.card').hover(
  //alert("hover");
     // trigger when mouse hover
	 function() {
	    $(this).animate({
		   marginTop: "-=2%",
		},200);
	 },
	 
	 // trigger when mouse out
	 function() {
	 $(this).animate({
		 marginTop: "0%",
		},200);
	 }
   );	 
 });
</script>
<style>
	.pr-3, .px-3 {
	padding-right: 0rem !important;
	}	
.dashheader {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;  
  padding: 0.75rem 1rem;
  margin-bottom: 1rem;
  list-style: none;
  background-color: #3172AA;
  border-radius: 0.25rem;
  font-size: 25px;	
  color: #FFFFFF;
}

.card:hover {
  box-shadow: -1px 8px 40px -12px rgba(0, 0, 0, 0.75);
}
.wiki {
    margin-left: 350px;
    margin-top: 12px;
}

.dash{
    width: 90%;
    display: inline-block;
    }
</style>

</head>
<body onload="fnChart(opensameday,topicksameday,toshipsameday);">
<html action="/gmLoanerDash.do?method=loadLoanerDash">
<input type="hidden" name="opensameday" value="<%=strToOpenSameDay%>">
<input type="hidden" name="topicksameday" value="<%=strToPickSameDay%>">	
<input type="hidden" name="toshipsameday" value="<%=strToShipSameDay%>">
<input type="hidden" name="open" value="<%=strOpen%>">
<input type="hidden" name="allocated" value="<%=strAllocated%>">	
<input type="hidden" name="putaway" value="<%=strPutAway%>">
<input type="hidden" name="pendingship" value="<%=strPendingShip%>">
<input type="hidden" name="pendingpick" value="<%=strPendingShip%>">
	
<div class="container" style="float:left; width: 65%; padding: 10px;">
  <div  class=" border w-100" style="background-color:#DCDCDC">
  <div class="dashheader">
	  <div class="dash"> <fmtLoanerDashBoard:message key="LBL_LOANER_DASHBOARD"/> </div>
	  <div style="display: inline-block;width: 10%;text-align: right;" >  <a style="cursor: hand;position: float;">
	  <span class="fa fa-question-circle" title='Help' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');"></span></a>	   
	  </div>
  </div>
  
  </div>
   <div class="container border w-100" style="background-color:#DCDCDC">
	<div class="row align-items-center pt-4 pl-4 pr-4 ">
		<div class="col pt-2 pr-2">
		<div class="card" >			
			  <div class="card-header" style="font-size:20; bold" style="position:relative">Open  </div>
			  <a style="cursor: hand;position: absolute; right:14px; top: 19px;"> </a>
			  <div class="card-body">				  
				<h1 class="card-title text-center text-primary"  onClick="fnOpenLoanerRequest('Open');" style="font-size:75; cursor: hand;"> <%=strOpen%> </h1>								
			  </div>
		</div>	
		</div>
		<div style="float:left; width:1px; padding: 10px;"></div>	
		<div class="col pt-2 pr-2">
			<div class="card " >
			  <div class="card-header" style="font-size:20; bold" onClick="fnReload('Open');">Allocated</div>
			   <a style="cursor: hand;position: absolute; right:14px; top: 19px;"></a>
			  <div class="card-body">
				<h1 class="card-title text-center text-primary" onClick="fnOpenLoanerRequest('Allocated');" style="font-size:75; cursor: hand;"> <%=strAllocated %> </h1>				
			  </div>
			</div>
		</div>
		<div style="float:left; width:1px; padding: 10px;"></div>			
		<div class="col pt-2 pr-2">
			<div class="card  " >
			  <div class="card-header" style="font-size:20; bold">To Put Away</div>
			   <a style="cursor: hand;position: absolute; right:11px; top: 19px;"></a>
			  <div class="card-body">
				<h1 class="card-title text-center text-primary" onClick="fnOpenLoanerRequest('ToPutAway');" style="font-size:75; cursor: hand;"> <%=strPutAway %> </h1>				
			  </div>
			</div>
		</div>
		<div style="float:left; width:1px; padding: 10px;"></div>	
		<div class="col pt-2 pr-2">
			<div class="card  " >
			  <div class="card-header" style="font-size:20; bold">To Pick</div>
			  <a style="cursor: hand;position: absolute; right:14px; top: 19px;"></a>
			  <div class="card-body">
				<h1 class="card-title text-center text-primary" onClick="fnOpenLoanerRequest('ToPick');" style="font-size:75; cursor: hand;"> <%=strPendingPick %> </h1>				
			  </div>
			</div>
		</div>
	 </div>
	<div class="row align-items-center pt-3 pb-5 pl-4 pr-4"> 		
		<div class="col-3 pt-4 pr-4">	
			<div class="card" >
			  <div class="card-header" style="font-size:20; bold">Pending Shipping</div>
			  <a style="cursor: hand;position: absolute; right:11px; top: 19px;"></a>
			  <div class="card-body">
				<h1 class="card-title text-center text-primary" onClick="fnOpenLoanerRequest('PendingShipping');" style="font-size:75; cursor: hand;"> <%=strPendingShip %> </h1>				
			  </div>
			</div>
		</div>
		<div style="float:left; width:1px; padding: 3px;"></div>	
		 <div class="col pt-4 pr-2">	
			<div class="card" >
			  <div class="card-header" style="font-size:20; bold">Same Day Shipment</div>
			  <a style="cursor: hand;position: absolute; right:14px; top: 19px;"></a>
			  <div class="card-body">
				<div id="chart-container"></div>			
			  </div>
			</div>
		</div>
	</div>
 </div>	
</div>
</body>	
</html>	