<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Arrays,java.lang.String" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ taglib prefix="fmtBackOrderRelease" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtBackOrderRelease:setLocale value="<%=strLocale%>"/>
<fmtBackOrderRelease:setBundle basename="properties.labels.operations.logistics.GmBackOrderRelease"/>
<% 
	String strWikiTitle = GmCommonClass.getWikiTitle("BACKORDER_RELEASE");
%>
<bean:define id="gridXmlData" name="frmBackOrderRelease" property="gridXmlData" type="java.lang.String"></bean:define>
<bean:define id="strAccountName" name="frmBackOrderRelease" property="strAccountName" type="java.lang.String"></bean:define>
<bean:define id="strSalesRep" name="frmBackOrderRelease" property="strSalesRep" type="java.lang.String"></bean:define>
<bean:define id="strPartNos" name="frmBackOrderRelease" property="strPartNos" type="java.lang.String"></bean:define>
<bean:define id="strOrderId" name="frmBackOrderRelease" property="strOrderId" type="java.lang.String"></bean:define>
<bean:define id="searchstrAccountName" name="frmBackOrderRelease" property="searchstrAccountName" type="java.lang.String"></bean:define>
<bean:define id="searchstrSalesRep" name="frmBackOrderRelease" property="searchstrSalesRep" type="java.lang.String"></bean:define>
<bean:define id="chkFullQty" name="frmBackOrderRelease" property="chkFullQty" type="java.lang.String"></bean:define>
<bean:define id="chkPartialQty" name="frmBackOrderRelease" property="chkPartialQty" type="java.lang.String"></bean:define>
<bean:define id="chkFGQty" name="frmBackOrderRelease" property="chkFGQty" type="java.lang.String"></bean:define>
<bean:define id="alPartNos" name="frmBackOrderRelease" property="alPartNos" type="java.util.ArrayList"></bean:define>
<bean:define id="initialFullchk" name="frmBackOrderRelease" property="initialFullchk" type="java.lang.String"></bean:define>
 <!-- Logistics\GmBackOrderRelease.jsp -->

<%
String strLogisticsJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_LOGISTICS");
String strPnum = "";
StringBuffer sb = new StringBuffer();
String strChkPartialQty = "";
String strChkFGQty = "";

if(alPartNos.size()>0){
    for(int i=0; i<alPartNos.size(); i++ ){
    	sb.append(alPartNos.get(i)).append(",");
    }
    strPnum = sb.toString();
}
String strInitialCheck = initialFullchk;
String strUserId = GmCommonClass.parseNull((String)session.getAttribute("strSessUserId"));
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Back Order Release</title>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">

<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<link rel="stylesheet" href="<%=strCssPath%>/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=strJsPath%>/common/bootstrap.min.css">

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strLogisticsJsPath%>/GmBackOrderRelease.js"></script> 
<script language="JavaScript" src="<%=strJsPath%>/common/bootstrap.min.js"></script>
<script>
var objGridData;
var hpnum;
var sales = '50261';
var fullQtyCheck = '<%=strInitialCheck%>';
var sessionUserId = '<%=strUserId%>';
objGridData = '<%=gridXmlData%>'; 
hpnum = '<%=strPnum%>';
</script>
<style>
.RightTextBlue { 
	FONT-SIZE: 11px; 
	COLOR: blue; 
	FONT-FAMILY: verdana, arial, sans-serif;
	text-decoration: None;
}

A.RightTextBlue { 
	FONT-SIZE: 11px; 
	COLOR: blue; 
	FONT-FAMILY: verdana, arial, sans-serif;
	text-decoration: underline;
}

A.RightTextBlue:hover { 
    text-decoration: underline;
	FONT-SIZE: 11px; 
	COLOR: blue; 
	FONT-FAMILY: verdana, arial, sans-serif;
}
</style>

</head>
<body leftmargin="20" topmargin="10" onload="fnOnLoad();">
<html:form action="/gmBackOrderReleaseAction.do?method=loadBackOrderDetails">
  <%-- <html:hidden property="inputCheckFull" value="<%=chkFullQty %>" /> --%>
  <html:hidden property="switchUserFl" value="<%=strSwitchUserFl %>" />
   	<table border="0" class="dttable1300" cellspacing="0" cellpadding="0">
				<!-- <tr><td colspan="10" height="1" bgcolor="#666666"></td></tr> -->
	<tr>
	<!-- <td class="Line" width="1" rowspan="6"></td> -->
		  <td height="25" class="RightDashBoardHeader">&nbsp;<fmtBackOrderRelease:message key="LBL_BACKORDER_RELEASE"/></td>
		  <td class="RightDashBoardHeader" align="right"><fmtBackOrderRelease:message key="IMG_HELP" var = "varHelp"/><img id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16'
					height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');"/>
				</td>
		
		<tr>
			<td colspan="2" height="1" class="LLine" ></td>
		</tr>
		</tr>
		<tr>
			<td colspan="2">
				<table width="100%" cellspacing="0" cellpadding="0">
					<tr>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
<td class="RightTableCaption" align="right" HEIGHT="24"><fmtBackOrderRelease:message key="LBL_ACCOUNTNAME"/>:</td>
 <td class="RightText" HEIGHT="24">
 							<table><tr HEIGHT="16"><td>	
 							<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
							<jsp:param name="CONTROL_NAME" value="strAccountName" />
							<jsp:param name="METHOD_LOAD" value="loadAccountList&searchType=LikeSearch" />
							<jsp:param name="WIDTH" value="400" />
							<jsp:param name="CSS_CLASS" value="search" />
							<jsp:param name="CONTROL_NM_VALUE" value="<%=searchstrAccountName%>" />
							<jsp:param name="CONTROL_ID_VALUE" value="<%=strAccountName %>" />
							<jsp:param name="SHOW_DATA" value="50" />
							<jsp:param name="ON_BLUR" value="fnAcBlur(this);" />
							</jsp:include>	
							</td></tr>
							</table>
							
<td class="RightTableCaption" align="right" HEIGHT="24"><fmtBackOrderRelease:message key="LBL_SALESREP"/>:</td>
<td class="RightText" HEIGHT="24">
            <table><tr HEIGHT="16"><td>
                    <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="strSalesRep" />
					<jsp:param name="METHOD_LOAD" value="loadSalesRepList&searchType=LikeSearch" />
					<jsp:param name="WIDTH" value="340" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="CONTROL_NM_VALUE" value="<%=searchstrSalesRep%>" /> 
					<jsp:param name="CONTROL_ID_VALUE" value="<%=strSalesRep %>" />
					<jsp:param name="SHOW_DATA" value="10" />
				</jsp:include>
				 </td></tr>
				</table>
</td>
</tr >
<tr height="25" class="shade">
<td class="RightTableCaption" align="right" HEIGHT="24"><fmtBackOrderRelease:message key="LBL_PARTNUMBER"/>:</td>
<td>&nbsp;<input type="text" size="68" value="<%=strPartNos%>" name="strPartNos" class="InputArea" style="text-transform: uppercase;" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"></td>
<td class="RightTableCaption" align="right" HEIGHT="24"><fmtBackOrderRelease:message key="LBL_ORDERID"/>:</td>
 <td> &nbsp;<input type="text" size="57" value="<%=strOrderId%>" name="strOrderId" class="InputArea" style="text-transform: uppercase;" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"></td>
</tr>

<tr><td colspan="6" class="Line" height="1"></td></tr>
<tr height="25">
 <td colspan="6">
  <table border="0" cellspacing="0" cellpadding="0" width="100%">
<tr>
  <td>&nbsp;&nbsp;<html:checkbox property="chkFullQty" name="frmBackOrderRelease" /><fmtBackOrderRelease:message key="CHK_RELEASE_FULL_QTY"/></td>
 <td>&nbsp;&nbsp;<html:checkbox property="chkPartialQty" name="frmBackOrderRelease" /><fmtBackOrderRelease:message key="CHK_RELEASE_PARTIAL_QTY"/></td>
 <td>&nbsp;&nbsp;<html:checkbox property="chkFGQty" name="frmBackOrderRelease" /><fmtBackOrderRelease:message key="CHK_ZERO_FG_QTY"/></td> 
 <fmtBackOrderRelease:message key="BTN_LOAD" var="varLoad"/> 
 <td width="40" height="40" align="center">
	 <button class="btn btn-primary btn-sm" style="margin-right:40px;width:70%" type="button" title="${varLoad}" onClick="fnLoad();">Load</button>
 </td>
</tr>
</table>
</td>
</tr>
</table>
<tr><td colspan="6" class="Line" height="1"></td></tr>
      <%
			  if (gridXmlData.indexOf("cell") != -1) {
			%>
	<tr>
		<td colspan="4">
			<div id="dataGridDiv" class="" height="500px"></div>
		</td>
	</tr>
<tr>
	<td class="RightTableCaption" align="center" colspan="6">
	    <table border="0" cellspacing="0" cellpadding="0" >
	    	<tr><td align="center">
		   		<button class="btn btn-primary btn-sm" id="btnReleaseBO" style="margin-bottom:5px;margin-top:5px" type="button" title="${varSubmit}" onClick="fnReleaseBackOrder();">Release Back Order</button>
				<button class="btn btn-primary btn-sm" style="margin-bottom:5px;margin-top:5px" type="button" title="${varInventoryLookup}" onClick="fnOverallLookup();">Inventory Lookup</button>
				<button class="btn btn-primary btn-sm" style="margin-bottom:5px;margin-top:5px" type="button" title="${varPartFulfillReport}" onClick="fnloadPartFulfillReport();">PartFullfil Report</button>
				<select name="strUpdateBillAction" class="RightText" tabindex=0 onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"> 
					<option value="0"><fmtBackOrderRelease:message key="OPT_CHOOSE_ONE"/>
					<option value="billonly"><fmtBackOrderRelease:message key="OPT_UPD_TO_BILL_ONLY"/></option>
					<option value="billonlyrtn"><fmtBackOrderRelease:message key="OPT_UPD_TO_BILL_ONLY_RTN"/></option>
			  </select>

                <fmtBackOrderRelease:message key="BTN_SUBMIT" var="varSubmit"/>
				<gmjsp:button value="&nbsp;${varSubmit}&nbsp;"gmClass="btn btn-primary btn-sm" buttonType="Save" onClick="fnSubmit()" />
			</td></tr>
		</table>
	</td>
</tr>
<tr>
   <td colspan="4" align="center">
		<div class='exportlinks'>
			<fmtBackOrderRelease:message key="LBL_EXPORT_OPTIONS" />:<img src='img/ico_file_excel.png' />&nbsp;
			 <a href="#" onclick="fnDownloadXLS();"><fmtBackOrderRelease:message key="LBL_EXCEL" /> </a>
		</div>
	</td>
</tr>
			<%
			  } else {
			%>

			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr height="30">
				<td colspan="4" align="center"><fmtBackOrderRelease:message
						key="LBL_NO_DATA_AVAILABLE" /></td>
			</tr>
			<%
			  }
			%>		
 </table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>