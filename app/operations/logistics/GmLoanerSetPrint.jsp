 <%
/**********************************************************************************
 * File		 		: GmLoanerSetPrint.jsp
 * Desc		 		: This screen is used for the print version of Set Loaner
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %> 
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@page import="java.util.Date"%>


<%@ taglib prefix="fmtLoanerSetPrint" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmLoanerSetPrint.jsp -->

<fmtLoanerSetPrint:setLocale value="<%=strLocale%>"/>
<fmtLoanerSetPrint:setBundle basename="properties.labels.operations.logistics.GmLoanerSetPrint"/>


<%
try {

	String strApplnDateFmt = strGCompDateFmt;

	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmCONSIGNMENT");
	HashMap hmConDetails = new HashMap();
	ArrayList alSetLoad = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("CONSIGNSETDETAILS"));
	String strType = GmCommonClass.parseNull((String)request.getAttribute("hType"));
	String strlottrkFlg = GmCommonClass.parseNull((String)request.getAttribute("LOTTRKFL"));
	
	String strAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	strAction = (strAction == null) ? "ViewPrint" : strAction;
	
	String strShade = "";
	String strConsignId = "";
	String strDistName = "";
	String strShipAdd = "";
	Date dtDate = null; 
	
	String strTemp = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strQty = "";
	String strUserName = "";
	String strCompanyId = "";
	String strCompanyName = "";
	String strCompanyAddress = "";
	String strCompanyLogo = "";
	String strTransCompId = "";
	String strTransCompanyLocale = "";
	String strRepName = "";
	
	String strStickCompanyLocale = "";
	String strStrickerCompId = "";
	String strPlantId = "";
	String strHeadAddrSticker = "";
	
	strPlantId = GmCommonClass.parseNull(gmDataStoreVO.getPlantid());
	strStrickerCompId= GmCommonClass.parseNull(gmDataStoreVO.getCmpid());  
	
	if (hmReturn != null)
	{
		strConsignId = GmCommonClass.parseNull((String)hmReturn.get("CONSIGNID"));
		strDistName = GmCommonClass.parseNull((String)hmReturn.get("DISTNM"));
		strRepName = GmCommonClass.parseNull((String)hmReturn.get("REPNM"));
		strShipAdd = GmCommonClass.getStringWithTM(GmCommonClass.parseNull((String)hmReturn.get("SETNAME")));
		dtDate = (Date)hmReturn.get("LDATE"); 
		strUserName = GmCommonClass.parseNull((String)hmReturn.get("ETCHID"));
		strCompanyId = GmCommonClass.parseNull((String)hmReturn.get("COMPANYID"));
		// To getting the transaction company ID. If not available then take the company id from context.
		strTransCompId = GmCommonClass.parseNull((String)hmReturn.get("TRANS_COMPANY_ID"));
		strTransCompId = strTransCompId.equals("")? gmDataStoreVO.getCmpid() : strTransCompId; 
		String strDivisionID = strCompanyId.equals("100801")?"2001":"2000";
		HashMap hmCompanyAddress = GmCommonClass.fetchCompanyAddress(gmDataStoreVO.getCmpid(), strDivisionID);
		strCompanyName = GmCommonClass.parseNull((String)hmCompanyAddress.get("COMPNAME"));
		strCompanyAddress = GmCommonClass.parseNull((String)hmCompanyAddress.get("COMPADDRESS"));
		strCompanyLogo = GmCommonClass.parseNull((String)hmCompanyAddress.get("LOGO"));
		strCompanyLogo = "/"+strCompanyLogo+".gif";
		strCompanyAddress = strCompanyAddress.replaceAll("/","<br>");
		
		//****PC_5344****
		if(strStrickerCompId.equals("1010") && strPlantId.equals("3003"))
		{
			
			strStickCompanyLocale = GmCommonClass.getCompanyLocale(strStrickerCompId);
			GmResourceBundleBean gmStkCompResourceBundleBean =
			        GmCommonClass.getResourceBundleBean("properties.Company", strStickCompanyLocale);
			
			strHeadAddrSticker = "<br>"+ GmCommonClass.parseNull((String)gmStkCompResourceBundleBean.getProperty("COMPANY_ADDRESS_STICKER"));
			
		}
	}
	//strConsignId = strConsignId + "$50182";
	strTransCompanyLocale = GmCommonClass.getCompanyLocale(strTransCompId);
	// Locale
    GmResourceBundleBean gmPaperworkResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strTransCompanyLocale);
    String strHeader  =	GmCommonClass.parseNull((String)gmPaperworkResourceBundleBean.getProperty("LOANER."+strType));
    strHeader = strHeader.equals("")? "Loaner": strHeader;
    
	int intSize = 0;
	HashMap hcboVal = null;
	String strClass="DtRuleTable700";
	StringBuffer sbStartSection = new StringBuffer();
	sbStartSection.append("<table border=0 cellspacing=0 cellpadding=0><tr><td align=center height=30 id=button>");
	
		//sbStartSection.append("<input type=button value=&nbsp;Print&nbsp; name=Btn_Print class=button onClick=fnPrint();>&nbsp;&nbsp;");
		sbStartSection.append("</td><tr></table>");
	
	sbStartSection.append("<table border=0 class="+strClass+" cellspacing=0 cellpadding=0 align=center>");
	sbStartSection.append("<tr><td bgcolor=#666666 height=1 colspan=3></td></tr>");
	sbStartSection.append("<tr><td bgcolor=#666666 width=1></td>");
	sbStartSection.append("<td>");
	sbStartSection.append("<table cellpadding=0 cellspacing=0 border=0 >");
	sbStartSection.append("<tr>");
	sbStartSection.append("<td width=170><img src=");sbStartSection.append(strImagePath+strCompanyLogo);
	sbStartSection.append(" width=138 height=60></td>");
	//sbStartSection.append("<td class=RightText width=510>"+strCompanyName+"<br>"+strCompanyAddress+"<br></td>");
	sbStartSection.append("<td class=RightText width=510>"+"<b>"+strHeadAddrSticker+strCompanyName+"</b><br>"+strCompanyAddress+"<br></td>");
	sbStartSection.append("<td align=right class=RightText width=420><font size=+3>");
	sbStartSection.append(strHeader);
	sbStartSection.append("</font>&nbsp;<br><img align=right src='/GmCommonBarCodeServlet?ID=");
	sbStartSection.append(strConsignId + "$50182"); 
	sbStartSection.append("	' height=40 width=150/></td>"); 
	sbStartSection.append("	<tr> ");						
	sbStartSection.append("	<td colspan=3>");
	sbStartSection.append("	<img align=left src='/GmCommonBarCodeServlet?ID=");
	sbStartSection.append(strConsignId); 
	sbStartSection.append("' height=60 width=120 />&nbsp;&nbsp;</td>");							
	sbStartSection.append("	</tr> ");
	sbStartSection.append("</tr>");
	sbStartSection.append("<tr><td height=1 colspan=3></td></tr>");
	sbStartSection.append("<tr><td bgcolor=#666666 colspan=3></td></tr>");
	sbStartSection.append("<tr><td colspan=3>");
	sbStartSection.append("<table border=0 cellspacing=0 cellpadding=0>");
	sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption>");
	sbStartSection.append("<td height=24 align=center width=250>&nbsp;Loaned To</td>");
	sbStartSection.append("<td bgcolor=#666666 width=1 rowspan=11></td>");
	sbStartSection.append("<td width=250 align=center>&nbsp;Set Details</td>");
	sbStartSection.append("<td bgcolor=#666666 width=1 rowspan=11></td>");
	sbStartSection.append("<td width=100 align=center>&nbsp;Date</td>");
	sbStartSection.append("<td width=100 align=center>&nbsp;Trans. ID</td>");
	sbStartSection.append("</tr><tr><td bgcolor=#666666 height=1 colspan=6></td></tr>");
	sbStartSection.append("<tr><td class=RightText rowspan=5 valign=top>");sbStartSection.append("D: "+strDistName+"<br> R: "+strRepName);
	sbStartSection.append("</td>");
	sbStartSection.append("<td rowspan=5 class=RightText valign=top>");sbStartSection.append(strShipAdd);
	sbStartSection.append("</td>");
	sbStartSection.append("<td height=25 class=RightText align=center>&nbsp;");sbStartSection.append(GmCommonClass.getStringFromDate(dtDate,strApplnDateFmt));
	sbStartSection.append("</td><td class=RightText align=center>&nbsp;");
	sbStartSection.append(strConsignId);
	sbStartSection.append("</td></tr><tr><td bgcolor=#666666 height=1 colspan=2></td></tr>");
	sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption><td align=center>&nbsp;</td><td align=center>Etch ID</td></tr>");
	sbStartSection.append("<tr><td bgcolor=#666666 height=1 colspan=2></td></tr>");
	sbStartSection.append("<tr><td align=center class=RightText>&nbsp;");
	sbStartSection.append("<Br></td><td align=center height=25 nowrap class=RightText>&nbsp;");sbStartSection.append(strUserName);
	sbStartSection.append("</td></tr><tr><td bgcolor=#666666 height=1 colspan=6></td></tr></table></td></tr>");
	sbStartSection.append("<tr><td colspan=3><table border=0 cellspacing=0 cellpadding=0>");
	sbStartSection.append("<tr class=RightTableCaption><td width=25 height=24>S#</td><td width=100 height=24>&nbsp;Part #</td>");
	sbStartSection.append("<td  width=300>Description</td><td align=left width=35>Qty</td>");
	if(strlottrkFlg.equals ("Y") && strType.equals("4127")){  
	sbStartSection.append("<td align=center width=100>Expiry Date</td>");
	}
	sbStartSection.append("<td align=center width=75>Price</td><td align=center width=75>Amount</td><td align=center width=120>&nbsp;&nbsp;Lot #</td><td align=center width=110>Noti/NAPPI Code</td></tr>");
	sbStartSection.append("</table></td></tr></table></td><td bgcolor=#666666 width=1></td></tr></table>");

	StringBuffer sbEndSection = new StringBuffer();
	sbEndSection.append("<table class="+strClass+" cellspacing=0 cellpadding=0 align=center>");
	sbEndSection.append("<tr><td bgcolor=#666666 width=1 rowspan=7></td><td height=1 width=698></td><td bgcolor=#666666 width=1 rowspan=7></td></tr>");
	sbEndSection.append("<tr><td height=10>&nbsp;</td></tr>");
	sbEndSection.append("<tr><td height=50>&nbsp;</td></tr>");
	sbEndSection.append("<tr><td height=20 class=RightText valign=top><B>Consigning Agent</B></td></tr>");
	sbEndSection.append("<tr><td height=30 class=RightText valign=top><B><u>NOTES</u></B>:");
	//sbEndSection.append("<BR>-Please sign and return the Consignment Agreement attached with this Form");
	sbEndSection.append("</td></tr>");
	sbEndSection.append("<tr><td bgcolor=#666666 height=1></td></tr></table>");
	String strOpt = GmCommonClass.parseNull(request.getParameter("hOpt"));
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Loaner Set - Print Version </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Rule.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<style>
TR.PageBreak {
     page-break-after: always;
}
</style>
<script>
var hopt = '<%=strOpt%>';
function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	buttonTableObj=document.getElementById("butTableF");
	buttonTableObj.style.display='none';
	buttonTableObj=document.getElementById("butTableS");
	buttonTableObj.style.display='none';
}

function showPrint()
{
	buttonTableObj=document.getElementById("butTableF");
	buttonTableObj.style.display='block';
	buttonTableObj=document.getElementById("butTableS");
	buttonTableObj.style.display='block';

}
function fnWPrint()
{
	if(hopt != 'manually'){
		setTimeout(fnAutoPrint,1);
	}
}
function fnAutoPrint(){
	var OLECMDID = 6;
	var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
	document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
	WebBrowser1.ExecWB(OLECMDID, 2);
	WebBrowser1.outerHTML = "";
} 
</script>
</HEAD>

<BODY topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();" onload="fnWPrint();">
<!--StartSection starts here-->
<%
if(strAction.equals("ViewPrint")) 
	{ 
%>
<table border=0 class="DtRuleTable700" cellspacing=0 cellpadding=0 id="butTableF" >
	<tr>
	<td align="center" height=30 id=button>
	<fmtLoanerSetPrint:message key="BTN_PRINT" var="varPrint"/>
	<gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" buttonType="Load" onClick="fnPrint();"/>&nbsp;&nbsp;
	</td><tr></table>
<%
	}
out.print(sbStartSection);%>
<!--StartSection ends here-->
	<table border="0" class="DtRuleTable700" cellspacing="0" cellpadding="0" align=center >
<%
	double dbCount = 0.0;
	HashMap hmLoop = new HashMap();
	HashMap hmTempLoop = new HashMap();

	String strNextPartNum = "";
	int intCount = 0;
	String strColor = "";
	String strLine = "";
	String strPartNumHidden = "";
	String strConNum="";
	String strNtfc="";
	String strPrice = "";
	String strTotal = "";
	String strAmount = "";
	double dbAmount = 0.0;
	double dbTotal = 0.0;
	int intQty = 0;
	intSize = alSetLoad.size();
	
	for (int i=0;i<intSize;i++)
	{
		hmLoop = (HashMap)alSetLoad.get(i);
		if (i<intSize-1)
		{
			hmTempLoop = (HashMap)alSetLoad.get(i+1);
			strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("PNUM"));
		}
		strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
		strPartNumHidden = strPartNum;
		strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
		strQty = GmCommonClass.parseZero((String)hmLoop.get("QTY"));
		String strExpDt = GmCommonClass.getStringFromDate((java.util.Date)hmLoop.get("EXPDATE"),strApplnDateFmt);
		strTemp = strPartNum;
		strConNum=GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
		strNtfc=GmCommonClass.parseNull((String)hmLoop.get("NTFC"));
		if (strType.equals("40050"))
		{
			strPrice = GmCommonClass.parseZero((String)hmLoop.get("PRICE"));

			intQty = Integer.parseInt(strQty);
			dbAmount = Double.parseDouble(strPrice);
			dbAmount = intQty * dbAmount; // Multiply by Qty
			strAmount = ""+dbAmount;

			dbTotal = dbTotal + dbAmount;
			strTotal = "$"+dbTotal;
		}
		else
		{
			strPrice = "-";
			strAmount = "-";
			strTotal = "-";
		}
		
		  if((!strlottrkFlg.equals ("Y")) && (!strType.equals("4127"))){  
	  if (strPartNum.equals(strNextPartNum))
		{
			intCount++;
			strLine = "<TR><TD colspan=9 height=1 bgcolor=#eeeeee></TD></TR>";
		}

		else 
		{
			strLine = "<TR><TD colspan=9 height=1 bgcolor=#eeeeee></TD></TR>";
			if (intCount > 0)
			{
				strPartNum = "";
				strPartDesc = "";
				strQty = "";
				strLine = "";
			}
			else
			{
				//strColor = "";
			}
			intCount = 0;
		}

		if (intCount > 1)
		{
			strPartNum = "";
			strPartDesc = "";
			strQty = "";
			strLine = "";
			strConNum="";
		}
	
		strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
		out.print(strLine); 
		  } 
	
%>
		<tr>
			<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif"></td>
			<td class="RightText" width="24"><%=i+1%></td>
			<td class="RightText" width="100" height="20">&nbsp;<%=strPartNum%></td>
			<td class="RightText" width="300"><%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
			<td class="RightText" align="center" width="35">&nbsp;<%=strQty%></td>
			<% if(strlottrkFlg.equals ("Y") && strType.equals("4127")){ %>
			<td class="RightText" align="center" width="100">&nbsp;<%=strExpDt%></td>
			<% } %>
			<td class="RightText" align="right" width="75"><%=GmCommonClass.getStringWithCommas(strPrice)%>&nbsp;&nbsp;</td>
			<td class="RightText" align="right" width="75"><%=GmCommonClass.getStringWithCommas(strAmount)%>&nbsp;&nbsp;</td>
			<td class="RightText" align="left" width="120">&nbsp;&nbsp;&nbsp;<%=strConNum%></td>
			<td class="RightText" align="left" width="110"><%=strNtfc%></td>
			<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif"></td>
		</tr>
<%
		if (i == 29)
		{
%>
		<tr><td colspan="9" height="1" bgcolor="#666666"></td></tr>
		<tr><td colspan="9" height="20" class="RightText" align="right"><fmtLoanerSetPrint:message key="LBL_CONTON_NXT_PAGE"/> -></td></tr>
		</table>
		<p STYLE="page-break-after: always"></p>
<%
		out.print(sbStartSection);
%>
		<table border="0" class="DtRuleTable700" cellspacing="0" cellpadding="0" align=center>
<%
		} //end of IF

	} // end of FOR
%>

		<tr><td colspan="10" height="1" bgcolor="#666666"></td></tr>
		<tr border="1">
			<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1" height="1"></td>
			<td colspan="3" height="25">&nbsp;</td>
			<%if(strlottrkFlg.equals ("Y") && strType.equals("4127")){  %>
			<td></td>
			<%} %>
			<td width="5" class="RightTableCaption" align="center"></td>
			<td width="40" class="RightTableCaption" align="right"><fmtLoanerSetPrint:message key="LBL_TOTAL"/></td>
			<td width="80" class="RightTableCaption" align="right"><%=GmCommonClass.getStringWithCommas(strTotal)%></td>
			<td width="140" class="RightTableCaption" align="center"></td>
			<td colspan="1">&nbsp;</td>
			<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1" height="1"></td>
		</tr>
		<tr><td colspan="10" height="1" bgcolor="#666666" width="80"><img src="<%=strImagePath%>/spacer.gif"></td></tr>		
	</table>
<!--EndSection starts here-->
<%out.print(sbEndSection);
if(strAction.equals("ViewPrint")) 
	{ 

%>
<table border=0 class="DtTable700" cellspacing=0 cellpadding=0 align=center>
	<tr>
		<td>
			<jsp:include page="/common/GmRuleDisplayInclude.jsp">
			<jsp:param name="Show" value="true" />
			<jsp:param name="Fonts" value="false" />
			</jsp:include>
		</td>
	</tr>
</table>
<table border=0 class="DtRuleTable700" cellspacing=0 cellpadding=0 align=center>	
	<tr>
		<td>
				<jsp:include page="/common/GmIncludeDateStamp.jsp" />
		</td>	
	</tr>
</table>	
<table border=0 align="center" class="DtRuleTable700" cellspacing=0 cellpadding=0 align=center id="butTableS">		
	<tr>
		<td height="30" align="center" id=button>
		<fmtLoanerSetPrint:message key="BTN_PRINT" var="varPrint"/>
		<gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" onClick="fnPrint();" buttonType="Load" />&nbsp;&nbsp;
		</td>
	</tr>	
</table>
<!--EndSection ends here-->

<%
	}
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
