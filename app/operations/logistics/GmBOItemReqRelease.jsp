<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Arrays,java.lang.String" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ taglib prefix="fmtBOItemReqRelease" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtBOItemReqRelease:setLocale value="<%=strLocale%>"/>
<fmtBOItemReqRelease:setBundle basename="properties.labels.operations.logistics.GmBackOrderRelease"/>
<% 
	String strWikiTitle = GmCommonClass.getWikiTitle("BO_ITEM_REQ_RELEASE");
%>
<bean:define id="strAccountName" name="frmBOItemReqRelease" property="strAccountName" type="java.lang.String"></bean:define>
<bean:define id="strSalesRep" name="frmBOItemReqRelease" property="strSalesRep" type="java.lang.String"></bean:define>
<bean:define id="strPartNos" name="frmBOItemReqRelease" property="strPartNos" type="java.lang.String"></bean:define>
<bean:define id="strReqId" name="frmBOItemReqRelease" property="strReqId" type="java.lang.String"></bean:define>
<bean:define id="searchstrAccountName" name="frmBOItemReqRelease" property="searchstrAccountName" type="java.lang.String"></bean:define>
<bean:define id="searchstrSalesRep" name="frmBOItemReqRelease" property="searchstrSalesRep" type="java.lang.String"></bean:define>
<bean:define id="chkFullQty" name="frmBOItemReqRelease" property="chkFullQty" type="java.lang.String"></bean:define>
<bean:define id="chkPartialQty" name="frmBOItemReqRelease" property="chkPartialQty" type="java.lang.String"></bean:define>
<bean:define id="chkFGQty" name="frmBOItemReqRelease" property="chkFGQty" type="java.lang.String"></bean:define>
<bean:define id="initialFullchk" name="frmBOItemReqRelease" property="initialFullchk" type="java.lang.String"></bean:define>
 <!-- Logistics\GmBOItemReqRelease.jsp -->

<%
String strLogisticsJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_LOGISTICS");

StringBuffer sb = new StringBuffer();
String strChkPartialQty = "";
String strChkFGQty = "";
String strInitialCheck = initialFullchk;
String strUserId = GmCommonClass.parseNull((String)session.getAttribute("strSessUserId"));
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Release Item BO</title>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">

<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<link rel="stylesheet" href="<%=strCssPath%>/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=strJsPath%>/common/bootstrap.min.css">

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strLogisticsJsPath%>/GmBOItemReqRelease.js"></script> 
<script language="JavaScript" src="<%=strJsPath%>/common/bootstrap.min.js"></script>
<script>
var hpnum;
var items = '50263';//items
var fullQtyCheck = '<%=strInitialCheck%>';
var sessionUserId = '<%=strUserId%>';
</script>
<style>
.RightTextBlue { 
	FONT-SIZE: 11px; 
	COLOR: blue; 
	FONT-FAMILY: verdana, arial, sans-serif;
	text-decoration: None;
}

A.RightTextBlue { 
	FONT-SIZE: 11px; 
	COLOR: blue; 
	FONT-FAMILY: verdana, arial, sans-serif;
	text-decoration: underline;
}

A.RightTextBlue:hover { 
    text-decoration: underline;
	FONT-SIZE: 11px; 
	COLOR: blue; 
	FONT-FAMILY: verdana, arial, sans-serif;
}
</style>

</head>
<body leftmargin="20" topmargin="10" onload="fnOnLoad();">
<html:form action="/gmBOItemReqReleaseAction.do?method=loadBOItemReqDetails">
  <%-- <html:hidden property="inputCheckFull" value="<%=chkFullQty %>" /> --%>
  <html:hidden property="switchUserFl" value="<%=strSwitchUserFl %>" />
   	<table border="0" class="dttable1300" cellspacing="0" cellpadding="0">
				<!-- <tr><td colspan="10" height="1" bgcolor="#666666"></td></tr> -->
	<tr>
	<!-- <td class="Line" width="1" rowspan="6"></td> -->
		  <td height="25" class="RightDashBoardHeader">&nbsp;<fmtBOItemReqRelease:message key="LBL_BO_ITEM_REQ_RELEASE"/></td>
		  <td class="RightDashBoardHeader" align="right"><fmtBOItemReqRelease:message key="IMG_HELP" var = "varHelp"/><img id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16'
					height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');"/>
				</td>
		
		<tr>
			<td colspan="2" height="1" class="LLine" ></td>
		</tr>
		</tr>
		<tr>
			<td colspan="2">
				<table width="100%" cellspacing="0" cellpadding="0">
					<tr>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
<td class="RightTableCaption" align="right" HEIGHT="24"><fmtBOItemReqRelease:message key="LBL_ACCOUNTNAME"/>:</td>
 <td class="RightText" HEIGHT="24">
 							<table><tr HEIGHT="16"><td>	
 							<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
							<jsp:param name="CONTROL_NAME" value="strAccountName" />
							<jsp:param name="METHOD_LOAD" value="loadAccountList&searchType=LikeSearch" />
							<jsp:param name="WIDTH" value="400" />
							<jsp:param name="CSS_CLASS" value="search" />
							<jsp:param name="CONTROL_NM_VALUE" value="<%=searchstrAccountName%>" />
							<jsp:param name="CONTROL_ID_VALUE" value="<%=strAccountName %>" />
							<jsp:param name="SHOW_DATA" value="50" />
							<jsp:param name="ON_BLUR" value="fnAcBlur(this);" />
							</jsp:include>	
							</td></tr>
							</table>
							
<td class="RightTableCaption" align="right" HEIGHT="24"><fmtBOItemReqRelease:message key="LBL_SALESREP"/>:</td>
<td class="RightText" HEIGHT="24">
            <table><tr HEIGHT="16"><td>
                    <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="strSalesRep" />
					<jsp:param name="METHOD_LOAD" value="loadSalesRepList&searchType=LikeSearch" />
					<jsp:param name="WIDTH" value="340" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="CONTROL_NM_VALUE" value="<%=searchstrSalesRep%>" /> 
					<jsp:param name="CONTROL_ID_VALUE" value="<%=strSalesRep %>" />
					<jsp:param name="SHOW_DATA" value="10" />
				</jsp:include>
				 </td></tr>
				</table>
</td>
</tr >
<tr height="25" class="shade">
<td class="RightTableCaption" align="right" HEIGHT="24"><fmtBOItemReqRelease:message key="LBL_PARTNUMBER"/>:</td>
<td>&nbsp;<input type="text" size="68" value="<%=strPartNos%>" name="strPartNos" class="InputArea" style="text-transform: uppercase;" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"></td>
<td class="RightTableCaption" align="right" HEIGHT="24"><fmtBOItemReqRelease:message key="LBL_REQ_ID"/>:</td>
 <td> &nbsp;<input type="text" size="57" value="<%=strReqId%>" name="strReqId" class="InputArea" style="text-transform: uppercase;" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"></td>
</tr>

<tr><td colspan="6" class="Line" height="1"></td></tr>
<tr height="25">
 <td colspan="6">
  <table border="0" cellspacing="0" cellpadding="0" width="100%">
<tr>
  <td>&nbsp;&nbsp;<html:checkbox property="chkFullQty" name="frmBOItemReqRelease" /><fmtBOItemReqRelease:message key="CHK_RELEASE_ITEM_FULL_QTY"/></td>
 <td>&nbsp;&nbsp;<html:checkbox property="chkPartialQty" name="frmBOItemReqRelease" /><fmtBOItemReqRelease:message key="CHK_RELEASE_ITEM_PARTIAL_QTY"/></td>
 <td>&nbsp;&nbsp;<html:checkbox property="chkFGQty" name="frmBOItemReqRelease" /><fmtBOItemReqRelease:message key="CHK_ZERO_ITEM_FG_QTY"/></td> 
 <fmtBOItemReqRelease:message key="BTN_LOAD" var="varLoad"/> 
 <td width="40" height="40" align="center">
	 <button class="btn btn-primary btn-sm" style="margin-right:40px;width:70%" type="button" title="${varLoad}" onClick="fnLoad();">Load</button>
 </td>
</tr>
</table>
</td>
</tr>
</table>
<tr><td colspan="6" class="Line" height="1"></td></tr>
	<tr>
		<td colspan="4">
			<div id="dataGridDiv" class=""  width="100%"></div>
		</td>
	</tr>
<tr id="buttonDiv">
	<td  class="RightTableCaption" align="center" colspan="6">
	    <table border="0" cellspacing="0" cellpadding="0" >
	    	<tr><td align="center">
		   		<button class="btn btn-primary btn-sm" id="btnReleaseBO" style="margin-bottom:5px;margin-top:5px" type="button" title="${varSubmit}" onClick="fnReleaseBackOrder();">Release Back Order</button>
				<button class="btn btn-primary btn-sm" style="margin-bottom:5px;margin-top:5px" type="button" title="${varInventoryLookup}" onClick="fnOverallLookup();">Inventory Lookup</button>
				<button class="btn btn-primary btn-sm" style="margin-bottom:5px;margin-top:5px" type="button" title="${varPartFulfillReport}" onClick="fnloadPartFulfillReport();">PartFullfil Report</button>
			</td></tr>
		</table>
	</td>
</tr>
<tr id="exportDiv">
   <td  colspan="4" align="center">
		<div class='exportlinks'>
			<fmtBOItemReqRelease:message key="LBL_EXPORT_OPTIONS" />:<img src='img/ico_file_excel.png' />&nbsp;
			 <a href="#" onclick="fnDownloadXLS();"><fmtBOItemReqRelease:message key="LBL_EXCEL" /> </a>
		</div>
	</td>
</tr>
			

			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr id="noDataDiv" height="30">
				<td  colspan="4" align="center"><fmtBOItemReqRelease:message
						key="LBL_NO_DATA_AVAILABLE" /></td>
			</tr>
				
 </table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>