<%@page import="java.util.Date"%>
<%
/**********************************************************************************
 * File		 		: GmInHouseSetStatus.jsp
 * Desc		 		: This screen is used for the DashBoard Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%> 
<%@ page language="java" %>
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc"%>

<!-- WEB-INF path corrected for JBOSS migration changes -->



<%@ taglib prefix="fmtInHouseSetStatus" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmInHouseSetStatus.jsp -->

<fmtInHouseSetStatus:setLocale value="<%=strLocale%>"/>
<fmtInHouseSetStatus:setBundle basename="properties.labels.operations.logistics.GmInHouseSetStatus"/>



<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strAction = GmCommonClass.parseNull((String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction"));
	String strCommentsIconAccess = GmCommonClass.parseNull((String)request.getAttribute("UPDFL")==null?"":(String)request.getAttribute("UPDFL"));
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmParam = (HashMap)request.getAttribute("hmParam");
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	String strMngLnrWikiTitle = GmCommonClass.getWikiTitle("MANAGE_LOANER");
	String strInLnrWikiTitle = GmCommonClass.getWikiTitle("MANAGE_INHOUSE_LOANER");
	String strhType = GmCommonClass.parseNull((String)hmParam.get("TYPE"));
    // Check whether the priority should be displayed for the set type or not
    String strShowPriorityKey = strhType+ ("_SHOW_PRIORITY");
    String strShowPriorityFl = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty(strShowPriorityKey));
    
    String strhCategories = GmCommonClass.parseNull((String)hmParam.get("CATEGORY"));
	String strhSetIds = GmCommonClass.parseNull((String)hmParam.get("SETID"));
	String strhStatus = GmCommonClass.parseNull((String)hmParam.get("STATUS"));
	String strhPriority = GmCommonClass.parseNull((String)hmParam.get("PRIORITYSTR"));
	String strhStatusGrp = GmCommonClass.parseNull((String)hmParam.get("STATUSGRP"));
	String strhEtchId = GmCommonClass.parseNull((String)hmParam.get("ETCHID"));
	String strhConId = GmCommonClass.parseNull((String)hmParam.get("CONID"));
	String strhOverdue = GmCommonClass.parseNull((String)hmParam.get("OVERDUE"));
	String strhHoldTxn = GmCommonClass.parseNull((String)hmParam.get("HOLDTXN"));
	String strhSetName = GmCommonClass.parseNull((String)hmParam.get("SETNAME"));
	String strhRequestId = GmCommonClass.parseNull((String)hmParam.get("REQID"));
	strhOverdue = strhOverdue.equals("")?"":"checked";
	strhHoldTxn=strhHoldTxn.equals("")?"":"checked";
	String strhDueDays = GmCommonClass.parseNull((String)hmParam.get("DUEDAYS"));
	String strhDistributors = GmCommonClass.parseNull((String)hmParam.get("DISTIDS"));
	String strCaseInfoID = GmCommonClass.parseZero((String)hmParam.get("CASEINFOID"));
	String strMissedAFL = GmCommonClass.parseNull((String)hmParam.get("MACCESSFL"));
	String strRepId = GmCommonClass.parseNull((String)request.getAttribute("REPID"));
	String strRepName = GmCommonClass.parseNull((String)request.getAttribute("REPNAME"));
	ArrayList alReport = new ArrayList();
	ArrayList alSets = new ArrayList();
	ArrayList alCategories = new ArrayList();
	ArrayList alStatus = new ArrayList();
	ArrayList alPriority = new ArrayList();
	ArrayList alDist = new ArrayList();
	ArrayList alEventName = new ArrayList();
	ArrayList alLoanerOpts = new ArrayList();
	
	String strApplDateFmt = strGCompDateFmt;
	java.util.Date dtLDate = null;
	java.util.Date dtEDate = null;
	
	int intSetLength = 0;
	int intCatLength = 0;
	int intStatLength = 0;
	int intPriority = 0;
	int intRepLength = 0;
	int intDaysOverdue = 0;
	
	HashMap hmAlDetails = new HashMap ();
	String strCodeId="";
	if (hmReturn != null) 
	{
		alSets = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("SETLIST"));
		alCategories = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("CATEGORIES"));
		alStatus = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("STATUS"));
		alPriority= GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("PRIORITY"));
		alDist = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("DISTLIST"));
		alEventName = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("EVENTNAME"));
		alLoanerOpts = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("LOANEROPT"));
		
		if (!alSets.isEmpty())
		{
			intSetLength = alSets.size();
		}
		if (!alCategories.isEmpty())
		{
			intCatLength = alCategories.size();
		}
		if (!alStatus.isEmpty())
		{
			intStatLength = alStatus.size();
		}	
		if (!alPriority.isEmpty())
		{
			intPriority = alPriority.size();
		}	
		if (strAction.equals("Reload"))
		{
			alReport = (ArrayList)hmReturn.get("REPORT");
			if (!alReport.isEmpty())
			{
				intRepLength = alReport.size();
			}
		}
	}
	/*if (!strhType.equals("4127")) {
	 for (int i = 0; i < alStatus.size(); i++){
			hmAlDetails = (HashMap) alStatus.get(i);
			strCodeId = (String) hmAlDetails.get("CODEID");
			if (strCodeId.equals("50198")){//inactive
				alStatus.remove(i);
			} 			
		  }
	}*/

	HashMap hmLoop = new HashMap();

	String strConId = "";
	String strSetName = "";
	String strCategory = "";
	String strDateInitiated = "";
	String strComments = "";
	String strEtchId = "";
	String strLoanStatus = "";
	String strLoanStatusId = "";
	String strLoanToNm = "";
	String strExpDate = "";
	String strLoanDt = "";
	String strElapsedDays = "";
	String strDaysOverdue = "";
	String strShade = "";
	String strHoldFl="";
	String strRequestID = "";
	String strRequestCnt = "";
	HashMap hmMap = new HashMap();
	hmMap.put("ID","");
	hmMap.put("PID","CODEID");
	hmMap.put("NM","CODENM");
	
	HashMap hmSetMap = new HashMap();
	hmSetMap.put("ID","");
	hmSetMap.put("PID","CODENMALT");
	hmSetMap.put("NM","CODENM");
	
	HashMap hmSetMapPriority = new HashMap();
	hmSetMapPriority.put("ID","");
	hmSetMapPriority.put("PID","CODEID");
	hmSetMapPriority.put("NM","CODENM");
	
	HashMap hmMapSets = new HashMap();
	hmMapSets.put("ID","");
	hmMapSets.put("PID","ID");
	hmMapSets.put("NM","NAME");	
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: InHouse Sets Consign Process</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> <!-- PC-2233 Japanese Character search in Manage Loaner changes to Junk -->
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmInhouseSetStatus.js"></script>
<script>
var sServletPath = '<%=strServletPath%>';
var stype = '<%=strhType%>';
var missedFl ='<%=strMissedAFL%>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="javascript:fnLoad();">
<xml src="<%=strXmlPath%>/<%=strhType%>.xml" id="xmldso" async="false"></xml>
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmInHouseSetServlet">
<input type="hidden" name="hOpt" value="<%=strhType%>">
<input type="hidden" name="hSetIds" value="<%=strhSetIds%>">
<input type="hidden" name="hAction" id="hAction" value="<%=strAction%>">
<input type="hidden" name="hCategories" value="<%=strhCategories%>">
<input type="hidden" name="hConsignId" value="">
<input type="hidden" name="hFlagId" value="">
<input type="hidden" name="hStatus" value="<%=strhStatus%>">
<input type="hidden" id="hPriorityStr" name="hPriorityStr" value="<%=strhPriority%>">
<input type="hidden" name="hStatusGrp" value="<%=strhStatusGrp%>">
<input type="hidden" name="hEtchId" value="">
<input type="hidden" name="hCancelType" value="">
<input type="hidden" name="hTxnId" value="">
<input type="hidden" name="hInputEditPart" value ="">
<input type="hidden" name="hDistIds" value="<%=strhDistributors%>">
<input type="hidden" name="hAddChargeId" value ="">
<input type="hidden" name="hCancelReturnVal" value ="">
<input type="hidden" name="hSalesRepName" value ="<%=strRepName %>">

<table border="0" width="1100" cellspacing="0" cellpadding="0">
		<tr>
			<td rowspan="5" width="1" class="Line"></td>
			<td height="1" bgcolor="#666666"></td>
			<td rowspan="5" width="1" class="Line"></td>
		</tr>
		<tr>			
			<%if (strhType.equals("4127")){%>
				<td height="25" class="RightDashBoardHeader">
					<fmtInHouseSetStatus:message key="LBL_PRODUCT_LOANERS"/>
				<fmtInHouseSetStatus:message key="IMG_ALT_HELP" var="varHelp"></fmtInHouseSetStatus:message> 
					<img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strMngLnrWikiTitle%>');" />
				</td>
			<%}else if (strhType.equals("4119")){ %>
				<td height="25" class="RightDashBoardHeader">
					<fmtInHouseSetStatus:message key="LBL_INHOUSE_LOANERS"/>
					<fmtInHouseSetStatus:message key="IMG_ALT_HELP" var="varHelp"></fmtInHouseSetStatus:message> 
					<img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strInLnrWikiTitle%>');" />
				</td>
			<%} %>
		</tr>
		<tr>
			<td height="35" class="RightText">
				<table class="Border" cellspacing="0" width="100%">
					<tr height="18" bgcolor="#eeeeee" class="RightTableCaption">
<%
	if (strhType.equals("4119")){
%>					
						<td><fmtInHouseSetStatus:message key="LBL_CATEGORIES"/></td>
<%
	}else if (strhType.equals("4127")){ 
%>
						<td><fmtInHouseSetStatus:message key="LBL_FIELS_SALES"/></td>
<%
	} 
%>

						<td><fmtInHouseSetStatus:message key="LBL_SETS"/></td>
						<td><fmtInHouseSetStatus:message key="LBL_STATUS"/></td>
						
			            <% if(strShowPriorityFl.equals("YES"))  {  %>  
            				<td><fmtInHouseSetStatus:message key="LBL_PRIORITY"/></td>
			            <%} %>	  
			 
						<td>&nbsp;</td>
						<td rowspan="2" width="100" valign="middle" align="center">
						<fmtInHouseSetStatus:message key="BTN_LOAD" var="varLoad"/>						
							<gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="fnReload();" tabindex="25" buttonType="Load" />&nbsp;
							<BR><BR><fmtInHouseSetStatus:message key="BTN_RESET" var="varReset"/>	
							<gmjsp:button value="&nbsp;${varReset}&nbsp;" gmClass="button" onClick="document.frmAccount.reset();" tabindex="25" buttonType="Load" />&nbsp;
						</td>
					</tr>
					<tr>
<%
	if (strhType.equals("4119")){
%>					
						<td><%=GmCommonControls.getChkBoxGroup("",alCategories,"Cat",hmMap)%></td>
<%
	}else if (strhType.equals("4127")){ 
%>
						<td><%=GmCommonControls.getChkBoxGroup("",alDist,"Dist",hmMapSets)%></td>
<%
	} 
%>						
						<td><%=GmCommonControls.getChkBoxGroup("",alSets,"Set",hmMapSets,"IDNM")%></td>
						<td><%=GmCommonControls.getChkBoxGroup("",alStatus,"Stat",hmSetMap)%></td>
						
			            <% if(strShowPriorityFl.equals("YES"))  {  %>  
             			<td><%=GmCommonControls.getChkBoxGroup("",alPriority,"Priority",hmSetMapPriority)%></td>
            			<%} %>	  
			 
						<td align="right" valign="middle">
							<table cellspacing="0" width="100%">
								<tr>
									<td align="right" class="RightTableCaption"><fmtInHouseSetStatus:message key="LBL_SET_NAME"/>:</td>
									<td>&nbsp;<input type="text" name="Txt_SetNm" value="<%=strhSetName%>" class="RightText" size="15"></td>
								</tr>
								<tr>
									<td align="right" class="RightTableCaption"><fmtInHouseSetStatus:message key="LBL_RQ_ID"/>:</td>
									<td>&nbsp;<input type="text" name="Txt_ReqId" value="<%=strhRequestId%>" class="RightText" size="15"></td>
								</tr>
								<tr>
									<td align="right" class="RightTableCaption"><fmtInHouseSetStatus:message key="LBL_ETCH_ID"/>:</td>
									<td>&nbsp;<input type="text" name="Txt_EtchId" value="<%=strhEtchId%>" class="RightText" size="15"></td>
								</tr>
								<tr>
									<td align="right" class="RightTableCaption"><fmtInHouseSetStatus:message key="LBL_CN_ID"/>:</td>
									<td>&nbsp;<input type="text" name="Txt_ConId" value="<%=strhConId%>" class="RightText" size="15"></td>
								</tr>
								<tr>
									<td align="right" class="RightTableCaption"><fmtInHouseSetStatus:message key="LBL_RETURN_IN"/>:</td>
									<td class="RightTableCaption">&nbsp;<input type="text" name="Txt_DueDays" value="<%=strhDueDays%>" class="RightText" size="1"><fmtInHouseSetStatus:message key="LBL_DAYS"/> </td>
								</tr>
								<tr>
									<td align="right" class="RightTableCaption"><fmtInHouseSetStatus:message key="LBL_OVERDUE"/>:</td>
									<td class="RightTableCaption">&nbsp;<input type="checkbox" name="Chk_Overdue" <%=strhOverdue%>/></td>
								</tr>
								<tr>
									<td align="right" class="RightTableCaption"><fmtInHouseSetStatus:message key="LBL_HOLD_TXN"/>:</td>
									<td class="RightTableCaption">&nbsp;<input type="checkbox" name="Chk_HoldTxn" <%=strhHoldTxn%>></td>
								</tr>
								<%
								if (strhType.equals("4119")){
								%>	
								<tr>
								<!-- Custom tag lib code modified for JBOSS migration changes -->
									<td align="right" class="RightTableCaption"><fmtInHouseSetStatus:message key="LBL_EVENT_NAME"/>:</td>
									<td class="RightTableCaption">&nbsp;<gmjsp:dropdown controlName="eventName"   seletedValue="<%=strCaseInfoID%>"  value="<%=alEventName%>" codeId = "CID"  
									codeName = "ENAME" defaultValue= "[Choose One]" width="220"/>
									</td>
								</tr>
								<%} %>
							</table>									
						</td>
					</tr>
				</table>			
			</td>
		</tr>
		<!-- Added Sales Rep filter for PC-884 -->
		<%
			if (strhType.equals("4127")) {
		%>
			<tr height="40">
				<td>
					<table>
						<fmtInHouseSetStatus:message key="LBL_SALES_REP_LIST"
							var="varSalesRepList" />
						<gmjsp:label type="BoldText"
							SFLblControlName="${varSalesRepList}:" td="true" />
						<td><jsp:include page="/common/GmAutoCompleteInclude.jsp">
								<jsp:param name="CONTROL_NAME" value="Cbo_RepId" />
								<jsp:param name="METHOD_LOAD" value="loadSalesRepList" />
								<jsp:param name="WIDTH" value="400" />
								<jsp:param name="CSS_CLASS" value="search" />
								<jsp:param name="CONTROL_NM_VALUE" value="<%=strRepName %>" />
								<jsp:param name="CONTROL_ID_VALUE" value="<%=strRepId %>" />
								<jsp:param name="SHOW_DATA" value="10" />
								<jsp:param name="ON_BLUR" value="fnSetRepName(this.value);" />
							</jsp:include></td>
					</table>
				</td>
			</tr>
			
			<%
				}
			%>
		<tr>
			<td>
				<table border="0" width="1098" cellspacing="0" cellpadding="0">
				<%
					if (strhType.equals("4127")) {
				%>
					<tr><td colspan="11" height="1" bgcolor="#666666"></td></tr>
				<%
					}
				%>
					<tr class="ShadeRightTableCaption">
						<td width="50"><fmtInHouseSetStatus:message key="LBL_SET_ID"/></td>
						<td HEIGHT="24" width="100"><fmtInHouseSetStatus:message key="LBL_CONSIGN_ID"/></td>
						<td HEIGHT="24" width="100"><fmtInHouseSetStatus:message key="LBL_ETCH_ID"/></td>
						<td width="100" align="center"><fmtInHouseSetStatus:message key="LBL_CATEGORY"/></td>
						<td HEIGHT="24" align="center" width="150"><fmtInHouseSetStatus:message key="LBL_STATUS"/></td>
		               <% if(strShowPriorityFl.equals("YES"))  {  %>  
			            <td HEIGHT="24" align="center" width="150"><fmtInHouseSetStatus:message key="LBL_PRIORITY"/></td> 
			           <%} %>	  
						<td width="150">&nbsp;<fmtInHouseSetStatus:message key="LBL_LOANED_TO"/></td>
						<td width="100">&nbsp;<fmtInHouseSetStatus:message key="LBL_LOANED_ON"/></td>
						<td width="60">&nbsp;<fmtInHouseSetStatus:message key="LBL_DAYS_ELAPSED"/></td>
						<td width="100" align="center">&nbsp;<fmtInHouseSetStatus:message key="LBL_EXP.RETURN_DATE"/></td>
						<td width="60">&nbsp;<fmtInHouseSetStatus:message key="LBL_DAYS_OVERDUE"/></td>
					</tr>
<%

	
		if (strAction.equals("Reload"))
		{
			if (intRepLength > 0)
			{
				HashMap hmTempLoop = new HashMap();
				String strSetId = "";
				String strNextId = "";
				String strCmtCnt="0";
				int intCount = 0;
				String strLine = "";
				boolean blFlag = false;
				boolean blFlag1 = false;
				String strTemp = "";
				int intSetCnt = 0;
				StringBuffer sbTemp = new StringBuffer();
				for (int i = 0;i < intRepLength ;i++ )
				{
					hmLoop = (HashMap)alReport.get(i);

					if (i<intRepLength-1)
					{
						hmTempLoop = (HashMap)alReport.get(i+1);
						strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("SETID"));
					}
					strSetId = GmCommonClass.parseNull((String)hmLoop.get("SETID"));
					strConId = GmCommonClass.parseNull((String)hmLoop.get("CONID"));
					String strPriority = GmCommonClass.parseNull((String)hmLoop.get("PRIORITY"));
					strSetName = GmCommonClass.parseNull((String)hmLoop.get("SNAME"));
					strCategory = GmCommonClass.parseNull((String)hmLoop.get("PNAME"));
					strDateInitiated = GmCommonClass.parseNull((String)hmLoop.get("IDATE"));
					strEtchId = GmCommonClass.parseNull((String)hmLoop.get("ETCHID"));
					strLoanStatus = GmCommonClass.parseNull((String)hmLoop.get("LOANSFL"));
					strLoanStatusId = GmCommonClass.parseNull((String)hmLoop.get("LSFL"));
					strComments = GmCommonClass.parseNull((String)hmLoop.get("COMMENTS"));
					strLoanToNm = GmCommonClass.parseNull((String)hmLoop.get("LOANNM"));
					dtLDate=(java.util.Date)hmLoop.get("LDATE");
					dtEDate=(java.util.Date)hmLoop.get("EDATE");
					
					strLoanDt = GmCommonClass.getStringFromDate(dtLDate,strApplDateFmt);
					strExpDate = GmCommonClass.getStringFromDate(dtEDate,strApplDateFmt);
					
					strElapsedDays = GmCommonClass.parseNull((String)hmLoop.get("ELPDAYS"));
					strDaysOverdue = GmCommonClass.parseZero((String)hmLoop.get("OVERDUE"));
					strCmtCnt= GmCommonClass.parseNull((String)hmLoop.get("CMTCNT"));
					strHoldFl= GmCommonClass.parseNull((String)hmLoop.get("HOLDFL"));
					strRequestID = GmCommonClass.parseNull((String)hmLoop.get("REQID"));
					strRequestCnt = GmCommonClass.parseNull((String)hmLoop.get("RECONCMTCNT"));
					intDaysOverdue = Integer.parseInt(strDaysOverdue);
					strDaysOverdue = intDaysOverdue <= 0 ?"":""+intDaysOverdue;
					strTemp = strSetId.replaceAll("\\.","");

					if (strSetId.equals(strNextId))
					{
						intCount++;
						strLine = "<TR><TD colspan=11 height=1 class=Line></TD></TR>";
						intSetCnt++;
					}
					else
					{
						intSetCnt++;
						sbTemp.append("document.getElementById('tdset"+strTemp+"').innerHTML = '<b>"+intSetCnt+"</b>';");
						intSetCnt = 0;
						strLine = "<TR><TD colspan=11 height=1 class=Line></TD></TR>";
						if (intCount > 0)
						{
							strSetName = "";
							strLine = "";
						}
						else
						{
							blFlag = true;
						}
						intCount = 0;
					}

					if (intCount > 1)
					{
						strSetName = "";
						strLine = "";
					}
				
					strShade = (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows

					out.print(strLine);
					
					if (intCount == 1 || blFlag)
					{
%>
						<% if(strShowPriorityFl.equals("YES"))  {  %>  
					<tr id="tr<%=strTemp%>">
						<td colspan="6"  height="18" nowrap class="RightText">&nbsp;<A class="RightText" title="Click to Expand" href="javascript:Toggle('<%=strTemp%>')"><%=strSetId%> / <%=GmCommonClass.getStringWithTM(strSetName)%></td>
						<td colspan="5" class="RightText"  id="tdset<%=strTemp%>"></td>
					</tr>
					 <%}else{ %>	  
					
					<tr id="tr<%=strTemp%>">
						<td colspan="5"  height="18" nowrap class="RightText">&nbsp;<A class="RightText" title="Click to Expand" href="javascript:Toggle('<%=strTemp%>')"><%=strSetId%> / <%=GmCommonClass.getStringWithTM(strSetName)%></td>
						<td colspan="5" class="RightText"  id="tdset<%=strTemp%>"></td>
					</tr>
					<%} %>
					<tr>
						<td colspan="11"><div style="display:none" id="div<%=strTemp%>">
							<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tab<%=strTemp%>">
<%
								blFlag = false;
								}
%>
					<tr <%=strShade%>>
						<td width="70" class="RightText">&nbsp;<input type="radio" name="rad" onClick="fnCallDiv('<%=strConId%>','<%=strLoanStatusId%>','<%=strEtchId%>');">
						<%
							if(strCmtCnt.equals("0")){
						%>
						<fmtInHouseSetStatus:message key="IMG_ALT_CLICK_ADD_COMMENTS" var = "varAddComments"/>
						<img id="imgEdit" style="cursor:hand" src="<%=strImagePath%>/phone_icon.jpg" title="${varAddComments}" width="14" height="14" onClick="javascript:fnOpenLog('<%=strConId%>',1222)"/>
						<%}else{ %>
						<fmtInHouseSetStatus:message key="IMG_ALT_CLICK_ADD_COMMENTS" var = "varAddComments"/>
						<img id="imgEdit" style="cursor:hand" src="<%=strImagePath%>/phone-icon_ans18.gif" title="${varAddComments}" width="14" height="14" onClick="javascript:fnOpenLog('<%=strConId%>',1222)"/>
						<%} %>
						<%
						if(strHoldFl.equals("Y")){
						%>
						<fmtInHouseSetStatus:message key="IMG_ALT_LOANER_HOLD" var = "varLoanerHold"/>
						<img id="imgEdit" src="<%=strImagePath%>/hold-icon.png" title="${varLoanerHold}" width="14" height="14"/>
						<%} %>
						<% if((strLoanStatusId.equals("20")|| strLoanStatusId.equals("22") || strLoanStatusId.equals("21")) && strCommentsIconAccess.equals("Y")){ //PMT-30595 - Adding missing status id(22) for diplaying two Phone icon in missing status also. 
							if(strRequestCnt.equals("N")){
						%>
						<fmtInHouseSetStatus:message key="IMG_ALT_CLICK_TO_RECONCILIATION" var = "varReconciliation"/>						
						<img id="imgEdit" style="cursor:hand" src="<%=strImagePath%>/phone_icon.jpg" title="${varReconciliation}" width="14" height="14" onClick="javascript:fnOpenLog('<%=strRequestID%>',4000316)"/>
						<%}else{ %>
						<fmtInHouseSetStatus:message key="IMG_ALT_CLICK_TO_RECONCILIATION" var = "varReconciliation"/>
						<img id="imgEdit" style="cursor:hand" src="<%=strImagePath%>/phone-icon_ans18.gif" title="${varReconciliation}" width="14" height="14" onClick="javascript:fnOpenLog('<%=strRequestID%>',4000316)"/>						
						<%} } %>
						</td>
						<td width="100" height="20" class="RightText">&nbsp;<a class="RightText" href="javascript:fnPrintVer('<%=strConId%>');"><%=strConId%></a></td>
						<td width="100" class="RightText"><%=strEtchId%></td>
						<td width="100" class="RightText" align="center"><%=strCategory%></td>
						<td width="150" class="RightText" align="center"><%=strLoanStatus%>
						
						<% if(strShowPriorityFl.equals("YES"))  {  %>  
						<td width="150" class="RightText" align="center"><%=strPriority%>
			            <%} %>	  
			            
						<%if(strLoanStatusId.equals("60")){%>
						<fmtInHouseSetStatus:message key="IMG_ALT_CLICK_TO_HISTORY" var = "varHistory"/>
						<img id="imgEdit" style="cursor:hand" src="<%=strImagePath%>/HospitalIcon.gif" title="${varHistory}" width="15" height="12" onClick="javascript:fnOpenHistory('<%=strConId%>')"/>
						<% } %></td>
						<td width="150" class="RightText"><%=strLoanToNm%></td>
						<td width="100" class="RightText">&nbsp;<%=strLoanDt%></td>
						<td width="60" align="center" class="RightText">&nbsp;<%=strElapsedDays%></td>
						<td width="100" align="center" class="RightText">&nbsp;<%=strExpDate%></td>
						<td width="60" align="center" class="RightText">&nbsp;<%=strDaysOverdue%></td>
					</tr>
					<tr><td colspan="11" height="1" bgcolor="#cccccc"></td></tr>
<%					
					if ( intCount == 0 || i == intRepLength-1)
					{
						if (intRepLength == 1)
						{
							intSetCnt = 1;
						}
						if (i == intRepLength-1)
						{
							sbTemp.append("document.getElementById('tdset"+strTemp+"').innerHTML = '<b>"+intSetCnt+"</b>';");
						}
%>
							</table></div>
						</td>
					</tr>
<%
					}
				} // end of FOR	
							out.print("<script>");
							out.print(sbTemp.toString());
							out.print("</script>");
%>
					<tr>
						<td colspan="11" class="RightText" align="Center"><BR><fmtInHouseSetStatus:message key="LBL_CHOOSE_ACTION"/>:&nbsp;
						<gmjsp:dropdown controlName="Cbo_Action" seletedValue="" value="<%=alLoanerOpts%>"
						  codeId="CODENMALT"  codeName ="CODENM" defaultValue="[Choose One]"/>
						<fmtInHouseSetStatus:message key="BTN_SUBMIT" var="varSubmit"/>
						<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="fnCallEdit();" tabindex="25" buttonType="Save" />&nbsp;<BR><BR>
						</td>
					</tr>
<%
			} else	{
%> 
					<tr>
						<td height="30" colspan="11" align="center" class="RightTextBlue"><fmtInHouseSetStatus:message key="MSG_NO_DATA"/></td>
					</tr>
<%
			}
		}
%>
					<tr><td colspan="11" height="1" bgcolor="#666666"></td></tr>
				</table>
			</td>
		</tr>
	</table>
</FORM>
</BODY>
<%@ include file="/common/GmFooter.inc" %>
</HTML>
