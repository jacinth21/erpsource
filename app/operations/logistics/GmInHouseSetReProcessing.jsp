<?xml version="1.0" encoding="ISO-8859-1" ?>
    <%@include file="/common/GmHeader.inc"%>
    <%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
    
    <%
    	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
    	ArrayList CONSIGNSETDETAILS=GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("CONSIGNSETDETAILS"));
    	HashMap hmResult = new HashMap();
    	int intSetListQty =0;
    	int intQty = 0;
    	boolean bDontHide = false;
    	
    	for(int i = 0; i < CONSIGNSETDETAILS.size();i++)
    	{
    		hmResult = (HashMap)CONSIGNSETDETAILS.get(i);
    		
    		 intSetListQty=Integer.parseInt(GmCommonClass.parseZero((String)hmResult.get("SETLISTQTY")));
    		 intQty=Integer.parseInt(GmCommonClass.parseZero((String)hmResult.get("QTY")));
    		
    		if(intSetListQty < intQty || intSetListQty > intQty)
    		{
    			bDontHide = true;
    			break;
    		}    		
    	}
    
		String strAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
		HashMap hmCsg = new HashMap();
		// To get the Consignment information 
		hmCsg = (HashMap)request.getAttribute("hmCONSIGNMENT");
		if (hmCsg == null)
		{
			hmCsg = (HashMap)session.getAttribute("hmCONSIGNMENT");
		}
		
		String strReplenishId = GmCommonClass.parseNull((String)request.getAttribute("REPLENISHID"));	
		String strConsignId = GmCommonClass.parseNull((String)hmCsg.get("CONSIGNID"));
		String strLoanStatus = GmCommonClass.parseNull((String)hmCsg.get("LOANSFL"));
		String strType = GmCommonClass.parseNull((String)hmCsg.get("CTYPE"));
		String strReturnDt = GmCommonClass.parseNull((String)hmCsg.get("RETURNDT"));
		String strbDontHide = "fnSubmit('"+bDontHide+"');";
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%><html xmlns="http://www.w3.org/1999/xhtml">

<%@ taglib prefix="fmtInHouseSetReProcessing" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmInHouseSetReProcessing.jsp -->

<fmtInHouseSetReProcessing:setLocale value="<%=strLocale%>"/>
<fmtInHouseSetReProcessing:setBundle basename="properties.labels.operations.logistics.GmInHouseSetReProcessing"/>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Process Loaner Set - Pending-Process</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css"/>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmInHouseSetReProcessing.js"></script>
<script type="text/javascript">
var sServletPath = '<%=strServletPath%>';
var val = '<%=strConsignId%>';
var type = '<%=strType%>';


</script>
</head>
<body leftmargin="20" topmargin="10">
<form name="frmReprocess" method="post" >
<input type="hidden" name="hAction" id="hAction"/>
<input type="hidden" name="hCancelType" />
<input type="hidden" name="hTxnId" />
<input type="hidden" name="hDeployFl" value=""/>
	<table border="0" cellspacing="0" cellpadding="0" class="GtTable850">
	<tr>
	<td class="RightDashBoardHeader" height="20" align="left"><fmtInHouseSetReProcessing:message key="LBL_PROCESS_PENDING"/></td>
	<td height="25" class="RightDashBoardHeader" align="Right">
	<fmtInHouseSetReProcessing:message key="IMG_ALT_HELP" var = "varHelp"/>
	<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%//=strWikiTitle%>');" /></td>
	</tr>
	<tr>
	<td colspan="2">
	<jsp:include page="/include/GmIncludeConsignment.jsp" >
		<jsp:param name="PARENTJSP" value="PendingProcess" />
	</jsp:include>
	</td>
	</tr>
	<%
	if(!strAction.equals("Process_Reprocess")){
	%>
	<tr>
	<td colspan="2">
	<jsp:include page="/include/GmIncludeConsignSetDetails.jsp" />
	</td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<% if(bDontHide) { %>
		<tr><td colspan="2"><input type="radio" name="Chb_AckRback" value="Ack" checked></input><fmtInHouseSetReProcessing:message key="LBL_CS_ACKNOWLEDGES"/> </td></tr>
	<%} %>
	<tr><td colspan="2"><input type="radio" name="Chb_AckRback" value="Rback"></input><fmtInHouseSetReProcessing:message key="LBL_ROLBACK_PENDING_CHECK"/>  </td></tr>
	<%if(strType.equals("4119")){ %>
	<tr><td colspan="2"><input type="radio" name="Chb_AckRback" value="Deploye"></input> <fmtInHouseSetReProcessing:message key="LBL_DEPLOY_SET"/> </td></tr>
	<%} %>
	<tr><td colspan="2"></td></tr>
		<tr><td colspan="2" align="center">
		<fmtInHouseSetReProcessing:message key="BTN_SUBMIT" var="varSubmit"/>
	<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="<%=strbDontHide%>" buttonType="Save"/>
	</td></tr>	<%} %>
	<tr><td colspan="2">&nbsp;</td></tr>
	
	</table>
	</form>
	<%@ include file="/common/GmFooter.inc" %>
</body>
</html>