 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%>
 <%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Iterator" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%--<%@ page import="org.apache.commons.beanutils.DynaBean"%> --%>

<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ page buffer="16kb" autoFlush="true" %>
<%@ page import ="com.globus.common.beans.GmCommonControls" %>
<%@ taglib prefix="fmtChargesApproval" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- GmChargesApproval.jsp-->
<fmtChargesApproval:setLocale value="<%=strLocale%>"/>
<fmtChargesApproval:setBundle basename="properties.labels.operations.logistics.GmLoanerLateFeeApproval"/> 

<!-- Redis DropDown -->
<bean:define id="salesRep" name="frmLateFeeCharges" property="salesRep" type="java.lang.String"></bean:define>
<bean:define id="searchsalesRep" name="frmLateFeeCharges" property="searchsalesRep" type="java.lang.String"></bean:define>  
<bean:define id="fieldSales" name="frmLateFeeCharges" property="fieldSales" type="java.lang.String"></bean:define>
<bean:define id="searchfieldSales" name="frmLateFeeCharges" property="searchfieldSales" type="java.lang.String"></bean:define>  
<bean:define id="accessfl" name="frmLateFeeCharges" property="accessfl" type="java.lang.String"></bean:define>
<bean:define id="shwDtl" name="frmLateFeeCharges" property="shwDtl" type="java.lang.String"> </bean:define>
<bean:define id="applnDateFmt" name="frmLateFeeCharges" property="applnDateFmt" type="java.lang.String"> </bean:define>
<bean:define id="returnList" name="frmLateFeeCharges" property="alReturn" type="java.util.ArrayList"> </bean:define>
<bean:define id="deptId" name="frmLateFeeCharges" property="deptId" type="java.lang.String"> </bean:define>
<bean:define id="alStatus" name="frmLateFeeCharges" property="alStatus" type="java.util.ArrayList"> </bean:define>
<bean:define id="amounteventaccfl" name="frmLateFeeCharges" property="amounteventaccfl" type="java.lang.String"> </bean:define>
<bean:define id="statuseventaccfl" name="frmLateFeeCharges" property="statuseventaccfl" type="java.lang.String"> </bean:define>
<bean:define id="holdeventaccfl" name="frmLateFeeCharges" property="holdeventaccfl" type="java.lang.String"> </bean:define>
<bean:define id="crediteventaccfl" name="frmLateFeeCharges" property="crediteventaccfl" type="java.lang.String"> </bean:define>
<bean:define id="noteseventaccfl" name="frmLateFeeCharges" property="noteseventaccfl" type="java.lang.String"> </bean:define>
<bean:define id="statuslist" name="frmLateFeeCharges" property="alStatustListRpt" type="java.util.List"> </bean:define>
<bean:define id="strPOHtml"	name="frmLateFeeCharges" property="strPOHtml"	type="java.lang.String"></bean:define>	
<bean:define id="strReasonHtml"	name="frmLateFeeCharges" property="strReasonHtml"	type="java.lang.String"></bean:define>	
<bean:define id="alZone" name="frmLateFeeCharges" property="alZone" type="java.util.ArrayList"></bean:define>
<bean:define id="alRegions" name="frmLateFeeCharges" property="alRegions" type="java.util.ArrayList"></bean:define>
<bean:define id="alDivision" name="frmLateFeeCharges" property="alDivision" type="java.util.ArrayList"></bean:define>
	
<%
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
    String strWikiTitle = GmCommonClass.getWikiTitle("LOANER_LATE_FEE");

    //Enable/disable button
	String strDisable = "";
	if(!statuseventaccfl.equals("Y") && !noteseventaccfl.equals("Y")){
		strDisable = "true";
	}
	
    int currMonth = GmCalenderOperations.getCurrentMonth()-1;// To get the current month
	
	String strFromdaysDate = GmCommonClass.parseNull(GmCalenderOperations.getFirstDayOfMonth(currMonth)); //To get the first day of month
	String strTodaysDate = GmCommonClass.parseNull(GmCalenderOperations.getCurrentDate(strGCompDateFmt));
	
%>
<HTML>
<TITLE> Globus Medical: Late Fee Charges Approval</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>  
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<%-- <script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmGridCommon.js"></script> --%>
<!-- PC-5229 -->
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_dhxcalendar.js"></script>

<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/skins/dhtmlxcalendar_yahoolike.css">
<script language="javascript" src="<%=strOperationsJsPath%>/GmLoanerLateFeeApproval.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/jquery-latest.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<style>
	 ul {
     margin-left : 5px;
	}
	</style>
	<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
	table.DtTable1450 {
		margin: 0 0 0 0;
		width: 1450px;
		border: 1px solid  #676767;
	}
	.lblSpan {
  		float: left;
  		display: inline-block;
  		padding-top: 38px;
  		margin-right: 15px;
	}
	.divSpan{
	float:left;  
	margin-right: 100px;
	}
</style>
<script>
var switchUserFlag = '<%=strSwitchUserFl%>';
var accessfl = '<%=accessfl%>';
var format = '<%=applnDateFmt%>';
var amounteventaccfl = '<%=amounteventaccfl%>';
var statuseventaccfl = '<%=statuseventaccfl%>';
var holdeventaccfl = '<%=holdeventaccfl%>';
var crediteventaccfl = '<%=crediteventaccfl%>';
var noteseventaccfl = '<%=noteseventaccfl%>';
var strFromdaysDate = '<%=strFromdaysDate%>';
var strTodaysDate = '<%=strTodaysDate%>';
//Create Array Object and initialize array values for PO dropdown
var statusArr = new Array(1);
<%=strPOHtml%>;
var reasonArr = new Array(1);
<%=strReasonHtml%>;

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
<html:form action="/gmLoanerLateFeeAction.do?method=loadLateFeeDashboard">
<html:hidden property="incidentId" />
<html:hidden property="chargeFromDate" />

 <table border="0" class="DtTable1500"  cellspacing="0" cellpadding="0">
		<tr  style="height: 30px;">
			<td height="25" class="RightDashBoardHeader" colspan="6"> <fmtChargesApproval:message key="LBL_LOANER_LATE_FEE_CHARGES"/> </td>
			<td  height="25" class="RightDashBoardHeader" align="right" colspan="2">
			<!-- <fmtChargesApproval:message key="IMG_HELP" var="varHelp"/> -->
			<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
		       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		    </td>
		</tr>
		<tr><td class="LLine" height="1" colspan="8"></td></tr>	
		<tr class=""  style="height: 40px;" >
		<!-- Custom tag lib code modified for JBOSS migration changes -->
			<td height="25" class="RightTableCaption" align="right" width="15%">&nbsp;<fmtChargesApproval:message key="LBL_DISTRIBUTOR_NAME"/>:</td>
			<td colspan="2" style="vertical-align:top;" >&nbsp;<jsp:include page="/common/GmAutoCompleteInclude.jsp">
						<jsp:param name="CONTROL_NAME" value="fieldSales" />
						<jsp:param name="METHOD_LOAD" value="loadFieldSalesList" />
						<jsp:param name="WIDTH" value="200" />
						<jsp:param name="CSS_CLASS" value="search" />
						<jsp:param name="TAB_INDEX" value="4" />
						<jsp:param name="CONTROL_NM_VALUE" value="<%=searchfieldSales%>" />
						<jsp:param name="CONTROL_ID_VALUE" value="<%=fieldSales%>" />
						<jsp:param name="SHOW_DATA" value="10" />
					</jsp:include>
			</td>
			<td height="25" class="RightTableCaption" align="right"  width="12%">&nbsp;<fmtChargesApproval:message key="LBL_REP_NAME"/>:</td>
			<td padding="" style="vertical-align:top;">&nbsp;<jsp:include page="/common/GmAutoCompleteInclude.jsp">
						<jsp:param name="CONTROL_NAME" value="salesRep" />
						<jsp:param name="METHOD_LOAD" value="loadSalesRepList" />
						<jsp:param name="WIDTH" value="200" />
						<jsp:param name="CSS_CLASS" value="search" />
						<jsp:param name="TAB_INDEX" value="4" />
						<jsp:param name="CONTROL_NM_VALUE" value="<%=searchsalesRep%>" />
						<jsp:param name="CONTROL_ID_VALUE" value="<%=salesRep%>" />  
						<jsp:param name="SHOW_DATA" value="10" />
					</jsp:include>		         		
			</td>
			
		</tr>
		<tr><td class="LLine" height="1" colspan="8"></td></tr>	
		<tr class="oddshade"  style="height: 30px;">		
					<td class="RightTableCaption"  align="right">&nbsp;<fmtChargesApproval:message key="LBL_FROM_DATE"/>:</td>
			<td class="RightTableCaption" >&nbsp;<gmjsp:calendar SFFormName="frmLateFeeCharges"  controlName="fromDate"   gmClass="InputArea"  
					onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;<fmtChargesApproval:message key="LBL_TO_DATE"/>:&nbsp;<gmjsp:calendar SFFormName="frmLateFeeCharges" controlName="toDate"  gmClass="InputArea" 
					onFocus="changeBgColor(this,'#AACCE8');"  onBlur="changeBgColor(this,'#ffffff');"/></td>
		
			<td height="25" class="RightTableCaption" align="right">&nbsp;<fmtChargesApproval:message key="LBL_STATUS"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="status"  SFFormName='frmLateFeeCharges' SFSeletedValue="status"  defaultValue= "[Choose One]"	
						SFValue="alStatus" codeId="CODEID" codeName="CODENM"/> 
			</td>
			<td height="25" class="RightTableCaption" align="right" width="15%">&nbsp;<fmtChargesApproval:message key="LBL_REQUEST_ID"/>:</td>
			<td colspan="4">&nbsp;<html:text maxlength="50" property="requestID"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');fnClearReqId(this);"/> </td>
		</tr>
		<tr><td class="LLine" height="1" colspan="8"></td></tr>	
			<tr>
			<td class="RightRedCaption" colspan="8">
			<jsp:include page="/sales/Loaners/GmIncludeLoanerLateFeeFilters.jsp" >
			<jsp:param value="frmLateFeeCharges" name="FORMNAME"/>
			<jsp:param value="alZone" name="ALZONE"/>
			<jsp:param value="alRegions" name="ALREGIONS"/>
			<jsp:param value="alDivision" name="ALDIVISION"/>
			</jsp:include>
			 <span style="float: left;display: inline-block;padding-top: 38px;margin-right: 15px;">
			
			<fmtChargesApproval:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}" style="width: 5em;height: 22px;" gmClass="button" onClick="fnGo();" buttonType="Load" /></span></td>
				
		</tr>
		<tr><td class="LLine" height="1" colspan="8"></td></tr>	
		 <tr>
			<td colspan="8">	
				<div id="dataGridDiv"  style="width: 100%"></div>		
			</td>
		</tr> 
		
		 <tr>
			<td colspan="8" align="center">
				<div id="messagedata" style="visibility: visible;"><fmtChargesApproval:message key="LBL_NOTHING_FOUND_TO_DISPLAY"/></div></td>
		</tr> 
		<tr>
			<td colspan="8" align="center">
				<div id="amterrorMsg" style="display: none;"><fmtChargesApproval:message key="LBL_SAVE_AMOUNT_ERROR"/></div></td>
		</tr>
		<tr>
			<td colspan="8" align="center">
				<div id="cmnterrorMsg" style="display: none;"><fmtChargesApproval:message key="LBL_SAVE_COMMENT_ERROR"/></div></td>
		</tr>
<tr>
			<td colspan="8" align="center">
				<div id="mailsentmsg" style="display: none; padding-right:50px; color:green;"><fmtChargesApproval:message key="LBL_SEND_MAIL"/></div></td>
		</tr>
		<tr>
			<td colspan="8" align="center">
				<div id="mailsenterror" style="display: none;"><fmtChargesApproval:message key="LBL_SEND_MAIL_ERROR"/></div></td>
		</tr>
        	<tr style="display: none" id="DivExportExcel" height="25">
			<td colspan="8" align="center">
			    <div class='exportlinks'><fmtChargesApproval:message key="DIV_EXPORT_OPT"/> : <img
					src='img/ico_file_excel.png' />&nbsp;<a href="#"
					onclick="fnExport();"><fmtChargesApproval:message key="DIV_EXCEL"/> 
				</a></div>
			</td>
		</tr>
		<tr><td align="right"  colspan="3"><span id="submitBtn" style="visibility:hidden;">
		<fmtChargesApproval:message key="BTN_SUBMIT" var="varSubmit"/>
              <gmjsp:button value="${varSubmit}" disabled="<%=strDisable%>"  gmClass="button" onClick="fnSaveStatus();" buttonType="Save" /> 
		  </span>  </td>
        <td align="left" style="padding-left: 20px;" colspan="3"><span id="sendMailBtn" style="visibility:hidden;">
		<fmtChargesApproval:message key="BTN_SEND_MAIL" var="varSendMail"/>
             <gmjsp:button value="${varSendMail}" gmClass="button" onClick="fnSendHREmails();" buttonType="Save" /> 
		  </span></td>
		</tr>
		<tr></tr>
		
	</table>
</html:form>
		<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
